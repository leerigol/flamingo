
#include "crealstring.h"

namespace gui_fmt {

/*!
 * \brief CRealString::_positiveScaleList
 * 数值级数字符串表
 */
QList<QString> CRealString::_positiveScaleList;
QList<QString> CRealString::_negativeScaleList;
static const QString  _zeroPads=QLatin1Literal("00");

void CRealString::createScaleList()
{
    //! have created
    if ( _positiveScaleList.size() > 0 )
    { return; }

    _positiveScaleList.append("");
    _positiveScaleList.append("k");
    _positiveScaleList.append("M");
    _positiveScaleList.append("G");
    _positiveScaleList.append("T");

    _negativeScaleList.append("");
    _negativeScaleList.append("m");
    _negativeScaleList.append("u");
    _negativeScaleList.append("n");
    _negativeScaleList.append("p");
}

void CRealString::tuneError( float &value )
{
    if ( value == 0.0f ) return;

    //! raw value
    CRealString raw;
    raw.setValue( value );

    //! tune error
    float err;
    float val;

    //! float precison 7
    val = value;
    err = 9 * pow( 10, raw.mEVal - 7 );
    if ( raw.mbSign  )
    { val += err; }
    else
    { val -= err; }

    //! err tune
    CRealString errCaled;
    errCaled.setValue( val );

    //! E not changed
    if ( errCaled.mEVal == raw.mEVal )
    {
        int trimZLenPre, trimZLenPost;

        trimZLenPre = trimZeroLen( raw.mstrFract );
        trimZLenPost = trimZeroLen( errCaled.mstrFract );

        //! scale update
        if ( trimZLenPre > trimZLenPost )
        {
            value = val;
        }
        else
        {}
    }
    else
    {
        value = val;
    }
}

void CRealString::tuneError( double &value )
{
    if ( value == 0.0 ) return;

    //! raw value
    CRealString raw;
    raw.setValue( value );

    //! tune error
    double err;
    double val;

    //! double precision 16
    val = value;
    err = 9 * pow( 10, raw.mEVal - 16 );
    if ( raw.mbSign  )
    { val += err; }
    else
    { val -= err; }

    //! err tuned
    CRealString errCaled;
    errCaled.setValue( val );

    //! E not changed
    if ( errCaled.mEVal == raw.mEVal )
    {
        int trimZLenPre, trimZLenPost;

        trimZLenPre = trimZeroLen( raw.mstrFract );
        trimZLenPost = trimZeroLen( errCaled.mstrFract );

        //! scale update
        if ( trimZLenPre > trimZLenPost )
        {
            value = val;
        }
        else
        {}

    }
    else
    {
        value = val;
    }
}

//! trim the tail zero
int CRealString::trimZeroLen( const QString &str )
{
    //! raw len
    int len;
    int i;

    len = str.length();

    //! detect zero from tail
    for ( i = 0; i < len; i++ )
    {
        if ( str[len - 1 - i ] == '0' )
        {}
        else
        { break; }
    }

    return len - i;
}

CRealString::CRealString()
{
    mbValid = false;
}

CRealString::CRealString( qint32 val )
{
    *this = val;
}
CRealString::CRealString( qlonglong val )
{
    *this = val;
}
CRealString::CRealString( float val )
{
    *this = val;
}
CRealString::CRealString( double val )
{
    *this = val;
}
CRealString::CRealString( dsoReal &real )
{
    *this = real;
}

CRealString CRealString::operator= ( qint32 val )
{
    QString str;

    str = QString("%1").arg( val);

    mbSign = (val >= 0);
    mbValid = splitInteger( str );

    return *this;
}
CRealString CRealString::operator= ( qlonglong val )
{
    QString str;

    str = QString("%1").arg( val);

    mbSign = (val >= 0);
    mbValid = splitInteger( str );

    return *this;
}

//! 浮点数据需要修正最后一位的舍入误差
//! bug542:99.9999847,格式化为了99.99
//! 最好根据所需精度补数，而不是固定补数，
//! 已在servcursor_ui.cpp中需要4位有效数字时，在第6（7）位上补数
//! add_by_zx
CRealString CRealString::operator= ( float val )
{
    tuneError( val );

    setValue( val );

    return *this;
}

CRealString CRealString::operator= ( double val )
{
    tuneError( val );

    setValue( val );

    return *this;
}
CRealString CRealString::operator= ( dsoReal &real )
{
    //! base eval
    mEVal = real.mE;
    mbSign = (real.mA >= 0);

    //! full string
    QString str = QString::number( real.mA );
    int len = str.length();

    //! add scale
    if ( mbSign )
    { mEVal += (len - 1); }
    else
    { mEVal += (len - 2); }

    if ( real.mA >= 0 )
    {
        str.append("000000");
    }
    else
    {
        str.append("000000");
    }

    mstrFract = str;

    mbValid = true;

    return *this;
}

CRealString CRealString::operator= ( CRealString &val )
{
    mbValid = val.mbValid;
    mbSign = val.mbSign;

    mstrFract = val.mstrFract;
    mEVal = val.mEVal;

    return *this;
}
CRealString::CRealString( CRealString &val )
{
    mbValid = val.mbValid;
    mbSign = val.mbSign;

    mstrFract = val.mstrFract;
    mEVal = val.mEVal;
}

//! split val into fract and e
void CRealString::setValue( float val )
{
    QString str;

    //! paded with 0.00000xx e xx
    str = QString("%1").arg( val, 0, 'E', 6 );

    mbSign = ( val >= 0 );
    mbValid = splitSci( str );
}
void CRealString::setValue( double val )
{
    QString str;

    //! paded with 0.00000xx e xx
    str = QString("%1").arg( val, 0, 'E', 15 );

    mbSign = ( val >= 0 );
    mbValid = splitSci( str );
}

bool CRealString::isValid()
{
    return mbValid;
}

bool CRealString::normalize( QString &str,
                             int maxWidth,
                             int notrim )
{
    if ( !mbValid )
    { return false; }

    //! scale list
    createScaleList();

    //! calc the m/k/M/G/T
    int shiftDot;
    int scaleLevel;
    if ( mEVal >= 0 )
    {
        scaleLevel = mEVal / 3;
        shiftDot = mEVal - scaleLevel * 3;
    }
    else
    {
        scaleLevel = ( mEVal - 2 )/3;
        shiftDot = mEVal - scaleLevel * 3;
    }

    //! set the dot
    str = mstrFract;
    int fractLen;
    int dotPos;

    fractLen = str.length();
    dotPos = ( mbSign ? 1 : 2 ) + shiftDot;
    //! 小数点在最后面了
    if ( dotPos >= fractLen )
    {}
    else
    { str.insert( dotPos, "." ); }

    if ( mbSign )
    {
        //! limit the number count
        if ( str.size() > (maxWidth + 1 ) )
        {
            str = str.left( maxWidth + 1 );
        }
        //! no change
        else
        {}
    }
    else
    {
        //! 符号位 + 小数点
        if ( str.size() > (maxWidth + 2 ) )
        {
            str = str.left( maxWidth + 2 );
        }
        //! no change
        {}
    }

    //! trim the single dot at last
    do
    {
        int dotIndex;
        int len;

        len = str.length();
        dotIndex = str.indexOf('.');

        //! last dot?
        if ( dotIndex == len -1)
        {
            str.remove( dotIndex, 1 );
        }
    }while(0);

    //! trim end 0 from .
    if ( str.contains('.')  && notrim != 1 )
    {
        //! trim redunt zero
        {
            const QChar *pData;
            int dotIndex, fractLen;
            int len = str.length();
            int rzLen;

            dotIndex = str.indexOf( '.' );

            fractLen = len - dotIndex - 1;

            pData = str.constData();
            Q_ASSERT( pData != NULL );

            //! point to last
            pData += len - 1;
            rzLen = 0;
            while ( *pData == '0' && fractLen > 0 )
            {
                pData--;
                rzLen++;
                fractLen--;
            }

            //! has fract
            if ( fractLen > 0 )
            {
                str = str.left( len - rzLen );
            }
            //! no fract
            else
            {
                str = str.left( dotIndex );
            }
        }
    }

    //! add the 0 pad
    if ( notrim > 1 )
    {
        int zeroPad;

        if ( str.contains('.') )
        {
            zeroPad = (mbSign ? 0 : 1) + notrim + 1 - str.length();
        }
        else
        {
            zeroPad = (mbSign ? 0 : 1) + notrim - str.length();
            if ( zeroPad > 0 )
            { str.append('.'); }
        }

        //! add zero
        if ( zeroPad > 0 )
        {
            str = str.append( _zeroPads.data(), zeroPad );
        }
    }

    //! append the scale
    if ( mEVal >= 0 )
    {
        if ( scaleLevel >= _positiveScaleList.size() )
        { return false; }

        str.append( _positiveScaleList[scaleLevel] );
    }
    else
    {
        if ( -scaleLevel >= _negativeScaleList.size() )
        { return false; }

        str.append( _negativeScaleList[-scaleLevel] );
    }

    return true;
}

/*!
 * \brief CRealString::splitSci
 * \param sciStr
 *
 * 将规格化的数据串拆分
 * 例如：-1.23E-5 拆分成两个 “-1.23” 和 “-5”
 */
bool CRealString::splitSci( QString &sciStr )
{
    QStringList strList;

    strList = sciStr.split( "E" );

    //! fail
    if ( strList.size() < 2 )
    {
        qWarning()<<"!!! fail"<<__FUNCTION__<<__LINE__<<sciStr;
//        Q_ASSERT( false );
        return false;
    }

    //! remove the dot
    mstrFract = strList[0];
    if ( mbSign )
    {
        mstrFract.remove( 1, 1 );
    }
    else
    {
        mstrFract.remove( 2, 1 );
    }

    bool bOk;
    mEVal = strList[1].toInt( &bOk );
    if ( !bOk )
    {
        Q_ASSERT( false );
        return false;
    }

    return true;
}

bool CRealString::splitInteger( QString &intStr )
{
    int len;

    len = intStr.size();

    if ( len < 1 )
    {
        Q_ASSERT( false );
        return false;
    }

    if ( mbSign )
    {
        mEVal = intStr.size() - 1;
    }
    else
    {
        mEVal = intStr.size() - 2;
    }

    mstrFract = intStr;

    return true;
}
}
