#ifndef CGUIFORMATTER_H
#define CGUIFORMATTER_H

#include <QtCore>
#include "../include/dsotype.h"
#include "../baseclass/dsoreal.h"

namespace gui_fmt {

struct nameString
{
    int name;
    QString str;
};

#define START_OF_NAME_STRING()
#define A_NAME_STRING( name, str )  { (int)name, QLatin1Literal(str) }
#define A_NAME_QSTRING( name, str )  { (int)name, (str) }
#define END_OF_NAME_STRING()    {-1, "" }

class CGuiFormatter
{
private:
    static QString cvtString;
    static QString emptyString;

protected:
    static QString &searchString( nameString *pPairs, int val );
public:
    static QString &toString( const DsoType::Unit unit );
    static QString &toString( const DsoType::LaScale scale );
    static QString &toString( const DsoType::Chan ch );
    static QString &toString( const DsoType::Bandwidth bw );
    static QString &toString( const DsoType::TriggerSweep sweep );

    //! with seperator
    static void formatInteger( QString &str,
                               qlonglong val,
                               const DsoType::Unit unit=DsoType::Unit_none);
    static void formatInteger( QString &str,
                               qulonglong val,
                               const DsoType::Unit unit=DsoType::Unit_none);

    /*!
     * \brief CGuiFormatter::format
     *
     * -转换为含有单位字符表达式 p/n/u/m/k/M/G/T
     * -
     */
    static void format( QString &str, int val,
                        DsoType::DsoViewFmt fmt=fmt_def,
                        const DsoType::Unit unit=DsoType::Unit_none );
    static void format( QString &str, qlonglong val,
                        DsoType::DsoViewFmt fmt=fmt_def,
                        const DsoType::Unit unit=DsoType::Unit_none );
    static void format( QString &str, float val,
                        DsoType::DsoViewFmt fmt=fmt_def,
                        const DsoType::Unit unit=DsoType::Unit_none );
    static void format( QString &str, double val,
                        DsoType::DsoViewFmt fmt=fmt_def,
                        const DsoType::Unit unit=DsoType::Unit_none );
    static void format( QString &str, dsoReal &val,
                        DsoType::DsoViewFmt fmt=fmt_def,
                        const DsoType::Unit unit=DsoType::Unit_none );
};

}
#endif // CGUIFORMATTER_H
