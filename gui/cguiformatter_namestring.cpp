
static nameString _unit_string[]= {
    START_OF_NAME_STRING()

    A_NAME_STRING( DsoType::Unit_none, ("") ),

    A_NAME_STRING( DsoType::Unit_W, ("W") ),
    A_NAME_STRING( DsoType::Unit_A, ("A") ),
    A_NAME_STRING( DsoType::Unit_V, ("V") ),
    A_NAME_STRING( DsoType::Unit_U, ("U") ),

    A_NAME_STRING( DsoType::Unit_s, ("s") ),
    A_NAME_STRING( DsoType::Unit_hz, ("Hz") ),
    A_NAME_QSTRING( DsoType::Unit_degree, (QChar(0x00B0)) ),   //! degree
    A_NAME_STRING( DsoType::Unit_percent, ("%") ),

    A_NAME_STRING( DsoType::Unit_dbV, ("dBV") ),
    A_NAME_STRING( DsoType::Unit_db, ("dB") ),
    A_NAME_STRING( DsoType::Unit_dbm, ("dBm") ),

    A_NAME_STRING( DsoType::Unit_VmulS, ("v*s") ),
    A_NAME_STRING( DsoType::Unit_VdivS, ("v/s") ),
    A_NAME_STRING( DsoType::Unit_Vrms, ("Vrms") ),
    A_NAME_QSTRING( DsoType::Unit_oum,  (QChar(0x03A9)) ),

    A_NAME_STRING( DsoType::Unit_Div, ("div") ),
    A_NAME_STRING( DsoType::Unit_SaS, ("Sa/s") ),
    A_NAME_STRING( DsoType::Unit_Pts, ("pts") ),
    A_NAME_STRING( DsoType::Unit_Pts_, ("pts*") ),

    A_NAME_STRING( DsoType::Unit_VA,  ("VA") ),
    A_NAME_STRING( DsoType::Unit_VAR, ("VAR") ),

    A_NAME_STRING( DsoType::Unit_wfm, ("wfm") ),
    A_NAME_QSTRING( DsoType::Unit_V2,  (QString("V") + QChar(0x00B2)) ),
    A_NAME_QSTRING( DsoType::Unit_W2,  (QString("W") + QChar(0x00B2)) ),
    A_NAME_QSTRING( DsoType::Unit_A2,  (QString("A") + QChar(0x00B2)) ),
    A_NAME_QSTRING( DsoType::Unit_U2,  (QString("U") + QChar(0x00B2)) ),
    A_NAME_STRING( DsoType::Unit_WmulS, ("W*s") ),
    A_NAME_STRING( DsoType::Unit_AmulS, ("A*s") ),
    A_NAME_STRING( DsoType::Unit_UmulS, ("U*s") ),

    A_NAME_STRING( DsoType::Unit_WdivS, ("W/s") ),
    A_NAME_STRING( DsoType::Unit_AdivS, ("A/s") ),
    A_NAME_STRING( DsoType::Unit_UdivS, ("U/s") ),

    END_OF_NAME_STRING()
};

static nameString _lascale_string[]=
{
    START_OF_NAME_STRING()

    A_NAME_STRING( DsoType::Large, ("L") ),
    A_NAME_STRING( DsoType::Small, ("S") ),
    A_NAME_STRING( DsoType::Medium, ("M")),

    END_OF_NAME_STRING()
};

static nameString _ch_string[]=
{
    START_OF_NAME_STRING()

    A_NAME_STRING( DsoType::chan1, ("CH1") ),
    A_NAME_STRING( DsoType::chan2, ("CH2") ),
    A_NAME_STRING( DsoType::chan3, ("CH3") ),
    A_NAME_STRING( DsoType::chan4, ("CH4") ),

    A_NAME_STRING( DsoType::d0, ("D0") ),
    A_NAME_STRING( DsoType::d1, ("D1") ),
    A_NAME_STRING( DsoType::d2, ("D2") ),
    A_NAME_STRING( DsoType::d3, ("D3") ),

    A_NAME_STRING( DsoType::d4, ("D4") ),
    A_NAME_STRING( DsoType::d5, ("D5") ),
    A_NAME_STRING( DsoType::d6, ("D6") ),
    A_NAME_STRING( DsoType::d7, ("D7") ),

    A_NAME_STRING( DsoType::d8, ("D8") ),
    A_NAME_STRING( DsoType::d9, ("D9") ),
    A_NAME_STRING( DsoType::d10, ("D10") ),
    A_NAME_STRING( DsoType::d11, ("D11") ),

    A_NAME_STRING( DsoType::d12, ("D12") ),
    A_NAME_STRING( DsoType::d13, ("D13") ),
    A_NAME_STRING( DsoType::d14, ("D14") ),
    A_NAME_STRING( DsoType::d15, ("D15") ),

    A_NAME_STRING( DsoType::d0d7,("D0-D7")),
    A_NAME_STRING( DsoType::d8d15,("D8-D15")),
    A_NAME_STRING( DsoType::d0d15,("D0-D15")),

    A_NAME_STRING( DsoType::acline,("AC")),
    A_NAME_STRING( DsoType::ext,("Ext")),
    A_NAME_STRING( DsoType::ext5,("Ext5")),

    A_NAME_STRING( DsoType::r1,("R1")),
    A_NAME_STRING( DsoType::r2,("R2")),
    A_NAME_STRING( DsoType::r3,("R3")),
    A_NAME_STRING( DsoType::r4,("R4")),

    A_NAME_STRING( DsoType::r5,("R5")),
    A_NAME_STRING( DsoType::r6,("R6")),
    A_NAME_STRING( DsoType::r7,("R7")),
    A_NAME_STRING( DsoType::r8,("R8")),

    A_NAME_STRING( DsoType::r9,("R9")),
    A_NAME_STRING( DsoType::r10,("R10")),

    A_NAME_STRING( DsoType::m1,("M1")),
    A_NAME_STRING( DsoType::m2,("M2")),
    A_NAME_STRING( DsoType::m3,("M3")),
    A_NAME_STRING( DsoType::m4,("M4")),

    END_OF_NAME_STRING()
};

static nameString _bw_string[]=
{
    START_OF_NAME_STRING()

    A_NAME_STRING( DsoType::BW_20M, ("20M") ),
    A_NAME_STRING( DsoType::BW_25M, ("25M") ),
    A_NAME_STRING( DsoType::BW_50M, ("50M") ),
    A_NAME_STRING( DsoType::BW_70M, ("70M") ),
    A_NAME_STRING( DsoType::BW_100M, ("100M") ),

    A_NAME_STRING( DsoType::BW_150M, ("150M") ),
    A_NAME_STRING( DsoType::BW_200M, ("200M") ),
    A_NAME_STRING( DsoType::BW_250M, ("250M") ),
    A_NAME_STRING( DsoType::BW_300M, ("300M") ),
    A_NAME_STRING( DsoType::BW_350M, ("350M") ),

    A_NAME_STRING( DsoType::BW_500M, ("500M") ),
    A_NAME_STRING( DsoType::BW_600M, ("600M") ),
    A_NAME_STRING( DsoType::BW_750M, ("750M") ),
    A_NAME_STRING( DsoType::BW_1G, ("1G") ),
    A_NAME_STRING( DsoType::BW_2G, ("2G") ),

    A_NAME_STRING( DsoType::BW_4G, ("4G") ),
    A_NAME_STRING( DsoType::BW_5G, ("5G") ),
    A_NAME_STRING( DsoType::BW_10G, ("10G") ),
    A_NAME_STRING( DsoType::BW_20G, ("20G") ),

    END_OF_NAME_STRING()
};

static nameString _sweep_string[]=
{
    START_OF_NAME_STRING()

    A_NAME_STRING( DsoType::Trigger_Sweep_Auto,   ("A") ), /*"Auto"*/
    A_NAME_STRING( DsoType::Trigger_Sweep_Normal, ("N") ), /*"Normal"*/
    A_NAME_STRING( DsoType::Trigger_Sweep_Single, ("S") ), /*"Single"*/
    A_NAME_STRING( DsoType::Trigger_Sweep_Free,   ("F") ), /*"Free"*/

    END_OF_NAME_STRING()
};
