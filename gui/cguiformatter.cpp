
#include "../include/dsostd.h"

#include "crealstring.h"
#include "cguiformatter.h"

namespace gui_fmt {

//! internal var
QString CGuiFormatter::cvtString;
QString CGuiFormatter::emptyString;

//! name string
#include "cguiformatter_namestring.cpp"

//! -- converters
QString &CGuiFormatter::searchString( nameString *pPairs, int val )
{
    Q_ASSERT( NULL != pPairs );

    while( pPairs->name != -1 )
    {
        if ( pPairs->name == val )
        { return pPairs->str; }

        pPairs++;
    }

    Q_ASSERT( false );
    return CGuiFormatter::emptyString;
}

QString& CGuiFormatter::toString( const DsoType::Unit unit )
{
    return searchString( _unit_string, (int)unit );
}

QString& CGuiFormatter::toString( const DsoType::LaScale laScale )
{
    return searchString( _lascale_string, (int)laScale );
}

QString &CGuiFormatter::toString( const DsoType::Chan ch )
{
    return searchString( _ch_string, (int)ch );
}

QString &CGuiFormatter::toString( const DsoType::Bandwidth bw )
{
    return searchString( _bw_string, (int)bw );
}

QString &CGuiFormatter::toString( const DsoType::TriggerSweep sweep )
{
    return searchString( _sweep_string, (int)sweep );
}

void CGuiFormatter::formatInteger(
                           QString &str,
                           qlonglong val,
                           const DsoType::Unit unit)
{
    bool bNeg;

    //! convert to unsigned
    if ( val < 0 )
    {
        bNeg = true;
        val = -val;
    }
    else
    {
        bNeg = false;
    }

    //! format by qulonglong
    formatInteger( str, (qulonglong)val, unit );
    if ( bNeg )
    {
        str.prepend('-');
    }
    else
    {}
}

void CGuiFormatter::formatInteger(
                           QString &str,
                           qulonglong val,
                           const DsoType::Unit unit )
{
    //! number
    str = QString::number(val);

    //! add seperator
    QString strSep;
    int sepPoint, leftLen;

    //! raw length
    leftLen = str.length();
    sepPoint = leftLen - 3;

    while( sepPoint > 0  )
    {
        strSep.prepend( str.mid( sepPoint, 3) );
        strSep.prepend(',');
        leftLen -= 3;

        sepPoint -= 3;
    }

    //! the last one
    if ( leftLen > 0 )
    {
        strSep.prepend( str.mid( 0, leftLen) );
    }

    //! the sep item
    strSep.append( CGuiFormatter::toString(unit) );

    str = strSep;
}

//! format the val by int or real
//! -- int
//! -- real: xxx.x K/M/G/T m/u/n/p
void CGuiFormatter::format( QString &str,
                            int val,
                            DsoType::DsoViewFmt fmt,
                            const DsoType::Unit unit)
{
    if ( fmt_type(fmt) == fmt_int )
    {
        formatInteger( str, (qlonglong)val, unit );
    }
    else if ( fmt_type(fmt) == fmt_float )
    {
        CRealString realVal( val );

        bool bOk;
        bOk = realVal.normalize( str, fmt_width(fmt) );
        if ( bOk )
        {
            str.append( CGuiFormatter::toString(unit) );
        }
        else
        { str.clear(); }
    }
    else
    {}
}

void CGuiFormatter::format( QString &str,
                            qlonglong val,
                            DsoType::DsoViewFmt fmt,
                            const DsoType::Unit unit )
{
    if ( fmt_type(fmt) == fmt_int )
    {
        formatInteger( str, (qulonglong)val, unit );
    }
    else if ( fmt_type(fmt) == fmt_float )
    {
        CRealString realVal( val );

        bool bOk;
        bOk = realVal.normalize( str, fmt_width(fmt) );
        if ( bOk )
        {
            str.append( CGuiFormatter::toString(unit) );
        }
        else
        { str.clear(); }
    }
    else
    {}
}

void CGuiFormatter::format( QString &str,
                            float val,
                            DsoType::DsoViewFmt fmt,
                            const DsoType::Unit unit )
{
    //! .nf
    if ( fmt_type(fmt) == fmt_f )
    {
        //! val to double
        str = QString::number( val, 'f', f_precision(fmt) ) ;

        str.append( CGuiFormatter::toString(unit) );
    }
    else if ( fmt_type(fmt) == fmt_float )
    {
        CRealString realVal( val );

        bool bOk;
        bOk = realVal.normalize( str,
                                 fmt_width(fmt),
                                 fmt_trim(fmt) );

        if ( bOk )
        {
            str.append( CGuiFormatter::toString(unit) );
        }
        else
        {
            str.clear();
        }
    }
    else
    {
        Q_ASSERT( false );
    }
}

void CGuiFormatter::format( QString &str,
                            double val,
                            DsoType::DsoViewFmt fmt,
                            const DsoType::Unit unit )
{
    //! .nf
    if ( fmt_type(fmt) == fmt_f )
    {
        str = QString::number( val, 'f', f_precision(fmt) ) ;

        str.append( CGuiFormatter::toString(unit) );
    }
    else if ( fmt_type(fmt) == fmt_float )
    {
        CRealString realVal( val );

        bool bOk;
        bOk = realVal.normalize( str,
                                 fmt_width(fmt),
                                 fmt_trim(fmt) );
        if ( bOk )
        {
            str.append( CGuiFormatter::toString(unit) );
        }
        else
        { str.clear(); }
    }
    else
    {
        Q_ASSERT( false );
    }
}

void CGuiFormatter::format(
                        QString &str,
                        dsoReal &val,
                        DsoType::DsoViewFmt fmt,
                        const DsoType::Unit unit )
{
    DsoReal real( val );
    real.normalize();

    //! float format
    if ( fmt_type(fmt) == fmt_float )
    {
        //! zero
        if ( real.mA == 0 )
        {
            int fmtWidth = fmt_width( fmt );

            str.clear();
            for ( int i = 0; i < fmtWidth; i++ )
            {
                str.append('0');
            }

            //! add dot
            str.insert( 1, '.' );

            //! append unit
            str.append( CGuiFormatter::toString(unit) );
            return;
        }

        CRealString realString( val );

        bool bOk;
        bOk = realString.normalize( str,
                                 fmt_width(fmt),
                                 fmt_trim(fmt) );
        if ( bOk )
        {
            str.append( CGuiFormatter::toString(unit) );
        }
        else
        {
            str.clear();
        }

        return ;
    }

    //! .nf
    if ( fmt_type(fmt) == fmt_f )
    {
        str = QString::number( real.toFloat(), 'f', f_precision(fmt) ) ;

        str.append( CGuiFormatter::toString(unit) );

        return;
    }

    if( fmt_type(fmt) == fmt_int )
    {
        qlonglong value = 1;
        int e = val.mE - E_0;
        for(int i=0; i<e; i++ )
        {
            value = value * 10;
        }
        if( e > 0 )
        {
            value = value * val.mA;
            formatInteger( str, value, unit );
            return;
        }
    }

    //! interger format
    DsoRealType_A a;
    DsoE e;
    real.getVal( a, e );

    //! 0
    if ( a == 0 )
    {
        str = "0";
        str.append( CGuiFormatter::toString(unit) );
        return;
    }

    //! append scale
    if ( e == E_0 )
    {
        str = QString("%1").arg( a );
        str.append( CGuiFormatter::toString(unit) );
        return;
    }
    else
    {
        str = QString("%1E%2").arg( a ).arg( (int)e );
        str.append( CGuiFormatter::toString(unit) );
    }
}

}

