#ifndef CREALSTRING
#define CREALSTRING

#include <QtCore>

#include "../baseclass/dsoreal.h"

namespace gui_fmt {

/*!
 * \brief The CRealString class
 * 用于表示规格化后的数字string
 */
class CRealString
{
protected:
    static QList<QString> _positiveScaleList;
    static QList<QString> _negativeScaleList;

protected:
    static void createScaleList();

public:
    static void tuneError( float &val );
    static void tuneError( double &val );

    static int trimZeroLen( const QString &str );

public:
    CRealString();
    CRealString( qint32 val );
    CRealString( qlonglong val );
    CRealString( float val );
    CRealString( double val );
    CRealString( dsoReal &real );

    CRealString operator= ( qint32 val );
    CRealString operator= ( qlonglong val );
    CRealString operator= ( float val );
    CRealString operator= ( double val );
    CRealString operator= ( dsoReal &real );

private:
    CRealString operator= ( CRealString &val );
    CRealString( CRealString &val );

    void setValue( float val );
    void setValue( double val );

public:
    bool isValid();

    bool normalize( QString &str,
                    int maxWidth = 4,
                    int notrim = 0 );

protected:
    bool splitSci( QString &sciStr );
    bool splitInteger( QString &intStr );


public:
    QString mstrFract;  /*!< 小数部分*/
    int     mEVal;      /*!< 指数部分 */
    bool    mbValid;
    bool    mbSign;     /*!< 符号位 */
};

}

#endif // CREALSTRING

