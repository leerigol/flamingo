#ifndef DSOATTR_H
#define DSOATTR_H

#include <QtCore>
#include "../baseclass/dsoreal.h"
#include "../baseclass/dsofract.h"

class DsoAttr
{
public:
    DsoAttr();

    //! -- hori
    void sett( qlonglong t0,
               qlonglong tinc,
               DsoReal real=DsoReal(1,E_N12) );
    void sett( dsoFract t0,
               dsoFract tinc,
               DsoReal real=DsoReal(1,E_N12) );

    void setLength( int len );
    int getLength();

    void setllt0( qlonglong t0 );
    qlonglong getllt0();

    void setlltInc( qlonglong tinc );
    qlonglong getlltInc();

    void settBase( DsoReal base );
    DsoReal gettBase();

    DsoReal gett0();
    DsoReal gettInc();

    //! -- vert
    void setv( qlonglong gnd,
               dsoFract yinc,
               bool inv= false );

    void setyInc( dsoFract inc );
    void setyInc( dsoFract inc, DsoReal realBase );
    dsoFract getyInc();

    float realyInc();

    void setyGnd( qlonglong ygnd, int yorig = 128 );
    qlonglong getyGnd();
    int getyOrig();

    void setInvert( bool inv );
    bool getInvert();

    void setyDiv( dsoFract ydiv );
    dsoFract getyDiv();

    void setyRefScale( qlonglong yscale );
    qlonglong getyRefScale();

    void setyRefOffset( qlonglong yOffset );
    qlonglong getyRefOffset();

    void setyRefBase( DsoReal realBase );
    DsoReal getyRefBase();

    //! -- hybid
    void setAttr( DsoAttr *pAttr );
    DsoAttr* getAttr();

    void settAttr( DsoAttr *pAttr );
    void settAttr( qlonglong t0,
                   qlonglong tinc,
                   int saLength,
                   DsoReal real=DsoReal(1,E_N12) );
    void setvAttr( DsoAttr *pAttr );

    void setAttrId(int id);
    int  getAttrId( );
protected:
                                    //! hori
    qlonglong m_llt0;
    qlonglong m_lltInc;
    DsoReal m_tRefBase;

    int m_length;                   //! sa length

    dsoFract m_yInc;                //! vertical
    dsoFract m_yDiv;

    qlonglong  m_yGnd;               //! math的gnd范围可能特别大
    int  m_yOrig;                    //! 128
    int  m_yRng;                     //! 200
    bool m_yInvert;                  /*!< bottom to top: true */

    qlonglong m_yRefScale;          //! ref info
    qlonglong m_yRefOffset;
    qlonglong m_yRefDiv;
    DsoReal m_yRefBase;             //! refbase, eg, 1e-6

    int  m_AttrId;
};

#endif // DSOATTR_H
