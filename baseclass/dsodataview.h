#ifndef DSOVIEW_H
#define DSOVIEW_H

#include "dsowfm.h"

class DsoDataView
{

public:
    DsoDataView();

public:
    void view( DsoWfm &wfmIn, DsoWfm *pWfmOut );

private:
    float tInc;     //! view attributes
    float t0;
    float yInc;
    float yGnd;
    bool yInvert;
                    //! view left, length
    int vLeft, vLength;
};

#endif // DSOVIEW_H
