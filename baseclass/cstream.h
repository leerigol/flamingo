#ifndef CSTREAM
#define CSTREAM

#include <QtCore>


typedef unsigned int  cryptKey;

/*!
 * \brief The CStream class
 *
 * 记录数据流
 * -数据流就是一段串行的缓冲
 * -数据流分私密和公开
 *   -私密数据流不可以存储在公开的介质上，例如，系统的上电次数，只能存储在内部的FRAM中
 *   -私密数据需要进行加密
 */
class CStream
{
public:
    CStream( void *pStream=NULL, int cap = 0 );

protected:
    void *p_mBase;
    int mCap;
    int mLen;
    int mOff;

public:
    int getLength();
    int setLength( int len );
    int setSeek( int off );
    void *getBase();

private:
    int out( const void *ptr, int len );
    int in( void *ptr, int len );

public:
    /*!
     * \brief write
     * \param name 名称
     * \param dat  数据
     * \param bPrivate 私密
     * \param key  密钥
     * \return
     * 存储类型值到数据流
     */
    //! aligned
    virtual int write( const QString &name, const bool &dat, bool bPrivate = false, cryptKey key=0);
    virtual int write( const QString &name, const char &dat, bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, const short &dat, bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, const int &dat, bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, const qlonglong &dat, bool bPrivate = false,  cryptKey key=0 );

    virtual int write( const QString &name, const unsigned char &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const unsigned short &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const unsigned int &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const qulonglong &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const QString &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, void *pBuf, int length, bool bPrivate = false,  cryptKey key=0 );

    virtual int read( const QString &name, bool &val, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, char &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, short &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, int &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, qlonglong &dat, bool bPrivate = false,  cryptKey key=0 );

    virtual int read( const QString &name, unsigned char &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, unsigned short &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, unsigned int &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, qulonglong &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, QString &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, void *pBuf, int cap, bool bPrivate = false,  cryptKey key=0 );

    //! bitstream
    //! write
    virtual int write( const QString &name, bool *pdat,
                       int bits = 1, int size = 1,
                       bool bPrivate = false, cryptKey key=0);
    virtual int write( const QString &name, char *dat,
                       int bits = 8, int size = 1,
                       bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, short *dat,
                       int bits = 16,int size = 2,
                       bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, int *dat,
                       int bits = 32,int size = 4,
                       bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, qlonglong *dat,
                       int bits = 64,int size = 8,
                       bool bPrivate = false,  cryptKey key=0 );

    virtual int write( const QString &name, unsigned char *dat,
                       int bits = 8, int size = 1,
                       bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, unsigned short *dat,
                       int bits = 16,int size = 2,
                       bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, unsigned int *dat,
                       int bits = 32,int size = 4,
                       bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, qulonglong *dat,
                       int bits = 64,int size = 8,
                       bool bPrivate = false,  cryptKey key=0 );

    //! read
    virtual int read( const QString &name, bool *pdat,
                       int bits = 1,int size = 1,
                       bool bPrivate = false, cryptKey key=0);
    virtual int read( const QString &name, char *dat,
                       int bits = 8,int size = 1,
                       bool bPrivate = false,  cryptKey key=0);
    virtual int read( const QString &name, short *dat,
                       int bits = 16,int size = 2,
                       bool bPrivate = false,  cryptKey key=0);
    virtual int read( const QString &name, int *dat,
                       int bits = 32,int size = 4,
                       bool bPrivate = false,  cryptKey key=0);
    virtual int read( const QString &name, qlonglong *dat,
                       int bits = 64,int size = 8,
                       bool bPrivate = false,  cryptKey key=0 );

    virtual int read( const QString &name, unsigned char *dat,
                       int bits = 8,int size = 1,
                       bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, unsigned short *dat,
                       int bits = 16,int size = 2,
                       bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, unsigned int *dat,
                       int bits = 32,int size = 4,
                       bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, qulonglong *dat,
                       int bits = 64,int size = 8,
                       bool bPrivate = false,  cryptKey key=0 );

protected:
    CStream & operator<<( const bool &dat );
    CStream & operator<<( const char &dat );
    CStream & operator<<( const short &dat );
    CStream & operator<<( const int &dat );

    CStream & operator<<( const unsigned char &dat );
    CStream & operator<<( const unsigned short &dat );
    CStream & operator<<( const unsigned int &dat );
    CStream & operator<<( const qlonglong &dat );
    CStream & operator<<( const qulonglong &dat );
    CStream & operator<<( const QString &str );

    CStream & operator>>( bool &dat );
    CStream & operator>>( char &dat );
    CStream & operator>>( short &dat );
    CStream & operator>>( int &dat );

    CStream & operator>>( unsigned char &dat );
    CStream & operator>>( unsigned short &dat );
    CStream & operator>>( unsigned int &dat );
    CStream & operator>>( qlonglong &dat );
    CStream & operator>>( qulonglong &dat );
    CStream & operator>>( QString &str );
};

#define ar_out( name, val ) stream.write( QStringLiteral( name ), val  );
#define ar_in( name, val ) stream.read( QStringLiteral( name ), val  );
#define ar_e_in( name, val ) stream.read( QStringLiteral( name ), (int&)val  );

#define ar_pack_out_n( name, val, n ) stream.write( QStringLiteral( name ), &val, n  );
#define ar_pack_in_n( name, val, n ) stream.read( QStringLiteral( name ), &val, n  );

#define ar_pack_out( name, val ) stream.write( QStringLiteral( name ), &val );
#define ar_pack_in( name, val ) stream.read( QStringLiteral( name ), &val );

#define ar_pack_e_out( name, val ) stream.write( QStringLiteral( name ), &val );
#define ar_pack_e_in( name, val ) stream.read( QStringLiteral( name ), (int*)(&val) );

#define pack_out( name, val ) stream.write( QStringLiteral( name ), val );
#define pack_in( name, val )  stream.read( QStringLiteral( name ), val );

#endif // CSTREAM

