#ifndef RLANGCHANGEEVENT_H
#define RLANGCHANGEEVENT_H

#include "../include/dsotype.h"

using namespace DsoType;

/*!
 * \brief The RLangchangeEvent class
 * 系统语言切换事件
 */
class RLangchangeEvent : public QEvent
{
public:
    RLangchangeEvent( SystemLanguage lang = language_english );
public:
    SystemLanguage mLanguage;
};
#endif // RLANGCHANGEEVENT_H
