
#include <math.h>
#include <QMap>
#include <QRegExp>
#include <QDebug>

#include "dsoreal.h"
#include "../include/dsostd.h"


float realf( int a, DsoE e )
{
    return a * (float)pow( 10.0, (double)e );
}
double reald( int a, DsoE e )
{
    return a * pow( 10.0, (double)e );
}

float realf( long long a, DsoE e )
{
    return a * (float)pow( 10.0, (double)e );
}
double reald( long long a, DsoE e )
{
    return a * pow( 10.0, (double)e );
}

qlonglong normf( float a, DsoE e )
{
    return a / (float)pow( 10.0, (double)e );
}

qlonglong normd( double a, DsoE e )
{
    return a / pow( 10.0, (double)e );
}


DsoErr DsoReal::mErr = ERR_NONE;

/*!
 * \brief DsoReal::DsoReal
 *
 * default constructor, the default mA = 0, mE = E_0
 */
DsoReal::DsoReal( DsoE e )
{
    mA = 1;
    mE = e;

    mbUnk = false;
}
/*!
 * \brief DsoReal::DsoReal
 * \param a assigned to mA
 * \param e assigned to mE
 */
DsoReal::DsoReal( DsoRealType_A a, DsoE e )
{
    mA = a;
    mE = e;

    mbUnk = false;
}

/*!
 * \brief DsoReal::DsoReal
 * \param dsoReal
 * assign constructor
 */
DsoReal::DsoReal( const DsoReal &dsoReal )
{
    *this = dsoReal;
}

/*!
 * \brief DsoReal::DsoReal
 * \param str
 * string constructor. 输入必须有效，没有错误返回机制。
 * 输入有效的条件：
 * - 没有非数字字符
 * - 没有超限
 * 有效值检查请使用函数 isValid( QString str )
 * \sa isValid
 *
 */
DsoReal::DsoReal( const QString &str )
{
    setLastErr( tryConvert(str) );
}

DsoReal::DsoReal( float val )
{
    setLastErr( setVal(val) );
}

/*!
 * \brief DsoReal::operator =
 * \param dsoReal
 * \return dsoReal
 * 赋值运算符
 */
DsoReal & DsoReal::operator=( const DsoReal &val )
{
    val.getVal( mA, mE );
    mbUnk = val.isUnk();
    return *this;
}

DsoReal& DsoReal::operator=( DsoRealType_A val )
{
    mA = val;
    mE = E_0;
    mbUnk = false;
    return *this;
}

DsoReal::DsoReal( const dsoReal &real )
{
    *this = real;
}
DsoReal &DsoReal::operator=( const dsoReal &real )
{
    mA = real.mA;
    mE = real.mE;
    mbUnk = false;
    return *this;
}
bool DsoReal::isUnk() const
{
    return mbUnk;
}

/*!
 * \brief DsoReal::setVal
 * \param a
 * \param e
 * \return DsoErr
 * 数值设置
 */
DsoErr DsoReal::setVal( DsoRealType_A a, DsoE e )
{
    mA = a;
    mE = e;
    mbUnk = false;

    //normalize();
    return ERR_NONE;
}

/*!
 * \brief DsoReal::setVal
 * \param str
 * \return
 * set value with string
 * -ERR_CONVERT_FAIL convert to DsoReal fail
 */
DsoErr DsoReal::setVal( QString str )
{
    return tryConvert( str );
}

DsoErr DsoReal::setVal( double fVal )
{
    QString str;
    bool bOk;
    //! convert to string
//    str = QString("%1").arg(fVal,0,'E', 0 );
    str = QString::number( fVal, 'E' );

    QStringList strList = str.split("E");
    if ( strList.size() < 2 )
    {
        qWarning()<<"Fail convert"<<fVal;
        return ERR_CONVERT_FAIL;
    }

    //! find dot
    int dot, fractLen;
    QString strFract;

    strFract = strList[0];
    dot = strFract.indexOf( '.' );
    if ( dot < 0 )
    {
        fractLen = 0;
    }
    else
    {
        fractLen = strFract.length() - 1 - dot;
        //! remove dot
        strFract.remove( dot, 1 );
    }

    mA =  strFract.toLongLong( &bOk );
    if ( !bOk )
    {
        mA = 0;
        qWarning()<<"Fail convert"<<fVal;
        return ERR_CONVERT_FAIL;
    }

    mE =  (DsoE)( (int)E_0 + strList[1].toInt( &bOk ) - fractLen );
    if ( !bOk )
    {
        mA = 0;
        mE = E_0;
        qWarning()<<"Fail convert"<<fVal;
        return ERR_CONVERT_FAIL;
    }

    mbUnk = false;

    //! normalize -- remove the redunt zero
    normalize();

    return ERR_NONE;
}

/*!
 * \brief DsoReal::getVal
 * \param a [out]
 * \param e [out]
 * 数值读取
 */
void DsoReal::getVal( DsoRealType_A &a, DsoE &e ) const
{
    a = mA;
    e = mE;
}

DsoRealType_A DsoReal::getInteger() const
{
    return mA;
}

/*!
 * \brief DsoReal::toReal
 * \return
 * 转换为实际数值
 */
DsoErr DsoReal::toReal( float &out, float def )
{
    double val;

    if ( ERR_NONE != toReal( val, def ) )
    {
        out = def;
        return ERR_CONVERT_FAIL;
    }

    if ( val > FLOAT_MAX || val < FLOAT_MIN )
    {
        out = def;
        return ERR_OVER_RANGE;
    }

    out = (float)val;

    return ERR_NONE;
}
/*!
 * \brief DsoReal::toReal
 * \param out
 * \return
 * convert to double
 */
DsoErr DsoReal::toReal( double &out, double /*def*/ )
{
    out = mA * pow( 10, mE - DsoType::E_0 );
    return ERR_NONE;
}
/*!
 * \brief DsoReal::toReal
 * \param out
 * \return
 * -ERR_CONVERT_FAIL
 * -ERR_OVER_RANGE
 * convert to int
 */
DsoErr DsoReal::toReal( int &out, int def )
{
    double val;

    if ( ERR_NONE != toReal( val) )
    {
        out = def;
        return ERR_CONVERT_FAIL;
    }

    if ( val > INT_MAX || val < INT_MIN )
    {
        out = def;
        return ERR_OVER_RANGE;
    }

    out = (int)val;

    return ERR_NONE;
}

DsoErr DsoReal::toReal( qlonglong &out, qlonglong /*def*/ )
{
    int e = (int)mE;

    out = (qlonglong)mA;
    if ( e > 0 )
    {
        while( e > 0 )
        {
            out *= 10;
            e--;
        }
    }
    else if ( e < 0 )
    {
        while( e < 0 )
        {
            out /= 10;
            e++;
        }
    }
    else
    {}

    return ERR_NONE;
}

float DsoReal::toFloat()
{
    float val;

    toReal( val );

    return val;
}
double DsoReal::toDouble()
{
    double val;

    toReal( val );

    return val;
}

DsoErr DsoReal::alignBase( DsoReal &base )
{
    int eBase, eNow;

    eBase = (int)base.mE;
    eNow = (int)mE;
    while ( eNow > eBase )
    {
        mA *= 10;
        eNow--;
    }

    while ( eNow < eBase )
    {
        mA /= 10;
        eNow++;
    }

    Q_ASSERT( base.mA != 0 );

    mE = (DsoE)eNow;
//    mA = ( mA / base.mA ) * base.mA;
    mA = ( mA / base.mA );

    return ERR_NONE;
}

DsoErr DsoReal::alignBase( DsoE e )
{
    int eBase, eNow;

    eBase = (int)e;
    eNow = (int)mE;
    while ( eNow > eBase )
    {
        mA *= 10;
        eNow--;
    }

    while ( eNow < eBase )
    {
        mA /= 10;
        eNow++;
    }

    mE = (DsoE)eNow;

    return ERR_NONE;
}

/*!
 * \brief DsoReal::toString
 * \return
 * convert to string with fmt=[-] mA E [-] mE
 */
QString  DsoReal::toString()
{
    QString str;

    if ( mE != E_0 )
    {
        str = QString("%1E%2").arg(mA).arg( mE - E_0 );
    }
    else
    {
        str = QString("%1").arg(mA);
    }

    return str;
}

void DsoReal::addE( DsoE e )
{
    mE = (DsoE)( (int)mE + (int)e );

    Q_ASSERT( mE > E_MIN && mE < E_MAX );
}

/*!
 * \brief DsoReal::operator <
 * \param real
 * \return bool
 * low than
 */
bool DsoReal::operator<(  DsoReal real )
{
    DsoRealType_A a;
    DsoE e;

    real.getVal( a, e );

    /// -,+
    if ( (mA < 0 && a>=0) || (mA <= 0 && a > 0) )
    {
        return true;
    }
    /// +,-
    if ( (mA > 0 && a<=0) || (mA >=0 && a < 0) )
    {
        return false;
    }

    if ( mA == 0 && a==0 )
    {
        return false;
    }

    /// same flag
    if ( mA > 0 )
    {
        if ( mA > a )
        {
            return lt( *this, real );
        }
        else if ( mA < a )
        {
            return mt( real, *this );
        }
        else // mA == a
        {
            return mE < e;
        }
    }
    /// mA < 0
    else
    {
        DsoReal r1, r2;

        r1 = -*this;
        r2 = -real;

        return r1 > r2;
    }

    return false;
}
/*!
 * \brief DsoReal::operator <=
 * \param real
 * \return
 * low or equal
 */
bool DsoReal::operator<=( DsoReal real )
{
    return !(*this>real);
}
/*!
 * \brief DsoReal::operator >
 * \param real
 * \return
 * more
 */
bool DsoReal::operator>( DsoReal real )
{
    return (real < *this);
}
/*!
 * \brief DsoReal::operator >=
 * \param real
 * \return
 * more or equal
 */
bool DsoReal::operator>=( DsoReal real )
{
    return !( *this < real );
}
/*!
 * \brief DsoReal::operator !=
 * \param real
 * \return
 * not equal
 */
bool DsoReal::operator!=( DsoReal real )
{
    return ( *this < real || *this > real );
}
/*!
 * \brief DsoReal::operator ==
 * \param real
 * \return
 * equal
 */
bool DsoReal::operator==(  DsoReal &real )
{
    return !(*this != real);
}

/*!
 * \brief DsoReal::operator +
 * \param real
 * \return
 * 算术运算需要注意计算中的溢出判定
 * getLastErr会复位ERR
 */
DsoReal DsoReal::operator+( DsoReal real )
{
    DsoReal a1;
    DsoReal ret;
    DsoErr err;
    a1 = *this;

    //! + 0
    if ( real == DsoReal(0ll) )
    {
        return *this;
    }

    err = fixedAb( real, a1 );
    setLastErr( err );
    if ( err != ERR_NONE )
    {
        return 0ll;
    }

    DsoRealType_A aA, bA, retA;
    DsoE aE, bE;

    real.getVal( aA, aE );
    a1.getVal( bA, bE );

    retA = aA + bA;
    if ( aA > 0 && bA >0 && retA < 0 )
    {
        setLastErr( ERR_OVER_RANGE );
        return 0ll;
    }
    else if ( aA < 0 && bA < 0 && retA > 0 )
    {
        setLastErr( ERR_OVER_RANGE );
        return 0ll;
    }
    else
    {}

    ret.setVal( retA, aE );

    ret.normalize();

    return ret;
}
/*!
 * \brief DsoReal::operator -
 * \param real
 * \return
 *
 */
DsoReal DsoReal::operator-( DsoReal real )
{
    return *this + (-real);
}
/*!
 * \brief DsoReal::operator *
 * \param real
 * \return
 *
 */
DsoReal DsoReal::operator*( DsoReal real )
{
    DsoReal ret;
    DsoReal a( *this );

    a.normalize();
    real.normalize();

    DsoRealType_A aA, bA, retA;
    DsoE aE, bE, retE;

    a.getVal( aA, aE );
    real.getVal( bA, bE );

    retE = (DsoE)(aE + bE);
    retA = aA * bA;

    if ( retE <= E_MIN )
    {
        setLastErr( ERR_OVER_RANGE );
        return 0ll;
    }
    else if ( retE >= E_MAX )
    {
        setLastErr( ERR_OVER_RANGE );
        return 0ll;
    }

    if ( retA != 0 && (retA / aA) != bA )
    {
        setLastErr( ERR_OVER_RANGE );
        return 0ll;
    }

    ret.setVal( retA, retE );
    ret.normalize();

    return ret;
}
/*!
 * \brief DsoReal::operator /
 * \param real
 * \return
 * \todo
 * 默认下为3位精度，即分母先扩大1000倍
 */
DsoReal DsoReal::operator/( DsoReal real )
{
    if ( real == DsoReal(0ll) )
    {
        setLastErr(ERR_DIV_0);
        return 0ll;
    }

    if ( mA == 0ll )
    {
        return DsoReal(0ll);
    }

    DsoReal a( *this );
    DsoRealType_A aA, bA, retA;
    DsoE aE, bE, retE;

    a.normalize();
    real.normalize();

    a.getVal( aA, aE );
    real.getVal( bA, bE );

    retA = aA * 1000;
    if ( retA / 1000 != aA )
    {
        setLastErr( ERR_OVER_RANGE );
        return 0ll;
    }
    aE = (DsoE)(aE - 3);

    retA = retA / bA;
    retE = (DsoE)(aE - bE);

    if ( retE <= E_MIN || retE >= E_MAX )
    {
        setLastErr( ERR_OVER_RANGE );
        return 0ll;
    }

    return DsoReal( retA, retE );
}

/*!
 * \brief DsoReal::normalize
 * normalize the value the remove redunt 0
 * 1000 E0 ==> 1 E3
 */
void DsoReal::normalize()
{
    DsoRealType_A a;

    if ( mA == 0 )
    {
        mE = E_0;
        return;
    }

    a = mA;
    while ( a == ((a / 10) * 10 ) )
    {
        a /= 10;
        mE = (DsoE)(mE + 1);
    }

    mA = a;
}

/*!
 * \brief tryConvert
 * \param str
 * \return
 * check the string to convert to DsoReal
 * use QRegExp to find the value
 */
DsoErr DsoReal::tryConvert( QString str )
{
    static QMap<QString,int> map;

    //! dictionary
    map.insert( "p", -12);
    map.insert( "n", -9);
    map.insert( "u", -6);
    map.insert( "m", -3);
    map.insert( "k", 3);
    map.insert( "M", 6);
    map.insert( "G", 9);
    map.insert( "T", 12);

    QRegExp regExp( "^([+|-]?\\d*)(.(\\d+))?(e|E)([+|-]?(\\d+))?(p|n|u|m|k|M|G|T)?$");
    regExp.setCaseSensitivity( Qt::CaseSensitive );

    if ( regExp.indexIn( str ) < 0 )
    {
        return ERR_CONVERT_FAIL;
    }

    //! +1.25e-10
    int a,b, fractLen;
    int e;
    bool ok;
    a = 0;
    if ( regExp.cap(1).length() > 0 )
    {
        a = regExp.cap(1).toInt( &ok );
        if ( !ok )
        {
            return ERR_CONVERT_FAIL;
        }
    }

    b = 0;
    fractLen = regExp.cap(3).length();
    if ( fractLen > 0 )
    {
        b = regExp.cap(3).toInt( &ok );
        if ( !ok )
        {
            return ERR_CONVERT_FAIL;
        }

        for ( int i = 0; i < fractLen; i++ )
        {
            if ( COverflow::overflow_mul(a,10) )
            {
                return ERR_CONVERT_FAIL;
            }
            a *= 10;
        }

        if ( a < 0 )
        {
            b = -b;
        }
    }

    if ( COverflow::overflow_add(a,b))
    {
        return ERR_CONVERT_FAIL;
    }
    a += b;

    e = -fractLen;
    if ( regExp.cap(5).length() > 0 )
    {
        e += regExp.cap(5).toInt( &ok );
        if ( !ok )
        {
            return ERR_CONVERT_FAIL;
        }
    }

    if ( regExp.cap(7).length() > 0 )
    {
        QString unit = regExp.cap(7);
        if ( COverflow::overflow_add(e, map[unit]) )
        {
            return ERR_CONVERT_FAIL;
        }
        e += map[ unit ];
    }

    mA = a;
    mE = (DsoE)e;
    mbUnk = false;

    return ERR_NONE;
}


/*!
 * \brief DsoReal::lt
 * \param a
 * \param b
 * \return
 * low than
 * both a and b are +
 * - a<b: true
 */
bool DsoReal::lt( DsoReal &a, DsoReal &b )
{
    DsoRealType_A aA, bA;
    DsoE aE, bE;

    int delta, scale, rnk, remi;

    a.getVal( aA, aE );
    b.getVal( bA, bE );

    delta = aE - bE;
    rnk = rank10( aA, bA, remi );
    scale = delta + rnk;

    return scale < 0;
}

/*!
 * \brief DsoReal::mt
 * \param a
 * \param b
 * \return
 * more than
 * both a and b are +
 * - a>b: true
 */
bool DsoReal::mt( DsoReal &a, DsoReal &b )
{
    DsoRealType_A aA, bA;
    DsoE aE, bE;

    int delta, scale, rnk, remi;

    a.getVal( aA, aE );
    b.getVal( bA, bE );

    delta = aE - bE;
    rnk = rank10( aA, bA, remi );
    scale = delta + rnk;

    if ( scale >0 ) return true;
    if ( scale == 0 && remi > 0) return true;
    return false;
}

/*!
 * \brief DsoReal::fixedAb
 * \param a
 * \param b
 * \return
 * 调整 a, b的级数，以小的进行对齐
 */
DsoErr DsoReal::fixedAb( DsoReal &a, DsoReal &b )
{
    //! normalize
    a.normalize();
    b.normalize();

    //! tune scale
    DsoRealType_A aA, bA;
    DsoE aE, bE;
    int remi, rnk;
    int deltaE;

    a.getVal( aA, aE );
    b.getVal( bA, bE );

    //! scale to b
    if ( aE > bE )
    {
        deltaE = aE - bE;

        if ( aA > 0 )
        {
            rnk = rank10( INT_MAX, aA, remi );
        }
        else if ( aA < 0 )
        {
            rnk = rank10( INT_MIN, aA, remi );
        }
        else
        {
            return ERR_NONE;
        }

        if ( deltaE > rnk )
        {
            return ERR_OVER_RANGE;
        }
        else
        {
            aA *= (DsoRealType_A)pow(10, deltaE );
            aE = bE;
        }
    }
    else if ( aE < bE )
    {
        return fixedAb( b, a );
    }
    else
    {}


    return ERR_NONE;
}

/*!
 * \brief DsoReal::rank10
 * \param a
 * \param b
 * \param remi [out]
 * \return
 * a b must be positive
 * a === b * 10(rank10)
 */
int DsoReal::rank10( DsoRealType_A a,
                     DsoRealType_A b,
                     int &remi )
{
    int rnk;
    DsoRealType_A raw;
    int i, scale;

    rnk = 0;
    if ( a > b )
    {
        Q_ASSERT( b != 0 );

        raw = a;
        while( a/b >=10 )
        {
            a /= 10;
            rnk++;
        }

        for ( i =0, scale=1; i < rnk; i++ )
        {
            scale *= 10;
        }

        remi = raw - b * scale;
    }
    else if ( a < b )
    {
        Q_ASSERT( a != 0 );

        raw = b;
        while( b/a >=10 )
        {
            b /= 10;
            rnk--;
        }

        for ( i =0, scale=1; i > rnk; i-- )
        {
            scale *= 10;
        }

        remi = raw - a * scale;
    }
    else
    {
        rnk = 0;
        remi = 0;
    }

    return rnk;
}
/*!
 * \brief DsoReal::doLimit
 * \param valIn
 * \param valOut [out]
 * \param min
 * \param max
 * \return
 * limit the valIn in [ min, max ]
 */
DsoErr DsoReal::doLimit( DsoReal &valIn, DsoReal &valOut, const DsoReal &min, const DsoReal &max )
{
    DsoReal rMin( min );
    DsoReal rMax( max );

    if ( valIn < rMin )
    {
        valOut = rMin;
        return ERR_OVER_RANGE;
    }
    else if ( valIn > rMax )
    {
        valOut = rMax;
        return ERR_OVER_RANGE;
    }
    else
    {
        valOut = valIn;
        return ERR_NONE;
    }
}

/*!
 * \brief DsoReal::setLastErr
 * \param err
 * \return
 * save the err when not ERR_NONE
 */
void  DsoReal::setLastErr( DsoErr err )
{
    if ( err != ERR_NONE )
    {
        DsoReal::mErr = err;
    }
}

/*!
 * \brief DsoReal::getLastErr
 * \return
 * get the err and rst to 0
 */
DsoErr DsoReal::getLastErr()
{
    DsoErr err = DsoReal::mErr;
    DsoReal::mErr = ERR_NONE;
    return err;
}

//! realGp
DsoRealGp::DsoRealGp()
{
//    vNow.setVal( 10ll );
//    vMax.setVal( 100ll );
//    vMin.setVal( 1ll );
//    vDef.setVal( 10ll );
}

void DsoRealGp::setValue(
               const DsoReal& now,
               const DsoReal& max,
               const DsoReal& min,
               const DsoReal& def )
{
    vNow = now;
    vMax = max;
    vMin = min;
    vDef = def;
}

void DsoRealGp::setValue( DsoRealType_A vnow,
               DsoRealType_A vmax,
               DsoRealType_A vmin,
               DsoRealType_A vdef,
               DsoE vE )
{
    vNow.setVal( vnow, vE );
    vMax.setVal( vmax, vE );
    vMin.setVal( vmin, vE );
    vDef.setVal( vdef, vE );
}

void DsoRealGp::mul( const DsoReal &scale )
{
    vNow = vNow * scale;
    vMax = vMax * scale;
    vMin = vMin * scale;
    vDef = vDef * scale;
}

DsoReal operator-( const DsoReal &real )
{
    DsoRealType_A a;
    DsoE e;
    DsoReal realB;

    real.getVal( a, e );
    realB.setVal( -a, e );
    return realB;
}
bool operator==( const DsoReal &a, const DsoReal &b )
{
    DsoReal a1 = a;
    DsoReal b1 = b;

    return a1 == b1;
}

bool COverflow::overflow_add(int a, int b)
{
    return !( (a+b-b) ) == a ;
}

bool COverflow::overflow_mul(int a, int b)
{
    return !( ( a*b ) / b) == a;
}
