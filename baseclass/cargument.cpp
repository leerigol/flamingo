#include <QDebug>
#include "cargument.h"

CObj::CObj()
{
    mbGc = true;
}

bool CObj::getGc()
{
    return mbGc;
}

/*!
 * \brief CObj::accept
 * 主动接收，不再进行垃圾回收
 */
void CObj::accept()
{
    mbGc = false;
}
void CObj::ignore()
{
    mbGc = true;
}

CVal::CVal()
{
    vType = val_none;
}

CVal::CVal( const CVal &val )
{
    vType = val.vType;

    bVal = val.bVal;
    iVal = val.iVal;
    llVal = val.llVal;

    fVal = val.fVal;
    dVal = val.dVal;

    pVal = val.pVal;
    eVal = val.eVal;

    strVal = val.strVal;
    arbVal = val.arbVal;
    pObj = val.pObj;

    realVal = val.realVal;
}

DsoErr CVal::checkValType( ValType refType )
{
    if ( refType != vType )
    {
        return ERR_TYPE_MIS_MATCH;
    }

    return ERR_NONE;
}

void CVal::setVal(bool val)
{
    vType = val_bool;
    bVal = val;
}
void CVal::setVal( int val )
{
    vType = val_int;
    iVal = val;
}
void CVal::setVal( qlonglong val )
{
    vType = val_longlong;
    llVal = val;
}
void CVal::setVal( float val )
{
    vType = val_float;
    fVal = val;
}
void CVal::setVal( double val )
{
    vType = val_double;
    dVal = val;
}
void CVal::setVal( pointer val )
{
    vType = val_ptr;
    pVal = val;
}

void CVal::setVal( QString &str )
{
    vType = val_string;
    strVal = str;
}
void CVal::setVal( ArbBin &val )
{
    vType = val_arb_bin;
    arbVal = val;
}

void CVal::setVal( CObj *obj )
{
    vType = val_obj;
    pObj = obj;
}

void CVal::setVal( const DsoReal &real )
{
    vType = val_real;
    realVal = real;
}

DsoErr CVal::getVal( bool &val )
{
    GET_x_VAL( val_bool, bVal );
}
DsoErr CVal::getVal( int &val )
{
    GET_x_VAL( val_int, iVal );
}
DsoErr CVal::getVal( qlonglong &val )
{
    GET_x_VAL( val_longlong, llVal );
}

DsoErr CVal::getVal( float &val )
{
    GET_x_VAL( val_float, fVal );
}
DsoErr CVal::getVal( double &val )
{
    GET_x_VAL( val_double, dVal );
}
DsoErr CVal::getVal( pointer &val )
{
    GET_x_VAL( val_ptr, pVal );
}
DsoErr CVal::getVal( QString &val )
{
    GET_x_VAL( val_string, strVal );
}

DsoErr CVal::getVal( ArbBin &val )
{
    GET_x_VAL( val_arb_bin, arbVal );
}

DsoErr CVal::getVal( CObj  * &val )
{
    GET_x_VAL( val_obj, pObj );
}

DsoErr CVal::getVal( DsoReal &val )
{
    GET_x_VAL( val_real, realVal );
}

void CVal::enter()
{}
void CVal::exit()
{}

bool operator==( const CVal &a, const CVal &b )
{
    if ( a.vType != b.vType )
    {
        return false;
    }

    switch( a.vType )
    {
        case val_bool:
            return a.bVal == b.bVal;
        case val_int:
            return a.iVal == b.iVal;
        case val_longlong:
            return a.llVal == b.llVal;

        case val_float:
            return a.fVal == b.fVal;
        case val_double:
            return a.dVal == b.dVal;

        case val_ptr:
            return a.pVal == b.pVal;

        case val_string:
            return a.strVal == b.strVal;

        default:
            return false;
    }
}

CArgument::CArgument()
{
}
CArgument::~CArgument()
{
}
CArgument::CArgument( const CArgument &val )
{
    mArgs = val.mArgs;
}
CArgument &CArgument::operator=( const CArgument &val )
{
    mArgs = val.mArgs;

    return (*this);
}

void CArgument::append( const CArgument &vals )
{
    mArgs.append( vals.mArgs );
}

void CArgument::append( CVal &val )
{
    mArgs.append( val );
}

void CArgument::append( bool val )
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}
void CArgument::append( int val  )
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}
void CArgument::append( qlonglong val )
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}

void CArgument::append( const DsoReal &val )
{
    CVal obj;

    obj.setVal( val );
    mArgs.append( obj );
}

void CArgument::append( float val )
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}
void CArgument::append( double val)
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}
void CArgument::append( pointer val )
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}

void CArgument::append(  QString val)
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}
void CArgument::append(  ArbBin val )
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}
void CArgument::append( CObj *val)
{
    CVal obj;
    obj.setVal( val );
    mArgs.append( obj );
}

void CArgument::clear()
{
    mArgs.clear();
}

void CArgument::setVal( bool val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}
void CArgument::setVal( int val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}
void CArgument::setVal( qlonglong val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}

void CArgument::setVal( DsoReal val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}

void CArgument::setVal( float val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}
void CArgument::setVal( double val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}
void CArgument::setVal( pointer val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}

void CArgument::setVal(  QString val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}
void CArgument::setVal(  ArbBin val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}

void CArgument::setVal( CObj *val, int index )
{
    prepareIndex( index );

    mArgs[index].setVal( val );
}


DsoErr CArgument::getVal( bool &val, int index  )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}
DsoErr CArgument::getVal( int &val, int index  )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}
DsoErr CArgument::getVal( qlonglong &val, int index  )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}

DsoErr CArgument::getVal( float &val, int index  )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}
DsoErr CArgument::getVal( double &val, int index  )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}
DsoErr CArgument::getVal( pointer &val, int index  )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}

DsoErr CArgument::getVal( QString &str, int index  )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( str );
}
DsoErr CArgument::getVal( ArbBin &val, int index )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}

DsoErr CArgument::getVal( CObj* &val, int index )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}

DsoErr CArgument::getVal( DsoReal &val, int index )
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    return mArgs[index].getVal( val );
}

DsoErr CArgument::getVal( CVal &val, int index ) const
{
    if ( !checkIndex(index ) ) return ERR_OVER_RANGE;

    val = mArgs[index];

    return ERR_NONE;
}

CVal& CArgument::operator[]( int index )
{
    Q_ASSERT( index >=0 );
    if(index >= mArgs.size())
    {
        Q_ASSERT( index < mArgs.size() );
    }
    return mArgs[ index ];
}

//! only value different
bool CArgument::isLike( const CArgument &args )
{
    if ( mArgs.size() != args.mArgs.size() )
    {
        return false;
    }

    //! type match
    for ( int i = 0; i < mArgs.size(); i++ )
    {
        if ( mArgs[i].vType == args.mArgs[i].vType )
        {
        }
        else
        {
            return false;
        }
    }

    return true;
}

bool CArgument::checkType( ValType t1,
                           ValType t2,
                           ValType t3,
                           ValType t4 ) const
{
    ValType ts[4];

    int paraCnt = 0;

    do
    {
        ts[paraCnt] = t1; paraCnt += ( t1 != val_none );
        ts[paraCnt] = t2; paraCnt += ( t2 != val_none );
        ts[paraCnt] = t3; paraCnt += ( t3 != val_none );
        ts[paraCnt] = t4; paraCnt += ( t4 != val_none );
    }while(0);

    //! size mismatch
    if ( paraCnt != size() )
    { return false; }

    //! type check
    for ( int i = 0; i < paraCnt; i++ )
    {
        if ( mArgs[i].vType != ts[i] )
        { return false; }
    }

    return true;
}

bool CArgument::operator==( const CArgument &args )
{
    if ( mArgs.size() != args.mArgs.size() )
    {
        return false;
    }

    for ( int i = 0; i < mArgs.size(); i++ )
    {
        if ( mArgs[i] == args.mArgs[i] )
        {
        }
        else
        {
            return false;
        }
    }

    return true;
}

bool CArgument::operator!=( const CArgument &args )
{
    return !( *this == args );
}

int CArgument::size() const
{
    return mArgs.size();
}

void CArgument::prepareIndex( int index )
{
    if ( index < 0 )
    {
        Q_ASSERT( false );
        return;
    }

    CVal val;
    for ( int i=mArgs.size(); i <= index;i++ )
    {
        mArgs.append( val );
    }
}

bool CArgument::checkIndex( int index ) const
{
    if ( index < 0 || index >= mArgs.count() )
    {
        return false;
    }

    return true;
}

//bool operator==( const CArgument &args,
//                 const CArgument &argsB )
//{
//    return args.operator==(argsB);
//}
