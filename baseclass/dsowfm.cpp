#include "dsowfm.h"
#include "../include/dsodbg.h"
#include "./resample/reSample.h"

const int DsoWfm::_ySIZE;
const int DsoWfm::_yTOP;
const int DsoWfm::_yDOWN;

void DsoWfm::buildConvertTable( float table[] )
{
    float rInc;
    rInc = realyInc();
    //! calc the table
    for ( int i = 0; i < DsoWfm::_ySIZE; i++ )
    {
        table[i] = ( i - m_yGnd) * rInc;
    }
}

void DsoWfm::_init()
{
    m_pPoint = NULL;
    m_capPoint = 0;

    m_pValue = NULL;
    m_capValue = 0;

    m_vLeft = 0;
    m_vRight = 0;

    m_size = 0;
    m_start = 0;
    m_length = 0;

    mPhyAddr = 0;
    m_bFloatValid = true;
}

void DsoWfm::setFloatValid(bool bValid)
{
    m_bFloatValid = bValid;
}

bool DsoWfm::getFloatValid()
{
    return m_bFloatValid;
}

DsoWfm::DsoWfm()
{
    _init();
}

DsoWfm::DsoWfm( const DsoWfm &wfm )
{
    _init();
    *this = wfm;
}
DsoWfm::DsoWfm( DsoWfm &wfm )
{
    _init();
    *this = wfm;
}
DsoWfm::DsoWfm( DsoWfm *pWfm )
{
    _init();
    *this = *pWfm;
}

DsoWfm &DsoWfm::operator=( const DsoWfm &wfm )
{
    assign( wfm );
    return *this;
}

DsoWfm & DsoWfm::operator=(DsoWfm &wfm )
{
    assign( wfm );
    return *this;
}

void DsoWfm::assign( const DsoWfm &wfm )
{
    if ( this == &wfm )
    {
        return;
    }
    //! base attr
    m_AttrId = wfm.m_AttrId;
    setAttr((DsoAttr*) &wfm );

    //! view attr
    m_vLeft = wfm.m_vLeft;
    m_vRight = wfm.m_vRight;
    m_bFloatValid = wfm.m_bFloatValid;

    //! has point
    if ( wfm.m_pPoint != NULL )
    {
        //! free current resource
        if ( m_capPoint < wfm.m_capPoint )
        {
            if ( m_capPoint == 0 )
            {}
            else
            {
                delete []m_pPoint;
            }

            //! allocate memory
            m_pPoint = R_NEW( DsoPoint[ wfm.m_capPoint ] );
            Q_ASSERT( NULL != m_pPoint );
            m_capPoint = wfm.m_capPoint;
        }
        else
        {
            //! keep memory
        }

        memcpy( m_pPoint,
                wfm.m_pPoint,
                sizeof(DsoPoint)*wfm.m_capPoint );
    }
    //! no point
    else
    {
        if ( m_pPoint != NULL )
        {
            delete []m_pPoint;
            m_pPoint = NULL;
        }
        m_capPoint = 0;
    }

    //! has value
    if ( wfm.m_pValue != NULL )
    {
        if ( m_capValue < wfm.m_capValue )
        {
            if ( m_capValue == 0 )
            {}
            else
            {
                delete []m_pValue;
            }

            m_pValue = R_NEW( float[ wfm.m_capValue ] );
            Q_ASSERT( NULL != m_pValue );
            m_capValue = wfm.m_capValue;
        }
        else
        {
            //! keep
        }

        if(m_bFloatValid)
        {
            memcpy( m_pValue,
                    wfm.m_pValue,
                    wfm.m_capValue * sizeof(float) );
        }
    }
    else
    {
        if ( m_pValue !=NULL )
        {
            delete []m_pValue;
            m_pValue = NULL;
        }
        m_capValue = 0;
    }

    //! data size
    m_start = wfm.m_start;
    m_size = wfm.m_size;
}

DsoWfm::~DsoWfm()
{
    if ( m_pPoint != NULL )
    {
        delete []m_pPoint;
        m_capPoint = 0;
    }

    if ( m_pValue != NULL )
    {
        delete []m_pValue;
        m_capValue = 0;
    }
}

void DsoWfm::setViewRange( int left, int right )
{
    m_vLeft = left;
    m_vRight = right;
}

void DsoWfm::setAddr( quint32 addr )
{ mPhyAddr = addr; }
quint32 DsoWfm::getAddr( )
{ return mPhyAddr; }

void DsoWfm::init( int cap )
{
    Q_ASSERT( cap >= 0 );

    //! allocate memory
    if ( NULL != m_pPoint )
    {
        if ( cap > m_capPoint )
        {
            delete []m_pPoint;

            m_pPoint = R_NEW( DsoPoint[ cap ] );
            Q_ASSERT( NULL != m_pPoint );
            m_capPoint = cap;
        }
    }
    else
    {
        if ( cap > 0 )
        {
            m_pPoint = R_NEW( DsoPoint[ cap ] );
            Q_ASSERT( NULL != m_pPoint );
            m_capPoint = cap;
        }
        //! keep null
        else
        {}
    }

    //! allocate value
    if ( NULL != m_pValue )
    {
        if ( cap > m_capValue )
        {
            delete []m_pValue;

            m_pValue = R_NEW( float[cap] );
            Q_ASSERT( NULL != m_pValue );
            m_capValue = cap;
        }
    }
    else
    {
        if ( cap > 0 )
        {
            m_pValue = R_NEW(float[cap]);
            Q_ASSERT( NULL != m_pValue );
        }
        //! keep null
        else
        {}

        m_capValue = cap;
    }

    m_start = 0;
    m_size = 0;
}

void DsoWfm::setPoint( DsoPoint *point,
                       int size,    //! disk size
                       int length,  //! dot count
                       int offset )
{
//    Q_ASSERT( length >= 0 && offset >= 0 );

    if( length < 0 || offset < 0)
    {
        qDebug()<<"dsoWfm"<<length<<offset;
        length = offset;
    }

    //! no data in
    if ( length > 0 )
    { Q_ASSERT( NULL != point ); }
    else
    {
        m_size = 0;
        m_length = 0;
        return;
    }

    //! real cap
    int cap = size + offset;

    //! allocate memory
    if ( NULL != m_pPoint )
    {
        if ( cap > m_capPoint )
        {
            delete []m_pPoint;
            m_pPoint = R_NEW( DsoPoint[ cap ] );
            Q_ASSERT( NULL != m_pPoint );
            m_capPoint = cap;
        }

        m_size = size;
    }
    else
    {
        m_pPoint = R_NEW( DsoPoint[ cap ] );
        Q_ASSERT( NULL != m_pPoint );
        m_capPoint = cap;

        m_size = size;
    }

    m_start = offset;
    m_length = length;


    //! add_by_zx：在la的trace中，size表示块的大小，length表示la数据的长度
//    if(size != length)
//    {
//        //! zx注：cap,size和length含义模糊
//        Q_ASSERT(false);
//    }
    //! copy data in
    memcpy( m_pPoint + offset,
            point,
            size * sizeof(DsoPoint) );
}

DsoPoint *DsoWfm::getPoint() const
{
    return m_pPoint;
}

void DsoWfm::setValue( float *pVal,
                       int size,
                       int offset )
{
    Q_ASSERT( NULL != pVal );
    Q_ASSERT( size >= 0 && offset >= 0);

    int cap;

    cap = size + offset;

    if ( m_capValue < cap )
    {
        if ( m_capValue > 0 )
        {
            delete []m_pValue;
        }

        m_pValue = R_NEW( float[cap] );
        Q_ASSERT( NULL != m_pValue );
        m_capValue = cap;
    }

    m_size = size;
    m_start = offset;
    m_length = size;

    Q_ASSERT( NULL != m_pValue );
    memcpy( m_pValue + offset,
            pVal,
            size * sizeof(float) );

}
float *DsoWfm::getValue() const
{
    return m_pValue;
}

void DsoWfm::setRange( int offset, int size )
{
    if(offset < 0 )
    {
        offset = 0;
    }
    if(size < 0)
    {
        size = 0;
    }

    m_size = size;
    m_start = offset;
}

int DsoWfm::size()
{
    return m_size;
}

int DsoWfm::start()
{ return m_start; }

DsoPoint DsoWfm::at(int i) const
{
    return m_pPoint[i];
}

DsoPoint DsoWfm::operator[](int i) const
{
    return at(i);
}
DsoPoint & DsoWfm::operator[](int i)
{
    return m_pPoint[i];
}

float DsoWfm::valueAt( int i ) const
{
    return m_pValue[i];
}

float DsoWfm::toReal( int adcY )
{
    float val;

    val = ( adcY - m_yGnd ) * realyInc();

    if ( m_yInvert )
    {val = -val;}

    return val;
}

float * DsoWfm::toValue( float val[],
                         int from,
                         int length )
{
    int i;
    int end;

    float cvtTable[ DsoWfm::_ySIZE ];

    //! build the table
    buildConvertTable( cvtTable );

    //! guess the end
    if ( length == -1 )
    {
        end = m_size + m_start;
    }
    else
    {
        end = from + length;


        if ( end > m_size + m_start )
        {
            end = m_size + m_start;
        }
    }

    for ( i = from; i < end; i++ )
    {       
        val[i-from] = cvtTable[ at(i) ];
    }
    return val;
}

float * DsoWfm::toValue( int from, int length )
{
    int dataSize;
    if ( length == -1 )
    {
        dataSize = size() - from;
    }
    else if( length == 0)
    {
        return NULL;
    }
    else
    {
        dataSize = length;
    }

    if ( m_capValue < dataSize )
    {
        delete []m_pValue;
        m_pValue = R_NEW( float[ dataSize ] );
        m_capValue = dataSize;
    }
    else
    {}

    return toValue( m_pValue, from, length );
}

DsoPoint * DsoWfm::toPoint( DsoPoint points[],
                            int from, int length )
{
    int i;
    int end;
    float yInc;

    //! guess the end
    if ( length == -1 )
    {
        end = m_size + m_start;
    }
    else
    {
        end = from + length;

        if ( end > m_size + m_start )
        {
            end = m_size + m_start;
        }
    }

    //! yinc
    yInc = realyInc();

    for ( i = from; i < end; i++ )
    {
        int v = m_pValue[i] / yInc + m_yGnd;
        if(v > 255)
        {
            v = 255;
        }
        else if( v < 0 )
        {
            v = 0;
        }
        points[i] = v;    
    }

    return points;
}
DsoPoint * DsoWfm::toPoint( int from, int length )
{
    int dataSize;
    if ( length == -1 )
    {
        dataSize = size() + from;
    }
    else
    {
        dataSize = length + from;
    }

    if ( m_capPoint < dataSize )
    {
        delete []m_pPoint;
        m_pPoint = R_NEW( DsoPoint[ dataSize ] );
        Q_ASSERT( NULL != m_pPoint );
        m_capPoint = dataSize;
    }
    else
    {}
    return toPoint( m_pPoint, from, length );
}

DsoPoint * DsoWfm::toLogic( float lev,
                            float sens,
                            int from,
                            int length)
{
    //! memory
    int dataSize;
    if ( length == -1 )
    {
        dataSize = size() + from;
    }
    else
    {
        dataSize = length + from;
    }

    if ( m_capPoint < dataSize )
    {
        delete []m_pPoint;
        m_pPoint = R_NEW( DsoPoint[ dataSize ] );
        Q_ASSERT( NULL != m_pPoint );
        m_capPoint = dataSize;
    }
    else
    {}

    int i, end;
    //! guess the end
    if ( length == -1 )
    {
        end = m_size + m_start;
    }
    else
    {
        end = from + length;

        if ( end > m_size + m_start )
        {
            end = m_size + m_start;
        }
    }

    //! analog to logic
    float h, l;
    int pre, dither;

    h = lev + sens / 2;
    l = lev - sens / 2;

    pre = -1;
    dither = 0;
    for ( i = from; i < end; i++ )
    {
        if ( m_pValue[i] >= h )
        {
            m_pPoint[i] = 1;
            pre = 1;
        }
        else if ( m_pValue[i] <= l )
        {
            m_pPoint[i] = 0;
            pre = 0;
        }
        else
        {
            if ( pre != -1 )
            {
                m_pPoint[i] = pre;
            }
            else
            {
                dither++;
            }
        }
    }

    //! set dither
    if ( pre == -1 )
    {
        pre = 0;
    }
    else
    {
        pre = m_pPoint[ from + dither ];
    }
    for ( i = from; i < dither + from; i++ )
    {
        m_pPoint[i] = pre;
    }

    return m_pPoint;
}

DsoPoint * DsoWfm::toLogic( int lev,
                            int sens,
                            int from,
                            int length)
{
    int i, end;
    //! guess the end
    if ( length == -1 )
    {
        end = m_size + m_start;
    }
    else
    {
        end = from + length;

        if ( end > m_size + m_start )
        {
            end = m_size + m_start;
        }
    }

    //! analog to logic
    int h, l;
    int pre, dither;

    h = lev + sens / 2;
    l = lev - ( sens - sens / 2 );

    pre = -1;
    dither = 0;
    for ( i = from; i < end; i++ )
    {
        if ( m_pPoint[i] >= h )
        {
            m_pPoint[i] = 1;
            pre = 1;
        }
        else if ( m_pPoint[i] <= l )
        {
            m_pPoint[i] = 0;
            pre = 0;
        }
        else
        {
            if ( pre != -1 )
            {
                m_pPoint[i] = pre;
            }
            else
            {
                dither++;
            }
        }
    }

    //! set dither
    if ( pre == -1 ) pre = 0;
    for( i = from; dither > 0 ; dither-- )
    {
        m_pPoint[i] = m_pPoint[ i+dither ];
    }

    return m_pPoint;
}

void DsoWfm::expandLa()
{
//    if( m_size*8 == m_length)
//    {
    DsoPoint* pPoint;
    if(size() == 0) { return;}

    pPoint = new DsoPoint[ size() * 8 ];
    for(int i = 0; i < size();i++)
    {
        for(int j = 0; j < 8;j++)
        {
            pPoint[i*8 + j] = (at(i) << j) && 0x80;
        }
    }
    LOG_DBG() << m_size << m_start;
    int tempStart = m_start;
    setPoint(pPoint, m_length, m_length, 0);
    m_start = tempStart;
    delete[] pPoint;
//    }
//    else
//    {
//        qDebug() <<"wfm Assert:" << m_size << m_length;
//    }
}

void DsoWfm::dbgShow()
{
    LOG_DBG()<<"y:inc/gnd/invert"<<realyInc()<<m_yGnd<<m_yInvert;
    LOG_DBG()<<"ref:scale/offset/div/a/e"<<m_yRefScale<<m_yRefOffset<<m_yRefDiv<<m_yRefBase.mA<<m_yRefBase.mE;
//    LOG_DBG()<<"x:t0/tinc/tbase"<<m_llt0<<m_lltInc<<m_tRefBase.toFloat();
//    LOG_DBG()<<"start/size"<<m_start<<m_size<<m_length;
}

void DsoWfm::save( const QString &fileName )
{

    QFile file(fileName);
    LOG_DBG()<<fileName<<m_start<<m_length<<m_size;
    if ( !file.open( QIODevice::WriteOnly) )
    {
        LOG_DBG()<<"save fail!";
        return;
    }

    if ( m_size < 1 )
    {
        LOG_DBG()<<"no data!";
        file.close();
        return;
    }

    //! write data
    if ( file.write( (const char*)m_pPoint + m_start, m_size ) != m_size )
    {
        LOG_DBG()<<"write fail!";
    }
    else
    {
        qDebug() << "write success!";
    }

    file.close();
}

void DsoWfm::saveValue( const QString &fileName )
{
    QFile file(fileName + "value");
    LOG_DBG();

    if ( !file.open( QIODevice::WriteOnly) )
    {
        LOG_DBG()<<"save fail!";
        return;
    }

    if ( m_size < 1 )
    {
        LOG_DBG()<<"no data!";
        file.close();
        return;
    }

    QTextStream out(&file);
    for(int i = m_start; i < m_size;i ++)
    {
        out<<m_pValue[i]<<'\n';
    }

    file.close();
}

void DsoWfmProc::voffset( DsoWfm *pWfm, float offset )
{
    int i;
    Q_ASSERT( NULL != pWfm );
    for ( i = pWfm->m_start; i < pWfm->size(); i++ )
    {
        pWfm->m_pValue[i] += offset;
    }
}

void DsoWfmProc::vscale( DsoWfm *pWfm, float scale )
{
    int i;

    Q_ASSERT( NULL != pWfm );
    for ( i = pWfm->m_start; i < pWfm->size(); i++ )
    {
        pWfm->m_pValue[i] /= scale;
    }
}

