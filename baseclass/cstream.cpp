
#include "cstream.h"
#include "../include/dsotype.h"
#include "../include/dsodbg.h"
/*!
 * \brief CStream::CStream
 * \param pStream
 * \param cap
 */
CStream::CStream(void *pStream, int cap)
{
    p_mBase = pStream;
    mCap = cap;

    mLen = 0;
    mOff = 0;
}
/*!
 * \brief CStream::getLength
 * \return
 */
int CStream::getLength()
{
    return mLen;
}
/*!
 * \brief CStream::setLength
 * \param len
 * \return
 */
int CStream::setLength( int len )
{
    if ( len > mCap )
    {
        return ERR_OVER_RANGE;
    }

    mLen = len;

    return ERR_NONE;
}

/*!
 * \brief CStream::setSeek
 * \param off
 * \return
 */
int CStream::setSeek( int off )
{
    if ( off < 0 || off >= mLen )
    {
        return ERR_OVER_RANGE;
    }

    mOff = off;

    return ERR_NONE;
}

void* CStream::getBase()
{
    return p_mBase;
}

/*!
 * \brief CStream::out
 * \param ptr
 * \param len
 * \return
 * 数据挂载到流上
 */
int CStream::out( const void *ptr, int len )
{
    if ( len > mCap-mLen )
    {
        char * p = (char*)ptr;
        qDebug() << "cstream error:" << p << len << mLen << mCap-mLen;
        Q_ASSERT( false );
        return ERR_OVER_ACCESS;
    }

    memcpy( (unsigned char*)p_mBase + mOff, ptr, len );
    mLen += len;
    mOff += len;

    return ERR_NONE;
}
/*!
 * \brief CStream::in
 * \param ptr
 * \param len
 * \return
 * 从流上卸载数据
 */
int CStream::in( void *ptr, int len )
{
    if ( len > mLen-mOff )
    {
        Q_ASSERT( false );
        return ERR_OVER_ACCESS;
    }

    memcpy( ptr, (unsigned char*)p_mBase + mOff, len );
    mOff += len;

    return ERR_NONE;
}
/*!
 * \brief CStream::write
 * \param name
 * \param dat
 * \return
 */
int CStream::write( const QString &/*name*/, const bool &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this << dat;
    return 0;
}
int CStream::write( const QString &/*name*/, const char &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this << dat;
    return 0;
}
int CStream::write( const QString &/*name*/, const short &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this << dat;
    return 0;
}
int CStream::write( const QString &/*name*/, const int &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this << dat;
    return 0;
}

int CStream::write( const QString &/*name*/, const unsigned char &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this << dat;
    return 0;
}
int CStream::write( const QString &/*name*/, const unsigned short &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this << dat;
    return 0;
}
int CStream::write( const QString &/*name*/, const unsigned int &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this << dat;
    return 0;
}

int CStream::write( const QString &/*name*/, const qlonglong &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this << dat;
    return 0;
}

int CStream::write( const QString &/*name*/, const qulonglong &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this << dat;
    return 0;
}

int CStream::write( const QString &/*name*/, const QString &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this << dat;
    return 0;
}
/*!
 * \brief CStream::write
 * \param name
 * \param pBuf
 * \param length
 * \param bPrivate
 * \param key
 * \return
 * 二进制数据串存储格式：
 * -#[length]
 * -length:4 bytes
 */
int CStream::write( const QString &/*name*/, void *pBuf, int length, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this << '#';
    *this << length;
    out( pBuf, length );
    return 0;
}

/*!
 * \brief CStream::read
 * \param name
 * \param dat
 * \return
 */
int CStream::read( const QString &/*name*/,  bool &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this>>dat;
    return 0;
}
int CStream::read( const QString &/*name*/,  char &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this>>dat;
    return 0;
}
int CStream::read( const QString &/*name*/,  short &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this>>dat;
    return 0;
}
int CStream::read( const QString &/*name*/,  int &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this>>dat;
    return 0;
}

int CStream::read( const QString &/*name*/,  qlonglong &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this>>dat;
    return 0;
}

int CStream::read( const QString &/*name*/,  unsigned char &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this>>dat;
    return 0;
}
int CStream::read( const QString &/*name*/,  unsigned short &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this>>dat;
    return 0;
}
int CStream::read( const QString &/*name*/,  unsigned int &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this>>dat;
    return 0;
}

int CStream::read( const QString &/*name*/,  qulonglong &dat, bool /*bPrivate*/,  cryptKey /*key*/)
{
    *this>>dat;
    return 0;
}

int CStream::read( const QString &/*name*/, QString &dat, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    *this>>dat;
    return 0;
}
int CStream::read( const QString &/*name*/, void *pBuf, int cap, bool /*bPrivate*/,  cryptKey /*key*/ )
{
    char sig;
    int length;

    *this>>sig;
    *this>>length;

    //! invalid input
    if ( sig != '#' || length > cap )
    {
        return -1;
    }

    //! get data
    in( pBuf, length );

    return 0;
}

int CStream::write( const QString &/*name*/, bool */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/, cryptKey /*key*/)
{ return 0; }
int CStream::write( const QString &/*name*/, char */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */)
{ return 0; }
int CStream::write( const QString &/*name*/, short */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*key*/)
{ return 0; }
int CStream::write( const QString &/*name*/, int */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*key*/)
{ return 0; }
int CStream::write( const QString &/*name*/, qlonglong */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*key*/ )
{ return 0; }

int CStream::write( const QString &/*name*/, unsigned char */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }
int CStream::write( const QString &/*name*/, unsigned short */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }
int CStream::write( const QString &/*name*/, unsigned int */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }
int CStream::write( const QString &/*name*/, qulonglong */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }

//! read
int CStream::read( const QString &/*name*/, bool */*pdat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/, cryptKey /*bits */)
{ return 0; }
int CStream::read( const QString &/*name*/, char */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */)
{ return 0; }
int CStream::read( const QString &/*name*/, short */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */)
{ return 0; }
int CStream::read( const QString &/*name*/, int */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */)
{ return 0; }
int CStream::read( const QString &/*name*/, qlonglong */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }

int CStream::read( const QString &/*name*/, unsigned char */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }
int CStream::read( const QString &/*name*/, unsigned short */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }
int CStream::read( const QString &/*name*/, unsigned int */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }
int CStream::read( const QString &/*name*/, qulonglong */*dat*/,
                   int /*bits */, int /*size*/,
                   bool /*bPrivate*/,  cryptKey /*bits */ )
{ return 0; }

/*!
 * \brief CStream::operator <<
 * \param dat
 * \return
 */
CStream & CStream::operator<<( const bool &dat )
{
    out( &dat, sizeof(dat));
    return *this;
}

CStream & CStream::operator<<( const char &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator <<
 * \param dat
 * \return
 */
CStream & CStream::operator<<( const short &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator <<
 * \param dat
 * \return
 */
CStream & CStream::operator<<( const int &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator <<
 * \param dat
 * \return
 */
CStream & CStream::operator<<( const unsigned char &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator <<
 * \param dat
 * \return
 */
CStream & CStream::operator<<( const unsigned short &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator <<
 * \param dat
 * \return
 */
CStream & CStream::operator<<( const unsigned int &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}

CStream & CStream::operator<<( const qlonglong &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}
CStream & CStream::operator<<( const qulonglong &dat )
{
    out( &dat, sizeof(dat) );
    return *this;
}
CStream & CStream::operator<<( const QString &str )
{
    *this<<'$';
    *this<<str.length();
    out( str.data(), str.length()*sizeof(QChar) );
    return *this;
}
/*!
 * \brief CStream::operator >>
 * \param dat
 * \return
 */

CStream & CStream::operator>>( bool &dat )
{
    in( &dat, sizeof(dat));
    return *this;
}

CStream & CStream::operator>>( char &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator >>
 * \param dat
 * \return
 */
CStream & CStream::operator>>( short &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator >>
 * \param dat
 * \return
 */
CStream & CStream::operator>>( int &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator >>
 * \param dat
 * \return
 */
CStream & CStream::operator>>( unsigned char &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator >>
 * \param dat
 * \return
 */
CStream & CStream::operator>>( unsigned short &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
/*!
 * \brief CStream::operator >>
 * \param dat
 * \return
 */
CStream & CStream::operator>>( unsigned int &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
CStream & CStream::operator>>( qlonglong &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
CStream & CStream::operator>>( qulonglong &dat )
{
    in( &dat, sizeof(dat) );
    return *this;
}
CStream & CStream::operator>>( QString &dat )
{
    int len;
    char sig;

    *this>>sig;

    if ( sig != '$' )
    {
        qWarning()<<"invalid string";
        return *this;
    }

    *this>>len;
    QChar *pBuf = R_NEW( QChar[ len ] );
    Q_ASSERT( NULL != pBuf );

    in( pBuf, len * sizeof(QChar) );
    dat.clear();
    dat.append( pBuf, len );

    delete []pBuf;

    return *this;
}
