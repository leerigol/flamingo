#include "ckeyrequest.h"

CKeyRequest::CKeyRequest()
{
    mMsg = 0;
    m_pContext = NULL;

    m_ikeyProc = NULL;
    m_llkeyProc = NULL;
}

void CKeyRequest::set( int msg,
          void *pContext,
          pFunc_ikeyproc proc )
{
    mMsg = msg;
    m_pContext = pContext;
    m_ikeyProc = proc;
}

void CKeyRequest::set( int msg,
          void *pContext,
          pFunc_llkeyproc proc )
{
    mMsg = msg;
    m_pContext = pContext;
    m_llkeyProc = proc;
}


