#ifndef DSOVERT_H
#define DSOVERT_H

#include "../include/dsotype.h"

#include "dsowfm.h"
#include "dsoreal.h"

using namespace DsoType;

/*!
 * \brief The dsoVert class
 * 封装垂直通道数据源
 * -数据源有数据和描述数据的属性
 * -数据有多种
 *  -像素数据：严格对应屏幕宽度，高度和adc相同
 *  -轨迹数据：不小于像素数据，不多于内存数据
 *  -内存数据:有效的内存数据
 */
class dsoVert
{
private:
    static QList< dsoVert* > _chList;
public:
    static dsoVert *getCH( Chan ch );
    static int getVertSize();
    static Chan getActiveChan();
    static QList<Chan> getChanList();
    static void scanCHOrder( Chan chPos[], int cnt );

    static bool setTop( Chan ch );
    static bool setDeTop( Chan ch );
public:
    dsoVert( Chan ch );

public:
    virtual void getPixel( DsoWfm &wfm,
                           Chan subChan= chan_none,
                           HorizontalView view = horizontal_view_main );  //! screen width
    virtual void getTrace( DsoWfm &wfm,
                           Chan subChan= chan_none,
                           HorizontalView view = horizontal_view_main );  //! trace
    virtual void getMemory( DsoWfm &wfm, Chan subChan= chan_none );  //! memory
    virtual void getDigi( DsoWfm &wfm,
                          Chan subChan= chan_none,
                          HorizontalView view = horizontal_view_main
                          );    //! packed bit stream
    virtual void getEye( DsoWfm &wfm, Chan subChan= chan_none );

public:
    bool  getOnOff();
    qlonglong getyGnd();
    int   getyAfeGnd();

    float getyInc();            //! no probe ratio
    Unit  getUnit();

    qlonglong   getYRefScale();
    qlonglong   getYRefOffset();
    int   getYRefDiv();
    DsoReal getYRefBase();
                                //! for digital dx
    virtual int getyPos( Chan subChan = chan_none );
    int getyMaxPos();
public:
    int   getyAfeScale();

public:
    virtual VertZone  getVertZone();
    chZone    getZone();
    Impedance getImpedance();
    QString   getCHLabel();
    DsoReal   getProbe();       //! 探头比在阈值设置中会用到
    Coupling  getCoupling();
    QColor    getColor();

public:
    int  voltToChPt( float val );
    int  voltToAdc( float val );

    float chPtToVolt( int chpt );
    float adcToVolt( int adc );

protected:
    bool isExist( Chan ch );

    void setyOnOff( bool b );
    void setyInc( float inc );
    void setyGnd(qlonglong gnd );

    void setyUnit( Unit unit );
    void setyLabel( const QString &str );
    void setyZone( chZone yZone );
    void setyImpedance( Impedance yImp );

    void setyProbe( DsoReal real );

    void setyRefScale(qlonglong scale );
    void setyRefOffset(qlonglong offset );

    void setyMaxPos( int yMax );

    //! true -- changed
    bool setTop();
    bool setDeTop();

public:
    void setServId( int id );
    int getServId();

    void setChan( Chan ch );
    Chan getChan();

protected:
    bool    yOnOff;
    float   yInc;bool yIncValid;
    qlonglong     yGnd;bool yGndValid;

    Unit    yUnit;
    QString yChlabel;
    chZone  yZone;
    Impedance yImp;
    DsoReal yProbe;

    Coupling yCoupling;
    QColor yColor;

    qlonglong     yRefScale;      //! y scale, uv unit
    qlonglong     yRefOffset;     //! y offset, uv unit
    int     yRefDiv;        //! dots in div
    DsoReal yRefBase;       //! base

    int     yMaxPos;
protected:
    Chan yChId;
    int  mServId;
};

#endif // DSOVERT_H
