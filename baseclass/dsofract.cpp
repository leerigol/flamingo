#include <bits/stdc++.h>
#include "dsofract.h"
#include "../arith/gcd.h"

bool isMultiOverflow(qlonglong x, qlonglong y)
{
    qlonglong m = x * y;
    return !x || m / x == y;
}

dsoFract::dsoFract()
{
    mM = 0;
    mN = 1;
}

dsoFract::dsoFract( qint64 m, qint64 n )
{
    set( m, n );
}

dsoFract::dsoFract( const dsoFract &fract )
{
    *this = fract;
}

dsoFract &dsoFract::operator=( const dsoFract &fract )
{
    set( fract.mM, fract.mN );

    return *this;
}

dsoFract dsoFract::operator+( const dsoFract &fract ) const
{
    qint64 m,n;
    dsoFract obj;

    if( isMultiOverflow(mM, fract.mN) &&
        isMultiOverflow(mN, fract.mM) &&
        isMultiOverflow(mN, fract.mN) )
    {
        m = mM * fract.mN + mN * fract.mM;
        n = mN * fract.mN;
    }
    else
    {
        qWarning()<<"warnning: qint64 over flow" << "mM" << fract.mM << "mN" << fract.mN;
        qWarning()<<__FUNCTION__<<__LINE__<<"replace qint64 with int_128";

    }
    obj.set( m, n );
    return obj;
}
dsoFract dsoFract::operator-( const dsoFract &fract ) const
{
    qint64 m,n;
    dsoFract obj;

    m = mM * fract.mN - mN * fract.mM;
    n = mN * fract.mN;

    obj.set( m, n );

    return obj;
}
dsoFract dsoFract::operator*( const dsoFract &fract ) const
{
    qint64 m,n;
    dsoFract obj;

    m = mM * fract.mM;
    n = mN * fract.mN;

    obj.set( m, n );

    return obj;
}
dsoFract dsoFract::operator/( const dsoFract &fract ) const
{
    qint64 m,n;
    dsoFract obj;

    m = mM * fract.mN;
    n = mN * fract.mM;

    obj.set( m, n );

    return obj;
}

dsoFract dsoFract::operator-() const
{
    dsoFract obj;

    obj.set( -mM, mN );

    return obj;
}

bool dsoFract::operator>( const dsoFract &fract ) const
{
    dsoFract delta;

    delta = *this - fract;

    return ( delta.mM > 0 );
}
bool dsoFract::operator<( const dsoFract &fract ) const
{
    dsoFract delta;

    delta = *this - fract;

    return ( delta.mM < 0 );
}

bool dsoFract::operator==( const dsoFract &fract ) const
{
    if ( *this>fract )
    { return false; }
    else if ( *this < fract )
    { return false; }
    else
    { return true; }
}
bool dsoFract::operator!=( const dsoFract &fract ) const
{
    return !(*this==fract);
}
bool dsoFract::operator>=( const dsoFract &fract ) const
{
    return (*this > fract) || (*this==fract);
}
bool dsoFract::operator<=( const dsoFract &fract ) const
{
    return (*this < fract) || (*this==fract);
}

void dsoFract::set( qint64 m, qint64 n )
{
    if ( n <= 0 )
    {
        Q_ASSERT( false );
    }
    else
    {}

    norm_m_n( m, n );

    mM = m;
    mN = n;

    if ( n < 0 )
    {
        mM = -mM;
        mN = -mN;
    }
}

qint64 dsoFract::M()
{ return mM; }
qint64 dsoFract::N()
{ return mN; }

double dsoFract::toDouble()
{
    Q_ASSERT( mN != 0 );

    return (double)mM/mN;
}
float dsoFract::toFloat()
{
    Q_ASSERT( mN != 0 );

    return (float)mM/mN;
}
int dsoFract::toInt()
{
    Q_ASSERT( mN != 0 );
    return (int)(mM/mN);
}
qlonglong dsoFract::toLonglong()
{
    Q_ASSERT( mN != 0 );
    return (qlonglong)(mM/mN);
}
int dsoFract::ceilInt()
{
    Q_ASSERT( mN != 0 );
    return (int)((mM+mN-1)/mN);
}
qlonglong dsoFract::ceilLonglong()
{
    Q_ASSERT( mN != 0 );
    return (qlonglong)((mM+mN-1)/mN);
}

quint32 dsoFract::ceilUInt()
{
    Q_ASSERT( mN != 0 );
    return (quint32)((mM+mN-1)/mN);
}
qulonglong dsoFract::ceilULonglong()
{
    Q_ASSERT( mN != 0 );
    return (qulonglong)((mM+mN-1)/mN);
}

//! outer operator
dsoFract operator+( const int v, const dsoFract &fract )
{ return fract + v; }
dsoFract operator-( const int v, const dsoFract &fract )
{ return dsoFract(v)-fract; }
dsoFract operator*( const int v, const dsoFract &fract )
{ return fract * v; }
dsoFract operator/( const int v,const dsoFract &fract )
{ return dsoFract(v)/fract; }

dsoFract operator+( const qlonglong v, const dsoFract &fract )
{ return fract + v; }
dsoFract operator-( const qlonglong v, const dsoFract &fract )
{ return dsoFract(v)-fract; }
dsoFract operator*( const qlonglong v, const dsoFract &fract )
{ return fract * v; }
dsoFract operator/( const qlonglong v,const dsoFract &fract )
{ return dsoFract(v)/fract; }
