#include "chelprequest.h"

CHelpRequest::CHelpRequest()
{
    mMsg = 0;

    mbOptValid = false;
}
CHelpRequest::CHelpRequest( const QString &str,
                            int msg )
{
    mName = str;
    mMsg = msg;

    mbOptValid = false;
}

void CHelpRequest::setName( const QString &name )
{ mName = name; }
QString &CHelpRequest::getName()
{ return mName; }

void CHelpRequest::setMsg( int msg )
{ mMsg = msg; }
int CHelpRequest::getMsg()
{ return mMsg; }

void CHelpRequest::setOption( int opt )
{
    mOpt = opt;
    mbOptValid = true;
}
bool CHelpRequest::getOption( int &opt )
{
    if ( mbOptValid )
    {
        opt = mOpt;
        return true;
    }
    else
    {
        return false;
    }
}

