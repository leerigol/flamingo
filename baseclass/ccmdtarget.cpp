#include "ccmdtarget.h"

IMPLEMENT_BASE_CMD( CExecutor, CExecutor )
start_of_entry()
end_of_entry()

pprocRPC CExecutor::pRpcCall = NULL;
void CExecutor::setRpcCall( pprocRPC rpc )
{
    CExecutor::pRpcCall = rpc;
}

/*!
 * \brief CExecutor::onSet
 * \param pEntry
 * \param val
 * \return
 * \todo para type && count check
 */
int CExecutor::onSet( const struCmdEntry *pEntry,
                      CArgument &val )
{
    CArgument valOut;

    if ( NULL == pEntry || pEntry->doProc == NULL )
    {
        return ERR_NULL_PTR;
    }

    return onExec( pEntry, val, valOut );
}

/*!
 * \brief CExecutor::onGet
 * \param pEntry
 * \param [in] val
 * \param [out] valOut
 * \return
 * the return is attached to val
 */
int CExecutor::onGet( const struCmdEntry *pEntry,
                      CArgument &val,
                      CArgument &valOut )
{
    if ( NULL == pEntry || pEntry->doProc == NULL )
    {
        return ERR_NULL_PTR;
    }

    return onExec( pEntry, val, valOut );
}

int CExecutor::onExec( const struCmdEntry *pEntry,
                       CArgument &val,
                       CArgument &valOut )
{
    int err;
    if ( NULL == pEntry || pEntry->doProc == NULL )
    {
        return ERR_NULL_PTR;
    }

    vProc proc;
    proc.unProc = pEntry->doProc;
    switch( pEntry->typeProc )
    {
        case enum_no_type:
            return ERR_INVALID_CONFIG;

        case enum_int_do_bool:
            Q_ASSERT( val.size() > 0 );
            return ( (this->*(proc.int_proc_bool))( val[0].bVal ) );

        case enum_int_do_int:
            Q_ASSERT( val.size() > 0 );
            return( (this->*proc.int_proc_int)( val[0].iVal ) );

        case enum_int_do_longlong:
            Q_ASSERT( val.size() > 0 );
            return( (this->*proc.int_proc_longlong)( val[0].llVal ) );

        //! fract
        case enum_int_do_float:
            Q_ASSERT( val.size() > 0 );
            return( (this->*proc.int_proc_float)( val[0].fVal ) );

        case enum_int_do_double:
            Q_ASSERT( val.size() > 0 );
            return( (this->*proc.int_proc_double)( val[0].dVal ) );

        case enum_int_do_pointer:
            Q_ASSERT( val.size() > 0 );
            return( (this->*(proc.int_proc_pointer))( val[0].pVal ) );

        //! string
        case enum_int_do_string:
            Q_ASSERT( val.size() > 0 );
            return( (this->*(proc.int_proc_string))( val[0].strVal )  );
            break;

        //! arb
        case enum_int_do_arb:
            Q_ASSERT( val.size() > 0 );
            return( (this->*(proc.int_proc_arb))( val[0].arbVal ) );

        //! real
        case enum_int_do_real:
            Q_ASSERT( val.size() > 0 );
            return( (this->*(proc.int_proc_real))( val[0].realVal ) );

        //! void
        case enum_int_do_void:
            return( (this->*(proc.int_proc_void))() );

        case enum_void_do_int:
            Q_ASSERT( val.size() > 0 );
            (this->*(proc.void_proc_int))( val[0].iVal );
            return 0;

        //! void do void
        case enum_void_do_void:
            (this->*(proc.unProc))();
            return 0;

        //! more paras
        case enum_int_do_arg:
            return (this->*(proc.int_proc_arg))( val );

        case enum_void_do_arg:
            (this->*(proc.void_proc_arg))( val );
            return 0;

       case enum_int_do_obj:
            Q_ASSERT( val[0].pObj!=NULL );
            err =  (this->*(proc.int_proc_obj))( val[0].pObj );
            if ( val[0].pObj->getGc() )
            {
                delete val[0].pObj;
            }
            return err;

        case enum_void_do_obj:
            Q_ASSERT( val[0].pObj!=NULL );
            (this->*(proc.void_proc_obj))( val[0].pObj );
            if ( val[0].pObj->getGc() )
            {
                delete val[0].pObj;
            }
            return 0;

        //! 2 paras
        case enum_int_do_int_int:
            Q_ASSERT( val.size() >=2 );
            return(  (this->*proc.int_proc_int_int)( val[0].iVal, val[1].iVal ) );
            break;

        //! no para
        //! get
        case enum_bool_q:
            valOut.setVal( (this->*proc.bool_qproc)() );
            break;

        case enum_int_q:
            valOut.setVal( (this->*proc.int_qproc)() );
            break;

        case enum_longlong_q:
            valOut.setVal( (this->*proc.longlong_qproc)() );
            break;

        case enum_float_q:
            valOut.setVal( (this->*proc.float_qproc)() );
            break;

        case enum_double_q:
            valOut.setVal( (this->*proc.double_qproc)() );
            break;

        case enum_pointer_q:
            valOut.setVal( (this->*proc.pointer_qproc)() );
            break;

        case enum_string_q:
            valOut.setVal( (this->*proc.string_qproc)() );
            break;

        case enum_arb_q:
            valOut.setVal( (this->*proc.arb_qproc)() );
            break;

        case enum_dsoreal_q:
            valOut.setVal( (this->*proc.dsoreal_qproc)() );
            break;

        //! get by int
        case enum_bool_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.bool_qproc_int)( val[0].iVal ) );
            break;
        case enum_int_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.int_qproc_int)( val[0].iVal ) );
            break;

        case enum_longlong_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.longlong_qproc_int)( val[0].iVal ) );
            break;

        case enum_float_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.float_qproc_int)( val[0].iVal ) );
            break;

        case enum_double_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.double_qproc_int)( val[0].iVal ) );
            break;

        case enum_dsoreal_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.dsoreal_qproc_int)( val[0].iVal ) );
            break;

        case enum_string_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.string_qproc_int)( val[0].iVal ) );
            break;

        case enum_pointer_q_int:
            Q_ASSERT( val.size() > 0 );
            Q_ASSERT( val[0].vType == val_int );
            valOut.setVal( (this->*proc.pointer_qproc_int)( val[0].iVal ) );
            break;

        //! with ptr
        case enum_void_q_ptr:
            (this->*proc.void_qproc_ptr)( val[0].pVal );
            valOut.setVal( val[0].pVal );
            break;

        case enum_int_q_ptr:
            valOut.setVal( (this->*proc.int_qproc_ptr)( val[0].pVal ) );
            break;

        //! with para get
        case enum_bool_q_arg:
            valOut.setVal( (this->*proc.bool_qproc_arg)( val ) );
            break;

        case enum_int_q_arg:
            valOut.setVal( (this->*proc.int_qproc_arg)( val ) );
            break;

        case enum_longlong_q_arg:
            valOut.setVal( (this->*proc.longlong_qproc_arg)( val ) );
            break;

        case enum_dsoreal_q_arg:
            valOut.setVal( (this->*proc.dsoreal_qproc_arg)( val ) );
            break;

        case enum_float_q_arg:
            valOut.setVal( (this->*proc.float_qproc_arg)( val ) );
            break;

        case enum_double_q_arg:
            valOut.setVal( (this->*proc.double_qproc_arg)( val ) );
            break;

        case enum_pointer_q_arg:
            valOut.setVal( (this->*proc.pointer_qproc_arg)( val ) );
            break;

        case enum_string_q_arg:
            valOut.setVal( (this->*proc.string_qproc_arg)( val ) );
            break;

        case enum_void_q_argi_argo:
            ( (this->*proc.void_qproc_argi_argo)( val, valOut ) );
            break;

        case enum_void_q_argo:
            ( (this->*proc.void_qproc_argo)( valOut ) );
            break;

        case enum_arb_q_arg:
            valOut.setVal( (this->*proc.arb_qproc_arg)( val ) );
            break;

        default:
            return ERR_INVALID_CONFIG;
    }

    return ERR_NONE;
}

/*!
 * \brief CExecutor::onPostuser_set
 * \param pEntry
 * \param val
 * \return
 */
int CExecutor::onPostuser_set( const struCmdEntry */*pEntry*/, CArgument &/*val*/ )
{
    //! \todo invalid
//    CVal valOut;

//    if ( NULL == pEntry || pEntry->doProc == NULL )
//    {
//        return scpi_err_null_ptr;
//    }

//    if ( onGet( pEntry, val, valOut, pEntry->postUserProc ) != 0 )
//    {
//        return scpi_err_do;
//    }

//    //! return the set value
//    return valOut.iVal;
    return 0;
}

/*!
 * \brief CExecutor::getEntry
 * \param cmd
 * \param wr
 * \return
 * get the cmd proc entry
 */
const struCmdEntry *CExecutor::getEntry( int cmd, enumWR wr ) const
{
    //! get all map
    QList< struCmdEntry *> entryMap;
    getEntries( entryMap );

    //! search the map list
    const struCmdEntry *pCmdEntry;
    foreach( struCmdEntry *pMapEntry, entryMap )
    {
        pCmdEntry = pMapEntry;

        while( pCmdEntry != NULL && pCmdEntry->cmd != 0 )
        {
            if ( cmd == pCmdEntry->cmd )
            {
                //! certain w/r
                if ( wr != enum_any )
                {
                    if ( pCmdEntry->wr == wr )
                    {
                        return pCmdEntry;
                    }
                    //! find more
                    else
                    {

                    }
                }
                //! any direction
                else
                {
                    return pCmdEntry;
                }
            }

            pCmdEntry++;
        }
    }

    return NULL;
}

bool CExecutor::isEntryExist( int cmd, enumWR wr ) const
{
    //! get all map
    QList< struCmdEntry *> entryMap;
    getEntries( entryMap );

    //! search the map list
    const struCmdEntry *pCmdEntry;
    foreach( struCmdEntry *pMapEntry, entryMap )
    {
        //! for each entry in map
        pCmdEntry = pMapEntry;
        while( pCmdEntry != NULL && pCmdEntry->cmd != 0 )
        {
            if ( cmd == pCmdEntry->cmd )
            {
                if ( wr != enum_any )
                {
                    if ( pCmdEntry->wr == wr )
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }

            pCmdEntry++;
        }
    }

    return false;
}

int CExecutor::getEntries( int cmd,
                 const struCmdEntry* entries[],
                 int maxSize,
                 enumWR wr ) const
{
    int now = 0;
//    const struCmdEntry *pEntry;

    //! get all map
    QList< struCmdEntry *> entryMap;
    getEntries( entryMap );

    const struCmdEntry *pCmdEntry;
    foreach( struCmdEntry *pMapEntry, entryMap )
    {
        pCmdEntry = pMapEntry;
        while( pCmdEntry != NULL && pCmdEntry->cmd != 0 )
        {
            if ( cmd == pCmdEntry->cmd )
            {
                if ( wr != enum_any )
                {
                    if ( pCmdEntry->wr == wr )
                    {
                        Q_ASSERT( now < maxSize );
                        entries[ now++ ] = pCmdEntry;
                    }
                }
                else
                {
                    Q_ASSERT( now < maxSize );
                    entries[ now++ ] = pCmdEntry;
                    return now;
                }
            }

            pCmdEntry++;
        }

        if ( now > 0 )
        { return now; }
    }
    return now;
}

bool CExecutor::queryEntryExist( int cmd )
{
    return isEntryExist( cmd );
}

/*!
 * \brief CExecutor::onDoCmd
 * \param cmd
 * \param val
 * \param wr
 * \return
 * loop the command table
 * for the wr(write and read) the onGet will be called.
 * \toto the err code : find fail, set/get do fail
 */
int CExecutor::onDoCmd( int cmd,
                        CArgument &val,
                        enumWR wr,
                        CArgument &valOut )
{
    const struCmdEntry *pEntry;

    const struCmdEntry * entries[32];
    int cnt;

    //! get match entries
    cnt = getEntries( cmd, entries, array_count(entries), wr );

    if ( cnt < 1 )
    {
//        qDebug()<< __FUNCTION__ << __LINE__ << "msg = " << cmd ;
        return ERR_TARGET_MIS_MATCH;
    }

    int err, sumErr = ERR_NONE;
    for ( int i = 0; i < cnt; i++ )
    {
        pEntry = entries[i];

        //! set
        if ( pEntry->wr == enum_w )
        {
            err = onSet( pEntry, val );
            //! save err
            if ( err != ERR_NONE )
            {
                sumErr = err;
            }
        }
        //! only one get
        else if ( pEntry->wr == enum_r || pEntry->wr == enum_wr )
        {
            //! get attr
            if ( pEntry->attrGet != NULL )
            {
                vProc proc;
                proc.unProc = pEntry->attrGet;

                //! get ui attr
                ( (this->*(proc.void_proc_int))( e_ui_attr ) );
            }

            //! call get now
            return onGet( pEntry, val, valOut );
        }
        else
        {
            return ERR_TARGET_INVALID_CFG;
        }
    }

    return sumErr;
}

/*!
 * \brief CExecutor::onDoCmd
 * \param cmd
 * \param val
 * \param wr
 * \return
 * overload for CVal input
 */
int CExecutor::onDoCmd( int cmd,  CArgument &val, enumWR wr )
{
    int err;

    CArgument valOut;

    err = onDoCmd( cmd, val, wr, valOut );
    if ( err != 0 )
    {
        return err;
    }

    if ( wr == enum_r )
    {
        val = valOut;
    }

    return 0;
}

/*!
 * \brief CExecutor::onRpcCmd
 * \param cmd
 * \param val
 * \param wr
 * \param valOut
 * \return
 * for remote call
 */
int CExecutor::onRpcCmd( int cmd, CArgument &val, enumWR wr, CArgument &valOut )
{
    const struCmdEntry *pEntry;
    int err;

    pEntry = getEntry( cmd, wr );
    if ( NULL == pEntry )
    {
        //qDebug()<< __FUNCTION__ << __LINE__ << "msg = " << cmd ;

        return ERR_TARGET_MIS_MATCH;
    }

    if ( pEntry->wr == enum_w )
    {
        if ( NULL != CExecutor::pRpcCall )
        {
            err = CExecutor::pRpcCall( this, cmd, val );
        }
        else
        {
            err = onSet( pEntry, val );
        }

        //! \todo err to remote error queue

        return err;
    }
    else if ( pEntry->wr == enum_r || pEntry->wr == enum_wr )
    {
        err = onGet( pEntry, val, valOut );

        //! \todo err to queue

        return err;
    }
    else
    {
        return ERR_TARGET_INVALID_CFG;
    }
}
