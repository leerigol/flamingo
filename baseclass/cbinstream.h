#ifndef CBINSTREAM_H
#define CBINSTREAM_H

#include "cstream.h"

/*!
 * \brief The CBinStream class
 * binary stream
 */
class CBinStream : public CStream
{
public:
    CBinStream( void *pStream, int cap );
    CBinStream( int block );
    virtual ~CBinStream();
public:
    void attach( void *pStream, int size );

protected:
    int mBlockSize;
};

#endif // CBINSTREAM_H
