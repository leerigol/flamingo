#ifndef DSOWFM_H
#define DSOWFM_H

#include <QObject>  /*!< declare metatype */
#include <QByteArray>
#include <QMutex>

#include "../include/dsotype.h"
#include "../include/dsocfg.h"
#include "dsoattr.h"


using namespace DsoType;


class DsoWfmProc;


/*!
 * \brief The DsoWfm class
 *
 * \todo waveform point can be more than 8 bit
 */
class DsoWfm : public DsoAttr
{
protected:
    static const int _ySIZE= ADC_DOTs;    /*!< convert table size */
    static const int _yTOP = ADC_DOTs-1;
    static const int _yDOWN = 0;

public:
    int m_vLeft;                    /*!< view left */
    int m_vRight;                   /*!< view right */
                                    /*!< [ ) */
    int m_size;                     //! disk size
    int m_start;                    //! data from m_start with m_size

    QMutex m_wfmMutex;

private:
    DsoPoint *m_pPoint;             /*!< sample point */
    int m_capPoint;

    float *m_pValue;
    int m_capValue;
    quint32 mPhyAddr;               //! addr
    //! 标记当前wfm中float是否有效，trace线程优化用
    //! 默认为true，只有在取trace流程中被置为false
    bool m_bFloatValid;
protected:
    void buildConvertTable( float table[] );

public:
    DsoWfm();
    ~DsoWfm();

    DsoWfm( const DsoWfm &wfm );
    DsoWfm( DsoWfm &wfm );
    DsoWfm( DsoWfm *pWfm );

    DsoWfm &operator=(const DsoWfm &wfm);
    DsoWfm &operator=(DsoWfm &wfm);

private:
    void assign( const DsoWfm &wfm );
    void _init();

public:
    void setFloatValid(bool bValid );
    bool getFloatValid();

    void setViewRange( int left, int right );
    void setAddr( quint32 addr );
    quint32 getAddr( );

    //! 分配容量
    void init( int cap );

    void setPoint( DsoPoint *point,
                   int length,
                   int size,
                   int offset = 0);
    DsoPoint *getPoint() const;

    void   setValue( float *pVal,
                   int size,
                   int offset = 0 );
    float  *getValue() const;

    void setRange( int offset, int size);
    int  size();
    int  start();
    DsoPoint at(int i) const;

    DsoPoint operator[](int i) const;
    DsoPoint & operator[](int i);

    float valueAt( int i ) const;
    float toReal( int adcY );

public:
    float * toValue( float val[], int from=0, int length=-1 );
    float * toValue( int from=0, int length = -1 );

    DsoPoint * toPoint( DsoPoint points[], int from, int length = -1 );
    DsoPoint * toPoint( int from=0, int length = -1 );

    DsoPoint * toLogic( float lev,
                        float sens,
                        int from=0,
                        int length = -1 );
    DsoPoint * toLogic( int lev,
                        int sens,
                        int from=0,
                        int length = -1 );
public:
    void expandLa();
    void dbgShow();
    void save( const QString &fileName );
    void saveValue( const QString &fileName );

    friend class DsoWfmProc;
};

Q_DECLARE_METATYPE( DsoWfm )

class DsoWfmProc
{
public:
    static void voffset( DsoWfm *pWfm, float offset );
    static void vscale( DsoWfm *pWfm, float scale );

    static void hReSample( DsoWfm *pWfmIn,
                           int m, int n, int mCenter, int screenWidth,
                           DsoWfm *pWfmOut );

};


#endif // DSOWFM_H
