#ifndef ISERIAL_H
#define ISERIAL_H

#include <QString>
#include "../include/dsotype.h"

#include "./cstream.h"
#include "./cbinstream.h"

/*!\typedef serialVersion
 * \brief serialVersion
 *
 * only 0~255 allowed
 */
typedef unsigned char serialVersion;


/*!
 * \brief The ISerial class
 *
 * 序列化数据接口
 * -序列化指的是将应用状态存储和导入
 * -序列化数据有版本号，可以根据版本号判断数据兼容性
 * -rst对应状态复位(恢复出厂设置)
 * -init对应启动过程中的初始化，只会执行一次
 */
class ISerial
{
public:
    ISerial();
    virtual ~ISerial();
protected:
    serialVersion mVersion;     /*< 版本号 */
    bool mBinCompatible;        /*< 二进制是否兼容 */
    CBinStream *m_pBinStream;   //! bin stream
public:
    serialVersion getVersion(); //! version now
    bool getBinCompatible();    //! bin compatible
    virtual int serialOut( CStream &stream, serialVersion &ver );
    virtual int serialIn( CStream &stream, serialVersion ver );
    virtual void rst()=0;
    virtual int startup()=0;
    virtual void init();

    void save( int block=512 );
    void restore();
};



#endif // ISERIAL_H
