#ifndef CARGUMENT_H
#define CARGUMENT_H

#include "../include/dsotype.h"
#include "../baseclass/dsoreal.h"

using namespace DsoType;

/*! 数值类型
  */
typedef enum
{
    val_none,

    val_bool,
    val_int,
    val_longlong,

    val_float,
    val_double,
    val_string,

    val_ptr,    /*!< void* */

    val_arb_bin,
    val_obj,

    val_enum,
    val_real,  /*! 定点类型*/

}ValType;


class CObj
{
public:
    CObj();
    virtual ~CObj(){}

public:
    bool getGc();
    void accept();
protected:
    void ignore();

protected:
    bool mbGc;  /*!< 启动垃圾搜集*/
};

/*!
 * \brief The CVal struct
 * -值，表示接受的参数
 * -一个参数
 */
struct CVal
{
    CVal();
    CVal( const CVal &val );

    ValType vType;

    union
    {
        bool bVal;
        int iVal;
        unsigned int uVal;
        qlonglong llVal;

        float fVal;
        double dVal;

        pointer pVal;

        int eVal;
    };

    QString strVal;
    ArbBin  arbVal;
    DsoReal dsorVal;

    CObj *pObj;
    DsoReal realVal;
private:
    DsoErr checkValType( ValType refType );

public:
    void setVal( bool val );
    void setVal( int val );
    void setVal( qlonglong val );

    void setVal( float val );
    void setVal( double val );
    void setVal( pointer val );

    void setVal( QString &str );
    void setVal( ArbBin &val );
    void setVal( CObj *obj );

    void setVal( const DsoReal &real );

    DsoErr getVal( bool &val );
    DsoErr getVal( int &val );
    DsoErr getVal( qlonglong &val );

    DsoErr getVal( float &val );
    DsoErr getVal( double &val );
    DsoErr getVal( pointer &val );

    DsoErr getVal( QString &val );
    DsoErr getVal( ArbBin &val );   
    DsoErr getVal( CObj * &obj );

    DsoErr getVal(DsoReal &val);

    void enter();
    void exit();


};

bool operator==( const CVal &a, const CVal &b );

#define assert_val_type( vType )    if ( checkValType(vType) != ERR_NONE )\
                                    { return ERR_TYPE_MIS_MATCH; }
#define GET_x_VAL( vType, fieldVal )   assert_val_type( vType );\
                                        val = fieldVal;\
                                        return ERR_NONE;
/*!
 * \brief The CArgument class
 * -参数集合
 * -多个参数
 */
class CArgument
{
public:
    CArgument();
    ~CArgument();
    CArgument( const CArgument &val );
    CArgument & operator=( const CArgument &val );
public:
    void append( const CArgument &val );
    void append( CVal &val );

    void append( bool val );
    void append( int val  );
    void append( qlonglong val );
    void append( const DsoReal &val );

    void append( float val );
    void append( double val);
    void append( pointer val );

    void append( QString str);
    void append( ArbBin val );
    void append( CObj *val);

    void clear();

    void setVal( bool val, int index=0 );
    void setVal( int val, int index=0 );
    void setVal( qlonglong val, int index=0 );
    void setVal( DsoReal val, int index = 0 );

    void setVal( float val, int index=0 );
    void setVal( double val, int index=0 );
    void setVal( pointer val, int index=0 );

    void setVal( QString str, int index=0 );
    void setVal( ArbBin val, int index=0 );
    void setVal( CObj *val, int index = 0 );

    DsoErr getVal( bool &val, int index=0  );
    DsoErr getVal( int &val, int index=0  );
    DsoErr getVal( qlonglong &val, int index=0  );

    DsoErr getVal( float &val, int index=0  );
    DsoErr getVal( double &val, int index=0  );
    DsoErr getVal( pointer &val, int index=0  );

    DsoErr getVal( QString &str, int index=0  );
    DsoErr getVal( ArbBin &val, int index=0  );
    DsoErr getVal( CObj * &val, int index = 0 );

    DsoErr getVal( DsoReal &val, int index = 0 );

    DsoErr getVal( CVal &val, int index = 0 ) const;
public:
    CVal& operator[]( int index );

    //! only value different
    bool isLike( const CArgument &args );

    bool checkType( ValType t1,
                    ValType t2=val_none,
                    ValType t3=val_none,
                    ValType t4=val_none ) const;

    bool operator==( const CArgument &args );
    bool operator!=( const CArgument &args );

    int size() const;

protected:
    void prepareIndex( int index );
    bool checkIndex( int index ) const;

private:
    QList<CVal> mArgs;
};

//bool operator==( const CArgument &args,
//                 const CArgument &argsB );

#define struServPara CArgument

typedef CVal  GCVal;


#endif // CARGUMENT_H
