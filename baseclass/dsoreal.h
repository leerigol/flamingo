#ifndef DSOREAL_H
#define DSOREAL_H

#include <QtCore>
#include <QString>

#include "../include/dsotype.h"

using namespace DsoType;

float realf( int a, DsoE e );
double reald( int a, DsoE e );

float realf( long long a, DsoE e );
double reald( long long a, DsoE e );

qlonglong normf( float a, DsoE e );
qlonglong normd( double a, DsoE e );

#define DsoRealType_A   qlonglong

struct dsoReal
{
    DsoRealType_A mA;
    DsoE mE;
};

/*!
 * \brief DsoReal 以定点形式表示内部的数值
 *
 * 将数值分为两个部分：数+幂
 */
class DsoReal : public dsoReal
{
public:
    DsoReal( DsoE e=DsoType::E_0 );
    DsoReal( DsoRealType_A a, DsoE e=DsoType::E_0  );
    DsoReal( const DsoReal &dsoReal );
    DsoReal( const QString& str );
    DsoReal( float val );

    DsoReal& operator=( const DsoReal &dsoReal );
    DsoReal& operator=( DsoRealType_A val );

    DsoReal( const dsoReal &real );
    DsoReal &operator=( const dsoReal &real );

public:
    bool isUnk() const;

public:
    DsoErr setVal( DsoRealType_A a, DsoE e=DsoType::E_0 );
    DsoErr setVal( QString str );
    DsoErr setVal( double fVal );
    void getVal( DsoRealType_A &a, DsoE &e ) const;

    DsoRealType_A getInteger() const;

    DsoErr toReal( float &out, float def=0.0f );
    DsoErr toReal( double &out, double def=0.0 );
    DsoErr toReal( int &out, int def=0 );
    DsoErr toReal( qlonglong &out, qlonglong def=0ll );

    float toFloat();
    double toDouble();

    DsoErr alignBase( DsoReal &base );
    DsoErr alignBase( DsoE e );

    QString toString();

    void addE( DsoE e );

public:
    bool operator<(  DsoReal real );
    bool operator<=(  DsoReal real );
    bool operator>(  DsoReal real );
    bool operator>=(  DsoReal real );
    bool operator!=(  DsoReal real );
    bool operator==(  DsoReal &real );

    DsoReal operator+(  DsoReal real );
    DsoReal operator-(  DsoReal real );
    DsoReal operator*(  DsoReal real );
    DsoReal operator/(  DsoReal real );

public:
    void normalize();
private:
    DsoErr tryConvert( QString str );

public:
    static bool lt(  DsoReal &a,  DsoReal &b );
    static bool mt(  DsoReal &a,  DsoReal &b );
    static DsoErr fixedAb( DsoReal &a, DsoReal &b );
    static DsoErr doLimit( DsoReal &valIn, DsoReal &valOut, const DsoReal &min,  const DsoReal &max );

private:
    static int rank10( DsoRealType_A a, DsoRealType_A b, int &remi );
    static DsoErr mErr;
    static void setLastErr( DsoErr err );
    static DsoErr getLastErr();

private:
    bool mbUnk;
};

class DsoRealGp
{
public:
    DsoReal vNow;
    DsoReal vMax;
    DsoReal vMin;
    DsoReal vDef;

public:
    DsoRealGp();

public:
    void setValue( const DsoReal& now,
                   const DsoReal& max,
                   const DsoReal& min,
                   const DsoReal& def );
    void setValue( DsoRealType_A vnow,
                   DsoRealType_A vmax,
                   DsoRealType_A vmin,
                   DsoRealType_A vdef,
                   DsoE vE );

    void mul( const DsoReal &scale );
};

#define DsoReal_0 DsoReal(0,E_0)

DsoReal operator-( const DsoReal &real );
bool operator==( const DsoReal &a, const DsoReal &b );

/*!
 * \brief The COverflow class
 * overflow detect
 */
class COverflow
{

public:
static bool overflow_add( int a, int b);
static bool overflow_mul( int a, int b );

};

#endif // DSOREAL_H
