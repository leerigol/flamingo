#ifndef CHELPREQUEST_H
#define CHELPREQUEST_H

#include <QtCore>
#include "./cargument.h"

/*!
 * \brief The CHelpRequest class
 * 系统帮助事件
 */
class CHelpRequest : public CObj
{
public:
    CHelpRequest();
    CHelpRequest( const QString &str, int msg );

public:
    void setName( const QString &name );
    QString &getName();

    void setMsg( int msg );
    int getMsg();

    void setOption( int opt );
    bool getOption( int &opt );

private:
    QString mName;
    int mMsg;
    int mOpt;
    bool mbOptValid;            //! 选项数值是否有效
};

#endif // CHELPREQUEST_H
