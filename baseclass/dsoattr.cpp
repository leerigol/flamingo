#include "dsoattr.h"

DsoAttr::DsoAttr()
{
    m_llt0 = 0;
    m_lltInc = 1;
    m_tRefBase.setVal( 1, E_N12 );
    m_length = 0;

    m_yInc.set( 100, 25 );
    m_yDiv.set( 25, 1 );

    m_yGnd = 128;
    m_yOrig = 128;
    m_yRng = 200;         //! top - bottom
    m_yInvert = false;

    m_yRefScale = 1000000;
    m_yRefOffset = 0;
    m_yRefDiv = 25;
    m_yRefBase.setVal( 1, E_N6 );

    m_AttrId = 0;
}

void DsoAttr::setLength( int len )
{
    m_length = len;
}
int DsoAttr::getLength()
{
    return m_length;
}

void DsoAttr::sett( qlonglong t0,
           qlonglong tinc,
           DsoReal real )
{
    m_llt0 = t0;
    m_lltInc   = tinc;
    m_tRefBase = real;
}

void DsoAttr::sett( dsoFract t0,
           dsoFract tinc,
           DsoReal real )
{
    m_llt0 = t0.ceilLonglong();
    m_lltInc   = tinc.ceilLonglong();
    m_tRefBase = real;
}

void DsoAttr::setllt0( qlonglong t0 )
{
    m_llt0 = t0;
}

qlonglong DsoAttr::getllt0()
{
    return m_llt0;
}

void DsoAttr::setlltInc( qlonglong tinc )
{
    m_lltInc = tinc;
}

qlonglong DsoAttr::getlltInc()
{
    return m_lltInc;
}

void DsoAttr::settBase( DsoReal base )
{
    m_tRefBase = base;
}

DsoReal DsoAttr::gettBase()
{
    return m_tRefBase;
}

DsoReal DsoAttr::gett0()
{
    return m_tRefBase * m_llt0;
}

DsoReal DsoAttr::gettInc()
{
    return m_tRefBase * m_lltInc;
}

void DsoAttr::setv( qlonglong gnd,
           dsoFract inc,
           bool inv )
{
    m_yInc = inc;
    m_yGnd = gnd;
    m_yInvert = inv;
}

void DsoAttr::setyInc( dsoFract inc )
{
    m_yInc = inc;
}
void DsoAttr::setyInc( dsoFract inc, DsoReal realBase )
{
    m_yInc = inc;
    m_yRefBase = realBase;
}
dsoFract DsoAttr::getyInc()
{
    return m_yInc;
}

float DsoAttr::realyInc()
{
    float rInc;

    m_yRefBase.toReal( rInc );
    rInc = rInc * m_yInc.M()/m_yInc.N();

    if ( m_yInvert ) rInc = -rInc;

    return rInc;
}

void DsoAttr::setyGnd( qlonglong ygnd, int yori )
{
    m_yGnd = ygnd;
    m_yOrig = yori;
}
qlonglong DsoAttr::getyGnd()
{
    return m_yGnd;
}
int DsoAttr::getyOrig()
{
    return m_yOrig;
}

void DsoAttr::setInvert( bool inv )
{
    m_yInvert = inv;
}
bool DsoAttr::getInvert()
{
    return m_yInvert;
}

void DsoAttr::setyDiv( dsoFract ydiv )
{
    m_yDiv = ydiv;
}
dsoFract DsoAttr::getyDiv()
{
    return m_yDiv;
}

void DsoAttr::setyRefScale( qlonglong yScale )
{ m_yRefScale = yScale; }
qlonglong DsoAttr::getyRefScale()
{ return m_yRefScale; }

void DsoAttr::setyRefOffset( qlonglong yOffset )
{ m_yRefOffset = yOffset; }
qlonglong DsoAttr::getyRefOffset()
{ return m_yRefOffset; }

void DsoAttr::setyRefBase( DsoReal realBase )
{ m_yRefBase = realBase; }
DsoReal DsoAttr::getyRefBase()
{ return m_yRefBase; }

void DsoAttr::setAttr( DsoAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );
    *this = *pAttr;
}
DsoAttr* DsoAttr::getAttr()
{
    return this;
}

void DsoAttr::settAttr( DsoAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );

    m_llt0 = pAttr->m_llt0;
    m_lltInc = pAttr->m_lltInc;
    m_tRefBase = pAttr->m_tRefBase;
    m_length = pAttr->m_length;
}

void DsoAttr::settAttr( qlonglong t0,
               qlonglong tinc,
               int saLength,
               DsoReal base )
{
    m_llt0 = t0;
    m_lltInc = tinc;
    m_tRefBase = base;
    m_length = saLength;
}

void DsoAttr::setvAttr( DsoAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );

    m_yInc = pAttr->m_yInc;
    m_yDiv = pAttr->m_yDiv;
    m_yGnd = pAttr->m_yGnd;
    m_yInvert = pAttr->m_yInvert;

    m_yRefScale = pAttr->m_yRefScale;
    m_yRefOffset = pAttr->m_yRefOffset;
    m_yRefDiv = pAttr->m_yRefDiv;
    m_yRefBase = pAttr->m_yRefBase;
}

void DsoAttr::setAttrId(int id)
{
    m_AttrId = id;
}

int DsoAttr::getAttrId()
{
    return m_AttrId;
}
