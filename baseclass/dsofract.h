#ifndef DSOFRACT_H
#define DSOFRACT_H

#include <QtCore>

class Int_128{

    unsigned long long a,b;
    Int_128(long long x)
    {a = 0, b = x;}

    friend bool operator < (Int_128 x,Int_128 y)
    {
        return x.a < y.a || (x.a == y.a && x.b < y.b);
    }

    friend Int_128 operator + (Int_128 x,Int_128 y)
    {
        Int_128 re(0);
        re.a=x.a+y.a+(x.b+y.b<x.b);
        re.b=x.b+y.b;
        return re;
    }

    friend Int_128 operator - (Int_128 x,Int_128 y)
    {
        y.a=~y.a;
        y.b=~y.b;
        return x+y+1;
    }

    void Div2()
    {
        b>>=1;
        b|=(a&1ll)<<63;
        a>>=1;
    }

    friend Int_128 operator * (Int_128 x,Int_128 y)
    {
        Int_128 re=0;
        while(y.a||y.b)
        {
            if(y.b&1)  re=re+x;
            x=x+x;
            y.Div2();
        }
        return re;
    }

    friend Int_128 operator % (Int_128 x,Int_128 y)
    {
        Int_128 temp=y;
        int cnt=0;
        while(temp<x)temp=temp+temp,++cnt;
        for(;cnt>=0;cnt--)
        {
            if(temp<x)x=x-temp;
            temp.Div2();
        }
        return x;
    }
};

class dsoFract
{
public:
    dsoFract();
    dsoFract( qint64 m, qint64 n = 1 );
    dsoFract( const dsoFract &fract );

public:
    dsoFract &operator=( const dsoFract &fract );

    dsoFract operator+( const dsoFract &fract ) const;
    dsoFract operator-( const dsoFract &fract ) const;
    dsoFract operator*( const dsoFract &fract ) const;
    dsoFract operator/( const dsoFract &fract ) const;
    dsoFract operator-() const;

    bool operator>( const dsoFract &fract ) const;
    bool operator<( const dsoFract &fract ) const;

    bool operator==( const dsoFract &fract ) const;
    bool operator!=( const dsoFract &fract ) const;
    bool operator>=( const dsoFract &fract ) const;
    bool operator<=( const dsoFract &fract ) const;

public:
    void set( qint64 m, qint64 n=1 );
    qint64 M();
    qint64 N();

    double toDouble();
    float toFloat();
    int   toInt();
    qlonglong toLonglong();

    int ceilInt();
    qlonglong ceilLonglong();

    quint32 ceilUInt();
    qulonglong ceilULonglong();
public:
    qint64 mM,mN;
};

dsoFract operator+( const int v, const dsoFract &fract ) ;
dsoFract operator-( const int v, const dsoFract &fract ) ;
dsoFract operator*( const int v, const dsoFract &fract ) ;
dsoFract operator/( const int v,const dsoFract &fract ) ;

dsoFract operator+( const qlonglong v, const dsoFract &fract ) ;
dsoFract operator-( const qlonglong v, const dsoFract &fract ) ;
dsoFract operator*( const qlonglong v, const dsoFract &fract ) ;
dsoFract operator/( const qlonglong v,const dsoFract &fract ) ;

#endif // DSOFRACT_H
