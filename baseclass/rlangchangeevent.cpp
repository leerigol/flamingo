#include "rlangchangeevent.h"

RLangchangeEvent::RLangchangeEvent( SystemLanguage lang )
                 : QEvent( QEvent::LanguageChange )
{
    mLanguage = lang;
}

