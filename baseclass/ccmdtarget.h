#ifndef CCMDTARGET_H
#define CCMDTARGET_H

#include <QtCore>
#include "../include/dsotype.h"

#include "cargument.h"

using namespace DsoType;

class CExecutor;

enum enumDoType
{
    enum_no_type,

    enum_int_do_bool,
    enum_int_do_int,
    enum_int_do_longlong,

    enum_int_do_float,
    enum_int_do_double,
    enum_int_do_pointer,

    enum_int_do_string,
    enum_int_do_arb,
    enum_int_do_real,

    enum_int_do_void,

    enum_void_do_void,
    enum_void_do_int,

    //! more args
    enum_int_do_arg,
    enum_void_do_arg,

    enum_int_do_obj,
    enum_void_do_obj,

    //! 2 para
    enum_int_do_int_int,

    //! no para
    enum_bool_q,

    enum_int_q,
    enum_longlong_q,

    enum_float_q,
    enum_double_q,
    enum_pointer_q,

    enum_string_q,
    enum_arb_q,
    enum_real_q,
    enum_dsoreal_q,

    //! int para
    enum_bool_q_int,
    enum_int_q_int,
    enum_longlong_q_int,

    enum_float_q_int,
    enum_double_q_int,
    enum_dsoreal_q_int,

    enum_string_q_int,
    enum_pointer_q_int,

    //! pointer para
    enum_void_q_ptr,
    enum_int_q_ptr,

    //! arg para
    enum_bool_q_arg,

    enum_int_q_arg,
    enum_longlong_q_arg,
    enum_dsoreal_q_arg,

    enum_float_q_arg,
    enum_double_q_arg,
    enum_pointer_q_arg,

    enum_string_q_arg,
    enum_void_q_argi_argo,
    enum_void_q_argo,
    enum_arb_q_arg,
};

enum enumWR
{
    enum_any, /*!< undefined */
    enum_w,   /*!< write */
    enum_r,   /*!< read */
    enum_wr,  /*!< write and read */
};

enum enumAttr
{
    e_ui_attr,
};

//! vritual
typedef void (CExecutor::*void_do_void)();

//! proc type
//! set
typedef int (CExecutor::*int_do_bool)( bool val );
typedef int (CExecutor::*int_do_int)( int val );
typedef int (CExecutor::*int_do_longlong)( qlonglong val );

typedef int (CExecutor::*int_do_float)( float val );
typedef int (CExecutor::*int_do_double)( double val );
typedef int (CExecutor::*int_do_pointer)( pointer val );
typedef int (CExecutor::*int_do_string)( QString& val );

typedef int (CExecutor::*int_do_arb)( ArbBin& val );
typedef int (CExecutor::*int_do_void)();
typedef void (CExecutor::*void_do_int)( int val );
typedef void (CExecutor::*void_do)();

typedef int (CExecutor::*int_do_real)( DsoReal &val );

//! more arguments
typedef int (CExecutor::*int_do_arg)( CArgument &arg );
typedef void (CExecutor::*void_do_arg)( CArgument &arg );

typedef int (CExecutor::*int_do_obj)( CObj *obj );
typedef void(CExecutor::*void_do_obj)( CObj *obj );

//! set 2 paras
typedef int (CExecutor::*int_do_int_int)( int val1, int val2 );

//! get
typedef bool (CExecutor::*bool_q)();
typedef int (CExecutor::*int_q)();
typedef qlonglong (CExecutor::*longlong_q)();

typedef float (CExecutor::*float_q)();
typedef double (CExecutor::*double_q)();
typedef pointer (CExecutor::*pointer_q)();
typedef QString (CExecutor::*string_q)();

typedef ArbBin (CExecutor::*arb_q)();
typedef DsoReal (CExecutor::*dsoreal_q)();

//! by int
typedef bool (CExecutor::*bool_q_int)( int );
typedef int (CExecutor::*int_q_int)( int );
typedef qlonglong (CExecutor::*longlong_q_int)( int );

typedef float (CExecutor::*float_q_int)( int );
typedef double (CExecutor::*double_q_int)( int );
typedef DsoReal (CExecutor::*dsoreal_q_int)( int );

typedef QString (CExecutor::*string_q_int)( int );
typedef pointer (CExecutor::*pointer_q_int)( int );

typedef void (CExecutor::*void_q_ptr)( pointer );
typedef int (CExecutor::*int_q_ptr)( pointer );

//! by arg
typedef bool (CExecutor::*bool_q_arg)( CArgument &arg );
typedef int (CExecutor::*int_q_arg)( CArgument &arg );
typedef qlonglong (CExecutor::*longlong_q_arg)( CArgument &arg );
typedef DsoReal (CExecutor::*dsoreal_q_arg)( CArgument &arg );

typedef float (CExecutor::*float_q_arg)( CArgument &arg );
typedef double (CExecutor::*double_q_arg)( CArgument &arg );
typedef pointer (CExecutor::*pointer_q_arg)( CArgument &arg );
typedef QString (CExecutor::*string_q_arg)( CArgument &arg );

typedef void (CExecutor::*void_q_argin_argout)( CArgument &arg,
                                                CArgument &argOut );
typedef void (CExecutor::*void_q_argout)( CArgument &argOut );

typedef ArbBin (CExecutor::*arb_q_arg)( CArgument &arg );

union vProc
{
    void_do_void unProc;        //! virtual

    //! set
    int_do_bool int_proc_bool;
    int_do_int int_proc_int;
    int_do_longlong int_proc_longlong;

    int_do_float int_proc_float;
    int_do_double int_proc_double;
    int_do_pointer int_proc_pointer;

    int_do_string int_proc_string;
    int_do_arb int_proc_arb;
    int_do_real int_proc_real;

    int_do_void int_proc_void;
    void_do_int void_proc_int;

    int_do_obj int_proc_obj;
    void_do_obj void_proc_obj;

    //! special
    void_do void_proc;

    int_do_arg int_proc_arg;
    void_do_arg void_proc_arg;

    //! set 2 paras
    int_do_int_int int_proc_int_int;

    //! get
    bool_q bool_qproc;
    int_q int_qproc;
    longlong_q longlong_qproc;

    float_q float_qproc;
    double_q double_qproc;
    pointer_q pointer_qproc;

    string_q string_qproc;
    arb_q arb_qproc;
    dsoreal_q dsoreal_qproc;

    //! get by int
    bool_q_int bool_qproc_int;
    int_q_int int_qproc_int;
    longlong_q_int longlong_qproc_int;

    float_q_int float_qproc_int;
    double_q_int double_qproc_int;
    dsoreal_q_int dsoreal_qproc_int;

    string_q_int string_qproc_int;
    pointer_q_int pointer_qproc_int;

    //! get by ptr
    void_q_ptr void_qproc_ptr;
    int_q_ptr int_qproc_ptr;

    //! get by arg
    bool_q_arg bool_qproc_arg;
    int_q_arg int_qproc_arg;
    longlong_q_arg longlong_qproc_arg;
    dsoreal_q_arg dsoreal_qproc_arg;

    float_q_arg float_qproc_arg;
    double_q_arg double_qproc_arg;
    pointer_q_arg pointer_qproc_arg;

    string_q_arg string_qproc_arg;
    void_q_argin_argout void_qproc_argi_argo;
    void_q_argout void_qproc_argo;

    arb_q_arg arb_qproc_arg;

};

//! signals
//! vritual
typedef void (QObject::*sig_void_void)();

typedef void (QObject::*sig_void_bool)( bool val );
typedef void (QObject::*sig_void_int)( int val );
typedef void (QObject::*sig_void_longlong)( qlonglong val );

typedef void (QObject::*sig_void_float)( float val );
typedef void (QObject::*sig_void_double)( double val );
typedef void (QObject::*sig_void_pointer)( pointer val );
typedef void (QObject::*sig_void_string)( QString val );

typedef void (QObject::*sig_void_arb)( ArbBin val );

typedef union
{
    sig_void_void unSig;        /*!< virtual */

    //! signal
    sig_void_bool void_sig_bool;
    sig_void_int void_sig_int;
    sig_void_longlong void_sig_longlong;

    sig_void_float void_sig_float;
    sig_void_double void_sig_double;
    sig_void_pointer void_sig_pointer;

    sig_void_string void_sig_string;
    sig_void_arb void_sig_arb;

}unSignal;

/*!
 * message table in the class
 */
typedef struct _struCmdEntry
{
    int cmd;                    /*!< cmd \note 0 is seperator */
    void_do_void doProc;        /*!< api */
    void_do_void attrGet;       /*!< attr void getAttr( enumAttr ) */
    enumDoType typeProc;        /*!< api type */
    enumWR wr;                  /*!< write or read */

    QList<int> *pChangeList;
}struCmdEntry;

struct struStubDo
{
    int cmd;
    void_do_arg stubDo;
};

#define start_of_entry()

//! write in
#define on_set( cmd, doProc, apiType )   \
{ (int)cmd, (void_do_void)doProc, NULL, apiType, enum_w, NULL }

//! read from
#define on_get( cmd, queryProc, queryAttr, apiType )   \
{ (int)cmd, (void_do_void)queryProc, (void_do_void)queryAttr, apiType, enum_r, NULL }

//! get without attr
#define on_get_bool(cmd, queryProc )    on_get( cmd, queryProc, NULL, enum_bool_q )
#define on_get_int(cmd, queryProc )     on_get( cmd, queryProc, NULL, enum_int_q )
#define on_get_string( cmd, queryProc)  on_get( cmd, queryProc, NULL, enum_string_q )
#define on_get_ll(cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_longlong_q )

#define on_get_float( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_float_q )
#define on_get_double( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_double_q )
#define on_get_pointer( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_pointer_q)
#define on_get_real( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_dsoreal_q)

#define on_get_dsoreal( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_dsoreal_q)

#define on_get_bool_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_bool_q_int )
#define on_get_int_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_int_q_int )
#define on_get_longlong_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_longlong_q_int )

#define on_get_float_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_float_q_int )
#define on_get_double_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_double_q_int )
#define on_get_dsoreal_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_dsoreal_q_int )

#define on_get_string_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_string_q_int )
#define on_get_pointer_int( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_pointer_q_int )

#define on_get_void_ptr( cmd, queryProc )   on_get( cmd, queryProc, NULL, enum_void_q_ptr )
#define on_get_int_ptr( cmd, queryProc )    on_get( cmd, queryProc, NULL, enum_int_q_ptr )

#define on_get_bool_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_bool_q_arg )
#define on_get_int_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_int_q_arg )
#define on_get_string_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_string_q_arg )
#define on_get_ll_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_longlong_q_arg )

#define on_get_dsoreal_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_dsoreal_q_arg )

#define on_get_float_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_float_q_arg )
#define on_get_double_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_double_q_arg )
#define on_get_pointer_arg( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_pointer_q_arg )

#define on_get_void_argi_argo( cmd, queryProc ) on_get( cmd, queryProc, NULL, enum_void_q_argi_argo )
#define on_get_void_argo( cmd, queryProc )  on_get( cmd, queryProc, NULL, enum_void_q_argo )

//! with attr
#define on_get_bool_attr(cmd, queryProc, queryAttr )    on_get( cmd, queryProc, queryAttr, enum_bool_q )
#define on_get_int_attr(cmd, queryProc, queryAttr )     on_get( cmd, queryProc, queryAttr, enum_int_q )
#define on_get_string_attr( cmd, queryProc, queryAttr )  on_get( cmd, queryProc, queryAttr, enum_string_q )
#define on_get_ll_attr(cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_longlong_q )

#define on_get_float_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_float_q )
#define on_get_double_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_double_q )
#define on_get_pointer_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_pointer_q)
#define on_get_real_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_dsoreal_q)

#define on_get_dsoreal_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_dsoreal_q)

#define on_get_bool_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_bool_q_int )
#define on_get_int_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_int_q_int )
#define on_get_longlong_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_longlong_q_int )

#define on_get_float_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_float_q_int )
#define on_get_double_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_double_q_int )
#define on_get_dsoreal_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_dsoreal_q_int )

#define on_get_string_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_string_q_int )
#define on_get_pointer_int_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_pointer_q_int )

#define on_get_void_ptr_attr( cmd, queryProc, queryAttr )   on_get( cmd, queryProc, queryAttr, enum_void_q_ptr )
#define on_get_int_ptr_attr( cmd, queryProc, queryAttr )    on_get( cmd, queryProc, queryAttr, enum_int_q_ptr )

#define on_get_bool_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_bool_q_arg )
#define on_get_int_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_int_q_arg )
#define on_get_string_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_string_q_arg )
#define on_get_ll_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_longlong_q_arg )

#define on_get_dsoreal_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_dsoreal_q_arg )

#define on_get_float_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_float_q_arg )
#define on_get_double_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_double_q_arg )
#define on_get_pointer_arg_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_pointer_q_arg )

#define on_get_void_argi_argo_attr( cmd, queryProc, queryAttr ) on_get( cmd, queryProc, queryAttr, enum_void_q_argi_argo )
#define on_get_void_argo_attr( cmd, queryProc, queryAttr )  on_get( cmd, queryProc, queryAttr, enum_void_q_argo )

//! set no sig, no user
#define on_set_int_void( cmd, doProc ) on_set( cmd, doProc, enum_int_do_void )
#define on_set_int_bool( cmd, doProc )  on_set( cmd, doProc, enum_int_do_bool )
#define on_set_int_int( cmd, doProc )  on_set( cmd, doProc, enum_int_do_int )
#define on_set_int_string( cmd, doProc )  on_set( cmd, doProc, enum_int_do_string )
#define on_set_int_ll( cmd, doProc )    on_set( cmd, doProc, enum_int_do_longlong )
#define on_set_int_real( cmd, doProc )    on_set( cmd, doProc, enum_int_do_real )

#define on_set_int_pointer( cmd, doProc )    on_set( cmd, doProc, enum_int_do_pointer )
#define on_set_void_void( cmd, doProc ) on_set( cmd, doProc, enum_void_do_void )
#define on_set_void_int( cmd, doProc )  on_set( cmd, doProc, enum_void_do_int )

#define on_set_int_arg( cmd, doProc ) on_set( cmd, doProc, enum_int_do_arg )
#define on_set_void_arg( cmd, doProc ) on_set( cmd, doProc, enum_void_do_arg )

#define on_set_int_obj( cmd, doProc )   on_set( cmd, doProc, enum_int_do_obj )
#define on_set_void_obj( cmd, doProc )  on_set( cmd, doProc, enum_void_do_obj )

#define on_set_int_int2( cmd, doProc )  on_set( cmd, doProc, enum_int_do_int_int )

//! \note must be set at the end of table
#define end_of_entry()  {0,0,NULL, enum_no_type,enum_any, NULL },\
};

//! predos
#define DECLARE_PRE_DO()   protected: \
static struStubDo _preDoEntry[];\
virtual struStubDo *getPreDoEntry() const\
{ return _preDoEntry; }

#define IMPLEMENT_PRE_DO( baseClass, selfClass ) \
struStubDo selfClass::_preDoEntry[]={
#define start_of_predo()
#define on_predo( msg, doProc )     { msg, (void_do_arg)doProc }
#define end_of_predo()    {0,NULL}, };

//! postdos
#define DECLARE_POST_DO()   protected: \
static struStubDo _postDoEntry[];\
virtual struStubDo *getPostDoEntry() const\
{ return _postDoEntry; }

#define IMPLEMENT_POST_DO( baseClass, selfClass ) \
struStubDo selfClass::_postDoEntry[]={
#define start_of_postdo()
#define on_postdo( msg, doProc )     { msg, (void_do_arg)doProc }
#define end_of_postdo()    {0,NULL}, };

#define DECLARE_CMD()   protected:\
static  struCmdEntry _cmdEntry[];\
    \
virtual void getEntries( QList<struCmdEntry*> &maps ) const;\
virtual  struCmdEntry* getCmdEntry() const\
{ return _cmdEntry; }\


//! base class
#define IMPLEMENT_BASE_CMD( baseClass, selfClass )   \
void selfClass::getEntries( QList<struCmdEntry*> &maps ) const\
{\
    maps.append( selfClass::_cmdEntry );\
    return;\
}\
struCmdEntry selfClass::_cmdEntry[] = {

//! sub class
#define IMPLEMENT_CMD( baseClass, selfClass )   \
void selfClass::getEntries( QList<struCmdEntry*> &maps ) const\
{\
    maps.append( selfClass::_cmdEntry );\
    baseClass::getEntries( maps );\
}\
struCmdEntry selfClass::_cmdEntry[] = {


/*! \typedef pprocRPC
 *
 * used for remote interface input
 */
typedef int (*pprocRPC)( CExecutor *pExe, int cmd, CArgument &val );

/*!
 * \brief The CExecutor class
 */
class CExecutor : public QObject
{
    Q_OBJECT
public:
    CExecutor()
    {}

    DECLARE_CMD()

protected:
    static pprocRPC pRpcCall;  /*!< rpc */
public:
    static void setRpcCall( pprocRPC rpc );

protected:
    int onSet( const struCmdEntry *pEntry, CArgument &val );
    int onGet( const struCmdEntry *pEntry, CArgument &val,
               CArgument &valOut );

    int onExec( const struCmdEntry *pEntry, CArgument &val,
                CArgument &valOut );


    int onPostuser_set( const struCmdEntry *pEntry, CArgument &val );

    const struCmdEntry *getEntry( int cmd, enumWR wr = enum_any ) const;
    bool isEntryExist( int cmd, enumWR wr = enum_any ) const;
    int getEntries(  int cmd,
                     const struCmdEntry* entries[],
                     int maxSize,
                     enumWR wr = enum_any ) const;
    bool queryEntryExist( int cmd );

public:
    int onDoCmd( int cmd, CArgument &val, enumWR wr, CArgument &valOut );
    int onDoCmd( int cmd, CArgument &val, enumWR wr );

    int onRpcCmd( int cmd, CArgument &val, enumWR wr, CArgument &valOut );
};

#endif // CCMDTARGET_H
