#ifndef CKEYREQUEST_H
#define CKEYREQUEST_H

#include <QtCore>

typedef int (*pFunc_ikeyproc)( int key, void *pContext );
typedef qlonglong (*pFunc_llkeyproc)( int key, void *pContext );

class CKeyRequest
{
public:
    CKeyRequest();

    void set( int msg,
              void *pContext,
              pFunc_ikeyproc proc );

    void set( int msg,
              void *pContext,
              pFunc_llkeyproc proc );
public:
    int mMsg;
    void *m_pContext;

    pFunc_ikeyproc m_ikeyProc;
    pFunc_llkeyproc m_llkeyProc;
};

#endif // CKEYREQUEST_H
