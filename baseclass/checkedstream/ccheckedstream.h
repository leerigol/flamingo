#ifndef CCHECKEDSTREAM_H
#define CCHECKEDSTREAM_H

#include <QtCore>

class CCheckedStream
{
public:
    CCheckedStream();
public:
    void setVersion( int ver );
    int getVersion();

    qint64 getEpoch();
    void   setEpoch( qint64 d);

    int save( const QString &file,
              void *pStream,
              qint32 size );
    int load( const QString &file,
              void *pStream,
              qint32 size );

private:
    quint32 mHeadCheck;
    qint32 mHeadLen;
    //! >>>> start head check
    qint32 mVer;
    qint64 mTime;       //! to ms

    qint32 mStreamLen;  //! stream
    quint32 mStreamCheck;
    //! <<<< end head check

    //! checked by stream check
    void *m_pStream;    //! stream

};

#endif // CCHECKEDSTREAM_H
