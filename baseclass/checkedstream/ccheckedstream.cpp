#include <unistd.h>
#include "ccheckedstream.h"

#include "../../com/crc32/ccrc32.h"


CCheckedStream::CCheckedStream()
{
    mHeadCheck = 0;
    mHeadLen = 0;

    mVer = 0;
    mTime = 0;
    mStreamLen = 0;
    mStreamCheck = 0;

    m_pStream = NULL;
}

void CCheckedStream::setVersion( int ver )
{
    mVer = ver;
}
int CCheckedStream::getVersion()
{ return mVer; }

qint64 CCheckedStream::getEpoch()
{
    return mTime;
}

void CCheckedStream::setEpoch(qint64 d)
{
    mTime = d;
}

int CCheckedStream::save( const QString &fileName,
          void *pStream,
          qint32 size )
{
    Q_ASSERT( NULL != pStream );
    Q_ASSERT( size > 0 );

    //! time
    mTime = QDateTime::currentMSecsSinceEpoch();

    //! 1. stream
    mStreamLen = size;
    mStreamCheck = com_algorithm::CCrc32::crc32( (quint8*)pStream, size);

    //! 2. head check
    QByteArray headStream;
    headStream.append( (const char*)&mVer, sizeof(mVer) );
    headStream.append( (const char*)&mTime,sizeof(mTime) );
    headStream.append( (const char*)&mStreamLen,sizeof(mStreamLen) );
    headStream.append( (const char*)&mStreamCheck,sizeof(mStreamCheck) );
    mHeadCheck = com_algorithm::CCrc32::crc32( (quint8*)headStream.data(),
                                               headStream.size() );
    mHeadLen = headStream.size();

    //! file op.
    QFile file(fileName);
    if ( !file.open( QIODevice::WriteOnly ) )
    { return -1; }

    //! ---- write
    //! write the check
    if ( sizeof(mHeadCheck) !=
         file.write( (const char*)&mHeadCheck, sizeof(mHeadCheck) ) )
    {
         file.close();
         return -2;
    }

    //! write the headlen
    if ( sizeof(mHeadLen) !=
         file.write( (const char*)&mHeadLen, sizeof(mHeadLen) ) )
    {
         file.close();
         return -2;
    }

    //! write the header stream
    if ( file.write(headStream) != mHeadLen )
    {
        file.close();
        return -2;
    }

    //! write the payload
    if ( file.write( (const char*)pStream, mStreamLen) != mStreamLen )
    {
        file.close();
        return -2;
    }

    file.flush();
    fsync(file.handle());
    //! end
    file.close();

    return mStreamLen;
}

int CCheckedStream::load( const QString &fileName,
          void *pStream,
          qint32 size )
{
    Q_ASSERT( NULL != pStream );
    Q_ASSERT( size > 0 );

    QFile file( fileName );
    if ( !file.open( QIODevice::ReadOnly) )
    {
        return -1;
    }

    //! read the check
    if ( file.read( (char*)&mHeadCheck, sizeof(mHeadCheck)) != sizeof(mHeadCheck) )
    {
        file.close();
        return -2;
    }

    //! read the head len
    if ( file.read( (char*)&mHeadLen, sizeof(mHeadLen))
         != sizeof(mHeadLen) )
    {
        file.close();
        return -2;
    }

    //! 4 + 8 + 4 + 4
    int headerLen = sizeof(mVer) + sizeof(mTime) + sizeof(mStreamLen) + sizeof(mStreamCheck);
    if ( mHeadLen != headerLen )
    {
        file.close();
        return -2;
    }

    //! read the header
    char headerStream[ headerLen ];
    if ( file.read( headerStream, headerLen) != headerLen )
    {
        file.close();
        return -2;
    }

    //! verify the header check
    if ( mHeadCheck !=
         com_algorithm::CCrc32::crc32( (quint8*)headerStream, headerLen) )
    {
        file.close();
        return -3;
    }

    //! unpack
    int offs = 0;
    memcpy( &mVer, headerStream + offs, sizeof(mVer) );
    offs += sizeof(mVer);
    memcpy( &mTime, headerStream + offs, sizeof(mTime) );
    offs += sizeof(mTime);
    memcpy( &mStreamLen, headerStream + offs, sizeof(mStreamLen) );
    offs += sizeof(mStreamLen);
    memcpy( &mStreamCheck, headerStream + offs, sizeof(mStreamCheck) );
    offs += sizeof(mStreamCheck);

    //! now read the file
    if ( mStreamLen != size )
    {
        file.close();
        return -3;
    }

    if ( file.read( (char*)pStream,mStreamLen) != mStreamLen )
    {
        file.close();
        return -2;
    }

    if ( com_algorithm::CCrc32::crc32( (quint8*)pStream, mStreamLen )
                != mStreamCheck )
    {
        file.close();
        return -3;
    }

    file.close();

    return size;
}
