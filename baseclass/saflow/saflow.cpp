#include "saflow.h"

#include "../../include/dsoarith.h"
#include "../../include/dsocfg.h"

CFlowAttr::CFlowAttr()
{
}

void CFlowAttr::setLength( qlonglong m, qlonglong n )
{
    mDepth.set( m, n );
}

void CFlowAttr::setLength( const dsoFract &dep )
{
    mDepth = dep;
}

void CFlowAttr::setDotTime( qlonglong m, qlonglong n )
{
    mDotTime.set( m, n );

    if( mDotTime <= 0 )
    {
      qDebug() << "Will not come here" <<__LINE__ ;
    }
}

void CFlowAttr::setDotTime( const dsoFract &tm )
{
    mDotTime = tm;
    if( mDotTime <= 0 )
    {
      qDebug() << "Will not come here" <<__LINE__ ;
    }
}

void CFlowAttr::setSaInfo( qlonglong saScale,
                           qlonglong saOffset )
{
    mSaScale = saScale;
    mSaOffset = saOffset;
}

//! uHz
qlonglong CFlowAttr::getSa()
{
    if( mDotTime <= 0 )
    {
      qDebug() << "Will not come here" <<__LINE__ ;
    }
                                            //! used ps
    return (dsoFract(sa_base)/mDotTime).toLonglong();
}

dsoFract CFlowAttr::getDotTime()
{ return mDotTime; }

dsoFract CFlowAttr::getDepth()
{ return mDepth; }

CSaFlow::CSaFlow()
{
    mT0 = 0;
    mScale = 0;
    mOffset = 0;

    mDotTime.set(1);//added by hxh
}

CSaFlow::CSaFlow( CFlowAttr &flowCfg,
                  qlonglong scale,
                  qlonglong offset )
{
    //! sa attr
    setLength( flowCfg.mDepth );
    setDotTime( flowCfg.mDotTime );

    saT0( offset );

    //! ref used
    mScale = scale;
    mOffset = offset;
}

void CSaFlow::setT0( qlonglong m, qlonglong n )
{
    mT0.set( m, n );
}

//! only used for raw memory
void CSaFlow::saT0( qlonglong offset )
{
    //! mT0
    //! offset - ( depth ) * dotTime / 2
    //! offset - dM/dN/2*tM/tN
    //! offset - dM*tM/(dN*tN*2)

    mT0 = offset - mDepth * mDotTime/2;
}

dsoFract CSaFlow::getT0()
{ return mT0; }

//! [T0, tEnd)
dsoFract CSaFlow::getTEnd()
{
    return mT0 + mDepth * mDotTime;
}

dsoFract CSaFlow::getTLength()
{
    return mDepth * mDotTime;
}

