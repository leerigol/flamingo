#ifndef _SAFLOW_H_
#define _SAFLOW_H_

#include <QtCore>
#include "../dsofract.h"

//! sa meta
class CFlowAttr
{
public:
    CFlowAttr();

    void setLength( qlonglong m, qlonglong n = 1 );
    void setDotTime( qlonglong m, qlonglong n = 1 );

    void setLength( const dsoFract &dep );
    void setDotTime( const dsoFract &tm );

    void setSaInfo( qlonglong saScale, qlonglong saOffset );

    qlonglong getSa();
    dsoFract getDotTime();
    dsoFract getDepth();

public:
    dsoFract mDepth;        //! dot:1
    dsoFract mDotTime;      //! ps

    dsoFract mRollSaSum;
    qlonglong mSaScale, mSaOffset;
};

class CSaFlow : public CFlowAttr
{
public:
    CSaFlow();

    CSaFlow( CFlowAttr &cfg, qlonglong scale=1000, qlonglong off=0 );

    void setT0( qlonglong m, qlonglong n = 1 );
    void saT0( qlonglong offset );

    dsoFract getT0();
    dsoFract getTEnd();
    dsoFract getTLength();

public:
    dsoFract mT0;
                            //! attribute
    qlonglong mScale;
    qlonglong mOffset;
};

#endif // SAFLOW

