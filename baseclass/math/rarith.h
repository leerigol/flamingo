#ifndef RARITH_H
#define RARITH_H

#include "../dsowfm.h"

class RArith
{
public:
    RArith();

public:
    virtual DsoErr vector_op2( DsoWfm &a,
                               DsoWfm &b,
                               DsoWfm &c );

    virtual DsoErr vector_op1( DsoWfm &a,
                               DsoWfm &c );

    virtual DsoErr scalar_op( DsoWfm &a, float &out, int shift = 100 );
    virtual DsoErr scalar_op( DsoWfm &a, int &out, int shift = 100 );
};

class RAvg : public RArith
{
public:
    RAvg();

public:
    virtual DsoErr scalar_op( DsoWfm &a, float &out, int shift = 100 );
};

class RVpp : public RArith
{
public:
    RVpp();
public:
    virtual DsoErr scalar_op( DsoWfm &a, int &out, int shift = 100 );
};

#endif // RARITH_H
