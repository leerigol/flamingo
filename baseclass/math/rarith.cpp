#include "rarith.h"

RArith::RArith()
{
}

DsoErr RArith::vector_op2( DsoWfm &/*a*/,
                           DsoWfm &/*b*/,
                           DsoWfm &/*c*/ )
{ return ERR_NONE; }

DsoErr RArith::vector_op1( DsoWfm &/*a*/,
                           DsoWfm &/*c*/ )
{ return ERR_NONE; }

DsoErr RArith::scalar_op( DsoWfm &/*a*/, float &/*out*/, int )
{ return ERR_NONE; }
DsoErr RArith::scalar_op( DsoWfm &/*a*/, int &/*out*/, int )
{ return ERR_NONE; }

RAvg::RAvg()
{}

DsoErr RAvg::scalar_op( DsoWfm &a, float &out, int shift )
{
    DsoPoint *pPt;
    int sum;

    if ( a.size() < (1+shift*2) ) return ERR_INVALID_INPUT;

    sum = 0;
    pPt = a.getPoint();
    for( int i = shift; i < a.size()-shift; i++ )
    {
        sum += pPt[i];
    }

    out = (float)sum / (a.size()-shift*2);

    return ERR_NONE;
}

RVpp::RVpp()
{}
DsoErr RVpp::scalar_op( DsoWfm &a, int &out, int shift )
{
    DsoPoint *pPt, ptMax, ptMin, ptNow;

    if ( a.size() < (1+shift*2) ) return ERR_INVALID_INPUT;

    pPt = a.getPoint();
    //! init to same
    ptMax = pPt[shift]; ptMin = pPt[shift];
    for( int i = shift; i < a.size()-shift; i++ )
    {
        ptNow = pPt[i];

        if ( ptNow > ptMax )
        { ptMax = ptNow; }
        else if ( ptNow < ptMin )
        { ptMin = ptNow; }
        else
        {}
    }

    out = ptMax - ptMin;
    return ERR_NONE;
}
