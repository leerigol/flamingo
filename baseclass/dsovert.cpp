#include "dsovert.h"

QList< dsoVert* > dsoVert::_chList;
dsoVert *dsoVert::getCH( Chan ch )
{
    dsoVert *pVert;
    foreach( pVert, dsoVert::_chList )
    {
        Q_ASSERT( pVert != NULL );
        if ( pVert->yChId == ch )
        {
            return pVert;
        }
    }

    return NULL;
}

int dsoVert::getVertSize()
{
    return dsoVert::_chList.size();
}

Chan dsoVert::getActiveChan()
{
    dsoVert *pVert;
    foreach( pVert, dsoVert::_chList )
    {
        Q_ASSERT( NULL != pVert );
        if ( pVert->yOnOff )
        { return pVert->yChId; }
    }

    return chan_none;
}

QList<Chan> dsoVert::getChanList()
{
    QList<Chan> _chanList;
    dsoVert *pVert;
    foreach( pVert, dsoVert::_chList )
    {
        Q_ASSERT( NULL != pVert );
        if ( pVert->getOnOff() )
        {
            _chanList.append( pVert->getChan() );
        }
    }

    return _chanList;
}

void dsoVert::scanCHOrder( Chan chPos[], int cnt )
{
    dsoVert *pVert;

    Q_ASSERT( cnt >= dsoVert::_chList.size() );

    //! for the opened
    int i = 0;
    foreach( pVert, dsoVert::_chList )
    {
        Q_ASSERT( NULL != pVert );

        //! add_by_zx:开机如果4个模拟通道没有都打开，但是打开了一个math，则会出现
        //! 图层错误
        if ( pVert->getOnOff() && (pVert->getChan() < r1) )
        {
            chPos[i] = pVert->getChan();
            i++;
        }
    }

    //! for the closed
    foreach( pVert, dsoVert::_chList )
    {
        Q_ASSERT( NULL != pVert );
        if ( !pVert->getOnOff() || (pVert->getChan() >= r1) )
        {
            chPos[i] = pVert->getChan();
            i++;
        }
    }
}

bool dsoVert::setTop( Chan ch )
{
    dsoVert *pVert = getCH( ch );

    Q_ASSERT( NULL != pVert );
    return pVert->setTop();
}
bool dsoVert::setDeTop( Chan ch )
{
    dsoVert *pVert = getCH( ch );

    Q_ASSERT( NULL != pVert );
    return pVert->setDeTop();
}

dsoVert::dsoVert( Chan ch )
{
    yIncValid = false;
    yGndValid = false;

    yUnit = Unit_V;
    yProbe.setVal( 1, E_0 );

    yRefBase.setVal( 1, E_N6 );

    yColor = Qt::white;
    //! find ch
    if ( isExist( ch ) )
    {
        Q_ASSERT( false );
    }

    yMaxPos = 0;

    //! append
    yChId = ch;
    _chList.append( this );

    mServId = -1;
}

void dsoVert::getPixel( DsoWfm &/*wfm*/, Chan /*subChan*/, HorizontalView )
{
}
void dsoVert::getTrace( DsoWfm &/*wfm*/, Chan /*subChan*/, HorizontalView )
{}
void dsoVert::getMemory( DsoWfm &/*wfm*/, Chan /*subChan*/ )
{}
void dsoVert::getDigi( DsoWfm &/*wfm*/, Chan /*subChan*/, HorizontalView )
{}
void dsoVert::getEye( DsoWfm &/*wfm*/, Chan /*subChan*/ )
{}
bool dsoVert::getOnOff()
{
    return yOnOff;
}
qlonglong dsoVert::getyGnd()
{
    if ( yGndValid )
    {
        return yGnd;
    }
    else
    {
        //! offset
        int offset = yRefOffset * yRefDiv/yRefScale;

        return adc_center + offset;
    }
}

int dsoVert::getyAfeGnd()
{
    int offset;

    offset = yRefOffset * yRefDiv / getyAfeScale();

    return adc_center + offset;
}

float dsoVert::getyInc()
{
    if ( yIncValid )
    {
        return yInc;
        //return yRefDiv;
    }
    else
    {
        Q_ASSERT( yRefDiv > 0 );
        return yRefScale*yRefBase.toFloat()/yRefDiv;
    }
}

Unit dsoVert::getUnit()
{
    return yUnit;
}

qlonglong dsoVert::getYRefScale()
{
    return yRefScale;
}

qlonglong dsoVert::getYRefOffset()
{
    return yRefOffset;
}

int dsoVert::getYRefDiv()
{ return yRefDiv; }
DsoReal dsoVert::getYRefBase()
{ return yRefBase; }

int dsoVert::getyPos( Chan /*subChan*/ )
{
    return 0;
}
int dsoVert::getyMaxPos()
{ return yMaxPos; }
int dsoVert::getyAfeScale()
{
    return yRefScale;
}

VertZone  dsoVert::getVertZone()
{
    if ( yChId >= chan1 && yChId <= chan4 )
    { return vert_analog; }
    else if ( yChId >= d0 && yChId <= d15 )
    { return vert_la; }
    else if ( yChId >= digi_ch1 && yChId <= digi_ch4 )
    { return vert_analog; }
    else if ( yChId >= digi_ch1_l && yChId <= digi_ch4_l )
    { return vert_analog; }
    else if ( yChId >= eye_ch1 && yChId <= eye_ch4 )
    { return vert_analog; }
    else if ( yChId == la )
    { return vert_la; }
    else if ( yChId == ana )
    { return vert_analog; }
    else if ( yChId >= m1 && yChId <= m4 )
    { return vert_math; }
    else if ( yChId >= r1 && yChId <= r10 )
    { return vert_ref; }
    else if ( yChId == ref )
    { return vert_ref;}
    else
    {
        Q_ASSERT(false);
        return vert_analog;
    }
}

chZone dsoVert::getZone()
{
    return yZone;
}

Impedance dsoVert::getImpedance()
{
    return yImp;
}

QString dsoVert::getCHLabel()
{
    return yChlabel;
}

DsoReal dsoVert::getProbe()
{
    return yProbe;
}
Coupling dsoVert::getCoupling()
{ return yCoupling; }
QColor dsoVert::getColor()
{ return yColor; }

int  dsoVert::voltToChPt( float val )
{
    //! val / inc + yGnd
    float probe;
    probe = yProbe.toFloat();

    int dist = val * yRefDiv / probe / yRefScale;

    return getyGnd() + dist;
}
int  dsoVert::voltToAdc( float val )
{
    //! only for afe channel
    Q_ASSERT( yChId >= chan1 && yChId <= chan4 );

    //! val / inc + yGnd
    float probe;
    probe = yProbe.toFloat();

    int afeScale = getyAfeScale();

    int dist = val * yRefDiv / probe / (afeScale*ch_volt_base) ;

    return getyAfeGnd() + dist;
}

float dsoVert::chPtToVolt( int chpt )
{
    int dist = chpt - getyGnd();

    float probe;
    probe = yProbe.toFloat();

    float base = yRefBase.toFloat();

    return dist*probe*base*yRefScale/yRefDiv;
}
float dsoVert::adcToVolt( int adc )
{
    //! only for afe channel
    Q_ASSERT( yChId >= chan1 && yChId <= chan4 );

    int dist = adc - getyAfeGnd();

    float probe;
    probe = yProbe.toFloat();

    float base = getyAfeScale() * ch_volt_base;

    return dist*probe*base/yRefDiv;
}

bool dsoVert::isExist( Chan ch )
{
    dsoVert *pVert;

    foreach( pVert, _chList )
    {
        Q_ASSERT( pVert != NULL );

        if ( pVert->yChId == ch )
        {
            return true;
        }
    }

    return false;
}

void dsoVert::setyOnOff( bool b )
{
    yOnOff = b;
}

void dsoVert::setyInc( float inc )
{
    yInc = inc;
    yIncValid = true;
}

void dsoVert::setyGnd( qlonglong gnd )
{
    yGnd = gnd;
    yGndValid = true;
}

void dsoVert::setyUnit( Unit unit )
{
    yUnit = unit;
}

void dsoVert::setyLabel( const QString &str )
{ yChlabel = str; }

void dsoVert::setyZone( chZone zone )
{
    yZone = zone;
}

void dsoVert::setyImpedance( Impedance imp )
{
    yImp = imp ;
}

void dsoVert::setyProbe( DsoReal real )
{
    yProbe = real;
}

void dsoVert::setyRefScale( qlonglong scale )
{
    yRefScale = scale;
}

void dsoVert::setyRefOffset( qlonglong offset )
{
    yRefOffset = offset;
}

void dsoVert::setyMaxPos( int yMax )
{
    yMaxPos = yMax;
}

bool dsoVert::setTop()
{
    if ( yOnOff )
    {
        Q_ASSERT( !dsoVert::_chList.isEmpty() );

        if ( this == dsoVert::_chList.first() )
        {
            return false;
        }

        dsoVert::_chList.removeAll( this );
        dsoVert::_chList.prepend( this );

        return true;
    }
    else
    { return false; }
}
bool dsoVert::setDeTop()
{
    if ( dsoVert::_chList.isEmpty() )
    { return false; }

    //! is the first one
    if ( this == dsoVert::_chList.first() )
    {  }
    else
    { return false; }

    //! remove the first
    if ( dsoVert::_chList.size() > 1 )
    {
        dsoVert::_chList.removeAll( this );
        dsoVert::_chList.append( this );

        return dsoVert::_chList.first()->yOnOff;
    }
    else
    { return false; }
}

void dsoVert::setServId( int id )
{
    mServId = id;
}
int dsoVert::getServId()
{ return mServId; }

void dsoVert::setChan( Chan ch )
{
    yChId = ch;
}
Chan dsoVert::getChan()
{ return yChId; }
