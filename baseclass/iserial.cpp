#include "iserial.h"
#include <QDebug>
#include <string.h>
#include "../include/dsodbg.h"
/*!
 * \brief ISerial::ISerial
 *
 */
ISerial::ISerial()
{
    mVersion = 0;
    mBinCompatible = false;

    m_pBinStream = NULL;
}

ISerial::~ISerial()
{
    if ( NULL != m_pBinStream )
    { delete m_pBinStream; }
}

serialVersion ISerial::getVersion()
{
    return mVersion;
}

bool ISerial::getBinCompatible()
{
    return mBinCompatible;
}

/*!
 * \brief ISerial::serialOut
 *
 * \param stream
 * \param ver [out] the version of the stream
 * \return int
 */
int ISerial::serialOut( CStream &/*stream*/, serialVersion &ver )
{
    ver = mVersion;

    return ERR_NONE;
}
/*!
 * \brief ISerial::serialIn
 * \param stream
 * \param ver, the version of the input stream
 * \return
 * \note do not overwrite the internal version
 */
int ISerial::serialIn( CStream &/*stream*/, serialVersion /*ver*/ )
{
    return ERR_NONE;
}
/*!
 * \brief ISerial::rst
 */
void ISerial::rst()
{}
/*!
 * \brief ISerial::startup
 * \return
 */
int ISerial::startup()
{
    return ERR_NONE;
}

void ISerial::init()
{}


void ISerial::save( int blockSize )
{
    if ( m_pBinStream != NULL )
    { delete m_pBinStream; }

    m_pBinStream = R_NEW( CBinStream( blockSize ) );
    Q_ASSERT( NULL != m_pBinStream );

    serialVersion ver;
    int ret = serialOut( *m_pBinStream, ver );
    Q_ASSERT( ret == 0 );
}

void ISerial::restore()
{
    Q_ASSERT( NULL != m_pBinStream );

    int ret;
    ret = serialIn( *m_pBinStream, mVersion );
    Q_ASSERT( ret == 0 );

    delete m_pBinStream;
    m_pBinStream = NULL;

    startup();


}

