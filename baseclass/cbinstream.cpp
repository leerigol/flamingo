#include "cbinstream.h"
#include "../include/dsodbg.h"
CBinStream::CBinStream( void *pStream, int cap ) : CStream(pStream, cap )
{
    mBlockSize = 0;
}

CBinStream::CBinStream( int block )
{
    Q_ASSERT( block > 0 );

    p_mBase = R_NEW( quint8[ block ] );
    Q_ASSERT( p_mBase != NULL );
    mBlockSize = block;

    mCap = block;
    mLen = 0;
    mOff = 0;
}
CBinStream::~CBinStream()
{
    if ( mBlockSize > 0 )
    {
        Q_ASSERT( p_mBase != NULL );
        quint8 *pBuf;
        pBuf = (quint8*)p_mBase;
        delete []pBuf;
    }
}

void CBinStream::attach( void *pStream, int size )
{
    p_mBase = pStream;
    mCap = size;

    mLen = size;
    mOff = 0;
}
