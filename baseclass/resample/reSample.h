#ifndef _RE_SAMPLE_H_
#define _RE_SAMPLE_H_
#include <QDebug>
#include "memView.h"

#include "dataCompress.h"
#include "dataExpand.h"

enum enumSampleAttr
{
    e_sample_norm = 1,  /*< 等间隔: 隐含保留头部 */
    e_sample_peak = 2,  /*< 峰值 */

    e_sample_head = 4,  /*< 保留头部 */
    e_sample_tail = 8,  /*< 保留尾部 */

};
/*! \brief reSample
 *
 * 从mem中提取数据，包含
 * -抽样：mem中点多于view，有峰值／普通两种方式，只能选择一种
 * -插值：mem中点小于view，采用的是线性插值
 *
 * 抽样时可以根据需要选择保留‘头部’或‘尾部’，或‘头部|尾部’
 * 例如，在进行数据添加时，就需要保留头部和尾部数据，以确保压缩后能够看到整体的轮廓
 *
 * ‘普通’抽样时已经隐含的包含了‘头部’
 * 插值时，属性设置无效
 */
template<class ptType >
int reSample( struMem *pMem,
              struView *pView,

              ptType *pDataIn,
              ptType *pDataOut,

              int attr = e_sample_norm | e_sample_head | e_sample_tail
              )
{
    int vLength;
    int mLength;
    int offset;
    struFract mfLength;

    //! view set
    pView->mapToMem( pMem );

    //! invalid range
    if ( pView->vLeft >= pView->vRight )
    {
        return -1;
    }

    if ( pMem->m1 > pMem->m2 )
    {
        return -1;
    }

    if ( pMem->m1 == pMem->m2 )
    {
        return -1;
    }

    //! expand or compress
    vLength = pView->vRight - pView->vLeft;
    mfLength = pMem->m2 - pMem->m1;

    mLength = mfLength.value();

    //! compress
    if ( mLength >= vLength )
    {
        offset = pMem->m1.value();

        do
        {
            //! peak
            if ( (attr & e_sample_peak ) == e_sample_peak )
            {
                dataCompress_Peak( pDataIn + offset,
                              pDataOut,

                              mLength,
                              vLength );
            }

            if ( (attr & e_sample_norm)  == e_sample_norm )
            {
                dataCompress( pDataIn + offset,
                              pDataOut,

                              mLength,
                              vLength );
            }

            //! 保留头部
            if ( (attr & e_sample_head) == e_sample_head )
            {
                pDataOut[0] = pDataIn[ offset ];
            }

            //! 保留尾部
            if ( (attr & e_sample_tail) == e_sample_tail )
            {
                pDataOut[ vLength - 1 ] = pDataIn[ mLength -1 ];
            }

        }while ( 0 );
    }
    //! expand
    else
    {
        dataExpand( pDataIn + pMem->m1.value(),
                    pDataOut,

                    mLength,
                    vLength );

    }

    return 0;
}

template<class ptType >
int reSampleExt( struMem *pMem,
              struView *pView,

              ptType *pDataIn,
              ptType *pDataOut,

              int attr = e_sample_norm | e_sample_head | e_sample_tail
              )
{
    int vLength, mLength;
    int padedVLen;

    //! view set
    if ( pView->mapOfMem( pMem ) < 0 )
    {
        //qDebug()<<__FUNCTION__<<__LINE__<<"invalid config";
        return -1;
    }

    //! no data
    vLength = pView->vLen;
    if ( vLength < 1  )
    { return -1; }

    mLength = pMem->mPtLen;
    Q_ASSERT( mLength > 0 );
    //! zoom
    if ( pMem->mtInc > pView->mvtInc )
    {
        //! err: 0.5
        padedVLen = mLength * pMem->mtInc / pView->mvtInc;

        //! tune
        dataExpandExt<ptType>( pDataIn + pMem->mPtL,
                    pDataOut + pView->vLeft,

                    mLength,
                    padedVLen,

                    pView->mvIntxpShift,
                    pView->vLen );
    }
    //! compress
    else if ( pMem->mtInc < pView->mvtInc )
    {
        do
        {
            //! peak
            if ( (attr & e_sample_peak ) == e_sample_peak )
            {
                dataCompress_Peak<ptType>(
                              pDataIn + pMem->mPtL,
                              pDataOut + pView->vLeft,

                              mLength,
                              vLength );
            }

            //! norm
            if ( (attr & e_sample_norm)  == e_sample_norm )
            {
                dataCompress<ptType>(
                              pDataIn + pMem->mPtL,
                              pDataOut + pView->vLeft,

                              mLength,
                              vLength );
            }

            //! 保留头部
            if ( (attr & e_sample_head) == e_sample_head )
            {
                pDataOut[ pView->vLeft ] = pDataIn[ pMem->mPtL ];
            }

            //! 保留尾部
            if ( (attr & e_sample_tail) == e_sample_tail )
            {
                pDataOut[ pView->vLeft + vLength - 1 ] = pDataIn[ pMem->mPtL + pMem->mPtLen - 1 ];
            }

        }while ( 0 );
    }
    //! 1:1
    else
    {

        memcpy( pDataOut + pView->vLeft,
                pDataIn + pMem->mPtL,
                vLength * sizeof(ptType) );
    }

    return 0;
}

template<class ptType >
int reSampleExt( long long inLen,
              long long outLen,
              ptType *pDataIn,
              ptType *pDataOut,

              int attr = e_sample_peak | e_sample_head | e_sample_tail
              )
{
    struMem mem;
    struView view;
    //! mem
    mem.mt0 = 0;
    mem.mLen = inLen;

    mem.mtInc = outLen;

    //! view
    view.mvt0 = 0;
    view.mWidth = outLen;

    view.mvtInc = inLen;

    //! resample
    return reSampleExt( &mem, &view,
                 pDataIn, pDataOut,
                 attr );
}

#endif
