
#include "fract.h"
#include "../../arith/gcd.h"
//static int gcd(int x, int y);

//! fract operator
struFract struFract::operator=( qlonglong v )
{
    mM = v;
    mN = 1;

    return *this;
}

struFract struFract::operator-()
{
    struFract fract;

    fract.mM = -mM;
    fract.mN = mN;

    return fract;
}

struFract struFract::operator+( qlonglong x )
{
    struFract fract;

    fract.mM = mM + x * mN;
    fract.mN = mN;

    return fract;
}

struFract struFract::operator-( qlonglong x )
{
    return *this+(-x);
}

struFract struFract::operator+( struFract fractIn )
{
    struFract fract;

    fract.mM = mM * fractIn.mN + fractIn.mM * mN;
    fract.mN = mN * fractIn.mN;

    fract.norm();

    return fract;
}

struFract struFract::operator-( struFract fractIn )
{
    return *this + (-fractIn);
}

struFract struFract::operator*( qlonglong x )
{
    struFract fract;

    fract.mM = mM * x;
    fract.mN = mN;

    fract.norm();

    return fract;
}

struFract struFract::operator/( qlonglong v )
{
    struFract fract;
    
    fract = *this;

    fract.mM = mM;
    fract.mN *= v;

    fract.norm();

    return fract;
}

struFract struFract::operator*( struFract v )
{
    struFract fract;

    fract = *this;

    fract.mM *= v.mM;
    fract.mN *= v.mN;

    fract.norm();

    return fract;
}

struFract struFract::operator/( struFract v )
{
    struFract fract;

    fract = *this;

    fract.mM *= v.mN;
    fract.mN *= v.mM;

    fract.norm();

    return fract;
}

bool struFract::operator>( qlonglong val )
{
    //! compare to 0
    if ( val == 0 )
    {
        //! + / +
        if ( mM > 0 && mN > 0 )
        {
            return true;
        }
        //! - / -
        else if ( mM < 0 && mN < 0 )
        {
            return true;
        }
        //! 0 /
        else if ( mM == 0 )
        {
            return false;
        }
        else
        {
            return false;
        }
    }

    //! val != 0
    if ( mM > val *mN )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool struFract::operator==( qlonglong val )
{
    //! compare to 0
    if ( val == 0 )
    {
        if ( mM == 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    if ( mM == val * mN )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool struFract::operator>( struFract v )
{
    struFract f;

    f = *this;

    f = f - v;

    return f > 0;
}

bool struFract::operator==( struFract v )
{
    struFract f;

    f = *this;

    f = f - v;

    return f.mM == 0;
}

//! 最小公倍数约减
void struFract::norm()
{
    int m;

    if ( mM >= mN )
    {
        m = gcd( mM, mN );
    }
    else
    {
        m = gcd( mN, mM );
    }

    mM /= m;
    mN /= m;
}

int struFract::value()
{
    return mM/mN;
}

void struFract::setMN( qlonglong m, qlonglong n )
{
    mM = m;
    mN = n;

    norm();
}

///*!
// * \brief gcd
// * \param x
// * \param y
// * \return
// * 欧几里得辗转相除法求两数的最大的公约数
// */
//int gcd(int x, int y)  	 // x >= y
//{
//	int m;
	
//    //! to positive
//    if ( x < 0 )
//    {
//        x = -x;
//    }
//    if ( y < 0 )
//    {
//        y = -y;
//    }

//	if( x < y )
//	{
//		return gcd( y, x );
//	}

//	if ( y == 0 )
//	{
//		return x;
//	}

//	m = x % y;
//	if( m != 0 )
//	{
//		return gcd( y, m );
//	}
//	else
//	{
//		return y;
//	}
//}
