
#include <QtCore>

#ifndef _FRACT_H_

//! fract = mM / mN;
struct struFract
{
    qlonglong mM;
    qlonglong mN;

    struFract operator=( qlonglong v );
    
    struFract operator-();

    struFract operator+( qlonglong v );
    struFract operator-( qlonglong v );

    struFract operator+( struFract v );
    struFract operator-( struFract v );

    struFract operator*( qlonglong v );
    struFract operator/( qlonglong v );

    struFract operator*( struFract v );
    struFract operator/( struFract v );

    bool operator>( qlonglong v );

    bool operator==(qlonglong v );
    
    bool operator>( struFract v );

    bool operator==( struFract v );
    
    void norm();
    
    int value();

    void setMN( qlonglong m, qlonglong n );

    void show();
};


#endif 
