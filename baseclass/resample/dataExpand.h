
#ifndef _DATA_EXPAND_H_
#define _DATA_EXPAND_H_


template<class DATA_ZOOM_TYPE >
void dataExpand( DATA_ZOOM_TYPE *pIn,
                 DATA_ZOOM_TYPE * pOut,
                 int s32LenIn,	// in < out
                 int s32LenOut)
{
	DATA_ZOOM_TYPE* pExpOut;
	
    DATA_ZOOM_TYPE a, b;
    int s32Dist;
    DATA_ZOOM_TYPE ma, mb, mk;
	
	int s32Mul, s32Rem, s32Exp;
	
	int i, j, k;
	
	int s32Cur;

	pExpOut = pOut;
	
	s32Mul = s32LenOut / s32LenIn;			// 组基数
	s32Rem = s32LenOut - s32Mul * s32LenIn;	// 余数

	s32Exp = s32Rem;
	s32Cur = 0;
	j      = 0;
	for ( i = 0; i < s32LenIn-1; i++ )
	{
		s32Cur += s32Mul;
		s32Exp += s32Rem;
		if ( s32Exp >= s32LenIn )
		{
			s32Exp -= s32LenIn;
			s32Cur++;
		}
											// 扩展数据
        a = pIn[i];
		s32Dist = s32Cur - j;				// 扩展数量
		j = s32Cur;
		
		if ( s32Dist > 1 )
		{
            b = pIn[i+1];                   // 后一个点
            ma = a * 256;                   //! muled
            mb = b * 256;
            mk = (mb - ma)/s32Dist;
											// a + kx
			for ( k = 0; k < s32Dist; k++ )
			{
                *pExpOut++ = ( ma + ( mk * k ) ) / 256;
			}
		}
		else // = 1
		{
            *pExpOut++ = a;
		}
	}

    //! the last set
    for ( ; s32Cur < s32LenOut; s32Cur++ )
    {
        *pExpOut++ = pIn[i];
    }
}

template<typename STACK_TYPE>
class dataStack
{
private:
    STACK_TYPE *m_pBase;
    int mPad, mLen;
    int mTop;
public:
    dataStack( STACK_TYPE *pBase,
               int pad,
               int len )
    {
        m_pBase = pBase;
        mPad = pad;
        mLen = len;
        mTop = 0;
    }

    //! return left size
    int push( STACK_TYPE val )
    {
        if ( mPad > 0 )
        {
            mPad--;
            return mLen;
        }

        Q_ASSERT( mTop < mLen );

        m_pBase[mTop++] = val;

        return (mLen-mTop);
    }
};

template<class DATA_ZOOM_TYPE >
void dataExpandExt( DATA_ZOOM_TYPE *pIn,
                    DATA_ZOOM_TYPE * pOut,

                    int s32LenIn,     // in < out
                    int s32LenOut,

                    int interpOffset,
                    int vLen
                    )
{
    DATA_ZOOM_TYPE a, b, val;
    int s32Dist;
    DATA_ZOOM_TYPE ma, mb, mk;

    int s32Mul, s32Rem, s32Exp;

    int i, j, k;

    int s32Cur;

    s32Mul = s32LenOut / s32LenIn;			// 组基数
    s32Rem = s32LenOut - s32Mul * s32LenIn;	// 余数

    s32Exp = s32Rem;
    s32Cur = 0;
    j      = 0;

    //! set stack
    dataStack<DATA_ZOOM_TYPE> expStack( pOut, interpOffset, vLen );

    for ( i = 0; i < s32LenIn-1; i++ )
    {
        s32Cur += s32Mul;
        s32Exp += s32Rem;
        if ( s32Exp >= s32LenIn )
        {
            s32Exp -= s32LenIn;
            s32Cur++;
        }
                                            // 扩展数据
        a = pIn[i];
        s32Dist = s32Cur - j;				// 扩展数量
        j = s32Cur;

        if ( s32Dist > 1 )
        {
            b = pIn[i+1];                   // 后一个点
            ma = a * 256;                   //! muled
            mb = b * 256;
            mk = (mb - ma)/s32Dist;
                                            // a + kx
            for ( k = 0; k < s32Dist; k++ )
            {
                val = ( ma + ( mk * k ) ) / 256;

                //! full
                if ( expStack.push( val ) == 0 )
                {
                    return;
                }
            }
        }
        else // = 1
        {
            //! full
            if ( expStack.push( a ) == 0 )
            {
                return;
            }
        }
    }

    //! the last set
    val = pIn[i];
    for ( ; s32Cur < s32LenOut; s32Cur++ )
    {
        //! full
        if ( expStack.push( val ) == 0 )
        {
            return;
        }
    }
}

#endif
