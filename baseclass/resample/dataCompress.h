#ifndef _DATA_COMPRESS_H_
#define _DATA_COMPRESS_H_

/*! \brief dataCompress
 *
 * compress from head
 */
template<typename dataType >
void dataCompress( dataType *pIn,
                  dataType *pOut,
                  int s32LenIn,
                  int s32LenOut )
{
    int i;
	int s32ptr,s32tempptr,s32temp;

	dataType datTemp;	
	
	int s32mul = s32LenIn / s32LenOut;		// 整数倍
	int s32rem = s32LenIn % s32LenOut;		// 余数

	s32tempptr = 0;
	s32ptr = 0;
	s32temp = 0;

	for( i = 0; i < s32LenOut; i++)
	{
		s32ptr += s32mul;					// 终点	
		s32temp += s32rem;					// 余数
        if( s32temp >= s32LenOut )			// 扩展1
		{
			s32ptr ++;
			s32temp -= s32LenOut;
		}
											// 前一组的终点
//		for( k=s32tempptr; k<s32ptr; k++ )	// 从一组中抽取第一个数据
		{
			datTemp = pIn[s32tempptr];
		}

		pOut[i] = datTemp;					// 数据输出
		
		s32tempptr = s32ptr;				// 下一组起始位置
	}	
}

/*! \brief dataCompress_Peak
 *
 * - compress from head by peak
 * - the 'peak' means extract two points from each group
 * - the 'peak' order is keeped
 */
template<typename dataType >
void dataCompress_Peak( dataType  *pIn,
						dataType  *pOut,

						int s32LenIn,		    // In > out
						int s32LenOut		    //
					)
{
    int i,k;
	int s32ptr,s32tempptr,s32temp;
    int gpCnt;

	dataType datTemp;	
	
    //! return the first
    if ( s32LenOut < 2 )
    {
        pOut[0] = pIn[0];
        return;
    }

    gpCnt = s32LenOut/2;
    int s32mul = 2 * s32LenIn / ( s32LenOut );	// 整数倍
    int s32rem = s32LenIn % (gpCnt);            // 余数
	
	bool bBigLast;
	dataType big,small;
	
	s32tempptr = 0;
	s32ptr = 0;
	s32temp = 0;
	
    for( i=0; i< (gpCnt)*2; i += 2 )
	{
		s32ptr += s32mul;					// 终点	
		s32temp += s32rem;					// 余数
        if( s32temp >= gpCnt )		// 扩展1
		{
			s32ptr ++;
            s32temp -= gpCnt;
		}
											// init
		big = pIn[s32tempptr];
		small = big;
		bBigLast = true;
											// 前一组的终点
		for( k=s32tempptr; k<s32ptr; k++ )	// 从一组中抽取两个数据
		{
			datTemp = pIn[k];
			
			if ( datTemp > big )
			{ big = datTemp; bBigLast = true; }
			else if ( datTemp < small )
			{ small = datTemp; bBigLast = false; }		
		}

											// save two pts
		if ( bBigLast )											
		{	
			pOut[i]  = small;
			pOut[i+1] = big;
		}
		else
		{
			pOut[i]  = big;
			pOut[i+1] = small;
		}			
		s32tempptr = s32ptr;				// 下一组起始位置
	}	

    //! the last point 
    if ( i < s32LenOut )
    {
        pOut[i] = pIn[ s32LenIn - 1 ];
    }
}

#endif
