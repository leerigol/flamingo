
#ifndef _MEM_VIEW_H_
#define _MEM_VIEW_H_

#include "fract.h"

struct struMem
{
    int mLen;           //! 存储深度
    int mCenter;        //! 扩展中心点

    struFract m1;       //! mleft
    struFract m2;       //! mright

    void setLength( int len );
    void setCenter( int cen );

    void sett( long long t0,
               long long tInc );

    long long mt0;      //! left time
    long long mtInc;    //! tInc

    int mPtL, mPtLen;
};

struct struView
{
    int mWidth;         //! 显示宽度
    struFract mfRatio;  //! 内存／屏幕比例，内存小于屏幕则放大，内存大于屏幕则缩小

    int vLeft;          //! 显示起始位置
    int vRight;         //! 显示终止位置

    int vLen;           //! view length

    long long mvt0;     //! view t0
    long long mvtInc;   //! view inc

    int mvIntxpShift;   //! interp shift

    struView();

    void setWidth( int wid );
    void setRatio( struFract fract );
    void setRatio( qlonglong m, qlonglong n );
    void setRatio( qlonglong v );

    void sett( long long t0, long long tinc );

    void mapToMem( struMem *pMem );

                        //! calc the m1, m2
                        //! vleft, vLen
    int mapOfMem( struMem *pMem );

    void mapCompress( struMem *pMem );
    void mapZoom( struMem *pMem );
};

#endif
