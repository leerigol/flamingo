

#include "memView.h"

void struMem::setLength( int len )
{
    mLen = len;
}

void struMem::setCenter( int cent )
{
    mCenter = cent;
}

void struMem::sett( long long t0,
           long long tInc )
{
    mt0 = t0;
    mtInc = tInc;
}

struView::struView()
{
    vLeft = 0;
    vRight = 0;
    vLen = 0;

    mvt0 = 0;
    mvtInc = 0;

    mvIntxpShift = 0;
}

void struView::setWidth( int wid )
{
    mWidth = wid;
}

void struView::setRatio( struFract fract )
{
    mfRatio = fract;
}

void struView::setRatio( qlonglong m, qlonglong n )
{
    mfRatio.setMN( m, n );
}

void struView::setRatio( qlonglong v )
{
    mfRatio = v;
}

void struView::sett( long long t0,
                     long long tinc )
{
    mvt0 = t0;
    mvtInc = tinc;
}

void struView::mapToMem( struMem *pMem )
{
    struFract fSpan, halfSpan;
    struFract fVLen;
    struFract fTRight;
    struFract tmpFract;

    int cent;

    fSpan   = mfRatio * mWidth;
    halfSpan = fSpan / 2;

    cent = pMem->mCenter;

    //! move to left
    if ( !( halfSpan > cent ) )
    {
        vLeft = 0;

        pMem->m1 =  -halfSpan + cent;

        fTRight = halfSpan + cent;

        //! out of right
        if ( fTRight > pMem->mLen )
        {
            pMem->m2 = pMem->mLen;
        }
        else
        {
            pMem->m2 = fTRight;
        }

        fVLen = ( pMem->m2 - pMem->m1 ) /mfRatio;
        vRight = vLeft + fVLen.value();
    }
    //! move to right
    else
    {
        tmpFract = ( halfSpan - cent  ) / mfRatio;
        vLeft = tmpFract.value();

        pMem->m1 = 0;

        fTRight = halfSpan + cent;

        //! not full
        if ( fTRight > (pMem->mLen ) )
        {
            pMem->m2 = pMem->mLen;
        }
        else
        {
            pMem->m2 = fTRight;
        }

        fVLen = ( pMem->m2 - pMem->m1 ) / mfRatio;
        vRight = vLeft + fVLen.value();
    }
}

int struView::mapOfMem( struMem *pMem )
{
    //! no data
    if ( pMem->mLen < 1 )
    {
        vLeft = 0;
        vLen = 0;
        return -1;
    }

    //! no view
    if ( mWidth < 1 )
    {
        vLeft = 0;
        vLen = 0;
        return -1;
    }

    //! invalid config
    if ( mvtInc < 1 || pMem->mtInc < 1 )
    {
        vLeft = 0;
        vLen = 0;
        return -1;
    }

    //! tinc
    mfRatio.setMN( mvtInc, pMem->mtInc );

    long long tMEnd, tVEnd;
    //! memory end dot time
    tMEnd = pMem->mt0 + pMem->mtInc * (pMem->mLen-1) + pMem->mtInc - 1;

    //! view end dot time
    tVEnd = mvt0 + mvtInc * ( mWidth - 1 ) + mvtInc - 1;

    //! check in range
    if ( mvt0 <= pMem->mt0 &&
         tVEnd >= pMem->mt0 )
    {}
    else if ( mvt0 >= pMem->mt0 &&
              mvt0 <= tMEnd )
    {}
    else
    {
        vLeft = 0;
        vLen = 0;
        return -1;
    }

    //! now for the view left
    qlonglong tLeft, tRight;
    if ( mvt0 < pMem-> mt0 )
    {
        pMem->mPtL = 0;
        mvIntxpShift = 0;

        vLeft = (pMem->mt0 - mvt0)/mvtInc;

        tLeft = pMem->mt0;
    }
    else if ( mvt0 > pMem->mt0 )
    {
        //! align to left
        //! add_by_zx
        pMem->mPtL = (mvt0 - pMem->mt0)/pMem->mtInc;

        //! interp shift
        long long memRema;
        memRema = (mvt0 - pMem->mt0)%pMem->mtInc;
        //! 内插后的偏移
        //! 内插倍数不会超过G,所以不会溢出
        mvIntxpShift = memRema / mvtInc;

        vLeft = 0;

        tLeft = mvt0;
    }
    else
    {
        pMem->mPtL = 0;

        mvIntxpShift = 0;
        vLeft = 0;

        tLeft = pMem->mt0;
    }

    //! for the view end
    //int vRight;
    if ( tVEnd < tMEnd )
    {
        vRight = 0;

        tRight = tVEnd;
    }
    else if ( tVEnd > tMEnd )
    {
        vRight = (tVEnd - tMEnd + mvtInc - 1 )/mvtInc;

        tRight = tMEnd;
    }
    else
    {
        vRight = 0;

        tRight = tMEnd;
    }

    //! aligh to right
    pMem->mPtLen = (tRight - tLeft + pMem->mtInc - 1 )/pMem->mtInc;


    //! view columns
    vLen = mWidth - vRight - vLeft;

    //! no data
    //! error protect
    if ( vLen < 1 )
    {
        vLeft = 0;
        vLen = 0;
        return -1;
    }

    return 0;
}

void struView::mapCompress( struMem */*pMem*/ )
{}
void struView::mapZoom( struMem */*pMem*/ )
{}


