#ifndef IHELP_H
#define IHELP_H

#include <QtCore>

/*!
 * \brief The IHelp class
 * help接口
 */
class IHelp
{
private:
    static QList< QEvent::Type > _uiEvents;
public:
    IHelp();

    void setHelp( const QString &name, int msg );
    void setHelpable( bool b );

public:
    bool canHelp();
    bool doHelp( QEvent *pEvent );
    bool doHelp();
protected:
    void actionHelp();

protected:
    QString mServName;
    int mMsg;
    bool mHelpAble;
};

#endif // IHELP_H
