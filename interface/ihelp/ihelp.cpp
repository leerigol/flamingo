
#include "../../include/dsostd.h"

#include "ihelp.h"
#include "../../baseclass/chelprequest.h"

QList< QEvent::Type > IHelp::_uiEvents;

IHelp::IHelp()
{
    mMsg = -1;
    mHelpAble = true;
    if ( IHelp::_uiEvents.size() < 1 )
    {
        IHelp::_uiEvents.append( QEvent::MouseButtonPress );
        IHelp::_uiEvents.append( QEvent::MouseButtonRelease );
        IHelp::_uiEvents.append( QEvent::MouseButtonDblClick );
        IHelp::_uiEvents.append( QEvent::MouseMove );

        IHelp::_uiEvents.append( QEvent::KeyRelease );
    }
}

void IHelp::setHelp( const QString &name, int msg )
{
    mServName = name;
    mMsg = msg;
}

void IHelp::setHelpable( bool b )
{
    mHelpAble = b;
}

bool IHelp::canHelp()
{
    return mHelpAble;
}
bool IHelp::doHelp( QEvent *pEvent )
{
    if ( !canHelp() )
    { return false; }

    if ( sysGetWorkMode() == work_help )
    {
        Q_ASSERT( NULL != pEvent );
        QEvent::Type type = pEvent->type();

        //! action msg
        if ( IHelp::_uiEvents.contains( type) )
        {}
        else
        { return false; }

        actionHelp();

        return true;
    }

    return false;
}

bool IHelp::doHelp()
{
    if ( !canHelp() )
    { return false; }

    if ( sysGetWorkMode() == work_help )
    {
        actionHelp();

        return true;
    }

    return false;
}

void IHelp::actionHelp()
{
    if ( mServName.length() > 0 && mMsg > 0 )
    {}
    else
    {
        qWarning()<<"no help name && msg";
        return;
    }

    //! service active
    serviceExecutor::post( mServName,
                           CMD_SERVICE_ACTIVE,
                           1,
                           NULL );

    //! post to help service
    CHelpRequest *pReq;
    pReq = R_NEW( CHelpRequest( mServName,
                             mMsg ) );
    if ( NULL == pReq )
    { return; }

    serviceExecutor::post( (int)E_SERVICE_ID_HELP,
                           CMD_SERVICE_HELP_REQUESST,
                           pReq );
}
