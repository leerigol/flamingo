#ifndef RCOMBOBOX_H
#define RCOMBOBOX_H

#include <QtWidgets>

namespace menu_res {

class RComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit RComboBox(QWidget *parent = 0);

protected:

signals:


public slots:

public:

};

}

#endif // RCOMBOX_H
