#include "rfuncknob.h"

namespace menu_res {

RFuncKnob_Style RFuncKnob::_style;

QList<RFuncKnob::KnobEvent> RFuncKnob::_padFilter;

RFuncKnob::RFuncKnob( QWidget *parent ) : QWidget(parent)
{
    //! load events
    if ( RFuncKnob::_padFilter.size() < 1 )
    {
        RFuncKnob::_padFilter.append( RFuncKnob::knob_inc);
        RFuncKnob::_padFilter.append( RFuncKnob::knob_dec);
    }

    setWindowFlags( Qt::FramelessWindowHint );

    _style.loadResource("ui/func_knob/" );

    mView = control_knob_tune_pad;
    mStat = element_deactived;
}

void RFuncKnob::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);

    _style.paintEvent( painter, mView, mStat, 0, 0 );
}

void RFuncKnob::setState( ElementStatus stat )
{
    mStat = stat;
}
void RFuncKnob::setView( eControlKnob view )
{
    mView = view;
}

eControlKnob RFuncKnob::getView()
{
    return mView;
}

bool RFuncKnob::knobEventFilter( KnobEvent kE )
{
    if ( mView == control_knob_pad )
    {
        return RFuncKnob::_padFilter.contains( kE );
    }

    return false;
}

}

