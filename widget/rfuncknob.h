#ifndef RFUNCKNOB_H
#define RFUNCKNOB_H

#include <QtWidgets>

#include "../menu/menustyle/rcontrolstyle.h"
#include "../menu/menustyle/rfuncknob_style.h"

namespace menu_res {

class RFuncKnob : public QWidget
{
public:
    enum KnobEvent
    {
        knob_inc,
        knob_dec,
        knob_z,
    };
private:
    static RFuncKnob_Style _style;

    static QList<KnobEvent> _padFilter;
public:
    RFuncKnob( QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    void setState( ElementStatus stat );
    void setView( eControlKnob view );
    eControlKnob getView();

    bool knobEventFilter( KnobEvent kE );

protected:
    ElementStatus mStat;
    eControlKnob mView;
};

}

#endif // RFUNCKNOB_H
