#ifndef RSCROLLBAR_H
#define RSCROLLBAR_H

#include <QWidget>

#include "rimagebutton.h"
#include "../menu/menustyle/rscrollbar_style.h"

namespace menu_res {

class RScrollBar : public QWidget
{
    Q_OBJECT
public:
    explicit RScrollBar(QWidget *parent = 0);

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual void resizeEvent(QResizeEvent *event);
    virtual void wheelEvent(QWheelEvent * event);
signals:
    void sig_page_changed( int from, int to );  //! range changed
    void sig_value_changed( int val );          //! now changed

public slots:
    void on_head_clicked();
    void on_tail_clicked();

    void on_page_changed( int from, int to );
    void on_now_changed();
    void on_range_changed();

public:
    void moveHandle();
    void moveUp( int n = 1 );
    void moveDown( int n = 1 );
public:
    void setStyle( const QString &path );

    void setScroll( int min, int max,
                    int now,
                    int pageSize );

    void setNow( int now );
    int getNow();

    int getViewId();
    void getViewRange( int &a, int &b );
    int getViewFrom();
protected:
    RImageButton *m_pHead, *m_pTail, *m_pHandle;

    int mMin, mMax;
    int mNow, mPageSize;

    RScrollBar_Style mStyle;

    //! variable
    QRect mBarRect;
    int mViewFrom, mViewTo;
};

}
#endif // RSCROLLBAR_H
