#include "rline.h"

namespace menu_res {

RVLine::RVLine(QWidget *parent) : QFrame(parent)
{
    QFrame::setFrameStyle( frameStyle() | QFrame::VLine );
}

RHLine::RHLine(QWidget *parent) : QFrame(parent)
{
    QFrame::setFrameStyle( frameStyle() | QFrame::HLine );
}

}

