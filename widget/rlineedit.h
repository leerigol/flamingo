#ifndef RLINEEDIT_H
#define RLINEEDIT_H

#include <QtWidgets>

namespace menu_res {

class RLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit RLineEdit(QWidget *parent = 0);

protected:
    virtual void focusInEvent(QFocusEvent *event );
    virtual void focusOutEvent( QFocusEvent *event );

    virtual void mouseReleaseEvent( QMouseEvent *event );

signals:
    void sig_focusIn();
    void sig_focusOut();
    void sig_mouseRelease();
public slots:

};

}

#endif // RLINEEDIT_H
