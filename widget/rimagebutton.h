#ifndef RIMAGEBUTTON_H
#define RIMAGEBUTTON_H

#include <QtWidgets>

namespace menu_res {

class RImageButton : public QPushButton
{
    Q_OBJECT
public:
    RImageButton( QWidget *parent=0);

public:
    void setImage( const QString &nImage,
                   const QString &downImage,
                   const QString &dImage,
                   bool bChecked = false );
    void setImage( const QString &image,
                   bool bChecked = false );

    void setImage( const QStringList &imgList,
                   bool bChecked = false );
    void setImage( const QString &imgs,
                   const QString &sep,
                   bool bChecked = false );
    void setImage( const QString &imgs,
                   const char *sep,
                   bool bChecked = false );

protected:
    virtual void paintEvent( QPaintEvent *event );

private:
    //! not checked, checked
    QString mNormalImage[2], mDownImage[2], mDisabledImage[2];
};

}

#endif // RIMAGEBUTTON_H
