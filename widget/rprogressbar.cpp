#include "rprogressbar.h"

namespace menu_res {

RProgressBarStyle RProgressBar::_style;

RProgressBar::RProgressBar( QWidget *parent ) : QProgressBar( parent )
{
    _style.loadResource("ui/progress_bar/");
}

void RProgressBar::paintEvent(QPaintEvent */*event*/)
{
    QPainter painter(this);

    _style.paintBar( painter, minimum(),
                              maximum(),
                              value(),
                              rect() );

    _style.paintText( painter, minimum(),
                               maximum(),
                               value(),
                               rect(),
                               text() );
}

}

