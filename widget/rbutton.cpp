#include "rbutton.h"

namespace menu_res {

RButton_Style RButton::_style;

RButton::RButton( QWidget *parent ) : QPushButton( parent )
{
    _style.loadResource("ui/button/");

    mImgId = -1;

    mFontSize = 0;
}

/*!
 * \brief RButton::addImage
 * \param imageEn
 * \param imageDis
 * 添加两张图片，用于指示允许和禁止状态
 */
void RButton::addImage( const QString &imageEn ,
                        const QString &imageDis)
{
    mImages.append(imageEn);
    mImages.append(imageDis);
    mImgId = 0;
}
void RButton::selectImage( int imgId )
{
    if ( imgId >=0 && imgId < mImages.size()/2 )
    {
        mImgId = imgId;
        update();
    }
}

void RButton::setFont( const QString &family,
              int size )
{
    mFontSize = size;
    mFont = family;
}

void RButton::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    ElementStatus stat;
    if ( !isEnabled() )
    {
        stat = element_disabled;
    }
    else if ( hasFocus() && isChecked() )
    {
        stat = element_checked_down;
    }
    else if ( !isDown() && isChecked() )
    {
        stat = element_checked;
    }
    else if ( isDown() || isChecked() )
    {
        stat = element_actived_down;
    }
    else if ( hasFocus() )
    {
        stat = element_actived;
    }    
    else
    {
        stat = element_deactived;
    }

    QFont font;
    if ( mFontSize > 0 )
    {
        font.setFamily( mFont );
        font.setPointSize( mFontSize );
        painter.setFont( font );
    }

    if ( mImgId >=0 )
    {
        _style.paintEvent( painter,
                           rect(),
                           text(),
                           mImages[mImgId*2 + (stat==element_disabled? 1 : 0 ) ],
                           stat );
    }
    else
    {
        _style.paintEvent( painter,
                           rect(),
                           text(),
                           QLatin1Literal(""),
                           stat );
    }
}

}
