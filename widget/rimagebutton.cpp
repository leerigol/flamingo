#include "rimagebutton.h"

namespace menu_res {

RImageButton::RImageButton( QWidget *parent ) : QPushButton( parent )
{
}

void RImageButton::setImage( const QString &nImage,
               const QString &downImage,
               const QString &dImage,
               bool bChecked )
{
    mNormalImage[bChecked] = nImage;
    mDownImage[bChecked] = downImage;
    mDisabledImage[bChecked] = dImage;

    setCheckable(bChecked);

    update();
}

void RImageButton::setImage( const QString &image, bool bChecked )
{
    mNormalImage[bChecked] = image;
    mDownImage[bChecked] = image;
    mDisabledImage[bChecked] = image;

    setCheckable(bChecked);

    update();
}

void RImageButton::setImage( const QStringList &imgList, bool bChecked )
{
    Q_ASSERT( imgList.size() >= 3 );

    mNormalImage[bChecked] = imgList[0];
    mDownImage[bChecked] = imgList[1];
    mDisabledImage[bChecked] = imgList[2];

    setCheckable(bChecked);

    update();
}

void RImageButton::setImage( const QString &imgs,
                             const QString &sep,
                             bool bChecked )
{
    QStringList strList;

    strList = imgs.split( sep );

    setImage( strList, bChecked );

}

void RImageButton::setImage( const QString &imgs,
               const char *sep,
               bool bChecked  )
{
    QStringList strList;

    strList = imgs.split( QString(sep) );

    setImage( strList, bChecked );
}

void RImageButton::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QImage img;

    //! load image by status
    if ( !isEnabled() )
    {
        img.load( mDisabledImage[ isChecked() ] );
    }
    else if ( isDown() )
    {
        img.load( mDownImage[isChecked()] );
    }
    else
    {
        if(mNormalImage[isChecked()].isEmpty())
        {
            //! error
            return;
        }

        img.load( mNormalImage[isChecked()] );
    }

    if ( !img.isNull() )
    { painter.drawImage(0,0, img ); }
}

}

