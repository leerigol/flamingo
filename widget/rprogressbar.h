#ifndef RPROGRESSBAR_H
#define RPROGRESSBAR_H

#include <QtWidgets>

#include "../menu/menustyle/rprogressbarstyle.h"

namespace menu_res {

class RProgressBar : public QProgressBar
{
    Q_OBJECT
private:
    static RProgressBarStyle _style;
public:
    RProgressBar( QWidget *parent=0 );

protected:
    virtual void paintEvent(QPaintEvent *event);

};

}

#endif // RPROGRESSBAR_H
