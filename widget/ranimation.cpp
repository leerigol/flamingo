#include "ranimation.h"

namespace menu_res {

RAnimation::RAnimation( QWidget *parent ) : QPushButton( parent )
{
    mNow = 0;

    connect( &mTimer, SIGNAL(timeout()), this, SLOT(on_timeout()) );
}

void RAnimation::paintEvent( QPaintEvent */*event*/ )
{
    Q_ASSERT( mNow >=0 );

    QPainter painter(this);

    //! down
    if ( isDown() )
    {
        showPic( mDown, painter );
        return;
    }

    //! running
    if ( mTimer.isActive() )
    {
        if ( mNow < mAnimateList.size() )
        {
            showPic( mAnimateList[mNow], painter );
        }
    }
    //! not running
    else
    {
        showPic( mNorm, painter );
    }
}

void RAnimation::on_timeout()
{
    mNow++;
    if ( mNow >= mAnimateList.size() )
    {
        mNow = 0;
    }

    update();
}

void RAnimation::start( int timems )
{
    mTimer.start( timems );
}
void RAnimation::stop()
{
    mTimer.stop();

    update();
}

void RAnimation::addAnimation( const QString &str )
{
    mAnimateList.append( str );
}
void RAnimation::clearAnimation()
{
    mAnimateList.clear();
}

void RAnimation::setNormalDown( const QString &norm,
                                const QString &down )
{
    mNorm = norm;
    mDown = down;
}

void RAnimation::showPic( const QString &pic,
                          QPainter &painter )
{
    QImage img;

    if ( img.load( pic ) )
    {
        painter.drawImage( 0, 0, img );
    }
}

}

