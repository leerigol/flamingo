#include "rscrollbar.h"

namespace menu_res {

RScrollBar::RScrollBar(QWidget *parent) : QWidget(parent)
{
    mStyle.loadResource("ui/scrollbar/");

    m_pHead = new RImageButton( this );
    m_pTail = new RImageButton( this );
    m_pHandle = new RImageButton( this );

    Q_ASSERT( m_pHead!=NULL );
    Q_ASSERT( m_pTail!=NULL );
    Q_ASSERT( m_pHandle!=NULL );

    m_pHead->setImage( mStyle.mImageHeadN,
                       mStyle.mImageHeadDown,
                       mStyle.mImageHeadDown );
    m_pTail->setImage( mStyle.mImageTailN,
                       mStyle.mImageTailDown,
                       mStyle.mImageTailDown );
    m_pHandle->setImage( mStyle.mImageHandleN,
                         mStyle.mImageHandleDown,
                         mStyle.mImageHandleDown );
    m_pHandle->hide();

    connect( this, SIGNAL(sig_page_changed(int, int)),
             this, SLOT(on_page_changed(int,int)) );
    connect( m_pHead, SIGNAL(clicked(bool)),
             this, SLOT(on_head_clicked()) );
    connect( m_pTail, SIGNAL(clicked(bool)),
             this, SLOT(on_tail_clicked()) );
}

void RScrollBar::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( event != NULL );
#if 0
    QPainter painter(this);

    painter.fillRect( rect(), mStyle.mBgColor );

    QLinearGradient grad;
    grad.setStart( mStyle.mBarXY.x(), mStyle.mBarXY.y());
    grad.setFinalStop( mStyle.mBarXY.x(),
                       mStyle.mBarSize + mStyle.mBarXY.y());

    grad.setColorAt( 0, mStyle.mBgColor );
    grad.setColorAt( 1, mStyle.mBgColor );
    grad.setColorAt( (float)(mNow-mViewFrom)/(mPageSize-1), mStyle.mFgColor );

    painter.fillRect( mBarRect,
                      QBrush(grad) );
#endif
}

void RScrollBar::resizeEvent(QResizeEvent *event)
{
    Q_ASSERT( event != NULL );

    QRect selfRect = rect();

    mStyle.resize( selfRect );

    m_pHead->move( mStyle.mHeadRect.topLeft() );
    m_pTail->move( mStyle.mTailX, mStyle.mTailY );

    mBarRect.setRect( mStyle.mBarXY.x(),
                      mStyle.mBarXY.y(),
                      mStyle.mBarW,
                      mStyle.mBarSize );
    moveHandle();
}

void RScrollBar::wheelEvent(QWheelEvent * event)
{
    QPoint numPixels = event->pixelDelta();
    QPoint numDegrees = event->angleDelta() / 8;

    int step = 0;

    if (!numPixels.isNull())
    {
        step = numPixels.y();
    }
    else if (!numDegrees.isNull())
    {
        QPoint numSteps = numDegrees / 15;

        step = numSteps.y();
    }
    else
    {
        step = 0;
    }

    if ( step == 0 )
    { return; }

    //! forward
    if ( step > 0 )
    {
        moveUp( step );
    }
    //! backword
    else if ( step < 0 )
    {
        moveDown( -step );
    }
    else
    {
    }
}

void RScrollBar::on_head_clicked()
{
    moveUp( 1 );
}
void RScrollBar::on_tail_clicked()
{
    moveDown( 1 );
}

void RScrollBar::on_page_changed( int /*from*/, int /*to*/ )
{
    m_pHead->setVisible( mViewFrom > mMin );
    m_pTail->setVisible( mViewTo < mMax );

    //! image changed
    if ( !m_pHead->isVisible() )
    {
        m_pHandle->setImage( mStyle.mImageHandleHeadN,
                             mStyle.mImageHandleHeadDown,
                             mStyle.mImageHandleHeadDown);
    }
    else if ( !m_pTail->isVisible() )
    {
        m_pHandle->setImage( mStyle.mImageHandleTailN,
                             mStyle.mImageHandleTailDown,
                             mStyle.mImageHandleTailDown);
    }
    else
    {
        m_pHandle->setImage( mStyle.mImageHandleN,
                             mStyle.mImageHandleDown,
                             mStyle.mImageHandleDown );
    }

    moveHandle();

    update();
}

//! change range by now
void RScrollBar::on_now_changed()
{
    if ( mNow < mViewFrom )
    {
        mViewFrom = mNow;
        mViewTo = mViewFrom + mPageSize - 1;

        emit sig_page_changed( mViewFrom, mViewTo );
    }
    else if ( mNow > mViewTo )
    {
        mViewTo = mNow;
        mViewFrom = mViewTo - mPageSize + 1;

        emit sig_page_changed( mViewFrom, mViewTo );
    }
    else
    {
        moveHandle();

        update();
    }
}

//! change now by range
void RScrollBar::on_range_changed()
{
    if ( mNow < mViewFrom )
    {
        mNow = mViewFrom;
        emit sig_value_changed( mNow );
    }
    else if ( mNow > mViewTo )
    {
        mNow = mViewTo;
        emit sig_value_changed( mNow );
    }
    else
    {}

    emit sig_page_changed( mViewFrom, mViewTo );
}

void RScrollBar::moveHandle()
{
    //! the handle
    int handlePos;
    int handleY, pureDist;

    do
    {
        pureDist = ( mStyle.mBarSize - mStyle.mHandleRect.height() );
        Q_ASSERT( mMax >= mMin );
        if ( mMax == mMin )
        {
            handleY = pureDist;
        }
        else
        {
            handleY = pureDist * ( mNow - mMin) / (mMax-mMin);
        }
        handlePos = handleY + mStyle.mBarXY.y();

        m_pHandle->move( mStyle.mHandleRect.x(), handlePos );
    }while( 0 );
}

void RScrollBar::moveUp( int n  )
{
    if ( mViewFrom > mMin )
    {
        mViewFrom -= n;

        if ( mViewFrom < mMin )
        {
            mViewFrom = mMin;
        }

        mViewTo = mViewFrom + mPageSize - 1;

        on_range_changed();
    }
}
void RScrollBar::moveDown( int n  )
{
    if ( mViewTo < mMax )
    {
        mViewTo += n;

        if ( mViewTo > mMax )
        {
            mViewTo = mMax;
        }

        mViewFrom = mViewTo - mPageSize + 1;

        on_range_changed();
    }
}

void RScrollBar::setStyle( const QString &path )
{
    mStyle.loadResource( path );

    mStyle.resize( rect() );
}

void RScrollBar::setScroll( int min, int max,
                int now,
                int pageSize )
{
    mMin = min;
    mMax = max;

    //! init now
    mNow = now;

    mPageSize = pageSize;

    //! init from and to range
    mViewFrom = mNow - mPageSize/2;
    if ( mViewFrom < mMin )
    {
        mViewFrom = mMin;
    }

    //! init end
    mViewTo = mViewFrom + mPageSize - 1;
    if ( mViewTo >= mMax )
    {
        mViewTo = mMax;
        mViewFrom = mViewTo - mPageSize + 1;
    }

    //! init from
    if ( mViewFrom < mMin )
    {
        mViewFrom = mMin;
    }

    on_range_changed();
}

void RScrollBar::setNow( int now )
{
    if ( now < mMin )
    {
        now = mMin;
    }
    else if ( now >= mMax )
    {
        now = mMax;
    }
    else
    {}

    if ( mNow != now )
    {
        mNow = now;
        on_now_changed();

        emit sig_value_changed( now );
    }
}
int RScrollBar::getNow()
{
    return mNow;
}

int RScrollBar::getViewId()
{
    return (mNow - mViewFrom);
}

void RScrollBar::getViewRange(int &a, int &b)
{
    a = mViewFrom;
    b = mViewTo;
}

int RScrollBar::getViewFrom()
{
    return mViewFrom;
}

}
