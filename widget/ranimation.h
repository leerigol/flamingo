#ifndef RANIMATION_H
#define RANIMATION_H

#include <QtWidgets>

namespace menu_res {

/*!
 * \brief The RAnimation class
 * -动画控件定时切换显示的图片
 * -图片显示的顺序和添加的图片顺序一致
 */
class RAnimation : public QPushButton
{
    Q_OBJECT

public:
    RAnimation( QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event );

protected Q_SLOTS:
    void on_timeout();

public:
    void start( int timems = 500 );
    void stop();

    void addAnimation( const QString &str );
    void clearAnimation();

    void setNormalDown( const QString &strNorm,
                        const QString &strDown );

protected:
    void showPic( const QString &pic,
                  QPainter &painter );

private:
    int mNow;
    QStringList mAnimateList;
    QTimer mTimer;
    QString mNorm, mDown;
};

}

#endif // RANIMATION_H
