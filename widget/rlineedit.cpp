#include "rlineedit.h"

namespace menu_res {

RLineEdit::RLineEdit(QWidget *parent) : QLineEdit(parent)
{
    this->setStyleSheet("QLineEdit{background-color: black;color:#c0c0c0;border:1px solid rgb(68, 68, 68);} ");
}

void RLineEdit::focusInEvent(QFocusEvent */*event*/ )
{
    emit sig_focusIn();
}
void RLineEdit::focusOutEvent( QFocusEvent */*event*/ )
{
    emit sig_focusOut();
}

void RLineEdit::mouseReleaseEvent( QMouseEvent */*event*/ )
{
    emit sig_mouseRelease();
}

}

