#ifndef RBUTTON_H
#define RBUTTON_H

#include <QtWidgets>

#include "../menu/menustyle/rbutton_style.h"

namespace menu_res {

class RButton : public QPushButton
{
    Q_OBJECT

private:
    static RButton_Style _style;
    QStringList mImages;
    int mImgId;
    int mFontSize;
    QString mFont;
public:
    RButton( QWidget *parent=0);

    void addImage( const QString &imageEn,
                   const QString &imageDisable );
    void selectImage( int imgId=0 );

    void setFont( const QString &family,
                  int size );
protected:
    virtual void paintEvent( QPaintEvent *event );
};


}

#endif // RBUTTON_H
