#ifndef RLINE_H
#define RLINE_H

#include <QtWidgets>

namespace menu_res {

class RVLine : public QFrame
{
    Q_OBJECT
public:
    RVLine( QWidget *parent = 0 );
};

class RHLine : public QFrame
{
    Q_OBJECT
public:
    RHLine( QWidget *parent = 0 );
};

}

#endif // RLINE_H
