#ifndef CFRAM_H
#define CFRAM_H

#include <QtCore>

namespace fram_disk
{

class CFram
{
public:
    CFram( int cap=8192,
           int addr=0x52,
           const QString &fileName="/dev/i2c-0");
    ~CFram();
public:
    int open();
    void close();

    quint8* getDiskBuf();

    int read( void *pBuf, int offset, int length );
    int write( void *pBuf, int offset, int length );

    int loadCache();
    int flushCache();

public:
    int _write( int offset, void *pBuf, int len );
    int _read( int offset, void *pBuf, int len );

public:
    int page_write( int offset, void *pBuf, int len );
    int page_read( int offset, void *pBuf, int len );

protected:
    QString mFileName;
    int mFd;            /**< file handle */
    int mAddr;

    int mCapacity;

    quint8 *mDiskBuf;
    quint8 *mDiskCache;

private:
    QMutex mMutex;
};

}

#endif // CFRAM_H
