#include "cfram.h"

#include <unistd.h>
#include <sys/ioctl.h>

#include <fcntl.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include "../include/dsodbg.h"

namespace fram_disk {

/*
 * 这里size不能超过8，比如不能为16，
 * 因为这样会影响触摸屏，导致某些情况下触摸屏复位了
*/
#define batch_size  1

CFram::CFram( int cap,
              int addr,
              const QString &fileName )
{
    mCapacity = cap;
    mAddr = addr;

    mFileName = fileName;
    mFd = -1;

    Q_ASSERT( mCapacity != 0 );

    mDiskBuf = NULL;
    mDiskCache = NULL;
}

CFram::~CFram()
{
    close();
}

int CFram::open()
{
    mFd = ::open( mFileName.toLatin1().data(), O_RDWR);

    if( mFd < 0)
    {
        return -1;
    }

    //! disk buf
    mDiskBuf = R_NEW( quint8[ mCapacity ] );
    Q_ASSERT( mDiskBuf != NULL );

    mDiskCache = R_NEW( quint8[ mCapacity ] );
    Q_ASSERT( mDiskCache != NULL );

    return 0;
}
void CFram::close()
{
    if ( mFd >= 0 )
    {
        ::close( mFd );
    }

    if ( NULL != mDiskBuf )
    {
        delete []mDiskBuf;
        mDiskBuf = NULL;
    }

    if ( NULL != mDiskCache )
    {
        delete []mDiskCache;
        mDiskCache = NULL;
    }
}

quint8* CFram::getDiskBuf()
{
    return mDiskBuf;
}

int CFram::read( void *pBuf, int offset, int length )
{
    //! invalid input
    if ( NULL == pBuf
         || (length + offset) > mCapacity
         )
    {
        return -1;
    }

    memcpy ( pBuf, mDiskBuf + offset, length );

    return length;
}

int CFram::write( void *pBuf, int offset, int length )
{
    //! invalid input
    if ( NULL == pBuf
         || (length + offset) > mCapacity
         )
    {
        return -1;
    }

    memcpy ( mDiskBuf + offset, pBuf, length );

    return length;
}

int CFram::loadCache()
{
    Q_ASSERT( mDiskCache != NULL );

    if ( mFd < 0 ) return -1;

    //! read to cache
    if( mCapacity != _read( 0, mDiskCache, mCapacity ) )
    {
        return -1;
    }

    //! set the buffer
    memcpy( mDiskBuf, mDiskCache, mCapacity );

    return 0;
}

int CFram::flushCache()
{
    if ( mFd < 0 ) return -1;

    int i;
    int from, count, patch;
    int ret;

    from = -1;
    count = 0;
    patch = 0;
    for ( i = 0; i < mCapacity; i++ )
    {
        //! not equal
        if ( mDiskBuf[i] != mDiskCache[i] )
        {
            //! set the base
            if ( from == -1 )
            {
                from = i;
                count = 0;
            }
            else
            {}

            count++;
        }
        else
        {
            //! write to disk
            if ( from != -1 )
            {
                ret = _write( from, mDiskBuf + from, count );
                patch += count;
                //! write to cache
                if ( ret == count )
                {
                    memcpy( mDiskCache + from, mDiskBuf + from, count );
                }
                //! flush fail
                else
                {
                    return -1;
                }

                //! reset the base
                from = -1;
            }
            else
            {}
        }
    }

    //! write the last patch
    if ( from != -1 )
    {
        ret = _write( from, mDiskBuf + from, count );
        patch += count;
        //! write to cache
        if ( ret == count )
        {
            memcpy( mDiskCache + from, mDiskBuf + from, count );
        }
        //! flush fail
        else
        {
            return -1;
        }

        //! reset the base
        from = -1;
    }
    else
    {}

    LOG_DBG()<<patch;

    return 0;
}

int CFram::_write( int offset, void *pBuf, int len )
{
    int i, ret;

    i = 0;
    for ( i = 0; i < len / batch_size; i++ )
    {
        ret = page_write( offset + i * batch_size,
                          (quint8*)pBuf + i * batch_size,
                          batch_size );
        if ( ret != batch_size )
        { return -1; }
    }

    int rema;
    rema = len - (len / batch_size ) * batch_size;
    if ( rema > 0 )
    {
        ret = page_write( offset + i * batch_size,
                          (quint8*)pBuf + i * batch_size,
                          rema );
        if ( ret != rema )
        { return -1; }
    }

    return len;
}

int CFram::_read( int offset, void *pBuf, int len )
{
    int i, ret;

    i = 0;
    for ( i = 0; i < len / batch_size; i++ )
    {
        ret = page_read( offset + i * batch_size,
                          (quint8*)pBuf + i * batch_size,
                          batch_size );
        if ( ret != batch_size )
        { return -1; }
    }

    int rema;
    rema = len - (len / batch_size ) * batch_size;
    if ( rema > 0 )
    {
        ret = page_read( offset + i * batch_size,
                          (quint8*)pBuf + i * batch_size,
                          rema );
        if ( ret != rema )
        { return -1; }
    }

    return len;
}

int CFram::page_write( int offset, void *pBuf, int len )
{
    Q_ASSERT( NULL != pBuf );
    Q_ASSERT( offset >= 0 );
    Q_ASSERT( len > 0 );

    //! buf from stack
    unsigned char outbuf[ 2 + len ];
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[1];

    mMutex.lock();

    messages[0].addr  = mAddr;
    messages[0].flags = 0;
    messages[0].len   = len + 2;
    messages[0].buf   = outbuf;

    outbuf[0] = (offset >> 8) & 0xff;
    outbuf[1] = offset & 0xff;
    memcpy( outbuf + 2, pBuf, len );

    packets.msgs  = messages;
    packets.nmsgs = 1;
    if(ioctl( mFd, I2C_RDWR, &packets) < 0)
    {
        len = -1;
    }

    mMutex.unlock();

    return len;
}

#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#define I2C_FILE_NAME "/dev/i2c-0"
#define SIZE 256
int read_touch( int reg )
{
    int addr = 0x48;

    static int i2c_file = -1;
    if( i2c_file == -1 )
    {
        i2c_file = open(I2C_FILE_NAME, O_RDWR);
    }
    {
        unsigned char outbuf[4];
        unsigned char buf[4];
        struct i2c_rdwr_ioctl_data packets;
        struct i2c_msg messages[2];

        /*
         * In order to read a register, we first do a "dummy write" by writing
         * 0 bytes to the register we want to read from.  This is similar to
         * the packet in set_i2c_register, except it's 1 byte rather than 2.
         */
        outbuf[0] = reg & 0xff;//(reg >> 8) & 0xff;
        //outbuf[1] = reg & 0xff;
        messages[0].addr  = addr;
        messages[0].flags = 0;
        messages[0].len   = 1;//sizeof(outbuf);
        messages[0].buf   = outbuf;

        /* The data will get returned in this structure */
        messages[1].addr  = addr;
        messages[1].flags = I2C_M_RD/* | I2C_M_NOSTART*/;
        messages[1].len   = 2;
        messages[1].buf   = buf;

        /* Send the request to the kernel and get the result back */
        packets.msgs      = messages;
        packets.nmsgs     = 2;
        if(ioctl(i2c_file, I2C_RDWR, &packets) < 0)
        {
            perror("Unable to send data");
            return 1;
        }
        if( buf[0] == 0 && (buf[1] == 0x64 || buf[1] == 0))
        {
            //printf("Read %02x = 0x%02x%02x--------\n\n",reg, buf[0],buf[1]);
            return 2;
        }
        return 0;
    }
}
int CFram::page_read( int offset, void *pBuf, int len )
{
    Q_ASSERT( NULL != pBuf );
    Q_ASSERT( offset >= 0 );
    Q_ASSERT( len > 0 );

    unsigned char outbuf[2];
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[2];

    mMutex.lock();

    outbuf[0] = (offset >> 8) & 0xff;
    outbuf[1] = offset & 0xff;
    messages[0].addr  = mAddr;
    messages[0].flags = 0;
    messages[0].len   = sizeof(outbuf);
    messages[0].buf   = outbuf;

    messages[1].addr  = mAddr;
    messages[1].flags = I2C_M_RD/* | I2C_M_NOSTART*/;
    messages[1].len   = len;
    messages[1].buf   = (unsigned char*)pBuf;

    packets.msgs      = messages;
    packets.nmsgs     = 2;
    if( ioctl( mFd, I2C_RDWR, &packets) < 0 )
    {
        qDebug() << "FRAM read error";
        len = -1;
    }

    mMutex.unlock();

//    if( 2 == read_touch(0x34) )
//    {

//    }
//    else
//    {
//        qDebug() << "fram address:" << offset;
//    }
    return len;
}

}

