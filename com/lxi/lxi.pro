TEMPLATE = lib
TARGET = ../../lib$$(PLATFORM)/3rdlib/lxi

CONFIG += static

INCLUDEPATH += include/

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

SOURCES += \
    source/authuxprot.c \
    source/bindrsvprt.c \
    source/lxi_api.c \
    source/lxi_arch.c \
    source/lxi_mdns.c \
    source/lxi_net.c \
    source/lxi_vxi.c \
    source/lxi_web.c \
    source/rpc_cmsg.c \
    source/rpc_dtable.c \
    source/rpc_prot.c \
    source/svc.c \
    source/svc_auth.c \
    source/svc_authux.c \
    source/svc_run.c \
    source/svc_tcp.c \
    source/svc_udp.c \
    source/svc_vxi.c \
    source/xdr.c \
    source/xdr_array.c \
    source/xdr_mem.c \
    source/xdr_rec.c \
    source/xdr_vxi.c

HEADERS += \
    include/auth.h \
    include/auth_unix.h \
    include/clnt.h \
    include/DataType.h \
    include/lxi_api.h \
    include/lxi_arch.h \
    include/lxi_int.h \
    include/lxi_mdns.h \
    include/lxi_net.h \
    include/lxi_vxi.h \
    include/lxi_web.h \
    include/netdb.h \
    include/pmap_clnt.h \
    include/pmap_prot.h \
    include/rpc.h \
    include/rpc_msg.h \
    include/svc.h \
    include/svc_auth.h \
    include/svc_vxi.h \
    include/types.h \
    include/xdr.h \
    include/xdr_vxi.h

