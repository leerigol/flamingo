/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: svc_vxi.h
  功能描述: Vxi-11功能中对xdr解析的结构体和函数声明

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-06

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-06          1.0        initial

*******************************************************************************/
#ifndef XDR_VXI_H
#define XDR_VXI_H

#include "xdr.h"
#include "lxi_vxi.h"

#ifdef __cplusplus
extern "C" {
#endif

/*定义VXI设备关于32位有符号整型数据类型的定义*/
#if VXI_DEBUG == 1

typedef     int                 VXI_INT32;
#define     XDR_VXI_INT         xdr_int

#else

typedef     long                VXI_INT32;
#define     XDR_VXI_INT         xdr_long

#endif

/*定义VXI设备关于32位无符号整型数据类型的定义*/
#if VXI_DEBUG == 1

typedef     unsigned int        VXI_UINT32;
#define     XDR_VXI_UINT        xdr_u_int

#else

typedef     unsigned long       VXI_UINT32;
#define     XDR_VXI_UINT        xdr_u_long

#endif

/*VXI设备连接时的参数类型定义*/
typedef     VXI_INT32           DEVICE_LINK;
/*VXI设备通信时网络报文标识类型定义*/
typedef     VXI_INT32           DEVICE_FLAGS;
/*VXI设备错误码值类型*/
typedef     VXI_INT32           DEVICE_ERROR_CODE;

/*VXI设备通信时使用的协议类定义*/
typedef enum
{
    DEVICE_TCP = 0,
    DEVICE_UDP
}DEVICE_FAMILY_EM;

/*定义VXI设备错误的结构体*/
typedef struct
{
    DEVICE_ERROR_CODE  error;
}DEVICE_ERROR_STRU;

/*定义VXI设备进行连接时的参数结构体*/
typedef struct
{
    VXI_INT32      id;
    bool_t         lock;
    VXI_UINT32     lock_timeout;
    char          *device;
}DEVICE_LINK_PARA_STRU;

/*定义VXI设备连接时的响应报文内容存储的结构体*/
typedef struct
{
    DEVICE_ERROR_CODE error;
    DEVICE_LINK       id;
    unsigned short    port;
    VXI_UINT32        recv_size;
}DEVICE_LINK_RESP_STRU;

/*定义VXI设备写操作参数存储的结构体*/
typedef struct
{
    DEVICE_LINK         id;
    VXI_UINT32          io_timeout;
    VXI_UINT32          lock_timeout;
    DEVICE_FLAGS        flags;
    struct
    {
        u_int           data_len;
        char           *data_val;
    }data;
}DEVICE_WRITE_PARA_STRU;

/*定义VXI设备写操作的响应报文参数结构体*/
typedef struct
{
    DEVICE_ERROR_CODE  error;
    VXI_UINT32         size;
}DEVICE_WRITE_RESP_STRU;

/*定义VXI设备读操作参数存储的结构体*/
typedef struct
{
    DEVICE_LINK         id;
    VXI_UINT32          size;
    VXI_UINT32          io_timeout;
    VXI_UINT32          lock_timeout;
    DEVICE_FLAGS        flags;
    char                termChar;
}DEVICE_READ_PARA_STRU;

/*定义VXI设备读操作响应报文参数的结构体*/
typedef struct
{
    DEVICE_ERROR_CODE   error;
    VXI_INT32           reason;
    struct
    {
        u_int           data_len;
        char           *data_val;
    }data;
}DEVICE_READ_RESP_STRU;

/*定义VXI设备的*STB?命令返回参数结构体*/
typedef struct
{
    DEVICE_ERROR_CODE   error;
    unsigned char       stb;
}DEVICE_STB_RESP_STRU;

/*定义VXI设备通用参数结构体*/
typedef struct
{
    DEVICE_LINK         id;
    DEVICE_FLAGS        flags;
    VXI_UINT32          lock_timeout;
    VXI_UINT32          io_timeout;
}DEVICE_GENERIC_PARA_STRU;

/*定义VXI设备锁定参数结构体*/
typedef struct
{
    DEVICE_LINK         id;
    DEVICE_FLAGS        flags;
    VXI_UINT32          lock_timeout;
}DEVICE_LOCK_PARA_STRU;

/*定义请求VXI设备连接远程服务器的参数结构体*/
typedef struct
{
    VXI_UINT32          hostAddr;
    unsigned short      hostPort;
    VXI_UINT32          progNum;
    VXI_UINT32          progVers;
    DEVICE_FAMILY_EM    progFamily;
}DEVICE_REMOTE_PARA_STRU;

/*定义设置VXI设备远程连接使能或关闭的参数结构体*/
typedef struct
{
    DEVICE_LINK         id;
    bool_t              enable;
    struct
    {
        u_int           handle_len;
        char           *handle_data;
    }handle;
}DEVICE_ENABLE_SRQ_PARA_STRU;

/*定义VXI设备执行do cmd 命令参数结构体*/
typedef struct
{
    DEVICE_LINK         id;
    DEVICE_FLAGS        flags;
    VXI_UINT32          io_timeout;
    VXI_UINT32          lock_timeout;
    VXI_INT32           cmd;
    bool_t              network_order;
    VXI_INT32           datasize;
    struct
    {
        u_int           data_in_len;
        char           *data_in_data;
    }data_in;
}DEVICE_DOCMD_PARA_STRU;

/*定义VXI设备DOCMD命令响应数据结构体*/
typedef struct
{
    DEVICE_ERROR_CODE   error;
    struct
    {
        u_int           data_out_len;
        char           *data_out_data;
    }data_out;
}DEVICE_DOCMD_RESP_STRU;

/*定义VXI设备执行中断请求的参数结构体*/
typedef struct
{
    struct
    {
        u_int           handle_len;
        char           *handle_data;
    }handle;
}DEVICE_SRQ_PARA_STRU;

/*
 * 定义VXI设备进行连接时的连接参数的获取
*/

extern bool_t vxi_device_link(XDR *pstXdrs , DEVICE_LINK *pLinkPara);

/*
 * 定义VXI设备的错误获取
*/
extern bool_t vxi_device_error( XDR *pstXdrs , DEVICE_ERROR_STRU *pstError);

/*
 * 获取VXI设备建立连接时的参数
*/
extern bool_t vxi_device_link_para( XDR *pstXdrs , DEVICE_LINK_PARA_STRU *pstLinkPara);

/*
 * 处理VXI设备的连接响应报文
*/
extern bool_t vxi_device_link_resp( XDR *pstXdrs , DEVICE_LINK_RESP_STRU *pstResp );

/*
 * 处理VXI设备的写操作参数
*/
extern bool_t vxi_device_write_para( XDR *pstXdrs , DEVICE_WRITE_PARA_STRU *pstWritePara);

/*
 * 定义VXI设备写操作的响应处理
*/
extern bool_t vxi_device_write_resp( XDR *pstXdrs , DEVICE_WRITE_RESP_STRU *pstWriteResp);

/*
 * 定义VXI设备读请求参数的获取
*/
extern bool_t  vxi_device_read_para( XDR *pstXdrs , DEVICE_READ_PARA_STRU *pstReadPara);

/*
 * 定义VXI设备读操作的响应报文组织
*/
extern bool_t vxi_device_read_resp( XDR *pstXdrs , DEVICE_READ_RESP_STRU *pstReadResp );

/*
 * 定义VXI设备的*STB?命令的处理接口
*/
extern bool_t vxi_device_stb_resp( XDR *pstXdrs , DEVICE_STB_RESP_STRU *pstStbResp );

/*
 * 定义通用的VXI设备命令参数获取接口
*/
extern bool_t vxi_device_generic_para( XDR *pstXdrs , DEVICE_GENERIC_PARA_STRU * pstGenPara );

/*
 * 定义VXI设备锁定命令的参数获取接口
*/
extern bool_t vxi_device_lock_para( XDR *pstXdrs , DEVICE_LOCK_PARA_STRU *pstLockPara);

/*
 * 定义请求VXI设备进行远程连接的参数获取
*/
extern bool_t vxi_device_remote_para( XDR *pstXdrs , DEVICE_REMOTE_PARA_STRU *pstRemotePara );

/*
 * 定义VXI设备打开或者关闭远程请求的参数获取函数
*/
extern bool_t  vxi_device_enable_srq_para( XDR *pstXdrs , DEVICE_ENABLE_SRQ_PARA_STRU *pstEnableSrqPara );

/*
 * 定义DOCMD命令参数的获取函数
*/
extern bool_t vxi_device_docmd_para( XDR *pstXdrs , DEVICE_DOCMD_PARA_STRU *pstDoCmdPara );

/*
 * 填充DOCMD命令对应的响应数据的函数
*/
extern bool_t vxi_device_docmd_resp( XDR *pstXdrs , DEVICE_DOCMD_RESP_STRU *pstDoCmdResp );

/*
 * 定义获取中断请求参数的获取函数
*/
extern bool_t vxi_device_srq_para( XDR *pstXdrs , DEVICE_SRQ_PARA_STRU *pstSrqPara);

#ifdef __cplusplus
}
#endif
#endif // XDR_VXI_H
