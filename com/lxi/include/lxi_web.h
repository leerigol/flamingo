#ifndef LXI_WEB_H
#define LXI_WEB_H

#include "lxi_int.h"

#ifdef __cplusplus
extern "C" {
#endif
//  根据HTTP5的规定，定义HTTP服务的解析标志
#define  LXI_WEB_REGISTER_FAIL             -1
#define  LXI_WEB_REGISTER_SUCCESS           0
#define  LXI_WEB_SEEK_FAIL                 -1
#define  LXI_WEB_SEEK_SUCCESS               0
#define  LXI_WEB_REQUEST_IS_GET             0
#define  LXI_WEB_REQUEST_IS_POST            1
#define  LXI_WEB_OPRATION_UNSUPPORT        -1
#define  LXI_WEB_OPRATION_APPLY             0
#define  LXI_WEB_OPRATION_DEFAULT           1
#define  LXI_WEB_OPRATION_SUBMIT            2
#define  LXI_WEB_VERIFY_FAIL                0
#define  LXI_WEB_VERIFY_SUCCESS             1
//  当前LXI模块支持的文本类型个数
#define  LXI_WEB_SUPPORT_TEXT_TYPE_NUM      5
//  当前LXI模块支持的文本类型描述符的最大长度
#define  LXI_WEB_TEXT_DESCRIBE_SIZE         50
//  定义WEB页面节点标志的最大长度
#define  LXI_WEB_NOTE_STRING_SIZE           20
//  定义WEB服务允许的最大连接请求数
#define  LXI_WEB_SUPPORT_MAX_SOCKET_CONNECT_NUM 10

//  定义HTTP网线中的特殊字符
#define  LXI_WEB_FIELD_NULL                 '\0'
#define  LXI_WEB_FIELD_EQUAL                '='
#define  LXI_WEB_FIELD_AND                  '&'
#define  LXI_WEB_FIELD_SPACE                ' '

//  定义浏览器请求命令的错误响应报文
#define  LXI_WEB_ERR_IS_OK                  "HTTP/1.0 200 OK\r\nContent-type: %s\r\n\r\n"
#define  LXI_WEB_ERR_NOT_IMPLEMENTED        "HTTP/1.0 501 Method Not Implemented\r\nContent-Type: text/html\r\n\r\n"
#define  LXI_WEB_ERR_NOT_FOUND              "HTTP/1.0 404 NOT FOUND\r\nContent-Type: text/html\r\n\r\n"
#define  LXI_WEB_ERR_SERVER_ERROR           "HTTP/1.0 500 Internal Server Error\r\nContent-Type: text/html\r\n\r\n"
#define  LXI_WEB_ERR_BAD_REQUEST            "HTTP/1.0 400 BAD REQUEST\r\nContent-Type: text/html\r\n\r\n"
#define  LXI_WEB_ERR_UNAUTHORIZED           "HTTP/1.0 401 Unauthorized\r\nServer: rigol\r\nWWW-Authenticate: Basic realm=\"Default username is admin and password is rigol!\"\r\n"

#define  LXI_WEB_RELOCATION                 "HTTP/1.0 303 See Other\r\nLocation: http://%s%s\r\nContent-length: 0\r\nServer: rigol\r\n\r\n"
#define  LXI_WEB_CLEAR_CACHE                "Pragma: no-cache\r\nCache-Control: no-cache\r\n"

/*
 * 定义网页类型枚举值
*/
typedef enum
{
    //  页面
    LXI_PAGE_TYPE_WELCOME           = 0     ,   //  欢迎页面,主页
    LXI_PAGE_TYPE_NETWORK_STATE             ,   //  网络状态显示页面
    LXI_PAGE_TYPE_NETWORK_SETTING           ,   //  网络配置页面
    LXI_PAGE_TYPE_HELP                      ,   //  帮助页面
    LXI_PAGE_TYPE_PASSWORG_VALIDATE         ,   //  密码安全验证页面
    LXI_PAGE_TYPE_PASSWORG_CHANGE           ,   //  密码设置页面
    LXI_PAGE_TYPE_PASSWORG_WRONG            ,   //  密码验证错误页面
    LXI_PAGE_TYPE_PASSWORG_SET_SUCCESS      ,   //  密码设置成功页面
    LXI_PAGE_TYPE_PASSWORG_SET_WRONG        ,   //  密码设置失败页面
    LXI_PAGE_TYPE_WEB_CONTROL               ,   //  Web Control页面
    LXI_PAGE_TYPE_XML                       ,   //  XML页面
    LXI_PAGE_TYPE_XSD                       ,   //  XML页面的架构
    //  CSS
    LXI_PAGE_TYPE_CSS_DEFUALT               ,   //  默认的CSS文件
    LXI_PAGE_TYPE_CSS_WELCOME               ,   //  欢迎页面的CSS文件
    LXI_PAGE_TYPE_CSS_HELP                  ,   //  帮助页面的CSS文件
    LXI_PAGE_TYPE_CSS_NETWORK_STATUS        ,   //  网络状态页面的CSS文件
    LXI_PAGE_TYPE_CSS_NETWORK_SETTING       ,   //  网络配置页面的CSS文件
    LXI_PAGE_TYPE_CSS_PASSWORG              ,   //  密码页面的CSS文件
    LXI_PAGE_TYPE_CSS_WEB_CONTROL           ,   //  Web Control 页面的CSS文件
    //  图片
    LXI_PAGE_TYPE_IMAGES_WELCOME            ,   //  欢迎页面选中图标
    LXI_PAGE_TYPE_IMAGES_WELCOME_NO         ,   //  欢迎页面未选中图标
    LXI_PAGE_TYPE_IMAGES_NETWORK_STATE      ,   //  网络状态页面选中图标
    LXI_PAGE_TYPE_IMAGES_NETWORK_STATE_NO   ,   //  网络状态页面未选中图标
    LXI_PAGE_TYPE_IMAGES_NETWORK_SETTING    ,   //  网络配置页面选中图标
    LXI_PAGE_TYPE_IMAGES_NETWORK_SETTING_NO ,   //  网络配置页面未选中图标
    LXI_PAGE_TYPE_IMAGES_HELP               ,   //  帮助页面选中图标
    LXI_PAGE_TYPE_IMAGES_HELP_NO            ,   //  帮助页面未选中图标
    LXI_PAGE_TYPE_IMAGES_PASSWORD           ,   //  密码设置页面选中图标
    LXI_PAGE_TYPE_IMAGES_PASSWORD_NO        ,   //  密码设置页面未选中图标
    LXI_PAGE_TYPE_IMAGES_WEB_CONTROL        ,   //  Web Control选中图标
    LXI_PAGE_TYPE_IMAGES_WEB_CONTROL_NO     ,   //  Web Control未选中图标
    LXI_PAGE_TYPE_IMAGES_LXI_LOGO           ,   //  LXI图标
    LXI_PAGE_TYPE_IMAGES_MANUFACTRUE_LOGO   ,   //  生产厂商LOGO图标
    LXI_PAGE_TYPE_IMAGES_INSTRUMENT_LOGO    ,   //  仪器LOGO图标
    //  可扩充页面
    LXI_PAGE_TYPE_RESVER                        //  保留
}LXI_WEB_PAGE_TYPE_EM;

/*
*定义网页服务socket的建立信息管理结构体
*/
typedef struct
{
    s32     sockfd;
    s32     used;
}LXI_WEB_SOCKET_CONNECT_INFO;
/*
 * 定义LXI模块的网页数据管理的结构体
*/
typedef struct
{
    LXI_WEB_PAGE_TYPE_EM        type            ;   //  网页类型
    s8                         *url             ;   //  网页的路由地址
    s8                         *page            ;   //  网页在内存中的地址
    u32                         size            ;   //  网页的大小
    void                      (*call)(void*)    ;   //  网页刷新函数接口
}LXI_WEB_PAGE_INFO_STRU;

/*
 * 定义浏览器请求的网页文本类型枚举值
*/
typedef enum
{
    LXI_WEB_PAGE_TEXT_TYPE_HTML         =   0   ,   //  HTML类网页请求
    LXI_WEB_PAGE_TEXT_TYPE_XML                  ,   //  XML类网页请求
    LXI_WEB_PAGE_TEXT_TYPE_JPEG                 ,   //  图片请求
    LXI_WEB_PAGE_TEXT_TYPE_CSS                  ,   //  CSS架构请求
    LXI_WEB_PAGE_TEXT_TYPE_JAVA                     //  JAVA小程序
}LXI_WEB_PAGE_TEXT_TYPE_EM;

/*
 *  浏览器的CGI命令类型
*/
typedef enum
{
    LXI_WEB_CGI_TYPE_CONFIG     =   0   ,   //  网络配置操作
    LXI_WEB_CGI_TYPE_IDENTIFY           ,   //  仪器识别操作
    LXI_WEB_CGI_TYPE_VERIFT_PASSWORD    ,   //  密码验证
    LXI_WEB_CGI_TYPE_SET_PASSWORD           //  密码设置操作
}LXI_WEB_CGI_TYPE_EM;

/*
 * 定义网络配置页面中节点的枚举值
*/
typedef enum
{
    LXI_WEB_NOTE_TYPE_APPLY                 = 0 ,
    LXI_WEB_NOTE_TYPE_FACTORY_SETTING           ,
    LXI_WEB_NOTE_TYPE_HOST_NAME                 ,
    LXI_WEB_NOTE_TYPE_SERVICE_NAME              ,
    LXI_WEB_NOTE_TYPE_MDNS_ENABLE               ,
    LXI_WEB_NOTE_TYPE_DHCP_ENABLE               ,
    LXI_WEB_NOTE_TYPE_AUTOIP_ENABLE             ,
    LXI_WEB_NOTE_TYPE_MANUALIP_ENABLE           ,
    LXI_WEB_NOTE_TYPE_IPADDR1                   ,
    LXI_WEB_NOTE_TYPE_IPADDR2                   ,
    LXI_WEB_NOTE_TYPE_IPADDR3                   ,
    LXI_WEB_NOTE_TYPE_IPADDR4                   ,
    LXI_WEB_NOTE_TYPE_NETMASK1                  ,
    LXI_WEB_NOTE_TYPE_NETMASK2                  ,
    LXI_WEB_NOTE_TYPE_NETMASK3                  ,
    LXI_WEB_NOTE_TYPE_NETMASK4                  ,
    LXI_WEB_NOTE_TYPE_GATEWAY1                  ,
    LXI_WEB_NOTE_TYPE_GATEWAY2                  ,
    LXI_WEB_NOTE_TYPE_GATEWAY3                  ,
    LXI_WEB_NOTE_TYPE_GATEWAY4                  ,
    LXI_WEB_NOTE_TYPE_DNS1                      ,
    LXI_WEB_NOTE_TYPE_DNS2                      ,
    LXI_WEB_NOTE_TYPE_DNS3                      ,
    LXI_WEB_NOTE_TYPE_DNS4                      ,
    LXI_WEB_NOTE_TYPE_ON                        ,
    LXI_WEB_NOTE_TYPE_IDENTIFICATION            ,
    LXI_WEB_NOTE_TYPE_SUBMIT                    ,
    LXI_WEB_NOTE_TYPE_PASS                      ,
    LXI_WEB_NOTE_TYPE_PASS0                     ,
    LXI_WEB_NOTE_TYPE_PASS1                     ,
    LXI_WEB_NOTE_TYPE_PASS2                     ,
    LXI_WEB_NOTE_TYPE_BUFF
}LXI_WEB_NOTE_TYPE_EM;

/*LXI模块Web页面数据*/
extern LXI_WEB_PAGE_INFO_STRU  m_astWebPageInfo[LXI_PAGE_TYPE_RESVER];

/*
 * LXI模块Web服务线程
*/
extern void *lxi_web_service_thread( void *pInitPara );

#ifdef __cplusplus
}
#endif
#endif // LXI_WEB_H
