#ifndef LXI_MDNS_H
#define LXI_MDNS_H

#include "lxi_int.h"

#ifdef __cplusplus
extern "C" {
#endif
/*定义mDNS处理的报文最大长度*/
#define     LXI_MDNS_PACKET_MAX_SIZE            1500

/*根据mDNS标准，定义mDNS服务的网络端口号*/
#define     LXI_MDNS_SERVICE_PORT               5353

/*根据mDNS标准，定义mDNS服务的IP地址*/
#define     LXI_MDNS_SERVICE_ADDR               "224.0.0.251"

/*定义mDNS事件队列的队宽*/
#define     LXI_MDNS_EVENT_QUEUE_MAX_SIZE       50

/*定义mDNS对网络状态监控的周期,单位:us*/
#define     LXI_MDNS_MONITOR_PERIOD             5000

/*定义mDNS对网络状态监控的最大时间*/
#define     LXI_MDNS_MONITOR_MAX_TIME           0xFFFFFFFF

/*定义mDNS报文头的长度*/
#define     LXI_MDNS_PACKET_HEADER_SIZE         12

/*定义mDNS使用的IP地址长度*/
#define     LXI_MDNS_IP_ADDR_LEN                4

/*定义mDNS的报文解析最大问题查询数*/
#define     LXI_MDNS_MAX_QUESTIONS_NUM          10

/*定义mDNS的报文解析中每个NAME中的Lable的最大长度*/
#define     LXI_MDNS_LABEL_MAX_SIZE             64

/*定义mDNS的报文解析最大的声明数*/
#define     LXI_MDNS_MAX_RESPONSE_NUM           20

/* 字节翻转的宏定义 */
/* UINT16类型数据的翻转 */
#define LXI_REV_WORD(x) ((u16)(((u16)(x)>>8)|((u16)(x)<<8)))
/* UINT32类型数据的翻转 */
#define LXI_REV_DWORD(x) ((u32)((u32)(LXI_REV_WORD(x)<<16)|(u32)(LXI_REV_WORD((x)>>16))))

/* 判断NAME格式是否为压缩格式 */
#define DNS_IS_NAME_COMPRESS(name) ( ( ( (name)&0xC0 ) == 0xC0 ) ? 1 : 0 )

/* 压缩格式下NAME的位置偏移  */
#define DNS_NAME_OFFSET_IN_COMPRESS(name_compress) (((u16)(name_compress))&0x3FFF)

/*定义mDNS服务的阶段*/
 typedef enum
{
    LXI_MDNS_STATE_HOST_PROBE = 0   ,   //  主机名探测阶段
    LXI_MDNS_STATE_SERVICE_PROBE    ,   //  服务名探测阶段
    LXI_MDNS_STATE_SUCCESS          ,   //  探测成功
    LXI_MDNS_STATE_HOST_CONFLICT    ,   //  主机名冲突
    LXI_MDNS_STATE_SERVICE_CONFLICT     //  服务名冲突
}LXI_MDNS_STATE_EM;


/*定义MDNS事件码值*/
typedef enum
{
    LXI_MDNS_EVENT_HOST_QUERY           =  0                ,   //  主机名QU探测
    LXI_MDNS_EVENT_HOST_QUERY_M                             ,   //  主机名QM探测
    LXI_MDNS_EVENT_HOST_RESPONSE                            ,   //  主机名声明
    LXI_MDNS_EVENT_HTTP_SERVICE_QUERY                       ,   //  HTTP服务QU探测
    LXI_MDNS_EVENT_HTTP_SERVICE_QUERY_M                     ,   //  HTTP服务QM探测
    LXI_MDNS_EVENT_HTTP_SERVICE_RESPONSE                    ,   //  HTTP服务声明
    LXI_MDNS_EVENT_HTTP_SERVICE_RESPONSE_HOST               ,   //  主机名支持HTTP服务的声明
    LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE                ,   //  HTTP服务的TXT文件声明
    LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE_WITH_PTR       ,   //  HTTP服务的TXT文件与PTR属性的声明
    LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE_WITH_PTR_SVC   ,   //  HTTP服务的TXT文件、SVC、PTR属性的声明
    LXI_MDNS_EVENT_HTTP_SERVICE_ALL_WITH_IP_RESPONSE        ,   //  HTTP服务的TXT文件、SVC、PTR属性以及IP地址绑定的声明
    LXI_MDNS_EVENT_LXI_SERVICE_QUERY                        ,   //  LXI服务QU探测
    LXI_MDNS_EVENT_LXI_SERVICE_QUERY_M                      ,   //  LXI服务QM探测
    LXI_MDNS_EVENT_LXI_SERVICE_RESPONSE_WITH_TXT            ,   //  LXI服务的TXT声明
    LXI_MDNS_EVENT_LXI_SERVICE_RESPONSE_WITH_HOST           ,   //  LXI服务绑定主机名的声明
    LXI_MDNS_EVENT_SCPI_SERVICE_QUERY                       ,   //  SCPI服务QU探测
    LXI_MDNS_EVENT_SCPI_SERVICE_QUERY_M                     ,   //  SCPI服务QM探测
    LXI_MDNS_EVENT_SCPI_SERVICE_RESPONSE_WITH_TXT           ,   //  SCPI服务的TXT声明
    LXI_MDNS_EVENT_VXI_SERVICE_QUERY                        ,   //  VXI-11服务QU探测
    LXI_MDNS_EVENT_VXI_SERVICE_QUERY_M                      ,   //  VXI-11服务QM探测
    LXI_MDNS_EVENT_VXI_SERVICE_RESPONSE_WITH_TXT            ,   //  VXI-11服务的TXT声明
    LXI_MDNS_EVENT_VXI_SERVICE_RESPONSE_WITH_HOST           ,   //  VXI-11服务绑定主机名的声明
    LXI_MDNS_EVENT_HTTP_LXI_SCPI_RESPONSE                   ,   //  HTTP、LXI、SCPI服务的声明
    LXI_MDNS_EVENT_SCPI_VXI_RESPONSE                        ,   //  SCPI、LXI服务的声明
    LXI_MDNS_EVENT_TIMER                                    ,   //  定时声明报文
    LXI_MDNS_EVENT_PACKET_PARSE                                 //  mDNS报文解析事件
}LXI_MDNS_EVENT_EM;

typedef struct
{
    u32                         use;
    LXI_MDNS_EVENT_EM           code;
}LXI_MDNS_EVENT_NODE_STRU;

typedef struct
{
    u32                         len;
    s8                          buff[LXI_MDNS_PACKET_MAX_SIZE];
}LXI_MDNS_BUFF_NODE_STRU;

/*定义mDNS事件队列管理的结构体*/
typedef struct
{
    u32                         front                                   ;   //  队列首地址
    u32                         rear                                    ;   //  队列尾地址
    u32                         full                                    ;   //  队列满标志
    lxi_sem_t                   wr_en                                   ;   //  队列写权限控制
    lxi_sem_t                   rd_en                                   ;   //  队列读权限控制
    LXI_MDNS_EVENT_NODE_STRU    event[LXI_MDNS_EVENT_QUEUE_MAX_SIZE]    ;   //  队列
    LXI_MDNS_BUFF_NODE_STRU     data[LXI_MDNS_EVENT_QUEUE_MAX_SIZE]     ;   //  数据队列
}LXI_MDNS_EVENT_QUEUE_STRU;

/*根据DNS标准中规定的DNS报文格式定义响应数据的结构体*/
typedef struct
{
    s8           *name;
    u16           type, Class;
    u32           ttl;
    u16           rdlength;
    s8           *rdata;
    union
    {
        struct { u32 ip; s8 *name; } a;
        struct { u8 *name; } ns;
        struct { u8 *name; } cname;
        struct { u8 *name; } ptr;
        struct { u16 priority, weight, port; u8 *name; } srv;
    } known;
}LXI_MDNS_RESOURCE_STRU;
/*根据DNS标准中规定的DNS报文格式定义问题数据的结构体*/
typedef struct
{
    u8     *name;
    u16     type;
    u16     Class;
}LXI_MDNS_QUESTION_STRU;

/*根据DNS标准中规定的DNS报文格式定义整体数据包的结构体*/
typedef struct
{
    u16 id;
    struct { u16 rcode:4,z:3,ra:1,rd:1,tc:1,aa:1,opcode:4,qr:1; } header;
    u16 qdcount, ancount, nscount, arcount;
    LXI_MDNS_QUESTION_STRU *qd;
    LXI_MDNS_RESOURCE_STRU *an, *ns, *ar;
}LXI_MDNS_MESSAGE_STRU;

/*根据mDNS的发送数据格式定义发送数据结构体*/
typedef struct
{
    u16     id;
    u16     flags;
    u16     qucount;
    u16     ancount;
    u16     aucount;
    u16     adcount;
    s8      buff[LXI_MDNS_PACKET_MAX_SIZE - LXI_MDNS_PACKET_HEADER_SIZE];
}LXI_MDNS_PACKET_HEADER;

/*根据DNS标准中规定,定义查询类型*/
typedef enum
{
    LXI_MDNS_QUERY_HOST_ADDR = 1,       //1.查询主机名
    LXI_MDNS_QUERY_SERVER_NAME,         //2.查询服务名
    LXI_MDNS_QUERY_MAIL_DESTINATION,    //3.查询邮箱目的地址，已过时，不再使用
    LXI_MDNS_QUERY_MAIL_FORWARDER,      //4.查询邮箱代收服务器，已过时，不再使用
    LXI_MDNS_QUERY_CHAME,               //5.查询一个规范的别名
    LXI_MDNS_QUERY_SOA,                 //6.标识使用的规范命名
    LXI_MDNS_QUERY_MB,                  //7.查询邮箱的动态域名
    LXI_MDNS_QUERY_MG,                  //8.查询邮箱的成员组织
    LXI_MDNS_QUERY_MR,                  //9.设置邮箱新的域名
    LXI_MDNS_QUERY_NULL,                //10.空的邮箱操作
    LXI_MDNS_QUERY_WKS,                 //11.一个众所周知的服务描述
    LXI_MDNS_QUERY_PTR,                 //12.域名查询
    LXI_MDNS_QUERY_HINFO,               //13.主机信息查询
    LXI_MDNS_QUERY_MINFO,               //14.邮箱信息查询
    LXI_MDNS_QUERY_MX,                  //15.邮件通信
    LXI_MDNS_QUERY_TXT,                 //16.查询文本字符串
    LXI_MDNS_QUERY_AAAA = 28,           //ipv6查询
    LXI_MDNS_QUERY_SRV = 33,            //
    LXI_MDNS_QUERY_AXFR = 252,          //请求广播传输
    LXI_MDNS_QUERY_MAILB,               //一个依赖于邮箱的传输
    LXI_MDNS_QUERY_MAILA,               //邮件代理RRS传输
    LXI_MDNS_QUERY_ALL                  //查询所有的记录信息
}LXI_MDNS_QUERY_EM;
/*根据DNS标准中规定,定义MDNS报文类型*/
typedef enum
{
    LXI_MDNS_PACKET_HOST,               //生成主机名报文
    LXI_MDNS_PACKET_SERVER              //生成服务名报文
}LXI_MDNS_PACKET_EM;

typedef enum
{
    QR_QUERY = 0,
    QR_RESPONSE,
    QR_INVALID
}DNS_HEADER_QR_E;

typedef enum
{
    OPCODE_QUERY = 0,
    OPCODE_IQUERY,
    OPCODE_STATUS
}DNS_HEADER_OPCODE_E;

typedef enum
{
    AA_INVALID = 0,
    AA_VALID
}DNS_HEADER_AA_E;


/*
 * LXI-MDNS功能启动接口定义
*/
extern void lxi_mdns_start();
/*
 *添加路由地址
*/
extern void lxi_net_route_add(s32 s32GateWay , s32 s32SubMask );

#ifdef __cplusplus
}
#endif

#endif // LXI_MDNS_H
