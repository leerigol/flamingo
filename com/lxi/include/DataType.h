/*********************************************************************
 *                   普源精电版权所有
 *********************************************************************
 *头文件名:datatype.h
 *功能描述:
 *作        者:haohu
 * 创建日期:2017.01.16
 * 版        本:Ver0.1
 ********************************************************************/
#ifndef DATATYPE_H
#define DATATYPE_H

#ifdef __cplusplus
#include <QMap>

#ifndef MAP
typedef QMap<unsigned int ,long long> Mapdef;  //槽中不能使用泛型,在这用typedef定义一个新类型
#endif

#endif
#ifdef __cplusplus
extern "C" {
#endif

#define   _LINUX_OS_

#define  S64_UP_LIMIT            0x7FFFFFFFFFFFFFFF
#define  S32_UP_LIMIT            0x7FFFFFFF
#define  S16_UP_LIMIT            0x7FFF
#define  S8_UP_LIMIT             0x7F
#define  U64_UP_LIMIT            0xFFFFFFFFFFFFFFFF
#define  U32_UP_LIMIT            0xFFFFFFFF
#define  U16_UP_LIMIT            0xFFFF
#define  U8_UP_LIMIT             0xFF

#ifdef _LINUX_OS_

#include <linux/types.h>




#ifndef u32
typedef  unsigned int   u32;
#endif

#ifndef u16
typedef unsigned short   u16;
#endif

#ifndef u8
typedef unsigned char      u8; /* 无符号字符型 */
#endif

#ifndef s32
typedef int      s32;   /* 有符号整型 */
#endif
#ifndef s16
typedef  short    s16;
#endif

#ifndef s8
typedef  char      s8;  /* 有符号字符型 */
#endif

#ifndef f32
typedef float  f32;
#endif

#ifndef f64
typedef double    f64;   /* 双精度浮点 */
#endif

#ifndef u64
//typedef unsigned long   u64;
typedef unsigned long long   u64;
#endif

#ifndef s64
//typedef long      s64;
typedef long long      s64;
#endif

#ifndef NULL
#define NULL      0L
#endif

#ifndef true
#define true   1
#endif

#ifndef false
#define false 0
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif // DATATYPE

