/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: lxi_int.h
  功能描述: LXI模块内部使用的头文件，进行全局变量的声明

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-02

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-02          1.0        initial

*******************************************************************************/
#ifndef LXI_INT_H
#define LXI_INT_H

#include    "DataType.h"
#include    "lxi_api.h"
#include    "lxi_arch.h"

#ifdef __cplusplus
extern "C" {
#endif

/*定义LXI模块的配置参数变量*/
extern LXI_PARAMETER_INFO_STRU     m_stLxiConfPara;

/*定义LXI模块的初始化参数变量*/
extern LXI_INIT_PARAMETER_STRU     m_stLxiInitPara;

/*定义当前网络的状态*/
extern LXI_STATUS_EM               m_emLxiCurrState;

/*记录当前网络状态是否发生变化*/
extern s8                          m_s8NetStateIsChange;

/*定义完成声明的mDNS主机名和服务名*/
extern s8                          m_as8DHostName[LXI_MDNS_NAME_MAX_LEN];
extern s8                          m_as8DServiceName[LXI_MDNS_NAME_MAX_LEN];

/*定义仪器相关信息*/
extern s8                          m_as8Manufacture[LXI_DEVICE_PRODUCE_INFO_MAX_LEN];
extern s8                          m_as8InstrumentModel[LXI_DEVICE_PRODUCE_INFO_MAX_LEN];
extern s8                          m_as8SerialNum[LXI_DEVICE_PRODUCE_INFO_MAX_LEN];
extern s8                          m_as8FirmwareVersion[LXI_DEVICE_PRODUCE_INFO_MAX_LEN];
extern s8                          m_as8InstrumentSeries[LXI_DEVICE_PRODUCE_INFO_MAX_LEN];
extern s8                          m_as8UsbVisaAddr[LXI_DEVICE_PRODUCE_INFO_MAX_LEN];

/*定义Web Service服务密码*/
extern s8                          m_as8WebPassword[LXI_HTTP_SERVICE_PASSWORD_MAX_LEN];

/*记录Socket端口号*/
extern u32                         m_u32SocketPort ;

/*定义记录DHCP服务器IP地址的变量*/
extern s32                         m_s32LxiDhcpServer;
/*定义当前网络环境的域名*/
extern s8                          m_as8CurrDomainName[LXI_DOMAIN_NAME_MAX_LEN];

/*定义当前网卡自适应后的网络带宽*/
extern LXI_NETWORK_SPEED_EM        m_emCurrNetSpeed;

/*定义当前网卡自适应后的工作模式*/
extern LXI_NETWORK_WORK_MODE_EM    m_emCurrNetWorkMode; 

/*定义LXI摸块调试消息的打印接口*/
#define LXI_DEBUG(x)               //printf(x);

#ifdef __cplusplus
}
#endif

#endif // LXI_INT_H
