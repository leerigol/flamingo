/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: lxi_vxi.h
  功能描述: LXI模块Vxi-11功能的函数声明

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-02

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-02          1.0        initial

*******************************************************************************/
#ifndef LXI_VXI_H
#define LXI_VXI_H

#include    "lxi_int.h"

#ifdef __cplusplus
extern "C" {
#endif

/*定义VXI调试开关,受long在64位系统为8字节影响*/
#define     VXI_DEBUG                   1

/*定义Vxi-11 Core 的端口号*/
#define     LXI_VXI_CORE_PORT           15626

/*定义VXI-11 Core的RPC服务项目号*/
#define     LXI_VXI_CORE_PROGRAM        0x000607AF

/*定义VXI-11 Core的RPC服务版本号*/
#define     LXI_VXI_CORE_VERSION        1

/*定义Vxi-11 Async 的端口号*/
#define     LXI_VXI_ASYNC_PORT          1016

/*定义VXI-11 Async的RPC服务项目号*/
#define     LXI_VXI_ASYNC_PROGRAM       0x000607B0

/*定义VXI-11 Async的RPC服务版本号*/
#define     LXI_VXI_ASYNC_VERSION       1

/*定义VXI-11 Intr的端口号*/
#define     LXI_VXI_INTR_PORT           1017

/*定义VXI-11 Intr的RPC服务项目号*/
#define     LXI_VXI_INTR_PROGRAM        0x000607B1

/*定义VXI-11 Intr的RPC服务版本号*/
#define     LXI_VXI_INTR_VERSION        1

/*定义Vxi-11 Core的数据接收缓冲区大小*/
#define     LXI_VXI_CORE_RECV_BUFF_SIZE LXI_MODULE_BUFF_MAX_LEN

/*定义Vxi-11 Core的数据发送缓冲区大小*/
#define     LXI_VXI_CORE_SEND_BUFF_SIZE LXI_MODULE_BUFF_MAX_LEN

/*定义VXI设备最大客户端数量*/
#define     LXI_VXI_MAX_CILENT_NUM      10

/*根据VXI-11协议标准定义错误码值*/
typedef enum
{
    LXI_VXI_NO_ERROR                      = 0   ,
    LXI_VXI_SYNTAX_ERROR                  = 1   ,
    LXI_VXI_DEVICE_NOT_ACCESSIBLE         = 3   ,
    LXI_VXI_INVALID_LINK_IDENTIFIER       = 4   ,
    LXI_VXI_PARAMETER_ERROR               = 5   ,
    LXI_VXI_CHANNEL_NOT_ESTABLISHED       = 6   ,
    LXI_VXI_OPERATION_NOT_SUPPORTED       = 8   ,
    LXI_VXI_OUT_OF_RESOURCE               = 9   ,
    LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK = 11  ,
    LXI_VXI_NO_LOCK_HELD_BY_THIS_LINK     = 12  ,
    LXI_VXI_IO_TIMEOUT                    = 15  ,
    LXI_VXI_IO_ERROR                      = 17  ,
    LXI_VXI_INVALID_ADDRESS               = 21  ,
    LXI_VXI_ABORT                         = 23  ,
    LXI_VXI_CHANNEL_ALREADY_ESTABLISHED   = 29
}LXI_VXI_ERROR_CODE_EM;

/*根据VXI-11协议标准，定义VXI-11 Core 服务的命令码值*/
typedef enum
{
    LXI_VXI_CMD_NULL                        =   0   ,
    LXI_VXI_CMD_ABORT                       =   1   ,
    LXI_VXI_CMD_CREATE_LINK                 =   10  ,
    LXI_VXI_CMD_WRITE                       =   11  ,
    LXI_VXI_CMD_READ                        =   12  ,
    LXI_VXI_CMD_READ_STB                    =   13  ,
    LXI_VXI_CMD_TRIGGER                     =   14  ,
    LXI_VXI_CMD_CLEAR                       =   15  ,
    LXI_VXI_CMD_REMOTE                      =   16  ,
    LXI_VXI_CMD_LOCAL                       =   17  ,
    LXI_VXI_CMD_LOCK                        =   18  ,
    LXI_VXI_CMD_UNLOCK                      =   19  ,
    LXI_VXI_CMD_ENABLE_SRQ                  =   20  ,
    LXI_VXI_CMD_DO_CMD                      =   22  ,
    LXI_VXI_CMD_DESTROY_LINK                =   23  ,
    LXI_VXI_CMD_CREATE_INTR_CHAN            =   25  ,
    LXI_VXI_CMD_DESTROY_INTR_CHAN           =   26  ,
    LXI_VXI_CMD_INTR_SRQ                    =   30
}LXI_VXI_CMD_EM;

/*
 * VXI-11功能的执行函数
*/
extern void * lxi_vxi_thread( void *pInitPara );

#ifdef __cplusplus
}
#endif
#endif // LXI_VXI_H

