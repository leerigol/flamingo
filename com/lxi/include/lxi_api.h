/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: lxi_api.h
  功能描述: 用于项目组进行集成LXI使用的函数和结构体声明

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-10-24

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-10-24          1.0        initial

*******************************************************************************/
#ifndef LXI_API_H
#define LXI_API_H

#include "DataType.h"

#ifdef __cplusplus
extern "C" {
#endif

/*LXI模块对相关字符串长度的定义
*/
#define             LXI_MODULE_VERSION_MAX_LEN              12      //  LXI模块版本号长度：00.00.00.00
#define             LXI_MAC_ADDR_MAX_LEN                    6       //  MAC地址长度(16进制描述)：00：11：22：33：44：55
#define             LXI_MAC_ADDR_STRING_MAX_LEN             17      //  MAC地址长度(ASCII描述): 00:11:22:33:44:55
#define             LXI_IP_ADDR_STRING_MAX_LEN              15      //  IP地址字符化长度:255.255.255.255
#define             LXI_DOMAIN_NAME_MAX_LEN                 50      //  域名长度：Rigol.Tech
#define             LXI_MDNS_NAME_MAX_LEN                   100     //  MDNS主机名/服务名长度
#define             LXI_DEVICE_PRODUCE_INFO_MAX_LEN         50      //  LXI设备生产信息的描述字符串最大长度,包括生产厂商、设备型号、设备序列号和固件版本号
#define             LXI_HTTP_SERVICE_PASSWORD_MAX_LEN       50      //  LXI设备的HTTP服务密码的最大长度
#define             LXI_MODULE_BUFF_MAX_LEN                 4096    //  LXI模块接收和发送缓冲区的大小,SCPI资源申请时应遵循不小于该值
#define             LXI_NETWORK_ADAPTOR_MAX_NUM             10      //  当前LXI设备拥有的网络适配器最大个数
#define             LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN    20      //  当前LXI设备每个网络适配器的描述符最大长度
#define             LXI_WIRELESS_NETWORK_WIFI_NAME_LEN      100     //  LXI模块定义无线网络适配器能连接的WIFI名称的最大字节数
#define             LXI_WIRELESS_NETWORK_WIFI_MAX_NUM       50      //  LXI模块定义无线网络适配器能识别的WIFI最大数量
#define             LXI_WIRELESS_NETWORK_WIFI_PASSWORD_LEN  50      //  LXI模块定义无线网络适配器要连接的WIFI的密码长度
#define             LXI_WEB_HELP_INFO_MAX_LEN               1024    //  LXI模块支持帮助信息的最大长度

/******************************************************************************/
/*                  LXI模块集成时,提供给项目参考使用的变量和函数定义                  */
/*                      项目组必须满足如下定义的需求                               */
/******************************************************************************/

/*
 * 定义LXI模块上报UI层的消息标识值
*/
typedef enum
{
    LXI_UI_CABLE_LINK   =   0               ,   //  网线接入
    LXI_UI_CABLE_UNLINK                     ,   //  网线拔出
    LXI_UI_NO_ADAPTER                       ,   //  当前设备没有可用的网络适配器
    LXI_UI_WIRELESS_NETWORK_ADAPTER         ,   //  无线网卡能连接的WIFI扫描成功
    LXI_UI_NETWORK_INITIALIZATION           ,   //  网络配置初始化
    LXI_UI_NETWORK_SUCCESS                  ,   //  网络配置成功
    LXI_UI_INVALID_IP_ADDR                  ,   //  无效的IP地址配置
    LXI_UI_DHCP_IP_ACHIEVE_FAIL             ,   //  DHCP获取IP地址失败
    LXI_UI_DHCP_IP_LOSE                     ,   //  DHCP关闭后，提示DHCP分配的IP失效
    LXI_UI_MANUAL_IP_CONFLICT               ,   //  手动IP地址配置冲突
    LXI_UI_MDNS_HOSTNAME_CONFLICT           ,   //  mDNS主机名配置冲突
    LXI_UI_MDNS_SERVICENAME_CONFLICT        ,   //  mDNS服务名配置冲突
    LXI_UI_LXI_IDENTIFICATION_DISPLAY       ,   //  LXI设备标识显示或者关闭
    LXI_UI_DEVICE_ENTER_REMOTE              ,   //  LXI设备进入远程界面
    LXI_UI_DEVICE_ENTER_LOCAL                   //  LXI设备进入本地模式
}LXI_UI_MESSAGE_ID_EM;

/*
 * 定义LXI模块使用SCPI模块的事件请求标识值
*/
typedef enum
{
    LXI_SCPI_RESOURCE_INITIALIZATION = 0    ,   //  SCPI资源初始化
    LXI_SCPI_WRITE                          ,   //  向SCPI模块写入数据
    LXI_SCPI_READ                           ,   //  从SCPI模块读取数据
    LXI_SCPI_PARAMETER_CONFIG               ,   //  配置SCPI参数
    LXI_SCPI_STATUS_ACHIEVE                 ,   //  获取SCPI参数
    LXI_SCPI_RESOURCE_FREE                      //  SCPI资源释放
}LXI_SCPI_EVENT_ID_EM;

/*
 * 定义LXI模块配置SCPI模块的命令码值
*/
typedef enum
{
    LXI_SCPI_CONFIG_READ_LENGTH_CMD =   0   ,   //  LXI模块配置SCPI模块，LXI模块准备读取的数据长度
    LXI_SCPI_CONFIG_WAIT_FINISH                 //  LXI模块配置SCPI模块,等待SCPI模块命令处理完成并准备好数据
}LXI_SCPI_CONFIG_CMD_EM;

/*
 * 定义LXI模块从SCPI模块获取的参数类型码值
*/
typedef enum
{
    LXI_SCPI_ACHIEVE_RESPONSE_DATA_TOTAL_LENGTH =   0   ,   //  LXI模块获取SCPI模块待LXI模块读取的数据长度
    LXI_SCPI_ACHIEVE_CURRENT_INTERFACE_ERROR_NUM        ,   //  LXI模块获取SCPI模块指定接口的错误队列中错误的个数
    LXI_SCPI_ACHIEVE_CURRENT_INTERFACE_NUM                  //  LXI模块获取SCPI当前执行命令的接口，供Socket绑定动态端口
}LXI_SCPI_ACHIEVE_CMD_EM;

/*
 * 定义LXI模块写SCPI接口的写标识
*/
typedef enum
{
    LXI_SCPI_WRITE_NORMAL   = 0             ,   //  未写完
    LXI_SCPI_WRITE_EOP                      ,   //  写结束
    LXI_SCPI_WRITE_DISCARD_PKT                  //  写撤销
}LXI_SCPI_WRITE_FLAG_EM;

/*
 * 定义LXI模块操作FLASH的标识值
*/
typedef enum
{
    LXI_FLASH_WRITE         =   0           ,   //  LXI模块写FLASH操作
    LXI_FLASH_READ                              //  LXI模块读FLASH操作
}LXI_FLASH_OPTION_MODE_EM;

/*
 * LXI模块对网络内核请求标识值
*/
typedef enum
{
    LXI_NETWORK_INITIALIZATION              ,   //  网络初始化
    LXI_NETWORK_CONFIG_REQUST               ,   //  网络配置请求
    LXI_NETWORK_ADAPTOR_INFO_ACHIEVE        ,   //  LXI设备拥有的网络适配器获取
    LXI_NETWORK_ADAPTER_CHANGE              ,   //  网络适配器的选择
    LXI_WIRELESS_NETWORK_SCAN               ,   //  请求进行周围WIFI信号扫描
    LXI_WIFI_START_CONNECT                  ,   //  WIFI开始连接
    LXI_NETWORK_PARAMETER_ACHIEVE               //  网络参数的读取请求
}LXI_NETWORK_REQUST_ID_EM;

/*
 * 定义LXI模块网络状态标识值
*/
typedef enum
{
    LXI_STATUS_NOTHING_NETWORK_ADAPTOR  = 0 ,   //  当前没有网络适配器
    LXI_STATUS_UNLINK                       ,   //  网线未连接
    LXI_STATUS_LINK                         ,   //  网线接入
    LXI_STATUS_INIT                         ,   //  网络初始化过程
    LXI_STATUS_CONFIG_SUCCESS               ,   //  网络配置成功
    LXI_STATUS_DHCP_FAIL                    ,   //  从DHCP服务器获取IP失败
    LXI_STATUS_DHCP_IP_LOSE                 ,   //  从DHCP分配到的IP丢失,也就是DHCP服务器关闭或者出现故障
    LXI_STATUS_IP_CONFLICT                  ,   //  IP地址冲突
    LXI_STATUS_INVALID_IP_ADDR              ,   //  无效的IP地址
    LXI_STATUS_MDNS_PROBE_SUCCESS               //  mDNS服务探测成功
}LXI_STATUS_EM;

/*
 * 定义LXI模块关于当前网卡协商后的带宽码值定义
*/
typedef enum
{
    LXI_NETWORK_SPEED_10M =   0             ,   //  10兆网络
    LXI_NETWORK_SPEED_100M                  ,   //  百兆网络
    LXI_NETWORK_SPEED_1000M                     //  千兆网络
}LXI_NETWORK_SPEED_EM;

/*
 * 定义LXI模块关于当前网卡协商后的工作方式
*/
typedef enum
{
    LXI_NETWORK_HALF_WORK   =   0           ,   //  半双工
    LXI_NETWORK_FULL_WORK                       //  全双工
}LXI_NETWORK_WORK_MODE_EM;


/*
 * LXI设备拥有的网络适配器相关参数的结构体
*/
typedef struct
{
    u32     u32Num;
    s8      as8Name[LXI_NETWORK_ADAPTOR_MAX_NUM][LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN];
    u8      au8Open[LXI_NETWORK_ADAPTOR_MAX_NUM];
}LXI_NETWORK_ADAPTOR_INFO_STRU;

/*
 * 定义无线网络适配器连接秘钥结构体
*/
typedef struct
{
    s8      *ps8Name    ;   //  要连接的WIFI名字
    s8      *psPassword ;   //  连接WIFI的密码,如果该指针为NULL,标识连接无密的WIFI
}LXI_WIRELESS_NETWORK_CONNECT_KEY_STRU;

/*
 * 定义无线网络适配器所在的网络环境中WIFI的信息
*/
typedef struct
{
    u32     u32Num                                                                            ; //  当前LXI识别到的能连接的WIFI数量
    s8      as8WifiWpa[LXI_WIRELESS_NETWORK_WIFI_MAX_NUM]                                     ; //  当前LXI设备识别的WIFI是否需要密码访问
    s8      as8WifiName[LXI_WIRELESS_NETWORK_WIFI_MAX_NUM][LXI_WIRELESS_NETWORK_WIFI_NAME_LEN]; //  当前LXI设备能识别的WIFI名字

}LXI_WIFI_INFO_STRU;

/*
 * 定义LXI模块IP配置方式
*/
typedef enum
{
    LXI_IP_CONFIG_MODE_NONE         = -1,   //  目前还没有IP模式的配置
    LXI_IP_CONFIG_MODE_DHCP         = 0 ,   //  从DHCP服务器获取IP地址
    LXI_IP_CONFIG_MODE_AUTOIP           ,   //  由仪器遵循Autoip的方式分配IP地址
    LXI_IP_CONFIG_MODE_MANUALIP             //  由用户进行IP地址的分配
}LXI_IP_CONFIG_MODE_EM;

/*
 * 定义LXI模块参数存储的结构体
*/
typedef struct
{
    u8                          u8DhcpEnable                             ;   //  DHCP使能状态,支持UI和SCPI的读/写、NET层的读
    u8                          u8AutoIpEnable                           ;   //  AUTO IP 使能状态,支持UI和SCPI的读/写、NET层的读
    u8                          u8ManualIpEnable                         ;   //  Manual IP 使能状态,支持UI和SCPI的读/写、NET层的读
    u8                          u8MdnsEnable                             ;   //  mDNS使能状态,支持UI和SCPI的读/写、不支持NET层的读/写
    LXI_IP_CONFIG_MODE_EM       emIpMode                                ;   //  当前IP地址的获取方式,支持UI和SCPI的读，NET层的写
    s32                         s32IpAddr                               ;   //  IP地址,支持UI和SCPI的读/写，NET层的写
    s32                         s32GateAddr                             ;   //  网关地址,支持UI和SCPI的读/写，NET层的写
    s32                         s32MaskAddr                             ;   //  子网掩码,支持UI和SCPI的读/写，NET层的写
    s32                         s32DnsAddr[2]                           ;   //  DNS地址,支持UI和SCPI的读/写，NET层的写
    s8                          as8MacAddr[LXI_MAC_ADDR_MAX_LEN]        ;   //  MAC地址,支持UI的读，SCPI的读/写、NET层的读/写
    s8                          as8HostName[LXI_MDNS_NAME_MAX_LEN]      ;   //  mDNS主机名,支持UI和SCPI的读/写、不支持NET层的读/写
    s8                          as8ServiceName[LXI_MDNS_NAME_MAX_LEN]   ;   //  mDNS服务名,支持UI和SCPI的读/写、不支持NET层的读/写
}LXI_PARAMETER_INFO_STRU;

/**/
typedef struct
{
    u8          u8DhcpEnable;
    u8          u8AutoipEnable;
    u8          u8ManualIpEnable;
    u8          u8MdnsEnable;
    u32         u32IpAddr;
    u32         u32GatewayAddr;
    u32         u32SubMaskAddr;
    u32         u32PreferDnsAddr;
    u32         u32AlternativeDnsAddr;
    u32         u32SocketPort;
    s8          as8HostSettingName[LXI_MDNS_NAME_MAX_LEN];
    s8          as8HostDeclareName[LXI_MDNS_NAME_MAX_LEN];
    s8          as8ServiceSettingName[LXI_MDNS_NAME_MAX_LEN];
    s8          as8ServiceDeclareName[LXI_MDNS_NAME_MAX_LEN];
    s8          as8MacAddr[LXI_MAC_ADDR_MAX_LEN];
    s8          as8WebSettingPassword[LXI_HTTP_SERVICE_PASSWORD_MAX_LEN];
    s8          as8DefauleNetAdapterName[LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN];
    s8          as8WifiName[LXI_WIRELESS_NETWORK_WIFI_NAME_LEN];
    s8          as8WifiPassword[LXI_WIRELESS_NETWORK_WIFI_PASSWORD_LEN];
}LXI_FLASH_SAVE_INFO_STRU;

/*
 * LXI_UI_MESSAGE_REPORT_CALLBACK
 * UI层注册给LXI模块使用，LXI使用该函数进行消息的上报
 * parameter:   emMessId -- 消息ID
*/
typedef void (*LXI_UI_MESSAGE_REPORT_CALLBACK)(LXI_UI_MESSAGE_ID_EM emMessId);

/*
 * LXI_SCPI_EVENT_HANDLE_CALLBACK
 * SCPI层注册给LXI模块使用，LXI使用该函数进行SCPI资源的初始化、读写和控制操作
 * parameter:   emEventId -- SCPI事件ID标识号
*/
typedef s32 (*LXI_SCPI_EVENT_HANDLE_CALLBACK)(LXI_SCPI_EVENT_ID_EM emEventId , ...);

/*
 * LXI_SERVICE_REQUST_HANDLE_CALLBACK
 * 项目服务层注册给LXI模块使用，LXI将获取仪器信息
 * parameter:
 *            ps8Manufacturer       -- 生产厂商
 *            ps8SerialNumber       -- 产品序列号
 *            ps8InstrumentModel    -- 仪器型号
 *            ps8FirmwareRevision   -- 固件版本号
 *            ps8InstrumentSeries   -- 仪器系列
 *            ps8UsbVisaAddr        -- USB VISA地址
 *            u32MaxLen             -- 字符串最大长度
*/
typedef void (*LXI_SERVICE_REQUST_HANDLE_CALLBACK)(s8 *ps8Manufacturer,
                                                   s8 *ps8SerialNumber,
                                                   s8 *ps8InstrumentModel,
                                                   s8 *ps8FirmwareRevision,
                                                   s8 *ps8InstrumentSeries,
                                                   s8 *ps8UsbVisaAddr,
                                                   u32 u32MaxLen);
/*
 * LXI_FLASH_REQUST_HANDLE_CALLBACK
 * 驱动层提供Flash操作接口供LXI模块使用，LXI模块使用该接口进行变量的存储和读取操作
 * parameter:   emType      --  操作Flash模式，读/写
 *              emOptionId  --  操作类型
 *              pParaVal    --  需要操作的对象
 *              u32MaxLen   --  对于写操作，该值为操作对象的当前长度
 *                              对于读操作，该值为操作对象的长度DEMO限制
*/
typedef void (*LXI_FLASH_REQUST_HANDLE_CALLBACK)(LXI_FLASH_OPTION_MODE_EM    emType,
                                                 LXI_FLASH_SAVE_INFO_STRU   *pParaVal);

/*
 * LXI_DEBUG_INFO_WRITE_CALLBACK
 * 项目组注册给LXI模块进行调试信息输出的记录接口,可以为空
 * parameter:   ps8Info --  打印的消息
*/
typedef void (*LXI_DEBUG_INFO_WRITE_CALLBACK)( s8 *ps8Info );

/******************************************************************************/
/*                  LXI模块提供的变量以及函数声明，供项目组调用                      */
/******************************************************************************/

/*
 * LXI模块初始化参数,由LXI模块提供给项目系统层使用
*/
typedef struct
{
    u8                                  u8DSocketEn          ;   //  项目组决定是否支持动态端口上报仪器状态
    u8                                  u8WebSocketEn        ;   //  项目组决定是否支持Web Socket供Webontrol使用
    u8                                  u8ConnetKeepAlive    ;   //  项目组决定上位机与仪器建立的网络连接是否发送心跳包
    u8                                  u8LxiSocketMode      ;   //  项目组决定Socket工作模式:false -- I/O多路并发机制;true -- 建立子线程工作
    u32                                 u32LxiBuffLen       ;   //  项目组决定LXI模块最大数据接收处理能力
    s32                                 s32VxiPrio          ;   //  项目组决定LXI模块的Vxi-11服务线程优先级
    s32                                 s32HttpPrio         ;   //  项目组决定LXI模块的HTTP服务线程优先级
    s32                                 s32MdnsPrio         ;   //  项目组决定LXI模块的MDNS服务线程优先级
    s32                                 s32HiSlipPrio       ;   //  项目组决定LXI模块的HiSLIP服务线程优先级
    s32                                 s32SocketPrio       ;   //  项目组决定LXI模块的Socket服务线程优先级
    s32                                 s32DSocketPrio      ;   //  项目组决定LXI模块的动态Socket服务线程优先级
    s32                                 s32WebSocketPrio    ;   //  项目组决定LXI模块的WebSocket服务线程优先级
    s32                                 s32WebSocketPort    ;   //  项目组决定LXI模块的WebSocket服务的端口号
    s32                                 s32DSockeStartPort  ;   //  项目组决定LXI模块的动态Socket端口分配的起始地址
    s8                                 *ps8InstrumentHelp   ;   //  项目组决定Help页面中显示的内容,必须
    s32                                 s32InstHelpLength   ;   //  项目组决定Help页面中显示的内容的长度,必须
    s8                                 *ps8InstrumentLogo   ;   //  项目Logo图片的地址,必须
    s32                                 s32InstLogoSize     ;   //  项目Logo图片的长度,必须
    s8                                 *ps8WebControlSrc    ;   //  项目组WebControl网页源码,不存在,可以为空
    s32                                 s32WebControlSrcLen ;   //  项目组WebControl网页长度,不存在为0
    s8                                 *ps8WebControlCss    ;   //  项目组Webontrol的CSS文件,不存在,可以为空
    s32                                 s32WebControlCssLen ;   //  项目组Webontrol的CSS文件长度,不存在为0
    LXI_UI_MESSAGE_REPORT_CALLBACK      pfunUiCallback      ;   //  项目组提供的UI接口
    LXI_SCPI_EVENT_HANDLE_CALLBACK      pfunScpiCallback    ;   //  项目组提供的SCPI模块操作接口
    LXI_SERVICE_REQUST_HANDLE_CALLBACK  pfunServiceCallback ;   //  项目组提供的Service层接口函数
    LXI_FLASH_REQUST_HANDLE_CALLBACK    pfunFlashCallback   ;   //  项目组提供的Flash操作接口
    LXI_DEBUG_INFO_WRITE_CALLBACK       pfunPrintf          ;   //  项目组提供的调试信息输出接口
}LXI_INIT_PARAMETER_STRU;

/*
 * LXI_Open
 * 该函数提供给项目组使用，供项目组进行LXI模块的初始化
 * 该函数内部启动各网络服务，并启动NET内核
 * Parameter:   stInitPara   --  初始化配置参数
*/
extern void LXI_Open(LXI_INIT_PARAMETER_STRU *pstInitPara);

/*
 * 定义LXI模块操作码值
*/
typedef enum
{
    LXI_MODULE_VERSION_ACHIEVE  =   0       ,   //  LXI模块版本获取
    LXI_MODULE_PARAMETER_CONFIGURATION      ,   //  配置LXI,供UI和SCPI模块调用
    LXI_MODULE_PARAMETER_ACHIEVE            ,   //  获取LXI参数,供UI和SCPI模块调用
    LXI_MODULE_SOCKET_SERVICE_PORT_CHANGE   ,   //  配置当前socket服务的端口号，供UI和SCPI模块使用
    LXI_MODULE_SOCKET_SERVICE_PORT_ACHIEVE  ,   //  获取当前socket服务的端口号，供UI和SCPI模块使用
    LXI_MODULE_INSTRUMENT_STATUS_REPORT     ,   //  仪器状态上报,供项目功能模块使用
    LXI_MODULE_NETWORK_STATUS_REPORT        ,   //  供Net层使用，进行当前网络连接状态上报
    LXI_MODULE_NETWORK_ADAPTER_INFO_ACHIEVE ,   //  供UI层使用,查询当前拥有的网络适配器的名字
    LXI_MODULE_ADAPTER_SELECT               ,   //  供UI层和SCPI模块使用，选中网络适配器
    LXI_MODULE_FLASH_TO_SAVE                    //  LXI模块内部使用,进行Flash信息保存
}LXI_MODULE_OPTION_EM;

/*
 * LXI_Option
 * 该函数提供给项目组使用，供项目组进行UI界面的更新、SCPI命令的处理以及仪器状态的上报
 * Parameter1:  emOptionId  --  操作码值
 * Parameter2:  pParameter  --  参数
 * 获取LXI模块版本操作，Parameter2类型为 s8*
 * 配置LXI和获取LXI操作，Parameter2类型为    LXI_PARAMETER_INFO_STRU*
 * 仪器状态上报，Parameter2类型为   s8*
 * 网络状态上报，Parameter2类型为   LXI_STATUS_EM *
*/
extern void LXI_Option(LXI_MODULE_OPTION_EM emOptionId  ,   void *pParameter);

#ifdef __cplusplus
}
#endif

#endif // LXI_API_H
