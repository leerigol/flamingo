/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: svc_vxi.h
  功能描述: Vxi-11功能相关svc服务的结构体和函数声明

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-06

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-06          1.0        initial

*******************************************************************************/
#ifndef SVC_VXI_H
#define SVC_VXI_H

#include <sys/time.h>
#include "xdr_vxi.h"

#ifdef __cplusplus
extern "C" {
#endif

/*定义VXI资源管理的结构体*/
typedef struct
{
    int             io;
    DEVICE_LINK     id;
    bool_t          use;
    bool_t          lock;
    bool_t          abort;
    bool_t          first_write;
    struct  timeval frist_write_time;
    unsigned int    io_timeout;
    char            send_buf[LXI_VXI_CORE_SEND_BUFF_SIZE];
}LXI_VXI_RES_PARA_STRU;

/*DEVICE_FLAGS的bit位定义*/
typedef union
{
    struct
    {
        DEVICE_FLAGS   waitlock  :1;
        DEVICE_FLAGS   reserved_1:2;
        DEVICE_FLAGS   end       :1;
        DEVICE_FLAGS   reserved_2:3;
        DEVICE_FLAGS   termchrset:1;
        DEVICE_FLAGS   reserved_3:24;
    }flags;
    DEVICE_FLAGS     all;
}VXI_FLAGS_BITS_UN;

/*VXI客户端读操作返回值的bits定义*/
typedef union
{
    struct
    {
        VXI_INT32       reqcnt:1;
        VXI_INT32       chr:1;
        VXI_INT32       end:1;
        VXI_INT32       reserved:29;
    }reason;
    VXI_INT32           all;
}VXI_READ_REASON_BITS_UN;

/*
 * 定义VXI设备连接的处理函数
*/
extern DEVICE_LINK_RESP_STRU  *vxi_create_link( DEVICE_LINK_PARA_STRU *stLinkPara);

/*
 * 断开VXI-11连接
*/
extern DEVICE_ERROR_STRU   *vxi_destroy_link( DEVICE_LINK *pid );

/*
 * VXI客户端的写操作处理
*/
extern DEVICE_WRITE_RESP_STRU *vxi_write( DEVICE_WRITE_PARA_STRU *pstWritePara );

/*
 * 定义VXI客户端读命令的处理
*/
extern DEVICE_READ_RESP_STRU *vxi_read(DEVICE_READ_PARA_STRU *pstReadPara );

/*
 * 处理客户端的*STB?查询
*/
extern DEVICE_STB_RESP_STRU *vxi_readstb(DEVICE_GENERIC_PARA_STRU *pstPara );

/*
 *处理客户端的*TRG命令
*/
extern DEVICE_ERROR_STRU *vxi_trigger(DEVICE_GENERIC_PARA_STRU *pstPara );

/*
 * 处理客户端*CLR命令
*/
extern DEVICE_ERROR_STRU *vxi_clear( DEVICE_GENERIC_PARA_STRU *pstPara );

/*
 * 处理客户端要求VXI处于远程模式
*/
extern DEVICE_ERROR_STRU *vxi_remote( DEVICE_GENERIC_PARA_STRU *pstPara );

/*
 * 处理客户端要求VXI处于本地模式
*/
extern DEVICE_ERROR_STRU *vxi_local( DEVICE_GENERIC_PARA_STRU *pstPara );

/*
 *处理客户端的VXI设备锁定的命令
*/
extern DEVICE_ERROR_STRU   *vxi_lock(DEVICE_LOCK_PARA_STRU *pstLockPara );

/*
 *处理客户端的VXI设备解锁的命令
*/
extern DEVICE_ERROR_STRU   *vxi_unlock( DEVICE_LINK *pid );

/*
 * 处理客户端建立高速通信接口的请求
*/
extern DEVICE_ERROR_STRU *vxi_create_intr_chan( DEVICE_REMOTE_PARA_STRU *pstRmtPara );

/*
 * 处理销毁高速通信接口的请求
*/
extern DEVICE_ERROR_STRU *vxi_destroy_intr_chan( void );

/*
 * 处理客户端打开/关闭intr的RPC服务
*/
extern DEVICE_ERROR_STRU *vxi_enable_srq( DEVICE_ENABLE_SRQ_PARA_STRU *pstSrqPara );

/*
 * 处理客户端的DO CMD请求
*/
extern DEVICE_DOCMD_RESP_STRU *vxi_docmd( DEVICE_DOCMD_PARA_STRU *pstDoCmdPara );

/*
 * 处理客户端abort的请求
*/
extern DEVICE_ERROR_STRU *vxi_abort( DEVICE_LINK *pid );

/*
 * 处理客户端的intr-srq的命令
*/
extern void vxi_intr_srq( DEVICE_SRQ_PARA_STRU *pstSrqPara );

#ifdef __cplusplus
}
#endif
#endif // SVC_VXI_H

