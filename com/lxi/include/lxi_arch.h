/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: lxi_arch.h
  功能描述: 用于集成数字系统平台,这里是对数字系统平台兼容性函数的声明

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-10-24

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-10-24          1.0        initial

*******************************************************************************/

#ifndef LXI_ARCH_H
#define LXI_ARCH_H

//
#define LINUX_OS

//#ifdef  LINUX_OS

#include    <pthread.h>
#include    <semaphore.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef pthread_t   lxi_pthread_t;
typedef sem_t       lxi_sem_t;


//#endif

#ifndef SEM_FAILED
#define SEM_FAILED      ((lxi_sem_t *) 0)
#endif

/*
 * lxi_thread_create
 * LXI模块对于数字系统平台的线程创建函数的封装
 * Parameter1:  pThreadId   --  线程ID
 * Parameter2:  pFunc       --  线程的执行函数指针
 * Parameter3:  pInitPara   --  该线程的初始化参数
 * Return    :  线程创建错误码值
*/

extern s32 lxi_thread_create(lxi_pthread_t   *pThreadId,
                                void            *(*pFunc)(void*),
                                void            *pInitPara);

/*
 * lxi_sem_open
 * 信号量创建
 * Parameter1:  pSemaphore  --  创建的信号量指针
 * Parameter2:  ps8SemName  --  信号量的名称
 * Parameter3:  u32InitVal  --  信号量的初始值
 * Return    :  信号量创建错误码值:  0   --  无错误
 *                          -1  --  错误码值
*/
extern s32 lxi_sem_open(lxi_sem_t  *pSemaphore ,
                        s8         *ps8SemName ,
                        u32         u32InitVal);

/*
 *lxi_sem_wait
 *挂起信号量，等待信号量激活
 *Parameter1:   pSemaphore  --  需要进入挂起状态的信号量指针
 *Parameter2:   u32WaitTime --  进入挂起状态的时间,单位us
 *Return    :   错误码值
*/
extern s32 lxi_sem_wait(lxi_sem_t  *pSemaphore ,   u32 u32WaitTime);

/*
 * lxi_sem_post
 * 激活信号量，向信号量发送激励
 * Parameter1:  pSemaphore  --  需要进入激活状态的信号量指针
 * Return    :   错误码值
*/
extern s32 lxi_sem_post(lxi_sem_t  *pSemaphore);

/*
 * lxi_sem_close
 * 关闭指定的信号量，释放资源
 * Parameter1:  pSemaphore  --  需要关闭信号量指针
 * Return    :   错误码值
*/
extern s32 lxi_sem_close(lxi_sem_t *pSemaphore );

/*
 * lxi_sem_value_get
 * 获取指定的信号量当前的值
 * Parameter1:  pSemaphore      --  需要获取信号量值的指针
 * Parameter2:  pu32SemValue    --  当前信号量的值
 * Return    :   错误码值
*/
extern s32 lxi_sem_value_get(lxi_sem_t *pSemaphore ,   s32 *ps32SemValue);

/*
 * lxi_sleep
 * LXI模块使用计时器接口
 * Parameter:   u32TimeVal  --  需要延迟的值,单位us
*/

extern void lxi_sleep(u32  u32TimeVal);

#ifdef __cplusplus
}
#endif

#endif // LXI_ARCH_H
