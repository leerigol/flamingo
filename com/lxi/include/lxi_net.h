#ifndef LXI_NET_H
#define LXI_NET_H
#include <net/ethernet.h>
#include <linux/types.h>
#include <netinet/if_tr.h>
#include <netinet/ip.h>
#include "DataType.h"
#include "lxi_api.h"

#ifdef __cplusplus
extern "C" {
#endif

#define LXI_DHCP_CLIENT_PORT                    68                          //  DHCP客户端端口号
#define LXI_DHCP_SERVER_PORT                    67                          //  DHCP服务器端端口号
#define LXI_DHCP_IP_ADDR                        "255.255.255.255"           //  DHCP广播地址
#define LXI_DHCP_PACKET_MAX_SIZE                1500                        //  DHCP报文包的大小
#define LXI_DHCP_MAGIC_COOKIE                   0x63538263                  //  DHCP的Magic Cookie值
#define LXI_DHCP_DEFAULT_LEASETIME              0xFFFFFFFF                  //  默认的占用时间
#define LXI_DHCP_MAC_BCAST_ADDR                 "\xff\xff\xff\xff\xff\xff"  //  DHCP客户端广播的目的物理层地址
#define LXI_DHCP_INITIAL_RTO                    (4*1000000)                 //  DHCP重传超时时间,us
#define LXI_DHCP_MAX_RTO                        (10*1000000)                //  DHCP最大超时重传时间,us
#define LXI_DHCP_CLASS_ID_MAX_LEN               48                          //  DHCP类ID描述长度
#define LXI_DHCP_CLIENT_ID_MAX_LEN              48                          //  DHCP客户端ID描述长度
#define LXI_AUTOIP_START_ADDR                   0xA9FE0000                  //  AUTOIP配置的网段地址

/*定义DHCP服务处理函数的类型*/
typedef void *(*LXI_DHCP_STATUS_PROCESS)(void);

/*定义DHCP报文组织函数接口类型*/
typedef void (*LXI_DHCP_MSG_PROCESS)(u32 xid);

/*定义DHCP报文包格式*/
typedef struct
{
    u8          op;
    u8          htype;
    u8          hlen;
    u8          hops;
    u32         xid;
    u16         secs;
    u16         flags;
    u32         ciaddr;
    u32         yiaddr;
    u32         siaddr;
    u32         giaddr;
    u8          chaddr[16];
    u8          sname[64];
    u8          file[128];
    u8          options[312];
}LXI_DHCP_PACKET_STRU;

/*定义DHCP的消息类型的操作结构体*/
typedef struct
{
  u8        num;
  u8        len[256];
  u8        val[256][32];
}LXI_DHCP_OPTION_STRU;

/*DHCP服务相关属性结构体*/
typedef struct
{
    s8      version[11];
    s32     ciaddr;
    s32     siaddr;
    s32     class_len;
    s32     client_len;
    u32     xid;
    u8      shaddr[ETH_ALEN];
    s8      class_id[LXI_DHCP_CLASS_ID_MAX_LEN];
    u8      client_id[LXI_DHCP_CLIENT_ID_MAX_LEN];
} LXI_DHCP_INTERFACE_STRU;

typedef struct
{
    u16 uh_sport;
    u16 uh_dport;
    u16 uh_ulen;
    u16 uh_sum;
}LXI_DHCP_UDP_HDR_STRU;

/*定义UDP头*/
typedef struct
{
  s8 ip[sizeof(struct ip)];
  s8 udp[sizeof(LXI_DHCP_UDP_HDR_STRU)];
}LXI_DHCP_UDP_IP_HDR_STRU;

/*定义数据链路层头结构体*/
typedef struct
{
    u8  ether_dhost[ETH_ALEN];      /* destination eth addr */
    u8  ether_shost[ETH_ALEN];      /* source ether addr    */
    u16 ether_type;                 /* packet type ID field */
}LXI_DHCP_ETHER_HEADER_STRU;

typedef struct
{
  LXI_DHCP_ETHER_HEADER_STRU	ethhdr;
  s8                            udpipmsg[LXI_DHCP_PACKET_MAX_SIZE];
  s8                            pad_for_tokenring_header[sizeof(struct trh_hdr) + sizeof(struct trllc)];
} LXI_DHCP_UDP_MESSAGE_STRU;

typedef struct
{
  s32            ih_next,ih_prev;
  u8             ih_x1;
  u8             ih_pr;
  u16            ih_len;
  struct in_addr ih_src;
  struct in_addr ih_dst;
}LXI_DHCP_IPOVLY_STRU;

/*定义ARP报文包的信息结构体*/
typedef struct
{
  struct ethhdr ethhdr;
  u16           htype;              /* hardware type (must be ARPHRD_ETHER) */
  u16           ptype;              /* protocol type (must be ETHERTYPE_IP) */
  u8            hlen;               /* hardware address length (must be 6) */
  u8            plen;               /* protocol address length (must be 4) */
  u16           operation;          /* ARP opcode */
  u8            sHaddr[ETH_ALEN];	/* sender's hardware address */
  u8            sInaddr[4];         /* sender's IP address */
  u8            tHaddr[ETH_ALEN];	/* target's hardware address */
  u8            tInaddr[4];         /* target's IP address */
  u8            pad[18];            /* pad for min. Ethernet payload (60 bytes) */
}LXI_ARP_PACKET_STRU;

/*定义手动IP地址配置信息的结构体*/
typedef struct
{
    u32         ipaddr;
    u32         mask;
    u32         gate;
    u32         dns[2];
}LXI_MANUAL_IP_CFG_STRU;

/*定义DHCP报文消息的OP字段的码值*/
typedef enum
{
    LXI_DHCP_OP_CODE_REQUEST = 1,
    LXI_DHCP_OP_CODE_REPLY
}LXI_DHCP_OP_CODE_EM;
/*定义DHCP报文消息类型*/
typedef enum
{
    LXI_DHCP_MSG_TYPE_NULL     = 0  ,
    LXI_DHCP_MSG_TYPE_DISCOVER      ,
    LXI_DHCP_MSG_TYPE_OFFER         ,
    LXI_DHCP_MSG_TYPE_REQUEST       ,
    LXI_DHCP_MSG_TYPE_DECLINE       ,
    LXI_DHCP_MSG_TYPE_ACK           ,
    LXI_DHCP_MSG_TYPE_NAK           ,
    LXI_DHCP_MSG_TYPE_RELEASE       ,
    LXI_DHCP_MSG_TYPE_INFORM
}LXI_DHCP_MSG_TYPE_EM;
/*定义当前网络连接状态*/
typedef enum
{
    LXI_NET_LINK_STATUS_IS_LINK  = 0,
    LXI_NET_LINK_STATUS_NOT_LINK
}LXI_NET_LINK_STATUS_EM;

/* DHCP option and value (cf. RFC1533) */
/*根据标准RFC1533和标准补充RFC3397,定义DHCP操作和码值*/
typedef enum
{
    LXI_DHCP_OPTION_PAD                     = 0 ,   //  补充字段
    LXI_DHCP_OPTION_SUBNET_MASK                 ,   //  1
    LXI_DHCP_OPTION_TIMER_OFFSET                ,   //  2
    LXI_DHCP_OPTION_ROUTER                      ,   //  3
    LXI_DHCP_OPTION_SERVER_TIME                 ,   //  4
    LXI_DHCP_OPTION_SERVER_NAME                 ,   //  5
    LXI_DHCP_OPTION_DNS                         ,   //  6
    LXI_DHCP_OPTION_SERVER_LOG                  ,   //  7
    LXI_DHCP_OPTION_SERVER_COOKIE               ,   //  8
    LXI_DHCP_OPTION_SERVER_LPR                  ,   //  9
    LXI_DHCP_OPTION_SERVER_IMPRESS              ,   //  10
    LXI_DHCP_OPTION_SERVER_RESOURCE_LOCATION    ,   //  11
    LXI_DHCP_OPTION_HOST_NAME                   ,   //  12
    LXI_DHCP_OPTION_BOOT_FILE_SIZE              ,   //  13
    LXI_DHCP_OPTION_MERIT_DUMP_FILE             ,   //  14
    LXI_DHCP_OPTION_DOMAIN_NAME                 ,   //  15
    LXI_DHCP_OPTION_SERVER_SWAP                 ,   //  16
    LXI_DHCP_OPTION_ROOT_PATH                   ,   //  17
    LXI_DHCP_OPTION_EXTENTIONS_PATH             ,   //  18
    LXI_DHCP_OPTION_IP_FORWARDING               ,   //  19
    LXI_DHCP_OPTION_NONLOCAL_SOURCE_ROUTING     ,   //  20
    LXI_DHCP_OPTION_POLICY_FILTER               ,   //  21
    LXI_DHCP_OPTION_MAX_DGRAM_REASM_SIZE        ,   //  22
    LXI_DHCP_OPTION_DEFAULT_IP_TTL              ,   //  23
    LXI_DHCP_OPTION_PATH_MTUAGING_TIMEOUT       ,   //  24
    LXI_DHCP_OPTION_PATH_MTUPLATEAU_TABLE       ,   //  25
    LXI_DHCP_OPTION_IF_MTU                      ,   //  26
    LXI_DHCP_OPTION_ALL_SUBNETS_LOCAL           ,   //  27
    LXI_DHCP_OPTION_BROADCAST_ADDR              ,   //  28
    LXI_DHCP_OPTION_PERFORM_MASK_DISCOVERY      ,   //  29
    LXI_DHCP_OPTION_MASK_SUPPLIER               ,   //  30
    LXI_DHCP_OPTION_PERFORM_ROUTER_DISCOVERY    ,   //  31
    LXI_DHCP_OPTION_ROUTER_SOLICITATION_ADDR    ,   //  32
    LXI_DHCP_OPTION_STATIC_ROUTE                ,   //  33
    LXI_DHCP_OPTION_TRAILER_ENCAP_SULATION      ,   //  34
    LXI_DHCP_OPTION_ARP_CACHE_TIMEOUT           ,   //  35
    LXI_DHCP_OPTION_ETHERNET_ENCAP_SULATION     ,   //  36
    LXI_DHCP_OPTION_TCP_DEFAULT_TTTL            ,   //  37
    LXI_DHCP_OPTION_TCP_KEEPALIVE_INTERVAL      ,   //  38
    LXI_DHCP_OPTION_TCP_KEEPALIVE_GARBAGE       ,   //  39
    LXI_DHCP_OPTION_NIS_DOMAIN_NAME             ,   //  40
    LXI_DHCP_OPTION_NIS_SERVERS                 ,   //  41
    LXI_DHCP_OPTION_NTP_SERVERS                 ,   //  42
    LXI_DHCP_OPTION_VENDOR_SPECIFIC_INFO        ,   //  43
    LXI_DHCP_OPTION_NET_BIOS_NAME_SERVER        ,   //  44
    LXI_DHCP_OPTION_NET_BIOS_DGRAM_DIST_SERVER  ,   //  45
    LXI_DHCP_OPTION_NET_BIOS_SNODE_TYPE         ,   //  46
    LXI_DHCP_OPTION_NET_BIOS_SCOPE              ,   //  47
    LXI_DHCP_OPTION_XFONT_SERVER                ,   //  48
    LXI_DHCP_OPTION_XDISPLAY_MANAGER            ,   //  49
    LXI_DHCP_OPTION_REQUESTED_IP_ADDR           ,   //  50
    LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME          ,   //  51
    LXI_DHCP_OPTION_OVERLOAD                    ,   //  52
    LXI_DHCP_OPTION_MESSAGE_TYPE                ,   //  53
    LXI_DHCP_OPTION_SERVER_IDENTIFIER           ,   //  54
    LXI_DHCP_OPTION_PARAM_REQUEST               ,   //  55
    LXI_DHCP_OPTION_MESSAGE                     ,   //  56
    LXI_DHCP_OPTION_MESSAGE_MAX_SIZE            ,   //  57
    LXI_DHCP_OPTION_T1_VALUE                    ,   //  58
    LXI_DHCP_OPTION_T2_VALUE                    ,   //  59
    LXI_DHCP_OPTION_CLASS_IDENTIFIER            ,   //  60
    LXI_DHCP_OPTION_CLIENTIDENTIFIER            ,   //  61
    LXI_DHCP_OPTION_FQDN_HOSTNAME       = 81    ,   //
    LXI_DHCP_OPTION_DNS_SEARCH_PATH     = 119   ,   //  RFC 3397
    LXI_DHCP_OPTION_END                 = 255
}LXI_DHCP_OPTION_EM;

//  定义DHCP客户端状态机状态
typedef enum
{
    LXI_DHCP_WORK_STATUS_REBOOT = 0 ,
    LXI_DHCP_WORK_STATUS_DISCOVER   ,
    LXI_DHCP_WORK_STATUS_REQUEST    ,
    LXI_DHCP_WORK_STATUS_RENEW      ,
    LXI_DHCP_WORK_STATUS_REBIND     ,
    LXI_DHCP_WORK_STATUS_RELEASE
}LXI_DHCP_WORK_STATUS_EM;
extern LXI_NET_LINK_STATUS_EM lxi_net_link_status_get( s8  *ps8NetName );

extern void lxi_net_speed_and_duplex_get( s8 *ps8NetName , LXI_NETWORK_SPEED_EM * pemSpeed , LXI_NETWORK_WORK_MODE_EM *pemMode );

/*
 * IP配置模块初始化
*/
extern void lxi_net_init( s8 *ps8DevName , LXI_PARAMETER_INFO_STRU * pstInitPara );

/*
 * 获取当前设备拥有的网络适配器名
*/
extern void lxi_net_device_info( LXI_NETWORK_ADAPTOR_INFO_STRU *pstAdaptorInfo );

/*
 * 配置网络
*/
extern void lxi_net_config( LXI_PARAMETER_INFO_STRU *pstCfgPara );

/*
 * 获取网络参数
*/
extern void lxi_net_para_get( LXI_PARAMETER_INFO_STRU *pstPara );

/*
 * 设置当前使用的网络适配器
*/
extern void lxi_net_device_select( s8 *ps8DevName );

/*
 *添加路由地址
*/
extern void lxi_net_route_add(s32 s32GateWay , s32 s32SubMask );

#ifdef __cplusplus
}
#endif

#endif // LXI_NET_H

