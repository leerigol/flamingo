/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: svc_vxi.c
  功能描述: Vxi-11功能相关svc服务的函数定义

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-06

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-06          1.0        initial

*******************************************************************************/
#include <unistd.h>
#include <string.h>
#include "svc_vxi.h"

bool_t                          m_bVxiDeviceLock = FALSE;   //  当前VXI设备的锁定状态
LXI_VXI_RES_PARA_STRU           m_astVxiRes[LXI_VXI_MAX_CILENT_NUM] = { {0} };

bool_t vxi_link_id_get( DEVICE_LINK_PARA_STRU stLinkPara , DEVICE_LINK *pId )
{
    int         scpi_io = -1;
    int         i;

    /*查找可用资源*/
    for( i = 0 ; i < LXI_VXI_MAX_CILENT_NUM ; i ++ )
    {
        if( FALSE == m_astVxiRes[i].use )
        {
            break;
        }
    }
    /*没有查找到可以使用的VXI资源，则返回FALSE*/
    if( i == LXI_VXI_MAX_CILENT_NUM )
    {
        return FALSE;
    }
    /*获取SCPI资源*/
    if( NULL != m_stLxiInitPara.pfunScpiCallback )
    {
        /*申请SCPI资源*/
        scpi_io = m_stLxiInitPara.pfunScpiCallback(LXI_SCPI_RESOURCE_INITIALIZATION);
        if( -1 == scpi_io )
        {
            /*申请SCPI资源失败*/
            return FALSE;
        }
    }
    /*更新数据*/
    m_astVxiRes[i].use          = TRUE;
    m_astVxiRes[i].io           = scpi_io;
    m_astVxiRes[i].lock         = stLinkPara.lock;
    m_astVxiRes[i].id           = i;
    /*如果资源锁定，则置位*/
    if( TRUE == m_astVxiRes[i].lock )
    {
        m_bVxiDeviceLock = TRUE;
    }
    /*初始化发送数据缓冲区*/
    memset( m_astVxiRes[i].send_buf , '\0' , LXI_VXI_CORE_SEND_BUFF_SIZE );
    /*
     * 记录当前Vxi通信ID
    */
    *pId = i;

    return TRUE;
}

bool_t vxi_link_id_check( DEVICE_LINK id )
{
    /*判定当前ID是否被使用*/
    if( TRUE == m_astVxiRes[id].use )
    {
        return TRUE;
    }
    return FALSE;
}

void vxi_link_id_free( DEVICE_LINK id )
{
    /*置该ID对应的资源为未使用状态*/
    m_astVxiRes[id].use = FALSE;
    /*释放SCPI资源*/
    if( NULL != m_stLxiInitPara.pfunScpiCallback )
    {
        m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_RESOURCE_FREE , m_astVxiRes[id].io );
    }
}

bool_t  vxi_wait_for_lock( DEVICE_LINK *pId , VXI_UINT32 lock_timeout , DEVICE_ERROR_CODE *pError )
{
    VXI_UINT32      time = 0;
    struct timeval  start , stop;

    /*如果当前的锁定超时时间为0,则直接返回*/
    if( lock_timeout == 0 )
    {
        return FALSE;
    }
    else
    {
        gettimeofday(&start , NULL );
        /*检测时间*/
        do
        {
            usleep( 500 );  //  500微秒检测一次
            gettimeofday(&stop , NULL );
            if( stop.tv_usec > start.tv_usec )
            {
                time = (stop.tv_sec - start.tv_sec) * 1000 + ( stop.tv_usec - start.tv_usec)/1000;
            }
            else
            {
                time = (stop.tv_sec - start.tv_sec) * 1000 + ( start.tv_usec - stop.tv_usec)/1000;
            }
            //  VXI设备解锁
            if( FALSE == m_bVxiDeviceLock )
            {
                return TRUE;
            }
            //  接收到客户端的abort命令
            if( NULL != pId && TRUE == m_astVxiRes[*pId].abort )
            {
                *pError = LXI_VXI_ABORT;
                return TRUE;
            }
        }while( time < lock_timeout );
    }
    return FALSE;
}

/*定义VXI设备连接的处理函数*/
DEVICE_LINK_RESP_STRU  *vxi_create_link( DEVICE_LINK_PARA_STRU *pstLinkPara)
{
    static DEVICE_LINK_RESP_STRU   result = { 0 };
    DEVICE_LINK                    id;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_LINK_RESP_STRU ) );

    /*判断设备描述符是否为inst0*/
    if( !strcmp( pstLinkPara->device , "inst0" ) )
    {
        result.error = LXI_VXI_NO_ERROR;
    }
    else
    {
        /*设备不支持*/
        result.error = LXI_VXI_DEVICE_NOT_ACCESSIBLE;
        return &result;
    }
    /*当前设备已被锁定,不再进行连接*/
    if( TRUE == m_bVxiDeviceLock )
    {
        if( !vxi_wait_for_lock( NULL , pstLinkPara->lock_timeout , &result.error ) )
        {
            result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
            return &result;
        }
    }
    if( LXI_VXI_NO_ERROR == result.error )
    {
        /*获取VXI资源*/
        if( !vxi_link_id_get( *pstLinkPara , &id ) )
        {
            result.error = LXI_VXI_OUT_OF_RESOURCE;
            return &result;
        }

        /*组织返回值*/
        result.id           = id;
        result.port         = LXI_VXI_ASYNC_PORT;
        result.recv_size    = LXI_VXI_CORE_RECV_BUFF_SIZE;
    }

    return &result;
}
/*
 * 断开VXI-11连接
*/
DEVICE_ERROR_STRU *vxi_destroy_link( DEVICE_LINK *pid )
{
    static DEVICE_ERROR_STRU   result = { 0 };

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    /*检测释放的资源是否存在*/
    if( TRUE == vxi_link_id_check( *pid ) )
    {
        vxi_link_id_free( *pid );
        result.error = LXI_VXI_NO_ERROR;
    }
    else
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
    }
    return &result;
}

/*
 * VXI客户端的写操作处理
*/
DEVICE_WRITE_RESP_STRU *vxi_write( DEVICE_WRITE_PARA_STRU *pstWritePara )
{
    static DEVICE_WRITE_RESP_STRU   result;
    VXI_FLAGS_BITS_UN               unflags;
    bool_t                          bFristWrite = FALSE;
    struct timeval                  time;
    VXI_UINT32                      io_time;
    int                             s32Ret = 0;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_WRITE_RESP_STRU ) );

    /*检测该ID是否存在*/
    if( !vxi_link_id_check( pstWritePara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }
    /*判断参数的有效*/
    if( LXI_VXI_CORE_RECV_BUFF_SIZE < pstWritePara->data.data_len || 0 == pstWritePara->data.data_len )
    {
        result.error = LXI_VXI_PARAMETER_ERROR;
        return &result;
    }
    /*获取标识*/
    unflags.all = pstWritePara->flags;
    /*判断是否资源被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*如果是该接口被锁定则不处理,否则根据lock_timeout值进行监控*/
        if( FALSE == m_astVxiRes[pstWritePara->id].lock )
        {
            /*进行监控*/
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstWritePara->id , pstWritePara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                return &result;
            }
        }
    }
    if( LXI_VXI_NO_ERROR != result.error )
    {
        return &result;
    }
    /*
     * 判断是否为首次写，首次写标识为FALSE且当前写标识end位为FALSE，则置首次写标识为TRUE
     * 并获取当前系统时间，且更新写超时时间
    */
    if( FALSE == m_astVxiRes[pstWritePara->id].first_write && FALSE == unflags.flags.end )
    {
        m_astVxiRes[pstWritePara->id].first_write = TRUE;
        gettimeofday(&m_astVxiRes[pstWritePara->id].frist_write_time , NULL );
        m_astVxiRes[pstWritePara->id].io_timeout = pstWritePara->io_timeout;
        bFristWrite = TRUE;
    }
    else if( unflags.flags.end )  //  如果当前写结束则置写标识为FALSE
    {
        m_astVxiRes[pstWritePara->id].first_write = FALSE;
    }
    /*如果当前写操作没有结束,则计算耗时*/
    if( TRUE == m_astVxiRes[pstWritePara->id].first_write && FALSE == bFristWrite )
    {
        /*获取当前时间*/
        gettimeofday(&time , NULL );
        /*计算IO写操作耗时时间*/
        io_time = ( time.tv_sec - m_astVxiRes[pstWritePara->id].frist_write_time.tv_sec ) * 1000;
        if( time.tv_usec > m_astVxiRes[pstWritePara->id].frist_write_time.tv_usec )
        {
            io_time += (time.tv_usec - m_astVxiRes[pstWritePara->id].frist_write_time.tv_usec)/1000;
        }
        else
        {
            io_time += (m_astVxiRes[pstWritePara->id].frist_write_time.tv_usec - time.tv_usec)/1000;
        }
        //  判断是否写超时
        if( io_time > m_astVxiRes[pstWritePara->id].io_timeout )
        {
            result.error = LXI_VXI_IO_TIMEOUT;
            return &result;
        }
        /*检测在连续写操作的过程中是否发生Abort命令*/
        if( TRUE == m_astVxiRes[pstWritePara->id].abort )
        {
            result.error = LXI_VXI_ABORT;
            /*向SCPI模块设置数据无效的标识*/
            m_stLxiInitPara.pfunScpiCallback(LXI_SCPI_WRITE,
                                             m_astVxiRes[pstWritePara->id].io,
                                             pstWritePara->data.data_val,
                                             pstWritePara->data.data_len,
                                             pstWritePara->io_timeout,
                                             LXI_SCPI_WRITE_DISCARD_PKT);
            return &result;
        }
    }
    /*发送到SCPI模块*/
    if( NULL != m_stLxiInitPara.pfunScpiCallback )
    {
        if( unflags.flags.end )
        {
            s32Ret = m_stLxiInitPara.pfunScpiCallback(LXI_SCPI_WRITE,
                                                      m_astVxiRes[pstWritePara->id].io,
                                                      pstWritePara->data.data_val,
                                                      pstWritePara->data.data_len,
                                                      pstWritePara->io_timeout,
                                                      LXI_SCPI_WRITE_EOP);
        }
        else
        {
            s32Ret = m_stLxiInitPara.pfunScpiCallback(LXI_SCPI_WRITE,
                                                      m_astVxiRes[pstWritePara->id].io,
                                                      pstWritePara->data.data_val,
                                                      pstWritePara->data.data_len,
                                                      pstWritePara->io_timeout,
                                                      LXI_SCPI_WRITE_NORMAL);
        }
    }
    /*写错误判断*/
    if( 0 != s32Ret)
    {
        result.error = LXI_VXI_IO_ERROR;

        return &result;
    }
    /*填充接收到的字节数*/
    result.error = LXI_VXI_NO_ERROR;
    result.size  = pstWritePara->data.data_len;
    return &result;
}

/*
 * 定义VXI客户端读命令的处理
*/
DEVICE_READ_RESP_STRU *vxi_read( DEVICE_READ_PARA_STRU *pstReadPara )
{
    static DEVICE_READ_RESP_STRU    result   = { 0 };
    VXI_FLAGS_BITS_UN               unflags  = { {0} };
    VXI_READ_REASON_BITS_UN         unreason = { {0} };
    struct timeval                  start,stop;
    VXI_UINT32                      time;
    int                             s32Ret = 0;
    unsigned int                    u32ReadLen;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_READ_RESP_STRU ) );

    //  获取读操作标识
    unflags.all = pstReadPara->flags;

    //  获取开始读取的时间
    gettimeofday(&start , NULL );

    /*检测通信句柄是否已经创建*/
    if( !vxi_link_id_check( pstReadPara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        goto read_end;
    }
    /*检测当前设备是不是被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*如果设备已被锁定，则判断是否为该通信ID锁定，如果是，则不处理，否则则进行监控处理，直到超时*/
        if( FALSE == m_astVxiRes[pstReadPara->id].lock )
        {
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstReadPara->id , pstReadPara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                goto read_end;
            }
        }
    }
    /*判断是否客户端发送了Abort命令*/
    if( TRUE == m_astVxiRes[pstReadPara->id].abort )
    {
        result.error = LXI_VXI_ABORT;
        goto read_end;
    }
    /*计算从SCPI模块提取的数据长度*/
    u32ReadLen = ( LXI_VXI_CORE_SEND_BUFF_SIZE < pstReadPara->size ) ? LXI_VXI_CORE_SEND_BUFF_SIZE : pstReadPara->size;

    /*
     * 开始从SCPI模块获取数据，并发送出去
    */
    if( NULL != m_stLxiInitPara.pfunScpiCallback )
    {
        /*设置本次需要读取的数据长度*/
        m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_PARAMETER_CONFIG ,
                                          m_astVxiRes[pstReadPara->id].io,
                                          LXI_SCPI_CONFIG_READ_LENGTH_CMD,
                                          u32ReadLen);
        /*从SCPI模块读取数据*/
        s32Ret = m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_READ ,
                                                   m_astVxiRes[pstReadPara->id].io,
                                                   m_astVxiRes[pstReadPara->id].send_buf,
                                                   u32ReadLen);
    }

    while( s32Ret <= 0 )
    {
        //  以500us轮询的方式获取数据
        usleep( 500 );
        /*从SCPI再次获取数据*/
        s32Ret = m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_READ ,
                                                   m_astVxiRes[pstReadPara->id].io,
                                                   m_astVxiRes[pstReadPara->id].send_buf,
                                                   u32ReadLen);
        if( s32Ret > 0 )    //  获取到数据
        {
            break;
        }
        else
        {
            //  获取当前系统时间
            gettimeofday(&stop , NULL);
            //  计算当前读操作耗时
            if( stop.tv_usec > start.tv_usec )
            {
                time = (stop.tv_sec - start.tv_sec) * 1000 + ( stop.tv_usec - start.tv_usec)/1000;
            }
            else
            {
                time = (stop.tv_sec - start.tv_sec) * 1000 + ( start.tv_usec - stop.tv_usec)/1000;
            }
            //  判断是否超时
            if( time > pstReadPara->io_timeout )
            {
                result.error = LXI_VXI_IO_TIMEOUT;
                s32Ret       = 0;
                break;
            }
            //  如果收到客户端的abort命令则跳出
            if( TRUE == m_astVxiRes[pstReadPara->id].abort )
            {
                result.error = LXI_VXI_ABORT;
                s32Ret       = 0;
                break;
            }
        }
    }

    /*判定是否读操作结束*/
    if( ( u32ReadLen == (unsigned int)s32Ret && m_astVxiRes[pstReadPara->id].send_buf[s32Ret-1] == pstReadPara->termChar ) ||
        ( u32ReadLen > (unsigned int)s32Ret  && s32Ret != 0 ))
    {
        unreason.reason.end = TRUE;
    }
    /*判断客户端请求的字节长度是否发送完*/
    if( pstReadPara->size == (VXI_UINT32)s32Ret )
    {
        unreason.reason.reqcnt = TRUE;
    }
    /*判断是否对结束符比对的工作*/
    if( unflags.flags.termchrset && s32Ret != 0 )
    {
        unreason.reason.chr = TRUE;
    }
read_end:
    //  更新数据
    result.data.data_len = s32Ret;
    result.data.data_val = m_astVxiRes[pstReadPara->id].send_buf;
    result.reason        = unreason.all;
    //  填充结束符
    if( 0 == s32Ret )
    {
        m_astVxiRes[pstReadPara->id].send_buf[0] = pstReadPara->termChar;
    }
    else if( unreason.reason.chr )
    {
        if( m_astVxiRes[pstReadPara->id].send_buf[s32Ret-1] != pstReadPara->termChar && s32Ret != LXI_VXI_CORE_SEND_BUFF_SIZE )
        {
            m_astVxiRes[pstReadPara->id].send_buf[s32Ret] = pstReadPara->termChar;
            result.data.data_len ++;
        }
    }

    return &result;
}

/*
 * 处理客户端的*STB?查询
*/
DEVICE_STB_RESP_STRU *vxi_readstb( DEVICE_GENERIC_PARA_STRU *pstPara )
{
    static DEVICE_STB_RESP_STRU result  = { 0 };
    VXI_FLAGS_BITS_UN           unflags = { {0} };
    struct timeval              start,stop;
    VXI_UINT32                  time;
    int                         s32Ret  = 0;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_STB_RESP_STRU ) );

    /*获取查询标识*/
    unflags.all = pstPara->flags;

    /*判断通信ID是否存在*/
    if( !vxi_link_id_check( pstPara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }

    /*判断VXI设备是否被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*判断是否为该通信ID锁定*/
        if( FALSE == m_astVxiRes[pstPara->id].lock )
        {
            //  如果标识的waitlock设置成FALSE或者等待超时，则返回错误
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstPara->id , pstPara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                return &result;
            }
        }
    }

    if( LXI_VXI_NO_ERROR == result.error )
    {
        /*获取命令处理开始时间*/
        gettimeofday(&start,NULL);

        /*发送*STB?命令到SCPI模块*/
        if( NULL != m_stLxiInitPara.pfunScpiCallback )
        {
            s32Ret = m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_WRITE ,
                                                       m_astVxiRes[pstPara->id].io,
                                                       "*STB?",
                                                       strlen("*STB?"),
                                                       pstPara->io_timeout,
                                                       LXI_SCPI_WRITE_EOP);
            if( 0 != s32Ret )
            {
                result.error = LXI_VXI_IO_ERROR;
                return &result;
            }
        }
        /*设置LXI模块准备读取的数据长度*/
        m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_PARAMETER_CONFIG ,
                                          m_astVxiRes[pstPara->id].io,
                                          LXI_SCPI_CONFIG_READ_LENGTH_CMD,
                                          LXI_VXI_CORE_SEND_BUFF_SIZE);
        /*判断命令是否执行完成*/
        do
        {
            /*间隔500us检测一次*/
            usleep( 500 );
            /*查询*/
            s32Ret = m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_READ ,
                                                       m_astVxiRes[pstPara->id].io,
                                                       m_astVxiRes[pstPara->id].send_buf,
                                                       LXI_VXI_CORE_SEND_BUFF_SIZE);
            if( 0 < s32Ret )    //  获取到查询结果
            {
                result.error = LXI_VXI_NO_ERROR;
                break;
            }
            else
            {
                /*获取此时的时间*/
                gettimeofday(&stop , NULL);
                //  计算当前读操作耗时
                if( stop.tv_usec > start.tv_usec )
                {
                    time = (stop.tv_sec - start.tv_sec) * 1000 + ( stop.tv_usec - start.tv_usec)/1000;
                }
                else
                {
                    time = (stop.tv_sec - start.tv_sec) * 1000 + ( start.tv_usec - stop.tv_usec)/1000;
                }
                //  判断是否超时
                if( time > pstPara->io_timeout )
                {
                    result.error = LXI_VXI_IO_TIMEOUT;
                    s32Ret       = 0;
                    break;
                }
            }
            /*如果在检测时间内，收到客户端的Abort命令，则直接结束*/
            if( TRUE == m_astVxiRes[pstPara->id].abort )
            {
                result.error = LXI_VXI_ABORT;
                break;
            }
        }while( 1 );
    }
    //  如果获取到数据，则进行数据转换
    if( s32Ret > 0 )
    {
        result.stb = ( unsigned char )atoi( m_astVxiRes[pstPara->id].send_buf );
    }

    return &result;
}

/*
 *处理客户端的*TRG命令
*/
DEVICE_ERROR_STRU *vxi_trigger( DEVICE_GENERIC_PARA_STRU *pstPara )
{
    static DEVICE_ERROR_STRU        result  = { 0 };
    VXI_FLAGS_BITS_UN               unflags = { {0} };
    int                             s32Ret;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    /*获取查询标识*/
    unflags.all = pstPara->flags;

    /*判断通信ID是否存在*/
    if( !vxi_link_id_check( pstPara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }

    /*判断VXI设备是否被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*判断是否为该通信ID锁定*/
        if( FALSE == m_astVxiRes[pstPara->id].lock )
        {
            //  如果标识的waitlock设置成FALSE或者等待超时，则返回错误
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstPara->id , pstPara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                return &result;
            }
        }
    }
    if( LXI_VXI_NO_ERROR == result.error)
    {
        /*向SCPI发送*TRG命令*/
        if( NULL != m_stLxiInitPara.pfunScpiCallback )
        {
            s32Ret = m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_WRITE ,
                                                       m_astVxiRes[pstPara->id].io,
                                                       "*TRG?",
                                                       strlen("*TRG?"),
                                                       pstPara->io_timeout,
                                                       LXI_SCPI_WRITE_EOP);
            if( 0 != s32Ret )
            {
                result.error = LXI_VXI_IO_ERROR;
                return &result;
            }
        }
    }
    return &result;
}

/*
 * 处理客户端*CLR命令
*/
DEVICE_ERROR_STRU *vxi_clear( DEVICE_GENERIC_PARA_STRU *pstPara )
{
    static DEVICE_ERROR_STRU        result  = { 0 };
    VXI_FLAGS_BITS_UN               unflags = { {0} };
    int                             s32Ret;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    /*获取查询标识*/
    unflags.all = pstPara->flags;

    /*判断通信ID是否存在*/
    if( !vxi_link_id_check( pstPara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }

    /*判断VXI设备是否被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*判断是否为该通信ID锁定*/
        if( FALSE == m_astVxiRes[pstPara->id].lock )
        {
            //  如果标识的waitlock设置成FALSE或者等待超时，则返回错误
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstPara->id , pstPara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                return &result;
            }
        }
    }
    if( LXI_VXI_NO_ERROR == result.error)
    {
        /*向SCPI发送*CLR命令*/
        if( NULL != m_stLxiInitPara.pfunScpiCallback )
        {
            s32Ret = m_stLxiInitPara.pfunScpiCallback( LXI_SCPI_WRITE ,
                                                       m_astVxiRes[pstPara->id].io,
                                                       "*CLR",
                                                       strlen("*CLR"),
                                                       pstPara->io_timeout,
                                                       LXI_SCPI_WRITE_EOP);
            if( 0 != s32Ret )
            {
                result.error = LXI_VXI_IO_ERROR;
                return &result;
            }
        }
    }
    return &result;
}

/*
 * 处理客户端要求VXI处于远程模式
*/
DEVICE_ERROR_STRU *vxi_remote( DEVICE_GENERIC_PARA_STRU *pstPara )
{
    static DEVICE_ERROR_STRU        result  = { 0 };
    VXI_FLAGS_BITS_UN               unflags = { {0} };

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    /*获取查询标识*/
    unflags.all = pstPara->flags;

    /*判断通信ID是否存在*/
    if( !vxi_link_id_check( pstPara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }

    /*判断VXI设备是否被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*判断是否为该通信ID锁定*/
        if( FALSE == m_astVxiRes[pstPara->id].lock )
        {
            //  如果标识的waitlock设置成FALSE或者等待超时，则返回错误
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstPara->id , pstPara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                return &result;
            }
        }
    }
    if( LXI_VXI_NO_ERROR == result.error)
    {
        /*向UI层发送消息，使仪器处于远程模式*/
        if( NULL != m_stLxiInitPara.pfunUiCallback )
        {
            m_stLxiInitPara.pfunUiCallback(LXI_UI_DEVICE_ENTER_REMOTE);
        }
    }

    return &result;
}

/*
 * 处理客户端要求VXI处于本地模式
*/
DEVICE_ERROR_STRU *vxi_local( DEVICE_GENERIC_PARA_STRU *pstPara )
{
    static DEVICE_ERROR_STRU        result  = { 0 };
    VXI_FLAGS_BITS_UN               unflags = { {0} };

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    /*获取查询标识*/
    unflags.all = pstPara->flags;

    /*判断通信ID是否存在*/
    if( !vxi_link_id_check( pstPara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }

    /*判断VXI设备是否被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*判断是否为该通信ID锁定*/
        if( FALSE == m_astVxiRes[pstPara->id].lock )
        {
            //  如果标识的waitlock设置成FALSE或者等待超时，则返回错误
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstPara->id , pstPara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                return &result;
            }
        }
    }
    if( LXI_VXI_NO_ERROR == result.error)
    {
        /*向UI层发送消息，使仪器处于本地模式*/
        if( NULL != m_stLxiInitPara.pfunUiCallback )
        {
            m_stLxiInitPara.pfunUiCallback(LXI_UI_DEVICE_ENTER_LOCAL);
        }
    }

    return &result;
}

/*
 *处理客户端的VXI设备锁定的命令
*/
DEVICE_ERROR_STRU   *vxi_lock( DEVICE_LOCK_PARA_STRU *pstLockPara )
{
    static DEVICE_ERROR_STRU    result  = { 0 };
    VXI_FLAGS_BITS_UN           unflags = {{0}};

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    /*获取查询标识*/
    unflags.all = pstLockPara->flags;

    /*判断通信ID是否存在*/
    if( !vxi_link_id_check( pstLockPara->id ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }

    /*判断VXI设备是否被锁定*/
    if( TRUE == m_bVxiDeviceLock )
    {
        /*判断是否为该通信ID锁定*/
        if( FALSE == m_astVxiRes[pstLockPara->id].lock )
        {
            //  如果标识的waitlock设置成FALSE或者等待超时，则返回错误
            if( !unflags.flags.waitlock || !vxi_wait_for_lock( &pstLockPara->id , pstLockPara->lock_timeout , &result.error ) )
            {
                result.error = LXI_VXI_DEVICE_LOCKED_BY_ANOTHER_LINK;
                return &result;
            }
        }
    }
    if( LXI_VXI_NO_ERROR == result.error)
    {
        //  置当前VXI设备被锁定的标识
        m_bVxiDeviceLock = TRUE;
        //  置当前接口的锁定状态为TRUE
        m_astVxiRes[pstLockPara->id].lock = TRUE;
    }

    return &result;
}

/*
 *处理客户端的VXI设备解锁的命令
*/
DEVICE_ERROR_STRU   *vxi_unlock( DEVICE_LINK *pid )
{
    static DEVICE_ERROR_STRU       result  = { 0 };

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    /*判断通信ID是否存在*/
    if( !vxi_link_id_check( *pid ) )
    {
        result.error = LXI_VXI_INVALID_LINK_IDENTIFIER;
        return &result;
    }

    /*判断该通信ID是锁定了VXI设备*/
    if( TRUE == m_astVxiRes[*pid].lock )
    {
        /*释放锁定*/
        m_astVxiRes[*pid].lock = FALSE;
        m_bVxiDeviceLock     = FALSE;
        result.error = LXI_VXI_NO_ERROR;
    }
    else
    {
        result.error = LXI_VXI_NO_LOCK_HELD_BY_THIS_LINK;
    }

    return &result;
}

/*
 * 处理客户端建立高速通信接口的请求
*/
DEVICE_ERROR_STRU *vxi_create_intr_chan( DEVICE_REMOTE_PARA_STRU *pstRmtPara )
{
    static DEVICE_ERROR_STRU result;

    pstRmtPara = pstRmtPara;
    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    result.error = LXI_VXI_OPERATION_NOT_SUPPORTED;

    return &result;
}

/*
 * 处理销毁高速通信接口的请求
*/
DEVICE_ERROR_STRU *vxi_destroy_intr_chan( void )
{
    static DEVICE_ERROR_STRU result;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    result.error = LXI_VXI_CHANNEL_NOT_ESTABLISHED;

    return &result;
}

/*
 * 处理客户端打开/关闭intr的RPC服务
*/
DEVICE_ERROR_STRU *vxi_enable_srq( DEVICE_ENABLE_SRQ_PARA_STRU *pstSrqPara )
{
    static DEVICE_ERROR_STRU result;

    pstSrqPara = pstSrqPara;
    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    return &result;
}

/*
 * 处理客户端的DO CMD请求
*/
DEVICE_DOCMD_RESP_STRU *vxi_docmd( DEVICE_DOCMD_PARA_STRU *pstDoCmdPara )
{
    static DEVICE_DOCMD_RESP_STRU result;

    pstDoCmdPara = pstDoCmdPara;
    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_DOCMD_RESP_STRU ) );

    result.error = LXI_VXI_OPERATION_NOT_SUPPORTED;

    return &result;
}

/*
 * 处理客户端abort的请求
*/
DEVICE_ERROR_STRU *vxi_abort( DEVICE_LINK *pid )
{
    static DEVICE_ERROR_STRU result;

    /*初始化返回结果*/
    memset( (void*)&result , '\0' , sizeof( DEVICE_ERROR_STRU ) );

    m_astVxiRes[*pid].abort = TRUE;

    result.error = LXI_VXI_NO_ERROR;

    return &result;
}

/*
 * 处理客户端的intr-srq的命令
*/
void vxi_intr_srq( DEVICE_SRQ_PARA_STRU *pstSrqPara )
{
    pstSrqPara = pstSrqPara;
}
