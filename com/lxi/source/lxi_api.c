/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: lxi_api.c
  功能描述: LXI模块的接口定义

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-02

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-02          1.0        initial

*******************************************************************************/
#include    <string.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <arpa/inet.h>
#include    "lxi_api.h"
#include    "lxi_arch.h"
#include    "lxi_web.h"
//#include    "lxi_socket.h"
#include    "lxi_mdns.h"
#include    "lxi_vxi.h"
#include    "lxi_net.h"

/*定义当前LXI模块的版本号*/
s8                          m_as8LxiVersion[LXI_MODULE_VERSION_MAX_LEN] = "00.03.00.01";

/*定义LXI模块的配置参数变量*/
LXI_PARAMETER_INFO_STRU     m_stLxiConfPara = {
                                                /*u8DhcpEnable     = */true,
                                                /*u8AutoIpEnable   = */true,
                                                /*u8ManualIpEnable = */false,
                                                /*u8MdnsEnable     = */false,
                                                /*emIpMode        = */LXI_IP_CONFIG_MODE_NONE,
                                                /*s32IpAddr       = */0,
                                                /*s32GateAddr     = */0,
                                                /*s32MaskAddr     = */0,
                                                /*s32DnsAddr      = */{0},
                                                /*as8MacAddr      = */{0x00,0x11,0x22,0x33,0x44,0x55},
                                                /*as8HostName     = */"lxi_host",
                                                /*as8ServiceName  = */"lxi_service"
                                              };
/*定义LXI模块的初始化参数变量*/
LXI_INIT_PARAMETER_STRU     m_stLxiInitPara ;

/*定义完成声明的mDNS主机名和服务名*/
s8                          m_as8DHostName[LXI_MDNS_NAME_MAX_LEN] = "test-service";
s8                          m_as8DServiceName[LXI_MDNS_NAME_MAX_LEN] = "test-host";

/*定义仪器相关信息*/
s8                          m_as8Manufacture[LXI_DEVICE_PRODUCE_INFO_MAX_LEN]      = "Rigol Technologies Inc";
s8                          m_as8InstrumentModel[LXI_DEVICE_PRODUCE_INFO_MAX_LEN]  = "LXI Model";
s8                          m_as8SerialNum[LXI_DEVICE_PRODUCE_INFO_MAX_LEN]        = "LXI-20171108";
s8                          m_as8FirmwareVersion[LXI_DEVICE_PRODUCE_INFO_MAX_LEN]  = "00.00.01.00.01";
s8                          m_as8InstrumentSeries[LXI_DEVICE_PRODUCE_INFO_MAX_LEN] = "LXI";
s8                          m_as8UsbVisaAddr[LXI_DEVICE_PRODUCE_INFO_MAX_LEN]      = "USB";
/*定义Web Service服务密码*/
s8                          m_as8WebPassword[LXI_HTTP_SERVICE_PASSWORD_MAX_LEN]    = "Rigol1998";

/*定义当前网络的状态*/
LXI_STATUS_EM               m_emLxiCurrState = LXI_STATUS_UNLINK;

/*记录当前网络状态是否发生变化*/
s8                          m_s8NetStateIsChange    = false;

/*记录当前选中的网络适配器名称*/
s8                          m_as8AdapterName[LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN];
/*记录Socket端口号*/
u32                         m_u32SocketPort = 5555;
/*定义Flash读写操作的变量*/
LXI_FLASH_SAVE_INFO_STRU    m_stLxiFlashInfo = { 0 };
/*定义记录当前LXI发生了网络配置事件*/
u8                          m_u8LxiReConfig = 0 ;
/*定义记录DHCP服务器IP地址的变量*/
s32                         m_s32LxiDhcpServer = 0;
/*定义当前网络环境的域名*/
s8                          m_as8CurrDomainName[LXI_DOMAIN_NAME_MAX_LEN] = { 0 };
/*定义当前网卡自适应后的网络带宽*/
LXI_NETWORK_SPEED_EM        m_emCurrNetSpeed = LXI_NETWORK_SPEED_10M;
/*定义当前网卡自适应后的工作模式*/
LXI_NETWORK_WORK_MODE_EM    m_emCurrNetWorkMode = LXI_NETWORK_FULL_WORK;

/*
 * LXI_Open
 * 该函数提供给项目组使用，供项目组进行LXI模块的初始化
 * 该函数内部启动各网络服务，并启动NET内核
 * Parameter:   stInitPara   --  初始化配置参数
*/
void LXI_Open(LXI_INIT_PARAMETER_STRU  * pstInitPara)
{
//    lxi_pthread_t   id;

    /*首先获取LXI模块的初始化参数*/
    if( NULL != pstInitPara )
    {
        m_stLxiInitPara = *pstInitPara;

        /*
         * 获取仪器的相关信息
        */
        if( NULL != m_stLxiInitPara.pfunServiceCallback )
        {
            m_stLxiInitPara.pfunServiceCallback( m_as8Manufacture ,
                                                 m_as8SerialNum,
                                                 m_as8InstrumentModel,
                                                 m_as8FirmwareVersion,
                                                 m_as8InstrumentSeries,
                                                 m_as8UsbVisaAddr,
                                                 LXI_DEVICE_PRODUCE_INFO_MAX_LEN);
        }

        /*
         * 组织结构体初始化网络，调用Flash接口获取LXI配置初始化信息
        */
        if( NULL != m_stLxiInitPara.pfunFlashCallback )
        {
            /*读取Flash信息*/
            memset( (s8*)&m_stLxiFlashInfo , '\0' , sizeof( LXI_FLASH_SAVE_INFO_STRU ) );
            m_stLxiInitPara.pfunFlashCallback( LXI_FLASH_READ , &m_stLxiFlashInfo );
            
            /*更新到LXI模块*/
            memset( (s8*)&m_stLxiConfPara , '\0' , sizeof( LXI_PARAMETER_INFO_STRU ) );
            m_stLxiConfPara.u8DhcpEnable     = m_stLxiFlashInfo.u8DhcpEnable;           /*获取DHCP配置开关*/
            m_stLxiConfPara.u8AutoIpEnable   = m_stLxiFlashInfo.u8AutoipEnable;         /*获取AUTOIP配置开关*/
            m_stLxiConfPara.u8ManualIpEnable = m_stLxiFlashInfo.u8ManualIpEnable;       /*获取Manual IP配置开关*/
            m_stLxiConfPara.u8MdnsEnable     = m_stLxiFlashInfo.u8MdnsEnable;           /*获取mNDS服务开关*/
            m_stLxiConfPara.s32IpAddr        = m_stLxiFlashInfo.u32IpAddr;              /*获取IP地址*/
            m_stLxiConfPara.s32GateAddr      = m_stLxiFlashInfo.u32GatewayAddr;         /*获取网关*/
            m_stLxiConfPara.s32MaskAddr      = m_stLxiFlashInfo.u32SubMaskAddr;         /*获取子网掩码*/
            m_stLxiConfPara.s32DnsAddr[0]    = m_stLxiFlashInfo.u32PreferDnsAddr;       /*获取DNS1地址*/
            m_stLxiConfPara.s32DnsAddr[1]    = m_stLxiFlashInfo.u32AlternativeDnsAddr;  /*获取DNS2地址*/
            m_u32SocketPort                  = m_stLxiFlashInfo.u32SocketPort;          /*获取Socket端口号*/
            strncpy( m_stLxiConfPara.as8MacAddr , m_stLxiFlashInfo.as8MacAddr , LXI_MAC_ADDR_MAX_LEN ); /*获取MAC地址*/
            /*获取mDNS主机名*/
            if( strlen(m_stLxiFlashInfo.as8HostSettingName) == 0 || strlen(m_stLxiFlashInfo.as8HostSettingName) > LXI_MDNS_NAME_MAX_LEN )
            {
                sprintf(m_stLxiConfPara.as8HostName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum);
            }
            else
            {
                strcpy( m_stLxiConfPara.as8HostName , m_stLxiFlashInfo.as8HostSettingName );
            }
            /*获取mDNS声明后的主机名*/
            if( strlen(m_stLxiFlashInfo.as8HostDeclareName) == 0 || strlen(m_stLxiFlashInfo.as8HostDeclareName) > LXI_MDNS_NAME_MAX_LEN )
            {
                sprintf(m_as8DHostName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum);
            }
            else
            {
                strcpy( m_as8DHostName , m_stLxiFlashInfo.as8HostDeclareName );
            }
            /*获取mDNS服务名*/
            if( strlen(m_stLxiFlashInfo.as8ServiceSettingName) == 0 || strlen(m_stLxiFlashInfo.as8ServiceSettingName) > LXI_MDNS_NAME_MAX_LEN )
            {
                sprintf(m_stLxiConfPara.as8ServiceName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum);
            }
            else
            {
                strcpy( m_stLxiConfPara.as8ServiceName , m_stLxiFlashInfo.as8ServiceSettingName );
            }
            /*获取mDNS声明后的服务名*/
            if( strlen(m_stLxiFlashInfo.as8ServiceDeclareName) == 0 || strlen(m_stLxiFlashInfo.as8ServiceDeclareName) > LXI_MDNS_NAME_MAX_LEN )
            {
                sprintf(m_as8DServiceName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum);
            }
            else
            {
                strcpy( m_as8DServiceName , m_stLxiFlashInfo.as8ServiceDeclareName );
            }
            /*获取Web密码*/
            if( 0 != strlen(m_stLxiFlashInfo.as8WebSettingPassword) && strlen(m_stLxiFlashInfo.as8WebSettingPassword) < LXI_HTTP_SERVICE_PASSWORD_MAX_LEN )
            {
                strcpy( m_as8WebPassword , m_stLxiFlashInfo.as8WebSettingPassword );
            }
            /*获取网络适配器名*/
            if( 0 == strlen(m_stLxiFlashInfo.as8WebSettingPassword) || strlen(m_stLxiFlashInfo.as8WebSettingPassword) > LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN )
            {
                strcpy( m_as8AdapterName , "eth0" );
            }
            else
            {
                strcpy( m_as8AdapterName , m_stLxiFlashInfo.as8DefauleNetAdapterName );
            }

            /*更新网页显示信息*/
            m_astWebPageInfo[LXI_PAGE_TYPE_IMAGES_INSTRUMENT_LOGO].page = m_stLxiInitPara.ps8InstrumentLogo;
            m_astWebPageInfo[LXI_PAGE_TYPE_IMAGES_INSTRUMENT_LOGO].size = m_stLxiInitPara.s32InstLogoSize;
            m_astWebPageInfo[LXI_PAGE_TYPE_WEB_CONTROL].page            = m_stLxiInitPara.ps8WebControlSrc;
            m_astWebPageInfo[LXI_PAGE_TYPE_WEB_CONTROL].size            = m_stLxiInitPara.s32WebControlSrcLen;
            m_astWebPageInfo[LXI_PAGE_TYPE_CSS_WEB_CONTROL].page        = m_stLxiInitPara.ps8WebControlCss;
            m_astWebPageInfo[LXI_PAGE_TYPE_CSS_WEB_CONTROL].size        = m_stLxiInitPara.s32WebControlCssLen;

            /*开始LXI模块线程的启动*/
            lxi_net_init( m_as8AdapterName , &m_stLxiConfPara );    //  先进行IP配置的初始化
            //lxi_thread_create( &id , lxi_vxi_thread , NULL );  //  VXI-11 Service启动
            //lxi_thread_create( &id , lxi_web_service_thread , NULL );    //  Web Server 启动
            lxi_mdns_start();  //  mDNS Service 启动
        }
    }
}

/*
*  Flash配置结构体信息刷新,并配置到Flash中
*/
void lxi_flash_info_save( void )
{
    /*更新功能使能配置值*/
    m_stLxiFlashInfo.u8DhcpEnable              = m_stLxiConfPara.u8DhcpEnable;      /*更新DHCP配置开关*/
    m_stLxiFlashInfo.u8AutoipEnable            = m_stLxiConfPara.u8AutoIpEnable;    /*更新AUTOIP配置开关*/
    m_stLxiFlashInfo.u8ManualIpEnable          = m_stLxiConfPara.u8ManualIpEnable;  /*更新Manual IP配置开关*/
    m_stLxiFlashInfo.u8MdnsEnable              = m_stLxiConfPara.u8MdnsEnable;      /*更新mNDS服务开关*/
    /*只有手动模式才进行IP地址信息的更新*/
    if( 0 == m_stLxiConfPara.u8DhcpEnable && 0 == m_stLxiConfPara.u8AutoIpEnable )
    {
        m_stLxiFlashInfo.u32IpAddr             = m_stLxiConfPara.s32IpAddr;         /*更新IP地址*/
        m_stLxiFlashInfo.u32GatewayAddr        = m_stLxiConfPara.s32GateAddr;       /*更新网关地址*/
        m_stLxiFlashInfo.u32SubMaskAddr        = m_stLxiConfPara.s32MaskAddr;       /*更新子网掩码*/
        m_stLxiFlashInfo.u32PreferDnsAddr      = m_stLxiConfPara.s32DnsAddr[0];     /*更新首选DNS地址*/
        m_stLxiFlashInfo.u32AlternativeDnsAddr = m_stLxiConfPara.s32DnsAddr[1];     /*更新备选DNS地址*/
    }
    /*更新主机名、服务名*/
    if( 1 == m_stLxiConfPara.u8MdnsEnable )
    {
        memset( m_stLxiFlashInfo.as8HostSettingName    , '\0' , LXI_MDNS_NAME_MAX_LEN );
        memset( m_stLxiFlashInfo.as8HostDeclareName    , '\0' , LXI_MDNS_NAME_MAX_LEN );
        memset( m_stLxiFlashInfo.as8ServiceSettingName , '\0' , LXI_MDNS_NAME_MAX_LEN );
        memset( m_stLxiFlashInfo.as8ServiceDeclareName , '\0' , LXI_MDNS_NAME_MAX_LEN );
        strcpy( m_stLxiFlashInfo.as8HostSettingName    , m_stLxiConfPara.as8HostName );
        strcpy( m_stLxiFlashInfo.as8HostDeclareName    , m_as8DHostName );
        strcpy( m_stLxiFlashInfo.as8ServiceSettingName , m_stLxiConfPara.as8ServiceName );
        strcpy( m_stLxiFlashInfo.as8ServiceDeclareName , m_as8DServiceName );
    }
    /*更新MAC地址*/
    memcpy( m_stLxiFlashInfo.as8MacAddr , m_stLxiConfPara.as8MacAddr , LXI_MAC_ADDR_MAX_LEN );
    /*更新网页配置密码*/
    memset( m_stLxiFlashInfo.as8WebSettingPassword , '\0' , LXI_HTTP_SERVICE_PASSWORD_MAX_LEN );
    strcpy( m_stLxiFlashInfo.as8WebSettingPassword , m_as8WebPassword );
    /*更新网络适配器名字*/
    memset( m_stLxiFlashInfo.as8DefauleNetAdapterName , '\0' , LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN );
    strcpy( m_stLxiFlashInfo.as8DefauleNetAdapterName , m_as8AdapterName );
    /*更新Socket端口号*/
    m_stLxiFlashInfo.u32SocketPort = m_u32SocketPort;
    
    /*调用Flash操作接口函数进行写操作*/
    if( NULL != m_stLxiInitPara.pfunFlashCallback )
    {
        m_stLxiInitPara.pfunFlashCallback( LXI_FLASH_WRITE , &m_stLxiFlashInfo );
    }
}
/*
 * LXI_Option
 * 该函数提供给项目组使用，供项目组进行UI界面的更新、SCPI命令的处理以及仪器状态的上报
 * Parameter1:  emOptionId  --  操作码值
 * Parameter2:  pParameter  --  参数
 * 获取LXI模块版本操作，Parameter2类型为 s8*
 * 配置LXI和获取LXI操作，Parameter2类型为    LXI_PARAMETER_INFO_STRU*
 * 仪器状态上报，Parameter2类型为   s8*
 * 网络状态上报，Parameter2类型为   LXI_STATUS_EM *
*/
void LXI_Option(LXI_MODULE_OPTION_EM emOptionId  ,   void *pParameter)
{
    LXI_PARAMETER_INFO_STRU                *stLxiConfigPara ;

    /*进行LXI操作的判定*/
    switch( emOptionId )
    {
        case LXI_MODULE_VERSION_ACHIEVE:    //  获取LXI模块的版本号
            memcpy( pParameter , m_as8LxiVersion , LXI_MODULE_VERSION_MAX_LEN );
            break;
        case LXI_MODULE_PARAMETER_CONFIGURATION:
            if( NULL != pParameter )
            {
                stLxiConfigPara = (LXI_PARAMETER_INFO_STRU*)pParameter;
                //  复制IP配置方式
                if( stLxiConfigPara->u8DhcpEnable != m_stLxiConfPara.u8DhcpEnable )
                {
                    m_stLxiConfPara.u8DhcpEnable   = stLxiConfigPara->u8DhcpEnable;
                }
                if( m_stLxiConfPara.u8AutoIpEnable != stLxiConfigPara->u8AutoIpEnable )
                {
                    m_stLxiConfPara.u8AutoIpEnable = stLxiConfigPara->u8AutoIpEnable;
                }
                if( m_stLxiConfPara.u8ManualIpEnable != stLxiConfigPara->u8ManualIpEnable )
                {
                    m_stLxiConfPara.u8ManualIpEnable = stLxiConfigPara->u8ManualIpEnable;
                }
                if( m_stLxiConfPara.u8DhcpEnable == false && m_stLxiConfPara.u8AutoIpEnable == false )
                {
                    if( m_stLxiConfPara.s32IpAddr != stLxiConfigPara->s32IpAddr )
                    {
                        m_stLxiConfPara.s32IpAddr = stLxiConfigPara->s32IpAddr;
                    }
                    if( m_stLxiConfPara.s32MaskAddr != stLxiConfigPara->s32MaskAddr )
                    {
                        m_stLxiConfPara.s32MaskAddr = stLxiConfigPara->s32MaskAddr;
                    }
                    if( m_stLxiConfPara.s32GateAddr != stLxiConfigPara->s32GateAddr )
                    {
                        m_stLxiConfPara.s32GateAddr = stLxiConfigPara->s32GateAddr;
                    }
                    if( m_stLxiConfPara.s32DnsAddr[0] != stLxiConfigPara->s32DnsAddr[0] )
                    {
                        m_stLxiConfPara.s32DnsAddr[0] = stLxiConfigPara->s32DnsAddr[0];
                    }
                    if( m_stLxiConfPara.s32DnsAddr[1] != stLxiConfigPara->s32DnsAddr[1] )
                    {
                        m_stLxiConfPara.s32DnsAddr[1] = stLxiConfigPara->s32DnsAddr[1];
                    }
                }
                if( ( 0 != strlen( stLxiConfigPara->as8HostName ) ) &&
                    ( ( 0 != strcmp( m_stLxiConfPara.as8HostName , stLxiConfigPara->as8HostName ) ) ||
                      ( 0 != strcmp( m_as8DHostName , stLxiConfigPara->as8HostName ) )
                    )
                  )
                {
                    memset( m_stLxiConfPara.as8HostName , '\0' , LXI_MDNS_NAME_MAX_LEN );
                    memset( m_as8DHostName , '\0' , LXI_MDNS_NAME_MAX_LEN );

                    strcpy( m_stLxiConfPara.as8HostName , stLxiConfigPara->as8HostName );
                    strcpy( m_as8DHostName , stLxiConfigPara->as8HostName );
                }
                if( ( 0 != strlen( stLxiConfigPara->as8ServiceName ) ) &&
                    ( ( 0 != strcmp( m_stLxiConfPara.as8ServiceName , stLxiConfigPara->as8ServiceName ) ) ||
                      ( 0 != strcmp( m_as8DServiceName , stLxiConfigPara->as8ServiceName ) )
                    )
                  )
                {
                    memset( m_stLxiConfPara.as8ServiceName , '\0' , LXI_MDNS_NAME_MAX_LEN );
                    memset( m_as8DServiceName , '\0' , LXI_MDNS_NAME_MAX_LEN );

                    strcpy( m_stLxiConfPara.as8ServiceName , stLxiConfigPara->as8ServiceName );
                    strcpy( m_as8DServiceName , stLxiConfigPara->as8ServiceName );
                }
                if( m_stLxiConfPara.u8MdnsEnable != stLxiConfigPara->u8MdnsEnable )
                {
                    m_stLxiConfPara.u8MdnsEnable = stLxiConfigPara->u8MdnsEnable;
                }
            }
            else
            {
                m_stLxiConfPara.u8DhcpEnable     = true;
                m_stLxiConfPara.u8AutoIpEnable   = true;
                m_stLxiConfPara.u8ManualIpEnable = false;
                m_stLxiConfPara.u8MdnsEnable     = true;
                memset( m_stLxiConfPara.as8HostName , 0 , LXI_MDNS_NAME_MAX_LEN );
                memset( m_stLxiConfPara.as8ServiceName , 0 , LXI_MDNS_NAME_MAX_LEN );
                memset( m_as8DHostName , 0 , LXI_MDNS_NAME_MAX_LEN );
                memset( m_as8DServiceName , 0 , LXI_MDNS_NAME_MAX_LEN );
                memset( m_as8WebPassword , 0 , LXI_HTTP_SERVICE_PASSWORD_MAX_LEN );
                sprintf( m_stLxiConfPara.as8HostName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum );
                sprintf( m_stLxiConfPara.as8ServiceName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum );
                sprintf( m_as8DHostName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum);
                sprintf( m_as8DServiceName , "%s_%s" , m_as8InstrumentModel , m_as8SerialNum );
            }
            //  置当前网络状态为网线未接入状态
            m_emLxiCurrState = LXI_STATUS_UNLINK;
            //  配置到NET层
            lxi_net_config( &m_stLxiConfPara );
            //  置当前网络发生了配置事件
            m_u8LxiReConfig = 1;
            break;
        case LXI_MODULE_PARAMETER_ACHIEVE:                //  获取LXI参数,供UI和SCPI模块调用
            *(LXI_PARAMETER_INFO_STRU*)pParameter = m_stLxiConfPara;
            break;
        case LXI_MODULE_SOCKET_SERVICE_PORT_CHANGE:       //  配置当前socket服务的端口号，供UI和SCPI模块使用
            m_u32SocketPort = *(s32*)pParameter;
            break;
        case LXI_MODULE_SOCKET_SERVICE_PORT_ACHIEVE:      //  获取当前socket服务的端口号，供UI和SCPI模块使用
            *(s32*)pParameter = m_u32SocketPort;
            break;
        case LXI_MODULE_INSTRUMENT_STATUS_REPORT:         //  仪器状态上报,供项目功能模块使用

            break;
        case LXI_MODULE_NETWORK_STATUS_REPORT:            //  供Net层使用，进行当前网络连接状态上报

            //  初始化当前网络配置模式为NONE
            m_stLxiConfPara.emIpMode = LXI_IP_CONFIG_MODE_NONE;

            switch( *(LXI_STATUS_EM*)pParameter )
            {
                case LXI_STATUS_NOTHING_NETWORK_ADAPTOR:   //  当前没有网络适配器
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_NO_ADAPTER );
                    }
                    break;
                case LXI_STATUS_UNLINK:                     //  网线未连接
                    m_emLxiCurrState = LXI_STATUS_UNLINK;
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_CABLE_UNLINK );
                    }
                    break;
                case LXI_STATUS_LINK:                            //  网线接入
                    m_emLxiCurrState = LXI_STATUS_LINK;
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_CABLE_LINK );
                    }
                    break;
                case LXI_STATUS_INIT:                            //  网络初始化过程
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_NETWORK_INITIALIZATION );
                    }
                    m_emLxiCurrState = LXI_STATUS_INIT;
                    break;
                case LXI_STATUS_CONFIG_SUCCESS:                  //  网络配置成功
                    //  向NET要网络相关参数
                    lxi_net_para_get( &m_stLxiConfPara );
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_NETWORK_SUCCESS );
                    }
                    m_emLxiCurrState = LXI_STATUS_CONFIG_SUCCESS;
                    //  置网络状态发生改变
                    m_s8NetStateIsChange = true;
                    //  如果mDNS服务没有启动,则这里进行Flash信息的保存
                    if( 0 == m_stLxiConfPara.u8MdnsEnable && 1 == m_u8LxiReConfig )
                    {

                        m_u8LxiReConfig = 0;
                    }
                    lxi_flash_info_save();
                    break;
                case LXI_STATUS_DHCP_FAIL:                       //  从DHCP服务器获取IP失败
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_DHCP_IP_ACHIEVE_FAIL );
                    }
                    m_emLxiCurrState = LXI_STATUS_DHCP_FAIL;
                    break;
                case LXI_STATUS_DHCP_IP_LOSE:                    //  从DHCP分配到的IP丢失,也就是DHCP服务器关闭或者出现故障
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_DHCP_IP_LOSE );
                    }
                    m_emLxiCurrState = LXI_STATUS_DHCP_IP_LOSE;
                    break;
                case LXI_STATUS_IP_CONFLICT:                     //  IP地址冲突
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_MANUAL_IP_CONFLICT );
                    }
                    m_emLxiCurrState = LXI_STATUS_IP_CONFLICT;
                    break;
                case LXI_STATUS_INVALID_IP_ADDR:                  //  无效的IP地址
                    //  通知UI层
                    if( NULL != m_stLxiInitPara.pfunUiCallback )
                    {
                        m_stLxiInitPara.pfunUiCallback( LXI_UI_INVALID_IP_ADDR );
                    }
                    m_emLxiCurrState = LXI_STATUS_INVALID_IP_ADDR;
                    break;
                case LXI_STATUS_MDNS_PROBE_SUCCESS:             //  mDNS服务探测成功
                    //  进行Flash信息的保存
                    if( 1 == m_u8LxiReConfig )
                    {
                        lxi_flash_info_save();
                        m_u8LxiReConfig = 0 ;
                    }
                    break;
                default:
                    break;
            }
            break;
        case LXI_MODULE_NETWORK_ADAPTER_INFO_ACHIEVE:     //  供UI层使用,查询当前拥有的网络适配器的名字
            lxi_net_device_info( (LXI_NETWORK_ADAPTOR_INFO_STRU*)pParameter );
            break;
        case LXI_MODULE_ADAPTER_SELECT:                   //  供UI层和SCPI模块使用，选中网络适配器
            //  选中网络适配器
            lxi_net_device_select( pParameter );
            break;
        case LXI_MODULE_FLASH_TO_SAVE:                      //  LXI模块内部使用,进行Flash信息保存
            lxi_flash_info_save();
            break;
    }
}
