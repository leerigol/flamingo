/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: lxi_vxi.c
  功能描述: LXI模块中Vxi-11功能的实现

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-02

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-02          1.0        initial

*******************************************************************************/
#include    <unistd.h>
#include    <sys/socket.h>
#include    <netinet/in.h>
#include    "DataType.h"
#include    "lxi_vxi.h"
#include    "svc.h"
#include    "pmap_prot.h"
#include    "svc_vxi.h"

/*定义VXI-11命令对应的回调函数类型*/
typedef void *(*vxi_process_callback)( void *);

void lxi_vxi_service(  register struct svc_req* pstSvcReqPara , register SVCXPRT* xprt )
{
    union{
        DEVICE_LINK_PARA_STRU       link_arg;
        DEVICE_WRITE_PARA_STRU      write_arg;
        DEVICE_READ_PARA_STRU       read_arg;
        DEVICE_GENERIC_PARA_STRU    stb_arg;
        DEVICE_GENERIC_PARA_STRU    trig_arg;
        DEVICE_GENERIC_PARA_STRU    clear_arg;
        DEVICE_GENERIC_PARA_STRU    remote_arg;
        DEVICE_GENERIC_PARA_STRU    local_arg;
        DEVICE_LOCK_PARA_STRU       lock_arg;
        DEVICE_LINK                 unlock_arg;
        DEVICE_ENABLE_SRQ_PARA_STRU ensrq_arg;
        DEVICE_DOCMD_PARA_STRU      docmd_arg;
        DEVICE_LINK                 destroy_link_arg;
        DEVICE_REMOTE_PARA_STRU     intr_chan_arg;
    }parameter;

    void                   *result;
    xdrproc_t               parameter_get , response_process;
    vxi_process_callback    local;

    /*开始VXI-11命令的处理*/
    switch( pstSvcReqPara->rq_proc )
    {
        case LXI_VXI_CMD_NULL:
            svc_sendreply( xprt , (xdrproc_t)xdr_void , NULL );
            return;
        case LXI_VXI_CMD_CREATE_LINK:
            parameter_get    = (xdrproc_t)vxi_device_link_para;
            response_process = (xdrproc_t)vxi_device_link_resp;
            local            = (vxi_process_callback)vxi_create_link;
            break;
        case LXI_VXI_CMD_WRITE:
            parameter_get    = (xdrproc_t)vxi_device_write_para;
            response_process = (xdrproc_t)vxi_device_write_resp;
            local            = (vxi_process_callback)vxi_write;
            break;
        case LXI_VXI_CMD_READ:
            parameter_get    = (xdrproc_t)vxi_device_read_para;
            response_process = (xdrproc_t)vxi_device_read_resp;
            local            = (vxi_process_callback)vxi_read;
            break;
        case LXI_VXI_CMD_READ_STB:
            parameter_get    = (xdrproc_t)vxi_device_generic_para;
            response_process = (xdrproc_t)vxi_device_stb_resp;
            local            = (vxi_process_callback)vxi_readstb;
            break;
        case LXI_VXI_CMD_TRIGGER:
            parameter_get    = (xdrproc_t)vxi_device_generic_para;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_trigger;
            break;
        case LXI_VXI_CMD_CLEAR:
            parameter_get    = (xdrproc_t)vxi_device_generic_para;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_clear;
            break;
        case LXI_VXI_CMD_REMOTE:
            parameter_get    = (xdrproc_t)vxi_device_generic_para;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_remote;
            break;
        case LXI_VXI_CMD_LOCAL:
            parameter_get    = (xdrproc_t)vxi_device_generic_para;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_local;
            break;
        case LXI_VXI_CMD_LOCK:
            parameter_get    = (xdrproc_t)vxi_device_lock_para;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_lock;
            break;
        case LXI_VXI_CMD_UNLOCK:
            parameter_get    = (xdrproc_t)vxi_device_link;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_unlock;
            break;
        case LXI_VXI_CMD_ENABLE_SRQ:
            parameter_get    = (xdrproc_t)vxi_device_enable_srq_para;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_enable_srq;
            break;
        case LXI_VXI_CMD_DO_CMD:
            parameter_get    = (xdrproc_t)vxi_device_docmd_para;
            response_process = (xdrproc_t)vxi_device_docmd_resp;
            local            = (vxi_process_callback)vxi_docmd;
            break;
        case LXI_VXI_CMD_DESTROY_LINK:
            parameter_get    = (xdrproc_t)vxi_device_link;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_destroy_link;
            break;
        case LXI_VXI_CMD_CREATE_INTR_CHAN:
            parameter_get    = (xdrproc_t)vxi_device_remote_para;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_create_intr_chan;
            break;
        case LXI_VXI_CMD_DESTROY_INTR_CHAN:
            parameter_get    = (xdrproc_t)xdr_void;
            response_process = (xdrproc_t)vxi_device_error;
            local            = (vxi_process_callback)vxi_destroy_intr_chan;
            break;
        default:
            svcerr_noproc( xprt );
            return;
    }
    /*根据上面的命令处理结果，进行函数的执行*/
    //  初始化参数空间
    memset((char*)&parameter , '\0' , sizeof( parameter ) );
    //  获取参数
    if( !svc_getargs( xprt , parameter_get , (caddr_t)&parameter ) )
    {
        svcerr_decode(xprt);
        return;
    }
    //  执行回调函数获取响应值
    result = local(&parameter);
    //  组织响应报文并发送
    if( NULL != result && !svc_sendreply( xprt , response_process , (caddr_t)result ) )
    {
        svcerr_systemerr(xprt);
    }
    //  释放资源
    if( !svc_freeargs( xprt , parameter_get , (caddr_t)&parameter ) )
    {
        return;
    }
    return;
}


void lxi_vxi_resource_discovery_service( register struct svc_req* pstSvcReqPara , register SVCXPRT* xprt)
{
    struct  pmap    reg;    //  记录当前通信的RPC信息
    static  s32     port;   //  VXI-11 Core 端口号
    /*
     * 解析远程程序调用的命令,根据VISA协议中关于VXI-11的使用，目前只提供端口获取服务
    */
    switch (pstSvcReqPara->rq_proc)
    {
        case PMAPPROC_GETPORT:

            if( !svc_getargs( xprt , (xdrproc_t)xdr_pmap , (caddr_t )&reg ) )
            {
                svcerr_decode(xprt);
            }
            else
            {
                if( reg.pm_prog == LXI_VXI_CORE_PROGRAM )
                {
                    port = LXI_VXI_CORE_PORT;
                }
                else
                {
                    port = 0;
                }
                if( !svc_sendreply( xprt , (xdrproc_t)xdr_int , (caddr_t)&port ) )
                {
                    printf("rpc reply info send fail !\n");
                    return;
                }
            }
            break;
        default:
            svcerr_noproc(xprt);
            break;
    }
}

/*
 * VXI-11功能的执行函数
*/
void * lxi_vxi_thread( void *pInitPara )
{
    struct  sockaddr_in udpaddr , tcpaddr , coreaddr;
    s32                 udpsock , tcpsock , coresock;
    s32                 sockoptcode = 1;
    SVCXPRT            *xprt;

    pInitPara= pInitPara;
    /*
     * 初始化UDP、TCP和VXI-11 Core 的socket绑定地址
    */
    bzero((void*)&udpaddr,sizeof(udpaddr));
    udpaddr.sin_addr.s_addr =   htonl(INADDR_ANY);
    udpaddr.sin_family      =   AF_INET;
    udpaddr.sin_port        =   htons(PMAPPORT);

    //  TCP
    bzero((void*)&tcpaddr,sizeof(tcpaddr));
    tcpaddr.sin_addr.s_addr =   htonl(INADDR_ANY);
    tcpaddr.sin_family      =   AF_INET;
    tcpaddr.sin_port        =   htons(PMAPPORT);

    //  VXI-11 Core
    bzero((void*)&coreaddr,sizeof(coreaddr));
    coreaddr.sin_addr.s_addr =   htonl(INADDR_ANY);
    coreaddr.sin_family      =   AF_INET;
    coreaddr.sin_port        =   htons(LXI_VXI_CORE_PORT);

    /*
     * 创建基于UDP协议的RPC查询服务
    */
    //  申请socket资源
    if( -1 == ( udpsock = socket( AF_INET , SOCK_DGRAM , IPPROTO_UDP ) ) )
    {
        printf("cannot create udp socket !\n");
        return NULL;
    }
    //  设置socket的属性--允许当前地址重复使用
    setsockopt( udpsock , SOL_SOCKET , SO_REUSEADDR , &sockoptcode , sizeof(sockoptcode));
    //  给udpsocket进行地址绑定
    if( 0 != bind( udpsock , (struct sockaddr *)&udpaddr , sizeof(struct sockaddr_in) ) )
    {
        printf("cannot bind udp socket !\n");
        return NULL;
    }
    //  申请UDP RPC服务资源
    if( (SVCXPRT*)NULL == ( xprt = svcudp_create(udpsock) ) )
    {
        printf("udp rpc service create fail !\n");
        return NULL;
    }
    //  向UDP RPC服务注册处理函数
    if( 0 == svc_register( xprt , PMAPPROG , PMAPVERS , lxi_vxi_resource_discovery_service , false ) )
    {
        printf("udp rpc service process callback register fail !\n");
        return NULL;
    }

    /*
     * 创建基于TCP协议的RPC查询服务
    */
    //  申请socket资源
    if( -1 == ( tcpsock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ) )
    {
        printf("cannot create tcp socket !\n");
        return NULL;
    }
    //  设置socket的属性--允许当前地址重复使用
    setsockopt( tcpsock , SOL_SOCKET , SO_REUSEADDR , &sockoptcode , sizeof(sockoptcode));
    //  给tcpsocket进行地址绑定
    if( 0 != bind( tcpsock , (struct sockaddr *)&tcpaddr , sizeof(struct sockaddr_in) ) )
    {
        printf("cannot bind tcp socket !\n");
        return NULL;
    }
    //  申请TCP RPC服务资源
    if( (SVCXPRT*)NULL == ( xprt = svctcp_create( tcpsock , RPCSMALLMSGSIZE , RPCSMALLMSGSIZE ) ) )
    {
        printf("tcp rpc service create fail !\n");
        return NULL;
    }
    //  向TCP RPC服务注册处理函数
    if( 0 == svc_register( xprt , PMAPPROG , PMAPVERS , lxi_vxi_resource_discovery_service , false ) )
    {
        printf("tcp rpc service process callback register fail !\n");
        return NULL;
    }

    /*
     * 创建基于TCP的VXI-11 Core 服务
    */
    //  申请socket资源
    if( -1 == ( coresock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ) )
    {
        printf("cannot create vxi-11 core socket !\n");
        return NULL;
    }
    //  设置socket的属性--允许当前地址重复使用
    setsockopt( coresock , SOL_SOCKET , SO_REUSEADDR , &sockoptcode , sizeof(sockoptcode));

    //  给coresock进行地址绑定
    if( 0 != bind( coresock , (struct sockaddr *)&coreaddr , sizeof(struct sockaddr_in) ) )
    {
        printf("cannot bind vxi-11 core socket !\n");
        return NULL;
    }
    //  申请VXI-11 Core RPC服务资源
    if( (SVCXPRT*)NULL == ( xprt = svctcp_create( coresock , LXI_VXI_CORE_SEND_BUFF_SIZE , LXI_VXI_CORE_RECV_BUFF_SIZE ) ) )
    {
        printf("vxi-11 core rpc service create fail !\n");
        return NULL;
    }
    //  向TCP RPC服务注册处理函数
    if( 0 == svc_register( xprt , LXI_VXI_CORE_PROGRAM , LXI_VXI_CORE_VERSION , lxi_vxi_service , false ) )
    {
        printf("vxi-11 core rpc service process callback register fail !\n");
        return NULL;
    }

    printf("svc service run!\n");
    /*
     * 运行RPC SVC 服务
    */
    svc_run();

    return NULL;
}
