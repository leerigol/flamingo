
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <net/if_arp.h>
#include <sys/ioctl.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <linux/route.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <net/if_packet.h>
#include <sys/utsname.h>
#include <fcntl.h>
#include <errno.h>
#include "lxi_net.h"
#include "lxi_arch.h"
#include "lxi_int.h"

s32                         m_s32MagicCookie;
s32                         m_s32DhcpSocket;
s16                         m_s16DhcpClientID;
s32                         m_s32UdpFooSocket;
u16                         m_u16DhcpMsgSize;
u32                         m_u32DhcpLeaseTime;
s8                          m_as8CurrDevName[LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN];
LXI_DHCP_INTERFACE_STRU     m_stDhcpInterface;
LXI_DHCP_OPTION_STRU        m_stDhcpOption;
LXI_DHCP_UDP_MESSAGE_STRU   m_stSendMsg,m_stRecvMsg;
LXI_DHCP_PACKET_STRU       *m_pstDhcpSendMsg    = (LXI_DHCP_PACKET_STRU *)&m_stSendMsg.udpipmsg[sizeof(LXI_DHCP_UDP_IP_HDR_STRU)];
LXI_DHCP_PACKET_STRU       *m_pstDhcpRecvMsg    = (LXI_DHCP_PACKET_STRU *)&m_stRecvMsg.udpipmsg[sizeof(LXI_DHCP_UDP_IP_HDR_STRU)];
const struct ip            *m_pstSendIp         = (struct ip*)((LXI_DHCP_UDP_IP_HDR_STRU*)m_stSendMsg.udpipmsg)->ip;
const struct ip            *m_pstRecvIp         = (struct ip*)((LXI_DHCP_UDP_IP_HDR_STRU*)m_stRecvMsg.udpipmsg)->ip;
time_t                      m_ReqSendTime;
LXI_NET_LINK_STATUS_EM      m_emCurrLinKState   = LXI_NET_LINK_STATUS_NOT_LINK;
LXI_IP_CONFIG_MODE_EM       m_emCurrIpCfgMode   = LXI_IP_CONFIG_MODE_NONE;
u8                          m_bLxiIpConfig      = false;
u8                          m_bDhcpServerNoFind = false;
u8                          m_bDhcpServerLost   = false;
u8                          m_bDhcpFirstConfig  = true;
u8                          m_bLxiDhcpRelease   = false;
u8                          m_bDhcpAgainDiscover= false;

u8                          m_b8DhcpServerReset = false;

LXI_PARAMETER_INFO_STRU     m_stNetConfPara;
lxi_sem_t                   m_semIpRelease , m_semIpConfFinish , m_semIpConfStart;
LXI_MANUAL_IP_CFG_STRU      m_stStaticIpInfo;
lxi_pthread_t               m_DhcpClientId , m_IpConfigId;

static s32 timeval_subtract( struct timeval *result ,
                             struct timeval *x,
                             struct timeval *y)
{
    /* Perform the carry for the later subtraction by updating Y. */
    if( x->tv_usec < y->tv_usec )
    {
        s32 nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }
    if( x->tv_usec - y->tv_usec > 1000000 )
    {
        s32 nsec = ( x->tv_usec - y->tv_usec ) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec  -= nsec;
    }
    /* Compute the time remaining to wait.`tv_usec' is certainly positive. */
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;
    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

LXI_NET_LINK_STATUS_EM lxi_net_link_status_get( s8  *ps8NetName )
{
    s32                         sockfd;
    struct  ifreq               ifr;
    LXI_NET_LINK_STATUS_EM      emLinkStatus = LXI_NET_LINK_STATUS_NOT_LINK;
    struct  ethtool_value       ethinfo;

    //  申请一个socket
    if( ( sockfd = socket( AF_INET , SOCK_DGRAM , IPPROTO_IP ) ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:Link Create fail!\n" );
        return emLinkStatus;
    }

    //  初始化后ifr的网卡名
    strcpy( ifr.ifr_ifrn.ifrn_name , ps8NetName );

    //  设置ethcmd的命令
    ethinfo.cmd  = ETHTOOL_GLINK;

    ifr.ifr_ifru.ifru_data = (s8*)&ethinfo;

    //  获取当前NET物理层连接状态
    if( ioctl( sockfd , SIOCETHTOOL , &ifr ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:Link ioctl fail!\n" );
        return emLinkStatus;
    }
    //  进行判定
    if( ethinfo.data == 1 )
    {
        emLinkStatus =  LXI_NET_LINK_STATUS_IS_LINK;
    }
    else
    {
        emLinkStatus = LXI_NET_LINK_STATUS_NOT_LINK;
    }

    close( sockfd );

    return emLinkStatus;
}

void lxi_net_speed_and_duplex_get( s8 *ps8NetName , LXI_NETWORK_SPEED_EM * pemSpeed , LXI_NETWORK_WORK_MODE_EM *pemMode )
{
    s32                         sockfd;
    struct  ifreq               ifr;
    struct  ethtool_cmd         ethinfo;

    //  申请一个socket
    if( ( sockfd = socket( AF_INET , SOCK_DGRAM , IPPROTO_IP ) ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:Speed Create fail!\n" );
        return;
    }

    //  初始化后ifr的网卡名
    strcpy( ifr.ifr_ifrn.ifrn_name , ps8NetName );

    //  设置ethcmd的命令
    ethinfo.cmd  = ETHTOOL_GSET;

    ifr.ifr_ifru.ifru_data = (s8*)&ethinfo;

    //  获取当前NET物理层连接状态
    if( ioctl( sockfd , SIOCETHTOOL , &ifr ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:Speed ioctl fail!\n" );
        close( sockfd );
        return;
    }
    //  进行判定
    if( ethinfo.duplex == DUPLEX_HALF )
    {
        *pemMode = LXI_NETWORK_HALF_WORK;
    }
    else
    {
        *pemMode = LXI_NETWORK_FULL_WORK;
    }
    //  带宽判定
    if( ethinfo.speed == SPEED_10 )
    {
        *pemSpeed = LXI_NETWORK_SPEED_10M ;
    }
    else if( ethinfo.speed == SPEED_100 )
    {
        *pemSpeed = LXI_NETWORK_SPEED_100M ;
    }
    else //if( ethinfo.speed == SPEED_1000 )
    {
        *pemSpeed = LXI_NETWORK_SPEED_1000M;
    }
    close( sockfd );
}

void lxi_dhcp_class_id_setup( void )
{
    //struct utsname   sname;
    u8              *pu8ClientId = m_stDhcpInterface.client_id;

//    if( uname(&sname) )
//    {
//        LXI_DEBUG( "lxi_net.c:Class Id:Setup fail!\n" );
//        return;
//    }
    //m_stDhcpInterface.class_len = sprintf( m_stDhcpInterface.class_id , "%s %s %s" , sname.sysname , sname.release , sname.machine);

    m_stDhcpInterface.class_len = sprintf( m_stDhcpInterface.class_id, "RIGOL-DSO");
    *pu8ClientId++ = LXI_DHCP_OPTION_CLIENTIDENTIFIER;
    *pu8ClientId++ = ETH_ALEN + 1;            /* length: 6 (MAC Addr) + 1 (# field) */
    *pu8ClientId++ = ARPHRD_ETHER;            /* type: Ethernet address */
    memcpy(pu8ClientId,m_stNetConfPara.as8MacAddr,ETH_ALEN);
    m_stDhcpInterface.client_len = ETH_ALEN + 3;
}

s32 lxi_dhcp_waitfd( s32 fd, time_t usec )
{
    fd_set          fs;
    struct timeval  t;
    s32             r;

    FD_ZERO(&fs);
    FD_SET(fd, &fs);
    t.tv_sec = usec/1000000;
    t.tv_usec = usec%1000000;

    if ( ( r = select( fd+1 , &fs , NULL , NULL , &t ) ) == -1 )
    {
        return -1;
    }
    if( FD_ISSET( fd , &fs ) )
    {
        return 0;
    }
    return 1;
}

LXI_DHCP_MSG_TYPE_EM lxi_dhcp_msg_parse( void )
{
    register u8     *p   = m_pstDhcpRecvMsg->options + 4;
    u8              *end = m_pstDhcpRecvMsg->options + sizeof( m_pstDhcpRecvMsg->options );

    //  对于两次续约时间,ACK中都会体现,后面将重新计算
    if( m_stDhcpOption.val[LXI_DHCP_OPTION_T1_VALUE] && m_stDhcpOption.len[LXI_DHCP_OPTION_T1_VALUE] > 0 )
    {
        memset( m_stDhcpOption.val[LXI_DHCP_OPTION_T1_VALUE] , 0 , m_stDhcpOption.len[LXI_DHCP_OPTION_T1_VALUE] );
        m_stDhcpOption.len[LXI_DHCP_OPTION_T1_VALUE] = 0;
    }
    if( m_stDhcpOption.val[LXI_DHCP_OPTION_T2_VALUE] && m_stDhcpOption.len[LXI_DHCP_OPTION_T2_VALUE] > 0 )
    {
        memset( m_stDhcpOption.val[LXI_DHCP_OPTION_T2_VALUE] , 0 , m_stDhcpOption.len[LXI_DHCP_OPTION_T2_VALUE] );
        m_stDhcpOption.len[LXI_DHCP_OPTION_T2_VALUE] = 0;
    }

    //  开始DHCP操作的内容获取
    while( p < end )
    {
        //  处理END操作和PAD操作
        switch( (LXI_DHCP_OPTION_EM)*p )
        {
            case LXI_DHCP_OPTION_END:
                goto swend;
            case LXI_DHCP_OPTION_PAD:
                p++;
                break;
            default:
                //  如果长度不为0,则继续解析
                if (p[1])
                {
                    //  判定报文是否读取到报文的尾部
                    if( p + 2 + p[1] >= end )
                    {
                        goto swend; /* corrupt packet */
                    }
                    //  赋值
                    memset( m_stDhcpOption.val[*p] , '\0' , 32 );
                    memcpy( m_stDhcpOption.val[*p] , p + 2 , p[1] );
                    m_stDhcpOption.len[*p] = p[1];
                }
            }
            p += (p[1]+2);
        }
swend:
    //  如果没有分配地址,则使用发送时使用的IP地址
    if ( !m_pstDhcpRecvMsg->yiaddr )
    {
        m_pstDhcpRecvMsg->yiaddr = m_pstDhcpSendMsg->ciaddr;
    }
    //  如果没有接收到DHCP服务器的IP地址,采用接收报文的IP地址
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_SERVER_IDENTIFIER] )
    {
        memcpy( m_stDhcpOption.val[LXI_DHCP_OPTION_SERVER_IDENTIFIER] , &m_pstRecvIp->ip_src.s_addr , 4 );
        m_stDhcpOption.len[LXI_DHCP_OPTION_SERVER_IDENTIFIER] = 4;
    }
    //  如果没有子网掩码,则根据当前接收端的IP地址所在网段进行自动分配
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_SUBNET_MASK] )
    {
        m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][0] = 255;
        //  A类地址判定
        if( IN_CLASSA( ntohl(m_pstDhcpRecvMsg->yiaddr) ) )
        {
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][1] = 0;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][2] = 0;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][3] = 0;
        }
        //  B类地址判定
        else if( IN_CLASSB( ntohl(m_pstDhcpRecvMsg->yiaddr) ) )
        {
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][1] = 255;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][2] = 0;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][3] = 0;
        }
        //  C类地址判定
        else if( IN_CLASSC( ntohl(m_pstDhcpRecvMsg->yiaddr) ) )
        {
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][1] = 255;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][2] = 255;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][3] = 0;
        }
        else
        {
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][1] = 255;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][2] = 255;
            m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK][3] = 255;
        }
        m_stDhcpOption.len[LXI_DHCP_OPTION_SUBNET_MASK] = 4;
    }
    //  如果没有得到广播地址的,则使用接收的报文IP地址与子网掩码的补码做或运算
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_BROADCAST_ADDR] )
    {
        s32 br = m_pstDhcpRecvMsg->yiaddr | ~*((s32 *)m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK]);
        *((s32*)m_stDhcpOption.val[LXI_DHCP_OPTION_BROADCAST_ADDR]) = br;
        m_stDhcpOption.len[LXI_DHCP_OPTION_BROADCAST_ADDR] = 4;
    }
    //  如果获取的租期为0或者没有获取到租期时间,则使用默认租期值
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME] || *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME]) == 0 )
    {
        *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME]) = m_u32DhcpLeaseTime;
        m_stDhcpOption.len[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME] = 4;
    }
    //  如果没有获取到新的续约时间,则根据租期重新计算第一次续约的时间
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_T1_VALUE] || *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_T1_VALUE]) == 0 )
    {
        u32 t = 0.5 * ntohl( *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME]) );
        *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_T1_VALUE]) = htonl(t);
        m_stDhcpOption.len[LXI_DHCP_OPTION_T1_VALUE] = 4;
    }
    //  如果没有获取到第2次续约的续约时间,则根据租期重新计算第2次续约的时间
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_T2_VALUE] || *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_T2_VALUE]) == 0 )
    {
        u32 t = 0.875 * ntohl( *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME]) );
        *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_T2_VALUE]) = htonl(t);
        m_stDhcpOption.len[LXI_DHCP_OPTION_T2_VALUE] = 4;
    }
    //  如果当前消息类型不为空,则返回当前消息类型,否则返回PAD
    if ( m_stDhcpOption.len[LXI_DHCP_OPTION_MESSAGE_TYPE] )
      return (LXI_DHCP_MSG_TYPE_EM)(*m_stDhcpOption.val[LXI_DHCP_OPTION_MESSAGE_TYPE]);
    return LXI_DHCP_MSG_TYPE_NULL;
}

s32 lxi_dhcp_send_and_recv( u32 u32Xid , LXI_DHCP_MSG_TYPE_EM emType , void (*pfunMsgProcess)(u32) )
{
    struct sockaddr                 addr;
    struct timeval                  begin, current, diff;
    const LXI_DHCP_UDP_HDR_STRU    *udpRecv;
    s32                             i,len,timeout = 0;
    socklen_t                       addrLength;
    s8                              foobuf[512];
    s32                             j = LXI_DHCP_INITIAL_RTO/2;
    s32                             s32SendNum = 0;

    do
    {
        //  发送
        do
        {
            if( ++s32SendNum > 6 )
            {
                return 1;
            }

            j += j;
            if( j > LXI_DHCP_MAX_RTO )
            {
                j = LXI_DHCP_MAX_RTO;
            }
            memset( &addr , 0 , sizeof(struct sockaddr) );
            memcpy( addr.sa_data , m_as8CurrDevName , strlen(m_as8CurrDevName) );
            //  组织报文
            (*pfunMsgProcess)(u32Xid);
            //  计算报文长度
            len = sizeof(LXI_DHCP_ETHER_HEADER_STRU) + sizeof(LXI_DHCP_UDP_IP_HDR_STRU) + sizeof(LXI_DHCP_PACKET_STRU);
            //  发送
            if( sendto( m_s32DhcpSocket , &m_stSendMsg , len , 0 , &addr , sizeof(struct sockaddr) ) == -1 )
            {
                LXI_DEBUG( "lxi_net.c:Socket:Sendto fail!\n" );
                return (-1);
            }
            //  计时
            gettimeofday(&begin, NULL);
            //  获取随机数
            i = random();
        }while ( lxi_dhcp_waitfd( m_s32DhcpSocket, j + i%200000 ) );
        //  接收
        do
        {
            struct ip   ipRecv_local;
            s8         *tmp_ip;

            //  接收
            memset( &m_stRecvMsg , 0 , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) );
            addrLength = sizeof( struct sockaddr );
            len = recvfrom( m_s32DhcpSocket , &m_stRecvMsg , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) , 0 , &addr , &addrLength );
            if( len == -1 )
            {
                LXI_DEBUG( "lxi_net.c:Socket:Recvfrom fail!\n" );
                return (-1);
            }
            //  计算耗时
            gettimeofday(&current, NULL);
            timeval_subtract(&diff, &current, &begin);
            timeout = j - diff.tv_sec*1000000 - diff.tv_usec + random()%200000;
            //  报文类型
            if ( m_stRecvMsg.ethhdr.ether_type != htons(ETHERTYPE_IP) )
            {
                continue;
            }
            //  查找到IP地址段
            tmp_ip = m_stRecvMsg.udpipmsg;
            for( i = 0 ; i < (s32)sizeof(struct ip) - 2 ; i += 2 )
            {
                if( ( m_stRecvMsg.udpipmsg[i] == 0x45) && ( m_stRecvMsg.udpipmsg[i+1] == 0x00 ) )
                {
                    tmp_ip = &m_stRecvMsg.udpipmsg[i];
                    break;
                }
            }
            //  获取IP地址信息
            memcpy( &ipRecv_local , ( (LXI_DHCP_UDP_IP_HDR_STRU *)tmp_ip)->ip , sizeof( struct ip ) );
            //  获取UDP报文头信息
            udpRecv = ( LXI_DHCP_UDP_HDR_STRU *)( (s8*)( ( ( LXI_DHCP_UDP_IP_HDR_STRU* )tmp_ip)->ip ) + sizeof( struct ip ) );
            //  判定IP的协议是否为UDP
            if( ipRecv_local.ip_p != IPPROTO_UDP )
            {
                continue;
            }
            //  校验IP报文长度
            len -= sizeof( LXI_DHCP_ETHER_HEADER_STRU );
            i = (s32)ntohs( ipRecv_local.ip_len );
            if( len < i )
            {
                continue;
            }
            //  校验UDP报文长度
            len = i - ( ipRecv_local.ip_hl<<2 );
            i = (s32)ntohs( udpRecv->uh_ulen );
            if( len < i )
            {
                continue;
            }
            //  获取DHCP的报文
            m_pstDhcpRecvMsg = ( LXI_DHCP_PACKET_STRU* )&tmp_ip[( ipRecv_local.ip_hl<<2 ) + sizeof(LXI_DHCP_UDP_HDR_STRU)];

            //  判断XID、HTYPE和OP字段
            if( ( m_pstDhcpRecvMsg->xid != u32Xid ) || ( m_pstDhcpRecvMsg->htype != ARPHRD_ETHER ) || ( m_pstDhcpRecvMsg->op != LXI_DHCP_OP_CODE_REPLY ) )
            {
                continue;
            }
            //  接收多余的UDP报文
            while ( m_s32UdpFooSocket > 0 && recvfrom( m_s32UdpFooSocket , (void *)foobuf , sizeof(foobuf) , 0 , NULL , NULL ) != -1 );
            //  解析DHCP报文
            if( lxi_dhcp_msg_parse() == emType )
            {
                return 0;
            }
            //  判定DHCP报文中的操作码值,如果报文类型为DHCP_NAK,且MSG的值为空,则返回1
            if( m_stDhcpOption.val[LXI_DHCP_OPTION_MESSAGE_TYPE] )
            {
                if( *(u8*)m_stDhcpOption.val[LXI_DHCP_OPTION_MESSAGE_TYPE] == LXI_DHCP_MSG_TYPE_NAK )
                {
//                    if( m_stDhcpOption.val[LXI_DHCP_OPTION_MESSAGE] )
//                    {
//                        printf( "DHCP_NAK server response received: %s\n" , (s8 *)m_stDhcpOption.val[LXI_DHCP_OPTION_MESSAGE] );
//                    }
                    return 1;
                }
            }
        } while ( timeout > 0 && lxi_dhcp_waitfd( m_s32DhcpSocket , timeout ) == 0 );
    }while ( 1 );
    return 1;
}

u16 lxi_dhcp_check_sum( u16 *addr , s32 len)
{
    register s32    sum = 0;
    register u16   *w = addr;
    register s32    nleft = len;
    u8              a;

    while (nleft > 1)
    {
        sum += *w++;
        nleft -= 2;
    }
    if (nleft == 1)
    {
        a = 0;
        memcpy(&a,w,1);
        sum += a;
    }
    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    return ~sum;
}

void lxi_dhcp_udp_ip_gen( LXI_DHCP_UDP_IP_HDR_STRU *pstUdpIp ,
                          u32 u32SrcAddr ,
                          u32 u32DstAddr ,
                          u16 u16SrcPort,
                          u16 u16DstPort,
                          u16 u16MsgLen)
{
    struct ip               ip_local;
    struct ip              *ip=&ip_local;
    LXI_DHCP_IPOVLY_STRU   *io=(LXI_DHCP_IPOVLY_STRU *)pstUdpIp->ip;
    LXI_DHCP_UDP_HDR_STRU  *udp=(LXI_DHCP_UDP_HDR_STRU *)pstUdpIp->udp;

    io->ih_next = io->ih_prev = 0;
    io->ih_x1 = 0;
    io->ih_pr = IPPROTO_UDP;
    io->ih_len = htons(u16MsgLen + sizeof(LXI_DHCP_UDP_HDR_STRU));
    io->ih_src.s_addr = u32SrcAddr;
    io->ih_dst.s_addr = u32DstAddr;
    udp->uh_sport = u16SrcPort;
    udp->uh_dport = u16DstPort;
    udp->uh_ulen = io->ih_len;
    udp->uh_sum = 0;
    udp->uh_sum = lxi_dhcp_check_sum((u16 *)pstUdpIp,u16MsgLen+sizeof(LXI_DHCP_UDP_IP_HDR_STRU));
    if (udp->uh_sum == 0)
    {
        udp->uh_sum = 0xffff;
    }
    memcpy(ip,(struct ip *)pstUdpIp->ip,sizeof(ip_local));
    ip->ip_hl = 5;
    ip->ip_v = IPVERSION;
    ip->ip_tos = 0;	/* normal service */
    ip->ip_len = htons(u16MsgLen + sizeof(LXI_DHCP_UDP_IP_HDR_STRU));
    ip->ip_id = htons(m_s16DhcpClientID);
    m_s16DhcpClientID++;
    ip->ip_off = 0;
    ip->ip_ttl = IPDEFTTL; /* time to live, 64 by default */
    ip->ip_p = IPPROTO_UDP;
    ip->ip_src.s_addr = u32SrcAddr;
    ip->ip_dst.s_addr = u32DstAddr;
    ip->ip_sum = 0;
    memcpy(pstUdpIp->ip,ip,sizeof(ip_local));
    ip->ip_sum = lxi_dhcp_check_sum((u16 *)&ip_local,sizeof(struct ip));
    memcpy(pstUdpIp->ip,ip,sizeof(ip_local));
}

void lxi_net_arp_msg_process( LXI_ARP_PACKET_STRU *msg ,
                              u16 opcode ,
                              u32 tipaddr ,
                              s8 *thaddr ,
                              u32 sipaddr ,
                              s8 *shaddr )
{
    memset( msg , 0 , sizeof(LXI_ARP_PACKET_STRU) );
    memcpy( msg->ethhdr.h_dest , LXI_DHCP_MAC_BCAST_ADDR , 6 );
    memcpy( msg->ethhdr.h_source , shaddr , ETH_ALEN );
    msg->ethhdr.h_proto = htons( ETHERTYPE_ARP );
    msg->htype          = htons(ARPHRD_ETHER);
    msg->ptype          = htons(ETHERTYPE_IP);
    msg->hlen           = ETH_ALEN;
    msg->plen           = 4;
    msg->operation      = htons(opcode);
    memcpy( msg->sHaddr , shaddr , ETH_ALEN );
    msg->sInaddr[3]     = sipaddr >> 24;
    msg->sInaddr[2]     = ( sipaddr & 0x00ff0000) >> 16;
    msg->sInaddr[1]     = ( sipaddr & 0x0000ff00) >> 8;
    msg->sInaddr[0]     = ( sipaddr & 0x000000ff) ;

    msg->tInaddr[3]     = tipaddr >> 24;
    msg->tInaddr[2]     = ( tipaddr & 0x00ff0000) >> 16;
    msg->tInaddr[1]     = ( tipaddr & 0x0000ff00) >> 8;
    msg->tInaddr[0]     = ( tipaddr & 0x000000ff) ;
    if ( opcode == ARPOP_REPLY )
    {
        memcpy( msg->tHaddr , thaddr , ETH_ALEN );
    }
}

s32  lxi_dhcp_send_release_arp( void )
{
    LXI_ARP_PACKET_STRU ReleaseArp;
    struct sockaddr     addr;
    s32                 len;
    s8                  tHaddr[6] = {'\xff','\xff','\xff','\xff','\xff','\xff'};

    memset( &ReleaseArp , 0 , sizeof(LXI_ARP_PACKET_STRU) );
    lxi_net_arp_msg_process( &ReleaseArp      ,
                             ARPOP_REPLY      ,
                             INADDR_BROADCAST ,
                             tHaddr           ,
                             m_stDhcpInterface.ciaddr ,
                             m_stNetConfPara.as8MacAddr );
    memset( &addr , 0 , sizeof(struct sockaddr) );
    memcpy( addr.sa_data , m_as8CurrDevName , strlen( m_as8CurrDevName ) );
    len = sizeof( LXI_ARP_PACKET_STRU );
    if( sendto( m_s32DhcpSocket , &ReleaseArp , len , 0 , &addr , sizeof(struct sockaddr) ) == -1)
    {
        LXI_DEBUG( "lxi_net.c:ARP:Sendto fail!\n" );
        return -1;
    }
    return 0;
}

s32  lxi_ip_addr_check( u32 taddr )
{
    struct sockaddr     addr;
    LXI_ARP_PACKET_STRU ArpMsgSend,ArpMsgRecv;
    s32                 i  , len;
    socklen_t           j;
    u32                 myaddr;
    s8                  tHaddr[ETH_ALEN];

    //  组织ARP广播包
    memset( tHaddr , 0 , ETH_ALEN );
    len = sizeof( LXI_ARP_PACKET_STRU );
    myaddr = 0;
    memset( &ArpMsgSend , 0 , sizeof(LXI_ARP_PACKET_STRU) );
    lxi_net_arp_msg_process( &ArpMsgSend , ARPOP_REQUEST , taddr , tHaddr , myaddr , m_stNetConfPara.as8MacAddr );
    //  设置发送目的地址
    memset( &addr , 0 , sizeof(struct sockaddr) );
    memcpy( addr.sa_data , m_as8CurrDevName , strlen( m_as8CurrDevName ) );
    //  发送ARP报文,并接收响应报文
    i = 0;
    while (1)
    {
        do
        {
            //  如果4次广播,都没有收到响应报文,则返回0,表示该地址本机可以使用
            if ( i++ > 4)
            {
                return 0;
            }
            //  发送报文
            if( sendto( m_s32DhcpSocket , &ArpMsgSend , len , 0 , &addr , sizeof(struct sockaddr) ) == -1 )
            {
                LXI_DEBUG( "lxi_net.c:IPCheck:Sendto fail!\n" );
                return -1;
            }

        }while( lxi_dhcp_waitfd( m_s32DhcpSocket , 50000 ) );
        //  有响应报文,接收
        do
        {
            memset( &ArpMsgRecv , 0 , sizeof(LXI_ARP_PACKET_STRU) );
            j = sizeof(struct sockaddr);
            //  接收
            if( recvfrom( m_s32DhcpSocket , &ArpMsgRecv , sizeof(LXI_ARP_PACKET_STRU) , 0 , (struct sockaddr *)&addr, &j ) == -1)
            {
                return -1;
            }
            //  判断报文协议类
            if( ArpMsgRecv.ethhdr.h_proto != htons(ETHERTYPE_ARP) )
            {
                continue;
            }
            //  判断当前ARP报文是否为响应报文
            if( ArpMsgRecv.operation == htons(ARPOP_REPLY) )
            {
            }
            else
            {
                continue;
            }
            //  判断报文的MAC地址是不是本机的
            if( memcmp( ArpMsgRecv.tHaddr , m_stNetConfPara.as8MacAddr , ETH_ALEN ) )
            {
                continue;
            }
            //  判断报文中的IP地址是否为本机广播的地址
            if( memcmp( &ArpMsgRecv.sInaddr , &taddr , 4 ) )
            {
                continue;
            }

            return 1;
        }while( lxi_dhcp_waitfd( m_s32DhcpSocket , 50000 ) == 0 );
    }
    return 0;
}

s32  lxi_autoip_config( void )
{
    struct in_addr      ip;
    s32                 status;
    u32                 r = rand();
    u32                 host;

    srand( (u32)time(NULL) );

    //  生成IP地址
    ip = inet_makeaddr( (IN_CLASSB_NET & LXI_AUTOIP_START_ADDR) , (IN_CLASSB_HOST & r) );
    //  广播该地址
    while( ( status = lxi_ip_addr_check( ip.s_addr ) ) )
    {
        //  该IP已经被使用,重新生成
        srand((u32)time(NULL) + 100);
        r = rand();
        ip = inet_makeaddr( (IN_CLASSB_NET & LXI_AUTOIP_START_ADDR) , (IN_CLASSB_HOST & r) );
        host = inet_lnaof(ip) & IN_CLASSB_HOST;
        if( host >> 8 == 0 )
        {
            host = 0x100 | ( 0xff & host );
        }
        else
        {
            if( host >> 8 == 255 )
            {
                host = 0xfe00 | ( 0xff & host );
            }
        }
        ip = inet_makeaddr( (IN_CLASSB_NET & LXI_AUTOIP_START_ADDR) , host );
    }
    m_stDhcpInterface.ciaddr = ip.s_addr;
    return 0;
}

s32  lxi_ip_configuration( void )
{
    struct sockaddr_in* addr;
    struct ifreq        ifr;
    FILE               *fd;
    struct in_addr      dns;

    addr = (struct sockaddr_in*)&ifr.ifr_addr;
    bzero( addr , sizeof( struct sockaddr_in ) );
    memset( &ifr , 0 , sizeof(struct ifreq) );
    memcpy( ifr.ifr_name , m_as8CurrDevName , strlen(m_as8CurrDevName) );
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = m_stNetConfPara.s32IpAddr;

    //  设置IP地址
    if( ioctl( m_s32DhcpSocket , SIOCSIFADDR , &ifr ) == -1 )
    {
        LXI_DEBUG( "lxi_net.c:IPConfig:IP Addr Set Fail!\n" );
        return -1;
    }
    //  设置广播地址
    addr = (struct sockaddr_in*)&ifr.ifr_broadaddr;
    bzero( addr , sizeof( struct sockaddr_in ) );
    addr->sin_family      = AF_INET;
    addr->sin_addr.s_addr = ( m_stNetConfPara.s32IpAddr & m_stNetConfPara.s32MaskAddr ) | ~m_stNetConfPara.s32MaskAddr;
    if( ioctl( m_s32DhcpSocket , SIOCSIFBRDADDR , &ifr ) == -1 )
    {
        LXI_DEBUG( "lxi_net.c:IPConfig:Broad Addr Set Fail!\n" );
        return -1;
    }
    //  设置子网掩码
    addr = (struct sockaddr_in*)&ifr.ifr_netmask;
    bzero( addr , sizeof( struct sockaddr_in ) );
    addr->sin_family      = AF_INET;
    addr->sin_addr.s_addr = m_stNetConfPara.s32MaskAddr;
    if( ioctl( m_s32DhcpSocket , SIOCSIFNETMASK , &ifr ) == -1 )
    {
        LXI_DEBUG( "lxi_net.c:IPConfig:Sub Mask Addr Set Fail!\n" );
        return -1;
    }
    //  设置网关
    lxi_net_route_add( m_stNetConfPara.s32GateAddr , 0 );
    //  设置DNS
    fd = fopen( "/etc/resolv.conf" , "w" );
    //  写首选DNS地址
    dns.s_addr = m_stNetConfPara.s32DnsAddr[0];
    fprintf( fd , "%s%s\n" , "nameserver "  , inet_ntoa( dns ) );
    //  写备选DNS地址
    dns.s_addr = m_stNetConfPara.s32DnsAddr[1];
    fprintf( fd , "%s%s\n" , "nameserver "  , inet_ntoa( dns ) );

    fclose( fd );
    return 0;
}

int  lxi_dhcp_add_hostname(u8 *ptr)
{
    int hostNameLen = strlen(m_stNetConfPara.as8HostName);
    *ptr++ = LXI_DHCP_OPTION_HOST_NAME;
    *ptr++ = hostNameLen;
    memcpy(ptr,m_stNetConfPara.as8HostName, hostNameLen );

    return hostNameLen+1+1;
}

void lxi_dhcp_reboot_msg_process( u32 u32Xid )
{
    register u8 *p = m_pstDhcpSendMsg->options + 4;

    /*构造数据链路层的报文头*/
    memset( &m_stSendMsg , '\0' , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) );
    memcpy( m_stSendMsg.ethhdr.ether_dhost , LXI_DHCP_MAC_BCAST_ADDR , ETH_ALEN );
    memcpy( m_stSendMsg.ethhdr.ether_shost , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    m_stSendMsg.ethhdr.ether_type = htons(ETHERTYPE_IP);
    m_pstDhcpSendMsg->op          = LXI_DHCP_OP_CODE_REQUEST;
    m_pstDhcpSendMsg->htype       = ARPHRD_ETHER;
    m_pstDhcpSendMsg->hlen        = ETH_ALEN;
    m_pstDhcpSendMsg->xid         = u32Xid;
    m_pstDhcpSendMsg->secs        = htons(10);
    memcpy( m_pstDhcpSendMsg->chaddr , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    memcpy( m_pstDhcpSendMsg->options , &m_s32MagicCookie , 4 );
    //  组织options
    *p++ = LXI_DHCP_OPTION_MESSAGE_TYPE;
    *p++ = 1;
    *p++ = LXI_DHCP_MSG_TYPE_REQUEST;

    //
    p += lxi_dhcp_add_hostname(p);

    //
    *p++ = LXI_DHCP_OPTION_MESSAGE_MAX_SIZE;
    *p++ = 2;
    memcpy(p,&m_u16DhcpMsgSize,2);
    p += 2;
    //
    *p++ = LXI_DHCP_OPTION_REQUESTED_IP_ADDR;
    *p++ = 4;
    memcpy(p,&m_stDhcpInterface.ciaddr,4);
    p += 4;

    //
    *p++ = LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME;
    *p++ = 4;
    memcpy(p,&m_u32DhcpLeaseTime,4);
    p += 4;
    //
    *p++ = LXI_DHCP_OPTION_PARAM_REQUEST;
    *p++ = 14;
    *p++ = LXI_DHCP_OPTION_SUBNET_MASK;
    *p++ = LXI_DHCP_OPTION_ROUTER;
    *p++ = LXI_DHCP_OPTION_DNS;
    //*p++ = LXI_DHCP_OPTION_HOST_NAME;
    *p++ = LXI_DHCP_OPTION_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_ROOT_PATH;
    *p++ = LXI_DHCP_OPTION_DEFAULT_IP_TTL;
    *p++ = LXI_DHCP_OPTION_BROADCAST_ADDR;
    *p++ = LXI_DHCP_OPTION_PERFORM_MASK_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_PERFORM_ROUTER_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_STATIC_ROUTE;
    *p++ = LXI_DHCP_OPTION_NIS_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_NIS_SERVERS;
    *p++ = LXI_DHCP_OPTION_NTP_SERVERS;
    *p++ = LXI_DHCP_OPTION_DNS_SEARCH_PATH;
    //
    *p++ = LXI_DHCP_OPTION_CLASS_IDENTIFIER;
    *p++ = m_stDhcpInterface.class_len;
    memcpy(p,m_stDhcpInterface.class_id,m_stDhcpInterface.class_len);
    p += m_stDhcpInterface.class_len;
    //
    memcpy(p,m_stDhcpInterface.client_id,m_stDhcpInterface.client_len);
    p += m_stDhcpInterface.client_len;
    //
    *p = LXI_DHCP_OPTION_END;
    //
    lxi_dhcp_udp_ip_gen( (LXI_DHCP_UDP_IP_HDR_STRU*)m_stSendMsg.udpipmsg,0,INADDR_BROADCAST,
                         htons(LXI_DHCP_CLIENT_PORT),htons(LXI_DHCP_SERVER_PORT),sizeof(LXI_DHCP_PACKET_STRU));
}

void lxi_dhcp_discover_msg_process( u32 u32Xid )
{
    register u8 *p = m_pstDhcpSendMsg->options + 4;

    //填充报文的数据链路层头
    memset( &m_stSendMsg , '\0' , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) );
    memcpy( m_stSendMsg.ethhdr.ether_dhost , LXI_DHCP_MAC_BCAST_ADDR , ETH_ALEN );
    memcpy( m_stSendMsg.ethhdr.ether_shost , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    m_stSendMsg.ethhdr.ether_type = htons(ETHERTYPE_IP);
    m_pstDhcpSendMsg->op          = LXI_DHCP_OP_CODE_REQUEST;
    m_pstDhcpSendMsg->htype       = ARPHRD_ETHER;
    m_pstDhcpSendMsg->hlen        = ETH_ALEN;
    m_pstDhcpSendMsg->xid         = u32Xid;
    m_pstDhcpSendMsg->secs        = htons(10);
    memcpy( m_pstDhcpSendMsg->chaddr , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    memcpy( m_pstDhcpSendMsg->options , &m_s32MagicCookie , 4 );
    //  组织DHCP报文中options
    *p++ = LXI_DHCP_OPTION_MESSAGE_TYPE;
    *p++ = 1;
    *p++ = LXI_DHCP_MSG_TYPE_DISCOVER;

    p += lxi_dhcp_add_hostname(p);

    //
    *p++ = LXI_DHCP_OPTION_MESSAGE_MAX_SIZE;
    *p++ = 2;
    memcpy(p,&m_u16DhcpMsgSize,2);
    p += 2;
    //
    if (m_stDhcpInterface.ciaddr)
    {
        *p++ = LXI_DHCP_OPTION_REQUESTED_IP_ADDR;
        *p++ = 4;
        memcpy(p,&m_stDhcpInterface.ciaddr,4);
        p += 4;
    }

    //
    *p++ = LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME;
    *p++ = 4;
    memcpy(p,&m_u32DhcpLeaseTime,4);
    p += 4;
    //
    *p++ = LXI_DHCP_OPTION_PARAM_REQUEST;
    *p++ = 14;
    *p++ = LXI_DHCP_OPTION_SUBNET_MASK;
    *p++ = LXI_DHCP_OPTION_ROUTER;
    *p++ = LXI_DHCP_OPTION_DNS;
    //*p++ = LXI_DHCP_OPTION_HOST_NAME;
    *p++ = LXI_DHCP_OPTION_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_ROOT_PATH;
    *p++ = LXI_DHCP_OPTION_DEFAULT_IP_TTL;
    *p++ = LXI_DHCP_OPTION_BROADCAST_ADDR;
    *p++ = LXI_DHCP_OPTION_PERFORM_MASK_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_PERFORM_ROUTER_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_STATIC_ROUTE;
    *p++ = LXI_DHCP_OPTION_NIS_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_NIS_SERVERS;
    *p++ = LXI_DHCP_OPTION_NTP_SERVERS;
    *p++ = LXI_DHCP_OPTION_DNS_SEARCH_PATH;
    //
    *p++ = LXI_DHCP_OPTION_CLASS_IDENTIFIER;
    *p++ = m_stDhcpInterface.class_len;
    memcpy(p,m_stDhcpInterface.class_id,m_stDhcpInterface.class_len);
    p += m_stDhcpInterface.class_len;
    //
    memcpy(p,m_stDhcpInterface.client_id,m_stDhcpInterface.client_len);
    p += m_stDhcpInterface.client_len;
    //
    *p = LXI_DHCP_OPTION_END;
    //
    lxi_dhcp_udp_ip_gen( (LXI_DHCP_UDP_IP_HDR_STRU*)m_stSendMsg.udpipmsg,0,INADDR_BROADCAST,
                         htons(LXI_DHCP_CLIENT_PORT),htons(LXI_DHCP_SERVER_PORT),sizeof(LXI_DHCP_PACKET_STRU));
}

void lxi_dhcp_request_msg_process( u32 u32Xid )
{
    register u8 *p = m_pstDhcpSendMsg->options + 4;

    //填充报文的数据链路层头
    memset( &m_stSendMsg , '\0' , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) );
    memcpy( m_stSendMsg.ethhdr.ether_dhost , LXI_DHCP_MAC_BCAST_ADDR , ETH_ALEN );
    memcpy( m_stSendMsg.ethhdr.ether_shost , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    m_stSendMsg.ethhdr.ether_type = htons(ETHERTYPE_IP);
    m_pstDhcpSendMsg->op          = LXI_DHCP_OP_CODE_REQUEST;
    m_pstDhcpSendMsg->htype       = ARPHRD_ETHER;
    m_pstDhcpSendMsg->hlen        = ETH_ALEN;
    m_pstDhcpSendMsg->xid         = u32Xid;
    m_pstDhcpSendMsg->secs        = htons(10);
    memcpy( m_pstDhcpSendMsg->chaddr , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    memcpy( m_pstDhcpSendMsg->options , &m_s32MagicCookie , 4 );
    //  组织DHCP报文中options
    *p++ = LXI_DHCP_OPTION_MESSAGE_TYPE;
    *p++ = 1;
    *p++ = LXI_DHCP_MSG_TYPE_REQUEST;


    //hostname
    p += lxi_dhcp_add_hostname(p);

    //
    *p++ = LXI_DHCP_OPTION_MESSAGE_MAX_SIZE;
    *p++ = 2;
    memcpy(p,&m_u16DhcpMsgSize,2);
    p += 2;
    //
    *p++ = LXI_DHCP_OPTION_SERVER_IDENTIFIER;
    *p++ = 4;
    memcpy(p,m_stDhcpOption.val[LXI_DHCP_OPTION_SERVER_IDENTIFIER],4);
    p += 4;
    //
    *p++ = LXI_DHCP_OPTION_REQUESTED_IP_ADDR;
    *p++ = 4;
    memcpy(p,&m_stDhcpInterface.ciaddr,4);
    p += 4;

    //
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME] )
    {
        *p++ = LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME;
        *p++ = 4;
        memcpy(p,m_stDhcpOption.val[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME],4);
        p += 4;
    }
    //
    *p++ = LXI_DHCP_OPTION_PARAM_REQUEST;
    *p++ = 14;
    *p++ = LXI_DHCP_OPTION_SUBNET_MASK;
    *p++ = LXI_DHCP_OPTION_ROUTER;
    *p++ = LXI_DHCP_OPTION_DNS;
    //*p++ = LXI_DHCP_OPTION_HOST_NAME;
    *p++ = LXI_DHCP_OPTION_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_ROOT_PATH;
    *p++ = LXI_DHCP_OPTION_DEFAULT_IP_TTL;
    *p++ = LXI_DHCP_OPTION_BROADCAST_ADDR;
    *p++ = LXI_DHCP_OPTION_PERFORM_MASK_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_PERFORM_ROUTER_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_STATIC_ROUTE;
    *p++ = LXI_DHCP_OPTION_NIS_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_NIS_SERVERS;
    *p++ = LXI_DHCP_OPTION_NTP_SERVERS;
    *p++ = LXI_DHCP_OPTION_DNS_SEARCH_PATH;
    //
    *p++ = LXI_DHCP_OPTION_CLASS_IDENTIFIER;
    *p++ = m_stDhcpInterface.class_len;
    memcpy(p,m_stDhcpInterface.class_id,m_stDhcpInterface.class_len);
    p += m_stDhcpInterface.class_len;
    //
    memcpy(p,m_stDhcpInterface.client_id,m_stDhcpInterface.client_len);
    p += m_stDhcpInterface.client_len;
    //
    *p = LXI_DHCP_OPTION_END;
    //
    lxi_dhcp_udp_ip_gen( (LXI_DHCP_UDP_IP_HDR_STRU*)m_stSendMsg.udpipmsg,0,INADDR_BROADCAST,
                         htons(LXI_DHCP_CLIENT_PORT),htons(LXI_DHCP_SERVER_PORT),sizeof(LXI_DHCP_PACKET_STRU));
}

void lxi_dhcp_renew_msg_process( u32 u32Xid )
{
    register u8 *p = m_pstDhcpSendMsg->options + 4;

    //填充报文的数据链路层头
    memset( &m_stSendMsg , '\0' , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) );
    memcpy( m_stSendMsg.ethhdr.ether_dhost , m_stDhcpInterface.shaddr , ETH_ALEN );
    memcpy( m_stSendMsg.ethhdr.ether_shost , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    m_stSendMsg.ethhdr.ether_type = htons(ETHERTYPE_IP);
    m_pstDhcpSendMsg->op          = LXI_DHCP_OP_CODE_REQUEST;
    m_pstDhcpSendMsg->htype       = ARPHRD_ETHER;
    m_pstDhcpSendMsg->hlen        = ETH_ALEN;
    m_pstDhcpSendMsg->xid         = u32Xid;
    m_pstDhcpSendMsg->secs        = htons(10);
    m_pstDhcpSendMsg->ciaddr      = m_stDhcpInterface.ciaddr;
    memcpy( m_pstDhcpSendMsg->chaddr , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    memcpy( m_pstDhcpSendMsg->options , &m_s32MagicCookie , 4 );
    //  组织DHCP报文中options
    *p++ = LXI_DHCP_OPTION_MESSAGE_TYPE;
    *p++ = 1;
    *p++ = LXI_DHCP_MSG_TYPE_REQUEST;

    //hostname
    p += lxi_dhcp_add_hostname(p);

    //
    *p++ = LXI_DHCP_OPTION_MESSAGE_MAX_SIZE;
    *p++ = 2;
    memcpy(p,&m_u16DhcpMsgSize,2);
    p += 2;    

    //
    *p++ = LXI_DHCP_OPTION_PARAM_REQUEST;
    *p++ = 14;
    *p++ = LXI_DHCP_OPTION_SUBNET_MASK;
    *p++ = LXI_DHCP_OPTION_ROUTER;
    *p++ = LXI_DHCP_OPTION_DNS;
    //*p++ = LXI_DHCP_OPTION_HOST_NAME;

    *p++ = LXI_DHCP_OPTION_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_ROOT_PATH;
    *p++ = LXI_DHCP_OPTION_DEFAULT_IP_TTL;
    *p++ = LXI_DHCP_OPTION_BROADCAST_ADDR;
    *p++ = LXI_DHCP_OPTION_PERFORM_MASK_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_PERFORM_ROUTER_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_STATIC_ROUTE;
    *p++ = LXI_DHCP_OPTION_NIS_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_NIS_SERVERS;
    *p++ = LXI_DHCP_OPTION_NTP_SERVERS;
    *p++ = LXI_DHCP_OPTION_DNS_SEARCH_PATH;
    //
    *p++ = LXI_DHCP_OPTION_CLASS_IDENTIFIER;
    *p++ = m_stDhcpInterface.class_len;
    memcpy(p,m_stDhcpInterface.class_id,m_stDhcpInterface.class_len);
    p += m_stDhcpInterface.class_len;
    //
    memcpy(p,m_stDhcpInterface.client_id,m_stDhcpInterface.client_len);
    p += m_stDhcpInterface.client_len;
    //
    *p = LXI_DHCP_OPTION_END;
    //
    lxi_dhcp_udp_ip_gen( (LXI_DHCP_UDP_IP_HDR_STRU*)m_stSendMsg.udpipmsg , m_stDhcpInterface.ciaddr , m_stDhcpInterface.siaddr ,
                         htons(LXI_DHCP_CLIENT_PORT),htons(LXI_DHCP_SERVER_PORT),sizeof(LXI_DHCP_PACKET_STRU));
}

void lxi_dhcp_rebind_msg_process( u32 u32Xid )
{
    register u8 *p = m_pstDhcpSendMsg->options + 4;

    //填充报文的数据链路层头
    memset( &m_stSendMsg , '\0' , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) );
    memcpy( m_stSendMsg.ethhdr.ether_dhost , LXI_DHCP_MAC_BCAST_ADDR , ETH_ALEN );
    memcpy( m_stSendMsg.ethhdr.ether_shost , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    m_stSendMsg.ethhdr.ether_type = htons(ETHERTYPE_IP);
    m_pstDhcpSendMsg->op          = LXI_DHCP_OP_CODE_REQUEST;
    m_pstDhcpSendMsg->htype       = ARPHRD_ETHER;
    m_pstDhcpSendMsg->hlen        = ETH_ALEN;
    m_pstDhcpSendMsg->xid         = u32Xid;
    m_pstDhcpSendMsg->secs        = htons(10);
    m_pstDhcpSendMsg->ciaddr      = m_stDhcpInterface.ciaddr;
    memcpy( m_pstDhcpSendMsg->chaddr , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    memcpy( m_pstDhcpSendMsg->options , &m_s32MagicCookie , 4 );
    //  组织DHCP报文中options
    *p++ = LXI_DHCP_OPTION_MESSAGE_TYPE;
    *p++ = 1;
    *p++ = LXI_DHCP_MSG_TYPE_REQUEST;

    p += lxi_dhcp_add_hostname(p);

    //
    *p++ = LXI_DHCP_OPTION_MESSAGE_MAX_SIZE;
    *p++ = 2;
    memcpy(p,&m_u16DhcpMsgSize,2);
    p += 2;

    //
    if( !m_stDhcpOption.len[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME] )
    {
        *p++ = LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME;
        *p++ = 4;
        memcpy(p,m_stDhcpOption.val[LXI_DHCP_OPTION_IP_ADDR_LEASE_TIME],4);
        p += 4;
    }
    //
    *p++ = LXI_DHCP_OPTION_PARAM_REQUEST;
    *p++ = 14;
    *p++ = LXI_DHCP_OPTION_SUBNET_MASK;
    *p++ = LXI_DHCP_OPTION_ROUTER;
    *p++ = LXI_DHCP_OPTION_DNS;
    //*p++ = LXI_DHCP_OPTION_HOST_NAME;

    *p++ = LXI_DHCP_OPTION_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_ROOT_PATH;
    *p++ = LXI_DHCP_OPTION_DEFAULT_IP_TTL;
    *p++ = LXI_DHCP_OPTION_BROADCAST_ADDR;
    *p++ = LXI_DHCP_OPTION_PERFORM_MASK_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_PERFORM_ROUTER_DISCOVERY;
    *p++ = LXI_DHCP_OPTION_STATIC_ROUTE;
    *p++ = LXI_DHCP_OPTION_NIS_DOMAIN_NAME;
    *p++ = LXI_DHCP_OPTION_NIS_SERVERS;
    *p++ = LXI_DHCP_OPTION_NTP_SERVERS;
    *p++ = LXI_DHCP_OPTION_DNS_SEARCH_PATH;
    //
    *p++ = LXI_DHCP_OPTION_CLASS_IDENTIFIER;
    *p++ = m_stDhcpInterface.class_len;
    memcpy(p,m_stDhcpInterface.class_id,m_stDhcpInterface.class_len);
    p += m_stDhcpInterface.class_len;
    //
    memcpy(p,m_stDhcpInterface.client_id,m_stDhcpInterface.client_len);
    p += m_stDhcpInterface.client_len;
    //
    *p = LXI_DHCP_OPTION_END;
    //
    lxi_dhcp_udp_ip_gen( (LXI_DHCP_UDP_IP_HDR_STRU*)m_stSendMsg.udpipmsg,m_stDhcpInterface.ciaddr,INADDR_BROADCAST,
                         htons(LXI_DHCP_CLIENT_PORT),htons(LXI_DHCP_SERVER_PORT),sizeof(LXI_DHCP_PACKET_STRU));
}

void lxi_dhcp_release_msg_process( u32 u32Xid )
{
    register u8 *p = m_pstDhcpSendMsg->options + 4;

    //填充报文的数据链路层头
    memset( &m_stSendMsg , '\0' , sizeof(LXI_DHCP_UDP_MESSAGE_STRU) );
    memcpy( m_stSendMsg.ethhdr.ether_dhost , m_stDhcpInterface.shaddr , ETH_ALEN );
    memcpy( m_stSendMsg.ethhdr.ether_shost , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    m_stSendMsg.ethhdr.ether_type = htons(ETHERTYPE_IP);
    m_pstDhcpSendMsg->op          = LXI_DHCP_OP_CODE_REQUEST;
    m_pstDhcpSendMsg->htype       = ARPHRD_ETHER;
    m_pstDhcpSendMsg->hlen        = ETH_ALEN;
    m_pstDhcpSendMsg->xid         = u32Xid;
    m_pstDhcpSendMsg->secs        = htons(10);
    m_pstDhcpSendMsg->ciaddr      = m_stDhcpInterface.ciaddr;
    memcpy( m_pstDhcpSendMsg->chaddr , m_stNetConfPara.as8MacAddr , ETH_ALEN );
    memcpy( m_pstDhcpSendMsg->options , &m_s32MagicCookie , 4 );
    //  组织DHCP报文中options
    *p++ = LXI_DHCP_OPTION_MESSAGE_TYPE;
    *p++ = 1;
    *p++ = LXI_DHCP_MSG_TYPE_RELEASE;
    //
    *p++ = LXI_DHCP_OPTION_SERVER_IDENTIFIER;
    *p++ = 4;
    memcpy( p , m_stDhcpOption.val[LXI_DHCP_OPTION_SERVER_IDENTIFIER] , 4 );
    p += 4;
    //
    memcpy(p,m_stDhcpInterface.client_id,m_stDhcpInterface.client_len);
    p += m_stDhcpInterface.client_len;
    //
    *p = LXI_DHCP_OPTION_END;
    //
    lxi_dhcp_udp_ip_gen( (LXI_DHCP_UDP_IP_HDR_STRU*)m_stSendMsg.udpipmsg , m_stDhcpInterface.ciaddr , m_stDhcpInterface.siaddr ,
                         htons(LXI_DHCP_CLIENT_PORT),htons(LXI_DHCP_SERVER_PORT),sizeof(LXI_DHCP_PACKET_STRU));
}

s32  lxi_dhcp_request( void )
{
    //  进行数据报文的组织和接收响应值
    if( lxi_dhcp_send_and_recv( random() ,
                                LXI_DHCP_MSG_TYPE_ACK ,
                                lxi_dhcp_request_msg_process ) )
    {
        //  没有收到ACK报文,则进入初始化
        return -1;
    }
    //  收到DHCP服务端发送的ACK响应报文
    //  重新获取时间
    m_ReqSendTime = time(NULL);

    //  如果配置成功,则更新dhcp报文发送使用的服务器IP地址和MAC地址
    memcpy( &m_stDhcpInterface.siaddr , m_stDhcpOption.val[LXI_DHCP_OPTION_SERVER_IDENTIFIER] , 4 );
    memcpy( m_stDhcpInterface.shaddr , m_stRecvMsg.ethhdr.ether_shost , ETH_ALEN );

    return 0;
}

s32  lxi_dhcp_discover( void )
{
    //  初始化DHCP操作记录
    memset( &m_stDhcpOption , '\0' , sizeof( m_stDhcpOption ) );
    //  发送DHCP DISCOVER报文
    if( lxi_dhcp_send_and_recv( random() ,
                                LXI_DHCP_MSG_TYPE_OFFER ,
                                lxi_dhcp_discover_msg_process ) )
    {
        //  失败,没有找到DHCP服务器
        return -1;
    }
    //  更新DHCP发送接口的数据
    m_stDhcpInterface.ciaddr = m_pstDhcpRecvMsg->yiaddr;
    m_stDhcpInterface.xid    = m_pstDhcpRecvMsg->xid;
    memcpy( &m_stDhcpInterface.siaddr , m_stDhcpOption.val[LXI_DHCP_OPTION_SERVER_IDENTIFIER] , 4 );
    memcpy( m_stDhcpInterface.shaddr , m_stRecvMsg.ethhdr.ether_shost , ETH_ALEN );

    return 0;
}

s32  lxi_dhcp_init( void )
{
    struct ifreq        ifr;
    struct sockaddr_pkt sap;
    struct sockaddr_in  clientAddr;
    s32                 flags;
    s32                 on = 1;
    s32                 divisor;

    //  初始化接口描述
    memset(&ifr,0,sizeof(struct ifreq));
    memcpy(ifr.ifr_name,m_as8CurrDevName,strlen(m_as8CurrDevName));

    //  申请pkt-socket
    if( ( m_s32DhcpSocket = socket( PF_PACKET , SOCK_PACKET , htons(ETH_P_ALL) ) ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:Create Fail!\n" );
        return (-1);
    }
    //  设置该socket为非阻塞模式
    if( ( flags = fcntl( m_s32DhcpSocket , F_GETFL , 0 ) ) == -1 || fcntl( m_s32DhcpSocket , F_SETFL , flags | O_NONBLOCK ) == -1 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:fcntl Fail!\n" );
        return (-1);
    }
    //  获取MAC地址
    if( ioctl( m_s32DhcpSocket , SIOCGIFHWADDR , &ifr ) )
    {
        LXI_DEBUG( "lxi_net.c:Socket:ioctl SIOCGIFHWADDR!\n" );
        return (-1);
    }
    memcpy( m_stNetConfPara.as8MacAddr , ifr.ifr_hwaddr.sa_data , ETH_ALEN);
    //  判断当前的网络适配器是否可以进行ARP数据传输
    if( ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER )
    {
        return (-1);
    }
    //  设置Socket支持广播
    if ( setsockopt( m_s32DhcpSocket , SOL_SOCKET , SO_BROADCAST , &on , sizeof(on) ) == -1)
    {
        LXI_DEBUG( "lxi_net.c:Socket:setsockopt fail!\n" );
        return (-1);
    }
    //  获取当前网络适配器的配置
    if( ioctl( m_s32DhcpSocket , SIOCGIFFLAGS , &ifr ) )
    {
        LXI_DEBUG( "lxi_net.c:Socket:ioctl SIOCGIFFLAGS!\n" );
        return (-1);
    }
    //  设置当前网络适配器的功能
    ifr.ifr_flags = ifr.ifr_flags | IFF_UP | IFF_BROADCAST | IFF_NOTRAILERS | IFF_RUNNING;
    if( ioctl( m_s32DhcpSocket , SIOCSIFFLAGS , &ifr ) )
    {
        LXI_DEBUG( "lxi_net.c:Socket:ioctl SIOCSIFFLAGS!\n" );
        return (-1);
    }
    //  绑定本地地址
    memset( &sap , '\0' , sizeof( sap ) );
    sap.spkt_family = PF_PACKET;
    sap.spkt_protocol = htons(ETH_P_ALL);
    memcpy( sap.spkt_device , m_as8CurrDevName , strlen(m_as8CurrDevName) );
    if( bind( m_s32DhcpSocket , (void*)&sap , sizeof(struct sockaddr) ) == -1 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:bind fail!\n" );
        return (-1);
    }
    //  设置随机数产生因子
    divisor = time(NULL) + m_stNetConfPara.as8MacAddr[5]
                         + 4  * m_stNetConfPara.as8MacAddr[4]
                         + 8  * m_stNetConfPara.as8MacAddr[3]
                         + 16 * m_stNetConfPara.as8MacAddr[2]
                         + 32 * m_stNetConfPara.as8MacAddr[1]
                         + 64 * m_stNetConfPara.as8MacAddr[0];
    srandom( divisor );
    m_s16DhcpClientID = divisor & 0xFFFF;
    //  创建UDP socket
    if( ( m_s32UdpFooSocket = socket( AF_INET , SOCK_DGRAM , 0 ) ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:Socket:UDP Create Fail!\n" );
        return (-1);
    }
    //  设置UDP socket支持广播
    if( setsockopt( m_s32UdpFooSocket , SOL_SOCKET , SO_BROADCAST , &on , sizeof(on) ) )
    {
        LXI_DEBUG( "lxi_net.c:Socket:UDP setsockopt Fail!\n" );
        close( m_s32UdpFooSocket );
        m_s32UdpFooSocket = -1;
        return (-1);
    }

    //  绑定本地地址
    clientAddr.sin_family = AF_INET;
    clientAddr.sin_port = htons(LXI_DHCP_CLIENT_PORT);
    clientAddr.sin_addr.s_addr = INADDR_ANY;

    if( bind( m_s32UdpFooSocket , (struct sockaddr *)&clientAddr , sizeof(clientAddr) ) )
    {
        if( errno != EADDRINUSE )
        {
            LXI_DEBUG( "lxi_net.c:Socket:UDP bind fail!\n" );
        }
        close(m_s32UdpFooSocket);
        m_s32UdpFooSocket = -1;
        return (-1);
    }
    else
    {
        //  设置非阻塞模式
        if( fcntl( m_s32UdpFooSocket , F_SETFL , O_NONBLOCK ) == -1 )
        {
            LXI_DEBUG( "lxi_net.c:Socket:Udp fcntl fail!\n" );
            close(m_s32UdpFooSocket);
            m_s32UdpFooSocket = -1;
            return (-1);
        }
    }
    return 0;
}

s32  lxi_dhcp_reboot( void )
{
    if( 1 == lxi_dhcp_send_and_recv( random() ,
                                     LXI_DHCP_MSG_TYPE_ACK ,
                                     lxi_dhcp_reboot_msg_process ) )
    {
        return 0;
    }
    return -1;
}

s32  lxi_dhcp_renew( void )
{
    u64   u64Delay;
    u64   u64TimeCnt = 0;

    int  ttl = ntohl(*(u32 *)m_stDhcpOption.val[LXI_DHCP_OPTION_T1_VALUE] );
    //  计算等待时间
    u64Delay = m_ReqSendTime - time( NULL ) + ttl;
    //  转化为毫秒
    //u64Delay *= 1000;
    while( u64Delay > 0 && u64TimeCnt < u64Delay )
    {
        if( true == m_bLxiDhcpRelease )
        {
            return -2;
        }

        if( true == m_b8DhcpServerReset )
        {
            m_b8DhcpServerReset = false;
            return -3;
        }

        sleep( 1 );
        u64TimeCnt += 1;
    }

    if( 0 == lxi_dhcp_send_and_recv( random() ,
                                     LXI_DHCP_MSG_TYPE_ACK ,
                                     lxi_dhcp_renew_msg_process ) )
    {
        m_ReqSendTime = time( NULL );
        return 0;
    }
    return -1;
}

s32  lxi_dhcp_rebind( void )
{
    s32 i;

    //  计算等待时间
    i = m_ReqSendTime + ntohl(*(u32 *)m_stDhcpOption.val[LXI_DHCP_OPTION_T2_VALUE] ) - time( NULL );
    if( i > 0 )
    {
        sleep(i);
    }

    if( 0 == lxi_dhcp_send_and_recv( random() ,
                                     LXI_DHCP_MSG_TYPE_ACK ,
                                     lxi_dhcp_rebind_msg_process ) )
    {
        m_ReqSendTime = time( NULL );
        //  重新更新DHCP服务器的地址
        memcpy( &m_stDhcpInterface.siaddr , m_stDhcpOption.val[LXI_DHCP_OPTION_SERVER_IDENTIFIER] , m_stDhcpOption.len[LXI_DHCP_OPTION_SERVER_IDENTIFIER] );
        memcpy( m_stDhcpInterface.shaddr  , m_stRecvMsg.ethhdr.ether_shost , ETH_ALEN );
        return 0;
    }
    return -1;
}

s32  lxi_dhcp_release( void )
{
    struct sockaddr addr;

    //  组织Release报文
    lxi_dhcp_release_msg_process( random() );
    //  初始化发送地址
    memset( &addr , 0 , sizeof(struct sockaddr) );
    memcpy( addr.sa_data , m_as8CurrDevName , strlen( m_as8CurrDevName ) );
    //  发送报文
    if( sendto( m_s32DhcpSocket ,
                &m_stSendMsg ,
                sizeof( LXI_DHCP_ETHER_HEADER_STRU ) + sizeof( LXI_DHCP_UDP_IP_HDR_STRU ) + sizeof( LXI_DHCP_PACKET_STRU ) ,
                0 ,
                &addr ,
                sizeof( struct sockaddr ) ) == -1 )
    {
        LXI_DEBUG( "lxi_net.c:DHCP:Release Message Send Fail!\n" );
    }
    //  清除网络中关于本机IP地址的缓存
    lxi_dhcp_send_release_arp( );
    //  清除本地IP地址
    m_stDhcpInterface.ciaddr = 0 ;

    return 0;
}

void *lxi_dhcp_service_thread( void * pInitPara )
{
    LXI_DHCP_WORK_STATUS_EM     emCurrState = LXI_DHCP_WORK_STATUS_REBOOT;
    s32                         s32Ret = 0;
    u8                        bDhcpServerLost = false;
    u8                        bDhcpDiscoverExit = false;

    pInitPara = pInitPara;
    //  DHCP客户端状态处理
    while( 1 )
    {
        //  等待DHCP开始配置的信号量
        lxi_sem_wait( &m_semIpConfStart , 0 );
        bDhcpDiscoverExit = false;
        if( false == m_stNetConfPara.u8DhcpEnable )
        {
            continue;
        }
        while( 1 )
        {
            if( true == bDhcpDiscoverExit ||
                LXI_NET_LINK_STATUS_NOT_LINK == m_emCurrLinKState )
            {
                LXI_DEBUG( "lxi_net.c:DHCP:Disable!\n" );
                break;
            }
            switch( emCurrState )
            {
                case LXI_DHCP_WORK_STATUS_REBOOT:
                    if( 0 == lxi_dhcp_reboot() )
                    {
                        emCurrState = LXI_DHCP_WORK_STATUS_DISCOVER;
                    }
                    break;
                case LXI_DHCP_WORK_STATUS_DISCOVER:
                    if( 0 == lxi_dhcp_discover() )
                    {
                        emCurrState = LXI_DHCP_WORK_STATUS_REQUEST;
                        if( true == m_bDhcpFirstConfig )
                        {
                            m_bDhcpFirstConfig = false;
                        }
                        if( true == bDhcpServerLost )
                        {
                            m_bDhcpAgainDiscover = true;
                            bDhcpServerLost = false;
                        }
                    }
                    else
                    {
                        bDhcpServerLost = true;
                        if( true == m_bDhcpFirstConfig )
                        {
                            m_bDhcpServerNoFind = true;
                            lxi_sem_post( &m_semIpConfFinish );
                        }
                    }
                    break;
                case LXI_DHCP_WORK_STATUS_REQUEST:
                    if( 0 == lxi_dhcp_request() )
                    {
                        lxi_sem_post( &m_semIpConfFinish );
                        emCurrState = LXI_DHCP_WORK_STATUS_RENEW;
                    }
                    break;
                case LXI_DHCP_WORK_STATUS_RENEW:
                    s32Ret = lxi_dhcp_renew();
                    if( -1 ==  s32Ret )
                    {
                        emCurrState = LXI_DHCP_WORK_STATUS_REBIND;
                    }
                    else if( -2 == s32Ret )
                    {
                        emCurrState = LXI_DHCP_WORK_STATUS_RELEASE;
                    }
                    else if( -3 == s32Ret )
                    {
                        emCurrState = LXI_DHCP_WORK_STATUS_DISCOVER;
                    }

                    break;
                case LXI_DHCP_WORK_STATUS_REBIND:
                    if( 0 != lxi_dhcp_rebind() )
                    {
                        //  上报DHCP分配的IP地址失效的状态
                        m_bDhcpServerLost = true;
                        bDhcpServerLost = true;
                        emCurrState = LXI_DHCP_WORK_STATUS_RELEASE;
                    }
                    else
                    {
                        emCurrState = LXI_DHCP_WORK_STATUS_RENEW;
                    }
                    break;
                case LXI_DHCP_WORK_STATUS_RELEASE:
                    lxi_dhcp_release();
                    //  如果处于IP地址释放等待完成状态
                    if( true == m_bLxiDhcpRelease )
                    {
                        lxi_sem_post( &m_semIpRelease );
                        bDhcpDiscoverExit = true;
                    }
                    emCurrState = LXI_DHCP_WORK_STATUS_DISCOVER;
                    break;
            }
        }
    }
    return NULL;
}

void *lxi_ip_control_thread( void *pInitPara )
{
    LXI_NET_LINK_STATUS_EM      emLastLinkState = LXI_NET_LINK_STATUS_NOT_LINK;
    LXI_STATUS_EM               emLxiStatus = LXI_STATUS_UNLINK;

    pInitPara=pInitPara;
    while( 1 )
    {
        m_emCurrLinKState = lxi_net_link_status_get( m_as8CurrDevName );
        //printf("old:%d, new:%d\n", emLastLinkState,m_emCurrLinKState);
        if( m_emCurrLinKState != emLastLinkState )
        {
            //  连接状态发生改变,提示消息
            if( LXI_NET_LINK_STATUS_IS_LINK == m_emCurrLinKState )
            {
                emLxiStatus = LXI_STATUS_LINK;
                LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                //  如果当前网卡的状态由未连接变化到连接,则进行IP的配置
                m_bLxiIpConfig = true;
            }
            else
            {
                //  当前网络已经断开
                emLxiStatus = LXI_STATUS_UNLINK;
                LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                if( m_emCurrIpCfgMode == LXI_IP_CONFIG_MODE_DHCP )
                {
                    m_b8DhcpServerReset = true;
                }
                m_emCurrIpCfgMode = LXI_IP_CONFIG_MODE_NONE;
            }
            emLastLinkState = m_emCurrLinKState;
        }
        //  判断是否发生了DHCP服务器分配的IP地址失效
        if( true == m_bDhcpServerLost )
        {
            //  上报DHCP服务器分配的IP地址失效
            emLxiStatus = LXI_STATUS_DHCP_IP_LOSE;
            LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
            //  置当前IP配置模式为空
            m_emCurrIpCfgMode = LXI_IP_CONFIG_MODE_NONE;
            //  清除标志位
            m_bDhcpServerLost = false;
            //  跳转到其他模式的配置
            goto other_mode;
        }
        //  再次发现了DHCP服务器并且获取到IP地址,则跳转到配置流程
        if( true == m_bDhcpAgainDiscover )
        {
            //  如果此时已有AUTOIP或者手动IP地址,则进行ARP广播释放该地址
            if( LXI_IP_CONFIG_MODE_NONE != m_emCurrIpCfgMode )
            {
                lxi_dhcp_send_release_arp();
            }
            m_emCurrIpCfgMode = LXI_IP_CONFIG_MODE_DHCP;
            m_bDhcpAgainDiscover = false;
            goto finish;
        }
        //  当前网卡已经连接,且发生了IP配置,则进入
        if( LXI_NET_LINK_STATUS_IS_LINK == m_emCurrLinKState && true == m_bLxiIpConfig )
        {
            //  网络配置进入初始化
            emLxiStatus = LXI_STATUS_INIT;
            LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
            //  如果当前还没有IP地址的配置,则不注销IP地址的操作
            if( LXI_IP_CONFIG_MODE_NONE != m_emCurrIpCfgMode )
            {
                if( LXI_IP_CONFIG_MODE_DHCP == m_emCurrIpCfgMode )
                {
                    m_bLxiDhcpRelease = true;
                    //  等待IP注销完毕
                    lxi_sem_wait( &m_semIpRelease , 0 );
                    m_bLxiDhcpRelease = false;
                }
                else
                {
                    //  对于AUTOIP和手动IP的配置,发送ARP广播注销IP地址
                    lxi_dhcp_send_release_arp();
                }
                m_emCurrIpCfgMode = LXI_IP_CONFIG_MODE_NONE;
            }
            //  初始化本地IP记录,修改由手动配置IP后，DHCP获取IP地址失败的问题
            m_stDhcpInterface.ciaddr = 0;
            //  开始IP地址的配置
            if( true == m_stNetConfPara.u8DhcpEnable )
            {
                LXI_DEBUG( "lxi_net.c:IPConfig:IP Start Config!\n" );
                //  开始DHCP客户端的工作
                lxi_sem_post( &m_semIpConfStart );
                //  等待IP获取完成
                lxi_sem_wait( &m_semIpConfFinish , 0 );
                //  判断IP地址是否获取成功
                if( true == m_bDhcpServerNoFind )
                {
                    //  DHCP服务器没有发现,上报DHCP服务器获取IP地址失败的消息,并转入其他模式配置
                    emLxiStatus = LXI_STATUS_DHCP_FAIL;
                    LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                    m_bDhcpServerNoFind = false;
                    goto other_mode;
                }
                m_emCurrIpCfgMode = LXI_IP_CONFIG_MODE_DHCP;
                goto finish;
            }
        other_mode:
            //  AUTOIP配置
            if( true == m_stNetConfPara.u8AutoIpEnable )
            {
                if ( 0 == lxi_autoip_config( ) )
                {
                    m_emCurrIpCfgMode = LXI_IP_CONFIG_MODE_AUTOIP;
                    goto finish;
                }
            }
            //  手动配置
            if( true == m_stNetConfPara.u8ManualIpEnable )
            {
                if( 0 != lxi_ip_addr_check( m_stStaticIpInfo.ipaddr ) )
                {
                    //  IP地址冲突,提示消息
                    emLxiStatus = LXI_STATUS_IP_CONFLICT;
                    LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                    goto fail;
                }
                m_emCurrIpCfgMode = LXI_IP_CONFIG_MODE_MANUALIP;
            }
        finish:
            if( LXI_IP_CONFIG_MODE_NONE != m_emCurrIpCfgMode )
            {
                //  如果IP地址获取完成,则更新IP地址值,并配置
                if( LXI_IP_CONFIG_MODE_DHCP == m_emCurrIpCfgMode )
                {
                    // ip地址
                    m_stNetConfPara.s32IpAddr   = m_stDhcpInterface.ciaddr;
                    //  子网掩码
                    m_stNetConfPara.s32MaskAddr = *(u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_SUBNET_MASK];
                    //  网关
                    m_stNetConfPara.s32GateAddr = *(u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_ROUTER];
                    //  DNS
                    if( m_stDhcpOption.len[LXI_DHCP_OPTION_DNS] == 4 )
                    {
                        m_stNetConfPara.s32DnsAddr[0]  = *(u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_DNS];
                        m_stNetConfPara.s32DnsAddr[1]  = 0;
                    }
                    else if( m_stDhcpOption.len[LXI_DHCP_OPTION_DNS] == 8 )
                    {
                        m_stNetConfPara.s32DnsAddr[0]  = *(u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_DNS];
                        m_stNetConfPara.s32DnsAddr[1]  = *((u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_DNS] + 1 );
                    }
                    //  DHCP服务器
                    m_s32LxiDhcpServer = *(u32*)m_stDhcpOption.val[LXI_DHCP_OPTION_SERVER_IDENTIFIER];
                    //  域名
                    memset( m_as8CurrDomainName , '\0' , LXI_DOMAIN_NAME_MAX_LEN );
                    memcpy( m_as8CurrDomainName , m_stDhcpOption.val[LXI_DHCP_OPTION_DOMAIN_NAME] , m_stDhcpOption.len[LXI_DHCP_OPTION_DOMAIN_NAME] );
                }
                else if( LXI_IP_CONFIG_MODE_AUTOIP == m_emCurrIpCfgMode )
                {
                    //  ip地址
                    m_stNetConfPara.s32IpAddr = m_stDhcpInterface.ciaddr;
                    //  子网掩码
                    m_stNetConfPara.s32MaskAddr = 0x0000FFFF;
                    //  网关
                    m_stNetConfPara.s32GateAddr = 0;
                    //  DNS地址
                    m_stNetConfPara.s32DnsAddr[0] = 0;
                    m_stNetConfPara.s32DnsAddr[1] = 0;
                    //  DHCP服务器
                    m_s32LxiDhcpServer = 0;
                    //  域名
                    memset( m_as8CurrDomainName , '\0' , LXI_DOMAIN_NAME_MAX_LEN );
                    memcpy( m_as8CurrDomainName , "workgroup" , strlen( "workgroup" ) );
                }
                else
                {
                    m_stDhcpInterface.ciaddr = m_stStaticIpInfo.ipaddr;
                    //  ip地址
                    m_stNetConfPara.s32IpAddr = m_stStaticIpInfo.ipaddr;
                    //  子网掩码
                    m_stNetConfPara.s32MaskAddr = m_stStaticIpInfo.mask;
                    //  网关
                    m_stNetConfPara.s32GateAddr = m_stStaticIpInfo.gate;
                    //  DNS地址
                    m_stNetConfPara.s32DnsAddr[0] = m_stStaticIpInfo.dns[0];
                    m_stNetConfPara.s32DnsAddr[1] = m_stStaticIpInfo.dns[1];
                    //  DHCP服务器
                    m_s32LxiDhcpServer = 0;
                    //  域名
                    memset( m_as8CurrDomainName , '\0' , LXI_DOMAIN_NAME_MAX_LEN );
                    memcpy( m_as8CurrDomainName , "workgroup" , strlen( "workgroup" ) );
                }
                //  配置到NET中
                if( lxi_ip_configuration( ) )
                {
                    //  提示IP地址配置无效
                    emLxiStatus = LXI_STATUS_INVALID_IP_ADDR;
                    LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                }
                else
                {
                    //  提示IP地址配置成功
                    emLxiStatus = LXI_STATUS_CONFIG_SUCCESS;
                    LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                }
            }
            else
            {
                //  置初值
                // ip地址
                m_stNetConfPara.s32IpAddr   = 0x0100007F;
                //  子网掩码
                m_stNetConfPara.s32MaskAddr = 0x000000FF;
                //  网关
                m_stNetConfPara.s32GateAddr = 0;
                //  DNS
                m_stNetConfPara.s32DnsAddr[0]  = 0;
                m_stNetConfPara.s32DnsAddr[1]  = 0;
                //  DHCP服务器
                m_s32LxiDhcpServer = 0;
                //  域名
                memset( m_as8CurrDomainName , '\0' , LXI_DOMAIN_NAME_MAX_LEN );
                memcpy( m_as8CurrDomainName , "workgroup" , strlen( "workgroup" ) );
                //  配置到NET
                if( lxi_ip_configuration( ) )
                {
                    //  提示IP地址配置无效
                    emLxiStatus = LXI_STATUS_INVALID_IP_ADDR;
                    LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                }
            }
        fail:
            m_bLxiIpConfig = false;
        }
        //  监控周期
        sleep( 1 );
    }
}

/*
 * IP配置模块初始化
*/
void lxi_net_init( s8 *ps8DevName , LXI_PARAMETER_INFO_STRU * pstInitPara )
{
    //  初始化本地变量
    memset( &m_stNetConfPara , '\0' , sizeof( LXI_PARAMETER_INFO_STRU) );
    m_stNetConfPara = *pstInitPara;

    //  如果手动IP地址打开,则复制IP配置值
    if( true == m_stNetConfPara.u8ManualIpEnable )
    {
        m_stStaticIpInfo.ipaddr = m_stNetConfPara.s32IpAddr;
        m_stStaticIpInfo.gate   = m_stNetConfPara.s32GateAddr;
        m_stStaticIpInfo.mask   = m_stNetConfPara.s32MaskAddr;
        m_stStaticIpInfo.dns[0] = m_stNetConfPara.s32DnsAddr[0];
        m_stStaticIpInfo.dns[1] = m_stNetConfPara.s32DnsAddr[1];
    }
    //  置IP地址重新配置
    m_bLxiIpConfig = true;

    //  初始化DHCP报文发送使用的资源
    m_s32MagicCookie   = LXI_DHCP_MAGIC_COOKIE;
    m_u16DhcpMsgSize   = htons( sizeof(LXI_DHCP_PACKET_STRU) + sizeof(LXI_DHCP_UDP_IP_HDR_STRU) );
    m_u32DhcpLeaseTime = LXI_DHCP_DEFAULT_LEASETIME;
    memset( &m_stDhcpInterface , '\0'      , sizeof( m_stDhcpInterface ) );
    memset( &m_stDhcpOption    , '\0'      , sizeof( m_stDhcpOption ) );
    memset( m_as8CurrDevName   , '\0'      , LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN );

    //  设置网络适配器名
    strcpy( m_as8CurrDevName , ps8DevName );
    //  建立DHCP客户端工作的socket
    if( lxi_dhcp_init() )
    {
        LXI_DEBUG( "Dhcp Client Socket Create Fail !\n" );
        return ;
    }
    //  建立DHCP协议类标识
    lxi_dhcp_class_id_setup();

    //  创建线程使用的信号量
    lxi_sem_open( &m_semIpRelease , "IP Release" , 0 );
    lxi_sem_open( &m_semIpConfFinish , "Dhcp Finish" , 0 );
    lxi_sem_open( &m_semIpConfStart , "Dhcp Start" , 0 );
    //  建立DHCP配置线程
    lxi_thread_create( &m_DhcpClientId , lxi_dhcp_service_thread , NULL );
    //  建立IP配置管理线程
    lxi_thread_create( &m_IpConfigId , lxi_ip_control_thread , NULL );
}

/*
 * 获取当前设备拥有的网络适配器名
*/
void lxi_net_device_info( LXI_NETWORK_ADAPTOR_INFO_STRU *pstAdaptorInfo )
{
    s32                         sockfd;
    struct  ifconf              iflist;
    struct  ifreq               buf[LXI_NETWORK_ADAPTOR_MAX_NUM];
    u32                         u32InterfaceNum;
    u32                         i;

    //  设置网络适配器信息占用的空间
    iflist.ifc_len = sizeof( buf );
    iflist.ifc_ifcu.ifcu_buf = (caddr_t)buf;

    //  申请一个socket
    if( ( sockfd = socket( AF_INET , SOCK_DGRAM , IPPROTO_IP ) ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:DEVICE:Socket Create Fail!\n" );
        return;
    }

    //  获取当前NET物理层连接状态
    if( ioctl( sockfd , SIOCGIFCONF , &iflist ) < 0 )
    {
        LXI_DEBUG( "lxi_net.c:DEVICE:Socket ioctl Fail!\n" );
        return;
    }

    //  分解网卡信息
    u32InterfaceNum = iflist.ifc_len / sizeof( struct ifreq );

    //  初始化当前网络适配器数
    pstAdaptorInfo->u32Num = 0;

    //  开始赋值
    for( i = 0 ; i < u32InterfaceNum ; i ++ )
    {
        //  复制网网络适配器的名字
        strncpy( pstAdaptorInfo->as8Name[i] , buf[i].ifr_name , strlen( buf[i].ifr_name ) );
        //  获取当前网络适配器是否启动
        if( ioctl( sockfd , SIOCGIFFLAGS , &buf[i] ) )
        {
            LXI_DEBUG( "lxi_net.c:DEVICE:Network Adaptor Flags Read Fail!\n" );
            continue;
        }
        //  本地回环
        if( buf[i].ifr_flags & IFF_LOOPBACK )
        {
            continue;
        }
        //  进行网络适配器的信息COPY
        memset( pstAdaptorInfo->as8Name[pstAdaptorInfo->u32Num] , '\0' , LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN );
        strcpy( pstAdaptorInfo->as8Name[pstAdaptorInfo->u32Num] , buf[i].ifr_name );
        pstAdaptorInfo->au8Open[pstAdaptorInfo->u32Num] = (u8)( buf[i].ifr_flags & IFF_UP );
        pstAdaptorInfo->u32Num ++;
    }
    close( sockfd );
}

/*
 * 配置网络
*/
void lxi_net_config( LXI_PARAMETER_INFO_STRU *pstCfgPara )
{
    m_stNetConfPara = *pstCfgPara;
    //  如果手动IP地址打开,则复制IP配置值
    if( true == m_stNetConfPara.u8ManualIpEnable )
    {
        m_stStaticIpInfo.ipaddr = m_stNetConfPara.s32IpAddr;
        m_stStaticIpInfo.gate   = m_stNetConfPara.s32GateAddr;
        m_stStaticIpInfo.mask   = m_stNetConfPara.s32MaskAddr;
        m_stStaticIpInfo.dns[0] = m_stNetConfPara.s32DnsAddr[0];
        m_stStaticIpInfo.dns[1] = m_stNetConfPara.s32DnsAddr[1];
    }
    //  置IP地址重新配置
    m_bLxiIpConfig = true;

    if( pstCfgPara->u8DhcpEnable )
    {
        m_bDhcpFirstConfig = true;
    }
}

/*
 * 获取网络参数
*/
void lxi_net_para_get( LXI_PARAMETER_INFO_STRU *pstPara )
{
    //  获取当前网络适配器的参数
    lxi_net_speed_and_duplex_get( m_as8CurrDevName , &m_emCurrNetSpeed , &m_emCurrNetWorkMode );
    //  更新网络配置模式
    m_stNetConfPara.emIpMode = m_emCurrIpCfgMode;

    //  赋值
    *pstPara = m_stNetConfPara;
}

/*
 * 设置当前使用的网络适配器
*/
void lxi_net_device_select( s8 *ps8DevName )
{
    //  复制网络适配器名
    memset( m_as8CurrDevName , '\0' , LXI_NETWORK_ADAPTOR_DESCRIBE_MAX_LEN );
    strcpy( m_as8CurrDevName , ps8DevName );
}

/*
 *添加路由地址
*/
void lxi_net_route_add( s32 s32GateWay , s32 s32SubMask )
{
    struct sockaddr_in* addr;
    struct rtentry      rtent;

    bzero( &rtent , sizeof( struct rtentry ) );
    addr = ( struct sockaddr_in* )&rtent.rt_dst;
    addr->sin_family      = AF_INET;
    addr->sin_addr.s_addr = 0;
    addr = ( struct sockaddr_in* )&rtent.rt_gateway;
    addr->sin_family      = AF_INET;
    addr->sin_addr.s_addr = s32GateWay;
    addr = ( struct sockaddr_in* )&rtent.rt_genmask;
    addr->sin_family      = AF_INET;
    addr->sin_addr.s_addr = s32SubMask;
    rtent.rt_dev          = m_as8CurrDevName;
    rtent.rt_metric       = 1;
    rtent.rt_flags        = RTF_UP | RTF_GATEWAY;
    if( ioctl( m_s32DhcpSocket , SIOCADDRT , &rtent ) == -1 )
    {
        if( errno == ENETUNREACH )  //  如果没有配置成功,可能当前路由没有查找到,添加路由地址
        {
            bzero( &rtent , sizeof( struct rtentry ) );
            addr = ( struct sockaddr_in* )&rtent.rt_dst;
            addr->sin_family      = AF_INET;
            addr->sin_addr.s_addr = s32GateWay;
            addr = ( struct sockaddr_in* )&rtent.rt_gateway;
            addr->sin_family      = AF_INET;
            addr->sin_addr.s_addr = 0;
            addr = ( struct sockaddr_in* )&rtent.rt_genmask;
            addr->sin_family      = AF_INET;
            addr->sin_addr.s_addr = 0xFFFFFFFF;
            rtent.rt_dev          = m_as8CurrDevName;
            rtent.rt_metric       = 1;
            rtent.rt_flags        = RTF_UP | RTF_HOST;
            //  再次设置
            if( ioctl( m_s32DhcpSocket , SIOCADDRT , &rtent ) == 0 )
            {
                //  配置成功，再次配置路由地址
                bzero( &rtent , sizeof( struct rtentry ) );
                addr = ( struct sockaddr_in* )&rtent.rt_dst;
                addr->sin_family      = AF_INET;
                addr->sin_addr.s_addr = 0;
                addr = ( struct sockaddr_in* )&rtent.rt_gateway;
                addr->sin_family      = AF_INET;
                addr->sin_addr.s_addr = s32GateWay;
                addr = ( struct sockaddr_in* )&rtent.rt_genmask;
                addr->sin_family      = AF_INET;
                addr->sin_addr.s_addr = s32SubMask;
                rtent.rt_dev          = m_as8CurrDevName;
                rtent.rt_metric       = 1;
                rtent.rt_flags        = RTF_UP | RTF_GATEWAY;
            }
            if( ioctl( m_s32DhcpSocket , SIOCADDRT , &rtent ) == -1 )
            {
                LXI_DEBUG( "lxi_net.c:IPConfig:RT Addr Set Fail!\n" );
            }
        }
    }
}
