/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: lxi_arch.c
  功能描述: 用于集成数字系统平台,这里是对数字系统平台兼容性函数的封装和定义

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-10-28

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-10-28          1.0        initial

*******************************************************************************/
#include    <fcntl.h>
#include    <pthread.h>
#include    <semaphore.h>
#include    <unistd.h>
#include    "DataType.h"
#include    "lxi_arch.h"


/*
 * lxi_thread_create
 * LXI模块对于数字系统平台的线程创建函数的封装
 * Parameter1:  pThreadId   --  线程ID
 * Parameter2:  pFunc       --  线程的执行函数指针
 * Parameter3:  pInitPara   --  该线程的初始化参数
 * Return    :  线程创建错误码值
*/

s32 lxi_thread_create(lxi_pthread_t *pThreadId,
                      void          *(*pFunc)(void*),
                      void          *pInitPara)
{
    /*创建线程属性*/
    return pthread_create(pThreadId,NULL,pFunc,pInitPara);
}

/*
 * lxi_sem_open
 * 信号量创建
 * Parameter1:  pSemaphore  --  创建的信号量指针
 * Parameter2:  ps8SemName  --  信号量的名称
 * Parameter3:  u32InitVal  --  信号量的初始值
 * Return    :  信号量创建错误码值:  0   --  无错误
 *                          -1  --  错误码值
*/
s32 lxi_sem_open(lxi_sem_t  *pSemaphore ,
                 s8         *ps8SemName ,
                 u32         u32InitVal)
{
    /*创建信号量*/
    pSemaphore = sem_open(ps8SemName,O_CREAT,0644,u32InitVal);
    /*判断信号量是否创建成功*/
    if(pSemaphore   ==  SEM_FAILED)
    {
        /*创建信号量失败*/
        return -1;
    }
    else
    {
        /*信号量创建成功*/
        return  0;
    }
}

/*
 *lxi_sem_wait
 *挂起信号量，等待信号量激活
 *Parameter1:   pSemaphore  --  需要进入挂起状态的信号量指针
 *Parameter2:   u32WaitTime --  进入挂起状态的时间,单位us
 *Return    :   错误码值
*/
s32 lxi_sem_wait(lxi_sem_t  *pSemaphore ,   u32 u32WaitTime)
{
    u32WaitTime= u32WaitTime;
    /*Linux平台的信号挂起操作属于阻塞式的,不进行时间的设置*/
    return  sem_wait(pSemaphore);
}

/*
 * lxi_sem_post
 * 激活信号量，向信号量发送激励
 * Parameter1:  pSemaphore  --  需要进入激活状态的信号量指针
 * Return    :   错误码值
*/
s32 lxi_sem_post(lxi_sem_t  *pSemaphore)
{
    return  sem_post(pSemaphore);
}

/*
 * lxi_sem_close
 * 关闭指定的信号量，释放资源
 * Parameter1:  pSemaphore  --  需要关闭信号量指针
 * Return    :   错误码值
*/
s32 lxi_sem_close(lxi_sem_t *pSemaphore )
{
    return  sem_close(pSemaphore);
}

/*
 * lxi_sem_value_get
 * 获取指定的信号量当前的值
 * Parameter1:  pSemaphore      --  需要获取信号量值的指针
 * Parameter2:  pu32SemValue    --  当前信号量的值
 * Return    :   错误码值
*/
s32 lxi_sem_value_get(lxi_sem_t *pSemaphore ,   s32 *ps32SemValue)
{
    return  sem_getvalue(pSemaphore,ps32SemValue);
}

/*
 * lxi_sleep
 * LXI模块使用计时器接口
 * Parameter:   u32TimeVal  --  需要延迟的值,单位us
*/

void lxi_sleep(u32  u32TimeVal)
{
    usleep(u32TimeVal);
}
