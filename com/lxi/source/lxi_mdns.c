
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "lxi_mdns.h"

extern DNS_HEADER_QR_E lxi_mdns_packet_parse(s8 *ps8Buff , s32 s32BuffLen);

s32                         m_s32MdnsSock;
LXI_STATUS_EM               m_emLastNetStatus = LXI_STATUS_UNLINK;
LXI_MDNS_STATE_EM           m_emMdnsState     = LXI_MDNS_STATE_HOST_PROBE;
lxi_sem_t                   m_semMdnsEvent;
LXI_MDNS_EVENT_QUEUE_STRU   m_stMdnsEventQueue;
s8                          m_as8MdnsRecvBuf[LXI_MDNS_PACKET_MAX_SIZE];
s8                          m_as8MdnsSendBuf[LXI_MDNS_PACKET_MAX_SIZE];
s8                          m_as8MdnsParseBuf[LXI_MDNS_PACKET_MAX_SIZE];
s8                          m_as8MdnsHttpTxtInfo[LXI_MDNS_PACKET_MAX_SIZE];
s8                          m_as8MdnsLxiTxtInfo[LXI_MDNS_PACKET_MAX_SIZE];
s8                          m_as8MdnsTempBuf[LXI_MDNS_PACKET_MAX_SIZE];
s8                          m_as8MdnsQuestions[LXI_MDNS_MAX_QUESTIONS_NUM][LXI_MDNS_NAME_MAX_LEN];
u16                         m_au16MdnsQuestType[LXI_MDNS_MAX_QUESTIONS_NUM];
s8                          m_as8MdnsResponses[LXI_MDNS_MAX_RESPONSE_NUM][LXI_MDNS_NAME_MAX_LEN];
u16                         m_au16MdnsRespType[LXI_MDNS_MAX_RESPONSE_NUM];
u32                         m_u32MdnsCueeQueryNum = 0;

/*
 * mDNS 事件队列的初始化
*/
void lxi_mdns_event_queue_init( void )
{
    m_stMdnsEventQueue.front = 0;    /*队列首指针初始为0*/
    m_stMdnsEventQueue.rear  = 0;    /*队列尾指针初始为0*/
    m_stMdnsEventQueue.full  = 0;    /*清空队列满标志*/
    lxi_sem_open( &m_stMdnsEventQueue.wr_en , "mDNSEventWritePermit" , 0 ); /*mDNSs事件队列的写控制*/
    lxi_sem_open( &m_stMdnsEventQueue.rd_en , "mDNSEventReadPermit"  , 0 ); /*mDNS事件队列的读控制*/
    memset( (s8*)m_stMdnsEventQueue.event , '\0' , sizeof(LXI_MDNS_EVENT_NODE_STRU) * LXI_MDNS_EVENT_QUEUE_MAX_SIZE );
    memset( (s8*)m_stMdnsEventQueue.data  , '\0' , sizeof(LXI_MDNS_BUFF_NODE_STRU)  * LXI_MDNS_EVENT_QUEUE_MAX_SIZE );
}

/*
 * mDNS 事件队列写操作
*/
void lxi_mdns_event_queue_write( LXI_MDNS_EVENT_EM event , s8 *ps8Buf , s32 s32Len )
{
    /*判断当前事件队列的写指针是否已指向队尾，如果是则初始化为队列的首部*/
    if( LXI_MDNS_EVENT_QUEUE_MAX_SIZE == m_stMdnsEventQueue.front )
    {
        m_stMdnsEventQueue.front = 0;
    }
    if( 1 == m_stMdnsEventQueue.event[m_stMdnsEventQueue.front].use )
    {
        /*如果当前事件节点目前没有被读取执行,则认为该事件还没有被处理,且事件队列已满，等待该事件被处理*/
        m_stMdnsEventQueue.full = 1;
        while( lxi_sem_wait( &m_stMdnsEventQueue.wr_en , 5000 ) != 0 );
    }
    /*记录事件，并更新事件队列的首地址*/
    m_stMdnsEventQueue.event[m_stMdnsEventQueue.front].code = event;
    m_stMdnsEventQueue.event[m_stMdnsEventQueue.front].use  = 1;
    /*如果当前的数据指针不为空,则复制数据*/
    if( NULL != ps8Buf )
    {
        memset( m_stMdnsEventQueue.data[m_stMdnsEventQueue.front].buff , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
        memcpy( m_stMdnsEventQueue.data[m_stMdnsEventQueue.front].buff , ps8Buf , s32Len );
        m_stMdnsEventQueue.data[m_stMdnsEventQueue.front].len = s32Len;
    }
    m_stMdnsEventQueue.front ++;
    /*抛送信号量,唤醒事件处理线程*/
    lxi_sem_post( &m_stMdnsEventQueue.rd_en );
}

/*
 * mDNS事件队列的读操作
 * 如果没有读取到数据,则返回-1，否则返回0
*/
LXI_MDNS_EVENT_EM lxi_mdns_event_queue_read( s8 *ps8Buf , s32 *ps32Len )
{
    static  LXI_MDNS_EVENT_EM   emEvent;

    /*等待信号量唤醒*/
    while( lxi_sem_wait( &m_stMdnsEventQueue.rd_en , 5000 ) != 0 );
    /*判定当前事件队列是否为空*/
    if( LXI_MDNS_EVENT_QUEUE_MAX_SIZE == m_stMdnsEventQueue.rear )
    {
        /*当前地址已到队列的尾部,将从队列头部从新开始*/
        m_stMdnsEventQueue.rear = 0;
    }
    /*如果当前节点的长度为0,则表示事件队列中没有数据*/
    if( 0 == m_stMdnsEventQueue.event[m_stMdnsEventQueue.rear].use )
    {
        /*等待信号量唤醒*/
        lxi_sem_wait( &m_stMdnsEventQueue.rd_en , 5000 );
    }
    /*开始取数据*/
    emEvent = m_stMdnsEventQueue.event[m_stMdnsEventQueue.rear].code;
    /*更新事件节点的使用标识，以便于事件队列的满空判定*/
    m_stMdnsEventQueue.event[m_stMdnsEventQueue.rear].use = 0;
    /*如果事件为报文解析,则复制数据*/
    if( LXI_MDNS_EVENT_PACKET_PARSE == emEvent )
    {
        memcpy( ps8Buf , m_stMdnsEventQueue.data[m_stMdnsEventQueue.rear].buff , m_stMdnsEventQueue.data[m_stMdnsEventQueue.rear].len );
        *ps32Len = m_stMdnsEventQueue.data[m_stMdnsEventQueue.rear].len;
    }
    /*更新事件队列的尾指针*/
    m_stMdnsEventQueue.rear ++;
    /*如果当前处于写阻止状态,释放写权限*/
    if( 1 == m_stMdnsEventQueue.full )
    {
        lxi_sem_post( &m_stMdnsEventQueue.wr_en );
        m_stMdnsEventQueue.full = 0;
    }
    return emEvent;
}

/*
 * mDNS状态检测定时器
 * 该定时器已500us为间隔,检测当前网络状态是否发生变化
 * mDNS探测线程根据网络状态调整报文的发送顺序
 * 其中网络状态指:网络配置,网络断开,主机名冲突,服务名冲突
 * 该函数返回0,则标志当前网络状态没有变化
 * 返回-1,标志当前网络状态发生了变化,需要根据当前网络状态,进行事件发送顺序的调整
 * 该函数的输入参数为500us的个数
*/
s32 lxi_mdns_network_state_monitor( u32 u32Times )
{
    u32 i;

    for( i = 0 ; i < u32Times || u32Times == LXI_MDNS_MONITOR_MAX_TIME ; i ++ )
    {
        usleep( LXI_MDNS_MONITOR_PERIOD );
        if( m_s8NetStateIsChange == true )
        {
            m_s8NetStateIsChange = false;
            m_emLastNetStatus = LXI_STATUS_UNLINK;
            return -1;
        }
        if( LXI_MDNS_STATE_HOST_CONFLICT    == m_emMdnsState ||
            LXI_MDNS_STATE_SERVICE_CONFLICT == m_emMdnsState)
        {
            return -1;
        }
    }

    return 0;
}

/*
 * 描    述: 将输入的主机名转化为符合DNS 要求的格式
 *输入参数:
 *          参数名         类型              描述
 *          ps8Src         s8_t *        待转换名字的地址
 *          ps8Dst         s8_t *        保存转换后名字的地址
 *输出参数:
 *返 回 值: 成功返回格式化长度，否则返回-1
 *说    明:
 *举    例:如rigol.local.cn转换为5rigol5local2cn，及每段字符前面加上该段字符长度
 */
s32 lxi_mdns_name_convert(s8 *ps8Src , s8 *ps8Dst)
{
    s32  count = 0;
    s32  len   = 0;
    s8  *src   = ps8Src;
    s8  *dst   = ps8Dst;
    s8  *mark  = ps8Dst;

    if(ps8Src == NULL ||
       ps8Dst == NULL ||
       strlen(ps8Src) == 0)
    {
        //参数不正确
        return -1;
    }

    if(*src != '.')//第一个不为" . "时预留空间
    {
        count ++;
        dst ++;
    }
    while(*src != '\0')
    {
        *dst = *src;
        dst ++;
        src ++;

        count ++;

        if(*src == '\0' || *src == '.')
        {
            *mark = count - 1;
            len += count;
            count = 0;
            mark = dst;
        }
    }
    *dst = '\0';
    len ++;
    return len;
}

/*
*函 数 名: lxi_mdns_packet_send
*描       述: mdns对外发送数据的接口
*输入参数:
*          参数名         类型                  描述
*          ps8Data        s8_t*           需要发送的数据地址
*          s32Len         s32_t           发送数据的长度
*输出参数:
*返 回 值:-1--发送失败,其他正数值均表示发送成功
*说    明:
*/
s32 lxi_mdns_packet_send(s8 *ps8Data , s32 s32Len)
{
    struct sockaddr_in raddr = {0};//远程的IP地址
    s32  len = sizeof(struct sockaddr_in);
    s32  err_t;
    /*修改目的地址*/
    raddr.sin_family = AF_INET;//IPV4
    raddr.sin_port   = htons(LXI_MDNS_SERVICE_PORT);//指定端口号5353
    raddr.sin_addr.s_addr = inet_addr(LXI_MDNS_SERVICE_ADDR);//根据标准,指定目的地址为：224.0.0.251

    //UDP-Socket已完成分配，则进行数据的发送
    err_t = sendto( m_s32MdnsSock , ps8Data , s32Len , 0 , (struct sockaddr *)&raddr,len);
    return err_t;
}

void lxi_mdns_host_packet( s8 *ps8Buf , DNS_HEADER_OPCODE_E emOpcode )
{
    LXI_MDNS_PACKET_HEADER *pstHeader = (LXI_MDNS_PACKET_HEADER *)ps8Buf;
    s32                    s32Length   = 0;

    /*当前有1个问题和1个授权*/
    pstHeader->id      = 0;
    pstHeader->flags   = 0;
    pstHeader->adcount = 0;
    pstHeader->ancount = 0;
    pstHeader->qucount = LXI_REV_WORD(1);
    pstHeader->aucount = LXI_REV_WORD(1);
    /*格式化主机名*/
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf( m_as8MdnsTempBuf , "%s.%s" , m_as8DHostName , "local" );
    s32Length = lxi_mdns_name_convert( m_as8MdnsTempBuf , pstHeader->buff );
    //配置问题类型
    pstHeader->buff[s32Length++] = 0x00;
    pstHeader->buff[s32Length++] = 0xff;
    //判断是QU还是QM，0为QM，1为QU
    if(emOpcode == OPCODE_QUERY)
    {
        pstHeader->buff[s32Length++] = 0x80;
    }
    else
    {
        pstHeader->buff[s32Length++] = 0x00;
    }
    pstHeader->buff[s32Length++] = 0x01;
    //配置属性
    pstHeader->buff[s32Length++] = 0xc0;
    pstHeader->buff[s32Length++] = 0x0c;
    //配置属性类型
    pstHeader->buff[s32Length++] = 0x00;
    pstHeader->buff[s32Length++] = 0x01;
    pstHeader->buff[s32Length++] = 0x00;
    pstHeader->buff[s32Length++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32Length++] = 0x00;
    pstHeader->buff[s32Length++] = 0x00;
    pstHeader->buff[s32Length++] = 0x00;
    pstHeader->buff[s32Length++] = 0x78;
    //数据长度, IP地址长度为4
    pstHeader->buff[s32Length++] = 0x00;
    pstHeader->buff[s32Length++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32Length],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //最终数据的长度为
    s32Length  = s32Length + LXI_MDNS_IP_ADDR_LEN + LXI_MDNS_PACKET_HEADER_SIZE;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32Length);
}

void lxi_mdns_host_declare(s8 *ps8Buf)
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    struct  in_addr          localAddr;

    //先初始化报文描述头，共1个问题1个描述
    pstHeader->id       = 0;
    pstHeader->flags    = LXI_REV_WORD(0x8400);
    pstHeader->qucount  = 0;
    pstHeader->ancount  = LXI_REV_WORD(2);
    pstHeader->aucount  = 0;
    pstHeader->adcount  = 0;

    //声明1
    //开始主机名的转换并添加后缀.local
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf( m_as8MdnsTempBuf , "%s.%s" , m_as8DHostName , "local" );
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    //配置问题类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //数据长度, IP地址长度为4
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //当前数据的长度为
    s32DataLen  = s32DataLen + LXI_MDNS_IP_ADDR_LEN;

    //声明2
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    //根据规则,原IP地址需要转成逆序，如172.16.3.8转成8.3.16.172;且在后面添加.in-addr.arpa
    localAddr.s_addr = htonl(m_stLxiConfPara.s32IpAddr);
    sprintf(m_as8MdnsTempBuf,"%s%s",inet_ntoa(localAddr) , ".in-addr.arpa" );
    //转换
    s32DataLen += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);

    //类型及标识
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //附加域名
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //域名
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    //计算发送数据的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;
    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_question(s8 *ps8Name,s8 *ps8Buf,u8 u8Type,u16  u16Port)
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    u16                      u16LocalIndex;

    //先初始化报文描述头，共1个问题1个描述
    pstHeader->id      = 0;
    pstHeader->flags   = 0;
    pstHeader->qucount = LXI_REV_WORD(1);
    pstHeader->aucount = LXI_REV_WORD(1);
    pstHeader->ancount = 0;
    pstHeader->adcount = 0;

    //获取服务名
    //在服务名后添加对应的服务项
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf( m_as8MdnsTempBuf,"%s.%s", m_as8DServiceName , ps8Name );
    //名称转换
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    //保存.local的地址
    u16LocalIndex = LXI_MDNS_PACKET_HEADER_SIZE + s32DataLen - strlen(".local") - 1;
    //配置问题类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0xff;
    //判断是QU还是QM，0为QM，1为QU
    if(u8Type == 1)
    {
        pstHeader->buff[s32DataLen++] = 0x80;
    }
    else
    {
        pstHeader->buff[s32DataLen++] = 0x00;
    }
    pstHeader->buff[s32DataLen++] = 0x01;

    //开始组织描述
    //服务名
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //类型及标识
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //名称转换
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert(m_as8DHostName,m_as8MdnsTempBuf);
    //添加数据长度字段，主机名描述字段长度+6个字节的数据描述头
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsTempBuf)+6+2;
    //添加优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加端口
    pstHeader->buff[s32DataLen++] = (u16Port&0xFF00)>>8;
    pstHeader->buff[s32DataLen++] = (u16Port&0xFF);
    //添加数据
    memcpy(&pstHeader->buff[s32DataLen],m_as8MdnsTempBuf,strlen(m_as8MdnsTempBuf));
    //计算数据长度
    s32DataLen += strlen(m_as8MdnsTempBuf);
    //添加,local后缀的压缩码制
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = u16LocalIndex;

    //更新发送数据的长度
     s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;
    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_http_with_host_declare( s8  *ps8Buf )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    struct in_addr           localAddr;

    //初始化报文描述头
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(3);
    pstHeader->adcount = 0;

    /*填充主机名声明*/
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf( m_as8MdnsTempBuf , "%s.%s" , m_as8DHostName , "local" );
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    //配置问题类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //配置Class
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //数据长度, IP地址长度为4
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //当前数据的长度为
    s32DataLen  +=  LXI_MDNS_IP_ADDR_LEN;

    //声明2
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );

    //根据规则,原IP地址需要转成逆序，如172.16.3.8转成8.3.16.172;且在后面添加.in-addr.arpa
    localAddr.s_addr = htonl(m_stLxiConfPara.s32IpAddr);
    sprintf(m_as8MdnsTempBuf,"%s%s",inet_ntoa(localAddr) , ".in-addr.arpa" );
    //转换
    s32DataLen += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);

    //类型及标识
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //附加域名
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //域名
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    //声明3
    //转换
    s32DataLen += lxi_mdns_name_convert("_services._dns-sd._udp.local",&pstHeader->buff[s32DataLen]);
    //类型及标识
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //附加域名
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0d;

    s32DataLen += lxi_mdns_name_convert("_http._tcp.local",&pstHeader->buff[s32DataLen]);

    //计算最终的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

    //发送
    lxi_mdns_packet_send( ps8Buf , s32DataLen );
}

void lxi_mdns_server_http_ptr( s8  *ps8Buf )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    //先初始化报文描述头，共1个问题1个描述
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(1);
    pstHeader->adcount = 0;

    //配置声明
    s32DataLen = lxi_mdns_name_convert("_http._tcp.local",pstHeader->buff);
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //转换服务名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert(m_as8DServiceName,m_as8MdnsTempBuf);

    //其中http服务名前面已经添加，这里只需要进行压缩即可，所以长度为服务名长度+2字节的压缩描述符
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsTempBuf)+2;
    memcpy(&pstHeader->buff[s32DataLen],m_as8MdnsTempBuf,strlen(m_as8MdnsTempBuf));
    //计算数据长度
    s32DataLen += strlen(m_as8MdnsTempBuf);
    //添加压缩描述符
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    //更新发送数据的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_http_txt_declare( s8 *ps8Buf )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    s32                      s32TempLen;
    u8                       u8LocalIndex;

    //先初始化报文描述头，共1个问题1个描述
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(3);
    pstHeader->adcount = 0;

    //配置声明
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s" , m_as8DServiceName , "_http._tcp.local");
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    // 计算local的位置,便于下面进行压缩
    u8LocalIndex = s32DataLen - 1 - strlen(".local") + LXI_MDNS_PACKET_HEADER_SIZE;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号,1个字节的主机名长度，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8DHostName) + 2 + 2 + 2 + 1 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号::80
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;

    //转换主机名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert(m_as8DHostName,m_as8MdnsTempBuf);
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);

    //  .local地址
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;

    //  声明2
    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT属性
    s32TempLen = strlen(m_as8MdnsHttpTxtInfo);
    pstHeader->buff[s32DataLen++] = ( s32TempLen >> 8 );
    pstHeader->buff[s32DataLen++] = ( s32TempLen & 0xFF );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsHttpTxtInfo , s32TempLen );
    s32DataLen += s32TempLen;

    //  声明3
    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE );

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //更新发送数据的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_http_txt_declare_with_ptr( s8 *ps8Buf  , u8 u8Additional )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    u8                       u8LocalIndex , u8HostIndex;

    //先初始化报文描述头，共4个声明1个附属
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(4);
    pstHeader->adcount = LXI_REV_WORD(u8Additional);

    //配置声明
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s" , m_as8DServiceName , "_http._tcp.local");
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    // 计算local的位置,便于下面进行压缩
    u8LocalIndex = s32DataLen - 1 - strlen(".local") + LXI_MDNS_PACKET_HEADER_SIZE;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号,1个字节的主机名长度，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8DHostName) + 2 + 2 + 2 + 1 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号::80
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;

    //转换主机名
    u8HostIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;   //  记录主机名,便于下面的压缩
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert(m_as8DHostName,m_as8MdnsTempBuf);
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);

    //  .local地址
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;

    //  声明2
    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT属性
    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = (u8)strlen( m_as8MdnsHttpTxtInfo );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsHttpTxtInfo , strlen(m_as8MdnsHttpTxtInfo) );
    s32DataLen += strlen(m_as8MdnsHttpTxtInfo);

    //  dns-sd服务声明
    //  服务名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert( "_services._dns-sd._udp" , m_as8MdnsTempBuf );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);
    //  添加local的压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE );

    //  声明4
    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE );

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //  附属信息
    if( u8Additional == 1 )
    {
        //  主机名
        pstHeader->buff[s32DataLen++] = 0xC0;
        pstHeader->buff[s32DataLen++] = u8HostIndex;
        //配置问题类型
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x01;
        //配置Class
        pstHeader->buff[s32DataLen++] = 0x80;
        pstHeader->buff[s32DataLen++] = 0x01;
        //生存时间120TTL
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x78;

        //数据长度, IP地址长度为4
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x04;
        //添加IP地址
        memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
        //更新发送数据的长度
        s32DataLen += (LXI_MDNS_PACKET_HEADER_SIZE + LXI_MDNS_IP_ADDR_LEN);
    }
    else
    {
        //更新发送数据的长度
        s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;
    }

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_http_all_declare( s8 *ps8Buf )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    u8                       u8LocalIndex , u8HttpIndex;
    struct in_addr           localAddr;

    //先初始化报文描述头，共4个声明1个附属
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(6);
    pstHeader->adcount = 0;

    //声明1
    //开始主机名的转换并添加后缀.local
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf( m_as8MdnsTempBuf , "%s.%s" , m_as8DHostName , "local" );
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    // 计算local的位置,便于下面进行压缩
    u8LocalIndex = s32DataLen - 1 - strlen(".local") + LXI_MDNS_PACKET_HEADER_SIZE;
    //配置问题类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //数据长度, IP地址长度为4
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //当前数据的长度为
    s32DataLen  = s32DataLen + LXI_MDNS_IP_ADDR_LEN;

    //声明2
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );

    //根据规则,原IP地址需要转成逆序，如172.16.3.8转成8.3.16.172;且在后面添加.in-addr.arpa
    localAddr.s_addr = htonl(m_stLxiConfPara.s32IpAddr);
    sprintf(m_as8MdnsTempBuf,"%s%s",inet_ntoa(localAddr) , ".in-addr.arpa" );
    //转换
    s32DataLen += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);

    //类型及标识
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //附加域名
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //域名
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    //http服务声明
    //  记录当前http服务名的位置,便于后面的压缩
    u8HttpIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s" , m_as8DServiceName , "_http._tcp");
    s32DataLen += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);
    //  添加local的压缩标志
    pstHeader->buff[s32DataLen-1] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 2 + 2 + 2 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号::80
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;
    //  主机名压缩地址
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //  声明4
    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HttpIndex;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT属性
    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = (u8)strlen( m_as8MdnsHttpTxtInfo );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsHttpTxtInfo , strlen(m_as8MdnsHttpTxtInfo) );
    s32DataLen += strlen(m_as8MdnsHttpTxtInfo);

    //  dns-sd服务声明
    //  服务名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert( "_services._dns-sd._udp" , m_as8MdnsTempBuf );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);
    //  添加local的压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + u8HttpIndex );

    //  声明6
    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + u8HttpIndex );

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HttpIndex;

    //更新发送数据的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_txt_declare( s8 *ps8ServerName , u16 u16port ,  s8 *ps8Buf , u8  u8DnsFlag )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    u8                       u8LocalIndex;

    //先初始化报文描述头，共4个声明1个附属
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD((3+u8DnsFlag));
    pstHeader->adcount = 0;

    //配置声明
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s" , m_as8DServiceName , ps8ServerName );
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    // 计算local的位置,便于下面进行压缩
    u8LocalIndex = s32DataLen - 1 - strlen(".local") + LXI_MDNS_PACKET_HEADER_SIZE;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号,1个字节的主机名长度，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8DHostName) + 2 + 2 + 2 + 1 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号
    pstHeader->buff[s32DataLen++] = (u8)(u16port >> 8);
    pstHeader->buff[s32DataLen++] = (u8)(u16port & 0xFF);

    //转换主机名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert(m_as8DHostName,m_as8MdnsTempBuf);
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);

    //  .local地址
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;

    //  声明2
    /*
     * TXT声明
     */
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT描述长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  填充数据
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    if( u8DnsFlag == 1)
    {
        //  dns-sd服务声明
        //  服务名
        memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
        lxi_mdns_name_convert( "_services._dns-sd._udp" , m_as8MdnsTempBuf );
        strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
        s32DataLen += strlen(m_as8MdnsTempBuf);
        //  添加local的压缩标志
        pstHeader->buff[s32DataLen++] = 0xC0;
        pstHeader->buff[s32DataLen++] = u8LocalIndex;
        //配置声明类型
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x0c;

        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x01;

        //生存时间4500TTL
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x11;
        pstHeader->buff[s32DataLen++] = 0x94;

        //  数据长度
        pstHeader->buff[s32DataLen++] = 0x00;
        pstHeader->buff[s32DataLen++] = 0x02;

        //  服务名
        pstHeader->buff[s32DataLen++] = 0xC0;
        pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE );
    }
    //  声明4
    //  服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE );

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //更新发送数据的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_lxi_txt_with_host_declare( s8 *ps8Buf )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    u8                       u8LocalIndex , u8ServerIndex , u8ModelIndex;
    struct in_addr           localAddr;

    //先初始化报文描述头，共4个声明1个附属
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(6);
    pstHeader->adcount = 0;

    //开始主机名的转换并添加后缀.local
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf( m_as8MdnsTempBuf , "%s.%s" , m_as8DHostName , "local" );
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    // 计算local的位置,便于下面进行压缩
    u8LocalIndex = LXI_MDNS_PACKET_HEADER_SIZE + strlen(m_as8DHostName) + 1;
    //配置问题类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //数据长度, IP地址长度为4
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //当前数据的长度为
    s32DataLen  = s32DataLen + LXI_MDNS_IP_ADDR_LEN;

    //声明2
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );

    //根据规则,原IP地址需要转成逆序，如172.16.3.8转成8.3.16.172;且在后面添加.in-addr.arpa
    localAddr.s_addr = htonl(m_stLxiConfPara.s32IpAddr);
    sprintf(m_as8MdnsTempBuf,"%s%s",inet_ntoa(localAddr) , ".in-addr.arpa" );
    //转换
    s32DataLen += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);

    //类型及标识
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //附加域名
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //域名
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    // 开始服务的声明
    //  记录服务名的位置
    u8ServerIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //  记录功能名的位置
    u8ModelIndex = u8ServerIndex + strlen( m_as8DServiceName ) + 1;
    //配置声明
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s" , m_as8DServiceName , "_lxi._tcp");
    s32DataLen += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);
    //  由于服务名的转换会在字符串结尾处多一个'\0'的结束标记，而后面需要添加".local"的压缩标志,所以
    //  长度s32DataLen值应减1
    s32DataLen -= 1;
    //  添加".local"的标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 2 + 2 + 2 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号::80
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;

    //  主机名压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //  声明4
    //  服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8ServerIndex;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT属性
    //  添加TXT描述长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  添加版本号
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    //  dns-sd服务声明
    //  服务名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert( "_services._dns-sd._udp" , m_as8MdnsTempBuf );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);
    //  添加local的压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8ModelIndex;

    //  声明4
    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8ModelIndex;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8ServerIndex;

    //更新发送数据的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_http_lxi_scpi_declare( s8 *ps8Buf )
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    u8                       u8LocalIndex , u8HostIndex , u8DnsSdIndex , u8LxiFlagsIndex , u8LxiServerIndex , u8TcpIndex;
    u16                      u16ScpiFlagsIndex , u16ScpiServerIndex;

    //先初始化报文描述头，共4个声明1个附属
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(12);
    pstHeader->adcount = LXI_REV_WORD(1);

    //配置声明
    /*
     * HTTP服务名的声明
    */
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s" , m_as8DServiceName , "_http._tcp.local");
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    // 计算local的位置,便于下面进行压缩
    u8LocalIndex = s32DataLen - 1 - strlen(".local") + LXI_MDNS_PACKET_HEADER_SIZE;
    //  计算"_tcp.local"的位置便于下面的压缩
    u8TcpIndex = s32DataLen - 1 - strlen("._tcp.local") + LXI_MDNS_PACKET_HEADER_SIZE;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号,1个字节的主机名长度，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8DHostName) + 2 + 2 + 2 + 1 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号::80
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;

    //转换主机名
    u8HostIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;   //  记录主机名,便于下面的压缩
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert(m_as8DHostName,m_as8MdnsTempBuf);
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);

    //  .local地址
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;

    /*
     * http-txt的声明
    */
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT属性
    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = (u8)strlen( m_as8MdnsHttpTxtInfo );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsHttpTxtInfo , strlen(m_as8MdnsHttpTxtInfo) );
    s32DataLen += strlen(m_as8MdnsHttpTxtInfo);

    /*
     * HTTP dns-sd服务声明
     */
    //  记录dns-sd服务名，便于后面的压缩
    u8DnsSdIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //  服务名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert( "_services._dns-sd._udp" , m_as8MdnsTempBuf );
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
    s32DataLen += strlen(m_as8MdnsTempBuf);
    //  添加local的压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE );

    /*
     * http PTR 声明
     */
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = (u8)(strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE );

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  http服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = 0x0C;

    /*
     * LXI服务名的声明
     */
    //  记录LXI服务名的位置,便于后面的压缩
    u8LxiServerIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    u8LxiFlagsIndex = s32DataLen + strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE ;

    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_lxi");

    s32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[s32DataLen] );

    //  受字符串转换函数的影响，当前长度减1
    s32DataLen -= 1;

    //  添加"_tcp.local"压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8TcpIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 2 + 2 + 2 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号::80
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;

    //  主机名压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HostIndex;

    /*
     * lxi-txt声明
    */

    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LxiServerIndex;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT属性
    //  添加TXT描述长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  添加版本号
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    /*
     * LXI DNS-SD服务声明
    */
    //  DNS-SD名字的压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8DnsSdIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  LXI服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LxiFlagsIndex;

    /*
     * LXI 服务名的PTR声明
    */
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LxiFlagsIndex;

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  LXI服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LxiServerIndex;

    /*
     * SCPI-RAW服务名的声明
     */
    //  记录SCPI-RAW服务名的位置,便于后面的压缩
    u16ScpiServerIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    u16ScpiFlagsIndex  = s32DataLen + strlen(m_as8DServiceName) + 1 + LXI_MDNS_PACKET_HEADER_SIZE ;

    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_scpi-raw");

    s32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[s32DataLen] );

    //  受字符串转换函数的影响，当前长度减1
    s32DataLen -= 1;

    //  添加"_tcp.local"压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8TcpIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    // 主机名描述的长度,2个字节的优先级,2个字节的权重,2个字节的端口号，2个压缩标志
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 2 + 2 + 2 + 2;

    //  优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;

    //  端口号
    pstHeader->buff[s32DataLen++] = (u8)(m_u32SocketPort >> 8);
    pstHeader->buff[s32DataLen++] = (u8)(m_u32SocketPort & 0x00FF);

    //  主机名压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HostIndex;

    /*
     * scpi-raw-txt声明
    */

    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)(u16ScpiServerIndex >> 8));
    pstHeader->buff[s32DataLen++] = (u8)( u16ScpiServerIndex & 0xFF );

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT描述长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  填充数据
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    /*
     * scpi-raw DNS-SD服务声明
    */
    //  DNS-SD名字的压缩标志
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8DnsSdIndex;
    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  SCPI-RAW服务名
    pstHeader->buff[s32DataLen++] = (0xC0 | (u8)(u16ScpiFlagsIndex >> 8));
    pstHeader->buff[s32DataLen++] = (u8)(u16ScpiFlagsIndex & 0xFF);

    /*
     * SCPI-RAW 服务名的PTR声明
    */
    pstHeader->buff[s32DataLen++] = (0xC0 | (u8)(u16ScpiFlagsIndex >> 8));
    pstHeader->buff[s32DataLen++] = (u8)(u16ScpiFlagsIndex & 0xFF);

    //配置声明类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;

    //  scpi-raw服务名
    pstHeader->buff[s32DataLen++] = (0xC0 | (u8)(u16ScpiServerIndex >> 8));
    pstHeader->buff[s32DataLen++] = (u8)(u16ScpiServerIndex & 0xFF);

    //  附属信息
    //  主机名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HostIndex;
    //配置问题类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //配置Class
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //数据长度, IP地址长度为4
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //更新发送数据的长度
    s32DataLen += (LXI_MDNS_PACKET_HEADER_SIZE + LXI_MDNS_IP_ADDR_LEN);

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_server_scpi_with_vxi_declare(s8 *ps8Buf)
{
    LXI_MDNS_PACKET_HEADER  *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    s32                      s32DataLen;
    u16                      u16VxiNameIndex, u16VxiFlagsIndex ;
    u16                      u16LocalIndex,u16TcpIndex,u16DNSSDIndex,u16HostNameIndex,u16ScpiNameIndex;

    //先初始化报文描述头，共8个声明,1个附属信息
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(8);
    pstHeader->adcount = LXI_REV_WORD(1);

    //获取服务名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_scpi-raw._tcp.local");

    /*
     * 声明1,服务名scpi-raw声明
    */
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    //  记录scpi服务名的标记
    u16ScpiNameIndex = LXI_MDNS_PACKET_HEADER_SIZE + 1 + strlen( m_as8DServiceName );
    //记录.local的位置:相对于报文起始位置首先偏移12字节的报文头，再偏移.local之前的字符长度
    u16LocalIndex = LXI_MDNS_PACKET_HEADER_SIZE + s32DataLen - strlen(".local")-1;
    //记录._tcp.local的位置:相对于报文起始位置首先偏移12字节的报文头，再偏移._tcp.local之前的字符长度
    u16TcpIndex   = LXI_MDNS_PACKET_HEADER_SIZE + s32DataLen - strlen("._tcp.local")-1;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //转换主机名
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert(m_as8DHostName,m_as8MdnsTempBuf);

    //数据长度, 6个字节的描述符+主机名长度+2个字节的压缩描述符
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 6 + strlen(m_as8MdnsTempBuf)+2;
    //优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //端口号
    pstHeader->buff[s32DataLen++] = (u8)(m_u32SocketPort >> 8 );
    pstHeader->buff[s32DataLen++] = (u8)(m_u32SocketPort & 0xFF);
    //添加数据
    memcpy( &pstHeader->buff[s32DataLen],m_as8MdnsTempBuf,strlen(m_as8MdnsTempBuf));

    //记录主机名
    u16HostNameIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //更新数据长度
    s32DataLen += strlen(m_as8MdnsTempBuf);
    //添加".local"的压缩符
    pstHeader->buff[s32DataLen++] = ( 0xc000 | ( u16LocalIndex & 0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16LocalIndex & 0xFF;

    //声明2:scpi-raw:text声明
    //由于服务名已经完成组织，则采用压缩
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT属性
    //  添加TXT描述长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  填充数据
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    //声明3:dns-sd声明
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert("_services._dns-sd._udp",m_as8MdnsTempBuf);
    memcpy(&pstHeader->buff[s32DataLen],m_as8MdnsTempBuf,strlen(m_as8MdnsTempBuf));

    //记录dns-sd位置
    u16DNSSDIndex = s32DataLen+LXI_MDNS_PACKET_HEADER_SIZE;
    //更新数据长度
    s32DataLen   += strlen(m_as8MdnsTempBuf);
    //添加".local"的压缩符
    pstHeader->buff[s32DataLen++] = (0xc000|(u16LocalIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16LocalIndex&0xFF;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度，因为"_scpi-raw._tcp.local"以及组织了，所以采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = u16ScpiNameIndex;

    //声明4:绑定scpi-raw服务
    //服务名
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = u16ScpiNameIndex;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度，因为"_scpi-raw._tcp.local"以及组织了，所以采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    //声明5:vxi服务声明
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_vxi-11");
    //  记录vxi服务名的位置
    u16VxiNameIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //  记录_vxi._tcp.local的位置,便于后面的压缩填充
    u16VxiFlagsIndex = u16VxiNameIndex + strlen( m_as8DServiceName ) + 1;
    //更新数据长度
    s32DataLen  += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);
    s32DataLen  --;
    //添加"._tcp.local"的压缩符
    pstHeader->buff[s32DataLen++] = (0xc000|(u16TcpIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16TcpIndex&0xFF;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //由于主机名已经组织了，所以采用压缩符
    //长度,6个字节的数据描述符+2个字节的压缩符
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x08;
    //添加优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加端口:111
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x6f;
    //数据
    pstHeader->buff[s32DataLen++] = (0xc000|(u16HostNameIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16HostNameIndex&0xFF;

    //声明6:vxi-text声明
    //由于服务名已经完成组织，这里采用压缩
    pstHeader->buff[s32DataLen++] = (0xc000|(u16VxiNameIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16VxiNameIndex&0xFF;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;

    //  添加TXT描述长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  填充数据
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    //声明7:dns-sd声明
    //由于dns-sd已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = (0xc000|(u16DNSSDIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16DNSSDIndex&0xFF;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_vxi._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = (0xc000|(u16VxiFlagsIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16VxiFlagsIndex&0xFF;

    //声明8:vxi的绑定
    //数据长度,由于"_vxi._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = (0xc000|(u16VxiFlagsIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16VxiFlagsIndex&0xFF;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_vxi._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = (0xc000|(u16VxiNameIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16VxiNameIndex&0xFF;

    //附属信息
    //由于主机名已组织，采用压缩
    pstHeader->buff[s32DataLen++] = (0xc000|(u16HostNameIndex&0xFF00))>>8;
    pstHeader->buff[s32DataLen++] = u16HostNameIndex&0xFF;

    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //数据长度, IP地址长度为4
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //更新发送数据的长度
    s32DataLen =s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE + LXI_MDNS_IP_ADDR_LEN ;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);

}

void lxi_mdns_server_timer_declare(s8 *ps8Buf)
{
    LXI_MDNS_PACKET_HEADER     *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    u8                          u8LocalIndex , u8HttpIndex , u8HttpServerIndex , u8TcpIndex ;
    u16                         u16LxiServerIndex , u16LxiIndex , u16ScpiServerIndex , u16ScpiIndex;
    u16                         u16VxiServerIndex , u16VxiIndex , u16DNSSDIndex;
    struct  in_addr             localAddr;
    s32                         s32DataLen;

    //先初始化报文描述头，共22个声明
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = LXI_REV_WORD(18);
    pstHeader->adcount = 0;

    /*
     * 声明1:主机名的声明
    */
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DHostName,"local");
    //声明1,主机名声明
    s32DataLen = lxi_mdns_name_convert(m_as8MdnsTempBuf,pstHeader->buff);
    //记录.local的位置:相对于报文起始位置首先偏移12字节的报文头，再偏移.local之前的字符长度
    u8LocalIndex = LXI_MDNS_PACKET_HEADER_SIZE + strlen( m_as8DHostName ) + 1;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //数据长度, IP地址长度为4
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x04;
    //添加IP地址
    memcpy( &pstHeader->buff[s32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
    //更新数据长度
    s32DataLen += LXI_MDNS_IP_ADDR_LEN;

    /*
     * 声明2:主机名绑定
     */
    //根据规则,原IP地址需要转成逆序，如172.16.3.8转成8.3.16.172;且在后面添加.in-addr.arpa
    localAddr.s_addr = htonl(m_stLxiConfPara.s32IpAddr);
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s%s",inet_ntoa(localAddr) , ".in-addr.arpa" );
    //转换
    s32DataLen += lxi_mdns_name_convert(m_as8MdnsTempBuf,&pstHeader->buff[s32DataLen]);

    //类型及标识
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;

    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;

    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;

    //附加域名
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //域名
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    /*
     * 声明3:http服务名声明
     */
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_http._tcp");
    //记录http服务名的位置，便于数据压缩
    u8HttpServerIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //  记录"_http._tcp.local"的位置便于数据压缩填充
    u8HttpIndex = u8HttpServerIndex + strlen( m_as8DServiceName ) + 1;
    //记录._tcp.local的位置:相对于服务名偏移._tcp.local之前的字符长度
    u8TcpIndex  = u8HttpIndex + strlen("._http");
    //更新数据长度
    s32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[s32DataLen] );
    s32DataLen -= 1;
    //添加".local"的压缩符
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //数据长度，主机名进行压缩，只有2个字节，再加6个字节的数据描述头，共8个字节
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x08;
    //优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //端口:80
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;
    //数据
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    /*
     * 声明4:http-txt声明
     */
    //由于http服务名已经完成组织，这里进行压缩即可
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HttpServerIndex;

    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsHttpTxtInfo);
    strncpy(&pstHeader->buff[s32DataLen] , m_as8MdnsHttpTxtInfo , strlen(m_as8MdnsHttpTxtInfo) );
    //更新数据长度
    s32DataLen += strlen(m_as8MdnsHttpTxtInfo);

    /*
     * 声明5:dns-sd声明
     */
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert("_services._dns-sd._udp",m_as8MdnsTempBuf);
    //记录dns-sd位置
    u16DNSSDIndex = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //  填充数据
    strncpy(&pstHeader->buff[s32DataLen],m_as8MdnsTempBuf,strlen(m_as8MdnsTempBuf));

    //更新数据长度
    s32DataLen   += strlen(m_as8MdnsTempBuf);
    //添加".local"的压缩符
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8LocalIndex;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度，因为"_http._tcp.local"以及组织了，所以采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HttpIndex;

    /*
     * 声明6:绑定http服务
     */
    //服务名
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HttpIndex;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度，因为"_http._tcp.local"以及组织了，所以采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8HttpServerIndex;

    /*
     * 声明7:lxi服务声明
     */
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_lxi");
    //  记录lxi服务名的位置
    u16LxiServerIndex  = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //  记录"._lxi._tcp.local"的位置
    u16LxiIndex = u16LxiServerIndex + 1 + strlen( m_as8DServiceName );
    //更新数据长度
    s32DataLen  += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[s32DataLen] );
    s32DataLen  -=1;
    //添加"._tcp.local"的压缩符
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8TcpIndex;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //由于主机名已经组织了，所以采用压缩符
    //长度,6个字节的数据描述符+2个字节的压缩符
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x08;
    //添加优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加端口
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x50;
    //数据
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    /*
     * 声明8:lxi-text声明
     */
    //由于服务名已经完成组织，这里采用压缩
    pstHeader->buff[s32DataLen++] = (0xC0 | (u8)(u16LxiServerIndex>>8));
    pstHeader->buff[s32DataLen++] = (u8)(u16LxiServerIndex & 0xFF );
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //  添加TXT属性
    //  添加TXT描述长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  填充数据
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    /*
     * 声明9:dns-sd声明
     */
    //由于dns-sd已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16DNSSDIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16DNSSDIndex & 0xFF );

    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_lxi._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16LxiIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16LxiIndex & 0xFF );

    /*
     * 声明10:lxi的绑定
     */
    //数据长度,由于"_lxi._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16LxiIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16LxiIndex & 0xFF );
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_lxi._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16LxiServerIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16LxiServerIndex & 0xFF );

    /*
     * 声明11:scpi-raw服务声明
     */
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf(m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_scpi-raw");
    //记录scpi-raw服务名的位置
    u16ScpiServerIndex  = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
    //  记录"_scpi-raw._tcp.local"的位置
    u16ScpiIndex = u16ScpiServerIndex + strlen( m_as8DServiceName ) + 1;
    //更新数据长度
    s32DataLen  += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[s32DataLen] );
    s32DataLen  -= 1;
    //添加"._tcp.local"的压缩符
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8TcpIndex;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //由于主机名已经组织了，所以采用压缩符
    //长度,6个字节的数据描述符+2个字节的压缩符
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x08;
    //添加优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加端口
    pstHeader->buff[s32DataLen++] = (u8)( m_u32SocketPort >> 8 );
    pstHeader->buff[s32DataLen++] = (u8)( m_u32SocketPort & 0xFF);
    //数据
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    //声明12:scpi-raw-text声明
    //由于服务名已经完成组织，这里采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16ScpiServerIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16ScpiServerIndex & 0xFF );
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  填充数据
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    /*
     * 声明13:dns-sd声明
     */
    //由于dns-sd已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16DNSSDIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16DNSSDIndex & 0xFF );

    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_scpi-raw._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16ScpiIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16ScpiIndex & 0xFF );

    /*
     * 声明14:scpi的绑定
     */
    //数据长度,由于"_scpi-raw._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16ScpiIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16ScpiIndex & 0xFF );
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_scpi-raw._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16ScpiServerIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16ScpiServerIndex & 0xFF );

    /*
     * 声明15:vxi-11服务声明
     */
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    sprintf( m_as8MdnsTempBuf,"%s.%s",m_as8DServiceName,"_vxi-11");
    //记录vxi-11服务名的位置
    u16VxiServerIndex  = s32DataLen + LXI_MDNS_PACKET_HEADER_SIZE ;
    //  记录"_vxi-11._tcp.local"的位置
    u16VxiIndex = u16VxiServerIndex + strlen(m_as8DServiceName) + 1;
    //更新数据长度
    s32DataLen  += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[s32DataLen] );
    s32DataLen  -= 1;
    //添加"._tcp.local"的压缩符
    pstHeader->buff[s32DataLen++] = 0xC0;
    pstHeader->buff[s32DataLen++] = u8TcpIndex;
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x21;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间120TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x78;
    //由于主机名已经组织了，所以采用压缩符
    //长度,6个字节的数据描述符+2个字节的压缩符
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x08;
    //添加优先级
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加权重
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    //添加端口::111
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x6f;
    //数据
    pstHeader->buff[s32DataLen++] = 0xc0;
    pstHeader->buff[s32DataLen++] = 0x0c;

    /*
     * 声明16:vxi-text声明
     */
    //由于服务名已经完成组织，这里采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16VxiServerIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16VxiServerIndex & 0xFF );
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x10;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x80;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = strlen(m_as8MdnsLxiTxtInfo);
    //  填充数据
    strncpy( &pstHeader->buff[s32DataLen] , m_as8MdnsLxiTxtInfo , strlen(m_as8MdnsLxiTxtInfo) );
    //  更新长度
    s32DataLen += strlen(m_as8MdnsLxiTxtInfo);

    /*
     * 声明17:dns-sd声明
     */
    //由于dns-sd已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16DNSSDIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16DNSSDIndex & 0xFF );

    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_vxi-11._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16VxiIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16VxiIndex & 0xFF );

    //声明18:vxi-11的绑定
    //数据长度,由于"_vxi-11._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16VxiIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16VxiIndex & 0xFF );
    //类型
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x0c;
    //所属类
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x01;
    //生存时间4500TTL
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x11;
    pstHeader->buff[s32DataLen++] = 0x94;
    //数据长度,由于"_vxi-11._tcp.local"已经完成组织，采用压缩
    pstHeader->buff[s32DataLen++] = 0x00;
    pstHeader->buff[s32DataLen++] = 0x02;
    //数据
    pstHeader->buff[s32DataLen++] = ( 0xC0 | (u8)( u16VxiServerIndex >> 8 ) );
    pstHeader->buff[s32DataLen++] = (u8)( u16VxiServerIndex & 0xFF );

    //更新发送数据的长度
    s32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

    //发送数据
    lxi_mdns_packet_send(ps8Buf,s32DataLen);
}

void lxi_mdns_query_process( s8 *ps8Buf )
{
    LXI_MDNS_PACKET_HEADER     *pstHeader = (LXI_MDNS_PACKET_HEADER*)ps8Buf;
    u32                         i;
    s16                         s16LocalIndex = -1 , s16HostIndex = -1 , s16TcpIndex = -1 ;
    s16                         s16HttpServerIndex = -1 , s16HttpIndex = -1;
    s16                         s16LxiServerIndex = -1 , s16LxiIndex = -1;
    s16                         s16ScpiServerIndex = -1 , s16ScpiIndex = -1;
    s16                         s16VxiServerIndex = -1 , s16VxiIndex = -1;
    s16                        *ps16ServerIndex , *ps16ModexIndex ;
    u32                         u32DataLen = 0;
    u16                         u16Port ;
    s8                         *ps8TxtInfo;
    s8                          as8ClassName[10];

    //先初始化报文描述头
    pstHeader->id      = 0;
    pstHeader->flags   = LXI_REV_WORD(0x8400);
    pstHeader->qucount = 0;
    pstHeader->aucount = 0;
    pstHeader->ancount = 0;
    pstHeader->adcount = 0;

    /*轮询当前报文的查询类型*/
    for( i = 0 ; i < m_u32MdnsCueeQueryNum ; i ++ )
    {
        switch( m_au16MdnsQuestType[i] )
        {
            case LXI_MDNS_QUERY_HOST_ADDR:  //  主机名对应的IP地址查询
                //  声明数加 1
                pstHeader->ancount ++;

                if( -1 ==  s16LocalIndex )
                {
                    //".local"标识未出现,则以主机名+".local"进行数据组织,并记录主机名地址和".local"字段的地址
                    //  因为主机名后面必有".local"字符
                    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
                    sprintf( m_as8MdnsTempBuf , "%s%s" , m_as8DHostName , ".local" );
                    //  记录主机名的位置
                    s16HostIndex = LXI_MDNS_PACKET_HEADER_SIZE + u32DataLen ;
                    //  记录".local"的位置
                    s16LocalIndex = s16HostIndex + strlen( m_as8DHostName ) + 1;

                    //  填充主机名
                    u32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[u32DataLen] );
                }
                else
                {
                    if( -1 == s16HostIndex )    //  主机名还没有填充
                    {
                        //  格式化主机名
                        memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
                        lxi_mdns_name_convert( m_as8DHostName , m_as8MdnsTempBuf );
                        //  记录当前主机名的位置
                        s16HostIndex = LXI_MDNS_PACKET_HEADER_SIZE + u32DataLen;
                        //  填充主机名
                        strncpy( &pstHeader->buff[u32DataLen] , m_as8MdnsTempBuf , strlen(m_as8MdnsTempBuf) );
                        //  更新长度
                        u32DataLen += strlen(m_as8MdnsTempBuf);
                        //  添加".local"的位置
                        pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16LocalIndex >> 8 ) ) );
                        pstHeader->buff[u32DataLen++] = (u8)( s16LocalIndex & 0xFF );
                    }
                    else
                    {
                        //  主机名已经填充
                        pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16HostIndex >> 8 ) ) );
                        pstHeader->buff[u32DataLen++] = (u8)( s16HostIndex & 0xFF );
                    }
                }
                //  填充该声明的类型以及所述类
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x01;
                pstHeader->buff[u32DataLen++] = 0x80;
                pstHeader->buff[u32DataLen++] = 0x01;
                //  标识该声明的生存时间,120TTL
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x78;
                //  标识IP长度的字段,4个字节
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x04;
                //  添加IP地址
                memcpy(&pstHeader->buff[u32DataLen],&m_stLxiConfPara.s32IpAddr ,LXI_MDNS_IP_ADDR_LEN);
                //更新数据长度
                u32DataLen += LXI_MDNS_IP_ADDR_LEN;
                break;
            case LXI_MDNS_QUERY_PTR:
                //  根据当前的子服务的类型,获取当前的TXT属性和端口号
                memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
                if( strncmp( &m_as8MdnsQuestions[i][1] , "_http" , strlen("_http") ) == 0 )
                {
                    ps8TxtInfo = m_as8MdnsHttpTxtInfo;
                    u16Port = 80;
                    strncpy( m_as8MdnsTempBuf , "_http" , strlen("_http") );
                    ps16ServerIndex = &s16HttpServerIndex;
                    ps16ModexIndex  = &s16HttpIndex;
                }
                else if( strncmp( &m_as8MdnsQuestions[i][1] , "_lxi" , strlen("_lxi") ) == 0 )
                {
                    ps8TxtInfo = m_as8MdnsLxiTxtInfo;
                    //  端口号
                    u16Port = 80;
                    strncpy( m_as8MdnsTempBuf , "_lxi" , strlen("_lxi") );
                    ps16ServerIndex = &s16LxiServerIndex;
                    ps16ModexIndex  = &s16LxiIndex;
                }
                else if( strncmp( &m_as8MdnsQuestions[i][1] , "_scpi-raw" , strlen("_scpi-raw") ) == 0)
                {
                    ps8TxtInfo = m_as8MdnsLxiTxtInfo;
                    //  端口号
                    u16Port = m_u32SocketPort;
                    strncpy( m_as8MdnsTempBuf , "_scpi-raw" , strlen("_scpi-raw") );
                    ps16ServerIndex = &s16ScpiServerIndex;
                    ps16ModexIndex  = &s16ScpiIndex;
                }
                else if( strncmp( &m_as8MdnsQuestions[i][1] , "_vxi-11" , strlen("_vxi-11") ) == 0)
                {
                    ps8TxtInfo = m_as8MdnsLxiTxtInfo;
                    //  端口号
                    u16Port = 111;
                    strncpy( m_as8MdnsTempBuf , "_vxi-11" , strlen("_vxi-11") );
                    ps16ServerIndex = &s16VxiServerIndex;
                    ps16ModexIndex  = &s16VxiIndex;
                }
                else
                {
                    break;
                }
                //  组织数据
                //  声明
                if( -1 == s16LocalIndex )   //  目前".local"没有填充,也就是说"._tcp"也没有填充
                {
                    //  记录当前功能的索引
                    *ps16ModexIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                    //  记录"._tcp.local"的位置
                    s16TcpIndex = *ps16ModexIndex + strlen(m_as8MdnsTempBuf) + 1;
                    //  记录".local"的位置
                    s16LocalIndex = s16TcpIndex + strlen("._tcp");
                    //  添加后缀
                    strcat( m_as8MdnsTempBuf , "._tcp.local" );
                    //  填充报文
                    u32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[u32DataLen] );
                }
                else
                {
                    //  当前存在".local"的索引
                    if( -1 == s16TcpIndex )
                    {
                        //  当前不存在"._tcp"的索引

                        //  记录当前功能的索引
                        *ps16ModexIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                        //  记录"._tcp.local"的位置
                        s16TcpIndex = *ps16ModexIndex + strlen(m_as8MdnsTempBuf) + 1;

                        //  添加后缀
                        strcat( m_as8MdnsTempBuf , "._tcp" );
                        //  填充报文
                        u32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[u32DataLen] );
                        //  后面添加压缩标志,长度减1
                        u32DataLen -= 1;
                        //  添加".local"的位置
                        pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16LocalIndex >> 8 ) ) );;
                        pstHeader->buff[u32DataLen++] = (u8)( s16LocalIndex & 0xFF );
                    }
                    else
                    {
                        //  当前存在"._tcp.local"标志
                        if( -1 == *ps16ModexIndex )
                        {
                            //  当前功能不存在

                            //  记录当前功能的索引
                            *ps16ModexIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                            //  填充报文
                            u32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[u32DataLen] );
                            //  后面添加压缩标志,长度减1
                            u32DataLen -= 1;
                            //  添加"_tcp.local"的位置
                            pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16TcpIndex >> 8 ) ) );;
                            pstHeader->buff[u32DataLen++] = (u8)( s16TcpIndex & 0xFF );
                        }
                        else
                        {
                            //  当前功能存在
                            //  添加"_model._tcp.local"的位置
                            pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( *ps16ModexIndex >> 8 ) ) );
                            pstHeader->buff[u32DataLen++] = (u8)( *ps16ModexIndex & 0xFF );
                        }
                    }
                }
                //开始配置属性和所属类
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x0c;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x01;
                //标识该声明的生存时间,4500TTL
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x11;
                pstHeader->buff[u32DataLen++] = 0x94;
                //  填充该功能的服务名
                if( -1 == *ps16ServerIndex )
                {
                    //  当前功能的服务名没有填充

                    //  记录服务名
                    *ps16ServerIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE + 2;
                    //  填充字符的长度
                    pstHeader->buff[u32DataLen++] = 0x00;
                    pstHeader->buff[u32DataLen++] = strlen(m_as8DServiceName) + 1 + 2 ;
                    //  填充服务名
                    u32DataLen += lxi_mdns_name_convert( m_as8DServiceName , &pstHeader->buff[u32DataLen] );
                    //  受转换函数的影响,此处应把"\0"的结束符的位置填充压缩标记
                    u32DataLen -= 1;
                    //  填写压缩标志
                    pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( *ps16ModexIndex >> 8 ) ) );;
                    pstHeader->buff[u32DataLen++] = (u8)( *ps16ModexIndex & 0xFF );
                }
                else
                {
                    //  填充字符的长度
                    pstHeader->buff[u32DataLen++] = 0x00;
                    pstHeader->buff[u32DataLen++] = 0x02;
                    //  该服务已经填充
                    pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( *ps16ServerIndex >> 8 ) ) );;
                    pstHeader->buff[u32DataLen++] = (u8)( *ps16ServerIndex & 0xFF );
                }
                /*
                 * 附属声明信息1:主机名声明
                */
                if( -1 == s16HostIndex )
                {
                    //  主机名没有填充
                    //  记录主机名的地址
                    s16HostIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                    //  填充主机名
                    u32DataLen += lxi_mdns_name_convert( m_as8DHostName , &pstHeader->buff[u32DataLen]);
                    //  受转换函数的影响,此处应把"\0"的结束符的位置填充压缩标记
                    u32DataLen -= 1;
                    //  填写压缩标志
                    pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16LocalIndex >> 8 ) ) );
                    pstHeader->buff[u32DataLen++] = (u8)( s16LocalIndex & 0xFF );
                }
                else
                {
                    //  主机名已填充
                    pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16HostIndex >> 8 ) ) );
                    pstHeader->buff[u32DataLen++] = (u8)( s16HostIndex & 0xFF );
                }
                //  填充该声明的类型以及所述类
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x01;
                pstHeader->buff[u32DataLen++] = 0x80;
                pstHeader->buff[u32DataLen++] = 0x01;
                //  标识该声明的生存时间,120TTL
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x78;
                //  标识IP长度的字段,4个字节
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x04;
                //  添加IP地址
                memcpy(&pstHeader->buff[u32DataLen],&m_stLxiConfPara.s32IpAddr ,LXI_MDNS_IP_ADDR_LEN);
                //更新数据长度
                u32DataLen += LXI_MDNS_IP_ADDR_LEN;
                /*
                 * 附属声明信息2：服务名绑定到主机名
                */
                pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( *ps16ServerIndex >> 8 ) ) );
                pstHeader->buff[u32DataLen++] = (u8)( *ps16ServerIndex & 0xFF );
                //标识该声明的类型以及所述类
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x21;
                pstHeader->buff[u32DataLen++] = 0x80;
                pstHeader->buff[u32DataLen++] = 0x01;
                //标识该声明的生存时间,120TTL
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x78;
                //标识信息长度:6个字节的属性描述+2个字节的主机名压缩标识
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x08;
                //优先级
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                //权重
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                //端口号
                pstHeader->buff[u32DataLen++] = (u8)( u16Port >> 8   );
                pstHeader->buff[u32DataLen++] = (u8)( u16Port & 0xFF );
                //主机名
                pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16HostIndex >> 8 ) ) );
                pstHeader->buff[u32DataLen++] = (u8)( s16HostIndex & 0xFF );
                /*
                 * 附属信息3：TXT属性的声明
                */
                pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( *ps16ServerIndex >> 8 ) ) );
                pstHeader->buff[u32DataLen++] = (u8)( *ps16ServerIndex & 0xFF );
                //标识该声明的类型以及所述类
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x10;
                pstHeader->buff[u32DataLen++] = 0x80;
                pstHeader->buff[u32DataLen++] = 0x01;
                //标识该声明的生存时间,4500TTL
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x11;
                pstHeader->buff[u32DataLen++] = 0x94;
                //标识信息长度:6个字节的属性描述+2个字节的主机名压缩标识
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = strlen(ps8TxtInfo);
                //数据填充
                memcpy(&pstHeader->buff[u32DataLen],ps8TxtInfo,strlen(ps8TxtInfo));
                //更新数据长度
                u32DataLen += strlen(ps8TxtInfo);

                //  更新报文头
                pstHeader->ancount ++;
                pstHeader->adcount += 3;
                break;
            case LXI_MDNS_QUERY_SRV:

                //  服务名长度是否一致
                if( (s8)strlen(m_as8DServiceName) == m_as8MdnsQuestions[i][0] )
                {
                    //  判定服务名是否一致
                    if( strncmp( m_as8DServiceName , &m_as8MdnsQuestions[i][1] , strlen(m_as8DServiceName) ) == 0 )
                    {
                        //  功能判定
                        //  根据当前的子服务的类型,获取当前的端口号
                        memset( as8ClassName , '\0' , 10 );

                        if( strncmp( &m_as8MdnsQuestions[i][1+strlen(m_as8DServiceName)+1] , "_http" , strlen("_http") ) == 0 )
                        {
                            u16Port = 80;
                            strncpy( as8ClassName , "_http" , strlen("_http") );
                            ps16ServerIndex = &s16HttpServerIndex;
                            ps16ModexIndex  = &s16HttpIndex;
                        }
                        else if( strncmp( &m_as8MdnsQuestions[i][1+strlen(m_as8DServiceName)+1] , "_lxi" , strlen("_lxi") ) == 0 )
                        {
                            //  端口号
                            u16Port = 80;
                            strncpy( as8ClassName , "_lxi" , strlen("_lxi") );
                            ps16ServerIndex = &s16LxiServerIndex;
                            ps16ModexIndex  = &s16LxiIndex;
                        }
                        else if( strncmp( &m_as8MdnsQuestions[i][1+strlen(m_as8DServiceName)+1] , "_scpi-raw" , strlen("_scpi-raw") ) == 0)
                        {
                            //  端口号
                            u16Port = m_u32SocketPort;
                            strncpy( as8ClassName , "_scpi-raw" , strlen("_scpi-raw") );
                            ps16ServerIndex = &s16ScpiServerIndex;
                            ps16ModexIndex  = &s16ScpiIndex;
                        }
                        else if( strncmp( &m_as8MdnsQuestions[i][1+strlen(m_as8DServiceName)+1] , "_vxi-11" , strlen("_vxi-11") ) == 0)
                        {
                            //  端口号
                            u16Port = 111;
                            strncpy( as8ClassName , "_vxi-11" , strlen("_vxi-11") );
                            ps16ServerIndex = &s16VxiServerIndex;
                            ps16ModexIndex  = &s16VxiIndex;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
                /*
                 * 组织服务声明
                */
                if( -1 == s16LocalIndex )
                {
                    //  当前数据报文没有填充".local"，则认为当前为空包
                    //  记录当前服务名的位置
                    *ps16ServerIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                    //  记录当前功能的索引
                    *ps16ModexIndex = *ps16ServerIndex + strlen( m_as8DServiceName ) + 1;
                    //  记录"._tcp.local"的位置
                    s16TcpIndex = *ps16ModexIndex + strlen(m_as8MdnsTempBuf) + 1;
                    //  记录".local"的位置
                    s16LocalIndex = s16TcpIndex + strlen("._tcp");

                    //  格式化服务名
                    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
                    sprintf( m_as8MdnsTempBuf , "%s.%s.%s" , m_as8DServiceName , as8ClassName , "_tcp.local" );
                    //  填充报文
                    u32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[u32DataLen] );
                }
                else
                {
                    //  存在".local"标志
                    if( -1 == s16TcpIndex )
                    {
                        //  不存在"._tcp.local"，认为不存在服务名
                        //  记录当前服务名的位置
                        *ps16ServerIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                        //  记录当前功能的索引
                        *ps16ModexIndex = *ps16ServerIndex + strlen( m_as8DServiceName ) + 1;
                        //  记录"._tcp.local"的位置
                        s16TcpIndex = *ps16ModexIndex + strlen(as8ClassName) + 1;
                        //  格式化服务名
                        memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
                        sprintf( m_as8MdnsTempBuf , "%s.%s.%s" , m_as8DServiceName , as8ClassName , "_tcp" );
                        //  填充报文
                        u32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[u32DataLen] );
                        //  受转换函数的影响,此处应把"\0"的结束符的位置填充压缩标记
                        u32DataLen -= 1;
                        //  填写压缩标志
                        pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16LocalIndex >> 8 ) ) );
                        pstHeader->buff[u32DataLen++] = (u8)( s16LocalIndex & 0xFF );
                    }
                    else
                    {
                        //  存在"._tcp.local"数据
                        if( -1 == *ps16ModexIndex )
                        {
                            //  不存在该功能,也就不存在服务名
                            //  记录当前服务名的位置
                            *ps16ServerIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                            //  记录当前功能的索引
                            *ps16ModexIndex = *ps16ServerIndex + strlen( m_as8DServiceName ) + 1;
                            //  格式化服务名
                            memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
                            sprintf( m_as8MdnsTempBuf , "%s.%s" , m_as8DServiceName , as8ClassName );
                            //  填充报文
                            u32DataLen += lxi_mdns_name_convert( m_as8MdnsTempBuf , &pstHeader->buff[u32DataLen] );
                            //  受转换函数的影响,此处应把"\0"的结束符的位置填充压缩标记
                            u32DataLen -= 1;
                            //  填写压缩标志
                            pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16TcpIndex >> 8 ) ) );
                            pstHeader->buff[u32DataLen++] = (u8)( s16TcpIndex & 0xFF );
                        }
                        else
                        {
                            //  存在该功能
                            if( -1 == *ps16ServerIndex )
                            {
                                //  当前服务名没有填充
                                //  记录当前服务名的位置
                                *ps16ServerIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                                //  填充报文
                                u32DataLen += lxi_mdns_name_convert( m_as8DServiceName , &pstHeader->buff[u32DataLen] );
                                //  受转换函数的影响,此处应把"\0"的结束符的位置填充压缩标记
                                u32DataLen -= 1;
                                //  填写压缩标志
                                pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( *ps16ModexIndex >> 8 ) ) );
                                pstHeader->buff[u32DataLen++] = (u8)( *ps16ModexIndex & 0xFF );
                            }
                            else
                            {
                                //  存在当前服务
                                //  填写压缩标志
                                pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( *ps16ServerIndex >> 8 ) ) );
                                pstHeader->buff[u32DataLen++] = (u8)( *ps16ServerIndex & 0xFF );
                            }
                        }
                    }
                }
                //标识该声明的类型以及所述类
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x21;
                pstHeader->buff[u32DataLen++] = 0x80;
                pstHeader->buff[u32DataLen++] = 0x01;
                //标识该声明的生存时间,4500TTL
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x78;

                if( -1 == s16HostIndex )
                {
                    //  主机名没有填充
                    pstHeader->buff[u32DataLen++] = 0x00;
                    pstHeader->buff[u32DataLen++] = 6 + strlen(m_as8DHostName) + 2 + 1;
                }
                else
                {
                    //  主机名已填充
                    pstHeader->buff[u32DataLen++] = 0x00;
                    pstHeader->buff[u32DataLen++] = 0x08;
                }
                //优先级
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                //权重
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                //端口号:80
                pstHeader->buff[u32DataLen++] = (u8)( u16Port >> 8   );
                pstHeader->buff[u32DataLen++] = (u8)( u16Port & 0xFF );

                if( -1 == s16HostIndex )
                {
                    //  主机名没有填充
                    //  记录主机名的地址
                    s16HostIndex = u32DataLen + LXI_MDNS_PACKET_HEADER_SIZE;
                    //  填充主机名
                    u32DataLen += lxi_mdns_name_convert( m_as8DHostName , &pstHeader->buff[u32DataLen]);
                    //  受转换函数的影响,此处应把"\0"的结束符的位置填充压缩标记
                    u32DataLen -= 1;
                    //  填写压缩标志
                    pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16LocalIndex >> 8 ) ) );
                    pstHeader->buff[u32DataLen++] = (u8)( s16LocalIndex & 0xFF );
                }
                else
                {
                    //  主机名已填充
                    pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16HostIndex >> 8 ) ) );
                    pstHeader->buff[u32DataLen++] = (u8)( s16HostIndex & 0xFF );
                }

                /*
                 * 组织附属信息:主机名信息
                */
                //主机名
                pstHeader->buff[u32DataLen++] = ( 0xC0 | ( (u8)( s16HostIndex >> 8 ) ) );
                pstHeader->buff[u32DataLen++] = (u8)( s16HostIndex & 0xFF );
                //标识该声明的类型以及所述类
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x01;
                pstHeader->buff[u32DataLen++] = 0x80;
                pstHeader->buff[u32DataLen++] = 0x01;
                //标识该声明的生存时间,120TTL
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x78;
                //标识IP长度的字段,4个字节
                pstHeader->buff[u32DataLen++] = 0x00;
                pstHeader->buff[u32DataLen++] = 0x04;
                //添加IP地址
                memcpy(&pstHeader->buff[u32DataLen],&m_stLxiConfPara.s32IpAddr,LXI_MDNS_IP_ADDR_LEN);
                //更新数据长度
                u32DataLen += LXI_MDNS_IP_ADDR_LEN;

                //  更新报文头
                pstHeader->ancount ++;
                pstHeader->adcount ++;
                break;
            default:
                break;
        }
    }
    //  数据翻转
    pstHeader->ancount = LXI_REV_WORD( pstHeader->ancount );
    pstHeader->adcount = LXI_REV_WORD( pstHeader->adcount );
    if( u32DataLen != 0 )
    {
        u32DataLen += LXI_MDNS_PACKET_HEADER_SIZE;

        lxi_mdns_packet_send( ps8Buf , u32DataLen );
    }
}

/**************************************************************************
*函 数 名: lxi_mdns_name_rename
*描       述: 根据mdns协议,当命名冲突时重命名
*输入参数:
*          参数名         类型              描述
*          ps8Name       s8_t *         待修改的名字字段
*输出参数:
*返 回 值:
*说    明:
***************************************************************************/
void lxi_mdns_name_rename(s8 *ps8Data)
{
    s32 i = 0,len = strlen(ps8Data);
    s8  tag;//标识下划线后面是否全为数字

    for(; i < len ; i ++)//先查找当前名字是否含有下划线'_'
    {
        if(ps8Data[i] == '_')
        {
            //查找到下划线'_'
            break;
        }
    }
    if(i == len)
    {
        //查找完字符串,没有发现下划线'_',则直接在字符串后面添加'_'和数字'1'
        ps8Data[i]   = '_';
        ps8Data[i+1] = '1';
        ps8Data[i+2] = '\0';
    }
    else
    {
        //已在字符串添加了后缀'_',继续查找,是否为已经重新命名
        for(;i< len ; i ++)
        {
            if(ps8Data[i] >= '0' && ps8Data[i] <= '9')
            {
                tag = 1;
            }
            else
            {
                tag = 0;
            }
        }
        if(tag == 1)
        {
            if(ps8Data[i-1] == '9')
            {
                ps8Data[i-1] = '0';
                ps8Data[i]   = '1';
            }
            else
            {
                ps8Data[i-1] += 1;
            }
        }
        else
        {
            ps8Data[i]   = '_';
            ps8Data[i+1] = '1';
            ps8Data[i+2] = '\0';
        }
    }
}

void lxi_mdns_response_process( void )
{
    memset( m_as8MdnsTempBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    //  只判断第一个报文
    if( LXI_MDNS_QUERY_HOST_ADDR == m_au16MdnsRespType[0] )
    {
        m_as8MdnsTempBuf[0] = strlen( m_as8DHostName );
        strcpy( &m_as8MdnsTempBuf[1] , m_as8DHostName );
        m_as8MdnsTempBuf[strlen( m_as8DHostName )+1] = strlen("local");
        strcat( m_as8MdnsTempBuf , "local" );
        //判断是否主机名冲突
        if( 0 == strcmp( m_as8MdnsResponses[0] , m_as8MdnsTempBuf ) )
        {
            //主机名冲突
            lxi_mdns_name_rename( m_as8DHostName );
            //  置主机名冲突状态,交给事件触发线程处理
            m_emMdnsState = LXI_MDNS_STATE_HOST_CONFLICT;
            //  通知UI层
            if( NULL != m_stLxiInitPara.pfunUiCallback )
            {
                m_stLxiInitPara.pfunUiCallback( LXI_UI_MDNS_HOSTNAME_CONFLICT );
            }
        }
    }
    else if( LXI_MDNS_QUERY_SRV == m_au16MdnsRespType[0] )
    {
        m_as8MdnsTempBuf[0] = strlen( m_as8DServiceName );
        strcpy( &m_as8MdnsTempBuf[1] , m_as8DServiceName );
        lxi_mdns_name_convert( "_http._tcp.local" , &m_as8MdnsTempBuf[strlen( m_as8DServiceName )+1] );
        //判断是否主机名冲突
        if( 0 == strcmp( m_as8MdnsResponses[0] , m_as8MdnsTempBuf ) )
        {
            //主机名冲突
            lxi_mdns_name_rename( m_as8DServiceName );
            //  置主机名冲突状态,交给事件触发线程处理
            m_emMdnsState = LXI_MDNS_STATE_SERVICE_CONFLICT;
            //  通知UI层
            if( NULL != m_stLxiInitPara.pfunUiCallback )
            {
                m_stLxiInitPara.pfunUiCallback( LXI_UI_MDNS_SERVICENAME_CONFLICT );
            }
        }
    }
}

/*
 * mDNS事件处理线程
*/
void *lxi_mdns_event_process_thread( void )
{
    LXI_MDNS_EVENT_EM           emEvent;
    DNS_HEADER_QR_E             emPacketType;
    s32                         s32BufLen = 0;

    while( 1 )
    {
        /*清空缓冲区*/
        memset( m_as8MdnsParseBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
        /*监听事件*/
        emEvent = lxi_mdns_event_queue_read( m_as8MdnsParseBuf , &s32BufLen );
        /*事件处理*/
        switch( emEvent )
        {
            case LXI_MDNS_EVENT_HOST_QUERY:
                lxi_mdns_host_packet( m_as8MdnsSendBuf , OPCODE_QUERY );
                break;
            case LXI_MDNS_EVENT_HOST_QUERY_M:
                lxi_mdns_host_packet( m_as8MdnsSendBuf , OPCODE_IQUERY);
                break;
            case LXI_MDNS_EVENT_HOST_RESPONSE:
                lxi_mdns_host_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_QUERY:
                lxi_mdns_server_question("_http._tcp.local",m_as8MdnsSendBuf , 1 , 80 );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_QUERY_M:
                lxi_mdns_server_question("_http._tcp.local",m_as8MdnsSendBuf , 0 , 80 );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_RESPONSE:
                lxi_mdns_server_http_ptr( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_RESPONSE_HOST:
                lxi_mdns_server_http_with_host_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE:
                lxi_mdns_server_http_txt_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE_WITH_PTR:
                lxi_mdns_server_http_txt_declare_with_ptr( m_as8MdnsSendBuf , 1 );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_ALL_WITH_IP_RESPONSE:
                lxi_mdns_server_http_all_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE_WITH_PTR_SVC:
                lxi_mdns_server_http_txt_declare_with_ptr( m_as8MdnsSendBuf , 0 );
                break;
            case LXI_MDNS_EVENT_LXI_SERVICE_QUERY:
                lxi_mdns_server_question("_lxi._tcp.local",m_as8MdnsSendBuf , 1 , 80 );
                break;
            case LXI_MDNS_EVENT_LXI_SERVICE_QUERY_M:
                lxi_mdns_server_question("_lxi._tcp.local",m_as8MdnsSendBuf , 0 , 80 );
                break;
            case LXI_MDNS_EVENT_LXI_SERVICE_RESPONSE_WITH_TXT:
                lxi_mdns_server_txt_declare( "_lxi._tcp.local" , 80 , m_as8MdnsSendBuf , 1 );
                break;
            case LXI_MDNS_EVENT_LXI_SERVICE_RESPONSE_WITH_HOST:
                lxi_mdns_server_lxi_txt_with_host_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_SCPI_SERVICE_QUERY:
                lxi_mdns_server_question("_scpi-raw._tcp.local",m_as8MdnsSendBuf , 1 , m_u32SocketPort );
                break;
            case LXI_MDNS_EVENT_SCPI_SERVICE_QUERY_M:
                lxi_mdns_server_question("_scpi-raw._tcp.local",m_as8MdnsSendBuf , 0 , m_u32SocketPort );
                break;
            case LXI_MDNS_EVENT_SCPI_SERVICE_RESPONSE_WITH_TXT:
                lxi_mdns_server_txt_declare( "_scpi-raw._tcp.local" , m_u32SocketPort , m_as8MdnsSendBuf , 1);
                break;
            case LXI_MDNS_EVENT_VXI_SERVICE_QUERY:
                lxi_mdns_server_question("_vxi-11._tcp.local",m_as8MdnsSendBuf , 1 , 111 );
                break;
            case LXI_MDNS_EVENT_VXI_SERVICE_QUERY_M:
                lxi_mdns_server_question("_vxi-11._tcp.local",m_as8MdnsSendBuf , 0 , 111 );
                break;
            case LXI_MDNS_EVENT_VXI_SERVICE_RESPONSE_WITH_TXT:
                lxi_mdns_server_txt_declare( "_vxi-11.tcp.local" , 111 , m_as8MdnsSendBuf , 0 );
                break;
            case LXI_MDNS_EVENT_VXI_SERVICE_RESPONSE_WITH_HOST:
                lxi_mdns_server_txt_declare( "_vxi-11.tcp.local" , 111 , m_as8MdnsSendBuf , 1 );
                break;
            case LXI_MDNS_EVENT_HTTP_LXI_SCPI_RESPONSE:
                lxi_mdns_server_http_lxi_scpi_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_SCPI_VXI_RESPONSE:
                lxi_mdns_server_scpi_with_vxi_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_TIMER:
                lxi_mdns_server_timer_declare( m_as8MdnsSendBuf );
                break;
            case LXI_MDNS_EVENT_PACKET_PARSE:
                emPacketType = lxi_mdns_packet_parse( m_as8MdnsParseBuf , s32BufLen );
                if( QR_QUERY == emPacketType && LXI_MDNS_STATE_SUCCESS == m_emMdnsState )
                {
                    //  只有当前mDNS声明完成后,才接受报文的查询
                    lxi_mdns_query_process( m_as8MdnsSendBuf );
                }
                else if( QR_RESPONSE == emPacketType && ( LXI_MDNS_STATE_HOST_PROBE == m_emMdnsState || LXI_MDNS_STATE_SERVICE_PROBE == m_emMdnsState ))
                {
                    //  只有在mDNS状态为探测阶段,才进行响应报文的处理
                    //  此时,只判断主机名冲突和服务名冲突,不进行报文的发送
                    lxi_mdns_response_process();
                }
                break;
        }
    }
}

/*
 * mDNS探测线程
 * 该线程按照安捷伦仪器的mDNS探测顺序进行触发事件
 * 当所有事件发送完成后,间隔轮询发送主机名和当前LXI设备支持的所有服务的声明报文
 * 每次事件触发前会判定当前网络状态是否发生改变,如果发生变化,则根据当前状态变化
 * 进行报文发送顺序的调整,为保证实时性,定时器将采用500us一次的检查网络状态
*/
void *lxi_mdns_probe_thread( void )
{
/*由于该线程对事件队列的写入操作,只需要写事件码值,为操作方便,定义此宏*/
#define     MDNS_EVENT_WR(e)  lxi_mdns_event_queue_write( e , NULL , 0 )
    u32     u32TrigNum          = 0;    //  记录当前mDNS的事件触发次数
    u32     u32MonitorTime      = 0;    //  监控时间,单位us
    s32     s32MonitorResult    = 0;    //  监控结果
    LXI_STATUS_EM emLxiStatus   = LXI_STATUS_UNLINK;

    while(1)
    {
        //printf("Trigger Number Is %d\n",u32TrigNum);
        /*当前网络已经完成配置且mDNS服务启动,才开始进行事件的触发*/
        if( LXI_STATUS_CONFIG_SUCCESS == m_emLxiCurrState && 1 == m_stLxiConfPara.u8MdnsEnable )
        {
            //  如果上次的网络连接状态与当前的网络连接状态发生了改变,进行延迟处理,等待网络环境稳定
            //  避免LXI认证过程中不能识别主机名冲突
            if( m_emLastNetStatus == LXI_STATUS_UNLINK )
            {
                sleep(5);
                m_emLastNetStatus = m_emLxiCurrState;
            }
            switch( u32TrigNum )
            {
                case 0:
                case 4:
                case 17:
                    /*置当前mDNS状态为主机名探测*/
                    m_emMdnsState = LXI_MDNS_STATE_HOST_PROBE;
                    /*发送主机名的QU查询*/
                    MDNS_EVENT_WR(LXI_MDNS_EVENT_HOST_QUERY);
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 1:
                case 2:
                case 5:
                case 6:
                case 18:
                case 19:
                    /*发送主机名的QM查询*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HOST_QUERY_M );
                    if( u32TrigNum == 1 )
                    {
                        u32MonitorTime = 2000000;    //  2s
                    }
                    else
                    {
                        u32MonitorTime = 250000;    //  250ms
                    }
                    break;
                case 3:
                case 7:
                case 20:
                case 24:
                    /*发送主机名的声明事件*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HOST_RESPONSE );
                    /*如果主机名第一次声明,需要等待过长的时间,给当前的网络环境反馈的时间*/
                    if( u32TrigNum == 3 )
                    {
                        u32MonitorTime = 750000;    //  750ms
                    }
                    else if ( u32TrigNum == 24 )    //  主机名和HTTP服务已经完成了探测和声明,接下来进行LXI服务的探测和声明,该等待时间不需要太长
                    {
                        u32MonitorTime = 100000;    //  100ms
                    }
                    else
                    {
                        u32MonitorTime = 250000;    //  250ms
                    }
                    break;
                case 8:
                case 21:
                    /*置当前mDNS状态为服务名探测*/
                    m_emMdnsState = LXI_MDNS_STATE_SERVICE_PROBE;
                    /*发送HTTP服务的QU查询*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_QUERY );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 9:
                case 10:
                case 22:
                case 23:
                    /*发送HTTP服务的QM查询*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_QUERY_M );
                    if( u32TrigNum == 23 )  //  此时已经完成了HTTP服务的探测和声明,最后一次重申,时间不需要太长
                    {
                        u32MonitorTime = 150000;    //  150ms
                    }
                    else
                    {
                        u32MonitorTime = 250000;    //  250ms
                    }
                    break;
                case 11:
                    /*发送当前主机映射支持HTTP服务的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_RESPONSE_HOST );
                    u32MonitorTime = 100000;    //  100ms
                    break;
                case 12:
                    /*发送HTTP服务的TXT属性的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE );
                    u32MonitorTime = 900000;    //  900ms
                    break;
                case 13:
                case 29:
                    /*发送HTTP服务以及TXT属性和主机名的绑定声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE_WITH_PTR );
                    if ( u32TrigNum == 29 )
                    {
                        /*此时已经完成了主机名和HTTP服务名的探测和声明服务,不需要等待太长时间*/
                        u32MonitorTime = 100000;    //  100ms
                    }
                    else
                    {
                        u32MonitorTime = 1000000;   //  1s
                    }
                    break;
                case 14:
                case 15:
                    /*发送HTTP服务的TXT属性、属于主机名所属仪器IP地址的服务的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_ALL_WITH_IP_RESPONSE );
                    /*该报文第一次发送后,需要留足充分的时间供局域网中的设备进行记录*/
                    if( u32TrigNum == 14 )
                    {
                        u32MonitorTime = 4000000;   //  4s
                    }
                    else
                    {
                        u32MonitorTime = 1000000;   //  1s
                    }
                    break;
                case 16:
                    /*此时没有收到服务名冲突的报文,开始HTTP服务名的声明报文发送*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_RESPONSE );
                    u32MonitorTime = 500000;    //  500ms
                    break;
                case 25:
                    /*发送HTTP服务的TXT、PTR、SVC属性的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_SERVICE_TXT_RESPONSE_WITH_PTR_SVC );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 26:
                    /*发送LXI服务的QU探测*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_LXI_SERVICE_QUERY );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 27:
                case 28:
                    /*发送LXI服务的QM探测*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_LXI_SERVICE_QUERY_M );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 30:
                    /*发送LXI服务的TXT属性的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_LXI_SERVICE_RESPONSE_WITH_TXT );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 31:
                    /*发送SCPI服务的QU探测*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_SCPI_SERVICE_QUERY );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 32:
                case 33:
                    /*发送SCPI服务的QM探测*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_SCPI_SERVICE_QUERY_M );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 34:
                    /*发送LXI服务的TXT、PTR、SVC属性的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_LXI_SERVICE_RESPONSE_WITH_HOST );
                    u32MonitorTime = 150000;    //  150ms
                    break;
                case 35:
                    /*发送SCPI服务的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_SCPI_SERVICE_RESPONSE_WITH_TXT );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 36:
                    /*发送VXI-11服务的QU探测*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_VXI_SERVICE_QUERY );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 37:
                case 38:
                    /*发送VXI-11服务的QM探测*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_VXI_SERVICE_QUERY_M );
                    u32MonitorTime = 250000;    //  250ms
                    break;
                case 39:
                    /*发送HTTP、LXI、SCPI的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_HTTP_LXI_SCPI_RESPONSE );
                    u32MonitorTime = 100000;    //  100ms
                    break;
                case 40:
                    /*发送VXI-11服务的声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_VXI_SERVICE_RESPONSE_WITH_TXT );
                    u32MonitorTime = 700000;    //  700ms
                    break;
                case 41:
                    /*发送SCPI服务与VXI-11服务的联合声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_SCPI_VXI_RESPONSE );
                    u32MonitorTime = 1000000;   //  1s
                    break;
                case 42:
                    /*发送VXI-11服务的主机IP地址声明*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_VXI_SERVICE_RESPONSE_WITH_HOST );
                    u32MonitorTime = 4000000;   //  4s
                    break;
                default:
                    /*轮询发送全服务的重申*/
                    MDNS_EVENT_WR( LXI_MDNS_EVENT_TIMER );
                    if( u32TrigNum == 43 )
                    {
                        /*mDNS主机名和所拥有的服务均已完成探测和声明，接下来将逐步增加轮询间隔,直到间隔时间为32s不再增加间隔时间*/
                        m_emMdnsState = LXI_MDNS_STATE_SUCCESS;
                        /*上报LXI应用层mDNS探测成功*/
                        emLxiStatus = LXI_STATUS_MDNS_PROBE_SUCCESS;
                        LXI_Option( LXI_MODULE_NETWORK_STATUS_REPORT , &emLxiStatus );
                        u32MonitorTime = 8000000;   //  8s
                    }
                    else if( u32TrigNum == 44 )
                    {
                        u32MonitorTime = 16000000;  //  16s
                    }
                    else
                    {
                        u32MonitorTime = 32000000;  //  32s
                    }
                    break;
            }
            /*进行当前网络状态的监控*/
            s32MonitorResult = lxi_mdns_network_state_monitor( u32MonitorTime/LXI_MDNS_MONITOR_PERIOD );
            if( -1 == s32MonitorResult )    //  发生了网络变化
            {
                //  其中服务名冲突,需要特殊处理,此时不需要主机名的探测
                if( LXI_MDNS_STATE_SERVICE_CONFLICT == m_emMdnsState )
                {
                    u32TrigNum = 8;
                }
                else
                {
                    //  其他一律从主机名走起lxi_mdns_probe
                    u32TrigNum = 0;
                }
            }
            else
            {
                //  等待时间正常结束,进行下一个事件的发送
                //  当进入32s轮询发送主机名和所拥有服务的重申报文后，不再累加事件触发数
                if( 45 > u32TrigNum )
                {
                    u32TrigNum ++;
                }
            }
        }
        else
        {
            //  如果当前网络未完成配置 , 则需要一直监控
            lxi_mdns_network_state_monitor(LXI_MDNS_MONITOR_MAX_TIME);
            //  从主机名的QU探测走起
            u32TrigNum = 0;
        }
    }
}

/*
 * mDNS报文接收线程
*/

void *lxi_mdns_service_thread( void )
{
    struct  sockaddr_in remote;
    socklen_t           len     = sizeof( struct sockaddr_in );
    s32                 s32RecvLen;
    /*开始数据的接收*/
    while(1)
    {
        memset( m_as8MdnsRecvBuf , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
        s32RecvLen = recvfrom( m_s32MdnsSock , m_as8MdnsRecvBuf , LXI_MDNS_PACKET_MAX_SIZE , 0 , (struct sockaddr*)&remote , &len );

        if( s32RecvLen > 0 && 1 == m_stLxiConfPara.u8MdnsEnable && (s32)remote.sin_addr.s_addr != m_stLxiConfPara.s32IpAddr )
        {
            lxi_mdns_event_queue_write( LXI_MDNS_EVENT_PACKET_PARSE , m_as8MdnsRecvBuf , s32RecvLen );
        }
    }
}

/*
 * LXI-MDNS功能启动接口定义
*/
void lxi_mdns_start( )
{
    struct      sockaddr_in     local = { 0 };
    socklen_t                   len   = sizeof( struct sockaddr_in );
    struct      ip_mreq         addr;
    lxi_pthread_t               id;
    s32                         s32IpTTLVal = 255;
    s32                         s32BufIndex;

    /*
     * 初始化Http服务的TXT描述内容
    */
    memset( m_as8MdnsHttpTxtInfo , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    lxi_mdns_name_convert( "txtvers=1.path=/" , m_as8MdnsHttpTxtInfo );
    /*
     * 初始化lxi、scpi、vxi服务的TXT描述内容
    */
    memset( m_as8MdnsLxiTxtInfo , '\0' , LXI_MDNS_PACKET_MAX_SIZE );
    s32BufIndex = lxi_mdns_name_convert( "txtvers=1" , m_as8MdnsLxiTxtInfo );
    s32BufIndex --;
    //  生产厂商格式化
    m_as8MdnsLxiTxtInfo[s32BufIndex++] = strlen( m_as8Manufacture ) + strlen( "Manufacturer=" );
    s32BufIndex += sprintf( &m_as8MdnsLxiTxtInfo[s32BufIndex] , "Manufacturer=%s" , m_as8Manufacture );
    //  仪器型号
    m_as8MdnsLxiTxtInfo[s32BufIndex++] = strlen( m_as8InstrumentModel ) + strlen( "Model=" );
    s32BufIndex += sprintf( &m_as8MdnsLxiTxtInfo[s32BufIndex] , "Model=%s" , m_as8InstrumentModel );
    //  仪器序列号
    m_as8MdnsLxiTxtInfo[s32BufIndex++] = strlen( m_as8InstrumentSeries ) + strlen( "SerialNumber=" );
    s32BufIndex += sprintf( &m_as8MdnsLxiTxtInfo[s32BufIndex] , "SerialNumber=%s" , m_as8InstrumentSeries );
    //  版本号
    m_as8MdnsLxiTxtInfo[s32BufIndex++] = strlen( m_as8FirmwareVersion ) + strlen( "FirmwareVersion=" );
    s32BufIndex += sprintf( &m_as8MdnsLxiTxtInfo[s32BufIndex] , "FirmwareVersion=%s" , m_as8FirmwareVersion );
    /*
     * 初始化事件队列
    */
    lxi_mdns_event_queue_init();

    usleep(100000);

    /*添加mDNS组播地址的路由*/
    lxi_net_route_add( inet_addr( LXI_MDNS_SERVICE_ADDR ) , 0xFFFFFFFF );
    //printf("Config mDNS Gateway\n");

    /*
     * 创建mDNS通信过程中使用的Socket句柄
    */
    if( ( m_s32MdnsSock = socket( AF_INET , SOCK_DGRAM , IPPROTO_UDP ) ) < 0 )
    {
        LXI_DEBUG( "lxi_mdns.c:Socket:Create Fail!\n" );
        return;
    }

    /*
     * 设置该socket的IP层的TTL时间
    */
    if( setsockopt( m_s32MdnsSock , IPPROTO_IP , IP_MULTICAST_TTL , (void*)&s32IpTTLVal , sizeof(s32) ) != 0 )
    {
        LXI_DEBUG( "lxi_mdns.c:Socket:setsockopt TTL Value Fail!\n" );
        return ;
    }
    /*
     * 进行Socket地址的绑定
    */
    local.sin_family      = AF_INET;
    local.sin_port        = htons( LXI_MDNS_SERVICE_PORT );
    local.sin_addr.s_addr = htonl( INADDR_ANY );
    if( bind( m_s32MdnsSock , (struct sockaddr*)&local , len ) < 0 )
    {
        LXI_DEBUG( "lxi_mdns.c:Socket:Bind Fail!\n" );
        return ;
    }
    /*
     * 把本地地址和组播地址添加到网络内核中
    */
    addr.imr_interface.s_addr = htonl(INADDR_ANY);
    addr.imr_multiaddr.s_addr = inet_addr(LXI_MDNS_SERVICE_ADDR);
    if( setsockopt( m_s32MdnsSock , IPPROTO_IP , IP_ADD_MEMBERSHIP , (void*)&addr , sizeof(addr) ) < 0 )
    {
        LXI_DEBUG( "lxi_mdns.c:Socket:Add Membership Fail!\n" );
        return ;
    }

    lxi_thread_create( &id , (void*)lxi_mdns_event_process_thread , NULL );    //  启动mDNS事件处理线程
    lxi_thread_create( &id , (void*)lxi_mdns_service_thread       , NULL );    //  启动mDNS报文的接收
    lxi_thread_create( &id , (void*)lxi_mdns_probe_thread         , NULL );    //  启动mDNS探测服务
}

/*
    +---------------------+
    |        Header       |
    +---------------------+
    |       Question      | the question for the name server
    +---------------------+
    |        Answer       | RRs answering the question
    +---------------------+
    |      Authority      | RRs pointing toward an authority
    +---------------------+
    |      Additional     | RRs holding additional information
    +---------------------+

    Header section format
    The header contains the following fields:

                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QDCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ANCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    NSCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ARCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

*/
/*
*函 数 名: lxi_mdns_resource_name_parse
*描       述: mdns资源名字的解析
*输入参数:
*          参数名             类型                  描述
*        pscPktBase          s8_t *         原mdns接收的数据包
*        ppscCurBuf          u8_t **        当前数据包的位置
*        ps32CurBufLen       s32_t  *       当前数据包的长度
*        pscDstBuf           s8_t *         保存资源名字的地址
*        s32DstBufMaxLen     s32_t          资源名字的最大长度
*输出参数:
*返 回 值:返回0表示成功，其他值失败
*说    明:
*/
s32 lxi_mdns_resource_name_parse( s8    *pscPktBase,
                                  u8   **ppscCurBuf,
                                  s32   *ps32CurBufLen,
                                  s8    *pscDstBuf,
                                  s32    s32DstBufMaxLen)
{
    s32     s32DstBufCurLen = 0;
    s32     s32Len;
    u8     *pscBuf, *pscTmp;
    s32     s32IsNameCompress;
    s32     bLastLabelIsCompress = 0;
    u16     u16Offset;

    pscBuf = *ppscCurBuf;
    // NAME 分多个label存储,且其中有些label可能存在压缩情况,使用while分别取各个label
    // 压缩label只出现在 NAME 的末尾.
    // 每个 label 的最大长度为 63 字节
    // 最后一个label是一个字节的 0, 表示NAME结束
    while(*pscBuf)
    {
        s32IsNameCompress = DNS_IS_NAME_COMPRESS(*pscBuf);
        if(s32IsNameCompress)
        {
            //compression format
            //取当前 NAME 在DNS报文净荷中的偏移位置
            u16Offset = DNS_NAME_OFFSET_IN_COMPRESS((*pscBuf<<8)|((u16)*(pscBuf + 1)));
            pscTmp = (u8 *)(pscPktBase + u16Offset);
            while(*pscTmp)
            {
                s32IsNameCompress = DNS_IS_NAME_COMPRESS(*pscTmp);
                if(s32IsNameCompress)
                {
                    //取当前 NAME 在DNS报文净荷中的偏移位置
                    u16Offset = DNS_NAME_OFFSET_IN_COMPRESS((*pscTmp<<8)|((u16)*(pscTmp + 1)));
                    pscTmp = (u8 *)(pscPktBase + u16Offset);
                }
                // NAME 格式为: 1个字节的长度域 + 指定长度的字符
                s32Len = *pscTmp + 1;
                if(s32Len > LXI_MDNS_LABEL_MAX_SIZE )
                {
                    return -1;
                }
                memcpy(pscDstBuf,pscTmp,s32Len);
                pscDstBuf += s32Len;
                pscTmp += s32Len;
            }
            //压缩格式，此处长度仅有 2 byte
            s32Len = 2;
            bLastLabelIsCompress = 1;
        }
        else
        {
            // NAME 格式为: 1个字节的长度域 + 指定长度的字符
            s32Len = *pscBuf + 1;
            if((s32Len > *ps32CurBufLen)
                ||(s32Len > LXI_MDNS_LABEL_MAX_SIZE)
                ||(s32Len > (s32DstBufMaxLen - s32DstBufCurLen)))
            {
                return -2;
            }
            memcpy(pscDstBuf,pscBuf,s32Len);
            pscDstBuf += s32Len;
            s32DstBufCurLen += s32Len;
            bLastLabelIsCompress = 0;
        }

        if(s32DstBufCurLen >= s32DstBufMaxLen)
        {
            return -3;
        }
        pscBuf += s32Len;
        *ps32CurBufLen -= s32Len;
    }
    if(s32DstBufCurLen + 1 >= s32DstBufMaxLen)
    {
        return -4;
    }
    // NAME中的各个label结束以后,需要copy一个空的label
    memcpy(pscDstBuf,"\0",1);
    //最后一个label如果不是压缩形式,则待解析报文指针需要向后移一个byte
    if(bLastLabelIsCompress == 0)
    {
        pscBuf++;
        *ps32CurBufLen = *ps32CurBufLen - 1;
    }
    *ppscCurBuf = pscBuf;
    return 0;
}


/*
*函 数 名: lxi_mdns_packet_parse
*描       述: mdns报文的解析
*输入参数:
*          参数名         类型                  描述
*          ps8Buff       s8_t *          待解析的数据包
*          u32BuffLen    s32_t           待解析数据包的长度
*输出参数:
*返 回 值:当前接收到数据包的类型，1--询问报文,2--响应报文
*说    明:
*/
DNS_HEADER_QR_E lxi_mdns_packet_parse(s8 *ps8Buff , s32 s32BuffLen)
{
    LXI_MDNS_MESSAGE_STRU   *pstMDNSMsg = (LXI_MDNS_MESSAGE_STRU *)ps8Buff;
    u16                     *pu16Buf    = (u16*)ps8Buff;
    u8                      *pscBuf     = NULL;
    s32                      i = 0,s32Len = 0;
    s32                      s32PktRemainLen = s32BuffLen;

    //校验输入报文长度,DNS Header是必须有的，字节长度为12
    if(s32PktRemainLen <= LXI_MDNS_PACKET_HEADER_SIZE)
    {
        return QR_INVALID;
    }
    //字节翻转以后，可以直接使用结构读取
    for(;i<LXI_MDNS_PACKET_HEADER_SIZE;i+=2)
    {
        *pu16Buf = LXI_REV_WORD(*pu16Buf);
        pu16Buf++;
    }
    s32PktRemainLen -= LXI_MDNS_PACKET_HEADER_SIZE;

    //指向header之后的第一个字段
    pscBuf = (u8 *)(ps8Buff + LXI_MDNS_PACKET_HEADER_SIZE);

    if(pstMDNSMsg->header.qr == QR_QUERY)
    {
        if(pstMDNSMsg->header.opcode > OPCODE_STATUS)
        {
            //检测到一个非法值
            return QR_INVALID;
        }
        if(pstMDNSMsg->header.aa > AA_VALID)
        {
            //检测到一个非法值
            return QR_INVALID;
        }
        if( pstMDNSMsg->qdcount > LXI_MDNS_MAX_QUESTIONS_NUM ||
            ( pstMDNSMsg->ancount + pstMDNSMsg->nscount ) > LXI_MDNS_MAX_RESPONSE_NUM )
        {
            //  超出当前mDNS缓存的个数,不进行解析
            return QR_INVALID;
        }
        /*mDNS当前报文查询个数的记录变量先初始化*/
        m_u32MdnsCueeQueryNum = 0;
        if(pstMDNSMsg->qdcount > 0)
        {
            for(i=0; ((i < pstMDNSMsg->qdcount)&&(i < LXI_MDNS_MAX_QUESTIONS_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                                     m_as8MdnsQuestions[i],
                                                     LXI_MDNS_NAME_MAX_LEN) )
                {
                    return QR_INVALID;
                }
                //QTYPE,QCLASS字段长度分别为 2 byte
                m_au16MdnsQuestType[i] = (u16)((*pscBuf << 8)|((u16)*(pscBuf + 1)));
                pscBuf += 4;
                s32PktRemainLen -= 4;
                if(s32PktRemainLen < 0)
                {
                    return QR_INVALID;
                }
                //  当前查询有效，则计数
                m_u32MdnsCueeQueryNum ++;
            }
        }

        if(pstMDNSMsg->ancount > 0)
        {
            for(i=0; ((i<pstMDNSMsg->ancount)&&(i < LXI_MDNS_MAX_RESPONSE_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                                     m_as8MdnsResponses[i],
                                                     LXI_MDNS_NAME_MAX_LEN))
                {
                    return QR_INVALID;
                }

                m_au16MdnsRespType[i] = (u16)((*pscBuf << 8)|((u16)*(pscBuf + 1)));
                //这儿的8 是 TYPE(2 byte), CLASS(2 byte), TTL(4 byte) 的长度总和
                pscBuf += 8;
                s32PktRemainLen -= 8;
                if(s32PktRemainLen < 0)
                {
                    return QR_INVALID;
                }
                //这儿取 RDLENGTH 中的长度, 协议定义该长度为 2byte长度
                //且应该加上 RDLENGTH 本身的长度(2)
                s32Len = ((*pscBuf << 8)|((u16)*(pscBuf + 1))) + 2;
                pscBuf += s32Len;
                s32PktRemainLen -= s32Len;
                if(s32PktRemainLen < 0)
                {
                    return QR_INVALID;
                }
            }
        }

        if(pstMDNSMsg->nscount > 0)
        {
            for(i=0; ((i<pstMDNSMsg->nscount)&&(i < LXI_MDNS_MAX_RESPONSE_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                                     m_as8MdnsResponses[i],
                                                     LXI_MDNS_NAME_MAX_LEN))
                {
                    return QR_INVALID;
                }

                m_au16MdnsRespType[i] = (u16)((*pscBuf << 8)|((u16)*(pscBuf + 1)));
                //这儿的8 是 TYPE(2 byte), CLASS(2 byte), TTL(4 byte) 的长度总和
                pscBuf += 8;
                s32PktRemainLen -= 8;
                if(s32PktRemainLen < 0)
                {
                    return QR_INVALID;
                }
                //这儿取 RDLENGTH 中的长度, 协议定义该长度为 2byte长度
                //且应该加上 RDLENGTH 本身的长度(2)
                s32Len = ((*pscBuf << 8)|((u16)*(pscBuf + 1))) + 2;
                pscBuf += s32Len;
                s32PktRemainLen -= s32Len;
                if(s32PktRemainLen < 0)
                {
                    return QR_INVALID;
                }
            }
        }
        return QR_QUERY;

    }
    else
    {
        //rcode 字段值为错误码,0表示没有错误
        if(pstMDNSMsg->header.rcode != 0)
        {
            //暂时不处理错误码
            return QR_INVALID;
        }
        
        if( pstMDNSMsg->ancount > LXI_MDNS_MAX_RESPONSE_NUM )
        {
            //  超出mDNS模块接收响应个数的上限,不进行处理
            return QR_INVALID;
        }

        if(pstMDNSMsg->ancount> 0)
        {
            for(i=0; ((i<pstMDNSMsg->ancount)&&(i < LXI_MDNS_MAX_RESPONSE_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                                     m_as8MdnsResponses[i],
                                                     LXI_MDNS_NAME_MAX_LEN))
                {
                    return QR_INVALID;
                }

                m_au16MdnsRespType[i] = (u16)((*pscBuf << 8)|((u16)*(pscBuf + 1)));
                //这儿的8 是 TYPE(2 byte), CLASS(2 byte), TTL(4 byte) 的长度总和
                pscBuf += 8;
                s32PktRemainLen -= 8;
                if(s32PktRemainLen < 0)
                {
                    return QR_INVALID;
                }
                //这儿取 RDLENGTH 中的长度, 协议定义该长度为 2byte长度
                //且应该加上 RDLENGTH 本身的长度(2)
                s32Len = ((*pscBuf << 8)|((u16)*(pscBuf + 1))) + 2;
                pscBuf += s32Len;
                s32PktRemainLen -= s32Len;
                if(s32PktRemainLen < 0)
                {
                    return QR_INVALID;
                }
            }
        }
        else
        {
            return QR_INVALID;
        }
        return QR_RESPONSE;
    }
}

