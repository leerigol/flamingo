/*******************************************************************************
                普源精电科技有限公司版权所有(2017-2021)
********************************************************************************
  文 件 名: xdr_vxi.h
  功能描述: Vxi-11功能相关xdr解析的函数定义

  作    者: 孙哲
  版    本: V1.0
  创建日期: 2017-11-06

  修改历史：
  作者          修改时间            版本        修改内容
  孙哲         2017-11-06          1.0        initial

*******************************************************************************/

#include "xdr_vxi.h"

/*
 * 定义VXI设备进行连接时的连接参数的获取
*/

bool_t vxi_device_link(XDR *pstXdrs , DEVICE_LINK *pLinkPara)
{
    if( !XDR_VXI_INT( pstXdrs , pLinkPara ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备连接时的通信协议类获取
*/
bool_t vxi_device_family( XDR* pstXdrs , DEVICE_FAMILY_EM *pemFamily)
{
    if( !xdr_enum( pstXdrs , (enum_t*)pemFamily ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备通信时的标识获取
*/
bool_t vxi_device_flags( XDR *pstXdrs , DEVICE_FLAGS *pemFlags)
{
    if( !XDR_VXI_INT( pstXdrs , pemFlags ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备的错误码值获取,内部接口
*/
bool_t  vxi_device_error_code( XDR *pstXdrs , DEVICE_ERROR_CODE *pemErrorCode )
{
    if( !XDR_VXI_INT( pstXdrs , pemErrorCode ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备的错误获取
*/
bool_t vxi_device_error( XDR *pstXdrs , DEVICE_ERROR_STRU *pstError)
{
    if( !vxi_device_error_code( pstXdrs , &pstError->error ) )
    {
        return FALSE;
    }
    return TRUE;
}
/*
 * 获取VXI设备建立连接时的参数
*/
bool_t vxi_device_link_para( XDR *pstXdrs , DEVICE_LINK_PARA_STRU *pstLinkPara)
{
    /*设置VXI设备ID*/
    if( !XDR_VXI_INT( pstXdrs , &pstLinkPara->id ) )
    {
        return FALSE;
    }
    /*设置当前设备是否被锁定*/
    if( !xdr_bool( pstXdrs , &pstLinkPara->lock ) )
    {
        return FALSE;
    }
    /*设置锁定超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstLinkPara->lock_timeout ) )
    {
        return FALSE;
    }
    /*获取客户端VXI设备描述符*/
    if( !xdr_string( pstXdrs , &pstLinkPara->device , ~0 ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 处理VXI设备的连接响应报文
*/
bool_t vxi_device_link_resp( XDR *pstXdrs , DEVICE_LINK_RESP_STRU *pstResp )
{
    if( !vxi_device_error_code( pstXdrs , &pstResp->error ) )
    {
        return FALSE;
    }
    if( !vxi_device_link( pstXdrs , &pstResp->id ) )
    {
        return FALSE;
    }
    if( !xdr_u_short( pstXdrs , &pstResp->port ) )
    {
        return FALSE;
    }
    if( !XDR_VXI_UINT( pstXdrs , &pstResp->recv_size ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 处理VXI设备的写操作参数
*/
bool_t vxi_device_write_para( XDR *pstXdrs , DEVICE_WRITE_PARA_STRU *pstWritePara)
{
    /*设备ID*/
    if( !vxi_device_link( pstXdrs , &pstWritePara->id ) )
    {
        return FALSE;
    }
    /*接口超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstWritePara->io_timeout ) )
    {
        return FALSE;
    }
    /*锁定超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstWritePara->lock_timeout ) )
    {
        return FALSE;
    }
    /*通信协议*/
    if( !vxi_device_flags( pstXdrs , &pstWritePara->flags ) )
    {
        return FALSE;
    }
    /*写的数据*/
    if( !xdr_bytes( pstXdrs , &pstWritePara->data.data_val , &pstWritePara->data.data_len , ~0 ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备写操作的响应处理
*/
bool_t vxi_device_write_resp( XDR *pstXdrs , DEVICE_WRITE_RESP_STRU *pstWriteResp)
{
    /*错误码值填充*/
    if ( !vxi_device_error_code( pstXdrs , &pstWriteResp->error ) )
    {
        return FALSE;
    }
    /*数据大小填充*/
    if( !XDR_VXI_UINT( pstXdrs , &pstWriteResp->size ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备读请求参数的获取
*/
bool_t  vxi_device_read_para( XDR *pstXdrs , DEVICE_READ_PARA_STRU *pstReadPara)
{
    /*获取设备ID*/
    if( !vxi_device_link( pstXdrs , &pstReadPara->id ) )
    {
        return FALSE;
    }
    /*获取读取的长度*/
    if( !XDR_VXI_UINT( pstXdrs , &pstReadPara->size ) )
    {
        return FALSE;
    }
    /*接口超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstReadPara->io_timeout ) )
    {
        return FALSE;
    }
    /*锁定超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstReadPara->lock_timeout ) )
    {
        return FALSE;
    }
    /*获取VXI设备的标识*/
    if( !vxi_device_flags( pstXdrs , &pstReadPara->flags ) )
    {
        return FALSE;
    }
    /*获取设备描述符*/
    if( !xdr_char( pstXdrs , &pstReadPara->termChar ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备读操作的响应报文组织
*/
bool_t vxi_device_read_resp( XDR *pstXdrs , DEVICE_READ_RESP_STRU *pstReadResp )
{
    /*填充错误码值*/
    if( !vxi_device_error_code( pstXdrs , &pstReadResp->error ) )
    {
        return FALSE;
    }
    /*填充响应码值*/
    if( !XDR_VXI_INT( pstXdrs , &pstReadResp->reason ) )
    {
        return FALSE;
    }
    /*填充数据*/
    if( !xdr_bytes( pstXdrs , &pstReadResp->data.data_val , &pstReadResp->data.data_len , ~0) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备的*STB?命令的处理接口
*/
bool_t vxi_device_stb_resp( XDR *pstXdrs , DEVICE_STB_RESP_STRU *pstStbResp )
{
    /*填充错误码值*/
    if( !vxi_device_error_code( pstXdrs , &pstStbResp->error ) )
    {
        return FALSE;
    }
    /*填充仪器的状态字节寄存器的值*/
    if( !xdr_u_char( pstXdrs , &pstStbResp->stb ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义通用的VXI设备命令参数获取接口
*/
bool_t vxi_device_generic_para( XDR *pstXdrs , DEVICE_GENERIC_PARA_STRU * pstGenPara )
{
    /*获取VXI设备ID*/
    if( !vxi_device_link( pstXdrs , &pstGenPara->id ) )
    {
        return FALSE;
    }
    /*获取VXI设备的标识*/
    if( !vxi_device_flags( pstXdrs , &pstGenPara->flags ) )
    {
        return FALSE;
    }
    /*获取VXI设备的锁定超时*/
    if( !XDR_VXI_UINT( pstXdrs , &pstGenPara->lock_timeout ) )
    {
        return FALSE;
    }
    /*获取接口超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstGenPara->io_timeout ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备锁定命令的参数获取接口
*/
bool_t vxi_device_lock_para( XDR *pstXdrs , DEVICE_LOCK_PARA_STRU *pstLockPara)
{
    /*获取设备ID*/
    if( !vxi_device_link( pstXdrs , &pstLockPara->id ) )
    {
        return FALSE;
    }
    /*获取当前锁定是否使能的标识*/
    if( !vxi_device_flags( pstXdrs , &pstLockPara->flags ) )
    {
        return FALSE;
    }
    /*获取锁定请求的超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstLockPara->lock_timeout ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义请求VXI设备进行远程连接的参数获取
*/
bool_t vxi_device_remote_para( XDR *pstXdrs , DEVICE_REMOTE_PARA_STRU *pstRemotePara )
{
    /*主机地址的处理*/
    if( !XDR_VXI_UINT( pstXdrs , &pstRemotePara->hostAddr ) )
    {
        return FALSE;
    }
    /*主机端口号的处理*/
    if( !XDR_VXI_UINT( pstXdrs , (VXI_UINT32*)&pstRemotePara->hostPort ) )
    {
        return FALSE;
    }
    /*远程RPC服务号处理*/
    if( !XDR_VXI_UINT( pstXdrs , &pstRemotePara->progNum ) )
    {
        return FALSE;
    }
    /*远程RPC服务的版本号*/
    if( !XDR_VXI_UINT( pstXdrs , &pstRemotePara->progVers ) )
    {
        return FALSE;
    }
    /*获取或者填充当前通信协议*/
    if( !vxi_device_family( pstXdrs , &pstRemotePara->progFamily ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义VXI设备打开或者关闭远程请求的参数获取函数
*/
bool_t  vxi_device_enable_srq_para( XDR *pstXdrs , DEVICE_ENABLE_SRQ_PARA_STRU *pstEnableSrqPara )
{
    /*获取设备ID*/
    if( !vxi_device_link( pstXdrs , &pstEnableSrqPara->id ) )
    {
        return FALSE;
    }
    /*获取使能标识*/
    if( !xdr_bool( pstXdrs , &pstEnableSrqPara->enable ) )
    {
        return FALSE;
    }
    /*获取主机发送的数据*/
    if( !xdr_bytes( pstXdrs , &pstEnableSrqPara->handle.handle_data , &pstEnableSrqPara->handle.handle_len , ~0) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义DOCMD命令参数的获取函数
*/
bool_t vxi_device_docmd_para( XDR *pstXdrs , DEVICE_DOCMD_PARA_STRU *pstDoCmdPara )
{
    /*获取VXI设备ID*/
    if( !vxi_device_link( pstXdrs , &pstDoCmdPara->id ) )
    {
        return FALSE;
    }
    /*获取通信标识*/
    if( !vxi_device_flags( pstXdrs , &pstDoCmdPara->flags ) )
    {
        return FALSE;
    }
    /*获取IO操作超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstDoCmdPara->io_timeout ) )
    {
        return FALSE;
    }
    /*获取锁定超时时间*/
    if( !XDR_VXI_UINT( pstXdrs , &pstDoCmdPara->lock_timeout ) )
    {
        return FALSE;
    }
    /*获取命令码值*/
    if( !XDR_VXI_INT( pstXdrs , &pstDoCmdPara->cmd ) )
    {
        return FALSE;
    }
    /*获取网络字节顺序*/
    if( !xdr_bool( pstXdrs , &pstDoCmdPara->network_order ) )
    {
        return FALSE;
    }
    /*获取数据缓冲区大小*/
    if( !XDR_VXI_INT( pstXdrs , &pstDoCmdPara->datasize ) )
    {
        return FALSE;
    }
    /*获取输入的数据信息*/
    if( !xdr_bytes( pstXdrs , &pstDoCmdPara->data_in.data_in_data , &pstDoCmdPara->data_in.data_in_len , ~0 ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 填充DOCMD命令对应的响应数据的函数
*/
bool_t vxi_device_docmd_resp( XDR *pstXdrs , DEVICE_DOCMD_RESP_STRU *pstDoCmdResp )
{
    /*填充错误码值*/
    if( !vxi_device_error_code( pstXdrs , &pstDoCmdResp->error ) )
    {
        return FALSE;
    }
    /*填充响应数据*/
    if( !xdr_bytes( pstXdrs , &pstDoCmdResp->data_out.data_out_data , &pstDoCmdResp->data_out.data_out_len , ~0 ) )
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * 定义获取中断请求参数的获取函数
*/
bool_t vxi_device_srq_para( XDR *pstXdrs , DEVICE_SRQ_PARA_STRU *pstSrqPara)
{
    /*获取参数*/
    if( !xdr_bytes( pstXdrs , &pstSrqPara->handle.handle_data , &pstSrqPara->handle.handle_len , ~0 ) )
    {
        return FALSE;
    }
    return TRUE;
}
