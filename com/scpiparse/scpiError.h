/*！
***此文件用excel自动生成
***2018年1月24日11:38
*/

//! 0        none err                                    无错误
//! -100     Command error                               命令错误 
//! -101     Invalid character                           非法字符
//! -102     Syntax error                                语法错误
//! -103     Invalid separator                           提取器无法分离
//! -104     Data type error                             数据类型错误
//! -105     GET not allowed                             获取数据失败
//! -108     Parameter not allowed                       参数错误
//! -109     Missing parameter                           参数个数不足
//! -110     Command header error                        命令头错误
//! -111     Header separator error                      命令头与参数分割错误
//! -112     Program mnemonic too long                   助记符太长，超过12字节
//! -113     Undefined header                            未定义的命令
//! -114     Header suffix out of range                  数字后缀错误
//! -115     Unexpected number of parameters             参数数量错误
//! -120     Numeric data error                          数值错误
//! -121     Invalid character in number                 无效的字符参数，例如：输入参数与其类型不对应
//! -123     Exponent too large                          指数太大
//! -124     Too many digits                             数字位数超过255位
//! -128     Numeric data not allowed                    数值合法但是机器不接受这个值
//! -130     Suffix error                                后缀错误
//! -131     Invalid suffix                              无效的后缀
//! -134     Suffix too long                             后缀太长
//! -138     Suffix not allowed                          
//! -140     Character data error                        字符数据错误
//! -141     Invalid character data                      无效的字符数据
//! -144     Character data too long                     字符数据的时间太长
//! -148     Character data not allowed                  字符数据不允许
//! -150     String data error                           字符串数据错误
//! -151     Invalid string data                         无效的字符串数据
//! -158     String data not allowed                     字符串数据不允许
//! -160     Block data error                            
//! -161     Invalid block data                          无效的数据块
//! -168     Block data not allowed                      数据块不允许
//! -170     Expression error                            表达错误
//! -171     Invalid expression                          无效的表达式
//! -178     Expression data not allowed                 表达数据不允许
//! -180     Macro error                                 宏错误
//! -181     Invalid outside macro definition            无效的宏定义外
//! -183     Invalid inside macro definition             无效的内部宏定义
//! -184     Macro parameter error                       
//! -200     Execution error                             执行错误
//! -201     Invalid while in local                      无效的在当地
//! -202     Settings lost due to rtl                    由于rtl设置了
//! -203     Command protected                           命令保护
//! -210     Trigger error                               引发错误
//! -211     Trigger ignored                             触发忽略
//! -212     Arm ignored                                 手臂被忽略
//! -213     Init ignored                                Init忽略
//! -214     Trigger deadlock                            引起死锁
//! -215     Arm deadlock                                手臂死锁
//! -220     Parameter error                             参数错误
//! -221     Settings conflict                           设置冲突
//! -222     Data out of range                           数据的范围
//! -223     Too much data                               太多的数据
//! -224     Illegal parameter value                     非法的参数值
//! -225     Out of memory                               内存不足
//! -226     Lists not same length                       列表不一样长
//! -230     Data corrupt or stale                       数据腐败或过期
//! -231     Data questionable                           数据有问题
//! -232     Invalid format                              无效的格式
//! -233     Invalid version                             无效的版本
//! -240     Hardware error                              硬件错误
//! -241     Hardware missing                            硬件缺失
//! -250     Mass storage error                          大容量存储器错误
//! -251     Missing mass storage                        失踪的大容量存储器
//! -252     Missing media                               失踪的媒体
//! -253     Corrupt media                               腐败的媒体
//! -254     Media full                                  媒体全面
//! -255     Directory full                              目录完整
//! -256     File name not found                         文件名没有找到
//! -257     File name error                             文件名称错误
//! -258     Media protected                             媒体的保护
//! -260     Expression error                            表达错误
//! -261     Math error in expression                    数学错误的表达
//! -270     Macro error                                 宏错误
//! -271     Macro syntax error                          宏观语法错误
//! -272     Macro execution error                       宏执行错误
//! -273     Illegal macro label                         非法宏的标签
//! -274     Macro parameter error                       Macro parameter error
//! -275     Macro definition too long                   宏定义的时间太长
//! -276     Macro recursion error                       Macro recursion error
//! -277     Macro redefinition not allowed              宏定义不允许
//! -278     Macro header not found                      宏头未找到
//! -280     Program error                               程序错误
//! -281     Cannot create program                       不能创建程序
//! -282     Illegal program name                        非法的程序名
//! -283     Illegal variable name                       非法的变量名
//! -284     Program currently running                   当前程序运行
//! -285     Program syntax error                        程序语法错误
//! -286     Program runtime error                       程序运行时错误
//! -290     Memory use error                            内存使用错误
//! -291     Out of memory                               内存不足
//! -292     Referenced name does not exist              引用名称不存在
//! -293     Referenced name already exists              引用名称已经存在
//! -294     Incompatible type                           不兼容的类型
//! -300     Device-specific error                       特定于设备的错误
//! -310     System error                                系统错误
//! -311     Memory error                                内存错误
//! -312     PUD memory lost                             前脚记忆丧失
//! -313     Calibration memory lost                     校准记忆丧失
//! -314     Save/recall memory lost                     保存/召回记忆丢失
//! -315     Configuration memory lost                   配置内存了
//! -320     Storage fault                               存储故障
//! -321     Out of memory                               内存不足
//! -330     Self-test failed                            自检失败
//! -340     Calibration failed                          校准失败
//! -350     Queue overflow                              队列溢出
//! -360     Communication error                         通信错误
//! -361     Parity error in program message             奇偶校验错误的程序信息
//! -362     Framing error in program message            框架程序误差信息
//! -363     Input buffer overrun                        输入缓冲区溢出
//! -365     Time out error                              超时错误
//! -400     Query error                                 查询错误
//! -410     Query INTERRUPTED                           查询中断
//! -420     Query UNTERMINATED                          查询无端接的
//! -430     Query DEADLOCKED                            查询陷入僵局
//! -440     Query UNTERMINATED after indefinite response查询后无端接的无限期的回应
//! -500     Power on                                    上电
//! -600     User request                                用户请求
//! -700     Request control                             请求控制
//! -800     Operation complete                          操作完成
typedef enum  
{
  scpi_err_none                 = 0        ,  //!
  scpi_err_empty                = -100     ,  //!function do not implement
  scpi_err_invalid_input_stream = -102     ,  //!invalid input chars
  scpi_err_conv                 = -104     ,  //!in convert
  scpi_err_missing_parameter    = -109     ,  //!
  scpi_err_operator             = -110     ,  //!invalid operator
  scpi_err_operator_match_fail  = -110     ,  //!match fail
  scpi_err_operand_lack         = -110     ,  //!operand lack
  scpi_err_num_data             = -120     ,  //!
  scpi_err_exponent_too_large   = -123     ,  //!
  scpi_err_no_val               = -128     ,  //!invalid value
  scpi_err_0_length             = -161     ,  //!stream length 0
  scpi_err_cmd_perform_fail     = -200     ,  //!命令执行失败
  scpi_err_parmeter_too_little  = -220     ,  //!参数太少
  scpi_err_parmeter_too_much    = -223     ,  //!参数太多
  scpi_err_conv_overflow        = -225     ,  //!
  scpi_err_format               = -232     ,  //!格式化错误
  scpi_err_flie_load_fail       = -256     ,  //!文件加载失败
  scpi_err_expression           = -260     ,  //!
  scpi_err_null_ptr             = -290     ,  //!type mismatch
  scpi_err_out_of_memory        = -291     ,  //!
  scpi_err_val_type_mismatch    = -294     ,  //!type mismatch
  scpi_err_index_overflow       = -350     ,  //!array index overflow
  scpi_err_intf_read            = -360     ,  //!read fail from interface
}scpiError;
