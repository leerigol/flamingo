#ifndef CFORMATFACTORY_H
#define CFORMATFACTORY_H
#include "cconverter.h"     /*!< converter */

using namespace scpi_parse;

class formatFactor
{
public:
    static scpiError createFactory(struTotalTable &item,
                                   QVector<struIndexesTable> *indexesTable,
                                   QVector<QSharedPointer<CConverter> > &ppConverter,
                                   QVector<QSharedPointer<CFormater> > &ppFormat
                                   );

#if 0  //! ppConverter和ppFormat使用的时智能指针　所以不需要手动释放
    static scpiError destroyFormat(QVector<CConverter *> &ppConverter,
                                   QVector<CFormater *> &ppFormat
                                   );
#endif

};

#endif // CFORMATFACTORY_H
