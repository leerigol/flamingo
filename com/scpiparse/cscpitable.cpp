#include "cscpitable.h"
#include "../../service/servdso/sysdso.h"

#include <QFile>
#include <QDomDocument>
#include <QDir>

#define SCPI_TABLE_CONFIG_NAME  "scpiConfig.xml"

/*!
 * \brief scpi_table_path scpi数据表的存放目录
 */
QString  scpiTable::scpi_table_path;

/*!
 * \brief scpiTableConfig
 * 说明  ：XML数据表的配置文件缓存
 * 值来源：从xml config文件中读取
 * 用 途 ：1、告诉scpiParser当前系统中有哪些XML数据表可用.
 *        2、记录每张表的名称和对应的SCPI的根节点名称
 */
QMap<QString,QString>     scpiTable::scpiTableConfig;

/*!
 * \brief TotalTable
 * 说明  ：SCPI命令表的缓存
 * 值来源：从XML文件中读取
 * 用 途 ：1、通过从端口获取的SCPI命令头索引该命令的详细信息，提供给解析器使用
 */
QVector<struTotalTable>   scpiTable::TotalTable;


/*!
 * \brief IndexesTable
 * 说 明 ：参数索引表的缓存
 * 值来源：从XML文件中读取
 * 用 途 ：1、为formatfactory提供命令的参数信息，创建对应的格式化器和转换器
 */
QVector<struIndexesTable> scpiTable::IndexesTable;


/*!
 * \brief scpiTable::CompatibleTable
 * 说明  : 兼容表缓存
 * 值来源: 从XML文件中读取
 * 用途  :
 */
QVector<struCompatibleTable> scpiTable::CompatibleTable;





/*!
 * \brief scpiTable::scpiTable  构造一张SCPI数据表，
 * * * * * * * * * * * * * * *  数据表的数据来自scpiTable类的静态数据区
 * * * * * * * * * * * * * * *  调用静态函数loadTableXml可以更新静态数据区数据。
 * \param name                  给数据表起一个名称.
 */
scpiTable::scpiTable(QString name)
{
    m_name = name;

    m_totalTable      = new QVector<struTotalTable>      (TotalTable)      ;
    m_indexesTable    = new QVector<struIndexesTable>    (IndexesTable)    ;
    m_compatibleTable = new QVector<struCompatibleTable> (CompatibleTable) ;

    Q_ASSERT( m_totalTable      != NULL );
    Q_ASSERT( m_indexesTable    != NULL );
    Q_ASSERT( m_compatibleTable != NULL );

    /*
    for(int i = 0; i< m_compatibleTable->count(); i++)
    {
        qDebug()<<"origin = "<<m_compatibleTable->at(i).originCmd;
        for(int j = 0; j< m_compatibleTable->at(i).CompatibleCmd.count(); j++)
        {
            qDebug()<<"c_head = "<<m_compatibleTable->at(i).CompatibleCmd.at(j).head;
            qDebug()<<"c_head = "<<m_compatibleTable->at(i).CompatibleCmd.at(j).format;
        }
    }
    */
}

/*!
 * \brief scpiTable::~scpiTable 销毁数据，释放空间。
 */
scpiTable::~scpiTable()
{
    if(m_totalTable   != NULL)
    {
      delete(m_totalTable);
      m_totalTable = NULL;
    }

    if(m_indexesTable != NULL)
    {
        delete(m_indexesTable);
        m_indexesTable = NULL;
    }

    if(m_compatibleTable != NULL)
    {
        delete(m_compatibleTable);
        m_compatibleTable = NULL;
    }
}

/*!
 * \brief scpiTable::loadTableConfig  加载xml配置文件到缓存
 * \return                            scpi_err_flie_load_fail | scpi_err_none
 */
scpiError scpiTable::loadTableConfig()
{

    scpi_table_path = sysGetResourcePath() + "/scpi/";

    QFile file(QString(scpi_table_path + SCPI_TABLE_CONFIG_NAME));
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        //!error config File 不存在
        qDebug()<<"scpi table config File losd failure";
        return  scpi_err_flie_load_fail;
    }

    QDomDocument config;
    config.setContent(&file);
    file.close();

    QDomElement docElem = config.documentElement();
    QDomNode node = docElem.firstChild();

    while(!node.isNull())
    {
        QDomElement item = node.toElement();    //! try to convert the node to an element.
        if(!item.isNull())
        {
            QString headName = item.tagName();   //!节点名称
            QString xmlName  = item.text();

            QDir xml_dir;
            if(!xml_dir.exists(QString(scpi_table_path + "%1").arg(xmlName)))
            {
               //! error XML文件不存在
//               qDebug()<< xmlName << "文件不存在";
//               return  scpi_err_flie_load_fail;
            }
            else
            {
               scpiTableConfig.insert(headName, xmlName);
            }
        }
      node = node.nextSibling();
    }
    return  scpi_err_none;
}

scpiError scpiTable::tableConfigFilter()
{
#if 0
    if(!sysHasLA())
    {
        scpiTableConfig.remove("la");
        qWarning()<<"!!!scpi filter: la";
    }

   if((!sysHasDG())  || (!sysCheckLicense(OPT_DG)))
    {
        scpiTableConfig.remove("sour");
        scpiTableConfig.remove("sour1");
        scpiTableConfig.remove("sour2");
        qWarning()<<"!!!scpi filter: DG";
    }
#endif
    return  scpi_err_none;
}

/*!
 * \brief scpiTable::loadScpiXml      加载SCPI数据表的xml文件,数据保存到静态成员IndexesTable 和 TotalTable 中
 * \param xmlName                     被加载的xml数据表名称
 * \return                            scpi_err_flie_load_fail | scpi_err_none
 */
scpiError scpiTable::loadTableXml(QString xmlName)
{
    /*! 加载XML到缓存 */
    QFile file(QString(scpi_table_path + "%1").arg(xmlName));
    if(!file.open( QIODevice::ReadOnly | QIODevice::Text  ))
    {
        //! error XML Open the failure
        qDebug()<< "error:"<< xmlName << "Open the failure";
        return  scpi_err_flie_load_fail;
    }

    QDomDocument table;
    table.setContent(&file);
    file.close();

    /*! 解析XML*/
    QDomElement docElem = table.documentElement(); //!返回文档的根元素
    QDomNode node = docElem.firstChild(); //!返回节点的第一个孩子

    struTotalTable    lsttv_item;
    struIndexesTable  lsitv_item;

    while(!node.isNull()) //!(TotalItem|IndexesItem)
    {
        QDomElement item = node.toElement();    //! try to convert the node to an element.
        if(!item.isNull())
        {
            QString name = item.tagName();   //!父节点名称

            if(name == "TotalItem")
            {
                QDomNode ldnv_Item1 = item.firstChild(); //!子节点 (head|service|..........)
                while(!ldnv_Item1.isNull())
                {
                    QDomElement ldev_item1 = ldnv_Item1.toElement();

                    if(!ldev_item1.isNull())
                    {
                        QString item1_name  =  ldev_item1.tagName();      //!子节点名称

                        if( item1_name == "head")
                        {
                            lsttv_item.head = ldev_item1.text();
                        }
                        else if(item1_name == "service")
                        {
                            lsttv_item.service = ldev_item1.text();
                        }
                        else if(item1_name == "cmd")
                        {
                            lsttv_item.cmd = ldev_item1.text().toInt();
                        }
                        else if(item1_name == "minSize")
                        {
                            lsttv_item.minSize = ldev_item1.text().toInt();
                        }
                        else if(item1_name == "indexes")
                        {
                            QDomNode ldnv_item2 = ldev_item1.firstChild();
                            while(!ldnv_item2.isNull())
                            {
                                QDomElement ldev_item2 = ldnv_item2.toElement();

                                lsttv_item.indexes << ldev_item2.text().toInt();

                                ldnv_item2 = ldnv_item2.nextSibling();
                            }
                        }
                        else if(item1_name == "unit")
                        {
                            QDomNode ldnv_item2 = ldev_item1.firstChild();
                            while(!ldnv_item2.isNull())
                            {
                                QDomElement ldev_item2 = ldnv_item2.toElement();

                                lsttv_item.unit << ldev_item2.text();

                                ldnv_item2 = ldnv_item2.nextSibling();
                            }
                        }
                    }
                    ldnv_Item1 = ldnv_Item1.nextSibling();
                }
                TotalTable << lsttv_item;
                lsttv_item.indexes.clear();
                lsttv_item.unit.clear();
                lsttv_item.cmd = 0;
                lsttv_item.service.clear();
                lsttv_item.head.clear();
            }
            else if(name == "IndexesItem")
            {
                QDomNode ldnv_Item1 = item.firstChild(); //!子节点 (number|direction|..........)
                while(!ldnv_Item1.isNull())
                {
                    QDomElement ldev_item1 = ldnv_Item1.toElement();

                    if(!ldev_item1.isNull())
                    {
                        QString item1_name  =  ldev_item1.tagName(); //!子节点名称
                        if( item1_name == "number")
                        {
                            lsitv_item.number = ldev_item1.text().toInt();
                        }
                        else if(item1_name == "direction")
                        {
                            lsitv_item.direction = (Direction)(ldev_item1.text().toInt());
                        }
                        else if(item1_name == "type")
                        {
                            lsitv_item.type = (PType)ldev_item1.text().toInt();
                        }
                        else if(item1_name == "enum")
                        {
                            QDomNode ldnv_item2 = ldev_item1.firstChild();
                            while(!ldnv_item2.isNull())
                            {   struEnum lsev_item;
                                QDomElement ldev_item2 = ldnv_item2.toElement();

                                lsev_item.str = ldev_item2.text();

                                ldnv_item2 = ldnv_item2.nextSibling();
                                ldev_item2 = ldnv_item2.toElement();


                                lsev_item.value = ldev_item2.text().toInt();

                                lsitv_item.enumTable<<lsev_item;

                                ldnv_item2 = ldnv_item2.nextSibling();
                            }
                        }
                    }
                    ldnv_Item1 = ldnv_Item1.nextSibling();
                }
                IndexesTable << lsitv_item;
                lsitv_item.number    = -1;
                lsitv_item.enumTable.clear();
            }
        }
        node = node.nextSibling();
    }
    return scpi_err_none;
}



scpiError scpiTable::loadCompatibleXml(QString xmlName)
{
    /*! 加载XML到缓存 */
    QFile file(QString(scpi_table_path + "compatible/" + "%1").arg(xmlName));
    if(!file.open( QIODevice::ReadOnly | QIODevice::Text  ))
    {
        qDebug()<< "error:compatible/"<< xmlName << "Open the failure";
        return  scpi_err_flie_load_fail;
    }
    QDomDocument compatible;
    compatible.setContent(&file);
    file.close();


    //!解析XML
    QDomElement docElem = compatible.documentElement(); //!0级元素
    QDomNode node1 = docElem.firstChild();               //!1级节点
    while(!node1.isNull())
    {
        //!item 级
        QDomElement docElem1 = node1.toElement();        //!1级元素
        QDomNode    node2    = docElem1.firstChild();          //!2级节点

        struCompatibleTable lsctv_item;
        while(!node2.isNull())
        {
            //!item内容 级
            QDomElement docElem2 = node2.toElement();   //!2级元素
            QString name = docElem2.tagName();          //!2级元素名称
            if(name == "origin")
            {
                lsctv_item.originCmd = docElem2.text();
            }
            else if(name == "compatible")
            {
                QDomNode    node3;    //!3级节点
                QDomElement docElem3; //!3级元素

                struCompatible lsctv_compatible;

                //!head
                node3    = docElem2.firstChild();
                docElem3 = node3.toElement();
                lsctv_compatible.head = docElem3.text();

                //!format
                node3    = node3.nextSibling();  //!切到下一个兄弟节点
                docElem3 = node3.toElement();
                lsctv_compatible.format = docElem3.text();

                lsctv_item.CompatibleCmd << lsctv_compatible;
            }
            node2 = node2.nextSibling();
        }
        CompatibleTable<< lsctv_item;
        lsctv_item.originCmd.clear();
        lsctv_item.CompatibleCmd.clear();
        node1 = node1.nextSibling();
    }
    return scpi_err_none;
}



/*!
 * \brief scpiTable::loadScpiXml  提供给外部的接口，加载XML文件.
 *  * * * * * * * * * * * * * *  构造新的scpiTable对象时需要先执行这个函数，更新静态区的数据。
 * \param key                     数据表根节点数据的简写名称，在配置表中保存了key与xml文件名的对应关系
 * \return                        scpi_err_flie_load_fail | scpi_err_none
 */
scpiError scpiTable::loadScpiXml(QString key)
{
    /*! 检查配置表 */
    if(scpiTableConfig.isEmpty())
    {
        if(loadTableConfig() != scpi_err_none)
        {
            return scpi_err_flie_load_fail;
        }
        else
        {
            //!命令表过滤器，根据系统环境，过滤一些指令表的加载。
            tableConfigFilter();
        }
    }

    /*! 根据key从配置表中得到需要加载的XML表的名称 */
    QMap<QString,QString>::const_iterator i = scpiTableConfig.find(key);
    if(i != scpiTableConfig.end() && i.key() == key)
    {
        IndexesTable.clear();
        TotalTable.clear();
        CompatibleTable.clear();
        loadTableXml(i.value());
        loadCompatibleXml(i.value());

//      Q_ASSERT( !IndexesTable.isEmpty());
//      Q_ASSERT( !TotalTable.isEmpty() );
        if((!IndexesTable.isEmpty()) && (!TotalTable.isEmpty()))
        {
             return scpi_err_none;
        }
    }

    return  scpi_err_flie_load_fail;
}
