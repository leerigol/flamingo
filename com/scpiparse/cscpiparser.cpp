#include "cscpiparser.h"
#include "../../fw/service/service.h"
#include "cformatfactory.h"
#include <QSharedPointer>
#include <QDebug>

#include "../../service/servdso/sysdso.h"
#include "../../service/servscpi/servscpidso.h"
#include "cscpieventreg.h"

namespace scpi_parse {

/*!
 * \var _dbgIntf
 */
CScpiInterface _dbgIntf;

/*!
 * \var CKeyWExtractor::m_pExtractor
 * \var CKeyWExtractor::mChunk
 * \var CKeyWExtractor::mChunkEnd
 * \var CKeyWExtractor::_C_TRIGGER_CNT
 */
CKeyWExtractor *CKeyWExtractor::m_pExtractor;
QList<QByteArray> CKeyWExtractor::mChunk;
bool CKeyWExtractor::mChunkEnd;
bool CKeyWExtractor::mEof;

const int CKeyWExtractor::_C_TRIGGER_CNT;

/*!
 * \var _scpiErr
 */
ScpiErrorLog _scpiErr(32);

ScpiErrorLog::ScpiErrorLog(int max)
    :queueCapacity(max)
{
    errQueue.clear();
}
/*!
 * \brief ScpiErrorLog::clear
 * \todo ?????????????????????????????????????
 */
void ScpiErrorLog::clear()
{
    errQueue.clear();
}
/*!
 * \brief ScpiErrorLog::logError
 * \param err
 */
void ScpiErrorLog::logError( scpiError err )
{
    if ( err != scpi_err_none )
    {
        append( (int)err );
    }
}
/*!
 * \brief ScpiErrorLog::logError
 * \param err
 */
void ScpiErrorLog::logError( int err )
{
    if ( err != 0 )
    {
        append( err );
    }
}
/*!
 * \brief ScpiErrorLog::append
 * \param err
 */
void ScpiErrorLog::append( int err )
{
    if ( errQueue.size()>=queueCapacity )
    {
        errQueue.removeFirst();
    }

    errQueue.append( err );
}

int ScpiErrorLog::getError()
{
    int err = scpi_err_none;
    if( !_scpiErr.errQueue.isEmpty() )
    {
        err = _scpiErr.errQueue.first();
        _scpiErr.errQueue.removeFirst();
    }
    return err;
}


/*!
 * \var CScpiExtractor::mCommandQueue
 * the comand queue
 */
CScpiCommandQueue CScpiExtractor::mCommandQueue;
QMutex            CScpiExtractor::CommandQueueMutex;

/*!
 * \brief CScpiInterface static m_pController
*/
CScpiController* CScpiInterface::m_pController = NULL;

/*!
 * \brief CScpiInterface::CScpiInterface
 * \param scpiType
 * \param id
 */
CScpiInterface::CScpiInterface( scpiIntfType scpiType, scpiIntfId id )
    : mType(scpiType), mId(id)
{
    m_inDataQueue.clear();
    m_InActiveBuff.clear();
}

CScpiInterface::~CScpiInterface()
{
    //    qDebug()<<__FILE__<<__LINE__<<__FUNCTION__;
    m_inDataQueue.clear();
    m_outDataQueue.clear();
    m_InActiveBuff.clear();
}

/*!
 * \brief CScpiInterface::isSame
 * \param sigA
 * \param sigB
 * \return
 * ????????????????????????????????????????????????????????????
 * - ??????????????????SOCKET?????????????????????
 * - ??????????????A?????????????????????????????????????B????????????
 *   ????A????????????????????????????????????B????????????????
 */
bool CScpiInterface::isSame( scpiIntfSig sigA, scpiIntfSig sigB )
{
    return sigA == sigB;
}

/*!
 * \brief CScpiInterface::setController 设置scpi控制器
 * \param pController
 */
void CScpiInterface::setController(CScpiController *pController)
{
    Q_ASSERT( pController != NULL );
    m_pController = pController;
}

/*!
 * \brief CScpiInterface::inSize
 * \return int
 *
 * ?????????????
 */
int CScpiInterface::inSize()
{
    return m_InActiveBuff.Data.size();
}


/*!
 * \brief CScpiInterface::read
 * \param pStreamIn
 * \param n
 * \return the read count
 * ????????????????????download
 */
int CScpiInterface::read( void *pStreamIn, int n )
{
    int readN;

    if ( pStreamIn == NULL )
    {
        return -1;
    }

    readN = (m_InActiveBuff.Data.size() > n) ? n : m_InActiveBuff.Data.size();

    memcpy( pStreamIn,m_InActiveBuff.Data.data(), readN );

    return readN;
}

bool CScpiInterface::nextInBuffisEmpty()
{
    bool b = true;

    inQueueMutex.lock();
    if(!m_inDataQueue.isEmpty())
    {
        m_InActiveBuff = m_inDataQueue.first();
        m_inDataQueue.removeFirst();

        b = false;
    }
    else
    {
        b = true;
    }

    inQueueMutex.unlock();

    return b;
}

void CScpiInterface::appendInData(QByteArray &data, bool eof)
{
    /*!写超时问题 debug******************************************************/
    QTime  starTime = QTime::currentTime();
    /*!******************************************************/

    if(!data.isEmpty())
    {   //! 新数据进入接口，关闭输出，但仅eof进入接口时，不需要关闭输出。
        outEnable = false;
    }

    bool b_onController = false;

    inData lbv_Data;
    lbv_Data.Data = data;
    lbv_Data.eof  = eof;

    inQueueMutex.lock();
    SCPI_INTERFACE_DEBUG("append:%s", data.data());

    b_onController = m_inDataQueue.isEmpty();

    m_inDataQueue.append(lbv_Data);
#ifdef _QUEUE_SIZE_MAX
    if(_QUEUE_SIZE_MAX < m_inDataQueue.count())
    {
        m_inDataQueue.removeLast();
    }
#endif

#ifdef _QUEUE_DEBUG
    int queueCount = m_inDataQueue.count();
#endif


    inQueueMutex.unlock();

    if(b_onController)
    {
        m_pController->onReceive( this );
    }

#ifdef  _QUEUE_DEBUG
    qDebug()<<__FILE__<<__LINE__<<"Extractor Queue size:"<<queueCount;
#endif

    /*!写超时问题 debug******************************************************/
    int timeOut = starTime.msecsTo(QTime::currentTime());
    if(timeOut > 1000)
    {
        qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<timeOut;
        //Q_ASSERT(false);
    }
    /*!******************************************************/
}

void CScpiInterface::appendppFormater(QVector<QSharedPointer<CFormater> > ppFormater, bool eof)
{
    SCPI_PARSER_DEBUG("append Formater count: %d",ppFormater.count());
    //!??????pFormaterData
    outQueueMutex.lock();

    outEnable = false;//有新数据进入队列，需要禁止接口输出。
    //qDebug()<<__FILE__<<__LINE__<<"Out enable:"<<outEnable;

    if(outEof)
    {   //! 当接口受到eof后准备放入新数据时，要先清空输出队列。
        m_outDataQueue.clear();
    }

    //!????????????
    outData lodv_data;

    for(int i = 0; i <ppFormater.count(); i++)
    {
        ppFormater[i]->startup();

        lodv_data.pFormaterData = ppFormater[i];

        lodv_data.additionalOutData = ",";

        m_outDataQueue.append(lodv_data);
    }
    m_outDataQueue.last().additionalOutData = eof? "\n" : ";";

    outEof = eof;
    //!?????????pFormaterData
    outQueueMutex.unlock();

    //!??SCPI ???????????????????????????
    //qDebug()<<__FILE__<<__LINE__<<"scpi_event_output_queue"<<QTime::currentTime();
    sysSetScpiEvent(scpi_event_output_queue,1);
}

void CScpiInterface::inPut(QByteArray &data, bool eof)
{
    appendInData(data, eof);
}

void CScpiInterface::outPut(QVector< QSharedPointer<CFormater> > ppFormater, bool eof)
{
    appendppFormater(ppFormater, eof);
}

quint32 CScpiInterface::getOutData(quint8 *buf, const quint32 &len, bool &eom)
{
    QByteArray outData;
    qint64     retSize              = 0;
    qint64     bufUsedSize          = 0;
    qint64     bufAvailableSize     = len;

    if(!outEnable)
    {
        QThread::msleep(2);
        return 0;
    }

    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<"-----------------------"<<len;
    outQueueMutex.lock(); //! lock outDataQueue

    while (1)
    {
        Q_ASSERT( !m_outDataQueue.isEmpty() );

        QSharedPointer<CFormater>  &pFormaterData = m_outDataQueue.first().pFormaterData;
        Q_ASSERT( !pFormaterData.isNull() );

        //! get additional data:  ["\n", "," ";" ....]
        QByteArray &additionalOutData = m_outDataQueue.first().additionalOutData;


        //!  retSize  must be less than or equal to bufAvailableSize;
        //!  getOutData() Perform data after the reduction
        retSize = pFormaterData->getOutData(outData, bufAvailableSize);

        if(retSize > 0)
        {
            Q_ASSERT(outData.size()   >= retSize);
            Q_ASSERT(bufAvailableSize >= retSize);

            memcpy(buf + bufUsedSize, outData.data(), retSize);

            bufAvailableSize -= retSize;
            bufUsedSize      += retSize;
            Q_ASSERT( bufAvailableSize >= 0 );
            Q_ASSERT( bufUsedSize      <= len );
        }

        //!formater data is empty, to add additional data;
        if ( pFormaterData->getOutDataLen() <= 0 )
        {
            retSize = qMin( bufAvailableSize, (qint64)additionalOutData.size() );

            memcpy(buf + bufUsedSize, additionalOutData.data(), retSize);

            additionalOutData.remove(0, retSize);

            bufAvailableSize -= retSize;
            bufUsedSize      += retSize;
            Q_ASSERT( bufAvailableSize >= 0 );
            Q_ASSERT( bufUsedSize      <= len );
        }

        //! (pFormaterData is empty) && (additionalOutData is empty) ----> (m_outDataQueue remove first)
        if( (pFormaterData->getOutDataLen() <= 0) && (additionalOutData.size() <= 0) )
        {
            m_outDataQueue.removeFirst();
        }

        if(m_outDataQueue.isEmpty()) //! finish
        {
            outEnable = false;
            eom = true;
            break;
        }

        if(bufAvailableSize <= 0)    //! unfinished
        {
            outEnable = true;
            eom = false;
            break;
        }
    }

    outQueueMutex.unlock();
    return bufUsedSize;
}


bool CScpiInterface::outisValid()
{
    outQueueMutex.lock();
    outEnable = false;

    if(outEof && (!m_outDataQueue.isEmpty()))
    {
        int i = 0;
        for(; i < m_outDataQueue.count(); i++)
        {
            if( m_outDataQueue.at(i).pFormaterData->isWaiting())
            {
                break;
            }
        }

        if(i == m_outDataQueue.count())
        {
            outEnable = true;
        }
    }

    bool ret = outEnable;
    outQueueMutex.unlock();

    //qDebug()<<__FILE__<<__LINE__<<"Out enable:"<<outEnable;
    return ret;
}

quint8 CScpiInterface::getStbReg()
{
    eventReg i = RegSTB.getReg();
    if( this->m_outDataQueue.isEmpty() )
    {
        i &= (~(1<<scpi_event_b4));
    }
    else
    {
        i &= (1<<scpi_event_b4);
    }
    return (quint8)i;
}

void CScpiInterface::clearOutQueue()
{
    outQueueMutex.lock();
    outEnable = false;
    m_outDataQueue.clear();
    outQueueMutex.unlock();
}

void CScpiInterface::clearInQueue()
{
    inQueueMutex.lock();
    m_InActiveBuff.clear();
    m_inDataQueue.clear();
    inQueueMutex.unlock();
}

void CScpiInterface::onRequestOut()
{
    SCPI_INTERFACE_DEBUG("interface %d :outEnable = %d", this->mType, outEnable);
}


/*!
 * \brief CScpiInterface::srqRespond??? ???srq?????????????????????????????????????????????
 * \param stbReg??????????????????????????????????????????STB???????????
 * ????????????????????????????????????????
 */
void CScpiInterface::srqRespond(quint8 stbReg)
{
    Q_UNUSED(stbReg)
    SCPI_INTERFACE_DEBUG("SRQ:%d",stbReg);
}

/*!
 * \brief CScpiCommand::CScpiCommand
 * constructor
 */
CScpiCommand::CScpiCommand()
{
    m_pIO = NULL;
}

/*!
 * \brief CScpiCommand::operator =
 * \param cmd
 * \return
 * assign QList<QByteArray>
 */
CScpiCommand & CScpiCommand::operator=( QList<QByteArray> &cmd )
{
    clear();
    append( cmd );

    return *this;
}

/*!
 * \brief CScpiCommand::setIO
 * \param pIO
 * \return
 * ?????????????????
 * ???????????????????????????????????????????????????
 * ????????????????????????????????
 */
scpiError CScpiCommand::setIO( CScpiInterface *pIO )
{
    assert_null(pIO);

    m_pIO = pIO;

    return scpi_err_none;
}
CScpiInterface *CScpiCommand::getIO()
{
    return m_pIO;
}

bool CScpiCommand::getEof() const
{
    return mEof;
}

void CScpiCommand::setEof(bool eof)
{
    mEof = eof;
}

/*!
 * \brief CKeyWExtractor::isChunkComplete
 * \return
 * chunk????????????
 * ????chunk?????????????????????[???????]
 */
bool CKeyWExtractor::isChunkComplete()
{
    return mChunkEnd;
}

/*!
 * \brief CKeyWExtractor::clrChunk
 * clear chunk
 */
void CKeyWExtractor::clrChunk()
{
    mChunkEnd = false;
    mChunk.clear();
}

/*!
 * \brief CKeyWExtractor::setExtractor
 * \param pNxt
 * \return
 */
scpiError CKeyWExtractor::setExtractor( CKeyWExtractor *pNxt )
{
    assert_null( pNxt );

    CKeyWExtractor::m_pExtractor = pNxt;

    return scpi_err_none;
}
/*!
 * \brief CKeyWExtractor::getExtractor
 * \return
 * get the current active extractor
 */
CKeyWExtractor *CKeyWExtractor::getExtractor()
{
    return CKeyWExtractor::m_pExtractor;
}

/*!
 * \brief CKeyWExtractor::addTrigger
 * \param byteAry
 * \param pNext
 * \return
 * ??????????????????????????
 * ??????????????
 * ?????????????????????????????????
 */
scpiError CKeyWExtractor::addTrigger( QByteArray byteAry, CKeyWExtractor *pNext )
{
    //! index verify
    if ( mTriggerCnt >= CKeyWExtractor::_C_TRIGGER_CNT  )
    {
        return scpi_err_index_overflow;
    }

    assert_null( pNext );

    //! add path
    mTrigger[ mTriggerCnt ] = byteAry;
    m_pNext[ mTriggerCnt ] = pNext;

    mTriggerCnt++;

    return scpi_err_none;
}
/*!
 * \brief CKeyWExtractor::addTrigger
 * \param str
 * \param pNext
 * \return
 * ??????
 */
scpiError CKeyWExtractor::addTrigger( const char *str, CKeyWExtractor *pNext )
{
    return addTrigger( QByteArray(str), pNext );
}

void CKeyWExtractor::setIdle( CKeyWExtractor *pIdle )
{
    m_pIdle = pIdle;
}

/*!
 * \brief CKeyWExtractor::onInput
 * \param cInput
 * \return
 * ??????????????????????
 */
scpiError CKeyWExtractor::onInput( CInput &input )
{
    int i;

    //! loop for all trigger
    for ( i = 0; i < mTriggerCnt; i++ )
    {
        //! no match
        if ( mTrigger[i].indexOf( (char)input.ucInput ) < 0 )
        {
            continue;
        }

        //! find && change
        scpiError err = onChange( input, m_pNext[i] );
        if ( err != scpi_err_none )
        {
            return err;
        }
    }

    return scpi_err_none;
}
/*!
 * \brief CKeyWExtractor::onChange
 * \param cInput
 * \param pNext
 * \return
 * ?????????????:
 * 1. ?????????????onExit
 * 2. ???????????????onEnter
 */
scpiError CKeyWExtractor::onChange( CInput &input,
                                    CKeyWExtractor *pNext,
                                    void *ptr )
{
    assert_null( pNext );

    onExit( input );

    CKeyWExtractor::m_pExtractor = pNext;

    pNext->onEnter( input, ptr );

    return scpi_err_none;
}

scpiError CKeyWExtractor::onShift( CInput &input,
                                   CKeyWExtractor *pNext,
                                   void *ptr)
{
    assert_null( pNext );

    CKeyWExtractor::m_pExtractor = pNext;

    pNext->onEnter( input, ptr );

    return scpi_err_none;
}

/*!
 * \brief CKeyWExtractor::onEnter
 * \param cInput
 * \return
 */
scpiError CKeyWExtractor::onEnter( CInput &/*input*/, void */*ptr*/ )
{
    return scpi_err_none;
}

/*!
 * \brief CKeyWExtractor::onExit
 * \param cInput
 * \return
 */
scpiError CKeyWExtractor::onExit( CInput &/*input*/ )
{
    return scpi_err_none;
}

void CKeyWExtractor::rst()
{
    CKeyWExtractor::m_pExtractor = m_pIdle;

    clrChunk();
}

/*!
 * \brief onEnter
 *
 * ????????
 */
scpiError CIdleExtractor::onEnter( CInput &input, void */*ptr*/ )
{
    //! chunk end
    if ( mChunk.size() > 0 )
    {
        mChunkEnd = true;
    }

    mEof = ((input.ucInput == '\n')||(input.ucInput == '\r')||input.eof )? true : false;
    return scpi_err_none;
}

/*!
 * \brief COperatorExtractor::onInput
 * \param cInput
 * \return
 * ???????????????????
 */
scpiError COperatorExtractor::onInput( CInput &input )
{
    scpiError err;

    //! filter
    err = CKeyWExtractor::onInput( input );
    if ( err != scpi_err_none )
    {
        return err;
    }

    //! if no change, append the char
    //! if the change happened, the operator has been exported in
    //! the onExit
    mOperator.append( input.ucInput );

    return scpi_err_none;
}

/*!
 * \brief COperatorExtractor::onEnter
 * \param cInput
 * \return
 * The following tasks:
 * 1. clear the chunk
 * 2. clear the operator
 * 3. init the operator with cInput
 */
scpiError COperatorExtractor::onEnter( CInput &input, void */*ptr*/ )
{
    //! chunk clear
    mChunk.clear();
    mChunkEnd = false;

    //! clear
    mOperator.clear();

    //! add the first one
    mOperator.append( input.ucInput );

    return scpi_err_none;
}

/*!
 * \brief COperatorExtractor::onExit
 * \param cInput
 * \return
 * ???????????????????????????????????????????chunk???
 */
scpiError COperatorExtractor::onExit( CInput &/*input*/ )
{
    mChunk.append( mOperator );

    return scpi_err_none;
}

/*!
 * \brief COperandExtractor::onInput
 * \param cInput
 * \return
 * ???????????????????
 */
scpiError COperandExtractor::onInput( CInput &input )
{
    scpiError err;

    err = CKeyWExtractor::onInput( input );
    if ( err != scpi_err_none )
    {
        return err;
    }

    //! if no change, append the char
    mOperand.append( input.ucInput );

    //! status change
    if ( mOperand.size() == 2 )
    {
        //! def arb
        if ( mOperand.at(0) == '#'
             && mOperand.at(1) > '0'
             && mOperand.at(1) <='9' )
        {
            onShift( input, m_pNext[2] );
        }
        //! indef arb
        else if ( mOperand.at(0) == '#'
                  && mOperand.at(1) == '0'
                  )
        {}
        else
        {
            //            return scpi_err_invalid_input_stream;
        }
    }

    return scpi_err_none;
}

/*!
 * \brief COperandExtractor::onEnter
 * \param cInput
 * \return
 * ???????????????????
 */
scpiError COperandExtractor::onEnter( CInput &input, void */*ptr*/ )
{
    mOperand.clear();

    mOperand.append( input.ucInput );

    return scpi_err_none;
}

/*!
 * \brief COperandExtractor::onExit
 * \param cInput
 * \return
 */
scpiError COperandExtractor::onExit( CInput &/*input*/ )
{
    mChunk.append( mOperand );

    return scpi_err_none;
}

scpiError COperandDefArbHeaderExtractor::onEnter( CInput &input, void */*ptr*/ )
{
    quint8 cInput = input.ucInput;

    if ( cInput > '0' && cInput <='9' )
    {
        blockWidth = cInput - '0';
        mOperand.clear();

        return scpi_err_none;
    }
    else
    {
        return scpi_err_invalid_input_stream;
    }
}

scpiError COperandDefArbHeaderExtractor::onInput( CInput &input )
{
    int blockLength;
    mOperand.append( input.ucInput );

    //! get block data
    if ( mOperand.size() ==  blockWidth )
    {
        QByteArray aryBlock;
        bool ok;

        aryBlock = mOperand.mid(0,blockWidth);

        blockLength = aryBlock.toInt( &ok );

        if ( !ok || blockLength < 0 )
        {
            return scpi_err_invalid_input_stream;
        }

        //! to arb data
        onShift( input, m_pNext[1], &blockLength );

        return scpi_err_none;
    }

    return scpi_err_none;
}

scpiError COperandArbExtractor::onEnter( CInput &/*input*/, void *ptr )
{
    if ( NULL != ptr )
    {
        blockLength = *(int*)ptr;
        mOperand.clear();

        return scpi_err_none;
    }
    else
    {
        return scpi_err_invalid_input_stream;
    }
}

scpiError COperandArbExtractor::onExit(CInput &input)
{
    if ( blockLength != mOperand.size() )
    {
        mChunk.clear();
    }
    else
    {
        COperandExtractor::onExit(input);
    }
    return scpi_err_none;
}

scpiError COperandArbExtractor::onInput(CInput &input)
{
    mOperand.append( input.ucInput );

    if ( mOperand.size() == blockLength )
    {
        onChange( input, m_pNext[0], NULL );
    }

    return scpi_err_none;
}

/*!
 * \brief CScpiExtractor::init
 * Link the extractor. All the extractors can make a state machine.
 * 1. set the filters for each extractor
 * 2. link them by pre-post order
 */
void CScpiExtractor::init()
{


    //! \todo the first ':' may be omited
    mIdle.addTrigger( "*:"
                      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                      "_",
                      &mOperator );

    mOperator.addTrigger(" ", &mPreOperand );
    mOperator.addTrigger(";\n\r", &mIdle );

    //! \todo the operand filter
    mPreOperand.addTrigger("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                           "0123456789"
                           "._+-*/"
                           "#"
                           "\"\'"
                           " ()",
                           &mOperand
                           );
    mPreOperand.addTrigger(";\n\r", &mIdle );

    mPstOperand.addTrigger(",", &mPreOperand );
    mPstOperand.addTrigger(";\n\r", &mIdle );

    //! operand
    mOperand.addTrigger(",", &mPreOperand );
    mOperand.addTrigger(";\n\r", &mIdle );

    mOperand.addTrigger("", &mDefArbHeaderOperand );

    //! arb header
    mDefArbHeaderOperand.addTrigger("", &mPreOperand );
    mDefArbHeaderOperand.addTrigger("", &mArbOperand );

    //! arb
    mArbOperand.addTrigger("", &mPstOperand );

    //! default status
    mIdle.setIdle( &mIdle );

    //! init to idle
    CKeyWExtractor::setExtractor( &mIdle );
}

/*!
 * \brief CScpiExtractor::getCommand
 * \return
 * only one command
 */
bool CScpiExtractor::getCommand(CScpiCommand &outCommand)
{
    int err = false;
    CommandQueueMutex.lock();

    if( !mCommandQueue.isEmpty())
    {
        outCommand =  mCommandQueue[0];
        mCommandQueue.removeFirst();
        err =  true;
    }
    CommandQueueMutex.unlock();
    return err;
}

/*!
 * \brief CScpiExtractor::getCommandQueue
 * \return
 * command queue
 */
CScpiCommandQueue &CScpiExtractor::getCommandQueue()
{
    return mCommandQueue;
}

/*!
 * \brief CScpiExtractor::cls
 * clear input queue
 * \todo clear output queue
 * \todo make the executor exit block status ( if blocked )
 */
void CScpiExtractor::cls()
{
    CommandQueueMutex.lock();
    mCommandQueue.clear();
    CommandQueueMutex.unlock();

    CKeyWExtractor::setExtractor( &mIdle );
    CKeyWExtractor::clrChunk();
}

/*!
 * \brief CScpiExtractor::receive
 * \param pStream
 * \param n
 * \param pInterface
 * \return
 * \sa receivePackage
 * Receive a complete stream, no more data in the following data.
 * The difference:
 * + with complete stream, the status is idle at the end_of_end
 * + with normal package, the status is unknown, as the stream in is
 *   not complete. For example, ":chan1:" is the first package, it is
 *   in operator. Then received "disp ", it goes to operand, and waits
 *   for more data.
 */
scpiError CScpiExtractor::receive( unsigned char *pStream, int n,
                                   CScpiInterface *pInterface )
{
    scpiError err;

    //! do as a package
    err = receivePackage( pStream, n );
    if ( err != scpi_err_none )
    {
        return err;
    }

    err = packageEnd( pInterface );

    return err;
}

scpiError CScpiExtractor::receive( char *pStream, CScpiInterface *pInterface )
{
    return receive( (unsigned char*)pStream, strlen(pStream), pInterface );
}

/*!
 * \brief CScpiExtractor::receivePackage
 * \param pStream
 * \param n
 * \param pInterface
 * \return
 * \sa receive
 * receive only package, which implies that there may be more
 * data received.
 * The influence:
 * 1. The status after the last data is not idle
 */
scpiError CScpiExtractor::receivePackage( unsigned char *pStream, int n,
                                          CScpiInterface *pInterface )
{
    CKeyWExtractor *pPre;
    scpiError err;
    CInput input;

    assert_null( pInterface );

    //! interface changed
    //! return to idle
    if ( !CScpiInterface::isSame(mIntfSig, pInterface->getSig()) )
    {
        mIntfSig = pInterface->getSig();
        CKeyWExtractor::setExtractor( &mIdle );
    }

    //! do extract
    for ( int i = 0; i < n; i++ )
    {
        pPre = CKeyWExtractor::getExtractor();
        assert_null( pPre );

        //input.eof = pInterface->getEof();
        input.eof = false;
        input.ucInput = pStream[i];
        err = pPre->onInput( input );
        if ( err != scpi_err_none )
        {
            //! rst to idle
            pPre->rst();
            return err;
        }

        //! export chunk on complete
        err = exportChunk( pInterface );
        if ( err != scpi_err_none )
        {
            return err;
        }
    }

    //! eof
    if ( pInterface->getEof() )
    {
        err = packageEnd( pInterface );
        if ( err != scpi_err_none )
        {
            return err;
        }
    }

    return scpi_err_none;
}
scpiError CScpiExtractor::receivePackage( char *pStream, CScpiInterface *pInterface )
{
    return receivePackage( (unsigned char*)pStream, strlen(pStream), pInterface );
}

/*!
 * \brief receivePackage
 * \param pInterface
 * \return
 * receive a packet from interface
 */
scpiError CScpiExtractor::receivePackage( CScpiInterface *pInterface )
{
    scpiError err;
    assert_null( pInterface );

    unsigned char *pStream;
    int n;

    //! get data
    err = unBundle( pInterface, &pStream, &n );
    if ( err == scpi_err_0_length )
    {
        //! ?????????????????????????????????????EOF???
        //! ????????????????????????
    }
    else if ( err != scpi_err_none )
    {
        return err;
    }

    //! do receive
    err = receivePackage( pStream, n, pInterface );

    //! recollect
    if ( n > 0 && pStream != NULL )
    {
        delete []pStream;
        pStream = NULL;
    }

    return err;
}

/*!
 * \brief receive
 * \param pInterfce
 * \return
 * receive a complete packet
 */
scpiError CScpiExtractor::receive( CScpiInterface *pInterface )
{
    assert_null( pInterface );

    scpiError err;

    //! do as a package
    err = receivePackage( pInterface );
    if ( err != scpi_err_none )
    {
        return err;
    }

    //! return to idle
    err = packageEnd( pInterface );

    return err;
}

/*!
 * \brief CScpiExtractor::onNewCommand
 * \param newCmd
 * \return
 * \todo proc the command
 * ?????????????????????
 */
scpiError CScpiExtractor::onNewCommand( CScpiCommand &/*newCmd*/ )
{
    return scpi_err_none;
}

/*!
 * \brief CScpiExtractor::exportChunk
 * \param pInterface
 * \return
 * ???????????????????chunk
 * \note ??????????????????
 */
scpiError CScpiExtractor::exportChunk( CScpiInterface *pInterface )
{
    scpiError err;

    assert_null( pInterface );

    //! enter in to idle
    if ( CKeyWExtractor::isChunkComplete()
         )
    {
        //! get the command array
        mCommand = CKeyWExtractor::mChunk;

        //! set interface
        mCommand.setIO( pInterface );

        //! set Eof
        mCommand.setEof(CKeyWExtractor::mEof);

        CKeyWExtractor::clrChunk();

        //! do the command
        err = onNewCommand( mCommand );
        if ( err != scpi_err_none )
        {
            return err;
        }
    }

    return scpi_err_none;
}

/*!
 * \brief CScpiExtractor::unBundle
 * \param pInterface
 * \param ppStream
 * \param pLen
 * \return
 * ???????????????????????????????????????????
 * \note memory alloc in the function, recollect the memory by ppStream
 * in the caller.
 */
scpiError CScpiExtractor::unBundle( CScpiInterface *pInterface,
                                    unsigned char **ppStream, int *pLen )
{
    assert_null( pInterface );

    //! init
    *ppStream = NULL;
    *pLen = 0;

    //! get size
    int n = pInterface->inSize();
    if ( n < 1 )
    {
        return scpi_err_0_length;
    }

    //! read data
    unsigned char *pStream = new unsigned char[ n ];
    assert_null_alloc( pStream );

    if ( n != pInterface->read( pStream, n ) )
    {
        delete []pStream;
        return scpi_err_intf_read;
    }

    //! export addr and size
    *ppStream = pStream;
    *pLen = n;
    return scpi_err_none;
}

/*!
 * \brief CScpiExtractor::packageEnd
 * \param pIntf
 * \return
 * return to idle at package end
 */
scpiError CScpiExtractor::packageEnd( CScpiInterface *pIntf )
{
    CKeyWExtractor *pNxt;
    scpiError err;
    CInput input;

    //! current
    pNxt = CKeyWExtractor::getExtractor();

    //! package end enter the idle
    if ( pNxt != NULL && pNxt!= &mIdle )
    {
        input.eof = true;
        err = pNxt->onChange( input, &mIdle );

        err = exportChunk( pIntf );
        if ( err != scpi_err_none )
        {
            return err;
        }
    }
    else
    {
        err = scpi_err_none;
    }

    return err;
}


/*!
 * \brief s_pListTable  scpi????????????????
 */
QList< scpiTable *> CScpiParser::s_pListTable;
/*!
 * \brief CScpiParser::CScpiParser  ?????????
 */
CScpiParser::CScpiParser()
{
    m_pScpiTable = NULL;
}
/*!
 * \brief CScpiParser::doParse
 * \return
 * start parse for the input queue
 * \note getCommandQueue return the reference
 */
scpiError CScpiParser::doParse( )
{
    CScpiCommand scpiCmd;
    scpiCmd.clear();

    while(CScpiExtractor::getCommand(scpiCmd))
    {
#ifndef  _PARSER_OFF
        doTranslate(scpiCmd);
        scpiCmd.clear();
#else
        //qDebug()<<__FILE__<<__LINE__<<"Parser Cmd:"<<scpiCmd.at(0);
        scpiCmd.clear();
#endif
    }
    return scpi_err_none;
}


/*!
 * \brief CScpiParser::loadScpiTable  ???????????????????????????????????????????????????????????????????
 * *********************************  ?????????????????????????????????????????????????XML?????????????????????????????????????????????
 * \param scpiCmd                     ??????????????????,???????????????????????XML????
 * \return                            scpi_err_operator | scpi_err_none
 * *????
 * *??????????xml?????????????????????????????????????????????????????????????????????????????????????
 * *???????? scpi_err_none ??? ?????????????????????????????????? .
 */
scpiError CScpiParser::loadScpiTable(CScpiCommand &scpiCmd)
{
    static QString historykey;

    //!???????????????????????
    QRegExp reRoot("(\\*?)([A-Z_][A-Z_0-9]{1,2}[B-DF-HJ-NP-TV-Z]?)(?:[A-Z_]*)([0-9]*)",Qt::CaseInsensitive);
    int pos = reRoot.indexIn(scpiCmd[0],0);
    if(pos == (-1))
    {
        SCPI_ERROR_DEBUG("There is no SCPI table!");
        return scpi_err_operator;
    }

    QString key;
    if(!reRoot.cap(1).isEmpty())
    {
        key = "sys_"+(reRoot.cap(2) + reRoot.cap(3)).toLower(); //!???
    }
    else
    {
        key = (reRoot.cap(2) + reRoot.cap(3)).toLower(); //!???
    }

    if(key != historykey)
    {
        historykey = key;

        //!?????????????????????????????????????
        for(int i = 0; i < s_pListTable.count(); i++)
        {
            if(s_pListTable.at(i) != NULL)
            {
                if(key == (s_pListTable.at(i)->m_name))
                {
                    m_pScpiTable = s_pListTable.at(i);
                    Q_ASSERT(m_pScpiTable != NULL);
                    Q_ASSERT(m_pScpiTable->m_totalTable != NULL);
                    Q_ASSERT(m_pScpiTable->m_indexesTable != NULL);
                    Q_ASSERT(m_pScpiTable->m_compatibleTable != NULL);
                    return scpi_err_none;
                }
            }
        }

        /*!---------------------debug---------------------------*/
        //QTime  starTime = QTime::currentTime();
        /*!----------------------------------------------------*/
        //!从磁盘加载xml
        if( scpiTable::loadScpiXml(key) !=  scpi_err_none)
        {
            return scpi_err_none;
        }

        //!????????,???????????????
        s_pListTable.insert(0, new scpiTable(key)); 
        //qDebug()<<__FILE__<<__LINE__<<key<<starTime.msecsTo(QTime::currentTime());

        //!存在从XML读取失败的情况下，数据表的内容为空，所以需要检查数据表的有效性
        if(s_pListTable.first()->m_totalTable->isEmpty())
        {
            Q_ASSERT(false);
            s_pListTable.removeFirst();
            return scpi_err_flie_load_fail;
        }

        m_pScpiTable = s_pListTable.first();
        Q_ASSERT(m_pScpiTable != NULL);
        Q_ASSERT(m_pScpiTable->m_totalTable != NULL);
        Q_ASSERT(m_pScpiTable->m_indexesTable != NULL);
        Q_ASSERT(m_pScpiTable->m_compatibleTable != NULL);
        //!????????????????????? ?????????????????
        while(s_pListTable.count() > s_BUFF_SIZE)
        {
            if(s_pListTable.last() != NULL)
            {
                delete(s_pListTable.last());
                s_pListTable.removeLast();
            }
        }
    }

    return scpi_err_none;
}

/*!
 * \brief CScpiParser::dofind   ???????????????????:??????????????? ?????????? ????????????????????????
 * \param strOperator           ?????????????????
 * \param item                  struTotalTable??????????,??????????????
 * \return                      scpi_err_operator_match_fail | scpi_err_none
 */
scpiError CScpiParser::dofind(const QByteArray &strOperator, struTotalTable &item)
{

    struTotalTable  outItem;
    static QString  parentHeld;

    QString completeHeld = strOperator;

    outItem = findItem(completeHeld, parentHeld);
    if(!outItem.head.isEmpty())
    {
        //!ok
        item = outItem;
        return scpi_err_none;
    }
    else if(!parentHeld.isEmpty()) //!Default nodes search
    {
        completeHeld =  parentHeld;
        completeHeld += strOperator;

        int pos2 = completeHeld.lastIndexOf("::");
        if(pos2 != (-1))
        {
            completeHeld.remove(pos2,1);
        }

        outItem = findItem(completeHeld, parentHeld);
        if(!outItem.head.isEmpty())
        {
            //!ok
            item = outItem;
            return scpi_err_none;
        }
    }

    //!error ????????
    return scpi_err_operator_match_fail;
}


/*!
 * \brief CScpiParser::findItem ????????????????????????????????????????????
 * \param strOperator            ???????
 * \param parent                 ???????? ??????????????????????????? ??????????????????
 * \return                       ?????????????????????????????????????????????????????
 */
struTotalTable CScpiParser::findItem(const QString &strOperator, QString &parent)
{
    struTotalTable outItem;

    if(m_pScpiTable == NULL )
    {
        return outItem;
    }

    QVector<struTotalTable>::const_iterator tableItem = (m_pScpiTable->m_totalTable)->begin();

    QRegExp reHeld("", Qt::CaseInsensitive);
    while( tableItem != m_pScpiTable->m_totalTable->end() )
    {
        reHeld.setPattern(tableItem->head);

        int pos = reHeld.indexIn(strOperator,0);

        if(pos != (-1))
        {
            outItem = (*tableItem);

            int pos1 = strOperator.lastIndexOf(":");
            if(pos1 <= 0 )
            {
                //!????????
                parent.clear();
            }
            else
            {
                //!???????????
                parent = strOperator.left(pos1+1);
            }

            return outItem; //!????????
        }
        tableItem++;
    }
    return outItem;//!????????
}


/*!
 * \brief CScpiParser::doCompatible ???????????????????:??????SCPI????????????????????????????,?????????????????????
 * \param scpiCmd                   ??????&??????: ???????????????????????????,??????????????????????????????????????????????(??????????????????????????????)
 * \return                          ???????????
 */
scpiError CScpiParser::doCompatible(CScpiCommand &scpiCmd)
{
    static QString  parent;
    QString origin;
    QString format;


    if(scpi_err_none != compatibleFind(scpiCmd[0], origin, format, parent))
    {
        QString completeHeld =  parent + scpiCmd[0];

        int pos2 = completeHeld.lastIndexOf("::");
        if(pos2 != (-1))
        {
            completeHeld.remove(pos2,1);
        }

        if(scpi_err_none != compatibleFind(completeHeld, origin, format, parent))
        {
            return scpi_err_operator_match_fail;
        }
    }

    if(!format.isEmpty())
    {
        for(int i = 1; i<scpiCmd.count();i++)
        {
            QString str = QString("\%%1").arg(i);

            format.replace(QString(str),(QString)scpiCmd[i]);
        }
        format = format.left(format.indexOf(QRegExp("\%\\d+"), 0));

        if(!origin.isEmpty())
        {
            QList<QByteArray> blist;
            QStringList slist = format.split(",", QString::SkipEmptyParts);
            blist.append(origin.toLatin1());

            foreach (const QString i, slist)
            {
                blist.append(i.toLatin1());
            }
            scpiCmd = blist;
        }
    }
    else if(!origin.isEmpty())
    {
        scpiCmd[0] = origin.toLatin1();
    }
    else
    {
        return scpi_err_operator_match_fail;
    }
    return scpi_err_none;
}


/*!
 * \brief CScpiParser::compatibleFind ?????????????????????????????????????????
 * \param strOperator                 ????????????: ???????
 * \param origin                      ????????????: ??????????? ???????????????????
 * \param format                      ????????????: ??????????? ??????????????????????????????? ??????????????
 * \param parent                      ????????????: ??????????? ??????????,??????????????,??????????????????
 * \return
 */
scpiError CScpiParser::compatibleFind(const QString strOperator,
                                      QString &origin, QString &format, QString &parent)
{
    if(m_pScpiTable == NULL )
    {
        SCPI_ERROR_DEBUG("scpi cmd table is empty!");
        return scpi_err_operator_match_fail;
    }

    QVector<struCompatibleTable>::const_iterator tableItem = (m_pScpiTable->m_compatibleTable)->begin();

    QRegExp reHeld("", Qt::CaseInsensitive);
    while( tableItem != (m_pScpiTable->m_compatibleTable)->end() )
    {
        for(int i = 0; i < tableItem->CompatibleCmd.count(); i++)
        {
            reHeld.setPattern(tableItem->CompatibleCmd.at(i).head);
            int pos = reHeld.indexIn(strOperator,0);

            if(pos != (-1))
            {
                origin = tableItem->originCmd;
                format = tableItem->CompatibleCmd.at(i).format;

                int pos1 = strOperator.lastIndexOf(":");
                if(pos1 <= 0 )
                {
                    //!????????
                    parent.clear();
                }
                else
                {
                    //!???????????
                    parent = strOperator.left(pos1+1);
                    //qDebug()<<__FILE__<<__LINE__<<"The parent node:"<<parent;
                }
                return scpi_err_none;
            }
        }
        tableItem++;
    }
    return scpi_err_operator_match_fail;
}


/*!
 * \brief CScpiParser::doConvert  ??????????????????????
 * \param ppConverter              ???????????
 * \param item                     ?????????????  ??????????????????????????????????????????
 * \param cmd                      ??????????????????? ???????????????? ???????
 * \param outValList               ???????????????????????
 * \return                         scpi_err_parmeter_too_much | scpi_err_cmd_perform_fail | scpi_err_none
 */
scpiError CScpiParser::doConvert(QVector<QSharedPointer<CConverter> > &ppConverter,
                                 const struTotalTable &item,
                                 CScpiCommand &cmd, CArgument &outValList)
{
    CVal val;
    if(ppConverter.isEmpty())
    {
        return scpi_err_none;
    }

    //!用户可以输入比规定数量多的参数个数，但是不能少于规定个数
    int parameterMinSize = (item.minSize != (-1)) ? item.minSize : ppConverter.count();
    if(  (cmd.count()-1) < parameterMinSize)
    {
        //!error Too many parameters
        SCPI_ERROR_DEBUG("The input parameters   expect:%d , reality:%d!   %s",
                         parameterMinSize,
                         (cmd.count()-1),
                         item.head.toLatin1().data()
                         );
        return scpi_err_parmeter_too_much;
    }

    //!以实际有效的参数个数作为终止条件，无效的参数不进行转换，直接丢弃。
    int convertSize = ( (cmd.count()-1) <= ppConverter.count())? (cmd.count()-1) : ppConverter.count();
    for(int i = 0; i < convertSize; i++)  //!??????????????????
    {
        QString unit = "";
        if(item.unit.count() > i)
        {
            unit = item.unit[i];
        }

       if(ppConverter[i]->toVal(cmd[i+1], val, unit) != scpi_err_none)
       {
           //SCPI_ERROR_DEBUG("Converter error!");
           qDebug()<< "["<< cmd[0] << cmd[i+1] << "]------------------- Invalid paramater";
           return scpi_err_conv;
       }
       outValList.append(val);
    }

    return scpi_err_none;
}

/*!
 * \brief CScpiParser::doTranslate ?????? ????
 * \param scpiCmd
 * \return
 */
scpiError CScpiParser::doTranslate(CScpiCommand &scpiCmd)
{
    //qDebug()<<__FILE__<<__LINE__;
    scpiError err = scpi_err_none;
    //! no operator
    if ( scpiCmd.size() < 1 )
    {
        _scpiErr.logError( scpi_err_operator );
        return scpi_err_operator;
    }
    SCPI_PARSER_DEBUG("Scpi Parser cmd : %s", scpiCmd.at(0).data());

    //!do the readTable
    err = loadScpiTable(scpiCmd);
    CHECK_SCPI_ERROR(err,scpi_event_command_error);


    //! do the find
    struTotalTable item;
    err = dofind(scpiCmd[0], item);
#if 1
    if( err != scpi_err_none )
    {
        if(scpi_err_none == doCompatible(scpiCmd))
        {
            err = dofind(scpiCmd[0], item);

            if( err != scpi_err_none )
            {
                qDebug()<< "["<< scpiCmd[0] << "]------------------- not work";
            }
        }
        else
        {
            //SCPI_ERROR_DEBUG("There is no matching scpi cmd!");
            qDebug()<< "["<< scpiCmd[0] << "]------------------- not work";
            err = scpi_err_operator_match_fail;
        }
    }
    CHECK_SCPI_ERROR(err,scpi_event_command_error);

    //! do the Factory
    QVector<QSharedPointer<CConverter> >  ppConverter;
    QVector<QSharedPointer<CFormater>  >  ppFormat;
    err = formatFactor::createFactory(item, m_pScpiTable->m_indexesTable, ppConverter, ppFormat);
    CHECK_SCPI_ERROR(err, scpi_event_command_error);


    //! now do convert
    CArgument convertOutData;
    err = doConvert(ppConverter, item, scpiCmd, convertOutData);
    CHECK_SCPI_ERROR(err, scpi_event_command_error);

    CScpiExecuteCommand  ExeItem;
    ExeItem.eof      = scpiCmd.getEof();
    ExeItem.m_pIO    = scpiCmd.getIO();
    ExeItem.mInArg   = convertOutData;
    ExeItem.item     = item;
    ExeItem.ppFormat = ppFormat;
    //qDebug()<<__FILE__<<__LINE__<<item.service<<item.cmd;
    complete(ExeItem);
#endif
    return err;
}

void CScpiParser::complete(CScpiExecuteCommand &/*exeCmd*/)
{

}

void CScpiParserWorker::complete(CScpiExecuteCommand &exeCmd)
{
    RegESR.rstRb(scpi_event_b0);
    appenExecuteItem(exeCmd);
    //emit eventNewCmd();
}


CScpiExecuteQueue   CScpiParserWorker::mExecuteQueue;     //!命令执行队列
QMutex              CScpiParserWorker::mExecuteQueueMutex;

bool CScpiParserWorker::getExecuteItem(CScpiExecuteCommand &outItem)
{
    bool err   = false;
    bool empty = false;
    mExecuteQueueMutex.lock();
    if(!mExecuteQueue.isEmpty())
    {
        SCPI_EXECUTE_DEBUG("%d",mExecuteQueue.count());
        outItem = mExecuteQueue[0];
        mExecuteQueue.removeFirst();
        err = true;
    }

    if(mExecuteQueue.isEmpty())
    {
        //        SCPI_DEBUG("mExecuteQueue.isEmpty()");
        empty = true;
    }
    mExecuteQueueMutex.unlock();

    //!队列为空 需要设置esr寄存器opc位
    if(empty)
    {
        RegESR.setRb(scpi_event_b0);
    }
    return err;
}

void CScpiParserWorker::appenExecuteItem(CScpiExecuteCommand &inItem)
{
    bool  exePriority = false;
    bool  postNewSig  = false;

    //!有些指令需要优先执行,例如哦*OPC
    if( (inItem.item.cmd == dsoServScpi::cmd_common_opc)&&
            (inItem.item.service == serv_name_common_cmd) )
    {
        exePriority  = true;
    }

    mExecuteQueueMutex.lock();

    postNewSig = mExecuteQueue.isEmpty();

    if(!exePriority)
    {
        mExecuteQueue.append(inItem);
    }
    else
    {
        mExecuteQueue.insert(0,inItem);
    }

#ifdef _QUEUE_SIZE_MAX
    if(_QUEUE_SIZE_MAX < mExecuteQueue.count())
    {
        mExecuteQueue.removeLast();
    }
#endif

#ifdef  _QUEUE_DEBUG
    int queueCount = mExecuteQueue.count();
#endif
    mExecuteQueueMutex.unlock();
    //! mExecuteQueue is  empty; Tell the Parser new start
    //! mExecuteQueue not empty; Parser In the run, Don't need to notice it
    if(postNewSig)
    {
        emit eventNewCmd();
    }

#ifdef  _QUEUE_DEBUG
    qDebug()<<__FILE__<<__LINE__<<"Execute Queue size:"<<queueCount;
#endif
}

void CScpiParserWorker::cls()
{
    mExecuteQueueMutex.lock();
    mExecuteQueue.clear();
    mExecuteQueueMutex.unlock();
}

/*!
 * \brief CScpiExtractorWorker::onNewCommand
 * \param newCmd
 * \return
 * new command received
 */
scpiError CScpiExtractorWorker::onNewCommand( CScpiCommand &newCmd )
{
    bool  postNewSig  = false;

    CommandQueueMutex.lock();

    postNewSig = mCommandQueue.isEmpty();
#ifndef _QUEUE_SIZE_MAX
    mCommandQueue.append( newCmd );
#else
    if(_QUEUE_SIZE_MAX > mCommandQueue.count())
    {
        mCommandQueue.append( newCmd );
    }
#endif

#ifdef  _QUEUE_DEBUG
    int queueCount = mCommandQueue.count();
#endif
    CommandQueueMutex.unlock();

#ifdef  _QUEUE_DEBUG
    qDebug()<<__FILE__<<__LINE__<<"Parser Queue size:"<<queueCount;
#endif

    if(postNewSig)
    {
        emit eventNewCmd();
    }

    return CScpiExtractor::onNewCommand( newCmd );
}

/*!
 * \brief CScpiExtractorWorker::doWork
 *
 */
void CScpiExtractorWorker::onReceive( unsigned char *pStream, int n )
{
    scpiError err;

    err = receivePackage( pStream, n );

    _scpiErr.logError( err );
}

/*!
 * \brief CScpiExtractorWorker::onReceive
 * \param pIntf
 *
 */
void CScpiExtractorWorker::onReceive( CScpiInterface *pIntf )
{
    Q_ASSERT( pIntf !=  NULL );
    while(!(pIntf->nextInBuffisEmpty()))
    {
#ifndef  _EXTRACTOR_OFF
        scpiError err;
        err = receivePackage( pIntf );
        _scpiErr.logError( err );
        Q_ASSERT( pIntf !=  NULL );
#else
        Q_ASSERT( pIntf !=  NULL );
        //qDebug()<<__FILE__<<__LINE__<<"Interface ID:"<<pIntf->getId();
#endif
    }
}

/*!
 * \brief CScpiParserWorker::doWork
 * for each command in queue
 */
void CScpiParserWorker::doWork()
{
    doParse();
}


/*!
 * \brief CScpiController::start
 * attach the extractor to worker thread
 */
void CScpiController::start()
{
    mExecute.moveToThread(   &mExecuteThread);
    mParser.moveToThread(    &mParseThread);
    mExtractor.moveToThread( &mExtractThread );

    connect( this, SIGNAL(eventReceive(unsigned char*,int)),
             &mExtractor, SLOT(onReceive(unsigned char*,int)) );

    connect( this, SIGNAL(eventReceive(CScpiInterface*)),
             &mExtractor, SLOT(onReceive(CScpiInterface*)) );

    connect( &mExtractor, SIGNAL(eventNewCmd()),
             &mParser, SLOT(doWork()) );

    connect( &mParser, SIGNAL(eventNewCmd()),
             &mExecute, SLOT(doExecute()) );

    //! two worker thread
    mExtractThread.start();
    mParseThread.start();
    mExecuteThread.start();
}

void CScpiController::setExecuteDisable(QMultiMap<QString, int> &whiteMap)
{
    mExecute.setDisable(whiteMap);
}

void CScpiController::setExecuteEnable()
{
    mExecute.setEnable();
}

void CScpiController::setExecuteLock()
{
    mExecute.setLock();
}

void CScpiController::setExecuteUnLock()
{
    mExecute.setUnLock();
}

void CScpiController::addFilterServive(QString servName)
{
    mExecute.addFilterServive(servName);
}

void CScpiController::rmFilterServive(QString servName)
{
    mExecute.rmFilterServive(servName);
}

void CScpiController::onReceive( unsigned char *pStream, int n )
{
    if ( NULL != pStream && n > 0 )
    {
        emit eventReceive( pStream, n );
    }
}

void CScpiController::onReceive( char *pStream )
{
    if ( NULL != pStream )
    {
        onReceive( (unsigned char*)pStream, strlen(pStream) );
    }
}

void CScpiController::onReceive( CScpiInterface *pIntf )
{
    if ( NULL != pIntf )
    {
        emit eventReceive( pIntf );
    }
}

void CScpiExecuteCommand::clear()
{
    m_pIO          = NULL;
    eof            = true;
    item.cmd       = 0;
    item.service   = "";
    mInArg.clear();
    ppFormat.clear();
}

CScpiExecuteWorker::CScpiExecuteWorker()
{
    mLock  = 0;
    m_enab = true;
}

scpiError CScpiExecuteWorker::doTranslate(CScpiExecuteCommand &ExecuteItem)
{
#ifndef   _EXECUTE_OFF
    scpiError err = scpi_err_none;
    //! do the execute
    if(ExecuteItem.ppFormat.isEmpty())
    {
        err = executeCmd(ExecuteItem.item, ExecuteItem.mInArg);
        CHECK_SCPI_ERROR(err,scpi_event_execution_error);
    }
    else
    {
        CArgument executeOutData;
        err = executeCmd(ExecuteItem.item, ExecuteItem.mInArg, executeOutData);
        CHECK_SCPI_ERROR(err,scpi_event_query_error);

        err = doFormat( ExecuteItem.ppFormat, executeOutData,
                        ExecuteItem.m_pIO, ExecuteItem.eof);

        CHECK_SCPI_ERROR(err,scpi_event_query_error);
    }
    return err;
#else
    //qDebug()<<__FILE__<<__LINE__<<ExecuteItem.item.service<<ExecuteItem.item.cmd;
    return scpi_err_none;
#endif
}

/*!
 * \brief CScpiExecute::executeCmd   执行设置指令
 * \param item                       包含service 和 cmd
 * \param para                       输入参数
 * \return                          scpi_err_cmd_perform_fail | scpi_err_none
 */
scpiError CScpiExecuteWorker::executeCmd(const struTotalTable &item,
                                         CArgument &para)
{
    DsoErr err;
    err =  serviceExecutor::post(item.service,
                                 item.cmd,
                                 para,
                                 &serviceExecutor::_contextRmt
                                 );
    if( err !=  ERR_NONE)
    {
        SCPI_ERROR_DEBUG("Failed service:%s cmd:%d err:%d!",
                         item.service.toLatin1().data(), item.cmd, err);

        return scpi_err_cmd_perform_fail;
    }

    //! wait for exec completed
    Q_ASSERT( NULL != serviceExecutor::_contextRmt.m_pSema );

    Q_ASSERT( serviceExecutor::_contextRmt.m_pSema->available() <= 1 );

    serviceExecutor::_contextRmt.m_pSema->acquire();

    //! now translate the err code to scpi err
    if ( ERR_NONE != serviceExecutor::_contextRmt.mErr )
    {
        //! \todo "-1,invalid input"
        //! :syst:err?
        SCPI_ERROR_DEBUG("Failed service:%s cmd:%d err:%d!",
                         item.service.toLatin1().data(), item.cmd,
                         serviceExecutor::_contextRmt.mErr);

        return scpi_err_cmd_perform_fail;
    }
    else
    {
        return scpi_err_none;
    }
}

/*!
 * \brief CScpiExecute::executeCmd  执行查询指令
 * \param item                      包含service 和 cmd
 * \param para                      输入参数
 * \param paraOut                   返回参数
 * \return                         scpi_err_cmd_perform_fail | scpi_err_none
 */
scpiError CScpiExecuteWorker::executeCmd(const struTotalTable &item,
                                         CArgument &para,
                                         CArgument &paraOut)
{
    //! 有输入参数
    if( para.size() != 0)
    {
        if(serviceExecutor::query(item.service, item.cmd, para, paraOut) !=  ERR_NONE)
        {
            SCPI_ERROR_DEBUG("execute query fail service: %s   cmd: %d!",
                             item.service.toLatin1().data(), item.cmd);

            return scpi_err_cmd_perform_fail;
        }
    }
    else
    {
        if( serviceExecutor::query(item.service, item.cmd, paraOut) !=  ERR_NONE)
        {
            SCPI_ERROR_DEBUG("execute query fail service: %s   cmd: %d!",
                             item.service.toLatin1().data(), item.cmd);

            return scpi_err_cmd_perform_fail;
        }
    }
    return scpi_err_none;
}

/*!
 * \brief CScpiExecute::doFormat  格式化输出
 * \param ppFormater              一组与输出参数对应的格式化器
 * \param para                    ??????????????????????
 * \return                        scpi_err_parmeter_too_little | scpi_err_format | scpi_err_null_ptr | scpi_err_none
 */
scpiError CScpiExecuteWorker::doFormat(QVector<QSharedPointer<CFormater> > &ppFormater,
                                       CArgument &para,
                                       CScpiInterface *m_pIO,
                                       bool eof)
{
    if(ppFormater.isEmpty())
    {
        SCPI_ERROR_DEBUG("scpi cmd table  Parameter error!");
        return scpi_err_none;
    }

    //!format
    if( para.size() < ppFormater.count())
    {
        //!error Returns the number of arguments is not enough
        SCPI_ERROR_DEBUG("Returns the number of arguments is not enough!");
        return scpi_err_parmeter_too_little;
    }

    /*! 输出参数逐一格式化 */
    for(int i = 0; i< ppFormater.count(); i++)
    {
        ppFormater[i]->init();

        if(ppFormater[i]->format(para[i]) != scpi_err_none)
        {
            SCPI_ERROR_DEBUG("format error!");
            return  scpi_err_format;
        }
    }

    Q_ASSERT(NULL != m_pIO);
    CArgument cmdInterface;
    cmdInterface.setVal(m_pIO->getType(), 0);
    cmdInterface.setVal(m_pIO->getId(),   1);

    {//~!check Interface
        CArgument activeInterface;
        activeInterface.clear();

        serviceExecutor::query(serv_name_scpi,
                               dsoServScpi::cmd_active_out_interface,
                               activeInterface);

        //!Interface change , 如果当前指令的输入接口不是当前活跃的输出接口,则需要切换输出接口
        if( activeInterface != cmdInterface )
        {
            //! 必须等待当前post被执行,才能执行后续操作.
            DsoErr err;
            err =  serviceExecutor::post(serv_name_scpi,
                                         dsoServScpi::cmd_active_out_interface,
                                         cmdInterface,
                                         &serviceExecutor::_contextRmt);
            if( err !=  ERR_NONE)
            {
                SCPI_ERROR_DEBUG("set activeInterface fail!");
                return scpi_err_null_ptr;
            }

            //! wait for exec completed
            Q_ASSERT( NULL != serviceExecutor::_contextRmt.m_pSema );
            Q_ASSERT( serviceExecutor::_contextRmt.m_pSema->available() <= 1 );
            serviceExecutor::_contextRmt.m_pSema->acquire();

            //! now translate the err code to scpi err
            if ( ERR_NONE != serviceExecutor::_contextRmt.mErr )
            {
                //! \todo "-1,invalid input"
                //! :syst:err?
                SCPI_ERROR_DEBUG("set activeInterface fail!");
                return scpi_err_null_ptr;
            }
        }
    }

    //!send
    SCPI_PARSER_DEBUG("eof:%d ",eof);

    Q_ASSERT(NULL != m_pIO);
    m_pIO->outPut(ppFormater, eof);

    return scpi_err_none;
}


void CScpiExecuteWorker::doExecute()
{
    CScpiExecuteCommand executeCmd;
    executeCmd.clear();
    QString servName = "";
    int     cmd      = -1;
    int     waitTime = 0;

    while(CScpiParserWorker::getExecuteItem(executeCmd))
    {
        servName = executeCmd.item.service;
        cmd      = executeCmd.item.cmd;
        waitTime = 0;
        //!wait
        while(mLock)
        {
//            if(waitTime%200 == 0)
//            {
//                qDebug()<<__FILE__<<__LINE__<<servName<<cmd<<" wait! ";
//            }

            waitTime ++;
            QThread::msleep(1);
        }

        //!公共指令永远不能被禁用
        if(servName == serv_name_common_cmd)
        {
            doTranslate(executeCmd);
            executeCmd.clear();
            continue;
        }

        //!服务过滤
        if(!m_servFilter.isEmpty())
        {
            if(m_servFilter.contains(servName))
            {
                //qDebug()<<__FILE__<<__LINE__<<"disenable:"<<servName;
                executeCmd.clear();
                continue;
            }
        }

        if(!m_enab)//! 禁用除白名单以外的指令。
        {
            QList<int> cmdList  =  m_whiteMap.values(servName);

            if( cmdList.contains(cmd) )
            {
                doTranslate(executeCmd);
                executeCmd.clear();
            }
            else
            {
                serviceExecutor::post( E_SERVICE_ID_UI,
                                       CMD_SERVICE_DO_ERR,
                                       ERR_ACTION_DISABLED );
                //qDebug()<<__FILE__<<__LINE__<<"disable:"<<servName<<cmd;
            }
        }
        else//!正常指令执行
        {
            doTranslate(executeCmd);
            executeCmd.clear();
        }
    }
}

void CScpiExecuteWorker::setDisable(QMultiMap<QString, int> &whiteMap)
{
    m_whiteMap = whiteMap;
    m_enab = false;
}

void CScpiExecuteWorker::setEnable()
{
    m_enab = true;
}

void CScpiExecuteWorker::setLock()
{
    //Q_ASSERT(mLock == 0);
//    if(mLock != 0)
//    {
//        qWarning()<<__FILE__<<__LINE__<<"!!!Warning Lock: mLock != 0 ";
//    }

    mLock  = 1;
}

void CScpiExecuteWorker::setUnLock()
{
    //Q_ASSERT(mLock == 1);
//    if(mLock != 1)
//    {
//        qWarning()<<__FILE__<<__LINE__<<"!!!Warning unLock: mLock != 1 ";
//    }

    mLock = 0;
}

void CScpiExecuteWorker::addFilterServive(QString servName)
{
    if (!m_servFilter.contains(servName))
    {
        m_servFilter.append(servName);
    }
}

void CScpiExecuteWorker::rmFilterServive(QString servName)
{
    if (m_servFilter.contains(servName))
    {
        m_servFilter.removeAll(servName);
    }
}

}

