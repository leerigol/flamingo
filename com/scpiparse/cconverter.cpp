#include "cconverter.h"

#include <QtCore>   //! QByteArray


namespace scpi_parse{

DsoReal toDsoReal(QByteArray &bytes, QString &reUnit, bool &ok)
{
    ok = true;

    DsoReal Value;
    int pos = -1;

    //!对传入的参数和单位字符串进行修剪,去掉首尾的空白字符.
    bytes  = bytes.trimmed();
    reUnit = reUnit.trimmed();

    //!eliminate unit  首先删除单位　去除　例如　MA(兆)和MA(毫安)　的冲突
    if(!reUnit.isEmpty())
    {
        QRegExp reValue = QRegExp(QString("%1$").arg(reUnit), Qt::CaseInsensitive);
        pos = reValue.lastIndexIn(bytes); //!这里需要从后向前搜索

        if(pos != -1)
        {
         bytes = bytes.left(pos);
         pos =  -1;
        }
    }

    //!extract mult  提取乘数　例如　MA M U U P
    QString   multStr;
    qlonglong multValue = 0;

    QRegExp reMult("\\s*(EX|PE|T|G|MA|K|M|U|N|P|F|A)", Qt::CaseInsensitive);
    pos = reMult.indexIn(bytes, 0);
    if(pos != -1)
    {
         multStr = reMult.cap(0).toUpper();
         bytes = bytes.left(pos);
         pos =  -1;
    }
    else
    {
         multStr.clear();
    }

    if(multStr == "EX")
    {
        multValue = +18;
    }
    else if(multStr == "PE")
    {
        multValue = +15;
    }
    else if(multStr == "T")
    {
        multValue = +12;
    }
    else if(multStr == "G")
    {
        multValue = +9;
    }
    else if(multStr == "MA")
    {
        multValue = +6;
    }
    else if(multStr == "K")
    {
        multValue = +3;
    }
    else if(multStr == "M")
    {
        multValue = -3;
    }
    else if(multStr == "U")
    {
        multValue = -6;
    }
    else if(multStr == "N")
    {
        multValue = -9;
    }
    else if(multStr == "P")
    {
        multValue = -12;
    }
    else if(multStr == "F")
    {
        multValue = -15;
    }
    else if(multStr == "A")
    {
        multValue = -18;
    }
    else
    {
        multValue = 0;
    }

    //!提取数值部分
    QRegExp reNR3("^(?:((?:\\-|\\+)?(?:\\d+)?)(?:(\\.)((?:\\d+)?[1-9])?0*)?)(?:(?:E|e)((?:\\+|\\-)?\\d+))?$");
    pos = reNR3.indexIn(bytes);
    if(pos == -1 )
    {
        //error
        qWarning()<<__FILE__<<__LINE__<<"!!!Illegal numerical input.";

        ok = false;
        return Value;
    }

    QStringList list = reNR3.capturedTexts();
#if 0
    for(int i = 0; i < list.count(); i++)
    {
      qDebug()<<i<<"....."<< list.at(i);
    }
#endif
    QString sInteger  = list.at(1);
    QString sDecimal  = list.at(3);
    QString sExponent = list.at(4);

    qlonglong llExponent  = 0;
    if(!sExponent.isEmpty())
    {
        llExponent  = sExponent.toLongLong(&ok);
        if(!ok){return Value;}
    }

    //! 数值部分　＝　浮点部分去掉小数点；　幂　＝　小数有效位数　＋　乘数　＋　指数
    qlonglong ma   = 0;
    if( !(sInteger.isEmpty() && sDecimal.isEmpty()) )
    {
        ma   = (sInteger + sDecimal).toLongLong(&ok);
        if(!ok){return Value;}
    }

    DsoE      me = (DsoE)((-sDecimal.count()) +  llExponent + multValue);
    Value.setVal(ma, me);

    return Value;
}

scpiError CConverter::toVal(QByteArray &, CVal &, QString &)
{
   return scpi_err_empty;
}

qint64 CFormater::getOutDataLen() const
{
    return mOutDataLen;
}

CFormater::CFormater()
{
    mOutDataLen = 0;
    mOutStr.clear();
}

quint32 CFormater::getOutData(QByteArray &buff, quint32 len)
{
    quint32 outSize = 0;
    if((int)len <= mOutStr.count() )
    {
        buff = mOutStr.left(len);
        outSize = len;
        mOutStr =  mOutStr.remove(0,len);
        mOutDataLen -= len;
    }
    else
    {
        buff = mOutStr;
        outSize = mOutStr.count();
        mOutStr.clear();
        mOutDataLen = 0;
    }
    return outSize;
}

scpiError CFormater::format(CVal &val)
{
    if ( val.vType == val_none )
    {
        return scpi_err_no_val;
    }

    switch( val.vType )
    {
    case val_bool:
        mOutStr = QString("%1").arg(val.bVal).toLatin1();
        break;

    case val_int:
        mOutStr = QString("%1").arg(val.iVal).toLatin1();
        break;

    case val_longlong:
        mOutStr = QString("%1").arg(val.llVal).toLatin1();
        break;

    case val_float:
        mOutStr = QString("%1").arg(val.fVal).toLatin1();
        break;

    case val_double:
        mOutStr = QString("%1").arg(val.dVal).toLatin1();
        break;

    case val_string:
        mOutStr = val.strVal.toLatin1();
        break;

    default:
        return scpi_err_no_val;
    }

    return scpi_err_none;
}


/*! format*/
scpiError CNR1Formater::format(CVal &val)
{
    if ( val.vType == val_longlong )
    {
        mOutStr = QString("%1").arg(val.llVal).toLatin1();
    }
    else if ( val.vType == val_int )
    {
        mOutStr = QString("%1").arg(val.iVal).toLatin1();
    }
    else if ( val.vType == val_bool )
    {
        mOutStr = QString("%1").arg(val.bVal).toLatin1();
    }
    else
    {
       return scpi_err_val_type_mismatch;
    }

    mOutDataLen = mOutStr.length();

    return scpi_err_none;
}

scpiError CNR2Formater::format(CVal &val)
{
    if ( val.vType == val_double )
    {
        mOutStr = QString("%1").arg(val.dVal).toLatin1();
    }
    else if ( val.vType == val_float )
    {
        mOutStr = QString("%1").arg(val.fVal).toLatin1();
    }
    else
    {
       return scpi_err_val_type_mismatch;
    }

    mOutDataLen = mOutStr.length();

    return scpi_err_none;
}

scpiError CNR3Formater::format(CVal &val)
{
    if ( val.vType == val_double )
    {
        mOutStr = QString("%1").arg(val.dVal, 0, 'E').toLatin1();
    }
    else if ( val.vType == val_float )
    {
        mOutStr = QString("%1").arg(val.fVal, 0, 'E').toLatin1();
    }
    else if ( val.vType == val_longlong )
    {
        mOutStr = QString("%1").arg((float)val.llVal, 0, 'E').toLatin1();
    }
    else if ( val.vType == val_int )
    {
        mOutStr = QString("%1").arg((float)val.iVal, 0, 'E').toLatin1();
    }
    else
    {
       return scpi_err_val_type_mismatch;
    }

    //qDebug()<<__FILE__<<__LINE__<<"NR3_FORMAT"<<mOutStr;
    mOutDataLen = mOutStr.length();

    return scpi_err_none;
}

scpiError CEnumFormater::format(CVal &val)
{
    qlonglong liv_val = 0;
    switch (val.vType)
    {
    case val_int:
        liv_val = val.iVal;
        break;
    case val_bool:
        liv_val = val.bVal;
        break;
    case val_longlong:
        liv_val = val.llVal;
        break;
    default:
        return scpi_err_val_type_mismatch;
    }

   for(int i = 0; i<tabel.count(); i++)
   {
       if( tabel[i].value == liv_val )
       {
           mOutStr = QString("%1").arg(tabel[i].str).toLatin1();

           mOutDataLen = mOutStr.length();
           return scpi_err_none;
       }
   }

    return scpi_err_no_val;
}

scpiError CNR3REALFormater::format(CVal &val)
{
     if ( val.vType == val_real )
     {
         mOutStr = val.realVal.toString().toLatin1();
         mOutDataLen = mOutStr.length();
         return scpi_err_none;
     }
    return scpi_err_no_val;
}

scpiError CStringFormater::format(CVal &val)
{
    if ( val.vType == val_string )
    {
        mOutStr = val.strVal.toUtf8();

        mOutDataLen = mOutStr.length();
        return scpi_err_none;
    }
    else
    {
       return scpi_err_val_type_mismatch;
    }
}

scpiError CBinFormater::format(CVal &val)
{
    if ( val.vType != val_arb_bin )
    {
       return scpi_err_val_type_mismatch;
    }

    QString head = QString("#9%1").arg(val.arbVal.mBlockLen, 9, 10, QLatin1Char('0'));
    quint8 *data = static_cast<quint8 *>(val.arbVal.mPtr);

    mOutStr.append(head);
    mOutStr.append((char*)data, val.arbVal.mBlockLen);

    mOutDataLen = mOutStr.length();

    val.arbVal.free(); //!释放arbVal空间

    return scpi_err_none;
}

template <typename T>
scpiError CPointerFormater < T > ::format(CVal &val)
{
    if ( val.vType != val_ptr )
    {
        return scpi_err_val_type_mismatch;
    }

    T *p_val = static_cast<T*>(val.pVal);

    mOutStr = QString("%1").arg(*p_val).toLatin1();

    mOutDataLen = mOutStr.length();

    return scpi_err_no_val;
}
CPointerFormater<double> CpNR2Formater ;

scpiError CNR3EXFormater::format(CVal &val)
{
    if ( val.vType == val_longlong )
    {
        mOutStr = QString("%1").arg((float)val.llVal, 0, 'E').toLatin1();
    }
    else if(  val.vType == val_int )
    {
        mOutStr = QString("%1").arg((float)val.iVal, 0, 'E').toLatin1();
    }
    else
    {
        return scpi_err_val_type_mismatch;
    }

    QStringList vList = QString(mOutStr).split('E');
    if(vList.count() != 2)
    {
        return scpi_err_val_type_mismatch;
    }

    QString sCardinality =  vList.at(0);
    int     iExponent    =  vList.at(1).toInt() +  magnification;
    QString sExponent    =  (iExponent > 0)? (QString("+%1").arg(iExponent)) : (QString("%1").arg(iExponent));
    mOutStr    =  (sCardinality +QString("E")+ sExponent).toLatin1();

    //qDebug()<<__FILE__<<__LINE__<<"NR3_E"<<magnification<<":"<<mOutStr;

    mOutDataLen = mOutStr.length();
    return scpi_err_none;
}


/*! Converter*/
//scpiError CNR1Converter::toVal(QByteArray &bytes, CVal &val , QString  &unit)
//{
//    DsoReal drValue;

//    drValue = toDsoReal(bytes, unit);
//    int ival = 0;

//    drValue.toReal(ival);

//    val.setVal( ival );
//    return scpi_err_none;
//}


/*! CNR1Converter
 *  T: int; long long; quint64; quint32; qint64; qint32
 */
template <typename T>
scpiError CNR1Converter<T>::toVal(QByteArray &bytes, CVal &val, QString &unit)
{
    DsoReal drValue;
    bool ok = false;
    drValue = toDsoReal(bytes, unit, ok);
    if(!ok)
    {
        qDebug()<<__FILE__<<__LINE__<<"!!!Numerical cross-border or illegal conversion.";
        return scpi_err_num_data;
    }

    T iVal = 0;
    if( ERR_NONE != drValue.toReal(iVal) )
    {
        qDebug()<<__FILE__<<__LINE__<<"!!!Numerical cross-border or illegal conversion.";
        return scpi_err_num_data;
    }

    val.setVal( iVal );
    return scpi_err_none;
}
CNR1Converter<int>        CNR1_i_Converter;
CNR1Converter<long long>  CNR1_ll_Converter;

scpiError CNR3Converter::toVal(QByteArray &bytes, CVal &val , QString  &unit)
{
    DsoReal drValue;
    bool ok = false;

    drValue = toDsoReal(bytes, unit, ok);
    if(!ok)
    {
        qDebug()<<__FILE__<<__LINE__<<"!!!Numerical cross-border or illegal conversion.";
        return scpi_err_num_data;
    }

    qlonglong ma = 0;//!基数
    DsoE      me = E_0;//!指数
    drValue.getVal(ma,me);

    double value = (QString("%1E%2").arg(ma).arg(me)).toDouble();

    val.setVal(value);

    return scpi_err_none;
}

scpiError CEnumConverter::toVal(QByteArray &bytes, CVal &val , QString  &)
{
    bytes  = bytes.trimmed();

    QRegExp re_bytes("((?:\\w)(?:(?:[\\w.]+[A-Za-z_]+)*))(\\d*)"); //!字母 下划线 数字开头

    int cmp = -1;
    for(int i = 0; i< tabel.count(); i++)
    {
        cmp = QString::compare( tabel[i].str, bytes,Qt::CaseInsensitive);
        if( cmp == 0)
        {
            val.setVal( tabel[i].value );
            return scpi_err_none;
        }
    }

    for(int i = 0; i< tabel.count(); i++)
    {
        int pos = re_bytes.indexIn( tabel[i].str,0);
        if( pos != (-1) )
        {
            QString  Memoni;
            QString  vowels = "aeiouAEIOU"; //!5个yuanyin
            QString  str = re_bytes.cap(1);
            QString  digital = re_bytes.cap(2);

            if(str.count()<=4)
            {
                continue;
            }
            else if( vowels.indexOf(str[3]) != (-1) )
            {
                Memoni = str.mid(0,3) + digital;
            }
            else
            {
                Memoni = str.mid(0,4) + digital;
            }

            cmp = QString::compare( Memoni, bytes,Qt::CaseInsensitive);
            if( cmp == 0)
            {
                val.setVal( tabel[i].value );
                return scpi_err_none;
            }
        }
    }

    return scpi_err_no_val;
}

const QStringList CBoolConverter::_trueTable  = QStringList()<<"true"<<"1"<<"on";
const QStringList CBoolConverter::_falseTable = QStringList()<<"false"<<"0"<<"off";
scpiError CBoolConverter::toVal(QByteArray &bytes, CVal &val , QString  &)
{
    bytes  = bytes.trimmed();
    QString qStr( bytes );
    QString refStr;

    //! true input
    for (int i = 0; i<_trueTable.count(); i++)
    {
        refStr = _trueTable.at(i);
        if ( refStr.compare(qStr, Qt::CaseInsensitive) == 0 )
        {
            val.setVal( true );
            return scpi_err_none;
        }
    }

    //! false
    for (int i = 0; i<_trueTable.count(); i++)
    {
        refStr = _falseTable.at(i);
        if ( refStr.compare(qStr, Qt::CaseInsensitive) == 0 )
        {
            val.setVal( false );
            return scpi_err_none;
        }
    }
    return scpi_err_conv;
}


scpiError CStringConverter::toVal(QByteArray &bytes, CVal &val, QString &)
{
    val.setVal("");
    val.strVal.append( bytes );
    val.vType = val_string;
    return scpi_err_none;
}

scpiError CArbBinConverter::toVal(QByteArray &bytes, CVal &val, QString &)
{
#if 0
    if( bytes.data()[0] != '#'   )
    {
        //!error 输入数据格式不正确
        return scpi_err_none;
    }

    //! arb bin
    int ret;
    val.vType = val_arb_bin;

    if( bytes.data()[1] == '0' )
    {
        //!去掉开头的#0
        ret = val.arbVal.attach( bytes.size() - 2,
                           bytes.data() + 2 );
        if ( ret != 0 )
        { return scpi_err_conv; }
    }
    else
    {
        //! sig check
        //! #1 -- #9
        int headLen = bytes.data()[1] - '0';
        if ( headLen <= 0 || headLen >9 )
        { return scpi_err_conv; }

        //! head check
        QByteArray payloadStr;
        quint32 payloadLen;

        if ( (headLen + 2 ) < bytes.length() )
        { return scpi_err_conv; }

        payloadStr = bytes.mid( 2, headLen );
        bool bOk;
        payloadLen = payloadStr.toUInt( &bOk );
        if ( !bOk )
        { return scpi_err_conv; }

        if ( payloadLen > 999999999 )
        { return scpi_err_conv; }

        //! check payload
        if ( ( 2 + headLen + payloadLen ) < bytes.length() )
        { return scpi_err_conv; }

        //! attach data
        ret = val.arbVal.attach( payloadLen,
                                 bytes.data() + 2 + headLen );
        if ( ret != 0 )
        { return scpi_err_conv; }

    }
#else
    int ret;

    //! invalid data
    if ( bytes.size() <= 0 )
    { return scpi_err_conv; }

    val.vType = val_arb_bin;
    ret = val.arbVal.attach( bytes.size(),
                             bytes.data() );
    if ( ret != 0 )
    { return scpi_err_conv; }
#endif

    return scpi_err_none;
}

scpiError CNR3REALConverter::toVal(QByteArray &bytes, CVal &val, QString &unit)
{
    DsoReal drValue;
    bool    ok = false;
    drValue = toDsoReal(bytes, unit, ok);
    if(!ok)
    {
        qDebug()<<__FILE__<<__LINE__<<"!!!Numerical cross-border or illegal conversion.";
        return scpi_err_num_data;
    }

    val.setVal(drValue);

    return scpi_err_none;

}

scpiError CNR3EXConverter::toVal(QByteArray &bytes, CVal &val, QString &unit)
{
    DsoReal drValue;
    bool    ok = false;
    drValue = toDsoReal(bytes, unit, ok);
    if(!ok)
    {
        qDebug()<<__FILE__<<__LINE__<<"!!!Numerical cross-border or illegal conversion.";
        return scpi_err_num_data;
    }

    qlonglong ma = 0;    //!基数
    DsoE      me = E_0;  //!指数
    drValue.getVal(ma,me);

    QString valueStr = QString("%1").arg(ma);
    int zeroFill = (int)me + magnification;

    if( zeroFill >= 0 )
    {
        //!补0
        valueStr +=QString(zeroFill, '0');
    }
    else if( (-zeroFill) < valueStr.count())
    {
        //!删0
        valueStr = valueStr.left(valueStr.count() - (-zeroFill));
    }
    else
    {
        //!等于0
        valueStr = "0";
    }

    qlonglong value = valueStr.toLongLong(&ok);
    if(!ok)
    {
        qDebug()<<__FILE__<<__LINE__<<"!!!Numerical cross-border or illegal conversion.";
        return scpi_err_num_data;
    }

    val.setVal(value);
    return scpi_err_none;
}


}
