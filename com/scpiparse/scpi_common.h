#ifndef SCPI_COMMON
#define SCPI_COMMON

#include <QString>      //! for string
#include <QList>        //! for list
#include <QByteArray>   //! byte array
#include <QObject>
#include <QMetaEnum>
namespace scpi_parse {

/*! \brief scpiError*/
#if 0
typedef enum
{
    scpi_err_null_ptr = -32768,
    scpi_err_null_alloc,            /*!< alloc fail */
    scpi_err_no_val,                /*!< invalid value */
    scpi_err_empty,                 /*!< function do not implement */
    scpi_err_conv,                  /*!< in convert */
    scpi_err_conv_overflow,         /*!< range overflow */
    scpi_err_invalid_cfg,           /*!< invalid config in class */
    scpi_err_index_overflow,        /*!< array index overflow */
    scpi_err_operator,              /*!< invalid operator */
    scpi_err_operator_match_fail,   /*!< match fail */
    scpi_err_operand_lack,          /*!< operand lack */
    scpi_err_0_length,              /*!< stream length 0 */
    scpi_err_intf_read,             /*!< read fail from interface */
    scpi_err_val_type_mismatch,     /*!< type mismatch */
    scpi_err_do,                    /*!< do err */
    scpi_err_cmd_entry_match_fail,  /*!< entry match fail */
    scpi_err_invalid_input_stream,  /*!< invalid input chars*/
    scpi_err_flie_load_fail,        /*!< 文件加载失败*/
    scpi_err_parmeter_too_much,     /*!< 参数太多*/
    scpi_err_parmeter_too_little,   /*!< 参数太少*/
    scpi_err_cmd_perform_fail,      /*!< 命令执行失败*/
    scpi_err_format,                /*!< 格式化错误*/

    scpi_err_none = 0,              /*!< no err */

}scpiError;
#else
#include "scpiError.h"
#endif


/*!
  *功能:检查scpi解析执行过程中的返回值
  *    如果错误,加入错误队列,且发出错误事件消息.
**/
#define  CHECK_SCPI_ERROR(err, event)  do{\
                                    if(err != scpi_err_none){\
                                        _scpiErr.logError( err );\
                                        sysSetScpiEvent(event,1);\
                                        qDebug()<<__FILE__<<__LINE__<<"error:"<<err<<"\n";\
                                    return err;}\
                                }while(0)

typedef QList<int> scpiErrorQueue;

class ScpiErrorLog
{
public:
    ScpiErrorLog(int max);

private:
    int            queueCapacity;      /*!< queue capacity */
    scpiErrorQueue errQueue;
    void           append( int err );

public:
    void clear();
    void logError( scpiError err );
    void logError( int err );
    int  getError();
};
extern ScpiErrorLog _scpiErr;


/*! \brief scpiIntfType
  */
typedef enum
{
    scpi_intf_debug,        /*!< debug used */
    scpi_intf_socket,       /*!< socket */
    scpi_intf_usb,          /*!< usb */
    scpi_intf_lxi,          /*!< lxi */
    scpi_intf_gpib,         /*!< gpib */
    scpi_intf_rs232,        /*!< rs232 */


}scpiIntfType;

typedef enum
{
    scpi_socket_connected_new,
    scpi_socket_connected_dis,
    scpi_socket_connected_ceiling,

    scpi_lxi_connected_new,
    scpi_lxi_connected_dis,
    scpi_lxi_connected_ceiling,

    scpi_usb_connected_new,
    scpi_usb_connected_dis,

}scpiIntfStatus;

//! \typedef scpiIntfId
typedef int scpiIntfId;
//! \typedef scpiIntfSig
typedef qlonglong scpiIntfSig;

#define INTERFACE_INVALID_ID  (-1)
#define USB_INTERFACE_MAX     (1)

//Tek4000为16. for bug1996 by hxh
#define LXI_INTERFACE_MAX     (16)

#define SOCKET_INTERFACE_MAX  (16)
#define GPIB_INTERFACE_MAX    (1)

//! data type range
#define char_max    (0x7f)
#define char_min    (-char_max-1)

#define short_max   (0x7fff)
#define short_min   (-short_max-1)

#define int_max     (0x7fffffff)
#define int_min     (-int_max-1)

#define lonlong_max (0x7fffffffffffffffll)
#define longlong_min (-lonlong_max-1)

#define uchar_max   (0xff)
#define uchar_min   0

#define ushort_max  0xffff
#define ushort_min  0

#define uint_max    0xffffffff
#define uint_min    0

#define ulonglong_max   0xffffffffffffffffull
#define ulonglong_min   0

#define float_max   (9.9e37f)
#define float_min   -(float_max)

//! \todo dobule range
#define double_max  (9.9e53)
#define double_min  -(9.9e53)


//! assert
//#define assert_null_alloc( ptr ) if ( NULL == ptr ) { return scpi_err_null_alloc; }
//#define assert_null( ptr )  if ( NULL == ptr ) { return scpi_err_null_ptr; }
#define assert_null_alloc( ptr ) if ( NULL == ptr ) { Q_ASSERT(false); }
#define assert_null( ptr )  if ( NULL == ptr )      {  Q_ASSERT(false);}

#define assert_err( err )   if ( err != scpi_err_none ) { return err; }
#define assert_range( val, range_max, range_min )   if ( val > range_max || val < range_min )\
                                                    { return scpi_err_conv_overflow; }

}

#endif // SCPI_COMMON

