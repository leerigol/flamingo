#include "cscpieventreg.h"
#include <QDebug>
namespace scpi_parse {

/*!
 * \var _dbgReg
 */
int CScpiEventReg::_dbgReg;


/*!
 * \brief CScpiEventReg::CScpiEventReg
 */
CScpiEventReg::CScpiEventReg( CScpiEventReg *pNotifier,
                              scpiEventBit nxtBit,
                              eventReg maskReg ) : m_pNotifier(pNotifier),
                                                   m_inPlace( nxtBit ),
                                                   iMask ( maskReg )
{

}

/*!
 * \brief CScpiEventReg::rst
 * rst all to 0
 */
void CScpiEventReg::rst()
{
    iMask = 0;
    iType = 0;
    iReg = 0;
}

void CScpiEventReg::setIMask(eventReg mask)
{
    iMask = mask;
    onEventChanged();
}

void CScpiEventReg::clearReg()
{
    iReg = 0;
}
void CScpiEventReg::setRb( scpiEventBit iBit )
{
    SET_b( iReg, iBit );
    onEventChanged();
}
void CScpiEventReg::rstRb( scpiEventBit iBit )
{
    RST_b( iReg, iBit );
}
void CScpiEventReg::toggleRb( scpiEventBit iBit )
{
    TOGGLE_b( iReg, iBit );
    onEventChanged();
}

void CScpiEventReg::setIMaskb( scpiEventBit iBit )
{
    SET_b( iMask, iBit );
    onEventChanged();
}
void CScpiEventReg::rstIMaskMb( scpiEventBit iBit )
{
    RST_b( iMask, iBit );
}
void CScpiEventReg::toggleIMaskb( scpiEventBit iBit )
{
    TOGGLE_b( iMask, iBit );
    onEventChanged();
}

void CScpiEventReg::setITypeb( scpiEventBit iBit )
{
    SET_b( iType, iBit );
}
void CScpiEventReg::rstITypeb( scpiEventBit iBit )
{
    RST_b( iType, iBit );
}
void CScpiEventReg::toggleITypeb( scpiEventBit iBit )
{
    TOGGLE_b( iType, iBit );
}

/*!
 * \brief CScpiEventReg::onEventChanged
 */
void CScpiEventReg::onEventChanged()
{
    //! event happened start reporting
    if ( iReg & iMask )
    {
        if ( NULL != m_pNotifier )
        {
            m_pNotifier->setRb( m_inPlace );
        }
        //! top one
        else
        {
            reportEvent( iReg );
            RST_reg( iReg );
        }
    }
}

/*!
 * \brief CScpiEventReg::reportEvent
 * \param reg
 * \todo reimplement this method to report
 */
void CScpiEventReg::reportEvent( eventReg reg )
{
    _dbgReg = reg;
}

/*--------------------------------------------------------------------------------------------------------------------
===============  STB  ================================================================================================
----------------------------------------------------------------------------------------------------------------------
7 OPER | Operation Status Register   | An enabled condition in the Operation Status Register (OPER) has occurred.
----------------------------------------------------------------------------------------------------------------------
6 RQS  | Request Service             | When polled, that the device is requesting service.
  MSS  | Master Summary Status       | When read (by *STB?), whether the device has a reason for requesting service.
----------------------------------------------------------------------------------------------------------------------
5 ESB  | Event Status                | Bit An enabled condition in the Standard Event Status Register (ESR) has occurred.
----------------------------------------------------------------------------------------------------------------------
4 MAV  | Message                     | Available There are messages in the Output Queue.
----------------------------------------------------------------------------------------------------------------------
3 ---  | ---                         | (Not used, always 0.)
----------------------------------------------------------------------------------------------------------------------
2 MSG  | Message                     | An advisory has been displayed on the oscilloscope.
----------------------------------------------------------------------------------------------------------------------
1 USR  | User Event                  | An enabled user event condition has occurred.
----------------------------------------------------------------------------------------------------------------------
0 TRG  | Trigger                     | A trigger has occurred.
--------------------------------------------------------------------------------------------------------------------*/
CScpiStbReg RegSTB;

CScpiStbReg::CScpiStbReg():CScpiEventReg(NULL,scpi_event_b0,0X0000)
{
}
/*!
 * \brief CScpiStbReg::reportEvent STB寄存器事件处理函数
 * \param reg
 */
void CScpiStbReg::reportEvent(eventReg reg)
{
    //!发送　中断事件
    emit this->srq((quint8)reg);
}



/*--------------------------------------------------------------------------------------------------------------------
===============  ESR  ================================================================================================
----------------------------------------------------------------------------------------------------------------------
7 PON  | Power On                | An OFF to ON transition has occurred
----------------------------------------------------------------------------------------------------------------------
6 URQ  |User Request            | A front-panel key has been pressed.
----------------------------------------------------------------------------------------------------------------------
5 CME  | Command Error          | A command error has been detected
----------------------------------------------------------------------------------------------------------------------
4 EXE  | Execution Error        | An execution error has been detected.
----------------------------------------------------------------------------------------------------------------------
3 DDE  | Device Dependent Error | A device-dependent error has been detected
----------------------------------------------------------------------------------------------------------------------
2 QYE  | Query Error            | A query error has been detected
----------------------------------------------------------------------------------------------------------------------
1 RQL  | Request Control        | The device is requesting control. (Not used.)
----------------------------------------------------------------------------------------------------------------------
0 OPC  | Operation Complete     | Operation is complete
--------------------------------------------------------------------------------------------------------------------*/
CScpiEventReg RegESR(&RegSTB, scpi_event_b5,  0x0000);

/*! OPER ******************************************************************** */
CScpiEventReg RegOPER(&RegSTB, scpi_event_b7,  0x0000);

/*! PWRR ******************************************************************** */
CScpiEventReg RegPWRR(&RegOPER, scpi_event_b7, 0x0000);
/*! MTER ******************************************************************** */
CScpiEventReg RegMTER(&RegOPER, scpi_event_b9, 0x0000);
/*! OVLR ******************************************************************** */
CScpiEventReg RegOVLR(&RegOPER, scpi_event_b11,0x0000);











}

