#include "cformatfactory.h"

#include "../../service/servcursor/servcursor.h"
#include "../../service/servtrig/servtrig.h"
#include "../../service/servstorage/storage_formater.h"
#include "../../service/servmeasure/meas_formater.h"
#include "../../service/servwfmdata/wfmdataformater.h"
#include "../../service/servscpi/servscpitest.h"
/*!
 * \brief formatFactor::createFactory 格式化器&转换器 工厂
 * \param item                        命令信息项, 内含该命令包含的参数索引信息
 * \param indexesTable                参数索引表，内含所有参数类型的详细数据
 * \param ppConverter                 工厂产生的转换器对象的指针保存在ppConverter
 * \param ppFormat                    工厂产生的格式化器对象的指针保存在ppFormat
 * \return                            scpi_err_val_type_mismatch | scpi_err_none
 */
scpiError formatFactor::createFactory(struTotalTable             &item,
                                      QVector<struIndexesTable>  *indexesTable,
                                      QVector<QSharedPointer<CConverter> >     &ppConverter,
                                      QVector< QSharedPointer<CFormater> >      &ppFormat)
{
    for(int i = 0; i< item.indexes.count(); i++)
    {

        QVector<struIndexesTable>::const_iterator indexesBegin = indexesTable->begin();

        int       offset    = item.indexes[i];
        PType     type      = (indexesBegin + offset)->type;
        Direction direction = (indexesBegin + offset)->direction;

        if(direction)
        {
            switch(type)
            {
            case PT_NR1 :
                ppConverter<< QSharedPointer<CNR1Converter<int> >(new CNR1Converter<int> );
                break;

            case PT_NR1_LL :
                ppConverter<< QSharedPointer<CNR1Converter<long long> >(new CNR1Converter<long long> );
                break;

            case PT_NR2 :
                ppConverter<< QSharedPointer<CNR3Converter>(new CNR3Converter);

                break;

            case PT_NR3 :
                ppConverter<< QSharedPointer<CNR3Converter>(new CNR3Converter);

                break;

            case PT_BOOL:
                ppConverter<< QSharedPointer<CBoolConverter>(new CBoolConverter);
                break;

            case PT_STRING:
                ppConverter<< QSharedPointer<CStringConverter>(new CStringConverter);
                break;

            case PT_BIN:
                ppConverter<< QSharedPointer<CArbBinConverter>(new CArbBinConverter);
                break;

            case PT_ENUM:
                ppConverter<< QSharedPointer<CEnumConverter>(new CEnumConverter((indexesBegin + offset)->enumTable));
                break;

            case PT_NR3_REAL:
                ppConverter<< QSharedPointer<CNR3REALConverter>(new CNR3REALConverter);
                break;

            case PT_NR3_EN18:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-18));
                break;
            case PT_NR3_EN15:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-15));
                break;
            case PT_NR3_EN12:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-12));
                break;
            case PT_NR3_EN9:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-9));
                break;
            case PT_NR3_EN6:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-6));
                break;
            case PT_NR3_EN3:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-3));
                break;
            case PT_NR3_EN2:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-2));
                break;
            case PT_NR3_EN1:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(-1));
                break;
            case PT_NR3_E0:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(0));
                break;
            case PT_NR3_EP1:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(1));
                break;
            case PT_NR3_EP2:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(2));
                break;
            case PT_NR3_EP3:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(3));
                break;
            case PT_NR3_EP6:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(6));
                break;
            case PT_NR3_EP9:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(9));
                break;
            case PT_NR3_EP12:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(12));
                break;
            case PT_NR3_EP15:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(15));
                break;
            case PT_NR3_EP18:
                ppConverter<< QSharedPointer<CNR3EXConverter>(new CNR3EXConverter(18));
                break;


            default  : return scpi_err_val_type_mismatch;
            }
        }
        else
        {
            switch(type)
            {
            case PT_NR1 :
                 ppFormat<< QSharedPointer<CNR1Formater>(new CNR1Formater);
                break;

            case PT_NR2 :
                ppFormat<< QSharedPointer<CNR2Formater>(new CNR2Formater);
                break;

            case PT_NR3 :
                ppFormat<< QSharedPointer<CNR3Formater>(new CNR3Formater);
                break;

            case PT_NR3_REAL:
                ppFormat<< QSharedPointer<CNR3REALFormater>(new CNR3REALFormater);
                break;

            case PT_NR3_EN18:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-18));
                break;
            case PT_NR3_EN15:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-15));
                break;
            case PT_NR3_EN12:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-12));
                break;
            case PT_NR3_EN9:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-9));
                break;
            case PT_NR3_EN6:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-6));
                break;
            case PT_NR3_EN3:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-3));
                break;
            case PT_NR3_EN2:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-2));
                break;
            case PT_NR3_EN1:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(-1));
                break;
            case PT_NR3_E0:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(0));
                break;
            case PT_NR3_EP1:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(1));
                break;
            case PT_NR3_EP2:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(2));
                break;
            case PT_NR3_EP3:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(3));
                break;
            case PT_NR3_EP6:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(6));
                break;
            case PT_NR3_EP9:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(9));
                break;
            case PT_NR3_EP12:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(12));
                break;
            case PT_NR3_EP15:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(15));
                break;
            case PT_NR3_EP18:
                ppFormat<< QSharedPointer<CNR3EXFormater>(new CNR3EXFormater(18));
                break;

            case PT_BOOL:
                ppFormat<< QSharedPointer<CNR1Formater>(new CNR1Formater);
                break;

            case PT_STRING:
                ppFormat<< QSharedPointer<CStringFormater>(new CStringFormater);
                break;

            case PT_BIN:
                ppFormat<< QSharedPointer<CBinFormater>(new CBinFormater);
                break;

            case PT_ENUM:
                ppFormat<< QSharedPointer<CEnumFormater>(new CEnumFormater((indexesBegin + offset)->enumTable));
                break;

            case PT_CURSOR:
                ppFormat<< QSharedPointer<CursorFormater>(new CursorFormater);
                break;

            case PT_TRIG_CODE:
                ppFormat<< QSharedPointer<CodeFormater>(new CodeFormater);
                break;

            case PT_IMAGE:
                ppFormat<< QSharedPointer<CImageFormater>(new CImageFormater);
                break;

            case PT_PATH:
                ppFormat<< QSharedPointer<CPathFormater>(new CPathFormater);
                break;

            case PT_MEAS_DATA:
                ppFormat<< QSharedPointer<CMeasFormater>(new CMeasFormater);
                break;

            case PT_WFM_DATA:
                ppFormat<< QSharedPointer<wfmdataFormater>(new wfmdataFormater);
                break;

            case PT_TEST:
                ppFormat<< QSharedPointer<CTestFormater>(new CTestFormater);
                break;

            default  : return scpi_err_val_type_mismatch;
            }
        }

    }
    return scpi_err_none;
}

#if 0
/*!
 * \brief formatFactor::destroyFormat  释放转换器和格式化器对象
 * \param ppConverter                  释放转换器对象的指针数组
 * \param ppFormat                     释放格式化器对象的指针数组
 * \return                             scpi_err_none
 */
scpiError formatFactor::destroyFormat(QVector<CConverter *> &ppConverter,
                                      QVector<CFormater *> &ppFormat)
{
    //!delete Converter
    for(int i = 0; i<ppConverter.count(); i++)
    {

       delete(ppConverter[i]);
       ppConverter[i] = NULL;
    }
    ppConverter.clear();

    //!delete Format
    for(int i = 0; i<ppFormat.count(); i++)
    {
       delete(ppFormat[i]);
       ppFormat[i] = NULL;
    }
    ppFormat.clear();

    return scpi_err_none;
}
#endif
