#ifndef CCONVERTER_H
#define CCONVERTER_H


#include <QRegExp>
#include "scpi_common.h"

#include "../../include/dsotype.h"
#include "../../baseclass/cargument.h"
#include "cscpitable.h"


namespace scpi_parse {
/*!
 * \brief The CConverter class
 * convert stream to type
 */
class CConverter
{
public:
    CConverter(){}
    virtual ~CConverter(){/*qDebug("delete Converter");*/}

public:
    virtual scpiError toVal(QByteArray&, CVal&, QString& );
};


class CFormater
{
public:
    CFormater();
    virtual ~CFormater(){ /*qDebug("delete Formater");*/ }

protected:
    qint64      mOutDataLen;
    QByteArray  mOutStr;

public:
    virtual void       init(){}          //! 格式化进行之前执行
    virtual void       startup(){}       //! 进入输出队列后执行

    virtual quint32    getOutData(QByteArray &buff, quint32 len);
    virtual scpiError  format( CVal &val );
    virtual bool       isWaiting(){return false;}

    qint64             getOutDataLen() const;
};

/*-------------------------------------------------------------------------------------------*/
/*! NR1*/
class CNR1Formater : public CFormater
{
public:
    CNR1Formater(){}

public:
    virtual scpiError format( CVal &val );
};

/*! NR2*/
class CNR2Formater : public CFormater
{
public:
    CNR2Formater(){}

public:
    virtual scpiError format( CVal &val );
};

/*! NR3*/
class CNR3Formater : public CFormater
{
public:
    CNR3Formater(){}

public:
    virtual scpiError format( CVal &val );
};

/*! enum*/
class CEnumFormater : public CFormater
{
public:
    CEnumFormater( QVector<struEnum> s):tabel(s){}
//    ~CEnumFormater()
//    {
//        mOutStr.clear();
//        tabel.clear();
//    }
private:
    QVector<struEnum> tabel;
public:
    virtual scpiError format( CVal &val );


};

/*! CNR3REAL*/
class CNR3REALFormater : public CFormater
{
public:
    CNR3REALFormater(){}

public:
    virtual scpiError format( CVal &val );
};

/*! string*/
class CStringFormater : public CFormater
{
public:
    CStringFormater(){}

public:
    virtual scpiError format( CVal &val );
};

/*! bin*/
class CBinFormater : public CFormater
{
public:
    CBinFormater(){}

public:
    virtual scpiError format( CVal &val );
};

/*! void *p */
template <typename T> class CPointerFormater : public CFormater
{
public:
    CPointerFormater() {}
private:
public:
    virtual scpiError format( CVal &val );

};

/*! NR3 amplification E X ;    X = 6, 9, 12......*/
class CNR3EXFormater : public CFormater
{
public:
    CNR3EXFormater(int x = 0):magnification(x){}

private:
    int magnification;

public:
    virtual scpiError format( CVal &val );
};

/*! ~~*****************************************************************************~~~!*/
///*! NR1 - int*/
//class CNR1Converter : public CConverter
//{
//public:
//    CNR1Converter(){}

//public:
//    virtual scpiError toVal(  QByteArray &bytes, CVal &val , QString  &unit );
//};


/*! CNR1Converter
 *  T: int; long long; quint64; quint32; qint64; qint32
 */
template <typename T>
class CNR1Converter : public CConverter
{
public:
    CNR1Converter(){}

public:
    virtual scpiError toVal(QByteArray &bytes, CVal &val , QString  &unit );
};


/*! NR3 */
class CNR3Converter : public CConverter
{
public:
    CNR3Converter(){}
public:
    virtual scpiError toVal(QByteArray &bytes, CVal &val , QString  &unit);
};

/*! Enum */
class CEnumConverter : public CConverter
{
public:
    CEnumConverter(QVector<struEnum> s):tabel(s) {}

private:
    QVector<struEnum> tabel;

public:
    virtual scpiError toVal( QByteArray &bytes, CVal &val , QString  &);
};

/*! bool*/
class CBoolConverter : public CConverter
{
public:
    CBoolConverter(){}

private:
#if 0
    static char*  _trueTable[];
    static char* _falseTable[];
#else
    static const QStringList  _trueTable;
    static const QStringList  _falseTable;
#endif

public:
    virtual scpiError toVal( QByteArray &bytes, CVal &val , QString  &);
};

/*! QString*/
class CStringConverter : public CConverter
{
public:
    CStringConverter(){}

public:
    virtual scpiError toVal(QByteArray &bytes, CVal &val , QString  &);
};

/*! bin*/
class CArbBinConverter : public CConverter
{
public:
    CArbBinConverter(){}

public:
    virtual scpiError toVal( QByteArray &bytes, CVal &val , QString & );
};

/*! NR3_REAL */
class CNR3REALConverter : public CConverter
{
public:
    CNR3REALConverter(){}
public:
    virtual scpiError toVal(QByteArray &bytes, CVal &val , QString  &unit);
};


/*! NR3 amplification E X ;    X = 6, 9, 12......*/
class CNR3EXConverter : public CConverter
{
public:
    CNR3EXConverter(int x = 0):magnification(x){}

private:
    int magnification;

public:
    virtual scpiError toVal(QByteArray &bytes, CVal &val , QString  &unit);
};






}
#endif // CConverter_H
