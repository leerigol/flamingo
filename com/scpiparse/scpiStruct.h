/*! public****************************************/

#ifndef SCPI_STRUCT
#define SCPI_STRUCT
#include <QString>
#include <QVector>


/*!
 *  参数类型
 */
enum PType
{
    PT_NR1 = 0,
    PT_NR2,
    PT_NR3,
    PT_BOOL,
    PT_STRING,
    PT_BIN,
    PT_ENUM,

    PT_NR1_LL,        //!long long

    PT_NR3_REAL = 10,  //!定点格式
    PT_NR3_EN18,
    PT_NR3_EN15,
    PT_NR3_EN12,
    PT_NR3_EN9,
    PT_NR3_EN6,
    PT_NR3_EN3,        //!缩小E-3倍 ,转longlong
    PT_NR3_EN2,
    PT_NR3_EN1,
    PT_NR3_E0   = 30,  //!大小不变  ,转longlong
    PT_NR3_EP1,
    PT_NR3_EP2,
    PT_NR3_EP3,        //!放大E+3倍 ,转longlong
    PT_NR3_EP6,
    PT_NR3_EP9,
    PT_NR3_EP12,
    PT_NR3_EP15,
    PT_NR3_EP18 = 50,


    PT_CURSOR   = 80,
    PT_TRIG_CODE,
    PT_IMAGE,
    PT_MEAS_DATA,
    PT_PATH,
    PT_WFM_DATA,

    PT_TEST = 255,
};


/*!
 *  参数方向
 */
enum Direction
{
    D_OUT,
    D_IN,
};



/*!
 *  枚举表表结构
 */
typedef struct _struEnum
{
    QString  str;
    int      value;
    _struEnum& operator=(const _struEnum& se)
    {
        str    = se.str;
        value  = se.value;
        return *this;
    }

    bool operator!=(const _struEnum& se)
    {
        if(str != se.str)
        {
            return true;
        }
        else if(value != se.value)
        {
            return true;
        }

        return false;
    }

}struEnum;



/*!
 *  指令参数信息索引表结构
 */
typedef struct _struIndexesTable
{
    int                 number;    /*!  索引表编号       */
    PType               type;      /*!  参数类型         */
    Direction           direction; /*!  参数方向 in|out  */
    QVector<struEnum>   enumTable; /*!  枚举型参数的枚举表 */

    _struIndexesTable& operator=(const _struIndexesTable& si)
    {
        number    = si.number;
        type      = si.type;
        direction = si.direction;
        enumTable = si.enumTable;
        return *this;
    }

    bool operator==(const _struIndexesTable& si)
    {
        if(type != si.type)
        {
            return false;
        }
        else if(direction != si.direction)
        {
            return false;
        }
        else if( enumTable.count() != si.enumTable.count())
        {
            return false;
        }
        else if(!enumTable.isEmpty())
        {
            for(int i=0; i<enumTable.count();i++)
            {
                if(enumTable[i] != si.enumTable[i])
                {
                    return false;
                }
            }
        }
        return true;
    }

}struIndexesTable;

typedef struct _struTotalTable
{
 QString            head;         /*! 头     */
 int                cmd;          /*! 消息   */
 QString            service;      /*! 服务   */
 int                minSize;      /*! 输入参数的最小个数*/
 QVector<int>       indexes;      /*! 参数索引*/
 QVector<QString>   unit;         /*! 物理单位*/


 _struTotalTable& operator=(const _struTotalTable& st)
 {
     head    = st.head;
     cmd     = st.cmd;
     service = st.service;
     minSize = st.minSize;
     indexes = st.indexes;
     unit    = st.unit;
     return *this;
 }
}struTotalTable;


typedef struct _struCompatible
{
    QString  head;     //!兼容命令正则表达式
    QString  format;   //!兼容命令向原始命令转换时的参数转换格式
    _struCompatible& operator=(const _struCompatible& se)
    {
        head    = se.head;
        format  = se.format;
        return *this;
    }
}struCompatible;

typedef struct _struCompatibleTable
{
    QString originCmd;                      //!原始命令
    QVector<struCompatible> CompatibleCmd;  //!兼容命令表
    _struCompatibleTable& operator=(const _struCompatibleTable& se)
    {
        originCmd      = se.originCmd;
        CompatibleCmd  = se.CompatibleCmd;
        return *this;
    }
}struCompatibleTable;

#endif // scpi struct
