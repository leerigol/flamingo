#ifndef CSCPITABLE
#define CSCPITABLE

#include "scpiStruct.h"
#include "scpi_common.h"
#include <QMap>
#include <QDebug>

using namespace scpi_parse;


class scpiTable
{

public:
   scpiTable(QString name);
   ~scpiTable();

private:    //!此处的静态成员用做于加载数据表时的缓存
   static  QString                      scpi_table_path;
   static  QVector<struIndexesTable>    IndexesTable;       //!索引表
   static  QVector<struTotalTable>      TotalTable;         //!命令表
   static  QVector<struCompatibleTable> CompatibleTable;    //!兼容表
   static  QMap<QString,QString>        scpiTableConfig;    //!配置表


private:
   static scpiError loadTableConfig();                  //!加载配置表
   static scpiError tableConfigFilter();                //!配置表过滤器，有些指令表在某些条件下不能被加载，例如LA在DS系列。
   static scpiError loadTableXml(QString xmlName);      //!加载命令表
   static scpiError loadCompatibleXml(QString xmlName); //!加载兼容命令表
public:    //!此处的数据表才是scpiTable类的对象的数据
   QString m_name;
   QVector<struIndexesTable>    *m_indexesTable;         //!索引表
   QVector<struTotalTable>      *m_totalTable;           //!命令表
   QVector<struCompatibleTable> *m_compatibleTable;      //!兼容表
public:    //!接口函数
   static scpiError loadScpiXml(QString key);            //!加载SCP命令表


};




#endif // CSCPITABLE

