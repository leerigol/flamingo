#ifndef CSCPIEVENTREG_H
#define CSCPIEVENTREG_H

#include "scpi_common.h"
#include <QObject>

namespace scpi_parse {

/*!
 * \typedef eventReg
 */
typedef unsigned short eventReg;

typedef enum
{
    scpi_event_b0 = 0,
    scpi_event_b1,
    scpi_event_b2,
    scpi_event_b3,

    scpi_event_b4,
    scpi_event_b5,
    scpi_event_b6,
    scpi_event_b7,

    scpi_event_b8,
    scpi_event_b9,
    scpi_event_b10,
    scpi_event_b11,

    scpi_event_b12,
    scpi_event_b13,
    scpi_event_b14,
    scpi_event_b15,
}scpiEventBit;

#define SET_b( reg, bitn )      { reg |= (((unsigned)1)<<bitn); }
#define RST_b( reg, bitn )      { reg &= ~(((unsigned)1)<<bitn); }
#define TOGGLE_b( reg, bitn )   { if ( reg & (1<<bitn) ){ RST_b(reg,bitn);}\
                                  else { SET_b(reg,bitn); }\
}
#define RST_reg( reg )  { reg = 0; }

/*!
 * \brief The CScpiEventReg class
 * event register
 * + mask: 1 -- enable
 * + type: edge or level
 * + reg: value
 */
class CScpiEventReg
{
public:
    CScpiEventReg( CScpiEventReg *pNotifier = NULL,
                   scpiEventBit nxtBit = scpi_event_b0,
                   eventReg maskReg = 0xffff );
private:
    CScpiEventReg *m_pNotifier;   /*!< notifier */
    scpiEventBit m_inPlace;       /*!< bit */

    eventReg iMask;
    eventReg iType;
    eventReg iReg;
public:
    static int _dbgReg;           /*!< debug used */

public:
    void rst();

    eventReg getReg(){ return iReg;}
    eventReg getIMask(){ return iMask; }
    eventReg getIType(){ return iType; }
    void setIMask(eventReg mask);
    void clearReg();

    void setRb( scpiEventBit iBit );
    void rstRb( scpiEventBit iBit );
    void toggleRb( scpiEventBit iBit );

    void setIMaskb( scpiEventBit iBit );
    void rstIMaskMb( scpiEventBit iBit );
    void toggleIMaskb( scpiEventBit iBit );


    void setITypeb( scpiEventBit iBit );
    void rstITypeb( scpiEventBit iBit );
    void toggleITypeb( scpiEventBit iBit );

    void onEventChanged();
    virtual void reportEvent( eventReg reg );
};


class CScpiStbReg:public QObject, public CScpiEventReg
{
    Q_OBJECT
public:
    CScpiStbReg();
    void reportEvent( eventReg reg );
signals:
    void srq(quint8 stb);
};



extern CScpiStbReg   RegSTB;
extern CScpiEventReg RegESR;
extern CScpiEventReg RegOPER;

extern CScpiEventReg RegPWRR;
extern CScpiEventReg RegMTER;
extern CScpiEventReg RegOVLR;

}



#endif // CSCPIEVENTREG_H
