#ifndef CSCPIPARSER_H
#define CSCPIPARSER_H

#include "scpi_common.h"
#include "cconverter.h"     /*!< converter */
#include <QObject>
#include <QThread>

#define _QUEUE_SIZE_MAX 100000

#ifdef _DEBUG
#define  SCPI_DEBUG(fmt,...)  do{qDebug("error: %s %u:" fmt,__FILE__,__LINE__,##__VA_ARGS__);} while(0)
#else
#define  SCPI_DEBUG(fmt,...)
#endif

#define  SCPI_INTERFACE_DEBUG(fmt,...) /*SCPI_DEBUG(fmt,##__VA_ARGS__)*/
#define  SCPI_PARSER_DEBUG(fmt,...)    /*SCPI_DEBUG(fmt,##__VA_ARGS__)*/
#define  SCPI_EXECUTE_DEBUG(fmt,...)   /*SCPI_DEBUG(fmt,##__VA_ARGS__)*/
#define  SCPI_ERROR_DEBUG(fmt,...)    SCPI_DEBUG(fmt)

namespace scpi_parse{

/*!
 * \brief The CScpiInterface class
 * 返回值：
 * + 0 -- 无错误
 * + <0 -- 有错误
 * + 对于读写，返回n
 */
class   CScpiController;
typedef quint32  (*get_formatData)(QByteArray &buff, quint32 len);

class CScpiInterface: public QObject
{
     Q_OBJECT
public:
    CScpiInterface( scpiIntfType scpiType = scpi_intf_debug, scpiIntfId id = 0 );
    ~CScpiInterface();

public:
    static bool isSame( scpiIntfSig sigA, scpiIntfSig sigB  );
    static void setController( CScpiController *pController );
protected:
    static CScpiController *m_pController;
    scpiIntfType            mType;
    scpiIntfId              mId;

public:
    scpiIntfType getType(){ return mType; }
    scpiIntfId getId(){ return mId; }
    scpiIntfSig getSig() { return ( (scpiIntfSig)mType << 32 ) | ( (scpiIntfSig)mId << 0 ); }

    virtual bool getEof(){ return m_InActiveBuff.eof; }

    virtual int inSize();
    virtual int read( void *pStreamIn, int n );

protected:
    struct inData
    {
        QByteArray Data;
        bool eof;
        void clear()
        {
            Data.clear();
            eof = false;
        }
        inData& operator=(const inData& buf)
        {
            Data  = buf.Data;
            eof   = buf.eof;
            return *this;
        }
    };

    struct outData
    {
        QSharedPointer<CFormater> pFormaterData;  //!从scpi端获取的数据
        QByteArray additionalOutData;              //!附加数据 例如最后的换行符等

        outData& operator=(const outData& data)
        {
            pFormaterData     = data.pFormaterData;
            additionalOutData = data.additionalOutData;
            return *this;
        }
    };

protected:
    //!输入队列
    QList<inData>   m_inDataQueue;
    inData          m_InActiveBuff;
    QMutex          inQueueMutex;

    //!输出队列
    QList<outData>  m_outDataQueue;
    QMutex          outQueueMutex;

protected:
    void            appendInData(QByteArray &data, bool eof);
    void            appendppFormater(QVector<QSharedPointer<CFormater> > ppFormater, bool eof);

public:
    virtual void    inPut( QByteArray &data, bool eof);
    virtual void    outPut(QVector<QSharedPointer<CFormater> > ppFormater, bool eof);

public:
    bool            nextInBuffisEmpty();
    quint32         getOutData(quint8 *buf, const quint32 &len, bool &eom);
    bool            outisValid();      //检查输出队列中是否是所有formater都准备好了数据。
    quint8          getStbReg();      //!获取STB寄存器状态,其中MAV(4)位与接口输出缓冲区相关.
    void            clearOutQueue();
    void            clearInQueue();

public Q_SLOTS:
    virtual void    onRequestOut();
    virtual void    srqRespond(quint8 stbReg);

private:
    bool outEof; //！是否是一批次的输出。

protected:
    volatile bool outEnable;  //! 输出使能  有新数据和输出完成时关闭，在收到onRequestOut时打开。
};

//! 调试使用模拟通讯接口
extern CScpiInterface _dbgIntf;

/*!
 * \brief The CScpiCommand class
 * scpi command in memory:
 * + str [str]+[,str]*
 * + io attribute, where the command from and where the result go
 */
class CScpiCommand : public QList<QByteArray>
{
public:
    CScpiCommand();
    CScpiCommand & operator=( QList<QByteArray> &cmd );
public:
    scpiError setIO( CScpiInterface *pIO );
    CScpiInterface *getIO();

    bool getEof() const;
    void setEof(bool eof);

private:
    QList<QByteArray> mList;
    CScpiInterface   *m_pIO;
    bool               mEof;

};

struct CInput
{
    unsigned char ucInput;
    bool eof;
};

/*!
 * \brief The CKeyWExtractor class
 * The base of scpi command extractor. It is used to extract the command
 * from the input string by the scpi format. In common, the scpi format is
 * "cmd [para][,para]+".
 */
class CKeyWExtractor
{
public:
    static CKeyWExtractor *m_pExtractor;    /*!< the active extractor */
    static QList<QByteArray> mChunk;        /*!< a chunk is composed of operator[operator]+ */
    static bool mChunkEnd;                  /*!< chunk complete */
    static bool mEof;                       /*!< 结束标志　*/

public:
    static bool isChunkComplete();
    static void clrChunk();

protected:
    const static int _C_TRIGGER_CNT = 4;    /*!< post status */

    QByteArray mTrigger[_C_TRIGGER_CNT];    /*!< trigger on */
    CKeyWExtractor *m_pNext[_C_TRIGGER_CNT];
    int mTriggerCnt;                        /*!< valid trigger count */

    CKeyWExtractor *m_pIdle;                /*!< idle */
public:
    CKeyWExtractor(){ mTriggerCnt = 0; mChunkEnd = false; }

    static scpiError setExtractor( CKeyWExtractor *pNxt );
    static CKeyWExtractor *getExtractor();

public:
    scpiError addTrigger( QByteArray byteAry, CKeyWExtractor *pNext );
    scpiError addTrigger( const char *str, CKeyWExtractor *pNext );

    void setIdle( CKeyWExtractor *pIdle );
public:
    virtual scpiError onInput( CInput &input );
    virtual scpiError onChange( CInput &input,
                                CKeyWExtractor *pNext,
                                void *ptr=NULL );
    virtual scpiError onShift( CInput &input,
                               CKeyWExtractor *pNext,
                               void *ptr=NULL );
    virtual scpiError onEnter( CInput &input, void *ptr=NULL );
    virtual scpiError onExit( CInput &input );
    void rst();
};

/*!
 * \brief The CIdleExtractor class
 * Idle or unknown extractor. It leaves by received
 */
class CIdleExtractor : public CKeyWExtractor
{
public:
    CIdleExtractor(){}

public:
    scpiError onEnter( CInput &input, void *ptr=NULL );
};

class COperatorExtractor : public CKeyWExtractor
{
public:
    COperatorExtractor(){}

protected:
    QByteArray mOperator;   /*!< the operator element */

public:
    scpiError onInput( CInput &input );
    scpiError onEnter( CInput &input, void *ptr=NULL );
    scpiError onExit(CInput &input );
};

class CPreOperandExtractor : public CKeyWExtractor
{
public:
    CPreOperandExtractor(){}

public:

};

class COperandExtractor : public CKeyWExtractor
{
public:
    COperandExtractor(){}
protected:
    QByteArray mOperand;

public:
    scpiError onInput( CInput &input );
    scpiError onEnter( CInput &input, void *ptr=NULL );
    scpiError onExit( CInput &input );
};

class COperandStringExtractor : public COperandExtractor
{
public:
    COperandStringExtractor(){}
public:
//    scpiError onInput( unsigned char cInput );
};

class COperandDefArbHeaderExtractor : public COperandExtractor
{
public:
    COperandDefArbHeaderExtractor(){}

protected:
    int blockWidth;
public:
    scpiError onEnter( CInput &input, void *ptr=NULL );
    scpiError onInput( CInput &input );
};

class COperandArbExtractor : public COperandExtractor
{
public:
     COperandArbExtractor(){}

protected:
     int blockLength;

public:
    scpiError onEnter( CInput &input, void *ptr=NULL );
    scpiError onExit(CInput &input);
    scpiError onInput(CInput &input);
};

/*!
 * \typedef CScpiCommandQueue
 */
typedef QList<CScpiCommand> CScpiCommandQueue;


class CScpiExtractor
{
protected:
    CIdleExtractor mIdle;           /*!< extractor status */
    COperatorExtractor mOperator;

    CPreOperandExtractor mPreOperand;
    CPreOperandExtractor mPstOperand;

    COperandExtractor mOperand;

    COperandDefArbHeaderExtractor mDefArbHeaderOperand;
    COperandArbExtractor mArbOperand;

                                    /*!< a chunk is composed of operator[operator]+ */
    CScpiCommand mCommand;
                                    /*!< command queue */
    static CScpiCommandQueue mCommandQueue;
    static QMutex            CommandQueueMutex;

    scpiIntfSig mIntfSig;           /*!< signature of the intf */

public:
    CScpiExtractor()
    {
        mCommandQueue.clear();
    }

public:
    static CScpiCommandQueue &getCommandQueue();

public:
    void init();

    static bool getCommand(CScpiCommand &outCommand);
    void cls();

    scpiError receive( unsigned char *pStream, int n, CScpiInterface *pInterfce=&_dbgIntf );
    scpiError receive( char *pStream, CScpiInterface *pInterfce=&_dbgIntf );

    scpiError receivePackage( unsigned char *pStream, int n, CScpiInterface *pInterfce=&_dbgIntf );
    scpiError receivePackage( char *pStream, CScpiInterface *pInterfce=&_dbgIntf );

    scpiError receivePackage( CScpiInterface *pInterfce );
    scpiError receive( CScpiInterface *pInterfce );

protected:
    virtual scpiError onNewCommand( CScpiCommand &newCmd );
    scpiError exportChunk( CScpiInterface *pInterfce );
    scpiError unBundle( CScpiInterface *pInterface,
                        unsigned char **pStream, int *pLen );

    scpiError packageEnd( CScpiInterface *pIntf );
};


class CScpiExecuteCommand
{
public:
    CScpiInterface *m_pIO;
    bool            eof;

    struTotalTable  item;
    CArgument       mInArg;
    QVector< QSharedPointer<CFormater> >  ppFormat;

public:
    CScpiExecuteCommand& operator=( const CScpiExecuteCommand &cmd )
    {
        m_pIO    = cmd.m_pIO;
        eof      = cmd.eof;
        item     = cmd.item;
        mInArg   = cmd.mInArg;
        ppFormat = cmd.ppFormat;
        return *this;
    }

    void clear();

};
typedef QList<CScpiExecuteCommand> CScpiExecuteQueue;

/*!
 * \brief The CScpiParser class
 * Use the following steps
 * 1. register the command table
 *
 * #About the table
 * the table is like the parent-child-subchild, so rigister the top parent
 */
class CScpiParser
{
public:
    CScpiParser();

public:
    scpiError doParse();
/*************************************************/
private:
    static const int            s_BUFF_SIZE = 10;   //!缓冲区数量 根据使用中频繁切换的模块数量确定最佳值
    static QList< scpiTable *>  s_pListTable;       //!缓冲区
    scpiTable                   *m_pScpiTable;      //!解析器当前使用的数据表
protected:
    scpiError loadScpiTable(CScpiCommand &scpiCmd );

    scpiError dofind(const QByteArray &strOperator,
                     struTotalTable   &item);

    struTotalTable findItem(const QString &strOperator,
                            QString       &parent);

    scpiError doCompatible(CScpiCommand &scpiCmd);

    scpiError compatibleFind(const QString strOperator,
                             QString       &origin,
                             QString       &format,
                             QString       &parent);

    scpiError doConvert(QVector<QSharedPointer<CConverter> > &ppConverter,
                        const struTotalTable                 &item,
                        CScpiCommand                         &cmd,
                        CArgument                            &outValList);
#if 0
    scpiError executeCmd(const struTotalTable &item,
                         CArgument            &para );

    scpiError executeCmd(const struTotalTable &item,
                         CArgument            &para,
                         CArgument            &paraOut);

    scpiError doFormat(QVector<QSharedPointer<CFormater> > &ppFormater,
                       CScpiCommand                        &cmd,
                       CArgument                           &para );
#endif
public:
    scpiError      doTranslate( CScpiCommand &scpiCmd );
    virtual        void complete(CScpiExecuteCommand &exeCmd);
};


/*!
 * \brief The CScpiExtractorWorker class
 * extractor worker
 * selaed in QObject
 */
class CScpiExtractorWorker : public QObject, public CScpiExtractor
{
    Q_OBJECT
public:
    CScpiExtractorWorker(){}

protected:
    scpiError onNewCommand( CScpiCommand &newCmd );

public Q_SLOTS:
    void onReceive( unsigned char *pStream, int n );
    void onReceive( CScpiInterface *pIntf );
Q_SIGNALS:
    void eventNewCmd();

};

/*!
 * \brief The CScpiParserWorker class
 * parser worker
 * selaed in QObject
 */
class CScpiParserWorker : public QObject, public CScpiParser
{
    Q_OBJECT
public:
    CScpiParserWorker(){}
    virtual   void complete(CScpiExecuteCommand &exeCmd);
protected:
    static CScpiExecuteQueue     mExecuteQueue;     //!命令执行队列
    static QMutex                mExecuteQueueMutex;
public:
    static bool                  getExecuteItem(CScpiExecuteCommand &outItem);
    void                         appenExecuteItem(CScpiExecuteCommand &inItem);
    void                         cls();
    int                          getQueueCount();
Q_SIGNALS:
    void eventNewCmd();
public Q_SLOTS:
    void doWork();
};

/*!
 * \brief The CScpiParserWorker class
 * parser worker
 * selaed in QObject
 */
class CScpiExecuteWorker : public QObject
{
    Q_OBJECT
public:
    CScpiExecuteWorker();
    scpiError doTranslate( CScpiExecuteCommand &ExecuteItem );

protected:
    scpiError executeCmd(const struTotalTable &item,
                         CArgument            &para );

    scpiError executeCmd(const struTotalTable &item,
                         CArgument            &para,
                         CArgument            &paraOut);

    scpiError doFormat(QVector<QSharedPointer<CFormater> > &ppFormater, CArgument &para,
                       CScpiInterface *m_pIO,  bool eof);
public Q_SLOTS:
    void doExecute();

/*! ----------------------------------*/
//! 指令添加禁止执行开关和白名单机制。
public :
    void setDisable(QMultiMap<QString, int> &whiteMap);
    void setEnable();
    void setLock();
    void setUnLock();
    void addFilterServive(QString servName);
    void rmFilterServive(QString servName);

private:
    int                      mLock;
    bool                     m_enab;
    QMultiMap<QString, int>  m_whiteMap;
    QList<QString>           m_servFilter;
};

/*!
 * \brief The CScpiXXController class
 * controller of base
 */
class CScpiController : public QObject
{
    Q_OBJECT
protected:
    QThread mExtractThread;         /*!< extract */
    QThread mParseThread;           /*!< Parse   */
    QThread mExecuteThread;         /*!< Execute */

protected:
    CScpiExtractorWorker mExtractor;
    CScpiParserWorker    mParser;
    CScpiExecuteWorker   mExecute;
public:
    CScpiController(){ mExtractor.init(); }
    void start();

public: //!ExecuteWorker
    void setExecuteDisable(QMultiMap<QString, int> &whiteMap);
    void setExecuteEnable();
    void setExecuteLock();
    void setExecuteUnLock();
    void addFilterServive(QString servName);
    void rmFilterServive(QString servName);

public Q_SLOTS:
    void onReceive( unsigned char *pStream, int n );
    void onReceive( char *pStream );
    void onReceive( CScpiInterface *pIntf );

Q_SIGNALS:
    void eventReceive( unsigned char *pStream, int n );
    void eventReceive( CScpiInterface *pIntf );
};


}

#endif // CSCPIPARSER_H
