#include "csocketinterface.h"
#include<QtConcurrent/QtConcurrent>

#include "../service/service_name.h"
#include "../service/servscpi/servscpidso.h"
#include "../service/service.h"
#include "../../service/servgui/servgui.h"

QTcpServer      * CSocketInterface::m_pServer     = NULL;

CSocketInterface::CSocketInterface(scpiIntfType scpiType, scpiIntfId id)
    :CScpiInterface(scpiType, id)
{
    m_pActSocket  = NULL;
}

CSocketInterface::~CSocketInterface()
{
    if(m_pActSocket != NULL)
    {
        m_pActSocket->deleteLater();
        m_pActSocket = NULL;
    }
}

/*!
 * \brief CSocketInterface::setServer
 * \param pServer
 * 设置tcp服务器
 */
void CSocketInterface::setServer( QTcpServer *pServer )
{
    Q_ASSERT( pServer != NULL );
    m_pServer = pServer;
}

/*!
 * \brief CSocketInterface::onNewConnection
 * socket 建立新连接
 */
void CSocketInterface::onNewConnection( QTcpSocket *socket, int id)
{
    Q_ASSERT( m_pActSocket == NULL );
    m_pActSocket = socket;
    Q_ASSERT( m_pActSocket != NULL );
    mId = id;

    //! connect new
    connect( m_pActSocket, SIGNAL(readyRead()),    this, SLOT(onDataIn())     );
    connect( m_pActSocket, SIGNAL(disconnected()), this, SLOT(disconnected()) );
    //servGui::showInfo(sysGetString( INF_SOCKET_CONNECTED, "SOCKET disconnected") );
}

void CSocketInterface::disconnected()
{

    disconnect( m_pActSocket, 0, this, 0);

    outEnable = false; //!禁止从此端口发送数据
    if(m_pActSocket != NULL)
    {
        m_pActSocket->deleteLater();
        m_pActSocket = NULL;
    }

   mId = INTERFACE_INVALID_ID;
   //servGui::showInfo(sysGetString( INF_SOCKET_DISCONNECTED, "SOCKET disconnected") );
}

/*!
 * \brief CSocketInterface::onDataIn
 * 接收到来自客户端的数据，将数据转发到scpi控制器中
 */
void CSocketInterface::onDataIn()
{
    QByteArray data = m_pActSocket->readAll();

    CScpiInterface::inPut(data, false);

    Q_ASSERT( m_pController != NULL );
//    m_pController->onReceive( this );
}

/*!
 * \brief CSocketInterface::onRequestOut
 * 接收到来自数据输出请求
 */
void CSocketInterface::onRequestOut()
{
    quint8  *buff  = new quint8[1024000];
    Q_ASSERT( buff != NULL );

    bool eom = false;
    quint32 liv_size = 0;

    while( (true != eom) && (true == outEnable) )
    {
        liv_size = getOutData(buff, 1024000, eom);
        if( liv_size  > 0 )
        {
            if(m_pActSocket != NULL )
            {
                qint64 writeSize = m_pActSocket->write((char*)buff, liv_size);
                if( writeSize != liv_size)
                {
                    qWarning()<<__FUNCTION__<<__FILE__
                             <<"socket write:"<< writeSize<<"\t"<<liv_size;
                    break;
                }
            }
            else
            {
                qWarning()<<__FUNCTION__<<__FILE__
                         <<"m_pActSocket == NULL";
                 break;
            }
        }
        else
        {
            break;
        }
    }

    outEnable = false;
    delete []buff;
}

void CSocketInterface::srqRespond(quint8 stbReg)
{
     SCPI_INTERFACE_DEBUG("socket srq event: %d",stbReg);
     stbReg = stbReg;
}


