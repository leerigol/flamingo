#ifndef CSOCKETINTERFACE_H
#define CSOCKETINTERFACE_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

#include "../scpiparse/cscpiparser.h"

using namespace scpi_parse;

class CSocketInterface :public CScpiInterface
{
    Q_OBJECT

public:
    CSocketInterface(scpiIntfType scpiType = scpi_intf_socket, scpiIntfId id = 0);
    ~CSocketInterface();
public:
    static void setServer( QTcpServer *pServer );
    void onNewConnection(QTcpSocket *socket, int id);

private Q_SLOTS:
    void disconnected();
    void onDataIn();

protected:
    static QTcpServer      *m_pServer;
    QTcpSocket             *m_pActSocket;

public Q_SLOTS:
    void onRequestOut();
    void srqRespond(quint8 stbReg);

};

#endif // CSOCKETINTERFACE_H
