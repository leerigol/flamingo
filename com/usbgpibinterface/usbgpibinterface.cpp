#include "usbgpibinterface.h"
#include "./usbgpib/usbgpib_user.h"
#include "../../service/servgui/servgui.h"

CUsbGpibInterface::CUsbGpibInterface(scpiIntfType scpiType, scpiIntfId id)
    :CScpiInterface(scpiType, id)
{
    m_bParseEnd   = true;
    m_bConnection = false;
}

int CUsbGpibInterface::setGpibAddr(int addr)
{
    return usbgpibSetAddress((u16)addr);
}

void CUsbGpibInterface::parseEnd()
{
    if(m_bParseEnd)
    {
        usbgpibParseEnd();
    }
}

void CUsbGpibInterface::checkLink()
{
    usbGpibCheckLink();
}

void CUsbGpibInterface::outPut(QVector<QSharedPointer<CFormater> > ppFormater, bool eof)
{
    CScpiInterface::outPut(ppFormater, eof);
    //!已经有数据准备输出了，不能再返回 ParseEnd
    m_bParseEnd = false;
}

void CUsbGpibInterface::onConnection()
{
    m_bConnection = true;
    servGui::showInfo(sysGetString( INF_USB_GPIB_CONNECTED, "USB-GPIB connected") );
}

void CUsbGpibInterface::onDisconnected()
{
    m_bConnection = false;
    servGui::showInfo(sysGetString( INF_USB_GPIB_DISCONNECTED, "USB-GPIB disconnected") );
}

void CUsbGpibInterface::onNewConnection()
{
    pGpibInterface = this;
    Q_ASSERT( pGpibInterface != NULL );
    usbgpibInit();
}

bool CUsbGpibInterface::getConnectionState()
{
    return m_bConnection;
}

void CUsbGpibInterface::onRequestOut()
{
    usbgpibOutRequest();
    //!本次数据已经输出，下次来数据之前 可以返回 ParseEnd
    m_bParseEnd = true;
}

