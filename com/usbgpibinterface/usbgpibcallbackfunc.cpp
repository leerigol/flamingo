#include "./usbgpib/usbgpib_user.h"
#include "usbgpibinterface.h"

CUsbGpibInterface *pGpibInterface = NULL;


s32  usbgpibTxCallback(u8 *pBuf, u32 len,  int* const pEof)
{
    Q_ASSERT( pGpibInterface != NULL );
    Q_ASSERT( pBuf != NULL );

    bool eom = false;

    u32 retuenSize = pGpibInterface->getOutData((quint8 *)pBuf, len, eom);
    *pEof = eom;

    return retuenSize;
}

void usbgpibRxCallback(s8 *pBuf, u32 len, u8 eom, int /*id*/)
{
    Q_ASSERT( pGpibInterface != NULL );
    Q_ASSERT( pBuf != NULL );
    QByteArray data((char*)pBuf, len);

    //!送数据到ＳＣＰＩ
    pGpibInterface->inPut(data, (bool)eom );
}

void usbgpibLinkCallback(int status)
{
    Q_ASSERT( pGpibInterface != NULL );
    if(status != 0)
    {
        pGpibInterface->onConnection();
    }
    else
    {
        pGpibInterface->onDisconnected();
    }
}

void usbgpibCallbackRegister()
{
    g_stSCPIIO.pfunTxData2IOBuf    = usbgpibTxCallback;
    g_stSCPIIO.pfunWriteInputQueue = usbgpibRxCallback;
    g_stSCPIIO.pfunLinkStatus      = usbgpibLinkCallback;

}
