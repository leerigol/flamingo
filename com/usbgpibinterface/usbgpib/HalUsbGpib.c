/*******************************************************************************
                      普源精电科技有限公司版权所有
********************************************************************************
  文 件 名: HalUsbGpib.c
  功能描述: 针对USB HOST口连接RIGOL的USB-GPIB产品书写的HAL层文件
  作    者: sn02241
  版    本: V1.00
  创建日期: 2017-10-26

  修改历史:
  作者          修改时间            版本        修改内容
  sn02241       2017.10.26          V1.00       initial

*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <asm/ioctls.h>
#include <time.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <linux/usb/tmc.h>
#include "DataType.h"
#include "HalUsbGpib.h"
#include "HalUsbtmc.h"

/* 功能测试选项，值为0关闭测试，为1打开测试 */
#define DEBUG_USB_GPIB      0
#define DEBUG_USB_GPIB_MAIN 0
#if DEBUG_USB_GPIB
#    define usb_gpib_debug(fmt,...)                  \
        do                                           \
        {                                            \
            printf("QT-Hal: ");                      \
            printf(fmt,##__VA_ARGS__);               \
        }while(0)
#else
#    define usb_gpib_debug(fmt,...)
#endif
#define usb_gpib_print(fmt,...) do{printf("%s %u:",__FILE__,__LINE__);\
                                   printf(fmt,##__VA_ARGS__); }while(0)

#include "usbgpib_user.h"

/* 错误 */
#define USB_GPIB_NO_ERROR (0)
#define USB_GPIB_ERROR    (-1)

/* USB-GPIB设备节点名称 */
#define USB_GPIB_DEV_NAME        "/dev/usbgpib0"
#define INSMOD_USB_GPIB_DRIVE    "insmod /rigol/drivers/usb_gpib.ko"
#define SCPI_PARSEEND_CMD        ":gpib:parse:end"

/* USB-GPIB设置地址的命令 */
#define USB_GPIB_IOCTL_SET_USB_GPIB_ADDR	_IO(USBTMC_IOC_NR, 10)
//设置读数据标志，用于判断是第一次读取还是续读
#define USBTMC_IOCTL_SET_USB_GPIB_RD_FLAG	_IO(USBTMC_IOC_NR, 11)

#define USBTMC_IOCTL_INDICATOR_PULSE       _IO(USBTMC_IOC_NR, 1)
#define USBTMC_IOCTL_CLEAR		           _IO(USBTMC_IOC_NR, 2)
#define USBTMC_IOCTL_ABORT_BULK_OUT        _IO(USBTMC_IOC_NR, 3)
#define USBTMC_IOCTL_ABORT_BULK_IN	       _IO(USBTMC_IOC_NR, 4)
#define USBTMC_IOCTL_CLEAR_OUT_HALT	       _IO(USBTMC_IOC_NR, 6)
#define USBTMC_IOCTL_CLEAR_IN_HALT	       _IO(USBTMC_IOC_NR, 7)

/* USB-GPIB每次获取最大的数据长度 */
#define USB_GPIB_TX_RX_MAX_LEN        1024
#define USB_GPIB_READ_DATA_MAX_LEN    USB_GPIB_TX_RX_MAX_LEN
#define USB_GPIB_WR_DATA_MAX_LEN      USB_GPIB_TX_RX_MAX_LEN
#define USBTMC_HEADER_SIZE    12

/* GPIB默认地址 */
#define USB_GPIB_DEFAULT_ADDRESS 1


/* 接收数据包 */
struct halUsbGpibRxStru
{
    struct halUsbtmcHeaderStru stUsbmcHeader;
    u8  au8UsbGpibRxData[USB_GPIB_READ_DATA_MAX_LEN]; /* usb-gpib接收缓冲区，最大为1024字节 */
};

/* 数据结构 */
struct halUsbGpibStru
{
    s8  as8UsbGpibName[40];  /* USB-GPIB设备节点文件名 */
    s32 s32Fd;               /* 文件句柄 */
    u8  u8Work;              /* USB-GPIB循环是否工作的标志，为1工作，为0不工作 */
    u8  u8bTag;              /* USBTMC数据包的需要 */
    u8  u8bTagLast;          /* USBTMC数据包的需要，最新使用的序号 */
    u16 u16GpibAddr;         /* UPIB地址 */
    struct halUsbGpibRxStru stUsbGpibRxData;
    //u8  au8UsbGpibRxData[USB_GPIB_READ_DATA_MAX_LEN]; /* usb-gpib接收缓冲区，最大为1024字节 */
    u8  au8UsbGpibTxData[USB_GPIB_WR_DATA_MAX_LEN];   /* usb-gpib发送缓冲区，最大为1024字节 */
    pthread_mutex_t mutex;   /* 用于设置循环标记时保护 */
}

stUsbGpib = {
    .as8UsbGpibName = USB_GPIB_DEV_NAME,
    .s32Fd = -1,
    .u8Work = 0,
    .u8bTag = 1,
    .u8bTagLast = 0,
    .u16GpibAddr = USB_GPIB_DEFAULT_ADDRESS,
    .mutex = PTHREAD_MUTEX_INITIALIZER,
};


/*******************************************************************************
  * 函    数：halUsbGpibWr
  * 描    述：将数据从USB-HOST口发送出去，注意这里发送的是数据，该函数会自动添加
  *           USBTMC的数据包头
  * 输入参数：
  *           参数名称      参数类型    参数说明
  * 输出参数：
  *           参数名称      参数类型    参数说明
  * 返 回 值：
  * 说    明：内部函数
 ******************************************************************************/
static s32 halUsbGpibWr(u8 *pu8UsbpibData, u32 u32DataLen, u8 u8Eom)
{
    s32 s32Fd = stUsbGpib.s32Fd;
    struct halUsbtmcHeaderStru stUsbtmcWrHeader; //数据包需要的TMC头
    s32 s32Ret;

    //判断设备节点是否打开成功
    if(s32Fd < 0)
    {
        usb_gpib_print("Write error, device is not opened!\n");
        return USB_GPIB_ERROR;
    }

    //填充数据头
    s32Ret = halUsbtmcHeaderSet(&stUsbtmcWrHeader, DEV_DEP_MSG_OUT, stUsbGpib.u8bTag,
                                   u32DataLen, u8Eom, 0);
    if(s32Ret != USB_GPIB_NO_ERROR)
    {
        return USB_GPIB_ERROR;
    }

    //保存本次使用的序号，序号计数自动加1
    stUsbGpib.u8bTagLast = stUsbGpib.u8bTag;
    stUsbGpib.u8bTag ++;
    if(stUsbGpib.u8bTag == 0)
        stUsbGpib.u8bTag ++;

    //把TMC头写出去
    s32Ret = write(s32Fd, (u8*)&stUsbtmcWrHeader, USBTMC_HEADER_SIZE);
    if(s32Ret != USBTMC_HEADER_SIZE)
    {
        printf("write usbtmc header error!\n");
        return USB_GPIB_ERROR;
    }

    //写入数据
    s32Ret = write(s32Fd, pu8UsbpibData, u32DataLen);
    if(s32Ret != (s32)u32DataLen)
    {
        printf("write data error!\n");
        return USB_GPIB_ERROR;
    }

    return USB_GPIB_NO_ERROR;
}

/*******************************************************************************
  * 函    数：halUsbGpibSCPIOpenIO
  * 描    述：解决USB-GPIB的BUG，当PC发送无返回或者错误的命令到仪器时，USB-GPIB
  *           要求返回一个“Parse End”字符串
  * 输入参数：无
  * 输出参数：无
  * 返 回 值：无
  * 说    明：外部函数，用于解决USB-GPIB自带的BUG
 ******************************************************************************/
void halUsbGpibSCPIOpenIO(void)
{
    u8 au8Data[10] = "Parse End";

    halUsbGpibWr(au8Data, 9, HAL_USBTMC_HEADER_EOM_1);
    stUsbGpib.u8bTagLast = stUsbGpib.u8bTag;
    stUsbGpib.u8bTag ++;
    if(stUsbGpib.u8bTag == 0)
        stUsbGpib.u8bTag ++;
}

/*******************************************************************************
  * 函    数：halUsbGpibCopyDataFromSCPI
  * 描    述：将数据拷贝到本模块，然后等待发送出去
  * 输入参数：无
  * 输出参数：无
  * 返 回 值：无
  * 说    明：外部函数
 ******************************************************************************/
void halUsbGpibCopyDataFromSCPI(void)
{
    s32 s32ReadRet = 0;
    s32 s32Eof = 0;

    //获取锁
    pthread_mutex_lock(&stUsbGpib.mutex);

    //循环从SPCI读取数据并将数据写入USB-GPIB模块
    do{
        //先清空发送缓冲区
        memset(stUsbGpib.au8UsbGpibTxData, 0, USB_GPIB_WR_DATA_MAX_LEN);

        //拷贝数据到Buffer中，但是需要注意不能覆盖已经存在的数据
        assert( g_stSCPIIO.pfunTxData2IOBuf != NULL );
        s32ReadRet = g_stSCPIIO.pfunTxData2IOBuf(stUsbGpib.au8UsbGpibTxData, USB_GPIB_WR_DATA_MAX_LEN, &s32Eof);

        if( (s32ReadRet > 0)/* && (HAL_USBTMC_TX_ON == stUsbGpib.u8TxEnable) */)
        {
            if(s32Eof == HAL_USBTMC_HEADER_EOM_1)
            {
                s32ReadRet = halUsbGpibWr(stUsbGpib.au8UsbGpibTxData, s32ReadRet-1, HAL_USBTMC_HEADER_EOM_1);
            }
            else
            {
                s32ReadRet = halUsbGpibWr(stUsbGpib.au8UsbGpibTxData, s32ReadRet, HAL_USBTMC_HEADER_EOM_0);
            }
        }
        else
        {
            usb_gpib_print("To get the data length is zero!\n");
            break;
        }

    }while( (s32Eof == HAL_USBTMC_HEADER_EOM_0) && (s32ReadRet == USB_GPIB_NO_ERROR) );

    pthread_mutex_unlock(&stUsbGpib.mutex);
}

/*******************************************************************************
  * 函    数：halUsbGpibSendToSCPI
  * 描    述：scpi从TMC中接收数据
  * 输入参数：
  *           参数名称      参数类型    参数说明
  *           pu8buf        u8*         读取到的数据数组指针
  *           u32Len        u32         数据长度
  *           u8LastPacket  u8          表示是否是最后一包数据
  * 输出参数：无
  * 返 回 值：无
  * 说    明：内部函数
 ******************************************************************************/
static void halUsbGpibSendToSCPI(u8 *pu8buf, u32 u32Len, u8 u8Eom)
{
    //线程操作，先获取线程锁
    pthread_mutex_lock(&stUsbGpib.mutex);

#if DEBUG_USB_GPIB
    {
        u32 i;
        //打印出数据，然后填充需要返回的数据即可
        usb_gpib_debug("len = %d, eom = %d, recv data: ", u32Len, u8Eom);
        for(i = 0; i < u32Len; i++)
            printf("%c", pu8buf[i]);
        printf("\n");
    }
#endif

#if 0 //!test
    {
        u8 au8Data[] = "RIGOL TECHNOLOGIES,DG888,DG80000000001,00.04.03.SP2";
        u32 u32DataLen = strlen((const s8*)au8Data);

        //将数据写下去
        halUsbGpibWr(au8Data, u32DataLen, HAL_USBTMC_HEADER_EOM_1);
    }
#else
    /* 调用SCPI的回调函数，g_stSCPIIO是SCPI模块中的一个结构体变量，结构体成员全为回调函数指针
     * 这里调用写入（即读取）数据
     */
    assert( g_stSCPIIO.pfunWriteInputQueue != NULL );
    g_stSCPIIO.pfunWriteInputQueue((s8*)pu8buf, u32Len, u8Eom, SCP_GPIB);

    //!强行在每次数据后加入一条跟踪指令，解决rigol的GPIB模块的bug。
    //! :gpib:parse:end 在 usbgpibinterface.cpp中实现
    //! 当有数据返回时，这条指令什么也不做， 当设置指令或错误指令等无返回的指令时，返回“Parse End”字符串。
    if(HAL_USBTMC_HEADER_EOM_1 == u8Eom)
    {
        u8  au8Data[] = SCPI_PARSEEND_CMD;
        u32 u32DataLen = strlen((const s8*)au8Data);
        g_stSCPIIO.pfunWriteInputQueue((s8*)au8Data, u32DataLen, HAL_USBTMC_HEADER_EOM_1, SCP_GPIB);
    }
#endif
    //释放线程锁
    pthread_mutex_unlock(&stUsbGpib.mutex);
}


/*******************************************************************************
  * 函    数：halUsbGpibRd
  * 描    述：从USB-GPIB的USB HOST口读取数据，读取数据的流程是：
  *           1.首先写入索要包数据，该数据包中的数据长度为12字节，表示只索要一个TMC头
  *           2.然后读取返回的数据包头，从中获取需要返回的数据长度，如果无数据则返回，
  *             如果数据长度大于0（有数据），则进行第3步
  *           3.一次性读取PC传过来的数据，如果数据过长，则需要多次读取，读取完毕之后
  *             返回读取的数据长度即可
  * 输入参数：
  *           参数名称      参数类型    参数说明
  * 输出参数：
  *           参数名称      参数类型    参数说明
  * 返 回 值：读取的数据长度，如果小于0则表示读取错误或失败
  * 说    明：内部函数
 ******************************************************************************/
static s32 halUsbGpibRd(void)
{
    s32 s32Fd = stUsbGpib.s32Fd;
    struct halUsbtmcHeaderStru stUsbtmcWrHeader; //查询包需要的TMC头
    struct halUsbtmcHeaderStru *pstUsbtmcRdHeader; //读取到的数据头
    u32 u32LastRdLen, u32ReadyDataLen, u32RemainLen;
    s32 s32Ret;

    //取出保存数据地址
    pstUsbtmcRdHeader = &stUsbGpib.stUsbGpibRxData.stUsbmcHeader;

    //先写入索要包数据
    //填充数据头，第一次索要长度为12字节，即索要一个头部信息即可
    s32Ret = halUsbtmcHeaderSet(&stUsbtmcWrHeader,
                                REQUEST_DEV_DEP_MSG_IN,
                                stUsbGpib.u8bTag,
                                USBTMC_HEADER_SIZE,
                                HAL_USBTMC_HEADER_EOM_1,
                                HAL_USBTMC_HEADER_TERM_CH);

    if(s32Ret != USB_GPIB_NO_ERROR)
    {
        return USB_GPIB_ERROR;
    }

    //保存本次使用的序号
    stUsbGpib.u8bTagLast = stUsbGpib.u8bTag;

    //把TMC头写出去
    s32Ret = write(s32Fd, (u8*)&stUsbtmcWrHeader, USBTMC_HEADER_SIZE);
    if(s32Ret != USBTMC_HEADER_SIZE)
    {
        usb_gpib_print("write error!\n");
        return USB_GPIB_ERROR;
    }

    //! 序号计数自动加1
    stUsbGpib.u8bTag ++;
    if(stUsbGpib.u8bTag == 0)
        stUsbGpib.u8bTag ++;

    //设置标志，这是第一次读取，内容有TMC头，方便驱动层循环接收
    ioctl(s32Fd, USBTMC_IOCTL_SET_USB_GPIB_RD_FLAG, 1);

    //读取12字节的头部数据，用于判断需要接收数据的长度
    s32Ret = read(s32Fd, pstUsbtmcRdHeader, USBTMC_HEADER_SIZE);
    if(s32Ret < USBTMC_HEADER_SIZE)
    {
        usb_gpib_print("read TMC Header error, s32Ret = %d!\n", s32Ret);
        return USB_GPIB_ERROR;
    }

    //判断是否为返回包
    if(pstUsbtmcRdHeader->u8MsgID != DEV_DEP_MSG_IN)
    {
        usb_gpib_print("Device sent reply with wrong MsgID: %u != 2\n", pstUsbtmcRdHeader->u8MsgID);
        return USB_GPIB_ERROR;
    }

    //判断是否为本次索要的数据包
    if(pstUsbtmcRdHeader->u8bTag != stUsbGpib.u8bTagLast)
    {
        usb_gpib_print("Device sent reply with wrong bTag: %u != %u\n",
                            pstUsbtmcRdHeader->u8bTag, stUsbGpib.u8bTagLast);
        return USB_GPIB_ERROR;
    }

    //计算仪器传输数据的长度（即需要回读的数据总长度）
    //FUCK!!! USB-GPIB发送的数据长度顺序有BUG，没有按照USBTMC协议来
    //协议中TMC头部第4字节为最低位，第7字节为最高位，该模块是反的。。。
    //u32ReadyDataLen = pstUsbtmcRdHeader->u32Size;
    u32ReadyDataLen = ((pstUsbtmcRdHeader->u32Size & 0x000000FF) << 24) |
                      ((pstUsbtmcRdHeader->u32Size & 0x0000FF00) << 8)  |
                      ((pstUsbtmcRdHeader->u32Size & 0x00FF0000) >> 8)  |
                      ((pstUsbtmcRdHeader->u32Size & 0xFF000000) >> 24);


    //没有需要读取的数据，这是循环查询出现的情况，直接返回即可
    if(u32ReadyDataLen == 0)
        return USB_GPIB_NO_ERROR;

    //debug
    usb_gpib_debug("there are %d data is ready!\n", u32ReadyDataLen);

    /** 以下是一次性读取所有剩余数据 **/
    //读取数据，就要通知驱动这个包不含头部信息
    ioctl(s32Fd, USBTMC_IOCTL_SET_USB_GPIB_RD_FLAG, 0);

    //使用固定的区域读取数据，缓冲区大小为1024字节，因此如果大于1024字节需要多次读取
    //保存需要读取的数据长度
    u32RemainLen = u32ReadyDataLen;
    do{
        //然后将缓冲区清空
        memset(stUsbGpib.stUsbGpibRxData.au8UsbGpibRxData, 0, USB_GPIB_READ_DATA_MAX_LEN);

        //计算本次需要读取的长度
        u32LastRdLen = u32RemainLen < USB_GPIB_READ_DATA_MAX_LEN ? u32RemainLen : USB_GPIB_READ_DATA_MAX_LEN;

        //读取
        s32Ret = read(s32Fd, stUsbGpib.stUsbGpibRxData.au8UsbGpibRxData, u32LastRdLen);
        if(s32Ret < (s32)u32LastRdLen)
        {
            //既然读取失败，就返回呗
            usb_gpib_print("read last packet failed, s32Ret = %d!\n", s32Ret);
            break;
        }

        //计算还剩多少数据需要读取
        u32RemainLen -= u32LastRdLen;

        //读取完毕了，就将数据发给SCPI就可以了
        if(u32RemainLen)
        {
            usb_gpib_debug("read part, write to SCPI\n");
            halUsbGpibSendToSCPI(stUsbGpib.stUsbGpibRxData.au8UsbGpibRxData, u32LastRdLen, HAL_USBTMC_HEADER_EOM_0);
        }
        else
        {
            usb_gpib_debug("read part end, write to SCPI\n");
            halUsbGpibSendToSCPI(stUsbGpib.stUsbGpibRxData.au8UsbGpibRxData, u32LastRdLen, HAL_USBTMC_HEADER_EOM_1);
        }

    }while(u32RemainLen);

    //读取数据，就要通知驱动这个包不含头部信息
    ioctl(s32Fd, USBTMC_IOCTL_SET_USB_GPIB_RD_FLAG, 1);

    //返回数据总长
    return u32ReadyDataLen;
}


/*******************************************************************************
  * 函    数：halUsbGpibStartWork
  * 描    述：启动USB-GPIB设备，启动之后本部分代码自动监听，然后自动调用SCPI
  *           获取返回值，然后自动回复上位机。
  *           该函数正常情况下不会返回，当有返回时说明设备已经移除，所以，需要
  *           单独为该函数实现的功能开一个线程。
  * 输入参数：无
  * 输出参数：无
  * 返 回 值：错误代码
  * 说    明：外部函数
 ******************************************************************************/
s32 halUsbGpibStartWork(void)
{
    //开始工作，这个标志可能会在别的地方被修改
    pthread_mutex_lock(&stUsbGpib.mutex);
    stUsbGpib.u8Work = 1;
    pthread_mutex_unlock(&stUsbGpib.mutex);

    while(stUsbGpib.u8Work)
    {
#if 0
        //等待设备插入
        if(access(stUsbGpib.as8UsbGpibName, F_OK) != 0)
        {
            if( stUsbGpib.s32Fd != (-1) )
            {
                close(stUsbGpib.s32Fd);
                stUsbGpib.s32Fd = -1;
                g_stSCPIIO.pfunLinkStatus(0);
            }
            usleep(100000);
            continue;
        }

        //!重新初始化设备
        if(stUsbGpib.s32Fd < 0)
        {
            //!打开设备文件
            if(USB_GPIB_ERROR == halUsbGpibInit())
            {
                return USB_GPIB_ERROR;
            }

            //设置一下GPIB地址
            if(USB_GPIB_ERROR == halUsbGpibSetAddress(stUsbGpib.u16GpibAddr))
            {
                return USB_GPIB_ERROR;
            }

            g_stSCPIIO.pfunLinkStatus(1);
        }

        //读取数据，每次最大读取长度为1024字节
        halUsbGpibRd();
#else
        if(stUsbGpib.s32Fd != (-1) )
        {
            s32 err = halUsbGpibRd();

            //!重新初始化设备
            if(USB_GPIB_ERROR == err)
            {
                if(USB_GPIB_ERROR == halUsbGpibInit())
                {
                    return USB_GPIB_ERROR;
                }
            }
        }
#endif
        usleep(100000);
    }
    return USB_GPIB_NO_ERROR;
}


/*******************************************************************************
  * 函    数：halUsbGpibStopWork
  * 描    述：停止USB-GPIB设备的工作，这样，在启动工作时创建的线程可能会退出
  *           停止的原理很简单，其实就是退出本部分的循环，导致启动函数退出
  * 输入参数：
  *           参数名称      参数类型    参数说明
  * 输出参数：
  *           参数名称      参数类型    参数说明
  * 返 回 值：
  * 说    明：外部函数
 ******************************************************************************/
s32 halUsbGpibStopWork(void)
{
    //判断是否已经开始
    if(!stUsbGpib.u8Work)
    {
        //如果没有开始，则直接返回停止成功即可
        return USB_GPIB_NO_ERROR;
    }

    //如果开始，则强制结束即可，注意需要加锁
    pthread_mutex_lock(&stUsbGpib.mutex);
    stUsbGpib.u8Work = 0;
    pthread_mutex_unlock(&stUsbGpib.mutex);

    //返回
    return USB_GPIB_NO_ERROR;
}


/*******************************************************************************
  * 函    数：halUsbGpibSetAddress
  * 描    述：设置USB-GPIB模块的地址，该地址是GPIB的地址
  * 输入参数：
  *           参数名称      参数类型    参数说明
  *           u16Addr       u16         GPIB的地址
  * 输出参数：无
  * 返 回 值：错误代码，0-设置成功，其他-设置失败
  * 说    明：外部函数
 ******************************************************************************/
s32 halUsbGpibSetAddress(u16 u16Addr)
{
    s32 s32Ret;

    //首先判断文件是否打开
    if(stUsbGpib.s32Fd < 0)
    {
        //设备文件未打开，设置失败
        usb_gpib_print("USB-GPIB is not opened!\n");
        return USB_GPIB_ERROR;
    }

    //使用ioctl设置GPIB地址
    s32Ret = ioctl(stUsbGpib.s32Fd, USB_GPIB_IOCTL_SET_USB_GPIB_ADDR, u16Addr);

    if(s32Ret <0 )
    {
        usb_gpib_print("USB-GPIB set address failed!\n");
        return USB_GPIB_ERROR;
    }

    //保存GPIB地址
    stUsbGpib.u16GpibAddr = u16Addr;
    return USB_GPIB_NO_ERROR;
}


/*******************************************************************************
  * 函    数：halUsbGpibInit
  * 描    述：USB-GPIB设备初始化，即设备的打开
  * 输入参数：无
  * 输出参数：无
  * 返 回 值：错误代码，0-初始化成功，其他-初始化失败
  * 说    明：外部函数
 ******************************************************************************/
s32 halUsbGpibInit(void)
{
    s32 s32Fd = 0;

    //!关闭旧设备
    if((-1) != stUsbGpib.s32Fd)
    {
        close(stUsbGpib.s32Fd);
    }

    //打开设备
    s32Fd = open((const s8*)stUsbGpib.as8UsbGpibName, O_RDWR);
    if (s32Fd < 0)
    {
        usb_gpib_print("Can not open %s, fd: %d\n", stUsbGpib.as8UsbGpibName, s32Fd);
        return USB_GPIB_ERROR;
    }

    //保存文件句柄
    stUsbGpib.s32Fd = s32Fd;

    //! 默认设置
    stUsbGpib.u8bTag = 1;
    stUsbGpib.u8bTagLast = 0;

    return USB_GPIB_NO_ERROR;
}


/*******************************************************************************
  * 函    数：halUsbGpibThread
  * 描    述：创建线程完成，具体执行
  * 输入参数：无
  * 输出参数：无
  * 返 回 值：线程参数
  * 说    明：内部函数
 ******************************************************************************/
static void *halUsbGpibThread(void *arg)
{
    (void)arg; //无意义，用于避免没有使用arg引起的编译警告

    //!安装驱动。
    system(INSMOD_USB_GPIB_DRIVE);

    //！线程开始工作 while(1)
    halUsbGpibStartWork();

    //线程退出
    usb_gpib_debug("thread exit!\n");
    pthread_exit((void *)1);
}


/*******************************************************************************
  * 函    数：halUsbGpibStart
  * 描    述：这是一个测试函数，用于开辟USB-GPIB模块监听的线程，理论上该线程不应该
  *           开在HAL层，因此，该函数只用作测试使用，在应用层开辟线程时可以参考这个函数
  * 输入参数：无
  * 输出参数：无
  * 返 回 值：线程号
  * 说    明：外部函数，但是只用作测试，需要在应用层模拟该函数的实现
 ******************************************************************************/
pthread_t halUsbGpibStart(void)
{
    pthread_t thread_id;         //线程号
    pthread_attr_t unThreadAttr; //线程属性，联合体
    struct sched_param stParam;  //线程参数，指明所要设置的静态线程优先级

    //初始化线程属性
    pthread_attr_init(&unThreadAttr);
    //获取默认的线程属性，保存在刚初始化的线程属性变量中
    pthread_attr_getschedparam(&unThreadAttr, &stParam);
    //设置线程的优先级值，该数值越大优先级越高，可以通过sched_get_priority_max函数
    //和sched_get_priority_min函数获取系统所支持的最大和最小优先级值
    stParam.sched_priority = 11;
    //设置优先级
    pthread_attr_setschedparam(&unThreadAttr, &stParam);
    //设置线程策略，SCHED_FIFO仅在优先级大于0的线程中使用，且只能在超级用户下使用
    //线程策略包括：默认分时SCHED_OTHER，实时的先进先出SCHED_FIFO，增强FIFO的SCHED_RR
    //pthread_attr_setschedpolicy(&unThreadAttr, SCHED_OTHER);
    //pthread_attr_setschedpolicy(&unThreadAttr, SCHED_FIFO);
    pthread_attr_setschedpolicy(&unThreadAttr, SCHED_RR);
    //创建线程
    pthread_create(&thread_id, &unThreadAttr, halUsbGpibThread, NULL);
    //注销已使用的属性参数
    pthread_attr_destroy(&unThreadAttr);

    //返回线程号
    return thread_id;
}

/* end of file */



void halUsbGpibCheckLink()
{
    //等待设备插入
    if(access(stUsbGpib.as8UsbGpibName, F_OK) != 0)
    {
        if( stUsbGpib.s32Fd != (-1) )
        {
            close(stUsbGpib.s32Fd);

            pthread_mutex_lock(&stUsbGpib.mutex);
            stUsbGpib.s32Fd = -1;
            pthread_mutex_unlock(&stUsbGpib.mutex);

            g_stSCPIIO.pfunLinkStatus(0);
        }
    }
    else
    {
        //!重新初始化设备
        if(stUsbGpib.s32Fd < 0)
        {
            //!打开设备文件
            if(USB_GPIB_ERROR == halUsbGpibInit())
            {
                return;
            }

            //设置一下GPIB地址
            if(USB_GPIB_ERROR == halUsbGpibSetAddress(stUsbGpib.u16GpibAddr))
            {
                return;
            }
            g_stSCPIIO.pfunLinkStatus(1);
        }
    }
}
