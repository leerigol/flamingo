#include "usbgpib_user.h"
#include <stdio.h>
#include <string.h>

#include "./HalUsbGpib.h"

struct usbgpibCallbackFunc g_stSCPIIO = {
    .pfunTxData2IOBuf    = NULL,
    .pfunWriteInputQueue = NULL,
    .pfunLinkStatus      = NULL,
};

static u8 au8Data[] = "RIGOL TECHNOLOGIES, TEST , TEST";

__attribute__((weak)) s32  usbgpibTxCallback(u8 *pBuf, u32 len,  int* const pEof)
{
    u32 slen = strlen((const s8*)au8Data);
    assert(pBuf != NULL);

    slen = len<slen ? len: slen;

    memcpy(pBuf, au8Data, slen);

    *pEof = 1;

    return slen;
}

__attribute__((weak)) void usbgpibRxCallback(s8 *pBuf, u32 len, u8 /*eom*/, int /*id*/)
{
    u32 i = 0;
    for(i = 0; i < len; i++)
        printf("%c", pBuf[i]);
        printf("\n");
}

__attribute__((weak)) void usbgpibLinkCallback(int status)
{
    printf("link %d", status);
}

__attribute__((weak)) void usbgpibCallbackRegister()
{
    g_stSCPIIO.pfunTxData2IOBuf    = usbgpibTxCallback;
    g_stSCPIIO.pfunWriteInputQueue = usbgpibRxCallback;
    g_stSCPIIO.pfunLinkStatus      = usbgpibLinkCallback;
}

int usbgpibInit()
{
    usbgpibCallbackRegister();

    assert( g_stSCPIIO.pfunTxData2IOBuf    != NULL );
    assert( g_stSCPIIO.pfunWriteInputQueue != NULL );
    assert( g_stSCPIIO.pfunLinkStatus      != NULL );

    return (int)halUsbGpibStart();
}

void usbgpibOutRequest()
{
   halUsbGpibCopyDataFromSCPI();
}

int usbgpibSetAddress(u16 u16Addr)
{
    return  (int)halUsbGpibSetAddress(u16Addr);
}

void usbgpibParseEnd()
{
    halUsbGpibSCPIOpenIO();
}

void usbGpibCheckLink()
{
    halUsbGpibCheckLink();
}
