/*******************************************************************************
                      普源精电科技有限公司版权所有
********************************************************************************
  文 件 名: HalUsbtmc.c
  功能描述: 针对所有使用到USBTMC协议模块书写的HAL层文件，源文件
  作    者: sn02241
  版    本: V1.00
  创建日期: 2017-10-30

  修改历史:
  作者          修改时间            版本        修改内容
  sn02241       2017.10.30          V1.00       initial

*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <asm/ioctls.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <linux/usb/tmc.h>
#include "DataType.h"
#include "HalUsbtmc.h"

/** 宏定义 **/
#define HAL_USBTMC_NO_ERROR     0
#define HAL_USBTMC_ERROR       -1



/*******************************************************************************
  * 函    数：halUsbtmcHeaderSet
  * 描    述：设置USBTMC协议的数据包头
  * 输入参数：
  *           参数名称      参数类型          参数说明
  *           euMsgID       halUsbtmcMsgEu    USBTMC数据包类型
  *           u8bTag        u8                USBTMC数据包的序号
  *           u32Size       u32               数据的长度
  *           u8Eom         u8                数据包是否完整标志
  * 输出参数：
  *           参数名称         参数类型              参数说明
  *           pstUsbtmcHeader  halUsbtmcHeaderStru   USBTMC数据头的结构体变量指针
  * 返 回 值：错误代码，0-表示正确，-1表示错误
  * 说    明：外部函数
 ******************************************************************************/
s32 halUsbtmcHeaderSet(struct halUsbtmcHeaderStru *pstUsbtmcHeader, halUsbtmcMsgEu euMsgID,
                                      u8 u8bTag, u32 u32Size, u8 u8Eom, u8 u8TermChar)
{
    //判断传入的参数是否合法
    if(!pstUsbtmcHeader)
    {
        printf("USBTMC Header buffer point is NULL!\n");
        return HAL_USBTMC_ERROR;
    }

    //然后对输出参数赋值即可
    pstUsbtmcHeader->u8MsgID = euMsgID;
    pstUsbtmcHeader->u8bTag  = u8bTag;
    pstUsbtmcHeader->u8bTagInverse = ~u8bTag;
    pstUsbtmcHeader->u8Res0  = 0;
    pstUsbtmcHeader->u32Size = u32Size;

    //非公共内容的赋值
    switch(euMsgID)
    {
        case DEV_DEP_MSG_OUT:
            pstUsbtmcHeader->ubmMsg.stMsgOut.u8Eom = u8Eom;
            pstUsbtmcHeader->ubmMsg.stMsgOut.u8Res[0] = 0;
            pstUsbtmcHeader->ubmMsg.stMsgOut.u8Res[1] = 0;
            pstUsbtmcHeader->ubmMsg.stMsgOut.u8Res[2] = 0;
            break;
        case REQUEST_DEV_DEP_MSG_IN:
            pstUsbtmcHeader->ubmMsg.stReqMsgIn.u8bmTransferAttr = 0x02;
            pstUsbtmcHeader->ubmMsg.stReqMsgIn.TermChar = u8TermChar;
            pstUsbtmcHeader->ubmMsg.stReqMsgIn.u8Res[0] = 0;
            pstUsbtmcHeader->ubmMsg.stReqMsgIn.u8Res[1] = 0;
            break;
        default:
            break;
    }

    //返回成功
    return HAL_USBTMC_NO_ERROR;
}




/* end of file */


