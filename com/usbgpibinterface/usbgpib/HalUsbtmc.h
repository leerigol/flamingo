/*******************************************************************************
                      普源精电科技有限公司版权所有
********************************************************************************
  文 件 名: HalUsbtmc.h
  功能描述: 针对所有使用到USBTMC协议模块书写的HAL层文件，头文件
  作    者: sn02241
  版    本: V1.00
  创建日期: 2017-10-30

  修改历史:
  作者          修改时间            版本        修改内容
  sn02241       2017.10.30          V1.00       initial

*******************************************************************************/

#ifndef _HAL_USB_TMC_H
#define _HAL_USB_TMC_H


#include "DataType.h"

#ifdef __cplusplus
extern "C" {
#endif


/* 数据包头的包完整标志，1表示数据包完整，0表示不完整 */
#define HAL_USBTMC_HEADER_EOM_1  1
#define HAL_USBTMC_HEADER_EOM_0  0

/* USBTMC索要包头中的结束符 */
#define HAL_USBTMC_HEADER_TERM_CH 0x0A

/* usbtmc协议中对数据包类型的规定 */
typedef enum
{
    DEV_DEP_MSG_OUT            = 1,
    REQUEST_DEV_DEP_MSG_IN     = 2,
    DEV_DEP_MSG_IN             = 2,
    VENDOR_SPECIFIC_OUT        = 126,
    REQUEST_VENDOR_SPECIFIC_IN = 127,
    VENDOR_SPECIFIC_IN         = 127,
}halUsbtmcMsgEu;

/* DEV_DEP_MSG_OUT类型包的头部后半部分，Host->device */
struct halUsbGpibHeaderStru_MSG_OUT
{
    u8 u8Eom;    /* 完整包标志，D0=1,包完整 */
    u8 u8Res[3]; /* 保留 */
};

/* REQUEST_DEV_DEP_MSG_IN类型包的头部后半部分，Host->device, request */
struct halUsbGpibHeaderStru_REQ_MSG_IN
{
    u8 u8bmTransferAttr; /* 结束符属性，D1=1,使能结束符 */
    u8 TermChar;         /* 结束符 */
    u8 u8Res[2];         /* 保留 */
};

/* DEV_DEP_MSG_IN类型包的头部后半部分，device response */
struct halUsbGpibHeaderStru_MSG_IN
{
    u8 u8bmTransferAttr; /* 结束符属性，D1=1, 支持结束符，结束符使能属性由REQ包发送得到，
                            本包数据最后一个字节为结束符 */
                         /*             D0=1, EOM */
    u8 u8Res[3];         /* 保留 */
};

/* usbtmc协议的数据包头信息，每个包头12Bytes */
struct halUsbtmcHeaderStru{
    u8  u8MsgID;       /* [0]: usbtmc包类型，1-主动发的包，2-回复包 */
    u8  u8bTag;        /* [1]: 包的序号，[1, 255]，回复时必须与询问包一致 */
    u8  u8bTagInverse; /* [2]: 包的反序号，[1, 255]，回复时必须与询问包一致 */
    u8  u8Res0;        /* [3]: 保留 */
    u32 u32Size;       /* [4..7]: 数据的长度位 */
#if 1
    union{
        struct halUsbGpibHeaderStru_MSG_OUT    stMsgOut;   //DEV_DEP_MSG_OUT
        struct halUsbGpibHeaderStru_REQ_MSG_IN stReqMsgIn; //REQUEST_DEV_DEP_MSG_IN
        struct halUsbGpibHeaderStru_MSG_IN     stMsgIn;    //DEV_DEP_MSG_IN
    }ubmMsg;
#else
    u8  u8Eom;         /* [8]: EOM是否完整标志，1-完整，0-不完整 */
    u8  u8TermChar;    /* [9]: 终止符 */
    u8  u8Resv[2];     /* [10..11]: 保留 */
#endif
};


/*******************************************************************************
  * 函    数：halUsbtmcHeaderSet
  * 描    述：设置USBTMC协议的数据包头
  * 输入参数：
  *           参数名称      参数类型          参数说明
  *           euMsgID       halUsbtmcMsgEu    USBTMC数据包类型
  *           u8bTag        u8                USBTMC数据包的序号
  *           u32Size       u32               数据的长度
  *           u8Eom         u8                数据包是否完整标志
  * 输出参数：
  *           参数名称         参数类型              参数说明
  *           pstUsbtmcHeader  halUsbtmcHeaderStru   USBTMC数据头的结构体变量指针
  * 返 回 值：错误代码，0-表示正确，-1表示错误
  * 说    明：外部函数
 ******************************************************************************/
s32 halUsbtmcHeaderSet(struct halUsbtmcHeaderStru *pstUsbtmcHeader, halUsbtmcMsgEu euMsgID,
                                      u8 u8bTag, u32 u32Size, u8 u8Eom, u8 u8TermChar);



#ifdef __cplusplus
}
#endif

#endif //#ifndef _HAL_USB_TMC_H

/* end of file */


