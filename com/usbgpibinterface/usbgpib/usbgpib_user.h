#ifndef _USBGPIB_USER_H
#define _USBGPIB_USER_H

#include <assert.h>
#include "DataType.h"

#define SCP_GPIB 1

typedef s32  (*_pfunTxData2IOBuf)(u8 *pBuf, u32 len, int* const pEof );
typedef void (*_pfunWriteInputQueue)(s8 *pBuf, u32 len, u8 eom, int t);

typedef void (*_pfunLinkStatus)(int status);


struct usbgpibCallbackFunc
{
    _pfunTxData2IOBuf       pfunTxData2IOBuf;
    _pfunWriteInputQueue    pfunWriteInputQueue;
    _pfunLinkStatus         pfunLinkStatus;
};

extern struct usbgpibCallbackFunc g_stSCPIIO;

void usbgpibCallbackRegister();
int  usbgpibInit();
void usbgpibOutRequest();

int  usbgpibSetAddress(u16 u16Addr);

void usbgpibParseEnd();

void usbGpibCheckLink(void);
#endif
