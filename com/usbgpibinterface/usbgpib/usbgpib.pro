#-------------------------------------------------
#
# Project created by QtCreator 2017-12-06T14:19:07
#
#-------------------------------------------------
QT += core
QT -= gui

staticlib
TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += .

#根据环境变量 确定是否定义宏 _SIMULATE
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
}

#***********************************************************#
#工程配置
#***********************************************************#
# *1* 仅用debug模式构建
CONFIG  -= debug_and_release
CONFIG  -= release
CONFIG  -= debug_and_release_target
CONFIG  -= build_all
CONFIG  += debug

# *2* 中间文件(.o文件)路径
#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

# *3* 可执行文件(.exe文件)路径
TARGET = ../../../lib$$(PLATFORM)/scpi/usbgpib
#TARGET = ./usbgpib
#***********************************************************#
#源文件
#***********************************************************#
SOURCES += \
    HalUsbGpib.c \
    HalUsbtmc.c \
    usbgpib_user.cpp

HEADERS += \
    DataType.h \
    HalUsbGpib.h \
    HalUsbtmc.h \
    usbgpib_user.h

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}
