#ifndef CUSBGPIBINTERFACE_H
#define CUSBGPIBINTERFACE_H
#include "../scpiparse/cscpiparser.h"

class CUsbGpibInterface : public CScpiInterface
{
public:
    CUsbGpibInterface(scpiIntfType scpiType = scpi_intf_gpib, scpiIntfId id = 1);

public:
    int  setGpibAddr(int addr);
    void parseEnd();
    void checkLink(void);
public:
    void outPut(QVector<QSharedPointer<CFormater> > ppFormater, bool eof);
    void onNewConnection();
    bool getConnectionState();

public Q_SLOTS:
    void onConnection(void);
    void onDisconnected();

public Q_SLOTS:
    void onRequestOut();

private:
    volatile bool m_bParseEnd;
    bool          m_bConnection;
};

extern CUsbGpibInterface  *pGpibInterface;

#endif // CUSBGPIBINTERFACE_H
