#include "appicon.h"

#include "../../meta/crmeta.h"
#include "../../include/dsostd.h"
AppIcon::AppIcon()
{
    mMsg = 0;

    mState = app_installed;
}

AppIcon::~AppIcon()
{
    foreach( StateIcon *pItem, mStateIcons )
    {
        Q_ASSERT( NULL != pItem );

        delete pItem;
    }
}

void AppIcon::setMsg( int msg )
{ mMsg = msg; }
int AppIcon::getMsg()
{ return mMsg; }

void AppIcon::setTitleID(int tid)
{
    mTitleID = tid;
}

int AppIcon::getTitleID()
{
    return mTitleID;
}

void AppIcon::setTitle( const QString &title )
{ mTitle = title; }
QString AppIcon::getTitle()
{ return mTitle; }

void AppIcon::setServName( const QString &name )
{ mServName = name; }
QString AppIcon::getServName()
{ return mServName; }

void AppIcon::setState( AppState stat )
{ mState = stat; }
AppState AppIcon::getState()
{ return mState; }

bool AppIcon::addIcon( AppState stat, const QString &icon )
{
    StateIcon *pItem = NULL;

    //! find the stat
    foreach( StateIcon * pStatItem, mStateIcons )
    {
        Q_ASSERT( NULL != pStatItem );
        if ( pStatItem->mStat == stat )
        {
            pItem = pStatItem;
            break;
        }
    }

    //! find item
    bool bRet;
    if ( NULL != pItem )
    {
        pItem->mIcon = icon;
        bRet = true;
    }
    //! new one
    else
    {
        pItem = R_NEW( StateIcon() );
        if ( NULL == pItem )
        { return false; }

        pItem->mStat = stat;
        pItem->mIcon = icon;

        mStateIcons.append( pItem );

        bRet = true;
    }

    //! post split sub img
    r_meta::CRMeta appMeta;

    appMeta.getMetaVal( icon + "normal", pItem->mSubImgs[0] );
    appMeta.getMetaVal( icon + "down", pItem->mSubImgs[1] );

    return bRet;
}

//! by current state
bool AppIcon::getSubImage( int id, QString &subImg )
{
    //! state match
    foreach( StateIcon *pItem, mStateIcons )
    {
        Q_ASSERT( NULL != pItem );
        if ( pItem->mStat == mState )
        {
            Q_ASSERT( id >= 0 && id < array_count(pItem->mSubImgs) );

            subImg = pItem->mSubImgs[id];
            return true;
        }
    }

    //! not match
    //! the default item
    if ( mStateIcons.size() > 0 )
    {
        subImg = mStateIcons[0]->mSubImgs[ id ];
        return true;
    }

    return false;
}
