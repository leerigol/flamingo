#ifndef APPICON_H
#define APPICON_H

#include <QtCore>

enum AppState
{
    app_installed = 0x1,

    app_opened = 0x10000,

    app_disable,
};

class StateIcon
{
public:
    AppState mStat;
    QString  mIcon;     //! path

    QString  mSubImgs[2];
};

class AppIcon
{
public:
    AppIcon();
    ~AppIcon();

public:
    void setMsg( int msg );
    int getMsg();

    void setTitleID( int tid );
    int  getTitleID();
    void setTitle( const QString &title );
    QString getTitle();

    void setServName( const QString &name );
    QString getServName();

    void setState( AppState stat );
    AppState getState();

    bool addIcon( AppState stat, const QString &icon );

    bool getSubImage( int id, QString &subImg );

public:
    int mMsg;
    int     mTitleID;
    QString mTitle;
    QString mServName;

    AppState mState;
    QList<StateIcon*> mStateIcons;
};

#endif // APPICON_H
