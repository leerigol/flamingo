TEMPLATE = lib
TARGET = ../../lib$$(PLATFORM)/apps/appicon
INCLUDEPATH += .

QT += widgets

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

DEFINES += _DEBUG

CONFIG += static

# Input
HEADERS += appicon.h
SOURCES += appicon.cpp
