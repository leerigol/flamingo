#include "uiproperty.h"
#include "../../include/dsodbg.h"
namespace ui_attr {

bool operator<( const PackInt &a, const PackInt &b )
{
    if ( a.width == 32 )
    {
        return a.val32 < b.val32;
    }
    else if ( a.width == 64 )
    {
        return a.val64 < b.val64;
    }
    return false;
}

bool operator>( const PackInt &a, const PackInt &b )
{
    if ( a.width == 32 )
    {
        return a.val32 > b.val32;
    }
    else if ( a.width == 64 )
    {
        return a.val64 > b.val64;
    }
    return false;
}

PackInt::PackInt()
{
    width = -1;

    val64 = 0;

    val128.mA = 0;
    val128.mE = E_0;
}

bool PackInt::isUnk()
{
    //return ( width != 32 || width != 64 || width != 128 );
    return ( width != 32 && width != 64 && width != 128 );
}

PackInt PackInt::operator+(int val )
{
    if ( width == 32 )
    {
        val32 += val;
    }
    else if ( width == 64 )
    {
        val64 += val;
    }
    else
    {
        Q_ASSERT( false );
    }

    return *this;
}
PackInt PackInt::operator-(int val )
{
    if ( width == 32 )
    {
        val32 -= val;
    }
    else if ( width == 64 )
    {
        val64 -= val;
    }
    else
    {
        Q_ASSERT( false );
    }

    return *this;
}

PackInt PackInt::operator+(qlonglong val )
{
    if ( width == 32 )
    {
        val32 += val;
    }
    else if ( width == 64 )
    {
        val64 += val;
    }
    else
    {
        Q_ASSERT( false );
    }

    return *this;
}

bool PackInt::operator>( int val )
{
    if ( width == 32 )
    {
        return val32 > val;
    }
    else if ( width == 64 )
    {
        return val64 > val;
    }
    else
    {
        Q_ASSERT( false );
    }
}
bool PackInt::operator<( int val )
{
    if ( width == 32 )
    {
        return val32 < val;
    }
    else if ( width == 64 )
    {
        return val64 < val;
    }
    else
    {
        Q_ASSERT( false );
    }
}

bool PackInt::operator>=( int val )
{
    if ( width == 32 )
    {
        return val32 >= val;
    }
    else if ( width == 64 )
    {
        return val64 >= val;
    }
    else
    {
        Q_ASSERT( false );
    }
}
bool PackInt::operator<=( int val )
{
    if ( width == 32 )
    {
        return val32 <= val;
    }
    else if ( width == 64 )
    {
        return val64 <= val;
    }
    else
    {
        Q_ASSERT( false );
    }
}

bool PackInt::operator>( qlonglong val )
{
    if ( width == 32 )
    {
        return val32 > val;
    }
    else if ( width == 64 )
    {
        return val64 > val;
    }
    else
    {
        Q_ASSERT( false );
    }
}
bool PackInt::operator<( qlonglong val )
{
    if ( width == 32 )
    {
        return val32 < val;
    }
    else if ( width == 64 )
    {
        return val64 < val;
    }
    else
    {
        Q_ASSERT( false );
    }
}
bool PackInt::operator>=( qlonglong val )
{
    if ( width == 32 )
    {
        return val32 >= val;
    }
    else if ( width == 64 )
    {
        return val64 >= val;
    }
    else
    {
        Q_ASSERT( false );
    }
}
bool PackInt::operator<=( qlonglong val )
{
    if ( width == 32 )
    {
        return val32 <= val;
    }
    else if ( width == 64 )
    {
        return val64 <= val;
    }
    else
    {
        Q_ASSERT( false );
    }
}

PackInt PackInt::operator-(qlonglong val )
{
    if ( width == 32 )
    {
        val32 -= val;
    }
    else if ( width == 64 )
    {
        val64 -= val;
    }
    else
    {
        Q_ASSERT( false );
    }

    return *this;
}

PackInt PackInt::operator=(int val)
{
    if ( width == 32 )
    {
        val32 = val;
    }
    else if ( width == 64 )
    {
        val64 = val;
    }
    else
    {
        Q_ASSERT( false );
    }

    return *this;
}
PackInt PackInt::operator=(qlonglong val)
{
    if ( width == 32 )
    {
        val32 = val;
    }
    else if ( width == 64 )
    {
        val64 = val;
    }
    else
    {
        Q_ASSERT( false );
    }

    return *this;
}

void PackInt::set( int val )
{
    width = 32;
    val32 = val;
    val64 = val;
}
void PackInt::set( qlonglong val )
{
    width = 64;
    val64 = val;
}

void PackInt::set( dsoReal real )
{
    width = 128;
    val128 = real;
}

void PackInt::set( DsoRealType_A a, DsoE e )
{
    width = 128;
    val128.mA = a;
    val128.mE = e;
}

void PackInt::assignPower2( int scale )
{
    if ( scale <0 ) scale = 0;

    if ( width == 32 )
    {
        if ( scale >= 31 ) scale = 30;

        val32 = 1<<scale;
    }
    else if ( width == 64 )
    {
        if ( scale >= 63 ) scale = 62;

        val64 = 1ll << scale;
    }
    else
    {
        qWarning()<<"invalid power 2";
        return;
    }
}

float PackInt::realf( float fBase )
{
    return (float)reald( fBase );
}

double PackInt::reald( double dBase )
{
    if ( width == 32 )
    {
        return val32 * dBase;
    }
    else if ( width == 64 )
    {
        return val64 * dBase;
    }
    else if ( width == 128 )
    {
        return 0;
    }
    else
    {
//        Q_ASSERT( false );
        //! \todo 测试菜单第二页显示数值崩溃
        return 0.99999f;
    }
}

uiProperty::uiProperty( int msg ): mMsg(msg), mComponent(e_item), mValue(-1)
{
    mEnable = true;
    mVisible = true;

    mModifiedMask = 0;
    mValidMask = 0;

    mNodeCompress = false;
    m_pKeyRequest = NULL;

    mAttrs = property_none;
}
uiProperty::uiProperty( int msg, int opt ): mMsg(msg), mComponent(e_option), mValue(opt)
{
    mEnable = true;
    mVisible = true;

    mModifiedMask = 0;
    mValidMask = 0;

    mNodeCompress = false;
    m_pKeyRequest = NULL;
}

uiProperty::~uiProperty()
{
    if ( NULL != m_pKeyRequest )
    {
        delete m_pKeyRequest;
        m_pKeyRequest = NULL;
    }
}

void uiProperty::setEnable( bool b)
{
    if ( !is_enable_valid() )
    {
        mModifiedMask |= e_enable_mask;
    }
    else if ( b != mEnable )
    {
        mModifiedMask |= e_enable_mask;
    }

    mEnable = b;
    validate_enable();
}
bool uiProperty::getEnable()
{
    return mEnable;
}

void uiProperty::setVisible( bool b )
{
    if ( !is_visible_valid() )
    {
        mModifiedMask |= e_visible_mask;
    }
    else if ( b != mVisible )
    {
        mModifiedMask |= e_visible_mask;
    }

    mVisible = b;
    validate_visible();
}
bool uiProperty::getVisible()
{
    return mVisible;
}

void uiProperty::setStep( qlonglong step )
{
    mStep = step;
    validate_i_step();
}
qlonglong uiProperty::getStep()
{ return mStep; }

void uiProperty::setIStep( qlonglong step )
{
    mStep = step;
    validate_i_step();
}
qlonglong uiProperty::getIStep()
{ return mStep; }

void uiProperty::setDStep( qlonglong step )
{
    mDStep = step;
    validate_d_step();
}
qlonglong uiProperty::getDStep()
{ return mDStep; }

void uiProperty::setZMsg( int msg )
{
    mZMsg = msg;
    validate_z_msg();
}
int uiProperty::getZMsg()
{
    return mZMsg;
}

void uiProperty::setZVal( int val )
{
    mZVal.set( val );
    validate_zval();
}
void uiProperty::setZVal( qlonglong val )
{
    mZVal.set( val );
    validate_zval();
}

void uiProperty::setZVal( dsoReal val )
{
    mZVal.set( val );
    validate_zval();
}
void uiProperty::setZVal( DsoRealType_A a, DsoE e )
{
    mZVal.set( a, e );
    validate_zval();
}

PackInt uiProperty::getZVal()
{
    return mZVal;
}

void uiProperty::setMinVal( int val )
{
    mMinVal.set( val );
    validate_min_val();
}
void uiProperty::setMinVal( qlonglong val )
{
    mMinVal.set( val );
    validate_min_val();
}
void uiProperty::setMinVal( dsoReal val )
{
    mMinVal.set( val );
    validate_min_val();
}
void uiProperty::setMinVal( DsoRealType_A a, DsoE e )
{
    mMinVal.set( a, e );
    validate_min_val();
}
PackInt uiProperty::getMinVal()
{
    return mMinVal;
}

void uiProperty::setMaxVal( int val )
{
    mMaxVal.set( val );
    validate_max_val();
}
void uiProperty::setMaxVal( qlonglong val )
{
    mMaxVal.set( val );
    validate_max_val();
}
void uiProperty::setMaxVal( dsoReal val )
{
    mMaxVal.set( val );
    validate_max_val();
}
void uiProperty::setMaxVal( DsoRealType_A a, DsoE e )
{
    mMaxVal.set( a, e );
    validate_max_val();
}
PackInt uiProperty::getMaxVal()
{
    return mMaxVal;
}

void uiProperty::setRange( int minVal, int maxVal, int zVal )
{
    setMinVal( minVal );
    setMaxVal( maxVal );
    setZVal( zVal );
}
void uiProperty::setRange( qlonglong minVal, qlonglong maxVal, qlonglong zVal )
{
    setMinVal( minVal );
    setMaxVal( maxVal );
    setZVal( zVal );
}
void uiProperty::setRange( dsoReal &minVal, dsoReal &maxVal, dsoReal &zVal)
{
    setMinVal( minVal );
    setMaxVal( maxVal );
    setZVal( zVal );
}
void uiProperty::setRange(
               DsoRealType_A mina, DsoE mine,
               DsoRealType_A maxa, DsoE maxe,
               DsoRealType_A za, DsoE ze
               )
{
    setMinVal( mina, mine );
    setMaxVal( maxa, maxe );
    setZVal( za, ze );
}
void uiProperty::setUnit( DsoType::Unit unit )
{
    mUnit = unit;
    validate_unit();
}
DsoType::Unit uiProperty::getUnit()
{
    return mUnit;
}

void uiProperty::setViewFmt( DsoType::DsoViewFmt fmt )
{
    mViewFmt = fmt;
    validate_fmt();
}
DsoType::DsoViewFmt uiProperty::getViewFmt()
{
    return mViewFmt;
}

void uiProperty::setAcc( key_acc::KeyAcc acc )
{
    mAcc = acc;
    validate_acc();
}
key_acc::KeyAcc uiProperty::getAcc()
{
    return mAcc;
}

void uiProperty::setBase( double base )
{
    mBase.setVal( base );

    validate_base();
}
double uiProperty::getBase()
{
    double dVal;

    if ( ERR_NONE != mBase.toReal( dVal ) )
    {
        return 0.0f;
    }

    return dVal;
}

void uiProperty::setRealBase( DsoReal realBase )
{
    mBase = realBase;
    validate_base();
}
void uiProperty::setRealBase( DsoRealType_A intPart, DsoE ePart )
{
    mBase.setVal( intPart, ePart );
    validate_base();
}
DsoReal uiProperty::getRealBase()
{
    return mBase;
}

void uiProperty::setStick( int val )
{
    mStick.clear();

    PackInt pack;
    pack.set( val );

    mStick.append( pack );

    validate_stick();
}
void uiProperty::setStick( qlonglong val )
{
    mStick.clear();

    PackInt pack;
    pack.set( val );

    mStick.append( pack );

    validate_stick();
}

void uiProperty::setStick( QList<int> &listStick )
{
    mStick.clear();

    PackInt pack;
    int val;
    foreach( val, listStick )
    {
        pack.set( val );
        mStick.append( pack );
    }

    qSort( mStick );

    validate_stick();
}
void uiProperty::setStick( QList<qlonglong> &listStick )
{
    mStick.clear();

    PackInt pack;
    qlonglong val;
    foreach( val, listStick )
    {
        pack.set( val );
        mStick.append( pack );
    }

    qSort( mStick );

    validate_stick();
}

void uiProperty::setPreStr( const QString &str )
{
    mPreStr = str;

    validate_prestr();
}
QString &uiProperty::getPreStr()
{
    return mPreStr;
}

void uiProperty::setPostStr( const QString &str )
{
    mPostStr = str;

    validate_poststr();
}
QString &uiProperty::getPostStr()
{
    return mPostStr;
}

void uiProperty::setOutStr( const QString &str )
{
    mStrOut = str;

    validate_strout();
}
QString &uiProperty::getOutStr()
{
    return mStrOut;
}

void uiProperty::setFgColor( const QColor &color )
{
    mFgColor = color;
    validate_fg_color();
}
QColor &uiProperty::getFgColor()
{ return mFgColor; }
void uiProperty::setBgColor( const QColor &color )
{
    mBgColor = color;
    validate_bg_color();
}
QColor &uiProperty::getBgColor()
{
    return mBgColor;
}

/*!
 * \brief uiProperty::setNodeCompress
 * \param b
 * - do not expand the combox child menu
 * - expand may be to other service
 * - edge trigger to pulse trigger
 */
void uiProperty::setNodeCompress( bool b )
{
    if ( !is_node_compress_valid() )
    {
        mModifiedMask |= e_node_compress;
    }
    else if ( b != mVisible )
    {
        mModifiedMask |= e_node_compress;
    }

    mNodeCompress = b;
    validate_node_compress();
}
bool uiProperty::getNodeCompress()
{
    return mNodeCompress;
}

void uiProperty::setMaxLength( int len )
{
    mMaxLength = len;
    validate_max_text_length();
}
int uiProperty::getMaxLength()
{ return mMaxLength; }
void uiProperty::setNoCache( bool b )
{
    if ( b )
    { validate_no_cache_msg(); }
    else
    { mValidMask &= ~e_no_cache; }
}
bool uiProperty::getNoCache()
{
    return is_attr( mValidMask, e_no_cache );
}
void uiProperty::setAttr( quint32 attr )
{
    mAttrs = attr;
    validate_attr();
}
quint32 uiProperty::getAttr()
{ return mAttrs; }

bool uiProperty::isXControl( eMenuControl cont )
{
    quint32 control;

    control = (mAttrs & property_control_mask);

    return control == cont;
}

void uiProperty::setKeyRequest( CKeyRequest &keyRequest )
{
    //! new key request
    if ( NULL == m_pKeyRequest )
    {
        m_pKeyRequest = R_NEW( CKeyRequest() );
        Q_ASSERT( NULL != m_pKeyRequest );
    }

    *m_pKeyRequest = keyRequest;
    validate_key_request();
}

CKeyRequest *uiProperty::getKeyRequest()
{
    return m_pKeyRequest;
}

bool uiProperty::isOption()
{
    return mComponent == e_option;
}
bool uiProperty::isItem()
{
    return mComponent == e_item;
}

bool uiProperty::isEnableModified()
{
    return (mModifiedMask & e_enable_mask) == e_enable_mask;
}
bool uiProperty::isVisibleModified()
{
    return (mModifiedMask & e_visible_mask) == e_visible_mask;
}

qulonglong uiProperty::getModified()
{
    return mModifiedMask;
}
void uiProperty::rstModified()
{
    mModifiedMask = 0;
}

bool uiProperty::isXxxValid( UiPropertyModifyMask mask )
{
    return (mValidMask & mask) == mask;
}

bool uiProperty::filter( )
{
    if ( !mEnable ) return true;
    if ( !mVisible ) return true;

    return false;
}

}
