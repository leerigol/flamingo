#ifndef UIPROPERTY_H
#define UIPROPERTY_H

#include <QtCore>
#include "../keyacc/ckeyacc.h"
#include "../../include/dsotype.h"
#include "../../baseclass/dsoreal.h"
#include "../../baseclass/ckeyrequest.h"
#include "uicontrol.h"
namespace ui_attr
{

enum UiComponent
{
    e_item,     /*!< 菜单控件 */
    e_option,   /*!< 复合选择控件，选择项 */
};

enum UiPropertyModifyMask
{
    e_enable_mask = 0x01,
    e_visible_mask = 0x02,

    e_step_mask = 0x04,
    e_i_step_mask = 0x04,       //! increase step
    e_z_mask = 0x08,
    e_min_mask = 0x10,
    e_max_mask = 0x20,

    e_acc_mask = 0x40,
    e_base_mask = 0x80,         /*!< 1 = 0.001 eg. 1000 will show as 1.00 */
    e_prestr_mask = 0x100,      /*!< string */
    e_poststr_mask = 0x200,

    e_format_mask  = 0x400,     /*!< format */
    e_stick_mask = 0x800,       /*!< stick mask*/

    e_str_out_mask = 0x1000,    /*!< string out */

    e_node_compress = 0x2000,

    e_unit_mask = 0x4000,       /*!< 单位*/

    e_max_text_length = 0x8000,
    e_key_request = 0x10000,
    e_z_msg = 0x20000,

    e_fg_color = 0x40000,
    e_bg_color = 0x80000,

    e_attrs = 0x100000,
    e_d_step_mask = 0x200000,
    e_no_cache  = 0x400000,     //! do not cache value
                                //! read before each op
};

enum PropertyAttr
{
    property_none = 0,

    property_control_mask = 0xff,
};

#define validate_enable()   {mValidMask |= e_enable_mask;}
#define validate_visible()  {mValidMask |= e_visible_mask;}
#define validate_step()     {mValidMask |= e_step_mask;}
#define validate_i_step()     {mValidMask |= e_i_step_mask;}

#define validate_zval()     {mValidMask |= e_z_mask;}
#define validate_min_val()     {mValidMask |= e_min_mask;}
#define validate_max_val()     {mValidMask |= e_max_mask;}

#define validate_acc()      {mValidMask |= e_acc_mask;}
#define validate_base()     {mValidMask |= e_base_mask;}
#define validate_prestr()     {mValidMask |= e_prestr_mask;}
#define validate_poststr()  {mValidMask |= e_poststr_mask;}

#define validate_fmt()      {mValidMask |= e_format_mask;}
#define validate_stick() {mValidMask |= e_stick_mask;}

#define validate_strout()     {mValidMask |= e_str_out_mask;}
#define validate_fg_color()     {mValidMask |= e_fg_color;}
#define validate_bg_color()     {mValidMask |= e_bg_color;}
#define validate_attr()     {mValidMask |= e_attrs;}
#define validate_d_step()     {mValidMask |= e_d_step_mask;}

#define validate_node_compress() {mValidMask |= e_node_compress;}
#define validate_unit() { mValidMask |= e_unit_mask; }
#define validate_max_text_length() { mValidMask |= e_max_text_length; }

#define validate_key_request() { mValidMask |= e_key_request; }
#define validate_z_msg()        { mValidMask |= e_z_msg; }
#define validate_no_cache_msg()  { mValidMask |= e_no_cache; }

#define is_enable_valid()   ( ( mValidMask & e_enable_mask ) == e_enable_mask )
#define is_visible_valid()  ( ( mValidMask & e_visible_mask ) == e_visible_mask )
#define is_node_compress_valid()  ( ( mValidMask & e_node_compress ) == e_node_compress )

struct PackInt
{
    int width;
    union
    {
        int val32;
        qlonglong val64;
        dsoReal val128;
    };

    PackInt();
    bool isUnk();

    PackInt operator+(int val );
    PackInt operator-(int val );

    PackInt operator+(qlonglong val );
    PackInt operator-(qlonglong val );

    bool operator>( int val );
    bool operator<( int val );

    bool operator>=( int val );
    bool operator<=( int val );

    bool operator>( qlonglong val );
    bool operator<( qlonglong val );

    bool operator>=( qlonglong val );
    bool operator<=( qlonglong val );


    PackInt operator=(int val);
    PackInt operator=(qlonglong val);

    void set( int val );
    void set( qlonglong val );
    void set( dsoReal real );
    void set( DsoRealType_A, DsoE e );

    //! do not change type
    void assignPower2( int scale );

    float realf( float fBase );
    double reald( double dBase );
};

bool operator<( const PackInt &a, const PackInt &b );
bool operator>( const PackInt &a, const PackInt &b );

/*!
 * \brief The uiProperty class
 * UI 元素的属性
 */
class uiProperty
{
public:
    uiProperty( int msg );
    uiProperty( int msg, int opt );
    ~uiProperty();

public:
    void setEnable( bool b);
    bool getEnable();

    void setVisible( bool b );
    bool getVisible();

    void setStep( qlonglong step );
    qlonglong getStep();

    void setIStep( qlonglong step );
    qlonglong getIStep();

    void setDStep( qlonglong step );
    qlonglong getDStep();

    void setZMsg( int msg );
    int getZMsg();

    void setZVal( int val );
    void setZVal( qlonglong val );
    void setZVal( dsoReal real );
    void setZVal( DsoRealType_A a, DsoE e );
    PackInt getZVal();

    void setMinVal( int val );
    void setMinVal( qlonglong val );
    void setMinVal( dsoReal real );
    void setMinVal( DsoRealType_A a, DsoE e );
    PackInt getMinVal();

    void setMaxVal( int val );
    void setMaxVal( qlonglong val );
    void setMaxVal( dsoReal real );
    void setMaxVal( DsoRealType_A a, DsoE e );
    PackInt getMaxVal();

    void setRange( int minVal, int maxVal, int zVal);
    void setRange( qlonglong minVal, qlonglong maxVal, qlonglong zVal );
    void setRange( dsoReal &minVal, dsoReal &maxVal, dsoReal &zVal);
    void setRange(
                   DsoRealType_A mina, DsoE mine,
                   DsoRealType_A maxa, DsoE maxe,
                   DsoRealType_A za, DsoE ze
                   );

    void setUnit( DsoType::Unit unit );
    DsoType::Unit getUnit();

    void setViewFmt( DsoType::DsoViewFmt fmt );
    DsoType::DsoViewFmt getViewFmt();

    void setAcc( key_acc::KeyAcc acc );
    key_acc::KeyAcc getAcc();

    void setBase( double base );
    double getBase();

    void setRealBase( DsoReal realBase );
    void setRealBase( DsoRealType_A intPart, DsoE ePart );
    DsoReal getRealBase();

    void setStick( int val );
    void setStick( qlonglong val );

    void setStick( QList<int> &listStick );
    void setStick( QList<qlonglong> &listStick );

    void setPreStr( const QString &str );
    QString &getPreStr();

    void setPostStr( const QString &str );
    QString &getPostStr();

    void setOutStr( const QString &str );
    QString &getOutStr();

    void setFgColor( const QColor &color );
    QColor &getFgColor();

    void setBgColor( const QColor &color );
    QColor &getBgColor();

    void setNodeCompress( bool b );
    bool getNodeCompress();

    void setMaxLength( int len );
    int getMaxLength();

    void setNoCache( bool b=true );
    bool getNoCache();

    void setAttr( quint32 attr );
    quint32 getAttr();

    bool isXControl( eMenuControl cont );

    void setKeyRequest( CKeyRequest &pRequest );
    CKeyRequest *getKeyRequest();

    bool isOption();
    bool isItem();

    bool isEnableModified();
    bool isVisibleModified();

    qulonglong getModified();
    void rstModified();

    bool isXxxValid( UiPropertyModifyMask mask );

    bool filter();

public:
    bool mEnable;
    bool mVisible;

    qlonglong  mStep, mDStep;       /*!< step */

    int  mZMsg;
    PackInt mZVal;          //! z = default val
    PackInt mMinVal, mMaxVal;

                            //! unit and format
    DsoType::Unit mUnit;
    DsoType::DsoViewFmt mViewFmt;

    key_acc::KeyAcc mAcc;

    DsoReal mBase;
                            //! stick values
    QList< PackInt > mStick;

    QString mPreStr;
    QString mPostStr;

    QString mStrFmt;

    QString mStrOut;        /*!< out string */

    QColor mFgColor, mBgColor;

    bool mNodeCompress;
    quint32 mAttrs;

    int mMsg;
    UiComponent mComponent;
    int mValue;             /*!< for e_option only */

    int mMaxLength;

    CKeyRequest *m_pKeyRequest;

    qulonglong mModifiedMask;
    qulonglong mValidMask;
};


}

#endif // UIPROPERTY_H
