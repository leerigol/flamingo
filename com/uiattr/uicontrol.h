#ifndef UICONTROL
#define UICONTROL

/*!
 * \brief The eMenuControl enum
 * - Menu control type
 * - The control string is listed in .xls
 */
enum eMenuControl
{
    MC_Invalid = 0,

    Menu_Title = 1,
    Combox_Func,
    Button_Two,
    Button_Sub,
    Label_Float,    /*< int int float */
    Sub_Menu_Title,
    Combox,

    Label_Icon,
    Button_One,
    Check_Box,

    Label_int_float_float_knob,

    Label_int,
    Label_str,
    Button_sub_no_ret,

    Label_ime,

    TimeEdit,
    DateEdit,
    IpEdit,
    SeekBar,

    ComboxKnob,
    Space,          /*!< space */

    Button_Parent,  /*!< gened by group */
    LiteCombox,
};


#endif // UICONTROL

