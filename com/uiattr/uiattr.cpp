#include "uiattr.h"
#include "../../include/dsodbg.h"
namespace ui_attr {

uiAttr::uiAttr()
{

}

uiAttr::~uiAttr()
{
    uiProperty* pProperty;

    foreach( pProperty, mListProperty )
    {
        Q_ASSERT( pProperty != NULL );
        delete pProperty;
    }
}

void uiAttr::setEnable( int msg, bool b )
{
    getItem(msg)->setEnable(b);
}
void uiAttr::setVisible( int msg, bool b )
{
    getItem(msg)->setVisible( b );
}
void uiAttr::setEnableVisible( int msg, bool bE, bool bV )
{
    uiProperty *pProp;
    pProp = getItem(msg);
    if( NULL == pProp )
    { return; }
    pProp->setEnable( bE );
    pProp->setVisible( bV );
}
void uiAttr::setFgColor( int msg, const QColor &color )
{
    getItem(msg)->setFgColor( color );
}
void uiAttr::setBgColor( int msg, const QColor &color )
{
    getItem(msg)->setBgColor( color );
}

void uiAttr::setEnable( int msg, int opt, bool b )
{
    getOption(msg,opt)->setEnable( b );
}
void uiAttr::setVisible( int msg, int opt, bool b )
{
    getOption(msg,opt)->setVisible( b );
}
void uiAttr::setEnableVisible( int msg, int opt, bool bE, bool bV )
{
    uiProperty *pProp;
    pProp = getOption(msg, opt );
    if( NULL == pProp )
    { return; }

    pProp->setEnable( bE );
    pProp->setVisible( bV );
}

void uiAttr::setFgColor( int msg, int opt, const QColor &color )
{
    getOption(msg, opt )->setFgColor( color );
}
void uiAttr::setBgColor( int msg, int opt, const QColor &color )
{
    getOption(msg, opt )->setBgColor( color );
}

void uiAttr::setStep( int msg, qlonglong step )
{
    getItem(msg)->setStep(step);
}
void uiAttr::setIStep( int msg, qlonglong step )
{
    getItem( msg )->setIStep( step );
}
void uiAttr::setDStep( int msg, qlonglong step )
{
    getItem( msg )->setDStep( step );
}
void uiAttr::setZMsg( int msg, int zMsg )
{ getItem(msg)->setZMsg( zMsg ); }
void uiAttr::setZVal( int msg, int zval )
{
    getItem(msg)->setZVal(zval);
}
void uiAttr::setZVal( int msg, qlonglong z )
{
    getItem(msg)->setZVal(z);
}
void uiAttr::setZVal( int msg, dsoReal &z )
{ getItem(msg)->setZVal(z); }
void uiAttr::setZVal( int msg, DsoRealType_A a, DsoE e )
{ getItem(msg)->setZVal(a, e);}

#define bk_val  1296
#ifdef _DEBUG
#define check_msg() if ( msg == bk_val ){ Q_ASSERT(false); }
#else
#define check_msg()
#endif
void uiAttr::setMinVal( int msg, int v )
{
    check_msg();
    getItem(msg)->setMinVal( v );
}
void uiAttr::setMinVal( int msg, qlonglong v )
{
    check_msg();
    getItem(msg)->setMinVal( v );
}
void uiAttr::setMinVal( int msg, dsoReal &z )
{
    check_msg();
    getItem(msg)->setMinVal( z );
}
void uiAttr::setMinVal( int msg, DsoRealType_A a, DsoE e )
{
    check_msg();
    getItem(msg)->setMinVal( a, e );
}

void uiAttr::setMaxVal( int msg, int v )
{
    check_msg();
    getItem(msg)->setMaxVal( v );
}
void uiAttr::setMaxVal( int msg, qlonglong v )
{
    check_msg();
    getItem(msg)->setMaxVal( v );
}
void uiAttr::setMaxVal( int msg, dsoReal &z )
{
    check_msg();
    getItem(msg)->setMaxVal( z );
}
void uiAttr::setMaxVal( int msg, DsoRealType_A a, DsoE e )
{
    check_msg();
    getItem(msg)->setMaxVal( a, e );
}

void uiAttr::setRange( int msg, int minVal, int maxVal, int zVal )
{
    getItem(msg)->setRange( minVal, maxVal, zVal );
}
void uiAttr::setRange( int msg, qlonglong minVal, qlonglong maxVal, qlonglong zVal )
{
    getItem(msg)->setRange( minVal, maxVal, zVal );
}
void uiAttr::setRange( int msg,
                 dsoReal &minVal, dsoReal &maxVal, dsoReal &zVal )
{ getItem(msg)->setRange( minVal, maxVal, zVal ); }
void uiAttr::setRange( int msg,
                 DsoRealType_A mina, DsoE mine,
                 DsoRealType_A maxa, DsoE maxe,
                 DsoRealType_A za, DsoE ze
                 )
{
    getItem(msg)->setRange(
                          mina, mine,
                          maxa, maxe,
                          za, ze);
}

void uiAttr::setUnit( int msg, DsoType::Unit unit )
{
    getItem(msg)->setUnit( unit );
}
void uiAttr::setFormat( int msg, DsoType::DsoViewFmt fmt )
{
    getItem(msg)->setViewFmt( fmt );
}
void uiAttr::setAcc( int msg, key_acc::KeyAcc acc )
{
    getItem(msg)->setAcc(acc);
}

/*!
 * \brief uiAttr::setStick
 * \param msg
 * \param val
 * 设置单个的粘滞值
 * -先清除粘滞列表
 * -然后添加粘滞值
 */
void uiAttr::setStick( int msg, int val )
{
    getItem(msg)->setStick( val );
}
void uiAttr::setStick( int msg, qlonglong val )
{
    getItem(msg)->setStick( val );
}
/*!
 * \brief uiAttr::setStick
 * \param msg
 * \param listStick
 * 添加一系列粘滞值
 * -之前的粘滞值会被清除
 */
void uiAttr::setStick( int msg, QList<int> &listStick )
{
    getItem(msg)->setStick( listStick );
}
void uiAttr::setStick( int msg, QList<qlonglong> &listStick )
{
    getItem(msg)->setStick( listStick );
}

void uiAttr::setBase( int msg, float base )
{
    getItem(msg)->setBase(base);
}

void uiAttr::setBase( int msg, DsoReal real )
{
    getItem(msg)->setRealBase( real );
}
void uiAttr::setBase( int msg, DsoRealType_A a, DsoE e )
{
    getItem(msg)->setRealBase( a, e );
}

void uiAttr::setPreStr( int msg, const QString &str )
{
    getItem(msg)->setPreStr(str);
}
void uiAttr::setPostStr( int msg, const QString &str )
{
    getItem(msg)->setPostStr(str);
}

void uiAttr::setOutStr( int msg, const QString &str )
{
    getItem(msg)->setOutStr(str);
}

void uiAttr::setNodeCompress( int msg, bool b )
{
    getItem(msg)->setNodeCompress( b );
}

void uiAttr::setAttr( int msg, quint32 attr )
{
    getItem(msg)->setAttr( attr );
}

void uiAttr::setMaxLength( int msg, int len )
{
    getItem(msg)->setMaxLength( len );
}
void uiAttr::setNoCache( int msg, bool b )
{
    getItem(msg)->setNoCache( b );
}
void uiAttr::setKeyRequest( int msg,
                            CKeyRequest &keyReq )
{
    getItem(msg)->setKeyRequest( keyReq );
}

ListProperty & uiAttr::getListProperty()
{
    return mListProperty;
}

/*!
 * \brief uiAttr::findProperty
 * \param msg
 * \param comp
 * \param opt only valid when comp == e_option
 * \return
 */
uiProperty *uiAttr::findProperty( int msg, UiComponent comp, int opt )
{
    uiProperty *pProperty;

    //! find property
    foreach( pProperty, mListProperty )
    {
        Q_ASSERT( pProperty != NULL );

        if ( pProperty->mMsg == msg &&
             pProperty->mComponent == comp )
        {
            if ( comp == e_option )
            {
                //! option match
                if ( pProperty->mValue == opt )
                {
                    return pProperty;
                }
                //! continue serarch
                else
                {
                }
            }
            //! item match
            else
            {
                return pProperty;
            }
        }
    }

    return NULL;
}

uiProperty *uiAttr::getItem( int msg )
{
    uiProperty* pProperty;

    pProperty = findProperty( msg, e_item );
    if ( NULL != pProperty )
    {}
    else
    {
        pProperty = R_NEW( uiProperty( msg ) );
        Q_ASSERT( NULL != pProperty );

        mListProperty.append(pProperty);
    }

    return pProperty;
}

uiProperty *uiAttr::getOption( int msg, int opt )
{
    uiProperty* pProperty;

    pProperty = findProperty( msg, e_option, opt );
    if ( NULL != pProperty )
    {}
    else
    {
        pProperty = R_NEW( uiProperty( msg, opt ) );
        Q_ASSERT( NULL != pProperty );

        mListProperty.append(pProperty);
    }

    return pProperty;
}

}
