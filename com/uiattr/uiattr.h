#ifndef UIATTR_H
#define UIATTR_H

#include <QtCore>   /*!< for QList */
#include "uicontrol.h"
#include "uiproperty.h"

namespace ui_attr
{

typedef QList<uiProperty*> ListProperty;

/*!
 * \brief The uiAttr class
 * UI属性集合
 * -各个UI属性的集合
 * -利用消息值进行索引
 */
class uiAttr
{
public:
    uiAttr();
    ~uiAttr();
public:
    void setEnable( int msg, bool b );
    void setVisible( int msg, bool b );
    void setEnableVisible( int msg, bool bE=true, bool bV=true );

    void setFgColor( int msg, const QColor &color );
    void setBgColor( int msg, const QColor &color );

    void setEnable( int msg, int opt, bool b );
    void setVisible( int msg, int opt, bool b );
    void setEnableVisible( int msg, int opt, bool bE=true, bool bV=true );

    void setFgColor( int msg, int opt, const QColor &color );
    void setBgColor( int msg, int opt, const QColor &color );

    void setStep( int msg, qlonglong step );
    void setIStep( int msg, qlonglong step );
    void setDStep( int msg, qlonglong step );

    void setZMsg( int msg, int zMsg );
    void setZVal( int msg, int z );
    void setZVal( int msg, qlonglong z );
    void setZVal( int msg, dsoReal &z );
    void setZVal( int msg, DsoRealType_A a, DsoE e );

    void setMinVal( int msg, int v );
    void setMinVal( int msg, qlonglong v );
    void setMinVal( int msg, dsoReal &z );
    void setMinVal( int msg, DsoRealType_A a, DsoE e );

    void setMaxVal( int msg, int v );
    void setMaxVal( int msg, qlonglong v );
    void setMaxVal( int msg, dsoReal &z );
    void setMaxVal( int msg, DsoRealType_A a, DsoE e );

    void setRange( int msg, int minVal, int maxVal, int zVal );
    void setRange( int msg, qlonglong minVal, qlonglong maxVal, qlonglong zVal );

    void setRange( int msg,
                     dsoReal &minVal, dsoReal &maxVal, dsoReal &zVal );
    void setRange( int msg,
                     DsoRealType_A mina, DsoE mine,
                     DsoRealType_A maxa, DsoE maxe,
                     DsoRealType_A za, DsoE ze
                     );


    void setUnit( int msg, DsoType::Unit unit );
    void setFormat( int msg, DsoType::DsoViewFmt fmt );

    void setAcc( int msg, key_acc::KeyAcc acc );

    void setStick( int msg, int val );
    void setStick( int msg, qlonglong val );

    void setStick( int msg, QList<int> &listStick );
    void setStick( int msg, QList<qlonglong> &listStick );

    void setBase( int msg, float base );
    void setBase( int msg, DsoReal real );
    void setBase( int msg, DsoRealType_A a, DsoE e );

    void setPreStr( int msg, const QString &str );
    void setPostStr( int msg, const QString &str );

    void setOutStr( int msg, const QString &str );

    void setNodeCompress( int msg, bool b );

    void setAttr( int msg, quint32 attr );

    void setMaxLength( int msg, int len );

    void setNoCache( int msg, bool b=true );

    void setKeyRequest( int msg, CKeyRequest &keyReq );

    ListProperty & getListProperty();

public:
    uiProperty *findProperty( int msg, UiComponent comp, int opt=-1 );

public:
    uiProperty *getItem( int msg );
    uiProperty *getOption( int msg, int opt );

protected:
    ListProperty mListProperty;

private:

};

}

#endif // UIATTR_H
