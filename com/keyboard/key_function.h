#ifndef KEY_FUNCTION__H
#define KEY_FUNCTION__H

#ifdef __cplusplus
extern "C"{
#endif

/*
 *调用此函数是使用默认值对键盘进行初始化，默认值如下：
 *串口节点路径:		/dev/ttyPS1
 *波特率:		1000000
 *数据位:		8
 *奇偶校验位：‘N’	//---None
 *停止位：		1
 */
int keyboardInit(void);

/*
 *参数:
 *	char* path  串口设备节点路径	 ---flamingo串口设备节点路径：/dev/ttyPS1
 *	int speed 波特率						  ---flamingo键盘串口波特率支持：2400、4800、9600、115200、1000000
 *	int bit 数据位								---flamingo键盘串口数据位可设置为：8位、7位
 *	char event积偶校验位         ---flamingo键盘串口奇偶校验位：(N)--None、(O)--Odd、(E)--Even
 *	int stop停止位								---flamingo键盘串口停止位：1位、2位
 *返回值:  == 0 ok
 *	
 */
int keyboardOpen(char* path,int speed,int bit,char event,int stop);

void keyboardClose();

void keyboardScan();

/*
 *此函数需要在一个单独线程循环调用，用以接收按键键值
 *参数:无
 *	
 *返回值 类型：unsigned short  其中返回的0xff为无效键值，需要进行过滤
 */
unsigned short keyboardGetKey(void);

#ifdef __cplusplus
}
#endif

#endif	



