#include <unistd.h>


#include "keyboard.h"
#include "keyboard_p.h"

//! configs
#define KEYBOARD_LED_GROUP_NUM      4
#define KEYBOARD_LED_NUM            15
#define KEYBOARD_LED_GROUP_LEDS     4

static unsigned int n_u32KeyLedStat = 0x0;//default is off
static unsigned char n_au8KeyLedGroupCmd[KEYBOARD_LED_GROUP_NUM] = {
    0x10,                  /**< 第一组  LED4~LED1 */
    0x20,                  /**< 第二组  LED8~LED5 */
    0x30,                  /**< 第三组  LED12~LED9 */
    0x40                   /**< 第三组  LED16~LED13 */
};
static unsigned char n_au8KeyLedKey2LedMap[KEYBOARD_LED_NUM] = {
    0x01,                     /**< LED1  <-> KEYBOARD_LED_CH4 */
    0x02,                     /**< LED2  <-> KEYBOARD_LED_MATH */
    0x04,                     /**< LED5  <-> KEYBOARD_LED_SOURCE2 */
    0x08,                     /**< LED3  <-> KEYBOARD_LED_CH2 */

    0x01,                     /**< LED4  <-> KEYBOARD_LED_SOURCE1 */
    0x02,                     /**< LED6  <-> KEYBOARD_LED_CH1 */
    0x04,                     /**< LED7  <-> KEYBOARD_LED_DG */
    0x08,                     /**< LED13  <-> KEYBOARD_LED_RUN */

    0x01,                     /**< LED14  <-> KEYBOARD_LED_STOP */
    0x02,                     /**< LED8 <-> KEYBOARD_LED_SINGLE */
    0x04,                     /**< LED9 <-> KEYBOARD_LED_DECODE */
    0x08,                     /**< LED10 <-> KEYBOARD_LED_REF */

    0x01,                     /**< LED11 <-> KEYBOARD_LED_CH3 */
    0x02,                     /**< LED12 <-> KEYBOARD_LED_FUNC */
    0x04,                     /**< LED15 <-> KEYBOARD_LED_TOUCH */
};

void keyboardLedOn(unsigned int u32Leds)
{
    unsigned char u8Cmd;
    unsigned char u8GroupStat;
    unsigned char u8Led;

    unsigned char i,j;

    for (i = 0; i < KEYBOARD_LED_NUM; i++)
	{
        if (((u32Leds>>i) & 0x01))
		{
			j = i / KEYBOARD_LED_GROUP_LEDS;
			u8Cmd = n_au8KeyLedGroupCmd[j];                                     /**< 获取组别 */
			u8GroupStat = n_u32KeyLedStat>>(j * KEYBOARD_LED_GROUP_LEDS);       /**< 得到当前该组键盘灯状态 */
			u8GroupStat &= 0x0F;
			
			u8Led = u8GroupStat | n_au8KeyLedKey2LedMap[i];                     /**< 将对应位设置为 1 即点亮LED */

			u8Cmd |= u8Led;
            write(n_pstKeyboard->fHandle,&u8Cmd,1);

            n_u32KeyLedStat &= ~((unsigned int)0x0F<<(j * KEYBOARD_LED_GROUP_LEDS));
            n_u32KeyLedStat |= (unsigned int)u8Led<<(j * KEYBOARD_LED_GROUP_LEDS);
		}
	}
}

void keyboardLedOff(unsigned int u32Leds)
{
    unsigned int u8Cmd;
    unsigned int u8GroupStat;
    unsigned int u8Led;

    unsigned char i, j;

    for (i = 0; i < KEYBOARD_LED_NUM; i++)
	{
        if (((u32Leds>>i) & 0x01))
		{
			j = i / KEYBOARD_LED_GROUP_LEDS;
			u8Cmd = n_au8KeyLedGroupCmd[j];                                     /**< 获取组别 */
			u8GroupStat = n_u32KeyLedStat>>(j * KEYBOARD_LED_GROUP_LEDS);       /**< 得到当前该组键盘灯状态 */
			u8GroupStat &= 0x0F;

			u8Led = u8GroupStat & (~n_au8KeyLedKey2LedMap[i]);                  /**< 将对应位清空为 0 即熄灭LED */
			u8Cmd |= u8Led;
            write(n_pstKeyboard->fHandle,&u8Cmd,1);

            n_u32KeyLedStat &= ~((unsigned int)0x0F<<(j * KEYBOARD_LED_GROUP_LEDS));
            n_u32KeyLedStat |= (unsigned int)u8Led<<(j * KEYBOARD_LED_GROUP_LEDS);
		}
	}
}


