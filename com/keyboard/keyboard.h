#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#ifdef __cplusplus
extern "C"{
#endif

/***************************************************************************
 * 键盘键值定义
 * 1.一个完整的键值是一个unsigned short（16位）整形数
 * 2.16位中[7...0]表示按键键值，[15...8]表示按键状态或旋钮方向
 * 3.按键键值宏定义
 ***************************************************************************/

/*******************************************************************************
 * 垂直通道相关
 ******************************************************************************/
#define KEY_CH1_MENU	                0x1a
#define KEY_CH2_MENU	                0x6a
#define KEY_CH3_MENU	                0x17
#define KEY_CH4_MENU	                0x77

#define KEY_CH1_VOLT                    0x43
#define KEY_CH1_VOLT_Z                  0x41
#define KEY_CH1_POS                     0x13
#define KEY_CH1_POS_Z                   0x11

#define KEY_CH2_VOLT                    0x83
#define KEY_CH2_VOLT_Z                  0x81
#define KEY_CH2_POS                     0x73
#define KEY_CH2_POS_Z                   0x71

#define KEY_CH3_VOLT                    0x66
#define KEY_CH3_VOLT_Z                  0x64
#define KEY_CH3_POS                     0x56
#define KEY_CH3_POS_Z                   0x54

#define KEY_CH4_VOLT                    0x16
#define KEY_CH4_VOLT_Z                  0x14
#define KEY_CH4_POS                     0x76
#define KEY_CH4_POS_Z                   0x74

/*******************************************************************************
 * 触发相关
 ******************************************************************************/
#define KEY_TRIG_MENU   	            0x78
#define KEY_TRIG_MODE	                0x57
#define KEY_TRIG_FORCE	                0x27

#define KEY_TRIG_LEVEL                  0x36
#define KEY_TRIG_LEVEL_Z                0x34

/*******************************************************************************
 * 水平相关
 ******************************************************************************/
#define KEY_TIME_NAVIGATE  	            0x8a
#define KEY_HORI_ZOOM                   0x5a

#define KEY_TIME_SCLAE                  0x33
#define KEY_TIME_SCLAE_Z                0x31
#define KEY_TIME_POS                    0x63
#define KEY_TIME_POS_Z                  0x61
/*******************************************************************************
 * 其他菜单键
 ******************************************************************************/
#define KEY_LA_MENU	                    0x28
#define KEY_DECODE_MENU                 0x37

#define KEY_SOURCE1_MENU	            0x4a
#define KEY_SOURCE2_MENU                0x67
#define KEY_MATH_MENU	                0x48
#define KEY_REF_MENU	                0x88

#define KEY_MEASURE   	                0x39
#define KEY_ACQUIRE   	                0x29
#define KEY_STORAGE	                    0x89

#define KEY_CURSOR   	                0x19
#define KEY_DISPLAY   	                0x79
#define KEY_UTILITY  	                0x59

#define KEY_FUNC                        0x53
#define KEY_FUNC_Z                      0x51

/*******************************************************************************
 * 快捷键
 ******************************************************************************/

#define KEY_DISP_CLEAR                  0x58
#define KEY_AUTO   	                    0x38
#define KEY_RUN_STOP                    0x18
#define KEY_SINGLE	                    0x68

#define KEY_QUICK                       0x69
#define KEY_DEFAULT                     0x49
#define KEY_TOUCH                       0x47

#define KEY_PLAY_PRE                    0x3A
#define KEY_PLAY_NEXT                   0x7A
#define KEY_PLAY_STOP                   0x2A

#define KEY_WAVE_VOLT                   0x26
#define KEY_WAVE_VOLT_Z                 0x24
#define KEY_WAVE_POS                    0x46
#define KEY_WAVE_POS_Z                  0x44

/*******************************************************************************
 * menu
 ******************************************************************************/
#define KEY_MENU_F1                     0x4B
#define KEY_MENU_F2                     0x3B
#define KEY_MENU_F3                     0x2B
#define KEY_MENU_F4                     0x1B
#define KEY_MENU_F5                     0x6B
#define KEY_MENU_F6                     0x7B
#define KEY_MENU_F7                     0x87

#define KEY_MENU_BACK                   0x8B
#define KEY_MENU_OFF                    0x5B

/******************************************************************************
 * 按键状态或旋钮旋转方向宏定义
 ******************************************************************************/
#define     KEY_UP		0                   /**按键弹起*/
#define     KEY_DOWN	        (0x2<<8)    /**按键普通按下*/
#define     KNOB_CLOCKWISE      (0x8<<8)   /**旋钮顺时针旋转*/
#define     KNOB_CONUT_CLOCK    (0x10<<8)    /**旋钮逆时针旋转*/

/******************************************************************************
 * 按键无效键值
 ******************************************************************************/
#define KEYBOARD_INVALID_KEYVAL		0xff

//! led
#define LED_CH1         (1<<5)
#define LED_CH2         (1<<3)
#define LED_CH3         (1<<12)
#define LED_CH4         (1<<0)

#define LED_SOURCE1     (1<<4)
#define LED_SOURCE2     (1<<2)

#define LED_DIGITAL     (1<<1)
#define LED_MATH        (1<<6)
#define LED_REF         (1<<11)
#define LED_DECODE      (1<<10)

#define LED_FUNC        (1<<13)
#define LED_RUN         (1<<7)
#define LED_STOP        (1<<8)
#define LED_SINGLE      (1<<9)
#define LED_TOUCH       (1<<14)

#define LED_ALL         (0xffffffff)

/*
 *调用此函数是使用默认值对键盘进行初始化，默认值如下：
 *串口节点路径:		/dev/ttyPS1
 *波特率:		1000000
 *数据位:		8
 *奇偶校验位：‘N’	//---None
 *停止位：		1
 */
int keyboardInit(void);

/*
 *参数:
 *	char* path  串口设备节点路径	 ---flamingo串口设备节点路径：/dev/ttyPS1
 *	int speed 波特率						  ---flamingo键盘串口波特率支持：2400、4800、9600、115200、1000000
 *	int bit 数据位								---flamingo键盘串口数据位可设置为：8位、7位
 *	char event积偶校验位         ---flamingo键盘串口奇偶校验位：(N)--None、(O)--Odd、(E)--Even
 *	int stop停止位								---flamingo键盘串口停止位：1位、2位
 *返回值:  == 0 ok
 *	
 */
int keyboardOpen(char* path,int speed,int bit,char event,int stop);

void keyboardClose();

void keyboardScan();

/*
 *此函数需要在一个单独线程循环调用，用以接收按键键值
 *参数:无
 *	
 *返回值 类型：unsigned short  其中返回的0xff为无效键值，需要进行过滤
 */
unsigned short keyboardGetKey(void);

void keyboardLedOn( unsigned int u32Leds);
void keyboardLedOff( unsigned int u32Leds);

#ifdef __cplusplus
}
#endif

#endif	



