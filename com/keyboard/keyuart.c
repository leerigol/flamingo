
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include "keyuart.h"

#include "keyboard_p.h"
#include "keyqueue.h"


/*!
 * \brief keyuartSetup
 * \param nSpeed
 * \param nBit
 * \param nEvent
 * \param nStop
 * \return error < 0
 */
int keyuartSetup(int nSpeed,int nBit,char nEvent,int nStop)
{
    int status;
    struct termios Opt;

    /*配置串口*/
    tcgetattr(n_pstKeyboard->fHandle ,&Opt);
    /*设置波特率*/
    switch(nSpeed)
    {
        case 2400:
            cfsetspeed(&Opt,2400);
            break;
        case 4800:
            cfsetspeed(&Opt,4800);
            break;
        case 9600:
            cfsetspeed(&Opt,9600);
            break;
        case 115200:
            cfsetspeed(&Opt,115200);
            break;
        case 1000000:
            cfsetspeed(&Opt,1000000);
            break;
    }
    status = tcsetattr(n_pstKeyboard->fHandle,TCSANOW,&Opt);
    if(status != 0)
    {
        return -1;
    }

    Opt.c_cflag |= (CREAD | CLOCAL);
    Opt.c_cflag &= ~CSIZE;

    /*设置数据位*/
    switch(nBit)
    {
        case 7:
            Opt.c_cflag |= CS7;
            break;
        case 8:
            Opt.c_cflag |= CS8;
            break;
    }
    /*设置校验位*/
    switch(nEvent)
    {
        case 'O':
            Opt.c_cflag |= PARENB;
            Opt.c_cflag |= PARODD;
            Opt.c_iflag |= (INPCK | ISTRIP);
            break;
        case 'E':
            Opt.c_iflag |= (INPCK | ISTRIP);
            Opt.c_cflag |= PARENB;
            Opt.c_cflag &= ~PARODD;
            break;
        case 'N':
            Opt.c_cflag &= ~PARENB;
            break;
    }

    /*设置停止位*/
    if(nStop == 1)
    {
        Opt.c_cflag &= ~CSTOPB;
    }
    else if(nStop == 2)
    {
        Opt.c_cflag |= CSTOPB;
    }
    else
    {}

    Opt.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); /* Raw mode */
    Opt.c_oflag &= ~OPOST; /* raw output */
    Opt.c_oflag &= ~(ONLCR | OCRNL);

    Opt.c_iflag &= ~(ICRNL | INLCR | IGNCR);
    Opt.c_iflag &= ~(IXON | IXOFF | IXANY);
    /*设置超时时间*/
    Opt.c_cc[VTIME] = 10;   //设置超时为15sec
    Opt.c_cc[VMIN] = 1;     //Update the Opt and do it now

    /*处理未接收字符*/
    tcflush(n_pstKeyboard->fHandle, TCIFLUSH);

    status = tcsetattr(n_pstKeyboard->fHandle, TCSANOW, &Opt);
    if(status != 0)
    {
        return -1;
    }

    return 0;
}

/*!
 * \brief open_uart
 * \param path
 * \return error < 0
 */
int keyuartOpen(char* path)
{
    /** 以非阻塞方式打开 串口 */
    n_pstKeyboard->fHandle = open(path, O_RDWR|O_NONBLOCK);

    if ( n_pstKeyboard->fHandle < 0 )
    {
        return -1;
    }

    return 0;
}

void keyuartRecv( int frameLength )
{
    unsigned char rx_buffer[ frameLength ];
    int i, len;
    do
    {
        //! try read a frame
        len = read(n_pstKeyboard->fHandle,(void*)rx_buffer, frameLength );

        for ( i = 0; i < len; i++ )
        {
            queueIn( &_scanQueue, rx_buffer[i] );
        }

    } while( len > 0 );

}

