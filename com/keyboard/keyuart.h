#ifndef KEYUART
#define KEYUART

int keyuartSetup(int nSpeed,int nBit,char nEvent,int nStop);

int keyuartOpen(char* path);

void keyuartRecv( int frameLength );

#endif // KEYUART

