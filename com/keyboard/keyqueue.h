#ifndef KEYQUEUE_H
#define KEYQUEUE_H

typedef struct _Queue{
    union
    {
        unsigned char *qBase;
        unsigned short *qBase16;
    };
    unsigned int front;
    unsigned int rear;
    int count;
}Queue;

//!vars
extern Queue _scanQueue;

//! decl
void queueCreate( Queue *qp );

int queueInit(Queue *pq, int queueCount );
int queue16Init( Queue *pq, int queueCount );
void queueDeInit( Queue *pq);

int queueIsEmpty(Queue *pq);
int queueIsFull(Queue *pq);

void queueIn(Queue *pq, unsigned char in);
void queueOut( Queue *pq, unsigned char* out );

void queue16In(Queue *pq, unsigned short in);
void queue16Out( Queue *pq, unsigned short *out );

void queueClear(Queue *pq);
int queueLength(Queue *pq);

#endif // KEYQUEUE_H

