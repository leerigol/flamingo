
#include "keyqueue.h"
#include <stdlib.h>

void queueCreate( Queue *pq )
{
    if ( NULL == pq )
    {
        return;
    }

    pq->qBase = NULL;
    pq->front = 0;
    pq->rear = 0;
    pq->count = 0;
}

int queueInit( Queue *pq, int queueCount )
{
    pq->qBase = (unsigned char *)malloc(sizeof(unsigned char)*queueCount);
    if(pq->qBase == 0)
    {
        return -1;
    }
    pq->front = pq->rear = 0;
    pq->count = queueCount;

    return 0;
}

int queue16Init( Queue *pq, int queueCount )
{
    pq->qBase16 = (unsigned short *)malloc(sizeof(unsigned short)*queueCount);
    if(pq->qBase16 == 0)
    {
        return -1;
    }
    pq->front = pq->rear = 0;
    pq->count = queueCount;

    return 0;
}

void queueDeInit( Queue *pq)
{
    if(pq->qBase)
    {
        free(pq->qBase);
    }
    pq->qBase = 0;
    pq->front = pq->rear = 0;
}

int queueIsEmpty(Queue *pq)
{
    return pq->front == pq->rear;
}

int queueIsFull(Queue *pq)
{
    return ( ((pq->rear + 1)%pq->count ) == pq->front );
}

void queueIn(Queue *pq, unsigned char in)
{
    if( queueIsFull(pq) )
    {
    }
    else
    {
        pq->qBase[pq->rear] = in;
        pq->rear=(pq->rear+1)%pq->count;
    }
}

void queueOut( Queue *pq, unsigned char* out )
{
    if( queueIsEmpty(pq) )
    {
    }
    else
    {
        *out = pq->qBase[pq->front];
        pq->front = (pq->front+1)%pq->count;
    }
}

void queue16In(Queue *pq, unsigned short in)
{
    if( queueIsFull(pq) )
    {
    }
    else
    {
        pq->qBase16[pq->rear] = in;
        pq->rear=(pq->rear+1)%pq->count;
    }
}

void queue16Out( Queue *pq, unsigned short *out )
{
    if( queueIsEmpty(pq) )
    {
    }
    else
    {
        *out = pq->qBase16[pq->front];
        pq->front = (pq->front+1)%pq->count;
    }
}

void queueClear(Queue *pq)
{
    pq->front = pq->rear = 0;
}

int queueLength(Queue *pq)
{
    int length;

    length = (pq->rear + pq->count - pq->front ) % pq->count;

    return length;
}

