######################################################################
# Automatically generated by qmake (2.01a) ?? 8? 10 20:08:34 2016
######################################################################

TEMPLATE = lib
TARGET = ../../lib/3rdlib/keyboard
DEPENDPATH += .
INCLUDEPATH += .

CONFIG += static

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

# Input
HEADERS += \
           keyboard.h \
           keyboard_p.h\
           keyuart.h \
           keyqueue.h
SOURCES += keyoperation.c \
           ledoperation.c \
           keyuart.c \
           keyqueue.c
