#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <fcntl.h>
#include <termios.h>

#include <stdbool.h>
#include <time.h>

#include "keyboard.h"
#include "keyboard_p.h"

#include "keyuart.h"
#include "keyqueue.h"

//! cfg
#define SCAN_QUEUE_SIZE     (128*KEYBOARD_SCAN_CODE_LEN)
#define KEY_QUEUE_SIZE      (128)

#define CW()    { _cw++;if ( _ccw>0){_ccw--;} }
#define CCW()   { _ccw++;if ( _cw>0){_cw--;} }

//! vars
devKeyStatStru*     paastKeyStat;
devKeyboardStru* n_pstKeyboard = NULL;

//! queue
Queue _scanQueue;
Queue _keyQueue;

unsigned short keyboardGetButton(unsigned short u16Row,
                                 unsigned short u16Col,
                                 unsigned short u16RefBits,
                                 unsigned short u16CurBits)
{
    unsigned short u16Key = KEYBOARD_KEY_INVALID;
	devKeyStatStru* pstKeyStat;

    pstKeyStat = KeyboardGetKeyStatObj(n_pstKeyboard->paastKeyStat,u16Row, u16Col);

	switch(pstKeyStat->u8State)
	{
		case KEYBOARD_KEY_RELEASED:
			{
				if (u16RefBits == 0x01 && u16CurBits == 0x00)
				{
					pstKeyStat->u8State = KEYBOARD_KEY_PRESSED;
                    u16Key = KeyboardGetKey(u16Row, u16Col);
					u16Key = KEYBOARD_PRESS(u16Key);
				}
            }
			break;
		case KEYBOARD_KEY_PRESSED:
			{
				if (u16RefBits == 0x00 && u16CurBits == 0x01)
				{
					pstKeyStat->u8State = KEYBOARD_KEY_RELEASED;
                    u16Key = KeyboardGetKey(u16Row, u16Col);
					u16Key = KEYBOARD_EJECT(u16Key);
				}
			}
			break;
		default:
			break;
	}

	return u16Key;
}

unsigned short keyboardGetKnob(unsigned char u8Row,
                               unsigned char u8Col,
                               unsigned char u8RefBits,
                               unsigned char u8CurBits )
{
    u8RefBits = u8RefBits;
    unsigned short u16Key = KEYBOARD_KEY_INVALID;
    devKeyStatStru* pstKeyStat;

    bool bInc, bDec;

    static int _cw = 0, _ccw = 0;

    pstKeyStat = KeyboardGetKeyStatObj(n_pstKeyboard->paastKeyStat,u8Row, u8Col);
 
    bInc = false;
    bDec = false;

    switch(pstKeyStat->u8State)
    {
        case KEYBOARD_KEY_KONB_IDLE_A:
        {
             if (u8CurBits == KEYBOARD_KEY_KONB_STAT_2)                          /**< 0x00->0x02 */
             {
                 pstKeyStat->u8State = u8CurBits;
                 bInc = true;
             }
             else if(u8CurBits == KEYBOARD_KEY_KONB_STAT_1)                      /**< 0x00->0x01 */
             {
                 pstKeyStat->u8State = u8CurBits;
                 bDec = true;
             }
        }
        break;

        case KEYBOARD_KEY_KONB_STAT_2:
        {
        	if (u8CurBits == KEYBOARD_KEY_KONB_IDLE_B)                          /**< 0x02->0x03 */
        	{
        		pstKeyStat->u8State = u8CurBits;
                bInc = true;
        	}
        	else if(u8CurBits == KEYBOARD_KEY_KONB_IDLE_A)                      /**< 0x02->0x00 */
        	{
        		pstKeyStat->u8State = u8CurBits;
                bDec = true;
        	}
        }
        break;

        case KEYBOARD_KEY_KONB_IDLE_B:
        {
        	if (u8CurBits == KEYBOARD_KEY_KONB_STAT_1)                          /**< 0x03->0x01 */
        	{
        		pstKeyStat->u8State = u8CurBits;
                bInc = true;
        	}
        	else if(u8CurBits == KEYBOARD_KEY_KONB_STAT_2)                      /**< 0x03->0x02 */
        	{
        		pstKeyStat->u8State = u8CurBits;
                bDec = true;
        	}
        }
        break;

        case KEYBOARD_KEY_KONB_STAT_1:
        {
        	if (u8CurBits == KEYBOARD_KEY_KONB_IDLE_A)                          /**< 0x01->0x00 */
        	{
        		pstKeyStat->u8State = u8CurBits;
                bInc = true;
        	}
        	else if(u8CurBits == KEYBOARD_KEY_KONB_IDLE_B)                      /**< 0x01->0x03 */
        	{
        		pstKeyStat->u8State = u8CurBits;
                bDec = true;
        	}
        }
        break;

        default:
        	break;
    }
    
    //! counter
    if ( bInc )
    {
        CW();
    }

    if ( bDec )
    {
        CCW();
    }

    //! special knob
    u16Key = KeyboardGetKey( u8Row, u8Col );
    if ( u16Key == KEY_FUNC )
    {
        if ( _cw>=4 )
        {
            u16Key = KEYBOARD_KNOB_CLOCKWISE(u16Key);
            _cw = 0;
        }
        else if ( _ccw >= 4 )
        {
            u16Key = KEYBOARD_KNOB_COUNTERCLOCKWISE(u16Key);
            _ccw = 0;
        }
        else
        {
            u16Key = KEYBOARD_KEY_INVALID;
        }
    }
    else
    {
        //! export key
        if ( _cw >=2 )
        {
            u16Key = KeyboardGetKey(u8Row, u8Col);
            u16Key = KEYBOARD_KNOB_CLOCKWISE(u16Key);
            _cw = 0;
        }
        else if ( _ccw>=2 )
        {
            u16Key = KeyboardGetKey(u8Row, u8Col);
            u16Key = KEYBOARD_KNOB_COUNTERCLOCKWISE(u16Key);
            _ccw = 0;
        }
        else
        {
            u16Key = KEYBOARD_KEY_INVALID;
        }
    }

    return u16Key;
}

void keyboardDecode( devKeyScanCodeStru* pstRefScanCode,
                     devKeyScanCodeStru* pstCurScanCode,
                     u32 deltaTime )
{
    unsigned short i, j;
    unsigned short u16RowBits;
    unsigned short u16Col;
    unsigned short u16Row;

    unsigned short u16ScanVal;

    unsigned short u16Key = KEYBOARD_KEY_INVALID;

    for ( i = 0; i < KEYBOARD_SCAN_COL_NUM; i++ )
	{
		u16RowBits = pstRefScanCode->au16Code[i] ^ pstCurScanCode->au16Code[i];    

        //! no change
		if (u16RowBits == 0)
		{
			continue;    
		}
		else
		{
        }

        //! button
        u16ScanVal = u16RowBits & KEYBOARD_BUTTON_MASK;
        if ( u16ScanVal != 0 )
        {
            //! find the change bit
            for (j = 0; j < KEYBOARD_SCAN_ROW_NUM; j++)
            {
                if ( ( (u16ScanVal>>j)&(0x01) ) != 0  )
                {
                    u16Row = j + 1;
                    u16Col = i + 1;
                    u16Key = keyboardGetButton(u16Row,u16Col,
                                               (pstRefScanCode->au16Code[i]>>j)&0x01,
                                               (pstCurScanCode->au16Code[i]>>j)&0x01);

                    if (u16Key != KEYBOARD_KEY_INVALID )
                    {
                        //! time filter
                        if ( deltaTime < KEYBOARD_BTN_NRJ_TIME )
                        {
                            break;
                        }

                        queue16In( &_keyQueue, u16Key );
                        break;
                    }
                }
            }
        }

        //! knob56
        u16ScanVal = u16RowBits & KEYBOARD_KNOB56_MASK;
        if ( u16ScanVal != 0 )
        {
            u16Row = KEYBOARD_SCAN_KNOB_5COL;
            u16Col = i + 1;
            u16Key = keyboardGetKnob(u16Row, u16Col,
                                            KeyboardGetKnobBits5(pstRefScanCode->au16Code[i]),
                                            KeyboardGetKnobBits5(pstCurScanCode->au16Code[i]));

            if (u16Key != KEYBOARD_KEY_INVALID)
            {
                //! time filter
                if ( deltaTime < KEYBOARD_KNOB_NRJ_TIME )
                {
                    break;
                }

                queue16In( &_keyQueue, u16Key );
                break;
            }
        }

        //! knob89
        u16ScanVal = u16RowBits & KEYBOARD_KNOB89_MASK;
        if ( u16ScanVal != 0 )
        {
            u16Row = KEYBOARD_SCAN_KNOB_8COL;
            u16Col = i + 1;
            u16Key = keyboardGetKnob(u16Row, u16Col,
                                            KeyboardGetKnobBits8(pstRefScanCode->au16Code[i]),
                                            KeyboardGetKnobBits8(pstCurScanCode->au16Code[i]) );

            if (u16Key != KEYBOARD_KEY_INVALID)
            {
                //! time filter
                if ( deltaTime < KEYBOARD_KNOB_NRJ_TIME )
                {
                    break;
                }

                queue16In( &_keyQueue, u16Key );
                break;
            }
        }
	}
}

//! reset the state by the current scan code
void keyboardStatRst(
                     devKeyScanCodeStru* pstCurScanCode,
                     u32 deltaTime )
{
    deltaTime = deltaTime;
    unsigned short i, j;
    unsigned short u16RowBits;
    unsigned short u16ScanVal;

    int buttonRows[]={0,3,6,7,8,9,10,11};
    int rowNow;


    devKeyStatStru* pstKeyStat;

    for ( i = 0; i < KEYBOARD_SCAN_COL_NUM; i++ )
    {
        u16RowBits = pstCurScanCode->au16Code[i];

        //! button
        u16ScanVal = u16RowBits & KEYBOARD_BUTTON_MASK;

        //! for each button
        for ( j = 0; j < sizeof(buttonRows)/sizeof(buttonRows[0]); j++ )
        {
            rowNow = buttonRows[j];

            pstKeyStat = KeyboardGetKeyStatObj(n_pstKeyboard->paastKeyStat,
                                               rowNow + 1,
                                               i + 1 );
            //! key state
            if ( u16ScanVal & (1<<rowNow) )
            {
                pstKeyStat->u8State = KEYBOARD_KEY_RELEASED;
            }
            else
            {
                pstKeyStat->u8State = KEYBOARD_KEY_PRESSED;
            }
        }

        //! knob56
        u16ScanVal = u16RowBits & KEYBOARD_KNOB56_MASK;
        pstKeyStat = KeyboardGetKeyStatObj(n_pstKeyboard->paastKeyStat, KEYBOARD_SCAN_KNOB_5COL, i + 1 );
        pstKeyStat->u8State = KeyboardGetKnobBits5( u16ScanVal );

        //! knob89
        u16ScanVal = u16RowBits & KEYBOARD_KNOB89_MASK;
        pstKeyStat = KeyboardGetKeyStatObj(n_pstKeyboard->paastKeyStat, KEYBOARD_SCAN_KNOB_8COL, i + 1 );
        pstKeyStat->u8State = KeyboardGetKnobBits8( u16ScanVal );
    }
}

bool keyboardGetValidScanCode(unsigned int u32MaxBytes,
                              devKeyScanCodeStru* pstScanCode,
                              unsigned int* pu32ReadBytes
                               )
{
    u32MaxBytes = u32MaxBytes;
    unsigned char i;

    //! find the sig
    unsigned char head;
    queueOut( &_scanQueue, &head );
    if ( KEYBOARD_FRAME_SIG != head )
    {
        *pu32ReadBytes = 1;
        return false;
    }

    //! get the full frame
    pstScanCode->au8Code[0] = head;
    for( i=1;i<KEYBOARD_SCAN_CODE_LEN;i++)
    {
        queueOut( &_scanQueue, pstScanCode->au8Code+i );
    }
    *pu32ReadBytes = KEYBOARD_SCAN_CODE_LEN;

    return true;
}

void keyboardTranScanCode2Key(void)
{
    bool bValid = 0;

    s32 i,j;

    u32 u32RcvBytes, u32ReadBytes;
    u16 *pAlignCode;
    u8  *pRawCode;
    u32 timeStampe;

    //! queue length
    u32RcvBytes = queueLength( &_scanQueue );

    //! more than a frame
	while(u32RcvBytes >= KEYBOARD_SCAN_CODE_LEN)
	{
        bValid = keyboardGetValidScanCode(u32RcvBytes,
                                          &n_pstKeyboard->stKeyScanCode,
                                          &u32ReadBytes );
		u32RcvBytes -= u32ReadBytes;
        if ( !bValid )
		{
            n_pstKeyboard->bPreValid = false;
            continue;
		}

        //! convert code
        pAlignCode = n_pstKeyboard->stKeyScanCode.au16Code;
        pRawCode = n_pstKeyboard->stKeyScanCode.au8Code;
        for ( i = 0, j = 1; i < 8 && j < 13;  )
        {
            pAlignCode[i] = (pRawCode[j]<<4) | ( (pRawCode[j+1]>>4) & 0x0f );
            pAlignCode[i] &= KEYBOARD_SCAN_ROW_MASK;
            i++;

            pAlignCode[i] = ((pRawCode[j+1]&0x0f)<<8) | pRawCode[j+2];
            pAlignCode[i] &= KEYBOARD_SCAN_ROW_MASK;
            i++; j += 3;
        }

        //! convert time
        timeStampe = (pRawCode[13]<<16) |
                     (pRawCode[14]<<8) |
                     (pRawCode[15]<<0);

        //! pre code -> current code
        if ( n_pstKeyboard->bPreValid )
        {
            keyboardDecode( &n_pstKeyboard->preKeyCode,
                            &n_pstKeyboard->stKeyScanCode,
                            timeStampe );
        }
        else
        {
            keyboardStatRst( &n_pstKeyboard->stKeyScanCode,
                             timeStampe );
        }

        //! shift in code
        memcpy( n_pstKeyboard->preKeyCode.au16Code,
                n_pstKeyboard->stKeyScanCode.au16Code,
                sizeof(n_pstKeyboard->stKeyScanCode.au16Code) );

        n_pstKeyboard->bPreValid = bValid;
	}
}

void keyboardClear()
{
    tcflush(n_pstKeyboard->fHandle, TCIFLUSH);
    queueClear( &_scanQueue );
}

int keyboardOpen(char* path,int speed,int bit,char event,int stop)
{
    int err;
    int rstCode;

    //! queue init
    queueCreate( &_scanQueue );
    queueCreate( &_keyQueue );

    //! keyboard obj
	n_pstKeyboard = (devKeyboardStru*)calloc(1,sizeof(devKeyboardStru)); 
	if(n_pstKeyboard == NULL)
	{
        return -2;
	}

    //! init handle
    n_pstKeyboard->fHandle = -1;
    n_pstKeyboard->bPreValid = 0;

	do{
        n_pstKeyboard->paastKeyStat = (devKeyStatStru*)calloc(
                                             KEYBOARD_SCAN_COL_NUM*KEYBOARD_SCAN_ROW_NUM,
                                             sizeof(devKeyStatStru) );
        if (n_pstKeyboard->paastKeyStat == NULL)
        { break; }

        err = keyuartOpen(path);
        if ( err < 0 )
        {   break; }

        err = keyuartSetup(speed,bit,event,stop);
        if ( err < 0 )
        {   break; }
		
        //! scan queue buf
        err = queueInit(&_scanQueue, SCAN_QUEUE_SIZE );
        if ( err < 0 )
        {   break; }

        //! key queue buf
        err = queue16Init( &_keyQueue, KEY_QUEUE_SIZE );
        if ( err < 0 )
        {   break; }

        //! reset the keyboard
        rstCode = 0x00;
        write( n_pstKeyboard->fHandle, &rstCode, 1 );

        return 0;
		 
	}while(0);

    //! on error proc
	if (n_pstKeyboard != NULL)
	{
        if ( n_pstKeyboard->fHandle >= 0 )
        {
            close( n_pstKeyboard->fHandle );
            n_pstKeyboard->fHandle = -1;
        }

		if (n_pstKeyboard->paastKeyStat != NULL)
		{
			free(n_pstKeyboard->paastKeyStat);
            n_pstKeyboard->paastKeyStat = NULL;
		}
		
		free(n_pstKeyboard);
		
		n_pstKeyboard = NULL;

        queueDeInit( &_scanQueue );
        queueDeInit( &_keyQueue );
	}

    return -3;
}

void keyboardClose()
{
    close( n_pstKeyboard->fHandle );

    free(n_pstKeyboard->paastKeyStat);

    free( n_pstKeyboard );

    queueDeInit( &_scanQueue );
    queueDeInit( &_keyQueue );
}

int keyboardInit(void)
{
    return keyboardOpen( "/dev/ttyPS1",
                     1000000,
                     8,
                     'N',
                     1 );
}

void keyboardDeInit()
{
    keyboardClose();
}

void keyboardScan()
{
    keyuartRecv( KEYBOARD_SCAN_CODE_LEN );
    keyboardTranScanCode2Key();
}

unsigned short keyboardGetKey()
{
    if ( queueIsEmpty(&_keyQueue) )
    {
        return KEYBOARD_INVALID_KEYVAL;
    }
    else
    {
        unsigned short key;
        queue16Out( &_keyQueue, &key );
        return key;
    }
}
