
#ifndef _KEY_HEADER_H
#define _KEY_HEADER_H

//! type
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef char s8;
typedef short s16;
typedef int s32;
typedef long long s64;

typedef float f32;
typedef double f64;

//! macro
#define KEYBOARD_SCAN_CODE_LEN        16        //! 1 + 12 + 3

#define KEYBOARD_BTN_NRJ_TIME         (500)     //! 5ms/10us
#define KEYBOARD_KNOB_NRJ_TIME        (5)       //! 50us

#define KEYBOARD_SCAN_COL_NUM         8                                          /**< 键盘扫描列个数 */
#define KEYBOARD_SCAN_ROW_NUM         12                                         /**< 键盘扫描行个数 */
#define KEYBOARD_SCAN_ROW_MASK        (0x0fff)

#define KEYBOARD_SCAN_KNOB_5COL        6
#define KEYBOARD_SCAN_KNOB_8COL        3

#define KEYBOARD_FRAME_SIG          0xAA

/**!
 * 键值相关定义
 */
#define KEYBOARD_KEY_INVALID            0x00

#define KEYBOARD_PRESS_MASK                   ((u16)1<<9)                   /**< 按下 */
#define KEYBOARD_CLOCKWISE_MASK               ((u16)1<<11)                  /**< 顺时针 */
#define KEYBOARD_COUNTERCLOCKWISE_MASK        ((u16)1<<12)                  /**< 逆时针 */

#define KEYBOARD_EJECT(key)                   (key)                             /**< 按键弹起 无需处理 */
#define KEYBOARD_PRESS(key)                   ((key) | KEYBOARD_PRESS_MASK)     /**< 按键按下 */
#define KEYBOARD_KNOB_CLOCKWISE(key)          ((key) | KEYBOARD_CLOCKWISE_MASK) /**< 旋钮 顺时针旋转 */
#define KEYBOARD_KNOB_COUNTERCLOCKWISE(key)   ((key) | KEYBOARD_COUNTERCLOCKWISE_MASK)                                    /**< 旋钮逆时针旋转 */

#define KeyboardGetKeyStatObj(paastKeyStat, u16Row, u16Col)\
                   (paastKeyStat + (u16Row - 1) * KEYBOARD_SCAN_COL_NUM + (u16Col - 1))

#define KeyboardGetKey(u16Row,u16Col) (((u16)(u16Col) << 4) | (u16)(u16Row))

#define KEYBOARD_BUTTON_MASK    (0xfc9)
#define KEYBOARD_KNOB56_MASK    (0x30)
#define KEYBOARD_KNOB89_MASK    (0x06)

#define KeyboardGetKnobBits5(u8Bits) (((u8Bits) & (KEYBOARD_KNOB56_MASK))>> 4)
#define KeyboardGetKnobBits8(u8Bits) (((u8Bits) & (KEYBOARD_KNOB89_MASK))>> 1)

/**
 * 按键状态
 */
typedef struct
{
    u8 u8State;
}devKeyStatStru;

typedef struct {
    u8 au8Code[KEYBOARD_SCAN_CODE_LEN];
    u16 au16Code[ KEYBOARD_SCAN_COL_NUM ];
}devKeyScanCodeStru;

typedef enum
{
	KEYBOARD_KEY_RELEASED,
	KEYBOARD_KEY_PRESSED
}devKeyPressStatEnum;

/**
 *逆时针顺序 00->10->11->01->00
 *顺时针顺序 00->01->11->10->00
 */
typedef enum
{
	KEYBOARD_KEY_KONB_IDLE_A = 0x00,
	KEYBOARD_KEY_KONB_STAT_1 = 0x01,
	KEYBOARD_KEY_KONB_STAT_2 = 0x02,
	KEYBOARD_KEY_KONB_IDLE_B = 0x03,
}devKeyKnobStatEnum;

typedef struct
{
    u8                  bPreValid;
    s32                 fHandle;
    devKeyStatStru*     paastKeyStat;   /**< 二维数组指针  以行、列为索引访问  array[row][col] */
    devKeyScanCodeStru  preKeyCode;     /**< previous scan code*/
    devKeyScanCodeStru  stKeyScanCode;  /*!< scan code now */
}devKeyboardStru;

extern devKeyboardStru* n_pstKeyboard;

#endif
