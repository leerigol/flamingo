#ifndef CKEYSCAN_H
#define CKEYSCAN_H

#include <QThread>
#include <QQueue>

#include "../../engine/enginemsg.h"

namespace app_key_board
{

typedef QQueue<quint16> KeyScanQueue;
typedef QQueue<quint32> LedOpQueue;

class CKeyScan : public QThread
{
    Q_OBJECT

public:

    const static int DATA_KEY_COUNT     =  256;
    const static int DATA_FILE_FLAG     =  0xABCD1234;

    static int m_au32KeyCount[DATA_KEY_COUNT];

    static void ledOp( int led, bool b );

    void   loadDataKey();
    void   saveDataKey();

private:
    static LedOpQueue *m_pLedOpQueue;

public:
    CKeyScan();    

protected:
    virtual void run();

public:
    void attachQueue( KeyScanQueue *pQueue );

private:
    unsigned int findPhyLed( quint32 opLed );

public:
    static QMutex mKeyMutex;
private:
    KeyScanQueue *m_pQueue;

};

}

#endif // CKEYSCAN_H
