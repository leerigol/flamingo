#ifndef CKEYPROC_H
#define CKEYPROC_H

#include <QThread>
#include "ckeyscan.h"

namespace app_key_board
{

/*!
 * \brief The CKeyProc class
 * 键盘处理过程
 * - 包含键盘扫描和键值投递
 * - 键盘扫描：接收来自串口的键盘扫描码，并提取出键值
 * - 键盘扫描所提取的键值被缓存在队列中，由键值投递线程发送到Qt事件系统中
 */
class CKeyProc : public QThread
{
    Q_OBJECT

public:
    CKeyProc();

protected:
    virtual void run();

protected:
    void postKey( quint16 keyCode,
                  int cnt,
                  int deltaTime );
private:
    KeyScanQueue mKeyScanQueue;
    QString mkeyTxt;
};

}

#endif // CKEYPROC_H
