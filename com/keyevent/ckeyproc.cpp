

#include <QKeyEvent>
#include <QApplication>
#include <QDebug>

#include "appkeyboard.h"
#include "ckeyproc.h"

#include "../keyboard/keyboard.h"
#include "../../include/dsodbg.h"
#define KEY_PROC_INTERVAL   50     //! ms

namespace app_key_board
{

CKeyProc::CKeyProc()
{
    mkeyTxt.clear();
}

/*!
 * \brief CKeyProc::run
 * -从键值队列中提取键
 */
void CKeyProc::run()
{
#ifdef _SIMULATE
    #ifndef _DSO_KEYBOARD
        return;
    #else
    #endif
#endif

    quint16 keyNow, keyLast;
    int keyCnt;
    int idleCnt;
    //! 1. run the key scan thread
    CKeyScan keyScanT;

    keyScanT.attachQueue( &mKeyScanQueue );

    keyScanT.start();

    //! now for proc thread
    idleCnt = 0;
    while( 1 )
    {
        //! 提取出相同的键，累计出键的数量
        keyCnt = 0;
        keyLast = KEYBOARD_INVALID_KEYVAL;
        while( !mKeyScanQueue.isEmpty() )
        {
            CKeyScan::mKeyMutex.lock();
            keyNow = mKeyScanQueue.dequeue();
            CKeyScan::mKeyMutex.unlock();

            //! key do not change
            if ( keyNow == keyLast )
            {
                keyCnt++;
            }
            //! key change
            else
            {
                //! has key in history
                if ( keyCnt != 0 )
                {
                    //! post the last key
                    postKey( keyLast,
                             keyCnt,
                             idleCnt * KEY_PROC_INTERVAL );
                }
                //! the first key
                else
                {
                }

                keyCnt = 1;
                keyLast = keyNow;
            }
        }

        //! post the current key
        if ( keyCnt > 0 )
        {
            postKey( keyLast, keyCnt, idleCnt * KEY_PROC_INTERVAL );

            //! release
            if ( KEY_STATUS(keyLast) != KEY_DOWN )
            { idleCnt = 0; }
        }
        //! no key
        else
        {
            idleCnt++;

            //! limit the range
            //! 10s*1000ms/50ms
            if ( idleCnt > 10*1000/KEY_PROC_INTERVAL )
            { idleCnt = 10*1000/KEY_PROC_INTERVAL; }
        }

        msleep( KEY_PROC_INTERVAL );        //! time interval
    }
}

/*!
 * \brief CKeyProc::postKey
 * \param keyCode
 * \param cnt
 * \param deltaTime 键事件的时间间隔 ms
 * 转换键到 事件(QEvent)
 */
void CKeyProc::postKey( quint16 keyCode,
                        int cnt,
                        int deltaTime )
{
    QKeyEvent *pEvt;

    QEvent::Type keyType;

    if ( KEY_STATUS(keyCode) == KEY_DOWN )
    {
        keyType = QEvent::KeyPress;
        keyCode &= 0xff;
    }
    else if ( KEY_STATUS(keyCode) == KEY_UP )
    {
        keyType = QEvent::KeyRelease;
        keyCode &= 0xff;
    }
    else
    {
        keyType = QEvent::KeyRelease;
    }

//    if( system_lock )
//    {
//        return;
//    }
//    qWarning("%x, %x, %d, %d", keyCode, Encode_ScanCode(keyCode), cnt, (int)keyType );
    //! new event
    //! 键描述用字符表示键的时间间隔
    mkeyTxt = QString::number( deltaTime );
    pEvt = R_NEW( QKeyEvent( keyType,
                          Encode_ScanCode(keyCode),
                          Qt::NoModifier,
                          mkeyTxt,
                          false,
                          cnt ) );

    qApp->postEvent( qApp, (QEvent*)pEvt );
}

}



