#include <QDebug>
#ifdef _DSO_KEYBOARD
#include <QSerialPort>
#endif
#include "ckeyscan.h"

#include "../keyboard/keyboard.h"   //! key board scan
#include "../../include/dsodbg.h"
namespace app_key_board
{

#define led_on_op( led )        (1<<31) | ( ((quint32)led) & 0x7fffffff )
#define led_off_op( led )       ( ((quint32)led) & 0x7fffffff )

const static QString DATA_STAT_FILE = QString("/rigol/data/stat.dat");
int CKeyScan::m_au32KeyCount[DATA_KEY_COUNT];

QMutex CKeyScan::mKeyMutex;
//! led op key
LedOpQueue *CKeyScan::m_pLedOpQueue;

static quint32 _led_matrix[]=
{
    DsoEngine::led_ch1,     LED_CH1,
    DsoEngine::led_ch2,     LED_CH2,
    DsoEngine::led_ch3,     LED_CH3,
    DsoEngine::led_ch4,     LED_CH4,

    DsoEngine::led_source1, LED_SOURCE1,
    DsoEngine::led_source2, LED_SOURCE2,

    DsoEngine::led_digital, LED_DIGITAL,
    DsoEngine::led_math,    LED_MATH,
    DsoEngine::led_ref,     LED_REF,
    DsoEngine::led_decode,  LED_DECODE,

    DsoEngine::led_run,     LED_RUN,
    DsoEngine::led_stop,    LED_STOP,
    DsoEngine::led_single,  LED_SINGLE,

    DsoEngine::led_func,    LED_FUNC,
    DsoEngine::led_touch,   LED_TOUCH,
};

#ifndef _SIMULATE
void CKeyScan::ledOp( int led, bool b )
{

    Q_ASSERT( CKeyScan::m_pLedOpQueue != NULL );

    if ( m_pLedOpQueue->size()>64 )
    { return; }

    quint32 ledOp;
    if ( b )
    { ledOp = led_on_op( led ); }
    else
    { ledOp = led_off_op( led ); }

    CKeyScan::mKeyMutex.lock();
    m_pLedOpQueue->enqueue( ledOp );
    CKeyScan::mKeyMutex.unlock();
}
#else
void CKeyScan::ledOp( int /*led*/, bool /*b*/ )
{
}
#endif

CKeyScan::CKeyScan()
{
    m_pQueue = NULL;
    m_pLedOpQueue = NULL;

    //! led op key
    CKeyScan::m_pLedOpQueue = R_NEW( LedOpQueue() );
    Q_ASSERT( CKeyScan::m_pLedOpQueue != NULL );


    for(int i=0; i<DATA_KEY_COUNT; i++)
    {
        m_au32KeyCount[i] = 0;
    }
}

void CKeyScan::loadDataKey()
{
    QFile file(DATA_STAT_FILE);
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream in( &file );
        int flag=0;


        in >> flag;
        if( flag == DATA_FILE_FLAG &&
            file.size() == (DATA_KEY_COUNT+1)*4)
        {
            for(int i=0; i<DATA_KEY_COUNT; i++)
            {
                in >> flag;
                m_au32KeyCount[i] = flag;
            }
        }

        file.close();
    }
}

void CKeyScan::saveDataKey()
{
    QFile file(DATA_STAT_FILE);
    if( file.open(QIODevice::WriteOnly | QIODevice::Truncate ) )
    {
        QDataStream out( &file );
        out << DATA_FILE_FLAG;
        for(int i=0; i<DATA_KEY_COUNT; i++)
        {
            out << m_au32KeyCount[i];
        }
        file.close();
    }
}

/*!
 * \brief CKeyScan::run
 * 键盘扫描线程
 * -循环提取键值
 * -将led控制信号发送到键盘
 * \todo 将扫描和处理线程改成信号量同步
 */
void CKeyScan::run()
{
    //! key assert
    Q_ASSERT( m_pQueue != NULL );

#ifndef _SIMULATE

    quint16 key;
    quint32 ledOp;
    quint32 phyLed, onLed, offLed;

    qlonglong timeout = 0;
    //! init kb
    keyboardInit();

    //! all key off
    keyboardLedOff( 0xffffffff );

    while( 1 )
    {
        //! scan key
        keyboardScan();

        //! empty the keys
        do
        {
            key = keyboardGetKey();
            if ( key == KEYBOARD_INVALID_KEYVAL )
            {
                break;
            }

            CKeyScan::mKeyMutex.lock();
            m_pQueue->enqueue( key );
            CKeyScan::mKeyMutex.unlock();

            if( key > 0 && key < 256 )
            {
                m_au32KeyCount[key]++;
            }

        }while( 1 );

        //! for led proc
        onLed = 0;
        offLed = 0;
        Q_ASSERT( CKeyScan::m_pLedOpQueue != NULL );
        while ( !CKeyScan::m_pLedOpQueue->isEmpty() )
        {
            CKeyScan::mKeyMutex.lock();
            ledOp = CKeyScan::m_pLedOpQueue->dequeue();
            CKeyScan::mKeyMutex.unlock();

            phyLed = findPhyLed( ledOp & 0x7fffffff );

            //! find none
            if ( phyLed == 0 )
            {
                continue;
            }

            //! on
            if ( ledOp & 0x80000000 )
            {
                onLed |= phyLed;
                offLed &= ~phyLed;
            }
            //! off
            else
            {
                offLed |= phyLed;
                onLed &= ~phyLed;
            }
        }

        if ( onLed != 0 )
        {
            keyboardLedOn( onLed );
        }
        if ( offLed != 0 )
        {
            keyboardLedOff( offLed );
        }

        //save key stat data every 30s
        timeout++;
        if( (timeout % 1500) == 0 )
        {
            saveDataKey();
        }
        msleep( 20 );
    }

    keyboardClose();

#endif
#ifdef _DSO_KEYBOARD

    //! com port init
    QSerialPort *comPort;
    comPort = new QSerialPort();
    Q_ASSERT( NULL!=comPort );

    comPort->setPortName("ttyS0");
    comPort->setBaudRate( QSerialPort::Baud115200 );
    if ( !comPort->open( QIODevice::ReadWrite) )
    {
        delete comPort;
        qWarning()<<"Fail to open com port";
        return;
    }
    else
    {
        qWarning()<<"Success to open com port";
    }

    QByteArray arrayRead;
    QString  strSend;
    QList<QByteArray> listRead;
    QString strKey;
    quint16 key;
    bool bOk;

    quint32 ledOp;
    quint32 phyLed;

    forever
    {
        //! key input
        if ( comPort->waitForReadyRead(10) )
        {
            arrayRead = comPort->readAll();
            arrayRead.replace('\"',"");
            arrayRead.replace('\n',"");
            arrayRead.replace('\r',"");
            listRead = arrayRead.split('#');
            foreach( strKey, listRead )
            {
                key = strKey.toUShort( &bOk, 16 );
                if ( bOk )
                {
                    m_pQueue->enqueue( key );
                }
            }
        }

        //! led op.
        Q_ASSERT( CKeyScan::m_pLedOpQueue != NULL );
        while ( !CKeyScan::m_pLedOpQueue->isEmpty() )
        {
            ledOp = CKeyScan::m_pLedOpQueue->dequeue();

            phyLed = findPhyLed( ledOp & 0x7fffffff );

            //! find none
            if ( phyLed == 0 )
            {
                continue;
            }

            //! on
            if ( ledOp & 0x80000000 )
            {
                strSend = QString("#1,%1#").arg(phyLed,0,16);
            }
            //! off
            else
            {
                strSend = QString("#0,%1#").arg(phyLed,0,16);
            }
            comPort->write( strSend.toLatin1() );
            comPort->waitForBytesWritten( 10 );
        }
    }
#endif

}

void CKeyScan::attachQueue( KeyScanQueue *pQueue )
{
    Q_ASSERT( pQueue != NULL );
    m_pQueue = pQueue;
}

unsigned int CKeyScan::findPhyLed( quint32 opLed )
{
    quint32 i;

    for ( i = 0;
          i < sizeof(_led_matrix)/sizeof(_led_matrix[0]);
          i+=2  )
    {
        if( _led_matrix[i] == opLed )
        {
            return _led_matrix[i+1];
        }
    }

    //! do not find
    return 0;
}

}
