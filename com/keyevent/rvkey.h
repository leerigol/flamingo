#ifndef RVKEY
#define RVKEY

//! phisical key id
enum RVKey
{
 key_none = 0,

 key_Multi_Knob,
 key_pre,
 key_stop,
 key_next,

 key_wave_scale,
 key_wave_offset,

};

#endif // RVKEY

