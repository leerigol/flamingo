
#ifndef _R_DSO_KEY_H_
#define _R_DSO_KEY_H_

//#define _DSO_KEYBOARD
//! 示波器物理键盘
#ifdef _DSO_KEYBOARD

#include "../keyboard/keyboard.h"
#include "appkeyboard.h"

//! inc/dec key
#define encode_inc(key)     ( key | (KNOB_CLOCKWISE) )
#define encode_dec(key)     ( key | (KNOB_CONUT_CLOCK) )

//! physical key

#define R_Pkey_null     0

//! menu
#define R_Pkey_F1       Encode_ScanCode( KEY_MENU_F1 )
#define R_Pkey_F2       Encode_ScanCode( KEY_MENU_F2 )
#define R_Pkey_F3       Encode_ScanCode( KEY_MENU_F3 )
#define R_Pkey_F4       Encode_ScanCode( KEY_MENU_F4 )
#define R_Pkey_F5       Encode_ScanCode( KEY_MENU_F5 )
#define R_Pkey_F6       Encode_ScanCode( KEY_MENU_F6 )
#define R_Pkey_F7       Encode_ScanCode( KEY_MENU_F7 )

#define R_Pkey_PageReturn   Encode_ScanCode( KEY_MENU_BACK )

#define R_Pkey_PageOff  Encode_ScanCode( KEY_MENU_OFF )

//! 物理上没有这两个键
#define R_Pkey_PageUp   Qt::Key_PageUp
#define R_Pkey_PageDown Qt::Key_PageDown

#define R_Pkey_FInc     Encode_ScanCode( encode_inc( KEY_FUNC ) )
#define R_Pkey_FDec     Encode_ScanCode( encode_dec( KEY_FUNC ) )
#define R_Pkey_FZ       Encode_ScanCode( KEY_FUNC_Z )
#define R_Pkey_FUNC     Encode_ScanCode( KEY_FUNC )
//! ch
#define R_Pkey_CH1      Encode_ScanCode( KEY_CH1_MENU )
#define R_Pkey_CH2      Encode_ScanCode( KEY_CH2_MENU )
#define R_Pkey_CH3      Encode_ScanCode( KEY_CH3_MENU )
#define R_Pkey_CH4      Encode_ScanCode( KEY_CH4_MENU )

#define R_Pkey_CH1_VOLT     Encode_ScanCode( KEY_CH1_VOLT )
#define R_Pkey_CH1_VOLT_Z   Encode_ScanCode( KEY_CH1_VOLT_Z )
#define R_Pkey_CH1_POS      Encode_ScanCode( KEY_CH1_POS )
#define R_Pkey_CH1_POS_Z    Encode_ScanCode( KEY_CH1_POS_Z )

#define R_Pkey_CH2_VOLT     Encode_ScanCode( KEY_CH2_VOLT )
#define R_Pkey_CH2_VOLT_Z   Encode_ScanCode( KEY_CH2_VOLT_Z )
#define R_Pkey_CH2_POS      Encode_ScanCode( KEY_CH2_POS )
#define R_Pkey_CH2_POS_Z    Encode_ScanCode( KEY_CH2_POS_Z )

#define R_Pkey_CH3_VOLT     Encode_ScanCode( KEY_CH3_VOLT )
#define R_Pkey_CH3_VOLT_Z   Encode_ScanCode( KEY_CH3_VOLT_Z )
#define R_Pkey_CH3_POS      Encode_ScanCode( KEY_CH3_POS )
#define R_Pkey_CH3_POS_Z    Encode_ScanCode( KEY_CH3_POS_Z )

#define R_Pkey_CH4_VOLT     Encode_ScanCode( KEY_CH4_VOLT )
#define R_Pkey_CH4_VOLT_Z   Encode_ScanCode( KEY_CH4_VOLT_Z )
#define R_Pkey_CH4_POS      Encode_ScanCode( KEY_CH4_POS )
#define R_Pkey_CH4_POS_Z    Encode_ScanCode( KEY_CH4_POS_Z )

//! app
#define R_Pkey_LA       Encode_ScanCode( KEY_LA_MENU )
#define R_Pkey_DECODE   Encode_ScanCode( KEY_DECODE_MENU )

#define R_Pkey_MATH     Encode_ScanCode( KEY_MATH_MENU )
#define R_Pkey_REF      Encode_ScanCode( KEY_REF_MENU )
#define R_Pkey_SOURCE1   Encode_ScanCode( KEY_SOURCE1_MENU )
#define R_Pkey_SOURCE2   Encode_ScanCode( KEY_SOURCE2_MENU )

#define R_Pkey_MEASURE  Encode_ScanCode( KEY_MEASURE )
#define R_Pkey_ACQUIRE  Encode_ScanCode( KEY_ACQUIRE )
#define R_Pkey_STORAGE  Encode_ScanCode( KEY_STORAGE )
#define R_Pkey_CURSOR   Encode_ScanCode( KEY_CURSOR )

#define R_Pkey_DISPLAY  Encode_ScanCode( KEY_DISPLAY )
#define R_Pkey_UTILITY  Encode_ScanCode( KEY_UTILITY )

//! quick
#define R_Pkey_CLEAR    Encode_ScanCode( KEY_DISP_CLEAR )
#define R_Pkey_AUTO     Encode_ScanCode( KEY_AUTO )
#define R_Pkey_RunStop  Encode_ScanCode( KEY_RUN_STOP )
#define R_Pkey_SINGLE   Encode_ScanCode( KEY_SINGLE )

#define R_Pkey_QUICK    Encode_ScanCode( KEY_QUICK )
#define R_Pkey_DEFAULT  Encode_ScanCode( KEY_DEFAULT )
#define R_Pkey_TOUCH    Encode_ScanCode( KEY_TOUCH )

#define R_Pkey_PLAY_PRE     Encode_ScanCode( KEY_PLAY_PRE )
#define R_Pkey_PLAY_NEXT    Encode_ScanCode( KEY_PLAY_NEXT )
#define R_Pkey_PLAY_STOP    Encode_ScanCode( KEY_PLAY_STOP )

//! wave
#define R_Pkey_WAVE_VOLT    Encode_ScanCode( KEY_WAVE_VOLT )
#define R_Pkey_WAVE_VOLT_Z  Encode_ScanCode( KEY_WAVE_VOLT_Z )
#define R_Pkey_WAVE_POS     Encode_ScanCode( KEY_WAVE_POS )
#define R_Pkey_WAVE_POS_Z   Encode_ScanCode( KEY_WAVE_POS_Z )

//! trig
#define R_Pkey_TRIG_MENU    Encode_ScanCode( KEY_TRIG_MENU )
#define R_Pkey_TRIG_MODE    Encode_ScanCode( KEY_TRIG_MODE )
#define R_Pkey_TRIG_FORCE    Encode_ScanCode( KEY_TRIG_FORCE )

#define R_Pkey_TRIG_LEVEL       Encode_ScanCode( KEY_TRIG_LEVEL )
#define R_Pkey_TRIG_LEVEL_Z       Encode_ScanCode( KEY_TRIG_LEVEL_Z )

//! hori
#define R_Pkey_HORI_NAGAVITE    Encode_ScanCode( KEY_TIME_NAVIGATE )
#define R_Pkey_HORI_ZOOM        Encode_ScanCode( KEY_HORI_ZOOM )

#define R_Pkey_TIME_SCALE       Encode_ScanCode( KEY_TIME_SCLAE )
#define R_Pkey_TIME_SCALE_Z     Encode_ScanCode( KEY_TIME_SCLAE_Z )
#define R_Pkey_TIME_OFFSET       Encode_ScanCode( KEY_TIME_POS )
#define R_Pkey_TIME_OFFSET_Z     Encode_ScanCode( KEY_TIME_POS_Z )

#else

//! inc/dec key
#define encode_inc(key)     ( key  )
#define encode_dec(key)     ( key  )

//! simulate key
#define R_Pkey_null     0
//! menu
#define R_Pkey_F1       Qt::Key_F1
#define R_Pkey_F2       Qt::Key_F2
#define R_Pkey_F3       Qt::Key_F3
#define R_Pkey_F4       Qt::Key_F4
#define R_Pkey_F5       Qt::Key_F5
#define R_Pkey_F6       Qt::Key_F6
#define R_Pkey_F7       Qt::Key_F7

#define R_Pkey_PageReturn   Qt::Key_Home
#define R_Pkey_PageOff  Qt::Key_End

#define R_Pkey_PageUp   Qt::Key_PageUp
#define R_Pkey_PageDown Qt::Key_PageDown

#define R_Pkey_FInc     Qt::Key_Up
#define R_Pkey_FDec     Qt::Key_Down
#define R_Pkey_FZ       Qt::Key_Right
#define R_Pkey_FUNC     Qt::Key_Right
//! ch
#define R_Pkey_CH1      Qt::Key_1
#define R_Pkey_CH2      Qt::Key_2
#define R_Pkey_CH3      Qt::Key_3
#define R_Pkey_CH4      Qt::Key_4

#define R_Pkey_CH1_VOLT     0
#define R_Pkey_CH1_VOLT_Z   Qt::Key_W
#define R_Pkey_CH1_POS      Qt::Key_E
#define R_Pkey_CH1_POS_Z    Qt::Key_R

#define R_Pkey_CH2_VOLT     Qt::Key_T
#define R_Pkey_CH2_VOLT_Z   Qt::Key_Y
#define R_Pkey_CH2_POS      Qt::Key_U
#define R_Pkey_CH2_POS_Z    Qt::Key_I

#define R_Pkey_CH3_VOLT     Qt::Key_A
#define R_Pkey_CH3_VOLT_Z   0
#define R_Pkey_CH3_POS      Qt::Key_D
#define R_Pkey_CH3_POS_Z    Qt::Key_F

#define R_Pkey_CH4_VOLT     Qt::Key_G
#define R_Pkey_CH4_VOLT_Z   Qt::Key_H
#define R_Pkey_CH4_POS      Qt::Key_J
#define R_Pkey_CH4_POS_Z    0

//! app
#define R_Pkey_LA       Qt::Key_Z
#define R_Pkey_DECODE   Qt::Key_X

#define R_Pkey_MATH     Qt::Key_C
#define R_Pkey_REF      Qt::Key_V
#define R_Pkey_SOURCE1   0
#define R_Pkey_SOURCE2   0

#define R_Pkey_MEASURE  0
#define R_Pkey_ACQUIRE  Qt::Key_O
#define R_Pkey_STORAGE  Qt::Key_S
#define R_Pkey_CURSOR   0

#define R_Pkey_DISPLAY  Qt::Key_BracketLeft
#define R_Pkey_UTILITY  Qt::Key_BracketRight

//! quick
#define R_Pkey_CLEAR    Qt::Key_5
#define R_Pkey_AUTO     Qt::Key_6
#define R_Pkey_RunStop  Qt::Key_7
#define R_Pkey_SINGLE   Qt::Key_8

#define R_Pkey_HELP     Qt::Key_0
#define R_Pkey_QUICK    Qt::Key_Q
#define R_Pkey_DEFAULT  Qt::Key_Back
#define R_Pkey_TOUCH    0

#define R_Pkey_PLAY_PRE     Qt::Key_B
#define R_Pkey_PLAY_NEXT    Qt::Key_N
#define R_Pkey_PLAY_STOP    Qt::Key_M

//! wave
#define R_Pkey_WAVE_VOLT    Qt::Key_F9
#define R_Pkey_WAVE_VOLT_Z  Qt::Key_F10
#define R_Pkey_WAVE_POS     Qt::Key_F11
#define R_Pkey_WAVE_POS_Z   Qt::Key_F12

//! trig
#define R_Pkey_TRIG_MENU    0
#define R_Pkey_TRIG_MODE    0
#define R_Pkey_TRIG_FORCE    0

#define R_Pkey_TRIG_LEVEL       Qt::Key_K
#define R_Pkey_TRIG_LEVEL_Z     Qt::Key_L

//! hori
#define R_Pkey_HORI_NAGAVITE    0
#define R_Pkey_HORI_ZOOM        0

#define R_Pkey_TIME_SCALE       0
#define R_Pkey_TIME_SCALE_Z     0
#define R_Pkey_TIME_OFFSET       0
#define R_Pkey_TIME_OFFSET_Z     0

#endif

//! soft key
//! 菜单软按键
#define R_Skey_Active       0x10001     /*!< down, release */

//! 功能旋钮键动作
#define R_Skey_TuneInc      0x10002     /*!< left, right */
#define R_Skey_TuneDec      0x10003
#define R_Skey_TuneEnter    0x10004     /*!< down, release */

//! 功能
#define R_Skey_Zval         0x10005     /*!< z command */
#define R_Skey_Next         0x10006     //! loop next

//! soft key
#define is_skey( key )  ( (key)>=R_Skey_Active && (key)<=R_Skey_Next )

#endif
