#ifndef APPKEYBOARD
#define APPKEYBOARD

//! 示波器物理键盘
#define KEY_STATUS( status )    (( status) & 0xff00 )

#define Encode_ScanCode( keyScCode )   ( ((keyScCode) & 0xffff ) + 0x40000000 )
#define Decode_QtKey( qtKey )   ( (qtKey) - 0x40000000 )

#endif // APPKEYBOARD

