/*****************************************************************************
                                普源精电科技有限公司版权所有
******************************************************************************
源文件名: virtualkb.h
功能描述:
作 者:       周洋
版 本:       v1.0
完成日期:17/10/19
修改历史:
*****************************************************************************/
#ifndef VIRTUALKB_H
#define VIRTUALKB_H

#include <QtWidgets>
#include "../../menu/rmenus.h"  //! rbutton
#include "../../menu/wnd/rpopmenuchildwnd.h"
#include "../ime/ime.h"
#include "../ime/pinyininputmethod.h"

using namespace menu_res;

class KBSubElement
{
public:
    void load( const QString &dir,
               const r_meta::CMeta &meta );

public:
    QString mName;
    QString mType;
    QRect mGeo;
    QString mQss;
};

class VirtualKB_style : public RUI_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );
    void loadSubElement( KBSubElement *subE,
                         const r_meta::CMeta &meta,
                         const QString &dir,
                         int id );
public:
    int mCount;
    QRect mRect;
    QString mVocCh, mVocTch, mVocEn, mVocUser;
    int mInputW1, mInputW2;
    int mMaxLen;
    QList<KBSubElement*> mElements; /*!< 许多控件 */

    QMap<QChar,QChar> mCharMap;

    QString imgChn, imgEng, imgtChn, imgSpace, imgLeft, imgRight;
    QString imgNext,imgPre, imgBack, imgChars;

    QString imgChnDis, imgEngDis, imgtChnDis, imgSpaceDis;
    QString imgLeftDis, imgRightDis;
    QString imgNextDis,imgPreDis, imgBackDis, imgCharsDis;

    QString fontFamily;
    int fontSize;

    RBgFrame mBg;
    QRect frameRect;
    QString imeHead,imeBar,imeTail;
};

class VWidget
{
public:
    VWidget( QWidget *wig, bool bFocusEn = true );
public:
    void attach( QWidget *pWig, bool focusEn = true );
public:
    QWidget *mpWidget;
    bool mbFocusEn;
};

class VirtualKB : public RPopMenuChildWnd
{
    Q_OBJECT
public:
    enum ImeLanguage
    {
        lang_en,
        lang_ch,
        lang_tch,
        lang_max = lang_tch,
    };

private:
    static VirtualKB *_sysKB;
public:
    static VirtualKB *Create( QWidget *parent = 0 );
    static VirtualKB *object();
    static void Show( QWidget *pRefWidget,
                      const char *slot_completed,
                      const QString &caption="FileName",
                      const QString &defInput="",
                      const QPoint &shift=QPoint(0,0) );

public:
    explicit VirtualKB(QWidget *parent = 0);
    ~VirtualKB();
protected:
    virtual void paintEvent(QPaintEvent *event);
    void    showEvent(QShowEvent  */*event*/);

    void timerEvent(QTimerEvent *);
    int id1;
    int  time_count;

    int find(QString str,QString cha,int num);
protected:
    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );
    virtual void tuneZ( int keyCnt );

public:
    virtual void drop( int x, int y );

public:
    void setLanguage( ImeLanguage lang );
    ImeLanguage getLanguage();

    void setDefault( const QString &inputStr );

    void setMaxLength( int maxLen );

signals:
    void sig_langChanged();
    void sig_wordPageChanged();
    void sig_subenableChanged();

    void sig_completed( const QString &);
    void sig_editing( const QString & );
    void sig_canceled();

protected slots:
    void on_sig_btn_clicked( QWidget *obj );
    void on_sig_btn_clicked( int id );

    void on_sig_langChanged();
    void on_sig_wordPageChanged();
    void on_sig_subenableChanged();

    void on_input_changed( const QString &str );
    void on_pinyin_changed();
    void on_pinyin_cursor(int, int);

    void on_langClicked();
    void on_capsClicked();
    void on_charsClicked();
    void on_leftClicked();

    void on_rightClicked();
    void on_cancelClicked();
    void on_okClicked();
    void on_backspaceClicked();

    void on_spaceClicked();
    void on_pointClicked();
    void on_nextPageClicked();
    void on_prevPageClicked();

protected:
    void setupUi();
    void buildConnection();
    void startup();
    void translate();

    void attachImage();
    void addButtonImage( const QString &name,
                         const QString &imgPath,
                         const QString &imgDis );

    QPoint translatePoint( QRect &rect );

    void connectObj( const QString &name,
                     const char *slot );
    QWidget *findObject( const QString &name,
                         QList<VWidget*> *pWidgetList );
    RButton *findButton( const QString &name);
    QPushButton *findQButton( const QString &name );

    void setChar26Enable( const QString &name, bool bEn );
    void setKeyXEnable( const QString &name, bool bEn );

    void showSubWidget( const QString &namePattern,
                        int nameLen,
                        bool bVisible );

    void appendInput( const QString &str );
    void appendPinyin( const QString &pinyin );

    void setPinyinActive( bool act );

protected:
    void engProc( const QString &str );
    void chnProc( const QString &str );

private:
    void focusAbleQuery( QList<QWidget*> &wigs,
                         QList<int> &indexes );
    void focusNext( int cnt );
    void focusOn( const QString &name );

private:
    VirtualKB_style _style;

    //! 窗口上的控件
    QList<VWidget*> mWidgets;

    //! sub element
    QLabel *mCaption;
    QLineEdit *mEditInput;

    QLineEdit *mEditPinyin;

    RButton *mCaps, *mLang, *mChars;
    RButton *mOK, *mCancel;

    QPushButton **mWds;
    QPushButton *mNextPage, *mPrevPage;

    //! mapping
    QSignalMapper *mMapper;

    //! vocabulary
    CVocabulary *mVoc;
    CPinyinInputMethod *mVoc_Cn;
    CWordOption mWdOpt;

    //! attri
    ImeLanguage meLang;


    int mFocusNow;

    int mMaxLength;     /*!< 最大字符长度*/
};

#endif // VIRTUALKB_H
