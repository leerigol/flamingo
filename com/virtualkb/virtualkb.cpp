/*****************************************************************************
                                普源精电科技有限公司版权所有
******************************************************************************
源文件名: virtualkb.vpp
功能描述:启动输入法界面，输入选择基本操作
作 者:       周洋
版 本:       v1.0
完成日期:17/10/19
修改历史:
*****************************************************************************/
#include "../../menu/event/reventfilter.h"
#include "virtualkb.h"
#include "../ime/ime.h"
#include "../ime/pinyininputmethod.h"
#include "../ime/pinyin/pinyinime.h"
using namespace ime_pinyin;
#define pinyin_max_num 7


void KBSubElement::load( const QString &dir,
                         const r_meta::CMeta &meta )
{
    meta.getMetaVal( dir + "name", mName );
    meta.getMetaVal( dir + "type", mType );
    meta.getMetaVal( dir + "geo", mGeo );
    //! 其它元素含有qss
    if ( mType != "button" )
    {
        meta.getMetaVal( dir + "qss", mQss );
    }
}

void VirtualKB_style::load(const QString &path,
                           const r_meta::CMeta &meta )
{
    KBSubElement *subElement;
    int i;

    meta.getMetaVal( path + "count", mCount);
    meta.getMetaVal( path + "geo", mRect );
    meta.getMetaVal( path + "voc_tch", mVocTch );

    //! 根据资源路径推断出全路径
    mVocTch = sysGetResourcePath() + mVocTch;
    mVocEn  = sysGetResourcePath() + "vocabulary/pinyinEx.ini";
    mVocCh  = sysGetResourcePath() + "vocabulary/dict_pinyin.txt";

    QString strCharMap;
    meta.getMetaVal( path + "charmap", strCharMap );
    strCharMap = strCharMap.trimmed();
    Q_ASSERT( ( strCharMap.size() % 2) == 0 );
    for ( i = 0; i < strCharMap.size(); i+=2 )
    {
        mCharMap.insert( strCharMap.at(i), strCharMap.at(i+1) );
    }

    //! 在中英文下输入框的宽度不同
    meta.getMetaVal( path + "input/w1", mInputW1 );
    meta.getMetaVal( path + "input/w2", mInputW2 );
    meta.getMetaVal( path + "max_length", mMaxLen );
    //! 图片
    QString refPath = path + "image_enable/";
    meta.getMetaVal( refPath + "space", imgSpace );
    meta.getMetaVal( refPath + "eng", imgEng );
    meta.getMetaVal( refPath + "chn", imgChn );
    meta.getMetaVal( refPath + "tchn", imgtChn );
    meta.getMetaVal( refPath + "left", imgLeft );

    meta.getMetaVal( refPath + "right", imgRight );
    meta.getMetaVal( refPath + "next", imgNext );
    meta.getMetaVal( refPath + "pre", imgPre );
    meta.getMetaVal( refPath + "back", imgBack );

    meta.getMetaVal( refPath + "chars", imgChars );

    //! disable
    refPath = path + "image_disable/";
    meta.getMetaVal( refPath + "space", imgSpaceDis );
    meta.getMetaVal( refPath + "eng", imgEngDis );
    meta.getMetaVal( refPath + "chn", imgChnDis );
    meta.getMetaVal( refPath + "tchn", imgtChnDis );
    meta.getMetaVal( refPath + "left", imgLeftDis );

    meta.getMetaVal( refPath + "right", imgRightDis );
    meta.getMetaVal( refPath + "next", imgNextDis );
    meta.getMetaVal( refPath + "pre", imgPreDis );
    meta.getMetaVal( refPath + "back", imgBackDis );

    meta.getMetaVal( refPath + "chars", imgCharsDis );

    //! font
    meta.getMetaVal( path + "font/family", fontFamily );
    meta.getMetaVal( path + "font/size", fontSize );

    //! 背景
    mBg.load( path + "frame/", meta );
    meta.getMetaVal( path + "frame/geo", frameRect );

    meta.getMetaVal( path + "frame/imehead", imeHead );
    meta.getMetaVal( path + "frame/imebar", imeBar );
    meta.getMetaVal( path + "frame/imetail", imeTail );

    //! \note 从1开始
    for ( i = 1; i <= mCount; i++ )
    {
        subElement = new KBSubElement();
        Q_ASSERT( NULL != subElement );

        loadSubElement( subElement,
                        meta,
                        path,
                        i );

        mElements.append( subElement );
    }
}

void VirtualKB_style::loadSubElement( KBSubElement *subE,
                                      const r_meta::CMeta &meta,
                                      const QString &dir,
                                      int id )
{
    Q_ASSERT( NULL != subE );

    QString str;
    str = QString("%1sub%2/").arg(dir).arg(id);

    subE->load( str, meta );

}

VWidget::VWidget( QWidget *wig,
                  bool bFocusEn )
{
    mpWidget = wig;
    mbFocusEn = bFocusEn;
}
void VWidget::attach( QWidget *pWig, bool focusEn )
{
    mpWidget = pWig;
    mbFocusEn = focusEn;
}

VirtualKB *VirtualKB::_sysKB = NULL;
VirtualKB *VirtualKB::Create( QWidget *parent )
{
    if ( VirtualKB::_sysKB != NULL )
    { return VirtualKB::_sysKB; }

    VirtualKB::_sysKB = new VirtualKB( parent );
    Q_ASSERT( NULL != VirtualKB::_sysKB );

    return VirtualKB::_sysKB;
}

VirtualKB *VirtualKB::object()
{
    Q_ASSERT( VirtualKB::_sysKB != NULL );

    return VirtualKB::_sysKB;
}

void VirtualKB::Show( QWidget *pRefWidget,
                      const char *slot_completed,
                      const QString &/*caption*/,
                      const QString &defInput,
                      const QPoint &shift )
{
    Q_ASSERT( pRefWidget != NULL );
    Q_ASSERT( slot_completed != NULL );

    if( VirtualKB::_sysKB == NULL )
    { Create(); }

    VirtualKB *pKB = VirtualKB::_sysKB;

    pKB->disconnect( SIGNAL(sig_completed(QString)) );

    connect( pKB, SIGNAL(sig_completed(QString)),
             pRefWidget, slot_completed );

    pKB->mEditInput->setPlaceholderText( defInput );
    pKB->mEditInput->setText( defInput );
    pKB->mEditPinyin->clear();

    //! 复位键盘设置
    pKB->mEditInput->setMaxLength( pKB->_style.mMaxLen );
    pKB->setLinkKey( -1 );

    //! 目标的绝对位置，左上角
    QPoint pt;
    QRect dstPhyRect;
    pt = pRefWidget->mapToGlobal( QPoint(0,0) );
    pt += shift;

    dstPhyRect = pRefWidget->rect();
    dstPhyRect.setTopLeft( pt );

    //! 置
    pt = pKB->translatePoint( dstPhyRect );

    //! 父窗口的绝对位置
    QWidget *wigParent = (QWidget*)pKB->parent();
    //! window
    if ( NULL == wigParent )
    {
        pKB->move( pt );
        pKB->show();

        pKB->focusOn( "lang" );
        return;
    }

    //! 如果父窗口是window，则使用全局坐标
    if ( wigParent->isWindow() )
    {
    }
    //! 子部件
    else
    {
        QPoint ptParent;
        ptParent = wigParent->mapToGlobal( QPoint(0,0) );
        pt -= ptParent;
    }

    pKB->move( pt );
    pKB->show();

    pKB->focusOn( QLatin1Literal("ok") );
}

VirtualKB::VirtualKB(QWidget *parent) : RPopMenuChildWnd(parent)
{   
    //! init
    mCaption = NULL;
    mEditInput = NULL;

    mEditPinyin = NULL;

    meLang = lang_en;

    mFocusNow = 0;

    //! words
    mWds = new QPushButton*[ CWordOption::_pageCount] ;
    Q_ASSERT( NULL != mWds );

    //! map
    mMapper = new QSignalMapper(this);
    Q_ASSERT( NULL != mMapper );

    //! voc
    mVoc = new CVocabulary();
    Q_ASSERT( NULL != mVoc );

    mVoc_Cn =  new CPinyinInputMethod();
    Q_ASSERT( NULL != mVoc_Cn );

    //! load
    _style.loadResource("ui/virtual_kb/");
    resize( _style.mRect.width(),
            _style.mRect.height() );

    setupUi();

    buildConnection();

    startup();

    //! key event
    addTuneKey();
}

VirtualKB::~VirtualKB()
{
    delete []mWds;

    KBSubElement *pEle;
    foreach( pEle, _style.mElements )
    {
        Q_ASSERT( NULL != pEle );
        delete pEle;
    }

    VWidget *pVWig;
    foreach( pVWig, mWidgets )
    {
        Q_ASSERT( NULL != pVWig );
        delete pVWig;
    }

    delete mVoc;
    delete mVoc_Cn;
}

void VirtualKB::paintEvent(QPaintEvent */*event*/ )
{
    QPainter painter(this);

    QRect rect = _style.frameRect;
    RBgFrame bgFrame = _style.mBg;

    painter.fillRect(0,0,this->rect().width(),this->height()-rect.height(),Qt::black);
    painter.fillRect(0,2,215,32,QColor(34,34,34,255));

    QImage myImg;
    myImg.load(_style.imeHead);
    painter.drawImage(215, 0, myImg);

    myImg.load(_style.imeBar);
    for(int i=0;i<336; i++)
        painter.drawImage(248+i, 0, myImg);

    myImg.load(_style.imeTail);
    painter.drawImage(584, 0, myImg);

    //! bg
    QRect bgRect;
    bgRect = _style.frameRect;
    bgRect.adjust(4,4,-5,-5);
    painter.fillRect( bgRect, bgFrame.bgColor );

    QImage img;
    int start, end;

    //! ltc
    img.load( bgFrame.ltc );
    painter.drawImage( rect.left(), rect.top(), img );
    start = img.width() + rect.left();

    //! rtc
    img.load( bgFrame.rtc );
    end = rect.right() - img.width();
    painter.drawImage( end, rect.top(), img );

    //! tb
    img.load( bgFrame.tb );
    RControlStyle::fillImage( painter,
                              start, rect.top(), end - start, img.height(),
                              img );

    //! ldc
    img.load( bgFrame.ldc );
    painter.drawImage( rect.left(), rect.bottom() - img.height(), img );
    end = rect.bottom() - img.height();

    //! lb
    img.load( bgFrame.ltc );
    start = rect.top() + img.height();

    img.load( bgFrame.lb );
    RControlStyle::fillImage( painter, rect.left(), start, img.width(), end - start, img );

    //! rdc
    img.load( bgFrame.rdc );
    end = rect.right() - img.width();
    painter.drawImage( end, rect.bottom() - img.height(), img );

    img.load( bgFrame.ldc );
    start = rect.left() + img.width();
    //! db
    img.load( bgFrame.db );
    RControlStyle::fillImage( painter, start, rect.bottom()-img.height(),
                        end - start, img.height(),
                        img );

    //! rtc
    img.load( bgFrame.rtc );
    start = rect.top() + img.height();

    //! rb
    img.load( bgFrame.rdc );
    end = rect.bottom() - img.height();

    img.load( bgFrame.rb );
    RControlStyle::fillImage( painter, rect.right() - img.width(), start,
                        img.width(), end - start,
                        img );
}

void VirtualKB::showEvent(QShowEvent  * event)
{
    RPopMenuChildWnd::showEvent(event);
}
void VirtualKB::tuneInc( int keyCnt )
{
    focusNext( keyCnt );
}
void VirtualKB::tuneDec( int keyCnt )
{
    focusNext( -keyCnt );
}
void VirtualKB::tuneZ( int /*keyCnt*/ )
{
    Q_ASSERT( mWidgets[mFocusNow] !=  NULL );

    focusNext( 0 );

    QWidget *pWig;
    pWig = mWidgets[mFocusNow]->mpWidget;
    Q_ASSERT( NULL != pWig );

    if ( !pWig->isEnabled() ) return;

    QPushButton *pBtn;
    pBtn = dynamic_cast<QPushButton*>( pWig );
    if ( NULL == pBtn ) return;

    pBtn->click();
}

void VirtualKB::drop( int x, int y )
{
    move( x, y );
}

void VirtualKB::setLanguage( VirtualKB::ImeLanguage lang )
{
    if ( meLang != lang )
    {
        meLang = lang;
        emit sig_langChanged();
    }
}
VirtualKB::ImeLanguage VirtualKB::getLanguage()
{
    return meLang;
}

void VirtualKB::setDefault( const QString &inputStr )
{
    mEditInput->setText(inputStr);
}

void VirtualKB::setMaxLength( int maxLen )
{
    mEditInput->setMaxLength(maxLen);
}

void VirtualKB::on_sig_btn_clicked( QWidget *obj )
{
    Q_ASSERT( NULL != obj );
    RButton *pBtn;

    pBtn = dynamic_cast<RButton*>( obj );
    if ( NULL == pBtn )
    { return; }

    chnProc( pBtn->text());

    //! focus changed
    focusOn( pBtn->objectName() );
}

void VirtualKB::on_sig_btn_clicked( int id )
{
    Q_ASSERT( id >=1 && id <= CWordOption::_pageCount );

    QString str = mWds[id-1]->text();

    mVoc->select( str );

    appendInput( str );

    //!选中汉字个数
    int hz_num =   str.length();
    QString rawStr;

    rawStr = mEditPinyin->text();

    if (rawStr.size() && find(rawStr,"'",hz_num) !=  -1)
    {
        rawStr.remove(0,   find(rawStr,"'",hz_num) + 1);
        mEditPinyin->setText( rawStr );
    }
    else
    {
         mEditPinyin->clear();
    }
    hz_num = 0;

    //! 选中ok
    focusOn( QLatin1Literal("ok") );
}

/*!
 * \brief VirtualKB::on_sig_langChanged
 * - 语言切换后可视项目会发生变化
 */
void VirtualKB::on_sig_langChanged()
{
    do
    {
        QRect geo;

        mEditPinyin->clear();

        mCaps->setEnabled( false );
        mCaps->setChecked( false );

        geo = mEditInput->geometry();
        geo.setWidth( _style.mInputW1 );
        mEditInput->setGeometry( geo );

        mOK->setText( tr("OK") );
        mCancel->setText( tr("Cancel") );

        int load_ret = 0;
        //! vocabulary
        if ( meLang == lang_ch )
        {
           load_ret = mVoc_Cn->load(
                              _style.mVocEn,
                              _style.mVocCh,
                              _style.mVocUser,
                              CPinyinInputMethod::ImChn);
           Q_ASSERT(  load_ret == 0 );

           mLang->selectImage( 1 );
        }
        else  if( meLang == lang_tch)
        {
          //  mVoc->load( _style.mVocTch );
            load_ret = mVoc_Cn->load(
                               _style.mVocEn,
                               _style.mVocCh,
                               _style.mVocUser,
                               CPinyinInputMethod::ImChn);
            Q_ASSERT(  load_ret == 0 );
            mLang->selectImage( 2 );
        }
        else
        {
            load_ret = mVoc_Cn->load(
                              _style.mVocEn,
                              _style.mVocCh,
                              _style.mVocUser,
                              CPinyinInputMethod::ImEn );
            Q_ASSERT(  load_ret==0 );

            geo = mEditInput->geometry();
            geo.setWidth( _style.mInputW2 );
            mEditInput->setGeometry( geo );

            mCaps->setEnabled( true );
            mCaps->setChecked( false );
            mLang->selectImage( 0 );
         }

    }while(0);

    emit sig_subenableChanged();
}

void VirtualKB::on_sig_wordPageChanged()
{
    //! current page
    CWordPage wdPage = mWdOpt.getPage();

    QStringList &words = wdPage.getView();

    int i;
    for ( i = 0; i < words.size(); i++ )
    {
        mWds[i]->setText( words[i] );
        mWds[i]->setEnabled(true);
        mWds[i]->setVisible( true );
    }

    for ( ; i < CWordOption::_pageCount; i++ )
    {
        mWds[i]->setVisible( true );
        mWds[i]->setText(" ");
        mWds[i]->setEnabled(false);
    }

    mNextPage->setVisible( true );
    mPrevPage->setVisible( true );
}

void VirtualKB::on_sig_subenableChanged()
{
    QList<QWidget*> focusAbleList;
    QList<int> indexes;

    focusAbleQuery( focusAbleList, indexes );

    //! now valid
    if ( focusAbleList.contains( mWidgets[mFocusNow]->mpWidget ) )
    {}
    else if ( focusAbleList.size() > 0 )
    //! move to next
    {
        focusAbleList[0]->setFocus();
        //! update focusNow
        mFocusNow = indexes[0];
    }
    else
    {}
}

void VirtualKB::on_input_changed( const QString& str )
{
    //! valid by str || placeholder
    mOK->setEnabled( str.length() > 0
                     || mEditInput->placeholderText().length() > 0 );

    emit sig_editing( str );

    emit sig_subenableChanged();
}

//! find the last cha
int VirtualKB::find(QString str,QString cha,int num)
{
    int x = -1;
    x = str.indexOf(cha);
    for( int i=0;i<num -1 ;i++)
    {
        x=str.indexOf(cha,x+1);
        if(x == -1)
        {
            break;
        }
    }
    return x;
}

void VirtualKB::timerEvent(QTimerEvent */*event*/)
{
    time_count++;
}

void VirtualKB::on_pinyin_changed()
{
    QString str = mEditPinyin->text();

    CPinyinInputMethod::InputLang inputLang;

    if ( meLang == lang_ch)
    {
         inputLang = CPinyinInputMethod::ImChn;
         //! 只选取字符串限定的拼音个数长度
         if( -1 != find(str,"'",pinyin_max_num) )
         {
            str = str.mid(0,find(str,"'",pinyin_max_num));
         }
    }
    else if(meLang == lang_en)
    {
         inputLang = CPinyinInputMethod::ImEn;
    }
    else
    {
         inputLang = CPinyinInputMethod::ImTChn;

         if( -1 != find(str,"'",pinyin_max_num) )
         {
            str = str.mid(0,find(str,"'",pinyin_max_num));
         }
    }

    QString py;
    QStringList strList = mVoc->ime( str,
                                      py,
                                      inputLang,
                                      mVoc_Cn);
    //! for formated pinyin
    if( inputLang == CPinyinInputMethod::ImChn
         || inputLang == CPinyinInputMethod::ImTChn)
    { mEditPinyin->setText(py); }

    mWdOpt.setText(strList);

    if ( str.length() == 0 )
    {
        setChar26Enable( QLatin1Literal(""), false);
        setPinyinActive( false );
    }
    else
    {
        setPinyinActive( true );
    }

    mEditPinyin->setVisible( str.length() > 0 );
    emit sig_wordPageChanged();
}

void VirtualKB::on_pinyin_cursor(int, int)
{
    mEditPinyin->setCursorPosition(mEditPinyin->text().size());
}

void VirtualKB::on_langClicked()
{
    int langId;

    //! lang change
    langId = (int)meLang;
    langId++;
    if ( langId > VirtualKB::lang_max )
    { langId = 0;}
    else
    {}

    if( meLang == VirtualKB::lang_en && langId != (int)meLang )
    {
        mCaps->setChecked(false);
        translate();

        meLang = (ImeLanguage)langId;
        emit sig_langChanged();
    }
    else if ( langId != (int)meLang )
    {
        meLang = (ImeLanguage)langId;
        emit sig_langChanged();
    }

    focusOn( QLatin1Literal("lang") );
}
void VirtualKB::on_capsClicked()
{
    focusOn( QLatin1Literal("Caps") );
    translate();
}
void VirtualKB::on_charsClicked()
{
    focusOn( QLatin1Literal("?123") );

    if ( mChars->isChecked() )
    {
        mChars->setText(tr("abc"));

        mCaps->setEnabled( false );
        mCaps->setChecked( false );

        mLang->setEnabled( false );
        mLang->setChecked( false );
    }
    else
    {
        mChars->setText(tr("?123"));
        if ( meLang == lang_en )
        {
            mCaps->setEnabled( true );
        }
        mLang->setEnabled( true );
    }

    translate();
}
void VirtualKB::on_leftClicked()
{
    mEditInput->cursorWordBackward(false);
    focusOn( QLatin1Literal("left") );
}

void VirtualKB::on_rightClicked()
{
    mEditInput->cursorWordForward(false);
    focusOn( QLatin1Literal("right") );
}
void VirtualKB::on_cancelClicked()
{
    hide();
}
void VirtualKB::on_okClicked()
{
    //! by input
    if ( mEditInput->text().length() > 0 )
    {
        emit sig_completed( mEditInput->text());
    }
    //! by placeholder
    else if ( mEditInput->placeholderText().length() > 0 )
    {
        emit sig_completed( mEditInput->placeholderText() );
    }
    else
    {}

    hide();

    if ( meLang == lang_tch )
    {
        mVoc->commit();
    }
}
void VirtualKB::on_backspaceClicked()
{
    //! pinyin changed
    if ( mEditPinyin->isVisible()
         && mEditPinyin->text().size() > 0 )
    {
        mEditPinyin->backspace();
        if(mEditPinyin->text().endsWith("'", Qt::CaseInsensitive))
        {  mEditPinyin->backspace(); }
    }
    else
    {
        mEditInput->backspace();
    }

    focusOn( QLatin1Literal("backspace") );
}
void VirtualKB::on_spaceClicked()
{
    appendInput( QLatin1Literal(" ") );
    focusOn( QLatin1Literal("space") );
}

void VirtualKB::on_pointClicked()
{
    appendInput( QLatin1Literal(".") );
    focusOn( QLatin1Literal(".") );
}

void VirtualKB::on_nextPageClicked()
{
    mWdOpt.moveNext();

    emit sig_wordPageChanged();
}
void VirtualKB::on_prevPageClicked()
{
    mWdOpt.movePrevious();

    emit sig_wordPageChanged();
}

void VirtualKB::setupUi()
{
    //! create items
    RButton *pBtn;
    QPushButton *pQBtn;
    QLabel *pLabel;
    QLineEdit *pLineEdit;

    KBSubElement *pEle;

    QString typeButton="button";
    QString typeEdit="edit";
    QString typeLabel="label";
    QString typeQButton="qbutton";

    foreach( pEle, _style.mElements )
    {
        Q_ASSERT( NULL != pEle );

        if ( pEle->mType == typeButton )
        {
            pBtn = new RButton(this);
            Q_ASSERT( NULL != pBtn );

            pBtn->setObjectName( pEle->mName );
            pBtn->setGeometry( pEle->mGeo );
            pBtn->setText( pEle->mName );

            pBtn->setFont( _style.fontFamily, _style.fontSize );

            mWidgets.append( new VWidget( pBtn ) );
        }
        else if ( pEle->mType == typeEdit )
        {
            pLineEdit = new QLineEdit(this);
            Q_ASSERT( NULL != pLineEdit );
            pLineEdit->setObjectName( pEle->mName );
            pLineEdit->setGeometry( pEle->mGeo );

            if ( pEle->mQss.length() > 0 )
            { pLineEdit->setStyleSheet( pEle->mQss );}

            mWidgets.append( new VWidget( pLineEdit, false ) );
        }
        else if ( pEle->mType == typeLabel )
        {
            pLabel = new QLabel(this);
            Q_ASSERT( NULL != pLabel );
            pLabel->setObjectName( pEle->mName );
            pLabel->setGeometry( pEle->mGeo );

            if ( pEle->mQss.length() > 0 )
            { pLabel->setStyleSheet( pEle->mQss );}

            mWidgets.append( new VWidget( pLabel,false ) );
        }
        else if ( pEle->mType == typeQButton )
        {
            pQBtn = new QPushButton(this);
            Q_ASSERT( NULL != pQBtn );
            pQBtn->setObjectName( pEle->mName );
            pQBtn->setGeometry( pEle->mGeo );
            pQBtn->setText( pEle->mName );

            if ( pEle->mQss.length() > 0 )
            { pQBtn->setStyleSheet( pEle->mQss); }

            mWidgets.append( new VWidget( pQBtn ) );
        }
        else
        {   Q_ASSERT(false); }
    }

    //! dispatch items
    QWidget *pWidget;
    RButton *button;
    pWidget = findObject( "caption", &mWidgets );
    Q_ASSERT( NULL != pWidget );
    mCaption = dynamic_cast<QLabel*>( pWidget );
    mCaption->hide();

    pWidget = findObject( "left", &mWidgets );
    Q_ASSERT( NULL != pWidget );
    button = dynamic_cast<RButton*>( pWidget );
    button->hide();

    pWidget = findObject( "right", &mWidgets );
    Q_ASSERT( NULL != pWidget );
    button = dynamic_cast<RButton*>( pWidget );
    button->hide();

    pWidget = findObject( "cancel", &mWidgets );
    Q_ASSERT( NULL != pWidget );
    button = dynamic_cast<RButton*>( pWidget );
    button->hide();

    pWidget = findObject( "input", &mWidgets );
    Q_ASSERT( NULL != pWidget );
    mEditInput = dynamic_cast<QLineEdit*>( pWidget );

    pWidget = findObject( "pinyin", &mWidgets );
    Q_ASSERT( NULL != pWidget );
    mEditPinyin = dynamic_cast<QLineEdit*>( pWidget );
    connect(mEditPinyin,SIGNAL(cursorPositionChanged(int,int)),
            this,SLOT(on_pinyin_cursor(int,int)));
    mEditPinyin->setReadOnly(true);

    mCaps = findButton("Caps");
    mLang = findButton("lang");
    mChars = findButton("?123");
    mOK = findButton("ok");

    mCancel = findButton("cancel");

    //! wd button
    for ( int i = 1; i <= CWordOption::_pageCount; i++ )
    {
        mWds[i-1] = findQButton( QString("han%1").arg(i) );
    }

    mNextPage = findQButton("pnext");
    mPrevPage = findQButton("pprev");

    //! images
    attachImage();
}

void VirtualKB::buildConnection()
{
    QPushButton *pBtn;
    VWidget *pWidget;
    QString strObjName;
    QString strId;

    bool bOK;
    int id;

    //! char s
    Q_ASSERT( NULL != mMapper );
    foreach( pWidget, mWidgets )
    {
        Q_ASSERT( NULL != pWidget );
        Q_ASSERT( NULL != pWidget->mpWidget );

        pBtn = dynamic_cast<QPushButton*>(pWidget->mpWidget);
        if ( NULL != pBtn )
        {
            strObjName = pBtn->objectName();
            if ( strObjName.length() == 1 &&
                 strObjName >= "a" && strObjName <= "z" )
            {
                connect( pBtn, SIGNAL(pressed()),
                         mMapper, SLOT(map()) );
                mMapper->setMapping( pBtn, pBtn );
            }

            //! 汉字
            if ( strObjName.length() == 4 &&
                 strObjName.startsWith("han") )
            {
                strId = strObjName.right(1);
                id = strId.toInt( &bOK );
                Q_ASSERT( bOK );

                connect( pBtn, SIGNAL(clicked(bool)),
                         mMapper, SLOT(map()) );
                mMapper->setMapping( pBtn, id );
            }
        }
    }

    //! button group
    connect( mMapper, SIGNAL(mapped(QWidget*)),
             this, SLOT(on_sig_btn_clicked(QWidget*) ) );

    connect( mMapper, SIGNAL(mapped(int)),
             this, SLOT(on_sig_btn_clicked(int)) );

    //! subelement
    connectObj( "Caps", SLOT(on_capsClicked()) );
    connectObj( "lang", SLOT(on_langClicked()) );
    connectObj( "?123", SLOT(on_charsClicked()) );
    connectObj( "left", SLOT(on_leftClicked()) );

    connectObj( "right", SLOT(on_rightClicked()) );
    connectObj( "cancel", SLOT(on_cancelClicked()) );
    connectObj( "ok", SLOT(on_okClicked()) );
    connectObj( "backspace", SLOT(on_backspaceClicked()) );

    connectObj( "space", SLOT(on_spaceClicked()) );
    connectObj( ".", SLOT(on_pointClicked()) );

    //! self signal-slot
    connect( this, SIGNAL(sig_langChanged()),
             this, SLOT(on_sig_langChanged()) );

    connect( mEditInput, SIGNAL(textChanged( const QString&)),
             this, SLOT(on_input_changed( const QString&)) );

    connect( mEditPinyin, SIGNAL(textChanged( const QString&)),
           this, SLOT(on_pinyin_changed()) );

    connect( mNextPage, SIGNAL(clicked(bool)),
             this, SLOT(on_nextPageClicked()) );
    connect( mPrevPage, SIGNAL(clicked(bool)),
             this, SLOT(on_prevPageClicked()) );

    connect( this, SIGNAL(sig_wordPageChanged()),
             this, SLOT(on_sig_wordPageChanged()) );

    connect( this, SIGNAL(sig_subenableChanged()),
             this, SLOT(on_sig_subenableChanged()) );
}

void VirtualKB::startup()
{
    //! checkable
    mCaps->setCheckable( true );
    mChars->setCheckable( true );

    //! signals
    emit sig_langChanged();

    translate();

    QHBoxLayout *hl2 = new QHBoxLayout;
    hl2->setSpacing(0);
    hl2->setMargin(0);
    findQButton("pnext")->setText(">>");
    findQButton("pprev")->setText("<<");
    findQButton("han1")->setText(" ");
    hl2->addWidget(findQButton("han1"));
    findQButton("han2")->setText(" ");
    hl2->addWidget(findQButton("han2"));
    findQButton("han3")->setText(" ");
    hl2->addWidget(findQButton("han3"));
    findQButton("han4")->setText(" ");
    hl2->addWidget(findQButton("han4"));
    findQButton("han5")->setText(" ");
    hl2->addWidget(findQButton("han5"));

    //setLayout(hl2);
    mEditInput->clear();
}

void VirtualKB::translate()
{
    RButton *pBtn;
    VWidget *pWidget;
    QString objName;

    //! button
    foreach( pWidget, mWidgets )
    {
        Q_ASSERT( NULL != pWidget );
        Q_ASSERT( NULL != pWidget->mpWidget );
        pBtn = dynamic_cast<RButton*>( pWidget->mpWidget );
        if ( NULL != pBtn )
        {
            objName = pBtn->objectName();
//            qDebug()<<__FUNCTION__<<__LINE__<<objName;
            //! 符号
            if ( mChars->isChecked()
                 && objName.length() == 1
                 && _style.mCharMap.contains(objName.at(0)) )
            {
                pBtn->setText( QString(_style.mCharMap[objName.at(0)]) );
            }
            else
            {
                //! 字母
                if ( objName.length() == 1 &&
                     objName >="a" && objName <= "z" )
                {
                    if ( mCaps->isChecked() )
                    { objName = objName.toUpper(); }
                    else
                    {}

                    pBtn->setText( objName );
                }
                else
                {}
            }
        }
    }
}

void VirtualKB::attachImage()
{
    addButtonImage( "lang", _style.imgEng, _style.imgEngDis );
    addButtonImage( "lang", _style.imgChn, _style.imgChnDis );
    addButtonImage( "lang", _style.imgtChn, _style.imgtChnDis );

    addButtonImage( "space", _style.imgSpace, _style.imgSpaceDis );
    addButtonImage( "backspace", _style.imgBack, _style.imgBackDis );
    addButtonImage( "left", _style.imgLeft, _style.imgLeftDis );
    addButtonImage( "right", _style.imgRight, _style.imgRightDis );
}

void VirtualKB::addButtonImage( const QString &name,
                     const QString &imgPath,
                     const QString &imgDis )
{
    RButton *pBtn;

    pBtn = findButton( name );
    Q_ASSERT( NULL != pBtn );

    //! 有图片就没有文字了
    pBtn->addImage( imgPath, imgDis );
    pBtn->setText("");
}

QPoint VirtualKB::translatePoint( QRect &aimRect )
{
    QRect selfRect = rect();

    int x, y;
    if ( aimRect.left() > selfRect.width() )
    {
        x = aimRect.left() - selfRect.width();
    }
    else
    {
        x = 0;
    }

    if ( aimRect.bottom() > selfRect.height() )
    {
        y = aimRect.bottom() - selfRect.height();
    }
    else
    {
        y = aimRect.top();
    }

    return QPoint( x, y );
}

void VirtualKB::connectObj( const QString &name,
                 const char *slot )
{
    RButton *pBtn;
    VWidget *pWidget;

    foreach( pWidget, mWidgets )
    {
        Q_ASSERT( NULL != pWidget );
        Q_ASSERT( NULL != pWidget->mpWidget );
        pBtn = dynamic_cast<RButton*>(pWidget->mpWidget);
        if ( NULL != pBtn )
        {
            if ( pBtn->objectName() == name )
            {
                connect( pBtn, SIGNAL(clicked(bool)),
                         this, slot );
                return;
            }
        }
    }
}

QWidget *VirtualKB::findObject( const QString &name,
                     QList<VWidget*> *pWidgetList )
{
    VWidget *pWidget;

    Q_ASSERT( NULL != pWidgetList );
    foreach( pWidget, *pWidgetList )
    {
        Q_ASSERT( NULL != pWidget );
        Q_ASSERT( NULL != pWidget->mpWidget );
        if ( pWidget->mpWidget->objectName() == name )
        { return pWidget->mpWidget; }
    }

    return NULL;
}

RButton *VirtualKB::findButton( const QString &name )
{
    QWidget *pWidget;
    RButton *pBtn;

    pWidget = findObject( name, &mWidgets );
    Q_ASSERT( NULL != pWidget );

    pBtn = dynamic_cast<RButton*>( pWidget );
    Q_ASSERT( NULL != pBtn );

    return pBtn;
}

QPushButton *VirtualKB::findQButton( const QString &name )
{
    QWidget *pWidget;
    QPushButton *pBtn;
//qDebug()<<__FUNCTION__<<__LINE__<<name;
    pWidget = findObject( name, &mWidgets );
    Q_ASSERT( NULL != pWidget );

    pBtn = dynamic_cast<QPushButton*>( pWidget );
    Q_ASSERT( NULL != pBtn );

    return pBtn;
}

void VirtualKB::showSubWidget( const QString &namePattern,
                               int nameLen,
                               bool bVisible )
{
    VWidget *pWidget;
    QString objName;
    foreach( pWidget, mWidgets )
    {
        Q_ASSERT( NULL != pWidget );
        Q_ASSERT( NULL != pWidget->mpWidget );
        objName = pWidget->mpWidget->objectName();  //?

        if ( objName.length() == nameLen &&
             objName.startsWith(namePattern) )
        {
            pWidget->mpWidget->setVisible( bVisible );
        }
    }
}

void VirtualKB::setChar26Enable( const QString &name, bool bEn )
{
    QString objName;
    VWidget *pWidget;
    foreach( pWidget, mWidgets )
    {
        Q_ASSERT( NULL != pWidget );
        Q_ASSERT( NULL != pWidget->mpWidget );
        objName = pWidget->mpWidget->objectName();

        if ( objName.length() == 1 &&
             objName >= "a" && objName <= "z"
              )
        {
            if ( name.contains(objName) )
            { pWidget->mpWidget->setEnabled( bEn ); }
            else
            { pWidget->mpWidget->setEnabled( !bEn ); }
        }
        else
        {
        }
    }
}

void VirtualKB::setKeyXEnable( const QString &name, bool bEn )
{
    QString objName;
    VWidget *pWidget;
    foreach( pWidget, mWidgets )
    {
        Q_ASSERT( NULL != pWidget );
        Q_ASSERT( NULL != pWidget->mpWidget );
        objName = pWidget->mpWidget->objectName();

        if ( name == objName )
        {
            pWidget->mpWidget->setEnabled( bEn );
            return;
        }
    }
}

//! \todo 怎样插入到光标闪烁位置
void VirtualKB::appendInput( const QString &str )
{  
    QString temp = mEditInput->text() + str;
    int size = sysGetNormalSpace(temp);

    //! 已经是最大长度
    if ( size > mEditInput->maxLength() )
    {
        sysShowErr( ERR_IME_LENGTH_OVER );
        return;
    }

    mEditInput->insert( str );
}

void VirtualKB::appendPinyin( const QString &pinyin )
{
    QString rawStr;

    rawStr = mEditPinyin->text();
    rawStr.append( pinyin );

    mEditPinyin->setText( rawStr );
}

void VirtualKB::setPinyinActive( bool act )
{
    setKeyXEnable("lang", !act );
    setKeyXEnable("?123", !act );
    setKeyXEnable("space", !act );
    setKeyXEnable("left", !act );

    setKeyXEnable("right", !act );
    setKeyXEnable("dot", !act );
    setKeyXEnable("cancel", !act );
    setKeyXEnable("ok", !act );

    setKeyXEnable(".", !act );

    emit sig_subenableChanged();
}

void VirtualKB::engProc( const QString &str )
{
    appendInput( str );
}

void VirtualKB::chnProc( const QString &str )
{
    if ( mChars->isChecked() )
    { appendInput( str ); }
    else
    { appendPinyin(str); }
}

void VirtualKB::focusAbleQuery( QList<QWidget*> &wigs,
                                QList<int> &indexes )
{
    VWidget *pWig;
    QWidget *pQWig;
    int id;

    id = 0;
    foreach( pWig, mWidgets )
    {
        Q_ASSERT( pWig != NULL );
        pQWig = pWig->mpWidget;
        Q_ASSERT( pQWig != NULL );

        if ( pWig->mbFocusEn
             && pQWig->isVisible()
             && pQWig->isEnabled() )
        {
            wigs.append(pQWig);
            indexes.append( id );
        }

        id++;
    }
}

void VirtualKB::focusNext( int stepCnt )
{
    int sum;
    int next;
    QList<QWidget*> wigs;
    QList<int> indexes;

    focusAbleQuery( wigs, indexes );
    sum = wigs.size();

    //! no change
    if ( sum == 0 )
    {
        return;
    }

    int seq, id;
    seq = 0;
    foreach( id, indexes )
    {
        if ( id == mFocusNow )
        { break; }

        seq++;
    }

    //! next item
    next = (seq + stepCnt ) % sum;
    if ( next < 0 ) next += sum;

    //! select focus
    wigs[ next ]->setFocus();
    mFocusNow = indexes[ next ];
}

void VirtualKB::focusOn( const QString &name )
{
    VWidget *pVWig;
    QWidget *pWig;
    int id = 0;

    foreach( pVWig, mWidgets )
    {
        Q_ASSERT( NULL != pVWig );
        pWig = pVWig->mpWidget;
        Q_ASSERT( NULL != pWig );

        if ( pWig->objectName() == name )
        {
            pWig->setFocus();
            mFocusNow = id;
            break;
        }

        id++;
    }
}


