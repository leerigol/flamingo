
TEMPLATE = lib
TARGET = ../../lib$$(PLATFORM)/usbtmc488interface
CONFIG += staticlib
INCLUDEPATH += .

# Input


LIBS += -L../../lib$$(PLATFORM)/scpi
unix|win32: LIBS += -lscpiparse

#库路劲
LIBS += -L../../lib$$(PLATFORM)/scpi
#库
LIBS += -lusbtmc

HEADERS += \
    usbtmc488interface.h \
    usbtmc/usbtmc/usb488ioControl.h

SOURCES += \
    usbtmc488interface.cpp \
    usbTmcCallbackFunc.cpp

