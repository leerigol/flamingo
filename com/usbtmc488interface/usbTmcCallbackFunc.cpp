#include "usbtmc/usb488ioControl.h"
#include "usbtmc488interface.h"
#include "../include/dsodbg.h"

usbtmc488interface  *pInterface = NULL;


/*!
 * \brief UsbLinkStatusCallback  USB链接状态提示
 * \param status                 状态 link OR unlink
 * \return
 */
void UsbLinkStatusCallback(bool status)
{
    if(pInterface != NULL)
    {
        if(status)
        {
            pInterface->onConnection();
        }
        else
        {
            pInterface->onDisConnected();
        }
    }
}


/*!
 * \brief usbTmcRxCallback
 * \param buf
 * \param len
 * \return
 */
QByteArray inData;
USBTMCERROR usbTmcRxCallback(u8 *buf, u32 len)
{
    QByteArray data((char*)buf, len);
    //qDebug()<<__FILE__<<__LINE__<<data;
    //!送数据到ＳＣＰＩ
    pInterface->inPut(data, false);
    //!通知控制器
    pInterface->onController();

    return NONE_ERR;
}


/*!
 * \brief usbTmcTxCallback
 * \param buf
 * \param len
 * \param eom
 * \return
 */

USBTMCERROR usbTmcTxCallback(u8 *buf, u32 &len, bool &eom)
{
    Q_ASSERT( pInterface != NULL );
    Q_ASSERT( buf != NULL );

    u32 retuenSize = pInterface->getOutData(buf, len, eom);

    if((retuenSize == 0)&&(eom == false) )
    {
        return TX_BUFF_EMPTY;
    }
    else
    {
        len = retuenSize;
        return NONE_ERR;
    }
}

/*!
 * \brief get_stbValueCallback
 * \return
 */
u8 get_stbValueCallback()
{
    //! 获取STB寄存器值
    return pInterface->getStbReg();
}


void usbMepMessagesCallbackBav(bool value)
{
  Q_ASSERT( pInterface != NULL );
  if(value)
  {
      QByteArray data;
      //!送数据到ＳＣＰＩ
      pInterface->inPut(data, true);
      //!通知控制器
       pInterface->onController();
  }
}

/*!
 * \brief usbMepMessagesCallbackBrq
 * \param value    true:准备从ＳＣＰＩ读取数据，　ｆａｌｓｅ：读取数据结束．
 */
void usbMepMessagesCallbackBrq(bool value)
{
    if( value)
    {

    }
    else
    {
    }
}
void usbMepMessagesCallbackGet(bool value)
{

    if( value)
    {
        //锁　InData
    }
    else
    {
       //解锁锁　InData
    }

//    USBTMC_DEBUG("set get = %d", value);
}
void usbMepMessagesCallbackDacs(bool value)
{
      Q_UNUSED(value)
//    USBTMC_DEBUG("set dacs = %d", value);
}
void usbMepMessagesCallbackRmtsent(bool value)
{
      Q_UNUSED(value)
//    USBTMC_DEBUG("set rmtsent = %d", value);
}
