#include "../../include/dsostd.h"
#include "usbtmc488interface.h"
#include "usbtmc/usb488ioControl.h"

#include "../service/service_name.h"
#include "../service/servscpi/servscpidso.h"
#include "../service/service.h"

#include "../../service/servutility/servutility.h"
#include "../../service/servgui/servgui.h"

usbtmc488interface::usbtmc488interface(scpiIntfType scpiType, scpiIntfId id)
    :CScpiInterface(scpiType, id)
{

}

void usbtmc488interface::onController()
{
     Q_ASSERT( m_pController != NULL );
    //!通知 scpi 控制器
//    m_pController->onReceive( this );
}

/*!
 * \brief usbtmc488interface::onNewConnection 建立新链接
 */
void usbtmc488interface::onNewConnection(void)
{
    //!设置ＵＳＢ４８８接口对象
    pInterface = this;

    //!加载ＵＳＢ驱动
    QString model =  servUtility::getSystemModel();
    QString serial = servUtility::getSystemSerial();

    //qDebug() << "USBTMC INIT:" << model << serial;
#ifndef _SIMULATE
    usbtmcDeviceInit((u8*)(model.toLatin1().data()), (u8*)(serial.toLatin1().data()), SCPI_USB_PID);
#endif
}

void usbtmc488interface::onConnection()
{
    servGui::showInfo(sysGetString( INF_USBTMC_CONNECTED, "USBTMC connected") );
}

void usbtmc488interface::onDisConnected()
{
    servGui::showInfo(sysGetString( INF_USBTMC_DISCONNECTED, "USBTMC disconnected") );
}

void usbtmc488interface::srqRespond(quint8 stbReg)
{
    SCPI_INTERFACE_DEBUG("usb 488 srq event: %d",stbReg);
    usbTmcSendStbInterrupt((u8)stbReg );
}

