#ifndef USBTMC488INTERFACE_H
#define USBTMC488INTERFACE_H
#include "../scpiparse/cscpiparser.h"


#define USB488_TEST

class usbtmc488interface:public CScpiInterface
{
    Q_OBJECT
public:
    usbtmc488interface(scpiIntfType scpiType = scpi_intf_usb, scpiIntfId id = 1);

public:
    void onController();

public Q_SLOTS:
    void onNewConnection(void);
    void onConnection(void);
    void onDisConnected();
    void srqRespond(quint8 stbReg);
};

extern usbtmc488interface  *pInterface;

#endif // USBTMC488INTERFACE_H
