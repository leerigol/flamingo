#include "usb488ioControl.h"
#include "usbtmc.h"

/*! inux线程-互斥锁pthread_mutex_t, 用于多个线程保持同步  */

void usbtmcDeviceInit(u8 *pu8ProductDesc, u8 *pu8SerialNum, u16 productid/*, void *interfice*/)
{
    usbtmc_device_init(pu8ProductDesc, pu8SerialNum, productid);
}

void usbTmcSendStbInterrupt(u8 status)
{
    StbUsbTmcInterrupt(status);
}

/*! ****************回调函数 需要在用户程序中重新实现 才有实际意义.***************************************/

#if 1

/*!
 * \brief UsbLinkStatusCallback
 * \param status
 */
__attribute__((weak)) void UsbLinkStatusCallback(bool status)
{
   USBTMC_WARININ("LINK STATUS : %s", status? "link" : "unlink");
}


/*!
 * \brief usbTmcRxCallback
 * \param buf
 * \param len
 * \return
 */
__attribute__((weak)) USBTMCERROR usbTmcRxCallback(u8 *buf, u32 len)
{
     USBTMC_WARININ(" RXsize:%d;   RXDATA: %s ",len, buf);
     return NONE_ERR;
}


/*!
 * \brief usbTmcTxCallback
 * \param buf
 * \param len
 * \param eom
 * \return
 */
__attribute__((weak)) USBTMCERROR usbTmcTxCallback(u8 *buf, u32 &len, bool &eom)
{
     eom = true;
     USBTMC_WARININ(" TXsize:%d;   TXDATA: %s ",len, buf);
     return NONE_ERR;
}

/*!
 * \brief get_stbValueCallback
 * \return
 */
__attribute__((weak)) u8 get_stbValueCallback()
{
    return 0x00;
}


__attribute__((weak)) void usbMepMessagesCallbackBav(bool value)
{
    USBTMC_WARININ("set bav = %d", value);
}
__attribute__((weak)) void usbMepMessagesCallbackBrq(bool value)
{
    USBTMC_WARININ("set brq = %d", value);
}
__attribute__((weak)) void usbMepMessagesCallbackGet(bool value)
{
    USBTMC_WARININ("set get = %d", value);
}
__attribute__((weak)) void usbMepMessagesCallbackDacs(bool value)
{
    USBTMC_WARININ("set dacs = %d", value);
}
__attribute__((weak)) void usbMepMessagesCallbackRmtsent(bool value)
{
    USBTMC_WARININ("set rmtsent = %d", value);
}
#endif

