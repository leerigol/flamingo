#ifndef USB488IOCONTROL_H
#define USB488IOCONTROL_H
#include "DataType.h"
#include <pthread.h>     /*! 线程头文件   */

/*!
 * \brief usbTmcMutex
 * 线程锁
 */
extern pthread_mutex_t  &usbTmcMutex;

/*!
 * \brief usbtmcDeviceInit
 * USB初始化函数：
 * 功能：
 * ＊加载ＵＳＢ驱动
 * ＊创建ＵＳＢＴＭＣ数据处理和请求控制线程
 * \param pu8ProductDesc
 * \param pu8SerialNum
 * \param productid
 */
void usbtmcDeviceInit(u8* pu8ProductDesc, u8 * pu8SerialNum, u16 productid);

/*!
 * \brief usbTmcSendStbInterrupt
 * ＵＳＢＴＭＣ中断函数接口，用于发送　STB寄存器事件触发的中断
 * \param status　STB寄存器值
 */
void usbTmcSendStbInterrupt(u8 status);

/*! ********************回调函数********************************/

/*!
 * \brief UsbLinkStatusCallback  USB链接状态提示
 * \param status                 状态 link OR unlink
 * \return
 */
void UsbLinkStatusCallback(bool status);


/*!
 * \brief usbTmcRxCallback ＵＳＢＴＭＣ接受数据的回调函数
 * \param buf　　　　　　　　　用于保存数据的缓存
 * \param len　　　　　　　　　接收的数据长度
 * \return
 */
USBTMCERROR usbTmcRxCallback(u8 *buf, u32 len);
/*!
 * \brief usbTmcTxCallback　ＵＳＢＴＭＣ主机读取数据的回调函数
 * \param buf　　　　　　　　　ＵＳＢＴＭc缓冲区
 * \param len　　　　　　　　　数据长度
 * \param eom　　　　　　　　　数据结束标志
 * \return
 */
USBTMCERROR usbTmcTxCallback(u8 *buf, u32 &len, bool &eom);
/*!
 * \brief get_stbValueCallback　获取STB寄存器的值
 * \return
 */
u8   get_stbValueCallback();

/*!  Message Exchange Protocol for USB */
void usbMepMessagesCallbackBav(bool value);     //!bav
void usbMepMessagesCallbackBrq(bool value);     //!brq
void usbMepMessagesCallbackGet(bool value);     //!get
void usbMepMessagesCallbackDacs(bool value);    //!dacs
void usbMepMessagesCallbackRmtsent(bool value); //!RMT-sent

#endif // USBIOCONTROL_H
