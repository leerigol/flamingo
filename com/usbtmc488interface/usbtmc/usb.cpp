#include "usb.h"
#include "usbtmc.h"
#include  <string.h>

/*!
 * \brief usb_read  从ｂｕｌｋ－ｏｕｔ端点读数据
 * \param fd        usb的设备文件句柄
 * \param pBuf      数据包地址
 * \param u32Length 实际读取USB总线的数据长度
 * \return
 */
size_t usbRead(int fd, u8 *pBuf, u32 u32Length)
{
    USBTMC_ENTER();
    size_t i = read(fd,pBuf,(size_t)u32Length);
    USBTMC_EXIT();
    return i;
}


/*!
 * \brief usb_write   向ｂｕｌｋ－ｉｎ端点写数据
 * \param fd          usb的设备文件句柄
 * \param pBuf        数据包地址
 * \param u32Length   数据包长度
 * \return            实际写入USB总线的数据长度
 */
u32 usbWrite(int fd, u8 *pBuf, u32 u32Length)
{
    USBTMC_ENTER();
    size_t i = write(fd, (unsigned char *)pBuf, u32Length);
    USBTMC_DEBUG("Write Size = %d", i);
    USBTMC_EXIT();
    return i;
}


/*!
 * \brief usbReadEndpoint　从控制端点读数据
 * \param fd
 * \param pBuf
 * \param u8Length         控制端点固定返回8字节数据.
 * \return
 */
size_t usbReadEndpoint(int fd, u8 *pBuf, u8 /*u8Length*/)
{
    USBTMC_ENTER();
    ioctl(fd, USBDEVTMC_IOCTL_READ_ENDPOINT_DATA, pBuf);
    USBTMC_EXIT();
    return 8;
}


/*!
 * \brief usb_control_endpoint_write 向控制端点写数据
 * \param fd
 * \param pBuf
 * \param u8Length                   应为驱动只开了64字节缓存,所以u8Length不得大于64字节
 */
void usbControlEndpointWrite(int fd, u8* pBuf, u8 u8Length)
{
    USBTMC_ENTER();
    u8 buftemp[64] = {0};
    buftemp[0] = u8Length;
    memcpy(buftemp+1, pBuf, u8Length);
    ioctl(fd, USBDEVTMC_IOCTL_CONTROL_WRITE, buftemp);
    USBTMC_EXIT();
}


/*!
 * \brief usb_irq_write 向USB中断端点写数据
 * \param fd            usb的设备文件句柄
 * \param pBuf          2字节的状态字节
 */
void usbInterruptWrite(int fd, u8* pBuf)
{
    USBTMC_ENTER();
    USBTMC_DEBUG("usbInterruptWrite ");
    ioctl(fd, USBDEVTMC_IOCTL_INTERRUPT_WRITE, pBuf);
    USBTMC_EXIT();
}


/*!
 * \brief usbIoctlWriteRead   向ioctl写命令,并读取返回值
 * \param fd
 * \param cmd                  命令
 * \param p   　　　　　　　　　　存放返回值的指针；需要注意与驱动那边返回值的类型兼容,否则会段错误.
 */
void usbIoctlWriteRead(int fd, u8 cmd, void *p)
{ 
    USBTMC_ENTER();
    USBTMC_DEBUG("usbIoctlWriteRead CMD is: %d", cmd);

    if( p == NULL )
    {
       ioctl(fd, cmd);
    }
    else
    {
       ioctl(fd, cmd, p);
    }
    USBTMC_EXIT();
}

