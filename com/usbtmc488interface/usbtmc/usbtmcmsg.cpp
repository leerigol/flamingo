#include "usbtmcmsg.h"
#include "usb.h"
#include "usb488ioControl.h"
#include  <string.h>
#include <assert.h>

using namespace std;

usbtmcStatus usbTmcStatus;
//usbtmcStatus usbTmcStatus;

u8* rxBuff = NULL;
u8* txBuff = NULL;

#define RX_BUFF_MAX   20480000
#define TX_BUFF_MAX   20480000

#define USBTMC_HEADER_LEN    (12)
#define USBTMC_REQUESTS_LEN  (8)


static bool usbtmc_packaging(u8 *buf, u32 len, u8 bTag, bool eom);

static void usbTmcMsgidDevDepMsgOut(int usbDriveFd, usb_tmc_bulk_header usbTmcHead)
{
    //! status
    usbTmcStatus.set_BulkOutBXFStatus(transferProgress);

    usbTmcStatus.set_BulkOutbTag(usbTmcHead.bTag);      //!保存此次传输的bTag
    usbTmcStatus.clear_BulkOutTransferedCount();         //!清空传输计数

    if( (usbTmcHead.command_specific.dev_dep_msg_out.reserved[0]
            |usbTmcHead.command_specific.dev_dep_msg_out.reserved[1]
            |usbTmcHead.command_specific.dev_dep_msg_out.reserved[2] ) != 0)
    {
        USBTMC_ERROR("%s","error:reserved = 0");
        return;
    }

    //!读取主机这一包要给我发送多少数据 带字节对齐 4的整数倍
    u32 rxDataSize    = usbTmcHead.command_specific.dev_dep_msg_out.transferSize;      //从usb数据包中的有效字节数
    u32 alignmentSize = 4 - (rxDataSize & 0x00000003);                                 //对齐字节数
    alignmentSize     = (alignmentSize == 4)? 0 : alignmentSize;                       //!如果本身是字节对齐的，则不需要对齐。
    u32 residueSize   = rxDataSize + alignmentSize;                                    //需要从驱动读取的总字节数


    rxBuff = new  u8[ 1 + ((residueSize < RX_BUFF_MAX)? residueSize : RX_BUFF_MAX) ]();//!申请内存时按照最大数据申请;
    assert(rxBuff != NULL);

    do{
            u32 readBuffSize  = (RX_BUFF_MAX >= residueSize)? residueSize : RX_BUFF_MAX;//!读取数据时,按照最小数据读取
            size_t ret        = usbRead(usbDriveFd, rxBuff, readBuffSize);              //!从驱动读取数据
            if(ret != readBuffSize )
            {
                USBTMC_ERROR("error:usb dev Read the data failure  read:%d  return:%d",readBuffSize, ret);
                break;
            }

            residueSize -= ret;

            if( residueSize == 0)
            {
                usbTmcRxCallback(rxBuff, (ret - alignmentSize));//!将去掉对齐字节后的剩余字节 送到scpi接口
                break;
            }
            else
            {
                usbTmcRxCallback(rxBuff, ret);//! 将读取的数据送到scpi接口
            }

    }while(transferProgress == usbTmcStatus.get_BulkOutBXFStatus());

    delete [] rxBuff;
    rxBuff = NULL;

    //! status
    usbTmcStatus.set_BulkOutTransferedCount((rxDataSize + alignmentSize) - residueSize ); //!计算已经传输了多少字节数据

    if(residueSize != 0)
    {
        USBTMC_ERROR("%s","error:residueSize != 0");
    }
    else
    {
        //!判断这是否是最后一包　如果时最后一包　即数据接收结束，需要设置状态标志
        if( usbTmcHead.command_specific.dev_dep_msg_out.bmTransferAttributes == 0x01 )
        {
            //!设置 IEEE 488.2 bav标志  bav = true;　表示已经接收数据完成　准备写入缓冲区
            usbMepMessagesCallbackBav(true);
        }
    }

    //!设置传输状态空闲
    usbTmcStatus.set_BulkOutBXFStatus( transferIdle);
}

static void usbTmcMsgidRequestDevDepMsgIn(int usbDriveFd, usb_tmc_bulk_header usbTmcHead)
{
    //! status
    usbTmcStatus.set_BulkInBXFStatus(transferProgress);

    usbTmcStatus.set_BulkInbTag(usbTmcHead.bTag);       //!保存此次传输的bTag
    usbTmcStatus.clear_BulkInTransferedCount();   //!清空传输计数

    if( (usbTmcHead.command_specific.req_dev_dep_msg_in.reserved[0]
            |usbTmcHead.command_specific.req_dev_dep_msg_in.reserved[1])!= 0)
    {
        USBTMC_ERROR("%s","error:reerved = 0");
        return;
    }

    //!读取主机这一包要获取多少数据 　注意：从机不需要填充对其字节
     u32 TxDataSize = usbTmcHead.command_specific.req_dev_dep_msg_in.transferSize;

     u32 writeBuffSize = 0;
     //!主机要的数据量和从机准备的数据量谁的少，以谁的为一包数据的大小．
     writeBuffSize = (TX_BUFF_MAX <  TxDataSize)? TX_BUFF_MAX : TxDataSize;

     //!申请一块内存并初始化为0
     txBuff = new u8[writeBuffSize + USBTMC_HEADER_LEN]();
     assert(txBuff != NULL);
     //!设置 IEEE 488.2 brq标志  brq = true; 表示准备从输出队列取数据
     usbMepMessagesCallbackBrq(true);

     bool eom = false;//!最后一包标志

     //!填充数据 　（除非得到数据或主机发来终止请求　否则一直等待）
     while(NONE_ERR != usbTmcTxCallback(txBuff + USBTMC_HEADER_LEN, writeBuffSize, eom))
     {
         //!主机发来终止请求 退出
         if(transferProgress != usbTmcStatus.get_BulkInBXFStatus())
         {
              printf("AbortBulkin\n");
              writeBuffSize = 0;
              break;
         }
     }
     //!打包
     size_t ret = 0;
     if( usbtmc_packaging(txBuff, writeBuffSize, usbTmcHead.bTag, eom) )
     {
         //!将数据发送给驱动
         ret = usbWrite(usbDriveFd, txBuff, writeBuffSize + USBTMC_HEADER_LEN);
     }

     //! status
     usbTmcStatus.set_BulkInBXFStatus(transferIdle);

     //!计算已经传输了多少字节数据
     usbTmcStatus.add_BulkInTransferedCount( ( ret>USBTMC_HEADER_LEN )? (ret-USBTMC_HEADER_LEN) : 0);

     //!设置 IEEE 488.2  RMT-sent标志  RMT-sent = TRUE　表示数据发送完成
     usbMepMessagesCallbackRmtsent(true);

     //!释放缓存
     delete [] txBuff;
     txBuff = NULL;
}

static void usbTmcMsgidVendorSpecificOut(int /*usbDriveFd*/, usb_tmc_bulk_header /*usbTmcHead*/)
{

}

static void usbTmcMsgidRequestVendorSpecificIn(int/* usbDriveFd*/, usb_tmc_bulk_header /*usbTmcHead*/)
{

}

static void usbTmcMsgidTrigger()
{
    //!与SCPI交互。
    //!设置 IEEE 488.2  get标志  get = true;
    usbMepMessagesCallbackGet(true);
}

/*!
 * \brief usbTmcMsgidStateMachine 消息切换
 * \param usbDriveFd
 * \param usbTmcHead
 */
void usbTmcMsgidStateMachine(int usbDriveFd/*, usb_tmc_bulk_header usbTmcHead*/)
{
    usb_tmc_bulk_header usbTmcHead;
    size_t ret = usbRead(usbDriveFd, (u8*)&usbTmcHead, USBTMC_HEADER_LEN);

    if(ret != USBTMC_HEADER_LEN)
    {
        USBTMC_ERROR("bulk held:%X %X %X %X  %X %X %X %X  %X %X %X %X",usbTmcHead.MsgID,usbTmcHead.bTag,usbTmcHead.bTagInverse,usbTmcHead.reserved,
                     usbTmcHead.command_specific.raw[0],usbTmcHead.command_specific.raw[1],usbTmcHead.command_specific.raw[2],usbTmcHead.command_specific.raw[3],
                usbTmcHead.command_specific.raw[4],usbTmcHead.command_specific.raw[5],usbTmcHead.command_specific.raw[6],usbTmcHead.command_specific.raw[7]);
        USBTMC_ERROR("error:ret = %d",ret);
        return;
    }

    if(usbTmcHead.bTag != (u8)(~usbTmcHead.bTagInverse))
    {
        USBTMC_ERROR("bulk held:%X %X %X %X  %X %X %X %X  %X %X %X %X",usbTmcHead.MsgID,usbTmcHead.bTag,usbTmcHead.bTagInverse,usbTmcHead.reserved,
                     usbTmcHead.command_specific.raw[0],usbTmcHead.command_specific.raw[1],usbTmcHead.command_specific.raw[2],usbTmcHead.command_specific.raw[3],
                usbTmcHead.command_specific.raw[4],usbTmcHead.command_specific.raw[5],usbTmcHead.command_specific.raw[6],usbTmcHead.command_specific.raw[7]);
        USBTMC_ERROR("ignoring invalid: bTag != ~bTagInverse    bTag = %x;bTagInverse = %x", usbTmcHead.bTag, usbTmcHead.bTagInverse);
        return;
    }

    if(usbTmcHead.reserved != 0x00)
    {
        USBTMC_ERROR("error:reserved != 0x00 ");
        return;
    }

    switch(usbTmcHead.MsgID)
    {
    case USB_TMC_MSGID_DEV_DEP_MSG_OUT:
        usbTmcMsgidDevDepMsgOut(usbDriveFd, usbTmcHead);
        break;

    case USB_TMC_MSGID_REQUEST_DEV_DEP_MSG_IN:
        usbTmcMsgidRequestDevDepMsgIn(usbDriveFd, usbTmcHead);
        break;

    case USB_TMC_MSGID_VENDOR_SPECIFIC_OUT:
        usbTmcMsgidVendorSpecificOut(usbDriveFd, usbTmcHead);
        break;

    case USB_TMC_MSGID_REQUEST_VENDOR_SPECIFIC_IN:
        usbTmcMsgidRequestVendorSpecificIn(usbDriveFd, usbTmcHead);
        break;

    case USB_TMC_MSGID_TRIGGER:
        usbTmcMsgidTrigger();
        break;

    default:
        break;
    }
}


/*! USBTMC requests values */
//TMC
static void UsbtmcRequestInitiateAbortBulkOut(int usbDriveFd, usb_tmc_requests requests )
{
    usbtmc_initiate_abort_bulk_out_response_packet packed;

    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;
    packed.bTag          = usbTmcStatus.get_BulkOutbTag();

    //!设置状态 通知bulk-out处理程序 停止处理
    usbTmcStatus.set_BulkOutBXFStatus(transferAbort);

    //!与驱动交互
    //!清除bulk-out缓存
    usbIoctlWriteRead(usbDriveFd, USBDEVTMC_IOCTL_CLEAR_BULK_OUT);

    //!与SCPI交互。
    //!设置 IEEE 488.2  bav标志  bav = false;
    usbMepMessagesCallbackBav(false);

    if(((u8)requests.wValue) != usbTmcStatus.get_BulkOutbTag() )
    {
        //! bTag 不匹配
        packed.USBTMC_status = USBTMC_STATUS_TRANSFER_NOT_IN_PROGRESS;
    }

    if(transferProgress == usbTmcStatus.get_BulkOutBXFStatus())
    {
       //!当前没有传输　且ｆｉｆｏ为空
       packed.USBTMC_status = USBTMC_STATUS_PENDING;
    }

    if(0 == packed.bTag)
    {
       //!从未有过传输
       packed.USBTMC_status = USBTMC_STATUS_SUCCESS;
    }
    //!发送回复包
    USBTMC_WARININ("%x,%x",packed.bTag,packed.USBTMC_status);
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 2);
}
static void UsbtmcRequestCheckAbortBulkOutStatus(int usbDriveFd )
{
    usbtmc_check_abort_bulk_out_status_response_packet packed;
    //! 假设已经终止
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;

    if((usbTmcStatus.get_BulkOutTransferedCount() == 0) || (usbTmcStatus.get_BulkOutBXFStatus() != transferAbort))
    {
        //! 没有成功终止传输　或者　NBYTES_RXD无效
        packed.USBTMC_status = USBTMC_STATUS_PENDING;
    }

    packed.Reserved[0]      = 0x00;
    packed.Reserved[1]      = 0x00;
    packed.Reserved[2]      = 0x00;
    packed.NBYTES_RXD       = usbTmcStatus.get_BulkOutTransferedCount();

    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 8);
}
static void UsbtmcRequestInitiateAbortBulkIn(int usbDriveFd, usb_tmc_requests requests )
{
    //!设置状态 通知bulk-in处理程序 停止处理
    usbTmcStatus.set_BulkInBXFStatus(transferAbort);
    //!与驱动交互
    //! 清除bulk-in缓存
    usbIoctlWriteRead(usbDriveFd, USBDEVTMC_IOCTL_CLEAR_BULK_IN);

    //!发送回复包
    usbtmc_initiate_abort_bulk_in_response_packet packed;
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS ;

    if(usbTmcStatus.get_BulkInBXFStatus() == transferProgress)
    {
        packed.USBTMC_status = USBTMC_STATUS_PENDING ;
    }

    packed.bTag = usbTmcStatus.get_BulkInbTag();
    if( (requests.wValue & 0x00FF) != packed.bTag )
    {
        packed.USBTMC_status = USBTMC_STATUS_TRANSFER_NOT_IN_PROGRESS;
    }

    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 2);

    //! 终止后，需要再发送一个bulk in包，参考安捷伦的usb抓包分析。
    //! 否则会出现，终止后，重新打开visa，数据正确回去，但visa始终报错。
    //! 注意 ：tmc协议中没有这一步骤。
    u16 txbuf= 0x0000;
    usbWrite(usbDriveFd,(u8*)(&txbuf) , 2);
    //!与SCPI交互。
    //!设置 IEEE 488.2  brq标志  brq = false;
    usbMepMessagesCallbackBrq(false);
}
static void UsbtmcRequestCheckAbortBulkInStatus( int usbDriveFd )
{
    usbtmc_check_abort_bulk_in_status_response_packet packed;

    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;

    //!0x01 FIFO有数据,且USBTMC_status != USBTMC_STATUS_SUCCESS, 0X00:fifo是空的
    packed.bmAbortBulkIn = 0x00;

    packed.Reserved      = 0x0000 ;
    packed.NBYTES_TXD    = usbTmcStatus.get_BulkInTransferedCount() ;
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 8);
}
static void UsbtmcRequestInitiateClear( int usbDriveFd )
{
    //!设置状态 通知bulk-out处理程序 停止处理
    usbTmcStatus.set_BulkOutBXFStatus(transferAbort);
    //!设置状态 通知bulk-in处理程序 停止处理
    usbTmcStatus.set_BulkInBXFStatus(transferAbort);

    //!清除bulk-out缓存
    usbIoctlWriteRead(usbDriveFd, USBDEVTMC_IOCTL_CLEAR_BULK_OUT);
    //! 清除bulk-in缓存
    usbIoctlWriteRead(usbDriveFd, USBDEVTMC_IOCTL_CLEAR_BULK_IN);

    //! 回复包
    usbtmc_initiate_clear_response_packet  packed;
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;

    USBTMC_DEBUG("");
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 1);

    //!与SCPI交互。
    //!设置 IEEE 488.2  dcas 标志 dcas = true;
    usbMepMessagesCallbackDacs(true);
    //!设置 IEEE 488.2  brq  标志 brq = false;
     usbMepMessagesCallbackBrq(false);
    //!设置 IEEE 488.2  bav  标志 bav = false;
    usbMepMessagesCallbackBav(false);
}
static void UsbtmcRequestCheckClearStatus( int usbDriveFd, usb_tmc_requests /*requests*/ )
{
    //! 回复包
    usbtmc_check_clear_status_response_packet packed;
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;
    packed.bmClear       = 0x00; //这里需要改, 根据fifo是否为空来取值
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 2);

    //!与SCPI交互。
    //!设置 IEEE 488.2  dcas 标志 当USBTMC_STATUS不等于STATUS_PENDING时dcas = true;
    if(packed.USBTMC_status != USBTMC_STATUS_PENDING)
    {
        usbMepMessagesCallbackDacs(true);
    }
    else
    {
        usbMepMessagesCallbackDacs(false);
    }

}
static void UsbtmcRequestGetCapabilities( int usbDriveFd)
{
    usbtmc488_get_capabilities_response_packet packed;
    packed.USBTMC_status               = USBTMC_STATUS_SUCCESS;
    packed.Reserved_tmc_1              = 0x00;
    packed.bcdUSBTMC                   = 0x0210;
    packed.USBTMCInterfaceCapabilities = 0x00;  //!根据usb488 4.2.2节定义 D2必须等于1          0x04;
    packed.USBTMCDeviceCapabilities    = 0x00;  //!根据usb488 4.2.2节定义 D0,D1,D2,D3都等于1   0x0F
    packed.Reserved_tmc_2              = 0x0000;
    packed.Reserved_tmc_3              = 0x0000;
    packed.Reserved_tmc_4              = 0x0000;

    packed.bcdUSB488                   = 0x0210; //!usb2.0 即210H
    packed.USB488InterfaceCapabilities = 0x04;  //!支持  REN_CONTROL,GO_TO_LOCAL,LOCAL_LOCKOUT请求,不支持TRIGGER消息.  0x0e
    packed.USB488DeviceCapabilities    = 0x00;  //!0X0F表示全支持 但是其实我们应该是0x06.
    packed.Reserved_488_1              = 0x00000000;
    packed.Reserved_488_2              = 0x00000000;

    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 24);

}
static void UsbtmcRequestIndicatorPulse( int usbDriveFd, usb_tmc_requests /*requests*/ )
{
    usbtmc_indicator_pulse_response_packet packed;
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;
    USBTMC_DEBUG("");
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 1);
}

/* usbtmc 488*/
static void UsbtmcRequestReadStatusByte(int usbDriveFd, usb_tmc_requests requests )
{
    u8 bTag  = ((requests.wValue) & 0x007F); //!提取bTag
    //!因为有中断端点 所以状态值通过中断端点返回
    usb_tmc_interruot_in_setup_packet  InterruptPacket;
    InterruptPacket.bNotify1.Number = (bTag | 0X80);
    InterruptPacket.StatusByte      = get_stbValueCallback();
    usbInterruptWrite(usbDriveFd, (u8*)&InterruptPacket);


    usbtmc_read_status_byte_response_packet packet;
    packet.bTag = bTag;
    //ioctl(usbDriveFd, USBTMC_SCPI_INT_TRANS_RESULT, &(packet.USBTMC_status)); //!读取中断发送结果
    packet.USBTMC_status = USBTMC_STATUS_SUCCESS;
    packet.ie.Constant   = 0x00;
    //!返回有中断端点类型的应答包
    usbControlEndpointWrite(usbDriveFd, (u8*)&packet, 0x03);
}
static void UsbtmcRequestRenControl(int usbDriveFd, usb_tmc_requests /*requests */)
{
    usbtmc_ren_control_response_packet packed;
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;
    USBTMC_DEBUG("");
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 1);
}
static void UsbtmcRequestGoToLocal(int usbDriveFd, usb_tmc_requests /*requests*/ )
{
    usbtmc_go_to_local_response_packet packed;
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;
    USBTMC_DEBUG("");
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 1);

}
static void UsbtmcRequestLocalLockout(int usbDriveFd, usb_tmc_requests /*requests*/ )
{
    usbtmc_local_lockout_response_packet packed;
    packed.USBTMC_status = USBTMC_STATUS_SUCCESS;
    USBTMC_DEBUG("");
    usbControlEndpointWrite(usbDriveFd, (u8*)(&packed), 1);
}

/*!
 * \brief usbTmcRequestsStateMachine 控制请求切换
 * \param usbDriveFd
 * \param usbRequests
 */
void usbTmcRequestsStateMachine(int usbDriveFd/*, usb_tmc_requests usbRequests1*/)
{
    usb_tmc_requests usbRequests;
    size_t ret =    usbReadEndpoint(usbDriveFd, (u8*)&usbRequests, USBTMC_REQUESTS_LEN);

    if(ret != USBTMC_REQUESTS_LEN)
    {
        USBTMC_ERROR("ctl held:%X %X %2X  %2X %2X ",usbRequests.bRequestType, usbRequests.bRequest, usbRequests.wValue, usbRequests.wIndex,usbRequests.wLength);
        USBTMC_ERROR("error ctlRet = %d",ret);
        return;
    }

    switch(usbRequests.bRequestType & USB_TYPE_MASK)
    {
    case USB_TYPE_CLASS:
        switch (usbRequests.bRequest)
        {
        case USBTMC_REQUEST_INITIATE_ABORT_BULK_OUT:
             UsbtmcRequestInitiateAbortBulkOut(usbDriveFd, usbRequests );
            break;
        case USBTMC_REQUEST_CHECK_ABORT_BULK_OUT_STATUS:
             UsbtmcRequestCheckAbortBulkOutStatus(usbDriveFd );
            break;
        case USBTMC_REQUEST_INITIATE_ABORT_BULK_IN:
             UsbtmcRequestInitiateAbortBulkIn(usbDriveFd, usbRequests );
            break;
        case USBTMC_REQUEST_CHECK_ABORT_BULK_IN_STATUS:
             UsbtmcRequestCheckAbortBulkInStatus( usbDriveFd );
            break;
        case USBTMC_REQUEST_INITIATE_CLEAR:
             UsbtmcRequestInitiateClear( usbDriveFd );
            break;
        case USBTMC_REQUEST_CHECK_CLEAR_STATUS:
             UsbtmcRequestCheckClearStatus( usbDriveFd, usbRequests );
            break;
        case USBTMC_REQUEST_GET_CAPABILITIES:
             UsbtmcRequestGetCapabilities( usbDriveFd);
            break;
        case USBTMC_REQUEST_INDICATOR_PULSE:
             UsbtmcRequestIndicatorPulse( usbDriveFd, usbRequests );
            break;
        case USBTMC_REQUEST_READ_STATUS_BYTE:
             UsbtmcRequestReadStatusByte(usbDriveFd, usbRequests);
            break;
        case USBTMC_REQUEST_REN_CONTROL:
            UsbtmcRequestRenControl(usbDriveFd, usbRequests );
            break;
        case USBTMC_REQUEST_GO_TO_LOCAL:
            UsbtmcRequestGoToLocal(usbDriveFd, usbRequests );
            break;
        case USBTMC_REQUEST_LOCAL_LOCKOUT:
            UsbtmcRequestLocalLockout(usbDriveFd, usbRequests );
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

static bool usbtmc_packaging(u8 *buf, u32 len, u8 bTag, bool eom)
{
    USBTMC_DEBUG("bulk in pack size: %d  eom: %d", len, eom);

    if( (len <= 0) && (eom !=  true))
    {
        return false;
    }

    usb_tmc_bulk_header utbh_head;

    utbh_head.MsgID       = USB_TMC_MSGID_DEV_DEP_MSG_IN ;

    utbh_head.bTag        = bTag;

    utbh_head.bTagInverse = (u8)(~(utbh_head.bTag));

    utbh_head.reserved    = 0x00;

    utbh_head.command_specific.dev_dep_msg_in.transferSize = len;

    utbh_head.command_specific.dev_dep_msg_in.bmTransferAttributes = eom;

    utbh_head.command_specific.dev_dep_msg_in.reserved[0]  = 0x00;

    utbh_head.command_specific.dev_dep_msg_in.reserved[1]  = 0x00;

    utbh_head.command_specific.dev_dep_msg_in.reserved[2]  = 0x00;

    assert(buf != NULL);
    memcpy(buf, (u8*)(&utbh_head), USBTMC_HEADER_LEN);

    return true;
}


void notifyUsbLinkStatus(bool status)
{
    usbTmcStatus.set_BulkInbTag(0);      //!清除状态
    usbTmcStatus.set_BulkOutbTag(0);      //!清除状态

    //!设置状态 通知bulk-out处理程序 停止处理
    usbTmcStatus.set_BulkOutBXFStatus(transferAbort);
    //!设置状态 通知bulk-in处理程序 停止处理
    usbTmcStatus.set_BulkInBXFStatus(transferAbort);

    UsbLinkStatusCallback(status);
}


/*! ************* */

void  usbtmcStatus::set_BulkInBXFStatus(const transfer_status value)
{
    bulkin.BXFStatus = value;
}

void  usbtmcStatus::add_BulkInTransferedCount(const u32 value)
{
    bulkin.TransferedCount = value;
}

transfer_status  usbtmcStatus::get_BulkInBXFStatus()
{
    transfer_status value = bulkin.BXFStatus;
    return value;
}

u32  usbtmcStatus::get_BulkInTransferedCount()
{
    u32 value = bulkin.TransferedCount;
    return value;
}
void  usbtmcStatus::clear_BulkInTransferedCount()
{
    bulkin.TransferedCount = 0;
}


void  usbtmcStatus::set_BulkOutBXFStatus(const transfer_status value)
{
    bulkout.BXFStatus = value;
}

void  usbtmcStatus::set_BulkOutTransferedCount(const u32 value)
{
    bulkout.TransferedCount = value;
}

transfer_status  usbtmcStatus::get_BulkOutBXFStatus()
{
    transfer_status value = bulkout.BXFStatus;
    return value;
}

u32  usbtmcStatus::get_BulkOutTransferedCount()
{
    u32 value = bulkout.TransferedCount;
    return value;
}
void  usbtmcStatus::clear_BulkOutTransferedCount()
{
    bulkout.TransferedCount = 0;
}

u8 usbtmcStatus::get_BulkInStatus(int usbDriveFd)
{
    int status;

    switch(bulkin.BXFStatus)
    {
    case transferIdle:
        usbIoctlWriteRead(usbDriveFd, USBTMC_SCPI_CHECK_BULK_IN_STATUS, &status);
        break;
    case transferAbort:
        status = USBTMC_STATUS_FAILED;
        break;
    case transferProgress:
        status = USBTMC_STATUS_PENDING;
        break;
    default:
        status = USBTMC_STATUS_SUCCESS;
        break;
    }

    return status;
}

u8 usbtmcStatus::get_BulkOutStatus(int usbDriveFd)
{
    int status;

    switch(bulkout.BXFStatus)
    {
    case transferIdle:
        usbIoctlWriteRead(usbDriveFd, USBTMC_SCPI_CHECK_BULK_OUT_STATUS, &status);
        break;
    case transferAbort:
        status = USBTMC_STATUS_FAILED;
        break;
    case transferProgress:
        status = USBTMC_STATUS_PENDING;
        break;
    default:
        status = USBTMC_STATUS_SUCCESS;
        break;
    }

    return status;
}

u8 usbtmcStatus::get_BulkCtlStatus(int usbDriveFd)
{
     int status;
     usbIoctlWriteRead(usbDriveFd, USBTMC_SCPI_CHECK_BULK_CTL_STATUS, &status);
     return status;
}

u8 usbtmcStatus::get_BulkInterruptStatus(int usbDriveFd)
{
    int status;
    usbIoctlWriteRead(usbDriveFd, USBTMC_SCPI_CHECK_BULK_INTERRUPT_STATUS, &status);
    return status;
}

u8 usbtmcStatus::get_Status(int usbDriveFd)
{
    int status = USBTMC_STATUS_SUCCESS ;

    status = get_BulkInStatus(usbDriveFd);

    if( status != USBTMC_STATUS_SUCCESS)
    {
        return status;
    }

    status = get_BulkOutStatus(usbDriveFd);
    if( status != USBTMC_STATUS_SUCCESS)
    {
        return status;
    }

    return status;
}

void usbtmcStatus::set_BulkInbTag(u8 value)
{
    bulkin.bTag = value;
}

u8 usbtmcStatus::get_BulkInbTag()
{
    return  bulkin.bTag;
}

void usbtmcStatus::set_BulkOutbTag(u8 value)
{
       bulkout.bTag = value;
}

u8 usbtmcStatus::get_BulkOutbTag()
{
      return  bulkout.bTag;
}



