#ifndef USBTMCMSG_H
#define USBTMCMSG_H
#include "DataType.h"
#include <pthread.h>

/*!
 * \brief The usb_tmc_bulk_header struct
 */
struct usb_tmc_bulk_header
{
    u8 MsgID;
    u8 bTag;
    u8 bTagInverse;
    u8 reserved;

    union
    {
        struct _dev_dep_msg_out
        {
            u32 transferSize;
            u8 bmTransferAttributes;
            u8 reserved[3];
        } dev_dep_msg_out;

        struct _req_dev_dep_msg_in
        {
            u32 transferSize;
            u8 bmTransferAttributes;
            u8 TermChar;
            u8 reserved[2];
        } req_dev_dep_msg_in;

        struct _dev_dep_msg_in
        {
            u32 transferSize;
            u8 bmTransferAttributes;
            u8 reserved[3];
        } dev_dep_msg_in;

        struct _vendor_specific_out
        {
            u32 transferSize;
            u8 reserved[4];
        } vendor_specific_out;

        struct _req_vendor_specific_in
        {
            u32 transferSize;
            u8 reserved[4];
        } req_vendor_specific_in;

        struct _vendor_specific_in
        {
            u32 transferSize;
            u8 reserved[4];
        } vendor_specific_in;

        u8 raw[8];
    } command_specific;

} __attribute__((packed));//!取消结构在编译过程中的优化对齐 按照实际占用字节数进行对齐。


/*! Interrupt Setup packet*/
struct usb_tmc_interruot_in_setup_packet
{
    union
    {
        u8      Bitmap; //!  如果是事件触发，则bTag = 0x01;
        u8      Number; //!  如果READ_STATUS_BYTE request 触发中断 则 bTag = (READ_STATUS_BYTE的bTag);
    } bNotify1;

    u8 StatusByte;
}__attribute__((packed));


//! requests packet
#define USB_TYPE_MASK			(0x03 << 5) //!请求类型的掩码
#define USB_TYPE_STANDARD		(0x00 << 5) //!标准请求
#define USB_TYPE_CLASS			(0x01 << 5) //!类请求

struct usb_tmc_requests
{
    u8 bRequestType;
    u8 bRequest;
    u16 wValue;
    u16 wIndex;
    u16 wLength;
}__attribute__((packed));


//!  INITIATE_ABORT_BULK_OUT response packet
struct usbtmc_initiate_abort_bulk_out_response_packet
{
    u8  USBTMC_status;
    u8  bTag;

}__attribute__((packed));


//!  CHECK_ABORT_BULK_OUT_STATUS response packet
struct usbtmc_check_abort_bulk_out_status_response_packet
{
    u8        USBTMC_status;
    u8        Reserved[3];
    u32       NBYTES_RXD;

}__attribute__((packed));

//!  INITIATE_ABORT_BULK_IN response packet
struct usbtmc_initiate_abort_bulk_in_response_packet
{
    u8          USBTMC_status;
    u8          bTag;

}__attribute__((packed));

//!  CHECK_ABORT_BULK_IN_STATUS response packet
struct usbtmc_check_abort_bulk_in_status_response_packet
{
    u8          USBTMC_status;
    u8          bmAbortBulkIn;
    u16         Reserved;
    u32         NBYTES_TXD;

}__attribute__((packed));

//!  INITIATE_CLEAR response packet
struct usbtmc_initiate_clear_response_packet
{
    u8          USBTMC_status;
}__attribute__((packed));

//!  CHECK_CLEAR_STATUS response packet
struct usbtmc_check_clear_status_response_packet
{
    u8          USBTMC_status;
    u8          bmClear;

}__attribute__((packed));

//! GET_CAPABILITIES response packet
struct usbtmc488_get_capabilities_response_packet
{
    //!usbTmc;
    u8          USBTMC_status;
    u8          Reserved_tmc_1;
    u16         bcdUSBTMC;
    u8          USBTMCInterfaceCapabilities;
    u8          USBTMCDeviceCapabilities;
    u16         Reserved_tmc_2;
    u16         Reserved_tmc_3;
    u16         Reserved_tmc_4;

    //!usb 488
    u16         bcdUSB488;
    u8          USB488InterfaceCapabilities;
    u8          USB488DeviceCapabilities;
    u32         Reserved_488_1;
    u32         Reserved_488_2;

}__attribute__((packed));

//!  INDICATOR_PULSE response packet
struct usbtmc_indicator_pulse_response_packet
{
    u8          USBTMC_status;
}__attribute__((packed));

//!  READ_STATUS_BYTE response packet
struct usbtmc_read_status_byte_response_packet
{
    u8          USBTMC_status;
    u8          bTag;
    union
    {
        u8      StatusByte;//!  如果没有中断端点，则由控制端点返回Status Byte
        u8      Constant;  //!  如果有中断端点，由中断端点返回Status Byte，控制端点返回0x00
    } ie;

}__attribute__((packed));

//! REN_CONTROL response packet
struct usbtmc_ren_control_response_packet
{
    u8  USBTMC_status;
}__attribute__((packed));

//! GO_TO_LOCAL response packet
struct usbtmc_go_to_local_response_packet
{
    u8  USBTMC_status;
}__attribute__((packed));

//! LOCAL_LOCKOUT response packet
struct usbtmc_local_lockout_response_packet
{
    u8  USBTMC_status;
}__attribute__((packed));


/*! MsgID OUT   -- Host-to-device--*/
/* Table 2, MsgId values */
#define USB_TMC_MSGID_DEV_DEP_MSG_OUT		      (1)
#define USB_TMC_MSGID_REQUEST_DEV_DEP_MSG_IN	  (2)
/* 3-125 Reserved for USBTMC */
#define USB_TMC_MSGID_VENDOR_SPECIFIC_OUT		  (126)
#define USB_TMC_MSGID_REQUEST_VENDOR_SPECIFIC_IN  (127)
/* usbtmc 488 */
#define USB_TMC_MSGID_TRIGGER 128
/* 128-255 Reserved for USBTMC subclass and VISA */

/*! MsgID IN   --Device-to-Host--*/
/* Table 2, MsgId values */
#define USB_TMC_MSGID_DEV_DEP_MSG_IN              (2)
#define USB_TMC_MSGID_VENDOR_SPECIFIC_IN          (127)
void usbTmcMsgidStateMachine(int usbDriveFd);


/*! USB requests values */
#define USB_REQ_GET_STATUS		   0x00
#define USB_REQ_CLEAR_FEATURE	   0x01
#define USB_REQ_SET_FEATURE		   0x03
#define USB_REQ_SET_ADDRESS		   0x05
#define USB_REQ_GET_DESCRIPTOR	   0x06  //!总线复位
#define USB_REQ_SET_DESCRIPTOR	   0x07
#define USB_REQ_GET_CONFIGURATION  0x08
#define USB_REQ_SET_CONFIGURATION  0x09
#define USB_REQ_GET_INTERFACE	   0x0A
#define USB_REQ_SET_INTERFACE	   0x0B
#define USB_REQ_SYNCH_FRAME		   0x0C
#define USB_REQ_SET_SEL			   0x30
#define USB_REQ_SET_ISOCH_DELAY	   0x31

/*! USBTMC requests values */
//TMC
#define USBTMC_REQUEST_INITIATE_ABORT_BULK_OUT		(1)
#define USBTMC_REQUEST_CHECK_ABORT_BULK_OUT_STATUS	(2)
#define USBTMC_REQUEST_INITIATE_ABORT_BULK_IN		(3)
#define USBTMC_REQUEST_CHECK_ABORT_BULK_IN_STATUS	(4)
#define USBTMC_REQUEST_INITIATE_CLEAR				(5)
#define USBTMC_REQUEST_CHECK_CLEAR_STATUS			(6)
#define USBTMC_REQUEST_GET_CAPABILITIES				(7)
#define USBTMC_REQUEST_INDICATOR_PULSE				(64)
/* usbtmc 488*/
#define USBTMC_REQUEST_READ_STATUS_BYTE  			(128)
#define USBTMC_REQUEST_REN_CONTROL  			    (160)
#define USBTMC_REQUEST_GO_TO_LOCAL       			(161)
#define USBTMC_REQUEST_LOCAL_LOCKOUT  			    (162)

void usbTmcRequestsStateMachine(int usbDriveFd);


/*! USBTMC status values*/
/* tmc */
#define USBTMC_STATUS_SUCCESS						(0x01)
#define USBTMC_STATUS_PENDING						(0x02)
#define USBTMC_STATUS_FAILED						(0x80)
#define USBTMC_STATUS_TRANSFER_NOT_IN_PROGRESS		(0x81)
#define USBTMC_STATUS_SPLIT_NOT_IN_PROGRESS			(0x82)
#define USBTMC_STATUS_SPLIT_IN_PROGRESS				(0x83)
/* usbtmc 488 */
#define USBTMC_STATUS_INTERRUPT_IN_BUSY				(0x20)



/**************************************自定义的类型或宏 不在协议范围内*****************************************************/
/*! 传输状态控制字类型*/
typedef enum
{
    transferIdle,      //传输空闲
    transferAbort,     //传输被终止
    transferProgress,  //正在进行

}transfer_status;


/*! USBTMC 状态数据结构*/
class  usbtmcStatus
{
public:
    usbtmcStatus(){}

private:
    /*! 批量传输状态*/
    struct  usbtmc_bulk_pipe_status
    {
        u8          	  bTag;            // 传输标签
        transfer_status   BXFStatus;       // 当前状态
        u32         	  TransferedCount; // 已经传输的数据字节数
    }__attribute__((packed));

private:
    usbtmc_bulk_pipe_status    bulkin;   // Bulk-IN  状态
    usbtmc_bulk_pipe_status    bulkout;  // Bulk-OUT 状态

public:
    void set_BulkInBXFStatus(const transfer_status value);
    void add_BulkInTransferedCount(const u32 value);
    transfer_status get_BulkInBXFStatus();
    u32  get_BulkInTransferedCount();
    void clear_BulkInTransferedCount();

    void set_BulkOutBXFStatus(const transfer_status value);
    void set_BulkOutTransferedCount(const u32 value);
    transfer_status get_BulkOutBXFStatus();
    u32  get_BulkOutTransferedCount();
    void clear_BulkOutTransferedCount();


    u8 get_BulkInStatus(int usbDriveFd);
    u8 get_BulkOutStatus(int usbDriveFd);
    u8 get_BulkCtlStatus(int usbDriveFd);
    u8 get_BulkInterruptStatus(int usbDriveFd);
    u8 get_Status(int usbDriveFd);


    void set_BulkInbTag(const u8 value);
    u8   get_BulkInbTag();

    void set_BulkOutbTag(const u8 value);
    u8   get_BulkOutbTag();
};


/*! 通知上层 usb链接或断开 */
void notifyUsbLinkStatus(bool status);

#endif // USBTMCMSG_H
