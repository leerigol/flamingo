#ifndef DATATYPE_H
#define DATATYPE_H
#include <stdio.h>
#if 0
#define USBTMC_DEBUG(fmt,...)  do{\
                                  printf("%s %u:",__FILE__,__LINE__);\
                                  printf(fmt,##__VA_ARGS__);\
                                 printf("\n");}while(0)
#else
#define USBTMC_DEBUG(fmt,...)
#endif

#if 1
#define USBTMC_WARININ(fmt,...)  do{\
                                  printf("%s %u warning :",__FILE__,__LINE__);\
                                  printf(fmt,##__VA_ARGS__);\
                                  printf("\n");}while(0)

#define USBTMC_ERROR(fmt,...)  do{\
                                  printf("%s %u error:",__FILE__,__LINE__);\
                                  printf(fmt,##__VA_ARGS__);\
                                  printf("\n");}while(0)
#else
#define USBTMC_DEBUG(fmt,...)
#define USBTMC_DEBUG(fmt,...)
#endif

#define USBTMC_ENTER() USBTMC_DEBUG("------enter------")
#define USBTMC_EXIT()  USBTMC_DEBUG("------exit-------")

typedef char            s8;
typedef short           s16;
typedef int             s32;
typedef unsigned char 	u8;
typedef unsigned int  	u32;
typedef unsigned short	u16;


enum USBTMCERROR
{
    NONE_ERR,
    TX_BUFF_EMPTY,    //!tx 缓冲区空
    RX_BUFF_EMPTY,    //!rx 缓冲区空
    EVENT_QUEUE_FULL, //!事件 队列 满
};

#endif
