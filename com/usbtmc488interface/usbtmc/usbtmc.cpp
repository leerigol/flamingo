#include "usbtmc.h"
#include "usb.h"
#include "usbtmcmsg.h"

#include <signal.h>
#include <stdlib.h>     //!system(const char *string) 执行参数string字符串所代表的命令
#include <string.h>
#include <fcntl.h>      //!文件操作
#include <unistd.h>      //!getpid/getppid系统调用

#include <errno.h>
#define USB_TMC_SIG      (SIGRTMIN + 1)

sem_t  semControl;       /*! 控制传输     信号量*/
sem_t  semBulk;          /*! bulk传输    信号量*/
usbtmc_info usb_tmc_info ;
static void* usbtmc_Control_thread(void */*ptr*/);         //!控制传输处理线程
static void* usbtmc_bulk_thread(void */*ptr*/);            //!批量传输处理线程

/*!
 * \brief usb_tmc_info_init
 */
void usb_tmc_info_init()
{
    usb_tmc_info.ps8Devname        = "/dev/usbtmc_dev";                      //!usbtmc的驱动文件
    usb_tmc_info.usb_fd            = -1;
    usb_tmc_info.drive_mutex  = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;  //!静态的初始化互斥锁
}

/*!
 * \brief create_tmc_thread 创建tmc线程
 * \param prio              优先级
 * \param func              线程执行函数
 * \return                  线程标识符
 */
pthread_t create_tmc_thread(int prio, pthread_func func)
{
    pthread_t          thread_id;              //! 指向线程标识符。
    pthread_attr_t     attr;                   //! 线程属性
    struct sched_param param;                  //! 调度策略

    pthread_attr_init(&attr);                  //!线程属性初始化
    pthread_attr_getschedparam(&attr, &param); //!得到线程的调度策略
    param.sched_priority = prio;               //!设置优先级
    pthread_attr_setschedparam(&attr, &param); //!设置线程的调度策略

    pthread_create(&thread_id, &attr, func, NULL);//!线程创建

    pthread_attr_destroy(&attr);               //!对线程属性去除初始化

    return thread_id;
}

/*!
 * \brief usbtmc_signal_handler  TMC信号处理函数
 * \param signum                 信号量
 * * 当收到来自USBTMC驱动的信号量时
 * * 对usb_tmc_sem.semTmc信号量执行post操作(+1)
 * * 相当于激活了一个等待usb_tmc_sem.semTmc信号量的进程
 */
static void usbtmc_signal_sigaction(int signum, siginfo_t */*pInfo*/,void */*pReserved*/)
{
    if(signum != USB_TMC_SIG)
    {
        USBTMC_ERROR("signum ！= USB_TMC_SIG");
        return;
    }

    SigType  signalType = SIG_NONE;
    ioctl(usb_tmc_info.usb_fd, USBDEVTMC_IOCTL_READ_SIGNAL_TYPE, &signalType); //读取端点类型

    switch(signalType)
    {
    case SIG_IN_EP0:
        sem_post(&semControl);
        USBTMC_DEBUG("%d, %ld, %ld", signalType, semControl.__align, semBulk.__align);
        break;
    case SIG_IN_BULIK_IN:
        sem_post(&semBulk);
        USBTMC_DEBUG("%d, %ld, %ld", signalType, semControl.__align, semBulk.__align);
        break;
    case SIG_LINK:
        notifyUsbLinkStatus(true);
        break;
    case SIG_UN_LINK:
        notifyUsbLinkStatus(false);
        break;
    default:
        break;
    }
}

/*!
 * \brief usbtmc_device_init
 * \param pu8ProductDesc
 * \param pu8SerialNum
 * \param productid
 * \param fetch
 */
void usbtmc_device_init(u8* pu8ProductDesc, u8 * pu8SerialNum, u16 productid )
{
    //!初始化usbtmc的info
    usb_tmc_info_init();

    //!bulk传输信号
    sem_init(&semBulk,0,0);
    //!控制传输信号
    sem_init(&semControl,0,0);

    //!创建tmc控制传输处理线程  设置优先级10
    create_tmc_thread(10, &usbtmc_Control_thread);
    //!创建tmc批量传输处理线程  设置优先级10
    create_tmc_thread(10, &usbtmc_bulk_thread);

/*!因为每台机器有不同的序列号，所以需要动态获取SerialNum */
#ifndef _SIMULATE
    s8 cmdline[256] = {0};
    system("insmod /rigol/drivers/libcomposite.ko");
    sprintf(cmdline,"insmod /rigol/drivers/usbtmc_dev.ko iSerialNum=%s iProductDesc=%s ProductId=%d",pu8SerialNum,pu8ProductDesc,productid);
    //printf("cmdline:%s\n",cmdline);
    system(cmdline);
#endif

    //!open the USB device.
    int fd = open((const s8*)usb_tmc_info.ps8Devname, O_RDWR/*|O_NONBLOCK|O_NDELAY*/);

    if (fd < 0)
    {
        USBTMC_ERROR("can not open %s,fd:%d\n",usb_tmc_info.ps8Devname,fd);
        usb_tmc_info.usb_fd = -1;
        return ;
    }
    else
    {
        USBTMC_DEBUG("open usb_tmc_info ok  -- file: %s,fd:%d",usb_tmc_info.ps8Devname,fd);
        usb_tmc_info.usb_fd = fd;
    }

    //!信号绑定
    struct sigaction saio;

    //!sigaction结构初始化
    saio.sa_sigaction  = usbtmc_signal_sigaction; //!信号处理函数
    saio.sa_flags       = SA_SIGINFO;             //！指定sa_sigaction函数指针有效
    sigemptyset(&saio.sa_mask);
    saio.sa_restorer    = NULL;

    //!sigaction函数的功能是检查或修改与指定信号相关联的处理动作（或同时执行这两种操作）。
    sigaction(USB_TMC_SIG, &saio, NULL);
    //!将信号与本进程绑定;   getpid():取得进程识别码 getppid():取得父进程识别码
    fcntl(usb_tmc_info.usb_fd, F_SETOWN, getpid());
    //!切换到设备（fd）的异步操作模式
    fcntl(usb_tmc_info.usb_fd, F_SETFL , FASYNC|fcntl(usb_tmc_info.usb_fd, F_GETFL));
    //!设置标识输入输出可进行的信号
    fcntl(usb_tmc_info.usb_fd, F_SETSIG, USB_TMC_SIG);

    return;
}



static void* usbtmc_bulk_thread(void *)
{
    int err = 0;
    while(1)
    {
        err = sem_wait(&semBulk);

        if( 0 == err )
        {
            usbTmcMsgidStateMachine(usb_tmc_info.usb_fd/*, usbTmcHead*/); //!消息处理
        }
        else if( errno != EINTR)
        {
            USBTMC_WARININ("%d, %s",errno, strerror(errno) );
        }
    }
    return NULL;
}

static void* usbtmc_Control_thread(void *)
{
    int err = 0;
    while(1)
    {
        err = sem_wait(&semControl);
        if( 0 == err )
        {
            usbTmcRequestsStateMachine(usb_tmc_info.usb_fd/*, usbRequests*/); //!请求处理
        }
        else if( errno != EINTR)
        {

            USBTMC_WARININ("%d, %s",errno, strerror(errno) );
        }
    }
    return NULL;
}


/*!
 * \brief StbUsbTmcIrq STB事件触发的中断
 * \param status     STB寄存器的值
 * \return
 */
void StbUsbTmcInterrupt(u8 status)
{
    usb_tmc_interruot_in_setup_packet  InterruptPacket;
    InterruptPacket.bNotify1.Bitmap = 0x81;
    InterruptPacket.StatusByte      = status;

    usbInterruptWrite(usb_tmc_info.usb_fd, (u8*)&InterruptPacket);
}
