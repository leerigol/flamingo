QT += core
QT -= gui

TARGET = usbtmc
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  \
    usbtmc.cpp \
    usb.cpp \
    usbtmcmsg.cpp \
    usb488ioControl.cpp

HEADERS += \
    usbtmc.h \
    DataType.h \
    usb.h \
    usbtmcmsg.h \
    usb488ioControl.h

#staticlib
TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


#***********************************************************#
#工程配置
#***********************************************************#
# *1* 仅用debug模式构建
CONFIG  -= debug_and_release
CONFIG  -= release
CONFIG  -= debug_and_release_target
CONFIG  -= build_all
CONFIG  += debug

# *2* 中间文件(.o文件)路径
#OBJECTS_DIR = ../build/usbtmc
#MOC_DIR     = ../build/usbtmc

# *3* 可执行文件(.exe文件)路径
TARGET = ../../../lib$$(PLATFORM)/scpi/usbtmc
