#ifndef USBTMC
#define USBTMC
#include "DataType.h"
#include <pthread.h>    /*! 线程头文件   */
#include <semaphore.h>  /*! 信号量头文件 */
#include <queue>        /*! 队列  头文件 */

//!线程函数函数类型
typedef void* (*pthread_func)(void *);

//!usbTmc中断模式接收数据的回调函数类型
typedef void (*callbackFunc)(u8* buf, int len);


struct usbtmc_info
{
    const s8*        ps8Devname;
    int              usb_fd;               /*! usb的设备文件句柄 */
    pthread_mutex_t  drive_mutex;          /*! */
};
extern usbtmc_info usb_tmc_info;        //!USBTMC的数据info

/*! public*************************************************/

USBTMCERROR usbTmcPushEvent(u8 event); //!加入USBTMC事件

void usbtmc_device_init(u8* pu8ProductDesc, u8 * pu8SerialNum, u16 productid);

void StbUsbTmcInterrupt(u8 status); //!STB寄存器事件触发的中断

#endif // USBTMC

