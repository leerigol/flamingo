#ifndef USB_H
#define USB_H
#include "DataType.h"
#include <unistd.h>
#include <sys/ioctl.h>

u32    usbWrite(int fd, u8 *pBuf, u32 u32Length);
size_t usbRead(int fd, u8 *pBuf, u32 u32Length);
size_t usbReadEndpoint(int fd, u8 *pBuf, u8);


void  usbInterruptWrite(int fd, u8 *pBuf);

void  usbControlEndpointWrite(int fd, u8* pBuf, u8 u8Length);

void  usbIoctlCmdWrite(int fd, u8 cmd);

void usbIoctlWriteRead(int fd, u8 cmd, void *p = NULL);


/*! ioctl cmd */
#define USBDEVTMC_IOCTL_DEFAULT							(0)				// 默认
#define USBDEVTMC_IOCTL_TRANS_READSTATUS				(1)				// 设置 ReadStatus
#define USBDEVTMC_IOCTL_GET_CAPABILITIES				(2)				// 获取 Capabilities
#define USBDEVTMC_IOCTL_INDICATOR_PULSE					(3)
#define USBDEVTMC_IOCTL_INITIATE_CLEAR					(4)
#define USBDEVTMC_IOCTL_ABORT_BULK_OUT					(5)				// 终止Bulkout传输
#define USBDEVTMC_IOCTL_ABORT_BULK_IN					(6)				// 终止Bulkin传输
#define USBDEVTMC_IOCTL_SET_BULK_OUT_STATUS             (7)				// 设置Bulkout传输的状态
#define USBDEVTMC_IOCTL_SET_BULK_IN_STATUS              (8)				// 设置BUlkin传输的状态
#define USBDEVTMC_IOCTL_SET_INITIATE_CLEAR_STATUS       (9)
#define USBDEVTMC_IOCTL_RESET_CONF						(10)
#define USBDEVTMC_IOCTL_GET_CTL_STATUS					(11)			// 获取 控制传输 请求类型
#define USBDEVTMC_IOCTL_SET_BULKOUT_TAGID				(12)			// 设置Bulkout传输消息头ID，供控制传输使用
#define USBDEVTMC_IOCTL_SET_BULKIN_TAGID				(13)			// 设置Bulkin传输消息头ID，供控制传输使用

/**********************/
#define USBDEVTMC_IOCTL_CONTROL_WRITE					(15) //控制write的目标端点是控制端点
#define USBDEVTMC_IOCTL_INTERRUPT_WRITE					(16) //控制write的目标端点是中断端点
#define USBDEVTMC_IOCTL_READ_ENDPOINT_DATA	            (17) //控制read的目标端点是　端点０
#define USBDEVTMC_IOCTL_READ_SIGNAL_TYPE	            (18) //读取驱动的信号类型
#define USBDEVTMC_IOCTL_CLEAR_BULK_OUT					(19) //清除bulk-out缓冲
#define USBDEVTMC_IOCTL_CLEAR_BULK_IN					(20) //清除bulk-in 缓冲
#define USBTMC_SCPI_INT_TRANS_RESULT	                (21) //查询中断传输发送状态
#define USBTMC_SCPI_HARDWARE_STATE                      (22) //查询链接状态 (断开或链接)
#define USBTMC_SCPI_CHECK_BULK_IN_STATUS                (23) //查询 bulk-in端点状态
#define USBTMC_SCPI_CHECK_BULK_OUT_STATUS               (24) //查询 bulk-out端点状态
#define USBTMC_SCPI_CHECK_BULK_CTL_STATUS               (25) //查询 控制端点状态
#define USBTMC_SCPI_CHECK_BULK_INTERRUPT_STATUS         (26) //查询 中断端点状态


typedef enum
{
    SIG_NONE,
    SIG_IN_BULIK_IN,    //bulik IN 收到数据
    SIG_IN_EP0,         //ep0      收到数据
    SIG_LINK,           //         新连接
    SIG_UN_LINK,        //         断开链接
}SigType;



#endif // USB_H
