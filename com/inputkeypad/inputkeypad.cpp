#include <QPainter>
#include "inputkeypad.h"
#include "ui_inputkeypad.h"

#include "../../include/dsostd.h"
#include "../../gui/cguiformatter.h"

#include "../../menu/event/reventfilter.h"
#include "../../menu/rmenus.h"

#include "../../arith/xwnd.h"

static InputKeypad* instance = 0;
InputKeypad* InputKeypad::getInstance(QWidget *parent)
{
    if(0 == instance)
    {

        parent = sysGetMainWindow();

        instance = new InputKeypad(parent);

        qRegisterMetaType<QString>("QString&");
    }
    instance->setVisible(false);

    return instance;
}

void InputKeypad::rst()
{
    if ( 0 != instance )
    {
        instance->disconnect();
        instance->mLinkKey = -1;
    }
}

InputKeypad::KeypadView InputKeypad::toKeypadView( Unit unit )
{
    switch( unit )
    {
        case Unit_W: return WattUnit;
        case Unit_A: return AmpereUnit;
        case Unit_V: return VoltUnit;
        case Unit_U: return UnkUnit;

        case Unit_s:        return TimeUnit;
        case Unit_hz:       return FreqUnit;
        case Unit_degree:   return Degree;
        case Unit_percent:  return Percent;

        case Unit_db:   return dBUnit;
        case Unit_dbm:  return dBUnit;

        case Unit_VmulS:    return UnkUnit;
        case Unit_VdivS:    return UnkUnit;
        case Unit_Vrms:     return UnkUnit;
        case Unit_oum:      return OumUnit;

        case Unit_Div:  return DivUnit;
        case Unit_SaS:  return UnkUnit;
        case Unit_Pts:  return UnkUnit;
        case Unit_Pts_: return UnkUnit;

        case Unit_VA:   return UnkUnit;
        case Unit_VAR:  return UnkUnit;

        default:    return UnkUnit;
    }
}

InputKeypad::InputKeypad(QWidget *parent) :
    menu_res::RPopMenuChildWnd(parent),
    ui(new Ui::InputKeypad)
{
    ui->setupUi(this);

    connect(ui->button_0, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_1, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_2, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_3, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_4, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_5, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_6, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_7, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_8, SIGNAL(released()),this, SLOT(buttonReleased()));
    connect(ui->button_9, SIGNAL(released()),this, SLOT(buttonReleased()));

    this->setTabOrder(ui->button_1, ui->button_2);
    this->setTabOrder(ui->button_2, ui->button_3);
    this->setTabOrder(ui->button_3, ui->button_u1);
    this->setTabOrder(ui->button_u1, ui->button_back);
    this->setTabOrder(ui->button_back, ui->button_4);
    this->setTabOrder(ui->button_4, ui->button_5);
    this->setTabOrder(ui->button_5, ui->button_6);
    this->setTabOrder(ui->button_6, ui->button_u2);
    this->setTabOrder(ui->button_u2, ui->button_max);
    this->setTabOrder(ui->button_max, ui->button_7);
    this->setTabOrder(ui->button_7, ui->button_8);
    this->setTabOrder(ui->button_8, ui->button_9);
    this->setTabOrder(ui->button_9, ui->button_u3);
    this->setTabOrder(ui->button_u3, ui->button_min);
    this->setTabOrder(ui->button_min, ui->button_dot);
    this->setTabOrder(ui->button_dot, ui->button_0);
    this->setTabOrder(ui->button_0, ui->button_sub);
    this->setTabOrder(ui->button_sub, ui->button_u4);
    this->setTabOrder(ui->button_u4, ui->button_default);
    this->setTabOrder(ui->button_default, ui->button_exp);
    this->setTabOrder(ui->button_exp, ui->button_enter);
    this->setTabOrder(ui->button_enter, ui->button_u5);
    this->setTabOrder(ui->button_u5, ui->button_cls);

    init();

    addTuneKey();
    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

InputKeypad::~InputKeypad()
{
    delete ui;
}

void InputKeypad::init()
{
    ui->inputLabel->setFocusPolicy(Qt::ClickFocus);

    loadResource("keyPad/");
}

/// @details 显示数字键盘面板
/// @param id：系统参数ID；keypadType：单位类型；x：横坐标；y：纵坐标
/// @return
/// @see
/// @note
void InputKeypad::showInputKeypad( KeypadView keypadType, int x, int y)
{
    this->move(x, y);

    setResetStatus();
    ui->button_dot->setText(".");
    ui->button_dot->setEnabled(true);
    ui->button_exp->setText("Exp");
    ui->button_exp->setEnabled(true);
//    ui->button_sub->setText("-");
//    ui->button_sub->setEnabled(true);
    m_nPadType      = keypadType;

    m_UnitStr    = "";
    m_UnitButton = "";

    ui->button_sub->setText("+/-");
    switch (keypadType)
    {
        case FreqUnit:
            ui->button_u1->setText("MHz");
            ui->button_u2->setText("kHz");
            ui->button_u3->setText("Hz");
            ui->button_u4->setText("mHz");
            ui->button_u5->setText("uHz");            
            m_UnitStr = "Hz";
            break;
        case TimeUnit:
            ui->button_u1->setText("s");
            ui->button_u2->setText("ms");
            ui->button_u3->setText("us");
            ui->button_u4->setText("ns");
            ui->button_u5->setText("ps");
            m_UnitStr = "s";
            break;
        case VoltUnit:
            ui->button_u1->setText("kV");
            ui->button_u2->setText("V");
            ui->button_u3->setText("mV");
            ui->button_u4->setText("uV");
            ui->button_u5->setText("nV");
            m_UnitStr = "V";
            break;
        case AmpereUnit:
            ui->button_u1->setText("kA");
            ui->button_u2->setText("A");
            ui->button_u3->setText("mA");
            ui->button_u4->setText("uA");
            ui->button_u5->setText("nA");
            m_UnitStr = "A";
            break;
        case UnkUnit:
            ui->button_u1->setText("U");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
            m_UnitStr = "U";
            break;
        case WattUnit:
            ui->button_u1->setText("MW");
            ui->button_u2->setText("kW");
            ui->button_u3->setText("W");
            ui->button_u4->setText("mW");
            ui->button_u5->setText("uW");
            m_UnitStr = "W";
            break;

        case DivUnit:
            ui->button_u1->setText("div");
            ui->button_u2->setText("mdiv");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
//            ui->button_exp->setEnabled(false);
//            ui->button_sub->setText("");
//            ui->button_sub->setEnabled(false);
//            ui->button_dot->setText("");
//            ui->button_dot->setEnabled(false);
            m_UnitStr = "div";
            break;

        case dBUnit:
            ui->button_u1->setText("dB");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");

//            ui->button_exp->setText("");
//            ui->button_exp->setEnabled(false);
            m_UnitStr = "dB";
            break;

        case OumUnit:
            ui->button_u1->setText("Ω");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
//            ui->button_exp->setText("");
//            ui->button_exp->setEnabled(false);
//            ui->button_sub->setText("");
//            ui->button_sub->setEnabled(false);
//            ui->button_dot->setText("");
//            ui->button_dot->setEnabled(false);
            m_UnitStr = "Ω";

            break;

        case Degree:
            ui->button_u1->setText("°");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
//            ui->button_exp->setText("");
//            ui->button_exp->setEnabled(false);
            m_UnitStr = "°";
            break;

        case Percent:
            ui->button_u1->setText("%");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
//            ui->button_exp->setText("");
//            ui->button_exp->setEnabled(false);
//            ui->button_sub->setText("");
//            ui->button_sub->setEnabled(false);
//            ui->button_dot->setText("");
//            ui->button_dot->setEnabled(false);
            m_UnitStr = "%";
            break;

        case NumValue:
            ui->button_u1->setText("M");
            ui->button_u2->setText("k");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
            break;

       case Password:
            ui->button_u1->setText("");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
            ui->button_exp->setText("");
            ui->button_sub->setText("");
            ui->button_dot->setText("");
            break;

        case IPAddress:
            ui->button_u1->setText("");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
            ui->button_exp->setText("");
//            ui->button_exp->setEnabled(false);
            ui->button_sub->setText("");
//            ui->button_sub->setEnabled(false);
            ui->inputLabel->setReadOnly(false);
//            ui->button_left->setEnabled(true);
//            ui->button_right->setEnabled(true);
            break;

        default:
            ui->button_u1->setText("");
            ui->button_u2->setText("");
            ui->button_u3->setText("");
            ui->button_u4->setText("");
            ui->button_u5->setText("");
            break;
    }
    format4Display(mValueGp.vNow);
    this->show();
    ui->inputLabel->setFocus();
}

//! (x,y):global pos of the caller
void InputKeypad::input( int x, int y,
                         KeypadView view,
                         const DsoRealGp &gp,
                         const QSize &size )
{    
    mValueGp = gp;

    QWidget* placeWnd = sysGetMidWindow();
    Q_ASSERT( NULL != placeWnd );
    QPoint pt = sloveDstPt( x, y,
                            size,
                            this->rect().size(),
                            placeWnd->geometry()
                            );

    showInputKeypad( view, pt.x(), pt.y() );
    return;
}

void InputKeypad::input(KeypadView view,
                        const DsoRealGp &gp,
                        const QSize &/*size*/ )
{
    mValueGp = gp;

    int x = 400;
    int y = 200;

    showInputKeypad(view, x, y );
}

void InputKeypad::input( int x, int y,
              KeypadView view,
              const DsoRealGp &gp,
              QObject *pObj,
              const char *slotstring,
              const QSize &size )
{
    Q_ASSERT( NULL != pObj );
    Q_ASSERT( NULL != slotstring );

    InputKeypad::rst();

    //! connect again
    connect( this, SIGNAL(OnOK(DsoReal)),
             pObj, slotstring );

    //! show input
    input( x, y, view, gp, size );
}

void InputKeypad::input(
              const QPoint &pt,
              KeypadView view,
              const DsoRealGp &gp,
              QObject *obj,
              const char *slotstring,
              const QSize &size )
{
    input( pt.x(),
           pt.y(),
           view,
           gp,
           obj,
           slotstring,
           size );
}

void InputKeypad::loadResource( const QString &dir)
{
    r_meta::CRMeta meta;

    meta.getMetaVal( dir + "keypadbg", keypadBg );
}

void InputKeypad::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    QImage img;
    img.load(keypadBg);
    QRect r = rect();
    painter.drawImage(r, img);
}


bool InputKeypad::eventFilter(QObject *obj, QEvent *event)
{
    if(this->isVisible() &&
            event->type() == QEvent::TouchBegin )
    {
        QTouchEvent *tEvt = static_cast<QTouchEvent*>(event);
        Q_ASSERT( NULL != tEvt );
        QList<QTouchEvent::TouchPoint> tPoints = tEvt->touchPoints();
        if ( tPoints.size() > 0  )
        {
            //for bug796
            QPoint ptScreen;
            QPointF scrPos = tPoints[0].normalizedPos();
            ptScreen.setX( scrPos.x() * screen_width );
            ptScreen.setY( scrPos.y() * screen_height );
            emit sig_pressed(ptScreen);
        }
    }
    return RPopMenuChildWnd::eventFilter(obj, event);
}

void InputKeypad::hideEvent(QHideEvent *event )
{
    emit sig_hide();

    menu_res::RPopMenuChildWnd::hideEvent( event );
}

void InputKeypad::tuneInc(int /*keyCnt*/)
{
#if 0
    if(ui->button_u5->isEnabled()){
        if(ui->button_left->isEnabled()){
            if(ui->button_u5->hasFocus())
                ui->button_left->setFocus();
            else
                focusNextChild();
        }else{
            if(ui->button_u5->hasFocus())
                ui->button_max->setFocus();
            else
                focusNextChild();
        }
    }else{
        if(ui->button_left->isEnabled()){
            if(ui->button_exp->hasFocus())
                ui->button_left->setFocus();
            else
                focusNextChild();
        }else{
            if(ui->button_exp->hasFocus())
                ui->button_max->setFocus();
            else
                focusNextChild();
        }
    }
#endif
    if(ui->button_cls->hasFocus() || ui->inputLabel->hasFocus())
        ui->button_1->setFocus();
    else
        focusNextChild();
}

void InputKeypad::tuneDec(int /*keyCnt*/)
{
#if 0
    if(ui->inputLabel->hasFocus()){
        if(ui->button_u5->isEnabled())
            ui->button_u5->setFocus();
        else
            ui->button_exp->setFocus();
    }else if(ui->button_left->isEnabled()){
         if(ui->button_left->hasFocus()){
            if(ui->button_u5->isEnabled())
                ui->button_u5->setFocus();
            else
                ui->button_exp->setFocus();
        }else
            focusPreviousChild();
    }else{
        if(ui->button_max->hasFocus()){
            if(ui->button_u5->isEnabled())
                ui->button_u5->setFocus();
            else
                ui->button_exp->setFocus();
        }else
            focusPreviousChild();
    }
#endif
    if(ui->button_1->hasFocus() || ui->inputLabel->hasFocus())
        ui->button_cls->setFocus();
    else
        focusPreviousChild();
}

void InputKeypad::tuneZ(int /*keyCnt*/)
{
    QPushButton *toolBtn = static_cast<QPushButton *> (focusWidget());
    if(toolBtn->hasFocus() && !ui->inputLabel->hasFocus())
        toolBtn->click();
}

void InputKeypad::setButtonEnable(bool v)
{

    ui->inputLabel->setReadOnly( !v );

}

void InputKeypad::buttonPressed()
{

}

/// @details 数字button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::buttonReleased()
{
    QPushButton *b = static_cast<QPushButton *>(sender());
    b->setFocus();

    //Max inputed chars:18
    int cursor_pos = ui->inputLabel->cursorPosition();
    if(m_InputedStr.length() < Max_Inputed_Char)
    {
        QString str    = sender()->objectName();
        QString number = str.right(1);

        if(m_bInputing && (m_InputedStr != "0" || m_nPadType == Password) )
        {
            m_InputedStr.insert(ui->inputLabel->cursorPosition(), number);
            ui->inputLabel->setText(m_InputedStr);
            cursor_pos++;
        }
        else
        {
            m_InputedStr = number;
            ui->inputLabel->setText(m_InputedStr);
            m_bInputing = true;
            setButtonEnable(m_bInputing);
            cursor_pos = 1;
        }
        checkIP();

    }
    else
    {
        sysShowErr( ERR_IME_LENGTH_OVER );
        return;
    }
//    ui->inputLabel->setFocus();
    ui->inputLabel->setCursorPosition(cursor_pos);
}
/// @details 单位button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_u1_clicked()
{
    ui->button_u1->setFocus();
    if(m_InputedStr.size())
    {
        QPushButton* b = (QPushButton*)sender();
        m_UnitButton = b->text();
        if( m_UnitButton.size() > 0 )
        {
            on_button_enter_clicked();
        }
        return;
    }
    hide();
}
/// @details 单位button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_u2_clicked()
{
    ui->button_u2->setFocus();

    if(m_InputedStr.size())
    {
        QPushButton* b = (QPushButton*)sender();
        m_UnitButton = b->text();
        if( m_UnitButton.size() > 0 )
        {
            on_button_enter_clicked();
            return;
        }
    }
    hide();
}
/// @details 单位button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_u3_clicked()
{
    ui->button_u3->setFocus();

    //if(m_InputedStr != "0")
    if(m_InputedStr.size())
    {
        QPushButton* b = (QPushButton*)sender();
        m_UnitButton = b->text();
        if( m_UnitButton.size() > 0 )
        {
            on_button_enter_clicked();
            return;
        }
    }
    hide();
}
/// @details 单位button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_u4_clicked()
{
    ui->button_u4->setFocus();

    //if(m_InputedStr != "0")
    if(m_InputedStr.size())
    {
        QPushButton* b = (QPushButton*)sender();
        m_UnitButton = b->text();
        if( m_UnitButton.size() > 0 )
        {
            on_button_enter_clicked();
            return;
        }
    }
    hide();
}

void InputKeypad::on_button_u5_clicked()
{
    ui->button_u5->setFocus();

    //if(m_InputedStr != "0")
    if(m_InputedStr.size())
    {
        QPushButton* b = (QPushButton*)sender();
        m_UnitButton = b->text();
        if( m_UnitButton.size() > 0 )
        {
            on_button_enter_clicked();
            return;
        }
    }
    hide();
}

/// @details Set Max value
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_max_clicked()
{
    ui->button_max->setFocus();

    format4Display(mValueGp.vMax);
}

///
/// \brief Set Min Value
///
void InputKeypad::on_button_min_clicked()
{
    ui->button_min->setFocus();

    format4Display(mValueGp.vMin);
}

///
/// \brief Set Current Value
///
//void InputKeypad::on_button_curr_clicked()
//{
////    ui->button_curr->setFocus();

//    format4Display(mValueGp.vNow);
//}

///
/// \brief Set Default Value
///
void InputKeypad::on_button_default_clicked()
{
    ui->button_default->setFocus();

    format4Display(mValueGp.vDef);
}

/// @details 清零button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_cls_clicked()
{
    if(m_nPadType == IPAddress)
    {
        setResetStatus();
        m_InputedStr.clear();
        ui->inputLabel->clear();
        ui->button_cls->setFocus();
    }
    else
    {
        format4Display(mValueGp.vNow);
        ui->inputLabel->clear();
        ui->button_cls->setFocus();
    }
}
/// @details 返回button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_back_clicked()
{
    ui->button_back->setFocus();

    int cursor_pos = ui->inputLabel->cursorPosition();

    if(ui->inputLabel->text() != "0")
    {
        if(m_bInputing)
        {
            if(m_InputedStr.length() == 1 && m_nPadType != Password)
            {
                m_bHasSub = false;
                m_InputedStr = "0";
                cursor_pos = 1;
            }
            else
            {
                if(cursor_pos > 0)
                {
                    cursor_pos--;
                    QString deleted = m_InputedStr.mid(cursor_pos,1);
                    if(deleted.compare(".") == 0)
                    {
                        m_bHasDot = false;
                        m_nDots--;
                    }
                    else if(deleted.compare("-") == 0)
                    {
                        m_bHasSub = false;
                    }
                    else if(deleted.compare("e") == 0)
                    {
                        m_bHasE = false;
                    }
                    m_InputedStr.remove(cursor_pos ,1);                    
               }
            }
        }
        else
        {
            setResetStatus();
            cursor_pos = 1;
        }
        ui->inputLabel->setText(m_InputedStr);
        checkIP();
    }
//    ui->inputLabel->setFocus();
    ui->inputLabel->setCursorPosition(cursor_pos);
}
/// @details 小数点button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_dot_clicked()
{
    if( m_nPadType == Password )
    {
        hide();
        return;
    }

    ui->button_dot->setFocus();

    int cursor_pos = ui->inputLabel->cursorPosition();

    if( !m_bInputing )
    {
        setResetStatus();
        cursor_pos = 1;
    }
    if( !m_bHasE )
    {
        if(this->m_nPadType == IPAddress)
        {
            if(m_nDots < 3 )
            {
                m_nDots++;
                m_InputedStr.insert(cursor_pos++ , ".");
                ui->inputLabel->setText(m_InputedStr);
            }
        }
        else
        {
            if(m_bHasDot == false )
            {
                if(m_InputedStr.length() < Max_Inputed_Char)
                {
                    m_bHasDot = true;
                    m_InputedStr.insert(cursor_pos++, ".");
                    ui->inputLabel->setText(m_InputedStr);
                    setButtonEnable(true);
                }
                else
                {
                    sysShowErr( ERR_IME_LENGTH_OVER );
                    return;
                }
            }
        }
        checkIP();
    }
//    ui->inputLabel->setFocus();
    ui->inputLabel->setCursorPosition(cursor_pos);
}
/// @details 负号button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_sub_clicked()
{
    if( m_nPadType == IPAddress ||
        m_nPadType == Password )
    {
        hide();
        return;
    }
    ui->button_sub->setFocus();

    if( !m_bInputing )
    {
        setResetStatus();
    }

    if(m_InputedStr.endsWith("e"))
    {
        if(m_InputedStr.length() < Max_Inputed_Char)
        {
            if(m_InputedStr == "0")
            {
                m_InputedStr = "";
            }
            m_InputedStr = m_InputedStr+"-";
            ui->inputLabel->setText(m_InputedStr );
        }
        else
        {
            sysShowErr( ERR_IME_LENGTH_OVER );
            return;
        }
    }
    else
    {
        if(m_bHasSub == false)
        {
            if(m_InputedStr.length() < Max_Inputed_Char)
            {
                m_bHasSub = true;
                if(m_InputedStr == "0")
                {
                    m_InputedStr = "";
                }
                m_InputedStr = "-" + m_InputedStr;
                ui->inputLabel->setText(m_InputedStr );
            }
            else
            {
                sysShowErr( ERR_IME_LENGTH_OVER );
                return;
            }
        }
        else
        {
            m_bHasSub = false;
            if(m_InputedStr.startsWith("-"))
            {
                m_InputedStr.remove(0,1);
                ui->inputLabel->setText(m_InputedStr);
            }
        }
    }


//    ui->inputLabel->setFocus();
}

void InputKeypad::on_button_exp_clicked()
{
    if( m_nPadType == IPAddress ||
        m_nPadType == Password)
    {
        hide();
        return;
    }
    ui->button_exp->setFocus();

    if(m_bInputing && m_bHasE == false)
    {
        if(m_InputedStr.length() < Max_Inputed_Char)
        {
            m_bHasE = true;
            if(m_InputedStr.right(1).compare(".") == 0)
            {
                m_InputedStr += "0";
            }
            m_InputedStr += "e";
            ui->inputLabel->setText(m_InputedStr);
        }
        else
        {
            sysShowErr( ERR_IME_LENGTH_OVER );
            return;
        }
    }
//    ui->inputLabel->setFocus();
}

bool InputKeypad::isDigitStr(QString& src)
{
    QByteArray ba = src.toLatin1();//QString 转换为 char*
    const char *s = ba.data();

    while(*s && *s>='0' && *s<='9') s++;
    if (*s)
    { //不是纯数字
        return false;
    }
    else
    { //纯数字
        return true;
    }
}
///
/// \brief InputKeypad::format4Display
/// \param val
///
void InputKeypad::format4Display(DsoReal &val)
{
    if(m_nPadType == IPAddress)
    {
        DsoRealType_A realA;
        int ip;
        DsoE dummy;
        val.getVal( realA,dummy);
        ip = (int)realA;
        m_InputedStr = QString("%1.%2.%3.%4").arg((ip>>24)&0xff).arg((ip>>16)&0xff).arg((ip>>8)&0xff).arg(ip&0xff);

        ui->inputLabel->setText(m_InputedStr );
        m_bInputing = true;
        setButtonEnable(true);
        ui->inputLabel->setFocus();
    }
    else
    {
       if(m_nPadType == NumValue)
       {
           int value ;
           val.toReal(value);
           //gui_fmt::CGuiFormatter::format(m_InputedStr, value);
           m_InputedStr = QString("%1").arg(value);
       }
       else if( m_nPadType == Password )
       {
           int value ;
           val.toReal(value);
           if( value == -1 )
           {
               m_InputedStr = "";
           }
           else
           {
              m_InputedStr = QString("%1").arg(value);
           }
       }
       else
       {
           float value;
           val.toReal(value);
           gui_fmt::CGuiFormatter::format(m_InputedStr, value);
       }

       ui->inputLabel->setText(m_InputedStr + m_UnitStr);
       ui->inputLabel->selectAll();
       m_bInputing = false;
       setButtonEnable(false);
       ui->inputLabel->setFocus();
    }
}

void InputKeypad::format4Output(QString val)
{
    int iexp  = 0;
    qint64 ibase = 0;
    Q_ASSERT(val.isNull() == false);

    QString inputed = val;
    if(this->m_bHasE || val.contains("e"))
    {
        QStringList inputed_part = val.split("e");
        inputed     = inputed_part.at(0);
        iexp        = inputed_part.at(1).toInt();
    }

    if(this->m_bHasDot || val.contains("."))
    {
        QString base;
        QString exp;

        QStringList val_part = inputed.split(".");

        Q_ASSERT(val_part.count() == 2);

        base = val_part.at(0);


        exp = val_part.at(1);
        QString endStr = exp.right(1);
        if(!isDigitStr(endStr))
        {
            //modified by hxh
            if( m_UnitButton.size() == 0 )
            {
                m_UnitButton = endStr;
            }
            exp.remove(exp.size()-1, 1);
        }
        while(exp.right(1).compare("0") == 0)
        {
            exp.remove(exp.length()-1,1);
        }

        base += exp;
        iexp  += -1 * exp.length();
        ibase  = base.toLongLong();
    }
    else
    {
        QString endStr = inputed.right(1);
        if(!isDigitStr(endStr))
        {
            //modified by hxh
            if( m_UnitButton.size() == 0 )
            {
                m_UnitButton = endStr;
            }
            inputed.remove(inputed.size()-1, 1);
        }

        //12500 -> 125E2;
        while( inputed.right(1).compare("0") == 0 )
        {
            iexp++;
            inputed.remove(inputed.length()-1,1);
        }
        ibase = inputed.toLongLong();
    }

    int unitExp = getUnitExp( m_UnitButton );
    iexp  += unitExp;
    m_Output.setVal( ibase, (DsoE)iexp);
}
/// @details 回车button槽函数
/// @param
/// @return
/// @see
/// @note
void InputKeypad::on_button_enter_clicked()
{
    if(m_nPadType == IPAddress)
    {
        if(checkIP())
        {
            emit OnOK(m_InputedStr);
        }
    }
    else if( m_nPadType == Password )
    {
        QString ret = "";
        if( m_InputedStr.size() > 0 )
        {
            format4Output(m_InputedStr);
            //! range check
            if( !mValueGp.vMin.isUnk() && m_Output < mValueGp.vMin)
            {
                serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                                       CMD_SERVICE_DO_ERR,
                                       (int)ERR_INVALID_INPUT);
                emit OnOK(ret);
            }
            else if( !mValueGp.vMax.isUnk() && m_Output > mValueGp.vMax)
            {
                serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                                       CMD_SERVICE_DO_ERR,
                                       (int)ERR_INVALID_INPUT);
                emit OnOK(ret);
            }
            else
            {
                emit OnOK( m_InputedStr );
            }
        }
        else
        {
            emit OnOK( ret );
        }
    }
    else
    {
        QString endChar = m_InputedStr.right(1);
        if(endChar.compare("e") == 0 || endChar.compare(".") == 0)
        {
            m_InputedStr += "0";
        }

        format4Output(m_InputedStr);

        //! range check
        if( !mValueGp.vMin.isUnk() && m_Output < mValueGp.vMin)
        {            
            serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                                   CMD_SERVICE_DO_ERR,
                                   (int)ERR_OVER_LOW_RANGE);
            emit OnOK(mValueGp.vMin);
        }
        else if( !mValueGp.vMax.isUnk() && m_Output > mValueGp.vMax)
        {
            serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                                   CMD_SERVICE_DO_ERR,
                                   (int)ERR_OVER_UPPER_RANGE);
            emit OnOK(mValueGp.vMax);
        }
        else
        {
            //qDebug()<<m_Output.mA<<m_Output.mE<<__LINE__;
            emit OnOK(m_Output);
        }
    }

    //! disconnect in hide event
    this->hide();

    disconnect();
}

int InputKeypad::getUnitExp(QString unit)
{
    UnitExp b = Unit_x1;
    if(unit.startsWith("G"))
    {
        b = Unit_G;
    }
    else if(unit.startsWith("M"))
    {
        b = Unit_M;
    }
    else if(unit.startsWith("K", Qt::CaseInsensitive))
    {
        b = Unit_K;
    }
    else if(unit.startsWith("m"))
    {
        b = Unit_m;
    }
    else if(unit.startsWith("u"))
    {
        b = Unit_u;
    }
    else if(unit.startsWith("n"))
    {
        b = Unit_n;
    }
    else if(unit.startsWith("p"))
    {
        b = Unit_p;
    }

    switch(b)
    {
        case Unit_G: return 9;
        case Unit_M: return 6;
        case Unit_K: return 3;
        case Unit_x1:return 0;
        case Unit_m: return -3;
        case Unit_u: return -6;
        case Unit_n: return -9;
        case Unit_p: return -12;
    }
    return 0;
}


void InputKeypad::setResetStatus( void )
{
    if( m_nPadType != Password )
    {
        m_InputedStr    = "0";
    }
    else
    {
        m_InputedStr = "";
    }
    m_bHasDot       = false;
    m_bHasSub       = false;
    m_bHasE         = false;
    m_bInputing     = true;

    m_nDots         = 0;
    setButtonEnable(true);
}

bool InputKeypad::checkIP( void )
{
    if(m_nPadType == IPAddress )
    {
        bool is_valid = false;
        if(m_InputedStr.length() >= 7)
        {
            QStringList iplist = m_InputedStr.split(".");
            if(iplist.count() == 4 )
            {
                is_valid =  true;
                for(int i=0; i<iplist.count(); i++)
                {
                    int val = iplist.at(i).toInt();
                    if(val > 255 )
                    {
                        is_valid = false;
                    }
                    //qDebug() << val;
                }
            }
        }
        if( !is_valid )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    return false;
}

//void InputKeypad::on_button_left_clicked()
//{
//    //qDebug() << ui->inputLabel->cursorPosition();

//    if(ui->inputLabel->cursorPosition() > 0)
//    {
//        ui->inputLabel->setCursorPosition(ui->inputLabel->cursorPosition() - 1);
//    }
//    ui->inputLabel->setFocus();
//}

//void InputKeypad::on_button_right_clicked()
//{
//    //qDebug() << ui->inputLabel->cursorPosition();
//    if(ui->inputLabel->cursorPosition() < ui->inputLabel->text().length())
//    {
//        ui->inputLabel->setCursorPosition(ui->inputLabel->cursorPosition() + 1);
//    }
//    ui->inputLabel->setFocus();
//}


void InputKeypad::on_inputLabel_cursorPositionChanged(int /*arg1*/, int /*arg2*/)
{
}
