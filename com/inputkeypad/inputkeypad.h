#ifndef INPUTKEYPAD_H
#define INPUTKEYPAD_H

#include <QKeyEvent>
#include <QWidget>
#include "../../baseclass/dsoreal.h"

#include "../../menu/wnd/rpopmenuchildwnd.h"

namespace Ui {
class InputKeypad;
}

struct RNumber
{
    RNumber() {base=0; exp=0;}
    qlonglong base;
    qlonglong exp;
};

class InputKeypad : public menu_res::RPopMenuChildWnd
{
    Q_OBJECT

public:
    enum KeypadView
    {
        FreqUnit, //mH,Hz,KHz,MHz,GHz
        TimeUnit, //ps,ns,us,ms,s
        VoltUnit,  //uV,mV,V,KV
        AmpereUnit,
        WattUnit,

        // one unit,5
        OumUnit,
        dBUnit,
        Degree,
        Percent,

        BinValue,   // 0,1,10
        HexValue,   // 0~9, A~F
        NumValue,
        IPAddress,
        DivUnit,
        Password,

        NoUnit,
        UnkUnit,
    };

    enum UnitExp
    {
        Unit_G,
        Unit_M,
        Unit_K,
        Unit_x1,
        Unit_m,
        Unit_u,
        Unit_n,
        Unit_p
    };

    //qlonglong max is 20
    const static int Max_Inputed_Char = 16;

public:
    static InputKeypad *getInstance(QWidget *parent = 0);
    static void rst();

    static KeypadView toKeypadView( Unit unit );
public:
    explicit InputKeypad(QWidget *parent = 0);
    ~InputKeypad();
    void   init();

protected:
    void   showInputKeypad(KeypadView keypadType, int x, int y);

public:
    void   input( int x, int y,
                  KeypadView view,
                  const DsoRealGp &gp,
                  const QSize &callerSize=QSize(0,0) );
    void   input( KeypadView view,
                  const DsoRealGp &gp,
                  const QSize &size=QSize(0,0) );

    void   input( int x, int y,
                  KeypadView view,
                  const DsoRealGp &gp,
                  QObject *obj,
                  const char *slotstring,
                  const QSize &callerSize=QSize(0,0) );

    void   input( const QPoint &pt,
                  KeypadView view,
                  const DsoRealGp &gp,
                  QObject *obj,
                  const char *slotstring,
                  const QSize &callerSize=QSize(0,0) );

protected:
    void    loadResource(const QString &dir );
    virtual void paintEvent( QPaintEvent *event );
    virtual void hideEvent(QHideEvent *event );
    virtual bool eventFilter(QObject *, QEvent *);

    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );
    virtual void tuneZ( int keyCnt );

Q_SIGNALS:
    void sig_hide();
    //Deprecated
    void OnOK(int);
    void OnOK( qlonglong );
    void OnOK(float);
    void OnOK(double);


    void OnOK(QString);
    void OnOK(DsoReal);
    void sig_pressed(QPoint);


protected:
//    virtual void keyPressEvent(QKeyEvent *event) ;
//    virtual void keyReleaseEvent(QKeyEvent *);
private slots:
    void buttonPressed();
    void buttonReleased();
    void on_button_u1_clicked();
    void on_button_u2_clicked();
    void on_button_u3_clicked();
    void on_button_u4_clicked();
    void on_button_u5_clicked();

    void on_button_cls_clicked();

    void on_button_back_clicked();

    void on_button_enter_clicked();

    void on_button_dot_clicked();

    void on_button_sub_clicked();

    void on_button_max_clicked();

    void on_button_min_clicked();

//    void on_button_curr_clicked();

    void on_button_default_clicked();

    void on_button_exp_clicked();


//    void on_button_left_clicked();

//    void on_button_right_clicked();

    void on_inputLabel_cursorPositionChanged(int arg1, int arg2);

public Q_SLOTS:
    //! override
//    void show();
//    void hide();

private:
    void        format4Display(DsoReal &val);
    void        format4Output(QString val);

    int         getUnitExp(QString unit);

    void        setResetStatus(void);
    void        setButtonEnable(bool v);
    bool        isDigitStr(QString &);
    bool        checkIP(void);

    int         getLog(qlonglong);

private:
    QString     keypadBg;

    int         widthPic;

    bool        m_bHasDot;
    bool        m_bHasSub;
    bool        m_bHasE;
    bool        m_bInputing;
    int         m_nDots;

    QString     m_UnitStr;
    QString     m_InputedStr;

    DsoReal     m_Output;
    //! view
    KeypadView  m_nPadType;
    DsoRealGp   mValueGp;
    QString     m_UnitButton;

    bool        isEditDef;

private:
    Ui::InputKeypad *ui;

    QList<QPushButton *>    buttonList;
};

#endif // INPUTKEYPAD_H
