#include "ccrc32.h"

//! static members

namespace com_algorithm
{
bool CCrc32::mbInited = false;
unsigned int CCrc32::mCrcTable[256];
const unsigned int CCrc32::cPOLYNOMIAL;

/*!
 * \brief CCrc32::CCrc32
 */
CCrc32::CCrc32()
{
}

/*!
 * \brief CCrc32::initTable
 * build the table with polynomial
 */
void CCrc32::initTable()
{
    unsigned int c;
    int n, k;

    for ( n = 0; n < 256; n++ )
    {
        c = (unsigned int)n;

        for ( k = 0; k <8; k++ )
        {
            if ( c & 0x01 )
            {
                c = CCrc32::cPOLYNOMIAL ^ ( c>>1);
            }
            else
            {
                c = c >> 1;
            }
        }

        CCrc32::mCrcTable[ n ] = c;
    }
}

/*!
 * \brief CCrc32::crc32
 * \param pStream
 * \param len
 * \return
 */
unsigned int CCrc32::crc32( void *pBuf, int len )
{
    //! check input
    if ( 0 == pBuf )
    {
        return 0xffffffff;
    }

    unsigned char *pStream = (unsigned char*)pBuf;

    //! init table
    if ( !CCrc32::mbInited )
    {
        CCrc32::initTable();
        CCrc32::mbInited = true;
    }

    //! gen crc
    unsigned int ret;
    int i;

    ret = 0xffffffff;

    for ( i = 0; i < len; i++ )
    {
        ret = CCrc32::mCrcTable[ ( pStream[i] ^ ret ) & 0xff ] ^ ( ret >> 8 );
    }

    return ret ^ 0xffffffff;
}
/*!
 * \brief crc32
 * \param pStream
 * \param len
 * \return
 * api for C used
 */
unsigned int crc32( void *pStream, int len )
{
    return CCrc32::crc32( pStream,len);
}

}
