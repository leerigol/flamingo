#ifndef CCRC32_H
#define CCRC32_H

namespace com_algorithm
{

/*!
 * \brief The CCrc32 class
 * calc the CCrc32
 * The polynomial is: 0xEDB88320
 * There is an online website to calculate CRC32 at atool.org.
 * There is no pre-calculated crc table, however it is inited
 * in the first call.
 */
class CCrc32
{
public:
    CCrc32();

private:
    static bool mbInited;
    static unsigned int mCrcTable[256];

    const static unsigned int cPOLYNOMIAL = 0xEDB88320;

private:
    static void initTable();

public:
    //! byte aligned
    static unsigned int crc32( void *pStream, int len );
};

extern "C"
{
unsigned int crc32( void *pStream, int len );
}

}

#endif // CCRC32_H
