#ifndef _LXI_ARCH_H
#define _LXI_ARCH_H

#ifdef __cplusplus
extern "C"{
#endif


#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>
#include <semaphore.h>
#include <pthread.h>
#include "Cc.h"

/******************************************
 * 宏定义
 ******************************************/
/* 设备信息参数的最大字符串长度定义 */
#define LXI_DEVICE_INFO_MAX_LEN 50

typedef struct
{
	s16_t use_dhcp;
	s16_t use_autoip;
	s16_t use_staticip;
	u8_t  mac_addr[6];
	s32_t ipaddr;
	s32_t netmask;
	s32_t gateway;
	s32_t dns;
}net_config_info;

/* 当前网络的状态 */
typedef enum tagLXI_STATUS_EN
{
	LXI_STATUS_UNLINK = 0,    //网络无连接
	LXI_STATUS_CONNECTED,     //网络已连接
	LXI_STATUS_INIT,          //网络初始化
	LXI_STATUS_UNBOUND,       //网络未绑定
	LXI_STATUS_IPCONFLICT,    //网络IP冲突
	LXI_STATUS_BUSY,          //网络繁忙
	LXI_STATUS_CONFIGURED,    //网络已配置
	LXI_STATUS_DHCP_FAILED,   //DHCP分配失败
	LXI_STATUS_INVALID_IP,    //无效的IP
	LXI_STATUS_BUTT,
	LXI_STATUS_IP_LOSE        //当前IP地址失效
}LXI_STATUS_E;

extern LXI_STATUS_E    m_eLxiCurrLanStatus;//记录当前网络状态

/* 网络IP配置模式 */
typedef enum tagLXI_IP_CFG_MODE_EN
{
	LXI_IP_CFG_MODE_NONE = 0,
	LXI_IP_CFG_MODE_DHCP,
	LXI_IP_CFG_MODE_AUTOIP,
	LXI_IP_CFG_MODE_STATIC
}LXI_IP_CFG_MODE_E;

/*
 * LXI网页类型定义
 * */
typedef enum
{
	/* 现有网页类型定义 */
	WELCOME_PAGE = 0,        //欢迎主界面网页
	NETWORK_STATUS_PAGE,     //网络状态界面网页
	NETWORK_SETTING_PAGE,    //网络设置界面网页
	HELP_PAGE,               //帮助界面网页
	SECURITY_PAGE,           //密码修改设置界面网页
	SECURITY_ENTRY_PAGE,     //进入设置界面时输入密码界面网页
	PASSWORD_WRONG_PAGE,     //输入密码错误界面网页
	PASSWORD_SUCCESS_PAGE,   //密码修改成功界面网页
	SETPASSWD_WRONG_PAGE,
	WEB_CONTROL_PAGE,        //web control网页
	WEB_CSS_DEFAULT,
	WEB_CSS_HELP,
	WEB_CSS_NETWORKSETTING,
	WEB_CSS_NETWORKSTATUS,
	WEB_CSS_SECURITY,
	WEB_CSS_WEBCONTROL,
	WEB_CSS_WELCOME,
	IMAGES_APPLY,
	IMAGES_CANCEL,
	IMAGES_MANUFACTRUE_LOGO,
	IMAGES_INSTR_LOGO,
	IMAGES_NAV1,
	IMAGES_NAV1_NO,
	IMAGES_NAV2,
	IMAGES_NAV2_NO,
	IMAGES_NAV3,
	IMAGES_NAV3_NO,
	IMAGES_NAV4,
	IMAGES_NAV4_NO,
	IMAGES_NAV5,
	IMAGES_NAV5_NO,
	IMAGES_NAV6,
	IMAGES_NAV6_NO,
	IMAGES_LXI_LOGO,
	IMAGES_XML,
	IMAGES_XSD,
	/* 后续如有新增网页,在此扩展,最多支持5个扩展 */
	WEB_PAGE_USER_DEFINE1,
	WEB_PAGE_USER_DEFINE2,
	WEB_PAGE_USER_DEFINE3,
	WEB_PAGE_USER_DEFINE4,
	WEB_PAGE_USER_DEFINE5,
	WEB_PAGE_BUTT
}LXI_WEB_PAGE_TYPE_E;

/**************************************************************************
 *函 数 名: LXI_SaveHostName_Callback
 *描    述: 保存主机名到FLASH
 *输入参数: pscHostName，需要保存主机名的首地址；
 *        s32Length，需要保存主机名的字节数
 *输出参数: 无
 *返 回 值: 0，保存正确；错误返回其他值
 *说    明: 长度最大字节数为 LXI_HOST_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_SaveHostName_Callback)(char *pscHostName,int s32Length);

/**************************************************************************
 * 函 数 名: LXI_SaveServiceName_Callback
 * 描    述: 保存服务名到FLASH
 * 输入参数: pscServiceName，需要保存服务名的首地址；
 *            u32Length，需要保存服务名的字节数
 * 输出参数: 
 * 返 回 值: 0，保存正确；错误返回其他值
 * 说    明: 长度最大字节数为 LXI_SERVICE_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_SaveServiceName_Callback)(char *pscServiceName,int u32Length);

/**************************************************************************
 *函 数 名: LXI_SaveUserHostName_Callback
 *描    述: 保存用户设置的主机名到FLASH
 *输入参数: pscHostName，需要保存主机名的首地址；
 *         s32Length，需要保存主机名的字节数
 *输出参数: 无
 *返 回 值: 0，保存正确；错误返回其他值
 *说    明: 长度最大字节数为 LXI_HOST_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_SaveUserHostName_Callback)(char *pscHostName,int s32Length);

/**************************************************************************
 *函 数 名: LXI_SaveUserServiceName_Callback
 *描    述: 保存用户设置的服务名到FLASH
 *输入参数: pscServiceName，需要保存服务名的首地址；
 *         u32Length，需要保存服务名的字节数
 *输出参数: 
 *返 回 值: 0，保存正确；错误返回其他值
 *说    明: 长度最大字节数为 LXI_SERVICE_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_SaveUserServiceName_Callback)(char *pscServiceName,int u32Length);


/**************************************************************************
 *函 数 名: LXI_ReadHostName_Callback
 *描    述: 从FLASH中读取主机名
 *输入参数: None
 *输出参数: pscHostName，读取到的主机名(字符串)
 *           pu32Length，读取到的主机名的字节数
 *返 回 值: 0,成功；其他值，失败
 *说    明: 长度最大字节数为 LXI_HOST_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_ReadHostName_Callback)(char *pscHostName,int *pu32Length);

/**************************************************************************
 *函 数 名: LXI_ReadServiceName_Callback
 *描    述: 从FLASH中读取服务名
 *输入参数: None
 *输出参数: pscServiceName，读取到的服务名(字符串)
 *          pu32Length，读取到服务名的字节数
 *返 回 值: 0,成功；其他值，失败
 *说    明: 长度最大字节数为 LXI_SERVICE_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_ReadServiceName_Callback)(char *pscServiceName,int *pu32Length);


/**************************************************************************
 *函 数 名: LXI_ReadUserHostName_Callback
 *描    述: 从FLASH中读取用户设置的主机名
 *输入参数: None
 *输出参数: pscHostName，读取到的主机名(字符串)
 *            pu32Length，读取到的主机名的字节数
 *返 回 值: 0,成功；其他值，失败
 *说    明: 长度最大字节数为 LXI_HOST_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_ReadUserHostName_Callback)(char *pscHostName,int *pu32Length);

/**************************************************************************
 *函 数 名: LXI_ReadUserServiceName_Callback
 *描    述: 从FLASH中读取用户设置的服务名
 *输入参数: None
 *输出参数: pscServiceName，读取到的服务名(字符串)
 *          pu32Length，读取到服务名的字节数
 *返 回 值: 0,成功；其他值，失败
 *说    明: 长度最大字节数为 LXI_SERVICE_NAME_MAX_LEN
 ****************************************************************************/
typedef int (*LXI_ReadUserServiceName_Callback)(char *pscServiceName,int *pu32Length);

/**************************************************************************
 *函 数 名: LXI_NotifyLxiIdentification_Callback
 *描    述: 通知刷新仪器识别标识
 *输入参数:
 *          参数名         类型           描述
 *输出参数: 无
 *返 回 值: 0,成功；其他值，失败
 *说    明: 项目组需要依据下面规则进行显示(LXI协议规定):
 *          如果Identification没有显示,则向用户显示,且在网络断开连接以前,该标识应该一直存在;
 *          如果Identification已经显示,则取消显示.
 ****************************************************************************/
typedef int (*LXI_NotifyLxiIdentification_Callback)(void);

/**************************************************************************
 *函 数 名: LXI_GetInstrumentInformation_Callback
 *描    述: 返回仪器类型型号信息
 *输入参数: 
 *          参数名         类型           描述
 *          u32MaxLen      unsigned int   每条信息的最大字符长度
 *输出参数: 
 *          参数名               类型           描述
 *          pscManufacturer      char*          生产厂商存放地址
 *          pscSerialNumber      char*          序列号存放地址
 *          pscInstrumentModel   char*          仪器型号存放地址
 *          pscFirmwareRevision  char*          固件版本存放地址
 *返 回 值: 0,成功；其他值，失败
 *说    明: 每条信息最大字符长度为 LXI_DEVICE_INFO_MAX_LEN
 ***************************************************************************/
typedef int (*LXI_GetInstrumentInformation_Callback)(char* pscManufacturer,char* pscSerialNumber,char* pscInstrumentModel,char* pscFirmwareRevision,unsigned int u32MaxLen);

/**************************************************************************
 *函 数 名: LXI_GetLanPassword_Callback
 *描    述: 获取Web登陆密码
 *输入参数: 无
 *          参数名         类型           描述
 *          u32MaxLength   unsigned int   输出最大密码长度
 *输出参数: char* pscPassword  网络密码地址
 *返 回 值: 网络密码的实际长度
 *说    明: 长度最大值为 LXI_PASSWORD_MAX_LEN
 *          项目组需要判断返回的密码长度是否大于 u32MaxLength, 
 *          如果大于则返回失败,同时不应给 pscPassword 赋值,防止越界发生
 ***************************************************************************/
typedef int (*LXI_GetLanPassword_Callback)(unsigned char* pscPassword,unsigned int u32MaxLength);

/**************************************************************************
 * *函 数 名: LXI_DefaultLanPassword_Callback
 * *描    述: 初始化Web登录密码为默认值。
 * *输入参数: 无
 * *输出参数: 
 * *返 回 值: 0,成功；其他值，失败
 * *说    明: 
 * ***************************************************************************/
typedef int (*LXI_DefaultLanPassword_Callback)(void);

/**************************************************************************
 * *函 数 名: LXI_SetLanPassword_Callback
 * *描    述: 设置并保存web登录密码
 * *输入参数: 
 * *          参数名         类型           描述
 * *          pscPassword    char*          密码存放地址
 * *          u32Length      unsigned int   密码长度
 * *输出参数: 无
 * *返 回 值: 0,成功；其他值，失败
 * *说    明: 长度最大值为 LXI_PASSWORD_MAX_LEN
 * ***************************************************************************/
typedef int (*LXI_SetLanPassword_Callback)(unsigned char* pscPassword,unsigned int u32Length);

/**************************************************************************
 *函 数 名: LXI_SetLanRefreshEnable_Callback
 *描    述: 网络状态发生变化时,通知刷新仪器界面
 *输入参数: 
 *          参数名         类型              描述
 *          u32Status
 *          LXI_LAN_UNLINK        0  //网络无连接
 *          LXI_LAN_INIT          1  //网络初始化
 *          LXI_STATUS_CONNECTED  2  //网络已连接
 *          LXI_LAN_IPCONFLICT    3  //网络IP冲突
 *          LXI_LAN_CONFIGURED    4  //网络已配置
 *          LXI_DHCP_FAILED       5  //DHCP分配失败
 *          LXI_LAN_INVALID_IP    6  //无效的IP
 *输出参数: 无
 *返 回 值: 0,成功；其他值，失败
 *说    明: 
 ****************************************************************************/
typedef int (*LXI_SetLanRefreshEnable_Callback)(LXI_STATUS_E u32LanStatus);

/**************************************************************************
 *函 数 名: LXI_StackMemAlloc_Callback
 *描    述: 获取协议栈内部存储空间的首地址(由项目组分配,LXI模块使用,只申请不释放)
 *输入参数: 
 *          参数名         类型              描述
 *          u32MemSize     unsigned int      需要内存大小
 *输出参数: None
 *返 回 值: char * 申请到的内存地址. 如果申请失败,应返回NULL
 *说    明: 最大内存为 LXI_STACK_MEM_MAX_LEN，协议栈分配使用
 ****************************************************************************/
typedef char * (*LXI_StackMemAlloc_Callback)(unsigned int u32MemSize);

/**************************************************************************
 *函 数 名: LXI_MemAlloc_Callback
 *描    述: 申请内存块
 *输入参数: 
 *          参数名         类型              描述
 *          u32MemSize     unsigned int      需要内存大小
 *输出参数: None
 *返 回 值: char * 申请到的内存地址. 如果申请失败,应返回NULL
 *说    明: 
 ****************************************************************************/
typedef char * (*LXI_MemAlloc_Callback)(unsigned int u32MemSize);

/**************************************************************************
 *函 数 名: LXI_MemFree_Callback
 *描    述: 释放申请的内存块
 *输入参数: None
 *输出参数: pscMemAddr     char *            待释放的内存地址
 *返 回 值: 0成功，其他失败
 *说    明: 
 ****************************************************************************/
typedef int (*LXI_MemFree_Callback)(char *pscMemAddr);

/**************************************************************************
 *函 数 名: LXI_RequestToVisitWebPage_Callback
 *描    述: 获取访问指定网页的权限
 *输入参数: 
 *          参数名         类型                 描述
 *          eType          LXI_WEB_PAGE_TYPE_E  待访问的网页
 *输出参数: None
 *返 回 值: 0--表示LXI立即获得访问指定网页的权限; 其他失败,表示网页不可访问
 *说    明: 用于项目组完善网页访问管理,防止读写冲突.访问权限由项目组分配和管理
 *          LXI获得访问权限以后,项目组需要保证在LXI释放访问权限以前,
 *          其他模块不能获得该网页的写权限.
 ****************************************************************************/
typedef int (*LXI_RequestToVisitWebPage_Callback)(LXI_WEB_PAGE_TYPE_E eType);

/**************************************************************************
 *函 数 名: LXI_ReleaseWebPageVisit_Callback
 *描    述: 释放指定网页的访问权限
 *输入参数: 
 *          参数名                 类型              描述
 *          eType          LXI_WEB_PAGE_TYPE_E    要释放的网页
 *输出参数: None
 *返 回 值: 0--成功; 其他失败
 *说    明: 用于项目组完善网页访问管理,防止读写冲突.访问权限由项目组分配和管理
 ***************************************************************************/
typedef int (*LXI_ReleaseWebPageVisit_Callback)(LXI_WEB_PAGE_TYPE_E eType);

/**************************************************************************
 *函 数 名: LXI_RecvLanCommand_Callback
 *描    述: 从网络接收命令写入SCPI解析队列的接口
 *输入参数: 
 *          参数名            类型              描述
 *          pscCommand        char*             命令存放地址    
 *          s32Len                   int               命令的长度
 *          bFinishedFlag          int              结束标记，1，结束；0，未结束
 *          u8Type           unsigned char       接口类型
 *输出参数: 无
 *返 回 值: 0,成功；其他值，失败
 *说    明: 
 ****************************************************************************/
typedef int (*LXI_RecvLanCommand_Callback)(char* pscCommand,unsigned int u32Length,int bFinishedFlag,unsigned char u8Type);

/**************************************************************************
 *函 数 名: LXI_SetTransDataLength_Callback
 *描    述: 向网络发送命令响应前,设置发送长度限制的接口
 *输入参数: 
 *          参数名         类型                     描述
 *         u32DataLength     int                读取数据字节数    
 *         u8Type           unsigned char       接口类型
 *输出参数: 无
 *返 回 值: 0,成功；其他值，失败
 *说    明: 
 ***************************************************************************/
typedef int (*LXI_SetTransDataLength_Callback)(int u32DataLength,unsigned char u8Type);

/************************************************************************
 *函 数 名: LXI_SetTransFlag_Callback
 *描    述: 向命令解析模块写入命令前,设置网络传输标志位
 *输入参数: 
 *          参数名         类型              描述    
 *输出参数: 无
 *返 回 值: 无
 *说    明: 
 ****************************************************************************/
typedef void (*LXI_SetTransFlag_Callback)(unsigned char u8Type);

/**************************************************************************
 * *函 数 名: LXI_PendTransSemp_Callback
 * *描    述: Socket线程挂起,等待命令处理完,再读取数据
 * *输入参数: 
 * *          参数名         类型              描述    
 * *输出参数: 无
 * *返 回 值: 无
 * *说    明: 
 * ***************************************************************************/
typedef void (*LXI_PendTransSemp_Callback)(void);

/**************************************************************************
 *函 数 名: LXI_TransLanData_Callback
 *描    述: 向网络发送命令响应接口
 *输入参数: 
 *          参数名         类型              描述
 *          pscLanData         char*           读取数据存放地址    
 *          u32Length           u32_t           传输数据的最大长度
 *          u8Type           unsigned char       接口类型
 *输出参数: 
 *返 回 值: 读取实际数据长度
 *说    明: 
 ****************************************************************************/
typedef int (*LXI_TransLanData_Callback)(char* pscLanData,unsigned int u32Length,unsigned char u8Type);

/**************************************************************************
 *函 数 名: LXI_GetRspDataTotalLen_Callback
 *描    述: 获取命令响应数据的总长度
 *输入参数: 
 *          参数名         类型              描述 
 *          u8Type           unsigned char       接口类型
 *输出参数: 无
 *返 回 值: 命令响应数据的总长度
 *说    明: 
 ****************************************************************************/
typedef int (*LXI_GetRspDataTotalLen_Callback)(unsigned char u8Type);

/**************************************************************************
 *函 数 名: LXI_GetInterfaceTypeFromSCPI_Callback
 *描    述: 从命令解析模块获取分配的接口类型
 *输入参数: 
 *          参数名         类型              描述 
 *输出参数: 无
 *返 回 值: 命令解析模块分配的接口类型
 *说    明: 
 ****************************************************************************/
typedef char (*LXI_GetInterfaceNumFromSCPI_Callback)(void);
/**************************************************************************
 *函 数 名: LXI_FreeInterface_Callback
 *描    述: 释放当前Socket获得的接口
 *输入参数: 
 *          参数名         类型              描述 
 *           u8Type           unsigned char       接口类型
 *输出参数: 无
 *返 回 值: 命令响应数据的总长度
 *说    明: 
 ****************************************************************************/
typedef void (*LXI_FreeInterface_Callback)(unsigned char u8Type);
/**************************************************************************
 *函 数 名: LXI_GetSocketErrorNumber_Callback
 *描    述: 获取当前建立连接的Socket对应的错误队列中元素个数
 *输入参数: 
 *          参数名                 类型              描述
 *          u8Type                 unsigned char    接口类型
 *输出参数: None
 *返 回 值: 当前错误队列中错误的个数
 *说    明:
 ****************************************************************************/
typedef int (*LXI_GetSocketErrorNumber_Callback)(unsigned char u8Type);

/**************************************************************************
 *函 数 名: LXI_GetCurrInfType_Callback
 *描    述: 获取当前命令解析中正在解析的命令的端口类型
 *输入参数: 
 *          参数名                 类型              描述
 *输出参数: 
 *          参数名                 类型              描述
 *            u8Type              unsigned char *        端口类型
 *返 回 值: 
 *说    明:
 ****************************************************************************/
typedef void (*LXI_GetCurrInfType_Callback)(unsigned char *u8Type);

typedef struct tagLXI_CALLBACK_S
{
	/*主机名、服务名处理*/
	LXI_SaveHostName_Callback          pfSaveHostName;        //保存主机名
	LXI_SaveServiceName_Callback       pfSaveServiceName;     //保存服务名
	LXI_SaveUserHostName_Callback      pfSaveUserHostName;      //保存用户设置的主机名
	LXI_SaveUserServiceName_Callback   pfSaveUserServiceName;   //保存用户设置的服务名
	LXI_ReadHostName_Callback          pfReadHostName;        //读取主机名
	LXI_ReadServiceName_Callback       pfReadServiceName;     //读取服务名
	LXI_ReadUserHostName_Callback      pfReadUserHostName;      //读取用户配置的主机名
	LXI_ReadUserServiceName_Callback   pfReadUserServiceName;   //读取用户配置的服务名
	/*网页刷新使用*/
	LXI_NotifyLxiIdentification_Callback  pfNotifyLxiIdentification;  //LXI Identification功能通知刷新
	LXI_GetInstrumentInformation_Callback pfGetInstrumentInformation; //获取仪器信息
	LXI_GetLanPassword_Callback        pfGetLanPassword;          //获取web登录密码
	LXI_DefaultLanPassword_Callback    pfDefaultLanPassword;  //设置web登陆密码为默认值
	LXI_SetLanPassword_Callback        pfSetLanPassword;          //设置并保存web登录密码
	LXI_SetLanRefreshEnable_Callback   pfSetLanRefreshEnable; //网络状态发生变化时,通知刷新仪器界面
	/*内存及权限的申请*/
	LXI_StackMemAlloc_Callback         pfStackMemAlloc; //申请分配协议栈使用的内存块
	LXI_MemAlloc_Callback              pfMemAlloc;      //申请内存块
	LXI_MemFree_Callback               pfMemFree;       //释放申请的内存块
	LXI_RequestToVisitWebPage_Callback pfRequestToVisitWebPage; //申请web page访问权限
	LXI_ReleaseWebPageVisit_Callback   pfReleaseWebPageVisit;   //释放web page访问权限
	/*与命令解析模块相关的*/
	LXI_RecvLanCommand_Callback        pfRecvLanCommand;      //从网络接收命令写入SCPI解析队列的接口
	LXI_SetTransDataLength_Callback    pfSetTransDataLength;  //向网络发送命令响应前,设置发送长度限制的接口
	LXI_SetTransFlag_Callback          pfSetTransFlag;        //设置网络命令的启动解析
	LXI_PendTransSemp_Callback         pfPendTransSemp;       //挂起Socket线程,等待命令解析的处理完成
	LXI_TransLanData_Callback          pfTransLanData;  //向网络发送命令响应接口
	LXI_GetRspDataTotalLen_Callback    pfGetRspDataTotalLen;  //获取响应数据的总长度
	LXI_GetInterfaceNumFromSCPI_Callback pfGetInterfaceNum;//获取Socket接口的类型
	LXI_FreeInterface_Callback         pfFreeInterface;        //释放当前Socket获取到的接口
	LXI_GetSocketErrorNumber_Callback  pfGetSocketErrorNum;     //获取指定接口的错误个数
	LXI_GetCurrInfType_Callback        pfGetCurrInfType;
}LXI_CALLBACK_S;

extern LXI_CALLBACK_S  m_stLxiCallback;

/**************************************************
 * 函 数 名：lxi_get_ip_addr
 * 描    述：获取本地ip地址
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 **************************************************/
u32_t lxi_get_ip_addr(void);

/*****************************************************
 * 函 数 名：sys_msleep
 * 描    述：系统延时操作
 * 输入参数：
 * 	参数名：time  单位是毫秒
 * 输出参数：
 * 返 回 值：
 *****************************************************/
void sys_msleep(int time);


/*****************************************************
 * 函 数 名：pfGetInstrumentInformation
 * 描    述：获取厂商信息、序列号、设备型号、版本号
 * 输入参数：
 * 	manufacture		厂商信息
 * 	serialNumber		序列号
 * 	instrumentModel		设备型号
 * 	firmwareRevision 	版本号
 * 输出参数：
 * 返 回 值：	
 *****************************************************/ 
void pfGetInstrumentInformation(s8_t *manufacture,s8_t *serialNumber,s8_t *instrumentModel,s8_t *firmwareRevision);


/*****************************************************
 * 函 数 名：lxi_get_cur_lan_status
 * 描    述：获取网络状态
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 *****************************************************/
LXI_STATUS_E lxi_get_cur_lan_status(void);

/**************************************************************************
 * *函 数 名: LXI_GetLanStatus
 * *描    述: 得到当前的网络状态
 * *输入参数: 
 * *          参数名         类型              描述
 * *          
 * *输出参数: 
 * *返 回 值: 当前网络状态
 *            参见 LXI_STATUS_E 定义
 *            *说    明: 
 *            ***************************************************************************/
LXI_STATUS_E LXI_GetLanStatus(void);

#ifdef __cplusplus
}
#endif

#endif

