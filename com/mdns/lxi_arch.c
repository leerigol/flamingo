#include "lxi_arch.h"
//记录当前网络状态
LXI_STATUS_E    m_eLxiCurrLanStatus;
//!lxi相关回调函数表
LXI_CALLBACK_S  m_stLxiCallback;

/**************************************************
 * 函 数 名：lxi_get_ip_addr
 * 描    述：获取本地ip地址
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 **************************************************/
u32_t lxi_get_ip_addr(void)
{
#if 1

    int inet_sock;
    struct ifreq ifr;
    long ip;
    inet_sock = socket(AF_INET, SOCK_DGRAM, 0);

    memset(&ifr,0,sizeof(ifr));
    strcpy(ifr.ifr_name, "eth0"); //uncomment by hxh
//    strcpy(ifr.ifr_name, "ens33");
    if (ioctl(inet_sock, SIOCGIFADDR, &ifr) <  0)
             perror("ioctl-lxi");

    ip = inet_addr(inet_ntoa(((struct sockaddr_in*)&(ifr.ifr_addr))->sin_addr));
    return ip;
#else
    int inet_sock;
    struct ifreq ifr;
    long ip;
    inet_sock = socket(AF_INET, SOCK_DGRAM, 0);
    memset(&ifr,0,sizeof(ifr));
    //strcpy(ifr.ifr_name, "eth0");
    strcpy(ifr.ifr_name, "ens33");
    if (ioctl(inet_sock, SIOCGIFADDR, &ifr) <  0)
        perror("ioctl");
    ip = inet_addr(inet_ntoa(((struct sockaddr_in*)&(ifr.ifr_addr))->sin_addr));
    return ip;
#endif

}

/*****************************************************
 * 函 数 名：sys_msleep
 * 描    述：系统延时操作
 * 输入参数：
 * 	参数名：time  单位是毫秒
 * 输出参数：
 * 返 回 值：
 *****************************************************/
void sys_msleep(int time)
{
    usleep(time*1000);
}

/*****************************************************
 * 函 数 名：pfGetInstrumentInformation
 * 描    述：获取厂商信息、序列号、设备型号、版本号
 * 输入参数：
 * 	manufacture		厂商信息
 * 	serialNumber		序列号
 * 	instrumentModel		设备型号
 * 	firmwareRevision 	版本号
 * 输出参数：
 * 返 回 值：
 *****************************************************/
void pfGetInstrumentInformation(s8_t *manufacture,s8_t *serialNumber,s8_t *instrumentModel,s8_t *firmwareRevision)
{
    s8_t *man = "RIGOL TECHNOLOGIES";
    strcpy(manufacture,man);
    s8_t *ser = "DS2D171400356";
    strcpy(serialNumber,ser);
    s8_t *ins = "DS7000-EDU";
    strcpy(instrumentModel,ins);
    s8_t *fir = "00:03:04:SP2";
    strcpy(firmwareRevision,fir);
}

/*****************************************************
 * 函 数 名：lxi_get_cur_lan_status
 * 描    述：获取网络状态
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 *****************************************************/
LXI_STATUS_E lxi_get_cur_lan_status(void)
{
    return m_eLxiCurrLanStatus;
}

/**************************************************************************
 * *函 数 名: LXI_GetLanStatus
 * *描    述: 得到当前的网络状态
 * *输入参数:
 * *          参数名         类型              描述
 * *
 * *输出参数:
 * *返 回 值: 当前网络状态
 *            参见 LXI_STATUS_E 定义
 *            *说    明:
 ****************************************************************************/
LXI_STATUS_E LXI_GetLanStatus(void)
{
    return lxi_get_cur_lan_status();
}


