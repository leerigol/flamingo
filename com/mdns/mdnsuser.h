#ifndef LXI_MDNSUSER_H
#define LXI_MDNSUSER_H


#include <QObject>
#include <QThread>
#include <QDebug>

class Cmdns:public QThread
{
public:
    Cmdns();
    ~Cmdns();
protected:
    void  virtual run();

public:
    int init(QString serial);
    int setEnable(bool b);
    int setHostName(QString name);
    int setServName(QString name);

    static QString getHostName();
    static QString getServName();

    static int SaveHostName(char *pscHostName, int length);
    static int SaveServiceName(char *pscServiceName, int length);
    static int SaveUsrHostName(char *pscHostName, int length);
    static int SaveUsrServiceName(char *pscServiceName, int length);

    static int ReadHostName(char *pscHostName, int *pLength);
    static int ReadServiceName(char *pscServiceName, int *pLength);
    static int ReadUsrHostName(char *pscHostName, int *pLength);
    static int ReadUsrServiceName(char *pscServiceName, int *pLength);

private:
    QString hostName;
    QString servName;

protected:

};

extern Cmdns mdnsThread;

#endif // MDNSUSER_H
