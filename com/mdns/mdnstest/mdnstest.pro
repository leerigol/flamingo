QT += core
QT -= gui

CONFIG -= app_bundle
TEMPLATE = app

#***********************************************************#
#源文件
#***********************************************************#
INCLUDEPATH += "../"
INCLUDEPATH += .

SOURCES += \
    ../mdns.c \
    ../mdnsuser.cpp \
    ../lxi_arch.c\
       main.cpp

HEADERS += \
    ../Cc.h \
    ../mdns.h \
    ../lxi_arch.h \
    ../mdnsuser.h
#***********************************************************#
#工程配置
#***********************************************************#
# *1* 仅用debug模式构建
CONFIG  -= debug_and_release
CONFIG  -= release
CONFIG  -= debug_and_release_target
CONFIG  -= build_all
CONFIG  += debug

# *2* 中间文件(.o文件)路径
OBJECTS_DIR = ../build/
MOC_DIR     = ../build/
# *3* 可执行文件(.exe文件)路径
TARGET = ../build/mdnstest
