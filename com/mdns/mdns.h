#ifndef _MDNS_API_H_
#define _MDNS_API_H_

#ifdef __cplusplus
extern "C"{
#endif

#if 0
#define mdns_debug(fmt,...)  do{\
                                  printf("%s %u:",__FUNCTION__,__LINE__);\
                                  printf(fmt,##__VA_ARGS__);\
                                  printf("\n");}while(0)
#else
#define mdns_debug(fmt,...)
#endif

#include <stdio.h>
#include <string.h>
#include "Cc.h"


/********************************局部宏定义**********************************/
/****************************************************************************/
#define LXI_MDNS_NAME_MAX_LEN       100//mdns的主机名和服务名最大的长度
#define LXI_MDNS_RECORD_MAX_LEN     300//文本信息记录最大长度
#define LXI_MDNS_PACKET_MAX_LEN     1500//接收的mdns报文最大长度
#define LXI_MDNS_QUEUE_MAX_NUM      20//定义mdns队列的最大宽度
#define LXI_MDNS_HOST_PORT          5353//mDNS标准规定，mDNS报文默认通信端口为5353
#define LXI_MDNS_HOST_IPADDR        0xfb0000e0 //mDNS标准规定，mDNS报文广播的目的地址为224.0.0.251
#define LXI_MDNS_IP_ADDR_LEN        4//mdns主机IP地址的长度
#define LXI_MDNS_PROBE_MIN_NUM      5//mdns探测发送的最小次数
#define LXI_MDNS_PROBE_MAX_NUM      20//mdns探测发送的最大次数
#define LXI_MDNS_PROBE_SUCCESS_NUM  6//mdns探测成功发送的次数

extern char host_name[LXI_MDNS_NAME_MAX_LEN];
#define LXI_MDNS_DEFAULT_NAME       host_name//mdns默认的主机名和服务名

#define DNS_NAME_LABEL_MAX_LEN      63
#define DNS_HEADER_SECTION_LEN      12
#define DNS_MAX_RESPONSE_NUM        20
#define DNS_MAX_QUESTIONS_NUM       10
#define DNS_SERVICE_NAME_MAX_LEN    128

typedef enum
{
    LXI_MDNS_NAME_EM_HOST_SET = 0,  //设置主机名
    LXI_MDNS_NAME_EM_HOST_SAVE,     //保存主机名
    LXI_MDNS_NAME_EM_HOST_TEMP,     //临时主机名
    LXI_MDNS_NAME_EM_HOST_FLASH,    //保存到flash的主机名
    LXI_MDNS_NAME_EM_HOST_LOCAL,    //带.local后缀的主机名
    LXI_MDNS_NAME_EM_SERVER_SET,    //设置服务名
    LXI_MDNS_NAME_EM_SERVER_SAVE,   //保存服务名
    LXI_MDNS_NAME_EM_SERVER_TEMP,   //临时服务名
    LXI_MDNS_NAME_EM_SERVER_FLASH,  //保存到flash的服务名

    LXI_MDNS_NAME_EM_END            //定义结束
}LXI_MDNS_NAME_EM;

typedef enum
{
    LXI_MDNS_STATE_IDLE = 0,      //初始化
    LXI_MDNS_STATE_PROBE = 1,     //探测阶段
    LXI_MDNS_STATE_PRONOUNCE = 2, //成功
    LXI_MDNS_STATE_CONFLICT = 3   //冲突
}LXI_MDNS_STATUS_E;

/**************************************************************************
*函 数 名: lxi_mdns_name_set
*描       述: mdns主机名以及服务名的设置接口
*输入参数: 
*          参数名         类型              描述
*          emType   LXI_MDNS_NAME_EM    要进行设置的名字的类型，见头文件定义
*          ps8Name        s8_t *        要设置名字的地址
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
extern s32_t lxi_mdns_name_set(LXI_MDNS_NAME_EM emType,s8_t *ps8Name);
/**************************************************************************
*函 数 名: lxi_mdns_name_get
*描       述: mdns主机名以及服务名的获取接口
*输入参数: 
*          参数名         类型              描述
*          emType   LXI_MDNS_NAME_EM    要进行获取的名字的类型，见头文件定义
*          ps8Name        s8_t *        要获取名字的地址
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
extern s32_t lxi_mdns_name_get(LXI_MDNS_NAME_EM emType,s8_t *ps8Name);
/**************************************************************************
*函 数 名: lxi_mdns_timer1_set
*描       述: mdns设置计时器1的开关
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

extern s32_t lxi_mdns_timer1_set(s8_t s8Value);

/**************************************************************************
*函 数 名: lxi_mdns_need_init_set
*描       述: mdns设置mdns服务重新初始化
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

extern s32_t lxi_mdns_need_init_set(s8_t s8Value);
/**************************************************************************
*函 数 名: lxi_mdns_is_support_set
*描       述: mdns设置是否支持mdns服务
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

extern s32_t lxi_mdns_is_support_set(s8_t s8Value);
/**************************************************************************
*函 数 名: lxi_mdns_enable_status_set
*描       述: mdns设置mdns服务打开/关闭
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

extern s32_t lxi_mdns_enable_status_set(s8_t s8Value);
/**************************************************************************
*函 数 名: lxi_mdns_enable_status_get
*描       述: mdns获取mdns服务状态
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 1--打开,0--关闭
*说    明: 
***************************************************************************/

extern s8_t lxi_mdns_enable_status_get(void);
/**************************************************************************
*函 数 名: lxi_mdns_reload_defult
*描       述: mdns恢复默认值
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
extern void lxi_mdns_reload_defult(void);

/**************************************************************************
*函 数 名: lxi_mdns_name_case_change
*描       述: 把名字中的大写字符改为小写字符，便于比较
*输入参数: 
*          参数名         类型              描述
*          ps8Name       s8_t *         待修改的名字字段
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
extern void lxi_mdns_name_case_change(s8_t *ps8Name);

/**************************************************************************
*函 数 名: lxi_mdns_init
*描       述:初始化mdns参数
*输入参数:
*输出参数:
*返 回 值:
*说    明:
***************************************************************************/
void lxi_mdns_init(void);

/**************************************************************************
*函 数 名: lxi_mdns_open
*描       述: 创建mdns服务
*输入参数: 
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
extern void lxi_mdns_open(void);

#ifdef __cplusplus
}
#endif


#endif

