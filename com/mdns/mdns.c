#include "mdns.h"
#include "lxi_arch.h"

/****************************************************************************/
/* 主机名 */
char host_name[LXI_MDNS_NAME_MAX_LEN]={0};
/****************************************************************************/
/* 字节翻转的宏定义 */
/* UINT16类型数据的翻转 */
#define LXI_REV_WORD(x) ((u16_t)(((u16_t)(x)>>8)|((u16_t)(x)<<8)))
/* UINT32类型数据的翻转 */
#define LXI_REV_DWORD(x) ((u32_t)((u32_t)(LXI_REV_WORD(x)<<16)|(u32_t)(LXI_REV_WORD((x)>>16))))

/* 判断NAME格式是否为压缩格式 */
#define DNS_IS_NAME_COMPRESS(name) (((name)&0xC0)?1:0)
/* 压缩格式下NAME的位置偏移  */
#define DNS_NAME_OFFSET_IN_COMPRESS(name_compress) (((u16_t)(name_compress))&0x3FFF)

/****************************************************************************/
/*******************************局部枚举定义*********************************/
/****************************************************************************/
typedef enum
{
    LXI_MDNS_EVENT_NULL = 0,       //空事件
    LXI_MDNS_EVENT_HOST_NAME = 1,  //主机名事件
    LXI_MDNS_EVENT_SERV_NAME,      //服务名事件
    LXI_MDNS_EVENT_TIMEOUT,        //超时事件
    LXI_MDNS_EVENT_PACKET_PASER,   //数据包解析
    LXI_MDNS_EVENT_HOST_SERV       //主机服务事件
}LXI_MDNS_EVENT_EM;

typedef enum
{
    LXI_MDNS_QUERY_HOST_ADDR = 1,       //1.查询主机名
    LXI_MDNS_QUERY_SERVER_NAME,         //2.查询服务名
    LXI_MDNS_QUERY_MAIL_DESTINATION,    //3.查询邮箱目的地址，已过时，不再使用
    LXI_MDNS_QUERY_MAIL_FORWARDER,      //4.查询邮箱代收服务器，已过时，不再使用
    LXI_MDNS_QUERY_CHAME,               //5.查询一个规范的别名
    LXI_MDNS_QUERY_SOA,                 //6.标识使用的规范命名
    LXI_MDNS_QUERY_MB,                  //7.查询邮箱的动态域名
    LXI_MDNS_QUERY_MG,                  //8.查询邮箱的成员组织
    LXI_MDNS_QUERY_MR,                  //9.设置邮箱新的域名
    LXI_MDNS_QUERY_NULL,                //10.空的邮箱操作
    LXI_MDNS_QUERY_WKS,                 //11.一个众所周知的服务描述
    LXI_MDNS_QUERY_PTR,                 //12.域名查询
    LXI_MDNS_QUERY_HINFO,               //13.主机信息查询
    LXI_MDNS_QUERY_MINFO,               //14.邮箱信息查询
    LXI_MDNS_QUERY_MX,                  //15.邮件通信
    LXI_MDNS_QUERY_TXT,                 //16.查询文本字符串
    LXI_MDNS_QUERY_AAAA = 28,           //ipv6查询
    LXI_MDNS_QUERY_SRV = 33,            //
    LXI_MDNS_QUERY_AXFR = 252,          //请求广播传输
    LXI_MDNS_QUERY_MAILB,               //一个依赖于邮箱的传输
    LXI_MDNS_QUERY_MAILA,               //邮件代理RRS传输
    LXI_MDNS_QUERY_ALL                  //查询所有的记录信息
}LXI_MDNS_QUERY_EM;

typedef enum
{
    LXI_MDNS_PACKET_HOST,               //生成主机名报文
    LXI_MDNS_PACKET_SERVER              //生成服务名报文
}LXI_MDNS_PACKET_EM;

typedef enum
{
    QR_QUERY = 0,
    QR_RESPONSE
}DNS_HEADER_QR_E;

typedef enum
{
    OPCODE_QUERY = 0,
    OPCODE_IQUERY,
    OPCODE_STATUS
}DNS_HEADER_OPCODE_E;

typedef enum
{
    AA_INVALID = 0,
    AA_VALID
}DNS_HEADER_AA_E;

typedef enum
{
    LXI_MDNS_FOUND_NULL = 0,                    //未发现资源字段
    LXI_MDNS_FOUND_NO_SERVER_HTTP = 1,          //发现HTTP资源字段
    LXI_MDNS_FOUND_NO_SERVER_LXI,               //发现LXI资源字段
    LXI_MDNS_FOUND_NO_SERVER_VXI11,             //发现VXI-11资源字段
    LXI_MDNS_FOUND_NO_SERVER_SCPI,              //发现SCPI资源字段
    LXI_MDNS_FOUND_SERVER_HTTP,                 //发现服务器HTTP资源
    LXI_MDNS_FOUND_SERVER_LXI,                  //发现服务器LXI资源字段
    LXI_MDNS_FOUND_SERVER_VXI11,                //发现服务器VXI-11资源字段
    LXI_MDNS_FOUND_SERVER_SCPI,                 //发现服务器SCPI资源字段
}LXI_MDNS_FOUND_EM;

/****************************************************************************/
/******************************局部结构体定义********************************/
/****************************************************************************/
typedef struct
{
    s8_t                name[LXI_MDNS_NAME_EM_END][LXI_MDNS_NAME_MAX_LEN];  //定义主机名和服务名的保存
    s8_t                httpText[LXI_MDNS_RECORD_MAX_LEN];                  //记录Http文本信息
    s8_t                lxiText[LXI_MDNS_RECORD_MAX_LEN];                   //记录Lxi文本信息
    s8_t                timer0;                                             //mdns定时器0是否启动
    s8_t                timer1;                                             //mdns定时器1是否启动
    s8_t                packet_t;                                           //mdns报文类型 0,配置主机名,1,配置服务名,2,全配置
    s8_t                timer_t;                                            //定时器类型
    s8_t                ip[LXI_MDNS_IP_ADDR_LEN];                           //记录主机IP地址
    s8_t                buff[LXI_MDNS_PACKET_MAX_LEN];                      //mdns发送缓冲区
    s8_t                questions[DNS_MAX_QUESTIONS_NUM][DNS_SERVICE_NAME_MAX_LEN];//问题队列
    s8_t                record[DNS_MAX_RESPONSE_NUM][DNS_SERVICE_NAME_MAX_LEN];//记录队列
    u16_t               query_t[DNS_MAX_QUESTIONS_NUM];                     //查询类型         
    u16_t               resp_t[DNS_MAX_RESPONSE_NUM];                       //响应类型
    s32_t               sockfd;                                             //UDP-Socket的编号
    s32_t               probe_n;                                            //报文探测次数
    s32_t               found_t;                                            //报文资源比对时，记录主机名或服务名冲突情况
    LXI_MDNS_STATUS_E   hostStatus;                                         //主机名服务处于的状态
    LXI_MDNS_STATUS_E   servStatus;                                         //服务名服务处于的状态
}LXI_MDNS_LOG_STRU;

typedef struct
{
    struct mdns_event
    {
        s16_t               front;                           //事件队列的首指针
        s16_t               rear;                            //事件队列的尾指针
        LXI_MDNS_EVENT_EM   queue[LXI_MDNS_QUEUE_MAX_NUM];   //事件队列
        sem_t           sem;                             //事件信号量
    }event;

    struct mdns_recv_queue
    {
        s16_t front;                                         //接收队列的首指针
        s16_t rear;                                          //接收队列的尾指针
        struct mdns_packet
        {
            s8_t  packet[LXI_MDNS_PACKET_MAX_LEN];           //接收包的地址
            s32_t len;                                       //接收数据长度的记录
        }queue[LXI_MDNS_QUEUE_MAX_NUM];                      //接收队列
    }recv;
}LXI_MDNS_QUEUE_STRU;

struct MdnsResource
{
    s8_t *name;
    u16_t type, class;
    u32_t ttl;
    u16_t rdlength;
    s8_t *rdata;
    union 
    {
        struct { u32_t ip; s8_t *name; } a;
        struct { u8_t *name; } ns;
        struct { u8_t *name; } cname;
        struct { u8_t *name; } ptr;
        struct { u16_t priority, weight, port; u8_t *name; } srv;
    } known;
};

struct question
{
    u8_t *name;
    u16_t type, class;
};


typedef struct MdnsMessage
{
    u16_t id;
    struct { u16_t rcode:4,z:3,ra:1,rd:1,tc:1,aa:1,opcode:4,qr:1; } header;
    u16_t qdcount, ancount, nscount, arcount;
    struct question *qd;
    struct MdnsResource *an, *ns, *ar;
}MDNS_MESSAGE_S;


/****************************************************************************/
/*******************************局部变量定义*********************************/
/****************************************************************************/

#ifdef _LXI_USE_MEM_ALLOC_
LXI_MDNS_LOG_STRU   *m_stMdnsLog;
#define LXI_MDNS_LOG_MEMBER_GET(n) m_stMdnsLog->n
LXI_MDNS_QUEUE_STRU *m_stMdnsQueue;
#define LXI_MDNS_QUEUE_GET(n)      m_stMdnsQueue->n
#else
LXI_MDNS_LOG_STRU   m_stMdnsLog ;
#define LXI_MDNS_LOG_MEMBER_GET(n) m_stMdnsLog.n
LXI_MDNS_QUEUE_STRU m_stMdnsQueue;
#define LXI_MDNS_QUEUE_GET(n)      m_stMdnsQueue.n
#endif

s8_t  m_s8MdnsNeedInit     = 0;//mdns重启标识
s8_t  m_s8MdnsIsSupport    = 1;//是否支持mdns服务,默认支持
s8_t  m_s8MdnsEnableStatus = 0;//是否打开mdns服务,默认关闭

/****************************************************************************/
/*******************************函数接口定义*********************************/
/****************************************************************************/

/**************************************************************************
*函 数 名: lxi_mdns_name_set
*描       述: mdns主机名以及服务名的设置接口
*输入参数: 
*          参数名         类型              描述
*          emType   LXI_MDNS_NAME_EM    要进行设置的名字的类型，见头文件定义
*          ps8Name        s8_t *        要设置名字的地址
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_name_set(LXI_MDNS_NAME_EM emType,s8_t *ps8Name)
{
    if((emType >= LXI_MDNS_NAME_EM_END) ||
        ps8Name == NULL || strlen(ps8Name) > LXI_MDNS_NAME_MAX_LEN)
    {
        //设置错误
        return -1;
    }
    memset(LXI_MDNS_LOG_MEMBER_GET(name)[emType],0,LXI_MDNS_NAME_MAX_LEN);
    strcpy(LXI_MDNS_LOG_MEMBER_GET(name)[emType],ps8Name);
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_name_get
*描       述: mdns主机名以及服务名的获取接口
*输入参数: 
*          参数名         类型              描述
*          emType   LXI_MDNS_NAME_EM    要进行获取的名字的类型，见头文件定义
*          ps8Name        s8_t *        要获取名字的地址
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_name_get(LXI_MDNS_NAME_EM emType,s8_t *ps8Name)
{
    if((emType >= LXI_MDNS_NAME_EM_END) || ps8Name == NULL)
    {
        //获取错误
        return -1;
    }
    strcpy(ps8Name,LXI_MDNS_LOG_MEMBER_GET(name)[emType]);
    return strlen(ps8Name);
}
/**************************************************************************
*函 数 名: lxi_mdns_timer0_set
*描       述: mdns设置计时器0的开关
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_timer0_set(s8_t s8Value)
{
    if(s8Value != 0 && s8Value != 1)
    {
        return -1;
    }

    LXI_MDNS_LOG_MEMBER_GET(timer0) = s8Value;
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_timer0_get
*描       述: mdns获取计时器0的开关状态
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 1--打开,0--关闭
*说    明: 
***************************************************************************/
s8_t lxi_mdns_timer0_get(void)
{
    return LXI_MDNS_LOG_MEMBER_GET(timer0);
}
/**************************************************************************
*函 数 名: lxi_mdns_timer1_set
*描       述: mdns设置计时器1的开关
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

s32_t lxi_mdns_timer1_set(s8_t s8Value)
{
    if(s8Value != 0 && s8Value != 1)
    {
        return -1;
    }

    LXI_MDNS_LOG_MEMBER_GET(timer1) = s8Value;
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_timer1_get
*描       述: mdns获取计时器1的开关状态
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 1--打开,0--关闭
*说    明: 
***************************************************************************/

s8_t lxi_mdns_timer1_get(void)
{
    return LXI_MDNS_LOG_MEMBER_GET(timer1);
}
/**************************************************************************
*函 数 名: lxi_mdns_packet_type_set
*描       述: mdns设置报文包的类型
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--主机名，0--服务名,2--主机服务
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

s32_t lxi_mdns_packet_type_set(s8_t s8Value)
{
    if(s8Value != 0 && s8Value != 1 && s8Value != 2)
    {
        return -1;
    }

    LXI_MDNS_LOG_MEMBER_GET(packet_t) = s8Value;
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_packet_type_get
*描       述: mdns获取报文包的类型
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 1--主机名，0--服务名
*说    明: 
***************************************************************************/

s8_t lxi_mdns_packet_type_get(void)
{
    return LXI_MDNS_LOG_MEMBER_GET(packet_t);
}
/**************************************************************************
*函 数 名: lxi_mdns_timer_type_set
*描       述: mdns设置计时器的类型
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t       0--timer0/1--timer1
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_timer_type_set(s8_t s8Value)
{
    if(s8Value != 0 && s8Value != 1)
    {
        return -1;
    }

    LXI_MDNS_LOG_MEMBER_GET(timer_t) = s8Value;
    return 0;
}

/**************************************************************************
*函 数 名: lxi_mdns_timer_type_get
*描       述: mdns获取计时器的类型
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 0--timer0/1--timer1
*说    明: 
***************************************************************************/
s8_t lxi_mdns_timer_type_get(void)
{
    return LXI_MDNS_LOG_MEMBER_GET(timer_t);
}

/**************************************************************************
*函 数 名: lxi_mdns_host_status_set
*描       述: mdns设置主机名状态
*输入参数: 
*          参数名          类型                    描述
*          emStatus  LXI_MDNS_STATUS_E     mdns状态,见头文件定义
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_host_status_set(LXI_MDNS_STATUS_E emStatus)
{
    if(emStatus > LXI_MDNS_STATE_CONFLICT)
    {
        return -1;
    }
    LXI_MDNS_LOG_MEMBER_GET(hostStatus) = emStatus;
    return 0;
}

/**************************************************************************
*函 数 名: lxi_mdns_host_status_get
*描       述: mdns获取主机名状态
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 见LXI_MDNS_STATUS_E定义
*说    明: 
***************************************************************************/
LXI_MDNS_STATUS_E lxi_mdns_host_status_get(void)
{
    return LXI_MDNS_LOG_MEMBER_GET(hostStatus);
}

/**************************************************************************
*函 数 名: lxi_mdns_server_status_set
*描       述: mdns设置服务名的状态
*输入参数: 
*          参数名          类型                    描述
*          emStatus  LXI_MDNS_STATUS_E     mdns状态,见头文件定义
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_server_status_set(LXI_MDNS_STATUS_E emStatus)
{
    if(emStatus > LXI_MDNS_STATE_CONFLICT)
    {
        return -1;
    }
    LXI_MDNS_LOG_MEMBER_GET(servStatus) = emStatus;
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_server_status_get
*描       述: mdns获取服务名的状态
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 见LXI_MDNS_STATUS_E定义
*说    明: 
***************************************************************************/

LXI_MDNS_STATUS_E lxi_mdns_server_status_get(void)
{
    return LXI_MDNS_LOG_MEMBER_GET(servStatus);
}
/**************************************************************************
*函 数 名: lxi_mdns_need_init_set
*描       述: mdns设置mdns服务重新初始化
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

s32_t lxi_mdns_need_init_set(s8_t s8Value)
{
    if(s8Value != 0 && s8Value != 1)
    {
        return -1;
    }

    m_s8MdnsNeedInit = s8Value;
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_need_init_get
*描       述: mdns获取mdns服务是否重新初始化
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 1--打开,0--关闭
*说    明: 
***************************************************************************/

s8_t lxi_mdns_need_init_get(void)
{
    return m_s8MdnsNeedInit;
}
/**************************************************************************
*函 数 名: lxi_mdns_is_support_set
*描       述: mdns设置是否支持mdns服务
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

s32_t lxi_mdns_is_support_set(s8_t s8Value)
{
    if(s8Value != 0 && s8Value != 1)
    {
        return -1;
    }

    m_s8MdnsIsSupport = s8Value;
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_is_support_get
*描       述: mdns获取是否支持mdns服务
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 1--打开,0--关闭
*说    明: 
***************************************************************************/

s8_t lxi_mdns_is_support_get(void)
{
    return m_s8MdnsIsSupport;
}
/**************************************************************************
*函 数 名: lxi_mdns_enable_status_set
*描       述: mdns设置mdns服务打开/关闭
*输入参数: 
*          参数名         类型              描述
*          s8Value        s8_t        1--打开，0--关闭
*输出参数: 
*返 回 值: -1--失败,0--成功
*说    明: 
***************************************************************************/

s32_t lxi_mdns_enable_status_set(s8_t s8Value)
{
    if(s8Value != 0 && s8Value != 1)
    {
        return -1;
    }

    m_s8MdnsEnableStatus = s8Value;
    return 0;
}
/**************************************************************************
*函 数 名: lxi_mdns_enable_status_get
*描       述: mdns获取mdns服务状态
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值: 1--打开,0--关闭
*说    明: 
***************************************************************************/

s8_t lxi_mdns_enable_status_get(void)
{
    return m_s8MdnsEnableStatus;
}
/**************************************************************************
*函 数 名: lxi_mdns_timer_start
*描       述: mdns启动计时器
*输入参数: 
*          参数名         类型              描述
*          u32TimeCoe     u32_t           时间系数，具体延迟时间 = 时间系数 × 时间系数
*输出参数: 
*返 回 值: 
*说    明: 
***************************************************************************/
s32_t lxi_mdns_timer_start(u32_t u32TimeCoe)
{
    u32_t time = 0;
    while(time < u32TimeCoe)
    {
	    if(m_s8MdnsNeedInit == 1 && lxi_mdns_host_status_get() == LXI_MDNS_STATE_PRONOUNCE)
	    {
	    	m_s8MdnsNeedInit = 0;
		break;
	    }
	    sys_msleep(200);
	    time++;
    }
    if(time == u32TimeCoe)
    {
    	return 0;
    }
    else
    {
    	return -1;
    }
#if 0
    	if(u32TimeCoe != 0 && u32TimeCoe != 1)
    	{
        	return;
    	}

    time = (u32TimeCoe == 0 ? 1500:15000);
    sys_msleep(time);
#endif
}

/**************************************************************************
*函 数 名: lxi_mdns_name_check
*描       述: 查询命名是否规范,不规范则进行变量命名规范
*输入参数: 
*          参数名         类型              描述
*          ps8Name       s8_t *         待检查的名字字段
*          emType    LXI_MDNS_NAME_EM   检查名称的类型
*输出参数: 
*返 回 值: -1--不合规范，0--符合规范
*说    明: 
***************************************************************************/
s32_t lxi_mdns_name_check(s8_t *ps8Name , LXI_MDNS_NAME_EM emType)
{
    s32_t len,i;
    s8_t  name[LXI_MDNS_NAME_MAX_LEN] = {0};

    if(ps8Name == NULL)
    {
        return -1;
    }
    len = strlen(ps8Name);

    lxi_mdns_name_get((LXI_MDNS_NAME_EM)(emType+3),name);

    if(len > LXI_MDNS_NAME_MAX_LEN)
    {
        //命名超出大小限制
        if(strlen(name) != 0)
        {
            //flash中保存的有名字，则使用flash中的名字
            lxi_mdns_name_set(emType,name);
        }
        else
        {
            //否则采用默认值
            lxi_mdns_name_set(emType,LXI_MDNS_DEFAULT_NAME);
            mdns_debug("check name error!");
        }
        return -1;
    }
    else
    {
        //不是字符超限，进入此分支
        for(i = 0 ; i < len ; i ++)
        {
            if(((*(ps8Name + i) >='a') && (*(ps8Name + i) <='z')) || 
                ((*(ps8Name + i) >='A') && (*(ps8Name + i) <='Z')) ||
                ((*(ps8Name + i) >='0') && (*(ps8Name + i) <='9')) ||
                *(ps8Name + i) =='_')
            {
                //字符符合要求，继续检验
                continue;
            }
            else
            {
                //否则，需要恢复主机名的设置
                if(strlen(name) != 0)
                {
                    //flash中保存的有名字，则使用flash中的名字
                    lxi_mdns_name_set(emType,name);
                }
                else
                {
                    //否则采用默认值
                    lxi_mdns_name_set(emType,LXI_MDNS_DEFAULT_NAME);
                    mdns_debug("check name error!");
                }
                return -1;
            }
        }
    } 
    return 0;
}


/**************************************************************************
*函 数 名: lxi_mdns_name_convert
*描    述: 将输入的主机名转化为符合DNS 要求的格式
*输入参数: 
*          参数名         类型              描述
*          ps8Src         s8_t *        待转换名字的地址
*          ps8Dst         s8_t *        保存转换后名字的地址
*输出参数: 
*返 回 值: 成功返回格式化长度，否则返回-1
*说    明: 
*举    例:如rigol.local.cn转换为5rigol5local2cn，及每段字符前面加上该段字符长度
***************************************************************************/
s32_t lxi_mdns_name_convert(s8_t *ps8Src , s8_t *ps8Dst)
{
    s32_t  count = 0;
    s32_t  len   = 0;
    s8_t  *src   = ps8Src;
    s8_t  *dst   = ps8Dst;
    s8_t  *mark  = ps8Dst;

    if(ps8Src == NULL || 
       ps8Dst == NULL || 
       strlen(ps8Src) == 0 || 
       strlen(ps8Src) > LXI_MDNS_NAME_MAX_LEN)
    {
        //参数不正确
        return -1;
    }

    if(*src != '.')//第一个不为" . "时预留空间
    {
        count ++;
        dst ++;
    }
    while(*src != '\0')
    {
        *dst = *src;
        dst ++;
        src ++;

        count ++;

        if(*src == '\0' || *src == '.')
        {
            *mark = count - 1;
            len += count;
            count = 0;
            mark = dst;
        }
    }
    *dst = '\0';
    len ++;
    return len;
}
/**************************************************************************
*函 数 名: lxi_mdns_name_case_change
*描       述: 把名字中的大写字符改为小写字符，便于比较
*输入参数: 
*          参数名         类型              描述
*          ps8Name       s8_t *         待修改的名字字段
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_name_case_change(s8_t *ps8Name)
{
    while(ps8Name)
    {
        if(*ps8Name >= 'A' && *ps8Name <= 'Z')
        {
            *ps8Name += 32;
        }
        else
        {
            break;
        }
        ps8Name ++;
    }
}
/**************************************************************************
*函 数 名: lxi_mdns_name_rename
*描       述: 根据mdns协议,当命名冲突时重命名
*输入参数: 
*          参数名         类型              描述
*          ps8Name       s8_t *         待修改的名字字段
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_name_rename(s8_t *ps8Data)
{
    s32_t i = 0,len = strlen(ps8Data);
    s8_t  tag;//标识下划线后面是否全为数字
    
    for(; i < len ; i ++)//先查找当前名字是否含有下划线'_'
    {
        if(ps8Data[i] == '_')
        {
            //查找到下划线'_'
            break;
        }
    }
    if(i == len)
    {
        //查找完字符串,没有发现下划线'_',则直接在字符串后面添加'_'和数字'1'
        ps8Data[i]   = '_';
        ps8Data[i+1] = '1';
        ps8Data[i+2] = '\0';
    }
    else
    {
        //已在字符串添加了后缀'_',继续查找,是否为已经重新命名
        for(;i< len ; i ++)
        {
            if(ps8Data[i] >= '0' && ps8Data[i] <= '9')
            {
                tag = 1;
            }
            else
            {
                tag = 0;
            }
        }
        if(tag == 1)
        {
            if(ps8Data[i-1] == '9')
            {
                ps8Data[i-1] = '0';
                ps8Data[i]   = '1';
            }
            else
            {
                ps8Data[i-1] += 1;
            }
        }
        else
        {
            ps8Data[i]   = '_';
            ps8Data[i+1] = '1';
            ps8Data[i+2] = '\0';
        }
    }
}

/**************************************************************************
*函 数 名: lxi_mdns_name_save
*描       述: 保存主机名和服务名到flash
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_name_save(LXI_MDNS_NAME_EM emType)
{
    s8_t name[LXI_MDNS_NAME_MAX_LEN] = {0};

    lxi_mdns_name_get(emType,name);
    if(emType == LXI_MDNS_NAME_EM_HOST_FLASH)
    {
        if(m_stLxiCallback.pfSaveHostName!=NULL)
        {
            m_stLxiCallback.pfSaveHostName(name,strlen(name));
        } 
    }
    else if(emType == LXI_MDNS_NAME_EM_SERVER_FLASH)
    {
        if(m_stLxiCallback.pfSaveServiceName!=NULL)
        {
            m_stLxiCallback.pfSaveServiceName(name,strlen(name));
        }
    }
}
/**************************************************************************
*函 数 名: lxi_mdns_name_read
*描       述: 从flash读取主机名和服务名
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_name_read(void)
{
    s32_t  s32Ret = 0;
    s8_t   name[LXI_MDNS_NAME_MAX_LEN] = {0};

    if(m_stLxiCallback.pfReadHostName != 0)
    {
        m_stLxiCallback.pfReadHostName(name,&s32Ret);
        name[s32Ret] = 0;
        if(lxi_mdns_name_check(name,LXI_MDNS_NAME_EM_HOST_SET) == 0)
        {
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SET,name);
        }
        else
        {
            mdns_debug("check name error!");
        }
    }
    else
    {
        mdns_debug("err: none read host name func!");
        lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SET,LXI_MDNS_DEFAULT_NAME);
    }
    if(m_stLxiCallback.pfReadServiceName != 0)
    {
        memset(name,0,LXI_MDNS_NAME_MAX_LEN);
        m_stLxiCallback.pfReadServiceName(name,&s32Ret);
        name[s32Ret] = 0;
        if(lxi_mdns_name_check(name,LXI_MDNS_NAME_EM_SERVER_SET) == 0)
        {
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET,name);
        }
        else
        {
            mdns_debug("check name error!");
        }
    }
    else
    {
        mdns_debug("err: none read service name func!");
        lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET,LXI_MDNS_DEFAULT_NAME);
    }
    if(m_stLxiCallback.pfReadUserHostName != 0)
    {
        memset(name,0,LXI_MDNS_NAME_MAX_LEN);
        m_stLxiCallback.pfReadUserHostName(name,&s32Ret);
        name[s32Ret] = 0;
        if(lxi_mdns_name_check(name,LXI_MDNS_NAME_EM_HOST_SET) == 0)
        {
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_TEMP,name);
        }
        else
        {
            mdns_debug("check name error!");
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_TEMP,LXI_MDNS_DEFAULT_NAME);
        }
    }
    else
    {
        mdns_debug("err: none read user hose name func!");
        lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_TEMP,LXI_MDNS_DEFAULT_NAME);
    }

    if(m_stLxiCallback.pfReadUserServiceName != 0)
    {
        memset(name,0,LXI_MDNS_NAME_MAX_LEN);
        m_stLxiCallback.pfReadUserServiceName(name,&s32Ret);
        name[s32Ret] = 0;
        if(lxi_mdns_name_check(name,LXI_MDNS_NAME_EM_SERVER_SET) == 0)
        {
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_TEMP,name);
        }
        else
        {
            mdns_debug("check name error!");
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_TEMP,LXI_MDNS_DEFAULT_NAME);
        }
    }
    else
    {
        mdns_debug("err: none read user service name func!");
        lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_TEMP,LXI_MDNS_DEFAULT_NAME);
    }
    memset(name,0,LXI_MDNS_NAME_MAX_LEN);
    lxi_mdns_name_get(LXI_MDNS_NAME_EM_HOST_SET,name);
    lxi_mdns_name_case_change(name);
    lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SET,name);
    lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SAVE,name);

    memset(name,0,LXI_MDNS_NAME_MAX_LEN);
    lxi_mdns_name_get(LXI_MDNS_NAME_EM_SERVER_SET,name);
    lxi_mdns_name_case_change(name);
    lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET,name);
    lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SAVE,name);
}
/**************************************************************************
*函 数 名: lxi_mdns_event_set
*描       述: 向事件队列中写入事件
*输入参数: 
*          参数名         类型              描述
*          emEvent   LXI_MDNS_EVENT_EM   事件枚举值  
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_event_set(LXI_MDNS_EVENT_EM emEvent)
{
    if(LXI_MDNS_QUEUE_GET(event).front == LXI_MDNS_QUEUE_MAX_NUM)
    {
        //当前事件的首指针到达队列的尾部，重置事件队列的首指针
        LXI_MDNS_QUEUE_GET(event).front = 0;
    }
    if(LXI_MDNS_QUEUE_GET(event).queue[LXI_MDNS_QUEUE_GET(event).front] != LXI_MDNS_EVENT_NULL)
    {
        /*判断当前事件队列中该索引对应的数据是否等于0,不等于0表示该队列满,反之为空*/
        return;
    }
    /*写事件类型进入事件队列,并Post信号量*/
    LXI_MDNS_QUEUE_GET(event).queue[LXI_MDNS_QUEUE_GET(event).front] = emEvent;
    LXI_MDNS_QUEUE_GET(event).front ++;
    sem_post(&LXI_MDNS_QUEUE_GET(event).sem);
}
/**************************************************************************
*函 数 名: lxi_mdns_event_get
*描       述: 从事件队列中获取事件
*输入参数: 
*          参数名         类型              描述
*输出参数: 
*返 回 值:事件的枚举值，见LXI_MDNS_EVENT_EM定义
*说    明: 
***************************************************************************/
LXI_MDNS_EVENT_EM lxi_mdns_event_get(void)
{
    LXI_MDNS_EVENT_EM emEvent;

    sem_wait(&LXI_MDNS_QUEUE_GET(event).sem);

    if(LXI_MDNS_QUEUE_GET(event).rear == LXI_MDNS_QUEUE_MAX_NUM)
    {
        //当前事件的尾指针到达队列的尾部，重置事件队列的尾指针
        LXI_MDNS_QUEUE_GET(event).rear = 0;
    }
    if(LXI_MDNS_QUEUE_GET(event).queue[LXI_MDNS_QUEUE_GET(event).rear] == LXI_MDNS_EVENT_NULL)
    {
        //事件队列为空队列,直接返回
        return LXI_MDNS_EVENT_NULL;
    }
    /*队列不为空，则读取该事件，并置为空事件*/
    emEvent = LXI_MDNS_QUEUE_GET(event).queue[LXI_MDNS_QUEUE_GET(event).rear];
    LXI_MDNS_QUEUE_GET(event).queue[LXI_MDNS_QUEUE_GET(event).rear] = LXI_MDNS_EVENT_NULL;
    LXI_MDNS_QUEUE_GET(event).rear ++;
    return emEvent;
}
/**************************************************************************
*函 数 名: lxi_mdns_packet_set
*描       述: 向接收队列中写入接收到的数据
*输入参数: 
*          参数名         类型                  描述
*          ps8Data        s8_t*     需要写入接收队列中的数据地址  
*          s32Len         s32_t     需要写入接收队列中的数据长度
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_packet_set(s8_t *ps8Data , s32_t s32Len)
{
    if(LXI_MDNS_QUEUE_GET(recv).front == LXI_MDNS_QUEUE_MAX_NUM)
    {
        //当前事件的首指针到达队列的尾部，重置事件队列的首指针
        LXI_MDNS_QUEUE_GET(recv).front = 0;
    }
    if(LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).front].len != 0)
    {
        /*判断当前事件队列中该索引对应的数据是否等于0,不等于0表示该队列满,反之为空*/
        return;
    }
    //队列未满,保存数据
    memset(LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).front].packet , 0 , LXI_MDNS_PACKET_MAX_LEN);
    memcpy(LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).front].packet,ps8Data,s32Len);
    LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).front].len = s32Len;
    LXI_MDNS_QUEUE_GET(recv).front ++;

    //发送数据包解析事件
    lxi_mdns_event_set(LXI_MDNS_EVENT_PACKET_PASER);
}
/**************************************************************************
*函 数 名: lxi_mdns_packet_get
*描       述: 从接收队列中获取接收到的数据
*输入参数: 
*          参数名         类型                  描述
*          ps8Data        s8_t*     需要把接收队列中的数据写入地址  
*          s32Len         s32_t     记录从接收队列中读取的数据长度
*输出参数: 
*返 回 值:-1--接收失败，0--接收成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_packet_get(s8_t *ps8Data , s32_t *ps32Len)
{
    if(LXI_MDNS_QUEUE_GET(recv).rear == LXI_MDNS_QUEUE_MAX_NUM)
    {
        //当前接收队列的尾指针指向了队列尾部，重置从队列首部开始
        LXI_MDNS_QUEUE_GET(recv).rear = 0;
    }
    if(LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).rear].len == 0)
    {
        //当前指针指向的接收缓冲区为空
        return -1;
    }
    //有接收数据,进行数据复制
    *ps32Len = LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).rear].len;
    memcpy(ps8Data,LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).rear].packet,*ps32Len);
    LXI_MDNS_QUEUE_GET(recv).queue[LXI_MDNS_QUEUE_GET(recv).rear].len = 0;
    LXI_MDNS_QUEUE_GET(recv).rear ++;
    return 0;
}

/**************************************************************************
*函 数 名: lxi_mdns_packet_send
*描       述: mdns对外发送数据的接口
*输入参数: 
*          参数名         类型                  描述
*          ps8Data        s8_t*           需要发送的数据地址
*          s32Len         s32_t           发送数据的长度
*输出参数: 
*返 回 值:-1--发送失败,其他正数值均表示发送成功
*说    明: 
***************************************************************************/
s32_t lxi_mdns_packet_send(s8_t *ps8Data , s32_t s32Len)
{
    struct sockaddr_in raddr = {0};//远程的IP地址
    s32_t  len = sizeof(struct sockaddr_in);
    s32_t  err_t;
    s32_t  sockfd = LXI_MDNS_LOG_MEMBER_GET(sockfd);
    /*修改目的地址*/
    raddr.sin_family = AF_INET;//IPV4
    raddr.sin_port   = htons(LXI_MDNS_HOST_PORT);//指定端口号5353
    raddr.sin_addr.s_addr = LXI_MDNS_HOST_IPADDR;//根据标准,指定目的地址为：224.0.0.251

    if(sockfd == -1)
    {
        //UDP-Socket未完成分配
        err_t = -1;
    }
    else
    {
        //UDP-Socket已完成分配，则进行数据的发送
        err_t = sendto(sockfd,ps8Data,s32Len,0,(struct sockaddr *)&raddr,len);
    }
    return err_t;
}

/**************************************************************************
*函 数 名: lxi_mdns_probe_query_make
*描       述: mdns组织对外发送探测报文的接口
*输入参数: 
*          参数名         类型                  描述
*          s32Id          s32_t          该mdns探测包的ID编号
*          ps8Buff        s8_t *         报文数据存放的地址
*          s32Type        s32_t          是组播还是单播传输
*          ps8Host        s8_t *         主机名存放地址
*          ps8Serv        s8_t *         服务名存放地址
*          u32TTL         u32_t          该报文在网络传输过程中的生存时间
*输出参数: 
*返 回 值:该报文的长度
*说    明: 
***************************************************************************/
s32_t lxi_mdns_probe_query_make(s32_t s32Id ,s8_t *pstBuff , s32_t s32Type,
                                          s8_t *ps8Host,s8_t *ps8Serv,u32_t u32TTL)
{
    s8_t  *ptr;
    s8_t   name[LXI_MDNS_NAME_MAX_LEN] = {0};
    s32_t  space1, space2, space3, space4;
    s32_t  space = 0,length = 0;

    ptr = pstBuff;

    *ptr++ = (s8_t)(s32Id >> 8);
    *ptr++ = (s8_t)(s32Id & 0xFF);//标识
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;//标志
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)5;//问题数
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;//资源记录数
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)5;//授权资源记录数
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;//额外资源记录数
    
    space += 12;
    //问题1
    strcpy(name,ps8Host);
    
    strcat(name,".local");
    length = lxi_mdns_name_convert(name,ptr);
    ptr += length;
    space += length;
    
    *ptr++ = (s8_t)0;
    *ptr++ = 0xff;//查询类型
    
    if((s32Type & 0x0001) == 0)
    {
        *ptr++ = 0x00;//QM查询
    }
    else
    {
        *ptr++ = 0x80;//QU查询
    }
    *ptr++ = 0x01;//查询类
    
    space += 4;
    space1 = space;
    //问题2
    memset(name, 0, LXI_MDNS_NAME_MAX_LEN);
    strcpy(name, ps8Serv);
    strcat(name,"._http._tcp.local");
    length = lxi_mdns_name_convert(name,ptr);
    ptr += length;
    space += length;
    *ptr++ = (s8_t)0;
    *ptr++ = 0xff;//查询类型
    
    if((s32Type & 0x0001) == 0)
    {
        *ptr++ = 0x00;//QM查询
    }
    else
    {
        *ptr++ = 0x80;//QU查询
    }
    *ptr++ = 0x01;//查询类
    
    space += 4;
    space2 = space;
    //问题3
    memset(name, 0, LXI_MDNS_NAME_MAX_LEN);
    strcpy(name, ps8Serv);
    strcat(name,"._lxi._tcp.local");
    length = lxi_mdns_name_convert(name,ptr);
    ptr += length;
    space += length;
    *ptr++ = (s8_t)0;
    *ptr++ = 0xff;//查询类型
    
    if((s32Type & 0x0001) == 0)
    {
        *ptr++ = 0x00;//QM查询
    }
    else
    {
        *ptr++ = 0x80;//QU查询
    }
    *ptr++ = 0x01;//查询类
    
    space += 4;
    space3 = space;
    //问题4
    memset(name, 0, LXI_MDNS_NAME_MAX_LEN);
    strcpy(name, ps8Serv);
    strcat(name,"._vxi-11._tcp.local");
    length = lxi_mdns_name_convert(name,ptr);
    ptr += length;
    space += length;
    *ptr++ = (s8_t)0;
    *ptr++ = 0xff;//查询类型
    
    if((s32Type & 0x0001) == 0)
    {
        *ptr++ = 0x00;//QM查询
    }
    else
    {
        *ptr++ = 0x80;//QU查询
    }
    *ptr++ = 0x01;//查询类
    
    space += 4;
    space4 = space;
    //问题5
    memset(name, 0, LXI_MDNS_NAME_MAX_LEN);
    strcpy(name, ps8Serv);
    strcat(name,"._scpi-raw._tcp.local");
    length = lxi_mdns_name_convert(name,ptr);
    ptr += length;
    space += length;
    *ptr++ = (s8_t)0;
    *ptr++ = 0xff;//查询类型
    
    if((s32Type & 0x0001) == 0)
    {
        *ptr++ = 0x00;//QM查询
    }
    else
    {
        *ptr++ = 0x80;//QU查询
    }
    *ptr++ = 0x01;//查询类
    
    space += 4;
    
    //授权资源记录1
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)0x0c;//此时对主机名的添加采用压缩格式
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)1;//授权类型:主机IP地址
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)1;//网络数据
    //生存时间
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    //数据长度
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)4;
    //IP地址
    memcpy(ptr,LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);
    ptr += LXI_MDNS_IP_ADDR_LEN;
    space += 16;
    //授权资源记录2
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)space1;//资源名采用压缩
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x21;//授权类型:服务定位
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x01;//网络数据
    //生存时间
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    //数据长度
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    //数据优先级
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //数据权重
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //访问端口
    *ptr++ = (s8_t)12;
    *ptr++ = (s8_t)12;
    //主机名，采用压缩格式
    *ptr++ = 0xc0;
    *ptr++ = 0x0c;
    space += 20;
    //授权资源记录3
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)space2;//资源名采用压缩
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x21;//授权类型:服务定位
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x01;//网络数据
    //生存时间
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    //数据长度
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    //数据优先级
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //数据权重
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //访问端口
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x50;
    //主机名，采用压缩格式
    *ptr++ = 0xc0;
    *ptr++ = 0x0c;
    space += 20;
    //授权资源记录4
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)space3;//资源名采用压缩
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x21;//授权类型:服务定位
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x01;//网络数据
    //生存时间
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    //数据长度
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    //数据优先级
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //数据权重
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //访问端口
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x6f;
    //主机名，采用压缩格式
    *ptr++ = 0xc0;
    *ptr++ = 0x0c;
    space += 20;
    //授权资源记录5
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)space4;//资源名采用压缩
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x21;//授权类型:服务定位
    
    *ptr++ = (s8_t)0x00;
    *ptr++ = (s8_t)0x01;//网络数据
    //生存时间
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    //数据长度
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    //数据优先级
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //数据权重
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    //访问端口
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x6f;
    //主机名，采用压缩格式
    *ptr++ = 0xc0;
    *ptr++ = 0x0c;
    space += 20;
    
    return space;
}
/**************************************************************************
*函 数 名: lxi_mdns_probe_response_make
*描       述: mdns组织对探测报文的响应
*输入参数: 
*          参数名         类型                  描述
*          s32Id          s32_t          该mdns探测包的ID编号
*          ps8Buff        s8_t *         报文数据存放的地址
*          ps8Host        s8_t *         主机名存放地址
*          ps8Serv        s8_t *         服务名存放地址
*          u32TTL         u32_t          该报文在网络传输过程中的生存时间
*输出参数: 
*返 回 值:该报文的长度
*说    明: 
***************************************************************************/
s32_t lxi_mdns_probe_response_make(s32_t s32Id ,s8_t *pstBuff ,s8_t *ps8Host,
                                              s8_t *ps8Serv,u32_t u32TTL)
{
    s8_t  *ptr;
    s8_t   name[LXI_MDNS_NAME_MAX_LEN] = {0};
    s32_t  len,retlen;
    
    ptr = pstBuff;
    /*添加报文的ID编号*/
    *ptr++ = (s8_t)(s32Id >> 8);
    *ptr++ = (s8_t)(s32Id & 0xFF);
    /*数据报文类型*/
    *ptr++ = (s8_t)0x84;
    *ptr++ = (s8_t)0;
    /*报文中包含0个问题*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    /*报文中包含5个授权资源*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)5;
    /*报文中包含授权资源记录数*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;			
    /*报文中包含额外资源记录数*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    len = 12;
    
    strcpy(name,ps8Host);
    strcat(name,".local");
    /*添加主机名信息*/
    retlen = lxi_mdns_name_convert(name,ptr);
    
    len += retlen;
    ptr += retlen;
    /*第一个授权资源*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)1;
    /*授权资源类型:主机名*/
    *ptr++ = (s8_t)0x80;
    *ptr++ = (s8_t)0x01;
    /*生存时间*/
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    /*数据长度*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)4;
    /*主机IP地址*/
    memcpy(ptr,LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);
    ptr += LXI_MDNS_IP_ADDR_LEN;
    len += 14;
    
    memset(name,0,LXI_MDNS_NAME_MAX_LEN);
    strcpy(name,ps8Serv);
    strcat(name,"._http._tcp.local");
    /*添加支持WenServer的服务*/
    retlen = lxi_mdns_name_convert(name,ptr);
    
    ptr += retlen;
    len += retlen;
    /*授权类型:服务定位*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x21;
    /*数据包类型*/
    *ptr++ = (s8_t)0x80;
    *ptr++ = (s8_t)1;
    /*生存时间*/
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    /*数据长度*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    /*数据权重*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    /*数据优先级*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    /*数据端口*/
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x50;
    /*采用压缩方式：0xC0表示数据为压缩模式,0x0C表示从有效数据便宜量*/
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)0x0c;
    len += 18;
    
    memset(name,0,LXI_MDNS_NAME_MAX_LEN);
    strcpy(name,ps8Serv);
    strcat(name,"._lxi._tcp.local");
    
    retlen = lxi_mdns_name_convert(name,ptr);
    
    ptr += retlen;
    len += retlen;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x21;
    
    *ptr++ = (s8_t)0x80;
    *ptr++ = (s8_t)1;
    
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x50;
    
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)0x0c;
    len += 18;
    
    memset(name,0,LXI_MDNS_NAME_MAX_LEN);
    strcpy(name,ps8Serv);
    strcat(name,"._vxi-11._tcp.local");
    
    retlen = lxi_mdns_name_convert(name,ptr);
    
    ptr += retlen;
    len += retlen;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x21;
    
    *ptr++ = (s8_t)0x80;
    *ptr++ = (s8_t)1;
    
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x6F;
    
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)0x0c;
    len += 18;
    
    memset(name,0,LXI_MDNS_NAME_MAX_LEN);
    strcpy(name,ps8Serv);
    strcat(name,"._scpi-raw._tcp.local");
    
    retlen = lxi_mdns_name_convert(name,ptr);
    
    ptr += retlen;
    len += retlen;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x21;
    
    *ptr++ = (s8_t)0x80;
    *ptr++ = (s8_t)1;
    
    *ptr++ = (s8_t)(u32TTL >> 24);
    *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
    *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
    *ptr++ = (s8_t)(u32TTL & 0x00ff);
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)8;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0x6F;
    
    *ptr++ = (s8_t)0xc0;
    *ptr++ = (s8_t)0x0c;
    len += 18;
    
    return len;    
}

/**************************************************************************
*函 数 名: lxi_mdns_single_query_make
*描       述: mdns根据名称字符串自发组织的查询报文
*输入参数: 
*          参数名         类型                  描述
*          s32Id          s32_t          该mdns探测包的ID编号
*          ps8Buff        s8_t *         报文数据存放的地址
*          s32Type        s32_t          是组播还是单播传输
*          ps8Name        s8_t *         要查询的名称组成的字符串
*          emType   LXI_MDNS_PACKET_EM   生成报文的种类
*输出参数: 
*返 回 值:该报文的长度
*说    明: 
***************************************************************************/
s32_t lxi_mdns_single_query_make(s32_t s32Id ,s8_t *pstBuff , s32_t s32Type,
                                           s8_t *ps8Name,LXI_MDNS_PACKET_EM emType)
{
    s8_t *ptr;
    s32_t  len,retLen,ptrlen;
    s32_t  space = 0,space1,space2,space3;
    s8_t   name[LXI_MDNS_NAME_MAX_LEN] = {0};
    
    ptr = pstBuff;

    if(emType == LXI_MDNS_PACKET_HOST)
    {
        
        *ptr++ = (s8_t)(s32Id >> 8);
        *ptr++ = (s8_t)(s32Id & 0xFF);//标识
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//标志
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//问题数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//授权资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//额外资源记录数
        len = 12;
        //查询问题 
        retLen = lxi_mdns_name_convert(ps8Name,ptr);//查询名
        ptr += retLen;
        len += retLen;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xFF;//查询问题的类型:任意
        
        if((s32Type & 0x0001) == 0)
        {
            *ptr++ = (s8_t)0;//单播查询
        }
        else
        {
            *ptr++ = (s8_t)0x80;//组播查询
        }
        *ptr++ = (s8_t)1;//查询类
        len += 4;
        //授权资源记录
        retLen = lxi_mdns_name_convert(ps8Name,ptr);//授权资源名
        ptr += retLen;
        len += retLen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//授权资源的类型:A
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//授权资源的类:为internet数据
        
        *ptr++ = 0x00;
        *ptr++ = 0x00;
        *ptr++ = 0x00;
        *ptr++ = 0xff;//授权资源的生存时间
        
        *ptr++ = 0x00;
        *ptr++ = 0x04;//资源数据长度 
        
        memcpy(ptr,LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);
        len += 14;
    }
    else
    {
        *ptr++ = (s8_t)(s32Id >> 8);
        *ptr++ = (s8_t)(s32Id & 0xFF);//标识
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//标志
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)4;//问题数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)4;//授权资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//额外资源记录数
        
        len = 12;
        space += 12;
        //查询问题 1
        retLen = lxi_mdns_name_convert(ps8Name,ptr);
        
        ptr += retLen;
        space += retLen;
        len += retLen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xFF;//查询问题的类型:任意
        if((s32Type & 0x0001) == 0)
        {
            *ptr++ = (s8_t)0;//单播查询
        }
        else
        {
            *ptr++ = (s8_t)0x80;//组播查询
        }
        *ptr++ = (s8_t)1;//查询类
        
        space += 4;
        len += 4;
        //查询问题 2
        ptrlen = strlen(ps8Name) + 1;
        space1 = space;
        retLen = lxi_mdns_name_convert(ps8Name + ptrlen,ptr);
        
        ptr += retLen;
        len += retLen;
        space += retLen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xFF;//查询问题的类型:任意
        if((s32Type & 0x0001) == 0)
        {
            *ptr++ = (s8_t)0;//单播查询
        }
        else
        {
            *ptr++ = (s8_t)0x80;//组播查询
        }
        *ptr++ = (s8_t)1;//查询类
        
        space += 4;
        len += 4;
        //查询问题 3
        space2 = space;
        ptrlen += strlen(ps8Name + ptrlen) + 1;
        retLen = lxi_mdns_name_convert(ps8Name + ptrlen,ptr);
        
        ptr += retLen;
        len += retLen;
        space += retLen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xFF;//查询问题的类型:任意
        if((s32Type & 0x0001) == 0)
        {
            *ptr++ = (s8_t)0;//单播查询
        }
        else
        {
            *ptr++ = (s8_t)0x80;//组播查询
        }
        *ptr++ = (s8_t)1;//查询类 
        
        space += 4;
        len += 4;
        //查询问题 4
        space3 = space;
        ptrlen += strlen(ps8Name + ptrlen) + 1;
        retLen = lxi_mdns_name_convert(ps8Name + ptrlen,ptr);
        
        ptr += retLen;
        len += retLen;
        space += retLen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xFF;//查询问题的类型:任意
        if((s32Type & 0x0001) == 0)
        {
            *ptr++ = (s8_t)0;//单播查询
        }
        else
        {
            *ptr++ = (s8_t)0x80;//组播查询
        }
        *ptr++ = (s8_t)1;//查询类 
        
        space += 4;
        len += 4;
        
        //授权资源记录1
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)0x0c;
        
        //授权资源记录类型
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x21;
        //数据类型:网络数据
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;
        //报文生存时间
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xf0;
        //数据长度
        lxi_mdns_name_get(LXI_MDNS_NAME_EM_HOST_LOCAL,name);
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)(strlen(name) + 8);
        //优先级
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //权重		
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //端口
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x50;
        
        space += 18;
        len += 18;
        
        retLen = lxi_mdns_name_convert(name,ptr);
        len += retLen;
        ptr += retLen;
        
        //授权资源记录2
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)space1;
        //授权资源记录类型
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x21;
        //数据类型:网络数据
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;
        //报文生存时间
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xf0;
        //数据长度
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)8;
        //优先级	
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //权重
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //端口
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x50;
        //主机名
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)space;
        
        len += 20;
        
        //授权资源记录3
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)space2;
        //授权资源记录类型
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x21;
        //数据类型:网络数据
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;
        //报文生存时间
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xf0;
        //数据长度
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)8;
        //优先级
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //权重
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //端口
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x6F;
        //主机名
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)space;
        
        len += 20;
        
        //授权资源记录4
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)space3;
        //授权资源记录类型
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x21;
        //数据类型:网络数据
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;
        //报文生存时间
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0xf0;
        //数据长度
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)8;
        //优先级
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //权重
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;
        //端口
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x6F;
        //主机名
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)space;
        
        len += 20;
    }
    return len;
}
/**************************************************************************
*函 数 名: lxi_mdns_single_response_make
*描       述: mdns组织响应报文
*输入参数: 
*          参数名         类型                  描述
*          s32Id          s32_t          该mdns报文的ID编号
*          ps8Buff        s8_t *         报文数据存放的地址
*          emType    LXI_MDNS_QUERY_EM   响应报文类型
*          u32TTL         u32_t          报文生存时间
*        ps8Response      s8_t *         响应数据的地址
*         s32ResLen       s32_t          响应数据的长度
*输出参数: 
*返 回 值:该报文的长度
*说    明: 
***************************************************************************/
s32_t lxi_mdns_single_response_make(s32_t s32Id , s8_t *ps8Buff , LXI_MDNS_QUERY_EM emType,
                                                u32_t u32TTL, s8_t *ps8Response , s32_t s32ResLen)
{
    s8_t *ptr;

    ptr = ps8Buff;
    
    *ptr++ = (s8_t)(s32Id >> 8);
    *ptr++ = (s8_t)(s32Id & 0xFF);
    *ptr++ = (s8_t)0x84;
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    *ptr++ = (s8_t)0;
    
    switch(emType)
    {
        case LXI_MDNS_QUERY_HOST_ADDR:
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)1;//当前回答个数
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)0;
            memcpy(ptr,ps8Response,s32ResLen);//响应数据
            ptr += s32ResLen;
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)1;
            *ptr++ = (s8_t)0x80;
            *ptr++ = (s8_t)1;
            *ptr++ = (s8_t)(u32TTL >> 24);
            *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
            *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
            *ptr++ = (s8_t)(u32TTL & 0x00ff);
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)4;
            memcpy(ptr,LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);//IP地址
            return (s32ResLen + 26);
        case LXI_MDNS_QUERY_PTR:
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)1;
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)0;			
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)0;
            memcpy(ptr,ps8Response,s32ResLen);
            return (s32ResLen + 12);
        case LXI_MDNS_QUERY_SRV:
        case LXI_MDNS_QUERY_TXT:
        case LXI_MDNS_QUERY_ALL:
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)3;
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)0;			
            *ptr++ = (s8_t)0;
            *ptr++ = (s8_t)0;
            memcpy(ptr,ps8Response,s32ResLen);
            return (s32ResLen + 12);
        default:
            return 0;
    }
}
/**************************************************************************
*函 数 名: lxi_mdns_name_declaration
*描       述: mdns组织主机名或者服务名的声明
*输入参数: 
*          参数名         类型                  描述
*          s32Id          s32_t          该mdns报文的ID编号
*          ps8Buff        s8_t *         报文数据存放的地址
*          ps8Name        s8_t *         名称地址
*          emType    LXI_MDNS_QUERY_EM   响应报文类型
*          u32TTL         u32_t          报文生存时间
*输出参数: 
*返 回 值:该报文的长度
*说    明: 
***************************************************************************/
s32_t lxi_mdns_name_declaration(s32_t s32Id , s8_t *ps8Buff , s8_t *ps8Name,
                                          LXI_MDNS_PACKET_EM emType, u32_t u32TTL)
{
    s8_t  *ptr;
    s8_t   name[LXI_MDNS_NAME_MAX_LEN] = {0};
    s32_t  len = 0,rlen,datalen;
    s32_t  servnamelen = strlen(ps8Name);

    ptr = ps8Buff;

    if(emType == LXI_MDNS_PACKET_HOST)
    {
        strcpy(name,ps8Name);
        strcat(name,".local");
		
        *ptr++ = (s8_t)(s32Id >> 8);
        *ptr++ = (s8_t)(s32Id & 0xFF);//标识
        *ptr++ = (s8_t)0x84;
        *ptr++ = (s8_t)0;//标志
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//问题数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//授权资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//额外资源记录数
        rlen = lxi_mdns_name_convert(name,ptr);
        ptr += rlen;
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//类型
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//网络数据
        
        //生存时间
        *ptr++ = (s8_t)(u32TTL >> 24);
        *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
        *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
        *ptr++ = (s8_t)(u32TTL & 0x00ff);
        //数据长度
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)4;
        //IP地址
        memcpy(ptr,LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);
        len = 26 + rlen; 
    }
    else
    {
        *ptr++ = (s8_t)(s32Id >> 8);
        *ptr++ = (s8_t)(s32Id & 0xFF);//标识
        *ptr++ = (s8_t)0x84;
        *ptr++ = (s8_t)0;//标志
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//问题数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)4;//资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//授权资源记录数
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0;//额外资源记录数 
        
        len += 12;//记录位置信息，便于压缩
        //资源记录1
        rlen = lxi_mdns_name_convert("_http._tcp.local",ptr);
        ptr += rlen;
        //len += rlen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x0c;//类型
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//网络数据
        
        //生存时间
        *ptr++ = (s8_t)(u32TTL >> 24);
        *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
        *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
        *ptr++ = (s8_t)(u32TTL & 0x00ff);
        //数据长度 
        datalen = servnamelen + 3; //3=服务名长度1 字节+ 压缩字段2字节
        *ptr++ = (s8_t)(datalen >> 8);
        *ptr++ = (s8_t)(datalen & 0x00FF);
        *ptr++ = (s8_t)(datalen - 3);
        memcpy(ptr,ps8Name,servnamelen);
        ptr += servnamelen;
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)0x0c;
        len = len + 13 + servnamelen + rlen;
        
        //资源记录2
        rlen = lxi_mdns_name_convert("_lxi._tcp.local",ptr);
        ptr += rlen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x0c;//类型
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//网络数据
        
        //生存时间
        *ptr++ = (s8_t)(u32TTL >> 24);
        *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
        *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
        *ptr++ = (s8_t)(u32TTL & 0x00ff);
        //数据长度 
        *ptr++ = (s8_t)(datalen >> 8);
        *ptr++ = (s8_t)(datalen & 0x00FF);
        *ptr++ = (s8_t)(datalen - 3);
        memcpy(ptr,ps8Name,servnamelen);
        ptr += servnamelen;
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)len;
        len = len + 13 + servnamelen + rlen;
        //资源记录3
        rlen = lxi_mdns_name_convert("_vxi-11._tcp.local",ptr);
        ptr += rlen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x0c;//类型
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//网络数据
        
        //生存时间
        *ptr++ = (s8_t)(u32TTL >> 24);
        *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
        *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
        *ptr++ = (s8_t)(u32TTL & 0x00ff);
        //数据长度 
        *ptr++ = (s8_t)(datalen >> 8);
        *ptr++ = (s8_t)(datalen & 0x00FF);
        *ptr++ = (s8_t)(datalen - 3);
        memcpy(ptr,ps8Name,servnamelen);
        ptr += servnamelen;
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)len;
        len = len + 13 + servnamelen + rlen;
        //资源记录4
        rlen = lxi_mdns_name_convert("_scpi-raw._tcp.local",ptr);
        ptr += rlen;
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)0x0c;//类型
        
        *ptr++ = (s8_t)0;
        *ptr++ = (s8_t)1;//网络数据
        
        //生存时间
        *ptr++ = (s8_t)(u32TTL >> 24);
        *ptr++ = (s8_t)((u32TTL & 0x0fff) >> 16);
        *ptr++ = (s8_t)((u32TTL & 0x00ff) >> 8);
        *ptr++ = (s8_t)(u32TTL & 0x00ff);
        //数据长度 
        *ptr++ = (s8_t)(datalen >> 8);
        *ptr++ = (s8_t)(datalen & 0x00FF);
        *ptr++ = (s8_t)(datalen - 3);
        memcpy(ptr,ps8Name,servnamelen);
        ptr += servnamelen;
        *ptr++ = (s8_t)0xc0;
        *ptr++ = (s8_t)len;
        len = len + 13 + servnamelen + rlen;
    }

    return len;
}

/**************************************************************************
*函 数 名: lxi_mdns_name_configure
*描       述: mdns主机名或服务名的配置
*输入参数: 
*          参数名            类型                  描述
*          emType    LXI_MDNS_PACKET_EM      报文类型:见LXI_MDNS_PACKET_EM定义
*          ps8Name           s8_t *          主机名或者服务名的地址
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_name_configure(LXI_MDNS_PACKET_EM emType , s8_t *ps8Name)
{
    s8_t  as8TempTxt[LXI_MDNS_NAME_MAX_LEN] = {0};
    s32_t s32Len = 0 , s32Temp = 0;

    if(lxi_mdns_enable_status_get() == 1)//判断是否打开了mdns服务
    {
        if(emType == LXI_MDNS_PACKET_HOST)//配置主机名
        {
            lxi_mdns_packet_type_set(0);
            if(strlen(ps8Name) == 0)
            {
                //当前主机名不存在，采用默认值
                memset(ps8Name,0,LXI_MDNS_NAME_MAX_LEN);
                strcpy(ps8Name,LXI_MDNS_DEFAULT_NAME);
                mdns_debug("check name error!");
            }
            strcpy(as8TempTxt,ps8Name);

            if(lxi_mdns_name_check(as8TempTxt,LXI_MDNS_NAME_EM_HOST_SET) == 0)
            {
                //主机名检测成功，则发送主机功能
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SAVE,as8TempTxt);
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_TEMP,as8TempTxt);

                strcat(as8TempTxt,".local");
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_LOCAL,as8TempTxt);

                memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                s32Len = lxi_mdns_single_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),3,
                                                    as8TempTxt,LXI_MDNS_PACKET_HOST);//组织查询报文
                s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32Len);//发送查询报文

                if(s32Temp > 0)
                {
                    //发送成功，进行计数
                    LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                    lxi_mdns_host_status_set(LXI_MDNS_STATE_PROBE);
                }
            }
        }
        else//配置服务名
        {
            lxi_mdns_packet_type_set(1);
            if(strlen(ps8Name) == 0)
            {
                //当前主机名不存在，采用默认值
                memset(ps8Name,0,LXI_MDNS_NAME_MAX_LEN);
                strcpy(ps8Name,LXI_MDNS_DEFAULT_NAME);
                mdns_debug("check name error!");
            }
            strcpy(as8TempTxt,ps8Name);

            if(lxi_mdns_name_check(as8TempTxt,LXI_MDNS_NAME_EM_SERVER_SET) == 0)
            {
                //服务名检测成功，发送服务名支持的功能和配置
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SAVE,as8TempTxt);
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_TEMP,as8TempTxt);

                s32Len = strlen(as8TempTxt);
                memcpy(as8TempTxt+s32Len,"._http._tcp.local",17);//支持WebServer功能
                *(as8TempTxt + s32Len + 17) = '\0';
                s32Temp = strlen(as8TempTxt) + 1;
                memcpy(as8TempTxt+s32Temp,ps8Name,strlen(ps8Name));
                s32Temp += strlen(ps8Name);
                memcpy(as8TempTxt+s32Temp,"._lxi._tcp.local",16);//支持LXI功能
                *(as8TempTxt+s32Temp+16) = '\0';
                s32Temp += 17;
                memcpy(as8TempTxt+s32Temp,ps8Name,strlen(ps8Name));
                s32Temp += strlen(ps8Name);
                memcpy(as8TempTxt+s32Temp,"._vxi-11._tcp.local",19);//支持Vxi-11标准
                *(as8TempTxt+s32Temp+19) = '\0';
                s32Temp += 20;
                memcpy(as8TempTxt+s32Temp,ps8Name,strlen(ps8Name));
                s32Temp += strlen(ps8Name);
                memcpy(as8TempTxt+s32Temp,"._scpi-raw._tcp.local",21);//支持命令解析功能
                *(as8TempTxt+s32Temp+21) = '\0';

                s32Len = lxi_mdns_single_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),3,
                                                    as8TempTxt,LXI_MDNS_PACKET_SERVER);
                s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32Len);
                if(s32Temp > 0)
                {
                    //发送成功，进行计数
                    LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                    lxi_mdns_server_status_set(LXI_MDNS_STATE_PROBE);
                }
            }
        }
    }
}

/**************************************************************************
*函 数 名: lxi_mdns_host_probe
*描       述: mdns主机名的探测
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_host_probe(void)
{
    s32_t   s32Temp,s32Len;
    s8_t    scTempName[LXI_MDNS_NAME_MAX_LEN] = {0};
    s8_t    name[LXI_MDNS_NAME_MAX_LEN] = {0};

    //只有在mdns启动timer0寄存器时，才能进行主机名的探测
    if(lxi_mdns_timer_type_get()== 0)
    {
        if(lxi_mdns_host_status_get() != LXI_MDNS_STATE_IDLE)//主机处于空闲状态，不能进行探测
        {
            lxi_mdns_name_get(LXI_MDNS_NAME_EM_HOST_SET,name);
            
            if(LXI_MDNS_LOG_MEMBER_GET(probe_n) > LXI_MDNS_PROBE_MIN_NUM)
            {
                //已经完成发送探测报文最小次数
                if(LXI_MDNS_LOG_MEMBER_GET(probe_n) < LXI_MDNS_PROBE_MAX_NUM)
                {
                    //未超出最大的探测报文发送次数
                    lxi_mdns_host_status_set(LXI_MDNS_STATE_PRONOUNCE);//设置主机名状态-探测成功
                    s32Temp = lxi_mdns_name_convert(name,scTempName);//转换主机名为mdns报文格式
                    memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_NAME_MAX_LEN);//清空缓冲区                                
                    s32Len = lxi_mdns_single_response_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),LXI_MDNS_QUERY_HOST_ADDR,
                                                           1255,scTempName,s32Temp);//组织主机名响应报文
                    s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32Len);//发送数据
                    if(s32Temp >0 && LXI_MDNS_LOG_MEMBER_GET(probe_n) == LXI_MDNS_PROBE_SUCCESS_NUM)
                    {
                        //发送成功,并且探测次数达到成功探测次数,保存到flash
                        lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_FLASH,name);
                        lxi_mdns_name_save(LXI_MDNS_NAME_EM_HOST_FLASH);
                    }
                }
                else
                {
                    //超出最大探测次数，复位
                    LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;
                    lxi_mdns_timer0_set(0);
                }
            }
            else
            {
                //未达到最小的探测次数,继续探测
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_FLASH , name);
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_LOCAL , name);
                strcat(LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_LOCAL],".local");

                memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                s32Len = lxi_mdns_single_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),
                                                    2,name,LXI_MDNS_PACKET_HOST);
                s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32Len);
                if(s32Temp > 0)
                {
                    lxi_mdns_host_status_set(LXI_MDNS_STATE_PROBE);
                    LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                }
            }
        }
    }
}

/**************************************************************************
*函 数 名: lxi_mdns_server_probe
*描       述: mdns服务名的探测
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_server_probe(void)
{
    s32_t   s32Temp,s32Len;

    //只有在mdns启动timer0寄存器时，才能进行服务名的探测
    if(lxi_mdns_timer_type_get()== 0)
    {
        if(lxi_mdns_server_status_get() != LXI_MDNS_STATE_IDLE)//服务器处于空闲状态，不能进行探测
        {
            if(LXI_MDNS_LOG_MEMBER_GET(probe_n) > LXI_MDNS_PROBE_MIN_NUM)
            {
                //已经完成发送探测报文最小次数
                if(LXI_MDNS_LOG_MEMBER_GET(probe_n) < LXI_MDNS_PROBE_MAX_NUM)
                {
                    //未超出最大的探测报文发送次数
                    lxi_mdns_server_status_set(LXI_MDNS_STATE_PRONOUNCE);//设置服务名状态-探测成功
                    memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                    s32Len = lxi_mdns_probe_response_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),
                                                          LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET],
                                                          LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SET],
                                                          1225);
                    s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32Len);

                    if(s32Temp >0 && LXI_MDNS_LOG_MEMBER_GET(probe_n) == LXI_MDNS_PROBE_SUCCESS_NUM)
                    {
                        //发送成功,并且探测次数达到成功探测次数,保存到flash
                        lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_FLASH,
                                          LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET]);
                        
                        lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_FLASH,
                                          LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SET]);
                        lxi_mdns_name_save(LXI_MDNS_NAME_EM_HOST_FLASH);
                        lxi_mdns_name_save(LXI_MDNS_NAME_EM_SERVER_FLASH);
                    }
                }
                else
                {
                    //超出最大探测次数，复位
                    LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;
                    lxi_mdns_timer0_set(0);
                }
            }
            else
            {
                //未达到最小的探测次数,继续探测
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_FLASH , 
                                  LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);
                lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET, 
                                  LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);

                lxi_mdns_name_configure(LXI_MDNS_PACKET_SERVER,
                                        LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SET]);
            }
        }
    }
}
/**************************************************************************
*函 数 名: lxi_mdns_init
*描       述: mdns变量初始化
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_init(void)
{
    u32_t u32HostIp;
    s32_t s32Length = 0;
    s8_t txtrecord1[]="txtvers=1.path=/";//本地TXT记录
    s8_t txtrecord2[LXI_MDNS_RECORD_MAX_LEN]={0};
    s8_t manufacture[LXI_DEVICE_INFO_MAX_LEN] = {0};
    s8_t instrumentModel[LXI_DEVICE_INFO_MAX_LEN] = {0};
    s8_t serialNumber[LXI_DEVICE_INFO_MAX_LEN] = {0};
    s8_t firmwareRevision[LXI_DEVICE_INFO_MAX_LEN] = {0};

    lxi_mdns_host_status_set(LXI_MDNS_STATE_IDLE);
    lxi_mdns_server_status_set(LXI_MDNS_STATE_IDLE);
    
    u32HostIp = lxi_get_ip_addr();

    memcpy(LXI_MDNS_LOG_MEMBER_GET(ip),(s8_t*)&u32HostIp,LXI_MDNS_IP_ADDR_LEN);//初始化mdns主机IP地址

    lxi_mdns_name_convert(txtrecord1,LXI_MDNS_LOG_MEMBER_GET(httpText));//初始化Http文本记录
    //初始化Lxi文本记录
    //获得厂商信息、序列号、设备型号、版本号
    pfGetInstrumentInformation(manufacture,serialNumber,instrumentModel,firmwareRevision);

    memcpy(txtrecord2,"txtvers=1.Manufacturer=",23);
    memcpy(&txtrecord2[23],manufacture,strlen(manufacture));
    strcat(txtrecord2,".Model=");
    s32Length = strlen(txtrecord2);
    memcpy(&txtrecord2[s32Length],instrumentModel,strlen(instrumentModel));
    strcat(txtrecord2,".SerialNumber=");
    s32Length = strlen(txtrecord2);
    memcpy(&txtrecord2[s32Length],serialNumber,strlen(serialNumber));
    lxi_mdns_name_convert(txtrecord2,LXI_MDNS_LOG_MEMBER_GET(lxiText));
    
    memset(txtrecord2,0,LXI_MDNS_RECORD_MAX_LEN);	
    sprintf(txtrecord2,"%s%s","FirmwareVersion=",firmwareRevision);

    s32Length = strlen(LXI_MDNS_LOG_MEMBER_GET(lxiText));
    LXI_MDNS_LOG_MEMBER_GET(lxiText)[s32Length++] = strlen(txtrecord2);
    memcpy(&LXI_MDNS_LOG_MEMBER_GET(lxiText)[s32Length],txtrecord2,strlen(txtrecord2));
    //获取主机名服务名
    lxi_mdns_name_read();
}

/**************************************************************************
*函 数 名: lxi_mdns_configure
*描       述: mdns的配置接口
*输入参数: 
*          参数名         类型                  描述
*          s8Enable       s8_t              mdns服务开关
*          ps8Host        s8_t *            主机名
*          ps8Serv        s8_t *            服务名
*         s8IsDefault     s8_t              mdns是否恢复默认值
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_configure(s8_t s8Enable , s8_t *ps8Host , s8_t *ps8Serv ,s8_t s8IsDefault)
{
    s32_t s32Len = 0 , s32Temp = 0;
    
    if(lxi_mdns_is_support_get() == 1)//支持mdns服务
    {
        if(s8IsDefault == 1)//恢复默认值
        {
            mdns_debug("check name error!");
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SET,LXI_MDNS_DEFAULT_NAME);
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_TEMP,LXI_MDNS_DEFAULT_NAME);
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SAVE,LXI_MDNS_DEFAULT_NAME);
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET,LXI_MDNS_DEFAULT_NAME);
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_TEMP,LXI_MDNS_DEFAULT_NAME);
            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SAVE,LXI_MDNS_DEFAULT_NAME);
            /*保存用户配置主机名和服务名*/
            if(m_stLxiCallback.pfSaveUserHostName != 0)
            {
                m_stLxiCallback.pfSaveUserHostName(LXI_MDNS_DEFAULT_NAME,strlen(LXI_MDNS_DEFAULT_NAME));
            }
            if(m_stLxiCallback.pfSaveUserServiceName != 0)
            {
                m_stLxiCallback.pfSaveUserServiceName(LXI_MDNS_DEFAULT_NAME,strlen(LXI_MDNS_DEFAULT_NAME));
            } 
            lxi_mdns_enable_status_set(0);//关闭mdns服务
        }
        else
        {
            //当前mdns服务为打开状态且重新配置为打开,则根据主机名和服务名是否发生更改发生更改报文
            if(lxi_mdns_enable_status_get() == 1 && 
               s8Enable == 1 &&
               lxi_mdns_host_status_get() == LXI_MDNS_STATE_PRONOUNCE &&
               lxi_mdns_server_status_get() == LXI_MDNS_STATE_PRONOUNCE)
            {
                if(strcmp(ps8Host,LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_FLASH]) |
                   strcmp(ps8Serv,LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_FLASH]))
                {
                    memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                    s32Len = lxi_mdns_name_declaration(0,LXI_MDNS_LOG_MEMBER_GET(buff),
                                                       LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_FLASH],
                                                       LXI_MDNS_PACKET_SERVER,0);
                    lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32Len);
                }
            }
            if(s8Enable == 1)
            {
                //清空所有标识变量
                LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;
                lxi_mdns_packet_type_set(2);
                lxi_mdns_host_status_set(LXI_MDNS_STATE_PROBE);
                lxi_mdns_server_status_set(LXI_MDNS_STATE_PROBE);
                lxi_mdns_timer0_set(1);
                
                memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                s32Len = lxi_mdns_probe_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),1,
                                          ps8Host,ps8Serv,600);
                s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32Len);
                if(s32Temp > 0)
                {
                    LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                }
            }
        }
    }
}

/**************************************************************************
*函 数 名: lxi_mdns_reload_defult
*描       述: mdns恢复默认值
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_reload_defult(void)
{
    lxi_mdns_configure(0,NULL,NULL,1);
}

/**************************************************************************
*函 数 名: lxi_mdns_packet_recv
*描       述: mdns接收报文线程
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_packet_recv()
{
    struct sockaddr_in laddr = {0};//记录本地IP地址
    struct sockaddr_in raddr = {0};//远程的IP地址
    s32_t  len = sizeof(struct sockaddr_in);
    s32_t  remote_len = sizeof(raddr);
    s8_t   recv_buff[LXI_MDNS_PACKET_MAX_LEN];
    s32_t  sockfd = 0;

    /*初始化本地地址*/
    laddr.sin_family        = AF_INET;//使用IPV4通信标准
    laddr.sin_port          = htons(LXI_MDNS_HOST_PORT);   //端口号
    laddr.sin_addr.s_addr   = LXI_MDNS_HOST_IPADDR/*inet_addr("224.0.0.251")*/;  //本地地址设置为0，由Lwip内部填写该字段

    /*获取socket描述符*/
    if((sockfd = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP)) < 0)
    {
        printf("Create UDP Socket err!\n");
    }

    /*绑定本地地址。说明该Socket被占用，*/
    if(bind(sockfd,(struct sockaddr *)&laddr,sizeof(laddr)) == -1)
    {
         printf("Bind UDP Socket%s\n", strerror(errno));
    }
    
    LXI_MDNS_LOG_MEMBER_GET(sockfd) = sockfd;

    //!--------------------------------------------------------------
    //! dba+
    struct ip_mreq     host_madd_ifadd;
    host_madd_ifadd.imr_interface.s_addr = inet_addr("172.16.3.151");
    host_madd_ifadd.imr_multiaddr.s_addr = LXI_MDNS_HOST_IPADDR/*inet_addr("224.0.0.251")*/;
    int err = setsockopt(sockfd,IPPROTO_IP/*SOL_IP*/, IP_ADD_MEMBERSHIP,
                         (void*)&host_madd_ifadd, sizeof(host_madd_ifadd));
    if( err < 0 )
    {
        mdns_debug("setsockopt err:%d  errno:%d  sockfd:%d",err, errno, sockfd);
    }
    //!--------------------------------------------------------------

    mdns_debug("recv start");
    while(1)
    {
        /*接收网络上到该地址和端口的数据包，即mDNS包；注：该接收为阻塞模式*/
        if( (len = recvfrom(sockfd,recv_buff,LXI_MDNS_PACKET_MAX_LEN,0,
                            (struct sockaddr*)&raddr,(socklen_t *)&remote_len)) <0)
        {
            mdns_debug("recvfrom errno:%d  len:%d", errno, len);
        }
        else
        {
//            mdns_debug("laddr ip:%s" ,inet_ntoa(host_madd_ifadd.imr_interface));
//            mdns_debug("raddr ip:%s" ,inet_ntoa(raddr.sin_addr));
        }

        /*如果mDNS服务未打开，继续等待网络上的数据包；或 如果收到自己的包 则  不抛送事件*/
        if( (lxi_mdns_enable_status_get() == 0) ||
            (raddr.sin_addr.s_addr == host_madd_ifadd.imr_interface.s_addr) )
        {
            continue;
        }
        /*接收到数据包，抛送接收数据事件*/
        lxi_mdns_packet_set(recv_buff,len);
    }
}

/**************************************************************************
*函 数 名: lxi_mdns_timer
*描       述: mdns计时器
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_timer()
{
    u32_t u32HostIp = 0;

    lxi_mdns_timer0_set(1);//先启动计时器0

    lxi_mdns_configure(lxi_mdns_enable_status_get(),
                       LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET],
                       LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SET],
                       0);

    while(1)
    {
        if(lxi_get_cur_lan_status()!= LXI_STATUS_CONFIGURED || lxi_mdns_enable_status_get() == 0)
        {
            /*网络状态未配置成功或者mDNS开关未打开，则不进行下面的配置*/
            lxi_mdns_timer_start(0);
            continue;
        }
        if(lxi_mdns_timer0_get() == 1)
        {
            /*mDNS初始化发送的控制事件*/
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer_start(0);
            lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);
            lxi_mdns_timer0_set(0);//关闭计时器0
            lxi_mdns_timer1_set(1);//打开计时器1
        }
        else
        {
            if(lxi_mdns_timer1_get() == 1)
            {
                /*mDNS初始化完成后，间隔一定时间，发送声明报文，向其他设备报告自己的存在*/
                lxi_mdns_timer_start(5);
                lxi_mdns_event_set(LXI_MDNS_EVENT_TIMEOUT);	   
            }
            else
            {
                if(lxi_mdns_need_init_get() == 1)//该数据置为在IP应用和恢复出厂设置时置位
                {
                    /*进入重新配置环节*/
                    while(LXI_GetLanStatus()!=LXI_STATUS_CONFIGURED)
                    {
                        lxi_mdns_timer_start(0);
                    }
                    u32HostIp = lxi_get_ip_addr();
            
                    memcpy(LXI_MDNS_LOG_MEMBER_GET(ip),(s8_t*)&u32HostIp,LXI_MDNS_IP_ADDR_LEN);//初始化mdns主机IP地址
                    lxi_mdns_need_init_set(0);
                    lxi_mdns_configure(lxi_mdns_enable_status_get(),
                                       LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET],
                                       LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SET],
                                       0);
                }
            }
        }
    }
}

/*
    +---------------------+
    |        Header       |
    +---------------------+
    |       Question      | the question for the name server
    +---------------------+
    |        Answer       | RRs answering the question
    +---------------------+
    |      Authority      | RRs pointing toward an authority
    +---------------------+
    |      Additional     | RRs holding additional information
    +---------------------+

    Header section format
    The header contains the following fields:
    
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QDCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ANCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    NSCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ARCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    
*/
/**************************************************************************
*函 数 名: lxi_mdns_resource_name_parse
*描       述: mdns资源名字的解析
*输入参数: 
*          参数名             类型                  描述
*        pscPktBase          s8_t *         原mdns接收的数据包
*        ppscCurBuf          u8_t **        当前数据包的位置
*        ps32CurBufLen       s32_t  *       当前数据包的长度
*        pscDstBuf           s8_t *         保存资源名字的地址
*        s32DstBufMaxLen     s32_t          资源名字的最大长度
*输出参数: 
*返 回 值:返回0表示成功，其他值失败
*说    明: 
***************************************************************************/
s32_t lxi_mdns_resource_name_parse( s8_t *pscPktBase, 
                                                u8_t **ppscCurBuf, 
                                                s32_t  *ps32CurBufLen, 
                                                s8_t *pscDstBuf,
                                                s32_t  s32DstBufMaxLen)
{
    s32_t s32DstBufCurLen = 0;
    s32_t s32Len;
    u8_t *pscBuf, *pscTmp;
    s32_t s32IsNameCompress;
    s32_t bLastLabelIsCompress = 0;
    u16_t u16Offset;
    
    pscBuf = *ppscCurBuf;
    // NAME 分多个label存储,且其中有些label可能存在压缩情况,使用while分别取各个label
    // 压缩label只出现在 NAME 的末尾.
    // 每个 label 的最大长度为 63 字节
    // 最后一个label是一个字节的 0, 表示NAME结束
    while(*pscBuf)
    {
        s32IsNameCompress = DNS_IS_NAME_COMPRESS(*pscBuf);
        if(s32IsNameCompress)
        {
            //compression format
            //取当前 NAME 在DNS报文净荷中的偏移位置
            u16Offset = DNS_NAME_OFFSET_IN_COMPRESS((*pscBuf<<8)|((unsigned short)*(pscBuf + 1)));
            pscTmp = (u8_t *)(pscPktBase + u16Offset);
            while(*pscTmp)
            {
                s32IsNameCompress = DNS_IS_NAME_COMPRESS(*pscTmp);
                if(s32IsNameCompress)
                {
                    //取当前 NAME 在DNS报文净荷中的偏移位置
                    u16Offset = DNS_NAME_OFFSET_IN_COMPRESS((*pscTmp<<8)|((unsigned short)*(pscTmp + 1)));
                    pscTmp = (u8_t *)(pscPktBase + u16Offset);
                }
                // NAME 格式为: 1个字节的长度域 + 指定长度的字符
                s32Len = *pscTmp + 1;
                if(s32Len > DNS_NAME_LABEL_MAX_LEN)
                {
                    return -1;
                }
                memcpy(pscDstBuf,pscTmp,s32Len);
                pscDstBuf += s32Len;
                pscTmp += s32Len;
            }
            //压缩格式，此处长度仅有 2 byte
            s32Len = 2;
            bLastLabelIsCompress = 1;
        }
        else
        {
            // NAME 格式为: 1个字节的长度域 + 指定长度的字符
            s32Len = *pscBuf + 1;
            if((s32Len > *ps32CurBufLen)
                ||(s32Len > DNS_NAME_LABEL_MAX_LEN)
                ||(s32Len > (s32DstBufMaxLen - s32DstBufCurLen)))
            {
                return -2;
            }
            memcpy(pscDstBuf,pscBuf,s32Len);
            pscDstBuf += s32Len;
            s32DstBufCurLen += s32Len;
            bLastLabelIsCompress = 0;
        }
    
        if(s32DstBufCurLen >= s32DstBufMaxLen)
        {
            return -3;
        }
        pscBuf += s32Len;
        *ps32CurBufLen -= s32Len;
    }
    if(s32DstBufCurLen + 1 >= s32DstBufMaxLen)
    {
        return -4;
    }
    // NAME中的各个label结束以后,需要copy一个空的label
    memcpy(pscDstBuf,"\0",1);
    //最后一个label如果不是压缩形式,则待解析报文指针需要向后移一个byte
    if(bLastLabelIsCompress == 0)
    {
        pscBuf++;

        (*ps32CurBufLen) = (*ps32CurBufLen) - 1;
    }
    *ppscCurBuf = pscBuf;
    return 0;
}

/**************************************************************************
*函 数 名: lxi_mdns_packet_parse
*描       述: mdns报文的解析
*输入参数: 
*          参数名         类型                  描述
*          ps8Buff       s8_t *          待解析的数据包
*          s32BuffLen    s32_t           待解析数据包的长度
*输出参数: 
*返 回 值:当前接收到数据包的类型，1--询问报文,2--响应报文
*说    明: 
***************************************************************************/
s32_t lxi_mdns_packet_parse(s8_t *ps8Buff , s32_t s32BuffLen)
{
    MDNS_MESSAGE_S *pstMDNSMsg = (MDNS_MESSAGE_S *)ps8Buff;
    u16_t          *pu16Buf    = (u16_t*)ps8Buff;
    u8_t           *pscBuf     = NULL;
    s32_t           i = 0,s32Len = 0,s32PktRemainLen = s32BuffLen;

    //校验输入报文长度,DNS Header是必须有的，字节长度为12
    if(s32PktRemainLen <= DNS_HEADER_SECTION_LEN)
    {
        return -1;
    }
    //字节翻转以后，可以直接使用结构读取
    for(;i<DNS_HEADER_SECTION_LEN;i+=2)
    {
        *pu16Buf = LXI_REV_WORD(*pu16Buf);
        pu16Buf++;
    }
    s32PktRemainLen -= DNS_HEADER_SECTION_LEN;

    //指向header之后的第一个字段
    pscBuf = (u8_t *)(ps8Buff + DNS_HEADER_SECTION_LEN);	
    
    if(pstMDNSMsg->header.qr == QR_QUERY)
    {
        if(pstMDNSMsg->header.opcode > OPCODE_STATUS)
        {
            //检测到一个非法值
            return -2;
        } 
        if(pstMDNSMsg->header.aa > AA_VALID)
        {
            //检测到一个非法值
            return -3;
        }
    
        if(pstMDNSMsg->qdcount > 0)
        {	
            for(i=0; ((i < pstMDNSMsg->qdcount)&&(i < DNS_MAX_QUESTIONS_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                              LXI_MDNS_LOG_MEMBER_GET(questions)[i],
                                              DNS_SERVICE_NAME_MAX_LEN))
                {
                    return -4;
                }
                //QTYPE,QCLASS字段长度分别为 2 byte
                LXI_MDNS_LOG_MEMBER_GET(query_t)[i] = (u16_t)((*pscBuf << 8)|((u16_t)*(pscBuf + 1)));
                pscBuf += 4;
                s32PktRemainLen -= 4;
                if(s32PktRemainLen < 0)
                {
                    return -5;
                }
            }
        }
    
        if(pstMDNSMsg->ancount > 0)
        {
            for(i=0; ((i<pstMDNSMsg->ancount)&&(i < DNS_MAX_RESPONSE_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                                      LXI_MDNS_LOG_MEMBER_GET(record)[i],
                                                      DNS_SERVICE_NAME_MAX_LEN))
                {
                    return -6;
                }
            
                LXI_MDNS_LOG_MEMBER_GET(resp_t)[i] = (u16_t)((*pscBuf << 8)|((u16_t)*(pscBuf + 1)));
                //这儿的8 是 TYPE(2 byte), CLASS(2 byte), TTL(4 byte) 的长度总和
                pscBuf += 8; 
                s32PktRemainLen -= 8;
                if(s32PktRemainLen < 0)
                {
                    return -9;
                }
                //这儿取 RDLENGTH 中的长度, 协议定义该长度为 2byte长度
                //且应该加上 RDLENGTH 本身的长度(2)
                s32Len = ((*pscBuf << 8)|((u16_t)*(pscBuf + 1))) + 2;
                pscBuf += s32Len;
                s32PktRemainLen -= s32Len;
                if(s32PktRemainLen < 0)
                {
                    return -10;
                }
            }
        }
    
        if(pstMDNSMsg->nscount > 0)
        {
            for(i=0; ((i<pstMDNSMsg->nscount)&&(i < DNS_MAX_QUESTIONS_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                                     LXI_MDNS_LOG_MEMBER_GET(record)[i],
                                                     DNS_SERVICE_NAME_MAX_LEN))
                {
                    return -7;
                }
                
                LXI_MDNS_LOG_MEMBER_GET(resp_t)[i] = (u16_t)((*pscBuf << 8)|((u16_t)*(pscBuf + 1)));
                //这儿的8 是 TYPE(2 byte), CLASS(2 byte), TTL(4 byte) 的长度总和
                pscBuf += 8; 
                s32PktRemainLen -= 8;
                if(s32PktRemainLen < 0)
                {
                    return -12;
                }
                //这儿取 RDLENGTH 中的长度, 协议定义该长度为 2byte长度
                //且应该加上 RDLENGTH 本身的长度(2)
                s32Len = ((*pscBuf << 8)|((u16_t)*(pscBuf + 1))) + 2;
                pscBuf += s32Len;
                s32PktRemainLen -= s32Len;
                if(s32PktRemainLen < 0)
                {
                    return -13;
                }
            }
        }
        else
        {    
        }
        return 1;
    
    }
    else
    {
        //rcode 字段值为错误码,0表示没有错误
        if(pstMDNSMsg->header.rcode != 0)
        {
            //暂时不处理错误码
            return -14;
        }
        
        if(pstMDNSMsg->ancount> 0)
        {
            for(i=0; ((i<pstMDNSMsg->ancount)&&(i < DNS_MAX_RESPONSE_NUM)); i++)
            {
                if(0 != lxi_mdns_resource_name_parse(ps8Buff,&pscBuf,&s32PktRemainLen,
                                                     LXI_MDNS_LOG_MEMBER_GET(record)[i],
                                                     DNS_SERVICE_NAME_MAX_LEN))
                {
                    return -8;
                }
                
                LXI_MDNS_LOG_MEMBER_GET(resp_t)[i] = (u16_t)((*pscBuf << 8)|((u16_t)*(pscBuf + 1)));
                //这儿的8 是 TYPE(2 byte), CLASS(2 byte), TTL(4 byte) 的长度总和
                pscBuf += 8; 
                s32PktRemainLen -= 8;
                if(s32PktRemainLen < 0)
                {
                    return -15;
                }
                //这儿取 RDLENGTH 中的长度, 协议定义该长度为 2byte长度
                //且应该加上 RDLENGTH 本身的长度(2)
                s32Len = ((*pscBuf << 8)|((u16_t)*(pscBuf + 1))) + 2;
                pscBuf += s32Len;
                s32PktRemainLen -= s32Len;
                if(s32PktRemainLen < 0)
                {
                    return -16;
                }
            }
        }
        else
        {
            return -1;    
        }
        return 2;
    }
}

/**************************************************************************
*函 数 名: lxi_mdns_packet_find
*描       述: 根据mdns报文解析后的类型进行资源的比对以及组织报文
*输入参数: 
*          参数名         类型                  描述
*          ps8Buff       s8_t *               原数据包
*          s32Type       s32_t              数据包的类型
*          ps8Temp       s8_t *             临时数据存放的地址
*输出参数: 
*返 回 值:组织报文的长度或者命名冲突的标识
*说    明: 
***************************************************************************/
s32_t lxi_mdns_packet_find(s8_t *ps8Buff , s32_t s32Type , s8_t *ps8Temp)
{
    s8_t *pscFindName;
    s8_t scLocalName[LXI_MDNS_NAME_MAX_LEN] = {0};
    s8_t scConvertLocalName[LXI_MDNS_NAME_MAX_LEN] = {0};
    s8_t scLocalServiceName[LXI_MDNS_NAME_MAX_LEN] = {0};
    s32_t len = 0;
    LXI_MDNS_FOUND_EM emType = LXI_MDNS_FOUND_NULL;//服务类型
    s32_t slen = 0;
    s32_t llen = 0;
    s32_t ttltime = 0x00000fff;
    s32_t i;
    s32_t s32RRCount;
    u16_t u16Type;
    MDNS_MESSAGE_S *pstMDNSMsg = (MDNS_MESSAGE_S *)ps8Buff;

    LXI_MDNS_LOG_MEMBER_GET(found_t) = 0;

    if(s32Type == 1)
    {
        s32RRCount = pstMDNSMsg->qdcount;
    }
    else
    {
        s32RRCount = pstMDNSMsg->ancount;
    }

    //printf("s32type:%d,s32RRCount:%d\n",s32Type,s32RRCount);
    for(i=0;i<s32RRCount;i++)
    {
        if(s32Type ==1)//查询报文查找
        {
            pscFindName = LXI_MDNS_LOG_MEMBER_GET(questions)[i];
            lxi_mdns_name_case_change(pscFindName);
            u16Type = LXI_MDNS_LOG_MEMBER_GET(query_t)[i];
        }
        else
        {
            pscFindName = LXI_MDNS_LOG_MEMBER_GET(record)[i];
            lxi_mdns_name_case_change(pscFindName);
            u16Type = LXI_MDNS_LOG_MEMBER_GET(resp_t)[i];
        }

        if(u16Type == LXI_MDNS_QUERY_ALL)
        {
            u16Type = LXI_MDNS_QUERY_HOST_ADDR;
        }

        switch(u16Type)
        {
            case LXI_MDNS_QUERY_HOST_ADDR:
            case LXI_MDNS_QUERY_SERVER_NAME:
            case LXI_MDNS_QUERY_AAAA:
            {
                //主机名的询问请求操作和响应报文
                memset(scLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                lxi_mdns_name_get(LXI_MDNS_NAME_EM_HOST_SAVE,scLocalName);
                strcat(scLocalName,".local");
                lxi_mdns_name_convert(scLocalName,scConvertLocalName);

                if(strcmp(pscFindName , scConvertLocalName) == 0)//查找到
                {
                    LXI_MDNS_LOG_MEMBER_GET(query_t)[i] = 1;
                    if(s32Type == 2)
                    {
                        if(LXI_MDNS_LOG_MEMBER_GET(found_t) == 2)//如果服务名也冲突
                        {
                            LXI_MDNS_LOG_MEMBER_GET(found_t) = 3;//主机名服务名全部冲突
                        }
                        else
                        {
                            LXI_MDNS_LOG_MEMBER_GET(found_t) = 1;//主机名冲突
                        }
                        break;
                    }
                    //组织响应报文
                    strcpy(&ps8Temp[len],pscFindName);
                    len += strlen(pscFindName);
                    ps8Temp[len++]   = (s8_t)0;
                    
                    ps8Temp[len++] = (s8_t)0;
                    ps8Temp[len++] = (s8_t)1;
                    
                    ps8Temp[len++] = (s8_t)0;
                    ps8Temp[len++] = (s8_t)1;
                    
                    ps8Temp[len++] = (s8_t)(ttltime >> 24);
                    ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                    ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                    ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);
                    
                    ps8Temp[len++] = (s8_t)0;
                    ps8Temp[len++] = (s8_t)4;
                    
                    memcpy(&ps8Temp[len],LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);
                    len += LXI_MDNS_IP_ADDR_LEN;
                    break;
                }
                else
                {
                    u16Type = LXI_MDNS_QUERY_ALL; 
                }
            }
            case LXI_MDNS_QUERY_PTR:
            case LXI_MDNS_QUERY_SRV:
            case LXI_MDNS_QUERY_TXT:
            case LXI_MDNS_QUERY_ALL:
            {
                if(lxi_mdns_enable_status_get() == 0 && s32Type == 1)
                {
                    return len;
                }
                if(lxi_mdns_server_status_get() == LXI_MDNS_STATE_IDLE)
                {
                    return 0;
                }

                //开始服务名的比对
                memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                strcpy(scLocalName,"_http._tcp.local");
                lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                {
                    strcpy(&ps8Temp[len],scConvertLocalName);
                    len += strlen(scConvertLocalName);
                    emType = LXI_MDNS_FOUND_NO_SERVER_HTTP;//http服务
                }
                else
                {
                    memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                    memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                    strcpy(scLocalName,"_lxi._tcp.local");
                    lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                    if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                    {
                        strcpy(&ps8Temp[len],scConvertLocalName);
                        len += strlen(scConvertLocalName);
                        emType = LXI_MDNS_FOUND_NO_SERVER_LXI;//lxi服务
                    }
                    else
                    {
                        memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                        memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                        strcpy(scLocalName,"_vxi-11._tcp.local");
                        lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                        if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                        {
                            strcpy(&ps8Temp[len],scConvertLocalName);
                            len += strlen(scConvertLocalName);
                            emType = LXI_MDNS_FOUND_NO_SERVER_VXI11;//vxi-11服务
                        }
                        else
                        {
                            memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                            memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                            strcpy(scLocalName,"_scpi-raw._tcp.local");
                            lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                            if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                            {
                                strcpy(&ps8Temp[len],scConvertLocalName);
                                len += strlen(scConvertLocalName);
                                emType = LXI_MDNS_FOUND_NO_SERVER_SCPI;//scpi服务
                            }
                            else
                            {
                                memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                                memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                                lxi_mdns_name_get(LXI_MDNS_NAME_EM_SERVER_SAVE,scLocalName);
                                strcat(scLocalName,"._http._tcp.local");
                                lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                                if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                                {
                                    emType = LXI_MDNS_FOUND_SERVER_HTTP;//http服务

                                    if(s32Type == 2)
                                    {
                                        if(LXI_MDNS_LOG_MEMBER_GET(found_t) == 1)//如果主机名也冲突
                                        {
                                            LXI_MDNS_LOG_MEMBER_GET(found_t) = 3;//主机名服务名全部冲突
                                        }
                                        else
                                        {
                                            LXI_MDNS_LOG_MEMBER_GET(found_t) = 2;//服务名冲突
                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                                    memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                                    lxi_mdns_name_get(LXI_MDNS_NAME_EM_SERVER_SAVE,scLocalName);
                                    strcat(scLocalName,"._lxi._tcp.local");
                                    lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                                    if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                                    {
                                        emType = LXI_MDNS_FOUND_SERVER_LXI;//lxi服务

                                        if(s32Type == 2)
                                        {
                                            if(LXI_MDNS_LOG_MEMBER_GET(found_t) == 1)//如果主机名也冲突
                                            {
                                                LXI_MDNS_LOG_MEMBER_GET(found_t) = 3;//主机名服务名全部冲突
                                            }
                                            else
                                            {
                                                LXI_MDNS_LOG_MEMBER_GET(found_t) = 2;//服务名冲突
                                            }
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                                        memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                                        lxi_mdns_name_get(LXI_MDNS_NAME_EM_SERVER_SAVE,scLocalName);
                                        strcat(scLocalName,"._vxi-11._tcp.local");
                                        lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                                        if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                                        {
                                            emType = LXI_MDNS_FOUND_SERVER_VXI11;//vxi-11服务

                                            if(s32Type == 2)
                                            {
                                                if(LXI_MDNS_LOG_MEMBER_GET(found_t) == 1)//如果主机名也冲突
                                                {
                                                    LXI_MDNS_LOG_MEMBER_GET(found_t) = 3;//主机名服务名全部冲突
                                                }
                                                else
                                                {
                                                    LXI_MDNS_LOG_MEMBER_GET(found_t) = 2;//服务名冲突
                                                }
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                                            memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                                            lxi_mdns_name_get(LXI_MDNS_NAME_EM_SERVER_SAVE,scLocalName);
                                            strcat(scLocalName,"._scpi-raw._tcp.local");
                                            lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                                            if(strcmp(pscFindName,scConvertLocalName) == 0)//查找到
                                            {
                                                emType = LXI_MDNS_FOUND_SERVER_SCPI;//scpi服务

                                                if(s32Type == 2)
                                                {
                                                    if(LXI_MDNS_LOG_MEMBER_GET(found_t) == 1)//如果主机名也冲突
                                                    {
                                                        LXI_MDNS_LOG_MEMBER_GET(found_t) = 3;//主机名服务名全部冲突
                                                    }
                                                    else
                                                    {
                                                        LXI_MDNS_LOG_MEMBER_GET(found_t) = 2;//服务名冲突
                                                    }
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                //未查找到
                                                break;
                                            }
                                        }
                                    }//LXI_MDNS_FOUND_SERVER_LXI结束
                                }//LXI_MDNS_FOUND_SERVER_HTTP结束
                            }//LXI_MDNS_FOUND_NO_SERVER_SCPI结束
                        }//LXI_MDNS_FOUND_NO_SERVER_LXI结束
                    }//LXI_MDNS_FOUND_NO_SERVER_LXI结束
                }//LXI_MDNS_FOUND_NO_SERVER_HTTP 结束
                //根据查找的服务资源，进行组织响应报文
                switch(emType)
                {
                    case LXI_MDNS_FOUND_NO_SERVER_HTTP:
                    case LXI_MDNS_FOUND_NO_SERVER_LXI:
                    case LXI_MDNS_FOUND_NO_SERVER_VXI11:
                    case LXI_MDNS_FOUND_NO_SERVER_SCPI:
                    {
                        if(u16Type == LXI_MDNS_QUERY_TXT || u16Type == LXI_MDNS_QUERY_SRV)
                        {
                            //如果当前请求的是服务器的TXT记录和请求的是服务器名，则报文出错直接退出
                            return 0;
                        }
                        ps8Temp[len++] = (s8_t)0;
                    
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)12;
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)1;
                        
                        ps8Temp[len++] = (s8_t)(ttltime >> 24);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                        ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);

                        ps8Temp[len++] = (s8_t)(slen >> 8);
                        ps8Temp[len++] = (s8_t)(slen & 0x00ff);
                        
                        memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                        memset(scConvertLocalName , 0 , LXI_MDNS_NAME_MAX_LEN);
                        lxi_mdns_name_get(LXI_MDNS_NAME_EM_SERVER_FLASH,scLocalName);
                        slen = lxi_mdns_name_convert(scLocalName,scConvertLocalName);
                        scConvertLocalName[slen++] = 0xc0;
                        scConvertLocalName[slen++] = 0x0c;
                        strcpy(&ps8Temp[len],scConvertLocalName);
                        len += slen;
                        break;
                    }
                    case LXI_MDNS_FOUND_SERVER_HTTP:
                    {
                        memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                        memset(scLocalServiceName, 0 ,LXI_MDNS_NAME_MAX_LEN);

                        lxi_mdns_name_get(LXI_MDNS_NAME_EM_HOST_FLASH,scLocalName);
                        strcat(scLocalName,".local");
                        slen = lxi_mdns_name_convert(scLocalName,scLocalServiceName);
                        strcpy(&ps8Temp[len] , scLocalServiceName);
                        len += slen;

                        ps8Temp[len++] = (s8_t)0;
                    
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)1;
                        
                        ps8Temp[len++] = (s8_t)0x80;
                        ps8Temp[len++] = (s8_t)1;
                        
                        ps8Temp[len++] = (s8_t)(ttltime >> 24);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                        ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)4;
                        
                        memcpy(&ps8Temp[len],LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);
                        len += LXI_MDNS_IP_ADDR_LEN;
                        llen = len;
                        
                        strcpy(&ps8Temp[len],scConvertLocalName);
                        len += strlen(scConvertLocalName);
                        ps8Temp[len++] = (s8_t)0;
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0x21;
                        
                        ps8Temp[len++] = (s8_t)0x80;
                        ps8Temp[len++] = (s8_t)1;
                        
                        ps8Temp[len++] = (s8_t)(ttltime >> 24);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                        ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)8;
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0;
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0;
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)80;
                        
                        ps8Temp[len++] = (s8_t)0xc0;
                        ps8Temp[len++] = (s8_t)0x0c;
                        
                        ps8Temp[len++] = (s8_t)0xc0;
                        ps8Temp[len++] = (s8_t)(llen + 12);
                        
                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0x10;
                        
                        ps8Temp[len++] = (s8_t)0x80;
                        ps8Temp[len++] = (s8_t)1;
                        
                        ps8Temp[len++] = (s8_t)(ttltime >> 24);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                        ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);
                        
                        slen = strlen(LXI_MDNS_LOG_MEMBER_GET(httpText));
                        ps8Temp[len++] = (s8_t)(slen >> 8);
                        ps8Temp[len++] = (s8_t)(slen & 0x00ff);
                        
                        strcpy(&ps8Temp[len],LXI_MDNS_LOG_MEMBER_GET(httpText));
                        len += slen;
                        break;
                    }
                    case LXI_MDNS_FOUND_SERVER_LXI:
                    case LXI_MDNS_FOUND_SERVER_VXI11:
                    case LXI_MDNS_FOUND_SERVER_SCPI:
                    {
                        memset(scLocalName,0,LXI_MDNS_NAME_MAX_LEN);
                        memset(scLocalServiceName, 0 ,LXI_MDNS_NAME_MAX_LEN);

                        lxi_mdns_name_get(LXI_MDNS_NAME_EM_HOST_FLASH,scLocalName);
                        strcat(scLocalName,".local");
                        slen = lxi_mdns_name_convert(scLocalName,scLocalServiceName);
                        strcpy(&ps8Temp[len] , scLocalServiceName);
                        len += slen;
                        ps8Temp[len++] = (s8_t)0;

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)1;

                        ps8Temp[len++] = (s8_t)0x80;
                        ps8Temp[len++] = (s8_t)1;

                        ps8Temp[len++] = (s8_t)(ttltime >> 24);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                        ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)4;

                        memcpy(&ps8Temp[len],LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN);
                        len += LXI_MDNS_IP_ADDR_LEN;
                        llen = len;

                        strcpy(&ps8Temp[len],scConvertLocalName);
                        len += strlen(scConvertLocalName);
                        ps8Temp[len++] = (s8_t)0;

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0x21;

                        ps8Temp[len++] = (s8_t)0x80;
                        ps8Temp[len++] = (s8_t)1;

                        ps8Temp[len++] = (s8_t)(ttltime >> 24);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                        ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)8;

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0;

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0;

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)80;

                        ps8Temp[len++] = (s8_t)0xc0;
                        ps8Temp[len++] = (s8_t)0x0c;

                        ps8Temp[len++] = (s8_t)0xc0;
                        ps8Temp[len++] = (s8_t)(llen + 12);

                        ps8Temp[len++] = (s8_t)0;
                        ps8Temp[len++] = (s8_t)0x10;

                        ps8Temp[len++] = (s8_t)0x80;
                        ps8Temp[len++] = (s8_t)1;

                        ps8Temp[len++] = (s8_t)(ttltime >> 24);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x0fff) >> 16);
                        ps8Temp[len++] = (s8_t)((ttltime & 0x00ff) >> 8);
                        ps8Temp[len++] = (s8_t)(ttltime & 0x00ff);

                        slen = strlen(LXI_MDNS_LOG_MEMBER_GET(lxiText));
                        ps8Temp[len++] = (s8_t)(slen >> 8);
                        ps8Temp[len++] = (s8_t)(slen & 0x00ff);

                        strcpy(&ps8Temp[len],LXI_MDNS_LOG_MEMBER_GET(lxiText));
                        len += slen;
                        break;
                    }
                    default:
                    {
                        //数据包错误，直接返回
                        return -1;
                    }
                }
                break;
            }//case LXI_MDNS_QUERY_PTR 结束
            default:
            {
                break;
            }
        }
    }
    if(s32Type == 2)
    {
        return LXI_MDNS_LOG_MEMBER_GET(found_t);
    }

    return len;
}
/**************************************************************************
*函 数 名: lxi_mdns_event_process
*描       述: mdns事件处理线程
*输入参数: 
*          参数名         类型                  描述
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_event_process()
{
    s32_t s32PacketType = 0 , s32SendLen = 0 , s32Temp = 0 , s32RecvLen = 0;
    s8_t  as8RecvBuf[LXI_MDNS_PACKET_MAX_LEN] = {0};
    s8_t  as8TempBuf[LXI_MDNS_PACKET_MAX_LEN] = {0};
    MDNS_MESSAGE_S *pstMDNSMsg = NULL;

    while(1)
    {
        if(lxi_get_cur_lan_status() != LXI_STATUS_CONFIGURED || lxi_mdns_enable_status_get() == 0)
        {
            lxi_mdns_timer_start(0);
            continue;
        }
        
	switch(lxi_mdns_event_get())
        {
            case LXI_MDNS_EVENT_TIMEOUT:
            {
                if(lxi_mdns_packet_type_get() == 0)//配置主机名
                {
                    lxi_mdns_host_probe();
                }
                else if(lxi_mdns_packet_type_get() == 1)//配置服务名
                {
                    lxi_mdns_server_probe();
                }
                else
                {
                    //主机名和服务名都进行配置和声明
                    if(LXI_MDNS_LOG_MEMBER_GET(probe_n) > 5)
                    {
                        if(LXI_MDNS_LOG_MEMBER_GET(probe_n) < 20)
                        {
                            lxi_mdns_host_status_set(LXI_MDNS_STATE_PRONOUNCE);//设置主机名状态-探测成功
                            lxi_mdns_server_status_set(LXI_MDNS_STATE_PRONOUNCE);//设置服务名状态-探测成功
                            //MDNS协议规定:6次探测后，均未有其他主机响应有冲突时，认为本次声明有效，则保存主机名和服务名
                            if(LXI_MDNS_LOG_MEMBER_GET(probe_n) == LXI_MDNS_PROBE_SUCCESS_NUM)
                            {
                                //发送成功,并且探测次数达到成功探测次数,保存到flash
                                lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_FLASH,
                                                  LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET]);
                                
                                lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_FLASH,
                                                  LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SET]);
                                lxi_mdns_name_save(LXI_MDNS_NAME_EM_HOST_FLASH);
                                lxi_mdns_name_save(LXI_MDNS_NAME_EM_SERVER_FLASH);
                            }
                            memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                            s32SendLen = lxi_mdns_probe_response_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),
                                                                        LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET],
                                                                        LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SET],
                                                                        1225);
                        }
                        else
                        {
                            //超出最大探测次数，复位
                            LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;
                            lxi_mdns_timer0_set(0);
                            break;
                        }
                    }
                    else
                    {
                        lxi_mdns_host_status_set(LXI_MDNS_STATE_PROBE);
                        lxi_mdns_server_status_set(LXI_MDNS_STATE_PROBE);
                        memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                        s32SendLen = lxi_mdns_probe_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),2,
                                                                 LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE],
                                                                 LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE],
                                                                 1225);
                    }
                    s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32SendLen);
                    if(s32Temp > 0)
                    {
                        LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                    }
                }
                break;
            }
            case LXI_MDNS_EVENT_PACKET_PASER:
            {
                memset(as8RecvBuf,0,LXI_MDNS_PACKET_MAX_LEN);
                if(lxi_mdns_packet_get(as8RecvBuf,&s32RecvLen) == -1)
                {
                    break;
                }
                s32PacketType = lxi_mdns_packet_parse(as8RecvBuf,s32RecvLen);
		
		if(s32PacketType == 1)//查询报文
                {
                    pstMDNSMsg = (MDNS_MESSAGE_S*)as8RecvBuf;

                    if(pstMDNSMsg->header.opcode == OPCODE_QUERY)//查询报文
                    {
                        //主机地址查询且声明完成后才进行响应
                        if(LXI_MDNS_LOG_MEMBER_GET(query_t)[0] != LXI_MDNS_QUERY_ALL &&
                           lxi_mdns_host_status_get() == LXI_MDNS_STATE_PRONOUNCE)
                        {
                            s32Temp = lxi_mdns_packet_find(as8RecvBuf,1,as8TempBuf);
                            if(s32Temp > 0)
                            {
                                memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                                s32SendLen = lxi_mdns_single_response_make(pstMDNSMsg->id,LXI_MDNS_LOG_MEMBER_GET(buff),
                                                                           (LXI_MDNS_QUERY_EM)(LXI_MDNS_LOG_MEMBER_GET(query_t)[0]),
                                                                           1255,as8TempBuf,s32Temp);
                                lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32SendLen);
                            }
                        }
                        else//包含服务器查询
                        {
                            if(lxi_mdns_host_status_get() == LXI_MDNS_STATE_PRONOUNCE)//探测完成
                            {
                                s32Temp = lxi_mdns_packet_find(as8RecvBuf,1,as8TempBuf);
                                if(s32Temp > 0)
                                {
                                    memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                                    s32SendLen = lxi_mdns_single_response_make(pstMDNSMsg->id,LXI_MDNS_LOG_MEMBER_GET(buff),
                                                                               (LXI_MDNS_QUERY_EM)(LXI_MDNS_LOG_MEMBER_GET(query_t)[0]),
                                                                               1255,as8TempBuf,s32Temp);
                                    lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32SendLen);
                                }
                            }
                            else if(lxi_mdns_host_status_get() == LXI_MDNS_STATE_PROBE)//探测阶段
                            {
                                if(strncmp(LXI_MDNS_LOG_MEMBER_GET(record)[0],LXI_MDNS_LOG_MEMBER_GET(ip),LXI_MDNS_IP_ADDR_LEN) != 0)
                                {
                                    //主机名发生冲突
                                    lxi_mdns_host_status_set(LXI_MDNS_STATE_CONFLICT);
                                    lxi_mdns_name_rename(LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET]);
                                    lxi_mdns_host_status_set(LXI_MDNS_STATE_PROBE);
                                    LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;
                                    lxi_mdns_name_configure(LXI_MDNS_PACKET_HOST,
                                                            LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SET]);
                                    
                                }
                            }
                        }
                    }
                }
                else if(s32PacketType == 2)
                {
                    if(lxi_mdns_host_status_get() == LXI_MDNS_STATE_PRONOUNCE ||
                       lxi_mdns_host_status_get() == LXI_MDNS_STATE_PROBE)
                    {
                        s32Temp = lxi_mdns_packet_find(as8RecvBuf,2,as8TempBuf);

                        if(s32Temp == 3)//主机名和服务名全部冲突
                        {
                            lxi_mdns_host_status_set(LXI_MDNS_STATE_CONFLICT);
                            lxi_mdns_server_status_set(LXI_MDNS_STATE_CONFLICT);

                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_TEMP,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE]);
                            lxi_mdns_name_rename(LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE]);
                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SET,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE]);

                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_TEMP,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);
                            lxi_mdns_name_rename(LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);
                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);

                            LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;

                            memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                            s32SendLen = lxi_mdns_probe_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),1,
                                                                     LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE],
                                                                     LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE],
                                                                     1255);
                            s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32SendLen);
                            if(s32Temp > 0)
                            {
                                LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                                lxi_mdns_timer0_set(1);//打开计时器0
                            }  
                        }
                        else if(s32Temp == 2)//服务器名冲突
                        {
                            lxi_mdns_server_status_set(LXI_MDNS_STATE_CONFLICT);
                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_TEMP,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);
                            lxi_mdns_name_rename(LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);
                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE]);
                            LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;

                            memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                            s32SendLen = lxi_mdns_probe_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),1,
                                                                     LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE],
                                                                     LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE],
                                                                     1255);
                            s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32SendLen);
                            if(s32Temp > 0)
                            {
                                LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                                lxi_mdns_timer0_set(1);//打开计时器0
                            }  
                        }
                        else if(s32Temp == 1)//主机名冲突
                        {
                            lxi_mdns_host_status_set(LXI_MDNS_STATE_CONFLICT);
                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_TEMP,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE]);
                            lxi_mdns_name_rename(LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE]);
                            lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SET,
                                              LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE]);
                            LXI_MDNS_LOG_MEMBER_GET(probe_n) = 0;

                            memset(LXI_MDNS_LOG_MEMBER_GET(buff),0,LXI_MDNS_PACKET_MAX_LEN);
                            s32SendLen = lxi_mdns_probe_query_make(0,LXI_MDNS_LOG_MEMBER_GET(buff),1,
                                                                     LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_HOST_SAVE],
                                                                     LXI_MDNS_LOG_MEMBER_GET(name)[LXI_MDNS_NAME_EM_SERVER_SAVE],
                                                                     1255);
                            s32Temp = lxi_mdns_packet_send(LXI_MDNS_LOG_MEMBER_GET(buff),s32SendLen);
                            if(s32Temp > 0)
                            {
                                LXI_MDNS_LOG_MEMBER_GET(probe_n)++;
                                lxi_mdns_timer0_set(1);//打开计时器0
                            }  
                        }
                    }
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }    
}

/**************************************************************************
*函 数 名: lxi_mdns_open
*描       述: 创建mdns服务
*输入参数: 
*输出参数: 
*返 回 值:
*说    明: 
***************************************************************************/
void lxi_mdns_open(void)
{
    int ret;
    pthread_t idmDNS_Packet_R;
    pthread_t idmDNS_Event_P;
    pthread_t idmDNS_Timer;
    
    if(lxi_mdns_is_support_get() == 0)
    {
        //不支持mdns,则退出创建
        return;
    }
    m_eLxiCurrLanStatus = LXI_STATUS_CONFIGURED;
    while(LXI_GetLanStatus()!=LXI_STATUS_CONFIGURED)
    {
        /*mDNS服务检测当先网络是否完成配置后，再初始化mDNs服务线程*/ 
        lxi_mdns_timer_start(5);
    }
#ifdef _LXI_USE_MEM_ALLOC
    s8_t *ps8MemAddr ; 
    ps8MemAddr = m_stLxiCallback.pfMemAlloc(sizeof(LXI_MDNS_LOG_STRU) + LWIP_ALIGNMENT);
    if(ps8MemAddr == NULL)
    {
	    printf("mdns lxi_mdns_open m_stMdnsLog mem alloc err!\n");
	    exit(0);
    }
    
    m_stMdnsLog = (LXI_MDNS_LOG_STRU*)LWIP_ALIGN((u32_t)ps8MemAddr);
    memset(m_stMdnsLog,0,sizeof(LXI_MDNS_LOG_STRU));
		        
    ps8MemAddr = m_stLxiCallback.pfMemAlloc(sizeof(LXI_MDNS_QUEUE_STRU) + LWIP_ALIGNMENT);
    if(ps8MemAddr == NULL)
    {
	    printf("mdns lxi_mdns_open m_stMdnsQueue mem alloc err!\n");
	    exit(0);
    }
    m_stMdnsQueue = (LXI_MDNS_QUEUE_STRU*)LWIP_ALIGN((u32_t)ps8MemAddr);
    memset(m_stMdnsQueue,0,sizeof(LXI_MDNS_QUEUE_STRU));
#endif
    sem_init(&LXI_MDNS_QUEUE_GET(event).sem,0,0);
    
    //lxi_mdns_init();
    //创建mdns报文接收线程
    ret=pthread_create(&idmDNS_Packet_R,NULL,(void *) lxi_mdns_packet_recv,NULL);
    if(ret!=0)
    {
	    printf ("Create mdns packet recv pthread error!\n");
    }

    while(LXI_MDNS_LOG_MEMBER_GET(sockfd) <= 0)//等待获取UDP-Socket编号
    {
        lxi_mdns_timer_start(0);
    }
    
    ret=pthread_create(&idmDNS_Event_P,NULL,(void *) lxi_mdns_event_process,NULL);
    if(ret!=0)
    {
	    printf ("Create mdns event process pthread error!\n");
    }
    
    ret=pthread_create(&idmDNS_Timer,NULL,(void *) lxi_mdns_timer,NULL);
    if(ret!=0)
    {
	    printf ("Create mdns timer pthread error!\n");
    }

    pthread_join(idmDNS_Packet_R,NULL);
    pthread_join(idmDNS_Event_P,NULL);
    pthread_join(idmDNS_Timer,NULL);
}

