#ifndef _CC_H_
#define _CC_H_

#ifdef __cplusplus
extern "C"{
#endif

typedef unsigned char u8_t;
typedef unsigned short u16_t;
typedef unsigned int u32_t;
typedef char	s8_t;
typedef short	s16_t;
typedef int	s32_t;

#ifdef __cplusplus
}
#endif


#endif
