#include "mdnsuser.h"

#include "Cc.h"
#include "lxi_arch.h"
#include "mdns.h"

#include <QFile>
#include <QDir>
#define SAVE_PATH       "/user/"
#define SAVE_HOST_PATH  ("host.txt")
#define SAVE_SERV_PATH  ("serv.txt")
#define SAVE_TXT_PATH   ("text.txt")

/*!
 * \brief getTxtFile 读取一个文本中的全部内容
 * \param path 路径
 * \param str 输出的内容
 */
static bool readAllFile(QString path, QString &str)
{
  path = QString(SAVE_PATH)+path;
  QFile file(path);
  if(!file.open( QIODevice::ReadOnly))
  {
      return false;
  }
  QTextStream in(&file);
  str = in.readAll();
  file.close();
  return true;
}

static bool writeAllFile(QString name, QString &str)
{
    QString path(SAVE_PATH);
    //!检查文件是否存在 如果不存在则创建一个
    QDir path_dir;
    if(!path_dir.exists(path))
    {
        path_dir.mkpath(path);
    }

    QFile file(path + name);
    if(!file.open( QIODevice::WriteOnly | QIODevice::Text ))
    {
        return false;
    }
    QTextStream in(&file);
    in<<str;
    file.close();
    return true;
}

Cmdns::Cmdns()
{  
}

Cmdns::~Cmdns()
{
}

void Cmdns::run()
{
    lxi_mdns_open();
}

int Cmdns::init(QString serial)
{
    QString name = QString("rigol_%1").arg(serial);
    memcpy(host_name, name.toLatin1().data(), qMin(name.count(),LXI_MDNS_NAME_MAX_LEN ));
    mdns_debug("%d",qMin(name.count(),LXI_MDNS_NAME_MAX_LEN ));

    m_stLxiCallback.pfSaveHostName        = &SaveHostName;
    m_stLxiCallback.pfSaveServiceName     = &SaveServiceName;
    m_stLxiCallback.pfSaveUserHostName    = &SaveUsrHostName;
    m_stLxiCallback.pfSaveUserServiceName = &SaveUsrServiceName;

    m_stLxiCallback.pfReadHostName        = &ReadHostName;
    m_stLxiCallback.pfReadServiceName     = &ReadServiceName;
    m_stLxiCallback.pfReadUserHostName    = &ReadUsrHostName;
    m_stLxiCallback.pfReadUserServiceName = &ReadUsrServiceName;

    //!初始化mdns服务
    lxi_mdns_need_init_set(0)    ;
    lxi_mdns_enable_status_set(1);
    lxi_mdns_init();

    return 0;
}

int Cmdns::setEnable(bool b)
{
    lxi_mdns_init();

    s8_t value = b? 1 : 0;
    lxi_mdns_enable_status_set(value);
    return 0;
}

int Cmdns::setHostName( QString name)
{
    return lxi_mdns_name_set(LXI_MDNS_NAME_EM_HOST_SET, name.toLatin1().data() );
}

int Cmdns::setServName(QString name)
{
    return lxi_mdns_name_set(LXI_MDNS_NAME_EM_SERVER_SET, name.toLatin1().data() );
}

QString Cmdns::getHostName()
{
    s8_t name[LXI_MDNS_NAME_MAX_LEN] = {0};
    if( (-1) == lxi_mdns_name_get(LXI_MDNS_NAME_EM_HOST_SET, name ) )
    {
        return LXI_MDNS_DEFAULT_NAME;
    }
   return QString(name);
}

QString Cmdns::getServName()
{
    s8_t name[LXI_MDNS_NAME_MAX_LEN] = {0};
    if( (-1) == lxi_mdns_name_get(LXI_MDNS_NAME_EM_SERVER_SET, name ) )
    {
        return LXI_MDNS_DEFAULT_NAME;
    }

   return QString(name);
}

int Cmdns::SaveHostName(char *pscHostName, int length)
{
    if(length>0)
    {
        QString name(pscHostName);
        mdns_debug("host name:%s",pscHostName);
        writeAllFile(SAVE_HOST_PATH, name);
    }

    return 0;
}

int Cmdns::SaveServiceName(char *pscServiceName, int length)
{
    if(length>0)
    {
        QString name(pscServiceName);
        mdns_debug("service name:%s",name.toLatin1().data());
        writeAllFile(SAVE_SERV_PATH, name);
    }
    return 0;
}

int Cmdns::SaveUsrHostName(char *pscHostName, int length)
{
    if(length>0)
    {
        QString name(pscHostName);
        //qDebug()<<"user host name:"<<name;
        writeAllFile(SAVE_HOST_PATH, name);
    }
    return 0;
}

int Cmdns::SaveUsrServiceName(char *pscServiceName, int length)
{
    if(length>0)
    {
        QString name(pscServiceName);
        //qDebug()<<"user Serv name:"<<name;
        writeAllFile(SAVE_HOST_PATH, name);
    }
    return 0;
}

int Cmdns::ReadHostName(char *pscHostName, int *pLength)
{
    QString name;

    Q_ASSERT( (pscHostName != NULL) && (pLength != NULL));

    if( (!readAllFile(SAVE_HOST_PATH,name))
            || (name.isEmpty())
            || (name.count() > LXI_MDNS_NAME_MAX_LEN)
            )
    {
        name = QString(LXI_MDNS_DEFAULT_NAME);
    }

    *pLength = name.count();
    memcpy(pscHostName, name.toLatin1().data(),*pLength);
    mdns_debug("%s",pscHostName);
    return 0;
}

int Cmdns::ReadServiceName(char *pscServiceName, int *pLength)
{
    QString name;

    Q_ASSERT( (pscServiceName != NULL) && (pLength != NULL));

    if( (!readAllFile(SAVE_SERV_PATH,name))
            || (name.isEmpty())
            || (name.count() < LXI_MDNS_NAME_MAX_LEN)
            )
    {
        name = QString(LXI_MDNS_DEFAULT_NAME);
    }

    *pLength = name.count();
    memcpy(pscServiceName, name.toLatin1().data(),*pLength);
    mdns_debug("%s",pscServiceName);
    return 0;
}

int Cmdns::ReadUsrHostName(char *pscHostName, int *pLength)
{
    return ReadHostName(pscHostName, pLength);
}

int Cmdns::ReadUsrServiceName(char *pscServiceName, int *pLength)
{
    return ReadServiceName(pscServiceName, pLength);
}

Cmdns mdnsThread;
