
#include "cwordoption.h"

/*!
 * \brief CWordPage::setWords
 * \param str
 * - 设置一页要显示的汉字
 * - 汉字没有分隔符，连接在一个string上
 */
void CWordPage::setWords( const QString& str )
{
    mWordh_List.append( str );
}


/*!
 * \brief CWordPage::getViewText
 * \return
 * - 获取一页汉字的显示视图
 * - 页中汉字显示格式: 顺序号+.+汉字
 */
QString CWordPage::getViewText()
{
    int i;
    QString strView;
    QString str;
    for ( i = 0; i < mWordh_List.length(); i++ )
    {
        str = QString("%1.%2 ").arg( i + 1 ).arg( mWordh_List.at(i) );
        strView += str;
    }
    //! 去掉首尾的空格
    strView = strView.simplified();
    return strView;
}

/*!
 * \brief CWordPage::select
 * \param id
 * \return
 * - 从一页中通过序号提取出字符
 * - 顺序号从1开始
 */
QString CWordPage::select( int id )
{
    if ( id < 1 || id > mWordh_List.length() )
    { return ""; }

    return mWordh_List.at( id - 1 );
}

QStringList& CWordPage::getView()
{
    mWordList.clear();

    mWordList = mWordh_List;

    return mWordList;
}

int CWordOption::_pageCount = 5;
CWordOption::CWordOption()
{
    mPageNow = 0;
    mPageCnt = 0;
}

/*!
 * \brief CWordOption::setText
 * \param list
 * - 一个拼音和多个sibling匹配
 * - 一个sibling形成一个string
 * - 所以，一个拼音和多个string匹配
 */
void CWordOption::setText( QStringList& list )
{
    //! 平化stringlist到string
    mOptionsList =  list;
    //! 根据每页的数量和总匹配汉字数量计算出页数
    mPageCnt = ( list.size() + CWordOption::_pageCount - 1 )/CWordOption::_pageCount;
    mPageNow = 0;
}

CWordPage CWordOption::getPage()
{
    int count;

    //! 在剩余显示项数量和页数量间取小值
    count = qMin( mOptionsList .size() - mPageNow * CWordOption::_pageCount,
                 CWordOption::_pageCount );

    CWordPage page;
    int i = 0;
    for(i = 0; i < count; i++)
         page.setWords( mOptionsList.at( mPageNow * CWordOption::_pageCount + i));
    return page;
}

bool CWordOption::isBof()
{ return (mPageCnt == 0) || (mPageNow == 0); }
bool CWordOption::isEof()
{ return (mPageCnt == 0) || (mPageNow==(mPageCnt-1)); }
bool CWordOption::isValid()
{ return (mPageCnt>0); }

void CWordOption::moveNext()
{
    if ( mPageNow < mPageCnt-1 )
    { mPageNow++;}
}
void CWordOption::movePrevious()
{
    if ( mPageNow > 0 )
    { mPageNow--; }
}

void CWordOption::clear()
{
    mOptions.clear();
    mPageNow = mPageCnt = 0;
    mOptionsList.clear();
}
