#include "cvocabulary.h"

QString CWordSibling::_seperator=QLatin1Literal(" ");

CWordSibling::CWordSibling()
{}

QStringList CWordSibling::stringList()
{
    QStringList strList;

    QChar ch;
    foreach( ch, mWords )
    {
        strList.append( QString(ch) );
    }

    return strList;
}

bool CWordSibling::select( QChar word )
{
    if ( mWords.contains(word) )
    {
        mWords.removeOne( word );
        mWords.prepend( word );
        return true;
    }

    return false;
}
/*!
 * \brief CWordSibling::load
 * \param str
 * \return
 * - 一行一个拼音
 * - 拼音和汉字之间用空格分隔
 * - 汉字之间没有间隔
 */
int CWordSibling::load( const QString &str )
{
    QStringList list1 = str.split(" ",QString::SkipEmptyParts);
    QString word;
    Q_ASSERT( list1.size()>1 );

    mPinyin = list1.at(0);
    word = list1.at( 1 );
    for ( int i = 0; i < word.size(); i++)
    {
        mWords.append( word.at(i) );
    }
    return 0;
}

void CWordSibling::save( QTextStream &textStream )
{
    textStream<<mPinyin;
    textStream<<CWordSibling::_seperator;
    QChar word;
    foreach(word, mWords)
    {
        textStream<<word;
    }
    textStream<<endl;
}

CWordGroup::CWordGroup( const QString& key )
{
    mPinyin = key;
}

CWordGroup::~CWordGroup()
{
    CWordSibling *pSibling;

    foreach( pSibling, mSiblings )
    {
        Q_ASSERT( NULL != pSibling );
        delete pSibling;
    }

    mSiblings.clear();
}

void CWordGroup::addChild( CWordSibling *pSibling )
{
    Q_ASSERT( NULL != pSibling );
    mSiblings.append( pSibling );
}
/*!
 * \brief CWordGroup::canChild
 * \param pChild
 * \return
 * - 检查wordsibling是否可以归入组下
 * - 判定首字母是否相同
 */
bool CWordGroup::canChild( CWordSibling *pChild )
{
    Q_ASSERT( NULL != pChild );
    if ( pChild->mPinyin.at(0) == mPinyin.at(0) )
    { return true; }
    else
    { return false; }
}

/*!
 * \brief CWordGroup::findChild
 * \param pinyin
 * \return
 * - 在组中查找和输入拼音匹配的wordsibling
 * - 完全匹配的wordsibling放在最前面
 */
SiblingList CWordGroup::findChild( const QString &pinyin )
{
    SiblingList siblings;
    CWordSibling *pSibling;

    foreach( pSibling, mSiblings )
    {
        Q_ASSERT( NULL != pSibling );
        if ( pSibling->mPinyin.startsWith(pinyin) )
        {
            siblings.append( pSibling );
        }
    }

    //! sort the items by full match
    foreach( pSibling, mSiblings )
    {
        Q_ASSERT( NULL != pSibling );
        if ( pSibling->mPinyin == pinyin )
        {
            siblings.removeOne( pSibling );
            siblings.prepend( pSibling );
            break;
        }
    }

    return siblings;
}

CVocabulary::CVocabulary()
{}
CVocabulary::~CVocabulary()
{
    destroy();
}

void CVocabulary::destroy()
{
    CWordGroup *pGp;

    foreach( pGp, mGroups )
    {
        Q_ASSERT( NULL!=pGp );
        delete pGp;
    }

    mGroups.clear();
}

int CVocabulary::load( const QString &path )
{
    //! destroy previous
    destroy();

    QFile file(path);

    if ( !file.open(QIODevice::ReadOnly) )
    {
        Q_ASSERT( false );
    }

    //! 一次读入所有的文件数据
    QByteArray array = file.readAll();
    file.close();

    QTextStream textStream( array );

    textStream.setCodec( "UTF-16" );

    QString strLine;
    CWordSibling *pSibling;

    QList< CWordSibling *> siblingList;

    while( !textStream.atEnd() )
    {
        strLine = textStream.readLine();

        pSibling = R_NEW( CWordSibling() );
        Q_ASSERT( NULL != pSibling );

        //! fail
        if ( 0 != pSibling->load( strLine ) )
        { delete pSibling; }
        else
        { siblingList.append(pSibling);}
    }

    //! 对wordsibling按首字母进行分组
    alignGroup( siblingList );

    mNow = path;

    return 0;
}

int CVocabulary::save( const QString &path )
{
    QByteArray ary;
    QTextStream outStream( &ary );

    outStream.setCodec( ("UTF-16") );
    outStream.setGenerateByteOrderMark( true );

    CWordGroup *pGp;
    CWordSibling *pSibling;
    foreach( pGp, mGroups )
    {
        Q_ASSERT( NULL!=pGp );
        foreach( pSibling, pGp->mSiblings )
        {
            Q_ASSERT( NULL != pSibling );
            pSibling->save( outStream );
        }
    }
    outStream.flush();

    //! rewrite file
    QFile file( path );
    if ( !file.open( QIODevice::WriteOnly ) )
    { return -1; }
    if ( ary.size() != file.write( ary ) )
    {
        file.close();
        return -2;
    }
    file.close();

    return 0;
}

/*!
 * \brief CVocabulary::commit
 * - 提交更新
 */
void CVocabulary::commit()
{
    if ( mNow.length() < 1 )
    { return; }

    save( mNow );
}

/*!
 * \brief CVocabulary::ime
 * \param pinyin
 * \return
 * - 输入拼音，查询匹配的汉字列表
 * - 输出待选择的后续拼音列表,利用该项确定使能的拼音字母项
 */
QStringList CVocabulary::ime_tch( const QString &pinyin,
                              QString &optCodes )
{
    CWordGroup *pGp;
    QStringList strList;

    //! 找到匹配拼音的组
    pGp = findGroup( pinyin );
    if ( pGp == NULL )
    { return strList; }

    //! 在组内找匹配拼音的sibling
    //! 将找到的sibling列表缓存
    mSiblingList = pGp->findChild( pinyin );

    CWordSibling *pSibling;
    foreach( pSibling, mSiblingList )
    {
        Q_ASSERT( pSibling != NULL );
        strList.append( pSibling->stringList() );
    }

    optCodes.clear();
    foreach( pSibling, mSiblingList )
    {
        Q_ASSERT( pSibling != NULL );

        //! length more
        if( pSibling->mPinyin.length() > pinyin.length()
            && pSibling->mPinyin.startsWith(pinyin) )
        {
            optCodes.append( pSibling->mPinyin.mid( pinyin.length(), 1) );
        }
    }

    return strList;
}

/*!
 * \brief CVocabulary::ime_h
 * \param pinyin
 * \return
 * - 输入拼音，查询匹配的字词列表
 */
QStringList CVocabulary::ime( const QString &pinyin,
                              QString &py,
                              CPinyinInputMethod::InputLang lang,
                              CPinyinInputMethod*&  m_pinyinInput)
{
        //!输入法目前只支持中文简体  英文 繁体中文
        m_pinyinInput->Matching( pinyin, lang );
        py  = m_pinyinInput->py ;
        return m_pinyinInput->HanziModel;
}

/*!
 * \brief CVocabulary::select
 * \param word
 * \return
 * - 确认选择的汉字
 * - 向sibling确认汉字，以调整sibling中的匹配顺序
 */
bool CVocabulary::select( const QString &word )
{
    if ( word.size() < 1 )
    { return false; }

    CWordSibling *pSibling;
    foreach( pSibling, mSiblingList )
    {
        Q_ASSERT( NULL != pSibling );
        if ( pSibling->select(word.at(0)) )
        { return true;}
    }

    return false;
}

/*!
 * \brief CVocabulary::alignGroup
 * \param nodes
 * - 依据首字母进行分组
 */
void CVocabulary::alignGroup( QList<CWordSibling*> &nodes )
{
    CWordSibling *sibling;
    CWordGroup *pGp, *pLastGp;

    bool bFind;
    pLastGp = NULL;

    foreach( sibling, nodes )
    {
        if ( pLastGp != NULL && pLastGp->canChild(sibling) )
        {
            pLastGp->addChild( sibling );
            continue;
        }

        //! find group
        bFind = false;
        foreach( pGp, mGroups )
        {
            if ( pGp->canChild( sibling ) )
            {
                pGp->addChild( sibling );
                pLastGp = pGp;
                bFind = true;
                break;
            }
        }

        //! no group
        if ( !bFind )
        {
            pGp = R_NEW( CWordGroup( sibling->mPinyin.at(0) ) );
            Q_ASSERT( NULL != pGp );
            mGroups.append( pGp );
            pGp->addChild( sibling );
            pLastGp = pGp;
        }
    }
}

CWordGroup * CVocabulary::findGroup( const QString &pinyin )
{
    CWordGroup *pGp;

    if ( pinyin.size() < 1 )
    { return NULL; }

    foreach( pGp, mGroups )
    {
        if ( pGp->mPinyin.at(0) == pinyin.at(0) )
        { return pGp; }
    }

    return NULL;
}

