
QT       += widgets


TEMPLATE = lib
TARGET = ../../lib$$(PLATFORM)/rpinyin

INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


CONFIG += static
CONFIG += c++11
# Input
HEADERS += \
    pinyin/atomdictbase.h \
    pinyin/dictbuilder.h \
    pinyin/dictdef.h \
    pinyin/dictlist.h \
    pinyin/dicttrie.h \
    pinyin/lpicache.h \
    pinyin/matrixsearch.h \
    pinyin/mystdlib.h \
    pinyin/ngram.h \
    pinyin/pinyinime.h \
    pinyin/searchutility.h \
    pinyin/spellingtable.h \
    pinyin/spellingtrie.h \
    pinyin/splparser.h \
    pinyin/sync.h \
    pinyin/userdict.h \
    pinyin/utf16char.h \
    pinyin/utf16reader.h \
    cvocabulary.h \
    cwordoption.h \
    ime.h \
    pinyininputmethod.h \
    pinyin/zhconverter.h
SOURCES += \
    pinyin/dictbuilder.cpp \
    pinyin/dictlist.cpp \
    pinyin/dicttrie.cpp \
    pinyin/lpicache.cpp \
    pinyin/matrixsearch.cpp \
    pinyin/mystdlib.cpp \
    pinyin/ngram.cpp \
    pinyin/pinyinime.cpp \
    pinyin/searchutility.cpp \
    pinyin/spellingtable.cpp \
    pinyin/spellingtrie.cpp \
    pinyin/splparser.cpp \
    pinyin/sync.cpp \
    pinyin/userdict.cpp \
    pinyin/utf16char.cpp \
    pinyin/utf16reader.cpp \
    cvocabulary.cpp \
    cwordoption.cpp \
    pinyininputmethod.cpp
