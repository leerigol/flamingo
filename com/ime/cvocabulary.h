#ifndef CWORDGROUP_H
#define CWORDGROUP_H

#include <QtCore>
#include "pinyininputmethod.h"
#include"../../service/servdso/sysdso.h"
class CWordGroup;
class CVocabulary;

/*!
 * \brief The CWordSibling class
 * - 一个拼应对应多个汉字
 * - 各个汉字间的关系是相同的(所以，它们是兄弟关系)
 */
class CWordSibling
{
private:
    static QString _seperator;
public:
    CWordSibling();
    QStringList stringList();

    bool select( QChar word );
public:
    int load( const QString &str  );
    void save( QTextStream &textStream );

private:
    QString mPinyin;
    QList< QChar > mWords;

friend class CWordGroup;
friend class CVocabulary;
};

typedef QList< CWordSibling *> SiblingList;

/*!
 * \brief The CWordGroup class
 */
class CWordGroup
{
public:
    CWordGroup( const QString& key );
    ~CWordGroup();

    void addChild( CWordSibling *pSibling );
    bool canChild( CWordSibling *pChild );

    SiblingList findChild( const QString &pinyin );

private:
    QString mPinyin;
    QList< CWordSibling *> mSiblings;

friend class CVocabulary;
};

/*!
 * \brief The CVocabulary class
 * - 汉字表文件
 * - 表文件按行排
 */
class CVocabulary
{
public:
    CVocabulary();
    ~CVocabulary();
private:
    void destroy();

public:
    int load( const QString &path );
    int save( const QString &path );

    void commit();

    QStringList ime( const QString &pinyin,
                                  QString &py,
                                  CPinyinInputMethod::InputLang lang,
                                  CPinyinInputMethod*&  m_pinyinInput);
    QStringList ime_tch( const QString &pinyin,
                        QString &optCodes );
    bool select( const QString &word );

protected:
    void alignGroup( QList<CWordSibling*> &nodes );
    CWordGroup * findGroup( const QString &pinyin );

private:
    QList< CWordGroup * > mGroups;
    SiblingList           mSiblingList;
    QString mNow;
};

#endif // CWORDGROUP_H
