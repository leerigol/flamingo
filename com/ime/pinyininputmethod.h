#ifndef PINYININPUTMETHOD_H
#define PINYININPUTMETHOD_H
#include <QObject>
#include <QStringList>

#include"../../service/servdso/sysdso.h"
/*!
 * \brief The CPinyinInputMethod class
 * - 查找字库，词库
  */
class CPinyinInputMethod: public QObject
{
Q_OBJECT

public:
    CPinyinInputMethod();
    ~CPinyinInputMethod();
   enum InputLang{
       ImUnk = 0,
       ImChn,
       ImTChn,
       ImEn
   };
public:
   void SearchCN(const QString &gemfiel,
           CPinyinInputMethod::InputLang lang);
    void BinarySearchEN(const QString &gemfiel);
    void Matching(const QString &gemfield, InputLang lang );
    int load( const QString &path_en,
              const QString &path_ch,
              const QString &path_user,
              InputLang lang );
    QString find(QString str,QString cha);
    QStringList HanziModel;
    QString py;

private:
    QStringList lstEN;
};

#endif
