#ifndef CWORDOPTION
#define CWORDOPTION

#include <QtCore>

/*!
 * \brief The CWordPage class
 * - 拼音会对应多个汉字
 * - 多个汉字分成页显示
 * - 在页中通过序号选择汉字
 */
class CWordPage
{
public:
    void setWords( const QString& str );
    QString getViewText();
    QString select( int id );
    QStringList& getView();

private:
    QString mWords;
    QStringList mWordList;
    QStringList mWordh_List;
};

/*!
 * \brief The CWordOption class
 * - 一个拼音所对应的所有汉字
 * - 分页方式，一次显示一页
 */
class CWordOption
{
public:
    static int  _pageCount;  /*!< 一页中的选择数量，ubuntu中的中文拼音输入有5个选项*/

public:
    CWordOption();
    void setText( QStringList& list );

public:
    CWordPage getPage();

    bool isBof();
    bool isEof();
    bool isValid();

    void moveNext();
    void movePrevious();

    void clear();
private:
    int mPageNow, mPageCnt;
    QString mOptions;

    QStringList mOptionsList;
};


#endif // CWORDOPTION

