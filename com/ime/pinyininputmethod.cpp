#include "pinyininputmethod.h"
#include <QDateTime>
#include <QString>
#include <QSettings>
#include <QDebug>
#include "./pinyin/pinyinime.h"
#include"./pinyin/zhconverter.h"
using namespace ime_pinyin;
//!查找单词的最大个数
#define max_word_num 50
//!汉字缓存空间大小
#define max_word_size 20
 //!中文词语的最大长度
#define max_hz_num 6
//!英文单词的最大长度
#define max_en_num 10
CPinyinInputMethod::CPinyinInputMethod()
{}

int CPinyinInputMethod::load( const QString &path_en,
                              const QString &path_ch,
                              const QString &path_user,
                              CPinyinInputMethod::InputLang lang )
{
    HanziModel.clear();
    if(ImEn == lang)
    {
        im_close_decoder();
        QSettings setter(path_en, QSettings::IniFormat);
        lstEN = setter.value("pyEn", lstEN).toStringList();
        if (lstEN.size() <= 1)
        {
            qDebug() << "Can't load pyEn!";
            return -1;
        }
    }
   else
    {
        lstEN.clear();
        im_close_decoder();
        bool ret = im_open_decoder(
                                   path_ch.toLatin1().data(),
                                   path_user.toLatin1().data() );
        if (!ret)
        {
            qDebug() << "open dict faild";
            return -1;
        }
    }

    return 0;
}
CPinyinInputMethod::~CPinyinInputMethod()
{
    lstEN.clear();
    HanziModel.clear();
    im_close_decoder();
}

void CPinyinInputMethod::SearchCN(const QString &gemfiel,
                                       CPinyinInputMethod::InputLang lang)
{
    HanziModel.clear();
    QString gemfield = gemfiel.toLower();
    if (gemfield == "")
         return;
     gemfield = find(gemfield,"'");

    im_reset_search();
    int num = im_search(gemfield.toLatin1().data(), gemfield.size());
    const uint16 *pos;
    int size = im_get_spl_start_pos(pos);

    py = gemfield;
    for (int i = size-1; i > 0; i--)
    {
        py.insert(pos[i], "'");
    }
    py.replace("''", "'");

    if (num > max_word_num)
    {
        num = max_word_num;
    }

    char16 buf[max_word_size]={0};
    JtFtConvert conver;
    for (int i = 0; i < num; i++)
    {
        im_get_candidate(i, buf, max_word_size);
        buf[max_hz_num] = '\0';
        if(lang == ImChn)
        {
            HanziModel << QString::fromUtf16(buf);
        }
        else
        {
            HanziModel << conver.J2F(QString::fromUtf16(buf));
        }
    }
}

void CPinyinInputMethod::BinarySearchEN(const QString &gemfiel)
{
    QString gemfield = gemfiel;
    HanziModel.clear();
    if(!gemfield.size())
    {
        return ;
    }
    if(gemfield.size() >= max_en_num)
    {
        gemfield = gemfield.mid(0,max_en_num);
    }

    HanziModel.append(gemfield);
    //!记录上一个单词避免重复
    QString word_last = gemfield;
    gemfield = gemfield.toLower();
    int min = 0;
    int max = lstEN.size();
    int idx = max / 2;

    //!运用二分法查找
    while (true)
    {
        if (lstEN[idx].startsWith(gemfield, Qt::CaseInsensitive))
        {
            break;
        }

        if (idx <= min || idx >= max || max ==  idx + 1)
        {
            break;
        }
        if (lstEN[idx].toLower() > gemfield)
        {
            max = idx;
        }
        else
        {
            min = idx;
        }
        idx = (max + min) / 2;
    }

    //!检测查找结果前面有没有符合的
    do
    {
        if (--idx < 0)
        {
           break;
        }
    }while(lstEN[idx].startsWith(gemfield, Qt::CaseInsensitive));

    //!最终查找结果
    ++idx;
    int cnt = 0;
    while(++cnt < max_word_num)
    {
        if (idx >= lstEN.size())
        {
            break;
        }
        if (lstEN[idx].startsWith(gemfield, Qt::CaseInsensitive))
        {
            if( !lstEN[idx].mid(0,10).endsWith(word_last, Qt::CaseInsensitive))
            {
                HanziModel.append(lstEN[idx].mid(0,10));
                word_last = lstEN.at(idx).mid(0,10);
            }
        }
        else
        {
            break;
        }
        idx++;
    }
}

void CPinyinInputMethod::Matching(const QString &gemfield,
                                  InputLang lang )
{
    HanziModel.clear();
    py.clear();
    switch(lang)
    {
         case ImChn:
         case ImTChn:
            SearchCN(gemfield,lang);
            break;
         case ImEn:
            BinarySearchEN(gemfield);
            break;
         default:
            break;
    }

}



QString CPinyinInputMethod::find(QString str,QString cha)
{
    int x = -1;
    QString buf;
    x = str.indexOf(cha);
    if(x == -1)
    {
        buf = str;
    }
    else
    {
        while(x !=  -1)
        {
            str = str.replace("'","");
           x=str.indexOf(cha,x+1);
        }
    }
    return str;
}
