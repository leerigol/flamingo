#ifndef XXTEA_H
#define XXTEA_H

#include <stdio.h>
#ifndef XXTEA_TYPE
#define XXTEA_TYPE int
#endif


namespace com_algorithm
{
    class CXXTEA
    {
    public:
        /* if keys == null then use  ds1000z KEY */
        static void setKeys(XXTEA_TYPE* keys = NULL);
        static int encode(XXTEA_TYPE* pData, long len);
        static int decode(XXTEA_TYPE* pData, long len);
    private:
        static int btea(XXTEA_TYPE* v, long n, const XXTEA_TYPE* k);
        static XXTEA_TYPE m_au32Keys[4];
    };
}
#endif // XXTEA_H

