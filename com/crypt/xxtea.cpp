#include "xxtea.h"

namespace com_algorithm
{
//数据密钥
static XXTEA_TYPE xxbea_keys[] =
{
    0x03920001,
    0x08410841,
    0x18c32104,
    0x318639c7
};

XXTEA_TYPE CXXTEA::m_au32Keys[4];

void CXXTEA::setKeys(XXTEA_TYPE *keys)
{
    if( keys != NULL )
    {
        m_au32Keys[0] = *keys;
        m_au32Keys[1] = *(keys + 1);
        m_au32Keys[2] = *(keys + 2);
        m_au32Keys[3] = *(keys + 3);
    }
    else
    {
        m_au32Keys[0] = xxbea_keys[0];
        m_au32Keys[1] = xxbea_keys[1];
        m_au32Keys[2] = xxbea_keys[2];
        m_au32Keys[3] = xxbea_keys[3];
    }
}

int CXXTEA::encode(XXTEA_TYPE *pData, long len)
{
    return  btea(pData, len/4, m_au32Keys);
}

int CXXTEA::decode(XXTEA_TYPE *pData, long len)
{
    return btea(pData, -len/4, m_au32Keys);
}

/**
 *	@brief	XXTEA官方算法，此类即是对其进行封装，详见http://en.wikipedia.org/wiki/XXTEA
 *
 *	@param 	v 	加解密数据流
 *	@param 	n 	加解密长度，n > 1为加密，n < -1为解密
 *	@param 	k 	密钥      注意：形式参数k需和MX中的k对应
 *
 *	@return	返回0表示加解密成功，返回1表示失败
*/

#define MX (((z>>5)^(y<<2)) + ((y>>3)^(z<<4)) ) ^ ( (sum^y) + ((k[(p&3)^e])^z));

int CXXTEA::btea(XXTEA_TYPE* v, long n, const XXTEA_TYPE* k)
{

    unsigned XXTEA_TYPE z=v[n-1], y=v[0];
    unsigned long sum=0, e, DELTA=0x9e3779b9;
    long p, q ;
    if (n > 1)
    {          /* Coding Part */
        q = 6 + 52/n;
        while (q-- > 0) {
            sum += DELTA;
            e = (sum >> 2) & 3;
            for (p=0; p<n-1; p++) y = v[p+1], z = v[p] += MX;
            y = v[0];
            z = v[n-1] += MX;
        }
        return 0 ;
    } else if (n < -1)
    {  /* Decoding Part */
        n = -n;
        q = 6 + 52/n;
        sum = q*DELTA ;
        while (sum != 0) {
            e = (sum >> 2) & 3;
            for (p=n-1; p>0; p--) z = v[p-1], y = v[p] -= MX;
            z = v[n-1];
            y = v[0] -= MX;
            sum -= DELTA;
        }
        return 0;
    }
    return 1;
}



}
