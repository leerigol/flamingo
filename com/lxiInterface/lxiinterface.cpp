#include "lxiinterface.h"

#include "../service/service_name.h"
#include "../service/servscpi/servscpidso.h"
#include "../service/service.h"
#include "../../service/servgui/servgui.h"

lxiInterface::lxiInterface(scpiIntfType scpiType, scpiIntfId id)
    :CScpiInterface(scpiType, id)
{
    m_ipAddr = "0.0.0.0";
    m_port   = 0;
}


void lxiInterface::onController()
{
     Q_ASSERT( m_pController != NULL );
}

void lxiInterface::onNewConnection(QString ip, int port, int id)
{
    m_port   = port;
    m_ipAddr = ip;
    mId    = id;
}

void lxiInterface::onDisConnected()
{
    mId = INTERFACE_INVALID_ID;
}

void lxiInterface::getInterfaceInfo(QString &ip, int &port)
{
    port = m_port;
    ip   = m_ipAddr;
}

void lxiInterface::srqRespond(quint8 stbReg)
{
    SCPI_INTERFACE_DEBUG("LXI srq event: = %d", stbReg);
    stbReg = stbReg;
}


