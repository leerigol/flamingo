#include "vxi11_user.h"
#define UNUSED_PARAMETER(par)  ((void)par)

__attribute__((weak)) int vxi11RxCallback(vxi11RxParms par)
{
    UNUSED_PARAMETER(par);
   VXI11_DEBUG("len :%d ,data:%s",par.data.len, par.data.pVal);
   return VXI11_NONE_ERR;
}
__attribute__((weak)) int vxi11TxCallback(vxi11TxParms par)
{
   UNUSED_PARAMETER(par);
   VXI11_DEBUG("id :%ld",par.id);
  return VXI11_NONE_ERR;
}
__attribute__((weak)) int vxi11NewConnectionCallback(vxi11ConnectionParms par)
{
    UNUSED_PARAMETER(par);
    VXI11_DEBUG("Connection lxi  %ld   %s", par.id, par.pIpAddr);
    return VXI11_NONE_ERR;
}
__attribute__((weak)) int vxi11DisConnectionCallback(vxi11ConnectionParms par)
{
    UNUSED_PARAMETER(par);
    VXI11_DEBUG("DisConnection lxi  %ld   %s", par.id, par.pIpAddr);
    return VXI11_NONE_ERR;
}
__attribute__((weak)) int vxi11GetStbCallback(long /*id*/)
{
    return VXI11_NONE_ERR;
}

__attribute__((weak)) void vxi11CallbackRegister()
{
    vxi11CallbackFunc.pRxFunc            = vxi11RxCallback;
    vxi11CallbackFunc.pTxFunc            = vxi11TxCallback;
    vxi11CallbackFunc.pNewConnectionFunc = vxi11NewConnectionCallback;
    vxi11CallbackFunc.pDisConnectionFunc = vxi11DisConnectionCallback;
    vxi11CallbackFunc.pGetStbFunc        = vxi11GetStbCallback;
}

