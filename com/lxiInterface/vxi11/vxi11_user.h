#ifndef VXI11_USER_H
#define VXI11_USER_H
#include <stdio.h>

#if 0
#define VXI11_DEBUG(fmt,...)  do{printf("--%s %u:", __FUNCTION__, __LINE__);\
                                 printf(fmt,##__VA_ARGS__);printf("\n");\
                                }while(0)
#else
#define VXI11_DEBUG(fmt,...)
#endif

enum VXI11ERROR
{
    VXI11_NONE_ERR,
    VXI11_TX_BUFF_EMPTY,        //!tx 缓冲区空
    VXI11_RX_BUFF_EMPTY,        //!rx 缓冲区空
    VXI11_EVENT_QUEUE_FULL,     //!事件 队列 满
    VXI11_NEW_CONNECT_FAILURE,  //!创建链接失败
    VXI11_DIS_CONNECT_FAILURE,  //!断开链接失败
    VXI11_INVALID_LINK_ID,      //!断开链接失败
    VXI11_TX_TIMEOUT,           //!read请求数据超时

};

struct vxi11RxParms
{
    long id;
    struct {
        unsigned int  len;
        char          *pVal;
    }data;
};
typedef struct vxi11RxParms vxi11RxParms;

struct vxi11TxParms
{
    long id;
    struct {
        unsigned int  *pLen;
        char          *pVal;
        int           *pEom;
    }data;
};
typedef struct vxi11TxParms vxi11TxParms;

struct vxi11ConnectionParms
{
    long          id;
    char*         pIpAddr;
    unsigned int  port;
};
typedef struct vxi11ConnectionParms vxi11ConnectionParms;

typedef int (*pRxCallback)(vxi11RxParms par);
typedef int (*pTxCallback)(vxi11TxParms par);
typedef int (*pNewConnectionCallback)(vxi11ConnectionParms par);
typedef int (*pDisConnectionCallback)(vxi11ConnectionParms par);
typedef int (*pGetStbCallback)(long  id);

struct vxi11Callback
{
    pRxCallback             pRxFunc;
    pTxCallback             pTxFunc;
    pNewConnectionCallback  pNewConnectionFunc;
    pDisConnectionCallback  pDisConnectionFunc;
    pGetStbCallback         pGetStbFunc;

};
typedef struct vxi11Callback vxi11Callback;


int vxi11RxCallback(vxi11RxParms par);
int vxi11TxCallback(vxi11TxParms par);
int vxi11NewConnectionCallback(vxi11ConnectionParms par);
int vxi11DisConnectionCallback(vxi11ConnectionParms par);
int vxi11GetStbCallback(long  id);
//int vxi11createIntrCallback();

extern vxi11Callback vxi11CallbackFunc;
void vxi11CallbackRegister();


#endif // VXI11_USER_H
