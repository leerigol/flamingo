#ifndef VXI11THREAD_H
#define VXI11THREAD_H
#include <QObject>
#include <QThread>
#include "vxi11_user.h"

class vxi11thread : public QThread
{
public:
    vxi11thread();
    
protected:
    void  virtual run();
};

#endif // VXI11THREAD_H
