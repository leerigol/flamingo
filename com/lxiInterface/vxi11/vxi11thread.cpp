#include <QDebug>
#include "vxi11thread.h"
#include "vxi11.h"

vxi11thread::vxi11thread()
{
    setStackSize(128*1024);
    setObjectName("vxi11");

    /*! [dba: 2018-04-19] bug:2753
    * SIGPIPE的默认处理方式是杀掉进程,这里将其在线程中屏蔽掉.*/
    sigset_t signal_mask;
    sigemptyset (&signal_mask);
    sigaddset (&signal_mask, SIGPIPE);
    int rc = pthread_sigmask (SIG_BLOCK, &signal_mask, NULL);
    if (rc != 0)
    {
        printf("block sigpipe error\n");
    }
}

void vxi11thread::run()
{ 
    //wait a moment to start. by hxh
    sleep(5);
    //qDebug() << "<<<<<<<<<<<<<<<vxi started>>>>>>>>>>>>";

    vxi11_start();
}
