#-------------------------------------------------
#
# Project created by QtCreator 2017-02-22T13:31:00
#
#-------------------------------------------------

QT += core
QT -= gui

staticlib
TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += .

#根据环境变量 确定是否定义宏 _SIMULATE
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
}

#***********************************************************#
#工程配置
#***********************************************************#
# *1* 仅用debug模式构建
CONFIG  -= debug_and_release
CONFIG  -= release
CONFIG  -= debug_and_release_target
CONFIG  -= build_all
CONFIG  += debug

# *2* 中间文件(.o文件)路径
#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

# *3* 可执行文件(.exe文件)路径
TARGET = ../../../lib$$(PLATFORM)/scpi/vxi11

#***********************************************************#
#源文件
#***********************************************************#
HEADERS += \
    vxi11.h \
    vxi11thread.h \
    vxi11_user.h

SOURCES += \
    vxi11thread.cpp \
    vxi11_server.c \
    vxi11_svc.c \
    vxi11_xdr.c \
    vxi11_clnt.c\
    vxi11_user.cpp

