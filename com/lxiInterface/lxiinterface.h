#ifndef LXIINTERFACE_H
#define LXIINTERFACE_H

#include "../scpiparse/cscpiparser.h"

class lxiInterface : public CScpiInterface
{
    Q_OBJECT
public:
    lxiInterface(scpiIntfType scpiType = scpi_intf_lxi, scpiIntfId id = 6);

protected:
    QString                m_ipAddr;
    int                    m_port;

public:
    void onController();
    void onNewConnection(QString ip, int port, int id);
    void onDisConnected();

    void getInterfaceInfo(QString &ip, int &port);

private Q_SLOTS:
    void srqRespond(quint8 stbReg);

};

//extern lxiInterface *plxiInterface;

#endif // LXIINTERFACE_H
