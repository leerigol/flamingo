#include "../service/service_name.h"
#include "../service/service.h"
#include "../service/servscpi/servscpidso.h"
#include "../service/servscpi/servscpi.h"

#include "lxiinterface.h"
#include "./vxi11/vxi11_user.h"

#if 0
#define LXI_INTF_DBG()    qDebug()<<__FILE__<<__FUNCTION__<<__LINE__
#else
#define LXI_INTF_DBG()    QT_NO_QDEBUG_MACRO()
#endif

int vxi11RxCallback(vxi11RxParms par)
{
    LXI_INTF_DBG()<<(int)(par.id)<<par.id;

    Q_ASSERT( par.data.pVal != NULL );

    lxiInterface *plxiInterface = servScpi::getLxiInterface(par.id);
    //Q_ASSERT( plxiInterface != NULL );
    if(NULL == plxiInterface )
    {
        qDebug() << "lxi connection lost";
        return VXI11_INVALID_LINK_ID;
    }

    QByteArray data(par.data.pVal, par.data.len);
    SCPI_INTERFACE_DEBUG("RX:%s",data.data());

    //!送数据到ＳＣＰＩ
    plxiInterface->inPut(data, true);
    //!通知控制器
    plxiInterface->onController();

    return VXI11_NONE_ERR;
}

int vxi11TxCallback(vxi11TxParms par)
{
    LXI_INTF_DBG()<<(int)(par.id)<<par.id;

    Q_ASSERT( par.data.pEom != NULL );
    Q_ASSERT( par.data.pLen != NULL );
    Q_ASSERT( par.data.pVal != NULL );

    lxiInterface *plxiInterface = servScpi::getLxiInterface(par.id);

    //Q_ASSERT( plxiInterface != NULL );
    if(NULL == plxiInterface )
    {
        return VXI11_INVALID_LINK_ID;
    }

    char    *buf    = par.data.pVal;
    quint32 liv_len = *par.data.pLen;
    bool    lbv_eom = false;

    quint32 retuenSize = plxiInterface->getOutData((quint8*)buf, liv_len, lbv_eom);

    if((retuenSize == 0)&&(lbv_eom == false) )
    {
        return VXI11_TX_BUFF_EMPTY;
    }
    else
    {
        *par.data.pLen = retuenSize;
        *par.data.pEom = lbv_eom? 1 : 0;

        return VXI11_NONE_ERR;
    }
}

int vxi11NewConnectionCallback(vxi11ConnectionParms par)
{
    LXI_INTF_DBG()<<(int)(par.id)<<par.id;

    CArgument arg;
    arg.setVal( QString(par.pIpAddr), 0);
    arg.setVal( (int)(par.id),        1);
    arg.setVal( (int)(par.port),      2);

    DsoErr err;
    err =  serviceExecutor::post( serv_name_scpi,
                           dsoServScpi::cmd_lxi_newconnected,
                           arg,
                           &serviceExecutor::_contextRmt);
    if( ERR_NONE != err)
    {
        return VXI11_NEW_CONNECT_FAILURE;
    }

    //! wait
    Q_ASSERT( NULL != serviceExecutor::_contextRmt.m_pSema );
    Q_ASSERT( serviceExecutor::_contextRmt.m_pSema->available() <= 1 );
    //qDebug()<<__FILE__<<__LINE__<<serviceExecutor::_contextRmt.m_pSema->available();
    serviceExecutor::_contextRmt.m_pSema->acquire();
    //qDebug()<<__FILE__<<__LINE__<<serviceExecutor::_contextRmt.m_pSema->available();

    if ( ERR_NONE != serviceExecutor::_contextRmt.mErr )
    {
        return VXI11_NEW_CONNECT_FAILURE;
    }

    return VXI11_NONE_ERR;
}

int vxi11DisConnectionCallback(vxi11ConnectionParms par)
{
    LXI_INTF_DBG()<<(int)(par.id)<<par.id;

    CArgument arg;
    arg.setVal( QString(par.pIpAddr), 0);
    arg.setVal( (int)(par.id),        1);
    serviceExecutor::post( E_SERVICE_ID_SCPI,
                           dsoServScpi::cmd_lxi_disconnected,
                           arg);
    return VXI11_NONE_ERR;
}

int vxi11GetStbCallback(long  id)
{
    lxiInterface *plxiInterface = servScpi::getLxiInterface(id);
    Q_ASSERT( plxiInterface != NULL );
    //! 获取STB寄存器值
    return plxiInterface->getStbReg();
}


void vxi11CallbackRegister()
{
    vxi11CallbackFunc.pRxFunc            = vxi11RxCallback;
    vxi11CallbackFunc.pTxFunc            = vxi11TxCallback;
    vxi11CallbackFunc.pNewConnectionFunc = vxi11NewConnectionCallback;
    vxi11CallbackFunc.pDisConnectionFunc = vxi11DisConnectionCallback;
    vxi11CallbackFunc.pGetStbFunc        = vxi11GetStbCallback;
}


