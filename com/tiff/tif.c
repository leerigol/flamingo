#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "lzw.h"
#include "tif.h"

#define HEAD_LEN_BYTE 10
#define TAIL_LEN_BYTE  4

#if 0
#define NUM_DIR_ENTRY  14

TIFFDirEntry dirEntry[NUM_DIR_ENTRY] ={
{254,TIFF_LONG,1,0,},  //NEWSUBfileTYPE 0
{256,TIFF_SHORT,1,0},  //1
{257,TIFF_SHORT,1,0},  //2
{258,TIFF_SHORT,3,0}, //BitPerSample.3
{259,TIFF_SHORT,1,5},    //compression.4
{262,TIFF_SHORT,1,2},    //PhotometricInterpretation.5

//{269,TIFF_ASCII,30,0},   //DocumentName.6
//{270,TIFF_ASCII,30,0},   //ImageDescription.7
//{271,TIFF_ASCII,30,0},   //Maker.8
//{272,TIFF_ASCII,30,0},   //Model.9
{273,TIFF_LONG,0,0}, //stripoffsets.10
{277,TIFF_SHORT,1,3},//SamplesPerStrip.11
{278,TIFF_SHORT,1,ROWPERSTRIP},//12
{279,TIFF_LONG,0,0},//stripBytecount.13
{282,TIFF_RATIONAL,1,0},//xResolution.14
{283,TIFF_RATIONAL,1,0}, //yResolution.15
{296,TIFF_SHORT,1,2},   //resolutionUnit.16
//{305,TIFF_ASCII,30,0},  //Software.17
{306,TIFF_ASCII,30,0}, //Datetime.18
//{315,TIFF_ASCII,30,0}, //Artist.19
};
#else

#define NUM_DIR_ENTRY  20

TIFFDirEntry dirEntry[NUM_DIR_ENTRY] ={
{254,TIFF_LONG,1,0,},  //NEWSUBfileTYPE 0
{256,TIFF_SHORT,1,0},  //1
{257,TIFF_SHORT,1,0},  //2
{258,TIFF_SHORT,3,0}, //BitPerSample.3
{259,TIFF_SHORT,1,5},    //compression.4
{262,TIFF_SHORT,1,2},    //PhotometricInterpretation.5

{269,TIFF_ASCII,30,0},   //DocumentName.6
{270,TIFF_ASCII,30,0},   //ImageDescription.7
{271,TIFF_ASCII,30,0},   //Maker.8
{272,TIFF_ASCII,30,0},   //Model.9
{273,TIFF_LONG,0,0}, //stripoffsets.10
{277,TIFF_SHORT,1,3},//SamplesPerStrip.11
{278,TIFF_SHORT,1,ROWPERSTRIP},//12
{279,TIFF_LONG,0,0},//stripBytecount.13
{282,TIFF_RATIONAL,1,0},//xResolution.14
{283,TIFF_RATIONAL,1,0}, //yResolution.15
{296,TIFF_SHORT,1,2},   //resolutionUnit.16
{305,TIFF_ASCII,30,0},  //Software.17
{306,TIFF_ASCII,30,0}, //Datetime.18
{315,TIFF_ASCII,30,0}, //Artist.19
};
#endif

static int Data_Offset = 0;
static int Num_Strip   = 0;
static int Strip_Size  = 0;

static int BitPerSample_Offset  = 0;
static int Strip_Offset         = 0;
static int Strip_ByteCount_Offset=0;

static int Name_Offset = 0;
static int Description_Offset = 0;
static int Make_Offset = 0;
static int Model_Offset = 0;

static int Software_Offset = 0;
static int DateTime_Offset = 0;
static int Artist_Offset   = 0;

static int XResolution_Offset = 0;
static int YResolution_Offset = 0;


static TIFFHeader tif_header={0x4949,0x002a,0x00000008,NUM_DIR_ENTRY};
static int XResolution[2]={960000,10000};
static int YResolution[2]={960000,10000};

#if 0
static void SetDirValue(int   hexWidth,  int   hexHeight)
{
   Num_Strip    = hexHeight / ROWPERSTRIP;
   Strip_Size   = ROWPERSTRIP * hexWidth;
   if(hexHeight % ROWPERSTRIP)
   {
       Num_Strip++;
   }

   BitPerSample_Offset = HEAD_LEN_BYTE + NUM_DIR_ENTRY*12 + TAIL_LEN_BYTE;

   Strip_Offset        =   BitPerSample_Offset + sizeof(short) *3;
   Strip_ByteCount_Offset = Strip_Offset + Num_Strip * sizeof(int);
   XResolution_Offset  = Strip_ByteCount_Offset + Num_Strip * sizeof(int);
   YResolution_Offset  = XResolution_Offset + sizeof(int) *2;
   DateTime_Offset     = YResolution_Offset + sizeof(int) *2;

   Data_Offset = DateTime_Offset + TAG_INFO_LEN;

   dirEntry[1].value = hexWidth;
   dirEntry[2].value = hexHeight;
   dirEntry[3].value = BitPerSample_Offset;

   dirEntry[6].count = Num_Strip;
   dirEntry[6].value = Strip_Offset;

   dirEntry[9].count = Num_Strip;
   dirEntry[9].value = Strip_ByteCount_Offset;

   dirEntry[10].value = XResolution_Offset;
   dirEntry[11].value = YResolution_Offset;
}
#else
static void SetDirValue(int   hexWidth,  int   hexHeight)
{
   Num_Strip    = hexHeight / ROWPERSTRIP;
   Strip_Size   = ROWPERSTRIP * hexWidth;
   if(hexHeight % ROWPERSTRIP)
   {
       Num_Strip++;
   }

   BitPerSample_Offset = HEAD_LEN_BYTE + NUM_DIR_ENTRY*12 + TAIL_LEN_BYTE;
   Name_Offset         = BitPerSample_Offset + sizeof(short) *3;
   Description_Offset  = Name_Offset + TAG_INFO_LEN;
   Make_Offset         = Description_Offset + TAG_INFO_LEN;
   Model_Offset        = Make_Offset + TAG_INFO_LEN;

   Strip_Offset        =   Model_Offset + TAG_INFO_LEN;;
   Strip_ByteCount_Offset = Strip_Offset + Num_Strip * sizeof(int);
   XResolution_Offset  = Strip_ByteCount_Offset + Num_Strip * sizeof(int);
   YResolution_Offset  = XResolution_Offset + sizeof(int) *2;
   Software_Offset     = YResolution_Offset + sizeof(int) *2;
   DateTime_Offset     = Software_Offset + TAG_INFO_LEN;
   Artist_Offset       = DateTime_Offset + TAG_INFO_LEN;
   Data_Offset         = Artist_Offset + TAG_INFO_LEN ;


   dirEntry[1].value = hexWidth;
   dirEntry[2].value = hexHeight;
   dirEntry[3].value = BitPerSample_Offset;

   dirEntry[6].value = Name_Offset;
   dirEntry[7].value = Description_Offset;
   dirEntry[8].value = Make_Offset;
   dirEntry[9].value = Model_Offset;

   dirEntry[10].count = Num_Strip;
   dirEntry[10].value = Strip_Offset;
   dirEntry[13].count = Num_Strip;
   dirEntry[13].value = Strip_ByteCount_Offset;
   dirEntry[14].value = XResolution_Offset;
   dirEntry[15].value = YResolution_Offset;
   dirEntry[17].value = Software_Offset;
   dirEntry[18].value = DateTime_Offset;
   dirEntry[19].value = Artist_Offset;
}
#endif


/******************************************************************************
** 函数名称:       userSetTIFTag
** 输入参数:
** pStreamBuf ----Tag写入地址
** iImgWidth  ----图像宽度
** iImgHeight ----图像高度
** pImgName  ----Document Name
** pImgDiscrp----Image discretion
** pImgMake  ----Image Maker
** pImgModel --- Make Model
** pImgSoft  --- Software
** pImgDate ---- DateTime
** pImgArtist--- Image Artist
** 输出参数；
** 返回类型:  int 型,返回0表示成功
**                   返回1表示失败
** 描述 :      设置TIF tag 信息
*****************************************************************************/
int  userSetTIFTag(  void *pStreamBuf,
                     int   iImgWidth,
                     int   iImgHeight,
                     char* pImgName,                //Document Name
                     char* pImgDiscrp,              //Image discretion
                     char* pImgMake,               //Image Maker
                     char* pImgModel,               //Make Model
                     char* pImgSoft,                //Software
                     char* pImgDate,                //DateTime
                     char* pImgArtist)	           //Image Artist
{
      int len = 0;

	  if(pStreamBuf == NULL)
      {
          return -1;
      }

      SetDirValue(iImgWidth,iImgHeight);


	if(pImgName!= NULL )
	{
		len = strlen(pImgName)<TAG_INFO_LEN ?strlen(pImgName) + 1 :TAG_INFO_LEN;
		strncpy((char *)pStreamBuf + Name_Offset,pImgName,len );
		dirEntry[6].count = len  ;
	}

	if(pImgDiscrp!= NULL )
	{
		len = strlen(pImgDiscrp) < TAG_INFO_LEN ?strlen(pImgDiscrp) + 1 : TAG_INFO_LEN ;
		strncpy((char *)pStreamBuf + Description_Offset,pImgDiscrp,len);
		dirEntry[7].count = len ;
	}

	if(pImgMake!= NULL )
	{
		len = strlen(pImgMake)<TAG_INFO_LEN? strlen(pImgMake) + 1:TAG_INFO_LEN;
		strncpy((char *)pStreamBuf + Make_Offset,pImgMake,len);
		dirEntry[8].count = len ;
	}

	if(pImgModel!= NULL )
	{
		len = strlen(pImgModel) <TAG_INFO_LEN?strlen(pImgModel) + 1:TAG_INFO_LEN;
		strncpy((char *)pStreamBuf + Model_Offset,pImgModel,len);
		dirEntry[9].count = len ;
	}

	if(pImgSoft!= NULL )
	{
		len = strlen(pImgSoft) <TAG_INFO_LEN?strlen(pImgSoft) + 1:TAG_INFO_LEN;
		strncpy((char *)pStreamBuf + Software_Offset,pImgSoft,len);
		 dirEntry[17].count = len ;
	}
	if(pImgDate != NULL)
	{
		len = strlen(pImgDate) <TAG_INFO_LEN ?strlen(pImgDate) + 1 :TAG_INFO_LEN;
		strncpy((char *)pStreamBuf + DateTime_Offset,pImgDate,len);
		dirEntry[18].count = len ;
	}

	if(pImgArtist!= NULL)
	{
		len = strlen(pImgArtist) < TAG_INFO_LEN? strlen(pImgArtist) + 1:TAG_INFO_LEN;
		strncpy((char *)pStreamBuf + Artist_Offset,pImgArtist,len);
		dirEntry[19].count = len ;
	}

      if(pImgDate != NULL)
      {
          len = strlen(pImgDate) <TAG_INFO_LEN ?strlen(pImgDate) + 1 :TAG_INFO_LEN;
          strncpy((char *)pStreamBuf + DateTime_Offset,pImgDate,len);
          dirEntry[18].count = len ;
      }


//    int i;
//    for( i=0; i<NUM_DIR_ENTRY; i++)
//    {
//        printf("i=%d:%04x,%d,%d,0x%x\n",i, dirEntry[i].tag,dirEntry[i].type,dirEntry[i].count,dirEntry[i].value);
//    }
    
	return 0;
}
/*
static void SetSoftIfo(unsigned char * addr)
{
  char soft[]= "DS6104";
  char date[] = "2010-11-02-14-40-00";
  char artist[] = "Rigol Technologies,Inc" ;

  memcpy(addr + Sofeware_Offset,&soft[0],sizeof(soft));
  memcpy(addr + DateTime_Offset,&date[0],sizeof(date));
  memcpy(addr + Artist_Offset,&artist[0],sizeof(artist));
}
*/
static void  TiffEncodeDirEntry(TIF* tif)
{

  memcpy(tif->output,(void*)(&tif_header),sizeof(tif_header));

  tif->out_off += sizeof(tif_header) - 2;

  int i;
  int size_ent = sizeof(TIFFDirEntry);
  for(i =0;i<NUM_DIR_ENTRY;i++)
  {
      memcpy(tif->output + tif->out_off, &dirEntry[i], size_ent);
      tif->out_off += size_ent;
  }

  //no next IFD
  memset(tif->output + tif->out_off, 0, 4);
  tif->out_off += 4;

  unsigned short bits[3] = {0x08,0x08,0x08};
  memcpy(tif->output + tif->out_off,&bits[0],sizeof(bits));

  tif->out_off += sizeof(bits);

  memcpy(tif->output+ XResolution_Offset,&XResolution[0],sizeof(XResolution));

  memcpy(tif->output+ YResolution_Offset,&YResolution[0],sizeof(YResolution));

}

static void SetStrip(TIF * tif, short count)
{

    unsigned int addr = Data_Offset ;
    unsigned int row  = tif->CurStrip.strip_num;

    int _cnt = count;
    memcpy(tif->output + Strip_ByteCount_Offset + row * sizeof(int),(void *)(&_cnt),sizeof(int));


    if(row == 0)
    {
        memcpy(tif->output + Strip_Offset ,(void *)(&addr),sizeof(int));
    }

    if((int)row == Num_Strip - 1)
    {
        return ;
    }

    addr = tif->CurStrip.strip_offset;
    memcpy(tif->output + Strip_Offset + (row +1)* sizeof(int),(void *)(&addr),sizeof(int));
 }

 static int  LZW(TIF * tif,LZWCodecState *sp)
{
    int count ;

	unsigned short * cp = tif->data + tif->data_off;
	unsigned char * op = tif->CurStrip.outcp;

	LZWPreEncode(sp,(unsigned char *)op,Strip_Size*3);
	count = LZWEncode(sp,cp,op,Strip_Size*3);
	count = LZWPostEncode(op,sp,count);

	return count;

}

static int InitTif(TIF *tif,unsigned short * pHex565,void * pStreamBuf)
{

   tif->data = pHex565;
   tif->output = (unsigned char *)(pStreamBuf);
   if(tif->output == NULL)
       return 0;

   tif->data_off = 0;
   tif->out_off = 0;

   tif->CurStrip.outcp = tif->output + Data_Offset;
   tif->CurStrip.strip_num = 0;
   tif->CurStrip.strip_offset = Data_Offset;

   TiffEncodeDirEntry(tif);  //设置各个DIR
    return 1;
}



 /*******************************************************************************

**  函数名称:           userTIFDecoder
**  输入参数
**  inStream     ---    unsigned char*类型,表示读取数据的地址起始指针
**  outStream    ---    unsigned char*类型，表示编码数据的地址起始指针
**	pBufferSize	 ---	函数中返回实际需要Buffer大小
**
**  输出参数:
**  返回类型     ---    int类型,返回1表示数据错误,
								返回2表示系统内存不足,配合返回的pBufferSize，得到实际需要的大小
                                返回0表示编码成功
**
**  描    述:          TIF编码函数
**
*******************************************************************************/
 int userTIFDecoder(unsigned char * inStream,
                    unsigned char * outStream,
                    int imgWidth,
                    int imgHeight,
                    int* pBufferSize)
{

     int  row,count = 0, status = 0;

     TIF *tif = (TIF *) malloc(sizeof(TIF));
     memset(tif, 0, sizeof(TIF));

    userSetTIFTag(outStream, imgWidth,imgHeight,"TIFF","RIGOL PRINT","RIGOL","DS7","RIGOL","2016-07-12","RIGOL");

    if(InitTif(tif,(unsigned short*)inStream,outStream) == 0)
    {
        free(tif);
        return 2;
    }


    LZWCodecState *sp  = (LZWCodecState *)malloc(sizeof(LZWCodecState));
    memset(sp, 0, sizeof(LZWCodecState));

    sp->enc_hashtab = (hash_t*) malloc(HSIZE*sizeof(hash_t));
    memset(sp->enc_hashtab, 0, HSIZE*sizeof(hash_t));

    if (sp->enc_hashtab == NULL || sp == NULL )
    {
        free(sp->enc_hashtab);
        free(sp);
        free(tif);
        return (NO_MEM);
    }

   for(row = 0;row < imgHeight;row += ROWPERSTRIP )
   {
      count = LZW(tif,sp);//LZW压缩

      tif->CurStrip.strip_offset += count;

      SetStrip(tif,count);

      tif->data_off += Strip_Size  ;

      tif->CurStrip.outcp += count;
      tif->CurStrip.strip_num++;

      if((int)(tif->CurStrip.outcp - tif->output) !=  tif->CurStrip.strip_offset)
      {
          status = 1 ;
      }
    }


    *pBufferSize =  tif->CurStrip.strip_offset;

    free(sp->enc_hashtab);
    free(sp);
    free(tif);

    return status;
}


#ifdef __cplusplus
}
#endif
