#ifndef _TIF_H_
#define _TIF_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef	enum {
	TIFF_NOTYPE	= 0,	/* placeholder */
	TIFF_BYTE	= 1,	/* 8-bit unsigned integer */
	TIFF_ASCII	= 2,	/* 8-bit bytes w/ last byte null */
	TIFF_SHORT	= 3,	/* 16-bit unsigned integer */
	TIFF_LONG	= 4,	/* 32-bit unsigned integer */
	TIFF_RATIONAL	= 5,	/* 64-bit unsigned fraction */
	TIFF_SBYTE	= 6,	/* !8-bit signed integer */
	TIFF_UNDEFINED	= 7,	/* !8-bit untyped data */
	TIFF_SSHORT	= 8,	/* !16-bit signed integer */
	TIFF_SLONG	= 9,	/* !32-bit signed integer */
	TIFF_SRATIONAL	= 10,	/* !64-bit signed fraction */
	TIFF_FLOAT	= 11,	/* !32-bit IEEE floating point */
	TIFF_DOUBLE	= 12,	/* !64-bit IEEE floating point */
	TIFF_IFD	= 13	/* %32-bit unsigned integer (offset) */

}TIFFDataType;


typedef struct
{
 unsigned short magic;
 unsigned short version;
 unsigned int off_dir;
 unsigned short num_de;

}TIFFHeader;


typedef struct
{
 unsigned short tag;
 unsigned short type;
 unsigned int   count;
 unsigned int   value;
}TIFFDirEntry;

typedef struct
{
  unsigned short strip_num;
  unsigned char * outcp;
  int  strip_offset;

}Strip;

typedef struct
{
  unsigned short * data;
  unsigned char * output;
  int out_off;
  int data_off;
  Strip CurStrip;
}TIF;


#define ROWPERSTRIP   10

//返回状态
#define NO_MEM  -1         //内存申请失败
#define LENGTH_OVER -5     //编码长度越界,错误代码为(-5)*strip_num
#define LEN_NO_EQU  -3      //长度不一致


#define TAG_INFO_LEN  30    //tag 最大长度为30

//时间时期格式为"20XX:mm:dd:hh:mm:ss"
//调用顺序，先设置Tag信息,再调用userDecoderTIF
/******************************************************************************
** 函数名称:       userSetTIFTag
** 输入参数:
** pStreamBuf ----Tag写入地址
** imgWidth  ----图像宽度
** imgHeight ----图像高度
** pImgName  ----Document Name
** pImgDiscrp----Image discretion
** pImgMake  ----Image Maker
** pImgModel --- Make Model
** pImgSoft  --- Software
** pImgDate ---- DateTime
** pImgArtist--- Image Artist
** 输出参数；
** 返回类型:  int 型,返回0表示成功
**                   返回1表示失败
** 描述 :      设置TIF tag 信息
*****************************************************************************/

extern int  userSetTIFTag(void *pStreamBuf,
                     int   imgWidth,
                     int   imgHeight,
                     char* pImgName,                //Document Name
                     char* pImgDiscrp,              //Image discretion
                     char* pImgMake,               //Image Maker
                     char* pImgModel,               //Make Model
                     char* pImgSoft,                //Software
                     char* pImgDate,                //DateTime
                     char* pImgArtist);	            //Image Artist

/*******************************************************************************

**  函数名称:           userTIFDecoder
**  输入参数
**  inStream     ---    unsigned char*类型,表示读取数据的地址起始指针
**  outStream    ---    unsigned char*类型，表示编码数据的地址起始指针
**	pBufferSize	 ---	函数中返回实际需要Buffer大小
**
**  输出参数:
**  返回类型     ---    int类型,返回1表示数据错误,
								返回2表示系统内存不足,配合返回的pBufferSize，得到实际需要的大小
                                返回0表示编码成功
**
**  描    述:          TIF编码函数
**
*******************************************************************************/

extern int userTIFDecoder(
                unsigned char * inStream,              //输入数据
	            unsigned char * outStream,             //输出数据
	            int   imgWidth,                        //图片宽度
	            int   imgHeight,                       //图片高度
	            int*    pBufferSize);                   //码流长度


#ifdef __cplusplus
}
#endif
#endif


