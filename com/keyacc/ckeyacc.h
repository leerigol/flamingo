#ifndef CKEYACC_H
#define CKEYACC_H

#include <QtCore>

namespace key_acc {

enum KeyAcc
{
    e_key_raw,          //! no acc
    e_key_one,
    e_key_square,
    e_key_trible,
    e_key_e,

    e_key_one_125,      //!< to integer index
    e_key_square_125,
    e_key_trible_125,
    e_key_e_125,

    e_key_power_2,      //!< 2^x, x + n
};

class CKeyAcc
{
public:
    static int acc( KeyAcc type, int count );
    static bool is125Acc( KeyAcc type );

    static int to125Index( int val, int dir );
    static int to125Index( qlonglong val, int dir );

    static void toValue( int index, int &val );
    static void toValue( int index, qlonglong &val );

};

}

#endif // CKEYACC_H
