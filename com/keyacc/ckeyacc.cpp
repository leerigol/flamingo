#include "ckeyacc.h"

namespace key_acc {

#define array_of( ary ) (int)(sizeof(ary)/sizeof(ary[0]))

/*! \var _acc_e
 * \var _acc_square
 * \var _acc_trible
 * \var _acc_one
*/
static int _acc_e[]={0,1,5,10,20,50,100,200,500,1000,2000};
static int _acc_square[]={0,1,4,9,16,25,36,49,64,81,100,121,144,169,196,225};
static int _acc_trible[]={0,1,8,27,64,125,216,343,512,729,1000};
static int _acc_one[]={0,1,1,1,1,1,1,1,1,};

static int _125Base[]={1,2,5};

#define set_table( table )  pAccTable = table; size = array_of( table );

int CKeyAcc::acc( KeyAcc type, int keyCnt )
{
    int *pAccTable;
    int size;
    int cnt;

    switch( type )
    {
    case e_key_raw:
        return keyCnt;

    case e_key_one:
    case e_key_one_125:
        set_table(_acc_one);
        break;
    case e_key_square:
    case e_key_square_125:
        set_table(_acc_square);
        break;
    case e_key_trible:
    case e_key_trible_125:
        set_table(_acc_trible);
        break;
    case e_key_e:
    case e_key_e_125:
        set_table(_acc_e);
        break;
    case e_key_power_2:
        return keyCnt;

    default:
        return keyCnt;
    }

    //! to positive
    if ( keyCnt < 0 )
    {
        cnt = -keyCnt;
    }
    else
    {
        cnt = keyCnt;
    }

    //! limit the range
    if ( cnt >= size )
    {
        cnt = size - 1;
    }

    if ( keyCnt < 0 )
    {
        return -pAccTable[cnt];
    }
    else
    {
        return pAccTable[cnt];
    }
}

bool CKeyAcc::is125Acc( KeyAcc type )
{
    if ( type == e_key_one_125
         || type == e_key_square_125
         || type == e_key_trible_125
         || type == e_key_e_125 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*!
 * \brief CKeyAcc::to125Index
 * \param val
 * \param dir -1: dec, 1: inc
 * \return
 * 1 ~ 1e9
 */
int CKeyAcc::to125Index( int val, int dir )
{
    int i;

    int e;
    int loop = 0;

    if ( val < _125Base[0] )
    {
        return 0;
    }

    //! increase
    if ( dir > 0 )
    {
        e = 1;
        for ( loop = 0; loop < 9; loop++ )
        {
            for ( i = 0; i < 3; i++ )
            {
                if ( val < _125Base[i]*e )
                {
                    return loop*3 + i-1;
                }
            }

            e = e*10;
        }

        //! 1G
        return loop*3;
    }
    //! decrease
    else if ( dir < 0 )
    {
        e = 1;
        for ( loop = 0; loop < 9; loop++ )
        {
            for ( i = 0; i < 3; i++ )
            {
                if ( val <= _125Base[i]*e )
                {
                    return loop*3 + i;
                }
            }

            e = e*10;
        }

        return loop*3;
    }
    else
    {
        return 0;
    }
}
/*!
 * \brief CKeyAcc::to125Index
 * \param val
 * \param dir
 * \return
 * 1e0 ~ 5e18
 */
int CKeyAcc::to125Index( qlonglong val, int dir )
{
    int i;

    qlonglong e;
    int loop = 0;

    if ( val < _125Base[0] )
    {
        return 0;
    }

    //! increase
    if ( dir > 0 )
    {
        e = 1;
        for ( loop = 0; loop < 19; loop++ )
        {
            for ( i = 0; i < 3; i++ )
            {
                if ( val < _125Base[i]*e )
                {
                    return loop*3 + i-1;
                }
            }

            e = e*10;
        }

        return loop*3;
    }
    //! decrease
    else if ( dir < 0 )
    {
        e = 1;
        for ( loop = 0; loop < 19; loop++ )
        {
            for ( i = 0; i < 3; i++ )
            {
                if ( val <= _125Base[i]*e )
                {
                    return loop*3 + i;
                }
            }

            e = e*10;
        }

        return loop*3-1;
    }
    else
    {
        return 0;
    }
}

void CKeyAcc::toValue( int index, int &val )
{
    if ( index < 0 )
    {
        val = 0;
        return;
    }
    //! 2G
    else if ( index >= (9*3+2) )
    {
        val = 2100000000;
        return;
    }
    else
    {}

    val = _125Base[ index % 3] * pow( 10, (index/3) );
}
void CKeyAcc::toValue( int index, qlonglong &val )
{
    if ( index < 0 )
    {
        val = 0;
        return;
    }
    //! 5E18
    else if ( index >= 19*3 )
    {
        index = 19*3 - 1;

        val = 9000000000000000000ll;
        return;
    }
    else
    {}

    val = _125Base[ index % 3] * pow( 10, (index/3) );
}

}

