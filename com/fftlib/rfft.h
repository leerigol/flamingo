#ifndef RFFT_H
#define RFFT_H

#include <QtCore>

#include "NE10.h"

#include "rfftc.h"
#include "../../include/dsocfg.h"   //! dsoPoint

namespace rfft {

class RWindow
{
public:
    RWindow();
    ~RWindow();

public:
    int load( const QString &file );
    int set( RWindow &rawWindow, int size );

public:
    fftWindow mWindow;
    int mLen;

    float *mDat;
    int mCap;
};

class FFTContext
{
public:
    FFTContext();
    ~FFTContext();

public:
    int config( int len, int size );
    bool missWindowCache( fftWindow wnd, int len );
    int loadWindow( const QString &name,
                    fftWindow wnd,
                    int len );
public:
    int mLen;   /*!< real len */
    int mSize;  /*!< fft size */
    float *pBuf;/*!< data buf */

    RWindow mCacheWindow;   /*!< aligned to the data size */
                            /*!< config */
    ne10_fft_r2c_cfg_float32_t mCfg;
    ne10_fft_cpx_float32_t *m_pCpxBuf;
};

class RFFT
{
public:
    static void init();
    static void deinit();

    static int addWindow( fftWindow wnd,
                          const QString &name );

    //! for real value
    static int fft(float *dataIn, int inLen,
                    int size,
                    fftWindow window,
                    fftSpecUnit unit,
                    float *dataOut,
                    int offset=0, int outLen = -1);

    //! for point
    static int fft(DsoPoint *dataIn, int inLen,
                    int size,
                    fftWindow window,
                    fftSpecUnit unit,
                    float *dataOut,
                    int offset = 0, int outLen = -1);

private:
    static int contextProc( int len, int size );

    static int windowProc( int len, fftWindow window );
    static int specProc(fftSpecUnit unit,
                         float *dataOut,
                         int offset, int inLen, int size, int outLen );

    static int fill0( float *pData, int len, int size );
    static int fill0( DsoPoint *pData, int len, int size );

    static int loadWindow( fftWindow wnd, int len );

    static int windowLize();
    static int specLize(fftSpecUnit unit,
                         int offset,
                         int inLen , int size, int outLen);

    static int specRms(int inLen, int outLen );
    static int specDbm(int inLen, int outLen );
    static int specDb(int inLen, int outLen );
    static int specPhase( int offset, int len );

private:
    static QMap< int, QString > windowFiles;
    static FFTContext fftContext;
};

}


#endif // RFFT_H
