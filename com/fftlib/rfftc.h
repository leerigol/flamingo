#ifndef RFFTC
#define RFFTC

#include "../../include/dsocfg.h"
typedef enum fftWindow
{
    fft_rectangle,
    fft_blackman,
    fft_hanning,
    fft_hamming,

    fft_flattop,
    fft_triangle,
}fftWindow;

typedef enum fftSpecUnit
{
    fft_spec_rms,
    fft_spec_db,
    fft_spec_dbm,
    fft_spec_phase,
}fftSpecUnit;

#ifdef __cplusplus
extern "C"{
#endif
int fft_c( float *dataIn, int len,
            int size,
            fftWindow window,
            fftSpecUnit unit,
            float *dataOut,
            int offset = 0, int outLen = -1 );
int fft_c_point( DsoPoint *dataIn, int len,
            int size,
            fftWindow window,
            fftSpecUnit unit,
            float *dataOut,
            int offset = 0, int outLen = -1 );
#ifdef __cplusplus
}
#endif

#endif // RFFTC

