#include "../../include/dsodbg.h"
#include <QFile>

#include "../../com/neon/projectNe10-Ne10-a08b29d/inc/NE10.h"
#include "rfft.h"

#include "../../baseclass/resample/reSample.h"

namespace rfft {

QMap< int, QString > RFFT::windowFiles;
FFTContext RFFT::fftContext;

RWindow::RWindow()
{
    mWindow = fft_rectangle;
    mLen = 0;
    mDat = NULL;
    mCap = 0;
}
RWindow::~RWindow()
{
    if (  NULL != mDat )
    {
        delete []mDat;
    }
}

int RWindow::load( const QString &file )
{
    QFile wndFile(file);

    bool b = QFile::exists(file) ;
    Q_ASSERT( b );

    if ( !wndFile.open( QIODevice::ReadOnly ) )
    {
        return -1;
    }

    mCap = (int)wndFile.size();
    Q_ASSERT( mCap > 4 );

    mLen = mCap / sizeof(float);
    mDat = R_NEW( float[ mCap ] );
    Q_ASSERT( mDat != NULL );

    wndFile.read( (char*)mDat, mCap );

    wndFile.close();

    return 0;
}

int RWindow::set( RWindow &rawWindow, int len )
{
    //! raw window lack
    if ( rawWindow.mLen < len )
    {
        Q_ASSERT( false );
        return -1;
    }

    //! more data
    if ( mCap < len * (int)sizeof(float) )
    {
        delete []mDat;

        mDat = R_NEW( float[ len ] );
        Q_ASSERT( NULL != mDat );
        mCap = len * sizeof(float);
    }

    //! cache window
    mLen = len;
    mWindow = rawWindow.mWindow;

    //! compress window
    reSampleExt( rawWindow.mLen,
              len,
              rawWindow.mDat, mDat );

    return 0;
}

FFTContext::FFTContext()
{
    mLen = 0;
    mSize = 0;
    pBuf = NULL;

    mCfg = NULL;
    m_pCpxBuf = NULL;
}

FFTContext::~FFTContext()
{
    if ( NULL != pBuf )
    {  delete []pBuf; }

#ifndef _SIMULATE
    if ( NULL != mCfg )
    {  ne10_fft_destroy_r2c_float32( mCfg ); }
#endif
    if ( NULL != m_pCpxBuf )
    { delete []m_pCpxBuf; }
}

int FFTContext::config( int len, int size )
{
    //! more data
    if ( size != mSize )
    {
        mSize = size;

        //! destroy
        if ( pBuf != NULL ) { delete []pBuf; }
        if ( m_pCpxBuf != NULL ) { delete[]m_pCpxBuf; }
#ifndef _SIMULATE
        if ( NULL != mCfg )
        {  ne10_fft_destroy_r2c_float32( mCfg ); }
#endif
        //! new again
        pBuf = R_NEW( float[ size ] );
        Q_ASSERT( NULL != pBuf );

        m_pCpxBuf = R_NEW( ne10_fft_cpx_float32_t[ size ] );
        Q_ASSERT( NULL != m_pCpxBuf );

#ifndef _SIMULATE
        mCfg = ne10_fft_alloc_r2c_float32( size );
        Q_ASSERT( NULL != mCfg );
#endif
    }

    mLen = len;

    return 0;
}

bool FFTContext::missWindowCache( fftWindow wnd, int len )
{
    if ( mCacheWindow.mWindow != wnd ) { return true; }
    if ( mCacheWindow.mLen != len ) { return true; }

    return false;
}

int FFTContext::loadWindow( const QString &name,
                            fftWindow wnd,
                            int len )
{
    RWindow rawWindow;
    if ( 0 != rawWindow.load( name ) )
    {
        return -1;
    }

    rawWindow.mWindow = wnd;

    //! cache window
    return mCacheWindow.set( rawWindow, len );
}

void RFFT::init()
{
    windowFiles.clear();
}
void RFFT::deinit()
{
    windowFiles.clear();
}

/*!
 * \brief RFFT::addWindow
 * \param wnd
 * \param name
 * \return
 * 添加窗文件路径
 */
int RFFT::addWindow( fftWindow wnd,
                     const QString &name )
{
    int wndId;

    wndId = (int)wnd;
    if ( windowFiles.contains(wndId) )
    {
        windowFiles[wndId] = name;
    }
    else
    {
        windowFiles.insert( wndId, name );
    }

    return 0;
}

/*!
 * \brief RFFT::fft
 * \param dataIn
 * \param len 有效的数据长度
 * \param size fft计算点数，2的n次幂
 * \param window 窗类型
 * \param unit 单位类型:db,dbm,rms
 * \param dataOut,输出数据，长度为fft size的一半
 * \return
 */
int RFFT::fft(float *dataIn, int inLen,
                int size,
                fftWindow window,
                fftSpecUnit unit,
                float *dataOut,
                int offset, int outLen)
{
    int ret;

    //! check size 2^n
    Q_ASSERT( dataIn != NULL );

    ret = contextProc( inLen, size );
    if ( ret != 0 )
    { return ret; }

    ret = fill0( dataIn, inLen, size );
    if ( ret != 0 )
    { return ret; }

    ret = windowProc( inLen, window );
    if ( ret != 0 )
    { return ret; }

    ret = specProc( unit, dataOut, offset, inLen, size , outLen);
    if ( ret != 0 )
    { return ret; }

    return 0;
}

int RFFT::fft(DsoPoint *dataIn, int inLen,
                int size,
                fftWindow window,
                fftSpecUnit unit,
                float *dataOut,
                int offset, int outLen)
{
    int ret;

    //! check size 2^n
    Q_ASSERT( dataIn != NULL );

    ret = contextProc( inLen, size );
    if ( ret != 0 )
    { return ret; }

    ret = fill0( dataIn, inLen, size );
    if ( ret != 0 )
    { return ret; }

    ret = windowProc( inLen, window );
    if ( ret != 0 )
    { return ret; }

    ret = specProc(unit, dataOut, offset, inLen, size ,outLen);
    if ( ret != 0 )
    { return ret; }

    return 0;
}

int RFFT::contextProc( int len, int size )
{
    if ( 0 != fftContext.config( len, size ) )
    {
        Q_ASSERT( false );
        return -1;
    }

    return 0;
}

int RFFT::windowProc( int len, fftWindow window )
{
    if ( fft_rectangle != window)
    {
        if ( 0 != loadWindow( window, len ) )
        {
            Q_ASSERT( false );
            return -1;
        }

        if ( 0 != windowLize() )
        {
            Q_ASSERT( false );
            return -2;
        }
    }

    return 0;
}

int RFFT::specProc(fftSpecUnit unit,
                    float *dataOut,
                    int offset, int inLen, int size, int outLen )
{
    //! neon fft
    ne10_fft_r2c_1d_float32_neon( fftContext.m_pCpxBuf,
                                  fftContext.pBuf,
                                  fftContext.mCfg );
    //! speclize
    int specLen;
    specLen = specLize( unit, offset, inLen, size, outLen );
    if ( specLen <= 0 )
    { return -1; }

    //! export spec data
    memcpy( dataOut, fftContext.pBuf + offset, specLen * sizeof(float) );

    return 0;
}

int RFFT::fill0( float *pData, int len, int size )
{
    //! copy
    memcpy( fftContext.pBuf, pData, len * sizeof(float) );

    //! fill 0
    if ( len <= size )
    {
        memset( (float*)fftContext.pBuf + len, 0, (size-len)*sizeof(float) );
    }
    else
    {
        qDebug("to resample");
        Q_ASSERT(false);
    }

    return 0;
}

int RFFT::fill0( DsoPoint *pData, int len, int size )
{
    //! set data
    for ( int i = 0; i < len; i++ )
    {
        fftContext.pBuf[i] = pData[i];
    }

    //! fill 0
    if ( len <= size )
    {
        for ( int i = len; i < size; i++ )
        {
            fftContext.pBuf[i] = 0;
        }
    }
    else
    {
        qDebug("to resample");
        Q_ASSERT(false);
    }

    return 0;
}

int RFFT::loadWindow( fftWindow wnd, int len )
{
    if ( !fftContext.missWindowCache(wnd,len) )
    {
        return 0;
    }

    //! no window set
    int wndId = (int)wnd;
    if ( !windowFiles.contains( wndId ) )
    {
        return -1;
    }

    //! load window
    return fftContext.loadWindow( windowFiles[(int)wnd], wnd, len );
}

/*!
 * \brief RFFT::windowLize
 * \return
 * 对数据加窗
 */
int RFFT::windowLize()
{
#ifndef _SIMULATE
    //! by neon
    ne10_mul_float_neon( fftContext.pBuf, fftContext.pBuf, fftContext.mCacheWindow.mDat, fftContext.mLen );
#else
    //! foreach data
    for ( int i = 0; i < fftContext.mLen; i++ )
    {
        fftContext.pBuf[i] *= fftContext.mCacheWindow.mDat[i];
    }
#endif

    return 0;
}

//! return the length
int RFFT::specLize( fftSpecUnit unit,
                    int offset, int inLen, int size,int outLen )
{
    //! check offset && len
    Q_ASSERT( offset >= 0 );
    if ( inLen < 0 )
    { inLen = fftContext.mSize/2; }

    //! spec
    if ( fft_spec_rms == unit )
    {
        return specRms(inLen, size );
    }
    else if ( fft_spec_db == unit )
    {
        return specDb(inLen, size );
    }
    else if ( fft_spec_dbm == unit)
    {
        return specDbm(inLen, size );
    }
    else if ( fft_spec_phase == unit )
    {
        return specPhase(offset, outLen );
    }
    else
    {
        Q_ASSERT( false );
        return 0;
    }
}

int RFFT::specRms( int inLen, int outLen )
{
    int i;
    double modeSquare;

    //! 直流分量和复数模差N倍，其他频率分量和复数模差N/2倍, 并求有效值。
    for ( i = 0; i < fftContext.mSize/2; i++ )
    {
        modeSquare = fftContext.m_pCpxBuf[i].r * fftContext.m_pCpxBuf[i].r
                      + fftContext.m_pCpxBuf[i].i * fftContext.m_pCpxBuf[i].i;
        //! sqrt(modeSquare) = Rms * sqrt(2) * mSize/2;
        //! zx_add:补0导致的幅度误差
        fftContext.pBuf[i] = sqrt(modeSquare * 2) / fftContext.mSize * outLen / inLen;
    }

    fftContext.pBuf[0] = fftContext.pBuf[0] / 4  * outLen / inLen;

#if 0
    QFile file("/rigol/trace/test");
    QTextStream out(&file);
    for(int i = 0; i < fftContext.mSize;i++)
    {
        out<<"i"<<fftContext.pBuf[i];
        i = i+2;
    }
#endif

    return fftContext.mSize/2;
}

int RFFT::specDbm(int inLen, int outLen)
{
    specDb( inLen, outLen );

    for (int i = 0; i < fftContext.mSize/2; i++ )
    {
        fftContext.pBuf[i] = fftContext.pBuf[i] + 13.01;
    }

    return fftContext.mSize/2;
}

int RFFT::specDb(int inLen , int outLen)
{
    int i;

    double modeSquare;
    modeSquare = fftContext.m_pCpxBuf[0].r * fftContext.m_pCpxBuf[0].r
                  + fftContext.m_pCpxBuf[0].i * fftContext.m_pCpxBuf[0].i;
    fftContext.pBuf[0] = 20*log10(sqrt((modeSquare + 1e-20)/2) * outLen / inLen /fftContext.mSize) ;

    for ( i = 1; i < fftContext.mSize/2; i++ )
    {
        modeSquare = fftContext.m_pCpxBuf[i].r * fftContext.m_pCpxBuf[i].r
                      + fftContext.m_pCpxBuf[i].i * fftContext.m_pCpxBuf[i].i;
        fftContext.pBuf[i] = 20*log10(sqrt((modeSquare + 1e-20)/2) * outLen / inLen / (fftContext.mSize/2));
    }

    return fftContext.mSize/2;
}

//! only for a few items
int RFFT::specPhase( int offset, int len )
{
    int i;
    for ( i = offset; i < (offset+len) && i < fftContext.mSize/2; i++ )
    {
        fftContext.pBuf[i] =  atan2( fftContext.m_pCpxBuf[i].i,
                                     fftContext.m_pCpxBuf[i].r );
    }

    return (i - offset);
}

}

int fft_c(  float *dataIn, int len,
            int size,
            fftWindow window,
            fftSpecUnit unit,
            float *dataOut,
            int offset, int /*outLen*/ )
{
    return rfft::RFFT::fft( dataIn, len, size, window,
                            unit,
                            dataOut, offset );
}

int fft_c_point(  DsoPoint *dataIn, int len,
            int size,
            fftWindow window,
            fftSpecUnit unit,
            float *dataOut,
            int offset, int outLen )
{
    return rfft::RFFT::fft( dataIn, len, size, window,
                            unit,
                            dataOut, offset, outLen );
}

