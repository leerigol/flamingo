TEMPLATE = lib
TARGET = ../../lib$$(PLATFORM)/rfft
INCLUDEPATH += .


#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

DEFINES += _DEBUG
INCLUDEPATH += "../../com/neon/projectNe10-Ne10-a08b29d/inc"

CONFIG += static

# Input
HEADERS += rfft.h rfftc.h
SOURCES += rfft.cpp
