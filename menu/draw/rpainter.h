#ifndef RPAINTER_H
#define RPAINTER_H

#include <QPainter>

namespace menu_res {

class RFont : public QFont
{
public:
    RFont();
    RFont(const QString & family,
          int pointSize = -1,
          int weight = -1,
          bool italic = false);
    RFont(const QFont & font,
          QPaintDevice * pd);
    RFont(const QFont & font);
};

class RPainter : public QPainter
{
public:
    RPainter();
    RPainter( QPaintDevice * device );
};

}

#endif // RPAINTER_H
