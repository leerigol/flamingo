
#include <QApplication>

#include "rpainter.h"

namespace menu_res {

RFont::RFont() : QFont()
{
}
RFont::RFont(const QString & family,
      int pointSize,
      int weight,
      bool italic) : QFont( family, pointSize, weight, italic )
{
    setStyleStrategy( QFont::NoAntialias );
}
RFont::RFont(const QFont & font,
      QPaintDevice * pd) : QFont( font, pd )
{
    setStyleStrategy( QFont::NoAntialias );
}
RFont::RFont(const QFont & font) : QFont( font )
{
    setStyleStrategy( QFont::NoAntialias );
}

RPainter::RPainter()
{
}

RPainter::RPainter( QPaintDevice * device ) : QPainter(device)
{
//    QFont font("arial");
    QFont font = qApp->font();
//    font.setStyleStrategy( QFont::NoAntialias );
    setFont( font );
}

}

