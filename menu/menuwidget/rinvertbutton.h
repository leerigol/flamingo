#ifndef RINVERTBUTTON_H
#define RINVERTBUTTON_H

#include "../menuwidget/rcontrol.h"

#include "../menustyle/rcontrolstyle.h"

#include "rmenuwidget.h"

namespace menu_res {

/*!
 * \brief The RInvertButton class
 * -二值控件
 * -bool类型
 */
class RInvertButton : public RMenuWidget
{
    Q_OBJECT

public:
    enum Type
    {
        t_bool,
        t_int,
    };

protected:
    static RInvertButtonStyle _style;

public:
    RInvertButton( QString txt="", QWidget *parent=0 );

protected:

protected:
    void paintEvent( QPaintEvent *event );

public:
    virtual RMenu *queryOptionMenu();

    virtual void on_value_changed( int msg );

public:
    void setType( Type t );
    void setOptions( int opt0, int opt1 );

protected:
    virtual void onClick();

private:
    Type mType;
    int mOpt0, mOpt1;
};

}

#endif // RINVERTBUTTON_H
