#include "ripedit.h"

namespace menu_res {

RIpEditStyle RIpEdit::_style;

RIpEdit::RIpEdit( QWidget *parent ) : RSegEdit( parent )
{
    mIp = 0x12345678;

    mFocus = RTimeEditStyle::focus_l3;
    mFocusA = RTimeEditStyle::focus_l0;
    mFocusB = RTimeEditStyle::focus_l3;

    _style.loadResource("ui/menu/ip_edit/");
    _style.resize( this );

    m_pKnob->move( _style.mKnobXY );
}

void RIpEdit::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);

    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }
    _style.paintEvent( painter );

    do
    {
        Q_ASSERT( NULL != m_pResMenuItem );
        Q_ASSERT( NULL != m_pResMenuItem->m_pResItem );

        //! fg
        eMenuRes resType;
        QRect boundRect;

        resType = m_pResMenuItem->m_pResItem->getType();
        boundRect = _style.mCaptionRect;
        //! drawIcon
        if ( is_attr(resType, MR_Icon) )
        {
            QImage img;
            int v = m_pResMenuItem->m_pResItem->getImage( img );
            Q_ASSERT( v == 0 );

            _style.drawIcon( painter,
                             img,
                             boundRect );
            boundRect.adjust( img.width(), 0, 0, 0 );
        }

        //! draw text
        if ( is_attr(resType, MR_Str) )
        {
            _style.drawCaption( painter,
                                boundRect,
                                m_pResMenuItem->getTitle() );
        }
    }while(0);

    //! content
    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), isTuneNow() );
    m_pKnob->setState( tuneStat );

    _style.drawIp( painter, mIp, mFocus, tuneStat );
}

void RIpEdit::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    int ip;
    queryCmd( ip );
    mIp = ip;

    update();
}

void RIpEdit::tuneInc( int keyCnt )
{ focusEdit( keyCnt, true ); }
void RIpEdit::tuneDec( int keyCnt )
{ focusEdit( keyCnt, false); }

void RIpEdit::focusEdit( int keyCnt, bool bInc )
{
    ui_attr::PackInt val;

    queryCmd( val );
    mIp = (val.val32 & 0xffffffff);

    //! focus value change
    switch( mFocus )
    {
        case RIpEditStyle::focus_l0:
        val.set( (int)( ( mIp >> 24 ) & 0xff ) );
        break;

        case RIpEditStyle::focus_l1:
        val.set( (int)( ( mIp >> 16 ) & 0xff ) );
        break;

        case RIpEditStyle::focus_l2:
        val.set( (int)( ( mIp >> 8 ) & 0xff ) );
        break;

        case RIpEditStyle::focus_l3:
        val.set( (int)( ( mIp >> 0 ) & 0xff ) );
        break;

        default:
        return;
    }

    RControl::keyIncDecProc( val, bInc, keyCnt );

    //! unpack val
    switch( mFocus )
    {
        case RIpEditStyle::focus_l0:
        setIp0( val.val32 );
        break;

        case RIpEditStyle::focus_l1:
        setIp1( val.val32 );
        break;

        case RIpEditStyle::focus_l2:
        setIp2( val.val32 );
        break;

        case RIpEditStyle::focus_l3:
        setIp3( val.val32 );
        break;

        default:
        return;
    }

    //! do cmd
    doCmd( (int)mIp );
}

void RIpEdit::setIp( quint32 ip )
{
    mIp = ip;
}
quint32 RIpEdit::getIp()
{
    return mIp;
}

void RIpEdit::setIp0( int ip )
{
    RControl::rangeRoll( ip, 0, 255 );

    mIp = (mIp & 0x00ffffff) | ( (ip<<24) & 0xff000000 );

    update();
}
int RIpEdit::getIp0()
{
    return (mIp >> 24 ) & 0xff;
}

void RIpEdit::setIp1( int ip )
{
    RControl::rangeRoll( ip, 0, 255 );

    mIp = (mIp & 0xff00ffff) | ( (ip<<16) & 0x00ff0000 );

    update();
}
int RIpEdit::getIp1()
{
    return (mIp >> 16 ) & 0xff;
}

void RIpEdit::setIp2( int ip )
{
    RControl::rangeRoll( ip, 0, 255 );

    mIp = (mIp & 0xffff00ff) | ( (ip<<8) & 0x0000ff00 );

    update();
}
int RIpEdit::getIp2()
{
    return (mIp >> 8 ) & 0xff;
}

void RIpEdit::setIp3( int ip )
{
    RControl::rangeRoll( ip, 0, 255 );

    mIp = (mIp & 0xffffff00) | ( (ip<<0) & 0x000000ff );

    update();
}
int RIpEdit::getIp3()
{
    return (mIp >> 0 ) & 0xff;
}

}

