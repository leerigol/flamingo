#ifndef RLISTWIDGETITEM_H
#define RLISTWIDGETITEM_H

#include "../menuwidget/rcontrol.h"

namespace menu_res {

class RPopListModal;
class RPopupView;
/*!
 * \brief The RPopListItem class
 * 弹出菜单列表
 */
class RPopListItem
{

public:
    RPopListItem();

public:
    void setResItem( CResItem * pResItem );
    int getValue();

    void setEnable( bool b );
    bool getEnable();

    void setVisible( bool b );
    bool getVisible();

    void setCheckable( bool b );
    bool getCheckable();

    void setChecked( bool b );
    bool getChecked();

private:
    CResItem *m_pResItem;

    bool mEnable;
    bool mVisible;

    bool mCheckable;
    bool mChecked;

friend class RCombox;
friend class RComboxCheck;
friend class RPopListModal;
friend class RPopupView;
};
/*!
 * \brief The RPopListModal class
 * 弹出菜单数据模型
 */
class RPopListModal
{
public:
    RPopListModal();
public:
    void setList( QList< RPopListItem *> *m_pList );

    void selectOption( int optValue );
    void setCurrent( int index );

    void setTitle( const QString &title );

    int size();

public:
    QList< RPopListItem *> mList;   //! visible item list
    int mCurrentIndex;
    QString mTitle;
};

}

#endif // RLISTWIDGETITEM_H
