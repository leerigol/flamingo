#ifndef RCOMBOX_H
#define RCOMBOX_H

#include "../menuwidget/rcontrol.h"

#include "../menustyle/rcontrolstyle.h"

#include "rmenuwidget.h"

namespace menu_res {

class RPopListItem;
class RPopupView;

typedef QList< RPopListItem *> RPopupList;
typedef QPair<int, RPopupList* > tComboxList;

/*!
 * \brief The RCombox class
 * -多选一控件
 */
class RCombox : public RMenuWidget
{
    Q_OBJECT

public:
    enum RComboxMode
    {
        RCombox_norm,           /*!< sel + apply */
        RCombox_check,          /*!< sel,check */
        RCombox_ack,            /*!< sel,apply */
    };

    /*!
     * \brief The RComboxPwr enum
     * combox状态
     * -sleep->active->popup
     * -菜单上的combox存在一个状态切换过程
     * -
     */
    enum RComboxPwr
    {
        RCombox_idle,           //! to popup
        RCombox_sleep,          //! to active
    };

    enum RComboxPwrOp
    {
        Pwr_low,
        Pwr_next,
    };

    enum RComboxAttr
    {
        combox_pwr,
        combox_compress,
    };

    enum RComboxState
    {
        combox_idle,
        combox_selecting,
        combox_selected,
        combox_inputing,
    };

protected:
    static QList< tComboxList * > _cacheList;
    static RCombox_Style _style;

    static QWidget *_popupParent;
    static RCombox *_focusCombox;
public:
    static RPopupView *_popupView;

public:
    static void cacheOpenPopupList();
    static void insertPopupList( int id, RPopupList *pWig );
    static RPopupList * findPopupList( int id );
    static void cacheClosePopupList();

public:
    RCombox( QString txt="",
             QWidget *parent = 0,
             QWidget *popParent = 0
             );
    ~RCombox();

    virtual void onParentDeactive();
protected:
    //! popup item list
    RPopupList *m_pPopupItemList;
    bool mbOwner;
    bool        m_bNeedClick;

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual void hideEvent(QHideEvent *);

public:
    virtual RMenu *queryOptionMenu();

    virtual void on_enable_changed( int msg, int opt, bool b );
    virtual void on_visible_changed( int msg, int opt, bool b );

    virtual void on_enable_changed( int msg, bool b );
    virtual void on_visible_changed( int msg, bool b );

    virtual void on_value_changed( int msg );

    virtual void on_context_changed( int msg );
Q_SIGNALS:

protected:
    virtual void onClick();

    virtual void tuneInc( int cnt );
    virtual void tuneDec( int cnt );

    virtual void zEnter();
    virtual void stepNext();

protected Q_SLOTS:
    void on_select_changed( int val );
    void on_apply_value( int val );
    void on_popupview_hide();    
    void on_press(QPoint);

protected:
    void paintBg( QPainter &painter );
    void paintCaption( QPainter &painter );
    void paintOption( QPainter &painter );
    void paintCaptionOnOption( QPainter &painter );

    void pwrOp( RComboxPwrOp op );
    bool pwrEventFilter();

    void setTuneView( RPopupView * pPopView );

    void collectValidList( RPopupList &validList );
    int valueToIndex( int val, RPopupList *pList );
    int indexToValue( int index, RPopupList *pList  );

    void updateEnable();
    void closePop();

public:
    void setResItem( CResMenuItem *pItem );

    void setMode( RComboxMode mode );
    RComboxMode getMode();

    void setLowerPwr( RComboxPwr pwr );
    void setNodeCompress( bool bCompress );

    void setAttr( int attr );

protected:
    RPopListItem *findOption( int opt );

    bool sumVisible();
    bool sumEnable();

    void toState( RComboxState stat );

protected:
    RComboxMode mComboxMode;
    RComboxPwr  mPwr, mLowPwr;

    RComboxState mComboxState;

    int mAttr;
    bool mbNodeCompress;
};

}



#endif // RCOMBOX_H
