#include "../arch/rmenu.h"
#include "rcombox_iif.h"

namespace menu_res
{
RCombox_iif::RCombox_iif( QString txt,
                          QWidget *parent ,
                          QWidget *popParent )
                         : RCombox( txt, parent, popParent )
{
    m_pKnob = new RFuncKnob(this);
    Q_ASSERT( m_pKnob != NULL );

    m_pKnob->move( RCombox::_style.mKnobXY );

    //! 复合框的选项和数值消息是不同的
    mValueSubCmd = combox_value;
}

void RCombox_iif::setTextVisible( bool b )
{
    RControl::setTextVisible( b );

    if ( b )
    {
        m_pKnob->move( _style.mKnobXY );
    }
    else
    {
        m_pKnob->move( _style.mKeyKnobXY );
    }
}

void RCombox_iif::setKnobView( eControlKnob view )
{
    m_pKnob->setView( view );
}

void RCombox_iif::keyReleaseEvent(QKeyEvent * event)
{
    Q_ASSERT( NULL != event );

    if ( mContPara == para_key )
    {
        paraKeyProc( event );
    }
    else if ( mContPara == para_val )
    {
        paraValProc( event );
    }
    else
    {
        Q_ASSERT( false );
    }
}

void RCombox_iif::on_value_changed( int msg )
{
    Q_ASSERT( m_pResMenuItem != NULL );

    if ( mContPara != para_val )
    {
        return;
    }

    //! combox option
    if ( msg == m_pResMenuItem->mId )
    {
        int opt;

        queryCmd( opt );

        //! opt
        setControlValue( opt );

        //! recascade
        RControl::reCascadeMenu();

        //! update the value directly
        queryCmd( mVal, mValueSubCmd );

        update();
    }
    //! combox value
    else if ( msg == m_pResMenuItem->mId + (int)mValueSubCmd )
    {
        queryCmd( mVal, mValueSubCmd );

        update();
    }
    else
    {
        return;
    }
}

void RCombox_iif::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( event != NULL );
    RPainter painter(this);

    //! frame
    paintFrame( painter );

    //! value
    if ( mbText )
    {
        //! draw value
        bool bUiStr;
        QString str;

        //! ui out string
        str = getUiOutStr( bUiStr, mValueSubCmd );
        if ( !bUiStr )
        {
            //! 如果没有设置基数，则认为是整数
            if ( hasUiBase(mValueSubCmd) )
            {
                DsoReal realBase = RControl::getUiRealBase( mValueSubCmd );
                DsoReal realVal;

                if ( mVal.width == 32 )
                { realVal.setVal( (DsoRealType_A)mVal.val32 ); }
                else if ( mVal.width == 64 )
                { realVal.setVal( mVal.val64 );}
                else if ( mVal.width == 128 )
                { realVal = mVal.val128; }
                else
                { Q_ASSERT( false ); }

                realVal = realVal * realBase;

                gui_fmt::CGuiFormatter::format(str,
                                               realVal,
                                               getUiViewFmt(mValueSubCmd)
                                               );
            }
            else
            {
                gui_fmt::CGuiFormatter::format( str,
                                                mVal.val32,
                                                getUiViewFmt(mValueSubCmd) );
            }
            str = getUiPreStr( mValueSubCmd )
                  + str
                  + getUiPostStr( mValueSubCmd );
        }

        //! text
        _style.drawText( painter,
                        _style.mTextRect,
                        str);
    }
}

void RCombox_iif::paraKeyProc( QKeyEvent * event )
{
    Q_ASSERT( NULL != event );
    key_acc::KeyAcc acc;
    int keyAccCnt;
    ui_attr::PackInt zVal;

    //! get ui attr
    queryCmd( mValueSubCmd );

    acc = RControl::getUiAcc();
    keyAccCnt = event->count();

    keyAccCnt = key_acc::CKeyAcc::acc( acc, keyAccCnt );

    if ( event->key() == R_Skey_TuneInc )
    {
        doCmd( keyAccCnt, mValueSubCmd );
    }
    else if ( event->key() == R_Skey_TuneDec )
    {
        doCmd( -keyAccCnt, mValueSubCmd );
    }
    else if ( event->key() == R_Skey_TuneEnter )
    {
        zVal = getUiZVal();
        doCmd( zVal, mValueSubCmd );         /*!< z proc */
    }
    else if ( event->key() == R_Skey_Zval )
    {
        zVal = getUiZVal();
        doCmd( zVal, mValueSubCmd );         /*!< z proc */
    }
    else if ( event->key() == R_Skey_Active )
    {
        setDown(false);
    }
    else
    {
        return;
    }
}
void RCombox_iif::paraValProc( QKeyEvent * event )
{
    Q_ASSERT( NULL != event );
    int key;

    key = event->key();

    if ( key == R_Skey_Active )
    {
        setDown(false);

        emit clicked();
    }
    else if ( key == R_Skey_TuneInc )
    {
        queryCmd( mVal, mValueSubCmd );

        RControl::keyIncDecProc( mVal, true, event->count(), mValueSubCmd );

        doCmd( mVal, mValueSubCmd );
    }
    else if ( key == R_Skey_TuneDec )
    {
        queryCmd( mVal, mValueSubCmd );

        RControl::keyIncDecProc( mVal, false, event->count(), mValueSubCmd );

        doCmd( mVal, mValueSubCmd );
    }
    else if ( key == R_Skey_TuneEnter
              && m_pKnob->getView() == control_knob_tune_pad )
    {
        keyPadProc();
    }
    else
    {
        return;
    }
}

void RCombox_iif::paintFrame( QPainter &painter )
{
    QString str;

    bool bSelected = isTuneNow();
    //! back ground
    if ( !isEnabled() )
    {
        _style.setStatus( element_disabled );
    }
    else if ( isDown() || bSelected)
    {
        _style.setStatus( element_actived_down );
    }
    else
    {
        _style.setStatus( element_deactived );
    }

    //! frame
    _style.paintEvent( painter );

    //! knob
    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), bSelected );
    m_pKnob->setState( tuneStat );

    eMenuRes resType;

    QImage img;
    QRect boundRect;

    //! option type
    resType = m_pResMenuItem->m_pResItem->getOptionType( m_pResMenuItem->mIntVal );
    boundRect = _style.mCaptionRect;

    //! draw icon
    if ( is_attr(resType, MR_Icon) )
    {
        int v = m_pResMenuItem->m_pResItem->getOptionImpage( m_pResMenuItem->mIntVal,
                                                           img );
        Q_ASSERT ( v == 0 );

        _style.drawIcon( painter, img, boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }

    //! draw caption
    //! 根据选项内容显示出标题
    if ( is_attr(resType, MR_Str) )
    {
        str = m_pResMenuItem->m_pResItem->getOptionString( m_pResMenuItem->mIntVal );
        _style.drawText( painter,
                         boundRect,
                         str,
                         Qt::AlignLeft );
    }
}

void RCombox_iif::tuneClick()
{
    if ( isTuneNow() )
    {
        Q_ASSERT( NULL != m_pKnob );
        eControlKnob knobView = m_pKnob->getView();
        if ( knobView == control_knob_tune_pad )
        {
            keyPadProc();
            return;
        }

        if ( knobView == control_knob_pad )
        {
            keyPadProc();
            return;
        }
    }
}

void RCombox_iif::onClick()
{
    if ( doFilter() ) return;

    //! tune focus
    if ( isTuneAble() && !isTuneNow() )
    {
        tuneFocusIn();
        return;
    }

    //! input popup
    if ( mComboxState == combox_selected )
    {
        tuneClick();
        return;
    }
    else
    {}

    //! selection by click
    int items;
    Q_ASSERT( m_pPopupItemList != NULL );
    items = m_pPopupItemList->size();

    //! pop up selection
    if ( !is_attr( mUiPara1, 0x100 ) || items > 2 )
    {
        RCombox::onClick();
        return ;
    }
    //! next selection
    else
    {}

    Q_ASSERT( items > 0 );

    //! option
    int option;
    m_pResMenuItem->getValue( option );
    int index = valueToIndex( option, m_pPopupItemList );

    index = ( index + 1 ) % items;
    int val = indexToValue(  index, m_pPopupItemList );

    //! select option now
    doCmd( m_pResMenuItem->mId, val );

    toState( combox_selected );
}

void RCombox_iif::keyPadProc( )
{
    CKeyPadContext keyPadContext;

    //! preset value mode
    if ( mAttr & 0x40 )
    {
        buildKeypadContext( keyPadContext, mVal );
    }
    else
    {
        buildKeypadContext( keyPadContext, mVal, combox_value );
    }

    //! 左上角的位置
    QPoint pt = mapToGlobal( QPoint(0,0) );

    InputKeypad::rst();

    connect( InputKeypad::getInstance(),
             SIGNAL(OnOK(DsoReal)),
             this,
             SLOT(onInput(DsoReal)) );

    connect( InputKeypad::getInstance(),
             SIGNAL(sig_hide()),
             this,
             SLOT(onHide()) );

    connect( InputKeypad::getInstance(),
             SIGNAL(sig_pressed(QPoint)),
             this,
             SLOT(on_press(QPoint)) );


    attachPageWidget( InputKeypad::getInstance() );

    //! config link key
    int id, linkKey;
    id = RMenu::findId( this );
    Q_ASSERT( id != -1 );
    linkKey = REventFilter::menuIndexToKey( id );
    Q_ASSERT( linkKey != -1 );
    InputKeypad::getInstance()->setLinkKey( linkKey );

    InputKeypad::getInstance()->input( pt.x(), pt.y(),
                                       keyPadContext.mView,
                                       keyPadContext.mValueGp );
}

void RCombox_iif::onInput( DsoReal real )
{
    DsoReal realBase;

    realBase = getUiRealBase( mValueSubCmd );

    real.alignBase( realBase );

    if ( mVal.width == 32 )
    {
        doCmd( (int)real.mA, mValueSubCmd );
    }
    else if ( mVal.width == 64 )
    {
        doCmd( real.mA, mValueSubCmd );
    }
    else
    {
        Q_ASSERT( false );
    }

    toState( combox_idle );
}

void RCombox_iif::onHide()
{
    toState( combox_idle );
}

}

