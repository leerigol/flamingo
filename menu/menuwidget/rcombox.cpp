
#include "../arch/rmenu.h"

#include "rpopupview.h"
#include "rpopupitem.h"

#include "rcombox.h"

namespace menu_res {

QList< tComboxList * > RCombox::_cacheList;

RCombox_Style RCombox::_style;
RPopupView *RCombox::_popupView = NULL;
QWidget *RCombox::_popupParent = NULL;
RCombox *RCombox::_focusCombox = NULL;

void RCombox::cacheOpenPopupList()
{
}

void RCombox::insertPopupList( int id, RPopupList *pList )
{
    Q_ASSERT( NULL != pList );
    tComboxList *pPair = new tComboxList();
    Q_ASSERT( NULL != pPair );

    pPair->first = id;
    pPair->second = pList;

    RCombox::_cacheList.append( pPair );
}

RPopupList * RCombox::findPopupList( int id )
{
    tComboxList *pPair;
    foreach( pPair, RCombox::_cacheList )
    {
        if ( pPair->first == id )
        {
            return pPair->second;
        }
    }

    return NULL;
}
void RCombox::cacheClosePopupList()
{
    tComboxList *pPair;
    foreach( pPair, RCombox::_cacheList )
    {
        Q_ASSERT( pPair != NULL );
        delete pPair;
    }

    RCombox::_cacheList.clear();
}

RCombox::RCombox( QString /*txt*/,
         QWidget *parent,
         QWidget *popParent
         ) : RMenuWidget(parent)
{
    //! load resource
    _style.loadResource( "ui/menu/combox/" );
    _style.resize( this );

    //! pop parent
    if ( popParent != NULL )
    {
        RCombox::_popupParent = popParent;
        Q_ASSERT( NULL != RCombox::_popupParent );
    }

    //! pre create popupview
    //! create single instance
    if ( RCombox::_popupView == NULL
         && NULL != RCombox::_popupParent)
    {
        RCombox::_popupView = new RPopupView( RCombox::_popupParent );
        Q_ASSERT( NULL != RCombox::_popupView );
    }

    m_pPopupItemList = NULL;
    mbOwner = false;

    mComboxMode = RCombox_norm;

    mLowPwr = RCombox_idle;
    mPwr = mLowPwr;

    mComboxState = combox_idle;

    mbNodeCompress = false;

    m_bNeedClick = false;
}

RCombox::~RCombox()
{
    if ( m_pPopupItemList != NULL && mbOwner )
    {
        RPopListItem *pItem;

        foreach( pItem, *m_pPopupItemList )
        {
            Q_ASSERT( pItem != NULL );
            delete pItem;
        }

        delete m_pPopupItemList;
        m_pPopupItemList = NULL;
    }
}

void RCombox::onParentDeactive()
{
    pwrOp( Pwr_low );
}

void RCombox::paintEvent( QPaintEvent */*event*/ )
{
    RPainter painter(this);

    paintBg( painter );

    if ( mbText )
    {
        paintCaption( painter );
        paintOption( painter );
    }
    else
    {
        paintCaptionOnOption( painter );
    }
}

void RCombox::hideEvent(QHideEvent *event)
{
    RMenuWidget::hideEvent( event );

    toState( combox_idle );
}

RMenu *RCombox::queryOptionMenu()
{
    int opt;

    queryCmd( opt );

    return getOptionMenu( opt );
}

void RCombox::on_enable_changed( int msg, int opt, bool b )
{
    if ( !cmdMatch(msg ) ) return;

    RPopListItem *pPopItem = findOption( opt );
    if ( NULL == pPopItem )
    {
        LOG_DBG()<<msg<<opt;
        Q_ASSERT( pPopItem != NULL );
    }

    pPopItem->setEnable( b );

    //! update the status
    updateEnable();
}

void RCombox::on_visible_changed( int msg, int opt, bool b )
{
    if ( !cmdMatch(msg ) ) return;

    RPopListItem *pPopItem = findOption( opt );
    if ( pPopItem != NULL )
    {
        pPopItem->setVisible( b );
    }

    //! update enable
    updateEnable();
}

void RCombox::on_enable_changed( int msg, bool b )
{
    if ( !cmdMatch(msg) ) return;

    RMenuWidget::on_enable_changed( msg, b );

    if ( !b )
    { closePop(); }
}
void RCombox::on_visible_changed( int msg, bool b )
{
    if ( !cmdMatch(msg) ) return;

    RMenuWidget::on_visible_changed( msg, b );

    if ( !b )
    { closePop(); }
}

void RCombox::on_value_changed( int msg )
{
    if ( !cmdMatch(msg) ) return;

    int opt;

    queryCmd( opt );

    setControlValue( opt );
    update();

    //! no expand
    if ( getUiNodeCompress() )
    {}
    else
    {
        //! service changed
        RControl::reCascadeMenu();
    }
}

void RCombox::on_context_changed( int msg )
{
    if ( !cmdMatch(msg) ) return;

    //! close the popup
    if ( NULL != RCombox::_popupView )
    { RCombox::_popupView->hide(); }
}

void RCombox::onClick()
{
    int linkId;

    if ( doFilter() ) return;

    if ( !isVisible() ) return;

    //! create single instance
    if ( NULL == RCombox::_popupView )
    {
        RCombox::_popupView = new RPopupView( RCombox::_popupParent );
        Q_ASSERT( RCombox::_popupView != NULL );
    }

    //! hide
    if ( RCombox::_popupView->isVisible() )
    {
        RCombox::_popupView->hide();
        RCombox::_popupView->disconnect();

        setDown(false);
    }
    //! show
    else
    {
        Q_ASSERT( m_pResMenuItem );
        Q_ASSERT( m_pPopupItemList );

        //! set selected
        RPopListModal modal;

        modal.setTitle( m_pResMenuItem->getTitle() );

        modal.setList( m_pPopupItemList );
        modal.selectOption( m_pResMenuItem->mIntVal );

        RCombox::_popupView->setModal( modal );

        setTuneView( RCombox::_popupView );

        RCombox::_popupView->disconnect();
        connect( RCombox::_popupView, SIGNAL(sig_select_changed(int)),
                 this, SLOT(on_select_changed(int)) );

        connect( RCombox::_popupView, SIGNAL(sig_value_changed(int)),
                 this, SLOT(on_apply_value(int)) );

        connect( RCombox::_popupView, SIGNAL(sig_hide()),
                 this, SLOT(on_popupview_hide()) );

        connect( RCombox::_popupView,
                 SIGNAL(sig_pressed(QPoint)),
                 this,
                 SLOT(on_press(QPoint)) );
        setDown(true);

        //! 查找控件对应的位置--弹出窗口中需要根据位置确定键盘响应
        linkId = RMenu::findId( this );
        Q_ASSERT( linkId != -1 );
        RCombox::_popupView->setCommonStyle( &_style.mCommon );
        RCombox::_popupView->setLinkId( linkId, m_pResMenuItem->mId );
        RCombox::_popupView->show( this );

        RServMenu::attachServiceWidget( m_pResMenuItem->mId,
                                        RCombox::_popupView );

        /*! [dba: 2018-06-05] 添加参数0x0A, 选中的同时应用当前值，解决多源触发的问题*/
        //qDebug()<<__FILE__<<__LINE__<<mUiPara2;
        if(0x0A == mUiPara2)
        {
           doCmd( m_pResMenuItem->mIntVal );
        }
    }
}

void RCombox::tuneInc( int cnt )
{
    int now = m_pResMenuItem->mIntVal;

    RPopupList validList;

    collectValidList( validList );
    if ( validList.size() < 1 ) return;

    int index = valueToIndex( now, &validList );

    ui_attr::PackInt packInt;
    packInt.set( index );

    //! key proc
    RControl::keyIncDecProc( packInt, true, cnt );
    index = packInt.val32;

    if ( index < 0 ) index = 0;
    else if ( index >= validList.size() )
    { index = validList.size()-1; }
    else
    {}
    now = indexToValue( index, &validList );

    doCmd( now );
}
void RCombox::tuneDec( int cnt )
{
    int now = m_pResMenuItem->mIntVal;

    RPopupList validList;

    collectValidList( validList );
    if ( validList.size() < 1 ) return;

    int index = valueToIndex( now, &validList );

    ui_attr::PackInt packInt;
    packInt.set( index );

    //! key proc
    RControl::keyIncDecProc( packInt, false, cnt );
    index = packInt.val32;

    if ( index < 0 ) index = 0;
    else if ( index >= validList.size() )
    { index = validList.size()-1; }
    else
    {}

    now = indexToValue( index, &validList );

    doCmd( now );
}

void RCombox::zEnter()
{
    doZCmd();
}

void RCombox::stepNext()
{
    //! current
    int val = m_pResMenuItem->mIntVal;

    //! scan valid items
    RPopupList validItems;
    Q_ASSERT( NULL != m_pPopupItemList );
    foreach( RPopListItem  * pItem, *m_pPopupItemList )
    {
        Q_ASSERT( NULL != pItem );

        if ( pItem->getVisible() && pItem->getEnable() )
        { validItems.append( pItem ); }
    }

    //! no item
    if ( validItems.size() < 1 )
    { return; }

    //! find the item
    int indexNow = -1;
    for ( int i = 0; i < validItems.size(); i++ )
    {
        if ( validItems[i]->getValue() == val )
        {
            indexNow = i;
            break;
        }
    }

    if ( indexNow == -1 )
    {
        qWarning()<<"!!!Invalid value";
        return;
    }

    //! next value
    int next = (indexNow + 1)%validItems.size();

    //! do it
    doCmd( validItems[next]->getValue() );
}

void RCombox::on_select_changed( int val )
{
    if ( mComboxMode != RCombox_ack )
    {
        doCmd( val );
    }
}

void RCombox::on_apply_value( int val )
{
    doCmd( val );
}

void RCombox::on_popupview_hide()
{
    setDown(false);

    toState( combox_selected );
}

void RCombox::on_press(QPoint p)
{
    if( getControl() == ComboxKnob)
    {
        QRect r = this->geometry();
        int w = r.width();
        int h = r.height();
        r.setLeft(860 + r.x());
        r.setTop(56 + r.y());
        r.setWidth(w);
        r.setHeight(h);

        if( r.contains(p,true) )
        {
            QTimer::singleShot(100,this, SLOT(on_clicked()) );
        }
    }
}

void RCombox::paintBg( QPainter &painter )
{
    //! background
    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }

    _style.paintEvent( painter );
}
void RCombox::paintCaption( QPainter &painter )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );

    eMenuRes resType;
    resType = m_pResMenuItem->m_pResItem->getType();

    QImage img;
    QRect boundRect;

    //! -- caption
    boundRect = _style.mCaptionRect;
    //! title image
    if ( is_attr( resType, MR_Icon ) )
    {
        int v = m_pResMenuItem->m_pResItem->getImage( img );
        Q_ASSERT( v == 0 );

        _style.drawIcon( painter,
                         img,
                         boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }

    //! draw caption
    if ( is_attr( resType, MR_Str ) )
    {
        _style.drawCaption( painter,
                         boundRect,
                         m_pResMenuItem->getTitle() );
    }
}
void RCombox::paintOption( QPainter &painter )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );

    eMenuRes resType;
    QImage img;

    //! -- option
    resType = m_pResMenuItem->m_pResItem->getOptionType( m_pResMenuItem->mIntVal );

    //! draw icon
    if ( is_attr( resType, MR_Icon ) )
    {
        int v = m_pResMenuItem->m_pResItem->getOptionImpage( m_pResMenuItem->mIntVal,
                                                             img );
        if ( v != 0 )
        {
            LOG_DBG()<<m_pResMenuItem->m_pResItem->getId()<<m_pResMenuItem->mIntVal;
            Q_ASSERT(false);
        }
        _style.drawIcon( painter,
                         img,
                         _style.mTextRect,
                         resType
                        );

    }

    //! draw text
    if ( is_attr( resType, MR_Str ) )
    {
        QString str;
        str = m_pResMenuItem->m_pResItem->getOptionString( m_pResMenuItem->mIntVal );

        QColor fgColor;
        if ( m_pResMenuItem->m_pResItem->getOptionColor( m_pResMenuItem->mIntVal, fgColor ) )
        {
            _style.drawText( painter,
                             fgColor,
                             _style.mTextRect,
                             str );
        }
        else
        {
            _style.drawText( painter,
                             _style.mTextRect,
                             str );
        }
    }
}

void RCombox::paintCaptionOnOption( QPainter &painter )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );

    eMenuRes resType;
    QImage img;

    //! -- caption type
    resType = m_pResMenuItem->m_pResItem->getType();

    //! draw icon
    if ( is_attr( resType, MR_Icon ) )
    {
        int v = m_pResMenuItem->m_pResItem->getImage( img );
        Q_ASSERT ( v == 0 );
        _style.drawIcon( painter,
                         img,
                         _style.mTextRect,
                         resType
                        );

    }

    //! draw text
    if ( is_attr( resType, MR_Str ) )
    {
        QString str;
        str = m_pResMenuItem->getTitle();

        QColor fgColor;
        //! user color
        if ( getUiFgColor(fgColor) )
        {
            _style.drawText( painter,
                             fgColor,
                             _style.mTextRect,
                             str );
        }
        //! user color
        else if ( is_attr(mUiPara1, 0x200) )
        {
            _style.drawText( painter,
                             QColor( mUiPara2),
                             _style.mTextRect,
                             str );
        }
        //! default color
        else
        {
            _style.drawText( painter,
                             _style.mTextRect,
                             str );
        }
    }
}

void RCombox::pwrOp( RComboxPwrOp op )
{
    //! to low power
    if ( op == Pwr_low )
    {
        mPwr = mLowPwr;
    }
    else if ( op == Pwr_next )
    {
        //! rst pre
        if ( RCombox::_focusCombox != NULL
             && RCombox::_focusCombox !=this )
        {
            RCombox::_focusCombox->pwrOp( Pwr_low );
        }

        //! wake up
        if ( mPwr == RCombox_sleep )
        {
            mPwr = RCombox_idle;
            RCombox::_focusCombox = this;
        }
        else if ( mPwr == RCombox_idle )
        {
            RCombox::_focusCombox = this;
        }
        else
        {
            Q_ASSERT( false );
        }
    }
    else
    {
        Q_ASSERT(false);
    }
}

bool RCombox::pwrEventFilter()
{
    if ( mPwr == RCombox_sleep )
    {
        pwrOp( Pwr_next );
        focusIn();

        return true;
    }
    else if ( mPwr == RCombox_idle )
    {
        pwrOp( Pwr_next );
        focusIn();

        return false;
    }
    else
    {
        Q_ASSERT( false );
        return false;
    }
}

void RCombox::setTuneView( RPopupView * pPopView )
{
    Q_ASSERT( NULL != pPopView );

    if ( RCombox_norm == mComboxMode )
    {   pPopView->setKnobView( RPopupView::knob_tune); }
    else if ( RCombox_ack == mComboxMode )
    {   pPopView->setKnobView( RPopupView::knob_tune_z); }
    else if ( RCombox_check == mComboxMode )
    {   pPopView->setKnobView( RPopupView::knob_tune_z); }
    else
    {   pPopView->setKnobView( RPopupView::knob_none); }
}

void RCombox::collectValidList( RPopupList &validList )
{
    Q_ASSERT( NULL != m_pPopupItemList );

    foreach( RPopListItem *pItem, *m_pPopupItemList )
    {
        Q_ASSERT( NULL != pItem );

        if ( pItem->getEnable() && pItem->getVisible() )
        {
            validList.append( pItem );
        }
    }
}

int RCombox::valueToIndex( int val, RPopupList *pList )
{
    RPopListItem *pItem;

    Q_ASSERT( NULL != pList );

    foreach( pItem, *pList )
    {
        Q_ASSERT( NULL != pItem );
        if ( pItem->m_pResItem->value == val )
        { return val; }
    }

    Q_ASSERT( false );
    return 0;
}
int RCombox::indexToValue( int index, RPopupList *pList )
{
    Q_ASSERT( NULL != pList );
    Q_ASSERT( index >= 0 && index < pList->size() );

    RPopListItem *pItem;

    pItem = pList->at(index);
    Q_ASSERT( NULL != pItem );
    Q_ASSERT( pItem->m_pResItem );
    return pItem->m_pResItem->value;

    return 0;
}

void RCombox::updateEnable()
{
    bool bEn = isEnabled();

    setEnabled( sumVisible() && sumEnable() );

    if ( bEn != isEnabled() )
    { update(); }

    if ( bEn && bEn != isEnabled() )
    { closePop(); }
}

void RCombox::closePop()
{
    Q_ASSERT ( NULL != m_pResMenuItem );

    //! disabled
    if ( NULL == RCombox::_popupView )
    { return; }

    if ( !RCombox::_popupView->isVisible() )
    { return; }

    //! msg matched
    if ( RCombox::_popupView->getLinkMsg() == m_pResMenuItem->mId )
    {
        RCombox::_popupView->hide();
        return;
    }
}

/*!
 * \brief RCombox::setResItem
 * \param pItem
 * 设置控件上挂载的资源数据
 */
void RCombox::setResItem( CResMenuItem *pItem )
{
    //! base call
    RControl::setResItem( pItem );

    RPopupList *pPopupList;

    //! find the item in cache
    pPopupList = RCombox::findPopupList( pItem->mId );
    if ( pPopupList != NULL )
    {
        m_pPopupItemList = pPopupList;

        return;
    }
    else
    {
        m_pPopupItemList = new RPopupList;
        Q_ASSERT( m_pPopupItemList != NULL );
        mbOwner = true;

        //! add to cache
        RCombox::insertPopupList( pItem->mId, m_pPopupItemList );

        CResItem *pOptItem;
        RPopListItem *pPopItemItem;
        bool bCheckable;

        if ( mComboxMode == RCombox_check )
        {
            bCheckable = true;
        }
        else
        {
            bCheckable = false;
        }

        //! insert option
        foreach( pOptItem, pItem->m_pResItem->mOption )
        {
            pPopItemItem = new RPopListItem();
            Q_ASSERT( NULL != pPopItemItem );

            pPopItemItem->setCheckable( bCheckable );
            pPopItemItem->setResItem( pOptItem );

            m_pPopupItemList->append( pPopItemItem );
        }
    }
}

void RCombox::setMode( RComboxMode mode )
{
    mComboxMode = mode;
}
RCombox::RComboxMode RCombox::getMode()
{
    return mComboxMode;
}

void RCombox::setLowerPwr( RComboxPwr pwr )
{
    mLowPwr = pwr;
    mPwr = mLowPwr;
}

void RCombox::setNodeCompress( bool bCompress )
{
    mbNodeCompress = bCompress;
}

void RCombox::setAttr( int attr )
{
    mAttr = attr;

    if ( mAttr & (1<<combox_pwr) )
    {
        setLowerPwr( RCombox_sleep );
    }

    if ( mAttr & (1<<combox_compress) )
    {
        setNodeCompress( true );
    }
}

RPopListItem *RCombox::findOption( int opt )
{
    RPopListItem *pItem;

    Q_ASSERT( m_pPopupItemList != NULL );
    foreach( pItem, *m_pPopupItemList )
    {
        Q_ASSERT( pItem != NULL );
        if ( pItem->m_pResItem->value == opt )
        {
            return pItem;
        }
    }

    return NULL;
}

bool RCombox::sumVisible()
{
    Q_ASSERT( m_pPopupItemList != NULL );
    foreach( RPopListItem *pItem, *m_pPopupItemList )
    {
        Q_ASSERT( pItem != NULL );
        if ( pItem->getVisible() )
        {
            return true;
        }
    }

    return false;
}
bool RCombox::sumEnable()
{
    Q_ASSERT( m_pPopupItemList != NULL );
    foreach( RPopListItem *pItem, *m_pPopupItemList )
    {
        Q_ASSERT( pItem != NULL );
        if ( pItem->getEnable() )
        {
            return true;
        }
    }

    return false;
}

void RCombox::toState( RComboxState stat )
{
    mComboxState = stat;
}

}

