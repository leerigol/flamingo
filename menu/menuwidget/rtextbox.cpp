#include "../../com/virtualkb/virtualkb.h"

#include "rtextbox.h"

namespace menu_res
{

RTextBox_Style RTextBox::_style;

RTextBox::RTextBox( QWidget *parent ) : RMenuWidget( parent )
{
//    m_pTextEdit = new QLineEdit( this );
//    Q_ASSERT( m_pTextEdit != NULL );

    _style.loadResource("ui/menu/textedit/");
    _style.resize( this );

    //! apply style
//    m_pTextEdit->setStyleSheet( _style.mTextEditQss );
//    m_pTextEdit->setAlignment( Qt::AlignRight );
//    m_pTextEdit->setGeometry( _style.mTextRect );

//    m_pTextEdit->setReadOnly(true);
//    QPalette palette=this->palette();
//    palette.setColor(QPalette::Disabled,QPalette::Text,Qt::red);
//    this->setPalette(palette);

    mShowText = "";
}

void RTextBox::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);
    eMenuRes resType;
    QImage img;
    QRect boundRect;

    if ( !isEnabled() )
    {
        _style.setStatus( element_disabled );
    }
    else if ( isDown() )
    {
        _style.setStatus( element_actived_down );
    }
    else
    {
        _style.setStatus( element_deactived );
    }
    _style.paintEvent( painter );

    Q_ASSERT( NULL != m_pResMenuItem );
    Q_ASSERT( NULL != m_pResMenuItem->m_pResItem );

    //! title
    resType = m_pResMenuItem->m_pResItem->getType();
    boundRect = _style.mCaptionRect;
    if ( resType & MR_Icon )
    {
        int v = m_pResMenuItem->m_pResItem->getImage( img );
        Q_ASSERT( 0 == v );

        _style.drawIcon( painter, img, boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }    

    if ( resType & MR_Str)
    {
        _style.drawCaption( painter,
                            boundRect,
                            m_pResMenuItem->getTitle() );
    }

    _style.drawText1(painter, _style.mTextRect, mShowText );
    //! text-now
}

bool RTextBox::eventFilter(QObject */*obj*/, QEvent */*event*/)
{
    //qDebug() << obj << event;
    return false;
}

void RTextBox::on_enable_changed(int msg, bool b)
{
    if ( !cmdMatch(msg) ) return;

    setEnabled(b);

//    qDebug() << "color= " << _style.mStatus << b;
//    if( m_pTextEdit )
//    {
//        m_pTextEdit->setEnabled(b);
//    }
}

void RTextBox::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    queryCmd( mText, combox_raw );

    QString temp = mText;
    //by hxh. not trim
    trimText( temp );

    mShowText = temp;
    //m_pTextEdit->setText( temp );

    update();
}

void RTextBox::onClick()
{
    //! 最大长度会恢复默认
    VirtualKB::Show( this,
                      SLOT(on_ime_completed(QString)),
                      m_pResMenuItem->getTitle(),
                      mText,
                      _style.mShfitPoint );

    attachPageWidget( VirtualKB::object() );

    //! 设置对应按键的目的是，窗口需要区别对待按键
    int id, linkKey;
    id = RMenu::findId( this );
    Q_ASSERT( id != -1 );
    linkKey = REventFilter::menuIndexToKey( id );
    Q_ASSERT( linkKey != -1 );
    VirtualKB::object()->setLinkKey( linkKey );

    //! 设置最大长度
    int maxLen = maxLength();
    if ( maxLen > 0 )
    { VirtualKB::object()->setMaxLength( maxLen ); }
}

/*!
 * \brief RTextBox::trimText
 * 限定长度,如果超过设定的最大长度则保留前面的部分
 */
void RTextBox::trimText( QString &str )
{
    int maxLen = sysGetNormalSpace(str);
    if( maxLen > 14 )
    {
        str = str.left(11)+ "...";
    }

//    maxLen = maxLength();
//    if ( maxLen <= 0 ) return;

//    if ( str.length() > maxLen )
//    {
//        str = str.left(maxLen);
//    }
}

int RTextBox::maxLength()
{
    //! ui attr
    int uiMax;
    uiMax = getUiMaxLength();
    if ( uiMax > 0 )
    { return uiMax; }

    //! 静态值
//    Q_ASSERT( m_pResMenuItem != NULL );
//    if ( m_pResMenuItem->mPara1 > 0 )
//    { return m_pResMenuItem->mPara1; }
    if ( mUiPara1 > 0 )
    { return mUiPara1; }

    return -1;
}
/*!
 * \brief RTextBox::on_ime_completed
 * \param str
 * 从ime设置过来的字符，一般在ime点击确定后发生
 */
void RTextBox::on_ime_completed( const QString &str )
{
    //m_pTextEdit->setText(str);
    mText = str;
    mShowText = str;

    doCmd( mText );
}

}

