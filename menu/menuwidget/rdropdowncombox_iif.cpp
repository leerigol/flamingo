#include "rdropdowncombox_iif.h"

namespace menu_res{

RDropDownCombox_iif::RDropDownCombox_iif(QString txt,
                                         QWidget *parent ,
                                         QWidget *popParent)
                    : RCombox_iif( txt, parent, popParent )
{
    //! dropdownlist 使用的值消息和选项消息相同，都是指的数值
    mValueSubCmd = combox_raw;
}

void RDropDownCombox_iif::paintFrame( QPainter &painter )
{
    //! back ground
    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }

    //! frame
    _style.paintEvent( painter );

    //! knob
    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), isTuneNow() );
    m_pKnob->setState( tuneStat );

    //! option type - 只能是数值

    //! draw caption
    QString str;
    QRect boundRect;
    boundRect = _style.mCaptionRect;
    str = m_pResMenuItem->getTitle();
    _style.drawCaption(
                     painter,
                     boundRect,
                     str );
}

void RDropDownCombox_iif::on_value_changed( int /*msg*/ )
{
    Q_ASSERT( m_pResMenuItem != NULL );

    if ( mContPara != para_val )
    { return; }

    //! 更新数值
    queryCmd( mVal, mValueSubCmd );
    setControlValue( mVal.val32 );

    update();

}

}
