#ifndef RPUSHBUTTON
#define RPUSHBUTTON

#include "../menuwidget/rcontrol.h"

#include "../menustyle/rcontrolstyle.h"

#include "rmenuwidget.h"

namespace menu_res {

/*!
 * \brief The RPushButton class
 * 按键
 */
class RPushButton : public RMenuWidget
{
    Q_OBJECT

protected:
    static RPushButtonStyle _style;

public:
    RPushButton( QString txt="", QWidget *parent=0 );

    void setValue( int value );
    int getValue();
protected:
    int mPushVal;   /*< 按键按下的数值 */

protected:
    virtual void paintEvent( QPaintEvent *event );

protected:
    virtual void onClick();

};

}

#endif // RPUSHBUTTON

