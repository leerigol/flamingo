#ifndef RLITECOMBOX_H
#define RLITECOMBOX_H

#include <QtWidgets>

#include "rmenuwidget.h"

namespace menu_res {

typedef QPair<int, bool> ROptPair;

class RLiteCombox : public RMenuWidget
{
    Q_OBJECT
public:
    RLiteCombox( QWidget *parent = 0 );
    ~RLiteCombox();
public:
    virtual void on_enable_changed( int msg, int opt, bool b );
    virtual void on_value_changed( int val );
protected:
    virtual void onClick();
    virtual void stepNext();

public:
    void addOption( int opt );

private:
    ROptPair *findOpt( int opt );
    int findIndex( int opt, QList<ROptPair*> &list );

    QList<ROptPair*> validItems();
private:
    QList< ROptPair* > mOpts;
};

}

#endif // RLITECOMBOX_H
