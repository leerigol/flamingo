#include "rcomboxcheck.h"
#include "rpopupview.h"
#include "rpopupitem.h"

#include "../arch/rmenu.h"

namespace menu_res
{

RCombox_Check_Style RComboxCheck::_style;

RComboxCheck::RComboxCheck( QString txt,
         QWidget *parent,
         QWidget *popParent
         ) : RCombox( txt, parent, popParent )
{
    _style.loadResource("ui/menu/combox_check/");
}

void RComboxCheck::on_value_changed( int msg )
{
    if ( !cmdMatch(msg) ) return;

    queryCmd( mbmValue );

    updateOptionBm( mbmValue );

    //! update view
    if ( NULL != RCombox::_popupView
         && RCombox::_popupView->isVisible() )
    {
        RCombox::_popupView->update();
    }
}

void RComboxCheck::on_checked_changed( int val )
{
    int bm;

    queryCmd( mbmValue );

    bm = getOptionBm();

    //! bm changed
    if ( bm != mbmValue )
    {
        doCmd( bm );
    }

    mModelCurVal = val;
}

//! do not show value
void RComboxCheck::paintEvent( QPaintEvent */*event*/ )
{
    RPainter painter(this);

    paintBg( painter );

    paintCaption( painter );

    //! no option show
}

void RComboxCheck::onClick()
{
    //! has not created
    if ( NULL == RCombox::_popupView )
    {
        RCombox::_popupView = new RPopupView( RCombox::_popupParent );
        Q_ASSERT( NULL != RCombox::_popupView );
    }

    if ( RCombox::_popupView->isVisible() )
    {
        RCombox::_popupView->hide();
        RCombox::_popupView->disconnect();
    }
    else
    {
        //! set selected
        RPopListModal modal;

        modal.setTitle( m_pResMenuItem->getTitle() );

        modal.setList( m_pPopupItemList );
        modal.setCurrent( 0 );

        RCombox::_popupView->setModal( modal );
        setTuneView( RCombox::_popupView );

        RCombox::_popupView->disconnect();

        connect( RCombox::_popupView, SIGNAL(sig_value_changed(int)),
                 this, SLOT(on_checked_changed(int)) );

        //! current status
        queryCmd( mbmValue );
        updateOptionBm( mbmValue );

        int linkId;
        linkId = RMenu::findId( this );
        Q_ASSERT( linkId != -1 );
        RCombox::_popupView->setCommonStyle( &_style.mCommon );
        RCombox::_popupView->setLinkId( linkId, m_pResMenuItem->mId );
        RCombox::_popupView->show( this );

        RServMenu::attachServiceWidget( m_pResMenuItem->mId,
                                        RCombox::_popupView );
    }
}

void RComboxCheck::paintCaption( QPainter &painter )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );

    eMenuRes resType;
    resType = m_pResMenuItem->m_pResItem->getType();

    QImage img;
    QRect boundRect;

    //! -- caption on content rect
    boundRect = RCombox::_style.mTextRect;
    //! title image
    if ( is_attr( resType, MR_Icon ) )
    {
        int v = m_pResMenuItem->m_pResItem->getImage( img );
        Q_ASSERT( v == 0 );

        RCombox::_style.drawIcon( painter,
                         img,
                         boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }

    //! draw caption on text rect
    if ( is_attr( resType, MR_Str ) )
    {
        QString str = m_pResMenuItem->getTitle();

        QColor fgColor;
        //! user color
        if ( getUiFgColor(fgColor) )
        {
            RCombox::_style.drawText( painter,
                             fgColor,
                             boundRect,
                             str );
        }
        //! fg color
        else if ( m_pResMenuItem->m_pResItem->getColor(fgColor) )
        {
            RCombox::_style.drawText( painter,
                             fgColor,
                             boundRect,
                             str );
        }
        else
        {
            RCombox::_style.drawText( painter,
                             boundRect,
                             str );
        }
    }
}

int RComboxCheck::getOptionBm()
{
    Q_ASSERT( NULL != m_pPopupItemList );

    RPopListItem *pItem;
    int bm = 0;
    foreach( pItem, *m_pPopupItemList )
    {
        Q_ASSERT( pItem != NULL );

        if ( pItem->getCheckable() && pItem->getChecked() )
        {
            bm |= 1 << ( pItem->m_pResItem->value);
        }
    }

    return bm;
}

void RComboxCheck::updateOptionBm( int value )
{
    RPopListItem *pItem;
    int bm = 0;

    Q_ASSERT( NULL != m_pPopupItemList );
    foreach( pItem, *m_pPopupItemList )
    {
        Q_ASSERT( pItem != NULL );

        if ( pItem->getCheckable() )
        {
            bm = 1 << ( pItem->m_pResItem->value);

            if ( (bm & value) == bm )
            {
                pItem->setChecked(true);
            }
            else
            {
                pItem->setChecked(false);
            }
        }
    }
}

}
