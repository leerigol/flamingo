#ifndef RDROPDOWNCOMBOX_IIF_H
#define RDROPDOWNCOMBOX_IIF_H

#include "rcombox_iif.h"

namespace menu_res{

/*!
 * \brief The RDropDownCombox_iif class
 * - 显示列表项目数值，通过旋钮更改数值
 * - 列表中的项目相当于预设数值
 */
class RDropDownCombox_iif : public RCombox_iif
{
public:
    RDropDownCombox_iif( QString txt="",
                         QWidget *parent = 0,
                         QWidget *popParent = 0);

protected:
    virtual void paintFrame( QPainter &painter );

public:
    virtual void on_value_changed( int msg );
};

}

#endif // RDROPDOWNCOMBOX_IIF_H
