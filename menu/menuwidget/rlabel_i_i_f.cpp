#include "../arch/rmenu.h"
#include "rlabel_i_i_f.h"
#include "../../arith/ln2.h"

namespace menu_res {

RLabel_i_i_f_Style RLabel_i_i_f::_style;

RLabel_i_i_f::RLabel_i_i_f( QString /*txt*/,
           QWidget *parent ) : RMenuWidget( parent )
{
    _style.loadResource("ui/menu/label_iif/");
    _style.resize( this );

    m_pKnob = new RFuncKnob(this);
    Q_ASSERT( NULL != m_pKnob );

    m_pKnob->move( _style.mKnobXY );

    mContFmt = control_fmt_fract;

    mbTuneAble = true;
}

void RLabel_i_i_f::setTextVisible( bool b )
{
    RControl::setTextVisible( b );

    if ( b )
    {
        m_pKnob->move( _style.mKnobXY );
    }
    else
    {
        m_pKnob->move( _style.mKnobKeyXY );
    }
}

void RLabel_i_i_f::setKnobView( eControlKnob view )
{
    Q_ASSERT( NULL != m_pKnob );

    m_pKnob->setView( view );
}

void RLabel_i_i_f::setFmt( eControlFmt fmt )
{
    mContFmt = fmt;
}
eControlFmt RLabel_i_i_f::getFmt()
{
    return mContFmt;
}

void RLabel_i_i_f::onInput( DsoReal real )
{
    DsoReal realBase;

    realBase = getUiRealBase( subValueAttr() );

    if ( mVal.width == 32 )
    {
        real.alignBase( realBase );
        doCmd( (int)real.mA, subValueAttr() );
    }
    else if ( mVal.width == 64 )
    {
        real.alignBase( realBase );
        doCmd( real.mA, subValueAttr() );
    }
    else if ( mVal.width == 128 )
    {
        doCmd( real );
    }
    else
    {
        Q_ASSERT( false );
    }
}

void RLabel_i_i_f::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );
    RPainter painter(this);

    bool bSelected = isTuneNow();
    //! bg
    if ( !isEnabled() )
    {
        _style.setStatus( element_disabled );
    }
    else if ( isDown() || bSelected)
    {
        _style.setStatus( element_actived_down );
    }
    else
    {
        _style.setStatus( element_deactived );
    }

    //! hide knob
    if ( mbReadOnly )
    {
        m_pKnob->setVisible( false );
        _style.setStatus( element_disabled );
    }

    //! icon
    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), bSelected );

    m_pKnob->setState( tuneStat );

    _style.paintEvent( painter, tuneStat );

    //! -- caption
    QImage img;
    eMenuRes resType;
    QRect boundRect;

    Q_ASSERT( NULL != m_pResMenuItem );
    Q_ASSERT( NULL != m_pResMenuItem->m_pResItem );

    resType = m_pResMenuItem->m_pResItem->getType();
    boundRect = _style.mCaptionRect;
    if ( is_attr(resType, MR_Icon ) )
    {
        int v = m_pResMenuItem->m_pResItem->getImage( img );
        Q_ASSERT( 0 == v );

        _style.drawIcon( painter, img, boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }

    if ( is_attr(resType, MR_Str ) )
    {
        _style.drawCaption( painter,
                            boundRect,
                            m_pResMenuItem->getTitle());
    }

    //! -- text
    if ( mbText )
    {
        QString str;
        bool bUiStr;

        //! ui out string
        str = getUiOutStr( bUiStr, subValueAttr() );
        if ( !bUiStr )
        {
            formatValue( mVal, mContFmt,
                         getUiRealBase( subValueAttr() ),
                         str,
                         getUiViewFmt(subValueAttr()) );

            str = getUiPreStr( subValueAttr() )
                  + str
                  + getUiPostStr( subValueAttr() );
        }

        //! text
        _style.drawText( painter,
                         _style.mTextRect,
                         str);
    }
}

void RLabel_i_i_f::wheelEvent( QWheelEvent *event )
{
    QPoint numPixels = event->pixelDelta();
    QPoint numDegrees = event->angleDelta() / 8;

     if (!numPixels.isNull())
     {
        event->ignore();
     }
     else if (!numDegrees.isNull())
     {
         QPoint numSteps = numDegrees / 15;

         //! tune proc
         if ( numSteps.y() > 0 )
         {
             tuneInc( numSteps.y() );
         }
         else if ( numSteps.y() < 0 )
         {
             tuneDec( -numSteps.y() );
         }
         else
         {}

         event->accept();
     }
     else
     {}

}

void RLabel_i_i_f::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    queryCmd( mVal, subValueAttr() );
    update();
}

void RLabel_i_i_f::tuneInc( int keyCnt )
{
    Q_ASSERT( NULL != m_pKnob );

    if ( m_pKnob->knobEventFilter( RFuncKnob::knob_inc) )
    { return; }

    QKeyEvent event( QEvent::KeyRelease,
                        R_Skey_TuneInc,
                        Qt::NoModifier,
                        "",
                        false,
                        keyCnt );

    //! 键处理
    if ( mContPara == para_key )
    {
        paraKeyProc( &event );
    }
    //! 数值处理
    else if ( mContPara == para_val  )
    {
        paraValProc( &event );
    }
    else
    {}
}
void RLabel_i_i_f::tuneDec( int keyCnt )
{
    if ( m_pKnob->knobEventFilter( RFuncKnob::knob_dec) )
    { return; }

    QKeyEvent event( QEvent::KeyRelease,
                        R_Skey_TuneDec,
                        Qt::NoModifier,
                        "",
                        false,
                        keyCnt );

    //! 键处理
    if ( mContPara == para_key )
    {
        paraKeyProc( &event );
    }
    //! 数值处理
    else if ( mContPara == para_val  )
    {
        paraValProc( &event );
    }
    else
    {}
}

void RLabel_i_i_f::tuneClick()
{
    eControlKnob knobView;

    Q_ASSERT( NULL != m_pKnob );
    knobView = m_pKnob->getView();
    if ( knobView == control_knob_tune_pad )
    {
        keyPadProc();
        return;
    }

    if ( knobView == control_knob_pad )
    {
        keyPadProc();
        return;
    }

    zEnter();
}

void RLabel_i_i_f::onClick()
{
    //! base
    RMenuWidget::onClick();

    eControlKnob knobView;

    Q_ASSERT( NULL != m_pKnob );
    knobView = m_pKnob->getView();
    if ( knobView == control_knob_tune_pad )
    {
        keyPadProc();
        return;
    }

    if ( knobView == control_knob_pad )
    {
        keyPadProc();
        return;
    }
}

bool RLabel_i_i_f::cmdMatch( int msg )
{
    Q_ASSERT( m_pResMenuItem != NULL );

    if ( mContPara == para_val )
    {
        if ( msg != m_pResMenuItem->mId )
        { return false; }
        else
        { return true; }
    }
    else
    {
        if ( msg == m_pResMenuItem->mId ||
             msg == m_pResMenuItem->mId + (combox_value - combox_raw) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

void RLabel_i_i_f::keyPadProc( )
{
    CKeyPadContext keyPadContext;

    buildKeypadContext( keyPadContext, mVal, subValueAttr() );

    QPoint pt = mapToGlobal( QPoint(0,0) );

    InputKeypad::rst();

    connect( InputKeypad::getInstance(), SIGNAL(OnOK(DsoReal)),
             this, SLOT(onInput(DsoReal)) );

    attachPageWidget( InputKeypad::getInstance() );
    //! config link key
    int id, linkKey;
    id = RMenu::findId( this );
    Q_ASSERT( id != -1 );
    linkKey = REventFilter::menuIndexToKey( id );
    Q_ASSERT( linkKey != -1 );
    InputKeypad::getInstance()->setLinkKey( linkKey );

    InputKeypad::getInstance()->input( pt.x(), pt.y(),
                                       keyPadContext.mView,
                                       keyPadContext.mValueGp );
}

void RLabel_i_i_f::key125Proc( int key,
                               int accCount,
                               ui_attr::PackInt &val )
{
    int index;

    //! 32 bit
    if ( val.width == 32 )
    {
        if ( key == R_Skey_TuneInc )
        {
            index = key_acc::CKeyAcc::to125Index( val.val32, 1 );

            index += accCount;

            key_acc::CKeyAcc::toValue( index, val.val32 );

            doCmd( val );
        }
        else if ( key == R_Skey_TuneDec )
        {
            index = key_acc::CKeyAcc::to125Index( val.val32, -1 );

            index -= accCount;

            key_acc::CKeyAcc::toValue( index, val.val32 );

            doCmd( val );
        }
        else if ( key == R_Skey_Zval )
        {
            ui_attr::PackInt zVal;
            zVal = RControl::getUiZVal();
            doCmd( zVal );
        }
        else
        {}
    }
    else
    {
        if ( key == R_Skey_TuneInc )
        {
            index = key_acc::CKeyAcc::to125Index( val.val64, 1 );

            index += accCount;

            key_acc::CKeyAcc::toValue( index, val.val64 );

            doCmd( val );
        }
        else if ( key == R_Skey_TuneDec)
        {
            index = key_acc::CKeyAcc::to125Index( val.val64, -1 );

            index -= accCount;

            key_acc::CKeyAcc::toValue( index, val.val64 );

            doCmd( val );
        }
        else if ( key == R_Skey_Zval )
        {
            ui_attr::PackInt zVal;
            zVal = RControl::getUiZVal();
            doCmd( zVal );
        }
        else
        {}
    }
}

void RLabel_i_i_f::keyStepProc( int key,
                                int keyAccCnt,
                                qlonglong step,
                                ui_attr::PackInt &val )
{
    ui_attr::PackInt valPre;

    valPre = val;
    if ( key == R_Skey_TuneInc )
    {
        if ( uiEventFilter() )
        { return; }

        val = val + keyAccCnt * step;

        stickProc( val, valPre );

        doCmd( val );
    }
    else if ( key == R_Skey_TuneDec )
    {
        if ( uiEventFilter() )
        { return; }

        val = val - keyAccCnt * step;

        stickProc( val, valPre );

        doCmd( val );
    }
    else if ( key == R_Skey_Zval )
    {
        val = RControl::getUiZVal();

        doCmd( val );         /*!< z proc */
    }
    else
    {
        return;
    }
}

void RLabel_i_i_f::keyPower2Proc(
                    int key,
                    int keyAccCnt,
                    qlonglong step,
                    ui_attr::PackInt &val )
{
    ui_attr::PackInt valPre;

    //! pow(2,x) == val
    int scale2;
    if ( val.width == 32 )
    {
        scale2 = ln2( val.val32 );
    }
    else if ( val.width == 64 )
    {
        scale2 = ln2( val.val64 );
    }
    else
    { return; }


    valPre = val;
    if ( key == R_Skey_TuneInc )
    {
        if ( uiEventFilter() )
        { return; }

        scale2 = scale2 + keyAccCnt * step;

        val.assignPower2( scale2 );

        stickProc( val, valPre );

        doCmd( val );
    }
    else if ( key == R_Skey_TuneDec )
    {
        if ( uiEventFilter() )
        { return; }

        scale2 = scale2 - keyAccCnt * step;

        val.assignPower2( scale2 );

        stickProc( val, valPre );

        doCmd( val );
    }
    else if ( key == R_Skey_Zval )
    {
        val = RControl::getUiZVal();

        doCmd( val );         /*!< z proc */
    }
    else
    {
        return;
    }
}

void RLabel_i_i_f::paraKeyProc( QKeyEvent *event )
{
    Q_ASSERT( NULL != event );

    key_acc::KeyAcc acc;
    int keyAccCnt;
    ui_attr::PackInt zVal;

    //! get ui attr
    queryCmd();

    acc = RControl::getUiAcc();
    keyAccCnt = event->count();

    keyAccCnt = key_acc::CKeyAcc::acc( acc, keyAccCnt );

    if ( event->key() == R_Skey_TuneInc )
    {
        doCmd( keyAccCnt );
    }
    else if ( event->key() == R_Skey_TuneDec )
    {
        doCmd( -keyAccCnt );
    }
    else if ( event->key() == R_Skey_TuneEnter )
    {
        zVal = getUiZVal();
        doCmd( zVal );         /*!< z proc */
    }
    else if ( event->key() == R_Skey_Zval )
    {
        zVal = getUiZVal();
        doCmd( zVal );         /*!< z proc */
    }
    else if ( event->key() == R_Skey_Active )
    {
        setDown(false);
    }
    else
    {
        return;
    }
}

void RLabel_i_i_f::paraValProc( QKeyEvent *event )
{
    Q_ASSERT( NULL != event );

    qlonglong step;
    ui_attr::PackInt val;
    int keyAccCnt;

    //! in cache ?
    if ( scanLau( val, subValueAttr() ) )
    {}
    else
    {
        queryCmd( val, subValueAttr() );
    }

    step = RControl::getUiStep( subValueAttr(),
                                event->key() == R_Skey_TuneInc ? 1 : -1 );

    key_acc::KeyAcc acc;
    acc = RControl::getUiAcc( subValueAttr() );

    alignValue( val, step, event->key() == R_Skey_TuneInc);

    keyAccCnt = event->count();
    keyAccCnt = key_acc::CKeyAcc::acc( acc, keyAccCnt );

    if ( key_acc::CKeyAcc::is125Acc(acc) )
    {
       key125Proc(  event->key(), keyAccCnt, val );
    }
    else if ( acc == key_acc::e_key_power_2 )
    {
       keyPower2Proc( event->key(), keyAccCnt, step, val );
    }
    else
    {
       keyStepProc(  event->key(), keyAccCnt, step, val );
    }

}

eComboxSubAttr RLabel_i_i_f::subValueAttr()
{
    if ( mContPara == para_key )
    {
        return combox_value;
    }
    else
    {
        return combox_raw;
    }
}

}

