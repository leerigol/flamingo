#ifndef RIPEDIT_H
#define RIPEDIT_H

#include "../menuwidget/rcontrol.h"
#include "../menustyle/rcontrolstyle.h"

#include "rtimeedit.h"
#include "../../widget/rfuncknob.h"
namespace menu_res {

class RIpEdit : public RSegEdit
{
    Q_OBJECT
private:
    static RIpEditStyle _style;

public:
    RIpEdit( QWidget *parent = NULL );

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    virtual void on_value_changed( int msg );
protected:
    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );

    void focusEdit( int keyCnt, bool bInc );

public:
    void setIp( quint32 ip );
    quint32 getIp();

    void setIp0( int ip );
    int getIp0();

    void setIp1( int ip );
    int getIp1();

    void setIp2( int ip );
    int getIp2();

    void setIp3( int ip );
    int getIp3();

private:
    quint32 mIp;
};

}

#endif // RIPEDIT_H
