#include "rpopupview.h"
#include "rpopupitem.h"

#include "../event/reventfilter.h"

namespace menu_res {

RPopupView_Style RPopupView::_style;

RPopupView::RPopupView( QWidget *parent ) : uiWnd(parent)
{
    _style.loadResource( "ui/menu/popup_view/");

    //! base attr
    setAttr( wnd_disconnect_on_hide );
    set_attr( wndAttr, mWndAttr, wnd_popup );

    //! scroll bar
    m_pScrollBar = new RScrollBar(this);
    Q_ASSERT( NULL != m_pScrollBar );

    m_pCommonStyle = NULL;
    mLinkId = -1;
    setVisible( false );

    mKnobIcon = RPopupView_Style::knob_none;

    //! physical - soft keys
    menuKeys.insert( R_Pkey_F1, 0 );
    menuKeys.insert( R_Pkey_F2, 1 );
    menuKeys.insert( R_Pkey_F3, 2 );
    menuKeys.insert( R_Pkey_F4, 3 );

    menuKeys.insert( R_Pkey_F5, 4 );
    menuKeys.insert( R_Pkey_F6, 5 );
    menuKeys.insert( R_Pkey_F7, 6 );

    //! connection
    connect( m_pScrollBar, SIGNAL(sig_page_changed(int,int)),
             this, SLOT(on_scroll_page_changed(int,int)) );
    connect( m_pScrollBar, SIGNAL(sig_value_changed(int)),
             this, SLOT(on_scroll_value_changed(int)) );

    //! filter keys
    mKeyList.append( R_Pkey_F1 );
    mKeyList.append( R_Pkey_F2 );
    mKeyList.append( R_Pkey_F3 );
    mKeyList.append( R_Pkey_F4 );

    mKeyList.append( R_Pkey_F5 );
    mKeyList.append( R_Pkey_F6 );
    mKeyList.append( R_Pkey_F7 );

    //! phisical key
    mKeyList.append( R_Pkey_FInc );
    mKeyList.append( R_Pkey_FDec );
    mKeyList.append( R_Pkey_FZ );

    mKeyList.append( R_Pkey_PageOff );
    mKeyList.append( R_Pkey_PageReturn );

    mKeyList.append( R_Pkey_PageUp );
    mKeyList.append( R_Pkey_PageDown );

    keyChanged();

    isMoved = false;

}

bool RPopupView::eventFilter(QObject *obj, QEvent *event)
{
    if(this->isVisible() &&
            event->type() == QEvent::TouchBegin )
    {
        QTouchEvent *tEvt = static_cast<QTouchEvent*>(event);
        Q_ASSERT( NULL != tEvt );
        QList<QTouchEvent::TouchPoint> tPoints = tEvt->touchPoints();
        if ( tPoints.size() > 0  )
        {
            QPoint ptScreen;
            QPointF scrPos = tPoints[0].normalizedPos();
            ptScreen.setX( scrPos.x() * 1024 );
            ptScreen.setY( scrPos.y() * 600 );
            emit sig_pressed(ptScreen);
        }
    }
    return uiWnd::eventFilter(obj, event);
}

void RPopupView::doPaint( QPaintEvent *event,
                      QPainter &painter )
{
    Q_ASSERT( event != NULL );

    eMenuRes resType;
    QImage img;
    int now;

    //! get the focus type
    now = m_pScrollBar->getNow();
    resType = mModal.mList[ now ]->m_pResItem->getType();
    _style.paintEvent( painter, m_pScrollBar->getViewId(),
                       now,
                       mModal.size(),
                       mKnobIcon,
                       resType
                       );

    //! draw element
    int  i;
    int from, to;

    QColor color;
    QString str;
    bool bEn;

    m_pScrollBar->getViewRange( from, to );
    for ( i = from; i <= to; i++ )
    {
        //! draw checkIcon
        if ( mModal.mList[i]->getCheckable() )
        {
            _style.drawCheck( painter, mModal.mList[i]->getChecked(), i - from );
        }

        //! draw text
        resType = mModal.mList[i]->m_pResItem->getType();
        if ( is_attr(resType, MR_Icon) )
        {
            //! image fail
            if ( 0 != mModal.mList[i]->m_pResItem->getImage(img) )
            { Q_ASSERT( false ); }

            _style.drawIcon( painter,
                             img,
                             i - from,
                             resType
                             );
        }

        if ( is_attr(resType, MR_Str) )
        {
            bEn = mModal.mList[i]->getEnable();
            str = mModal.mList[i]->m_pResItem->getContentString();

            //! color
            if ( !bEn )
            {
                color = _style.getColor( false );
            }
            //! enable
            else
            {
                do
                {
                    //! by resource setting
                    if ( mModal.mList[i]->m_pResItem->getColor(color) )
                    { break; }

                    //! by style setting
                    if ( NULL != m_pCommonStyle )
                    { color = m_pCommonStyle->getColor( element_actived, str ); }
                    else
                    { color = _style.getColor(true); }

                }while(0);
            }

            painter.setPen( color );

            _style.drawText( painter,
                             str,
                             i - from,
                             to - from,
                             resType
                             );
        }
    }

    //! draw title
    if(getLinkMsg() != MSG_APP_UTILITY_LANGUAGE
            && getLinkMsg() != MSG_APP_HELP_LANGUAGE)
    {
        _style.drawCaption( painter, mModal.mTitle );
    }
    else
    {
        _style.drawCaption( painter,  "Language");
    }
}

void RPopupView::mouseMoveEvent(QMouseEvent *event)
{
    Q_ASSERT( NULL != event );

    if(!isMoved){
        isMoved = true;
        mStartPos = event->pos();
    }else{
        QPoint tempP;
        tempP += event->pos()-mStartPos;
        if(tempP.y()>10 ){
            mStartPos.setY(mStartPos.y()+10);
            m_pScrollBar->moveUp();
        }else if(tempP.y() < -10){
            m_pScrollBar->moveDown();
            mStartPos.setY(mStartPos.y()-10);
        }
        else
        {}
    }
}

void RPopupView::mouseReleaseEvent(QMouseEvent * event)
{
    Q_ASSERT( NULL != event );

    if(!isMoved){
        QPoint pt;

        pt = event->pos();

        int index = indexFromPt( pt );
        if ( index < 0 )
        {
            return;
        }

        Q_ASSERT( index < mModal.mList.size() );

        //! disabled
        if ( !mModal.mList[index]->getEnable() )
        {
            return;
        }

        RPopListItem *pItem;
        pItem = mModal.mList[ index ];
        Q_ASSERT( NULL != pItem );

        //! checkable
        if ( mModal.mList[index]->getCheckable() )
        {
            mModal.mList[index]->setChecked(
                        !mModal.mList[index]->getChecked() );
        }
        else
        {
        }

        emit sig_value_changed( pItem->m_pResItem->value );

        m_pScrollBar->setNow( index );

        if ( mModal.mList[index]->getCheckable() )
        {}
        else
        { hide(); }
    }else{
        isMoved = false;
    }
}

void RPopupView::keyReleaseEvent( QKeyEvent *event )
{
    Q_ASSERT( event != NULL );

    int key;
    key = event->key();

    QList< RPopListItem * > validList;

    collectValidList( validList );
    if ( validList.size() < 1 )
    { return; }

    //! current
    int nowIndex = m_pScrollBar->getNow();
    Q_ASSERT( nowIndex >= 0 && nowIndex < mModal.mList.size() );

    //! value
    int valNow = indexToValue( nowIndex, mModal.mList );
    nowIndex = valueToIndex( valNow, validList );

    //! 对于视图,上面的序号小
    if ( key == R_Pkey_FInc )
    {
        nowIndex -= 1;
        if ( nowIndex < 0 )
        { nowIndex = 0; }

        valNow = indexToValue( nowIndex, validList );
        nowIndex = valueToIndex( valNow, mModal.mList );

        m_pScrollBar->setNow( nowIndex );
    }
    else if ( key == R_Pkey_FDec )
    {
        nowIndex += 1;
        if ( nowIndex >= validList.size() )
        { nowIndex = validList.size() - 1; }

        valNow = indexToValue( nowIndex, validList );
        nowIndex = valueToIndex( valNow, mModal.mList );

        m_pScrollBar->setNow( nowIndex );
    }
    else if ( key == R_Pkey_FZ )
    {
        RPopListItem *pItem;

        pItem = mModal.mList[ m_pScrollBar->getNow() ];
        Q_ASSERT( NULL != pItem );

        if ( !pItem->getEnable() )
        { return; }

        //! checkable
        if ( pItem->getCheckable() )
        {
            pItem->setChecked( !pItem->getChecked() );
            update();
            emit sig_value_changed( pItem->m_pResItem->value );
        }
        else
        {
            emit sig_value_changed( pItem->m_pResItem->value );

            hide();
        }
    }
    //! menu key
    else if ( menuKeys.contains(key)  )
    {
        //! is link key
        if ( menuKeys[key] == mLinkId )
        {}
        else
        {
            hide();

            //! repost again
            QKeyEvent *pKeyEvent = new QKeyEvent(
                                                    event->type(),
                                                    event->key(),
                                                    event->modifiers(),
                                                    event->text(),
                                                    false,
                                                    event->count()
                                                );
            if ( NULL != pKeyEvent )
            {
                qApp->postEvent( qApp, pKeyEvent );
            }
            else
            {}
            return;
        }

        //! ---- proc

        //! 自动回退
        Q_ASSERT( validList.size() > 0 );
        nowIndex = (nowIndex + 1 )% validList.size();

        valNow = indexToValue( nowIndex, validList );
        nowIndex = valueToIndex( valNow, mModal.mList );

        m_pScrollBar->setNow( nowIndex );
        if ( mModal.mList[ nowIndex ]->getEnable() )
        {
            emit sig_value_changed( valNow );
        }
    }

    //! 弹出菜单关闭
    else if ( key == R_Pkey_PageOff )
    {
        hide();
    }

    //! 弹出菜单关闭，并且返回
    else if ( key == R_Pkey_PageReturn
         || key == R_Pkey_PageUp
         || key == R_Pkey_PageDown )
    {
        hide();        
    }
    else
    {
        qWarning()<<"!!!no action defined"<<key;
    }

    return;
}

void RPopupView::hideEvent(QHideEvent * event)
{
    //added by hexiaohua. for bug2758
    RServMenu::removeServiceWidget( getLinkMsg() );

    emit sig_hide();

    return uiWnd::hideEvent( event );
}

void RPopupView::resizeEvent(QResizeEvent */*event*/)
{
    m_pScrollBar->setGeometry( _style.mScroll.x(),
                               _style.mScroll.y(),
                               _style.mScroll.width(),
                               rect().height() - _style.mScroll.y() - _style.mScrollBottomShift );
}

void RPopupView::wheelEvent(QWheelEvent * event)
{
    Q_ASSERT( event != NULL );
    QWheelEvent *pEvent;

    pEvent = new QWheelEvent( *event );
    Q_ASSERT( pEvent != NULL );

    QApplication::postEvent( m_pScrollBar, pEvent );

    event->accept();
}

void RPopupView::on_scroll_page_changed( int /*from*/, int /*to*/ )
{
    update();
}
void RPopupView::on_scroll_value_changed( int value )
{
    update();
    emit sig_select_changed( mModal.mList[ value ]->m_pResItem->value );
}

void RPopupView::setLinkId( int id, int msgId )
{
    mLinkId = id;
    mMsgId = msgId;
}

int RPopupView::getLinkId()
{
    return mLinkId;
}
int RPopupView::getLinkMsg()
{
    return mMsgId;
}

void RPopupView::setCommonStyle( RElementComm *pComm )
{
    Q_ASSERT( NULL != pComm );
    m_pCommonStyle = pComm;
}

void RPopupView::setModal( const RPopListModal &modal )
{
    mModal = modal;

    _style.resize( this, mModal.size() );

    m_pScrollBar->setVisible( mModal.size() > _style.mViewCapacity );
    m_pScrollBar->setScroll( 0,
                             mModal.size()-1,
                             mModal.mCurrentIndex,
                             qMin( _style.mViewCapacity, mModal.size()) );
}

void RPopupView::show( QWidget *caller )
{
    this->setParent(sysGetMainWindow());

    QRect selfRect, callerRect, parentRect;
    QWidget *popupWidget;
    QPoint pt, phyPt;
    int y1, y2;

    pt.setX( 0 );
    pt.setY( 0 );

    Q_ASSERT( NULL != caller );
    phyPt = caller->mapToGlobal( pt );

    popupWidget = dynamic_cast<QWidget*>( caller->parent()->parent() );
    Q_ASSERT( NULL != popupWidget );

    //! self rect
    selfRect = rect();

    //! caller rect
    callerRect = caller->geometry();

    //! root parent rect
    parentRect = popupWidget->geometry();

    //! y1 && y2
    y1 = callerRect.top() - selfRect.height() / 2;
    y2 = y1 + selfRect.height();

    //! shift the pos
    if ( y1 < 0 )
    {
        y1 = 0;
        y2 = y1 + selfRect.height();
    }
    else if ( y2 >= parentRect.height() )
    {
        y2 = parentRect.height();
        y1 = parentRect.height() - selfRect.height();
    }
    else
    {
    }

    y2 = phyPt.y();

    int popHeight = this->geometry().height();
    if( phyPt.y() + popHeight > 540 )
    {
        y2 = y2 - ( callerRect.top() - y1 );
    }
    //! move to the pos by the caller
    move( phyPt.x() -  selfRect.width(), y2  );

    QWidget::show();
}

void RPopupView::setKnobView( KnobView view )
{
    switch( view )
    {
    case knob_none:
        mKnobIcon = RPopupView_Style::knob_none;
        break;

    case knob_tune:
        mKnobIcon = RPopupView_Style::knob_tune;
        break;

    case knob_tune_z:
        mKnobIcon = RPopupView_Style::knob_tunez;
        break;

    default:
        mKnobIcon = RPopupView_Style::knob_none;
        break;
    }
}

bool RPopupView::keyFilter( int physicalKey )
{
    if ( !isVisible() )
    {
        return false;
    }

    Q_ASSERT( mLinkId >=0 && mLinkId< 7 );
    if ( physicalKey == menuKeys[mLinkId] )
    {
        return true;
    }

    hide();
    return false;
}

/*!
 * \brief RPopupView::indexFromPt
 * \param pt
 * \return
 * pt is relative self widget
 */
int RPopupView::indexFromPt( QPoint pt )
{
    //! not in text region
    if ( pt.x() < 0
         || pt.x() > _style.mTextRect.right()
         || pt.y() < _style.mTextRect.top() )
    { return -1; }

    int index;

    index = ( pt.y() - _style.mHeadHeight ) / _style.mTextRect.height();

    if ( index >= _style.mViewCount )
    { return -1; }

    return index + m_pScrollBar->getViewFrom();
}

void RPopupView::collectValidList( QList< RPopListItem *> &validList )
{
    foreach( RPopListItem *pItem, mModal.mList )
    {
        Q_ASSERT( NULL != pItem );

        if ( pItem->getEnable() && pItem->getVisible() )
        {
            validList.append( pItem );
        }
    }
}

int RPopupView::valueToIndex( int val, QList< RPopListItem *> &validList )
{
    int index = 0;
    foreach( RPopListItem *pItem, validList )
    {
        Q_ASSERT( NULL != pItem );
        Q_ASSERT( NULL != pItem->m_pResItem );

        if ( pItem->getValue() == val )
        { return index; }

        index++;
    }

    return -1;
}
int RPopupView::indexToValue( int index,
                              QList< RPopListItem *> &validList )
{
    Q_ASSERT( index >=0 && index < validList.size() );

    Q_ASSERT( validList[index]->m_pResItem != NULL );

    return validList[index]->getValue();
}

}
