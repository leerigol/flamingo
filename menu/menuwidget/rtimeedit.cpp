#include "rtimeedit.h"

namespace menu_res {

RTimeEditStyle RTimeEdit::_style;

RSegEdit::RSegEdit( QWidget *parent ) : RMenuWidget( parent )
{
    m_pKnob = new RFuncKnob(this);
    Q_ASSERT( NULL != m_pKnob );

    m_pKnob->setView( control_knob_tune_z );

    mbTuneAble = true;
}

void RSegEdit::focusProc()
{
    int focus;

    focus = (int)mFocus;
    focus++;
    //! loop 焦点
    if ( focus > (int)( mFocusB ) )
    {
        mFocus = mFocusA;
    }
    else if ( focus < (int)mFocusA )
    {
        mFocus = mFocusA;
    }
    else
    {
        mFocus = (RTimeEditStyle::SegFocus)focus;
    }

    update();
}

void RSegEdit::onClick()
{ focusProc(); }

void RSegEdit::tuneClick()
{ focusProc(); }

RTimeEdit::RTimeEdit( QWidget *parent ) : RSegEdit(parent)
{
    mHour = 12;
    mMinute = 31;
    mSecond = 30;

    mFocus = RTimeEditStyle::focus_l0;
    mFocusA = RTimeEditStyle::focus_l0;
    mFocusB = RTimeEditStyle::focus_l2;

    //! loadresource
    _style.loadResource("ui/menu/time_edit/");
    _style.resize( this );

    Q_ASSERT( NULL != m_pKnob );
    m_pKnob->move( _style.mKnobXY );
}

void RTimeEdit::paintEvent(QPaintEvent *event )
{
    Q_ASSERT( NULL != event );
    RPainter painter(this);

    //! bg
    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }
    _style.paintEvent( painter );

    do
    {
        Q_ASSERT( NULL != m_pResMenuItem );
        Q_ASSERT( NULL != m_pResMenuItem->m_pResItem );

        //! fg
        eMenuRes resType;
        QRect boundRect;

        resType = m_pResMenuItem->m_pResItem->getType();
        boundRect = _style.mCaptionRect;

        //! drawIcon
        if ( is_attr(resType, MR_Icon) )
        {
            QImage img;
            int v = m_pResMenuItem->m_pResItem->getImage( img );
            Q_ASSERT( v == 0 );

            _style.drawIcon( painter,
                             img,
                             boundRect );
            boundRect.adjust( img.width(), 0, 0, 0 );
        }

        //! draw text
        if ( is_attr(resType, MR_Str) )
        {
            _style.drawCaption( painter,
                                boundRect,
                                m_pResMenuItem->getTitle() );
        }
    }while(0);

    //! content
    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), isTuneNow() );
    m_pKnob->setState( tuneStat );

    _style.drawTime( painter, mHour, mMinute, mSecond, mFocus, tuneStat );
}

void RTimeEdit::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    int packVal;

    queryCmd( packVal );
    mHour = (packVal >> 16) & 0xff;
    mMinute = (packVal >> 8) & 0xff;
    mSecond = (packVal >> 0) & 0xff;

    update();
}

void RTimeEdit::tuneInc( int cnt )
{ focusEdit(cnt, true ); }
void RTimeEdit::tuneDec( int cnt )
{ focusEdit(cnt, false); }

void RTimeEdit::setHour( int hour )
{
    RControl::rangeRoll( hour, 0, 23 );

    mHour = hour;

    update();
}
int RTimeEdit::getHour()
{
    return mHour;
}

void RTimeEdit::setMinute( int minute )
{
    RControl::rangeRoll( minute, 0, 59 );

    mMinute = minute;

    update();
}
int RTimeEdit::getMinute()
{
    return mMinute;
}

void RTimeEdit::setSecond( int second )
{
    RControl::rangeRoll( second, 0, 59 );

    mSecond = second;

    update();
}
int RTimeEdit::getSecond()
{
    return mSecond;
}

void RTimeEdit::setTime( int h, int m, int s )
{
    setHour( h );
    setMinute( m );
    setSecond( s );
}

int RTimeEdit::packValue()
{
    return ( ( mHour << 16 ) & 0xff0000 ) |
           ( ( mMinute << 8 ) & 0xff00 )  |
           ( ( mSecond << 00 ) & 0xff );
}

void RTimeEdit::focusEdit( int keyCnt, bool bInc )
{
    //! function
    int packVal;
    ui_attr::PackInt val;

    //! current val
    queryCmd( packVal );
    mHour = (packVal >> 16) & 0xff;
    mMinute = (packVal >> 8) & 0xff;
    mSecond = (packVal >> 0) & 0xff;

    //! set value
    switch( mFocus )
    {
        case RTimeEditStyle::focus_l0:
        val.set( mHour );
        break;

        case RTimeEditStyle::focus_l1:
        val.set( mMinute );
        break;

        case RTimeEditStyle::focus_l2:
        val.set( mSecond );
        break;

        default:
        return;
    }

    keyIncDecProc( val, bInc, keyCnt );

    //! set value again
    switch( mFocus )
    {
        case RTimeEditStyle::focus_l0:
        setHour( val.val32 );
        break;

        case RTimeEditStyle::focus_l1:
        setMinute( val.val32 );
        break;

        case RTimeEditStyle::focus_l2:
        setSecond( val.val32 );
        break;

        default:
        return;
    }

    //! do cmd
    doCmd( packValue() );
}

}

