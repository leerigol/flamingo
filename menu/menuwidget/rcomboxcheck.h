#ifndef RCOMBOXCHECK_H
#define RCOMBOXCHECK_H

#include "rcombox.h"

#include "../menustyle/rcontrolstyle.h"

namespace menu_res {

/*!
 * \brief The RComboxCheck class
 * -m选n控件
 * -控件中选中的项目打上标记
 * -控件数值以位掩码的方式表示，也就是说选择项最多32项
 */
class RComboxCheck : public RCombox
{
    Q_OBJECT

protected:
    static RCombox_Check_Style _style;

public:
    RComboxCheck( QString txt="",
             QWidget *parent = 0,
             QWidget *popParent = 0
             );

public:
    virtual void on_value_changed( int msg );

protected Q_SLOTS:
    void on_checked_changed( int val );

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual void onClick();

protected:
    void paintCaption( QPainter &painter );

    int getOptionBm();
    void updateOptionBm( int value );

protected:
    int mbmValue;
    int mModelCurVal;
};

}

#endif // RCOMBOXCHECK_H
