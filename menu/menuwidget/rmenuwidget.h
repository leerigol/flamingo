#ifndef RMenuWidget_H
#define RMenuWidget_H

#include <QtWidgets>
#include "../draw/rpainter.h"
#include "rcontrol.h"

namespace menu_res{

//! menu base widget
class RMenuWidget : public QPushButton, public RControl
{
    Q_OBJECT

public:
    RMenuWidget( QWidget *parent = 0 );

public:
    virtual QSize sizeHint() const;

protected:
    virtual bool hitButton( const QPoint & pos) const;
    virtual void keyPressEvent(QKeyEvent * event);
    virtual void keyReleaseEvent(QKeyEvent * event);

public:
    virtual void on_enable_changed( int msg, bool b );
    virtual void on_visible_changed( int msg, bool b );

protected:
    virtual void onClick();

    virtual void tuneInc( int cnt );
    virtual void tuneDec( int cnt );
    virtual void tuneClick();
    virtual void zEnter();
    virtual void stepNext();

protected Q_SLOTS:
    void on_clicked();

protected:
    void attachPageWidget( QWidget *pWidget );

protected:
    bool mbFocusAble;
    bool mbTuneAble;
};

}

#endif // RMenuWidget_H
