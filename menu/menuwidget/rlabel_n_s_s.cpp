#include "rlabel_n_s_s.h"

namespace menu_res
{

RLabel_n_s_s_Style RLabel_n_s_s::_style;

RLabel_n_s_s::RLabel_n_s_s( QString /*txt*/, QWidget *parent )
            : RMenuWidget(parent)
{
    _style.loadResource("ui/menu/label_nss/");
    _style.resize( this );

    mbFocusAble = false;

    mCheckImg = _style.mUnCheckImg;
}

void RLabel_n_s_s::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);

    QImage img;
    eMenuRes resType;
    QRect boundRect;

    if ( !isEnabled() )
    {
        _style.setStatus( element_disabled );
    }
    else if ( isDown() )
    {
        _style.setStatus( element_actived_down );
    }
    else
    {
        _style.setStatus( element_deactived );
    }

    _style.paintEvent( painter );

    Q_ASSERT( NULL != m_pResMenuItem );
    Q_ASSERT( NULL != m_pResMenuItem->m_pResItem );
    resType = m_pResMenuItem->m_pResItem->getType();
    boundRect = _style.mCaptionRect;

    //! Caption
    //if ( is_attr(resType, MR_Icon) )
    if( mCheckImg.size() > 0 )
    {
        //int v = m_pResMenuItem->m_pResItem->getImage(img) ;
        //Q_ASSERT( 0 == v);

        img.load( mCheckImg );

        _style.drawIcon( painter, img, boundRect );
        boundRect.adjust( img.width()+4, 0, 0, 0 );
    }

    if ( is_attr(resType, MR_Str) )
    {
        _style.drawCaption( painter,
                            boundRect,
                            m_pResMenuItem->getTitle() );
    }


    //queryCmd( mVal );
    //! text
    _style.drawText( painter,
                     _style.mTextRect,
                     mVal );
}

void RLabel_n_s_s::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    queryCmd( mVal );

    if(mVal.contains("$"))
    {
        QStringList str = mVal.split("$");
        QString status = str.at(1);
        mVal = str.at(0);

        if( status.compare("ON") == 0 )
        {
            mCheckImg = _style.mCheckImg;
        }
        else
        {
            mCheckImg = _style.mUnCheckImg;
        }

    }

    update();
}

void RLabel_n_s_s::onClick()
{
    doCmd(0);
}

}

