#include "ckeypadcontext.h"

namespace menu_res {
CKeyPadContext::CKeyPadContext()
{
    mView = InputKeypad::NoUnit;

    pairUnitView unitView;

    //! for all unit
    unitView.first = DsoType::Unit_W;
    unitView.second = InputKeypad::WattUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_A;
    unitView.second = InputKeypad::AmpereUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_V;
    unitView.second = InputKeypad::VoltUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_U;
    unitView.second = InputKeypad::UnkUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_s;
    unitView.second = InputKeypad::TimeUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_hz;
    unitView.second = InputKeypad::FreqUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_degree;
    unitView.second = InputKeypad::Degree;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_percent;
    unitView.second = InputKeypad::Percent;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_db;
    unitView.second = InputKeypad::dBUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_dbm;
    unitView.second = InputKeypad::UnkUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_none;
    unitView.second = InputKeypad::NumValue;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_Div;
    unitView.second = InputKeypad::DivUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_VmulS;
    unitView.second = InputKeypad::UnkUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_VdivS;
    unitView.second = InputKeypad::UnkUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_Vrms;
    unitView.second = InputKeypad::UnkUnit;
    mListUnitView.append( unitView );

    unitView.first = DsoType::Unit_oum;
    unitView.second = InputKeypad::UnkUnit;
    mListUnitView.append( unitView );
}

void CKeyPadContext::setView( DsoType::Unit unit )
{
    pairUnitView unitView;

    foreach( unitView, mListUnitView )
    {
        if ( unitView.first == unit )
        {
            mView = unitView.second;
            return;
        }
    }

    mView = InputKeypad::UnkUnit;
}

}
