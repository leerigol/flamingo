#include "rinvertbutton.h"

namespace menu_res {

RInvertButtonStyle RInvertButton::_style;

RInvertButton::RInvertButton( QString /*txt*/, QWidget */*parent*/ )
{
    _style.loadResource( "ui/menu/invert_button/" );
    resize( _style.mCommon.mSize.width(),
            _style.mCommon.mSize.height() );

    mType = t_bool;

    mOpt0 = 0;
    mOpt1 = 1;
}

void RInvertButton::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);
    eMenuRes resType;
    QImage img;
    QRect boundRect;

    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }
    _style.paintEvent( painter );

    Q_ASSERT( NULL != m_pResMenuItem );
    Q_ASSERT( NULL != m_pResMenuItem->m_pResItem );

    //! title
    resType = m_pResMenuItem->m_pResItem->getType();
    boundRect = _style.mCaptionRect;
    if ( is_attr(resType, MR_Icon) )
    {
        int v = m_pResMenuItem->m_pResItem->getImage( img );
        Q_ASSERT( 0 ==  v);

        _style.drawIcon( painter, img, boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }

    if ( is_attr(resType, MR_Str) )
    {
        _style.drawCaption( painter,
                            boundRect,
                            m_pResMenuItem->getTitle() );
    }

    //! content
    resType = m_pResMenuItem->getOptionType( m_pResMenuItem->mIntVal );

    do
    {
        //! draw on/off img
        if ( is_attr(mUiPara1, 0x01) )
        {
            //! draw image
            if ( _style.getImage( img, m_pResMenuItem->mIntVal) )
            {
                _style.drawIcon( painter, img, _style.mTextRect, MR_Icon, false );
                break;
            }
        }

        //! draw icon
        if ( is_attr(resType, MR_Icon) )
        {
            int v = m_pResMenuItem->getOptionImage( m_pResMenuItem->mIntVal, img );
            Q_ASSERT ( 0 == v);

            _style.drawIcon( painter, img, _style.mTextRect, resType );
        }

        //! draw str
        if ( is_attr(resType, MR_Str) )
        {
            _style.drawText( painter,
                             _style.mTextRect,
                             m_pResMenuItem->getOptionStrNow() );
        }

    }while(0);
}

RMenu *RInvertButton::queryOptionMenu()
{
    bool bVal;
    RControl::queryCmd( bVal );

    return RControl::getOptionMenu( (int)bVal );
}

void RInvertButton::on_value_changed( int msg )
{
    if ( !RControl::cmdMatch(msg) )
    { return; }

    if ( mType == t_bool )
    {
        bool bVal;
        RControl::queryCmd( bVal );
        //! control value
        setControlValue( bVal );
    }
    else if ( mType == t_int )
    {
        int val;
        RControl::queryCmd( val );
        setControlValue( val );
    }
    else
    { Q_ASSERT(false); }

    if ( getUiNodeCompress() )
    {}
    else
    {
        RControl::reCascadeMenu();
    }

    update();
}

void RInvertButton::setType( Type t )
{ mType = t; }
void RInvertButton::setOptions( int opt0, int opt1 )
{
    mOpt0 = opt0;
    mOpt1 = opt1;
}

void RInvertButton::onClick()
{
    int iVal;
    bool bVal;

    //! check type
    if ( mType == t_bool )
    {
        if ( scanLau( bVal ) )
        { return; }

        queryCmd( bVal );

        doCmd( bVal == 0 ? true : false );
    }
    else if ( mType == t_int )
    {
        if ( scanLau( iVal ) )
        { return; }

        queryCmd( iVal );

        doCmd( iVal == mOpt0? mOpt1 : mOpt0 );
    }
    else
    {
        qWarning()<<"!!!Invalid invert button type";
        return;
    }
}

}
