#include "rlitecombox.h"

namespace menu_res {

RLiteCombox::RLiteCombox( QWidget *parent ) : RMenuWidget(parent)
{
}
RLiteCombox::~RLiteCombox()
{
    ROptPair *pPair;
    foreach( pPair, mOpts )
    {
        Q_ASSERT( NULL != pPair );
        delete pPair;
    }
}

void RLiteCombox::on_enable_changed( int msg, int /*opt*/, bool b )
{
    if ( !cmdMatch(msg ) ) return;

    ROptPair *pPair;
    pPair = findOpt( msg );
    Q_ASSERT( NULL != pPair );
    pPair->second = b;
}
void RLiteCombox::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    int val;

    queryCmd( val );

    setControlValue( val );
}

void RLiteCombox::onClick()
{
    stepNext();
}

void RLiteCombox::stepNext( )
{
    int val;

    queryCmd( val );

    //! find value
    QList<ROptPair*> items;
    items = validItems();
    if ( items.size() < 1 ) return;

    int index = findIndex( val, items );
    Q_ASSERT( index >= 0 );

    index = (index + 1 )%items.size();

    doCmd( items[index]->first );

    LOG_DBG()<<val<<items[index]->first;
}

void RLiteCombox::addOption( int opt )
{
    ROptPair *pPair;

    pPair = new ROptPair();
    Q_ASSERT( NULL != pPair );

    pPair->first = opt;
    pPair->second = true;

    mOpts.append( pPair );
}

ROptPair *RLiteCombox::findOpt( int opt )
{
    ROptPair *pPair;
    foreach( pPair, mOpts )
    {
        Q_ASSERT( NULL != pPair );

        if ( pPair->first == opt )
        { return pPair; }
    }

    return NULL;
}

int RLiteCombox::findIndex( int opt,
                            QList<ROptPair*> &list )
{
    int i;
    for( i =0; i < list.size(); i++ )
    {
        if ( list[i]->first == opt )
        { return i; }
    }

    return -1;
}

QList<ROptPair*> RLiteCombox::validItems()
{
    QList< ROptPair *> list;
    ROptPair *pPair;
    foreach( pPair, mOpts )
    {
        Q_ASSERT( NULL != pPair );
        if ( pPair->second )
        { list.append(pPair);}
    }

    return list;
}

}
