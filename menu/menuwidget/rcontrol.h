#ifndef RCONTROL_H
#define RCONTROL_H

#include <QtWidgets>
#include <QLayout>

#include "../../include/dsodbg.h"

#include "../arch/rmenures.h"

#include "../event/reventfilter.h"
#include "../event/ckeysticker.h"

#include "../../service/service.h"
#include "../../service/service_msg.h"

#include "../../gui/cguiformatter.h"

#include "../../com/uiattr/uiattr.h"
#include "../../com/keyacc/ckeyacc.h"

#include "./ckeypadcontext.h"

namespace menu_res {

class CResMenuItem;     //! resource menu item

class RMenu;            //! menu
class RServMenu;        //! service menu

/*!
 * \brief The RControl class
 * - 菜单控件基础类
 * - 不是widget
 */
class RControl
{

protected:
    static RControl *_tuneFocusControl;
    static QString nullString;
    static CKeySticker *_cpSticker;
    static void rangeRoll( int &val, int l, int r );
    static QList<eMenuControl> _noHelpControls;
protected:
    RControl();
public:
    virtual ~RControl();

    virtual void onParentActive(){}
    virtual void onParentDeactive(){}

public:
    CResMenuItem *m_pResMenuItem;       /*!< resource */
    RServMenu *m_pServMenu;             /*!< service dst*/

protected:
    qlonglong mStickTime;               /*!< stick time */

    eControlPara mContPara;             /*!< 按键或数值 */
    bool mbText;                        /*!< 是否显示标题名称*/

    eControlKnob mKnobView;
    int mUiPara1, mUiPara2;

    eMenuControl mContType;
    bool mbReadOnly;                    /*!< 只读控件*/

protected:
    void init();

    QSize sizeHint() const;

public:
    void setResItem( CResMenuItem *pItem );
    CResMenuItem * getResItem() const;

    void setServMenu( RServMenu *pMenu );
    RServMenu *getServMenu();

    void setControlType( eMenuControl cont );

    void setReadOnly( bool bRead );
    bool isReadOnly();
    //! key or value
    virtual void setPara( eControlPara para );
    eControlPara getPara();

    //! text visible
    virtual void setTextVisible( bool b );
    bool getTextVisible();

    //! key pad action
    virtual void setKnobView( eControlKnob view );
    eControlKnob getKnobView();

    QString getOptionString( int val );
    QString getTitleString();

    eMenuControl getControl();
    bool isTuneAble();

    void setControlValue( int val );
    void setControlValue( const QString &str );

    void reCascadeMenu();
public:
    virtual RMenu *queryOptionMenu();

    virtual void reTranslate();

public:
    virtual void on_value_changed( int msg );

    virtual void on_enable_changed( int msg, bool bEn );
    virtual void on_enable_changed( int msg, int opt, bool en );

    virtual void on_visible_changed( int msg, bool b );
    virtual void on_visible_changed( int msg, int opt, bool b );

    virtual void on_unit_changed( int msg, QString unit );

    virtual void on_context_changed( int msg );
public:
    bool cmdMatch( int msg );

    void focusIn();
    bool tuneFocusIn();
    bool isTuneNow();

public:
    virtual void setUiAttr( int para1, int para2 );

protected:
    int alignValue( int val, qlonglong step, bool add_or_sub = true);
    int alignValue( ui_attr::PackInt &now, qlonglong step, bool add_or_sub = true);

    bool canHelp();
    bool canBypass();
    bool doFilter();
    bool hasOption();

    void doCmd( int val, eComboxSubAttr attr=combox_raw );
    void doCmd( qlonglong val, eComboxSubAttr attr=combox_raw );
    void doCmd( ui_attr::PackInt val, eComboxSubAttr attr=combox_raw );
    void doCmd( bool val, eComboxSubAttr attr=combox_raw );
    void doCmd( const QString &str, eComboxSubAttr attr=combox_raw );
    void doCmd( dsoReal &val, eComboxSubAttr attr=combox_raw );


    void doCmd( int msg, int val );
    void doCmd( int msg, qlonglong val );
    void doCmd( int msg, dsoReal &packVal );

    void doCmd( int msg, ui_attr::PackInt packVal );

    void doZCmd();

    //! query success
    bool queryCmd( eComboxSubAttr attr=combox_raw );
    bool queryCmd( bool &val, eComboxSubAttr attr=combox_raw );
    bool queryCmd( int &val, eComboxSubAttr attr=combox_raw );
    bool queryCmd( ui_attr::PackInt &val, eComboxSubAttr attr=combox_raw );

    bool queryCmd( float &val, eComboxSubAttr attr=combox_raw );
    bool queryCmd( QString &str, eComboxSubAttr attr=combox_raw );

    bool queryCmd( int msg, ui_attr::PackInt &val, eComboxSubAttr attr=combox_raw );

    bool scanLau( bool &val, eComboxSubAttr attr=combox_raw );
    bool scanLau( int &val, eComboxSubAttr attr=combox_raw );
    bool scanLau( ui_attr::PackInt &val, eComboxSubAttr attr=combox_raw );

    RMenu *getOptionMenu( int val );

    ui_attr::uiProperty *queryUiProperty( eComboxSubAttr attr=combox_raw,
                                          int msg = 0 );

    void stickProc( ui_attr::PackInt &val,
                    ui_attr::PackInt &valPre,
                    eComboxSubAttr attr=combox_raw,
                    int msg = 0 );
    void stickOn();
    bool uiEventFilter();

    qlonglong getUiStep( eComboxSubAttr attr=combox_raw,
                         int dir = 1 );
    ui_attr::PackInt getUiZVal( eComboxSubAttr attr=combox_raw );
    ui_attr::PackInt getUiMax( eComboxSubAttr attr=combox_raw );
    ui_attr::PackInt getUiMin( eComboxSubAttr attr=combox_raw );

    bool getUiFgColor( QColor &color, eComboxSubAttr attr=combox_raw );
    bool getUiBgColor( QColor &color, eComboxSubAttr attr=combox_raw );

    DsoType::Unit getUiUnit( eComboxSubAttr attr=combox_raw );
    DsoType::DsoViewFmt getUiViewFmt( eComboxSubAttr attr=combox_raw );

    key_acc::KeyAcc getUiAcc( eComboxSubAttr attr=combox_raw );

    virtual double getUiBase( eComboxSubAttr attr=combox_raw );
    virtual DsoReal getUiRealBase( eComboxSubAttr attr=combox_raw );

    //!
    //! \brief hasUiBase
    //! \param attr
    //! \return
    //! 用户是否设置了uibase，如果没有设置，则默认为整数
    bool hasUiBase( eComboxSubAttr attr=combox_raw );

    QString getUiPreStr( eComboxSubAttr attr=combox_raw );
    virtual QString getUiPostStr( eComboxSubAttr attr=combox_raw );

    QString& getUiOutStr( bool &bOk, eComboxSubAttr attr=combox_raw );

    bool getUiNodeCompress( eComboxSubAttr subAttr=combox_raw );

    int getUiMaxLength();
    bool getUiNoCache();

    bool uiKeyRequest( int keyCnt );


    void keyIncDecProc( ui_attr::PackInt &val,
                        bool bInc,
                        int count,
                        eComboxSubAttr attr=combox_raw );

    void keyEventProc(
                       QKeyEvent *event,
                       eComboxSubAttr attr=combox_raw );

    void buildKeypadContext( CKeyPadContext &keyPadContext,
                             ui_attr::PackInt &now,
                             eComboxSubAttr attr=combox_raw );

    void formatValue( ui_attr::PackInt &val,
                      eControlFmt cType,
                      const DsoReal &real,
                      QString &strOut,
                      DsoViewFmt fmt=fmt_def );
};

}

#endif // RCONTROL_H
