#ifndef RCHILDBUTTON_H
#define RCHILDBUTTON_H

#include "../menuwidget/rcontrol.h"

#include "../menustyle/rcontrolstyle.h"

#include "rmenuwidget.h"

namespace menu_res {

/*!
 * \brief The RChildButton class
 * 子菜单控件
 * -点击后自动切换到子菜单页面上
 */
class RChildButton : public RMenuWidget
{
    Q_OBJECT

protected:
    static RChildButtonStyle _style;

public:
    RChildButton( QString txt="", QWidget *parent=0 );

protected:
    QLabel *m_pLabel;
    RMenu *m_pChild;

protected:
    virtual void paintEvent( QPaintEvent *event );
public:
    virtual void on_enable_changed( int msg, bool b );
    virtual void on_visible_changed( int msg, bool b );

protected:
    virtual void onClick();

public:
    void setChild( RMenu *child );
    RMenu *getChild();

    int getChildId();

    QString *getChildName( int id );
};

}
#endif // RCHILDBUTTON_H
