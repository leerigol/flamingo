#ifndef RLISTWIDGET_H
#define RLISTWIDGET_H

#include "../../widget/rimagebutton.h"
#include "../../widget/rscrollbar.h"

#include "../menustyle/rcontrolstyle.h"

#include "../wnd/uiwnd.h"   //! key control

#include "rcombox.h"
#include "rpopupitem.h"

namespace menu_res {

//! popup list wnd
class RPopupView : public uiWnd
{
    Q_OBJECT

public:
    /*!
     * \brief The KnobView enum
     * 旋钮视图
     */
    enum KnobView
    {
        knob_none,  //! 没有旋钮
        knob_tune_z,//! 旋钮+按下
        knob_tune,  //! 旋钮
    };

protected:
    static RPopupView_Style _style;

public:
    RPopupView( QWidget *parent = 0 );

protected:
    virtual bool eventFilter(QObject *, QEvent *);

    virtual void doPaint( QPaintEvent *event, QPainter &painter );
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent * event);
    virtual void keyReleaseEvent( QKeyEvent *event );

    virtual void hideEvent(QHideEvent * event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual void wheelEvent(QWheelEvent * event);

Q_SIGNALS:
    void sig_select_changed( int optVal );
    void sig_value_changed( int optVal );
    void sig_hide();
    void sig_pressed(QPoint);

protected Q_SLOTS:
    void on_scroll_page_changed( int from, int to );
    void on_scroll_value_changed( int value );

public:
    void setLinkId( int id, int msgId );
    int getLinkId();
    int getLinkMsg();

    void setCommonStyle( RElementComm *pComm );
    void setModal( const RPopListModal &modal );
    void show( QWidget *caller );

    void setKnobView( KnobView view );

    bool keyFilter( int physicalKey );

protected:
    int  indexFromPt( QPoint pt );

    void collectValidList( QList< RPopListItem *> &validList );
    int valueToIndex( int val, QList< RPopListItem *> &validList );
    int indexToValue( int index, QList< RPopListItem *> &validList );

private:
    RElementComm *m_pCommonStyle;

    //! data
    RPopListModal mModal;

    //! widgets
    RScrollBar *m_pScrollBar;

    int mLinkId;
    int mMsgId;        //! msg id

    RPopupView_Style::KnobIcon mKnobIcon;

    //! 物理按键
    QMap<int,int> menuKeys;

    bool    isMoved;
    QPoint  mStartPos;
};

}

#endif // RLISTWIDGET_H
