#ifndef RLABEL_ICON_H
#define RLABEL_ICON_H

#include "rlabel_i_i_f.h"
#include "../menustyle/rcontrolstyle.h"

namespace menu_res {

/*!
 * \brief The RLabel_Icon class
 * 不显示数值，显示为旋钮图标
 * -key input
 * -return int
 */
class RLabel_Icon : public RLabel_i_i_f
{
    Q_OBJECT

protected:
    static RLabel_icon_Style _style;

private:
    static QPainterPath _iconPath;

public:
    RLabel_Icon( QString txt="",
                 QWidget *parent=0 );

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    virtual void on_value_changed( int );
protected:
    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );

    virtual void tuneClick();

    void tuneProc( int keyCnt, bool bInc );
};

}

#endif // RLABEL_ICON_H
