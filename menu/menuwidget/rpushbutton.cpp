
#include "rpushbutton.h"

namespace menu_res {

RPushButtonStyle RPushButton::_style;

RPushButton::RPushButton( QString /*str*/, QWidget *parent )
                        : RMenuWidget( parent )
{
    _style.loadResource( "ui/menu/push_button/");
    _style.resize( this );

    mPushVal = 0;
}

void RPushButton::setValue( int val )
{
    mPushVal = val;
}

int RPushButton::getValue()
{
    return mPushVal;
}

void RPushButton::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( event != NULL );
    RPainter painter(this);

    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }
    _style.paintEvent( painter );

    eMenuRes resType;

    resType = m_pResMenuItem->m_pResItem->getType();

    if ( is_attr(resType, MR_Icon) )
    {
        QImage img;
        int v = m_pResMenuItem->m_pResItem->getImage(img);
        Q_ASSERT( 0 == v );

        _style.drawIcon( painter, img,
                         _style.mTextRect,
                         resType );
    }
    if ( is_attr(resType, MR_Str) )
    {
        _style.drawText( painter, _style.mTextRect, m_pResMenuItem->getTitle() );
    }
}

void RPushButton::onClick()
{
    doCmd( mPushVal );
}

}
