#ifndef RTIMEEDIT_H
#define RTIMEEDIT_H

#include "../menuwidget/rcontrol.h"
#include "../menustyle/rcontrolstyle.h"

#include "../../widget/rfuncknob.h"

#include "rmenuwidget.h"

namespace menu_res {
/*!
 * \brief The RSegEdit class
 * 数字段控件
 * -用于时间，日期，ip设置中
 */
class RSegEdit : public RMenuWidget
{
    Q_OBJECT
public:
    RSegEdit( QWidget *parent = 0 );

protected:
    void focusProc();

    virtual void onClick();
    virtual void tuneClick();

protected:
    RTimeEditStyle::SegFocus mFocus;
    RTimeEditStyle::SegFocus mFocusA, mFocusB;  /*!< 焦点边界 */

    RFuncKnob *m_pKnob;
};

class RTimeEdit : public RSegEdit
{
    Q_OBJECT
private:
    static RTimeEditStyle _style;

public:
    RTimeEdit( QWidget *parent = 0 );

protected:
    virtual void paintEvent(QPaintEvent *event );

public:
    virtual void on_value_changed( int msg );
protected:
    virtual void tuneInc( int cnt );
    virtual void tuneDec( int cnt );

public:
    void setHour( int hour );
    int getHour();

    void setMinute( int minute );
    int getMinute();

    void setSecond( int second );
    int getSecond();

    void setTime( int h, int m, int s = 0 );

private:
    int packValue();
    void focusEdit( int keyCnt, bool bInc );

private:
    int mHour;
    int mMinute;
    int mSecond;
};

}

#endif // RTIMEEDIT_H
