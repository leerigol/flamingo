#ifndef CKEYPADCONTEXT_H
#define CKEYPADCONTEXT_H

#include "../../include/dsotype.h"
#include "../../com/inputkeypad/inputkeypad.h"
namespace menu_res {
class CKeyPadContext
{
public:
    CKeyPadContext();
public:
    void setView( DsoType::Unit unit );

public:
    DsoRealGp mValueGp;
    InputKeypad::KeypadView mView;

private:
    typedef QPair< DsoType::Unit, InputKeypad::KeypadView > pairUnitView;
    QList< pairUnitView > mListUnitView;
};
}
#endif // CKEYPADCONTEXT_H
