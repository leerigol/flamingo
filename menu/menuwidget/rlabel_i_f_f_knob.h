#ifndef RLABEL_I_F_F_KNOB_H
#define RLABEL_I_F_F_KNOB_H

#include "../menuwidget/rcontrol.h"
#include "../menustyle/rcontrolstyle.h"

#include "../../widget/rfuncknob.h"

#include "rmenuwidget.h"

namespace menu_res {

/*!
 * \brief The RLabel_i_f_f_knob class
 * - set int
 * - get float
 * - view float
 * - knob proc
 * 手动处理键值，返回浮点数据
 */
class RLabel_i_f_f_knob : public RMenuWidget
{
    Q_OBJECT

protected:
    static RLabel_i_f_f_knob_Style _style;

public:
    RLabel_i_f_f_knob( QString txt="",
               QWidget *parent = 0 );
protected:
    float mVal;
    RFuncKnob *m_pKnob;

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    virtual void on_value_changed( int msg );
protected:
    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );

    virtual void tuneClick();

    virtual void zEnter();
};

}

#endif // RLABEL_I_F_F_KNOB_H
