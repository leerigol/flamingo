#include "rlabel_i_f_f_knob.h"

namespace menu_res {

RLabel_i_f_f_knob_Style RLabel_i_f_f_knob::_style;

RLabel_i_f_f_knob::RLabel_i_f_f_knob( QString /*txt*/,
                      QWidget *parent):RMenuWidget(parent)
{
    _style.loadResource( "ui/menu/label_iffk/" );
    _style.resize( this );

    m_pKnob = new RFuncKnob(this);
    Q_ASSERT( NULL != m_pKnob );

    m_pKnob->move( _style.mKnobXY );

    mVal = 1e-6f;

    mbTuneAble = true;
}

void RLabel_i_f_f_knob::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);

    bool bSelected = isTuneNow();
    //! bg
    if ( !isEnabled() )
    {
        _style.setStatus( element_disabled );
    }
    else if ( isDown() || bSelected)
    {
        _style.setStatus( element_actived_down );
    }
    else
    {
        _style.setStatus( element_deactived );
    }

    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(),  bSelected);

    _style.paintEvent( painter, tuneStat );

    m_pKnob->setState( tuneStat );

    //! -- caption
    QImage img;
    eMenuRes resType;
    QRect boundRect;

    Q_ASSERT( NULL != m_pResMenuItem );
    Q_ASSERT( NULL != m_pResMenuItem->m_pResItem );

    resType = m_pResMenuItem->m_pResItem->getType();
    boundRect = _style.mCaptionRect;
    if ( is_attr(resType, MR_Icon) )
    {
        int v = m_pResMenuItem->m_pResItem->getImage( img );
        Q_ASSERT( 0 ==  v);

        _style.drawIcon( painter, img, boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }

    if ( is_attr(resType, MR_Str) )
    {
        _style.drawCaption( painter,
                            boundRect,
                            m_pResMenuItem->getTitle());
    }

    if ( mbText )
    {
        QString str;
        bool bUiStr;

        //! ui out string
        str = getUiOutStr( bUiStr );
        if ( !bUiStr )
        {
            gui_fmt::CGuiFormatter::format(str, mVal);
            str = getUiPreStr() + str + getUiPostStr();
        }

        //! text
        _style.drawText( painter,
                                _style.mTextRect,
                                str);
    }
}

void RLabel_i_f_f_knob::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    queryCmd();
}

void RLabel_i_f_f_knob::tuneInc( int keyCnt )
{
    queryCmd();

    if ( RControl::uiKeyRequest( keyCnt) )
    {
    }
    else
    {
        doCmd(keyCnt);
    }
}
void RLabel_i_f_f_knob::tuneDec( int keyCnt )
{
    queryCmd();

    if ( RControl::uiKeyRequest( -keyCnt) )
    {}
    else
    {
        doCmd( -keyCnt );
    }
}

void RLabel_i_f_f_knob::tuneClick( )
{
    zEnter();
}

void RLabel_i_f_f_knob::zEnter()
{
    queryCmd();

    RMenuWidget::zEnter();
}

}

