#include <sys/time.h>           //! gettimeofday
#include "../include/dsostd.h"
#include "../../baseclass/chelprequest.h"

#include "./rmenus.h"

#include "rcontrol.h"

namespace menu_res {

RControl *RControl::_tuneFocusControl=NULL;
QString RControl::nullString;
CKeySticker *RControl::_cpSticker=NULL;
QList<eMenuControl> RControl::_noHelpControls;
/*!
 * \brief RControl::rangeRoll
 * \param val
 * \param l
 * \param r
 * 数值在左右边界间卷绕
 */
void RControl::rangeRoll( int &val, int l, int r )
{
    int dist, delta;
    dist = r - l + 1;
    delta = val - l;
    if ( delta >= dist ) { delta = delta % dist; val = l + delta;}
    if ( delta < 0) { delta = (-delta) % dist; val = r + 1 - delta; }
}

RControl::RControl()
{
    init();

    if ( nullString.isEmpty() )
    {
        nullString = "null";
    }

    if ( _cpSticker==NULL )
    {
        _cpSticker = new CKeySticker();
        Q_ASSERT( _cpSticker != NULL );
    }

    if ( RControl::_noHelpControls.size() < 1 )
    {
        RControl::_noHelpControls.append( MC_Invalid );
        RControl::_noHelpControls.append( Menu_Title );
        RControl::_noHelpControls.append( Button_Sub );
        RControl::_noHelpControls.append( Sub_Menu_Title );

        RControl::_noHelpControls.append( Button_sub_no_ret );
        RControl::_noHelpControls.append( Button_Parent );
    }
}

RControl::~RControl()
{
}

void RControl::init()
{
    m_pResMenuItem = NULL;
    m_pServMenu = NULL;

    mStickTime = 0;

    mContPara = para_val;
    mbText = true;

    mKnobView = control_knob_tune_pad;

    mUiPara1 = 0;
    mUiPara2 = 0;

    mContType = MC_Invalid;
    mbReadOnly = false;
}

QSize RControl::sizeHint() const
{
    return QSize(150,100 );
}

void RControl::setResItem( CResMenuItem *pItem )
{
    Q_ASSERT( NULL != pItem );
    m_pResMenuItem = pItem;
}
CResMenuItem * RControl::getResItem() const
{
    return m_pResMenuItem;
}

void RControl::setServMenu( RServMenu *pServMenu )
{
    Q_ASSERT(pServMenu !=NULL );

    m_pServMenu = pServMenu;
}
RServMenu *RControl::getServMenu()
{
    return m_pServMenu;
}

void RControl::setControlType( eMenuControl cont )
{
    mContType = cont;
}

void RControl::setReadOnly( bool bRead )
{ mbReadOnly = bRead; }
bool RControl::isReadOnly()
{ return mbReadOnly; }
void RControl::setPara( eControlPara para )
{
    mContPara = para;
}
eControlPara RControl::getPara()
{
    return mContPara;
}

void RControl::setTextVisible( bool b )
{
    mbText = b;
}
bool RControl::getTextVisible()
{
    return mbText;
}

void RControl::setKnobView( eControlKnob view )
{
    mKnobView = view;
}
eControlKnob RControl::getKnobView()
{
    return mKnobView;
}

QString RControl::getOptionString( int val )
{
    Q_ASSERT( m_pResMenuItem!=NULL );
    Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );

    return m_pResMenuItem->m_pResItem->getOptionString( val );
}
QString RControl::getTitleString()
{
    Q_ASSERT( m_pResMenuItem );
    Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );

    return m_pResMenuItem->m_pResItem->getTitleString();
}

eMenuControl RControl::getControl()
{
    Q_ASSERT( m_pResMenuItem );

    return m_pResMenuItem->getType();
}

bool RControl::isTuneAble()
{
    eMenuControl cont;

    cont = getControl();

    if ( mbReadOnly )
    { return false; }

    if ( cont == Label_Icon
         || cont == Label_Float
         || cont == Label_int_float_float_knob
         || cont == Label_int
         || cont == TimeEdit
         || cont == DateEdit
         || cont == IpEdit
         || cont == SeekBar
         || cont == ComboxKnob )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void RControl::setControlValue( int val )
{
    m_pResMenuItem->setValue( val );
}

void RControl::setControlValue( const QString &str )
{
    m_pResMenuItem->setValue( str );
}

void RControl::reCascadeMenu()
{
    RMenu *pMenuNow;
    RMenu *pMenuNext;

    pMenuNow = RMenu::_activeMenu;
    if(pMenuNow == NULL)
    {
        Q_ASSERT( pMenuNow != NULL );
    }

    pMenuNext = pMenuNow->expandLeaf();
    if ( pMenuNext != pMenuNow )
    {
        pMenuNow->moveToSibling( pMenuNext );
    }
}

RMenu *RControl::queryOptionMenu()
{
    return NULL;
}

void RControl::reTranslate()
{}

void RControl::on_value_changed( int msg )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    if ( !cmdMatch(msg) )
    { return; }

    struServPara para;
    Q_ASSERT( m_pServMenu != NULL );
    serviceExecutor::query( m_pServMenu->getConsoleName(),
                           msg,
                           para
                           );
}

void RControl::on_enable_changed( int msg, bool /*bEn*/ )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    if ( !cmdMatch(msg) )
    {
        return;
    }
}

void RControl::on_enable_changed( int /*msg*/, int /*opt*/, bool /*en*/ )
{}

void RControl::on_visible_changed( int msg, bool /*en*/ )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    if ( !cmdMatch(msg) )
    {
        return;
    }
}

void RControl::on_visible_changed( int /*msg*/, int /*opt*/, bool /*en*/ )
{}

void RControl::on_unit_changed( int /*msg*/, QString /*unit*/ )
{
}
void RControl::on_context_changed( int /*msg*/ )
{}
bool RControl::cmdMatch( int msg )
{
    Q_ASSERT( m_pResMenuItem != NULL );

    //! do not in menu
    if ( msg != m_pResMenuItem->mId )
    { return false; }

    return true;
}

void RControl::focusIn()
{
    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           CMD_SERVICE_ITEM_FOCUS,
                           m_pResMenuItem->mId );
}

bool RControl::tuneFocusIn()
{
    QWidget *pWig;

    //! tune focus control
    if ( RControl::_tuneFocusControl != this )
    {
        //!focus change
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                               CMD_SERVICE_ITEM_FOCUS,
                               m_pResMenuItem->mId );
    }
    else
    //! set tune again
    {
        if ( isTuneAble() && isTuneNow() )
        {
            //!focus change
            serviceExecutor::post( m_pServMenu->getConsoleName(),
                                   CMD_SERVICE_ITEM_FOCUS_AGAIN,
                                   m_pResMenuItem->mId );
        }

        pWig = dynamic_cast<QWidget*>( this );
        Q_ASSERT( NULL != pWig );
        RMenuPage::setTuneWidget( pWig );
        return false;
    }

    //! deactive previous
    if ( _tuneFocusControl != NULL
         && _tuneFocusControl != this )
    {
        pWig = dynamic_cast<QWidget*>( _tuneFocusControl );
        Q_ASSERT( NULL != pWig );
        pWig->update();
    }

    //! active now
    _tuneFocusControl = this;
    pWig = dynamic_cast<QWidget*>( _tuneFocusControl );
    Q_ASSERT( NULL != pWig );
    RMenuPage::setTuneWidget( pWig );
    pWig->update();

    return true;
}

bool RControl::isTuneNow()
{
    QWidget *pWig;
    pWig = dynamic_cast<QWidget*>( this );
    Q_ASSERT( NULL != pWig );

    return RMenuPage::hasTuneFocus(  pWig );
}

void RControl::setUiAttr( int para1, int para2 )
{
    mUiPara1 = para1;
    mUiPara2 = para2;

    //! base attr
    //! text visible
    if ( mUiPara1 & 0x04 )
    {
        setTextVisible( false );
    }

    //! key or value
    if ( mUiPara1 & 0x20 )
    {
        setPara( para_key );
    }

    //! knob view
    if ( mUiPara1 & 0x18 )
    {
        switch( (mUiPara1>>3)&0x03 )
        {
        case 0:
            setKnobView( control_knob_tune_pad );
            break;

        case 1:
            setKnobView( control_knob_tune_z );
            break;

        case 2:
            setKnobView( control_knob_tune );
            break;

        case 3:
            setKnobView( control_knob_pad );
            break;

        default:
            break;
        }
    }

    //! read only
    if ( mUiPara1 & 0x80 )
    {
        mbReadOnly = true;
    }

}

/*!
 * \brief RControl::alignValue
 * \param val
 * \param step
 * \return
 * 将数值按指定的步进进行对齐
 */
int RControl::alignValue( int val, qlonglong step,bool add_or_sub )
{
    if ( step == 0 ) return val;
    if( !add_or_sub )
    {
        step = -step;
    }

    return ( (val+step/2) / step) * step;
}

int RControl::alignValue( ui_attr::PackInt &now, qlonglong step,bool add_or_sub )
{    
    if ( step == 0 )
    {
        Q_ASSERT( step != 0 );
        return 0;
    }

    if( !add_or_sub )
    {
        step = -step;
    }
    //find nearnest coarse scale.by hxh
    if ( now.width == 32 )
    {
        //now.val32 = (now.val32/step) * step;
        if( (now.val32 % step) != 0 )
        {
            now.val32 = ((now.val32 + step/2) / step ) * step;
        }
    }
    else if ( now.width == 64 )
    {
        //now.val64 = (now.val64/step) * step;
        if( (now.val64 % step) != 0 )
        {
            now.val64 = ((now.val64 + step/2) / step ) * step;
        }
    }
    else if ( now.width == 128 )
    {
        //now.val128.mA = (now.val128.mA/step) * step;
        if( (now.val128.mA % step) != 0 )
        {
            now.val128.mA = ((now.val128.mA + step/2) / step ) * step;
        }
    }
    else
    {
        //Q_ASSERT( false );
    }

    return 0;
}

bool RControl::canHelp()
{
    Q_ASSERT( NULL != m_pResMenuItem );
    eMenuControl control;

    //! 非功能元素
    control = m_pResMenuItem->m_eType;
    if ( RControl::_noHelpControls.contains(control) )
    {
        return false;
    }

    //! 发向help的消息不进行过滤
//    if ( m_pServMenu->getConsoleName()
//         == service::getServiceName( E_SERVICE_ID_HELP ) )
//    {
//        if(m_pResMenuItem->mId != MSG_APP_HELP_HELP)
//        {
//            return false;
//        }
//    }

    return true;
}

bool RControl::canBypass()
{
    Q_ASSERT( NULL != m_pResMenuItem );

    int id;
    id = m_pResMenuItem->mId;
    if ( is_ui_msg(id) )
    { return true; }

    return false;
}

bool RControl::doFilter()
{
    //! in work mode
    if ( sysGetWorkMode() == work_help )
    {
        //! can help
        if ( canHelp() )
        {
            Q_ASSERT( m_pResMenuItem );
            Q_ASSERT( m_pServMenu );

            //! post to help service
            CHelpRequest *pReq;
            if(m_pResMenuItem->mId == MSG_APP_HELP_LANGUAGE)
            {
                QString name = serv_name_utility;
                int     msg =  MSG_APP_UTILITY_LANGUAGE;
                pReq = new CHelpRequest( name, msg );
            }
            else
            {
                pReq = new CHelpRequest( m_pServMenu->getConsoleName(),
                                         m_pResMenuItem->mId );
            }
            Q_ASSERT( NULL != pReq );

            //! option value
            if ( hasOption() )
            {
                int val;
                m_pResMenuItem->getValue( val );
                pReq->setOption( val );
            }

            //! help msg request
            serviceExecutor::post( (int)E_SERVICE_ID_HELP,
                                   CMD_SERVICE_HELP_REQUESST,
                                   pReq );

            if ( canBypass() )
            { return true; }
            else
            { return false; }
        }
    }

    //! no set action
    if ( mbReadOnly ) return true;

    return false;
}

/*!
 * \brief RControl::hasOption
 * \return
 * 控件含有选择项目
 */
bool RControl::hasOption()
{
    if ( mContType == Combox_Func
         || mContType == Combox
         || mContType == LiteCombox
         || mContType == Button_Two)
    { return true; }
    else
    { return false; }
}

void RControl::doCmd( int val, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu !=NULL );
    Q_ASSERT( m_pResMenuItem !=NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           val
                           );
}

void RControl::doCmd( qlonglong val, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu !=NULL );
    Q_ASSERT( m_pResMenuItem !=NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           val
                           );
}

void RControl::doCmd( ui_attr::PackInt val,
                      eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu !=NULL );
    Q_ASSERT( m_pResMenuItem !=NULL );
    Q_ASSERT( val.width == 32
              || val.width == 64
              || val.width == 128 );

    if ( doFilter() ) return;

    if ( val.width == 32 )
    {
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           val.val32);
    }
    else if ( val.width == 64 )
    {
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           val.val64);
    }
    else if ( val.width == 128 )
    {
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           val.val128);
    }
    else
    {
        Q_ASSERT( false );
    }
}

void RControl::doCmd( bool val, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId  + ( subAttr - combox_raw),
                           val
                           );
}

void RControl::doCmd( const QString &val, eComboxSubAttr attr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId  + ( attr - combox_raw),
                           val
                           );
}

void RControl::doCmd( dsoReal &val, eComboxSubAttr attr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId  + ( attr - combox_raw),
                           val
                           );
}

void RControl::doCmd( int msg, int val )
{
    Q_ASSERT( m_pServMenu != NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           msg,
                           val
                           );
}
void RControl::doCmd( int msg, qlonglong val )
{
    Q_ASSERT( m_pServMenu != NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           msg,
                           val
                           );
}

void RControl::doCmd( int msg, dsoReal &val )
{
    Q_ASSERT( m_pServMenu != NULL );

    if ( doFilter() ) return;

    serviceExecutor::post( m_pServMenu->getConsoleName(),
                           msg,
                           val
                           );
}

void RControl::doCmd( int msg, ui_attr::PackInt packVal )
{
    if ( doFilter() ) return;
    if ( packVal.width == 32 )
    { doCmd( msg, packVal.val32); }
    else if ( packVal.width == 64 )
    { doCmd( msg, packVal.val64); }
    else if ( packVal.width == 128 )
    { doCmd( msg, packVal.val128); }
    else
    { Q_ASSERT(false); }
}

void RControl::doZCmd()
{
    ui_attr::uiProperty * prop;
    ui_attr::PackInt zVal;
    prop = queryUiProperty( combox_raw );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_z_mask ) )
    {
        zVal = prop->getZVal();

        if ( prop->isXxxValid( ui_attr::e_z_msg) )
        {
            doCmd( prop->getZMsg(), zVal );
        }
        else
        {
            doCmd( m_pResMenuItem->mId, zVal );
        }
    }
    else
    {
        qWarning()<<"no z cmd";
    }
}

//! no value needed
bool RControl::queryCmd( eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    struServPara para;
    serviceExecutor::query( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId  + ( subAttr - combox_raw),
                           para
                           );

    return true;
}

bool RControl::queryCmd( bool &val, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    struServPara para;
    serviceExecutor::query( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           para
                           );

    if ( para[0].vType == val_bool )
    { val = para[0].bVal; }
    else if ( para[0].vType == val_int )
    { val = para[0].iVal; }
    else
    {
        LOG_DBG()<<m_pServMenu->getConsoleName()<<m_pResMenuItem->mId;
        Q_ASSERT( false );
    }

    return true;
}

bool RControl::queryCmd( int &val, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    struServPara para;
    serviceExecutor::query( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId  + ( subAttr - combox_raw),
                           para
                           );

    if ( para[0].vType == val_bool )
    { val = para[0].bVal; }
    else if ( para[0].vType == val_int )
    { val = para[0].iVal; }
    else
    {
        LOG_DBG()<<m_pServMenu->getConsoleName()<<m_pResMenuItem->mId;
        Q_ASSERT( false );
    }

    return true;
}

bool RControl::queryCmd( ui_attr::PackInt &val, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pResMenuItem != NULL );
    return queryCmd( m_pResMenuItem->mId, val, subAttr );
}

bool RControl::queryCmd( float &val, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    struServPara para;
    serviceExecutor::query( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           para
                           );

    if ( para[0].vType != val_float )
    { LOG_DBG()<<m_pServMenu->getConsoleName()<<m_pResMenuItem->mId; }
    Q_ASSERT( para[0].vType == val_float );

    val = para[0].fVal;

    return true;
}

bool RControl::queryCmd( QString &str, eComboxSubAttr subAttr )
{
    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );

    struServPara para;
    serviceExecutor::query( m_pServMenu->getConsoleName(),
                           m_pResMenuItem->mId + ( subAttr - combox_raw),
                           para
                           );
    Q_ASSERT( para[0].vType == val_string );

    str = para[0].strVal;

    return true;
}

bool RControl::queryCmd( int msg,
                         ui_attr::PackInt &val,
                         eComboxSubAttr attr )
{
    Q_ASSERT( m_pServMenu != NULL );

    struServPara para;
    DsoErr err;
    err = serviceExecutor::query( m_pServMenu->getConsoleName(),
                           msg + (attr-combox_raw),
                           para
                           );
    if ( err != ERR_NONE )
    {
        return false;
    }

    if ( para[0].vType == val_longlong )
    {
        val.val64 = para[0].llVal;
        val.width = 64;
    }
    else if ( para[0].vType == val_int )
    {
        val.val32 = para[0].iVal;
        val.width = 32;

        val.val64 = para[0].iVal;
        val.width = 32;
    }
    else if ( para[0].vType == val_real )
    {
        val.width = 128;
        val.val128 = para[0].realVal;
    }
    else
    {
        LOG_DBG()<< m_pServMenu->getConsoleName()<<msg<< __FUNCTION__ << __LINE__;
        Q_ASSERT( false );
    }

    return true;
}

bool RControl::scanLau( bool &val, eComboxSubAttr attr )
{
    CArgument para;
    DsoErr err;

    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );
    err = serviceExecutor::scanLau( m_pServMenu->getConsoleName(),
                              m_pResMenuItem->mId + (attr-combox_raw),
                              para );
    if ( err != ERR_NONE )
    { return false; }

    //! match
    if ( para.size() < 1 )
    { return false; }

    //! match
    if ( para[0].vType != val_bool )
    { return false; }

    val = para[0].bVal;

    return true;
}
bool RControl::scanLau( int &val, eComboxSubAttr attr )
{
    CArgument para;
    DsoErr err;

    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );
    err = serviceExecutor::scanLau( m_pServMenu->getConsoleName(),
                              m_pResMenuItem->mId + (attr-combox_raw),
                              para );
    if ( err != ERR_NONE )
    { return false; }

    //! match
    if ( para.size() < 1 )
    { return false; }

    //! match
    if ( para[0].vType != val_int )
    { return false; }

    val = para[0].iVal;

    return true;
}

bool RControl::scanLau(
                        ui_attr::PackInt &val,
                        eComboxSubAttr attr )
{
    CArgument para;
    DsoErr err;

    Q_ASSERT( m_pServMenu != NULL );
    Q_ASSERT( m_pResMenuItem != NULL );
    err = serviceExecutor::scanLau( m_pServMenu->getConsoleName(),
                              m_pResMenuItem->mId + (attr-combox_raw),
                              para );
    if ( err != ERR_NONE )
    { return false; }

    //! match
    if ( para.size() < 1 )
    { return false; }

    //! deparse
    do
    {
        if ( para[0].vType == val_int )
        {
            val.val32 = para[0].iVal;
            val.width = 32;

            val.val64 = para[0].iVal;
            val.width = 32;

            return true;
        }

        if ( para[0].vType == val_longlong )
        {
            val.val64 = para[0].llVal;
            val.width = 64;

            return true;
        }

        if ( para[0].vType == val_real )
        {
            val.width = 128;
            val.val128 = para[0].realVal;

            return true;
        }

    }while( 0 );

    return false;
}

RMenu *RControl::getOptionMenu( int val )
{
    CResItem* pOptItem;

    Q_ASSERT( m_pResMenuItem != NULL );
    Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );
    foreach( pOptItem, m_pResMenuItem->m_pResItem->mOption )
    {
        Q_ASSERT( pOptItem != NULL );

        //! option value match
        if ( pOptItem->value == val )
        {
            return (RMenu *)pOptItem->m_pOptionMenu;
        }
    }

    return NULL;
}

ui_attr::uiProperty *RControl::queryUiProperty( eComboxSubAttr subAttr,
                                                int msg )
{
    Q_ASSERT( NULL != m_pResMenuItem );
    Q_ASSERT( NULL != m_pServMenu );

    //! default val
    if ( msg == 0 )
    { msg = m_pResMenuItem->mId; }

    return serviceExecutor::queryUiProperty( m_pServMenu->getConsoleName(),
                                  msg  + ( subAttr - combox_raw) );
}
/*!
 * \brief RControl::stickProc
 * \param val
 * \param valPre
 * \param subAttr
 * 粘滞处理
 */
void RControl::stickProc( ui_attr::PackInt &val,
                          ui_attr::PackInt &valPre,
                          eComboxSubAttr subAttr,
                          int msg )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr, msg );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_stick_mask) )
    {
        QList< ui_attr::PackInt>::iterator iter;

        //! up
        if ( val > valPre )
        {
            for( iter = prop->mStick.end() - 1;
                 iter != prop->mStick.begin();
                 iter-- )
            {
                if ( val > *iter && *iter > valPre )
                {
                    val = *iter;
                    stickOn();
                    return;
                }
            }
        }
        //! down
        else if ( val < valPre )
        {
            for( iter = prop->mStick.begin();
                 iter != prop->mStick.end();
                 iter++ )
            {
                if ( val < *iter && *iter < valPre )
                {
                    val = *iter;
                    stickOn();
                    return;
                }
            }
        }
        else
        {
            return;
        }
    }
    else
    //! default stick to 0
    {
        if ( val > 0 && valPre < 0 )
        {
            val = 0;
            stickOn();
        }
        else if ( val < 0 && valPre > 0 )
        {
            val = 0;
            stickOn();
        }
        else
        {}
    }
}
/*!
 * \brief RControl::stickOn
 * 粘滞开启
 */
void RControl::stickOn()
{
    _cpSticker->stickOn();
}
/*!
 * \brief RControl::uiEventFilter
 * \return
 * 粘滞过滤是否有效
 */
bool RControl::uiEventFilter()
{
    return !_cpSticker->isTimeout();
}

qlonglong RControl::getUiStep( eComboxSubAttr subAttr,
                               int dir )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop  )
    {
        if ( dir > 0 )
        {
            if ( prop->isXxxValid( ui_attr::e_i_step_mask ) )
            { return prop->getIStep(); }

            if ( prop->isXxxValid( ui_attr::e_d_step_mask ) )
            { return prop->getDStep(); }

            { return 1; }
        }
        else if ( dir < 0 )
        {
            if ( prop->isXxxValid( ui_attr::e_d_step_mask ) )
            { return prop->getDStep(); }

            if ( prop->isXxxValid( ui_attr::e_i_step_mask ) )
            { return prop->getIStep(); }

            return 1;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}
ui_attr::PackInt RControl::getUiZVal( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_z_mask ) )
    {
        return prop->getZVal();
    }
    else
    {
        ui_attr::PackInt packVal;
        return packVal;
    }
}

ui_attr::PackInt RControl::getUiMax( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_max_mask ) )
    {
        return prop->getMaxVal();
    }
    else
    {
        ui_attr::PackInt packVal;
        return packVal;
    }
}
ui_attr::PackInt RControl::getUiMin( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_min_mask ) )
    {
        return prop->getMinVal();
    }
    else
    {
        ui_attr::PackInt packVal;
        return packVal;
    }
}

bool RControl::getUiFgColor( QColor &color, eComboxSubAttr attr )
{
    ui_attr::uiProperty *prop;

    prop = queryUiProperty( attr );
    if ( NULL != prop && prop->isXxxValid(ui_attr::e_fg_color) )
    {
        color = prop->getFgColor();
        return true;
    }
    else
    { return false; }
}
bool RControl::getUiBgColor( QColor &color, eComboxSubAttr attr )
{
    ui_attr::uiProperty *prop;

    prop = queryUiProperty( attr );
    if ( NULL != prop && prop->isXxxValid(ui_attr::e_fg_color) )
    {
        color = prop->getBgColor();
        return true;
    }
    else
    { return false; }
}

DsoType::Unit RControl::getUiUnit( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty *prop;
    int attr;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid(ui_attr::e_unit_mask) )
    {
        return prop->getUnit();
    }

    attr = (mUiPara1 >> 16) & 0x1f;
    if ( attr != 0 )
    {
        return (Unit)attr;
    }
    else
    {
        return DsoType::Unit_none;
    }
}
DsoType::DsoViewFmt RControl::getUiViewFmt( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty *prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid(ui_attr::e_format_mask) )
    {
        return prop->getViewFmt();
    }
    else
    {
        return fmt_def;
    }
}

key_acc::KeyAcc RControl::getUiAcc( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_acc_mask ) )
    {
        return prop->getAcc();
    }
    else
    {
        return key_acc::e_key_e;
    }
}

double RControl::getUiBase( eComboxSubAttr subAttr )
{
    int attr;
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_base_mask ) )
    {
        return prop->getBase();
    }

    attr = (mUiPara1>>21) & 0xf;
    if ( attr != 0 )
    {
        return pow(10,-attr);
    }
    else
    {
        return 0.001;
    }
}

DsoReal RControl::getUiRealBase( eComboxSubAttr subAttr )
{
    int attr;
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_base_mask ) )
    {
        return prop->getRealBase();
    }

    attr = (mUiPara1 >> 21)&0x0f;
    if ( attr != 0 )
    {
        return DsoReal( 1, (DsoE)((int)E_0 - attr) );
    }
    else
    {
        return DsoReal( 1, E_0 );
    }
}

bool RControl::hasUiBase( eComboxSubAttr subAttr )
{
    int attr;
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_base_mask ) )
    {
        return true;
    }

    attr = (mUiPara1 >> 21)&0x0f;
    if ( attr != 0 )
    { return true;}

    return false;
}

QString RControl::getUiPreStr( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_prestr_mask ) )
    {
        return prop->getPreStr();
    }
    else
    {
        return "";
    }
}

QString RControl::getUiPostStr( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;
    int attr;

    prop = queryUiProperty( subAttr );

    //! set by user
    if ( NULL != prop && prop->isXxxValid( ui_attr::e_poststr_mask ) )
    {
        return prop->getPostStr();
    }

    //! base unit
    if ( NULL != prop && prop->isXxxValid(ui_attr::e_unit_mask) )
    {
        QString str;

        str = gui_fmt::CGuiFormatter::toString( prop->getUnit() );
        return str;
    }

    //! ui unit by para
    attr = (mUiPara1 >> 16) & 0x1f;
    if ( attr != 0 )
    {
        Unit unit = (Unit)attr;
        QString str;

        str = gui_fmt::CGuiFormatter::toString( unit );
        return str;
    }
    else
    {
        return "";
    }
}

QString& RControl::getUiOutStr( bool &bStr, eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_str_out_mask ) )
    {
        bStr = true;
        return prop->getOutStr();
    }
    else
    {
        bStr = false;
        return RControl::nullString;
    }
}

bool RControl::getUiNodeCompress( eComboxSubAttr subAttr )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( subAttr );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_node_compress ) )
    {
        return prop->getNodeCompress();
    }
    else
    {
        return false;
    }
}

/*!
 * \brief RControl::getUiMaxLength
 * \return
 * 如果没有设置则返回-1
 */
int RControl::getUiMaxLength()
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_max_text_length ) )
    {
        return prop->getMaxLength();
    }
    else
    {
        return -1;
    }
}
bool RControl::getUiNoCache( )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( );

    if ( NULL != prop && prop->isXxxValid( ui_attr::e_no_cache ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool RControl::uiKeyRequest( int keyCnt )
{
    ui_attr::uiProperty * prop;

    prop = queryUiProperty( );
    if ( NULL != prop &&
         prop->isXxxValid( ui_attr::e_key_request) )
    {
        //! stick filter
        if ( uiEventFilter() )
        { return true; }

        CKeyRequest *pKeyReq;

        pKeyReq = prop->getKeyRequest();
        Q_ASSERT( NULL != pKeyReq );

        ui_attr::PackInt packPre, packNow;

        if ( pKeyReq->m_ikeyProc != NULL )
        {
            int iVal;

            //! query val
            queryCmd( pKeyReq->mMsg, packPre );

            iVal = pKeyReq->m_ikeyProc( keyCnt,
                                        pKeyReq->m_pContext);
            packNow.set( iVal );

            stickProc( packNow, packPre, combox_raw, pKeyReq->mMsg );

            doCmd( pKeyReq->mMsg, packNow.val32 );
        }
        else if ( pKeyReq->m_llkeyProc != NULL )
        {
            qlonglong llVal;

            //! query val
            queryCmd( pKeyReq->mMsg, packPre );

            llVal = pKeyReq->m_llkeyProc( keyCnt,
                                          pKeyReq->m_pContext);
            packNow.set( llVal );

            stickProc( packNow, packPre, combox_raw, pKeyReq->mMsg );

            //! add_by_zx::for bug3081 慢扫描调节档位时忽大忽小
            if(  sysGetStarted() &&
                 ( pKeyReq->mMsg == MSG_HORI_MAIN_SCALE ||
                 pKeyReq->mMsg == MSG_HORI_ZOOM_SCALE ||
               pKeyReq->mMsg == MSG_HORI_MAIN_OFFSET||
               pKeyReq->mMsg == MSG_HORI_ZOOM_OFFSET ) )
            {
                serviceExecutor::send( m_pServMenu->getConsoleName(),
                                       pKeyReq->mMsg, packNow.val64 );
            }
            else
            {
                doCmd( pKeyReq->mMsg, packNow.val64 );
            }
        }
        else
        { Q_ASSERT(false);}

        return true;
    }
    else
    { return false; }
}

void RControl::keyIncDecProc( ui_attr::PackInt &val,
                              bool bInc,
                              int count,
                              eComboxSubAttr subAttr )
{
    key_acc::KeyAcc acc;
    qlonglong keyAccCnt, step;
    ui_attr::PackInt valPre;

    valPre = val;

    acc = RControl::getUiAcc( subAttr );
    step = RControl::getUiStep( subAttr, bInc ? 1 : -1 );

    alignValue( val, step , bInc);

    keyAccCnt = key_acc::CKeyAcc::acc( acc, count );

    valPre = val;

    if ( bInc )
    {
        val = val + keyAccCnt * step;
        stickProc( val, valPre, subAttr );
    }
    else // dec
    {
        val = val - keyAccCnt * step;
        stickProc( val, valPre );
    }
}

void RControl::keyEventProc(
                   QKeyEvent *event,
                   eComboxSubAttr attr )
{
    key_acc::KeyAcc acc;
    int keyAccCnt;
    ui_attr::PackInt zVal;

    //! get ui attr
    queryCmd( attr );

    acc = RControl::getUiAcc();
    keyAccCnt = event->count();

    keyAccCnt = key_acc::CKeyAcc::acc( acc, keyAccCnt );

    if ( event->key() == R_Skey_TuneInc )
    {
        doCmd( keyAccCnt );
    }
    else if ( event->key() == R_Skey_TuneDec )
    {
        doCmd( -keyAccCnt );
    }
    else if ( event->key() == R_Skey_TuneEnter )
    {
        zVal = getUiZVal();
        doCmd( zVal );         /*!< z proc */
    }
    else if ( event->key() == R_Skey_Zval )
    {
        zVal = getUiZVal();
        doCmd( zVal );         /*!< z proc */
    }
    else if ( event->key() == R_Skey_Active )
    {

    }
    else
    {
        return;
    }
}

void RControl::buildKeypadContext( CKeyPadContext &keyPadContext,
                                   ui_attr::PackInt &valNow,
                                   eComboxSubAttr attr
                                   )
{
    keyPadContext.setView( getUiUnit( attr ) );

    ui_attr::PackInt integerMax, integerMin, integerDef;
    integerMax = getUiMax( attr );
    integerMin = getUiMin( attr );
    integerDef = getUiZVal( attr );

    DsoReal realMax, realMin, realNow, realDef;
    DsoReal realBase;

    //! real base
    realBase = getUiRealBase( attr );

    //! e
    if ( !integerMax.isUnk() )
    {
        realMax.setVal( integerMax.val64*realBase.mA, realBase.mE );
    }
    if ( !integerMin.isUnk() )
    {
        realMin.setVal( integerMin.val64*realBase.mA, realBase.mE );
    }
    if ( !integerDef.isUnk() )
    {
        realDef.setVal( integerDef.val64*realBase.mA, realBase.mE );
    }

    Q_ASSERT( valNow.width > 0 );
    realNow.setVal( valNow.val64 * realBase.mA, realBase.mE );

    keyPadContext.mValueGp.setValue(
                realNow,
                realMax,
                realMin,
                realDef
                );
}

void RControl::formatValue( ui_attr::PackInt &val,
                  eControlFmt cType,
                  const DsoReal &real,
                  QString &strOut,
                  DsoViewFmt fmt )
{
    if ( cType == control_fmt_fract )
    {
        if ( val.width == 32 || val.width == 64 )
        {
            DsoReal realVal;

            if ( val.width == 32 )
            {
                realVal.setVal( (DsoRealType_A)val.val32 );
            }
            else
            {
                realVal.setVal( val.val64 );
            }

            realVal = realVal * real;

            gui_fmt::CGuiFormatter::format(strOut, realVal, fmt );
        }
        else if ( val.width == 128 )
        {
            gui_fmt::CGuiFormatter::format(strOut, val.val128, fmt );
        }
        else
        {
            LOG_DBG()<<m_pResMenuItem->mId;
            Q_ASSERT(false);
        }
    }
    else if ( cType == control_fmt_integer )
    {
        if ( val.width == 32 )
        {
            gui_fmt::CGuiFormatter::format(strOut, val.val32, fmt );
        }
        else if ( val.width == 64 )
        {
            gui_fmt::CGuiFormatter::format(strOut, val.val64, fmt );
        }
        else if ( val.width == 128 )
        {
            gui_fmt::CGuiFormatter::format(strOut, val.val128, fmt );
        }
        else
        {
            LOG_DBG()<<m_pResMenuItem->mId;
            Q_ASSERT(false);
        }
    }
    else
    {
        LOG_DBG()<<m_pResMenuItem->mId;
        Q_ASSERT( false );
    }

}

}
