#include "rpopupitem.h"

namespace menu_res {

RPopListItem::RPopListItem()
{
    m_pResItem = NULL;

    mEnable = true;
    mVisible = true;
    mCheckable = false;
    mChecked = true;
}

void RPopListItem::setResItem( CResItem * pResItem )
{
    Q_ASSERT( NULL != pResItem );
    m_pResItem = pResItem;
}
int RPopListItem::getValue()
{
    return m_pResItem->value;
}

void RPopListItem::setEnable( bool b )
{
    mEnable = b;
}
bool RPopListItem::getEnable()
{
    return mEnable;
}

void RPopListItem::setVisible( bool b )
{
    mVisible = b;
}
bool RPopListItem::getVisible()
{
    return mVisible;
}

void RPopListItem::setCheckable( bool b )
{
    mCheckable = b;
}
bool RPopListItem::getCheckable()
{
    return mCheckable;
}

void RPopListItem::setChecked( bool b )
{
    mChecked = b;
}
bool RPopListItem::getChecked()
{
    return mChecked;
}

RPopListModal::RPopListModal()
{
    mCurrentIndex = -1;
}

void RPopListModal::setList( QList< RPopListItem *> *pList )
{
    Q_ASSERT( NULL != pList );

    RPopListItem *pItem;

    mList.clear();

    foreach( pItem, *pList )
    {
        Q_ASSERT( pItem != NULL );

        if ( pItem->getVisible() )
        {
            mList.append( pItem );
        }
    }

    //! at least one item
    Q_ASSERT( mList.size() > 0 );
}

void RPopListModal::selectOption( int optValue )
{
    RPopListItem *pItem;
    int id;

    id = 0;
    foreach( pItem, mList )
    {
        Q_ASSERT( pItem != NULL );
        Q_ASSERT( pItem->m_pResItem );

        if ( pItem->m_pResItem->value == optValue )
        {
            mCurrentIndex = id;
            return;
        }

        id++;
    }

    //! 用户修改了项目数值后，可能在列表中找不到对应的项目
    //! \todo 移动到最近的位置上
    mCurrentIndex = 0;
}

void RPopListModal::setCurrent( int index )
{
    mCurrentIndex = index;
}

void RPopListModal::setTitle( const QString &title )
{
    mTitle = title;
}

int RPopListModal::size()
{
    return mList.size();
}

}
