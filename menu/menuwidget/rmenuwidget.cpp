#include "rmenuwidget.h"
#include "../arch/rmenupage.h"
namespace menu_res{

RMenuWidget::RMenuWidget( QWidget *parent ) : QPushButton( parent )
{
    mbFocusAble = true;
    mbTuneAble = false;

    connect( this, SIGNAL(clicked(bool)),
             this, SLOT(on_clicked()) );
}

QSize RMenuWidget::sizeHint() const
{
    return RControl::sizeHint();
}

bool RMenuWidget::hitButton( const QPoint & pos) const
{
    return contentsRect().contains(pos);
}

void RMenuWidget::keyPressEvent(QKeyEvent * event)
{
    Q_ASSERT( NULL != event );

    if ( event->key() == R_Skey_Active )
    {
        setDown(true);
    }
}
void RMenuWidget::keyReleaseEvent(QKeyEvent * event)
{
    Q_ASSERT( NULL != event );

    int key, keyCnt;

    key = event->key();
    keyCnt = event->count();

    switch( key )
    {
    case R_Skey_Active:
        setDown(false);

        //! 软按键释放
        { emit clicked(); }
        break;

    case R_Skey_TuneInc:
        if ( doFilter() ) return;
        tuneInc( keyCnt );
        break;

    case R_Skey_TuneDec:
        if ( doFilter() ) return;
        tuneDec( keyCnt );
        break;

    case R_Skey_TuneEnter:
        if ( doFilter() ) return;
        tuneClick();
        break;

    case R_Skey_Zval:
        if ( doFilter() ) return;
        zEnter();
        break;

    case R_Skey_Next:
        if ( doFilter() ) return;
        stepNext();
        break;

    default:
        return;
    }
}

void RMenuWidget::on_enable_changed( int msg, bool b )
{
    if ( !cmdMatch(msg) ) return;

    setEnabled(b);

}
void RMenuWidget::on_visible_changed( int msg, bool b )
{
    if ( !cmdMatch(msg) ) return;

    setVisible(b);

}

void RMenuWidget::onClick()
{
    //! tune focus
    if ( isTuneAble() && isVisible() && isEnabled() )
    {
        Q_ASSERT( parentWidget() != NULL );

        RMenuPage *pPage;
        pPage = dynamic_cast< RMenuPage*>( parentWidget() );
        Q_ASSERT( NULL != pPage );
        pPage->toTop();

        pPage->update();
    }
}

void RMenuWidget::tuneInc( int /*cnt*/ )
{}
void RMenuWidget::tuneDec( int /*cnt*/ )
{}
void RMenuWidget::tuneClick()
{}

void RMenuWidget::zEnter()
{   LOG_DBG();doZCmd(); }

void RMenuWidget::stepNext()
{}

void RMenuWidget::on_clicked()
{
    //! ui action
    setDown(false);

    //! filter
    if ( doFilter() ) return;

    if ( mbTuneAble )
    {
        if ( tuneFocusIn() )
        { return; }
    }

    //! function action
    onClick();
}

/*!
 * \brief RMenuWidget::attachPageWidget
 * \param pWidget
 * 页面部件有：
 * - 菜单部件
 * - 菜单部件调出的输入部件
 * 当页面消失时，页面部件也需要隐藏
 */
void RMenuWidget::attachPageWidget( QWidget *pWidget )
{
    QObject *par;
    RMenuPage *page;

    par = parent();
    Q_ASSERT( par != NULL );

    page = dynamic_cast<RMenuPage*>( par );
    Q_ASSERT( page != NULL );

    page->attachWidget( pWidget );
}

}
