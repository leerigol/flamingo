#ifndef RSEEKBAR_H
#define RSEEKBAR_H

#include "../menuwidget/rcontrol.h"
#include "../menustyle/rcontrolstyle.h"

#include "../../widget/rfuncknob.h"

#include "rmenuwidget.h"

namespace menu_res {

/*!
 * \brief The RSeekBar class
 * - 滑块控件
 * - \todo drag
 */
class RSeekBar : public RMenuWidget
{
    Q_OBJECT

private:
    static RSeekBarStyle _style;

public:
    RSeekBar( QWidget *parent = NULL );

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    virtual void on_value_changed( int msg );
protected:
    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );
    virtual void tuneClick();

    void tuneEdit( int keyCnt, bool bInc );

protected Q_SLOTS:
    void onTimeout();

public:
    void setRange( ui_attr::PackInt from, ui_attr::PackInt to );
    void getRange( ui_attr::PackInt &from, ui_attr::PackInt &to );

    void setVal( ui_attr::PackInt val );
    ui_attr::PackInt getVal();

    void setPreStr( const QString &str );
    QString getPreStr();

    void setPostStr( const QString &str );
    QString getPostStr();
private:
    void startTimer();
private:
    RFuncKnob *m_pKnob;

    ui_attr::PackInt mVal;
    ui_attr::PackInt mMin;
    ui_attr::PackInt mMax;

    QString mPreStr, mPostStr;

    QTimer *mpTimer;
    bool mbBalloonVisible;
};

}

#endif // RSEEKBAR_H
