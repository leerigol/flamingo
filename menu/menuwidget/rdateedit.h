#ifndef RDATEEDIT_H
#define RDATEEDIT_H

#include "../menuwidget/rcontrol.h"
#include "../menustyle/rcontrolstyle.h"

#include "../../widget/rfuncknob.h"

#include "rtimeedit.h"

namespace menu_res {

class RDateEdit : public RSegEdit
{
    Q_OBJECT

public:
    static bool isLeap( int year );

private:
    static RDateEditStyle _style;

public:
    RDateEdit( QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    virtual void on_value_changed( int msg );
protected:
    virtual void tuneInc( int cnt );
    virtual void tuneDec( int cnt );

public:
    void setYear( int year );
    int getYear();

    void setMonth( int mon );
    int getMonth();

    void setDay( int day );
    int getDay();

    void setDate( int y, int m, int d );

private:
    int packValue();
    void focusEdit( int keyCnt, bool bInc );

private:
    int mYear;
    int mMonth;
    int mDay;
};

}

#endif // RDATEEDIT_H
