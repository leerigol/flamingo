#include "rseekbar.h"

namespace menu_res {

RSeekBarStyle RSeekBar::_style;

RSeekBar::RSeekBar( QWidget *parent) : RMenuWidget( parent )
{
    mMin.set( 0 );
    mMax.set( 100 );
    mVal.set( 10 );

    _style.loadResource("ui/menu/seek_bar/" );
    _style.resize( this );

    m_pKnob = new RFuncKnob(this);
    Q_ASSERT( NULL != m_pKnob );
    m_pKnob->move( _style.mKnobXY );
    m_pKnob->setView( control_knob_tune_z );

    mbTuneAble = true;

    mpTimer = new QTimer(this);
    Q_ASSERT( NULL != mpTimer );
    mbBalloonVisible = false;

    mpTimer->setSingleShot( true );

    connect( mpTimer, SIGNAL(timeout()),
             this, SLOT(onTimeout()) );
}

void RSeekBar::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( event != NULL );
    RPainter painter(this);

    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }
    _style.paintEvent( painter );

    do
    {
        Q_ASSERT( m_pResMenuItem != NULL );
        Q_ASSERT( m_pResMenuItem->m_pResItem != NULL );

        //! fg
        eMenuRes resType;
        QRect boundRect;

        resType = m_pResMenuItem->m_pResItem->getType();
        boundRect = _style.mCaptionRect;

        //! drawIcon
        if ( is_attr(resType, MR_Icon) )
        {
            QImage img;
            int v = m_pResMenuItem->m_pResItem->getImage( img );
            Q_ASSERT(v  == 0 );

            _style.drawIcon( painter,
                             img,
                             boundRect );
            boundRect.adjust( img.width(), 0, 0, 0 );
        }

        //! draw text
        if ( is_attr(resType, MR_Str) )
        {
            _style.drawCaption( painter,
                                boundRect,
                                m_pResMenuItem->getTitle() );
        }
    }while(0);

    float fBase;
    QString preStr, postStr;

    fBase = getUiBase();
    preStr = getPreStr();
    postStr = getPostStr();

    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), isTuneNow() );
    m_pKnob->setState( tuneStat );

    _style.drawValue( painter,
                      mMin, mMax, mVal,
                      fBase, preStr, postStr,
                      tuneStat,
                      mbBalloonVisible );
}

void RSeekBar::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    queryCmd( mVal );

    mMin = getUiMin();
    mMax = getUiMax();

    update();
}

void RSeekBar::tuneInc( int keyCnt )
{ tuneEdit( keyCnt, true); }
void RSeekBar::tuneDec( int keyCnt )
{ tuneEdit( keyCnt, false); }
void RSeekBar::tuneClick()
{
    ui_attr::PackInt val;

    val = getUiZVal();

    doCmd( val );

    startTimer();
}

void RSeekBar::tuneEdit( int keyCnt, bool bInc )
{
    queryCmd( mVal );

    RControl::keyIncDecProc( mVal, bInc, keyCnt );

    //! do cmd
    doCmd( mVal );

    startTimer();
}

void RSeekBar::onTimeout()
{
    mbBalloonVisible = false;

    update();
}

void RSeekBar::setRange( ui_attr::PackInt from, ui_attr::PackInt to )
{
    mMin = from;
    mMax = to;

    if ( mVal < mMin ) mVal = mMin;
    if ( mVal > mMax ) mVal = mMax;

    update();
}
void RSeekBar::getRange( ui_attr::PackInt &from, ui_attr::PackInt &to )
{
    from = mMin;
    to = mMax;
}

void RSeekBar::setVal( ui_attr::PackInt val )
{
    if ( val < mMin ) val = mMin;
    if ( val > mMax ) val = mMax;

    mVal = val;

    update();
}
ui_attr::PackInt RSeekBar::getVal()
{
    return mVal;
}

void RSeekBar::setPreStr( const QString &str )
{
    mPreStr = str;

    update();
}
QString RSeekBar::getPreStr()
{
    return mPreStr;
}

void RSeekBar::setPostStr( const QString &str )
{
    mPostStr = str;

    update();
}
QString RSeekBar::getPostStr()
{
    return mPostStr;
}

void RSeekBar::startTimer()
{
    mpTimer->setSingleShot( true );
    mbBalloonVisible = true;
    mpTimer->start( _style.mBalloonTmo );
}
}

