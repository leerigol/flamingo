#ifndef RLABEL_I_I_F_H
#define RLABEL_I_I_F_H

#include "../../widget/rfuncknob.h"
#include "../menuwidget/rcontrol.h"
#include "../menustyle/rcontrolstyle.h"
#include "rmenuwidget.h"
namespace menu_res {

/*!
 * \brief The RLabel_i_i_f class
 * set int
 * get int
 * view float
 */
class RLabel_i_i_f : public RMenuWidget
{
    Q_OBJECT
protected:
    static RLabel_i_i_f_Style _style;

public:
    RLabel_i_i_f( QString txt="",
               QWidget *parent = 0 );

public:
    virtual void setTextVisible( bool b );
    virtual void setKnobView( eControlKnob view );

    void setFmt( eControlFmt fmt );
    eControlFmt getFmt();

protected:
    RFuncKnob *m_pKnob;

protected:
    ui_attr::PackInt mVal;
    eControlFmt mContFmt;

protected Q_SLOTS:
    void onInput( DsoReal real );

protected:
    virtual void paintEvent( QPaintEvent */*event*/ );
    virtual void wheelEvent( QWheelEvent *event );
public:
    virtual void on_value_changed( int msg );
protected:
    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );

    virtual void tuneClick();

    virtual void onClick();
public:
    bool cmdMatch( int msg );

protected:
    void keyPadProc( );

    void key125Proc( int key,
                     int accCnt,
                     ui_attr::PackInt &val );
    void keyStepProc( int key,
                      int accCnt,
                      qlonglong step,
                      ui_attr::PackInt &val );

    void keyPower2Proc( int key,
                        int accCnt,
                        qlonglong step,
                        ui_attr::PackInt &val );

    void paraKeyProc( QKeyEvent *event );
    void paraValProc( QKeyEvent *event );

    eComboxSubAttr subValueAttr();
};

}

#endif // RLABEL_I_I_F_H
