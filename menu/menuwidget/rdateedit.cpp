#include "rdateedit.h"

namespace menu_res {

RDateEditStyle RDateEdit::_style;

bool RDateEdit::isLeap( int year )
{
    return QDate::isLeapYear( year );
}

RDateEdit::RDateEdit( QWidget *parent ) : RSegEdit(parent)
{
    mYear = 2970;
    mMonth = 12;
    mDay = 28;

    mFocus = RTimeEditStyle::focus_l0;
    mFocusA = RTimeEditStyle::focus_l0;
    mFocusB = RTimeEditStyle::focus_l2;

    _style.loadResource("ui/menu/date_edit/");
    _style.resize( this );

    m_pKnob->move( _style.mKnobXY );
}

void RDateEdit::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );
    RPainter painter(this);

    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }
    _style.paintEvent( painter );

    do
    {
        if ( m_pResMenuItem == NULL )
        {
            break;
        }

        //! fg
        eMenuRes resType;
        QRect boundRect;

        resType = m_pResMenuItem->m_pResItem->getType();
        boundRect = _style.mCaptionRect;

        //! drawIcon
        if ( is_attr(resType, MR_Icon) )
        {
            QImage img;
            int v = m_pResMenuItem->m_pResItem->getImage( img );
            Q_ASSERT( v == 0 );

            _style.drawIcon( painter,
                             img,
                             boundRect );
            boundRect.adjust( img.width(), 0, 0, 0 );
        }

        //! draw text
        if ( is_attr(resType, MR_Str) )
        {
            _style.drawCaption( painter,
                                boundRect,
                                m_pResMenuItem->getTitle() );
        }
    }while(0);

    //! content
    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), isTuneNow() );
    m_pKnob->setState( tuneStat );

    _style.drawDate( painter, mYear, mMonth, mDay, mFocus, tuneStat );
}

void RDateEdit::on_value_changed( int msg )
{
    if ( !cmdMatch(msg ) ) return;

    int packVal;

    queryCmd( packVal );
    mYear = (packVal >> 16) & 0xffff;
    mMonth = (packVal >> 8) & 0xff;
    mDay = (packVal >> 0) & 0xff;

    update();
}

void RDateEdit::tuneInc( int cnt )
{ focusEdit( cnt, true ); }
void RDateEdit::tuneDec( int cnt )
{ focusEdit( cnt, false); }

void RDateEdit::setYear( int year )
{
    RControl::rangeRoll( year, 2016, 2056 );

    mYear = year;

    //! leap detect
    if ( isLeap( mYear ) )
    {
    }
    else
    {
        if ( mMonth == 2 && mDay >= 29 )
        {
            mDay = 28;
        }
    }

    update();
}
int RDateEdit::getYear()
{
    return mYear;
}

void RDateEdit::setMonth( int mon )
{
    RControl::rangeRoll( mon, 1, 12 );

    mMonth = mon;
    update();
}
int RDateEdit::getMonth()
{
    return mMonth;
}

void RDateEdit::setDay( int day )
{
    int days[]={
        0,
        31,28,31,30,31,30,
        31,31,30,31,30,31
    };

    days[2] += isLeap( mYear ) ? 1 : 0;

    RControl::rangeRoll( day, 1, days[mMonth] );

    mDay = day;

    update();
}
int RDateEdit::getDay()
{
    return mDay;
}

void RDateEdit::setDate( int y, int m, int d )
{
    setYear( y );
    setMonth( m );
    setDay( d );

    update();
}

int RDateEdit::packValue()
{
    return ( ( mYear & 0xffff) << 16 ) |
           ( ( mMonth & 0xff) << 8 )   |
           ( ( mDay & 0xff) << 0 );
}

void RDateEdit::focusEdit( int keyCnt, bool bInc )
{
    int packVal;
    ui_attr::PackInt val;

    queryCmd( packVal );
    mYear = (packVal >> 16) & 0xffff;
    mMonth = (packVal >> 8) & 0xff;
    mDay = (packVal >> 0) & 0xff;

    switch( mFocus )
    {
    case RDateEditStyle::focus_l0:
        val.set( mYear );
        break;

    case RDateEditStyle::focus_l1:
        val.set( mMonth );
        break;

    case RDateEditStyle::focus_l2:
        val.set( mDay );
        break;

    default:
        return;
    }

    RControl::keyIncDecProc( val, bInc, keyCnt );

    switch( mFocus )
    {
    case RDateEditStyle::focus_l0:
        setYear( val.val32 );
        break;

    case RDateEditStyle::focus_l1:
        setMonth( val.val32 );
        break;

    case RDateEditStyle::focus_l2:
        setDay( val.val32 );
        break;

    default:
        return;
    }

    //! do cmd
    doCmd( packValue() );
}

}

