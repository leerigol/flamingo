#ifndef RLABEL_N_S_S_H
#define RLABEL_N_S_S_H

#include "../menuwidget/rcontrol.h"
#include "../menustyle/rcontrolstyle.h"
#include "rmenuwidget.h"
namespace menu_res {

/*!
 * \brief The RLabel_n_s_s class
 * 字符显示控件
 * -no input
 * -get string
 * -view string
 */
class RLabel_n_s_s : public RMenuWidget
{
    Q_OBJECT
protected:
    static RLabel_n_s_s_Style _style;

public:
    RLabel_n_s_s( QString txt="", QWidget *parent = 0 );
protected:
    QString mVal;

    QString mCheckImg;
    QLabel  mCheckLabel;
protected:
    virtual void paintEvent( QPaintEvent *event );
public:
    virtual void on_value_changed( int msg );

protected:
    virtual void onClick();

};
}

#endif // RLABEL_N_S_S_H
