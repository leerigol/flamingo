#ifndef RCOMBOX_IIF_H
#define RCOMBOX_IIF_H

#include "rcombox.h"

#include "../../widget/rfuncknob.h"

namespace menu_res
{
/*!
 * \brief The RCombox_iif class
 * -带数值调节的多选一控件
 * -可以通过旋钮对当前选中的模式进行数值修改
 */
class RCombox_iif : public RCombox
{
    Q_OBJECT
public:
    RCombox_iif( QString txt="",
                 QWidget *parent = 0,
                 QWidget *popParent = 0 );

public:
    virtual void setTextVisible( bool b );
    virtual void setKnobView( eControlKnob view );
    virtual void on_value_changed( int msg );

protected:
    virtual void keyReleaseEvent(QKeyEvent * event);
    virtual void paintEvent( QPaintEvent *event );

protected:
    void paraKeyProc( QKeyEvent * event );
    void paraValProc( QKeyEvent * event );

    virtual void paintFrame( QPainter &painter );
    virtual void tuneClick();

    virtual void onClick();
protected:
    void keyPadProc();

protected Q_SLOTS:
    void onInput( DsoReal real );
    void onHide();

protected:
    ui_attr::PackInt mVal;
    RFuncKnob *m_pKnob;

    //! 数值命令
    //! combox可以融合选项和数值命令
    //! 选项指的是弹出的可列举项目
    //! 数值指的是选项对应数值，例如：弹出频率，周期选项，根据选项设置对应的数值
    //! 所以，需要单独的选项和数值设置指令
    eComboxSubAttr mValueSubCmd;
};

}

#endif // RCOMBOX_IIF_H
