#include "rlabel_icon.h"

namespace menu_res {

QPainterPath RLabel_Icon::_iconPath;

RLabel_icon_Style RLabel_Icon::_style;

RLabel_Icon::RLabel_Icon( QString txt,
             QWidget *parent ) : RLabel_i_i_f( txt, parent )
{
    _style.loadResource("ui/menu/label_icon/");
    _style.resize( this );

    if ( _iconPath.isEmpty() )
    {
        _iconPath.moveTo(0,15);
        _iconPath.lineTo(5,10);
        _iconPath.lineTo(10,15);

        _iconPath.lineTo(5,10);
        _iconPath.arcTo(5,0,20,20,180,270);
    }

    m_pKnob->setView( control_knob_tune );

    m_pKnob->move( _style.mKnobXY );
}

void RLabel_Icon::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);

    QImage img;
    eMenuRes resType;
    QRect boundRect;

    //! bg
    if ( !isEnabled() )
    {   _style.setStatus( element_disabled );   }
    else if ( isDown() )
    {   _style.setStatus( element_actived_down );   }
    else
    {   _style.setStatus( element_deactived ); }

    ElementStatus tuneStat;
    tuneStat = RControlStyle::iconStat( isDown(), isEnabled(), isTuneNow() );

    _style.paintEvent( painter, tuneStat );

    m_pKnob->setState( tuneStat );

    resType = m_pResMenuItem->m_pResItem->getType();
    boundRect = _style.mCaptionRect;

    //! text
    if ( is_attr( resType, MR_Icon ) )
    {
        int v = m_pResMenuItem->m_pResItem->getImage(img);
        Q_ASSERT( 0 == v );

        _style.drawIcon( painter, img, boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );

    }
    if ( is_attr( resType, MR_Str ) )
    {
        _style.drawCaption( painter,
                            boundRect,
                            m_pResMenuItem->getTitle() );
    }
}

void RLabel_Icon::on_value_changed( int /*msg*/ )
{
    //qDebug() << __FUNCTION__<< msg;
    //! 不显示数值，所以没有数值读取过程
}

void RLabel_Icon::tuneInc( int keyCnt )
{ tuneProc( keyCnt, true); }
void RLabel_Icon::tuneDec( int keyCnt )
{ tuneProc( keyCnt, false); }

void RLabel_Icon::tuneClick()
{ doZCmd();}

void RLabel_Icon::tuneProc( int keyCnt, bool bInc )
{
    key_acc::KeyAcc acc;
    int keyAccCnt;

    //! get ui attr
    queryCmd( );

    acc = RControl::getUiAcc();

    keyAccCnt = key_acc::CKeyAcc::acc( acc, keyCnt );

    if ( !bInc )
    { keyAccCnt = -keyAccCnt; }

    doCmd( keyAccCnt );
}

}

