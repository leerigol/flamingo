#ifndef RTEXTBOX_H
#define RTEXTBOX_H

#include "../menuwidget/rcontrol.h"

#include "../menustyle/rtextbox_style.h"

#include "rmenuwidget.h"

namespace menu_res
{
/*!
 * \brief The RTextBox class
 * 菜单上的文本输入框
 */
class RTextBox : public RMenuWidget //public QPushButton, public RControl
{
    Q_OBJECT
protected:
    static RTextBox_Style _style;

public:
    RTextBox( QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event );

    virtual bool eventFilter(QObject *, QEvent *);

public:
    virtual void on_value_changed( int msg );
    virtual void on_enable_changed( int msg, bool b );
protected:
    virtual void onClick();

private:
    void trimText( QString &str );
    int  maxLength();

protected Q_SLOTS:
    void on_ime_completed( const QString &str );

private:
    QString mText;
    QLineEdit *m_pTextEdit;
    QString mShowText;
};

}
#endif // RTEXTBOX_H
