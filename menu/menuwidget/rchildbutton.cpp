#include "rchildbutton.h"

#include "../arch/rmenu.h"
#include "../arch/rservmenu.h"

namespace menu_res {

RChildButtonStyle RChildButton::_style;

RChildButton::RChildButton( QString /*txt*/, QWidget *parent )
             : RMenuWidget( parent )
{
    _style.loadResource("ui/menu/child_button/");
    _style.resize( this );

    m_pChild = NULL;
}

void RChildButton::paintEvent( QPaintEvent */*event*/ )
{
    RPainter painter(this);

    //! bg
    if ( !isEnabled() )
    { _style.setStatus( element_disabled ); }
    else if ( isDown() )
    { _style.setStatus( element_actived_down ); }
    else
    { _style.setStatus( element_deactived ); }
    _style.paintEvent( painter );

    //! fg
    eMenuRes resType;
    QRect boundRect;
    resType = m_pResMenuItem->m_pResItem->getType();

    boundRect = _style.mCaptionRect;

    //! drawIcon
    if ( is_attr( resType, MR_Icon) )
    {
        QImage img;
        Q_ASSERT( m_pResMenuItem->m_pResItem->getImage( img ) == 0 );

        _style.drawIcon( painter,
                         img,
                         boundRect );
        boundRect.adjust( img.width(), 0, 0, 0 );
    }

    //! draw text
    if ( is_attr( resType, MR_Str ))
    {
        _style.drawText( painter,
                            boundRect,
                            m_pResMenuItem->getTitle() );
    }
}

void RChildButton::on_enable_changed( int msg, bool b )
{
    if ( !cmdMatch(msg)) return;

    setEnabled( b );
}
void RChildButton::on_visible_changed( int msg, bool b )
{
    if ( !cmdMatch(msg)) return;

    setVisible( b );
}

void RChildButton::onClick()
{
    RMenu *pChildMenu;
    QString *pChildName;

    Q_ASSERT( m_pResMenuItem!=NULL );

    //! direct child
    if ( m_pChild != NULL )
    {
        pChildMenu = m_pChild->expandLeaf();
        Q_ASSERT( pChildMenu != NULL );

        //! enter sub menu
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                               CMD_SERVICE_SUB_ENTER,
                               (int)m_pResMenuItem->mId ) ;

        RMenu::_activeMenu->moveToChild( pChildMenu );
    }
    //! indirect child
    else if ( m_pResMenuItem->mRootChildMsg != -1 )
    {
        Q_ASSERT( m_pServMenu!=NULL );

        pChildMenu = m_pServMenu->findMenuByMsg( m_pResMenuItem->mRootChildMsg );
        Q_ASSERT( pChildMenu!=NULL );

        pChildMenu = pChildMenu->expandLeaf();
        Q_ASSERT( pChildMenu != NULL );

        //! enter sub menu
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                               CMD_SERVICE_SUB_ENTER,
                               (int)m_pResMenuItem->mId ) ;

        Q_ASSERT( RMenu::_activeMenu!=NULL );
        RMenu::_activeMenu->moveToChild( pChildMenu );
    }
    //! active the child
    else
    {
        pChildName = getChildName( m_pResMenuItem->mChild );

        if ( NULL != pChildName )
        {
            serviceExecutor::post( *pChildName, CMD_SERVICE_ACTIVE, (int)1 );
        }
        else
        {
            qWarning()<<"invalid child button";
        }
    }
}

void RChildButton::setChild( RMenu *child )
{
    Q_ASSERT( child != NULL );

    m_pChild = child;
}
RMenu *RChildButton::getChild()
{
    return m_pChild;
}

int RChildButton::getChildId()
{
    Q_ASSERT( m_pResMenuItem != NULL );
    return m_pResMenuItem->mChild;
}

QString *RChildButton::getChildName( int id )
{
    CServMenu *pCServMenu;

    Q_ASSERT( m_pServMenu != NULL );
    pCServMenu = m_pServMenu->getCServMenu();

    Q_ASSERT( pCServMenu != NULL );
    if ( id < 0 || id >= pCServMenu->mRefMenus.count() )
    {
        return NULL;
    }

    return pCServMenu->mRefMenus[ id ];
}

}

