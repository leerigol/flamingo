#ifndef RSPACE_H
#define RSPACE_H

#include "rmenuwidget.h"

namespace menu_res {

/*!
 * \brief The RSpace class
 * empty widget no visible content
 */
class RSpace : public RMenuWidget
{
public:
    RSpace( QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event);

};

}

#endif // RSPACE_H
