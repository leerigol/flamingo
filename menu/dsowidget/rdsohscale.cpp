#include "rdsohscale.h"
#include "../draw/rpainter.h"
#include "../../gui/cguiformatter.h"

namespace menu_res {

#define time_scale_fmt    dso_fmt_trim(fmt_float,fmt_width_3,fmt_no_trim_0)

RDsoHScale::RDsoHScale( QWidget *parent ) : RDsoSplitButton( parent )
{
    mScale = 0.0f;
    mDepth = 0;
    mSaRate = 0.0f;

    mStyle = style_yt;

    _style.loadResource("ui/hori/main_scale/");
    _style.resize( this );

    RDsoSplitButton::splitRect( _style.mRectHeader,
                                _style.mRectContent );

    setVisible( false );
}

void RDsoHScale::setScale( float scale )
{
    mScale = scale;
    update();
}
void RDsoHScale::setSaRate( float saRate )
{
    mSaRate = saRate;
    update();
}
void RDsoHScale::setDepth( int depth )
{
    mDepth = depth;
    update();
}

void RDsoHScale::setStyle( eScaleStyle style )
{
    mStyle = style;
    update();
}

void RDsoHScale::paintEvent( QPaintEvent *event )
{
    if ( mStyle == style_xy )
    {
        paintXy( event );
    }
    else
    {
        paintYt( event );
    }
}

void RDsoHScale::paintYt( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    QString str;

    RPainter painter(this);

    //! bg
    _style.paintEvent( painter, isDown()? uic_downed : uic_enabled );

    //! font
    //!
    painter.setPen( _style.enableStyle.mColor );

    QFont font( _style.mFont.mFamily );

    //! big font
    font.setPointSize( _style.mBigFont.mSize );
    painter.setFont( font );

    //! time scale
    gui_fmt::CGuiFormatter::format( str, mScale, time_scale_fmt, Unit_s );
    painter.setPen( _style.mColorScale );
    painter.drawText( _style.mRectScale, Qt::AlignLeft | Qt::AlignVCenter, str );

    //! small font
    font.setPointSize( _style.mFont.mSize );
    painter.setFont( font );

    //! sa rate
    gui_fmt::CGuiFormatter::format( str, mSaRate, fmt_def, Unit_SaS );
    painter.setPen( _style.mColorSa );
    painter.drawText( _style.mRectSa, Qt::AlignRight, str );

    //! points
    gui_fmt::CGuiFormatter::format( str, mDepth, fmt_def, Unit_Pts );
    painter.setPen( _style.mColorDepth );
    painter.drawText( _style.mRectDepth, Qt::AlignRight, str );
}

void RDsoHScale::paintXy( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    QString str;

    RPainter painter(this);

    //! bg
    _style.paintEvent( painter, isDown()? uic_downed : uic_enabled );

    //! font
    //!
    painter.setPen( _style.enableStyle.mColor );

    QFont font( _style.mFont.mFamily );
    font.setPointSize( _style.mBigFont.mSize );
    painter.setFont( font );

    //! time scale
    gui_fmt::CGuiFormatter::format( str, mScale, time_scale_fmt, Unit_s );
    painter.setPen( _style.mColorScale );
    painter.drawText( _style.mRectScale, Qt::AlignLeft | Qt::AlignVCenter, str );

    //! small font
    font.setPointSize( _style.mFont.mSize );
    painter.setFont( font );

    //! sa rate
    gui_fmt::CGuiFormatter::format( str, mSaRate, fmt_def, Unit_SaS );
    painter.setPen( _style.mColorSa );
    painter.drawText( _style.mRectSa, Qt::AlignRight, str );

    //! points
    gui_fmt::CGuiFormatter::format( str, mDepth, fmt_def, Unit_Pts );
    painter.setPen( _style.mColorDepth );
    painter.drawText( _style.mRectDepth, Qt::AlignRight, str );
}

}

