#include "rdsoquicklinkbutton.h"

#include "../../meta/crmeta.h"
#include "../menustyle/rcontrolstyle.h"

namespace menu_res {

//! common style
QRect RDsoQuickLinkButton::_mFrameRect;
QRect RDsoQuickLinkButton::_mLabelRect;
QPoint RDsoQuickLinkButton::_mIconXY;

RDsoQuickLinkButton::RDsoQuickLinkButton( QWidget *parent )
                    : QPushButton(parent)
{
    connect( this, SIGNAL(clicked(bool)),
             this, SLOT(on_clicked()) );

    setVisible( false );

    //! base style
    if ( RDsoQuickLinkButton::_mFrameRect.width() > 0 )
    {}
    else
    {
        r_meta::CRMeta rMeta;

        rMeta.getMetaVal("ui/desktop/common/frame", RDsoQuickLinkButton::_mFrameRect );
        rMeta.getMetaVal("ui/desktop/common/label", RDsoQuickLinkButton::_mLabelRect );
        rMeta.getMetaVal("ui/desktop/common/icon", RDsoQuickLinkButton::_mIconXY );
    }

    //! geo
    setGeometry( RDsoQuickLinkButton::_mFrameRect );
}

void RDsoQuickLinkButton::attachIcon( AppIcon *pIcon )
{
    Q_ASSERT( NULL != pIcon );

    m_pIcon = pIcon;
}

void RDsoQuickLinkButton::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );
    QPainter painter(this);

    //! draw the image
    QString strImg;
    if ( getImage(strImg) )
    {
        QImage img;
        if ( img.load(strImg) )
        { painter.drawImage( RDsoQuickLinkButton::_mIconXY, img ); }
        else
        {}
    }
    else
    {}

    //! draw text
    if(app_disable == m_pIcon->getState())
    {
        painter.setPen( QColor(70,70,70,255) );
    }
    else
    {
        painter.setPen( QColor(192,192,192,255) );
    }

    if( m_pIcon->getTitleID() == 0 )
    {
        painter.drawText( RDsoQuickLinkButton::_mLabelRect,
                          Qt::AlignHCenter|Qt::AlignTop,
                          m_pIcon->getTitle() );
    }
    else
    {
        painter.drawText( RDsoQuickLinkButton::_mLabelRect,
                          Qt::AlignHCenter|Qt::AlignTop,
                          sysGetString(m_pIcon->getTitleID(),"") );
    }
}

QSize RDsoQuickLinkButton::sizeHint() const
{
    return QSize( RDsoQuickLinkButton::_mFrameRect.width(),
                  RDsoQuickLinkButton::_mFrameRect.height() );
}

void RDsoQuickLinkButton::on_clicked()
{
    Q_ASSERT( NULL != m_pIcon );

    emit sig_clicked( m_pIcon->getServName(),
                      m_pIcon->getMsg());
}

//! current image path
bool RDsoQuickLinkButton::getImage( QString &img )
{
    Q_ASSERT( NULL != m_pIcon );

    if ( isDown() )
    { return m_pIcon->getSubImage(1, img); }
    else
    { return m_pIcon->getSubImage(0, img); }
}

}

