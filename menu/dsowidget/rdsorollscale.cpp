#include "rdsorollscale.h"

namespace menu_res {

RDsoRollScale::RDsoRollScale( QWidget *parent ) : RDsoHScale( parent )
{
    RDsoHScale_Style style;

    style.reloadResource("ui/hori/roll_scale/");
    style.resize( this );

    _style = style;

    RDsoSplitButton::splitRect( _style.mRectHeader,
                                _style.mRectContent );
}

}

