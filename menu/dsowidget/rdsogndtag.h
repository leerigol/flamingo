#ifndef RDSOGNDTAG_H
#define RDSOGNDTAG_H


#include <QtWidgets>
#include <QDebug>

#include "../../include/dsotype.h"
#include "../arch/rdsoui.h"
#include "../../interface/ihelp/ihelp.h"

#include "rdsomove.h"

using namespace DsoType;

namespace menu_res {

//! 普通模式下的宽度，高度
#define DSO_VGND_WIDTH   23
#define DSO_VGND_HEIGHT  15

//! 在边界上的宽度和高度
#define DSO_VGND_BOUND_WIDTH    15
#define DSO_VGND_BOUND_HEIGHT   23

#define DSO_HGND_WIDTH   13
#define DSO_HGND_HEIGHT  17

#define DSO_HGND_BOUND_WIDTH    17
#define DSO_HGND_BOUND_HEIGHT   13


class RGndTagIcon
{
public:
    QStringList mIcons; //! with different shapes
                        //! from top->bottom
                        //! left -> ritght
};

/*!
 * \brief The RDsoGndTag class
 * gnd图标
 * - 垂直通道零点
 * - 水平中点
 */
class RDsoGndTag : public QPushButton, public IHelp
{
    Q_OBJECT

public:
    enum GndDir
    {
        to_left,
        to_right,
        to_top,
        to_down,
    };

    enum GndPattern
    {
        triangle,
        cone,
        knife,
    };

    static void genPattern( QPainterPath &path,
                       GndDir dir,
                       GndPattern patt,
                       int w, int h );
private:
    //! 生成3角图标
    static void genPatternTriangle( QPainterPath &path,
                                    GndDir dir,
                                    int w, int h
                                     );
    //! 生成锥形图标
    static void genPatternCone( QPainterPath &path,
                                    GndDir dir,
                                    int w, int h
                                );
    //! LA Gnd
    static void genPatternKnife( QPainterPath &path,
                                GndDir dir,
                                int w, int h );

public:
    RDsoGndTag( QString str,
             GndDir dir,
             int w, int h,
             QWidget *parent=0);

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual QSize sizeHint() const;

    void paintImage( QPaintEvent *event );
    void paintPath( QPaintEvent *event );

protected:
    virtual void mousePressEvent(QMouseEvent *event ) Q_DECL_OVERRIDE;
    virtual void mouseMoveEvent(QMouseEvent *e);

    virtual bool event(QEvent *e);

protected Q_SLOTS:
    void onTimeout();

Q_SIGNALS:
    void sigMove( int xDist, int yDist );

public:
    void setActive( bool active );
    void setColor( QColor bgColor, QColor fgColor );
    void setCaption( QString caption );
    void setMoveAttr( DsoMove moveAttr );
    void setAttr( RDsoUI::uiAttribute attr );

    void setImage( const QString &str );

protected:
    bool mActive;

    QColor mBgColor;
    QColor mFgColor;
    QString mCaption;

protected:
    QPoint mptMoveOrig;
    DsoMove mMoveAttr;
    int mxDistCache, myDistCache;
    QTimer mCacheTimer;

    RDsoUI::uiAttribute mAttr;

public:
    GndDir mDir;

public:
    int mW;
    int mH;

    QPainterPath mPath;

    QString mImageFile;
};

/*!
 * \brief The RDsoVGndTag class
 * 垂直地图标
 */
class RDsoVGndTag : public RDsoGndTag
{
    Q_OBJECT

public:
    RDsoVGndTag( QString str="",
              RDsoGndTag::GndDir dir=RDsoGndTag::to_right,
              RDsoGndTag::GndPattern patt = RDsoGndTag::cone,
              int w=0, int h=0,
              QWidget *parent = 0);

};

/*!
 * \brief The RDsoHGndTag class
 * 水平地图标
 */
class RDsoHGndTag : public RDsoGndTag
{
    Q_OBJECT

public:
    RDsoHGndTag( QString str="",
              RDsoGndTag::GndDir dir=RDsoGndTag::to_down,
              RDsoGndTag::GndPattern patt = RDsoGndTag::cone,
              int w=0, int h=0,
              QWidget *parent=0);

};

class RDsoGnd : public QObject, public RDsoUI, public IHelp
{
    Q_OBJECT

public:
    RDsoGnd();
    ~RDsoGnd();
    static QList<menu_res::RDsoGnd *> mGndList;

Q_SIGNALS:
    void clicked( bool );

protected Q_SLOTS:
    void onClicked( );

protected:
    virtual void setWidgetVisible( bool b );
    virtual void refresh();

public:
    void setColor( QColor bgColor, QColor fgColor = Qt::black );
    void setParent( QWidget *parent );

    void setAlign( Qt::AlignmentFlag align );

    void setHelp( const QString &name, int msg );

    void setActive( bool bActive = true );
    void setAllInactive(QList<RDsoGnd *> list);

    void setVisible( bool bVisible );
    bool getVisible();

    void setAttr( RDsoUI::uiAttribute attr );

    void setViewGnd( int gnd );
public:
    void setIcon( int id );
    void attachIcon( int id, const QString &top,
                             const QString &norm,
                             const QString &bot );
protected:
    RGndTagIcon mIcons[6];       //!< a few colors
    int mIconId;

protected:
    RDsoGndTag *m_pTagNormal;    //!< normal
    RDsoGndTag *m_pTagTopLeft;   //!< top || left
    RDsoGndTag *m_pTagDownRight; //!< down || right

    Qt::AlignmentFlag mAlign;

    bool  m_bVisible;

    int   m_viewGnd;             //!< viewGnd
};

/*!
 * \brief The RDsoVGnd class
 * 垂直地容器
 * - 垂直地含有多个图标形状
 */
class RDsoVGnd : public RDsoGnd
{
    Q_OBJECT

public:
    RDsoVGnd(QString str="",
             RDsoGndTag::GndDir dir=RDsoGndTag::to_right,
             RDsoGndTag::GndPattern patt=RDsoGndTag::cone,
             int hw=DSO_VGND_WIDTH,
             int hh=DSO_VGND_HEIGHT,
             int vw=DSO_VGND_BOUND_WIDTH,
             int vh=DSO_VGND_BOUND_HEIGHT
              );

protected:
    virtual void setWidgetVisible( bool b );
    virtual void refresh();

Q_SIGNALS:
    void sigMove( int xDist, int yDist );

protected Q_SLOTS:
    void onMove( int xDist, int yDist );

public:
    void setVisible( bool bVisible );

    void move(int y );
    void moveTop( int y );
    void moveBottom( int y );

    void offset(int vOff );

    void setCaption( QString caption );

private:
    int   mY;

};

/*!
 * \brief The RDsoHGnd class
 * 水平地容器
 * - 水平地含有多个图标形状
 */
class RDsoHGnd : public RDsoGnd
{
    Q_OBJECT

public:
    RDsoHGnd( QString str="",
              RDsoGndTag::GndDir dir=RDsoGndTag::to_down,
              RDsoGndTag::GndPattern patt=RDsoGndTag::cone,
              int vw = DSO_HGND_WIDTH,
              int vh = DSO_HGND_HEIGHT,
              int hw = DSO_HGND_BOUND_WIDTH,
              int hh = DSO_HGND_BOUND_HEIGHT
              );

protected:
    virtual void setWidgetVisible( bool b );
    virtual void refresh();

Q_SIGNALS:
    void sigMove( int xDist, int yDist );

protected Q_SLOTS:
    void onMove( int xDist, int yDist );

public:
    void setInvert( bool bInv );
    bool getInvert();

    void move( qlonglong x );
    void offset( qlonglong x );

protected:
    void invertMove( qlonglong x);
    void normalMove( qlonglong x);

private:
    int   mX;

    bool  m_bInvert;
};

}
#endif // RDSOGNDTAG_H
