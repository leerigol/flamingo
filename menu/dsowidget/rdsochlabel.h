#ifndef RDSOCHLABEL_H
#define RDSOCHLABEL_H

#include <QtWidgets>

#include "../menustyle/rdsochlabel_style.h"
#include "../../interface/ihelp/ihelp.h"
#include "../../menu/arch/rdsoui.h"
namespace menu_res {

class RDsoCHLabel : public QLabel, public IHelp, public RDsoUIY
{
    Q_OBJECT

private:
    static RDsoCHLabel_Style _style;

public:
    explicit RDsoCHLabel(QWidget *parent = 0);

    void setColor( QColor color );
    void setName( const QString &name );
    QString& getName();

    void setMoveable( bool bMove );

    void setActive( bool bActive = true );
protected:
    virtual void mousePressEvent(QMouseEvent* event);
    virtual void mouseMoveEvent(QMouseEvent *evt);
    virtual bool event( QEvent *e );

protected:
    virtual void setWidgetVisible( bool b );
    virtual void refresh();

public:
    void setText(const QString &txt);
    void moveY( int y );

    void moveTop( int top );
    void moveBottom( int bot );

protected:
    QPoint mptMoveOrig;
    bool mbMoveAble;

private:
    QString mName;    

signals:
    void clicked();

public slots:
};
}
#endif // RDSOCHLABEL_H
