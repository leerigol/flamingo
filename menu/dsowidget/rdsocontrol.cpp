#include "rdsocontrol.h"
#include "../../meta/crmeta.h"

namespace menu_res {

RDsoBaseButton::RDsoBaseButton( QWidget *parent ):QPushButton(parent)
{
    setVisible( false );

    mText = "";
    mColor = Qt::white;
    mAlign = Qt::AlignCenter;

    mSubVisibles = sub_bg;
}

void RDsoBaseButton::loadResource( const QString &strPath )
{
    r_meta::CRMeta rMeta;

    rMeta.getMetaVal( strPath + "head_icon", mHeadIcon );
    rMeta.getMetaVal( strPath + "vline_icon", mVlineIcon );
    rMeta.getMetaVal( strPath + "tail_icon", mTailIcon );

    rMeta.getMetaVal( strPath + "rect", mRect );

    rMeta.getMetaVal( strPath + "vline_span", mVlineSpan );

    setGeometry( mRect );
}

void RDsoBaseButton::setText( const QString &str,
              const QColor &color ,
              Qt::AlignmentFlag align )
{
    mText = str;
    mColor = color;
    mAlign = align;

    update();
}

void RDsoBaseButton::setSubVisible( eSubItem items )
{
    mSubVisibles = items;

    update();
}

void RDsoBaseButton::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL!=event );
    QPainter painter(this);
    QImage image;
    int vLineStart, i;

    if ( is_attr(mSubVisibles, sub_bg) )
    {
        //! head
        image.load( mHeadIcon );
        painter.drawImage( 0, 0, image );
        vLineStart = image.width();

        //! span
        image.load( mVlineIcon );
        for ( i = 0; i < mVlineSpan; i++ )
        {
            painter.drawImage( vLineStart + i, 0, image );
        }

        //! tail
        image.load( mTailIcon );
        painter.drawImage( vLineStart + mVlineSpan, 0, image );
    }

    //! draw text
    if ( is_attr(mSubVisibles, sub_text) )
    {
        painter.setPen( mColor );

        QFont font( "arial" );
        font.setPointSize( 8 );
        painter.setFont( font );
        painter.drawText( rect(), mAlign, mText );
    }
}

RDsoImageButton::RDsoImageButton( QWidget *parent ) : QPushButton( parent )
{
    setVisible( false );
}

void RDsoImageButton::loadResource(const QString &strPath)
{
    r_meta::CRMeta meta;

    meta.getMetaVal( strPath+"normal_icon", mNormalIcon );
    meta.getMetaVal( strPath+"normal_down_icon", mNormalDownIcon );

    if ( isCheckable() )
    {
        meta.getMetaVal( strPath+"checked_icon", mCheckedIcon );
        meta.getMetaVal( strPath+"checked_down_icon", mCheckedDownIcon );
    }

    meta.getMetaVal( strPath+"rect", mRect );

    setGeometry( mRect );
}

void RDsoImageButton::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( event != NULL );
    QImage image;
    QPainter painter(this);

    if ( isDown() )
    {
        if ( isChecked() )
        {
            image.load( mCheckedDownIcon );
        }
        else
        {
            image.load( mNormalDownIcon);
        }
    }
    else
    {
        if ( isChecked() )
        {
            image.load( mCheckedIcon );
        }
        else
        {
            image.load( mNormalIcon);
        }
    }

    painter.drawImage( 0, 0, image );
}

bool RDsoImageButton::event( QEvent *event )
{
    if ( doHelp( event ) )
    {
        event->accept();
        return true;
    }
    return QPushButton::event(event);
}

}
