#ifndef RDSOSPLITBUTTON_H
#define RDSOSPLITBUTTON_H

#include <QtWidgets>

#include "../../interface/ihelp/ihelp.h"

namespace menu_res {

class RDsoSplitButton : public QPushButton, public IHelp
{
    Q_OBJECT
public:
    RDsoSplitButton( QWidget *parent = 0 );

    void setSplitEnabled(bool bEnabled);

protected:
    virtual void mouseReleaseEvent(QMouseEvent *e);
    virtual bool event( QEvent *e );

Q_SIGNALS:
    void sig_disenable_clicked();
    void sig_header_clicked( QWidget *pCaller );
    void sig_content_clicked( QWidget *pCaller );

protected Q_SLOTS:
    void on_clicked();

protected:
    void splitRect( const QRect &header,
                    const QRect &content );
protected:
    QRect mRectHeader, mRectContent;
    bool  mSplitEnabled;

private:
    QPoint mLastPt;
};

}

#endif // RDSOSPLITBUTTON_H
