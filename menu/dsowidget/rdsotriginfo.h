#ifndef RDSOTRIGINFO_H
#define RDSOTRIGINFO_H

#include <QtWidgets>

#include "../../include/dsostd.h"
using namespace DsoType;

#include "../menustyle/rdsotriginfo_style.h"
#include "rdsosplitbutton.h"
namespace menu_res {

typedef QPair< Chan, QString > pairChIcon;
typedef QPair< Chan, QColor >  pairChColor;

typedef QList< pairChIcon *> listChIcon;
typedef QList< pairChColor *> listChColor;

class RDsoTrigInfo : public RDsoSplitButton
{
public:
    RDsoTrigInfo( QWidget *parent = 0 );
    ~RDsoTrigInfo();
protected:
    static RDsoTrigInfo_Style _style;

    listChIcon _listChIcon;
    listChColor _listChColor;

    void buildListChIcon();
    void buildListChColor();
    void buildListTypeIcon();

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    void setSource( Chan src );
    void setMode( TriggerMode mode );
    void setLevel( float level , float lb = 0.0);
    void setUnit( const QString &unit );

    void setHasTwoLevel(bool b);
    void setSweep( TriggerSweep sweep );

    void  setSlope(int slope);
protected:
    QColor  getChColor( Chan ch );
    QString getChIcon( Chan ch );
    QString getTypeIcon(TriggerMode type , int slope);
private:
    Chan         mSrc;
    TriggerMode  mMode;
    float        mLevel;
    float        mLevelB;
    QString      mUnit;
    bool         bTwoLevel;
    TriggerSweep mSweep;
    int          mSlope;
};

}

#endif // RDSOTRIGINFO_H
