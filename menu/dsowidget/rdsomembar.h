#ifndef RDSOMEMBAR_H
#define RDSOMEMBAR_H

#include <QtWidgets>

#include "../menustyle/rdsomembar_style.h"

#include "../../arith/membar.h"

namespace menu_res {

class DsoSegView
{
public:
    DsoSegView();
public:
    void config( qlonglong t0, qlonglong tSpan );

public:
    qlonglong mT0;
    qlonglong mTSpan;
};

class RDsoMemBar : public QWidget
{
public:
    RDsoMemBar( QWidget *parent = 0 );
    ~RDsoMemBar();
protected:
    virtual void paintEvent( QPaintEvent * event );
    virtual void resizeEvent(QResizeEvent *event );

public:
    void setBarSession( barSession &sesion );

protected:
    void genPattern( int width, int peri = 12 );
    void genTIcons();
public:
    void updateBarCell();

protected:
    //! points
    QPoint *mpWavePoints;
    int *mpPattern;

    QPainterPath tLeft, tRight, tMiddle;

    barSession mBarSesion;
    barCell mBarCell;

#ifndef _UNIT_TEST
protected:
    RDsoMembar_Style _style;
#endif

};

}

#endif // RDSOMEMBAR_H
