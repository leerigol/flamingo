#include "rdsomembar.h"

namespace menu_res {

#ifndef _UNIT_TEST

#define color_wave  _style.mCWave
#define color_bg    _style.mCBg
#define color_zoom  _style.mCZoom
#define color_raw   _style.mCRaw

#define color_main  _style.mCMain
#define color_t     qRgb(0xff,0x80,0x00)
#else

#define color_wave  qRgb(0xc0,0xc0,0xc0)
#define color_bg    qRgb(0,0,0)
#define color_zoom  qRgb(0x00,0x10,0x40)
#define color_raw   qRgb(0x70,0x70,0x70)

#define color_main  qRgb(0,0,0)
#define color_t     qRgb(0xff,0x80,0x00)

#endif

#define T_ICON_W    9
#define T_ICON_H    5

RDsoMemBar::RDsoMemBar( QWidget *parent ) : QWidget( parent )
{
    mpWavePoints = NULL;
    mpPattern = NULL;


#ifndef _UNIT_TEST
    _style.loadResource("ui/mem_bar/");
#endif
}

RDsoMemBar::~RDsoMemBar()
{
    if ( NULL != mpWavePoints )
    {
        delete []mpWavePoints;
    }

    if ( NULL != mpPattern )
    {
        delete []mpPattern;
    }
}

void RDsoMemBar::paintEvent( QPaintEvent * event )
{
    Q_ASSERT( NULL!=event );
    QPainter painter(this);

    //! fill back
    painter.fillRect( mBarCell.stPool, color_raw );
    painter.fillRect( mBarCell.stScr, color_bg );

    //! zoom
    if ( mBarSesion.stCurPara.bZoomOn )
    {
        painter.fillRect( mBarCell.stZoomScrL, color_zoom );
        painter.fillRect( mBarCell.stZoomScrR, color_zoom );
    }

    //! wave
    QPen pen( color_wave );
    painter.setPen( pen );
    QRect tRect = rect();
    painter.save();
    painter.translate( mBarCell.stWave.x(),
                       tRect.y() + tRect.height() * 3 / 4 );
    painter.drawPoints( mpWavePoints, mBarCell.stWave.width() );
    painter.restore();

    //! draw T
    painter.save();
    if ( mBarCell.s16TrigTx  < mBarCell.stPool.left() + T_ICON_W/2 )
    {
        painter.translate( mBarCell.stPool.left(),
                           mBarCell.stPool.top() );
        painter.fillPath( tLeft, QBrush(color_t) );
    }
    else if ( mBarCell.s16TrigTx > mBarCell.stPool.right() - T_ICON_W/2 )
    {
        painter.translate( mBarCell.stPool.right() - T_ICON_H,
                           mBarCell.stPool.top() );
        painter.fillPath( tRight, QBrush(color_t) );
    }
    else
    {
        painter.translate( mBarCell.s16TrigTx - T_ICON_W/2,
                           mBarCell.stPool.top() );
        painter.fillPath( tMiddle, QBrush(color_t) );
    }
    painter.restore();

}

void RDsoMemBar::resizeEvent(QResizeEvent *event )
{
    Q_ASSERT( NULL!=event );
    QRect tRect = rect();

    genPattern( tRect.width(), tRect.height()*4/3 );

    genTIcons();

    updateBarCell();
}

void RDsoMemBar::setBarSession( barSession &sesion )
{
    mBarSesion = sesion;

    updateBarCell();

    update();
}

/*!
 * \brief RDsoMemBar::genPattern
 * \param peri
 * -pattern是内存条上显示的波形图示
 * -pattern 由 4 段组成
 * -周期是4的倍数
 */
void RDsoMemBar::genPattern( int width, int peri )
{
    if ( NULL != mpPattern )
    {
        delete []mpPattern;
    }

    mpPattern = new int[ peri ];
    Q_ASSERT( mpPattern!=NULL );

    int i;
    int now;

    now = 0;
    for ( i = 0; i < peri / 4; i++ )
    {
        mpPattern[i] = now;
    }
    now -= 1;

    for ( ; i < peri * 2 / 4; i++ )
    {
        mpPattern[i] = now--;
    }


    for ( ; i < peri * 3 / 4; i++ )
    {
        mpPattern[i] = now;
    }
    now += 1;

    for ( ; i < peri; i++ )
    {
        mpPattern[i] = now++;
    }

    //! gen points by patern
    if ( mpWavePoints != NULL )
    {
        delete []mpWavePoints;
    }

    mpWavePoints = new QPoint[ width ];
    Q_ASSERT( mpWavePoints!=NULL );

    for ( i = 0; i < width; i++ )
    {
        mpWavePoints[i].setX(i);
        mpWavePoints[i].setY( mpPattern[i%peri] );
    }
}


void RDsoMemBar::genTIcons()
{
    tLeft.moveTo( 0, T_ICON_W/2 );
    tLeft.lineTo( T_ICON_H, 0 );
    tLeft.lineTo( T_ICON_H, T_ICON_W );
    tLeft.closeSubpath();

    tMiddle.moveTo( 0, 0 );
    tMiddle.lineTo( T_ICON_W, 0 );
    tMiddle.lineTo( T_ICON_W/2, T_ICON_H );
    tMiddle.closeSubpath();

    tRight.moveTo( 0, 0);
    tRight.lineTo( T_ICON_H, T_ICON_W/2 );
    tRight.lineTo( 0, T_ICON_W );
    tRight.closeSubpath();
}

void RDsoMemBar::updateBarCell()
{
    QRect tRect;

    tRect = rect();

    memBarReMap( &tRect,
                 tRect.height() / 2,
                 &mBarSesion,
                 &mBarCell );

}

}

