#ifndef RDSOZOOMAREA_H
#define RDSOZOOMAREA_H

#include <QWidget>

namespace menu_res {

class RDsoZoomArea : public QWidget
{
    Q_OBJECT
public:
    explicit RDsoZoomArea(QWidget *parent = 0);

public:
    void setPhyRect( const QRect &rect );
    void setPhyRect( int x, int y, int w, int h );

    void setViewRect( int w );
    void setColor( QColor color );

    void setMask( int left, int right );
protected:
    virtual void paintEvent( QPaintEvent *event );

signals:

public slots:

private:
    int mLeftLength, mRightLength;
    QRect mViewRect;

    QRect mPhyRect;
    QColor mColor;
};

}

#endif // RDSOZOOMAREA_H
