#ifndef RDSOHLINE_H
#define RDSOHLINE_H

#include <QtWidgets>

namespace menu_res {
class RDsoLine : public QWidget
{
public:
    RDsoLine( QWidget *parent = 0);

protected:
    virtual void paintEvent(QPaintEvent *);

public:
    void setColor( QColor color );

protected:
    QColor mColor;
};

class RDsoHLine : public RDsoLine
{
public:
    RDsoHLine( QWidget *parent = 0 );
};

class RDsoVLine : public RDsoLine
{
public:
    RDsoVLine( QWidget *parent = 0 );
};

}

#endif // RDSOHLINE_H
