#ifndef RDSOHSCALE_H
#define RDSOHSCALE_H

#include <QtWidgets>

#include "../menustyle/rdsohscale_style.h"

#include "rdsosplitbutton.h"
namespace menu_res {


class RDsoHScale : public RDsoSplitButton
{       
    Q_OBJECT
public:
    enum eScaleStyle
    {
        style_yt,
        style_xy,
    };

protected:
    RDsoHScale_Style _style;
public:
    RDsoHScale( QWidget *parent = 0 );

public:
    void setScale( float scale );
    void setSaRate( float saRate );
    void setDepth( int depth );
    void setStyle( eScaleStyle style );

protected:
    virtual void paintEvent( QPaintEvent *event );

    void paintYt( QPaintEvent *event );
    void paintXy( QPaintEvent *event );

protected:
    float mScale;
    int mDepth;
    float mSaRate;

    eScaleStyle mStyle;
};

}

#endif // RDSOHSCALE_H
