#include "rdsodg.h"

#include "../menustyle/rcontrolstyle.h"

#include "../../gui/cguiformatter.h"

namespace menu_res {

RDsoDG::RDsoDG(int id, QWidget *parent ) : QPushButton( parent ),
    sourceId(id)
{
    mInited = false;
    isOpened = false;
    currWave = 1;
    loadResource("ui/dg/");

    if( 1 == sourceId )
        setGeometry(g1_Geo);
    else if( 2 == sourceId )
        setGeometry(g2_Geo);
}

RDsoDG::~RDsoDG()
{

}

void RDsoDG::setCurrWave(int v)
{
    currWave = v;
    update();
}

void RDsoDG::setOpen(bool b)
{
    isOpened = b;
    update();
}

void RDsoDG::paintEvent( QPaintEvent *event )
{
    Q_UNUSED(event);
    QPainter painter(this);
    QImage img;

    if(isDown())
        img.load(bg_down);
    else
        img.load(bg_normal);
    painter.drawImage(0, 0, img);

    if( 1 == sourceId ){
        if(isOpened)
            img.load(G1_open);
        else
            img.load(G1_close);
    }else if( 2 == sourceId ){
        if(isOpened)
            img.load(G2_open);
        else
            img.load(G2_close);
    }
    painter.drawImage(textGeo, img);

    if(isOpened){
        if(currWave-1 < picEnList.count())
            img.load(picEnList.at(currWave-1));
    }else{
        if(currWave-1 < picDisList.count())
            img.load(picDisList.at(currWave-1));
    }
    painter.drawImage(waveGeo, img);
}

void RDsoDG::loadResource( const QString &path )
{
    if ( mInited ) return;

    r_meta::CRMeta rMeta;

    rMeta.getMetaVal( path + "g1_geo",      g1_Geo );
    rMeta.getMetaVal( path + "g2_geo",      g2_Geo );

    rMeta.getMetaVal( path + "textgeo",    textGeo );
    rMeta.getMetaVal( path + "wavegeo",    waveGeo );

    rMeta.getMetaVal( path + "bg_normal",   bg_normal );
    rMeta.getMetaVal( path + "bg_down",     bg_down );

    rMeta.getMetaVal( path + "g1_open",     G1_open );
    rMeta.getMetaVal( path + "g1_close",    G1_close );

    rMeta.getMetaVal( path + "g2_open",     G2_open );
    rMeta.getMetaVal( path + "g2_close",    G2_close );

    rMeta.getMetaVal( path + "sourcecount", sourceCount );

    picEnList.clear();
    picDisList.clear();
    for(int i=1; i<=sourceCount; i++){
        QString itemPath = path + "item" + QString::number(i) + "/";
        QString strEn, strDis;
        rMeta.getMetaVal( itemPath + "enable", strEn );
        rMeta.getMetaVal( itemPath + "disable", strDis );
        picEnList.append(strEn);
        picDisList.append(strDis);
    }
    mInited = true;
}

}

