#ifndef RDSOQUICKLINKBUTTON_H
#define RDSOQUICKLINKBUTTON_H

#include <QtWidgets>
#include "rdsocontrol.h"

#include "../../com/appicon/appicon.h"

//! no help
namespace menu_res {

class RDsoQuickLinkButton : public QPushButton
{
    Q_OBJECT
public:
    RDsoQuickLinkButton( QWidget *parent = 0 );

    void attachIcon( AppIcon *pIcon );

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual QSize sizeHint() const;

protected Q_SLOTS:
    void on_clicked();

Q_SIGNALS:
    void sig_clicked( QString strName, int msg );

protected:
    bool getImage( QString &img );

private:
    AppIcon *m_pIcon;

    static QRect _mFrameRect, _mLabelRect;
    static QPoint _mIconXY;
};

}

#endif // RDSOQUICKLINKBUTTON_H
