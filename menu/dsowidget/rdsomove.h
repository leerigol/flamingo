#ifndef RDSOMOVE
#define RDSOMOVE

namespace menu_res {

enum DsoMove
{
    move_unknown = 0,
    move_left = 1,
    move_right = 2,
    move_up = 4,
    move_down = 8,

    move_horizontal = 3,
    move_vertical = 12,

    move_plane = 15,
};

}

#endif // RDSOMOVE

