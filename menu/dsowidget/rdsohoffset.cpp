#include "rdsohoffset.h"
#include "../draw/rpainter.h"
#include "../../gui/cguiformatter.h"

namespace menu_res {

//#define time_offset_fmt    dso_fmt_trim(fmt_float,fmt_width_8,fmt_no_trim_0)
#define time_offset_fmt    dso_fmt_trim(fmt_float,fmt_width_8, fmt_trim_0_3)

//! "ui/hori/main_offset/"
RDsoHOffset::RDsoHOffset( const QString &style,
                          QWidget *parent ) : RDsoSplitButton(parent)
{
    mOffset = 0.0f;

    _style.loadResource( style );
    _style.resize( this );

    setVisible( false );

    RDsoSplitButton::splitRect( _style.mRectHeader, _style.mRectContent );
}

void RDsoHOffset::setOffset( float offset )
{
    mOffset = offset;
    update();
}

void RDsoHOffset::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( event != NULL );

    QString str;

    RPainter painter(this);

    QFont font( _style.mFont.mFamily );
    font.setPointSize( _style.mFont.mSize );
    painter.setFont( font );

    //! bg
    _style.paintEvent( painter, isDown() ? uic_downed : uic_enabled );

    painter.setPen( _style.enableStyle.mColor );

    gui_fmt::CGuiFormatter::format( str, mOffset, time_offset_fmt, Unit_s );

    painter.setPen( _style.mColor );
    painter.drawText( _style.mOffsetRect, Qt::AlignLeft | Qt::AlignVCenter, str );
}

}

