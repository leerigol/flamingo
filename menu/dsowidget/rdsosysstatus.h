#ifndef RDSOSYSSTATUS_H
#define RDSOSYSSTATUS_H

#include <QtWidgets>

#include "../../include/dsostd.h"

#include "../menustyle/rdsosysstatus_style.h"

using namespace DsoType;

namespace menu_res {

class RDsoSysStatus : public QLabel
{
    Q_OBJECT
protected:
    static RDsoSysStatus_Style _style;
public:
    RDsoSysStatus( QWidget *parent = 0 );

public:
    void loadResource( const QString &strPath );
    void setStatus( ControlStatus stat );

protected:
    virtual void paintEvent( QPaintEvent *event );

protected:
    ControlStatus mStatus;

    QString mAutoIcon;
    QString mRunIcon;
    QString mStopIcon;
    QString mWaitIcon;
    QString mTdIcon;

    int mX, mY;
    int mWidth;
    int mHeight;

    bool mDimmAB;
};

}

#endif // RDSOSYSSTATUS_H
