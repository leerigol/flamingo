#ifndef RDSOLA_H
#define RDSOLA_H

#include <QtWidgets>


#include "../../include/dsostd.h"
#include "rdsosplitbutton.h"

#include "../menustyle/rdsolabtn_style.h"

using namespace DsoType;

namespace menu_res {

class RDsoLA : public RDsoSplitButton
{
    Q_OBJECT
public:
    RDsoLA( QWidget *parent = 0 );
    ~RDsoLA();

public slots:

protected:
    virtual void paintEvent( QPaintEvent *event );

public:
    void        loadResource( const QString &root );

public:
    void setActive( bool bAct );
    bool getActive();
    void setOpenLa( quint32 openLa );
    void setActiveDx( quint32 activeDx );

protected:
    RDsoLABTN_Style _style;

protected:
    quint32 mOpenLa;
    bool mActive;
    quint32 mActiveDx;

};

}

#endif // RDSOLA_H
