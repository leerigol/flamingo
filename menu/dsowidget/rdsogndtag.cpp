#include "rdsogndtag.h"

namespace menu_res {
void RDsoGndTag::genPattern( QPainterPath &path,
                 GndDir dir,
                 GndPattern patt,
                 int w, int h
                    )
{
    if ( patt == RDsoGndTag::triangle )
    {
        genPatternTriangle( path, dir,
                            w,h );
    }
    else if ( patt == RDsoGndTag::cone )
    {
        genPatternCone( path, dir,
                        w,h );
    }
    else if ( patt == RDsoGndTag::knife )
    {
        genPatternKnife( path, dir,
                         w,h );
    }
}

void RDsoGndTag::genPatternTriangle( QPainterPath &path,
                                GndDir dir,
                                int w, int h
                                 )
{
    //! __
    //! \ |
    if ( dir == RDsoGndTag::to_right )
    {
        path.moveTo(0,0);
        path.lineTo(w, h);
        path.lineTo(w-1,0);
        path.lineTo(0,0);
    }
    //! __
    //! |/
    else if ( dir == RDsoGndTag::to_left )
    {
        path.moveTo(0,0);
        path.lineTo(w,0);
        path.lineTo(0,h);
        path.lineTo(0,0);
    }
    else if ( dir == RDsoGndTag::to_down )
    {
        path.moveTo(0,0);
        path.lineTo(w,0);
        path.lineTo(w/2,h-1);
        path.lineTo(0,0);
    }
    else if ( dir == RDsoGndTag::to_top )
    {
        path.moveTo(w*1/2,0);
        path.lineTo(w, h);
        path.lineTo(0,h);
        path.lineTo(w*1/2,0);
    }
    else
    {}

}

//! (23+1)*2/3

#define sub_v( w, h )       ( (h) - ((w)+1)/2 )
#define delta_v( w, h )     ( h - sub_v( w, h ) )

void RDsoGndTag::genPatternCone( QPainterPath &path,
                                 GndDir dir,
                                 int w, int h
                                 )
{

    if ( dir == RDsoGndTag::to_right )
    {
        path.moveTo(0,0);
        path.lineTo( sub_v(h,w), 0);
        path.lineTo( (w)-1, h/2);
        path.lineTo( sub_v(h,w),h-1);
        path.lineTo(0,h-1);
        path.lineTo(0,0);
    }
    else if ( dir == RDsoGndTag::to_left )
    {
        path.moveTo(w-1,0);
        path.lineTo(w-1,h-1);
        path.lineTo( delta_v(h,w)-1,h-1);
        path.lineTo(0,(h)/2);
        path.lineTo( delta_v(h,w)-1,0);
        path.lineTo(w-1,0);
    }
    else if ( dir == RDsoGndTag::to_down )
    {
        path.moveTo(0,0);
        path.lineTo(w-1,0);
        path.lineTo(w-1, sub_v(w,h) );
        path.lineTo(w/2,h-1);
        path.lineTo(0, sub_v(w,h) );
        path.lineTo(0,0);
    }
    else if ( dir == RDsoGndTag::to_top )
    {
        path.moveTo(w*1/2,0);
        path.lineTo(w-1, delta_v(w,h)-1 );
        path.lineTo(w-1, h-1);
        path.lineTo(0,h-1);
        path.lineTo(0, delta_v(w,h) );
        path.lineTo(w*1/2,0);
    }
    else
    {}
}

//! (23+1)*2/3
#undef sub_v
#define sub_v( w, h )       ( (h) - ((w))/2 + 1 )
#define delta_v( w, h )     ( h - sub_v( w, h ) )
void RDsoGndTag::genPatternKnife( QPainterPath &path,
                            GndDir dir,
                            int w, int h )
{
    if ( dir == RDsoGndTag::to_right )
    {
        path.moveTo(0,0);
        path.lineTo( sub_v(h,w) ,0);
        path.lineTo( (w), h);
        path.lineTo(0,h);
        path.lineTo(0,0);
    }
    else if ( dir == RDsoGndTag::to_left )
    {
        path.moveTo(w,0);
        path.lineTo(w,h);
        path.lineTo(0,(h));
        path.lineTo( delta_v(h,w)-1,0);
        path.lineTo(w,0);
    }
    else if ( dir == RDsoGndTag::to_down )
    {
        path.moveTo(0,0);
        path.lineTo(w,0);
        path.lineTo(w, h );
        path.lineTo(0, sub_v(w,h) );
        path.lineTo(0,0);
    }
    else if ( dir == RDsoGndTag::to_top )
    {
        path.moveTo(w,0);
        path.lineTo(w, h);
        path.lineTo(0,h);
        path.lineTo(0, delta_v(w,h) );
        path.lineTo(w,0);
    }
    else
    {}
}

RDsoGndTag::RDsoGndTag(QString str,
                 RDsoGndTag::GndDir dir,
                 int w, int h,
                 QWidget *parent):QPushButton(parent)
{
    mCaption = str;

    mBgColor = palette().color( QPalette::BrightText );
    mFgColor = palette().color( QPalette::ButtonText );

    mDir = dir;

    mW = w;
    mH = h;
    mActive = false;

    mMoveAttr = move_unknown;
    mAttr = RDsoUI::ui_attr_none;

    connect( &mCacheTimer, SIGNAL(timeout()),
             this, SLOT(onTimeout()) );

    mCacheTimer.setSingleShot( true );
    mCacheTimer.setInterval( 500 );

    setVisible( false );
}

void RDsoGndTag::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    if ( mImageFile.length() > 0 )
    {
        paintImage( event );
    }
    else
    {
        paintPath( event );
    }
}

void RDsoGndTag::paintImage( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    QImage image(mImageFile);

    if ( image.isNull() )
    {
        qWarning()<<mImageFile<<"fail!!!";
        return;
    }

    QPainter painter(this);

    //! \todo position
    painter.drawImage( 0, 0, image );
}

void RDsoGndTag::paintPath( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    QPainter painter(this);

    QRect rect = contentsRect();

    painter.save();
    painter.scale( rect.width()/(float)mW, rect.height()/(float)mH );

    if(mActive)
    {
        painter.fillPath(mPath, QBrush(mBgColor) );
        painter.restore();

        painter.setPen( QPen(QBrush(Qt::black), 1, Qt::SolidLine) );
    }
    else
    {
        painter.fillPath(mPath, QBrush(Qt::black) );
        painter.restore();

        painter.setPen(QPen(mBgColor, 1, Qt::SolidLine));
        painter.drawPath(mPath);

        painter.setPen( QPen(mBgColor, 1, Qt::SolidLine) );
    }

    QFont font;
    font.setPointSize(9);
    font.setWeight(QFont::Light);
    font.setStretch(80);
    painter.setFont(font);
    if ( mCaption.size()!=0 )
    {
        if ( mDir == to_right )
        {
            if ( mCaption.size() > 2 )
            {
                painter.drawText( rect, Qt::AlignLeft | Qt::AlignVCenter, mCaption );
            }
            else
            {
                painter.drawText( rect, Qt::AlignCenter | Qt::AlignVCenter, mCaption );
            }

        }
        else if ( mDir == to_left )
        {
            painter.drawText( rect, Qt::AlignRight | Qt::AlignVCenter, mCaption );
        }
        else if ( mDir == to_down )
        {
            painter.drawText( rect, Qt::AlignTop | Qt::AlignHCenter, mCaption );
        }
        else if ( mDir == to_top )
        {
            painter.drawText( rect, Qt::AlignBottom | Qt::AlignHCenter, mCaption );
        }
        else
        {
            painter.drawText( rect, Qt::AlignCenter, mCaption );
        }
    }
}

QSize RDsoGndTag::sizeHint() const
{
    return QSize(mW,mH);
}

void RDsoGndTag::onTimeout()
{
    if ( mxDistCache != 0
         || myDistCache != 0 )
    {
        sigMove( mxDistCache,
                 myDistCache );

        mxDistCache = 0;
        myDistCache = 0;
    }
}

void RDsoGndTag::mousePressEvent(QMouseEvent *event)
{
    setActive(true);
    //! moveable
    if ( is_attr( mAttr, RDsoUI::ui_attr_moveable) )
    {}
    else
    {
        return QPushButton::mousePressEvent( event );
    }

    QPushButton::mousePressEvent( event );

    //! pos
    mptMoveOrig.setX( event->globalPos().rx() - pos().rx() );
    mptMoveOrig.setY( event->globalPos().ry() - pos().ry() );

    //! cache clear
    mxDistCache = 0;
    myDistCache = 0;

    event->ignore();
}

void RDsoGndTag::mouseMoveEvent(QMouseEvent *event)
{
    //! moveable
    if ( is_attr( mAttr, RDsoUI::ui_attr_moveable) )
    {}
    else
    {
        return QPushButton::mouseMoveEvent( event );
    }

    if (event->buttons() == Qt::LeftButton)
    {
        int xPos, yPos;
        int xDist, yDist;

        //! distance
        xPos = event->globalX() - mptMoveOrig.x();
        yPos = event->globalY() - mptMoveOrig.y();

        //! check range
        if ( xPos < 0 ) xPos = 0;
        if ( yPos < 0 ) yPos = 0;

        //! in parent region
        QWidget *pParent = (QWidget*)parent();
        if ( pParent != NULL )
        {
            QRect parRect = pParent->rect();

            QRect myRect = rect();

            if ( xPos > parRect.width() - myRect.width() )
            { xPos = parRect.width() - myRect.width(); }

            if ( yPos > parRect.height() - myRect.height() )
            { yPos = parRect.height() - myRect.height(); }
        }

        //! current position
        QPoint ptNow = pos();

        xDist = xPos - ptNow.x();
        yDist = yPos - ptNow.y();

        if ( is_attr( mMoveAttr, move_vertical) )
        {
            move( ptNow.x(), yPos );
        }
        else if ( is_attr( mMoveAttr, move_horizontal) )
        {
            move( xPos, ptNow.y() );
        }
        else if ( is_attr( mMoveAttr, move_left) )
        {
            if ( xDist < 0 )
            {
                move( xPos, ptNow.y() );
            }
            else
            { return; }
        }
        else if ( is_attr( mMoveAttr, move_right) )
        {
            if ( xDist > 0 )
            { move( xPos, ptNow.y()); }
            else
            { return; }
        }
        else if ( is_attr( mMoveAttr, move_up) )
        {
            if ( yDist < 0 )
            { move( ptNow.x(), yPos); }
            else
            { return; }
        }
        else if ( is_attr( mMoveAttr, move_down) )
        {
            if ( yDist > 0 )
            { move( ptNow.x(), yPos); }
            else
            { return; }
        }
        else
        {
            this->move( xPos, yPos );
        }

        mxDistCache += xDist;
        myDistCache += yDist;

        mCacheTimer.start();
    }
}

bool RDsoGndTag::event(QEvent *e)
{
    if ( doHelp(e) )
    {
        e->accept();
        return true;
    }

    return QPushButton::event( e );
}

void RDsoGndTag::setActive( bool active )
{    
    if(active)
        this->raise();
    mActive = active;
    update();
}

void RDsoGndTag::setColor( QColor bgColor, QColor fgColor )
{
    mBgColor = bgColor;
    mFgColor = fgColor;
    update();
}

void RDsoGndTag::setCaption(QString caption)
{
    mCaption = caption;
    update();
}

void RDsoGndTag::setMoveAttr( DsoMove moveAttr )
{
    mMoveAttr = moveAttr;
}

void RDsoGndTag::setAttr( RDsoUI::uiAttribute attr )
{
    mAttr = attr;
}

void RDsoGndTag::setImage( const QString &str )
{
    if ( mImageFile != str )
    { update(); }

    mImageFile = str;
}

RDsoVGndTag::RDsoVGndTag( QString str,
                    RDsoGndTag::GndDir dir,
                    RDsoGndTag::GndPattern patt,
                    int w, int h,
                    QWidget *parent ) :
                    RDsoGndTag(str,dir, w, h, parent)
{
    RDsoGndTag::genPattern( mPath, dir, patt, w, h );    
}

RDsoHGndTag::RDsoHGndTag( QString str,
                    RDsoGndTag::GndDir dir,
                    RDsoGndTag::GndPattern patt,
                    int w, int h,
                    QWidget *parent ) :
                    RDsoGndTag(str,dir, w, h, parent)
{
    RDsoGndTag::genPattern( mPath, dir, patt,
                            w,h );
}

QList<menu_res::RDsoGnd *> RDsoGnd::mGndList;
RDsoGnd::RDsoGnd()
{
    m_pTagNormal = NULL;
    m_pTagTopLeft = NULL;
    m_pTagDownRight = NULL;

    m_bVisible = false;
    mIconId = -1;
}

RDsoGnd::~RDsoGnd()
{

}

void RDsoGnd::onClicked()
{
    bool b = false;
    emit clicked( b );
}

void RDsoGnd::setWidgetVisible( bool b )
{   m_bVisible = b;}
void RDsoGnd::refresh()
{}
void RDsoGnd::setColor( QColor bgColor, QColor fgColor )
{
    m_pTagNormal->setColor( bgColor, fgColor );
    m_pTagTopLeft->setColor( bgColor, fgColor );
    m_pTagDownRight->setColor( bgColor, fgColor );
}

void RDsoGnd::setParent( QWidget *parent )
{
    m_pTagNormal->setParent(parent);
    m_pTagTopLeft->setParent(parent);
    m_pTagDownRight->setParent(parent);
}

void RDsoGnd::setAlign( Qt::AlignmentFlag align )
{
    mAlign = align;
}

void RDsoGnd::setHelp( const QString &name, int msg )
{
    IHelp::setHelp( name, msg );

    m_pTagNormal->setHelp( name, msg );
    m_pTagTopLeft->setHelp( name, msg );
    m_pTagDownRight->setHelp( name, msg );
}

void RDsoGnd::setActive( bool bActive )
{
    RDsoUI::setActive( bActive );

    QWidget *selfParent;

    selfParent = m_pTagNormal->parentWidget();
    Q_ASSERT( selfParent != NULL );

    m_pTagNormal->setActive(bActive);
    m_pTagTopLeft->setActive(bActive);
    m_pTagDownRight->setActive(bActive);

    refresh();
}

void RDsoGnd::setAllInactive(QList<RDsoGnd *> mGndList)
{
    for( int i=0; i<mGndList.count(); i++)
    {
        if(mGndList.at(i) != NULL)
        {
            if(mGndList.at(i)->getShow())
                mGndList.at(i)->setActive(false);
        }
    }
}

void RDsoGnd::setVisible( bool bVisible )
{
    m_bVisible = bVisible;

    //! hide all
    if ( !bVisible )
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(false);
        m_pTagDownRight->setVisible(false);
    }
}

bool RDsoGnd::getVisible()
{
    return m_bVisible;
}

void RDsoGnd::setAttr( RDsoUI::uiAttribute attr )
{
    RDsoUI::setAttr( attr );

    if ( m_pTagNormal != NULL )
    { m_pTagNormal->setAttr( attr ); }
    if ( m_pTagTopLeft != NULL )
    { m_pTagTopLeft->setAttr( attr ); }
    if ( m_pTagDownRight != NULL )
    { m_pTagDownRight->setAttr( attr ); }
}

void RDsoGnd::setViewGnd( int gnd )
{
    m_viewGnd = gnd;
}

void RDsoGnd::setIcon( int colorId )
{
    Q_ASSERT( colorId >= 0 && colorId < array_count(mIcons) );
    mIconId = colorId;

    Q_ASSERT( mIcons[colorId].mIcons.size() == 3 );

    Q_ASSERT( m_pTagNormal != NULL );
    //! set each icon
    m_pTagNormal->setImage( mIcons[colorId].mIcons[1] );
    m_pTagTopLeft->setImage( mIcons[colorId].mIcons[0] );
    m_pTagDownRight->setImage( mIcons[colorId].mIcons[2] );
}

void RDsoGnd::attachIcon( int id, const QString &top_left,
                         const QString &norm,
                         const QString &bot_right )
{
    Q_ASSERT( id >= 0 && id < array_count(mIcons) );

    mIcons[id].mIcons.clear();

    mIcons[id].mIcons.append( top_left );
    mIcons[id].mIcons.append( norm );
    mIcons[id].mIcons.append( bot_right );
}

RDsoVGnd::RDsoVGnd(QString str,
                    RDsoGndTag::GndDir dir,
                    RDsoGndTag::GndPattern patt,
                    int hw, int hh,
                    int vw, int vh )
{
    mAlign = Qt::AlignRight;

    m_pTagNormal = new RDsoVGndTag(str,dir,patt, hw, hh);
    Q_ASSERT( NULL != m_pTagNormal);

    m_pTagTopLeft = new RDsoHGndTag(str, RDsoGndTag::to_top, patt, vw, vh);
    Q_ASSERT( NULL != m_pTagTopLeft );
    m_pTagDownRight = new RDsoHGndTag(str, RDsoGndTag::to_down, patt, vw, vh);
    Q_ASSERT( NULL != m_pTagDownRight );

    //! default
    m_viewRect.setRect( 0,0,DSO_VGND_WIDTH,DSO_VGND_HEIGHT);
    m_phyRect.setRect( 0,1, DSO_VGND_WIDTH, 480 );

    //! set dir
    m_pTagNormal->setMoveAttr( move_vertical );
    m_pTagTopLeft->setMoveAttr( move_down );
    m_pTagDownRight->setMoveAttr( move_up );

    //! connection
    connect( m_pTagNormal, SIGNAL(pressed()),
             this, SLOT(onClicked()) );
    connect( m_pTagTopLeft, SIGNAL(pressed()),
             this, SLOT(onClicked()) );
    connect( m_pTagDownRight, SIGNAL(pressed()),
             this, SLOT(onClicked()) );

    connect( m_pTagNormal,SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );

    connect( m_pTagTopLeft,SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );

    connect( m_pTagDownRight,SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );

    //! attr
    setAttr( RDsoUI::ui_attr_moveable );
}

void RDsoVGnd::setWidgetVisible( bool b )
{
    RDsoGnd::setWidgetVisible( b );

    //! hide all
    if ( !m_bVisible )
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(false);
        m_pTagDownRight->setVisible(false);
    }
    //! visible according the pos
    else
    {
        if ( is_attr(mAttr, ui_attr_physical) )
        {}
        else
        { move( mY ); }
    }
}
void RDsoVGnd::refresh()
{
    if ( !m_bVisible ) return;

    //! move physical
    if ( is_attr(mAttr, ui_attr_physical) )
    {}
    else
    { move( mY ); }
}

void RDsoVGnd::onMove( int xDist, int yDist )
{
    if ( is_attr( mAttr, ui_attr_moveable) )
    {}
    else
    { return; }

    float yRatio;

    Q_ASSERT( m_phyRect.height() != 0 );

    //qDebug()<<m_viewRect.height()<<m_phyRect.height()<<yDist;

    yRatio = (float)m_viewRect.height() / m_phyRect.height();

    emit sigMove( xDist, yDist * yRatio );
}

void RDsoVGnd::move( int y )
{
    int pos;

    Q_ASSERT( m_viewRect.height() != 0 );
    Q_ASSERT( m_phyRect.height() != 0 );

    mY = y;

    if ( !m_bVisible ) return;

    //! physical pos size
    pos = y * m_phyRect.height() / m_viewRect.height();

    //! rect top
    pos -= m_pTagNormal->mH/2;

    //! top
    if ( y < 0 )
    {
        m_pTagTopLeft->setVisible(true);
        m_pTagNormal->setVisible(false);
        m_pTagDownRight->setVisible(false);
    }
    //! down
    else if ( y > (m_viewRect.height() - 1) )
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(false);
        m_pTagDownRight->setVisible(true);
    }
    //! normal
    else
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(true);
        m_pTagDownRight->setVisible(false);
    }

    //! physical pos
    int phyX;
    if ( mAlign == Qt::AlignRight )
    {
        phyX = m_phyRect.left() + m_phyRect.width() - m_pTagTopLeft->mW;
    }
    else
    {
        phyX = m_phyRect.left();
    }

    if ( m_pTagTopLeft->isVisible() )
    {
        m_pTagTopLeft->move( phyX, m_phyRect.top() );
    }

    if ( m_pTagNormal->isVisible() )
    {
        if( pos < 0 )
        {
            m_pTagNormal->move( m_phyRect.left(), m_phyRect.top() );
        }
        else if( pos > (m_phyRect.height() - m_pTagNormal->mH) )
        {
            m_pTagNormal->move( m_phyRect.left(), m_phyRect.bottom() - m_pTagNormal->mH );
        }
        else
        {
            m_pTagNormal->move( m_phyRect.left(), pos + m_phyRect.top() );
        }
    }

    if ( m_pTagDownRight->isVisible())
    {
        m_pTagDownRight->move( phyX, m_phyRect.bottom() - m_pTagDownRight->mH );
    }
}
//void RDsoVGnd::move( int y )
//{
//    int pos;

//    Q_ASSERT( m_viewRect.height() != 0 );
//    Q_ASSERT( m_phyRect.height() != 0 );

//    mY = y;

//    if ( !m_bVisible ) return;

//    //! physical pos size
//    pos = y * m_phyRect.height() / m_viewRect.height();

//    //! rect top
//    pos -= m_pTagNormal->mH/2;

//    //! top
//    if ( pos < 0 )
//    {
//        m_pTagTopLeft->setVisible(true);
//        m_pTagNormal->setVisible(false);
//        m_pTagDownRight->setVisible(false);
//    }
//    //! down
//    else if ( pos > m_phyRect.height() - m_pTagNormal->mH )
//    {
//        m_pTagTopLeft->setVisible(false);
//        m_pTagNormal->setVisible(false);
//        m_pTagDownRight->setVisible(true);
//    }
//    //! normal
//    else
//    {
//        m_pTagTopLeft->setVisible(false);
//        m_pTagNormal->setVisible(true);
//        m_pTagDownRight->setVisible(false);
//    }

//    //! physical pos
//    int phyX;
//    if ( mAlign == Qt::AlignRight )
//    {
//        phyX = m_phyRect.left() + m_phyRect.width() - m_pTagTopLeft->mW;
//    }
//    else
//    {
//        phyX = m_phyRect.left();
//    }

//    if ( m_pTagTopLeft->isVisible() )
//    {
//        m_pTagTopLeft->move( phyX, m_phyRect.top() );
//    }

//    if ( m_pTagNormal->isVisible() )
//    {
//        m_pTagNormal->move( m_phyRect.left(), pos + m_phyRect.top() );
//    }

//    if ( m_pTagDownRight->isVisible())
//    {
//        m_pTagDownRight->move( phyX, m_phyRect.bottom() - m_pTagDownRight->mH );
//    }
//}

//! move physical
void RDsoVGnd::moveTop( int y )
{
    if ( !m_bVisible )
    { return; }

    m_pTagNormal->setVisible( true );
    m_pTagTopLeft->setVisible( false );
    m_pTagDownRight->setVisible( false );

    m_pTagNormal->move( m_phyRect.left(), y + m_phyRect.top() );
}
void RDsoVGnd::moveBottom( int y )
{
    if ( !m_bVisible )
    { return; }

    m_pTagNormal->setVisible( true );
    m_pTagTopLeft->setVisible( false );
    m_pTagDownRight->setVisible( false );

    moveTop( y - m_pTagNormal->mH );
}

void RDsoVGnd::offset(int vOff )
{
    move( m_viewGnd - vOff );
}

void RDsoVGnd::setCaption(QString caption)
{
    m_pTagNormal->setCaption(caption);
    m_pTagTopLeft->setCaption(caption);
    m_pTagDownRight->setCaption(caption);
}

void RDsoVGnd::setVisible( bool bVisible )
{
    RDsoGnd::setVisible( bVisible );

    if ( bVisible )
    {
        move( mY );
    }
}

RDsoHGnd::RDsoHGnd( QString str, RDsoGndTag::GndDir dir,
                  RDsoGndTag::GndPattern patt,
                  int vw, int vh,
                  int hw, int hh )
{

    m_bInvert = false;

    m_pTagNormal = new RDsoHGndTag(str,dir, patt, vw,vh);
    Q_ASSERT( NULL != m_pTagNormal );
    m_pTagTopLeft = new RDsoVGndTag(str, RDsoGndTag::to_left, patt, hw, hh);
    Q_ASSERT( NULL != m_pTagTopLeft );
    m_pTagDownRight = new RDsoVGndTag(str, RDsoGndTag::to_right, patt, hw, hh);
    Q_ASSERT( NULL != m_pTagDownRight );

    //! default gnd
    //! \todo meta config
    m_viewRect.setRect( 0,0, 1000, DSO_HGND_HEIGHT );
    m_phyRect.setRect( 0,0,1000,DSO_HGND_HEIGHT );

    //! set dir
    m_pTagNormal->setMoveAttr( move_horizontal );
    m_pTagTopLeft->setMoveAttr( move_right );
    m_pTagDownRight->setMoveAttr( move_left );

    connect( m_pTagNormal, SIGNAL(pressed()),
             this, SLOT(onClicked()) );
    connect( m_pTagTopLeft, SIGNAL(pressed()),
             this, SLOT(onClicked()) );
    connect( m_pTagDownRight, SIGNAL(pressed()),
             this, SLOT(onClicked()) );

    //! drag and drop
    connect( m_pTagNormal, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );
    connect( m_pTagTopLeft, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );
    connect( m_pTagDownRight, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );

    mAlign = Qt::AlignTop;

    //! attr
    setAttr( RDsoUI::ui_attr_moveable );
}

void RDsoHGnd::setWidgetVisible( bool b )
{
    RDsoGnd::setWidgetVisible( b );

    //! hide all
    if ( !m_bVisible )
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(false);
        m_pTagDownRight->setVisible(false);
    }
    //! visible according the pos
    else
    {
       move( mX );
    }
}
void RDsoHGnd::refresh()
{
    if ( !m_bVisible ) return;

    if ( is_attr(mAttr, ui_attr_physical) )
    {}
    else
    { move( mX ); }
}

void RDsoHGnd::onMove( int xDist,
                             int yDist )
{
    float xRatio;

    Q_ASSERT( m_phyRect.width() != 0 );

    xRatio = (float)m_viewRect.width() / m_phyRect.width();

    emit sigMove( xDist * xRatio, yDist );
}

void RDsoHGnd::setInvert( bool bInv )
{
    m_bInvert = bInv;
}
bool RDsoHGnd::getInvert()
{
    return m_bInvert;
}

void RDsoHGnd::move( qlonglong x )
{
    mX = x;

    if ( !m_bVisible ) return;

    if ( m_bInvert )
    {
        invertMove( x );
    }
    else
    {
        normalMove( x );
    }
}

void RDsoHGnd::offset( qlonglong x )
{
    move( m_viewGnd - x );
}

void RDsoHGnd::invertMove( qlonglong x)
{
    qlonglong pos;

    //! physical pos
    pos = x * m_phyRect.width() / m_viewRect.width();

    //! left
    if ( pos < m_pTagNormal->mW/2 )
    {
        m_pTagDownRight->setVisible(true);
        m_pTagNormal->setVisible(false);
        m_pTagTopLeft->setVisible(false);
    }
    //! right
    else if ( pos > m_phyRect.width() - m_pTagNormal->mW/2 )
    {
        m_pTagDownRight->setVisible(false);
        m_pTagNormal->setVisible(false);
        m_pTagTopLeft->setVisible(true);
    }
    //! normal
    else
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(true);
        m_pTagDownRight->setVisible(false);
    }

    //! physical pos
    if ( m_pTagTopLeft->isVisible() )
    {
        m_pTagTopLeft->move( m_phyRect.left(), m_phyRect.top() );
    }

    if ( m_pTagNormal->isVisible() )
    {
        m_pTagNormal->move( m_phyRect.right() + 1 - pos - m_pTagNormal->mW / 2,
                            m_phyRect.top() );
    }

    if ( m_pTagDownRight->isVisible())
    {
        m_pTagDownRight->move( m_phyRect.right() + 1 - m_pTagDownRight->mW,
                           m_phyRect.top() );
    }
}
void RDsoHGnd::normalMove( qlonglong x )
{
    qlonglong pos;

    //! physical pos
    pos = x * m_phyRect.width() / m_viewRect.width();

    //! left
    if ( pos < m_pTagNormal->mW/2 )
    {
        m_pTagTopLeft->setVisible(true);
        m_pTagNormal->setVisible(false);
        m_pTagDownRight->setVisible(false);
    }
    //! right
    else if ( pos > m_phyRect.width() - m_pTagNormal->mW/2 )
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(false);
        m_pTagDownRight->setVisible(true);
    }
    //! normal
    else
    {
        m_pTagTopLeft->setVisible(false);
        m_pTagNormal->setVisible(true);
        m_pTagDownRight->setVisible(false);
    }

    //! physical pos
    if ( m_pTagTopLeft->isVisible() )
    {
        m_pTagTopLeft->move( m_phyRect.left(), m_phyRect.top() );
    }

    if ( m_pTagNormal->isVisible() )
    {
        m_pTagNormal->move( m_phyRect.left() + pos - m_pTagNormal->mW / 2,
                            m_phyRect.top() );
    }

    if ( m_pTagDownRight->isVisible())
    {
        m_pTagDownRight->move( m_phyRect.right() - m_pTagDownRight->mW + 1,
                           m_phyRect.top() );
    }
}

}


