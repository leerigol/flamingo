#include "rdsodragdrop.h"

namespace menu_res {
IDsoDragDrop::IDsoDragDrop()
{
    mbDragable = true;
    mDir = drag_plane;
}

void IDsoDragDrop::setDragable( bool b )
{ mbDragable = b;}
bool IDsoDragDrop::getDragable()
{ return mbDragable; }

void IDsoDragDrop::setDragDir( IDsoDragDrop::dragDir dir )
{ mDir = dir; }
IDsoDragDrop::dragDir IDsoDragDrop::getDragDir()
{ return mDir; }

/*!
 * \brief IDsoDragDrop::ui__drag_drop
 * \param event
 * \param pWidget
 * \return
 * - ui/drag_drop协议
 * - intf/point0/point event
 */
Qt::DropAction IDsoDragDrop::ui__drag_drop( QMouseEvent *event,
                    QWidget *pWidget
                    )
{
    Q_ASSERT( NULL!=pWidget );

    QByteArray itemData;
    QDataStream dataStream( &itemData, QIODevice::WriteOnly );

    //! intf pos0 eventPos
    dataStream<<(int)((void*)(this))<< pWidget->pos() << event->pos();

    QMimeData *mimeData = new QMimeData;
    Q_ASSERT( NULL != mimeData );
    mimeData->setData( "ui/drag_drop", itemData );

    QPixmap pixmap( pWidget->size() );
    pWidget->render( &pixmap );

    QDrag *drag =  new QDrag( pWidget );
    drag->setMimeData( mimeData );
    drag->setPixmap( pixmap );
    drag->setHotSpot( event->pos() );

    pWidget->setVisible( false );
    Qt::DropAction dropAct = drag->exec( Qt::MoveAction, Qt::MoveAction);

    pWidget->setVisible( true );

    return dropAct;
}

RDsoDragDropFrame::RDsoDragDropFrame( QWidget *parent )
                  : QWidget( parent )
{
    setAcceptDrops( true );

    //! 默认拖放协议
    addMime("ui/drag_drop");
}

void RDsoDragDropFrame::addMime( const QString &mime )
{
    mMimeList.append( mime );
}

void RDsoDragDropFrame::dragEnterEvent( QDragEnterEvent *event )
{
    Q_ASSERT( NULL != event );

    if ( !mimeMatch( event->mimeData() ) )
    {
        event->ignore();
        return;
    }

    event->acceptProposedAction();
}
void RDsoDragDropFrame::dragMoveEvent( QDragMoveEvent *event )
{
    Q_ASSERT( NULL != event );

    if ( !mimeMatch( event->mimeData() ) )
    {
        event->ignore();
        return;
    }

    event->acceptProposedAction();
}
void RDsoDragDropFrame::dropEvent( QDropEvent *event )
{
    Q_ASSERT( NULL != event );

    if ( !mimeMatch( event->mimeData() ) )
    {
        event->ignore();
        return;
    }

    QByteArray itemData = event->mimeData()->data( mMimeList[0] );
    QDataStream dataStream( &itemData, QIODevice::ReadOnly );

    uint objPtr;
    QPoint pt0, ptSpot;
    dataStream>>objPtr>>pt0>>ptSpot;

    event->setDropAction( Qt::MoveAction );
    event->accept();

    if ( objPtr == 0 ) return;

    //! move
    IDsoDragDrop *pIDragDrop;
    pIDragDrop = (IDsoDragDrop*)objPtr;
    if ( NULL == pIDragDrop ) return;

    drop_move( pIDragDrop,
               pt0,
               ptSpot,
               event );
}

bool RDsoDragDropFrame::mimeMatch( const QMimeData *pMime )
{
    Q_ASSERT( NULL != pMime );
    QString str;
    foreach( str, mMimeList )
    {
        if ( pMime->hasFormat( str ) )
        { return true; }
    }

    return false;
}
/*!
 * \brief RDsoDragDropFrame::drop_move
 * \param pIntf
 * \param pt0 左上角位置
 * \param ptSpot 热点位置
 * \param event
 * - 移动根据热点位置确定距离
 * - 根据左上角位置确定目标坐标
 */
void RDsoDragDropFrame::drop_move( IDsoDragDrop *pIntf,
                   QPoint &pt0,
                   QPoint &ptSpot,
                   QDropEvent *event )
{
    Q_ASSERT( NULL!=pIntf );

    int drag_type = (int)pIntf->getDragDir();
    switch( drag_type )
    {
        case IDsoDragDrop::drag_horizontal:
            pIntf->drop( event->pos().x() - ptSpot.x(), pt0.y() );
            break;

        case IDsoDragDrop::drag_vertical:
            pIntf->drop( pt0.x(), event->pos().y() - ptSpot.y() );
            break;

        case IDsoDragDrop::drag_plane:
            pIntf->drop( event->pos().x() - ptSpot.x(),
                         event->pos().y() - ptSpot.y() );
            break;
    }
}

}

