#ifndef RDSOSADEPTH_H
#define RDSOSADEPTH_H

#include "rdsohscale.h"

namespace menu_res {

class RDsoXyScale : public RDsoHScale
{
    Q_OBJECT
public:
    RDsoXyScale( QWidget *parent = 0 );
};

}

#endif // RDSOSADEPTH_H
