#ifndef RDSODRAGDROP_H
#define RDSODRAGDROP_H

#include <QtWidgets>

namespace menu_res {
/*!
 * \brief The IDsoDragDrop class
 * 支持拖放的元素接口
 * - 放置在父窗口中的位置
 */
class IDsoDragDrop
{
public:
    enum dragDir
    {
        drag_left = 1,
        drag_right = 2,
        drag_up = 4,
        drag_down = 8,

        drag_horizontal = 0x03,
        drag_vertical = 0x0c,

        drag_plane = 0x0f,  /*!< 平面4个方向*/
    };

protected:
    IDsoDragDrop();
public:
    virtual void drop( int x, int y )=0;
    void setDragable( bool b );
    bool getDragable();

public:
    void setDragDir( dragDir dir );
    dragDir getDragDir();

protected:
    Qt::DropAction ui__drag_drop( QMouseEvent *event,
                        QWidget *pWidget );

protected:
    bool mbDragable;
    dragDir mDir;
};

/*!
 * \brief The RDsoDragDropFrame class
 * - 拖动元素的目标容器
 * - 在容器内可以拖放元素位置
 * - 拖放协议：ui/drag_drop
 *  - mime type
 *  - object(IDsoDragDrop ptr)
 *  - point0
 *  - spot point
 */
class RDsoDragDropFrame : public QWidget
{
    Q_OBJECT
public:
    RDsoDragDropFrame( QWidget *parent );

public:
    void addMime( const QString &mime );

protected:
    void dragEnterEvent( QDragEnterEvent *event ) Q_DECL_OVERRIDE;
    void dragMoveEvent( QDragMoveEvent *event ) Q_DECL_OVERRIDE;
    void dropEvent( QDropEvent *event ) Q_DECL_OVERRIDE;

protected:
    bool mimeMatch( const QMimeData *pMime );
    void drop_move( IDsoDragDrop *pIntf,
                    QPoint &pt0,
                    QPoint &ptSpot,
                    QDropEvent *event );

private:
    QList <QString> mMimeList;
};

}

#endif // RDSODRAGDROP_H
