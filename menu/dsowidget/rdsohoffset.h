#ifndef RDSOHOFFSET_H
#define RDSOHOFFSET_H

#include <QtWidgets>

#include "../menustyle/rdsohoffset_style.h"

#include "rdsosplitbutton.h"

namespace menu_res {

class RDsoHOffset : public RDsoSplitButton
{
    Q_OBJECT
protected:
    RDsoHOffset_Style _style;

public:
    RDsoHOffset( const QString &style,
                 QWidget *parent = 0 );

public:
    void setOffset( float offset );

protected:
    virtual void paintEvent( QPaintEvent *event );

protected:
    float mOffset;
};

}


#endif // RDSOHOFFSET_H
