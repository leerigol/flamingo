#include "rdsolabel.h"
#include "../../meta/crmeta.h"
namespace menu_res {

RDsoLabel::RDsoLabel( QWidget *parent ) : QLabel( parent )
{
    mUnit = Unit_none;
    mFormat = fmt_def;

    r_meta::CRMeta meta;

    QString strStyle;
    meta.getMetaVal("ui/dsolabel/style", strStyle );
    setStyleSheet( strStyle );
}

void RDsoLabel::setCaption( const QString &caption )
{ mCaption = caption; }
QString RDsoLabel::getCaption()
{ return mCaption; }

void RDsoLabel::setPreStr( const QString &pre )
{ mPreStr = pre; }
QString RDsoLabel::getPreStr()
{ return mPreStr; }

void RDsoLabel::setPostStr( const QString &post )
{ mPostStr = post; }
QString RDsoLabel::getPostStr()
{ return mPostStr; }

void RDsoLabel::setUnit( Unit unit)
{ mUnit = unit; }
Unit RDsoLabel::getUnit()
{ return mUnit; }

void RDsoLabel::setFormat( DsoViewFmt fmt )
{ mFormat = fmt; }
DsoViewFmt RDsoLabel::getFormat()
{ return mFormat; }

void RDsoLabel::setValue( float val )
{
    QString fullTxt = updateText( val );

    setText( fullTxt );
}

QString RDsoLabel::updateText( float val )
{
    QString valStr;
    gui_fmt::CGuiFormatter::format( valStr, val, mFormat, mUnit );

    QString fullTxt;

    fullTxt = mCaption + mPreStr + valStr + mPostStr;

    return fullTxt;
}

}

