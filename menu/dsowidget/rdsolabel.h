#ifndef RDSOLABEL_H
#define RDSOLABEL_H

#include <QtWidgets>

#include "../../gui/cguiformatter.h"

namespace menu_res {

//! caption pre value post
class RDsoLabel : public QLabel
{
public:
    RDsoLabel( QWidget *parent = 0 );

public:
    void setCaption( const QString &caption );
    QString getCaption();

    void setPreStr( const QString &pre );
    QString getPreStr();

    void setPostStr( const QString &post );
    QString getPostStr();

    void setUnit( Unit unit);
    Unit getUnit();

    void setFormat( DsoViewFmt fmt );
    DsoViewFmt getFormat();

    void setValue( float val );

protected:
    QString updateText( float val );

public:
    QString mCaption;
    QString mPreStr, mPostStr;

    Unit mUnit;
    DsoViewFmt mFormat;

};

}

#endif // RDSOLABEL_H
