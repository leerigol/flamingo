#include "rdsotriginfo.h"

#include "../menustyle/rcontrolstyle.h"
#include "../../gui/cguiformatter.h"

#include "../../include/dsostd.h"

namespace menu_res {

#define fmt_level   dso_fmt_trim(fmt_float,fmt_width_3,fmt_no_trim_0)

RDsoTrigInfo_Style RDsoTrigInfo::_style;

RDsoTrigInfo::RDsoTrigInfo( QWidget *parent ) : RDsoSplitButton( parent )
{
    mSrc      = d0;
    mMode     = Trigger_Edge;
    mLevel    = 0.0f;
    mUnit     = "V";
    mSweep    = Trigger_Sweep_Auto;
    bTwoLevel = false;
    mSlope    = 0;

    //! load resource
    _style.loadResource("ui/trig/trig_info/");
    _style.resize( this );

    //! create table
    buildListChIcon();
    buildListChColor();
    buildListTypeIcon();

    setVisible( false );

    RDsoSplitButton::splitRect( _style.mRectHeader, _style.mRectContent );
}

RDsoTrigInfo::~RDsoTrigInfo()
{
    pairChIcon *pChIcon;
    pairChColor *pChColor;
//    pairTypeIcon *pTypeIcon;

    foreach( pChIcon, _listChIcon )
    {
        delete pChIcon;
    }

    foreach( pChColor, _listChColor )
    {
        delete pChColor;
    }

//    foreach( pTypeIcon, _listTypeIcon )
//    {
//        delete pTypeIcon;
//    }
}

#define new_ch_icon( ch, icon )     pChIcon = new pairChIcon;\
pChIcon->first = ch;\
pChIcon->second = _style.m##icon##Icon;\
_listChIcon.append( pChIcon );

void RDsoTrigInfo::buildListChIcon()
{
    pairChIcon *pChIcon;

    new_ch_icon( chan1, CH1 );
    new_ch_icon( chan2, CH2 );
    new_ch_icon( chan3, CH3 );
    new_ch_icon( chan4, CH4 );

    new_ch_icon( acline, Ac );
    new_ch_icon( ext, Ext );
    new_ch_icon( ext5, Ext5 );

    new_ch_icon( d0, D0 );
    new_ch_icon( d1, D1 );
    new_ch_icon( d2, D2 );
    new_ch_icon( d3, D3 );

    new_ch_icon( d4, D4 );
    new_ch_icon( d5, D5 );
    new_ch_icon( d6, D6 );
    new_ch_icon( d7, D7 );

    new_ch_icon( d8, D8 );
    new_ch_icon( d9, D9 );
    new_ch_icon( d10, D10 );
    new_ch_icon( d11, D11 );

    new_ch_icon( d12, D12 );
    new_ch_icon( d13, D13 );
    new_ch_icon( d14, D14 );
    new_ch_icon( d15, D15 );
}

#define new_ch_color( ch, color )  pChColor = new pairChColor;\
pChColor->first = ch;\
pChColor->second = _style.mColor##color;\
_listChColor.append( pChColor );

void RDsoTrigInfo::buildListChColor()
{
    pairChColor *pChColor;

    new_ch_color( chan1, _CH1 );
    new_ch_color( chan2, _CH2 );
    new_ch_color( chan3, _CH3 );
    new_ch_color( chan4, _CH4 );

    new_ch_color( acline, _AC );
    new_ch_color( ext, _Ext );
    new_ch_color( ext5, _Ext );

    new_ch_color( d0, _DX );
    new_ch_color( d1, _DX );
    new_ch_color( d2, _DX );
    new_ch_color( d3, _DX );

    new_ch_color( d4, _DX );
    new_ch_color( d5, _DX );
    new_ch_color( d6, _DX );
    new_ch_color( d7, _DX );

    new_ch_color( d8, _DX );
    new_ch_color( d9, _DX );
    new_ch_color( d10, _DX );
    new_ch_color( d11, _DX );

    new_ch_color( d12, _DX );
    new_ch_color( d13, _DX );
    new_ch_color( d14, _DX );
    new_ch_color( d15, _DX );
}

struct _pairTypeSlopeIcon
{
    TriggerMode      mType;
    int              mSlope;
    QString          mIcon;
};
static QVector<_pairTypeSlopeIcon> pairTypeSlopeIcon;

#define SET_TYPE_SLOPE_ICON(type, slope, icon) do{\
    typeSlopeIcon.mType   = type;\
    typeSlopeIcon.mSlope  = slope;\
    typeSlopeIcon.mIcon   = _style.m##icon##Icon;\
    pairTypeSlopeIcon.append(typeSlopeIcon);\
}while(0)

void RDsoTrigInfo::buildListTypeIcon()
{
    _pairTypeSlopeIcon typeSlopeIcon;
    SET_TYPE_SLOPE_ICON(Trigger_Edge,    Trigger_Edge_Rising,     EdgeRise  );
    SET_TYPE_SLOPE_ICON(Trigger_Edge,    Trigger_Edge_Falling,    EdgeFall  );
    SET_TYPE_SLOPE_ICON(Trigger_Edge,    Trigger_Edge_Any,        EdgeAll   );

    SET_TYPE_SLOPE_ICON(Trigger_Pulse,   Trigger_pulse_positive,  PulseRise );
    SET_TYPE_SLOPE_ICON(Trigger_Pulse,   Trigger_pulse_negative,  PulseFall );

    SET_TYPE_SLOPE_ICON(Trigger_Slope,   Trigger_pulse_positive,  SlopeRise );
    SET_TYPE_SLOPE_ICON(Trigger_Slope,   Trigger_pulse_negative,  SlopeFall );

    SET_TYPE_SLOPE_ICON(Trigger_Video,   -1, Video    );
    SET_TYPE_SLOPE_ICON(Trigger_Logic,   -1, Logic    );
    SET_TYPE_SLOPE_ICON(Trigger_Timeout, -1, Timeout  );
    SET_TYPE_SLOPE_ICON(Trigger_Delay,   -1, Delay    );
    SET_TYPE_SLOPE_ICON(Trigger_AB,      -1, AB       );
    SET_TYPE_SLOPE_ICON(Trigger_Area,    -1, Area     );
    SET_TYPE_SLOPE_ICON(Trigger_Setup,   -1, SetupHold);
    SET_TYPE_SLOPE_ICON(Trigger_RS232,   -1, Rs232    );
    SET_TYPE_SLOPE_ICON(Trigger_SPI,     -1, Spi      );
    SET_TYPE_SLOPE_ICON(Trigger_I2C,     -1, I2C      );
    SET_TYPE_SLOPE_ICON(Trigger_I2S,     -1, I2S      );
    SET_TYPE_SLOPE_ICON(Trigger_LIN,     -1, Lin      );
    SET_TYPE_SLOPE_ICON(Trigger_CAN,     -1, Can      );
    SET_TYPE_SLOPE_ICON(Trigger_CANFD,   -1, CanFD    );
    SET_TYPE_SLOPE_ICON(Trigger_Over,    -1, Window   );
    SET_TYPE_SLOPE_ICON(Trigger_NEdge,   -1, NEdge    );
    SET_TYPE_SLOPE_ICON(Trigger_FlexRay, -1, FlexRay  );
    SET_TYPE_SLOPE_ICON(Trigger_1553,    -1, 1553     );
    SET_TYPE_SLOPE_ICON(Trigger_429,     -1, 429      );
    SET_TYPE_SLOPE_ICON(Trigger_SENT,    -1, Sent     );
    SET_TYPE_SLOPE_ICON(Trigger_SBUS,    -1, SBus     );
    SET_TYPE_SLOPE_ICON(Trigger_Pattern, -1, Pattern  );
    SET_TYPE_SLOPE_ICON(Trigger_Duration,-1, Duration );
    SET_TYPE_SLOPE_ICON(Trigger_Runt,    -1, Runt     );
}

void RDsoTrigInfo::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );
    QPainter painter(this);

    //! bg
    _style.paintEvent( painter, isDown()? uic_downed : uic_enabled );

    QImage image;
    QString str;
    QColor srcColor;

    //! src color
    srcColor = getChColor( mSrc );

    //! type icon
    str  = getTypeIcon( mMode, mSlope);
    image.load( str );
    RControlStyle::renderFg( image, srcColor );
    painter.drawImage( _style.mTypeIconXY, image );

    //! src
    str = getChIcon( mSrc );    
    image.load( str );
    RControlStyle::renderFg( image, srcColor );
    painter.drawImage( _style.mSrcXY, image );

    /* don't show level if source is acline */
    if(mSrc == acline ) return ;

    //! level
    painter.setPen( srcColor );

    QRect levelRect = _style.mRectLevel;
    QFont font1;
    font1.setPointSize(11);
    painter.setFont(font1);

    if(!bTwoLevel)
    {
        gui_fmt::CGuiFormatter::format( str, mLevel, fmt_level );
        str += mUnit;

        painter.drawText( levelRect,
                          Qt::AlignLeft | Qt::AlignVCenter,
                          str );
    }
    else
    {
#if 0
        levelRect.setY( 12 );
        levelRect.setHeight(18);
        gui_fmt::CGuiFormatter::format( str, mLevel, fmt_level );
        str += mUnit;

        painter.drawText( levelRect,
                          Qt::AlignLeft | Qt::AlignVCenter | Qt::AlignTop,
                          str );


        levelRect.setY(27);
        levelRect.setHeight(18);
        gui_fmt::CGuiFormatter::format( str, mLevelB, fmt_level );
        str += mUnit;

        painter.drawText( levelRect,
                          Qt::AlignLeft | Qt::AlignVCenter | Qt::AlignBottom,
                          str );
#endif
        //! for bug:560格式化为59.99
        float levelD = (mLevel - mLevelB) + 0.000005;
        //qDebug()<<__FILE__<<__LINE__<<mLevel<<mLevelB<<levelD;

        gui_fmt::CGuiFormatter::format( str, levelD, fmt_level );

        //! 'Δ' + value + 'V'
        str.insert(0,QChar(0x0394));
        str += mUnit;

        painter.drawText( levelRect,
                          Qt::AlignLeft | Qt::AlignVCenter,
                          str );
    }

    //! -- sweep
    QFont font( _style.mSweepFont, _style.mSweepSize );
    painter.setFont(font);
    painter.setPen( _style.mSweepColor );

    painter.drawText( _style.mRectSweep,
                      Qt::AlignLeft | Qt::AlignVCenter,
                      gui_fmt::CGuiFormatter::toString( mSweep ) );
}

void RDsoTrigInfo::setSource( Chan src )
{
    mSrc = src;
    update();
}
void RDsoTrigInfo::setMode( TriggerMode mode )
{
    mMode = mode;
    update();
}
void RDsoTrigInfo::setLevel( float level , float lb)
{
    mLevel = level;
    mLevelB= lb;
    update();
}
void RDsoTrigInfo::setUnit( const QString &unit )
{
    mUnit = unit;
    update();
}

void RDsoTrigInfo::setHasTwoLevel(bool b)
{
    bTwoLevel = b;
}

void RDsoTrigInfo::setSweep( TriggerSweep num)
{
    mSweep = num;
    update();
}

void RDsoTrigInfo::setSlope(int slope)
{
    mSlope = slope;
    update();
}

QColor RDsoTrigInfo::getChColor( Chan ch )
{
    pairChColor *pChColor;

    foreach( pChColor, _listChColor )
    {
        Q_ASSERT( pChColor != NULL );
        if ( pChColor->first == ch )
        {
            return pChColor->second;
        }
    }

    return Qt::white;
}
QString RDsoTrigInfo::getChIcon( Chan ch )
{
    pairChIcon *pChIcon;

    foreach( pChIcon, _listChIcon )
    {
        Q_ASSERT( pChIcon != NULL );
        if ( pChIcon->first == ch )
        {
            return pChIcon->second;
        }
    }

    return _listChIcon[0]->second;
}
QString RDsoTrigInfo::getTypeIcon( TriggerMode type, int slope )
{
    _pairTypeSlopeIcon typeSlopeIcon;
    foreach( typeSlopeIcon, pairTypeSlopeIcon )
    {
        if ( typeSlopeIcon.mType == type )
        {
            if( (typeSlopeIcon.mSlope == (-1))
                    || (typeSlopeIcon.mSlope == slope) )
            {
                return  typeSlopeIcon.mIcon;
            }
        }
    }
    return "";
}

}

