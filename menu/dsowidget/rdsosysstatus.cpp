#include "rdsosysstatus.h"

namespace menu_res {

RDsoSysStatus_Style RDsoSysStatus::_style;

RDsoSysStatus::RDsoSysStatus( QWidget *parent ) : QLabel( parent )
{
    mStatus = Control_autoing;

    _style.loadResource("ui/sys_status/");
    _style.resize( this );

    setVisible( false );

    mDimmAB = false;
}


void RDsoSysStatus::setStatus( ControlStatus stat )
{
    if ( mStatus == stat )
    {
        mDimmAB = !mDimmAB;
    }
    else
    {
        mDimmAB = true;
        mStatus = stat;
    }

    if ( mStatus == Control_Stoped )
    { mDimmAB = false; }
    else if ( mStatus == Control_td )
    { mDimmAB = false; }
    else
    {}

    update();
}

void RDsoSysStatus::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );
    QString str;
    switch ( mStatus )
    {
        case Control_Runing:
            str = _style.mRunIcon;            
        break;

        case Control_Stoped:
            str = _style.mStopIcon;
        break;

        case Control_waiting:
            str = _style.mWaitIcon;            
        break;

        case Control_td:
            str = _style.mTdIcon;
        break;

        case Control_autoing:
            str = _style.mAutoIcon;            
        break;

        default:
            str = _style.mAutoIcon;
        break;
    }

    QPainter painter(this);
    QImage image;

    if( mDimmAB )
    {
        image.load(_style.mBackIcon);
        painter.drawImage(0, 0, image);
    }

    image.load( str );
    painter.drawImage(0,0, image );
}

}

