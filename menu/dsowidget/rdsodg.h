#ifndef RDSODG_H
#define RDSODG_H

#include <QtWidgets>

#include "../../include/dsostd.h"
#include "../dsowidget/rdsodgbtn.h"
#include "../../interface/ihelp/ihelp.h"

using namespace DsoType;

namespace menu_res {

class RDsoDG : public QPushButton, public IHelp
{
    Q_OBJECT
public:
    RDsoDG(int id, QWidget *parent = 0 );
    ~RDsoDG();

    void setCurrWave(int v);
    void setOpen(bool b);

protected:
    virtual void loadResource( const QString &root );
    virtual void paintEvent( QPaintEvent *event );

private:
    int         sourceId;
    int         currWave;
    bool        isOpened;

    bool        mInited;
    QRect       g1_Geo, g2_Geo;
    QRect       textGeo;
    QRect       waveGeo;

    QString     bg_normal, bg_down;

    QString     G1_open, G1_close;
    QString     G2_open, G2_close;

    int         sourceCount;
    QStringList picEnList;
    QStringList picDisList;

};

}

#endif // RDSODG_H
