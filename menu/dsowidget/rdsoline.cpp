#include "rdsoline.h"
namespace menu_res {
RDsoLine::RDsoLine( QWidget *parent) : QWidget( parent )
{
    mColor = Qt::white;
}

void RDsoLine::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.fillRect( rect(), mColor );
}

void RDsoLine::setColor( QColor color )
{
    mColor = color;
}

RDsoHLine::RDsoHLine( QWidget *parent )
          : RDsoLine( parent )
{}

RDsoVLine::RDsoVLine( QWidget *parent )
          : RDsoLine( parent )
{}

}

