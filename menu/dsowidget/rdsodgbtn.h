#ifndef RDSODGBTN_H
#define RDSODGBTN_H

#include <QtWidgets>

#include "../../include/dsostd.h"
#include "../menustyle/rdsodgbtn_style.h"

using namespace DsoType;

namespace menu_res {

enum Wav{ sinWav, squareWav };

class RDsoDGBTN : public QPushButton
{
    Q_OBJECT
public:
    RDsoDGBTN( QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual QSize sizeHint() const;

    QColor  getStatusColor(UicStatus stat);

    void    drawSin( );
    void    drawSquare( );

public:
    void    loadResource( const QString &root );

    void    setOnOff( bool b );
    bool    getOnOff();

    void    setActive( bool act );
    bool    getActive();

    void    setName( QString name );
    QString getName( );

    void    setWav( menu_res::Wav mWav );
    Wav     getWav( );


protected:
    RDsoDGBTN_Style _style;

    UicStatus   getStatus();

private:

    Wav         xWav;
    QString     dgName;


    bool        mOnOff;
    bool        mActive;

};

}

#endif // RDSODGBTN_H
