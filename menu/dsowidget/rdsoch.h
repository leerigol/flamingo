#ifndef RDSOCH_H
#define RDSOCH_H

#include <QtWidgets>

#include "../../include/dsostd.h"
#include "../menustyle/rdsoch_style.h"

#include "rdsosplitbutton.h"

using namespace DsoType;

namespace menu_res {

class RDsoCH : public RDsoSplitButton
{
    Q_OBJECT

protected:
    RDsoCH_Style _style;

public:
    RDsoCH( int chx, QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual QSize sizeHint() const;

public:
    void setOnOff( bool b );
    bool getOnOff();

    void setActive( bool act );
    bool getActive();

    void setScale( float scale );
    void setOffset( float offset );
    void setUnit( const QString &unit );

    void setInvert( bool invert );
    void setVernier( bool b );
    void setCoupling( Coupling coup );
    void setImpedance( Impedance imp );
    void setBw( bool bw );
public:
    void loadResource( const QString &root );

protected:
    UicStatus getStatus();

private:
    bool mOnOff;
    bool mActive;

    float   mScale;
    float   mOffset;
    QString mUnit;

    bool    mInvert, mVernier;
    Coupling mCoupling;
    Impedance mImpedance;

    bool mBw;
    int  mCh;
};

}

#endif // RDSOCH_H
