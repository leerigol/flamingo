#include "rdsola.h"

#include "../menustyle/rcontrolstyle.h"

#include "../../gui/cguiformatter.h"

namespace menu_res {

RDsoLA::RDsoLA( QWidget *parent ) : RDsoSplitButton( parent )
{
    setVisible( false );

    mActive = false;
    mOpenLa = 0;
    mActiveDx = chan_none;
}

RDsoLA::~RDsoLA()
{
}

void RDsoLA::paintEvent( QPaintEvent *event )
{
    Q_UNUSED(event);
    QPainter painter(this);

    UicStatus stat;
    if ( isDown() )
    {
        if ( mActive )
        { stat = uic_downed; }
        else if ( mOpenLa != 0 )
        { stat = uic_downed; }
        else
        { stat = uic_disabled; }

        _style.paintEvent( painter,
                           stat,
                           mOpenLa,
                           mActiveDx );
    }
    else
    {
        if ( mActive )
        { stat = uic_actived;}
        else if ( mOpenLa != 0 )
        { stat = uic_enabled; }
        else
        { stat = uic_disabled; }

        _style.paintEvent( painter,
                           stat,
                           mOpenLa,
                           mActiveDx );
    }
}

void RDsoLA::loadResource( const QString &path )
{
    _style.loadResource( path );

    setGeometry( _style.mGeo );

    RDsoSplitButton::splitRect( _style.mHeader,
                                _style.mContent );
}

void RDsoLA::setActive( bool bAct )
{
    mActive = bAct;

    update();
}
bool RDsoLA::getActive()
{
    return mActive;
}
void RDsoLA::setOpenLa( quint32 openLa )
{
    mOpenLa = openLa;

    update();
}

void RDsoLA::setActiveDx( quint32 activeDx )
{
    mActiveDx = activeDx;

    update();
}

}

