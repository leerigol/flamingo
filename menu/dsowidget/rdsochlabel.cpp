#include "rdsochlabel.h"

namespace menu_res {

RDsoCHLabel_Style RDsoCHLabel::_style;

RDsoCHLabel::RDsoCHLabel(QWidget *parent)
            : QLabel(parent)
{
    setWindowFlags( Qt::FramelessWindowHint );

    _style.loadResource("ui/dsochlabel/" );

    setPhyShift( 3, 0 );
    setAlignment( Qt::AlignLeft | Qt::AlignVCenter );

    mbMoveAble = false;
}

void RDsoCHLabel::setColor( QColor color )
{
    QString styleFull;
    QString styleColor;

    styleColor.append("border-color:");
    styleColor.append( color.name(QColor::HexRgb) );
    styleColor.append(";");

    styleColor.append("color:");
    styleColor.append( color.name(QColor::HexRgb) );
    styleColor.append(";");

    styleFull = _style.mStyle;
    styleFull.append( styleColor );

    setStyleSheet( styleFull );
}

void RDsoCHLabel::setName( const QString &name )
{
    mName = name;
}

QString& RDsoCHLabel::getName()
{
    return mName;
}

void RDsoCHLabel::setMoveable( bool bMove )
{ mbMoveAble = bMove; }

void RDsoCHLabel::setActive( bool bActive )
{
    RDsoUIY::setActive( bActive );

    raise();
}

void RDsoCHLabel::mousePressEvent(QMouseEvent* event)
{
    emit clicked();
    QLabel::mousePressEvent(event);

    if ( !mbMoveAble ) return;

    mptMoveOrig.setX( event->globalPos().rx() - pos().rx() );
    mptMoveOrig.setY( event->globalPos().ry() - pos().ry() );
}

void RDsoCHLabel::mouseMoveEvent(QMouseEvent *event)
{
    if ( !mbMoveAble ) return;

    if (event->buttons() == Qt::LeftButton)
    {
        QPoint pt = event->globalPos();
        QPoint next = pt - mptMoveOrig;

        if ( next.x() < 0 )
        { next.setX(0);}

        if ( next.y() < 0 )
        { next.setY(0); }

        QWidget *pParent = (QWidget*)parent();
        if ( NULL != pParent )
        {
            QRect parRect = pParent->rect();
            QRect myRect = rect();

            if ( next.x() > parRect.width() - myRect.width() )
            { next.setX(parRect.width() - myRect.width());}

            if ( next.y() > parRect.height() - myRect.height() )
            { next.setY(parRect.height() - myRect.height());}
        }

        QLabel::move( next );
    }
}

bool RDsoCHLabel::event( QEvent *event )
{
    if ( doHelp( event ) )
    {
        event->accept();
        return true;
    }
    return QLabel::event(event);
}

void RDsoCHLabel::setWidgetVisible( bool b )
{
    setVisible( b );
}

void RDsoCHLabel::refresh()
{
    if ( !mbVisible ) return;

    //! move physical
    if ( is_attr(mAttr, ui_attr_physical) )
    { }
    else
    {
        updatePhy();

        QRect myRect = rect();

        int y = 0;

        y = mPhyY - myRect.height()/2;

        //! top
        if ( y < m_phyRect.top() )
        {
            y = m_phyRect.top();
        }
        //! bottom
        else if ( y > (m_phyRect.bottom() - myRect.height() ) )
        {
            y = m_phyRect.bottom() - myRect.height();
        }
        else
        {}

        QWidget::move( m_phyRect.left() + m_ptShift.x(),
                       y+m_ptShift.y() );
    }
}

void RDsoCHLabel::setText(const QString &txt)
{
    QLabel::setText(txt);

    QFontMetrics fontMet( QGuiApplication::font() );

    int w, h;
    w = fontMet.width( txt ) + _style.mPadXY.x() * 2;
    h = fontMet.height() + _style.mPadXY.y() * 2;

    resize( w, h );
}

void RDsoCHLabel::moveY( int y )
{
    RDsoUIY::move( y );
}

//! move physical
void RDsoCHLabel::moveTop( int top )
{
    int y;

    y = m_phyRect.top() + top;

    //! top
    if ( y < m_phyRect.top() )
    {
        y = m_phyRect.top();
    }
    //! bottom
    else if ( y > (m_phyRect.bottom() - rect().height() ) )
    {
        y = m_phyRect.bottom() - rect().height();
    }
    else
    {}

    QWidget::move( m_phyRect.left() + m_ptShift.x(),
                   y+m_ptShift.y() );
}

void RDsoCHLabel::moveBottom( int bot )
{
    int y;

    y = bot - rect().height() + m_phyRect.top();

    //! top
    if ( y < m_phyRect.top() )
    {
        y = m_phyRect.top();
    }
    //! bottom
    else if ( y > (m_phyRect.bottom() - rect().height() ) )
    {
        y = m_phyRect.bottom() - rect().height();
    }
    else
    {}

    QWidget::move( m_phyRect.left() + m_ptShift.x(),
                   y+m_ptShift.y() );
}

}

