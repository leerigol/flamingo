#include "rdsodgbtn.h"

#include "../menustyle/rcontrolstyle.h"

#include "../../gui/cguiformatter.h"

namespace menu_res {

RDsoDGBTN::RDsoDGBTN( QWidget *parent ) : QPushButton( parent )
{
    xWav = sinWav;
    setVisible( false );
}

void RDsoDGBTN::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    QPainter painter(this);
    QColor curColor;
    UicStatus dgStatus;
    QString name;
    menu_res::Wav wav;

    dgStatus = getStatus();
    curColor = getStatusColor( dgStatus );
    name = getName();
    wav = getWav();

    painter.setPen( curColor );

    QFont ft;
    ft.setPointSize(11);
    painter.setFont(ft);

    painter.drawText( _style.mRectName, Qt::AlignHCenter | Qt::AlignLeft, name );
    if(sinWav == wav ){
        drawSin();
    }else if(squareWav == wav ){
        drawSquare();
    }
}

QSize RDsoDGBTN::sizeHint() const
{
    return QSize( _style.mRectName.width(), _style.mRectName.height() );
}

void RDsoDGBTN::drawSin()
{
    QPainter painter(this);
    QImage img;
    switch(getStatus()){
    case uic_actived:
        img.load(_style.sinActive);
        break;
    case uic_enabled:
        img.load(_style.sinEnable);
        break;
    case uic_disabled:
        img.load(_style.sinDisable);
        break;

    default:
        break;
    }
    painter.drawImage(_style.mRectWav.x(),_style.mRectWav.y(),img);
}

void RDsoDGBTN::drawSquare()
{
    QPainter painter(this);
    QImage img;
    switch(getStatus()){
    case uic_actived:
        img.load(_style.squareActive);
        break;
    case uic_enabled:
        img.load(_style.squareEnable);
        break;
    case uic_disabled:
        img.load(_style.squareDisable);
        break;

    default:
        break;
    }
    painter.drawImage(_style.mRectWav.x(),_style.mRectWav.y(),img);
}

void RDsoDGBTN::setOnOff( bool b )
{
    mOnOff = b;
    update();
}
bool RDsoDGBTN::getOnOff()
{
    return mOnOff;
}

void RDsoDGBTN::setActive( bool act )
{
    mActive = act;
    update();
}

bool RDsoDGBTN::getActive()
{
    return mActive;
}

void RDsoDGBTN::setName(QString name)
{
    dgName = name;
    update();
}

QString RDsoDGBTN::getName()
{
    return dgName;
}

void RDsoDGBTN::setWav(menu_res::Wav mWav)
{
    xWav = mWav;
    update();
}

menu_res::Wav RDsoDGBTN::getWav()
{
    return xWav;
}

QColor RDsoDGBTN::getStatusColor(UicStatus stat)
{
    if ( stat == uic_actived )
    {
        return _style.mActiveColor;
    }
    else if ( stat == uic_enabled )
    {
        return _style.mEnableColor;
    }
    else
    {
        return _style.mDisableColor;
    }
}

void RDsoDGBTN::loadResource( const QString &root )
{
    _style.loadResource( root );

    _style.resize( this );
}

UicStatus RDsoDGBTN::getStatus()
{
    if ( mActive )
    {
        return uic_actived;
    }

    if ( mOnOff )
    {
        return uic_enabled;
    }

    return uic_disabled;
}

}

