#include "rdsozoomarea.h"
#include <QPainter>
#include <QDebug>

namespace menu_res {

RDsoZoomArea::RDsoZoomArea(QWidget *parent) : QWidget(parent)
{
    mPhyRect.setRect( 1,1,1000, 342 );
    mViewRect.setRect( 0,0,1000,0 );

    mLeftLength = 100;
    mRightLength = 700;

    mColor.setRgb( 0,0,0xff);

    setVisible( false );
}
/*!
 * \brief RDsoZoomArea::setPhyRect
 * \param rect
 * -设置物理位置
 * -物理位置用于视图到显示的映射
 */
void RDsoZoomArea::setPhyRect( const QRect &rect )
{
    mPhyRect = rect;
}
void RDsoZoomArea::setPhyRect( int x, int y, int w, int h )
{
    mPhyRect.setRect( x, y, w, h );
}

/*!
 * \brief RDsoZoomArea::setViewRect
 * \param w
 * 设置视图宽度
 */
void RDsoZoomArea::setViewRect( int w )
{
    mViewRect.setRect( 0,0, w, 0 );
}

void RDsoZoomArea::setColor( QColor color )
{
    mColor = color;
}
/*!
 * \brief RDsoZoomArea::setMask
 * \param left
 * \param right
 * -设置掩码的左右宽度
 * -左右宽度参考的是视图宽度
 */
void RDsoZoomArea::setMask( int left, int right )
{
    mLeftLength = left;
    mRightLength = right;

    update();
}

void RDsoZoomArea::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( event != NULL );
    QPainter painter( this );
    int phyLength;

    QRect maskRect;

    Q_ASSERT( mViewRect.width() != 0 );

    //! left mask
    phyLength = mLeftLength * mPhyRect.width() / mViewRect.width();
    maskRect.setRect( mPhyRect.left(), mPhyRect.top(), phyLength, mPhyRect.height() );
    painter.fillRect( maskRect, mColor );
    //! right mask
    phyLength = mRightLength * mPhyRect.width() / mViewRect.width();
    maskRect.setRect( mPhyRect.right() - phyLength, mPhyRect.top(),
                      phyLength, mPhyRect.height() );
    painter.fillRect( maskRect, mColor );
}

}
