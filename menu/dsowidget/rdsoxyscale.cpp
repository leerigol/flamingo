#include "rdsoxyscale.h"

namespace menu_res {

RDsoXyScale::RDsoXyScale( QWidget *parent ) : RDsoHScale( parent )
{
    RDsoHScale_Style style;

    style.reloadResource("ui/hori/xy_scale/");
    style.resize( this );

    _style = style;

    mStyle = style_xy;

    RDsoSplitButton::splitRect( _style.mRectHeader,
                                _style.mRectContent );
}

}

