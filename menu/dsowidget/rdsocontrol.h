#ifndef RDSOCONTROL_H
#define RDSOCONTROL_H

#include <QtWidgets>
#include <QDebug>

#include "../../include/dsotype.h"
#include "../arch/rdsoui.h"

#include "../../interface/ihelp/ihelp.h"

using namespace DsoType;

namespace menu_res {

//! no status change
//! head + vLine + tail
class RDsoBaseButton : public QPushButton
{
public:
    enum eSubItem
    {
        sub_bg = 1,
        sub_text = 2,
    };

public:
    RDsoBaseButton( QWidget *parent = 0);

public:
    void loadResource( const QString &strPath );
    void setText( const QString &str,
                  const QColor &color = Qt::white,
                  Qt::AlignmentFlag align = Qt::AlignCenter );
    void setSubVisible( eSubItem items );

protected:
    virtual void paintEvent( QPaintEvent *event );

protected:
    QString mHeadIcon;
    QString mVlineIcon;
    QString mTailIcon;

    QRect mRect;

    int mVlineSpan;

    QString mText;
    QColor mColor;
    Qt::AlignmentFlag mAlign;
    eSubItem mSubVisibles;
};

class RDsoImageButton : public QPushButton, public IHelp
{
public:
    RDsoImageButton( QWidget *parent = 0 );

public:
    void loadResource( const QString &strPath );

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual bool event( QEvent *event );

private:
    QString mNormalIcon;
    QString mNormalDownIcon;

    QString mCheckedIcon;
    QString mCheckedDownIcon;

    QRect mRect;
};

}

#endif // RDSOCONTROL_H
