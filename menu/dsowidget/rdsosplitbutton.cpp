#include "rdsosplitbutton.h"

namespace menu_res {

RDsoSplitButton::RDsoSplitButton( QWidget *parent ) : QPushButton( parent )
{
    connect( this, SIGNAL(clicked(bool)),
             this, SLOT(on_clicked()));
    mSplitEnabled = true;
}

void RDsoSplitButton::setSplitEnabled(bool bEnabled)
{
    mSplitEnabled = bEnabled;
}

void RDsoSplitButton::mouseReleaseEvent(QMouseEvent *e)
{
    //! save the last point
    mLastPt = e->pos();

    QPushButton::mouseReleaseEvent( e );
}

bool RDsoSplitButton::event( QEvent *event )
{
    if ( doHelp( event ) )
    {
        event->accept();
        return true;
    }
    return QPushButton::event(event);
}

void RDsoSplitButton::on_clicked()
{
    if(!mSplitEnabled)
    {
        emit sig_disenable_clicked();
        return;
    }

    if ( mRectHeader.contains( mLastPt) )
    { emit sig_header_clicked( this ); }
    else if ( mRectContent.contains( mLastPt) )
    { emit sig_content_clicked( this ); }
    else
    {}
}

void RDsoSplitButton::splitRect( const QRect &header,
                const QRect &content )
{
    mRectHeader = header;
    mRectContent = content;
}

}
