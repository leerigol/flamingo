#include "rdsoch.h"

#include "../menustyle/rcontrolstyle.h"
#include "../draw/rpainter.h"
#include "../../gui/cguiformatter.h"

namespace menu_res {

#define volt_fmt    dso_fmt_trim(fmt_float,fmt_width_3,fmt_no_trim_0)

RDsoCH::RDsoCH(int chx, QWidget *parent ) : RDsoSplitButton( parent ),
    mCh(chx)
{
    mScale = 0.1f;
    mOffset = 0.0f;

    mUnit = "V";

    mInvert = false;
    mVernier = false;

    mCoupling = DC;
    mImpedance = IMP_50;
    mBw = true;

    //setVisible( false );
}

void RDsoCH::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);
    QColor curColor;
    UicStatus chStatus;
    QString fmtStr;

    chStatus = getStatus();
    curColor = _style.getStatusColor( chStatus );

    //! paint bg
    if ( isDown() &&
         ( chStatus == uic_actived || chStatus == uic_enabled) )
    {
        chStatus = uic_downed;
    }
    _style.paintEvent( painter, chStatus );

    //! set color
    painter.setPen( curColor );

    //! set font
    QFont font( _style.mFont );
    font.setPointSize( _style.mPtSize );
    painter.setFont( font );

    //! invert
    if ( mInvert )
    { painter.fillRect( _style.mRectInvert, _style.getHeadColor(chStatus) ); }
    else
    {}

    //! scale
    gui_fmt::CGuiFormatter::format( fmtStr,
                                    mScale,
                                    volt_fmt );
    fmtStr += mUnit;
    painter.drawText( _style.rectScale,
                      Qt::AlignLeft,
                      fmtStr );

    //! bw
    if ( mBw )
    {
        painter.drawText( _style.rectB,
                          Qt::AlignHCenter | Qt::AlignLeft,
                          "B" );
    }

    //! coupling
    painter.save();
    painter.translate( _style.rectCoup.x(),
                       _style.rectCoup.y() );
    QImage img;

    if ( mCoupling == DC )
    {
        if(getOnOff())
        {
            switch (mCh) {
            case 1:
                img.load(_style.dc1);
                break;
            case 2:
                img.load(_style.dc2);
                break;
            case 3:
                img.load(_style.dc3);
                break;
            case 4:
                img.load(_style.dc4);
                break;
            default:
                break;
            }
            painter.drawImage(_style.rectCoup, img);
        }else{
            img.load(_style.dcDis);
            painter.drawImage( _style.rectCoup, img );
        }
    }
    else if ( mCoupling == AC )
    {
        if(getOnOff())
        {
            switch (mCh) {
            case 1:
                img.load(_style.ac1);
                break;
            case 2:
                img.load(_style.ac2);
                break;
            case 3:
                img.load(_style.ac3);
                break;
            case 4:
                img.load(_style.ac4);
                break;
            default:
                break;
            }
            painter.drawImage(_style.rectCoup, img);
        }else
        {
            img.load(_style.acDis);
            painter.drawImage( _style.rectCoup, img );
        }
    }
    else
    {}
    painter.restore();

    //! imp
    if ( mImpedance == IMP_50 )
    {
        painter.setPen( curColor );
        painter.drawText( _style.impXY, QChar(0x03a9) );
    }
    //! offset
    gui_fmt::CGuiFormatter::format( fmtStr,
                                    mOffset,
                                    volt_fmt );
    fmtStr += mUnit;
    if ( mOffset > 0 )
    {
        fmtStr = "+" + fmtStr;
    }
    painter.drawText( _style.rectOffset,
                      Qt::AlignHCenter | Qt::AlignRight,
                      fmtStr );
}

QSize RDsoCH::sizeHint() const
{
    return _style.mSize;
}

void RDsoCH::setOnOff( bool b )
{
    mOnOff = b;
    update();
}

bool RDsoCH::getOnOff()
{
    return mOnOff;
}

void RDsoCH::setActive( bool act )
{
    mActive = act;
    update();
}

bool RDsoCH::getActive()
{
    return mActive;
}

void RDsoCH::setScale( float scale )
{
    mScale = scale;
    update();
}

void RDsoCH::setOffset( float offset )
{
    mOffset = offset;
    update();
}

void RDsoCH::setUnit( const QString &unit )
{
    mUnit = unit;
    update();
}

void RDsoCH::setInvert( bool invert )
{
    mInvert = invert;
    update();
}

void RDsoCH::setVernier( bool b )
{
    mVernier = b;
    update();
}

void RDsoCH::setCoupling( Coupling coup )
{
    mCoupling = coup;
    update();
}

void RDsoCH::setImpedance( Impedance imp )
{
    mImpedance = imp;
    update();
}

void RDsoCH::setBw( bool bw )
{
    mBw = bw;
    update();
}

void RDsoCH::loadResource( const QString &root )
{
    _style.loadResource( root );

    _style.resize( this );

    RDsoSplitButton::splitRect( _style.mRectHeader, _style.mRectContent );
}

UicStatus RDsoCH::getStatus()
{
    if ( mActive )
    {
        return uic_actived;
    }

    if ( mOnOff )
    {
        return uic_enabled;
    }

    return uic_disabled;
}

}

