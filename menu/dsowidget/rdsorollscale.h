#ifndef RDSOROLLSCALE_H
#define RDSOROLLSCALE_H

#include "rdsohscale.h"

namespace menu_res {

class RDsoRollScale : public RDsoHScale
{
    Q_OBJECT
public:
    RDsoRollScale( QWidget *parent = 0 );
};

}

#endif // RDSOROLLSCALE_H
