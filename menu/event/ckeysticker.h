#ifndef CKEYSTICKER_H
#define CKEYSTICKER_H

#include <QObject>
#include <QTimer>

namespace menu_res {
/*!
 * \brief The CKeySticker class
 * 键盘粘滞处理
 * -键盘粘滞启动一个定时器
 * -在定时时间未到达时，过滤掉键盘旋钮事件
 * -所有的粘滞使用一个定时器
 */
class CKeySticker : public QObject
{
    Q_OBJECT

    const static int _c_stick_time = 500;

public:
    explicit CKeySticker(QObject *parent = 0);
    ~CKeySticker();
signals:

private slots:
    void onTimeOut();

public:
    void stickOn();
    bool isTimeout();

private:
    QTimer *mpTimer;
    bool mbTimeout;
};

}

#endif // CKEYSTICKER_H
