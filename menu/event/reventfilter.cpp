#include "reventfilter.h"
#include "../rmenus.h"

#define CHMANUAL "hmanual"
#define CVMANUAL "vmanual"
#define CTRACK   "cursor.track"
#define CXY      "cursor.xy"

namespace menu_res {

#define rActiveMenu     RMenu::_activeMenu

QMap< int, int > REventFilter::mMenuKeyMap;
QMap< int, int > REventFilter::mFuncKeyMap;

/*!
 * \brief REventFilter::menuIndexToKey
 * \param index
 * \return
 * 将菜单序号对应的按键值
 */
int REventFilter::menuIndexToKey( int index )
{
    QMapIterator<int, int> i( REventFilter::mMenuKeyMap );
    while (i.hasNext())
    {
        i.next();
        if ( i.value() == index )
        { return i.key(); }
    }

    return -1;
}

REventFilter::REventFilter( QObject *parent ) : QObject( parent )
{
    //! not user wnd
    mbWnd = false;

    //! 物理键映射
    mPKeyMaper.createKeyMapper();

    //! 菜单键--位置索引
    mMenuKeyMap.insert( R_Pkey_F1, 0 );
    mMenuKeyMap.insert( R_Pkey_F2, 1 );
    mMenuKeyMap.insert( R_Pkey_F3, 2 );
    mMenuKeyMap.insert( R_Pkey_F4, 3 );
    mMenuKeyMap.insert( R_Pkey_F5, 4 );
    mMenuKeyMap.insert( R_Pkey_F6, 5 );
    mMenuKeyMap.insert( R_Pkey_F7, 6 );

    //! Func key
    mFuncKeyMap.insert( R_Pkey_FInc, R_Skey_TuneInc );
    mFuncKeyMap.insert( R_Pkey_FDec, R_Skey_TuneDec );
    mFuncKeyMap.insert( R_Pkey_FZ, R_Skey_TuneEnter );
}

RKeyMapper &REventFilter::getKeyMap()
{ return mPKeyMaper; }

bool REventFilter::eventFilter(QObject */*obj*/, QEvent *ev)
{   
    //! no menu
    if ( RMenu::_activeMenu == NULL )
    { return false; }

    //! ui events
    QEvent::Type eaterTypes[]=
    {
        QEvent::MouseButtonDblClick,
        QEvent::MouseButtonRelease,
        QEvent::TouchEnd,
    };

    QEvent::Type evtType;
    Q_ASSERT( ev != NULL );
    evtType = ev->type();

    //! do event
    QKeyEvent *evt;
    evt = static_cast<QKeyEvent*>(ev);
    if ( evtType == QEvent::KeyPress )
    {
        //! bypass key
        if ( keyEater( evt->key(), evt->count(), false ) )
        {
            return true;
        }

        return kPressEvent( evt );
    }
    else if ( evtType == QEvent::KeyRelease )
    {
        //! bypass key
        if ( keyEater( evt->key(), evt->count(), true ) )
        {
            return true;
        }

        return kReleaseEvent( evt );
    }
    else
    {
        //! other eater
        for ( int i = 0; i < array_count(eaterTypes); i++ )
        {
            if ( eaterTypes[i] == evtType )
            {
                if ( keyEater( evtType, evt->count(), true ) )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //! 其它非键盘事件，不进行过滤
        return false;
    }
}

bool REventFilter::keyEater( int key, int count, bool bRelease )
{
    //! not soft key
    if ( is_skey(key) ) return false;

    return serviceExecutor::keyEater( key, count, bRelease );
}

bool REventFilter::kPressEvent(QKeyEvent * event)
{
    int key, keyCnt;

    Q_ASSERT( event != NULL );

    key = event->key();
    keyCnt = event->count();

    //! 菜单按键 P_F1->P_F7
    if ( mMenuKeyMap.contains(key) )
    {
        Q_ASSERT( rActiveMenu != NULL );
        //! get the index
        //! to page widget by id
        rActiveMenu->postKey( mMenuKeyMap[key],
                              key,
                              keyCnt,
                              QEvent::KeyPress );

        return true;
    }
    else
    {
        return false;
    }
}

bool REventFilter::kReleaseEvent(QKeyEvent * event)
{
    int key, keyCnt;
    int intervalTime;

    Q_ASSERT( event != NULL );

    key = event->key();
    keyCnt = event->count();

    //! 间隔时间
    bool bOk;
    QString text = event->text();
    //! physical key
    if ( text.size() > 0  )
    {
        intervalTime = event->text().toInt( &bOk );
        if ( bOk )
        {}
        else
        { intervalTime = __INT32_MAX__; }

        //! 用户组合快捷键
        if ( serviceExecutor::quickKeyTranslate( key, intervalTime ) )
        { return true; }
    }
    //! soft key
    else
    { intervalTime = __INT32_MAX__; }

    //! menu page
    RMenuWnd *pMenuWindow;
    pMenuWindow = dynamic_cast<RMenuWnd*>( sysGetMenuWindow() );
    Q_ASSERT( NULL != pMenuWindow );

    //! 菜单键
    if ( mMenuKeyMap.contains(key) )
    {
        //! 显示出菜单窗口
        if ( !pMenuWindow->isExpand() )
        {
            pMenuWindow->setExpand(true);
            return true;
        }

        Q_ASSERT( rActiveMenu != NULL );
        //! get the index
        //! to page widget by id
        rActiveMenu->postKey( mMenuKeyMap[key],
                              key,
                              keyCnt,
                              QEvent::KeyRelease );

        return true;
    }

    //! 换页键
    {
        bool bExpand;
        switch( key )
        {
        case R_Pkey_PageReturn:

            if ( !pMenuWindow->isExpand() )
            {
                pMenuWindow->setExpand(true);
                return true;
            }

            Q_ASSERT( rActiveMenu != NULL );
            rActiveMenu->moveToParent();
            return true;

        case R_Pkey_PageOff:
            //! do not change the page order
            Q_ASSERT( rActiveMenu != NULL );
            bExpand = pMenuWindow->isExpand();
            pMenuWindow->setExpand( !bExpand );

            //! page msg
            //RMenuPage::onMsg( bExpand ? ui_page_collapse : ui_page_expand );
            return true;
        }
    }

    //! 功能旋钮键
    do
    {
        //! not func key
        if ( !mFuncKeyMap.contains(key) )
        { break; }

        //! menu invisible
        if ( !sysGetMenuWindow()->isVisible() )
        {
            if(RMenu::_activeMenu->getName().contains(CVMANUAL,       Qt::CaseInsensitive)
                    || RMenu::_activeMenu->getName().contains(CTRACK, Qt::CaseInsensitive)
                    || RMenu::_activeMenu->getName().contains(CXY,    Qt::CaseInsensitive)
                    || RMenu::_activeMenu->getName().contains(CHMANUAL, Qt::CaseInsensitive) )
            {
            }
            else
            {
                break;
            }
        }

        //! menu visible && no tune
        if ( RMenuPage::getTuneWidget() == NULL )
        { break; }

        //! has tune
        rActiveMenu->postKey( mFuncKeyMap[key], keyCnt, event->type() );
        return true;
    }while( 0 );

    //! 快捷键
    if ( mPKeyMaper.postKey( key, keyCnt, event->type() ) )
    {
        return true;
    }

    return false;
}

void REventFilter::on_changed()
{
    if ( rActiveMenu != NULL )
    {
        rActiveMenu->update();
    }
}

void REventFilter::postkey( QWidget *pWidget,
                            int key, int cnt,
                            QEvent::Type type )
{
    QKeyEvent *pEvt;

    Q_ASSERT( NULL != pWidget );

    //! disabled
    if ( !pWidget->isEnabled() )
    {
        if(work_help == sysGetWorkMode())
        {
            return;
        }
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_ACTION_DISABLED
                               );
        return;
    }

    pEvt = new QKeyEvent( type, key, Qt::NoModifier,"",false, (ushort)cnt );
    if ( NULL == pEvt )
    {
        qWarning()<<__FUNCTION__<<__LINE__<<"memory fail";
        return;
    }
    //! direct to widget
    qApp->postEvent( pWidget, pEvt );

    //! focus now
    pWidget->setFocus();
}

}
