#ifndef RKEYMAPPER_H
#define RKEYMAPPER_H

#include <QtCore>

#include "../arch/rmenures.h"

namespace menu_res {

class RControl;

struct keyMap
{
    keyMap( int pkey, int skey, int msg );
    ~keyMap();

    int pKey;       /*!< physical key */
    int sKey;       /*!< soft key */

    int msg;        /*!< service msg */

                    //! attached invisible controller
    RControl *pControl;
    bool sibling;   //! if sibling is false delete the control
};
/*!
 * \brief The keyConfig struct
 * 键盘映射配置项
 */
struct keyConfig
{
    int pKey;   //! 物理键值
    int sKey;   //! 软键值
    int msg;    //! 对应消息

    QString servName;           //! 服务名称
    eMenuControl controlType;   //! 控件类型

    void *pAttr;                //! 控件属性，和控件相关
};
/*!
 * \brief The AttrPushButton struct
 * 单按钮的属性
 */
struct AttrPushButton
{
    int pushVal;    //! 操作数值
};
struct AttrTwoButton
{
    int type;
};

struct AttrComboxOpt
{
    int cnt;
    int opts[1];
};

struct AttrComboxOpt3
{
    int cnt;
    int opt[3];
};

class RKeyMapper
{
public:
    static void deleteControl( RControl *pControl );
public:
    RKeyMapper();
    ~RKeyMapper();
public:
    void createKeyMapper();
    bool postKey( int pKey, int count, QEvent::Type type );
    void mimicKey( int key,
                   int count = 1,
                   QEvent::Type type = QEvent::KeyRelease );
    void mimicInc( int key );
    void mimicDec( int key );

    //! 数值被更新
    void on_value_changed( int msg );

protected:
    //! create a map item
    keyMap * createAKeyMapper( int pKey, int sKey, QString &servName,
                               int msg,
                               eMenuControl controlType,
                               void *pPara );
    keyMap * findKeyMapper( int msg, QString &servName );
    keyMap * findKeyMapper( int pKey );

    RControl * createControl( eMenuControl controlType,
                              int msg,
                              QString &servName,
                              void *pPara );

    bool filterKeyProc( int pKey,
                        int count,
                        QEvent::Type type );

    bool filterKeyOnWidget( QWidget* pWig,
                            int key,
                            int count,
                            QEvent::Type type );
    bool filterKeyOnMsg( const QString &servName, int msg, int action );

    bool mapKeyProc( int pKey,
                     int count,
                     QEvent::Type type );

private:
    QList< keyMap *> mKeyMaps;
    QList< int >     mWithMenu;
};

}

#endif // RKEYMAPPER_H
