#include <QDebug>
#include "ckeysticker.h"

namespace menu_res {

CKeySticker::CKeySticker(QObject *parent) : QObject(parent)
{
    mpTimer = new QTimer( parent );
    Q_ASSERT( NULL != mpTimer );

    mpTimer->setSingleShot( true );

    connect( mpTimer, SIGNAL(timeout()),
             this, SLOT(onTimeOut()) );
    mbTimeout = true;
}

CKeySticker::~CKeySticker()
{
    delete mpTimer;
}

void CKeySticker::onTimeOut()
{
    mbTimeout = true;
}

void CKeySticker::stickOn()
{
    mbTimeout = false;
    mpTimer->start( _c_stick_time );
}
bool CKeySticker::isTimeout()
{
    return mbTimeout;
}

}

