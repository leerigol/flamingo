######################################################################
# Automatically generated by qmake (3.0) ?? 11? 25 17:08:33 2016
######################################################################

QT += widgets

TEMPLATE = lib
TARGET = ../../lib$$(PLATFORM)/revent
INCLUDEPATH += .

CONFIG += static

DEFINES += _DSO_KEYBOARD


#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
DEFINES -= _DSO_KEYBOARD
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

keybd = $$(DSO_KEYBOARD)
equals(keybd, yes){
DEFINES += _DSO_KEYBOARD
message("physical kb")
}

# dbg show
dbg = $$(_DEBUG)
equals(dbg, _DEBUG){
DEFINES += _DEBUG
}

# Input
HEADERS += ckeysticker.h reventfilter.h rkeymapper.h
SOURCES += ckeysticker.cpp reventfilter.cpp rkeymapper.cpp
