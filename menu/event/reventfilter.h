#ifndef REVENTFILTER_H
#define REVENTFILTER_H

#include <QtWidgets>

#include "rkeymapper.h"

#include "../../com/keyevent/rdsokey.h"

#include "../eventfiltermgr/reventfiltermgr.h"

namespace menu_res {

/*!
 * \brief The REventFilter class
 * 菜单键盘事件过滤
 * -将窗口看成一个巨大的虚拟桌面
 * -桌面上有显示的当前菜单
 * -也有没有显示的一些快捷菜单项，可以认为这些菜单项被放置在了可见区域外面，所以用户不可见
 * -默认情况下下，键盘事件会被菜单接收，一旦有顶层窗口接管了键盘，菜单自然失去键盘
 * -顶层窗口可以只接管部分键盘，其余的键盘仍旧由菜单接收
 */
class REventFilter : public QObject, public IEventFilter
{
    Q_OBJECT
private:
    static QMap< int, int > mMenuKeyMap;
    static QMap< int, int > mFuncKeyMap;
public:
    static int menuIndexToKey( int index );

public:
    REventFilter( QObject *parent );
    RKeyMapper &getKeyMap();
protected:
    virtual bool eventFilter(QObject *obj, QEvent *ev);
    bool keyEater( int key , int count, bool bRelease );
    bool kPressEvent(QKeyEvent * event);
    bool kReleaseEvent(QKeyEvent * event);

protected:
    virtual void on_changed();

public:
    static void postkey( QWidget *pWidget,
                         int key, int cnt,
                         QEvent::Type type );

private:
    //! 快捷键映射表
    RKeyMapper mPKeyMaper;
};

}
#endif // REVENTFILTER_H
