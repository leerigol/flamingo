#include "rkeymapper.h"
#include "../rmenus.h"
#include "../../service/service_msg.h"         //! msgs
#include "../../service/service_name.h"

#include "../../service/servvertmgr/servvertmgr.h"
#include "../../service/servhori/servhori.h"
#include "../../service/servtrig/servtrig.h"
#include "../../service/servcursor/servcursor.h"
namespace menu_res {

//! parameters for the key mapper control
static AttrPushButton _activePush={ 1 };
static AttrTwoButton  _intTwoButton={ 1 };
static AttrComboxOpt3 _opt3Combox={3,2,0,1};

//! configs
static keyConfig _keyConfigs[]= {
    // phisical key  // soft key     //  msg     // servie name  // control type

    //! wave light
    { R_Pkey_FDec, R_Skey_TuneDec, MSG_DISPLAY_WAVE_INTENSITY, serv_name_display, Label_int, 0 },
    { R_Pkey_FInc, R_Skey_TuneInc, MSG_DISPLAY_WAVE_INTENSITY, serv_name_display, Label_int, 0 },
    { KEY_FUNC_Z,     R_Skey_Zval, MSG_DISPLAY_WAVE_INTENSITY, serv_name_display, Label_int, 0 },

    //! ch1
    { R_Pkey_CH1,     R_Skey_Active,   servVertMgr::cmd_chan1_button,   serv_name_vert_mgr,  Button_One, 0},

    { encode_inc(R_Pkey_CH1_VOLT), R_Skey_TuneDec, MSG_CHAN_SCALE, serv_name_ch1, Label_int_float_float_knob, 0 },
    { encode_dec(R_Pkey_CH1_VOLT), R_Skey_TuneInc, MSG_CHAN_SCALE, serv_name_ch1, Label_int_float_float_knob, 0 },
    { R_Pkey_CH1_VOLT_Z,     R_Skey_Active,   MSG_CHAN_FINE,   serv_name_ch1,  Button_Two, 0},

    { encode_inc(R_Pkey_CH1_POS), R_Skey_TuneInc, MSG_CHAN_OFFSET, serv_name_ch1, Label_Float, 0 },
    { encode_dec(R_Pkey_CH1_POS), R_Skey_TuneDec, MSG_CHAN_OFFSET, serv_name_ch1, Label_Float, 0 },
    { R_Pkey_CH1_POS_Z, R_Skey_Zval,  MSG_CHAN_OFFSET, serv_name_ch1, Label_Float, 0 },

    //! ch2
    { R_Pkey_CH2,     R_Skey_Active,   servVertMgr::cmd_chan2_button,   serv_name_vert_mgr,  Button_One, 0},

    { encode_inc(R_Pkey_CH2_VOLT), R_Skey_TuneDec, MSG_CHAN_SCALE, serv_name_ch2, Label_int_float_float_knob, 0 },
    { encode_dec(R_Pkey_CH2_VOLT), R_Skey_TuneInc, MSG_CHAN_SCALE, serv_name_ch2, Label_int_float_float_knob, 0 },
    { R_Pkey_CH2_VOLT_Z,     R_Skey_Active,   MSG_CHAN_FINE,   serv_name_ch2,  Button_Two, 0 },

    { encode_inc(R_Pkey_CH2_POS), R_Skey_TuneInc, MSG_CHAN_OFFSET, serv_name_ch2, Label_Float, 0 },
    { encode_dec(R_Pkey_CH2_POS), R_Skey_TuneDec, MSG_CHAN_OFFSET, serv_name_ch2, Label_Float, 0 },
    { R_Pkey_CH2_POS_Z, R_Skey_Zval,  MSG_CHAN_OFFSET, serv_name_ch2, Label_Float, 0 },

    //! ch3
    { R_Pkey_CH3,     R_Skey_Active,   servVertMgr::cmd_chan3_button,   serv_name_vert_mgr,  Button_One, 0},

    { encode_inc(R_Pkey_CH3_VOLT), R_Skey_TuneDec, MSG_CHAN_SCALE, serv_name_ch3, Label_int_float_float_knob, 0 },
    { encode_dec(R_Pkey_CH3_VOLT), R_Skey_TuneInc, MSG_CHAN_SCALE, serv_name_ch3, Label_int_float_float_knob, 0 },
    { R_Pkey_CH3_VOLT_Z,     R_Skey_Active,   MSG_CHAN_FINE,   serv_name_ch3,  Button_Two, 0 },

    { encode_inc(R_Pkey_CH3_POS), R_Skey_TuneInc, MSG_CHAN_OFFSET, serv_name_ch3, Label_Float, 0 },
    { encode_dec(R_Pkey_CH3_POS), R_Skey_TuneDec, MSG_CHAN_OFFSET, serv_name_ch3, Label_Float, 0 },
    { R_Pkey_CH3_POS_Z, R_Skey_Zval,  MSG_CHAN_OFFSET, serv_name_ch3, Label_Float, 0 },

    //! ch4
    { R_Pkey_CH4,     R_Skey_Active,   servVertMgr::cmd_chan4_button,   serv_name_vert_mgr,  Button_One, 0},

    { encode_inc(R_Pkey_CH4_VOLT), R_Skey_TuneDec, MSG_CHAN_SCALE, serv_name_ch4, Label_int_float_float_knob, 0 },
    { encode_dec(R_Pkey_CH4_VOLT), R_Skey_TuneInc, MSG_CHAN_SCALE, serv_name_ch4, Label_int_float_float_knob, 0 },
    { R_Pkey_CH4_VOLT_Z,     R_Skey_Active,   MSG_CHAN_FINE,   serv_name_ch4,  Button_Two, 0},

    { encode_inc(R_Pkey_CH4_POS), R_Skey_TuneInc, MSG_CHAN_OFFSET, serv_name_ch4, Label_Float, 0 },
    { encode_dec(R_Pkey_CH4_POS), R_Skey_TuneDec, MSG_CHAN_OFFSET, serv_name_ch4, Label_Float, 0 },
    { R_Pkey_CH4_POS_Z, R_Skey_Zval,  MSG_CHAN_OFFSET, serv_name_ch4, Label_Float, 0 },

    //! func
    { R_Pkey_LA,          R_Skey_Active,   servVertMgr::cmd_la_button,   serv_name_vert_mgr,        Button_One, 0},
    { R_Pkey_DECODE,      R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_decode_sel,Button_One, &_activePush},

    { R_Pkey_MATH,        R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_math_sel,  Button_One, &_activePush},
    { R_Pkey_REF,         R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_ref,       Button_One, &_activePush},

    { R_Pkey_SOURCE1,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_source1,   Button_One, &_activePush},
    { R_Pkey_SOURCE2,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_source2,   Button_One, &_activePush},

    { R_Pkey_MEASURE,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_measure,   Button_One, &_activePush},
    { R_Pkey_ACQUIRE,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_hori,      Button_One, &_activePush},
    { R_Pkey_STORAGE,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_storage,   Button_One, &_activePush},

    { R_Pkey_CURSOR,      R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_cursor,    Button_One, &_activePush},

    { R_Pkey_CURSOR,      R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_cursor,    Button_One, &_activePush},

    { R_Pkey_DISPLAY,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_display,   Button_One, &_activePush},
    { R_Pkey_UTILITY,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_utility,   Button_One, &_activePush},

    //! quick
    { R_Pkey_CLEAR,       R_Skey_Active,   MSG_DISPLAY_CLEAR,    serv_name_display,  Button_One, 0},
    { R_Pkey_AUTO,        R_Skey_Active,   MSG_AUTO_TITLE,       serv_name_autoset,  Button_One, 0},
    { R_Pkey_RunStop,     R_Skey_Active,   MSG_HORIZONTAL_RUN,   serv_name_hori,  Button_Two, &_intTwoButton},

    { R_Pkey_SINGLE,      R_Skey_Active,   MSG_TRIGGER_SINGLE,   serv_name_trigger,  Button_One, 0 },

    //! navigate
    { R_Pkey_PLAY_PRE,    R_Skey_Active,   MSG_CHAN_PLAY_PRE,   serv_name_hori,   Button_One, 0 },
    { R_Pkey_PLAY_NEXT,   R_Skey_Active,   MSG_CHAN_PLAY_NEXT,  serv_name_hori,   Button_One, 0  },
    { R_Pkey_PLAY_STOP,   R_Skey_Active,   MSG_CHAN_PLAY_STOP,  serv_name_hori,   Button_One, 0 },


    #ifdef _DEBUG
    { R_Pkey_TOUCH,        R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_menutester,      Button_One, 0},
#else
    { R_Pkey_TOUCH,       R_Skey_Active,   MSG_TOUCH_ENABLE,     serv_name_utility,   Button_Two, 0},
#endif

    { R_Pkey_QUICK,       R_Skey_Active,   MSG_APP_QUICK,        serv_name_quick,     Button_One, 0 },
    { R_Pkey_DEFAULT,     R_Skey_Active,   MSG_RESTORE_DEFAULT,  serv_name_storage,   Button_One, 0 },

    //! trig
    { R_Pkey_TRIG_MENU,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_trigger,  Button_One, &_activePush },
    { R_Pkey_TRIG_MODE,     R_Skey_Active,   CMD_TRIGGER_MODE_KEY, serv_name_trigger,  LiteCombox, &_opt3Combox },
    { R_Pkey_TRIG_FORCE,    R_Skey_Active,   MSG_TRIGGER_FORCE,    serv_name_trigger,  Button_One, 0 },

    { encode_inc(R_Pkey_TRIG_LEVEL), R_Skey_TuneInc, MSG_TRIGGER_LEVEL, serv_name_trigger, Label_int, 0 },
    { encode_dec(R_Pkey_TRIG_LEVEL), R_Skey_TuneDec, MSG_TRIGGER_LEVEL, serv_name_trigger, Label_int, 0 },
    { R_Pkey_TRIG_LEVEL_Z, R_Skey_Active,  MSG_TRIGGER_LEVEL_Z, serv_name_trigger, Button_One, &_activePush },

    //! hori
    { R_Pkey_HORI_NAGAVITE,     R_Skey_Active,   CMD_SERVICE_ACTIVE,   serv_name_search,  Button_One, &_activePush },
    { R_Pkey_HORI_ZOOM,         R_Skey_Active,   MSG_HOR_ZOOM_ON,   serv_name_hori,  Button_Two, 0 },

    { encode_inc(R_Pkey_TIME_SCALE), R_Skey_TuneDec, MSG_HORIZONTAL_TIMESCALE, serv_name_hori, Label_int_float_float_knob, 0 },
    { encode_dec(R_Pkey_TIME_SCALE), R_Skey_TuneInc, MSG_HORIZONTAL_TIMESCALE, serv_name_hori, Label_int_float_float_knob, 0 },

    { R_Pkey_TIME_SCALE_Z,     R_Skey_Active,   MSG_HOR_FINE_ON,   serv_name_hori,  Button_Two, 0 },

    { encode_inc(R_Pkey_TIME_OFFSET), R_Skey_TuneDec, MSG_HORIZONTAL_TIMEOFFSET, serv_name_hori, Label_int_float_float_knob, 0 },
    { encode_dec(R_Pkey_TIME_OFFSET), R_Skey_TuneInc, MSG_HORIZONTAL_TIMEOFFSET, serv_name_hori, Label_int_float_float_knob, 0 },

    { R_Pkey_TIME_OFFSET_Z,     R_Skey_Zval,   MSG_HORIZONTAL_TIMEOFFSET,   serv_name_hori,  Label_int, 0},

};

keyMap::keyMap( int pkey, int skey, int m )
{
    pKey = pkey;
    sKey = skey;
    msg = m;

    pControl = NULL;
    sibling = false;
}
keyMap::~keyMap()
{
    if ( sibling == false )
    {
        Q_ASSERT( pControl != NULL );
        RKeyMapper::deleteControl( pControl );
    }
}

void RKeyMapper::deleteControl( RControl *pControl )
{
    Q_ASSERT( pControl != NULL );
    Q_ASSERT( pControl->m_pResMenuItem != NULL );
    Q_ASSERT( pControl->m_pServMenu != NULL );

    delete pControl->m_pResMenuItem;
    delete pControl->m_pServMenu;

    delete pControl;
}

RKeyMapper::RKeyMapper()
{
}

RKeyMapper::~RKeyMapper()
{
    keyMap *pMap;

    foreach( pMap, mKeyMaps )
    {
        Q_ASSERT( NULL != pMap );
        delete pMap;
    }
}

void RKeyMapper::createKeyMapper()
{
    keyMap *pMap;
    quint32 i;

    for ( i = 0; i < sizeof(_keyConfigs)/sizeof(_keyConfigs[0]); i++ )
    {
        pMap = createAKeyMapper( _keyConfigs[i].pKey,
                                 _keyConfigs[i].sKey,
                                 _keyConfigs[i].servName,
                                 _keyConfigs[i].msg,
                                 _keyConfigs[i].controlType,
                                 _keyConfigs[i].pAttr
                                 );
        Q_ASSERT( NULL != pMap );
        mKeyMaps.append( pMap );
    }

    //add keys those will show the menu
    mWithMenu.append(R_Pkey_CH1);
    mWithMenu.append(R_Pkey_CH2);
    mWithMenu.append(R_Pkey_CH3);
    mWithMenu.append(R_Pkey_CH4);
    mWithMenu.append(R_Pkey_LA);
    mWithMenu.append(R_Pkey_REF);
    mWithMenu.append(R_Pkey_CURSOR);
    mWithMenu.append(R_Pkey_MEASURE);
    mWithMenu.append(R_Pkey_SOURCE1);
    mWithMenu.append(R_Pkey_SOURCE2);

}

bool RKeyMapper::postKey( int pKey, int count, QEvent::Type type )
{
    bool bRet;

    bRet = filterKeyProc( pKey, count, type );
    if ( bRet ) return bRet;

    return mapKeyProc( pKey, count, type );
}

void RKeyMapper::mimicKey( int key,
                           int count,
                           QEvent::Type type)
{
    postKey( key, count, type );
}
void RKeyMapper::mimicInc( int key )
{
    mimicKey( encode_inc(key) );
}
void RKeyMapper::mimicDec( int key )
{
    mimicKey( encode_dec(key) );
}

/*!
 * \brief RKeyMapper::on_value_changed
 * \param msg
 * keymapper中的对应控件更新数值
 */
void RKeyMapper::on_value_changed( int msg )
{
    keyMap *pMap;

    foreach( pMap, mKeyMaps )
    {
        Q_ASSERT( pMap != NULL );
        Q_ASSERT( pMap->pControl != NULL );

        //! msg matched
        if ( pMap->msg == msg )
        {
            pMap->pControl->on_value_changed( msg );
        }
    }
}

keyMap * RKeyMapper::createAKeyMapper( int pKey,
                           int sKey,
                           QString &servName,
                           int msg,
                           eMenuControl controlType,
                           void *pPara )
{
    //! find control in the list
    keyMap *pSibling;
    keyMap *pMaper;

    //! create mapper item
    pMaper = new keyMap( pKey, sKey, msg );
    Q_ASSERT( NULL != pMaper );

    QWidget *pWig = RMenuBuilder::findView( servName,
                                            msg );
    RMenuWidget *pRMWig;
    //! find
    if ( NULL != pWig )
    {
        pRMWig = dynamic_cast<RMenuWidget *>(pWig);
        Q_ASSERT( NULL != pRMWig );

        pMaper->pControl = dynamic_cast<RControl *>(pRMWig);
        pMaper->sibling = true;

        return pMaper;
    }

    //! find sibling
    pSibling = findKeyMapper( msg, servName );

    //! control not exist
    if ( NULL == pSibling )
    {
        //! create new control
        RControl *pControl = createControl( controlType,
                                            msg,
                                            servName,
                                            pPara );
        Q_ASSERT( pControl != NULL );

        pMaper->pControl = pControl;
    }
    else
    {
        //! use the default control
        pMaper->pControl = pSibling->pControl;
        pMaper->sibling = true;
    }

    return pMaper;
}

keyMap * RKeyMapper::findKeyMapper( int msg, QString &servName )
{
    keyMap *pMap;

    foreach( pMap, mKeyMaps )
    {
        Q_ASSERT( pMap != NULL );
        Q_ASSERT( pMap->pControl != NULL );
        Q_ASSERT( pMap->pControl->m_pServMenu != NULL );

        if ( pMap->msg == msg
             && pMap->pControl->m_pServMenu->getConsoleName() == servName )
        {
            return pMap;
        }
    }

    return NULL;
}

keyMap * RKeyMapper::findKeyMapper( int pKey )
{
    keyMap *pMap;

    foreach( pMap, mKeyMaps )
    {
        Q_ASSERT( pMap != NULL );

        if ( pMap->pKey == pKey )
        {
            return pMap;
        }
    }

    return NULL;
}

/*!
 * \brief RKeyMapper::createControl
 * \param controlType
 * \param msg
 * \param servName
 * \param pPara
 * \return
 * 先在菜单中搜索对应消息的控件
 */
RControl * RKeyMapper::createControl( eMenuControl controlType,
                                      int msg,
                                      QString &servName,
                                      void *pPara )
{
    QWidget *pWidget;

    CResMenuItem *pMenuItem;
    RControl *pControl;

    RServMenu *pServ;

    RPushButton *pPushButton;
    RInvertButton *pButInvert;
    RLabel_i_i_f *pLabiif;
    RLabel_i_f_f_knob *pLabiffk;
    RLiteCombox *pCombox;

    //! attr
    AttrPushButton *pPushAttr;
    AttrTwoButton *pTwoAttr;
    AttrComboxOpt *pComboxOpt;

    int i;

    //! pMenuItem
    pMenuItem = new CResMenuItem;
    Q_ASSERT( NULL != pMenuItem );
    pMenuItem->m_eType = controlType;
    pMenuItem->mId = msg;

    //! pServ
    pServ = new RServMenu;
    Q_ASSERT( NULL != pServ );
    pServ->setConsoleName( servName );

    switch( controlType )
    {

    case Button_One:
        pWidget = new RPushButton();
        Q_ASSERT( NULL != pWidget );
        pWidget->hide();

        pControl = dynamic_cast<RControl*>( pWidget );

        pPushButton = dynamic_cast<RPushButton*>(pWidget);
        pPushButton->setResItem( pMenuItem );
        pPushButton->setServMenu( pServ );

        //! init
        if ( NULL != pPara )
        {
            pPushAttr = (AttrPushButton*)pPara;

            pPushButton->setValue( pPushAttr->pushVal );
        }

        break;

    case Button_Two:
        pWidget = new RInvertButton();
        Q_ASSERT( NULL != pWidget );
        pWidget->hide();

        pControl = dynamic_cast<RControl*>( pWidget );

        //! control info
        pButInvert = dynamic_cast<RInvertButton*>(pWidget);
        pButInvert->setResItem( pMenuItem );
        pButInvert->setServMenu( pServ );

        //! 可能有数据类型不是bool的
        if ( NULL != pPara )
        {
            pTwoAttr = (AttrTwoButton*)pPara;
            pButInvert->setType( (RInvertButton::Type)pTwoAttr->type );
        }

        break;

    case Label_int:
        pWidget = new RLabel_i_i_f();
        Q_ASSERT( NULL != pWidget );
        pWidget->hide();

        pControl = dynamic_cast<RControl*>( pWidget );

        pLabiif = dynamic_cast<RLabel_i_i_f*>( pWidget );
        pLabiif->setResItem( pMenuItem );
        pLabiif->setServMenu( pServ );

        pLabiif->setFmt( control_fmt_integer );
        break;

    case Label_Float:
        pWidget = new RLabel_i_i_f();
        Q_ASSERT( NULL != pWidget );
        pWidget->hide();

        pControl = dynamic_cast<RControl*>( pWidget );

        pLabiif = dynamic_cast<RLabel_i_i_f*>(pWidget);
        pLabiif->setResItem( pMenuItem );
        pLabiif->setServMenu( pServ );
        break;

    case Label_int_float_float_knob:
        pWidget = new RLabel_i_f_f_knob();
        Q_ASSERT( NULL != pWidget );
        pWidget->hide();

        pControl = dynamic_cast<RControl*>( pWidget );

        pLabiffk = dynamic_cast<RLabel_i_f_f_knob*>(pWidget);
        pLabiffk->setResItem( pMenuItem );
        pLabiffk->setServMenu( pServ );
        break;

    case LiteCombox:
        pWidget = new RLiteCombox();
        Q_ASSERT( NULL != pWidget );
        pWidget->hide();

        pControl = dynamic_cast<RControl*>( pWidget );

        pCombox = dynamic_cast<RLiteCombox*>(pWidget);
        pCombox->setResItem( pMenuItem );
        pCombox->setServMenu( pServ );

        Q_ASSERT( NULL != pPara );

        //! 添加combox选项
        pComboxOpt = (AttrComboxOpt*)pPara;

        for ( i = 0; i < pComboxOpt->cnt; i++ )
        {
            pCombox->addOption( pComboxOpt->opts[i] );
        }
        break;

    default:
        Q_ASSERT( false );
        pWidget = NULL;
        break;
    }

    //! 设置控件类型
    pControl->setControlType( controlType );

    return pControl;
}

//! foreach service filter
bool RKeyMapper::filterKeyProc( int key,
                                int count,
                                QEvent::Type type )
{
    int msg, action;
    QString servName;
    //! from active to next
    if ( serviceExecutor::keyTranslate( key, servName, msg, action ) )
    {
        QWidget *pWig;

        do
        {
            //! find msg control
            pWig = RMenuBuilder::findView( servName, msg );
            if ( NULL != pWig ) break;

            //! find control in key mapper
            keyMap *pMap;
            pMap = findKeyMapper( msg, servName );
            if ( NULL != pMap && pMap->pControl != NULL )
            {
                pWig = dynamic_cast<QWidget*>( pMap->pControl );
            }

        }while(0);

        if ( NULL != pWig )
        {
            return filterKeyOnWidget( pWig,
                                    action,
                                    count,
                                    type );
        }
        else
        {
            return filterKeyOnMsg( servName, msg, action );
        }
    }

    return false;
}

bool RKeyMapper::filterKeyOnWidget( QWidget* pWig,
                                    int action,
                                    int count,
                                    QEvent::Type type )
{
    //! check widget
    Q_ASSERT( NULL != pWig );
    if ( !pWig->isEnabled() )
    {
        if(work_help == sysGetWorkMode())
        {
            return true;
        }
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_ACTION_DISABLED
                               );
        return true;
    }

    QKeyEvent *pEvt;
    pEvt = new QKeyEvent( type, action, Qt::NoModifier,"",false, (ushort)count );
    Q_ASSERT( NULL != pEvt );

    qApp->postEvent( pWig, pEvt );

    return true;
}

bool RKeyMapper::filterKeyOnMsg( const QString &servName, int msg, int action )
{
    serviceExecutor::post( servName, msg, action );
    return true;
}

bool RKeyMapper::mapKeyProc( int pKey,
                             int count,
                             QEvent::Type type )
{
    keyMap *pMap;

    pMap = findKeyMapper( pKey );
    if ( NULL == pMap )
    {
        return false;
    }

    //! new event
    QKeyEvent *pEvt;
    QWidget *pWidget;

    Q_ASSERT( pMap->pControl );

    //! 对应的菜单控件
    pWidget = dynamic_cast<QWidget*>(pMap->pControl);
    Q_ASSERT( pWidget!=NULL );
    if ( !pWidget->isEnabled() )
    {
        if(work_help == sysGetWorkMode())
        {
            return true;
        }
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_ACTION_DISABLED
                               );
        return true;
    }

    pEvt = new QKeyEvent( type, pMap->sKey, Qt::NoModifier,"",false, (ushort)count );
    Q_ASSERT( NULL != pEvt );

    //! post event to widget
    qApp->postEvent( pWidget, pEvt );

    return true;
}


}

