#ifndef RMENUS
#define RMENUS


#include "./arch/rmenures.h"
#include "./arch/rmenubuilder.h"
#include "./arch/rdsoui.h"

#include "./arch/rmenu.h"
#include "./arch/rmenupage.h"
#include "./arch/rservmenu.h"

#include "./wnd/uiwnd.h"
#include "./wnd/rinfownd.h"
#include "./wnd/rmodalwnd.h"
#include "./wnd/rmenuwnd.h"

#include "./dsowidget/rdsocontrol.h"
#include "./dsowidget/rdsoquicklinkbutton.h"

//! menu widgets
#include "./menuwidget/rcontrol.h"
#include "./menuwidget/rchildbutton.h"
#include "./menuwidget/rcombox.h"
#include "./menuwidget/rcomboxcheck.h"
#include "./menuwidget/rinvertbutton.h"

#include "./menuwidget/rlabel_i_f_f_knob.h"
#include "./menuwidget/rlabel_i_i_f.h"
#include "./menuwidget/rlabel_icon.h"

#include "./menuwidget/rlabel_n_s_s.h"
#include "./menuwidget/rpopupview.h"
#include "./menuwidget/rpopupitem.h"
#include "./menuwidget/rpushbutton.h"

#include "./menuwidget/rtimeedit.h"
#include "./menuwidget/rdateedit.h"
#include "./menuwidget/ripedit.h"
#include "./menuwidget/rseekbar.h"

#include "./menuwidget/rcombox_iif.h"
#include "./menuwidget/rdropdowncombox_iif.h"
#include "./menuwidget/rtextbox.h"
#include "./menuwidget/rlitecombox.h"

#include "./menuwidget/rspace.h"

#include "./dsowidget/rdsogndtag.h"
#include "./dsowidget/rdsoch.h"
#include "./dsowidget/rdsodg.h"
#include "./dsowidget/rdsodgbtn.h"

#include "./dsowidget/rdsola.h"
#include "./dsowidget/rdsoline.h"

#include "./dsowidget/rdsohscale.h"
#include "./dsowidget/rdsoxyscale.h"
#include "./dsowidget/rdsorollscale.h"
#include "./dsowidget/rdsohoffset.h"
#include "./dsowidget/rdsosysstatus.h"

#include "./dsowidget/rdsotriginfo.h"
#include "./dsowidget/rdsozoomarea.h"
#include "./dsowidget/rdsomembar.h"
#include "./dsowidget/rdsochlabel.h"

//! comwidget
#include "../widget/rbutton.h"

#endif // RMENUS

