#ifndef RDSOSYSSTATUS_STYLE_H
#define RDSOSYSSTATUS_STYLE_H

#include <QtWidgets>

#include "rui_style.h"

namespace menu_res {

class RDsoSysStatus_Style : public RUI_Style
{
public:
    RDsoSysStatus_Style();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta);
public:
    void resize( QWidget *pWidget );

    QString mBackIcon;
    QString mAutoIcon;
    QString mRunIcon;
    QString mStopIcon;
    QString mWaitIcon;
    QString mTdIcon;

    QRect mRect;

    int mAnimateTime;
};

}

#endif // RDSOSYSSTATUS_STYLE_H
