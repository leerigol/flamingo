#ifndef RMENUPAGE_STYLE_H
#define RMENUPAGE_STYLE_H

#include <QtWidgets>
#include <QtGui>

#include "rui_style.h"

namespace menu_res {

/*!
 * \brief The RMenuPage_Style class
 * 菜单页的风格描述
 */
class RMenuPage_Style : public RUI_Style
{
public:
    RMenuPage_Style();

protected:
    void load( const QString &path,
               const r_meta::CMeta &meta );
public:
    void paintEvent( QPainter &painter,
                     const QRect &dimmyRect,
                     ElementStatus meStatus = element_actived );
    void resize( QWidget *pWidget );

public:
    //! item
    int mItemX, mItemH, mItemCap;

    QString mHeadIcon;
    QString mTailIcon;

    QString mHeadIconDeActive;
    QString mTailIconDeActive;

    int mWidth, mHeight;
    QColor mBgColor;

    int mHeadIconX;
    int mTailIconX, mTailIconY;

    //! color
    QColor  mTopBorderColor;
    QColor  mRightBorderColor;

    //! pos
    QPoint mPtExpand, mPtCollapse;

    //! path nodes
    int mTopBorderNodes;
    QList< QPoint > mTopBorderList;
    int mTopBorderWidth;

    //! cen nodes
    int mCentBorderNodes;
    QList< QPoint > mCentBorderList;

    //! path nodes
    int mRightBorderNodes;
    QList< QPoint > mRightBorderList;
    int mRightBorderWidth;

    int mDownBorderH;

    //! cache
    QRect mRectTopText, mRectDownText;
    QPainterPath mPathTop, mPathRight, mPathCent;
};

}

#endif // RMENUPAGE_STYLE_H
