#include "rdsomembar_style.h"

namespace menu_res {

RDsoMembar_Style::RDsoMembar_Style()
{
    mCWave.setRgb( 0xff, 0xff, 0xff );
    mCBg.setRgb( 0, 0, 0 );
    mCZoom.setRgb( 0x00, 0x10, 0x40 );
    mCRaw.setRgb( 0x70,0x70,0x70 );

    mCMain.setRgb( 0,0,0 );
}

void RDsoMembar_Style::load( const QString &path,
                             const r_meta::CMeta &meta )
{
    meta.getMetaVal( path+"color/wave", mCWave );
    meta.getMetaVal( path+"color/zoom", mCZoom );
    meta.getMetaVal( path+"color/bg", mCBg );
    meta.getMetaVal( path+"color/raw", mCRaw );
    meta.getMetaVal( path+"color/main", mCMain );
}

}

