#ifndef RDSOLABTN_STYLE_H
#define RDSOLABTN_STYLE_H

#include <QtCore>
#include <QtWidgets>

#include "../../include/dsostd.h"

#include "rui_style.h"

namespace menu_res {

class RDsoLABTN_Style : public RUI_Style
{
public:
    RDsoLABTN_Style();

protected:
    virtual void load( const QString &root,
                       const r_meta::CMeta &meta);
public:
    void paintEvent( QPainter &painter,
                     UicStatus stat,
                     quint32 dxStat,
                     quint32 activeDx );

public:
    RBar_Style frameActive, frameDisable, frameEnable, frameDown;
    QString activeNum[16], deactiveNum[16], closeNum[16];

    QRect mNumGeo[16];

    QRect mGeo, mHeader, mContent;

};

}

#endif // RDSOLABTN_Style_H
