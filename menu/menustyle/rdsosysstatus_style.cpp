#include "rdsosysstatus_style.h"

#include "../../meta/crmeta.h"

namespace menu_res {

RDsoSysStatus_Style::RDsoSysStatus_Style()
{

}

void RDsoSysStatus_Style::load( const QString &strPath,
                                const r_meta::CMeta &meta )
{

    meta.getMetaVal( strPath+"back_icon", mBackIcon );
    meta.getMetaVal( strPath+"auto_icon", mAutoIcon );
    meta.getMetaVal( strPath+"run_icon", mRunIcon );
    meta.getMetaVal( strPath+"stop_icon", mStopIcon );
    meta.getMetaVal( strPath+"wait_icon", mWaitIcon );
    meta.getMetaVal( strPath+"td_icon", mTdIcon );

    meta.getMetaVal( strPath + "rect", mRect );

    meta.getMetaVal( strPath + "animate_time", mAnimateTime );
}

void RDsoSysStatus_Style::resize( QWidget *pWidget )
{
    pWidget->setGeometry( mRect );
}

}

