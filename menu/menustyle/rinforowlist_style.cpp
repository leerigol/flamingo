
#include "rinforowlist_style.h"
#include "../../meta/crmeta.h"

namespace menu_res {

void RInfoRowList_Style::setHeightPerRow(int height)
{
    mHeightPerRow = height;
}

void RInfoRowList_Style::setColumnsNum(int num)
{
    mColumnsNum = num;
}

void RInfoRowList_Style::setColumnsWidth(QList<int> widthList)
{
    mColumnsWidthList.clear();
    mColumnsWidthList.append(widthList);
}

void RInfoRowList_Style::setChoosedRow(int row)
{
    mChoosedRow = row;
}

void RInfoRowList_Style::paintEvent(QPainter &painter, QRect rect)
{
    for(int i = 0; i < (rect.height()-29)/mHeightPerRow; i++){
        if(i%2 == 0 ){
            painter.fillRect(rect.x()+4, rect.y()+25+i*mHeightPerRow,
                             rect.width()-8, mHeightPerRow, mGrayRow);
        }else{
            painter.fillRect(rect.x()+4,rect.y()+25+i*mHeightPerRow,
                             rect.width()-8, mHeightPerRow, mBlueRow);
        }
        if(i == mChoosedRow){
            painter.fillRect(rect.x()+4, rect.y()+25+i*mHeightPerRow,
                             rect.width()-8, mHeightPerRow, mActive);
        }
    }

    _style.paint(painter, rect);

    QPen     penLine;
    penLine.setColor(mGrayLine);
    painter.setPen(penLine);

    QImage img;
    img.load(mSpliter);
    if(mColumnsNum == mColumnsWidthList.count()){
        int posX = 4;
        for(int i=1; i<mColumnsWidthList.count(); i++){
            if(mColumnsWidthList.at(i-1) > 0){
                posX += mColumnsWidthList.at(i-1);
                painter.drawImage(posX, rect.y(), img);
                painter.drawLine(posX, 25, posX, rect.height()-5);
            }
        }
    }else{
        for(int i=1; i<mColumnsNum; i++){
            painter.drawImage((rect.width()-16)/mColumnsNum*i+8, rect.y(), img);
            painter.drawLine((rect.width()-16)/mColumnsNum*i+8, 25, (rect.width()-16)/mColumnsNum*i+8, rect.height()-5);
        }
    }
}

void RInfoRowList_Style::load( const QString &path,
                               const r_meta::CMeta &rMeta )
{

    QString str = path + "rowcolor/";
    int iColor = 0;

    rMeta.getMetaVal( str + "rspliter", mSpliter );
    rMeta.getMetaVal( str + "bluerow", iColor );
    this->setColor( mBlueRow, iColor );
    rMeta.getMetaVal( str + "grayrow", iColor );
    this->setColor( mGrayRow, iColor );
    rMeta.getMetaVal( str + "grayline", iColor );
    this->setColor( mGrayLine, iColor );
    rMeta.getMetaVal( str + "active", iColor );
    this->setColor( mActive, iColor );

    _style.loadResource(path);
}

void RInfoRowList_Style::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

}

