#ifndef RSCROLLBAR_STYLE_H
#define RSCROLLBAR_STYLE_H

#include "rui_style.h"

namespace menu_res {
/*!
 * \brief The RScrollBar_Style class
 * 弹出菜单中的滚动条风格
 */
class RScrollBar_Style : public RUI_Style
{
public:
    RScrollBar_Style();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    void resize( QRect aimRect );

public:
    QString mImageHeadN, mImageHeadDown;
    QString mImageTailN, mImageTailDown;

    QString mImageHandleN, mImageHandleDown;
    QString mImageHandleHeadN, mImageHandleHeadDown;
    QString mImageHandleTailN, mImageHandleTailDown;

    QRect mHeadRect, mHandleRect;
    QPoint mBarXY;

    QColor mBgColor, mFgColor;

    //! cache
    int mTailX, mTailY;
    int mBarW, mBarSize;  //! height or width
};

}

#endif // RSCROLLBAR_STYLE_H
