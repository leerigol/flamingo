#include "rinfownd_style.h"
#include "rcontrolstyle.h"
#include "../../meta/crmeta.h"

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<"style:"<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";


#define def_time()
#define start_time()
#define end_time()

namespace menu_res {

void RInfoWnd_Style::load( const QString &path,
                           const r_meta::CMeta &rMeta )
{
    //! images
    QString picPath;
    QImage  img;
    rMeta.getMetaVal( path  + "image/ltc", picPath );
    img.load(picPath);
    mImgLtc.load(picPath);
    rMeta.getMetaVal( path  + "image/lb", picPath );
    mImgLb.load( picPath );
    rMeta.getMetaVal( path  + "image/ldc", picPath );
    mImgLdc.load( picPath );
    rMeta.getMetaVal( path  + "image/db", picPath );
    mImgDb.load( picPath );
    rMeta.getMetaVal( path  + "image/rdc", picPath );
    mImgRdc.load( picPath );
    rMeta.getMetaVal( path  + "image/rb", picPath );
    mImgRb.load( picPath );
    rMeta.getMetaVal( path  + "image/rtc", picPath );
    mImgRtc.load( picPath );
    rMeta.getMetaVal( path  + "image/tb", picPath );
    mImgTb.load( picPath );

    //! color
    rMeta.getMetaVal( path + "bg", mBgColor );

    cacheSize();
}

void RInfoWnd_Style::cacheSize()
{
    if ( !sizeLtc.isEmpty() ) return;

    cacheImgSize( mImgLtc, sizeLtc );
    cacheImgSize( mImgLb, sizeLb );
    cacheImgSize( mImgLdc, sizeLdc );
    cacheImgSize( mImgDb, sizeDb );

    cacheImgSize( mImgRdc, sizeRdc );
    cacheImgSize( mImgRb, sizeRb );
    cacheImgSize( mImgRtc, sizeRtc );
    cacheImgSize( mImgTb, sizeTb );
}

void RInfoWnd_Style::cacheImgSize( QImage &strImg, QSize &size )
{

    size.setWidth( strImg.width() );
    size.setHeight( strImg.height() );
}

void RInfoWnd_Style::paint( QPainter &painter, QRect &frameRect )
{
    def_time();
    start_time();
    //QImage img;
    painter.fillRect( frameRect.x()+sizeLtc.width(), frameRect.y()+sizeTb.height(),
                      frameRect.width()-sizeLtc.width()-sizeRtc.width(),
                      (frameRect.height()+1)/2-sizeTb.height(), mBgColor );

    painter.fillRect( frameRect.x()+sizeLb.width(), frameRect.y()+sizeLtc.height(),
                      (frameRect.width()+1)/2-sizeLb.width(),
                      frameRect.height()-sizeLtc.height()-sizeLdc.height(), mBgColor );

    painter.fillRect( frameRect.x()+(frameRect.width()+1)/2, frameRect.y()+sizeRtc.height(),
                      (frameRect.width()+1)/2-sizeRb.width(),
                      frameRect.height()-sizeRtc.height()-sizeRdc.height(), mBgColor );

    painter.fillRect( frameRect.x()+sizeLdc.width(), frameRect.y()+frameRect.height()/2,
                      frameRect.width()-sizeLdc.width()-sizeRdc.width(),
                      (frameRect.height()+1)/2-sizeDb.height(), mBgColor );

//    img.load( mImgLtc );
    painter.drawImage(0, 0, mImgLtc );

//    img.load( mImgLdc );
    painter.drawImage( 0, frameRect.height()-sizeLdc.height(), mImgLdc );

//    img.load( mImgRdc );
    painter.drawImage( frameRect.width()-sizeRdc.width(),
                       frameRect.height()-sizeRdc.height(),
                       mImgRdc );

//    img.load( mImgRtc );
    painter.drawImage( frameRect.width()-sizeRtc.width(),
                       0,
                       mImgRtc );

//    QBrush brush;
//    img.load( mImgLb );
    painter.fillRect( 0, sizeLtc.height(), sizeLb.width(),
                      frameRect.height() - sizeLtc.height()-sizeLdc.height(),
                      mImgLb);

//    img.load( mImgDb );
    RControlStyle::fillImage(painter,
                             sizeLdc.width(),
                             frameRect.height() - sizeDb.height(),
                             frameRect.width() - sizeLdc.width() - sizeRdc.width(),
                             sizeDb.height(),
                             mImgDb );

//    img.load( mImgRb );
    RControlStyle::fillImage(painter, frameRect.width() - sizeRb.width(), sizeRtc.height(),
                             sizeRb.width(), frameRect.height()-sizeRtc.height()-sizeRdc.height(),
                             mImgRb );

//    img.load( mImgTb );
    painter.fillRect( sizeLtc.width(), 0,
                      frameRect.width()-sizeLtc.width()-sizeRtc.width(),
                      sizeTb.height(), mImgTb);
    end_time();
}

void RModelWnd_Style::load( const QString &path,
                            const r_meta::CMeta &rMeta )
{
    RInfoWnd_Style::load( path, rMeta );

    rMeta.getMetaVal( path + "button/down", mDownImg );
    rMeta.getMetaVal( path + "button/normal", mNormalImg );
    rMeta.getMetaVal( path + "button/disable", mDisableImg );

    rMeta.getMetaVal( path + "button/w", mW );
    rMeta.getMetaVal( path + "button/h", mH );

    rMeta.getMetaVal( path + "titlexy", mTitleXY );
    rMeta.getMetaVal( path + "closexy", mCloseXY );
    rMeta.getMetaVal( path + "titleqss", mTitleStyle );

}

}

