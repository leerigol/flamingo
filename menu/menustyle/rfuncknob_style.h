#ifndef RFUNCKNOB_STYLE_H
#define RFUNCKNOB_STYLE_H
#include "../arch/rmenures.h"
#include "rcontrolstyle.h"

namespace menu_res {

/*!
 * \brief The RFuncKnob_View class
 * 多功能旋钮视图
 * -active/normal/disabled
 * -一个视图由3张图片组成，分别表示了不同的状态
 */
class RFuncKnob_View
{
public:
    void load( const QString &path,
               const r_meta::CMeta &meta );
    void paintEvent( QPainter &painter,
                     ElementStatus stat,
                     int x, int y );

private:
    QString strA, strN, strD;
};
/*!
 * \brief The RFuncKnob_Style class
 * -多功能旋钮风格
 * -旋钮不带按下
 * -旋钮带按下
 * -旋钮按下调出数字输入法
 */
class RFuncKnob_Style : public RControlStyle
{
public:
    RFuncKnob_Style();    
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );

public:
    void paintEvent( QPainter &painter,
                     eControlKnob view,
                     ElementStatus stat,
                     int x, int y );

private:
    RFuncKnob_View mTune, mTuneZ, mTuneP, mPad;
};

}

#endif // RFUNCKNOB_STYLE_H
