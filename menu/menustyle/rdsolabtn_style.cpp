
#include "rcontrolstyle.h"

#include "rdsolabtn_style.h"

#include "../../meta/crmeta.h"

namespace menu_res {

RDsoLABTN_Style::RDsoLABTN_Style()
{
}

void RDsoLABTN_Style::load( const QString &root,
                            const r_meta::CMeta &rMeta)
{
    //! load base
    frameActive.loadResource( root + "frame/active/", rMeta );
    frameDown.loadResource( root + "frame/down/", rMeta );
    frameEnable.loadResource( root + "frame/enable/", rMeta );
    frameDisable.loadResource( root + "frame/disable/", rMeta );

    //! image
    QString path, strAct, strDeact, strDisable;
    rMeta.getMetaVal( root + "num/path", path );

    rMeta.getMetaVal( root + "num/active", strAct );
    rMeta.getMetaVal( root + "num/deactive", strDeact );
    rMeta.getMetaVal( root + "num/disable", strDisable );

    strAct = path + strAct;
    strDeact = path + strDeact;
    strDisable = path + strDisable;

    QString strId;
    for ( int i = 0; i < 16; i++ )
    {
        strId = QString("%1").arg(i);

        activeNum[i] = strAct + strId;
        deactiveNum[i] = strDeact + strId;
        closeNum[i] = strDisable + strId;
    }

    //! image rect
    for ( int i = 0; i < 16; i++ )
    {
        strId = QString("d%1").arg(i);

        rMeta.getMetaVal( root + "num/geo/" + strId, mNumGeo[i] );
    }

    rMeta.getMetaVal( root + "rect", mGeo );
    rMeta.getMetaVal( root + "header", mHeader );
    rMeta.getMetaVal( root + "content", mContent );
}

void RDsoLABTN_Style::paintEvent( QPainter &painter,
                                  UicStatus stat,
                                  quint32 dxStat,
                                  quint32 activeDx )
{
    //! bg
    if ( stat == uic_actived )
    {
        frameActive.paint( painter );
    }
    else if ( stat == uic_enabled )
    {
        frameEnable.paint( painter );
    }
    else if ( stat == uic_downed )
    {
        frameDown.paint( painter );
    }
    else //if ( stat == ch_disabled )
    {
        frameDisable.paint( painter );
    }

    //! paint the ch
    QString strs[16];

    //! for each ch
    for ( int i=d0; i <=d15; i++ )
    {
        //! enable
        if ( get_bit( dxStat, i) )
        { strs[i-d0] = deactiveNum[i-d0];  }
        else
        { strs[i-d0] = closeNum[ i-d0]; }

        //! active and on
        if ( get_bit( activeDx, i ) && get_bit( dxStat, i) )
        { strs[ i - d0 ] = activeNum[ i-d0 ]; }
    }

    QImage img;
    for( int i = 0; i < 16; i++ )
    {
        if ( img.load( strs[i] ) )
        {
            painter.drawImage( mNumGeo[i], img );
        }
    }
}

}
