
#include "rframewithtitle_style.h"
#include "rcontrolstyle.h"
#include "../../meta/crmeta.h"

namespace menu_res {

void RFrameWithTitle_Style::paint(QPainter &painter, QRect &frameRect )
{
    QImage img, imgTL, imgTR, imgBL;
    QImage imgBR;

    imgTL.load( mTL  );
    int widthTL = imgTL.width();
    int heightTL = imgTL.height();

    imgTR.load( mTR  );
    int widthTR = imgTR.width();
    int heightTR = imgTR.height();

     imgBL.load(mBL);
    int widthBL = imgBL.width();
    int heightBL = imgBL.height();

    imgBR.load(mBR );
    int widthBR = imgBR.width();
    int heightBR = imgBR.height();

    img.load( mTop );
    for( int i = 0; i<frameRect.width()-widthTL-widthTR; i++ ){
        painter.drawImage(frameRect.x()+widthTL+i, frameRect.y(), img );
    }

    img.load( mLeft );
    for( int i = 0; i<frameRect.height()-heightTL-heightBL; i++ ){
        painter.drawImage(frameRect.x(), frameRect.y()+heightTL+i, img);
    }

    img.load( mBottom );
    int tempH = img.height();
    for(int i = 0; i < frameRect.width()-widthBL-widthBR; i++){
        painter.drawImage(frameRect.x()+widthBL+i, frameRect.y()+frameRect.height()-tempH, img);
    }

    img.load( mRight );
    int tempW = img.width();
    for(int i = 0; i < frameRect.height()-heightTR-heightBR; i++){
        painter.drawImage(frameRect.x()+frameRect.width()-tempW, frameRect.y()+heightTR+i, img);
    }

    painter.drawImage(frameRect.x(), frameRect.y(), imgTL);
    painter.drawImage(frameRect.x()+frameRect.width()-widthTR, frameRect.y(), imgTR);
    painter.drawImage(frameRect.x(), frameRect.y()+frameRect.height()-heightBL, imgBL);

    painter.drawImage(frameRect.x()+frameRect.width()-widthBR, frameRect.y()+frameRect.height()-heightBR, imgBR);
}

void RFrameWithTitle_Style::load( const QString &path,
                                  const r_meta::CMeta &rMeta )
{

    QString str = path + "framebg/";

    rMeta.getMetaVal( str + "tl", mTL );
    rMeta.getMetaVal( str + "tr", mTR );
    rMeta.getMetaVal( str + "top", mTop );
    rMeta.getMetaVal( str + "left", mLeft );
    rMeta.getMetaVal( str + "right", mRight );
    rMeta.getMetaVal( str + "bottom", mBottom );
    rMeta.getMetaVal( str + "bl", mBL );
    rMeta.getMetaVal( str + "br", mBR );
}

}

