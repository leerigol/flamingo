
#include "rcontrolstyle.h"

#include "rdsodgbtn_style.h"

#include "../../meta/crmeta.h"

namespace menu_res {

RDsoDGBTN_Style::RDsoDGBTN_Style()
{
}

void RDsoDGBTN_Style::load( const QString &root,
                            const r_meta::CMeta &rMeta )
{
    int     mColor;
    QString cStr,sinStr,squareStr;

    cStr = root + "color/";
    rMeta.getMetaVal( cStr + "activeColor", mColor );
    RControlStyle::setColor( mActiveColor, mColor );

    rMeta.getMetaVal( cStr + "enableColor", mColor );
    RControlStyle::setColor( mEnableColor, mColor );

    rMeta.getMetaVal( cStr + "disableColor", mColor );
    RControlStyle::setColor( mDisableColor, mColor );

    //! wav
    sinStr = root + "sin/";
    rMeta.getMetaVal( sinStr + "active", sinActive );
    rMeta.getMetaVal( sinStr + "enable", sinEnable );
    rMeta.getMetaVal( sinStr + "disable", sinDisable );

    squareStr = root + "square/";
    rMeta.getMetaVal( squareStr + "active", squareActive );
    rMeta.getMetaVal( squareStr + "enable", squareEnable );
    rMeta.getMetaVal( squareStr + "disable", squareDisable );

    int xpos, ypos, width, height;
    rMeta.getMetaVal( root + "xposm", xpos );
    rMeta.getMetaVal( root + "yposm", ypos );
    rMeta.getMetaVal( root + "widthm", width );
    rMeta.getMetaVal( root + "heightm", height );
    mRectName.setRect( xpos, ypos, width, height );

    rMeta.getMetaVal( root + "xposn", xpos );
    rMeta.getMetaVal( root + "yposn", ypos );
    rMeta.getMetaVal( root + "widthn", width );
    rMeta.getMetaVal( root + "heightn", height );
    mRectWav.setRect( xpos, ypos, width, height );
}

void RDsoDGBTN_Style::resize( QWidget *pWidget )
{
    Q_ASSERT( NULL != pWidget );

    pWidget->setGeometry( mRectName.x(), mRectName.y(), mRectWav.width(), mRectName.height() );
}

}
