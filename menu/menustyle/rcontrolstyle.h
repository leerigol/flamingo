#ifndef RCONTROLSTYLE
#define RCONTROLSTYLE

#include <QtCore>

#include "../../com/uiattr/uiproperty.h"

#include "./rui_type.h"
#include "./rui_style.h"

namespace menu_res {

/*!
 * \brief The RControlStyle class
 * -控件风格基类
 */
class RControlStyle : public RUI_Style
{

public:
    static void setColor( QColor &color, int iColor );
    static void renderFg( QImage &image, QColor fg,
                          QColor bg=QColor(0x0,0x0,0x0) );
    static void renderBg( QImage &image, QColor fg,
                          QColor bg=QColor(0x0,0x0,0x0) );
    static QPoint extractPt( QString &str );

    static void routeToSibling( QString &strPath, const QString &sibling );

    static ElementStatus iconStat( bool controlDown,
                                   bool controlEnable,
                                   bool tuneFocus );

    static void fillImage( QPainter &painter, QRect &rect, QImage &img );
    static void fillImage( QPainter &painter, int x, int y, int w, int h, QImage &img );

    static void drawText( QPainter &painter,
                          QRect &rect,int flag,

                          QString &str,
                          QColor &fg, QColor &bg,
                          bool bInvert = false );

    static void drawFrame( QPainter &painter,
                           const QRect &rect,
                           RBgFrame &bgFrame );
    static void drawHLine( QPainter &painter, int x, int y, int w,
                           RBgHLine &hLine );
    static void drawVLine( QPainter &painter, int x, int y, int h,
                           RBgVLine &hLine );
    static void drawDot( QPainter &painter, int x, int y,
                           RBgDot &dot,
                           int flag,
                           const QRect &rect,
                           int border = 2 );

    static void limitRange( int &a, int low, int upper );
public:
    RControlStyle();

};

/*!
 * \brief The RElementFrame class
 * 背景边框
 */
class RElementFrame
{
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );
    void paintEvent( QPainter &painter);

public:
                        //! bg frame
    QString mHeadIcon;
    QString mTailIcon;
    QString mVlineIcon;

    int mVlineSpan;

    QColor mColor;
};

class ColorGroup
{
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );
public:
    QColor operator[]( ElementStatus stat );

public:
    QColor mActivedColor;
    QColor mActivedDownColor;
    QColor mDeactivedColor;
    QColor mDisabledColor;
};

class RFontCfg
{
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );
public:
    QString mFamily;
    int mSize;
};

class RElementComm
{
public:
    RElementComm();
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );
    QColor getColor( ElementStatus stat,
                     const QString &str );
    QColor getColor( ElementStatus stat,
                     const QColor &actColor );
public:
    ColorGroup mColor;

    RFontCfg mTitleFont;
    RFontCfg mContentFont;

    QSize mSize;
    QMap<QString, QColor> mStringColor;
    bool mbLoaded;
};

class RElementStyle : public RControlStyle
{
public:
    RElementStyle();

protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    void paintEvent( QPainter &painter );

    void setStatus( ElementStatus stat );
    QColor getCurrentColor();
    QColor getColor( ElementStatus stat );

    void resize( QWidget *pParentWidget );

    void drawCaption( QPainter &painter,
                   const QRect &rect,
                   const QString &str );

    void drawText( QPainter &painter,
                   const QRect &rect,
                   const QString &str,
                   Qt::AlignmentFlag opt = Qt::AlignRight );

    void drawText1( QPainter &painter,
                   const QRect &rect,
                   const QString &str,
                   Qt::AlignmentFlag opt = Qt::AlignRight );

    void drawText( QPainter &painter,
                   const QColor &color,
                   const QRect &rect,
                   const QString &str,
                   Qt::AlignmentFlag opt = Qt::AlignRight );

    void drawIcon( QPainter &painter,
                   QImage &img,
                   const QRect &boundRect,
                   eMenuRes resType = MR_StrIcon,
                   bool bRender = true );

public:
    ElementStatus mStatus;  /*!< status */
                            /*!< bgframe */
    RElementFrame mActivedFrame;
    RElementFrame mActivedDownFrame;
    RElementFrame mDeactivedFrame;
    RElementFrame mDisabledFrame;
                            /*!< fg */
    static RElementComm mCommon;
};

class RPushButtonStyle : public RElementStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );

public:
    QRect mTextRect;

};

class RChildButtonStyle : public RElementStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    void paintEvent( QPainter &painter );

public:
    QRect mCaptionRect;

protected:
    QString mChildIcon;
    QColor mChildIconBgColor;
    QPoint mChildIconPt;
};

class RInvertButtonStyle : public RElementStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    bool getImage( QImage &img, bool bVal );
public:
    QRect mCaptionRect, mTextRect;
    QString mOnImg, mOffImg, mOnImgDis, mOffImgDis;
};

class RTimeEditStyle : public RElementStyle
{
public:
    enum SegFocus
    {
        focus_none,
        focus_l0,
        focus_l1,
        focus_l2,
        focus_l3
    };

protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    QRect mCaptionRect, mContentRect;

public:
    void drawTime( QPainter &painter, int h, int m, int s, SegFocus focus,
                   ElementStatus iconStat );

protected:
    void segNFocus( SegFocus &focus, ElementStatus iconStat );

protected:
    QColor mBgColor, mFgColor, mBorderColor;
    int    mBorderWidth, mSepWidth;
public:
    QPoint mKnobXY;
};

class RDateEditStyle : public RTimeEditStyle
{
public:
    void drawDate( QPainter &painer,
                   int y, int m, int d, SegFocus focus,
                   ElementStatus iconStat );
};

class RIpEditStyle : public RTimeEditStyle
{

public:
    void drawIp( QPainter &painter,
                 quint32 ip,
                 SegFocus focus,
                 ElementStatus iconStat );
};

class RSeekBarStyle : public RElementStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    void drawValue( QPainter &painter,
                    ui_attr::PackInt from,
                    ui_attr::PackInt to,
                    ui_attr::PackInt value,
                    float fBase,
                    const QString &preSr="",
                    const QString &postStr="",
                    ElementStatus iconStat=element_actived,
                    bool bBalloon = false );

public:
    QRect mCaptionRect, mContentRect;

protected:
    int mBarShiftY, mHandleShiftY, mBalloonShiftY, mBalloonHandleShiftY;
    int mFillHeight, mFillShiftY, mFillShiftX;


    QColor mBgColor, mFgColor;

    //! pics
    QString mStrBarLc, mStrBarRc, mStrInBar;
    QString mStrBarHandle;

    QString mStrBalloonLtc, mStrBalloonLb, mStrBalloonLdc, mStrBalloonDb;
    QString mStrBalloonRdc, mStrBalloonRb, mStrBalloonRtc, mStrBalloonTb;
    QString mStrBalloonHandle;

    int mBalloonBorder;
public:
    int mBalloonTmo;
public:
    QPoint mKnobXY;
};

class RLabel_n_s_s_Style : public RInvertButtonStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    void paintEvent( QPainter &painter );

public:
    QString mChildIcon;
    QColor  mChildIconBgColor;
    QPoint  mChildIconPt;

    QString mCheckImg;
    QString mUnCheckImg;
};

class RLabel_i_i_i_Style : public RInvertButtonStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta);
public:
    void paintEvent( QPainter &painter, ElementStatus iconStat = element_actived );

public:
    QPoint mKnobXY, mKnobKeyXY;
};

class RLabel_i_f_f_knob_Style : public RLabel_i_i_i_Style
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
};

class RLabel_i_i_f_Style : public RLabel_i_i_i_Style
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
};

class RLabel_icon_Style : public RElementStyle
{
public:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    void paintEvent(QPainter &painter,
                    ElementStatus iconStat = element_actived );

public:
    QRect mCaptionRect;

public:
    QPoint mKnobXY;
};

class RCombox_Style : public RElementStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    void paintEvent(QPainter &painter);

public:
    QRect mCaptionRect, mTextRect;

protected:
    QString mPopupIcon;
    QColor mPopupIconBg;
    QPoint mPopupXY;

public:
    QPoint mKnobXY, mKeyKnobXY;
};

class RCombox_Check_Style : public RChildButtonStyle
{
protected:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );
};

class RPopupView_Style : public RUI_Style
{
public:
    enum KnobIcon
    {
        knob_none,
        knob_tune,
        knob_tunez,
    };

public:
    RPopupView_Style();

protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );
public:
    void paintEvent(QPainter &painter, int select, int id, int count,
                    KnobIcon icon = knob_none,
                    eMenuRes resType = MR_Str);
    void resize( QWidget *pWidget, int viewCount );

    void drawCaption( QPainter &painter, const QString &str );
    void drawText( QPainter &painter, const QString &str, int yId, int yMax,
                   eMenuRes resType = MR_Str );
    void drawCheck( QPainter &painter, bool bCheck, int yId );

    void drawIcon( QPainter &painter, QImage &img, int yId,
                   eMenuRes resType = MR_StrIcon );

    QRgb getFgColor(int sta, int end,
                    int act, int now );

    QColor getColor( bool bEnable );
public:
    QRect mCaptionRect, mTextRect;
    int   mViewCount;

protected:
    QPoint mIconXY;

    //! focus icon
    QString mFocusHeadIcon;
    QString mFocusVLineIcon;
    QString mFocusTailIcon;
    int     mFocusVLineSpan;

    //! bg frame
    QString mBgHeadIcon;
    QString mBgHlineIcon;
    QString mBgTailIcon;

    //! check icon
    QString mCheckIcon;
    QString mUnCheckIcon;
    QPoint mCheckXY;

public:
    QRect mScroll;
    int mScrollBottomShift;

    //! tune icon
    QString mTuneIcon, mTuneZIcon;
    QRect mTuneRect;

    //! dummyScale;
    int mDummyScale;

    QColor mEnableColor, mDisableColor;

public:
    int     mViewCapacity;

    int     mWidth;
    int     mHeadHeight;
    int     mTailHeight;
};

}

#endif // RCONTROLSTYLE

