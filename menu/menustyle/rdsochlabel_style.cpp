#include "rdsochlabel_style.h"

namespace menu_res {

RDsoCHLabel_Style::RDsoCHLabel_Style()
{
}

void RDsoCHLabel_Style::load( const QString &path,
                              const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "style", mStyle );
    meta.getMetaVal( path + "pad", mPadXY );
}

}
