#ifndef RDSOHOFFSET_STYLE_H
#define RDSOHOFFSET_STYLE_H

#include "rui_style.h"
#include "../menustyle/rcontrolstyle.h"

#include "../../include/dsostd.h"

namespace menu_res {

class RDsoHOffset_Style :  public RButtonDoEn_Style
{
protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QRect mRectHeader, mRectContent;
    QRect mOffsetRect;
    RFontCfg mFont;
    QColor mColor;
};

}

#endif // RDSOHOFFSET_STYLE_H
