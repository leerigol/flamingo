#ifndef RPROGRESSDIALOG_STYLE_H
#define RPROGRESSDIALOG_STYLE_H

#include "rcontrolstyle.h"

namespace menu_res {
/*!
 * \brief The RProgressDialog_Style class
 * 进度对话框风格
 * -背景框
 * -动画图片
 */
class RProgressDialog_Style : public RControlStyle
{
public:
    RProgressDialog_Style();

protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );

public:
    void paintEvent( QPainter &painter,
                     const QRect &wigRect );

public:
    RBgFrame mFrame;
    QColor mFgColor;
    QString mStyleText;

    //! placement
    QRect rectAnimation, rectBar, rectText;
    QRect closeRect, titleRect;
    QRect rectWnd;

    QString mNormalImg, mDownImg, mDisableImg;

    int mAnimateCnt;
    QString mAnimatePattern;
};

}

#endif // RPROGRESSDIALOG_STYLE_H
