#ifndef RDSOCHLABEL_STYLE_H
#define RDSOCHLABEL_STYLE_H

#include "rui_style.h"

namespace menu_res {

class RDsoCHLabel_Style : public RUI_Style
{
public:
    RDsoCHLabel_Style();

public:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QString mStyle;
    QPoint mPadXY;
};

}

#endif // RDSOCHLABEL_STYLE_H
