#include "rintensitywnd_style.h"

namespace menu_res {

RIntensityWnd_Style::RIntensityWnd_Style()
{
}

void RIntensityWnd_Style::load( const QString &path,
                                const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "size", mSize );
    meta.getMetaVal( path + "style", mStyle );
    meta.getMetaVal( path + "rect", mRect );
}

}

