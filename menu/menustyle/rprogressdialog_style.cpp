#include "rprogressdialog_style.h"
#include "../../meta/crmeta.h"

namespace menu_res {

RProgressDialog_Style::RProgressDialog_Style()
{

}

void RProgressDialog_Style::load(const QString &path,
                                 const r_meta::CMeta &meta )
{
    mFrame.load( path + "frame/", meta );

    meta.getMetaVal( path + "color/fg", mFgColor );

    meta.getMetaVal( path + "rect/animate", rectAnimation );
    meta.getMetaVal( path + "rect/bar", rectBar );
    meta.getMetaVal( path + "rect/text", rectText );
    meta.getMetaVal( path + "rect/frame", rectWnd  );
    meta.getMetaVal( path + "rect/closerect", closeRect  );
    meta.getMetaVal( path + "rect/titlerect", titleRect  );

    meta.getMetaVal( path + "style/text", mStyleText );

    meta.getMetaVal( path + "animate/count", mAnimateCnt );
    meta.getMetaVal( path + "animate/pattern", mAnimatePattern );

    meta.getMetaVal( path + "button/normal", mNormalImg );
    meta.getMetaVal( path + "button/down", mDownImg );
    meta.getMetaVal( path + "button/disable", mDisableImg );
}

void RProgressDialog_Style::paintEvent( QPainter &painter,
                 const QRect &wigRect )
{
    RControlStyle::drawFrame( painter, wigRect, mFrame );
}

}
