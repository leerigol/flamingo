#ifndef RDSOTRIGINFO_STYLE_H
#define RDSOTRIGINFO_STYLE_H

#include <QtWidgets>

#include "rui_style.h"

namespace menu_res {

class RDsoTrigInfo_Style : public RButtonDoEn_Style
{

protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );

public:
    //! type icons
    QString mEdgeRiseIcon;
    QString mEdgeFallIcon;
    QString mEdgeAllIcon;

    QString mPulseRiseIcon;
    QString mPulseFallIcon;

    QString mSlopeRiseIcon;
    QString mSlopeFallIcon;

    QString mVideoIcon;

    QString mLogicIcon;
    QString mTimeoutIcon;
    QString mDelayIcon;
    QString mABIcon;

    QString mAreaIcon;
    QString mSetupHoldIcon;
    QString mRs232Icon;
    QString mSpiIcon;

    QString mI2CIcon;
    QString mI2SIcon;
    QString mLinIcon;
    QString mCanIcon;

    QString mCanFDIcon;
    QString mWindowIcon;
    QString mNEdgeIcon;
    QString mFlexRayIcon;

    QString m1553Icon;
    QString m429Icon;
    QString mSentIcon;
    QString mSBusIcon;

    QString mPatternIcon;
    QString mDurationIcon;
    QString mRuntIcon;

    QPoint mTypeIconXY;

    //! src icon
    QString mCH1Icon, mCH2Icon, mCH3Icon, mCH4Icon;
    QString mAcIcon, mExtIcon, mExt5Icon;
    QString mD0Icon, mD1Icon, mD2Icon, mD3Icon,
            mD4Icon, mD5Icon, mD6Icon, mD7Icon,
            mD8Icon, mD9Icon, mD10Icon, mD11Icon,
            mD12Icon, mD13Icon, mD14Icon, mD15Icon;
    QPoint mSrcXY;

    //! level Icon
    QString mLevelIcon_Slope;
    QPoint mLevelIconXY;

    //! ch color
    QColor mColor_CH1, mColor_CH2, mColor_CH3, mColor_CH4;
    QColor mColor_AC, mColor_Ext;
    QColor mColor_DX;

    QRect mRectLevel;

    QRect mRectSweep;
    int   mSweepSize;
    QString mSweepFont;
    QColor mSweepColor;

    QRect mRectHeader, mRectContent;
};

}

#endif // RDSOTRIGINFO_STYLE_H
