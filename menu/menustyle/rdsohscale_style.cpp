#include "rdsohscale_style.h"

#include "rcontrolstyle.h"
#include "../../meta/crmeta.h"

namespace menu_res {

void RDsoHScale_Style::load( const QString &path,
                             const r_meta::CMeta &meta )
{
    RButtonDoEn_Style::load( path + "frame/", meta );

    meta.getMetaVal( path + "header", mRectHeader );
    meta.getMetaVal( path + "content", mRectContent );

    meta.getMetaVal( path + "scale", mRectScale );
    meta.getMetaVal( path + "sa", mRectSa );
    meta.getMetaVal( path + "depth", mRectDepth );

    mFont.load( path + "font/", meta );
    mBigFont.load( path + "big_font/", meta );

    meta.getMetaVal( path + "color/scale", mColorScale );
    meta.getMetaVal( path + "color/sa", mColorSa );
    meta.getMetaVal( path + "color/depth", mColorDepth );

}

}
