
#include "../../meta/crmeta.h"

#include "rtextbox_style.h"


namespace menu_res {

RTextBox_Style::RTextBox_Style()
{

}

void RTextBox_Style::load( const QString &strPath,
                           const r_meta::CMeta &meta )
{
    QString strInvert;

    strInvert = strPath;
    RControlStyle::routeToSibling( strInvert, "invert_button/" );
    RInvertButtonStyle::load( strInvert, meta );

    meta.getMetaVal( strPath + "editbox/qss", mTextEditQss  );
    meta.getMetaVal( strPath + "editbox/shift", mShfitPoint  );
}

}

