
#include "rcontrolstyle.h"

#include "../../meta/crmeta.h"

namespace menu_res {

#define disable_icon_color  0x404040

void RControlStyle::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

//! keep bg color
void RControlStyle::renderFg( QImage &image,
                              QColor fg,
                              QColor bg )
{
    int i, j;
    uchar *pBits;
    uchar *pRow;
    uchar r, g, b;
    uchar bgR, bgG, bgB;

    r = fg.red();
    g = fg.green();
    b = fg.blue();

    bgR = bg.red();
    bgG = bg.green();
    bgB = bg.blue();

    pBits = image.bits();

    for ( i = 0; i < image.height(); i++ )
    {
        pRow = pBits + image.bytesPerLine()*i;
        for ( j = 0; j < image.width()*4; j+=4 )
        {
            if ( pRow[j+0] != bgR
                 && pRow[j+1] != bgG
                 && pRow[j+2] != bgB )
            {
                pRow[j+0] = b;
                pRow[j+1] = g;
                pRow[j+2] = r;
            }
        }
    }
}

//! replace bg by fg
void RControlStyle::renderBg( QImage &image,
                              QColor fg,
                              QColor bg )
{
    int i, j;
    uchar *pBits;
    uchar *pRow;
    uchar r, g, b;
    uchar bgR, bgG, bgB;

    r = fg.red();
    g = fg.green();
    b = fg.blue();

    bgR = bg.red();
    bgG = bg.green();
    bgB = bg.blue();

    pBits = image.bits();

    for ( i = 0; i < image.height(); i++ )
    {
        pRow = pBits + image.bytesPerLine()*i;
        for ( j = 0; j < image.width()*4; j+=4 )
        {
            if ( pRow[j+0] == bgR
                 && pRow[j+1] == bgG
                 && pRow[j+2] == bgB )
            {
                pRow[j+0] = b;
                pRow[j+1] = g;
                pRow[j+2] = r;
            }
        }
    }
}
QPoint RControlStyle::extractPt( QString &str )
{
    str.remove('(');
    str.remove(')');

    QStringList strList;
    QPoint pt;
    strList = str.split(",");

    Q_ASSERT( strList.size() == 2 );

    pt.setX( strList[0].toInt() );
    pt.setY( strList[1].toInt() );

    return pt;
}

/*!
 * \brief RControlStyle::routeToSibling
 * \param strPath
 * \param sibling
 * abc/def hij/
 * abc/hij/
 */
void RControlStyle::routeToSibling( QString &strPath,
                                           const QString &sibling )
{
    int lastSep;

    //! find the last
    lastSep = strPath.lastIndexOf('/');
    Q_ASSERT( lastSep != -1 );

    //! is last
    if ( lastSep != strPath.size() - 1 )
    {
        strPath.remove( lastSep, strPath.size() - lastSep );
    }
    else
    {
        strPath.remove( lastSep, strPath.size() - lastSep );

        lastSep = strPath.lastIndexOf('/');
        Q_ASSERT( lastSep != -1 );

        strPath.remove( lastSep, strPath.size() - lastSep );
    }

    //! cat the sibling
    if ( sibling[0] == '/' )
    {
        strPath = strPath + sibling;
    }
    else
    {
        strPath = strPath + "/" + sibling;
    }
}

ElementStatus RControlStyle::iconStat( bool controlDown,
                               bool controlEnable,
                               bool tuneFocus )
{
    if ( !controlEnable ) return element_disabled;

    if ( controlDown )  return element_actived_down;

    if ( tuneFocus ) return element_actived;

    return element_deactived;
}

void RControlStyle::fillImage( QPainter &painter, QRect &rect, QImage &img )
{
    int w,h;
    int i, j;

    w = img.width();
    h = img.height();

    for ( i = 0; i < rect.height(); i+=h )
    {
        for ( j = 0; j < rect.width(); j+=w )
        {
            painter.drawImage( j + rect.left(), i + rect.top(), img );
        }
    }
}

void RControlStyle::fillImage( QPainter &painter,
                               int x, int y, int w, int h,
                               QImage &img )
{
    QRect rect( x, y, w, h);
    fillImage( painter, rect, img );
}

void RControlStyle::drawText( QPainter &painter,
                      QRect &rect,int flag,

                      QString &str,
                      QColor &fg, QColor &bg,
                      bool bInvert )
{
    QRect boundRect;

    boundRect = painter.fontMetrics().boundingRect( rect, flag, str );

    painter.save();

    if ( bInvert )
    {
        painter.fillRect( boundRect, fg );
        painter.setPen( bg );
    }
    else
    {
        painter.setPen( fg );
    }

    painter.drawText( rect, flag, str );

    painter.restore();
}

void RControlStyle::drawFrame( QPainter &painter,
                               const QRect &rect,
                               RBgFrame &bgFrame )
{
    QRect bgRect;

    //! bg
    bgRect = rect;
    bgRect.adjust(0,0,-1,-1);
    painter.fillRect( bgRect, bgFrame.bgColor );

    QImage img;
    int start, end;

    //! ltc
    img.load( bgFrame.ltc );
    painter.drawImage( rect.left(), rect.top(), img );
    start = img.width() + rect.left();

    //! rtc
    img.load( bgFrame.rtc );
    end = rect.right() - img.width();
    painter.drawImage( end, rect.top(), img );

    //! tb
    img.load( bgFrame.tb );
    RControlStyle::fillImage( painter,
                              start, rect.top(), end - start, img.height(),
                              img );

    //! ldc
    img.load( bgFrame.ldc );
    painter.drawImage( rect.left(), rect.bottom() - img.height(), img );
    end = rect.bottom() - img.height();

    //! lb
    img.load( bgFrame.ltc );
    start = rect.top() + img.height();

    img.load( bgFrame.lb );
    fillImage( painter, rect.left(), start, img.width(), end - start, img );

    //! rdc
    img.load( bgFrame.rdc );
    end = rect.right() - img.width();
    painter.drawImage( end, rect.bottom() - img.height(), img );

    img.load( bgFrame.ldc );
    start = rect.left() + img.width();
    //! db
    img.load( bgFrame.db );
    fillImage( painter, start, rect.bottom()-img.height(),
                        end - start, img.height(),
                        img );

    //! rtc
    img.load( bgFrame.rtc );
    start = rect.top() + img.height();

    //! rb
    img.load( bgFrame.rdc );
    end = rect.bottom() - img.height();

    img.load( bgFrame.rb );
    fillImage( painter, rect.right() - img.width(), start,
                        img.width(), end - start,
                        img );
}

void RControlStyle::drawHLine( QPainter &painter, int x, int y, int w,
                       RBgHLine &hLine )
{
    QImage img;
    int start, end;

    img.load( hLine.lc );
    painter.drawImage( x, y, img );
    start = x + img.width();

    img.load( hLine.rc );
    end = x + w - img.width();
    painter.drawImage( end, y, img );

    img.load( hLine.inn );
    RControlStyle::fillImage( painter, start, y, end-start, img.height(), img );

}
void RControlStyle::drawVLine( QPainter &painter,
                               int x, int y, int h,
                               RBgVLine &line )
{
    QImage img;
    int start, end;

    img.load( line.tc );
    painter.drawImage( x, y, img );
    start = y + img.height();

    img.load( line.dc );
    end = y + h - img.height();
    painter.drawImage( x, end, img );

    img.load( line.inn );
    RControlStyle::fillImage( painter, x, start, img.width(), end-start, img );
}

void RControlStyle::drawDot( QPainter &painter,
                             int x, int y,
                             RBgDot &dot,
                             int flag,
                             const QRect &rect,
                             int border )
{
    QImage img;
    int px, py;

    img.load( dot.dot );

    px = x;
    py = y;

    if ( flag & Qt::AlignVCenter )
    {
        py = y - img.height()/2;
    }
    if ( flag & Qt::AlignHCenter )
    {
        px = x - img.width() / 2;
    }

    //! limit the range
    limitRange( px, rect.left() + border, rect.right()  - img.width() - border );
    limitRange( py, rect.top() + border , rect.bottom() - img.height() + border );

    painter.drawImage( px, py, img );
}

void RControlStyle::limitRange( int &a, int low, int upper )
{
    if ( a < low ) { a = low; }
    else if ( a > upper ) { a = upper; }
    else {}
}

RControlStyle::RControlStyle()
{
}

void RElementFrame::load( const QString &strPath,
                          const r_meta::CMeta &rMeta )
{
    rMeta.getMetaVal( strPath + "head_icon", mHeadIcon );
    rMeta.getMetaVal( strPath + "tail_icon", mTailIcon );
    rMeta.getMetaVal( strPath + "vline_icon", mVlineIcon );

    rMeta.getMetaVal( strPath + "vline_span", mVlineSpan );

    rMeta.getMetaVal( strPath + "color", mColor );
}

void RElementFrame::paintEvent( QPainter &painter )
{
    QImage image;
    int vStart, i;
    int vLineWidth;

    //! head
    image.load( mHeadIcon );
    painter.drawImage( 0, 0, image );
    vStart = image.width();

    //! vline span
    image.load( mVlineIcon );
    vLineWidth = image.width();
    for ( i = vStart; i < (vStart + mVlineSpan); i += vLineWidth )
    {
        painter.drawImage( i, 0, image );
    }

    //! tail
    image.load( mTailIcon );
    painter.drawImage( vStart + mVlineSpan, 0, image );
}

void ColorGroup::load( const QString &strPath,
                       const r_meta::CMeta &meta )
{
    meta.getMetaVal( strPath + "actived", mActivedColor );
    meta.getMetaVal( strPath + "deactived", mDeactivedColor );
    meta.getMetaVal( strPath + "actived_down", mActivedDownColor );
    meta.getMetaVal( strPath + "disabled", mDisabledColor );
}

QColor ColorGroup::operator[]( ElementStatus stat )
{
    switch( stat )
    {
        case element_actived:
            return mActivedColor;
        case element_deactived:
            return mDeactivedColor;
        case element_actived_down:
            return mActivedDownColor;
        case element_disabled:
            return mDisabledColor;
        default:
            Q_ASSERT( false );
            return mDisabledColor;
    }
}

void RFontCfg::load( const QString &strPath,
                     const r_meta::CMeta &meta )
{
    meta.getMetaVal( strPath + "size", mSize );
    meta.getMetaVal( strPath + "family", mFamily );
}

RElementComm::RElementComm()
{
    mbLoaded = false;
}
void RElementComm::load( const QString &strPath,
                         const r_meta::CMeta &meta )
{
    if ( !mbLoaded )
    { mbLoaded = true; }
    else
    { return; }

    mColor.load( strPath + "color/", meta );

    mTitleFont.load( strPath + "font/title/", meta );
    mContentFont.load( strPath + "font/content/", meta );

    meta.getMetaVal( strPath + "size", mSize );

    //! string - color loaded
    QString str;
    QStringList strList, subStrList;
    quint32 colorVal;
    bool bOK;
    meta.getMetaVal( strPath + "string_color", str );


    strList = str.split(";",QString::SkipEmptyParts);
    foreach( QString subStr, strList )
    {
        subStrList = subStr.split(",",QString::SkipEmptyParts );

        //! check size
        if ( subStrList.size() == 2 )
        {
            colorVal = subStrList[1].trimmed().toUInt( &bOK, 0 );
            if ( bOK )
            {
                RElementComm::mStringColor.insert( subStrList[0].trimmed(), QColor(colorVal));
            }
        }
    }
}

QColor RElementComm::getColor( ElementStatus stat,
                               const QString &str )
{
    if ( stat == element_disabled )
    { return mColor[stat]; }
    else if ( RElementComm::mStringColor.contains(str) )
    { return RElementComm::mStringColor[str]; }
    else
    { return mColor[stat]; }
}

QColor RElementComm::getColor( ElementStatus stat,
                 const QColor &actColor )
{
    if ( stat == element_disabled )
    { return mColor[stat]; }
    else
    { return actColor; }
}

RElementComm RElementStyle::mCommon;
RElementStyle::RElementStyle()
{
    mStatus = element_deactived;

    mInited = false;
}

void RElementStyle::load( const QString &strPath,
                          const r_meta::CMeta &meta )
{
    mCommon.load( "ui/menu/element/", meta );

    mActivedFrame.load( strPath + "actived/", meta );
    mActivedDownFrame.load( strPath + "actived_down/", meta );
    mDeactivedFrame.load( strPath + "deactived/", meta );
    mDisabledFrame.load( strPath + "disabled/", meta );
}

void RElementStyle::paintEvent( QPainter &painter )
{
    switch( mStatus )
    {
    case element_actived:
        mActivedFrame.paintEvent( painter );
        break;

    case element_actived_down:
        mActivedDownFrame.paintEvent( painter );
        break;

    case element_deactived:
        mDeactivedFrame.paintEvent( painter );
        break;

    case element_disabled:
        mDisabledFrame.paintEvent( painter );
        break;
    default:
        break;
    }
}

void RElementStyle::setStatus( ElementStatus stat )
{
    mStatus = stat;
}

QColor RElementStyle::getCurrentColor()
{
    return getColor( mStatus );
}

QColor RElementStyle::getColor( ElementStatus stat )
{
    switch( stat )
    {
    case element_actived:
        return mActivedFrame.mColor;

    case element_actived_down:
        return mActivedDownFrame.mColor;

    case element_deactived:
        return mDeactivedFrame.mColor;

    case element_disabled:
        return mDisabledFrame.mColor;

    default:
        return mActivedFrame.mColor;
    }
}

void RElementStyle::resize( QWidget *pParentWidget )
{
    Q_ASSERT( NULL != pParentWidget );

    pParentWidget->resize( mCommon.mSize.width(),
                           mCommon.mSize.height() );
}

void RElementStyle::drawCaption( QPainter &painter,
               const QRect &rect,
               const QString &str )
{
    painter.save();

    //! color
    painter.setPen( mCommon.mColor[mStatus] );

    //! font
    QFont font = qApp->font();
    font.setPointSize( mCommon.mTitleFont.mSize );
    painter.setFont( font );

    painter.drawText( rect,
                      Qt::AlignLeft | Qt::AlignVCenter,
                      str );

    painter.restore();
}

void RElementStyle::drawText( QPainter &painter,
               const QRect &rect,
               const QString &str,
               Qt::AlignmentFlag align )
{
    painter.save();

    painter.setPen( mCommon.getColor( mStatus, str) );

    //! font
    QFont font = qApp->font();
    font.setPointSize( mCommon.mContentFont.mSize );
    painter.setFont( font );

    painter.drawText( rect,
                      align | Qt::AlignVCenter,
                      str );

    painter.restore();
}

void RElementStyle::drawText1( QPainter &painter,
               const QRect &rect,
               const QString &str,
               Qt::AlignmentFlag align )
{
    painter.save();

    painter.setPen( mCommon.getColor( mStatus, str) );

    //! font
    QFont font = qApp->font();
    font.setPointSize( mCommon.mContentFont.mSize );
    painter.setFont( font );

    painter.drawText( rect,
                      align,
                      str );

    painter.restore();
}

void RElementStyle::drawText( QPainter &painter,
               const QColor &color,
               const QRect &rect,
               const QString &str,
               Qt::AlignmentFlag align )
{
    painter.save();

    QColor fgColor = mCommon.getColor( mStatus, color );
    painter.setPen( fgColor );

    //! font
    QFont font = qApp->font();
    font.setPointSize( mCommon.mContentFont.mSize );
    painter.setFont( font );

    painter.drawText( rect,
                      align | Qt::AlignVCenter,
                      str );

    painter.restore();
}

void RElementStyle::drawIcon( QPainter &painter,
               QImage &img,
               const QRect &boundRect,
               eMenuRes resType,
               bool bRender )
{
    int w, h, yShift;

    w = qMin( img.width(), boundRect.width() );
    h = qMin( img.height(), boundRect.height() );

    if ( h < boundRect.height() )
    {
        yShift = ( boundRect.height() - h )/2;
    }
    else
    {
        yShift = 0;
    }

    int x;
    if ( is_attr(resType, MR_Str) )
    { x = boundRect.left(); }
    else
    //! align to right
    { x = boundRect.left() + (boundRect.width() - w ); }

    //! image render
    if (mStatus == element_disabled && bRender )
    {
        renderFg( img, QColor(disable_icon_color) );
    }
    else
    {}


    painter.drawImage( QPoint( x, boundRect.top()+yShift),
                       img,
                       QRect(0,0,w,h) );
}

void RPushButtonStyle::load( const QString &strPath,
                             const r_meta::CMeta &meta )
{
    RElementStyle::load( strPath + "base_style/", meta );

    meta.getMetaVal( strPath + "text_rect", mTextRect );
}

void RChildButtonStyle::load( const QString &strPath,
                              const r_meta::CMeta &meta )
{
    RElementStyle::load( strPath + "base_style/", meta );

    meta.getMetaVal( strPath + "caption_rect", mCaptionRect );

    meta.getMetaVal( strPath + "child_icon", mChildIcon );
    meta.getMetaVal( strPath + "child_icon_xy", mChildIconPt );

    meta.getMetaVal( strPath + "child_bg", mChildIconBgColor );

}
void RChildButtonStyle::paintEvent( QPainter &painter )
{
    //! bg
    RElementStyle::paintEvent( painter );

    //! child icon
    QImage image;
    image.load( mChildIcon );
    RControlStyle::renderFg( image,
                             getCurrentColor(),
                             mChildIconBgColor );
    painter.drawImage( mChildIconPt, image );
}

void RInvertButtonStyle::load( const QString &strPath,
                               const r_meta::CMeta &meta )
{
    RElementStyle::load( strPath + "base_style/", meta );

    meta.getMetaVal( strPath + "caption_rect", mCaptionRect );
    meta.getMetaVal( strPath + "text_rect", mTextRect );

    meta.getMetaVal( strPath + "image/on/normal", mOnImg );
    meta.getMetaVal( strPath + "image/on/disable", mOnImgDis );
    meta.getMetaVal( strPath + "image/off/normal", mOffImg );
    meta.getMetaVal( strPath + "image/off/disable", mOffImgDis );
}

bool RInvertButtonStyle::getImage( QImage &img, bool bVal )
{
    QString imgName;
    if ( bVal )
    {
        if ( mStatus == element_disabled )
        { imgName = mOnImgDis; }
        else
        { imgName = mOnImg; }
    }
    else
    {
        if ( mStatus == element_disabled )
        { imgName = mOffImgDis; }
        else
        { imgName = mOffImg; }
    }

    return img.load( imgName );
}

void RTimeEditStyle::load( const QString &strPath,
                           const r_meta::CMeta &meta )
{
    RElementStyle::load( strPath + "base_style/", meta );

    meta.getMetaVal( strPath + "knob", mKnobXY );

    meta.getMetaVal( strPath + "caption_rect", mCaptionRect );
    meta.getMetaVal( strPath + "content_rect", mContentRect );

    //! color
    meta.getMetaVal( strPath + "color/bg", mBgColor );
    meta.getMetaVal( strPath + "color/fg", mFgColor );
    meta.getMetaVal( strPath + "color/border", mBorderColor );

    //! width
    meta.getMetaVal( strPath + "width/border", mBorderWidth );
    meta.getMetaVal( strPath + "width/sep", mSepWidth );
}

void RTimeEditStyle::drawTime( QPainter &painter,
                               int h, int m, int s,
                               SegFocus focus,
                               ElementStatus iconStat )
{
    QRect rect;
    int shift1, shift2;

    //! bg
    painter.fillRect( mContentRect, mBgColor );

    segNFocus( focus, iconStat );

    painter.setPen( mFgColor );

    rect = mContentRect;
    //! sep
    QString str=":";
    shift1 = rect.width()/2-2*mSepWidth;
    shift2 = rect.width()/2 + 1*mSepWidth;
    rect.adjust( shift1, 0, -shift2, 0 );
    painter.drawText( rect, Qt::AlignCenter, str );

    rect.adjust( 3 * mSepWidth, 0, 3 * mSepWidth, 0 );
    painter.drawText( rect, Qt::AlignCenter, str );

    //! hour
    rect = mContentRect;
    rect.adjust( 0,0, -shift2 - mSepWidth, 0 );
    str = QString("%1").arg(h);

    RControlStyle::drawText( painter, rect, Qt::AlignVCenter | Qt::AlignRight, str,
                             mFgColor, mBgColor, focus == focus_l0 );

    //! m
    rect = mContentRect;
    rect.adjust( rect.width()/2 - mSepWidth,0, - rect.width()/2 + mSepWidth, 0 );
    str = QString("%1").arg(m);

    RControlStyle::drawText( painter, rect, Qt::AlignVCenter | Qt::AlignLeft, str,
                             mFgColor, mBgColor, focus == focus_l1 );

    //! s
    rect = mContentRect;
    rect.adjust( rect.width()/2 + 2*mSepWidth,0,0, 0 );
    str = QString("%1").arg(s);

    RControlStyle::drawText( painter, rect, Qt::AlignVCenter | Qt::AlignLeft, str,
                             mFgColor, mBgColor, focus == focus_l2 );
}

void RTimeEditStyle::segNFocus( SegFocus &focus, ElementStatus iconStat )
{
    //! foucus ok
    if ( iconStat == element_actived
         || iconStat == element_actived_down )
    {}
    else
    {
        focus = focus_none;
    }
}

void RDateEditStyle::drawDate( QPainter &painter,
                               int y, int m, int d,
                               SegFocus focus,
                               ElementStatus iconStat )
{
    QRect rect;
    int shift;

    //! bg
    painter.fillRect( mContentRect, mBgColor );

    segNFocus( focus, iconStat );

    painter.setPen( mFgColor );

    rect = mContentRect;
    //! sep
    QString str="/";
    shift = rect.width() / 2 - mSepWidth;
    rect.adjust( shift, 0, -rect.width() / 2, 0 );
    painter.drawText( rect, Qt::AlignCenter, str );

    rect.adjust( 3 * mSepWidth, 0, 3 * mSepWidth, 0 );
    painter.drawText( rect, Qt::AlignCenter, str );

    //! year
    rect = mContentRect;
    rect.adjust( 0,0, -rect.width() / 2 - mSepWidth, 0 );
    str = QString("%1").arg(y);
    RControlStyle::drawText( painter, rect, Qt::AlignVCenter | Qt::AlignRight, str,
                             mFgColor, mBgColor,
                             focus == focus_l0 );

    //! month
    rect = mContentRect;
    rect.adjust( rect.width()/2, 0,  -( rect.width() / 2 - 2 * mSepWidth ), 0 );
    str = QString("%1").arg(m);
    RControlStyle::drawText( painter, rect, Qt::AlignCenter, str,
                             mFgColor, mBgColor,
                             focus == focus_l1 );
    //! day
    rect = mContentRect;
    rect.adjust( rect.width()/2 + 3 * mSepWidth,0,0, 0 );
    str = QString("%1").arg(d);
    RControlStyle::drawText( painter, rect, Qt::AlignVCenter | Qt::AlignLeft, str,
                             mFgColor, mBgColor,
                             focus == focus_l2 );
}

void RIpEditStyle::drawIp( QPainter &painter,
                           quint32 ip,
                           SegFocus focus,
                           ElementStatus iconStat )
{
    QRect rect;
    int shift;

    //! bg
    painter.fillRect( mContentRect, mBgColor );

    segNFocus( focus, iconStat );

    painter.setPen( mFgColor );

    rect = mContentRect;

    //! sep
    QString str=".";
    shift = ( rect.width() - mSepWidth ) / 2;
    rect.adjust( shift, 0, -shift, 0 );
    painter.drawText( rect, Qt::AlignCenter, str );

    rect.adjust( - 4 * mSepWidth, 0, - 4 * mSepWidth, 0 );
    painter.drawText( rect, Qt::AlignCenter, str );

    rect.adjust( 8 * mSepWidth, 0, 8 * mSepWidth, 0 );
    painter.drawText( rect, Qt::AlignCenter, str );

    //! ip 0/1/2/3
    rect = mContentRect;
    rect.setX( rect.x() + rect.width()/2 - 15*mSepWidth / 2 );
    rect.setWidth( 3 * mSepWidth );
    str = QString("%1").arg( (ip >> 24)&0xff );
    RControlStyle::drawText( painter, rect, Qt::AlignCenter, str,
                             mFgColor, mBgColor, focus == focus_l0 );

    rect.adjust( 4 * mSepWidth, 0, 4 * mSepWidth, 0 );
    str = QString("%1").arg( (ip >> 16)&0xff );
    RControlStyle::drawText( painter, rect, Qt::AlignCenter, str,
                             mFgColor, mBgColor, focus == focus_l1 );

    rect.adjust( 4 * mSepWidth, 0, 4 * mSepWidth, 0 );
    str = QString("%1").arg( (ip >> 8)&0xff );
    RControlStyle::drawText( painter, rect, Qt::AlignCenter, str,
                             mFgColor, mBgColor, focus == focus_l2 );

    rect.adjust( 4 * mSepWidth, 0, 4 * mSepWidth, 0 );
    str = QString("%1").arg( (ip >> 0)&0xff );
    RControlStyle::drawText( painter, rect, Qt::AlignCenter, str,
                             mFgColor, mBgColor, focus == focus_l3 );
}

void RSeekBarStyle::load( const QString &strPath,
                          const r_meta::CMeta &meta )
{
    if ( mInited ) return;

    RElementStyle::load( strPath + "base_style/", meta );

    meta.getMetaVal( strPath + "knob", mKnobXY );

    meta.getMetaVal( strPath + "caption_rect/", mCaptionRect );
    meta.getMetaVal( strPath + "content_rect/", mContentRect );

    //! color
    meta.getMetaVal( strPath + "color/bg", mBgColor );
    meta.getMetaVal( strPath + "color/fg", mFgColor );

    //! pics
    QString str;
    str = strPath + "bar/";
    meta.getMetaVal( str + "lc", mStrBarLc );
    meta.getMetaVal( str + "rc", mStrBarRc );
    meta.getMetaVal( str + "ibar", mStrInBar );
    meta.getMetaVal( str + "handle", mStrBarHandle );

    str = strPath + "balloon/";
    meta.getMetaVal( str + "ltc", mStrBalloonLtc );
    meta.getMetaVal( str + "ldc", mStrBalloonLdc );
    meta.getMetaVal( str + "rdc", mStrBalloonRdc );
    meta.getMetaVal( str + "rtc", mStrBalloonRtc );
    meta.getMetaVal( str + "lb", mStrBalloonLb );
    meta.getMetaVal( str + "db", mStrBalloonDb );
    meta.getMetaVal( str + "rb", mStrBalloonRb );
    meta.getMetaVal( str + "tb", mStrBalloonTb );

    meta.getMetaVal( str + "handle", mStrBalloonHandle );

    meta.getMetaVal( str + "border", mBalloonBorder );

    meta.getMetaVal( str + "timeout", mBalloonTmo );
    //! shiftY
    str = strPath + "shift/";
    meta.getMetaVal( str + "bar", mBarShiftY );
    meta.getMetaVal( str + "handle", mHandleShiftY );
    meta.getMetaVal( str + "balloon", mBalloonShiftY );
    meta.getMetaVal( str + "balloon_handle", mBalloonHandleShiftY );

    meta.getMetaVal( str + "fill_x", mFillShiftX );
    meta.getMetaVal( str + "fill_y", mFillShiftY );
    meta.getMetaVal( str + "fill_h", mFillHeight );
}

void RSeekBarStyle::drawValue(
                QPainter &painter,
                ui_attr::PackInt from,
                ui_attr::PackInt to,
                ui_attr::PackInt value,
                float fBase,
                const QString &preStr, const QString &postStr,
                ElementStatus /*iconStat*/,
                bool bBalloon )
{
    QImage img;
    int i, left, right;

    //! draw the bar
    img.load( mStrBarLc );
    painter.drawImage( mContentRect.left(), mContentRect.bottom() + mBarShiftY,
                       img );
    left = mContentRect.left() + img.width();

    img.load( mStrBarRc );
    painter.drawImage( mContentRect.right() - img.width(),
                       mContentRect.bottom() + mBarShiftY,
                       img );
    right = mContentRect.right() - img.width();


    img.load( mStrInBar );

    for ( i = left; i < right; i+= img.width() )
    {
        painter.drawImage( i, mContentRect.bottom() + mBarShiftY, img );
    }

    //! draw the handle
    img.load( mStrBarHandle );
    int fullSpan, pos;

    fullSpan = mContentRect.width() - img.width();
    if ( value.width==32 )
    {
        pos = mContentRect.left() + img.width()/2 +
                (value.val32-from.val32)*fullSpan/(to.val32-from.val32 );
    }
    else if ( value.width == 64 )
    {
        pos = mContentRect.left() + img.width()/2 +
                (value.val64-from.val64)*fullSpan/(to.val64-from.val64 );
    }
    else
    {
        Q_ASSERT( false );
    }

    QLinearGradient grad( mContentRect.left() + mFillShiftX,
                          mContentRect.bottom() + mFillShiftY,
                          pos,
                          mContentRect.bottom() + mFillShiftY);
    grad.setColorAt(0, mBgColor );
    grad.setColorAt(1, mFgColor );
    QBrush brush( grad );
    QPainterPath painterPath;

    painterPath.moveTo( mContentRect.left() + mFillShiftX,
                        mContentRect.bottom() + mFillShiftY );
    painterPath.lineTo( pos,mContentRect.bottom() + mFillShiftY );
    painterPath.lineTo( pos,mContentRect.bottom() + mFillShiftY + mFillHeight );
    painterPath.lineTo( mContentRect.left() + mFillShiftX,
                        mContentRect.bottom() + mFillShiftY + mFillHeight );
    painterPath.closeSubpath();

    painter.fillPath( painterPath, brush );

    //! draw the handle
    painter.drawImage( pos - img.width()/2, mContentRect.bottom() + mHandleShiftY, img );

    //! the balloon
    QString str;
    int balloonWidth, balloonHeight;
    if ( value.width==32 )
    {
        str = QString("%1%2%3").arg(preStr).arg(value.val32*fBase).arg(postStr);
    }
    else if ( value.width == 64 )
    {
        str = QString("%1%2%3").arg(preStr).arg(value.val64*fBase).arg(postStr);
    }
    else
    {
        Q_ASSERT( false );
    }

    QFontMetrics fontMetrics = painter.fontMetrics();
    balloonWidth = fontMetrics.width( str );
    balloonHeight = fontMetrics.height( );

    balloonWidth += mBalloonBorder *2;
    balloonHeight += mBalloonBorder * 2;

    //! the baloon pos
    left = pos - balloonWidth/2;
    if ( left < mContentRect.left() )
    {
        left = mContentRect.left();
    }
    right = left + balloonWidth;

    if ( right > mContentRect.right() )
    {
        right = mContentRect.right();
        left = right - balloonWidth;
    }

    //! no balloon
    if ( !bBalloon ) return;

    QRect balloonRect( left, mContentRect.bottom() + mBalloonShiftY - balloonHeight,
                       balloonWidth, balloonHeight );
    painter.fillRect( balloonRect.adjusted(0,0,-1,-1), mBgColor );

    int start;
    img.load( mStrBalloonLtc );
    painter.drawImage( balloonRect.left(), balloonRect.top(), img );

    start = balloonRect.top() + img.height();
    img.load( mStrBalloonLb );
    for( ; start < balloonRect.bottom(); start += img.height() )
    {
        painter.drawImage( balloonRect.left(), start, img );
    }

    img.load( mStrBalloonLdc );
    painter.drawImage( balloonRect.left(), balloonRect.bottom() - img.height(), img );

    start = balloonRect.left() + img.width();
    img.load( mStrBalloonDb );
    for ( ; start < balloonRect.right(); start += img.width() )
    {
         painter.drawImage( start, balloonRect.bottom()-img.height(), img );
    }

    img.load( mStrBalloonRdc );
    painter.drawImage( balloonRect.right() - img.width(), balloonRect.bottom() - img.height(), img );

    start = balloonRect.bottom() - img.height();
    img.load( mStrBalloonRb );
    start -= img.height();
    for ( ; start > balloonRect.top(); start -= img.height() )
    {
        painter.drawImage( balloonRect.right() - img.width(), start, img );
    }

    img.load( mStrBalloonRtc );
    painter.drawImage( balloonRect.right() - img.width(), balloonRect.top(), img );

    start = balloonRect.right() - img.width();
    img.load( mStrBalloonTb );
    start -= img.width();
    for ( ; start > balloonRect.left(); start -= img.width() )
    {
        painter.drawImage( start, balloonRect.top(), img );
    }

    img.load( mStrBalloonLtc );
    painter.drawImage( balloonRect.left(), balloonRect.top(), img );

    painter.setPen( mFgColor );
    painter.drawText( balloonRect, Qt::AlignCenter, str );

    //! draw the balloon handle
    img.load( mStrBalloonHandle );
    painter.drawImage( pos-img.width()/2, mContentRect.bottom() + mBalloonHandleShiftY, img );
}

void RLabel_n_s_s_Style::load( const QString &strPath,
                               const r_meta::CMeta &meta )
{
    QString str;

    str = strPath;

    RControlStyle::routeToSibling( str, "/invert_button/" );

    //! load from parent
    RInvertButtonStyle::load( str, meta );

    meta.getMetaVal( strPath + "child_icon", mChildIcon );
    meta.getMetaVal( strPath + "child_icon_xy", mChildIconPt );
    meta.getMetaVal( strPath + "child_bg", mChildIconBgColor );

    meta.getMetaVal( strPath + "text_rect", mTextRect );

    meta.getMetaVal( strPath + "checked",   mCheckImg );
    meta.getMetaVal( strPath + "unchecked", mUnCheckImg );
}

void RLabel_n_s_s_Style::paintEvent(QPainter &painter)
{
    //! bg
    RElementStyle::paintEvent( painter );

    //! child icon
    QImage image;
    image.load( mChildIcon );
    RControlStyle::renderFg( image,
                             getCurrentColor(),
                             mChildIconBgColor );
    painter.drawImage( mChildIconPt, image );
}

void RLabel_i_i_i_Style::load( const QString &strPath,
                               const r_meta::CMeta &meta )
{
    QString str;

    str = strPath;

    RControlStyle::routeToSibling( str, "/invert_button/");

    //! load from parent
    RInvertButtonStyle::load( str, meta );

    meta.getMetaVal( strPath + "knob", mKnobXY );

    meta.getMetaVal( strPath + "keyknob", mKnobKeyXY );
}

void RLabel_i_i_i_Style::paintEvent( QPainter &painter,
                                     ElementStatus /*iconStat*/ )
{
    RInvertButtonStyle::paintEvent( painter );
}

void RLabel_i_f_f_knob_Style::load( const QString &strPath,
                                    const r_meta::CMeta &meta )
{
    QString str;

    str = strPath;

    RControlStyle::routeToSibling( str, "/label_iii/");

    RLabel_i_i_i_Style::load( str, meta );
}

void RLabel_i_i_f_Style::load( const QString &strPath,
                               const r_meta::CMeta &meta )
{
    QString str;

    str = strPath;

    RControlStyle::routeToSibling( str, "/label_iii/");

    RLabel_i_i_i_Style::load( str, meta );
}

void RLabel_icon_Style::load( const QString &strPath,
                              const r_meta::CMeta &meta )
{
    QString str;

    str = strPath;

    RControlStyle::routeToSibling( str, "/invert_button/base_style/");

    RElementStyle::load( str, meta );

    meta.getMetaVal( strPath + "caption_rect/", mCaptionRect );

    meta.getMetaVal( strPath + "knob", mKnobXY );
}
void RLabel_icon_Style::paintEvent(QPainter &painter,
                                   ElementStatus /*stat*/ )
{
    RElementStyle::paintEvent( painter );
}

void RCombox_Style::load( const QString &strPath,
                          const r_meta::CMeta &meta )
{
    QString str;

    str = strPath;

    //! to sibling
    RControlStyle::routeToSibling( str, "/invert_button/base_style/" );

    RElementStyle::load( str, meta );

    meta.getMetaVal( strPath + "knob", mKnobXY );

    meta.getMetaVal( strPath + "keyknob", mKeyKnobXY );

    meta.getMetaVal( strPath + "caption_rect", mCaptionRect );
    meta.getMetaVal( strPath + "text_rect", mTextRect );

    meta.getMetaVal( strPath + "popup_icon", mPopupIcon );
    meta.getMetaVal( strPath + "popup_xy", mPopupXY );
    meta.getMetaVal( strPath + "popup_icon_bg", mPopupIconBg );
}

void RCombox_Style::paintEvent(QPainter &painter)
{
    RElementStyle::paintEvent( painter );

    QImage image;
    image.load( mPopupIcon );
    RControlStyle::renderFg( image,
                             getCurrentColor(),
                             mPopupIconBg );
    painter.drawImage( mPopupXY, image );

    painter.setPen( getCurrentColor() );
}

void RCombox_Check_Style::load( const QString &strPath,
                                const r_meta::CMeta &meta )
{
    QString str;

    str = strPath;

    RControlStyle::routeToSibling( str, "/child_button/");

    RChildButtonStyle::load( str, meta );
}

RPopupView_Style::RPopupView_Style()
{
    mInited = false;
}

void RPopupView_Style::load( const QString &strPath,
                             const r_meta::CMeta &meta )
{
    //! caption
    meta.getMetaVal( strPath + "caption_rect", mCaptionRect );
    meta.getMetaVal( strPath + "text_rect", mTextRect );

    meta.getMetaVal( strPath + "icon_xy", mIconXY );

    //! icon
    meta.getMetaVal( strPath + "focus/head_icon", mFocusHeadIcon );
    meta.getMetaVal( strPath + "focus/line_icon", mFocusVLineIcon );
    meta.getMetaVal( strPath + "focus/tail_icon", mFocusTailIcon );
    meta.getMetaVal( strPath + "focus/vline_span", mFocusVLineSpan );

    meta.getMetaVal( strPath + "bg/head_icon", mBgHeadIcon  );
    meta.getMetaVal( strPath + "bg/line_icon", mBgHlineIcon  );
    meta.getMetaVal( strPath + "bg/tail_icon", mBgTailIcon  );

    meta.getMetaVal( strPath + "view_capacity", mViewCapacity );
    meta.getMetaVal( strPath + "color/enable", mEnableColor );
    meta.getMetaVal( strPath + "color/disable", mDisableColor );

    //! check icon
    meta.getMetaVal( strPath + "check/checked", mCheckIcon );
    meta.getMetaVal( strPath + "check/unchecked", mUnCheckIcon );
    meta.getMetaVal( strPath + "check/xy", mCheckXY );

    //! scroll
    meta.getMetaVal( strPath + "scroll/rect/", mScroll );
    meta.getMetaVal( strPath + "scroll/bt_shift", mScrollBottomShift );

    //! knob
    meta.getMetaVal( strPath + "knob/tune", mTuneIcon );
    meta.getMetaVal( strPath + "knob/tune_z", mTuneZIcon );

    meta.getMetaVal( strPath + "knob/rect", mTuneRect );

    meta.getMetaVal( strPath + "dummy", mDummyScale );

    //! cache paras
    QImage image;
    image.load( mBgHeadIcon );

    mWidth = image.width();
    mHeadHeight = image.height();

    image.load( mBgTailIcon );
    mTailHeight = image.height();
}

void RPopupView_Style::paintEvent(QPainter &painter,

                                   int select,      //! select in view count
                                   int /*modelId*/, //! id in model
                                   int modelCnt,    //! count of model
                                   KnobIcon knobIcon,
                                   eMenuRes resType )
{
    QImage image;
    int vStart, i, vSpan;
    int focusHeadWidth;

    vSpan = mViewCount * mTextRect.height();

    //! top head
    image.load( mBgHeadIcon );
    painter.drawImage( 0, 0, image );

    //! vLine
    image.load( mBgHlineIcon );
    for ( i = 0; i < vSpan; i += image.height() )
    {
        painter.drawImage( 0, i + mHeadHeight, image );
    }

    //! tail
    image.load( mBgTailIcon );
    painter.drawImage( 0, mHeadHeight + vSpan, image );

    //! focus
    int focusStart, focusVLine;

    if ( is_attr(resType, MR_StrIcon) )
    {
        focusStart = mTextRect.left();
        focusVLine = mFocusVLineSpan;
    }
    else
    {
        focusStart = mTextRect.left() + mIconXY.x();
        focusVLine = mFocusVLineSpan - mIconXY.x();
    }

    vStart = mHeadHeight + select * mTextRect.height();
    image.load( mFocusHeadIcon );
    painter.drawImage( focusStart, vStart, image );
    focusHeadWidth = image.width();

    image.load( mFocusVLineIcon );
    for ( i = 0; i < focusVLine; i += image.width() )
    {
        painter.drawImage( focusStart + focusHeadWidth + i, vStart, image );
    }

    image.load( mFocusTailIcon );
    painter.drawImage( focusStart + focusHeadWidth + focusVLine, vStart, image );

    //! draw knob
    if ( knobIcon == knob_tune )
    {
        image.load( mTuneIcon );
        painter.drawImage( mTuneRect.topLeft(), image );
    }
    else if ( knobIcon == knob_tunez )
    {
        image.load( mTuneZIcon);
        painter.drawImage( mTuneRect.topLeft(), image );
    }
    else
    {}

    //! not full
    if ( modelCnt <= mViewCapacity )
    {
        return;
    }
}

void RPopupView_Style::resize( QWidget *pWidget, int viewCount )
{
    Q_ASSERT( pWidget != NULL );

    //! limit the view count
    if ( viewCount > mViewCapacity )
    {
        mViewCount = mViewCapacity;
    }
    else
    {
        mViewCount = viewCount;
    }

    pWidget->resize( mWidth, mViewCount * mTextRect.height() + mHeadHeight + mTailHeight );
}

void RPopupView_Style::drawCaption( QPainter &painter,
                                     const QString &str )
{
    painter.setPen( Qt::white );

    painter.drawText( mCaptionRect,
                      Qt::AlignHCenter | Qt::AlignVCenter,
                      str );

}
void RPopupView_Style::drawText( QPainter &painter,
                                 const QString &str,
                                 int yId, int yMax,
                                 eMenuRes resType )
{
    QRect rect;
    QRect sepLine;
    if ( is_attr( resType, MR_Icon ) )
    {
        rect = mTextRect.adjusted( 0,
                                   yId * mTextRect.height(),
                                   0,
                                   yId * mTextRect.height() );
    }
    else
    {
        rect = mTextRect.adjusted( mIconXY.x(),
                                   yId * mTextRect.height(),
                                   0,
                                   yId * mTextRect.height() );
    }

    painter.drawText( rect,
                      Qt::AlignCenter,
                      str );
    //! draw the sep line
    if ( yId < yMax )
    {
        sepLine = rect;
        sepLine.adjust( rect.width()/4, 0, -rect.width()/4, 0 );
        painter.setPen( Qt::gray );
        painter.drawLine( sepLine.bottomLeft(), sepLine.bottomRight() );
    }
}

void RPopupView_Style::drawCheck( QPainter &painter, bool bCheck, int yId )
{
    QRect iconRect;

    //! ref to the text area
    iconRect = mTextRect.adjusted( mCheckXY.x(),
                                   yId * mTextRect.height() + mCheckXY.y(),
                                   0,
                                   yId * mTextRect.height() + mCheckXY.y() );

    QImage image;
    if ( bCheck )
    {
        image.load( mCheckIcon );
    }
    else
    {
        image.load( mUnCheckIcon );
    }

    painter.drawImage( mCheckXY.x(), iconRect.top(),
                       image );
}

void RPopupView_Style::drawIcon( QPainter &painter,
                                 QImage &img,
                                 int yId,
                                 eMenuRes resType )
{
    QRect iconRect;
    int x, y, w, h;

    //! ref to the text area
    iconRect = mTextRect.adjusted( mIconXY.x(),
                                   yId * mTextRect.height() + mIconXY.y(),
                                   -mTextRect.width(),
                                   yId * mTextRect.height() + mIconXY.y() );

    //! to v center
    if ( img.height() < mTextRect.height() )
    {
        y = iconRect.top() + ( mTextRect.height() - img.height()) / 2 ;
    }
    else
    {
        y = iconRect.top();
    }

    //! image
    w = qMin( img.width(), iconRect.width() );
    h = qMin( img.height(), iconRect.height() );

    if ( is_attr(resType, MR_Str) )
    { x = iconRect.left(); }
    else
    {
        int areaWidth = iconRect.width() + mTextRect.width();
        w = qMin( img.width(), areaWidth );
        x = iconRect.left() + ( areaWidth - w )/2;
    }

    painter.drawImage( QPoint(x, y), img,
                       QRect(0,0,w,h) );
}

QRgb RPopupView_Style::getFgColor( int sta, int end,
                                   int act,
                                     int now )
{
    QRgb fgColor = 0xffffff;
    int step;

    if ( now < act )
    {
        Q_ASSERT( act-sta > 0 );
        step = ( 0xff - mDummyScale ) * (now-sta)/ (act-sta) + mDummyScale;
    }
    else if ( now > act )
    {
        Q_ASSERT( end-act > 0 );
        step = ( 0xff - mDummyScale ) * (end-now)/ (end-act) + mDummyScale;
    }
    else
    {
        step = 0xff;
    }

    fgColor = (step<<16) | (step<<8) | (step);

    return fgColor;
}

QColor RPopupView_Style::getColor( bool bEnable )
{
    return bEnable ? mEnableColor : mDisableColor;
}

}
