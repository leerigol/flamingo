#ifndef RINTENSITYWND_STYLE_H
#define RINTENSITYWND_STYLE_H

#include "rcontrolstyle.h"

namespace menu_res {

class RIntensityWnd_Style : public RControlStyle
{
public:
    RIntensityWnd_Style();

protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );

public:
    QRect mSize;
    QString mStyle;
    QRect mRect;
};

}

#endif // RINTENSITYWND_STYLE_H
