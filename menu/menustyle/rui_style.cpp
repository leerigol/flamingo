#include "rui_style.h"

#include "rcontrolstyle.h"

namespace menu_res {

void RBgFrame::load( const QString &strPath,
                     const r_meta::CMeta &rMeta )
{
    rMeta.getMetaVal( strPath + "bg", bgColor );
    rMeta.getMetaVal( strPath + "fg", fgColor );

    rMeta.getMetaVal( strPath + "ltc", ltc );
    rMeta.getMetaVal( strPath + "lb", lb );

    rMeta.getMetaVal( strPath + "ldc", ldc );
    rMeta.getMetaVal( strPath + "db", db );

    rMeta.getMetaVal( strPath + "rdc", rdc );
    rMeta.getMetaVal( strPath + "rb", rb );

    rMeta.getMetaVal( strPath + "rtc", rtc );
    rMeta.getMetaVal( strPath + "tb", tb );
}

void RBgHLine::load( const QString &strPath,
                     const r_meta::CMeta &rMeta )
{
    rMeta.getMetaVal( strPath + "lc", lc );
    rMeta.getMetaVal( strPath + "rc", rc );
    rMeta.getMetaVal( strPath + "inn", inn );
}

void RBgVLine::load( const QString &strPath,
                     const r_meta::CMeta &rMeta )
{

    rMeta.getMetaVal( strPath + "lc", tc );
    rMeta.getMetaVal( strPath + "rc", dc );
    rMeta.getMetaVal( strPath + "inn", inn );
}

void RBgDot::load( const QString &strPath,
                   const r_meta::CMeta &rMeta )
{
    rMeta.getMetaVal( strPath + "dot", dot );
}

RUI_Style::RUI_Style() : mInited(false)
{
}

void RUI_Style::loadResource(
                   const QString &path,
                   const r_meta::CMeta &meta )
{
    if ( mInited ) return;

    load( path, meta );

    mInited = true;
}
void RUI_Style::reloadResource(
                     const QString &path,
                     const r_meta::CMeta &meta )
{
    mInited = false;

    load( path, meta );

    mInited = true;
}

void RUI_Style::load(
                      const QString &/*path*/,
                      const r_meta::CMeta &/*meta*/ )
{
}

/*!
 * \brief RUI_Style::loadRect
 * \param meta
 * \param path
 * \param rect
 * -从元数据表中加载矩形
 * -矩形的描述是:x/y/w/h
 */
void RUI_Style::loadRect( r_meta::CRMeta &meta,
                          const QString &path,
                          QRect &rect )
{
    int x, y, w, h;

    meta.getMetaVal( path + "x", x );
    meta.getMetaVal( path + "y", y );
    meta.getMetaVal( path + "w", w );
    meta.getMetaVal( path + "h", h );

    rect.setRect( x, y, w, h );
}

void RBar_Style::load(const QString &path,
                      const r_meta::CMeta &meta )
{
    int iColor;

    //! strings
    meta.getMetaVal( path + "head", mHead );
    meta.getMetaVal( path + "bar", mBar );
    meta.getMetaVal( path + "tail", mTail );

    meta.getMetaVal( path + "span", mSpan );

    meta.getMetaVal( path + "color", iColor );

    RControlStyle::setColor( mColor, iColor );
}

void RBar_Style::paint( QPainter &painter )
{
    QImage img;
    int w, i;

    //! head
    img.load( mHead );
    painter.drawImage( 0, 0, img );
    w = img.width();

    //! draw span
    img.load( mBar );
    for ( i = 0; i < mSpan; i++ )
    {
        painter.drawImage( w + i, 0, img );
    }

    //! draw Tail
    img.load( mTail );
    painter.drawImage( w + mSpan, 0, img );

}

void RButtonDoEn_Style::load( const QString &path,
                              const r_meta::CMeta &meta )
{
    downStyle.loadResource( path + "down/", meta );
    enableStyle.loadResource( path + "enable/", meta );

    meta.getMetaVal( path + "rect", mRect );

}

void RButtonDoEn_Style::paintEvent( QPainter &painter, UicStatus stat )
{
    if ( stat == uic_downed )
    {
        downStyle.paint( painter );
    }
    else if ( stat == uic_enabled )
    {
        enableStyle.paint( painter );
    }
    else
    {}
}

void RButtonDoEn_Style::resize( QWidget *pWidget )
{
    Q_ASSERT( pWidget != NULL );
    pWidget->setGeometry( mRect );
}

}

