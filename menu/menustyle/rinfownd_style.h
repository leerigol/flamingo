#ifndef RINFOWND_STYLE_H
#define RINFOWND_STYLE_H

#include "rui_style.h"

namespace menu_res {
/*!
 * \brief The RInfoWnd_Style class
 * 非模式窗口风格
 * -边框8+背景
 * -边框8位置
 */
class RInfoWnd_Style : public RUI_Style
{
public:
    void paint( QPainter &painter, QRect &frameRect );

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

    void cacheSize();
    void cacheImgSize( QImage &strImg, QSize &size );
public:
    QImage  mImgLtc;
    QImage mImgLb;
    QImage mImgLdc;
    QImage mImgDb;
    QImage mImgRdc;
    QImage mImgRb;
    QImage mImgRtc;
    QImage mImgTb;

    QColor mBgColor;

private:
    QSize sizeLtc, sizeLb, sizeLdc, sizeDb, sizeRdc, sizeRb, sizeRtc, sizeTb;
};
/*!
 * \brief The RModelWnd_Style class
 * 模式对话框风格
 * -继承非模式对话框
 * -关闭按钮：普通/按下/禁用图片
 * -关闭按钮位置
 */
class RModelWnd_Style : public RInfoWnd_Style
{

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QString mNormalImg, mDownImg, mDisableImg;
    int mW, mH;
    QPoint  mTitleXY, mCloseXY;
    QString mTitleStyle;
};

}

#endif // RINFOWND_STYLE_H
