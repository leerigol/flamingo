#include "rmenupage_style.h"

#include "../../meta/crmeta.h"
#include "../menustyle/rcontrolstyle.h"

namespace menu_res {

RMenuPage_Style::RMenuPage_Style()
{  
}

void RMenuPage_Style::load( const QString &path,
                            const r_meta::CMeta &meta )
{

    //! width height
    meta.getMetaVal( path + "w", mWidth );
    meta.getMetaVal( path + "h", mHeight );

    meta.getMetaVal( path + "bg_color", mBgColor );

    meta.getMetaVal( path + "item_x", mItemX );
    meta.getMetaVal( path + "item_h", mItemH );
    meta.getMetaVal( path + "item_cap", mItemCap );

    //! icon
    //! active
    meta.getMetaVal( path + "icon/active/head", mHeadIcon );
    meta.getMetaVal( path + "icon/active/tail", mTailIcon );

    //! deactive
    meta.getMetaVal( path + "icon/deactive/head", mHeadIconDeActive );
    meta.getMetaVal( path + "icon/deactive/tail", mTailIconDeActive );

    meta.getMetaVal( path + "head_x", mHeadIconX );
    meta.getMetaVal( path + "tail_x", mTailIconX );
    meta.getMetaVal( path + "tail_y", mTailIconY );

    //! border
    QString str, strNode, strNodeFmt, valNode;
    QPoint pt;

    int i;

    str = path + "top_border/";
    meta.getMetaVal( str + "color", mTopBorderColor );

    meta.getMetaVal( str + "width", mTopBorderWidth );
    meta.getMetaVal( str + "count", mTopBorderNodes );

    strNodeFmt = str + "node%1";
    for ( i = 1; i <= mTopBorderNodes; i++ )
    {
        strNode = strNodeFmt.arg(i);
        meta.getMetaVal( strNode, valNode );
        pt = RControlStyle::extractPt( valNode );
        mTopBorderList.append( pt );
    }

    //! right border
    str = path + "right_border/";
    meta.getMetaVal( str + "color", mRightBorderColor );

    meta.getMetaVal( str + "width", mRightBorderWidth );
    meta.getMetaVal( str + "count", mRightBorderNodes );

    strNodeFmt = str + "node%1";
    for ( i = 1; i <= mRightBorderNodes; i++ )
    {
        strNode = strNodeFmt.arg(i);
        meta.getMetaVal( strNode, valNode );
        pt = RControlStyle::extractPt( valNode );
        mRightBorderList.append( pt );
    }

    //! cent border
    str = path + "cen_border/";

    meta.getMetaVal( str + "count", mCentBorderNodes );

    strNodeFmt = str + "node%1";
    for ( i = 1; i <= mCentBorderNodes; i++ )
    {
        strNode = strNodeFmt.arg(i);
        meta.getMetaVal( strNode, valNode );
        pt = RControlStyle::extractPt( valNode );
        mCentBorderList.append( pt );
    }

    //! create cache
    //! create path
    mPathTop.moveTo( mTopBorderList[0] );
    for ( i = 1; i < mTopBorderList.size(); i++ )
    {
        mPathTop.lineTo( mTopBorderList[i] );
    }

    mPathRight.moveTo( mRightBorderList[0] );
    for ( i = 1; i < mRightBorderList.size(); i++ )
    {
        mPathRight.lineTo( mRightBorderList[i] );
    }

    mPathCent.moveTo( mCentBorderList[0] );
    for ( i = 1; i < mCentBorderList.size(); i++ )
    {
        mPathCent.lineTo( mCentBorderList[i] );
    }
}

void RMenuPage_Style::paintEvent( QPainter &painter,
                                  const QRect &dimmyRect,
                                  ElementStatus stat )
{
    QPen borderPen;
    QImage image;

    //! fill bg
    painter.fillPath( mPathCent, mBgColor );

    //! head
    QString strIcon;
    strIcon = ( stat == element_actived ? mHeadIcon : mHeadIconDeActive );
    image.load( strIcon );
    painter.drawImage( mHeadIconX, 0, image );

    painter.save();

    //! active line
    borderPen.setColor( mTopBorderColor );
    borderPen.setWidth( mTopBorderWidth );
    borderPen.setStyle( Qt::SolidLine );
    borderPen.setCapStyle( Qt::RoundCap );

    painter.setPen( borderPen );

    //! top path
    painter.drawPath( mPathTop );

    //! line
    painter.drawLine( dimmyRect.bottomRight(),
                      mCentBorderList[ 1 ] );

    painter.restore();

    //! right border
    painter.save();
    borderPen.setColor( mRightBorderColor );
    borderPen.setWidth( mRightBorderWidth );

    painter.setPen( borderPen );
    painter.drawPath( mPathRight );
    painter.restore();

    //! tail
    strIcon = ( stat == element_actived ? mTailIcon : mTailIconDeActive );
    image.load( strIcon );
    painter.drawImage( mTailIconX, mTailIconY, image );
}

void RMenuPage_Style::resize( QWidget *pWidget )
{
    pWidget->resize( mWidth, mHeight );
}

}

