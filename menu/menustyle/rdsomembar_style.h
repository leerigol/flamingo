#ifndef RDSOMEMBAR_STYLE_H
#define RDSOMEMBAR_STYLE_H

#include "rui_style.h"

namespace menu_res {

class RDsoMembar_Style : public RUI_Style
{
public:
    RDsoMembar_Style();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QColor mCWave, mCBg, mCZoom, mCRaw, mCMain;

};

}

#endif // RDSOMEMBAR_STYLE_H
