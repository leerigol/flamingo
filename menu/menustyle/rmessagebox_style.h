#ifndef RMESSAGEBOX_STYLE_H
#define RMESSAGEBOX_STYLE_H

#include "rcontrolstyle.h"

namespace menu_res {

class RMessageBox_Style : public RControlStyle
{
public:
    RMessageBox_Style();
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );

public:
    RBgFrame mFrame;
    QRect mRect;

    RFontCfg mFontPrompt;
    QColor mFgColor;

    QRect mIconRect, mPromptRect;

    QString mIconWarn, mIconQues, mIconInfo;

    QString mIconStyle, mPromptStyle;
};

}

#endif // RMESSAGEBOX_STYLE_H
