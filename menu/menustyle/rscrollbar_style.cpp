#include "rscrollbar_style.h"

namespace menu_res {

RScrollBar_Style::RScrollBar_Style()
{

}

void RScrollBar_Style::load( const QString &path,
                             const r_meta::CMeta &meta )
{

    //! images
    meta.getMetaVal( path+"image/head/normal", mImageHeadN );
    meta.getMetaVal( path+"image/head/down", mImageHeadDown );

    meta.getMetaVal( path+"image/tail/normal", mImageTailN );
    meta.getMetaVal( path+"image/tail/down", mImageTailDown );

    meta.getMetaVal( path+"image/handle/mid/normal", mImageHandleN );
    meta.getMetaVal( path+"image/handle/mid/down", mImageHandleDown );

    meta.getMetaVal( path+"image/handle/head/normal", mImageHandleHeadN );
    meta.getMetaVal( path+"image/handle/head/down", mImageHandleHeadDown );

    meta.getMetaVal( path+"image/handle/tail/normal", mImageHandleTailN );
    meta.getMetaVal( path+"image/handle/tail/down", mImageHandleTailDown );

    //! coord
    meta.getMetaVal( path + "pos/head", mHeadRect );
    meta.getMetaVal( path + "pos/handle", mHandleRect );

    meta.getMetaVal( path + "pos/bar", mBarXY );

    //! color
    meta.getMetaVal( path + "color/bg", mBgColor );
    meta.getMetaVal( path + "color/fg", mFgColor );
}

void RScrollBar_Style::resize( QRect aimRect )
{
    mTailX = mHeadRect.x();
    mTailY = aimRect.height() - mHeadRect.y() - mHeadRect.height();

    mBarSize = aimRect.height() - 2 * mBarXY.y();
    mBarW = aimRect.width() - 2 *mBarXY.x();
}

}
