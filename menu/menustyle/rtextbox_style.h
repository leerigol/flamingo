#ifndef RTEXTBOX_STYLE_H
#define RTEXTBOX_STYLE_H

#include "rcontrolstyle.h"

namespace menu_res {

/*!
 * \brief The RTextBox_Style class
 * 输入框显示风格
 */
class RTextBox_Style : public RInvertButtonStyle
{
public:
    RTextBox_Style();

protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );

public:
    QString mTextEditQss;   /*!< 编辑框显示风格 */
    QPoint mShfitPoint;
};

}

#endif // RTEXTBOX_STYLE_H
