#ifndef RUI_STYLE_H
#define RUI_STYLE_H

#include <QtCore>

#include "../../meta/crmeta.h"
#include "../../include/dsostd.h"
#include "rui_type.h"

namespace menu_res {

/*!
 * \brief The RBgFrame class
 * -背景边框
 * -8段图+背景颜色
 */
class RBgFrame
{
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );
public:
    QColor bgColor, fgColor;
    QString ltc, lb, ldc, db, rdc, rb, rtc, tb;
};
/*!
 * \brief The RBgHLine class
 * -背景横线
 * -左角/中间/右角
 */
class RBgHLine
{
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );

public:
    QString lc, inn, rc;
};
/*!
 * \brief The RBgVLine class
 * -垂直线
 * -顶角/底角/中间
 */
class RBgVLine
{
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );

public:
    QString tc, inn, dc;
};
/*!
 * \brief The RBgDot class
 * -背景点
 * -只有一个图片
 */
class RBgDot
{
public:
    void load( const QString &strPath,
               const r_meta::CMeta &meta );

public:
    QString dot;
};

/*!
 * \brief The RUI_Style class
 * -ui元素的风格基类
 * -每个ui元素通过风格进行参数化，ui控件显示过程中使用对应风格的数据
 * -ui风格中存储的一般是位置，图片等信息
 * -大多数控件只需要加载一次风格
 * -派生类中需要重载load方法
 */
class RUI_Style
{
public:
    RUI_Style();

public:
    void loadResource( const QString &path, const r_meta::CMeta &meta = r_meta::CRMeta() );
    void reloadResource( const QString &path, const r_meta::CMeta &meta = r_meta::CRMeta() );

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

    void loadRect( r_meta::CRMeta &meta,
                   const QString &path,
                   QRect &rect );

protected:
    bool mInited;
};

/*!
 * \brief The RBar_Style class
 * -横条风格
 * -头/中间条/尾部图片
 * -中间条扩展宽度
 * -颜色
 */
class RBar_Style : public RUI_Style
{
protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    void paint( QPainter &painter );

public:
    QString mHead, mBar, mTail;
    int mSpan;
    QColor mColor;
};

/*!
 * \brief The RButtonDoEn_Style class
 * -横条型按键
 * -包含按下和允许两个横条风格
 * -x/y/w/h
 */
class RButtonDoEn_Style : public RUI_Style
{
protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );
public:
    void paintEvent( QPainter &painter, UicStatus stat );

    void resize( QWidget *pWidget );
public:
    RBar_Style downStyle;
    RBar_Style enableStyle;

    QRect mRect;
};

}

#endif // RUI_STYLE_H
