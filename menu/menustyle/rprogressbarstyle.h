#ifndef RPROGRESSBARSTYLE_H
#define RPROGRESSBARSTYLE_H

#include "rcontrolstyle.h"

namespace menu_res {

/*!
 * \brief The RProgressBarStyle class
 * 进度条风格描述
 * -横线风格：横线用于表示进度
 * -进度指示数字背景
 * -进度指示三角背景
 * -进度的颜色a/b,颜色a/b作为线性渐变
 * -3个基本元素的位置
 */
class RProgressBarStyle : public RControlStyle
{
protected:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );

public:
    void paintBar( QPainter &painter,
                     int min, int max, int val,
                   const QRect &wigRect );
    void paintText( QPainter &painter,
                       int min, int max, int val,
                       const QRect &wigRect,
                       const QString &text);

public:
    RBgHLine barLine;
    RBgFrame textFrame;
    RBgDot   handleDot;

    QColor mLineAColor, mLineBColor;
    QColor mBgColor, mFgColor;
    int mBarY, mTextY, mHandleY;
    int mFillShiftX, mFillShiftY;
    int mTextBorder;
};

}

#endif // RPROGRESSBARSTYLE_H
