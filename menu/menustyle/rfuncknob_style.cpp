#include "rfuncknob_style.h"

#include "../../meta/crmeta.h"

namespace menu_res {

void RFuncKnob_View::load( const QString &strPath,
                           const r_meta::CMeta &rMeta )
{
    rMeta.getMetaVal( strPath + "active", strA );
    rMeta.getMetaVal( strPath + "normal", strN );
    rMeta.getMetaVal( strPath + "disable", strD );

}
void RFuncKnob_View::paintEvent( QPainter &painter,
                 ElementStatus stat,
                 int x, int y )
{
    QString str;

    switch( stat )
    {
        case element_actived:
        case element_actived_down:
            str = strA;
            break;

        case element_deactived:
            str = strN;
            break;

        case element_disabled:
            str = strD;
            break;

        default:
            Q_ASSERT( false );
            return;
    }

    QImage img;
    img.load( str );

    painter.drawImage( x, y, img );
}

RFuncKnob_Style::RFuncKnob_Style()
{
}

void RFuncKnob_Style::load(const QString &path,
                           const r_meta::CMeta &meta )
{
    mTune.load( path + "tune/", meta );
    mTuneZ.load( path + "tunez/", meta );
    mTuneP.load( path + "tunep/", meta );
    mPad.load( path + "pad/", meta );
}

void RFuncKnob_Style::paintEvent( QPainter &painter,
                 eControlKnob view,
                 ElementStatus stat,
                 int x, int y )
{
    switch( view )
    {
    case control_knob_tune:
        mTune.paintEvent( painter, stat, x, y );
        break;

    case control_knob_tune_z:
        mTuneZ.paintEvent( painter, stat, x, y );
        break;

    case control_knob_tune_pad:
        mTuneP.paintEvent( painter, stat, x, y );
        break;

    case control_knob_pad:
        mPad.paintEvent( painter, stat, x, y );
        break;

    default:
        Q_ASSERT( false );
        return;
    }
}

}

