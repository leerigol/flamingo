#include "rmessagebox_style.h"

namespace menu_res {

RMessageBox_Style::RMessageBox_Style()
{
}

void RMessageBox_Style::load(const QString &path,
                             const r_meta::CMeta &meta )
{
    mFrame.load( path + "frame/", meta );

    meta.getMetaVal( path + "frame/geo", mRect );

    mFontPrompt.load( path + "font/prompt/", meta );

    meta.getMetaVal( path + "color/fg", mFgColor );

    meta.getMetaVal( path + "rect/icon", mIconRect );
    meta.getMetaVal( path + "rect/prompt", mPromptRect );

    meta.getMetaVal( path + "icon/warn", mIconWarn );
    meta.getMetaVal( path + "icon/ques", mIconQues );
    meta.getMetaVal( path + "icon/info", mIconInfo );

    meta.getMetaVal( path + "style/prompt", mPromptStyle );
    meta.getMetaVal( path + "style/icon", mIconStyle );
}

}

