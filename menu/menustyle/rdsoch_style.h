#ifndef RDSOCH_STYLE_H
#define RDSOCH_STYLE_H

#include <QtCore>
#include <QtWidgets>

#include "../../include/dsostd.h"

#include "rui_style.h"

namespace menu_res {

/*!
 * \brief The RDsoCH_Style class
 * 通道控件风格
 */
class RDsoCH_Style : public RUI_Style
{
public:
    RDsoCH_Style();

protected:
    virtual void load( const QString &root,
                       const r_meta::CMeta &meta );
public:
    void paintEvent( QPainter &painter, UicStatus status );

    void resize( QWidget *pWidget );
    QColor getStatusColor( UicStatus stat );
    QColor getHeadColor( UicStatus stat );

public:

    RBar_Style frameActive;
    RBar_Style frameEnable;
    RBar_Style frameDown;
    RBar_Style frameDisable;

    QPainterPath coupDc, coupAc;

                        //! coord
    QRect rectScale, rectB, rectOffset, rectCoup;
    QRect mRectHeader, mRectContent;
    QRect mRectInvert, mRectVernier;
    QPoint impXY;

    int mX, mY;
    QSize mSize;

    QString mFont;
    int mPtSize;

    QString ac1, ac2, ac3, ac4, acDis;
    QString dc1, dc2, dc3, dc4, dcDis;

};

}

#endif // RDSOCH_STYLE_H
