#include "rprogressbarstyle.h"
#include "../../meta/crmeta.h"
namespace menu_res {

void RProgressBarStyle::load( const QString &strPath,
                              const r_meta::CMeta &meta )
{
    //! load frames
    barLine.load( strPath + "bar/", meta );
    textFrame.load( strPath + "textframe/", meta );
    handleDot.load( strPath + "handle/", meta );


    meta.getMetaVal( strPath + "color/a", mLineAColor );

    meta.getMetaVal( strPath + "color/b", mLineBColor );

    meta.getMetaVal( strPath + "color/bg", mBgColor  );

    meta.getMetaVal( strPath + "color/fg", mFgColor );

    //! y
    meta.getMetaVal( strPath + "shift/bary", mBarY );
    meta.getMetaVal( strPath + "shift/texty", mTextY );
    meta.getMetaVal( strPath + "shift/handley", mHandleY );
    meta.getMetaVal( strPath + "shift/fillx", mFillShiftX );
    meta.getMetaVal( strPath + "shift/filly", mFillShiftY );

    meta.getMetaVal( strPath + "text/border", mTextBorder );
}

void RProgressBarStyle::paintBar( QPainter &painter,
                 int min, int max, int val, const QRect &wigRect )
{
    //! bar frame
    RControlStyle::drawHLine( painter, wigRect.left(),
                                       wigRect.bottom() + mBarY, wigRect.width(), barLine );

    //! linear color
    QRect fillRect;
    int pos;

    fillRect = wigRect;
    fillRect.adjust( mFillShiftX, wigRect.height() + mBarY + mFillShiftY, -mFillShiftX, -mFillShiftY );

    Q_ASSERT( max != min );
    pos = (qlonglong)( val - min ) * fillRect.width()/(max-min);
    if ( pos < 0 ) pos = 0;
    else if ( pos > fillRect.width() ) pos = fillRect.width();
    else {}

    QLinearGradient grad ( fillRect.left(), fillRect.top(),
                           pos + fillRect.left(), fillRect.top() );
    grad.setColorAt( 0, mLineAColor );
    grad.setColorAt( 1, mLineBColor );

    QPainterPath painterPath;

    painterPath.moveTo( fillRect.left(), fillRect.top() );
    painterPath.lineTo( fillRect.left() + pos, fillRect.top() );
    painterPath.lineTo( fillRect.left() + pos, fillRect.bottom() );
    painterPath.lineTo( fillRect.left(), fillRect.bottom() );
    painterPath.closeSubpath();

    painter.fillPath( painterPath, QBrush(grad) );
}

void RProgressBarStyle::paintText( QPainter &painter,
                   int min, int max, int val,
                   const QRect &wigRect,
                   const QString &text )
{
    int pos, fillWidth;
    int left, right;

    fillWidth = wigRect.width() - 2 * mFillShiftX;

    Q_ASSERT( max != min );
    pos = (qlonglong)( val - min ) * fillWidth / (max-min);
    if ( pos < 0 ) pos = 0;
    else if ( pos > fillWidth ) pos = fillWidth;
    else {}

    int textW, textH;

    QFontMetrics fontMetrics = painter.fontMetrics();
    textW = fontMetrics.width( text );
    textH = fontMetrics.height( );

    textW += mTextBorder * 2;
    textH += mTextBorder * 2;

    //! physical pos
    pos += mFillShiftX + wigRect.left();
    left = pos - textW / 2;
    if ( left < wigRect.left() )
    {
        left = wigRect.left();
    }

    right = left + textW;
    if ( right > wigRect.right() )
    {
        right = wigRect.right();
        left = right - textW;
    }
    //! draw handle
    RControlStyle::drawDot( painter, pos, wigRect.bottom() + mHandleY,
                                              handleDot, Qt::AlignHCenter,
                                              wigRect );

    QRect textRect( left, wigRect.bottom() + mTextY - textH,
                       textW, textH );

    //! draw
    RControlStyle::drawFrame( painter, textRect, textFrame );

    //! draw text
    painter.save();

    painter.setPen( mFgColor );
    painter.drawText( textRect, Qt::AlignCenter, text );

    painter.restore();

}

}

