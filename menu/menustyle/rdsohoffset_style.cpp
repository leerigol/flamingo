#include "rdsohoffset_style.h"

#include "rcontrolstyle.h"
#include "../../meta/crmeta.h"

namespace menu_res {


void RDsoHOffset_Style::load( const QString &path,
                              const r_meta::CMeta &meta )
{
    RButtonDoEn_Style::load( path + "frame/", meta  );

    meta.getMetaVal( path + "header", mRectHeader );
    meta.getMetaVal( path + "content", mRectContent );
    meta.getMetaVal( path + "offset", mOffsetRect );

    mFont.load( path + "font/", meta );
    meta.getMetaVal( path + "color", mColor );
}

}

