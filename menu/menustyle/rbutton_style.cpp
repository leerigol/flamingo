#include "rbutton_style.h"

namespace menu_res {

RButton_Style::RButton_Style()
{

}

void RButton_Style::load(const QString &path,
                         const r_meta::CMeta &meta )
{
    bgA.load( path + "active/", meta );
    bgN.load( path + "normal/", meta );
    bgD.load( path + "disable/", meta );

    bgAD.load( path + "down/", meta );

    bgC.load(path + "checked/", meta);
    bgCD.load(path + "checkeddown/", meta);

    meta.getMetaVal(path + "font/size", mFontSize);
    meta.getMetaVal(path + "font/family", mFontFamily);
}

void RButton_Style::paintEvent( QPainter &painter,
                                const QRect &wigRect,
                                const QString &text,
                                const QString &imgFullName,
                                ElementStatus stat )
{
    //! draw frame
    if ( stat == element_actived )
    {
        RControlStyle::drawFrame( painter, wigRect, bgA );
        painter.setPen( bgA.fgColor );
    }
    else if ( stat == element_actived_down )
    {
        RControlStyle::drawFrame( painter, wigRect, bgAD );
        painter.setPen( bgAD.fgColor );
    }
    else if ( stat == element_deactived )
    {
        RControlStyle::drawFrame( painter, wigRect, bgN );
        painter.setPen( bgN.fgColor );
    }
    else if ( stat == element_checked_down )
    {
        RControlStyle::drawFrame( painter, wigRect, bgCD );
        painter.setPen( bgCD.fgColor );
    }
    else if ( stat == element_checked )
    {
        RControlStyle::drawFrame( painter, wigRect, bgC );
        painter.setPen( bgC.fgColor );
    }
    else //if ( stat == element_disabled)
    {
        RControlStyle::drawFrame( painter, wigRect, bgD );
        painter.setPen( bgD.fgColor );
    }

    //! draw image
    if ( imgFullName.size() > 0 )
    {
        QImage img;
        img.load( imgFullName );

        int w, h, x, y;
        w = img.width();
        h = img.height();

        x = ( wigRect.width() - w )/2;
        y = ( wigRect.height() - h )/2;

        x = x < 0 ? 0 : x;
        y = y < 0 ? 0 : y;

        painter.drawImage( x, y, img );
    }

    QFont font;
    font.setFamily(mFontFamily);
    font.setPointSize(mFontSize);
    painter.setFont(font);
    painter.drawText( wigRect, Qt::AlignCenter, text );
}

}
