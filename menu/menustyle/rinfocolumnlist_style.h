#ifndef RINFOCOLUMNLIST_STYLE_H
#define RINFOCOLUMNLIST_STYLE_H
#include "rframewithtitle_style.h"
//#include <QRect>
//#include <QPainter>

namespace menu_res {
/*!
 * 带标题行列表风格
 * -边框8+背景
 * -边框8位置
 */
class RInfoColumnList_Style : public RUI_Style
{
public:
    void setHeightPerRow(int height);
    void setColumnsNum(int num);
    void setColumnsWidth(QList<int> widthList);
    void setChoosedRow(int row);
    void paintEvent(QPainter &painter, QRect rect);

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );
    void setColor( QColor &color, int hexRgb );

private:
    menu_res::RFrameWithTitle_Style _style;

    QString mSpliter;

    QColor  mBlueRow;
    QColor  mGrayRow;
    QColor  mGrayLine;
    QColor  mActive;

    int     mHeightPerRow;
    int     mColumnsNum;
    int     mChoosedRow;

    QList<int> mColumnsWidthList;

};
}
#endif // RINFOCOLUMNLIST_STYLE_H

