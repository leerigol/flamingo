#ifndef RDSODGBTN_STYLE_H
#define RDSODGBTN_STYLE_H

#include <QtCore>
#include <QtWidgets>

#include "../../include/dsostd.h"

#include "rui_style.h"

namespace menu_res {

class RDsoDGBTN_Style : public RUI_Style
{
public:
    RDsoDGBTN_Style();

protected:
    virtual void load( const QString &root,
                       const r_meta::CMeta &meta );
public:
    void    resize( QWidget *pWidget );

public:
    QRect   mRectName,mRectWav;
    QColor  mActiveColor,mEnableColor,mDisableColor;
    int     alpha;

    QString sinActive, sinEnable, sinDisable;
    QString squareActive, squareEnable, squareDisable;

};

}

#endif // RDSODGBTN_Style_H
