#ifndef RBUTTON_STYLE_H
#define RBUTTON_STYLE_H

#include "rcontrolstyle.h"

namespace menu_res {

/*!
 * \brief The RButton_Style class
 * 按钮风格
 * -active/normal/diable
 */
class RButton_Style : public RControlStyle
{
public:
    RButton_Style();    

protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );

public:
    void paintEvent( QPainter &painter,
                     const QRect &wigRect,
                     const QString &text,
                     const QString &img,
                     ElementStatus stat );

public:
    RBgFrame bgA, bgN, bgD, bgAD, bgC, bgCD;
    QString  mFontFamily;
    int      mFontSize;
};

}

#endif // RBUTTON_STYLE_H
