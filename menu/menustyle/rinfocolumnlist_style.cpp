
#include "rinfocolumnlist_style.h"
#include "../../meta/crmeta.h"

namespace menu_res {

void RInfoColumnList_Style::setHeightPerRow(int height)
{
    mHeightPerRow = height;
}

void RInfoColumnList_Style::setColumnsNum(int num)
{
    mColumnsNum = num;
}

void RInfoColumnList_Style::setColumnsWidth(QList<int> widthList)
{
    mColumnsWidthList.clear();
    mColumnsWidthList.append(widthList);
}

void RInfoColumnList_Style::setChoosedRow(int row)
{
    mChoosedRow = row;
}

void RInfoColumnList_Style::paintEvent(QPainter &painter, QRect rect)
{
    QRect mBoxRect = rect;
    painter.fillRect(mBoxRect.x()+4, mBoxRect.y()+25, mColumnsWidthList.first(), 15*mHeightPerRow, QColor(24,27,39,255));
    for(int i = 1; i < 5; i++){
        if(i%2 == 0 )
            painter.fillRect(mBoxRect.x()+mColumnsWidthList.at(i)*i, mBoxRect.y()+25, mColumnsWidthList.at(i), 15*mHeightPerRow, QColor(24,27,39,255));
        else
            painter.fillRect(mBoxRect.x()+mColumnsWidthList.at(i)*i, mBoxRect.y()+25, mColumnsWidthList.at(i), 15*mHeightPerRow, QColor(39,39,39,255));
    }
    painter.fillRect(mBoxRect.x()+mColumnsWidthList.last()*5, mBoxRect.y()+25, mColumnsWidthList.last()-1, 15*mHeightPerRow, QColor(39,39,39,255));


    painter.setPen(QPen(QColor(0,70,140,255)));
    for(int i = 1; i < 15; i++){
        painter.drawLine(mBoxRect.x()+4, mBoxRect.y()+25+22*i, mBoxRect.x()+mBoxRect.width()-4,mBoxRect.y()+25+22*i  );
    }

    painter.setPen(QPen(QColor(51,51,51,255)));

    int px1 = mBoxRect.x();
    int px2 = 0;
    if( mColumnsWidthList.count() == mColumnsNum && mColumnsNum%2 == 0){
        for(int i = 0; i < mColumnsNum/2; i++){

            px1 += mColumnsWidthList.at(2*i);
            px2 = mColumnsWidthList.at(2*i+1);
            for(int j = 1; j < 15; j++){
                int py1 = mBoxRect.y()+25+22*j;
                painter.drawLine(px1,py1,px1+px2,py1);
            }
            px1 = px1+px2;
        }
    }

    _style.paint(painter, rect);

    QPen     penLine;
    penLine.setColor(mGrayLine);
    painter.setPen(penLine);

    QImage img;
    img.load(mSpliter);
    if(mColumnsNum == mColumnsWidthList.count()){
        int posX = 0;
        for(int i=1; i<mColumnsWidthList.count(); i++){
            if(mColumnsWidthList.at(i-1) > 0){
                posX += mColumnsWidthList.at(i-1);
                painter.drawImage(posX, rect.y(), img);
                painter.drawLine(posX, 25, posX, rect.height()-5);
            }
        }
    }else{
        for(int i=1; i<mColumnsNum; i++){
            painter.drawImage(rect.width()/mColumnsNum*i, rect.y(), img);
            painter.drawLine(rect.width()/mColumnsNum*i, 25, rect.width()/mColumnsNum*i, rect.height()-4);
        }
    }
}

void RInfoColumnList_Style::load( const QString &path,
                                  const r_meta::CMeta &rMeta )
{

    QString str = path + "rowcolor/";

    rMeta.getMetaVal( str + "rspliter", mSpliter );
    rMeta.getMetaVal( str + "bluerow", mBlueRow );
    rMeta.getMetaVal( str + "grayrow", mGrayRow );
    rMeta.getMetaVal( str + "grayline", mGrayLine );
    rMeta.getMetaVal( str + "active", mActive );

    _style.loadResource(path);
}

void RInfoColumnList_Style::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

}

