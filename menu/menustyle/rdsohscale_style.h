#ifndef RDSOHSCALE_STYLE_H
#define RDSOHSCALE_STYLE_H

//#include "rdsobasebutton_style.h"
#include "rui_style.h"
#include "../menustyle/rcontrolstyle.h"
#include "../../include/dsostd.h"

namespace menu_res {

class RDsoHScale_Style : public RButtonDoEn_Style
{
protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QRect mRectHeader, mRectContent;
    QRect mRectSa, mRectDepth, mRectScale;
    RFontCfg mFont;

    RFontCfg mBigFont;

    QColor mColorSa, mColorDepth, mColorScale;
};

}

#endif // RDSOHSCALE_STYLE_H
