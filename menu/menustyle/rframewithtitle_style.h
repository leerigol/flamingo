#ifndef RFRAMEWITHTITLE_STYLE_H
#define RFRAMEWITHTITLE_STYLE_H

#include "rui_style.h"

namespace menu_res {

/*!
 * 带标题窗口风格
 * -边框8+背景
 * -边框8位置
 */
class RFrameWithTitle_Style : public RUI_Style
{
public:
    void paint(QPainter &painter, QRect &frameRect );

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta);

private:
    QString mTL, mTop, mTR;
    QString mLeft, mRight;
    QString mBL, mBottom;
    QString mBR;

};

}

#endif // RFRAMEWITHTITLE_STYLE_H


