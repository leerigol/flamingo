#include "rdsotriginfo_style.h"

#include "../menustyle/rcontrolstyle.h"
#include "../../meta/crmeta.h"

namespace menu_res {

void RDsoTrigInfo_Style::load( const QString &strPath,
                               const r_meta::CMeta &meta )
{
    //! base style
    RButtonDoEn_Style::load( strPath+"frame/", meta );

    meta.getMetaVal( strPath + "header", mRectHeader );
    meta.getMetaVal( strPath + "content", mRectContent );

    QString str;

    //! type icon
    str = strPath + "type_icon/";

    meta.getMetaVal( str + "edge/rise", mEdgeRiseIcon );
    meta.getMetaVal( str + "edge/fall", mEdgeFallIcon );
    meta.getMetaVal( str + "edge/all",  mEdgeAllIcon );

    meta.getMetaVal( str + "pulse/rise", mPulseRiseIcon );
    meta.getMetaVal( str + "pulse/fall", mPulseFallIcon );

    meta.getMetaVal( str + "slope/rise", mSlopeRiseIcon );
    meta.getMetaVal( str + "slope/fall", mSlopeFallIcon );


    meta.getMetaVal( str + "video", mVideoIcon );

    meta.getMetaVal( str + "logic", mLogicIcon );
    meta.getMetaVal( str + "timeout", mTimeoutIcon );
    meta.getMetaVal( str + "delay", mDelayIcon );
    meta.getMetaVal( str + "ab", mABIcon );

    meta.getMetaVal( str + "area", mAreaIcon );
    meta.getMetaVal( str + "setuphold", mSetupHoldIcon );
    meta.getMetaVal( str + "rs232", mRs232Icon );
    meta.getMetaVal( str + "spi", mSpiIcon );

    meta.getMetaVal( str + "i2c", mI2CIcon );
    meta.getMetaVal( str + "i2s", mI2SIcon );
    meta.getMetaVal( str + "lin", mLinIcon );
    meta.getMetaVal( str + "can", mCanIcon );

    meta.getMetaVal( str + "canfd", mCanFDIcon );
    meta.getMetaVal( str + "window", mWindowIcon );
    meta.getMetaVal( str + "nedge", mNEdgeIcon );
    meta.getMetaVal( str + "flexray", mFlexRayIcon );

    meta.getMetaVal( str + "m1553", m1553Icon );
    meta.getMetaVal( str + "a429", m429Icon );
    meta.getMetaVal( str + "sent", mSentIcon );
    meta.getMetaVal( str + "sbus", mSBusIcon );

    meta.getMetaVal( str + "pattern", mPatternIcon );
    meta.getMetaVal( str + "duration", mDurationIcon );
    meta.getMetaVal( str + "runt", mRuntIcon );

    meta.getMetaVal( str + "xy", mTypeIconXY );

    //! ch icon
    str = strPath + "ch_icon/";

    meta.getMetaVal( str + "ch1", mCH1Icon );
    meta.getMetaVal( str + "ch2", mCH2Icon );
    meta.getMetaVal( str + "ch3", mCH3Icon );
    meta.getMetaVal( str + "ch4", mCH4Icon );

    meta.getMetaVal( str + "ac", mAcIcon );
    meta.getMetaVal( str + "ext", mExtIcon );
    meta.getMetaVal( str + "ext5", mExt5Icon );

    meta.getMetaVal( str + "d0", mD0Icon );
    meta.getMetaVal( str + "d1", mD1Icon );
    meta.getMetaVal( str + "d2", mD2Icon );
    meta.getMetaVal( str + "d3", mD3Icon );

    meta.getMetaVal( str + "d4", mD4Icon );
    meta.getMetaVal( str + "d5", mD5Icon );
    meta.getMetaVal( str + "d6", mD6Icon );
    meta.getMetaVal( str + "d7", mD7Icon );

    meta.getMetaVal( str + "d8", mD8Icon );
    meta.getMetaVal( str + "d9", mD9Icon );
    meta.getMetaVal( str + "d10", mD10Icon );
    meta.getMetaVal( str + "d11", mD11Icon );

    meta.getMetaVal( str + "d12", mD12Icon );
    meta.getMetaVal( str + "d13", mD13Icon );
    meta.getMetaVal( str + "d14", mD14Icon );
    meta.getMetaVal( str + "d15", mD15Icon );

    meta.getMetaVal( str + "xy", mSrcXY );

    //! level icon
    str = strPath + "level_icon/";
    meta.getMetaVal( str + "slope", mLevelIcon_Slope );

    meta.getMetaVal( str + "xy", mLevelIconXY );

    //! level rect
    str = strPath + "level/";
    meta.getMetaVal( str + "rect", mRectLevel );

    //! color
    str = strPath + "color/";

    meta.getMetaVal( str + "ch1", mColor_CH1 );
    meta.getMetaVal( str + "ch2", mColor_CH2 );
    meta.getMetaVal( str + "ch3", mColor_CH3 );
    meta.getMetaVal( str + "ch4", mColor_CH4 );

    meta.getMetaVal( str + "ac", mColor_AC );
    meta.getMetaVal( str + "ext", mColor_Ext );
    meta.getMetaVal( str + "dx", mColor_DX );

    //! sweep
    str = strPath + "sweep/";
    meta.getMetaVal( str + "rect", mRectSweep );
    meta.getMetaVal( str + "font/size", mSweepSize );
    meta.getMetaVal( str + "font/family", mSweepFont );
    meta.getMetaVal( str + "color", mSweepColor );
}

}

