#ifndef RUI_TYPE
#define RUI_TYPE

namespace menu_res {

/*!
 * \brief The ElementStatus enum
 * ui元素状态
 */
enum ElementStatus
{
    element_actived,        //! 活动
    element_actived_down,   //! 活动且按下
    element_deactived,      //! 非活动
    element_disabled,       //! 禁用
    element_checked,
    element_checked_down,
};

enum UiMsg
{
    ui_msg_none = 0,
    ui_show,
    ui_hide,

    ui_page_expand,
    ui_page_collapse,
};

}

#endif // RUI_TYPE

