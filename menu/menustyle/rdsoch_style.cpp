
#include "rcontrolstyle.h"

#include "rdsoch_style.h"

#include "../../meta/crmeta.h"

namespace menu_res {

RDsoCH_Style::RDsoCH_Style()
{
}

void RDsoCH_Style::load( const QString &root,
                         const r_meta::CMeta &rMeta )
{

    //! load styles
    frameActive.loadResource( root + "frame/active/", rMeta );
    frameDown.loadResource( root + "frame/down/", rMeta );
    frameEnable.loadResource( root + "frame/enable/", rMeta );
    frameDisable.loadResource( root + "frame/disable/", rMeta );

    rMeta.getMetaVal( root + "left", mX );

    //! common config
    QString str=root;

    RControlStyle::routeToSibling( str, "/ch_common/" );

    //! coord
    rMeta.getMetaVal( str + "coupling", rectCoup );

    rMeta.getMetaVal( str + "scale", rectScale );

    rMeta.getMetaVal( str + "bw", rectB );

    rMeta.getMetaVal( str + "imp", impXY );

    rMeta.getMetaVal( str + "offset", rectOffset );

    rMeta.getMetaVal( str + "header", mRectHeader );
    rMeta.getMetaVal( str + "content", mRectContent );
    rMeta.getMetaVal( str + "invert", mRectInvert );
    rMeta.getMetaVal( str + "vernier", mRectVernier );

    rMeta.getMetaVal( str + "size", mSize );
    rMeta.getMetaVal( str + "top", mY );

    rMeta.getMetaVal( str + "font/family", mFont );
    rMeta.getMetaVal( str + "font/size", mPtSize );

    rMeta.getMetaVal( str + "ac1",  ac1 );
    rMeta.getMetaVal( str + "ac2",  ac2 );
    rMeta.getMetaVal( str + "ac3",  ac3 );
    rMeta.getMetaVal( str + "ac4",  ac4 );
    rMeta.getMetaVal( str + "acdis",acDis );
    rMeta.getMetaVal( str + "dc1",  dc1 );
    rMeta.getMetaVal( str + "dc2",  dc2 );
    rMeta.getMetaVal( str + "dc3",  dc3 );
    rMeta.getMetaVal( str + "dc4",  dc4 );
    rMeta.getMetaVal( str + "dcdis",dcDis );
}

void RDsoCH_Style::paintEvent( QPainter &painter, UicStatus stat )
{
    if ( stat == uic_actived )
    {
        frameActive.paint( painter );
    }
    else if ( stat == uic_enabled )
    {
        frameEnable.paint( painter );
    }
    else if ( stat == uic_downed )
    {
        frameDown.paint( painter );
    }
    else //if ( stat == ch_disabled )
    {
        frameDisable.paint( painter );
    }
}

void RDsoCH_Style::resize( QWidget *pWidget )
{
    Q_ASSERT( NULL != pWidget );

    pWidget->setGeometry( mX, mY, mSize.width(), mSize.height() );
}

QColor RDsoCH_Style::getStatusColor( UicStatus stat )
{
    if ( stat == uic_enabled )
    {
        return frameEnable.mColor;
    }
    else if ( stat == uic_disabled )
    {
        return frameDisable.mColor;
    }
    else
    {
        return frameActive.mColor;
    }
}
QColor RDsoCH_Style::getHeadColor( UicStatus stat )
{
    if ( stat == uic_enabled )
    { return frameActive.mColor; }
    else
    { return Qt::black; }
}

}
