#include "../draw/rpainter.h"
#include "rmessagebox.h"

namespace menu_res {

#define bm_button( btn, btn_mask ) ( (btn & (btn_mask) ) == (btn_mask) )

RMessageBox_Style RMessageBox::_style;
RMessageBox* RMessageBox::_msgBox = NULL;

void RMessageBox::Show( const QString &info,
                  QObject *obj,
                  const char *objSlot,
                  MsgBox_Icon icon,
                  MsgBox_Button /*btn*/ )
{
    Q_ASSERT( NULL != obj );
    Q_ASSERT( NULL != objSlot );

    RMessageBox *pBox;

    //! has created
    if ( RMessageBox::_msgBox != NULL )
    {
        pBox = RMessageBox::_msgBox;
    }
    else
    {
        pBox = new RMessageBox( sysGetMainWindow() );
        Q_ASSERT( NULL != pBox );

        RMessageBox::_msgBox = pBox;
    }

    //! config box
    if ( icon == icon_question )
    { pBox->mpIcon->setPixmap(QPixmap(_style.mIconQues));}
    else if ( icon == icon_info)
    { pBox->mpIcon->setPixmap(QPixmap(_style.mIconInfo));}
    else if ( icon == icon_warnning )
    { pBox->mpIcon->setPixmap(QPixmap(_style.mIconWarn));}
    else
    {}

    //! info
    pBox->mpEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    pBox->mpEdit->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    pBox->mpEdit->setText( info );

    pBox->show();
}

bool RMessageBox::isUsed()
{
    if( RMessageBox::_msgBox == NULL )
    {
        return false;
    }
    return RMessageBox::_msgBox->isVisible();
}

void RMessageBox::Hide()
{
    Q_ASSERT( RMessageBox::_msgBox != NULL );
    RMessageBox::_msgBox->hide();
}

RMessageBox::RMessageBox( QWidget *parent ) : uiWnd(parent)
{
    setWindowFlags( Qt::FramelessWindowHint );
    setAttribute( Qt::WA_DeleteOnClose  );

    _style.loadResource("ui/messagebox/");

    mpIcon = new QLabel(this);
    Q_ASSERT( NULL != mpIcon );
    mpEdit = new QTextEdit(this);
    Q_ASSERT( NULL != mpEdit );

    //! setup ui
    setGeometry( _style.mRect );

    mpIcon->setGeometry( _style.mIconRect );
    mpEdit->setGeometry( _style.mPromptRect );

    mpIcon->setStyleSheet( _style.mIconStyle );
    mpEdit->setStyleSheet( _style.mPromptStyle );

    mpEdit->setReadOnly( true );

    mpEdit->setFont( QFont( qApp->font().family(),
                   _style.mFontPrompt.mSize ) );
}

void RMessageBox::paintEvent( QPaintEvent *event )
{
    QPainter painter(this);

    _style.drawFrame( painter, rect(), _style.mFrame );

    uiWnd::paintEvent( event );
}

}

