#define _DEBUG
#include <QEvent>
#include <QKeyEvent>
#include <QApplication>
#include <QDebug>

#include "../../menu/event/reventfilter.h"
#include "../../menu/draw/rpainter.h"
#include "uiwnd.h"


#include "../../app/measure/measItemBox.h"
#include "../../app/measure/measValueBox.h"



namespace menu_res {

QList<uiWnd*> uiWnd::cfgDialogs;

uiWndMgr::uiWndMgr()
{
    m_pActiveWnd = NULL;
}

void uiWndMgr::activeNext()
{
    //! no window
    if ( mWindows.size() < 1 )
    { return; }

    int iFrom,iLen;
    //! rst window
    if ( m_pActiveWnd == NULL )
    {
        iFrom = 0;
        iLen = mWindows.size();
    }
    else
    {
        //! find the window
        Q_ASSERT( mWindows.contains(m_pActiveWnd) );

        int index = mWindows.indexOf( m_pActiveWnd );
        Q_ASSERT( index >= 0 );

        //! is top now
        if ( m_pActiveWnd->isTop() )
        {
            iFrom = index + 1;
        }
        else
        { iFrom = index; }

        if ( iFrom >= mWindows.size() ) iFrom = 0;
        iLen = mWindows.size() - 1;
    }

    int cnt =0;
    for ( int i = iFrom; cnt < iLen; cnt++, i++ )
    {
        //! roll back
        if ( i >= mWindows.size() ) i = 0;

        //! find success
        Q_ASSERT( mWindows[i] != NULL );
        if ( mWindows[i]->isVisible() )
        {
            //! unload previous
            if ( m_pActiveWnd != NULL )
            {
                m_pActiveWnd->unload();
            }

            m_pActiveWnd = mWindows[i];
            m_pActiveWnd->setActive();
            break;
        }
    }

    //! do not change
}

void uiWndMgr::setActive( uiWnd *pWnd )
{
    Q_ASSERT( NULL != pWnd );
    m_pActiveWnd = pWnd;

    m_pActiveWnd->setActive();
}

void uiWndMgr::unloadAll()
{
    foreach( menu_res::uiWnd *pWnd, mWindows )
    {
        Q_ASSERT( NULL != pWnd );
        pWnd->unload();
    }

    foreach( menu_res::uiWnd *pWnd, mWindows )
    {
        Q_ASSERT( NULL != pWnd );
        if ( pWnd->isPopInfo() )
        {
            pWnd->hide();
        }
    }

    //! no active wnd
    m_pActiveWnd = NULL;
}

void uiWndMgr::append( uiWnd * pWnd )
{
    Q_ASSERT( NULL != pWnd );

    mWindows.append( pWnd );
}
void uiWndMgr::removeAll( uiWnd *pWnd )
{
    Q_ASSERT( NULL != pWnd );

    mWindows.removeAll( pWnd );
}

uiWnd::uiWnd(QWidget *parent, uiWndMgr *pWndMgr ) : QWidget(parent)
{
    m_pWndMgr = pWndMgr;

    //! add to mgr
    if ( NULL != m_pWndMgr )
    { m_pWndMgr->append( this ); }

    mWndAttr = wnd_unk;

    setObjectName( QString("wnd:%1").arg( (int)rand() ) );

    mTmo = 0;

    mbSetupUi = false;

    isFirst = false;

    //! default to hide
    hide();
}

uiWnd::~uiWnd()
{
    unload();

    if ( NULL != m_pWndMgr )
    {
        m_pWndMgr->removeAll( this );
    }
}

void uiWnd::loadResource( const QString &/*path*/,
                          const r_meta::CMeta &/*meta*/ )
{}
void uiWnd::setupUi( const QString &/*path*/,
                      const r_meta::CMeta &/*meta*/ )
{}

bool uiWnd::setup( const QString &path,
            const r_meta::CMeta &meta )
{
    //! no created
    if ( !mbSetupUi )
    {
        setupUi( path, meta );
        mbSetupUi = true;

        return true;
    }
    else
    { return false; }
}
bool uiWnd::isSetuped()
{
    return mbSetupUi;
}

void uiWnd::setObjectName(const QString &name)
{
    QWidget::setObjectName( name );

    setName( name );
}

void uiWnd::setAutoHide( int tmo )
{
//    mTimer.setSingleShot( true );
    mTmo = tmo;

    //qDebug() << "tmo:" << tmo << QTime::currentTime();
    mTimer.disconnect();
    connect( &mTimer,
             SIGNAL(timeout()),
             this,
             SLOT(onTimeout()) );

    //! wnd attr
    set_attr( wndAttr, mWndAttr, wnd_tmo_hide );
    set_attr( wndAttr, mWndAttr, wnd_disconnect_on_hide );
}

void uiWnd::arrangeOn( QWidget *pWig,
                       placePriority pri,
                       int dist )
{
    Q_ASSERT( NULL != pWig );

    if ( !isVisible() ) return;

    //! calc the pos
    QPoint pt;
    pt = pWig->mapToGlobal( QPoint(0,0) );

    QPoint dstPt;
    //! in global coord.
    dstPt = sloveDstPt( pt,
                        pWig->size(),
                        size(),
                        sysGetMainWindow()->geometry(),
                        pri,
                        dist );

    //! to parent coord.
    if ( parentWidget()!=NULL )
    { dstPt = parentWidget()->mapFromGlobal( dstPt ); }

    move( dstPt );
    mArrangePt = dstPt;
}

void uiWnd::rstArrange()
{
    //! created and arranged
    if ( mbSetupUi && !mArrangePt.isNull() )
    {
        move(mArrangePt);
    }
    else
    { }
}

void uiWnd::redraw()
{
    hide();
    show();
}

bool uiWnd::eventFilter(QObject */*obj*/, QEvent *evt)
{
    Q_ASSERT( evt != NULL );

    QKeyEvent   *pKeyEvent;
    QEvent::Type evtType;
    evtType = evt->type();

    if ( evtType == QEvent::KeyPress )
    {
        pKeyEvent = dynamic_cast<QKeyEvent*>( evt );

        Q_ASSERT( NULL != pKeyEvent );

        //! key eater
        if ( keyEater( pKeyEvent->key(), pKeyEvent->count(), true) )
        {
            return true;
        }

        //! configed special key
        if ( !mKeyList.contains(pKeyEvent->key()) )
        {
            return false;
        }

        keyPressEvent( pKeyEvent );

        return true;
    }
    else if ( evtType == QEvent::KeyRelease )
    {
        pKeyEvent = dynamic_cast<QKeyEvent*>( evt );
        Q_ASSERT( NULL != pKeyEvent );

        //! key eater
        if ( keyEater( pKeyEvent->key(), pKeyEvent->count(), false) )
        {
            return true;
        }

        if ( !mKeyList.contains(pKeyEvent->key()) )
        {
            //! pop up
            if ( is_attr(mWndAttr, wnd_popup) )
            {
                hide();
            }

            return false;
        }

        keyReleaseEvent( pKeyEvent );

        return true;
    }
    else if ( evtType == QEvent::MouseButtonPress
             || evtType == QEvent::MouseButtonRelease
             || evtType == QEvent::MouseButtonDblClick
             )
    {
        //! not popup
        if ( !is_attr(mWndAttr, wnd_popup) )
        {
            return false;
        }

        //! check the region
        QMouseEvent *mEvt = static_cast<QMouseEvent *>(evt);
        Q_ASSERT( NULL != mEvt );

        QPoint posG = mEvt->globalPos();

        //! in region
        if ( containsPt(posG) )
        {
            return false;
        }
        else
        {
            //BY HXH 2018-6-1
            if( evtType != QEvent::MouseButtonRelease )
            {
                qDebug() << __FUNCTION__ << __LINE__ << this;
                hide();
            }
            return true;
        }
    }
    else if ( evtType == QEvent::TouchBegin
             || evtType == QEvent::TouchUpdate
             || evtType == QEvent::TouchEnd
             )
    {
        QTouchEvent *tEvt = static_cast<QTouchEvent*>(evt);
        Q_ASSERT( NULL != tEvt );

        //! not popup
        if ( !is_attr(mWndAttr, wnd_popup) )
        {
            return false;
        }

        QList<QTouchEvent::TouchPoint> tPoints = tEvt->touchPoints();
        if ( tPoints.size() < 1 )
        {
            return false;
        }

        QPoint ptScreen;
        QPointF scrPos = tPoints[0].normalizedPos();
        ptScreen.setX( scrPos.x() * screen_width );
        ptScreen.setY( scrPos.y() * screen_height );
        if ( containsPt( ptScreen) )
        {
            return false;
        }
        else
        {
            //bug796
            if( evtType == QEvent::TouchBegin )
            {
                hide();
            }
            else
            {

            }
            return false;
        }
    }
    else
    {
        return false;
    }
}

void uiWnd::keyPressEvent(QKeyEvent */*evt*/)
{ return ; }
void uiWnd::keyReleaseEvent(QKeyEvent *evt)
{
    Q_ASSERT( evt != NULL );

    int key, cnt;

    key = evt->key();
    cnt = evt->count();
    if ( key == R_Pkey_FInc )
    { tuneInc(cnt); }
    else if ( key == R_Pkey_FDec )
    { tuneDec(cnt); }
    else if ( key == R_Pkey_FZ )
    { tuneZ(cnt); }
    else if ( key == R_Pkey_PageOff )
    { menuOff(); }
    else if ( key == R_Pkey_PageReturn )
    { menuBack(); }
    //! other key
    else
    { keyEvent( key, cnt ); }
}

void uiWnd::mousePressEvent(QMouseEvent *event)
{
    tryActiveIn();

    this->raise();

    if ( !is_attr(mWndAttr, wnd_moveable ))
    {
        QWidget::mousePressEvent( event );
        event->ignore();
        return;
    }
}

void uiWnd::mouseMoveEvent(QMouseEvent *event)
{
    if ( !is_attr(mWndAttr, wnd_moveable ))
    {
        event->ignore();
        return;
    }

    if (event->buttons() == Qt::LeftButton)
    {
        if(!isFirst){
            isFirst = true;
            mptMoveOrig.setX( event->globalPos().rx() - pos().rx() );
            mptMoveOrig.setY( event->globalPos().ry() - pos().ry() );
        }else{
            QPoint pt = event->globalPos();
            QPoint next = pt - mptMoveOrig;

            if ( next.x() < 0 )
            { next.setX(0);}

            if ( next.y() < 0 )
            { next.setY(0); }

            QWidget *pParent = (QWidget*)parent();
            if ( NULL != pParent )
            {
                QRect parRect = pParent->rect();
                QRect myRect = rect();

                if ( next.x() > parRect.width() - myRect.width() )
                { next.setX(parRect.width() - myRect.width());}

                if ( next.y() > parRect.height() - myRect.height() )
                { next.setY(parRect.height() - myRect.height()); }
            }

            this->move( next );
            emit sig_move(next);
        }
    }
}

void uiWnd::mouseReleaseEvent(QMouseEvent */*event*/)
{
    isFirst = false;
}

/*!
 * \brief uiWnd::focusInEvent
 * 通过点击获得焦点时没有被调用
 * \todo
 */
void uiWnd::focusInEvent(QFocusEvent *)
{
    //! 键盘响应焦点
    setActive();

    LOG_DBG();
}

void uiWnd::enterEvent(QEnterEvent *)
{
    LOG_DBG();
}

void uiWnd::showEvent(QShowEvent *)
{
    raise();
    setActive();

    //! start again
    if ( is_attr(mWndAttr, wnd_tmo_hide) && mTmo>0)
    {
        mTimer.start( mTmo );
        //qDebug() << "show:" << QTime::currentTime();
    }
}
void uiWnd::hideEvent(QHideEvent *)
{
    unload();

    if ( is_attr( mWndAttr, wnd_disconnect_on_hide) )
    {
        disconnect();
        //qDebug() << "hide:" << QTime::currentTime();
    }
}

void uiWnd::paintEvent( QPaintEvent *event )
{
    Q_ASSERT( NULL != event );

    RPainter painter(this);

    doPaint( event, painter );

}
void uiWnd::doPaint( QPaintEvent */*event*/,
                      QPainter &/*painter*/ )
{}

void uiWnd::tuneInc( int /*keyCnt*/ )
{}
void uiWnd::tuneDec( int /*keyCnt*/ )
{}
void uiWnd::tuneZ( int /*keyCnt*/ )
{}
void uiWnd::menuOff()
{ hide(); }
void uiWnd::menuBack()
{ hide(); }
void uiWnd::keyEvent( int /*key*/, int /*keyCnt*/ )
{}

bool uiWnd::isServiceActive()
{
    return true;
}
void uiWnd::serviceActiveIn()
{
}

bool uiWnd::keyEater( int key, int count, bool bRelease )
{
    //! not soft key
    if ( is_skey(key) ) return false;

    return serviceExecutor::keyEater( key, count, bRelease );
}

bool uiWnd::containsPt( const QPoint &globalPt )
{
    QRect selfRect = geometry();

    QPoint pt(0,0);
    QPoint ptG;
    ptG = mapToGlobal( pt );
    selfRect.setTopLeft( ptG );

    return selfRect.contains( globalPt, true );
}

bool uiWnd::tryActiveIn()
{
    if ( NULL == m_pWndMgr )
    { return true; }

    //! is service active?
    if ( isServiceActive() )
    {
        m_pWndMgr->setActive( this );
        return false;
    }
    else
    {
        m_pWndMgr->setActive( this );
        serviceActiveIn();

        return true;
    }
}

void uiWnd::keyChanged()
{
    //! has key
    if ( mKeyList.size() > 0 )
    {
        set_attr( uiWnd::wndAttr, mWndAttr, uiWnd::wnd_grab );
    }
    //! no key
    else
    {
        unset_attr( uiWnd::wndAttr, mWndAttr, uiWnd::wnd_grab );
        unload();
    }
}

void uiWnd::getGeo( const r_meta::CMeta &meta,
             const QString &path,
             QWidget *pWidget )
{
    Q_ASSERT( NULL != pWidget );

    QRect geoRect;

    meta.getMetaVal( path, geoRect );

    pWidget->setGeometry( geoRect );
}

void uiWnd::getGeo( const r_meta::CMeta &meta,
             const QString &path,
             QWidget &widget )
{
    getGeo( meta, path, &widget );
}

void uiWnd::getStyle( const r_meta::CMeta &meta,
             const QString &path,
             QWidget *pWidget )
{
    Q_ASSERT( NULL != pWidget );

    QString str;

    meta.getMetaVal( path, str );

    pWidget->setStyleSheet( str );
}

void uiWnd::getStyle( const r_meta::CMeta &meta,
             const QString &path,
             QWidget &widget )
{
    getStyle( meta, path, &widget );
}


bool uiWnd::canGrab()
{
    return  ( is_attr(mWndAttr,wnd_grab ) );
}

bool uiWnd::isPopInfo()
{
    return  ( is_attr(mWndAttr,wnd_active_pop_info ) );
}

/*!
 * \brief uiWnd::setActive
 * 设置为最顶层的窗口,当前窗口最先得到键盘消息
 */
void uiWnd::setActive()
{
    load();
}
/*!
 * \brief uiWnd::setDeactive
 * 空，还没有定义动作
 */
void uiWnd::setDeactive()
{
}

/*!
 * \brief uiWnd::load
 * 装载窗口
 * -将窗口添加到键盘响应路径中
 * -添加的窗口处于最顶层，最先得到消息
 */
void uiWnd::load()
{
    if ( canGrab() )
    {}
    else
    { return; }

    //! add key widget
    foreach( int key, mKeyList )
    {
        attachKey( key, this );
    }

    install();
}
/*!
 * \brief uiWnd::unload
 * -将窗口从事件响应路径中移除
 * -关闭窗口时需要从事件路径中移除
 */
void uiWnd::unload()
{
    mKeyMaps.clear();

    uninstall();
}

void uiWnd::setAttr( uiWnd::wndAttr attr )
{
    mWndAttr = attr;
}

void uiWnd::onTimeout()
{
    if ( isVisible() )
    {
        hide();
    }
    mTimer.stop();
}

void uiWnd::addAllKeys()
{
    mKeyList.clear();
    mKeyList.append( R_Pkey_F1 );
    mKeyList.append( R_Pkey_F2 );
    mKeyList.append( R_Pkey_F3 );
    mKeyList.append( R_Pkey_F4 );
    mKeyList.append( R_Pkey_F5 );
    mKeyList.append( R_Pkey_F6 );
    mKeyList.append( R_Pkey_F7 );

    mKeyList.append( R_Pkey_PageReturn );
    mKeyList.append( R_Pkey_PageOff    );
    mKeyList.append( R_Pkey_PageUp     );
    mKeyList.append( R_Pkey_PageDown   );

    mKeyList.append( R_Pkey_FInc       );
    mKeyList.append( R_Pkey_FDec       );
    mKeyList.append( R_Pkey_FZ         );

    //!ch
    mKeyList.append( R_Pkey_CH1  );
    mKeyList.append( R_Pkey_CH2  );
    mKeyList.append( R_Pkey_CH3  );
    mKeyList.append( R_Pkey_CH4  );

    mKeyList.append( encode_dec(R_Pkey_CH1_VOLT) );
    mKeyList.append( encode_inc(R_Pkey_CH1_VOLT) );
    mKeyList.append( R_Pkey_CH1_VOLT_Z );

    mKeyList.append( encode_dec(R_Pkey_CH1_POS)    );
    mKeyList.append( encode_inc(R_Pkey_CH1_POS)    );
    mKeyList.append( R_Pkey_CH1_POS_Z  );

    mKeyList.append( encode_dec(R_Pkey_CH2_VOLT)   );
    mKeyList.append( encode_inc(R_Pkey_CH2_VOLT)   );
    mKeyList.append( R_Pkey_CH2_VOLT_Z );

    mKeyList.append( encode_dec(R_Pkey_CH2_POS)    );
    mKeyList.append( encode_inc(R_Pkey_CH2_POS)    );
    mKeyList.append( R_Pkey_CH2_POS_Z  );

    mKeyList.append( encode_dec(R_Pkey_CH3_VOLT)   );
    mKeyList.append( encode_inc(R_Pkey_CH3_VOLT)   );
    mKeyList.append( R_Pkey_CH3_VOLT_Z );

    mKeyList.append( encode_dec(R_Pkey_CH3_POS)    );
    mKeyList.append( encode_inc(R_Pkey_CH3_POS)    );
    mKeyList.append( R_Pkey_CH3_POS_Z  );

    mKeyList.append( encode_dec(R_Pkey_CH4_VOLT)   );
    mKeyList.append( encode_inc(R_Pkey_CH4_VOLT)   );
    mKeyList.append( R_Pkey_CH4_VOLT_Z );

    mKeyList.append( encode_dec(R_Pkey_CH4_POS)    );
    mKeyList.append( encode_inc(R_Pkey_CH4_POS)    );
    mKeyList.append( R_Pkey_CH4_POS_Z  );

    //!app
    mKeyList.append( R_Pkey_LA       );
    mKeyList.append( R_Pkey_DECODE   );
    mKeyList.append( R_Pkey_MATH     );
    mKeyList.append( R_Pkey_REF      );
    mKeyList.append( R_Pkey_SOURCE1  );
    mKeyList.append( R_Pkey_SOURCE2  );
    mKeyList.append( R_Pkey_MEASURE  );
    mKeyList.append( R_Pkey_ACQUIRE  );
    mKeyList.append( R_Pkey_STORAGE  );
    mKeyList.append( R_Pkey_CURSOR   );
    mKeyList.append( R_Pkey_DISPLAY  );
    mKeyList.append( R_Pkey_UTILITY  );

    //!quick
    mKeyList.append( R_Pkey_CLEAR     );
    mKeyList.append( R_Pkey_AUTO      );
    mKeyList.append( R_Pkey_RunStop   );
    mKeyList.append( R_Pkey_SINGLE    );

    mKeyList.append( R_Pkey_QUICK     );
    mKeyList.append( R_Pkey_DEFAULT   );
    mKeyList.append( R_Pkey_TOUCH     );

    mKeyList.append( R_Pkey_PLAY_PRE  );
    mKeyList.append( R_Pkey_PLAY_NEXT );
    mKeyList.append( R_Pkey_PLAY_STOP );

    //! wave
    mKeyList.append( encode_inc(R_Pkey_WAVE_VOLT)     );
    mKeyList.append( encode_dec(R_Pkey_WAVE_VOLT)     );
    mKeyList.append( R_Pkey_WAVE_VOLT_Z   );

    mKeyList.append( encode_inc(R_Pkey_WAVE_POS)      );
    mKeyList.append( encode_dec(R_Pkey_WAVE_POS)      );
    mKeyList.append( R_Pkey_WAVE_POS_Z    );

    //! trig
    mKeyList.append( R_Pkey_TRIG_FORCE );
    mKeyList.append( R_Pkey_TRIG_MENU     );
    mKeyList.append( R_Pkey_TRIG_MODE     );

    mKeyList.append( encode_inc(R_Pkey_TRIG_LEVEL)    );
    mKeyList.append( encode_dec(R_Pkey_TRIG_LEVEL)    );
    mKeyList.append( R_Pkey_TRIG_LEVEL_Z  );
    //! hori
    mKeyList.append( R_Pkey_HORI_NAGAVITE );
    mKeyList.append( R_Pkey_HORI_ZOOM     );

    mKeyList.append( encode_inc(R_Pkey_TIME_SCALE)    );
    mKeyList.append( encode_dec(R_Pkey_TIME_SCALE)    );
    mKeyList.append( R_Pkey_TIME_SCALE_Z  );

    mKeyList.append( encode_inc(R_Pkey_TIME_OFFSET)   );
    mKeyList.append( encode_dec(R_Pkey_TIME_OFFSET)   );
    mKeyList.append( R_Pkey_TIME_OFFSET_Z );

    keyChanged();
}

void uiWnd::addTuneKey()
{
    mKeyList.append( R_Pkey_FInc       );
    mKeyList.append( R_Pkey_FDec       );
    mKeyList.append( R_Pkey_FZ         );

    keyChanged();
}
void uiWnd::addTuneIncDec()
{
    mKeyList.append( R_Pkey_FInc       );
    mKeyList.append( R_Pkey_FDec       );

    keyChanged();
}
void uiWnd::addMenuOff()
{
    mKeyList.append( R_Pkey_PageOff    );

    keyChanged();
}
void uiWnd::addMenuBack()
{
    mKeyList.append( R_Pkey_PageReturn );

    keyChanged();
}

void uiWnd::addPageUpDown()
{
    mKeyList.append( R_Pkey_PageUp );
    mKeyList.append( R_Pkey_PageDown );

    keyChanged();
}

void uiWnd::addToShowQueue(uiWnd *d)
{
    cfgDialogs.append( d );
}
void uiWnd::showMeOnly(uiWnd *d)
{
    Q_ASSERT( NULL != d );

    //! hide other
    foreach( uiWnd *pItem, cfgDialogs )
    {
        Q_ASSERT( pItem != NULL );

        if ( d != pItem && pItem->isVisible() )
        {
            pItem->setVisible( false );
        }
    }
    d->setVisible( true );
}
void uiWnd::setShow(bool bShow)
{
    if( bShow )
    {
        showMeOnly( this );
    }
    else
    {
        setVisible( bShow );
    }
}
}
