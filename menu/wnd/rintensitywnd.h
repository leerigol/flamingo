#ifndef RINTENSITYWND_H
#define RINTENSITYWND_H

#include "rinfownd.h"
#include "../menustyle/rintensitywnd_style.h"
namespace menu_res {

class RIntensityWnd : public RPopWnd
{
    Q_OBJECT
public:
    RIntensityWnd( QWidget *parent = 0 );

public:
    void setIntensity( int intens );

public slots:
    void sliderValueChanged();

protected:
    RIntensityWnd_Style mStyle;

protected:
    QSlider *m_pSlider;
    QLabel  *m_pLabel;
};

}

#endif // RINTENSITYWND_H
