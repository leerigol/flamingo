#include "rprogressdialog.h"
#include "../draw/rpainter.h"
namespace menu_res {

RProgressDialog_Style RProgressDialog::_style;

RProgressDialog::RProgressDialog( QWidget *parent )
                : uiWnd( parent )
{
    setWindowFlags( Qt::FramelessWindowHint );

    _style.loadResource( "ui/progress_dialog/" );

    m_pBar = new RProgressBar( this );
    Q_ASSERT( NULL != m_pBar );
    m_pAnimation = new RAnimation( this );
    Q_ASSERT( NULL != m_pAnimation );
    m_pLabel = new QLabel( this );
    Q_ASSERT( NULL != m_pLabel );
    m_pTitleLabel = new QLabel( this );
    Q_ASSERT( NULL != m_pTitleLabel );

    m_pCloseButton = new RImageButton(this);
    m_pCloseButton->hide();
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(_style.closeRect);
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(close()) );

    //! resize the child
    m_pBar->setGeometry( _style.rectBar );
    m_pAnimation->setGeometry( _style.rectAnimation );
    m_pLabel->setGeometry( _style.rectText );
    m_pTitleLabel->setGeometry( _style.titleRect );

    m_pLabel->setWordWrap(true);
    m_pLabel->setAlignment(Qt::AlignTop);

    //! geometry
    setGeometry( _style.rectWnd );

    //! style sheet
    m_pLabel->setStyleSheet( _style.mStyleText );

    m_pTitleLabel->setStyleSheet(_style.mStyleText);

    //! set the animate pic
    int i;
    QString strAnimate;
    for ( i = 0; i < _style.mAnimateCnt; i++ )
    {
        strAnimate = QString("%1%2.bmp").arg( _style.mAnimatePattern).arg(i);
        m_pAnimation->addAnimation( strAnimate );
    }
    m_pAnimation->start();
}

void RProgressDialog::doPaint( QPaintEvent *event,
                                  QPainter &painter )
{
    _style.paintEvent( painter, rect() );

    QWidget::paintEvent( event );
}

void RProgressDialog::setRange( int min, int max )
{
    m_pBar->setRange( min, max );
}
void RProgressDialog::setValue( int val )
{
    m_pBar->setValue( val );
}

void RProgressDialog::setInfo( const QString &text )
{
    m_pLabel->setText( text );
}

void RProgressDialog::setTitle( const QString &text )
{
    m_pTitleLabel->setText( text );
}

}

