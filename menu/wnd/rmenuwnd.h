#ifndef RMENUWND_H
#define RMENUWND_H

#include <QWidget>

namespace menu_res {

class RMenuWnd : public QWidget
{
    Q_OBJECT
public:
    explicit RMenuWnd(QWidget *parent = 0);

public:
    bool isExpand();
    void setExpand( bool bExpand );

    void setPosition( QPoint &ptExpand,
                      QPoint &ptCollapse );
signals:

public slots:

protected:
    bool mbExpand;
    QPoint mPtExpand, mPtCollapse;
};

}

#endif // RMENUWND_H
