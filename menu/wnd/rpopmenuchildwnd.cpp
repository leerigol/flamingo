#include "../../menu/event/reventfilter.h"

#include "rpopmenuchildwnd.h"
#include "../../service/servdso/sysdso.h"

namespace  menu_res {

//! child wnd called by menu
RPopMenuChildWnd::RPopMenuChildWnd( QWidget *parent )
                 : uiWnd(parent)
{
    this->setParent(sysGetMainWindow());

    mLinkKey = -1;

    set_attr( wndAttr, mWndAttr, wnd_popup );
}

RPopMenuChildWnd::~RPopMenuChildWnd()
{}

void RPopMenuChildWnd::show()
{
    this->installEventFilter(this);
    setVisible(true);
}

void RPopMenuChildWnd::hide()
{
    setVisible(false);
    this->removeEventFilter(this);
}

bool RPopMenuChildWnd::eventFilter(QObject *object, QEvent *evt)
{
    Q_ASSERT( evt != NULL );
    if(object->parent() != this && object != this && object != this->parent())
    {
        if (evt->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent *e = static_cast<QMouseEvent *>(evt);

            QRect rect;
            rect.setX(geometry().x() + sysGetMainWindow()->geometry().x());
            rect.setY(geometry().y() + sysGetMainWindow()->geometry().y());
            rect.setWidth(geometry().width());
            rect.setHeight(geometry().height());

            //! filter the close click
            if( !rect.contains(e->pos()) )
            {
                this->hide();
                return true;
            }
        }
    }

    QKeyEvent *pKeyEvent;

    //! filter all press
    if ( evt->type() == QEvent::KeyPress )
    {
        return uiWnd::eventFilter( object, evt );
    }
    else if ( evt->type() == QEvent::KeyRelease )
    {
        pKeyEvent = dynamic_cast<QKeyEvent*>( evt );

        Q_ASSERT( NULL != pKeyEvent );
        //! 关闭显示
        if ( !mKeyList.contains(pKeyEvent->key()) )
        {
            hide();

            //! 如果是关联的按键
            if ( pKeyEvent->key() == mLinkKey )
            { return true; }
            if ( pKeyEvent->key() == R_Pkey_PageOff )
            { return true; }
            else
            { return false; }
        }

        //! 自定义键处理过程
        keyReleaseEvent( pKeyEvent );
        return true;
    }
    else
    {
        return uiWnd::eventFilter( object, evt );
    }
}

void RPopMenuChildWnd::setLinkKey( int key )
{
    mLinkKey = key;
}

}

