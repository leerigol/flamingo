#ifndef RMODALWND_H
#define RMODALWND_H

#include <QtWidgets>

#include "../menustyle/rinfownd_style.h"

#include "../../widget/rimagebutton.h"

#include "./uiwnd.h"

namespace menu_res {

/*!
 * \brief The RModalWnd class
 * 模式对话框
 */
class RModalWnd : public uiWnd
{
    Q_OBJECT

    static RModelWnd_Style _style;

public:
    explicit RModalWnd( QWidget *parent = 0 );
public:
    void setCloseEnable( bool bEn );
    void setCloseVisible( bool b );
    void setTitle( const QString &str );
    void setTitle( int titleId );
    void setTitleStyle( const QString &style );
    void setTitleColor( const QColor &color );

protected:
    virtual void showEvent(QShowEvent *event );
    virtual void resizeEvent(QResizeEvent * /*event*/);
    virtual void doPaint( QPaintEvent *,
                             QPainter &painter );

Q_SIGNALS:
    void sigClose();

public Q_SLOTS:
    void show();
    void hide();
protected Q_SLOTS:
    void on_close_click();

private:
    RImageButton *m_pCloseButton;
    QLabel       *m_pLabel;
    int           mIdsTitle;
};

}

#endif // RMODALWND_H
