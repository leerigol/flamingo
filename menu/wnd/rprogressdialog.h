#ifndef RPROGRESSDIALOG_H
#define RPROGRESSDIALOG_H

#include <QtWidgets>

#include "../../widget/rprogressbar.h"
#include "../../widget/rbutton.h"
#include "../../widget/ranimation.h"

#include "../menustyle/rprogressdialog_style.h"
#include "../../widget/rimagebutton.h"
#include "uiwnd.h"

namespace menu_res {

class RProgressDialog : public uiWnd
{
    Q_OBJECT
private:
    static RProgressDialog_Style _style;

public:
    RProgressDialog( QWidget *parent = NULL );

protected:
    virtual void doPaint( QPaintEvent *event,
                          QPainter &painter );

public:
    void setRange( int min, int max );
    void setValue( int val );
    void setInfo( const QString &text );
    void setTitle( const QString &text );

protected:
    RProgressBar *m_pBar;
    RAnimation *m_pAnimation;
    RImageButton   *m_pCloseButton;
    QLabel *m_pLabel;
    QLabel *m_pTitleLabel;
};

}

#endif // RPROGRESSDIALOG_H
