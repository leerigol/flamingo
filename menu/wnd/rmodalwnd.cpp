#include "rmodalwnd.h"

#include "../../include/dsostd.h"
#include "../draw/rpainter.h"

namespace menu_res {

RModelWnd_Style RModalWnd::_style;

RModalWnd::RModalWnd( QWidget *parent ) : uiWnd(  parent != 0 ? parent : sysGetMainWindow() )
{
    setWindowFlags( Qt::FramelessWindowHint  );

    _style.loadResource( "ui/wnd/model/" );

    m_pLabel = new QLabel(this);
    Q_ASSERT( NULL != m_pLabel );
    m_pLabel->move(_style.mTitleXY);
    m_pLabel->setStyleSheet(_style.mTitleStyle);
    mIdsTitle = -1;

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(on_close_click()) );
}

void RModalWnd::setCloseEnable( bool bEn )
{
    Q_ASSERT( NULL != m_pCloseButton );
    m_pCloseButton->setEnabled( bEn );
}

void RModalWnd::setCloseVisible( bool b )
{
    Q_ASSERT( NULL != m_pCloseButton );
    m_pCloseButton->setVisible( b );
}

void RModalWnd::setTitle( const QString &str )
{
    Q_ASSERT( NULL != m_pLabel );
    m_pLabel->setText( str );
}

void RModalWnd::setTitle( int titleId )
{
    Q_ASSERT( NULL != m_pLabel );
    mIdsTitle = titleId;
}

void RModalWnd::setTitleStyle( const QString &style )
{
    Q_ASSERT( NULL != m_pLabel );
    m_pLabel->setStyleSheet( style );
}

void RModalWnd::setTitleColor( const QColor &color )
{
    Q_ASSERT( NULL != m_pLabel );

    QString str;
    str = QString("color:#%1").arg( color.rgb(),0,16 );
    m_pLabel->setStyleSheet( str );
}

void RModalWnd::showEvent(QShowEvent *event )
{
    uiWnd::showEvent( event );

    //! change title by now
    if ( mIdsTitle != -1 )
    {
        QString strTitle;
        strTitle = sysGetString( mIdsTitle );
        m_pLabel->setText( strTitle );
    }
}

void RModalWnd::resizeEvent(QResizeEvent * event )
{
    //! move the cancel button
    m_pCloseButton->resize(_style.mW, _style.mH);
    m_pCloseButton->move( rect().width() - _style.mW + _style.mCloseXY.x(),
                          _style.mCloseXY.y() );

    //! resize the title
    m_pLabel->resize( rect().width()- _style.mCloseXY.x() - _style.mW + _style.mCloseXY.x(),
                      _style.mH );

    //! manual resize
    if ( is_attr( mWndAttr, wnd_manual_resize ) )
    { return uiWnd::resizeEvent( event ); }

    QRect selfRect;
    QRect windowRect;

    selfRect = rect();
    windowRect = sysGetMainWindow()->geometry();

    move(  (windowRect.width() - selfRect.width())/2,
          (windowRect.height() - selfRect.height())/2
          ); 
}

void RModalWnd::doPaint( QPaintEvent * event,
                         QPainter &painter )
{
    Q_ASSERT( NULL != event );

    QRect selfRect = rect();
    QPen pen;

    pen.setColor( Qt::white );
    painter.setPen(pen);

    _style.paint( painter, selfRect );

}

void RModalWnd::show()
{
    QWidget::show();
}
void RModalWnd::hide()
{
    QWidget::hide();
}

void RModalWnd::on_close_click()
{
    hide();

    emit sigClose();
}

}

