#ifndef RINFOWND_H
#define RINFOWND_H

#include <QtWidgets>
#include "./uiwnd.h"
#include "../menustyle/rinfownd_style.h"

namespace menu_res {
/*!
 * \brief The RInfoWnd class
 * 无模式对话框
 */
class RInfoWnd : public uiWnd
{
    Q_OBJECT
private:
    static RInfoWnd_Style _style;

public:
    explicit RInfoWnd(QWidget *parent = 0);

    void show();
    void hide();

public:
    virtual void setVisible( bool b );
protected:
    virtual void resizeEvent(QResizeEvent * event);
    virtual void doPaint(QPaintEvent *event,
                         QPainter &painter
                         );

    void closeEvent(QCloseEvent *event);
    virtual bool eventFilter(QObject *, QEvent *);

signals:
    void sig_visible( bool b );

public slots:
};

/*!
 * \brief The RPopWnd class
 * 定时自动消失的弹出窗口
 */
class RPopWnd : public RInfoWnd
{
    Q_OBJECT
private:
    static RInfoWnd_Style _style;
public:
    explicit RPopWnd(QWidget *parent = 0);

protected:
    virtual void resizeEvent(QResizeEvent * event);
    virtual void doPaint(QPaintEvent *event,
                         QPainter &painter
                         );
};

}

#endif // RINFOWND_H
