#ifndef UIWND_H
#define UIWND_H

#include <QtWidgets>
#include "../../com/keyevent/rdsokey.h"
#include "../eventfiltermgr/reventfiltermgr.h"
#include "../../meta/crmeta.h"
#include "../../arith/xwnd.h"
namespace menu_res {

/*!
 * \brief The uiWnd class
 * 提供键盘处理能力
 * -窗口注册到事件过滤器上
 * -当窗口为活动时可以拦截所添加的键盘事件
 * -处理键盘消息在keyPressEvent/keyReleaseEvent中
 */
//! declare
class uiWndMgr;
class uiWnd;
typedef QList<uiWnd*> uiWndList;

class uiWnd : public QWidget, public IEventFilter
{
    Q_OBJECT
public:
    enum wndAttr
    {
        wnd_unk = 0,
        wnd_active_pop_info = 1,    //! show only at active, can not recover
        wnd_grab = 2,

        wnd_tmo_hide = 4,           //! timeout hide
        wnd_disconnect_on_hide = 8, //! disconnect on hide
        wnd_moveable = 16,
        wnd_manual_resize = 32,

        wnd_popup = 64,
    };

public:
    explicit uiWnd(QWidget *parent = 0, uiWndMgr *pWndMgr = NULL );
    ~uiWnd();

public:
    virtual void loadResource( const QString &path,
                               const r_meta::CMeta &meta );
protected:
    virtual void setupUi( const QString &path,
                          const r_meta::CMeta &meta );
public:
    //! true -- first setup
    bool setup( const QString &path,
                const r_meta::CMeta &meta );
    bool isSetuped();

    void setObjectName(const QString &name);
    void setAutoHide( int tmo = 2000 );

    void arrangeOn( QWidget *pWigCaller,
                    placePriority pri = priority_left,
                    int dist = 0 );
    void rstArrange();
    void redraw();

protected:
    virtual bool eventFilter(QObject *obj, QEvent *);

    virtual void keyPressEvent(QKeyEvent *);
    virtual void keyReleaseEvent(QKeyEvent *);

    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *);

    virtual void focusInEvent(QFocusEvent *);

    virtual void enterEvent( QEnterEvent * );

    virtual void showEvent(QShowEvent *);
    virtual void hideEvent(QHideEvent *);

    virtual void paintEvent( QPaintEvent *event );
    virtual void doPaint( QPaintEvent *event,
                          QPainter &painter );

protected:
    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );
    virtual void tuneZ( int keyCnt );
    virtual void menuOff();
    virtual void menuBack();
    virtual void keyEvent( int key, int keyCnt );

    virtual bool isServiceActive();
    virtual void serviceActiveIn();

protected:
    bool keyEater( int key , int count, bool bRelease );

    bool containsPt( const QPoint &globalPt );
    bool tryActiveIn();
    void keyChanged();

    void getGeo( const r_meta::CMeta &meta,
                 const QString &path,
                 QWidget *pWidget );

    void getGeo( const r_meta::CMeta &meta,
                 const QString &path,
                 QWidget &widget );

    void getStyle( const r_meta::CMeta &meta,
                 const QString &path,
                 QWidget *pWidget );

    void getStyle( const r_meta::CMeta &meta,
                 const QString &path,
                 QWidget &widget );

public:
    void addAllKeys();
    void addTuneKey();
    void addTuneIncDec();
    void addMenuOff();
    void addMenuBack();

    void addPageUpDown();

public://added by hxh for showing mutually
    static void addToShowQueue(uiWnd*);
    static void showMeOnly(uiWnd*);
    void   setShow( bool bShow );
    static QList<uiWnd*> cfgDialogs;

public:
    bool canGrab();
    bool isPopInfo();

public:
    void setActive();
    void setDeactive();

    void load();
    void unload();

    void setAttr( wndAttr attr );

 signals:
    void sig_move(QPoint);

public slots:
    void onTimeout();

protected:
    QList< int > mKeyList;

    QPoint mptMoveOrig;

    wndAttr mWndAttr;
    uiWndMgr *m_pWndMgr;

    QTimer mTimer;
    int mTmo;

    bool isFirst;

protected:
    bool mbSetupUi;
    QPoint mArrangePt;
};

class uiWndMgr
{
public:
    uiWndMgr();

    void activeNext();
    void setActive( uiWnd *pWnd );

    void unloadAll();

    void append( uiWnd * pWnd );
    void removeAll( uiWnd *pWnd );
public:
    uiWnd *m_pActiveWnd;
    uiWndList mWindows;
};

}

#endif // UIWND_H
