#include "rmenuwnd.h"
#include "../../include/dsodbg.h"
#include "../../service/service.h"
#include "../../service/service_msg.h"

namespace menu_res {

RMenuWnd::RMenuWnd(QWidget *parent) : QWidget(parent)
{
    mbExpand = true;
}

bool RMenuWnd::isExpand()
{
    return mbExpand;
}

void RMenuWnd::setExpand( bool bExpand )
{
    mbExpand = bExpand;

    if ( mbExpand )
    { show(); }
    else
    {
        hide();
        //! only hide
        serviceExecutor::post( E_SERVICE_ID_UI, CMD_SERVICE_PAGE_HIDE, 0 );
    }

    //! menu show or hide by bExpand
    serviceExecutor::post( E_SERVICE_ID_UI, CMD_SERVICE_PAGE_SHOW, bExpand );
}

void RMenuWnd::setPosition( QPoint &ptExpand,
                            QPoint &ptCollapse )
{
    mPtExpand = ptExpand;
    mPtCollapse = ptCollapse;
}

}
