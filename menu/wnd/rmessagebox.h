#ifndef RMESSAGEBOX_H
#define RMESSAGEBOX_H

#include <QtWidgets>
#include "uiwnd.h"
#include "../../widget/rbutton.h"
#include "../menustyle/rmessagebox_style.h"
namespace menu_res {

/*!
 * \brief The RMessageBox class
 * 确认对话框
 */
class RMessageBox : public uiWnd
{
    Q_OBJECT

private:
    static RMessageBox_Style _style;

public:
    enum MsgBox_Icon
    {
        icon_none = 0,
        icon_info,
        icon_warnning,
        icon_question,
    };

    enum MsgBox_Button
    {
        button_ok = 1,
        button_cancel = 2,
        button_okcancel = 3,
        button_yes = 4,
        button_no = 8,
        button_yesno = 12,
    };

public:
    static void Show( const QString &info,
                      QObject *obj,
                      const char *sigOK,
                      MsgBox_Icon icon=icon_question,
                      MsgBox_Button btn=button_okcancel );
    static void Hide();
    static bool isUsed();
private:
    static RMessageBox *_msgBox;
public:
    RMessageBox( QWidget *parent = 0 );

protected:
    virtual void paintEvent( QPaintEvent *event );

Q_SIGNALS:
    void sigOK();

private:
    QString mPrompt;
    MsgBox_Icon mIcon;
    MsgBox_Button mButton;

    QLabel *mpIcon;
    QTextEdit *mpEdit;
};

}

#endif // RMESSAGEBOX_H
