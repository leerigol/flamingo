#include "rinfownd.h"
#include "../draw/rpainter.h"
#include "../../include/dsostd.h"

namespace menu_res {

RInfoWnd_Style RInfoWnd::_style;
RInfoWnd_Style RPopWnd::_style;

RInfoWnd::RInfoWnd(QWidget *parent) : uiWnd(parent)
{
    setWindowFlags( Qt::FramelessWindowHint );
    this->setParent(sysGetMainWindow());

    RInfoWnd::_style.loadResource("ui/wnd/info/");

    set_attr( wndAttr, mWndAttr, wnd_popup );
}

void RInfoWnd::setVisible( bool b )
{
    QWidget::setVisible( b );

    emit sig_visible( b );
}

//! show at middle
void RInfoWnd::resizeEvent(QResizeEvent * /*event*/)
{
    QRect selfRect;
    QRect windowRect;

    selfRect = rect();

    //! parent rect
    windowRect = sysGetMainWindow()->geometry();

    //! left && top
    move( windowRect.left() + (windowRect.width() - selfRect.width())/2,
          windowRect.top() + (windowRect.height() - selfRect.height())/2
          );
}

void RInfoWnd::doPaint(QPaintEvent */*event*/,
                       QPainter &painter )
{
    QRect frameRect = rect();

    RInfoWnd::_style.paint( painter, frameRect );
}

void RInfoWnd::show()
{
    this->installEventFilter(this);
    setVisible(true);
}
void RInfoWnd::hide()
{
    setVisible(false);
    this->removeEventFilter(this);
}

void RInfoWnd::closeEvent(QCloseEvent *)
{
    hide();
}

bool RInfoWnd::eventFilter(QObject *object, QEvent *event)
{
    return menu_res::uiWnd::eventFilter(object, event);
}

RPopWnd::RPopWnd(QWidget *parent) : RInfoWnd( parent )
{
    RPopWnd::_style.loadResource("ui/wnd/popup/");

    setAutoHide();
}

//! show at bottom
void RPopWnd::resizeEvent(QResizeEvent * /*event*/)
{
    QRect selfRect;
    QRect windowRect;

    selfRect = rect();

    windowRect = sysGetMainWindow()->geometry();

    move( windowRect.left() + (windowRect.width() - selfRect.width())/2,
          windowRect.top() + windowRect.height() - selfRect.height()*4
          );

}

void RPopWnd::doPaint(QPaintEvent */*event*/,
                      QPainter &painter )
{
    QRect frameRect = rect();

    RPopWnd::_style.paint( painter, frameRect );
}

}

