#ifndef RPOPMENUCHILDWND_H
#define RPOPMENUCHILDWND_H

#include "uiwnd.h"
#include "../dsowidget/rdsodragdrop.h"

namespace menu_res {

class RPopMenuChildWnd : public uiWnd
{
    Q_OBJECT

public:
    RPopMenuChildWnd( QWidget *parent = 0 );
    ~RPopMenuChildWnd();

    void show();
    void hide();

protected:
    virtual bool eventFilter(QObject *, QEvent *);

public:
    void setLinkKey( int key );

protected:
    int mLinkKey;       /*!< 关联的按键*/
};

}

#endif // RPOPMENUCHILDWND_H
