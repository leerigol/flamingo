#include "rintensitywnd.h"

namespace menu_res {

RIntensityWnd::RIntensityWnd( QWidget *parent ) : RPopWnd(parent)
{
    //! style
    mStyle.loadResource( "ui/intensity/" );

    //! slider
    m_pSlider = new QSlider( this );
    Q_ASSERT( NULL != m_pSlider );
    connect(m_pSlider, SIGNAL(valueChanged(int)),this,SLOT(sliderValueChanged()));

    m_pSlider->setOrientation( Qt::Horizontal );
    m_pSlider->setStyleSheet( mStyle.mStyle );

    m_pSlider->setRange( 0, 99 );

    //! size
    resize( mStyle.mSize.width(), mStyle.mSize.height() );
    m_pSlider->setGeometry( mStyle.mRect );

    m_pLabel = new QLabel(this);
    m_pLabel->setGeometry(10,6,40,15);
    m_pLabel->setStyleSheet( mStyle.mStyle );

    //! auto hide
    setAutoHide();
}

void RIntensityWnd::setIntensity( int intens )
{
    if(isVisible())
    {
        mTimer.start( mTmo );
    }

    m_pSlider->setValue( intens );
    m_pSlider->update();
}

void RIntensityWnd::sliderValueChanged()
{
    QString str = QString::number(m_pSlider->value()+1)+"%";
    float x = m_pSlider->geometry().x();
    float temp = m_pSlider->width();
    x += temp/110*m_pSlider->value();
    m_pLabel->move(x,m_pLabel->y());
    m_pLabel->setText(str);
}

}

