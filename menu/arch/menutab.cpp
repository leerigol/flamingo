
#include <QtWidgets>
#include <QColor>

#include "menutab.h"

#include "../../service/servdso/sysdso.h"   //! sysgetstring

namespace menu_res
{

//! active color
QColor _activeColor( qRgb(0x00,0x59,0xb5) );
QColor _deactiveColor( qRgb(0x08,0x54,0x87) );
QColor _pressColor( qRgb(0x1a,0x1a,0x1a) );
QColor _fontColor = 0xc0c0c0;
#define border_width    3

#define max_title_size_ch  4
#define max_title_size_en  9

//! unfocus color
QColor _activeColor_focusout( qRgb(0x00,0x59/2,0xb5/2) );
QColor _deactiveColor_focusout( qRgb(0x08/2,0x54/2,0x87/2) );
QColor _pressColor_focusout( qRgb(0x1a/2,0x1a/2,0x1a/2) );
QColor _fontColor_focusout = Qt::lightGray;
//! bars
#define normal_bar  QStringLiteral(":/pictures/menu/normalBar.png")
#define active_bar  QStringLiteral(":/pictures/menu/activeBar.png")

MenuColorGp::MenuColorGp()
{}

void MenuColorGp::setColor( QColor act,
               QColor deAct,
               QColor pres,
               QColor font )
{
    mActive = act;
    mDeactive = deAct;
    mPress = pres;
    mFont = font;
}

RTitleElement::RTitleElement()
{
    mTitleId = 0;

    mFgColor = _activeColor;
    mBgColor = Qt::black;

    mTbColor = _activeColor;
    mLbColor = _activeColor;
    mDbColor = _activeColor;

    mSeq = 0;
    mTotal = 0;
}
RTitleElement::~RTitleElement()
{}
void RTitleElement::init( RTitleElementAttr &ele )
{
    mOvHeight = ele.mOvHeight;
    mRect = ele.mRect;
    mSubRect = ele.mSubRect;
    mPath = ele.mPath;

    mLBorder = ele.mLBorder;
    mRBorder = ele.mRBorder;
    mTBorder = ele.mTBorder;
    mDBorder = ele.mDBorder;
}

void RTitleElement::rst()
{}

void RTitleElement::setTitle( const int titleId )
{ mTitleId = titleId; }

void RTitleElement::setOrigin( const QPoint &origin )
{
    mOrigin = origin;

    //! shift the path
    mRect.translate( origin );
    mSubRect.translate( origin );
    mPath.translate( origin );

    mLBorder.translate( origin );
    mRBorder.translate( origin );
    mTBorder.translate( origin );
    mDBorder.translate( origin );
}

void RTitleElement::setColor(
               const QColor &ftColor,
               const QColor &fgColor,
               const QColor &bgColor )
{
    mFontColor = ftColor;
    mFgColor = fgColor;
    mBgColor = bgColor;
}

void RTitleElement::setBgColor( const QColor &color )
{ mBgColor = color; }
void RTitleElement::setFgColor( const QColor &color )
{ mFgColor = color; }

void RTitleElement::setBdColor( const QColor &tbColor,
                 const QColor &lbColor,
                 const QColor &dbColor )
{
    mTbColor = tbColor;
    mLbColor = lbColor;
    mDbColor = dbColor;
}

void RTitleElement::setSequence( int seq, int total )
{
    mSeq = seq;
    mTotal = total;
}

bool RTitleElement::ptInRegion( QPoint pt )
{
    QRect rect;

    rect.setRect( pt.x(), pt.y(), 1, 1 );

    return ( mPath.intersects(rect) );
}

const QRect& RTitleElement::rect()
{
    return mRect;
}

int RTitleElement::getOvHeight()
{
    return mOvHeight;
}

void RTitleElement::paint( QPainter &painter )
{
    QRect rect = mRect;

    painter.save();
    QPen borderPen;

    borderPen.setCapStyle( Qt::RoundCap );

    //! bg
    painter.fillPath( mPath, mBgColor );

    //! border
    borderPen.setWidth( border_width );
    borderPen.setStyle( Qt::SolidLine );

    //! top bd
    borderPen.setColor( mTbColor );
    painter.setPen( borderPen );

    //! tborder
    if ( mSeq == 0 )
    {
        painter.drawPath( mTBorder );
    }
    //! not last
    else if ( mSeq != mTotal - 1 )
    { paintImage( painter, mOrigin, normal_bar ); }
    else
    { paintImage( painter, mOrigin, active_bar ); }

    //! left bd
    borderPen.setColor( mLbColor );
    painter.setPen( borderPen );
    painter.drawPath( mLBorder );

    //! down bd
    borderPen.setColor( mDbColor );
    painter.setPen( borderPen );

    if ( mSeq == mTotal - 1 )
    { painter.drawPath( mDBorder ); }
    else
    { }

    painter.restore();

    //! title
    painter.save();

    painter.setPen( mFontColor );

    QString title;
    title = sysGetString( mTitleId, "unk" );
    if( title.size() == 0 )
    {
        qDebug() << mTitleId << __FUNCTION__ << __LINE__;
        Q_ASSERT( title.size() > 0 );
    }

    QString abbTitle = title;
    //! ch/tch/jp/korea
    if ( title[0].unicode() >= 0x3000 &&
         title[0].unicode() <= 0x9fff )
    {
        //abbTitle = sysTrimString( title, max_title_size_ch );

        QString str;
        for ( int i = 0; i < abbTitle.size()-1; i++ )
        {
            str = str + abbTitle[i] + '\n';
        }

        //! the last one
        str += abbTitle[ abbTitle.size() - 1 ];

        rect.setX(rect.x()+4);
        //! adjust the slope Qt::AlignRight | Qt::AlignVCenter
        painter.drawText( rect,Qt::AlignCenter, str );
    }
    else
    {
        painter.rotate( 90.0f );
        genTextRect( rect );

        //abbTitle = sysTrimString( title, max_title_size_en );

        painter.drawText( rect, Qt::AlignHCenter | Qt::AlignTop, title );
    }

    painter.restore();
}

void RTitleElement::genTextRect( QRect &rect )
{
    rect = mRect;

    //! painter rotate 90
    rect.setRect( mRect.top(),
                  -mRect.width(),
                  mRect.height(),
                  mRect.width() );
}

void RTitleElement::paintVText( QPainter &painter,
                 QString &str,
                 QRect &rect,
                 int height )
{
    int len = str.length();

    Q_ASSERT( height > 0 );
    int count = rect.height() / height;

    QRect subRect, boundRect;
    subRect.setRect( 0, 0, rect.width(), height );
    QFontMetrics metrics = painter.fontMetrics();
    for ( int i = 0; i < len && i < count; i++ )
    {
        boundRect = metrics.boundingRect( str.at(i) );
        subRect.setHeight( boundRect.height() );

        painter.drawText( subRect, Qt::AlignRight, str.at(i) );

        subRect.adjust(0, boundRect.height(), 0, boundRect.height() );
    }

}

void RTitleElement::paintImage(
                 QPainter &painter,
                 QPointF pt,
                 const QString &imgName )
{
    QImage img;

    if ( !img.load( imgName ) )
    {
        qWarning()<<"invalid image";
        return;
    }

    painter.drawImage( pt, img );

}

void RTitleElement::makePath( QPoint *pPoints,
                              QPainterPath *pPath )
{
    Q_ASSERT( NULL != pPoints );
    Q_ASSERT( NULL != pPath );

    QPainterPath clrPath;
    *pPath = clrPath;

    //! origin
    pPath->moveTo( pPoints[0] );

    //! append from [1]
    appendPath( pPoints + 1, pPath );
}

void RTitleElement::appendPath( QPoint *pPoints,
                                QPainterPath *pPath )
{
    Q_ASSERT( NULL != pPoints );
    Q_ASSERT( NULL != pPath );

    int i = 0;
    while( pPoints[i].x() != -1 )
    {
        pPath->lineTo( pPoints[i] );
        i++;
    }
}

void RTitleElement::closePath( QPainterPath *pPath )
{
    Q_ASSERT( NULL != pPath );

    pPath->closeSubpath();
}

#define end_of_bd()     {-1,-1}

QPoint _top_top_bd[]=
{
    {33,0},
    {18,0},

    end_of_bd(),
};

QPoint _top_left_bd[]=
{
    {18,0},
    {0,18},
    {0,100},

    end_of_bd(),
};

QPoint _top_down_bd[]=
{
    {0,100},
    {33,132},

    end_of_bd(),
};

RTitleElementAttr RTopTitle::_attr;
RTopTitle::RTopTitle()
{
    //! template
    makePath( _top_top_bd, &RTopTitle::_attr.mTBorder );
    makePath( _top_left_bd, &RTopTitle::_attr.mLBorder );
    makePath( _top_down_bd, &RTopTitle::_attr.mDBorder );

    makePath( _top_top_bd, &RTopTitle::_attr.mPath );
    appendPath( _top_left_bd, &RTopTitle::_attr.mPath );
    appendPath( _top_down_bd, &RTopTitle::_attr.mPath );
    closePath( &RTopTitle::_attr.mPath );

    RTopTitle::_attr.mRect.setRect( 0,0, 32, 130 );
    //RTopTitle::_attr.mSubRect.setRect( 0,15, 32, 110-15 );


    RTopTitle::_attr.mOvHeight = 98;
}

void RTopTitle::rst()
{
    init( RTopTitle::_attr );
}

QPoint _bot_top_bd[]=
{
    {33,33},
    {0,0},

    end_of_bd(),
};

QPoint _bot_left_bd[]=
{
    {0,0},
    {0,132},

    end_of_bd(),
};

QPoint _bot_down_bd[]=
{
    {0,132},
    {33,165},

    end_of_bd(),
};

RTitleElementAttr RBottomTitle::_attr;
RBottomTitle::RBottomTitle()
{
    //! template
    makePath( _bot_top_bd, &RBottomTitle::_attr.mTBorder );
    makePath( _bot_left_bd, &RBottomTitle::_attr.mLBorder );
    makePath( _bot_down_bd, &RBottomTitle::_attr.mDBorder );

    makePath( _bot_top_bd, &RBottomTitle::_attr.mPath );
    appendPath( _bot_left_bd, &RBottomTitle::_attr.mPath );
    appendPath( _bot_down_bd, &RBottomTitle::_attr.mPath );
    closePath( &RBottomTitle::_attr.mPath );

    RBottomTitle::_attr.mRect.setRect( 0,0, 32, 164 );
    //RBottomTitle::_attr.mSubRect.setRect( 0,32, 32, 121-32 );

    RBottomTitle::_attr.mOvHeight = 126;
}

void RBottomTitle::rst()
{
    init( RBottomTitle::_attr );
}

RMenuTab::RMenuTab(QWidget *parent) :
    QWidget(parent)
{
    mTitleCount = 0;

    //! default
    focusInColorGp.setColor( _activeColor,
                            _deactiveColor,
                            _pressColor,
                            _fontColor );
    focusOutColorGp.setColor( _activeColor_focusout,
                              _deactiveColor_focusout,
                              _pressColor_focusout,
                              _fontColor_focusout );

    setStatus( element_actived );
}

RMenuTab::~RMenuTab()
{
}

void RMenuTab::cfgColorGp( MenuColorGp &focusGp,
                 MenuColorGp &focusOutGp )
{
    focusInColorGp = focusGp;
    focusOutColorGp = focusOutGp;
}

void RMenuTab::cfgFontColor( const QColor &focusIn,
                                  const QColor &focusOut )
{
    focusInColorGp.mFont = focusIn;
    focusOutColorGp.mFont = focusOut;
}

void RMenuTab::doPaint( QPainter &painter )
{
    RTitleElement *pEle;

    for ( int i = 0; i < mTitleCount; i++ )
    {
        pEle = mTitleList[i];
        Q_ASSERT( pEle != NULL );

        pEle->paint( painter );
    }
}

void RMenuTab::mousePressEvent(QMouseEvent *evt )
{
    int id;

    id = findIdByPt( evt->pos() );

    if ( id >= 0 )
    {
        mTitleList[id]->setBgColor( mCurrentColorGp.mPress );
        update();
    }
    else
    {}
}

void RMenuTab::mouseReleaseEvent(QMouseEvent *evt)
{
    int id;

    id = findIdByPt( evt->pos() );

    if ( id >= 0 )
    {
        //! now
        if ( id != mActiveId )
        {
            emit sig_active_changed( mTitleCount, id );
        }
        else
        {
            emit sig_active_pressed( mTitleCount, id );
        }
        setActive(id);
    }
    else
    {}
}

int RMenuTab::findIdByPt( const QPoint &pt )
{
    QPoint pos;
    RTitleElement *pEle;

    pos = pt;

    //! check the active at first
    if ( mTitleList[mActiveId]->ptInRegion(pos) )
    { return mActiveId; }

    //! for the deactive
    //! from top to down
    pos = pt;
    for ( int i = 0 ; i < mTitleCount; i++ )
    {
        pEle = mTitleList[i];
        Q_ASSERT( NULL != pEle );

        if ( pEle->ptInRegion(pos) )
        {
            return i;
        }
    }

    return -1;
}

void RMenuTab::setTitle( const QList<int> &titles )
{
    mTitleIds = titles;

    mTitleCount = mTitleIds.size();
    Q_ASSERT( mTitleCount > 0 );

    mActiveId = mTitleCount - 1;

    relayout();
}

void RMenuTab::setActive( int id )
{
    if ( id < 0 || id >= mTitleCount )
    { return; }

    mActiveId = id;

    relayout();

    update();
}

QRect RMenuTab::getActiveRect()
{
    RTitleElement *pEle;

    pEle = mTitleList[mActiveId];
    Q_ASSERT( NULL != pEle );

    QRect rect = pEle->rect();

    return rect;
}

void RMenuTab::setStatus( ElementStatus stat )
{
    meStatus = stat;

    //! color init
    if ( meStatus == element_actived
         || meStatus == element_actived_down )
    {
        mCurrentColorGp = focusInColorGp;
    }
    else
    {  
        mCurrentColorGp = focusOutColorGp;
    }

    relayout();

    update();
}
ElementStatus RMenuTab::getStatus()
{
    return meStatus;
}

void RMenuTab::relayout()
{
    //! delete the resource
    for ( int i = mTitleCount; i < mTitleList.size(); i++ )
    {
        Q_ASSERT( mTitleList[i] != NULL );
        delete mTitleList[i];
        mTitleList[i] = NULL;
    }

    //! remove the deleted item
    mTitleList.removeAll( NULL );

    //! new one
    RTitleElement *pEle;
    for ( int i = mTitleList.size(); i < mTitleCount; i++ )
    {
        //! the first item
        if ( i == 0 )
        {
            pEle = new RTopTitle();
            Q_ASSERT( NULL != pEle );
        }
        else
        {
            pEle = new RBottomTitle();
            Q_ASSERT( NULL != pEle );
        }

        mTitleList.append( pEle );
    }

    //! sure
    Q_ASSERT( mTitleCount == mTitleList.size() );

    //! -- placement

    //! set title
    int mOrig = 0;
    for( int i = 0; i < mTitleCount; i++ )
    {
        pEle = mTitleList[i];
        Q_ASSERT( NULL != pEle );

        pEle->rst();

        Q_ASSERT( i >= 0 && i < mTitleIds.size() );
        pEle->setTitle( mTitleIds[i] );

        pEle->setOrigin( QPoint(0,mOrig) );        
        mOrig += pEle->getOvHeight();
        if( mOrig>=350)
        {
            mOrig-=21;
        }
    }

    //! top item
    for( int i = 0; i < 1 && i < mTitleCount; i++ )
    {
        pEle = mTitleList[i];
        Q_ASSERT( NULL != pEle );

        pEle->setColor( mCurrentColorGp.mFont,
                        mCurrentColorGp.mDeactive );
        pEle->setBdColor( mCurrentColorGp.mActive,
                          mCurrentColorGp.mActive,
                          mCurrentColorGp.mDeactive );

        pEle->setSequence( 0, mTitleCount );
    }

    //! middle item
    for ( int i = 1; i < mTitleCount - 1 ; i++ )
    {
        pEle = mTitleList[i];
        Q_ASSERT( NULL != pEle );

        pEle->setColor( mCurrentColorGp.mFont,
                        mCurrentColorGp.mDeactive );
        pEle->setBdColor( mCurrentColorGp.mDeactive,
                          mCurrentColorGp.mActive,
                          mCurrentColorGp.mDeactive );

        pEle->setSequence( i, mTitleCount );
    }

    //! last item
    for ( int i = mTitleCount - 1; i < mTitleCount && i >=0 ; i++ )
    {
        pEle = mTitleList[i];
        Q_ASSERT( NULL != pEle );

        pEle->setColor( mCurrentColorGp.mFont,
                        mCurrentColorGp.mActive );
        pEle->setBdColor( mCurrentColorGp.mActive,
                          mCurrentColorGp.mActive,
                          mCurrentColorGp.mActive );

        pEle->setSequence( i, mTitleCount );
    }
}

}
