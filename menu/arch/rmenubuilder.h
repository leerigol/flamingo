#ifndef RMENUBUILDER_H
#define RMENUBUILDER_H

#include "../menuwidget/rcontrol.h"
#include "rmenures.h"

namespace   menu_res {

typedef QMap<int, CResMenuItem*> tItemMap;

/*!
 * \brief The RMenuBuilder class
 * menu builder
 */
class RMenuBuilder
{
private:
    static REventFilter *_pEvtFilter;

    //! config items
    static QString _descFile;   /*!< 资源描述文件*/
    static QString _menuFile;   /*!< 菜单文件*/
    static QString _resFile;    /*!< 菜单资源索引*/

public:
    static void installEventFilter();
    static void uninstallEventFilter();

    static void attachResource();
    static void on_value_changed( int msg );

    static QWidget* findView(const QString &servName,
                                 int msg );

    static QString getString( int msg, const QString &defStr="" );
    static QString getOptionString(int msg, int opt, const QString &defStr="" );

    static int getImage( int msg, QImage &img );
    static int getResource( int msg, QString &str, QImage &img );

private:
    static CResItem *findRes( int msg );

public:
    RMenuBuilder();
public:
    RServMenu* loadMenu( const QString &strRsrcName,
                         QWidget *parent=0,
                         int srcId = 0,
                         int dstId = 0 );

    int loadServMenu( CServMenu *pMenu, vDesc *pDesc );
    int loadServRes( CServRes *pRes, vDesc *pDesc );

    RServMenu * newMenu( CServMenu *pMenu,
                           CServRes *pres,
                           QWidget *parent );

    RMenu* buildAMenu( RServMenu *pServ,
                       CResMenu *p_aMenu,
                       QWidget *parent,
                       tItemMap &list );

    void crossRefMenu( RServMenu *pServMenu);

protected:
    void openMenuItem( tItemMap &list  );
    CResMenuItem * findMenuItem( int msg, tItemMap &list );
    void insertMenuItem( int msg, CResMenuItem *pMenuItem,
                       tItemMap &list );

    void clearMenuItem( tItemMap &list );

};

}
#endif // RMENUBUILDER_H
