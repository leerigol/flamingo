#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include "../menustyle/rui_type.h"

namespace menu_res
{

#define title_sep   "\\"

class RTitleElementAttr
{
public:
    int   mOvHeight;        //! over height
    QRect mRect;            //! base rect
    QRect mSubRect;         //! sub rect
    QPainterPath mPath;     //! base path

    QPainterPath mLBorder;  //! left border
    QPainterPath mRBorder;  //! right border
    QPainterPath mTBorder;  //! top border
    QPainterPath mDBorder;  //! down border
};

class MenuColorGp
{
public:
    MenuColorGp();

public:
    void setColor( QColor act,
                   QColor deAct,
                   QColor pres,
                   QColor font );

public:
    QColor mActive, mDeactive, mPress, mFont;

};

class RTitleElement : public RTitleElementAttr
{
public:
    RTitleElement();
    virtual ~RTitleElement();
protected:
    void init( RTitleElementAttr &ele );
public:
    virtual void rst();

public:
    void setTitle( const int titleId );

    void setOrigin( const QPoint &origin );

    void setColor( const QColor &ftColor,
                   const QColor &fgColor,
                   const QColor &bgColor=Qt::black );

    void setBgColor( const QColor &color );
    void setFgColor( const QColor &color );

    void setBdColor( const QColor &tbColor,
                     const QColor &lbColor,
                     const QColor &dbColor );

    void setSequence( int seq, int total );

    bool ptInRegion( QPoint pt );

    const QRect& rect();

    int getOvHeight();

public:
    virtual void paint( QPainter &painter );

    //! gen the text rect
    void genTextRect( QRect &rect );

    void paintVText( QPainter &painter,
                     QString &str,
                     QRect &rect,
                     int height );

protected:
    void paintImage( QPainter &painter,
                     QPointF pt,
                     const QString &imgName );

    void makePath( QPoint *pPoints,
                   QPainterPath *pPath );

    void appendPath( QPoint *pPoints,
                     QPainterPath *pPath );

    void closePath( QPainterPath *pPath );

protected:
    int mTitleId;
    QPoint mOrigin;         //! origin

    QColor mFontColor, mFgColor, mBgColor;

    QColor mTbColor, mLbColor, mDbColor;
    int mSeq, mTotal;
};

class RTopTitle : public RTitleElement
{
private:
    static RTitleElementAttr _attr;

public:
    RTopTitle();

public:
    virtual void rst();

};

class RBottomTitle : public RTitleElement
{
private:
    static RTitleElementAttr _attr;
public:
    RBottomTitle();

public:
    virtual void rst();
};

class RMenuTab : public QWidget
{
    Q_OBJECT

public:
    explicit RMenuTab(QWidget *parent = 0);
    virtual ~RMenuTab();

    void cfgColorGp( MenuColorGp &focusGp,
                     MenuColorGp &focusOutGp );
    void cfgFontColor( const QColor &focusIn,
                       const QColor &focusOut );

protected:
    void doPaint( QPainter &painter );

protected:
    virtual void mousePressEvent(QMouseEvent *evt );
    virtual void mouseReleaseEvent(QMouseEvent *evt);

protected:
    int findIdByPt( const QPoint &pt );



Q_SIGNALS:
    void sig_active_changed( int total, int now );
    void sig_active_pressed( int total, int now );
public:
    void setTitle( const QList<int> &titles );
    void setActive( int id );
    QRect getActiveRect();

    void setStatus( ElementStatus stat );
    ElementStatus getStatus();

protected:
    void relayout();

protected:
    ElementStatus meStatus;

public:
    QList<int> mTitleIds;

    int mTitleCount;
    int mActiveId;

    QList<RTitleElement*> mTitleList;

    //! colors
    MenuColorGp mCurrentColorGp;

protected:
    MenuColorGp focusInColorGp, focusOutColorGp;
};

}

#endif // WIDGET_H
