#include "rmenu.h"

#include "rmenupage.h"
#include "rservmenu.h"

#include "../menuwidget/rcontrol.h"
#include "../menuwidget/rpopupview.h"
#include "../menuwidget/rcombox.h"
#include "../wnd/rmenuwnd.h"

namespace menu_res {

/*!
 * \var RMenu::_activeMenu
 */
RMenu * RMenu::_activeMenu = NULL;

/*!
 * \brief RMenu::findId
 * \param pWidget
 * \return
 * 查找到的是控件在页面中的位置序号
 */
int RMenu::findId( QWidget *pWidget )
{
    int id;

    RMenuPage *pPage;
    foreach( pPage, RMenu::_activeMenu->mPageList )
    {
        id = pPage->findId( pWidget );
        if ( id != -1 )
        {
            return id;
        }
    }

    return -1;
}

RMenu::RMenu()
{
    init();
}

RMenu::RMenu( RServMenu *pServ )
{
    Q_ASSERT( pServ != NULL );

    init();

    m_pServMenu = pServ;
}

void RMenu::init()
{
    mRoll = true;
    mbInTask = false;
    mActivePage = 0;
    m_pParent = NULL;

    m_pResMenu = NULL;
    m_pServMenu = NULL;

}

/*!
 * \brief RMenu::postKey
 * \param wigId
 * \param cnt
 * \param type
 * 消息发送到页面上的第n个部件上
 */
void RMenu::postKey( int wigId,
                     int /*key*/,
                     int cnt, QEvent::Type type )
{
    Q_ASSERT( mActivePage >=0 && mActivePage < mPageList.size() );

    //! has active page
    if ( mPageList[mActivePage] )
    {
        //! the menu widget
        mPageList[mActivePage]->postKey( wigId, R_Skey_Active, cnt, type );
    }
}

/*!
 * \brief RMenu::postKey
 * \param key
 * \param cnt
 * \param type
 * tune proc
 */
void RMenu::postKey( int key,
                     int cnt,
                     QEvent::Type type )
{
    QWidget *pWig;

    //! tune focus
    pWig = RMenuPage::getTuneWidget();
    if ( NULL != pWig )
    {
        REventFilter::postkey( pWig,
                               key,
                               cnt,
                               type );
        return;
    }

    REventFilter::postkey( QApplication::focusWidget(),
                           key,
                           cnt,
                           type );
}

void RMenu::setParent( RMenu *parent )
{
    //! parent can be null
    m_pParent = parent;
}

RMenu * RMenu::getParent() const
{
    return m_pParent;
}

void RMenu::setRoll( bool b )
{
    mRoll = b;
}
bool RMenu::getRoll()
{
    return mRoll;
}
/*!
 * \brief RMenu::pushStack
 * -菜单被其它服务通过intent方式切换时相当于被其他
 * 服务所调用，这时需要将当前页面入栈
 * -在进行回退时，回退到的将是调用前的才菜单
 *
 * \todo 被调用菜单显示时，如果是下级菜单，也不需要显示父级菜单
 */
void RMenu::pushStack()
{
    mbInTask = true;
}
void RMenu::popStack()
{
    mbInTask = false;
}

void RMenu::setActive()
{
    //! menu visible
    Q_ASSERT( sysGetMenuWindow() != NULL );

    RMenuWnd *pMenuWnd;
    bool bVisible;
    pMenuWnd = dynamic_cast<RMenuWnd*>( sysGetMenuWindow() );
    Q_ASSERT( pMenuWnd != NULL );
    bVisible = pMenuWnd->isExpand();
    pMenuWnd->setExpand( true );

    //! is active now
    if ( RMenu::_activeMenu == this )
    {
        //! pre visible = true
        if ( bVisible )
        {
            //! play active animation
            RMenu::_activeMenu->active();
        }
        //! pre visible = false
        else
        {}
        return;
    }

    //! has active menu now
    if ( RMenu::_activeMenu )
    {
        RMenu::_activeMenu->deActive();
    }
    RMenu::_activeMenu = this;

    //! inactive to active
    RMenu::_activeMenu->active( 1 );
}
bool RMenu::getActive()
{
    return this == RMenu::_activeMenu;
}

void RMenu::addPage( RMenuPage *pPage )
{
    Q_ASSERT( pPage != NULL );
    mPageList.append( pPage );

    pPage->setParent( this );
}

void RMenu::cfgFontColor( const QColor &focusIn,
                          const QColor &focusOut )
{
    foreach( RMenuPage* pPage, mPageList )
    {
        Q_ASSERT( NULL != pPage );

        pPage->cfgFontColor( focusIn, focusOut );
    }
}

void RMenu::moveNext()
{
    if ( mActivePage < mPageList.size() - 1 )
    {
        mPageList[mActivePage]->onDeActive();
        mPageList[mActivePage]->hide();
        mActivePage++;
        mPageList[mActivePage]->onActive();
        mPageList[mActivePage]->show();
    }
    //! to first
    else if ( mRoll )
    {
        mPageList[mActivePage]->onDeActive();
        mPageList[mActivePage]->hide();
        mActivePage = 0;
        mPageList[mActivePage]->onActive();
        mPageList[mActivePage]->show();

    }
    //! do nothing
    else
    {
    }
}
void RMenu::movePrevious()
{
    if ( mActivePage > 0 )
    {
        mPageList[mActivePage]->onDeActive();
        mPageList[mActivePage]->hide();
        mActivePage--;
        mPageList[mActivePage]->onActive();
        mPageList[mActivePage]->show();
    }
    //! to last
    else if ( mRoll )
    {
        mPageList[mActivePage]->onDeActive();
        mPageList[mActivePage]->hide();
        mActivePage = mPageList.size() - 1;
        mPageList[mActivePage]->onActive();
        mPageList[mActivePage]->show();
    }
    //! do nothing
    else
    {
    }
}

void RMenu::moveToParent()
{
    if ( mbInTask )
    {
        popStack();

        serviceExecutor::post( m_pServMenu->getConsoleName(),
                               CMD_SERVICE_DEACTIVE,
                               0
                               );
        LOG_DBG();
        return;
    }

    //! not top
    if ( NULL != m_pParent )
    {
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                               CMD_SERVICE_SUB_RETURN,
                               m_pResMenu->m_pResItem->getId()
                               );

        //! active the child
        m_pParent->setActive();

        LOG_DBG();
    }
    //! top now
    else
    {
        serviceExecutor::post( m_pServMenu->getConsoleName(),
                               CMD_SERVICE_DEACTIVE,
                               0
                               );
    }
}

void RMenu::moveToChild( RMenu *pMenuChild )
{
    Q_ASSERT( pMenuChild != NULL );

    //! set parent
    pMenuChild->setParent( this );

    //! active the child
    pMenuChild->setActive();
}

void RMenu::moveToSibling( RMenu *pMenu )
{
    Q_ASSERT( pMenu != NULL );

    //! parent do not change
    pMenu->setParent( this->getParent() );

    pMenu->setActive();
}

RMenu * RMenu::cascadeLeaf( int msg,
                            QList<int> &rootedList )
{
    RMenu *pMenu;

    foreach( RMenuPage* pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );
        pMenu = pPage->getOptionMenu( msg, rootedList );
        if ( NULL != pMenu )
        {
            return pMenu;
        }
    }

    return NULL;
}

RMenu *RMenu::expandLeaf()
{
    int i;
    QList<int> routeMsgList;
    QList<int> optMenuMsgList;

    RMenu *pMenu;

    pMenu = this;

    //! option menu msg list
    collectOptionMenuMsg( optMenuMsgList );

    //! expand the child menu successively
    for ( i = 0; i < optMenuMsgList.count(); i++ )
    {
        pMenu = pMenu->cascadeLeaf( optMenuMsgList[i], routeMsgList );

        if( pMenu == NULL )
        {
            LOG_DBG()<<optMenuMsgList[i]<<routeMsgList<<optMenuMsgList;
        }
        Q_ASSERT( pMenu != NULL );

        optMenuMsgList.clear();
        //! recollect option menu list
        pMenu->collectOptionMenuMsg( optMenuMsgList );
    }

    return pMenu;
}

bool RMenu::containsMsg( int msg )
{
    RMenuPage* pPage;

    Q_ASSERT( m_pResMenu != NULL );
    Q_ASSERT( m_pResMenu->m_pResItem != NULL );

    //! 标题消息匹配
    if ( msg == m_pResMenu->m_pResItem->getId() )
    {
        return true;
    }

    foreach( pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );

        if ( pPage->containsMsg(msg) )
        {
            return true;
        }
    }

    return false;
}

void RMenu::collectOptionMenuMsg( QList<int> &optMenuMsg )
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );

        pPage->collectOptionMenuMsg( optMenuMsg );
    }
}

void RMenu::active( int intoAct )
{
    if ( mPageList.count() < 1 )
    {
        return;
    }

    if ( mActivePage >=0 && mActivePage < mPageList.count() )
    {
        mPageList[ mActivePage]->onActive( intoAct );

        mPageList[ mActivePage]->show();
    }
}

void RMenu::deActive()
{
    if ( mPageList.count() < 1 )
    {
        return;
    }

    //! for all deactive
    if ( mActivePage >=0 && mActivePage < mPageList.count() )
    {
        mPageList[ mActivePage]->onDeActive();

        mPageList[ mActivePage]->hide();
    }
}

void RMenu::reTranslate()
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        pPage->reTranslate();
    }
}

void RMenu::setName( const QString &str )
{
    mName = str;
}
QString & RMenu::getName()
{
    return mName;
}

void RMenu::setServMenu( RServMenu *pServMenu )
{
    Q_ASSERT( pServMenu != NULL );

    m_pServMenu = pServMenu;
}
RServMenu *RMenu::getServMenu()
{
    return m_pServMenu;
}

void RMenu::setResMenu( CResMenu *pResMenu )
{
    m_pResMenu = pResMenu;
}
CResMenu *RMenu::getResMenu()
{
    return m_pResMenu;
}

//! change page id
void RMenu::setTitle( int titleId )
{
    Q_ASSERT( m_pResMenu != NULL );
    Q_ASSERT( m_pResMenu->m_pResItem != NULL );
    m_pResMenu->m_pResItem->setId( titleId );
}

void RMenu::replaceTitle( int srcTitle,
                          int dstTitle )
{
    Q_ASSERT( m_pResMenu != NULL );
    Q_ASSERT( m_pResMenu->m_pResItem != NULL );

    if ( m_pResMenu->m_pResItem->getId() == srcTitle )
    {
        m_pResMenu->m_pResItem->setId( dstTitle );
    }
}

void RMenu::getTitle( QList<int> &idList )
{
    Q_ASSERT( m_pResMenu != NULL );

    int selfTitleId;

    selfTitleId = m_pResMenu->m_pResItem->getId();

    //! recursively
    if ( m_pParent != NULL )
    {
        m_pParent->getTitle( idList );

        idList.append( selfTitleId );
    }
    //! the first one
    else
    {
        idList.append( selfTitleId );
    }
}

QWidget * RMenu::findView( int msg )
{
    RMenuPage* pPage;
    QWidget *pWig;
    foreach( pPage, mPageList )
    {
        pWig = pPage->findView( msg );
        if ( NULL != pWig )
        { return pWig; }
    }

    return NULL;
}

void RMenu::crossRef( RServMenu *pServMenu )
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        pPage->crossRef( pServMenu );
    }
}

void RMenu::on_value_changed( int msg )
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );
        pPage->on_value_changed( msg );
    }
}

void RMenu::on_enable_changed( int msg, bool b )
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );
        pPage->on_enable_changed(msg,b);
    }
}
void RMenu::on_visible_changed( int msg, bool b )
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );
        pPage->on_visible_changed(msg,b);
    }
}

void RMenu::on_enable_changed( int msg, int opt, bool b )
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );
        pPage->on_enable_changed(msg, opt, b);
    }
}
void RMenu::on_visible_changed( int msg, int opt, bool b )
{
    RMenuPage* pPage;

    foreach( pPage, mPageList )
    {
        Q_ASSERT( pPage != NULL );
        pPage->on_visible_changed(msg, opt, b );
    }
}

void RMenu::on_context_changed( int msg )
{
    Q_ASSERT( mActivePage >=0 && mActivePage < mPageList.size() );

    mPageList[mActivePage]->on_context_changed( msg );
}

void RMenu::on_focus_changed( int msg )
{
    Q_ASSERT( mActivePage >=0 && mActivePage < mPageList.size() );

    mPageList[mActivePage]->on_focus_changed( msg );
}

void RMenu::update()
{
    Q_ASSERT( mActivePage >=0 && mActivePage < mPageList.size() );

    mPageList[mActivePage]->update();
}

}
