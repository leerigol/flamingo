#include "rservmenu.h"
#include "rmenu.h"
#include "rmenubuilder.h"
#include "../../include/dsodbg.h"
namespace menu_res {

RMenu **RServMenu::_subMenuAry = NULL;
RMenu **RServMenu::_optMenuAry = NULL;
QMap<int, QWidget*> RServMenu::_mServWidgets;

void RServMenu::openMenuMapCache()
{
    _subMenuAry = NULL;
    _optMenuAry= NULL;
}
void RServMenu::closeMenuMapCache()
{
    if ( NULL != _subMenuAry )
    {
        delete []_subMenuAry;
        _subMenuAry = NULL;
    }

    if ( NULL != _optMenuAry )
    {
        delete []_optMenuAry;
        _optMenuAry = NULL;
    }
}

//added by hexiaohua. for bug2758
void RServMenu::removeServiceWidget(int msg)
{
    if ( RServMenu::_mServWidgets.contains(msg) )
    {
        RServMenu::_mServWidgets.remove( msg );
    }
}

void RServMenu::attachServiceWidget( int msg, QWidget *pWig )
{
    Q_ASSERT( NULL != pWig );

    if ( !RServMenu::_mServWidgets.contains(msg) )
    {
        RServMenu::_mServWidgets.insert( msg, pWig );
    }
}

RServMenu::RServMenu()
{
    m_pServMenu = NULL;
    m_pServRes = NULL;

    m_pLastMenu = NULL;
}
RServMenu::RServMenu( CServMenu *pServMenu,
           CServRes *pServRes
           )
{
    m_pServMenu = pServMenu;
    m_pServRes = pServRes;

    m_pLastMenu = NULL;

    setConsoleName( m_pServMenu->getServName() );
}

RServMenu::~RServMenu()
{
    foreach( RMenu *pMenu, mListMenu )
    {
        Q_ASSERT( NULL != pMenu );
        delete pMenu;
    }

    //! collect resorce
    if ( m_pServMenu !=NULL )
    {
        delete m_pServMenu;
        m_pServMenu = NULL;
    }
    if ( m_pServRes !=NULL )
    {
        delete m_pServRes;
        m_pServRes = NULL;
    }
}

void RServMenu::addMenu( RMenu *pMenu )
{
    mListMenu.append( pMenu );
}

QList<RMenu*> & RServMenu::getMenus()
{
    return mListMenu;
}

RMenu * RServMenu::operator[]( int i )
{
    if ( i < 0 || i >= mListMenu.size() )
    {
        return NULL;
    }

    return mListMenu[i];
}

void RServMenu::setConsoleName( const QString &str )
{
    mConsoleName = str;
}
QString & RServMenu::getConsoleName()
{
    return mConsoleName;
}

/*!
 * \brief RServMenu::findMenu
 * \param id
 * \return
 * find menu in service menus by child id
 */
RMenu * RServMenu::findMenu( int id )
{
    if ( id < 0 || id >= m_pServMenu->mRefMenus.size() )
    {
        Q_ASSERT( false );
        return NULL;
    }

    if ( NULL != _subMenuAry )
    {
        return _subMenuAry[ id ];
    }

    Q_ASSERT( false );
    return NULL;
}

/*!
 * \brief RServMenu::findOptionMenu
 * \param id
 * \return
 * find menu by option menu id
 */
RMenu * RServMenu::findOptionMenu( int id )
{
    if ( id < 0 || id >= m_pServRes->mRefMenus.size() )
    {
        Q_ASSERT( false );
        return NULL;
    }

    if ( NULL != _optMenuAry  )
    {
        return _optMenuAry[ id ];
    }

    Q_ASSERT( false );
    return NULL;
}

//! 根据消息找资源
CResItem *RServMenu::fineRes( int msg )
{
    Q_ASSERT( NULL != m_pServRes );
    CResItem *pItem;

    foreach( pItem, m_pServRes->mResTree )
    {
        Q_ASSERT(pItem!=NULL);

        if ( pItem->getId() == msg )
        { return pItem; }
    }

    return NULL;
}

RMenu * RServMenu::findMenuByMsg( int msg )
{
    foreach( RMenu *pMenu, mListMenu )
    {
        if (pMenu->containsMsg(msg))
        {
            return pMenu;
        }
    }

    return NULL;
}

QWidget * RServMenu::findView( int msg )
{
    QWidget *pWig;
    foreach( RMenu *pMenu, mListMenu )
    {
        pWig = pMenu->findView( msg );
        if ( NULL != pWig )
        { return pWig; }
    }

    return NULL;
}

CServMenu *RServMenu::getCServMenu()
{
    return m_pServMenu;
}

void RServMenu::reTranslate()
{
    foreach( RMenu *pMenu, mListMenu )
    {
        pMenu->reTranslate();
    }
}

void RServMenu::buildCrossArray()
{
    if ( m_pServMenu->mRefMenus.size() < 1 )
    {
    }

    //! create the array buffer
    if ( m_pServMenu->mRefMenus.size() > 0 )
    {
        _subMenuAry = new RMenu*[ m_pServMenu->mRefMenus.size() ];
        Q_ASSERT( NULL != _subMenuAry );
    }

    if ( m_pServRes->mRefMenus.size() > 0 )
    {
        _optMenuAry = new RMenu*[ m_pServRes->mRefMenus.size() ];
        Q_ASSERT( NULL != _optMenuAry );
    }

    QList<RMenu*> selfMenuList;
    int id;
    RMenu *pMenu;
    QString refName;

    //! cache menu in
    //! menu ref
    selfMenuList = mListMenu;
    for ( id = 0; id < m_pServMenu->mRefMenus.size(); id++ )
    {
        //! inited
        _subMenuAry[id] = NULL;

        refName = *m_pServMenu->mRefMenus[id];

        //! fill the table
        foreach( pMenu, selfMenuList )
        {
            if ( pMenu->getName() == refName )
            {
                _subMenuAry[id] = pMenu;
                selfMenuList.removeOne( pMenu );
                break;
            }
        }
    }

    //! resource ref
    selfMenuList = mListMenu;
    for( id = 0; id < m_pServRes->mRefMenus.size(); id++ )
    {
        _optMenuAry[id] = NULL;
        refName = *m_pServRes->mRefMenus[id];

        foreach( pMenu, selfMenuList )
        {
            if ( pMenu->getName() == refName )
            {
                _optMenuAry[id] = pMenu;
                selfMenuList.removeOne( pMenu );
                break;
            }
        }
    }
}

void RServMenu::initResAttr( ui_attr::uiAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );

    Q_ASSERT( NULL != m_pServRes );

    m_pServRes->initResAttr( pAttr );
}

void RServMenu::cfgFontColor( const QColor &focusIn,
                              const QColor &focusOut )
{
    foreach( RMenu *pMenu, mListMenu )
    {
        Q_ASSERT( NULL != pMenu );
        pMenu->cfgFontColor( focusIn, focusOut );
    }
}

void RServMenu::on_value_changed( int msg )
{
    foreach( RMenu *pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );
        pMenu->on_value_changed( msg );
    }

    //! 更新在keymapper中的控件
    RMenuBuilder::on_value_changed( msg );
}
void RServMenu::on_enable_changed( int msg, bool b )
{
    RMenu* pMenu;

    foreach( pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );
        pMenu->on_enable_changed( msg, b );
    }
}
void RServMenu::on_visible_changed( int msg, bool b )
{
    foreach( RMenu *pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );
        pMenu->on_visible_changed( msg, b );
    }
}
void RServMenu::on_enable_changed( int msg, int opt, bool b )
{
    foreach( RMenu *pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );
        pMenu->on_enable_changed( msg, opt, b );
    }
}

void RServMenu::on_visible_changed( int msg, int opt, bool b )
{
    foreach( RMenu *pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );
        pMenu->on_visible_changed( msg, opt, b );
    }
}

void RServMenu::on_content_changed( int /*msg*/ )
{
}

void RServMenu::on_context_changed( int msg )
{
    foreach( RMenu *pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );
        pMenu->on_context_changed( msg );
    }
}

void RServMenu::on_lang_changed()
{
    RMenu* pMenu;

    foreach( pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );
        pMenu->reTranslate();
    }
}

void RServMenu::on_active_changed( int /*act*/ )
{

}

void RServMenu::on_active_in( int msg )
{
    RMenu *pMenu;
    int linkMsg = -1;

    //! check service widget on
    QMap<int, QWidget*>::const_iterator iter = RServMenu::_mServWidgets.constBegin();
    while (iter != RServMenu::_mServWidgets.constEnd())
    {
        if ( iter.value() != NULL && iter.value()->isVisible() )
        {
            linkMsg = iter.key();
            break;
        }

        iter++;
    }

    //! service widget on
    if ( linkMsg != -1 )
    {
        //! \todo the corrent page
        pMenu = findMenuByMsg( linkMsg );
        if ( pMenu != NULL )
        {
            Q_ASSERT( mListMenu.size() > 0 );
            pMenu  = mListMenu[0];
            Q_ASSERT( pMenu != NULL );
        }
        else
        { return; }
    }
    else
    {
        pMenu = findMenuByMsg( msg );
        if ( NULL == pMenu )
        {
            //! use the last menu
            if ( NULL != m_pLastMenu
                 && CMD_SERVICE_DEACTIVE == msg )
            {
                pMenu = m_pLastMenu;
            }
            else
            {
                Q_ASSERT( mListMenu.size() > 0 );
                pMenu  = mListMenu[0];
                Q_ASSERT( pMenu != NULL );
            }
        }
        else
        {
            //! intent used
            pMenu->pushStack();

            serviceExecutor::post( m_pServMenu->getServName(),
                                   CMD_SERVICE_SUB_ENTER,
                                   msg );
        }

    }

    pMenu = pMenu->expandLeaf();
    Q_ASSERT( pMenu != NULL );

    pMenu->setActive();
}

void RServMenu::on_active_out( int /*msg*/ )
{
    m_pLastMenu = RMenu::_activeMenu;
//LOG_DBG()<<QString::number( (quint32)m_pLastMenu, 16 );
    m_pLastMenu->popStack();
}

void RServMenu::on_focus_changed( int msg )
{
    Q_ASSERT( RMenu::_activeMenu!= NULL );

    RMenu::_activeMenu->on_focus_changed( msg );
}

}
