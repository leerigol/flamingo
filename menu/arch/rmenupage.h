#ifndef RMENUPAGE_H
#define RMENUPAGE_H

#include <QtWidgets>
#include "../menustyle/rmenupage_style.h"
#include "menutab.h"
#include "../eventfiltermgr/reventfiltermgr.h"

namespace menu_res
{

class RMenuLayout;
class RMenu;
class RServMenu;

/*!
 * \brief The RMenuPage class
 * -菜单页是一个窗口部件
 * -菜单页中含有菜单项控件
 * -菜单项的数量和ui设计有关
 */
class RMenuPage : public RMenuTab
{
    Q_OBJECT

protected:
    static RMenuPage_Style _style;  /*!< 菜单页风格资源*/
    static QWidget *m_pTuneWidget;  /*!< 旋钮控件*/
    static IEventFilter *m_pEventFilter;
    static QTimer _playTimer;
    static int _playNow;
    const static int _playPeriod = 5;
    const static int _playInterval = 50;   //! ms
public:
    static void onMsg( UiMsg msg );

    static void setTuneWidget( QWidget *pWidget );
    static QWidget *getTuneWidget();
    static bool hasTuneFocus( QWidget *pWidget );
    static void setEventFilter( IEventFilter *pFiler );
    static bool isTop();
    static void toTop();

public:
    RMenuPage( QWidget *parent ); 

private:
    void init();

public:
    int findId( QWidget *pWidget );
    QWidget *findView( int msg );
    void postKey( int wigId, int key, int keyCnt, QEvent::Type type );

    void attachWidget( QWidget *pWig );
    void closeAttachWidget();

    void playActive();

protected:
    virtual void paintEvent(QPaintEvent * event);
    virtual void showEvent(QShowEvent * event);
    virtual void hideEvent(QHideEvent *event);

public:
    void update();
    void toggleExpand();

    void setStatus(ElementStatus stat);
protected:
    void decideTune();

public Q_SLOTS:
    void show();
    void on_playActiveTimeout();

protected Q_SLOTS:
    void on_sig_active_changed( int total, int now );
    void on_sig_active_pressed( int total, int now );
public:
    void onActive( int intoAct = -1 );
    void onDeActive();

    void setParent( RMenu *pParent );

    void addWidget( QWidget *pwidget );
    QList< QWidget* > &getWidgetList();

    void crossRef( RServMenu *pServMenu );

    RMenu *getOptionMenu( int msg, QList<int> &rootedList );
    bool containsMsg( int msg );

    void collectOptionMenuMsg( QList<int> &optionMenu );

    void getTitle( QList<int> &titleIds );

    void reTranslate();

protected:
    void tuneLedShow( bool bShow );

public:
    void on_value_changed( int msg );

    void on_enable_changed( int msg, bool b );
    void on_visible_changed( int msg, bool b );

    void on_enable_changed( int msg, int opt, bool b );
    void on_visible_changed( int msg, int opt, bool b );

    void on_focus_changed( int msg );

    void on_context_changed( int msg );

protected:
    QList< QWidget* > mWidgetList;

    RMenu *mParent;     /*!< parent menu*/
                        //! 附加在页面上的控件
    QList< QWidget* > mAttachedWidgets;

    bool mbExpand;
};

}


#endif // RMENUPAGE_H
