#ifndef RMENU_H
#define RMENU_H

#include <QtCore>
#include <QEvent>

namespace menu_res
{

class RServMenu;
class RMenuPage;
class CResMenu;

/*!
 * \brief The RMenu class
 *
 * -逻辑菜单
 * -逻辑菜单指的是具有相同菜单头的菜单，例如桌面系统中的“文件”菜单
 * -逻辑菜单只是一个数据结构，其中包含了多个菜单页
 * -菜单页是根据能够显示的数量确定的
 * -一次只能显示一个菜单页，菜单页中可以翻动
 * -对于不要求菜单分页的程序，需要在生成时满足一个菜单只有一个菜单页
 */
class RMenu
{
public:
    static RMenu *_activeMenu;  /*!< 当前活动菜单 */
    static int  findId( QWidget *pWidget );

public:
    RMenu();
    RMenu( RServMenu *pServ );

private:
    void init();

public:
    void postKey( int wigId, int key, int cnt, QEvent::Type type  );
    void postKey( int key, int cnt , QEvent::Type type );

public:
    void setParent( RMenu *parent );
    RMenu *getParent() const;

    void setRoll( bool b );
    bool getRoll();

    void pushStack();
    void popStack();

    void setActive();
    bool getActive();

    void addPage( RMenuPage *pPage );

    void cfgFontColor( const QColor &focusIn,
                       const QColor &focusOut );
public:
    void moveNext();
    void movePrevious();
    void moveToParent();
    void moveToChild( RMenu *pMenuChild );

    void moveToSibling( RMenu *pMenu );

    RMenu *cascadeLeaf( int optMsg, QList<int> &msgList );
    RMenu *expandLeaf();
    bool containsMsg( int msg );

    void collectOptionMenuMsg( QList<int> &optMenuMsg );

    void active( int intoAct = -1 );
    void deActive();

    void reTranslate();

public:
    void setName( const QString &str );
    QString & getName() ;

    void setServMenu( RServMenu *pServMenu );
    RServMenu *getServMenu();

    void setResMenu( CResMenu *pResMenu );
    CResMenu *getResMenu();

    void setTitle( int titleId );
    void replaceTitle( int srcTitle, int dstTitle );

    //! title by id
    void getTitle( QList<int> &idList );

    QWidget * findView( int msg );

    void crossRef( RServMenu *pServMenu );

public:
    void on_value_changed( int msg );

    void on_enable_changed( int msg, bool b );
    void on_visible_changed( int msg, bool b );

    void on_enable_changed( int msg, int opt, bool b );
    void on_visible_changed( int msg, int opt, bool b );

    void on_context_changed( int msg );
    void on_focus_changed( int msg );

    void update();

private:
    bool mRoll;             /*!< can loop back*/
    bool mbInTask;
    int mActivePage;

    RMenu *m_pParent;       /*!< parent menu */
    QList<RMenuPage*> mPageList;

    QString mName;          /*!< menu name for cross ref */

    RServMenu *m_pServMenu; /*!< to service, the sibling is in service menu*/

    CResMenu *m_pResMenu;   /*!< link to resource menu */

friend class RMenuPage;
friend class REventFilter;
};


}

#endif // RMENU_H
