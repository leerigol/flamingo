#ifndef MENU_RES_H
#define MENU_RES_H

#include <QtCore>

#include "../../com/uiattr/uiattr.h"
#include "../../com/uiattr/uicontrol.h"
#include "../rmenu_cfg.h"

namespace menu_res {


/*!
 * \brief The eMenuRes enum
 * Menu Resource
 * the resource item is:
 * - title
 * - option
 * the resource content is:
 * - string
 * - icon
 * - string | icon
 */
enum eMenuRes
{
    MR_Title_Option_Mask = 0x00ff,
    MR_Title = 2,   /*!< menu title */
    MR_Option = 1,  /*!< menu option */

    MR_Str_Icon_Mask = 0xff00,
    MR_Str = 0x100,  /*!< content is string */
    MR_Icon = 0x200, /*!< content is icon(picture) */

    MR_StrIcon = 0x300, /*!< string | icon */
};

enum eResAttr
{
    ResAttr_Disable = 1,
    ResAttr_Hide = 2,

    ResAttr_Control_Mask = 0xff,
};

/*!
 * \brief The eComboxSubAttr enum
 * -combox可以含有两个属性
 * -改变选择项目：在a/b/c/d中进行选择
 * -改变项目的值: 设置选中的a/b/c/d数值
 */
enum eComboxSubAttr
{
    combox_raw = 0,
    combox_value = 1,
};
/*!
 * \brief The eControlPara enum
 * 控件参数类型：
 * -键，控件不处理键值，键值交由功能自己处理。对应了两个消息，原生消息是键消息。
 * -数值
 */
enum eControlPara
{
    para_key,
    para_val,
};

/*!
 * \brief The eControlFmt enum
 * 控件显示的数值格式
 * -整数型
 * -小数型
 */
enum eControlFmt
{
    control_fmt_fract,
    control_fmt_integer,
};

enum eControlKnob
{
    control_knob_tune_pad,
    control_knob_tune_z,
    control_knob_tune,
    control_knob_pad,   //! no tune
};

class CResMenu;
class CServMenu;
class CResItem;

/*!
 * \brief The CResMenuItem class
 * resource menu item
 */
class CResMenuItem
{
public:
    CResMenuItem();
    ~CResMenuItem();

public:
    int serialIn( QDataStream &stream );
    eMenuControl getType() const;

    void setValue( const int val );
    void setValue( const qint64 val );
    void setValue( const QString &str );

    void getValue( int &val );
    void getValue( qint64 &val );
    void getValue( QString &val );

    QString getTitle();
    QString getOptionStr( int option = 0 );

    QString getOptionStrNow();

    eMenuRes getOptionType( int value );
    int getOptionImage( int value, QImage &img );

public:
    int mId;                /*!< msgid && res id */
    eMenuControl m_eType;   /*!< control type */
    void *m_pChild;         /*!< child menu */

    union
    {
        int    mIntVal;     /*!< menu value */
        qint64 mInt64Val;   /*!< int64 value */
    };
    QString mStrVal;        /*!< string val */

public:
    CResItem *m_pResItem;   /*!< point to res*/

public:
    int mChild;             /*!< for cross ref */
    int mRootChildMsg;      /*!< for menu options */

    int mPara1, mPara2;

friend class CServMenu;     /*!< for child ref */
};

/*!
 * \brief The CResMenu class
 * contains a few menu items
 */
class CResMenu
{
public:
    CResMenu();
    ~CResMenu();
public:
    int serialIn( QDataStream &);
    QList< CResMenuItem *> & getItems();
    QString &getName();

protected:
    QString mName;          /*!< menu name */
    int mId;                /*!< menu id*/

    int mItemCnt;           /*!< menu item count */
                            /*!< items */
    QList< CResMenuItem *> mItems;

public:
    CResItem *m_pResItem;   /*!< point to res*/

protected:

friend class CServMenu;     /*!< for child ref */
friend class CResMenuItem;

};

class CServRes;
/*!
 * \brief The CServMenu class
 * service resource menu
 * contains a few resource menus
 */
class CServMenu
{
public:
    CServMenu();
    ~CServMenu();
public:
    int serialIn( QDataStream &);
    void linkResource( CServRes &servRes );
    void replace( int srcId, int dstId );

    QList< CResMenu *> & getMenus();
    QString &getServName();

protected:
    static CResItem* findResItem( int id, CServRes &servRes );

    CResMenu *findRefMenu( int id );

protected:
    QString mServName;              /*!< service name*/
    QList< CResMenu *> mMenus;      /*!< resource menus */

public:
    QList< QString *> mRefMenus;    /*!< ref menu */
};

class CServRes;
class CResData;

/*!
 * \brief The CResItem class
 * resource item
 */
class CResItem
{
public:
    CResItem();
    ~CResItem();
public:
    int serialIn( QDataStream &stream );
    void initResAttr( ui_attr::uiAttr *pAttr );
    bool getColor( QColor &fgColor );

protected:
    int mId;                    /*!< msg id */
    eMenuRes mType;             /*!< resource type: title string or title icon? */
    quint32 mResAttrMask;       /*!< 资源属性掩码*/
public:
    QList<int> mStrAddr;        /*!< string addr */
    QList<int> mIconAddr;       /*!< icons addr: 3 */

    int value;                  /*!< option value */
    quint32    mColor;          /*!< item color */
    int mOptionMenuId;          /*!< ref menuId */
    void *m_pOptionMenu;        /*!< to option menu*/

    QList<CResItem*> mOption;   /*!< the resoure is menu title, so it has a few options*/

public:
    void setId( int id );
    int getId();

    bool isTitle();

    bool isStr();
    bool isIcon();

    QString getContentString();
    QString getTitleString();

    eMenuRes getType();

    QString getString();
    int     getImage( QImage &image );

    QString getOptionString( int value );
    int     getOptionImpage( int value, QImage &image );

    bool    getOptionColor( int value, QColor &fgColor );

    eMenuRes getOptionType( int value );

friend class CServRes;
friend class CServMenu;
};

/*!
 * \brief The CServRes class
 * contain the resource items
 */
class CServRes
{
public:
    CServRes();
    ~CServRes();
public:
    int serialIn( QDataStream &stream );
    void initResAttr( ui_attr::uiAttr *pAttr );           /*!< 初始化ui属性*/
public:
    QList<CResItem*> mResTree;  /*!< resoruce items */
    QList<QString *> mRefMenus; /*!< resource cross ref table */
friend class CServMenu;
};

/*!
 * \brief The vDesc struct
 * file description
 */
struct vDesc
{

    int mOffset;        /*!< offset */
    int mLength;        /*!< length */

    vDesc();
    void serialIn( QDataStream &stream );
};

/*!
 * \brief The CResDesc class
 */
class CResDesc
{
public:
    static QList< CResDesc * > _descList;
    static CResDesc *getDesc( const QString &rsrcName );
    static int loadDesc( const QString &file );
    static void unloadDesc();

public:
    CResDesc();

public:
    void serialIn( QDataStream &stream );

public:
    QString mName;      /*!< resource name */

    vDesc mDescMenu;    /*!< 菜单结构描述*/
    vDesc mDescRes;     /*!< 资源描述*/
    vDesc mDescDat[ LANG_COUNT ];   /*!< 各语言数据描述*/
    vDesc mDescPic;     /*!< 图片描述*/
};

/*!
 * \brief The CResData class
 * resource raw data
 */
class CResData
{

public:
    static QList< QString > _mNameStringList;       /*! file name list */
    static QList< QFile *> _mFileList;              /*! file list   */
    static QList< QDataStream *> _mStreamList;      /*! stream list */

    static QString _mPicString;                     /*! for picture */
    static QFile * _mPicFile;
    static QDataStream * _mPicStream;
    static QMutex _mDataMutex;

private:
    static QDataStream *_m_pActiveStream;           /*! active data stream for language*/
    static int    _mIdActLang;

public:

    static void *getData( int addr, int &bytes );

    //! 通过地址读取string
    static QString getString( int addr );
    static QString getString( QList<int> & addrList );
    static QString getString( int addr, QDataStream &file );

    static int getImage( int addr, QImage &img );

    static void addFile( const QString &file );
    static void setImageFile( const QString &file );

    static int setLanguageSeq( int langSeq );
    static int  getLanguageSeq();

    static void open();
    static void close();

private:
    static void* getData( int addr, QDataStream &file, int &bytes );
    static void selLanguage( int seq );

};

/*!
 * \brief The CStringReader class
 * - get string from data stream
 * - the string is unicode
 * - the string may contain pad in the end
 * - the format: length + string + [pad]
 */
class CStringReader
{
public:
    static QString getString( QDataStream &file );
    static int getPad( int count );
};

struct cacheItem
{
    int offset;
    int length;
    char *pBuf;
};

/*!
 * \brief The CCache class
 * memory cache to reduce picese
 */
class CCache
{
private:
    static char *mBuf;
    static int mSize;

    static QList< cacheItem > _cacheResList;

public:
    static char * getBuffer( int n );
    static void open();
    static void close();

    static void cacheIn( int offset, int length, char *buf );
    static char *getCache( int offset, int length );
};

QString toString( eMenuControl control );
QString toString( eMenuRes control );

}

#endif // MENUDEMO_H
