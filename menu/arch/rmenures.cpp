#include <QImage>

#include "rmenures.h"
#include "../include/dsodbg.h"
namespace  menu_res {

#define res_attr_match( attr, mask )    \
                        ( ((attr)&(quint32)(mask))==(quint32)(mask) )

CResMenuItem::CResMenuItem()
{

    mId = 0;
    m_eType = MC_Invalid;

    m_pResItem = NULL;

    m_pChild = NULL;
    mChild = -1;

    mIntVal = 0;
    mInt64Val = 0;
}

//! \todo 不删除resItem吗
CResMenuItem::~CResMenuItem()
{}

int CResMenuItem::serialIn( QDataStream &stream )
{
    int type;

    stream>>mId;
    stream>>type; m_eType = (eMenuControl)type;
    stream>>mChild;
    stream>>mRootChildMsg;

    stream>>mPara1;
    stream>>mPara2;

    return 0;
}

eMenuControl CResMenuItem::getType() const
{
    return m_eType;
}

void CResMenuItem::setValue( const int val )
{
    mIntVal = val;
}

void CResMenuItem::setValue( const qint64 val )
{
    mInt64Val = val;
}

void CResMenuItem::setValue( const QString &val )
{
    mStrVal = val;
}

void CResMenuItem::getValue( int &val )
{
    val = mIntVal;
}
void CResMenuItem::getValue( qint64 &val )
{
    val = mInt64Val;
}
void CResMenuItem::getValue( QString &val )
{
    val = mStrVal;
}

QString CResMenuItem::getTitle()
{
    return m_pResItem->getTitleString();
}
QString CResMenuItem::getOptionStr( int option )
{
    return m_pResItem->getOptionString( option );
}

QString CResMenuItem::getOptionStrNow()
{
    int val;

    getValue( val );

    return getOptionStr( val );
}

eMenuRes CResMenuItem::getOptionType( int value )
{
    return m_pResItem->getOptionType( value );
}

int CResMenuItem::getOptionImage( int value, QImage &img )
{
    return m_pResItem->getOptionImpage( value, img );
}

CResMenu::CResMenu()
{
    mId = -1;
    mItemCnt = 0;

    m_pResItem = NULL;
}

CResMenu::~CResMenu()
{
    CResMenuItem *pItem;

    foreach( pItem, mItems )
    {
        Q_ASSERT(pItem!=NULL);
        delete pItem;
    }
}

int CResMenu::serialIn(QDataStream & stream )
{
    int i;

    //! get the menu name
    mName = CStringReader::getString(stream);

    stream>>mId;
    stream>>mItemCnt;

#ifdef HAS_PAD
    stream.skipRawData( 2 * 4 );
#endif

    CResMenuItem *pItem;
    for ( i = 0; i < mItemCnt; i++ )
    {
        pItem = new CResMenuItem();

        pItem->serialIn(stream);

        mItems.append( pItem );
    }

    return 0;
}

QList< CResMenuItem *> & CResMenu::getItems()
{
    return mItems;
}

QString &CResMenu::getName()
{
    return mName;
}

CServMenu::CServMenu()
{
    mMenus.clear();
    mRefMenus.clear();
}
CServMenu::~CServMenu()
{
    CResMenu *pMenu;
    QString *pStr;

    //! delete each menu
    foreach( pMenu, mMenus )
    {
        Q_ASSERT( pMenu != NULL );

        delete pMenu;
    }

    //! delete ref menu string
    foreach( pStr, mRefMenus )
    {
        Q_ASSERT( pStr != NULL );
        delete pStr;
    }
}
int CServMenu::serialIn( QDataStream &stream)
{
    CResMenu *menu;
    int cnt;
    int id;
    QString *pStr;

    mServName = CStringReader::getString( stream );

    //! cross ref cnt
    stream>>cnt;
#ifdef HAS_PAD
    stream.skipRawData( 12 );
#endif
    for ( int i = 0; i < cnt; i++ )
    {
        stream>>id;
#ifdef HAS_PAD
    stream.skipRawData( 12 );
#endif
        pStr = new QString();

        *pStr = CStringReader::getString( stream );

        mRefMenus.append( pStr );
    }

    while( !stream.atEnd() )
    {
        menu = new CResMenu();

        menu->serialIn( stream );

        mMenus.append( menu );
    }

    return 0;
}

void CServMenu::linkResource( CServRes &servRes )
{
    CResMenuItem *pItem;

    CResItem *pResItem;

    foreach( CResMenu *pMenuA, mMenus )
    {
        //! title resource
        pResItem = findResItem( pMenuA->mId, servRes );
        Q_ASSERT( pResItem !=NULL );
        pMenuA->m_pResItem = pResItem;

        //! item
        foreach ( pItem, pMenuA->mItems )
        {
            pResItem = findResItem( pItem->mId, servRes );
            if(pResItem == NULL)
            {
                qDebug() << "msg=" << pItem->mId;
                Q_ASSERT( pResItem != NULL );
            }
            pItem->m_pResItem = pResItem;
        }
    }
}

void CServMenu::replace( int srcId, int dstId )
{
    foreach( CResMenu *pMenuA, mMenus )
    {
        Q_ASSERT( NULL != pMenuA );
        if ( pMenuA->mId == srcId )
        {
            pMenuA->mId = dstId;
        }
    }
}

QList< CResMenu *> & CServMenu::getMenus()
{
    return mMenus;
}

QString &CServMenu::getServName()
{
    return mServName;
}

CResItem* CServMenu::findResItem( int id, CServRes &servRes )
{
    CResItem *pItem;

    foreach( pItem, servRes.mResTree )
    {
        if ( pItem->mId == id )
        {
            return pItem;
        }
    }

    return NULL;
}

CResMenu *CServMenu::findRefMenu( int id )
{
    QString *pStr;
    if ( id < 0 || id >= mRefMenus.count() )
    {
        return NULL;
    }

    //! get str
    pStr = mRefMenus[id];

    CResMenu *pMenu;
    foreach( pMenu, mMenus )
    {
        if ( pMenu->mName.compare( *pStr, Qt::CaseInsensitive) == 0 )
        {
            return pMenu;
        }
    }

    return NULL;
}

CResItem::CResItem()
{
    mOption.clear();

    m_pOptionMenu = NULL;
}

CResItem::~CResItem()
{
    CResItem *pItem;

    //! for sub item
    foreach( pItem, mOption )
    {
        Q_ASSERT( pItem );
        delete pItem;
    }
}

int CResItem::serialIn( QDataStream &stream )
{
    int val;
    int i;

    stream>>mId;
    stream>>val;
    mType = (eMenuRes)val;

    for ( i = 0; i < LANG_COUNT; i++ )
    {
        stream>>val;
        mStrAddr.append(val);
    }

    stream>>value;
    stream>>mOptionMenuId;
    stream>>mResAttrMask;
    stream>>mColor;
    for ( i = 0; i < ICON_COUNT; i++ )
    {
        stream>>val;
        mIconAddr.append( val );
    }

    return 0;
}

void CResItem::initResAttr( ui_attr::uiAttr *pAttr )
{
    Q_ASSERT( pAttr != NULL );

    quint32 attr;

    //! for title
    attr = mResAttrMask;
    if ( attr != 0 )
    {
        if ( is_attr( attr, ResAttr_Disable) )
        {
            pAttr->getItem( mId )->setEnable( false );
        }

        if ( is_attr( attr, ResAttr_Hide) )
        {
            pAttr->getItem( mId )->setVisible( false );
        }

        //! attrs : controls
        if ( ( (attr >> 8 ) & ResAttr_Control_Mask ) != 0 )
        {
            pAttr->getItem( mId )->setAttr( (attr>>8)&ResAttr_Control_Mask );
        }
    }

    //! now for options
    if ( isTitle() )
    {
        CResItem *pItem;
        foreach( pItem, mOption )
        {
            Q_ASSERT( NULL != pItem );

            attr = pItem->mResAttrMask;

            if ( attr != 0 )
            {
                if ( is_attr( attr, ResAttr_Disable) )
                {
                    pAttr->getOption( mId, pItem->value )->setEnable(false);
                }

                if ( is_attr( attr, ResAttr_Hide) )
                {
                    pAttr->getOption( mId, pItem->value )->setVisible(false);
                }
            }
        }
    }
}

bool CResItem::getColor( QColor &fgColor )
{
    if ( mColor == 0 )
    { return false; }
    else
    {
        fgColor.setRgb( mColor );
        return true;
    }
}

void CResItem::setId( int id )
{
    mId = id;
}

int CResItem::getId()
{
    return mId;
}

bool CResItem::isTitle()
{
    return ( mType & MR_Title) == MR_Title;
}

bool CResItem::isStr()
{
    return ( mType & MR_Str) == MR_Str;
}

bool CResItem::isIcon()
{
    return ( mType & MR_Icon) == MR_Icon;
}

QString CResItem::getContentString()
{
    return CResData::getString( mStrAddr );
}

QString CResItem::getTitleString()
{
    return CResData::getString( mStrAddr );
}

eMenuRes CResItem::getType()
{
    return mType;
}

QString CResItem::getString()
{
    return CResData::getString( mStrAddr );
}
int CResItem::getImage( QImage &image )
{
    return CResData::getImage( mIconAddr[0], image );
}

QString CResItem::getOptionString( int value )
{
    if ( mOption.size() < 1 )
    {
        qWarning()<<"no option use title instead"<<mId;
        return getTitleString();
    }

    CResItem *pOptionRes;
    foreach( pOptionRes, mOption )
    {
        Q_ASSERT( pOptionRes!=NULL );

        if ( pOptionRes->value == value )
        {
            return pOptionRes->getContentString();
        }
    }

    return QLatin1Literal("no option");
}

int CResItem::getOptionImpage( int value, QImage &image )
{
    if ( mOption.size() < 1 )
    {
        qWarning()<<"No image";
        return -1;
    }

    CResItem *pOptionRes;
    foreach( pOptionRes, mOption )
    {
        Q_ASSERT( pOptionRes!=NULL );

        if ( pOptionRes->value == value )
        {
            return CResData::getImage( pOptionRes->mIconAddr[0], image );
        }
    }

    return -1;
}

bool CResItem::getOptionColor( int value, QColor &fgColor )
{
    if ( mOption.size() < 1 )
    { return false; }

    CResItem *pOptionRes;
    foreach( pOptionRes, mOption )
    {
        Q_ASSERT( pOptionRes!=NULL );

        if ( pOptionRes->value == value )
        {
            return pOptionRes->getColor( fgColor );
        }
    }

    return false;
}

eMenuRes CResItem::getOptionType( int value )
{
    if ( mOption.size() < 1 )
    {
        qWarning()<<"no option use title instead"<<mId;
        return MR_Option;
    }

    CResItem *pOptionRes;
    foreach( pOptionRes, mOption )
    {
        Q_ASSERT( pOptionRes!=NULL );

        if ( pOptionRes->value == value )
        {
            return pOptionRes->mType;
        }
    }

    return MR_Option;
}

CServRes::CServRes()
{
    mResTree.clear();
}

/*!
 * \brief CServRes::~CServRes
 * the res items and menu string are been attached to servmenu
 */
CServRes::~CServRes()
{
    CResItem *pItem;
    QString *pStr;

    foreach( pItem, mResTree )
    {
        Q_ASSERT(pItem!=NULL);
        delete pItem;
    }

    foreach( pStr, mRefMenus )
    {
        Q_ASSERT( pStr !=NULL );
        delete pStr;
    }
}

int CServRes::serialIn(QDataStream &stream)
{
    CResItem *pItem;
    CResItem *pTitleItem;

    int count;
    int i;
    int id;

    //! read string
    stream>>count;
#ifdef HAS_PAD
    stream.skipRawData( 12 );
#endif
    QString *pStr;
    for ( i = 0; i < count; i++ )
    {

        stream>>id;
#ifdef HAS_PAD
    stream.skipRawData( 12 );
#endif

        pStr = new QString();
        *pStr = CStringReader::getString( stream );

        mRefMenus.append( pStr );
    }

    while( !stream.atEnd() )
    {
        //! a new one
        pItem = new CResItem();

        //! serial in
        pItem->serialIn( stream );

        //! new title
        if ( pItem->isTitle() )
        {
            mResTree.append(pItem);
            pTitleItem = pItem;
        }
        else
        {
            Q_ASSERT( pTitleItem != NULL );
            pTitleItem->mOption.append( pItem );
        }
    }

    return 0;
}

void CServRes::initResAttr( ui_attr::uiAttr *pAttr )
{
    Q_ASSERT( pAttr != NULL );

    CResItem *pItem;
    foreach( pItem, mResTree )
    {
        Q_ASSERT( NULL != pItem );

        pItem->initResAttr( pAttr );
    }
}

vDesc::vDesc()
{}

void vDesc::serialIn( QDataStream &stream )
{
    stream>>mOffset;
    stream>>mLength;
}

QList< CResDesc *> CResDesc::_descList;
CResDesc *CResDesc::getDesc( const QString &rsrcName )
{
    CResDesc *pDesc;

    foreach( pDesc, _descList )
    {
        Q_ASSERT( pDesc != NULL );
        if ( pDesc->mName.compare( rsrcName, Qt::CaseInsensitive ) == 0 )
        {
            return pDesc;
        }
    }

    return NULL;
}

int CResDesc::loadDesc( const QString &fileName )
{
    QFile file( fileName );
    bool bOpen;

    bOpen =  file.open( QIODevice::ReadOnly );
    Q_ASSERT( bOpen );

    QDataStream stream( &file );
    stream.setByteOrder( QDataStream::LittleEndian );

    unloadDesc();

    CResDesc *pDesc;
    while( !stream.atEnd() )
    {
        pDesc = new CResDesc();

        pDesc->serialIn( stream );

        _descList.append( pDesc );
    }

    file.close();

    return 0;
}

void CResDesc::unloadDesc()
{
    CResDesc *pDesc;

    foreach( pDesc, _descList )
    {
        Q_ASSERT( pDesc != NULL );
        delete pDesc;
    }
}

CResDesc::CResDesc()
{}

void CResDesc::serialIn( QDataStream &stream )
{
    int i;
    QString str;

    str = CStringReader::getString(stream);

    mName = str;
    mDescMenu.serialIn(stream);
    mDescRes.serialIn(stream);
    for ( i = 0; i < LANG_COUNT; i++ )
    {
        mDescDat[i].serialIn( stream );
    }

    mDescPic.serialIn( stream );
}

QList< QString > CResData::_mNameStringList;       /*! file name list */
QList< QFile *> CResData::_mFileList;              /*! file list   */
QList< QDataStream *> CResData::_mStreamList;      /*! stream list */

QString CResData::_mPicString;                     /*! for picture */
QFile * CResData::_mPicFile = NULL;
QDataStream * CResData::_mPicStream = NULL;
QMutex CResData::_mDataMutex;

QDataStream * CResData:: _m_pActiveStream;
int CResData::_mIdActLang;

void *CResData::getData( int addr, int &bytes )
{
    Q_ASSERT( _m_pActiveStream != NULL );
    return getData( addr, *_m_pActiveStream, bytes );
}
QString CResData::getString( int addr )
{
    Q_ASSERT( _m_pActiveStream != NULL );
    return getString( addr, *_m_pActiveStream );
}

QString CResData::getString( QList<int> &addrList )
{
    Q_ASSERT( _mIdActLang >= 0 );
    Q_ASSERT( _mIdActLang < addrList.count() );

    return getString( addrList[_mIdActLang] );
}

QString CResData::getString( int addr, QDataStream &file )
{
    void *pBuf;
    int bytes;

    //! get data
    pBuf = getData( addr, file, bytes );

    //! unicode
    QString str( (QChar*)pBuf, bytes / 2 );

    return str;
}

int CResData::getImage( int addr, QImage &img )
{
    //! open file
    if ( CResData::_mPicFile == NULL )
    {
        CResData::_mPicFile = new QFile( _mPicString );
        _mPicFile->open( QIODevice::ReadOnly );

        CResData::_mPicStream = new QDataStream( _mPicFile );
        _mPicStream->setByteOrder( QDataStream::LittleEndian );
    }

    CResData::_mDataMutex.lock();

    int length;
    _mPicStream->device()->seek( addr );
    //! read length
    *_mPicStream>>length;

    //! why 54: see bmp24 format = bitmapfileheader + bitmapinfoheader
    if ( length < 54 )
    {
        CResData::_mDataMutex.unlock();
        LOG_DBG()<<length;
        return -1;
    }

    char *buf;
    buf = CCache::getBuffer( length );
    if ( NULL == buf )
    {
        CResData::_mDataMutex.unlock();
        LOG_DBG()<<length;
        return -1;
    }

    if ( length != _mPicStream->readRawData( buf, length ) )
    {
        CResData::_mDataMutex.unlock();
        LOG_DBG()<<length;
        return -1;
    }

    if ( img.loadFromData( (uchar*)buf, length, "BMP" ) )
    {
        CResData::_mDataMutex.unlock();
        return 0;
    }

    CResData::_mDataMutex.unlock();

    return -1;
}

/*!
 * \brief CResData::addFile
 * \param file
 * the add sequence is the language Sequence in setLanguageSeq
 * \sa setLanguageSeq
 */
void CResData::addFile( const QString &file )
{
    CResData::_mNameStringList.append( file );
}

void CResData::setImageFile( const QString &file )
{
    CResData::_mPicString = file;
}

/*!
 * \brief CResData::setLanguageSeq
 * \param langSeq
 * \return  0 -- set lang success
 *
 */
int CResData::setLanguageSeq( int langSeq )
{
    bool bOpen;

    //! invalid input
    if ( langSeq < 0 || langSeq >= _mNameStringList.count() )
    {
        Q_ASSERT( false );
    }

    //! has been opened
    if ( _mFileList[ langSeq ] != NULL )
    {
        selLanguage( langSeq );
        return 0;
    }

    QFile *pFile;
    QDataStream *pStream;

    //! file
    pFile = new QFile( _mNameStringList[langSeq] );
    if ( pFile == NULL )
    {
        return -1;
    }

    bOpen = pFile->open(QIODevice::ReadOnly );
    if ( !bOpen )
    {
        delete pFile;
        return -2;
    }

    pStream = new QDataStream( pFile );
    if ( NULL == pStream )
    {
        pFile->close();
        delete pFile;
        return -2;
    }
    pStream->setByteOrder( QDataStream::LittleEndian );

    //! add to list
    _mFileList[ langSeq ] = pFile;
    _mStreamList[ langSeq ] = pStream;

    //! sel lang
    selLanguage( langSeq );

    return 0;
}

int  CResData::getLanguageSeq()
{
    return _mIdActLang;
}

void CResData::open()
{
    _m_pActiveStream = NULL;
    _mIdActLang = -1;

    int i;
    for ( i = 0; i < LANG_COUNT; i++ )
    {
        _mFileList.append( NULL );
        _mStreamList.append( NULL );
    }

    _mPicFile = NULL;
    _mPicStream = NULL;
}

void CResData::close()
{
    QDataStream *pStream;
    foreach( pStream, _mStreamList )
    {
        if ( pStream != NULL )
        {
            delete pStream;
        }
    }

    QFile *pFile;
    foreach( pFile, _mFileList )
    {
        if ( pFile != NULL && pFile->isOpen() )
        {
            pFile->close();
            delete pFile;
        }
    }

    if ( _mPicFile != NULL && _mPicFile->isOpen() )
    {
        _mPicFile->close();

        delete _mPicFile;
        delete _mPicStream;
    }
}

//! get data
void* CResData::getData( int addr,
                         QDataStream &file,
                         int &bytes )
{
    char *buf;

    CResData::_mDataMutex.lock();

    //! seek to addr
    file.device()->seek( addr );

    file>>bytes;

    buf = CCache::getBuffer( bytes );
    file.readRawData( buf, bytes );

#ifdef HAS_PAD
    file.skipRawData( ((4+bytes+15)/16)*16 - 4 - bytes );
#endif

    CResData::_mDataMutex.unlock();

    return buf;
}

void CResData::selLanguage( int seq )
{
    Q_ASSERT( seq >= 0 );
    Q_ASSERT( seq < _mStreamList.count() );
    Q_ASSERT( _mStreamList[ seq ]!=NULL );

    _m_pActiveStream = _mStreamList[ seq ];
    _mIdActLang = seq;
}

QString CStringReader::getString( QDataStream &file )
{
    int count,pad;
    char *buf;

    CResData::_mDataMutex.lock();

    file>>count;

    buf = CCache::getBuffer( count );
    file.readRawData( buf, count );

    //! byte array
    QString str( (QChar*)buf, count / 2 );

#ifdef HAS_PAD
    pad = getPad( count );
    file.skipRawData( pad );
#endif

    CResData::_mDataMutex.unlock();

    //! convert to string
    return str;
}

int CStringReader::getPad( int count )
{
    int pad;

    pad = ( ( 4 + count + 15 )/16 ) * 16 - 4 - count;

    return pad;
}


char *CCache::mBuf = NULL;
int CCache::mSize = 0;
QList< cacheItem > CCache::_cacheResList;
char * CCache::getBuffer( int n )
{
    bool bEmpty;
    Q_ASSERT( n >= 0 );

    if( n == 0 )
    {
        qWarning()<<"empty string"<<QString::number(n,10);
        n = 10;
        bEmpty = true;
    }
    else
    {
        bEmpty = false;
    }

    //! need more
    if ( n > mSize )
    {
        //! delete pre buf
        if ( NULL != mBuf )
        {
            delete []mBuf;
        }

        //! renew
        mBuf = new char[ n ];
        Q_ASSERT( NULL != mBuf );
        mSize = n;
    }
    else
    {
    }

    if ( bEmpty )
    {
        strcpy( mBuf, "Empty");
    }

    return mBuf;
}

void CCache::open()
{}
void CCache::close()
{
    if ( NULL != mBuf )
    {
        delete []mBuf;
        mBuf = NULL;
        mSize = 0;
    }

    //! clean cache
    cacheItem item;
    foreach( item, _cacheResList )
    {
        delete []item.pBuf;
    }

    _cacheResList.clear();
}

void CCache::cacheIn( int offset, int length, char *buf )
{
    cacheItem cacheKey;

    cacheKey.offset = offset;
    cacheKey.length = length;
    cacheKey.pBuf = new char[length];

    memcpy( cacheKey.pBuf, buf, length );
    _cacheResList.append( cacheKey );
}
char *CCache::getCache( int offset, int length )
{
    //! find cache
    cacheItem item;
    foreach( item, _cacheResList )
    {
        if ( item.offset == offset && item.length == length )
        {
            return item.pBuf;
        }
    }

    //! clean cache
    if ( _cacheResList.size() >= 2 )
    {
        foreach( item, _cacheResList )
        {
            delete []item.pBuf;
        }

        _cacheResList.clear();
    }

    return NULL;
}

QString toString( eMenuControl control )
{
    switch( control )
    {
    case Menu_Title:
        return "Menu_Title";
    case Combox_Func:
        return "Combox_Func";
    case Button_Two:
        return "Button_Two";
    case Button_Sub:
        return "Button_Sub";
    case Label_Float:
        return "Label_Float";
    case Sub_Menu_Title:
        return "Sub_Menu_Title";
    case Combox:
        return "Combox";

    default:
        return "None";
    }
}
QString toString( eMenuRes control )
{
    QString str;
    switch (control & MR_Title_Option_Mask )
    {
    case MR_Title:
        str = "title";
        break;
    case MR_Option:
        str = "option";
        break;
    }

    switch (control & MR_Str_Icon_Mask )
    {
    case MR_Str:
        str += "/str";
        break;
    case MR_Icon:
        str += "/icon";
        break;

    case MR_StrIcon:
        str += "/StrIcon";
        break;
    }

    return str;
}

}
