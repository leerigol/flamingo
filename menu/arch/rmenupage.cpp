#include "rmenupage.h"

#include "rmenu.h"
#include "rmenupage.h"
#include "rservmenu.h"
#include "rmenures.h"

#include "../menuwidget/rcontrol.h"
#include "./menuwidget/rchildbutton.h"

#include "./wnd/rmenuwnd.h"
#include "../../com/keyevent/rvkey.h"
namespace menu_res {

RMenuPage_Style RMenuPage::_style;
QWidget *RMenuPage::m_pTuneWidget;
IEventFilter *RMenuPage::m_pEventFilter;
QTimer RMenuPage::_playTimer;
int RMenuPage::_playNow;

void RMenuPage::onMsg( UiMsg msg )
{
    do
    {
        //! pop info
        if ( msg == ui_page_collapse )
        {
            if ( RMenuPage::m_pTuneWidget == NULL )
            { break; }

            //! pop the widget
            RControl *pControl;
            pControl = dynamic_cast<RControl*>( RMenuPage::m_pTuneWidget );
            if ( NULL == pControl )
            { break; }

            //! pop info
            CArgument arg;
            arg.append( (int)key_Multi_Knob );
            arg.append( pControl->getResItem()->mId );
            serviceExecutor::post( E_SERVICE_ID_UI,
                                   MSG_GUI_NOTIFY_KEY_INFO,
                                   arg
                                   );
        }
        else
        {}

    }while(0);
}

void RMenuPage::setTuneWidget( QWidget *pWidget )
{
    RMenuPage::m_pTuneWidget = pWidget;

    Q_ASSERT( NULL != RMenuPage::m_pEventFilter );
    if ( pWidget == NULL )
    {
        RMenuPage::m_pEventFilter->detachKey( R_Pkey_FInc );
        RMenuPage::m_pEventFilter->detachKey( R_Pkey_FDec );
        RMenuPage::m_pEventFilter->detachKey( R_Pkey_FZ );
    }
    else
    {
        RMenuPage::m_pEventFilter->attachKey( R_Pkey_FInc, pWidget );
        RMenuPage::m_pEventFilter->attachKey( R_Pkey_FDec, pWidget );
        RMenuPage::m_pEventFilter->attachKey( R_Pkey_FZ, pWidget );
    }
}

QWidget *RMenuPage::getTuneWidget()
{
    return RMenuPage::m_pTuneWidget;
}

bool RMenuPage::hasTuneFocus( QWidget *pWidget )
{
    Q_ASSERT( NULL != pWidget );

    Q_ASSERT( NULL != RMenuPage::m_pEventFilter );
    Q_ASSERT( NULL != RMenuPage::m_pEventFilter->getMgr() );

    QWidget *pKeyWig;
    pKeyWig = RMenuPage::m_pEventFilter->getMgr()->findWidget( R_Pkey_FInc );
    if ( pKeyWig == pWidget )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void RMenuPage::setEventFilter( IEventFilter *pFilter )
{
    Q_ASSERT( NULL != pFilter );

    RMenuPage::m_pEventFilter = pFilter;
}

bool RMenuPage::isTop()
{
    Q_ASSERT( NULL != RMenuPage::m_pEventFilter );

    return RMenuPage::m_pEventFilter->isTop();
}

void RMenuPage::toTop()
{
    Q_ASSERT( NULL != RMenuPage::m_pEventFilter );

    return RMenuPage::m_pEventFilter->toTop();
}

RMenuPage::RMenuPage( QWidget *parent ) : RMenuTab( parent )
{
    init();

    _style.loadResource("ui/menu/page/");
    _style.resize( this );

    RMenuPage::m_pTuneWidget = NULL;

    hide();

    connect( this, SIGNAL(sig_active_changed(int,int)),
             this, SLOT(on_sig_active_changed(int,int)) );

    connect( this, SIGNAL(sig_active_pressed(int,int)),
             this, SLOT(on_sig_active_pressed(int,int)) );
}

void RMenuPage::init()
{
    mParent = NULL;
}

/*!
 * \brief RMenuPage::findId
 * \param pWidget
 * \return
 * 查找在widget中的序号
 */
int RMenuPage::findId( QWidget *pWidget )
{
    Q_ASSERT( NULL != pWidget );

    int i;
    QWidget *pWig;

    i = 0;
    foreach( pWig, mWidgetList )
    {
        Q_ASSERT( NULL != pWig );
        if ( pWig == pWidget )
        {
            return i;
        }

        i++;
    }

    return -1;
}

//! find the widget in the page
QWidget *RMenuPage::findView( int msg )
{
    QWidget *pWig;
    RMenuWidget *pMenuWidget;
    foreach( pWig, mWidgetList )
    {
        pMenuWidget = dynamic_cast<RMenuWidget*>(pWig);
        if ( NULL != pMenuWidget )
        {
            if ( pMenuWidget->m_pResMenuItem->mId == msg )
            { return pMenuWidget;}
        }
    }

    return NULL;
}

/*!
 * \brief RMenuPage::postKey
 * \param wigId
 * \param key
 * \param keyCnt
 * \param type
 * 页面某个控件消息
 */
void RMenuPage::postKey( int wigId,
                         int key, int keyCnt,
                         QEvent::Type type )
{
    Q_ASSERT( wigId >= 0 );

    //! to widget
    if ( wigId < mWidgetList.size() && mWidgetList[ wigId ] )
    {
        REventFilter::postkey( mWidgetList[wigId],
                               key,
                               keyCnt,
                               type );
    }
    //! invalid key
    else
    {
        LOG_DBG()<<"invalid key";
    }
}

void RMenuPage::attachWidget( QWidget *pWig )
{
    Q_ASSERT( NULL != pWig );

    if ( !mAttachedWidgets.contains( pWig ))
    {
        mAttachedWidgets.append( pWig );
    }
}

void RMenuPage::closeAttachWidget()
{
    QWidget* pWidget;
    foreach( pWidget, mAttachedWidgets )
    {
        Q_ASSERT( pWidget != NULL );
        pWidget->close();
    }
}

void RMenuPage::playActive()
{
    RMenuPage::_playTimer.disconnect();

    connect( &RMenuPage::_playTimer,
             SIGNAL(timeout()),
             this,
             SLOT(on_playActiveTimeout()) );

    RMenuPage::_playNow = 0;
    RMenuPage::_playTimer.setSingleShot( false );
    RMenuPage::_playTimer.start( RMenuPage::_playInterval );

    setStatus( element_actived );
    RMenuTab::update();
}

void RMenuPage::paintEvent(QPaintEvent * /*event*/ )
{
    RPainter painter(this);

    painter.save();
    doPaint( painter );
    painter.restore();

    QRect dimmyRect = getActiveRect();

    _style.paintEvent( painter, dimmyRect, meStatus );
}

void RMenuPage::showEvent(QShowEvent * event)
{
    RMenuTab::showEvent( event );

    QList<int> titleIds;
    getTitle( titleIds );
    setTitle( titleIds  );

    if ( isTop() )
    { decideTune(); }
}

void RMenuPage::hideEvent(QHideEvent *event)
{
    QWidget::hideEvent( event );

    closeAttachWidget();

    tuneLedShow( false );
}

void RMenuPage::update()
{
    setStatus( isTop() ? element_actived : element_deactived );

    RMenuTab::update();
}

void RMenuPage::toggleExpand()
{
    RMenuWnd *pMenuWnd;
    pMenuWnd = dynamic_cast<RMenuWnd*>( sysGetMenuWindow() );
    Q_ASSERT( pMenuWnd != NULL );
    pMenuWnd->setExpand( !pMenuWnd->isExpand() );

    //! on hide
    if ( !pMenuWnd->isExpand() )
    {
        closeAttachWidget();
    }
}

void RMenuPage::setStatus(ElementStatus stat)
{
    RMenuTab::setStatus( stat );

    _style.mTopBorderColor = mCurrentColorGp.mActive;
}

void RMenuPage::decideTune()
{
    QWidget* pWig;
    RControl *pControl;

    //! de tune
    RMenuPage::setTuneWidget( NULL );

    //! tnue focus
    foreach( pWig, mWidgetList )
    {
        pControl = dynamic_cast<RControl*>(pWig);

        Q_ASSERT( NULL != pControl );

        //! focus
        if ( pControl->isTuneAble() && pWig->isEnabled() )
        {
            pControl->tuneFocusIn();
            break;
        }
    }
}

void RMenuPage::show()
{
    QWidget* pWig;
    RControl *pControl;

    //! update value
    foreach( pWig, mWidgetList )
    {
        pControl = dynamic_cast<RControl*>(pWig);
        Q_ASSERT( NULL != pControl );

        //! update value
        pControl->on_value_changed( pControl->m_pResMenuItem->mId );
    }

    QWidget::show();
}

void RMenuPage::on_playActiveTimeout()
{
    RMenuPage::_playNow++;

    //! timeout
    if ( RMenuPage::_playNow >= RMenuPage::_playPeriod )
    {
        RMenuPage::_playTimer.stop();
        RMenuPage::_playTimer.disconnect();

        setStatus( element_actived );
    }
    else
    {
        if ( RMenuPage::_playNow & 0x01 )
        { setStatus( element_deactived ); }
        else
        { setStatus( element_actived ); }
    }

    RMenuTab::update();
}

void RMenuPage::on_sig_active_changed( int total, int now )
{
    RMenu *pParent;

    //! parent menu
    pParent = mParent;
    for ( int i = 0; i < total - now - 1; i++ )
    {
        Q_ASSERT( NULL != pParent );
        pParent = pParent->getParent();
    }

    //! now show parent
    Q_ASSERT( NULL != pParent );

    //! return to parent
    serviceExecutor::post( mParent->m_pServMenu->getConsoleName(),
                           CMD_SERVICE_SUB_RETURN,
                           mParent->m_pResMenu->m_pResItem->getId()
                           );

    pParent->setActive();

    //! toTop
    RMenuPage::toTop();
}

void RMenuPage::on_sig_active_pressed( int /*total*/, int /*now*/ )
{
    //! now show parent
    Q_ASSERT( NULL != mParent );
    //! into active
    if ( !mParent->getActive() )
    { mParent->setActive(); }
    //! active now
    else
    {
        toggleExpand();
    }

    //! toTop
    RMenuPage::toTop();
}

void RMenuPage::onActive( int intoAct )
{
    QWidget *pWig;
    RControl *pControl;

    foreach( pWig, mWidgetList )
    {
        Q_ASSERT( NULL != pWig );
        pControl = dynamic_cast<RControl*>(pWig);

        Q_ASSERT( NULL != pControl );
        pControl->onParentActive();
    }

    if ( intoAct == 1 )
    {
        toTop();

        //added by hexiaohua. for bug2616
        decideTune();

        update();
    }
    else
    {
        playActive();
    }
}
void RMenuPage::onDeActive()
{
    QWidget *pWig;
    RControl *pControl;

    RMenuPage::_playTimer.stop();
    RMenuPage::_playTimer.disconnect();

    foreach( pWig, mWidgetList )
    {
        Q_ASSERT( NULL != pWig );
        pControl = dynamic_cast<RControl*>(pWig);

        Q_ASSERT( NULL != pControl );
        pControl->onParentDeactive();
    }
}

void RMenuPage::setParent( RMenu *pParent )
{
    mParent = pParent;
}

/*!
 * \brief RMenuPage::addWidget
 * \param pWidget
 * 向菜单页中添加菜单项控件
 */
void RMenuPage::addWidget( QWidget *pWidget )
{
    Q_ASSERT( NULL != pWidget );

    pWidget->move( _style.mItemX,
                   _style.mItemCap + mWidgetList.size()*_style.mItemH );

    //! parent
    pWidget->setParent( this );

    mWidgetList.append( pWidget );
}

QList< QWidget* > & RMenuPage::getWidgetList()
{
    return mWidgetList;
}

/*!
 * \brief RMenuPage::crossRef
 * \param pServMenu
 * 交叉链接菜单项
 * -不同的菜单页中存在链接关系
 * -例如，在父菜单中选择了不同项目后，菜单的内容／结构会发生变化，
 * 这个变化过程是通过在菜单中增加链接关系实现的
 */
void RMenuPage::crossRef( RServMenu *pServMenu )
{
    QWidget* pWidget;
    RControl *pControl;

    RMenu *pMenu, *pChildMenu;
    RChildButton *pChildButton;

    CResItem *pResItem;

    int childId;

    //! each menu
    foreach( pMenu, pServMenu->getMenus() )
    {
        //! each widget
        foreach( pWidget, mWidgetList )
        {
            pControl = dynamic_cast<RControl*>(pWidget);

            //! sub button
            if( pControl->getControl() == Button_Sub )
            {
                pChildButton = dynamic_cast<RChildButton*>(pWidget);

                //! direct child
                childId = pChildButton->getChildId();
                if ( childId != -1 )
                {
                    pChildMenu = pServMenu->findMenu( childId );

                    if ( pChildMenu == NULL )
                    {
                        qDebug()<< pMenu->getName()<<pServMenu->getConsoleName()<<childId;
                    }
                    Q_ASSERT( pChildMenu != NULL );

                    pChildButton->setChild( pChildMenu );
                }
            }
            //! options
            else
            {
                foreach( pResItem, pControl->m_pResMenuItem->m_pResItem->mOption )
                {
                    if ( pResItem->mOptionMenuId != -1 )
                    {
                        pChildMenu = pServMenu->findOptionMenu( pResItem->mOptionMenuId );

                        if ( pChildMenu == NULL )
                        {
                            qDebug()<<pResItem->getContentString()<<pResItem->mOptionMenuId;
                        }
                        Q_ASSERT( pChildMenu != NULL );

                        pResItem->m_pOptionMenu = pChildMenu;
                    }
                    else
                    {
                        pResItem->m_pOptionMenu = NULL;
                    }
                }
            }
        }
    }
}

RMenu *RMenuPage::getOptionMenu( int msg,
                                 QList<int> &routedMsgList )
{
    QWidget* pWidget;
    RControl *pControl;
    CResMenuItem *pResItem;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>(pWidget);
        Q_ASSERT( pControl != NULL );

        pResItem = pControl->m_pResMenuItem;
        Q_ASSERT( pControl != NULL );

        //! msg has proced
        if ( routedMsgList.contains(pResItem->mId) )
        {
            continue;
        }

        //! cmd match
        if ( pResItem->mId == msg )
        {
            routedMsgList.append( pResItem->mId );
            return pControl->queryOptionMenu();
        }
    }

    return NULL;
}

bool RMenuPage::containsMsg( int msg )
{
    QWidget* pWidget;
    RControl *pControl;
    CResMenuItem *pResItem;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>(pWidget);
        Q_ASSERT( pControl != NULL );

        pResItem = pControl->m_pResMenuItem;
        Q_ASSERT( pControl != NULL );

        //! cmd match
        if ( pResItem->mId == msg )
        {
           return true;
        }
    }

    return false;
}

void RMenuPage::collectOptionMenuMsg( QList<int> &optionMenu )
{
    QWidget* pWidget;
    RControl *pControl;
    CResMenuItem *pResItem;
    CResItem *pOptItem;

    //! each widget
    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>(pWidget);
        Q_ASSERT( pControl != NULL );

        pResItem = pControl->m_pResMenuItem;
        Q_ASSERT( pControl != NULL );

        Q_ASSERT( pResItem->m_pResItem != NULL );

        //! option item
        foreach( pOptItem, pResItem->m_pResItem->mOption )
        {
            if ( pOptItem->m_pOptionMenu != NULL )
            {
                optionMenu.append( pResItem->mId );
                break;
            }
        }
    }
}

void RMenuPage::getTitle( QList<int> &titleIds )
{
    Q_ASSERT( mParent != NULL );

    mParent->getTitle( titleIds );
}

void RMenuPage::reTranslate()
{
    QWidget *pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>( pWidget );
        Q_ASSERT( pControl != NULL );
        pControl->reTranslate();
    }
}

void RMenuPage::tuneLedShow( bool bShow )
{
    //! led control
    CArgument arg;

    arg.append( DsoEngine::led_func );
    arg.append( bShow );
    serviceExecutor::post( E_SERVICE_ID_DSO,
                           CMD_SERVICE_LED_REQUESET,
                           arg );
}

void RMenuPage::on_value_changed( int msg )
{
    QWidget* pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        pControl = dynamic_cast<RControl*>(pWidget);

        Q_ASSERT( pControl!=NULL );

        pControl->on_value_changed( msg );
    }
}

void RMenuPage::on_enable_changed( int msg, bool b )
{
    QWidget *pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>( pWidget );
        Q_ASSERT( pControl != NULL );
        pControl->on_enable_changed(msg, b );
    }

    //! no tune
    if ( RMenuPage::getTuneWidget() == NULL && isVisible() )
    { RMenuPage::decideTune(); }
}
void RMenuPage::on_visible_changed( int msg, bool b )
{
    QWidget *pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>( pWidget );
        Q_ASSERT( pControl != NULL );
        pControl->on_visible_changed(msg, b );
    }

    //! no tune
    if ( RMenuPage::getTuneWidget() == NULL && isVisible() )
    { RMenuPage::decideTune(); }
}

void RMenuPage::on_enable_changed( int msg, int opt, bool b )
{
    QWidget *pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>( pWidget );
        Q_ASSERT( pControl != NULL );
        pControl->on_enable_changed(msg, opt, b );
    }
}
void RMenuPage::on_visible_changed( int msg, int opt, bool b )
{
    QWidget *pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>( pWidget );
        Q_ASSERT( pControl != NULL );
        pControl->on_visible_changed(msg, opt, b );
    }
}

void RMenuPage::on_focus_changed( int msg )
{
    QWidget *pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>( pWidget );
        Q_ASSERT( pControl != NULL );

        if ( pControl->cmdMatch(msg ) && pWidget->isEnabled() )
        {
            pControl->tuneFocusIn();
            break;
        }
    }
}

void RMenuPage::on_context_changed( int msg )
{
    QWidget *pWidget;
    RControl *pControl;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( pWidget != NULL );
        pControl = dynamic_cast<RControl*>( pWidget );
        Q_ASSERT( pControl != NULL );

        if ( pControl->cmdMatch(msg ) && pWidget->isEnabled() )
        {
            pControl->on_context_changed( msg );
            break;
        }
    }
}

}
