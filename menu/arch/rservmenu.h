#ifndef RSERVMENU_H
#define RSERVMENU_H

#include <QtCore>

#include "../menuwidget/rcontrol.h"

namespace menu_res {

/*!
 * \brief The RServMenu class
 *
 * a service menu
 * - one or more menus
 * - one or more pages in each menu
 * - the menu is a tree
 */
class RServMenu : public QObject
{
    Q_OBJECT

private:
    static RMenu **_subMenuAry;        //! index array
    static RMenu **_optMenuAry;

    static QMap<int, QWidget*> _mServWidgets;

public:
    static void openMenuMapCache();
    static void closeMenuMapCache();

    static void attachServiceWidget( int msg, QWidget *pWig );
    static void removeServiceWidget( int msg);
public:
    RServMenu();
    RServMenu( CServMenu *pServMenu,
               CServRes *pServRes
                );

    ~RServMenu();

public:
    void addMenu( RMenu *pMenu );
    QList<RMenu*> & getMenus() ;
    RMenu * operator[]( int i );

    void setConsoleName( const QString &str );
    QString &getConsoleName() ;

    RMenu * findMenu( int id );
    RMenu * findOptionMenu( int id );

    CResItem *fineRes( int msg );

    RMenu * findMenuByMsg( int msg );
    QWidget * findView( int msg );
    CServMenu *getCServMenu();

    void reTranslate();

    void buildCrossArray();

    void initResAttr( ui_attr::uiAttr *pAttr );

    void cfgFontColor( const QColor &focusIn,
                       const QColor &focusOut );

protected Q_SLOTS:
    void on_value_changed( int msg );

    void on_enable_changed( int msg, bool en );
    void on_enable_changed( int msg, int opt, bool en );

    void on_visible_changed( int msg, bool en );
    void on_visible_changed( int msg, int opt, bool en );

    void on_content_changed( int msg );
    void on_context_changed( int msg );

    void on_lang_changed();

    void on_active_changed( int act );

    void on_active_in( int msg );
    void on_active_out( int msg );

    void on_focus_changed( int msg );

private:
    QList<RMenu*> mListMenu;        /*!< service menus */
    QString mConsoleName;           /*!< console name */

    CServMenu *m_pServMenu;
    CServRes *m_pServRes;

    RMenu *m_pLastMenu;
};

typedef menu_res::RServMenu serviceMenu;
typedef QList<serviceMenu*> serviceMenuList;

}

#endif // RSERVMENU_H
