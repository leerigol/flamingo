#include "rmenubuilder.h"

#include "../rmenus.h"

namespace   menu_res {

/*!
 * \var RMenuBuilder::menu_page_count
 * menu page item count
 */
REventFilter * RMenuBuilder:: _pEvtFilter = NULL;

//! config
QString RMenuBuilder::_descFile;
QString RMenuBuilder::_menuFile;
QString RMenuBuilder::_resFile;

void RMenuBuilder::installEventFilter()
{
    //! has installed before
    if ( RMenuBuilder::_pEvtFilter!=NULL )
    {
        Q_ASSERT(false);
    }

    //! 安装系统事件过滤器
    RMenuBuilder::_pEvtFilter = new REventFilter(0);
    Q_ASSERT( NULL != RMenuBuilder::_pEvtFilter );
    RMenuBuilder::_pEvtFilter->install();

    RMenuPage::setEventFilter( RMenuBuilder::_pEvtFilter );

    //! sys mapper
    sysSetKeyMapper( &RMenuBuilder::_pEvtFilter->getKeyMap() );
}

void RMenuBuilder::uninstallEventFilter()
{
    Q_ASSERT( RMenuBuilder::_pEvtFilter != NULL );
    RMenuBuilder::_pEvtFilter->uninstall();

    delete RMenuBuilder::_pEvtFilter;
    RMenuBuilder::_pEvtFilter = NULL;
}

void RMenuBuilder::attachResource()
{
    QString path;

    path = sysGetResourcePath();

    Q_ASSERT( path.endsWith("/"));
    Q_ASSERT( path.length() > 0 );

    //! 相对路径
    path += hex_dir;

    _descFile = path + desc_file;
    _menuFile = path + menu_file;
    _resFile = path + res_file;

    //! 配置菜单数据文件
    //! 配置顺序必须和语言顺序一致
    //! 语言顺序从0开始
    CResData::addFile( path + dat_c_file );
    CResData::addFile( path + dat_b_file );
    CResData::addFile( path + dat_d_file );
    CResData::addFile( path + dat_h_file );

    CResData::addFile( path + dat_i_file );
    CResData::addFile( path + dat_j_file );
    CResData::addFile( path + dat_k_file );
    CResData::addFile( path + dat_l_file );

    CResData::addFile( path + dat_m_file );
    CResData::addFile( path + dat_n_file );
    CResData::addFile( path + dat_o_file );

    CResData::addFile( path + dat_t_file );
    CResData::addFile( path + dat_u_file );

    Q_ASSERT( CResData::_mNameStringList.size() == LANG_COUNT );

    CResData::setImageFile( path + dat_pic_file );
}

void RMenuBuilder::on_value_changed( int msg )
{
    Q_ASSERT( RMenuBuilder::_pEvtFilter != NULL );

    RMenuBuilder::_pEvtFilter->getKeyMap().on_value_changed( msg );
}

/*!
 * \brief RMenuBuilder::findView
 * \param msg
 * \return
 * 从系统中查找对应消息的控件
 */
QWidget* RMenuBuilder::findView(const QString &servName,
                                int msg )
{
    serviceMenuList *pList;

    pList = sysGetMenuList();
    Q_ASSERT( NULL != pList );

    serviceMenu *pServMenu;

    foreach( pServMenu, *pList )
    {
        Q_ASSERT( NULL != pServMenu );

        if ( pServMenu->getConsoleName() == servName )
        {
            return pServMenu->findView( msg );
        }
    }

    return NULL;
}

QString RMenuBuilder::getString( int msg,
                                 const QString &defStr )
{
    CResItem *pItem;
    pItem = RMenuBuilder::findRes( msg );
    if ( NULL == pItem )
    {
        return defStr;
    }

    return pItem->getString();
}

QString RMenuBuilder::getOptionString(int msg, int opt, const QString &defStr)
{
    CResItem *pItem;
    pItem = RMenuBuilder::findRes( msg );
    if ( NULL == pItem )
    {
        return defStr;
    }

    return pItem->getOptionString( opt );
}

int RMenuBuilder::getImage( int msg, QImage &img )
{
    CResItem *pItem;

    pItem = RMenuBuilder::findRes( msg );
    Q_ASSERT( NULL != pItem );

    return pItem->getImage( img );
}

int RMenuBuilder::getResource( int msg, QString &str, QImage &img )
{
    CResItem *pItem;

    pItem = RMenuBuilder::findRes( msg );
    if ( NULL == pItem )
    { return -1; }

    eMenuRes type = pItem->getType();

    if ( is_attr( type, MR_Str) )
    {
        str = pItem->getString();
    }
    if ( is_attr( type, MR_Icon) )
    {
        pItem->getImage( img );
    }

    return 0;
}

//! 搜索资源项目
CResItem *RMenuBuilder::findRes( int msg )
{
    serviceMenuList *pList;

    pList = sysGetMenuList();
    Q_ASSERT( NULL != pList );

    serviceMenu *pServMenu;
    CResItem *pItem;

    foreach( pServMenu, *pList )
    {
        Q_ASSERT( NULL != pServMenu );
        pItem = pServMenu->fineRes( msg );
        if ( NULL != pItem )
        {
            return pItem;
        }
    }

    return NULL;
}

RMenuBuilder::RMenuBuilder()
{
    QFile file( _descFile );

    if ( file.exists() )
    {
    }
    else
    {
        qFatal("desc file missing");
    }

    CResDesc::loadDesc( _descFile );
}

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";

#define def_time()
#define start_time()
#define end_time()

RServMenu* RMenuBuilder::loadMenu( const QString &rsrcName,
                                   QWidget *parent,
                                   int srcId,
                                   int dstId )
{
    def_time();

    //! get desc
    CResDesc *pDesc = CResDesc::getDesc( rsrcName );
    if(pDesc == NULL)
    {
         LOG_DBG()<<rsrcName;
    }
    Q_ASSERT( pDesc != NULL );

    //! sub component
    RServMenu *pServMenu;
    CServMenu *pMenu;
    CServRes *pRes;

    int ret;

    //! menu
    pMenu = new CServMenu();
    //! options
    pRes = new CServRes();
    Q_ASSERT( pMenu != NULL );
    Q_ASSERT( pRes != NULL );

    //! load
    ret = loadServMenu( pMenu, &pDesc->mDescMenu );
    if ( ret != 0 )
    {
        return NULL;
    }
    ret = loadServRes( pRes, &pDesc->mDescRes );
    if ( ret != 0 )
    {
        return NULL;
    }

    //! replace title
    if ( srcId != 0 && dstId != 0 )
    {
        pMenu->replace( srcId, dstId );
    }

    //! link menu && resource
    start_time();
    pMenu->linkResource( *pRes );
    end_time();

    start_time();
    pServMenu = newMenu( pMenu, pRes, parent );
    end_time();

    RServMenu::openMenuMapCache();

    //! cross ref
    start_time();
    pServMenu->buildCrossArray();
    crossRefMenu( pServMenu );
    end_time();

    RServMenu::closeMenuMapCache();

    return pServMenu;
}

int RMenuBuilder::loadServMenu( CServMenu *pMenu, vDesc *desc  )
{
    Q_ASSERT( NULL != pMenu );
    char *pBuf;

    pBuf = CCache::getCache( desc->mOffset, desc->mLength );

    //! cache fail
    if ( pBuf == NULL )
    {
        QFile file( _menuFile );
        file.open( QIODevice::ReadOnly );
        file.seek( desc->mOffset );
        pBuf = CCache::getBuffer( desc->mLength );
        Q_ASSERT( pBuf != NULL );
        if ( desc->mLength != file.read( pBuf, desc->mLength ) )
        {
            file.close();
            return -1;
        }

        file.close();

        //! cache in
        CCache::cacheIn( desc->mOffset, desc->mLength, pBuf );
    }
    //! cache success
    else
    {
        //! use buf directly
    }

    QByteArray byteAry( pBuf, desc->mLength );
    QDataStream stream( &byteAry, QIODevice::ReadOnly );
    stream.setByteOrder( QDataStream::LittleEndian );
    pMenu->serialIn( stream );

    return 0;
}
int RMenuBuilder::loadServRes( CServRes *pRes, vDesc *desc  )
{
    Q_ASSERT( NULL != pRes );

    char *pBuf;

    pBuf = CCache::getCache( desc->mOffset, desc->mLength );

    //! cache fail
    if ( NULL == pBuf )
    {
        QFile file( _resFile );
        file.open( QIODevice::ReadOnly );
        file.seek( desc->mOffset );

        pBuf = CCache::getBuffer( desc->mLength );
        Q_ASSERT( pBuf != NULL );

        if ( desc->mLength != file.read( pBuf, desc->mLength ) )
        {
            file.close();
            return -1;
        }

        file.close();

        CCache::cacheIn( desc->mOffset, desc->mLength, pBuf );
    }
    //! cache success
    else
    {

    }

    QByteArray byteAry( pBuf, desc->mLength );
    QDataStream stream( &byteAry, QIODevice::ReadOnly );
    stream.setByteOrder( QDataStream::LittleEndian );

    pRes->serialIn( stream );

    return 0;
}

RServMenu * RMenuBuilder::newMenu( CServMenu *pMenu,
                       CServRes *pres,
                       QWidget *parent )
{
    def_time();
    //! new service menu
    RServMenu *pServMenu = new RServMenu( pMenu, pres );

    tItemMap itemMap;

    CResMenu *pResMenu;
    RMenu *pRMenu;

    //! open cache
    RCombox::cacheOpenPopupList();

    openMenuItem( itemMap );

    //! each menu
    foreach( pResMenu, pMenu->getMenus() )
    {
        //! build new menu
        start_time();
        pRMenu = buildAMenu( pServMenu, pResMenu, parent, itemMap);
        end_time();
        pRMenu->setServMenu( pServMenu );

        pServMenu->addMenu( pRMenu );
    }

    //! delete cache
    clearMenuItem( itemMap );

    //! clear cache
    RCombox::cacheClosePopupList();

    return pServMenu;
}

RMenu* RMenuBuilder::buildAMenu( RServMenu *pServ,
                                 CResMenu *p_aMenu,
                                 QWidget *parent,
                                 tItemMap &itemMap )
{
    RMenu *pMenu;

    RMenuPage *pPage;
    QWidget *pWidget;

    //! new menu
    pMenu = new RMenu();
    pMenu->setName( p_aMenu->getName() );
    pMenu->setResMenu( p_aMenu );
    //! page
    pPage = NULL;

    CResMenuItem *pMenuItem,*pCacheItem;

    //! control types
    RInvertButton* pButInvert;
    RChildButton * pButChild;
    RLabel_i_i_f *pLabiif;

    RCombox *pCombox;
    RComboxCheck *pComboxCheck;
    RPushButton *pPushButton;

    RControl *pControl;

    def_time();
    start_time();
    foreach( pMenuItem, p_aMenu->getItems() )
    {
        //! new page
        //! 多于一页的菜单
        if ( pPage != NULL &&
             pPage->getWidgetList().count() >= MENU_PAGE_COUNT )
        {
            pPage = NULL;
        }

        //! a new page
        if ( pPage == NULL )
        {
            pPage = new RMenuPage( parent );
            Q_ASSERT( NULL != pPage );
            pMenu->addPage( pPage );
        }

        //! menu item cache
        pCacheItem = RMenuBuilder::findMenuItem( pMenuItem->mId,
                                                 itemMap );
        //! find none
        if ( pCacheItem == NULL )
        {
            //! insert
            RMenuBuilder::insertMenuItem( pMenuItem->mId,
                                          pMenuItem, itemMap );
        }
        else
        {
            pMenuItem = pCacheItem;
        }

        //! new controls
        pWidget = NULL;
        switch( pMenuItem->getType() )
        {
        //! top menu
        //! no action
        case Menu_Title:
            continue;
            break;

        //! child menu
        //! no action
        case Sub_Menu_Title:
            continue;
            break;

        case Button_Two:
            start_time();
            pWidget = new RInvertButton();

            pControl = dynamic_cast<RControl*>( pWidget );

            //! control info
            pButInvert = dynamic_cast<RInvertButton*>(pWidget);
            pButInvert->setResItem( pMenuItem );
            pButInvert->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Button_Sub:
        case Button_sub_no_ret:
            start_time();
            pWidget = new RChildButton();

            pControl = dynamic_cast<RControl*>( pWidget );

            pButChild = dynamic_cast<RChildButton*>(pWidget);
            pButChild->setResItem( pMenuItem );
            pButChild->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Label_Float:
            start_time();
            pWidget = new RLabel_i_i_f();

            pControl = dynamic_cast<RControl*>( pWidget );

            pLabiif = dynamic_cast<RLabel_i_i_f*>(pWidget);

            pLabiif->setResItem( pMenuItem );
            pLabiif->setServMenu( pServ );

            pLabiif->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Label_int_float_float_knob:
            start_time();
            pWidget = new RLabel_i_i_f();

            pControl = dynamic_cast<RControl*>( pWidget );

            pLabiif = dynamic_cast<RLabel_i_i_f*>(pWidget);

            pLabiif->setResItem( pMenuItem );
            pLabiif->setServMenu( pServ );

            pLabiif->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            pLabiif->setPara( para_key );
            end_time();
            break;

        //! 选中即应用
        case Combox_Func:
            start_time();
            pWidget = new RCombox( "", 0, parent );

            pControl = dynamic_cast<RControl*>( pWidget );

            pCombox = dynamic_cast<RCombox*>(pWidget);
            pCombox->setMode( RCombox::RCombox_norm );

            pCombox->setResItem( pMenuItem );
            pCombox->setServMenu( pServ );

            pCombox->setAttr( pMenuItem->mPara1 );
            pCombox->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        //! 选中!=应用
        //! 需要确认
        case Combox:
            start_time();
            pWidget = new RCombox( "", 0, parent );

            pControl = dynamic_cast<RControl*>( pWidget );

            pCombox = dynamic_cast<RCombox*>(pWidget);
            pCombox->setMode( RCombox::RCombox_ack );
            pCombox->setResItem( pMenuItem );
            pCombox->setServMenu( pServ );

            pCombox->setAttr( pMenuItem->mPara1 );
            pCombox->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Label_Icon:
            start_time();
            pWidget = new RLabel_Icon();

            pControl = dynamic_cast<RControl*>(pWidget);

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );

            pControl->setTextVisible( false );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            pControl->setPara( para_key );
            end_time();
            break;

        case Button_One:
            start_time();
            pWidget = new RPushButton();

            pControl = dynamic_cast<RControl*>( pWidget );

            pPushButton = dynamic_cast<RPushButton*>(pWidget);
            pPushButton->setResItem( pMenuItem );
            pPushButton->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Check_Box:
            start_time();
            pWidget = new RComboxCheck("",0, parent );

            pControl = dynamic_cast<RControl*>( pWidget );

            pComboxCheck = dynamic_cast<RComboxCheck*>(pWidget);
            pComboxCheck->setMode( RCombox::RCombox_check );

            pComboxCheck->setResItem( pMenuItem );
            pComboxCheck->setServMenu( pServ );

            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Label_int:
            start_time();
            pLabiif = new RLabel_i_i_f();
            pWidget = pLabiif;

            pControl = dynamic_cast<RControl*>( pWidget );

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );

            pLabiif->setFmt( control_fmt_integer );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Label_str:
            start_time();
            pWidget = new RLabel_n_s_s();

            pControl = dynamic_cast<RControl*>( pWidget );

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case Label_ime:
            start_time();
            pWidget = new RTextBox();

            pControl = dynamic_cast<RControl*>( pWidget );
            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );

            pControl->setUiAttr( pMenuItem->mPara1,
                                 pMenuItem->mPara2 );
            end_time();
            break;

        case TimeEdit:
            start_time();
            pWidget = new RTimeEdit();

            pControl = dynamic_cast<RControl*>( pWidget );

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case DateEdit:
            start_time();
            pWidget = new RDateEdit();

            pControl = dynamic_cast<RControl*>( pWidget );

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case IpEdit:
            start_time();
            pWidget = new RIpEdit();

            pControl = dynamic_cast<RControl*>( pWidget );

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case SeekBar:
            start_time();
            pWidget = new RSeekBar();

            pControl = dynamic_cast<RControl*>( pWidget );

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        //! 复合选项控件
        case ComboxKnob:
            start_time();
            //! drop down list
            if ( (pMenuItem->mPara1 & 0x40)==0x40 )
            {
                pWidget = new RDropDownCombox_iif( "", 0, parent );
            }
            else
            {
                pWidget = new RCombox_iif( "", 0, parent );
            }
            pCombox = dynamic_cast<RCombox*>(pWidget);

            pControl = dynamic_cast<RControl*>( pWidget );

            pCombox->setMode( RCombox::RCombox_norm );

            pCombox->setResItem( pMenuItem );
            pCombox->setServMenu( pServ );

            pCombox->setAttr( pMenuItem->mPara1 );
            pCombox->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        case LiteCombox:
            pWidget = new RLiteCombox();
            Q_ASSERT( false );
            break;

        case Space:
            start_time();
            pWidget = new RSpace();

            pControl = dynamic_cast<RControl*>( pWidget );

            pControl->setResItem( pMenuItem );
            pControl->setServMenu( pServ );
            pControl->setUiAttr( pMenuItem->mPara1, pMenuItem->mPara2 );
            end_time();
            break;

        default:
            Q_ASSERT( false );
        }

        Q_ASSERT( pPage != NULL );
        Q_ASSERT( pWidget != NULL );

        //! add widget into page
        //! pWidget.parent = pPage
        pPage->addWidget( pWidget );

        //! 设置控件类型
        pControl->setControlType( pMenuItem->getType() );
    }
    end_time();

    return pMenu;
}

void RMenuBuilder::crossRefMenu( RServMenu *pServMenu )
{
    Q_ASSERT( pServMenu != NULL );

    RMenu *pMenu;
    foreach ( pMenu, pServMenu->getMenus() )
    {
        pMenu->crossRef( pServMenu );
    }
}

void RMenuBuilder::openMenuItem( tItemMap &/*list*/  )
{
}

CResMenuItem * RMenuBuilder::findMenuItem( int msg, tItemMap &list )
{
    if ( list.contains(msg) )
    {
        return list[msg];
    }

    return NULL;
}

void RMenuBuilder::insertMenuItem( int msg, CResMenuItem *pWig,
                                 tItemMap &list )
{
    list.insert( msg, pWig );
}

void RMenuBuilder::clearMenuItem( tItemMap &list )
{
    list.clear();
}

}

