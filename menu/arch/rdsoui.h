#ifndef RDSOUI_H
#define RDSOUI_H

#include <QtCore>
#include "../../include/dsotype.h"

using namespace DsoType;

namespace menu_res {

class RDsoUI;
/*!
 * \brief The RDsoView class
 * 视图
 * -一个虚拟的结构，可以单独控制显示或隐藏
 * -视图有物理大小
 */
class RDsoView
{
public:
    RDsoView( DsoView mView );
public:
    RDsoView* setVisible( bool b );
    bool getVisible();

    RDsoView* setPhy( QRect &rect );
    RDsoView* setPhy( int l, int t, int w, int h );

    void attachUI( RDsoUI *pUI );
    void detachUI( RDsoUI *pUI );

public:
    DsoView mView;
    QList< RDsoUI *> mUiList;

    bool mVisible;

private:
    QRect mPhyRect;
};

/*!
 * \brief The RDsoUI class
 * UI元素
 * -ui元素布置在视图上
 * -如果视图关闭，则所有的ui均不能显示
 * -各个ui可以单独控制显示
 */
class RDsoUI
{
public:
    enum uiAttribute
    {
        ui_attr_none = 0,
        ui_attr_moveable = 1,
        ui_attr_physical = 2,
    };

public:
    RDsoUI();

    void setShow( bool b );
    bool getShow();

    void setView( QRect &rect );
    void setPhy( QRect &rect );

    void setPhyShift( int x, int y );
    void setPhyShift( const QPoint &pt );

    void setView( int w, int h );
    void setPhy( int l, int t, int w, int h );

    void setDsoView( RDsoView *pView );
    RDsoView *getDsoView();

    void setAttr( uiAttribute attr );
    uiAttribute getAttr();

    void setActive( bool bActive );
    bool getActive();

protected:
    virtual void setWidgetVisible( bool b ) = 0;
    virtual void refresh() = 0;

protected:
    void viewShow( bool bShow );

protected:
    bool mbVisible;             //! soft visible
    QRect m_viewRect;           //!< (0,0) --> (width,height)
    QRect m_phyRect;            //!< physical rect

    RDsoView *mpDsoView;
    QPoint m_ptShift;

    uiAttribute mAttr;
    bool mbActive;

friend class RDsoView;
};

/*!
 * \brief The RDsoUIY class
 * 垂直方向上的ui元素
 */
class RDsoUIY : public RDsoUI
{
public:
    RDsoUIY();

public:
    void setViewGnd( int viewGnd );

    void move( int viewY );
    void offset( int offset );

protected:
    void updatePhy();

protected:
    int mViewY;
    int mPhyY;

    int mViewGnd;
};

}

#endif // RDSOUI_H
