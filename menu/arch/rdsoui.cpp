#include "rdsoui.h"

namespace menu_res {

/*!
 * \brief RDsoView::RDsoView
 * \param view
 * 显示窗口被划分成了不同的视图区域
 */
RDsoView::RDsoView( DsoView view )
{
    mVisible=false;
    mUiList.clear();

    mView = view;
}

RDsoView* RDsoView::setVisible( bool b )
{
    mVisible = b;

    RDsoUI *pUi;
    foreach( pUi, mUiList )
    {
        Q_ASSERT( NULL != pUi );

        pUi->viewShow( b );
    }

    return this;
}
bool RDsoView::getVisible()
{
    return mVisible;
}

RDsoView* RDsoView::setPhy( QRect &rect )
{
    RDsoUI *pUi;
    foreach( pUi, mUiList )
    {
        Q_ASSERT( NULL != pUi );
        pUi->setPhy( rect );
    }

    mPhyRect = rect;

    return this;
}
RDsoView* RDsoView::setPhy( int l, int t, int w, int h )
{
    RDsoUI *pUi;
    foreach( pUi, mUiList )
    {
        Q_ASSERT( NULL != pUi );
        pUi->setPhy( l,t,w,h);
    }

    mPhyRect.setRect( l, t, w, h );

    return this;
}

void RDsoView::attachUI( RDsoUI *pUI )
{
    Q_ASSERT( pUI != NULL );
    mUiList.append( pUI );

    if ( mPhyRect.width() != 0 )
    {
        pUI->setPhy( mPhyRect );
    }
}

void RDsoView::detachUI( RDsoUI *pUI )
{
    Q_ASSERT( pUI != NULL );
    mUiList.removeOne( pUI );
}


RDsoUI::RDsoUI( )
{
    mbVisible = false;

    mpDsoView = NULL;

    m_ptShift.setX( 0 );
    m_ptShift.setY( 0 );

    mAttr = ui_attr_none;
    mbActive = false;
}

void RDsoUI::setShow( bool b )
{
    mbVisible = b;

    if ( !mbVisible )
    {
        viewShow( false );
        return;
    }

    if( mpDsoView == NULL )
    {
        //! has parent view
        Q_ASSERT( mpDsoView != NULL );
    }
    if ( mpDsoView->getVisible() )
    {
        viewShow( true );
    }

}
bool RDsoUI::getShow()
{
    return mbVisible;
}

void RDsoUI::setView( QRect &rect )
{
    m_viewRect = rect;
}
void RDsoUI::setPhy( QRect &rect )
{
    m_phyRect = rect;
    refresh();
}

void RDsoUI::setPhyShift( int x, int y )
{
    m_ptShift.setX( x );
    m_ptShift.setY( y );
}
void RDsoUI::setPhyShift( const QPoint &pt )
{
    m_ptShift = pt;
}

void RDsoUI::setView( int w, int h )
{
    m_viewRect.setRect( 0,0,w,h);
}

void RDsoUI::setPhy( int l, int t, int w, int h )
{
    m_phyRect.setRect( l, t, w, h );
    refresh();
}

void RDsoUI::setDsoView( RDsoView *pView )
{
    Q_ASSERT( NULL != pView );
    mpDsoView = pView;
    refresh();
}

RDsoView *RDsoUI::getDsoView()
{
    return mpDsoView;
}

void RDsoUI::viewShow( bool bShow )
{
    if ( bShow && mbVisible )
    {
        setWidgetVisible( true );
    }
    else
    {
        setWidgetVisible( false );
    }
}

void RDsoUI::setAttr( uiAttribute attr )
{
    mAttr = attr;
}
RDsoUI::uiAttribute RDsoUI::getAttr()
{
    return mAttr;
}

void RDsoUI::setActive( bool bActive )
{ mbActive = bActive; }
bool RDsoUI::getActive()
{ return mbActive; }

RDsoUIY::RDsoUIY()
{
    mViewGnd = 0;
}

void RDsoUIY::setViewGnd( int viewGnd )
{
    mViewGnd = viewGnd;
}

void RDsoUIY::move( int viewY )
{
    mViewY = viewY;

    refresh();
}
void RDsoUIY::offset( int offset )
{
    move( mViewGnd - offset );
}

void RDsoUIY::updatePhy()
{
    Q_ASSERT( m_viewRect.height() > 0 );
    mPhyY = (mViewY - m_viewRect.top()) * m_phyRect.height() / m_viewRect.height()
            + m_phyRect.top();

    if ( mPhyY < m_phyRect.top() )
    {
        mPhyY = m_phyRect.top();
    }
    else if ( mPhyY > m_phyRect.bottom() )
    {
        mPhyY = m_phyRect.bottom();
    }
}
}
