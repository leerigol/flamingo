#ifndef WNDLOGINFO_H
#define WNDLOGINFO_H

#include <QString>
#include <QFont>
#include <QDateTime>
#include <QtWidgets>

#include "../../menu/rmenus.h"  //! controlers
#include "../../menu/wnd/rmodalwnd.h"

namespace menu_res  {

class WndLogInfo : public RModalWnd
{
    Q_OBJECT
public:
    WndLogInfo();

protected:
    virtual void setupUi( const QString &path,
                          const r_meta::CMeta &meta );
    void setBarStyle();

protected Q_SLOTS:
    void on_clear_clicked();

public:
    void logInfo( const QString &str );
    void logInfo( const QStringList &strList );
    void logInfo( const QStringList *ptr );

public:
    QListWidget *m_pDetailList;
    RButton *m_pBtnClear;
    RButton *m_pBtnExport;
};

}

#endif // WNDLOGINFO_H
