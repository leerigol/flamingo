#include "wndchcfg.h"

#include "../../com/inputkeypad/inputkeypad.h"

namespace menu_res  {

WndChCfg::WndChCfg( QWidget *parent )
          : menu_res::RModalWnd( parent )
{
    m_pLabScale = NULL;
    m_pLabOffset = NULL;
    m_pEditScale = NULL;
    m_pEditOffset = NULL;

    m_pBtnScaleAdd = NULL;
    m_pBtnScaleSub = NULL;

    m_pBtnOffsetAdd = NULL;
    m_pBtnOffsetSub = NULL;

    mUnit = Unit_V;

    mScaleFmt = dso_fmt_trim(fmt_float,fmt_width_3, fmt_no_trim_0 );
    mOffsetFmt = dso_fmt_trim(fmt_float,fmt_width_3, fmt_no_trim_0 );

    mLabScaleId = 0;
    mLabOffsetId = 0;
    mLabTitleId = 0;

    set_attr( wndAttr, mWndAttr, wnd_moveable|wnd_manual_resize );

    //! append to mgr
    addToShowQueue( this );
}

void WndChCfg::showEvent( QShowEvent *event )
{
    //! base
    RModalWnd::showEvent( event );

    //! format each value
    Q_ASSERT( NULL != m_pLabScale );

    //! label
    m_pLabScale->setText( sysGetString(mLabScaleId,"Scale") );
    m_pLabOffset->setText( sysGetString(mLabOffsetId,"Offset") );

    //! edit
    QString str;
    gui_fmt::CGuiFormatter::format( str, mScale, mScaleFmt, mUnit );
    m_pEditScale->setText( str );
    gui_fmt::CGuiFormatter::format( str, mOffset, mOffsetFmt, mUnit );
    m_pEditOffset->setText( str );
}

void WndChCfg::setupUi( const QString &path,
                        const r_meta::CMeta &meta  )
{
    if ( NULL != m_pLabScale ) return;

    //! create
    m_pLabScale = new QLabel(this);
    Q_ASSERT( NULL != m_pLabScale );
    m_pLabOffset = new QLabel( this );
    Q_ASSERT( NULL != m_pLabOffset );

    m_pEditOffset = new menu_res::RLineEdit( this );
    Q_ASSERT( NULL != m_pEditOffset );
    m_pEditScale = new menu_res::RLineEdit( this );
    Q_ASSERT( NULL != m_pEditScale );

    m_pBtnScaleAdd = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnScaleAdd );

    m_pBtnScaleSub = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnScaleSub );

    m_pBtnScaleSub->setArrowType(Qt::UpArrow);
    m_pBtnScaleAdd->setArrowType(Qt::DownArrow);

    m_pBtnOffsetSub = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnOffsetSub );
    m_pBtnOffsetSub->setArrowType(Qt::DownArrow);

    m_pBtnOffsetAdd = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnOffsetAdd );
    m_pBtnOffsetAdd->setArrowType(Qt::UpArrow);

    //! apply ui
    getGeo( meta, path + "geo/wnd", this );

    getGeo( meta, path + "geo/scale_lab", m_pLabScale );
    getGeo( meta, path + "geo/offset_lab", m_pLabOffset );

    getGeo( meta, path + "geo/scale_edit", m_pEditScale );
    getGeo( meta, path + "geo/offset_edit", m_pEditOffset );

    getGeo( meta, path + "geo/scale_add", m_pBtnScaleAdd );
    getGeo( meta, path + "geo/offset_add", m_pBtnOffsetAdd );

    getGeo( meta, path + "geo/scale_sub", m_pBtnScaleSub );
    getGeo( meta, path + "geo/offset_sub", m_pBtnOffsetSub );

    //! images
    QString str;

    //! style
    meta.getMetaVal( path + "style/wnd", str );
    this->setStyleSheet( str );

    //! connection
    connect( m_pEditScale, SIGNAL(sig_mouseRelease()),
             this, SLOT(on_scale_editing()) );
    connect( m_pEditOffset, SIGNAL(sig_mouseRelease()),
             this, SLOT(on_offset_editing()) );

    m_pSigMap = new QSignalMapper( this );
    Q_ASSERT( NULL != m_pSigMap );

    connect( m_pBtnScaleAdd, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );
    connect( m_pBtnScaleSub, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );
    connect( m_pBtnOffsetAdd, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );
    connect( m_pBtnOffsetSub, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );

    m_pSigMap->setMapping( m_pBtnScaleAdd, tune_scale_add );
    m_pSigMap->setMapping( m_pBtnScaleSub, tune_scale_sub );
    m_pSigMap->setMapping( m_pBtnOffsetAdd, tune_offset_add );
    m_pSigMap->setMapping( m_pBtnOffsetSub, tune_offset_sub );

    connect( m_pSigMap, SIGNAL(mapped(int)),
             this, SLOT(on_tune(int)) );
}

void WndChCfg::on_scale_real( DsoReal real )
{
    setScale( real );
    emit sig_scale_real( real );

}

void WndChCfg::on_offset_real( DsoReal real )
{
    setOffset( real );

    emit sig_offset_real( real );
}

void WndChCfg::on_scale_editing()
{
    QPoint ptOrg = this->mapToGlobal( QPoint(0,0) );
    InputKeypad::getInstance()->input( ptOrg,
                                       InputKeypad::toKeypadView( mUnit ),
                                       mScaleGp,
                                       this,
                                       SLOT(on_scale_real(DsoReal)),
                                       rect().size() );
}

void WndChCfg::on_offset_editing()
{
    QPoint ptOrg = this->mapToGlobal( QPoint(0,0) );
    InputKeypad::getInstance()->input( ptOrg,
                                       InputKeypad::toKeypadView( mUnit ),
                                       mOffsetGp,
                                       this,
                                       SLOT(on_offset_real(DsoReal)),
                                       rect().size() );
}

void WndChCfg::on_tune( int id )
{
    if( sysGetStarted() )
    {
        //QCoreApplication::processEvents();
    }
    emit sig_tune( id );
}

void WndChCfg::setLabelId( int scaleId, int offsetId )
{
    mLabScaleId = scaleId;
    mLabOffsetId = offsetId;
}

void WndChCfg::setScale( const DsoReal &scale )
{
    mScale = scale;

    if ( !mbSetupUi ) return;

    QString str;
    gui_fmt::CGuiFormatter::format( str, mScale, mScaleFmt, mUnit );
    m_pEditScale->setText( str );
    update();
}
DsoReal WndChCfg::getScale()
{ return mScale; }

void WndChCfg::setOffset( const DsoReal &offset )
{
    mOffset = offset;

    if ( !mbSetupUi ) return;

    DsoReal zero((qlonglong)0);
    QString str;
    if(offset == zero )
    {
        DsoType::DsoViewFmt fmt = dso_fmt_trim(fmt_float,fmt_width_3, fmt_no_trim_0 );
        gui_fmt::CGuiFormatter::format( str, mOffset,fmt , mUnit );
    }
    else
    {
        gui_fmt::CGuiFormatter::format( str, mOffset,mOffsetFmt , mUnit );
    }
    m_pEditOffset->setText( str );

    update();   
}
DsoReal WndChCfg::getOffset()
{ return mOffset; }

void WndChCfg::setScaleGp( const DsoRealGp &gp )
{
    mScaleGp = gp;
    setScale( gp.vNow );
}
void WndChCfg::setOffsetGp( const DsoRealGp &gp )
{
    mOffsetGp = gp;
    setOffset( gp.vNow );
}

void WndChCfg::setUnit( Unit unit )
{
    mUnit = unit;

    if ( !mbSetupUi ) return;

    update();
}
Unit WndChCfg::getUnit()
{ return mUnit; }

void WndChCfg::setScaleFmt( DsoType::DsoViewFmt fmt )
{ mScaleFmt = fmt; }
void WndChCfg::setOffsetFmt( DsoType::DsoViewFmt fmt )
{ mOffsetFmt = fmt; }

}
