#ifndef WNDTRIGCFG_H
#define WNDTRIGCFG_H

#include <QtWidgets>
#include "../../menu/wnd/rmodalwnd.h"
#include "../../widget/rlineedit.h"
namespace menu_res  {

class wndTrigCfg: public RModalWnd
{

    Q_OBJECT
public:
    enum tuneId
    {
        tune_level_a_add,
        tune_level_b_add,

        tune_level_a_sub,
        tune_level_b_sub,

        tune_sweep_auto,
        tune_sweep_single,
        tune_sweep_normal,
    };

public:
    wndTrigCfg( QWidget *parent );

protected:
    virtual void showEvent( QShowEvent *event );
    virtual void setupUi( const QString &path,
                  const r_meta::CMeta &meta  );
public:
    //! uis
    QLabel                 *m_pLabTitle;
    QLabel                 *m_pLabLevelA,   *m_pLabLevelB;
    menu_res::RLineEdit    *m_pEditLevelA,  *m_pEditLevelB;

    QToolButton *m_pBtnLevelAAdd, *m_pBtnLevelASub;
    QToolButton *m_pBtnLevelBAdd, *m_pBtnLevelBSub;

    QRadioButton           *m_pRadButAtuo,*m_pRadButSingle,*m_pRadButNormal;

    QSignalMapper          *m_pSigMap;

    QString       m_path;
    r_meta::CMeta m_meta;

public:
    int titleId() const;
    void setTitleId(int titleId);

    int leveleAId() const;
    void setLeveleAId(int leveleAId);

    int leveleBId() const;
    void setLeveleBId(int leveleBId);

    DsoReal leveleA() const;
    void setLeveleA(const DsoReal &leveleA);

    DsoReal leveleB() const;
    void setLeveleB(const DsoReal &leveleB);

    DsoRealGp leveleAGp() const;
    void setLeveleAGp(const DsoRealGp &leveleAGp);

    DsoRealGp leveleBGp() const;
    void setLeveleBGp(const DsoRealGp &leveleBGp);

    Unit leveleUnit() const;
    void setLeveleUnit(const Unit &leveleUnit);

    void setTwoLevel(bool TwoLevel);

    void setSweep(int sweep);

protected:
    int       m_titleId,   m_leveleAId, m_leveleBId;
    DsoReal   m_leveleA,   m_leveleB;
    DsoRealGp m_leveleAGp, m_leveleBGp;
    Unit      m_leveleUnit;
    DsoType::DsoViewFmt m_LevelFmt;
    bool      m_TwoLevel;  //! false:1个触发电平 true:2个触发电平

protected Q_SLOTS:
    void on_leveleA_real( DsoReal real);
    void on_leveleB_real( DsoReal real);
    void on_tune( int id );
    void on_leveleA_editing();
    void on_leveleB_editing();

    void on_sweep_auto(   bool b);
    void on_sweep_normal( bool b);
    void on_sweep_single( bool b);

Q_SIGNALS:
    void sig_leveleA_real( DsoReal );
    void sig_leveleB_real( DsoReal );
    void sig_tune( int id );
    void sig_sweep(int value);
};

}
#endif // WNDTRIGCFG_H
