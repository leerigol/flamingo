#include "rwndlacfg.h"
#include "../../com/inputkeypad/inputkeypad.h"
namespace menu_res {

RWndLaCfg::RWndLaCfg( QWidget *parent ) : RModalWnd( parent )
{
    mLtLabelId = 0;
    mHtLabelId = 0;

    mLT = 0;
    mHT = 0;

    mDxOnOff = 0;

    set_attr( wndAttr, mWndAttr, wnd_moveable|wnd_manual_resize );

    addToShowQueue( this );
}

void RWndLaCfg::showEvent( QShowEvent *event )
{
    //! base
    RModalWnd::showEvent( event );

    //! label text
    m_pLabelLT->setText( sysGetString(mLtLabelId,"Threshold") );
    m_pLabelHT->setText( sysGetString(mHtLabelId,"Threshold") );

    //! combox
    formatThreshold();
}
void RWndLaCfg::setupUi( const QString &path,
              const r_meta::CMeta &meta  )
{
    //! create
    for ( int i = 0; i < array_count( m_pCheck ); i++ )
    {
        m_pCheck[i] = new QCheckBox( this );
        Q_ASSERT( NULL != m_pCheck[i] );

        m_pCheck[i]->setText( QString("D%1").arg(i) );
        m_pCheck[i]->setFocusPolicy(Qt::NoFocus);
    }

    m_pCheck70 = new QCheckBox( this );
    Q_ASSERT( NULL != m_pCheck70 );
    m_pCheck70->setText( "D7-D0");
    m_pCheck70->setFocusPolicy(Qt::NoFocus);

    m_pCheck158 = new QCheckBox( this );
    Q_ASSERT( NULL != m_pCheck158 );
    m_pCheck158->setText( "D15-D8");
    m_pCheck158->setFocusPolicy(Qt::NoFocus);

    m_pLabelLT = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelLT );

    m_pLabelHT = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelHT );

    m_pComboxHT = new RComboBox( this );
    Q_ASSERT( NULL != m_pComboxHT );
    m_pComboxHT->setEditable( true );

    m_pComboxLT = new RComboBox( this );
    Q_ASSERT( NULL != m_pComboxLT );
    m_pComboxLT->setEditable( true );

    m_pEdtHT = new RLineEdit( this );
    Q_ASSERT( NULL != m_pEdtHT );
    m_pEdtLT = new RLineEdit( this );
    Q_ASSERT( NULL != m_pEdtLT );

    m_pComboxHT->setFocusPolicy(Qt::NoFocus);
    m_pComboxLT->setFocusPolicy(Qt::NoFocus);
    m_pEdtHT->setFocusPolicy(Qt::NoFocus);
    m_pEdtLT->setFocusPolicy(Qt::NoFocus);

    //! set text
    m_pLabelLT->setText("Threshold");
    m_pLabelHT->setText("Threshold");
    m_pLabelLT->setAlignment(Qt::AlignRight);
    m_pLabelHT->setAlignment(Qt::AlignRight);

    //! combox
    QComboBox *combox[]={ m_pComboxLT,
                        m_pComboxHT };
    QComboBox *pCurComb;
    for( int i = 0; i < array_count(combox); i++ )
    {
        pCurComb = combox[i];
        pCurComb->insertItem( 0,"TTL(1.4V)", 1400000 );

        //! same as the menu threshold string
        pCurComb->insertItem( 1,"CMOS5.0(2.5V)", 2500000 );
        pCurComb->insertItem( 2,"CMOS3.3(1.65V)", 1650000 );
        pCurComb->insertItem( 3,"CMOS2.5(1.25V)", 1250000 );
        pCurComb->insertItem( 4,"CMOS1.8(0.9V)", 900000 );
        pCurComb->insertItem( 5,"ECL(-1.3V)", -1300000 );
        pCurComb->insertItem( 6,"PECL(3.7V)", 3700000 );
        pCurComb->insertItem( 7,"LVDS(1.2V)", 1200000 );
        pCurComb->insertItem( 8,"0.00V", 0 );
    }

    //! placement
    QRect geo;
    QString geoPath = path + "geo/";
    meta.getMetaVal( geoPath + "wnd", geo ); resize( geo.size() );

    getGeo( meta, geoPath + "d70", m_pCheck70 );
    getGeo( meta, geoPath + "d158", m_pCheck158 );
    QString idStr;
    for ( int i = 0; i < array_count(m_pCheck); i++ )
    {
        idStr = QString("d%1").arg(i);

        getGeo( meta, geoPath + idStr, m_pCheck[i] );
    }

    getGeo( meta, geoPath + "llt", m_pLabelLT );
    getGeo( meta, geoPath + "lht", m_pLabelHT );

    getGeo( meta, geoPath + "lcomb", m_pComboxLT );
    getGeo( meta, geoPath + "hcomb", m_pComboxHT );

    getGeo( meta, geoPath + "ledt", m_pEdtLT );
    getGeo( meta, geoPath + "hedt", m_pEdtHT );

    //! style
    QString style;
    meta.getMetaVal( path + "style", style );
    setStyleSheet( style );

    meta.getMetaVal( path + "editstyle", style );
    m_pEdtLT->setStyleSheet(style);
    m_pEdtHT->setStyleSheet(style);

    //! connect
    connect( m_pComboxLT, SIGNAL(currentIndexChanged(int)),
             this, SLOT(on_lcombox_index_changed()) );
    connect( m_pComboxLT,
             SIGNAL(activated(int)),
             this,
             SLOT(on_lcombox_index_changed()));


    connect( m_pComboxHT, SIGNAL(currentIndexChanged(int)),
             this, SLOT(on_hcombox_index_changed()) );

    connect( m_pComboxHT,
             SIGNAL(activated(int)),
             this,
             SLOT(on_hcombox_index_changed()));

    connect( m_pEdtLT, SIGNAL(sig_mouseRelease()),
             this, SLOT(on_lcombox_edit_click()) );
    connect( m_pEdtHT, SIGNAL(sig_mouseRelease()),
             this, SLOT(on_hcombox_edit_click()) );

    for ( int i = 0; i < array_count(m_pCheck); i++ )
    {
        connect( m_pCheck[i], SIGNAL(clicked(bool)),
                 this, SLOT(on_dx_check()) );
    }

    connect( m_pCheck70, SIGNAL(clicked(bool)),
             this, SLOT(on_d70_check()) );
    connect( m_pCheck158, SIGNAL(clicked(bool)),
             this, SLOT(on_d158_check()) );
}

void RWndLaCfg::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);

    RModalWnd::doPaint(e, painter);

    QRect r = rect();
    r.setX(9);
    r.setY(39);
    r.setWidth(geometry().width()-18);
    r.setHeight(40);

    painter.fillRect(r, QColor(34,34,34,255));

    r.moveTo(r.bottomLeft());
    painter.fillRect(r, QColor(21,21,21,255));

    r.moveTo(r.bottomLeft());
    painter.fillRect(r, QColor(34,34,34,255));

    r.moveTo(r.bottomLeft());
    painter.fillRect(r, QColor(21,21,21,255));
}

void RWndLaCfg::on_hcombox_index_changed()
{
    QVariant var = m_pComboxHT->currentData();

    mHT = var.toInt( );
    formatThreshold();
    update();

    emit sig_threshold_changed( mLT, mHT );
}
void RWndLaCfg::on_lcombox_index_changed()
{
    QVariant var = m_pComboxLT->currentData();

    mLT = var.toInt( );
    formatThreshold();
    update();

    emit sig_threshold_changed( mLT, mHT );
}

void RWndLaCfg::on_hcombox_edit_click()
{
    DsoRealGp realGp;

    buildRealGp( realGp, mHT );

    QPoint ptOrg = this->mapToGlobal( QPoint(0,0) );
    InputKeypad::getInstance()->input( ptOrg,
                                       InputKeypad::toKeypadView( Unit_V ),
                                       realGp,
                                       this,
                                       SLOT(on_h_threshold(DsoReal)),
                                       rect().size() );
}
void RWndLaCfg::on_lcombox_edit_click()
{
    DsoRealGp realGp;

    buildRealGp( realGp, mLT );

    QPoint ptOrg = this->mapToGlobal( QPoint(0,0) );
    InputKeypad::getInstance()->input( ptOrg,
                                       InputKeypad::toKeypadView( Unit_V ),
                                       realGp,
                                       this,
                                       SLOT(on_l_threshold(DsoReal)),
                                       rect().size() );
}

void RWndLaCfg::on_h_threshold( DsoReal real )
{
    real.alignBase( E_N6 );

    mHT = real.getInteger();
    formatThreshold();
    update();

    emit sig_threshold_changed( mLT, mHT );
}
void RWndLaCfg::on_l_threshold( DsoReal real )
{
    real.alignBase( E_N6 );

    mLT = real.getInteger();
    formatThreshold();
    update();

    emit sig_threshold_changed( mLT, mHT );
}

void RWndLaCfg::on_d70_check()
{
    if ( m_pCheck70->isChecked() )
    {
        set_attr( quint32, mDxOnOff, (0xff<<d0) );
    }
    else
    {
        unset_attr( quint32, mDxOnOff, (0xff<<d0) );
    }

    setDxOnOff( mDxOnOff );

    emit sig_dx_changed( mDxOnOff );
}
void RWndLaCfg::on_d158_check()
{
    if ( m_pCheck158->isChecked() )
    {
        set_attr( quint32, mDxOnOff, (0xff<<d8) );
    }
    else
    {
        unset_attr( quint32, mDxOnOff, (0xff<<d8) );
    }

    setDxOnOff( mDxOnOff );

    emit sig_dx_changed( mDxOnOff );
}

void RWndLaCfg::on_dx_check()
{
    //! for each check
    quint32 dxOnOff;
    dxOnOff = 0;
    for ( int i = 0; i < array_count(m_pCheck); i++ )
    {
        if ( m_pCheck[i]->isChecked() )
        {
            set_bit(dxOnOff, (i + d0) );
        }
        else
        {
            unset_bit(dxOnOff, (i + d0) );
        }
    }

    setDxOnOff( dxOnOff );

    emit sig_dx_changed( mDxOnOff );
}

void RWndLaCfg::formatThreshold()
{
    if ( !mbSetupUi ) return;

    QString str;
    DsoReal realT( mLT, E_N6 );
    gui_fmt::CGuiFormatter::format( str, realT, fmt_def, Unit_V );
    m_pComboxLT->setEditText( str );
    m_pEdtLT->setText( str );

    realT.setVal( mHT, E_N6 );
    gui_fmt::CGuiFormatter::format( str, realT, fmt_def, Unit_V );
    m_pComboxHT->setEditText( str );
    m_pEdtHT->setText( str );
}

void RWndLaCfg::buildRealGp( DsoRealGp &gp, int thre )
{
    gp.setValue( thre, 20000000, -20000000, 0, E_N6 );
}

void RWndLaCfg::setLabelId( int lId, int hId )
{
    mLtLabelId = lId;
    mHtLabelId = hId;
}

void RWndLaCfg::setThreshold( int lT, int hT )
{
    mLT = lT;
    mHT = hT;

    formatThreshold();

    update();
}

void RWndLaCfg::setDxOnOff( quint32 dxOnoff )
{
    mDxOnOff = dxOnoff;

    if ( !mbSetupUi ) return;

    //! d0..d15
    for( int i = d0; i <= d15; i++ )
    {
        m_pCheck[ i - d0]->setChecked( get_bit(mDxOnOff, i) );
    }

    //! now for hw gp
    m_pCheck70->setChecked( is_attr( mDxOnOff, (0xff<<d0)) );
    m_pCheck158->setChecked( is_attr( mDxOnOff, (0xff<<d8)) );

    update();
}

}

