#include "wndtrigcfg.h"
namespace menu_res  {

wndTrigCfg::wndTrigCfg(QWidget *parent)
    : menu_res::RModalWnd( parent )
{
     m_pLabTitle     = NULL;
     m_pLabLevelA    = NULL;
     m_pLabLevelB    = NULL;
     m_pEditLevelA   = NULL;
     m_pEditLevelB   = NULL;

     m_pBtnLevelAAdd = NULL;
     m_pBtnLevelASub = NULL;
     m_pBtnLevelBAdd = NULL;
     m_pBtnLevelBSub = NULL;

     m_pRadButAtuo   = NULL;
     m_pRadButSingle = NULL;
     m_pRadButNormal = NULL;

     m_pSigMap       = NULL;

     m_titleId       = 0;
     m_leveleAId     = 0;
     m_leveleBId     = 0;

     m_leveleUnit    = Unit_V;
     m_LevelFmt      = dso_view_format(fmt_float,fmt_width_3);
     set_attr( wndAttr, mWndAttr, wnd_moveable|wnd_manual_resize );

     addToShowQueue( this );
}

void wndTrigCfg::showEvent(QShowEvent *event)
{
    //! base
    RModalWnd::showEvent( event );

    //! format each value
    Q_ASSERT( NULL != m_pLabTitle );
}

void wndTrigCfg::setupUi(const QString &path, const r_meta::CMeta &meta)
{
    m_path = path;
    m_meta = meta;

    //! create
    m_pLabTitle = new QLabel(this);
    Q_ASSERT( NULL != m_pLabTitle );

    m_pLabLevelA = new QLabel(this);
    Q_ASSERT( NULL != m_pLabLevelA );

    m_pLabLevelB = new QLabel(this);
    Q_ASSERT( NULL != m_pLabLevelB );

    m_pEditLevelA = new menu_res::RLineEdit( this );
    Q_ASSERT( NULL != m_pEditLevelA );

    m_pEditLevelB = new menu_res::RLineEdit( this );
    Q_ASSERT( NULL != m_pEditLevelB );

    m_pBtnLevelAAdd = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnLevelAAdd );

    m_pBtnLevelASub = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnLevelASub );

    m_pBtnLevelBAdd = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnLevelBAdd );

    m_pBtnLevelBSub = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnLevelBSub );

    m_pRadButAtuo   = new QRadioButton(this);
    Q_ASSERT( NULL != m_pRadButAtuo );
    m_pRadButSingle = new QRadioButton(this);
    Q_ASSERT( NULL != m_pRadButSingle );
    m_pRadButNormal = new QRadioButton(this);
    Q_ASSERT( NULL != m_pRadButNormal );

    m_pRadButAtuo->setChecked(true);

    m_pRadButAtuo->setFocusPolicy(Qt::NoFocus);
    m_pRadButSingle->setFocusPolicy(Qt::NoFocus);
    m_pRadButNormal->setFocusPolicy(Qt::NoFocus);

    //! apply ui
    getGeo( meta, path + "geo/wnd", this );
    getGeo( meta, path + "geo/title",        m_pLabTitle );
    getGeo( meta, path + "geo/level_a_lab",  m_pLabLevelA );
    getGeo( meta, path + "geo/level_b_lab",  m_pLabLevelB );

    getGeo( meta, path + "geo/level_a_edit", m_pEditLevelA );
    getGeo( meta, path + "geo/level_b_edit", m_pEditLevelB );

    getGeo( meta, path + "geo/level_a_add",  m_pBtnLevelAAdd );
    getGeo( meta, path + "geo/level_a_sub",  m_pBtnLevelASub );

    getGeo( meta, path + "geo/level_b_add",  m_pBtnLevelBAdd );
    getGeo( meta, path + "geo/level_b_sub",  m_pBtnLevelBSub );

    getGeo( meta, path + "geo/sweep_auto",    m_pRadButAtuo   );
    getGeo( meta, path + "geo/sweep_single",  m_pRadButSingle );
    getGeo( meta, path + "geo/sweep_normal",  m_pRadButNormal );

    //! images
    QString str;
    //! style
    meta.getMetaVal( path + "style/wnd", str );
    this->setStyleSheet( str );

    //! connection
    connect( m_pEditLevelA, SIGNAL(sig_mouseRelease()),
             this, SLOT(on_leveleA_editing()) );
    connect( m_pEditLevelB, SIGNAL(sig_mouseRelease()),
             this, SLOT(on_leveleB_editing()) );

    m_pSigMap = new QSignalMapper( this );
    Q_ASSERT( NULL != m_pSigMap );

    connect( m_pBtnLevelAAdd, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );
    connect( m_pBtnLevelBAdd, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );
    connect( m_pBtnLevelASub, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );
    connect( m_pBtnLevelBSub, SIGNAL(released()),
             m_pSigMap, SLOT(map()) );

    connect( m_pRadButAtuo, SIGNAL(clicked(bool)),
             this, SLOT(on_sweep_auto(bool)) );
    connect( m_pRadButSingle, SIGNAL(clicked(bool)),
             this, SLOT(on_sweep_single(bool)) );
    connect( m_pRadButNormal, SIGNAL(clicked(bool)),
             this, SLOT(on_sweep_normal(bool)) );

    m_pSigMap->setMapping( m_pBtnLevelAAdd, tune_level_a_add);
    m_pSigMap->setMapping( m_pBtnLevelBAdd, tune_level_b_add );
    m_pSigMap->setMapping( m_pBtnLevelASub, tune_level_a_sub );
    m_pSigMap->setMapping( m_pBtnLevelBSub, tune_level_b_sub );

    connect( m_pSigMap, SIGNAL(mapped(int)),
             this, SLOT(on_tune(int)) );
}

int wndTrigCfg::titleId() const
{
    return m_titleId;
}

void wndTrigCfg::setTitleId(int titleId)
{
    m_titleId = titleId;
}

int wndTrigCfg::leveleAId() const
{
    return m_leveleAId;
}

void wndTrigCfg::setLeveleAId(int leveleAId)
{
    m_leveleAId = leveleAId;
    update();
}

int wndTrigCfg::leveleBId() const
{
    return m_leveleBId;
}

void wndTrigCfg::setLeveleBId(int leveleBId)
{
    m_leveleBId = leveleBId;
    update();
}

DsoReal wndTrigCfg::leveleA() const
{
    return m_leveleA;
}

void wndTrigCfg::setLeveleA(const DsoReal &leveleA)
{
    m_leveleA = leveleA;
    if ( !mbSetupUi ) return;

    QString str;
    gui_fmt::CGuiFormatter::format( str, m_leveleA, m_LevelFmt, m_leveleUnit );
    m_pEditLevelA->setText( str );
    update();
}

DsoReal wndTrigCfg::leveleB() const
{
    return m_leveleB;
}

void wndTrigCfg::setLeveleB(const DsoReal &leveleB)
{
    m_leveleB = leveleB;
    if ( !mbSetupUi ) return;

    QString str;
    gui_fmt::CGuiFormatter::format( str, m_leveleB, m_LevelFmt, m_leveleUnit );
    m_pEditLevelB->setText( str );
    update();
}

DsoRealGp wndTrigCfg::leveleAGp() const
{
    return m_leveleAGp;
}

void wndTrigCfg::setLeveleAGp(const DsoRealGp &leveleAGp)
{
    m_leveleAGp = leveleAGp;
    setLeveleA(leveleAGp.vNow);
}

DsoRealGp wndTrigCfg::leveleBGp() const
{
    return m_leveleBGp;
}

void wndTrigCfg::setLeveleBGp(const DsoRealGp &leveleBGp)
{
    m_leveleBGp = leveleBGp;
    setLeveleB(leveleBGp.vNow);
}

Unit wndTrigCfg::leveleUnit() const
{
    return m_leveleUnit;
}

void wndTrigCfg::setLeveleUnit(const Unit &leveleUnit)
{
    m_leveleUnit = leveleUnit;
    update();
}

void wndTrigCfg::setTwoLevel(bool TwoLevel)
{
    m_pLabTitle->setFont(qApp->font());
    m_pLabLevelA->setFont(qApp->font());
    m_pLabLevelB->setFont(qApp->font());

    m_pLabLevelB->setVisible(    TwoLevel);
    m_pBtnLevelBAdd->setVisible( TwoLevel);
    m_pBtnLevelBSub->setVisible( TwoLevel);
    m_pEditLevelB->setVisible(   TwoLevel);

    //! label
    m_pRadButAtuo->setText(   "Auto"   ) ;
    m_pRadButNormal->setText( "Normal" ) ;
    m_pRadButSingle->setText( "Single" ) ;
    m_pLabTitle->setText (   sysGetString(STRING_TRIGGER_SETTING,   "Trigger Setting") );
    m_pLabLevelA->setText(   sysGetString(STRING_TRIGGER_LEVEL_A,   "Level A" )        );
    m_pLabLevelB->setText(   sysGetString(STRING_TRIGGER_LEVEL_B,   "Level B" )        );


    QSize wndSize;
    if(TwoLevel)
    {
         m_meta.getMetaVal( m_path + "geo/wnd_level_two", wndSize );
    }
    else
    {
        m_pLabLevelA->setText( sysGetString(STRING_TRIGGER_LEVEL,   "Level" ) );
        m_meta.getMetaVal( m_path + "geo/wnd_level_one", wndSize );
    }
    this->resize(wndSize);

    m_TwoLevel = TwoLevel;
    update();
}

void wndTrigCfg::setSweep(int sweep)
{
    switch(sweep)
    {
    case 0:
        m_pRadButAtuo->setChecked(true);
        break;
    case 1:
        m_pRadButNormal->setChecked(true);
        break;
    case 2:
        m_pRadButSingle->setChecked(true);
        break;
    default:
        Q_ASSERT(false);
        break;
    }
    update();
}

void wndTrigCfg::on_leveleA_real(DsoReal real)
{
    setLeveleA(real);
    emit sig_leveleA_real(real);
}

void wndTrigCfg::on_leveleB_real(DsoReal real)
{
    setLeveleB(real);
    emit sig_leveleB_real(real);
}

void wndTrigCfg::on_tune(int id)
{
    emit sig_tune( id );
}

void wndTrigCfg::on_leveleA_editing()
{
    QPoint ptOrg = this->mapToGlobal( QPoint(0,0) );
    InputKeypad::getInstance()->input( ptOrg,
                                       InputKeypad::toKeypadView( m_leveleUnit ),
                                       m_leveleAGp,
                                       this,
                                       SLOT(on_leveleA_real(DsoReal)),
                                       rect().size() );
}

void wndTrigCfg::on_leveleB_editing()
{
    QPoint ptOrg = this->mapToGlobal( QPoint(0,0) );
    InputKeypad::getInstance()->input( ptOrg,
                                       InputKeypad::toKeypadView( m_leveleUnit ),
                                       m_leveleBGp,
                                       this,
                                       SLOT(on_leveleB_real(DsoReal)),
                                       rect().size() );
}

void wndTrigCfg::on_sweep_auto(bool b)
{
    if(b)
    { emit sig_sweep(0); }
}

void wndTrigCfg::on_sweep_normal(bool b)
{
    if(b)
    { emit sig_sweep(1); }
}

void wndTrigCfg::on_sweep_single(bool b)
{
    if(b)
    { emit sig_sweep(2); }
}

}
