#ifndef WNDCHCFG_H
#define WNDCHCFG_H

#include <QtWidgets>
#include "../../menu/wnd/rinfownd.h"
#include "../../menu/wnd/rmodalwnd.h"
#include "../../widget/rimagebutton.h"
#include "../../widget/rlineedit.h"

namespace menu_res  {

class WndChCfg : public RModalWnd
{
    Q_OBJECT
public:
    enum tuneId
    {
        tune_scale_add,
        tune_scale_sub,
        tune_offset_add,
        tune_offset_sub,
    };

public:
    WndChCfg( QWidget *parent );

protected:
    virtual void showEvent( QShowEvent *event );
    virtual void setupUi( const QString &path,
                  const r_meta::CMeta &meta  );

Q_SIGNALS:
    void sig_scale_real( DsoReal );
    void sig_offset_real( DsoReal );

    void sig_tune( int id );

protected Q_SLOTS:
    void on_scale_real( DsoReal );
    void on_offset_real( DsoReal );

    void on_scale_editing();
    void on_offset_editing();

    void on_tune( int id );

public:
    void setLabelId( int scaleId, int offsetId );
    void setScale( const DsoReal &scale );
    DsoReal getScale();

    void setOffset( const DsoReal &offset );
    DsoReal getOffset();

    void setScaleGp( const DsoRealGp &gp );
    void setOffsetGp( const DsoRealGp &gp );

    void setUnit( Unit unit );
    Unit getUnit();

    void setScaleFmt( DsoType::DsoViewFmt fmt );
    void setOffsetFmt( DsoType::DsoViewFmt fmt );

public:
    //! uis
    QLabel *m_pLabScale, *m_pLabOffset;
    menu_res::RLineEdit *m_pEditScale, *m_pEditOffset;

    QToolButton *m_pBtnScaleAdd, *m_pBtnScaleSub;
    QToolButton *m_pBtnOffsetAdd, *m_pBtnOffsetSub;

    QSignalMapper *m_pSigMap;
protected:
    //! varialbles
    int mLabScaleId, mLabOffsetId, mLabTitleId;
    DsoReal mScale, mOffset;
    Unit mUnit;

    DsoType::DsoViewFmt mScaleFmt, mOffsetFmt;

    DsoRealGp mScaleGp, mOffsetGp;
};

}

#endif // WNDCHCFG_H
