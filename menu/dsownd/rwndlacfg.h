#ifndef RWNDLACFG_H
#define RWNDLACFG_H

#include <QtWidgets>

#include "../../menu/wnd/rinfownd.h"
#include "../../menu/wnd/rmodalwnd.h"
#include "../../widget/rimagebutton.h"
#include "../../widget/rlineedit.h"

#include "../../widget/rcombobox.h"

namespace menu_res  {

class RWndLaCfg : public RModalWnd
{
    Q_OBJECT
public:
    RWndLaCfg( QWidget *parent );

protected:
    virtual void showEvent( QShowEvent *event );
    virtual void setupUi( const QString &path,
                  const r_meta::CMeta &meta  );
    virtual void paintEvent(QPaintEvent *e);

Q_SIGNALS:
    void sig_threshold_changed( int lt, int ht );
    void sig_dx_changed( quint32 dx );


protected Q_SLOTS:
    void on_hcombox_index_changed();
    void on_lcombox_index_changed();

    void on_hcombox_edit_click();
    void on_lcombox_edit_click();

    void on_h_threshold( DsoReal );
    void on_l_threshold( DsoReal );

    void on_d70_check();
    void on_d158_check();
    void on_dx_check();

protected:
    void formatThreshold();
    void buildRealGp( DsoRealGp &gp, int thre );

public:
    void setLabelId( int lId, int hId );
    void setThreshold( int lT, int hT );
    void setDxOnOff( quint32 dxOnoff );

protected:
    int mLtLabelId, mHtLabelId;
    int mLT, mHT;
    quint32 mDxOnOff;

protected:
    QCheckBox *m_pCheck[16];
    QCheckBox *m_pCheck70, *m_pCheck158;
    QLabel *m_pLabelHT, *m_pLabelLT;
    RComboBox *m_pComboxHT, *m_pComboxLT;

    RLineEdit *m_pEdtHT, *m_pEdtLT;
};

}

#endif // RWNDLACFG_H
