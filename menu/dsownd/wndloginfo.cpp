#include "wndloginfo.h"

namespace menu_res  {

WndLogInfo::WndLogInfo()
{
    m_pDetailList = NULL;
    m_pBtnClear = NULL;

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void WndLogInfo::setupUi( const QString &path,
                          const r_meta::CMeta &meta )
{
    //! create widgets
    m_pDetailList = new QListWidget(this);
    Q_ASSERT( NULL != m_pDetailList );
    setBarStyle();

    m_pBtnClear = new menu_res::RButton(this);
    Q_ASSERT( NULL != m_pBtnClear );
    m_pBtnClear->setText("clear");
    m_pBtnExport = new menu_res::RButton(this);
    Q_ASSERT( NULL != m_pBtnExport );
    m_pBtnExport->setText("export");

    //! set style
    QRect geo;

    meta.getMetaVal( path + "geo", geo );
    resize( geo.size() );

    getGeo( meta, path + "info/geo", m_pDetailList );
    getStyle( meta, path + "info/style", m_pDetailList );

    getGeo( meta, path + "clear/geo", m_pBtnClear );
    getStyle( meta, path + "clear/style", m_pBtnClear );

    getGeo( meta, path + "export/geo", m_pBtnExport );
    getStyle( meta, path + "export/style", m_pBtnExport );

    //! build connect
    connect( m_pBtnClear, SIGNAL(clicked(bool)),
             this, SLOT(on_clear_clicked()) );
}

void WndLogInfo::on_clear_clicked()
{
    m_pDetailList->clear();
}

void WndLogInfo::logInfo( const QString &str )
{
    Q_ASSERT( m_pDetailList != NULL );
    m_pDetailList->addItem( str );
}
void WndLogInfo::logInfo( const QStringList &strList )
{
    Q_ASSERT( m_pDetailList != NULL );
    m_pDetailList->addItems( strList );
}
void WndLogInfo::logInfo( const QStringList *ptr )
{
    Q_ASSERT( m_pDetailList != NULL );
    m_pDetailList->addItems( *ptr );
}

void WndLogInfo::setBarStyle()
{
    m_pDetailList->verticalScrollBar()->setStyleSheet("QScrollBar:vertical\
    {\
      width:16px;\
      background:rgba(50,50,50,100%);\
      margin:0px,0px,0px,0px;\
    }\
      QScrollBar::handle:vertical\
    {\
      width:16px;\
      background:rgba(100,100,100,50%);\
      border-radius:4px;  \
      min-height:60;\
    }\
    QScrollBar::handle:vertical:hover\
    {\
      width:16px;\
      background:rgba(100,100,100,25%);  \
      border-radius:4px;\
      min-height:60;\
    }\
    QScrollBar::add-line:vertical  \
    {\
      height:9px;width:16px;\
      border:0px;\
      subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical  \
    {\
      height:9px;width:16px;\
      border:0px;\
      subcontrol-position:top;\
    }\
    QScrollBar::add-line:vertical:hover \
    {\
      height:9px;width:16px;\
      border:0px;\
      subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical:hover \
    {\
      height:9px;width:16px;\
      border:0px;\
      subcontrol-position:top;\
    }\
    QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical   \
    {\
      background:rgba(0,0,0,10%);\
      border-radius:4px;\
    }");
}

}
