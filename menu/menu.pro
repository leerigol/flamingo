TEMPLATE= lib
QT       += core
QT       += widgets
QT       += gui

TARGET = ../lib$$(PLATFORM)/menu

CONFIG += static
CONFIG += c++11

DEFINES += _DSO_KEYBOARD

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
DEFINES -= _DSO_KEYBOARD
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

keybd = $$(DSO_KEYBOARD)
equals(keybd, yes){
DEFINES += _DSO_KEYBOARD
message("physical kb")
}

dbg = $$(_DEBUG)
equals(dbg, _DEBUG){
DEFINES += _DEBUG
}

#DEFINES += _DEBUG

HEADERS += \
    arch/rdsoui.h \
    arch/rmenubuilder.h \
    arch/rmenures.h \
    arch/rmenu.h \
    arch/rmenupage.h \
    arch/rservmenu.h \
    arch/menutab.h \
    menuwidget/rcontrol.h \
    menuwidget/rcomboxcheck.h \
    menuwidget/rlabel_i_f_f_knob.h \
    menuwidget/rcombox.h \
    menuwidget/rinvertbutton.h \
    menuwidget/rpushbutton.h \
    menuwidget/rlabel_n_s_s.h \
    menuwidget/rlabel_icon.h \
    menuwidget/rchildbutton.h \
    menuwidget/rlabel_i_i_f.h \
    menuwidget/rseekbar.h \
    menuwidget/ripedit.h \
    menuwidget/rtimeedit.h \
    menuwidget/rcombox_iif.h \
    menuwidget/rdateedit.h \
    menuwidget/rpopupview.h \
    menuwidget/rpopupitem.h \
    menuwidget/rdropdowncombox_iif.h \
    menuwidget/rtextbox.h \
    menuwidget/ckeypadcontext.h \
    menuwidget/rmenuwidget.h \
    menuwidget/rlitecombox.h \
    menuwidget/rspace.h \
    draw/rpainter.h \
    rmenu_cfg.h \
    rmenus.h \
    dsowidget/rdsodragdrop.h

SOURCES += \
    arch/rdsoui.cpp \
    arch/rmenubuilder.cpp \
    arch/rmenures.cpp \
    arch/rmenu.cpp \
    arch/rmenupage.cpp \
    arch/rservmenu.cpp \
    arch/menutab.cpp \
    menuwidget/rpushbutton.cpp \
    menuwidget/rlabel_i_i_f.cpp \
    menuwidget/rcontrol.cpp \
    menuwidget/rcombox_iif.cpp \
    menuwidget/rlabel_i_f_f_knob.cpp \
    menuwidget/rcomboxcheck.cpp \
    menuwidget/rcombox.cpp \
    menuwidget/rlabel_n_s_s.cpp \
    menuwidget/rchildbutton.cpp \
    menuwidget/rinvertbutton.cpp \
    menuwidget/rlabel_icon.cpp \
    menuwidget/rtimeedit.cpp \
    menuwidget/rdateedit.cpp \
    menuwidget/rseekbar.cpp \
    menuwidget/ripedit.cpp \
    menuwidget/rpopupview.cpp \
    menuwidget/rpopupitem.cpp \
    menuwidget/rdropdowncombox_iif.cpp \
    menuwidget/rtextbox.cpp \
    menuwidget/ckeypadcontext.cpp \
    menuwidget/rmenuwidget.cpp \
    menuwidget/rlitecombox.cpp \
    menuwidget/rspace.cpp \
    draw/rpainter.cpp



