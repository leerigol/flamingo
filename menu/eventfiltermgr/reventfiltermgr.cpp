#include "reventfiltermgr.h"
#include "../../include/dsodbg.h"
REventFilterMgr *REventFilterMgr::m_instance=NULL;

QWidget *REventFilterMgr::findOwner( int key )
{
    if ( m_instance == NULL )
    { return NULL; }

    return m_instance->findWidget( key );
}

void REventFilterMgr::rst()
{
    Q_ASSERT( NULL != m_instance );

    foreach( IEventFilter *pFilter, m_instance->mFilterList )
    {
        Q_ASSERT( NULL != pFilter );

        if ( pFilter->isWnd() )
        {
            pFilter->uninstall();
        }
    }
}

REventFilterMgr::REventFilterMgr()
{
    Q_ASSERT( REventFilterMgr::m_instance == NULL );

    REventFilterMgr::m_instance = this;
}

void REventFilterMgr::install( IEventFilter *pFilter )
{   
    Q_ASSERT( NULL != pFilter );

    //! top is wnd
    if ( mFilterList.size() > 0 && mFilterList[0]->isWnd() )
    {
        installOnWndTop( pFilter );
    }
    else
    {
        installNormal( pFilter );
    }

    for ( int i = 1; i < mFilterList.size(); i++ )
    {
        Q_ASSERT( NULL != mFilterList[i] );

        mFilterList[i]->on_changed();
    }

    LOG_DBG();
    dbgShow();
}

//! top is wnd
void REventFilterMgr::installOnWndTop( IEventFilter *pFilter )
{
    Q_ASSERT( NULL != pFilter );
    QObject *pObj;

    if ( mFilterList.contains( pFilter ) )
    {
        //! wnd
        if ( pFilter->isWnd() )
        {
            mFilterList.removeAll(pFilter);
            mFilterList.prepend( pFilter );

            pObj = dynamic_cast< QObject*>( pFilter );
            Q_ASSERT( NULL != pObj );
            qApp->removeEventFilter( pObj );
        }
        //! not wnd
        else
        {
            mFilterList.removeAll(pFilter);
            mFilterList.append( pFilter );

            //! remove all
            foreach( IEventFilter *pItem, mFilterList )
            {
                pObj = dynamic_cast< QObject*>( pItem );
                Q_ASSERT( NULL != pObj );
                qApp->removeEventFilter( pObj );
            }
        }
    }
    else
    {
        //! wnd
        if ( pFilter->isWnd() )
        {
            mFilterList.prepend( pFilter );
        }
        else
        {
            mFilterList.append( pFilter );
        }
    }

    //! reinstall form bottom to top
    QListIterator<IEventFilter *> iter(mFilterList);
    iter.toBack();
    while( iter.hasPrevious() )
    {
        pObj = dynamic_cast< QObject*>( iter.previous() );
        Q_ASSERT( NULL != pObj );
        qApp->installEventFilter( pObj );
    }
}
void REventFilterMgr::installNormal( IEventFilter *pFilter )
{
    Q_ASSERT( NULL != pFilter );
    QObject *pObj;

    //! add to head
    if ( mFilterList.contains( pFilter ) )
    {
        mFilterList.removeAll(pFilter);
        mFilterList.prepend( pFilter );

        pObj = dynamic_cast< QObject*>( pFilter );
        Q_ASSERT( NULL != pObj );
        qApp->removeEventFilter( pObj );
        qApp->installEventFilter( pObj );
    }
    else
    {
        mFilterList.prepend( pFilter );

        pObj = dynamic_cast< QObject*>( pFilter );
        Q_ASSERT( NULL != pObj );
        qApp->installEventFilter( pObj );
    }
}

void REventFilterMgr::uninstall( IEventFilter *pFilter )
{
    if ( !mFilterList.contains(pFilter) )
    { return; }

    QObject *pObj;

    //! remove
    if ( mFilterList.contains( pFilter ) )
    {
        mFilterList.removeAll(pFilter);

        pObj = dynamic_cast< QObject*>( pFilter );
        Q_ASSERT( NULL != pObj );
        qApp->removeEventFilter( pObj );
    }

    //! leaving changed
    for ( int i = 0; i < mFilterList.size(); i++ )
    {
        Q_ASSERT( NULL != mFilterList[i] );

        mFilterList[i]->on_changed();
    }

    LOG_DBG();
    dbgShow();
}

//! set to top
void REventFilterMgr::setTop( IEventFilter *pFilter )
{
    Q_ASSERT( NULL != pFilter );

    install( pFilter );
}

bool REventFilterMgr::isTop( IEventFilter *pFilter )
{
    Q_ASSERT( NULL != pFilter );

    if ( mFilterList.size() < 1 )
    {
        return false;
    }
    else
    {
        return mFilterList[0] == pFilter;
    }
}

QWidget *REventFilterMgr::findWidget( int key )
{
    QWidget *pWig;
    foreach( IEventFilter *pFilter, mFilterList )
    {
        Q_ASSERT( NULL != pFilter );

        pWig = pFilter->findWidget( key );
        if ( NULL != pWig )
        {
            return pWig;
        }
    }

    return NULL;
}

void REventFilterMgr::dbgShow()
{
//    qDebug()<<"---------------Begin";
//    foreach( IEventFilter *pFilter, mFilterList )
//    {
//        Q_ASSERT( NULL != pFilter );
//        qDebug()<<"--"<<pFilter->getName();
//    }
//    qDebug()<<"---------------Over";
}
