#ifndef REVENTFILTERMGR_H
#define REVENTFILTERMGR_H

#include <QtWidgets>
#include "ieventfilter.h"

class REventFilterMgr
{
public:
    static QWidget *findOwner( int key );
    static void rst();

protected:
    static REventFilterMgr *m_instance;

public:
    REventFilterMgr();

public:
    void install( IEventFilter *pFilter );
    void installOnWndTop( IEventFilter *pFilter );
    void installNormal( IEventFilter *pFilter );

    void uninstall( IEventFilter *pFilter );

    void setTop( IEventFilter *pFilter );

    bool isTop( IEventFilter *pFilter );

    QWidget *findWidget( int key );
protected:
    void dbgShow();
protected:
    QList< IEventFilter *> mFilterList;
};
#endif // REVENTFILTERMGR_H
