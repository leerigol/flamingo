#include "ieventfilter.h"

#include "reventfiltermgr.h"

//! event filter manager
static REventFilterMgr _rEventFilterMgr;

IEventFilter::IEventFilter()
{
    m_pMgr = &_rEventFilterMgr;

    mName = QString("filter:%1").arg( (int)rand() );

    mbWnd = true;
}

IEventFilter::~IEventFilter()
{
}

void IEventFilter::on_changed()
{}


void IEventFilter::install()
{
    Q_ASSERT( NULL != m_pMgr );

    m_pMgr->install( this );
}
void IEventFilter::uninstall()
{
    Q_ASSERT( NULL != m_pMgr );

    m_pMgr->uninstall( this );
}
void IEventFilter::toTop()
{
    m_pMgr->setTop( this );
}
bool IEventFilter::isTop()
{
    return m_pMgr->isTop( this );
}

QWidget *IEventFilter::findWidget( int key )
{
    if ( mKeyMaps.contains(key) )
    {
        return mKeyMaps[key];
    }
    else
    {
        return NULL;
    }
}
REventFilterMgr *IEventFilter::getMgr()
{
    return m_pMgr;
}

void IEventFilter::setName( const QString &name )
{
    mName = name;
}
QString IEventFilter::getName()
{ return mName; }

bool IEventFilter::isWnd()
{ return mbWnd; }

void IEventFilter::attachKey( int key, QWidget *pWidget )
{
    mKeyMaps[key] = pWidget;
}

void IEventFilter::detachKey( int key )
{
    mKeyMaps.remove( key );
}

