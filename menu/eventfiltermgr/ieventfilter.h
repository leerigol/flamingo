#ifndef IEVENTFILTER_H
#define IEVENTFILTER_H

#include <QtWidgets>

class REventFilterMgr;

class IEventFilter
{
public:
    IEventFilter();
    ~IEventFilter();

public:
    virtual void on_changed();

public:
    void install();
    void uninstall();
    void toTop();

    bool isTop();

    QWidget *findWidget( int key );
    REventFilterMgr *getMgr();

    void setName( const QString &name );
    QString getName();

    bool isWnd();

public:
    void attachKey( int key, QWidget *pWidget );
    void detachKey( int key );

protected:
    QMap< int , QWidget * > mKeyMaps;
    REventFilterMgr *m_pMgr;

    QString mName;
    bool mbWnd;
};

#endif // IEVENTFILTER_H
