#ifndef CAPPMENUTEST_H
#define CAPPMENUTEST_H

#include "../capp.h"
#include "../../menu/rmenus.h"

class CWndTest : public menu_res::uiWnd
{
    Q_OBJECT

public:
    CWndTest( CApp *pApp,
              int id,
              QWidget *parent = 0
              );

protected:
    virtual void keyPressEvent( QKeyEvent * );
    virtual void keyReleaseEvent( QKeyEvent * );

    virtual bool isServiceActive();
    virtual void serviceActiveIn();

Q_SIGNALS:
    void sig_close( int id );

private Q_SLOTS:
    void on_close();

public:
    void attachKey( int key );

private:
//    QPushButton *pBtn;
    QLabel *pBtn;
    int mId;

    CApp *m_pApp;


};

class CAppMenuTest : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppMenuTest();

public:
    void setupUi( CAppMain */*pMain*/ )
    {
    }
    virtual void retranslateUi(){}
    virtual void buildConnection(){}

private Q_SLOTS:
    void on_close( int id );

protected:
    void on_button_two();
    void on_button_one();
    void on_startup();

private:
    CWndTest *pWnd1, *pWnd2;

};

#endif // CAPPMENUTEST_H
