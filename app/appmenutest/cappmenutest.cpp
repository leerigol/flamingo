#include "cappmenutest.h"

#include "../../service/servmenutest/servmenutest.h"
#include "../../menu/event/reventfilter.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppMenuTest )
start_of_entry()

//on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_TWO, &CAppMenuTest::on_button_two),
//on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE, &CAppMenuTest::on_button_one),

//! system
//on_set_void_void( CMD_SERVICE_ENTER_ACTIVE, &CAppMenuTest::enterActive ),
//on_set_void_void( CMD_SERVICE_RE_ENTER_ACTIVE, &CAppMenuTest::reEnterActive ),
//on_set_void_void( CMD_SERVICE_EXIT_ACTIVE, &CAppMenuTest::exitActive ),

on_set_void_void( CMD_SERVICE_STARTUP, &CAppMenuTest::on_startup),
end_of_entry()

CAppMenuTest::CAppMenuTest()
{
    mName = "menutest";

    mservName = mName;

    pWnd1 = NULL;
    pWnd2 = NULL;

    menu_res::RLabel_i_f_f_knob label;
}

void CAppMenuTest::on_close( int id )
{
    if ( id == 1 )
    {
        pWnd1->close();
        delete pWnd1;
        pWnd1 = NULL;
    }
    else if ( id == 2 )
    {
        pWnd2->close();
        delete pWnd2;
        pWnd2 = NULL;
    }
}

void CAppMenuTest::on_button_two()
{
    if ( pWnd1 == NULL )
    {
        pWnd1 = new CWndTest( this, 1, sysGetMainWindow() );
        pWnd1->attachKey( R_Pkey_CH1 );
        pWnd1->attachKey( R_Pkey_CH2 );
        pWnd1->attachKey( R_Pkey_CH3 );
        pWnd1->attachKey( R_Pkey_CH4 );
        pWnd1->load();

        connect( pWnd1, SIGNAL(sig_close(int)), this, SLOT(on_close(int)) );

        pWnd1->setGeometry( 0,0, 100, 100 );
        pWnd1->setObjectName("wnd1");
    }

    pWnd1->show();
    pWnd1->setActive();

    qDebug()<<__FUNCTION__;
}

void CAppMenuTest::on_button_one()
{
    if ( pWnd2 == NULL )
    {
        pWnd2 = new CWndTest( this, 2, sysGetMainWindow() );
        pWnd2->attachKey( R_Pkey_CH3 );
        pWnd2->attachKey( R_Pkey_CH4 );

        pWnd2->addTuneKey();
        pWnd2->load();

        connect( pWnd2, SIGNAL(sig_close(int)), this, SLOT(on_close(int)) );

        pWnd2->setGeometry( 200,200, 300, 100 );
        pWnd2->setObjectName("wnd2");
    }

    pWnd2->show();
    pWnd2->setActive();

    qDebug()<<__FUNCTION__;
}

void CAppMenuTest::on_startup()
{
    //qDebug()<<__FUNCTION__<<__LINE__;
}

CWndTest::CWndTest( CApp *pApp,
                    int id,
                    QWidget *parent
                     )
                    : menu_res::uiWnd(parent, pApp->getuiWndMgr() )
{
    Q_ASSERT( pApp != NULL );

    m_pApp = pApp;

//    pBtn = new QPushButton( this );
//    pBtn->setText( QString("close%1").arg(id) );

//    connect( pBtn, SIGNAL(clicked(bool)),
//             this, SLOT(on_close()) );

    pBtn = new QLabel(this);
    pBtn->setText( QString("t%1").arg(id) );

    mId = id;
}

void CWndTest::keyPressEvent( QKeyEvent * evt )
{
    qDebug()<<evt->key()<<"press"<<mId;
}
void CWndTest::keyReleaseEvent( QKeyEvent * evt )
{
    qDebug()<<evt->key()<<"release"<<mId;
}

bool CWndTest::isServiceActive()
{
    Q_ASSERT( NULL != m_pApp );

    return m_pApp->isServiceActive();
}
void CWndTest::serviceActiveIn()
{
    Q_ASSERT( NULL != m_pApp );

    m_pApp->serviceActiveIn();
}

void CWndTest::on_close()
{
    unload();

    hide();

    emit sig_close( mId );
}

void CWndTest::attachKey( int key )
{
    mKeyList.append( key );
    keyChanged();
}


