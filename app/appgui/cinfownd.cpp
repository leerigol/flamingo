#include "cinfownd.h"

CInfoWnd_Style CInfoWnd::_style;

CInfoWnd::CInfoWnd()
{
    mPtLT.setX( -1 );
    mPtLT.setY( -1 );

    _style.loadResource("ui/wnd/info/metric/");

    setAutoHide();
}

void CInfoWnd::paintEvent( QPaintEvent *event )
{
    Paragraph paragraph;
    QRect txtRect;

    //! background
    menu_res::RPopWnd::paintEvent( event );
    QRect selfRect = rect();

    int lineHeight = _style.lineHeight();
    //! foreground
    txtRect.setRect( _style.mFront ,
                     _style.mHead  ,
                     selfRect.width(),
                     lineHeight );
    //! set font
    QPainter painter(this);
    QFont font( qApp->font().family(), _style.mSize );
    painter.setFont( font );

    foreach( paragraph, mParagraphList )
    {
        painter.setPen( paragraph.mColor );

        painter.drawText( txtRect,
                          Qt::AlignTop | Qt::AlignLeft,
                          paragraph.mContent );

        txtRect.adjust(0, lineHeight, 0, lineHeight );
    }

    //! show picture
    if ( !mImage.isNull() )
    {
        //int x;
        int y;
        //! has text
        if ( mParagraphList.size() > 0 )
        {
            //x = 0;
            y = txtRect.bottom() + _style.mHLine;
        }
        else
        {
            //x = 0;
            y = _style.mHead;
        }

        painter.drawImage( (selfRect.width() - mImage.width())/2,
                           y,
                           mImage );
    }
}

void CInfoWnd::resizeEvent(QResizeEvent * event)
{
    //! base resize
    RPopWnd::resizeEvent( event );

    //! default pos
    if ( mPtLT.x() == -1 && mPtLT.y() == -1 )
    {}
    else
    {
        move( mPtLT );
    }
}

void CInfoWnd::show()
{
    menu_res::RInfoWnd::show();
}
void CInfoWnd::show( int x, int y )
{
    mPtLT.setX( x );
    mPtLT.setY( y );

    show();
}
void CInfoWnd::hide()
{
    menu_res::RInfoWnd::hide();
}

void CInfoWnd::setParagraphList( ParagraphList &list,
                                 QImage *pImg,
                                 int x,
                                 int y )
{
    int width, height;
    Paragraph paragraph;

    //! deload content
    mParagraphList = list;
    if ( pImg != NULL )
    { mImage = *pImg; }
    else
    { mImage = QImage(); }

    QFont font( qApp->font().family(), _style.mSize );
    QFontMetrics fontMet( font );

    //! height
    height = fontMet.height() * list.size()
            + _style.mHead
            + _style.mFoot;

    //! add img height
    if ( NULL != pImg )
    {
        height += pImg->height() + _style.mHLine*( list.size() > 0 );
    }

    //! width
    int maxWidth = 0;
    foreach( paragraph, mParagraphList )
    {
        width = fontMet.width( paragraph.mContent );
        if ( width > maxWidth )
        { maxWidth = width; }
    }

    if ( NULL != pImg )
    {
        maxWidth = qMax( pImg->width(), maxWidth );
    }

    width = maxWidth + _style.mFront + _style.mBack;



    //! 如果已经显示了窗口，则向大扩展，否则会有残留缺块
    if ( isVisible() )
    {
//        QRect nowRect = rect();
//        width = width > nowRect.width() ? width : nowRect.width();
//        height = height > nowRect.height() ? height : nowRect.height();
//    }

//    //! just move
//    if ( isVisible()  )
//    {
        if ( x != -1 && y != -1 )
        {
            hide();
            move( x, y );
        }
        //! align to center
        else
        {
            QRect windowRect;

            windowRect = sysGetMainWindow()->geometry();

            move( windowRect.left() + (windowRect.width() - width)/2,
                  windowRect.top() + windowRect.height() - height*4
                  );
        }
    }

    //! rsize
    resize( width, height );
}


