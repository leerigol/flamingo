#include "cinfownd_style.h"

#include "../../meta/crmeta.h"

void CInfoWnd_Style::load( const QString &path,
                           const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "head", mHead );
    meta.getMetaVal( path + "foot", mFoot );
    meta.getMetaVal( path + "front", mFront );
    meta.getMetaVal( path + "back", mBack );
    meta.getMetaVal( path + "hline", mHLine );

    meta.getMetaVal( path + "font/size", mSize );
}

int CInfoWnd_Style::lineWidth( const QString& line )
{
    QFontMetrics fontMet( QFont( qApp->font().family(),mSize) );

    return fontMet.width( line );
}
int CInfoWnd_Style::lineHeight()
{
    QFontMetrics fontMet( QFont( qApp->font().family(),mSize) );

    return fontMet.height();
}
