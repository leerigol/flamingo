#ifndef LOGOANI_STYLE_H
#define LOGOANI_STYLE_H

#include "../../menu/menustyle/rui_style.h"

class LogoAni_Style : public menu_res::RUI_Style
{
public:
    LogoAni_Style();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta);

public:
    QRect mRect;
    int mAniCnt;
    QString mAniPattern, mAniExt;

    QString mNormal, mDown;
};

#endif // LOGOANI_STYLE_H
