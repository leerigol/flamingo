
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../com/virtualkb/virtualkb.h"

#include "../../menu/wnd/rmessagebox.h"

#include "../appmain/cappmain.h"

#include "appgui.h"

//! msg table
IMPLEMENT_CMD( CApp, appGui )
start_of_entry()

//! menu
on_set_void_void( MSG_GUI_SERV_ASK_OK, &appGui::on_msg_ok_cancel ),
on_set_void_void( MSG_GUI_SERV_ASK_CANCEL, &appGui::on_msg_ok_cancel ),

on_set_void_void( MSG_GUI_NOTIFY_KEY_INFO, &appGui::on_key_info ),

//! system
on_set_void_void( CMD_SERVICE_DO_ERR, &appGui::on_err_code ),

//! user
on_set_void_void( servGui::cmd_info_cfg, &appGui::on_info_wnd ),

on_set_void_void( servGui::cmd_popup, &appGui::on_popup ),

on_set_void_void( servGui::cmd_msg_box, &appGui::on_msg_box ),

on_set_void_void( servGui::cmd_progress_cfg, &appGui::on_progress_visible ),
on_set_void_void( servGui::cmd_progress_visible, &appGui::on_progress_visible ),
on_set_void_void( servGui::cmd_progress_info, &appGui::on_progress_info ),
on_set_void_void( servGui::cmd_progress_step, &appGui::on_progress_step ),
on_set_void_void( servGui::cmd_progress_val, &appGui::on_progress_step ),

on_set_void_void( servGui::cmd_ch_label_set, &appGui::on_chlabel_set ),

on_set_void_void( servGui::cmd_v_line_set, &appGui::on_vline_set ),
on_set_void_void( servGui::cmd_h_line_set, &appGui::on_hline_set ),

//! animate
//on_set_void_void( servGui::cmd_logo_animate_run, &appGui::on_animate_changed ),
//on_set_void_void( servGui::cmd_logo_animate_rst, &appGui::on_animate_changed ),

end_of_entry()

//! config
#define err_show_time   2000    //! ms

#define err_wnd_width   200
#define err_wnd_height  50

appGui::appGui()
{
    mservName = serv_name_gui;

    mp_popWnd = NULL;
    mp_InfoWnd = NULL;

    mp_Progress = NULL;

    mp_wndIcon = NULL;
}

appGui::~appGui()
{
}

void appGui::setupUi( CAppMain *pMain )
{
    Q_ASSERT( NULL != pMain );
    m_pCentBar = pMain->getCentBar();
    Q_ASSERT( NULL != m_pCentBar );

    //! animate
    mp_logoAnimate = new menu_res::RAnimation( pMain->getTopBar() );
    Q_ASSERT( NULL != mp_logoAnimate );

    mLogoAniStyle.loadResource("appgui/logo_ani/", r_meta::CAppMeta() );

    mp_logoAnimate->setNormalDown( mLogoAniStyle.mNormal,
                                   mLogoAniStyle.mDown );
    QString strAni;
    for ( int i = 1; i <= mLogoAniStyle.mAniCnt; i++ )
    {
        strAni = QString("%1%2%3").arg( mLogoAniStyle.mAniPattern ).arg( i ).arg( mLogoAniStyle.mAniExt );
        mp_logoAnimate->addAnimation( strAni );
    }

    mp_logoAnimate->setGeometry( mLogoAniStyle.mRect );

    //! visible
    mp_logoAnimate->setVisible( true );

    //! wndicon
    mp_wndIcon = new WndIconInfo();
    Q_ASSERT( NULL != mp_wndIcon );
    attachPopupWindow( &mp_wndIcon );

    //! load keyInfoMeta
    QString keyImagePath;
    QString keyInfoImage;
    r_meta::CAppMeta meta;
    meta.getMetaVal("appgui/icon_info/path", keyImagePath );
    meta.getMetaVal("appgui/icon_info/image", keyInfoImage );
    keyInfoImage.trimmed();
    QStringList strList;
    strList = keyInfoImage.split(';');
    QStringList subList;
    foreach( QString item, strList )
    {
        item = item.simplified();
        if ( item.size() < 1 )
        { break; }

        subList = item.split(',');

        mIconPath.insert( subList[0].toInt(),
                          keyImagePath + subList[1].simplified() );
    }
}
void appGui::retranslateUi()
{}
void appGui::buildConnection()
{}

void appGui::on_progress_cancel()
{
    serviceExecutor::post( serv_name_gui,
                           servGui::cmd_progress_cancel,
                           (int)0 );
}

void appGui::on_msgbox_ok()
{
//    CCtrlMsgBox *pBox;
//    void *pSmartPtr;

//    query( servGui::cmd_msg_box, pSmartPtr );
//    Q_ASSERT( NULL != pSmartPtr );

//    pBox = (CCtrlMsgBox*)pSmartPtr;

//    serviceExecutor::post( pBox->mServName,
//                           pBox->mOkId,
//                           (int)0 );
}


/*!
 * \brief appGui::on_err_code
 * 发生错误，显示出错误码
 */
void appGui::on_err_code()
{
    int iCode;
    DsoErr errCode;

    //! get err code
    query( CMD_SERVICE_DO_ERR, iCode );
    errCode = (DsoErr)iCode;

    //! no err
    if ( errCode == ERR_NONE )
    { return ; }

    //! format error
    QString str;
    str = sysGetString( iCode );
    if ( str.length() > 0 )
    {
        str = str.trimmed();
        if ( !str.endsWith("!") )
        {
            str.append('!');
        }
    }
    else
    {
        qDebug() << "Unknown error:" << QString().setNum(iCode,16);
        str = QString("Error:%1!").arg( QString().setNum(iCode,16) );
    }

    showPopup( str );
}

//! show info
//! many lines
void appGui::on_info_wnd()
{
    CtrlParagraph *pCtrl;
    CtrlParagraph ctrlParagraph;
    void *pSmartptr;

    //! get paragraph
    query( servGui::cmd_info_cfg, pSmartptr );
    pCtrl = (CtrlParagraph*)pSmartptr;

    Q_ASSERT( NULL != pCtrl );
    ctrlParagraph = *pCtrl;

    //! create wnd
    if ( NULL == mp_InfoWnd )
    {
        mp_InfoWnd =new CInfoWnd();
        Q_ASSERT( NULL != mp_InfoWnd );
    }

    //! set paragraph
    mp_InfoWnd->setParagraphList( ctrlParagraph.mParagraphs,
                                  ctrlParagraph.m_pImage,
                                  ctrlParagraph.mX,
                                  ctrlParagraph.mY );
    if( ctrlParagraph.nHideTimeout == -1 )
    {
        mp_InfoWnd->setAutoHide();
    }
    else
    {
        mp_InfoWnd->setAutoHide( ctrlParagraph.nHideTimeout );
    }

    //! use font
    mp_InfoWnd->setFont( qApp->font() );

    //! show
    mp_InfoWnd->show( ctrlParagraph.mX, ctrlParagraph.mY );

    //! update
    //mp_InfoWnd->update();
}

void appGui::on_popup()
{
    int popId;

    query( servGui::cmd_popup, popId );

    QString str;
    str = sysGetString( popId );

    if ( str.length() < 1 )
    {
        Q_ASSERT( false );
        return;
    }

    showPopup( str );
}

void appGui::on_msg_box()
{
    CCtrlMsgBox *pBox;
    void *pSmartPtr;

    query( servGui::cmd_msg_box, pSmartPtr );
    Q_ASSERT( NULL != pSmartPtr );

    pBox = (CCtrlMsgBox*)pSmartPtr;

    menu_res::RMessageBox::Show( pBox->mInfo,
                                 this,
                                 SLOT(on_msgbox_ok()),
                                 pBox->mIcons,
                                 pBox->mButtons
                                );

//    QMessageBox msgBox;
//    msgBox.setText( pBox->mInfo );
//    msgBox.exec();
}

void appGui::on_msg_ok_cancel()
{
    menu_res::RMessageBox::Hide();
}

void appGui::on_key_info()
{
    CtrlIconInfo info;

    //! query
    void *pSmartptr = NULL;
    query( MSG_GUI_NOTIFY_KEY_INFO, pSmartptr );
    if ( NULL == pSmartptr )
    { return; }

    //! deload
    info = *((CtrlIconInfo*)pSmartptr);

    //! show the item
    r_meta::CAppMeta meta;
    if ( mp_wndIcon->setup( "appgui/", meta ) )
    {
        QString style;
        meta.getMetaVal("appgui/icon_info/style", style );
        mp_wndIcon->setStyleSheet( style );
    }


    //! deload the info and images
    QStringList imgs, infos;

    foreach( int id, info.mInfoIds )
    {
        infos.append( sysGetString( id ) );
    }

    foreach ( int id, info.mIconIds )
    {
        if ( mIconPath.contains(id) )
        {
            imgs.append( mIconPath[id] );
        }
    }

    mp_wndIcon->setInfo( infos, imgs );
    mp_wndIcon->show();
}

void appGui::on_progress_visible()
{
    CtrlProgress *pProgCfg;
    void *pSmartptr;

    //! get paragraph
    query( servGui::cmd_progress_cfg, pSmartptr );
    pProgCfg = (CtrlProgress*)pSmartptr;

    Q_ASSERT( NULL != pProgCfg );

    //! show now
    if ( pProgCfg->mbShow )
    {
        //! create
        if ( NULL == mp_Progress )
        {
            mp_Progress = new menu_res::RProgressDialog( sysGetMainWindow() );
            Q_ASSERT( NULL != mp_Progress );
        }

        //! range
        mp_Progress->setRange( pProgCfg->mValA, pProgCfg->mValB );
        mp_Progress->setValue( pProgCfg->mVal );
        mp_Progress->setInfo( pProgCfg->mInfo );
        mp_Progress->setTitle( pProgCfg->mTitle);

        //! set geomerty
        QRect parRect = sysGetMainWindow()->rect();
        QRect progRect = mp_Progress->rect();

        mp_Progress->move( ( parRect.width() - progRect.width() ) / 2,
                           ( parRect.height() - progRect.height()) / 2 );

        //! visible
        mp_Progress->show();
    }
    //! hide now
    else
    {
        if ( NULL != mp_Progress )
        {
            mp_Progress->hide();
            delete mp_Progress;
            mp_Progress = NULL;
        }
    }
}

void appGui::on_progress_info()
{
    if ( NULL != mp_Progress )
    {
        QString strInfo;

        query( servGui::cmd_progress_info, strInfo );

        mp_Progress->setInfo( strInfo );
        mp_Progress->update();
    }
}
void appGui::on_progress_step()
{
    if ( NULL != mp_Progress )
    {
        int progVal;

        query( servGui::cmd_progress_val, progVal );

        mp_Progress->setValue( progVal );

        mp_Progress->update();
    }
}

void appGui::on_chlabel_set()
{
    CtrlList *pList;
    pList = getCtrlList();
    Q_ASSERT( NULL != pList );

    Ctrl *pCtrl;
    QWidget *pWidget;

    //! delete widget
    trimWidgetList( pList );

    //! new item
    menu_res::RDsoCHLabel *pUI;
    foreach( pCtrl, *pList )
    {
        Q_ASSERT( NULL != pCtrl );

        if ( pCtrl->getVisible() )
        {
            pWidget = findWidget( pCtrl->objectName() );
            if ( NULL == pWidget )
            {
                Q_ASSERT( NULL != m_pCentBar );
                pUI = new menu_res::RDsoCHLabel(  m_pCentBar );
                Q_ASSERT( NULL != pUI );

                pUI->setObjectName( pCtrl->objectName() );

                mWidgetList.append( pUI );
            }
        }
    }

    //! config
    CtrlCHLabel *pCtrlProxy;
    foreach( pCtrl, *pList )
    {
        Q_ASSERT( NULL != pCtrl );

        if ( pCtrl->getVisible() )
        {
            pWidget = findWidget( pCtrl->objectName() );
            Q_ASSERT( pWidget != NULL );

            pUI = (menu_res::RDsoCHLabel*)pWidget;
            pCtrlProxy = (CtrlCHLabel*)pCtrl;

            pUI->setColor( pCtrlProxy->getColor() );
            pUI->setText( pCtrlProxy->getLabel() );
            ((QLabel*)pUI)->move( pCtrlProxy->getPos() );
            pUI->setVisible( true );
        }
    }
}

void appGui::on_vline_set()
{
    CtrlList *pList;
    pList = getCtrlList();
    Q_ASSERT( NULL != pList );

    Ctrl *pCtrl;
    QWidget *pWidget;

    //! delete widget
    trimWidgetList( pList );

    //! new item
    menu_res::RDsoLine *pUI;
    foreach( pCtrl, *pList )
    {
        Q_ASSERT( NULL != pCtrl );

        if ( pCtrl->getVisible() )
        {
            pWidget = findWidget( pCtrl->objectName() );
            if ( NULL == pWidget )
            {
                Q_ASSERT( NULL != m_pCentBar );
                pUI = new menu_res::RDsoVLine( m_pCentBar );
                Q_ASSERT( NULL != pUI );

                pUI->setObjectName( pCtrl->objectName() );

                mWidgetList.append( pUI );
            }
        }
    }

    //! config
    CtrlVLine *pCtrlProxy;
    foreach( pCtrl, *pList )
    {
        Q_ASSERT( NULL != pCtrl );

        if ( pCtrl->getVisible() )
        {
            pWidget = findWidget( pCtrl->objectName() );
            Q_ASSERT( pWidget != NULL );

            pUI = (menu_res::RDsoVLine*)pWidget;
            pCtrlProxy = (CtrlVLine*)pCtrl;

            pUI->setColor( pCtrlProxy->getColor() );
            pUI->setGeometry( pCtrlProxy->getRect() );
            pUI->setVisible( true );
        }
    }
}
void appGui::on_hline_set()
{
    CtrlList *pList;
    pList = getCtrlList();
    Q_ASSERT( NULL != pList );

    Ctrl *pCtrl;
    QWidget *pWidget;

    //! delete widget
    trimWidgetList( pList );

    //! new item
    menu_res::RDsoHLine *pUI;
    foreach( pCtrl, *pList )
    {
        Q_ASSERT( NULL != pCtrl );

        if ( pCtrl->getVisible() )
        {
            pWidget = findWidget( pCtrl->objectName() );
            if ( NULL == pWidget )
            {
                Q_ASSERT( NULL != m_pCentBar );
                pUI = new menu_res::RDsoHLine( m_pCentBar );
                Q_ASSERT( NULL != pUI );

                pUI->setObjectName( pCtrl->objectName() );

                mWidgetList.append( pUI );
            }
        }
    }

    //! config
    CtrlHLine *pCtrlProxy;
    foreach( pCtrl, *pList )
    {
        Q_ASSERT( NULL != pCtrl );

        if ( pCtrl->getVisible() )
        {
            pWidget = findWidget( pCtrl->objectName() );
            Q_ASSERT( pWidget != NULL );

            pUI = (menu_res::RDsoHLine*)pWidget;
            pCtrlProxy = (CtrlHLine*)pCtrl;

            pUI->setColor( pCtrlProxy->getColor() );
            pUI->setGeometry( pCtrlProxy->getRect() );
            pUI->setVisible( true );
        }
    }
}

void appGui::on_animate_changed()
{
    DsoErr err;

    bool bAnimate;

    err = query( servGui::cmd_logo_animate_run, bAnimate );
    if ( err != ERR_NONE )
    { return; }

    if ( bAnimate )
    { mp_logoAnimate->start(); }
    else
    { mp_logoAnimate->stop(); }
}

//! only one line
void appGui::showPopup( const QString &str )
{
    //! show popup
    if ( NULL == mp_popWnd )
    {
        mp_popWnd = new CPopWnd();
        Q_ASSERT( NULL != mp_popWnd );

        mp_popWnd->setGeometry(0,0,0,0);
        mp_popWnd->setStyleSheet("QLabel {color: #ffffff;}");

    }

    mp_popWnd->setInfo( str );
    mp_popWnd->setFont( qApp->font() );

    mp_popWnd->show();
}

QWidget *appGui::findWidget( const QString &objName )
{
    QWidget *pWidget;

    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( NULL != pWidget );

        if ( pWidget->objectName() == objName )
        {
            return pWidget;
        }
    }

    return NULL;
}
/*!
 * \brief appGui::getCtrlList
 * \return
 * 从服务中得到代理列表
 */
CtrlList *appGui::getCtrlList()
{
    void *pSmartptr;
    query( servGui::qcmd_ctrl_list,
           pSmartptr );
    Q_ASSERT( NULL != pSmartptr );

    CtrlList *pList;
    pList = (CtrlList*)pSmartptr;
    return pList;
}

/*!
 * \brief appGui::trimWidgetList
 * - 去掉 widget list 和 ctrl list 中不一致的项目
 * - ctrllist在服务中，而控件在app中
 */
void appGui::trimWidgetList( CtrlList *pList )
{
    Ctrl *pCtrl;
    QWidget *pWidget;

    //! delete widget
    foreach( pWidget, mWidgetList )
    {
        Q_ASSERT( NULL != pWidget );

        pCtrl = servGui::findCtrl( pWidget->objectName(),
                                   pList
                                   );
        //! find none
        if ( NULL == pCtrl )
        {
            mWidgetList.removeOne( pWidget );
            delete pWidget;
        }
    }
}
