#include "cpopwnd.h"
#include "../../menu/draw/rpainter.h"
CInfoWnd_Style CPopWnd::_style;

CPopWnd::CPopWnd()
{
    m_pLabel = new QLabel(this);
    Q_ASSERT( NULL != m_pLabel );

    _style.loadResource("ui/wnd/info/metric/");
}

/*!
 * \brief CPopWnd::setInfo
 * \param info
 * 设置显示的错误信息
 */
void CPopWnd::setInfo( const QString &info )
{
    int w, h, wT, hT;

    wT = _style.lineWidth( info );
    hT = _style.lineHeight();

    w = wT + _style.mFront + _style.mBack;
    h = hT + _style.mHead + _style.mFoot;

    //if ( isVisible() )
    {
        QRect selfRect;

        selfRect = rect();

        w = w > selfRect.width() ? w : selfRect.width();
        h = h > selfRect.height() ? h : selfRect.height();

        //! set label font
        m_pLabel->setFont( QFont( qApp->font().family(), _style.mSize) );
    }

    resize( w, h );

    m_pLabel->setText( info );
    m_pLabel->setGeometry( _style.mFront,
                           _style.mHead,
                           wT, hT
                           );
    m_pLabel->move( _style.mFront, _style.mHead );
}

void CPopWnd::show()
{
    menu_res::RPopWnd::show();
}
void CPopWnd::hide()
{
    menu_res::RPopWnd::hide();
}
