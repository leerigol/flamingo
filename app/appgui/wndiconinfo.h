#ifndef WNDKEYINFO_H
#define WNDKEYINFO_H


#include "../../menu/rmenus.h"  //! controlers


class WndIconInfo :public menu_res::RPopWnd
//class WndIconInfo :public QWidget
{
    Q_OBJECT

public:
    WndIconInfo();
protected:

public:
    void setInfo(  QStringList &strs,
                   QStringList &imgs );
protected:
    QList<QLabel *> mImages;
    QList<QLabel *> mLabels;

    QGridLayout *m_pLayout;
};

#endif // WNDKEYINFO_H
