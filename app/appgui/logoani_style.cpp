#include "logoani_style.h"

LogoAni_Style::LogoAni_Style()
{
}

void LogoAni_Style::load( const QString &path,
                          const r_meta::CMeta &appMeta )
{
//    r_meta::CAppMeta appMeta;

    appMeta.getMetaVal( path + "geo", mRect );

    appMeta.getMetaVal( path + "animate/num", mAniCnt);
    appMeta.getMetaVal( path + "animate/pattern", mAniPattern);
    appMeta.getMetaVal( path + "animate/ext", mAniExt );

    appMeta.getMetaVal( path + "outline/normal", mNormal );
    appMeta.getMetaVal( path + "outline/down", mDown );
}
