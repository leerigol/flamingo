#ifndef CPopWnd_H
#define CPopWnd_H

#include "../../menu/rmenus.h"  //! controlers

#include "cinfownd_style.h"

/*!
 * \brief The CPopWnd class
 * 错误提示窗口
 */
class CPopWnd : public menu_res::RPopWnd
{
    Q_OBJECT

    static CInfoWnd_Style _style;

public:
    CPopWnd();

public:
    void setInfo( const QString &info );

public Q_SLOTS:
    void show();
    void hide();

private:
    QLabel *m_pLabel;

};

#endif // CPopWnd_H
