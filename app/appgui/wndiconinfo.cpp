#include "wndiconinfo.h"

WndIconInfo::WndIconInfo()
{
    m_pLayout = new QGridLayout( this );

    setLayout( m_pLayout );
}

void WndIconInfo::setInfo(  QStringList &strs,
               QStringList &imgs )
{
    int i;
    //! check control count
    QLabel *pLabel;
    for ( i = mImages.size(); i < strs.size(); i++ )
    {
        pLabel = new QLabel( this );
        Q_ASSERT( NULL != pLabel );
        mImages.append( pLabel );
        m_pLayout->addWidget( pLabel, i, 0 );

        pLabel = new QLabel( this );
        Q_ASSERT( NULL != pLabel );
        mLabels.append( pLabel );
        m_pLayout->addWidget( pLabel, i, 1, Qt::AlignLeft );

    }

    //! set info
    for ( i = 0; i < strs.size(); i++ )
    {
        mLabels[i]->setText( strs[i] );
        mLabels[i]->show();
    }
    for ( ; i < mLabels.size(); i++ )
    {
        mLabels[i]->hide();
    }

    //! image
    for ( i = 0; i < imgs.size(); i++ )
    {
        QPixmap pic;
        if ( pic.load( imgs[i] ) )
        {
            mImages[i]->setPixmap( pic );
            mImages[i]->show();
        }
        else
        {
            mImages[i]->hide();
        }
    }

    for ( ; i < mImages.size(); i++ )
    {
        mImages[i]->hide();
    }

    //! \todo
    resize( 150, 40 );
}
