#ifndef CINFOWND_H
#define CINFOWND_H
#include "../../menu/rmenus.h"  //! controlers

#include "../../service/servgui/ctrlparagraph.h"

#include "cinfownd_style.h"

/*!
 * \brief The CInfoWnd class
 * notify 弹出提示窗口
 */
class CInfoWnd : public menu_res::RPopWnd
{
    Q_OBJECT

    static CInfoWnd_Style _style;

public:
    CInfoWnd();

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual void resizeEvent(QResizeEvent * event);

public Q_SLOTS:
    void show();
    void show( int x, int y );
    void hide();

public:
    void setParagraphList( ParagraphList &list,
                           QImage *pImage,
                           int x,
                           int y );

private:
    ParagraphList mParagraphList;
    QImage mImage;
    QPoint mPtLT;
};

#endif // CINFOWND_H
