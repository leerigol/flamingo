#ifndef APPGUI_H
#define APPGUI_H

#include <QtCore>
#include <QtWidgets>

#include "../capp.h"

#include "../../menu/wnd/rprogressdialog.h"
#include "../../menu/dsowidget/rdsochlabel.h"

#include "../../service/servgui/servgui.h"

#include "cpopwnd.h"
#include "cinfownd.h"
#include "wndiconinfo.h"

#include "../../widget/ranimation.h"
#include "logoani_style.h"

#include "../../menu/dsownd/wndloginfo.h"

/*!
 * \brief The appGui class
 * servgui 对应的界面实现
 */
class appGui : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    appGui();
    ~appGui();

public:
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();
    virtual void buildConnection();

protected Q_SLOTS:
    void on_progress_cancel();
    void on_msgbox_ok();

protected:
    void on_err_code();

    void on_info_wnd();
    void on_popup();

    void on_msg_box();
    void on_msg_ok_cancel();

    void on_key_info();

    void on_progress_visible();
    void on_progress_info();
    void on_progress_step();

    void on_chlabel_set();

    void on_vline_set();
    void on_hline_set();

    void on_animate_changed();

private:
    void showPopup( const QString &str );

    void format( QString &str, DsoErr errCode );
    QWidget *findWidget( const QString &objName );

    CtrlList *getCtrlList();
    void trimWidgetList( CtrlList *pList );

private:
    CPopWnd *mp_popWnd;     //! only one line

    CInfoWnd *mp_InfoWnd;   //! a few lines

    menu_res::RProgressDialog *mp_Progress;

    menu_res::RAnimation *mp_logoAnimate;
    LogoAni_Style mLogoAniStyle;

    WndIconInfo *mp_wndIcon;
    QMap<int,QString> mIconPath;

    QList< QWidget *> mWidgetList;
    QWidget *m_pCentBar;
};

#endif // APPGUI_H
