#ifndef CINFOWND_STYLE_H
#define CINFOWND_STYLE_H

#include "../../menu/rmenus.h"  //! controlers

class CInfoWnd_Style : public menu_res::RUI_Style
{
protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    int lineWidth( const QString& line );
    int lineHeight();

public:
    int mHead, mFoot, mHLine;   //! hline
    int mFront, mBack;

    int mSize;
};

#endif // CINFOWND_STYLE_H
