#include "appgui.h"

void appGui::format( QString &str, DsoErr errCode )
{
    switch( errCode )
    {
        case ERR_NONE:
            str = tr("No Error");
            break;

        case ERR_INVALID_INPUT:
            str = tr("Invalid Input");
            break;

        case ERR_INVALID_CONFIG:
            str = tr("Invalid Config");
            break;

        case ERR_OVER_RANGE:
            str = tr("Over Range");
            break;

        case ERR_OVER_LOW_RANGE:
            str = tr("Over Low Range");
            break;

        case ERR_OVER_UPPER_RANGE:
            str = tr("Over Upper Range");
            break;

        case ERR_FILE_MAGIC:
            str = tr("Invalid file magic");
            break;

        case ERR_FILE_OLD_VER:
            str = tr("File version is OLD");
            break;

        case ERR_FILE_NEW_VER:
            str = tr("File version is NEW");
            break;
        case ERR_FILE_TYPE:
            str = tr("Invalid file TYPE");
            break;

        case ERR_FILE_MODEL:
            str = tr("Model is mismatch");
            break;

        case ERR_FILE_EXIST:
            str = tr("File or directory exist");
            break;
        case ERR_FILE_NOT_EXIST:
            str = tr("File or directory not exist");
            break;

        case ERR_FILE_SAVE_OK:
            str = tr("Saving success");
            break;

        case ERR_FILE_SAVE_FAIL:
            str = tr("Saving failed");
            break;

        case ERR_FILE_COPY_OK:
            str = tr("Copying success");
            break;

        case ERR_FILE_COPY_FAIL:
            str = tr("Copying failed");
            break;

        case ERR_FILE_DELETE_OK:
            str = tr("Deleting success");
            break;

        case ERR_FILE_DELETE_FAIL:
            str = tr("Deleting failed");
            break;

        case ERR_FILE_RENAME_FAIL:
            str = tr("Rename failed");
            break;

        case ERR_MENU_JUMP_INVALID:
            str = tr("Invalid menu jump");
            break;

        case ERR_LAN_NOLINK:
            str = tr("Network unavailable");
            break;

        case ERR_KEYBOARD_LOCKED:
        case ERR_THE_KEY_LOCKED:
            str = tr("The key was locked");
            break;

        default:
            str = tr("Unknown");
            break;
    }

    str = str + QString(":%1").arg( (int)errCode );
}
