#include <QPainter>
#include "ripplevaluebox.h"
#include "../../meta/crmeta.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"
#include "../../service/servmeasure/meas_interface.h"

menu_res::RModelWnd_Style CRippleValueBox::_style;

CRippleValueBox::CRippleValueBox(QWidget *parent) : uiWnd(parent)
{
    set_attr( wndAttr, mWndAttr, wnd_moveable);

    m_RippleVal = NULL;
}

CRippleValueBox::~CRippleValueBox()
{
    Q_ASSERT( NULL != m_RippleVal);
    delete m_RippleVal;
}

void CRippleValueBox::setupUi(QWidget* parent)
{
    loadResource("upa/ripple_values/");
//    _style.loadResource("ui/wnd/cursor_value/");
    _style.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(mCloseRect);
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(hideThis()) );


    m_RippleVal = new CMeasValueItem( CMeasItem(Meas_Vpp));
    Q_ASSERT(m_RippleVal != NULL);

    setParent(parent);
}

void CRippleValueBox::loadResource(const QString&path)
{
    r_meta::CRMeta meta;
    QString str = path ;

    meta.getMetaVal(str + "row_h", m_nRowH);//20

    meta.getMetaVal(str + "x", m_nBoxX);//126
    meta.getMetaVal(str + "y", m_nBoxY);//410
    meta.getMetaVal(str + "w", m_nBoxW);//665
    meta.getMetaVal(str + "h", m_nBoxH);//120

    this->setGeometry(m_nBoxX, m_nBoxY, m_nBoxW, m_nBoxH);

    meta.getMetaVal(str + "closerect", mCloseRect);
    int num = 0;
    meta.getMetaVal(str + "fontcolor", num);
    setColor(mFontColor, num);
    meta.getMetaVal(str + "titlecolor", num);
    setColor(mTitlebgColor, num);
    meta.getMetaVal(str + "gray15", num);
    setColor(mGray15, num);
    meta.getMetaVal(str + "gray27", num);
    setColor(mGray27, num);
}
void CRippleValueBox::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

void CRippleValueBox::setSrc(Chan src)
{
    m_RippleVal->setSrcA(src);
}

void CRippleValueBox::updateValue()
{
    pointer pVal = NULL;
    serviceExecutor::query( serv_name_upa, servUPA::cmd_get_ripple_val, pVal);
    if( NULL != pVal)
    {
        measStatData *pRipple = (measStatData *)pVal;
        m_RippleVal->setValue(pRipple);

        if( isVisible())
        {
            update();
        }
    }
}

void CRippleValueBox::hideThis()
{
    emit closeSig();
    hide();
}

void CRippleValueBox::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QFont font;
    font.setPointSize(11);
    QFontMetrics font_metric(font);
    painter.setFont(font);

    QRect r = this->geometry();
    int nWidth = r.width();
    int nStartX = 15;
    int nStartY = 6;
    int nItemStartX = 152;
    int nItemStartY = nStartY + 33;
    int nItemValStartY = nItemStartY + m_nRowH;
    int nItemWidth = 90;
    QString  temp("%1"),cntVal;

    r.setX(0);
    r.setY(0);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    _style.paint( painter, r);

    QRect rLeftBox = r;
    rLeftBox.setX(9);
    rLeftBox.setY(39);
    rLeftBox.setWidth(r.width()-18);
    rLeftBox.setHeight(m_nRowH);
    painter.fillRect(rLeftBox.x(),rLeftBox.y(),
                     rLeftBox.width(),rLeftBox.height(), mTitlebgColor);

    for(int i=0; i<1; i++)
    {
        painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                         rLeftBox.width(),rLeftBox.height(), mGray15);
        i++;
        if( i<1)
        {
            rLeftBox.moveTo(rLeftBox.bottomLeft());
            painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                             rLeftBox.width(),rLeftBox.height(),  mGray27);
        }
        rLeftBox.moveTo(rLeftBox.bottomLeft());
    }

    //Draw Ripple Title
    r.setX(nStartX);
    r.setY(nStartY);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    QPen pen;
    pen.setColor(Qt::gray);
    painter.setPen(pen);
    QString title = sysGetString(MSG_APP_RIPPLE_TITLE, "Ripple");
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, title);

    //Draw Ripple Source
    QColor color = m_RippleVal->getItemInfo()->mSrcAColor;
    pen.setColor(color);
    painter.setPen(pen);
    QRect r_title = font_metric.boundingRect(r, Qt::TextWrapAnywhere, title);
    QRect r_src = r_title;
    r_src.setX(r_title.x() + r_title.width());
    r_src.setY(r_title.y());
    r_src.setWidth(nWidth);
    r_src.setHeight(m_nBoxH);

    QString src = m_RippleVal->getItemInfo()->mSrcAStr;
    painter.drawText(r_src, Qt::AlignLeft | Qt::AlignTop,  "("+src+")");

    //Draw Ripple Item

    r.setX(nItemStartX);
    r.setY(nItemStartY);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    QString curr = sysGetString(MSG_APP_RIPPLE_CURR, "Curr");
    QString avg = sysGetString(MSG_APP_RIPPLE_AVG, "Avg");
    QString min = sysGetString(MSG_APP_RIPPLE_MIN, "Min");
    QString max = sysGetString(MSG_APP_RIPPLE_MAX, "Max");
    QString dev = sysGetString(MSG_APP_RIPPLE_DEV, "Dev");
    QString cnt = sysGetString(MSG_APP_RIPPLE_CNT, "Cnt");

    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, curr);
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, avg);
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, min);
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, max);
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, dev);
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, cnt);


    //Draw Item Name
    r.setX(nStartX);
    r.setY(nItemValStartY);
    r.setWidth(nWidth);
    r.setHeight(m_nRowH);

    QString itemName = sysGetString(MSG_APP_RIPPLE_TITLE, "Ripple");
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, itemName);

    //Draw Item Value
    r.setX(nItemStartX);
    r.setY(nItemValStartY);
    r.setWidth(nWidth);
    r.setHeight(m_nRowH);

    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_RippleVal->getCurr());
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_RippleVal->getAvg());
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_RippleVal->getMin());
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_RippleVal->getMax());
    r.setX(r.x() + nItemWidth);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_RippleVal->getDev());
    r.setX(r.x() + nItemWidth);
    cntVal = temp.arg(m_RippleVal->getCnt());
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, cntVal);

    event->ignore();
}
