#ifndef RIPPLEVALUEBOX_H
#define RIPPLEVALUEBOX_H

#include "../measure/cmeasValueItem.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;

class CRippleValueBox : public uiWnd
{
    Q_OBJECT
public:
    CRippleValueBox(QWidget *parent = 0);
    ~CRippleValueBox();
    void setupUi(QWidget* parent);

    void setSrc(Chan src);
    void updateValue();

    static menu_res::RModelWnd_Style _style;

public slots:
    void hideThis();

protected:
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString&path);

signals:
    void closeSig();

private:
   void setColor( QColor &color, int hexRgb );

private:
    CMeasValueItem *m_RippleVal;
    RImageButton   *m_pCloseButton;

    int  m_nBoxX;
    int  m_nBoxY;
    int  m_nBoxW;
    int  m_nBoxH;

    int  m_nRowH;
    QRect    mCloseRect;
    QColor   mFontColor, mTitlebgColor;
    QColor   mGray15, mGray27;
};

#endif // CRIPPLEVALUEBOX_H
