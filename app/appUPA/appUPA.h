#ifndef APPUPA_H
#define APPUPA_H

#include "../appmain/cappmain.h"
#include "PowerQValueBox.h"
#include "ripplevaluebox.h"
#include "powerqtipsdlg.h"

class CAppUPA : public CApp
{  
    Q_OBJECT

    DECLARE_CMD()
public:
    CAppUPA();
    ~CAppUPA();

    enum ListenerEvents
    {
        cmd_base = CMD_APP_BASE,

        cmd_ripple_disp,
        cmd_ripple_src,

        cmd_powerq_disp,
        cmd_powerq_volt,
        cmd_powerq_curr,
        cmd_powerq_fref,

        cmd_upa_update,
        cmd_close_upa_app,
        cmd_upa_tips,
        cmd_upa_opt_active,
        cmd_upa_opt_exp,
        cmd_upa_opt_invalid,
    };

public:
    void setupUi( CAppMain *pMain );
    void retranslateUi();
    void buildConnection();

    void upaUpdate();

    int closeUpaApp();
    int onShowTips();
    void showPowerQTips();
    void showRippleTips();

    void  onOptDetect();

    void  onOptActive();
    void  onOptExp();
    void  onOptInvalid();

protected:
    void registerSpy();

public:
    //PowerQ
    void onShowPowerQBox(void);
    void setPowerQVoltSrc(void);
    void setPowerQCurrSrc(void);
    void setPowerQRefSrc(void);

    //Ripple
    void onShowRippleBox();
    void setRippleSrc();


public Q_SLOTS:
    void postCloseSig();
    void postRippleCloseSig();

private:
    CPowerQValueBox*  m_pPowerQValueBox;
    CRippleValueBox*  m_pRippleValueBox;

    CUpaTipsDlg* m_pPowewrQTips;
    CUpaTipsDlg* m_pRippleTips;
};

#endif // APPUPA_H
