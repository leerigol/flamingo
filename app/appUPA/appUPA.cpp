#include "appUPA.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppUPA )
start_of_entry()
on_set_int_void (  CAppUPA::cmd_powerq_volt,        &CAppUPA::setPowerQVoltSrc ),
on_set_int_void (  CAppUPA::cmd_powerq_curr,        &CAppUPA::setPowerQCurrSrc ),
on_set_int_void (  CAppUPA::cmd_powerq_fref,        &CAppUPA::setPowerQRefSrc ),
on_set_void_void ( CAppUPA::cmd_powerq_disp,        &CAppUPA::onShowPowerQBox ),

on_set_void_void ( CAppUPA::cmd_ripple_src,         &CAppUPA::setRippleSrc),
on_set_void_void ( CAppUPA::cmd_ripple_disp,        &CAppUPA::onShowRippleBox ),

on_set_int_void ( servUPA::cmd_close_upa_app,      &CAppUPA::closeUpaApp),
on_set_int_void ( servUPA::cmd_upa_update,         &CAppUPA::upaUpdate),
on_set_int_void(  CAppUPA::cmd_upa_tips,           &CAppUPA::onShowTips),

on_set_void_void( CAppUPA::cmd_upa_opt_active,     &CAppUPA::onOptActive),
on_set_void_void( CAppUPA::cmd_upa_opt_exp,        &CAppUPA::onOptExp),
on_set_void_void( CAppUPA::cmd_upa_opt_invalid,    &CAppUPA::onOptInvalid),

end_of_entry()

CAppUPA::CAppUPA()
{
    mservName = serv_name_upa;

    addQuick(   MSG_APP_UPA,
                QStringLiteral("ui/desktop/upa/icon/") );

    //! quick opened
    addQuick( MSG_APP_UPA,
              QStringLiteral("ui/desktop/upa/icon_open/"),
              1,
              app_disable );

    m_pPowerQValueBox = new CPowerQValueBox();
    Q_ASSERT(NULL != m_pPowerQValueBox);
    m_pPowerQValueBox->setVisible(false);

    m_pRippleValueBox = new CRippleValueBox();
    Q_ASSERT(NULL != m_pRippleValueBox);
    m_pRippleValueBox->setVisible(false);

    m_pPowewrQTips = NULL;
    m_pRippleTips = NULL;
}

CAppUPA::~CAppUPA()
{

}

void CAppUPA::setupUi( CAppMain *pMain )
{
    m_pPowerQValueBox->setupUi(pMain->getCentBar());
    m_pPowerQValueBox->hide();

    m_pRippleValueBox->setupUi(pMain->getCentBar());
    m_pRippleValueBox->hide();

    onOptDetect();
}

void CAppUPA::retranslateUi()
{

}

void CAppUPA::buildConnection()
{
    connect(m_pPowerQValueBox, SIGNAL(closeSig()),this,SLOT(postCloseSig()));
    connect(m_pRippleValueBox, SIGNAL(closeSig()),this,SLOT(postRippleCloseSig()));
}

void CAppUPA::registerSpy()
{
    spyOn( serv_name_upa_ripple, MSG_UPA_RIPPLE_DISP,   CAppUPA::cmd_ripple_disp);
    spyOn( serv_name_upa_ripple, MSG_UPA_RIPPLE_SOURCE, CAppUPA::cmd_ripple_src);

    spyOn( serv_name_upa_powerq, MSG_UPA_POWER_DISP,    CAppUPA::cmd_powerq_disp);
    spyOn( serv_name_upa_powerq, MSG_UPA_POWER_VOLT,    CAppUPA::cmd_powerq_volt);
    spyOn( serv_name_upa_powerq, MSG_UPA_POWER_CURR,    CAppUPA::cmd_powerq_curr);
    spyOn( serv_name_upa_powerq, MSG_UPA_POWER_FREF,    CAppUPA::cmd_powerq_fref);

    spyOn( serv_name_upa_ripple, MSG_UPA_RIPPLE_TIPS,   CAppUPA::cmd_upa_tips);
    spyOn( serv_name_upa_powerq, MSG_UPA_POWER_TIPS,    CAppUPA::cmd_upa_tips);

    spyOn(serv_name_license, servLicense::cmd_Opt_Active , CAppUPA::cmd_upa_opt_active);
    spyOn(serv_name_license, servLicense::cmd_Opt_Exp    , CAppUPA::cmd_upa_opt_exp);
    spyOn(serv_name_license, servLicense::cmd_Opt_Invalid, CAppUPA::cmd_upa_opt_invalid);
}

int CAppUPA::closeUpaApp()
{
    m_pPowerQValueBox->hide();
    m_pRippleValueBox->hide();

    return ERR_NONE;
}

int CAppUPA::onShowTips()
{
    int upaType = (int)UPA_Ripple;
    query( serv_name_upa, servUPA::cmd_get_AnalysisType, upaType);

    switch ((UPA_Type)upaType)
    {
    case UPA_PowerQ:
        showPowerQTips();
        break;
    case UPA_Ripple:
        showRippleTips();
        break;
    default:
        break;
    }

    return ERR_NONE;
}

void CAppUPA::showPowerQTips()
{
    if( m_pPowewrQTips == NULL)
    {
        m_pPowewrQTips = new CUpaTipsDlg( UPA_PowerQ, sysGetMidWindow());

        Q_ASSERT( m_pPowewrQTips != NULL);
    }

    if( m_pPowewrQTips->isHidden() )
    {
        m_pPowewrQTips->show();
    }
    else
    {
        if( m_pPowewrQTips != NULL)
        {
            m_pPowewrQTips->hide();
        }
    }
}

void CAppUPA::showRippleTips()
{
    if( m_pRippleTips == NULL)
    {
        m_pRippleTips = new CUpaTipsDlg( UPA_Ripple, sysGetMidWindow());

        Q_ASSERT( m_pRippleTips != NULL);
    }

    if( m_pRippleTips->isHidden() )
    {
        m_pRippleTips->show();
    }
    else
    {
        if( m_pRippleTips != NULL)
        {
            m_pRippleTips->hide();
        }
    }
}

void CAppUPA::onOptDetect()
{
   bool b = false;
   b = sysCheckLicense(OPT_PWR);
   if (b)
   {
       setState( app_installed );
   }
   else
   {
       setState( app_disable );
   }
}
void CAppUPA::onOptActive()
{
    onOptDetect();
}
void CAppUPA::onOptExp()
{
    onOptDetect();
}
void CAppUPA::onOptInvalid()
{
    onOptDetect();
}
void CAppUPA::onShowPowerQBox(void)
{
    bool show = false;

    query( serv_name_upa_powerq, MSG_UPA_POWER_DISP, show);

    if( show)
    {
        setPowerQVoltSrc();
        setPowerQCurrSrc();
        setPowerQRefSrc();
        m_pPowerQValueBox->show();
    }
    else
    {
        m_pPowerQValueBox->hide();
    }
}

void CAppUPA::setPowerQVoltSrc(void)
{
    int volt = 0;
    query(serv_name_upa_powerq, MSG_UPA_POWER_VOLT, volt);
    m_pPowerQValueBox->setVoltSrc((Chan)volt);
    bool ref = true;
    query(serv_name_upa_powerq, MSG_UPA_POWER_FREF, ref);
    if( !ref )
    {
        m_pPowerQValueBox->setRefSrc((Chan)volt);
    }
}

void CAppUPA::setPowerQCurrSrc(void)
{
    int curr = 0;
    query(serv_name_upa_powerq, MSG_UPA_POWER_CURR, curr);
    m_pPowerQValueBox->setCurrSrc((Chan)curr);
    bool ref = true;
    query(serv_name_upa_powerq, MSG_UPA_POWER_FREF, ref);
    if( ref )
    {
        m_pPowerQValueBox->setRefSrc((Chan)curr);
    }
}

void CAppUPA::setPowerQRefSrc(void)
{
    bool ref = true;
    query(serv_name_upa_powerq, MSG_UPA_POWER_FREF, ref);
    if( ref )
    {
        int curr = 0;
        query(serv_name_upa_powerq, MSG_UPA_POWER_CURR, curr);
        m_pPowerQValueBox->setRefSrc((Chan)curr);
    }
    else
    {        
        int volt = 0;
        query(serv_name_upa_powerq, MSG_UPA_POWER_VOLT, volt);
        m_pPowerQValueBox->setRefSrc((Chan)volt);
    }
}

void CAppUPA::onShowRippleBox()
{
    bool show = false;

    query( serv_name_upa_ripple, MSG_UPA_RIPPLE_DISP, show);
    if(show)
    {
        int src = (int)chan_none;
        query( serv_name_upa_ripple, MSG_UPA_RIPPLE_SOURCE, src);
        if( (Chan)src != chan_none)
        {
            m_pRippleValueBox->setSrc((Chan)src);
            m_pRippleValueBox->show();
        }
    }
    else
    {
        m_pRippleValueBox->hide();
    }
}

void CAppUPA::setRippleSrc()
{
    int src = (int)chan_none;
    query( serv_name_upa_ripple, MSG_UPA_RIPPLE_SOURCE, src);
    if( (Chan)src != chan_none)
    {
        m_pRippleValueBox->setSrc((Chan)src);
    }
}

void CAppUPA::upaUpdate()
{
    int type;
    query( serv_name_upa, servUPA::cmd_get_AnalysisType, type);

    switch (type)
    {
    case UPA_PowerQ:
        m_pPowerQValueBox->updatePowerQValue();
        break;
    case UPA_Ripple:
        m_pRippleValueBox->updateValue();
        break;
    default:
        break;
    }

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void CAppUPA::postCloseSig()
{
    serviceExecutor::post(serv_name_upa_powerq,
                           MSG_UPA_POWER_DISP,
                          false);
}

void CAppUPA::postRippleCloseSig()
{
    serviceExecutor::post(serv_name_upa_ripple,
                           MSG_UPA_RIPPLE_DISP,
                          false);
}
