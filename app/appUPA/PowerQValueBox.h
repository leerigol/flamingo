#ifndef POWERQVALUEBOX_H
#define POWERQVALUEBOX_H

#include <QWidget>
#include "../measure/cmeasValueItem.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;
class CPowerQValueBox : public uiWnd
{
    Q_OBJECT
public:
    explicit CPowerQValueBox(QWidget *parent = 0);

    void setupUi(QWidget* parent);
    void updatePowerQValue(void);

    void setVoltSrc(Chan volt);
    void setCurrSrc(Chan curr);
    void setRefSrc(Chan ref);

public slots:
    void hideThis();

protected:
   void paintEvent(QPaintEvent *event);
   void loadResource(const QString &path);

signals:
    void closeSig();

private:
   void setColor( QColor &color, int hexRgb );

private:
   static menu_res::RModelWnd_Style _style;
   RImageButton   *m_pCloseButton;

   QList<CMeasValueItem*>    m_MeasItems;

   int      m_nBoxX;
   int      m_nBoxY;
   int      m_nBoxW;
   int      m_nBoxH;

   int      m_nRowH;
   QRect    mCloseRect;
   QColor   mFontColor, mTitlebgColor;
   QColor   mGray15, mGray27;

};

#endif // POWERQVALUEBOX_H
