#include "powerqtipsdlg.h"


menu_res::RModelWnd_Style CUpaTipsDlg::_bgStyle;
CUpaTipsDlg::CUpaTipsDlg(UPA_Type type, QWidget */*parent*/)
{
    addAllKeys();
    set_attr( wndAttr, mWndAttr, wnd_moveable );
    setupUI( type);
}

CUpaTipsDlg::~CUpaTipsDlg()
{
    if( m_pCloseButton != NULL)
    {
        delete m_pCloseButton;
        m_pCloseButton = NULL;
    }
}

void CUpaTipsDlg::loadResource(QString path)
{
    r_meta::CRMeta rMeta;
    rMeta.getMetaVal(path + "closerect",    m_CloseRect);
    rMeta.getMetaVal(path + "titlerect",    m_TitleRect);
    rMeta.getMetaVal(path + "tipspicrect",  m_TipsPicRect);
    rMeta.getMetaVal(path + "tipspicpath",  m_TipsPicPath);

    QRect geo;
    rMeta.getMetaVal(path + "geo",  geo);

    move( geo.left(), geo.top());
    resize( geo.width(), geo.height());
}

void CUpaTipsDlg::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QRect frameRect = rect();

    _bgStyle.paint( painter, frameRect );
}

void CUpaTipsDlg::setupUI( UPA_Type type)
{
    if( type == UPA_PowerQ)
    {
        loadResource( "upa/powerq_tips/");
    }
    else if( type == UPA_Ripple)
    {
        loadResource( "upa/ripple_tips/");
    }
    else
    {
        loadResource( "upa/powerq_tips/");
    }

    _bgStyle.loadResource("ui/wnd/model/");

    m_Title.setParent(this);
    m_TipsPic.setParent(this);

    m_Title.setGeometry( m_TitleRect);
    m_Title.setText( "Tips");
    m_TipsPic.setGeometry( m_TipsPicRect);
    m_TipsPic.setPixmap( QPixmap( m_TipsPicPath));

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(m_CloseRect);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(close()) );

    this->setStyleSheet("QLabel{color: #c0c0c0;font-family:Arial; font:12pt;}");
}

void CUpaTipsDlg::keyPressEvent( QKeyEvent *)
{
}

void CUpaTipsDlg::keyReleaseEvent(QKeyEvent *)
{
    hide();
}
