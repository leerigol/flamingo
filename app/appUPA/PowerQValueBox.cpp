#include "PowerQValueBox.h"

#include "../../include/dsostd.h"
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"
#include "../../service/servmeasure/meas_dsrc.h"
#include "../../service/servmeasure/meas_interface.h"
#include "../../service/servUPA/upapowerqdata.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"

using namespace DsoType;

menu_res::RModelWnd_Style CPowerQValueBox::_style;

CPowerQValueBox::CPowerQValueBox(QWidget *parent) : uiWnd(parent)
{
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_Vrms)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_Irms)) );

    CMeasValueItem *ptr = new CMeasValueItem( CMeasItem( UPA_Real_P));
    ptr->setSrcA(m1);  //Math1 for UPA Real Power calc
    m_MeasItems.append(ptr);

    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_Apparent_P)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_Reactive_P)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_P_Factor)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_Ref_Freq)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_Phase_Angle)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_Imp)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_V_CrestFactor)) );
    m_MeasItems.append(new CMeasValueItem( CMeasItem( UPA_I_CrestFactor)) );

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void CPowerQValueBox::setupUi(QWidget* parent)
{
    loadResource("upa/powerq_values/");
    _style.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(mCloseRect);
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(hideThis()) );

    setParent(parent);
}

void CPowerQValueBox::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

void CPowerQValueBox::setVoltSrc(Chan volt)
{
    if(m_MeasItems.size() > 0)
    {
        for(int i=0; i<m_MeasItems.size(); ++i)
        {
            if(  (list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Vrms)
               ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Apparent_P)
               ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Reactive_P)
               ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_P_Factor)
               ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Phase_Angle)
               ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Imp)
               ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_V_CrestFactor) )
            {
                list_at( m_MeasItems,i)->setSrcA(volt);
            }
        }
    }
}

void CPowerQValueBox::setCurrSrc(Chan curr)
{
    if(m_MeasItems.size() > 0)
    {
        for(int i=0; i<m_MeasItems.size(); ++i)
        {
            if(  (list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Irms)
               ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_I_CrestFactor) )
            {
                list_at( m_MeasItems,i)->setSrcA(curr);
            }
            else if(  (list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Apparent_P)
                    ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Reactive_P)
                    ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_P_Factor)
                    ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Phase_Angle)
                    ||(list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Imp)  )
            {
                list_at( m_MeasItems,i)->setSrcB(curr);
            }
        }
    }
}

void CPowerQValueBox::setRefSrc(Chan ref)
{
    if(m_MeasItems.size() > 0)
    {
        for(int i=0; i<m_MeasItems.size(); ++i)
        {
            if( (list_at( m_MeasItems,i)->getItemInfo()->mType == UPA_Ref_Freq))
            {
                list_at( m_MeasItems,i)->setSrcA(ref);
            }
        }
    }
}

void CPowerQValueBox::hideThis()
{
    emit closeSig();
    hide();
}

void CPowerQValueBox::updatePowerQValue(void)
{
    if(m_MeasItems.size() > 0)
    {
        pointer pVal = NULL;
        MeasType UPAType = UPA_MAX;
//        serviceExecutor::query( serv_name_measure, servMeasure::MSG_UPA_POWERQ_DATA, pVal);
        serviceExecutor::query( serv_name_upa, servUPA::cmd_get_powerq_val, pVal);

        CUpaPowerQData *pPowerQVal = (CUpaPowerQData*)pVal;
        measStatData *pStatData = NULL;

        if( NULL != pPowerQVal)
        {
            QMap<MeasType,measStatData*> *pData =  pPowerQVal->getStatData();

            if( NULL != pData)
            {
                for(int i=0; i<m_MeasItems.size(); ++i)
                {
                    UPAType = list_at( m_MeasItems,i)->getItemInfo()->mType;
                    pStatData = pData->value(UPAType);

                    list_at( m_MeasItems,i)->setValue(pStatData);
                }
                if(isVisible())
                {
                    update();
                }
            }
        }
    }
}

void CPowerQValueBox::loadResource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path ;

    meta.getMetaVal(str + "row_h", m_nRowH);//16

    meta.getMetaVal(str + "x", m_nBoxX);//180
    meta.getMetaVal(str + "y", m_nBoxY);//120
    meta.getMetaVal(str + "w", m_nBoxW);//638
    meta.getMetaVal(str + "h", m_nBoxH);//16*11+25+4=205
    this->setGeometry(m_nBoxX, m_nBoxY, m_nBoxW, m_nBoxH);

    meta.getMetaVal(str + "closerect", mCloseRect);
    int num = 0;
    meta.getMetaVal(str + "fontcolor", num);
    setColor(mFontColor, num);
    meta.getMetaVal(str + "titlecolor", num);
    setColor(mTitlebgColor, num);
    meta.getMetaVal(str + "gray15", num);
    setColor(mGray15, num);
    meta.getMetaVal(str + "gray27", num);
    setColor(mGray27, num);
}
void CPowerQValueBox::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QFont font;
    font.setPointSize(11);
    painter.setFont(font);

    QRect r = this->geometry();
    int nWidth  = r.width();
    int nStartX = 9;//in central

    r.setX(0);
    r.setY(0);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    _style.paint(painter, r);

    QRect rLeftBox = r;
    rLeftBox.setX(9);
    rLeftBox.setY(39);
    rLeftBox.setWidth(r.width()-18);
    rLeftBox.setHeight(m_nRowH);
    painter.fillRect(rLeftBox.x(),rLeftBox.y(),
                     rLeftBox.width(),rLeftBox.height(), mTitlebgColor);

    for(int i=0; i<11; i++)
    {
        painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                         rLeftBox.width(),rLeftBox.height(), mGray15);
        i++;
        if( i<11)
        {
            rLeftBox.moveTo(rLeftBox.bottomLeft());
            painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                             rLeftBox.width(),rLeftBox.height(),  mGray27);
        }
        rLeftBox.moveTo(rLeftBox.bottomLeft());
    }

    r.setX(15);
    r.setY(7);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    QString  temp("%1"),val;
    QPen     pen;

    pen.setColor(QColor(mFontColor));
    painter.setPen(pen);

    painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, "Power Quality");

    r.setY(39);
    r.setX(r.x()+153);
    painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, "Curr");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, "Avg");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, "Max");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, "Min");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, "Dev");

    r.setX(r.x()+76);
    painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, "Cnt");

    r.setY(r.y() + m_nRowH-1);
    r.setX(nStartX);
    r.setHeight(m_nRowH-1);

    CMeasValueItem *p = NULL;
    for(int i=0; i<m_MeasItems.count(); i++)  //Power Quality: 14 items
    {
        p = list_at(m_MeasItems,i);

        pen.setColor(p->getItemInfo()->mSymbolColor);
        painter.setPen(pen);

        QRect r_name = r;
        r_name.setHeight(m_nRowH);

        //Draw Power Quality item name: symbol
        QString itemName = p->getItemInfo()->mSymbol
                         + p->getItemInfo()->mBracketL;
        painter.drawText(r_name, Qt::AlignLeft || Qt::AlignTop, itemName);

        QFontMetrics font_metric(font);
        QRect r_symbol = font_metric.boundingRect(r_name,Qt::TextWrapAnywhere, itemName);
        QRect r_symbol_srcA = font_metric.boundingRect(r_name,Qt::TextWrapAnywhere,
                                                              itemName + p->getItemInfo()->mSrcAStr);
        QRect r_symbol_and_srcA_con = font_metric.boundingRect(r_name,Qt::TextWrapAnywhere,
                                                        itemName + (p->getItemInfo()->mSrcAStr)
                                                                 + (p->getItemInfo()->mConnector) );
        QRect r_symbol_and_srcA_con_srcB = font_metric.boundingRect(r_name,Qt::TextWrapAnywhere,
                                                        itemName + (p->getItemInfo()->mSrcAStr)
                                                                 + (p->getItemInfo()->mConnector)
                                                                 + (p->getItemInfo()->mSrcBStr) );
        //Draw Power Quality item name: (source)
        pen.setColor(p->getItemInfo()->mSrcAColor);
        painter.setPen(pen);
        painter.drawText(r_symbol.x()+r_symbol.width(),
                         r_name.y(),r_symbol.width(),r_name.height(),
                         Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mSrcAStr);
        if( p->getItemInfo()->isDsrcType())
        {
            pen.setColor(p->getItemInfo()->mSymbolColor);
            painter.setPen(pen);
            painter.drawText(r_symbol_srcA.x()+r_symbol_srcA.width(),
                             r_name.y(),r_symbol_srcA.width(),r_name.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mConnector);
            pen.setColor(p->getItemInfo()->mSrcBColor);
            painter.setPen(pen);
            painter.drawText(r_symbol_and_srcA_con.x()+r_symbol_and_srcA_con.width(),
                             r_name.y(),r_symbol_and_srcA_con.width(),r_name.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mSrcBStr);
            pen.setColor(p->getItemInfo()->mSymbolColor);
            painter.setPen(pen);
            painter.drawText(r_symbol_and_srcA_con_srcB.x()+r_symbol_and_srcA_con_srcB.width(),
                             r_name.y()-1,r_symbol_and_srcA_con_srcB.width(),r_name.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mBracketR);
        }
        else
        {
            pen.setColor(p->getItemInfo()->mSymbolColor);
            painter.setPen(pen);
            painter.drawText(r_symbol_srcA.x()+r_symbol_srcA.width(),
                             r_name.y()-1,r_symbol_srcA.width(),r_name.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mBracketR);
        }

        //Draw Power Quality value
        r.setX(r.x()+160);
        painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, p->getCurr());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, p->getAvg());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, p->getMax());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, p->getMin());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, p->getDev());

        val = temp.arg(p->getCnt());
        r.setX(r.x()+76);
        painter.drawText(r, Qt::AlignLeft || Qt::AlignTop, val);

        r.setY(r.y() + m_nRowH-1);
        r.setX(nStartX);
        r.setHeight(m_nRowH-1);
    }
}

