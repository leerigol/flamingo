#ifndef CUPAPOWERQTIPSDLG_H
#define CUPAPOWERQTIPSDLG_H

#include "../../menu/wnd/rpopmenuchildwnd.h"
#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"
#include "../../service/servicefactory.h"

using namespace menu_res;
class CUpaTipsDlg :  public menu_res::RPopMenuChildWnd
{
    Q_OBJECT
public:
    CUpaTipsDlg( UPA_Type type,QWidget *parent = 0);
     ~CUpaTipsDlg();
    void loadResource(QString path);
    virtual void paintEvent( QPaintEvent *event );

    static menu_res::RModelWnd_Style _bgStyle;

public:
    void setupUI(UPA_Type type);

protected:
    void keyPressEvent( QKeyEvent *event );
    void keyReleaseEvent(QKeyEvent *event);

private:
    RImageButton   *m_pCloseButton;

    QLabel m_Title;
    QLabel m_TipsPic;

    QRect   m_CloseRect;
    QRect   m_TitleRect;
    QRect   m_TipsPicRect;

    QString m_TipsPicPath;
};

#endif // CUPAPOWERQTIPSDLG_H
