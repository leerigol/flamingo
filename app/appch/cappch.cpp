#include "cappch.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../service/servch/servch.h"

#include "../../service/servvertmgr/servvertmgr.h"

#include "../appmain/cappmain.h"

#include "../../service/servdso/sysdso.h"
#include "../../com/keyevent/rdsokey.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppCH )
start_of_entry()

on_set_void_void( MSG_CHAN_ON_OFF,            &CAppCH::on_enable_x_changed ),
on_set_void_void( servVert::user_cmd_on_off,  &CAppCH::on_enable_x_changed ),

on_set_void_void( MSG_CHAN_SCALE,   &CAppCH::on_scale_x_changed ),
on_set_void_void( MSG_CHAN_OFFSET,  &CAppCH::on_offset_x_changed ),

on_set_void_void( MSG_CHAN_PROBE,  &CAppCH::on_scale_x_changed ),
on_set_void_void( MSG_CHAN_PROBE,  &CAppCH::on_offset_x_changed ),

on_set_void_void( MSG_CHAN_UNIT,  &CAppCH::on_scale_x_changed ),
on_set_void_void( MSG_CHAN_UNIT,  &CAppCH::on_offset_x_changed ),

on_set_void_void( MSG_CHAN_IMPEDANCE, &CAppCH::on_imp_changed ),
on_set_void_void( MSG_CHAN_COUP,      &CAppCH::on_coupling_changed ),

on_set_void_void( MSG_CHAN_BWLIMIT,   &CAppCH::on_bw_changed ),
on_set_void_void( servCH::cmd_bw_apply, &CAppCH::on_bw_changed ),

on_set_void_void( MSG_CHAN_INVERT, &CAppCH::on_invert_changed ),

on_set_void_void( MSG_CHAN_ON_OFF, &CAppCH::on_label_changed ),
on_set_void_void( MSG_CHAN_OFFSET, &CAppCH::on_label_changed ),
on_set_void_void( MSG_CHAN_LABEL_NAME, &CAppCH::on_label_changed ),
on_set_void_void( MSG_CHAN_LABEL_EDIT, &CAppCH::on_label_changed ),
on_set_void_void( MSG_CHAN_LABEL_SHOW, &CAppCH::on_label_changed ),
on_set_void_void( MSG_CHAN_LABEL_SHOW, &CAppCH::on_offset_x_changed ),

//!probe
on_set_int_int( MSG_CHAN_PROBE_DETAIL,  &CAppCH::showProbeDetail),

//! system
on_set_void_void( CMD_SERVICE_ENTER_ACTIVE, &CAppCH::on_enter_active ),
//! spy
on_set_void_void( CAppCH::cmd_vert_active, &CAppCH::on_active_x_changed ),

on_set_void_void( MSG_HOR_TIME_MODE, &CAppCH::on_hori_mode_changed ),

on_set_void_void( CMD_SERVICE_RST, &CAppCH::on_rst ),

end_of_entry()

QColor CAppCH::_chColor[] =
{
    Qt::black,
    CH1_color,
    CH2_color,
    CH3_color,
    CH4_color,
};

QList<menu_res::RDsoGnd *> CAppCH::mGndList;
/*! \brief CAppCH
* 将通道id格式化为CH1/2/3/4形式作为app的标题和名称
* \param id 1,2,3,4
*/
CAppCH::CAppCH( Chan id )
{
    mName = QString("chan%1").arg(id-chan1+1);

    mControlTitle = QString("CH%1").arg(id-chan1+1);

    mservName = mName;

    mChan = id;

    m_pServCH = NULL;

    m_pProbe = NULL;

    //! physical link key
    if ( mChan == chan1 )
    {
        mScaleKey = R_Pkey_CH1_VOLT;
        mOffsetKey = R_Pkey_CH1_POS;
    }
    else if ( mChan == chan2 )
    {
        mScaleKey = R_Pkey_CH2_VOLT;
        mOffsetKey = R_Pkey_CH2_POS;
    }
    else if ( mChan == chan3 )
    {
        mScaleKey = R_Pkey_CH3_VOLT;
        mOffsetKey = R_Pkey_CH3_POS;
    }
    else if ( mChan == chan4 )
    {
        mScaleKey = R_Pkey_CH4_VOLT;
        mOffsetKey = R_Pkey_CH4_POS;
    }
    else
    {
        mScaleKey = 0;
        mOffsetKey = 0;
    }
}
/*! \brief setupUi
* 创建Ui
*/
void CAppCH::setupUi( CAppMain *pMain )
{
    QWidget *pWig;

    //! ch control
    if ( mChan == chan1 )
    {
        pDsoCH = new menu_res::RDsoCH(1);
    }
    else if ( mChan == chan2 )
    {
        pDsoCH = new menu_res::RDsoCH(2);
    }
    else if ( mChan == chan3 )
    {
        pDsoCH = new menu_res::RDsoCH(3);
    }
    else if ( mChan == chan4 )
    {
        pDsoCH = new menu_res::RDsoCH(4);
    }
    Q_ASSERT( pDsoCH != NULL );

    //! res load
    pDsoCH->loadResource( "ui/"+ QString("ch%1/").arg(mChan - chan1 + 1 ) );

    pWig = pMain->getDownBar();
    pDsoCH->setParent( pWig );

    pDsoCH->setHelp( mservName, MSG_CHAN_ON_OFF );

    //! ch gnd
    pWig = pMain->getLeftBar();
    pGnd = new menu_res::RDsoVGnd( QString("%1").arg(mChan - chan1 + 1 ));
    pGndZoom = new menu_res::RDsoVGnd( QString("%1").arg(mChan - chan1 + 1 ));

    Q_ASSERT( pGnd != NULL );
    Q_ASSERT( pGndZoom != NULL );
    mGndList.append(pGnd);
    mGndList.append(pGndZoom);

    pGnd->setParent(pWig);
    pGndZoom->setParent( pWig );

    //! gnd style
    _style.loadResource( "ui/"+ QString("ch%1/").arg(mChan - chan1 + 1 ));

    //! main gnd
    pGnd->setView( _style.mSize.width(), _style.mSize.height() );
    pGnd->setViewGnd( _style.mSize.height() / 2 -1 );

    //! zoom gnd
    pGndZoom->setView( _style.mSize.width(), _style.mSize.height() );
    pGndZoom->setViewGnd( _style.mSize.height() / 2 -1 );

    //! x gnd
    r_meta::CAppMeta appMeta;
    QRect xgndRect;
    appMeta.getMetaVal("appch/x_gnd", xgndRect );
    pGndX = new menu_res::RDsoHGnd( QString("%1").arg(mChan - chan1 + 1 ),
                                    menu_res::RDsoGndTag::to_down,
                                    menu_res::RDsoGndTag::cone,
                                    xgndRect.left(),xgndRect.top(),
                                    xgndRect.width(), xgndRect.height()
                                    );
    pGndY = new menu_res::RDsoVGnd( QString("%1").arg(mChan - chan1 + 1 ) );
    Q_ASSERT( NULL != pGndX );
    Q_ASSERT( NULL != pGndY );

    pGndX->setView( _style.mSize.height(), _style.mSize.width() );
    pGndX->setViewGnd( _style.mSize.height() / 2 );
    pGndX->setInvert( true );

    pGndY->setView( _style.mSize.width(), _style.mSize.height() );
    pGndY->setViewGnd( _style.mSize.height() / 2 -1 );
    pGndY->setAlign( Qt::AlignLeft );

    pWig = pMain->getCentBar();
    pGndX->setParent( pWig );
    pGndY->setParent( pWig );

    pGndX->setHelp( mservName, MSG_CHAN_OFFSET );
    pGndY->setHelp( mservName, MSG_CHAN_OFFSET );

    //! label
    pLabel = new menu_res::RDsoCHLabel();
    pLabelZoom = new menu_res::RDsoCHLabel();
    Q_ASSERT( NULL != pLabel );
    Q_ASSERT( NULL != pLabelZoom );
    pLabel->hide();
    pLabelZoom->hide();

    pLabel->setView( _style.mSize.width(), _style.mSize.height() );
    pLabel->setViewGnd( _style.mSize.height() / 2 );

    pLabelZoom->setView( _style.mSize.width(), _style.mSize.height() );
    pLabelZoom->setViewGnd( _style.mSize.height() / 2 );

    pLabel->setParent( pWig );
    pLabelZoom->setParent( pWig );

    pLabel->setHelp( mservName, MSG_CHAN_LABEL_EDIT );
    pLabelZoom->setHelp( mservName, MSG_CHAN_LABEL_EDIT );

    //! chCfg wnd
    m_pCfgWnd = new menu_res::WndChCfg( pWig );
    Q_ASSERT( NULL != m_pCfgWnd );

    //! probe
    m_pProbe = new menu_res::cappch_probe( pMain->getCentBar());
    Q_ASSERT( NULL != m_pProbe );
    m_pProbe->setup("appch/probe/", r_meta::CAppMeta());
    m_pProbe->setVisible(false);

    //! attach view
    sysAttachUIView( pGnd, view_ygnd_main );
    sysAttachUIView( pGndZoom, view_ygnd_zoom );

    sysAttachUIView( pGndX, view_xy_xgnd );
    sysAttachUIView( pGndY, view_xy_ygnd );

    sysAttachUIView( pLabel, view_yt_main );
    sysAttachUIView( pLabelZoom, view_yt_zoom );
}
/*! \brief 翻译Ui
*/
void CAppCH::retranslateUi()
{
    if ( m_pCfgWnd && m_pCfgWnd->isVisible() )
    {
         m_pCfgWnd->redraw();
    }
}

/*! \brief 连接signal,slot
*/
void CAppCH::buildConnection()
{
    connect( pDsoCH,SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT(on_dsoHeader_click(QWidget*)) );

    connect( pDsoCH,SIGNAL(sig_content_clicked(QWidget*)),
             this, SLOT(on_dsoContent_click(QWidget*)) );

    //! gnds
    connect( pGnd,SIGNAL(clicked(bool)),
             this, SLOT(on_dsognd_click()) );
    connect( pGndZoom,SIGNAL(clicked(bool)),
             this, SLOT(on_dsognd_click()) );
    connect( pGndX,SIGNAL(clicked(bool)),
             this, SLOT(on_dsognd_click()) );
    connect( pGndY,SIGNAL(clicked(bool)),
             this, SLOT(on_dsognd_click()) );

    connect( pGnd, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );
    connect( pGndZoom, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );
    connect( pGndX, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );
    connect( pGndY, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );

}

void CAppCH::resize()
{
    on_offset_x_changed( );
}

void CAppCH::registerSpy()
{
    spyOn( serv_name_vert_mgr,
           servVertMgr::cmd_active_chan,
           cmd_vert_active );

    spyOn( serv_name_hori,
           MSG_HOR_TIME_MODE,e_spy_pre );
}

void CAppCH::boundtoService( service *pServ )
{
    Q_ASSERT( pServ != NULL );

    //! convert
    m_pServCH = dynamic_cast< servCH*>( pServ );
    Q_ASSERT( m_pServCH != NULL );
}

void CAppCH::on_dsoHeader_click( QWidget */*pCaller*/ )
{
    //! 如果通道已经为active,则关闭通道
    if ( pDsoCH->getActive() )
    {
        serviceExecutor::post( mservName, MSG_CHAN_ON_OFF, (int)0 );
        return;
    }

    if ( pDsoCH->getOnOff() )
    {
        serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, (int)1 );
    }
    else
    {
        serviceExecutor::post( mservName, MSG_CHAN_ON_OFF, (int)1 );

        //set scale and offset when from OFF to ON
        //for kestrel by hxh
        int scale=0;
        serviceExecutor::query(mservName, MSG_CHAN_SCALE_VALUE, scale );
        serviceExecutor::post(mservName, MSG_CHAN_SCALE_VALUE, scale);

    }
}

void CAppCH::on_dsoContent_click( QWidget *pCaller )
{
    //! check enable
    if ( !pDsoCH->getOnOff() )
    {
        serviceExecutor::post( mservName, MSG_CHAN_ON_OFF, (int)1 );

        //set scale and offset when from OFF to ON
        //for kestrel by hxh
        int scale=0;
        serviceExecutor::query(mservName, MSG_CHAN_SCALE_VALUE, scale );
        serviceExecutor::post(mservName, MSG_CHAN_SCALE_VALUE, scale);

        return;
    }

    r_meta::CAppMeta meta;
    bool bSetup;
    //! ch config wnd
    bSetup = m_pCfgWnd->setup( "appch/wnd/", meta );
    if ( bSetup )
    {
        //! reconfig style
        QString node, style;

        node = QString( "appch/wnd/style/%1").arg(mName);
        meta.getMetaVal(  node, style );

        QString dft = m_pCfgWnd->styleSheet();
        m_pCfgWnd->setStyleSheet( dft+style );
        m_pCfgWnd->setTitleColor( m_pServCH->getColor() );

        m_pCfgWnd->setTitle( IDS_CHAN1 + (mChan - chan1) );
        m_pCfgWnd->setLabelId( MSG_CHAN_SCALE, MSG_CHAN_OFFSET );

        //! get range
        DsoRealGp scaleGp, offsetGp;
        m_pServCH->getOffsetRange( &offsetGp );
        m_pServCH->getScaleRange( &scaleGp );

        //! now contains in the gp
        m_pCfgWnd->setScaleGp( scaleGp );
        m_pCfgWnd->setOffsetGp( offsetGp );

        //! connect
        connect( m_pCfgWnd, SIGNAL(sig_scale_real(DsoReal)),
                 this, SLOT(on_scale_real(DsoReal)) );

        connect( m_pCfgWnd, SIGNAL(sig_offset_real(DsoReal)),
                 this, SLOT(on_offset_real(DsoReal)) );
        connect( m_pCfgWnd, SIGNAL(sig_tune(int)),
                 this, SLOT(on_tune(int)) );
    }

    //! switch the visible
    m_pCfgWnd->setShow( !m_pCfgWnd->isVisible() );

    //! arrange the pos, the init pos
    if ( bSetup )
    { m_pCfgWnd->arrangeOn( pCaller, priority_top, 20 ); }
}

void CAppCH::on_dsognd_click()
{
    if ( !pDsoCH->getActive() )
    {
        serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, (int)1 );
    }
    else
    {
        on_enter_active();
    }
}

void CAppCH::onMove( int /*xDist*/, int yDist )
{
    Q_ASSERT( m_pServCH != NULL );

    int offs;

    offs = m_pServCH->getOffset() - m_pServCH->getScale() * yDist / scr_vdiv_dots;

    serviceExecutor::post( mservName, MSG_CHAN_OFFSET, offs );
}

void CAppCH::on_scale_real( DsoReal real )
{
    serviceExecutor::post( mservName, servCH::cmd_scale_ui_dsoreal, real );
}
void CAppCH::on_offset_real( DsoReal real )
{
    serviceExecutor::post( mservName, servCH::cmd_offset_ui_dsoreal, real );
}

void CAppCH::on_tune( int id )
{
    switch( id )
    {
        case menu_res::WndChCfg::tune_scale_add:
            sysMimicInc( mScaleKey );
            break;

        case menu_res::WndChCfg::tune_scale_sub:
            sysMimicDec( mScaleKey );
            break;

        case menu_res::WndChCfg::tune_offset_add:
            sysMimicInc( mOffsetKey );
            break;

        case menu_res::WndChCfg::tune_offset_sub:
            sysMimicDec( mOffsetKey );
            break;

        default:
            break;
    }
}

void CAppCH::updateUnit()
{
    Q_ASSERT( m_pServCH != NULL );

    pDsoCH->setUnit( m_pServCH->getStringUnit() );

    m_pCfgWnd->setUnit( m_pServCH->getUnit() );
}

void CAppCH::on_enable_x_changed(  )
{
    Q_ASSERT( m_pServCH != NULL );
    bool val;

    val = m_pServCH->getOnOff();
    pDsoCH->setOnOff( val );
    pDsoCH->setVisible( true );
    if ( val == false )
    {
        pDsoCH->setActive(false);
    }

    if ( !val && m_pCfgWnd->isVisible() )
    {
        m_pCfgWnd->setShow(false);
    }

    pGnd->setShow( val );
    pGndZoom->setShow( val );

    val = val && m_pServCH->getLabelOnOff();
    pLabel->setShow( val );
    pLabelZoom->setShow( val );
}

void CAppCH::on_scale_x_changed(  )
{
    Q_ASSERT( m_pServCH != NULL );
    float val;

    //! value
    serviceExecutor::query( mservName, servCH::cmd_scale_real, val );
    pDsoCH->setScale( val );

    updateUnit();

    //! get range
    DsoRealGp scaleGp, offsetGp;
    m_pServCH->getOffsetRange( &offsetGp );
    m_pServCH->getScaleRange( &scaleGp );

    //! now contains in the gp
    m_pCfgWnd->setScaleGp( scaleGp );
    m_pCfgWnd->setOffsetGp( offsetGp );
}
void CAppCH::on_offset_x_changed( )
{
    float offset;
    float scale;
    int vOff;

    serviceExecutor::query( mservName, servCH::cmd_offset_real, offset );
    pDsoCH->setOffset( offset );

    serviceExecutor::query( mservName, servCH::cmd_scale_real, scale );
    pDsoCH->setScale( scale );

    m_pCfgWnd->setOffset( m_pServCH->getOffsetDsoReal() );

    //! cord convert
    //! \todo 10*10
    vOff = ( offset * _style.mSize.height() / (8*scale) );

    //! offset
    pGnd->offset( vOff );
    pGndZoom->offset( vOff );

    pGndX->offset( vOff );
    pGndY->offset( vOff );

    pLabel->offset( vOff );
    pLabelZoom->offset( vOff );

    updateUnit();

    //for GND sync with wave
    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void CAppCH::on_enter_active()
{
    if(pDsoCH->getActive())//bug 1973 qxl
    {
        pGnd->setAllInactive(mGndList);
        pGnd->setActive();
        pGndZoom->setActive();

        pGndX->setActive();
        pGndY->setActive();

        pLabel->setActive();
        pLabelZoom->setActive();

        pGndX->setActive();
        pGndY->setActive();
    }
//    LOG_DBG()<<m_pServCH->getName();

    CApp::on_enterActive();
}

void CAppCH::on_active_x_changed(  )
{
    int actiChan;
    Chan actChan;
    serviceExecutor::query( serv_name_vert_mgr,
                            servVertMgr::cmd_active_chan,
                            actiChan );

    actChan = (Chan)actiChan;

    Q_ASSERT( pDsoCH != NULL );
    pDsoCH->setActive( m_pServCH->getOnOff() &&
                       actChan == mChan );

    //! active now
    if ( actChan == mChan )
    {
        pGnd->setColor( _style.mActiveColor );
        pGndZoom->setColor( _style.mActiveColor );

        pGndX->setColor( _style.mActiveColor );
        pGndY->setColor( _style.mActiveColor );

        pLabel->setColor( _style.mActiveColor );
        pLabelZoom->setColor( _style.mActiveColor );

        on_enter_active();
    }
    else
    {
        pGnd->setColor( _style.mEnableColor );
        pGndZoom->setColor( _style.mEnableColor );

        pGndX->setColor( _style.mEnableColor );
        pGndY->setColor( _style.mEnableColor );

        pLabel->setColor( _style.mEnableColor );
        pLabelZoom->setColor( _style.mEnableColor );
    }
}

void CAppCH::on_imp_changed()
{
    bool imp;

    query( MSG_CHAN_IMPEDANCE, imp );

    pDsoCH->setImpedance( (Impedance)imp );
}
void CAppCH::on_coupling_changed()
{
    int coup;

    query( MSG_CHAN_COUP, coup );

    pDsoCH->setCoupling( (Coupling)coup );
}

void CAppCH::on_bw_changed()
{
    int bw;

    query( MSG_CHAN_BWLIMIT, bw );

    pDsoCH->setBw( bw != BW_OFF );
}
void CAppCH::on_invert_changed()
{
    bool inv;

    query( MSG_CHAN_INVERT, inv );

    pDsoCH->setInvert( inv );
}

void CAppCH::on_label_changed()
{
    bool bVisible;

    bVisible = m_pServCH->getOnOff()
            && m_pServCH->getLabelOnOff();


    pLabel->setColor( m_pServCH->getColor() );
    pLabelZoom->setColor( m_pServCH->getColor() );

    pLabel->setShow( bVisible );
    pLabelZoom->setShow( bVisible );

    pLabel->setText( m_pServCH->getLabel() );
    pLabelZoom->setText( m_pServCH->getLabel() );
}

void CAppCH::on_hori_mode_changed()
{
    enter_app_context();

    query_service_enum( serv_name_hori, MSG_HOR_TIME_MODE, AcquireTimemode, timeMode );
    if ( !_bOK ) return;

    //! time mode
    if ( timeMode == Acquire_XY )
    {
        pLabel->setShow( false );
        pLabelZoom->setShow( false );
    }
    else
    {
        pGndX->setShow( false );
        pGndY->setShow( false );
        return;
    }

    //! xy
    int xSrc, ySrc;
    int selfId;
    query( serv_name_hori, servHori::cmd_xy_src_x, xSrc );
    query( serv_name_hori, servHori::cmd_xy_src_y, ySrc );

    query( servCH::cmd_ch_id, selfId );

    if ( selfId == xSrc )
    {
        pGndX->setShow( true );
        pGndY->setShow( false );
    }
    else if ( selfId == ySrc )
    {
        pGndX->setShow( false );
        pGndY->setShow( true );
    }
    else
    {
        pGndX->setShow( false );
        pGndY->setShow( false );
    }

    exit_app_context();
}

void CAppCH::on_rst()
{
    if( NULL != m_pCfgWnd )
    {
        m_pCfgWnd->hide();// by hxh
        m_pCfgWnd->rstArrange();
    }
}

DsoErr CAppCH::showProbeDetail(AppEventCause cause)
{
    if(cause != cause_value)
    {
      m_pProbe->setVisible(false);
      return ERR_NONE;
    }

    m_pProbe->setVisible( !m_pProbe->isVisible() );
    if(!m_pProbe->isVisible()) return ERR_NONE;

    DsoErr err = ERR_NONE;
    pointer ptr = NULL;
    err = serviceExecutor::query( mservName,
                                  servCH::cmd_probe_info_ptr,
                                  ptr);
    if(err != ERR_NONE) return err;
    Q_ASSERT(ptr != NULL );

    OneWireProbeInfo *pProbePtr = static_cast<OneWireProbeInfo*>(ptr);
    Q_ASSERT(pProbePtr != NULL );

    //! [bug:rp7探头可以保存型号，但已经出售的没有写入型号，
    //! A:7150; B:7080; S:7150S; T:7080S。
    QRegExp    re("^(?:RP7)([ABST])",Qt::CaseInsensitive);
    QByteArray sn( (char*)pProbePtr->sn);
    int pos = re.indexIn(sn,0);
    if(pos != -1)
    {
        QString S    = re.cap(1).toUpper();
        QString mode = "RP7150";
        if(S.contains("A") )
        {
            mode = "RP7150";
        }
        else if(S.contains("B"))
        {
            mode = "RP7080";
        }
        else if(S.contains("S"))
        {
            mode = "RP7150S";
        }
        else if(S.contains("T"))
        {
            mode = "RP7080S";
        }
        //qDebug()<<__FILE__<<__LINE__<<S<<mode;

        memset(pProbePtr->model, 0, 8);
        memcpy(pProbePtr->model, mode.toLatin1().data(), qMin(mode.toLatin1().size(), 8));
    }

    m_pProbe->setProbInfo(pProbePtr);

    return err;
}

