#ifndef CAPPCH_H
#define CAPPCH_H

#include <QtCore>
#include <QtWidgets>

#include "../../menu/rmenus.h"  //! controlers
#include "../../menu/dsownd/wndchcfg.h"

#include "../capp.h"

#include "../../service/servch/servch.h"

#include "cappch_style.h"

#include "cappch_probe.h"

/*!
 * \brief The CAppCH class
 * the ui of the ch config
 */
class CAppCH : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    static QColor _chColor[];

    enum appCmd
    {
        cmd_base = CMD_APP_BASE,

        cmd_vert_active,
    };

public:
    CAppCH( Chan id= chan1 );

private:
    menu_res::RDsoCH *pDsoCH;       /*!< ch controller */

    menu_res::RDsoVGnd *pGnd;       /*!< main gnd */
    menu_res::RDsoVGnd *pGndZoom;
    static QList<menu_res::RDsoGnd *> mGndList;

    menu_res::RDsoVGnd *pGndY;      /*!< gnd x ,y */
    menu_res::RDsoHGnd *pGndX;
                                    //! labels
    menu_res::RDsoCHLabel *pLabel;
    menu_res::RDsoCHLabel *pLabelZoom;

    menu_res::WndChCfg *m_pCfgWnd;  //! wnd ch config
    menu_res::cappch_probe *m_pProbe; //!有源探头信息

    QString mControlTitle;
    Chan mChan;                     /*!< channel id */
    int  mScaleKey, mOffsetKey;

    servCH *m_pServCH;              /*!< service ch */

private:
    CAppCH_Style _style;

public:
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();

    virtual void buildConnection();
    virtual void resize();
    virtual void registerSpy();
    virtual void boundtoService( service *pServ );

protected Q_SLOTS:
    void on_dsoHeader_click( QWidget *pCaller );
    void on_dsoContent_click( QWidget *pCaller );

    void on_dsognd_click();

    void onMove( int xDist, int yDist );

    void on_scale_real( DsoReal );
    void on_offset_real( DsoReal );

    void on_tune( int id );
protected:
    void updateUnit();

public:
    void on_enable_x_changed();
    void on_scale_x_changed();
    void on_offset_x_changed();

    void on_enter_active();

    void on_active_x_changed();
    void on_imp_changed();
    void on_coupling_changed();
    void on_bw_changed();

    void on_invert_changed();

    void on_label_changed();

    void on_hori_mode_changed();

    void on_rst();

public:
    DsoErr showProbeDetail( AppEventCause cause );
};

#endif // CAPPCH_H
