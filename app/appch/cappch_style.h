#ifndef CAPPCH_STYLE_H
#define CAPPCH_STYLE_H

#include <QColor>

#include "../../menu/menustyle/rui_style.h"

class CAppCH_Style : public menu_res::RUI_Style
{
public:
    CAppCH_Style();

public:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta);

public:
    QSize mSize;
    QColor mActiveColor, mEnableColor, mDisableColor;
};

#endif // CAPPCH_STYLE_H
