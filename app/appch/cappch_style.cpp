#include "cappch_style.h"

#include "../../menu/menustyle/rcontrolstyle.h"
#include "../../meta/crmeta.h"

CAppCH_Style::CAppCH_Style()
{
}

void CAppCH_Style::load( const QString &path,
                         const r_meta::CMeta &meta )
{
    QString str;

    //! color
    str = path + "color/active";
    meta.getMetaVal( str, mActiveColor );

    str = path + "color/enable";
    meta.getMetaVal( str, mEnableColor );

    str = path + "color/disable";
    meta.getMetaVal( str, mDisableColor );

    //! gnd region
    str = path;
    menu_res::RControlStyle::routeToSibling( str, "/ch_gnd/view/" );
    meta.getMetaVal( str + "size", mSize );
}
