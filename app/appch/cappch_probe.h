#ifndef CAPPCH_PROBE_H
#define CAPPCH_PROBE_H
#include "../../menu/wnd/rinfownd.h"
#include "../../service/servch/servch.h"
#include "../../widget/rimagebutton.h"

namespace menu_res  {
class cappch_probe: public RInfoWnd
{
public:
    cappch_probe(QWidget *parent);

    virtual void paintEvent( QPaintEvent *event );

    static menu_res::RModelWnd_Style _bgStyle;

protected:
    virtual void showEvent( QShowEvent *event );
    virtual void setupUi( const QString &path,
                  const r_meta::CMeta &meta  );
    void    keyReleaseEvent(QKeyEvent *);
    void    keyPressEvent(QKeyEvent *);

private:
    QLabel  *m_pLabelMFRTitle;
    QLabel  *m_pLabelMFR;

    QLabel  *m_pLabelModelTitle;
    QLabel  *m_pLabelModel;

    QLabel  *m_pLabelSnTitle;
    QLabel  *m_pLabelSn;
//    QLabel  *m_pLabelZero;
//    QLabel  *m_pLabelGain;
//    QLabel  *m_pLabelOffs;
    QLabel  *m_pLabelDateTitle;
    QLabel  *m_pLabelDate;
    QLabel  *m_pLabel;

    RImageButton   *m_pCloseButton;

public:
    void    setProbInfo(OneWireProbeInfo *pInfo);
    void    setTitle( const QString &str );
};
}
#endif // CAPPCH_PROBE_H
