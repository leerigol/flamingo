#include "cappch_probe.h"
namespace menu_res  {
menu_res::RModelWnd_Style cappch_probe::_bgStyle;
cappch_probe::cappch_probe(QWidget *parent)
    : menu_res::RInfoWnd( parent )
{
    m_pLabelModelTitle = NULL;
    m_pLabelModel      = NULL;

    m_pLabelSnTitle    = NULL;
    m_pLabelSn         = NULL;

    //m_pLabelZero     = NULL;
    //m_pLabelGain     = NULL;
    //m_pLabelOffs     = NULL;

    m_pLabelDateTitle  = NULL;
    m_pLabelDate       = NULL;

    m_pLabelMFRTitle   = NULL;
    m_pLabelMFR        = NULL;
    m_pLabel           = NULL;

    addAllKeys();
    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void cappch_probe::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QRect frameRect = rect();

    _bgStyle.paint( painter, frameRect );

}

void cappch_probe::showEvent(QShowEvent *event)
{
    //! base
    RInfoWnd::showEvent( event );
}

void cappch_probe::setupUi(const QString &path, const r_meta::CMeta &meta)
{
    m_pLabelMFRTitle = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelMFRTitle );
    m_pLabelMFR = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelMFR );

    m_pLabelModelTitle = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelModelTitle );
    m_pLabelModel = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelModel );

    m_pLabelSnTitle    = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelSnTitle );
    m_pLabelSn    = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelSn );

//    m_pLabelZero  = new QLabel(this);
//    Q_ASSERT( NULL != m_pLabelZero );

//    m_pLabelGain  = new QLabel(this);
//    Q_ASSERT( NULL != m_pLabelGain );

//    m_pLabelOffs  = new QLabel(this);
//    Q_ASSERT( NULL != m_pLabelOffs );

    m_pLabelDateTitle  = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelDateTitle );
    m_pLabelDate  = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelDate );

    m_pLabel = new QLabel(this);

    _bgStyle.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(close()) );

    getGeo( meta, path + "geo/wnd",          this );

    getGeo( meta, path + "geo/titlexy",     m_pLabel );
    getGeo( meta, path + "geo/closerect",   m_pCloseButton );

    getGeo( meta, path + "geo/probe_MFR_title",    m_pLabelMFRTitle );
    getGeo( meta, path + "geo/probe_model_title",  m_pLabelModelTitle );
    getGeo( meta, path + "geo/probe_sn_title",     m_pLabelSnTitle );
    getGeo( meta, path + "geo/probe_date_title",   m_pLabelDateTitle );


    getGeo( meta, path + "geo/probe_MFR",    m_pLabelMFR );
    getGeo( meta, path + "geo/probe_model",  m_pLabelModel );
    getGeo( meta, path + "geo/probe_sn",     m_pLabelSn );
    getGeo( meta, path + "geo/probe_date",   m_pLabelDate );

    //! style
    QString str;
    meta.getMetaVal( path + "style", str );
    this->setStyleSheet( str );
}

void cappch_probe::keyReleaseEvent(QKeyEvent *)
{
    this->hide();
}

void cappch_probe::keyPressEvent(QKeyEvent *pKeyEvent)
{
    int key = pKeyEvent->key();
    if ( key != R_Pkey_F3 )
    {
        hide();
    }
}

void cappch_probe::setProbInfo(OneWireProbeInfo *pInfo)
{
    Q_ASSERT(pInfo != NULL);

    this->setTitle(  sysGetString(IDS_CHAN1+(pInfo->getId()-chan1), "CH1")
                     +sysGetString(MSG_CHAN_PROBE_DETAIL, "About") );

    m_pLabelMFRTitle->setText(   sysGetString(IDS_PROBE_MFR,      "厂商")+": "    );
    m_pLabelModelTitle->setText( sysGetString(IDS_PROBE_MODEL,    "型号")+": "    );
    m_pLabelSnTitle->setText(    sysGetString(IDS_PROBE_SN,       "序列号")+": "  );
    m_pLabelDateTitle->setText(  sysGetString(IDS_PROBE_CAL_TIME, "校准时间")+": " );

    m_pLabelMFR->setText(   "RIGOL TECHNOLOGIES");
    m_pLabelModel->setText( QByteArray( (char*)pInfo->model) );
    m_pLabelSn->setText(    QByteArray( (char*)pInfo->sn)    );
    quint32 dateTime = (pInfo->date[0]<<16) + pInfo->date[1];
    m_pLabelDate->setText( QString("%1-%2-%3 %4:%5:%6"  )
                           .arg( ( (dateTime>>(32-5-6-6-6))     &0x3f)+1980 )
                           .arg( ( (dateTime>>(32-5-6-6-6-4))   &0x0f)      )
                           .arg( ( (dateTime>>(32-5-6-6-6-4-5)) &0x1f)      )
                           .arg( ( (dateTime>>(32-5))           &0x1f)      )
                           .arg( ( (dateTime>>(32-5-6))         &0x3f)      )
                           .arg( ( (dateTime>>(32-5-6-6))       &0x3f)      )
                           );
    update();
}

void cappch_probe::setTitle( const QString &str )
{
    m_pLabel->setText( str );
}
}
