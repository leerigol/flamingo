#include "capphelp.h"

#include "../appmain/cappmain.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppHelp )
start_of_entry()

on_set_void_void(    MSG_APP_HELP_HELP,         &CAppHelp::setHelpShowOrHide),

on_set_void_void(    MSG_APP_HELP_OPTION,       &CAppHelp::setOptionShowOrHide),

on_set_void_void(    CAppHelp::cmd_Opt_update,  &CAppHelp::onOptUpdate),
on_set_void_void(    servHelp::cmd_opt_redraw,  &CAppHelp::onOptUpdate),

on_set_void_void(    servHelp::cmd_opt_show,    &CAppHelp::onOptShow),

on_set_void_void(    MSG_OPT_HELP_INFO,    &CAppHelp::onOptHelpShow),

on_set_void_void( CMD_SERVICE_HELP_REQUESST,    &CAppHelp::loadCurrContext ),

on_set_void_void( servHelp::cmd_online_upgradeWnd, &CAppHelp::onUpgradeWnd),

on_set_void_void( CAppHelp::cmd_lang_change,    &CAppHelp::on_lang_change),

end_of_entry()

CAppHelp::CAppHelp()
{
    mservName = serv_name_help;

    addQuick( MSG_APP_HELP,
              QStringLiteral("ui/desktop/help/icon/") );

    pHelp = NULL;
    pOptionList = NULL;
    pUpgradeWnd = NULL;
    pOptionHelp  = NULL;
    isLangChange = false;
}

void CAppHelp::setupUi( CAppMain *pMain )
{
    pHelp = new Help(pMain->getCentBar());
    Q_ASSERT( pHelp != NULL );
    pHelp->hide();

    pUpgradeWnd = new UpgradeWnd();
    connect(pUpgradeWnd, SIGNAL(btnClicked(int)), this, SLOT(onlineUpgradeSlot(int)));
    Q_ASSERT( pUpgradeWnd != NULL );
    pUpgradeWnd->hide();

}

void CAppHelp::retranslateUi()
{
    if( pHelp  )
    {
        bool b = pHelp->isVisible();
        if ( b )
        {
            pHelp->hide();
        }
        loadCurrContext();
        if( b )
        {
            pHelp->show();
        }
    }
}

void CAppHelp::buildConnection()
{
    connect(pHelp,SIGNAL(servChangedSignal(HelpContext*)),
            this,SLOT(servChangedSlot(HelpContext*)));
}

void CAppHelp::resize()
{

}

void CAppHelp::registerSpy()
{
    spyOn(serv_name_license, servLicense::cmd_Opt_Active,  CAppHelp::cmd_Opt_update);
    spyOn(serv_name_license, servLicense::cmd_Opt_Exp,     CAppHelp::cmd_Opt_update);
    spyOn(serv_name_license, servLicense::cmd_Opt_Invalid, CAppHelp::cmd_Opt_update);

    spyOn(serv_name_utility, MSG_APP_UTILITY_LANGUAGE,     CAppHelp::cmd_lang_change);
}

void CAppHelp::setHelpShowOrHide()
{
    bool b;
    serviceExecutor::query( serv_name_help, MSG_APP_HELP_HELP,  b);
    if(b)
    {
        loadCurrContext();
        pHelp->show();
    }
    else
    {
        pHelp->setClose();
    }
}

void CAppHelp::setOptionShowOrHide()
{
    if ( !isServiceActive() )
    {
        return;
    }

    if ( pOptionList == NULL )
    {
        pOptionList = new OptionList();
        Q_ASSERT( pOptionList != NULL );
    }

    if( (pOptionList != NULL) && (pOptionList->isVisible()) )
    {
        pOptionList->hide();
    }
    else
    {
        pointer p;
        serviceExecutor::query( serv_name_license,
                                servLicense::cmd_get_OptInfo,  p);
        pOptionList->setMap(p);
        pOptionList->setActive();
        pOptionList->show();
    }
}

void CAppHelp::onOptUpdate()
{
    if( pOptionList && pOptionList->isVisible())
    {
        pointer p;
        serviceExecutor::query( serv_name_license,
                                servLicense::cmd_get_OptInfo,  p);
        pOptionList->setMap(p);
        pOptionList->raise();
    }
}

void CAppHelp::onOptShow()
{
    if( pOptionList && pOptionList->isHidden())
    {
        pointer p;
        serviceExecutor::query( serv_name_license,
                                servLicense::cmd_get_OptInfo,  p);
        pOptionList->setMap(p);
        pOptionList->raise();
        pOptionList->show();
    }
}

void CAppHelp::onUpgradeWnd()
{
    bool b;
    serviceExecutor::query( mservName, MSG_APP_HELP_CHECK_NEW,  b);

    if(b)
    {
        pUpgradeWnd->hide();
    }
    else
    {
        pUpgradeWnd->setUpdate();
        pUpgradeWnd->show();
    }
}

void CAppHelp::on_lang_change()
{
    pHelp->clearContentList();
    if(!isLangChange)
    {
        if(pHelp->isVisible())
        {
            isLangChange = true;
            loadCurrContext();
        }
    }
}

void CAppHelp::onOptHelpShow()
{
    if ( pOptionHelp == NULL )
    {
        pOptionHelp = new OptionHelp();
    }

    if( pOptionHelp && pOptionHelp->isHidden())
    {
        pointer p;
        serviceExecutor::query( serv_name_license,
                                servLicense::cmd_get_OptInfo,  p);
        pOptionHelp->setMap(p);
        int opt = 0;
        serviceExecutor::query( mservName, MSG_OPT_HELP_INFO,  opt);
        pOptionHelp->setOpt(opt);
        pOptionHelp->raise();
        pOptionHelp->show();

    }
    else
    {
        pOptionHelp->hide();
    }
}

void CAppHelp::onlineUpgradeSlot(int v)
{
    serviceExecutor::post( mservName, servHelp::cmd_online_upgrade,  v);
}

void CAppHelp::loadCurrContext()
{
    pointer pContext;
    HelpContext *pHelpContext;
    serviceExecutor::query( serv_name_help, servHelp::cmd_help_context, pContext );
    pHelpContext = (HelpContext*)pContext;
    servChangedSlot(pHelpContext);
    pHelp->loadContextFrMsg(pHelpContext);
}

void CAppHelp::servChangedSlot(HelpContext *pHelpContext)
{
    pHelp->clearContentList();
    getHelpContent(pHelpContext->servName);

    pHelpContext->mhlpPath = getHelpFilePath(pHelpContext->servName);
    pHelpContext->mPicPath = getHelpPicPath(pHelpContext->servName);
    pHelp->setHelpItemsList(getHelpItemsList());
    pHelp->creatTreeListByTitle();
}

