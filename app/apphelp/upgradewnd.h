#ifndef UPGRADEWND_H
#define UPGRADEWND_H

#include <QWidget>
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;

class Browser : public QTextBrowser
{
public:
    explicit Browser(QWidget *parent = 0);
    void mouseMoveEvent(QMouseEvent *){}
    void mousePressEvent(QMouseEvent *){}
    void mouseReleaseEvent(QMouseEvent *){}
    void mouseDoubleClickEvent(QMouseEvent *){}
};

class UpgradeWnd : public uiWnd
{
    Q_OBJECT
public:
    explicit UpgradeWnd( );
    ~UpgradeWnd();
    void setTitle(QString& t);
    void setContent(QString& c){ mContent = c;}

    void setUpdate();

    void setGeoRect(QRect r);
    void setTitleRect(QRect r);
    void setInfoRect(QRect r);
    void setContentStr(QString str);

protected:
    void mouseMoveEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *evt);
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString path);

    void tuneInc( int keyCnt );
    void tuneDec( int keyCnt );
    void tuneZ( int keyCnt );

public:
    QPushButton *yesBtn;
    QPushButton *noBtn;
    RImageButton *m_pCloseButton;
    Browser     *contentBrowser;

signals:
    void btnClicked(int );

public slots:
    void emitYes();
    void emitNo();

public:
    QString egDeleted, egContinue, egOverWitten;
    QString egInvalidPkg, egInvalidVer;
    QString egDownload, egNoNewFW;

    QString chDeleted, chContinue, chOverWitten;
    QString chInvalidPkg, chInvalidVer;
    QString chDownload, chNoNewFW;

private:
    static menu_res::RModelWnd_Style _bgStyle;
    bool    isIconHidden;

    QString mTitle;
    QString mContent;
    QString yesQss;
    QString noQss;
    QString leftStr;
    QString midStr;
    QString rightStr;
    QString iconStr;
    QRect   iconRect;

    QRect   nGeo;
    QRect   nCloseRect;
    QRect   nYesRect;
    QRect   nNoRect;
    QRect   nTitleRect;
    QRect   nInfoRect;

    QString chUpgrade;
    QString egUpgrade;

};

#endif // UPGRADEWND_H
