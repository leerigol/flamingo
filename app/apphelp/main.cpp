#if 0
#include "help.h"
#include <QApplication>

#include "../../menu/rmenus.h"
#include "../../meta/crmeta.h"

#define meta_path   "/home/rigolee/workspace/framework/meta/platform_a/"
#define file_path   "/home/rigolee/workspace/framework/resource"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    r_meta::CRMeta::setMetaFile( meta_path"dsometa.xml" );
    r_meta::CBMeta::setMetaFile( meta_path"boardmeta.xml");
    menu_res::RMenuBuilder::setResourcePath( file_path );

    Help w;
    w.show();

    return a.exec();
}
#endif
