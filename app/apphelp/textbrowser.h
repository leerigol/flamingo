#ifndef TEXTBROWSER_H
#define TEXTBROWSER_H

#include <QTextBrowser>
#include <QScrollBar>
#include <QMouseEvent>
class TextBrowser : public QTextBrowser
{
    Q_OBJECT
public:
    explicit TextBrowser(QWidget *parent = 0);
    ~TextBrowser();

    int         getBarValue();
    void        setBarValue(int val);

protected:
    void        mouseMoveEvent(QMouseEvent * event);
    void        mousePressEvent(QMouseEvent * event);
    void        mouseReleaseEvent(QMouseEvent *event );

    void        setScrollBarStyle();

private:
    QPoint      point,point2;
    bool        boolMoved,boolFirst;
};

#endif // TEXTBROWSER_H
