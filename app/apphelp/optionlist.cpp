#include "optionlist.h"

menu_res::RModelWnd_Style OptionList::_style;
OptionList::OptionList(QWidget */*parent*/)
{
    loadSource("option/");
    _style.loadResource("ui/wnd/model/");
    addAllKeys();

    m_pOptInfoList = NULL;
    mCurrentItem = 0;
    set_attr( wndAttr, mWndAttr, wnd_moveable );

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(closeRect);
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(close()) );
}

OptionList::~OptionList()
{
}

void OptionList::setMap(pointer p)
{
    m_pOptInfoList = (QMap<OptType, COptInfo*> *)p;
    this->resize(m_width, 58+m_rowh*m_pOptInfoList->count());

    update();
}

void OptionList::loadSource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path  ;

    meta.getMetaVal(str + "x", m_x);
    meta.getMetaVal(str + "y", m_y);
    meta.getMetaVal(str + "w", m_width);
    meta.getMetaVal(str + "h", m_height);
    this->setGeometry(m_x, m_y, m_width, m_height);

    meta.getMetaVal(str + "closerect", closeRect);
    meta.getMetaVal(str + "m_rowh", m_rowh);
    int numc = 0;
    meta.getMetaVal(str + "fontcolor", numc);
    setColor(mFontColor, numc);
    meta.getMetaVal(str + "titlecolor", numc);
    setColor(mTitlebgColor, numc);
    meta.getMetaVal(str + "gray15", numc);
    setColor(mGray15, numc);
    meta.getMetaVal(str + "gray27", numc);
    setColor(mGray27, numc);
}

void OptionList::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    QRect    rect;
    rect.setX(0);
    rect.setY(0);
    rect.setWidth(this->geometry().width());
    rect.setHeight(this->geometry().height());
    _style.paint(painter, rect);

    QRect rLeftBox = rect;
    rLeftBox.setX(9);
    rLeftBox.setY(39);
    rLeftBox.setWidth(rect.width()-18);
    rLeftBox.setHeight(m_rowh);
    painter.fillRect(rLeftBox.x(),rLeftBox.y(),
                     rLeftBox.width(),rLeftBox.height(), mTitlebgColor);

    for(int i=0; i<m_pOptInfoList->count(); i++)
    {
        painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                         rLeftBox.width(),rLeftBox.height(), mGray15);
        i++;
        if( i<m_pOptInfoList->count())
        {
            rLeftBox.moveTo(rLeftBox.bottomLeft());
            painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                             rLeftBox.width(),rLeftBox.height(),  mGray27);
        }
        rLeftBox.moveTo(rLeftBox.bottomLeft());
    }

    QFont saved = painter.font();
    painter.setFont(QFont("Droid Sans Fallback",11));

    painter.setPen(QPen(mFontColor));
    painter.drawText(15,4,200,25,Qt::AlignLeft|Qt::AlignVCenter,
                     sysGetString( MSG_APP_HELP_OPTION, "Option list"));

    QStringList Titel;
    Titel << "Name" << "Licence" << "Info";

    int space = 15;
    rect.setY( 39);
    QPen   pen;
    pen.setColor(mFontColor);
    painter.setPen(pen);
    foreach(QString strTitel, Titel)
    {
        rect.setX(rect.x() + space);
        painter.drawText(rect, Qt::AlignLeft | Qt::AlignTop, strTitel);
        space = 85;
    }

    QRect    r;
    r.setX(0);
    r.setY(61);
    r.setWidth(100);
    r.setHeight(20);

    if( m_pOptInfoList != NULL )
    {
        QList<COptInfo*> values = m_pOptInfoList->values();
        for(int i=mCurrentItem; i<values.size() &&
                                i<(mCurrentItem+m_pOptInfoList->count()); i++)
        {
            COptInfo *item = values.at(i);
            if( item != NULL )
            {
                drawItem(item, r, painter);
                r.moveTo(r.x(), r.y()+20);
            }
        }
    }

    painter.setFont( saved );
}

void OptionList::keyReleaseEvent(QKeyEvent */*pKeyEvent*/)
{
    this->hide();
}

void OptionList::keyPressEvent(QKeyEvent *event)
{
    if ( event->key() != R_Pkey_F3 )
    {
        hide();
    }
}

void OptionList::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

void OptionList::drawItem(COptInfo *item, QRect r, QPainter &painter )
{
    if(item)
    {
        r.setX(r.x()+15);
        painter.setPen(QPen(QColor(200,200,200,255)));
        QString str = item->getName();
        if(str == "DG")
        {
            str = "AWG";
        }
        else
        {
            str = item->getName();
        }
        painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, str );

        r.setX(r.x()+85);
        r.setWidth(100);        
        if( item->isEnable())
        {
            if( item->getBndEable())
            {
                painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, "Forever");
            }
            else if( LicTime_Forever == item->getLicenseTime())
            {
                painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, "Forever");
            }
            else if(LicTime_Limit == item->getLicenseTime())
            {
                QString remaintime = QString("%1").arg(item->getRemainTime());
                remaintime += "min";
                painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, remaintime);
            }
            else
            {
                painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, "Limit");
            }
        }
        else
        {
            painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, "----");
        }

        r.setX(r.x()+85);
        r.setWidth(500);
        painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, sysGetString(item->getInfoID()));
    }
}

void OptionList::onActionTriggered(int num)
{
    mCurrentItem = num;
    update();
}


