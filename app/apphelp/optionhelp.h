#ifndef OPTIONHELP_H
#define OPTIONHELP_H

#include <QWidget>
#include "../../service/servlicense/servlicense.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../menu/wnd/rpopmenuchildwnd.h"
#include "../../widget/rimagebutton.h"
#include "../../service/servicefactory.h"
#include "../../meta/crmeta.h"

using namespace menu_res;
class OptionHelp : public menu_res::RPopMenuChildWnd
{
    Q_OBJECT

public:
    explicit OptionHelp( QWidget *parent = 0);
    ~OptionHelp();
    void setMap(pointer p);
    void setOpt(int opt);

protected:
    void    loadSource(const QString &path);
    void    paintEvent( QPaintEvent * );
    void    keyReleaseEvent(QKeyEvent *);
    void    keyPressEvent(QKeyEvent *);
    void    setColor( QColor &color, int hexRgb );
    void    drawItem(COptInfo *item, QRect r, QPainter &painter);

public slots:
    void    onActionTriggered(int num);

private:
    static menu_res::RModelWnd_Style _style;
    RImageButton   *m_pCloseButton;

private:
    int     m_x;
    int     m_y;
    int     m_width;
    int     m_height;
    int     m_rowh;

    QRect   closeRect;
    QColor  mFontColor, mTitlebgColor, mGray15, mGray27;

    QMap<OptType, COptInfo*> *m_pOptInfoList;
    OptType  mOptType;

    int     mCurrentItem;
};

#endif // OPTIONHELP_H

