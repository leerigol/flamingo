#include "optionhelp.h"

menu_res::RModelWnd_Style OptionHelp::_style;
OptionHelp::OptionHelp(QWidget */*parent*/)
{
    loadSource("option/");
    _style.loadResource("ui/wnd/model/");
    addAllKeys();

    m_pOptInfoList = NULL;

    mCurrentItem = 0;
    set_attr( wndAttr, mWndAttr, wnd_moveable );

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(closeRect);
    m_pCloseButton->move(m_pCloseButton->x()-100,m_pCloseButton->y());
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(close()) );
}

OptionHelp::~OptionHelp()
{
}

void OptionHelp::setMap(pointer p)
{
    m_pOptInfoList = (QMap<OptType, COptInfo*> *)p;
//    this->resize(m_width, 56+m_rowh*m_pOptInfoList->count());

    update();
}

void OptionHelp::setOpt(int opt)
{
    mOptType = (OptType)opt;
    update();
}

void OptionHelp::loadSource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path  ;

    meta.getMetaVal(str + "x", m_x);
    meta.getMetaVal(str + "y", m_y);
    meta.getMetaVal(str + "w", m_width);
    meta.getMetaVal(str + "h", m_height);
    this->setGeometry(m_x, 180, m_width-100, 180);

    meta.getMetaVal(str + "closerect", closeRect);
    meta.getMetaVal(str + "m_rowh", m_rowh);
    int numc = 0;
    meta.getMetaVal(str + "fontcolor", numc);
    setColor(mFontColor, numc);
    meta.getMetaVal(str + "titlecolor", numc);
    setColor(mTitlebgColor, numc);
    meta.getMetaVal(str + "gray15", numc);
    setColor(mGray15, numc);
    meta.getMetaVal(str + "gray27", numc);
    setColor(mGray27, numc);
}

void OptionHelp::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    QRect    rect;
    rect.setX(0);
    rect.setY(0);
    rect.setWidth(this->geometry().width());
    rect.setHeight(this->geometry().height());
    _style.paint(painter, rect);

    QFont saved = painter.font();
    painter.setFont(QFont("Droid Sans Fallback",11));

    painter.setPen(QPen(mFontColor));
    painter.drawText(15,4,200,25,
                     Qt::AlignLeft|Qt::AlignVCenter,
                     sysGetString( MSG_OPT_HELP_TITLE, "Required option"));

    painter.drawText(25,60,500,30,
                     Qt::AlignLeft|Qt::AlignVCenter,
                     sysGetString( MSG_OPT_HELP_CONT, "This function requires the following license:"));

    QRect    r;
    r.setX(10);
    r.setY(120);
    r.setWidth(100);
    r.setHeight(30);

    if( m_pOptInfoList != NULL )
    {
        COptInfo *item = m_pOptInfoList->value(mOptType);
        drawItem(item, r, painter);
    }

    painter.setFont( saved );
}

void OptionHelp::keyReleaseEvent(QKeyEvent */*pKeyEvent*/)
{
    this->hide();
}

void OptionHelp::keyPressEvent(QKeyEvent */*pKeyEvent*/)
{
    this->hide();
}

void OptionHelp::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

void OptionHelp::drawItem(COptInfo *item, QRect r, QPainter &painter )
{
    if(item)
    {
        r.setX(r.x()+55);
        painter.setPen(QPen(QColor(200,200,200,255)));
        QString str = item->getName();
        if(str == "DG")
        {
            str = "AWG";
        }
        else
        {
            str = item->getName();
        }
        painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, str );

        r.setX(r.x()+85);
        r.setWidth(500);
        painter.drawText(r, Qt::AlignLeft|Qt::AlignTop, sysGetString(item->getInfoID()));
    }
}

void OptionHelp::onActionTriggered(int num)
{
    mCurrentItem = num;
    update();
}


