#-------------------------------------------------
#
# Project created by QtCreator 2016-11-17T10:49:42
#
#-------------------------------------------------
QT       += core
QT       += gui
QT       += widgets
QT       += network
QT       += xml

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/apps/apphelp
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

DEFINES += _DSO_KEYBOARD

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


# Input
HEADERS += capphelp.h help.h treeWidget.h textbrowser.h \
    iapphelp.h \
    optionlist.h \
    upgradewnd.h \
    optionhelp.h
SOURCES += capphelp.cpp help.cpp treeWidget.cpp textbrowser.cpp \
    iapphelp.cpp \
    optionlist.cpp \
    upgradewnd.cpp \
    optionhelp.cpp
