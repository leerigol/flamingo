#ifndef OPTIONLIST_H
#define OPTIONLIST_H

#include <QWidget>
#include "../../service/servlicense/servlicense.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../menu/wnd/rpopmenuchildwnd.h"
#include "../../widget/rimagebutton.h"
#include "../../service/servicefactory.h"
#include "../../meta/crmeta.h"

using namespace menu_res;
class OptionList : public menu_res::RPopMenuChildWnd
{
    Q_OBJECT

public:
    explicit OptionList(QWidget *parent = 0);
    ~OptionList();
    void setMap(pointer p);

protected:
    void    loadSource(const QString &path);
    void    paintEvent( QPaintEvent * );
    void    keyReleaseEvent(QKeyEvent *);
    void    keyPressEvent(QKeyEvent *);
    void    setColor( QColor &color, int hexRgb );
    void    drawItem(COptInfo *item, QRect r, QPainter &painter);

public slots:
    void    onActionTriggered(int num);

private:
    static menu_res::RModelWnd_Style _style;
    RImageButton   *m_pCloseButton;

private:
    int     m_x;
    int     m_y;
    int     m_width;
    int     m_height;
    int     m_rowh;

    QRect   closeRect;
    QColor  mFontColor, mTitlebgColor, mGray15, mGray27;

    QMap<OptType, COptInfo*> *m_pOptInfoList;

    int     mCurrentItem;
};

#endif // OPTIONLIST_H

