#ifndef TREEWIDGET_H
#define TREEWIDGET_H
#include <QWidget>
#include <QTreeWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QScrollBar>
#include <QHeaderView>
class TreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit    TreeWidget(QWidget *parent = 0);
    ~TreeWidget();

    int         getBarValue();
    void        setBarValue(int val);

    QTreeWidgetItem   * itemFrIndex(const QModelIndex & index);
    QModelIndex         indexFrItem(QTreeWidgetItem * item, int column = 0);

protected:
    void        mouseMoveEvent(QMouseEvent * event);
    void        mousePressEvent(QMouseEvent * event);
    void        mouseReleaseEvent(QMouseEvent *event );

    void        setScrollBarStyle();
private:
    QPoint      point,point2;
    bool        boolMoved,boolFirst;
    int         barValue;
    
};

#endif // LINEWIDGET_H
