
#include "iapphelp.h"
#include <QDomDocument>
#include <QFile>

IAppHelp::IAppHelp()
{
    mInited = false;
    loadPathFrResource("help/");
}

void IAppHelp::getHelpContent(QString servName)
{
    clear();
    QString fileName = getHelpFilePath(servName);
    QDomDocument doc("mydocument");
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)){
        return ;
    }
    if (!doc.setContent(&file)){
        file.close();
        return;
    }
    file.close();

    QDomElement docElem = doc.documentElement();
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement();
        if(!e.isNull()) {
            if("name" == e.tagName()){

            }else if("items" == e.tagName()){

                QDomNode n = e.firstChild();
                while(!n.isNull()) {
                    QDomElement itemEle = n.toElement();
                    if("item" == itemEle.tagName()){
                        QDomNode n = itemEle.firstChild();
                        HelpItem *helpItem = new HelpItem;
                        while(!n.isNull()) {
                            QDomElement itemEle = n.toElement();
                            if("msg" == itemEle.tagName()){
                                helpItem->msg = itemEle.text().toInt();
                            }
                            if("relation" == itemEle.tagName()){
                                helpItem->relationList = itemEle.text().simplified().split(",",QString::SkipEmptyParts);
                            }
                            if("title" == itemEle.tagName()){
                                helpItem->title = itemEle.text();
                            }
                            if("key" == itemEle.tagName()){
                                helpItem->keyList = itemEle.text().simplified().split(",",QString::SkipEmptyParts);
                            }
                            if("option" == itemEle.tagName()){
                                helpItem->option = itemEle.text();
                            }
                            if("sibling" == itemEle.tagName()){
                                helpItem->siblingList = itemEle.text().simplified().split(",",QString::SkipEmptyParts);
                            }
                            if("content" == itemEle.tagName()){
                                helpItem->content = itemEle.text();
                            }
                            n = n.nextSibling();
                        }
                        listHelpItems.append(helpItem);
                    }
                    n = n.nextSibling();
                }
            }
        }
        n = n.nextSibling();
    }
}

void IAppHelp::getHelpContent(QString servName, QString )
{
    clear();
    QString fileName = getHelpFilePath(servName);
    QDomDocument doc("mydocument");
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return ;
    if (!doc.setContent(&file)){
        file.close();
        return;
    }
    file.close();

    QDomElement docElem = doc.documentElement();
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement();
        if(!e.isNull()) {
            if("name" == e.tagName()){

            }else if("items" == e.tagName()){

                QDomNode n = e.firstChild();
                while(!n.isNull()) {
                    QDomElement itemEle = n.toElement();
                    if("item" == itemEle.tagName()){
                        QDomNode n = itemEle.firstChild();
                        HelpItem *helpItem = new HelpItem;
                        while(!n.isNull()) {
                            QDomElement itemEle = n.toElement();
                            if("key" == itemEle.tagName()){
                                helpItem->keyList = itemEle.text().simplified().split(",",QString::SkipEmptyParts);
                            }
                            if("msg" == itemEle.tagName()){
                                helpItem->msg = itemEle.text().toInt();
                            }
                            if("relation" == itemEle.tagName()){
                                helpItem->relationList = itemEle.text().simplified().split(",",QString::SkipEmptyParts);
                            }
                            if("title" == itemEle.tagName()){
                                helpItem->title = itemEle.text();
                            }
                            if("option" == itemEle.tagName()){
                                helpItem->option = itemEle.text();
                            }
                            if("sibling" == itemEle.tagName()){
                                helpItem->siblingList = itemEle.text().simplified().split(",",QString::SkipEmptyParts);
                            }
                            if("content" == itemEle.tagName()){
                                helpItem->content = itemEle.text();
                            }
                            n = n.nextSibling();
                        }
                        if(helpItem->option != "")
                            listHelpItems.append(helpItem);
                    }
                    n = n.nextSibling();
                }
            }
        }
        n = n.nextSibling();
    }
}

QList<HelpItem *> IAppHelp::getHelpItemsList()
{
    return listHelpItems;
}

QString IAppHelp::getHelpFilePath(QString servName)
{
    QString strPath;
    SystemLanguage lang = sysGetLanguage();

    if(mMapFile.contains(servName))
    {
        strPath = mMapFile.value(servName);
        if(language_chinese == lang ||
           language_traditional_chinese == lang)
        {
            strPath = sysGetResourcePath() + mChinese + strPath;
        }else
        {
            strPath = sysGetResourcePath() + mEnglish + strPath;
        }
    }
    return strPath;
}

QString IAppHelp::getHelpPicPath(QString servName)
{
    QString strPic;
    if(mMapPic.contains(servName)){
        strPic = mMapPic.value(servName);
        strPic = sysGetResourcePath() + strPic;
    }
    return strPic;
}

void IAppHelp::clear()
{
    for(int i=0; i<listHelpItems.count(); i++){
        HelpItem *item = listHelpItems.at(i);
        delete item;
        item = NULL;
    }
    listHelpItems.clear();
}

void IAppHelp::loadPathFrResource(const QString &root)
{
    if ( mInited ) return;

    r_meta::CAppMeta rMeta;
    QString pathLang = root + "langpath/";
    rMeta.getMetaVal(pathLang + "chinese", mChinese);
    rMeta.getMetaVal( pathLang + "english", mEnglish );


    int num = 0;
    rMeta.getMetaVal(root + "number", num);
    QString sname, pname, path;
    for(int i=0; i<num; i++){
        QString str = root + "item_" + QString("%1").arg(i, 2, 10, QChar('0'));
        rMeta.getMetaVal( str+"/sname", sname);
        rMeta.getMetaVal( str+"/pname", pname);
        rMeta.getMetaVal( str+"/path",  path);

        mMapFile[sname] = pname;
        mMapPic[sname] = path;
    }

    mInited = true;
}



