#ifndef IAPPHELP_H
#define IAPPHELP_H

#include <QtCore>
#include <QtWidgets>

#include "../../menu/rmenus.h"

#include "../../service/service_name.h"

class HelpItem
{
public:
    int         msg;
    QStringList relationList;
    QString     title;
    QStringList keyList;
    QString     option;
    QStringList siblingList;
    QString     content;

    HelpItem &operator=(const HelpItem &other){
        this->msg           = other.msg;
        this->relationList  = other.relationList;
        this->title         = other.title;
        this->keyList       = other.keyList;
        this->option        = other.option;
        this->siblingList   = other.siblingList;
        this->content       = other.content;
        return *this;
    }
};

class IAppHelp
{

public:
    IAppHelp();

    void  getHelpContent(QString servName);
    void  getHelpContent(QString servName, QString);

    QList<HelpItem *> getHelpItemsList();

    QString    getHelpFilePath(QString servName);
    QString    getHelpPicPath(QString servName);

protected:
    void    clear();
    void    loadPathFrResource(const QString &root);

private:
    QList<HelpItem *> listHelpItems;

    bool            mInited;
    QString         mChinese;
    QString         mEnglish;

    QMap<QString, QString>  mMapFile;
    QMap<QString, QString>  mMapPic;


};

#endif // IAPPHELP_H
