#include "textbrowser.h"

TextBrowser::TextBrowser(QWidget *parent) :
    QTextBrowser(parent)
{
    boolMoved = false;
    boolFirst = true;

    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setStyleSheet("color:rgb(180,180,180);background:rgb(0, 0, 0);border: 0px solid #32435E;");

    setScrollBarStyle();
}

TextBrowser::~TextBrowser()
{

}

void TextBrowser::setScrollBarStyle()
{
    this->verticalScrollBar()->setStyleSheet("QScrollBar:vertical\
                {\
                    width:8px;\
                    background:rgba(50,50,50,100%);\
                    margin:0px,0px,0px,0px;\
                }\
                QScrollBar::handle:vertical\
                {\
                    width:8px;\
                    background:rgba(100,100,100,50%);\
                    border-radius:4px;  \
                    min-height:20;\
                }\
                QScrollBar::handle:vertical:hover\
                {\
                    width:8px;\
                    background:rgba(100,100,100,25%);  \
                    border-radius:4px;\
                    min-height:20;\
                }\
                QScrollBar::add-line:vertical  \
                {\
                    height:9px;width:8px;\
                    border:none;\
                    subcontrol-position:bottom;\
                }\
                QScrollBar::sub-line:vertical  \
                {\
                    height:9px;width:8px;\
                    border:none;\
                    subcontrol-position:top;\
                }\
                QScrollBar::add-line:vertical:hover \
                {\
                    height:9px;width:8px;\
                    border:none;\
                    subcontrol-position:bottom;\
                }\
                QScrollBar::sub-line:vertical:hover \
                {\
                   height:9px;width:8px;\
                    border:none;\
                   subcontrol-position:top;\
                }\
                QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical   \
                {\
                    background:rgba(0,0,0,10%);\
                    border-radius:4px;\
                }");
}

int TextBrowser::getBarValue()
{
    return this->verticalScrollBar()->value();
}

void TextBrowser::setBarValue(int val)
{
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + val);
    update();
}

void TextBrowser::mouseMoveEvent(QMouseEvent *event )
{
    if(event->buttons() & Qt::LeftButton){
        int y = event->y();
        int numberSteps = (point.y()-y);
        if(numberSteps < 0){
            if(abs(numberSteps)/10 > 0 ){
                if(boolFirst){
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + numberSteps);
                    boolFirst = false;
                }else{
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + point2.y()-y);

                }
                boolMoved = true;
            }
        }else{
            if(abs(numberSteps)/10 > 0 ){
                if(boolFirst){
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + numberSteps);
                    boolFirst = false;
                }else{
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + point2.y()-y);
                }
                boolMoved = true;
            }
        }
        point2.setY(event->y());
    }
}

void TextBrowser::mousePressEvent(QMouseEvent *event )
{
    if(event->buttons() & Qt::LeftButton){
        point.setY(event->y());
        boolMoved = false;
        event->accept();
    }

    QTextBrowser::mousePressEvent(event);
}

void TextBrowser::mouseReleaseEvent(QMouseEvent *event )
{
    boolFirst = true;
    if(!boolMoved)
        QTextBrowser::mouseReleaseEvent(event);
}
