#ifndef CAPPHELP_H
#define CAPPHELP_H

#include <QtCore>
#include <QtWidgets>

#include "../capp.h"

#include "../../menu/rmenus.h"
#include "help.h"
#include "optionlist.h"
#include "optionhelp.h"
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "iapphelp.h"
#include "upgradewnd.h"

class CAppHelp : public CApp, public IAppHelp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    CAppHelp();

    enum ListenerEvents
    {
        cmd_base = CMD_APP_BASE,

        cmd_Opt_update,
        cmd_lang_change,
    };

public:
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();

    virtual void buildConnection();
    virtual void resize();
    virtual void registerSpy();

public Q_SLOTS:
    void loadCurrContext();
    void servChangedSlot(HelpContext *pHelpContext);
    void onlineUpgradeSlot(int v);

protected:
    void setHelpShowOrHide();
    void setOptionShowOrHide();

    void onOptUpdate();
    void onOptShow();

    void onUpgradeWnd();

    void on_lang_change();

    void onOptHelpShow();

private:
     Help       *pHelp;
     OptionList *pOptionList;
     OptionHelp *pOptionHelp;
     UpgradeWnd *pUpgradeWnd;

     bool       isLangChange;


};

#endif // CAPPHELP_H
