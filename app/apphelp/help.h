#ifndef HELP_H
#define HELP_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QMutex>
#include "treeWidget.h"
#include "textbrowser.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../service/servdso/sysdso.h"
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "iapphelp.h"

class Help : public menu_res::uiWnd
{
    Q_OBJECT

public:
    explicit     Help(QWidget *parent = 0);
    ~Help();

    void         setShow();
    void         setClose();

    void         setHelpItemsList(QList<HelpItem *> list);
    void         clearContentList();

protected:
    void         loadResource( const QString &root );
    void         paintEvent( QPaintEvent *event );

    void         creatWidgetItems();
    void         buildConnection();

    virtual void tuneInc(int keyCnt );
    virtual void tuneDec(int keyCnt );

    void		 mousePressEvent(QMouseEvent *event);
    void		 mouseMoveEvent(QMouseEvent *event);
    void         mouseReleaseEvent(QMouseEvent *);

public slots:
    void         setContextByTreeList(QTreeWidgetItem *item, int);
    void         loadContextFrMsg(HelpContext *helpContext);
    void         creatTreeListByTitle();
    void         doLoadContextFrMsg();
    void         appendItem(HelpItem *item);
    void         setCurrTreeIndex(HelpItem *item);
    void         postCurrentContext(HelpItem *item);

    void         loadRelationContext(QUrl urlTitle);

    void         backward();
    void         forward();
    void         addNewItem(HelpContext *item);

    void         onClose();
signals:
    void         servChangedSignal(HelpContext*);
private:
    QString      contentProcess(QString str);
    QString      processTitle(QString str);
    QString      processPicture(QString str);
    QString      processList(QStringList strlist);
    QString      processRelations(QStringList strlist);

    QString      addTableRow(QString str);
    QString      addLink(QString str);

public:
    QList<HelpItem *> listHelpItems;
    HelpContext  mCurrAppContext;

private:
    QLabel       *title;
    QPushButton  *closeBtn;
    QPushButton  *backwardBtn;
    QPushButton  *forwardBtn;
    TextBrowser  *textBrowser;
    TreeWidget   *treeWidget;

    QList<HelpContext *> forwardList;
    QList<HelpContext *> backwardList;

    int			 xOffset;
    int			 yOffset;
    bool         isMove;

    bool         mInited;
    QString     nClose, nClosePressed;
    QString      mTop, mTL, mTR, mRight;
    QString      mBR, mBottom, mBL, mLeft;

    QString     nTl, nTr, nBl, nBr;
    QString     nLeft, nRight;
    QString     nTop1, nTop2, nTop3;
    QString     nBottom1, nBottom2, nBottom3;
};

#endif // HELP_H
