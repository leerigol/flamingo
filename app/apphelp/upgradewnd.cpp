#include "upgradewnd.h"
#include "../../service/servdso/sysdso.h"
#include "../../meta/crmeta.h"

Browser::Browser(QWidget *parent) : QTextBrowser(parent)
{
}

menu_res::RModelWnd_Style UpgradeWnd::_bgStyle;
UpgradeWnd::UpgradeWnd() : uiWnd(sysGetMainWindow())
{
    this->setGeometry(sysGetMainWindow()->geometry());
    loadResource("storage/info/");
    _bgStyle.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(nCloseRect);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(emitNo()) );

    yesBtn  = new QPushButton(this);
    yesBtn->setStyleSheet(yesQss);
    connect(yesBtn, SIGNAL(clicked()),this,SLOT(emitYes()));
    yesBtn->setFocusPolicy(Qt::TabFocus);
    yesBtn->setGeometry(nYesRect);

    noBtn   = new QPushButton(this);
    connect(noBtn, SIGNAL(clicked()),this,SLOT(emitNo()));
    noBtn->setStyleSheet(noQss);
    noBtn->setFocusPolicy(Qt::TabFocus);
    noBtn->setGeometry(nNoRect);

    contentBrowser = new Browser(this);
    contentBrowser->setStyleSheet("color:rgb(180,180,180);background:rgb(0, 0, 0);border: 0px solid #32435E;");
    contentBrowser->verticalScrollBar()->setVisible(false);
    contentBrowser->setFocusPolicy(Qt::NoFocus);
    contentBrowser->setGeometry(45,95,770,380);
    contentBrowser->setReadOnly(true);

    addAllKeys();

}

UpgradeWnd::~UpgradeWnd()
{
    if(contentBrowser)
    {
        delete contentBrowser;
        contentBrowser = NULL;
    }
}

void UpgradeWnd::setTitle(QString &t)
{
    mTitle = t;
    update();
}

void UpgradeWnd::setUpdate()
{
    noBtn->setText(sysGetString(INF_UPGRADE_CANCEL, "Cancel"));
    yesBtn->setText(sysGetString(INF_UPGRADE_AGREE, "Agree"));
    yesBtn->setFocus();
    QString title = sysGetString(INF_UPGRADE_TITLE, "System Update Information");
    setTitle(title);

    QString content = sysGetString(INF_UPGRADE_CONTEXT);
    contentBrowser->clear();
    contentBrowser->append(content);
}

void UpgradeWnd::setGeoRect(QRect r)
{
    nGeo = r;
    update();
}

void UpgradeWnd::setTitleRect(QRect r)
{
    nTitleRect = r;
    update();
}

void UpgradeWnd::setInfoRect(QRect r)
{
    nInfoRect = r;
    update();
}

void UpgradeWnd::setContentStr(QString str)
{
    contentBrowser->clear();
    contentBrowser->append(str);
}

void UpgradeWnd::mouseMoveEvent(QMouseEvent *)
{}
void UpgradeWnd::keyPressEvent(QKeyEvent *)
{}

void UpgradeWnd::keyReleaseEvent(QKeyEvent *evt)
{
    Q_ASSERT( evt != NULL );
    int key, cnt;
    key = evt->key();
    cnt = evt->count();
    if ( key == R_Pkey_FInc )
    { tuneInc(cnt); }
    else if ( key == R_Pkey_FDec )
    { tuneDec(cnt); }
    else if ( key == R_Pkey_FZ )
    { tuneZ(cnt); }
    else if ( key == R_Pkey_PageReturn )
    { emitNo(); }
}

void UpgradeWnd::paintEvent(QPaintEvent */*event*/)
{
    QPainter painter(this);
    QImage img;

    int widthTemp1 = _bgStyle.mImgLtc.width();//topleft 17 17
    int heightTemp1 = _bgStyle.mImgLtc.height();

    int widthTemp2 = _bgStyle.mImgRtc.width();//topright 4 4
    int heightTemp2 = _bgStyle.mImgRtc.height();

    int widthTemp3 = _bgStyle.mImgLdc.width();//bottomleft 4 4
    int heightTemp3 = _bgStyle.mImgLdc.height();

    int widthTemp4 = _bgStyle.mImgRdc.width();//bottomright 14 14
    int heightTemp4 = _bgStyle.mImgRdc.height();

    int lb,tb,rb,db;
    lb=tb=rb=db=0;

    tb = _bgStyle.mImgTb.height();
    for( int i = 0; i<nGeo.width()-widthTemp1-widthTemp2; i++ ){
        painter.drawImage(nGeo.x()+widthTemp1+i, nGeo.y(), _bgStyle.mImgTb );
    }

    lb = _bgStyle.mImgLb.width();
    for( int i = 0; i<nGeo.height()-heightTemp1-heightTemp3; i++ ){
        painter.drawImage(nGeo.x(), nGeo.y()+heightTemp1+i, _bgStyle.mImgLb);
    }

    db = _bgStyle.mImgDb.height();
    for(int i = 0; i < nGeo.width()-widthTemp3-widthTemp4; i++){
        painter.drawImage(nGeo.x()+widthTemp3+i, nGeo.y()+nGeo.height()-db-1, _bgStyle.mImgDb);
    }

    rb = _bgStyle.mImgRb.width();
    for(int i = 0; i < nGeo.height()-heightTemp2-heightTemp4; i++){
        painter.drawImage(nGeo.x()+nGeo.width()-widthTemp2, nGeo.y()+heightTemp2+i, _bgStyle.mImgRb);
    }

    painter.fillRect(nGeo.x()+lb, nGeo.y()+tb, nGeo.width()-lb-rb, nGeo.height()-tb-db, _bgStyle.mBgColor);

    painter.drawImage(nGeo.x(), nGeo.y(), _bgStyle.mImgLtc);
    painter.drawImage(nGeo.x()+nGeo.width()-widthTemp2, nGeo.y(), _bgStyle.mImgRtc);//
    painter.drawImage(nGeo.x(), nGeo.y()+nGeo.height()-heightTemp3-1, _bgStyle.mImgLdc);
    painter.drawImage(nGeo.x()+nGeo.width()-widthTemp4, nGeo.y()+nGeo.height()-heightTemp4-1, _bgStyle.mImgRdc);

    painter.setPen(QPen(Qt::white));
    painter.drawText(nTitleRect, Qt::AlignLeft, mTitle);
    painter.drawText(nInfoRect, Qt::TextWordWrap, mContent);
}

void UpgradeWnd::loadResource(const QString path)
{
    r_meta::CRMeta meta;
    QString str = path ;
    meta.getMetaVal(str + "yesqss", yesQss);
    meta.getMetaVal(str + "noqss", noQss);


    meta.getMetaVal(str + "leftstr", leftStr);
    meta.getMetaVal(str + "midstr", midStr);
    meta.getMetaVal(str + "rightstr", rightStr);
    meta.getMetaVal(str + "iconstr", iconStr);

    meta.getMetaVal(str + "upgrade/closerect", nCloseRect);
    meta.getMetaVal(str + "upgrade/geometry", nGeo);
    meta.getMetaVal(str + "upgrade/yesrect", nYesRect);
    meta.getMetaVal(str + "upgrade/norect", nNoRect);
    meta.getMetaVal(str + "upgrade/titlerect", nTitleRect);
    meta.getMetaVal(str + "upgrade/inforect", nInfoRect);

}

void UpgradeWnd::tuneInc( int /*keyCnt*/ )
{
    if(yesBtn->hasFocus())
        noBtn->setFocus();
    else
        yesBtn->setFocus();
}
void UpgradeWnd::tuneDec( int /*keyCnt*/ )
{
    if(yesBtn->hasFocus())
        noBtn->setFocus();
    else
        yesBtn->setFocus();
}
void UpgradeWnd::tuneZ( int /*keyCnt*/ )
{
    if(yesBtn->hasFocus())
        emitYes();
    else
        emitNo();//qxl
}

void UpgradeWnd::emitYes()
{
    emit btnClicked(1);
    this->close();
}


void UpgradeWnd::emitNo()
{
    emit btnClicked(0);
    this->close();
}
