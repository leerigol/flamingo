#include "treeWidget.h"
#include <QFont>
#include <QEvent>
#include <QDebug>
TreeWidget::TreeWidget(QWidget *parent) :
    QTreeWidget(parent)
{
    QFont font;
    font.setPointSize(12);
    this->setFont(font);

    boolMoved = false;
    boolFirst = true;

    this->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    this->header()->setMinimumSectionSize(200);
    this->setIndentation(0);
    this->setTextElideMode(Qt::ElideNone);

    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setScrollBarStyle();
}

TreeWidget::~TreeWidget()
{

}

void TreeWidget::setScrollBarStyle()
{
    this->setStyleSheet("QTreeWidget{color:rgb(180,180,180);FONT-FAMILY:Droid Sans Fallback;background:rgb(0, 0, 0);border:none;outline:none;}\
                         QTreeView::branch:selected{background:rgb(0, 0, 0);}\
                         QTreeView::item:selected:active{background: rgb(63, 147, 168);}  \
                         QTreeView::item:selected:!active {background: rgb(90, 90, 90);}\
                         ");

    this->verticalScrollBar()->setStyleSheet("QScrollBar:vertical\
                {\
                    width:4px;\
                    background:rgba(50,50,50,100%);\
                    margin:0px,0px,0px,0px;\
                }\
                QScrollBar::handle:vertical\
                {\
                    width:4px;\
                    background:rgba(100,100,100,50%);\
                    border-radius:2px;  \
                    min-height:20;\
                }\
                QScrollBar::handle:vertical:hover\
                {\
                    width:4px;\
                    background:rgba(100,100,100,25%);  \
                    border-radius:2px;\
                    min-height:20;\
                }\
                QScrollBar::add-line:vertical  \
                {\
                    height:5px;width:4px;\
                    border-image:url(:/images/a/3.png);\
                    subcontrol-position:bottom;\
                }\
                QScrollBar::sub-line:vertical  \
                {\
                    height:5px;width:4px;\
                    border-image:url(:/images/a/1.png);\
                    subcontrol-position:top;\
                }\
                QScrollBar::add-line:vertical:hover \
                {\
                    height:5px;width:4px;\
                    border-image:url(:/images/a/4.png);\
                    subcontrol-position:bottom;\
                }\
                QScrollBar::sub-line:vertical:hover \
                {\
                   height:5px;width:4px;\
                   border-image:url(:/images/a/2.png);\
                   subcontrol-position:top;\
                }\
                QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical   \
                {\
                    background:rgba(0,0,0,10%);\
                    border-radius:2px;\
                }");
}

int TreeWidget::getBarValue()
{
    barValue =  this->verticalScrollBar()->value();
    return barValue;
}

void TreeWidget::setBarValue(int val)
{
    barValue = val;
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + barValue);
    update();
}

QTreeWidgetItem *TreeWidget::itemFrIndex(const QModelIndex &index)
{
    return this->itemFromIndex(index);
}

QModelIndex TreeWidget::indexFrItem(QTreeWidgetItem *item, int column)
{
    return this->indexFromItem(item, column);
}

void TreeWidget::mouseMoveEvent(QMouseEvent *event )
{
    if(event->buttons() & Qt::LeftButton){
        int y = event->y();
        int numberSteps = (point.y()-y);
        if(numberSteps < 0){
            if(abs(numberSteps)/10 > 0 ){
                if(boolFirst){
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + numberSteps);
                    boolFirst = false;
                }else{
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + point2.y()-y);
                }
                boolMoved = true;
            }
        }else{
            if(abs(numberSteps)/10 > 0 ){
                if(boolFirst){
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + numberSteps);
                    boolFirst = false;
                }else{
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + point2.y()-y);
                }
                boolMoved = true;
            }
        }
        point2.setY(event->y());
    }
}

void TreeWidget::mousePressEvent(QMouseEvent *event )
{
    if(event->buttons() & Qt::LeftButton){
        point.setY(event->y());
        boolMoved = false;
        event->accept();
    }

    QTreeWidget::mousePressEvent(event);
}

void TreeWidget::mouseReleaseEvent(QMouseEvent *event )
{
    boolFirst = true;
    if(!boolMoved)
        QTreeWidget::mouseReleaseEvent(event);
}
