#include "help.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "../../menu/rmenus.h"
#include "../../meta/crmeta.h"

#include <QDomDocument>

Help::Help(QWidget *parent) : menu_res::uiWnd(parent)
{
    setGeometry(168,15,610,456);
    mInited = false;

    this->loadResource( "help/");
    this->creatWidgetItems();

    this->buildConnection();

    mCurrAppContext.servName = "";
    yOffset = 0;
    xOffset = 0;
    isMove = false;

    addTuneKey();
    keyChanged();
}

Help::~Help()
{
    for(int i=0; i<forwardList.count(); i++){
        HelpContext *context = forwardList.at(i);
        delete context;
        context = NULL;
    }
    forwardList.clear();
    for(int i=0; i<backwardList.count(); i++){
        HelpContext *context = backwardList.at(i);
        delete context;
        context = NULL;
    }
    backwardList.clear();

    clearContentList();
}

void Help::setShow()
{
    this->raise();
    this->load();
    this->show();
}

void Help::setClose()
{
    this->hide();
    this->setDeactive();
    this->unload();
    for(int i=0; i<forwardList.count(); i++){
        HelpContext *context = forwardList.at(i);
        delete context;
        context = NULL;
    }
    forwardList.clear();
    for(int i=0; i<backwardList.count(); i++){
        HelpContext *context = backwardList.at(i);
        delete context;
        context = NULL;
    }
    backwardList.clear();

    backwardBtn->setEnabled(false);
    forwardBtn->setEnabled(false);
}

void Help::setHelpItemsList(QList<HelpItem *> list)
{
    for(int i=0; i<list.count(); i++){
        HelpItem *item = new HelpItem;
        *item = *(list.at(i));
        listHelpItems.append(item);
    }
}

void Help::clearContentList()
{
    textBrowser->clear();

    for(int i=0; i<treeWidget->topLevelItemCount(); ){
        delete treeWidget->takeTopLevelItem(0);
    }
    for(int num=0; num<listHelpItems.count(); num++){
        HelpItem *item = listHelpItems.at(num);
        delete  item;
        item = NULL;
    }
    listHelpItems.clear();
}

void Help::loadResource(const QString &root)
{
    if ( mInited ) return;

    r_meta::CAppMeta rMeta;
    QString path = root + "back/";

    rMeta.getMetaVal(path + "bl", nBl);
    rMeta.getMetaVal(path + "bottom1", nBottom1);
    rMeta.getMetaVal(path + "bottom2", nBottom2);
    rMeta.getMetaVal(path + "bottom3", nBottom3);
    rMeta.getMetaVal(path + "br", nBr);
    rMeta.getMetaVal(path + "left", nLeft);
    rMeta.getMetaVal(path + "right", nRight);
    rMeta.getMetaVal(path + "tl", nTl);
    rMeta.getMetaVal(path + "top1", nTop1);
    rMeta.getMetaVal(path + "top2", nTop2);
    rMeta.getMetaVal(path + "top3", nTop3);
    rMeta.getMetaVal(path + "tr", nTr);

    rMeta.getMetaVal( path + "close", nClose );
    rMeta.getMetaVal( path + "closePressed", nClosePressed );

    mInited = true;
}

void Help::paintEvent(QPaintEvent *event)
{
    if(!this->hasFocus())
        setActive();
    Q_UNUSED(event);
    QPainter painter(this);
    QImage imgTl, imgTr, imgTop2, img;
    QImage imgBl, imgBr, imgBottom2;

    QRect r = rect();
    painter.fillRect(20,29,575,410,QColor(0,0,0,255));

    imgTl.load(nTl);
    int widthTl = imgTl.width();
    int heightTl = imgTl.height();
    imgTr.load(nTr);
    int widthTr = imgTr.width();
    int heightTr = imgTr.height();
    imgTop2.load(nTop2);
    int widthTop2 = imgTop2.width();
    imgBl.load(nBl);
    int widthBl = imgBl.width();
    int heightBl = imgBl.height();
    imgBr.load(nBr);
    int widthBr = imgBr.width();
    int heightBr = imgBr.height();
    imgBottom2.load(nBottom2);
    int widthBottom2 = imgBottom2.width();
    int heightBottom2 = imgBottom2.height();

    painter.drawImage(0, 26, imgTl);
    img.load(nTop1);
    for(int i=0; i<r.width()-widthTl-widthTr-widthTop2-82; i++)
        painter.drawImage(widthTl+i, 26, img);
    painter.drawImage(r.width()-widthTr-82-widthTop2, 0, imgTop2);
    img.load(nTop3);
    for(int i=0; i<82; i++)
        painter.drawImage(r.width()-widthTr-82+i, 0, img);
    painter.drawImage(r.width()-widthTr, 0, imgTr);

    img.load(nLeft);
    for(int i=0; i<r.height()-heightTl-heightBl-26; i++)
        painter.drawImage(0, 26+heightTl+i, img);
    img.load(nRight);
    for(int i=0; i<r.height()-heightTr-heightBr; i++)
        painter.drawImage(r.width()-img.width(),heightTr+i, img);

    painter.drawImage(0, r.height()-heightBl, imgBl);

    img.load(nBottom1);
    for(int i=0; i<171; i++)
        painter.drawImage(widthBl+i,r.height()-img.height(), img);
    painter.drawImage(widthBl+171, r.height()-heightBottom2, imgBottom2);
    painter.drawImage(r.width()-widthBr, r.height()-heightBr, imgBr);
    img.load(nBottom3);
    for(int i=0; i<r.width()-widthBl-171-widthBottom2-widthBr; i++)
        painter.drawImage(widthBl+171+widthBottom2+i, r.height()-img.height(), img);


    QRect rLeftBox;
    rLeftBox.setX(r.x()+8);
    rLeftBox.setY(r.y()+40);
    rLeftBox.setWidth(78);
    rLeftBox.setHeight(26);

    painter.fillRect(rLeftBox, QColor(72,72,72,255));
    painter.setPen(QColor(200,200,200,255));
    painter.drawText(rLeftBox,"Index",QTextOption(Qt::AlignCenter));


    rLeftBox.setX(r.x()+8);
    rLeftBox.setY(r.y()+65);
    rLeftBox.setWidth(192);
    rLeftBox.setHeight(367);
    painter.fillRect(rLeftBox, QColor(0,88,176,255));

    rLeftBox.setX(rLeftBox.x()+1);
    rLeftBox.setY(rLeftBox.y()+1);
    rLeftBox.setWidth(rLeftBox.width()-1);
    rLeftBox.setHeight(rLeftBox.height()-1);

    const qreal radius = 5;
    QPainterPath path;
    path.moveTo(rLeftBox.topRight() - QPointF(radius, 0));
    path.lineTo(rLeftBox.topLeft() + QPointF(radius, 0));
    path.quadTo(rLeftBox.topLeft(), rLeftBox.topLeft() + QPointF(0, radius));
    path.lineTo(rLeftBox.bottomLeft() + QPointF(0, -radius));
    path.quadTo(rLeftBox.bottomLeft(), rLeftBox.bottomLeft() + QPointF(radius, 0));
    path.lineTo(rLeftBox.bottomRight() - QPointF(radius, 0));
    path.quadTo(rLeftBox.bottomRight(), rLeftBox.bottomRight() + QPointF(0, -radius));
    path.lineTo(rLeftBox.topRight() + QPointF(0, radius));
    path.quadTo(rLeftBox.topRight(), rLeftBox.topRight() + QPointF(-radius, -0));
    painter.fillPath(path, QColor(0,0,0,255));
    painter.drawPath(path);


}

void Help::creatWidgetItems()
{
    title = new QLabel(tr("Help"),this);
    title->setMinimumHeight(26);
    title->setGeometry(491,0,82,29);
    title->setAlignment(Qt::AlignCenter);
    title->setStyleSheet("background:transparent;font:22px;color:rgb(200,200,200)");

    backwardBtn = new QPushButton("   <<<   ", this);
    backwardBtn->setEnabled(false);
    backwardBtn->setFlat(true);
    backwardBtn->setFocusPolicy(Qt::NoFocus);
    backwardBtn->setStyleSheet("QPushButton{background-color:transparent;color:white;}"
                               "QPushButton::Disabled{background-color:transparent;color:gray;}");

    forwardBtn = new QPushButton("   >>>   ", this);
    forwardBtn->setEnabled(false);
    forwardBtn->setFlat(true);
    forwardBtn->setFocusPolicy(Qt::NoFocus);
    forwardBtn->setStyleSheet("QPushButton{background-color:transparent;color:white;}"
                              "QPushButton::Disabled{background-color:transparent;color:gray;}");

    closeBtn = new QPushButton(this);
    closeBtn->setGeometry(577,0,33,40);
    closeBtn->setFlat(true);
    closeBtn->setFocusPolicy(Qt::NoFocus);
    QString str = "QPushButton{background-image: url(" + nClose + ");background-color:transparent;}" +
            "QPushButton:pressed{background-image:url(" + nClosePressed + ");background-color:transparent;}";
    closeBtn->setStyleSheet(str);
    closeBtn->setMinimumHeight(26);

    backwardBtn->hide();
    forwardBtn->hide();

    treeWidget  = new TreeWidget(this);
    treeWidget->setGeometry(9, 66, 190, 365);
    treeWidget->setFocusPolicy(Qt::StrongFocus);
//    treeWidget->resizeColumnToContents(0);
//    treeWidget->setMinimumWidth(150);
//    treeWidget->setMaximumWidth(150);
//    treeWidget->setExpandsOnDoubleClick(false);
//    treeWidget->setColumnCount(1);
    treeWidget->header()->hide();

    textBrowser = new TextBrowser(this);
    textBrowser->setGeometry(215, 50, 387, 388);
    textBrowser->setFocusPolicy(Qt::NoFocus);

#if 0
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(1, 2, 1, 0);
    QHBoxLayout *hTitleLayout = new QHBoxLayout();
    hTitleLayout->setSpacing(2);
    hTitleLayout->setContentsMargins(15, 0, 8, 0);

    title = new QLabel(tr("Rigol帮助"),this);
    title->setMinimumHeight(26);
    title->setStyleSheet("background:transparent;");
    hTitleLayout->addWidget(title);

    QSpacerItem *hSpacer2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    hTitleLayout->addItem(hSpacer2);

    backwardBtn = new QPushButton("   <<<   ", this);
    backwardBtn->setEnabled(false);
    backwardBtn->setFlat(true);
    backwardBtn->setFocusPolicy(Qt::NoFocus);
    backwardBtn->setStyleSheet("QPushButton{background-color:transparent;color:white;}"
                               "QPushButton::Disabled{background-color:transparent;color:gray;}");
    hTitleLayout->addWidget(backwardBtn);

    forwardBtn = new QPushButton("   >>>   ", this);
    forwardBtn->setEnabled(false);
    forwardBtn->setFlat(true);
    forwardBtn->setFocusPolicy(Qt::NoFocus);
    forwardBtn->setStyleSheet("QPushButton{background-color:transparent;color:white;}"
                              "QPushButton::Disabled{background-color:transparent;color:gray;}");
    hTitleLayout->addWidget(forwardBtn);

    closeBtn = new QPushButton(this);
    closeBtn->setFlat(true);
    closeBtn->setFocusPolicy(Qt::NoFocus);
    QString str = "QPushButton{background-image: url(" + mClose + ");}" +
            "QPushButton:pressed{background-image:url(" + mClosePressed + ");}";
    closeBtn->setStyleSheet(str);
    closeBtn->setMinimumHeight(26);
    hTitleLayout->addWidget(closeBtn);
    mainLayout->addLayout(hTitleLayout);

    QHBoxLayout *hTextLayout = new QHBoxLayout();
    hTextLayout->setSpacing(1);
    hTextLayout->setContentsMargins(9, 9, 9, 9);

    treeWidget  = new TreeWidget(this);
    treeWidget->setFocusPolicy(Qt::StrongFocus);
    treeWidget->resizeColumnToContents(0);
    treeWidget->setMinimumWidth(150);
    treeWidget->setMaximumWidth(150);
    treeWidget->setExpandsOnDoubleClick(false);
    treeWidget->setColumnCount(1);
    treeWidget->header()->hide();

    hTextLayout->addWidget(treeWidget);

    textBrowser = new TextBrowser(this);
    textBrowser->setFocusPolicy(Qt::NoFocus);
    hTextLayout->addWidget(textBrowser);
    mainLayout->addLayout(hTextLayout);
#endif

}

void Help::buildConnection()
{
    connect(closeBtn,   SIGNAL(clicked()), this, SLOT(onClose()));
    connect(backwardBtn,SIGNAL(clicked()), this, SLOT(backward()));
    connect(forwardBtn, SIGNAL(clicked()), this, SLOT(forward()));

    connect(treeWidget, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
            this, SLOT(setContextByTreeList(QTreeWidgetItem*,int)));

    connect(textBrowser,SIGNAL(anchorClicked(QUrl)), this, SLOT(loadRelationContext(QUrl)));

}
void Help::tuneInc(int  )
{
//    qDebug()<<__FUNCTION__<<__LINE__;
    if(treeWidget->itemAbove(treeWidget->currentItem())){
        treeWidget->setCurrentItem(treeWidget->itemAbove(treeWidget->currentItem()));
        setContextByTreeList(treeWidget->currentItem(), 0);
    }
}

void Help::tuneDec(int )
{
    if(treeWidget->itemBelow(treeWidget->currentItem())){
        treeWidget->setCurrentItem(treeWidget->itemBelow(treeWidget->currentItem()));
        setContextByTreeList(treeWidget->currentItem(), 0);
    }
}
void Help::mousePressEvent(QMouseEvent *event)
{
    QRect rect = geometry();
    rect.setX(0);
    rect.setY(0);
    if(rect.contains(event->pos())){
        raise();
        setActive();
        isMove = true;
        this->xOffset = event->globalPos().rx() - this->pos().rx();
        this->yOffset = event->globalPos().ry() - this->pos().ry();
    }else
        isMove = false;

    return menu_res::uiWnd::mousePressEvent(event);
}

void Help::mouseMoveEvent(QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton && isMove ) {

        int toX = event->globalX()-xOffset;
        int toY = event->globalY()-yOffset;

        if( event->globalX()-xOffset < 0 )
            toX = 0;
        else if(event->globalX()-xOffset > 300)
            toX = 300;

        if( event->globalY()-yOffset < 0 )
            toY = 0;
        else if(event->globalY()-yOffset > 209)
            toY = 209;
        move(toX, toY);
        update();
    }
}
void Help::mouseReleaseEvent(QMouseEvent *)
{
    isMove = false;
    xOffset = 0;
    yOffset = 0;
}
void Help::setContextByTreeList(QTreeWidgetItem *item,int )
{
    for(int num=0; num<listHelpItems.count(); num++)
    {
        HelpItem *helpItem = listHelpItems.at(num);
        if(helpItem->title == item->text(0)){
            textBrowser->clear();
            appendItem(helpItem);
            postCurrentContext(helpItem);
            break;
        }
    }

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void Help::loadContextFrMsg( HelpContext *helpContext )
{
//    qDebug()<<__FUNCTION__<<__LINE__<<helpContext->servName<<helpContext->servMsg;

//    qDebug()<<__FUNCTION__<<__LINE__<<helpContext->mOpt<<helpContext->mhlpPath;

    if( helpContext->servName == mCurrAppContext.servName ){
        textBrowser->clear();
    }
    mCurrAppContext = *helpContext;
    if(CMD_SERVICE_ACTIVE == helpContext->servMsg){
        if(listHelpItems.count())
            appendItem(listHelpItems.at(0));
        else
            return;
    }else
        doLoadContextFrMsg();
}

void Help::creatTreeListByTitle()
{
    for(int num=0; num<listHelpItems.count(); num++)
    {
        HelpItem *helpItem = listHelpItems.at(num);
        QTreeWidgetItem *treeItem = new QTreeWidgetItem(treeWidget);
        treeItem->setText(0, helpItem->title);
    }
}

void Help::doLoadContextFrMsg( )
{
    for(int num=0; num<listHelpItems.count(); num++)
    {
        HelpItem *item = listHelpItems.at(num);
        if(item->msg == mCurrAppContext.servMsg){
            if(mCurrAppContext.mValid ){
                if(num+1 < listHelpItems.count()){
                    HelpItem *itemNext = listHelpItems.at(num+1);
                    if(itemNext->msg == item->msg){
                        if(item->option != QString::number(mCurrAppContext.mOpt+1)){
                            continue;
                        }
                    }
                }
            }
            appendItem(item);
            addNewItem(&mCurrAppContext);
            break;
        }
    }
}

void Help::appendItem(HelpItem *item)
{
    setCurrTreeIndex(item);

    textBrowser->append(processTitle(item->title));

    if ( sysGetLanguage() == language_chinese ||
         sysGetLanguage() == language_traditional_chinese)
    {
        textBrowser->append("<FONT style='COLOR:rgb(180,180,180);FONT-FAMILY:Droid Sans Fallback'><p>"+contentProcess(item->content)+"</p></FONT>");
    }
    else
    {
        textBrowser->append("<FONT style='COLOR:rgb(180,180,180);FONT-FAMILY:Arial'><p>"+contentProcess(item->content)+"</p></FONT>");
    }
//    textBrowser->append("<FONT style='COLOR:rgb(200,200,200);FONT-FAMILY:SimHei'><p>"+contentProcess(item->content)+"</p></FONT>");
//    textBrowser->insertHtml("<p>"+contentProcess(item->content)+"</p>");

    QStringList titleList;    
    for(int i=0; i<item->relationList.count(); i++){
        for(int num=0; num<listHelpItems.count(); num++){
            HelpItem *helpItem = listHelpItems.at(num);
            if(QString::number(helpItem->msg) == item->relationList.at(i)){
                if(!titleList.contains(helpItem->title))
                    titleList.append(helpItem->title);
            }
        }
    }
    for(int i=0; i<item->keyList.count(); i++){
        QString strKey = item->keyList.at(i);
        strKey = strKey.simplified();
        for(int num=0; num<listHelpItems.count(); num++){
            HelpItem *helpItem = listHelpItems.at(num);
            if(helpItem->keyList.contains(strKey)){
                if(!titleList.contains(helpItem->title)){
                    titleList.append(helpItem->title);
                }
            }
        }
    }
    for(int i=0; i<item->siblingList.count(); i++){
        QString str = item->siblingList.at(i);
        if(treeWidget->currentItem()->parent()){
            QTreeWidgetItem *treeItemTemp = treeWidget->currentItem()->parent()->child(str.toInt());
            if(treeItemTemp){
                if(!titleList.contains(treeItemTemp->text(0)))
                    titleList.append(treeItemTemp->text(0));
            }
        }
    }
    titleList.removeOne(item->title);
//    if(item->content.size() < 150 )
//        textBrowser->append("<br>");
//    else if(item->content.size() < 250 )
//        textBrowser->append("<br>");
    if ( sysGetLanguage() == language_chinese  ||
         sysGetLanguage() == language_traditional_chinese)
    {
        textBrowser->append("<FONT style='FONT-SIZE:12pt;COLOR:rgb(200,200,200);FONT-FAMILY:Droid Sans Fallback'>相关项 :</FONT>");
    }
    else
    {
        textBrowser->append("<FONT style='FONT-SIZE:12pt;COLOR:rgb(200,200,200);FONT-FAMILY:Arial'>Relations :</FONT>");
    }
    textBrowser->append( processRelations( titleList ) );

    textBrowser->verticalScrollBar()->setValue(0);
}

void Help::setCurrTreeIndex(HelpItem *item)
{
    QList<QTreeWidgetItem *> listItem = treeWidget->findItems(item->title, Qt::MatchRecursive, 0);
    if(listItem.count()){
        QTreeWidgetItem *treeItem  = listItem.at(0);
        treeWidget->setCurrentIndex(treeWidget->indexFrItem(treeItem));
    }
}

void Help::postCurrentContext(HelpItem *helpItem)
{
    mCurrAppContext.servMsg    = helpItem->msg;
    mCurrAppContext.mValid     = true;
    mCurrAppContext.mOpt       = helpItem->option.toInt();

    addNewItem(&mCurrAppContext);
    serviceExecutor::post(serv_name_help, servHelp::cmd_help_context, (void *)(&mCurrAppContext));//
}

void Help::loadRelationContext(QUrl urlTitle)
{
    for(int num=0; num<listHelpItems.count(); num++){
        HelpItem *helpItem = listHelpItems.at(num);
        if(helpItem->title == urlTitle.toString().remove("#")){
            textBrowser->clear();
            appendItem(helpItem);
            postCurrentContext(helpItem);
        }
    }
}

void Help::backward()
{
    forwardBtn->setEnabled(true);
    if(backwardList.count() > 1){
        forwardList.append(backwardList.last());
        backwardList.removeLast();

        if( backwardList.last()->mhlpPath != mCurrAppContext.mhlpPath ){
            emit servChangedSignal(backwardList.last());
        }else{
            textBrowser->clear();
        }
        mCurrAppContext = *backwardList.last();

        for(int num=0; num<listHelpItems.count(); num++){
            HelpItem *item = listHelpItems.at(num);
            if(item->msg == mCurrAppContext.servMsg){
                if(num+1<listHelpItems.count()){
                    HelpItem *itemNext = listHelpItems.at(num+1);
                    if(item->msg == itemNext->msg){
                        if(item->option != QString::number(mCurrAppContext.mOpt)){
                            continue;
                        }
                    }
                }
                appendItem(item);
                break;
            }
        }
        if(backwardList.count() <= 1)
            backwardBtn->setEnabled(false);
    }
}

void Help::forward()
{
    if(forwardList.count()){
        if( forwardList.last()->mhlpPath != mCurrAppContext.mhlpPath ){
            emit servChangedSignal(forwardList.last());
        }else{
            textBrowser->clear();
        }
        mCurrAppContext = *forwardList.last();

        for(int num=0; num<listHelpItems.count(); num++){
            HelpItem *item = listHelpItems.at(num);
            if(item->msg == mCurrAppContext.servMsg){
                if(num+1<listHelpItems.count()){
                    HelpItem *itemNext = listHelpItems.at(num+1);
                    if(item->msg == itemNext->msg){
                        if(item->option != QString::number(mCurrAppContext.mOpt)){
                            continue;
                        }
                    }
                }
                appendItem(item);
                break;
            }
        }
        backwardList.append(forwardList.last());
        forwardList.removeLast();

        if(!forwardList.count())
            forwardBtn->setEnabled(false);
        backwardBtn->setEnabled(true);
    }
}

void Help::addNewItem(HelpContext *item)
{
    for(int i=0; i<forwardList.count(); i++){
        HelpContext *context = forwardList.at(i);
        delete context;
        context = NULL;
    }
    forwardList.clear();

    while(backwardList.count() > 16){
        HelpContext *con = backwardList.first();
        delete con;
        con = NULL;
        backwardList.removeFirst();
    }
    for(int num=0; num<backwardList.count(); num++){
        HelpContext *con = backwardList.at(num);
        if(con->servMsg == item->servMsg && con->mOpt == item->mOpt){
            HelpContext *con = backwardList.at(num);
            delete con;
            con = NULL;
            backwardList.removeAt(num);
        }
    }
    HelpContext *context = new HelpContext;
    *context = *item;
    backwardList.append(context);

    if(backwardList.count() > 1)
        backwardBtn->setEnabled(true);
    forwardBtn->setEnabled(false);
}

void Help::onClose()
{
    serviceExecutor::post(serv_name_help, MSG_APP_HELP_HELP, false);
}

QString Help::contentProcess(QString str)
{
    QString strData;
    QStringList strList, strRowList;
    strRowList = str.split("\n", QString::SkipEmptyParts);
    for(int num=0; num<strRowList.count(); num++)
    {
        QString line = strRowList.at(num);
        if("**" == line.simplified().left(2)){
            strList.append(line.remove(0,2).toHtmlEscaped());
            continue;
        }else{
            if( strList.count() ){
                strData.append(processList(strList));
                strList.clear();
            }
            if(line.contains("##")){
                strData.append(processPicture(line));
            }else
                strData.append(line.simplified());
        }
    }
    if( strList.count() ){
        strData.append(processList(strList));
        strList.clear();
    }
    return strData;
}

QString Help::processTitle(QString str)
{
    if ( sysGetLanguage() == language_chinese ||
         sysGetLanguage() == language_traditional_chinese)
    {
        QString pre = "<FONT style='FONT-SIZE:12pt;COLOR:rgb(200,200,200);FONT-FAMILY:Droid Sans Fallback'>";
        return str.simplified().prepend(pre).append("</FONT>");
    }
    else
    {
        QString pre = "<FONT style='FONT-SIZE:12pt;COLOR:rgb(200,200,200);FONT-FAMILY:Arial'>";
        return str.simplified().prepend(pre).append("</FONT>");
    }
}

QString Help::processPicture(QString str)
{
    QStringList list;
    QString strPath =  mCurrAppContext.mPicPath;
    list = str.split("##",QString::KeepEmptyParts);

    if(3 == list.count()){
        QString picHtml = list.at(1);
        picHtml.prepend(strPath);
        if(QFile::exists(picHtml)){
            picHtml.prepend("<img src='");
            picHtml.append("' align='right' />");
            return picHtml;
        }
    }
    return NULL;
}

QString Help::processList(QStringList strlist)
{
    QString strData, strRow;
    QStringList dataList;
    int strMaxLength = 0;

    for(int num=0; num<strlist.count(); num++)
    {
        strRow = strlist.at(num);
        QStringList list = strRow.simplified().split("**", QString::SkipEmptyParts);
        for(int i=0; i<list.count(); i++){
            QString strTemp = list.at(i);
            strMaxLength = ( strMaxLength > strTemp.size() ) ? strMaxLength:strTemp.size();
            dataList.append(strTemp.simplified());
        }
    }
    strData.prepend("<table border='0';>");
    if(strMaxLength < 12){
        for(int num=0; num<dataList.count(); num+=2){
            strData.append("<tr>");
            strData.append(addTableRow(dataList.at(num)));

            if(num+1 < dataList.count()){
                strData.append(addTableRow(dataList.at(num+1)));
            }
            strData.append("</tr>");
        }
        strData.append("</table>");
    }else{
        for(int num=0; num<dataList.count(); num++){
            strData.append("<tr>");
            strData.append(addTableRow(dataList.at(num)));
            strData.append("</tr>");
        }
        strData.append("</table>");
    }
    return strData;
}

QString Help::processRelations(QStringList strlist)
{
    QString textString;
    int num=0;
    textString.append("<FONT style='COLOR:gray;FONT-FAMILY:Droid Sans Fallback'><table border='0' cellspacing='6'>");
    for( ; num<strlist.count()/3; num++){
        textString.append("<tr>");
        for(int n=0; n<3; n++){
            QString strTemp = addLink(strlist.at(num*3+n));
            textString.append(addTableRow(strTemp));
        }
        textString.append("</tr>");
    }
    textString.append("<tr>");
    for(int n=0; n<strlist.count()%3; n++){
        QString strTemp = addLink(strlist.at(num*3+n));
        textString.append(addTableRow(strTemp));
    }
    textString.append("</tr>");
    textString.append("</table></FONT>");
    return textString;
}

QString Help::addTableRow(QString str)
{
    return str.prepend("<td><ul><li>").append("</li><ul></td>");
}

QString Help::addLink(QString str)
{
    QString textString = QString::fromUtf8("<a href='#%1' style='color:rgb(80,80,255)'>%1</a>").arg(str);
    return textString;
}




