#include "touchzone.h"
#include <QMouseEvent>
#include <QPainter>


TouchZone::TouchZone(QWidget *parent) : QWidget(parent)
{
    m_zone.setCoords(0,0,0,0);
    isZoomOn = false;
    resize(parent->width(), parent->height());
    connect( &m_menuCombox, SIGNAL(itemClicked(QListWidgetItem*)),
             this, SLOT(on_menu_activated(QListWidgetItem*)) );

    m_menuCombox.setParent(sysGetMainWindow());
    m_menuCombox.verticalScrollBar()->setVisible(false);
    m_menuCombox.horizontalScrollBar()->setVisible(false);
    m_menuCombox.resize(170,250);
    m_menuCombox.setStyleSheet("QListWidget {\
                               background-color: rgb(15,15,15);\
                               border: 1px solid #333333;\
                               color: rgb(192,192,192);}");
}

void TouchZone::paintEvent(QPaintEvent *)
{
    if(m_zone.topLeft() == m_zone.bottomRight())
        return;
    if( m_bRectShow )
    {
        QPainter touchPainter(this);
        QPen     pen;
        pen.setColor(Qt::white);
        pen.setStyle(Qt::DashDotLine);
        touchPainter.setPen(pen);
        //touchPainter.setBrush(QBrush( Qt::white, Qt::Dense7Pattern));  
        QRect rect = m_zone.normalized();
        touchPainter.drawRect(rect);
    }
}

void TouchZone::mouseMoveEvent(QMouseEvent *event)
{
    QPoint point = event->pos();
    rangeCheck(point);
    m_zone.setBottomRight(point);
    emit sig_mouse_move_event(point);
}

void TouchZone::mousePressEvent(QMouseEvent *event)
{
    m_zone.setTopLeft(event->pos());
    m_zone.setBottomRight(event->pos());
    serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, isZoomOn);
    update();
    emit sig_mouse_pressEvent(event->pos());
}

void TouchZone::mouseReleaseEvent(QMouseEvent *event)
{
    QPoint point = event->pos();/// set range
    rangeCheck(point);
    m_zone.setBottomRight(point);
    emit sig_mouse_releaseEvent(point);
}

void TouchZone::rangeCheck(QPoint &point)
{
    if(isZoomOn)
    {
        QRect rectMain(0, 0, 1000, 144);
        QRect rectZoom(0, 160, 1000, 320);
        if( rectMain.contains(m_zone.topLeft()) )
        {
            if(point.x()<=0)
            {
                point.setX(0);
            }
            if(point.y()<=0)
            {
                point.setY(0);
            }
            if(point.x()>=999)
            {
                point.setX(999);
            }
            if(point.y()>=143)
            {
                point.setY(142);
            }
        }
        else if( rectZoom.contains(m_zone.topLeft()) )
        {
            if(point.x()<=0)
            {
                point.setX(0);
            }
            if(point.y()<=160)
            {
                point.setY(160);
            }
            if(point.x()>=999)
            {
                point.setX(999);
            }
            if(point.y()>=479)
            {
                point.setY(478);
            }
        }
    }
    else
    {
        if(point.x()<=0)
        {
            point.setX(0);
        }
        if(point.y()<=0)
        {
            point.setY(0);
        }
        if(point.x()>=999)
        {
            point.setX(999);
        }
        if(point.y()>=479)
        {
            point.setY(478);
        }
    }
}

void TouchZone::convertView(QRect &rect, QWidget *pView)
{
    Q_ASSERT(pView != NULL);
    float yRatio = (float)pView->height() / this->height();
    float xRatio = (float)pView->width()  / this->width();

    //! 换算wiew坐标
    QPoint phyPoint1 = m_zone.topLeft();
    QPoint phyPoint2 = m_zone.bottomRight();

    QPoint viewPoint1 = pView->geometry().topLeft();
    //QPoint viewPoint2 = view->geometry().bottomRight();


    int   y1 = phyPoint1.y() * yRatio - viewPoint1.y();
    int   y2 = phyPoint2.y() * yRatio - viewPoint1.y();
    int   x1 = phyPoint1.x() * xRatio - viewPoint1.x();
    int   x2 = phyPoint2.x() * xRatio - viewPoint1.x();

    rect = QRect(QPoint(x1,y1), QPoint(x2,y2));
}

QRect TouchZone::getZone()
{
    return m_zone;
}

void TouchZone::setTouchEn(bool en)
{
    menuHide();
    rectHide();

    QWidget::setVisible(en);
}

void TouchZone::menuShow()
{
    m_menuCombox.clear();

    QMap<int, bool>::const_iterator i = m_menuMap.constBegin();
    while (i != m_menuMap.constEnd())
    {
        if(i.value())
        {
            QListWidgetItem *item = new QListWidgetItem(sysGetString(i.key(), QString("Zone %1").arg(i.key())) ,&m_menuCombox );
            item->setSizeHint(QSize(160,40));
        }
        ++i;
    }

    QRect zone    = m_zone.normalized();
    QRect midWin  = sysGetMidWindow()->rect().normalized();
    QRect mainWin = sysGetMainWindow()->rect().normalized();

    QPoint p = zone.topRight();
    //qDebug()<<__FILE__<<__LINE__<< p << midWin.size()<<mainWin.height();

    p.setY(p.y()+56);
    p.setX(p.x()+30);
    QSize size = midWin.size()-m_menuCombox.size();
    if(p.x() > size.width()+30)
    {
        p.setX(p.x()-m_menuCombox.size().width()-zone.width()-10);
        if(p.x()<23)
        {
            p.setX(zone.topLeft().x()+30);
        }
    }
    if(p.y() > mainWin.height()-62-m_menuCombox.height())
    {
        p.setY(mainWin.height()-62-m_menuCombox.height());
    }
    m_menuCombox.move( p );
    m_menuCombox.show();
}

void TouchZone::menuHide()
{
    m_menuCombox.hide();
}

void TouchZone::rectShow()
{
    m_bRectShow = true;
    //update();
}

void TouchZone::rectHide()
{
    m_bRectShow = false;
    update();
}

void TouchZone::setMenuVisible(int index, bool en)
{
    m_menuMap[index] = en;
}

bool TouchZone::getMenuVisible(int index)
{
    return m_menuMap.value(index);
}

void TouchZone::on_menu_activated(QListWidgetItem* /*item*/)
{
    int index = m_menuCombox.currentRow();
    int j = 0;
    QMap<int, bool>::const_iterator i = m_menuMap.constBegin();
    while (i != m_menuMap.constEnd())
    {
        if(i.value())
        {
            if(j == index)
            {
                m_menuCombox.hide();
                rectHide();
                emit sig_menu_activated( i.key() );
                return;
            }
            j++;
        }
        ++i;
    }

    rectHide();
    m_menuCombox.hide();
    m_menuCombox.clearSelection();
}


