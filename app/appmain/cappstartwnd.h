#ifndef CAPPSTARTWND_H
#define CAPPSTARTWND_H

#include <QWidget>
#include <QListWidget>
#include <QGridLayout>

#include "../../menu/wnd/rinfownd.h"
#include "../../menu/rmenus.h"  //! controlers
#include "../../menu/menustyle/rinfownd_style.h"

class CAppStartWnd : public menu_res::RInfoWnd
{
    Q_OBJECT
public:
    explicit CAppStartWnd(QWidget *parent = 0);

public:
    virtual void loadResource( const QString &path,
                               const r_meta::CMeta &meta );

protected:
    virtual void paintEvent( QPaintEvent */*event*/ );
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    virtual void resizeEvent(QResizeEvent */*event*/);

public:
    void addQuick( AppIcon *pIcon );

private:
    QGridLayout *m_pLayout;

    QSize mSize;

    int mRow;
    int mCol;

    static const int _columns = 4;
    static const int _rows = 5;

protected:
    menu_res::RInfoWnd_Style mStyle;

signals:

public slots:
    void on_appx_clicked(QString str, int msg );
};


#endif // CAPPSTARTWND_H
