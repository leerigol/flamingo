#ifndef CAPPMAIN_H
#define CAPPMAIN_H

//#include <QtWidgets>
#include <QMainWindow>
#include <QMouseEvent>

#include "../../app/capp.h"
#include "../../menu/rmenus.h"
#include "../../menu/dsowidget/rdsodragdrop.h"

#include "../appmenu/cappmenu.h"

#include "cappstartwnd.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/wnd/rmenuwnd.h"
#include "../../service/servicefactory.h"
#include "touchlabel.h"
class TouchZone;

enum AppMainControl
{
    main_control_none,

    main_control_ch1,
    main_control_ch2,
    main_control_ch3,
    main_control_ch4,
};

class langFont
{
public:
    langFont( const QString &lang, const QString &font );
    langFont( const langFont &lF );
    langFont & operator=( const langFont &lF );
public:
    int mLang;
    QString mFontFamily;
};

class appMain_style : public menu_res::RUI_Style
{
public:
    QString getFont( int lang );
protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );
public:
    QRect rectLeft, rectTop, rectRight, rectDown, rectCenter;
    QRect rectMain, rectMenu;

    QList <langFont> mLangFontList;
    QString mDefFont;

    QPoint mPtExpand, mPtCollapse;
};

class CAppMain : public QMainWindow
{
    Q_OBJECT
public:
    explicit CAppMain(QWidget *parent = 0);

    virtual bool event( QEvent *pEvent );

protected:
    QList<CApp*> mListApps;

protected:
    appMain_style _style;   /*!< 一个系统中只有一个mainWnd */

    //! frame
    menu_res::RDsoDragDropFrame *mp_wigLeft;
    menu_res::RDsoDragDropFrame *mp_wigTop;
    menu_res::RDsoDragDropFrame *mp_wigRight;
    menu_res::RDsoDragDropFrame *mp_wigDown;
    menu_res::RDsoDragDropFrame *mp_wigCent;
    TouchLabel                  *touchLabel;
    TouchZone                   *touchZone;

    QWidget *mp_wigBg;

    //!right
    QGridLayout *mp_layMenu;
    QToolBar    *mp_barMenu;
    QWidget     *mp_wigMenu;

    menu_res::RMenuWnd *mpMenuDlg;

    //! start
    menu_res::RDsoImageButton *m_pStartButton;
    CAppStartWnd *m_pStartWnd;

    //! menu
    CAppMenu mappMenu;
    QTimer mDbgTimer;

    //! debug
    QAction *m_pActCH1;
    QAction *m_pActCH2;
    QAction *m_pActCH3;
    QAction *m_pActCH4;

    QAction *m_pActLa;
    QAction *m_pActRef;
    QAction *m_pActSearch;
    QAction *m_pActStorage;

    QAction *m_pActSource1;
    QAction *m_pActSource2;

    QAction *m_pActMask;
    QAction *m_pUtility;
    QAction *m_pActWRec;
    QAction *m_pActHori;

    QAction *m_pActMath;
    QAction *m_pActCursor;

    QAction *m_pActTest;
    QAction *m_pActMeas;
    QAction *m_pActUPA;
    QAction *m_pActEyeJit;
    QAction *m_pActMenuOff;
    QAction *m_pActDec;

    QAction *m_pHelp;

public:
    QWidget *getLeftBar();
    QWidget *getRightBar();
    QWidget *getTopBar();
    QWidget *getDownBar();
    QWidget *getCentBar();
    QWidget *getBackground();

    TouchZone *getTouchZoneBar();
public:
    void createApps();

    void setupUi();
    void retranslateUi();

    void buildConnection();

    void connectService();

protected:
    void resizeEvent(QResizeEvent * event);
    void moveEvent(QMoveEvent * event);

protected Q_SLOTS:

    void on_ch1_trigger();
    void on_ch2_trigger();
    void on_ch3_trigger();
    void on_ch4_trigger();

    void on_source1_trigger();
    void on_source2_trigger();

    void on_search_trigger();
    void on_la_trigger();
    void on_ref_trigger();
    void on_hori_trigger();
    void on_math_trigger();
    void on_cursor_trigger();

    void on_test_trigger();

    void on_meas_trigger();
    void on_upa_trigger();

    void on_eyejit_trigger();

    void on_utility_menu();

    void on_wrec_trigger();

    void on_storage_trigger();

    void on_mask_trigger();

    void on_help_trigger();

    void on_trigger();
    void on_display();
    void on_dec_trigger();
    void on_menu();
    void on_quick();

    void on_start_trigger();
    void onDbgTimeout();
};

#endif // CAPPMAIN_H
