#include <QMouseEvent>

#include "../../service/service_name.h"
#include "../../baseclass/rlangchangeevent.h"

#include "../appAuto/appAuto.h"
#include "../appch/cappch.h"
#include "../appdg/cappdg.h"
#include "../appla/cappla.h"
#include "../appplot/appplot.h"
#include "../appdisplay/appdisplay.h"
#include "../trigger/apptrigger.h"
#include "../apphori/capphori.h"
#include "../appref/cappref.h"
#include "../appmask/cappmask.h"
#include "../appRecord/capprecord.h"
#include "../appsearch/cappsearch.h"
#include "../apputility/apputility.h"
#include "../appcursor/cappcursor.h"

#include "../apputility/appInterface/appInterface.h"
#include "../apputility/appmail/appmail.h"
#include "../apputility/appwifi/appwifi.h"

#include "../appgui/appgui.h"

#include "../measure/appmeasure.h"
#include "../appcounter/cappcounter.h"
#include "../appdvm/cappdvm.h"
#include "../appUPA/appUPA.h"
#include "../appeyejit/appeyejit.h"
#include "../appHisto/capphisto.h"

#include "../appmath/cappmath.h"
#include "../appmath/cappmathgp.h"

#include "../appdso/cappdso.h"
#include "../appdecode/cappdecode.h"
#include "../appdecode/cappdecodegp.h"

#include "../storage/appstorage.h"
#include "../appmenutest/cappmenutest.h"

#include "../apphelp/capphelp.h"
#include "../appcal/appcal.h"

#include "../appquick/appquick.h"

#include "cappmain.h"
#include "touchzone.h"

#include "../../service/servdso/sysdso.h"


langFont::langFont( const QString &lang, const QString &font )
{
    bool bOK;
    mLang = lang.toInt( &bOK );
    Q_ASSERT( bOK && mLang >= 0 );

    mFontFamily = font;
}

langFont::langFont( const langFont &lF )
{
    *this = lF;
}
langFont & langFont::operator=( const langFont &lF )
{
    mLang = lF.mLang;
    mFontFamily = lF.mFontFamily;
    return *this;
}

QString appMain_style::getFont( int lang )
{
    QList<langFont>::iterator it;
    it = mLangFontList.begin();
    while( it != mLangFontList.end() )
    {
        if ( it->mLang == lang )
        { return it->mFontFamily; }

        it++;
    }

    return mDefFont;
}

void appMain_style::load( const QString &path,
                          const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "widget_left", rectLeft );
    meta.getMetaVal( path + "widget_top", rectTop );
    meta.getMetaVal( path + "widget_right", rectRight );
    meta.getMetaVal( path + "widget_down", rectDown );

    meta.getMetaVal( path + "widget_center", rectCenter );
    meta.getMetaVal( path + "wnd_main", rectMain );
    meta.getMetaVal( path + "menu", rectMenu );

    int count;
    QString str, subPath;
    meta.getMetaVal( path + "font/count", count );

    meta.getMetaVal( path + "font/default", mDefFont );
    Q_ASSERT( mDefFont.size() != 0 );

    QStringList strList;
    QStringList langList;
    for ( int i = 0; i < count; i++ )
    {
        subPath = QString("font/font%1").arg(i);
        meta.getMetaVal( path + subPath, str );

        strList = str.split( QStringLiteral(":") );
        Q_ASSERT( strList.size() == 2 );

        langList = strList[1].split( QStringLiteral(",") );
        Q_ASSERT( langList.size() > 0 );

        for ( int j = 0; j < langList.size(); j++ )
        {
            mLangFontList.append( langFont(langList[j], strList[0]) );
        }
    }

    //! pos
    meta.getMetaVal( path + "pos/expand", mPtExpand );
    meta.getMetaVal( path + "pos/collapse", mPtCollapse );
}


CAppMain::CAppMain(QWidget *parent) : QMainWindow(parent)
{
    _style.loadResource( "appmain/", r_meta::CAppMeta() );

    //! default font
    qApp->setFont( QFont( _style.mDefFont) );
}

bool CAppMain::event( QEvent *pEvent )
{
    //qDebug() << "enent:" << pEvent;
    Q_ASSERT( pEvent != NULL );
    if ( pEvent->type() == QEvent::LanguageChange )
    {
        //! convert type
        RLangchangeEvent *pLangEvent;
        pLangEvent = dynamic_cast<RLangchangeEvent *>( pEvent );
        Q_ASSERT( pLangEvent != NULL );

        //! set language
        menu_res::CResData::setLanguageSeq( pLangEvent->mLanguage - language_english );

        //! 根据语言选择字体
        QFont font;
        font.setFamily(_style.getFont( pLangEvent->mLanguage - language_english));
        if(pLangEvent->mLanguage == language_thailand )
        {
            font.setBold(true);
        }
        else
        {
            font.setBold(false);
        }
        qApp->setFont( font );
        //! menu updated
        Q_ASSERT( mpMenuDlg != NULL );
        if ( mpMenuDlg->isVisible() )
        {
            mpMenuDlg->update();
        }
        //! now app ui updated
        CApp *pApp;
        foreach( pApp, mListApps )
        {
            Q_ASSERT( pApp != NULL );
            pApp->retranslateUi();
        }
        return true;
    }
    return QMainWindow::event( pEvent );
}

QWidget *CAppMain::getLeftBar()
{
    return mp_wigLeft;
}
QWidget *CAppMain::getRightBar()
{
    return NULL;
}
QWidget *CAppMain::getTopBar()
{
    return mp_wigTop;
}
QWidget *CAppMain::getDownBar()
{
    return mp_wigDown;
}

QWidget *CAppMain::getCentBar()
{
    return mp_wigCent;
}
QWidget *CAppMain::getBackground()
{ return mp_wigBg; }

TouchZone *CAppMain::getTouchZoneBar()
{
    return touchZone;
}

//#include "appmain_factory.cpp"

//#include "appmain_ui.cpp"
//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";
//#define dbgout()            qWarning()<<__FUNCTION__<<__LINE__;

#define def_time()
#define start_time()
#define end_time()

void CAppMain::setupUi()
{
    def_time();

    #ifdef _SIMULATE
    //! system menu
    QMenu *pMenu = new QMenu("Vertical");
    m_pActCH1 = pMenu->addAction("CH1");
    m_pActCH2 = pMenu->addAction("CH2");
    m_pActCH3 = pMenu->addAction("CH3");
    m_pActCH4 = pMenu->addAction("CH4");

    pMenu->addSeparator();
    m_pActMath   = pMenu->addAction("Math");
    m_pActCursor = pMenu->addAction("Cursor");

    pMenu->addSeparator();
    m_pActHori = pMenu->addAction("Hori");
    pMenu->addSeparator();
    m_pActTest = pMenu->addAction("Test");

    pMenu->addSeparator();
    m_pActMeas = pMenu->addAction("Measure");

    pMenu->addSeparator();
    m_pActMenuOff = pMenu->addAction("MenuOff");

    connect( m_pActCH1, SIGNAL(triggered(bool)), this, SLOT(on_ch1_trigger()) );
    connect( m_pActCH2, SIGNAL(triggered(bool)), this, SLOT(on_ch2_trigger()) );
    connect( m_pActCH3, SIGNAL(triggered(bool)), this, SLOT(on_ch3_trigger()) );
    connect( m_pActCH4, SIGNAL(triggered(bool)), this, SLOT(on_ch4_trigger()) );

    connect( m_pActMath,   SIGNAL(triggered(bool)), this, SLOT(on_math_trigger()) );
    connect( m_pActCursor, SIGNAL(triggered(bool)), this, SLOT(on_cursor_trigger()) );

    connect( m_pActHori,    SIGNAL(triggered(bool)), this, SLOT(on_hori_trigger()) );
    connect( m_pActTest,    SIGNAL(triggered(bool)), this, SLOT(on_test_trigger()) );
    connect( m_pActMeas,    SIGNAL(triggered(bool)), this, SLOT(on_meas_trigger()) );
    connect( m_pActMenuOff, SIGNAL(triggered(bool)), this, SLOT(on_menu()) );

    QMenu *pAdvMenu = new QMenu("Advance");
    m_pActRef     = pAdvMenu->addAction("REF");

    pAdvMenu->addSeparator();
    m_pActLa      = pAdvMenu->addAction("La");

    pAdvMenu->addSeparator();
    m_pActMask    = pAdvMenu->addAction("Mask");

    pAdvMenu->addSeparator();
    m_pActStorage = pAdvMenu->addAction("Storage");

    pAdvMenu->addSeparator();
    m_pUtility    = pAdvMenu->addAction("Utility");

    pAdvMenu->addSeparator();
    m_pActDec     = pAdvMenu->addAction("Decoder");

    pAdvMenu->addSeparator();
    m_pActWRec    = pAdvMenu->addAction("WRec");

    pAdvMenu->addSeparator();
    m_pActSource1 = pAdvMenu->addAction("Source1");
    m_pActSource2 = pAdvMenu->addAction("Source2");

    pAdvMenu->addSeparator();
    m_pActSearch = pAdvMenu->addAction("Search");

    pAdvMenu->addSeparator();

    m_pActUPA = pAdvMenu->addAction("Power Analysis");
    m_pActEyeJit = pAdvMenu->addAction("Eye Jit");
    m_pActJitter = pAdvMenu->addAction("Jitter");

    m_pHelp = pAdvMenu->addAction("Help");


    connect( m_pActRef,     SIGNAL(triggered(bool)), this, SLOT(on_ref_trigger()));
    connect( m_pActSearch,  SIGNAL(triggered(bool)), this, SLOT(on_search_trigger()));
    connect( m_pActLa,      SIGNAL(triggered(bool)), this, SLOT(on_la_trigger()));
    connect( m_pActMask,    SIGNAL(triggered(bool)), this, SLOT(on_mask_trigger()));
    connect( m_pActStorage, SIGNAL(triggered(bool)), this, SLOT(on_storage_trigger()));
    connect( m_pUtility,    SIGNAL(triggered(bool)), this, SLOT(on_utility_menu()));
    connect( m_pActDec,     SIGNAL(triggered(bool)), this, SLOT(on_dec_trigger()));
    connect( m_pActWRec,    SIGNAL(triggered(bool)), this, SLOT(on_wrec_trigger()));
    connect( m_pActSource1, SIGNAL(triggered(bool)), this, SLOT(on_source1_trigger()));
    connect( m_pActSource2, SIGNAL(triggered(bool)), this, SLOT(on_source2_trigger()));
    connect( m_pActUPA,     SIGNAL(triggered(bool)), this, SLOT(on_upa_trigger()));

    connect( m_pActEyeJit,  SIGNAL(triggered(bool)), this, SLOT(on_eyejit_trigger()));
    connect( m_pHelp,       SIGNAL(triggered(bool)), this, SLOT(on_help_trigger()));


    QMenuBar *pMenuBar = menuBar();
    pMenuBar->addMenu( pMenu );
    pMenuBar->addMenu( pAdvMenu );

    #endif

    int menuHeight;
#ifdef _SIMULATE
    menuHeight = 26;
#else
    menuHeight = 0;
#endif

    mp_wigLeft = new menu_res::RDsoDragDropFrame(this);
    Q_ASSERT( NULL != mp_wigLeft );
    mp_wigLeft->setGeometry( _style.rectLeft.translated(0,menuHeight) );

    mp_wigTop  = new menu_res::RDsoDragDropFrame(this);
    Q_ASSERT( NULL != mp_wigTop );
    mp_wigTop->setGeometry( _style.rectTop.translated(0,menuHeight));

    mp_wigDown = new menu_res::RDsoDragDropFrame(this);
    Q_ASSERT( NULL != mp_wigDown );
    mp_wigDown->setGeometry( _style.rectDown.translated(0,menuHeight) );

    mp_wigCent = new menu_res::RDsoDragDropFrame(this);
    Q_ASSERT( NULL != mp_wigCent );
    mp_wigCent->setGeometry( _style.rectCenter.translated(0,menuHeight) );
    mp_wigCent->setObjectName("centerWidget");

    resize( _style.rectMain.width(),
            _style.rectMain.height() + menuHeight );

    //! back layer
    mp_wigBg = new QWidget;
    Q_ASSERT( NULL != mp_wigBg );
    mp_wigBg->setObjectName(QStringLiteral("BACK_LAYER"));
    mp_wigBg->setGeometry( 0,0,1024,600 );
    mp_wigBg->setStyleSheet("background:black");
    mp_wigBg->show();

    touchZone = new TouchZone(this->getCentBar());
    Q_ASSERT( NULL != touchZone );

    //! set main widget
    sysSetMainWindow( this );
    sysSetMidWindow( this->getCentBar() );
    sysSetBgWindow( mp_wigBg );

    //! app widget
    CApp *pApp;
    foreach( pApp, mListApps )
    {
        Q_ASSERT( pApp != NULL );
        pApp->setupUi( this );
    }

    //! touch panel
    touchLabel = new TouchLabel(mp_wigCent);
    Q_ASSERT( NULL != touchLabel );
    touchLabel->resize(mp_wigCent->size());
    touchLabel->setObjectName("centerLabel");
    touchLabel->setStyleSheet("background-color:transparent;");

    //! start button
    start_time();
    m_pStartButton = new menu_res::RDsoImageButton();
    Q_ASSERT( m_pStartButton != NULL );
    m_pStartButton->setParent( mp_wigDown );
    m_pStartButton->setHelpable( false );

    m_pStartButton->loadResource("ui/start/");
    connect( m_pStartButton,
             SIGNAL(clicked(bool)) ,
             this,
             SLOT(on_start_trigger()) );
    end_time();

    //! desktop wnd
    start_time();
    m_pStartWnd = new CAppStartWnd(this);
    Q_ASSERT( NULL != m_pStartWnd );
    m_pStartWnd->loadResource( "appmain/start/wnd/", r_meta::CAppMeta() );

    foreach( pApp, mListApps )
    {
        Q_ASSERT( pApp != NULL );

        //! invalid service
        if ( pApp->mservName.size() < 1  )
        {
            qWarning()<<"app"<<pApp->mName<<"has no menu";
            continue;
        }

        //! a few quick icons
        foreach( AppIcon *pIcon, pApp->mQuickIcons )
        {
            Q_ASSERT( NULL != pIcon );

            m_pStartWnd->addQuick( pIcon );
        }

    }
    end_time();

    //! menu window
    mpMenuDlg = new menu_res::RMenuWnd( this );
    Q_ASSERT( NULL != mpMenuDlg );
    mpMenuDlg->setPosition( _style.mPtExpand, _style.mPtCollapse );
    sysSetMenuWindow( mpMenuDlg );

    #ifdef _SIMULATE
    {
        QPushButton *pBtn = new QPushButton("Quick");
        connect(pBtn, SIGNAL(clicked(bool)), this, SLOT(on_quick()));

        pBtn->setParent(mp_wigDown);
        pBtn->move(920,5);

        pBtn = new QPushButton("MenuOff");
        connect(pBtn, SIGNAL(clicked(bool)), this, SLOT(on_menu()));
        pBtn->setParent(mp_wigDown);
        pBtn->move(920,35);

    }
    #endif

    mpMenuDlg->show();

    //! menu

    start_time();
    mappMenu.loadMenus( mpMenuDlg );
    end_time();

    //! event system
    menu_res::RMenuBuilder::installEventFilter();

    //! visible controls
    m_pStartButton->setVisible( true );
}


void CAppMain::createApps()
{
    CApp *pApp;
    int id;

    pApp = new CAppMenuTest();
    Q_ASSERT( NULL != pApp );
    //mListApps.append( pApp );

    //! display
    pApp = new CAppDisplay();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );


    //! chs
    pApp = new CAppCH( chan1 );
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppCH( chan2 );
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppCH( chan3 );
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppCH( chan4 );
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );


#ifdef _SIMULATE
    //! plot
    pApp = new appPlot();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

#endif


    //! hori
    //! 水平偏移在波形显示前面
    pApp = new CAppHori();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    //! eyejit
    pApp = new CAppEyeJit();
    Q_ASSERT( NULL!=pApp);
    mListApps.append( pApp );

    //! histo
    pApp = new cappHisto();
    Q_ASSERT( NULL!=pApp);
    mListApps.append( pApp);

    //! cursor
    pApp = new CAppCursor();
    Q_ASSERT( NULL != pApp );
    mListApps.append( pApp );

    //! trigger
    pApp = new CAppTrigger();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );


    //! measure
    pApp = new CAppMeasure();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    //! counter
    pApp = new CAppCounter();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    //! DVM
    pApp = new CAppDvm();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    //! DG
    for(int id = 1; id<=2 ; id++)
    {
        pApp = new CAppDG(id);
        Q_ASSERT( NULL!=pApp );
        mListApps.append( pApp );
    }

    pApp = new CAppLA();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );


    //! mask
    pApp = new CAppMask();
    Q_ASSERT( NULL != pApp);
    mListApps.append( pApp);

    //! search
    pApp = new CAppSearch();
    Q_ASSERT( NULL != pApp);
    mListApps.append(pApp);

    //! utility_wrec
    pApp = new CAppRecord();
    Q_ASSERT( NULL != pApp);
    mListApps.append(pApp);

    //! utility_system
    pApp = new CAppUtility();
    Q_ASSERT( NULL != pApp );
    mListApps.append( pApp );

    //! utility_ioset
    pApp = new CAppInterface();
    Q_ASSERT( NULL != pApp );
    mListApps.append( pApp );

    //!utility wifi
    pApp = new CAppWifi();
    Q_ASSERT( NULL != pApp);
    mListApps.append( pApp );

    //!quick
    pApp = new CAppQuick();
    Q_ASSERT( NULL != pApp);
    mListApps.append( pApp );


    //!utility_mail
    pApp = new CAppMail();
    Q_ASSERT( NULL != pApp);
    mListApps.append( pApp );

    //! ref
    pApp = new CAppRef();
    Q_ASSERT( NULL != pApp );
    mListApps.append( pApp );

    //! UPA
    pApp = new CAppUPA();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    //! gui
    pApp = new appGui;
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    //! mathes
    for ( id = 1; id <=4; id++ )
    {
        pApp = new CAppMath( id );
        Q_ASSERT( NULL!=pApp );
        mListApps.append( pApp );
    }

    pApp = new CAppMathGp();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppFFTGp();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    for ( id = 1; id <= 4; id++ )
    {
        pApp = new CAppDecode( id );
        Q_ASSERT( NULL!=pApp );
        mListApps.append( pApp );
    }

    pApp = new CAppDecodeGp();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppStorage();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppHelp();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppCal();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    pApp = new CAppAuto();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

    //! dso
    pApp = new CAppDso();
    Q_ASSERT( NULL!=pApp );
    mListApps.append( pApp );

}
void CAppMain::retranslateUi()
{
}
void CAppMain::buildConnection()
{
    CApp *pApp;
    foreach( pApp, mListApps )
    {
        Q_ASSERT( pApp != NULL );
        pApp->buildConnection();

        pApp->registerSpy();
    }

    connectService();
}

void CAppMain::connectService()
{
    CApp *pApp;


    int from = 0;
    int to = mListApps.size();///8;
    for(int i=from; i<to; i++)
    {
        pApp = mListApps.at(i);
        //! bound
        pApp->boundtoService( serviceExecutor::findService( pApp->getServiceName()) );

        #ifdef FPU_DEBUG
            qDebug() << "app:" << pApp->getServiceName();
        #endif

        //! link
        pApp->linkService();
    }
}

void CAppMain::resizeEvent(QResizeEvent * /*event*/)
{
#ifdef _SIMULATE
    QRect r = contentsRect();
    QPoint pt( r.right(), r.top() );
    mpMenuDlg->setGeometry( pt.x()-218, pt.y() + 44 + 26, 218, 498 + 10 );
#else
    mpMenuDlg->setGeometry( _style.rectMenu );
#endif

#ifdef FPU_DEBUG
    return;
#endif

    CApp *pApp;
    foreach( pApp, mListApps )
    {
        Q_ASSERT( pApp != NULL );
        pApp->resize();
    }
}

void CAppMain::moveEvent(QMoveEvent * /*event*/)
{
    QRect r = contentsRect();

    QPoint pt( r.right(), r.top() );
#ifdef _SIMULATE
    mpMenuDlg->setGeometry( pt.x()-218, pt.y() + 44 + 26, 218, 498 + 10 );
#else
    mpMenuDlg->setGeometry( pt.x()-218, pt.y() + 44 + 10, 218, 498 );
#endif
}

#include "appmain_simulate.cpp"

void CAppMain::on_start_trigger()
{
    //! switch visible
    m_pStartWnd->setVisible( !m_pStartWnd->isVisible() );

    m_pStartWnd->arrangeOn( m_pStartButton, priority_top );
}
void CAppMain::onDbgTimeout()
{
#ifdef _DEBUG
    LOG_ON_FILE();
#endif
}
