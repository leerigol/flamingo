#ifndef TOUCHLABEL_H
#define TOUCHLABEL_H
#include <QtWidgets>
#include "../../service/servicefactory.h"

class TouchLabel : public QLabel
{
    enum CURSORLINE{
        ax,
        bx,
        ay,
        by,
        abx,
        aby
    };
    enum TRIGLEVEL{
        noneL,
        level0,
        level1,
        levelD
    };

    Q_OBJECT
public:
    TouchLabel(QWidget *parent = 0);

protected:
    virtual bool event( QEvent *event );

private:
    bool    onTouchBegin(QEvent *event);
    bool    onTouchUpdate(QEvent *event);
    bool    onTouchEnd(QEvent *);

    bool    onTouchBeginCursor(QPoint touchP);
    bool    onTouchBeginTrigLevel(QPoint touchP);

    void    onUpdatePCursor(QPointF p);
    void    onUpdatePLa(QPointF p);
    void    onUpdatePRef(QPointF p);
    void    onUpdatePMath(QPointF p, int logicY);
    void    onUpdatePTrigLevel(QPoint pos, QPointF p);
    void    onUpdatePChan(QPoint pos, QPointF p);

    void    onUpdatePinch(QList<QTouchEvent::TouchPoint> l);

    void    getScaleOffsetValue();

    void    chooseWave(QPoint p);
    void    postChoosedWave(int touchI);
    void    distinguishWave(int chx, QPoint p);
    bool    distinguishLa(QPoint p);

    bool    on_cursor_mode_manual(QPoint touchP);
    bool    on_cursor_mode_track(QPoint touchP);
    bool    on_cursor_mode_xy(QPoint touchP);

    void    on_post_mode_manual(QPointF p);
    void    on_post_mode_track(QPointF p);
    void    on_post_mode_xy(QPointF p);

    QString getChanName(Chan ch);

private:
    long long   horiScaleBegin;
    long long   horiOffsetBegin;
    long long   activeScaleBegin;
    long long   activeOffsetBegin;

    long long   horiZoomScaleBegin;
    long long   horiZoomOffsetBegin;

    long long   scaleXYBeginCh1;
    long long   offsetXYBeginCh1;
    long long   scaleXYBeginCh2;
    long long   offsetXYBeginCh2;

    int         mathOperator;

    QString     servName;
    bool        once;
    bool        isHori;
    bool        oncePinch;
    bool        isHoriPinch;

    bool        isOnOff[40], isInArea[40], isInMax[40];

    int         max[40], min[40];
    int         maxAll[40], minAll[40];

    bool        isChanChanged;
    Chan        activeChan;

    bool        isCursorChoosed;
    CURSORLINE  mLine;
    CURSORLINE  twoLine;

    int         posVA, posVB;
    int         posHA, posHB;

    int         posTHA, posTHB;
    int         posTVA, posTVB;

    int         cax, cbx, cay, cby;

    DsoScreenMode scrMode;
    bool        isZoomArea;

    bool        isLaChoosed;
    Chan        choosedChan;

    //trig level
    TRIGLEVEL   levelNum;
    qint64      levelBegin;

    bool        isFirstMove;
    bool        isMoveToUp;

    QPointF  logicLastPoint;

};

#endif // TOUCHLABEL_H
