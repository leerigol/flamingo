#ifndef TOUCHZONE_H
#define TOUCHZONE_H
#include <qwidget.h>
#include <QComboBox>
#include "../../service/servdso/sysdso.h"

#define  ZONE_MENU_TRIG_A         (STRING_TRIG_ZONE_A)
#define  ZONE_MENU_TRIG_B         (STRING_TRIG_ZONE_B)
#define  ZONE_MENU_HISTO          (STRING_HISTO_ZONE)
#define  ZONE_MENU_HOR_ZOOM       (STRING_HOR_ZONE_ZOOM)
#define  ZONE_MENU_VERT_ZOOM      (STRING_VERT_ZONE_ZOOM)
#define  ZONE_MENU_WAVEFORM_ZOOM  (STRING_WAVEFORM_ZONE_ZOOM)

class TouchZone : public QWidget
{
    Q_OBJECT
public:
    TouchZone(QWidget *parent = 0);
    void  paintEvent(QPaintEvent *);
    void  mouseMoveEvent(QMouseEvent * event);
    void  mousePressEvent(QMouseEvent * event);
    void  mouseReleaseEvent(QMouseEvent *event );

public:
    void  rangeCheck(QPoint &point);
    void  convertView(QRect &rect, QWidget *pView);
    QRect getZone();

    void  setTouchEn(bool en);

    void  menuShow();
    void  menuHide();

    void  rectShow();
    void  rectHide();

    void  setMenuVisible(int index, bool en);
    bool  getMenuVisible(int index);

private:
    bool             m_bRectShow;
    QRect            m_zone;
    bool              isZoomOn;

    QListWidget        m_menuCombox;
    QMap<int, bool>  m_menuMap;

protected Q_SLOTS:
    void  on_menu_activated(QListWidgetItem *item);

Q_SIGNALS:
    void  sig_mouse_move_event(QPoint   );
    void  sig_mouse_pressEvent(QPoint   );
    void  sig_mouse_releaseEvent(QPoint );

    void  sig_menu_activated(int);
};



#endif // TOUCHZONE_H
