void CAppMain::on_ch1_trigger()
{
    serviceExecutor::post( serv_name_ch1, CMD_SERVICE_ACTIVE, (int)1 );
}
void CAppMain::on_ch2_trigger()
{
    serviceExecutor::post( serv_name_ch2, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_ch3_trigger()
{
    serviceExecutor::post( serv_name_ch3, CMD_SERVICE_ACTIVE, (int)1 );
}
void CAppMain::on_ch4_trigger()
{
    serviceExecutor::post( serv_name_ch4, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_la_trigger()
{
    bool bNow;
    bNow = sysCheckLicense(OPT_MSO);
    if(!bNow)
        return;

    serviceExecutor::post( serv_name_la, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_ref_trigger()
{
    serviceExecutor::post( serv_name_ref, CMD_SERVICE_ACTIVE, (int)1 );
}

void  CAppMain::on_storage_trigger()
{
    serviceExecutor::post( serv_name_storage, CMD_SERVICE_ACTIVE, (int)1 );

}

void CAppMain::on_mask_trigger()
{
    serviceExecutor::post( serv_name_mask, CMD_SERVICE_ACTIVE, (int)1 );

}

void CAppMain::on_utility_menu()
{
    serviceExecutor::post( serv_name_utility, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_wrec_trigger()
{
    serviceExecutor::post(serv_name_wrec, CMD_SERVICE_ACTIVE, (int)1);

}

void CAppMain::on_hori_trigger()
{
    serviceExecutor::post( serv_name_hori, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_math_trigger()
{
    serviceExecutor::post( serv_name_math_sel, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_cursor_trigger()
{
    serviceExecutor::post( serv_name_cursor, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_test_trigger()
{
    serviceExecutor::post( serv_name_menutester, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_meas_trigger()
{
    serviceExecutor::post( serv_name_measure, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_trigger()
{
#ifndef _GUI_CASE
    serviceExecutor::post(TriggerBase::currTrigger(), CMD_SERVICE_ACTIVE, (int)1 );
#else
//    serviceExecutor::post(serv_name_storage, CMD_SERVICE_ACTIVE, (int) servStorage::CMD_EXPT_MSK );
#endif
    if(mpMenuDlg->isHidden())
    {
        mpMenuDlg->show();
    }
}

void CAppMain::on_display()
{
    serviceExecutor::post(serv_name_display, MSG_APP_MEASURE, true );
    if(mpMenuDlg->isHidden())
    {
        mpMenuDlg->show();
    }
}

void CAppMain::on_quick()
{
    serviceExecutor::post(serv_name_quick, MSG_APP_QUICK, (int)1 );
}
void CAppMain::on_dec_trigger()
{
    serviceExecutor::post(serv_name_decode_sel,CMD_SERVICE_ACTIVE,(int)1);
    if(mpMenuDlg->isHidden())
    {
        mpMenuDlg->show();
    }
}

void CAppMain::on_menu()
{
    if(mpMenuDlg->isHidden())
    {
        mpMenuDlg->show();
    }
    else
    {
        mpMenuDlg->hide();
    }


//    serviceExecutor::post(serv_name_quick, CMD_SERVICE_ACTIVE, (int)1 );
//    if(mpMenuDlg->isHidden())
//    {
//        mpMenuDlg->show();
//    }
}

void CAppMain::on_source1_trigger()
{
    serviceExecutor::post(serv_name_source1, CMD_SERVICE_ACTIVE, (int)1 );
    if(mpMenuDlg->isHidden())
    {
        mpMenuDlg->show();
    }

}

void CAppMain::on_source2_trigger()
{
    serviceExecutor::post(serv_name_source2, CMD_SERVICE_ACTIVE, (int)1 );
    if(mpMenuDlg->isHidden())
    {
        mpMenuDlg->show();
    }
}

void CAppMain::on_help_trigger()
{
    serviceExecutor::post(serv_name_help, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMain::on_search_trigger()
{
    serviceExecutor::post(serv_name_search, CMD_SERVICE_ACTIVE, (int)1);
}

void CAppMain::on_upa_trigger()
{
    serviceExecutor::post(serv_name_upa, CMD_SERVICE_ACTIVE, (int)1);
}

void CAppMain::on_eyejit_trigger()
{
    serviceExecutor::post(serv_name_eyejit, CMD_SERVICE_ACTIVE, (int)1);
}
