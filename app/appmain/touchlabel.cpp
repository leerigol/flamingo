#include "touchlabel.h"
#include "../../app/trigger/apptrigger.h"

#define VDIV 60
#define HDIV 100
#define WAVEWIDE 1000
#define WAVEHEIGHT 480

TouchLabel::TouchLabel(QWidget *parent) : QLabel(parent)
{
    setAttribute(Qt::WA_AcceptTouchEvents);

    horiScaleBegin  = 0;
    horiOffsetBegin = 0;
    activeScaleBegin   = 0;
    activeOffsetBegin  = 0;

    horiZoomScaleBegin  = 0;
    horiZoomOffsetBegin = 0;

    once            = false;
    isHori          = false;
    oncePinch       = false;
    isHoriPinch     = false;
    isChanChanged   = false;
    isCursorChoosed = false;
    mLine           = ax;
    twoLine         = ax;

    isLaChoosed     = false;

    levelNum        = noneL;
    levelBegin      = 0;

    isMoveToUp      = false;
    isFirstMove     = false;

}

bool TouchLabel::event(QEvent *event)
{
    QEvent::Type eventType = event->type();
    if ( eventType == QEvent::TouchBegin )
    {
        return onTouchBegin(event);
    }
    else if( eventType == QEvent::TouchUpdate )
    {
        return onTouchUpdate(event);
    }
    else if (  eventType == QEvent::TouchEnd )
    {
        return onTouchEnd(event);
    }

    return QLabel::event(event);
}

bool TouchLabel::onTouchBegin(QEvent *event)
{
    QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
    QList<QTouchEvent::TouchPoint> l = touchEvent->touchPoints();
    QPoint touchP(0,0);
    touchP += l.first().startPos().toPoint();

    int mode;
    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, mode );
    scrMode = (DsoScreenMode)mode;

    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S23MAREA, isZoomArea );

    if ( 1 == l.size() )
    {
        logicLastPoint = l.first().lastPos();
        getScaleOffsetValue();

        bool b;
        if(CAppTrigger::getTimeout())
        {
            b = onTouchBeginTrigLevel(touchP);
            if(b){
                return true;
            }
        }

        int cMode = 0;
        serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32CURSORMODE, cMode );
        if(cMode)
        {
            b = onTouchBeginCursor( touchP );
            if(b){
                return true;
            }
        }
        else
        {
            isCursorChoosed = false;
        }

        chooseWave(l.first().startPos().toPoint());
    }

    getScaleOffsetValue();
    return true;
}

bool TouchLabel::onTouchUpdate(QEvent *event)
{
    QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
    QList<QTouchEvent::TouchPoint> l = touchEvent->touchPoints();

    if ( 1 == l.size() )
    {
        QPointF p = l.first().lastPos() - l.first().startPos();
        int length = l.first().lastPos().y()-logicLastPoint.y();
        if(isCursorChoosed)
        {
            onUpdatePCursor(p);
        }
        else
        {
            if(qAbs(p.x()) == 0 && qAbs(p.y()) == 0)
                return true;
            if(!once)
            {
                once = true;
                serviceExecutor::query( serv_name_trigger, MSG_TRIGGER_LEVEL, levelBegin);
                if(qAbs(p.x()) >= qAbs(p.y()))
                {
                    isHori = true;
                }
                else
                {
                    isHori = false;
                }
            }
            else
            {
                if(isHori)
                {
                    if(scrMode == screen_yt_main_zoom )
                    {
                        long long cx = p.x();
                        if(cx)
                        {
                            long long offsetNow = horiZoomOffsetBegin - cx*horiZoomScaleBegin/HDIV;
                            serviceExecutor::post( serv_name_hori, MSG_HORI_ZOOM_OFFSET,  (long long)(offsetNow) );
                        }
                    }
                    else if( scrMode == screen_xy_full || scrMode == screen_xy_normal
                             || scrMode == screen_xy_fft )
                    {
                        long long cx = p.x();
                        if(cx)
                        {
                            long long offsetNow = offsetXYBeginCh1 + cx*scaleXYBeginCh1/VDIV;
                            serviceExecutor::post( serv_name_ch1, MSG_CHAN_OFFSET,  (long long)(offsetNow) );
                        }
                    }
                    else
                    {
                        long long cx = p.x();
                        if(cx)
                        {
                            long long offsetNow = horiOffsetBegin - cx*horiScaleBegin/HDIV;
                            serviceExecutor::post( serv_name_hori, servHori::cmd_main_offset,  (long long)(offsetNow) );
                        }
                    }
                }
                else
                {
                    if(isLaChoosed)
                    {
                        QPoint p(0,0);
                        p += l.first().lastPos().toPoint();
                        onUpdatePLa(p);
                    }
                    else if(activeChan==ref)
                    {
                        onUpdatePRef(p);
                    }
                    else if(activeChan<=m4 && activeChan>= m1)
                    {
                        onUpdatePMath(p, length);
                        logicLastPoint = l.first().lastPos();
                    }
                    else if(levelNum > 0)
                    {
                        if(CAppTrigger::getTimeout())
                        {
                            QPoint pos(0,0);
                            pos += l.first().lastPos().toPoint();
                            onUpdatePTrigLevel(pos, p);
                        }
                    }
                    else if(activeChan<=chan4 && activeChan>= chan1)
                    {
                        QPoint pos(0,0);
                        pos += l.first().lastPos().toPoint();
                        onUpdatePChan( pos, p );
                    }
                }
            }
        }
    }
    else if( 2 == l.size())
    {
        onUpdatePinch(l);
    }

    return true;
}

bool TouchLabel::onTouchEnd(QEvent */*event*/)
{
    horiScaleBegin      = 0;
    horiOffsetBegin     = 0;
    activeScaleBegin    = 0;
    activeOffsetBegin   = 0;
    horiZoomScaleBegin  = 0;
    horiZoomOffsetBegin = 0;
    once                = false;
    oncePinch           = false;
    isCursorChoosed     = false;
    twoLine             = ax;
    isLaChoosed         = false;
    levelNum            = noneL;
    isFirstMove         = false;

    return true;
}

bool TouchLabel::onTouchBeginCursor(QPoint touchP)
{
    int focusMsg;
    serviceExecutor::query(serv_name_cursor, CMD_SERVICE_ITEM_FOCUS, focusMsg );
    if(MSG_CURSOR_S32MHABPOS == focusMsg
            || MSG_CURSOR_S32TABPOS == focusMsg
            || MSG_CURSOR_S32XY_XAB == focusMsg )
    {
        twoLine = abx;
    }
    else if(MSG_CURSOR_S32MVABPOS == focusMsg
            || MSG_CURSOR_S32TABPOS_V == focusMsg
            || MSG_CURSOR_S32XY_YAB == focusMsg )
    {
        twoLine = aby;
    }

    int cursorMode;
    serviceExecutor::query( serv_name_cursor, MSG_CURSOR_S32CURSORMODE, cursorMode );
    if ( cursorMode == cursor_mode_manual )
    {
        bool b = on_cursor_mode_manual(touchP);
        if(b)
            return true;
    }
    else if ( cursorMode == cursor_mode_track )
    {
        bool b = on_cursor_mode_track(touchP);
        if(b)
            return true;
    }
    else if ( cursorMode == cursor_mode_xy )
    {
        bool b = on_cursor_mode_xy(touchP);
        if(b)
            return true;
    }

    return false;
}

bool TouchLabel::onTouchBeginTrigLevel(QPoint touchP)
{
    void *ptr;
    serviceExecutor::query( TriggerBase::currTrigger(), MSG_TRIGGER_SOURCE_PTR, ptr);
    Q_ASSERT(ptr != NULL);
    CSource *m_pSource = (CSource*)ptr;

    if ( scrMode == screen_yt_main || scrMode == screen_roll_main)
    {
        if(m_pSource->hasTwoLevel(1) )
        {
            int id;
            serviceExecutor::query( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, id);

            int levelV0 = m_pSource->get_nLevelPost(0);
            levelV0 = 240 - levelV0;
            int levelV1 = m_pSource->get_nLevelPost(1);
            levelV1 = 240 - levelV1;

            if( levelV0 <= touchP.y()+20 && levelV0 >= touchP.y()-20 )
            {
                levelNum = level0;
                serviceExecutor::post( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, 0);
                if(id == 2)
                {
                    levelNum = levelD;
                }
                return true;
            }

            if( levelV1 <= touchP.y()+20 && levelV1 >= touchP.y()-20 )
            {
                levelNum = level1;
                serviceExecutor::post( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, 1);
                if(2 == id)
                {
                    levelNum = levelD;
                }
                return true;
            }
        }
        else
        {
            int levelV0 = m_pSource->get_nLevelPost(0);
            levelV0 = 240 - levelV0;
            if( levelV0 <= touchP.y()+20 && levelV0 >= touchP.y()-20 )
            {
                levelNum = level0;
                return true;
            }
        }

    }else if(scrMode == screen_yt_main_zoom )
    {
        if(touchP.y()>160)
        {
            float  ty = (touchP.y()-160)*1.5;
            touchP.setY(ty);

            if(m_pSource->hasTwoLevel(1) )
            {
                int id;
                serviceExecutor::query( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, id);

                int levelV0 = m_pSource->get_nLevelPost(0);
                levelV0 = 240 - levelV0;
                int levelV1 = m_pSource->get_nLevelPost(1);
                levelV1 = 240 - levelV1;

                if( levelV0 <= touchP.y()+40 && levelV0 >= touchP.y()-40 )
                {
                    levelNum = level0;
                    serviceExecutor::post( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, 0);
                    if(id == 2){
                        levelNum = levelD;
                    }
                    return true;
                }

                if( levelV1 <= touchP.y()+40 && levelV1 >= touchP.y()-40 )
                {
                    levelNum = level1;
                    serviceExecutor::post( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, 1);
                    if(2 == id){
                        levelNum = levelD;
                    }
                    return true;
                }
            }
            else
            {
                int levelV0 = m_pSource->get_nLevelPost(0);
                levelV0 = 240 - levelV0;
                if( levelV0 <= touchP.y()+40 && levelV0 >= touchP.y()-40 )
                {
                    levelNum = level0;
                    return true;
                }
            }

        }
        else if(touchP.y()<144)
        {
            float  ty = touchP.y()*WAVEHEIGHT/144;
            touchP.setY(ty);

            if(m_pSource->hasTwoLevel(1) )
            {
                int id;
                serviceExecutor::query( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, id);

                int levelV0 = m_pSource->get_nLevelPost(0);
                levelV0 = 240 - levelV0;
                int levelV1 = m_pSource->get_nLevelPost(1);
                levelV1 = 240 - levelV1;

                if( levelV0 <= touchP.y()+40 && levelV0 >= touchP.y()-40 )
                {
                    levelNum = level0;
                    serviceExecutor::post( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, 0);
                    if(id == 2){
                        levelNum = levelD;
                    }
                    return true;
                }

                if( levelV1 <= touchP.y()+40 && levelV1 >= touchP.y()-40 )
                {
                    levelNum = level1;
                    serviceExecutor::post( TriggerBase::currTrigger(), MSG_TRIGGER_LEVELSELECT, 1);
                    if(2 == id){
                        levelNum = levelD;
                    }
                    return true;
                }
            }
            else
            {
                int levelV0 = m_pSource->get_nLevelPost(0);
                levelV0 = 240 - levelV0;
                if( levelV0 <= touchP.y()+40 && levelV0 >= touchP.y()-40 )
                {
                    levelNum = level0;
                    return true;
                }
            }
        }
    }

    return false;
}

void TouchLabel::onUpdatePCursor(QPointF p)
{
    int cursorMode;
    serviceExecutor::query( serv_name_cursor, MSG_CURSOR_S32CURSORMODE, cursorMode );

    if ( cursorMode == cursor_mode_manual )
    {
        on_post_mode_manual(p);
    }
    else if ( cursorMode == cursor_mode_track )
    {
        on_post_mode_track(p);
    }
    else if ( cursorMode == cursor_mode_xy )
    {
        on_post_mode_xy(p);
    }
}

void TouchLabel::onUpdatePLa(QPointF p)
{
    dsoVert *pVert;
    pVert = dsoVert::getCH(la);

    if(scrMode == screen_yt_main_zoom )
    {
        if(p.y() > 160)
        {//扩展区
            int choosedNum = (320-(p.y()-160))*(pVert->getyMaxPos()+1)/320;
            CArgument arg;
            arg.append(choosedChan);
            arg.append(choosedNum);
            serviceExecutor::post(serv_name_la, servLa::cmd_dx_pos, arg);

        }
        else if(p.y() <= 144)
        {//上边区域
            int choosedNum = (160-p.y())*(pVert->getyMaxPos()+1)/160;
            CArgument arg;
            arg.append(choosedChan);
            arg.append(choosedNum);
            serviceExecutor::post(serv_name_la, servLa::cmd_dx_pos, arg);
        }
    }
    else
    {//大区域
        int choosedNum = (WAVEHEIGHT-p.y())*(pVert->getyMaxPos()+1)/WAVEHEIGHT;
        CArgument arg;
        arg.append(choosedChan);
        arg.append(choosedNum);
        serviceExecutor::post(serv_name_la, servLa::cmd_dx_pos, arg);
    }
}

void TouchLabel::onUpdatePRef(QPointF p)
{
    long long cx = p.y();
    if(cx)
    {        
        long long offsetNow = activeOffsetBegin - cx*activeScaleBegin/VDIV;
        serviceExecutor::post( servName, MSG_REF_POS,  (long long)(offsetNow) );
    }
}

void TouchLabel::onUpdatePMath(QPointF p, int logicY)
{
    long long cx = p.y();
    if(cx)
    {
        long long offsetNow = activeOffsetBegin - cx*activeScaleBegin/VDIV;
        if( operator_fft == mathOperator )
        {
            serviceExecutor::post( servName, MSG_MATH_FFT_OFFSET,  (long long)(offsetNow) );
        }
        else if(operator_and == mathOperator
                ||operator_or == mathOperator
                ||operator_xor == mathOperator
                ||operator_not == mathOperator )
        {
            if(logicY)
            {
                serviceExecutor::post( servName, MSG_MATH_LOGIC_OFFSET,  -logicY/2 );
            }
        }
        else
        {
            serviceExecutor::post( servName, MSG_MATH_VIEW_OFFSET,  (long long)(offsetNow) );
        }
    }
}

void TouchLabel::onUpdatePTrigLevel(QPoint pos, QPointF p)
{
    if ( scrMode == screen_yt_main || scrMode == screen_roll_main)
    {

    }
    else if(scrMode == screen_yt_main_zoom )
    {
        if(pos.y()>160){
            float  ty = p.y()*1.5;
            p.setY(ty);
        }else if(pos.y()<144){
            float  ty = p.y()*WAVEHEIGHT/144;
            p.setY(ty);
        }
    }

    switch( levelNum )
    {
    case level0:
    {
        qint64 lev = levelBegin-activeScaleBegin*(p.y())/VDIV;
        serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL, lev);
        break;
    }
    case level1:
    {
        qint64 lev1 = levelBegin-activeScaleBegin*(p.y())/VDIV;
        serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL, lev1);
        break;
    }
    case levelD:
    {
        qint64 lev2 = levelBegin-activeScaleBegin*(p.y())/VDIV;
        serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL, lev2);
        break;
    }
    default:
        break;
    }
}

void TouchLabel::onUpdatePChan(QPoint pos, QPointF p)
{
    if( scrMode == screen_xy_full || scrMode == screen_xy_normal
              || scrMode == screen_xy_fft )
    {
        long long cx = p.y();
        if(cx)
        {
            long long offsetNow = offsetXYBeginCh2 - cx*scaleXYBeginCh2/VDIV;
            serviceExecutor::post( serv_name_ch2, MSG_CHAN_OFFSET,  (long long)(offsetNow) );
        }
    }
    else
    {
        if(scrMode == screen_yt_main_zoom )
        {
            if(pos.y()>160)
            {
                float  ty = p.y()*1.5;
                p.setY(ty);
            }
            else if(pos.y()<144)
            {
                float  ty = p.y()*WAVEHEIGHT/144;
                p.setY(ty);
            }
        }

        long long cx = p.y();
        if(cx){
            long long offsetNow = activeOffsetBegin - cx*activeScaleBegin/VDIV;
            serviceExecutor::post( servName, MSG_CHAN_OFFSET,  (long long)(offsetNow) );
        }
    }
}

void TouchLabel::onUpdatePinch(QList<QTouchEvent::TouchPoint> l)
{
    float hStartOffset = l.last().startPos().x() - l.first().startPos().x();
    float vStartOffset = l.last().startPos().y() - l.first().startPos().y();
    float hOffset = l.last().lastPos().x() - l.first().lastPos().x();
    float vOffset = l.last().lastPos().y() - l.first().lastPos().y();

    if(!oncePinch)
    {
        oncePinch = true;
        getScaleOffsetValue();
        QPointF p = l.last().startPos() - l.first().startPos();
        if(qAbs(p.x()) >= qAbs(p.y()))
        {
            isHoriPinch = true;
        }
        else
        {
            isHoriPinch = false;
        }
    }
    else if(l.first().lastPos().toPoint() != l.first().startPos().toPoint() ||
             l.last().lastPos().toPoint() != l.last().startPos().toPoint())
    {
        float k = 1.0;
        //QPointF pCenter = (l.last().lastPos() + l.first().lastPos())/2;
        if( isHoriPinch )
            k = hOffset/hStartOffset;
        else
            k = vOffset/vStartOffset;
        if(k<=0)
            return;

        if(isHoriPinch)
        {
            if(scrMode == screen_yt_main_zoom )
            {
                 long long scaleNow = horiZoomScaleBegin/k;
                 serviceExecutor::post( serv_name_hori, MSG_HORI_ZOOM_SCALE,  (long long)(scaleNow) );
            }
            else if( scrMode == screen_xy_full || scrMode == screen_xy_normal || scrMode == screen_xy_fft )
            {
                long long scaleNow = scaleXYBeginCh1/k;
                serviceExecutor::post( serv_name_ch1, MSG_CHAN_SCALE_VALUE,  (long long)(scaleNow) );
            }
            else
            {
                float hStartOffset = (l.last().startPos().x() + l.first().startPos().x())/2;
                qlonglong expandGnd = hStartOffset-wave_width/2;
                long long scaleNow = horiScaleBegin/k;
                CArgument arg;
                arg.setVal(scaleNow, 0);
                arg.setVal(expandGnd, 1);
                serviceExecutor::post( serv_name_hori, servHori::cmd_pinch_main_scale,  arg );
            }
        }
        else
        {
            if( scrMode == screen_xy_full || scrMode == screen_xy_normal || scrMode == screen_xy_fft )
            {
                long long scaleNow = scaleXYBeginCh2/k;
                serviceExecutor::post( serv_name_ch2, MSG_CHAN_SCALE_VALUE,  (long long)(scaleNow) );
            }
            else
            {
                if(activeChan==ref)
                {
                    long long scaleNow = activeScaleBegin/k;
                    serviceExecutor::post( servName, MSG_REF_VOLT,  (long long)(scaleNow) );
                }
                else if(activeChan<=m4 && activeChan>= m1)
                {
                    long long scaleNow = activeScaleBegin/k;
                    if( operator_fft == mathOperator )
                    {
                        serviceExecutor::post( servName, MSG_MATH_FFT_SCALE,  (long long)(scaleNow) );
                    }
                    else if(operator_and == mathOperator
                            ||operator_or == mathOperator
                            ||operator_xor == mathOperator
                            ||operator_not == mathOperator )
                    {
                        serviceExecutor::post( servName, MSG_MATH_LOGIC_SCALE,  (long long)(scaleNow) );
                    }
                    else
                    {
                        serviceExecutor::post( servName, MSG_MATH_S32FUNCSCALE,  (long long)(scaleNow) );
                    }
                }
                else
                {
                    long long scaleNow = activeScaleBegin/k;
                    serviceExecutor::post( servName, MSG_CHAN_SCALE_VALUE,  (long long)(scaleNow) );

                    float vMidStartOffset = (l.last().startPos().y() - l.first().startPos().y())/2;
                    float vMidLastOffset = (l.last().lastPos().y() - l.first().lastPos().y())/2;

                    if(!isFirstMove)
                    {
                        isFirstMove = true;
                        float vMSOffset = (l.last().startPos().y() + l.first().startPos().y())/2;
                        if(activeOffsetBegin > (240-vMSOffset)/60*activeScaleBegin)
                        {
                            isMoveToUp = false;
                        }
                        else
                        {
                            isMoveToUp = true;
                        }
                    }

                    float offsetset = 0.0;
                    float vMidOffset = qAbs(vMidLastOffset)-qAbs(vMidStartOffset);
                    if(isMoveToUp)
                    {
                       offsetset = activeOffsetBegin - vMidOffset/60*activeScaleBegin;
                    }
                    else
                    {
                       offsetset = activeOffsetBegin + vMidOffset/60*activeScaleBegin;
                    }

                    serviceExecutor::post( servName, MSG_CHAN_OFFSET,  (int)(offsetset) );
                }
            }
        }
    }
}

void TouchLabel::getScaleOffsetValue()
{
    serviceExecutor::query( serv_name_hori, servHori::cmd_main_scale,  horiScaleBegin );
    serviceExecutor::query( serv_name_hori, servHori::cmd_main_offset, horiOffsetBegin );

    serviceExecutor::query( serv_name_hori, MSG_HORI_ZOOM_SCALE,  horiZoomScaleBegin );
    serviceExecutor::query( serv_name_hori, MSG_HORI_ZOOM_OFFSET, horiZoomOffsetBegin );

    if( scrMode == screen_xy_full || scrMode == screen_xy_normal
                                 || scrMode == screen_xy_fft )
    {
        scaleXYBeginCh1     = dsoVert::getCH( chan1 )->getYRefScale();
        offsetXYBeginCh1    = dsoVert::getCH( chan1 )->getYRefOffset();
        scaleXYBeginCh2     = dsoVert::getCH( chan2 )->getYRefScale();
        offsetXYBeginCh2    = dsoVert::getCH( chan2 )->getYRefOffset();
    }

    if(!isChanChanged)
        activeChan = dsoVert::getActiveChan();

    servName = getChanName(activeChan);

    if(activeChan<=chan4 && activeChan >= chan1)
    {
        activeScaleBegin   = dsoVert::getCH( activeChan )->getYRefScale();
        activeOffsetBegin  = dsoVert::getCH(activeChan)->getYRefOffset();
    }
    else if((activeChan<=r10 && activeChan >= r1) || activeChan == ref )
    {
        serviceExecutor::query( serv_name_ref, MSG_REF_POS,  activeOffsetBegin );
        serviceExecutor::query( serv_name_ref, MSG_REF_VOLT,  activeScaleBegin );
    }
    else if(activeChan<=m4 && activeChan >= m1 )
    {
        serviceExecutor::query(servName, MSG_MATH_S32MATHOPERATOR, mathOperator);
        if( operator_fft == mathOperator )
        {
            serviceExecutor::query( servName, MSG_MATH_FFT_OFFSET,  activeOffsetBegin );
            serviceExecutor::query( servName, MSG_MATH_FFT_SCALE,  activeScaleBegin );
        }
        else if(operator_and == mathOperator
                ||operator_or == mathOperator
                ||operator_xor == mathOperator
                ||operator_not == mathOperator )
        {
            serviceExecutor::query( servName, MSG_MATH_LOGIC_OFFSET,  activeOffsetBegin );
            serviceExecutor::query( servName, MSG_MATH_LOGIC_SCALE,  activeScaleBegin );
        }
        else
        {
            serviceExecutor::query( servName, MSG_MATH_VIEW_OFFSET,  activeOffsetBegin );
            serviceExecutor::query( servName, MSG_MATH_S32FUNCSCALE,  activeScaleBegin );
        }
    }
}

void TouchLabel::chooseWave(QPoint p)
{
    bool b;

    for(int i=0; i<40; i++)
    {
        isInArea[i] = false;
        isOnOff[i] = false;
        isInMax[i] = false;
    }
    isChanChanged   = false;

    b = sysCheckLicense(OPT_MSO);
    if(b)
    {
        bool bEn;
        serviceExecutor::query( serv_name_la, MSG_LA_ENABLE,  bEn );
        if(bEn)
        {
            if(distinguishLa(p))
                return;
        }
    }

    if(servMath::mathOn)
    {
        for(int i=36; i<40; i++)
            distinguishWave(i, p);
    }

    for(int i=0; i<4; i++)
    {
        distinguishWave(i, p);
    }

    serviceExecutor::query( serv_name_ref, servRef::MSG_REF_SHOW,  b );
    if(b)
    {
        for(int i=26; i<36; i++)
            distinguishWave(i, p);
    }

    int touchCount  = 0;
    QList<int> touchI;
    for(int i=0; i<40; i++)
    {
        if(isInMax[i])
        {
            touchCount++;
            touchI.append(i+1);
        }
    }

    if( 1 == touchCount )
    {
        postChoosedWave(touchI.first()-1);
        return ;
    }
    else if( touchCount > 1)
    {
        int touchAreaCount = 0;
        QList<int> touchAreaI;

        for(int i=0; i<40; i++)
        {
            if(isInArea[i])
            {
                touchAreaCount++;
                touchAreaI.append( i+1);
            }
        }

        if( 1 == touchAreaCount )
        {
            postChoosedWave(touchAreaI.first()-1);
            return;
        }
        else if( touchAreaCount > 1)
        {
            for(int i=0; i< dsoVert::getChanList().size(); i++)
            {
                if(touchAreaI.contains(dsoVert::getChanList().at(i)))
                {
                    postChoosedWave(dsoVert::getChanList().at(i)-1);
                    return ;
                }
            }
        }

        for(int i=0; i< dsoVert::getChanList().size(); i++)
        {
            if(touchI.contains(dsoVert::getChanList().at(i)))
            {
                postChoosedWave(dsoVert::getChanList().at(i)-1);
                return ;
            }
        }
    }
}

void TouchLabel::postChoosedWave(int touchI )
{
    if(touchI<chan4 && touchI >= 0)
    {
        if(dsoVert::getActiveChan() != touchI+1)
        {
            isChanChanged   = true;
            activeChan = (Chan)(touchI+1);
            QString sName = getChanName((Chan)(touchI+1));

            int isShow = 0;
            serviceExecutor::query( sName, CMD_SERVICE_ACTIVE, isShow );

            if(!isShow)
                serviceExecutor::post( sName, CMD_SERVICE_ACTIVE, (int)1 );
        }
    }
    else if(touchI<r10 && touchI >= r1-1 )
    {
        if(dsoVert::getActiveChan() != ref )
        {
            serviceExecutor::post( serv_name_ref, MSG_REF_CHANNEL, touchI+1-r1 );

            isChanChanged   = true;
            activeChan = ref;
            int isShow = 0;
            serviceExecutor::query( serv_name_ref, CMD_SERVICE_ACTIVE, isShow );
            if(!isShow)
            {
                serviceExecutor::post( serv_name_ref, CMD_SERVICE_ACTIVE, (int)1 );
            }
        }
    }
    else if(touchI<m4 && touchI >= m1-1 )
    {

        if(dsoVert::getActiveChan() != touchI+1 )
        {
            isChanChanged   = true;
            activeChan = (Chan)(touchI+1);
            QString sName = getChanName((Chan)(touchI+1));

            int isShow = 0;
            serviceExecutor::query( sName, CMD_SERVICE_ACTIVE, isShow );
            if(!isShow)
                serviceExecutor::post( sName, CMD_SERVICE_ACTIVE, (int)1 );
        }
    }
}

void TouchLabel::distinguishWave(int chx, QPoint p)
{
    dsoVert *pVert;
    DsoWfm  dsoWfm;
    pVert = dsoVert::getCH( Chan(chx+1) );
    if ( NULL == pVert )
    {
        qDebug()<<"NULL == pVert";
        return;
    }

    isOnOff[chx] = pVert->getOnOff();

    if(isOnOff[chx])
    {
        //pVert->getPixel( dsoWfm );
        //pVert->getPixel( dsoWfm ,chan_none, horizontal_view_zoom);//zoom

        if ( scrMode == screen_yt_main || scrMode == screen_roll_main)
        {
            pVert->getPixel( dsoWfm );
        }
        else if(scrMode == screen_yt_main_zoom )
        {
            if(p.y()>160){
                pVert->getPixel( dsoWfm ,chan_none, horizontal_view_zoom);//zoom
                float  ty = (p.y()-160)*1.5;
                p.setY(ty);
            }else if(p.y()<144){
                pVert->getPixel( dsoWfm );
                float  ty = p.y()*WAVEHEIGHT/144;
                p.setY(ty);
            }
        }

        if ( dsoWfm.size() < 1 )
        {
            qDebug()<<"no dsoWfm data!";
            return;
        }
        else
        {
            DsoPoint *point = dsoWfm.getPoint();

            maxAll[chx] = point[0];
            minAll[chx] = point[0];
            for(int i=0; i<dsoWfm.size(); i++){
                maxAll[chx] = maxAll[chx]>point[i] ? maxAll[chx]:point[i];
                minAll[chx] = minAll[chx]<point[i] ? minAll[chx]:point[i];
            }
            float maxY = 0.0;
            if(maxAll[chx] < 225){
                maxY = (maxAll[chx]-25);
                maxY = maxY/200;
                maxY = (1-maxY)*WAVEHEIGHT;
            }
            maxAll[chx] = maxY;

            float minY = 0.0;
            if(minAll[chx] < 225){
                minY = (minAll[chx]-25);
                minY = minY/200;
                minY = (1-minY)*WAVEHEIGHT;
            }
            minAll[chx] = minY;

            int iAvg = (minAll[chx]+maxAll[chx])/2;
            if( minAll[chx]-maxAll[chx] < 60){
                maxAll[chx] = iAvg-30;
                minAll[chx] = iAvg+30;
            }

            if( p.y()<minAll[chx] && p.y()>maxAll[chx] )
            {
                isInMax[chx] = true;
                DsoPoint *pointArea;
                int pY[40];
                int width = 0;
                if(p.x()<=20)
                {
                    width = p.x()+20;
                    pointArea = &point[0];
                }
                else if(WAVEWIDE-p.x() <= 20)
                {
                    width = 1020-p.x();
                    pointArea = &point[p.x()-20];
                }
                else
                {
                    width = 40;
                    pointArea = &point[p.x()-20];
                }

                for(int i=0; i<width; i++)
                {
                    float ay = 0.0;
                    if(pointArea[i] < 225)
                    {
                        ay = (pointArea[i]-25);
                        ay = ay/200;
                        ay = (1-ay)*WAVEHEIGHT;
                    }
                    pY[i] = ay;

                    max[chx] = pY[0];
                    min[chx] = pY[0];
                    max[chx] = max[chx] > pY[i] ? max[chx] : pY[i];
                    min[chx] = min[chx] < pY[i] ? min[chx] : pY[i];
                    int areaAvg = (min[chx]+max[chx])/2;
                    if( max[chx]-min[chx] < 40){
                        min[chx] = areaAvg-20;
                        max[chx] = areaAvg+20;
                    }
                    if( p.y()>min[chx] && p.y()<max[chx] )
                    {
                        isInArea[chx] = true;
                        break;
                    }
                }
            }
        }
    }
}


bool TouchLabel::distinguishLa( QPoint p)
{
    dsoVert *pVert;
    pVert = dsoVert::getCH(la);

    if(pVert->getOnOff())
    {
        //LOG_DBG()<<pVert->getyMaxPos();// 最大个数
        //LOG_DBG()<<pVert->getyPos(d0);//d0位置

        if(scrMode == screen_yt_main_zoom )
        {
            if(p.y() > 160)
            {
                int choosedNum = (320-(p.y()-160))*(pVert->getyMaxPos()+1)/320;
                for(int i=d0; i<=d15; i++)
                {
                    if( pVert->getyPos((Chan)i) == choosedNum)
                    {
                        isLaChoosed = true;
                        choosedChan = (Chan)i;
                        serviceExecutor::post(serv_name_la, MSG_LA_CURRENT_CHAN, (Chan)i);
                        serviceExecutor::post( serv_name_la, CMD_SERVICE_ACTIVE, (int)1 );
                        return true;
                    }
                }
            }
            else if(p.y() <= 144)
            {
                int choosedNum = (160-p.y())*(pVert->getyMaxPos()+1)/160;
                for(int i=d0; i<=d15; i++)
                {
                    if( pVert->getyPos((Chan)i) == choosedNum)
                    {
                        isLaChoosed = true;
                        choosedChan = (Chan)i;
                        serviceExecutor::post(serv_name_la, MSG_LA_CURRENT_CHAN, (Chan)i);
                        serviceExecutor::post( serv_name_la, CMD_SERVICE_ACTIVE, (int)1 );
                        return true;
                    }
                }
            }
        }
        else
        {
            int choosedNum = (WAVEHEIGHT-p.y())*(pVert->getyMaxPos()+1)/WAVEHEIGHT;

            for(int i=d0; i<=d15; i++)
            {
                if( pVert->getyPos((Chan)i) == choosedNum)
                {
                    isLaChoosed = true;
                    choosedChan = (Chan)i;
                    serviceExecutor::post(serv_name_la, MSG_LA_CURRENT_CHAN, (Chan)i);
                    serviceExecutor::post( serv_name_la, CMD_SERVICE_ACTIVE, (int)1 );
                    return true;
                }
            }
        }
    }
    return false;
}

bool TouchLabel::on_cursor_mode_manual(QPoint touchP)
{
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32MVAPOS, posVA );
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32MVBPOS, posVB );
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32MHAPOS, posHA );
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32MHBPOS, posHB );
    QRect   rectVA(0, posVA-30, WAVEWIDE, 60);
    QRect   rectVB(0, posVB-30, WAVEWIDE, 60);
    QRect   rectHA(posHA-30, 0, 60, WAVEHEIGHT);
    QRect   rectHB(posHB-30, 0, 60, WAVEHEIGHT);
    if ( scrMode == screen_yt_main || scrMode == screen_roll_main)
    {
        if(rectVA.contains(touchP)&&!rectVB.contains(touchP)
                &&!rectHA.contains(touchP)&&!rectHB.contains(touchP))
        {
            mLine           = ay;
            if(twoLine == aby)
                mLine = twoLine;
            isCursorChoosed = true;
            serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ay, 1 );
            return true;
        }
        else if(rectVB.contains(touchP)
                 &&!rectHA.contains(touchP)
                &&!rectHB.contains(touchP))
        {
            mLine           = by;
            if(twoLine == aby)
                mLine = twoLine;
            isCursorChoosed = true;
            serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_by, 1 );
            return true;
        }
        else if(!rectVA.contains(touchP)&&!rectVB.contains(touchP)
                 &&rectHA.contains(touchP)&&!rectHB.contains(touchP))
        {
            mLine           = ax;
            if(twoLine == abx)
                mLine = twoLine;
            serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ax, 1 );
            isCursorChoosed = true;
            return true;
        }
        else if(!rectVA.contains(touchP)
                &&!rectVB.contains(touchP)
                 &&rectHB.contains(touchP))
        {
            mLine           = bx;
            if(twoLine == abx)
                mLine = twoLine;
            isCursorChoosed = true;
            serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_bx, 1 );
            return true;
        }
    }
    else if(scrMode == screen_yt_main_zoom )
    {
        if(isZoomArea){
            float  ty = (touchP.y()-160)*1.5;
            touchP.setY(ty);
            if(rectVA.contains(touchP)&&!rectVB.contains(touchP)
                    &&!rectHA.contains(touchP)&&!rectHB.contains(touchP))
            {
                mLine           = ay;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ay, 1 );
                return true;
            }
            else if(rectVB.contains(touchP)
                     &&!rectHA.contains(touchP)
                    &&!rectHB.contains(touchP))
            {
                mLine           = by;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_by, 1 );
                return true;
            }
            else if(!rectVA.contains(touchP)&&!rectVB.contains(touchP)
                     &&rectHA.contains(touchP)&&!rectHB.contains(touchP))
            {
                mLine           = ax;
                if(twoLine == abx)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ax, 1 );
                return true;
            }
            else if(!rectVA.contains(touchP)
                     &&!rectVB.contains(touchP)
                     &&rectHB.contains(touchP))
            {
                mLine           = bx;
                if(twoLine == abx)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_bx, 1 );
                return true;
            }
        }
        else
        {
            float  ty = touchP.y()*WAVEHEIGHT/144;
            touchP.setY(ty);
            if(rectVA.contains(touchP)&&!rectVB.contains(touchP)
                    &&!rectHA.contains(touchP)&&!rectHB.contains(touchP))
            {
                mLine           = ay;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ay, 1 );
                return true;
            }
            else if(rectVB.contains(touchP)
                     &&!rectHA.contains(touchP)
                    &&!rectHB.contains(touchP))
            {
                mLine           = by;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_by, 1 );
                return true;
            }
            else if(!rectVA.contains(touchP)&&!rectVB.contains(touchP)
                     &&rectHA.contains(touchP)&&!rectHB.contains(touchP))
            {
                mLine           = ax;
                if(twoLine == abx)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ax, 1 );
                return true;
            }
            else if(!rectVA.contains(touchP)&&!rectVB.contains(touchP)
                     &&rectHB.contains(touchP))
            {
                mLine           = bx;
                if(twoLine == abx)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_bx, 1 );
                return true;
            }
        }
    }

    return false;
}

bool TouchLabel::on_cursor_mode_track(QPoint touchP)
{
    bool type;
    serviceExecutor::query( serv_name_cursor, MSG_CURSOR_TRACK_MODE, type );

    if(!type)
    {
        serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32TAPOS, posTHA );
        serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32TBPOS, posTHB );
        QRect rectTHA(posTHA-30, 0, 60, WAVEHEIGHT);
        QRect rectTHB(posTHB-30, 0, 60, WAVEHEIGHT);
        if( rectTHA.contains(touchP) && !rectTHB.contains(touchP) )
        {
            mLine           = ax;
            if(twoLine == abx)
                mLine = twoLine;
            isCursorChoosed = true;
            serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ax, 1 );
            return true;
        }
        else if(rectTHB.contains(touchP))
        {
            mLine           = bx;
            if(twoLine == abx)
                mLine = twoLine;
            isCursorChoosed = true;
            serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_bx, 1 );
            return true;
        }
    }
    else
    {
        if ( scrMode == screen_yt_main || scrMode == screen_roll_main)
        {
            serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32TAPOS_V, posTVA );
            serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32TBPOS_V, posTVB );
            QRect rectTVA(0, posTVA-30, WAVEWIDE, 60);
            QRect rectTVB(0, posTVB-30, WAVEWIDE, 60);
            if( rectTVA.contains(touchP) && !rectTVB.contains(touchP) )
            {
                mLine           = ay;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ay, 1 );
                return true;
            }
            else if( rectTVB.contains(touchP))
            {
                mLine           = by;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_by, 1 );
                return true;
            }
        }
        else if(scrMode == screen_yt_main_zoom)
        {
            serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32TAPOS_V, posTVA );
            serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32TBPOS_V, posTVB );

            QRect rectTVA(0, posTVA-50, WAVEWIDE, 100);
            QRect rectTVB(0, posTVB-50, WAVEWIDE, 100);
            float  ty = (touchP.y()-160)*1.5;
            touchP.setY(ty);
            if( rectTVA.contains(touchP) && !rectTVB.contains(touchP) )
            {
                mLine           = ay;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ay, 1 );
                return true;
            }
            else if( rectTVB.contains(touchP))
            {
                mLine           = by;
                if(twoLine == aby)
                    mLine = twoLine;
                isCursorChoosed = true;
                serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_by, 1 );
                return true;
            }
        }
    }

    return false;
}

bool TouchLabel::on_cursor_mode_xy(QPoint touchP)
{
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32XY_XA, cax );
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32XY_XB, cbx );
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32XY_YA, cay );
    serviceExecutor::query(serv_name_cursor, MSG_CURSOR_S32XY_YB, cby );
    QRect   rectAX(cax-50, 0, 100, WAVEHEIGHT);
    QRect   rectBX(cbx-50, 0, 100, WAVEHEIGHT);
    QRect   rectAY(0, cay-30, WAVEWIDE, 60);
    QRect   rectBY(0, cby-30, WAVEWIDE, 60);
    //float  tx = (touchP.x() - 237)/0.48;
    int tx = touchP.x() - 237;
    touchP.setX(tx);

    if(rectAY.contains(touchP)&&!rectBY.contains(touchP)
            &&!rectAX.contains(touchP)&&!rectBX.contains(touchP))
    {
        mLine           = ay;
        if(twoLine == aby)
            mLine = twoLine;
        isCursorChoosed = true;
        serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ay, 1 );
        return true;
    }
    else if(rectBY.contains(touchP)
             &&!rectAX.contains(touchP)&&!rectBX.contains(touchP))
    {
        mLine           = by;
        if(twoLine == aby)
            mLine = twoLine;
        isCursorChoosed = true;
        serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_by, 1 );
        return true;
    }
    else if(!rectAY.contains(touchP)&&!rectBY.contains(touchP)
             &&rectAX.contains(touchP)&&!rectBX.contains(touchP))
    {
        mLine           = ax;
        if(twoLine == abx)
            mLine = twoLine;
        isCursorChoosed = true;
        serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_ax, 1 );
        return true;
    }
    else if(!rectAY.contains(touchP)&&!rectBY.contains(touchP)
             &&rectBX.contains(touchP))
    {
        mLine           = bx;
        if(twoLine == abx)
            mLine = twoLine;
        isCursorChoosed = true;
        serviceExecutor::post(serv_name_cursor, servCursor::cmd_cursor_raise_bx, 1 );
        return true;
    }

    return false;
}

void TouchLabel::on_post_mode_manual(QPointF p)
{
    if ( scrMode == screen_yt_main || scrMode == screen_roll_main)
    {
        if( ax == mLine )
        {
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHAPOS, posHA+p.toPoint().x() );
        }
        else if( bx == mLine)
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHBPOS, posHB+p.toPoint().x()  );
        else if(ay == mLine)
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVAPOS, posVA+p.toPoint().y()  );
        else if(by == mLine)
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVBPOS, posVB+p.toPoint().y()  );
        else if(abx == mLine)
        {
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHAPOS, posHA+p.toPoint().x() );
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHBPOS, posHB+p.toPoint().x()  );
        }
        else if(aby == mLine)
        {
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVAPOS, posVA+p.toPoint().y()  );
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVBPOS, posVB+p.toPoint().y()  );
        }
    }
    else if(scrMode == screen_yt_main_zoom )
    {
        if(isZoomArea)
        {
            p.setY(p.y()*1.5);
            if( ax == mLine )
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHAPOS, posHA+p.toPoint().x() );
            else if( bx == mLine)
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHBPOS, posHB+p.toPoint().x()  );
            else if(ay == mLine)
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVAPOS, posVA+p.toPoint().y()  );
            else if(by == mLine)
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVBPOS, posVB+p.toPoint().y()  );
            else if(abx == mLine)
            {
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHAPOS, posHA+p.toPoint().x() );
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHBPOS, posHB+p.toPoint().x()  );
            }
            else if(aby == mLine)
            {
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVAPOS, posVA+p.toPoint().y()  );
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVBPOS, posVB+p.toPoint().y()  );
            }
        }
        else
        {
            p.setY(p.y()*WAVEHEIGHT/144);
            if( ax == mLine )
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHAPOS, posHA+p.toPoint().x() );
            else if( bx == mLine)
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHBPOS, posHB+p.toPoint().x()  );
            else if(ay == mLine)
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVAPOS, posVA+p.toPoint().y()  );
            else if(by == mLine)
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVBPOS, posVB+p.toPoint().y()  );
            else if(abx == mLine)
            {
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHAPOS, posHA+p.toPoint().x() );
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MHBPOS, posHB+p.toPoint().x()  );
            }
            else if(aby == mLine)
            {
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVAPOS, posVA+p.toPoint().y()  );
                serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32MVBPOS, posVB+p.toPoint().y()  );
            }
        }
    }
}

void TouchLabel::on_post_mode_track(QPointF p)
{
    bool type;
    serviceExecutor::query( serv_name_cursor, MSG_CURSOR_TRACK_MODE, type );//y==0  record x

    if(!type)
    {
        if( ax == mLine )
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TAPOS, posTHA+p.toPoint().x() );
        else if( bx == mLine)
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TBPOS, posTHB+p.toPoint().x()  );
        else if( abx == mLine)
        {
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TAPOS, posTHA+p.toPoint().x() );
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TBPOS, posTHB+p.toPoint().x()  );
        }
    }
    else
    {
        if(scrMode == screen_yt_main_zoom )
        {
            p.setY(p.y()*WAVEHEIGHT/320);
        }
        if( ay == mLine )
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TAPOS_V, posTVA+p.toPoint().y() );
        else if( by == mLine)
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TBPOS_V, posTVB+p.toPoint().y()  );
        else if( aby == mLine)
        {
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TAPOS_V, posTVA+p.toPoint().y() );
            serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32TBPOS_V, posTVB+p.toPoint().y()  );
        }
    }
}

void TouchLabel::on_post_mode_xy(QPointF p)
{
    if( ax == mLine )
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_XA, cax+p.toPoint().x() );
    else if( bx == mLine)
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_XB, cbx+p.toPoint().x()  );
    else if(ay == mLine)
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_YA, cay+p.toPoint().y()  );
    else if(by == mLine)
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_YB, cby+p.toPoint().y()  );
    else if(abx == mLine)
    {
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_XA, cax+p.toPoint().x() );
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_XB, cbx+p.toPoint().x()  );
    }
    else if(aby == mLine)
    {
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_YA, cay+p.toPoint().y()  );
        serviceExecutor::post(serv_name_cursor, MSG_CURSOR_S32XY_YB, cby+p.toPoint().y()  );
    }
}

QString TouchLabel::getChanName( Chan ch )
{
    switch (ch)
    {
    case chan1:
        return serv_name_ch1;
    case chan2:
        return serv_name_ch2;
    case chan3:
        return serv_name_ch3;
    case chan4:
        return serv_name_ch4;
    case m1:
        return serv_name_math1;
    case m2:
        return serv_name_math2;
    case m3:
        return serv_name_math3;
    case m4:
        return serv_name_math4;
    case ref:
        return serv_name_ref;
    default:
        break;
    }
    return NULL;
}
