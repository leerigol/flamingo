#include <QDebug>
#include <QtWidgets>

#include "cappstartwnd.h"
#include "../../service/servgui/servgui.h"
#include "../../service/service.h"
#include "../../service/service_msg.h"

CAppStartWnd::CAppStartWnd(QWidget *parent) : menu_res::RInfoWnd(parent)
{
    m_pLayout = new QGridLayout();
    setLayout( m_pLayout );

    mRow = 0; mCol = 0;
    m_pLayout->setRowStretch( CAppStartWnd::_rows, 90 );
    m_pLayout->setColumnStretch( CAppStartWnd::_columns, 1 );

    addAllKeys();

    setVisible( false );
}

void CAppStartWnd::loadResource( const QString &path,
                                 const r_meta::CMeta &meta )
{
    //! the style
    mStyle.loadResource( path, meta );

    QSize size;
    meta.getMetaVal( path + "size", size );
    resize( size );
}

void CAppStartWnd::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter( this );
    QRect selfRect;

    selfRect = rect();

    //! paint the frame
    mStyle.paint( painter, selfRect );

}

void CAppStartWnd::keyReleaseEvent(QKeyEvent *)
{
    hide();
}

void CAppStartWnd::keyPressEvent(QKeyEvent *)
{
    hide();
}

void CAppStartWnd::resizeEvent(QResizeEvent */*event*/)
{
    //! do nothing
    //! avoid the base set the wnd pos
}

void CAppStartWnd::addQuick( AppIcon *pIcon )
{
    Q_ASSERT( NULL != pIcon );

    //! new quick button
    menu_res::RDsoQuickLinkButton *pQuickBtn;

    pQuickBtn = new menu_res::RDsoQuickLinkButton(this);
    Q_ASSERT( pQuickBtn );

    connect( pQuickBtn, SIGNAL(sig_clicked(QString, int)),
             this, SLOT(on_appx_clicked(QString, int)) );

    //! attach icon
    pQuickBtn->attachIcon( pIcon );

    //! layout
    m_pLayout->addWidget( pQuickBtn, mRow, mCol );

    pQuickBtn->setVisible( true );

    mCol++;
    if ( mCol >= _columns )
    {
        mRow++;
        mCol = 0;
    }
}

void CAppStartWnd::on_appx_clicked( QString str, int msg )
{
    if( msg == MSG_APP_UTILITY_SHUTDOWN )
    {
        this->hide();
    }

    if(str == serv_name_upa)
    {
        bool b = false;
        b = sysCheckLicense(OPT_PWR);
        if(!b)
        {
            serviceExecutor::post( serv_name_help,
                                   MSG_OPT_HELP_INFO,
                                   (int)OPT_PWR);
            return;
        }
    }

    serviceExecutor::post( str, CMD_SERVICE_ACTIVE, msg );
}
