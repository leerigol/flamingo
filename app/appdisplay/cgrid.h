#ifndef CGrid_H
#define CGrid_H

#include "../capp.h"
#include "../../menu/dsowidget/rdsolabel.h"

#include "../../service/servdisplay/servdisplay.h"

class CGrid : public QWidget
{
    Q_OBJECT

public:
    explicit CGrid(QWidget *parent = 0);
    ~CGrid();

    void setupUi(void);
    enum
    {
        Main_Area,
        Bott_Area,
        All_Area
    };

    static CGrid *getCGridInstance();


    #ifdef _SIMULATE
    const static uint DSO_WAV_START_X = 0;
    const static uint DSO_WAV_START_Y = 0;
    #else
    //normal YT grid
    const static uint DSO_WAV_START_X = 23;
    const static uint DSO_WAV_START_Y = 56;
    #endif
    const static uint DSO_WAV_WIDTH   = 1000;
    const static uint DSO_WAV_HEIGHT  = 480;
    const static uint DSO_GRID_HDIV   = 10;
    const static uint DSO_GRID_VDIV_8 = 8;
    const static uint DSO_GRID_VDIV_10= 10;

    //const static uint DSO_GRID_DIV_H  = 100; //DSO_WAV_WIDTH / DSO_WAV_HDIV
    //const static uint DSO_GRID_DIV_V  = 60;  //DSO_WAV_HEIGHT / DSO_WAV_VDIV

    //fullscreen XY grid
    const static uint DSO_XY_FULL_X     = 260;
    const static uint DSO_XY_FULL_Y     = DSO_WAV_START_Y;
    const static uint DSO_XY_FULL_DIVS  = 8;
    const static uint DSO_XY_FULL_WIDTH  = DSO_WAV_HEIGHT;
    const static uint DSO_XY_FULL_HEIGHT = DSO_WAV_HEIGHT;

    //screen split-TOP
    const static uint DSO_TOP_GRID_X   = DSO_WAV_START_X;
    const static uint DSO_TOP_GRID_Y   = DSO_WAV_START_Y;
    //YT=1000x168,XY=1000x288 OR YT=1000x144，XY=1000x320
    const static uint DSO_TOP_GRID_WIDTH  = DSO_WAV_WIDTH;
    const static uint DSO_TOP_GRID_HEIGHT = 144;

    const static uint DSO_BOTTOM_GRID_X   = DSO_WAV_START_X;
    const static uint DSO_BOTTOM_GRID_Y   = DSO_WAV_START_Y + DSO_TOP_GRID_HEIGHT + 16;
    const static uint DSO_BOTTOM_GRID_HEIGHT = 320;
    const static uint DSO_BOTTOM_XY_X     = 340;
    const static uint DSO_BOTTOM_XY_Y     = DSO_BOTTOM_GRID_Y;

    //screen of IRE
    const static uint DSO_IRE_DIVS  = 15;
    const static uint DSO_IRE_ZERO  = 11;

public:
    static bool    hasTwoGrid(void)    {return m_bTwoGrid;}
    static QRect*  getArea(int a)      {return &m_nArea[a];}
    static int     getWaveHeight(void) {return (int)DSO_WAV_HEIGHT;}
    static int     getWaveWidth(void)  {return (int)DSO_WAV_WIDTH;}
    static int     getVDivPixels(void) {return (int)(DSO_WAV_HEIGHT / m_nVDiv);}

public:
    menu_res::RDsoLabel* getZoomScale();

public Q_SLOTS:
    void setGrid(int grid);
    void setRulersDisplay(bool);
    void setBrightness(int);

    void setXYMode(bool);
    void setXYFull(bool);
    void setFFT(bool);    
    void setFFTFull(bool);

    void setAxisX(qint64,qint64);
    void setAxisY(float,float);
    void setAxisYUnit(QString& unit);
    void clsAxisY(void);

    void setActiveChan(int);
    void setZoom(bool);

    void set10x8_10x10(bool b);
    //void
protected:
    void paintEvent(QPaintEvent *event);

private:
    menu_res::RDsoLabel *m_pZoomScale;
    //Inner data and api
private:
    QPainter *m_pPainter;
    QPen     *m_pPen;
    QFont     m_Font;
    QColor    m_ChanColor;
    float     m_fChanScale;
    float     m_fChanOffset;

    void drawGrid(uint x, uint y, uint w,uint h, uint hdiv,uint vdiv);
    void drawAxisY(uint x, uint y, uint w,uint h,uint div);
    void drawAxisX(uint x, uint y, uint w,uint h,uint div);
    void drawAxisIRE(uint x, uint y, uint w, uint h, uint div);

    void drawAroundRuler(uint x, uint y, uint w, uint h);
    void drawCenterRuler(uint x, uint y, uint w, uint h);

    void drawYT(void);
    void drawXY(bool bFullScreen);
    void drawZoom(void);
    void drawZoomZone(void);
    void drawFFT(bool bFullScreen);
    /*(Institute of Radio Engineers)*/
    void drawIRE(bool b);

    void adjustGrid(void);

    /* Ruler */
    bool m_bShowAxis;
    int  m_nGridType;
    unsigned char m_u8GridIntensity;
    unsigned char m_u8WavIntensity;

    /*YT,XY,ROLL*/
    bool m_bXYMode;
    bool m_bXYFull;
    bool m_bZoomOn;
    bool m_bFFT;
    bool m_bFFTFull;
    bool m_b10x8_10x10;//true=10x8,default. false=10x10

    QList<QString> m_AxisXStr;
    QList<QString> m_AxisYStr;
    QString        m_Unit;

    static bool  m_bTwoGrid;
    static QRect m_nArea[All_Area];
    static int   m_nVDiv;
};

#endif // CGrid_H
