#include <QPainter>
#include <QDebug>
#include "cgrid.h"

#include "../appch/cappch.h"
#include "../../gui/cguiformatter.h"
#include "../../include/dsocfg.h"

//void Q_ASSERT_X ( bool test, const char * where, const char * what )
bool  CGrid::m_bTwoGrid = false;
QRect CGrid::m_nArea[All_Area];
int   CGrid::m_nVDiv = DSO_GRID_VDIV_8;

CGrid::CGrid(QWidget *parent) :  QWidget(parent)
{
    m_bShowAxis         = false;
    m_nGridType         = 0;
    m_u8GridIntensity   = 127;

    m_bXYMode           = false;
    m_bXYFull           = false;
    m_bZoomOn 			= false;
    m_b10x8_10x10       = false;

    m_pPen              = new QPen();

    CGrid::m_bTwoGrid   = false;

    m_ChanColor         = CAppCH::_chColor[chan1];
    m_fChanScale        = 0.1f;
    m_fChanOffset       = 0.0f;
}

CGrid::~CGrid()
{
    delete m_pPen;
}

CGrid* CGrid::getCGridInstance()
{
    static CGrid* pGrid = NULL;
    if(pGrid == NULL)
    {
        pGrid = new CGrid;
        pGrid->lower();
    }
    return pGrid;
}

menu_res::RDsoLabel* CGrid::getZoomScale()
{
    return m_pZoomScale;
}

void CGrid::setupUi(void)
{
    this->resize(screen_width, screen_height);
    this->setStyleSheet("background:black");
//    this->setObjectName(QStringLiteral("BACK_LAYER"));//BACK_LAYER
    this->move(0,0);

    CGrid::m_nArea[Main_Area].setX(DSO_TOP_GRID_X);
    CGrid::m_nArea[Main_Area].setY(DSO_TOP_GRID_Y);
    CGrid::m_nArea[Main_Area].setWidth(DSO_WAV_WIDTH);
    CGrid::m_nArea[Main_Area].setHeight(DSO_WAV_HEIGHT);

    CGrid::m_nArea[Bott_Area].setX(DSO_BOTTOM_GRID_X);
    CGrid::m_nArea[Bott_Area].setY(DSO_BOTTOM_GRID_Y);
    CGrid::m_nArea[Bott_Area].setWidth(DSO_WAV_WIDTH);
    CGrid::m_nArea[Bott_Area].setHeight(DSO_BOTTOM_GRID_HEIGHT);

    //! zoom scale
    m_pZoomScale = new menu_res::RDsoLabel( (QWidget*)this->parent() );
    Q_ASSERT( NULL !=  m_pZoomScale );
    m_pZoomScale->setCaption( "Zoom:" );
    m_pZoomScale->setUnit( Unit_s );
    m_pZoomScale->setAlignment( Qt::AlignCenter );
    m_pZoomScale->setGeometry( 424, 201, 200, 15 );
    m_pZoomScale->hide();
}

/**
 * @brief CGrid::setGrid
 * @param grid
 */
void CGrid::setGrid(int grid)
{
    m_nGridType = (servDisplay::EWaveGrids)grid;
    update();
}

/**
 * @brief CGrid::setRulersDisplay
 * @param b
 */
void CGrid::setRulersDisplay(bool b)
{
    m_bShowAxis = b;
    update();
}

/**
 * @brief CGrid::setBrightness
 * @param uc
 */
void CGrid::setBrightness(int uc)
{
    m_u8GridIntensity = (uc & 0xff);
    update();
}

/**
 * @brief CGrid::setXYMode
 * @param b
 */
void CGrid::setXYMode(bool b)
{
    m_bXYMode = b;
    update();
    adjustGrid();
}

void CGrid::setZoom(bool b)
{
    m_bZoomOn = b;

    update();

    m_pZoomScale->setVisible( b );
    adjustGrid();
}

/**
 * @brief CGrid::setXYFull
 * @param b
 */
void CGrid::setXYFull(bool b)
{
    m_bXYFull = b;
    update();
    adjustGrid();
}

void CGrid::setFFT(bool b)
{
    m_bFFT = b;
    update();
    adjustGrid();
}

void CGrid::setFFTFull(bool b)
{
    m_bFFTFull = b;
    update();
    adjustGrid();
}

/**
 * @brief CGrid::setAxisX
 * @param scale
 * @param offset
 */
void CGrid::setAxisX(qint64 scale, qint64 offset)
{
   // qint64 lower = -4 * scale - offset;
    qint64 lower = -4 * scale + offset;
    QString str;
    if(m_AxisXStr.size())
    {
        m_AxisXStr.clear();
    }

    for(uint i=0; i<DSO_GRID_HDIV; i++)
    {
        float value = lower;
        value = value / 1e12;
        gui_fmt::CGuiFormatter::format(str,value);
        str += "s";
        m_AxisXStr.append(str);
        lower += scale;
    }
    update();
}

/**
 * @brief CGrid::setAxisY
 * @param scale
 * @param offset
 */
void CGrid::setAxisY(float scale, float offset)
{
    float lower ;
    QString str;
    if(m_AxisYStr.size())
    {
        m_AxisYStr.clear();
    }

    m_fChanScale  = scale;
    m_fChanOffset = offset;

    if(DSO_GRID_VDIV_8 == m_nVDiv)
    {
        lower = -4.0 * scale  - offset;
    }
    else
    {
        lower = -5.0 * scale  - offset;
    }
    for(int i=0; i<(m_nVDiv+1); i++)
    {
        gui_fmt::CGuiFormatter::format(str,lower);

        if(str == "-999.9u")
            str = "-1m";
        str += m_Unit;

        m_AxisYStr.insert(0,str);
        lower += scale;

        if(lower > -9.9e-8  && lower < 9.9e-8)//i can't make sure
        {
            lower = 0.0;
        }

    }

    update();
}

void CGrid::clsAxisY()
{
    m_AxisYStr.clear();
    update();
}

void CGrid::setAxisYUnit(QString &unit)
{
    m_Unit = unit;
}

void CGrid::setActiveChan(int ch)
{
    m_ChanColor = CAppCH::_chColor[ch];
}

void CGrid::set10x8_10x10(bool b)
{
    m_b10x8_10x10 = b;
    if(b)
    {
        m_nVDiv = DSO_GRID_VDIV_8;
    }
    else
    {
        m_nVDiv = DSO_GRID_VDIV_10;
    }
    setAxisY(m_fChanScale, m_fChanOffset);
}

///
/// \brief Convert from the absolute location to the ralative
///
///
void CGrid::adjustGrid(void)
{
    int posX = DSO_WAV_START_X;
    int posY = DSO_WAV_START_Y;
    if(m_bXYMode)
    {
        if(m_bXYFull)
        {
            CGrid::m_nArea[Main_Area].setX(DSO_XY_FULL_X - posX);
            CGrid::m_nArea[Main_Area].setY(DSO_XY_FULL_Y - posY);
            CGrid::m_nArea[Main_Area].setWidth(DSO_XY_FULL_WIDTH);
            CGrid::m_nArea[Main_Area].setHeight(DSO_XY_FULL_HEIGHT);
            CGrid::m_bTwoGrid = false;
        }
        else
        {
            CGrid::m_nArea[Main_Area].setX(DSO_TOP_GRID_X - posX);
            CGrid::m_nArea[Main_Area].setY(DSO_TOP_GRID_Y - posY);
            CGrid::m_nArea[Main_Area].setWidth(DSO_WAV_WIDTH);
            CGrid::m_nArea[Main_Area].setHeight(DSO_TOP_GRID_HEIGHT);

            CGrid::m_nArea[Bott_Area].setX(DSO_BOTTOM_XY_X - posX);
            CGrid::m_nArea[Bott_Area].setY(DSO_BOTTOM_XY_Y - posY);
            CGrid::m_nArea[Bott_Area].setWidth(DSO_BOTTOM_GRID_HEIGHT);
            CGrid::m_nArea[Bott_Area].setHeight(DSO_BOTTOM_GRID_HEIGHT);
            CGrid::m_bTwoGrid = true;
        }
    }
    else if(m_bZoomOn)
    {
        CGrid::m_nArea[Main_Area].setX(DSO_TOP_GRID_X - posX);
        CGrid::m_nArea[Main_Area].setY(DSO_TOP_GRID_Y - posY);
        CGrid::m_nArea[Main_Area].setWidth(DSO_WAV_WIDTH);
        CGrid::m_nArea[Main_Area].setHeight(DSO_TOP_GRID_HEIGHT);

        CGrid::m_nArea[Bott_Area].setX(DSO_BOTTOM_GRID_X - posX);
        CGrid::m_nArea[Bott_Area].setY(DSO_BOTTOM_GRID_Y - posY);
        CGrid::m_nArea[Bott_Area].setWidth(DSO_WAV_WIDTH);
        CGrid::m_nArea[Bott_Area].setHeight(DSO_BOTTOM_GRID_HEIGHT);
        CGrid::m_bTwoGrid = true;
    }
    else if(m_bFFT)
    {
        if(m_bFFTFull)
        {
            CGrid::m_nArea[Main_Area].setX(DSO_TOP_GRID_X - posX);
            CGrid::m_nArea[Main_Area].setY(DSO_TOP_GRID_Y - posY);
            CGrid::m_nArea[Main_Area].setWidth(DSO_WAV_WIDTH);
            CGrid::m_nArea[Main_Area].setHeight(DSO_WAV_HEIGHT);
            CGrid::m_bTwoGrid = false;
        }
        else
        {
            CGrid::m_nArea[Main_Area].setX(DSO_TOP_GRID_X - posX);
            CGrid::m_nArea[Main_Area].setY(DSO_TOP_GRID_Y - posY);
            CGrid::m_nArea[Main_Area].setWidth(DSO_WAV_WIDTH);
            CGrid::m_nArea[Main_Area].setHeight(DSO_TOP_GRID_HEIGHT);

            CGrid::m_nArea[Bott_Area].setX(DSO_BOTTOM_GRID_X - posX);
            CGrid::m_nArea[Bott_Area].setY(DSO_BOTTOM_GRID_Y - posY);
            CGrid::m_nArea[Bott_Area].setWidth(DSO_WAV_WIDTH);
            CGrid::m_nArea[Bott_Area].setHeight(DSO_BOTTOM_GRID_HEIGHT);
            CGrid::m_bTwoGrid = true;
        }
    }
    else
    {
        CGrid::m_nArea[Main_Area].setX(DSO_TOP_GRID_X - posX);
        CGrid::m_nArea[Main_Area].setY(DSO_TOP_GRID_Y - posY);
        CGrid::m_nArea[Main_Area].setWidth(DSO_WAV_WIDTH);
        CGrid::m_nArea[Main_Area].setHeight(DSO_WAV_HEIGHT);
        CGrid::m_bTwoGrid = false;
    }

}

void CGrid::paintEvent(QPaintEvent *event)
{
    event = event;
    m_pPainter = new QPainter(this);

    m_Font.setPixelSize(14);
    m_pPainter->setFont(m_Font);

    m_pPen->setStyle(Qt::SolidLine);
    m_pPen->setColor(QColor(m_u8GridIntensity,m_u8GridIntensity,m_u8GridIntensity));
    m_pPainter->setPen(*m_pPen);

    if(m_bXYMode)
    {
        drawXY(m_bXYFull);
    }
    else if(m_bZoomOn)
    {
        drawZoom();
    }
    else if(m_bFFT)
    {
        drawFFT(m_bFFTFull);
    }
    else
    {
        drawYT();
    }
    delete m_pPainter;
}


/**
 * @brief CGrid::drawIRE
 * Trigger is Video and vertical scale is 140mV
 */
void CGrid::drawIRE(bool b)
{
    if(!b)
    {
        uint _div_pixel = DSO_WAV_HEIGHT / DSO_IRE_DIVS;
        drawGrid(DSO_WAV_START_X,
                 DSO_WAV_START_Y,
                 DSO_WAV_WIDTH,
                 DSO_WAV_HEIGHT, 1, DSO_IRE_DIVS);

        m_pPen->setStyle(Qt::DashDotDotLine);
        m_pPainter->setPen(*m_pPen);
        m_pPainter->drawLine(DSO_WAV_START_X,
                             DSO_WAV_START_Y + _div_pixel*DSO_IRE_ZERO,
                             DSO_WAV_START_X+DSO_WAV_WIDTH,
                             DSO_WAV_START_Y + _div_pixel*DSO_IRE_ZERO);

        //draw IRE rule
        drawAxisIRE(DSO_WAV_START_X,
                    DSO_WAV_START_Y,
                    DSO_WAV_WIDTH,
                    DSO_WAV_HEIGHT,DSO_IRE_DIVS);

    }
    else
    {        
        m_pPen->setStyle(Qt::SolidLine);
        m_pPainter->setPen(*m_pPen);
        drawGrid(DSO_TOP_GRID_X,
                 DSO_TOP_GRID_Y,
                 DSO_TOP_GRID_WIDTH,
                 DSO_TOP_GRID_HEIGHT, 1, 1);

        m_pPen->setStyle(Qt::SolidLine);
        m_pPainter->setPen(*m_pPen);
        drawGrid(DSO_BOTTOM_GRID_X,
                 DSO_BOTTOM_GRID_Y,
                 DSO_WAV_WIDTH,
                 DSO_BOTTOM_GRID_HEIGHT, 1,
                 DSO_IRE_DIVS);

        drawAxisIRE(DSO_BOTTOM_GRID_X,
                    DSO_BOTTOM_GRID_Y,
                    DSO_WAV_WIDTH,
                    DSO_BOTTOM_GRID_HEIGHT,
                    DSO_IRE_DIVS);

    }
    m_pPen->setColor(QColor(m_u8GridIntensity,m_u8GridIntensity,m_u8GridIntensity));
    m_pPainter->setPen(*m_pPen);
}

/**
 * @brief CGrid::drawAroundRuler
 * @param x
 * @param y
 * @param w
 * @param h
 */
void CGrid::drawAroundRuler(uint x, uint y, uint w, uint h)
{

    int nStep = 20;
    int nCnt  = w / nStep;

    int zoomTick = m_bZoomOn * 2;
    int x1=x;
    int y1=y;
    int tick=4;

    int x2=x;
    int y2=y + h;

    m_pPen->setStyle(Qt::SolidLine);
    m_pPainter->setPen(*m_pPen);

    for(int i=0; i<nCnt; i++)
    {
        x1 = x1 + nStep;
        x2 = x2 + nStep;
        if( (i+1) % 5 == 0 )
        {
            tick = 7 - zoomTick;
        }
        else
        {
            tick = 4 - zoomTick;
        }
        m_pPainter->drawLine(x1,y1,x1,y1+tick);
        m_pPainter->drawLine(x2,y2,x2,y2-tick);
    }

    if(m_bZoomOn || m_bFFT)
    {
        nStep = 8;
    }
    else
    {
        nStep = 12;
    }

    nCnt  = h / nStep;
    x1    = x;
    y1    = y;

    x2    = x + w;
    y2    = y;
    for(int i=0; i<nCnt; i++)
    {
        y1 = y1 + nStep;
        y2 = y2 + nStep;
        if( (i+1) % 5 == 0 )
        {
            tick = 7 - zoomTick;
        }
        else
        {
            tick = 4 - zoomTick;
        }
        m_pPainter->drawLine(x1,y1,x1+tick,y1);
        m_pPainter->drawLine(x2,y2,x2-tick,y2);
    }

}

void CGrid::drawCenterRuler(uint x, uint y, uint w, uint h)
{
    int nStep = 20;
    int nCnt  = w / nStep;

    int x1=x;
    int y1=y + h/2 - 2;
    int tick=4;

    m_pPen->setStyle(Qt::SolidLine);
    m_pPainter->setPen(*m_pPen);

    for(int i=0; i<nCnt; i++)
    {
        x1 = x1 + nStep;
        m_pPainter->drawLine(x1,y1,x1,y1+tick);
    }

    if(m_bZoomOn || m_bFFT)
    {
        if( h == DSO_TOP_GRID_HEIGHT )
        {
            return;//not
        }
        else
        {
            nStep = 8;
        }
    }
    else
    {
        nStep = 12;
    }

    nCnt  = h / nStep;
    x1    = x + w/2 - 2;
    y1    = y;
    for(int i=0; i<nCnt; i++)
    {
        y1 = y1 + nStep;
        m_pPainter->drawLine(x1,y1,x1+tick,y1);
    }
}

/**
 * @brief CGrid::drawZoom
 */
void CGrid::drawZoom(void)
{

    //Top
    if(m_nGridType == servDisplay::GRID_IS_FULL)
    {
        m_pPen->setStyle(Qt::SolidLine);
        m_pPainter->setPen(*m_pPen);

        drawGrid(DSO_TOP_GRID_X,
                 DSO_TOP_GRID_Y,
                 DSO_TOP_GRID_WIDTH,
                 DSO_TOP_GRID_HEIGHT,
                 DSO_GRID_HDIV,
                 m_nVDiv);

        //Bottom
        drawGrid(DSO_BOTTOM_GRID_X,
                 DSO_BOTTOM_GRID_Y,
                 DSO_TOP_GRID_WIDTH,
                 DSO_BOTTOM_GRID_HEIGHT,
                 DSO_GRID_HDIV, m_nVDiv);

    }

    if(m_nGridType == servDisplay::GRID_IS_IRE)
    {
        drawIRE(true);
    }
    else
    {
        drawGrid(DSO_TOP_GRID_X,
                 DSO_TOP_GRID_Y,
                 DSO_TOP_GRID_WIDTH,
                 DSO_TOP_GRID_HEIGHT, 1, 1);
        m_pPen->setStyle(Qt::SolidLine);
        m_pPainter->setPen(*m_pPen);
        //Bottom
        drawGrid(DSO_BOTTOM_GRID_X,
                 DSO_BOTTOM_GRID_Y,
                 DSO_TOP_GRID_WIDTH,
                 DSO_BOTTOM_GRID_HEIGHT, 1,1);
    }

    drawAroundRuler(DSO_BOTTOM_GRID_X,
                    DSO_BOTTOM_GRID_Y,
                    DSO_TOP_GRID_WIDTH,
                    DSO_BOTTOM_GRID_HEIGHT);

    if(m_nGridType != servDisplay::GRID_IS_NONE)
    {
        m_pPen->setColor(QColor(m_u8GridIntensity, m_u8GridIntensity, m_u8GridIntensity,120));
        m_pPainter->setPen(*m_pPen);

        m_pPainter->drawLine(DSO_TOP_GRID_X,
                             DSO_TOP_GRID_Y + DSO_TOP_GRID_HEIGHT/2,
                             DSO_TOP_GRID_X+DSO_TOP_GRID_WIDTH,
                             DSO_TOP_GRID_Y + DSO_TOP_GRID_HEIGHT/2);

        m_pPainter->drawLine(DSO_TOP_GRID_X + DSO_TOP_GRID_WIDTH/2,
                             DSO_TOP_GRID_Y ,
                             DSO_TOP_GRID_X + DSO_TOP_GRID_WIDTH/2,
                             DSO_TOP_GRID_Y + DSO_TOP_GRID_HEIGHT);


        m_pPainter->drawLine(DSO_BOTTOM_GRID_X,
                             DSO_BOTTOM_GRID_Y + DSO_BOTTOM_GRID_HEIGHT/2,
                             DSO_BOTTOM_GRID_X + DSO_TOP_GRID_WIDTH,
                             DSO_BOTTOM_GRID_Y + DSO_BOTTOM_GRID_HEIGHT/2);


        m_pPainter->drawLine(DSO_BOTTOM_GRID_X + DSO_TOP_GRID_WIDTH/2,
                             DSO_BOTTOM_GRID_Y ,
                             DSO_BOTTOM_GRID_X + DSO_TOP_GRID_WIDTH/2,
                             DSO_BOTTOM_GRID_Y + DSO_BOTTOM_GRID_HEIGHT);

        drawCenterRuler(DSO_TOP_GRID_X,
                        DSO_TOP_GRID_Y,
                        DSO_TOP_GRID_WIDTH,
                        DSO_TOP_GRID_HEIGHT);

        drawCenterRuler(DSO_BOTTOM_GRID_X,
                        DSO_BOTTOM_GRID_Y,
                        DSO_TOP_GRID_WIDTH,
                        DSO_BOTTOM_GRID_HEIGHT);

        m_pPen->setColor(QColor(m_u8GridIntensity,m_u8GridIntensity,m_u8GridIntensity));
        m_pPainter->setPen(*m_pPen);
    }

    /*Draw the rule*/

    if(this->m_bShowAxis)
    {
        drawAxisY(DSO_BOTTOM_GRID_X,
                  DSO_BOTTOM_GRID_Y,
                  DSO_TOP_GRID_WIDTH,
                  DSO_BOTTOM_GRID_HEIGHT,
                  m_nVDiv);

        drawAxisX(DSO_BOTTOM_GRID_X,
                  DSO_BOTTOM_GRID_Y,
                  DSO_TOP_GRID_WIDTH,
                  DSO_BOTTOM_GRID_HEIGHT,
                  DSO_GRID_HDIV);
    }

}

/**
 * @brief CGrid::drawXY
 * @param bFullScreen
 */
void CGrid::drawXY(bool bFullScreen)
{
    if(bFullScreen)
    {
        if(m_nGridType == servDisplay::GRID_IS_FULL)
        {
            drawGrid(DSO_XY_FULL_X, DSO_XY_FULL_Y,
                     DSO_XY_FULL_WIDTH,
                     DSO_XY_FULL_HEIGHT,
                     m_nVDiv, m_nVDiv);
            drawCenterRuler(DSO_XY_FULL_X, DSO_XY_FULL_Y, DSO_XY_FULL_WIDTH, DSO_XY_FULL_HEIGHT);
        }
        else if(m_nGridType == servDisplay::GRID_IS_HALF)
        {
            drawGrid(DSO_XY_FULL_X,
                     DSO_XY_FULL_Y,
                     DSO_XY_FULL_WIDTH,
                     DSO_XY_FULL_HEIGHT, 2,2);
            drawCenterRuler(DSO_XY_FULL_X, DSO_XY_FULL_Y, DSO_XY_FULL_WIDTH, DSO_XY_FULL_HEIGHT);
        }
        else
        {
            drawGrid(DSO_XY_FULL_X,
                     DSO_XY_FULL_Y,
                     DSO_XY_FULL_WIDTH,
                     DSO_XY_FULL_HEIGHT, 1,1);
        }

       drawAroundRuler(DSO_XY_FULL_X,
                        DSO_XY_FULL_Y,
                        DSO_XY_FULL_WIDTH,
                        DSO_XY_FULL_HEIGHT);
        if(m_nGridType != servDisplay::GRID_IS_NONE)
        {
            /*m_pPen->setStyle(Qt::DashDotLine);
            m_pPainter->setPen(*m_pPen);
            m_pPainter->drawLine(DSO_XY_FULL_X,
                                 DSO_XY_FULL_Y + DSO_XY_FULL_HEIGHT/2,
                                 DSO_XY_FULL_X+DSO_XY_FULL_WIDTH,
                                 DSO_XY_FULL_Y + DSO_XY_FULL_HEIGHT/2);

            m_pPainter->drawLine(DSO_XY_FULL_X + DSO_XY_FULL_WIDTH/2,
                                 DSO_XY_FULL_Y ,
                                 DSO_XY_FULL_X + DSO_XY_FULL_WIDTH/2,
                                 DSO_XY_FULL_Y + DSO_XY_FULL_HEIGHT);
           */
        }
        /*Draw the rule
        if(this->m_bShowAxis)
        {
            drawAxisY(DSO_XY_FULL_X-50,
                      DSO_XY_FULL_Y,
                      DSO_XY_FULL_WIDTH,
                      DSO_XY_FULL_HEIGHT,
                      m_nVDiv);

            drawAxisX(DSO_XY_FULL_X,
                      DSO_XY_FULL_Y,
                      DSO_XY_FULL_WIDTH,
                      DSO_XY_FULL_HEIGHT,
                      m_nVDiv);
        }*/
    }
    else
    {
        if(m_nGridType == servDisplay::GRID_IS_FULL)
        {
            drawGrid(DSO_TOP_GRID_X,
                     DSO_TOP_GRID_Y,
                     DSO_TOP_GRID_WIDTH,
                     DSO_TOP_GRID_HEIGHT,
                     DSO_GRID_HDIV, m_nVDiv);
        }
        else
        {
            drawGrid(DSO_TOP_GRID_X,
                     DSO_TOP_GRID_Y,
                     DSO_TOP_GRID_WIDTH,
                     DSO_TOP_GRID_HEIGHT, 1,1);
        }

        m_pPen->setStyle(Qt::SolidLine);
        m_pPainter->setPen(*m_pPen);
        if(m_nGridType == servDisplay::GRID_IS_FULL)
        {
            drawGrid(DSO_BOTTOM_XY_X,
                     DSO_BOTTOM_GRID_Y,
                     DSO_BOTTOM_GRID_HEIGHT,
                     DSO_BOTTOM_GRID_HEIGHT,
                     m_nVDiv, m_nVDiv);
        }
        else
        {
            drawGrid(DSO_BOTTOM_XY_X,
                     DSO_BOTTOM_GRID_Y,
                     DSO_BOTTOM_GRID_HEIGHT,
                     DSO_BOTTOM_GRID_HEIGHT, 1,1);
        }

    }



    // drawCenterRuler(DSO_WAV_START_X, DSO_WAV_START_Y, DSO_WAV_WIDTH, DSO_WAV_HEIGHT);
}

void CGrid::drawFFT(bool bFullScreen)
{
    if(bFullScreen)
    {
        drawYT();
    }
    else
    {
        drawZoom();
    }
}

/**
 * @brief CGrid::drawYT
 */
void CGrid::drawYT(void)
{
    if( m_nGridType == servDisplay::GRID_IS_IRE )
    {
        drawIRE(false);
        return;
    }

    if(m_nGridType == servDisplay::GRID_IS_FULL)
    {
        /*Draw YT grid of default mode*/
        drawGrid(DSO_WAV_START_X,
                 DSO_WAV_START_Y,
                 DSO_WAV_WIDTH,
                 DSO_WAV_HEIGHT,
                 DSO_GRID_HDIV, m_nVDiv);
    }
    else /*if(m_nGridType == servDisplay::GRID_IS_HALF)*/
    {
        drawGrid(DSO_WAV_START_X,
                 DSO_WAV_START_Y,
                 DSO_WAV_WIDTH,
                 DSO_WAV_HEIGHT, 1, 1);
    }


    m_pPen->setStyle(Qt::SolidLine);
    m_pPainter->setPen(*m_pPen);

    //middle
    if(m_nGridType != servDisplay::GRID_IS_NONE)
    {
        m_pPen->setColor(QColor(m_u8GridIntensity, m_u8GridIntensity, m_u8GridIntensity,120));
        m_pPainter->setPen(*m_pPen);

        m_pPainter->drawLine(DSO_WAV_START_X,
                             DSO_WAV_START_Y + DSO_WAV_HEIGHT/2,
                             DSO_WAV_START_X+DSO_WAV_WIDTH,
                             DSO_WAV_START_Y + DSO_WAV_HEIGHT/2);

        m_pPainter->drawLine(DSO_WAV_START_X + DSO_WAV_WIDTH/2,
                             DSO_WAV_START_Y ,
                             DSO_WAV_START_X + DSO_WAV_WIDTH/2,
                             DSO_WAV_START_Y + DSO_WAV_HEIGHT);

        m_pPen->setColor(QColor(m_u8GridIntensity, m_u8GridIntensity, m_u8GridIntensity));
        m_pPainter->setPen(*m_pPen);

        drawCenterRuler(DSO_WAV_START_X, DSO_WAV_START_Y, DSO_WAV_WIDTH, DSO_WAV_HEIGHT);
    }

    // draw ruler around the area
    {
        drawAroundRuler(DSO_WAV_START_X, DSO_WAV_START_Y, DSO_WAV_WIDTH, DSO_WAV_HEIGHT);
    }

    /*Draw the axis*/
    if(this->m_bShowAxis)
    {
        drawAxisY(DSO_WAV_START_X,DSO_WAV_START_Y,DSO_WAV_WIDTH, DSO_WAV_HEIGHT, m_nVDiv);
        drawAxisX(DSO_WAV_START_X,DSO_WAV_START_Y,DSO_WAV_WIDTH, DSO_WAV_HEIGHT, DSO_GRID_HDIV);

    }
}

/**
 * @brief CGrid::drawAxisIRE-Trigger is Video and vertical scale is 140mV
 * @param x
 * @param y
 * @param w
 * @param h
 * @param div
 */
void CGrid::drawAxisIRE(uint x, uint y, uint w, uint h, uint div)
{
    Q_ASSERT (x < w);
    Q_ASSERT (div > 0);

    QRect rect;
    qint64 s64Rule = 100;
    QString strRule;
    //draw right mV rule
    int font_size = m_Font.pixelSize()+2;

    uint _step = h / 11;
    uint _y1 = y + _step;
    uint _y2 = y + _step * 9 / 2;
    uint _y3 = y + _step * 8;

    for(uint _x = x; _x < (x+w); _x++)
    {
        m_pPainter->drawPoint(_x,_y1);
        m_pPainter->drawPoint(_x,_y2);
        m_pPainter->drawPoint(_x,_y3);

        _x+=5;
    }

    m_pPen->setColor(m_ChanColor);
    m_pPainter->setPen(*m_pPen);

    rect.setTop(_y1-font_size/2);
    rect.setBottom(_y1+font_size/2);
    rect.setLeft(x+w-30);
    rect.setRight(x+w);
    strRule = "0.7";
    m_pPainter->drawText(rect,Qt::AlignRight, strRule);

    rect.setTop(_y2-font_size/2);
    rect.setBottom(_y2+font_size/2);
    strRule = "0.35";
    m_pPainter->drawText(rect,Qt::AlignRight, strRule);


    uint _div_pixel =  h / div;
    uint _x1 = x;
    _y1 = y + _div_pixel ;


    rect.setLeft(_x1);
    rect.setRight(_x1+64);

    for(uint i=0; i<8; i++)
    {

        if(i == 7)
        {
            rect.setTop(_y1 - font_size);
            rect.setBottom(_y1 );
        }
        else
        {
            rect.setTop(_y1 - font_size/2);
            rect.setBottom(_y1 + font_size/2);
        }

        strRule = QString("%0").arg(s64Rule);
        m_pPainter->drawText(rect,Qt::AlignLeft, strRule);

        s64Rule -= 20;/* you need to redesign the tick by GND */
        _y1 += _div_pixel + _div_pixel;
    }
}

/**
 * @brief CGrid::drawAxisY
 * @param x
 * @param y
 * @param w
 * @param h
 * @param div
 */
void CGrid::drawAxisY(uint x, uint y, uint w, uint h, uint div)
{

    Q_ASSERT (w <= DSO_WAV_WIDTH);
    Q_ASSERT (h <= DSO_WAV_HEIGHT);
    Q_ASSERT (x < w);
    Q_ASSERT (y < h);

    if(m_AxisYStr.size() == 0)
    {
        return;
    }

    int font_size = m_Font.pixelSize()+2;

    m_pPen->setColor(m_ChanColor);
    m_pPainter->setPen(*m_pPen);
    QRect rect;
    uint _x1 = x+3;
    uint _y1 = y;
    uint _div_pixel = h / div;
    rect.setLeft(_x1);
    rect.setRight(_x1+64);

    for(uint i=0; i<=div; i++)
    {

        if( i == 0)
        {
            rect.setTop(_y1 );
            rect.setBottom(_y1+font_size);
        }
        else if(i == div)
        {
            rect.setTop(_y1 - font_size);
            rect.setBottom(_y1);
        }
        else
        {
            rect.setTop(_y1 - font_size/2);
            rect.setBottom(_y1 + font_size/2);
        }

        m_pPainter->drawText(rect,Qt::AlignLeft, list_at( m_AxisYStr,(i) ) );

        _y1 += _div_pixel ;
    }
}

/**
 * @brief CGrid::drawAxisX
 * @param x
 * @param y
 * @param w
 * @param h
 * @param div
 */
void CGrid::drawAxisX(uint x, uint y, uint w,uint h, uint div)
{
    Q_ASSERT (w <= DSO_WAV_WIDTH);
    Q_ASSERT (h <= DSO_WAV_HEIGHT);
    Q_ASSERT (x < w);
    Q_ASSERT (y < h);

    if(m_AxisXStr.size() == 0)
    {
        return;
    }

    m_pPen->setColor(0xff8000);
    m_pPainter->setPen(*m_pPen);


    int font_size = m_Font.pixelSize()+2;

    uint _div_pixel = w / div;
    uint _x1 = x + _div_pixel;
    uint _y1 = y ;//+ h - font_size;

    QRect rect;
    rect.setTop(_y1);
    rect.setBottom(_y1+font_size);



    for(uint i=0; i<div; i++)
    {
        rect.setLeft(_x1-32);
        rect.setRight(_x1+32);

        if(i == div-1)
        {
            rect.setLeft(_x1-64);
            rect.setRight(_x1);
            m_pPainter->drawText(rect,Qt::AlignRight, list_at(m_AxisXStr,(i)));
        }
        else
        {
            m_pPainter->drawText(rect,Qt::AlignCenter, list_at(m_AxisXStr,(i)));
        }
        _x1 += _div_pixel ;
    }

}

/**
 * @brief CGrid::drawGrid
 * @param x
 * @param y
 * @param w
 * @param h
 * @param hdiv
 * @param vdiv
 */
void CGrid::drawGrid(uint x, uint y, uint w,uint h, uint hdiv,uint vdiv)
{
    Q_ASSERT (w <= DSO_WAV_WIDTH);
    Q_ASSERT (h <= DSO_WAV_HEIGHT);
    //Q_ASSERT (x < w);
    Q_ASSERT (y < h);
    //Q_ASSERT (vdiv < m_nVDiv);
    //Q_ASSERT (vdiv < m_nVDiv);

    //m_pPainter->drawRect(x,y,w,h);
    m_pPainter->drawLine(x,y,x+w,y);
    m_pPainter->drawLine(x,y,x,y+h);
    m_pPainter->drawLine(x+w,y,x+w,y+h );
    m_pPainter->drawLine(x,y+h,x+w,y+h );

    m_pPen->setStyle(Qt::DotLine);
    m_pPainter->setPen(*m_pPen);

    if(vdiv > 1)
    {
        uint _vdiv_pixel = h / vdiv;
        uint _x1 = x;
        uint _y1 = y + _vdiv_pixel ;
        uint _x2 = _x1 + w;
        uint _y2 = _y1 ;

        while(_y1 < (y + h))
        {
            m_pPainter->drawLine(_x1, _y1, _x2, _y2);
            _y1 += _vdiv_pixel ;
            _y2 = _y1;
        }
    }

   if(hdiv > 1)
   {
        uint _hdiv_pixel = w / hdiv;
        uint _x1 = x + _hdiv_pixel;
        uint _y1 = y;
        uint _x2 = _x1;
        uint _y2 = _y1 + h;
        while(_x1<(x+w))
        {
            m_pPainter->drawLine(_x1, _y1, _x2, _y2);
            _x1 += _hdiv_pixel ;
            _x2 = _x1;
        }
   }
}

