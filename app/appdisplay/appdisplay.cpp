#include "appdisplay.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"


//! msg table
IMPLEMENT_CMD( CApp, CAppDisplay )
start_of_entry()

on_set_int_int( MSG_DISPLAY_GRID,           &CAppDisplay::onGridChanged ),
on_set_int_int( MSG_DISPLAY_RULERS,         &CAppDisplay::onRulers ),
on_set_int_int( MSG_DISPLAY_GRID_INTENSITY, &CAppDisplay::onGridBrightness ),
on_set_int_int( MSG_DISPLAY_10X8_10X10,     &CAppDisplay::onDivChanged ),

on_set_int_int( MSG_CHAN1_CHANGED,           &CAppDisplay::onChan1),
on_set_int_int( MSG_CHAN2_CHANGED,           &CAppDisplay::onChan2),
on_set_int_int( MSG_CHAN3_CHANGED,           &CAppDisplay::onChan3),
on_set_int_int( MSG_CHAN4_CHANGED,           &CAppDisplay::onChan4),

on_set_int_int( MSG_HOR_CHANGED,             &CAppDisplay::onHorChanged),

//on_set_void_void( CMD_SERVICE_VIEW_CHANGE,   &CAppDisplay::onViewChanged ),

on_set_int_int( MSG_ACTIVE_CHAN,             &CAppDisplay::onChanActive ),

end_of_entry()


CAppDisplay::CAppDisplay( )
{
    mservName =  serv_name_display;

//    addQuick( MSG_APP_DISPLAY,
//              QStringLiteral("ui/desktop/display/icon/") );

    m_ActiveChan = serv_name_ch1;
}


void CAppDisplay::setupUi( CAppMain *pMain )
{
    m_pGrid = CGrid::getCGridInstance();
    Q_ASSERT(m_pGrid != NULL );
    m_pGrid->setParent( pMain->getBackground() );

    m_pGrid->setupUi();
    m_pGrid->show();

#ifdef _SIMULATE
    m_pGrid->setParent(pMain->getCentBar());
#else
    pMain = pMain;
#endif

    onHorChanged(cause_visible);
}

void CAppDisplay::retranslateUi( )
{
    //DO NOTHING
}

void CAppDisplay::buildConnection()
{

}


void CAppDisplay::registerSpy()
{
    spyOn( serv_name_ch1, MSG_CHAN_SCALE,  MSG_CHAN1_CHANGED );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE,  MSG_CHAN2_CHANGED );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE,  MSG_CHAN3_CHANGED );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE,  MSG_CHAN4_CHANGED );

    spyOn( serv_name_ch1, MSG_CHAN_UNIT,   MSG_CHAN1_CHANGED);
    spyOn( serv_name_ch2, MSG_CHAN_UNIT,   MSG_CHAN2_CHANGED);
    spyOn( serv_name_ch3, MSG_CHAN_UNIT,   MSG_CHAN3_CHANGED);
    spyOn( serv_name_ch4, MSG_CHAN_UNIT,   MSG_CHAN4_CHANGED);

    spyOn( serv_name_hori, servHori::cmd_main_scale, MSG_HOR_CHANGED );
    spyOn( serv_name_hori, servHori::cmd_main_offset,MSG_HOR_CHANGED );

    spyOn( serv_name_hori, servHori::cmd_zoom_scale, MSG_HOR_CHANGED );
    spyOn( serv_name_hori, servHori::cmd_zoom_offset,MSG_HOR_CHANGED );


    spyOn( serv_name_vert_mgr, servVertMgr::cmd_active_chan, MSG_ACTIVE_CHAN );


}

int CAppDisplay::onDivChanged(AppEventCause)
{
    bool val ;
    serviceExecutor::query( serv_name_display, MSG_DISPLAY_10X8_10X10, val );
    m_pGrid->set10x8_10x10(val);

    return ERR_NONE;
}


int CAppDisplay::onChan1(AppEventCause)
{
    onChanChanged(serv_name_ch1);
    return ERR_NONE;
}

int CAppDisplay::onChan2(AppEventCause)
{
    onChanChanged(serv_name_ch2);
    return ERR_NONE;
}

int CAppDisplay::onChan3(AppEventCause)
{
    onChanChanged(serv_name_ch3);
    return ERR_NONE;
}

int CAppDisplay::onChan4(AppEventCause)
{
    onChanChanged(serv_name_ch4);
    return ERR_NONE;
}

int CAppDisplay::onChanChanged(QString name)
{
    if( name.compare(m_ActiveChan) == 0)
    {
        float scale,offset;
        QString unit;
        serviceExecutor::query( name, servCH::cmd_scale_real,  scale);
        serviceExecutor::query( name, servCH::cmd_offset_real, offset);
        serviceExecutor::query( name, servCH::qcmd_string_unit,unit);

        m_pGrid->setAxisYUnit( unit );
        m_pGrid->setAxisY(scale, offset);
    }

    return ERR_NONE;
}

int CAppDisplay::onChanActive(AppEventCause)
{
    int val;
    serviceExecutor::query( serv_name_vert_mgr, servVertMgr::cmd_active_chan, val );

    //! only analog channel
    if ( val >= chan1 && val <= chan4 )
    {
        m_pGrid->setActiveChan(val);
        m_ActiveChan = servCH::getServiceName((ServiceId)(E_SERVICE_ID_CH1 + val - 1));
        onChanChanged(m_ActiveChan);
    }
    else if( val == la )
    {
        m_pGrid->clsAxisY();
    }

    return ERR_NONE;
}

int CAppDisplay::onHorChanged(AppEventCause)
{
    bool isZoomOn = false;
    qint64 scale  = 0;
    qint64 offset = 0;
    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON,  isZoomOn);
    if(isZoomOn)
    {
        serviceExecutor::query( serv_name_hori, servHori::cmd_zoom_scale, scale );
        serviceExecutor::query( serv_name_hori, servHori::cmd_zoom_offset, offset);
    }
    else
    {
        serviceExecutor::query( serv_name_hori, servHori::cmd_main_scale, scale );
        serviceExecutor::query( serv_name_hori, servHori::cmd_main_offset, offset);
    }
    m_pGrid->setAxisX(scale, offset);

    return ERR_NONE;
}


int CAppDisplay::onGridChanged(AppEventCause)
{
    int val ;
    serviceExecutor::query( serv_name_display, MSG_DISPLAY_GRID, val );
    Q_ASSERT(val>= servDisplay::GRID_IS_FULL && val <= servDisplay::GRID_IS_IRE);

    m_pGrid->setGrid(val);

    return ERR_NONE;
}

int CAppDisplay::onRulers(AppEventCause)
{
    bool val ;
    serviceExecutor::query( serv_name_display, MSG_DISPLAY_RULERS, val );

    m_pGrid->setRulersDisplay(val);

    return ERR_NONE;
}

int CAppDisplay::onGridBrightness(AppEventCause)
{
    int val ;
    serviceExecutor::query( serv_name_display, MSG_DISPLAY_GRID_INTENSITY, val );

    //! normalize to 0xff(white)
    val = val * 0xff/100;

    m_pGrid->setBrightness(val);

    return ERR_NONE;
}


int CAppDisplay::onZoom(AppEventCause)
{
    bool val;
    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON, val );
    m_pGrid->setZoom(val);
    broadcast();

    return ERR_NONE;
}
void CAppDisplay::onViewChanged()
{
    int scrMode;
    //! get view from servdso
    query( serv_name_dso,
           servDso::CMD_SCREEN_MODE,
           scrMode );

    //! now screen mode
    if ( scrMode == screen_yt_main
         || scrMode == screen_roll_main
         )
    {
        m_pGrid->setZoom(false);

        m_pGrid->setFFT(false);

        m_pGrid->setXYMode(false);
        m_pGrid->setXYFull(false);
    }
    else if ( scrMode == screen_yt_main_zoom )
    {
        m_pGrid->setZoom(true);

        m_pGrid->setFFT(false);

        m_pGrid->setXYMode(false);
        m_pGrid->setXYFull(false);
    }
    else if ( scrMode == screen_yt_main_zfft )
    {
        m_pGrid->setZoom(false);

        m_pGrid->setFFT(true);
        m_pGrid->setFFTFull(false);

        m_pGrid->setXYMode(false);
        m_pGrid->setXYFull(false);
    }
    else if ( scrMode == screen_xy_full )
    {
        m_pGrid->setZoom(false);

        m_pGrid->setFFT(false);

        m_pGrid->setXYMode(true);
        m_pGrid->setXYFull(true);
    }
    else if ( scrMode == screen_xy_normal )
    {
        m_pGrid->setZoom(false);

        m_pGrid->setFFT(false);

        m_pGrid->setXYMode(true);
        m_pGrid->setXYFull(false);
    }
    else
    {}

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}


int CAppDisplay::broadcast(void)
{
    //serviceExecutor::broadcast(MSG_DISPLAY_GRID_CHANGED, 0);

    return ERR_NONE;
}
