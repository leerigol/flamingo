#ifndef APPDISPLAY_H
#define APPDISPLAY_H

#include "./cgrid.h"
#include "../appmain/cappmain.h"

CGrid* getCGridInstance();

class CAppDisplay : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppDisplay();

    enum ListenerEvents
    {
        cmd_base = CMD_APP_BASE,

        MSG_CHAN1_CHANGED,
        MSG_CHAN2_CHANGED,
        MSG_CHAN3_CHANGED,
        MSG_CHAN4_CHANGED,
        MSG_ACTIVE_CHAN,

        MSG_HOR_CHANGED,
    };

public:
    void setupUi( CAppMain *pMain );
    void retranslateUi();
    void buildConnection();

protected:
    void registerSpy();

private:
    int onGridChanged(AppEventCause);
    int onRulers(AppEventCause);
    int onIRE(AppEventCause);
    int onGridBrightness(AppEventCause);
    int onDivChanged(AppEventCause);

private:
    int onChan1(AppEventCause);
    int onChan2(AppEventCause);
    int onChan3(AppEventCause);
    int onChan4(AppEventCause);


    int onChanActive(AppEventCause);
    int onChanChanged(QString);
    int onHorChanged(AppEventCause);
    int onXYActive(AppEventCause);
    int onXYScreen(AppEventCause);
    int onTriggerChanged(AppEventCause);
    int onZoom(AppEventCause);
    void onViewChanged();
    int onPlotOpen(AppEventCause);

private:
    int broadcast(void);

private:
    CGrid*  m_pGrid;
    QString m_ActiveChan;
};

#endif // APPDISPLAY_H

