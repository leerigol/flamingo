#ifndef CPEAKMARKER_H
#define CPEAKMARKER_H

#include <QPainter>
#include <QWidget>
#include "../../service/servmath/servmath.h"

class CPeakMarker:public QWidget
{
    enum MarkerDispType
    {
        main_only,
        half_screen,
        main_and_zoom,
    };
public:
    CPeakMarker(QWidget* parent = 0);
    
    void setupUI();

    void toPoint(QString servName);
    void paintEvent(QPaintEvent*);

private:
    void setMainPoint(QVector<measPeakStru> *pPeakData, qlonglong freqStart, qlonglong freqEnd,
                      float ampScale, float ampOffset);
    void setZoomPoint(QVector<measPeakStru> *pPeakData, qlonglong freqStart, qlonglong freqEnd,
                      float ampScale, float ampOffset);

    int m_nW;
    int m_nH;
    int m_widthPen;
    QColor m_Color;
    MarkerDispType   m_markerDispType;
    QVector<QPointF> m_markerMainPoint;
    QVector<QPointF> m_markerZoomPoint;
};
#endif // CPEAKMARKER_H
