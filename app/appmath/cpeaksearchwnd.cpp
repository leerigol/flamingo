#include "cpeaksearchwnd.h"
#include "../../service/servmath/servmath.h"
#include "../../gui/cguiformatter.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpacerItem>

void CPeakSearchwnd_style::load(const QString &path, const r_meta::CMeta &meta)
{
    meta.getMetaVal( path + "mgeo",         mGeo );
    meta.getMetaVal( path + "mrowh",        mRowH );
    meta.getMetaVal( path + "mbtnqss",      mBtnQss );
    meta.getMetaVal( path + "mbtnqss2",      mBtnQss2 );
    int cnum = 0;
    meta.getMetaVal( path + "mtitlecolor",  cnum );
    setColor(mTitleColor, cnum);
    meta.getMetaVal( path + "mgray15",      cnum );
    setColor(mGray15, cnum);
    meta.getMetaVal( path + "mgray27",      cnum );
    setColor(mGray27, cnum);
    meta.getMetaVal( path + "mtitle1",      mTitle1 );
    meta.getMetaVal( path + "mtitle2",      mTitle2 );
}
void CPeakSearchwnd_style::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

menu_res::RInfoWnd_Style CPeakSearchWnd::_bgStyle;
CPeakSearchWnd * CPeakSearchWnd::m_pInstance = 0;
CPeakSearchWnd::CPeakSearchWnd(QWidget *parent) : uiWnd(parent)
{
    m_nPeakNum = 0;

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

CPeakSearchWnd *CPeakSearchWnd::GetInstance()
{
    if(m_pInstance == NULL){
        m_pInstance = new CPeakSearchWnd(sysGetMidWindow());
        m_pInstance->setupUI();
    }
    return m_pInstance;
}

void CPeakSearchWnd::upDatePeakRes(QString servName)
{
    if(servName != QString("math%1").arg(choosedCh))
    {
        return;
    }

    m_freqRes.reserve(20);
    m_ampRes.reserve(20);
    if(!m_freqRes.isEmpty())
    {
        m_freqRes.clear();
        m_ampRes.clear();
    }

    void* p = NULL;
    serviceExecutor::query(servName, servMath::cmd_q_fft_peak,p);

    Q_ASSERT(p != NULL);
    PeakBuffer* peakBuffer = static_cast <PeakBuffer* > (p);
    peakBuffer->peakDataMutex.lock();
    QVector<measPeakStru>* stPeak = &(peakBuffer->pointBuffer);

    //if(p == NULL || stPeak->isEmpty())
    if( p == NULL )
    {
        peakBuffer->peakDataMutex.unlock();
        update();//for bug1629 by hxh
        return;
    }

    qlonglong freqStart = 0;
    serviceExecutor::query(servName,MSG_MATH_FFT_H_START, freqStart);
    qlonglong freqEnd = 0;
    serviceExecutor::query(servName,MSG_MATH_FFT_H_END, freqEnd);
    float ampScale = 0;
    serviceExecutor::query(servName, servMath::cmd_q_real_scale, ampScale);
    float ampOffset = 0;
    serviceExecutor::query(servName,servMath::cmd_q_real_offset, ampOffset);

    QString freq;
    QString ampl;
    //! 在app上限制了峰值个数
    for(int i = 0;i < stPeak->size();i++)
    {
        gui_fmt::CGuiFormatter::format(freq,stPeak->at(i).freq*math_fft_hz_base,fmt_def,Unit_hz);
        int unit;
        serviceExecutor::query(servName, servMath::cmd_math_unit, unit );
        gui_fmt::CGuiFormatter::format(ampl,stPeak->at(i).ampl,fmt_def, (Unit)unit);
        m_freqRes.push_back(freq);
        m_ampRes.push_back(ampl);
    }

    resize(geometry().width(), (m_freqRes.count()+1)*_style.mRowH+50);
    peakBuffer->peakDataMutex.unlock();
    update();
}

void CPeakSearchWnd::setupUI()
{
    _style.loadResource("ui/wnd/fftpeaksearch/");
    _bgStyle.loadResource("measure/");

    setGeometry(_style.mGeo);
    QVBoxLayout *mLayout = new QVBoxLayout;
    mLayout->setContentsMargins(8, 8, 8, 8);
    QHBoxLayout *layout = new QHBoxLayout;
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    math1 = new QPushButton("Math1");
    math2 = new QPushButton("Math2");
    math3 = new QPushButton("Math3");
    math4 = new QPushButton("Math4");
    QSpacerItem *hSpacer2 = new QSpacerItem(20, 20, QSizePolicy::Expanding,
                                            QSizePolicy::Minimum);

    connect(math1, SIGNAL(pressed()),this, SLOT(math1DownQss()));
    connect(math2, SIGNAL(pressed()),this, SLOT(math2DownQss()));
    connect(math3, SIGNAL(pressed()),this, SLOT(math3DownQss()));
    connect(math4, SIGNAL(pressed()),this, SLOT(math4DownQss()));

    math1->setFocusPolicy(Qt::NoFocus);
    math2->setFocusPolicy(Qt::NoFocus);
    math3->setFocusPolicy(Qt::NoFocus);
    math4->setFocusPolicy(Qt::NoFocus);

    math1->setMinimumHeight(29);
    math1->setMinimumWidth(86);
    math2->setMinimumHeight(29);
    math2->setMinimumWidth(86);
    math3->setMinimumHeight(29);
    math3->setMinimumWidth(86);
    math4->setMinimumHeight(29);
    math4->setMinimumWidth(86);

    math1DownQss();

    layout->addWidget(math1);
    layout->addWidget(math2);
    layout->addWidget(math3);
    layout->addWidget(math4);
    layout->addSpacerItem(hSpacer2);
    layout->setGeometry(QRect(8,9,386,29));

    QSpacerItem *hSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum,
                                            QSizePolicy::Expanding);

    mLayout->addLayout(layout);
    mLayout->addSpacerItem(hSpacer);
    setLayout(mLayout);
}

void CPeakSearchWnd::setShow(QString servName)
{
    if(servName == serv_name_math1)
    {
        this->show();
        math1->show();
        math1DownQss();
    }
    if(servName == serv_name_math2)
    {
        this->show();
        math2->show();
        math2DownQss();
    }
    if(servName == serv_name_math3)
    {
        this->show();
        math3->show();
        math3DownQss();
    }
    if(servName == serv_name_math4)
    {
        this->show();
        math4->show();
        math4DownQss();
    }
}

void CPeakSearchWnd::setHide(QString servName)
{
    if(servName == serv_name_math1)
    {
        math1->hide();
    }
    if(servName == serv_name_math2)
    {
        math2->hide();
    }
    if(servName == serv_name_math3)
    {
        math3->hide();
    }
    if(servName == serv_name_math4)
    {
        math4->hide();
    }
    if(math1->isHidden() && math2->isHidden()
            && math3->isHidden() && math4->isHidden())
        this->hide();
}

void CPeakSearchWnd::math1DownQss()
{
    choosedCh = 1;
    math1->setStyleSheet(_style.mBtnQss2);
    math2->setStyleSheet(_style.mBtnQss);
    math3->setStyleSheet(_style.mBtnQss);
    math4->setStyleSheet(_style.mBtnQss);
}

void CPeakSearchWnd::math2DownQss()
{
    choosedCh = 2;
    math1->setStyleSheet(_style.mBtnQss);
    math2->setStyleSheet(_style.mBtnQss2);
    math3->setStyleSheet(_style.mBtnQss);
    math4->setStyleSheet(_style.mBtnQss);
}

void CPeakSearchWnd::math3DownQss()
{
    choosedCh = 3;
    math1->setStyleSheet(_style.mBtnQss);
    math2->setStyleSheet(_style.mBtnQss);
    math3->setStyleSheet(_style.mBtnQss2);
    math4->setStyleSheet(_style.mBtnQss);
}

void CPeakSearchWnd::math4DownQss()
{
    choosedCh = 4;
    math1->setStyleSheet(_style.mBtnQss);
    math2->setStyleSheet(_style.mBtnQss);
    math3->setStyleSheet(_style.mBtnQss);
    math4->setStyleSheet(_style.mBtnQss2);
}

void CPeakSearchWnd::paintEvent(QPaintEvent */*event*/)
{
    QPainter painter(this);
    QFont font;
    font.setPointSize(10);
    painter.setFont(font);

    QRect r = rect();
    _bgStyle.paint(painter,r);

    QRect rLeftBox;
    rLeftBox.setX(r.x()+8);
    rLeftBox.setY(r.y()+37);
    rLeftBox.setWidth(r.width()-15);
    rLeftBox.setHeight(r.height()-37-10);

    painter.fillRect(rLeftBox, QColor(0,88,176,255));

    rLeftBox.setX(rLeftBox.x()+1);
    rLeftBox.setY(rLeftBox.y()+1);
    rLeftBox.setWidth(rLeftBox.width()-1);
    rLeftBox.setHeight(rLeftBox.height()-1);

    const qreal radius = 5;
    QPainterPath path;
    path.moveTo(rLeftBox.topRight() - QPointF(radius, 0));
    path.lineTo(rLeftBox.topLeft() + QPointF(radius, 0));
    path.quadTo(rLeftBox.topLeft(), rLeftBox.topLeft() + QPointF(0, radius));
    path.lineTo(rLeftBox.bottomLeft() + QPointF(0, -radius));
    path.quadTo(rLeftBox.bottomLeft(), rLeftBox.bottomLeft() + QPointF(radius, 0));
    path.lineTo(rLeftBox.bottomRight() - QPointF(radius, 0));
    path.quadTo(rLeftBox.bottomRight(), rLeftBox.bottomRight() + QPointF(0, -radius));
    path.lineTo(rLeftBox.topRight() + QPointF(0, radius));
    path.quadTo(rLeftBox.topRight(), rLeftBox.topRight() + QPointF(-radius, -0));
    painter.fillPath(path, QColor(0,0,0,255));
    painter.drawPath(path);

    QPen     pen;
    pen.setColor(QColor(200,200,200,255));
    painter.setPen(pen);
    //9 38
    rLeftBox.setX(rLeftBox.x()+1);
    rLeftBox.setY(rLeftBox.y()+1);
    rLeftBox.setWidth(rLeftBox.width()-1);
    rLeftBox.setHeight(_style.mRowH);
    painter.fillRect(rLeftBox, _style.mTitleColor);
    painter.drawText(rLeftBox.x()+100,rLeftBox.y(),100,_style.mRowH,Qt::AlignCenter,_style.mTitle1);
    painter.drawText(rLeftBox.x()+200,rLeftBox.y(),100,_style.mRowH,Qt::AlignCenter,_style.mTitle2);

    if(m_freqRes.count())
    {
        for(int i=0; i<m_freqRes.count(); i++)
        {
            painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                             rLeftBox.width(),rLeftBox.height(), _style.mGray15);
            QRect textR = rLeftBox;
            textR.moveTo(rLeftBox.x()+30, rLeftBox.bottom());
            textR.setWidth(100);
            textR.setHeight(_style.mRowH);
            painter.drawText(textR, Qt::AlignLeft|Qt::AlignVCenter, QString::number(i+1));
            textR.moveTo(textR.topRight());
            painter.drawText(textR, Qt::AlignLeft|Qt::AlignVCenter, m_freqRes.at(i));
            textR.moveTo(textR.topRight());
            painter.drawText(textR, Qt::AlignLeft|Qt::AlignVCenter, m_ampRes.at(i));

            i++;
            if( i<m_freqRes.count())
            {
                rLeftBox.moveTo(rLeftBox.bottomLeft());
                painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                                 rLeftBox.width(),rLeftBox.height(), _style.mGray27);
                QRect textR = rLeftBox;
                textR.moveTo(rLeftBox.x()+30, rLeftBox.bottom());
                textR.setWidth(100);
                textR.setHeight(_style.mRowH);
                painter.drawText(textR, Qt::AlignLeft|Qt::AlignVCenter, QString::number(i+1));
                textR.moveTo(textR.topRight());
                painter.drawText(textR, Qt::AlignLeft|Qt::AlignVCenter, m_freqRes.at(i));
                textR.moveTo(textR.topRight());
                painter.drawText(textR, Qt::AlignLeft|Qt::AlignVCenter, m_ampRes.at(i));
            }
            rLeftBox.moveTo(rLeftBox.bottomLeft());
        }
    }
}

