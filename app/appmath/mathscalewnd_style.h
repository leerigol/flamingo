#ifndef MATHSCALEWND_STYLE_H
#define MATHSCALEWND_STYLE_H


#include "../../menu/menustyle/rui_style.h"

class MathScaleFrame_Style : public menu_res::RUI_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta);

    int top;
public:
    //! 返回当前scale所需要的宽度
    virtual int changeGeo(int num);
    QRect mFrameRect;
    QRect mSourceRect;
    QRect mOperatorRect;
    QRect mScaleRect;
    QRect mSaRateRect;
    QRect mWarningRect;
};

class FilterScaleFrame_Style : public MathScaleFrame_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta);

public:
    //! 返回当前scale所需要的宽度
    virtual int changeGeo(int num);
    QRect mSaRateRect;
};

class FftScaleFrame_Style: public MathScaleFrame_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta);

public:
    virtual int changeGeo(int num);
    QRect mHCenterRect;
    QRect mHScaleRect;
    QRect mHResolutionRect;
};

#endif // MATHSCALEWND_STYLE_H
