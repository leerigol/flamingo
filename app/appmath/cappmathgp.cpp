#include "cappmathgp.h"
IMPLEMENT_CMD( CApp, CAppMathGp )
start_of_entry()

end_of_entry()

CAppMathGp::CAppMathGp()
{
    mservName = QString( serv_name_math_sel );

    addQuick( MSG_MATH,
              QString("ui/desktop/math1/icon/") );
}


IMPLEMENT_CMD( CApp, CAppFFTGp )
start_of_entry()

end_of_entry()

CAppFFTGp::CAppFFTGp()
{
    mservName = QString( serv_name_math_fft );

    addQuick( MSG_MATHFFTSEL_FFTSEL,
              QString("ui/desktop/fft/icon/") );
}
