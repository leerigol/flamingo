#include "cthresline.h"
#include "QPainter"
#include "../appdisplay/cgrid.h"
#include <QTime>
bool CThresLine::m_onZoom = false;
///
/// \brief CThresLine::CThresLine
/// \param parent
///
CThresLine::CThresLine(QWidget *parent) : QWidget(parent)
{
    m_nColor = CH1_color;

    m_nStart  = 0;
    m_nRange = CGrid::getWaveHeight();

    m_pLine   = new RDsoVGnd( "F",RDsoGndTag::to_right,RDsoGndTag::cone);
    Q_ASSERT(m_pLine != NULL);
    m_pLine->setVisible( false );

    m_pLine->setView(DSO_VGND_WIDTH, m_nRange);
    m_pLine->setViewGnd(m_nRange/2);

    pZoomLine = NULL;
}

CThresLine::~CThresLine()
{
    if(pZoomLine != NULL)
    {
        delete pZoomLine;
    }
}

void CThresLine::setupUi()
{
    this->move(0, m_nRange/2);
    this->resize(CGrid::getWaveWidth(), 1);
    m_pLine->setColor( CH1_color, Qt::black );

    if(pZoomLine != NULL)
    {
        pZoomLine->move(0, m_nRange/2);
        pZoomLine->resize(CGrid::getWaveWidth(), 1);
    }

    connect(&m_Timer,SIGNAL(timeout()),this,SLOT(onTimeOut()));
}

void CThresLine::setGndParent(QWidget *parent)
{
    m_pLine->setParent(parent);
}

void CThresLine::setColor(QColor color)
{
    m_nColor = color;

    if(pZoomLine != NULL)
    {
        pZoomLine->setColor(color);
    }

    update();
}

void CThresLine::hideGND()
{
    m_pLine->setVisible( false );
    hide();

    if(pZoomLine != NULL)
    {
        pZoomLine->hideGND();
    }
}

void CThresLine::setZoomLine(CThresLine *p)
{
    Q_ASSERT(p != NULL);
    pZoomLine = p;
    pZoomLine->hide();
}

void CThresLine::onTimeOut()
{
    hideGND();
    m_Timer.stop();
}

void CThresLine::setPos(int pos)
{
    m_nPos = pos;
    if(this->isHidden())
    {
        this->show();
    }

    if((pZoomLine != NULL) && (m_onZoom))
    {
        int mainPos = pos * zoom_mainscr_height/CGrid::getWaveHeight();
        int zoonPos = pos * zoom_zoomscr_height/CGrid::getWaveHeight();
        zoonPos = zoonPos + CGrid::getWaveHeight() - zoom_zoomscr_height;
        this->move(0, mainPos);
        pZoomLine->move(0, zoonPos);
        pZoomLine->show();
    }
    else
    {
        this->move(0, pos);
    }

    m_Timer.start(5000);
}

void CThresLine::setOnlyZoomPos(int pos)
{
    m_nPos = pos;

    int zoonPos = pos * zoom_zoomscr_height/CGrid::getWaveHeight();
    zoonPos = zoonPos + CGrid::getWaveHeight() - zoom_zoomscr_height;
    pZoomLine->move(0, zoonPos);
    pZoomLine->show();

    m_Timer.start(5000);
}

int CThresLine::getPos()
{
    return m_nPos;
}

void CThresLine::setRange(int start, int range)
{
    Q_ASSERT(start >= 0 && start <  CGrid::getWaveHeight());
    Q_ASSERT(range >= 0 && range <= CGrid::getWaveHeight());

    m_nStart = start;
    m_nRange = range;
    m_pLine->setPhy(0,start,16,range);
}

void CThresLine::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen pen;

    pen.setStyle(Qt::DashDotLine);
    pen.setColor( m_nColor );
    painter.setPen(pen);

    painter.drawLine(0,0, 0+CGrid::getWaveWidth(),0);
}

void CThresLine::setOnZoom(bool b)
{
    m_onZoom = b;
}
