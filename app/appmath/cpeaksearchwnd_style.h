#ifndef CPEAKSEARCHWND_STYLE_H
#define CPEAKSEARCHWND_STYLE_H

#include <QWidget>
#include <QVector>
#include <QString>
#include <QHBoxLayout>
#include "../../menu/menustyle/rinforowlist_style.h"

class titleBtn: public QWidget
{
    Q_OBJECT
public slots:
    void changeTitle();

public:
    titleBtn(QWidget*  parent = 0);
    void setupUI();

    void paintEvent(QPaintEvent*);
    int  m_nChoosed;

    int  m_nH;
    int  m_nW;

    QHBoxLayout* m_btnLayout;
    QVector<QPushButton*> m_nTitle;
};

using namespace menu_res;

class CPeakSearchwnd_style : public uiWnd
{
    Q_OBJECT

public:
    explicit CPeakSearchwnd_style(QWidget *parent = 0);

public:
    void upDatePeakRes(QString servName);
    void setupUI(QWidget* parent);

    QVector<QString> m_freqRes;
    QVector<QString> m_ampRes;
    int m_nPeakNum;

private:

    void delAllItem(void);
    void setChooseRow(int row);

    void setChoosed(bool b);

signals:
    void delItemSig();
    void raiseValueBox();

protected:
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString&);

private:
    static menu_res::RInfoRowList_Style _style;
    static titleBtn* titleWnd;

private:

    int  m_nBoxX;
    int  m_nBoxY;
    int  m_nBoxDefaultY;
    int  m_nBoxW;
    int  m_nBoxH;
    int  m_nBoxDefaultH;

    int  m_nRowH;

    bool isActive;
    int  mChooseRow;

    int  mRowNum;
};

#endif // CPEAKSEARCHWND_STYLE_H
