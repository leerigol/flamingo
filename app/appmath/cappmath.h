#ifndef CAPPMATH_H
#define CAPPMATH_H

#include <QtCore>
#include <QtWidgets>

#include "../../menu/rmenus.h"  //! controlers

#include "../capp.h"

#include "../appch/cappch_style.h"

#include "mathscalewnd.h"
#include "cpeakmarker.h"
#include "mathgrade.h"
#include "cpeaksearchwnd.h"
#include "cthresline.h"

class CAppMath : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppMath( int mathId );

public:
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();

    virtual void buildConnection();
    virtual void resize();
    virtual void registerSpy();

protected Q_SLOTS:
    void on_gnd_click();
    void on_enter_active();
    void onMove( int /*xDist*/, int yDist );

protected:
    int  onZoom(AppEventCause);
    void on_scale_x_changed();
    void on_logic_scale_x_changed();
    void on_fft_scale_x_changed();
    void on_offset_x_changed();
    void on_oper_x_changed();
    void on_en_x_changed();
    void on_screen_changed();
    void on_input_valid_change();

    void on_en_label();
    void on_peak_update();
    void on_peak_thres_change();
    void on_logic_thres1_change();
    void on_logic_thres2_change();
    void on_logic_thres3_change();
    void on_logic_thres4_change();
    int  on_thres_pos(Chan ch);
    void on_peak_enable();
    void on_math_grade();
    void on_math_grade_onoff();
private:
    int mId;            //! math 1,2,3,4

    menu_res::RDsoVGnd *pGnd;
    menu_res::RDsoVGnd *pGndZoom;

    menu_res::RDsoCHLabel *m_pLabel;
    menu_res::RDsoCHLabel *m_pLabelZoom;


    static MathScaleWnd *m_pScaleWnd;

    CPeakMarker*       m_pPeakMarker;
    CPeakSearchWnd*    m_pPeakResWnd;
    CMathGradeWnd*     m_pMathGradeWnd;

    static CThresLine* m_pPeakThresLine;
    static CThresLine* m_pLogicThres1Line;
    static CThresLine* m_pLogicThres2Line;
    static CThresLine* m_pLogicThres3Line;
    static CThresLine* m_pLogicThres4Line;

    CAppCH_Style _style;
};

#endif // CAPPMATH_H
