#ifndef MATHSCALEWND_H
#define MATHSCALEWND_H

#include <QtWidgets>

#include "../../include/dsotype.h"

#include "../../menu/arch/rdsoui.h"
#include <QScopedPointer>

#include "mathscalewnd_style.h"

/*!
 * \brief The MathScaleWnd class
 * 模拟数学运算档位窗口
 * - 模拟数学运算指的是加减乘除
 * - 函数运算
 */
using namespace DsoType;
class MathScaleFrame
{
public:
    MathScaleFrame();
    virtual ~MathScaleFrame();

    virtual void paint( QPaintEvent *event, QWidget *wnd);
    virtual void setText(int mathId);
    void setChan( int ch );
    void setOperator( const QString &str );
    void setVScale( float scale, Unit unit );
    void setColor( QColor color );
    void setSaRate( qlonglong saRate );
    void setInvalid(bool valid );

    virtual MathScaleFrame_Style& getStyle();
protected:
    int mChId;          //! 1,2,3,4
    QString mOperator;  //! math operator
    float mVScale;
    Unit  mUnit;        //! scale unit
    QColor mColor;
    bool mbValid;
    qlonglong mSaRate;
private:
    MathScaleFrame_Style _style;
};

/*!
 * \brief The LogicScaleWnd class
 * 逻辑数学运算档位窗口
 * - 逻辑运算没有档位数值
 * - 大/中/小
 */
class LogicScaleFrame : public MathScaleFrame
{
public:
    LogicScaleFrame();
    ~LogicScaleFrame();

    void setVScale( LaScale scale, Unit unit );
    virtual void setText(int mathId);
    virtual void paint(QPaintEvent *event, QWidget *wnd);

    virtual MathScaleFrame_Style& getStyle();
protected:
    MathScaleFrame_Style _style;
    LaScale mLScale;
};

/*!
 * \brief The FftScaleWnd class
 * fft运算档位
 */
class FftScaleFrame : public MathScaleFrame
{
public:
    FftScaleFrame();
    ~FftScaleFrame();

    virtual void paint(QPaintEvent *, QWidget *wnd);

    virtual void setText(int mathId);
    void setHCenter( float center );
    void setHScale( float scale );
    void setResolution( float res );
    virtual void setRect(int num);

    virtual MathScaleFrame_Style& getStyle();
private:
    FftScaleFrame_Style _style;
    float mHCenter;
    float mHScale;
    float mHResolution;
};

class MathScaleWnd: public QPushButton,
                    public menu_res::RDsoUI
{
    Q_OBJECT
public:
    MathScaleWnd( QWidget *parent = 0);

    void setScaleText(int mathId);
    void changeEn(int mathId);//en change or operator change
    virtual void setWidgetVisible(bool);
    virtual void refresh();
    virtual void paintEvent( QPaintEvent *event );
    void setInputValid(int mId, bool inputValid);
private:
    void resetRect();
    void paintBase();
    void resetHandler(MathScaleFrame *&pDst, MathScaleFrame* pSrc);
    QColor mColor;
    QRect initWndRect;
    QVector<MathScaleFrame*>* m_pMathFrame;
    int mathNum;
};

#endif // MATHSCALEWND_H
