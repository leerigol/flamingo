#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../service/servmath/servmath.h"

#include "../appmain/cappmain.h"

#include "cappmath.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppMath )
start_of_entry()
on_set_int_int ( MSG_HOR_ZOOM_ON,       &CAppMath::onZoom ),

on_set_void_void( MSG_MATH_VIEW_OFFSET, &CAppMath::on_offset_x_changed ),
on_set_void_void( MSG_MATH_FFT_OFFSET, &CAppMath::on_offset_x_changed ),
on_set_void_void( MSG_MATH_LOGIC_OFFSET, &CAppMath::on_offset_x_changed ),
on_set_void_void( MSG_MATH_S32FUNCSCALE, &CAppMath::on_scale_x_changed ),
on_set_void_void( servMath::cmd_trace_sa_change, &CAppMath::on_scale_x_changed ),  //更新界面采样率

on_set_void_void( MSG_MATH_FFT_SCALE, &CAppMath::on_fft_scale_x_changed ),
on_set_void_void( MSG_MATH_FFT_H_CENTER, &CAppMath::on_fft_scale_x_changed ),
on_set_void_void( MSG_MATH_FFT_H_SPAN, &CAppMath::on_fft_scale_x_changed ),
on_set_void_void( MSG_MATH_FFT_H_START, &CAppMath::on_fft_scale_x_changed ),
on_set_void_void( MSG_MATH_FFT_H_END, &CAppMath::on_fft_scale_x_changed ),

on_set_void_void( MSG_MATH_LOGIC_SCALE, &CAppMath::on_logic_scale_x_changed ),

on_set_void_void( MSG_MATH_S32ARITHA, &CAppMath::on_oper_x_changed ),
on_set_void_void( MSG_MATH_S32ARITHB, &CAppMath::on_oper_x_changed ),
on_set_void_void( MSG_MATH_S32LOGICA, &CAppMath::on_oper_x_changed ),
on_set_void_void( MSG_MATH_S32LOGICB, &CAppMath::on_oper_x_changed ),
on_set_void_void( MSG_MATH_S32FFTSRC, &CAppMath::on_screen_changed ),

on_set_void_void( MSG_MATH_S32MATHOPERATOR, &CAppMath::on_oper_x_changed ),
on_set_void_void( MSG_MATH_EN, &CAppMath::on_en_x_changed ),

on_set_void_void( MSG_MATH_S32LABELEDIOR, &CAppMath::on_en_label ),
on_set_void_void( MSG_MATH_S32SHOWLABEL, &CAppMath::on_en_label ),

on_set_void_void( MSG_MATH_FFT_PEAK_THRESHOLD, &CAppMath::on_peak_thres_change ),
on_set_void_void( MSG_MATH_FFT_PEAK_ENABLE, &CAppMath::on_peak_enable ),

on_set_void_void( MSG_MATH_COLOR_ONOFF, &CAppMath::on_math_grade_onoff ),

on_set_void_void( servMath::cmd_ch1_threshold_change, &CAppMath::on_logic_thres1_change ),
on_set_void_void( servMath::cmd_ch2_threshold_change, &CAppMath::on_logic_thres2_change ),
on_set_void_void( servMath::cmd_ch3_threshold_change, &CAppMath::on_logic_thres3_change ),
on_set_void_void( servMath::cmd_ch4_threshold_change, &CAppMath::on_logic_thres4_change ),

on_set_void_void( servMath::cmd_q_real_hresolution, &CAppMath::on_fft_scale_x_changed ),
on_set_void_void( servMath::cmd_q_fft_peak,         &CAppMath::on_peak_update),
on_set_void_void( servMath::cmd_q_input_valid,      &CAppMath::on_input_valid_change),

on_set_void_void( servMath::cmd_math_grade_update,  &CAppMath::on_math_grade ),
on_set_void_void( CMD_SERVICE_ENTER_ACTIVE,         &CAppMath::on_enter_active ),
end_of_entry()

//! static objects
MathScaleWnd* CAppMath::m_pScaleWnd = 0;
CThresLine* CAppMath::m_pPeakThresLine = NULL;
CThresLine* CAppMath::m_pLogicThres1Line = NULL;
CThresLine* CAppMath::m_pLogicThres2Line = NULL;
CThresLine* CAppMath::m_pLogicThres3Line = NULL;
CThresLine* CAppMath::m_pLogicThres4Line = NULL;
//QList<menu_res::RDsoGnd *> CAppMath::mGndList;

#ifdef _DEBUG_MAP_TIME
     #define log_time()   qDebug()<<__FUNCTION__<<__LINE__<<getDebugTime()
#else
     #define log_time()   QT_NO_QDEBUG_MACRO()
#endif
CAppMath::CAppMath( int mathId ) : CApp()
{
    pGnd = NULL;
    pGndZoom = NULL;

    mId = mathId;
    mservName = QString("math%1").arg(mId);

    m_pPeakMarker = NULL;
    m_pPeakResWnd = NULL;
    m_pMathGradeWnd = NULL;
}

void CAppMath::setupUi( CAppMain *pMain )
{

    QString str;

    Q_ASSERT( NULL != pMain );

    //! create gnd
    str = QString("%1").arg(mId);
    pGnd = new menu_res::RDsoVGnd( str );
    Q_ASSERT( pGnd != NULL );
    pGnd->setColor( Math_color );

    pGndZoom = new menu_res::RDsoVGnd( str );
    Q_ASSERT( pGndZoom != NULL );
    pGndZoom->setColor( Math_color );

    //! link to parent
    pGnd->setParent( pMain->getLeftBar() );
    pGndZoom->setParent( pMain->getLeftBar() );

    //! same as ch style
    _style.loadResource( "ui/ch1/");

    //! main gnd
    pGnd->setView( _style.mSize.width(), _style.mSize.height() );
    pGnd->setViewGnd( _style.mSize.height() / 2 -1);

    //! zoom gnd
    pGndZoom->setView( _style.mSize.width(), _style.mSize.height() );
    pGndZoom->setViewGnd( _style.mSize.height() / 2 -1);

    //! attach
    sysAttachUIView( pGnd,     view_fft_main );
    sysAttachUIView( pGndZoom, view_fft_zoom );

    menu_res::RDsoGnd::mGndList.append(pGnd);
    menu_res::RDsoGnd::mGndList.append(pGndZoom);

    //! scale gnd

    if(m_pScaleWnd == NULL)
    {
        m_pScaleWnd = new MathScaleWnd(pMain->getCentBar());
        Q_ASSERT( NULL != m_pScaleWnd);
        m_pScaleWnd->hide();
        m_pScaleWnd->setView(_style.mSize.width(), _style.mSize.height());
        sysAttachUIView( m_pScaleWnd, view_yt_main );
    }

    //! peak Search
    m_pPeakMarker = new CPeakMarker(pMain->getCentBar());
    Q_ASSERT( NULL != m_pPeakMarker );
    m_pPeakMarker->setupUI();
    m_pPeakMarker->hide();

    m_pPeakResWnd =  CPeakSearchWnd::GetInstance();
    Q_ASSERT( NULL != m_pPeakResWnd);
    m_pPeakResWnd->hide();

    m_pLabel = new menu_res::RDsoCHLabel();
    Q_ASSERT( NULL != m_pLabel);
    m_pLabel->setColor(Math_color);
    m_pLabel->setParent(pMain->getCentBar());
    m_pLabel->setView( 23, 480 );
    m_pLabel->setViewGnd( 480 / 2 );
    m_pLabel->hide();

    m_pLabelZoom = new menu_res::RDsoCHLabel();
    Q_ASSERT( NULL != m_pLabelZoom);
    m_pLabelZoom->setColor(Math_color);
    m_pLabelZoom->setParent(pMain->getCentBar());
    m_pLabelZoom->setView( 23, 480 );
    m_pLabelZoom->setViewGnd( 480 / 2 );
    m_pLabelZoom->hide();

    //! static objects
    if(m_pLogicThres1Line == NULL)
    {
        m_pPeakThresLine = new CThresLine(pMain->getCentBar());
        Q_ASSERT( NULL != m_pPeakThresLine);
        m_pPeakThresLine->setZoomLine( new CThresLine(pMain->getCentBar()) );

        m_pPeakThresLine->setColor(Math_color);
        m_pPeakThresLine->setupUi();
        m_pPeakThresLine->hide();

        m_pLogicThres1Line = new CThresLine(pMain->getCentBar());
        Q_ASSERT( NULL != m_pLogicThres1Line);
        m_pLogicThres1Line->setZoomLine( new CThresLine(pMain->getCentBar()) );

        m_pLogicThres1Line->setColor(CH1_color);
        m_pLogicThres1Line->setupUi();
        m_pLogicThres1Line->hide();

        m_pLogicThres2Line = new CThresLine(pMain->getCentBar());
        Q_ASSERT( NULL != m_pLogicThres2Line);
        m_pLogicThres2Line->setZoomLine( new CThresLine(pMain->getCentBar()) );

        m_pLogicThres2Line->setColor(CH2_color);
        m_pLogicThres2Line->setupUi();
        m_pLogicThres2Line->hide();

        m_pLogicThres3Line = new CThresLine(pMain->getCentBar());
        Q_ASSERT( NULL != m_pLogicThres3Line);
        m_pLogicThres3Line->setZoomLine( new CThresLine(pMain->getCentBar()) );

        m_pLogicThres3Line->setColor(CH3_color);
        m_pLogicThres3Line->setupUi();
        m_pLogicThres3Line->hide();

        m_pLogicThres4Line = new CThresLine(pMain->getCentBar());
        Q_ASSERT( NULL != m_pLogicThres4Line);
        m_pLogicThres4Line->setZoomLine( new CThresLine(pMain->getCentBar()) );
        m_pLogicThres4Line->setColor(CH4_color);
        m_pLogicThres4Line->setupUi();
        m_pLogicThres4Line->hide();
    }

    m_pMathGradeWnd = new CMathGradeWnd(pMain->getCentBar());    
    Q_ASSERT( NULL != m_pMathGradeWnd );
    m_pMathGradeWnd->hide();

    //! attach view
    sysAttachUIView( m_pLabel,     view_fft_main );
    sysAttachUIView( m_pLabelZoom, view_fft_zoom );
}

void CAppMath::retranslateUi()
{

}

void CAppMath::buildConnection()
{
    Q_ASSERT( NULL != pGnd );
    connect( pGnd,SIGNAL(clicked(bool)), this, SLOT(on_gnd_click()) );
    Q_ASSERT( NULL != pGndZoom );
    connect( pGndZoom,SIGNAL(clicked(bool)), this, SLOT(on_gnd_click()) );

    //! drag drop
    connect( pGnd, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );
    connect( pGndZoom, SIGNAL(sigMove(int,int)),
             this, SLOT(onMove(int,int)) );

//    Q_ASSERT( NULL != m_pScaleWnd );
//    connect( m_pScaleWnd,SIGNAL(clicked(bool)), this, SLOT(on_gnd_click()) );
//    Q_ASSERT( NULL != m_pLogicScaleWnd );
//    connect( m_pLogicScaleWnd,SIGNAL(clicked(bool)), this, SLOT(on_gnd_click()) );
//    Q_ASSERT( NULL != m_pFftScaleWnd );
//    connect( m_pFftScaleWnd,SIGNAL(clicked(bool)), this, SLOT(on_gnd_click()) );
}

void CAppMath::resize()
{
}

void CAppMath::registerSpy()
{
    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON);
}

void CAppMath::on_gnd_click()
{
    serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppMath::on_enter_active()
{
    bool en;
    query( MSG_MATH_EN, en );
    if(en)
    {
        pGnd->setAllInactive(menu_res::RDsoGnd::mGndList);
        pGnd->setActive();
        pGndZoom->setActive();

        m_pLabel->setActive();
        m_pLabelZoom->setActive();
    }
}

void CAppMath::onMove( int /*xDist*/, int yDist )
{
    //! offset
    qlonglong offset;
    serviceExecutor::query( mservName,
                            MSG_MATH_VIEW_OFFSET,
                            offset );

    //! scale now
    qlonglong scale;
    query( servMath::cmd_q_view_scale, scale );

    qlonglong offs;
    offs = offset - scale * yDist / scr_vdiv_dots;

    serviceExecutor::post( mservName, MSG_MATH_VIEW_OFFSET, offs );
}

int CAppMath::onZoom(AppEventCause)
{
    log_time()<<"/*****math map begin******/";
    bool bZoom = false;
    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON, bZoom );

    CThresLine::setOnZoom(bZoom);
    m_pPeakThresLine->hideGND();
    m_pLogicThres1Line->hideGND();
    m_pLogicThres2Line->hideGND();
    m_pLogicThres3Line->hideGND();
    m_pLogicThres4Line->hideGND();

    return ERR_NONE;
}

void CAppMath::on_scale_x_changed()
{
    log_time();
    m_pScaleWnd->setScaleText(mId);
}

void CAppMath::on_logic_scale_x_changed()
{
    log_time();
    m_pScaleWnd->setScaleText(mId);
}

void CAppMath::on_fft_scale_x_changed()
{
    log_time();
    m_pScaleWnd->setScaleText(mId);
}

void CAppMath::on_offset_x_changed()
{
    log_time();
    qlonglong offset;
    qlonglong pixOffset;

    serviceExecutor::query( mservName, servMath::cmd_q_adc_offset, offset );

    pixOffset = offset * _style.mSize.height() / 8 / adc_vdiv_dots;

    //! add_by_zx : math的gnd offset 有可能超过int的表示范围，所以提前做检查
    if(pixOffset < -256)
    {
        pixOffset = -256;
    }
    else if(pixOffset >256)
    {
        pixOffset = 256;
    }

    pGnd->offset( pixOffset );
    pGndZoom->offset( pixOffset );
    m_pLabel->offset( pixOffset );
    m_pLabelZoom->offset( pixOffset );
}

void CAppMath::on_oper_x_changed()
{
    log_time();
    on_en_x_changed();
}

///
/// \brief for bug1317 by hxh
///
void CAppMath::on_screen_changed()
{
//    bool full = false;
//    query( MSG_MATH_S32FFTSCR, full );


//    if( pGnd )
//    {
//        pGnd->setShow( full );
//        pGndZoom->setShow( !full );
//        m_pLabel->setShow(!full);
//    }
}

void CAppMath::on_en_x_changed()
{
    log_time();
    bool en;

    query( MSG_MATH_EN, en );

    pGnd->setShow( en );
    pGndZoom->setShow( en );

    m_pScaleWnd->changeEn(mId);
    if(en)
    {
#if 0
       m_pPeakMarker->show();
#endif
        bool peakEn;
        query( MSG_MATH_FFT_PEAK_ENABLE, peakEn);

        int mathOperator;
        query(MSG_MATH_S32MATHOPERATOR,mathOperator );
        if( peakEn && mathOperator == operator_fft)
        {
            m_pPeakMarker->show();
            m_pPeakResWnd->setShow(mservName);
        }
        else
        {
             m_pPeakMarker->hide();
             m_pPeakResWnd->setHide(mservName);
        }

        if(m_pScaleWnd->isHidden())
        {
            m_pScaleWnd->show();
        }
        else
        {
            //for bug1630 by hxh
            m_pScaleWnd->update();
        }

        pGnd->setAllInactive(menu_res::RDsoGnd::mGndList);
        pGnd->setActive();
        pGndZoom->setActive();

        m_pLabel->setActive();
        m_pLabelZoom->setActive();
    }
    else
    {
#if 1
       m_pPeakMarker->hide();
#endif
        m_pPeakResWnd->setHide(mservName);
    }

    //qDebug() << "Math:" << m_pScaleWnd->geometry();
}

void CAppMath::on_input_valid_change()
{
    bool valid;
    query(servMath::cmd_q_input_valid, valid);
    if( m_pScaleWnd )
    {
        m_pScaleWnd->setInputValid(mId, valid);
    }
}

void CAppMath::on_en_label()
{
    bool bLabelEn;
    query(MSG_MATH_S32SHOWLABEL,bLabelEn);

    bool bMathEn;
    query(MSG_MATH_EN, bMathEn);

    bool bVisible = bMathEn && bLabelEn;
    QString labelString;
    query(MSG_MATH_S32LABELEDIOR,labelString);
    m_pLabel->setShow(bVisible);
    m_pLabel->setText(labelString);

    m_pLabelZoom->setShow(bVisible);
    m_pLabelZoom->setText(labelString);
    on_offset_x_changed();
}

void CAppMath::on_peak_update()
{
    bool peakEnable;
    bool mathEn;
    int mathOperator;
   // query(MSG_MATH_S32MATHOPERATOR,mathOperator);

    //test use
    serviceExecutor::query(mservName,MSG_MATH_EN,mathEn);
    serviceExecutor::query(mservName,MSG_MATH_FFT_PEAK_ENABLE,peakEnable);

    query(MSG_MATH_S32MATHOPERATOR,mathOperator);


    if(peakEnable && mathEn && mathOperator == operator_fft)
    {

//bug 1561 zy
#if 1
        if(m_pPeakResWnd->isHidden())
        {
            m_pPeakResWnd->setShow(mservName);
        }
        else
        {
             m_pPeakResWnd->upDatePeakRes(mservName);
        }
        m_pPeakMarker->toPoint(mservName);


        if(m_pPeakMarker->isHidden())
        {
            m_pPeakMarker->show();
        }
#else
         m_pPeakResWnd->upDatePeakRes(mservName);
         m_pPeakResWnd->setShow(mservName);

         m_pPeakMarker->toPoint(mservName);
         if(m_pPeakMarker->isHidden())
         {
            m_pPeakMarker->show();
         }
#endif
    }
}

void CAppMath::on_peak_thres_change()
{
    float peakThres;
    float offSet;
    float scale;
    query(servMath::cmd_q_fft_real_peakThres, peakThres);
    query(servMath::cmd_q_real_offset, offSet);
    query(servMath::cmd_q_real_scale, scale);
    int pixOffset = (peakThres + offSet)/scale*60;
    int pos = 240 - pixOffset;
    LOG_DBG();

    bool fftScr;
    query(MSG_MATH_S32FFTSCR, fftScr);
    if((int)fftScr == (int)fft_screen_half)
    {
        m_pPeakThresLine->setOnlyZoomPos(pos);
    }
    else
    {
        m_pPeakThresLine->setPos(pos);
    }
}

void CAppMath::on_logic_thres1_change()
{
    LOG_DBG();
    int pos;
    pos = on_thres_pos(chan1);
    if(pos != m_pLogicThres1Line->getPos())
    {
        LOG_DBG();
        m_pLogicThres1Line->setPos(pos);
    }
}

void CAppMath::on_logic_thres2_change()
{
    int pos;
    pos = on_thres_pos(chan2);
    if(pos != m_pLogicThres2Line->getPos())
    {
        m_pLogicThres2Line->setPos(pos);
    }
}

void CAppMath::on_logic_thres3_change()
{
    int pos;
    pos = on_thres_pos(chan3);
    if(pos != m_pLogicThres3Line->getPos())
    {
        m_pLogicThres3Line->setPos(pos);
    }
}

void CAppMath::on_logic_thres4_change()
{
    int pos;
    pos = on_thres_pos(chan4);
    if(pos != m_pLogicThres4Line->getPos())
    {
        m_pLogicThres4Line->setPos(pos);
    }
}

int CAppMath::on_thres_pos(Chan ch)
{
    qlonglong S32threshold;
    float fRatio;
    dsoVert::getCH( ch )->getProbe().toReal( fRatio, 1.0f );
    int cmd = MSG_MATH_S32LOGIC1THRE + ch - chan1;
    serviceExecutor::query(getServiceName(), cmd, S32threshold);
    float f32Thres = S32threshold * math_threshold_base * fRatio;

    QString servName = "chan"+QString::number(ch-chan1+1);
    float scale;
    serviceExecutor::query(servName,servCH::cmd_scale_real,scale);
    float offset;
    serviceExecutor::query(servName,servCH::cmd_offset_real,offset);

    int pixOffset;
    pixOffset = (f32Thres + offset) / scale * 60;
    pixOffset = (pixOffset <= 239) ? pixOffset:239;
    pixOffset = (pixOffset >= -240) ? pixOffset:-240;
    return (240 - pixOffset);
}

void CAppMath::on_peak_enable()
{
    log_time()<<"/*****math map end******/";
    bool peakEn;
    serviceExecutor::query(mservName,MSG_MATH_FFT_PEAK_ENABLE,peakEn);
    bool mathEn;
    serviceExecutor::query(mservName,MSG_MATH_EN,mathEn);

    int mathOperator;
    query(MSG_MATH_S32MATHOPERATOR,mathOperator);
   // qDebug() << "mathOperator = " << mathOperator;

    if(mathEn && peakEn && mathOperator == operator_fft)
    {
        m_pPeakMarker->show();
        m_pPeakResWnd->setShow(mservName);
    }
    else
    {
        m_pPeakMarker->hide();
        m_pPeakResWnd->setHide(mservName);
    }
}

void CAppMath::on_math_grade()
{    
    void* pCurve;
    query(servMath::cmd_math_grade_update, pCurve);
    m_pMathGradeWnd->setMathGrade( pCurve );
}

void CAppMath::on_math_grade_onoff()
{
    bool bColorOnff = false;
    query(MSG_MATH_COLOR_ONOFF, bColorOnff);
    if(bColorOnff)
    {
        m_pMathGradeWnd->show();
    }
    else
    {
        m_pMathGradeWnd->hide();
    }
}
