#ifndef CTHRESLINE_H
#define CTHRESLINE_H

#include <QWidget>
#include "../../menu/rmenus.h"

using namespace menu_res;
class CThresLine: public QWidget
{
    Q_OBJECT
public:
    explicit CThresLine(QWidget *parent = 0);
    ~CThresLine();

public:
    void setPos( int pos );
    void setOnlyZoomPos( int pos );
    int  getPos();

    void setColor( QColor color );
    void setRange(int start, int range);
    void setupUi(void);
    void setGndParent(QWidget *parent);
    void hideGND();
    void setZoomLine(CThresLine *p);

public slots:

    void onTimeOut();
protected:
    void paintEvent( QPaintEvent *event );

private:
    int       m_nStart; /* More than one GRID. level drawing start with*/
    int       m_nRange; /* Save as up. level drawing range */
    QColor    m_nColor;
    int       m_nPos;


    QTimer    m_Timer;
    RDsoVGnd* m_pLine;

    CThresLine *pZoomLine;

private:
    static bool m_onZoom;

public:
    static void setOnZoom(bool b);
};

#endif // CTHRESLINE_H
