#include "../../include/dsodbg.h"
#include "cpeaksearchwnd_style.h"
#include "../../service/servmath/servmath.h"
#include "../../gui/cguiformatter.h"

titleBtn::titleBtn(QWidget* parent) : QWidget(parent)
{
    m_nChoosed = 0;
    m_nH = 35;
    m_nW = 50;
    m_btnLayout = new QHBoxLayout;
    m_nTitle.resize(4);
    for(int i = 0;i < 4;i++)
    {
        m_nTitle[i] = new QPushButton(this);
        Q_ASSERT(m_nTitle[i] != NULL);
    }
}

void titleBtn::changeTitle()
{
    QPushButton* btn = qobject_cast<QPushButton *>(sender());
    QString tag = QString(btn->text().at(3));
    int choose = tag.toInt();

    //change the color of choosed before
    m_nTitle[m_nChoosed]->setStyleSheet("background-color:math_color;color: rgb(255, 255, 255);");

    m_nTitle[choose]->setStyleSheet("background-color:transparent;color: rgb(255, 255, 255);");
    m_nChoosed = choose;
    btn->setText("FFT"+QString::number(m_nChoosed));
}

void titleBtn::setupUI()
{
    for(int i = 0;i < 4;i++)
    {
        m_nTitle[i]->setText("FFT"+QString::number(i));
        m_nTitle[i]->setStyleSheet("background-color:math_color;color: rgb(255, 255, 255);");
        m_nTitle[i]->setStyleSheet("background-color:transparent;color: transparent;");
        m_nTitle[i]->setMaximumSize(m_nW,m_nH);
        m_btnLayout->addWidget(m_nTitle[i]);
        connect(m_nTitle[i],SIGNAL(clicked()),this,SLOT(changeTitle()));
    }
    setLayout(m_btnLayout);
}

void titleBtn::paintEvent(QPaintEvent*)
{

}

menu_res::RInfoRowList_Style CPeakSearchwnd_style::_style;
titleBtn* CPeakSearchwnd_style::titleWnd;

CPeakSearchwnd_style::CPeakSearchwnd_style(QWidget *parent) : uiWnd(parent)
{
    m_nPeakNum = 0;
    mChooseRow = 0;
    isActive = false;
    mRowNum = 0;

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void CPeakSearchwnd_style::upDatePeakRes(QString servName)
{
    m_freqRes.reserve(20);
    m_ampRes.reserve(20);
    if(!m_freqRes.isEmpty())
    {
        m_freqRes.clear();
        m_ampRes.clear();
    }

    qlonglong freqStart = 0;
    serviceExecutor::query(servName,MSG_MATH_FFT_H_START, freqStart);
    qlonglong freqEnd = 0;
    serviceExecutor::query(servName,MSG_MATH_FFT_H_END, freqEnd);
    qDebug()<<__FUNCTION__<<"freqEnd"<<freqEnd;
    float ampScale = 0;
    serviceExecutor::query(servName, servMath::cmd_q_real_scale, ampScale);
    qDebug()<<__FUNCTION__<<"ampScale"<<ampScale;
    float ampOffset = 0;
    serviceExecutor::query(servName,servMath::cmd_q_real_offset, ampOffset);

    void* p = NULL;
    serviceExecutor::query(servName,servMath::cmd_q_fft_peak,p);

    PeakBuffer* peakBuffer = static_cast <PeakBuffer* > (p);
    peakBuffer->peakDataMutex.lock();

    LOG_DBG();
    QVector<measPeakStru>* pPeakData = &(peakBuffer->pointBuffer);
    if(p == NULL || pPeakData->isEmpty())
    {
        LOG_DBG();
        peakBuffer->peakDataMutex.unlock();
        return;
    }
    LOG_DBG();
    int xPoint;
    QString freq;
    QString ampl;
    for(int i = 0;i < pPeakData->size();i++)
    {
        xPoint = (pPeakData->at(i).freq - freqStart)/(freqEnd - freqStart)*1000;

        if(xPoint < 1000 && xPoint > 0)
        {
            gui_fmt::CGuiFormatter::format(freq,pPeakData->at(i).freq*math_fft_hz_base,
                                           fmt_def,Unit_hz);
            //gui_fmt::CGuiFormatter::format(ampl,pPeakData->at(i).ampl/2,fmt_def,Unit_db);
            gui_fmt::CGuiFormatter::format(ampl,pPeakData->at(i).ampl,fmt_def,Unit_db);
            m_freqRes.push_back(freq);
            m_ampRes.push_back(ampl);
        }
    }
    LOG_DBG();
    peakBuffer->peakDataMutex.unlock();
    LOG_DBG();

    //!calcu the height of wnd
    int rowNum  = m_freqRes.size();
    m_nBoxH = m_nBoxDefaultH + m_nRowH * rowNum;
    this->resize(m_nBoxW, m_nBoxH + titleWnd->m_nH);

    if(mRowNum != rowNum){
        int tempH = geometry().y()+(mRowNum-rowNum)*m_nRowH;
        mRowNum = rowNum;
        if(tempH < 0)
            tempH = 0;
        if(tempH > 450)
            tempH = 450;
        move(geometry().x(), tempH);
    }
}

void CPeakSearchwnd_style::setupUI(QWidget *parent)
{
    loadResource("measure/stat_values/");
    _style.loadResource("rframe/");
    _style.setColumnsNum(3);
    _style.setHeightPerRow(m_nRowH);
    QList<int> widthList;
    widthList<<30<<100<<100;
    _style.setColumnsWidth(widthList);
    _style.setChoosedRow(-1);
    setParent(parent);

    titleWnd = new titleBtn;
    //titleWnd->setParent(this);
    //titleWnd->setupUI();
}

void CPeakSearchwnd_style::delAllItem(void)
{
    this->hide();

    mChooseRow = 0;
    m_nBoxY = m_nBoxDefaultY;
    m_nBoxH = m_nBoxDefaultH;
}

void CPeakSearchwnd_style::setChooseRow(int row)
{
    isActive = true;
    mChooseRow = row;
    update();
}

void CPeakSearchwnd_style::setChoosed(bool b)
{
    isActive = b;
    update();
}

void CPeakSearchwnd_style::loadResource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path ;

    meta.getMetaVal(str + "row_h", m_nRowH);//16

    meta.getMetaVal(str + "x", m_nBoxX);//170
    meta.getMetaVal(str + "y", m_nBoxY);//464
    //meta.getMetaVal(str + "w", m_nBoxW);//630
    m_nBoxW = 230;
    meta.getMetaVal(str + "h", m_nBoxH);//30

    m_nBoxDefaultY = m_nBoxY;
    m_nBoxDefaultH = m_nBoxH;
    this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);
}

void CPeakSearchwnd_style::paintEvent(QPaintEvent */*event*/)
{
    //Q_UNUSED(event);
    QPainter painter(this);
    QFont font;
    font.setPointSize(10);
    painter.setFont(font);

    //QRect r(20,0,4*titleWnd->m_nW,titleWnd->m_nH);
    QRect r(1000,0,0,0);
    //draw title
    //titleWnd->setMaximumHeight(titleWnd->m_nH);
    //titleWnd->setGeometry(r);
    titleWnd->hide();

    int nWidth = this->geometry().width();
    r = QRect(0,titleWnd->m_nH,nWidth,m_nBoxH);
    _style.setChoosedRow(-1);
    _style.paintEvent(painter,r);

    r.setX(30);
    r.setY(r.y()+5);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    QString  temp("%1"),val;
    QPen     pen;

    pen.setColor(QColor(200,200,200,255));
    painter.setPen(pen);

    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, " ");

    r.setX(r.x()+30);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Freq");

    r.setX(r.x()+100);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Amp");

    int nStartX = 8;//in central
    r.setY(r.y() + m_nRowH + 5);
    r.setX(nStartX);
    for(int i=0; i<m_freqRes.count(); i++)
    {
        //! set color with the different channel

        QRect rName = r;
        rName.setHeight(m_nRowH);
        pen.setColor(Qt::white);
        painter.setPen(pen);

        QString countTemp = QString::number(i+1);

        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, countTemp);

        r.setX(r.x()+30);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_freqRes.at(i));

        r.setX(r.x()+100);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_ampRes.at(i));

        r.setY(r.y() + m_nRowH);
        r.setX(nStartX);
    }
}
