#include "mathgrade.h"
#include "../../service/servmath/math_grade/math_grade.h"
#include <QPainter>
#include "../../include/dsodbg.h"


CMathGradeWnd::CMathGradeWnd(QWidget *parent) : QWidget(parent)
{
    m_colorForIds.reserve(7);

    m_colorForIds<<QColor(0,0,48)<<QColor(0,100,200)<<QColor(0,204,0)<<QColor(248,252,0)
                <<QColor(248,152,0)<<QColor(248,0,0)<<QColor(248,252,248);
    setGeometry(0,0,1000,480);
    m_pCurveRes = NULL;
}

void CMathGradeWnd::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    CMathGrade::m_cacheLock.lock();

    QVector<QVector<QPoint> >* temp  = (QVector<QVector<QPoint> >*) m_pCurveRes;
    if(m_pCurveRes != NULL)
    {
        for(int i = 0;i < 7;i++)
        {
            painter.setPen(m_colorForIds[i]);
            painter.drawPoints(temp->at(i));
        }
    }
    CMathGrade::m_cacheLock.unlock();
}

void CMathGradeWnd::setupUI()
{

}

void CMathGradeWnd::setMathGrade(void* p)
{
    LOG_DBG()<<"addr_p"<<p;
    Q_ASSERT(p != NULL);
    m_pCurveRes = p;
    update();
}

