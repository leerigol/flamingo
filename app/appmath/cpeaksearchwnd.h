#ifndef CPEAKSEARCHWND_H
#define CPEAKSEARCHWND_H

#include "../../menu/menustyle/rui_style.h"
#include "../../menu/menustyle/rinfownd_style.h"


class CPeakSearchwnd_style : public menu_res::RUI_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );
    void setColor( QColor &color, int hexRgb );

public:
    QRect   mGeo;
    int     mRowH;

    QString mBtnQss;
    QString mBtnQss2;

    QColor  mTitleColor;
    QColor  mGray15;
    QColor  mGray27;

    QString mTitle1;
    QString mTitle2;
};

using namespace menu_res;

class CPeakSearchWnd : public uiWnd
{
    Q_OBJECT

private:
    explicit CPeakSearchWnd(QWidget *parent = 0);
    static CPeakSearchWnd   *m_pInstance;

public:
    static CPeakSearchWnd * GetInstance();

    void upDatePeakRes(QString servName);
    void setupUI();
    void setShow(QString servName);
    void setHide(QString servName);
private:
    QVector<QString> m_freqRes;
    QVector<QString> m_ampRes;
    int m_nPeakNum;

public slots:
    void math1DownQss();
    void math2DownQss();
    void math3DownQss();
    void math4DownQss();

protected:
    void paintEvent(QPaintEvent *event);

private:
    int choosedCh;
    CPeakSearchwnd_style _style;
    static menu_res::RInfoWnd_Style _bgStyle;
    QPushButton *math1,*math2,*math3,*math4;

};

#endif // CPEAKSEARCHWND_H
