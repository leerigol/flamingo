#include "cpeakmarker.h"
#include "../../service/servmath/servmath.h"
#include "../../include/dsocfg.h"
CPeakMarker::CPeakMarker(QWidget *parent):QWidget(parent)
{
    resize(parent->size());
}

void CPeakMarker::setupUI()
{
    m_nW = 5;
    m_nH = 5;
    m_Color = Qt::white;
    m_widthPen = 1;
}

void CPeakMarker::toPoint(QString servName)
{
    bool halfOnOff;
    serviceExecutor::query(servName, MSG_MATH_S32FFTSCR, halfOnOff);
    bool zoomEn;
    serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);

    if( zoomEn )
    {
        m_markerDispType = main_and_zoom;
    }
    else if( !halfOnOff )
    {
        m_markerDispType = half_screen;
    }
    else
    {
        m_markerDispType = main_only;
    }

    qlonglong freqStart = 0;
    serviceExecutor::query(servName,MSG_MATH_FFT_H_START, freqStart);
    qlonglong freqEnd = 0;
    serviceExecutor::query(servName,MSG_MATH_FFT_H_END, freqEnd);
    float ampScale = 0;
    serviceExecutor::query(servName, servMath::cmd_q_real_scale, ampScale);
    float ampOffset = 0;
    serviceExecutor::query(servName, servMath::cmd_q_real_offset, ampOffset);

    void* p = NULL;
    serviceExecutor::query(servName, servMath::cmd_q_fft_peak,p);

    Q_ASSERT(p != NULL);
    PeakBuffer* peakBuffer = static_cast <PeakBuffer* > (p);
    QVector<measPeakStru>* pPeakData = &(peakBuffer->pointBuffer);

    peakBuffer->peakDataMutex.lock();
    if(pPeakData->isEmpty())
    {
        //for bug1629 by hxh
        m_markerMainPoint.clear();
        m_markerZoomPoint.clear();
        peakBuffer->peakDataMutex.unlock();
        update();
        return;
    }

    if(m_markerDispType != main_and_zoom)
    {
        setMainPoint(pPeakData, freqStart, freqEnd, ampScale, ampOffset);
        m_markerZoomPoint.clear();
    }
    else
    {
        setMainPoint(pPeakData, freqStart, freqEnd, ampScale, ampOffset);
        qlonglong zoomFreqStart = 0;
        serviceExecutor::query(servName,servMath::cmd_q_zoomfft_start, zoomFreqStart);
        qlonglong zoomFreqScale = 0;
        serviceExecutor::query(servName,servMath::cmd_q_zoomfft_scale, zoomFreqScale);
        setZoomPoint( pPeakData, zoomFreqStart, zoomFreqStart + zoomFreqScale * 10, ampScale, ampOffset);
    }
    peakBuffer->peakDataMutex.unlock();
    update();
}

void CPeakMarker::setMainPoint(QVector<measPeakStru>* pPeakData, qlonglong freqStart, qlonglong freqEnd,
                               float ampScale, float ampOffset)
{
    m_markerMainPoint.clear();
    m_markerMainPoint.reserve(pPeakData->size());
    float xPoint;
    float yPoint;
    int pix_vdiv_dots = 0;
    int pix_offset = 0;

    if(m_markerDispType == main_only)
    {
        pix_vdiv_dots = 60;
        pix_offset = 240;
    }
    else if(m_markerDispType == main_and_zoom)
    {
        pix_vdiv_dots = 18;
        pix_offset = 72;
    }
    else
    {
        pix_vdiv_dots = 40;
        pix_offset = 320;
    }
    for(int i = 0;i < pPeakData->size();i++)
    {
        xPoint = (pPeakData->at(i).freq - freqStart)/(freqEnd - freqStart) * 1000;
        yPoint = -(double)(pPeakData->at(i).ampl + ampOffset)/ampScale * pix_vdiv_dots + pix_offset;
        if( (0 < xPoint && xPoint < 999)
                && (yPoint < 479 && yPoint > 0))
        {
            m_markerMainPoint.push_back(QPointF(xPoint,yPoint));
        }
    }
}

void CPeakMarker::setZoomPoint(QVector<measPeakStru> *pPeakData, qlonglong freqStart, qlonglong freqEnd,
                               float ampScale, float ampOffset)
{
    m_markerZoomPoint.clear();
    m_markerZoomPoint.reserve(pPeakData->size());
    float xPoint;
    float yPoint;
    int pix_vdiv_dots = 40;
    int pix_offset = 320;

    if(freqStart == freqEnd)
    {
        return;
    }

    for(int i = 0;i < pPeakData->size();i++)
    {
        xPoint = (pPeakData->at(i).freq - freqStart)/(freqEnd - freqStart)*1000;
        yPoint = -(double)(pPeakData->at(i).ampl + ampOffset)/ampScale * pix_vdiv_dots + pix_offset;
        if( (0 < xPoint && xPoint < 999)
                && (yPoint < 479 && yPoint > 0))
        {
            m_markerZoomPoint.push_back(QPointF(xPoint,yPoint));
        }
    }
}

void CPeakMarker::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen elliPen(Qt::white);
    elliPen.setWidth(m_widthPen);
    painter.setPen(elliPen);

    if(m_markerMainPoint.isEmpty())
    {
        return;
    }

    for(int i = 0;i < m_markerMainPoint.size();i++)
    {
        int x = m_markerMainPoint.at(i).x();
        int y = m_markerMainPoint.at(i).y();
        QPoint points[4] = {
            QPoint(x - m_nW, y),
            QPoint(x , y - m_nH),
            QPoint(x + m_nW, y),
            QPoint(x , y + m_nH)
        };
        painter.drawPolygon(points,4);
    }

    if(m_markerDispType != main_and_zoom)
    {
        return;
    }

    if(m_markerZoomPoint.isEmpty())
    {
        return;
    }

    for(int i = 0;i < m_markerZoomPoint.size();i++)
    {
        int x = m_markerZoomPoint.at(i).x();
        int y = m_markerZoomPoint.at(i).y();
        QPoint points[4] = {
            QPoint(x - m_nW, y),
            QPoint(x , y - m_nH),
            QPoint(x + m_nW, y),
            QPoint(x , y + m_nH)
        };
        painter.drawPolygon(points,4);
    }
}
