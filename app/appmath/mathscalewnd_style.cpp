
#include "../../meta/crmeta.h"

#include "mathscalewnd_style.h"

void MathScaleFrame_Style::load(const QString &path,
                              const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "frame", mFrameRect );
    meta.getMetaVal( path + "source", mSourceRect );
    meta.getMetaVal( path + "operator", mOperatorRect );
    meta.getMetaVal( path + "scale", mScaleRect );
    meta.getMetaVal( path + "saRate", mSaRateRect);
    meta.getMetaVal( path + "warning", mWarningRect);
    top = mFrameRect.top();
}

int MathScaleFrame_Style::changeGeo(int num)
{
    int rectHeight = mFrameRect.height();
    mFrameRect.moveTop(top - num*rectHeight);
    mSourceRect.moveTop(num*rectHeight);
    mOperatorRect.moveTop(num*rectHeight);
    mScaleRect.moveTop(num*rectHeight);
    mSaRateRect.moveTop(num*rectHeight);
    mWarningRect.moveTop(num*rectHeight);
    return mFrameRect.width();
}

void FilterScaleFrame_Style::load(const QString &path, const r_meta::CMeta &meta)
{
    meta.getMetaVal( path + "frame", mFrameRect );
    meta.getMetaVal( path + "source", mSourceRect );
    meta.getMetaVal( path + "operator", mOperatorRect );
    meta.getMetaVal( path + "scale", mScaleRect );
    meta.getMetaVal( path + "saRate", mSaRateRect);

    top = mFrameRect.top();
}

int FilterScaleFrame_Style::changeGeo(int num)
{
    int rectHeight = mFrameRect.height();
    mFrameRect.moveTop(top - num*rectHeight);
    mSourceRect.moveTop(num*rectHeight);
    mOperatorRect.moveTop(num*rectHeight);
    mScaleRect.moveTop(num*rectHeight);
    mSaRateRect.moveTop(num*rectHeight);
    return mFrameRect.width();
}

void FftScaleFrame_Style::load(const QString &path,const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "frame", mFrameRect );
    meta.getMetaVal( path + "source", mSourceRect );
    meta.getMetaVal( path + "operator", mOperatorRect );
    meta.getMetaVal( path + "scale", mScaleRect );

    meta.getMetaVal( path + "hcenter", mHCenterRect );
    meta.getMetaVal( path + "hscale", mHScaleRect );
    meta.getMetaVal( path + "hresolution", mHResolutionRect );
    top = mFrameRect.top();
}

int FftScaleFrame_Style::changeGeo(int num)
{
    int rectHeight = mFrameRect.height();
    mFrameRect.moveTop(top - num*rectHeight);
    mSourceRect.moveTop(num*rectHeight);
    mOperatorRect.moveTop(num*rectHeight);
    mScaleRect.moveTop(num*rectHeight);

    mHCenterRect.moveTop(num*rectHeight);
    mHScaleRect.moveTop(num*rectHeight);
    mHResolutionRect.moveTop(num*rectHeight);
    return mFrameRect.width();
}
