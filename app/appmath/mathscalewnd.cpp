#include "../../service/servmath/servmath.h"
#include "../../include/dsocfg.h"
#include "../../gui/cguiformatter.h"

#include "mathscalewnd.h"
#include "../../service/servtrace/servtrace.h"

MathScaleFrame::MathScaleFrame()
{
    //mpStyle = new MathScaleFrame_Style;
    _style.loadResource("ui/wnd/mathscale/");

    //! init
    mChId = 1;
    mOperator = "CH1+CH2";

    mVScale = 0.5;
    mUnit = Unit_V;
    mColor = Math_color;
    mbValid = true;
}

MathScaleFrame::~MathScaleFrame()
{

}

void MathScaleFrame::paint(QPaintEvent */*event*/ , QWidget *wnd)
{
    QString str;
    QPainter painter(wnd);
    painter.setPen( QPen(mColor, 2) );

    //! source
    str = QString("%1").arg( mChId );

    painter.drawText( _style.mSourceRect, Qt::AlignCenter, str );
     //! operator
    painter.drawText( _style.mOperatorRect, Qt::AlignLeft | Qt::AlignVCenter, mOperator );

    if(!mbValid)
    {
        painter.drawText( _style.mWarningRect, Qt::AlignLeft | Qt::AlignVCenter,
                          //! temp test
                          sysGetString(-1, "Invalid Input") );
        return;
    }

    //! scale
    gui_fmt::CGuiFormatter::format( str, mVScale, fmt_def, mUnit );
    painter.drawText( _style.mScaleRect,
                      Qt::AlignLeft | Qt::AlignVCenter,
                      str );
    //! saRate
    gui_fmt::CGuiFormatter::format( str, mSaRate/1e6, fmt_def, Unit_SaS);
    painter.drawText( _style.mSaRateRect,
                      Qt::AlignLeft | Qt::AlignVCenter,
                      str);

}

void MathScaleFrame::setText(int mathId)
{
    mChId = mathId;

    QString str;
    QString mServName = QString("math%1").arg(mathId);
    serviceExecutor::query(mServName,servMath::cmd_q_operator_string, str );
    setOperator(str);

    float realScale;
    serviceExecutor::query(mServName, servMath::cmd_q_real_scale, realScale );
    int unit;
    serviceExecutor::query(mServName, servMath::cmd_math_unit, unit );
    setVScale( realScale, (Unit)unit );

    CArgument argI;
    argI.setVal((int) chan1,0);
    argI.setVal( horizontal_view_main,1 );
    CArgument argO;
    serviceExecutor::query(serv_name_trace, servTrace::cmd_query_trace_Sa, argI,argO);
    argO.getVal(mSaRate,0);

    setColor(Math_color);
}

void MathScaleFrame::setChan( int ch )
{
    mChId = ch;
}

void MathScaleFrame::setOperator( const QString &str )
{
    mOperator = str;
}

void MathScaleFrame::setVScale( float scale, Unit unit )
{
    mVScale = scale;
    mUnit = unit;
}

void MathScaleFrame::setColor( QColor color )
{
    mColor = color;
}

void MathScaleFrame::setSaRate(qlonglong saRate)
{
    mSaRate = saRate;
}

void MathScaleFrame::setInvalid(bool valid)
{
    mbValid = valid;
}

MathScaleFrame_Style &MathScaleFrame::getStyle()
{
    return _style;
}

LogicScaleFrame::LogicScaleFrame()
{
    mLScale = Small;
    _style.loadResource("ui/wnd/mathscale/");
}

LogicScaleFrame::~LogicScaleFrame()
{

}

void LogicScaleFrame::paint(QPaintEvent */*event*/ , QWidget *wnd)
{
    QString str;
    QPainter painter(wnd);
    painter.setPen( mColor );

    //! source
    str = QString("%1").arg( mChId );
    painter.drawText( _style.mSourceRect, Qt::AlignCenter, str );
    //! operator
    painter.drawText( _style.mOperatorRect, Qt::AlignLeft | Qt::AlignVCenter, mOperator );

    //! normal
    str = gui_fmt::CGuiFormatter::toString( mLScale );
    painter.drawText( _style.mScaleRect,
                      Qt::AlignLeft | Qt::AlignVCenter,
                      str );
}

MathScaleFrame_Style &LogicScaleFrame::getStyle()
{
    return _style;
}

void LogicScaleFrame::setVScale( LaScale scale, Unit unit )
{
    mLScale = scale;
    mUnit = unit;
}

void LogicScaleFrame::setText(int mathId)
{
    mChId = mathId;

    QString mServName = QString("math%1").arg(mathId);
    QString str;
    serviceExecutor::query(mServName,servMath::cmd_q_operator_string, str );
    setOperator(str);

    int lScale;
    int unit;
    serviceExecutor::query(mServName,MSG_MATH_LOGIC_SCALE, lScale );
    serviceExecutor::query(mServName,servMath::cmd_math_unit, unit );
    setVScale( (LaScale)lScale, (Unit)unit );

    setColor(Math_color);
}

FftScaleFrame::FftScaleFrame()
{
    _style.loadResource("ui/wnd/fftscale/");
    //! init
    mHCenter = 1.0e6f;
    mHScale = 500.0e3f;
    mHResolution = 100.0f;
}

FftScaleFrame::~FftScaleFrame()
{

}


void FftScaleFrame::paint(QPaintEvent */*event*/ , QWidget *wnd)
{
    QString str;
    QPainter painter(wnd);
    painter.setPen( mColor );

    //! source
    str = QString("%1").arg( mChId );
    painter.drawText( _style.mSourceRect, Qt::AlignCenter, str );
    //! operator
    painter.drawText( _style.mOperatorRect, Qt::AlignLeft | Qt::AlignVCenter, mOperator );
    //! scale

    if(!mbValid)
    {
        painter.drawText( _style.mWarningRect, Qt::AlignLeft | Qt::AlignVCenter,
                          //! temp test
                          sysGetString(-1, "Invalid Input") );
        return;
    }

    gui_fmt::CGuiFormatter::format( str, mVScale, fmt_def, mUnit );
    str.append("/Div");
    painter.drawText( _style.mScaleRect,
                      Qt::AlignLeft | Qt::AlignVCenter,
                      str );

    //! center
    QString strVal;
    gui_fmt::CGuiFormatter::format(   strVal,
                                      mHCenter,
                                      fmt_def,
                                      DsoType::Unit_hz );
    str = QString("Center:");
    str.append( strVal );
    painter.drawText( _style.mHCenterRect, Qt::AlignLeft | Qt::AlignVCenter, str );

    //! hscale
    gui_fmt::CGuiFormatter::format(   strVal,
                                      mHScale * 10,
                                      fmt_def,
                                      DsoType::Unit_hz );
    str = QString("Span:");
    str.append(strVal);
    painter.drawText( _style.mHScaleRect, Qt::AlignLeft | Qt::AlignCenter, str );

    //! Resolution
    gui_fmt::CGuiFormatter::format(   strVal,
                                      mHResolution,
                                      fmt_def,
                                      DsoType::Unit_hz );
    str = QString("RBW:");
    str.append( strVal );
    painter.drawText( _style.mHResolutionRect, Qt::AlignLeft | Qt::AlignCenter, str );
}

void FftScaleFrame::setText(int mathId)
{
    MathScaleFrame::setText(mathId);

    QString mServName = QString("math%1").arg(mathId);
    float val;
    serviceExecutor::query(mServName, servMath::cmd_q_real_hcenter, val );
    setHCenter( val );

    serviceExecutor::query(mServName, servMath::cmd_q_real_hscale, val );
    setHScale( val );

    serviceExecutor::query(mServName, servMath::cmd_q_real_hresolution, val );
    setResolution( val );
}

void FftScaleFrame::setHCenter( float center )
{
    mHCenter = center;
}

void FftScaleFrame::setHScale( float scale )
{
    mHScale = scale;
}

void FftScaleFrame::setResolution( float res )
{
    mHResolution = res;
}

void FftScaleFrame::setRect(int num)
{
    _style.changeGeo(num);
}

MathScaleFrame_Style &FftScaleFrame::getStyle()
{
    return _style;
}

MathScaleWnd::MathScaleWnd(QWidget *parent):QPushButton(parent)
{   
    mColor = Math_color;

    m_pMathFrame = new QVector<MathScaleFrame*>;
    m_pMathFrame->resize(4);
    for(int i = 0;i < 4;i++)
    {
        (*m_pMathFrame)[i] = NULL;
    }

    mathNum = 0;
    MathScaleFrame_Style style;
    style.loadResource("ui/wnd/mathscale/");
    initWndRect = style.mFrameRect;
    setGeometry(initWndRect);
}

void MathScaleWnd::setScaleText(int mathId)
{
    LOG_DBG();
    if(m_pMathFrame->at(mathId-1) != NULL)
    {
        m_pMathFrame->at(mathId-1)->setText(mathId);
    }
    else
    {
        LOG_DBG();
    }
    update();
}

void MathScaleWnd::changeEn(int mathId)
{
    bool en;
    QString mServName = QString("math%1").arg(mathId);
    serviceExecutor::query(mServName,MSG_MATH_EN,en);
    int zone;
    if(en)
    {
        serviceExecutor::query(mServName,servMath::cmd_q_zone, zone );

        if ( zone == math_analog_zone )
        {
            resetHandler((*m_pMathFrame)[mathId-1], new MathScaleFrame);
        }
        else if ( zone == math_logic_zone )
        {
            resetHandler((*m_pMathFrame)[mathId-1], new LogicScaleFrame);
        }
        else if ( zone == math_freq_zone )
        {
            resetHandler((*m_pMathFrame)[mathId-1], new FftScaleFrame);
        }
        else
        {
            Q_ASSERT(false);
        }
        m_pMathFrame->at(mathId-1)->setText(mathId);
    }
    else
    {
        if(m_pMathFrame->at(mathId-1) != NULL)
        {
            (*m_pMathFrame)[mathId-1] = NULL;
        }
    }
    resetRect();
}

void MathScaleWnd::setWidgetVisible(bool /*visible*/)
{

}

void MathScaleWnd::refresh()
{

}

void MathScaleWnd::paintEvent(QPaintEvent *event)
{
    paintBase();
    for(int i = 0;i < 4;i++)
    {
        if(m_pMathFrame->at(i) != NULL)
        {
            m_pMathFrame->at(i)->paint(event,this);
        }
    }
}

void MathScaleWnd::setInputValid(int mId, bool inputValid)
{
    if( (mId-1) < m_pMathFrame->size() )
    {
        MathScaleFrame* p = m_pMathFrame->at(mId - 1);
        if( p)
        {
            p->setInvalid(inputValid);
            update();
        }
    }
}

//! scale窗口的宽度的变化随着运算符变化而变化
void MathScaleWnd::resetRect()
{
    mathNum = 0;
    int wndWidth = 0;
    for(int i = 0;i < 4;i++)
    {
        if(m_pMathFrame->at(i) != NULL)
        {
            mathNum++;
            int currentWidth = m_pMathFrame->at(i)->getStyle().changeGeo(mathNum - 1);
            wndWidth = wndWidth > currentWidth ? wndWidth :currentWidth;
        }
    }

    int height = initWndRect.height();
    QRect rect = initWndRect.adjusted(0,-(mathNum-1)*height,0,0);
    if(wndWidth != 0)
    {
        rect.setWidth( wndWidth );
    }
    setGeometry(rect);
}

void MathScaleWnd::paintBase()
{
    QRect selfRect = contentsRect();

    QPainter painter(this);
    painter.fillRect( selfRect, Qt::black );

    painter.setPen( mColor );
    painter.drawLine( 0,0, selfRect.width() -1, 0 );
    painter.drawLine( 0,0, 0, selfRect.height()-1);
    painter.drawLine( selfRect.width() -1,0,
                      selfRect.width() -1, selfRect.height()-1);
    painter.drawLine( 0,selfRect.height()-1,
                      selfRect.width() -1, selfRect.height()-1);
}

void MathScaleWnd::resetHandler(MathScaleFrame *&pDst, MathScaleFrame *pSrc)
{
    Q_ASSERT(pSrc != NULL);
    if(pDst == NULL)
    {
        pDst = pSrc;
    }
    else
    {
        delete pDst;
        pDst = pSrc;
    }
}
