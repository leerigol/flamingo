#ifndef MATHGRADE_H
#define MATHGRADE_H

#include <QWidget>
#include <QVector>
#include <QPoint>
#include <QColor>

class CMathGradeWnd : public QWidget
{
    Q_OBJECT

public:
    explicit CMathGradeWnd(QWidget *parent = 0);
    void paintEvent(QPaintEvent* event);
    void setupUI();
    void setMathGrade(void *p);

private:
    QVector<QColor> m_colorForIds;
    void* m_pCurveRes;
};

#endif // MATHGRADE_H
