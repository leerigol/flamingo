#ifndef CAPPMATHGP_H
#define CAPPMATHGP_H

#include "../capp.h"

class CAppMathGp : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppMathGp();
};

class CAppFFTGp : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppFFTGp();
};

#endif // CAPPMATHGP_H
