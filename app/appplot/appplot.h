#ifndef APPPLOT_H
#define APPPLOT_H

#include "../capp.h"
#include "../../service/servplot/servplot.h"

#include <QtCore>
#include <QtWidgets>

//#include <qwt_plot.h>
//#include <qwt_plot_curve.h>
//#include <qwt_plot_grid.h>
//#include <qwt_plot_marker.h>
//#include <qwt_scale_div.h>

class RWaveLine
{
public:
    int mId;
    QColor mLineColor;
    DsoWfm wfm;
};
typedef QList<RWaveLine*> WaveLineList;

class RWaveWidget : public QWidget
{
    Q_OBJECT

public:
    RWaveWidget( QWidget *parent = 0 );
    ~RWaveWidget();
public:
    void attachLine( int id, QColor color, DsoWfm *pWfm );
    void detachLine( int id );

protected:
    virtual void paintEvent( QPaintEvent *event );

protected:
    void drawLine( QPainter *painter, RWaveLine *pLine );

private:
    WaveLineList mLineList;
};




class appPlot : public CApp
{
    Q_OBJECT
public:
    appPlot();
    virtual ~appPlot();
public:
    virtual void  setupUi( CAppMain *pMain );
    virtual void retranslateUi();
    virtual void buildConnection();
    virtual void resize();

protected:


private:
    RWaveWidget *m_pWidget;

private Q_SLOTS:
    void onRefreshPlot( int id, plotContext &context );
    void onClosePlot( int id );
};

#endif // APPPLOT_H
