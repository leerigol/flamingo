#include "appplot.h"

#include "../appmain/cappmain.h"
#include "../../service/service_name.h"

#define SCR_LEFT    0
#define SCR_RIGHT   999
#define SCR_SIZE    (SCR_RIGHT-SCR_LEFT+1)

#define SCR_TOP     225
#define SCR_BOTTOM  25

#define SCR_H_DIV   50
#define SCR_V_DIV   25



RWaveWidget::RWaveWidget( QWidget *parent ) : QWidget(parent)
{}

RWaveWidget::~RWaveWidget()
{
    RWaveLine* pLine;

    foreach( pLine, mLineList )
    {
        Q_ASSERT( pLine != NULL );

        delete pLine;
    }
}

void RWaveWidget::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);

    RWaveLine *pLine;

    foreach( pLine, mLineList )
    {
        Q_ASSERT( pLine != NULL );

        drawLine( &painter, pLine );
    }
}

void RWaveWidget::drawLine( QPainter *painter, RWaveLine *pLine )
{
    Q_ASSERT( painter != NULL );
    Q_ASSERT( pLine != NULL );

    int i;
    float xRatio, yRatio;
    int xStart;
    float y1, y2;
    QRect rect;

    rect = contentsRect();

    xRatio = (float)rect.width()/1000;
    yRatio = (float)rect.height()/200;

    xStart = pLine->wfm.start();

    painter->setPen( pLine->mLineColor );

    //! draw a line
    if ( pLine->wfm.size() > 1 )
    {
        //! draw lines
    }
    //! draw a dot
    else if ( pLine->wfm.size() > 0 )
    {
        y1 = yRatio*( 200 - pLine->wfm[xStart] + 8);

        y1 = y1 < 0 ? 0 : y1;
        y1 = y1 > rect.height() ? rect.height()-1 : y1;

        painter->drawPoint( xStart * xRatio,
                            y1 );

        return;
    }
    else
    { return; }

    int j = 0;
    for ( i = xStart; i < 999 && j < pLine->wfm.size() -1 ;i++, j++ )
    {
        y1 = yRatio*( 200 - pLine->wfm[i] + 8);
        y2 = yRatio*( 200 - pLine->wfm[i+1] + 8);
        y1 = y1 < 0 ? 0 : y1;
        y1 = y1 > rect.height() ? rect.height()-1 : y1;
        y2 = y2 < 0 ? 0 : y2;
        y2 = y2 > rect.height() ? rect.height()-1 : y2;
        painter->drawLine( (i)*xRatio, y1,
                           (i+1)*xRatio, y2 );
    }
}

void RWaveWidget::attachLine( int id, QColor color, DsoWfm *pWfm )
{
    RWaveLine* pLine;

    foreach( pLine, mLineList )
    {
        Q_ASSERT( pLine != NULL );

        if ( pLine->mId == id )
        {
            pLine->mLineColor = color;
            pLine->wfm = *pWfm;
            return;
        }
    }

    //! new one
    pLine = new RWaveLine();
    Q_ASSERT( pLine != NULL );
    pLine->mId = id;
    pLine->mLineColor = color;
    pLine->wfm = *pWfm;

    mLineList.append( pLine );
}
void RWaveWidget::detachLine( int id )
{
    RWaveLine* pLine;

    foreach( pLine, mLineList )
    {
        Q_ASSERT( pLine != NULL );

        if ( pLine->mId == id )
        {
            mLineList.removeOne( pLine );
            return;
        }
    }
}

appPlot::appPlot()
{
    mservName =  serv_name_plot;
}

appPlot::~appPlot()
{

}

void appPlot::setupUi( CAppMain *pMain )
{
    qRegisterMetaType<plotContext>( ("plotContext&") );

    m_pWidget = new RWaveWidget();

    m_pWidget->setParent( pMain->getCentBar() );
}
void appPlot::retranslateUi( )
{}

void appPlot::buildConnection()
{
    servPlot *plot;
    plot = dynamic_cast<servPlot *>( serviceExecutor::qobject( servPlot::singleName()) );
    connect( plot,
             SIGNAL(sigPlot(int,plotContext&)),
             this,
             SLOT(onRefreshPlot(int,plotContext&)) );

    connect( plot,
             SIGNAL(sigClosePlot(int)),
             this,
             SLOT(onClosePlot(int)));

}

void appPlot::resize()
{
    QWidget *p = dynamic_cast<QWidget *>( m_pWidget->parent() );

    Q_ASSERT( NULL != p );

    QRect rect = p->geometry();

    m_pWidget->resize( rect.width(), rect.height() );    

}


void appPlot::onRefreshPlot( int id, plotContext &context )
{
    context.enter();

//    qDebug()<<__FUNCTION__<<__LINE__;
//    qDebug()<<context.m_pWfm->size()<<context.m_pWfm->getyGnd();

    m_pWidget->attachLine( id, context.mColor, context.m_pWfm );
    m_pWidget->update();

    context.exit();
}

void appPlot::onClosePlot( int id )
{
    m_pWidget->detachLine( id );
}
