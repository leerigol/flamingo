#include "trigcodeboard.h"
CCodeBoard::CCodeBoard(QWidget */*parent*/)
{

    loadResource("trigcode/");
    setupUI();

    mCodeMode = CODE_TYPE_SOURCE;
    isHexCode = false;

    addAllKeys();
    set_attr( wndAttr, mWndAttr, wnd_moveable );

    m_BtnEdgeDisabled = false;
}

void CCodeBoard::setupUI()
{
    mBtnList.clear();
    mAllBtnList.clear();
    for(int i=0; i<mNameList.count(); i++){

        QPushButton *btn = new QPushButton(this);

        if(mNameList.at(i) == "left"){
            btn->setStyleSheet(mBtnLeftQss);
            connect(btn,SIGNAL(pressed()),this,SLOT(setPreBitSlot()));
        }else if(mNameList.at(i) == "right"){
            btn->setStyleSheet(mBtnRightQss);
            connect(btn,SIGNAL(pressed()),this,SLOT(setNextBitSlot()));
        }else if(mNameList.at(i) == "rise"){
            mBtnRise = btn;
            btn->setStyleSheet(mBtnRiseQss);
            connect(btn,SIGNAL(pressed()),this,SIGNAL(setCurrBitRise()));
        }else if(mNameList.at(i) == "fall"){
            mBtnFall = btn;
            btn->setStyleSheet(mBtnFallQss);
            connect(btn,SIGNAL(pressed()),this,SIGNAL(setCurrBitFall()));
        }else if(mNameList.at(i) == "OK"){
            btn->setStyleSheet(mBtnStyle);
            btn->setText(mNameList.at(i));
            connect(btn,SIGNAL(pressed()),this,SLOT(close()));
        }else if(mNameList.at(i) == "ALL"){
            btn->setStyleSheet(mBtnStyle);
            btn->setText(mNameList.at(i));
            connect(btn,SIGNAL(pressed()),this,SIGNAL(setAllSig()));
        }else if(mNameList.at(i) == "X"){
            mBtnX = btn;
            btn->setStyleSheet(mBtnXQss);
            btn->setText(mNameList.at(i));
            connect(btn,SIGNAL(pressed()),this,SIGNAL(setCurrBitX()));
        }else{
            mBtnList.append(btn);
            btn->setStyleSheet(mBtnStyle);
            btn->setText(mNameList.at(i));
            btn->setObjectName(mNameList.at(i));
            connect(btn,SIGNAL(pressed()),this,SLOT(setCurrBitSlot()));
        }

        if(mNameList.at(i) == "X")
            btn->resize(mDBtnSize);
        else
            btn->resize(mBtnSize);

        int btnX = mFirstBtnPos.x()+(mBtnPos.at(i).x())*(mSpaceSize.width()+mBtnSize.width());
        int btnY = mFirstBtnPos.y()+(mBtnPos.at(i).y())*(mSpaceSize.height()+mBtnSize.height());
        btn->move(btnX, btnY);
        mAllBtnList.append(btn);
    }
}

void CCodeBoard::setCode(QList<int> l, int count, int c)
{
    mCode.clear();
    if ( CODE_TYPE_SOURCE == mCodeMode )
    {
        if(c<4){
            for(int i=0; i<4; i++){
                mCode.append(list_at(l, i));
            }
            mCurrBit = c;
        }else{
            for(int i=4; i<20; i++){
                mCode.append(list_at(l, i));
            }
            mCurrBit = c-4;
        }
    }
    else
    {
        mCode.append(l);
        mCurrBit = c;
        if(mCurrBit<count){
            isHexCode = false;
        }else{
            isHexCode = true;
            mCurrBit = mCurrBit - count;
        }

    }

    update();
}

void CCodeBoard::setCodeMode(int mode)
{
    Q_ASSERT(   (mode == CODE_TYPE_SOURCE)||
                (mode == CODE_TYPE_DIGIT) );

    mCodeMode = mode;
    update();
}

void CCodeBoard::loadResource(QString path)
{
    r_meta::CAppMeta meta;

    meta.getMetaVal(path+"risepic",   mRisePic);
    meta.getMetaVal(path+"fallpic",   mFallPic);

    path = path+"board/";
    meta.getMetaVal(path+"bgcolor", mBgColor);
    meta.getMetaVal(path+"bgpic",   mBgPic);
    meta.getMetaVal(path+"bgpic2",   mBgPic2);

    meta.getMetaVal(path+"btnsize",    mBtnSize);
    meta.getMetaVal(path+"dbtnsize",   mDBtnSize);
    meta.getMetaVal(path+"rows",       mRows);
    meta.getMetaVal(path+"columns",    mColumns);
    meta.getMetaVal(path+"spacesize",  mSpaceSize);
    meta.getMetaVal(path+"firstpos",   mFirstBtnPos);
    meta.getMetaVal(path+"firstpos2",  mFirstBtnPos2);

    mNameList.clear();
    mBtnPos.clear();

    int num = 0;
    meta.getMetaVal(path + "numbers", num);
    QString sname;
    QPoint  spos;
    for(int i=0; i<num; i++){
        QString str = path + "item_" + QString("%1").arg(i, 2, 10, QChar('0'));
        meta.getMetaVal( str+"/sname", sname);
        meta.getMetaVal( str+"/point", spos);
        mNameList.append(sname);
        mBtnPos.append(spos);
    }

    meta.getMetaVal(path+"btnstyle",  mBtnStyle);
    meta.getMetaVal(path+"leftqss",   mBtnLeftQss);
    meta.getMetaVal(path+"rightqss",  mBtnRightQss);
    meta.getMetaVal(path+"xqss",      mBtnXQss);
    meta.getMetaVal(path+"riseqss",   mBtnRiseQss);
    meta.getMetaVal(path+"fallqss",   mBtnFallQss);

}

void CCodeBoard::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QImage img;
    if(mCode.count() <= rowBitMax)
    {
        img.load(mBgPic);
        for(int i=0; i<mAllBtnList.count(); i++)
        {
            QPushButton *btn = mAllBtnList.at(i);
            int btnX = mFirstBtnPos.x()+(mBtnPos.at(i).x())*(mSpaceSize.width()+mBtnSize.width());
            int btnY = mFirstBtnPos.y()+(mBtnPos.at(i).y())*(mSpaceSize.height()+mBtnSize.height());
            btn->move(btnX, btnY);
        }
    }
    else
    {
        img.load(mBgPic2);
        for(int i=0; i<mAllBtnList.count(); i++)
        {
            QPushButton *btn = mAllBtnList.at(i);

            int btnX = mFirstBtnPos2.x()+(mBtnPos.at(i).x())*(mSpaceSize.width()+mBtnSize.width());
            int btnY = mFirstBtnPos2.y()+(mBtnPos.at(i).y())*(mSpaceSize.height()+mBtnSize.height());
            btn->move(btnX, btnY);

        }
    }
    painter.drawImage(0, 0, img);

    QRect r;
    r.setX(20);
    r.setY(23);
    int x = r.x();
    r.setWidth(365);
    r.setHeight(25);

    QFont font;
    font.setPointSize(15);
    painter.setFont(font);

    painter.setPen(QPen(QColor(192, 192, 192, 255)));
    if (CODE_TYPE_SOURCE == mCodeMode)
    {
        for(int i=0;  i<mCode.count(); i++)
        {
            if(mCurrBit == i)
            {
                drawCode(r, painter, list_at( mCode, i), true);
            }
            else
            {
                drawCode(r, painter, list_at( mCode, i));
            }

            r.moveTo(r.x()+10, r.y());

            if( (i+1)%4 == 0)
            {
                r.moveTo(r.x()+4, r.y());
            }
        }
    }
    else
    {
        if(!isHexCode)
        {
            int align = 4 - ((mCode.count()%4));//!最高位缺 align 位
            for(int i=0; i<mCode.count(); i++)
            {
                if(mCurrBit == i)
                {
                    drawBin(r, painter, list_at( mCode, i), true);
                }
                else
                {
                    drawBin(r, painter, list_at( mCode, i), false);
                }

                if( (i != 0) && (0 == ((i+1)%rowBitMax ) ) )
                {
                    r.moveTo(x, r.y()+25 );
                }
                else
                {
                    r.moveTo(r.x()+10, r.y());
                    if(0 == ((i+align+1)%4))
                    {
                        r.moveTo(r.x()+4, r.y());
                    }
                }
            }
        }
        else
        {
            for(int i=0; i<mCode.count(); i++)
            {
                if(mCurrBit == i)
                {
                    drawHex(r, painter, list_at( mCode, i), true);
                }
                else
                {
                    drawHex(r, painter, list_at( mCode, i), false);
                }

                r.moveTo(r.x()+10, r.y());

                if(mCode.count()%2 == 0)
                {
                    if(( (i+1)%2) == 0 )
                    {
                        r.moveTo(r.x()+4, r.y());
                    }
                }
                else
                {
                    if(( i%2) == 0 )
                    {
                        r.moveTo(r.x()+4, r.y());
                    }
                }
            }
        }
    }
}

void CCodeBoard::keyReleaseEvent(QKeyEvent *pKeyEvent)
{
    int key = pKeyEvent->key();
    int cnt = pKeyEvent->count();
    if ( key == R_Pkey_FInc )
    {
        tuneInc(cnt);
    }
    else if ( key == R_Pkey_FDec )
    {
        tuneDec(cnt);
    }
    else if ( key == R_Pkey_FZ )
    {
        tuneZ(cnt);
    }
    else
    {
        this->hide();
    }
}

void CCodeBoard::drawCode(QRect &r, QPainter &p, int v, bool current)
{
    QColor  m_nBackColor = QColor(35,35,35,255);
    QColor  m_nSelColor  = QColor(18,85,159,255);

    if(current)
    {
        QRect sel = r;
        sel.moveTo(sel.x(), sel.y()+2);
        sel.setWidth(10);
        sel.setHeight(21);

        p.fillRect(sel, m_nSelColor);
    }

    if(v == Trigger_pat_x)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "X");
    }
    else if(v == Trigger_pat_h)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "1");
    }
    else if(v == Trigger_pat_l)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "0");
    }
    else if( v == Trigger_pat_rise)
    {
        QPoint point;
        QImage image(mRisePic);
        point.setX(r.x()+1);
        point.setY( r.y() +  (r.height() - image.height()) / 2 + 2);

        if(current)
        {
            RControlStyle::renderBg( image, m_nSelColor );
        }
        else
        {
            RControlStyle::renderBg( image, m_nBackColor );
        }
        p.drawImage( point, image );
    }
    else if(v == Trigger_pat_fall)
    {
        QPoint point;
        QImage image(mFallPic);
        point.setX(r.x()+1);
        point.setY(r.y() + ( r.height() - image.height()) / 2 + 2);
        if(current)
        {
            RControlStyle::renderBg( image, m_nSelColor );
        }
        else
        {
            RControlStyle::renderBg( image, m_nBackColor );
        }
        p.drawImage( point, image );
    }
}

void CCodeBoard::drawBin(QRect &r, QPainter &p, int v, bool current)
{
    QColor  m_nSelColor  = QColor(18,85,159,255);
    if(current)
    {
        QRect sel = r;
        sel.moveTo(sel.x(), sel.y()+4);
        sel.setWidth(9);
        sel.setHeight(20);
        p.fillRect(sel, m_nSelColor);
    }

    if(v == Trigger_Code_H)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "1");
    }
    else if(v == Trigger_Code_L)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "0");
    }
    else
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "X");
    }
}

void CCodeBoard::drawHex(QRect &r, QPainter &p, int v, bool current)
{
    QColor  m_nSelColor  = QColor(18,85,159,255);
    if(current)
    {
        QRect sel = r;
        sel.moveTo(sel.x(), sel.y()+4);
        sel.setWidth(9);
        sel.setHeight(20);
        p.fillRect(sel, m_nSelColor);
    }

    QString str = "X";
    if(v != Trigger_Code_X)
    {
        str = QString("%1").arg(v,1,16).toUpper();
    }
    p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, str);
}

void CCodeBoard::tuneInc(int /*keyCnt*/)
{
    for(int i=0; i<mAllBtnList.count(); i++)
    {
        QPushButton *btn = mAllBtnList.at(i);
        if(btn->hasFocus())
        {
            for( int next=i+1; next<mAllBtnList.count(); next++)
            {
                if(mAllBtnList.at(next)->isEnabled())
                {
                    mAllBtnList.at(next)->setFocus();
                    return;
                }
            }
            break;
        }
    }
}

void CCodeBoard::tuneDec(int /*keyCnt*/)
{
    for(int i=0; i<mAllBtnList.count(); i++)
    {
        QPushButton *btn = mAllBtnList.at(i);
        if(btn->hasFocus())
        {
            for( int next=i-1; next >= 0; next--)
            {
                if(mAllBtnList.at(next)->isEnabled())
                {
                    mAllBtnList.at(next)->setFocus();
                    return;
                }
            }
            break;
        }
    }

}

void CCodeBoard::tuneZ(int /*keyCnt*/)
{
    for(int i=0; i<mAllBtnList.count(); i++)
    {
        QPushButton *btn = mAllBtnList.at(i);
        if(btn->hasFocus())
        {
            btn->pressed();
            break;
        }
    }

}

void CCodeBoard::setCurrBitSlot()
{
    QString str = sender()->objectName();

    emit setCurrBitValue(str.toInt(0,16));
}

void CCodeBoard::setNextBitSlot()
{
    if(mCurrBit <= mCode.count()-1)
    {
        emit gotoNextBitSig();
        update();
    }
}

void CCodeBoard::setPreBitSlot()
{
    if(mCurrBit >= 0)
    {
        emit gotoPreBitSig();
        update();
    }
}

void CCodeBoard::setBtnDisabled(int bit)
{
    switch(bit)
    {
    case 0:
        for(int i=0; i<mBtnList.count(); i++)
        {
            mBtnList.at(i)->setEnabled(true);
        }
        break;
    case 1:
        for(int i=0; i<mBtnList.count(); i++)
        {
            QPushButton *btn = mBtnList.at(i);
            if(btn->objectName().toInt(0,16) >= 0
                    &&btn->objectName().toInt(0,16) <= 1)
            {
                btn->setEnabled(true);
            }
            else
            {
                btn->setEnabled(false);
            }
        }
        break;
    case 2:
        for(int i=0; i<mBtnList.count(); i++)
        {
            QPushButton *btn = mBtnList.at(i);
            if(btn->objectName().toInt(0,16) >= 0
                    &&btn->objectName().toInt(0,16) <= 3)
            {
                btn->setEnabled(true);
            }
            else
            {
                btn->setEnabled(false);
            }
        }
        break;
    case 3:
        for(int i=0; i<mBtnList.count(); i++)
        {
            QPushButton *btn = mBtnList.at(i);
            if(btn->objectName().toInt(0,16) >= 0
                    &&btn->objectName().toInt(0,16) <= 7)
            {
                btn->setEnabled(true);
            }
            else
            {
                btn->setEnabled(false);
            }
        }
        break;
    default:
        for(int i=0; i<mBtnList.count(); i++)
        {
            QPushButton *btn = mBtnList.at(i);
            if(btn->objectName().toInt(0,16) >= 0
                    &&btn->objectName().toInt(0,16) <= 1)
            {
                btn->setEnabled(true);
            }
            else
                btn->setEnabled(false);
        }
        break;
    }

    if ( (CODE_TYPE_SOURCE == mCodeMode)
         && (!m_BtnEdgeDisabled) )
    {
        mBtnRise->setEnabled(true);
        mBtnFall->setEnabled(true);
    }
    else
    {
        mBtnRise->setEnabled(false);
        mBtnFall->setEnabled(false);
    }
}

void CCodeBoard::setBtnEdgeDisabled(bool b)
{
    m_BtnEdgeDisabled = b;
}
void CCodeBoard::setCurrBitBtnFocus(int /*num*/)
{
    for(int i=0; i<mNameList.count(); i++)
    {
        if(mNameList.at(i) == "OK")
        {
            mAllBtnList.at(i)->setFocus();
        }
    }
}

bool CCodeBoard::getIsHexCode()
{
    return isHexCode;
}
