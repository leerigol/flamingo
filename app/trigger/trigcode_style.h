#ifndef TRIGCODE_STYLE_H
#define TRIGCODE_STYLE_H

#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rcontrolstyle.h"

class CCode_Style : public menu_res::RUI_Style
{

protected:
    virtual void load(const QString &path,
                       const r_meta::CMeta &meta );
public:
    //background
    QColor  mBgColor;

    int     mMidWidth1, mMidWidth2;
    int     mHeight;
//    QString mPatBg;

    QString mTL, mTm1, mTm2, mTm3, mTR;
    QString mLm, mCent, mRm;
    QString mBL, mBm1, mBm2, mBm3, mBR;

};

#endif // TRIGCODE_STYLE_H


