#include "trigcode_style.h"


void CCode_Style::load(const QString &path, const r_meta::CMeta &meta)
{
    mMidWidth1 = 0;
    mMidWidth2 = 0;
    mHeight    = 0;

    meta.getMetaVal(path+"bgcolor", mBgColor);

    meta.getMetaVal(path+"midwidth1", mMidWidth1);
    meta.getMetaVal(path+"midwidth2", mMidWidth2);
    meta.getMetaVal(path+"mheight",   mHeight);

//    meta.getMetaVal(path+"mtlt", mPatBg);

    meta.getMetaVal(path+"mtl", mTL);
    meta.getMetaVal(path+"mtm1", mTm1);
    meta.getMetaVal(path+"mtm2", mTm2);
    meta.getMetaVal(path+"mtm3", mTm3);
    meta.getMetaVal(path+"mtr", mTR);

    meta.getMetaVal(path+"mlm", mLm);
    meta.getMetaVal(path+"mcent", mCent);
    meta.getMetaVal(path+"mrm", mRm);

    meta.getMetaVal(path+"mbl", mBL);
    meta.getMetaVal(path+"mbm1", mBm1);
    meta.getMetaVal(path+"mbm2", mBm2);
    meta.getMetaVal(path+"mbm3", mBm3);
    meta.getMetaVal(path+"mbr", mBR);

}
