#ifndef TRIGCODEBOARD_H
#define TRIGCODEBOARD_H

//#include "../../menu/wnd/uiwnd.h"
#include "../../menu/wnd/rpopmenuchildwnd.h"
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../meta/crmeta.h"

#define Trigger_Code_X  (0xff)
#define Trigger_Code_H  (1)
#define Trigger_Code_L  (0)
#define rowBitMax       (32)

#define CODE_TYPE_DIGIT        (0)
#define CODE_TYPE_SOURCE       (1)

using namespace menu_res;
class CCodeBoard : public RPopMenuChildWnd
{
    Q_OBJECT

public:
    CCodeBoard(QWidget *parent = 0);
    void setupUI();

    void setCode(QList<int> l, int count, int c);
    void setCodeMode(int mode);
protected:
    void loadResource(QString path);
    void paintEvent(QPaintEvent *);
    void keyReleaseEvent(QKeyEvent *);

    void drawCode(QRect &r, QPainter&p, int v, bool current = false);

    void drawBin(QRect &r, QPainter &p, int v, bool current);
    void drawHex(QRect &r, QPainter &p, int v, bool current);

    virtual void tuneInc( int keyCnt );
    virtual void tuneDec( int keyCnt );
    virtual void tuneZ( int keyCnt );

signals:
    void setCurrBitValue(int currBit);
    void gotoNextBitSig();
    void gotoPreBitSig();
    void setCurrBitRise();
    void setCurrBitFall();
    void setAllSig();
    void setCurrBitX();

public slots:
    void    setCurrBitSlot();

    void    setNextBitSlot();
    void    setPreBitSlot();

    void    setBtnDisabled(int bit);
    void    setBtnEdgeDisabled(bool b);

    void    setCurrBitBtnFocus(int);

public:
    bool    getIsHexCode();

private:
    QList<int>  mCode;
    int         mCurrBit;
    int         mCodeMode;
    bool        isHexCode;
    bool        m_BtnEdgeDisabled;

    QList<QPushButton *> mBtnList, mAllBtnList;
    QPushButton     *mBtnX, *mBtnAll;
    QPushButton     *mBtnRise, *mBtnFall;
    QPushButton     *mBtnLeft, *mBtnRight;
//    QPushButton     *mBtnOk;

    QString     mRisePic, mFallPic;

    QColor      mBgColor;
    QString     mBgPic;
    QString     mBgPic2;

    QSize       mBtnSize;
    QSize       mDBtnSize;
    int         mRows;
    int         mColumns;
    QSize       mSpaceSize;
    QPoint      mFirstBtnPos, mFirstBtnPos2;

    QStringList mNameList;
    QList<QPoint> mBtnPos;

    QString      mBtnStyle, mBtnLeftQss, mBtnRightQss;
    QString      mBtnXQss, mBtnRiseQss, mBtnFallQss;

};

#endif // TRIGCODEBOARD_H
