#include "QPainter"
#include "triglevel.h"
#include "../appdisplay/cgrid.h"
#include "../../service/servicefactory.h"

CLevelLine::CLevelLine(QWidget *parent, QColor color) : QWidget(parent)
{
    m_nColor = color;
    this->resize(CGrid::getWaveWidth(), 1);
}

void CLevelLine::setVisible(bool visible)
{
    QWidget::setVisible(visible);
    update();
}

void CLevelLine::setColor(QColor color)
{
    m_nColor = color;
    update();
}

void CLevelLine::paintEvent(QPaintEvent */*event*/)
{
    QPainter painter(this);
    QPen pen;

    pen.setStyle(Qt::DashDotLine);
    pen.setColor( m_nColor );
    painter.setPen(pen);

    painter.drawLine(0,0,0+CGrid::getWaveWidth(),0);
}

/*!
 * \brief CTriggerLevel::CTriggerLevel
 * \param parent
 */
CTriggerLevel::CTriggerLevel(QWidget *parent) : QWidget(parent)
{
    m_pos = 0;
    m_inZoom = false;

    m_pLine = new CLevelLine(this);
    Q_ASSERT(m_pLine != NULL);
    m_pLine->setVisible( false );

    m_pGnd   = new RDsoVGnd( "T", RDsoGndTag::to_right,RDsoGndTag::cone);
    Q_ASSERT(m_pGnd != NULL);
    connect(m_pGnd, SIGNAL(sigMove(int,int)),this, SLOT(onGndMoved(int,int)));
    m_pGnd->setVisible( false );
    m_pGnd->setView(DSO_VGND_WIDTH, wave_height);
    m_pGnd->setViewGnd(wave_height/2 -1);

    m_viewRect.setRect(0,0,wave_width,wave_height);
}

void CTriggerLevel::setupUi()
{
    this->resize(wave_width, 1);
    m_pGnd->setColor( Trig_Color, Qt::black );
}

void CTriggerLevel::refresh()
{
    bool bLine = m_pLine->isVisible();
    bool bGnd  = m_pGnd->getVisible();

    setPos(m_pos, isVisible());

    m_pLine->setVisible(bLine);
    m_pGnd->setVisible(bGnd);
}

void CTriggerLevel::setVisible(bool visible)
{
    QWidget::setVisible(visible);
    m_pLine->setVisible(visible);
    m_pGnd->setVisible(visible);
}

void CTriggerLevel::setWidgetVisible(bool b)
{
    Q_UNUSED(b)
    update();
}
void CTriggerLevel::setGndParent(QWidget *parent)
{
    m_pGnd->setParent(parent);
    m_pGnd->setActive();
}

void CTriggerLevel::setColor(QColor color)
{
    m_pLine->setColor(color);
    m_pGnd->setColor( Trig_Color/*color*/, Qt::black);
    update();
}

void CTriggerLevel::setCaption(QString caption)
{
    m_pGnd->setCaption(caption);
    update();
}

void CTriggerLevel::hideGND()
{
    m_pGnd->setVisible( false );
    update();
}

void CTriggerLevel::hideLine()
{
    m_pLine->setVisible(false);
    update();
}

bool CTriggerLevel::getLineVisible()
{
    return m_pLine->isVisible();
}

void CTriggerLevel::setView(DsoView view)
{
    sysAttachUIView(this,   view);
    sysAttachUIView(m_pGnd, view);

    m_inZoom = (view == view_ygnd_zoom)?true:false;
    update();
}

void CTriggerLevel::onGndMoved(int , int posY)
{
    qint64      levelBegin;
    serviceExecutor::query( serv_name_trigger, MSG_TRIGGER_LEVEL, levelBegin);

    long long   activeScaleBegin = 0;
    Chan activeChan = dsoVert::getActiveChan();
    QString servName = getChanName(activeChan);

    if(activeChan<=chan4 && activeChan >= chan1){
        activeScaleBegin   = dsoVert::getCH( activeChan )->getYRefScale();
    }else if((activeChan<=r10 && activeChan >= r1) || activeChan == ref ){
        serviceExecutor::query( serv_name_ref, MSG_REF_VOLT,  activeScaleBegin );
    }else if(activeChan<=m4 && activeChan >= m1 ){
        serviceExecutor::query( servName, MSG_MATH_S32FUNCSCALE,  activeScaleBegin );
    }

    int mode    = 0;
    float temp  = 0;
    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, mode );
    DsoScreenMode scrMode = (DsoScreenMode)mode;
    if ( scrMode == screen_yt_main || scrMode == screen_roll_main)
    {
        temp = (float)posY/60;
    }
    else if(scrMode == screen_yt_main_zoom )
    {
        if(m_inZoom)
        {
            temp = (float)posY/40;
        }
        else
        {
            temp = (float)posY/18;
        }
    }
    qint64 lev2 = levelBegin-temp*activeScaleBegin;
    serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL, lev2);

}

QString CTriggerLevel::getChanName( Chan ch )
{
    switch (ch) {
    case m1:
        return serv_name_math1;
    case m2:
        return serv_name_math2;
    case m3:
        return serv_name_math3;
    case m4:
        return serv_name_math4;
    default:
        break;
    }
    return NULL;
}

void CTriggerLevel::setPos(int pos, bool visible)
{
    if(visible)
    {
        //! Gnd
        int nMiddle = m_viewRect.height() / 2 + m_viewRect.top();
        m_pGnd->move(nMiddle - pos);

        //!line
        nMiddle = m_phyRect.height() / 2 + m_phyRect.top();
        int phyPos  = nMiddle - pos*( (float)m_phyRect.height() / m_viewRect.height() );

        //qDebug()<<__FILE__<<__LINE__<<m_phyRect.height()<<m_phyRect.bottom()<<m_phyRect.top()<<"______"<<pos<<phyPos;

        if ( phyPos < m_phyRect.top() )
        {
            phyPos = m_phyRect.top();
        }
        else if ( phyPos > m_phyRect.bottom() )
        {
            phyPos = m_phyRect.bottom();
        }
        else
        {}
        this->move(m_phyRect.left(), phyPos);
    }
    else
    {
    }

    m_pos = pos;
    setVisible(visible);
    update();
}

void CTriggerLevel::paintEvent(QPaintEvent *)
{
    //qDebug() << this->geometry();
}



