#ifndef ZONE_H
#define ZONE_H
#include "qwidget.h"
#include "QComboBox"

#include "../../menu/rmenus.h"
#include "../appmain/touchzone.h"
/*!
 * \brief The touchZone class
 *  .负责计算图形坐标, 拖动，缩放。
 *  .单位是物理坐标
 */
class touchZone : public QWidget
{
    Q_OBJECT
public:
    touchZone(TouchZone *touch, QWidget *parent = 0);
    void setupUi(void);
    void paintEvent(QPaintEvent *);
    void findZone(QPoint point);
    bool mouseEventValid(QPoint pos);

public:
    void getZoneCoords(int index, QPoint &p1, QPoint &p2);
    void setZoneCoords(int index, QPoint p1,  QPoint p2);

    QMap<int, QRect> getZoneMap();
    void setZoneEn(int index, bool en);

private:
    TouchZone       *m_pTouchZone;

    bool             m_dragging;
    int              m_index;

    QRect            m_drawZone;
    QPoint           m_position;

    QMap<int, QRect> m_zoneMap;
    QMap<int, bool>  m_zoneEn;

protected Q_SLOTS:
    void  on_zone_selcet(int index);

    void  on_mouse_move_event(QPoint pos);
    void  on_mouse_pressEvent(QPoint pos);
    void  on_mouse_releaseEvent(QPoint pos);

Q_SIGNALS:
    void sig_zone_selcet(int index);
    void sig_zone_change(void);
    void sig_zone_paintEvent(void);
};

/*!
 * \brief The dispZone class
 * . 只负责画画。
 */
class dispZone : public QWidget
{
    Q_OBJECT
public:
    dispZone(QWidget * parent = 0);
    void paintEvent(QPaintEvent *);

    void setRect(QMap<int, QRect> rectMap);
    void setZoneEn(int index, bool en);
    void setZoneColor(int index, QColor color);
    void setZoneBrush(int index, Qt::BrushStyle brush);

private:
    QMap<int, bool>            m_enMap;
    QMap<int, QRect>           m_zoneMap;
    QMap<int, QColor>          m_colorMap;
    QMap<int, Qt::BrushStyle>  m_brushMap;
};


/*!
 * \brief The dispZone class
 * . 负责视图管理
 */
class viewZone : public QWidget, public menu_res::RDsoUI
{
    Q_OBJECT
public:
    viewZone(QWidget * parent = 0);
    void setWidgetVisible( bool b );
    void refresh();

public:
    QRect m_viewRect;           //!< (0,0) --> (width,height)
    QRect m_phyRect;            //!< physical rect

Q_SIGNALS:
    void sig_zone_view_change(void);
};


/*!
 * \brief The trigZoneWnd class
 * . 触摸操作需要在最顶层，画图应该画在最底层。
 */
class trigZoneWnd : public QObject
{
    Q_OBJECT
public:
    trigZoneWnd(TouchZone *touchParent,
                QWidget *viewParent = 0,
                QWidget *dispParent = 0);

public:
    void setZoneColor(int index, QColor color);
    void setupUi(void);
    void show();
    void setView(DsoView view);

    void getZoneCoords(int index, QPoint &p1, QPoint &p2);
    void setZoneCoords(int index, QPoint p1, QPoint p2);

public:
    void   setEnable(bool en);
    void   setEnableZoneX(int index,   bool en);
    void   setIntersectZoneX(int index, bool b);

protected:
    touchZone         *m_pTouchWnd;
    dispZone          *m_pDispWnd;
    viewZone          *m_pViewWnd;

protected Q_SLOTS:
    void on_zone_paintEvent(void);
    void on_zone_view_change(void);

Q_SIGNALS:
    void sig_zone_view_change(void);
    void sig_zone_selcet(int);
    void sig_zone_change(void);
};



#endif // ZONE_H
