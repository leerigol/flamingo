#include "apptrigger.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../appdisplay/cgrid.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppTrigger )
start_of_entry()

on_set_int_int ( MSG_HOR_ZOOM_ON,            &CAppTrigger::onZoom ),
on_set_int_int ( MSG_HOR_TIME_MODE,          &CAppTrigger::onXY ),

//! mode
on_set_int_int ( MSG_TRIGGER_TYPE,           &CAppTrigger::onModeChanged ),

//! code
on_set_int_int ( MSG_TRIGGER_SET_CODE,       &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_CODE,           &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_CODE_ALL,       &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_I2C_WHEN,       &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_I2C_CURRBIT,    &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_I2C_BYTELENGTH, &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_SPI_CURRBIT,    &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_SPI_DATABITS,   &CAppTrigger::onCodeChanged),

on_set_int_int ( MSG_TRIGGER_CAN_WHEN,       &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_CAN_ID_EXTENDED,&CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_CURR_BIT,       &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_CAN_DATA_BYTE,  &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_CAN_DEFINE,     &CAppTrigger::onCodeChanged),

on_set_int_int ( MSG_TRIGGER_IIS_DATA,       &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_IIS_DATA_MIN,   &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_IIS_DATA_MAX,   &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_IIS_CURR_BIT,   &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_IIS_WIDTH,      &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_IIS_USER_WIDTH, &CAppTrigger::onCodeChanged),

on_set_int_int ( MSG_TRIGGER_LIN_WHEN,       &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_LIN_DATA_BIT,   &CAppTrigger::onCodeChanged),
on_set_int_int ( MSG_TRIGGER_LIN_DATA_BYTE,  &CAppTrigger::onCodeChanged),

on_set_int_int ( MSG_TRIGGER_1553B_WHEN,     &CAppTrigger::onCodeChanged),

//! source
on_set_int_int ( MSG_TRIGGER_SOURCE_LA_EXT_AC,  &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_SOURCE_LA_EXT,     &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_SOURCE_LA,         &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_SOURCE,            &CAppTrigger::onSourceChanged ),

on_set_int_int ( MSG_TRIGGER_DELAY_SRCA,     &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_DELAY_SRCB,     &CAppTrigger::onSourceChanged ),

on_set_int_int ( MSG_TRIGGER_SETUP_SCL,      &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_SETUP_SDA,      &CAppTrigger::onSourceChanged ),

on_set_int_int ( MSG_TRIGGER_I2C_SCL,        &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_I2C_SDA,        &CAppTrigger::onSourceChanged ),

on_set_int_int ( MSG_TRIGGER_SPI_SCL,        &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_SPI_SDA,        &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_SPI_CS,         &CAppTrigger::onSourceChanged ),

on_set_int_int ( MSG_TRIGGER_IIS_SCLK,       &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_IIS_WS,         &CAppTrigger::onSourceChanged ),
on_set_int_int ( MSG_TRIGGER_IIS_SDA,        &CAppTrigger::onSourceChanged ),

on_set_int_int ( MSG_TRIGGER_COUPLING,       &CAppTrigger::onCoupling),

on_set_int_int ( MSG_TRIGGER_EDGE_A,         &CAppTrigger::onSlopeChanged),
on_set_int_int ( MSG_TRIGGER_SLOPE_POLARITY, &CAppTrigger::onSlopeChanged),
on_set_int_int ( MSG_TRIGGER_POLARITY,       &CAppTrigger::onSlopeChanged),

//!LEVEL
on_set_int_int ( MSG_TRIGGER_LEVEL,          &CAppTrigger::onLevelSelfChanged ),
on_set_int_int ( MSG_CHAN_INVERT,            &CAppTrigger::onLevelSelfChanged ),

on_set_int_int ( MSG_TRIGGER_APPLY_LEVEL,    &CAppTrigger::onLevelChanged ),
on_set_int_int ( MSG_CHAN_PROBE,             &CAppTrigger::onLevelChanged),
on_set_int_int ( MSG_CHAN_UNIT,              &CAppTrigger::onLevelChanged),

on_set_int_int ( MSG_LA_LOW_THRE_VAL,        &CAppTrigger::onLaLevelChanged),
on_set_int_int ( MSG_LA_HIGH_THRE_VAL,       &CAppTrigger::onLaLevelChanged),

on_set_void_void(   MSG_TRIGGER_SWEEP,       &CAppTrigger::onGetSweep),

on_set_void_void(CMD_TRIGGER_ALL_SPY_LICENSE,&CAppTrigger::onLicenseOpt),

//! zone
on_set_int_int ( MSG_TRIG_ZONE_A_ENABLE,     &CAppTrigger::onEnableZoneX),
on_set_int_int ( MSG_TRIG_ZONE_B_ENABLE,     &CAppTrigger::onEnableZoneX),
on_set_int_int ( MSG_TRIG_ZONE_A_INTERSECT,  &CAppTrigger::onIntersectZoneX),
on_set_int_int ( MSG_TRIG_ZONE_B_INTERSECT,  &CAppTrigger::onIntersectZoneX),
on_set_int_int ( MSG_TRIG_ZONE_A_SOURCE,     &CAppTrigger::onZoneSourceX),
on_set_int_int ( MSG_TRIG_ZONE_B_SOURCE,     &CAppTrigger::onZoneSourceX),

on_set_int_int ( CMD_TRIG_ZONE_UPDATE_SHOW,  &CAppTrigger::onZoneServChange),

//! sys
on_set_int_int (  CMD_SERVICE_EXIT_ACTIVE ,     &CAppTrigger::onDeActive),
on_set_void_void( CMD_SERVICE_ENTER_ACTIVE,     &CAppTrigger::onInActive ),
on_set_int_int  ( CMD_SERVICE_ITEM_FOCUS_AGAIN, &CAppTrigger::onItemFocus ),

end_of_entry()

QString  CAppTrigger::m_ChanNames[] = {"NONE", serv_name_ch1,serv_name_ch2,serv_name_ch3,serv_name_ch4};

bool CAppTrigger::m_bTimeout = false;
CAppTrigger * CAppTrigger::m_pAppTrigger = NULL;
CAppTrigger::CAppTrigger()
{
    mservName   =  serv_name_trigger;
    m_pTrigInfo = NULL;
    m_pSource   = NULL;
    m_bXYMode   = false;
    m_bXYFull   = false;
    m_bZoom     = false;
    m_coup      = DC;
    m_pTimer    = NULL;
    m_pCode     = NULL;
    m_mode      = Trigger_Edge;
    m_servName  = serv_name_trigger_edge;
    m_sweep     = Trigger_Sweep_Auto;
    m_pMainTrigCfg = NULL;
    m_pAppTrigger = this;

}

void CAppTrigger::setupUi( CAppMain *pMain )
{
    m_pCode = new CCodeKeyboard( pMain->getCentBar() );
    Q_ASSERT(NULL != m_pCode);
    m_pCode->setupUi();
    m_pCode->hide();

    for(int i=Main_Level_A; i<All_Level; i++)
    {
        m_pLevel[i] = new CTriggerLevel();
        Q_ASSERT(m_pLevel[i] != NULL);

        m_pLevel[i]->setParent(pMain->getCentBar());
        m_pLevel[i]->setGndParent(pMain->getLeftBar());
        m_pLevel[i]->setupUi();
        m_pLevel[i]->setVisible(false);
    }

    m_pLevel[Main_Level_A]->setView(view_ygnd_main);
    m_pLevel[Main_Level_B]->setView(view_ygnd_main);
    m_pLevel[Zoom_Level_A]->setView(view_ygnd_zoom);
    m_pLevel[Zoom_Level_B]->setView(view_ygnd_zoom);

    m_pTimer = new QTimer(this);
    Q_ASSERT(m_pTimer != NULL);
    onTimeout();

    /*Get default value for displaying*/
    {
        void *ptr;
        serviceExecutor::query( m_servName, MSG_TRIGGER_SOURCE_PTR, ptr);
        Q_ASSERT(ptr != NULL);
        m_pSource = (CSource*)ptr;
    }

    //! TrigInfo--------------------------------------------------------------------
    m_pTrigInfo = new RDsoTrigInfo(pMain->getTopBar());
    Q_ASSERT(m_pTrigInfo != NULL);
    m_pTrigInfo->setVisible( true );
    //!wnd-------------------------------------------------------------------------
    m_pMainTrigCfg = new menu_res::wndTrigCfg( pMain->getCentBar() );
    Q_ASSERT( NULL != m_pMainTrigCfg );

    bool bSetup = m_pMainTrigCfg->setup("apptrig/wnd/", r_meta::CAppMeta());
    if( bSetup )
    {
        //! connect
        connect( m_pMainTrigCfg, SIGNAL(sig_leveleA_real(DsoReal)),
                 this, SLOT(on_main_level_a_real(DsoReal)) );

        connect( m_pMainTrigCfg, SIGNAL(sig_leveleB_real(DsoReal)),
                 this, SLOT(on_main_level_b_real(DsoReal)) );

        connect( m_pMainTrigCfg, SIGNAL(sig_sweep(int)),
                 this, SLOT(on_sweep(int)) );

        connect( m_pMainTrigCfg, SIGNAL(sig_tune(int)),
                 this, SLOT(on_tune(int)) );

        m_pMainTrigCfg->m_pBtnLevelASub->setArrowType(Qt::DownArrow);
        m_pMainTrigCfg->m_pBtnLevelAAdd->setArrowType(Qt::UpArrow);

        m_pMainTrigCfg->m_pBtnLevelBSub->setArrowType(Qt::DownArrow);
        m_pMainTrigCfg->m_pBtnLevelBAdd->setArrowType(Qt::UpArrow);

        m_pMainTrigCfg->arrangeOn( pMain->getCentBar(), priority_bottom, 20 );
    }

    //!Zone-------------------------------------------------------------------------
    m_pTrigZone = new trigZoneWnd( pMain->getTouchZoneBar(),
                                   pMain->getCentBar(),
                                   pMain->getBackground() );
    Q_ASSERT(m_pTrigZone != NULL);
    m_pTrigZone->setupUi();
    m_pTrigZone->show();



    m_pTrigZoneZoom = new trigZoneWnd( pMain->getTouchZoneBar(),
                                       pMain->getCentBar(),
                                       pMain->getBackground() );
    Q_ASSERT(m_pTrigZoneZoom != NULL);
    m_pTrigZoneZoom->setupUi();
    m_pTrigZoneZoom->show();

    m_pTrigZoneZoom->setView(view_yt_zoom);
    m_pTrigZone->setView(view_yt_main);

    if( bSetup )
    {
        connect( m_pTrigZone, SIGNAL(sig_zone_change(void)),
                 this, SLOT(on_zone_main_change(void)) );

        connect( m_pTrigZone, SIGNAL(sig_zone_selcet(int)),
                 this, SLOT(on_zone_selcet(int)) );

        connect( m_pTrigZoneZoom, SIGNAL(sig_zone_view_change(void)),
                 this, SLOT(on_zone_main_change(void)) );

        connect( m_pTrigZoneZoom, SIGNAL(sig_zone_change(void)),
                 this, SLOT(on_zone_zoom_change(void)) );

        connect( m_pTrigZoneZoom, SIGNAL(sig_zone_selcet(int)),
                 this, SLOT(on_zone_selcet(int)) );
    }
}

void CAppTrigger::retranslateUi( )
{
    if ( m_pMainTrigCfg && m_pMainTrigCfg->isVisible() )
    {
        Q_ASSERT(m_pMainTrigCfg != NULL);
        Q_ASSERT(m_pSource      != NULL);
        if( m_pSource->hasTwoLevel(0) && m_pSource->hasTwoLevel(1) )
        {
            m_pMainTrigCfg->setTwoLevel(true);
        }
        else
        {
            m_pMainTrigCfg->setTwoLevel(false);
        }
    }
}

void CAppTrigger::buildConnection()
{
    connect( m_pTimer, SIGNAL(timeout()), this, SLOT(onTimeout()) );

    //connect( m_pTrigInfo, SIGNAL(clicked(bool)),this, SLOT(onStartTrigger()));

    connect( m_pCode ,SIGNAL(postCurrCodeSig(int)),this,SLOT(postCurrCode(int)));
    connect( m_pCode ,SIGNAL(postCurrBitSig(int )),this,SLOT(postCurrBit(int)));
    connect( m_pCode ,SIGNAL(postSetAllSig(int )),this,SLOT(postAllBit(int)));

    connect( m_pTrigInfo, SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT(on_trig_header_click(QWidget*))    );

    connect( m_pTrigInfo, SIGNAL(sig_content_clicked(QWidget*)),
             this, SLOT(on_trig_content_click(QWidget*))   );
}

void CAppTrigger::resize()
{
}

void CAppTrigger::linkService()
{
    //! set siblings
    mservSiblings.append( serv_name_trigger_edge    );
    mservSiblings.append( serv_name_trigger_pulse   );
    mservSiblings.append( serv_name_trigger_slope   );
    mservSiblings.append( serv_name_trigger_video   );
    mservSiblings.append( serv_name_trigger_pattern );
    mservSiblings.append( serv_name_trigger_duration);
    mservSiblings.append( serv_name_trigger_timeout );
    mservSiblings.append( serv_name_trigger_runt    );
    mservSiblings.append( serv_name_trigger_over    );
    //mservSiblings.append( serv_name_trigger_window  );
    mservSiblings.append( serv_name_trigger_delay   );
    mservSiblings.append( serv_name_trigger_setup   );
    mservSiblings.append( serv_name_trigger_nth     );
    mservSiblings.append( serv_name_trigger_rs232   );
    mservSiblings.append( serv_name_trigger_i2c     );
    mservSiblings.append( serv_name_trigger_i2s     );
    mservSiblings.append( serv_name_trigger_spi     );
    mservSiblings.append( serv_name_trigger_lin     );
    mservSiblings.append( serv_name_trigger_1553b   );
    mservSiblings.append( serv_name_trigger_can     );
    mservSiblings.append( serv_name_trigger_flexray );
    mservSiblings.append( serv_name_trigger_ab      );
    //mservSiblings.append( serv_name_trigger_sbus    );
    //mservSiblings.append( serv_name_trigger_429     );
    //mservSiblings.append( serv_name_trigger_1wire   );

    mservSiblings.append( serv_name_trigger_zone     );

    //! connect each sibling
    foreach( QString str, mservSiblings )
    {
        connectService( str );
    }

    //! call base at last
    CApp::linkService();
}

void CAppTrigger::registerSpy()
{
    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON     );
    spyOn( serv_name_hori,  MSG_HOR_TIME_MODE  );

    spyOn( serv_name_la, MSG_LA_LOW_THRE_VAL, e_spy_post);
    spyOn( serv_name_la, MSG_LA_HIGH_THRE_VAL,e_spy_post);

    spyOn( serv_name_ch1, MSG_CHAN_PROBE,e_spy_post);
    spyOn( serv_name_ch2, MSG_CHAN_PROBE,e_spy_post);
    spyOn( serv_name_ch3, MSG_CHAN_PROBE,e_spy_post);
    spyOn( serv_name_ch4, MSG_CHAN_PROBE,e_spy_post);

    spyOn( serv_name_ch1, MSG_CHAN_UNIT,e_spy_post);
    spyOn( serv_name_ch2, MSG_CHAN_UNIT,e_spy_post);
    spyOn( serv_name_ch3, MSG_CHAN_UNIT,e_spy_post);
    spyOn( serv_name_ch4, MSG_CHAN_UNIT,e_spy_post);
}

int CAppTrigger::onZoom(AppEventCause)
{
    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON, m_bZoom );
    onShow( false );

    m_pTrigZoneZoom->setEnable(m_bZoom);

    return ERR_NONE;
}

int CAppTrigger::onXY(AppEventCause)
{
    int val;
    serviceExecutor::query( serv_name_hori,
                            MSG_HOR_TIME_MODE, val );

    m_bXYMode = ((val == Acquire_XY) || (val == Acquire_ROLL) )?true:false;

    m_pTrigInfo->setVisible(!m_bXYMode);

    m_pTrigZoneZoom->setEnable( (!m_bXYMode) && m_bZoom );
    m_pTrigZone->setEnable(  !m_bXYMode  );

    onShow( false );
    return ERR_NONE;
}


int CAppTrigger::onSourceChanged(AppEventCause)
{
    void *ptr;
    serviceExecutor::query( m_servName,
                            MSG_TRIGGER_SOURCE_PTR, ptr);
    Q_ASSERT(ptr != NULL);

    m_pSource = (CSource*)ptr;

    m_pTrigInfo->setSource((Chan)m_pSource->getChan());

    onShow( true );

    setCode(m_mode);

    onUpdateWndCfg();

    return ERR_NONE;
}

/**
 * @brief CAppTrigger::onModeChanged
 * @param aec
 * @return
 */
int CAppTrigger::onModeChanged(AppEventCause aec)
{
    DsoErr err = serviceExecutor::query( serv_name_trigger, MSG_TRIGGER_TYPE, (int&)m_mode );
    Q_ASSERT(err == ERR_NONE);

    m_servName = TriggerBase::getTriggerName(m_mode);
    m_pTrigInfo->setMode( m_mode );
    onSourceChanged(aec);
    onUpdateWndCfg();

    onSlopeChanged(aec);
    return ERR_NONE;
}


/**
 * @brief CAppTrigger::setCode
 * @param trigMode
 */
void CAppTrigger::setCode(TriggerMode mode)
{
    Q_ASSERT(m_pCode != NULL);

    int nCurrBit = 0;
    int when     = 0;

    m_pCode->setCodeMode(CODE_TYPE_DIGIT);

    if(mode == Trigger_Pattern || mode == Trigger_Duration)
    {
        m_pCode->setSoucrEdgeDisbale( mode == Trigger_Duration );
        m_pCode->setCodeMode(CODE_TYPE_SOURCE);

        nCurrBit = m_pSource->getChan()-1;

        m_pCode->setIsHasOptMso( sysHasLA());
        m_pCode->show();
    }
    else if(mode == Trigger_I2C)
    {
        serviceExecutor::query( m_servName, MSG_TRIGGER_I2C_WHEN,    when    );
        serviceExecutor::query( m_servName, MSG_TRIGGER_I2C_CURRBIT, nCurrBit);

        if ( !( (when == trig_i2c_data) || (when == trig_i2c_addr_data) ) )
        {
            m_pCode->hide();
            return ;
        }
    }
    else if(mode == Trigger_SPI)
    {
        serviceExecutor::query( m_servName, MSG_TRIGGER_SPI_CURRBIT, nCurrBit);
    }
    else if(mode == Trigger_CAN)
    {
        serviceExecutor::query( m_servName, MSG_TRIGGER_CAN_WHEN,    when    );
        serviceExecutor::query( m_servName, MSG_TRIGGER_CURR_BIT,    nCurrBit);

        if ( !( (when == can_when_remote_id)
                || (when == can_when_frame_id)
                || (when == can_when_frame_data)
                || (when == can_when_frame_data_id) ) )
        {
            m_pCode->hide();
            return ;
        }
    }
    else if(mode == Trigger_I2S)
    {
        serviceExecutor::query( m_servName, MSG_TRIGGER_IIS_CURR_BIT, nCurrBit);
    }
    else if(mode == Trigger_LIN)
    {
        serviceExecutor::query( m_servName, MSG_TRIGGER_LIN_WHEN,     when    );
        serviceExecutor::query( m_servName, MSG_TRIGGER_LIN_DATA_BIT, nCurrBit);

        if ( !( (when == trig_lin_data )
                ||(when == trig_lin_id_data) )
             )
        {
            m_pCode->hide();
            return ;
        }
    }
    else if(mode == Trigger_1553)
    {
        serviceExecutor::query( m_servName, MSG_TRIGGER_1553B_WHEN, when    );
        serviceExecutor::query( m_servName, MSG_TRIGGER_CURR_BIT,   nCurrBit);

        if ( !( (when == trig_1553_data ) ||
                (when == trig_1553_cmd  ) ||
                (when == trig_1553_status) ) )
        {
            m_pCode->hide();
            return ;
        }
    }
    else
    {
        m_pCode->hide();
        return ;
    }

    m_pCode->setCurrBit(nCurrBit);

    CArgument arg;
    serviceExecutor::query( m_servName, MSG_TRIGGER_SET_CODE, arg);
    if(arg.size())
    {
        if(arg.size() <= 32){
            if(mode == Trigger_Pattern || mode == Trigger_Duration)
            {
                if(sysHasLA())
                    m_pCode->resize(m_pCode->m_nPatSize1);
                else
                    m_pCode->resize(m_pCode->m_nPatSize2);
            }else{
                m_pCode->resize(m_pCode->m_nPatSize1);
            }
        }else
            m_pCode->resize(m_pCode->m_nLine2Size);

        m_pCode->setCode(arg);
        m_pCode->show();
    }
}

void CAppTrigger::setTimeout()
{
    if( m_pLevel[Main_Level_A]->getLineVisible()
            || m_pLevel[Main_Level_B]->getLineVisible()
            || m_pLevel[Zoom_Level_A]->getLineVisible()
            || m_pLevel[Zoom_Level_B]->getLineVisible() )
    {
        m_bTimeout = true;
    }
    else
    {
        m_bTimeout = false;
    }
}

void CAppTrigger::onUpdateWndCfg()
{
    Q_ASSERT(m_pMainTrigCfg != NULL);
    Q_ASSERT(m_pSource      != NULL);

    if( m_pSource->getChan() == acline )
    {
        m_pMainTrigCfg->setShow(false);
        return;
    }

    if( m_pSource->hasTwoLevel(0) && m_pSource->hasTwoLevel(1) )
    {
        m_pMainTrigCfg->setTwoLevel(true);
    }
    else
    {
        m_pMainTrigCfg->setTwoLevel(false);
    }

    //!sweep
    m_pMainTrigCfg->setSweep(m_sweep);

    //!level value
    DsoReal levelA( m_pSource->get_nViewLevel(0) ) ;
    DsoReal levelB( m_pSource->get_nViewLevel(1) ) ;
    m_pMainTrigCfg->setLeveleA(levelA);
    m_pMainTrigCfg->setLeveleB(levelB);

    m_pMainTrigCfg->setLeveleUnit(m_pSource->get_nUnit());

    DsoReal realNowA,realNowB, realMax, realMin, realDef;
    realNowA.setVal( m_pSource->get_fViewLevel(0));
    realNowB.setVal( m_pSource->get_fViewLevel(1));
    realDef.setVal(  m_pSource->get_fLevelDef()  );
    realMax.setVal(  m_pSource->get_fLevelMax()  );
    realMin.setVal(  m_pSource->get_fLevelMin()  );
    DsoRealGp levelGPA,levelGPB;
    levelGPA.setValue(realNowA, realMax, realMin, realDef);
    levelGPB.setValue(realNowB, realMax, realMin, realDef);

    m_pMainTrigCfg->setLeveleAGp(levelGPA);
    m_pMainTrigCfg->setLeveleBGp(levelGPB);

    m_pMainTrigCfg->setLeveleAId(0);
    m_pMainTrigCfg->setLeveleBId(0);
}

/**
 * @brief CAppTrigger::onCodeChanged
 * @return
 */
int CAppTrigger::onCodeChanged(int)
{
    setCode(m_mode);
    return ERR_NONE;
}

void CAppTrigger::onGetSweep()
{
    enter_app_context();

    query_service_enum( serv_name_trigger, MSG_TRIGGER_SWEEP, TriggerSweep, sweep );
    if ( _bOK )
    {
        m_pTrigInfo->setSweep( sweep );
    }
    m_sweep = sweep;
    exit_app_context();

    onUpdateWndCfg();
}

int CAppTrigger::onCoupling(AppEventCause)
{
    int value = 0;
    DsoErr err = ERR_NONE;
    serviceExecutor::query( serv_name_trigger_edge,
                            MSG_TRIGGER_COUPLING,
                            value );
    if(err == ERR_NONE)
    {
        m_coup = (Coupling)value;
        onShow(false);
    }
    return err;
}

int CAppTrigger::onSlopeChanged(AppEventCause)
{
    int    slope = 0;
    DsoErr err   = ERR_NONE;

    switch (m_mode) {
    case Trigger_Edge:
        err = query( serv_name_trigger_edge,
                     MSG_TRIGGER_EDGE_A,
                     slope );
        break;
    case Trigger_Pulse:
        err = query( serv_name_trigger_pulse,
                     MSG_TRIGGER_POLARITY,
                     slope );
        break;
    case Trigger_Slope:
        err = query( serv_name_trigger_slope,
                     MSG_TRIGGER_SLOPE_POLARITY,
                     slope );
        break;
    default:
        break;
    }
    m_pTrigInfo->setSlope(slope);
    return err;
}

void CAppTrigger::onLicenseOpt()
{
    if(TriggerBase::currTriggerMode() == Trigger_Pattern
            || TriggerBase::currTriggerMode() == Trigger_Duration)
    {
        bool optMso  =  sysCheckLicense(OPT_MSO);

        /* by hxh
        CArgument argIn,argOut;
        argIn.setVal((int)OPT_MSO);
        query(serv_name_license, servLicense::cmd_get_OptEnable, argIn, argOut);
        argOut.getVal(optMso);*/

        m_pCode->setIsHasOptMso(optMso);
        if(optMso)
            m_pCode->resize(m_pCode->m_nPatSize1);
        else
            m_pCode->resize(m_pCode->m_nPatSize2);
    }
}

bool CAppTrigger::getTimeout()
{
    m_pAppTrigger->setTimeout();
    return m_bTimeout;
}

/**
 * @brief CAppTrigger::onDeActive
 * @return
 */
int CAppTrigger::onDeActive(int )
{
    m_pCode->hide();
    return ERR_NONE;
}

int CAppTrigger::onInActive()
{
    if( m_pMainTrigCfg && m_pMainTrigCfg->isVisible() )
    {
        //qDebug() << "onDeactive of trigger";
        m_pMainTrigCfg->hide();
    }

    setCode(TriggerBase::currTriggerMode());
    return ERR_NONE;
}

DsoErr CAppTrigger::onItemFocus(int)
{
    int msg = 0;
    query(m_servName, CMD_SERVICE_ITEM_FOCUS, msg);

    if( (msg == MSG_TRIGGER_CURR_BIT)       ||
            (msg == MSG_TRIGGER_SPI_CURRBIT)    ||
            (msg == MSG_TRIGGER_I2C_CURRBIT)    ||
            (msg == MSG_TRIGGER_LIN_DATA_BIT)   ||
            (msg == MSG_TRIGGER_IIS_DATA)       ||
            (msg == MSG_TRIGGER_IIS_DATA_MIN)   ||
            (msg == MSG_TRIGGER_IIS_DATA_MAX)   ||
            (msg == MSG_TRIGGER_1553B_RTA)      ||
            (msg == MSG_TRIGGER_1553B_RTA_11)   ||
            (msg == MSG_TRIGGER_1553B_DATA_MIN) ||
            (msg == MSG_TRIGGER_1553B_DATA_MAX) )
    {
        m_pCode->onCodeBoardClick();
    }

    //qDebug()<<__FILE__<<__LINE__<<msg;
    return ERR_NONE;
}

/**
 * @brief CAppTrigger::onTimeout
 */
void CAppTrigger::onTimeout()
{
    m_bTimeout = false;
    for(int i=Main_Level_A; i<All_Level; i++)
    {
        m_pLevel[i]->hideLine();
    }
    m_pTimer->stop();
}

void CAppTrigger::postCurrCode(int value)
{
    serviceExecutor::post( m_servName,
                           MSG_TRIGGER_CODE, value );

}

void CAppTrigger::postCurrBit(int bitNum)
{
    if( (m_mode == Trigger_Pattern) || (m_mode == Trigger_Duration) )
    {
        if( (chan1 <= bitNum) && (bitNum <= d15) )
        {
            serviceExecutor::post( m_servName,
                                   MSG_TRIGGER_SOURCE_LA, bitNum );
        }
    }
    else if(m_mode == Trigger_I2C )
    {
        serviceExecutor::post( m_servName,
                               MSG_TRIGGER_I2C_CURRBIT, bitNum );
    }
    else if(m_mode == Trigger_SPI)
    {
        serviceExecutor::post( m_servName,
                               MSG_TRIGGER_SPI_CURRBIT, bitNum );
    }
    else if(m_mode == Trigger_CAN)
    {
        serviceExecutor::post( m_servName,
                               MSG_TRIGGER_CURR_BIT, bitNum );
    }
    else if(m_mode == Trigger_I2S)
    {
        serviceExecutor::post( m_servName,
                               MSG_TRIGGER_IIS_CURR_BIT, bitNum );
    }
    else if(m_mode == Trigger_LIN)
    {
        serviceExecutor::post( m_servName,
                               MSG_TRIGGER_LIN_DATA_BIT, bitNum );
    }
    else if(m_mode == Trigger_1553)
    {
        serviceExecutor::post( m_servName,
                               MSG_TRIGGER_CURR_BIT, bitNum );
    }
}

void CAppTrigger::postAllBit(int s)
{
    serviceExecutor::post( m_servName,
                           MSG_TRIGGER_CODE_ALL, s );
}

/**
 * @brief CAppTrigger::onLevelChanged
 * @return
 */
int CAppTrigger::onLevelChanged(AppEventCause)
{
    onUpdateWndCfg();
    onShow(false);
    return ERR_NONE;
}

int CAppTrigger::onLevelSelfChanged(AppEventCause event)
{
    if(event == cause_value)
    {
        onUpdateWndCfg();
        onShow(true);
    }
    return ERR_NONE;
}

int CAppTrigger::onLaLevelChanged(AppEventCause)
{
    /*! 更新LA的閾值.*/
    CSource* pLad0d7  = TriggerBase::getChanSource(d0);
    CSource* pLad8d15 = TriggerBase::getChanSource(d8);

    int ThresholdL = 0;
    int ThresholdH = 0;
    serviceExecutor::query(serv_name_la,MSG_LA_LOW_THRE_VAL,  ThresholdL);
    serviceExecutor::query(serv_name_la,MSG_LA_HIGH_THRE_VAL, ThresholdH);
    pLad0d7->setLevel( 0, ThresholdL);
    pLad8d15->setLevel(0, ThresholdH);

    /*!ch = la, update trig wnd. */
    int ch =  m_pSource->getChan(); //!当前通道
    if( (d0<=ch)&&(ch<=d15))
    {
        onShow(false);
        onUpdateWndCfg();
    }
    return ERR_NONE;
}

/**
 * @brief CAppTrigger::onHoldoff
 * @return
 */
int CAppTrigger::onHoldoff(AppEventCause)
{
    return ERR_NONE;
}

/**
 * @brief CAppTrigger::onShow
 */
void CAppTrigger::onShow(bool is_self)
{     
    Q_ASSERT(m_pSource != NULL);
    //!显示通道电平值-------------------------------------------------------------
    float   flevelA = m_pSource->get_fViewLevel(0);
    float   flevelB = m_pSource->get_fViewLevel(1);
    QString sUnit   = m_pSource->get_sUnit();
    Chan    ch      = (Chan)m_pSource->getChan();

    setLevelLineColor(ch);

    m_pTrigInfo->setUnit(sUnit);

    if( m_pSource->hasTwoLevel(0) && m_pSource->hasTwoLevel(1))  //!显示两条电平
    {
        m_pTrigInfo->setHasTwoLevel(true);
        m_pTrigInfo->setLevel(flevelA, flevelB);
    }
    else if(m_pSource->hasTwoLevel(0))  //!显示电平A
    {
        m_pTrigInfo->setHasTwoLevel(false);
        m_pTrigInfo->setLevel(flevelA);
    }
    else
    {
        m_pTrigInfo->setHasTwoLevel(false); //!显示电平B
        m_pTrigInfo->setLevel(flevelB);
    }

    //!触发电平线位置----------------------------------------------------------------------
    int    posA    = m_pSource->get_nLevelPost(0);
    int    posB    = m_pSource->get_nLevelPost(1);
    m_pLevel[Main_Level_A]->setPos(posA,     m_pSource->hasTwoLevel(0)           );
    m_pLevel[Main_Level_B]->setPos(posB,     m_pSource->hasTwoLevel(1)           );
    m_pLevel[Zoom_Level_A]->setPos(posA,    (m_pSource->hasTwoLevel(0)&&m_bZoom) );
    m_pLevel[Zoom_Level_B]->setPos(posB,    (m_pSource->hasTwoLevel(1)&&m_bZoom) );


    if(m_pSource->hasTwoLevel(0) && m_pSource->hasTwoLevel(1))
    {
        m_pLevel[Main_Level_A]->setCaption("T1");
        m_pLevel[Main_Level_B]->setCaption("T2");
        m_pLevel[Zoom_Level_A]->setCaption("T1");
        m_pLevel[Zoom_Level_B]->setCaption("T2");
    }
    else
    {
        m_pLevel[Main_Level_A]->setCaption("T");
        m_pLevel[Main_Level_B]->setCaption("T");
        m_pLevel[Zoom_Level_A]->setCaption("T");
        m_pLevel[Zoom_Level_B]->setCaption("T");
    }

    onHide(is_self);
    m_pTimer->start(3000);
}

void CAppTrigger::onHide(bool is_self)
{
    int ch = m_pSource->getChan();
    if( (ch < chan1) || (chan4 < ch  ) ||
            ( (m_servName == serv_name_trigger_edge) && ((m_coup == AC) || (m_coup == LF)) ) ||
            m_bXYMode     ||  m_bXYFull    )
    {
        m_pLevel[Main_Level_A]->setVisible(false);
        m_pLevel[Main_Level_B]->setVisible(false);
        m_pLevel[Zoom_Level_A]->setVisible(false);
        m_pLevel[Zoom_Level_B]->setVisible(false);
    }

    if( !is_self )
    {
        m_pLevel[Main_Level_A]->hideLine();
        m_pLevel[Main_Level_B]->hideLine();
        m_pLevel[Zoom_Level_A]->hideLine();
        m_pLevel[Zoom_Level_B]->hideLine();
    }
}

void CAppTrigger::setLevelLineColor(Chan ch)
{
    QColor color = sysGetChColor(ch);
    for(int i=Main_Level_A; i<All_Level; i++)
    {
        Q_ASSERT(m_pLevel[i] != NULL);
        m_pLevel[i]->setColor(color);
    }
}

/**
 * @brief CAppTrigger::onStartTrigger
 */
void CAppTrigger::onStartTrigger()
{
    serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, 1 );
    //serviceExecutor::post( serv_name_trigger_zone, CMD_SERVICE_ACTIVE, 1 );
}

void CAppTrigger::on_trig_header_click(QWidget */*pCaller*/)
{
    onStartTrigger();
}

void CAppTrigger::on_trig_content_click(QWidget */*pCaller*/)
{
    if( m_pSource->getChan() == acline)
    {
        return;
    }

    if( !m_pMainTrigCfg->isVisible() )
    {
        if( m_pSource->hasTwoLevel(0) && m_pSource->hasTwoLevel(1) )
        {
            m_pMainTrigCfg->setTwoLevel(true);
        }
        else
        {
            m_pMainTrigCfg->setTwoLevel(false);
        }
    }
    m_pMainTrigCfg->setShow(!m_pMainTrigCfg->isVisible());
}

void CAppTrigger::selectLevel(int ab)
{
    if( (Trigger_Slope    == m_mode)   ||
            (Trigger_Over   == m_mode) ||
            (Trigger_Runt   == m_mode) ||
            (Trigger_Window == m_mode) )
    {
        serviceExecutor::post(m_servName, MSG_TRIGGER_LEVELSELECT, ab);
    }
}

void CAppTrigger::on_main_level_a_real(DsoReal real)
{
    float ratio = m_pSource->get_fRatio();
    real = real*DsoReal(E_P6)/ratio;

    selectLevel(0);
    qlonglong level = 0;
    real.toReal(level);
    serviceExecutor::post(serv_name_trigger, MSG_TRIGGER_LEVEL, level);
}

void CAppTrigger::on_main_level_b_real(DsoReal real)
{
    float ratio = m_pSource->get_fRatio();
    real = real*DsoReal(E_P6)/ratio;

    selectLevel(1);
    qlonglong level = 0;
    real.toReal(level);
    serviceExecutor::post(serv_name_trigger, MSG_TRIGGER_LEVEL, level);
}

void CAppTrigger::on_sweep(int sweep)
{
    serviceExecutor::post(serv_name_trigger, MSG_TRIGGER_SWEEP,    sweep);
    serviceExecutor::post(serv_name_trigger, CMD_TRIGGER_MODE_KEY, sweep);
}

void CAppTrigger::on_tune(int id)
{
    switch( id )
    {
    case menu_res::wndTrigCfg::tune_level_a_add:
        selectLevel(0);
        sysMimicInc( R_Pkey_TRIG_LEVEL );
        break;

    case menu_res::wndTrigCfg::tune_level_b_add:
        selectLevel(1);
        sysMimicInc( R_Pkey_TRIG_LEVEL );
        break;

    case menu_res::wndTrigCfg::tune_level_a_sub:
        selectLevel(0);
        sysMimicDec( R_Pkey_TRIG_LEVEL );
        break;

    case menu_res::wndTrigCfg::tune_level_b_sub:
        selectLevel(1);
        sysMimicDec( R_Pkey_TRIG_LEVEL );
        break;

    default:
        break;
    }
}

