#ifndef TRIGCODE
#define TRIGCODE
//#include "../../menu/wnd/uiwnd.h"
#include "../../baseclass/cargument.h"
//#include "../appmain/cappmain.h"
#include "../../menu/wnd/uiwnd.h"
#include "trigcode_style.h"
#include "trigcodeboard.h"

#define Trigger_Code_X  (0xff)
#define Trigger_Code_H  (1)
#define Trigger_Code_L  (0)

#define CODE_TYPE_DIGIT        (0)
#define CODE_TYPE_SOURCE       (1)

using namespace menu_res;

class CCodeKeyboard : public uiWnd
{
    Q_OBJECT
public:
    CCodeKeyboard(QWidget *parent = 0);

    ~CCodeKeyboard();

    void setupUi();
    void loadResource(QString path);
    void buildConnection();

    void setCode(QList<int>&);
    void setCode(CArgument&);
    void setCodeMode(int mode);
    void setSoucrEdgeDisbale(bool bEdge);

    void setCurrBit(int bit = 0);

    void setIsHasOptMso(bool b);

protected:
    void mouseReleaseEvent(QMouseEvent *e);

    void paintEvent(QPaintEvent *);

    void drawCode(QRect &r, QPainter&p, int v, bool current = false);
    void drawHex(QRect &r, QPainter &p, int v, bool current = false);

    void drawByteCode(QRect&r, QPainter& painter);
    void drawBitCode(QRect&r, QPainter& painter);
    void drawBin(QRect&r, QPainter& painter, int v, bool current = false);

private:
    void painterBackground(QPainter &p);
    void doPainter(QPainter &p, CCode_Style style);

public slots:
    void gotoNextBit();
    void gotoPreBit();

    void set_X();
    void set_rise();
    void set_fall();

    void set_all();
    void setCurrBitValue(int num);

public slots:
    void onCodeBoardClick();

signals:
    void postCurrCodeSig(int);
    void postCurrBitSig(int);
    void postSetAllSig(int );

public:
    QSize   m_nPatSize1;
    QSize   m_nPatSize2;
    QSize   m_nLine2Size;

private:
    QList<int> m_nCode;
    QList<int> m_nHexCode;

    int    m_nCodeMode;
    int    m_nCurrBit;
    QColor m_nBackColor;
    QColor m_nForeColor;
    QColor m_nSelColor;

    bool   isHexTrue;
    bool   isHasOptMso;

    CCodeBoard     *m_pCodeBoard;
    QList<QRect>    mRectList;

    CCode_Style mPatStyle;
    CCode_Style mCanStyle;

    QString mRisePic, mFallPic, mPatBg;

    int touchSplitX;

};

#endif // TRIGCODE

