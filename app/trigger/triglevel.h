#ifndef _TRIG_LEVEL_
#define _TRIG_LEVEL_

#include <QWidget>
#include "../../menu/rmenus.h"

using namespace menu_res;

class CLevelLine : public QWidget
{
    Q_OBJECT
public:
    explicit CLevelLine(QWidget *parent = 0,QColor color = Trig_Color);
public:
    void setVisible(bool visible = true);
    void setColor(QColor color);
protected:
    void paintEvent( QPaintEvent *event );
private:
    QColor    m_nColor;
};


class CTriggerLevel : public QWidget,
                      public menu_res::RDsoUI
{
    Q_OBJECT
public:
    explicit CTriggerLevel(QWidget *parent = 0);
    void  setupUi(void);
    virtual void setWidgetVisible( bool b );
    virtual void refresh();
    virtual void setVisible(bool visible);
public:
    void setPos( int pos, bool visible);
    void setColor( QColor color );
    void setCaption( QString caption );

    void setGndParent(QWidget *parent);
    void hideGND();
    void hideLine();
    bool getLineVisible();

    void setView(DsoView view);

public slots:
    void onGndMoved(int, int posY);
    QString getChanName( Chan ch );

protected:
    void paintEvent( QPaintEvent *event );

private:
    int         m_pos;
    CLevelLine  *m_pLine;
    RDsoVGnd    *m_pGnd;
    bool        m_inZoom;

};

#endif
