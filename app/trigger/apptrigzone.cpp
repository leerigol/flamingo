#include "apptrigger.h"
#if 0
#define ZONE_LOG()    qDebug()<<__FILE__<<__FUNCTION__<<__LINE__
#else
#define ZONE_LOG()    QT_NO_QDEBUG_MACRO()
#endif

DsoErr CAppTrigger::onEnableZoneX(AppEventCause)
{
    bool en = false;
    query(serv_name_trigger_zone, MSG_TRIG_ZONE_A_ENABLE, en);
    m_pTrigZone->setEnableZoneX(ZONE_MENU_TRIG_A, en);
    m_pTrigZoneZoom->setEnableZoneX(ZONE_MENU_TRIG_A, (en) );


    query(serv_name_trigger_zone, MSG_TRIG_ZONE_B_ENABLE, en);
    m_pTrigZone->setEnableZoneX(ZONE_MENU_TRIG_B, en);
    m_pTrigZoneZoom->setEnableZoneX(ZONE_MENU_TRIG_B, (en) );

    return ERR_NONE;
}

DsoErr CAppTrigger::onIntersectZoneX(AppEventCause)
{
    int value = 0;
    query(serv_name_trigger_zone, MSG_TRIG_ZONE_A_INTERSECT, value);
    m_pTrigZone->setIntersectZoneX(ZONE_MENU_TRIG_A,         value);
    m_pTrigZoneZoom->setIntersectZoneX(ZONE_MENU_TRIG_A,     value );


    query(serv_name_trigger_zone, MSG_TRIG_ZONE_B_INTERSECT, value);
    m_pTrigZone->setIntersectZoneX(ZONE_MENU_TRIG_B,         value);
    m_pTrigZoneZoom->setIntersectZoneX(ZONE_MENU_TRIG_B,     value );
    return ERR_NONE;
}

DsoErr CAppTrigger::onZoneSourceX(AppEventCause)
{
    int ch = 0;
    query(serv_name_trigger_zone, MSG_TRIG_ZONE_A_SOURCE, ch);
    m_pTrigZone->setZoneColor(ZONE_MENU_TRIG_A, sysGetChColor( (Chan)ch) );
    m_pTrigZoneZoom->setZoneColor(ZONE_MENU_TRIG_A, sysGetChColor( (Chan)ch) );

    query(serv_name_trigger_zone, MSG_TRIG_ZONE_B_SOURCE, ch);
    m_pTrigZone->setZoneColor(ZONE_MENU_TRIG_B, sysGetChColor( (Chan)ch) );
    m_pTrigZoneZoom->setZoneColor(ZONE_MENU_TRIG_B, sysGetChColor( (Chan)ch) );

    onZoneServChange(cause_value);
    return ERR_NONE;
}

void CAppTrigger::onZoneServChange(AppEventCause)
{
    if(m_bXYMode )
    {
       return ;
    }

    Q_ASSERT(m_pTrigZone != NULL );

    //! 获取cfg指针
    pointer cfgPtr = NULL;
    serviceExecutor::query( serv_name_trigger_zone,
                            servZoneTrig::cmd_zone_get_cfg_ptr,
                            cfgPtr);
    Q_ASSERT(cfgPtr != NULL );

    TrigZoneCfg *pCfg = static_cast<TrigZoneCfg*>(cfgPtr);
    Q_ASSERT(pCfg != NULL );

    //! 获取水平参数
    horiAttr  hAttr = getHoriAttr( chan1, horizontal_view_main);

    for(int i = 0; i < pCfg->a_zone.count(); i++)
    {
        //! 获取垂直参数
        Chan    ch      =  pCfg->a_zone[i].ch;
        QString chName  =  QString("chan%1").arg(ch);
        int     nScale  = 1e3;
        int     nOffset = 0;
        serviceExecutor::query( chName, MSG_CHAN_SCALE_VALUE,  nScale);
        serviceExecutor::query( chName, MSG_CHAN_OFFSET,       nOffset);

        //! 获取 时间-电压 参数 转换成 像素点 w-h 参数
        qint64  tMin       = pCfg->a_zone[i].tMin;
        qint64  tMax       = pCfg->a_zone[i].tMax;
        qint32  thresholdH = pCfg->a_zone[i].thresholdH;
        qint32  thresholdL = pCfg->a_zone[i].thresholdL;

        int i_x1  = tMin/hAttr.tInc + hAttr.gnd;
        int i_x2  = tMax/hAttr.tInc + hAttr.gnd;
        int i_y1  = (thresholdH + nOffset)*scr_vdiv_dots/nScale;
        int i_y2  = (thresholdL + nOffset)*scr_vdiv_dots/nScale;

        //! 转换成以屏幕左上角为参考，的坐标系。
        i_y1 = wave_height/2 - i_y1;
        i_y2 = wave_height/2 - i_y2;

        m_pTrigZone->setZoneCoords(toZoneIndex(i),
                                   QPoint(i_x1, i_y1),
                                   QPoint(i_x2, i_y2));


       ZONE_LOG()<<"i_x1:"<<i_x1<<"i_y1:"<<i_y1
            <<"i_x2:"<<i_x2<<"i_y2:"<<i_y2;
    }


    if(m_bZoom)
    {
        tozoneConver(horizontal_view_main, horizontal_view_zoom);
    }
    //on_zone_main_change();
}

int CAppTrigger::toZoneIndex(int i)
{
    switch (i) {
    case 0:
        return ZONE_MENU_TRIG_A;
    case 1:
        return ZONE_MENU_TRIG_B;
    default:
        return ZONE_MENU_TRIG_A;
    }
    return ZONE_MENU_TRIG_A;
}

void CAppTrigger::tozoneConver(HorizontalView src, HorizontalView dest)
{
    horiAttr     destHAttr  = getHoriAttr( chan1, dest);
    horiAttr     srcHAttr   = getHoriAttr( chan1, src);
    trigZoneWnd  *pDestWnd  = NULL;
    trigZoneWnd  *pSrcWnd   = NULL;

    if(horizontal_view_main == src)
    {
        pSrcWnd   = m_pTrigZone;
        pDestWnd  = m_pTrigZoneZoom;
    }
    else
    {
        pSrcWnd   = m_pTrigZoneZoom;
        pDestWnd  = m_pTrigZone;
    }
    Q_ASSERT(NULL != pDestWnd);
    Q_ASSERT(NULL != pSrcWnd);

    QPoint point1, point2;

    for(int i = 0; i < 2; i++)
    {
        pSrcWnd->getZoneCoords(toZoneIndex(i), point1, point2);
        int i_x1  = point1.x();
        int i_x2  = point2.x();
        ZONE_LOG()<< i_x1 <<i_x2;

        qint64 t1 = (i_x1 - srcHAttr.gnd) * srcHAttr.tInc;
        qint64 t2 = (i_x2 - srcHAttr.gnd) * srcHAttr.tInc;

        i_x1  = t1/destHAttr.tInc + destHAttr.gnd;
        i_x2  = t2/destHAttr.tInc + destHAttr.gnd;

        point1.setX(i_x1);
        point2.setX(i_x2);

        ZONE_LOG()<<"t1:"<<t1<<"t2:"<<t2 << i_x1 <<i_x2;
        pDestWnd->setZoneCoords(toZoneIndex(i), point1, point2);
    }
}

void CAppTrigger::onZoneAppChange()
{
    Q_ASSERT(m_pTrigZone != NULL );

    //! 获取cfg指针
    pointer cfgPtr = NULL;
    serviceExecutor::query( serv_name_trigger_zone,
                            servZoneTrig::cmd_zone_get_cfg_ptr,
                            cfgPtr);
    Q_ASSERT(cfgPtr != NULL );

    TrigZoneCfg *pCfg = static_cast<TrigZoneCfg*>(cfgPtr);
    Q_ASSERT(pCfg != NULL );

    //! 获取区域参数
    horiAttr  hAttr = getHoriAttr( chan1, horizontal_view_main);
    QPoint point1, point2;

    qint64  tMin       = 0;
    qint64  tMax       = 0;
    qint32  thresholdH = 0;
    qint32  thresholdL = 0;

    for(int i = 0; i < pCfg->a_zone.count(); i++)
    {
        Chan    ch      = pCfg->a_zone[i].ch;
        QString chName  =  QString("chan%1").arg(ch);

        int     nScale  = 1e3;
        int     nOffset = 0;
        serviceExecutor::query( chName, MSG_CHAN_SCALE_VALUE,  nScale);
        serviceExecutor::query( chName, MSG_CHAN_OFFSET,       nOffset);

        m_pTrigZone->getZoneCoords(toZoneIndex(i), point1, point2);

        /*ZONE_LOG()<<"px1:"<<point1.x()<<"px2:"<<point2.x()
               <<"py1:"<<point1.y()<<"py1:"<<point2.y()
               <<"tInc:"<<hAttr.tInc<<"t0:"<<(-hAttr.gnd);*/

        int i_xMin  = qMin( point1.x(), point2.x() );
        int i_xMax  = qMax( point1.x(), point2.x() );
        int i_yL    = qMin( wave_height/2 - point1.y(), wave_height/2 - point2.y() );
        int i_yH    = qMax( wave_height/2 - point1.y(), wave_height/2 - point2.y() );

        tMin = (i_xMin - hAttr.gnd) * hAttr.tInc;
        tMax = (i_xMax - hAttr.gnd) * hAttr.tInc;

        //! 转换成以屏幕中间为参考，向上为+，向下为-的坐标系。
        thresholdH  = i_yH * nScale / scr_vdiv_dots - nOffset;
        thresholdL  = i_yL * nScale / scr_vdiv_dots - nOffset;

        pCfg->a_zone[i].tMin       = tMin;
        pCfg->a_zone[i].tMax       = tMax;
        pCfg->a_zone[i].thresholdH = thresholdH;
        pCfg->a_zone[i].thresholdL = thresholdL;
    }

    //! apply
    serviceExecutor::post( serv_name_trigger_zone,
                           servZoneTrig::cmd_zone_apply,
                           1);
}

void CAppTrigger::on_zone_selcet(int index)
{

    if(ZONE_MENU_TRIG_A == index)
    {
        serviceExecutor::post(serv_name_trigger_zone, CMD_SERVICE_ACTIVE, 1);
        bool en =false;
        query(serv_name_trigger_zone, MSG_TRIG_ZONE_A_ENABLE, en);
        if(!en)
        {
            serviceExecutor::post(serv_name_trigger_zone, MSG_TRIG_ZONE_A_ENABLE, true);
        }

    }
    else if(ZONE_MENU_TRIG_B == index)
    {
        serviceExecutor::post(serv_name_trigger_zone, CMD_SERVICE_ACTIVE, 1);

        bool en =false;
        query(serv_name_trigger_zone, MSG_TRIG_ZONE_B_ENABLE, en);
        if(!en)
        {
            serviceExecutor::post(serv_name_trigger_zone, MSG_TRIG_ZONE_B_ENABLE, true);
        }
    }
}


void CAppTrigger::on_zone_zoom_change()
{
    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__;

    if(m_bZoom)
    {
        tozoneConver(horizontal_view_zoom, horizontal_view_main);
        onZoneAppChange();
    }
}

void CAppTrigger::on_zone_main_change()
{
    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__;

    if(m_bZoom)
    {
        tozoneConver(horizontal_view_main, horizontal_view_zoom);
    }
    onZoneAppChange();
}
