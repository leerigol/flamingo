#ifndef _APP_TRIGGER_
#define _APP_TRIGGER_

#include "../../menu/rmenus.h"
#include "../appmain/cappmain.h"
#include "../../service/servtrig/servtrig.h"
#include "triglevel.h"
#include "trigcode.h"
#include "trigZone.h"

#include "../../menu/dsownd/wndtrigcfg.h"

using namespace menu_res ;
class CAppTrigger : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppTrigger();

    enum
    {
        Main_Level_A = 0,
        Main_Level_B,
        Zoom_Level_A,
        Zoom_Level_B,
        All_Level,
    };

public:
    static bool        getTimeout();

public:
    static QString      m_ChanNames[];
    const static int    m_nMaxDiv = 5; /* Level range for max div*/

public:
    virtual void   setupUi( CAppMain *pMain );
    virtual void   retranslateUi();
    virtual void   buildConnection();
    virtual void   resize();
    virtual void   linkService();
    virtual void   registerSpy();

public:
    int            onSourceChanged(AppEventCause);
                   
    int            onLevelChanged(AppEventCause);
    int            onLevelSelfChanged(AppEventCause event);
    int            onLaLevelChanged(AppEventCause);
                   
    int            onModeChanged(AppEventCause);
    int            onHoldoff(AppEventCause);
                   
    int            onZoom(AppEventCause);
    int            onXY(AppEventCause);
    int            onDeActive(int);
    int            onInActive();
    DsoErr         onItemFocus(int);
                   
    int            onCodeChanged(int);
                   
    void           onGetSweep();
                   
    int            onCoupling(AppEventCause);
    int            onSlopeChanged(AppEventCause);
                   
    void           onUpdateWndCfg();
    void           onLicenseOpt();

    DsoErr         onEnableZoneX(    AppEventCause);
    DsoErr         onIntersectZoneX( AppEventCause);
    DsoErr         onZoneSourceX(    AppEventCause);
    void           onZoneServChange(AppEventCause);
    void           onZoneAppChange( void);

    int            toZoneIndex(int i);
    void           tozoneConver(HorizontalView src, HorizontalView dest);

private:
    void           onShow(bool is_self = true);
    void           onHide(bool is_self = true);
    void           setLevelLineColor(Chan ch);
    void           setCode( TriggerMode mode );
    void           setTimeout();

private:
    RDsoTrigInfo         *m_pTrigInfo;
    CTriggerLevel        *m_pLevel[All_Level];
    CSource              *m_pSource;
    QTimer               *m_pTimer;
    static CAppTrigger   *m_pAppTrigger;
    CCodeKeyboard            *m_pCode;

    bool                  m_bXYMode;
    bool                  m_bXYFull; //!xy 全屏/半屏
    bool                  m_bZoom;
    static bool           m_bTimeout;

    TriggerMode           m_mode;
    QString               m_servName;
    TriggerSweep          m_sweep;                   
    Coupling              m_coup;
                         
private:                 
    wndTrigCfg            *m_pMainTrigCfg;
    trigZoneWnd           *m_pTrigZone;
    trigZoneWnd           *m_pTrigZoneZoom;

protected Q_SLOTS:
    void onTimeout(void);

    void postCurrCode(int value);
    void postCurrBit(int bitNum);
    void postAllBit(int s);

    void on_trig_header_click( QWidget *pCaller = NULL );
    void on_trig_content_click(QWidget *pCaller = NULL  );

    void selectLevel(int ab);
    void onStartTrigger();

    void on_main_level_a_real(DsoReal  real);
    void on_main_level_b_real( DsoReal real);
    void on_sweep( int sweep);
    void on_tune( int id );

    void on_zone_selcet(int index);
    void on_zone_zoom_change(void);
    void on_zone_main_change(void);
};


#endif // TRIGGER

