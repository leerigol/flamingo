#include "trigZone.h"
#include <QMouseEvent>
#include <QPainter>
#include <QDebug>

#define screen_center__phy_left (23)
#define screen_center_phy_top   (56)

#if 0
#define LOG()    qDebug()<<__FILE__<<__LINE__<<__FUNCTION__
#else
#define LOG()    QT_NO_QDEBUG_MACRO()
#endif

touchZone::touchZone(TouchZone *touch, QWidget *parent):QWidget(parent)
{
    Q_ASSERT(touch != NULL);
    m_pTouchZone  = touch;

    m_index       = 0;
    m_dragging    = false;

    resize(parent->width(), parent->height());
}

void touchZone::setupUi()
{
    LOG();
    m_zoneMap[ZONE_MENU_TRIG_A] = QRect();
    m_zoneMap[ZONE_MENU_TRIG_B] = QRect();
    m_pTouchZone->setMenuVisible(ZONE_MENU_TRIG_A, true);
    m_pTouchZone->setMenuVisible(ZONE_MENU_TRIG_B, true);

    connect( m_pTouchZone, SIGNAL( sig_mouse_move_event(QPoint) ),
             this, SLOT( on_mouse_move_event(QPoint) ) );

    connect( m_pTouchZone, SIGNAL( sig_mouse_pressEvent(QPoint) ),
             this, SLOT( on_mouse_pressEvent(QPoint) ) );

    connect( m_pTouchZone, SIGNAL( sig_mouse_releaseEvent(QPoint) ),
             this, SLOT( on_mouse_releaseEvent(QPoint) ) );

    connect( m_pTouchZone, SIGNAL( sig_menu_activated(int) ),
             this, SLOT( on_zone_selcet(int) ) );
}

void touchZone::paintEvent(QPaintEvent *)
{
    emit sig_zone_paintEvent();
}

void touchZone::findZone(QPoint point)
{
    LOG();
    m_dragging = false;

    QMap<int, QRect>::const_iterator i = m_zoneMap.constBegin();
    while (i != m_zoneMap.constEnd())
    {
        QRect zoneItem = i.value();
        int   index    = i.key();

        if( zoneItem.contains(point) )
        {
            if( m_zoneEn.value(index) )
            {
                m_dragging = true;
                m_index    = index;
                m_drawZone = zoneItem;
                break;
            }
        }
        ++i;
    }
}

bool touchZone::mouseEventValid(QPoint pos)
{
    if( !this->isVisible() )
    {
        return false;
    }

    QRect  wnd = this->geometry();
    if(!wnd.contains(pos))
    {
        return false;
    }

    return true;
}


void touchZone::getZoneCoords(int index, QPoint &p1, QPoint &p2)
{
    LOG()<<index<<m_zoneMap.count();
    int x1 = 0, x2 = 0, y1 = 0, y2 = 0;

    m_zoneMap.value(index).getCoords(&x1, &y1, &x2,  &y2);

    p1 = QPoint(x1,y1);
    p2 = QPoint(x2,y2);
}

void touchZone::setZoneCoords(int index, QPoint p1, QPoint p2)
{
    LOG()<<index<<m_zoneMap.count();
    QRect rect;
    rect.setCoords(p1.x(), p1.y(), p2.x(), p2.y());
    m_zoneMap[index] = rect;
    update();
}

QMap<int, QRect> touchZone::getZoneMap()
{
    return m_zoneMap;
}


void touchZone::setZoneEn(int index, bool en)
{
    m_zoneEn[index] = en;
    update();
}



void touchZone::on_zone_selcet(int index)
{
    if( !(this->isVisible())||
            !(this->geometry().contains(m_pTouchZone->getZone().topLeft()) )
            )
    {
        return;
    }

    LOG()<<index;
    if( (ZONE_MENU_TRIG_A != index) && (ZONE_MENU_TRIG_B != index) )
    {
        return;
    }

    m_index = index;

    m_pTouchZone->rectHide();
    m_zoneMap[m_index] = m_drawZone;

    emit sig_zone_change();
    emit sig_zone_selcet(index);
    update();
}

void touchZone::on_mouse_move_event(QPoint pos)
{
    if(!mouseEventValid(pos))
    {
        return ;
    }

    QRect  rect = m_pTouchZone->getZone();
    if(!m_dragging) //!新绘制区域
    {
        m_drawZone = rect ;
    }
    else //!拖动
    {
        m_drawZone.moveCenter( pos - m_position );
        m_zoneMap[m_index] = m_drawZone;
    }

    update();
}

void touchZone::on_mouse_pressEvent(QPoint pos)
{
    if(!mouseEventValid(pos))
    {
        return ;
    }

    findZone( pos );

    if(!m_dragging) //!新绘制区域
    {
        m_pTouchZone->rectShow();
    }
    else//!拖动
    {
        m_pTouchZone->rectHide();
        m_position = pos - m_drawZone.center();
    }
    update();
}

void touchZone::on_mouse_releaseEvent(QPoint pos)
{
    if(!mouseEventValid(pos))
    {
        return ;
    }

    QRect  rect = m_pTouchZone->getZone();
    if(!m_dragging) //!新绘制区域
    {
        if( rect.topLeft() != rect.bottomRight() )
        {
            m_pTouchZone->menuShow();
        }
        else
        {
            m_pTouchZone->menuHide();
            m_pTouchZone->rectHide();
        }
    }
    else//!拖动
    {
         m_pTouchZone->menuHide();

        if( rect.topLeft() != rect.bottomRight() )
        {
            emit sig_zone_change();
        }
        else
        {
            emit sig_zone_selcet(m_index);
        }
    }

    update();
}


dispZone::dispZone(QWidget *parent):QWidget(parent)
{
}

void dispZone::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen pen;
    QMap<int, QRect>::const_iterator i = m_zoneMap.constBegin();

    QFont font;
    QFontMetrics fm(font);

    QString strTitle;

    while (i != m_zoneMap.constEnd())
    {
        int   key  = i.key();
        if(m_enMap.value(key))
        {
            //! 画矩形
            pen.setColor(QColor(0,0,0,0));
            pen.setStyle(Qt::NoPen);
            painter.setPen(pen);

            QRect rect = i.value();
            painter.setBrush(QBrush(m_colorMap.value(key), m_brushMap.value(key)) );
            painter.drawRect(rect);

            //! 画矩形标题
            int x1 = 0, x2 = 0, y1 = 0, y2 = 0;
            rect.getCoords(&x1, &y1, &x2, &y2);
            rect.setCoords(qMin(x1,x2), qMin(y1,y2), qMax(x1,x2), qMax(y1,y2) );

            pen.setColor(m_colorMap.value(key));
            pen.setStyle(Qt::SolidLine);
            painter.setPen(pen);

            //strTitle = sysGetString(i.key(), QString("Zone %1").arg(i.key()));
            strTitle = (ZONE_MENU_TRIG_B == i.key())? " B" : " A";

            QRect  wordRect  = fm.boundingRect(rect, Qt::TextWrapAnywhere, strTitle);
            //qDebug()<<__FILE__<<__LINE__<<wordRect.width()<<wordRect.height();
            //! 防止区域过小，一个字符显示不全
            wordRect.setWidth(  qMax(16,wordRect.width())  );
            wordRect.setHeight( qMax(23,wordRect.height()) );

            painter.drawText( wordRect, Qt::TextWrapAnywhere, strTitle );
        }
        ++i;
    }
}

void dispZone::setRect(QMap<int, QRect> rectMap)
{
    m_zoneMap = rectMap;
    update();
}

void dispZone::setZoneEn(int index, bool en)
{
    m_enMap[index] = en;
}

void dispZone::setZoneColor(int index, QColor color)
{
    m_colorMap[index] = color;
    update();
}

void dispZone::setZoneBrush(int index, Qt::BrushStyle brush)
{
    m_brushMap[index] = brush;
    update();
}

trigZoneWnd::trigZoneWnd(TouchZone *touchParent,
                         QWidget   *viewParent,
                         QWidget   *dispParent)
{
    Q_ASSERT(touchParent != NULL);

    m_pViewWnd = new viewZone(touchParent);
    Q_ASSERT(m_pViewWnd != NULL);

    m_pTouchWnd  = new touchZone(touchParent, viewParent);
    Q_ASSERT(m_pTouchWnd != NULL);

    m_pDispWnd   = new dispZone(dispParent);
    Q_ASSERT(m_pDispWnd != NULL);

    connect( m_pViewWnd, SIGNAL(sig_zone_view_change(void)),
             this, SLOT(on_zone_view_change(void)) );

    connect( m_pViewWnd, SIGNAL(sig_zone_view_change(void)),
             this, SIGNAL(sig_zone_view_change(void)) );

    connect( m_pTouchWnd, SIGNAL(sig_zone_paintEvent(void)),
             this, SLOT(on_zone_paintEvent(void)) );

    connect( m_pTouchWnd, SIGNAL(sig_zone_change(void)),
             this, SIGNAL(sig_zone_change(void)) );

    connect( m_pTouchWnd, SIGNAL(sig_zone_selcet(int)),
             this, SIGNAL(sig_zone_selcet(int)) );
}

void trigZoneWnd::setZoneColor(int index, QColor color)
{
    m_pDispWnd->setZoneColor(index, color);
}

void trigZoneWnd::setupUi()
{
    m_pTouchWnd->setupUi();
}

void trigZoneWnd::show()
{
    m_pDispWnd->show();
    m_pTouchWnd->show();
    m_pViewWnd->setVisible(true);
}

void trigZoneWnd::setView(DsoView view)
{
    Q_ASSERT(m_pViewWnd != NULL);
    sysAttachUIView(m_pViewWnd, view);

    m_pViewWnd->refresh();
}


void trigZoneWnd::setEnableZoneX(int index, bool en)
{
    LOG()<<"index:"<<index<<"en:"<<en;
    m_pDispWnd->setZoneEn(index, en);
    m_pTouchWnd->setZoneEn( index, en );
}

void trigZoneWnd::setIntersectZoneX(int index, bool b)
{
    if(b)
    {
        m_pDispWnd->setZoneBrush(index, Qt::Dense7Pattern);
    }
    else
    {
        m_pDispWnd->setZoneBrush(index, Qt::BDiagPattern);
    }
}

void trigZoneWnd::getZoneCoords(int index, QPoint &p1, QPoint &p2)
{
    Q_ASSERT(m_pTouchWnd != NULL);
    QPoint point1, point2;
    m_pTouchWnd->getZoneCoords(index, point1, point2);

    //! 计算窗体缩放比例
    QRect phyRect  = m_pViewWnd->m_phyRect;
    QRect viewRect  = m_pViewWnd->m_viewRect;
    float yRatio = (float)viewRect.height() / phyRect.height();
    float xRatio = (float)viewRect.width()  / phyRect.width();

    //! 换算wiew坐标
    int   y1 = ( point1.y() - phyRect.top() ) * yRatio;
    int   y2 = ( point2.y() - phyRect.top() ) * yRatio;
    int   x1 = point1.x() * xRatio;
    int   x2 = point2.x() * xRatio;

    p1 = QPoint(x1,y1);
    p2 = QPoint(x2,y2);
}

void trigZoneWnd::setZoneCoords(int index, QPoint p1, QPoint p2)
{
    Q_ASSERT(m_pTouchWnd != NULL);
    //! 计算窗体缩放比例
    QRect phyRect  = m_pViewWnd->m_phyRect;
    QRect viewRect  = m_pViewWnd->m_viewRect;
    float yRatio = (float)phyRect.height() / viewRect.height();
    float xRatio = (float)phyRect.width()  / viewRect.width();

  /*qDebug()<<"\n================-------------------------------------";
    qDebug()<<__FILE__<<__LINE__<<"yRatio:"<<yRatio<<"XRatio:"<<xRatio;
    qDebug()<<__FILE__<<__LINE__<<"phyRect:"<<phyRect<<"viewRect:"<<viewRect;
    qDebug()<<__FILE__<<__LINE__<<"p1:"<<p1<<"p2:"<<p2;
    qDebug()<<"-------------------------------------------===========\n"; */

    //! 换算phy坐标
    int   y1 =  p1.y()* yRatio + phyRect.top() ;
    int   y2 =  p2.y()* yRatio + phyRect.top() ;
    int   x1 =  p1.x() * xRatio;
    int   x2 =  p2.x() * xRatio;
    m_pTouchWnd->setZoneCoords(index, QPoint(x1, y1), QPoint(x2, y2));
}

void trigZoneWnd::setEnable(bool en)
{
    m_pTouchWnd->setVisible(en);
    m_pDispWnd->setVisible(en);
}

void trigZoneWnd::on_zone_paintEvent(void)
{
    Q_ASSERT(m_pDispWnd != NULL);
    QMap<int, QRect>  dispMap;

    QRect phyRect  =  m_pViewWnd->m_phyRect;
    QMap<int, QRect>  rectMap = m_pTouchWnd->getZoneMap();
    QMap<int, QRect>::const_iterator i = rectMap.begin();
    while (i != rectMap.constEnd())
    {
        QPoint point1 =   i.value().topLeft();
        QPoint point2 =   i.value().bottomRight();
        //LOG()<< point1 <<point2;
        point1.setX( point1.x() - phyRect.left() );
        point2.setX( point2.x() - phyRect.left() );
        point1.setY( point1.y() - phyRect.top()  );
        point2.setY( point2.y() - phyRect.top()  );

        //LOG()<< phyRect.top() <<phyRect.left()  << point1 <<point2;

        dispMap[i.key()] = QRect(point1, point2);
        ++i;
    }
    m_pDispWnd->setRect(dispMap);
}

void trigZoneWnd::on_zone_view_change()
{
    Q_ASSERT(m_pViewWnd != NULL);
    Q_ASSERT(m_pDispWnd != NULL);
    Q_ASSERT(m_pTouchWnd != NULL);

    QRect phyRect  = m_pViewWnd->m_phyRect;

    //! 计算窗体缩放比例
    float yRatio = (float)phyRect.height() / m_pTouchWnd->height();
    float xRatio = (float)phyRect.width() / m_pTouchWnd->width();

    //! 缩放窗体大小到新位置
    m_pDispWnd->resize(phyRect.width(),  phyRect.height());
    m_pDispWnd->move(phyRect.left() + screen_center__phy_left,
                     phyRect.top() + screen_center_phy_top);

    m_pTouchWnd->resize(phyRect.width(), phyRect.height());
    m_pTouchWnd->move(   phyRect.left(),  phyRect.top() );

    //! 缩放窗体内的矩形区域 适应 新窗体大小

    QPoint topLeft;
    QPoint bottomRight;

    QMap<int, QRect> zoneMap = m_pTouchWnd->getZoneMap();

    QMap<int, QRect>::const_iterator i = zoneMap.constBegin();
    while (i != zoneMap.constEnd())
    {
        int index = i.key();

        m_pTouchWnd->getZoneCoords(index, topLeft, bottomRight);

        LOG()<<"yRatio:"<<yRatio;
        int   y1 = topLeft.y()     * yRatio;
        int   y2 = bottomRight.y() * yRatio;

        int   x1 = topLeft.x()     * xRatio;
        int   x2 = bottomRight.x() * xRatio;

        m_pTouchWnd->setZoneCoords(index, QPoint(x1,y1), QPoint(x2,y2));

        ++i;
    }
}


viewZone::viewZone(QWidget *parent):QWidget(parent)
{
    menu_res::RDsoUI::m_viewRect.setRect(0,0,wave_width,wave_height);
    m_viewRect = menu_res::RDsoUI::m_viewRect;
    m_phyRect  = menu_res::RDsoUI::m_phyRect;
}

void viewZone::setWidgetVisible(bool b)
{
    Q_UNUSED(b)
    LOG();
    update();
}

void viewZone::refresh()
{
    m_viewRect = menu_res::RDsoUI::m_viewRect;
    m_phyRect  = menu_res::RDsoUI::m_phyRect;

    LOG()<<"width:"<<menu_res::RDsoUI::m_viewRect.width()
        <<"height:"<<menu_res::RDsoUI::m_viewRect.height();
    LOG()<<"phy-bottomRight:" <<menu_res::RDsoUI::m_phyRect.bottomRight()
        <<"phy-topLeft:"     <<menu_res::RDsoUI::m_phyRect.topLeft();
    LOG()<<"view-bottomRight:"<<menu_res::RDsoUI::m_viewRect.bottomRight()
        <<"view-topLeft:"    <<menu_res::RDsoUI::m_viewRect.topLeft();

    emit this->sig_zone_view_change();
}
