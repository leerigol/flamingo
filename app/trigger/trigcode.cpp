#include "trigcode.h"
#include "../../meta/crmeta.h"
#include "../../service/servtrig/servtrig.h"
#define rowBitMax       (32)

CCodeKeyboard::CCodeKeyboard(QWidget *parent):uiWnd(parent)
{
    this->setObjectName("triggercode");

    m_nCurrBit = 10;
    isHexTrue = false;
    isHasOptMso = false;
    touchSplitX = 0;
    m_nCodeMode = CODE_TYPE_SOURCE;

    m_nBackColor = QColor(35,35,35,255);
    m_nSelColor  = QColor(18,85,159,255);
    m_nForeColor = QColor(192,192,192,255);

    setAttr(wnd_moveable);

    this->move(180,420);
}

CCodeKeyboard::~CCodeKeyboard()
{

}

void CCodeKeyboard::setupUi()
{
    r_meta::CAppMeta meta;
    mPatStyle.loadResource("trigcode/code1/", meta);
    mCanStyle.loadResource("trigcode/code2/", meta);
    loadResource("trigcode/");

    m_pCodeBoard = new CCodeBoard();
    m_pCodeBoard->setGeometry(260,200,405,286);
    m_pCodeBoard->hide();

    buildConnection();
}

void CCodeKeyboard::loadResource(QString path)
{
    r_meta::CAppMeta meta;
    meta.getMetaVal(path+"patsize1",    m_nPatSize1);
    meta.getMetaVal(path+"patsize2",    m_nPatSize2);
    meta.getMetaVal(path+"line2size",   m_nLine2Size);

    meta.getMetaVal(path+"risepic",   mRisePic);
    meta.getMetaVal(path+"fallpic",   mFallPic);
    meta.getMetaVal(path+"patbg",     mPatBg);

}

void CCodeKeyboard::buildConnection()
{
    connect(m_pCodeBoard,SIGNAL(setCurrBitRise()),this,SLOT(set_rise()));
    connect(m_pCodeBoard,SIGNAL(setCurrBitFall()),this,SLOT(set_fall()));

    connect(m_pCodeBoard,SIGNAL(gotoPreBitSig()),this,SLOT(gotoPreBit()));
    connect(m_pCodeBoard,SIGNAL(gotoNextBitSig()),this,SLOT(gotoNextBit()));
    connect(m_pCodeBoard,SIGNAL(setCurrBitValue(int)),this,SLOT(setCurrBitValue(int )));
    connect(m_pCodeBoard,SIGNAL(setAllSig()),this,SLOT(set_all()));
    connect(m_pCodeBoard,SIGNAL(setCurrBitX()),this,SLOT(set_X()));
}

void CCodeKeyboard::drawHex(QRect &r, QPainter &p, int v, bool current)
{
    if(current)
    {
        QRect sel = r;
        sel.moveTo(sel.x(), sel.y()+4);
        sel.setWidth(11);
        sel.setHeight(20);
        p.fillRect(sel, m_nSelColor);
    }

    QString str = "X";
    if(v != Trigger_Code_X)
    {
        str = QString("%1").arg(v,1,16).toUpper();
    }
    p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, str);
}

void CCodeKeyboard::drawCode(QRect &r, QPainter &p, int v, bool current)
{
    if(current)
    {
        QRect sel = r;
        sel.moveTo(sel.x(), sel.y()+9);
        sel.setWidth(13);
        sel.setHeight(22);

        p.fillRect(sel, m_nSelColor);
    }

    if(v == Trigger_pat_x)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "X");
    }
    else if(v == Trigger_pat_h)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "1");
    }
    else if(v == Trigger_pat_l)
    {
        p.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "0");
    }
    else if( v == Trigger_pat_rise)
    {
        QPoint point;
        QImage image(mRisePic);
        point.setX(r.x()+1);
        point.setY( r.y() +  (r.height() - image.height()) / 2 + 2);

        if(current)
        {
            RControlStyle::renderBg( image, m_nSelColor );
        }
        else
        {
            RControlStyle::renderBg( image, m_nBackColor );
        }
        p.drawImage( point, image );
    }
    else if(v == Trigger_pat_fall)
    {
        QPoint point;
        QImage image(mFallPic);
        point.setX(r.x()+1);
        point.setY(r.y() + ( r.height() - image.height()) / 2 + 2);
        if(current)
        {
            RControlStyle::renderBg( image, m_nSelColor );
        }
        else
        {
            RControlStyle::renderBg( image, m_nBackColor );
        }
        p.drawImage( point, image );
    }
}


/*!
 * \brief CTrigCode::drawByteCode  按字节显示码型,高位不足时二进制补0
 * \param r
 * \param painter
 */
void CCodeKeyboard::drawByteCode(QRect &r, QPainter &painter)
{
//    const int rowBitMax =  24;

    int bitCount = m_nCode.size();
    if(bitCount < 1) return;

    r.moveTo(r.x() + 10, r.y());
    r.setHeight(30);


    //draw binary format data
    painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "Bin");
    r.moveTo(r.x() + 50, r.y());
    int x = r.x();
    int y = r.y();

//    qDebug()<<__FUNCTION__<<__LINE__<< bitCount<< "---" << m_nCurrBit;
    for(int i=0; i<bitCount; i++)
    {
        int val = list_at( m_nCode,i);

        /** draw bin */
        drawBin(r, painter, val,  (m_nCurrBit == i)?true:false);

        //!换行 或者 往后移动一位 显示
        if( (i != 0) && (0 == (i%(rowBitMax - 1)) ) )
        {
            r.moveTo(x, r.y()+30 );
        }
        else
        {
            r.moveTo(r.x()+14, r.y());
        }
        //!每4bit插入一个分割
        if( (0 == ((i+1)%4)) )
        {
            r.moveTo(r.x()+4, r.y());
        }
    }

    /** 将4个BIN数据 转换为 1个HEX数据. */
    m_nHexCode.clear();
    bool bHexMask = true;
    int  data     = 0;
    for(int i = bitCount-1; i>=0; i--)
    {
         int val = list_at( m_nCode,i);

         if(val == Trigger_Code_H)
         {
             data |= (1<< (3-(i%4)) );
         }
         else if(val == Trigger_Code_L)
         {
             data &= (~(1<<(3-(i%4))));
         }
         else
         {
             bHexMask = false;
         }

         if( (0 == (i%4)) || (i == 0) )
         {
              m_nHexCode.insert(0, bHexMask? data : Trigger_Code_X );
              bHexMask = true;
              data = 0;
         }
    }

    //显示HEX数据
    r.moveTo(x + 340, y);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "Hex");
    r.moveTo(r.x() + 50, r.y());

    x = r.x();
    y = r.y();

    for(int i=0; i<m_nHexCode.size(); i++)
    {
        //!显示HEX 如果当前bit位在HEX位置 则将对应位置加入阴影
        drawHex(r, painter, list_at(m_nHexCode,i), (i == (m_nCurrBit-bitCount) )?true:false);

        //!每 1 BYTE 中间插入4个像素的分割
        if(( (i+1)%2) == 0 )
        {
            r.moveTo(r.x()+4, r.y());
        }

        //!超过一行最大字节数 换行显示
        if( ((i+1)%(rowBitMax/4)) == 0 )
        {
            r.moveTo(x, y+30);
        }
        else
        {
            r.moveTo(r.x()+14, r.y());
        }
    }
}

/*!
 * \brief CTrigCode::drawBitCode  按bit显示码型,高位不足时二进制空缺
 * \param r
 * \param painter
 */
void CCodeKeyboard::drawBitCode(QRect &r, QPainter &painter)
{
    int bitCount = m_nCode.size();
    if(bitCount < 1) return;

    r.moveTo(r.x() + 10, r.y());
    r.setHeight(27);

    //draw binary format data
    painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "Bin");
    r.moveTo(r.x() + 45, r.y());
    int x = r.x();
    int y = r.y();
    r.setWidth(9);

//    qDebug()<<__FILE__<<__FUNCTION__<<__LINE__<< bitCount<< "---" << m_nCurrBit;

    int align = 4 - ((bitCount)%4);//!最高位缺 align 位

    for(int i=0; i<bitCount; i++)
    {
        int val = list_at( m_nCode,i);
        /** draw bin */
        mRectList.append(r);
        drawBin(r, painter, val,  (m_nCurrBit == i)?true:false);

        //!换行 或者 往后移动一位 显示
        if( (i != 0) && (0 == ((i+1)%rowBitMax ) ) )
        {
            r.moveTo(x, r.y()+25 );
        }
        else
        {
            r.moveTo(r.x()+9, r.y());
            //!每4bit插入一个分割
            if(0 == ((i+align+1)%4))
            {
                r.moveTo(r.x()+4, r.y());
            }
        }
    }

    //显示HEX数据
    r.moveTo(x + 338, y);
    r.setWidth(100);
    touchSplitX = r.x();
    painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "Hex");
    r.moveTo(r.x() + 44, r.y());

    x = r.x();
    y = r.y();
    r.setWidth(12);

    for(int i=0; i<m_nHexCode.size(); i++)
    {
//        qDebug()<<__FUNCTION__<<__LINE__<<i<< r;
        //!显示HEX 如果当前bit位在HEX位置 则将对应位置加入阴影        
        mRectList.append(r);
//        qDebug()<<__FUNCTION__<<__LINE__<<i<< r<<mRectList.count();
        drawHex(r, painter, list_at(m_nHexCode,i), (i == (m_nCurrBit-bitCount) )?true:false);

        //!每 1 BYTE 中间插入4个像素的分割
        if(m_nHexCode.count()%2 == 0){
            if(( (i+1)%2) == 0 )
            {
                r.moveTo(r.x()+4, r.y());
            }
        }else{
            if(( i%2) == 0 )
            {
                r.moveTo(r.x()+4, r.y());
            }
        }

        //!超过一行最大字节数 换行显示
        if( ((i+1)%(rowBitMax/4)) == 0  )
        {
            r.moveTo(x, y+25);
        }
        else
        {
            r.moveTo(r.x()+11, r.y());
        }
    }

}

/*!
 * \brief CTrigCode::drawBin  显示一位bit位: X/1/0
 * \param r
 * \param painter
 * \param v                   X/1/0
 * \param current             当前选中位 深色标记
 */
void CCodeKeyboard::drawBin(QRect &r, QPainter &painter, int v, bool current)
{
    if(current)
    {
        QRect sel = r;
        sel.moveTo(sel.x(), sel.y()+4);
        sel.setWidth(9);
        sel.setHeight(20);
        painter.fillRect(sel, m_nSelColor);
    }

    if(v == Trigger_Code_H)
    {
        painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "1");
    }
    else if(v == Trigger_Code_L)
    {
        painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "0");
    }
    else
    {
        painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "X");
    }
}

void CCodeKeyboard::painterBackground(QPainter &p)
{
    CCode_Style style;
    if (CODE_TYPE_SOURCE == m_nCodeMode)
    {
        style = mPatStyle;
        if(isHasOptMso){
            doPainter(p, style);
        }else{
            QImage img;
            bool   b;
            b = img.load(mPatBg);
            Q_ASSERT( b == true );
            p.drawImage(0, 0, img);
        }
    }else{
        style = mCanStyle;
        doPainter(p, style);
    }
}

void CCodeKeyboard::doPainter(QPainter &p, CCode_Style style)
{
    QImage img, img1, img2;
    bool b;
    b = img.load(style.mTL);
    int widthTL = img.width();
    int heightTL = img.height();
    Q_ASSERT( b == true );
    p.drawImage(0, 0, img);

    b = img.load(style.mTm1);
    Q_ASSERT( b == true );
    for(int i=0; i<style.mMidWidth1; i++)
        p.drawImage(widthTL+i, 0, img);

    b = img.load(style.mTm2);
    Q_ASSERT( b == true );
    int widthMtm2 = img.width();
    p.drawImage(style.mMidWidth1+widthTL, 0, img);

    b = img.load(style.mTm3);
    Q_ASSERT( b == true );
    for(int i=0; i<style.mMidWidth2; i++)
        p.drawImage(style.mMidWidth1+widthTL+widthMtm2+i, 0, img);

    b = img.load(style.mTR);
    Q_ASSERT( b == true );
    p.drawImage(style.mMidWidth1+widthTL+widthMtm2+style.mMidWidth2, 0, img);

    b = img.load(style.mBL);
    int heightBL = img.height();
    int tempH = this->geometry().height()-heightBL;
    Q_ASSERT( b == true );
    p.drawImage(0, tempH, img);

    b = img.load(style.mLm);
    Q_ASSERT( b == true );
    b = img1.load(style.mCent);
    Q_ASSERT( b == true );
    b = img2.load(style.mRm);
    Q_ASSERT( b == true );
    int midHeight = this->geometry().height()-heightTL-heightBL;
    for(int i=0; i<midHeight; i++){
        p.drawImage(0, heightTL+i, img);
        p.drawImage(style.mMidWidth1+widthTL, heightTL+i, img1);
        p.drawImage(style.mMidWidth1+widthTL+widthMtm2+style.mMidWidth2, heightTL+i, img2);
    }

    b = img.load(style.mBm1);
    Q_ASSERT( b == true );
    for(int i=0; i<style.mMidWidth1; i++)
        p.drawImage(widthTL+i, tempH, img);
    p.fillRect(widthTL, heightTL, style.mMidWidth1, midHeight, style.mBgColor);

    b = img.load(style.mBm2);
    Q_ASSERT( b == true );
    p.drawImage(style.mMidWidth1+widthTL, tempH, img);

    b = img.load(style.mBm3);
    Q_ASSERT( b == true );
    for(int i=0; i<style.mMidWidth2; i++)
        p.drawImage(style.mMidWidth1+widthTL+widthMtm2+i, tempH, img);
    p.fillRect(style.mMidWidth1+widthTL+widthMtm2, heightTL, style.mMidWidth2, midHeight, style.mBgColor);

    b = img.load(style.mBR);
    Q_ASSERT( b == true );
    p.drawImage(style.mMidWidth1+widthTL+widthMtm2+style.mMidWidth2, tempH, img);

}

void CCodeKeyboard::mouseReleaseEvent(QMouseEvent *e)
{
    QPoint p = e->pos();

    if (CODE_TYPE_SOURCE == m_nCodeMode)
    {
        for(int i=0; i<mRectList.count(); i++)
        {
            if(mRectList.at(i).contains(p))
            {
                emit postCurrBitSig(i+1);
                m_pCodeBoard->setCode(m_nCode, m_nCode.count(), i);
                break;
            }
        }
        m_pCodeBoard->setBtnDisabled(1);
        m_pCodeBoard->setCurrBitBtnFocus(1);
    }
    else
    {
        for(int i=0; i<mRectList.count(); i++)
        {
            if(mRectList.at(i).contains(p))
            {
                emit postCurrBitSig(i-m_nCurrBit);

                if(i<m_nCode.count())
                {
                    m_pCodeBoard->setCode(m_nCode, m_nCode.count(), i);
                    m_pCodeBoard->setBtnDisabled(1);
                    m_pCodeBoard->setCurrBitBtnFocus(list_at(m_nCode, i));
                }
                else
                {
                    m_pCodeBoard->setCode(m_nHexCode, m_nCode.count(), i);
                    if(i == m_nCode.count())
                        m_pCodeBoard->setBtnDisabled(m_nCode.count()%4);
                    else
                        m_pCodeBoard->setBtnDisabled(0);
                    m_pCodeBoard->setCurrBitBtnFocus(list_at(m_nHexCode,i-m_nCode.count()));
                }
                m_pCodeBoard->show();
                break;
            }
        }

        if(m_pCodeBoard->isHidden())
        {
            if(e->pos().x() < touchSplitX)
            {
                emit postCurrBitSig(0 - m_nCurrBit);

                m_pCodeBoard->setCode(m_nCode, m_nCode.count(), 0);
                m_pCodeBoard->setBtnDisabled(1);
                m_pCodeBoard->setCurrBitBtnFocus(1);
            }
            else
            {
                emit postCurrBitSig(m_nCode.count() - m_nCurrBit);

                m_pCodeBoard->setCode(m_nHexCode, m_nCode.count(), m_nCode.count());
                m_pCodeBoard->setBtnDisabled(m_nCode.count()%4);
                m_pCodeBoard->setCurrBitBtnFocus(1);
            }
        }
    }
    m_pCodeBoard->show();
}

void CCodeKeyboard::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen   pen;

    //! font
    QFont font( qApp->font().family(), 16);
    painter.setFont( font );

    QRect r = this->geometry();
    r.moveTo(0,0);

    painterBackground(painter);

    mRectList.clear();
    pen.setColor(m_nForeColor);
    painter.setPen(pen);

    if (CODE_TYPE_SOURCE == m_nCodeMode)
    {
        if(m_nCode.size() < all_pattern_src) return;

        r.moveTo(r.x() + 7, r.y());
        painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "CH1");
        r.moveTo(r.x() + 55, r.y());
        r.setWidth(14);
        for(int i=0; i<4; i++)
        {
            mRectList.append(r);
            if(m_nCurrBit == i)
            {
                drawCode(r, painter, list_at( m_nCode,i), true);
            }
            else
            {
                drawCode(r, painter, list_at( m_nCode,i));
            }
            r.moveTo(r.x()+14, r.y());
        }
        r.moveTo(r.x()+18, r.y());
        r.setWidth(100);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "CH4");

        if(isHasOptMso){
            r.moveTo(r.x()+52, r.y());
            painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "D0");
            r.moveTo(r.x() + 55, r.y());
            r.setWidth(14);
            for(int i=4; i<20; i++)
            {
                mRectList.append(r);
                if(m_nCurrBit == i)
                {
                    drawCode(r, painter, list_at( m_nCode,i), true);
                }
                else
                {
                    drawCode(r, painter, list_at( m_nCode,i));
                }
                r.moveTo(r.x()+14, r.y());
                if( (i+1)%4 == 0)
                {
                    r.moveTo(r.x()+4, r.y());
                }
            }
            r.moveTo(r.x()+23, r.y());
            r.setWidth(100);
            painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter, "D15");
        }
    }
    else
    {
        //! font
        QFont font( qApp->font().family(), 14);
        painter.setFont( font );

        QRect rect = r;
        rect.setY(rect.y()+7);
        rect.setHeight(rect.height()-14);

        drawBitCode(rect, painter);
    }
}


/**
 * @brief CTrigCode::setCode
 * @param arg
 */
void CCodeKeyboard::setCode(QList<int> &arg)
{
    m_nCode = arg;
    update();
}

/**
 * @brief CTrigCode::setCode
 * @param arg
 */
void CCodeKeyboard::setCode(CArgument &arg)
{
    int size = arg.size();
    int val = 0;
    m_nCode.clear();
    for(int i=0; i<size; i++)
    {
        arg.getVal(val, i);
        m_nCode.append(val);
    }

    if (CODE_TYPE_SOURCE != m_nCodeMode)
    {
        /** 将4个BIN数据 转换为 1个HEX数据. */
        m_nHexCode.clear();
        bool bHexMask = true;
        int  data     = 0;
        int bitCount = m_nCode.size();
        int align = 4 - ((bitCount)%4);//!最高位缺 align 位

        for(int i = bitCount-1; i>=0; i--)
        {
             int val = list_at( m_nCode,i);

             if(val == Trigger_Code_H)
             {
                 data |= ( 1<< (3-((i+align)%4)) );
             }
             else if(val == Trigger_Code_L)
             {
                 data &= (~(1<<(3-((i+align)%4))) );
             }
             else
             {
                 bHexMask = false;
             }

             if( (0 == ((i+align)%4)) || (i == 0) )
             {
                  m_nHexCode.insert(0, bHexMask? data : Trigger_Code_X );
                  bHexMask = true;
                  data = 0;
             }
        }

        if(m_pCodeBoard->getIsHexCode())
            m_pCodeBoard->setCode(m_nHexCode, m_nCode.count(), m_nCurrBit);
        else
            m_pCodeBoard->setCode(m_nCode, m_nCode.count(), m_nCurrBit);
    }else
        m_pCodeBoard->setCode(m_nCode, m_nCode.count(), m_nCurrBit);

    update();
}

void CCodeKeyboard::setCodeMode(int mode)
{
    Q_ASSERT(   (mode == CODE_TYPE_SOURCE)||
                (mode == CODE_TYPE_DIGIT) );

    m_nCodeMode = mode;
    Q_ASSERT(NULL != m_pCodeBoard);
    m_pCodeBoard->setCodeMode(m_nCodeMode);

    update();
}

void CCodeKeyboard::setSoucrEdgeDisbale(bool bEdge)
{
    m_pCodeBoard->setBtnEdgeDisabled(bEdge);
}

/**
 * @brief CTrigCode::setCurrBit
 * @param bit
 */
void CCodeKeyboard::setCurrBit(int bit)
{
    m_nCurrBit = bit;
    update();
}

void CCodeKeyboard::setIsHasOptMso(bool b)
{
    isHasOptMso = b;
    update();
}

void CCodeKeyboard::set_X()
{
    if (CODE_TYPE_SOURCE == m_nCodeMode )
    {
        if(m_nCurrBit < m_nCode.count() && m_nCurrBit >=0){
            m_nCode.replace(m_nCurrBit,2);
            emit postCurrCodeSig(list_at(m_nCode,m_nCurrBit) );
        }

        if(isHasOptMso || (!isHasOptMso && m_nCurrBit != 3))
        {
            gotoNextBit();
        }
    }
    else
    {
        emit postCurrCodeSig( Trigger_Code_X );
        gotoNextBit();
    }
    update();
}

void CCodeKeyboard::set_rise()
{
    if(m_nCurrBit < m_nCode.count() && m_nCurrBit >=0){
        m_nCode.replace(m_nCurrBit,3);
        emit postCurrCodeSig(list_at(m_nCode,m_nCurrBit) );

        if(isHasOptMso || (!isHasOptMso && m_nCurrBit != 3))
        {
            gotoNextBit();
        }
        update();
    }
}

void CCodeKeyboard::set_fall()
{
    if(m_nCurrBit < m_nCode.count() && m_nCurrBit >=0 ){
        m_nCode.replace(m_nCurrBit,4);
        emit postCurrCodeSig(list_at(m_nCode,m_nCurrBit) );

        if(isHasOptMso || (!isHasOptMso && m_nCurrBit != 3))
        {
            gotoNextBit();
        }
        update();
    }
}

void CCodeKeyboard::gotoNextBit()
{
    int bitMin = 0;
    int bitMAx = m_nCode.count() - 1;

    int hexMin = m_nCode.count();
    int hexMAx = m_nCode.count()  + m_nHexCode.count() - 1;

    if(m_nCurrBit < bitMAx)
    {
        ++m_nCurrBit;

        m_nCurrBit = qBound(bitMin, m_nCurrBit, bitMAx);

        m_pCodeBoard->setBtnDisabled(1);
        if (CODE_TYPE_SOURCE == m_nCodeMode)
        {
            emit postCurrBitSig(m_nCurrBit+1);
        }
        else
        {
            emit postCurrBitSig(1);
        }
    }
    else if( m_nCurrBit < hexMAx)
    {
        ++m_nCurrBit;

        m_nCurrBit = qBound(hexMin, m_nCurrBit, hexMAx);

        if(m_nCurrBit == m_nCode.count())//-1+m_nHexCode.count()
        {
            m_pCodeBoard->setBtnDisabled(m_nCode.count()%4);
        }
        else
        {
            m_pCodeBoard->setBtnDisabled(0);
        }

        emit postCurrBitSig(1);
    }

    update();
}

void CCodeKeyboard::gotoPreBit()
{
    int bitMin = 0;
    int bitMAx = m_nCode.count() - 1;

    int hexMin = m_nCode.count();
    int hexMAx = m_nCode.count()  + m_nHexCode.count() - 1;

    if( m_nCurrBit <= hexMin)
    {
        --m_nCurrBit;

        m_nCurrBit = qBound(bitMin, m_nCurrBit, bitMAx);

        m_pCodeBoard->setBtnDisabled(1);

        if (CODE_TYPE_SOURCE == m_nCodeMode )
        {
            emit postCurrBitSig(m_nCurrBit+1);
        }
        else
        {
            emit postCurrBitSig(-1);
        }
    }
    else if( m_nCurrBit <= hexMAx)
    {
        --m_nCurrBit;

        m_nCurrBit = qBound(hexMin, m_nCurrBit, hexMAx);

        if(m_nCurrBit == m_nCode.count())
        {
            m_pCodeBoard->setBtnDisabled(m_nCode.count()%4);
        }

        else
        {
            m_pCodeBoard->setBtnDisabled(0);
        }

        emit postCurrBitSig(-1);
    }

    update();
}

void CCodeKeyboard::set_all()
{
    if(m_nCurrBit < m_nCode.count())
        emit postSetAllSig(list_at(m_nCode, m_nCurrBit));
    else
        emit postSetAllSig(list_at(m_nHexCode,m_nCurrBit-m_nCode.count()));
}

void CCodeKeyboard::setCurrBitValue(int num)
{
    if (CODE_TYPE_SOURCE == m_nCodeMode )
    {
        if( 1 == num )
        {
            if(m_nCurrBit < m_nCode.count() && m_nCurrBit >=0)
            {
                m_nCode.replace(m_nCurrBit,0);
                emit postCurrCodeSig(list_at(m_nCode,m_nCurrBit) );
            }
        }
        else if( 0 == num)
        {
            if(m_nCurrBit < m_nCode.count() && m_nCurrBit >=0)
            {
                m_nCode.replace(m_nCurrBit,1);
                emit postCurrCodeSig(list_at(m_nCode,m_nCurrBit) );
            }
        }

        if(isHasOptMso || (!isHasOptMso && m_nCurrBit != 3))
        {
            gotoNextBit();
        }
    }
    else
    {
        if(m_nCurrBit < m_nCode.count() && m_nCurrBit >=0)
        {
            m_nCode.replace(m_nCurrBit,num);
            emit postCurrCodeSig(list_at(m_nCode,m_nCurrBit) );
        }
        else if(m_nCurrBit >= m_nCode.count()
                 && m_nCurrBit < m_nCode.count()+m_nHexCode.count())
        {
            m_nHexCode.replace(m_nCurrBit-m_nCode.count(),num);
            emit postCurrCodeSig(list_at(m_nHexCode,m_nCurrBit-m_nCode.count()) );
        }

        gotoNextBit();
    }

    update();
}

void CCodeKeyboard::onCodeBoardClick()
{
    if(!m_pCodeBoard->isVisible())
    {
        int i = m_nCurrBit;
        if (CODE_TYPE_SOURCE == m_nCodeMode )
        {
            m_pCodeBoard->setCode(m_nCode, m_nCode.count(), i);
            m_pCodeBoard->setBtnDisabled(1);
            m_pCodeBoard->setCurrBitBtnFocus(list_at(m_nCode, i));
        }else{
            if(i<m_nCode.count()){
                m_pCodeBoard->setCode(m_nCode, m_nCode.count(), i);
                m_pCodeBoard->setBtnDisabled(1);
                m_pCodeBoard->setCurrBitBtnFocus(list_at(m_nCode, i));
            }else{
                m_pCodeBoard->setCode(m_nHexCode, m_nCode.count(), i);
                if(i == m_nCode.count())
                    m_pCodeBoard->setBtnDisabled(m_nCode.count()%4);
                else
                    m_pCodeBoard->setBtnDisabled(0);
                m_pCodeBoard->setCurrBitBtnFocus(list_at(m_nHexCode,i-m_nCode.count()));
            }
        }
        m_pCodeBoard->show();
    }
    else
    {m_pCodeBoard->hide();}
}
