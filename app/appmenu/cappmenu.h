#ifndef CAPPMENU_H
#define CAPPMENU_H

#include "../../menu/rmenus.h"

/*!
 * \brief The CAppMenu class
 * 菜单应用
 * - 管理菜单系统
 */
class CAppMenu
{
public:
    CAppMenu();
    ~CAppMenu();
public:
    void loadMenus( QWidget *parent = 0 );
    void connectMenu( serviceExecutor *pExe,
                      menu_res::RServMenu *pMenu );
    void initMenuAttr( serviceExecutor *pExe,
                       menu_res::RServMenu *pMenu );

    void active( int index );
private:
    menu_res::serviceMenuList mListMenu;
};

#endif // CAPPMENU_H
