
#include "../../service/servicefactory.h"

#include "../../service/service_name.h"

#include "../../include/dsostd.h"

#include "../../service/service_msg.h"

#include "cappmenu.h"

struct menuCfg
{
    const char* menuRes;
    const char* servName;
    int  srcTitle;
    int  dstTitle;
};

//#define _GUI_CASE
/*! \var _menus
* 服务菜单配置表
* - menu: 菜单名称，即 excel 文件名称
* - service: 服务名称
* - titile: 菜单显示标题
*/
static menuCfg _menus[]=
{
    //! menu,       service           srcTitle,     dstTitle,
//    { "menutest",     0,                0, 0,},

#if 1
//    { "menutest",     "test2",          0, 0,},

#ifndef _GUI_CASE

    { "chan1",        serv_name_ch1,    MSG_CHAN_ACTIVE, IDS_CHAN1, },
    { "chan1",        serv_name_ch2,    MSG_CHAN_ACTIVE, IDS_CHAN2, },
    { "chan1",        serv_name_ch3,    MSG_CHAN_ACTIVE, IDS_CHAN3, },
    { "chan1",        serv_name_ch4,    MSG_CHAN_ACTIVE, IDS_CHAN4, },

    { "horizontal",   serv_name_hori,            0,0, },

    { "trigger",      serv_name_trigger_edge,     0,0, },
    { "trigger",      serv_name_trigger_pulse,    0,0, },
    { "trigger",      serv_name_trigger_slope,    0,0, },
    { "trigger",      serv_name_trigger_video,    0,0, },
    { "trigger",      serv_name_trigger_pattern,  0,0, },
    { "trigger",      serv_name_trigger_duration, 0,0, },
    { "trigger",      serv_name_trigger_timeout,  0,0, },
    { "trigger",      serv_name_trigger_runt,     0,0, },
    { "trigger",      serv_name_trigger_over,     0,0, },
    { "trigger",      serv_name_trigger_window,   0,0, },
    { "trigger",      serv_name_trigger_setup,    0,0, },
    { "trigger",      serv_name_trigger_nth,      0,0, },
    { "trigger",      serv_name_trigger_delay,    0,0, },
    { "trigger",      serv_name_trigger_rs232,    0,0, },
    { "trigger",      serv_name_trigger_i2c,      0,0, },
    { "trigger",      serv_name_trigger_spi,      0,0, },
    { "trigger",      serv_name_trigger_ab,       0,0, },
    { "trigger",      serv_name_trigger_can,      0,0, },
    { "trigger",      serv_name_trigger_lin,      0,0, },

    { "trigger",      serv_name_trigger_i2s,      0,0, },
    { "trigger",      serv_name_trigger_1553b,    0,0, },
    { "trigger",      serv_name_trigger_flexray,  0,0, },
//    { "trigger",      serv_name_trigger_sbus,     0,0, },
//    { "trigger",      serv_name_trigger_429,      0,0, },
//    { "trigger",      serv_name_trigger_1wire,    0,0, },

    { "ZoneTrigger",  serv_name_trigger_zone,     0,0, },

    { "storage",      serv_name_storage,          0,0, },

    { "Search",       serv_name_search,           0,0, },

    { "Source",       serv_name_source1,  MSG_APP_DG, IDS_SOURCE1, },
    { "Source",       serv_name_source2,  MSG_APP_DG, IDS_SOURCE2, },

    { "display",      serv_name_display,           0,0, },
    { "quick",        serv_name_quick,             0,0, },

    { "LA",           serv_name_la,              0,0, },
    { "ref",          serv_name_ref,             0,0, },
    { "mask",         serv_name_mask,            0,0, },

    { "mathsel",      serv_name_math_sel,         0,0, },
    { "mathfftsel",   serv_name_math_fft,         0,0, },

    { "math",         serv_name_math1,    MSG_MATH, IDS_MATH1, },
    { "math",         serv_name_math2,    MSG_MATH, IDS_MATH2, },
    { "math",         serv_name_math3,    MSG_MATH, IDS_MATH3, },
    { "math",         serv_name_math4,    MSG_MATH, IDS_MATH4, },

    { "utility",      serv_name_utility,          0,0, },

    { "IOset",        serv_name_utility_IOset,    0,0, },
    { "Print",        serv_name_utility_Print,    0,0, },
    { "Email",        serv_name_utility_Email,    0,0, },
    { "wifi",         serv_name_utility_Wifi,     0,0, },

    { "help",         serv_name_help,             0,0, },

    { "SelfCal",      serv_name_cal,              0,0, },

    { "Record",       serv_name_wrec,             0,0, },
    { "cursor",       serv_name_cursor,           0,0, },

    { "vdecode",      serv_name_decode1,   MSG_APP_DECODE,   IDS_DECODE1, },
    { "vdecode",      serv_name_decode2,   MSG_APP_DECODE,   IDS_DECODE2, },
    { "vdecode",      serv_name_decode3,   MSG_APP_DECODE,   IDS_DECODE3, },
    { "vdecode",      serv_name_decode4,   MSG_APP_DECODE,   IDS_DECODE4, },
    { "vdecodesel",   serv_name_decode_sel,   0,0, },

    { "measure",      serv_name_measure,      0,0, },
    { "Counter",      serv_name_counter,      0,0, },
    { "DVM",          serv_name_dvm,          0,0, },

    { "UPA",          serv_name_upa_powerq, MSG_APP_UPA, IDS_POWERQ, },
    { "UPA",          serv_name_upa_ripple, MSG_APP_UPA, IDS_RIPPLE, },

    { "EyeJit",       serv_name_eyejit,       0,   0, },
    { "Histogram",    serv_name_histo,        0,0, },
    { "guiserve",     serv_name_gui,          0,0, },
    { "autoset",      serv_name_autoset,      0,0, },
#endif
#endif
};

CAppMenu::CAppMenu()
{
}

CAppMenu::~CAppMenu()
{
    menu_res::serviceMenu *pMenu;

    foreach( pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );

        delete pMenu;
    }
}

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";

#define def_time()
#define start_time()
#define end_time()

void CAppMenu::loadMenus( QWidget *parent )
{
    def_time();

    int i;
    //! menu data
    menu_res::CResData::open();
    menu_res::CResData::setLanguageSeq( 0 );
    //! a builder
    menu_res::RMenuBuilder builder;

    r_meta::CAppMeta appMeta;
    QString strPath;
    QColor color;

    start_time();
    menu_res::serviceMenu *pMenu;
    for ( i = 0; i < array_count(_menus); i++ )
    {
        //! load menu
        pMenu = builder.loadMenu( _menus[i].menuRes,
                                  parent,
                                  _menus[i].srcTitle,
                                  _menus[i].dstTitle
                                  );
        Q_ASSERT( pMenu != NULL );

        //! config color
        strPath = QString("appmenu/page_color/%1").arg( _menus[i].servName );
        if ( appMeta.isNodeExist(strPath) )
        {
            appMeta.getMetaVal( strPath, color );
            pMenu->cfgFontColor( color, color );
        }

        //! set console name
        if ( _menus[i].servName != 0 )
        { pMenu->setConsoleName( _menus[i].servName ); }

        //! append menu
        mListMenu.append( pMenu );
    }
    end_time();

    menu_res::CCache::close();

    //! find service
    start_time();
    service *pService;
    serviceExecutor *pExe;
    foreach( pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );

        pService = serviceFactory::findService( pMenu->getConsoleName() );
        if(pService == NULL)
        {
            LOG_DBG()<<  pMenu->getConsoleName();
            Q_ASSERT( pService != NULL );
        }

        pExe = dynamic_cast<serviceExecutor *>(pService);
        Q_ASSERT( pExe != NULL );
        connectMenu( pExe, pMenu );

        initMenuAttr( pExe, pMenu );
    }
    end_time();

    //! retranslate
    start_time();
    foreach( pMenu, mListMenu )
    {
        Q_ASSERT( pMenu != NULL );

        pMenu->reTranslate();
    }
    end_time();

    sysSetMenuList( &mListMenu );
}

/*!
 * \brief CAppMenu::connectMenu
 * \param pExe
 * \param pMenu
 * 建立菜单和服务间的信号连接
 * - 服务发出信号，菜单响应信号
 */
void CAppMenu::connectMenu( serviceExecutor *pExe,
                            menu_res::RServMenu *pMenu )
{
    Q_ASSERT( NULL != pExe );
    Q_ASSERT( NULL != pMenu );

    QObject::connect( pExe, SIGNAL(sig_active_changed(int,void*,void*)),
                        pMenu, SLOT(on_active_changed(int)) );

    QObject::connect( pExe, SIGNAL(sig_active_in(int,void*,void*)),
                        pMenu, SLOT(on_active_in(int)) );
    QObject::connect( pExe, SIGNAL(sig_active_out(int,void*,void*)),
                        pMenu, SLOT(on_active_out( int ) ) );
//    return;

    QObject::connect( pExe, SIGNAL(sig_value_changed(int,void*,void*)),
                        pMenu, SLOT(on_value_changed(int)) );

    QObject::connect( pExe, SIGNAL(sig_enable_changed(int,bool,void*,void*)),
                        pMenu, SLOT(on_enable_changed(int,bool)) );
    QObject::connect( pExe, SIGNAL(sig_enable_changed(int,int,bool,void*,void*)),
                        pMenu, SLOT(on_enable_changed(int,int,bool)) );

    QObject::connect( pExe, SIGNAL(sig_visible_changed(int,bool,void*,void*)),
                        pMenu, SLOT(on_visible_changed(int,bool)) );
    QObject::connect( pExe, SIGNAL(sig_visible_changed(int,int,bool,void*,void*)),
                        pMenu, SLOT(on_visible_changed(int,int,bool)) );

    QObject::connect( pExe, SIGNAL(sig_content_changed(int,void*,void*)),
                        pMenu, SLOT(on_content_changed(int)) );

    QObject::connect( pExe, SIGNAL(sig_context_changed(int,void*,void*)),
                        pMenu, SLOT(on_context_changed(int)) );

    QObject::connect( pExe, SIGNAL(sig_focus_changed(int)),
                        pMenu, SLOT(on_focus_changed(int)) );
}

void CAppMenu::initMenuAttr( serviceExecutor *pExe,
                   menu_res::RServMenu *pMenu )
{
    Q_ASSERT( NULL != pExe );
    Q_ASSERT( NULL != pMenu );

    ui_attr::uiAttr *pAttr;

    pAttr = pExe->getAttr();
    Q_ASSERT( NULL != pAttr );

    pMenu->initResAttr( pAttr );
}

void CAppMenu::active( int index )
{
    int id;
    if ( index < 0 )
    {
        id = mListMenu.count() + index;
    }
    else
    {
        id = index;
    }

    if ( id < 0 || id >= mListMenu.count() )
    {
        Q_ASSERT( false );
        return;
    }

    ((*mListMenu[id])[0])->setActive();
}

