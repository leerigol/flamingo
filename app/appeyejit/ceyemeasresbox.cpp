#include "../../meta/crmeta.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"
#include "ceyemeasresbox.h"
#include <QFont>

menu_res::RModelWnd_Style CEyeMeasResBox::_style;

CEyeMeasResBox::CEyeMeasResBox(QWidget *parent) : uiWnd(parent)
{
    m_eyeMeasItems = new QList<CEyeMeasItem*>;
    setupUI(parent);
    this->setAttr(wnd_moveable);
}

void CEyeMeasResBox::setEyeMeasItem()
{
    if(m_eyeMeasItems->size() == 0)
    {
        for(int i = EYE_BEGIN;i < EYE_END; i++)
        {
            r_meta::CRMeta meta;
            QString str("eyejit/");
            QString name;
            meta.getMetaVal(str +"item"+ QString::number(i),name);

            QString value("-");
            CEyeMeasItem* item = new CEyeMeasItem(EyeMeasType(i),name,value);
            m_eyeMeasItems->append(item);
        }
//        m_nBoxH = m_nBoxDefaultH + m_nRowH * m_eyeMeasItems->size();
//        this->resize(m_nBoxW,m_nBoxH);
        update();
    }
}

void CEyeMeasResBox::upDateMeasRes(QVector<double> *results)
{
    for(int i = EYE_BEGIN;i < EYE_END; i++)
    {
        QString value;
        gui_fmt::CGuiFormatter::format( value, results->at(i), fmt_def, Unit_none );
        value += initUnit(static_cast<EyeMeasType> (i));
        m_eyeMeasItems->at(i)->m_eyeMeasValue = value;
    }
    update();
}

void CEyeMeasResBox::setupUI(QWidget *parent)
{
    setParent(parent);

    loadResource("eyejit/resultbox/");
    _style.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(m_nCloseRect);
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SIGNAL(closeSig()) );


}

QString CEyeMeasResBox::initUnit(EyeMeasType type)
{
    switch (type)
    {
    case EYE_ONE:
    case EYE_ZERO:
    case EYE_HEIGHT:
    case EYE_AMP:
        return QString("V");

    case EYE_WIDTH:
        return QString("S");

    case EYE_QFACTOR:
        return QString("");

    case EYE_CROSSPER:
        return QString("%");

    default:
        return QString("");
    }
}

void CEyeMeasResBox::delAllItem(void)
{
    this->hide();
    m_nBoxY = m_nBoxDefaultY;
    m_nBoxH = m_nBoxDefaultH;
}

void CEyeMeasResBox::loadResource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path ;

    meta.getMetaVal(str + "row_h", m_nRowH);//16
    meta.getMetaVal(str + "closerect", m_nCloseRect);
    int grayColor = 0;
    int blueColor = 0;
    meta.getMetaVal(str + "gray27", grayColor);
    setColor(gray27, grayColor);
    meta.getMetaVal(str + "blueSquare", blueColor);

    setColor(blue39, blueColor);

    meta.getMetaVal(str + "x", m_nBoxX);//170
    meta.getMetaVal(str + "y", m_nBoxY);//464
    meta.getMetaVal(str + "w", m_nBoxW);//630
    meta.getMetaVal(str + "h", m_nBoxH);//30

    m_nBoxDefaultY = m_nBoxY;
    m_nBoxDefaultH = m_nBoxH;
    this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);
}

void CEyeMeasResBox::paintEvent(QPaintEvent */*event*/)
{
    //Q_UNUSED(event);
    QPainter painter(this);
    QFont font;
    font.setPointSize(14);
    painter.setFont(font);

    //QRect r(20,0,4*titleWnd->m_nW,titleWnd->m_nH);
    QRect r(1000,0,0,0);

    int nWidth = this->geometry().width();
    r = QRect(0,0,nWidth,m_nBoxH);

    _style.paint(painter,r);

    int nStartX = 20;//in central
    r.setX(nStartX);
    r.setY(r.y()+8);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    QPen     pen;
    pen.setColor(QColor(200,200,200,255));
    painter.setPen(pen);
    QFont ft;
    ft.setPointSize(12);
    painter.setFont(ft);

    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Eye&Jit");


    r.setY(r.y() + m_nRowH + 8);
    r.setX(nStartX);//20,35,127,22
    r.setWidth(geometry().width()/2-nStartX);
    r.setHeight(m_nRowH);
    for(int i=0; i < m_eyeMeasItems->size(); i++)
    {
        QRect bgr = r;
        bgr.setX(7);
        bgr.setWidth(geometry().width()-14);
        if(i%2)
            painter.fillRect(bgr,gray27);
        else
            painter.fillRect(bgr,blue39);

        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_eyeMeasItems->at(i)->m_eyeMeasName);
        painter.drawText(r, Qt::AlignRight | Qt::AlignTop, ":");

        r.moveTo(r.x()+150, r.y());
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, m_eyeMeasItems->at(i)->m_eyeMeasValue);

        r.moveTo(nStartX, r.y()+m_nRowH);
    }
}
void CEyeMeasResBox::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}
