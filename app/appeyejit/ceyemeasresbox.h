#ifndef CEyeMeasResBox_H
#define CEyeMeasResBox_H
#include <QWidget>
#include <QVector>
#include <QString>
#include <QHBoxLayout>
#include "../../include/dsotype.h"

#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace DsoType;
struct CEyeMeasItem
{
    EyeMeasType m_eyeMeasType;
    QString m_eyeMeasName;
    QString m_eyeMeasValue;

    CEyeMeasItem(EyeMeasType a,QString b, QString c)
    {m_eyeMeasType = a;m_eyeMeasName = b;m_eyeMeasValue = c;}
};

using namespace menu_res;

class CEyeMeasResBox : public uiWnd
{
    Q_OBJECT

public:
    explicit CEyeMeasResBox(QWidget *parent = 0);

public:
    void setEyeMeasItem();
    void upDateMeasRes(QVector<double>* results);
    void setupUI(QWidget* parent);

    QString initUnit(EyeMeasType type);
private:
   QList<CEyeMeasItem*>* m_eyeMeasItems;

private:
    void delAllItem(void);
    void setChooseRow(int row);

    void setChoosed(bool b);

signals:
    void delItemSig();
    void raiseValueBox();
    void closeSig();

protected:
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString&);
    void setColor( QColor &color, int hexRgb );

private:
    static menu_res::RModelWnd_Style _style;

private:
    RImageButton   *m_pCloseButton;

    QRect m_nCloseRect;
    int  m_nBoxX;
    int  m_nBoxY;
    int  m_nBoxDefaultY;
    int  m_nBoxW;
    int  m_nBoxH;
    int  m_nBoxDefaultH;

    int  m_nRowH;

    QColor gray27,blue39;
};
#endif // CMEASVALUEBOX_H
