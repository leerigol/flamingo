#include "ceyeplotwidget.h"
#include <QDebug>
#include <cmath>
#include <QTime>
#include <QStylePainter>
#include "../../include/dsocfg.h"


CEyePlotWidget::CEyePlotWidget(QWidget *parent) : QWidget(parent)
{
    m_EyeMap = NULL;
    m_RowMap = 480;
    m_ColMap = 1000;

    m_MaskDisp = false;
    m_EyeDisp = false;
    m_offset = 0;

    setParent(parent);
    resize(parent->size());
    m_colorForIds.reserve(7);

    m_colorForIds<<QColor(0,0,48)<<QColor(0,100,200)<<QColor(0,204,0)<<QColor(248,252,0)
                <<QColor(248,152,0)<<QColor(248,0,0)<<QColor(248,252,248);
    this->setObjectName("EYE_LAYER");
    this->resize(wave_width, wave_height);
    this->move(0,0);
}

void CEyePlotWidget::initMapSize(void *p)
{
    //!
    m_EyeMap = (QVector<QVector<int> >*)p;
    if(!m_EyeMap->isEmpty())
    {
        m_RowMap = m_EyeMap->size();
        if(!m_EyeMap->at(0).isEmpty())
        {
            m_ColMap = m_EyeMap->at(0).size();
        }
    }
}

void CEyePlotWidget::setEyeMap(void *curveScreen)
{
    QMap<int,QVector<QPoint> >* p = (QMap<int,QVector<QPoint> >*) curveScreen;
    m_curveScreen = *p;
}

void CEyePlotWidget::setOffset(int offset)
{
    m_offset = offset;
    update();
}

void CEyePlotWidget::setMaskOnOff(bool onoff)
{
    m_MaskDisp = onoff;
}

void CEyePlotWidget::setEyeOnOff(bool onoff)
{
    m_EyeDisp = onoff;
}

void CEyePlotWidget::setMaskMap(QVector<int> X,QVector<int> Y)
{
    m_maskMapX = X;
    m_maskMapY = Y;
    //!Y数组是从下往上逐渐增大的，而pixmap的坐标原点在左上角，所以用Y数组表示坐标要求补
    for(int i = 0; i < m_maskMapY.size();i++)
    {
        m_maskMapY[i] = m_RowMap - m_maskMapY[i];
    }
}

void CEyePlotWidget::drawMap(QPainter &painter)
{
    QMap<int, QVector<QPoint> >::iterator i = m_curveScreen.begin();
    while (i != m_curveScreen.end())
    {
        int id = i.key();
        painter.setPen(m_colorForIds[uint(id) % 7]);
        painter.drawPoints(i.value());
        i++;
    }
}

void CEyePlotWidget::drawMask(QPainter &painter)
{
    //!QPainter painter(&pixmap);
    int vStart = m_maskMapX[4];
    int vEnd = m_maskMapX[5];
    painter.setBrush(QBrush(Qt::blue, Qt::Dense1Pattern));

    QRect rectUp(QPoint(vStart,0),QPoint(vEnd,m_maskMapY[4]));
    QRect rectBelow(QPoint(vStart,m_maskMapY[0]),
                    QPoint(vEnd,m_RowMap));
    QPolygon midPolygon(6);
    midPolygon[0] = QPoint(m_maskMapX[0],m_maskMapY[2]);
    midPolygon[1] = QPoint(m_maskMapX[1],m_maskMapY[3]);
    midPolygon[2] = QPoint(m_maskMapX[2],m_maskMapY[3]);
    midPolygon[3] = QPoint(m_maskMapX[3],m_maskMapY[2]);
    midPolygon[4] = QPoint(m_maskMapX[2],m_maskMapY[1]);
    midPolygon[5] = QPoint(m_maskMapX[1],m_maskMapY[1]);
    painter.drawPolygon(rectUp);
    painter.drawPolygon(rectBelow);
    painter.drawPolygon(midPolygon);
}

void CEyePlotWidget::refreshPixmap()
{
    QTime scaleTime;
    scaleTime.start();

    if(!m_EyeDisp && !m_MaskDisp)
    {
        hide();
        return;
    }

    pixmap = QPixmap(wave_width,wave_height);
    #ifdef _SIMULATE
    pixmap.fill(Qt::black);
    #else
    pixmap.fill(Qt::transparent);
    #endif
    QPainter painters(&pixmap);
    //painters.fillRect(pixmap.rect(), QColor(0xC8,0x98,0x60));
    if(m_EyeDisp)
    {
        drawMap(painters);
    }
    if(m_MaskDisp)
    {
        drawMask(painters);
    }

//    m_DrawMap = pixmap.scaled(QSize(this->width(), this->height()),
//                              Qt::IgnoreAspectRatio, Qt::FastTransformation);
    update();
    //qDebug()<<"scaleTime"<<scaleTime.elapsed()/1000.0<<"s";
}

void CEyePlotWidget::paintEvent(QPaintEvent* )
{
    if(!pixmap.isNull()){
        QStylePainter stylePaint(this);
        stylePaint.drawPixmap(0,0, pixmap);

//        QPixmap temp;
//        if(m_offset >= 300 || m_offset <= -300)
//        {
//            return;
//        }
//        else if(m_offset >= 0 && m_offset <= 60)
//        {
//            temp = m_DrawMap.copy(0,60 + m_offset,width(),480);
//            stylePaint.drawPixmap(0, 0, temp);//将像素映射复制到窗口的（0,0）位置
//        }
//        else if(m_offset >= 60)
//        {
//            temp = m_DrawMap.copy(0,60 + m_offset,width(),540 - m_offset);
//            stylePaint.drawPixmap(0, 0, temp);//将像素映射复制到窗口的（0,0）位置
//        }
//        else if(m_offset < 0 && m_offset >= -60)
//        {
//            temp = m_DrawMap.copy(0,60 + m_offset,width(),480);
//            stylePaint.drawPixmap(0, 0, temp);//将像素映射复制到窗口的（0,0）位置
//        }
//        else // m_offset : -60 ~ -300
//        {
//            temp = m_DrawMap.copy(0,0,width(),540 + m_offset);
//            stylePaint.drawPixmap(0, -m_offset, temp);//将像素映射复制到窗口的位置
//        }

    }
}
