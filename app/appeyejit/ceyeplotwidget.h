#ifndef CEYEPLOTWIDGET_H
#define CEYEPLOTWIDGET_H

#include <QMap>
#include <QPainter>
#include <QPixmap>
#include <QVector>
#include <QWidget>

class CEyePlotWidget : public QWidget
{
    Q_OBJECT
    
public:
    CEyePlotWidget(QWidget *parent = 0);

    void paintEvent(QPaintEvent* event);

    void initMapSize(void *p);
    void setEyeMap(void *curveScreen);
    void setOffset(int offset);
    void setMaskMap(QVector<int> X,QVector<int> Y);

    void refreshPixmap();

    void setMaskOnOff(bool onoff);
    void setEyeOnOff(bool onoff);

private:

    void drawMap(QPainter &painter);
    void drawMask(QPainter &painter);

    QMap<int, QVector<QPoint> > m_curveScreen;
    QVector<QVector<int> >* m_EyeMap;
    QVector<QColor> m_colorForIds;
    QVector<int> m_maskMapX;
    QVector<int> m_maskMapY;

    int m_RowMap;
    int m_ColMap;
    int m_offset;//! 单位为屏幕像素

    QPixmap  pixmap;
    QPixmap  m_DrawMap;
    bool m_EyeDisp;
    bool m_MaskDisp;
    bool m_JitTrackDisp;
    bool m_JitSpecDisp;
};
#endif // CEYEPLOTWIDGET_H
