#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../service/servch/servch.h"
#include "../appmain/cappmain.h"

#include "ceyeplotwidget.h"

#include "appeyejit.h"
#include <QDebug>

//! msg table
IMPLEMENT_CMD( CApp, CAppEyeJit )
start_of_entry()

on_set_void_void (servEyeJit::MSG_EYEJIT_EYEMAP, &CAppEyeJit::onSetEyeDisp),
on_set_void_void (servEyeJit::MSG_EYEJIT_MASKMAP, &CAppEyeJit::onSetMaskDisp),
on_set_void_void (servEyeJit::MSG_EYEJIT_UPDATELOOP, &CAppEyeJit::refreshDisp),
on_set_void_void (servEyeJit::MSG_EYEJIT_EMEASRES,&CAppEyeJit::upDateMeasRes),
///
/// \brief on_set_void_void
///
on_set_void_void (MSG_EYEJIT_YOFFSET,       &CAppEyeJit::onOffset),
on_set_void_void (MSG_EYEJIT_EMEAS_SELITEM, &CAppEyeJit::onSetEyeMeas),

end_of_entry()

CEyePlotWidget   *eyePlotWidget;

CAppEyeJit::CAppEyeJit()
{
    mservName = serv_name_eyejit;
    mName  = mservName;

#ifndef FLAMINGO_TR5
    addQuick(MSG_EYEJIT,
             QString("ui/desktop/eyejit/icon/"),1);
    addQuick(MSG_JITTER,
             QString("ui/desktop/jit/icon/"),MSG_JITTER);
#endif
}

void CAppEyeJit::setupUi(CAppMain *pMain)
{
    eyeMeasResBox = new CEyeMeasResBox(pMain->getCentBar());
    Q_ASSERT( eyeMeasResBox != NULL );
    eyeMeasResBox->hide();

    eyePlotWidget = new CEyePlotWidget(pMain->getCentBar());
    Q_ASSERT( eyePlotWidget != NULL );
    eyePlotWidget->hide();
}

void CAppEyeJit::retranslateUi()
{

}

void CAppEyeJit::buildConnection()
{
    connect(eyeMeasResBox, SIGNAL(closeSig()),this,SLOT(postCloseSig()));
}

void CAppEyeJit::resize()
{

}

void CAppEyeJit::registerSpy()
{

}

void CAppEyeJit::postCloseSig()
{
    serviceExecutor::post(serv_name_eyejit,MSG_EYEJIT_EMEAS_SELITEM,false);
}

void CAppEyeJit::onSetEyeMeas()
{
    bool disResOnOff = true;
    serviceExecutor::query(serv_name_eyejit,MSG_EYEJIT_EMEAS_SELITEM,disResOnOff);

    if(disResOnOff)
    {
        eyeMeasResBox->setEyeMeasItem();
        void* p;
        serviceExecutor::query(serv_name_eyejit,servEyeJit::MSG_EYEJIT_EMEASRES,p);
        QVector<double>* eyeMeasRes = static_cast<QVector<double>*> (p);
        eyeMeasResBox->upDateMeasRes(eyeMeasRes);
        eyeMeasResBox->show();
        eyeMeasResBox->raise();
        eyeMeasResBox->update();
    }
    else
    {
        eyeMeasResBox->hide();
    }
}


void CAppEyeJit::onSetJitMeas()
{

}

void CAppEyeJit::onSetEyeDisp()
{
    void *eyeMap;
    serviceExecutor::query(serv_name_eyejit,servEyeJit::MSG_EYEJIT_EYEMAP,eyeMap);   
    eyePlotWidget->initMapSize(eyeMap);

    bool onoff = false;
    serviceExecutor::query(serv_name_eyejit,MSG_EYEJIT_EMEAS_EN,onoff);
    eyePlotWidget->setEyeOnOff(onoff);

    void *curveMap;
    serviceExecutor::query(serv_name_eyejit,servEyeJit::MSG_EYEJIT_EYESCREEN,curveMap);
    eyePlotWidget->setEyeMap(curveMap);
    eyePlotWidget->refreshPixmap();
    eyePlotWidget->show();
}

void CAppEyeJit::onSetMaskDisp()
{
    void *eyeMap;
    serviceExecutor::query(serv_name_eyejit,servEyeJit::MSG_EYEJIT_EYEMAP,eyeMap);

    eyePlotWidget->initMapSize(eyeMap);
    eyePlotWidget->show();

    void *p;
    serviceExecutor::query(serv_name_eyejit,servEyeJit::MSG_EYEJIT_MASKMAP,p);
    MaskNRZMap* maskMap = static_cast<MaskNRZMap*> (p);    
    QVector<int> x = maskMap->X;
    QVector<int> y = maskMap->Y;
    eyePlotWidget->setMaskMap(x,y);

    bool onoff = false;
    serviceExecutor::query(serv_name_eyejit,MSG_EYEJIT_MASK_EN,onoff);
    eyePlotWidget->setMaskOnOff(onoff);
    eyePlotWidget->refreshPixmap();
}

void CAppEyeJit::onOffset()
{
    int vOffset;
    serviceExecutor::query(serv_name_eyejit,servEyeJit::cmd_eye_viewOffset,vOffset);
    eyePlotWidget->setOffset(vOffset);
}

void CAppEyeJit::refreshDisp()
{
    bool onoff = false;
    serviceExecutor::query(serv_name_eyejit,MSG_EYEJIT_EMEAS_EN,onoff);
    eyePlotWidget->setEyeOnOff(onoff);

    serviceExecutor::query(serv_name_eyejit,MSG_EYEJIT_MASK_EN,onoff);
    eyePlotWidget->setMaskOnOff(onoff);

//    qDebug()<<"refresh";
    eyePlotWidget->refreshPixmap();
}

void CAppEyeJit::upDateMeasRes()
{
    void *p;
    serviceExecutor::query(serv_name_eyejit,servEyeJit::MSG_EYEJIT_EMEASRES,p);
    QVector<double>* temp = static_cast<QVector<double>* > (p);
    eyeMeasResBox->upDateMeasRes(temp);
}
