#ifndef APPEYEJIT_H
#define APPEYEJIT_H

#include <QtCore>
#include <QtWidgets>
#include "../capp.h"

#include "ceyemeasresbox.h"


class CEyePlotWidget;

class CAppEyeJit : public CApp
{
    Q_OBJECT

    DECLARE_CMD()
private:

    CEyeMeasResBox   *eyeMeasResBox;
    CEyePlotWidget   *eyePlotWidget;

public:
    CAppEyeJit();

    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();

    virtual void buildConnection();
    virtual void resize();
    virtual void registerSpy();

public slots:
    void postCloseSig();

private:
    void onSetCR();
    void onSetEyeMeas();
    void onSetJitMeas();

    void onSetEyeDisp();
    void onSetMaskDisp();

    void onOffset();
    void refreshDisp();

    void upDateMeasRes();
};

#endif // APPEYEJIT_H
