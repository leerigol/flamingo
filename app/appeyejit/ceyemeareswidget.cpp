#include "ceyemeareswidget.h"

CEyeMeaResWidget::CEyeMeaResWidget(QWidget *parent) : menu_res::uiWnd(parent)
{
    eyeMeasItems.append("One电平");
    eyeMeasItems.append("Zero电平");
    eyeMeasItems.append("眼高");
    eyeMeasItems.append("眼宽");
    eyeMeasItems.append("眼幅度");
    eyeMeasItems.append("交叉比");
    eyeMeasItems.append("Q因子");

    setupUi(parent);
}

void CEyeMeaResWidget::setupUi(QWidget *parent)
{
    loadResource("eyejit/resultbox/");
    setParent(parent);
}

void CEyeMeaResWidget::loadResource(const QString &root)
{
    r_meta::CRMeta meta;
    QString str = root ;

    meta.getMetaVal(str + "row_h", m_nRowH);//

    meta.getMetaVal(str + "x", m_nBoxX);//
    meta.getMetaVal(str + "y", m_nBoxY);//
    meta.getMetaVal(str + "w", m_nBoxW);//
    meta.getMetaVal(str + "h", m_nBoxH);//
    m_nBoxDefaultY = m_nBoxY;
    m_nBoxDefaultH = m_nBoxH;

    QRect m_nRect;
    m_nRect.setRect(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);

    measResuBox.loadResource(str, m_nRect);

    this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);
}

void CEyeMeaResWidget::updateMeasValue(void)
{

}

void CEyeMeaResWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);

    QRect r = this->geometry();
    int nWidth  = r.width();
    int nStartX = 14;//in central

    r.setX(0);
    r.setY(0);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    measResuBox.paintEvent(painter, 2, 7, 1, r);

    r.setX(55);
    r.setY(5);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    QPen     pen;

    pen.setColor(QColor(200,200,200,255));
    painter.setPen(pen);
    for(int i = 0;i<eyeMeasItems.size();i++)
    {
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, eyeMeasItems.at(i));
        r.setX(r.x()+80);
    }

    r.setY(8);
    r.setY(r.y() + m_nRowH);
    r.setX(nStartX);

    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "0.0");
    r.setX(r.x()+120);
    for(int i=1; i<eyeMeasItems.size(); i++)
    {
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "0.0");
        r.setX(r.x()+80);
    }
}
