#-------------------------------------------------
#
# Project created by QtCreator 2017-02-07T14:59:11
#
#-------------------------------------------------
QT       += core
QT       += gui
QT       += widgets
QT       += network

#根据环境变量 确定是否定义宏 _SIMULATE
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/apps/appeyejit
INCLUDEPATH += .

HEADERS  += ceyeplotwidget.h
HEADERS  += ceyemeasresbox.h
HEADERS  += appeyejit.h
#HEADERS  += ceyemeareswidget.h
#HEADERS  += cmeasurebox.h

SOURCES += ceyeplotwidget.cpp
SOURCES += ceyemeasresbox.cpp
SOURCES += appeyejit.cpp
#SOURCES += ceyemeareswidget.cpp
#SOURCES += cmeasurebox.cpp
