#ifndef CEYEMEARESWIDGET_H
#define CEYEMEARESWIDGET_H

#include <QWidget>
#include <QLabel>
#include <cmeasurebox.h>
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/rmenus.h"

class CEyeMeaResWidget : public menu_res::uiWnd
{
    Q_OBJECT
public:
    explicit CEyeMeaResWidget(QWidget *parent = 0);

    void         setupUi(QWidget* parent);
    void         setShow();
    void         setClose();

protected:
    void         loadResource( const QString &root );
    void         paintEvent( QPaintEvent *event );

    void         creatWindowItems();
    void         buildConnection();

    void         updateMeasValue(void);
signals:

private:
    int  m_nBoxX;
    int  m_nBoxY;
    int  m_nBoxDefaultY;
    int  m_nBoxW;
    int  m_nBoxH;
    int  m_nBoxDefaultH;

    int  m_nRowH;

    bool mIsChoosed;

    QColor  gray27, blue;

    QString mMark, mTopLeft, mTopRight, mTop, mLeft;
    QString mRight, mBottom, mBottomLeft, mBottomRight;

    CMeasureBox       measResuBox;

    QStringList      eyeMeasItems;
    QVector<float> eyeMeasResults;
};

#endif // CEYEMEARESWIDGET_H
