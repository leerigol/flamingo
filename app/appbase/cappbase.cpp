#include "cappbase.h"
#include "../../service/servicefactory.h"
#include "../../service/service_msg.h" //! CMD_SERVICE_ACTIVE
#include "../../include/dsodbg.h"


AppWndHandle::AppWndHandle( QWidget **ppWindow,
                            wndType type,
                            int msg ,
                            const QString &name )
{
    m_ppWindow = ppWindow;
    mMsg = msg;
    mType = type;
    mServName = name;
}

AppWndHandle::wndType AppWndHandle::getType()
{ return mType; }

IMPLEMENT_CMD( CExecutor, CApp )
start_of_entry()
on_set_void_void( CMD_SERVICE_ACTIVE, &CApp::on_enterActive ),
on_set_void_void( CMD_SERVICE_ENTER_ACTIVE, &CApp::on_enterActive ),
on_set_void_void( CMD_SERVICE_EXIT_ACTIVE, &CApp::on_exitActive ),

on_set_void_void( CMD_SERVICE_SUB_ENTER, &CApp::on_enterSub ),
on_set_void_void( CMD_SERVICE_SUB_RETURN, &CApp::on_exitSub ),

on_set_void_void( CMD_SERVICE_RE_ENTER_ACTIVE, &CApp::on_reEnterActive ),

on_set_void_void( CMD_SERVICE_RST, &CApp::on_rst ),
on_set_void_void( CMD_SERVICE_PAGE_HIDE, &CApp::on_pageHide ),
end_of_entry()


CApp::CApp()
{
    mClass = app_application;
}

CApp::~CApp()
{
    struSpyItem* pItem;

    foreach( pItem, mSpyList )
    {
        Q_ASSERT( pItem != NULL );

        delete pItem;
    }

    foreach( AppWndHandle *pHandle, mAppWindows )
    {
        Q_ASSERT( NULL != pHandle );
        delete pHandle;
    }

    //! icons
    foreach( AppIcon *pIcon, mQuickIcons )
    {
        Q_ASSERT( NULL != pIcon );
        delete pIcon;
    }
}

void CApp::on_value_changed( int msg, void * pServ, void *pActContext )
{
    if ( filterMsg(msg, pServ, pActContext) ) return;

    doMsg( msg, cause_value );
}
void CApp::on_enable_changed( int msg, bool /*b*/, void * pServ, void *pActContext )
{
    if ( filterMsg(msg, pServ, pActContext) ) return;

    doMsg( msg, cause_enable );
}
void CApp::on_visible_changed( int msg, bool /*b*/, void * pServ, void *pActContext )
{
    if ( filterMsg(msg, pServ, pActContext) ) return;

    doMsg( msg, cause_visible );
}
void CApp::on_enable_changed( int msg, int /*opt*/, bool /*b*/, void * pServ, void *pActContext )
{
    if ( filterMsg(msg, pServ, pActContext) ) return;

    doMsg( msg, cause_enable );
}
void CApp::on_visible_changed( int msg, int /*opt*/, bool /*b*/, void * pServ, void *pActContext )
{
    if ( filterMsg(msg, pServ, pActContext) ) return;

    doMsg( msg, cause_visible );
}
void CApp::on_content_changed( int msg, void * pServ, void *pActContext )
{
    if ( filterMsg(msg, pServ, pActContext) ) return;

    doMsg( msg, cause_content );
}
void CApp::on_active_changed( int msg, void * pServ, void *pActContext )
{
    if ( filterMsg(msg, pServ, pActContext) ) return;

    doMsg( CMD_SERVICE_ACTIVE, cause_active );
}

/*!
 * \brief CApp::doMsg
 * \param msg
 * \param cause
 * for all msg entry
 */
void CApp::doMsg( int msg, AppEventCause cause )
{
    const struCmdEntry * entries[32];
    int cnt;

    //! get match entries for write
    cnt = CExecutor::getEntries( msg, entries, array_count(entries), enum_w );

    //! do each entry
    for ( int i = 0; i < cnt; i++  )
    {
        serviceExecutor::lockExec();

        doEntry( entries[i], cause );

        serviceExecutor::unlockExec();
    }
}

void CApp::doEntry( const struCmdEntry *pEntry,
                    AppEventCause cause )
{
    Q_ASSERT( pEntry != NULL );

    vProc proc;

    proc.unProc = pEntry->doProc;

    switch( pEntry->typeProc )
    {
        case enum_int_do_int:
            (this->*proc.int_proc_int)( (int)cause );
            break;

        case enum_void_do_void:
            (this->*proc.void_proc)( );
            break;

        case enum_int_do_void:
            (this->*proc.int_proc_void)( );
            break;

        case enum_void_do_int:
            (this->*proc.void_proc_int)( (int)cause );
            break;

        default:
            qWarning()<<"mismatch"<<__FUNCTION__<<__LINE__;
            break;
    }
}

void CApp::spyOn( const QString &strName,
                  int servMsg,
                  int aliasMsg,
                  eSpyType spyType
                      )
{
#ifdef FPU_DEBUG
    return;
#endif

    struServItem *pServItem;

    pServItem = service::findItem( strName );
    Q_ASSERT( pServItem != NULL );

    struSpyItem *pSpyItem = R_NEW( struSpyItem( pServItem->servId,
                                             servMsg,
                                             E_SERVICE_ID_NONE,
                                             aliasMsg,
                                             spyType  ) );
    Q_ASSERT( pSpyItem != NULL );

    //! find item in list
    struSpyItem *pListSpyItem;
    foreach( pListSpyItem, mSpyList )
    {
        Q_ASSERT( pListSpyItem != NULL );

        //! exist now
        if ( *pListSpyItem == *pSpyItem )
        {
            delete pSpyItem;
            return;
        }
    }

    //! connection exist
    foreach( pListSpyItem, mSpyList )
    {
        Q_ASSERT( pListSpyItem != NULL );

        if ( pListSpyItem->servId == pSpyItem->servId )
        {
            mSpyList.append( pSpyItem );
            return;
        }
    }

    //! build the connection
    mSpyList.append( pSpyItem );

    serviceExecutor *pExe;
    pExe = dynamic_cast<serviceExecutor *>( pServItem->pService );
    Q_ASSERT( pExe != NULL );

    //! connect to value
    connect( pExe, SIGNAL(sig_value_changed(int,void*,void*)),
             this, SLOT(on_value_changed(int,void*,void*)) );
}

void CApp::spyOn( const QString &strName,
                  int servMsg,
                  eSpyType spyType )
{
    spyOn( strName, servMsg, servMsg, spyType );
}

bool CApp::filterMsg( int &msg, void *pServ, void *pContext )
{
    int servId;

    //! service
    Q_ASSERT( NULL != pServ );
    serviceExecutor *pService;
    pService = static_cast<serviceExecutor*>(pServ);
    Q_ASSERT( NULL != pService );
    servId = pService->getId();

    //! self event
    if ( mservName == pService->getName() )
    {
        return false;
    }

    //! sibling event
    if ( mservSiblings.contains( pService->getName() ) )
    {
        return false;
    }

    //! spy event
    struSpyItem *pListSpyItem;
    foreach( pListSpyItem, mSpyList )
    {
        Q_ASSERT( pListSpyItem != NULL );

        //! exist now
        if ( pListSpyItem->servId == servId )
        {   
            //! msg match
            if ( msg == pListSpyItem->msg )
            {
                //! user attr
                if ( is_attr(pListSpyItem->spyType, e_spy_user) )
                {
                    //! spy type do not match
                    if ( NULL == pContext )
                    {
                        return true;
                    }

                    //! action context
                    struActionContext *pActContext;
                    pActContext = static_cast<struActionContext*>( pContext );

                    //! is user
                    if ( pActContext->isUserAction() )
                    {
                        msg = pListSpyItem->aliasMsg;
                        return false;
                    }
                    //! not user
                    else
                    {
                        return true;
                    }
                }

                msg = pListSpyItem->aliasMsg;
                return false;
            }
        }
    }

    return true;
}

void CApp::setName( const QString &name )
{
    mName = name;
}
const QString &CApp::getName()
{
    return mName;
}

void CApp::setServiceName( const QString &name )
{
    mservName = name;
}
const QString &CApp::getServiceName()
{
    return mservName;
}

void CApp::setClass( AppClass aClass )
{
    mClass = aClass;
}


AppClass CApp::getClass()
{
    return mClass;
}

void CApp::setState( AppState stat, int iconId )
{
    Q_ASSERT( iconId >= 0 && iconId < mQuickIcons.size() );
    mQuickIcons[iconId]->setState( stat );
}

AppState CApp::getState( int iconId )
{
    Q_ASSERT( iconId >= 0 && iconId < mQuickIcons.size() );

    return mQuickIcons[iconId]->getState();
}

bool CApp::addQuick(int tid,
                    const QString &entry,
                    int msg,
                    AppState stat)
{
    addQuick("", entry, msg, stat);
    AppIcon *pIcon = mQuickIcons.last();
    pIcon->setTitleID( tid );
    return true;
}

bool CApp::addQuick( const QString &title,
                     const QString &entry,
                     int msg,
                     AppState stat )
{
    //! find the item
    AppIcon *pIcon = NULL;
    foreach( AppIcon *pSubIcon, mQuickIcons )
    {
        Q_ASSERT( pSubIcon != NULL );
        if ( pSubIcon->mMsg == msg )
        {
            pIcon = pSubIcon;
            break;
        }
    }

    //! find item
    if ( NULL != pIcon )
    {
        return pIcon->addIcon( stat, entry);
    }
    //! new item
    else
    {
        pIcon = R_NEW( AppIcon() );
        if ( NULL == pIcon )
        { return false; }

        mQuickIcons.append( pIcon );

        pIcon->setMsg( msg );
        pIcon->setTitle( title );
        pIcon->setTitleID(0);

        Q_ASSERT( mservName.length() > 0 );
        pIcon->setServName( mservName );    //! service name

        return pIcon->addIcon( stat, entry );
    }
}

void CApp::setupUi( CAppMain */*pMain*/ )
{}
void CApp::retranslateUi()
{}
void CApp::buildConnection()
{}

void CApp::resize()
{}

void CApp::registerSpy()
{}

void CApp::boundtoService( service */*pServ*/ )
{}
void CApp::linkService()
{
    connectService( mservName );
}

void CApp::connectService( service *pServ )
{
    Q_ASSERT( NULL != pServ );

    serviceExecutor *pExe;
    CApp *pApp;

    //! convert
    pExe = dynamic_cast<serviceExecutor *>( pServ );
    Q_ASSERT( pExe != NULL );
    pApp = this;

    //! connect
    QObject::connect( pExe,
                      SIGNAL(sig_value_changed(int, void*, void*)),
                      pApp,
                      SLOT(on_value_changed(int,void*,void*)));

    QObject::connect( pExe,
                      SIGNAL(sig_enable_changed(int,bool,void*,void*)),
                      pApp,
                      SLOT(on_enable_changed(int,bool,void*,void*)));

    QObject::connect( pExe,
                      SIGNAL(sig_enable_changed(int,int,bool,void*,void*)),
                      pApp,
                      SLOT(on_enable_changed(int,int,bool,void*,void*)));

    QObject::connect( pExe, SIGNAL(sig_visible_changed(int,bool,void*,void*)),
                        pApp, SLOT(on_visible_changed(int,bool,void*,void*)) );
    QObject::connect( pExe, SIGNAL(sig_visible_changed(int,int,bool,void*,void*)),
                        pApp, SLOT(on_visible_changed(int,int,bool,void*,void*)) );

    QObject::connect( pExe, SIGNAL(sig_content_changed(int,void*,void*)),
                        pApp, SLOT(on_content_changed(int,void*,void*)) );

    QObject::connect( pExe, SIGNAL(sig_active_changed(int,void*,void*)),
                        pApp, SLOT(on_active_changed(int,void*,void*)) );

}

void CApp::connectService( const QString &servName )
{
    service *pService;

    //! find service
    pService = findService( servName );

    Q_ASSERT( pService != NULL );

    //! valid service
    connectService( pService );
}

service* CApp::findService( const QString &servName )
{
    service *pService;

    //! find service
    pService = serviceFactory::findService( servName );

    return pService;
}

bool CApp::isServiceActive()
{
    Q_ASSERT( mservName.size() > 0 );

    return serviceExecutor::queryActive( mservName );
}

void CApp::serviceActiveIn()
{
    serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, 1 );
}

menu_res::uiWndMgr * CApp::getuiWndMgr()
{
    return &mWndMgr;
}

void CApp::on_enterActive()
{
    closePopupWindow();

    enterActiveWnd();

    showPageWindow();
}

void CApp::on_exitActive()
{
    closePopupWindow();

    closePageWindow();

    unloadAllWnd();
}

void CApp::on_reEnterActive()
{
    activeNextWnd();
}

void CApp::on_enterSub()
{
    closePopupWindow();
}
void CApp::on_exitSub()
{
    closePopupWindow();
}

void CApp::on_rst()
{
    closePopupWindow();
}
void CApp::on_pageHide()
{
    closePopupWindow();
}
//! popup widget
void CApp::attachPopupWindow( void *ppWindow, int msg )
{
    attachPopupWindow( (QWidget**)ppWindow, msg );
}

void CApp::attachPopupWindow( QWidget **ppWindow, int msg )
{
    Q_ASSERT( NULL != ppWindow );

    AppWndHandle *pHandle = R_NEW( AppWndHandle( ppWindow,
                                              AppWndHandle::wnd_popup,
                                              msg,
                                              mservName ) );
    Q_ASSERT( NULL != pHandle );

    mAppWindows.append( pHandle );

    spyOn( serv_name_gui, CMD_SERVICE_PAGE_HIDE );
}

void CApp::closePopupWindow()
{
    QWidget *pWig, *pRelatedWig;

    //! foreach pop
    foreach( AppWndHandle *pHandle, mAppWindows )
    {
        Q_ASSERT( pHandle != NULL );

        do
        {
            //! invalid window var
            if ( pHandle->m_ppWindow == NULL )
            { break; }

            //! invalid window
            pWig = *pHandle->m_ppWindow;
            if ( NULL == pWig )
            { break; }

            //! invisible
            if ( !pWig->isVisible() )
            { break; }

            //! popup
            if ( pHandle->getType() != AppWndHandle::wnd_popup )
            { break; }

            //! related menu widget
            if ( pHandle->mMsg != 0 && pHandle->mServName.length() > 0 )
            {}
            else
            {
                pWig->setVisible( false );
                break;
            }

            //! find widget
            pRelatedWig = sysGetWidget( pHandle->mServName, pHandle->mMsg );
            if ( NULL == pRelatedWig )
            {
                pWig->setVisible( false );
            }
            else
            {
                if ( pRelatedWig->isVisible() )
                {}
                else
                {
                    pWig->setVisible( false );
                }
            }

        }while(0);
    }
}

//! page widget
void CApp::attachPageWindow( void *ppWindow, int msg )
{
    attachPageWindow( (QWidget**)ppWindow, msg );
}
void CApp::attachPageWindow( QWidget **ppWindow, int msg )
{
    Q_ASSERT( NULL != ppWindow );

    AppWndHandle *pHandle = R_NEW( AppWndHandle( ppWindow,
                                              AppWndHandle::wnd_page,
                                              msg,
                                              mservName ) );
    Q_ASSERT( NULL != pHandle );

    mAppWindows.append( pHandle );
}
void CApp::closePageWindow()
{
    QWidget *pWig, *pRelatedWig;

    //! foreach pop
    foreach( AppWndHandle *pHandle, mAppWindows )
    {
        Q_ASSERT( pHandle != NULL );

        do
        {
            //! invalid window var
            if ( pHandle->m_ppWindow == NULL )
            { break; }

            //! invalid window
            pWig = *pHandle->m_ppWindow;
            if ( NULL == pWig )
            { break; }

            //! invisible
            if ( !pWig->isVisible() )
            { break; }

            //! popup
            if ( pHandle->getType() != AppWndHandle::wnd_page )
            { break; }

            //! related menu widget
            if ( pHandle->mMsg != 0 && pHandle->mServName.length() > 0 )
            {}
            else
            {
                pWig->setVisible( false );
                break;
            }

            //! find widget
            pRelatedWig = sysGetWidget( pHandle->mServName, pHandle->mMsg );
            if ( NULL == pRelatedWig )
            {
                pWig->setVisible( false );
            }
            else
            {
                if ( pRelatedWig->isVisible() )
                {}
                else
                { pWig->setVisible( false ); }
            }

        }while(0);
    }
}

void CApp::showPageWindow()
{
    QWidget *pWig, *pRelatedWig;

    //! foreach pop
    foreach( AppWndHandle *pHandle, mAppWindows )
    {
        Q_ASSERT( pHandle != NULL );

        do
        {
            //! invalid window var
            if ( pHandle->m_ppWindow == NULL )
            { break; }

            //! invalid window
            pWig = *pHandle->m_ppWindow;
            if ( NULL == pWig )
            { break; }

            //! invisible
            if ( pWig->isVisible() )
            { break; }

            //! popup
            if ( pHandle->getType() != AppWndHandle::wnd_page )
            { break; }

            //! related menu widget
            if ( pHandle->mMsg != 0 && pHandle->mServName.length() > 0 )
            {}
            else
            {
                pWig->setVisible( true );
                break;
            }

            //! find widget
            pRelatedWig = sysGetWidget( pHandle->mServName, pHandle->mMsg );
            if ( NULL == pRelatedWig )
            {
                pWig->setVisible( true );
            }
            else
            {
                if ( pRelatedWig->isVisible() )
                { pWig->setVisible( true ); }
                else
                { pWig->setVisible( false ); }
            }

        }while(0);
    }
}

void CApp::enterActiveWnd()
{
    //! has now
    if ( mWndMgr.m_pActiveWnd != NULL )
    {
        setActiveWnd( mWndMgr.m_pActiveWnd );
    }
    else
    {
        activeNextWnd();
    }
}

//! find the next by current active
void CApp::activeNextWnd()
{
    mWndMgr.activeNext();
}
void CApp::setActiveWnd( menu_res::uiWnd *pWnd )
{
    mWndMgr.setActive( pWnd );
}

void CApp::unloadAllWnd()
{
    mWndMgr.unloadAll();
}
