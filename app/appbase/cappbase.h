#ifndef CAPP_BASE_H
#define CAPP_BASE_H

#include <QObject>
#include <QWidget>
#include <QApplication>
#include <QIcon>

#include <QDebug>

#include "../../include/dsostd.h"
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"

#include "../../baseclass/ccmdtarget.h"    //! for cmd table

#include "../../service/service.h"         //! for spy list
#include "../../menu/wnd/uiwnd.h"          //! app windows

#include "../../com/appicon/appicon.h"

//! for translate
#define TR  QApplication::translate

enum AppEventCause
{
    cause_value,
    cause_enable,
    cause_visible,
    cause_content,

    cause_active,
    cause_size,
};

enum AppClass
{
    app_application,
    app_setup,
    app_quick,
};

class AppWndHandle
{
public:
    enum wndType
    {
        wnd_popup,
        wnd_page,
    };

public:
    AppWndHandle( QWidget **m_ppWindow,
                  wndType type = wnd_popup,
                  int msg = 0, const QString &name="" );

public:
    wndType getType( );

public:
    QWidget **m_ppWindow;
    int mMsg;
    wndType mType;
    QString mServName;
};

class CAppMain;

//! query enumerate
#define query_e( serv_name, msg, type, val, err ) do\
                                             {\
                                             int _iVal;\
                                             err = query( serv_name, msg, _iVal );\
                                             if ( err != ERR_NONE ) break; \
                                             val = (type)_iVal;\
                                             }while(0);

/*!
 * \brief The CApp class
 * app基类
 * - app = view
 */
class CApp : public CExecutor
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CApp();
    virtual ~CApp();

protected:
    QString mName;          /*!< internal used */

    AppClass mClass;        //! app class
//    AppStatus mStatus;      //! app status

                            //! a few quick items
//    QStringList mQuickTitles;
//    QStringList mQuickEntries;
//    QList<int>  mQuickMsgs;

    QList< AppIcon*> mQuickIcons;

    QString mservName;      /*!< service name */
    QStringList mservSiblings;

    servSpyList mSpyList;   /*! spy list */

protected Q_SLOTS:
    void on_value_changed( int msg, void * pServ, void *pActContext );

    void on_enable_changed( int msg, bool b, void * pServ, void *pActContext );
    void on_visible_changed( int msg, bool b, void * pServ, void *pActContext );

    void on_enable_changed( int msg, int opt, bool b, void * pServ, void *pActContext );
    void on_visible_changed( int msg, int opt, bool b, void * pServ, void *pActContext );

    void on_content_changed( int msg, void * pServ, void *pActContext );
    void on_active_changed( int active, void * pServ, void *pActContext );

protected:
    void doMsg( int msg, AppEventCause cause );
    void doEntry( const struCmdEntry *pEntry,
                  AppEventCause cause );

    void spyOn( const QString &strName,
                int servMsg,
                int aliasMsg,
                eSpyType spyType = e_spy_post
               );
    void spyOn( const QString &strName,
                int servMsg,
                eSpyType spyType = e_spy_post );

    bool filterMsg( int &msg, void *pServ, void *pActContext );

public:
    void setName( const QString &name );
    const QString &getName();

    void setServiceName( const QString &name );
    const QString &getServiceName();

    void setClass( AppClass aClass );
    AppClass getClass();

    void setState( AppState stat, int iconId = 0 );
    AppState getState( int iconId = 0 );

    bool addQuick( const QString &title,
                   const QString &entry,
                   int msg = 1,
                   AppState stat = app_installed );
    bool addQuick( int tid,
                   const QString &entry,
                   int msg = 1,
                   AppState stat = app_installed );

protected:
    template<typename type>
    DsoErr query( int msg, type &val )
    {
        Q_ASSERT( mservName.size() > 0 );

        return serviceExecutor::query( mservName, msg, val );
    }

    template<typename type>
    DsoErr query( const QString &name, int msg, type &val )
    {
        Q_ASSERT( name.size() > 0 );

        return serviceExecutor::query( name, msg, val );
    }

    DsoErr query( int msg,
                  CArgument &valIn,
                  CArgument &valOut )
    {
        Q_ASSERT( mservName.size() > 0 );

        return serviceExecutor::query( mservName, msg, valIn, valOut );
    }

    DsoErr query( const QString &name,
                  int msg,
                  CArgument &valIn,
                  CArgument &valOut )
    {
        Q_ASSERT( name.size() > 0 );

        return serviceExecutor::query( name, msg, valIn, valOut );
    }

public:
    /*! \brief setupUi
    * 创建app的各个子部件，且只调用一次
    */
    virtual void setupUi( CAppMain *pMain );

    /*! \brief retranslateUi
    * 根据语言环境进行资源填充，在进行语言切换后被调用
    */
    virtual void retranslateUi();

    /*!
     * \brief buildConnection
     * build connection
    */
    virtual void buildConnection();

    virtual void resize();

    virtual void registerSpy();

    /*!
     * \brief boundtoService
     * bound to the service
     */
    virtual void boundtoService( service *pServ );
    virtual void linkService();

protected:
    void connectService( service *pServ );
    void connectService( const QString &servName );
    service* findService( const QString &servName );
public:
    bool isServiceActive();
    void serviceActiveIn();

    menu_res::uiWndMgr * getuiWndMgr();

protected:
    void on_enterActive();         //! deactive->active
    void on_exitActive();          //! service exit active
    void on_reEnterActive();       //! active->active

    void on_enterSub();
    void on_exitSub();

    void on_rst();
    void on_pageHide();

protected:
    //! popup widget
    void attachPopupWindow( void *ppWindow, int msg = 0 );
    void attachPopupWindow( QWidget **ppWindow, int msg = 0 );
    void closePopupWindow();

    //! page widget
    void attachPageWindow( void *ppWindow, int msg = 0 );
    void attachPageWindow( QWidget **ppWindow, int msg = 0 );
    void closePageWindow();
    void showPageWindow();
protected:
    void enterActiveWnd();      //! deactive->active
    void activeNextWnd();       //! active->active
    void setActiveWnd( menu_res::uiWnd *pWnd );

    void unloadAllWnd();

protected:
    menu_res::uiWndMgr mWndMgr;
    QList<AppWndHandle*> mAppWindows;

friend class CAppMain;
};

#define enter_app_context() DsoErr _err; bool _bOK = false;
#define exit_app_context()


#endif // CAPP_H
