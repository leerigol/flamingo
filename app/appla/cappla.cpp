#include "cappla.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../service/servch/servch.h"
#include "../../service/servvertmgr/servvertmgr.h"
#include "../../service/servla/servla.h"

#include "../appmain/cappmain.h"

IMPLEMENT_CMD( CApp, CAppLA )
start_of_entry()

on_set_void_void( MSG_LA_ENABLE, &CAppLA::on_dx_changed ),
on_set_void_void( servLa::cmd_dx_changed, &CAppLA::on_dx_changed ),
on_set_void_void( servLa::cmd_dx_changed, &CAppLA::on_dx_pos ),
on_set_void_void( servLa::cmd_group_changed, &CAppLA::on_group_changed ),

on_set_void_void( CMD_SERVICE_VIEW_CHANGE, &CAppLA::on_dx_pos ),
on_set_void_void( MSG_LA_POS, &CAppLA::on_dx_pos ),
on_set_void_void( MSG_LA_AUTO_SET, &CAppLA::on_dx_pos ),
on_set_void_void( MSG_LA_WAVE_SIZE, &CAppLA::on_dx_pos ),

on_set_void_void( servLa::cmd_group_changed, &CAppLA::on_cur_changed ),
on_set_void_void( MSG_LA_CURRENT_CHAN, &CAppLA::on_cur_changed ),

on_set_void_void( MSG_LA_LABEL_VIEW, &CAppLA::on_dx_changed ),
on_set_void_void( MSG_LA_LABEL_VIEW, &CAppLA::on_dx_pos ),

on_set_void_void( MSG_LA_INPUT_LABEL, &CAppLA::on_label_changed ),

on_set_void_void( MSG_LA_LOW_THRE_VAL, &CAppLA::on_thre_changed ),
on_set_void_void( MSG_LA_HIGH_THRE_VAL, &CAppLA::on_thre_changed ),

on_set_void_void( CAppLA::cmd_vert_active, &CAppLA::on_vert_active ),
on_set_void_void( CAppLA::cmd_vert_active, &CAppLA::on_dx_pos ),

on_set_void_void( CMD_SERVICE_ENTER_ACTIVE, &CAppLA::on_enter_active ),
on_set_void_void( CMD_SERVICE_RST, &CAppLA::on_rst ),

on_set_void_void( servLa::cmd_has_la, &CAppLA::setHasLa ),

on_set_void_void( EVT_TIME_MODE_CHANGED, &CAppLA::on_hori_mode ),

end_of_entry()

#define GP1_COLOR   0xcc6600
#define GP2_COLOR   0xcc66ff
#define GP3_COLOR   0xcc9900
#define GP4_COLOR   0xccffff

CAppLA::CAppLA()
{
    mservName = serv_name_la;
}

void CAppLA::setupUi( CAppMain *pMain )
{
    Q_ASSERT( NULL != pMain );

    //! la control
    m_pDsoLA = new menu_res::RDsoLA(pMain->getDownBar());
    Q_ASSERT( m_pDsoLA != NULL );
    m_pDsoLA->loadResource( "ui/la/" );
    m_pDsoLA->hide();

    m_pDsoLA->setHelp( mservName, MSG_LA_ENABLE );

    //! gnds
    QString str;
    QWidget *pWigLeft, *pWigCenter;
    pWigLeft = pMain->getLeftBar();
    pWigCenter = pMain->getCentBar();

    //! metas
    r_meta::CAppMeta meta;
    QString gndPath, gndPattern, gndPost;
    meta.getMetaVal("appla/gnds/path", gndPath );
    meta.getMetaVal("appla/gnds/pattern", gndPattern );
    meta.getMetaVal("appla/gnds/colors", gndPost );

    QStringList colorList = gndPost.split(',');
    int gndColor = 0;
    QString strIcon;

    Q_ASSERT( colorList.size() <= 6 );

    for ( int i = 0; i < array_count(m_pGnds); i++ )
    {
        str = QString("%1").arg( i );

        //! gnds
        {
            //! main
            m_pGnds[i] = new menu_res::RDsoVGnd( str,
                                                 menu_res::RDsoGndTag::to_right,
                                                 menu_res::RDsoGndTag::knife);
            Q_ASSERT( m_pGnds[i] != NULL );
            m_pGnds[i]->setColor( Qt::green );
            m_pGnds[i]->setObjectName(QString::number(i+5));

            gndColor = 0;
            foreach( QString subColor, colorList )
            {
                strIcon = QString( gndPath + gndPattern).arg(i).arg(subColor);
                m_pGnds[i]->attachIcon( gndColor, strIcon, strIcon, strIcon);

                gndColor++;
            }

            m_pGnds[i]->setParent( pWigLeft );
            m_pGnds[i]->setView( 1000, 400 );

            m_pGnds[i]->setAttr( menu_res::RDsoUI::ui_attr_physical );
            sysAttachUIView( m_pGnds[i], view_ygnd );

            m_pGnds[i]->setHelp( mservName, MSG_LA_POS );
        }

        //! labels
        {
            //! main
            m_pLabels[i] = new menu_res::RDsoCHLabel( pWigCenter );
            Q_ASSERT( m_pLabels[i] != NULL );
            m_pLabels[i]->setObjectName(QString::number(i+5));
            m_pLabels[i]->setColor( Qt::green );
            m_pLabels[i]->setView( 1000, 400 );
            m_pLabels[i]->hide();

            m_pLabels[i]->setAttr( menu_res::RDsoUI::ui_attr_physical );
            sysAttachUIView( m_pLabels[i], view_yt );

            m_pLabels[i]->setHelp( mservName, MSG_LA_INPUT_LABEL );
        }
    }

    //! wnd cfg
    m_pWndCfg = new menu_res::RWndLaCfg( pWigCenter );
    Q_ASSERT( NULL!=m_pWndCfg );
}
void CAppLA::retranslateUi()
{

}

void CAppLA::buildConnection()
{
    for ( int i = 0; i < array_count(m_pGnds); i++ )
    {
        //! gnds
        connect( m_pGnds[i],
                 SIGNAL(clicked(bool)),
                 this,
                 SLOT(on_gndx_clicked()) );

        //! labels
        connect( m_pLabels[i],
                 SIGNAL(clicked()),
                 this,
                 SLOT(on_gndx_clicked()) );
    }

    //! header
    connect( m_pDsoLA, SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT( on_header_clicked(QWidget*)) );
    connect( m_pDsoLA, SIGNAL(sig_content_clicked(QWidget*)),
             this, SLOT( on_content_clicked(QWidget*)) );

    //! wnd
    connect( m_pWndCfg,
             SIGNAL(sig_dx_changed(quint32)),
             this,
             SLOT(on_wnd_dx_changed(quint32)));

    connect( m_pWndCfg,
             SIGNAL(sig_threshold_changed(int,int)),
             this,
             SLOT(on_wnd_th_changed(int,int)) );
}

void CAppLA::resize()
{
}

void CAppLA::registerSpy()
{
    spyOn( serv_name_vert_mgr,
           servVertMgr::cmd_active_chan,
           cmd_vert_active );

//    spyOn( serv_name_hori,
//           MSG_HOR_ZOOM_ON,
//           MSG_HOR_ZOOM_ON );
}

void CAppLA::boundtoService( service *pServ )
{
    Q_ASSERT( NULL != pServ );

    m_pServLa = dynamic_cast< servLa* >( pServ );
    Q_ASSERT( NULL != m_pServLa );
}

void CAppLA::on_header_clicked( QWidget* /*pCaller*/ )
{
    if ( m_pDsoLA->getActive() )
    {
        serviceExecutor::post( mservName, MSG_LA_ENABLE, false );
        return;
    }

    //! on now
    if ( m_pServLa->getLaEn() )
    {
        serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, (int)1 );
    }
    else
    {
        serviceExecutor::post( mservName, MSG_LA_ENABLE, true );
    }
}
void CAppLA::on_content_clicked( QWidget* /*pCaller*/ )
{
    //! la enable
    if ( !m_pServLa->getLaEn() )
    {
        serviceExecutor::post( mservName, MSG_LA_ENABLE, true);
        return;
    }

    bool bSetup;
    //! setup ui
    bSetup = m_pWndCfg->setup("appla/wnd/", r_meta::CAppMeta() );
    if ( bSetup )
    {
        m_pWndCfg->setTitle( MSG_LA_ENABLE );
        m_pWndCfg->setTitleColor( m_pServLa->getColor() );

        m_pWndCfg->setLabelId( MSG_LA_THRE_SET, MSG_LA_THRE_SET );
    }

    //! config
    m_pWndCfg->setThreshold( m_pServLa->getLThrevalue(),
                             m_pServLa->getHThrevalue() );
    m_pWndCfg->setDxOnOff( m_pServLa->getDxOnOff() );

    //! visible
    m_pWndCfg->setShow( !m_pWndCfg->isVisible() );

    if ( bSetup )
    {
        m_pWndCfg->move((screen_width-m_pWndCfg->width())/2,290);
    }
}

void CAppLA::on_gndx_clicked()
{
    Chan ch = (Chan)sender()->objectName().toInt();
    serviceExecutor::post(serv_name_la, MSG_LA_CURRENT_CHAN, ch);
    serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, 1 );
}

void CAppLA::on_wnd_dx_changed( quint32 dxs )
{
    serviceExecutor::post( serv_name_la,
                           servLa::cmd_la_state,
                           (int)dxs );
}

void CAppLA::on_wnd_th_changed( int l, int h )
{
    serviceExecutor::post( serv_name_la,
                           MSG_LA_LOW_THRE_VAL, l );

    serviceExecutor::post( serv_name_la,
                           MSG_LA_HIGH_THRE_VAL, h );
}

void CAppLA::on_dx_changed()
{
    //! total visible
    bool bEn = m_pServLa->getLaEn();
    bool bLabelEn = m_pServLa->getLabelView();

    //! close wnd
    if ( !bEn && m_pWndCfg->isVisible() )
    { m_pWndCfg->hide(); }

    //! dx visible
    quint32 dxOnOff;
    bool bView;
    dxOnOff = m_pServLa->getDxOnOff();
    for( int i = d0; i <= d15; i++ )
    {
        bView = bEn && get_bit( dxOnOff, i );

        //! view
        m_pGnds[ i - d0 ]->setShow( bView );

        m_pLabels[ i - d0 ]->setShow( bView && bLabelEn );
    }

    //! do not shift
    m_pWndCfg->setDxOnOff( dxOnOff );

    m_pDsoLA->setOpenLa( bEn ? dxOnOff : 0 );
    if( dxOnOff == 0 || bEn == false )//bug2332 by hxh
    {
        m_pDsoLA->setActive( false );
    }
    else
    {
        m_pDsoLA->setActive( true );
    }
}

void CAppLA::on_group_changed()
{}

void CAppLA::on_dx_pos()
{
    //! get info
    LaScale scale = m_pServLa->getWaveSize();

    int activeMask = m_pServLa->collectActiveMask();
    int openMask = m_pServLa->getDxOnOff();
    int pos[16];

    m_pServLa->collectDxPos( pos );

    //! split
    groupLa gpLa;
    //! shift d0
    gpLa.setDx( openMask>>d0, activeMask>>d0, pos );

    //! main or zoom
    bool bZoom;
    query( serv_name_hori, MSG_HOR_ZOOM_ON, bZoom );
    if ( bZoom )
    { gpLa.split( 479, 320, scale, true ); }
    else
    { gpLa.split( 479, 480, scale, true ); }

    QSize labelSize;
    labelSize = m_pLabels[0]->size();
    int gnd;
    for ( int i = d0; i <= d15; i++ )
    {
        //! physical gnd
        gnd = gpLa.getDxGnd( i-d0, true );

        //! gnd
        m_pGnds[ i - d0 ]->moveBottom( gnd );

        //! label
        m_pLabels[ i - d0]->moveBottom( gnd  );
        if ( i == d0 )
        { LOG_DBG()<<gnd; }
    }
}

void CAppLA::on_cur_changed()
{
    //! init
    {
        //! deactive
        for ( int i = d0; i <= d15; i++ )
        {
            m_pGnds[i - d0]->setColor( Qt::green );
            m_pLabels[ i-d0]->setColor( Qt::green );

            m_pGnds[i - d0]->setIcon( 0 );
        }

        //! group color
        quint32 gpChs;
        QColor gpColors[]={ QColor(GP1_COLOR),
                            QColor(GP2_COLOR),
                            QColor(GP3_COLOR),
                            QColor(GP4_COLOR),
                          };
        int gpIcon[]={2,3,4,5};
        QColor gpColor;
        quint32 gpOnOff;
        for ( int i = la_g1; i <= la_g4; i++ )
        {
            //! group on user group color
            gpOnOff = m_pServLa->getGroupOnOff();
            if ( get_bit( gpOnOff, i) )
            {
                gpChs = m_pServLa->getGroupx( (Chan)i );
                gpColor = gpColors[ i - la_g1 ];

                //! foreach group item
                for ( int j = d0; j <= d15; j++ )
                {
                    if ( get_bit( gpChs, j ) )
                    {
                        m_pGnds[ j - d0 ]->setColor( gpColor );
                        m_pLabels[j-d0]->setColor( gpColor );

                        m_pGnds[ j - d0]->setIcon( gpIcon[ i - la_g1] );
                    }
                }
            }
        }
    }

    //! active now
    Chan curCh = (Chan)m_pServLa->getCurCh();
    if ( curCh >= d0 && curCh <= d15 )
    {
        //! active ch
        m_pGnds[ curCh - d0 ]->setColor( Qt::red );
        m_pLabels[ curCh-d0]->setColor( Qt::red );

        m_pGnds[ curCh - d0 ]->setIcon( 1 );

        //! only one d
        m_pDsoLA->setActiveDx( (1<<curCh) );
    }
    else if ( curCh >= la_g1 && curCh <= la_g4 )
    {
        //! active gp
        quint32 activeDxS = m_pServLa->getGroupx( curCh );

        //! group dxs
        for ( int i = d0; i <= d15; i++ )
        {
            if ( get_bit( activeDxS, i) )
            {
                m_pGnds[ i - d0 ]->setColor( Qt::red );
                m_pLabels[ i-d0]->setColor( Qt::red );

                m_pGnds[ i - d0 ]->setIcon( 1 );
            }
        }

        m_pDsoLA->setActiveDx( activeDxS );
    }
    else
    {
        m_pDsoLA->setActiveDx( chan_none );
    }

    //! active ?
    if ( isServiceActive() )
    { activeSelected(); }
}

void CAppLA::on_label_changed()
{
    DsoLa *pLa;
    QString str;
    for ( int i = d0; i <= d15; i++ )
    {
        pLa = m_pServLa->getDx( (Chan)i );
        Q_ASSERT( NULL != pLa );

        str = pLa->getLabel();

        m_pLabels[ i - d0 ]->setText( str );
    }
}

void CAppLA::on_thre_changed()
{
    int h,l;

    h = m_pServLa->getHThrevalue();
    l = m_pServLa->getLThrevalue();

    m_pWndCfg->setThreshold( l, h );
}

void CAppLA::on_vert_active()
{
    int actiChan;
    Chan actChan;
    serviceExecutor::query( serv_name_vert_mgr,
                            servVertMgr::cmd_active_chan,
                            actiChan );

    actChan = (Chan)actiChan;

    m_pDsoLA->setActive( actChan == la );
    if ( actChan == la )
    {
        on_enter_active();
    }
}

void CAppLA::on_enter_active()
{
    //! all to top
    for ( int i = d0; i <= d15; i++ )
    {
        m_pGnds[ DX_TO_INDEX(i) ]->setActive();
        m_pLabels[ DX_TO_INDEX(i) ]->setActive();
    }

    activeSelected();
    on_dx_changed();

    //! raise the current again
    CAppLA::on_enterActive();
}

void CAppLA::on_rst()
{
    Q_ASSERT( m_pWndCfg != NULL );

    m_pWndCfg->rstArrange();
}

void CAppLA::activeSelected()
{
    //! active now
    Chan curCh = (Chan)m_pServLa->getCurCh();
    if ( curCh >= d0 && curCh <= d15 )
    {
        //! active ch
        m_pGnds[ curCh - d0 ]->setActive();
        m_pLabels[ curCh-d0]->setActive();
    }
    else if ( curCh >= la_g1 && curCh <= la_g4 )
    {
        //! active gp
        quint32 activeDxS = m_pServLa->getGroupx( curCh );

        //! group dxs
        for ( int i = d0; i <= d15; i++ )
        {
            if ( get_bit( activeDxS, i) )
            {
                m_pGnds[ i - d0 ]->setActive();
                m_pLabels[ i-d0]->setActive();
            }
        }
    }
    else
    {
    }
}

void CAppLA::setHasLa()
{
    bool bNow;
    bNow = sysCheckLicense(OPT_MSO);
    if(!bNow)
        m_pDsoLA->hide();
    else
        m_pDsoLA->show();
}

void CAppLA::on_hori_mode()
{
    int mode;
    serviceExecutor::query( serv_name_hori,
                            MSG_HOR_TIME_MODE,
                            mode );

    if ( mode == Acquire_XY && m_pServLa->getLaEn() )
    {
        on_dx_changed();
    }
    else
    {
    }
}
