#ifndef CAPPLA_H
#define CAPPLA_H

#include <QtCore>
#include <QtWidgets>

#include "../capp.h"

#include "../../menu/rmenus.h"  //! controlers

#include "../../service/servla/servla.h"

#include "../../menu/dsownd/rwndlacfg.h"

class CAppLA : public CApp
{
    Q_OBJECT
    DECLARE_CMD()

public:
    enum cmd
    {
        cmd_app_none = CMD_APP_BASE,
        cmd_vert_active,
    };

public:
    CAppLA();

public:
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();

    virtual void buildConnection();
    virtual void resize();
    virtual void registerSpy();
    virtual void boundtoService( service *pServ );

protected Q_SLOTS:
    void on_header_clicked( QWidget* );
    void on_content_clicked( QWidget* );

    void on_gndx_clicked();

    void on_wnd_dx_changed( quint32 dxs );
    void on_wnd_th_changed( int l, int h );

protected:
    void on_dx_changed();
    void on_group_changed();

    void on_dx_pos();
    void on_cur_changed();

    void on_label_changed();
    void on_thre_changed();

    void on_vert_active();
    void on_enter_active();
    void on_rst();

    void activeSelected();

    void setHasLa();

    void on_hori_mode();

private:
    menu_res::RDsoLA *m_pDsoLA;

    //! show at zoom in zoom mode
    menu_res::RDsoVGnd *m_pGnds[16];

    menu_res::RDsoCHLabel *m_pLabels[16];

    menu_res::RWndLaCfg *m_pWndCfg;

    servLa *m_pServLa;

};

#endif // CAPPLA_H
