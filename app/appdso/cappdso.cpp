#include "cappdso.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../service/servdso/servdso.h"
#include "../../app/appdisplay/cgrid.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppDso )
start_of_entry()

on_set_void_void( servDso::CMD_VIEW_CHANGE,   &CAppDso::on_view_changed ),
on_set_void_void( MSG_DISPLAY_WAVE_INTENSITY, &CAppDso::on_intensity_changed ),

end_of_entry()


QList< menu_res::RDsoView* > CAppDso::_mViewList;

/*!
 * \brief sysAttachUIView
 * \param pUI
 * \param viewId
 * UI 元素放置到视图中
 * - ui元素被视图进行统一管理，例如视图的大小变化，显示/关闭
 */
void sysAttachUIView( menu_res::RDsoUI *pUI, DsoView viewId )
{
    Q_ASSERT( NULL != pUI );
    CAppDso::attachUI( pUI, viewId );
}

void CAppDso::attachUI( menu_res::RDsoUI *pUI, DsoView viewId )
{
    Q_ASSERT( pUI != NULL );

    menu_res::RDsoView *pView;

    //! detach from previous
    pView = pUI->getDsoView();
    if ( pView != NULL )
    {
        pView->detachUI( pUI );
    }

    pView = findView( viewId );
    //! create a new one
    if ( pView == NULL )
    {
        pView = new menu_res::RDsoView( viewId );
        Q_ASSERT( pView != NULL );

        CAppDso::_mViewList.append( pView );
    }

    //! attach UI
    pView->attachUI( pUI );
    pUI->setDsoView( pView );
}

/*!
 * \brief CAppDso::findView
 * \param viewId
 * \return
 * 查找视图
 */
menu_res::RDsoView* CAppDso::findView( DsoView viewId )
{
    menu_res::RDsoView *pView;

    foreach( pView, CAppDso::_mViewList )
    {
        Q_ASSERT( pView != NULL );

        if ( pView->mView == viewId )
        {
            return pView;
        }
    }

    //! find none
    return NULL;
}
/*!
 * \brief CAppDso::view
 * \param viewId
 * \return
 * 通过索引访问视图
 */
menu_res::RDsoView* CAppDso::view( DsoView viewId )
{
    menu_res::RDsoView *pView;

    pView = CAppDso::findView( viewId );
    if ( NULL == pView )
    {
        pView = new menu_res::RDsoView( viewId );
        Q_ASSERT( pView != NULL );

        CAppDso::_mViewList.append( pView );
    }

    return pView;
}

CAppDso::CAppDso()
{
    mservName = serv_name_dso;

    m_pIntensityWnd = NULL;
    m_pTimer = NULL;

    /////////////////////////////////////
//    on_init();
}

CAppDso::~CAppDso()
{
    menu_res::RDsoView *pView;

    foreach( pView, CAppDso::_mViewList )
    {
        Q_ASSERT( pView != NULL );

        delete pView;
    }
}

void CAppDso::registerSpy()
{
    spyOn( serv_name_display,
           MSG_DISPLAY_WAVE_INTENSITY,
           (eSpyType)(e_spy_post | e_spy_user) );
}

void CAppDso::on_timeout()
{
}

void CAppDso::on_init()
{
}

void CAppDso::on_tick()
{
    serviceExecutor::releaseUi();
}

void CAppDso::on_view_changed()
{
    //! change view phy && set view visible
    int mode;
    DsoScreenMode scrMode;

    query( servDso::CMD_SCREEN_MODE, mode );
    scrMode = (DsoScreenMode)mode;

    //! changed
    serviceExecutor::broadcast( CMD_SERVICE_VIEW_CHANGE, (int)mode );

    //! config view
    if ( scrMode == screen_yt_main  ||
         scrMode == screen_roll_main )

    {
        view( view_yt )->setPhy(0,0,1000,480 )->setVisible(true);
        view( view_yt_main )->setPhy(0,0,1000,480 )->setVisible(true);
        view( view_yt_zoom )->setVisible( false );

        view( view_xy )->setVisible( false );
        view( view_xy_xgnd )->setVisible( false );
        view( view_xy_ygnd )->setVisible( false );

        view( view_fft_main )->setPhy(0,0,23,480)->setVisible( true );
        view( view_fft_zoom )->setVisible( false );

        view( view_ygnd )->setPhy(0,0,23,480)->setVisible(true);
        view( view_ygnd_main )->setPhy(0,0,23,480)->setVisible(true);
        view( view_ygnd_zoom )->setVisible( false );

        view( view_tgnd_main )->setPhy(0,0,1000,7)->setVisible( true );
        view( view_tgnd_zoom )->setVisible(false);

        view( view_h_main )->setPhy(0,7,1000,13)->setVisible(true);
        view( view_h_zoom )->setVisible(false);

        view( view_lv_gnd_main )->setPhy(1014,0,12,480)->setVisible(true);
        view( view_lv_gnd_zoom )->setVisible(false);

    }
    else if ( scrMode == screen_yt_main_zoom )
    {
        LOG_DBG();

        view( view_yt )->setPhy(0,0,1000,480 )->setVisible(true);
        view( view_yt_main )->setPhy(0,0,1000,144 )->setVisible(true);
        view( view_yt_zoom )->setPhy(0,160,1000,320 )->setVisible( true );

        view( view_xy )->setVisible( false );
        view( view_xy_xgnd )->setVisible( false );
        view( view_xy_ygnd )->setVisible( false );

        view( view_fft_main )->setPhy(0,0,23,144)->setVisible( true );
        view( view_fft_zoom )->setPhy(0,160,23,320)->setVisible( true );

        view( view_ygnd )->setPhy(0,0,23,480)->setVisible(true);
        view( view_ygnd_main )->setPhy(0,0,23,144)->setVisible(true);
        view( view_ygnd_zoom )->setPhy(0,160,23,320)->setVisible( true );

        view( view_tgnd_main )->setPhy(0,0,1000,7)->setVisible( true );
        view( view_tgnd_zoom )->setPhy(0,160,1000,13)->setVisible(true);

        view( view_h_main )->setPhy(0,7,1000,7)->setVisible(true);
        view( view_h_zoom )->setPhy(0,160+7,1000,13)->setVisible(true);

        view( view_lv_gnd_main )->setPhy(1014,0,12,144)->setVisible(true);
        view( view_lv_gnd_zoom )->setPhy(1014,160,12,320)->setVisible(true);

        LOG_DBG();
    }
    //! main + zfft
    else if ( scrMode == screen_yt_main_zfft )
    {
        view( view_yt )->setPhy(0,0,1000,480 )->setVisible(true);
        view( view_yt_main )->setPhy(0,0,1000,144 )->setVisible(true);
        view( view_yt_zoom )->setPhy(0,160,1000,320 )->setVisible( false );

        view( view_xy )->setVisible( false );
        view( view_xy_xgnd )->setVisible( false );
        view( view_xy_ygnd )->setVisible( false );

        view( view_fft_main )->setVisible( false );
        view( view_fft_zoom )->setPhy(0,160,23,320)->setVisible( true );

        view( view_ygnd )->setPhy(0,0,23,480)->setVisible(true);
        view( view_ygnd_main )->setPhy(0,0,23,144)->setVisible(true);
        view( view_ygnd_zoom )->setPhy(0,160,23,320)->setVisible( false );

        view( view_tgnd_main )->setPhy(0,0,1000,7)->setVisible( true );
        view( view_tgnd_zoom )->setPhy(0,160,1000,13)->setVisible(false);

        view( view_h_main )->setPhy(0,7,1000,7)->setVisible(true);
        view( view_h_zoom )->setPhy(0,160+7,1000,13)->setVisible(false);

        view( view_lv_gnd_main )->setPhy(1014,0,12,144)->setVisible(true);
        view( view_lv_gnd_zoom )->setPhy(1014,160,12,320)->setVisible(false);
    }
    else if ( scrMode == screen_xy_full )
    {
        LOG_DBG();

        view( view_yt )->setVisible( false );
        view( view_yt_main )->setVisible( false );
        view( view_yt_zoom )->setVisible( false );

        view( view_xy )->setPhy(237,0,480,480)->setVisible( true );
        view( view_xy_xgnd )->setPhy(260-23,0,480,23)->setVisible( true );
        view( view_xy_ygnd )->setPhy(260-23,0,23,480)->setVisible( true );

        view( view_fft_main )->setVisible( false );
        view( view_fft_zoom )->setVisible( false );

        view( view_ygnd )->setVisible(false);
        view( view_ygnd_main )->setVisible( false );
        view( view_ygnd_zoom )->setVisible( false );

        view( view_tgnd_main )->setVisible( false );
        view( view_tgnd_zoom )->setVisible( false );

        view( view_h_main )->setVisible( false );
        view( view_h_zoom )->setVisible( false );

        view( view_lv_gnd_main )->setVisible( false );
        view( view_lv_gnd_zoom )->setVisible( false );

        LOG_DBG();
    }
    else if ( scrMode == screen_xy_normal )
    {
        LOG_DBG();

        view( view_yt )->setPhy(0,0,1000,144 )->setVisible(true);
        view( view_yt_main )->setPhy(0,0,1000,144 )->setVisible(true);
        view( view_yt_zoom )->setVisible( false );

        view( view_xy )->setPhy(340,160,320,320)->setVisible( true );
        view( view_xy_xgnd )->setPhy(340,160,320,23)->setVisible( true );
        view( view_xy_ygnd )->setPhy(340,160,23,320)->setVisible( true );

        view( view_fft_main )->setVisible( false );
        view( view_fft_zoom )->setVisible( false );

        view( view_ygnd )->setPhy(0,0,23,144)->setVisible(true);
        view( view_ygnd_main )->setPhy(0,0,23,144)->setVisible(true);
        view( view_ygnd_zoom )->setVisible( false );

        view( view_tgnd_main )->setPhy(0,0,1000,7)->setVisible( true );
        view( view_tgnd_zoom )->setVisible(false);

        view( view_h_main )->setPhy(0,7,1000,13)->setVisible(true);
        view( view_h_zoom )->setVisible(false);

        view( view_lv_gnd_main )->setPhy(1014,0,12,144)->setVisible(true);
        view( view_lv_gnd_zoom )->setVisible(false);

        LOG_DBG();
    }
    else
    {
        //Q_ASSERT(false);
        qDebug() << "invalid screen mode";
        return;
    }

    //on_grid_changed( scrMode );
}

void CAppDso::on_intensity_changed()
{
    if ( m_pIntensityWnd == NULL )
    {
        m_pIntensityWnd = new menu_res::RIntensityWnd();
        Q_ASSERT( NULL != m_pIntensityWnd );
    }

    int intens;
    DsoErr err;
    err = query( serv_name_display, MSG_DISPLAY_WAVE_INTENSITY, intens );
    if ( err != ERR_NONE )
    { return; }

    m_pIntensityWnd->setIntensity( intens-1 );
    m_pIntensityWnd->show();
}

void CAppDso::on_grid_changed(DsoScreenMode scrMode)
{
    CGrid *m_pGrid = CGrid::getCGridInstance();
    if( m_pGrid != NULL )
    {
        if ( scrMode == screen_yt_main
             || scrMode == screen_roll_main
             )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(false);
            m_pGrid->setXYFull(false);
        }
        else if ( scrMode == screen_yt_main_zoom )
        {
            m_pGrid->setZoom(true);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(false);
            m_pGrid->setXYFull(false);
        }
        else if ( scrMode == screen_yt_main_zfft )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(true);
            m_pGrid->setFFTFull(false);

            m_pGrid->setXYMode(false);
            m_pGrid->setXYFull(false);
        }
        else if ( scrMode == screen_xy_full )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(true);
            m_pGrid->setXYFull(true);
        }
        else if ( scrMode == screen_xy_normal )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(true);
            m_pGrid->setXYFull(false);
        }
        else
        {}

        if( sysGetStarted() )
        {
            QCoreApplication::processEvents();
        }
    }
}
