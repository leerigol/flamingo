#ifndef CAPPDSO_H
#define CAPPDSO_H


#include "../capp.h"

#include "../../menu/rmenus.h"  //! controlers

#include "../../menu/wnd/rintensitywnd.h"

/*!
 * \brief The CAppDso class
 * servdso 对应的界面实现
 */
class CAppDso : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:                         //! 视图列表
    static QList< menu_res::RDsoView* > _mViewList;
    static void attachUI( menu_res::RDsoUI *pUI, DsoView viewId );

protected:
    static menu_res::RDsoView* findView( DsoView viewId );
    static menu_res::RDsoView* view( DsoView viewId );
public:
    CAppDso();
    ~CAppDso();

public:
    virtual void registerSpy();

protected Q_SLOTS:
    void on_timeout();

protected:
    void on_init();
    void on_tick();

    void on_view_changed();
    void on_intensity_changed();

    void on_grid_changed( DsoScreenMode scrMode );

protected:
    menu_res::RIntensityWnd *m_pIntensityWnd;

    QTimer *m_pTimer;

};

#endif // CAPPDSO_H
