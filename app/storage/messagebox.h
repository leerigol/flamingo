#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QWidget>
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;
class MessageBox : public uiWnd
{
    Q_OBJECT
public:
    explicit MessageBox(QString title,QString content,QWidget *parent = 0);

    void setTitle(QString& t){ mTitle = t;}
    void setContent(QString& c){ mContent = c;}

    void setGeoRect(QRect r);
    void setTitleRect(QRect r);
    void setInfoRect(QRect r);
    void setUpdateLang();

protected:
    void mouseMoveEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *evt);
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString path);

    void tuneInc( int keyCnt );
    void tuneDec( int keyCnt );
    void tuneZ( int keyCnt );

public:
    QPushButton *yesBtn;
    QPushButton *noBtn;
    RImageButton *m_pCloseButton;

signals:
    void btnClicked(int );

public slots:
    void emitYes();
    void emitNo();

public:
    QString egDeleted, egContinue, egOverWitten;
    QString egInvalidPkg, egInvalidVer;
    QString egDownload, egNoNewFW;

    QString chDeleted, chContinue, chOverWitten;
    QString chInvalidPkg, chInvalidVer;
    QString chDownload, chNoNewFW;

private:
    static menu_res::RModelWnd_Style _bgStyle;
    bool    isIconHidden;

    QString mTitle;
    QString mContent;
    QString yesQss;
    QString noQss;
    QString leftStr;
    QString midStr;
    QString rightStr;
    QString iconStr;
    QRect   iconRect;

    QRect   nGeo;
    QRect   nCloseRect;
    QRect   nYesRect;
    QRect   nNoRect;
    QRect   nTitleRect;
    QRect   nInfoRect;

};

#endif // MESSAGEBOX_H
