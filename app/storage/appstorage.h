#ifndef CAPPSTORAGE_H
#define CAPPSTORAGE_H
#include <QMessageBox>
#include "../capp.h"
#include "../appmain/cappmain.h"
#include "../../service/servstorage/servstorage.h"
#include "filedialog.h"
#include "rhttp.h"
#include "messagebox.h"

class CAppStorage : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

    enum
    {
        CHECKING_NEW_FIRMWARE = 0x8000,
        DOWNLOAD_NEW_FIRMWARE,
    };
public:
    CAppStorage();

public:
    void setupUi( CAppMain *pMain );
    void retranslateUi();
    void buildConnection();

protected:
    void registerSpy();

private Q_SLOTS:
    void onDirReturn();
    void onDirEnter();
    void onIndexChanged(int);
    void onDownFinish(QNetworkReply::NetworkError);
    void onUploadFinish(QNetworkReply::NetworkError);
    void postCloseSig();

private:
    int  onSubMenu(int);
    int  onReturn(int);
    int  onFileFormat(AppEventCause);

    int  onVisible(int);   
    int  onUsbEvent(int);

    int  updateFileName(bool bUpdate = true);
    int  updatePathName(bool bUpdate = true);

    int  onNewFolder(void);
    int  onNewFolderPre( int );

    int  onDelete(int);
    int  onRename(int);
    int  onCopy(void);
    int  onPaste(int);

    int  onSave(int);
    int  onSavePre(int);

    int  onUpgrade(int);

    int  onRedraw(int);
    int  onProc(int);
    int  onPrefixChanged(int);
    void setFilter(int);

private Q_SLOTS:
    void onDeleteSlot(int c);
    void onPasteSlot(int c);
    void onSaveSlot(int c);
    void onUpgradeSlot(int c);
    void onMessageSlot(int);


private: //api for online upgrading
    int  checkingFirmware(int);
    int  downloadFirmware(int);
    bool download(const QString& remoteURL,
                  const QString& localPath,
                  bool b = false);

    bool upload(const QString& remoteURL,
                const QByteArray &datae);

    int  parseXML(const QString&);
    int  checkModel(QString &newModel);

private:
    CFileDialog*    m_pDialog;
    int             m_nSubMenu;

    QHash<int,QString> m_hExt;

    CHttp*          m_pDownHttp;
    CHttp*          m_pUpHttp;

    int             m_downType;
    QString         m_remoteURL;

    QString         m_uploadURL;
    QByteArray      m_uploadData;

    QList<QString>  m_supportModels;

    MessageBox *msgBox;
};
#endif // STORAGE

