#include <QDebug>
#include "rhttp.h"
#include "../../service/servstorage/storage_api.h"

CHttp::CHttp(QObject *, bool showWin)
{
    m_bShowWin = showWin;
    m_pSession = new QNetworkAccessManager(this);
    m_pResponse= NULL;
    m_pRequest = NULL;
    m_pTimer   = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(handleTimeOut()));
}

CHttp::~CHttp()
{
    m_pTimer->deleteLater();
    m_pSession->deleteLater();

    if(m_pResponse)
    {
        m_pResponse->deleteLater();
    }

    if( m_pRequest)
    {
        delete m_pRequest;
        m_pRequest = NULL;
    }
}

/* get file from url which we need to download, and restore to filePath */
bool CHttp::download(const QUrl &url, const QString &filePath)
{
    /* confirm the url is valid or not */
    if (!url.isValid())
    {
        qDebug() << (QString("Error:URL has specify a invalid name."));
        return false;
    }

    if (url.scheme() != "http")
    {
        qDebug() << (QString("Error:URL must start with 'http:'"));
        return false;
    }

    if (url.path().isEmpty())
    {
        qDebug() << (QString("Error:URL's path is empty."));
        return false;
    }

    if (filePath.isEmpty())
    {
        qDebug() << (QString("Error:invalid filePath."));
        return false;
    }

    m_File.setFileName(filePath);
    if (!m_File.open(QIODevice::WriteOnly | QIODevice::Truncate) )
    {
        qDebug() << QString("Error:Cannot open file.") << filePath;
        return false;
    }

    m_ServerURL    = url;
    m_FirmwarePath = filePath;
    m_bStarted     = false;


    m_pResponse = m_pSession->get(QNetworkRequest(m_ServerURL));
    connect(m_pResponse, SIGNAL(readyRead()),
              this, SLOT(slotReadyRead()));

    connect(m_pResponse, SIGNAL(downloadProgress(qint64, qint64)),
              this, SLOT(replyDownloadProgress(qint64, qint64)));

    connect(m_pResponse, SIGNAL(error(QNetworkReply::NetworkError)),
              this, SLOT(slotError(QNetworkReply::NetworkError)));

    connect(m_pResponse, SIGNAL(finished()),
              this, SLOT(replyFinished()) );
    return true;
}


//////  url = QUrl("http://www.rigol.com/Support/Upload/?act=up&filename=statistic.dat"
/// ///data 是文件内容, 一个字节数组  类似  char data[n];
bool CHttp::upload(const QUrl &url, const QByteArray &data)
{
    ///confirm the url is valid or not
    if (!url.isValid())
    {
        qDebug() << (QString("Error:URL has specify a invalid name."));
        return false;
    }

    if (url.scheme() != "http")
    {
        qDebug() << (QString("Error:URL must start with 'http:'"));
        return false;
    }

    if (url.path().isEmpty())
    {
        qDebug() << (QString("Error:URL's path is empty."));
        return false;
    }


    m_ServerURL    = url;
    m_pRequest = new QNetworkRequest( m_ServerURL );
    m_pRequest->setHeader(QNetworkRequest::ContentTypeHeader, "application/octet-stream");
    m_pResponse = m_pSession->post(*m_pRequest,data);


    connect(m_pResponse, SIGNAL(finished()),
            this, SLOT(uploadFinished()));

//    connect(m_pResponse, SIGNAL(uploadProgress(qint64, qint64)),
//            this, SLOT(replyUploadProgress(qint64, qint64)));

    connect(m_pResponse, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(uploadError(QNetworkReply::NetworkError)));



//    QEventLoop loop;
//    m_qEventLoop = &loop;
//    m_qEventLoop->exec();
//    m_qEventLoop = NULL;
//    disconnect(m_pResponse, SIGNAL(finished()),
//               this, SLOT(uploadFinished()));
//    disconnect(m_pResponse, SIGNAL(uploadProgress(qint64, qint64)),
//               this, SLOT(replyUploadProgress(qint64, qint64)));
//    disconnect(m_pResponse, SIGNAL(error(QNetworkReply::NetworkError)),
//               this, SLOT(slotUploadError(QNetworkReply::NetworkError)));
    return true;
}

/* slots */
void CHttp::handleTimeOut()
{
    slotError(QNetworkReply::TimeoutError );
}

/* download finished */
void CHttp::replyFinished()
{
    if(m_pResponse->error() == QNetworkReply::NoError)
    {
        slotError( m_pResponse->error() );
    }
}

/* downloading... */
void CHttp::replyDownloadProgress(qint64 done, qint64 total)
{
    total = 100000000;
    //qDebug()<<QString("%1%").arg(done*100/total);
    if (m_bShowWin && (total > 0 ))
    {
        if(!m_bStarted)
        {
            servFile::initProgress(QString("Downloading..."),QString("Upgradation"), total);
            m_bStarted = true;
        }
        servFile::setProgress(done);
    }
}

/* if this is not been fired for 30s,
 * we trade this timeout,
 *  */
void CHttp::slotReadyRead()
{
    if( -1 != m_File.write( m_pResponse->readAll() ) )
    {
        if (m_pTimer->isActive())
        {
            m_pTimer->stop();
        }
        m_pTimer->start(20000);/* wait 30 seconds */
    }
    else
    {
        qDebug() << "Download: no space.";
    }
}

/* handle error */
void CHttp::slotError(QNetworkReply::NetworkError e)
{
    //qDebug()<<"error:"<<e;
    if(m_bShowWin)
    {
        servFile::hideProgress();
    }

    if(e != QNetworkReply::NoError)
    {
       //m_File.remove();
    }

    m_File.close();
    m_pTimer->stop();

    emit sigFinish(e);
}

void CHttp::uploadFinished()
{
    uploadError( QNetworkReply::NoError );
}

void CHttp::uploadError(QNetworkReply::NetworkError e)
{
    emit sigFinish(e);
}
