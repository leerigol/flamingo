#include <QPainter>
#include <QDebug>
#include "../../include/dsoassert.h"
#include "filedialog.h"
#include "../../service/servstorage/storage_api.h"
#include "../../service/servstorage/servstorage.h"

#define DebugPrint() //qDebug() << __FUNCTION__ << __LINE__

CFileDialog::CFileDialog(QWidget *parent):uiWnd(parent)
{
    m_PathName = "/";

    m_nTopIndex = 0;
    m_bSave  = true;
    UI.selected = 0;

    isMoved = false;
    addTuneKey();

//    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

CFileDialog::~CFileDialog()
{
}

void CFileDialog::changeLanguage()
{
    UI.changeLanguage();
}

void CFileDialog::setupUI()
{

    UI.setParent(this);
    UI.loadResource("storage/dialog/");
    wnd.loadResource("ui/wnd/model/");
    setVisible(false);

    setGeometry(UI.mFrame);

    UI.bar.setMinimum(0);
    UI.bar.setPageStep(1);
    UI.bar.setSingleStep(1);

    connect(&UI.bar,
            SIGNAL(actionTriggered(int)),
            this,
            SLOT(onActionTriggered(int)));

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(UI.close_rect);
    m_pCloseButton->setImage( UI.mNormalImg,
                               UI.mDownImg,
                               UI.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SIGNAL(onCloseSig()) );

}

void CFileDialog::saveDialog(QString &dir, QString &fn, QFileInfoList* fi)
{
    m_bSave     = true;
    m_PathName  = dir;
    m_FileName  = fn;

    m_stFiles = *fi;
    selectFile();
}

void CFileDialog::loadDialog(QString &dir, QString &fn, QFileInfoList* fi)
{
    m_bSave     = false;
    m_PathName  = dir;
    m_FileName  = fn;

    m_stFiles = *fi;

    selectFile();
}


void CFileDialog::tuneDec(int keyCnt)
{
    moveNext(keyCnt);
}

void CFileDialog::tuneInc(int keyCnt)
{
    movePrev(keyCnt);
}

void CFileDialog::tuneZ(int)
{
    emit onDirEnter();
}

void CFileDialog::setPathName(const QString &d)
{
    //qDebug() << m_PathName << d;
    if(m_PathName.compare(d))
    {
        m_PathName  = d;
        //reset to the first item when change dir
        UI.selected = 0;
        UI.bar.setValue( UI.selected );
    }
}

void CFileDialog::showUpdate(void *ptr)
{
    if(ptr != NULL)
    {
        QFileInfoList* f = (QFileInfoList*)ptr;
        m_stFiles.clear();
        if(f->size())
        {
            m_stFiles = *f;
        }
    }
    if(this->isVisible())
    {
        selectFile();
        setSelectIndex();
    }
}

void CFileDialog::setFileName(QString &fn)
{
    m_FileName=fn;
}

void CFileDialog::getSelFileName(QString &fn)
{
    if( UI.bar.value() < m_stFiles.size() )
    {
        fn = m_stFiles.at(UI.bar.value()).baseName();
    }
}

void CFileDialog::selectFile()
{
    if(m_stFiles.size())
    {        
        UI.bar.setMaximum(m_stFiles.size()-1);
        //UI.bar.setValue(UI.selected);
    }
    else
    {
        UI.bar.setMaximum(0);
    }    
}

void CFileDialog::sizeToString(quint64 bytes, QString &s8Size)
{
    if(bytes >= 1024)
    {
        QStringList szUnits;
        quint64 left;
        quint64 div = 1000;
        szUnits << "KB" << "MB" << "GB" << "TB" ;
        // Tune from bytes to KB, to MB, to GB and so on diving by 1024
        for(int i=0; i<szUnits.size(); i++)
        {
            left = bytes / div;
            if( left < 1024)
            {
                s8Size = QString("%1 %2").arg(left).arg(szUnits.at(i));
                break;
            }
            else
            {
                div *= 1000;
            }
        }
    }
    else
    {
        s8Size = QString("%1 B").arg(bytes);
    }
}

int CFileDialog::getSelectIndex(QPoint p)
{
    int index = ( p.y() - UI.col_y ) / UI.col_h;
    return index;
}

void CFileDialog::mouseReleaseEvent(QMouseEvent *m)
{
    if(isMoved)
    {
        isMoved = false;
    }
    else
    {
        QPoint p = m->pos();
        if(UI.contentArea.contains(p))
        {
            int nShowCount = qMin(m_stFiles.size()-m_nTopIndex, UI.row_count);
            int nSel = getSelectIndex(m->pos());
            if( nSel < nShowCount)
            {
                UI.selected = nSel ;
                UI.bar.setValue(UI.selected + m_nTopIndex);
                setSelectIndex();
            }
        }

        if(UI.contentArea.contains(p) && m_stFiles.size())
        {
            int nShowCount = qMin(m_stFiles.size()-m_nTopIndex, UI.row_count);
            int nSel = getSelectIndex(m->pos());
            if( nSel >= nShowCount)
            {
                return;
            }
            emit onDirEnter();
        }
        else if(UI.backArea.contains(p))
        {
            emit onDirReturn();
        }
        else if(UI.pathArea.contains(p))
        {
            qDebug() << "ON PATH" << __LINE__;
        }
    }
}

void CFileDialog::mouseMoveEvent(QMouseEvent *evt)
{
    if(!isMoved)
    {
        isMoved = !isMoved;
        startPos = evt->pos();
    }
    else
    {
        QPoint tempP = evt->pos()-startPos;
        if( tempP.y() > 10)
        {
            startPos = evt->pos();

            int pos = UI.bar.sliderPosition();
            if(pos < m_stFiles.size())
            {
                if( pos<=0)
                {
                    m_nTopIndex = pos;
                    UI.selected = 0;
                }
                else if( (UI.selected < UI.row_count ) && (m_nTopIndex>0) )
                {
                    UI.selected = UI.selected+1;
                    if(UI.selected > UI.row_count-1)
                    {
                        UI.selected = UI.row_count-1;
                    }
                    m_nTopIndex = m_nTopIndex - 1;
                    UI.bar.setValue(UI.selected + m_nTopIndex);
                }
            }

            //Need to notify
            emit onIndexChanged(UI.selected + m_nTopIndex);
            update();
        }
        else if( tempP.y() < -10 )
        {
            startPos = evt->pos();

            int pos = UI.bar.sliderPosition()+1;
            if(pos < m_stFiles.size())
            {
                if( pos > m_nTopIndex && (m_nTopIndex<m_stFiles.size()-UI.row_count) )
                {
                    UI.selected = UI.selected-1;
                    if(UI.selected < 0)
                    {
                        UI.selected = 0;
                    }
                    m_nTopIndex = m_nTopIndex + 1;
                    UI.bar.setValue(UI.selected + m_nTopIndex);
                }
            }

            //Need to notify
            emit onIndexChanged(UI.selected + m_nTopIndex);
            update();
        }
    }
}

///
/// \brief Set the selected item index.
/// \param pos
///
void CFileDialog::setSelectIndex()
{
    int pos = UI.bar.sliderPosition();
    if(pos < m_stFiles.size())
    {
        //before top
        if(pos < m_nTopIndex)
        {
            m_nTopIndex = pos;
            UI.selected = 0;
        }
        else if(pos >= m_nTopIndex && pos < (m_nTopIndex + UI.row_count) )
        {
            UI.selected = pos - m_nTopIndex;
        }
        else
        {
            m_nTopIndex = pos + 1 - UI.row_count;
        }
    }
    else
    {
        if(m_stFiles.size())
        {
            UI.selected = m_stFiles.size() - 1;
        }
        else
        {
            UI.selected = 0;
        }
    }

    //Need to notify
    emit onIndexChanged(UI.selected + m_nTopIndex);

    update();
}

void CFileDialog::resetIndex()
{
    UI.selected = 0;
    UI.bar.setMaximum(0);
    setSelectIndex();
}

void CFileDialog::movePrev(int step)
{
    int pos = UI.bar.sliderPosition() - step;
    if(pos < 0)
    {
        pos = 0;
    }
    UI.bar.setValue( pos );
    setSelectIndex();
}

void CFileDialog::moveNext(int step )
{    
    int pos = UI.bar.sliderPosition() + step;
    UI.bar.setValue( pos );
    setSelectIndex();
}

void CFileDialog::wheelEvent(QWheelEvent *m)
{
    QPoint p = m->pos();
    if(UI.contentArea.contains(p))
    {
        if(m->delta() > 0)//up
        {
            movePrev();
        }
        else //down
        {
            moveNext();
        }
    }
}


void CFileDialog::onActionTriggered(int)
{
    setSelectIndex();
}

void CFileDialog::onImeName(const QString &str)
{
    serviceExecutor::post(serv_name_storage,
                          servStorage::FUNC_STORAGE_RENAME,
                          str);
}

void CFileDialog::onImeFolder(const QString &str)
{
    serviceExecutor::post(serv_name_storage,
                          servStorage::FUNC_STORAGE_NEWFOLDER,
                          str);
}

void CFileDialog::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //! font
    QFont font = qApp->font();
    painter.setFont( font );

    QRect r = rect();
    wnd.paint(painter, r);

    bool root = false;
    if( m_stFiles.size() > 0)
    {
        const QFileInfo* f = &( list_at( m_stFiles,0));
        if(f->fileName().compare("..") != 0)
        {
            root = true;
        }
    }
    UI.paint(painter, root);

    r.setY(UI.col_y);
    r.setHeight(UI.col_h);

#if 1
    QString fsize;
    QString fname;
    QImage image;

    int nShowCount = qMin(m_stFiles.size()-m_nTopIndex, UI.row_count);
    for(int i=0; i<nShowCount; i++)
    {
        const QFileInfo* f = &( list_at( m_stFiles,(i + m_nTopIndex)) );
        fname = f->fileName();

        //draw icon
        if(f->isDir())
        {
            if(fname.compare("..") == 0)
            {
                image.load( UI.icons.value("up") );
            }
            else
            {                
                fname = f->absoluteFilePath();
                //qDebug() << fname << STORAGE_USER_PATH;
                if(fname.compare(STORAGE_USER_PATH) == 0)
                {
                    image.load(UI.icons.value("local"));
                }
                //C,D...disk
                else if(fname.size() == STORAGE_USB_PATH.size() &&
                        fname.compare(STORAGE_USB_PATH) >= 0)
                {
                    image.load(UI.icons.value("usb"));
                }
                else
                {
                    image.load( UI.icons.value("folder"));
                }
                //read the file name
                fname = f->fileName();
            }
        }
        else
        {
            QString suffix = f->suffix().toLower();
            if(UI.icons.contains( suffix ))
            {
                image.load( UI.icons.value(suffix) );
            }
            else
            {
                image.load( UI.icons.value("un"));
            }
        }

        painter.drawImage( UI.icon_x, UI.icon_y + r.y(), image );

        r.setX(UI.icon_x + image.width() + 2);
        r.setWidth(UI.col_title_w[0] - image.width() - 2 - UI.icon_x);

        {
            ///
            /// \brief transfer the real path to display Disk
            ///
            QString fn  = fname;
            if(f->isDir())// /media/sda1 -> Removable Disk
            {
              fn = servStorage::getFrendlyDisk(f->filePath());
              if(fn.size() == 0)
              {
                  fn = fname;
              }
            }
            painter.drawText(r, Qt::AlignLeft | Qt::AlignVCenter,fn);
        }

        r.setX(r.x()+r.width() + UI.col_line_w+15); //Line
        r.setWidth(UI.col_title_w[1]);

        if(!f->isDir())
        {
            sizeToString(f->size(), fsize);
            painter.drawText(r, Qt::AlignCenter , fsize);
        }

        r.setX(r.x()+r.width() + UI.col_line_w);//Line
        r.setWidth(UI.col_title_w[2]);
        painter.drawText(r, Qt::AlignCenter ,
                         f->lastModified().toString("yy-MM-dd HH:mm:ss"));

        r.setY(r.y() + r.height());
        r.setHeight(UI.col_h);
    }

    //draw file path name
    {
        QString fn = m_PathName;

        if(m_bSave)
        {
            fn = fn +  "/" + m_FileName;
        }
        fn = servStorage::getFrendlyPath(fn);
        UI.setPathText( fn );
    }
#endif
}
void CFileDialogStyle::paint(QPainter &painter, bool root)
{
    QPen pen;
    QRect r = mFrame;
    r.setX(17);
    r.setY(39);
    r.setWidth(mFrame.width()-34);
    r.setHeight(28);
//    painter.fillRect( r,  caption_bg_color );


    //Draw back icon
    QImage image;
    if( !root )
    {
        image.load(back_icon);
        painter.drawImage( (back_icon_w-image.width())/2+7, (back_icon_h-image.height())/2, image );
        pathText.setGeometry(path_x, path_y, path_w, path_h);
    }
    else
    {
        pathText.setGeometry(path_x-30, path_y, path_w, path_h);
    }

    //Line
//    r.setX(9);
//    r.setY(hr_y);
//    r.setWidth(mFrame.width()-18);
//    r.setHeight(hr_h);
//    painter.fillRect( r,  hr_color );

    //Col Title
    pen.setColor(title_color);
    painter.setPen(pen);

    r.setY(col_y);
    r.setHeight(col_h);
    r.setX(17);
    r.setWidth( col_w - 3);

    for(int i=0; i<row_count; i++)
    {
        if(i == selected)
        {
            painter.fillRect( r,  row_bg_select );
        }
        else
        {
            painter.fillRect( r,  row_bg_color[i&1] );
        }

        r.setY(r.y() + r.height());
        r.setHeight(col_h);
    }

    r.setY(col_title_y);
    r.setHeight(col_title_h);
    r.setX(17);
    r.setWidth(col_title_w[0]);

    image.load(col_line);
    for(int i=0; i<col_count; i++)
    {
        //qDebug() << r;
        painter.fillRect( r,  title_bg_color );
        painter.drawText(r, Qt::AlignCenter , col_title[i]);

        if(i < (col_count-1))
        {
//            painter.drawImage(r.x()+r.width(),r.y(), image);
            painter.fillRect(r.x()+r.width(),r.y(), 2, 430, QColor(50,50,50,255));
        }

        r.setX(r.x()+r.width()+col_line_w);
        r.setWidth(col_title_w[i+1]);
    }


}

void CFileDialogStyle::setParent(QWidget *parent)
{
    pathText.setParent(parent);
    bar.setParent(parent);
}

void CFileDialogStyle::load(const QString &path,
                            const r_meta::CMeta &meta )
{

    int color;
    int x, y, w, h;
    meta.getMetaVal( path + "x", x );
    meta.getMetaVal( path + "y", y );
    meta.getMetaVal( path + "w", w );
    meta.getMetaVal( path + "h", h );
    mFrame.setRect( x, y, w, h );

    meta.getMetaVal( path + "scroll_x", scroll_x );
    meta.getMetaVal( path + "scroll_y", scroll_y );
    meta.getMetaVal( path + "scroll_w", scroll_w );
    meta.getMetaVal( path + "scroll_h", scroll_h );
    bar.setGeometry(scroll_x, scroll_y, scroll_w, scroll_h);


    meta.getMetaVal( path + "caption_bg_color" , color);
    caption_bg_color = QColor(color);

    meta.getMetaVal( path + "back_icon" , back_icon);
    meta.getMetaVal( path + "back_icon_x" , back_icon_x);
    meta.getMetaVal( path + "back_icon_y" , back_icon_y);
    meta.getMetaVal( path + "back_icon_w" , back_icon_w);
    meta.getMetaVal( path + "back_icon_h" , back_icon_h);
    backArea.setRect(back_icon_x, back_icon_y, back_icon_w, back_icon_h);

    meta.getMetaVal( path + "button/rect" , close_rect);
    meta.getMetaVal( path + "button/down", mDownImg );
    meta.getMetaVal( path + "button/normal", mNormalImg );
    meta.getMetaVal( path + "button/disable", mDisableImg );

    meta.getMetaVal( path + "path_x", path_x );
    meta.getMetaVal( path + "path_y", path_y );
    meta.getMetaVal( path + "path_w", path_w );
    meta.getMetaVal( path + "path_h", path_h );
    meta.getMetaVal( path + "path_color" , color);
    path_color = QColor(color);

    changeLanguage();
    pathArea.setRect(path_x, path_y, path_w, path_h);



    meta.getMetaVal( path + "hr_y" ,  hr_y );
    meta.getMetaVal( path + "hr_h" ,  hr_h );
    meta.getMetaVal( path + "hr_color" ,  color );
    hr_color = QColor(color);

    col_title_y = hr_y + hr_h;
    meta.getMetaVal( path + "col_title_h" ,  col_title_h );

    meta.getMetaVal( path + "col_count" , col_count);
    Q_ASSERT(col_count < 8);
    meta.getMetaVal( path + "row_count" , row_count);
    meta.getMetaVal( path + "col_line"  , col_line);
    meta.getMetaVal( path + "col_line_w"  , col_line_w);

    col_y = col_title_h + col_title_y;
    meta.getMetaVal( path + "col_h" ,  col_h );
    contentArea.setRect(0,col_y, w, col_h * row_count);

    col_w = 0;
    for(int i=1; i<= col_count; i++)
    {
        meta.getMetaVal( path + QString("col%1_title").arg(i) ,  col_title[i-1] );
        meta.getMetaVal( path + QString("col%1_title_w").arg(i) ,  col_title_w[i-1] );
        col_w +=  col_title_w[i-1];
    }


    meta.getMetaVal( path + "title_color" , color);
    title_color = QColor(color);

    meta.getMetaVal( path + "title_bg_color" , color);
    title_bg_color = QColor(color);

    meta.getMetaVal( path + "row_color" , color);
    row_color  = QColor(color);

    meta.getMetaVal( path + "row_bg_color1" , color);
    row_bg_color[0]  = QColor(color);

    meta.getMetaVal( path + "row_bg_color2" , color);
    row_bg_color[1]  = QColor(color);

    meta.getMetaVal( path + "row_bg_select" , color);
    row_bg_select  = QColor(color);

    meta.getMetaVal( path + "icon_x" , icon_x);
    meta.getMetaVal( path + "icon_y" , icon_y);

#define GET_ICON_NAME(fmt) meta.getMetaVal( path + "icon_" + fmt , icon_path); \
                                icons[fmt] = icon_path;
    QString icon_path;
    GET_ICON_NAME("png");
    GET_ICON_NAME("bmp");
    GET_ICON_NAME("jpg");
    GET_ICON_NAME("jpeg");
    GET_ICON_NAME("tif");

    GET_ICON_NAME("wfm");
    GET_ICON_NAME("bin");
    GET_ICON_NAME("csv");
    GET_ICON_NAME("ref");
    GET_ICON_NAME("stp");

    GET_ICON_NAME("arb");
    GET_ICON_NAME("txt");
    GET_ICON_NAME("gel");
    GET_ICON_NAME("pf");
    GET_ICON_NAME("htm");
    GET_ICON_NAME("rec");

    GET_ICON_NAME("folder");
    GET_ICON_NAME("local");
    GET_ICON_NAME("up");
    GET_ICON_NAME("un");
    GET_ICON_NAME("usb");

    selected = 0;

    setBarStyle();
}

void CFileDialogStyle::setBarStyle()
{
    bar.setStyleSheet("QScrollBar:vertical\
    {\
      width:3px;\
      background:rgba(50,50,50,100%);\
      margin:0px,0px,0px,0px;\
    }\
      QScrollBar::handle:vertical\
    {\
      width:3px;\
      background:rgba(100,100,100,50%);\
      border-radius:1px;  \
      min-height:60;\
    }\
    QScrollBar::handle:vertical:hover\
    {\
      width:3px;\
      background:rgba(100,100,100,25%);  \
      border-radius:1px;\
      min-height:60;\
    }\
    QScrollBar::add-line:vertical  \
    {\
      height:9px;width:3px;\
      border:0px;\
      subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical  \
    {\
      height:9px;width:3px;\
      border:0px;\
      subcontrol-position:top;\
    }\
    QScrollBar::add-line:vertical:hover \
    {\
      height:9px;width:3px;\
      border:0px;\
      subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical:hover \
    {\
      height:9px;width:3px;\
      border:0px;\
      subcontrol-position:top;\
    }\
    QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical   \
    {\
      background:rgba(0,0,0,10%);\
      border-radius:1px;\
    }");
}

void CFileDialogStyle::changeLanguage()
{
    QString style;

    if( sysGetLanguage() != language_korean )
    {
        style = QString("QLabel{background-color: transparent; \
                color: #FFFFFF;\
                border-radius:6px; \
                border:none; \
                }");
    }
    else
    {
        style = QString("QLabel{background-color: transparent; \
        color: #FFFFFF;\
        border-radius:6px; \
        border:none; \
        font: 12pt \"Courier MonoThai\";\
        }");

    }
    pathText.setStyleSheet(style);
}
