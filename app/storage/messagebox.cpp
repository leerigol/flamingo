#include "messagebox.h"
#include "../../service/servdso/sysdso.h"
#include "../../meta/crmeta.h"

menu_res::RModelWnd_Style MessageBox::_bgStyle;
MessageBox::MessageBox(QString title,QString content, QWidget */*parent*/) : uiWnd(sysGetMainWindow()),
    mTitle(title),mContent(content)
{
    this->setGeometry(sysGetMainWindow()->geometry());
    this->setAttribute(Qt::WA_DeleteOnClose,true);
    loadResource("storage/info/");
    _bgStyle.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(nCloseRect);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(emitNo()) );

    yesBtn  = new QPushButton(this);
    yesBtn->setStyleSheet(yesQss);
    yesBtn->setText("Yes");
    connect(yesBtn, SIGNAL(clicked()),this,SLOT(emitYes()));
    yesBtn->setFocusPolicy(Qt::TabFocus);
    yesBtn->setFocus();
    yesBtn->setGeometry(nYesRect);

    noBtn   = new QPushButton(this);
    connect(noBtn, SIGNAL(clicked()),this,SLOT(emitNo()));
    noBtn->setStyleSheet(noQss);
    noBtn->setText("No");
    noBtn->setFocusPolicy(Qt::TabFocus);
    noBtn->setGeometry(nNoRect);
    isIconHidden = false;

    addAllKeys();
}

void MessageBox::setGeoRect(QRect r)
{
    nGeo = r;
    update();
}

void MessageBox::setTitleRect(QRect r)
{
    nTitleRect = r;
    update();
}

void MessageBox::setInfoRect(QRect r)
{
    nInfoRect = r;
    update();
}

void MessageBox::setUpdateLang()
{
    if ( sysGetLanguage() == language_chinese)
    {
        yesBtn->setText("是");
        noBtn->setText("否");
    }
    else
    {
        yesBtn->setText("Yes");
        noBtn->setText("No");
    }
}

void MessageBox::mouseMoveEvent(QMouseEvent *)
{}
void MessageBox::keyPressEvent(QKeyEvent *)
{}

void MessageBox::keyReleaseEvent(QKeyEvent *evt)
{
    Q_ASSERT( evt != NULL );
    int key, cnt;
    key = evt->key();
    cnt = evt->count();
    if ( key == R_Pkey_FInc )
    { tuneInc(cnt); }
    else if ( key == R_Pkey_FDec )
    { tuneDec(cnt); }
    else if ( key == R_Pkey_FZ )
    { tuneZ(cnt); }
    else if ( key == R_Pkey_PageReturn )
    { emitNo(); }
}

void MessageBox::paintEvent(QPaintEvent */*event*/)
{
    QPainter painter(this);
    QImage img;

    int widthTemp1 = _bgStyle.mImgLtc.width();//topleft 17 17
    int heightTemp1 = _bgStyle.mImgLtc.height();

    int widthTemp2 = _bgStyle.mImgRtc.width();//topright 4 4
    int heightTemp2 = _bgStyle.mImgRtc.height();

    int widthTemp3 = _bgStyle.mImgLdc.width();//bottomleft 4 4
    int heightTemp3 = _bgStyle.mImgLdc.height();

    int widthTemp4 = _bgStyle.mImgRdc.width();//bottomright 14 14
    int heightTemp4 = _bgStyle.mImgRdc.height();

    int lb,tb,rb,db;
    lb=tb=rb=db=0;

    tb = _bgStyle.mImgTb.height();
    for( int i = 0; i<nGeo.width()-widthTemp1-widthTemp2; i++ ){
        painter.drawImage(nGeo.x()+widthTemp1+i, nGeo.y(), _bgStyle.mImgTb );
    }

    lb = _bgStyle.mImgLb.width();
    for( int i = 0; i<nGeo.height()-heightTemp1-heightTemp3; i++ ){
        painter.drawImage(nGeo.x(), nGeo.y()+heightTemp1+i, _bgStyle.mImgLb);
    }

    db = _bgStyle.mImgDb.height();
    for(int i = 0; i < nGeo.width()-widthTemp3-widthTemp4; i++){
        painter.drawImage(nGeo.x()+widthTemp3+i, nGeo.y()+nGeo.height()-db-1, _bgStyle.mImgDb);
    }

    rb = _bgStyle.mImgRb.width();
    for(int i = 0; i < nGeo.height()-heightTemp2-heightTemp4; i++){
        painter.drawImage(nGeo.x()+nGeo.width()-widthTemp2, nGeo.y()+heightTemp2+i, _bgStyle.mImgRb);
    }

    painter.fillRect(nGeo.x()+lb, nGeo.y()+tb, nGeo.width()-lb-rb, nGeo.height()-tb-db, _bgStyle.mBgColor);

    painter.drawImage(nGeo.x(), nGeo.y(), _bgStyle.mImgLtc);
    painter.drawImage(nGeo.x()+nGeo.width()-widthTemp2, nGeo.y(), _bgStyle.mImgRtc);//
    painter.drawImage(nGeo.x(), nGeo.y()+nGeo.height()-heightTemp3-1, _bgStyle.mImgLdc);
    painter.drawImage(nGeo.x()+nGeo.width()-widthTemp4, nGeo.y()+nGeo.height()-heightTemp4-1, _bgStyle.mImgRdc);


    img.load(iconStr);
    painter.drawImage(iconRect.x(), iconRect.y(), img);

    if(sysGetLanguage() == language_thailand )
    {
        painter.setFont(QFont("Droid Sans Fallback"));
    }

    painter.setPen(QPen(Qt::white));
    painter.drawText(nTitleRect, Qt::AlignLeft, mTitle);
    painter.drawText(nInfoRect, Qt::TextWordWrap, mContent);
}

void MessageBox::loadResource(const QString path)
{
    r_meta::CRMeta meta;
    QString str = path ;
    meta.getMetaVal(str + "yesqss", yesQss);
    meta.getMetaVal(str + "noqss", noQss);
    meta.getMetaVal(str + "iconrect", iconRect);

    meta.getMetaVal(str + "leftstr", leftStr);
    meta.getMetaVal(str + "midstr", midStr);
    meta.getMetaVal(str + "rightstr", rightStr);
    meta.getMetaVal(str + "iconstr", iconStr);

    meta.getMetaVal(str + "egDeleted", egDeleted);
    meta.getMetaVal(str + "chDeleted", chDeleted);
    meta.getMetaVal(str + "egContinue", egContinue);
    meta.getMetaVal(str + "chContinue", chContinue);
    meta.getMetaVal(str + "egOverWitten", egOverWitten);
    meta.getMetaVal(str + "chOverWitten", chOverWitten);
    meta.getMetaVal(str + "egInvalidPkg", egInvalidPkg);
    meta.getMetaVal(str + "chInvalidPkg", chInvalidPkg);
    meta.getMetaVal(str + "egInvalidVer", egInvalidVer);
    meta.getMetaVal(str + "chInvalidVer", chInvalidVer);
    meta.getMetaVal(str + "egDownload", egDownload);
    meta.getMetaVal(str + "chDownload", chDownload);
    meta.getMetaVal(str + "egNoNewFW", egNoNewFW);
    meta.getMetaVal(str + "chNoNewFW", chNoNewFW);

    meta.getMetaVal(str + "normal/closerect", nCloseRect);
    meta.getMetaVal(str + "normal/geometry", nGeo);
    meta.getMetaVal(str + "normal/yesrect", nYesRect);
    meta.getMetaVal(str + "normal/norect", nNoRect);
    meta.getMetaVal(str + "normal/titlerect", nTitleRect);
    meta.getMetaVal(str + "normal/inforect", nInfoRect);

}

void MessageBox::tuneInc( int /*keyCnt*/ )
{
    if(yesBtn->hasFocus())
        noBtn->setFocus();
    else
        yesBtn->setFocus();
}
void MessageBox::tuneDec( int /*keyCnt*/ )
{
    if(yesBtn->hasFocus())
        noBtn->setFocus();
    else
        yesBtn->setFocus();
}
void MessageBox::tuneZ( int /*keyCnt*/ )
{
    if(yesBtn->hasFocus())
        emitYes();
    else
        emitNo();//qxl
}

void MessageBox::emitYes()
{
    emit btnClicked(1);
    this->close();
}


void MessageBox::emitNo()
{
    emit btnClicked(0);
    this->close();
}
