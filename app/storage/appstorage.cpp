
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../com/virtualkb/virtualkb.h"
#include "../../com/keyevent/ckeyscan.h"
#include "appstorage.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppStorage )
start_of_entry()


on_set_int_int (        MSG_STORAGE_IMAGE_FORMAT,           &CAppStorage::onFileFormat),
on_set_int_int (        MSG_STORAGE_WAVE_FORMAT,            &CAppStorage::onFileFormat),
on_set_int_int (        MSG_STORAGE_FILETYPE,               &CAppStorage::onFileFormat),
on_set_int_int (        MSG_STORAGE_PREFIX,                 &CAppStorage::onPrefixChanged),

on_set_int_int (        MSG_STORAGE_DIALOG_VISIBLE,         &CAppStorage::onVisible),

on_set_int_int (        servStorage::FUNC_STORAGE_PUSH,     &CAppStorage::onRedraw),
on_set_int_int (        servStorage::FUNC_STORAGE_REDRAW,   &CAppStorage::onRedraw),

on_set_int_int (        MSG_STORAGE_USB_INSERT,             &CAppStorage::onUsbEvent),
on_set_int_int (        MSG_STORAGE_USB_REMOVE,             &CAppStorage::onUsbEvent),

/* Callback function for saved files */
on_set_int_int (        MSG_STORAGE_RESULT,                 &CAppStorage::onProc),

/* Change the current path when created new folder */
on_set_int_int (        MSG_STORAGE_NEWFOLDER,              &CAppStorage::onNewFolderPre),
on_set_int_int (        servStorage::FUNC_STORAGE_NEWFOLDER,&CAppStorage::onNewFolder),

on_set_int_int (        MSG_STORAGE_DELETE,                 &CAppStorage::onDelete),
on_set_int_int (        servStorage::FUNC_STORAGE_DELETE,   &CAppStorage::onProc),

on_set_int_int (        servStorage::FUNC_STORAGE_PASTE_PRE,&CAppStorage::onPaste),
on_set_int_int (        servStorage::FUNC_STORAGE_PASTE,    &CAppStorage::onProc),

on_set_int_int (        MSG_STORAGE_SAVE,                   &CAppStorage::onSave),
on_set_int_int (        servStorage::FUNC_STORAGE_SAVE_PRE, &CAppStorage::onSavePre),
on_set_int_int (        servStorage::FUNC_STORAGE_SAVE,     &CAppStorage::onProc),

on_set_int_int (        servStorage::FUNC_UPGRADE_RESULT,   &CAppStorage::onUpgrade),

on_set_int_int (        MSG_STORAGE_RENAME,                 &CAppStorage::onRename),
on_set_int_int (        servStorage::FUNC_STORAGE_RENAME,   &CAppStorage::onProc),


on_set_int_int (        MSG_CHECKING_FIRMWARE,              &CAppStorage::checkingFirmware),
on_set_int_int (        MSG_DOWNLOAD_FIRMWARE,              &CAppStorage::downloadFirmware),

on_set_int_int (        CMD_SERVICE_SUB_ENTER,              &CAppStorage::onSubMenu),
on_set_int_int (        CMD_SERVICE_SUB_RETURN,             &CAppStorage::onReturn),
on_set_int_int (        CMD_SERVICE_EXIT_ACTIVE  ,          &CAppStorage::onReturn),
on_set_int_int (        CMD_SERVICE_DEACTIVE  ,             &CAppStorage::onReturn),
on_set_int_int (        CMD_SERVICE_ACTIVE,                 &CAppStorage::onReturn),

end_of_entry()

#define DebugPrint(p) //qDebug() << __FUNCTION__ << __LINE__ << p

CAppStorage::CAppStorage()
{
    mservName =  serv_name_storage;

//    addQuick(   MSG_APP_STORAGE,
//                QStringLiteral("ui/desktop/storage/icon/") );

    m_pDialog = NULL;

    m_nSubMenu= MSG_STORAGE_SAVE_IMAGE;

    m_supportModels.append("MSO7000");
    m_supportModels.append("DS7000");
}

void CAppStorage::setupUi( CAppMain *pMain )
{
    if(m_pDialog == NULL)
    {
        m_pDialog = new CFileDialog(pMain->getCentBar());
        m_pDialog->setupUI();
    }
}

void CAppStorage::retranslateUi()
{
    if( m_pDialog )
    {
        m_pDialog->changeLanguage();
    }
    //DO NOTHING
}

void CAppStorage::buildConnection()
{
    connect(m_pDialog,
            SIGNAL(onIndexChanged(int)),
            this,
            SLOT(onIndexChanged(int)) );

    connect(m_pDialog,
            SIGNAL(onDirEnter()),
            this,
            SLOT(onDirEnter()) );

    connect(m_pDialog,
            SIGNAL(onDirReturn()),
            this,
            SLOT(onDirReturn()) );

    connect(m_pDialog,
            SIGNAL(onCloseSig()),
            this,
            SLOT(postCloseSig()) );
}


void CAppStorage::registerSpy()
{

}

int CAppStorage::onPrefixChanged(int aec)
{
    if(m_pDialog->isHidden() || aec != cause_value)
    {
        return ERR_NONE;
    }
    DebugPrint(-1);
    updateFileName();
    return ERR_NONE;
}

int CAppStorage::onFileFormat(AppEventCause aec)
{   
    if(aec != cause_value) return ERR_NONE;


    //if the dialog is visible, then filter the other message
    if(m_pDialog->isHidden())
    {
        return ERR_NONE;
    }
    DebugPrint(aec);

    updateFileName();

    return ERR_NONE;
}

int CAppStorage::onSubMenu(int)
{
    Q_ASSERT(m_pDialog);

    //if the dialog is visible, then filter the other message
    if(m_pDialog->isVisible())
    {
        return ERR_NONE;
    }
    DebugPrint(aec);

    QString path    = "";
    QString fname   = "";
    int     subMenu = 0;
    void*   ptr     = NULL;

    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_SUB_MENU,  subMenu);
    if(subMenu == MSG_STORAGE_OPTION)
    {
        return ERR_NONE;
    }
    serviceExecutor::query( serv_name_storage,
                            MSG_STORAGE_PATHNAME, path);
    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_STORAGE_PTR,ptr);

    if(subMenu & servStorage::FUNC_SAVE)//external save operation
    {
        serviceExecutor::query( serv_name_storage,
                                MSG_STORAGE_FILENAME, fname);
        m_pDialog->saveDialog(path,fname, (QFileInfoList*)ptr);
    }
    else if(subMenu != servStorage::FUNC_STORAGE_OPTION)
    {
        m_pDialog->loadDialog(path,fname,(QFileInfoList*)ptr);
    }

    if(subMenu != servStorage::FUNC_STORAGE_OPTION)
    {
        serviceExecutor::post( E_SERVICE_ID_STORAGE,
                               MSG_STORAGE_DIALOG_VISIBLE, true);
    }
    return ERR_NONE;
}


int CAppStorage::onReturn(int /*menu*/)
{
    int menu;
    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_RETURN_MENU,
                            menu);

    if(m_pDialog->isVisible() && menu != MSG_STORAGE_MEM_CSV_MORE)
    {
        m_pDialog->resetIndex();
        m_pDialog->hide();
    }
    return ERR_NONE;
}


int CAppStorage::onVisible(int aec)
{
    if(aec != cause_value) return ERR_NONE;

    bool visible = false;
    serviceExecutor::query( serv_name_storage,
                            MSG_STORAGE_DIALOG_VISIBLE, visible);
    if(visible)
    {
        if(m_pDialog->isHidden())
        {
            DebugPrint(-1);
            if(work_help == sysGetWorkMode())
            {
                return ERR_NONE;;
            }

            void* ptr = NULL;
            QString fname;
            serviceExecutor::query( serv_name_storage,
                                    servStorage::FUNC_STORAGE_PTR,
                                    ptr);
            m_pDialog->showUpdate( ptr );

            serviceExecutor::query( serv_name_storage,
                                    MSG_STORAGE_FILENAME,
                                    fname);
            m_pDialog->setFileName(fname);

            QString pathName;
            serviceExecutor::query( serv_name_storage,
                                    MSG_STORAGE_PATHNAME,
                                    pathName);
            m_pDialog->setPathName(pathName);
            m_pDialog->show();
        }
        else
        {
            m_pDialog->setActive();
        }
    }
    else
    {
        DebugPrint(-1);
        if(m_pDialog->isVisible())
        {
            m_pDialog->hide();
        }
    }
    return ERR_NONE;
}

int CAppStorage::onUsbEvent(int)
{
    //if the dialog is visible, then filter the other message
    if(m_pDialog->isHidden())
    {
        return ERR_NONE;
    }

    updatePathName();
    return ERR_NONE;
}

int CAppStorage::updateFileName(bool bUpdate)
{
    QString fname;
    serviceExecutor::query( serv_name_storage,
                            MSG_STORAGE_FILENAME,
                            fname);
    m_pDialog->setFileName(fname);
    if(bUpdate)
    {
        void* ptr;
        serviceExecutor::query( serv_name_storage,
                                servStorage::FUNC_STORAGE_PTR,
                                ptr);
        DebugPrint(-1);
        m_pDialog->showUpdate(ptr);
    }

    return ERR_NONE;
}

int CAppStorage::updatePathName(bool bUpdate)
{
    QString pathName;
    serviceExecutor::query( serv_name_storage,
                            MSG_STORAGE_PATHNAME,
                            pathName);
    m_pDialog->setPathName(pathName);
    updateFileName(bUpdate);
    return ERR_NONE;
}

void CAppStorage::onDeleteSlot( int c)
{
    if( c )
    {
        serviceExecutor::post( E_SERVICE_ID_STORAGE,
                               servStorage::FUNC_STORAGE_DELETE,0);
    }
}

void CAppStorage::onPasteSlot(int c)
{
    if( c )
    {
        serviceExecutor::post( E_SERVICE_ID_STORAGE,
                               servStorage::FUNC_STORAGE_PASTE,0);
    }
}

void CAppStorage::onSaveSlot(int c)
{
    if( c )
    {
        msgBox->repaint();
        serviceExecutor::post( E_SERVICE_ID_STORAGE,
                               servStorage::FUNC_STORAGE_SAVE_ING,0);
    }
}

void CAppStorage::onUpgradeSlot(int c)
{
    if(c)
    {
        downloadFirmware(0);
    }
    else
    {
        onMessageSlot(c);
    }
}

void CAppStorage::onMessageSlot(int)
{
    int status = 0 ;

    serviceExecutor::query( serv_name_utility_IOset,
                            servInterface::cmd_net_status,
                            status);
    if( status == servInterface::NET_STATUS_CONFIGURED )
    {
        //cancel upgrade, enable the menu item
        serviceExecutor::post(E_SERVICE_ID_HELP,
                              servHelp::cmd_lan_status,
                              true);
    }
}

/**
 * @brief before delete. show confirm box
 * @param cause
 * @return
 */
int CAppStorage::onDelete(int cause)
{
    if(cause == cause_enable || m_pDialog->isHidden() )
    {
        return ERR_NONE;
    }
    msgBox = new MessageBox("","");
    if ( sysGetLanguage() == language_chinese)
    {
        msgBox->setTitle(msgBox->chDeleted);
        msgBox->setContent(msgBox->chContinue);
    }
    else
    {
        msgBox->setTitle(msgBox->egDeleted);
        msgBox->setContent(msgBox->egContinue);
    }
    connect(msgBox, SIGNAL(btnClicked(int)),this,SLOT(onDeleteSlot(int)));
    msgBox->load();
    msgBox->setActive();
    msgBox->show();
    return ERR_NONE;
}

/**
 * @brief before overwrite. show confirm box
 * @param cause
 * @return
 */
int CAppStorage::onPaste(int cause)
{
    if(cause == cause_enable || m_pDialog->isHidden() )
    {
        return ERR_NONE;
    }

    msgBox = new MessageBox("","");
    if ( sysGetLanguage() == language_chinese)
    {
        msgBox->setTitle(msgBox->chOverWitten);
        msgBox->setContent(msgBox->chContinue);
    }
    else
    {
        msgBox->setTitle(msgBox->egOverWitten);
        msgBox->setContent(msgBox->egContinue);
    }
    connect(msgBox, SIGNAL(btnClicked(int)),this,SLOT(onPasteSlot(int)));
    msgBox->load();
    msgBox->setActive();
    msgBox->show();
    return ERR_NONE;
}

int CAppStorage::onSave(int cause)
{
    if(cause == cause_enable)
    {
        return ERR_NONE;
    }

    int subMenu;
    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_SUB_MENU,  subMenu);
    if(subMenu == servStorage::FUNC_SAVE_IMG &&
       m_pDialog && m_pDialog->isVisible() )
    {
        m_pDialog->hide();

        QWidget*p = (QWidget*)m_pDialog->parent();
        p->repaint();

        serviceExecutor::post(E_SERVICE_ID_STORAGE,
                               MSG_STORAGE_DIALOG_VISIBLE, false);
    }

    onProc(cause);

    return ERR_NONE;
}

int CAppStorage::onSavePre(int cause)
{
    if(cause == cause_enable )
    {
        return ERR_NONE;
    }

    msgBox = new MessageBox("","");
    if ( sysGetLanguage() == language_chinese)
    {
        msgBox->setTitle(msgBox->chOverWitten);
        msgBox->setContent(msgBox->chContinue);
    }
    else
    {
        msgBox->setTitle(msgBox->egOverWitten);
        msgBox->setContent(msgBox->egContinue);
    }

    connect(msgBox, SIGNAL(btnClicked(int)),this,SLOT(onSaveSlot(int)));
    msgBox->load();
    msgBox->setActive();
    msgBox->show();

    return ERR_NONE;
}

int CAppStorage::onRename(int cause)
{
    if(cause == cause_enable || m_pDialog->isHidden() )
    {
        return ERR_NONE;
    }

    QString oldFilename = "";
    m_pDialog->getSelFileName( oldFilename );
    //call inputMethod for newer name
    VirtualKB::Show(  m_pDialog,
                      SLOT(onImeName(QString)),
                      QString("Rename"),
                      oldFilename );
    VirtualKB::object()->setMaxLength( servStorage::MAX_FILENAME_LEN );
    return ERR_NONE;
}


int CAppStorage::onNewFolderPre(int cause)
{
    if(cause == cause_enable || m_pDialog->isHidden() )
    {
        return ERR_NONE;
    }

    QString foldName = "";
    query(serv_name_storage, MSG_STORAGE_NEWFOLDER, foldName);

    //call inputMethod for newer name
    VirtualKB::Show(  m_pDialog,
                      SLOT(onImeFolder(QString)),
                      QString("Folder"),
                      foldName );
    VirtualKB::object()->setMaxLength( servStorage::MAX_FILENAME_LEN );

    return ERR_NONE;
}

int CAppStorage::onNewFolder()
{
    DebugPrint(-1);
    int subMenu = 0;
    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_SUB_MENU,  subMenu);
    if(subMenu == servStorage::FUNC_STORAGE_DISK)
    {
        void* ptr;
        serviceExecutor::query( serv_name_storage,
                                servStorage::FUNC_STORAGE_PTR,ptr);
        m_pDialog->showUpdate( ptr );
    }
    else
    {
        updatePathName();
    }
    return ERR_NONE;
}


int CAppStorage::onRedraw(int)
{
    if(m_pDialog->isHidden())
    {
        return ERR_NONE;
    }
    DebugPrint(-1);
    updatePathName();
    return ERR_NONE;
}

//After save,delete,copy and export
int CAppStorage::onProc(int aes)
{   
    //if the dialog is visible, then filter the other message
    if( aes == cause_enable || !m_pDialog->isVisible())
    {
        return ERR_NONE;
    }

    int proc = 0;
    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_THREAD_PROC,
                            proc);

    if( proc != servFile::ProcUpgrading )
    {

        int subMenu = 0;
        void *ptr   = NULL;
        serviceExecutor::query( serv_name_storage, servStorage::FUNC_SUB_MENU,  subMenu);
        serviceExecutor::query( serv_name_storage, servStorage::FUNC_STORAGE_PTR,ptr);
        if(subMenu & servStorage::FUNC_SAVE)
        {
            updatePathName(false); //not call showUpdate
        }
        DebugPrint(-1);
        m_pDialog->showUpdate(ptr);
    }

    return ERR_NONE;
}

int CAppStorage::onUpgrade(int)
{
    int proc = 0;
    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_THREAD_PROC,
                            proc);

    //qDebug() << proc << "update result" << __FUNCTION__;

    if(proc == servFile::ProcUpgrading)
    {
        int result = 0;
        QString title = "Upgrade failed";
        serviceExecutor::query( serv_name_storage,
                                servStorage::FUNC_UPGRADE_RESULT,
                                result);

        msgBox = new MessageBox("","");
        msgBox->setTitle(title);
        if ( sysGetLanguage() == language_chinese)
        {
            //msgBox->setContent(msgBox->chInvalidPkg);
            //if( result == ERR_FILE_OLD_VER)
            {
                msgBox->setContent(msgBox->chInvalidVer);
            }
        }
        else
        {
            //msgBox->setContent(msgBox->egInvalidPkg);
            //if( result == ERR_FILE_OLD_VER)
            {
                msgBox->setContent(msgBox->egInvalidVer);
            }
        }
        connect(msgBox, SIGNAL(btnClicked(int)),this,SLOT(onMessageSlot(int)));

        msgBox->load();
        msgBox->setActive();
        msgBox->show();
    }
    return ERR_NONE;
}

////////////////////////////////////////////////////////////////////
/// \brief SLOT
/// \param index
///
void CAppStorage::onIndexChanged(int index)
{
    //make sure this will be executed at once.
    serviceExecutor::post( E_SERVICE_ID_STORAGE,
                           servStorage::FUNC_STORAGE_INDEX, index);
}

void CAppStorage::onDirEnter()
{
    //qDebug() << "onDirEnter";
    serviceExecutor::post(E_SERVICE_ID_STORAGE,
                          servStorage::FUNC_STORAGE_PUSH,0);
}

void CAppStorage::onDirReturn()
{
    serviceExecutor::post(E_SERVICE_ID_STORAGE,
                          servStorage::FUNC_STORAGE_PUSH,1);
}

//////////////////////////////download///////////////////////////////////////
int CAppStorage::checkingFirmware(int)
{
    bool bAuto = false;
    serviceExecutor::query( serv_name_storage,
                            MSG_CHECKING_FIRMWARE, bAuto);

    r_meta::CRMeta meta;
    QString url;
    if (false == meta.getMetaVal("storage/firmware", url) )
    {
        qDebug() << "can not get the address of download";
        return ERR_FILE_NOT_EXIST;
    }

    for(int i=0; i<url.size(); i++)
    {
        if ( url.at(i).toLatin1() == '$' )
        {
            url.replace(i,1,("&"));
        }
    }

    QString serial = servUtility::getSystemSerial();
    //"DS6A193100028"
    m_remoteURL = QString(url).arg(serial)
                              .arg("1.0")
                              .arg("soft")
                              .arg(DEFAULT_VERSION);


    QString to = "/tmp/firmware.xml";
    m_downType = CHECKING_NEW_FIRMWARE;
    if( false == download(m_remoteURL, to, bAuto) )
    {
        serviceExecutor::post(E_SERVICE_ID_HELP,
                              servHelp::cmd_lan_status, true);
    }


    ///////////////////////////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////////////////////////////



    url = "http://www.rigol.com/up.aspx?act=%1&filename=%2.dat";
    m_uploadURL = QString(url).arg("up").arg(serial);

    QString model   = servUtility::getSystemModel();
    QString version = servUtility::getSystemVersion();

    m_uploadData.clear();
    m_uploadData.append(model);
    m_uploadData.append(version);

    for(int i=0; i<app_key_board::CKeyScan::DATA_KEY_COUNT; i++)
    {
        m_uploadData.append( app_key_board::CKeyScan::m_au32KeyCount[i] );
    }

    upload(m_uploadURL, m_uploadData);
    //
    return ERR_NONE;
}

int CAppStorage::downloadFirmware(int)
{
    QString to = STORAGE_DOWN_PATH  + "firmware.gel";
    m_downType = DOWNLOAD_NEW_FIRMWARE;
    if(false == download(m_remoteURL, to, true) )
    {
        serviceExecutor::post(E_SERVICE_ID_HELP,
                              servHelp::cmd_lan_status, true);
    }
    return ERR_NONE;
}

void CAppStorage::onDownFinish(QNetworkReply::NetworkError e)
{
    QString filePath = m_pDownHttp->getFilePath();

    if(m_pDownHttp != NULL )
    {
        delete m_pDownHttp;
        m_pDownHttp = NULL;
    }

    if(e == QNetworkReply::NoError)
    {
        if(m_downType == CHECKING_NEW_FIRMWARE)
        {
            if( parseXML( filePath ) != ERR_NONE )
            {
                servGui::showInfo( sysGetString(ERR_CHECK_FIRMWARE, "Check error"));
                onMessageSlot(0);
            }
        }
        else if(m_downType ==  DOWNLOAD_NEW_FIRMWARE)
        {
            serviceExecutor::post(E_SERVICE_ID_STORAGE,
                                  servStorage::FUNC_UPGRADE_NET,
                                  filePath);
        }
    }
    else
    {

        servGui::showInfo( sysGetString(ERR_CHECK_FIRMWARE, "Check error"));
        onMessageSlot(0);
    }
}

bool CAppStorage::upload(const QString& remoteURL,
                         const QByteArray &data)
{
    m_pUpHttp = new CHttp(0);
    Q_ASSERT(m_pUpHttp);

    if(m_pUpHttp)
    {
        connect(m_pUpHttp, SIGNAL(sigFinish(QNetworkReply::NetworkError)),
                   this, SLOT(onUploadFinish(QNetworkReply::NetworkError)) );
        return m_pUpHttp->upload(remoteURL, data);
    }
    return false;
}

void CAppStorage::onUploadFinish(QNetworkReply::NetworkError /*e*/)
{
    if( m_pUpHttp != NULL )
    {
        delete m_pUpHttp;
        m_pUpHttp = NULL;
    }

}
void CAppStorage::postCloseSig()
{
    int subMenu;
    serviceExecutor::query( serv_name_storage,
                            servStorage::FUNC_SUB_MENU,  subMenu);
    if( subMenu == servStorage::FUNC_STORAGE_DISK )
    {
        quint32 keyCode = R_Pkey_PageReturn;
        QKeyEvent *pEvt = new QKeyEvent(QEvent::KeyRelease,
                                        keyCode,
                                        Qt::NoModifier,
                                        QString(),
                                        false,
                                        0 );

         qApp->postEvent( qApp, (QEvent*)pEvt );
    }
    else
    {
        serviceExecutor::post(E_SERVICE_ID_STORAGE,
                                MSG_STORAGE_DIALOG_VISIBLE, false);
    }
}

bool CAppStorage::download(const QString& remoteURL,
                           const QString& localPath,
                            bool b)
{
    m_pDownHttp = new CHttp(0,b);
    Q_ASSERT(m_pDownHttp);

    if(m_pDownHttp)
    {
        connect(m_pDownHttp, SIGNAL(sigFinish(QNetworkReply::NetworkError)),
                   this, SLOT(onDownFinish(QNetworkReply::NetworkError)) );
        return m_pDownHttp->download(remoteURL, localPath);
    }
    return false;
}


///
/// \brief CAppStorage::checkModel
/// \param newModel
/// \return
///
int CAppStorage::checkModel(QString &newModel)
{
    int size = m_supportModels.size();
    for(int i=0; i<size; i++)
    {
        if(newModel.left(2).compare(m_supportModels.at(i).left(2)) == 0)
        {
            return 0;
        }
        else if(newModel.left(3).compare(m_supportModels.at(i).left(3)) == 0)
        {
            return 0;
        }
    }
    return 1;
}

#include "../../service//servutility/servutility.h"

int CAppStorage::parseXML(const QString& to)
{
    QFileInfo f(to);
    if( QFile::exists(to) && f.size() > 0)
    {
        r_meta::CXmlMeta meta(to);

        QString comment= "";
        QString model  = "";
        QString version= "";


        QString local_model   =   servUtility::getSystemModel();
        QString local_version =   servUtility::getSystemVersion();

        //qDebug() << __FILE__<<__LINE__ << local_model<<local_version;

        if(false == meta.getMetaVal("firmware/series", model) )
        {
            //qDebug() << "can not get firmware/series";
            model = "MSO7000";
            return ERR_CHECK_FIRMWARE;
        }
        //qDebug() << "new Model:" << model;

        if(false == meta.getMetaVal("firmware/version", version) )
        {
            //qDebug() << "can not get firmware/version";
            return ERR_CHECK_FIRMWARE;
        }
        //qDebug() << "new Version:" << version;

        if(false == meta.getMetaVal("firmware/url", m_remoteURL) )
        {
            //qDebug() << "can not get firmware/url";
            return ERR_CHECK_FIRMWARE;
        }
        //qDebug() << "firmware URL:" << m_remoteURL;

        if ( sysGetLanguage() == language_chinese)
        {
            meta.getMetaVal("firmware/comment_cn", comment);
        }
        else
        {
            meta.getMetaVal("firmware/comment_en", comment);
        }

        if(checkModel(model) == 0)
        {
            QString new_version = version;
            QString local_ver = local_version;
            new_version.remove(0,2);
            local_ver.remove(0,2);

            bool canDown = false;
            if( new_version.size()  == local_ver.size() )
            {
                int ret = new_version.compare(local_ver);
                if(ret > 0)
                {
                    canDown = true;
                }
            }

            if(canDown)
            {
                msgBox = new MessageBox("","");
                if ( sysGetLanguage() == language_chinese)
                {
                    msgBox->setTitle(msgBox->chDownload);
                }
                else
                {
                    msgBox->setTitle(msgBox->egDownload);
                }
                QString content = local_model+"\n"+version;
                msgBox->setContent(content);
                connect(msgBox, SIGNAL(btnClicked(int)),this,SLOT(onUpgradeSlot(int)));
                msgBox->setActive();
                msgBox->setUpdateLang();
                msgBox->show();

                return ERR_NONE;
            }
        }
        else
        {
    //        QString msg = "Mismatch of the Model," + model + "," + local_model;
    //        qDebug() << msg;
            //serviceExecutor::post(serv_name_help, servHelp::cmd_lan_status, true);
        }

        msgBox = new MessageBox("","");
        if ( sysGetLanguage() == language_chinese)
        {
            msgBox->setContent(msgBox->chNoNewFW);
        }
        else
        {
            msgBox->setContent(msgBox->egNoNewFW);
        }
        connect(msgBox, SIGNAL(btnClicked(int)),this,SLOT(onMessageSlot(int)));
        msgBox->load();
        msgBox->setActive();
        msgBox->show();
        return ERR_NONE;
    }
    return ERR_CHECK_FIRMWARE;
}
