#ifndef FILEDIALOG
#define FILEDIALOG
#include <QDir>
#include <QPaintEvent>
#include <QWidget>
#include <QHash>
#include "../../menu/menustyle/rui_style.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../widget/rimagebutton.h"
#include "../capp.h"

using namespace menu_res;
class CFileDialogStyle: public RUI_Style
{
public:
    CFileDialogStyle() {}
    void paint(QPainter &painter, bool root = false);
    void setParent(QWidget *parent);
    void changeLanguage();
public:
    QRect  mFrame;

    int     scroll_x;
    int     scroll_y;
    int     scroll_w;
    int     scroll_h;

    int     selected;
    int     caption_x;
    int     caption_y;
    int     caption_w;
    int     caption_h;
    QColor  caption_bg_color;

    QString back_icon;
    int     back_icon_x;
    int     back_icon_y;
    int     back_icon_w;
    int     back_icon_h;

    QRect   close_rect;
    QString mNormalImg, mDownImg, mDisableImg;

    int     path_x;
    int     path_y;
    int     path_w;
    int     path_h;
    QColor  path_color;

    int     hr_y;
    int     hr_h;
    QColor  hr_color;

    int     col_count;
    int     row_count;

    int     col_y;
    int     col_h;
    int     col_w;

    int     col_title_y;
    int     col_title_h;

    int     col_line_w;
    QString col_line;

    QString col_title[8];
    int     col_title_w[8];

    QColor  title_color;
    QColor  title_bg_color;

    QColor  row_color;
    QColor  row_bg_color[4];
    QColor  row_bg_select;

    QRect   contentArea;
    QRect   backArea;
    QRect   pathArea;

    int     icon_x;
    int     icon_y;
    QHash<QString,QString> icons;

    QLabel    pathText;
    QString   getPathText()           { return pathText.text(); }
    void      setPathText(QString s)  { pathText.setText(s); }

    QScrollBar bar;
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );
    void setBarStyle();
};


class CFileDialog : public uiWnd
{
    Q_OBJECT
public:
    CFileDialog( QWidget *parent = 0);
    ~CFileDialog();

    void setupUI();
    void saveDialog(QString& dir, QString& fn, QFileInfoList*);
    void loadDialog(QString& dir, QString& fn, QFileInfoList*);

    void setPathName(const QString&);
    void setFileName(QString &);
    void getSelFileName(QString &);

    void showUpdate(void *ptr = NULL);

    void movePrev(int step = 1);
    void moveNext(int step = 1);

    void resetIndex();
    void changeLanguage();

Q_SIGNALS:
    void onDirReturn();
    void onDirEnter();
    void onIndexChanged(int);
    void onCloseSig();

protected:
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *evt);
    void mouseReleaseEvent(QMouseEvent *);
    void wheelEvent(QWheelEvent *);

    void tuneInc( int keyCnt );
    void tuneDec( int keyCnt );
    void tuneZ( int keyCnt );

protected Q_SLOTS:
    void onActionTriggered(int);
    void onImeName(const QString &str);
    void onImeFolder(const QString &str);

private:
    void selectFile(void);
    void sizeToString(quint64 bytes, QString &s8Size);

    int  getSelectIndex(QPoint p);
    void setSelectIndex();

private:
    RImageButton   *m_pCloseButton;
    QFileInfoList  m_stFiles;
    bool           m_bSave;

    QString        m_PathName;
    QString        m_FileName;

    /* start index of the all file list for showing */
    int            m_nTopIndex;
private:
    CFileDialogStyle UI;
    RModelWnd_Style wnd;

    bool            isMoved;
    QPoint          startPos;
};

#endif // FILEDIALOG

