#ifndef IHTTPDOWNLOADS_H
#define IHTTPDOWNLOADS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QFile>
#include <QNetworkReply>
#include <QTimer>


class CHttp : public QObject
{
    Q_OBJECT
public:
    explicit CHttp(QObject *parent = 0, bool showWin = false);
    ~CHttp();

    bool    download(const QUrl &url, const QString &filePath);
    bool    upload(const QUrl &url, const QByteArray &data);

    /* get file from url which we need to download, and restore to filePath */
    QString& getFilePath(void) { return m_FirmwarePath; }

signals:
    void sigFinish(QNetworkReply::NetworkError);

public slots:
    void replyFinished(); /* download finished */
    void replyDownloadProgress(qint64, qint64); /* downloading... */
    void slotError(QNetworkReply::NetworkError); /* handle error */
    void slotReadyRead();/* ready read */
    void handleTimeOut(); /* handle time out */

    void uploadFinished();
    void uploadError(QNetworkReply::NetworkError);

private:
    QNetworkAccessManager    *m_pSession;
    QNetworkReply            *m_pResponse;
    QNetworkRequest          *m_pRequest;
    QTimer                   *m_pTimer;

    QUrl                     m_ServerURL;
    QFile                    m_File;
    QString                  m_FirmwarePath;
    bool                     m_bShowWin;
    bool                     m_bStarted;
};

#endif // IHTTPDOWNLOADS_H
