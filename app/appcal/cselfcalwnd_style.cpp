#include "cselfcalwnd_style.h"

#include "../../menu/menustyle/rcontrolstyle.h"
#include "../../meta/crmeta.h"

CSelfcalWnd_Style::CSelfcalWnd_Style()
{
}

void CSelfcalWnd_Style::load( const QString &path,
                              const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "geo/normal", mRectGeo );
    meta.getMetaVal( path + "geo/expand", mExpGeo );

    meta.getMetaVal( path + "expand", mDefExpand );

    meta.getMetaVal( path + "style", mDefStyle );

    loadCommonMeta( meta, path + "notice_caption/", mMetaNoticeCaption );
    loadCommonMeta( meta, path + "notice/", mMetaNotice );

    loadCommonMeta( meta, path + "lasttime_caption/", mMetaLastTimeCaption );
    loadCommonMeta( meta, path + "lasttime/", mMetaLastTime );

    loadCommonMeta( meta, path + "nexttime_caption/", mMetaNextTimeCaption );
    loadCommonMeta( meta, path + "nexttime/", mMetaNextTime );

    loadCommonMeta( meta, path + "result_caption/", mMetaResultCaption );
    loadCommonMeta( meta, path + "result/", mMetaResult );

    loadCommonMeta( meta, path + "status/", mMetaStatus );
    loadCommonMeta( meta, path + "progress/", mMetaProg );

    loadCommonMeta( meta, path + "detail_btn/", mMetaDetailBtn );
    loadCommonMeta( meta, path + "detail_list/", mMetaDetailList );
}

void CSelfcalWnd_Style::loadCommonMeta( const r_meta::CMeta &meta,
                                       const QString &path,
                                       CommonMeta &comMeta )
{
    meta.getMetaVal( path + "rect", comMeta.mRect );
    meta.getMetaVal( path + "style", comMeta.mStyle );
}
