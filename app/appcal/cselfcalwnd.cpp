#include "cselfcalwnd.h"

#include "../../widget/rprogressbar.h"

CSelfcalWnd_Style CSelfcalWnd::_style;
QString CSelfcalWnd::_dateFmt= QLatin1Literal("yyyy/M/d hh:mm:ss");

CSelfcalWnd::CSelfcalWnd()
{
    m_pLabelNoticeCaption = NULL;
    m_pLabelNotice = NULL;

    m_pLabelTimeCaption = NULL;
    m_pLabelTime = NULL;

    m_pLabelNextTimeCaption = NULL;
    m_pLabelNextTime = NULL;

    m_pLabelResultCaption = NULL;
    m_pLabelResult = NULL;

    m_pProgressbar = NULL;

    addTuneKey();
    set_attr( wndAttr, mWndAttr, wnd_moveable );

    setCloseEnable( true );
    setCloseVisible( true );
}

void CSelfcalWnd::setupUi( const QString &path,
                      const r_meta::CMeta &meta )
{
    //! sub controls
    m_pLabelNoticeCaption = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelNoticeCaption );
    m_pLabelNotice = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelNotice );

    m_pLabelTimeCaption = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelTimeCaption );

    m_pLabelTime = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelTime );
    m_pLabelTime->hide();

    m_pLabelNextTimeCaption = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelNextTimeCaption );
    m_pLabelNextTimeCaption->hide();

    m_pLabelNextTime = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelNextTime );
    m_pLabelNextTime->hide();

    m_pLabelResultCaption = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelResultCaption );

    m_pLabelResult = new QLabel( this );
    Q_ASSERT( NULL != m_pLabelResult );
    m_pLabelResult->hide();

    m_pProgressbar = new menu_res::RProgressBar( this );
    Q_ASSERT( NULL != m_pProgressbar );

    m_pDetailBtn = new QPushButton(this);
    Q_ASSERT( NULL != m_pDetailBtn );
    m_pDetailBtn->setVisible(false);


    m_pLabelStatus = new QLabel(this);
    Q_ASSERT( NULL != m_pLabelStatus );
    m_pDetailList = new QListWidget( this );
    Q_ASSERT( NULL != m_pDetailList );
    m_pDetailBtn->setFocusPolicy(Qt::StrongFocus);
    m_pDetailBtn->setFocus();
    m_pDetailList->setFocusPolicy(Qt::NoFocus);
    m_pDetailList->setSelectionMode(QAbstractItemView::NoSelection);

    //! get style
    _style.loadResource( path, meta );

    //! apply style
    if ( _style.mDefStyle.length() > 0 )
    {
        setStyleSheet( _style.mDefStyle );
    }

    applyMeta( m_pLabelNoticeCaption,_style.mMetaNoticeCaption );

    applyMeta( m_pLabelNotice,_style.mMetaNotice );

    applyMeta( m_pLabelTimeCaption,_style.mMetaLastTimeCaption );

    applyMeta( m_pLabelTime,_style.mMetaLastTime );

    applyMeta( m_pLabelNextTimeCaption,_style.mMetaNextTimeCaption );

    applyMeta( m_pLabelNextTime,_style.mMetaNextTime );

    applyMeta( m_pLabelResultCaption,_style.mMetaResultCaption );
    applyMeta( m_pLabelResult,_style.mMetaResult );


    applyMeta( m_pLabelStatus,_style.mMetaStatus );
    applyMeta( m_pProgressbar,_style.mMetaProg );

    applyMeta( m_pDetailBtn,_style.mMetaDetailBtn );
    applyMeta( m_pDetailList,_style.mMetaDetailList );

    setScrollBarStyle();

    //! build connection
    connect( m_pDetailBtn, SIGNAL(clicked(bool)),
             this, SLOT(on_detail_clicked(bool)) );

    //! attr
    bool defExpand = _style.mDefExpand > 0;
    m_pDetailBtn->setCheckable( true );
    m_pDetailBtn->setChecked( defExpand );

    on_detail_clicked( defExpand );
}

void CSelfcalWnd::setScrollBarStyle()
{
    m_pDetailList->verticalScrollBar()->setStyleSheet("QScrollBar:vertical\
    {\
        width:12px;\
        background:rgba(40,40,40,100%);\
        margin:0px,0px,0px,0px;\
        padding-top:5px;   \
        padding-bottom:5px;\
    }\
    QScrollBar::handle:vertical\
    {\
        width:12px;\
        background:rgba(100,100,100,50%);\
        border-radius:4px;  \
        min-height:10;\
    }\
    QScrollBar::handle:vertical:hover\
    {\
        width:12px;\
        background:rgba(100,100,100,25%);  \
        border-radius:4px;\
        min-height:10;\
    }\
    QScrollBar::add-line:vertical  \
    {\
        height:6px;width:12px;\
        border:0px;\
        subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical  \
    {\
        height:6px;width:12px;\
        border:0px;\
        subcontrol-position:top;\
    }\
    QScrollBar::add-line:vertical:hover \
    {\
        height:6px;width:12px;\
        border:0px;\
        subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical:hover \
    {\
        height:9px;width:12px;\
        border:0px;\
        subcontrol-position:top;\
    }\
    QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical   \
    {\
        background:rgba(40,40,40,50%);\
        border-radius:4px;\
    }");
}

void CSelfcalWnd::on_detail_clicked( bool b )
{
    if ( b )
    {
        m_pDetailList->setVisible(b);
        setGeometry( _style.mExpGeo );
    }
    else
    {
        m_pDetailList->setVisible(b);
        setGeometry( _style.mRectGeo );
    }
}

void CSelfcalWnd::show()
{
    return RModalWnd::show();
}
void CSelfcalWnd::hide()
{
    return RModalWnd::hide();
}

void CSelfcalWnd::setNoticeCaption( const QString &str )
{
    m_pLabelNoticeCaption->setText(str);
}
void CSelfcalWnd::setNotice( const QString &str )
{
    m_pLabelNotice->setText(str);
}

void CSelfcalWnd::setLastTimeCaption( const QString &str )
{
    m_pLabelTimeCaption->setText(str);
}
void CSelfcalWnd::setLastTime( const QDateTime &lastTime )
{
    QString str = sysGetString(INF_CAL_LAST_TIME) + lastTime.toString( CSelfcalWnd::_dateFmt );
    //m_pLabelTime->setText( str );
    m_pLabelTimeCaption->setText(str);
}

void CSelfcalWnd::setNextTimeCaption( const QString &str )
{
    m_pLabelNextTimeCaption->setText(str);
}
void CSelfcalWnd::setNextTime( const QDateTime &/*nextTime*/ )
{
    //nextTime.toString(CSelfcalWnd::_dateFmt)
    //m_pLabelNextTime->setText( "Success" );
}

void CSelfcalWnd::setResultCaption( const QString &str )
{
    m_pLabelResultCaption->setText(str);
}
void CSelfcalWnd::setResult( const QString &res )
{
    QString str = sysGetString(INF_CAL_CURR_RESULT) + res;
    //m_pLabelResult->setText(  str );
    m_pLabelResultCaption->setText(str);
}

void CSelfcalWnd::setProgress( int now, int total )
{
    m_pProgressbar->setRange(0,total);
    m_pProgressbar->setValue( now );
}

void CSelfcalWnd::setStatus( const QString &str )
{
    m_pLabelStatus->setText( str );
}

void CSelfcalWnd::appendLog( const QString &str )
{
    m_pDetailList->addItem(str);
}
void CSelfcalWnd::appendLog( const QStringList &strList )
{
    m_pDetailList->addItems(strList);
    m_pDetailList->scrollToBottom();
}
void CSelfcalWnd::clearLog()
{
    m_pDetailList->clear();
}

void CSelfcalWnd::tuneInc(int /*keyCnt*/)
{
    m_pDetailBtn->setFocus();
}

void CSelfcalWnd::tuneDec(int /*keyCnt*/)
{
    m_pDetailBtn->setFocus();
}

void CSelfcalWnd::tuneZ(int /*keyCnt*/)
{
    m_pDetailBtn->setFocus();
    if ( m_pDetailBtn->isChecked() )
    {
        m_pDetailBtn->setChecked(false);
        setGeometry( _style.mRectGeo );
    }
    else
    {
        m_pDetailBtn->setChecked(true);
        setGeometry( _style.mExpGeo );

    }
}

void CSelfcalWnd::applyMeta( QWidget *pWig,
                    CommonMeta &meta )
{
    Q_ASSERT( NULL != pWig );

    pWig->setGeometry( meta.mRect );
    if ( meta.mStyle.length() > 0 )
    {
        pWig->setStyleSheet(meta.mStyle);
    }
}

int CSelfcalWnd::onProjectChanged(bool b)
{
    m_pDetailBtn->setVisible(b);
    return ERR_NONE;
}
