#ifndef APPSELCAL_H
#define APPSELCAL_H
#include <QtWidgets>
#include <QTime>
#include "../capp.h"
#include "cselfcalwnd.h"

class CAppCal : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

private:
    CSelfcalWnd *m_pCalWnd;
public:
    CAppCal();
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();
    virtual void buildConnection();

protected:
    void registerSpy();

    void onActiveChanged();
    void onCalUpdated();
    void onCalStarted( AppEventCause cause );

    void updateLog();

    int  onProjectChanged(bool);

    int  onVisible(int);
    void onSelfStart();

protected Q_SLOTS:
    void postCloseSig();
};
#endif // APPSELCAL_H

