#ifndef CSELFCORWND_H
#define CSELFCORWND_H

#include <QString>
#include <QFont>
#include <QDateTime>
#include <QtWidgets>
#include "../../menu/wnd/rmodalwnd.h"
#include "cselfcalwnd_style.h"


class CSelfcalWnd : public menu_res::RModalWnd
{
    Q_OBJECT

private:
    static CSelfcalWnd_Style _style;
    static QString _dateFmt;

public:
    CSelfcalWnd();

protected:
    virtual void setupUi( const QString &path,
                          const r_meta::CMeta &meta );

protected Q_SLOTS:
    void on_detail_clicked( bool b );

public:
    void show();
    void hide();

    void setNoticeCaption( const QString &str );
    void setNotice( const QString &str );

    void setLastTimeCaption( const QString &str );
    void setLastTime( const QDateTime &lastTime );

    void setNextTimeCaption( const QString &str );
    void setNextTime( const QDateTime &nextTime );

    void setResultCaption( const QString &str );
    void setResult( const QString &res );

    void setProgress( int now, int total );

    void setStatus( const QString &str );

    void appendLog( const QString &str );
    void appendLog( const QStringList &strList );
    void clearLog();

    void tuneInc( int keyCnt );
    void tuneDec( int keyCnt );
    void tuneZ( int keyCnt );

    int  onProjectChanged(bool);

private:
    void applyMeta( QWidget *pLabel,
                    CommonMeta &meta);
    void setScrollBarStyle();
public:
    QString mLastTime;
    QString mExpectNextTime;

private:
    QLabel *m_pLabelNoticeCaption;
    QLabel *m_pLabelNotice;

    QLabel *m_pLabelTimeCaption;
    QLabel *m_pLabelTime;

    QLabel *m_pLabelNextTimeCaption;
    QLabel *m_pLabelNextTime;

    QLabel *m_pLabelResultCaption;
    QLabel *m_pLabelResult;

    QLabel *m_pLabelStatus;

    QProgressBar *m_pProgressbar;

    QPushButton *m_pDetailBtn;
    QListWidget *m_pDetailList;

};
#endif // CSELFCORWND_H

