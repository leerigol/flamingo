#ifndef CSELFCALWND_STYLE_H
#define CSELFCALWND_STYLE_H

#include "../../menu/menustyle/rui_style.h"

class CommonMeta
{
public:
    QString mStyle;
    QRect mRect;
};

class CSelfcalWnd_Style : public menu_res::RUI_Style
{
public:
    CSelfcalWnd_Style();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );
private:
    void loadCommonMeta( const r_meta::CMeta &meta,
                        const QString &path,
                        CommonMeta &labelMeta );

public:
    QRect mRectGeo, mExpGeo;

    QString mDefStyle;

    CommonMeta mMetaNoticeCaption, mMetaNotice;
    CommonMeta mMetaLastTimeCaption, mMetaLastTime;
    CommonMeta mMetaNextTimeCaption, mMetaNextTime;
    CommonMeta mMetaResultCaption, mMetaResult;

    CommonMeta mMetaStatus;

    CommonMeta mMetaProg;

    CommonMeta mMetaDetailBtn;
    CommonMeta mMetaDetailList;

    int mDefExpand;
};

#endif // CSELFCALWND_STYLE_H
