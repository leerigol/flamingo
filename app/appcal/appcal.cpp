#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../gui/cguiformatter.h"

#include "../appmain/cappmain.h"
#include "appcal.h"

IMPLEMENT_CMD( CApp, CAppCal )
start_of_entry()

//on_set_void_int( MSG_SELF_CAL_START,        &CAppCal::onCalStarted ),
on_set_void_void( servCal::CMD_SELF_START,  &CAppCal::onCalStarted),


on_set_void_void( CMD_SERVICE_ACTIVE,       &CAppCal::onActiveChanged ),
on_set_void_void( CMD_SERVICE_EXIT_ACTIVE,  &CAppCal::onActiveChanged ),

on_set_void_void( CMD_SERVICE_ENTER_ACTIVE, &CAppCal::onCalUpdated ),
on_set_void_void( servCal::CMD_CAL_UPDATED, &CAppCal::onCalUpdated ),
on_set_void_void( servCal::CMD_CAL_DETAIL,  &CAppCal::onCalUpdated ),


on_set_int_int  ( MSG_SELF_CAL_WINDOW,      &CAppCal::onVisible),

end_of_entry()

CAppCal::CAppCal()
{
    m_pCalWnd = NULL;
    mservName = serv_name_cal;
}

void CAppCal::setupUi( CAppMain *pMain )
{
    Q_ASSERT( NULL != pMain );
    m_pCalWnd = new CSelfcalWnd();
    Q_ASSERT( NULL != m_pCalWnd );
    m_pCalWnd->setParent(  pMain );

    connect(m_pCalWnd,
            SIGNAL(sigClose()),
            this,
            SLOT(postCloseSig()));

    attachPageWindow( &m_pCalWnd, MSG_SELF_CAL_START );
}
void CAppCal::retranslateUi()
{}
void CAppCal::buildConnection()
{}

void CAppCal::registerSpy()
{
}

void CAppCal::onActiveChanged()
{
    //! filter
    if ( sysGetWorkMode() != work_normal )
    { return; }

    bool bAct;
    query( mservName, CMD_SERVICE_ACTIVE, bAct );

    Q_ASSERT( NULL != m_pCalWnd );

    //! create the window
    r_meta::CAppMeta meta;
    if ( m_pCalWnd->setup("appcal/selfcalwnd/", meta))
    {}

    //! visible
    if ( bAct )
    {
        //! title
        m_pCalWnd->setTitle( "Calibration" );

        //! config info
        m_pCalWnd->setNoticeCaption(
                    sysGetString(INF_CAL_NOTICE));

        m_pCalWnd->setNotice(
                    sysGetString(INF_CAL_INFO) );

//        m_pCalWnd->setLastTimeCaption(
//                    sysGetString(INF_CAL_LAST_TIME));

//        m_pCalWnd->setNextTimeCaption(
//                    sysGetString(INF_CAL_LAST_TIME));

//        m_pCalWnd->setResultCaption(
//                    sysGetString(INF_CAL_CURR_RESULT));

        qint64 lastDatems;
        QDateTime dateTime;

        //! query ms
        query( mservName, servCal::qCMD_CAL_DATE, lastDatems );
        dateTime.setMSecsSinceEpoch( lastDatems );

        m_pCalWnd->setLastTime( dateTime );

//        m_pCalWnd->setNextTimeCaption(
//                    sysGetString(INF_CAL_LAST_RESULT));

        //QDateTime dt = QDateTime::currentDateTime();
        //m_pCalWnd->setNextTime( "Success" );

        m_pCalWnd->setResult("Stop");
        m_pCalWnd->setProgress( 0, 100 );

        bool mode;
        query(serv_name_cal, MSG_APP_UTILITY_PROJECT, mode);
        m_pCalWnd->onProjectChanged( mode );

        if( m_pCalWnd && m_pCalWnd->isVisible() )
        {
            m_pCalWnd->update();
        }
        else
        {
            m_pCalWnd->show();
            serviceExecutor::post( getName(),
                                   MSG_SELF_CAL_WINDOW, true);
        }
    }
    else
    {
        if( servCal::m_bPreparing == false)
        {
            m_pCalWnd->hide();
            serviceExecutor::post( getName(),
                                   MSG_SELF_CAL_WINDOW, false);
        }
        else
        {
            servCal::m_bPreparing = false;
        }
    }
}

void CAppCal::onCalUpdated()
{
    //! no wnd
    if( NULL == m_pCalWnd || !m_pCalWnd->isSetuped() ) return;

    int stat;
    servCal::CalStatus calStat;

    query( mservName, servCal::qCMD_CAL_STATUS, stat );
    calStat = (servCal::CalStatus)stat;

    if ( calStat == servCal::cal_running )
    {
        int total, now;
        query( mservName, servCal::qCMD_CAL_PROGRESS_TOTAL, total );
        query( mservName, servCal::qCMD_CAL_PROGRESS_NOW, now );

        Q_ASSERT( NULL != m_pCalWnd );
        m_pCalWnd->setProgress( now, total );

        updateLog();
    }
    else if ( calStat == servCal::cal_completed )
    {
        int total;
        query( mservName, servCal::qCMD_CAL_PROGRESS_TOTAL, total );

        Q_ASSERT( NULL != m_pCalWnd );
        m_pCalWnd->setProgress( total, total );

        QString result;
        query(mservName, servCal::CMD_CAL_RESULT, result);
        if ( result.compare(CAL_SUCCESS) == 0 )
        {
            m_pCalWnd->setResult( result );
        }
        else
        {
            m_pCalWnd->setResult("Error:" + result );
        }

        updateLog();
    }
    else if ( calStat == servCal::cal_active )
    {}
    else if ( calStat == servCal::cal_deactive )
    {}
    else if ( calStat == servCal::cal_aborted )
    {
        int total;
        query( mservName, servCal::qCMD_CAL_PROGRESS_TOTAL, total );

        Q_ASSERT( NULL != m_pCalWnd );
        m_pCalWnd->setProgress( 0, total );

        QString result = "Abort";
        m_pCalWnd->setResult( result );

        updateLog();
    }
    else
    { Q_ASSERT( false); }
}

void CAppCal::onCalStarted( AppEventCause /*cause*/ )
{
    if(m_pCalWnd && m_pCalWnd->isHidden())
    {
        m_pCalWnd->show();
    }
    m_pCalWnd->clearLog();
    m_pCalWnd->setResult("Waitting");
    m_pCalWnd->update();
}

void CAppCal::updateLog()
{
    do
    {
        void *pointer = NULL;
        query( mservName, servCal::qCMD_CAL_DETAIL, pointer );
        if ( NULL == pointer )
        { break; }

        QStringList *pList = (QStringList*)pointer;
        m_pCalWnd->appendLog( *pList );

    }while(0);
}


void CAppCal::postCloseSig()
{
    serviceExecutor::post(serv_name_cal,
                            MSG_SELF_CAL_WINDOW, false);
}

int CAppCal::onVisible(int aec)
{
    if(aec != cause_value || m_pCalWnd == NULL )
    {
        return ERR_NONE;
    }

    bool visible = false;
    serviceExecutor::query( serv_name_cal,
                            MSG_SELF_CAL_WINDOW, visible);

    bool mode;
    query(serv_name_cal, MSG_APP_UTILITY_PROJECT, mode);
    m_pCalWnd->onProjectChanged( mode );

    if(visible)
    {
        if(m_pCalWnd->isHidden())
        {
            m_pCalWnd->show();
        }
        else
        {
            m_pCalWnd->setActive();
        }
    }
    else
    {
        if(m_pCalWnd->isVisible())
        {
            m_pCalWnd->hide();
        }
    }
    return ERR_NONE;
}

int CAppCal::onProjectChanged(bool b)
{
   if( m_pCalWnd )
   {
       m_pCalWnd->onProjectChanged(b);
   }
   return ERR_NONE;
}

void CAppCal::onSelfStart()
{
    if( m_pCalWnd )
    {
        m_pCalWnd->show();
    }
}
