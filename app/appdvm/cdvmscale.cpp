#include "cdvmscale.h"
#include "../../menu/menustyle/rcontrolstyle.h"

CDvmScale_style CDvmScale::_style;

CDvmScale::CDvmScale(QWidget *parent) : QWidget(parent)
{
    _style.loadResource("ui/wnd/DVM/");
    setGeometry( _style.mScaleRect);
    m_ScaleMax = 0.5;
    m_ScaleMin = -0.5;
    m_Unit     = "V";
    m_Mode     = DVM_DC;
    m_TagColor = _style.mScaleColor;
    m_StatColor= _style.mScaleStatColor;
}

void CDvmScale::setScaleMin( float min)
{
    m_ScaleMin = min;
}

void CDvmScale::setScaleMax( float max)
{
    m_ScaleMax = max;
}

void CDvmScale::setStatMin( float min)
{
    m_StatMin = min;
}

void CDvmScale::setStatMax( float max)
{
    m_StatMax = max;
}

void CDvmScale::setTagVal( float val)
{
    m_TagVal = val;
    update();
}

void CDvmScale::setUnit( Unit u)
{
    if( u == Unit_W)       m_Unit = "W";
    else if( u == Unit_A)  m_Unit = "A";
    else if( u == Unit_V)  m_Unit = "V";
    else if( u == Unit_U)  m_Unit = "U";
    else                   m_Unit = "V";
}

void CDvmScale::setMode( DvmMode mode)
{
    m_Mode = mode;
    update();
}

void CDvmScale::setTagColor(QColor c)
{
    m_TagColor = c;
    update();
}

void CDvmScale::paintEvent( QPaintEvent */*event*/)
{
//    painter    = new QPainter(this);
    QPainter painter(this);
    QPen pen;

    pen.setWidth( _style.mScalePenW);
    pen.setColor( _style.mScaleColor);
    painter.setPen(pen);
    painter.drawLine( _style.mScaleLeft,_style.mScaleRight);

    //! Statistic Max Min
    drawStat( painter);

    switch (m_Mode)
    {
    case DVM_AC_RMS:
    case DVM_DC_RMS:
        drawRmsScale( painter );
        break;

    case DVM_DC:
        drawDcScale( painter );
        break;
    default:
        break;
    }

    //! Scale Current Value tag
    drawTag( painter );

    //! Scale Min,Max
    QFont font;
    font.setFamily( _style.mScaleFont);
    font.setPointSize( _style.mScaleFontSize);
    painter.setFont( font);
    painter.setPen( _style.mScaleColor);
    painter.drawText( _style.mScaleMin, Qt::AlignVCenter | Qt::AlignLeft,
                      getScaleStr(m_ScaleMin));
    painter.drawText( _style.mScaleMax, Qt::AlignVCenter | Qt::AlignRight,
                      getScaleStr(m_ScaleMax));

}

void CDvmScale::drawRmsScale( QPainter &painter )
{
    int scaleW = _style.mScaleRight.x() - _style.mScaleLeft.x();
    int tickInc = scaleW/_style.mScaleRmsDiv;

    int tickX = _style.mScaleLeft.x();
    int tickY1 = _style.mScaleLeft.y();
    int tickY2 = _style.mScaleLeft.y()-_style.mScaleMinorTick;

    for(int i=0; i<=_style.mScaleRmsDiv; i++)
    {
        painter.drawLine(tickX,tickY1,
                          tickX,tickY2);
        tickX += tickInc;
    }
}

void CDvmScale::drawDcScale( QPainter &painter )
{
    int scaleW = _style.mScaleRight.x() - _style.mScaleLeft.x();
    int tickInc = scaleW/_style.mScaleDcDiv;

    int tickX = _style.mScaleLeft.x();
    int tickY1 = _style.mScaleLeft.y();
    int tickY2 = _style.mScaleLeft.y()-_style.mScaleMinorTick;
    int tickYMid = _style.mScaleLeft.y()-_style.mScaleMajorTick;

    for(int i=0; i<=_style.mScaleDcDiv; i++)
    {
        if(i == _style.mScaleDcDiv/2)   //MajorTick
        {
            painter.drawLine(tickX,tickY1,
                              tickX,tickYMid);
        }
        else                            //MinorTick
        {
            painter.drawLine(tickX,tickY1,
                              tickX,tickY2);
        }
        tickX += tickInc;
    }
}

void CDvmScale::drawTag( QPainter &painter )
{
    QPainterPath path;
    int scaleW = _style.mScaleRight.x() - _style.mScaleLeft.x();
    int tagX = _style.mScaleLeft.x()+(m_TagVal - m_ScaleMin)/(m_ScaleMax - m_ScaleMin)*scaleW;
    int tagY = _style.mScaleLeft.y();

    if( (tagX >= _style.mScaleLeft.x())&&(tagX <= _style.mScaleRight.x()) )
    {
        path.moveTo(tagX,tagY);
        path.lineTo(tagX+_style.mScaleTagW/2,tagY-_style.mScaleTagH);
        path.lineTo(tagX-_style.mScaleTagW/2,tagY-_style.mScaleTagH);
        path.lineTo(tagX,tagY);

        painter.fillPath(path, QBrush(m_TagColor) );
    }
}

void CDvmScale::drawStat( QPainter &painter)
{
    QPainterPath path;
    int scaleW = _style.mScaleRight.x() - _style.mScaleLeft.x();
    int statH = _style.mScaleTagH;
    int statY = _style.mScaleLeft.y();
    int statMinX = _style.mScaleLeft.x()+(m_StatMin-m_ScaleMin)/(m_ScaleMax-m_ScaleMin)*scaleW;
    int statMaxX = _style.mScaleLeft.x()+(m_StatMax-m_ScaleMin)/(m_ScaleMax-m_ScaleMin)*scaleW;

    if( (statMinX >= _style.mScaleLeft.x())&&(statMaxX <= _style.mScaleRight.x())
       &&(statMinX <= statMaxX) )
    {
        path.moveTo( statMinX, statY);
        path.lineTo( statMinX, statY-statH);
        path.lineTo( statMaxX, statY-statH);
        path.lineTo( statMaxX, statY);
        path.lineTo( statMinX, statY);

        painter.fillPath(path, QBrush(m_StatColor) );
    }

//    qDebug()<<"------------CDvmScale::drawStat  m_StatMin"<<m_StatMin;
//    qDebug()<<"------------CDvmScale::drawStat  m_ScaleMin"<<m_ScaleMin;

//    qDebug()<<"------------CDvmScale::drawStat  statMinX"<<statMinX;
//    qDebug()<<"------------CDvmScale::drawStat  _style.mScaleLeft.x()"<<_style.mScaleLeft.x();
//    qDebug()<<"------------CDvmScale::drawStat  statMaxX"<<statMaxX;
//    qDebug()<<"------------CDvmScale::drawStat  _style.mScaleRight.x()"<<_style.mScaleRight.x();
}

QString CDvmScale::getScaleStr(float v)
{
    QString str = "";
    gui_fmt::CGuiFormatter::format(str, v, fmt_def, Unit_none);
    str += m_Unit;
    return str;
}
