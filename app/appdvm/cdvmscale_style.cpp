#include "cdvmscale_style.h"
#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rcontrolstyle.h"

void CDvmScale_style::load(const QString &path,
                           const r_meta::CMeta &meta )
{

    meta.getMetaVal( path + "scaleRect",    mScaleRect );
    meta.getMetaVal( path + "scalePenW",    mScalePenW );
    meta.getMetaVal( path + "scaleLeft",    mScaleLeft );
    meta.getMetaVal( path + "scaleRight",   mScaleRight );

    meta.getMetaVal( path + "scaleMinorTick", mScaleMinorTick );
    meta.getMetaVal( path + "scaleMajorTick", mScaleMajorTick );
    meta.getMetaVal( path + "scaleDcDiv",     mScaleDcDiv );
    meta.getMetaVal( path + "scaleRmsDiv",    mScaleRmsDiv );

    meta.getMetaVal( path + "scaleTagW",    mScaleTagW );
    meta.getMetaVal( path + "scaleTagH",    mScaleTagH );

    meta.getMetaVal( path + "scaleColor",    mScaleColor );
    meta.getMetaVal( path + "scaleStatColor",mScaleStatColor);
    meta.getMetaVal( path + "scaleMin",      mScaleMin );
    meta.getMetaVal( path + "scaleMax",      mScaleMax );
    meta.getMetaVal( path + "scaleFont",     mScaleFont );
    meta.getMetaVal( path + "scaleFontSize", mScaleFontSize );
}

