#-------------------------------------------------
#
# Project created by QtCreator 2017-03-13T21:50:11
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets
QT       += network

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/apps/appdvm

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

INCLUDEPATH += .

# Input
SOURCES += cappdvm.cpp \
    cdvmwnd.cpp \
    cdvmwnd_style.cpp \
    cdvmscale.cpp \
    cdvmscale_style.cpp

HEADERS += cappdvm.h \
    cdvmwnd.h \
    cdvmwnd_style.h \
    cdvmscale.h \
    cdvmscale_style.h
