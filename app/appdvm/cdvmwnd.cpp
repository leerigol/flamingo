#include "cdvmwnd.h"
#include "../../include/dsostd.h"
#include "../../gui/cguiformatter.h"
#include "../../menu/menustyle/rcontrolstyle.h"

CDvmWnd_style CDvmWnd::_style;
menu_res::RModelWnd_Style CDvmWnd::_bgStyle;
CDvmWnd::CDvmWnd(QWidget */*parent*/) : uiWnd( sysGetMainWindow() )
{
    _style.loadResource("ui/wnd/DVM/");
    _bgStyle.loadResource("ui/wnd/model/");
    resize( _style.mFrame.width(),_style.mFrame.height() );

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(_style.mCloseRect);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SIGNAL(closeSig()) );

    m_Src = chan1;
    m_Mode = DVM_DC;

    m_Value = 0.0;
    m_UnitStr  = "V";
    m_UnitPrefix = "";
    m_ValStatus  = DVM_Invalid;

    m_Scale = new CDvmScale(this);
    m_Scale->setMode(m_Mode);
    m_Scale->setScaleMin(-2.0);
    m_Scale->setScaleMax(2.0);
    m_Scale->setTagColor(getSrcColor());
    m_Scale->show();

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void CDvmWnd::paintEvent( QPaintEvent */*event*/)
{
    QPainter painter(this);
    QFont font;
    QImage image;
    QString srcIcon;
    QColor srcColor;

    //! frameBg
    QRect frameRect = rect();
    _bgStyle.paint( painter, frameRect );
//    painter.fillRect(frameRect, _style.mFrameBgColor);

    //! frameRect
//    QPen pen;
//    pen.setWidth(_style.mFrameRectW);
//    pen.setColor(_style.mFrameRectColor);
//    painter.setPen(pen);
//    painter.drawRect(frameRect);

    //! Title
//    painter.fillRect(_style.mTitleBg,_style.mTitleBgColor);
    font.setFamily( _style.mTitleFont );
    font.setPointSize( _style.mTitleFontSize );
    painter.setFont( font );
    painter.setPen( _style.mTitleColor );
    painter.drawText( _style.mTitle, Qt::AlignVCenter | Qt::AlignLeft,
                      "DVM" );

    //! source
    srcColor = getSrcColor();
    srcIcon  = getSrcIcon();
    image.load( srcIcon);
    menu_res::RControlStyle::renderFg( image, srcColor );
    painter.drawImage( _style.mSrcXY, image );

    //! Mode
    font.setFamily(_style.mModeFont);
    font.setPointSize( _style.mModeFontSize);
    painter.setFont( font);
    painter.setPen( _style.mModeColor);
    painter.drawText( _style.mMode, Qt::AlignVCenter | Qt::AlignLeft,
                      getModeStr());

    //! result
    font.setFamily( _style.mResultFont );
    font.setPointSize( _style.mResultFontSize );
    painter.setFont( font );
    painter.setPen( srcColor );
    painter.drawText( _style.mResult, Qt::AlignVCenter | Qt::AlignRight,
                      getResultStr() );
    //! unit
    font.setFamily( _style.mUnitFont );
    font.setPointSize( _style.mUnitFontSize );
    painter.setFont( font );
    painter.setPen( srcColor );
    painter.drawText( _style.mUnit, Qt::AlignVCenter | Qt::AlignLeft,
                      getUnitStr() );

    //! Scale

}

void CDvmWnd::resizeEvent( QResizeEvent * /*event*/)
{
    move( _style.mFrame.left(),
          _style.mFrame.top() );
}

void CDvmWnd::mouseReleaseEvent(QMouseEvent *event)
{
    QRect titleRec = _style.mTitle;
    if(titleRec.contains(event->pos()))
    {
        emit sigTitleClick();
    }
    uiWnd::mouseReleaseEvent(event);
}

void CDvmWnd::setSource( Chan ch)
{
    m_Src = ch;
    m_Scale->setTagColor( getSrcColor());

    update();
}

void CDvmWnd::setMode(DvmMode mode)
{
    m_Mode = mode;
    m_Scale->setMode(m_Mode);

    update();
}

void CDvmWnd::setValue( DvmData *data)
{
    if( NULL != data)
    {
        if( m_Mode == DVM_AC_RMS)
        {
            m_Value = data->m_AcRms;
            m_Scale->setStatMax(data->m_AcRmsMax);
            m_Scale->setStatMin(data->m_AcRmsMin);
            m_Scale->setScaleMin(data->m_AcRms_ScaleMin);
            m_Scale->setScaleMax(data->m_AcRms_ScaleMax);
        }
        else if( m_Mode == DVM_DC)
        {
            m_Value = data->m_Dc;
            m_Scale->setStatMax(data->m_DcMax);
            m_Scale->setStatMin(data->m_DcMin);
            m_Scale->setScaleMin(data->m_Dc_ScaleMin);
            m_Scale->setScaleMax(data->m_Dc_ScaleMax);
        }
        else if( m_Mode == DVM_DC_RMS)
        {
            m_Value = data->m_DcRms;
            m_Scale->setStatMax(data->m_DcRmsMax);
            m_Scale->setStatMin(data->m_DcRmsMin);
            m_Scale->setScaleMin(data->m_DcRms_ScaleMin);
            m_Scale->setScaleMax(data->m_DcRms_ScaleMax);
        }
        else
        {
            m_Value = data->m_AcRms;
            m_Scale->setStatMax(data->m_AcRmsMax);
            m_Scale->setStatMin(data->m_AcRmsMin);
            m_Scale->setScaleMin(data->m_AcRms_ScaleMin);
            m_Scale->setScaleMax(data->m_AcRms_ScaleMax);
        }

        m_Scale->setTagVal(m_Value);
        m_ValStatus = data->m_Status;
        setUnitStr( data->m_Unit);
        m_Scale->setUnit( data->m_Unit);

        update();
    }
}

void CDvmWnd::setUnitStr( Unit u)
{
    if( u == Unit_W)       m_UnitStr = "W";
    else if( u == Unit_A)  m_UnitStr = "A";
    else if( u == Unit_V)  m_UnitStr = "V";
    else if( u == Unit_U)  m_UnitStr = "U";
    else                   m_UnitStr = "V";
}

QString CDvmWnd::getSrcIcon()
{
    if( m_Src==chan1 ) return _style.mCH1Icon;
    else if ( m_Src==chan2 ) return _style.mCH2Icon;
    else if ( m_Src==chan3 ) return _style.mCH3Icon;
    else if ( m_Src==chan4 ) return _style.mCH4Icon;

    else return _style.mCH1Icon;
}

QColor  CDvmWnd::getSrcColor()
{
    if ( m_Src == chan1 )
    {
        return _style.mColorCH1;
    }
    else if ( m_Src == chan2 )
    {
        return _style.mColorCH2;
    }
    else if ( m_Src == chan3 )
    {
        return _style.mColorCH3;
    }
    else if ( m_Src == chan4 )
    {
        return _style.mColorCH4;
    }
    else
    {
        return Qt::white;
    }
}

QString CDvmWnd::getModeStr()
{
    switch (m_Mode)
    {
    case DVM_AC_RMS:
        return "AC RMS";

    case DVM_DC:
        return "DC";

    case DVM_DC_RMS:
        return "AC+DC RMS";

    default:
        return "DC";
    }
}

QString CDvmWnd::getResultStr()
{
    QString str = "";

    if ( m_ValStatus == DVM_Valid )
    {
        gui_fmt::CGuiFormatter::format(str, m_Value, dso_fmt_trim(fmt_float, fmt_width_3, fmt_no_trim_0), Unit_none);
        if(str.contains(QRegExp("[TGMKkmunpf]")))
        {
            m_UnitPrefix = str.right(1);
        }
        else
        {
            m_UnitPrefix = "";
        }
//        qDebug()<<"---------------CDvmWnd::getResultStr str: "<<str;
        str.remove(QRegExp("[TGMKkmunpf]")); //Remove unit prefixs
    }
    else
    {
        str = _style.mInvalidString;
        m_UnitPrefix = "";
    }

    return str;
}

QString CDvmWnd::getUnitStr()
{
    QString unit;
    unit = m_UnitPrefix + m_UnitStr;
    return unit;
}
