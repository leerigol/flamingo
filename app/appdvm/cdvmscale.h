#ifndef CDVMSCALE_H
#define CDVMSCALE_H

#include <QWidget>
#include <QPainter>
#include "cdvmscale_style.h"
#include "../../service/servdvm/servdvm.h"


class CDvmScale : public QWidget
{
    Q_OBJECT

    static CDvmScale_style _style;

public:
    explicit CDvmScale(QWidget *parent = 0);

    void setScaleMin( float min);
    void setScaleMax( float max);
    void setStatMin( float min);
    void setStatMax( float max);
    void setTagVal( float val);
    void setUnit( Unit u);

    void setMode( DvmMode mode);
    void setTagColor(QColor c);

protected:
    virtual void paintEvent( QPaintEvent *event);


private:
    void drawRmsScale( QPainter &painter );
    void drawDcScale( QPainter &painter );
    void drawTag( QPainter &painter );
    void drawStat( QPainter &painter);

    QString getScaleStr(float v);

private:
    float   m_ScaleMin;
    float   m_ScaleMax;
    float   m_StatMin;
    float   m_StatMax;
    float   m_TagVal;

    QString m_Unit;
    DvmMode m_Mode;

    QColor  m_TagColor;
    QColor  m_StatColor;
//    QPainter *painter;
signals:

public slots:
};

#endif // CDVMSCALE_H
