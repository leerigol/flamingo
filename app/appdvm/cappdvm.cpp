#include "cappdvm.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../service/servch/servch.h"
#include "../../service/servvertmgr/servvertmgr.h"

#include "../appmain/cappmain.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppDvm )
start_of_entry()
on_set_void_void(  MSG_DVM_ENABLE,               &CAppDvm::onShowDvm ),
on_set_void_void (  MSG_DVM_SRC,                 &CAppDvm::setSrc ),
on_set_void_void (  MSG_DVM_MODE,                &CAppDvm::setMode ),

on_set_void_void ( CMD_SERVICE_RST,              &CAppDvm::onRst),
end_of_entry()

CAppDvm::CAppDvm()
{
    mservName = serv_name_dvm;
    addQuick( MSG_DVM,
              QString("ui/desktop/dvm/icon/") );

    m_DvmWnd = new CDvmWnd();
    Q_ASSERT(m_DvmWnd != NULL);
    m_DvmWnd->setVisible(false);

    m_Src = chan1;
    m_Timer = new QTimer(this);
    connect( m_Timer, SIGNAL(timeout()), this, SLOT(updateDvmValue()) );
    m_TimerCnt = 0;
}

void CAppDvm::setupUi( CAppMain *pMain )
{
    m_DvmWnd->setParent(pMain->getCentBar());
}

void CAppDvm::retranslateUi()
{

}

void CAppDvm::buildConnection()
{
    connect(m_DvmWnd, SIGNAL(closeSig()),this,SLOT(postCloseSig()));
    connect(m_DvmWnd, SIGNAL(sigTitleClick()),this,SLOT(postTitleClick()));
}
void CAppDvm::resize()
{

}

void CAppDvm::registerSpy()
{
}

void CAppDvm::onRst()
{
    if( m_DvmWnd->isVisible())
    {
        m_DvmWnd->hide();
    }
    on_rst();
}

void CAppDvm::onShowDvm()
{
    bool dvmStatus = false;

    serviceExecutor::query(serv_name_dvm, MSG_DVM_ENABLE, dvmStatus);

    setSrc();
    setMode();

    //! add meas items
    if(dvmStatus == true)
    {
//        serviceExecutor::post(serv_name_measure, MSG_APP_MEAS_SRCA, (int)m_Src);
//        serviceExecutor::post(serv_name_measure, servMeasure::MSG_ADD_STAT_ITEM, (int)Meas_Vrms);

        m_Timer->setInterval(500); //! update rate: 500ms
        m_Timer->start();
        m_DvmWnd->show();
    }
    else
    {
        m_Timer->stop();
        m_DvmWnd->hide();
    }
}

void CAppDvm::setSrc()
{
    int src = 1;
    serviceExecutor::query(serv_name_dvm, MSG_DVM_SRC, src);
    m_Src = (Chan)src;
    m_DvmWnd->setSource( m_Src);
}

void CAppDvm::setMode()
{
    int mode = 0;
    serviceExecutor::query(serv_name_dvm, MSG_DVM_MODE, mode);
    m_DvmWnd->setMode( (DvmMode)mode);
}

void CAppDvm::updateDvmValue()
{
    bool dvmEn = false;
    query( serv_name_dvm, MSG_DVM_ENABLE, dvmEn);

    if( dvmEn)
    {
//        if( checkResetTime())
//        {
//            serviceExecutor::send( serv_name_dvm, servDvm::cmd_Set_Reset, 1);
//        }

        void *ptr = NULL;
        //! Get DVM Data
        serviceExecutor::query( serv_name_dvm,
                     servDvm::cmd_Get_DvmVal,
                     ptr );

        //! m_DvmWnd->setValue
        if( NULL != ptr)
        {
            m_DvmWnd->setValue( (DvmData *)ptr);
        }
    }
    else
    {
        return;
    }
}

void CAppDvm::postTitleClick()
{
    serviceExecutor::post(serv_name_dvm, CMD_SERVICE_ACTIVE, (int)1 );
}

bool CAppDvm::checkResetTime()  //! Check dvm statistic val reset time
{
    if( m_TimerCnt >= (DVM_STAT_RESET_TIME/DVM_UPDATE_TIME))
    {
        m_TimerCnt = 0;
        return true;
    }
    else
    {
        m_TimerCnt += 1;
        return false;
    }
}

void CAppDvm::postCloseSig()
{
    serviceExecutor::post(serv_name_dvm, MSG_DVM_ENABLE, false);
}
