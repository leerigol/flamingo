#include "cdvmwnd_style.h"
#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rcontrolstyle.h"

void CDvmWnd_style::load(const QString &path,
                         const r_meta::CMeta &meta )
{

    meta.getMetaVal( path + "frame",          mFrame );
    meta.getMetaVal( path + "frameBgColor",   mFrameBgColor );
    meta.getMetaVal( path + "frameRectColor", mFrameRectColor );
    meta.getMetaVal( path + "frameRectW",     mFrameRectW );

    meta.getMetaVal( path + "title",         mTitle );
    meta.getMetaVal( path + "titleFont",     mTitleFont );
    meta.getMetaVal( path + "titleFontSize", mTitleFontSize );
    meta.getMetaVal( path + "titleColor",    mTitleColor );
    meta.getMetaVal( path + "titleBg",       mTitleBg );
    meta.getMetaVal( path + "titleBgColor",  mTitleBgColor );

    meta.getMetaVal( path + "mode",         mMode );
    meta.getMetaVal( path + "modeFont",     mModeFont );
    meta.getMetaVal( path + "modeFontSize", mModeFontSize );
    meta.getMetaVal( path + "modeColor",    mModeColor );

    meta.getMetaVal( path + "closerect",         mCloseRect );

    meta.getMetaVal( path + "result",         mResult );
    meta.getMetaVal( path + "resultFont",     mResultFont );
    meta.getMetaVal( path + "resultFontSize", mResultFontSize );

    meta.getMetaVal( path + "unit",         mUnit );
    meta.getMetaVal( path + "unitFont",     mUnitFont );
    meta.getMetaVal( path + "unitFontSize", mUnitFontSize );

    meta.getMetaVal( path + "srcXY",       mSrcXY );
    meta.getMetaVal( path + "chcolor/ch1", mColorCH1 );
    meta.getMetaVal( path + "chcolor/ch2", mColorCH2 );
    meta.getMetaVal( path + "chcolor/ch3", mColorCH3 );
    meta.getMetaVal( path + "chcolor/ch4", mColorCH4 );
    meta.getMetaVal( path + "chcolor/dx",  mColorDX );
    meta.getMetaVal( path + "chcolor/ext", mColorExt );
    meta.getMetaVal( path + "chcolor/ac",  mColorLine );

    //! invalid string
    meta.getMetaVal( path + "invalid_string", mInvalidString );

    QString ChIconPath = "ui/trig/trig_info/ch_icon/";
    meta.getMetaVal( ChIconPath  + "ch1", mCH1Icon );
    meta.getMetaVal( ChIconPath  + "ch2", mCH2Icon );
    meta.getMetaVal( ChIconPath  + "ch3", mCH3Icon );
    meta.getMetaVal( ChIconPath  + "ch4", mCH4Icon );

    meta.getMetaVal( ChIconPath  + "ac",   mAcIcon );
    meta.getMetaVal( ChIconPath  + "ext",  mExtIcon );
    meta.getMetaVal( ChIconPath  + "ext5", mExt5Icon );

    meta.getMetaVal( ChIconPath + "d0", mD0Icon );
    meta.getMetaVal( ChIconPath + "d1", mD1Icon );
    meta.getMetaVal( ChIconPath + "d2", mD2Icon );
    meta.getMetaVal( ChIconPath + "d3", mD3Icon );

    meta.getMetaVal( ChIconPath + "d4", mD4Icon );
    meta.getMetaVal( ChIconPath + "d5", mD5Icon );
    meta.getMetaVal( ChIconPath + "d6", mD6Icon );
    meta.getMetaVal( ChIconPath + "d7", mD7Icon );

    meta.getMetaVal( ChIconPath + "d8",  mD8Icon );
    meta.getMetaVal( ChIconPath + "d9",  mD9Icon );
    meta.getMetaVal( ChIconPath + "d10", mD10Icon );
    meta.getMetaVal( ChIconPath + "d11", mD11Icon );

    meta.getMetaVal( ChIconPath + "d12", mD12Icon );
    meta.getMetaVal( ChIconPath + "d13", mD13Icon );
    meta.getMetaVal( ChIconPath + "d14", mD14Icon );
    meta.getMetaVal( ChIconPath + "d15", mD15Icon );
}

