#ifndef CDVMSCALE_STYLE_H
#define CDVMSCALE_STYLE_H

#include "../../menu/menustyle/rui_style.h"

class CDvmScale_style : public menu_res::RUI_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta);

public:
    QRect  mScaleRect;
    int    mScalePenW;
    QPoint mScaleLeft;
    QPoint mScaleRight;

    int  mScaleMinorTick;
    int  mScaleMajorTick;
    int  mScaleDcDiv;
    int  mScaleRmsDiv;

    int  mScaleTagW;
    int  mScaleTagH;

    int     mScaleColor;
    int     mScaleStatColor;
    QRect   mScaleMin;
    QRect   mScaleMax;
    QString mScaleFont;
    int     mScaleFontSize;


};

#endif // CDVMSCALE_STYLE_H
