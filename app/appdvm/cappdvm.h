#ifndef CAPPDVM_H
#define CAPPDVM_H

#include <QtCore>
#include <QtWidgets>

#include "../capp.h"

#include "../../menu/rmenus.h"
#include "cdvmwnd.h"

#define DVM_UPDATE_TIME 500 //! ms
#define DVM_STAT_RESET_TIME 3000  //! dvm Statistic val reset time: ms

class CAppDvm : public CApp
{
    Q_OBJECT
    DECLARE_CMD()
public:
    CAppDvm();

    enum appMsg
    {
        cmd_base = CMD_APP_BASE,
    };

public:
     void setupUi( CAppMain *pMain );
     void retranslateUi();

     void buildConnection();
     void resize();
     void registerSpy();

     void onShowDvm();
     void setSrc();
     void setMode();

     bool checkResetTime();

     void onRst();
public slots:
     void postCloseSig();
     void updateDvmValue();
     void postTitleClick();
private:
     CDvmWnd *m_DvmWnd;
     Chan     m_Src;
     QTimer  *m_Timer;
     int      m_TimerCnt;  //! For Timer timeout count
};

#endif // CAPPDVM_H
