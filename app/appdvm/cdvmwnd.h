#ifndef CDVMWND_H
#define CDVMWND_H

#include <QWidget>
#include "cdvmwnd_style.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "cdvmscale.h"
#include "../../include/dsotype.h"
#include "../../service/servdvm/servdvm.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;
using namespace DsoType;
class CDvmWnd : public uiWnd
{
    Q_OBJECT
    static CDvmWnd_style _style;
    static menu_res::RModelWnd_Style _bgStyle;

public:
    CDvmWnd(QWidget *parent = 0);

protected:
    virtual void paintEvent( QPaintEvent *event);
    virtual void resizeEvent( QResizeEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
public:
    void setSource( Chan ch);
    void setMode( DvmMode mode);
    void setValue(DvmData *data);
    void setUnitStr( Unit u);
signals:
    void closeSig();
    void sigTitleClick();

protected:
    QString getSrcIcon();
    QColor  getSrcColor();
    QString getModeStr();
    QString getResultStr();
    QString getUnitStr();

private:
    RImageButton   *m_pCloseButton;

    Chan    m_Src;
    DvmMode m_Mode;

    float   m_Value;
    QString m_UnitStr;
    QString m_UnitPrefix;
    DvmStatus   m_ValStatus;

    CDvmScale *m_Scale;

};

#endif // CDVMWND_H
