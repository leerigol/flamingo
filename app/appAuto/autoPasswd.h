#ifndef CPasswdDlgWND_H
#define CPasswdDlgWND_H

#include <QWidget>
#include "../../menu/wnd/rmodalwnd.h"

#include "../../menu/menustyle/rui_style.h"

class CPasswdStyle : public menu_res::RUI_Style
{
public:
    CPasswdStyle();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QRect mRectGeo;

    QRect geo, yesGeo, noGeo;
    QRect labelOld,labelNew, labelPwd;
    QRect editOld, editNew, editPwd;

    QString qss;
};

class AutoLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit AutoLineEdit(QWidget *parent = 0);
protected:
    //重写mousePressEvent事件
    virtual void mousePressEvent(QMouseEvent *event);

signals:
    //自定义clicked()信号,在mousePressEvent事件发生时触发
    void clicked();

public slots:

};

class CPasswdDlg : public menu_res::RModalWnd
{
    Q_OBJECT

public:
    CPasswdDlg( QWidget *parent = 0);
    ~CPasswdDlg();

    void setup();

public:
    QLabel *m_pLabelOld;
    QLabel *m_pLabelNew;
    QLabel *m_pLabelPwd;


    AutoLineEdit *m_pOld;
    AutoLineEdit *m_pNew;
    AutoLineEdit *m_pPass;

Q_SIGNALS:
    void OnOK();
    void OnCancel();

    void sigOnOldClick();
    void sigOnNewClick();
    void sigOnPassClick();

protected slots:
    void OnOKClick();
    void OnCancelClick();

    void OnOldClick();
    void OnNewClick();
    void OnPassClick();

protected:
    void keyPressEvent( QKeyEvent *event );
    void mousePressEvent(QMouseEvent *event);
private:
    static CPasswdStyle _style;
    QPushButton *m_pOK;
    QPushButton *m_pCancel;

};

#endif // CPasswdDlgWND_H

