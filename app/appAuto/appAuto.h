#ifndef APPAUTO_H
#define APPAUTO_H

#include "../capp.h"
#include "autoPasswd.h"

class CAppAuto : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
        CAppAuto();

    enum
    {
        INPUT_OLD,
        INPUT_NEW,
        INPUT_RENEW,
        INPUT_PASS
    };

    int showDialog(int);

    int onReturn(int);


    int inputPwd(int, int);

    int startInputPass(int);

public slots:
    void  OnOK();
    void  OnCancel();
    void  OnClose();

    void  OnInputOld(QString);
    void  OnInputNew1(QString);
    void  OnInputNew2(QString);
    void  OnInputPass(QString);

    void  startInputOld(void);
    void  startInputNew1(void);
    void  startInputNew2(void);

private:
    CPasswdDlg *m_pPassword;

    QString     mPassword;
};


#endif // APPAUTO_H
