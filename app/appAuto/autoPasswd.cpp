#include "../../service/servauto/servauto.h"
#include "autoPasswd.h"


CPasswdStyle::CPasswdStyle()
{

}

void CPasswdStyle::load( const QString &path,
                        const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "geo", geo );
    meta.getMetaVal( path + "yesGeo", yesGeo );
    meta.getMetaVal( path + "noGeo", noGeo );

    meta.getMetaVal( path + "labelOld", labelOld );
    meta.getMetaVal( path + "labelNew", labelNew );
    meta.getMetaVal( path + "labelPwd", labelPwd );

    meta.getMetaVal( path + "editOld", editOld );
    meta.getMetaVal( path + "editNew", editNew );
    meta.getMetaVal( path + "editPwd", editPwd );

    meta.getMetaVal( path + "qss", qss );
}

AutoLineEdit::AutoLineEdit(QWidget *parent) : QLineEdit(parent)
{
}

//重写mousePressEvent事件,检测事件类型是不是点击了鼠标左键
void AutoLineEdit::mousePressEvent(QMouseEvent *event)
{
    //如果单击了就触发clicked信号
    if (event->button() == Qt::LeftButton)
    {
        //触发clicked信号
        emit clicked();
    }
    //将该事件传给父类处理
    QLineEdit::mousePressEvent(event);
}

CPasswdStyle CPasswdDlg::_style;
CPasswdDlg::CPasswdDlg( QWidget */*parent*/)
{

    m_pLabelOld = NULL;
    m_pLabelNew = NULL;
    m_pLabelPwd  = NULL;

    m_pOld = NULL;
    m_pNew = NULL;
    m_pPass  = NULL;

    m_pOK   = NULL;
    m_pCancel= NULL;

    _style.loadResource("ui/auto/");
    setAttr(wnd_moveable);
}

CPasswdDlg::~CPasswdDlg()
{
    delete m_pLabelOld;
    m_pLabelOld = NULL;

    delete m_pLabelNew;
    m_pLabelNew = NULL;

    delete m_pLabelPwd;
    m_pLabelPwd = NULL;



    delete m_pOld;
    m_pOld = NULL;

    delete m_pNew;
    m_pNew = NULL;

    delete m_pPass;
    m_pPass = NULL;

    delete m_pOK;
    m_pOK = NULL;

    delete m_pCancel;
    m_pCancel = NULL;
}

void CPasswdDlg::setup()
{
    if(m_pLabelOld == NULL)
    {
        m_pLabelOld = new QLabel(sysGetString(INF_OLD_PASSWORD,"Old:"),this);
        m_pLabelNew = new QLabel(sysGetString(INF_NEW_PASSWORD,"New:"),this);
        m_pLabelPwd = new QLabel(sysGetString(INF_RENEW_PASSWORD,"Renew:"),this);

        m_pOld = new AutoLineEdit(this);
        m_pNew = new AutoLineEdit(this);
        m_pPass = new AutoLineEdit(this);

        //m_pPass->setInputMask("********");
        m_pOld->setEchoMode(QLineEdit::Password);
        m_pNew->setEchoMode(QLineEdit::Password);
        m_pPass->setEchoMode(QLineEdit::Password);

        m_pOK = new QPushButton(tr("OK"),this);
        m_pCancel = new QPushButton(tr("Cancel"),this);

        setTitle( MSG_AUTO_PASSWORD );
    }

    //! placement
    setGeometry( _style.geo );

    m_pLabelOld->setGeometry(_style.labelOld);
    m_pLabelNew->setGeometry(_style.labelNew);
    m_pLabelPwd->setGeometry(_style.labelPwd);

    m_pOld->setGeometry(_style.editOld);
    m_pNew->setGeometry(_style.editNew);
    m_pPass->setGeometry(_style.editPwd);

    this->setStyleSheet(_style.qss);

    m_pOK->setGeometry(_style.yesGeo);
    m_pCancel->setGeometry(_style.noGeo);

    QObject::connect(m_pOK,SIGNAL(clicked()),this,SLOT(OnOKClick()));
    QObject::connect(m_pCancel,SIGNAL(clicked()),this,SLOT(OnCancelClick()));

    connect(m_pOld, SIGNAL(clicked()), this, SLOT(OnOldClick()) );
    connect(m_pNew, SIGNAL(clicked()), this, SLOT(OnNewClick()) );
    connect(m_pPass, SIGNAL(clicked()), this, SLOT(OnPassClick()) );
}


void CPasswdDlg::OnOKClick()
{
    emit OnOK();
}


void CPasswdDlg::OnCancelClick()
{
    emit OnCancel();
}


void CPasswdDlg::OnOldClick()
{
    //serviceExecutor::post(serv_name_autoset, servAuto::cmd_password_old,0);
    emit sigOnOldClick();
}

void CPasswdDlg::OnNewClick()
{
    //serviceExecutor::post(serv_name_autoset, servAuto::cmd_password_new,0);
    emit sigOnNewClick();
}

void CPasswdDlg::OnPassClick()
{
    //serviceExecutor::post(serv_name_autoset, servAuto::cmd_password_renew,0);
    emit sigOnPassClick();
}


void CPasswdDlg::keyPressEvent(QKeyEvent *event)
{
    Q_ASSERT( NULL != event);
    if(event->key() == R_Pkey_PageReturn)
    {
        emit sigClose();
        hide();
    }
}

void CPasswdDlg::mousePressEvent(QMouseEvent */*event*/)
{
    //qDebug() << event;
}
