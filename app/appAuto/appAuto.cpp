#include "../../com/virtualkb/virtualkb.h"
#include "../../service/servauto/servauto.h"
#include "../../service/servgui/servgui.h"
#include "appAuto.h"

IMPLEMENT_CMD( CApp, CAppAuto )
start_of_entry()
on_set_int_int (  MSG_AUTO_PASSWORD,            &CAppAuto::showDialog),

on_set_int_int (  servAuto::cmd_auto_unlock,    &CAppAuto::startInputPass),
on_set_int_int (  CMD_SERVICE_SUB_RETURN,       &CAppAuto::onReturn),
on_set_int_int (  CMD_SERVICE_EXIT_ACTIVE,      &CAppAuto::onReturn),
on_set_int_int (  CMD_SERVICE_DEACTIVE,         &CAppAuto::onReturn),
end_of_entry()


CAppAuto::CAppAuto()
{
    mservName   =  serv_name_autoset;
    m_pPassword = NULL;
}


int CAppAuto::showDialog(int /*a*/)
{
    if( m_pPassword == NULL )
    {
        m_pPassword = new CPasswdDlg();
        Q_ASSERT( m_pPassword != NULL );
        connect(m_pPassword, SIGNAL(OnOK()), this, SLOT(OnOK()) );
        connect(m_pPassword, SIGNAL(OnCancel()), this, SLOT(OnCancel()) );
        connect(m_pPassword, SIGNAL(sigClose()), this, SLOT(OnClose()) );

        connect(m_pPassword,
                SIGNAL(sigOnOldClick()),
                this,
                SLOT(startInputOld()));

        connect(m_pPassword,
                SIGNAL(sigOnNewClick()),
                this,
                SLOT(startInputNew1()));

        connect(m_pPassword,
                SIGNAL(sigOnPassClick()),
                this,
                SLOT(startInputNew2()));

        m_pPassword->setup();
    }
    else
    {
        OnClose();
        return ERR_NONE;
    }

    query(mservName, servAuto::cmd_auto_password, mPassword);
    m_pPassword->m_pOld->setText("");


    m_pPassword->m_pNew->setText("");


    m_pPassword->m_pPass->setText("");

    if(work_help != sysGetWorkMode())
    {
        m_pPassword->show();
    }

    return ERR_NONE;
}

int CAppAuto::startInputPass(int)
{
    bool locker = false;
    query(mservName, MSG_AUTO_S32AUTOLOCK, locker);

    if( locker )
    {
        query(mservName, servAuto::cmd_auto_password, mPassword);
        inputPwd(INPUT_PASS, -1);
    }
    return ERR_NONE;
}

int CAppAuto::inputPwd(int type, int pwd)
{
    DsoRealGp vg;

    vg.setValue(pwd, 999999, 0, 666666, E_0);
    InputKeypad* inputMethod = InputKeypad::getInstance(0);
    InputKeypad::rst();

    if(type == INPUT_OLD)
    {
        connect(inputMethod,
                SIGNAL(OnOK(QString)),
                this,
                SLOT(OnInputOld(QString)));
    }
    else if(type == INPUT_NEW)
    {
        connect(inputMethod,
                SIGNAL(OnOK(QString)),
                this,
                SLOT(OnInputNew1(QString)));
    }
    else if(type == INPUT_RENEW)
    {
        connect(inputMethod,
                SIGNAL(OnOK(QString)),
                this,
                SLOT(OnInputNew2(QString)));
    }
    else if( type == INPUT_PASS )
    {
        connect(inputMethod,
                SIGNAL(OnOK(QString)),
                this,
                SLOT(OnInputPass(QString)));
    }

    inputMethod->input(InputKeypad::Password, vg);

    return ERR_NONE;
}

void CAppAuto::startInputOld()
{
    if( m_pPassword )
    {
        if( m_pPassword->isVisible())
        {
            int pwd = -1;
            if(m_pPassword->m_pOld->text().size() > 0 )
            {
                 pwd = m_pPassword->m_pOld->text().toInt();
            }
            inputPwd(INPUT_OLD, pwd);
        }
        else
        {
            showDialog(0);
        }
    }
}


void CAppAuto::startInputNew1()
{
    if( m_pPassword )
    {
        if( m_pPassword->isVisible())
        {
            int pwd = -1;
            if(m_pPassword->m_pNew->text().size() > 0 )
            {
                 pwd = m_pPassword->m_pNew->text().toInt();
            }
            inputPwd(INPUT_NEW, pwd);
        }
        else
        {
            showDialog(0);
        }
    }
}


void CAppAuto::startInputNew2( )
{
    if( m_pPassword )
    {
        if( m_pPassword->isVisible())
        {
            int pwd = -1;
            if(m_pPassword->m_pPass->text().size() > 0 )
            {
                 pwd = m_pPassword->m_pPass->text().toInt();
            }
            inputPwd(INPUT_RENEW, pwd);
        }
        else
        {
            showDialog(0);
        }
    }
}

int CAppAuto::onReturn(int)
{
    OnClose();
    return ERR_NONE;
}

void CAppAuto::OnCancel()
{
    OnClose();
}

///
/// \brief save the config to file
///
void CAppAuto::OnOK()
{
    //different
    if( m_pPassword->m_pOld->text().compare(mPassword) != 0 )
    {
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_INVALID_OLD);
        return;
    }

    if( m_pPassword->m_pNew->text().compare(m_pPassword->m_pPass->text()) != 0 )
    {
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_TWO_DIFFERENT);
        return;
    }

    mPassword = m_pPassword->m_pPass->text();

    QString tmp = mPassword;
    if( tmp.size() == 0 )
    {
        tmp = AUTO_DEFAULT_PASSWORD;
    }
    serviceExecutor::post( serv_name_autoset,
                           servAuto::cmd_auto_password,
                           tmp);

    OnCancel();
}

void CAppAuto::OnInputOld(QString p)
{
    if( p.size() > 6 )
    {
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_PWD_LIMIT_6);
    }
    else
    {
        m_pPassword->m_pOld->setText( p );
    }
    //qDebug() << "old = " << p;
}

void CAppAuto::OnInputNew1(QString p)
{
    if( p.size() > 6 )
    {
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_PWD_LIMIT_6);
    }
    else
    {
        m_pPassword->m_pNew->setText( p );
    }
    //qDebug() << "new1 = " << p;
}

void CAppAuto::OnInputNew2(QString p)
{
    if( p.size() > 6 )
    {
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_PWD_LIMIT_6);
    }
    else
    {
        m_pPassword->m_pPass->setText( p );
    }
    //qDebug() << "new2 = " << p;
}

void CAppAuto::OnInputPass(QString p)
{
    if( p.compare( mPassword ) == 0 )
    {
        serviceExecutor::post( E_SERVICE_ID_AUTO_SET,
                               servAuto::cmd_lock_auto,
                               false);
    }
    else
    {
        serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                               CMD_SERVICE_DO_ERR,
                               (int)ERR_INVALID_PASS);
    }
}

void CAppAuto::OnClose()
{
    if( m_pPassword != NULL )
    {
        m_pPassword->hide();
        delete m_pPassword;
        m_pPassword = NULL;
    }
}
