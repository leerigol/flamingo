#include "./sysinfo.h"
#include <QHBoxLayout>
#include <QVBoxLayout>

menu_res::RModelWnd_Style CSysInfoDlg::_bgStyle;
CSysInfoDlg::CSysInfoDlg( QWidget */*parent*/)
{
    addAllKeys();

    set_attr( wndAttr, mWndAttr, wnd_moveable );
    setupUI();
}

CSysInfoDlg::~CSysInfoDlg()
{
}

void CSysInfoDlg::loadResource(QString path)
{
    r_meta::CRMeta rMeta;
    rMeta.getMetaVal(path + "closerect",    closeRect);
    rMeta.getMetaVal(path + "titlerect",    titleRect);
    rMeta.getMetaVal(path + "ultrarect",    ultraRect);
    rMeta.getMetaVal(path + "qrencoderect", qrencodeRect);
    rMeta.getMetaVal(path + "ultrapic",     ultraPic);
    rMeta.getMetaVal(path + "qrencodepic",  qrencodePic);
    QPoint geo;
    rMeta.getMetaVal(path + "geo",  geo);
    move(geo);
}
void CSysInfoDlg::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QRect frameRect = rect();

    _bgStyle.paint( painter, frameRect );

}
void CSysInfoDlg::setMode(QString model)
{
    m_labelModel.setText(model);
}

void CSysInfoDlg::setSerialSn(QString sn)
{
    m_labelSN.setText(sn);
}

void CSysInfoDlg::setSoftwarVer(QString softwarver)
{
    m_labelSoftVer.setText(softwarver);
}

void CSysInfoDlg::setHardwarVer(QString hardwarver)
{
    bool b = true;
    int hwver = hardwarver.toInt(&b, 16);
    QString hwstr = QString("%1.%2.%3").arg( 0x3 & (hwver >> 3),2 ,10,QLatin1Char('0') )
                                       .arg( hwver & 0x7, 2,10,QLatin1Char('0'))
                                       .arg( 0x7 & (hwver >> 7),3,10,QLatin1Char('0'));
    m_labelHardVer.setText(hwstr);
}

void CSysInfoDlg::setBootVer(QString bootver)
{
    m_labelBoot.setText(bootver);
}

void CSysInfoDlg::setDatetime(QString dt)
{
    m_labelDatetime.setText(dt);
}

void CSysInfoDlg::setSPUVer(QString spuver)
{
    m_labelSPU.setText(spuver);
}

void CSysInfoDlg::setWPUVer(QString wpuver)
{
    m_labelWPU.setText(wpuver);
}

void CSysInfoDlg::setCCUVer(QString ccuver)
{
    m_labelCCU.setText(ccuver);
}

void CSysInfoDlg::setMCUVer(QString mcuver)
{
    bool b = true;
    int ver = mcuver.toInt(&b, 16);
    QString verstr = QString("%1-%2-%3 [%4]").arg(2000 + (ver >> 9) )
                                             .arg( 0xf & (ver >> 5) )
                                             .arg( 0x1f & ver )
                                             .arg(mcuver);
    m_labelMCU.setText(verstr);
}

void CSysInfoDlg::setADCChipId0(int id)
{
    m_labelAdcId0.setText(QString::number(id,16));
}

void CSysInfoDlg::setADCChipId1(int id)
{
    m_labelAdcId1.setText(QString::number(id,16));
    m_labelAdcId1.show();
    m_labelAdcIdC1.show();
}

void CSysInfoDlg::setAFEChipVer0(int ver)
{
    m_labelAfeVer0.setText(QString::number(ver,16));
}

void CSysInfoDlg::setAFEChipVer1(int ver)
{
    m_labelAfeVer1.setText(QString::number(ver,16));
}

void CSysInfoDlg::setAFEChipVer2(int ver)
{
    m_labelAfeVer2.setText(QString::number(ver,16));
}

void CSysInfoDlg::setAFEChipVer3(int ver)
{
    m_labelAfeVer3.setText(QString::number(ver,16));
}

void CSysInfoDlg::setStartTimes(int times)
{
    m_labelStartTimes.setText(QString::number(times,10));
}

void CSysInfoDlg::setupUI()
{
    loadResource("ui/sysinfo/");
    _bgStyle.loadResource("ui/wnd/model/");

    m_labelTitleCaption.setParent(this);
    m_labelVendorCaption.setParent(this);
    m_labelModelCaption.setParent(this);

    m_labelSPUCaption.setParent(this);
    m_labelSNCaption.setParent(this);

    m_labelSoftVerCaption.setParent(this);

    m_labelHardVerCaption.setParent(this);
    m_labelBootCaption.setParent(this);
    m_labelDatetimeCaption.setParent(this);
    m_labelWPUCaption.setParent(this);

    m_labelCCUCaption.setParent(this);
    m_labelMCUCaption.setParent(this);

    m_labelAdcIdC0.setParent(this);
    m_labelAdcIdC1.setParent(this);
    m_labelAfeVerC0.setParent(this);
    m_labelAfeVerC1.setParent(this);
    m_labelAfeVerC2.setParent(this);
    m_labelAfeVerC3.setParent(this);

    m_labelAdcId0.setParent(this);
    m_labelAdcId1.setParent(this);
    m_labelAfeVer0.setParent(this);
    m_labelAfeVer1.setParent(this);
    m_labelAfeVer2.setParent(this);
    m_labelAfeVer3.setParent(this);

    m_labelStartTimesC.setParent(this);
    m_labelCycleCntC.setParent(this);

    m_labelVendor.setParent(this);
    m_labelVendor.setParent(this);
    m_labelModel.setParent(this);
    m_labelSN.setParent(this);
    m_labelSoftVer.setParent(this);
    m_labelHardVer.setParent(this);
    m_labelBoot.setParent(this);
    m_labelDatetime.setParent(this);
    m_labelFPGAVersion.setParent(this);
    m_labelSPU.setParent(this);
    m_labelWPU.setParent(this);
    m_labelCCU.setParent(this);
    m_labelMCU.setParent(this);
    m_labelStartTimes.setParent(this);
    m_labelCycleCnt.setParent(this);

    m_picture.setParent(this);
    m_qrencode.setParent(this);

    //! placement
    resize(460,305);
    m_labelTitleCaption.setGeometry(titleRect);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(50,40,190,11);

    QVBoxLayout *vlayout1 = new QVBoxLayout;
    vlayout1->addWidget(&m_labelVendorCaption);
    vlayout1->addWidget(&m_labelModelCaption);
    vlayout1->addWidget(&m_labelSNCaption);
    vlayout1->addWidget(&m_labelSoftVerCaption);
    vlayout1->addWidget(&m_labelHardVerCaption);
    vlayout1->addWidget(&m_labelBootCaption);
    vlayout1->addWidget(&m_labelDatetimeCaption);
    vlayout1->addWidget(&m_labelSPUCaption);
    vlayout1->addWidget(&m_labelWPUCaption);
    vlayout1->addWidget(&m_labelCCUCaption);
    vlayout1->addWidget(&m_labelMCUCaption);
    vlayout1->addWidget(&m_labelAdcIdC0);
    vlayout1->addWidget(&m_labelAdcIdC1);
    vlayout1->addWidget(&m_labelAfeVerC0);
    vlayout1->addWidget(&m_labelAfeVerC1);
    vlayout1->addWidget(&m_labelAfeVerC2);
    vlayout1->addWidget(&m_labelAfeVerC3);
    vlayout1->addWidget(&m_labelStartTimesC);
    vlayout1->addWidget(&m_labelCycleCntC);

    QVBoxLayout *vlayout2 = new QVBoxLayout;
    vlayout2->addWidget(&m_labelVendor);
    vlayout2->addWidget(&m_labelModel);
    vlayout2->addWidget(&m_labelSN);
    vlayout2->addWidget(&m_labelSoftVer);
    vlayout2->addWidget(&m_labelHardVer);
    vlayout2->addWidget(&m_labelBoot);
    vlayout2->addWidget(&m_labelDatetime);
    vlayout2->addWidget(&m_labelSPU);
    vlayout2->addWidget(&m_labelWPU);
    vlayout2->addWidget(&m_labelCCU);
    vlayout2->addWidget(&m_labelMCU);
    vlayout2->addWidget(&m_labelAdcId0);
    vlayout2->addWidget(&m_labelAdcId1);
    vlayout2->addWidget(&m_labelAfeVer0);
    vlayout2->addWidget(&m_labelAfeVer1);
    vlayout2->addWidget(&m_labelAfeVer2);
    vlayout2->addWidget(&m_labelAfeVer3);
    vlayout2->addWidget(&m_labelStartTimes);
    vlayout2->addWidget(&m_labelCycleCnt);

    layout->addLayout(vlayout1);
    layout->addLayout(vlayout2);
    setLayout(layout);

    m_labelAdcId1.hide();
    m_labelAdcIdC1.hide();

    m_labelTitleCaption.setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
    m_labelVendorCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelVendor.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelModelCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelModel.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelSNCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelSN.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelSoftVerCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelSoftVer.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelHardVerCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelHardVer.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelBootCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelBoot.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelDatetimeCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelDatetime.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelSPUCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelSPU.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelWPUCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelWPU.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelCCUCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelCCU.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelMCUCaption.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelMCU.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);

    m_labelAdcIdC0.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAdcIdC1.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVerC0.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVerC1.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVerC2.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVerC3.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAdcId0.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAdcId1.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVer0.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVer1.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVer2.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelAfeVer3.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);

    m_labelStartTimesC.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelStartTimes.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelCycleCnt.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
    m_labelCycleCntC.setAlignment(Qt::AlignVCenter|Qt::AlignLeft);

    m_picture.setGeometry(ultraRect);
    m_picture.setPixmap(QPixmap(ultraPic));

    m_qrencode.setGeometry(qrencodeRect);
    m_qrencode.setPixmap(QPixmap(qrencodePic));

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(closeRect);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(close()) );
}

void CSysInfoDlg::keyReleaseEvent(QKeyEvent */*event*/)
{
    hide();
}

void CSysInfoDlg::keyPressEvent(QKeyEvent *event)
{
    if ( event->key() != R_Pkey_F1 )
    {
        hide();
    }
}

void CSysInfoDlg::show(bool project)
{
    menu_res::RInfoWnd::show();

    int cnt = sysGetCycleCnt();
    m_labelCycleCnt.setText(QString("%1:%2:%3").arg(cnt/3600).arg(cnt/60%60).arg(cnt%60));

    changeLabelStyle();
    langChanged();
    projectModelChanged(project);
}

void CSysInfoDlg::changeLabelStyle()
{
//    if(sysGetLanguage() == language_english )
//    {
//        this->setStyleSheet("QLabel{color: #c0c0c0;font-family:Arial; font:10pt;}");
//    }
//    else
    {
        this->setStyleSheet("QLabel{color: #c0c0c0;font-family:Droid Sans Fallback; font:10pt;}");
    }
}

void CSysInfoDlg::langChanged()
{
    m_labelTitleCaption.setText(sysGetString(INF_SYS_INFO,"System inforamtion"));
    m_labelVendorCaption.setText(sysGetString(INF_SYS_VENDOR,"Manufacturer:"));
    m_labelModelCaption.setText(sysGetString(INF_SYS_MODEL, "Model:"));
    m_labelSNCaption.setText( sysGetString(INF_SYS_SERIAL,"Serial number:"));
    m_labelSoftVerCaption.setText(sysGetString(INF_SYS_FIRMWARE,"Firmware:"));
    m_labelHardVerCaption.setText(sysGetString(INF_SYS_HARDWARE,"Hardware:"));
    m_labelBootCaption.setText(sysGetString(INF_SYS_BOOT,"Boot:"));
    m_labelDatetimeCaption.setText(sysGetString(INF_SYS_BUILD,"Build date:"));
    m_labelSPUCaption.setText("FPGA.SPU:");
    m_labelWPUCaption.setText("FPGA.WPU:");
    m_labelCCUCaption.setText("FPGA.FCU:");
    m_labelMCUCaption.setText("FPGA.SP6:");
    m_labelAdcIdC0.setText("ADC.ID0:");
    m_labelAdcIdC1.setText("ADC.ID0:");
    m_labelAfeVerC0.setText("AFE.VER0:");
    m_labelAfeVerC1.setText("AFE.VER1:");
    m_labelAfeVerC2.setText("AFE.VER2:");
    m_labelAfeVerC3.setText("AFE.VER3:");
    m_labelStartTimesC.setText("Started:");
    m_labelCycleCntC.setText("Live Time:");
    m_labelVendor.setText("RIGOL TECHNOLOGIES");
}

void CSysInfoDlg::projectModelChanged(bool b)
{
    if( b )
    {
        m_labelSPUCaption.show();
        m_labelWPUCaption.show();
        m_labelCCUCaption.show();
        m_labelMCUCaption.show();
        m_labelAdcIdC0.show();
        m_labelAfeVerC0.show();
        m_labelAfeVerC1.show();
        m_labelAfeVerC2.show();
        m_labelAfeVerC3.show();
        m_labelStartTimesC.show();
        m_labelCycleCntC.show();

        m_labelStartTimes.show();
        m_labelMCU.show();
        m_labelCCU.show();
        m_labelSPU.show();
        m_labelWPU.show();
        m_labelAdcId0.show();
        m_labelAfeVer0.show();
        m_labelAfeVer1.show();
        m_labelAfeVer2.show();
        m_labelAfeVer3.show();
        m_labelCycleCnt.show();

        resize(500,420);
    }
    else
    {
        m_labelSPUCaption.hide();
        m_labelWPUCaption.hide();
        m_labelCCUCaption.hide();
        m_labelMCUCaption.hide();

        m_labelAdcIdC0.hide();
        m_labelAfeVerC0.hide();
        m_labelAfeVerC1.hide();
        m_labelAfeVerC2.hide();
        m_labelAfeVerC3.hide();

        m_labelStartTimesC.hide();
        m_labelCycleCntC.hide();

        m_labelStartTimes.hide();
        m_labelMCU.hide();
        m_labelCCU.hide();
        m_labelSPU.hide();
        m_labelWPU.hide();
        m_labelAdcId0.hide();
        m_labelAfeVer0.hide();
        m_labelAfeVer1.hide();
        m_labelAfeVer2.hide();
        m_labelAfeVer3.hide();
        m_labelCycleCnt.hide();

        resize(500,200);
    }
}


