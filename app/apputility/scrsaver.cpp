#include <QPainter>
#include <QDebug>
#include "../../include/dsocfg.h"
#include "../../service/servdso/sysdso.h"
#include "../../service/servutility/servutility.h"
#include "scrsaver.h"

CScrSaverDlg::CScrSaverDlg(int pic_or_text)
{
    m_nContentType = pic_or_text;

    m_strContent = ":/pictures/utility/scr.jpg";
    setWindowFlags( Qt::FramelessWindowHint | Qt::Popup );
    this->setStyleSheet("background:black");

    addAllKeys();
}

CScrSaverDlg::~CScrSaverDlg()
{
    this->hide();
}

void CScrSaverDlg::setString(QString &s)
{
    m_strContent = s;
}

void CScrSaverDlg::setContent(int c)
{
    m_nContentType = c;
}

void CScrSaverDlg::show()
{
    if(m_nContentType > servUtility::scr_saver_off)
    {
        if(m_nContentType == servUtility::scr_saver_pic)
        {
            m_scrImage.load(m_strContent);

            if(m_scrImage.width() > screen_width ||
               m_scrImage.height() > screen_height)
            {
                m_scrImage.scaled(screen_width,
                                  screen_height,
                                  Qt::KeepAspectRatio);
            }
        }
        setGeometry( 0,0, screen_width, screen_height);

        setVisible( true );
        connect(&m_Timer, SIGNAL(timeout()), this, SLOT(onMoveScr()));

    }
}

void CScrSaverDlg::paintEvent(QPaintEvent *)
{
    if(m_nContentType > servUtility::scr_saver_off)
    {
        QPainter painter(this);


        if(m_nContentType == servUtility::scr_saver_pic)
        {
            int x = 0;
            int y = 0;

            if( m_scrImage.width() < screen_width )
            {
                x = rand() % (screen_width  - m_scrImage.width());
            }

            if( m_scrImage.height() < screen_height )
            {
                y = rand() % (screen_height - m_scrImage.height());
            }

            painter.drawImage(x, y,  m_scrImage);
        }
        else
        {
            int fontSize = 40;
            QFont font( qApp->font().family(), fontSize);
            painter.setFont( font );

            painter.setPen( QPen(Qt::yellow) );

            int realSize = sysGetNormalSpace(m_strContent);

            int x = rand() % (screen_width  - realSize*fontSize);
            int y = rand() % (screen_height - fontSize*2);
            QRect pos;
            pos.setX(x);
            pos.setY(y);
            pos.setWidth(realSize * fontSize);
            pos.setHeight(fontSize*2);
            painter.drawText(pos,
                             Qt::AlignLeft | Qt::AlignTop,
                             m_strContent);
        }
        m_Timer.start(5000);//5m
    }
}

void CScrSaverDlg::keyPressEvent(QKeyEvent *key)
{
    mousePressEvent(0);
    key->accept();
}

void CScrSaverDlg::mousePressEvent(QMouseEvent *m)
{
    if( m!= 0 )
    {
        m->accept();
    }
    this->hide();
    closeEvent(0);

    disconnect(&m_Timer, SIGNAL(timeout()), this, SLOT(onMoveScr()));

    serviceExecutor::post(E_SERVICE_ID_UTILITY,
                          servUtility::cmd_dso_saver_stop,0);
}

void CScrSaverDlg::closeEvent(QCloseEvent *)
{
    //qDebug() << "Exit from screen saver";
    m_Timer.stop();
}

void CScrSaverDlg::onMoveScr()
{
    if(m_nContentType > servUtility::scr_saver_off)
    {
       update();
    }
}
