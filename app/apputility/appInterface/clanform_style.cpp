#include "clanform_style.h"

#include "../../menu/menustyle/rcontrolstyle.h"
#include "../../meta/crmeta.h"

CLanForm_Style::CLanForm_Style()
{

}

void CLanForm_Style::load( const QString &path,
                           const r_meta::CMeta &meta )
{

    meta.getMetaVal( path + "geo", mRectGeo );
    meta.getMetaVal( path + "allStyle",mAllStyle);
    meta.getMetaVal( path + "ipStyle",mIPStyle);
    meta.getMetaVal( path + "title", mTitle );
    meta.getMetaVal( path + "lxi",   mLXI );
    meta.getMetaVal( path + "line",  mLine );

    meta.getMetaVal( path + "lanStatus", mLanStatus );
    meta.getMetaVal( path + "lanType",   mLanType );
    meta.getMetaVal( path + "lanMAC",    mLanMAC );
    meta.getMetaVal( path + "lanVisa",   mLanVisa );

    meta.getMetaVal( path + "lanStatusCont", mLanStatusCont );
    meta.getMetaVal( path + "lanTypeCont",   mLanTypeCont );
    meta.getMetaVal( path + "lanMacCont",    mLanMacCont );
    meta.getMetaVal( path + "lanVisaCont",   mLanVisaCont );

    meta.getMetaVal( path + "ip",      mIp );
    meta.getMetaVal( path + "mask",    mMask );
    meta.getMetaVal( path + "gateway", mGateway );
    meta.getMetaVal( path + "dns",     mDns );

    meta.getMetaVal( path + "eip",      eIp );
    meta.getMetaVal( path + "emask",    eMask );
    meta.getMetaVal( path + "egateway", eGateway );
    meta.getMetaVal( path + "edns",     eDns );

    meta.getMetaVal( path + "linepic", mLinePath );
    meta.getMetaVal( path + "lxipic",  mLxiPath );

    meta.getMetaVal( path + "dhcp", mDHCP );
    meta.getMetaVal( path + "auto",  mAuto );
    meta.getMetaVal( path + "static", mStatic );

    meta.getMetaVal( path + "dhcpicon", mDHCPIcon );
    meta.getMetaVal( path + "autoicon",  mAutoIcon );
    meta.getMetaVal( path + "staticicon", mStaticIcon );

}

