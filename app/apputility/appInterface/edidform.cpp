#include "edidform.h"
#include "ui_edidform.h"

EdidForm::EdidForm(QWidget *parent) :
    uiWnd(parent),
    ui(new Ui::EdidForm)
{
    ui->setupUi(this);
}

EdidForm::~EdidForm()
{
    delete ui;
}


void EdidForm::setData(void *ptr)
{
    m_pData = (stEDID*)ptr;

    ui->pid->setText(  m_pData->pid );
    ui->vid->setText( m_pData->mid );
    ui->serial->setText( m_pData->sid );

    ui->version->setText( m_pData->version );

    this->setStyleSheet("QLabel{color: #ffffff; font:12pt;}");

//    QString res = "";
//    foreach(stEDID_TIMES*p, m_pData->establish_times)
//    {
//        res = res + QString("%1x%2@%3hz\n").arg(p->x).arg(p->y).arg(p->hz);
//    }

    ui->week->setText( QString("%1").arg(m_pData->week));
    ui->year->setText( QString("%1").arg(m_pData->year));
    //ui->resolution->setText( res );
    ui->checkSum->setText( QString("0x%1").arg((int)m_pData->CheckSum,2,16));
    ui->extension->setText( (m_pData->ExtensionFlag?"Yes":"No"));

    this->move((1024 - this->width())/2, (600-this->height())/2);
}
