#include <QDebug>

#include "../../service/service_name.h"
#include "../../service/servutility/servinterface/servInterface.h"
#include "../../gui/cguiformatter.h"
#include "../appmain/cappmain.h"
#include "appInterface.h"

IMPLEMENT_CMD( CApp, CAppInterface )
start_of_entry()

on_set_int_int  (   CMD_SERVICE_SUB_ENTER,          &CAppInterface::onShowForm),
on_set_int_int  (   CMD_SERVICE_SUB_RETURN,         &CAppInterface::onHideForm),
on_set_int_int  (   CMD_SERVICE_EXIT_ACTIVE  ,      &CAppInterface::onHideForm),

on_set_int_int  (   MSG_APP_UTILITY_LXI_CFG,        &CAppInterface::updateMode),

on_set_int_int  (   MSG_APP_UTILITY_LXI_IPADDRESS,  &CAppInterface::onInputIP),
on_set_int_int  (   MSG_APP_UTILITY_LXI_SUBNET,     &CAppInterface::onInputMask),
on_set_int_int  (   MSG_APP_UTILITY_LXI_GATEWAY,    &CAppInterface::onInputGateway),
on_set_int_int  (   MSG_APP_UTILITY_LXI_DNS,        &CAppInterface::onInputDNS),

on_set_int_int  (    servInterface::cmd_net_status,       &CAppInterface::updateStatus),
on_set_int_int  (    servInterface::cmd_net_config,       &CAppInterface::updateStatus),

on_set_int_int  (    servInterface::cmd_net_ip,           &CAppInterface::updateIP),
on_set_int_int  (    servInterface::cmd_net_mask,         &CAppInterface::updateMask),
on_set_int_int  (    servInterface::cmd_net_gateway,      &CAppInterface::updateGateway),
on_set_int_int  (    servInterface::cmd_net_dns,          &CAppInterface::updateDNS),
on_set_int_int  (    servInterface::cmd_net_rst,          &CAppInterface::onShowForm),


//on_set_void_void(   servInterface::cmd_iowin_dis,        &CAppInterface::LanWndDis),
//on_set_int_void(    servInterface::cmd_state_change,     &CAppInterface::ChangeLanState),

end_of_entry()

CAppInterface::CAppInterface()
{
    mservName  = serv_name_utility_IOset;

    m_pLanForm = NULL;
    m_nNetMode = 0;

    m_pEDID = NULL;

}

void CAppInterface::setupUi( CAppMain */*pMain*/ )
{

}

void CAppInterface::retranslateUi()
{}
void CAppInterface::buildConnection()
{}

void CAppInterface::onFormClose()
{
    if(m_pLanForm)
    {
        serviceExecutor::post(mservName,
                              servInterface::cmd_close_form,
                              0);
    }
}

DsoErr CAppInterface::onHideForm(int)
{
    onFormClose();
    if(m_pLanForm && m_pLanForm->isVisible())
    {
        m_pLanForm->hide();
    }
    return ERR_NONE;
}

DsoErr CAppInterface::onShowForm(int)
{
    //! filter
    if ( sysGetWorkMode() != work_normal )
    { return ERR_NONE; }

    bool b = false;
    serviceExecutor::query(mservName, CMD_SERVICE_SUB_ENTER, b);

    if(b)
    {
        if ( m_pLanForm == NULL )
        {
            m_pLanForm = new CLanForm();
            Q_ASSERT( m_pLanForm != NULL );

            m_pLanForm->setupUi();

            connect(m_pLanForm,
                    SIGNAL(sigClose()),
                    this,
                    SLOT(onFormClose()) );
        }

        if(m_pLanForm->isHidden())
        {
            updateStatus(0);
            updateMAC(0);

            m_pLanForm->show();
        }
    }
    return ERR_NONE;
}


DsoErr CAppInterface::updateMode(int)
{
    if(m_pLanForm)
    {
        int mode = servInterface::IP_ACQUIRE_DHCP;
        serviceExecutor::query(mservName, MSG_APP_UTILITY_LXI_CFG, mode);
        //qDebug()<<"app updateMode() "<<mode;
        if(mode & servInterface::IP_ACQUIRE_DHCP)
        {
            m_pLanForm->m_pDhcpStatus->setPixmap(QPixmap(":/pictures/menu/checked.bmp"));
        }else{
            m_pLanForm->m_pDhcpStatus->setPixmap(QPixmap(":/pictures/menu/unchecked.bmp"));
        }

        if(mode & servInterface::IP_ACQUIRE_AUTO){
            m_pLanForm->m_pAutoStatus->setPixmap(QPixmap(":/pictures/menu/checked.bmp"));
        }else{
            m_pLanForm->m_pAutoStatus->setPixmap(QPixmap(":/pictures/menu/unchecked.bmp"));
        }

        if(mode & servInterface::IP_ACQUIRE_STATIC)
        {
            m_pLanForm->m_pStaticStatus->setPixmap(QPixmap(":/pictures/menu/checked.bmp"));
        }else{
            m_pLanForm->m_pStaticStatus->setPixmap(QPixmap(":/pictures/menu/unchecked.bmp"));
        }

        if( mode == servInterface::IP_ACQUIRE_STATIC )
        {
            m_nNetMode = mode;
        }
    }
    return ERR_NONE;
}

DsoErr CAppInterface::updateStatus(int)
{
    if(m_pLanForm)
    {
        QStringList status;
        status << "Disconnected"            // 0
               << "Connected"               // 1
               << "Acquiring IP..."            // 2
               << "Network Config Failed"       // 4
               << "IP Conflict"             // 3
               << "Please wait..."           // 5
               << "Network Config Success"      // 6
               << "DHCP Config Failed"              // 7
               << "Invalid IP"               // 8
               << "IP lost";


        int s = 0;
        serviceExecutor::query(mservName, servInterface::cmd_net_status, s);

        //qDebug() << s << servInterface::NET_STATUS_UNLINK;
        int t = s - servInterface::NET_STATUS_UNLINK;
        if(t >=0 && t < status.size())
        {
            if( s == servInterface::NET_STATUS_CONFIGURED ||
                s == servInterface::NET_STATUS_CONNECTED)
            {
                m_pLanForm->m_strStatus->setStyleSheet("color:#00ff00");
            }
            else
            {
                m_pLanForm->m_strStatus->setStyleSheet("color:#ff0000");
            }

            m_pLanForm->m_strStatus->setText(status.at(s));
        }

        if( s != servInterface::NET_STATUS_INIT )
        {
            updateMode(0);
            updateNetMode(0);
            updateIP(0);
            updateMask(0);
            updateGateway(0);
            updateDNS(0);
        }

        //by bug1637
        //if(s == servInterface::NET_STATUS_CONFIGURED)
        {
            m_pLanForm->m_strVisa->setText("TCPIP::" +
                                           m_pLanForm->m_pAddrIP->text() +
                                           "::INSTR");
        }

    }
    return ERR_NONE;
}

DsoErr CAppInterface::updateNetMode(int)
{
    if(m_pLanForm)
    {
        int mode = servInterface::IP_ACQUIRE_DHCP;
        serviceExecutor::query(mservName, MSG_APP_UTILITY_LXI_CFG, mode);

        if(mode & servInterface::IP_ACQUIRE_DHCP)
        {
            m_pLanForm->m_strIPMode->setText("DHCP");
        }
        else if(mode & servInterface::IP_ACQUIRE_AUTO)
        {
            m_pLanForm->m_strIPMode->setText("AUTO");
        }
        else if(mode & servInterface::IP_ACQUIRE_STATIC)
        {
            m_pLanForm->m_strIPMode->setText("STATIC");
        }
        m_nNetMode = mode;
    }
    return ERR_NONE;
}

DsoErr CAppInterface::updateIP(int)
{
    if(m_pLanForm)
    {
        QString ipstr;

        serviceExecutor::query(mservName, servInterface::cmd_net_ip, ipstr);
        m_pLanForm->m_pAddrIP->setText(ipstr);
    }

    return ERR_NONE;
}

DsoErr CAppInterface::updateMask(int)
{
    if(m_pLanForm)
    {
        QString maskstr;

        serviceExecutor::query(mservName, servInterface::cmd_net_mask, maskstr);

        m_pLanForm->m_pAddrMask->setText(maskstr);
    }

    return ERR_NONE;
}

DsoErr CAppInterface::updateGateway(int)
{
    if(m_pLanForm)
    {
        QString gatewaystr;

        serviceExecutor::query(mservName, servInterface::cmd_net_gateway, gatewaystr);

        m_pLanForm->m_pAddrGateway->setText(gatewaystr);
    }

    return ERR_NONE;
}

DsoErr CAppInterface::updateDNS(int)
{
    if(m_pLanForm)
    {
        QString dns;

        serviceExecutor::query(mservName, servInterface::cmd_net_dns, dns);

        m_pLanForm->m_pAddrDNS->setText(dns);
    }

    return ERR_NONE;
}

DsoErr CAppInterface::updateMAC(int)
{
    if(m_pLanForm)
    {
        QString mac;

        serviceExecutor::query(mservName, servInterface::cmd_net_mac, mac);

        mac = mac.toUpper();
        m_pLanForm->m_strMAC->setText(mac);
    }

    return ERR_NONE;
}

DsoErr CAppInterface::onInputIP(int val)
{
    if(m_pLanForm && val == 0 && m_nNetMode == servInterface::IP_ACQUIRE_STATIC)
    {
        callInput(m_pLanForm->m_pAddrIP->text(), INPUT_IP);
    }
    return ERR_NONE;
}

void CAppInterface::on_input_ip(QString &ip)
{
    serviceExecutor::post(mservName, servInterface::cmd_net_ip, ip);
}

DsoErr CAppInterface::onInputMask(int val)
{
    if(m_pLanForm && val == 0 && m_nNetMode == servInterface::IP_ACQUIRE_STATIC)
    {
        callInput(m_pLanForm->m_pAddrMask->text(), INPUT_MASK);
    }
    return ERR_NONE;
}

void CAppInterface::on_input_mask(QString &mask)
{
    serviceExecutor::post(mservName, servInterface::cmd_net_mask, mask);
}

DsoErr CAppInterface::onInputGateway(int val)
{
    if(m_pLanForm && val == 0 &&
        (m_nNetMode == servInterface::IP_ACQUIRE_STATIC || m_nNetMode == servInterface::IP_ACQUIRE_AUTO) )
    {
        callInput(m_pLanForm->m_pAddrGateway->text(), INPUT_GATEWAY);
    }
    return ERR_NONE;
}

void CAppInterface::on_input_gateway(QString &gate)
{
    serviceExecutor::post(mservName, servInterface::cmd_net_gateway, gate);
}

DsoErr CAppInterface::onInputDNS(int val)
{
    if(m_pLanForm && val == 0 &&
            (m_nNetMode == servInterface::IP_ACQUIRE_STATIC || m_nNetMode == servInterface::IP_ACQUIRE_AUTO))
    {
        callInput(m_pLanForm->m_pAddrDNS->text(), INPUT_DNS);
    }
    return ERR_NONE;
}

void CAppInterface::on_input_dns(QString &dns)
{
    serviceExecutor::post(mservName, servInterface::cmd_net_dns, dns);
}


void CAppInterface::callInput(QString addr, int type)
{
    DsoReal now;
    DsoReal max;
    DsoReal min;
    DsoReal def;

    DsoRealGp vg;

    //qDebug() << __FUNCTION__ << __LINE__;

    QStringList ip = addr.split(".");

    if(ip.size() == 4)
    {
        QString val = ip.at(0);
        qlonglong iNow = val.toInt() << 24;
        val = ip.at(1);
        iNow = iNow | (val.toInt() << 16);

        val = ip.at(2);
        iNow = iNow | (val.toInt() << 8);

        val = ip.at(3);
        iNow = iNow | (val.toInt() );

        def.setVal(iNow);
        now.setVal(iNow);
        max.setVal((qlonglong)0xFFFFFFFF);
        min.setVal((qlonglong)0x0);

        vg.setValue(now, max, min, def);
        InputKeypad* inputMethod = InputKeypad::getInstance(0);
        InputKeypad::rst();

        if(type == INPUT_IP)
        {
            connect(inputMethod,
                    SIGNAL(OnOK(QString&)),
                    this,
                    SLOT(on_input_ip(QString&)));
        }
        else if(type == INPUT_MASK)
        {
            connect(inputMethod,
                    SIGNAL(OnOK(QString&)),
                    this,
                    SLOT(on_input_mask(QString&)));
        }
        else if(type == INPUT_GATEWAY)
        {
            connect(inputMethod,
                    SIGNAL(OnOK(QString&)),
                    this,
                    SLOT(on_input_gateway(QString&)));
        }
        else if(type == INPUT_DNS)
        {
            connect(inputMethod,
                    SIGNAL(OnOK(QString&)),
                    this,
                    SLOT(on_input_dns(QString&)));
        }
        else
        {
            return;
        }

        //qDebug() << __FUNCTION__ << __LINE__ << ip;

        inputMethod->input(InputKeypad::IPAddress, vg);
    }

}
