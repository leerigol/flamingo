#ifndef CSLANWND_H
#define CSLANWND_H

#include "../../menu/wnd/rmodalwnd.h"
#include "clanform_style.h"

class CLanForm : public menu_res::RModalWnd
{
    Q_OBJECT
    static CLanForm_Style _style;
public:
    CLanForm(QWidget *parent = 0);
    ~CLanForm();
    void show();
    void hide();
    void setupUi();

public:
    QLabel      *m_labelTitle;
//    QLabel      *m_labelLine;
    QLabel      *m_labelLxi;
    QLabel      *m_labelStatus;
    QLabel      *m_strStatus;
    QLabel      *m_labelIPMode;
    QLabel      *m_strIPMode;
    QLabel      *m_labelMAC;
    QLabel      *m_strMAC;
    QLabel      *m_labelVisa;
    QLabel      *m_strVisa;
    QLabel      *m_labelIP;
    QLabel      *m_labelMask;
    QLabel      *m_labelGateway;
    QLabel      *m_labelDNS;

    QLabel      *m_pAddrIP;
    QLabel      *m_pAddrMask;
    QLabel      *m_pAddrGateway;
    QLabel      *m_pAddrDNS;

    QLabel      *m_plabelDhcp;
    QLabel      *m_plabelAuto;
    QLabel      *m_plabelStatic;

    QLabel      *m_pDhcpStatus;
    QLabel      *m_pAutoStatus;
    QLabel      *m_pStaticStatus;

//    QPushButton *m_pBtnDhcp;
//    QPushButton *m_pBtnAuto;
//    QPushButton *m_pBtnStatic;
};
#endif // CSLANWND_H

