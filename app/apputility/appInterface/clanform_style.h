#ifndef CLanForm_STYLE_H
#define CLanForm_STYLE_H

#include "../../menu/menustyle/rui_style.h"

class CLanForm_Style : public menu_res::RUI_Style
{
public:
    CLanForm_Style();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QRect mRectGeo;

    QRect mTitle;
    QRect mLXI;
    QRect mLine;
    QRect mLanStatus,    mLanType,     mLanMAC,     mLanVisa;
    QRect mLanStatusCont,mLanTypeCont, mLanMacCont, mLanVisaCont;
    QRect mDHCP, mAuto, mStatic;
    QRect mDHCPIcon, mAutoIcon, mStaticIcon;

    QRect mIp,mMask,mGateway,mDns;
    QRect eIp,eMask,eGateway,eDns;

    QString mAllStyle;
    QString mIPStyle;

    QString mLinePath;
    QString mLxiPath;
};

#endif // CLanForm_STYLE_H
