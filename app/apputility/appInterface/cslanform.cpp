#include "clanform.h"


CLanForm_Style CLanForm::_style;
CLanForm::CLanForm( QWidget */*parent*/)
{
    setAttr(wnd_moveable);
//    state = "LINK";
//    setmode = "";
//    lanmac = "00-06-19-34-a0-cd";
//    visa = "TCPIP::172.16.3.111::INSTR";
}

CLanForm::~CLanForm()
{
    this->hide();
}

void CLanForm::setupUi()
{
     _style.loadResource("ui/LanSetting/");

    m_labelTitle    = new QLabel(this);
    m_labelLxi      = new QLabel(this);

    m_labelStatus   = new QLabel(this);
    m_strStatus     = new QLabel(this);

    m_labelIPMode   = new QLabel(this);
    m_strIPMode     = new QLabel(this);

    m_labelMAC      = new QLabel(this);
    m_strMAC        = new QLabel(this);

    m_labelVisa     = new QLabel(this);
    m_strVisa       = new QLabel(this);

    m_labelIP       = new QLabel(this);
    m_labelMask     = new QLabel(this);
    m_labelGateway  = new QLabel(this);
    m_labelDNS      = new QLabel(this);

    m_pAddrIP       = new QLabel(this);
    m_pAddrMask     = new QLabel(this);
    m_pAddrGateway  = new QLabel(this);
    m_pAddrDNS      = new QLabel(this);

    m_plabelDhcp    = new QLabel(this);
    m_plabelAuto    = new QLabel(this);
    m_plabelStatic = new QLabel(this);

    m_pDhcpStatus    = new QLabel(this);
    m_pAutoStatus    = new QLabel(this);
    m_pStaticStatus = new QLabel(this);

    m_pAutoStatus->setGeometry(_style.mAutoIcon);
    m_pDhcpStatus->setGeometry(_style.mDHCPIcon);
    m_pStaticStatus->setGeometry(_style.mStaticIcon);

    m_plabelDhcp->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_plabelAuto->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_plabelStatic->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    m_labelStatus->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_labelIPMode->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_labelMAC->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_labelVisa->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    m_labelIP->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_labelMask->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_labelGateway->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_labelDNS->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    m_strStatus->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    m_strIPMode->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    m_strVisa->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    m_strMAC->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

    setGeometry(_style.mRectGeo);
    this->setStyleSheet(_style.mAllStyle);

    m_labelLxi->setGeometry(_style.mLXI );
    m_labelLxi->setPixmap(QPixmap(_style.mLxiPath));
    m_labelTitle->setGeometry(_style.mTitle );

//    m_labelLine->setGeometry(_style.mLine);
//    m_labelLine->setPixmap(QPixmap(_style.mLinePath));

    m_labelStatus->setGeometry(_style.mLanStatus);
    m_labelIPMode->setGeometry(_style.mLanType);
    m_labelMAC->setGeometry(_style.mLanMAC);
    m_labelVisa->setGeometry(_style.mLanVisa);

    m_strStatus->setGeometry(_style.mLanStatusCont);
    m_strIPMode->setGeometry(_style.mLanTypeCont);
    m_strMAC->setGeometry(_style.mLanMacCont);
    m_strVisa->setGeometry(_style.mLanVisaCont);

    m_labelIP->setGeometry(_style.mIp);
    m_labelMask->setGeometry(_style.mMask);
    m_labelGateway->setGeometry(_style.mGateway);
    m_labelDNS->setGeometry(_style.mDns);

    m_pAddrIP->setGeometry(_style.eIp);
    m_pAddrMask->setGeometry(_style.eMask);
    m_pAddrGateway->setGeometry(_style.eGateway);
    m_pAddrDNS->setGeometry(_style.eDns);

    m_plabelDhcp->setGeometry(_style.mDHCP);
    m_plabelAuto->setGeometry(_style.mAuto);
    m_plabelStatic->setGeometry(_style.mStatic);


    m_pAddrIP->setStyleSheet(_style.mIPStyle);
    m_pAddrMask->setStyleSheet(_style.mIPStyle);
    m_pAddrGateway->setStyleSheet(_style.mIPStyle);
    m_pAddrDNS->setStyleSheet(_style.mIPStyle);
}

void CLanForm::show()
{

    m_labelTitle->setText( sysGetString(INF_LAN_SETTING));
    m_labelStatus->setText( sysGetString(INF_LAN_STATUS)+":");
    m_labelIPMode->setText( sysGetString( INF_IP_TYPE)+":");
    m_labelMAC->setText( sysGetString(INF_MAC_ADDR)+":");
    m_labelVisa->setText( sysGetString(INF_VISA_ADDR)+":");
    m_labelIP->setText( sysGetString(INF_IP_ADDR)+":");
    m_labelMask->setText(sysGetString(INF_SUBNET)+":");
    m_labelGateway->setText( sysGetString(INF_GATEWAY)+":");
    m_labelDNS->setText( sysGetString(INF_DNS)+":");

    m_plabelDhcp->setText( sysGetString(INF_DHCP)+":");
    m_plabelAuto->setText( sysGetString(INF_AUTOIP)+":");
    m_plabelStatic->setText( sysGetString(INF_STATIC_IP)+":");


    return menu_res::RModalWnd::show();
}


//void CLanForm::keyPressEvent(QKeyEvent *event)
//{
//    Q_ASSERT( NULL != event );
//    if(event->key() == R_Pkey_PageReturn)
//    {
//        hide();
//    }
//}

void CLanForm::hide()
{
    return menu_res::RModalWnd::hide();
}
