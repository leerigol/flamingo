#ifndef EDIDFORM_H
#define EDIDFORM_H

#include "../../menu/wnd/uiwnd.h"
#include "../../service/servutility/servinterface/edid.h"

namespace Ui {
class EdidForm;
}

class EdidForm : public menu_res::uiWnd
{
    Q_OBJECT

public:
    explicit EdidForm(QWidget *parent = 0);
    ~EdidForm();

    void setData(void*);

private:
    Ui::EdidForm *ui;
    stEDID* m_pData;
};

#endif // EDIDFORM_H
