#ifndef CAppLan_H
#define CAppLan_H
#include "../../app/capp.h"
#include "../../include/dsotype.h"
#include "../../baseclass/dsoreal.h"
#include "clanform.h"
#include "edidform.h"

class CAppInterface : public CApp
{
    Q_OBJECT

    DECLARE_CMD()
public:

    enum
    {
        INPUT_IP = 0xABCD,
        INPUT_MASK,
        INPUT_GATEWAY,
        INPUT_DNS
    };

public:
    CAppInterface();
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();
    virtual void buildConnection();


private:
    DsoErr updateMode(int);
    DsoErr updateStatus(int);
    DsoErr updateNetMode(int);

    DsoErr updateIP(int);
    DsoErr updateMask(int);
    DsoErr updateGateway(int);
    DsoErr updateDNS(int);
    DsoErr updateMAC(int);

    DsoErr onShowForm(int);
    DsoErr onHideForm(int);

    DsoErr onInputIP(int);
    DsoErr onInputMask(int);
    DsoErr onInputGateway(int);
    DsoErr onInputDNS(int);

    void   callInput(QString addr, int type);

private slots:
    void on_input_ip(QString &ip);
    void on_input_mask(QString &mask);
    void on_input_gateway(QString &gate);
    void on_input_dns(QString &dns);

    void onFormClose();

private:
    CLanForm *m_pLanForm;

    int       m_nNetMode;

    EdidForm        *m_pEDID;


};
#endif // CAppLan_H

