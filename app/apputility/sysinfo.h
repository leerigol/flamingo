#ifndef APP_SYS_INFO_H
#define APP_SYS_INFO_H

#include "../../menu/wnd/rinfownd.h"
#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"
#include "../../com/keyevent/rdsokey.h"

using namespace menu_res;
class CSysInfoDlg : public menu_res::RInfoWnd
{
    Q_OBJECT

public:
    CSysInfoDlg( QWidget *parent = 0);
    ~CSysInfoDlg();
    void loadResource(QString path);
    virtual void paintEvent( QPaintEvent *event );

    static menu_res::RModelWnd_Style _bgStyle;

public:
    void setMode(QString model);
    void setSerialSn(QString sn);
    void setSoftwarVer(QString softwarver);
    void setHardwarVer(QString hardwarver);
    void setBootVer(QString bootver);
    void setDatetime(QString dt);
    void setSPUVer(QString spuver);
    void setWPUVer(QString wpuver);
    void setCCUVer(QString ccuver);
    void setMCUVer(QString mcuver);
    void setADCChipId0(int id);
    void setADCChipId1(int id);
    void setAFEChipVer0(int ver);
    void setAFEChipVer1(int ver);
    void setAFEChipVer2(int ver);
    void setAFEChipVer3(int ver);
    void setStartTimes(int times);
    void setupUI();
    void show(bool);

protected:
    void keyPressEvent( QKeyEvent *event );
    void keyReleaseEvent(QKeyEvent *event);

private:
    void changeLabelStyle();
    void langChanged();
    void projectModelChanged(bool b);

private:
    RImageButton   *m_pCloseButton;

    QLabel m_labelTitleCaption;
    QLabel m_labelVendorCaption;
    QLabel m_labelModelCaption;
    QLabel m_labelSNCaption;
    QLabel m_labelSoftVerCaption;
    QLabel m_labelHardVerCaption;
    QLabel m_labelBootCaption;
    QLabel m_labelDatetimeCaption;
    QLabel m_labelSPUCaption;
    QLabel m_labelWPUCaption;
    QLabel m_labelCCUCaption;
    QLabel m_labelMCUCaption;
    QLabel m_labelAdcIdC0;
    QLabel m_labelAdcIdC1;
    QLabel m_labelAfeVerC0;
    QLabel m_labelAfeVerC1;
    QLabel m_labelAfeVerC2;
    QLabel m_labelAfeVerC3;
    QLabel m_labelStartTimesC;
    QLabel m_labelCycleCntC;

    QLabel m_labelVendor;
    QLabel m_labelModel;
    QLabel m_labelSN;
    QLabel m_labelSoftVer;
    QLabel m_labelHardVer;
    QLabel m_labelBoot;
    QLabel m_labelDatetime;
    QLabel m_labelFPGAVersion;
    QLabel m_labelSPU;
    QLabel m_labelWPU;
    QLabel m_labelCCU;
    QLabel m_labelMCU;
    QLabel m_labelAdcId0;
    QLabel m_labelAdcId1;
    QLabel m_labelAfeVer0;
    QLabel m_labelAfeVer1;
    QLabel m_labelAfeVer2;
    QLabel m_labelAfeVer3;
    QLabel m_labelStartTimes;
    QLabel m_labelCycleCnt;

    QLabel m_picture;
    QLabel m_qrencode;

    QRect   closeRect;
    QRect   titleRect;
    QRect   ultraRect;
    QRect   qrencodeRect;

    QString ultraPic, qrencodePic;

};

#endif // APP_SYS_INFO_H
