#ifndef CMAILSETWND_H
#define CMAILSETWND_H

#include <QWidget>
#include "../../menu/wnd/rmodalwnd.h"
#include "cmail_style.h"

class MyLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit MyLineEdit(QWidget *parent = 0);
protected:
    //重写mousePressEvent事件
    virtual void mousePressEvent(QMouseEvent *event);

signals:
    //自定义clicked()信号,在mousePressEvent事件发生时触发
    void clicked();

public slots:

};

class CMailSet : public menu_res::RModalWnd
{
    Q_OBJECT

public:
    CMailSet( QWidget *parent = 0);
    ~CMailSet();

    void setup();

public:
    QLabel *m_pLabelSMTP;
    QLabel *m_pLabelPort;
    QLabel *m_pLabelUser;
    QLabel *m_pLabelPwd;


    MyLineEdit *m_pSMTP;
    MyLineEdit *m_pPort;
    MyLineEdit *m_pUser;
    MyLineEdit *m_pPass;

Q_SIGNALS:
    void OnOK();
    void OnCancel();

protected slots:
    void OnOKClick();
    void OnCancelClick();

    void OnSmtpClick();
    void OnPortClick();
    void OnUserClick();
    void OnPassClick();

    void  OnInputIP(const QString &);
    void  OnInputUser(const QString &);
    void  OnInputPass(const QString &);
    void  OnInputPort(DsoReal);
protected:
    void keyPressEvent( QKeyEvent *event );
    void mousePressEvent(QMouseEvent *event);
private:
    static CMail_Style _style;
    QPushButton *m_pOK;
    QPushButton *m_pCancel;

};

#endif // CMAILSETWND_H

