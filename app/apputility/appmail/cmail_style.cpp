#include "cmail_style.h"

#include "../../menu/menustyle/rcontrolstyle.h"
#include "../../meta/crmeta.h"

CMail_Style::CMail_Style()
{

}

void CMail_Style::load( const QString &path,
                        const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "geo", geo );
    meta.getMetaVal( path + "yesGeo", yesGeo );
    meta.getMetaVal( path + "noGeo", noGeo );
    meta.getMetaVal( path + "labelSMTP", labelSMTP );
    meta.getMetaVal( path + "labelPort", labelPort );
    meta.getMetaVal( path + "labelUser", labelUser );
    meta.getMetaVal( path + "labelPwd", labelPwd );
    meta.getMetaVal( path + "editSMTP", editSMTP );
    meta.getMetaVal( path + "editPort", editPort );
    meta.getMetaVal( path + "editUser", editUser );
    meta.getMetaVal( path + "editPwd", editPwd );

    meta.getMetaVal( path + "qss", qss );
}

