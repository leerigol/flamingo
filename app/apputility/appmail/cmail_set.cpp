#include "../../service/servutility/servmail/servmail.h"
#include "cmail_set.h"

MyLineEdit::MyLineEdit(QWidget *parent) : QLineEdit(parent)
{
}

//重写mousePressEvent事件,检测事件类型是不是点击了鼠标左键
void MyLineEdit::mousePressEvent(QMouseEvent *event)
{
    //如果单击了就触发clicked信号
    if (event->button() == Qt::LeftButton)
    {
        //触发clicked信号
        emit clicked();
    }
    //将该事件传给父类处理
    QLineEdit::mousePressEvent(event);
}

CMail_Style CMailSet::_style;
CMailSet::CMailSet( QWidget */*parent*/)
{

    m_pLabelSMTP = NULL;
    m_pLabelPort = NULL;
    m_pLabelUser = NULL;
    m_pLabelPwd  = NULL;

    m_pPort = NULL;
    m_pSMTP = NULL;
    m_pUser = NULL;
    m_pPass  = NULL;

    m_pOK   = NULL;
    m_pCancel= NULL;

    _style.loadResource("ui/mail/");
    setAttr(wnd_moveable);
}

CMailSet::~CMailSet()
{
    delete m_pLabelSMTP;
    m_pLabelSMTP = NULL;

    delete m_pLabelPort;
    m_pLabelPort = NULL;

    delete m_pLabelUser;
    m_pLabelUser = NULL;

    delete m_pLabelPwd;
    m_pLabelPwd = NULL;


    delete m_pSMTP;
    m_pSMTP = NULL;

    delete m_pPort;
    m_pPort = NULL;

    delete m_pUser;
    m_pUser = NULL;

    delete m_pPass;
    m_pPass = NULL;

    delete m_pOK;
    m_pOK = NULL;

    delete m_pCancel;
    m_pCancel = NULL;
}

void CMailSet::setup()
{
    if(m_pLabelSMTP == NULL)
    {
        m_pLabelSMTP = new QLabel(sysGetString(INF_MAIL_SMTP,"SMTP:"),this);
        m_pLabelPort = new QLabel(sysGetString(INF_MAIL_PORT,"Port:"),this);
        m_pLabelUser = new QLabel(sysGetString(INF_MAIL_USER,"UID:"),this);
        m_pLabelPwd = new QLabel(sysGetString(INF_MAIL_PASS,"PWD:"),this);

        m_pSMTP = new MyLineEdit(this);
        m_pPort = new MyLineEdit(this);
        m_pUser = new MyLineEdit(this);
        m_pPass = new MyLineEdit(this);

        //m_pPass->setInputMask("********");
        m_pPass->setEchoMode(QLineEdit::Password);
        m_pOK = new QPushButton(tr("OK"),this);
        m_pCancel = new QPushButton(tr("Cancel"),this);
    }

    //! placement
    setGeometry( _style.geo );

    m_pLabelSMTP->setGeometry(_style.labelSMTP);
    m_pLabelPort->setGeometry(_style.labelPort);
    m_pLabelUser->setGeometry(_style.labelUser);
    m_pLabelPwd->setGeometry(_style.labelPwd);

    m_pSMTP->setGeometry(_style.editSMTP);
    m_pPort->setGeometry(_style.editPort);
    m_pUser->setGeometry(_style.editUser);
    m_pPass->setGeometry(_style.editPwd);

    this->setStyleSheet(_style.qss);

    m_pOK->setGeometry(_style.yesGeo);
    m_pCancel->setGeometry(_style.noGeo);

    m_pSMTP->setFocus();
    QObject::connect(m_pOK,SIGNAL(clicked()),this,SLOT(OnOKClick()));
    QObject::connect(m_pCancel,SIGNAL(clicked()),this,SLOT(OnCancelClick()));

    connect(m_pSMTP, SIGNAL(clicked()), this, SLOT(OnSmtpClick()) );
    connect(m_pPort, SIGNAL(clicked()), this, SLOT(OnPortClick()) );
    connect(m_pUser, SIGNAL(clicked()), this, SLOT(OnUserClick()) );
    connect(m_pPass, SIGNAL(clicked()), this, SLOT(OnPassClick()) );
}


void CMailSet::OnOKClick()
{
    emit OnOK();
}


void CMailSet::OnCancelClick()
{
    emit OnCancel();
}

void CMailSet::OnSmtpClick()
{
    serviceExecutor::post(serv_name_utility_Email, MSG_EMAIL_SMTP_IP,0);
}

void CMailSet::OnPortClick()
{
    serviceExecutor::post(serv_name_utility_Email, MSG_EMAIL_SMTP_PORT,0);
}

void CMailSet::OnUserClick()
{
    serviceExecutor::post(serv_name_utility_Email, MSG_EMAIL_USERNAME,0);
}

void CMailSet::OnPassClick()
{
    serviceExecutor::post(serv_name_utility_Email, MSG_EMAIL_PASSWORD,0);
}



void CMailSet::OnInputIP(const QString & txt)
{
    m_pSMTP->setText(txt);

    serviceExecutor::post(serv_name_utility_Email,servMail::cmd_mail_stmp, txt);
}

void CMailSet::OnInputPort(DsoReal p)
{
    int val = 0;
    p.toReal(val,0);

    m_pPort->setText( QString("%1").arg(val));
    serviceExecutor::post(serv_name_utility_Email,servMail::cmd_mail_port, val);
}


void CMailSet::OnInputUser(const QString &txt)
{
    m_pUser->setText(txt);

    serviceExecutor::post(serv_name_utility_Email,servMail::cmd_mail_user, txt);
}

void CMailSet::OnInputPass(const QString & txt)
{
    m_pPass->setText(txt);

    serviceExecutor::post(serv_name_utility_Email,servMail::cmd_mail_pass, txt);
}

void CMailSet::keyPressEvent(QKeyEvent *event)
{
    Q_ASSERT( NULL != event);
    if(event->key() == R_Pkey_PageReturn)
    {
        emit sigClose();
        hide();
    }
}

void CMailSet::mousePressEvent(QMouseEvent *event)
{
    qDebug() << event;
}
