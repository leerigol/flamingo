#ifndef CMAIL_STYLE_H
#define CMAIL_STYLE_H

#include "../../menu/menustyle/rui_style.h"

class CMail_Style : public menu_res::RUI_Style
{
public:
    CMail_Style();

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QRect mRectGeo;

    QRect geo, yesGeo, noGeo;
    QRect labelSMTP, labelPort,labelUser, labelPwd;
    QRect editSMTP, editPort, editUser, editPwd;

    QString qss;
};

#endif // CMAIL_STYLE_H
