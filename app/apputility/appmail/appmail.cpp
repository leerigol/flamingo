#include "../../com/virtualkb/virtualkb.h"

#include "../appmain/cappmain.h"
#include "appmail.h"

IMPLEMENT_CMD( CApp, CAppMail )
start_of_entry()

on_set_int_int (  CMD_SERVICE_SUB_ENTER,        &CAppMail::onMailSet),
on_set_int_int (  CMD_SERVICE_SUB_RETURN,       &CAppMail::onReturn),
on_set_int_int (  CMD_SERVICE_EXIT_ACTIVE,      &CAppMail::onReturn),
on_set_int_int (  CMD_SERVICE_DEACTIVE,         &CAppMail::onReturn),

on_set_int_int (  MSG_EMAIL_DEFAULT,            &CAppMail::onMailSet),

on_set_int_int (  MSG_EMAIL_SMTP_IP,            &CAppMail::OnIP),
on_set_int_int (  MSG_EMAIL_SMTP_PORT,          &CAppMail::OnPort),
on_set_int_int (  MSG_EMAIL_USERNAME,           &CAppMail::OnUser),
on_set_int_int (  MSG_EMAIL_PASSWORD,           &CAppMail::OnPass),
end_of_entry()

CAppMail::CAppMail()
{
    mservName  = serv_name_utility_Email;

    m_pMailSet = NULL;       

//    addQuick( MSG_APP_UTILITY_EMAIL_MENU,
//              QStringLiteral("ui/desktop/mail/icon/") );
}

void CAppMail::setupUi( CAppMain */*pMain*/ )
{
}
void CAppMail::retranslateUi()
{}
void CAppMail::buildConnection()
{}

int CAppMail::onMailSet(int)
{
    int nSubMenu = 0;
    query(mservName,CMD_SERVICE_SUB_ENTER,nSubMenu);

    if( nSubMenu )
    {
        if( m_pMailSet == NULL )
        {
            m_pMailSet = new CMailSet();
            Q_ASSERT( m_pMailSet != NULL );
            connect(m_pMailSet, SIGNAL(OnOK()), this, SLOT(OnOK()) );
            connect(m_pMailSet, SIGNAL(OnCancel()), this, SLOT(OnCancel()) );
            connect(m_pMailSet, SIGNAL(sigClose()), this, SLOT(OnClose()) );

            m_pMailSet->setup();
        }

        QString txt;
        query(mservName, servMail::cmd_mail_stmp, txt);
        m_pMailSet->m_pSMTP->setText(txt);

        int port = 0;
        query(mservName, servMail::cmd_mail_port, port);
        m_pMailSet->m_pPort->setText( QString("%1").arg(port) );

        query(mservName, servMail::cmd_mail_user, txt);
        m_pMailSet->m_pUser->setText(txt);

        query(mservName, servMail::cmd_mail_pass, txt);
        m_pMailSet->m_pPass->setText(txt);

        if(work_help != sysGetWorkMode())
            m_pMailSet->show();
    }
    else
    {
        OnClose();
    }
    return ERR_NONE;
}

int CAppMail::onReturn(int)
{
    OnClose();
    return ERR_NONE;
}

void CAppMail::OnCancel()
{
    serviceExecutor::post(serv_name_utility,
                          servUtility::cmd_key_press,
                          r_key_back);
}

///
/// \brief save the config to file
///
void CAppMail::OnOK()
{    
    OnCancel();
    serviceExecutor::post(serv_name_utility_Email, MSG_EMAIL_APPLY,0);
}



void CAppMail::OnClose()
{
    if( m_pMailSet && m_pMailSet->isVisible())
    {
        m_pMailSet->hide();
        delete m_pMailSet;
        m_pMailSet = NULL;        
    }
}


int CAppMail::OnIP(int)
{
    if( m_pMailSet )
    {
        if( m_pMailSet->isVisible())
        {
            //m_pMailSet->m_pSMTP->setFocus();
            QString txt = m_pMailSet->m_pSMTP->text();
            VirtualKB::Show( m_pMailSet,
                              SLOT(OnInputIP(const QString&)),
                              mservName,
                              txt );
            VirtualKB::object()->setMaxLength( 32 );
        }
        else
        {
            onMailSet(0);
        }
    }
    return ERR_NONE;
}



int CAppMail::OnPort(int)
{
    if( m_pMailSet )
    {
        if(m_pMailSet->isVisible())
        {
            DsoReal now;
            DsoReal max;
            DsoReal min;
            DsoReal def;

            DsoRealGp vg;
            def.setVal(25, E_0);
            now.setVal( m_pMailSet->m_pPort->text().toInt(), E_0);
            max.setVal(65535,E_0);
            min.setVal(1, E_0);

            vg.setValue(now, max, min, def);
            InputKeypad* inputMethod = InputKeypad::getInstance(0);
            InputKeypad::rst();
            connect(inputMethod,
                    SIGNAL(OnOK(DsoReal)),
                    m_pMailSet,
                    SLOT(OnInputPort(DsoReal)));

            inputMethod->input(InputKeypad::NumValue, vg);
        }
        else
        {
            onMailSet(0);
        }
    }

    return ERR_NONE;
}

int CAppMail::OnUser(int )
{
    if( m_pMailSet )
    {
        if( m_pMailSet->isVisible())
        {
            QString txt = m_pMailSet->m_pUser->text();
            VirtualKB::Show( m_pMailSet,
                              SLOT(OnInputUser(const QString&)),
                              mservName,
                              txt );
            VirtualKB::object()->setMaxLength( 32 );
        }
        else
        {
            onMailSet(0);
        }
    }
    return ERR_NONE;
}



int CAppMail::OnPass(int)
{
    if( m_pMailSet )
    {
        if( m_pMailSet->isVisible())
        {
            QString txt = m_pMailSet->m_pPass->text();
            VirtualKB::Show( m_pMailSet,
                              SLOT(OnInputPass(const QString&)),
                              mservName,
                              txt );
            VirtualKB::object()->setMaxLength( 32 );
        }
        else
        {
            onMailSet(0);
        }
    }
    return ERR_NONE;
}
