#ifndef CAPP_MAIL_H
#define CAPP_MAIL_H

#include "../../app/capp.h"
#include "cmail_set.h"

class CAppMail : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppMail();
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();
    virtual void buildConnection();

public:
    int   onMailSet(int);
    int   onReturn(int);
    int   OnIP(int);
    int   OnPort(int);
    int   OnUser(int);
    int   OnPass(int);

public slots:
    void  OnOK();
    void  OnCancel();
    void  OnClose();


private:
//    QString addresseestr;
//    QString title;
//    QString attachmentaddr;
//    QString attachmentname;
//    QString sender;
//    QString username;
//    QString pwd;
//    QString smtpaddr;
//    QString filesize;
//    bool writemailWnd;

    CMailSet *m_pMailSet;
//    CmailsendWnd *m_mailSend;
};

#endif // CAppMail_H

