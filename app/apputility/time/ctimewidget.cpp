#include "ctimewidget.h"
#include "ctimeconfig.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"

//#define DIALOGLAYOUT
CTimeWidget::CTimeWidget()
{
    //widget self attributions
    setWindowFlags( Qt::FramelessWindowHint );
    setAttribute( Qt::WA_TranslucentBackground, true );
    setAttribute( Qt::WA_NoSystemBackground, false );
//    setAttribute( Qt::WA_DeleteOnClose );
    setObjectName( "timeDialog" );

    m_slotMachine = new QWidget(this);

    //init widgets
    initTitle();
    initYearWidgets();
    initMonthWidgets();
    initDayWidgets();
    initHourWidgets();
    initMinuteWidgets();
    initSecondWidgets();
    initOtherButton();

    //layout
    setLayoutWidgets();

    //style
    setStyleWidgets();

    m_pCurrentLineEdit = hourLineEdit;

    xOffset = 0;
    yOffset = 0;
    isMove = false;
}


CTimeWidget::~CTimeWidget()
{

}

static CTimeWidget*_instance = NULL;
CTimeWidget* CTimeWidget::getInstance()
{
    if ( _instance == NULL )
    {
        _instance = new CTimeWidget;
    }
    return _instance;
}

void CTimeWidget::initTitle()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    //dateTime.currentDateTime();

    m_qDateTime.setDate( QDate( dateTime.date().year(), dateTime.date().month(), dateTime.date().day() ) );
    m_qDateTime.setTime( QTime( dateTime.time().hour(), dateTime.time().minute(), dateTime.time().second() ) );

    dateTimeLabel = new QLabel( this );
    dateTimeLabel->setObjectName( "dateTimeLabel" );

    dateTimeLabel->setText( QString( QString::number( m_qDateTime.date().year() ) + "-" \
                                            + QString::number( m_qDateTime.date().month() ) + "-"\
                                            + QString::number( m_qDateTime.date().day() ) ) );


}

void CTimeWidget::initYearWidgets()
{
    yearLabel = new QLabel( "Year", this );

    yearUpPushButton = new QPushButton( this );
    yearUpPushButton->setObjectName( "yearUpPushButton" );
    yearUpPushButton->setAutoRepeat( true );
    connect( yearUpPushButton, SIGNAL( clicked() ), this, SLOT( yearUpScroll() ) );

    yearList = new QStringList;
    for ( int year = YEAR_START; year < YEAR_STOP; year++ )
    {
        *yearList << QString::number( year );
    }
    m_yearWheel = new StringWheelWidget();
    m_yearWheel->setParent( this );
    m_yearWheel->setItems( *yearList );
    m_yearWheel->setCurrentIndex( yearList->indexOf( QRegExp( QString::number( m_qDateTime.date().year() )  ) ) );


    yearDownPushButton = new QPushButton( this );
    yearDownPushButton->setObjectName( "yearDownPushButton" );
    yearDownPushButton->setAutoRepeat( true );

    connect( yearDownPushButton, SIGNAL( clicked() ), this, SLOT( yearDownScroll() ) );

    connect( m_yearWheel, SIGNAL( stopped( int ) ), this, SLOT( yearOrMonthChanged( int ) ) );
}

void CTimeWidget::initMonthWidgets()
{
    monthLabel = new QLabel( "Month", this );

    monthUpPushButton = new QPushButton( this );
    monthUpPushButton->setObjectName( "monthUpPushButton" );
    monthUpPushButton->setAutoRepeat( true );
    connect( monthUpPushButton, SIGNAL( clicked() ), this, SLOT( monthUpScroll() ) );

    monthList = new QStringList;
    *monthList << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9" << "10" << "11" << "12";

    m_monthWheel = new StringWheelWidget();
    m_monthWheel->setParent( this );
    m_monthWheel->setItems( *monthList );
    m_monthWheel->setCurrentIndex( monthList->indexOf( QRegExp( QString::number( m_qDateTime.date().month() )  ) ) );

    monthDownPushButton = new QPushButton( this );
    monthDownPushButton->setObjectName( "monthDownPushButton" );
    monthDownPushButton->setAutoRepeat( true );
    connect( monthDownPushButton, SIGNAL( clicked() ), this, SLOT( monthDownScroll() ) );

    connect( m_monthWheel, SIGNAL( stopped( int ) ), this, SLOT( yearOrMonthChanged( int ) ) );
}

void CTimeWidget::initDayWidgets()
{
    dayLabel = new QLabel( "Day", this );

    dayUpPushButton = new QPushButton( this );
    dayUpPushButton->setObjectName( "dayUpPushButton" );
    dayUpPushButton->setAutoRepeat( true );
    connect( dayUpPushButton, SIGNAL( clicked() ), this, SLOT( dayUpScroll() ) );

    dayList = new QStringList;
    *dayList << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9"
            << "10" << "11" << "12" << "13" << "14" << "15" << "16" << "17" << "18" << "19"
            << "20" << "21" << "22" << "23" << "24" << "25" << "26" << "27" << "28" << "29"
            << "30"  << "31";
    m_dayWheel = new StringWheelWidget();
    m_dayWheel->setParent( this );
    m_dayWheel->setItems( *dayList );
    m_dayWheel->setCurrentIndex( dayList->indexOf( QRegExp( QString::number( m_qDateTime.date().day() )  ) ) );

    dayDownPushButton = new QPushButton( this );
    dayDownPushButton->setObjectName( "dayDownPushButton" );
    dayDownPushButton->setAutoRepeat( true );
    connect( dayDownPushButton, SIGNAL( clicked() ), this, SLOT( dayDownScroll() ) );
}

void CTimeWidget::initHourWidgets()
{
    hourLabel = new QLabel( "Hour", this );

    hourUpPushButton = new QPushButton( this );
    hourUpPushButton->setObjectName( "hourUpPushButton" );
    hourUpPushButton->setAutoRepeat( true );
    connect( hourUpPushButton, SIGNAL( clicked() ), this, SLOT( hourUp() ) );

    hourLineEdit = new CTimeLineEdit( this );
    QRegExp regExp24 ( "^(1?[0-9]|20|21|22|23)$" );
    hourLineEdit->setValidator( new QRegExpValidator( regExp24, this ) );
    hourLineEdit->setMaxLength( 2 );//name length needs to be set
    int u32Time;
    u32Time = m_qDateTime.time().hour();
    if ( u32Time < 10 )
    {
        hourLineEdit->setText( QString( "0" ) + QString::number( u32Time ) );
    }
    else
    {
        hourLineEdit->setText( QString::number( u32Time ) );
    }

    hourDownPushButton = new QPushButton( this );
    hourDownPushButton->setObjectName( "hourDownPushButton" );
    hourDownPushButton->setAutoRepeat( true );
    connect( hourDownPushButton, SIGNAL( clicked() ), this, SLOT( hourDown() ) );
}


void CTimeWidget::initMinuteWidgets()
{
    minuteLabel = new QLabel( "Minute", this );

    minuteUpPushButton = new QPushButton( this );
    minuteUpPushButton->setObjectName( "minuteUpPushButton" );
    minuteUpPushButton->setAutoRepeat( true );
    connect( minuteUpPushButton, SIGNAL( clicked() ), this, SLOT( minuteUp() ) );

    minuteLineEdit = new CTimeLineEdit( this );
    QRegExp regExp60 ( "^([1-5]?[0-9])$" );
    minuteLineEdit->setValidator( new QRegExpValidator( regExp60, this ) );
    minuteLineEdit->setMaxLength( 2 );//name length needs to be set
    int u32Time;
    u32Time = m_qDateTime.time().minute();
    if ( u32Time < 10 )
    {
        minuteLineEdit->setText( QString( "0" ) + QString::number( u32Time  ) );
    }
    else
    {
        minuteLineEdit->setText( QString::number( u32Time  ) );
    }

    minuteDownPushButton = new QPushButton( this );
    minuteDownPushButton->setObjectName( "minuteDownPushButton" );
    minuteDownPushButton->setAutoRepeat( true );
    connect( minuteDownPushButton, SIGNAL( clicked() ), this, SLOT( minuteDown() ) );
}

void CTimeWidget::initSecondWidgets()
{
    secondLabel = new QLabel( "Second", this );

    secondUpPushButton = new QPushButton( this );
    secondUpPushButton->setObjectName( "secondUpPushButton" );
    secondUpPushButton->setAutoRepeat( true );
    connect( secondUpPushButton, SIGNAL( clicked() ), this, SLOT( secondUp() ) );

    secondLineEdit = new CTimeLineEdit( this );
    QRegExp regExp60 ( "^([0][0-9]|[1-5]?[0-9])$" );
    secondLineEdit->setValidator( new QRegExpValidator( regExp60, this ) );
    secondLineEdit->setMaxLength( 2 );//name length needs to be set
    int u32Time;
    u32Time = m_qDateTime.time().second();
    if ( u32Time < 10 )
    {
        secondLineEdit->setText( QString( "0" ) + QString::number( u32Time ) );
    }
    else
    {
        secondLineEdit->setText( QString::number( u32Time ) );
    }

    secondDownPushButton = new QPushButton( this );
    secondDownPushButton->setObjectName( "secondDownPushButton" );
    secondDownPushButton->setAutoRepeat( true );
    connect( secondDownPushButton, SIGNAL( clicked() ), this, SLOT( secondDown() ) );
}

void CTimeWidget::initOtherButton()
{
    lineLabel = new QLabel( this );
    lineLabel->setObjectName( "lineLabel" );

    firstSepLabel = new QLabel( ":", this );
    firstSepLabel->setObjectName( "firstSepLabel" );

    secondSepLabel = new QLabel( ":", this );
    secondSepLabel->setObjectName( "secondSepLabel" );


    okPushButton = new QPushButton( tr( "OK" ), this );
    okPushButton->setObjectName( "okPushButton" );
    //okPushButton->setIconSize( QSize( 300, 300 ) );
    connect(okPushButton, SIGNAL(clicked()), this, SLOT(okButtonClicked()));

    cancelPushButton = new QPushButton( tr( "Cancel" ), this );
    cancelPushButton->setObjectName( "cancelPushButton" );
    //cancelPushButton->setIconSize( QSize( 300, 300 ) );
    connect(cancelPushButton, SIGNAL(clicked()), this, SLOT(cancelButtonClicked()));

}


void CTimeWidget::setLayoutWidgets()
{
    dateTimeLabel->move( 90, 10 );
    yearLabel->move( 54, 56 );
    yearUpPushButton->move( 59 - 15, 83 - 20 );
    m_yearWheel->move( 42, 98 );
    yearDownPushButton->move( 59 - 15, 180 - 8 );
    monthLabel->move( 118, 56 );
    monthUpPushButton->move( 126 - 15, 83 - 20 );
    m_monthWheel->move( 109, 98 );
    monthDownPushButton->move( 126 - 15, 180 - 8 );
    dayLabel->move( 191, 56 );
    dayUpPushButton->move( 192 - 15, 83 - 20 );
    m_dayWheel->move( 175, 98 );
    dayDownPushButton->move( 192 - 15, 180 - 8 );
    lineLabel->move( 40, 203 );
    hourLabel->move( 61 - 5, 216 );
    hourUpPushButton->move( 61 - 15, 242 - 20 );
    hourLineEdit->move( 52, 256 );
    hourDownPushButton->move( 61 - 15, 299 - 8 );
    minuteLabel->move( 124 - 8, 216 );
    minuteUpPushButton->move( 125 - 15, 242 - 20 );
    minuteLineEdit->move( 116, 256 );
    minuteDownPushButton->move( 125 - 15, 299 - 8 );
    secondLabel->move( 188 - 8, 216 );
    secondUpPushButton->move( 189 - 15, 242 - 20 );
    secondLineEdit->move( 180, 256 );
    secondDownPushButton->move( 189 - 15, 299 - 8 );
    firstSepLabel->move( 90 + 8, 262 + 1 );
    secondSepLabel->move( 155 + 8, 262 + 1 );
    okPushButton->move( 51, 321 );
    cancelPushButton->move( 149, 321 );
}

void CTimeWidget::rotateRandom()
{
    m_yearWheel->scrollTo(m_yearWheel->currentIndex() + (qrand() % 200));
    m_monthWheel->scrollTo(m_monthWheel->currentIndex() + (qrand() % 200));
    m_dayWheel->scrollTo(m_dayWheel->currentIndex() + (qrand() % 200));
}
//year
void CTimeWidget::yearUpScroll()
{
    m_yearWheel->scrollTo( m_yearWheel->currentIndex() - 1, 100 );
}
void CTimeWidget::yearDownScroll()
{
    m_yearWheel->scrollTo( m_yearWheel->currentIndex() + 1, 100 );
}
//month
void CTimeWidget::monthUpScroll()
{
    m_monthWheel->scrollTo( m_monthWheel->currentIndex() - 1, 100 );
}
void CTimeWidget::monthDownScroll()
{
    m_monthWheel->scrollTo( m_monthWheel->currentIndex() + 1, 100 );
}
//day
void CTimeWidget::dayUpScroll()
{
    m_dayWheel->scrollTo( m_dayWheel->currentIndex() - 1, 100 );
}
void CTimeWidget::dayDownScroll()
{
    m_dayWheel->scrollTo( m_dayWheel->currentIndex() + 1, 100 );
}

//hour
void CTimeWidget::hourUp()
{
    int hourValue = hourLineEdit->text().toInt();
    hourValue++;
    hourValue = hourValue > 23 ? ( hourValue % 24 ) : hourValue;
    hourLineEdit->setText( QString::number( hourValue ) );
    if ( hourValue < 10 )
    {
        hourLineEdit->setText( QString( "0" ) + QString::number( hourValue ) );
    }
    else
    {
        hourLineEdit->setText( QString::number( hourValue ) );
    }
}
void CTimeWidget::hourDown()
{
    int hourValue = hourLineEdit->text().toInt();
    hourValue--;
    hourValue = hourValue < 0 ? ( hourValue + 24 ) : hourValue;
    hourLineEdit->setText( QString::number( hourValue ) );
    if ( hourValue < 10 )
    {
        hourLineEdit->setText( QString( "0" ) + QString::number( hourValue ) );
    }
    else
    {
        hourLineEdit->setText( QString::number( hourValue ) );
    }
}

//minute
void CTimeWidget::minuteUp()
{
    int minuteValue = minuteLineEdit->text().toInt();
    minuteValue++;
    minuteValue = minuteValue > 59 ? ( minuteValue % 60 ) : minuteValue;
    minuteLineEdit->setText( QString::number( minuteValue ) );
    if ( minuteValue < 10 )
    {
        minuteLineEdit->setText( QString( "0" ) + QString::number( minuteValue ) );
    }
    else
    {
        minuteLineEdit->setText( QString::number( minuteValue ) );
    }
}
void CTimeWidget::minuteDown()
{
    int minuteValue = minuteLineEdit->text().toInt();
    minuteValue--;
    minuteValue = minuteValue < 0 ? ( minuteValue + 60 ) : minuteValue;
    minuteLineEdit->setText( QString::number( minuteValue ) );
    if ( minuteValue < 10 )
    {
        minuteLineEdit->setText( QString( "0" ) + QString::number( minuteValue ) );
    }
    else
    {
        minuteLineEdit->setText( QString::number( minuteValue ) );
    }
}
//second
void CTimeWidget::secondUp()
{
    int secondValue = secondLineEdit->text().toInt();
    secondValue++;
    secondValue = secondValue > 59 ? ( secondValue % 60 ) : secondValue;
    secondLineEdit->setText( QString::number( secondValue ) );
    if ( secondValue < 10 )
    {
        secondLineEdit->setText( QString( "0" ) + QString::number( secondValue ) );
    }
    else
    {
        secondLineEdit->setText( QString::number( secondValue ) );
    }
}
void CTimeWidget::secondDown()
{
    int secondValue = secondLineEdit->text().toInt();
    secondValue--;
    secondValue = secondValue < 0 ? ( secondValue + 60 ) : secondValue;
    secondLineEdit->setText( QString::number( secondValue ) );
    if ( secondValue < 10 )
    {
        secondLineEdit->setText( QString( "0" ) + QString::number( secondValue ) );
    }
    else
    {
        secondLineEdit->setText( QString::number( secondValue ) );
    }
}

//ok button
void CTimeWidget::okButtonClicked()
{
    m_qDateTime.setDate( QDate( m_yearWheel->getCurrentItem().toInt(),
                                m_monthWheel->getCurrentItem().toInt(),
                                m_dayWheel->getCurrentItem().toInt()
                                ) );
    m_qDateTime.setTime( QTime( hourLineEdit->text().toInt(),
                                minuteLineEdit->text().toInt(),
                                secondLineEdit->text().toInt()
                                ) );

    serviceExecutor::post(serv_name_utility, MSG_APP_UTILITY_YEAR,
                          (int)m_yearWheel->getCurrentItem().toInt());
    serviceExecutor::post(serv_name_utility, MSG_APP_UTILITY_MONTH,
                          (int)m_monthWheel->getCurrentItem().toInt());
    serviceExecutor::post(serv_name_utility, MSG_APP_UTILITY_DAY,
                          (int)m_dayWheel->getCurrentItem().toInt());
    serviceExecutor::post(serv_name_utility, MSG_APP_UTILITY_HOUR,
                          (int)hourLineEdit->text().toInt());
    serviceExecutor::post(serv_name_utility, MSG_APP_UTILITY_MINUTE,
                          (int)minuteLineEdit->text().toInt());
    serviceExecutor::post(serv_name_utility, MSG_APP_UTILITY_SECOND,
                          (int)secondLineEdit->text().toInt());
    this->close();
}

//cancel button
void CTimeWidget::cancelButtonClicked()
{
    this->hide();
    emit closeTimeSig();
}

//void CTimeWidget::currentTimeUpdate()
//{
//    QString currentTimeString = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh::mm::ss" );
//    dateTimeLabel->setText( currentTimeString );
//}
void CTimeWidget::setStyleWidgets()
{
    QFile qssFile( ":/pictures/time/timedialog.qss" );
    qssFile.open( QFile::ReadOnly );
    if( qssFile.isOpen() )
    {
        setStyleSheet( qssFile.readAll() );
        qssFile.close();
    }
}

//void	CTimeWidget::paintEvent(QPaintEvent * event)
//{
//    UN_USE()
//    QStyleOption opt;
//    opt.init( this );
//    QPainter p( this );
//    style()->drawPrimitive( QStyle::PE_Widget, &opt, &p, this );
//}

bool CTimeWidget::isLeapYear( int year )
{
    if ( year % 4 == 0 && year % 100 != 0 )
    {
        return true;
    }
    else if ( year % 400 == 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}

int  CTimeWidget::monthHasDays( int year, int month )
{
    if ( year > YEAR_STOP || year < YEAR_START  || month < 1 || month > 12 )
    {
        return -1;
    }

    switch ( month )
    {
    case 2:
        return isLeapYear( year ) ? 29 : 28;
    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
    default:
        return 31;
    }
}

void CTimeWidget::changeDayList( int year, int month )
{
    switch ( monthHasDays( year, month ) )
    {
    case 28:
        if ( dayList->size() != 28 )
        {
            dayList->clear();
            *dayList << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9"
                    << "10" << "11" << "12" << "13" << "14" << "15" << "16" << "17" << "18" << "19"
                    << "20" << "21" << "22" << "23" << "24" << "25" << "26" << "27" << "28";
            m_dayWheel->setItems( *dayList );
            m_dayWheel->setCurrentIndex( dayList->indexOf( QRegExp( QString::number( m_qDateTime.date().day() )  ) ) );
        }
        break;
    case 29:
        if ( dayList->size() != 29 )
        {
            dayList->clear();
            *dayList << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9"
                    << "10" << "11" << "12" << "13" << "14" << "15" << "16" << "17" << "18" << "19"
                    << "20" << "21" << "22" << "23" << "24" << "25" << "26" << "27" << "28" << "29";
            m_dayWheel->setItems( *dayList );
            m_dayWheel->setCurrentIndex( dayList->indexOf( QRegExp( QString::number( m_qDateTime.date().day() )  ) ) );

        }
        break;
    case 30:
        if ( dayList->size() != 30 )
        {
            dayList->clear();
            *dayList << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9"
                    << "10" << "11" << "12" << "13" << "14" << "15" << "16" << "17" << "18" << "19"
                    << "20" << "21" << "22" << "23" << "24" << "25" << "26" << "27" << "28" << "29"
                    << "30";
            m_dayWheel->setItems( *dayList );
            m_dayWheel->setCurrentIndex( dayList->indexOf( QRegExp( QString::number( m_qDateTime.date().day() )  ) ) );

        }
        break;
    case 31:
        if ( dayList->size() != 31 )
        {
            dayList->clear();
            *dayList << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9"
                    << "10" << "11" << "12" << "13" << "14" << "15" << "16" << "17" << "18" << "19"
                    << "20" << "21" << "22" << "23" << "24" << "25" << "26" << "27" << "28" << "29"
                    << "30" << "31";
            m_dayWheel->setItems( *dayList );
            m_dayWheel->setCurrentIndex( dayList->indexOf( QRegExp( QString::number( m_qDateTime.date().day() )  ) ) );

        }
        break;
    default:
        break;
    }
}

void CTimeWidget::yearOrMonthChanged( int )
{
    int year, month;
    year = m_yearWheel->getCurrentItem().toInt();
    month = m_monthWheel->getCurrentItem().toInt();
    changeDayList( year, month );
}

void CTimeWidget::mousePressEvent(QMouseEvent *event)
{
    QRect rect = geometry();
    rect.setX(0);
    rect.setY(0);
    if(rect.contains(event->pos())){
        isMove = true;
        this->xOffset = event->globalPos().rx() - this->pos().rx();
        this->yOffset = event->globalPos().ry() - this->pos().ry();
    }else
        isMove = false;

//    return QDialog::mousePressEvent(event);
}

void CTimeWidget::mouseMoveEvent(QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton && isMove ) {

        int toX = event->globalX()-xOffset;
        int toY = event->globalY()-yOffset;

        if( event->globalX()-xOffset < 0 )
            toX = 0;
        else if(event->globalX()-xOffset > 800)
            toX = 800;

        if( event->globalY()-yOffset < 0 )
            toY = 0;
        else if(event->globalY()-yOffset > 300)
            toY = 300;
        move(toX, toY);
    }
}
void CTimeWidget::mouseReleaseEvent(QMouseEvent *)
{
    isMove = false;
    xOffset = 0;
    yOffset = 0;
}

void CTimeWidget::keyPressEvent( QKeyEvent * )
{
//    switch ( e->key() )
//    {
//    case Qt::Key_Left:
//        selectLeftLineEdit();
//        break;
//    case Qt::Key_Right:
//        selectRightLineEdit();
//        break;
//    default:
//        changeCurrentLineEdit();
//        m_pCurrentLineEdit->keyPressEvent( e );
//        break;
//    }
}

void CTimeWidget::selectLeftLineEdit()
{

    if ( m_pCurrentLineEdit == secondLineEdit )
    {
        if ( m_pCurrentLineEdit->cursorPosition() == 0 )
        {
            m_pCurrentLineEdit = minuteLineEdit;
            m_pCurrentLineEdit->setFocus();
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->text().size() );
        }
        else
        {
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->cursorPosition() - 1 );
        }
    }
    else if ( m_pCurrentLineEdit == minuteLineEdit )
    {
        if ( m_pCurrentLineEdit->cursorPosition() == 0 )
        {
            m_pCurrentLineEdit = hourLineEdit;
            m_pCurrentLineEdit->setFocus();
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->text().size() );
        }
        else
        {
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->cursorPosition() - 1 );
        }
    }
    else if ( m_pCurrentLineEdit == hourLineEdit )
    {
        if ( m_pCurrentLineEdit->cursorPosition() == 0 )
        {
            m_pCurrentLineEdit = secondLineEdit;
            m_pCurrentLineEdit->setFocus();
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->text().size() );
        }
        else
        {
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->cursorPosition() - 1 );
        }
    }
}

void CTimeWidget::selectRightLineEdit()
{
    if ( m_pCurrentLineEdit == hourLineEdit )
    {
        if ( m_pCurrentLineEdit->cursorPosition() == m_pCurrentLineEdit->text().size() )
        {
            m_pCurrentLineEdit = minuteLineEdit;
            m_pCurrentLineEdit->setFocus();
            m_pCurrentLineEdit->setCursorPosition( 0 );
        }
        else
        {
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->cursorPosition() + 1 );
        }
    }
    else if ( m_pCurrentLineEdit == minuteLineEdit )
    {
        if ( m_pCurrentLineEdit->cursorPosition() == m_pCurrentLineEdit->text().size() )
        {
            m_pCurrentLineEdit = secondLineEdit;
            m_pCurrentLineEdit->setFocus();
            m_pCurrentLineEdit->setCursorPosition( 0 );
        }
        else
        {
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->cursorPosition() + 1 );
        }
    }
    else if ( m_pCurrentLineEdit == secondLineEdit )
    {
        if ( m_pCurrentLineEdit->cursorPosition() == m_pCurrentLineEdit->text().size() )
        {
            m_pCurrentLineEdit = hourLineEdit;
            m_pCurrentLineEdit->setFocus();
            m_pCurrentLineEdit->setCursorPosition( 0 );
        }
        else
        {
            m_pCurrentLineEdit->setCursorPosition( m_pCurrentLineEdit->cursorPosition() + 1 );
        }
    }
}

void CTimeWidget::changeCurrentLineEdit()
{
    if ( hourLineEdit->hasFocus() )
    {
        m_pCurrentLineEdit = hourLineEdit;
    }
    else if ( minuteLineEdit->hasFocus() )
    {
        m_pCurrentLineEdit = minuteLineEdit;
    }
    else if ( secondLineEdit->hasFocus() )
    {
        m_pCurrentLineEdit = secondLineEdit;
    }
}

void CTimeWidget::refresh()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    //dateTime.currentDateTime();

    m_qDateTime.setDate( QDate( dateTime.date().year(), dateTime.date().month(), dateTime.date().day() ) );
    m_qDateTime.setTime( QTime( dateTime.time().hour(), dateTime.time().minute(), dateTime.time().second() ) );

    dateTimeLabel->setText( QString( QString::number( m_qDateTime.date().year() ) + "-" \
                                            + QString::number( m_qDateTime.date().month() ) + "-"\
                                            + QString::number( m_qDateTime.date().day() ) ) );

    m_yearWheel->setCurrentIndex( yearList->indexOf( QRegExp( QString::number( m_qDateTime.date().year() )  ) ) );
    m_monthWheel->setCurrentIndex( monthList->indexOf( QRegExp( QString::number( m_qDateTime.date().month() )  ) ) );
    changeDayList( dateTime.date().year(), dateTime.date().month() );
    m_dayWheel->setCurrentIndex( dayList->indexOf( QRegExp( QString::number( m_qDateTime.date().day() )  ) ) );

    int u32Time = m_qDateTime.time().hour();
    if ( u32Time < 10 )
    {
        hourLineEdit->setText( QString( "0" ) + QString::number( u32Time ) );
    }
    else
    {
        hourLineEdit->setText( QString::number( u32Time ) );
    }

    u32Time = m_qDateTime.time().minute();
    if ( u32Time < 10 )
    {
        minuteLineEdit->setText( QString( "0" ) + QString::number( u32Time  ) );
    }
    else
    {
        minuteLineEdit->setText( QString::number( u32Time  ) );
    }

    u32Time = m_qDateTime.time().second();
    if ( u32Time < 10 )
    {
        secondLineEdit->setText( QString( "0" ) + QString::number( u32Time ) );
    }
    else
    {
        secondLineEdit->setText( QString::number( u32Time ) );
    }
}

void CTimeWidget::showEvent( QShowEvent *e )
{
    refresh();
    QDialog::showEvent(e);
}

