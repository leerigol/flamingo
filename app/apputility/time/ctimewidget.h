#ifndef CTIMEWIDGET_H
#define CTIMEWIDGET_H

#include <QtWidgets>
#include <qmath.h>
#include "cscrollerwheelwidget.h"
#include "ctimelineedit.h"
#include "../../menu/wnd/rinfownd.h"

class CTimeWidget : public QDialog
{
    Q_OBJECT
public:
    CTimeWidget();
    ~CTimeWidget();
    static CTimeWidget*getInstance();

    void refresh();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
    void keyPressEvent( QKeyEvent *event );

    void showEvent( QShowEvent *e );

private:
    void initTitle();
    void initYearWidgets();
    void initMonthWidgets();
    void initDayWidgets();
    void initHourWidgets();
    void initMinuteWidgets();
    void initSecondWidgets();
    void initOtherButton();

    void setLayoutWidgets();
    void setStyleWidgets();

    int  monthHasDays( int year, int month );
    bool isLeapYear( int year );

    void selectLeftLineEdit();
    void selectRightLineEdit();
    void changeCurrentLineEdit();

private slots:
    void rotateRandom();

    void yearUpScroll();
    void yearDownScroll();
    void monthUpScroll();
    void monthDownScroll();
    void dayUpScroll();
    void dayDownScroll();
    void hourUp();
    void hourDown();
    void minuteUp();
    void minuteDown();
    void secondUp();
    void secondDown();
    void okButtonClicked();
    void cancelButtonClicked();

    void changeDayList( int year, int month );
    void yearOrMonthChanged( int );

    //void currentTimeUpdate();
protected:
    //virtual void	paintEvent(QPaintEvent * event);
signals:
    void closeTimeSig();

private:
    QDateTime m_qDateTime;
    QLabel *dateTimeLabel;
    //year
    QLabel *yearLabel;
    QPushButton *yearUpPushButton;
    StringWheelWidget *m_yearWheel;
    QStringList *yearList;
    QPushButton *yearDownPushButton;

    //month
    QLabel *monthLabel;
    QPushButton *monthUpPushButton;
    StringWheelWidget *m_monthWheel;
    QStringList *monthList;
    QPushButton *monthDownPushButton;

    //day
    QLabel *dayLabel;
    QPushButton *dayUpPushButton;
    StringWheelWidget *m_dayWheel;
    QStringList *dayList;
    QPushButton *dayDownPushButton;

    //line
    QLabel *lineLabel;

    //hour
    QLabel *hourLabel;
    QPushButton *hourUpPushButton;
    CTimeLineEdit *hourLineEdit;
    QPushButton *hourDownPushButton;

    //minute
    QLabel *minuteLabel;
    QPushButton *minuteUpPushButton;
    CTimeLineEdit *minuteLineEdit;
    QPushButton *minuteDownPushButton;

    //second
    QLabel *secondLabel;
    QPushButton *secondUpPushButton;
    CTimeLineEdit *secondLineEdit;
    QPushButton *secondDownPushButton;

    //:
    QLabel *firstSepLabel;
    QLabel *secondSepLabel;

    QWidget *m_slotMachine;


    QPushButton *okPushButton;
    QPushButton *cancelPushButton;

    bool m_bDragMousePressed;
    QPoint m_qDragMousePoint;

    CTimeLineEdit *m_pCurrentLineEdit;

    int	xOffset;
    int	yOffset;
    bool isMove;
};



#endif //CTIMEWIDGET_H
