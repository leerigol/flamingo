#ifndef CCLICKEDLABEL_H
#define CCLICKEDLABEL_H

#include <QLabel>

class CClickedLabel : public QLabel
{
    Q_OBJECT
public:
    explicit CClickedLabel(QWidget *parent=0, Qt::WindowFlags f=0);
    explicit CClickedLabel(const QString &text, QWidget *parent=0, Qt::WindowFlags f=0);

    bool mouseIsIn( int x, int y );

protected:
    virtual void mouseReleaseEvent(QMouseEvent * ev);

signals:
     void clicked();
};

#endif // CCLICKEDLABEL_H
