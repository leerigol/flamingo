#include <QtWidgets>
#include "cscrollerwheelwidget.h"


#define WHEEL_SCROLL_OFFSET 50000.0

AbstractWheelWidget::AbstractWheelWidget(QWidget *parent)
    : QWidget(parent)
{
    QScroller::grabGesture(this, QScroller::TouchGesture );
    QScroller::grabGesture(this, QScroller::LeftMouseButtonGesture);
}

AbstractWheelWidget::~AbstractWheelWidget()
{ }

int AbstractWheelWidget::currentIndex() const
{
    return m_currentItem;
}


void AbstractWheelWidget::setCurrentIndex(int index)
{
    if (index >= 0 && index < itemCount()) {
        m_currentItem = index;
        m_itemOffset = 0;
        update();
    }
}

bool AbstractWheelWidget::event(QEvent *e)
{
    switch (e->type())
    {
    case QEvent::ScrollPrepare:
    {
        QScroller *scroller = QScroller::scroller(this);
        scroller->setSnapPositionsY( WHEEL_SCROLL_OFFSET, itemHeight() );

        QScrollPrepareEvent *se = static_cast<QScrollPrepareEvent *>(e);
        se->setViewportSize(QSizeF(size()));

        se->setContentPosRange(QRectF(0.0, 0.0, 0.0, WHEEL_SCROLL_OFFSET * 2));
        se->setContentPos(QPointF(0.0, WHEEL_SCROLL_OFFSET + m_currentItem * itemHeight() + m_itemOffset));
        se->accept();
        return true;
    }

    case QEvent::Scroll:
    {

        QScrollEvent *se = static_cast<QScrollEvent *>(e);

        qreal y = se->contentPos().y();
        int iy = y - WHEEL_SCROLL_OFFSET;
        int ih = itemHeight();

        int ic = itemCount();
        if (ic>0)
        {
            m_currentItem = iy / ih % ic;
            m_itemOffset = iy % ih;
            if (m_itemOffset < 0)
            {
                m_itemOffset += ih;
                m_currentItem--;
            }

            if (m_currentItem < 0)
            {
                m_currentItem += ic;
            }
            emit stopped( m_currentItem );
        }
        update();

        se->accept();
        return true;

    }
    default:
        return QWidget::event(e);
    }
    return true;
}

void AbstractWheelWidget::paintEvent(QPaintEvent* event)
{
    Q_UNUSED( event );

    QStyleOption opt;
    opt.init( this );
    QPainter p( this );
    style()->drawPrimitive( QStyle::PE_Widget, &opt, &p, this );

    int w = width();
    int h = height();

    QPainter painter(this);

    int iH = itemHeight();
    int iC = itemCount();

    painter.fillRect( QRect( 2, h / 2 - iH / 2, w - 5, iH ), QBrush( QColor( 51, 51, 51 ) ) );

    if (iC > 0)
    {
        m_itemOffset = m_itemOffset % iH;

        for (int i=-h/2/iH; i<=h/2/iH+1; i++) {

            int itemNum = m_currentItem + i;
            while (itemNum < 0)
                itemNum += iC;
            while (itemNum >= iC)
                itemNum -= iC;

            if ( m_currentItem == itemNum )
            {
                painter.setPen( QColor( 255, 255, 255 ) );
                QFont font = painter.font();
                font.setPixelSize( 16 );
                painter.setFont( font );
                paintItem( &painter, m_currentItem, QRect(6, h/2 +i*iH - m_itemOffset - iH/2, w-6, iH ) );
            }
            else
            {
                painter.setPen( QColor( 149, 149, 149 ) );
                QFont font = painter.font();
                font.setPixelSize( 10 );
                painter.setFont( font );
                paintItem(&painter, itemNum, QRect(6, h/2 +i*iH - m_itemOffset - iH/2, w-6, iH ));
            }
        }
    }
}

void AbstractWheelWidget::scrollTo(int index)
{
    QScroller *scroller = QScroller::scroller(this);

    scroller->scrollTo(QPointF(0, WHEEL_SCROLL_OFFSET + index * itemHeight()), 5000 );
}

void AbstractWheelWidget::scrollTo(int index, int mSec)
{
    QScroller *scroller = QScroller::scroller(this);

    scroller->scrollTo(QPointF(0, WHEEL_SCROLL_OFFSET + index * itemHeight()), mSec );
}



StringWheelWidget::StringWheelWidget()
    : AbstractWheelWidget()
{ }

QStringList StringWheelWidget::items() const
{
    return m_items;
}

void StringWheelWidget::setItems( const QStringList &items )
{
    m_items = items;
    if (m_currentItem >= items.count())
        m_currentItem = items.count()-1;
    update();
}


QSize StringWheelWidget::sizeHint() const
{

    QFontMetrics fm(font());

    return QSize( fm.width("m") * 10 + 6, fm.height() * 7 + 6 );
}

QSize StringWheelWidget::minimumSizeHint() const
{
    QFontMetrics fm(font());

    return QSize( fm.width("m") * 5 + 6, fm.height() * 3 + 6 );
}

void StringWheelWidget::paintItem(QPainter* painter, int index, const QRect &rect)
{
    painter->drawText(rect, Qt::AlignCenter, m_items.at(index));
}

int StringWheelWidget::itemHeight() const
{
    return 25;
}

int StringWheelWidget::itemCount() const
{
    return m_items.count();
}


QString StringWheelWidget::getCurrentItem()
{
    return m_items.at( m_currentItem );
}
