#ifndef CTIMELINEEDIT_H
#define CTIMELINEEDIT_H

#include <QLineEdit>

class CTimeLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    CTimeLineEdit( QWidget *parent = 0 );
    virtual void keyPressEvent( QKeyEvent *e );
};

#endif // CTIMELINEEDIT_H
