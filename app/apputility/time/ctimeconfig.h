#ifndef CTIMECONFIG
#define CTIMECONFIG

#include <QString>
#include <QSet>

//year
const int YEAR_START = 1970;
const int YEAR_STOP = 2090;
//month

    //months have 28 days
    const int MONTH_28DAYS[] = { 2 };
    //months have 30 days
    const int MONTH_30DAYS[] = { 4, 6, 9, 11 };
    //months have 31 days
    const int MONTH_31DAYS[] = { 1, 3, 5, 7, 8, 10, 12 };

//day
//hour
const QString HOUR_REG( "^(1?[0-9]|20|21|22|23)$" );

//minute
const QString MINUTE_REG( "^([1-5]?[0-9])$" );

//second
const QString SECOND_REG( "^([0][0-9]|[1-5]?[0-9])$" );

#endif // CTIMECONFIG

