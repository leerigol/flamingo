#include "cclickedlabel.h"


CClickedLabel::CClickedLabel(QWidget *parent, Qt::WindowFlags f):
    QLabel(parent,f)
{
    setAttribute( Qt::WA_Hover, true );
}

CClickedLabel::CClickedLabel(const QString &text, QWidget *parent, Qt::WindowFlags f):
    QLabel(text,parent,f)
{

}

void CClickedLabel::mouseReleaseEvent(QMouseEvent * ev)
{

    emit clicked();
    QLabel::mouseReleaseEvent(ev);
}

bool CClickedLabel::mouseIsIn( int x, int y )
{
    int xl = this->pos().x();
    int yl = this->pos().y();
    if ( x > xl && x < xl + width() && y > yl && y < yl + height() )
    {
        return true;
    }
    return false;
}



