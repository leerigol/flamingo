#ifndef APP_SHUTDOWN_H
#define APP_SHUTDOWN_H

#include "../../menu/wnd/rpopmenuchildwnd.h"
#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;
class Shutdown : public menu_res::RPopMenuChildWnd
{
    Q_OBJECT

public:
    Shutdown( QWidget *parent = 0);
    ~Shutdown();
    void loadResource(QString path);
    virtual void paintEvent( QPaintEvent *event );

    static menu_res::RModelWnd_Style _bgStyle;

    void setupUI();

public slots:
    void onShutdown();
    void onRestart();

private:
    RImageButton    *m_pCloseButton;

    RImageButton    *pShutdownBtn;
    RImageButton    *pRestartBtn;

    QString shutdownPic;
    QString restartPic;

    QString titleStr;
    QString shutdownStr;
    QString restartStr;

    QRect   closeRect;
    QRect   titleRect;
    QRect   shutdownR;
    QRect   restartR;

    QRect   shutdownStrR;
    QRect   restartStrR;


};

#endif // APP_SHUTDOWN_H
