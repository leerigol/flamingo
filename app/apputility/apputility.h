#ifndef APPUTILITY_H
#define APPUTILITY_H

#include "../appmain/cappmain.h"

#include "./sysinfo.h"
#include "./scrsaver.h"
#include "./shutdown.h"

#include "./selfcheck/checkfan.h"
#include "./selfcheck/ckeyform.h"
#include "./selfcheck/checklcd.h"
#include "./selfcheck/checktouch.h"
#include "./selfcheck/checkboard.h"
#include "./time/ctimewidget.h"
#include "../../widget/rimagebutton.h"
#include "../../meta/crmeta.h"

#include "../../menu/dsownd/wndloginfo.h"
#include "./keyRecord/keyrecord.h"
class CAppUtility : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    enum cmdUtility
    {
        cmd_base = CMD_APP_BASE,
        cmd_lan_changed,
        cmd_usb_insert,
        cmd_usb_remove,

        cmd_log_info,
        cmd_lxi_connect,
        cmd_lxi_disconnect,
        cmd_menu_onoff,
    };

public:
    CAppUtility();
    virtual void setupUi(CAppMain *pMain);
    virtual void retranslateUi();
    virtual void buildConnection();
    virtual void registerSpy();
    void loadResource(QString path);

private:
    int onShowInfo(int);
    int queryInfo(int);

    int ShowScr(int);
    int onScrStart(int);
    int onScrPreview(int);

private:
    int onKbCheck(int);
    int onLcdCheck(int);
    int onTouchCheck(int);
    int onFanCheck(int);

    //!internel check
    int onBoardCheck(int);
    int onBoardTestResult(int);

    int onDeActive(int);
    int onSetTime(bool v);
    int onEDID(int);

    void on_beeper();

    //! log info
    void on_log_info();

    int  on_shutdown();

    DsoErr onRecord(AppEventCause cause);

    int onMenuOnOff();

    int onZoneTouchOnOff();

private slots:
    int  onTimeShow();
    void onTimeShowClicked();
    void onTimeIsShow();
    void onTimeClose();
    void onTimeShowtouched();

    void onLanBtn();

    void onRemindBtn();
    void on_loginfo_clear();

    void onUsbInsert();
    void onUsbRemove();
    void onVoiceBtn(bool);

private:
    CSysInfoDlg     *m_pSysInfo;
    Shutdown        *m_pShutdown;

    CKeyForm        *m_pKeyTestDlg;
    CheckLcdDlg     *m_pLcdTestDlg;
    CheckTouchDlg   *m_pTouchTestDlg;
    CheckFanDlg     *m_pFanTestDlg;

    CScrSaverDlg    *m_pScrSaver;
    CheckBoardDlg   *m_pBoardTestDlg;
    CTimeWidget     *timeDlg;


    int             mLxiCount;

    bool            isTimeShow;
    QTimer          *pScrTimer;
    QTimer          *timer;
    QPushButton     *timeShowBtn;

    QPushButton     *iconTest;
    RImageButton    *lanBtn;
    RImageButton    *remindBtn;
    RImageButton    *usbBtn;
    int              usbCount;
    RImageButton    *voiceBtn;

    menu_res::WndLogInfo *m_pLogInfo;
    keyRecord            *m_pKeyRecord;

    QHBoxLayout     *hLayout;

    TouchZone       *m_pTouchZone;

    QRect           rectMargin, rectMarginUp;
    QRect           lanR, remindR, usbR, voiceR, timeR;
    QString         lanOn, lanOff, lxion;
    QString         remindOn, remindOff;
    QString         usbOn, usbOff;
    QString         voiceOn, voiceOff;
    QString         timeQss;
};

#endif // APPUTILITY_H
