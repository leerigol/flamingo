#include "keyrecord.h"
#include "../../service/servutility/servutility.h"
#include "../../com/virtualkb/virtualkb.h"
namespace menu_res  {

keyRecord::keyRecord(QWidget *parent)
    : menu_res::RModalWnd( parent )
{
    m_pBtnRun     = NULL;
    m_pBtnStop    = NULL;
    m_pBtnSave    = NULL;
    m_pEditName   = NULL;
    set_attr( wndAttr, mWndAttr, wnd_moveable|wnd_manual_resize );
}

void keyRecord::showEvent(QShowEvent *event)
{
    //! base
    RModalWnd::showEvent( event );
}

void keyRecord::setupUi(const QString &path, const r_meta::CMeta &meta)
{
    //! create
    m_pBtnRun = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnRun );

    m_pBtnStop = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnStop );

    m_pBtnSave = new QToolButton( this );
    Q_ASSERT( NULL != m_pBtnSave );

    m_pEditName = new menu_res::RLineEdit( this );
    Q_ASSERT( NULL != m_pEditName );

    //! apply ui
    getGeo( meta, path + "geo/wnd",       this );
    getGeo( meta, path + "geo/runStop",   m_pBtnRun );
    getGeo( meta, path + "geo/cancel",    m_pBtnStop );
    getGeo( meta, path + "geo/save",      m_pBtnSave );
    getGeo( meta, path + "geo/edit_name", m_pEditName );
    //! style
    QString str;
    meta.getMetaVal( path + "style", str );
    this->setStyleSheet( str );

    //! connection
    connect( m_pEditName, SIGNAL(sig_mouseRelease()),
             this, SLOT(on_name_change()) );

    connect( m_pBtnRun, SIGNAL(clicked(bool)),
             this, SLOT(on_btn_run_suspend(bool)) );
    connect( m_pBtnStop, SIGNAL(clicked(bool)),
             this, SLOT(on_btn_stop(bool)) );
    connect( m_pBtnSave, SIGNAL(clicked(bool)),
             this, SLOT(on_btn_save(bool)) );

    connect( this, SIGNAL(sigClose()),
             this, SLOT(on_close()) );
    //!  set str
    this->setTitle("key ecording"/*sysGetString(-1, "key ecording")*/);

    m_pBtnRun->setText("suspend");
    m_pBtnStop->setText("Stop");
    m_pBtnSave->setText("save");
    m_pEditName->setText("test");
}

void keyRecord::on_name_change()
{
    VirtualKB::Show(this,
                    SLOT(onImeName(QString)),
                    QString("key record name"),
                    m_pEditName->text()
                    );
}

void keyRecord::on_btn_run_suspend(bool /*b*/)
{
    m_runStop = !m_runStop;
    m_pBtnRun->setText(m_runStop?"Run":"suspend");

    if(m_runStop)
    {
        serviceExecutor::post(serv_name_utility,
                              servUtility::cmd_key_record_run,
                              0);
        m_pEditName->setEnabled(false);
    }
    else
    {
        serviceExecutor::post(serv_name_utility,servUtility::
                              cmd_key_record_suspend,
                              0);
        m_pEditName->setEnabled(true);
    }
}

void keyRecord::on_btn_stop(bool /*b*/)
{

    serviceExecutor::post(serv_name_utility,
                          servUtility::cmd_key_record_stop,
                          0);
    m_runStop = false;
    m_pBtnRun->setText("suspend");
    m_pEditName->setEnabled(true);
}

void keyRecord::on_btn_save(bool /*b*/)
{
    serviceExecutor::post(serv_name_utility,
                          servUtility::cmd_key_record_save,
                          0);
}

void keyRecord::onImeName(const QString &str)
{
    m_pEditName->setText(str);
    serviceExecutor::post(serv_name_utility,
                          servUtility::cmd_key_record_name,
                          str
                          );
}

void keyRecord::on_close()
{
    on_btn_stop(false);
}

}
