#ifndef KEYRECORD_H
#define KEYRECORD_H

#include "../../menu/wnd/rmodalwnd.h"
#include "../../widget/rlineedit.h"
namespace menu_res  {
class keyRecord: public RModalWnd
{
    Q_OBJECT
public:
    keyRecord(QWidget *parent);
protected:
    virtual void showEvent( QShowEvent *event );
    virtual void setupUi( const QString &path,
                  const r_meta::CMeta &meta  );

private:
    QToolButton            *m_pBtnRun;
    QToolButton            *m_pBtnStop;
    QToolButton            *m_pBtnSave;
    menu_res::RLineEdit    *m_pEditName;


protected Q_SLOTS:
    void   on_name_change();
    void   on_btn_run_suspend(bool b);
    void   on_btn_stop(  bool b);
    void   on_btn_save(bool b);
    void   onImeName(const QString &str);
    void   on_close();

Q_SIGNALS:

private:
    bool         m_runStop;
public:
    void  setRunStop(bool b);
    void  setCancel();
    void  save();
};
}
#endif // KEYRECORD_H
