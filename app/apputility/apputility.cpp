#include <QDateTime>
#include <QHBoxLayout>
#include "../../service/servplot/servplot.h"
#include "../../service/servutility/servutility.h"
#include "apputility.h"

#include "../../service/servscpi/servscpi.h"

#include "../../service/servscpi/servscpidso.h"

#include "../appmain/touchzone.h"

using namespace dsoServScpi;
IMPLEMENT_CMD( CApp, CAppUtility )
start_of_entry()

on_set_int_int( MSG_APP_UTILITY_SYSTEMINFO,        &CAppUtility::onShowInfo),

on_set_int_int( MSG_APP_UTILITY_SCREEN_PREVIEW,    &CAppUtility::onScrPreview),

on_set_int_int( servUtility::cmd_dso_saver_start,  &CAppUtility::onScrStart),

on_set_int_int( MSG_APP_UTILITY_KEY_TEST,          &CAppUtility::onKbCheck),
on_set_int_int( MSG_APP_UTILITY_SCREEN_TEST,       &CAppUtility::onLcdCheck),

on_set_int_int( MSG_APP_UTILITY_TOUCH_TEST,        &CAppUtility::onTouchCheck),
on_set_int_int( MSG_APP_UTILITY_FAN_TEST,          &CAppUtility::onFanCheck),
on_set_int_int( MSG_APP_UTILITY_SELF_TEST,         &CAppUtility::onBoardCheck),
on_set_int_int( servUtility::cmd_dso_checkboard,   &CAppUtility::onBoardTestResult),

on_set_int_void(MSG_APP_UTILITY_TIME,              &CAppUtility::onTimeShowClicked),
on_set_void_void( MSG_APP_UTILITY_SHOW_TIME,       &CAppUtility::onTimeIsShow ),
on_set_int_int( MSG_APP_UTILITY_HDMI_EDID,         &CAppUtility::onEDID),

on_set_void_void( MSG_APP_UTILITY_BEEPER,          &CAppUtility::on_beeper ),

on_set_void_void( CAppUtility::cmd_lan_changed,    &CAppUtility::onLanBtn),
on_set_void_void( CAppUtility::cmd_usb_insert,     &CAppUtility::onUsbInsert),
on_set_void_void( CAppUtility::cmd_usb_remove,     &CAppUtility::onUsbRemove),

on_set_void_void( CAppUtility::cmd_log_info,       &CAppUtility::on_log_info ),

on_set_int_void( MSG_APP_UTILITY_SHUTDOWN,         &CAppUtility::on_shutdown ),

on_set_int_int( MSG_APP_UTILITY_RECORD_KEY,        &CAppUtility::onRecord ),

on_set_int_int( CMD_SERVICE_DEACTIVE,              &CAppUtility::onDeActive),
on_set_int_int( CMD_SERVICE_EXIT_ACTIVE,           &CAppUtility::onDeActive),

on_set_int_void( CAppUtility::cmd_menu_onoff,      &CAppUtility::onMenuOnOff),

on_set_int_void( CMD_ZONE_TOUCH_ENABLE,            &CAppUtility::onZoneTouchOnOff),

end_of_entry()

CAppUtility::CAppUtility()
{
    mservName       = serv_name_utility;

//    addQuick(   MSG_APP_UTILITY_MENU,
//                QStringLiteral("ui/desktop/utility/icon/"),1 );

    //! quick en
    addQuick( CMD_ZONE_TOUCH_ENABLE,
              QStringLiteral("ui/desktop/zone/icon/")
              ,CMD_ZONE_TOUCH_ENABLE);

    //! quick opened
    addQuick( CMD_ZONE_TOUCH_ENABLE,
              QStringLiteral("ui/desktop/zone/icon_open/"),
              CMD_ZONE_TOUCH_ENABLE,
              app_opened );

    addQuick( MSG_APP_UTILITY_RESTART ,
             QString("ui/desktop/shutdown/icon/"), MSG_APP_UTILITY_SHUTDOWN);



    isTimeShow      = false;
    m_pSysInfo      = NULL;
    m_pShutdown     = NULL;
    m_pScrSaver     = NULL;
    m_pKeyTestDlg   = NULL;
    m_pLcdTestDlg   = NULL;
    m_pTouchTestDlg = NULL;
    m_pFanTestDlg   = NULL;
    m_pBoardTestDlg = NULL;

    timeShowBtn     = NULL;
    timeDlg         = NULL;

    pScrTimer       = NULL;
    m_pLogInfo      = NULL;
    mLxiCount       = 0;
    usbCount        = 0;

    timer = new QTimer();
    //! per = minute
    timer->start( 50000 );

    //! attach pop window
    attachPopupWindow( &m_pFanTestDlg, MSG_APP_UTILITY_FAN_TEST );
    attachPopupWindow( &m_pBoardTestDlg, MSG_APP_UTILITY_SELF_TEST );
}

void CAppUtility::setupUi( CAppMain *pMain )
{
    loadResource("appUtility/iconbtn/");

    timeShowBtn = new QPushButton(pMain->getDownBar());
    Q_ASSERT( timeShowBtn != NULL );
    timeShowBtn->setStyleSheet( timeQss );
    timeShowBtn->setFlat(true);

    QFont font;
    font.setFamily("Arial" );
    font.setPointSize( 12 );
    font.setWeight(QFont::Thin);
    timeShowBtn->setFont(font);

    timeShowBtn->setFocusPolicy(Qt::NoFocus);
    QDateTime d = QDateTime::currentDateTime();
    QString strTime = d.time().toString("HH:mm");
    timeShowBtn->setText(strTime);
    timeShowBtn->setCheckable(false);
    timeShowBtn->setMinimumSize(timeR.size());
    timeShowBtn->setMaximumSize(timeR.size());

    hLayout = new QHBoxLayout();
    hLayout->setSpacing(0);
    hLayout->setContentsMargins(rectMargin.x(), rectMargin.y(), rectMargin.width(), rectMargin.height());

    QSpacerItem *hSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    hLayout->addItem(hSpacer);

    lanBtn = new RImageButton(pMain->getDownBar());
    Q_ASSERT( lanBtn != NULL );
    lanBtn->setMinimumSize(lanR.size());
    lanBtn->setMaximumSize(lanR.size());
    lanBtn->setImage( lxion, lxion, lxion, false );

    remindBtn = new RImageButton(pMain->getDownBar());
    Q_ASSERT( remindBtn != NULL );
    remindBtn->setMinimumSize(remindR.size());
    remindBtn->setMaximumSize(remindR.size());
    remindBtn->setImage( remindOn, remindOn, remindOn, false );

    usbBtn = new RImageButton(pMain->getDownBar());
    Q_ASSERT( usbBtn != NULL );
    usbBtn->setMinimumSize(usbR.size());
    usbBtn->setMaximumSize(usbR.size());
    usbBtn->setImage( usbOn, usbOn, usbOn, false );

    voiceBtn = new RImageButton(pMain->getDownBar());
    Q_ASSERT( voiceBtn != NULL );
    voiceBtn->setMinimumSize(voiceR.size());
    voiceBtn->setMaximumSize(voiceR.size());
    voiceBtn->setImage( voiceOff, voiceOn, voiceOff, false );
    voiceBtn->setImage( voiceOn, voiceOn, voiceOff, true );

    hLayout->addWidget(remindBtn);
    hLayout->addWidget(usbBtn);
    hLayout->addWidget(lanBtn);
    hLayout->addWidget(voiceBtn);
    hLayout->addWidget(timeShowBtn);

    pMain->getDownBar()->setLayout(hLayout);

    //! log info
    m_pLogInfo = new menu_res::WndLogInfo();
    Q_ASSERT( NULL != m_pLogInfo );

    //! Key Recording
    m_pKeyRecord = new menu_res::keyRecord(pMain->getCentBar());
    Q_ASSERT( NULL != m_pKeyRecord );
    m_pKeyRecord->setup("appkeyrecord/", r_meta::CAppMeta());
    m_pKeyRecord->setVisible(false);

    //! init
    remindBtn->hide();
    usbBtn->hide();
    lanBtn->hide();

    //! TouchZone
    m_pTouchZone = pMain->getTouchZoneBar();
    Q_ASSERT( NULL != m_pTouchZone );
}
void CAppUtility::retranslateUi()
{}
void CAppUtility::buildConnection()
{
    connect(timer, SIGNAL(timeout()), this, SLOT(onTimeShow()));
    connect(timeShowBtn, SIGNAL(clicked()), this, SLOT(onTimeShowtouched()));

//    connect( lanBtn, SIGNAL(clicked()),this, SLOT(onLanBtn()) );
    connect( remindBtn, SIGNAL(clicked()),this, SLOT(onRemindBtn()) );
//    connect( usbBtn, SIGNAL(clicked()),this, SLOT(onUsbInsert()) );
    connect( voiceBtn, SIGNAL(clicked(bool)),this, SLOT(onVoiceBtn(bool)) );
}
void CAppUtility::registerSpy()
{
    spyOn( serv_name_utility_IOset, servInterface::cmd_net_status,CAppUtility::cmd_lan_changed );
    spyOn( serv_name_storage, MSG_STORAGE_USB_INSERT,       CAppUtility::cmd_usb_insert);
    spyOn( serv_name_storage, MSG_STORAGE_USB_REMOVE,       CAppUtility::cmd_usb_remove);

    spyOn( serv_name_gui,   servGui::cmd_log_str,           CAppUtility::cmd_log_info );

    spyOn( serv_name_scpi,  cmd_lxi_newconnected,           CAppUtility::cmd_lxi_connect );
    spyOn( serv_name_scpi,  cmd_lxi_disconnected,           CAppUtility::cmd_lxi_disconnect );
    spyOn( serv_name_gui,   CMD_SERVICE_PAGE_SHOW,              CAppUtility::cmd_menu_onoff );
}

void CAppUtility::loadResource(QString path)
{
    r_meta::CAppMeta meta;
    meta.getMetaVal( path + "lanrect",          lanR );
    meta.getMetaVal( path + "remindrect",       remindR );
    meta.getMetaVal( path + "usbrect",          usbR );
    meta.getMetaVal( path + "voicerect",        voiceR );
    meta.getMetaVal( path + "timerect",         timeR );

    meta.getMetaVal( path + "margin",         rectMargin );
    meta.getMetaVal( path + "marginup",       rectMarginUp );

    meta.getMetaVal( path + "lanon",        lanOn );
    meta.getMetaVal( path + "lxion",        lxion );
    meta.getMetaVal( path + "lanoff",       lanOff );
    meta.getMetaVal( path + "remindon",     remindOn );
    meta.getMetaVal( path + "remindoff",    remindOff );
    meta.getMetaVal( path + "usbon",        usbOn );
    meta.getMetaVal( path + "usboff",       usbOff );
    meta.getMetaVal( path + "voiceon",      voiceOn );
    meta.getMetaVal( path + "voiceoff",     voiceOff );

    meta.getMetaVal( path + "timeqss",      timeQss );
}

int CAppUtility::onShowInfo(int)
{
    if ( m_pSysInfo == NULL )
    {
        m_pSysInfo = new CSysInfoDlg();
        Q_ASSERT( m_pSysInfo != NULL );
    }

    if ( m_pSysInfo->isHidden() )
    {
        bool bView = false;
        query( MSG_APP_UTILITY_PROJECT, bView );
        queryInfo(0);
        m_pSysInfo->show(bView);
    }
    else
    {
        if ( m_pSysInfo != NULL )
        {
            m_pSysInfo->hide();
        }
    }
    return ERR_NONE;
}

int CAppUtility::queryInfo(int)
{
    QString str = servUtility::getSystemModel();
    QHash<Bandwidth,QString> hash;

    hash[BW_70M]   = "70M";
    hash[BW_100M]  = "100M";
    hash[BW_200M]  = "200M";
    hash[BW_250M]  = "250M";
    hash[BW_350M]  = "350M";
    hash[BW_500M]  = "500M";
    hash[BW_750M]  = "750M";
    hash[BW_1G]    = "1G";
    hash[BW_2G]    = "2G";
    hash[BW_4G]    = "4G";

    Bandwidth bw = sysGetBw();
    if( hash.contains( bw ) )
    {
        QString strBw = QString("   (%1%2)").arg(sysGetString(INF_SYSTEM_BW,"BW:"))
                                            .arg(hash[bw]);
        str.append( strBw );
    }

    m_pSysInfo->setMode(str);

    str = servUtility::getSystemSerial();
    m_pSysInfo->setSerialSn(str);

    str = servUtility::getSystemVersion();
    m_pSysInfo->setSoftwarVer(str);

    query(servUtility::cmd_dso_hardware,str);
    m_pSysInfo->setHardwarVer(str);

    query(servUtility::cmd_dso_bootware,str);
    m_pSysInfo->setBootVer(str);

    query(servUtility::cmd_dso_spu,str);
    m_pSysInfo->setSPUVer(str);

    query(servUtility::cmd_dso_wpu,str);
    m_pSysInfo->setWPUVer(str);

    query(servUtility::cmd_dso_scu,str);
    m_pSysInfo->setCCUVer(str);

    query(servUtility::cmd_dso_mcu,str);
    m_pSysInfo->setMCUVer(str);

#ifdef _SIMULATE
    str = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
#else
    query(servUtility::cmd_dso_builddate,str);
#endif
    m_pSysInfo->setDatetime(str);

    int adcId = 0;
    query(servUtility::cmd_dso_adc_id0, adcId);
    m_pSysInfo->setADCChipId0(adcId);

    if(sysHasArg( "-ds8000"))
    {
        int adcId = 0;
        query(servUtility::cmd_dso_adc_id1, adcId);
        adcId = adcId & 0xffff;
        m_pSysInfo->setADCChipId1(adcId);
    }

    int afeVer = 0;
    query(servUtility::cmd_dso_afe_ver0, afeVer);
    m_pSysInfo->setAFEChipVer0(afeVer);

    query(servUtility::cmd_dso_afe_ver1, afeVer);
    m_pSysInfo->setAFEChipVer1(afeVer);

    query(servUtility::cmd_dso_afe_ver2, afeVer);
    m_pSysInfo->setAFEChipVer2(afeVer);

    query(servUtility::cmd_dso_afe_ver3, afeVer);
    m_pSysInfo->setAFEChipVer3(afeVer);

    //qDebug()<<"sysgetcycleCnt:"<<sysGetCycleCnt();

    int times = sysGetLiveCnt();
    m_pSysInfo->setStartTimes(times);
    return ERR_NONE;
}

int CAppUtility::onScrStart(int)
{
    bool bScrShow = false;
    query(servUtility::cmd_dso_saver_start, bScrShow);
    if( bScrShow)
    {
        ShowScr(0);
    }

    return ERR_NONE;
}

int CAppUtility::ShowScr(int)
{
    int pic_or_text = 0;
    query(MSG_APP_UTILITY_SCREEN_SELECT, pic_or_text);
    if(pic_or_text > servUtility::scr_saver_off )
    {
        if ( m_pScrSaver == NULL )
        {
            m_pScrSaver = new CScrSaverDlg();
            Q_ASSERT( m_pScrSaver != NULL );
        }

        m_pScrSaver->setContent(pic_or_text);

        QString str = "";
        if(pic_or_text == servUtility::scr_saver_txt)
        {
            query(MSG_APP_UTILITY_SCREEN_WORD, str);
        }
        else
        {
            query(MSG_APP_UTILITY_SCREEN_PICTURE, str);
        }
        m_pScrSaver->setString(str);

        //qDebug() << str << __FUNCTION__;
        m_pScrSaver->show();
    }
    return ERR_NONE;
}

int CAppUtility::onScrPreview(int)
{

    bool preview = false;
    query(MSG_APP_UTILITY_SCREEN_PREVIEW, preview);

    if( preview )
    {
        ShowScr(0);
    }

    return ERR_NONE;
}

int CAppUtility::onKbCheck(int)
{
    bool bView = false;
    query( MSG_APP_UTILITY_KEY_TEST, bView );
    if(bView)
    {
        if ( m_pKeyTestDlg == NULL )
        {
            m_pKeyTestDlg = new CKeyForm();
            Q_ASSERT( m_pKeyTestDlg != NULL );
        }

        if(m_pKeyTestDlg && m_pKeyTestDlg->isHidden())
        {
            m_pKeyTestDlg->setGeometry(sysGetMainWindow()->geometry());
            m_pKeyTestDlg->show();
        }
    }
    return ERR_NONE;
}


int CAppUtility::onLcdCheck(int)
{
    bool bView = false;

    query( MSG_APP_UTILITY_SCREEN_TEST,bView);

    if(bView)
    {
        if ( m_pLcdTestDlg == NULL )
        {
            m_pLcdTestDlg = new CheckLcdDlg();
            Q_ASSERT( m_pLcdTestDlg != NULL );
        }

        if(m_pLcdTestDlg && m_pLcdTestDlg->isHidden())
        {
            m_pLcdTestDlg->setGeometry(sysGetMainWindow()->geometry());
            m_pLcdTestDlg->show();
        }
    }
    return ERR_NONE;
}

int CAppUtility::onTouchCheck(int)
{
    bool bView = false;

    query( MSG_APP_UTILITY_TOUCH_TEST,bView);

    if(bView)
    {
        if ( m_pTouchTestDlg == NULL )
        {
            m_pTouchTestDlg = new CheckTouchDlg();
            Q_ASSERT( m_pTouchTestDlg != NULL );
        }

        if(m_pTouchTestDlg && m_pTouchTestDlg->isHidden())
        {
            m_pTouchTestDlg->setGeometry(sysGetMainWindow()->geometry());
            m_pTouchTestDlg->reset();
            m_pTouchTestDlg->show();
        }
    }
    else
    {
        if(m_pTouchTestDlg && m_pTouchTestDlg->isVisible())
        {
            m_pTouchTestDlg->close();
            delete m_pTouchTestDlg;
            m_pTouchTestDlg = NULL;
        }
    }
    return ERR_NONE;
}


int CAppUtility::onFanCheck(int)
{
    bool bView = false;
    query( MSG_APP_UTILITY_FAN_TEST,bView);
    if(bView)
    {
        if( m_pFanTestDlg == NULL )
        {
            m_pFanTestDlg = new CheckFanDlg();
            Q_ASSERT( m_pFanTestDlg != NULL);
        }

        if(m_pFanTestDlg && m_pFanTestDlg->isHidden())
        {
            int speed = 100;
            query(servUtility::cmd_dso_fanspeed, speed);
//            qDebug() << "speed="<< speed;
            m_pFanTestDlg->setSpeed(speed);
            m_pFanTestDlg->show();
        }
    }
    else
    {
        if(m_pFanTestDlg && m_pFanTestDlg->isVisible())
        {
            m_pFanTestDlg->hide();
        }
    }
    return ERR_NONE;
}

int CAppUtility::onBoardTestResult(int)
{
    if( m_pBoardTestDlg )
    {
        void *ptr = NULL;
        query(servUtility::cmd_dso_checkboard, ptr);
        m_pBoardTestDlg->setResult( ptr );
        m_pBoardTestDlg->update();
    }
    return ERR_NONE;
}

int CAppUtility::onBoardCheck(int)
{
    bool bView = false;
    query(MSG_APP_UTILITY_SELF_TEST,bView);

    if(bView)
    {
        if ( m_pBoardTestDlg == NULL )
        {
            m_pBoardTestDlg = new CheckBoardDlg();
            Q_ASSERT( m_pBoardTestDlg != NULL );
        }        
        m_pBoardTestDlg->show();
    }
    else
    {
        if( m_pBoardTestDlg != NULL)
        {
            delete m_pBoardTestDlg;
            m_pBoardTestDlg = NULL;
        }
    }
    return ERR_NONE;
}

int CAppUtility::onDeActive(int)
{
    if(m_pBoardTestDlg && m_pBoardTestDlg->isVisible())
    {
        m_pBoardTestDlg->hide();
    }

    if( m_pFanTestDlg && m_pFanTestDlg->isVisible() )
    {
        m_pFanTestDlg->hide();
    }

    on_exitActive();

    return ERR_NONE;
}

int CAppUtility::onSetTime(bool )
{
    QDateTime d = QDateTime::currentDateTime();
    QString strTime = d.time().toString("HH:mm");
    timeShowBtn->setText(strTime);
//    if(v)
//    {
//        if ( timeDlg == NULL )
//        {
//            timeDlg = new CTimeWidget();
//            connect(timeDlg, SIGNAL(closeTimeSig()),this, SLOT(onTimeClose()));
//            timeDlg->setGeometry(200,60,250,400);
//            timeDlg->setParent(sysGetMidWindow());
//            Q_ASSERT( timeDlg != NULL );
//        }
//        timeDlg->setVisible(true);
//        timeDlg->raise();
//    }
//    else
//    {
//        if( timeDlg != NULL)
//        {
//            timeDlg->hide();
//        }
//    }
    return ERR_NONE;
}

int CAppUtility::onTimeShow()
{
    QDateTime d = QDateTime::currentDateTime();
    QString strTime = d.time().toString("HH:mm");

    timeShowBtn->setText(strTime);
    return ERR_NONE;
}

void CAppUtility::onTimeClose()
{
    serviceExecutor::post(E_SERVICE_ID_UTILITY,
                          MSG_APP_UTILITY_TIME, 1);
}

void CAppUtility::onTimeShowtouched()
{
    serviceExecutor::post( mservName,
                           CMD_SERVICE_ACTIVE,
                           MSG_APP_UTILITY_YEAR );

//    servGui::logInfo( "This is test sys log!" );
//    servGui::logInfo( __FILE__, 10 );
}

void CAppUtility::onLanBtn()
{
    int s = 0;
    serviceExecutor::query(serv_name_utility_IOset, servInterface::cmd_net_status, s);

    int t = s - servInterface::NET_STATUS_UNLINK;
    int v = servInterface::NET_STATUS_SET_MAC - servInterface::NET_STATUS_UNLINK;
    if(t >=0 && t < v)
    {
        if( s == servInterface::NET_STATUS_CONFIGURED ||
            s == servInterface::NET_STATUS_CONNECTED)
        {
            lanBtn->show();
        }
        else
        {
            lanBtn->hide();
        }
    }
}

void CAppUtility::onRemindBtn()
{
    //! create
    r_meta::CAppMeta meta;
    if ( m_pLogInfo->setup("appgui/loginfo/", meta) )
    {
        //! connect only one time
        connect( m_pLogInfo->m_pBtnClear,SIGNAL(clicked(bool)),
                 this,
                 SLOT(on_loginfo_clear()));
    }
    else
    {}

    if ( m_pLogInfo->isVisible() )
    {
        m_pLogInfo->hide();
    }
    else
    {
        on_log_info();

        m_pLogInfo->show();
    }
}

void CAppUtility::on_loginfo_clear()
{
    remindBtn->hide();
}

void CAppUtility::onUsbInsert()
{
    usbCount++;
    usbBtn->show();
}

void CAppUtility::onUsbRemove()
{
    usbCount--;
    if(usbCount > 0)
    {
        return;
    }

    usbBtn->hide();
}

void CAppUtility::onVoiceBtn( bool bCheck )
{
    serviceExecutor::post( E_SERVICE_ID_UTILITY,
                           MSG_APP_UTILITY_BEEPER, bCheck );
}

void CAppUtility::onTimeShowClicked()
{
    bool bView = false;
    query(MSG_APP_UTILITY_TIME, bView);
    onSetTime(bView);
}

void CAppUtility::onTimeIsShow()
{
    bool b=false;
    serviceExecutor::query( mservName, MSG_APP_UTILITY_SHOW_TIME, b );
    if(b)
        timeShowBtn->show();
    else
        timeShowBtn->hide();
}

int CAppUtility::onEDID(int)
{
//    bool bView = false;
//    query(MSG_APP_UTILITY_HDMI_EDID,bView);

//    if(bView)
//    {
//        if ( m_pEDID == NULL )
//        {
//            m_pEDID = new EdidForm();
//            Q_ASSERT( m_pEDID != NULL );

//            void* ptr = NULL;
//            query(servUtility::cmd_hdmi_edid, ptr);
//            if(ptr)
//            {
//                m_pEDID->setData(ptr);
//            }
//        }
//        m_pEDID->setVisible(true);
//    }
//    else
//    {
//        if( m_pEDID != NULL)
//        {
//            delete m_pEDID;
//            m_pEDID = NULL;
//        }
//    }
    return ERR_NONE;
}

void CAppUtility::on_beeper()
{
    bool bBeeper;

    query( mservName, MSG_APP_UTILITY_BEEPER, bBeeper );

    Q_ASSERT( NULL != voiceBtn );
    voiceBtn->setChecked( bBeeper );
}

void CAppUtility::on_log_info()
{
    DsoErr err;

    //! get size
    int size;
    err = query( serv_name_gui,
           servGui::qcmd_log_str_size,
           size );
    if ( err != ERR_NONE ) { return; }
    if ( size > 0 )
    { remindBtn->show(); }

    //! not created
    if ( !m_pLogInfo->isSetuped() )
    { return; }

    //! read info
    pointer ptr;
    err = query( serv_name_gui,
           servGui::qcmd_log_str,
           ptr );
    if ( err != ERR_NONE || NULL == ptr ){ return; }

    //! deload
    QStringList *pStrList;
    pStrList = static_cast<QStringList*>( ptr );
    if ( NULL == pStrList )
    { return; }

    m_pLogInfo->logInfo( pStrList );
}

int CAppUtility::on_shutdown()
{
    if ( m_pShutdown == NULL )
    {
        m_pShutdown = new Shutdown(sysGetMidWindow());

        Q_ASSERT( m_pShutdown != NULL );
    }

    if ( m_pShutdown->isHidden() )
    {
        m_pShutdown->show();
    }
    else
    {
        m_pShutdown->hide();
    }

    return ERR_NONE;
}

DsoErr CAppUtility::onRecord(AppEventCause cause)
{
    if(cause != cause_value)    return ERR_NONE;
    m_pKeyRecord->setVisible(!m_pKeyRecord->isVisible());
    return ERR_NONE;
}

int CAppUtility::onMenuOnOff()
{
    bool b;
    query(serv_name_gui, CMD_SERVICE_PAGE_SHOW,  b );
    if(b)
    {
        hLayout->setContentsMargins(rectMargin.x(),
                                    rectMargin.y(),
                                    rectMargin.width(),
                                    rectMargin.height());
    }
    else
    {
        hLayout->setContentsMargins(rectMarginUp.x(),
                                    rectMarginUp.y(),
                                    rectMarginUp.width(),
                                    rectMarginUp.height());
    }

    return ERR_NONE;
}

int CAppUtility::onZoneTouchOnOff()
{
    Q_ASSERT( NULL != m_pTouchZone );

    //! 1、第一次由appmain发送，将m_pTouchZone放到顶层
    static bool bFirst = true;
    if(bFirst)
    {
        m_pTouchZone->raise();
        bFirst = false;
        return ERR_NONE;
    }

    //! 2、区域与手势操作切换。
    bool en = false;
    query( getServiceName(), CMD_ZONE_TOUCH_ENABLE,  en );

    m_pTouchZone->setTouchEn(en);

    if(en)
    {
        setState( app_opened);
    }
    else
    {
        setState( app_installed);
    }

    return ERR_NONE;
}

