#ifndef CSCREENSAVE_H
#define CSCREENSAVE_H

#include <QImage>
#include <QWidget>
#include <QTimer>

#include "../../menu/wnd/uiwnd.h"

class CScrSaverDlg : public menu_res::uiWnd
{
    Q_OBJECT


public:
    CScrSaverDlg(int pic_or_text = 1); // default 0-off,1-picture,2-text
    ~CScrSaverDlg();

    void show();

public:
    void setString(QString&);
    void setContent(int);
protected:
    void mousePressEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *);
    void closeEvent(QCloseEvent *);

public Q_SLOTS:
    //timer handler
    void onMoveScr();
private:
    QImage  m_scrImage;
    int     m_nContentType;
    QString m_strContent;
    QTimer  m_Timer;
};

#endif // CSCREENSAVE_H

