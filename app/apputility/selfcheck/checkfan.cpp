#include "../../service/servutility/servutility.h"
#include"checkfan.h"

CheckFanDlg::CheckFanDlg( QWidget *parent):RInfoWnd(parent)
{
    setWindowFlags( Qt::FramelessWindowHint);
    mSpeed = 100;

    this->setStyleSheet("QLabel{color:rgb(255,255,255); font:14pt;}");
    this->setGeometry(100,200, 580,200);

    mReminder.setParent(this);
    mReminder.setGeometry(0,100,580,30);
    mReminder.setAlignment(Qt::AlignCenter);

    mDescription.setParent(this);
    mDescription.setGeometry(0,30,580,30);
    mDescription.setAlignment(Qt::AlignCenter);
    mDescription.setText( tr("Adjust the FAN speed by rotating the Function Knob") );

    mSlider = new QSlider(this);
    connect(mSlider,SIGNAL(valueChanged(int)),this,SLOT(sendSpeed()));
    mSlider->setGeometry(100,125,380,30);
    mSlider->setOrientation(Qt::Horizontal);
    setSlideStyle();

    mKeyList.append( R_Pkey_FInc);
    mKeyList.append( R_Pkey_FDec);
    setAttr(wnd_moveable);
    if ( mKeyList.size() > 0 )
    {
        set_attr( uiWnd::wndAttr, mWndAttr, uiWnd::wnd_grab|wnd_moveable );
    }
    else
    {
        unset_attr( uiWnd::wndAttr, mWndAttr, uiWnd::wnd_grab|wnd_moveable );
        unload();
    }
}

CheckFanDlg::~CheckFanDlg()
{
    hide();
}

void CheckFanDlg::setSpeed(int s)
{
    mSpeed = s;
    if(mSlider->value() != s)
        mSlider->setValue(mSpeed);
    mReminder.setText(QString("%1%").arg(mSpeed+1));
}

void CheckFanDlg::sendSpeed()
{
    mSpeed = mSlider->value();
    mReminder.setText(QString("%1%").arg(mSpeed+1));
    serviceExecutor::post( serv_name_utility,
                           servUtility::cmd_dso_fanspeed,
                           mSpeed );
}

void CheckFanDlg::adjustFanSpeed(int key)
{
    switch(key)
    {
        case R_Pkey_FInc:
            mSpeed += 1;
            if(mSpeed >=99)
            {
                mSpeed = 99;
            }
            serviceExecutor::post( serv_name_utility,
                                   servUtility::cmd_dso_fanspeed,
                                   mSpeed );
            break;
        case R_Pkey_FDec:
            mSpeed -= 1;
            if(mSpeed <= 0)
            {
                mSpeed = 0;
            }
            serviceExecutor::post( serv_name_utility,
                                   servUtility::cmd_dso_fanspeed,
                                   mSpeed );
            break;
    }
    mSlider->setValue(mSpeed);
    mReminder.setText(QString("%1%").arg(mSpeed+1));
}

void CheckFanDlg::keyPressEvent(QKeyEvent *event)
{
    Q_ASSERT( NULL != event );
    if(event->key() == R_Pkey_RunStop)
    {
        mRunCount++;
        if(3 == mRunCount)
        {
             this->hide();
        }
    }
    else
    {
         mRunCount = 0;
    }
//    adjustFanSpeed(event->key());
}

void CheckFanDlg::keyReleaseEvent(QKeyEvent *event)
{
    adjustFanSpeed(event->key());
}

void CheckFanDlg::setSlideStyle()
{
    mSlider->setStyleSheet("QSlider::groove:horizontal{border: 0px solid #bbb;background: #555;\
                          height: 10px;border-radius: 4px;}\
                          QSlider::sub-page:horizontal {\
                          background: qlineargradient(x1: 0, y1: 0,x2: 1, y2: 0,\
                              stop: 0 #007fff, stop: 1 #ffffff);\
                          border: 1px solid #777;height: 10px;border-radius: 4px;}\
                          QSlider::add-page:horizontal {border: 1px solid #777;\
                          height: 10px;border-radius: 4px;}\
                          QSlider::handle:horizontal{\
                          background: qlineargradient(x1:0, y1:0, x2:1, y2:1,\
                              stop:0 #eee, stop:1 #000);border: 1px solid #777;\
                          width: 20px; margin-top: -5px;margin-bottom: -5px;\
                          border-radius: 10px;}");
}
