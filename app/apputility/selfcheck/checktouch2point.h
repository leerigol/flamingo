#ifndef CHECKTOUCH2POINT_H
#define CHECKTOUCH2POINT_H

#include <QPainter>
#include <QMouseEvent>
#include "../../menu/wnd/uiwnd.h"
#define rWidth  41
#define rHeight 40
class CheckTouch2Point : public menu_res::uiWnd
{
    Q_OBJECT
public:
    explicit CheckTouch2Point(QWidget *parent = 0);
    void reset();

protected:
    void loadResource(QString path);
    void paintEvent(QPaintEvent *);
    bool event(QEvent *event);
    void keyPressEvent( QKeyEvent *event );

private:
    QImage img, pinchImg, stretchImg;
    qreal toWidth, toHeight;
    QLineF oriLineF;
    int    mRunStop ;

    QList<QPointF>  pList;

    QString   mScale;
    int     mTouchPointsNum;
    QTime   mTime;
    int     mSec;

};

#endif // CHECKTOUCH2POINT_H
