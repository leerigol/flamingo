#include "../../service/servutility/servutility.h"
#include "checklcd.h"

screenScene _testScene[]=
{
    //! color
    { 0xff0000, 0x00ffff, 100, QLatin1Literal("Red") },
    { 0x00ff00, 0xff00ff, 100, QLatin1Literal("Green") },
    { 0x0000ff, 0xffff00, 100, QLatin1Literal("Blue") },
    { 0x000000, 0xffffff, 100, QLatin1Literal("Black") },
    { 0xffffff, 0x000000, 100, QLatin1Literal("White") },
    { 0x808080, 0xffffff, 100, QLatin1Literal("Gray") },

    { 0x800000, 0xffffff, 100, QLatin1Literal("Gray") },
    { 0x008000, 0xffffff, 100, QLatin1Literal("Gray") },
    { 0x000080, 0xffffff, 100, QLatin1Literal("Gray") },

    { 0x400000, 0xffffff, 100, QLatin1Literal("Gray") },
    { 0x004000, 0xffffff, 100, QLatin1Literal("Gray") },
    { 0x000040, 0xffffff, 100, QLatin1Literal("Gray") },

    { 0xc00000, 0xffffff, 100, QLatin1Literal("Gray") },
    { 0x00c000, 0xffffff, 100, QLatin1Literal("Gray") },
    { 0x0000c0, 0xffffff, 100, QLatin1Literal("Gray") },

    //! light
    { 0xff0000, 0x00ffff, 10, QLatin1Literal("Red") },
    { 0xff0000, 0x00ffff, 50, QLatin1Literal("Red") },
    { 0xff0000, 0x00ffff, 100, QLatin1Literal("Red") },

    { 0x00ff00, 0xff00ff, 10, QLatin1Literal("Green") },
    { 0x00ff00, 0xff00ff, 50, QLatin1Literal("Green") },
    { 0x00ff00, 0xff00ff, 100, QLatin1Literal("Green") },

    { 0x0000ff, 0xffff00, 10, QLatin1Literal("Blue") },
    { 0x0000ff, 0xffff00, 50, QLatin1Literal("Blue") },
    { 0x0000ff, 0xffff00, 100, QLatin1Literal("Blue") },
};

CheckLcdDlg::CheckLcdDlg(QWidget *parent):uiWnd(parent)
{
    setWindowFlags(Qt::FramelessWindowHint);

    mRunStop = 0;

    //! label
    Reminder1.setParent(this);
    Reminder1.setText(tr("Press the 'SIGNLE' to the next screen."));

    Reminder2.setParent(this);
    Reminder2.setText(tr("Press the 'RUN/STOP' 3 times to exit."));

    mInfo.setParent(this);

    //! init
    setAutoFillBackground(true);


    //! all keys are bypassed
    addAllKeys();

    Reminder1.move(300,550);
    Reminder2.move(300,570);

    mInfo.setGeometry( 0, 0, 1024, 60 );
    mInfo.move( 10, 10 );

    QFont font( qApp->font().family(), 16);
    Reminder1.setFont(font);
    Reminder2.setFont(font);
    mInfo.setFont(font);
}

CheckLcdDlg::~CheckLcdDlg()
{
    this->hide();
}


bool CheckLcdDlg::eventFilter(QObject *o, QEvent *evt)
{
    Q_ASSERT( evt != NULL );

    if ( evt->type() == QEvent::MouseButtonPress )
    {
        QMouseEvent *e = dynamic_cast<QMouseEvent*>( evt );

        mousePressEvent( e );

        return true;
    }
    return uiWnd::eventFilter(o,evt);
}

void CheckLcdDlg::mousePressEvent(QMouseEvent *)
{
    onSceneChanged();
}

void CheckLcdDlg::keyPressEvent(QKeyEvent *event)
{
    if(R_Pkey_RunStop == event->key())
    {
        mRunStop++;
        if(3 == mRunStop)
        {
            this->hide();
            mRunStop = 0;
        }
    }
    else if(R_Pkey_SINGLE == event->key())
    {        
        onSceneChanged();
    }
    else
    {
        //! do nothing
    }
}

void CheckLcdDlg::onSceneChanged()
{
    QPalette palette;

    if ( mScene == array_count(_testScene) )
    {
        hide();
        mScene = 0;
        return;
    }
    Q_ASSERT( mScene >= 0 && mScene < array_count(_testScene) );

    //back
    palette.setColor(QPalette::Background,
                     QColor::fromRgb( _testScene[mScene].mBackColor ) );

    //fore for text
    palette.setColor(QPalette::Foreground,
                     QColor::fromRgb( _testScene[mScene].mForeColor ) );

    setPalette(palette);

    serviceExecutor::post( serv_name_utility,
                           servUtility::cmd_dso_lcdback,
                           _testScene[mScene].mBackLight );

    mInfo.setText( QString("%1 : %2% (%3/%4)").arg("Backlight")
                                            .arg(_testScene[mScene].mBackLight)
                                            .arg( mScene + 1 )
                                            .arg( array_count(_testScene) ));

    mScene++;
}

void CheckLcdDlg::show()
{    
    mScene = 0;
    onSceneChanged();
    return uiWnd::show();
}

void CheckLcdDlg::hide()
{
    //default value define in bootloader
    serviceExecutor::post( serv_name_utility,
                           servUtility::cmd_dso_lcdback,
                           50 );
    return uiWnd::hide();
}



