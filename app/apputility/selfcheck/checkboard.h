#ifndef _CHECK_BOARD_H
#define _CHECK_BOARD_H

#include "../capp.h"
#include "../../menu/wnd/rmodalwnd.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../service/servicefactory.h"
#include "../../service/servutility/servutility.h"
#include "../../menu/menustyle/rinforowlist_style.h"
#include "../../menu/wnd/rpopmenuchildwnd.h"

class CheckBoardDlg : public menu_res::RPopMenuChildWnd
{
    Q_OBJECT
public:
    CheckBoardDlg();
    ~CheckBoardDlg();

    void setResult(void*);

    void paintEvent(QPaintEvent *event);

    void drawItem(stBoardTestItem *item, QRect r);

private:
    QList<stBoardTestItem*> *m_pTestItems;
    QList<int> widthList;
    static menu_res::RInfoRowList_Style _style;

};
#endif // CSELFCHECKINTERNELWND_H

