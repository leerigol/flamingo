#include "mcurve.h"

MCurve::MCurve()
{
}

void MCurve::addPoint(QPoint point)
{
    this->points.append(point);
}

void MCurve::draw(QPainter &painter)
{
    painter.drawPolyline(QPolygonF(points));
}

void MCurve::clearPoints()
{
    points.clear();
}

int MCurve::countNum()
{
    return points.count();
}
