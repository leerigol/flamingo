#ifndef CHECK_LCD_DLG_H
#define CHECK_LCD_DLG_H

#include <QLabel>
#include "../../menu/wnd/uiwnd.h"

//! 测试场景
struct screenScene
{
    int mBackColor;
    int mForeColor;
    int mBackLight;
    QString mInfo;
};

class CheckLcdDlg : public menu_res::uiWnd
{
    Q_OBJECT
public:
    CheckLcdDlg( QWidget *parent = 0 );
    ~CheckLcdDlg();

public:
    void show();
    void hide();

protected:
    void mousePressEvent(QMouseEvent *event);
    void keyPressEvent( QKeyEvent *event );
    bool eventFilter(QObject *, QEvent *);

private:
    void onSceneChanged();

    QLabel Reminder1;
    QLabel Reminder2;
    QLabel mInfo;

    int    mScene;
    int    mRunStop ;
};

#endif // CHECK_LCD_DLG_H

