#include "checkboard.h"

menu_res::RInfoRowList_Style CheckBoardDlg::_style;
CheckBoardDlg::CheckBoardDlg()
{
    this->setGeometry(40,130, 750,369);
    this->setFixedSize(750,369);
    this->setStyleSheet("color:rgb(192,192,192); font:10pt;");

    setWindowFlags(Qt::FramelessWindowHint);
    mKeyList.append( R_Pkey_FInc);
    mKeyList.append( R_Pkey_FDec);
    if ( mKeyList.size() > 0 )
    {
        set_attr( uiWnd::wndAttr, mWndAttr, uiWnd::wnd_grab|wnd_moveable );
    }
    else
    {
        unset_attr( uiWnd::wndAttr, mWndAttr, uiWnd::wnd_grab|wnd_moveable );
        unload();
    }
//    keyChanged();

    _style.loadResource("rframe/");
    _style.setColumnsNum(6);
    _style.setHeightPerRow(20);
    widthList<<150<<75<<150<<150<<75<<150;
    _style.setColumnsWidth(widthList);
    _style.setChoosedRow(-1);

    m_pTestItems = NULL;
}

CheckBoardDlg::~CheckBoardDlg()
{
}

void CheckBoardDlg::setResult(void *p)
{
    m_pTestItems = (QList<stBoardTestItem*>*)p;
}

void CheckBoardDlg::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QRect    rect;
    rect.setX(0);
    rect.setY(0);
    rect.setWidth(this->geometry().width());
    rect.setHeight(this->geometry().height());
     _style.paintEvent(painter, rect);

     QPen     pen;

     pen.setColor(QColor(Qt::white));
     painter.setPen(pen);
     painter.drawText(0,0,150,29, Qt::AlignCenter, "Item");
     painter.drawText(150,0,75,29, Qt::AlignCenter , "Value");
     painter.drawText(225,0,150,29, Qt::AlignCenter , "Range");
     painter.drawText(375,0,150,29, Qt::AlignCenter , "Item");
     painter.drawText(525,0,75,29, Qt::AlignCenter , "Value");
     painter.drawText(600,0,150,29, Qt::AlignCenter , "Range");


    if(m_pTestItems)
    {
        QRect r;
        r.setX(0);
        r.setY(25);
        r.setWidth(150);
        r.setHeight(20);
        int size = m_pTestItems->size();
        for(int i=0; i<size; i++)
        {

            stBoardTestItem *item = m_pTestItems->at(i);
            drawItem(item, r);

            if( 16 == i){
                r.setY(25);
                r.setHeight(20);
                r.moveTo(r.x()+375, r.y());
            }else
                r.moveTo(r.x(), r.y()+20);
        }
    }
    else
    {
        QString str = QString("Board checking...");
        painter.drawText(0,25,150,29, Qt::AlignCenter, str);
    }
}

void CheckBoardDlg::drawItem(stBoardTestItem *item, QRect r)
{
    QPainter painter(this);
    if(item)
    {
        QString curr;
        QString min;
        QString max;

        if(item->is_ok)
        {
            bool valid = false;
            if(item->unit > Unit_none)
            {
                //gui_fmt::CGuiFormatter::format(curr, item->value);
                curr = QString().setNum(item->value, 'f', 3);

                if(item->value >= item->min && item->value <= item->max)
                {
                    valid = true;
                }
                if(item->unit == Unit_V)
                {
                    double f = item->min;
                    gui_fmt::CGuiFormatter::format(min, f);
                    min += "V";
                    f = item->max;
                    gui_fmt::CGuiFormatter::format(max, f);
                    max += "V";


                    curr += "V";
                }
                else if(item->unit == Unit_degree)
                {
                    min = QString().setNum(item->min, 'f', 1) + "D";
                    max = QString().setNum(item->max, 'f', 1) + "D";
                    curr+= "D";
                }
            }
            else
            {
                curr = item->_curr;
                min  = item->_lower;
                max  = item->_upper;
                if( curr.compare(item->_lower) > 0 &&
                        curr.compare(item->_upper) <0 )
                {
                    valid = true;
                }
            }

            if(valid)
            {
                painter.setPen( QPen(Qt::white));
            }
            else
            {
                painter.setPen( QPen(Qt::red));
            }
        }
        else
        {
            painter.setPen( QPen(Qt::red));
            curr = "NG";
        }

        painter.drawText(r, Qt::AlignCenter, item->testName);

        r.setX(r.x()+150);
        r.setWidth(75);
        painter.drawText(r, Qt::AlignCenter, curr);

        r.setX(r.x()+75);
        r.setWidth(150);
        QString rangeStr = min+"~"+max;
        painter.drawText(r, Qt::AlignCenter, rangeStr);
    }
}



