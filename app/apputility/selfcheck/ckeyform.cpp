#include <QDebug>

#include "../../service/servutility/servutility.h"
#include "../../service/servdso/sysdso.h"
#include "ckeyform.h"
#include "ui_ckeyform.h"

CKeyForm::CKeyForm(QWidget *parent) :
    uiWnd(parent),
    ui(new Ui::CKeyForm)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);

    m_nRunCounter = 0;
    m_nPressCounter = 0;
    addAllKeys();
    createTable();
    loadResource("appUtility/keyform/");
}

CKeyForm::~CKeyForm()
{
    mKeyList.clear();
    foreach(stKeyTestMap*p , m_stKeyMap )
    {
        delete p;
        p = NULL;
    }
    m_stKeyMap.clear();

    delete ui;
}

void CKeyForm::keyReleaseEvent( QKeyEvent *event )
{
    int key = event->key();

    if(key == R_Pkey_RunStop)
    {
        m_nRunCounter++;
        if(3 == m_nRunCounter)
        {
             m_nRunCounter = 0;
             this->hide();
             return;
        }
    }
    else
    {
         m_nRunCounter = 0;
    }

    //qDebug() << key << R_Pkey_FUNC << R_Pkey_FInc <<R_Pkey_FDec;
    if(m_stKeyMap.contains(key))
    {
//        qDebug() << QString().setNum(key,16) << "in";

        stKeyTestMap* pMap = m_stKeyMap[key];
        if(pMap->getKeyType() == KEY_TEST_BTN)
        {
            if(pMap->getSelect())
            {
                pMap->pKey->setStyleSheet(typeBtn_un);
                pMap->setSelect(false);
            }
            else
            {
                pMap->pKey->setStyleSheet(typeBtn);
                pMap->setSelect(true);
            }
        }
        else if(pMap->getKeyType() == KEY_TEST_PUSH)
        {
            if(pMap->getSelect())
            {
                pMap->pKey->setStyleSheet(typePush_un);
                pMap->setSelect(false);
            }
            else
            {
                pMap->pKey->setStyleSheet(typePush);
                pMap->setSelect(true);
            }
            pMap->pKey->setText( "0");
        }
        else if(pMap->getKeyType() == KEY_TEST_DEC)
        {
            if(pMap->getSelect())
            {
                pMap->pKey->setStyleSheet(typeDec_un);
                pMap->setSelect(false);
                //qDebug() << "---";
            }
            else
            {
                pMap->pKey->setStyleSheet(typeDec);
                pMap->setSelect(true);
                //qDebug() << "+++";
            }
            int val = pMap->pValue->text().toInt() - 1;
            pMap->pValue->setText( QString("%1").arg(val));
        }
        else if(pMap->getKeyType() == KEY_TEST_INC)
        {
            if(pMap->getSelect())
            {
                pMap->pKey->setStyleSheet(typeInc_un);
                pMap->setSelect(false);
            }
            else
            {
                pMap->pKey->setStyleSheet(typeInc);
                pMap->setSelect(true);
            }

            int val = pMap->pValue->text().toInt() + 1;
            pMap->pValue->setText( QString("%1").arg(val));
        }
        else if(pMap->getKeyType() == KEY_TEST_BTN_PRE)
        {
            if(pMap->getSelect())
            {
                pMap->pKey->setStyleSheet(typeBtnPre_un);
                pMap->setSelect(false);
            }
            else
            {
                pMap->pKey->setStyleSheet(typeBtnPre);
                pMap->setSelect(true);
            }
        }
        else if(pMap->getKeyType() == KEY_TEST_BTN_PAUSE)
        {
            if(pMap->getSelect())
            {
                pMap->pKey->setStyleSheet(typeBtnPause_un);
                pMap->setSelect(false);
            }
            else
            {
                pMap->pKey->setStyleSheet(typeBtnPause);
                pMap->setSelect(true);
            }
        }
        else if(pMap->getKeyType() == KEY_TEST_BTN_NEX)
        {
            if(pMap->getSelect())
            {
                pMap->pKey->setStyleSheet(typeBtnNex_un);
                pMap->setSelect(false);
            }
            else
            {
                pMap->pKey->setStyleSheet(typeBtnNex);
                pMap->setSelect(true);
            }
        }

        //! has led
        if(pMap->getLed() >= 0)
        {
            int led = pMap->getLed();

            if(key == R_Pkey_RunStop)
            {
               if(m_nRunCounter == 1)
                {
                    serviceExecutor::post( serv_name_utility,
                                           servUtility::cmd_dso_ledoff,
                                           led + 1 );
                }
                else if(m_nRunCounter == 2)
                {
                   serviceExecutor::post( serv_name_utility,
                                          servUtility::cmd_dso_ledoff,
                                          led );
                   led++;
                }
                serviceExecutor::post( serv_name_utility,
                                       servUtility::cmd_dso_ledon,
                                       led );
                //qDebug() << led << pMap->getSelect();
                return;
            }

            if(pMap->getSelect())
            {
                serviceExecutor::post( serv_name_utility,
                                       servUtility::cmd_dso_ledon,
                                       led );
            }
            else
            {
                serviceExecutor::post( serv_name_utility,
                                       servUtility::cmd_dso_ledoff,
                                       led );
            }

            //qDebug() << led << pMap->getSelect();
        }
    }
}

void CKeyForm::mousePressEvent(QMouseEvent *)
{
    if(m_nPressCounter < 2)
    {
        m_nPressCounter++;
    }
    else
    {
        this->hide();
        m_nPressCounter = 0;
    }
}

void CKeyForm::keyPressEvent(QKeyEvent *)
{

}
void CKeyForm::hide()
{
    serviceExecutor::post( serv_name_utility,
                           servUtility::cmd_dso_keytest,
                           false );
    setHidden(true);
}

void CKeyForm::show()
{
    foreach(stKeyTestMap* key, m_stKeyMap)
    {
        if(key->keyType == KEY_TEST_DEC || key->keyType == KEY_TEST_INC)
        {
            key->pKey->setText("");
        }
        else if(key->keyType == KEY_TEST_PUSH)
        {
            key->pKey->setText("0");
        }

        key->setSelect(false);
        key->pKey->setStyleSheet("");
    }

    if(sysCheckLicense(OPT_MSO))
    {
        ui->label_29->show();
    }
    else
    {
        ui->label_29->hide();
    }
    if(sysCheckLicense(OPT_DG))
    {
        ui->label_65->show();
        ui->label_67->show();
    }
    else
    {
        ui->label_65->hide();
        ui->label_67->hide();
    }

    QWidget::show();
}

#include "../../engine/enginemsg.h"

void CKeyForm::loadResource(QString path)
{
    r_meta::CAppMeta rMeta;
    rMeta.getMetaVal(path + "typeInc",    typeInc);
    rMeta.getMetaVal(path + "typeInc_un",    typeInc_un);
    rMeta.getMetaVal(path + "typeDec",    typeDec);
    rMeta.getMetaVal(path + "typeDec_un",    typeDec_un);
    rMeta.getMetaVal(path + "typePush",    typePush);
    rMeta.getMetaVal(path + "typePush_un",    typePush_un);
    rMeta.getMetaVal(path + "typeBtn",    typeBtn);
    rMeta.getMetaVal(path + "typeBtn_un",    typeBtn_un);
    rMeta.getMetaVal(path + "typeBtnPre",    typeBtnPre);
    rMeta.getMetaVal(path + "typeBtnPre_un",    typeBtnPre_un);

    rMeta.getMetaVal(path + "typeBtnPause",    typeBtnPause);
    rMeta.getMetaVal(path + "typeBtnPause_un",    typeBtnPause_un);
    rMeta.getMetaVal(path + "typeBtnNex",    typeBtnNex);
    rMeta.getMetaVal(path + "typeBtnNex_un",    typeBtnNex_un);
}
void CKeyForm::createTable()
{
    m_stKeyMap[R_Pkey_PageOff]=new stKeyTestMap(ui->label_1);
    m_stKeyMap[R_Pkey_F1]=new stKeyTestMap(ui->label_2);
    m_stKeyMap[R_Pkey_F2]=new stKeyTestMap(ui->label_3);
    m_stKeyMap[R_Pkey_F3]=new stKeyTestMap(ui->label_4);
    m_stKeyMap[R_Pkey_F4]=new stKeyTestMap(ui->label_5);
    m_stKeyMap[R_Pkey_F5]=new stKeyTestMap(ui->label_6);
    m_stKeyMap[R_Pkey_F6]=new stKeyTestMap(ui->label_7);
    m_stKeyMap[R_Pkey_F7]=new stKeyTestMap(ui->label_8);
    m_stKeyMap[R_Pkey_PageReturn]=new stKeyTestMap(ui->label_9);

    m_stKeyMap[R_Pkey_FZ]=new stKeyTestMap(ui->label_10,KEY_TEST_PUSH);
    m_stKeyMap[R_Pkey_FDec]=new stKeyTestMap(ui->label_11, KEY_TEST_DEC);
    m_stKeyMap[R_Pkey_FInc]=new stKeyTestMap(ui->label_12, KEY_TEST_INC);
    m_stKeyMap[R_Pkey_FDec]->setValue(ui->label_10);
    m_stKeyMap[R_Pkey_FInc]->setValue(ui->label_10);
    m_stKeyMap[R_Pkey_FZ]->setLedID( DsoEngine::led_func );

    m_stKeyMap[R_Pkey_DEFAULT]=new stKeyTestMap(ui->label_69);
    m_stKeyMap[R_Pkey_CLEAR]=new stKeyTestMap(ui->label_14);
    m_stKeyMap[R_Pkey_AUTO]=new stKeyTestMap(ui->label_15);
    m_stKeyMap[R_Pkey_RunStop]=new stKeyTestMap(ui->label_16);
    m_stKeyMap[R_Pkey_RunStop]->setLedID( DsoEngine::led_run );

    m_stKeyMap[R_Pkey_SINGLE]=new stKeyTestMap(ui->label_17);
    m_stKeyMap[R_Pkey_SINGLE]->setLedID( DsoEngine::led_single );

    m_stKeyMap[R_Pkey_QUICK]=new stKeyTestMap(ui->label_18);
    m_stKeyMap[R_Pkey_MEASURE]=new stKeyTestMap(ui->label_19);
    m_stKeyMap[R_Pkey_ACQUIRE]=new stKeyTestMap(ui->label_20);
    m_stKeyMap[R_Pkey_STORAGE]=new stKeyTestMap(ui->label_21);
    m_stKeyMap[R_Pkey_CURSOR]=new stKeyTestMap(ui->label_22);
    m_stKeyMap[R_Pkey_DISPLAY]=new stKeyTestMap(ui->label_23);
    m_stKeyMap[R_Pkey_UTILITY]=new stKeyTestMap(ui->label_24);

    m_stKeyMap[R_Pkey_WAVE_POS_Z]=new stKeyTestMap(ui->label_25,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_WAVE_POS)]=new stKeyTestMap(ui->label_26,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_WAVE_POS)]=new stKeyTestMap(ui->label_27,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_WAVE_POS)]->setValue(ui->label_25);
    m_stKeyMap[encode_inc(R_Pkey_WAVE_POS)]->setValue(ui->label_25);

    m_stKeyMap[R_Pkey_LA]=new stKeyTestMap(ui->label_29);
    m_stKeyMap[R_Pkey_LA]->setLedID( DsoEngine::led_digital );

    m_stKeyMap[R_Pkey_MATH]=new stKeyTestMap(ui->label_28);
    m_stKeyMap[R_Pkey_MATH]->setLedID( DsoEngine::led_math );

    m_stKeyMap[R_Pkey_REF]=new stKeyTestMap(ui->label_30);
    m_stKeyMap[R_Pkey_REF]->setLedID( DsoEngine::led_ref );

    m_stKeyMap[R_Pkey_DECODE]=new stKeyTestMap(ui->label_31);
    m_stKeyMap[R_Pkey_DECODE]->setLedID( DsoEngine::led_decode );

    m_stKeyMap[R_Pkey_TRIG_MENU]=new stKeyTestMap(ui->label_32);
    m_stKeyMap[R_Pkey_TRIG_MODE]=new stKeyTestMap(ui->label_33);
    m_stKeyMap[R_Pkey_TRIG_FORCE]=new stKeyTestMap(ui->label_34);
    m_stKeyMap[R_Pkey_PLAY_PRE]=new stKeyTestMap(ui->label_35, KEY_TEST_BTN_PRE);
    m_stKeyMap[R_Pkey_PLAY_STOP]=new stKeyTestMap(ui->label_36, KEY_TEST_BTN_PAUSE);
    m_stKeyMap[R_Pkey_PLAY_NEXT]=new stKeyTestMap(ui->label_37, KEY_TEST_BTN_NEX);

    m_stKeyMap[R_Pkey_TIME_SCALE_Z]=new stKeyTestMap(ui->label_38,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_TIME_SCALE)]=new stKeyTestMap(ui->label_39,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_TIME_SCALE)]=new stKeyTestMap(ui->label_40,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_TIME_SCALE)]->setValue(ui->label_38);
    m_stKeyMap[encode_inc(R_Pkey_TIME_SCALE)]->setValue(ui->label_38);


    m_stKeyMap[R_Pkey_HORI_NAGAVITE]=new stKeyTestMap(ui->label_41);
    m_stKeyMap[R_Pkey_HORI_ZOOM]=new stKeyTestMap(ui->label_42);

    m_stKeyMap[R_Pkey_TIME_OFFSET_Z]=new stKeyTestMap(ui->label_43,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_TIME_OFFSET)]=new stKeyTestMap(ui->label_44,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_TIME_OFFSET)]=new stKeyTestMap(ui->label_45, KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_TIME_OFFSET)]->setValue(ui->label_43);
    m_stKeyMap[encode_inc(R_Pkey_TIME_OFFSET)]->setValue(ui->label_43);

    m_stKeyMap[R_Pkey_WAVE_VOLT_Z]=new stKeyTestMap(ui->label_46,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_WAVE_VOLT)]=new stKeyTestMap(ui->label_47,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_WAVE_VOLT)]=new stKeyTestMap(ui->label_48,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_WAVE_VOLT)]->setValue(ui->label_46);
    m_stKeyMap[encode_inc(R_Pkey_WAVE_VOLT)]->setValue(ui->label_46);

    m_stKeyMap[R_Pkey_TRIG_LEVEL_Z]=new stKeyTestMap(ui->label_49,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_TRIG_LEVEL)]=new stKeyTestMap(ui->label_50,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_TRIG_LEVEL)]=new stKeyTestMap(ui->label_51,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_TRIG_LEVEL)]->setValue(ui->label_49);
    m_stKeyMap[encode_inc(R_Pkey_TRIG_LEVEL)]->setValue(ui->label_49);


    m_stKeyMap[R_Pkey_CH1_POS_Z]=new stKeyTestMap(ui->label_52,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH1_POS)]=new stKeyTestMap(ui->label_53,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH1_POS)]=new stKeyTestMap(ui->label_54,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH1_POS)]->setValue(ui->label_52);
    m_stKeyMap[encode_inc(R_Pkey_CH1_POS)]->setValue(ui->label_52);

    m_stKeyMap[R_Pkey_CH2_POS_Z]=new stKeyTestMap(ui->label_55,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH2_POS)]=new stKeyTestMap(ui->label_56,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH2_POS)]=new stKeyTestMap(ui->label_57,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH2_POS)]->setValue(ui->label_55);
    m_stKeyMap[encode_inc(R_Pkey_CH2_POS)]->setValue(ui->label_55);

    m_stKeyMap[R_Pkey_CH3_POS_Z]=new stKeyTestMap(ui->label_58,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH3_POS)]=new stKeyTestMap(ui->label_59,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH3_POS)]=new stKeyTestMap(ui->label_60,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH3_POS)]->setValue(ui->label_58);
    m_stKeyMap[encode_inc(R_Pkey_CH3_POS)]->setValue(ui->label_58);

    m_stKeyMap[R_Pkey_CH4_POS_Z]=new stKeyTestMap(ui->label_61,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH4_POS)]=new stKeyTestMap(ui->label_62,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH4_POS)]=new stKeyTestMap(ui->label_63,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH4_POS)]->setValue(ui->label_61);
    m_stKeyMap[encode_inc(R_Pkey_CH4_POS)]->setValue(ui->label_61);


    m_stKeyMap[R_Pkey_CH1]=new stKeyTestMap(ui->label_64);
    m_stKeyMap[R_Pkey_CH1]->setLedID( DsoEngine::led_ch1 );

    m_stKeyMap[R_Pkey_SOURCE1]=new stKeyTestMap(ui->label_65);
    m_stKeyMap[R_Pkey_SOURCE1]->setLedID( DsoEngine::led_source1 );

    m_stKeyMap[R_Pkey_CH2]=new stKeyTestMap(ui->label_66);
    m_stKeyMap[R_Pkey_CH2]->setLedID( DsoEngine::led_ch2 );

    m_stKeyMap[R_Pkey_SOURCE2]=new stKeyTestMap(ui->label_67);
    m_stKeyMap[R_Pkey_SOURCE2]->setLedID( DsoEngine::led_source2 );

    m_stKeyMap[R_Pkey_CH3]=new stKeyTestMap(ui->label_68);
    m_stKeyMap[R_Pkey_CH3]->setLedID( DsoEngine::led_ch3 );

    m_stKeyMap[R_Pkey_TOUCH]=new stKeyTestMap(ui->label_13);
    m_stKeyMap[R_Pkey_TOUCH]->setLedID( DsoEngine::led_touch );

    m_stKeyMap[R_Pkey_CH4]=new stKeyTestMap(ui->label_70);
    m_stKeyMap[R_Pkey_CH4]->setLedID( DsoEngine::led_ch4 );

    m_stKeyMap[R_Pkey_CH1_VOLT_Z]=new stKeyTestMap(ui->label_71,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH1_VOLT)]=new stKeyTestMap(ui->label_72,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH1_VOLT)]=new stKeyTestMap(ui->label_73,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH1_VOLT)]->setValue(ui->label_71);
    m_stKeyMap[encode_inc(R_Pkey_CH1_VOLT)]->setValue(ui->label_71);

    m_stKeyMap[R_Pkey_CH2_VOLT_Z]=new stKeyTestMap(ui->label_74,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH2_VOLT)]=new stKeyTestMap(ui->label_75,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH2_VOLT)]=new stKeyTestMap(ui->label_76,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH2_VOLT)]->setValue(ui->label_74);
    m_stKeyMap[encode_inc(R_Pkey_CH2_VOLT)]->setValue(ui->label_74);

    m_stKeyMap[R_Pkey_CH3_VOLT_Z]=new stKeyTestMap(ui->label_77,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH3_VOLT)]=new stKeyTestMap(ui->label_78,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH3_VOLT)]=new stKeyTestMap(ui->label_79,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH3_VOLT)]->setValue(ui->label_77);
    m_stKeyMap[encode_inc(R_Pkey_CH3_VOLT)]->setValue(ui->label_77);

    m_stKeyMap[R_Pkey_CH4_VOLT_Z]=new stKeyTestMap(ui->label_80,KEY_TEST_PUSH);
    m_stKeyMap[encode_dec(R_Pkey_CH4_VOLT)]=new stKeyTestMap(ui->label_81,KEY_TEST_DEC);
    m_stKeyMap[encode_inc(R_Pkey_CH4_VOLT)]=new stKeyTestMap(ui->label_82,KEY_TEST_INC);
    m_stKeyMap[encode_dec(R_Pkey_CH4_VOLT)]->setValue(ui->label_80);
    m_stKeyMap[encode_inc(R_Pkey_CH4_VOLT)]->setValue(ui->label_80);

}
