#ifndef CHECK_TOUCH_DLG_H
#define CHECK_TOUCH_DLG_H

#include <QLabel>
#include <QTouchEvent>
#include "mcurve.h"
#include "../../menu/wnd/uiwnd.h"
#include "checktouch2point.h"
#include "../../service/servplot/servplot.h"
#include "../../service/servutility/servutility.h"

#define TOUCHSCREEN_ICON_POINT  QString("../../resource/pictures/appselfcheck/point.jpg")
#define TOUCHSCREEN_ICON_LINE   QString("../../resource/pictures/appselfcheck/drawline.jpg")
#define rWidth  41
#define rHeight 40

class CheckTouchDlg : public menu_res::uiWnd
{
    Q_OBJECT
public:
    CheckTouchDlg( QWidget *parent = 0 );
    ~CheckTouchDlg();

    void init();
    void reset();

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent( QKeyEvent *event );

private:
    void setTouched(QPoint p);

private:

    CheckTouch2Point *checkTouch2Point;

    QList<QRect >   mRectList;
    QList<bool >    touchedList;

    QPoint beginPoint;
    QPoint endPoint;

    QList<MCurve *> shapes;
    MCurve *currentShape;

    bool isButtonPressed;
    int    mRunStop ;

};
#endif // CHECK_TOUCH_DLG_H

