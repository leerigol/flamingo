#ifndef MCURVE_H
#define MCURVE_H

#include <QPainter>
#include <QVector>
#include <QPointF>
#include <QPolygon>
#include <QWidget>
class MCurve
{
public:
    MCurve();

    QPoint getBeginPoint()
    {
        return beginPoint;
    }

    void setBeginPoint(QPoint point)
    {
        beginPoint = point;
    }

    QPoint getEndPoint()
    {
        return endPoint;
    }

    void setEndPoint(QPoint point)
    {
        endPoint = point;
    }

    void addPoint(QPoint point);
    void draw(QPainter &painter);
    void clearPoints();
    int  countNum();

private:
    QPoint beginPoint;
    QPoint endPoint;
    QVector<QPoint> points;
};

#endif // MCURVE_H
