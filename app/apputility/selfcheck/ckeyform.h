#ifndef CKEYFORM_H
#define CKEYFORM_H

#include <QKeyEvent>
#include <QLabel>

#include "../../menu/wnd/uiwnd.h"

namespace Ui {
class CKeyForm;
}

enum
{
    KEY_TEST_DEC,
    KEY_TEST_INC,
    KEY_TEST_PUSH,
    KEY_TEST_BTN,
    KEY_TEST_BTN_PRE,
    KEY_TEST_BTN_PAUSE,
    KEY_TEST_BTN_NEX,
};
struct stKeyTestMap
{
    stKeyTestMap(QLabel* key, int t = KEY_TEST_BTN)
    {
        keyType = t;
        pKey = key;
        bSelect=false;
        pValue = NULL;
        led    = -1;
    }
    void setValue(QLabel* key)
    {
        pValue = key;
    }

    void setLedID(int i)
    {
        led = i;
    }


    int getLed()
    {
        return led;
    }


    void setSelect(bool b=true)
    {
        bSelect = b;
    }

    bool getSelect()
    {
        return bSelect;
    }

    int getKeyType()
    {
        return keyType;
    }

    int     keyType;
    bool    bSelect;
    int     led;
    bool    on_off;
    QLabel* pKey;
    QLabel* pValue;
};
class CKeyForm : public menu_res::uiWnd
{
    Q_OBJECT

public:

    explicit CKeyForm(QWidget *parent = 0);
    ~CKeyForm();


    void mousePressEvent(QMouseEvent *);

    void show();
    void hide();


protected:
    void keyPressEvent( QKeyEvent *event );
    void keyReleaseEvent( QKeyEvent *event );

private:
    QMap<int,stKeyTestMap*>  m_stKeyMap;
    void loadResource(QString path);
    void createTable();

    int  m_nRunCounter;
    int  m_nPressCounter;


    QString typeInc;
    QString typeDec;
    QString typePush;
    QString typeBtn;
    QString typeBtnPre;
    QString typeBtnPause;
    QString typeBtnNex;

    QString typeInc_un;
    QString typeDec_un;
    QString typePush_un;
    QString typeBtn_un;
    QString typeBtnPre_un;
    QString typeBtnPause_un;
    QString typeBtnNex_un;
private:
    Ui::CKeyForm *ui;
};

#endif // CKEYFORM_H
