#include "checktouch2point.h"

CheckTouch2Point::CheckTouch2Point(QWidget *parent) : uiWnd(parent)
{
    this->setGeometry(0,0,1024,600);
    setAttribute(Qt::WA_AcceptTouchEvents );

    toWidth = 0.0;
    toHeight = 0.0;
    mRunStop = 0;
    mScale = "100%";
    mTouchPointsNum = 0;
    mSec = 0;
    pList.clear();

    loadResource("appUtility/touchpic/");

    addAllKeys();
}

void CheckTouch2Point::reset()
{
    mScale = "100%";
    toWidth = img.width();
    toHeight = img.height();
}

void CheckTouch2Point::loadResource(QString path)
{
    r_meta::CAppMeta rMeta;
    QString scr;
    rMeta.getMetaVal(path + "pinchImg",     scr);
    pinchImg.load(scr);
    rMeta.getMetaVal(path + "stretchImg",   scr);
    stretchImg.load(scr);
    rMeta.getMetaVal(path + "img",          scr);

    img.load(scr);
    toWidth = img.width();
    toHeight = img.height();
}

void CheckTouch2Point::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.fillRect(geometry(),Qt::black);

    QImage a = img.scaled(toWidth, toHeight, Qt::KeepAspectRatio);

    int tw = a.width();
    int th = a.height();
    painter.drawImage((geometry().width()-tw)/2, (geometry().height()-th)/2, a);

    painter.setPen(QPen(QColor(60,60,60,255),1,Qt::SolidLine));
    QFont font;
    font.setPointSize(14);
    painter.setFont(font);
    painter.drawText(360,560,tr("Press the 'RUN/STOP' 3 times to exit."));

    painter.drawText(40, 40, mScale);
    painter.drawText(100, 40, QString::number(mTouchPointsNum)+" points");

    painter.drawText(890, 510, "Pinch");
    painter.drawText(960, 510, "Stretch");
    painter.drawImage(890 ,530, pinchImg);
    painter.drawImage(963 ,530, stretchImg);

    painter.setPen(QPen(Qt::green, 2, Qt::SolidLine));
    for(int i=0; i< pList.count(); i++)
    {
        QPointF p = pList.at(i);
        painter.drawEllipse(p, 30, 30);
        if(mTime.elapsed() && mSec == 0)
        {
            mSec = mTime.elapsed()+25;
        }
        p += QPointF(-20, 5);
        QString time = QString::number(mSec).append("ms");
        QFont font;
        font.setPointSize(10);
        painter.setFont(font);
        painter.drawText(p, time );
    }
}

bool CheckTouch2Point::event(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::TouchBegin:
    {
        mTime.start();
        QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
        QList<QTouchEvent::TouchPoint> touchPoints = touchEvent->touchPoints();
        mTouchPointsNum = touchPoints.count();
        pList.clear();
        for(int i=0;i<mTouchPointsNum;i++)
            pList.append(touchPoints.at(i).pos());
        update();
        return true;
    }
    case QEvent::TouchUpdate:
    {
        QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
        QList<QTouchEvent::TouchPoint> touchPoints = touchEvent->touchPoints();
        mTouchPointsNum = touchPoints.count();
        pList.clear();
        for(int i=0;i<mTouchPointsNum;i++)
            pList.append(touchPoints.at(i).pos());
        update();
        if (touchPoints.count() == 2)
        {
            oriLineF.setPoints(touchPoints.first().lastPos(), touchPoints.last().lastPos());
            QLineF nowLineF(touchPoints.first().pos(), touchPoints.last().pos());
            qreal mRatio = ( nowLineF.length()/oriLineF.length());

            if(mRatio < 1){
                if(toWidth/img.width() > 0.27){
                    toWidth = toWidth*0.9;
                    toHeight = toHeight*0.9;
                }else{
                    toWidth = img.width()*0.25;
                    toHeight = img.width()*0.25;
                }
            }else if(mRatio > 1){
                if(toWidth/img.width() < 4.8){
                    toWidth = toWidth*1.1;
                    toHeight = toHeight*1.1;
                }else{
                    toWidth = img.width()*5;
                    toHeight = img.width()*5;
                }
            }
            mScale = QString::number(toWidth/img.width()*100,'f',0) + "%";
            update();
        }
        break;
    }
    case QEvent::TouchEnd:
        mTouchPointsNum = 0;
        pList.clear();
        mSec = 0;
        update();
        break;

    default:
        break;
    }
    return uiWnd::event(event);
}

void CheckTouch2Point::keyPressEvent(QKeyEvent *event)
{
    if(R_Pkey_RunStop == event->key())
    {
        mRunStop++;
        if(3 == mRunStop)
        {
            this->hide();
            mRunStop = 0;
        }
    }
}
