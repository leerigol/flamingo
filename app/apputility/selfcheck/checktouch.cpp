#include "../../service/servdso/sysdso.h"

#include "../../service/servutility/servutility.h"

#include "checktouch.h"
#include <qmath.h>

CheckTouchDlg::CheckTouchDlg(QWidget *parent):uiWnd(parent)
{
    this->setWindowFlags(Qt::FramelessWindowHint);

    currentShape = NULL;
    mRunStop = 0;
    addAllKeys();

    init();
}

CheckTouchDlg::~CheckTouchDlg()
{

}

void CheckTouchDlg::init()
{
    mRectList.clear();
    touchedList.clear();
    for(int wTemp=0; wTemp<1025/rWidth; wTemp++){
        for(int hTemp=0; hTemp<600/rHeight; hTemp++ ){
            if(wTemp ==0 || wTemp ==13
                    || wTemp ==24 || hTemp == 0
                    || hTemp == 7 || hTemp == 14){
                mRectList.append(QRect(wTemp*rWidth, hTemp*rHeight, rWidth, rHeight));
                touchedList.append(false);
            }
        }
    }
    checkTouch2Point = new CheckTouch2Point(sysGetMainWindow());
    checkTouch2Point->hide();
}

void CheckTouchDlg::reset()
{    
    mRunStop = 0;
    for(int i=0; i<touchedList.count(); i++){
        touchedList[i] = false;
    }
    for(int i=0; i<shapes.count();i++)
    {
        MCurve * curve = shapes.at(i);
        curve->clearPoints();
        delete curve;
    }
    shapes.clear();
}

void CheckTouchDlg::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.fillRect(0,0,geometry().width(),geometry().height(),QColor(255,255,255,255));
    painter.setPen(QPen(QColor(60,60,60,255), 1, Qt::SolidLine));
    for(int i=0; i<mRectList.count(); i++){
        if(touchedList.at(i))
            painter.fillRect(mRectList.at(i),Qt::green);

        if(mRectList.at(i).x() ==0 || mRectList.at(i).x() ==13*41
                || mRectList.at(i).x() ==24*41 || mRectList.at(i).y() == 0
                || mRectList.at(i).y() == 7*40 || mRectList.at(i).y() == 14*40)
            painter.drawRect(mRectList.at(i));
    }

    foreach(MCurve *shape, shapes)
    {
        painter.setPen(QPen(Qt::black, 2, Qt::SolidLine));
        shape->draw(painter);
    }

    painter.setPen(QPen(QColor(60,60,60,255),1,Qt::SolidLine));
    QFont font;
    font.setPointSize(14);
    painter.setFont(font);
    painter.drawText(120,480,tr("Press the 'SIGNLE' to the next screen."));
    painter.drawText(120,520,tr("Press the 'RUN/STOP' 3 times to exit."));
}

void CheckTouchDlg::mousePressEvent(QMouseEvent *event)
{
    currentShape = new MCurve;
    currentShape->setBeginPoint(event->pos());
    setTouched(event->pos());
    shapes.append(currentShape);

    endPoint = event->pos();
    isButtonPressed = true;
}

void CheckTouchDlg::mouseReleaseEvent(QMouseEvent *event)
{
    currentShape->setEndPoint(event->pos());
    isButtonPressed = false;
}

void CheckTouchDlg::mouseMoveEvent(QMouseEvent *event)
{
    if(isButtonPressed){
        int tempX = event->pos().x()-endPoint.x();
        int tempY = event->pos().y()-endPoint.y();

        if(tempX/25 || tempY/25){
            int max =qAbs(tempX)  > qAbs(tempY) ? qAbs(tempX) : qAbs(tempY);
            int num = (max+39)/20;
            for(int i=1; i<num; i++){
                QPoint tempP = QPoint(endPoint.x()+tempX/num*i, endPoint.y()+tempY/num*i);
                currentShape->addPoint(tempP);
                setTouched(tempP);
            }
        }
        setTouched(event->pos());

        currentShape->addPoint(event->pos());
        endPoint = event->pos();
        update(event->pos().x()-200,event->pos().y()-200,400,400);
    }
}

void CheckTouchDlg::keyPressEvent(QKeyEvent *event)
{
    if(R_Pkey_RunStop == event->key())
    {
        mRunStop++;
        if(3 == mRunStop)
        {
            this->hide();
            mRunStop = 0;
            serviceExecutor::post( serv_name_utility,MSG_APP_UTILITY_TOUCH_TEST,false);
        }
    }
    else if(R_Pkey_SINGLE == event->key())
    {
        this->hide();
        mRunStop = 0;
        serviceExecutor::post(serv_name_utility, MSG_APP_UTILITY_TOUCH_TEST,false);
        checkTouch2Point->reset();
        checkTouch2Point->show();
    }
    else
    {
        //! do nothing
    }
}

void CheckTouchDlg::setTouched(QPoint p)
{
    for(int i=0; i<mRectList.count(); i++)
        if(mRectList.at(i).contains(p)){
            touchedList[i] = true;
            update(p.x()-200,p.y()-200,400,400);
        }
}
