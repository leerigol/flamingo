#ifndef CHECK_FAN_DLG_H
#define CHECK_FAN_DLG_H

#include <QLabel>
#include <QProgressBar>
#include "../../menu/wnd/rinfownd.h"

class CheckFanDlg : public menu_res::RInfoWnd
{
    Q_OBJECT
public:
    CheckFanDlg( QWidget *parent = 0 );
    ~CheckFanDlg();

    void setSpeed(int s=100);
public slots:
    void sendSpeed();

protected:
    void adjustFanSpeed(int key);

    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);

    void setSlideStyle();

private:
    int    mSpeed;
    int    mRunCount;
    QLabel mReminder;
    QLabel mDescription;
    QSlider *mSlider;
};
#endif // CHECK_FAN_DLG_H

