#include "./shutdown.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "../../service/servicefactory.h"

menu_res::RModelWnd_Style Shutdown::_bgStyle;
Shutdown::Shutdown( QWidget */*parent*/)
{
    addAllKeys();

    set_attr( wndAttr, mWndAttr, wnd_moveable );
    setupUI();
}

Shutdown::~Shutdown()
{
}

void Shutdown::loadResource(QString path)
{
    r_meta::CRMeta rMeta;
    rMeta.getMetaVal(path + "closerect",    closeRect);
    rMeta.getMetaVal(path + "titlerect",    titleRect);
    rMeta.getMetaVal(path + "shutdown",    shutdownR);
    rMeta.getMetaVal(path + "restart",      restartR);
    rMeta.getMetaVal(path + "shutdownpic",   shutdownPic);
    rMeta.getMetaVal(path + "restartpic",    restartPic);
    rMeta.getMetaVal(path + "shutdownstrr",   shutdownStrR);
    rMeta.getMetaVal(path + "restartstrr",    restartStrR);

    QRect geo;
    rMeta.getMetaVal(path + "geo",  geo);
    setGeometry(geo);
}
void Shutdown::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QRect frameRect = rect();

    _bgStyle.paint( painter, frameRect );

    painter.setPen(QPen(QColor(192,192,192,255)));
    painter.drawText(shutdownStrR, Qt::AlignCenter, shutdownStr);
    painter.drawText(restartStrR, Qt::AlignCenter, restartStr);
    painter.drawText(titleRect, titleStr);

}

void Shutdown::setupUI()
{
    loadResource("ui/shutdown/");
    _bgStyle.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(closeRect);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    pShutdownBtn = new RImageButton(this);
    Q_ASSERT( pShutdownBtn != NULL );
    pShutdownBtn->setGeometry(shutdownR);
    pShutdownBtn->setImage( shutdownPic,
                               shutdownPic,
                               shutdownPic );

    pRestartBtn = new RImageButton(this);
    Q_ASSERT( pRestartBtn != NULL );
    pRestartBtn->setGeometry(restartR);
    pRestartBtn->setImage( restartPic,
                               restartPic,
                               restartPic );


    connect( m_pCloseButton, SIGNAL(released()),
             this, SLOT(close()) );
    connect( pShutdownBtn, SIGNAL(released()),
             this, SLOT(onShutdown()) );
    connect( pRestartBtn, SIGNAL(released()),
             this, SLOT(onRestart()) );

    shutdownStr = sysGetString(MSG_APP_UTILITY_SHUTDOWN,"Shutdown");
    restartStr  = sysGetString(MSG_APP_UTILITY_RESTART,"Restart");
    titleStr    = restartStr;

}

void Shutdown::onShutdown()
{
    serviceExecutor::post( E_SERVICE_ID_UTILITY,
                           servUtility::cmd_dso_shutdown, 0 );
}

void Shutdown::onRestart()
{
    serviceExecutor::post( E_SERVICE_ID_UTILITY,
                           servUtility::cmd_dso_restart, 0 );
}



