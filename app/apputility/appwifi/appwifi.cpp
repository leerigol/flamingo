#include "appwifi.h"
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../gui/cguiformatter.h"
#include "../appmain/cappmain.h"

IMPLEMENT_CMD( CApp, CAppWifi )
start_of_entry()

end_of_entry()

CAppWifi::CAppWifi()
{
    mservName = serv_name_utility_Wifi;
}

void CAppWifi::setupUi()
{
}

void CAppWifi::retranslateUi()
{}

void CAppWifi::buildConnection()
{}

