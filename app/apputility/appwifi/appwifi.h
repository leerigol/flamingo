#ifndef APPWIFI_H
#define APPWIFI_H

#include "../../app/capp.h"
#include "../../include/dsotype.h"
#include <QMessageBox>

class CAppWifi : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppWifi();
    virtual void setupUi();
    virtual void retranslateUi();
    virtual void buildConnection();
protected:

private:

};
#endif // APPWIFI_H

