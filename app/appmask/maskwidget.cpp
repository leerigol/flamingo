#include"maskwidget.h"
#include "../../include/dsodbg.h"
CMaskWidget::CMaskWidget(QWidget *parent) : menu_res::uiWnd(parent)
{
    myDivs = 8;
    myGnd = 240;

    mFgTimeout = 2000;
    mFgColor = Qt::white;
    mBgColor = Qt::blue;

    mpFgTimer = NULL;

    mpFgTimer = new QTimer(this);
    Q_ASSERT( NULL != mpFgTimer );
    mpFgTimer->setSingleShot( true );

    connect( mpFgTimer, SIGNAL(timeout()),
             this, SLOT(onTimeout()) );

    //! vars
    mpFgRule = NULL;
    mpBgRule = NULL;
    mbFgVisible = false;
    mbBgVisible = false;

    m_pLineA = NULL;
    m_pLineB = NULL;
}
CMaskWidget::~CMaskWidget()
{}

void CMaskWidget::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent( event );

    QPainter painter(this);

    if ( mbBgVisible && mpBgRule != NULL )
    {
        paintBgMask( mpBgRule, &painter );
    }

    if ( mbFgVisible && mpFgRule != NULL )
    {
        paintFgMask( mpFgRule, &painter );
    }
}

void CMaskWidget::resizeEvent(QResizeEvent *event)
{
    menu_res::uiWnd::resizeEvent( event );

    myHeight = rect().height();
    myGnd = myHeight/2;

    myTop = 0;
    myBase = myHeight - 1;
}

void CMaskWidget::setupUi( const QString &path,
                      const r_meta::CMeta &meta )
{
    m_pLineA = new menu_res::RVLine(this);
    Q_ASSERT( NULL != m_pLineA );
    m_pLineA->hide();
    m_pLineB = new menu_res::RVLine(this);
    Q_ASSERT( NULL != m_pLineB );
    m_pLineB->hide();

    QRect selfRect;
    meta.getMetaVal( path + "geo", selfRect );
    getGeo( meta, path + "geo", this );

    meta.getMetaVal( path + "color/fg", mFgColor );
    meta.getMetaVal( path + "color/bg", mBgColor );

    int lineW;
    meta.getMetaVal( path + "line/w", lineW );
    m_pLineA->setGeometry(0,0,lineW, selfRect.height() );
    m_pLineB->setGeometry(0,0,lineW, selfRect.height() );

    getStyle( meta, path + "line/style", m_pLineA );
    getStyle( meta, path + "line/style", m_pLineB );
}

void CMaskWidget::onTimeout()
{
    if ( mbFgVisible )
    {
        mbFgVisible = false;
        update();
    }

    if ( m_pLineA->isVisible() )
    { m_pLineA->hide(); }

    if ( m_pLineB->isVisible() )
    { m_pLineB->hide(); }
}

void CMaskWidget::showFgRule( bool bShow, EngineMaskRule *pRule )
{
    Q_ASSERT( NULL != pRule );

    Q_ASSERT( NULL != mpFgTimer );

    mbFgVisible = bShow;
    mpFgRule = pRule;

    update();

    mpFgTimer->setSingleShot( true );
    mpFgTimer->start( mFgTimeout );
}
void CMaskWidget::showBgRule( bool bShow, EngineMaskRule *pRule )
{
    Q_ASSERT( NULL != pRule );

    mbBgVisible = bShow;
    mpBgRule = pRule;

    update();
}

void CMaskWidget::showRangeLine( int a, int b )
{
    mpFgTimer->start( mFgTimeout );

    m_pLineA->show();
    m_pLineB->show();

    m_pLineA->move( a, 0 );
    m_pLineB->move( b, 0 );
}

//! return: length
int CMaskWidget::createPath( QPainterPath *pPath,
                             EngineMaskRule *pMask,
                              MaskRule *pRule )
{
    Q_ASSERT( NULL != pPath );
    Q_ASSERT( NULL != pRule );
    Q_ASSERT( NULL != pRule );

    //! not a line
    if ( pRule->mLen < 2 )
    { return 0; }

    //! origin
    pPath->moveTo( pRule->mOffset,
                   ruleToyPixel( pMask, pRule->mRule[pRule->mOffset] ) );
    for ( int i = pRule->mOffset + 1; i < pRule->mLen; i++ )
    {
        pPath->lineTo( i,
                       ruleToyPixel( pMask, pRule->mRule[i])
                       );
    }

    return pRule->mLen;
}

void CMaskWidget::paintFgRule( EngineMaskRule *pMask,
                               MaskRule *pRule,
                               QPainter *painter )
{
    Q_ASSERT( NULL != pMask );
    Q_ASSERT( NULL != pRule );
    Q_ASSERT( NULL != painter );

    QPainterPath paintPath;
    int pts;
LOG_DBG()<<pMask->myGnd<<pMask->myDiv.M()<<pMask->myDiv.N();
    pts = createPath( &paintPath, pMask, pRule );
    if ( pts < 2 )
    { return; }

    painter->save();
    painter->setPen( mFgColor );
    painter->drawPath( paintPath );
    painter->restore();
}
void CMaskWidget::paintBgRule( EngineMaskRule *pMask,
                               MaskRule *pRule,
                               QPainter *painter )
{
    Q_ASSERT( NULL != pRule );
    Q_ASSERT( NULL != pRule );
    Q_ASSERT( NULL != painter );

    QPainterPath paintPath;
    int pts;

    pts = createPath( &paintPath, pMask, pRule );
    if ( pts < 2 )
    { return; }

    paintPath.closeSubpath();

    painter->save();
    painter->fillPath( paintPath, mBgColor );
    painter->restore();

}

void CMaskWidget::paintFgMask(
                               EngineMaskRule *pMask,
                               QPainter *painter )
{
    Q_ASSERT( NULL != pMask );
    Q_ASSERT( NULL != painter );

    if ( pMask->m_pRules[0] != NULL && pMask->mRuleValidates[0] )
    { paintFgRule( pMask, pMask->m_pRules[0], painter ); }
    if ( pMask->m_pRules[3] != NULL && pMask->mRuleValidates[3] )
    { paintFgRule( pMask, pMask->m_pRules[3], painter ); }
}

void CMaskWidget::paintBgMask(
                               EngineMaskRule *pMask,
                               QPainter *painter )
{
    Q_ASSERT( NULL != pMask );
    Q_ASSERT( NULL != painter );

    if ( pMask->m_pRules[0] != NULL && pMask->mRuleValidates[0] )
    { paintBgRule( pMask, pMask->m_pRules[0], painter ); }
    if ( pMask->m_pRules[3] != NULL && pMask->mRuleValidates[3] )
    { paintBgRule( pMask, pMask->m_pRules[3], painter ); }
}

//! (adc - gnd)/adcdiv = pixel / (height/ydiv)
int CMaskWidget::ruleToyPixel( EngineMaskRule *pMask,
                int adc
                 )
{
    Q_ASSERT( NULL != pMask );

    int pix;

    //offset = ( adc_center - pMask->myGnd ) * myHeight*pMask->myDiv.mN/pMask->myDiv.mM/myDivs;
    //pix = (adc - pMask->myGnd)*myHeight*pMask->myDiv.mN/pMask->myDiv.mM/myDivs;
    //pix = myGnd - pix + offset;

    pix = ( adc_center - adc ) * myHeight*pMask->myDiv.mN/pMask->myDiv.mM/myDivs;
    pix = myGnd + pix;

    if ( pix < myTop )
    { pix = myTop; }
    else if ( pix > myBase )
    { pix = myBase; }
    else
    {}

    return pix;
}


