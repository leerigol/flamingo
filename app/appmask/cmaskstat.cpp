#include <QPainter>
#include "cmaskstat.h"

#include "../../meta/crmeta.h"
#include "../../gui/cguiformatter.h"


CMaskStat::CMaskStat(QWidget *parent) : menu_res::RModalWnd( parent )
{
    for ( int i = 0; i < array_count(m_pLabels); i++ )
    {
        m_pLabels[ i ] = NULL;
    }

    set_attr( wndAttr, mWndAttr, wnd_moveable );
    set_attr( wndAttr, mWndAttr, wnd_manual_resize );
}

CMaskStat::~CMaskStat()
{
}

void CMaskStat::showEvent(QShowEvent *event )
{
    //! base
    m_pLabels[0]->setText( sysGetString( IDS_MASK_MATCH, "Fail" ) );
    m_pLabels[2]->setText( sysGetString( IDS_MASK_MISCC, "Pass" ) );
    m_pLabels[4]->setText( sysGetString( IDS_MASK_TOTAL, "Total") );
    RModalWnd::showEvent( event );
}

void CMaskStat::paintEvent(QPaintEvent *event)
{
    //! paint frame
    RModalWnd::paintEvent( event );

    //! paint child
    QWidget::paintEvent( event );
}

void CMaskStat::retranslateUi()
{
    m_pLabels[0]->setText( sysGetString( IDS_MASK_MATCH, "Fail" ) );
    m_pLabels[2]->setText( sysGetString( IDS_MASK_MISCC, "Pass" ) );
    m_pLabels[4]->setText( sysGetString( IDS_MASK_TOTAL, "Total") );

    update();
}

void CMaskStat::setupUi(const QString &path,
                      const r_meta::CMeta &meta)
{
    //! create sub controls
    for ( int i = 0; i < array_count(m_pLabels); i++ )
    {
        m_pLabels[i] = new QLabel(this);

        Q_ASSERT( NULL != m_pLabels[i] );
    }

    //! set geo && style
    QString strPath;
    for ( int i = 0; i < array_count(m_pLabels); i++ )
    {
        //! path/label0/geo
        strPath = QString("%1label%2/geo").arg(path).arg(i);
        getGeo( meta, strPath, m_pLabels[i] );

        strPath = QString("%1label%2/style").arg(path).arg(i);
        getStyle( meta, strPath, m_pLabels[i] );
    }

    //! wnd
    getGeo( meta, path + "geo", this );
    getStyle( meta, path + "style", this );

    //! color
    m_pLabels[0]->setStyleSheet("color:rgb(255,0,0);");
    m_pLabels[1]->setStyleSheet("color:rgb(255,0,0);");
    m_pLabels[2]->setStyleSheet("color:rgb(11,230,16);");
    m_pLabels[3]->setStyleSheet("color:rgb(11,230,16);");
    m_pLabels[4]->setStyleSheet("color:white;");
    m_pLabels[5]->setStyleSheet("color:white;");
}

void CMaskStat::setValue( quint64 match,
                          quint64 miss,
                          quint64 total,
                          float  sigma )
{
    QString strValue;

    gui_fmt::CGuiFormatter::formatInteger( strValue, match, Unit_wfm );
    m_pLabels[1]->setText( strValue );

    gui_fmt::CGuiFormatter::formatInteger( strValue, miss, Unit_wfm );
    m_pLabels[3]->setText( strValue );

    gui_fmt::CGuiFormatter::formatInteger( strValue, total, Unit_wfm );
    m_pLabels[5]->setText( strValue );

    gui_fmt::CGuiFormatter::format( strValue, sigma );
//    m_pLabels[7]->setText( strValue );

    update();
}



