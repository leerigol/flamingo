#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../gui/cguiformatter.h"
#include "../appmain/cappmain.h"
#include "cappmask.h"

IMPLEMENT_CMD( CApp, CAppMask )
start_of_entry()

on_set_void_void(MSG_MASK_ENABLE,          &CAppMask::on_mask_changed ),
on_set_void_void(MSG_MASK_CREATE,          &CAppMask::on_mask_changed ),
on_set_void_void( servMask::cmd_mask_load, &CAppMask::on_mask_changed ),

on_set_int_int(MSG_MASK_X_MASK,       &CAppMask::on_imm_rule_changed),
on_set_int_int(MSG_MASK_Y_MASK,       &CAppMask::on_imm_rule_changed),
on_set_int_int(MSG_MASK_RANGE,        &CAppMask::on_imm_rule_changed),

on_set_void_void(MSG_MASK_RANGE_A,     &CAppMask::on_rangeline_changed),
on_set_void_void(MSG_MASK_RANGE_B,     &CAppMask::on_rangeline_changed),
on_set_void_void(MSG_MASK_RANGE_AB,    &CAppMask::on_rangeline_changed),

on_set_void_void(MSG_MASK_SOURCE,      &CAppMask::on_src_changed ),

on_set_void_void(MSG_MASK_SHOW_STAT,    &CAppMask::onShowStat),
on_set_void_void(MSG_MASK_ENABLE,       &CAppMask::onShowStat ),
on_set_void_void(MSG_MASK_RESET_STAT,   &CAppMask::onShowStat),

on_set_void_void( servMask::cmd_mask_update_report, &CAppMask::on_report_changed ),

end_of_entry()

CAppMask::CAppMask()
{
    mservName = serv_name_mask;

    //! quick en
    addQuick( MSG_APP_MASK,
              QStringLiteral("ui/desktop/mask/icon/") );

    //! quick opened
    addQuick( MSG_APP_MASK,
              QStringLiteral("ui/desktop/mask/icon_open/"),
              1,
              app_opened );

    m_pMaskStat = NULL;
    m_pMaskWidget = NULL;
}

void CAppMask::setupUi( CAppMain *pMain )
{
    m_pMaskStat   = new CMaskStat(pMain->getCentBar());
    Q_ASSERT(NULL != m_pMaskStat);

    m_pMaskWidget = new CMaskWidget( pMain->getBackground() );
    Q_ASSERT( NULL != m_pMaskWidget );

    r_meta::CAppMeta meta;
    m_pMaskStat->setup("appmask/wndstat/", meta);
    m_pMaskStat->setTitle( MSG_APP_MASK /*"Pass/Fail"*/);
}

void CAppMask::retranslateUi()
{
    m_pMaskStat->retranslateUi();

    if(m_pMaskStat->isVisible())
    {
        m_pMaskStat->redraw();
    }
}

void CAppMask::buildConnection()
{
    Q_ASSERT(NULL != m_pMaskStat);

    //! connect
    connect( m_pMaskStat, SIGNAL(sigClose()),
             this, SLOT(on_wndstat_close()) );
}

void CAppMask::registerSpy()
{

}

void CAppMask::on_wndstat_close()
{
    serviceExecutor::post( mservName,
                           MSG_MASK_SHOW_STAT,
                           false );
}

void  CAppMask::on_rangeline_changed()
{
    enter_app_context();

    int b;
    _err = query( MSG_MASK_RANGE, b );
    if ( _err != ERR_NONE ){ return; }
    if ( b != mask_range_user )
    { return; }

    //! a, b
    query_enum( mservName, MSG_MASK_RANGE_A, int, rngA );
    if ( !_bOK ){ return; }
    query_enum( mservName, MSG_MASK_RANGE_B, int, rngB );
    if ( !_bOK ){ return; }
    m_pMaskWidget->showRangeLine( rngA, rngB );

    exit_app_context();
}

DsoErr CAppMask::on_imm_rule_changed(AppEventCause aec)
{
//    qDebug()<<"CAppMask::on_imm_rule_changed  aec = "<<aec;
    if( aec != cause_value)
    {
        return ERR_NONE;
    }

    bool b;
    query( MSG_MASK_ENABLE, b );
    if (!b)
    {
        return ERR_NONE;
    }

    CArgument argI, argO;
    argI.setVal( (pointer)&mImmRule );
    query( servMask::cmd_mask_imm_rule, argI, argO );

    m_pMaskWidget->showFgRule( true, &mImmRule );
    return ERR_NONE;
}

void CAppMask::on_mask_changed()
{
//    qDebug()<<"CAppMask::on_mask_changed()";
    bool b;
    query( MSG_MASK_ENABLE, b );

    //! app status
    if (!b)
    {
        m_pMaskWidget->setVisible( false );
        setState( app_installed );
        return;
    }
    else
    {
        setState( app_opened );
    }

    //! create
    r_meta::CAppMeta meta;
    if ( b && m_pMaskWidget->setup("appmask/wndmask/", meta) )
    {
    }

    CArgument argI, argO;
    argI.setVal( (pointer)&mMask); LOG_DBG()<<((pointer)&mMask);
    query( servMask::cmd_mask_mask, argI, argO );

    LOG_DBG()<<mMask.m_pRules[0]->mRule[1]<<mMask.m_pRules[3]->mRule[1];

    m_pMaskWidget->showBgRule( true, &mMask );
    m_pMaskWidget->setVisible( true );
}

void CAppMask::on_src_changed()
{
    enter_app_context();
    query_enum( mservName, MSG_MASK_SOURCE, Chan, ch );
    if ( !_bOK ){ return; }

    dsoVert *pVert = dsoVert::getCH( ch );
    Q_ASSERT( NULL != pVert );

    m_pMaskStat->setTitleColor( pVert->getColor() );

    exit_app_context();
}

void CAppMask::onShowStat( )
{
    bool b;
    //! mask enable
    query( MSG_MASK_ENABLE, b );
    if (!b)
    {
        m_pMaskStat->setVisible(false);
        return;
    }

    //! view wnd
    query(MSG_MASK_SHOW_STAT, b);
    m_pMaskStat->setVisible(b);

    LOG_DBG()<<" m_pMaskStat->isSetuped(): "<<m_pMaskStat->isSetuped();
    //! change visible
    if ( m_pMaskStat->isSetuped() )
    { m_pMaskStat->setVisible(b);}

    //!update report
    on_report_changed();
}

void CAppMask::on_source_changed()
{}

void CAppMask::on_report_changed( )
{
    if ( !m_pMaskStat->isVisible() )
    { return; }

    DsoErr err;

    pointer ptr;
    err = query( mservName, servMask::cmd_mask_report, ptr );
    if ( err != ERR_NONE || NULL == ptr )
    { return; }

    maskReport *pReport;
    pReport = static_cast<maskReport*>( ptr );
    m_pMaskStat->setValue( pReport->mEvent,
                           pReport->mTotal - pReport->mEvent,
                           pReport->mTotal,
                           0 );

//    m_pMaskStat->setValue( 281474976710655,
//                           281474976710655,
//                           281474976710655,
//                           0 ); //!u64Max

}
