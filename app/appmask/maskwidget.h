#ifndef MASKWIDGET_H
#define MASKWIDGET_H

#include<QtWidgets>

#include "../../engine/base/enginecfg.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../widget/rline.h"

class CMaskWidget : public menu_res::uiWnd
{
    Q_OBJECT

public:
    CMaskWidget(QWidget *parent = 0);
    ~CMaskWidget();
protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void resizeEvent(QResizeEvent *);

protected:
    virtual void setupUi( const QString &path,
                      const r_meta::CMeta &meta );

protected Q_SLOTS:
    void onTimeout();

public:
    void showFgRule( bool bShow, EngineMaskRule *pRule );
    void showBgRule( bool bShow, EngineMaskRule *pRule );
    void showRangeLine( int a, int b );

protected:
    int createPath(
                    QPainterPath *pPath,
                    EngineMaskRule *pMask,
                     MaskRule *pRule );

    void paintFgRule( EngineMaskRule *pMask,
                      MaskRule *pRule,
                      QPainter *painter );
    void paintBgRule( EngineMaskRule *pMask,
                      MaskRule *pRule,
                      QPainter *painter );

    void paintBgMask( EngineMaskRule *pMask, QPainter *painter );
    void paintFgMask( EngineMaskRule *pMask, QPainter *painter );

    int ruleToyPixel( EngineMaskRule *pMask,
                     int adc);

protected:
                            //! cooridinates
    int myDivs, myGnd, myHeight, myTop, myBase;

    int mFgTimeout;
    QTimer *mpFgTimer;      //! single shot

    QColor mFgColor;
    QColor mBgColor;

    menu_res::RVLine *m_pLineA, *m_pLineB;

    //! variables
    EngineMaskRule *mpFgRule, *mpBgRule;
    bool mbFgVisible, mbBgVisible;
};


#endif // MASKWIDGET_H

