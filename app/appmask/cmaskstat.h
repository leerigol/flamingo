#ifndef CMASK_STAT_H
#define CMASK_STAT_H

#include <QtWidgets>

#include "../../menu/wnd/rmodalwnd.h"

class CMaskStat : public menu_res::RModalWnd
{
    Q_OBJECT
public:
    explicit CMaskStat(QWidget *parent = 0);
    ~CMaskStat();

protected:
    virtual void showEvent(QShowEvent *pEvent );
    virtual void paintEvent( QPaintEvent *event );
protected:
    void setupUi(const QString &path,
               const r_meta::CMeta &meta);

public:
    void setValue( quint64 match,
                   quint64 miss,
                   quint64 total,
                   float  sigma );
    void retranslateUi();


protected:
    //! match : value
    //! miss  : value
    //! total : value
    //! sigma : value
    QLabel *m_pLabels[6];
};


#endif // CREFLISAJOUS_H

