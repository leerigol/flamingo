#ifndef CAPPMASK_H
#define CAPPMASK_H
#include <QtWidgets>

#include "../capp.h"
#include "maskwidget.h"
#include "cmaskstat.h"

class CAppMask : public CApp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    CAppMask();

public:
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();
    virtual void buildConnection();

protected:
    virtual  void registerSpy();

protected Q_SLOTS:
    void on_wndstat_close();

protected:
    DsoErr  on_imm_rule_changed(AppEventCause aec);
    void  on_mask_changed();
    void  on_rangeline_changed();

    void  on_src_changed();
    void  onShowStat();

    void  on_source_changed();
    void  on_report_changed();
private:
    CMaskStat   *m_pMaskStat;
    CMaskWidget *m_pMaskWidget;

    EngineMaskRule mImmRule, mMask;
};

#endif // CAPPMASK_H

