#ifndef ARBDLG
#define ARBDLG

#include <QPainter>
#include <QDebug>

#include <QVector>
#include <QPointF>
#include "../../menu/wnd/uiwnd.h"
#include <QMouseEvent>
#include "../../menu/menustyle/rinfownd_style.h"

#define POINTS_NUM_ZOOM 6

enum
{
    AXIS_X,
    AXIS_Y
};

class CArbLine : public QWidget
{
    Q_OBJECT

public:
    explicit CArbLine(QWidget *parent = 0, int axis = AXIS_X, bool solid = false);

public:
    void setPos( int pos );
    void setColor( QColor color );
    void setupUI(int x, int y, int w, int h);

protected:
    void paintEvent( QPaintEvent *event );

private:

    int       m_nX;
    int       m_nY;
    int       m_nW;
    int       m_nH;

    int       m_nAxis;
    QColor    m_nColor;
    bool      m_bSolid;

    static QVector<qreal> _dashPattern;
};

class CArbDlg : public menu_res::uiWnd
{
    Q_OBJECT

    static menu_res::RModelWnd_Style _style;
public:
    CArbDlg(QWidget *parent = 0);

    void    setupUI();

    void    setCurrent(int);
    int     getCurrent();

    void    setPeriod(qint64);
    void    setTime(qint64);

    void    setMaxMinVolt(int,int);
    void    setDlgVolRange(int,int);

    void    setPoints(int);
    void    setPeriodChange(qint64 p);
    void    setChange(int points,int current);
    void    setLinear(bool bLinear);
    void    setZoom(bool bZoom);
    void    setWaveTable(void*);
    void    toScreenLine();
private:
    void    makeAxisX(void);
    void    makeAxisY(void);
    void    drawVolt(QPainter&);
    void    drawTime(QPainter&);
    void    drawPoint(QPainter&);

protected:
    void    loadResource( const QString & );
    void    paintEvent( QPaintEvent * );

private:
    CArbLine*  m_pLineX;
    CArbLine*  m_pLineY;
    CArbLine*  m_pCenter;

  //  CArbLine*  m_pCenterY;
   // CArbLine*  m_pCenterX;

    /*
    int     m_nMaxVolt;
    int     m_nMinVolt;
    */
    int     m_nDlgHighVol;
    int     m_nDlgLowVol;

    //this value can be larger than int,should transform to float
    int     m_nPeriod;
    int     m_nTime;

    int     m_nPoints;
    int     m_nCurrent;
    bool    m_bLinear;
    bool    m_bZoom;

    int     m_nStartIndex;

    QPushButton    *closeButton;
    QRect   m_nWaveArea;
    QList<QString> m_AxisYStr;
    QList<QString> m_AxisXStr;

    QList<int>*    m_pEditPointsTabel;
    //!如果使用m_pEditPointsTable在paintEvent直接计算绘图会与service同时访问出现争抢
    QVector<QPointF>      m_arbLine;
};

#endif // ARBDLG
