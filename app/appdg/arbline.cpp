#include "arbdlg.h"


QVector<qreal> CArbLine::_dashPattern;
///
/// \brief CArbLine::CArbLine
/// \param parent
///
CArbLine::CArbLine(QWidget *parent, int axis, bool solid) : QWidget(parent)
{
    m_nColor = Qt::yellow;

    m_nAxis = axis;
    m_bSolid= solid;


  //   m_nDlgHighVol;
   //  m_nDlgLowVol;


    if ( _dashPattern.isEmpty() )
    {
        _dashPattern.append( 10 );
        _dashPattern.append( 3 );
        _dashPattern.append( 1 );
        _dashPattern.append( 3 );
    }
}

void CArbLine::setupUI(int x, int y, int w, int h)
{
    if(m_nAxis == AXIS_X)
    {
        setGeometry(x,y,1,h);
    }
    else
    {
        setGeometry(x,y,w,1);
    }

    m_nW = w;
    m_nH = h;

    m_nX = x;
    m_nY = y;
}

void CArbLine::setColor(QColor color)
{
    m_nColor = color;
}

void CArbLine::setPos(int pos)
{
    if(m_nAxis == AXIS_X)
    {
        if(pos>=m_nX && pos <= (m_nW+m_nX) )
        {
            move(pos, m_nY);
        }
        else
        {
            qDebug() << "ASSERT:" << pos << m_nW + m_nX;
            Q_ASSERT(false);// error;
        }
    }
    else
    {
        //if(pos >= m_nY && pos < (m_nH+m_nY) )
        if(pos >= m_nY && pos <= (m_nH+m_nY) )
        {
            move(m_nX,pos);
        }
        else
        {
            Q_ASSERT(false);// error;
        }
    }
    update();
}


void CArbLine::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen pen;

    if(m_bSolid)
    {
        pen.setStyle(Qt::SolidLine);
    }
    else
    {
        pen.setStyle( Qt::CustomDashLine );
        pen.setDashPattern( _dashPattern );
        painter.setPen( pen );
    }
    pen.setColor( m_nColor );
    painter.setPen(pen);

    if(m_nAxis == AXIS_X)
    {
        painter.drawLine( 0, 0, 1, m_nH);
    }
    else
    {
        painter.drawLine( 0, 0, m_nW, 1);
    }

}
