#include "../../include/dsocfg.h"
#include "../../gui/cguiformatter.h"

#include "arbdlg.h"


#define ARB_GRID_YDIV 9
#define ARB_GRID_XDIV 7
#define dbgout()

menu_res::RModelWnd_Style CArbDlg::_style;
CArbDlg::CArbDlg(QWidget *parent) :uiWnd(parent)
{
    m_nPoints = 2;
    m_nCurrent= 1;
    m_nStartIndex = 1;
    m_nPeriod = time_ms(1);
    // m_nPeriod = time_us(10);

    m_bLinear = false;
    m_bZoom = false;
    m_pEditPointsTabel = NULL;

    m_nDlgHighVol = 0;
    m_nDlgLowVol = 0;

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void CArbDlg::setupUI()
{
    this->setGeometry(130,64,564,330);
    _style.loadResource("ui/wnd/model/");

  #if 1
    m_pCenter = new CArbLine(this, AXIS_Y );
    Q_ASSERT( NULL != m_pCenter );
#else
    m_pCenterY = new CArbLine(this, AXIS_Y );
    Q_ASSERT( NULL != m_pCenterY );

    m_pCenterX = new CArbLine(this, AXIS_X );
    Q_ASSERT( NULL != m_pCenterX );
#endif
    //! ASIX
    m_pLineX = new CArbLine(this, AXIS_X );
    Q_ASSERT( NULL != m_pLineX );

    m_pLineY = new CArbLine(this, AXIS_Y );
    Q_ASSERT( NULL != m_pLineY );

 #if 1
   // m_pCenterX->setColor(Qt::white);
   // m_pCenterY->setColor(Qt::green);
    m_pCenter->setColor(Qt::green);
    m_pLineX->setColor(QColor(150,80,0));
    m_pLineY->setColor(QColor(150,80,0));
#else
   // m_pCenterX->setColor(Qt::white);
   // m_pCenterY->setColor(Qt::white);
    m_pCenter->setColor(Qt::white);
    m_pLineX->setColor(QColor(150,80,0));
    m_pLineY->setColor(QColor(150,80,0));
#endif


    m_nWaveArea =  this->geometry();
    m_nWaveArea.moveTo(70,60);
    m_nWaveArea.setWidth(480);
    m_nWaveArea.setHeight(240);

    m_pLineX->setupUI(m_nWaveArea.x(), m_nWaveArea.y(),
                      m_nWaveArea.width(), m_nWaveArea.height());

    m_pLineY->setupUI(m_nWaveArea.x(), m_nWaveArea.y(),
                      m_nWaveArea.width(), m_nWaveArea.height());

#if 1
    m_pCenter->setupUI(m_nWaveArea.x(), m_nWaveArea.y(),
                     m_nWaveArea.width(), m_nWaveArea.height());
#else
    m_pCenterX->setupUI(m_nWaveArea.x(), m_nWaveArea.y(),
                      m_nWaveArea.width(), m_nWaveArea.height());
    m_pCenterY->setupUI(m_nWaveArea.x(), m_nWaveArea.y(),
                      m_nWaveArea.width(), m_nWaveArea.height());
#endif

    setDlgVolRange(mv(500), mv(-500));
    setPeriod(time_ms(1));

#if 1
    m_pCenter->setPos( m_nWaveArea.y() + m_nWaveArea.height() / 2 -1);
#else
     m_pCenterX->setPos( m_nWaveArea.x());
     m_pCenterY->setPos( m_nWaveArea.y() + m_nWaveArea.height() / 2 -1);
#endif
    closeButton = new QPushButton(this);
    connect(closeButton, SIGNAL(clicked(bool)),this,SLOT(close()));
    closeButton->setGeometry(524,2,34,27);
    closeButton->setFocusPolicy(Qt::NoFocus);
    closeButton->setStyleSheet("QPushButton{background-image:url(:/pictures/wnd/model_close.bmp);\
                               background-repeat:no-repeat;border:none;}\
                               QPushButton:pressed{background-image:url(:/pictures/wnd/model_close_down.bmp);\
                               background-repeat:no-repeat;border:none;}");

}

void CArbDlg::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QRect r = this->rect();
    _style.paint( painter, r );

    painter.setPen(QPen(Qt::white));
    painter.drawText(25,21,"Arbitrary wave");
    painter.fillRect(m_nWaveArea, Qt::black);

    drawVolt(painter);
    drawTime(painter);

    painter.setPen(QPen(QColor(255,80,0)));
    painter.drawPolyline(m_arbLine);
}

void CArbDlg::setDlgVolRange(int max, int min)
{
    if(max <= mv(80))
    {
        m_nDlgHighVol = mv(80);
    }
    else
    {
        while(m_nDlgHighVol < max)
        {
            if(m_nDlgHighVol < mv(800))
            {
                m_nDlgHighVol += mv(80);
            }
            else
            {
                m_nDlgHighVol += mv(400);
            }
        }
    }

    if(min >= mv(-80))
    {
        m_nDlgLowVol = mv(-80);
    }
    else
    {
        while(m_nDlgLowVol > min)
        {
            if(m_nDlgLowVol > mv(-800))
            {
                m_nDlgLowVol += mv(-80);
            }
            else
            {
                m_nDlgLowVol += mv(-400);
            }
        }
    }
   //qDebug() << "m_nDlgHighVol  1=  " << m_nDlgHighVol << "m_nDlgLowVol = " << m_nDlgLowVol << "m_nDlgHighVol - m_nDlgLowVol = " << m_nDlgHighVol - m_nDlgLowVol;
    int step = (m_nDlgHighVol - m_nDlgLowVol) / (ARB_GRID_YDIV-1);
    int volt = m_nDlgHighVol;
    QString str = "";

    m_AxisYStr.clear();
    for(int i=0; i<ARB_GRID_YDIV; i++)
    {
        float value = volt;
        value = value / 1e6;
        gui_fmt::CGuiFormatter::format(str, value);
        str   += "V";
        volt  -= step;
        m_AxisYStr.append(str);
    }
}

void CArbDlg::setPeriodChange(qint64 p)
{
    setPeriod(p);
    if(m_pEditPointsTabel != NULL)
    {
        toScreenLine();
    }
}


void CArbDlg::setChange(int points, int current)
{
    m_nPoints = points;
    m_nCurrent = current;

    if(m_bZoom)
    {//可能要修改m_nStartIndex
       setCurrent(m_nCurrent);
    }
    else
    {
        m_nStartIndex = 1;
    }

    makeAxisX();
    toScreenLine();
}

void CArbDlg::setTime(qint64)
{

}

void CArbDlg::makeAxisX()
{
    Q_ASSERT(m_nStartIndex>=1);
    QString str   = "";
    qint64 step   = m_nPeriod / m_nPoints;
    qint64 XTimeStep = 0;
    float startTime  = step * (m_nStartIndex-1);
    float endTime = 0;
    m_AxisXStr.clear();
    if(m_nPoints > POINTS_NUM_ZOOM && (!m_bZoom))
    {
        endTime = m_nPeriod;
        XTimeStep = m_nPeriod/4;
    }
    else if(m_nPoints > POINTS_NUM_ZOOM && m_bZoom)
    {
        endTime = m_nPeriod/m_nPoints*(m_nStartIndex + POINTS_NUM_ZOOM - 1);
        XTimeStep = step;
    }
    else
    {
        endTime = m_nPeriod;
        XTimeStep = step;
    }
    while(startTime <= endTime)
    {
        gui_fmt::CGuiFormatter::format(str,startTime/1e12);
        str += "s";
        startTime += XTimeStep;
        m_AxisXStr.append(str);
    }
}

void CArbDlg::setPeriod(qint64 p)
{
    m_nPeriod = p;
    makeAxisX(); 
}

void CArbDlg::setPoints(int p)
{
    m_nPoints = p;
    makeAxisX();
}

void CArbDlg::setLinear(bool bLinear)
{
    m_bLinear = bLinear;
    if(m_pEditPointsTabel != NULL)
    {
        toScreenLine();
    }
}

void CArbDlg::setZoom(bool bZoom)
{
    m_bZoom = bZoom;
    if(m_bZoom)
    {
       //可能要修改m_nStartIndex
       setCurrent(m_nCurrent);
    }
    else
    {
        m_nStartIndex = 1;
    }
    makeAxisX();
    if(m_pEditPointsTabel != NULL)
    {
        toScreenLine();
    }
}

int CArbDlg::getCurrent()
{
    return m_nCurrent;
}

void CArbDlg::setCurrent(int c)
{
    if(c && c <= m_nPoints)
    {
        m_nCurrent = c;
        if(m_bZoom)
        {
            if(c < m_nStartIndex)
            {
                m_nStartIndex = c;
            }
            else if(c >= m_nStartIndex && c < (m_nStartIndex + 6) )
            {
               //bug 1977 by zy
                if((m_nStartIndex + 6 - 1) > m_pEditPointsTabel->size())
                {
                  //  m_nStartIndex = m_nStartIndex -  (6 - (m_pEditPointsTabel->size() - m_nStartIndex)) + 1;
                    m_nStartIndex = m_pEditPointsTabel->size() - 5;
                }
               if(m_nStartIndex < 1)
               {
                   m_nStartIndex = 1;
               }
            }
            else
            {
                m_nStartIndex = c + 1 - 6;
            }
        }
        makeAxisX();
        toScreenLine();
    }
}

void CArbDlg::setWaveTable(void *ptr)
{
    Q_ASSERT(ptr != NULL);
    m_pEditPointsTabel = (QList<int>*)ptr;
}

void CArbDlg::drawVolt(QPainter& painter)
{

    if(m_AxisYStr.size() == ARB_GRID_YDIV)
    {
    //    qDebug() << "m_AxisYStr = " << m_AxisYStr;
        int step = m_nWaveArea.height()/(ARB_GRID_YDIV-1);
      //  qDebug() << "m_nWaveArea.height() = " << m_nWaveArea.height();
        int align = Qt::AlignRight | Qt::AlignVCenter;

        QRect r = this->geometry();
        r.moveTo(6,45);
        r.setWidth(58);
        r.setHeight(step);
        painter.setPen(QPen(Qt::white));
        for(int i=0; i<ARB_GRID_YDIV; i++)
        {
            painter.drawText(r, align,
                             m_AxisYStr.at(i));
           //  qDebug() << "m_AxisYStr.at(i) = " << m_nWaveArea.height();
            r.moveTo(r.x(), r.y()+step);
        }
    }

}

//draw current point
void CArbDlg::drawPoint(QPainter& painter)
{
    int step   = m_nWaveArea.width() / 6;
    int align = Qt::AlignLeft | Qt::AlignBottom;

    QRect r = this->geometry();
    r.moveTo(60,40);
    r.setWidth(step);
    r.setHeight(20);

    for(int i = m_nStartIndex; i<(m_nStartIndex+6); i++)
    {
        if(i > m_nPoints)
        {
            painter.setPen( QPen(Qt::gray));
        }
        painter.drawText(r, align, QString("%1").arg(i));

        /*from second point , text will be show on center*/
        if(i == m_nStartIndex )
        {
            r.moveTo(r.x()+step/2, r.y());
            align = Qt::AlignHCenter | Qt::AlignBottom;
        }
        else
        {
            r.moveTo(r.x()+step, r.y());
        }
    }
}

void CArbDlg::toScreenLine()
{
    m_arbLine.clear();
    QVector<QPointF> temp;
    temp.swap(m_arbLine);
    int startPoint;
    int screenPointNum;
    //bug 1977 by zy
    #if 1
    if(m_pEditPointsTabel->size() != m_nPoints)
    {
        m_nPoints = m_pEditPointsTabel->size();
    }
    #endif
    if( m_nPoints > POINTS_NUM_ZOOM && m_bZoom)
    {
        startPoint = m_nStartIndex;
        screenPointNum = POINTS_NUM_ZOOM;
    }
    else
    {
        startPoint = 1;
        screenPointNum = m_nPoints;
        //bug 1977 by zy
    #if 0
        if((m_pEditPointsTabel->size() != screenPointNum) && !m_bZoom)
        {
            screenPointNum = m_pEditPointsTabel->size();
        }
    #endif
    }
  //  qDebug() << "m_nStartIndex = " << m_nStartIndex << "m_pEditPointsTabel = " <<  m_pEditPointsTabel->size() << "screenPointNum = " << screenPointNum;
    int v = 0;
    double step = m_nWaveArea.width()*1.0/screenPointNum;
    if(m_bLinear)
    {
        m_arbLine.resize(screenPointNum+1);
    }
    else
    {
        m_arbLine.resize(2*screenPointNum);
    }

    double x = m_nWaveArea.left();

    for(int i = 0;i < screenPointNum;i++)
    {
    //    qDebug() <<"i = " << i <<  "m_nStartIndex = " << startPoint << "m_pEditPointsTabel = " <<  m_pEditPointsTabel->size() << "screenPointNum = " << screenPointNum;
        v = m_pEditPointsTabel->at(i + startPoint - 1);
    //    qDebug() <<"2i = " << i <<  "m_nStartIndex = " << startPoint << "m_pEditPointsTabel = " <<  m_pEditPointsTabel->size() << "screenPointNum = " << screenPointNum;
        int pos = m_nWaveArea.height() *
                (v - m_nDlgLowVol) / (m_nDlgHighVol - m_nDlgLowVol);
        pos = m_nWaveArea.height() - pos + m_nWaveArea.top() -1;
        if(v == m_nDlgHighVol)
        {
            pos = m_nWaveArea.top();
        }
        else if(v == m_nDlgLowVol)
        {
            pos--;
        }
        if(m_bLinear)
        {
            m_arbLine[i] = QPoint(x,pos);
        }
        else
        {
            m_arbLine[2*i] = QPoint(x,pos);
            m_arbLine[2*i+1] = QPoint(x+step,pos);
        }
        x = x+step;
    }

    if(m_bLinear)
    {
        m_arbLine[screenPointNum] =
                QPoint(m_nWaveArea.right(),m_arbLine[0].ry());
    }
    if(m_arbLine.size()!=0)
    {
        if(m_bLinear)
        {
            int current_X = m_arbLine.at(m_nCurrent - m_nStartIndex).x();
            int current_Y = m_arbLine.at(m_nCurrent - m_nStartIndex).y();
            m_pLineX->setPos(current_X);
            m_pLineY->setPos(current_Y);
            int cent_Y = m_nDlgHighVol*1.0/(m_nDlgHighVol - m_nDlgLowVol)*
                    m_nWaveArea.height()+m_nWaveArea.y();
            m_pCenter->setPos(cent_Y);
            // m_pCenterY->setPos(cent_Y);
        }
        else
        {
            int current_X = m_arbLine.at((m_nCurrent - m_nStartIndex)*2).x();
            int current_Y = m_arbLine.at((m_nCurrent - m_nStartIndex)*2).y();
            m_pLineX->setPos(current_X);
            m_pLineY->setPos(current_Y);
            int cent_Y = m_nDlgHighVol*1.0/(m_nDlgHighVol - m_nDlgLowVol)*
                    m_nWaveArea.height()+m_nWaveArea.y();
            m_pCenter->setPos(cent_Y);
            //m_pCenterY->setPos(cent_Y);
        }
    }
    update();
}

void CArbDlg::drawTime(QPainter& painter)
{
    //draw time information

    int step   = m_nWaveArea.width() / (m_AxisXStr.size()-1);
    int align = Qt::AlignLeft | Qt::AlignTop;

    QRect r = this->geometry();
    r.moveTo(60, m_nWaveArea.y() + m_nWaveArea.height()+2);
    r.setHeight(20);
    r.setWidth(step);
    painter.setPen(QPen(Qt::white));
    for(int i=0; i< m_AxisXStr.size(); i++)
    {
        painter.drawText(r, align, m_AxisXStr.at(i));

        /*from second point , text will be show on center*/
        if(i == 0)
        {
            r.moveTo(r.x() + step/2, r.y());
            align = Qt::AlignCenter | Qt::AlignTop;
        }
        //for last point
        else if(i == (m_AxisXStr.size()-2))
        {
            r.moveTo(m_nWaveArea.x() + m_nWaveArea.width() - step, r.y());
            align = Qt::AlignRight | Qt::AlignTop;
        }
        else
        {
            r.moveTo(r.x() + step, r.y());
        }
    }
}
