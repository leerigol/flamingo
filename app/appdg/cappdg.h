#ifndef CAPPDG_H
#define CAPPDG_H

#include <QtCore>
#include <QtWidgets>
#include <QVector>

#include "../capp.h"
#include "arbdlg.h"

#include "../../menu/rmenus.h"

class CAppDG : public CApp
{
    Q_OBJECT
    DECLARE_CMD()

public:
    CAppDG(int sourceId);

public:
     void setupUi( CAppMain *pMain );
     void retranslateUi();

     void buildConnection();
     void resize();
     void registerSpy();

     DsoErr  setWaveType();
     DsoErr  setSourceEn();

private slots:
     void setEnable();

private:

     int  onSubMenu(int);
     int  onReturn(int);
     int  onExit(int);
     void setHasDg();

     int  onLinear(int);
     int  onZoom(int);
     int  onCurrent(int);
     int  onVoltage(int);
     int  onLoadChan(int);
     int  onFreqChange(int);
     int  onChange(int);

private:
        int            sId;
        int            m_neditVolMax;
        int            m_neditVolMin;

    CArbDlg*           m_pArbDlg;
    menu_res::RDsoDG*  m_pDsoDG;
};

#endif // CAPPDG_H
