#include "cappdg.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../service/servch/servch.h"
#include "../../service/servvertmgr/servvertmgr.h"

#include "../appmain/cappmain.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppDG )
start_of_entry()

on_set_int_void(MSG_APP_DG,                 &CAppDG::setSourceEn),
on_set_int_void(MSG_DG_WAVE,                &CAppDG::setWaveType),

on_set_int_int ( MSG_DG_ARB_CURRENT,        &CAppDG::onCurrent),
on_set_int_int ( MSG_DG_ARB_VOLTAGE,        &CAppDG::onVoltage),
on_set_int_int ( MSG_DG_ARB_LINEAR,         &CAppDG::onLinear),
on_set_int_int ( MSG_DG_ARB_ZOOM,           &CAppDG::onZoom),

//改变初始化点数区分开
on_set_int_int ( servDG::MSG_DG_CHANGEPOINTS,&CAppDG::onChange),
//on_set_int_int ( servDG::MSG_DG_CHANGEFREQ,&CAppDG::onFreqChange),
on_set_int_int ( MSG_DG_FREQ_PERIOD_SUB_1,&CAppDG::onFreqChange),



//on_set_int_int ( MSG_DG_ARB_LOAD,          &CAppDG::onLoadChan),
on_set_int_int ( CMD_SERVICE_SUB_ENTER,      &CAppDG::onSubMenu),
on_set_int_int ( CMD_SERVICE_SUB_RETURN,     &CAppDG::onReturn),
//on_set_int_int ( CMD_SERVICE_EXIT_ACTIVE  ,  &CAppDG::onReturn),

on_set_int_int ( CMD_SERVICE_EXIT_ACTIVE  ,  &CAppDG::onExit),
on_set_void_void( servDG::MSG_CMD_HAS_DG, &CAppDG::setHasDg ),
end_of_entry()


CAppDG::CAppDG(int sourceId)
{
    sId = sourceId;
    mservName = QString("source%1").arg(sId);

//    addQuick( QString("G%1").arg(sId),
//              QString("ui/desktop/source%1/icon/").arg(sId) );

    m_pArbDlg = NULL;
    m_pDsoDG  = NULL;
    m_neditVolMax = 0;
    m_neditVolMin = 0;
}

void CAppDG::setupUi( CAppMain *pMain )
{
    if( 1 == sId)
    {
        m_pDsoDG = new menu_res::RDsoDG(1, pMain->getDownBar());
        connect(m_pDsoDG, SIGNAL(released()), this, SLOT(setEnable()));
        Q_ASSERT( m_pDsoDG != NULL );
        m_pDsoDG->hide();
    }

    if(2 == sId)
    {
        m_pDsoDG = new menu_res::RDsoDG(2, pMain->getDownBar());
        connect(m_pDsoDG, SIGNAL(released()), this, SLOT(setEnable()));
        Q_ASSERT( m_pDsoDG != NULL );
        m_pDsoDG->hide();
    }       

    if(m_pArbDlg == NULL)
    {
        m_pArbDlg = new CArbDlg(pMain->getCentBar());
        Q_ASSERT( m_pArbDlg != NULL );
        m_pArbDlg->setupUI();
        m_pArbDlg->hide();
    }
}

void CAppDG::retranslateUi()
{

}

void CAppDG::buildConnection()
{

}

void CAppDG::resize()
{

}

void CAppDG::registerSpy()
{

}

DsoErr CAppDG::setWaveType()
{
    int wavNum = 0;
    query( mservName, MSG_DG_WAVE, wavNum);
    m_pDsoDG->setCurrWave(wavNum+1);

    return ERR_NONE;
}

DsoErr CAppDG::setSourceEn()
{
    if(sysGetWorkMode() == work_help)
    {
        return ERR_NONE;
    }

    if(!sysHasDG())
        return ERR_NONE;

    bool isOpen = false;
    query( mservName, MSG_APP_DG, isOpen);
    m_pDsoDG->setOpen(isOpen);

    return ERR_NONE;
}

void CAppDG::setEnable()
{
    bool b = false;
    query( mservName, CMD_SERVICE_ACTIVE, b);

    if(b){
        bool isOpen = false;
        query( mservName, MSG_APP_DG, isOpen);
        if(isOpen)
            serviceExecutor::post(mservName, MSG_APP_DG, false);
        else
            serviceExecutor::post(mservName, MSG_APP_DG, true);
    }else{
        serviceExecutor::post(mservName, CMD_SERVICE_ACTIVE, true);
    }
}

int CAppDG::onSubMenu(int)
{
    Q_ASSERT(m_pArbDlg);
    //if the dialog is visible, then filter the other message
    if(m_pArbDlg->isVisible())
    {
        return ERR_NONE;
    }

    int subMenu = 0;
    query( mservName,servDG::MSG_DG_Get_SubMenu,subMenu);


    if(subMenu == MSG_DG_ARB_CREATE)
    {

       int nParam = 0;
       query( mservName,MSG_DG_ARB_POINTS,nParam);
       m_pArbDlg->setPoints(nParam);

       void *ptr = NULL;
       query( mservName,servDG::MSG_DG_Get_WavTable,ptr);
       QList<int>*  pEditPointsTable = (QList<int>*)ptr;

       if(pEditPointsTable != NULL)
       {
           for(int i = 0;i < pEditPointsTable->size();i++)
           {
               pEditPointsTable->replace(i,0);
           }
       }
        m_pArbDlg->setWaveTable(pEditPointsTable);
//        m_pArbDlg->setChange(nParam,1);
        //m_pArbDlg->setCurrent(1);
        m_neditVolMin = 0;
        m_neditVolMax = 0;
        m_pArbDlg->setDlgVolRange(m_neditVolMax,m_neditVolMin);
        m_pArbDlg->raise();
        m_pArbDlg->show();
       //bug 1976 by zy
#if 1
         m_pArbDlg->setCurrent(1);
#else
         m_pArbDlg->toScreenLine();
#endif
    }


    if( subMenu == MSG_DG_ARB_EDIT )
    {
        int nParam = 0;
        query( mservName,MSG_DG_ARB_POINTS,nParam);
        m_pArbDlg->setPoints(nParam);

        void *ptr = NULL;
        query( mservName,servDG::MSG_DG_Get_WavTable,ptr);
        QList<int>*  pEditPointsTable = (QList<int>*)ptr;

        m_pArbDlg->setWaveTable(pEditPointsTable);

        m_neditVolMin = 0;
        m_neditVolMax = 0;


        if(pEditPointsTable != NULL)
        {         
            for(int i = 0;i < pEditPointsTable->size();i++)
            {
                if(pEditPointsTable->at(i) < m_neditVolMin)
                {
                    m_neditVolMin = pEditPointsTable->at(i);
                }
                if(pEditPointsTable->at(i) > m_neditVolMax)
                {
                    m_neditVolMax = pEditPointsTable->at(i);
                }
            }
            m_pArbDlg->setDlgVolRange(m_neditVolMax,m_neditVolMin);
        }
        m_pArbDlg->raise();
        m_pArbDlg->show();
        m_pArbDlg->toScreenLine();

    }

    return ERR_NONE;
}

int CAppDG::onReturn(int)
{
    if(!sysHasDG())
        return ERR_NONE;
    if(!sysCheckLicense( OPT_DG ))
        return ERR_NONE;

    m_pDsoDG->setVisible(true);

    int exitMenu = 0;
    query( mservName,servDG::MSG_DG_Get_ExitMenu, exitMenu);
//    qDebug() << "return exitMenu = " << exitMenu;
    if(m_pArbDlg && (exitMenu == MSG_DG_ARB_CREATE || exitMenu == MSG_DG_ARB_EDIT))
                            // || exitMenu == MSG_DG_ARB_CREATE_MENU || exitMenu == MSG_DG_ARB_EDIT_MENU))
    {     
        m_pArbDlg->hide();
    }
#if 0
    else
    {
       m_pArbDlg->show();
    }
#endif

    if( !exitMenu)
    {
        //qDebug() << "hide";
        m_pArbDlg->hide();
    }
    return ERR_NONE;
}

int CAppDG::onExit(int)
{
    if(!sysHasDG())
        return ERR_NONE;

    if(!sysCheckLicense( OPT_DG ))
        return ERR_NONE;

    //!假如界面没出现
    if(m_pArbDlg->isVisible())
    {
        return ERR_NONE;
    }

    m_pDsoDG->setVisible(true);

    int exitMenu = 0;
    query( mservName,servDG::MSG_DG_Get_ExitMenu, exitMenu);
    //qDebug() << "exitMenu = " << exitMenu;
    if(m_pArbDlg && (exitMenu == MSG_DG_ARB_CREATE || exitMenu == MSG_DG_ARB_EDIT
                            || exitMenu == MSG_DG_ARB_CREATE_MENU || exitMenu == MSG_DG_ARB_EDIT_MENU) )
    {
        m_pArbDlg->hide();
    }

    if(m_pArbDlg && !exitMenu)
    {
       // qDebug() << "hide";
        m_pArbDlg->hide();
    }
    return ERR_NONE;

}

void CAppDG::setHasDg()
{
    bool bNow;
    bNow = sysHasDG();

    if(!bNow)
    {
        m_pDsoDG->hide();
    }
    else
    {
        bool en = sysCheckLicense( OPT_DG );
        if(!en)
        {
            m_pDsoDG->hide();
        }
        else
        {
             m_pDsoDG->show();
        }
    }
}

int CAppDG::onLinear(int)
{
    bool linear;
    query( mservName,MSG_DG_ARB_LINEAR,linear);
    m_pArbDlg->setLinear(linear);
    return ERR_NONE;
}

int CAppDG::onZoom(int)
{
    bool zoom;
    query( mservName,MSG_DG_ARB_ZOOM,zoom);
    m_pArbDlg->setZoom(zoom);
    return ERR_NONE;
}

int CAppDG::onVoltage(int)
{
    if(m_pArbDlg== NULL || !m_pArbDlg->isVisible())
    {
        return ERR_NONE;
    }

    int nCurrent = 1;
    query( mservName,MSG_DG_ARB_CURRENT,nCurrent);
    //改变电压值的情况下需要根据改变的电压值调整刻度范围和分度

    void *ptr = NULL;
    query( mservName,servDG::MSG_DG_Get_WavTable,ptr);
    QList<int>*  pEditPointsTable = (QList<int>*)ptr;

    if(pEditPointsTable->at(nCurrent-1) > m_neditVolMax)
    {
        m_neditVolMax = pEditPointsTable->at(nCurrent-1);
    }
    if(pEditPointsTable->at(nCurrent-1) < m_neditVolMin)
    {
        m_neditVolMin = pEditPointsTable->at(nCurrent-1);
    }

    m_pArbDlg->setDlgVolRange(m_neditVolMax,m_neditVolMin);

    if(pEditPointsTable)
    {
        Q_ASSERT(m_pArbDlg->getCurrent() <= pEditPointsTable->size());
        /*
        if(m_pArbDlg->getCurrent() > pEditPointsTable->size())
        {
            m_pArbDlg->setCurrent(pEditPointsTable->size());
        }
        */
        m_pArbDlg->toScreenLine();
    }

    return ERR_NONE;
}

int CAppDG::onFreqChange(int)
{

      qlonglong temp;
      serviceExecutor::query( mservName,MSG_DG_FREQ_PERIOD_SUB_1,temp);
      int isPeriod;
      serviceExecutor::query( mservName,MSG_DG_FREQ_PERIOD,isPeriod);
      qlonglong period = isPeriod? temp:(1e18/temp);


      //qDebug() << "HELLO" << "isPeriod " << isPeriod << "period"<< period;
      m_pArbDlg->setPeriodChange(period);
      return ERR_NONE;
}

int CAppDG::onChange(int)
{
    int points;
    query( mservName,MSG_DG_ARB_POINTS,points);

    int current;
    query( mservName,MSG_DG_ARB_CURRENT,current);

    void *ptr = NULL;
    query( mservName,servDG::MSG_DG_Get_WavTable,ptr);
    QList<int>*  pEditPointsTable = (QList<int>*)ptr;

    m_pArbDlg->setWaveTable(pEditPointsTable);
    m_pArbDlg->setChange(points,current);
    return ERR_NONE;
}

int CAppDG::onCurrent(int)
{
    if(m_pArbDlg== NULL || !m_pArbDlg->isVisible())
    {
        return ERR_NONE;
    }
    int nCurrent = 1;
    query( mservName,MSG_DG_ARB_CURRENT,nCurrent);
    m_pArbDlg->setCurrent(nCurrent);

    return ERR_NONE;
}
