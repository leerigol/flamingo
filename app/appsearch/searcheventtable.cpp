#include "searcheventtable.h"
#include "../../gui/cguiformatter.h"

searchEventTable::searchEventTable(QWidget *parent)
                    : RModalWnd(parent)
{
    Q_ASSERT(parent != NULL);
    set_attr( wndAttr, mWndAttr, wnd_moveable|wnd_manual_resize );
}

void searchEventTable::loadResource(const QString &path, const r_meta::CMeta &meta)
{
    Q_ASSERT(m_pTimeTable != NULL);
    getGeo( meta, path + "geo/wnd",        this );
    getGeo( meta, path + "geo/tableWnd",   m_pTimeTable );
    getGeo( meta, path + "geo/totalLabel", m_pTotalLabel );

    int width = 0;
    meta.getMetaVal( path + "table/ColumnWidth_0", width );
    m_pTimeTable->setColumnWidth(0, width );
    meta.getMetaVal( path + "table/ColumnWidth_1", width );
    m_pTimeTable->setColumnWidth(1, width);
    meta.getMetaVal( path + "table/ColumnWidth_2", width );
    m_pTimeTable->setColumnWidth(2, width);

    //! style
    QString str;
    meta.getMetaVal( path + "style/wnd", str );
    this->setStyleSheet( str );

    meta.getMetaVal( path + "defh", m_nDefH );
    meta.getMetaVal( path + "rowh", m_nRowH );
    meta.getMetaVal( path + "maxrow", m_nMaxRow );

    m_pTimeTable->horizontalHeader()->setFixedHeight(m_nRowH);
}

void searchEventTable::setupUi(const QString &path, const r_meta::CMeta &meta)
{
    m_pTotalLabel = new QLabel(this);
    Q_ASSERT(NULL != m_pTotalLabel);

     m_pTimeTable = new TableWidget(this);
     Q_ASSERT(m_pTimeTable != NULL);

     m_pTimeTable->setRowCount(EVENT_ITEN_COUNT);
     m_pTimeTable->setColumnCount(3);
     for(int i = 0; i < EVENT_ITEN_COUNT; i++)
     {
         m_pTimeTable->setItem(i,0, new QTableWidgetItem(QString("%1").arg(i+1,3,10,QLatin1Char('0'))));
         m_pTimeTable->setItem(i,1, new QTableWidgetItem(""));
         m_pTimeTable->setItem(i,2, new QTableWidgetItem(""));

         m_pTimeTable->setRowHidden(i, true);
         m_pTimeTable->item(i,0)->setTextAlignment(Qt::AlignCenter);
         m_pTimeTable->item(i,1)->setTextAlignment(Qt::AlignCenter);
         m_pTimeTable->item(i,2)->setTextAlignment(Qt::AlignCenter);
     }

     m_pTimeTable->horizontalHeader()->setFont(qApp->font());

     m_pTimeTable->setEditTriggers(QAbstractItemView::NoEditTriggers);   //设置不可编辑
     m_pTimeTable->setSelectionBehavior(QAbstractItemView::SelectRows);  //!选中模式, 行为单位
     m_pTimeTable->setSelectionMode(QAbstractItemView::SingleSelection); //!只能选中单行
     m_pTimeTable->setFocusPolicy(Qt::NoFocus);                          //去除选中虚线框
     m_pTimeTable->horizontalHeader()->setVisible(true);                //!不显示列表头
     m_pTimeTable->verticalHeader()->setVisible(false);                  //!不显示行表头
     m_pTimeTable->horizontalScrollBar()->setVisible(false);             //!不显示水平滚动条
     m_pTimeTable->verticalScrollBar()->setVisible(false);             //!不显示垂直滚动条

    loadResource(path, meta);
    buildConnection();
}

void searchEventTable::buildConnection()
{
    connect( m_pTimeTable, SIGNAL(itemSelectionChanged()), this, SLOT(onCurrEventChanged()) );
}

void searchEventTable::paintEvent(QPaintEvent *event)
{
    QStringList header = QStringList()<<sysGetString(STRING_SEARCH_NUMBER, "Num")
                                      <<sysGetString(STRING_SEARCH_TIME,   "Time")
                                      <<sysGetString(STRING_SEARCH_COUNT,  "Count");

    m_pTimeTable->setHorizontalHeaderLabels(header);
    RModalWnd::paintEvent( event );
}

void searchEventTable::tuneInc(int keyCnt)
{
    int value = m_pTimeTable->currentRow() + keyCnt;
    value = qMin(value, m_nCurEventCount-1);
    m_pTimeTable->setCurrentCell(value,0);
}

void searchEventTable::tuneDec(int keyCnt)
{
    int value = m_pTimeTable->currentRow() - keyCnt;
    value = qMax(value, 0);
    m_pTimeTable->setCurrentCell(value,0);
}

struct _time_s_n
{
    QString     s_Unit;
    qlonglong   n_Unit;
};
static const _time_s_n arrTimeUnit[] = {
    {"ps",  time_ps(1)},
    {"ns" , time_ns(1)},
    {"us" , time_us(1)},
    {"ms" , time_ms(1)},
    {"s"  , time_s(1)},
};

static void time_format(const qlonglong &nTime, QString &sTime)
{
    qlonglong ll_time  = qAbs(nTime);
    int n_i = 0;

    while(ll_time >= 1000)
    {
        ll_time = (ll_time/1000);
        n_i++;
    }
    Q_ASSERT( n_i<array_count(arrTimeUnit) );

    sTime =  QString("%1%2")
            .arg( (double)nTime/(arrTimeUnit[n_i].n_Unit))
            .arg(arrTimeUnit[n_i].s_Unit );

   //qDebug()<<__FILE__<<__LINE__<<nTime<<sTime;
}

void searchEventTable::setEventTime(QVector<qlonglong> *p)
{
    Q_ASSERT(p     != NULL);
    Q_ASSERT(m_pTimeTable != NULL);
    m_nCurEventCount = qMin(p->count(), EVENT_ITEN_COUNT);

    qlonglong  nTime = 0;
    QString    sTime;

    m_pTimeTable->setUpdatesEnabled(false);

    for(int i = 0; i < m_nCurEventCount; i++)
    {
        nTime = p->at(i);
        time_format(nTime, sTime);
        //gui_fmt::CGuiFormatter::format( sTime, (double)(nTime*hori_time_base), fmt_def, Unit_s );

        m_pTimeTable->item(i,1)->setText(sTime);
        m_pTimeTable->setRowHidden(i,false);
    }
    for(int i = m_nCurEventCount; i< EVENT_ITEN_COUNT; i++)
    {
        m_pTimeTable->setRowHidden(i,true);
    }

    m_pTimeTable->setUpdatesEnabled(true);

    int width = rect().width();
    if(m_nCurEventCount<1){
        m_pTimeTable->resize(m_pTimeTable->width(),m_nRowH);
        this->resize(width, m_nDefH+m_nRowH);
    }else if(m_nCurEventCount > m_nMaxRow){
        m_pTimeTable->resize(m_pTimeTable->width(),m_nMaxRow*m_nRowH+m_nRowH/2);
        this->resize(width, m_nDefH+m_nMaxRow*m_nRowH+m_nRowH/2);
    }else{
        m_pTimeTable->resize(m_pTimeTable->width(),m_nCurEventCount*m_nRowH);
        this->resize(width, m_nDefH+m_nCurEventCount*m_nRowH);
    }

    update();
}

DsoErr searchEventTable::setNavigatePre()
{
    tuneDec(1);
    return ERR_NONE;
}

DsoErr searchEventTable::setNavigateNext()
{
    tuneInc(1);
    return ERR_NONE;
}

DsoErr searchEventTable::setNavigateValue(int value)
{ 
    if(value >= m_nCurEventCount)
    {
        return ERR_INVALID_INPUT;
    }

    m_pTimeTable->setCurrentCell(value, 0);
    return ERR_NONE;
}

void searchEventTable::updateShow(searchData *pData)
{
    Q_ASSERT(pData  != NULL);
    Q_ASSERT(m_pTimeTable != NULL);

    EngineSearchData *p_data = &pData->m_data;
    Q_ASSERT(p_data  != NULL);

    m_nCurEventCount = 0;

    m_pTotalLabel->setText(QString("%1 : %2")
                           .arg( sysGetString(STRING_SEARCH_TOTAL_COUNT, "Total Count"))
                           .arg( p_data->getTotalCount() ));

    //qDebug()<<__FILE__<<__FUNCTION__<<p_data->count();
    QList<QString>  m_sTimeList;
    QList<QString>  m_sCountList;
    p_data->getViewTimeValue(m_sTimeList);
    p_data->getViewTimeCount(m_sCountList);
    if(m_sTimeList.count() != m_sCountList.count())
    {
        qWarning()<<__FILE__<<__LINE__<<"warning:"<<m_sTimeList.count()<<m_sCountList.count();
        return;
    }

    for(int  i = 0; i < m_sTimeList.count(); i++)
    {
        m_pTimeTable->item(m_nCurEventCount,1)->setText( m_sTimeList.at(i) );
        m_pTimeTable->item(m_nCurEventCount,2)->setText( m_sCountList.at(i) );
        m_pTimeTable->setRowHidden(m_nCurEventCount,false);
        m_nCurEventCount++;
    }

    for(int i = m_nCurEventCount; i< EVENT_ITEN_COUNT; i++)
    {
        m_pTimeTable->setRowHidden(i,true);
    }

    m_pTimeTable->setUpdatesEnabled(true);

    int width = rect().width();
    if(m_nCurEventCount<1){
        m_pTimeTable->resize(m_pTimeTable->width(),m_nRowH);
        this->resize(width, m_nDefH+m_nRowH);
    }else if(m_nCurEventCount>m_nMaxRow){
        m_pTimeTable->resize(m_pTimeTable->width(),m_nMaxRow*m_nRowH+m_nRowH/2);
        this->resize(width, m_nDefH+m_nMaxRow*m_nRowH+m_nRowH/2);
    }else{
        m_pTimeTable->resize(m_pTimeTable->width(),(m_nCurEventCount+1)*m_nRowH);
        this->resize(width, m_nDefH+(m_nCurEventCount+1)*m_nRowH);
    }

    update();
}

void searchEventTable::clearTable()
{
    for(int i = 0; i< EVENT_ITEN_COUNT; i++)
    {
        m_pTimeTable->setRowHidden(i,true);
    }
    int width = rect().width();
    m_pTimeTable->resize(m_pTimeTable->width(),m_nRowH);
    this->resize(width, m_nDefH + m_nRowH);
    m_pTimeTable->setUpdatesEnabled(true);
}

void searchEventTable::onCurrEventChanged()
{
    int    nRow = m_pTimeTable->currentRow();
    Q_ASSERT(nRow < EVENT_ITEN_COUNT);
    emit      sigCurrEventTime(nRow);
}



