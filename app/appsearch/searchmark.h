#ifndef SEARCHMARK_H
#define SEARCHMARK_H
#include<QWidget>
#include<QMap>
#include<QScrollBar>
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"

#include "../../service/servSearch/searchdso.h"
using namespace  search_dso;
using namespace  DsoType;

class searchmark : public QWidget
{
    Q_OBJECT
public:
    searchmark(QWidget *parent = 0);

enum enumItem
{
    mask_tabel_item_index,
    mask_tabel_item_searchtype,
    mask_tabel_item_time,
    mask_tabel_item_marknote,
};

public:
   void         setupUI(QString path);

protected:
   virtual void paintEvent(QPaintEvent *event);
   virtual void loadResource(const QString& path);
   void         drawMarker();

public:
    void    setMarkerPost(QVector<qlonglong> *p);
    void    setHoriAttr(horiAttr attr);

    void    updateShow(searchData *pData);
    void    setCurrMark(int value);
    void    clearMark();
private:
    QList<int>     m_mainList;
    QList<int>     m_zoomList;

    horiAttr       m_horiAttr;
    int            m_currMark;

Q_SIGNALS:
    void          sigMarkMax(int);
};

#endif // SEARCHMARK_H

