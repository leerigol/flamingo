#include "cappsearch.h"

void CAppSearch::onCodeCurrValue(int value)
{
    serviceExecutor::post( mservName,
                           CMD_SEARCH_CODE_VALUE, value );
}

void CAppSearch::onCodeCurrBit(int bitNum)
{
    serviceExecutor::post( mservName,
                           CMD_SEARCH_CODE_CURR_BIT, bitNum );
}

void CAppSearch::onCodeAllBit(int all)
{
    serviceExecutor::post( mservName,
                           CMD_SEARCH_CODE_ALL, all );
}


void CAppSearch::onCodeChange()
{
    int nCurrBit = 0;
    int when     = 0;

    if( !m_bSearchEn )
    {
        m_pCode->hide();
        return ;
    }

    int type = SEARCH_TYPE_EDGE;
    query(mservName, MSG_SEARCH_TYPE, type );

    m_pCode->setCodeMode(CODE_TYPE_DIGIT);

    if(type == SEARCH_TYPE_IIC)
    {
        serviceExecutor::query( mservName, MSG_SEARCH_I2C_WHEN,    when    );
        if ( !( (when == trig_i2c_data) || (when == trig_i2c_addr_data) ) )
        {
            m_pCode->hide();
            return ;
        }
    }
    else if(type == SEARCH_TYPE_SPI)
    {
    }
    else
    {
        m_pCode->hide();
        return ;
    }

    serviceExecutor::query( mservName, CMD_SEARCH_CODE_CURR_BIT, nCurrBit);
    m_pCode->setCurrBit(nCurrBit);

    CArgument arg;
    serviceExecutor::query( mservName, CMD_SEARCH_CODE_ARG, arg);
    if( 0 != arg.size())
    {
        if(arg.size() <= 32){
            if(type == SEARCH_PATTREN)
            {
                if(sysHasLA())
                    m_pCode->resize(m_pCode->m_nPatSize1);
                else
                    m_pCode->resize(m_pCode->m_nPatSize2);
            }else{
                m_pCode->resize(m_pCode->m_nPatSize1);
            }
        }else
            m_pCode->resize(m_pCode->m_nLine2Size);

        m_pCode->setCode(arg);
        m_pCode->show();
    }
    else
    {m_pCode->hide();}
}
