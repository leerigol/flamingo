#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../gui/cguiformatter.h"
#include "../appmain/cappmain.h"
#include "cappsearch.h"
#include "../appdisplay/cgrid.h"

#define left_icon_pattern       ":/pictures/hori/left1.bmp"
#define right_icon_pattern      ":/pictures/hori/right1.bmp"

using namespace search_dso;
IMPLEMENT_CMD( CApp, CAppSearch )
start_of_entry()
on_set_int_int ( MSG_HOR_ZOOM_ON,                  &CAppSearch::onZoom ),
on_set_int_int ( MSG_HOR_TIME_MODE,                &CAppSearch::onXY ),

on_set_void_void(MSG_SEARCH_EN,                    &CAppSearch::onEnable),
on_set_void_void(MSG_SEARCH_MARK_TABEL_EN,         &CAppSearch::onMarktableEnable),

on_set_void_void(MSG_HOR_ACQ_MEM_DEPTH,            &CAppSearch::onHoriChange),
on_set_void_void(MSG_HORI_MAIN_SCALE ,             &CAppSearch::onHoriChange),
on_set_void_void(MSG_HORI_MAIN_OFFSET ,            &CAppSearch::onHoriChange),
on_set_void_void(MSG_HORI_ZOOM_SCALE ,             &CAppSearch::onHoriChange),
on_set_void_void(MSG_HORI_ZOOM_OFFSET ,            &CAppSearch::onHoriChange),

on_set_void_void(MSG_HORIZONTAL_RUN ,              &CAppSearch::onRunStopChange),


on_set_void_void(MSG_SEARCH_EDGE_SOURCE ,          &CAppSearch::onSourceChange),

on_set_void_void(MSG_SEARCH_THRE ,                 &CAppSearch::onHthreChange),
on_set_void_void(MSG_SEARCH_THRE_A ,               &CAppSearch::onHthreChange),
on_set_void_void(MSG_SEARCH_THRE_B ,               &CAppSearch::onHthreChange),

on_set_void_void(MSG_SEARCH_THRE_IIC_SCL ,         &CAppSearch::onHthreChange),
on_set_void_void(MSG_SEARCH_THRE_IIC_SDA ,         &CAppSearch::onHthreChange),
on_set_void_void(MSG_SEARCH_THRE_SPI_SCL ,         &CAppSearch::onHthreChange),
on_set_void_void(MSG_SEARCH_THRE_SPI_SDA ,         &CAppSearch::onHthreChange),
on_set_void_void(MSG_SEARCH_THRE_SPI_CS ,          &CAppSearch::onHthreChange),

//!key
on_set_int_void (   MSG_SEARCH_PLAY_PRE,           &CAppSearch::setNavigatePre),
on_set_int_void (   MSG_SEARCH_PLAY_NEXT,          &CAppSearch::setNavigateNext),
on_set_int_void (   MSG_SEARCH_PLAY_STOP,          &CAppSearch::setNavigateStop),
on_set_int_void (   MSG_SEARCH_NAVIGATION_EVENT,   &CAppSearch::setNavigateValue),

on_set_void_void(   search_dso::CMD_UPDATE_UI ,    &CAppSearch::onShowUpdate),
on_set_void_void(   MSG_DISPLAY_CLEAR,             &CAppSearch::onDisplayClear),

on_set_void_void(   CMD_CODE_CHANGE,               &CAppSearch::onCodeChange),
on_set_int_int (    CMD_SERVICE_EXIT_ACTIVE ,      &CAppSearch::onDeActive),
on_set_void_void(   CMD_SERVICE_ENTER_ACTIVE,      &CAppSearch::onInActive ),

end_of_entry()

CAppSearch::CAppSearch()
{
    mservName           = serv_name_search;
    m_bSearchEn         = false;
    m_bZoom             = false;

    m_pCode             = NULL;
    m_psearchmark       = NULL;
    m_pSource           = NULL;
    m_psearchEventTable = NULL;
    m_pSearchTimer      = NULL;
    pSearchData         = NULL;

    for(int i=Main_Level_A; i<All_Level; i++)
    {
        m_pLevel[i] = NULL;
    }

    addQuick(   MSG_SEARCH_APP,
                QStringLiteral("ui/desktop/search/icon/") );
}

void CAppSearch::setupUi(CAppMain *pMain)
{
    m_pSource = getCurrSourcePtr();

    m_pCode = new CCodeKeyboard( pMain->getCentBar() );
    Q_ASSERT(NULL != m_pCode);
    m_pCode->setupUi();
    m_pCode->hide();

    for(int i=Main_Level_A; i<All_Level; i++)
    {
        m_pLevel[i] = new searchLevel(pMain->getCentBar());
        Q_ASSERT(m_pLevel[i] != NULL);
        m_pLevel[i]->setGndParent(pMain->getLeftBar());
        m_pLevel[i]->setupUi();
        m_pLevel[i]->setVisible(false);
    }
    sysAttachUIView(m_pLevel[Main_Level_A], view_yt_main);
    sysAttachUIView(m_pLevel[Main_Level_B], view_yt_main);
    sysAttachUIView(m_pLevel[Zoom_Level_A], view_yt_zoom);
    sysAttachUIView(m_pLevel[Zoom_Level_B], view_yt_zoom);

    m_pSearchTimer = new QTimer(this);
    Q_ASSERT(m_pSearchTimer != NULL);

    m_psearchmark = new searchmark(pMain->getCentBar());
    Q_ASSERT(NULL != m_psearchmark);
    m_psearchmark->setupUI("search/");
    m_psearchmark->setVisible(false);

    m_psearchEventTable = new searchEventTable(pMain->getCentBar());
    Q_ASSERT(NULL != m_psearchEventTable);
    //m_psearchEventTable->setTitle(STRING_SEARCH_TOTAL_COUNT);
    m_psearchEventTable->setupUi("appsearch/EventTable/",r_meta::CAppMeta());
    m_psearchEventTable->show();
    m_psearchEventTable->setVisible(false);

    pointer dataPtr = NULL;
    serviceExecutor::query( mservName, CMD_SEARCH_GET_DATA, dataPtr);
    Q_ASSERT(dataPtr != NULL );

    searchData *pData = static_cast<searchData*>(dataPtr);
    Q_ASSERT(pData != NULL );
    pSearchData = pData;
}

void CAppSearch::retranslateUi()
{
    QString titleStyle = QString("color:#c0c0c0;font:12pt;font-family:%1").arg(qApp->font().family());
    m_psearchEventTable->setTitleStyle(titleStyle);
    if(m_psearchEventTable->isVisible())
    {
        m_psearchEventTable->redraw();
    }
}

void CAppSearch::buildConnection()
{
    connect( m_pCode ,SIGNAL(postCurrCodeSig(int)),this,SLOT(onCodeCurrValue(int)));
    connect( m_pCode ,SIGNAL(postCurrBitSig(int )),this,SLOT(onCodeCurrBit(int)));
    connect( m_pCode ,SIGNAL(postSetAllSig(int )),this,SLOT(onCodeAllBit(int)));

    connect( m_pSearchTimer,      SIGNAL(timeout()),            this, SLOT(onTimeout()) );
    connect( m_psearchEventTable, SIGNAL(sigCurrEventTime(int)),this, SLOT(onCurrEventTime(int)) );
    connect( m_psearchmark,       SIGNAL(sigMarkMax(int)),      this, SLOT( onNavigationMax(int)) );
}

void CAppSearch::registerSpy()
{
    spyOn(serv_name_hori, MSG_HORI_MAIN_SCALE);
    spyOn(serv_name_hori, MSG_HORI_MAIN_OFFSET);
    spyOn(serv_name_hori, MSG_HORI_ZOOM_SCALE);
    spyOn(serv_name_hori, MSG_HORI_ZOOM_OFFSET);
    spyOn(serv_name_hori, MSG_HOR_ACQ_MEM_DEPTH);

    spyOn(serv_name_hori, MSG_HORIZONTAL_RUN);

    spyOn(serv_name_hori, MSG_HOR_ZOOM_ON         );
    spyOn(serv_name_hori, MSG_HOR_TIME_MODE       );
}

int CAppSearch::onDeActive(int)
{
    Q_ASSERT(NULL != m_pCode);
    m_pCode->hide();
    return ERR_NONE;
}

int CAppSearch::onInActive()
{
    onCodeChange();
    return ERR_NONE;
}

int CAppSearch::onZoom(AppEventCause)
{
    return serviceExecutor::query( serv_name_hori,
                                   MSG_HOR_ZOOM_ON,
                                   m_bZoom );
}

int CAppSearch::onXY(AppEventCause)
{
    return ERR_NONE;
}


void CAppSearch::onEnable()
{
    onDisplayClear();

    query(mservName, MSG_SEARCH_EN, m_bSearchEn);
    m_psearchmark->setVisible(m_bSearchEn);
    onCodeChange();

    onShowUpdate();
}
void CAppSearch::onMarktableEnable()
{
    onDisplayClear();

    bool bSearchEnable = false;
    bool bEnable      = false;
    query(mservName, MSG_SEARCH_EN, bSearchEnable);
    query(mservName, MSG_SEARCH_MARK_TABEL_EN, bEnable);

    m_psearchEventTable->setVisible( bSearchEnable && bEnable );

    onShowUpdate();
}



void CAppSearch::onShowUpdate()
{
    if(m_bSearchEn)
    {
        //qDebug()<<__FILE__<<__FUNCTION__<<"--------show--------";
        m_psearchmark->updateShow(pSearchData);

        if(m_psearchEventTable->isVisible())
        {
            m_psearchEventTable->updateShow(pSearchData);
        }
    }
}

void CAppSearch::onDisplayClear()
{
    m_psearchmark->clearMark();
    m_psearchEventTable->clearTable();
}

void CAppSearch::onHoriChange()
{
}

void CAppSearch::onRunStopChange()
{
    int st;
    query( serv_name_hori, MSG_HORIZONTAL_RUN, st );
    m_runStop = (ControlAction)st;
}

void CAppSearch::onHthreChange()
{
    Q_ASSERT(m_pSource != NULL);

    m_pSource = getCurrSourcePtr();
    int ch = m_pSource->getChan(); //!当前通道
    setHthreLineColor( (Chan)ch);

    if( (chan1<=ch) && (ch <= chan4))  //!模拟通道
    {
        int    posA    = m_pSource->get_nLevelPost(0);
        int    posB    = m_pSource->get_nLevelPost(1);

        m_pLevel[Main_Level_A]->setPos(posA,     m_pSource->hasTwoLevel(0) );
        m_pLevel[Main_Level_B]->setPos(posB,     m_pSource->hasTwoLevel(1) );

        m_pLevel[Zoom_Level_A]->setPos(posA,  (m_pSource->hasTwoLevel(0)&&m_bZoom) );
        m_pLevel[Zoom_Level_B]->setPos(posB,  (m_pSource->hasTwoLevel(1)&&m_bZoom) );
    }

    m_pLevel[Main_Level_A]->hideGND();
    m_pLevel[Main_Level_B]->hideGND();
    m_pLevel[Zoom_Level_A]->hideGND();
    m_pLevel[Zoom_Level_B]->hideGND();
}

void CAppSearch::onSourceChange()
{
    m_pSource = getCurrSourcePtr();
    Q_ASSERT(m_pSource != NULL);
}

DsoErr CAppSearch::setNavigatePre()
{
    if(m_runStop == Control_Stop)
    {
        servGui::showImage(left_icon_pattern );
    }
    return m_psearchEventTable->setNavigatePre();
}

DsoErr CAppSearch::setNavigateNext()
{
    if(m_runStop == Control_Stop)
    {
        servGui::showImage(right_icon_pattern);
    }
    return m_psearchEventTable->setNavigateNext();
}

DsoErr CAppSearch::setNavigateStop()
{
    return ERR_NONE;
}

DsoErr CAppSearch::setNavigateValue()
{
    int     valve = 0;
    query( mservName, MSG_SEARCH_NAVIGATION_EVENT, valve );

    m_psearchmark->setCurrMark(valve);
    return m_psearchEventTable->setNavigateValue(valve);
}


CSource *CAppSearch::getCurrSourcePtr()
{
    DsoErr  err   = ERR_NONE;
    pointer p     = NULL;
    err = query( mservName, CMD_SEARCH_SOURCE_CURR_PTR, p );
    if ( err != ERR_NONE ) return NULL;

    return static_cast<CSource*>(p);
}

void CAppSearch::setHthreLineColor(Chan ch)
{
    QColor color = sysGetChColor(ch);
    for(int i=Main_Level_A; i<All_Level; i++)
    {
        Q_ASSERT(m_pLevel[i] != NULL);
        m_pLevel[i]->setColor(color);
    }
}

void CAppSearch::onTimeout()
{
    m_pSearchTimer->start(50);
}

void CAppSearch::onCurrEventTime(int index)
{
    bool en = false;
    query(mservName, MSG_SEARCH_EN, en);
    //!修改列表索引。
    if(en)
    {
        serviceExecutor::post(mservName, MSG_SEARCH_NAVIGATION_EVENT,index);
    }
}

void CAppSearch::onNavigationMax(int index)
{
    int curIndex = 0;
    serviceExecutor::query(mservName,MSG_SEARCH_NAVIGATION_EVENT,curIndex);
    if(index < curIndex)
    {
       m_psearchEventTable->setNavigateValue(index);
       onCurrEventTime(index);
    }
}

