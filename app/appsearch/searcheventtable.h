#ifndef SEARCHEVENTTABLE_H
#define SEARCHEVENTTABLE_H
#include <QTableWidget>
#include "../../menu/wnd/rmodalwnd.h"
#include "../../include/dsocfg.h"
#include "../appdecode/tableWidget.h"
#define  EVENT_ITEN_COUNT  (1000)

#include "../../service/servSearch/searchdso.h"
using namespace  search_dso;
using namespace  menu_res;

class searchEventTable: public RModalWnd
{
       Q_OBJECT
public:
    explicit searchEventTable(QWidget *parent = 0);
public:
    virtual void   loadResource( const QString &path,
                                const r_meta::CMeta &meta );
    virtual void   setupUi( const QString &path,
                           const r_meta::CMeta &meta );
    virtual void   buildConnection();
protected:
    virtual void   paintEvent( QPaintEvent *event );
    virtual void   tuneInc(int keyCnt );
    virtual void   tuneDec(int keyCnt );

public:
    void           setEventTime(QVector<qlonglong> *p);

    DsoErr         setNavigatePre();
    DsoErr         setNavigateNext();
    DsoErr         setNavigateValue(int value);

    void           updateShow(searchData *pData);
    void           clearTable();
private:
    QLabel         *m_pTotalLabel;
    QTableWidget   *m_pTimeTable;
    int             m_nCurEventCount;

    int             m_nDefH;
    int             m_nRowH;
    int             m_nMaxRow;

public Q_SLOTS:
    void     onCurrEventChanged();

Q_SIGNALS:
    void     sigCurrEventTime(int);

};
#endif // SEARCHEVENTTABLE_H
