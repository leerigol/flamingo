#ifndef CAPPSEARCH_H
#define CAPPSEARCH_H
#include <QtCore>
#include <QtWidgets>
#include "searchmark.h"
#include  "searcheventtable.h"

#include "../capp.h"
#include "searchlevel.h"
#include "../../service/servSearch/servsearch.h"
#include "../../app/trigger/trigcode.h"

class CAppSearch :  public CApp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    CAppSearch();

public:
    enum appCmd
    {
        cmd_base = CMD_APP_BASE,
        cmd_mask_pathrect_onoff,
    };

    enum
    {
        Main_Level_A = 0,
        Main_Level_B,
        Zoom_Level_A,
        Zoom_Level_B,
        All_Level,
    };

public:
     virtual void         setupUi( CAppMain *pMain );
     virtual void         retranslateUi();
     virtual void         buildConnection();

protected:
     virtual  void        registerSpy();

public:
    int                   onDeActive(int);
    int                   onInActive();
    int                   onZoom(AppEventCause);
    int                   onXY(AppEventCause);

public:
     void                 onEnable();
     void                 onMarktableEnable();
     void                 onShowUpdate();
     void                 onDisplayClear();

     void                 onHoriChange();
     void                 onRunStopChange();
     void                 onHthreChange();
     void                 onSourceChange();

     DsoErr               setNavigatePre();
     DsoErr               setNavigateNext();
     DsoErr               setNavigateStop();
     DsoErr               setNavigateValue();


     CSource              *getCurrSourcePtr();
     void                 setHthreLineColor(Chan ch);

     void                 onCodeChange();

public Q_SLOTS:
    void                  onTimeout(void);
    void                  onCurrEventTime(int index);
    void                  onNavigationMax(int index);

    void                  onCodeCurrValue(int value);
    void                  onCodeCurrBit(int bitNum);
    void                  onCodeAllBit(int all);

private:
     CCodeKeyboard        *m_pCode;
private:
     searchLevel          *m_pLevel[All_Level];
     searchmark           *m_psearchmark;
     searchEventTable     *m_psearchEventTable;
     QTimer               *m_pSearchTimer;

private:
     searchData           *pSearchData;

     CSource*              m_pSource;
     ControlAction         m_runStop;
private:
    bool                    m_bZoom;
    bool                    m_bSearchEn;
};
#endif // CAPPSEARCH_H

