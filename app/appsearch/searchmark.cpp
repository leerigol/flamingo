#include"searchmark.h"
#include "../../meta/crmeta.h"
#include <QPainter>
#include <QStylePainter>
searchmark::searchmark(QWidget *parent):QWidget(parent)
{
  Q_ASSERT(parent != NULL);
  setGeometry(0, 0, parent->width(), parent->height());

  m_mainList.reserve(1000);
  m_zoomList.reserve(1000);

  m_currMark = 0;
}

void searchmark::setupUI(QString path)
{
    loadResource(path);
}

void searchmark::loadResource(const QString & /*path*/)
{
    r_meta::CRMeta meta;
}

void searchmark::drawMarker()
{
    QPen elliPen(Qt::red);
    elliPen.setStyle(Qt::SolidLine);
    elliPen.setWidth(1);

    QPainter painter(this);
    painter.setPen(elliPen);
    painter.setPen(Qt::white);
    painter.setBrush(Qt::white);

    int x = 0;
    QPixmap pixmap(":/pictures/search/serachMark.png");       //!13x7
    QPixmap pixmapSelect(":/pictures/search/serachMarkSelect.png"); //!13x7

    QRect   rect(0,0,0,0);

    for(int i = 0; i<m_zoomList.count(); i++)
    {
        x = m_zoomList.at(i);
        rect.setRect(x-6, 160, 12, 7);
        painter.drawPixmap(rect, pixmap);
    }

    for(int i = 0; i<m_mainList.count(); i++)
    {
        x = m_mainList.at(i);
        rect.setRect(x-6, 0, 12, 7);
        painter.drawPixmap(rect, pixmap);
    }

    //!draw select mark
    if(m_currMark < m_mainList.count() )
    {
        x = m_mainList.at(m_currMark);
        rect.setRect(x-6, 0, 12, 7);
        painter.drawPixmap(rect, pixmapSelect);
    }
}

void searchmark::paintEvent(QPaintEvent */*event*/)
{
    drawMarker();
}

void searchmark::setMarkerPost(QVector<qlonglong> *p)
{
    Q_ASSERT(p          != NULL);
    m_mainList.clear();

    for(int  i = 0; i < p->count(); i++)
    {
        int post  = (p->at(i)/m_horiAttr.tInc + m_horiAttr.gnd);
        m_mainList.append( post );
    }
    update();
}

void searchmark::setHoriAttr(horiAttr attr)
{
    m_horiAttr = attr;
}

void searchmark::updateShow(searchData *pData)
{
    Q_ASSERT(pData  != NULL);

    EngineSearchData *p_data = &pData->m_data;
    Q_ASSERT(p_data  != NULL);

#if 1 //!方案4
    p_data->getViewMarkValue(m_mainList);
    p_data->getZoomViewMarkValue(m_zoomList);
    emit sigMarkMax( m_mainList.count() );
#endif

    update();
}

void searchmark::setCurrMark(int value)
{
    m_currMark = value;
}

void searchmark::clearMark()
{
    m_mainList.clear();
    m_zoomList.clear();
    update();
}

