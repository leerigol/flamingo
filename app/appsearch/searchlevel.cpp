#include "searchlevel.h"

searchLevel::searchLevel(QWidget *parent)
    :CTriggerLevel(parent)
{
    m_pTimer = new QTimer(this);
    Q_ASSERT(m_pTimer != NULL);
}

void searchLevel::setupUi()
{
    connect( m_pTimer,  SIGNAL(timeout()),
             this,      SLOT(onTimeout()) );
   CTriggerLevel::setupUi();
}

void searchLevel::setPos(int pos, bool visible, int time)
{
    CTriggerLevel::setPos(pos, visible);
    m_pTimer->start(time);
}

void searchLevel::onTimeout()
{
    this->setVisible(false);
}
