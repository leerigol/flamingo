#ifndef  SEARCHLEVEL_H
#define  SEARCHLEVEL_H
#include "../trigger/triglevel.h"

class searchLevel: public CTriggerLevel
{
    Q_OBJECT
public:
    explicit searchLevel(QWidget *parent = 0);
    void     setupUi(void);
public:
    void setPos(int pos, bool visible, int time = 1000);

public Q_SLOTS:
    void onTimeout();
private:
    QTimer *m_pTimer;
};

#endif // SEARCHLEVEL_H
