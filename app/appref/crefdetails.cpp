#include <QPainter>
#include "crefdetails.h"
#include "../../meta/crmeta.h"
#include "../../service/servref/servref.h"
#include "../../gui/cguiformatter.h"

menu_res::RModelWnd_Style CRefDetails::_style;
CRefDetails::CRefDetails(QWidget *parent) : uiWnd(parent)
{
    _style.loadResource("ui/wnd/model/");
    loadSource("ref/detail_wnd/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(mCloseRect);
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SIGNAL(closeSig()) );

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

CRefDetails::~CRefDetails()
{
}

void CRefDetails::loadSource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path  ;

    meta.getMetaVal(str + "x", m_x);
    meta.getMetaVal(str + "y", m_y);
    meta.getMetaVal(str + "w", m_width);
    meta.getMetaVal(str + "h", m_height);
    meta.getMetaVal(str + "m_rowh", m_rowh);

    meta.getMetaVal(str + "closerect", mCloseRect);
    int num = 0;
    meta.getMetaVal(str + "fontcolor", num);
    setColor(mFontColor, num);
    meta.getMetaVal(str + "titlecolor", num);
    setColor(mTitlebgColor, num);
    meta.getMetaVal(str + "gray15", num);
    setColor(mGray15, num);
    meta.getMetaVal(str + "gray27", num);
    setColor(mGray27, num);

    this->setGeometry(m_x, m_y, m_width, m_height);
}

void CRefDetails::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}

void CRefDetails::paintEvent(QPaintEvent *)
{

    QPainter painter(this);
    QRect    r;
    r.setX(0);
    r.setY(0);
    r.setWidth(this->geometry().width());
    r.setHeight(this->geometry().height());

    _style.paint(painter, r);

    QRect rLeftBox = r;
    rLeftBox.setX(9);
    rLeftBox.setY(39);
    rLeftBox.setWidth(r.width()-18);
    rLeftBox.setHeight(m_rowh);
    painter.fillRect(rLeftBox.x(),rLeftBox.y(),
                     rLeftBox.width(),rLeftBox.height(), mTitlebgColor);

    for(int i=0; i<REF_COUNT; i++)
    {
        painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                         rLeftBox.width(),rLeftBox.height(), mGray15);
        i++;
        if( i<REF_COUNT)
        {
            rLeftBox.moveTo(rLeftBox.bottomLeft());
            painter.fillRect(rLeftBox.bottomLeft().x(),rLeftBox.bottomLeft().y(),
                             rLeftBox.width(),rLeftBox.height(),  mGray27);
        }
        rLeftBox.moveTo(rLeftBox.bottomLeft());
    }

    QPen   pen;
    pen.setColor(mFontColor);
    painter.setPen(pen);

    painter.drawText(15,4,80,30,Qt::AlignLeft|Qt::AlignVCenter,"REF");
#if 1
    //!Draw Titel
    r.setY(r.y() + 38);
    QString Status = "Status";
    r.setX(r.x() + r.width()/5+3);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, Status);

    QStringList Titel;
    Titel << "SaRate" << "Scale" << "Offset";

    int space = r.width()/5+6;
    foreach(QString strTitel, Titel)
    {
        r.setX(r.x() + space);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, strTitel);
        space = 88;
    }


    r.setX(20);
    r.setY(r.y() + m_rowh);

    for(int i=0; i<REF_COUNT; i++)
    {
        CRefData* pData = servRef::getRefData(i);
        //bug 3355 by zy
        Chan src = pData->m_nSaveSrc;
        if(pData)
        {
            QString str = QString("REF%1").arg((i+1));
            painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, str);
            r.setX(r.x() + r.width()/5 -10);
            if(pData->getOnOff())
            {
                str = "ON";
                painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, str);
                if(src >= d0 && src <= d15)
                {
                     qlonglong laSate = 0;
                     serviceExecutor::query(serv_name_hori,MSG_HOR_LA_SA_RATE,laSate);
                     gui_fmt::CGuiFormatter::format(str, laSate/1e6);
                }
                else
                {
                    qlonglong AcqSate = 0;
                    serviceExecutor::query(serv_name_hori,MSG_HOR_ACQ_SARATE,AcqSate);
                    gui_fmt::CGuiFormatter::format(str, AcqSate/1e6);
                }
            }
            else
            {
                str = "OFF";
                painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, str);

                str = "0";
                if(pData->isValid())
                {
                    if(src >= d0 && src <= d15)
                    {
                         qint64 laSate = 0;
                         serviceExecutor::query(serv_name_hori,MSG_HOR_LA_SA_RATE,laSate);
                         gui_fmt::CGuiFormatter::format(str, laSate);
                    }
                    else
                    {
                        qint64 AcqSate = 0;
                        serviceExecutor::query(serv_name_hori,MSG_HOR_ACQ_SARATE,AcqSate);
                        gui_fmt::CGuiFormatter::format(str, AcqSate);
                       // gui_fmt::CGuiFormatter::format(str, 1/pData->getViewData()->gettInc().toDouble());
                    }
                }
            }
            r.setX(r.x() + space-20);
            //str += "pts";
            str += "Sa/s";

            painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, str);
            r.setX(r.x() + space);


            gui_fmt::CGuiFormatter::format(str, ch_volt_base * pData->getVScale());
            str += pData->getUnit();

           // str += "V";
            if(src >= d0 && src <= d15)
            {
                painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "------");
            }
            else
            {
                painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, str);
            }
            r.setX(r.x() + space);

            gui_fmt::CGuiFormatter::format(str, ch_volt_base * pData->getVOffset());        
            str += pData->getUnit();

            painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, str);
            //painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, pData->getRefTime());
        }
        r.setX(20);
        r.setY(r.y() + 20);
    }

#endif
}

void CRefDetails::hideEvent(QHideEvent *)
{
}
