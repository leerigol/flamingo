#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../service/servref/servref.h"

#include "../../gui/cguiformatter.h"

#include "../appmain/cappmain.h"

#include "cappref.h"

IMPLEMENT_CMD( CApp, CAppRef )
start_of_entry()

on_set_void_void( MSG_REF_DETAILS,             &CAppRef::onDetails),
on_set_int_int  ( MSG_REF_SAVE,                &CAppRef::onDataChange),
on_set_int_int  ( MSG_REF_COLOR,               &CAppRef::onDataChange),

on_set_int_int  ( MSG_REF_TIMESCALE,           &CAppRef::onDataChange),
on_set_int_int  ( MSG_REF_OFFSET,              &CAppRef::onDataChange),
on_set_int_int  ( MSG_REF_VOLT,                &CAppRef::onDataChange),
on_set_int_int  ( MSG_REF_POS,                 &CAppRef::onDataChange),

on_set_int_int  ( MSG_REF_ONOFF,               &CAppRef::onClear),
on_set_int_int  ( servRef::MSG_REF_CLEAR_ALL,  &CAppRef::onClear),
on_set_int_int  ( servRef::MSG_REF_HOR_MODE,   &CAppRef::onHorMode),

on_set_int_int  ( servRef::MSG_REF_SHOW,       &CAppRef::onRefresh),

on_set_int_int  ( MSG_REF_LABEL_ONOFF,         &CAppRef::onLabel),
on_set_int_int  ( MSG_REF_LABEL_EDIT,          &CAppRef::onLabel),
on_set_int_int  ( MSG_REF_LABEL_NAME,          &CAppRef::onLabel),
on_set_int_int  ( MSG_REF_COLOR,               &CAppRef::onLabel),

on_set_int_void(servRef::MSG_REF_HALF_FFT,  &CAppRef::onHalfFFT),

//! sys msg
on_set_int_int ( CMD_SERVICE_DEACTIVE,          &CAppRef::onDeActive),
on_set_int_int ( CMD_SERVICE_EXIT_ACTIVE,       &CAppRef::onDeActive),
on_set_int_int ( CMD_SERVICE_ACTIVE,            &CAppRef::onActive),
on_set_void_void( CMD_SERVICE_ENTER_ACTIVE,     &CAppRef::on_enter_active ),
on_set_void_void( CMD_SERVICE_EXIT_ACTIVE,      &CAppRef::on_exit_active ),
end_of_entry()

CAppRef::CAppRef()
{
    mservName     = serv_name_ref;
    m_pRefDetails = NULL;
    mActive = false;

    addQuick(   MSG_APP_REF,
                QStringLiteral("ui/desktop/ref/icon/") );

}

void CAppRef::setupUi(CAppMain *pMain)
{   
    for(int i=0; i<REF_COUNT; i++)
    {
        CRefData *pRef = servRef::getRefData(i);
        Q_ASSERT(NULL != pRef);

        QString gnd = QString("R%1").arg(i + 1);
//        if( (i+1) > 9)
//        {
//            gnd = QString("A");
//        }

        //!main gnd
        pGnd[i] = new menu_res::RDsoVGnd(gnd);
        Q_ASSERT( pGnd[i] != NULL );

        pGnd[i]->setColor( servRef::getColor(pRef->getColorIndex()), Qt::black );
        pGnd[i]->setParent(pMain->getLeftBar());
        pGnd[i]->setViewGnd(240-1);
        pGnd[i]->setView(DSO_VGND_WIDTH, 480);
        pGnd[i]->setVisible(false);

        //!zoom gnd
        pGndZoom[i] = new menu_res::RDsoVGnd(gnd);
        Q_ASSERT( pGndZoom[i] != NULL );

        pGndZoom[i]->setColor( servRef::getColor(pRef->getColorIndex()), Qt::black );
        pGndZoom[i]->setParent(pMain->getLeftBar());
        pGndZoom[i]->setViewGnd(240-1);
        pGndZoom[i]->setView(DSO_VGND_WIDTH, 480);
        pGndZoom[i]->setVisible(false);

        pGndFFT[i] = new menu_res::RDsoVGnd(gnd);
        Q_ASSERT( pGndFFT[i] != NULL );

        pGndFFT[i]->setColor( servRef::getColor(pRef->getColorIndex()), Qt::black );
        pGndFFT[i]->setParent(pMain->getLeftBar());
        pGndFFT[i]->setViewGnd(240-1);
        pGndFFT[i]->setView(DSO_VGND_WIDTH, 480);
        pGndFFT[i]->setVisible(false);

        menu_res::RDsoGnd::mGndList.append(pGnd[i]);
        menu_res::RDsoGnd::mGndList.append(pGndZoom[i]);
        menu_res::RDsoGnd::mGndList.append(pGndFFT[i]);

        pGndX[i] = new menu_res::RDsoHGnd(gnd);
        Q_ASSERT( pGndX[i] != NULL );

        pGndY[i] = new menu_res::RDsoVGnd(gnd);
        Q_ASSERT( pGndY[i] != NULL );

        pGndX[i]->setView(DSO_VGND_WIDTH, 480);
        pGndX[i]->setViewGnd(240-1);
        pGndX[i]->setInvert(true);

        pGndY[i]->setView(DSO_VGND_WIDTH, 480);
        pGndY[i]->setViewGnd(240-1);

        pGndX[i]->setParent(pMain->getCentBar());
        pGndY[i]->setParent(pMain->getCentBar());

        //! attach view
        sysAttachUIView( pGnd[i], view_ygnd_main  );
        sysAttachUIView( pGndZoom[i], view_ygnd_zoom  );
        sysAttachUIView( pGndFFT[i], view_fft_zoom  );

        sysAttachUIView( pGndX[i], view_xy_xgnd );
        sysAttachUIView( pGndY[i], view_xy_ygnd );

        m_pLabel[i] = new menu_res::RDsoCHLabel();

        m_pLabel[i]->setColor(Qt::white);
        m_pLabel[i]->setParent(pMain->getCentBar());
        m_pLabel[i]->setView( 100, 480 );
        m_pLabel[i]->setViewGnd( 480 / 2 );
        m_pLabel[i]->hide();

        m_pLabelZoom[i] = new menu_res::RDsoCHLabel();

        m_pLabelZoom[i]->setColor(Qt::white);
        m_pLabelZoom[i]->setParent(pMain->getCentBar());
        m_pLabelZoom[i]->setView( 100, 480 );
        m_pLabelZoom[i]->setViewGnd( 480 / 2 );
        m_pLabelZoom[i]->hide();

        //! attach view
        sysAttachUIView( m_pLabel[i], view_yt_main );
        sysAttachUIView( m_pLabelZoom[i], view_yt_zoom );
    }

    m_pRefDetails = new CRefDetails(pMain->getCentBar());
    Q_ASSERT(m_pRefDetails);
    attachPopupWindow( &m_pRefDetails, MSG_REF_DETAILS );

    m_pRefDetails->hide();

}

void CAppRef::retranslateUi()
{

}

void CAppRef::buildConnection()
{
    for(int i=0; i<REF_COUNT; i++)
    {
        m_pVGND.append(pGnd[i]);
        //!gnd s
        connect(pGnd[i],
                SIGNAL(clicked(bool)),
                this,
                SLOT(onClick()));

        connect(pGndZoom[i],
                SIGNAL(clicked(bool)),
                this,
                SLOT(onClick()));

        connect(pGndFFT[i],
                SIGNAL(clicked(bool)),
                this,
                SLOT(onClick()));

        connect(pGndX[i],
                SIGNAL(clicked(bool)),
                this,
                SLOT(onClick()));

        connect(pGndY[i],
                SIGNAL(clicked(bool)),
                this,
                SLOT(onClick()));

        connect( pGnd[i],
                 SIGNAL(sigMove(int,int)),
                 this,
                 SLOT(onVMove(int,int)) );
    }
    connect(m_pRefDetails, SIGNAL(closeSig()),this,SLOT(postCloseSig()));
}

void CAppRef::registerSpy()
{
}

void CAppRef::onClick()
{
    menu_res::RDsoVGnd* pGndSender = (menu_res::RDsoVGnd*)sender();

    if( m_pVGND.contains(pGndSender))
    {
        int id = m_pVGND.indexOf(pGndSender);
        if( !mActive )
        {
            serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, (int)1 );
        }
        serviceExecutor::post( mservName, MSG_REF_CHANNEL, id );

        pGnd[id]->setAllInactive(menu_res::RDsoGnd::mGndList);
        pGnd[id]->setActive();
        pGndZoom[id]->setActive();
        pGndFFT[id]->setActive();

        m_pLabel[id]->setActive();
        m_pLabelZoom[id]->setActive();
    }
}

void CAppRef::on_enter_active()
{
    for(int i = 0; i<REF_COUNT; i++)
    {
        CRefData *pRef = servRef::getRefData(i);
        if(pRef->getOnOff())
        {
            mActive = true;
            int nChanID = 0;
            query(MSG_REF_CHANNEL, nChanID);

            pGnd[nChanID]->setAllInactive(menu_res::RDsoGnd::mGndList);
            pGnd[nChanID]->setActive();
            pGndZoom[nChanID]->setActive();
            pGndFFT[nChanID]->setActive();

            m_pLabel[nChanID]->setActive();
            m_pLabelZoom[nChanID]->setActive();
        }
    }
}

void CAppRef::on_exit_active()
{
    mActive = false;
}

void CAppRef::onVMove(int, int pos)
{
    menu_res::RDsoVGnd* pGnd = (menu_res::RDsoVGnd*)sender();

    if( m_pVGND.contains(pGnd))
    {
        int id = m_pVGND.indexOf(pGnd);
        if(id >=0 && id < REF_COUNT)
        {
            CRefData *pRef = servRef::getRefData(id);
            qlonglong scale  = pRef->getVScale();

            qlonglong offset = pRef->getVOffset() - pos * scale / scr_vdiv_dots;

            serviceExecutor::post( mservName, MSG_REF_CHANNEL, id );
            serviceExecutor::post( mservName, MSG_REF_POS, offset );
        }
    }
}

void CAppRef::onHMove(int, int)
{

}

void CAppRef::onDetails()
{
    m_pRefDetails->setVisible( !m_pRefDetails->isVisible() );
}

int CAppRef::onClear(int aec)
{
    onDataChange(aec);
    onLabel(aec);
    return ERR_NONE;
}

int CAppRef::onHorMode(int)
{
    int mode = 0;
    query(servRef::MSG_REF_HOR_MODE, mode);
    if( mode == Acquire_XY )
    {
        for(int i = 0; i<REF_COUNT; i++)
        {
            pGnd[i]->setShow( false );
            pGndZoom[i]->setShow( false );
            pGndFFT[i]->setShow( false );

            m_pLabel[i]->setShow( false );
            m_pLabelZoom[i]->setShow( false );
        }
    }
    return ERR_NONE;
}

int CAppRef::onHalfFFT()
{
    bool b = false;
    serviceExecutor::query(serv_name_math1,  MSG_MATH_S32FFTSCR,  b);

    if( !b )
    {
        for(int i=0; i<REF_COUNT; i++)
        {
            pGnd[i]->setShow(false);
            m_pLabel[i]->setShow( false );
        }
    }
    else
    {
        for(int i = 0; i<REF_COUNT; i++)
        {
            CRefData *pRef = servRef::getRefData(i);
            if(pRef->getOnOff())
            {
                pGnd[i]->setShow(true);
                bool bVisible = false;
                query(MSG_REF_LABEL_ONOFF,bVisible);
                m_pLabel[i]->setShow( bVisible );
            }
        }
    }
    return ERR_NONE;
}

int CAppRef::onDataChange(int aec)
{
    if(aec != cause_value) return ERR_NONE;

    //int  chanID = 0;
    //query(MSG_REF_CHANNEL,chanID);
    onRefresh(aec);
    onLabel(aec);

    for(int i = 0; i<REF_COUNT; i++)
    {
        CRefData *pRef = servRef::getRefData(i);
        if(pRef->getOnOff())
        {
            int nChanID = 0;
            query(MSG_REF_CHANNEL, nChanID);
            pGnd[nChanID]->setAllInactive(menu_res::RDsoGnd::mGndList);
            pGnd[nChanID]->setActive();
            pGndZoom[nChanID]->setActive();
            pGndFFT[nChanID]->setActive();
            m_pLabel[nChanID]->setActive();
            m_pLabelZoom[nChanID]->setActive();
        }
    }


    if(m_pRefDetails && m_pRefDetails->isVisible())
    {
        m_pRefDetails->update();
    }
    return ERR_NONE;
}


int CAppRef::onRefresh(int)
{
    bool bOpen = false;
    query(servRef::MSG_REF_SHOW, bOpen);
    if( !bOpen )
    {
        return ERR_NONE;
    }

    for(int i = 0; i<REF_COUNT; i++)
    {
        drawGND(i);
    }
    return ERR_NONE;
}

int CAppRef::onLabel(int)
{

    bool bVisible = false;
    query(MSG_REF_LABEL_ONOFF,bVisible);


    for(int i = 0; i<REF_COUNT; i++)
    {
        CRefData *pRef = servRef::getRefData(i);
//!For bug 510 ref切换当前通道，默认标签显示有误
//!使能显示标签位置不对
//!2017年12月26日20:22:29 by Yang,Zhou
 #if 0
        int nChanID = 0;
        query(MSG_REF_CHANNEL, nChanID);

        m_pLabel[i]->setShow(bVisible && pRef->getOnOff() && i == nChanID );
        m_pLabelZoom[i]->setShow(bVisible  && pRef->getOnOff() && i == nChanID);
#else
        bool b = false;
        serviceExecutor::query(serv_name_math1,  MSG_MATH_S32FFTSCR,  b);
        if(!b)
        {
            m_pLabel[i]->setShow(bVisible && pRef->getOnOff());
        }
        else
        {
            m_pLabel[i]->setShow(bVisible && pRef->getOnOff());
        }

        m_pLabelZoom[i]->setShow(bVisible  && pRef->getOnOff());
#endif

        if(pRef->getOnOff())
        {
            m_pLabel[i]->setText(pRef->getRefLabel());
            m_pLabel[i]->setColor( servRef::getColor(pRef->getColorIndex()));

            m_pLabelZoom[i]->setText(pRef->getRefLabel());
            m_pLabelZoom[i]->setColor( servRef::getColor(pRef->getColorIndex()));
        }

        {
            qlonglong offset = pRef->getVOffset();
            qlonglong scale  = pRef->getVScale();
            int pos  = (int)(offset * scr_vdiv_dots / scale);
            m_pLabel[i]->offset( pos );
            m_pLabelZoom[i]->offset( pos );
        }
    }

    return ERR_NONE;
}

int CAppRef::drawGND(int id)
{
    CRefData *pRef = servRef::getRefData(id);
    Q_ASSERT(NULL != pRef);

    qlonglong offset = pRef->getVOffset();
    qlonglong scale  = pRef->getVScale();
    bool      bOpen  = pRef->getOnOff();

    if(scale == 0)
    {
        return ERR_NONE;
    }
    //qDebug() << id << bOpen;
    /*****************************************************
    realoffset = offset / 1.0e6;
    realscale  = scale / 1.0e6;
    voff = (realoffset * 480 / (8 * realscale) );

    simplize the above and get the following
    ******************************************************/
    int pos  = (int)(offset * scr_vdiv_dots / scale);

    pGnd[id]->offset(pos);
    pGnd[id]->setColor(servRef::getColor(pRef->getColorIndex()), Qt::black);

    bool b = false;
    serviceExecutor::query(serv_name_math1,  MSG_MATH_S32FFTSCR,  b);
    if(!b)
    {
        pGnd[id]->setShow(b);
        m_pLabel[id]->setShow(b);
        pGndFFT[id]->setShow( bOpen);
    }
    else
    {
        pGnd[id]->setShow(bOpen);
        bool bVisible = false;
        query(MSG_REF_LABEL_ONOFF,bVisible);
        m_pLabel[id]->setVisible(bVisible);
    }

    pGndFFT[id]->offset(pos);
    pGndFFT[id]->setColor(servRef::getColor(pRef->getColorIndex()), Qt::black);
    pGndFFT[id]->setShow(bOpen);

    pGndZoom[id]->offset(pos);
    pGndZoom[id]->setColor(servRef::getColor(pRef->getColorIndex()), Qt::black);
    pGndZoom[id]->setShow(bOpen);

    return ERR_NONE;
}

int CAppRef::onActive(int)
{
    onDataChange(0);
    return ERR_NONE;
}

int CAppRef::onDeActive(int)
{
    if(m_pRefDetails && m_pRefDetails->isVisible())
    {
        m_pRefDetails->hide();
    }

    bool bOpen = false;
    query(servRef::MSG_REF_SHOW, bOpen);
    if( !bOpen )
    {
        for(int i = 0; i<REF_COUNT; i++)
        {
            pGnd[i]->setShow( false );
            pGndZoom[i]->setShow( false );
            pGndFFT[i]->setShow( false );

            m_pLabel[i]->setShow( false );
            m_pLabelZoom[i]->setShow( false );
        }
    }    
    return ERR_NONE;
}

void CAppRef::postCloseSig()
{
    serviceExecutor::post(serv_name_ref,
                           MSG_REF_DETAILS,
                           false);
}

