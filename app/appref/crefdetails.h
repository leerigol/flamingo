#ifndef CREFDETAILS_H
#define CREFDETAILS_H

#include <QWidget>
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;
class CRefDetails : public uiWnd
{
    Q_OBJECT
public:
    explicit CRefDetails(QWidget *parent = 0);
    ~CRefDetails();

public:
    void    loadSource(const QString &path);
    void    setColor( QColor &color, int hexRgb );
    void    paintEvent( QPaintEvent * );
    void    hideEvent(QHideEvent *) ;
signals:
    void closeSig();
private:
    static RModelWnd_Style _style;
    RImageButton   *m_pCloseButton;

private:
    int     m_x;
    int     m_y;
    int     m_width;
    int     m_height;
    int     m_rowh;

    QRect   mCloseRect;

    QColor  mFontColor, mTitlebgColor, mGray15, mGray27;

};


#endif // CREFLISAJOUS_H

