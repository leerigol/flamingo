#ifndef CAPPREF_H
#define CAPPREF_H

#include "crefdetails.h"
#include "../capp.h"
#include "../../menu/rmenus.h"

#include "../../service/servref/servref.h"


class CAppRef : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppRef();

public:
    void setupUi(CAppMain *pMain);
    void retranslateUi();
    void registerSpy();
    void buildConnection();

private:
    int  drawGND(int);

    void onDetails();
    int  onDataChange(int);

    int  onRefresh(int);
    int  onDeActive(int);
    int  onActive(int);
    int  onLabel(int);
    int  onColor(int);
    int  onClear(int);
    int  onHorMode(int);

    int  onHalfFFT();

private Q_SLOTS:
    void onClick();
    void on_enter_active();
    void on_exit_active();
    void onVMove(int,int);
    void onHMove(int,int);
    void postCloseSig();

private:
    CRefDetails *m_pRefDetails;

    menu_res::RDsoVGnd  *pGnd[REF_COUNT];
    menu_res::RDsoVGnd  *pGndZoom[REF_COUNT];
    menu_res::RDsoVGnd  *pGndFFT[REF_COUNT];

    menu_res::RDsoCHLabel *m_pLabel[REF_COUNT];
    menu_res::RDsoCHLabel *m_pLabelZoom[REF_COUNT];

    menu_res::RDsoVGnd  *pGndY[REF_COUNT];
    menu_res::RDsoHGnd  *pGndX[REF_COUNT];

    QList<menu_res::RDsoVGnd*> m_pVGND;
    bool    mActive;
};

#endif // CAPPREF_H

