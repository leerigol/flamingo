#include "capphori_style.h"

#include "../../meta/crmeta.h"

CAppHori_Style::CAppHori_Style()
{
}

void CAppHori_Style::load( const QString &strPath,
                           const r_meta::CMeta &meta )
{
    QString str;

    //! hgnd
    str = strPath + "hgnd/";
    meta.getMetaVal( str + "w", hGndW );
    meta.getMetaVal( str + "h", hGndH );
    meta.getMetaVal( str + "view_w", hGndViewW );
    meta.getMetaVal( str + "view_h", hGndViewH );

    meta.getMetaVal( str + "phy_x", hGndPhyX );
    meta.getMetaVal( str + "phy_y", hGndPhyY );
    meta.getMetaVal( str + "phy_w", hGndPhyW );
    meta.getMetaVal( str + "phy_h", hGndPhyH );

    meta.getMetaVal( str + "zoom_phy_x", hZoomGndPhyX );
    meta.getMetaVal( str + "zoom_phy_y", hZoomGndPhyY );
    meta.getMetaVal( str + "zoom_phy_w", hZoomGndPhyW );
    meta.getMetaVal( str + "zoom_phy_h", hZoomGndPhyH );

    //! exp gnd
    str = strPath + "hgnd_exp/";
    meta.getMetaVal( str + "w", hExpGndW );
    meta.getMetaVal( str + "h", hExpGndH );
    meta.getMetaVal( str + "view_w", hExpGndViewW );
    meta.getMetaVal( str + "view_h", hExpGndViewH );

    meta.getMetaVal( str + "phy_x", hExpGndPhyX );
    meta.getMetaVal( str + "phy_y", hExpGndPhyY );
    meta.getMetaVal( str + "phy_w", hExpGndPhyW );
    meta.getMetaVal( str + "phy_h", hExpGndPhyH );

    meta.getMetaVal( str + "zoom_phy_x", hZoomExpGndPhyX );
    meta.getMetaVal( str + "zoom_phy_y", hZoomExpGndPhyY );
    meta.getMetaVal( str + "zoom_phy_w", hZoomExpGndPhyW );
    meta.getMetaVal( str + "zoom_phy_h", hZoomExpGndPhyH );

    //! zoom area
    str = strPath + "zoom_area/";
    meta.getMetaVal( str + "view_w", hZoomViewW );
    meta.getMetaVal( str + "view_h", hZoomViewH );

    meta.getMetaVal( str + "phy_x", hZoomPhyX );
    meta.getMetaVal( str + "phy_y", hZoomPhyY );
    meta.getMetaVal( str + "phy_w", hZoomPhyW );
    meta.getMetaVal( str + "phy_h", hZoomPhyH );
    meta.getMetaVal( str + "geo", hZoomGeo );

    //! color
    meta.getMetaVal( str + "color", zoomColor );
}
