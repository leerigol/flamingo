#ifndef CZONEZOOM_H
#define CZONEZOOM_H
#include <QWidget>
#include "../appmain/touchzone.h"

class CZoneZoom: public QObject
{
    Q_OBJECT
public:
    explicit   CZoneZoom(TouchZone *touch);

public:
    QRect        getZone();

private:
    TouchZone   *m_pTouchZone;


protected Q_SLOTS:
    void  on_mouse_move_event(QPoint);
    void  on_mouse_pressEvent(QPoint);
    void  on_mouse_releaseEvent(QPoint );

Q_SIGNALS:
    void  sig_zone_active(int);
};

#endif // CZONEZOOM_H
