#include "capphori.h"

#include "../../service/servutility/servutility.h"

void CAppHori::on_zone_active(int id)
{
    Q_ASSERT( m_pZoneZoom != NULL);
    QRect viewRect  = m_pZoneZoom->getZone();

    int viewW = qAbs( viewRect.width()  );
    int viewH = qAbs( viewRect.height() );

    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<viewW<<viewH;
    if ( (viewW <= 0) || (viewH <= 0) )
    {
        return;
    }

    bool isZoomOn = false;
    serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, isZoomOn);

    //zoom模式下，在main区通过区域界面修改offset不可用，只有在
    //zoom区域可以修改offset
//    if(isZoomOn)
//    {
//        return;
//    }

    //! 计算窗体缩放比例
    float xRatio = (float)viewW / wave_width;
    float yRatio = (float)viewH / wave_height;

    int vScal   = 1;
    int vOffset = 0;

    qint64 hScal   = 1;
    qint64 hOffset = 0;

    //! ch
    if (  (ZONE_MENU_WAVEFORM_ZOOM == id)
          ||(ZONE_MENU_VERT_ZOOM == id) )
    {
        for (int ch = chan1; ch <= chan4; ch++)
        {
            QString servName = QString("chan%1").arg(ch);

            query(servName, MSG_CHAN_SCALE_VALUE,  vScal);
            query(servName, MSG_CHAN_OFFSET,       vOffset);

            vOffset = vOffset + (viewRect.y()+viewRect.height()/2-wave_height/2)/60.0*vScal;
            vScal   = vScal   * yRatio;

            serviceExecutor::post(servName, MSG_CHAN_SCALE_VALUE,  vScal);
            serviceExecutor::post(servName, MSG_CHAN_OFFSET,       vOffset);
        }
    }

    //!hor
    if (  (ZONE_MENU_WAVEFORM_ZOOM == id)
          ||(ZONE_MENU_HOR_ZOOM == id) )
    {
        if( isZoomOn )
        {
            /*query(serv_name_hori, MSG_HORI_ZOOM_SCALE,  hScal);
            query(serv_name_hori, MSG_HORI_ZOOM_OFFSET, hOffset);

            hOffset = hOffset + (viewRect.x()+viewRect.width()/2-wave_width/2)/100.0*hScal;
            hScal   = hScal * xRatio;

            serviceExecutor::post(serv_name_hori, MSG_HORI_ZOOM_SCALE,   hScal);
            serviceExecutor::post(serv_name_hori, MSG_HORI_ZOOM_OFFSET,  hOffset);*/
            sysKeyIncrease(r_key_hscale,1 );
        }
        else
        {
            query(serv_name_hori, MSG_HORI_MAIN_SCALE,  hScal);
            query(serv_name_hori, MSG_HORI_MAIN_OFFSET, hOffset);

            hOffset = hOffset + (viewRect.x()+viewRect.width()/2-wave_width/2)/100.0*hScal;
            hScal   = hScal * xRatio;

            serviceExecutor::post(serv_name_hori, MSG_HORI_MAIN_SCALE,   hScal);
            serviceExecutor::post(serv_name_hori, MSG_HORI_MAIN_OFFSET,  hOffset);
        }
    }
}

void CAppHori::onCfgMove(QPoint p)
{
    m_pMainHoriCfg->move(p);
    m_pZoomHoriCfg->move(p);
}
