#ifndef CAPPHORI_STYLE_H
#define CAPPHORI_STYLE_H

#include <QString>
#include <QColor>

#include "../../menu/rmenus.h"

class CAppHori_Style : public menu_res::RUI_Style
{
public:
    CAppHori_Style();

public:
    virtual void load( const QString &strPath,
                       const r_meta::CMeta &meta );

public:
    //! gnd
    int hGndW, hGndH;
    int hGndViewW, hGndViewH;
    int hGndPhyX, hGndPhyY, hGndPhyW, hGndPhyH;

    //! expand gnd
    int hExpGndW, hExpGndH;
    int hExpGndViewW, hExpGndViewH;
    int hExpGndPhyX, hExpGndPhyY, hExpGndPhyW, hExpGndPhyH;

    //! zoom gnd
    int hZoomGndPhyX, hZoomGndPhyY, hZoomGndPhyW, hZoomGndPhyH;

    //! zoom expand gnd
    int hZoomExpGndPhyX, hZoomExpGndPhyY, hZoomExpGndPhyW, hZoomExpGndPhyH;

    //! zoom area
    int hZoomViewW, hZoomViewH;
    int hZoomPhyX, hZoomPhyY, hZoomPhyW, hZoomPhyH;
    QRect hZoomGeo;

    QColor zoomColor;

};

#endif // CAPPHORI_STYLE_H
