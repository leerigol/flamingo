#include "zoneZoom.h"

CZoneZoom::CZoneZoom(TouchZone *touch)
{
    Q_ASSERT(touch != NULL);
    m_pTouchZone = touch;
    m_pTouchZone->setMenuVisible(ZONE_MENU_HOR_ZOOM,      true);
    m_pTouchZone->setMenuVisible(ZONE_MENU_VERT_ZOOM,     true);
    m_pTouchZone->setMenuVisible(ZONE_MENU_WAVEFORM_ZOOM, true);

    connect( m_pTouchZone,
             SIGNAL( sig_mouse_move_event(QPoint ) ),
             this,
             SLOT( on_mouse_move_event(QPoint) ) );

    connect( m_pTouchZone,
             SIGNAL( sig_mouse_pressEvent(QPoint ) ),
             this,
             SLOT( on_mouse_pressEvent(QPoint) ) );

    connect( m_pTouchZone,
             SIGNAL( sig_mouse_releaseEvent(QPoint ) ),
             this,
             SLOT( on_mouse_releaseEvent(QPoint) ) );

    connect( m_pTouchZone,
             SIGNAL( sig_menu_activated(int) ),
             this,
             SIGNAL( sig_zone_active(int) ) );
}

QRect CZoneZoom::getZone()
{
    Q_ASSERT(m_pTouchZone != NULL);
    return m_pTouchZone->getZone();
}

void CZoneZoom::on_mouse_move_event(QPoint /*event*/)
{

}

void CZoneZoom::on_mouse_pressEvent(QPoint /*event*/)
{

}

void CZoneZoom::on_mouse_releaseEvent(QPoint /*event*/)
{

}

