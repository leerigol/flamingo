#ifndef CAPPHORI_H
#define CAPPHORI_H

#include <QtCore>
#include <QtWidgets>

#include "../capp.h"
#include "../../menu/rmenus.h"  //! controlers

                                //! wnds
#include "../../menu/dsownd/wndchcfg.h"

#include "capphori_style.h"

#include "../../service/servhori/servhori.h"
#include "zoneZoom.h"

class CAppHori : public CApp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    CAppHori();

public:
    virtual void setupUi( CAppMain *pMain );

    virtual void buildConnection();
    virtual void resize();

    virtual void boundtoService( service *pServ );
    virtual void retranslateUi();

public:
    void on_enable_x_changed( );
    void on_scale_x_changed( );
    void on_offset_x_changed( );
    void on_expand_changed( );
    void on_onoff_changed( );

    void on_zoom_changed();
    void on_zoom_scale_changed();

    void on_timemode_changed();
    void on_hori_changed();

protected:
    qlonglong getTrigPos();
    qlonglong getTrigOffset();

    qlonglong getZoomTrigPos();
    qlonglong getZoomTrigOffset();
    qlonglong getZoomExpandOffset();

    void updateMemBar();

    void on_horiRangeChanged();
    void on_horiZoomRangeChanged();

    QString toString( ControlStatus stat );

protected Q_SLOTS:
    void on_run_click();

    void on_hori_header_click( QWidget *pCaller = NULL );
    void on_main_hori_content_click( QWidget *pCaller );
    void on_disEnable_click();

    void on_main_gnd_click();

    void onMainMove( int xdist, int ydist );
    void onZoomMove( int xdist, int ydist );

    void onMainExpandMove( int xdist, int ydist );
    void onZoomExpandMove( int xdist, int ydist );

    void onStatusTimeout();

    //! scale & offset
    void on_main_scale_real( DsoReal );
    void on_main_offset_real( DsoReal );
    void on_main_tune( int id );

    void on_zoom_scale_real( DsoReal );
    void on_zoom_offset_real( DsoReal );
    void on_zoom_tune( int id );

    void on_zone_active( int id );

    void onCfgMove( QPoint );

private:
    menu_res::RDsoHGnd *m_pHGnd;            //!< time offset
    menu_res::RDsoHGnd *m_pExpandGnd;       //!< expand center

    menu_res::RDsoHGnd *m_pZoomHGnd;
    menu_res::RDsoHGnd *m_pZoomExpandGnd;
                                            //!< zoom area
    menu_res::RDsoZoomArea *m_pZoomArea;

    menu_res::RDsoHScale *m_pMainScale;     //!< scale offset
    menu_res::RDsoHOffset *m_pMainOffset;

    menu_res::RDsoSysStatus *m_pSysStatus;


    menu_res::RDsoXyScale *m_pXyScale;      //! xy scale
    menu_res::RDsoRollScale *m_pRollScale;  //! roll scale
    menu_res::RDsoHOffset *m_pRollOffset;

    menu_res::RDsoImageButton *m_pRunStop;
    menu_res::RDsoBaseButton *m_pMembarFrame;

    menu_res::RImageButton *m_pXyRollIcon;
    QString mXyIcon, mRollIcon;

    menu_res::RImageButton *m_pXyRollDisIcon;
    QString mXyDisIcon;

    menu_res::RDsoMemBar *m_pMemBar;
    menu_res::WndChCfg *m_pMainHoriCfg;     //! scale/offset
    menu_res::WndChCfg *m_pZoomHoriCfg;

    QTimer *m_pStatusTimer;                 //! satus timer

    //!zone zoom
    CZoneZoom *m_pZoneZoom;

private:
    CAppHori_Style _style;

private:
    servHori *m_pServHori;
    int mScaleKey, mOffsetKey;
};

#endif // CAPPHORI_H
