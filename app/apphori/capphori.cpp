
#include "../../service/servicefactory.h"
#include "../../service/service.h"
#include "../../service/service_name.h"

#include "../../service/servhori/servhori.h"

#include "../../gui/cguiformatter.h"

#include "../../app/appdisplay/cgrid.h"

#include "../appmain/cappmain.h"
#include "../../meta/crmeta.h"

#include "capphori.h"

IMPLEMENT_CMD( CApp, CAppHori )
start_of_entry()

on_set_void_void( MSG_HORIZONTAL_RUN, &CAppHori::on_onoff_changed),
on_set_void_void( MSG_HORIZONTAL_ACQNORM_EXPAND, &CAppHori::on_expand_changed),
on_set_void_void( MSG_HORIZONTAL_ACQNORM_EXPAND_USER, &CAppHori::on_expand_changed),

on_set_void_void( MSG_HORI_MAIN_SCALE, &CAppHori::on_scale_x_changed ),
on_set_void_void( MSG_HORI_MAIN_SCALE, &CAppHori::on_offset_x_changed ),
on_set_void_void( MSG_HORI_MAIN_SCALE, &CAppHori::on_expand_changed),

on_set_void_void( MSG_HORI_MAIN_OFFSET, &CAppHori::on_offset_x_changed ),
on_set_void_void( MSG_HORI_MAIN_OFFSET, &CAppHori::on_expand_changed ),


//! zoom
on_set_void_void( MSG_HOR_ZOOM_ON,      &CAppHori::on_zoom_changed ),
on_set_void_void( MSG_HORI_ZOOM_SCALE,  &CAppHori::on_zoom_changed ),
on_set_void_void( MSG_HORI_ZOOM_OFFSET, &CAppHori::on_zoom_changed ),

on_set_void_void( MSG_HOR_ZOOM_ON, &CAppHori::on_offset_x_changed ),

on_set_void_void( MSG_HOR_ZOOM_ON, &CAppHori::on_zoom_scale_changed ),
on_set_void_void( MSG_HORI_ZOOM_SCALE, &CAppHori::on_zoom_scale_changed ),

on_set_void_void( MSG_HORI_ZOOM_OFFSET, &CAppHori::on_offset_x_changed ),
on_set_void_void( MSG_HORI_ZOOM_OFFSET, &CAppHori::on_expand_changed ),

on_set_void_void( servHori::cmd_hori_changed, &CAppHori::on_hori_changed ),
on_set_void_void( MSG_HOR_TIME_MODE, &CAppHori::on_timemode_changed ),

end_of_entry()

CAppHori::CAppHori()
{
    mName  = serv_name_hori;

    mservName = serv_name_hori;

    m_pStatusTimer = NULL;

    m_pMainHoriCfg = NULL;
    m_pZoomHoriCfg = NULL;

    m_pServHori = NULL;

    mScaleKey = R_Pkey_TIME_SCALE;
    mOffsetKey = R_Pkey_TIME_OFFSET;

    m_pZoneZoom = NULL;
}

void CAppHori::setupUi( CAppMain *pMain )
{
    Q_ASSERT( NULL != pMain );

    QWidget *pWidget;

    r_meta::CAppMeta meta;

    //! load resource
    _style.loadResource("ui/hori/");

    //! parent
    pWidget = pMain->getCentBar();
    Q_ASSERT( NULL != pWidget );

    //! zoom area
    m_pZoomArea = new menu_res::RDsoZoomArea();
    Q_ASSERT( NULL != m_pZoomArea );
    m_pZoomArea->setPhyRect( _style.hZoomPhyX, _style.hZoomPhyY,
                             _style.hZoomPhyW, _style.hZoomPhyH );
    m_pZoomArea->setViewRect( _style.hZoomViewW );
    m_pZoomArea->setColor( _style.zoomColor   );
    m_pZoomArea->setParent( pMain->getBackground() );

    m_pZoomArea->setGeometry( _style.hZoomGeo );

    //! gnd
    m_pHGnd = new menu_res::RDsoHGnd("T",
                                     menu_res::RDsoGndTag::to_down,
                                     menu_res::RDsoGndTag::cone,
                                     _style.hGndW, _style.hGndH,
                                     _style.hGndH, _style.hGndW );
    m_pExpandGnd = new menu_res::RDsoHGnd("",
                                        menu_res::RDsoGndTag::to_down,
                                        menu_res::RDsoGndTag::triangle,
                                      _style.hExpGndW, _style.hExpGndH,
                                      _style.hExpGndH, _style.hExpGndH
                                          );
    Q_ASSERT( m_pHGnd != NULL );
    Q_ASSERT( m_pExpandGnd != NULL );

    m_pHGnd->setColor( Hori_Color, Qt::black );
    m_pExpandGnd->setColor( Hori_Color, Qt::black );

    //! zoom gnd
    m_pZoomHGnd = new menu_res::RDsoHGnd("T",
                                     menu_res::RDsoGndTag::to_down,
                                     menu_res::RDsoGndTag::cone,
                                     _style.hGndW, _style.hGndH,
                                     _style.hGndH, _style.hGndW );
    m_pZoomExpandGnd = new menu_res::RDsoHGnd("",
                                        menu_res::RDsoGndTag::to_down,
                                        menu_res::RDsoGndTag::triangle,
                                        _style.hExpGndW, _style.hExpGndH,
                                        _style.hExpGndH, _style.hExpGndH
                                              );
    Q_ASSERT( m_pZoomHGnd != NULL );
    Q_ASSERT( m_pZoomExpandGnd != NULL );

    m_pZoomHGnd->setColor( Hori_Color, Qt::black );
    m_pZoomExpandGnd->setColor( Hori_Color, Qt::black );

    //! main ui config
    m_pHGnd->setView( _style.hGndViewW, _style.hGndViewH );
    m_pHGnd->setViewGnd( _style.hGndViewW / 2 );
    m_pHGnd->setParent( pWidget );

    m_pExpandGnd->setView( _style.hExpGndViewW, _style.hExpGndViewH );
    m_pExpandGnd->setViewGnd( _style.hExpGndViewW / 2 );
    m_pExpandGnd->setParent( pWidget );

    m_pHGnd->setHelp( mservName, MSG_HORI_MAIN_OFFSET );
    m_pExpandGnd->setHelp( mservName, MSG_HORIZONTAL_ACQNORM_EXPAND );

    //! zoom ui
    m_pZoomHGnd->setView( _style.hGndViewW, _style.hGndViewH );
    m_pZoomHGnd->setViewGnd( _style.hGndViewW / 2 );

    m_pZoomExpandGnd->setView( _style.hExpGndViewW, _style.hExpGndViewH );
    m_pZoomExpandGnd->setViewGnd( _style.hExpGndViewW/2 );

    m_pZoomHGnd->setParent( pWidget );
    m_pZoomExpandGnd->setParent( pWidget );

    m_pZoomHGnd->setHelp( mservName, MSG_HORI_ZOOM_OFFSET );
    m_pZoomExpandGnd->setHelp( mservName, MSG_HORIZONTAL_ACQNORM_EXPAND );

    //! icons
    //! attach icon
    QString iconPath, iconL, iconM, iconR;
    meta.getMetaVal("apphori/ticon/path", iconPath );
    meta.getMetaVal("apphori/ticon/left", iconL );
    meta.getMetaVal("apphori/ticon/mid", iconM );
    meta.getMetaVal("apphori/ticon/right", iconR );
    m_pHGnd->attachIcon( 0, iconPath + iconL, iconPath + iconM, iconPath + iconR );
    m_pZoomHGnd->attachIcon( 0, iconPath + iconL, iconPath + iconM, iconPath + iconR );
    m_pHGnd->setIcon(0);
    m_pZoomHGnd->setIcon(0);

    //! scale, offset
    m_pMainScale = new menu_res::RDsoHScale();
    Q_ASSERT( NULL != m_pMainScale );
    m_pXyScale = new menu_res::RDsoXyScale();
    Q_ASSERT( NULL != m_pXyScale );
    m_pRollScale = new menu_res::RDsoRollScale();
    Q_ASSERT( NULL != m_pRollScale );

    m_pMainOffset = new menu_res::RDsoHOffset( "ui/hori/main_offset/" );
    Q_ASSERT( NULL != m_pMainOffset );
    m_pRollOffset = new menu_res::RDsoHOffset( "ui/hori/roll_offset/" );
    Q_ASSERT( NULL != m_pRollOffset );

    m_pSysStatus = new menu_res::RDsoSysStatus();
    Q_ASSERT( NULL != m_pSysStatus );

    m_pRunStop = new menu_res::RDsoImageButton();
    Q_ASSERT( NULL != m_pRunStop );
    m_pRunStop->setCheckable( true );

    m_pMembarFrame = new menu_res::RDsoBaseButton();
    Q_ASSERT( NULL != m_pMembarFrame );

    m_pMemBar = new menu_res::RDsoMemBar();
    Q_ASSERT( NULL != m_pMemBar );

    m_pXyRollIcon = new menu_res::RImageButton();
    Q_ASSERT( NULL != m_pXyRollIcon );

    m_pXyRollDisIcon = new menu_res::RImageButton();
    Q_ASSERT( NULL != m_pXyRollDisIcon );

    pWidget = pMain->getTopBar();
    Q_ASSERT( NULL != pWidget );

    m_pMainScale->setParent( pWidget );
    m_pXyScale->setParent( pWidget );
    m_pRollScale->setParent( pWidget );
    m_pMainOffset->setParent( pWidget );
    m_pRollOffset->setParent( pWidget );
    m_pSysStatus->setParent( pWidget );

    m_pRunStop->setParent( pWidget );
    m_pMembarFrame->setParent( pWidget );
    m_pMemBar->setParent( m_pMembarFrame );

    m_pXyRollIcon->setParent( pWidget );
    m_pXyRollDisIcon->setParent(pWidget);
    //! help
    m_pMainScale->setHelp( mservName, MSG_HORIZONTAL_TIMESCALE );
    m_pMainOffset->setHelp( mservName, MSG_HORIZONTAL_TIMEOFFSET );
    m_pRollOffset->setHelp( mservName, MSG_HORIZONTAL_TIMEOFFSET );
    m_pXyScale->setHelp( mservName, MSG_HOR_ACQ_SARATE );
    m_pRollScale->setHelp( mservName, MSG_HOR_ACQ_SARATE );

    m_pRunStop->setHelp( mservName, MSG_HORIZONTAL_RUN );

    //! load resource
    m_pRunStop->loadResource("ui/runstop/");
    m_pMembarFrame->loadResource("ui/mem_btn/");

    QRect tRect;
    meta.getMetaVal( "apphori/membar", tRect );
    m_pMemBar->setGeometry( tRect );

    meta.getMetaVal( "apphori/xyroll_bar", tRect );
    m_pXyRollIcon->setGeometry( tRect );

    meta.getMetaVal( "apphori/xydisicon", tRect );
    m_pXyRollDisIcon->setGeometry( tRect );

    meta.getMetaVal( "apphori/xy_icon", mXyIcon );
    meta.getMetaVal( "apphori/roll_icon", mRollIcon );

    meta.getMetaVal( "apphori/disicon", mXyDisIcon );

    //! attach view
    sysAttachUIView( m_pExpandGnd, view_tgnd_main );
    sysAttachUIView( m_pHGnd, view_h_main );

    sysAttachUIView( m_pZoomExpandGnd, view_tgnd_zoom );
    sysAttachUIView( m_pZoomHGnd, view_h_zoom );

    m_pExpandGnd->setShow( true );
    m_pHGnd->setShow(true);

    m_pZoomExpandGnd->setShow( true );
    m_pZoomHGnd->setShow( true );

    //! visible
    m_pMainScale->setVisible( true );
    m_pXyScale->setVisible( false );
    m_pRollScale->setVisible( false );
    m_pRollOffset->setVisible( false );
    m_pMainOffset->setVisible( true );
    m_pSysStatus->setVisible( true );

    m_pRunStop->setVisible( true );
    m_pMembarFrame->setVisible( true );
    m_pMemBar->setVisible( true );

    //! timer
    m_pStatusTimer = new QTimer(this);

    //! wnd
    m_pMainHoriCfg = new menu_res::WndChCfg( pMain->getCentBar() );
    Q_ASSERT( NULL != m_pMainHoriCfg );
    m_pZoomHoriCfg = new menu_res::WndChCfg( pMain->getCentBar() );
    Q_ASSERT( NULL != m_pZoomHoriCfg );

    m_pZoneZoom = new CZoneZoom(pMain->getTouchZoneBar());
    Q_ASSERT(m_pZoneZoom != NULL);

}

void CAppHori::buildConnection()
{
    //! click
    connect( m_pRunStop, SIGNAL(clicked(bool)),
             this, SLOT(on_run_click()) );

    connect( m_pMainScale, SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT(on_hori_header_click(QWidget*)) );
    connect( m_pMainScale, SIGNAL(sig_content_clicked(QWidget*)),
             this, SLOT(on_main_hori_content_click(QWidget*)) );

    connect( m_pXyScale, SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT(on_hori_header_click(QWidget*)) );

    connect( m_pRollScale, SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT(on_hori_header_click(QWidget*)) );
    connect( m_pRollScale, SIGNAL(sig_content_clicked(QWidget*)),
             this, SLOT(on_main_hori_content_click(QWidget*)) );

    connect( m_pMainOffset, SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT(on_hori_header_click(QWidget*)) );
    connect( m_pRollOffset, SIGNAL(sig_header_clicked(QWidget*)),
             this, SLOT(on_hori_header_click(QWidget*)) );

    connect( m_pMainOffset, SIGNAL(sig_content_clicked(QWidget*)),
             this, SLOT(on_main_hori_content_click(QWidget*)) );
    connect( m_pRollOffset, SIGNAL(sig_content_clicked(QWidget*)),
             this, SLOT(on_main_hori_content_click(QWidget*)) );

    //! move
    connect( m_pHGnd, SIGNAL(sigMove(int,int)),
             this, SLOT(onMainMove(int,int)) );
    connect( m_pZoomHGnd, SIGNAL(sigMove(int,int)),
             this, SLOT(onMainExpandMove(int,int)) );

    connect( m_pExpandGnd, SIGNAL(sigMove(int,int)),
             this, SLOT(onZoomMove(int,int)) );
    connect( m_pZoomExpandGnd, SIGNAL(sigMove(int,int)),
             this, SLOT(onZoomExpandMove(int,int)) );

    connect( m_pMainHoriCfg, SIGNAL(sig_move(QPoint)),
             this, SLOT(onCfgMove(QPoint)) );
    connect( m_pZoomHoriCfg, SIGNAL(sig_move(QPoint)),
             this, SLOT(onCfgMove(QPoint)) );

//#ifndef _DEBUG
    //! timeout
    connect( m_pStatusTimer, SIGNAL(timeout()),
             this, SLOT(onStatusTimeout()) );
    m_pStatusTimer->start( 500 );
//#endif

    connect( m_pZoneZoom,
             SIGNAL(sig_zone_active(int)),
             this,
             SLOT(on_zone_active(int)) );

}

void CAppHori::resize()
{
    on_offset_x_changed(  );

    on_expand_changed(  );
}

void CAppHori::retranslateUi()
{
    if ( m_pMainHoriCfg && m_pMainHoriCfg->isVisible() )
    {
         m_pMainHoriCfg->redraw();
    }

    if ( m_pZoomHoriCfg && m_pZoomHoriCfg->isVisible() )
    {
         m_pZoomHoriCfg->redraw();
    }
}

void CAppHori::boundtoService( service *pServ )
{
    Q_ASSERT( pServ != NULL );

    //! convert
    m_pServHori = dynamic_cast< servHori*>( pServ );
    Q_ASSERT( m_pServHori != NULL );
}

void CAppHori::on_enable_x_changed()
{
}
void CAppHori::on_scale_x_changed()
{
    float realVal;
    serviceExecutor::query( mservName,
                            servHori::cmd_main_scale_real,
                            realVal );

    m_pMainScale->setScale( realVal );
    m_pXyScale->setScale( realVal );
    m_pRollScale->setScale( realVal );

    qlonglong length;
    serviceExecutor::query( mservName,
                            servHori::cmd_ch_length,
                            length );
    m_pMainScale->setDepth( length );
    m_pXyScale->setDepth( length );
    m_pRollScale->setDepth( length );

    qlonglong sa;
    serviceExecutor::query( mservName,
                            servHori::cmd_ch_sa,
                            sa );
    m_pMainScale->setSaRate( sa * 1.0e-6 );
    m_pXyScale->setSaRate( sa * 1.0e-6 );
    m_pRollScale->setSaRate( sa * 1.0e-6 );

    //! main scale
//    qint64 scale = m_pServHori->getMainScale();
//    m_pMainHoriCfg->setScale( DsoReal(scale, E_N12));

//    long long   horiZoomScale = 0;
//    serviceExecutor::query( serv_name_hori, MSG_HORI_ZOOM_SCALE,  horiZoomScale );
//    m_pZoomHoriCfg->setOffset(  DsoReal(horiZoomScale, E_N12));
}

void CAppHori::on_offset_x_changed()
{
    float offsetVal;

    //! offset
    if ( m_pServHori->getViewMode() == Horizontal_Main )
    {
        serviceExecutor::query( mservName,
                            servHori::cmd_main_offset_real,
                            offsetVal );
    }
    //! 无论是zoom模式还是非zoom模式，右上角的图标内都显示主时基下的偏移
    else
    {
        serviceExecutor::query( mservName,
                             servHori::cmd_zoom_offset_real,
                             offsetVal );
    }

    m_pMainOffset->setOffset( offsetVal );
    m_pRollOffset->setOffset( offsetVal );

    m_pHGnd->offset( getTrigOffset() );
    m_pZoomHGnd->offset( getZoomTrigOffset() );

    m_pZoomExpandGnd->offset( getZoomExpandOffset() );

    //for GND sync with wave
    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void CAppHori::on_expand_changed()
{
    qlonglong gnd;
    gnd = m_pServHori->getExpandGnd();

    m_pExpandGnd->offset( -gnd );
    m_pZoomExpandGnd->offset( getZoomExpandOffset() );
}

void CAppHori::on_onoff_changed( )
{
    int stat;
    serviceExecutor::query( mservName, MSG_HORIZONTAL_RUN, stat );

    //! run => checked
    m_pRunStop->setChecked( stat != Control_Stop );

    int timeMode;
    query( MSG_HOR_TIME_MODE, timeMode );
    AcquireTimemode acqTimeMode = (AcquireTimemode)timeMode;
    if ( acqTimeMode == Acquire_ROLL )
    {
        if(!m_pRunStop->isChecked())
        {
            m_pXyRollDisIcon->lower();
        }
        else
        {
            m_pXyRollDisIcon->raise();
        }
    }
    else
    {
        m_pXyRollDisIcon->raise();
    }

    //bug2329 by hxh
    {
        on_horiRangeChanged();
        on_horiZoomRangeChanged();
    }
}

#define min_zoom_window_width   7
void CAppHori::on_zoom_changed()
{
    bool bZoom;
    query( MSG_HOR_ZOOM_ON, bZoom );
    if(bZoom)
    {
        if(m_pMainHoriCfg->isVisible())
        {
            m_pMainHoriCfg->hide();
            m_pZoomHoriCfg->show();
        }
    }
    else
    {
        if(m_pZoomHoriCfg->isVisible())
        {
            m_pZoomHoriCfg->hide();
            m_pMainHoriCfg->show();
        }
    }

    m_pZoomArea->setVisible( bZoom );

//    m_pMemBar->setZoom( bZoom );
    m_pZoomHGnd->setVisible( bZoom );
    m_pZoomExpandGnd->setVisible( bZoom );

    //m_pZoomScale->setVisible( bZoom );
    //CGrid::getCGridInstance()->getZoomScale()->setVisible( bZoom );

//    m_pMainScale->setSplitEnabled( !bZoom );
//    m_pMainOffset->setSplitEnabled( ! bZoom );

    if ( bZoom )
    {
        //! zoom delay on main delay
        qlonglong tLeft, tRight;
        tLeft = m_pServHori->getZoomOffset() - hori_div * m_pServHori->getZoomScale()/2;
        tRight = m_pServHori->getZoomOffset() + hori_div * m_pServHori->getZoomScale()/2;

        //! main Start/End time
        qlonglong tS, tE;
        tS = m_pServHori->getMainOffset() - hori_div * m_pServHori->getMainScale()/2;
        tE = m_pServHori->getMainOffset() + hori_div * m_pServHori->getMainScale()/2;

        int leftLen, rightLen;
        leftLen = (tLeft - tS)*adc_hdiv_dots/m_pServHori->getMainScale();
        rightLen = ( tE - tRight)*adc_hdiv_dots/m_pServHori->getMainScale();

        //! tune left && right region
        int delta = adc_hdiv_dots * hori_div - (leftLen + rightLen);
        Q_ASSERT( delta >= 0 );
        if ( delta < min_zoom_window_width )
        {
            if ( leftLen > min_zoom_window_width /2 )
            { leftLen -= min_zoom_window_width/2; }

            if ( rightLen > min_zoom_window_width / 2 )
            { rightLen -= min_zoom_window_width / 2; }
        }

        m_pZoomArea->setMask( leftLen, rightLen );
    }
}

void CAppHori::on_zoom_scale_changed()
{
    float scale = m_pServHori->getZoomScaleReal();

    CGrid::getCGridInstance()->getZoomScale()->setValue( scale );
}

void CAppHori::on_timemode_changed()
{
    int timeMode;

    query( MSG_HOR_TIME_MODE, timeMode );

    AcquireTimemode acqTimeMode;
    acqTimeMode = (AcquireTimemode)timeMode;

    //! into roll
    if ( acqTimeMode == Acquire_ROLL )
    {
        m_pHGnd->setShow( true );
        m_pExpandGnd->setShow( true );
        m_pExpandGnd->move( _style.hExpGndViewW );

        m_pExpandGnd->setShow( true );

        m_pMemBar->setVisible( false );
        m_pMainOffset->setVisible( false );
        m_pRollOffset->setVisible( true );

        m_pMembarFrame->setVisible( false );
        m_pXyRollIcon->setVisible( true );
        m_pXyRollIcon->setImage( mRollIcon );

        if(!m_pRunStop->isChecked())
        {
            m_pXyRollDisIcon->lower();
        }
        else
        {
            m_pXyRollDisIcon->raise();
        }
        m_pXyRollDisIcon->setVisible( true );
        m_pXyRollDisIcon->setImage( mXyDisIcon );



        m_pMainScale->setVisible( false );
        m_pXyScale->setVisible( false );
        m_pRollScale->setVisible( true );
    }
    else if ( acqTimeMode == Acquire_XY )
    {
        m_pHGnd->setShow( false );
        m_pExpandGnd->setShow( false );

        m_pMemBar->setVisible( false );
        m_pMainOffset->setVisible( false );
        m_pRollOffset->setVisible( false );

        m_pMembarFrame->setVisible( false );
        m_pXyRollIcon->setVisible( true );
        m_pXyRollIcon->setImage( mXyIcon );

        m_pXyRollDisIcon->setVisible( true );
        m_pXyRollDisIcon->setImage( mXyDisIcon );

        m_pMainScale->setVisible( false );
        m_pXyScale->setVisible( true );
        m_pRollScale->setVisible( false );
    }
    else
    {
        m_pHGnd->setShow( true );
        m_pExpandGnd->setShow( true );
        m_pExpandGnd->offset( 0 );

        m_pMemBar->setVisible( true );
        m_pMainOffset->setVisible( true );
        m_pRollOffset->setVisible( false );

        m_pMembarFrame->setVisible( true );
        m_pXyRollIcon->setVisible( false );
        m_pXyRollDisIcon->setVisible( false );

        m_pMainScale->setVisible( true );
        m_pXyScale->setVisible( false );
        m_pRollScale->setVisible( false );
    }
}

void CAppHori::on_hori_changed()
{
    //! update membar
    updateMemBar();

    on_horiRangeChanged();
    on_horiZoomRangeChanged();
}

qlonglong CAppHori::getTrigPos()
{
    return getTrigOffset() + _style.hGndViewW/2;
}

qlonglong CAppHori::getTrigOffset()
{
    float offsetVal;
    float scaleVal;

    serviceExecutor::query( mservName, servHori::cmd_main_offset_real, offsetVal );
    serviceExecutor::query( mservName, servHori::cmd_main_scale_real, scaleVal );

    //! offset longlong 1s/100ps = 10G
    qlonglong offs = offsetVal * (qlonglong)adc_hdiv_dots/ scaleVal;

    return offs;
}

qlonglong CAppHori::getZoomTrigPos()
{
    return getZoomTrigOffset() + _style.hGndViewW/2;
}

qlonglong CAppHori::getZoomTrigOffset()
{
    qlonglong realOff;

    realOff = m_pServHori->getZoomOffset();

    qlonglong offs = realOff * adc_hdiv_dots / m_pServHori->getZoomScale();

    return offs;
}

qlonglong CAppHori::getZoomExpandOffset()
{
    return m_pServHori->getZoomExpandGnd() - trace_width/2;
}

void CAppHori::updateMemBar()
{
    DsoErr err;

    EngineHoriAttr attr;
    pointer ptr = &attr;
    err = serviceExecutor::query( mservName,
                                  servHori::cmd_hori_attr,
                                  ptr );
    if ( err != ERR_NONE ) return;

    //! cur para
    bool bZoom;
    err = query( MSG_HOR_ZOOM_ON, bZoom );
    if ( err != ERR_NONE ) return;

    barSession barSes;

//    qDebug()<<barSes.bPlay<<attr.mViewTag.mMainScale<<attr.mViewTag.mMainOffset<<attr.mViewTag.mCHLength;
//    qDebug()<<attr.mRecTag.mMainScale<<attr.mRecTag.mMainOffset<<attr.mRecTag.mCHLength;
//LOG_DBG()<<attr.mbPlay;
    barSes.bPlay = attr.mbPlay;

    barSes.stCurPara.bZoomOn = bZoom;
    barSes.stCurPara.u64MainScale = attr.mViewTag.mMainScale;
    barSes.stCurPara.s64MainOffset = attr.mViewTag.mMainOffset;

    barSes.stCurPara.u64ZoomScale = attr.mViewTag.mZoomScale;
    barSes.stCurPara.s64ZoomOffset = attr.mViewTag.mZoomOffset;

    barSes.stCurPara.u32MemPts = attr.mViewTag.mCHLength.ceilUInt();
    barSes.stCurPara.u64SaPeriod = attr.mViewTag.mCHDotTime.ceilULonglong();

    //! pre para
    barSes.stPrePara.u64MainScale = attr.mRecTag.mMainScale;
    barSes.stPrePara.s64MainOffset = attr.mRecTag.mMainOffset;

    barSes.stPrePara.u64ZoomScale = attr.mRecTag.mZoomScale;
    barSes.stPrePara.s64ZoomOffset = attr.mRecTag.mZoomOffset;

    barSes.stPrePara.u32MemPts = attr.mRecTag.mCHLength.ceilUInt();
    barSes.stPrePara.u64SaPeriod = attr.mRecTag.mCHDotTime.ceilULonglong();

    //! set bar
    m_pMemBar->setBarSession( barSes );
}

void CAppHori::on_horiRangeChanged()
{
    DsoRealGp scaleGp, offsetGp;
    m_pServHori->getMainScaleRange( &scaleGp );

    int timeMode;
    query( MSG_HOR_TIME_MODE, timeMode );
    //! into roll
    if ( (AcquireTimemode)timeMode == Acquire_ROLL )
    {
        m_pServHori->getRollOffsetRange( &offsetGp );
    }
    else
    {
        m_pServHori->getMainOffsetRange( &offsetGp );
    }

    m_pMainHoriCfg->setScaleGp( scaleGp );
    m_pMainHoriCfg->setOffsetGp( offsetGp );
}

void CAppHori::on_horiZoomRangeChanged()
{
    DsoRealGp scaleGp, offsetGp;
    m_pServHori->getZoomScaleRange( &scaleGp );
    m_pServHori->getZoomOffsetRange( &offsetGp );

    m_pZoomHoriCfg->setScaleGp( scaleGp );
    m_pZoomHoriCfg->setOffsetGp( offsetGp );
}

QString CAppHori::toString( ControlStatus stat )
{
    switch( stat )
    {
    case Control_Stoped:
        return QStringLiteral("stoped");
    case Control_Runing:
        return QStringLiteral("runing");
    case Control_autoing:
        return QStringLiteral("runing");
    case Control_waiting:
        return QStringLiteral("waiting");
    case Control_td:
        return QStringLiteral("td");
    default:
        return QStringLiteral("unk");
    }
}

/*!
 * \brief CAppHori::on_run_click
 * click后 check 状态改变，所以
 * 当前check = true: 表示之前是unchecked(即stop),所以需要执行的动作是 run
 */
void CAppHori::on_run_click()
{
    //! checked = run
    serviceExecutor::post( mservName, MSG_HORIZONTAL_RUN,
                           m_pRunStop->isChecked() ? (int)(Control_Run)
                                                   : (int)(Control_Stop) );
}

void CAppHori::on_hori_header_click( QWidget */*pCaller*/ )
{
    serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppHori::on_main_hori_content_click( QWidget *pCaller )
{
    bool bSetup;
    //! first config
    bSetup = m_pMainHoriCfg->setup( "appch/wnd/", r_meta::CAppMeta());
    if ( bSetup )
    {
        m_pMainHoriCfg->setTitle( MSG_HOR_MENU_CHANGE );
        m_pMainHoriCfg->setTitleColor( Hori_Color );

        m_pMainHoriCfg->setLabelId( MSG_HORIZONTAL_TIMESCALE,
                                    MSG_HORIZONTAL_TIMEOFFSET );

        m_pMainHoriCfg->setUnit( Unit_s );
        m_pMainHoriCfg->setScaleFmt( (DsoViewFmt)(fmt_float | fmt_width_3 | fmt_no_trim_0) );
        //! offset has more length
        //! eg. 1.0000001s
        m_pMainHoriCfg->setOffsetFmt( (DsoViewFmt)(fmt_float | fmt_width_9) );

        //! conect
        connect( m_pMainHoriCfg,
                 SIGNAL(sig_scale_real(DsoReal)),
                 this,
                 SLOT(on_main_scale_real(DsoReal)) );
        connect( m_pMainHoriCfg,
                 SIGNAL(sig_offset_real(DsoReal)),
                 this,
                 SLOT(on_main_offset_real(DsoReal)) );
        connect( m_pMainHoriCfg,
                 SIGNAL(sig_tune(int)),
                 this,
                 SLOT(on_main_tune(int)) );

        m_pMainHoriCfg->m_pBtnOffsetSub->setArrowType(Qt::LeftArrow);
        m_pMainHoriCfg->m_pBtnOffsetAdd->setArrowType(Qt::RightArrow);

        m_pMainHoriCfg->m_pBtnScaleSub->setArrowType(Qt::RightArrow);
        m_pMainHoriCfg->m_pBtnScaleAdd->setArrowType(Qt::LeftArrow);
    }
    else
    {}

    //! first config
    bool bSetup2 = m_pZoomHoriCfg->setup( "appch/wnd/", r_meta::CAppMeta());
    if ( bSetup2 )
    {
        m_pZoomHoriCfg->setTitle( MSG_HOR_MENU_CHANGE );
        m_pZoomHoriCfg->setTitleColor( Hori_Color );

        m_pZoomHoriCfg->setLabelId( MSG_HORIZONTAL_TIMESCALE,
                                    MSG_HORIZONTAL_TIMEOFFSET );

        m_pZoomHoriCfg->setUnit( Unit_s );
        m_pZoomHoriCfg->setScaleFmt( (DsoViewFmt)(fmt_float | fmt_width_3 | fmt_no_trim_0) );
        //! offset has more length
        //! eg. 1.0000001s
        m_pZoomHoriCfg->setOffsetFmt( (DsoViewFmt)(fmt_float | fmt_width_9) );

        //! conect
        connect( m_pZoomHoriCfg,
                 SIGNAL(sig_scale_real(DsoReal)),
                 this,
                 SLOT(on_zoom_scale_real(DsoReal)) );
        connect( m_pZoomHoriCfg,
                 SIGNAL(sig_offset_real(DsoReal)),
                 this,
                 SLOT(on_zoom_offset_real(DsoReal)) );
        connect( m_pZoomHoriCfg,
                 SIGNAL(sig_tune(int)),
                 this,
                 SLOT(on_main_tune(int)) );

        m_pZoomHoriCfg->m_pBtnOffsetSub->setArrowType(Qt::LeftArrow);
        m_pZoomHoriCfg->m_pBtnOffsetAdd->setArrowType(Qt::RightArrow);

        m_pZoomHoriCfg->m_pBtnScaleSub->setArrowType(Qt::RightArrow);
        m_pZoomHoriCfg->m_pBtnScaleAdd->setArrowType(Qt::LeftArrow);
    }
    else
    {}

    //! alternate the visible
    bool bZoom;
    query( MSG_HOR_ZOOM_ON, bZoom );
    if(bZoom)
    {
        m_pMainHoriCfg->setShow( false );
        m_pZoomHoriCfg->setShow( !m_pZoomHoriCfg->isVisible() );
    }
    else
    {
        m_pMainHoriCfg->setShow( !m_pMainHoriCfg->isVisible() );
        m_pZoomHoriCfg->setShow( false );
    }

    if ( bSetup ||  bSetup2 )
    {
        //m_pMainHoriCfg->arrangeOn( pCaller, priority_bottom, 20 );
        pCaller = pCaller;
        m_pMainHoriCfg->move((screen_width-m_pMainHoriCfg->width())/2,86);
        m_pZoomHoriCfg->move((screen_width-m_pZoomHoriCfg->width())/2,86);
    }

    //! get range
    on_horiRangeChanged();
    on_horiZoomRangeChanged();
}

void CAppHori::on_disEnable_click()
{
    servGui::showInfo( ERR_ACTION_DISABLED, "can not change in this mode");
}

void CAppHori::on_main_gnd_click()
{
}

void CAppHori::onMainMove( int /*xdist*/, int /*ydist*/ )
{}
void CAppHori::onZoomMove( int /*xdist*/, int /*ydist*/ )
{}

void CAppHori::onMainExpandMove( int /*xdist*/, int /*ydist*/ )
{}
void CAppHori::onZoomExpandMove( int /*xdist*/, int /*ydist*/ )
{}

//! update status
void CAppHori::onStatusTimeout()
{
    DsoErr err;
    int iVal;
    err = query( servHori::cmd_control_status, iVal );
    if ( err != ERR_NONE )
    { return; }

    ControlStatus stat;
    stat = (ControlStatus)iVal;

    Q_ASSERT( NULL != m_pSysStatus );
    m_pSysStatus->setStatus( stat );
}

void CAppHori::on_main_scale_real( DsoReal real )
{
    //! align
    if ( ERR_NONE != real.alignBase( E_N12 ) )
    { return; }

    //! set scale
    serviceExecutor::post( mservName,
                           servHori::cmd_main_scale,
                           real.mA );
}
void CAppHori::on_main_offset_real( DsoReal real )
{
    //! align
    if ( ERR_NONE != real.alignBase( E_N12 ) )
    { return; }

    //! set scale
    serviceExecutor::post( mservName,
                           servHori::cmd_main_offset,
                           real.mA );
}
void CAppHori::on_main_tune( int id )
{
    switch( id )
    {
        case menu_res::WndChCfg::tune_scale_add:
            sysMimicInc( mScaleKey );
            break;

        case menu_res::WndChCfg::tune_scale_sub:
            sysMimicDec( mScaleKey );
            break;

        case menu_res::WndChCfg::tune_offset_add:
            sysMimicInc( mOffsetKey );
            break;

        case menu_res::WndChCfg::tune_offset_sub:
            sysMimicDec( mOffsetKey );
            break;

        default:
            break;
    }
}

void CAppHori::on_zoom_scale_real( DsoReal real )
{
    //! align
    if ( ERR_NONE != real.alignBase( E_N12 ) )
    { return; }

    //! set scale
    serviceExecutor::post( mservName,
                           servHori::cmd_zoom_scale,
                           real.mA );
}
void CAppHori::on_zoom_offset_real( DsoReal real )
{
    //! align
    if ( ERR_NONE != real.alignBase( E_N12 ) )
    { return; }

    //! set scale
    serviceExecutor::post( mservName,
                           servHori::cmd_zoom_offset,
                           real.mA );
}
void CAppHori::on_zoom_tune( int  )
{}

