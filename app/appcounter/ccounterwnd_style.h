#ifndef CCOUNTERWND_STYLE_H
#define CCOUNTERWND_STYLE_H

#include "../../menu/menustyle/rui_style.h"
#include "../../menu/menustyle/rinfownd_style.h"

class CCounterWnd_Style : public menu_res::RUI_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );
    void createIconPath();

public:
    QRect  mFrameA;
    QRect  mFrameB;
    QColor mFrameBgColor;
    QColor mFrameRectColor;
    int    mFrameRectW;

    QRect   mTitle;
    QString mTitleFont;
    int     mTitleFontSize;
    QColor  mTitleColor;
    QRect   mTitleBg;
    QColor  mTitleBgColor;

    QPoint mSrcXY;
    QColor mColorCH1;
    QColor mColorCH2;
    QColor mColorCH3;
    QColor mColorCH4;
    QColor mColorDX;
    QColor mColorExt;
    QColor mColorLine;

    QString mCH1Icon, mCH2Icon, mCH3Icon, mCH4Icon;
    QString mAcIcon, mExtIcon, mExt5Icon;
    QString mD0Icon, mD1Icon, mD2Icon, mD3Icon,
            mD4Icon, mD5Icon, mD6Icon, mD7Icon,
            mD8Icon, mD9Icon, mD10Icon, mD11Icon,
            mD12Icon, mD13Icon, mD14Icon, mD15Icon;

    QRect   mType;
    QString mTypeFont;
    int     mTypeFontSize;
    QColor  mTypeColor;

    QRect   mCloseRect;

    QRect   mResult;
    QString mResultFont;
    int     mResultFontSize;

    QRect   mUnit;
    QString mUnitFont;
    int     mUnitFontSize;

    int     mStatHeight;
    QString mStatFont;
    int     mStatFontSize;
    QColor  mStatColor;
    QRect   mStatMax;
    QRect   mStatMaxValue;
    QRect   mStatMin;
    QRect   mStatMinValue;

    QString mInvalidString;
    //! cache
    QPainterPath iconPath;

};

#endif // CCOUNTERWND_STYLE_H
