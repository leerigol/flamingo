#ifndef CAPPCOUNTER_H
#define CAPPCOUNTER_H

#include <QtCore>
#include <QtWidgets>

#include "../capp.h"

#include "../../menu/rmenus.h"
#include "ccounterwnd.h"
#include "../../service/servcounter/servcounter.h"


class CAppCounter : public CApp
{
    Q_OBJECT
    DECLARE_CMD()
public:
    CAppCounter();

    enum appMsg
    {
        cmd_base = CMD_APP_BASE,
        cmd_counter_update,
    };

public:
     void setupUi( CAppMain *pMain );
     void retranslateUi();

     void buildConnection();
     void resize();
     void registerSpy();

     void onShowCounter();
     void setStatOn();
     void setSrc();
     void setType();
     void setResolution();

private Q_SLOTS:
     void updateCounter();
     void postCloseSig();
     void postTitleClick();
private:
     CCounterWnd *m_CounterA;
//     CCounterWnd *m_CounterB;

     Chan m_Src;
     CounterType m_type;
     int m_Resolution;

     QTimer *m_Timer;
};

#endif // CAPPCOUNTER_H
