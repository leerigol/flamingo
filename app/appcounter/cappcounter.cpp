#include "cappcounter.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../service/servch/servch.h"
#include "../../service/servvertmgr/servvertmgr.h"

#include "../appmain/cappmain.h"

//! msg table
IMPLEMENT_CMD( CApp, CAppCounter )
start_of_entry()
on_set_void_void (  MSG_COUNTER_1_ENABLE,             &CAppCounter::onShowCounter ),
on_set_void_void (  MSG_COUNTER_1_STAT,               &CAppCounter::setStatOn ),
on_set_void_void (  MSG_COUNTER_1_SRC,                &CAppCounter::setSrc ),
on_set_void_void (  MSG_COUNTER_1_MEAS_TYPE,          &CAppCounter::setType ),
on_set_void_void (  MSG_COUNTER_1_RESOLUTION,         &CAppCounter::setResolution ),

end_of_entry()

CAppCounter::CAppCounter()
{
    mservName = serv_name_counter;

    addQuick( MSG_COUNTER,
              QString("ui/desktop/counter/icon/") );

    m_CounterA = new CCounterWnd(0);
    Q_ASSERT(m_CounterA != NULL);
    m_CounterA->setVisible(false);

//    m_CounterB = new CCounterWnd(1);
//    Q_ASSERT(m_CounterB != NULL);
//    m_CounterB->setVisible(false);

    m_Src = chan1;
    m_type= FREQ;
    m_Resolution = 5;
    m_Timer = new QTimer(this);
    connect( m_Timer,SIGNAL(timeout()), this,SLOT(updateCounter()) );
}

void CAppCounter::setupUi( CAppMain *pMain )
{
    m_CounterA->setParent(pMain->getCentBar());
//    m_CounterB->setParent(pMain->getCentBar());
}

void CAppCounter::retranslateUi()
{

}

void CAppCounter::buildConnection()
{
    connect(m_CounterA, SIGNAL(closeSig()),this,SLOT(postCloseSig()));
//    connect(m_CounterB, SIGNAL(closeSig()),this,SLOT(postCloseSig()));
    connect(m_CounterA, SIGNAL(sigTitleClick()),this,SLOT(postTitleClick()));
}
void CAppCounter::resize()
{

}

void CAppCounter::registerSpy()
{
}

void CAppCounter::onShowCounter()
{
    bool ctr1Show = false;
    serviceExecutor::query(serv_name_counter,
                           MSG_COUNTER_1_ENABLE,
                           ctr1Show);

    if( ctr1Show)
    {
        //check Counter Valid, if not pop "Counter need active!"
        setSrc();
        setType();
        setResolution();
        m_CounterA->show();
        m_Timer->start();
    }
    else
    {
        m_Timer->stop();
        m_CounterA->hide();
    }
}

void CAppCounter::setStatOn()
{
    bool statOn = false;
    serviceExecutor::query(serv_name_counter, MSG_COUNTER_1_STAT, statOn);

    m_CounterA->setStatOn(statOn);
}

void CAppCounter::setSrc()
{
    int ch = (int)chan1;
    serviceExecutor::query(serv_name_counter, MSG_COUNTER_1_SRC, ch);

    m_Src = (Chan)ch;
    m_CounterA->setValue(0.0, 0.0, 0.0, CTR_No_Signal);
    m_CounterA->setSource(m_Src);
}

void CAppCounter::setType()
{
    int type;
    serviceExecutor::query(serv_name_counter, MSG_COUNTER_1_MEAS_TYPE, type);
    m_type = (CounterType)type;

    m_CounterA->setValue(0.0, 0.0, 0.0, CTR_No_Signal);
    m_CounterA->setType(m_type);

    if( m_type == TOTALIZE)
    {
        m_CounterA->setStatOn(false);
        serviceExecutor::post(serv_name_counter, MSG_COUNTER_1_STAT, false);
    }

    setResolution();
}

void CAppCounter::setResolution()
{
    serviceExecutor::query(serv_name_counter, MSG_COUNTER_1_RESOLUTION, m_Resolution);
    m_CounterA->setPrecision(m_Resolution);

    if( m_type == TOTALIZE)
    {
        m_Timer->setInterval(100);
    }
    else
    {
        if( m_Resolution == 3)       m_Timer->setInterval(200);   //! update rate: 200ms
        else if( m_Resolution == 4)  m_Timer->setInterval(200);   //! update rate: 200ms
        else if( m_Resolution == 5)  m_Timer->setInterval(200);  //! update rate: 200ms
        else if( m_Resolution == 6)  m_Timer->setInterval(1000); //! update rate: 1000ms
        else                         m_Timer->setInterval(100);   //! update rate: 100ms
    }

    LOG_DBG()<<"-------------------------m_Resolution: "<<m_Resolution;
}

//! timer
void CAppCounter::updateCounter()
{

    DsoErr err;

    //! disable now
    bool bEn;


    query( serv_name_counter, MSG_COUNTER_1_ENABLE, bEn);
    LOG_DBG()<<"-----------------------------bEn: "<<bEn;
    if( !bEn)  { return;}

    //! counter value
    void *ptr = NULL;
    err = query( serv_name_counter,
                 servCounter::cmd_Get_CounterData,
                 ptr );
    LOG_DBG()<<"-----------------------------CounterData ptr: "<<ptr;

    if ( err != ERR_NONE ) { return; }
    if ( NULL == ptr ) { return; }

    //! data set
    counterData *cData = (counterData *)ptr;
    if( cData != NULL )
    {
        m_CounterA->setValue( cData->getVal(),
                              cData->getMax(),
                              cData->getMin(),
                              cData->status() );
    }
}

void CAppCounter::postCloseSig()
{
    bool ctr1Status = false;
    serviceExecutor::query(serv_name_counter,
                           MSG_COUNTER_1_ENABLE,
                           ctr1Status);

    if(ctr1Status)
    {
        serviceExecutor::post(serv_name_counter,
                               MSG_COUNTER_1_ENABLE,
                               false);
    }
}

void CAppCounter::postTitleClick()
{
    serviceExecutor::post(serv_name_counter, CMD_SERVICE_ACTIVE, (int)1 );
}
