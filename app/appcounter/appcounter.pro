#-------------------------------------------------
#
# Project created by QtCreator 2017-03-13T21:38:34
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets
QT       += network

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/apps/appcounter

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

INCLUDEPATH += .

#DEFINES += _DEBUG

# Input
SOURCES += cappcounter.cpp \
           ccounterwnd.cpp \
           ccounterwnd_style.cpp \

HEADERS += cappcounter.h \
           ccounterwnd.h \
           ccounterwnd_style.h \
