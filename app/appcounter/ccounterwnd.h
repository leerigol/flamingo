#ifndef CCOUNTERWND_H
#define CCOUNTERWND_H

#include <QtWidgets>

#include "../../include/dsotype.h"
#include "ccounterwnd_style.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../service/servcounter/servcounter.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;
using namespace DsoType;

class CCounterWnd : public uiWnd
{
    Q_OBJECT
    static QPainterPath _iconPath;
    static CCounterWnd_Style _style;
    static menu_res::RModelWnd_Style _bgStyle;

public:
    CCounterWnd( int counterIndex, QWidget *parent = 0);

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual void resizeEvent(QResizeEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
public:
    void setSource( Chan chan );
    void setType( CounterType type);
    void setPrecision( int pre);
    void setValue(double value, double max, double min, CounterStatus status);
    void setStatMax( double value);
    void setStatMin( double value);
    void setStatOn( bool b);

signals:
    void closeSig();
    void sigTitleClick();

protected:
    QString getSrcIcon();
    QColor  getSrcColor();
    QString getTypeStr();
    QString getResultStr();
    QString getUnitStr();
    QString getStatStr(double stat);
    void    setSize();

private:
    RImageButton   *m_pCloseButton;

    int         m_Index;
    QString     m_Title;
    Chan        m_Src;
    CounterType m_Type;

    double      m_Value;
    bool        m_StatOn;
    double      m_Max;
    double      m_Min;
    QString     m_Unit;
    QString     m_UnitPrefix;
    CounterStatus    m_Status;

    DsoViewFmt  counterFmt;

};

#endif // CCOUNTERWND_H
