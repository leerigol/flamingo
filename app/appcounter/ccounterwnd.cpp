#include "ccounterwnd.h"
#include "../../include/dsostd.h"
#include "../../gui/cguiformatter.h"
#include "../../menu/menustyle/rcontrolstyle.h"

QPainterPath CCounterWnd::_iconPath;
CCounterWnd_Style CCounterWnd::_style;
menu_res::RModelWnd_Style CCounterWnd::_bgStyle;
CCounterWnd::CCounterWnd(int counterIndex, QWidget */*parent*/ ) : uiWnd( sysGetMainWindow() )
{
    _style.loadResource("ui/wnd/counter/");
    _bgStyle.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(_style.mCloseRect);
    m_pCloseButton->setImage( _bgStyle.mNormalImg,
                               _bgStyle.mDownImg,
                               _bgStyle.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SIGNAL(closeSig()) );
    m_Index = counterIndex;

    if(m_Index == 0)
    {
        resize( _style.mFrameA.width(),_style.mFrameA.height() );
        m_Title = "Counter";
    }
    else
    {
        resize( _style.mFrameB.width(),_style.mFrameB.height() );
        m_Title = "Counter";
    }

    m_Src        = d15;
    m_Type       = FREQ;

    m_Value      = 0.0;
    m_StatOn     = false;
    m_Max        = 0.0;
    m_Min        = 0.0;
    m_UnitPrefix = "";
    m_Unit       = "Hz";
    m_Status     = CTR_No_Signal;
    counterFmt   = dso_fmt_trim(fmt_float,fmt_width_6,fmt_no_trim_0);

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void CCounterWnd::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QString str;
    QFont font;
    QImage image;
    QString srcIcon;
    QColor srcColor;
//    QPainterPath path;


    //! frameBg
    QRect frameRect = rect();
//    _infoStyle.paint( painter, selfRect );
//    painter.fillRect(frameRect, _style.mFrameBgColor);

    _bgStyle.paint( painter, frameRect );

#if 1
    //! frameRect
//    QPen pen;
//    pen.setWidth(_style.mFrameRectW);
//    pen.setColor(_style.mFrameRectColor);
//    painter.setPen(pen);
//    painter.drawRect(frameRect);

    //! Title
//    painter.fillRect(_style.mTitleBg,_style.mTitleBgColor);
    font.setFamily( _style.mTitleFont );
    font.setPointSize( _style.mTitleFontSize );
    painter.setFont( font );
    painter.setPen( _style.mTitleColor );
    painter.drawText( _style.mTitle, Qt::AlignVCenter | Qt::AlignLeft,
                      m_Title );

    //! source
    srcColor = getSrcColor();
    srcIcon  = getSrcIcon();
    image.load( srcIcon);
    menu_res::RControlStyle::renderFg( image, srcColor );
    painter.drawImage( _style.mSrcXY, image );

    //! Type
    font.setFamily(_style.mTypeFont);
    font.setPointSize( _style.mTypeFontSize);
    painter.setFont( font);
    painter.setPen( _style.mTypeColor);
    painter.drawText( _style.mType, Qt::AlignVCenter | Qt::AlignLeft,
                      getTypeStr());

    //! result  
    font.setFamily( _style.mResultFont );
    font.setPointSize( _style.mResultFontSize );    
    painter.setFont( font );
    painter.setPen( srcColor );
    painter.drawText( _style.mResult, Qt::AlignVCenter | Qt::AlignRight,
                      getResultStr() );
    //! unit
    font.setFamily( _style.mUnitFont );
    font.setPointSize( _style.mUnitFontSize );
    painter.setFont( font );
    painter.setPen( srcColor );
    painter.drawText( _style.mUnit, Qt::AlignVCenter | Qt::AlignLeft,
                      getUnitStr() );

    //! Statistics
    if(m_StatOn)
    {
        font.setFamily( _style.mStatFont);
        font.setPointSize( _style.mStatFontSize);
        painter.setFont( font);
        painter.setPen( _style.mStatColor);
        painter.drawText( _style.mStatMax, Qt::AlignVCenter | Qt::AlignLeft,
                          "Max:");
        painter.drawText( _style.mStatMaxValue, Qt::AlignVCenter | Qt::AlignRight,
                          getStatStr(m_Max));
        painter.drawText( _style.mStatMin, Qt::AlignVCenter | Qt::AlignLeft,
                          "Min:");
        painter.drawText( _style.mStatMinValue, Qt::AlignVCenter | Qt::AlignRight,
                          getStatStr(m_Min));

    }
#endif
    //! icon
//    painter.save();
//    painter.translate( _style.mIcon.left(), _style.mIcon.top() );
//    painter.setPen ( _style.mColorText );
//    painter.drawPath( _style.iconPath );
//    painter.restore();
}

void CCounterWnd::resizeEvent(QResizeEvent * /*event*/)
{
    setSize();
}

void CCounterWnd::mouseReleaseEvent(QMouseEvent *event)
{
    QRect titleRec = _style.mTitle;
    if(titleRec.contains(event->pos()))
    {
        emit sigTitleClick();
    }

    uiWnd::mouseReleaseEvent(event);
}

void  CCounterWnd::setSize()
{
    if(m_Index == 0)
    {
        if(m_StatOn)
        {
//            move( _style.mFrameA.left(),
//                  _style.mFrameA.top()-_style.mStatHeight );
            move( _style.mFrameA.left(),
                  _style.mFrameA.top());

            resize( _style.mFrameA.width(),
                    _style.mFrameA.height()+_style.mStatHeight);
        }
        else
        {
            move( _style.mFrameA.left(),
                  _style.mFrameA.top() );
            resize( _style.mFrameA.width(),
                    _style.mFrameA.height());
        }
    }
    else
    {
        if(m_StatOn)
        {
            move( _style.mFrameB.left(),
                  _style.mFrameB.top()-_style.mStatHeight );
            resize( _style.mFrameB.width(),
                    _style.mFrameB.height()+_style.mStatHeight);
        }
        else
        {
            move( _style.mFrameB.left(),
                  _style.mFrameB.top() );
            resize( _style.mFrameB.width(),
                    _style.mFrameB.height());
        }
    }
}

void CCounterWnd::setSource( Chan chan )
{
    m_Src = chan;

    update();
}

void CCounterWnd::setType(CounterType type)
{
    m_Type = type;

    switch (m_Type)
    {
    case FREQ:
        m_Unit = "Hz";
        break;

    case PERIOD:
        m_Unit = "s";
        break;

    case TOTALIZE:
        m_Unit = "hits";
        break;

    default:
        m_Unit = "Hz";
        break;
    }

    update();
}

void CCounterWnd::setPrecision( int pre)
{
    if( m_Type == TOTALIZE)
    {
        counterFmt = dso_fmt_trim(fmt_float,fmt_width_9,fmt_trim_0);
    }
    else
    {
        if( pre == 3)      counterFmt = dso_fmt_trim(fmt_float,fmt_width_3,fmt_no_trim_0);
        else if( pre == 4) counterFmt = dso_fmt_trim(fmt_float,fmt_width_4,fmt_no_trim_0);
        else if( pre == 5) counterFmt = dso_fmt_trim(fmt_float,fmt_width_5,fmt_no_trim_0);
        else if( pre == 6) counterFmt = dso_fmt_trim(fmt_float,fmt_width_6,fmt_no_trim_0);
        else if( pre == 7) counterFmt = dso_fmt_trim(fmt_float,fmt_width_7,fmt_no_trim_0);
        else if( pre == 8) counterFmt = dso_fmt_trim(fmt_float,fmt_width_8,fmt_no_trim_0);
        else if( pre == 9) counterFmt = dso_fmt_trim(fmt_float,fmt_width_9,fmt_no_trim_0);
        else if( pre == 10) counterFmt = dso_fmt_trim(fmt_float,fmt_width_9,fmt_no_trim_0);
        else                counterFmt = dso_fmt_trim(fmt_float,fmt_width_5,fmt_no_trim_0);
    }
    update();
}

void CCounterWnd::setValue(double value, double max, double min, CounterStatus status )
{
    LOG_DBG() << "-----------value = "<<value;
    LOG_DBG() << "-----------status = "<<status;

    m_Value = value;
    m_Max   = max;
    m_Min   = min;
    m_Status = status;

    update();
}

void CCounterWnd::setStatMax( double value)
{
    m_Max = value;
    update();
}

void CCounterWnd::setStatMin( double value)
{
    m_Min = value;
    update();
}

void CCounterWnd::setStatOn( bool b)
{
    m_StatOn = b;
    setSize();
}

QString CCounterWnd::getTypeStr()
{
    switch (m_Type)
    {
    case FREQ:
        return "Frequency";

    case PERIOD:
        return "Period";

    case TOTALIZE:
        return "Totalize";

    default:
        return "Frequency";
    }
}

QString CCounterWnd::getResultStr()
{
    QString str = "";

    if ( m_Status == CTR_Valid )
    {
//        str = QString("%1Hz").arg( m_Value );
        gui_fmt::CGuiFormatter::format(str, m_Value, counterFmt, Unit_none);
        LOG_DBG()<<"---------------------m_Value: "<< m_Value;
        LOG_DBG()<<"---------------------str: "<<str;

        if(str.contains(QRegExp("[TGMkKmunpf]")))
        {
            m_UnitPrefix = str.right(1);
        }
        else
        {
            m_UnitPrefix = "";
        }
        str.remove(QRegExp("[TGMkKmunpf]")); //Remove unit prefixs

        if( m_Value <= 0)
        {
            str = _style.mInvalidString;
            m_UnitPrefix = "";
        }
    }
    else if( m_Status == CTR_No_Signal)
    {
        str = _style.mInvalidString;
//        qDebug() << "InvalidString:  "<<_style.mInvalidString;
        m_UnitPrefix = "";
    }
    else if( m_Status == CTR_Limit)
    {
        switch (m_Type)
        {
        case FREQ:
            str = "< 2"; // Counter Limit(GateTime = 1s): <2Hz
            break;
        case PERIOD:
            str = "> 0.5";  // Counter Limit(GateTime = 1s): <2Hz
            break;
        case TOTALIZE:
            str = _style.mInvalidString;
            break;
        default:
            str = _style.mInvalidString;
            break;
        }
        m_UnitPrefix = "";
    }
    else
    {
        str = _style.mInvalidString;
        m_UnitPrefix = "";
    }

    return str;
}

QString CCounterWnd::getUnitStr()
{
    QString unit;
    unit = m_UnitPrefix + m_Unit;
    return unit;
}

QString CCounterWnd::getStatStr(double stat)
{
    QString str = "";

    if ( m_Status == CTR_Valid )
    {
        gui_fmt::CGuiFormatter::format(str, stat, counterFmt, Unit_none);
        str = str + m_Unit;
    }
    else
    {
        str = _style.mInvalidString;
    }

    return str;
}

QString CCounterWnd::getSrcIcon()
{
    if( m_Src==chan1 ) return _style.mCH1Icon;
    else if ( m_Src==chan2 ) return _style.mCH2Icon;
    else if ( m_Src==chan3 ) return _style.mCH3Icon;
    else if ( m_Src==chan4 ) return _style.mCH4Icon;

    else if ( m_Src==d0 ) return _style.mD0Icon;
    else if ( m_Src==d1 ) return _style.mD1Icon;
    else if ( m_Src==d2 ) return _style.mD2Icon;
    else if ( m_Src==d3 ) return _style.mD3Icon;
    else if ( m_Src==d4 ) return _style.mD4Icon;
    else if ( m_Src==d5 ) return _style.mD5Icon;
    else if ( m_Src==d6 ) return _style.mD6Icon;
    else if ( m_Src==d7 ) return _style.mD7Icon;
    else if ( m_Src==d8 ) return _style.mD8Icon;
    else if ( m_Src==d9 ) return _style.mD9Icon;
    else if ( m_Src==d10) return _style.mD10Icon;
    else if ( m_Src==d11) return _style.mD11Icon;
    else if ( m_Src==d12) return _style.mD12Icon;
    else if ( m_Src==d13) return _style.mD13Icon;
    else if ( m_Src==d14) return _style.mD14Icon;
    else if ( m_Src==d15) return _style.mD15Icon;

    else if ( m_Src==acline ) return _style.mAcIcon;
    else if ( m_Src==ext )    return _style.mExtIcon;
    else if ( m_Src==ext5 )   return _style.mExt5Icon;
    else return _style.mCH1Icon;

}

QColor CCounterWnd::getSrcColor()
{
    if ( m_Src == chan1 )
    {
        return _style.mColorCH1;
    }
    else if ( m_Src == chan2 )
    {
        return _style.mColorCH2;
    }
    else if ( m_Src == chan3 )
    {
        return _style.mColorCH3;
    }
    else if ( m_Src == chan4 )
    {
        return _style.mColorCH4;
    }
    else if ( m_Src >= d0 && m_Src <= d15 )
    {
        return _style.mColorDX;
    }
    else if ( m_Src == acline )
    {
        return _style.mColorLine;
    }
    else if ( m_Src == ext || m_Src == ext5 )
    {
        return _style.mColorExt;
    }
    else
    {
        return Qt::white;
    }
}

//void CCounterWnd::toPath( const QString &str,
//                          QPainterPath &path,
//                          QFont &font )
//{
//    QPainterPath pathStr;
//    QFontMetrics metrics(font);

//    int w, h;
//    QRect rect;

//    rect.setRect( 0, 0, _style.mSrc.width(), _style.mSrc.height() );
//    path.addRoundRect( rect, 0, 0 );

//    w = metrics.width( str );
//    h = metrics.height();

//    pathStr.addText( ( rect.width() - w)/2,
//                      h + ( rect.height()-h)/2,
//                     font,
//                     str );
//    path = path.subtracted( pathStr );
//}

