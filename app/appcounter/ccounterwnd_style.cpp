#include "ccounterwnd_style.h"

#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rcontrolstyle.h"

void CCounterWnd_Style::load( const QString &path,
                              const r_meta::CMeta &meta )
{

    meta.getMetaVal( path + "frameA",         mFrameA );
    meta.getMetaVal( path + "frameB",         mFrameB );
    meta.getMetaVal( path + "frameBgColor",   mFrameBgColor );
    meta.getMetaVal( path + "frameRectColor", mFrameRectColor );
    meta.getMetaVal( path + "frameRectW",     mFrameRectW );

    meta.getMetaVal( path + "title",         mTitle );
    meta.getMetaVal( path + "titleFont",     mTitleFont );
    meta.getMetaVal( path + "titleFontSize", mTitleFontSize );
    meta.getMetaVal( path + "titleColor",    mTitleColor );
    meta.getMetaVal( path + "titleBg",       mTitleBg );
    meta.getMetaVal( path + "titleBgColor",  mTitleBgColor );

    meta.getMetaVal( path + "type",         mType );
    meta.getMetaVal( path + "typeFont",     mTypeFont );
    meta.getMetaVal( path + "typeFontSize", mTypeFontSize );
    meta.getMetaVal( path + "typeColor",    mTypeColor );

    meta.getMetaVal( path + "closerect",      mCloseRect );

    meta.getMetaVal( path + "result",         mResult );
    meta.getMetaVal( path + "resultFont",     mResultFont );
    meta.getMetaVal( path + "resultFontSize", mResultFontSize );

    meta.getMetaVal( path + "unit",         mUnit );
    meta.getMetaVal( path + "unitFont",     mUnitFont );
    meta.getMetaVal( path + "unitFontSize", mUnitFontSize );

    meta.getMetaVal( path + "srcXY",       mSrcXY );
    meta.getMetaVal( path + "chcolor/ch1", mColorCH1 );
    meta.getMetaVal( path + "chcolor/ch2", mColorCH2 );
    meta.getMetaVal( path + "chcolor/ch3", mColorCH3 );
    meta.getMetaVal( path + "chcolor/ch4", mColorCH4 );
    meta.getMetaVal( path + "chcolor/dx",  mColorDX );
    meta.getMetaVal( path + "chcolor/ext", mColorExt );
    meta.getMetaVal( path + "chcolor/ac",  mColorLine );

    meta.getMetaVal( path + "StatHeight",  mStatHeight );
    meta.getMetaVal( path + "StatFont",    mStatFont );
    meta.getMetaVal( path + "StatFontSize",mStatFontSize );
    meta.getMetaVal( path + "StatColor",   mStatColor );
    meta.getMetaVal( path + "StatMax",     mStatMax );
    meta.getMetaVal( path + "StatMaxValue",mStatMaxValue );
    meta.getMetaVal( path + "StatMin",     mStatMin );
    meta.getMetaVal( path + "StatMinValue",mStatMinValue );

    //! invalid string
    meta.getMetaVal( path + "invalid_string", mInvalidString );

    QString ChIconPath = "ui/trig/trig_info/ch_icon/";
    meta.getMetaVal( ChIconPath  + "ch1", mCH1Icon );
    meta.getMetaVal( ChIconPath  + "ch2", mCH2Icon );
    meta.getMetaVal( ChIconPath  + "ch3", mCH3Icon );
    meta.getMetaVal( ChIconPath  + "ch4", mCH4Icon );

    meta.getMetaVal( ChIconPath  + "ac",   mAcIcon );
    meta.getMetaVal( ChIconPath  + "ext",  mExtIcon );
    meta.getMetaVal( ChIconPath  + "ext5", mExt5Icon );

    meta.getMetaVal( ChIconPath + "d0", mD0Icon );
    meta.getMetaVal( ChIconPath + "d1", mD1Icon );
    meta.getMetaVal( ChIconPath + "d2", mD2Icon );
    meta.getMetaVal( ChIconPath + "d3", mD3Icon );

    meta.getMetaVal( ChIconPath + "d4", mD4Icon );
    meta.getMetaVal( ChIconPath + "d5", mD5Icon );
    meta.getMetaVal( ChIconPath + "d6", mD6Icon );
    meta.getMetaVal( ChIconPath + "d7", mD7Icon );

    meta.getMetaVal( ChIconPath + "d8",  mD8Icon );
    meta.getMetaVal( ChIconPath + "d9",  mD9Icon );
    meta.getMetaVal( ChIconPath + "d10", mD10Icon );
    meta.getMetaVal( ChIconPath + "d11", mD11Icon );

    meta.getMetaVal( ChIconPath + "d12", mD12Icon );
    meta.getMetaVal( ChIconPath + "d13", mD13Icon );
    meta.getMetaVal( ChIconPath + "d14", mD14Icon );
    meta.getMetaVal( ChIconPath + "d15", mD15Icon );

    //! cache
//    createIconPath();
}

//void CCounterWnd_Style::createIconPath()
//{
//    int barWidth, barHeight;

//    barWidth = mIcon.width()/7;
//    barHeight = mIcon.height();

//    iconPath.moveTo( 0, barHeight - 1 );

//    iconPath.lineTo( barWidth*1, barHeight - 1 );
//    iconPath.lineTo( barWidth*1, 0 );

//    iconPath.lineTo( barWidth*2, 0 );
//    iconPath.lineTo( barWidth*2, barHeight - 1 );

//    iconPath.lineTo( barWidth*3, barHeight - 1 );
//    iconPath.lineTo( barWidth*3, 0 );

//    iconPath.lineTo( barWidth*4, 0 );
//    iconPath.lineTo( barWidth*4, barHeight - 1 );

//    iconPath.lineTo( barWidth*5, barHeight - 1 );
//    iconPath.lineTo( barWidth*5, 0 );

//    iconPath.lineTo( barWidth*6, 0 );
//    iconPath.lineTo( barWidth*6, barHeight - 1 );

//    iconPath.lineTo( barWidth*7-1, barHeight - 1 );
//}
