#ifndef WNDRECORD_H
#define WNDRECORD_H

#include <QtWidgets>

//#include "../../menu/wnd/rinfownd.h"
#include "../../menu/wnd/rmodalwnd.h"
#include "../../widget/rimagebutton.h"

class WndRecord : public menu_res::RModalWnd
{
    Q_OBJECT

public:
    enum wndMode
    {
        wnd_record,
        wnd_play
    };

    enum wndSubButton
    {
        btn_play_first,
        btn_play_back,
        btn_play_next,
        btn_play_last,
        btn_play_start,
        btn_rec_start,
    };

public:
    explicit WndRecord(QWidget *parent = 0);
    ~WndRecord();

protected:
    virtual void paintEvent( QPaintEvent *event );
protected:
    virtual void setupUi( const QString &path,
                  const r_meta::CMeta &meta );


Q_SIGNALS:
    void sig_btn_clicked( WndRecord::wndSubButton );

protected Q_SLOTS:
    void on_btn_clicked( int );

public:
    void setMode( wndMode mode );
    wndMode getMode();

    void setSubEnable( wndSubButton btn, bool b );
    void setSubVisible( wndSubButton btn, bool b );

protected:
    void enterRecord();
    void enterPlay();

public:
    void updateRecord( int now, int total );
    void updatePlay(int now, int total);
    void updateTimeStamp(qlonglong time);

public:
    QLabel                 *m_pTimeStamp;
    QProgressBar           *m_pProgBar;        //! for play && record
    menu_res::RImageButton *m_pBtnRecStartStop;
    menu_res::RImageButton *m_pBtnPlayBack;
    menu_res::RImageButton *m_pBtnPlayNext;
    menu_res::RImageButton *m_pBtnPlayPause;
    menu_res::RImageButton *m_pBtnPlayFirst;
    menu_res::RImageButton *m_pBtnPlayLast;

    QSignalMapper                *m_pSigMap;
    QMap<wndSubButton, QWidget*>  mBtns;
private:
    wndMode mWndMode;

};

#endif // WNDRECORD_H
