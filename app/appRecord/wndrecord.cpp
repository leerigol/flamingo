#include "wndrecord.h"


WndRecord::WndRecord(QWidget *parent) :
                     menu_res::RModalWnd(parent)
{
    m_pTimeStamp = NULL;

    m_pProgBar   = NULL;

    m_pBtnRecStartStop = NULL;

    m_pBtnPlayBack = NULL;
    m_pBtnPlayNext = NULL;
    m_pBtnPlayPause = NULL;

    m_pBtnPlayFirst = NULL;
    m_pBtnPlayLast = NULL;

    mWndMode = wnd_record;

    set_attr( wndAttr, mWndAttr, wnd_moveable );
    set_attr( wndAttr, mWndAttr, wnd_manual_resize );

    setCloseVisible(false);
}

WndRecord::~WndRecord()
{

}

void WndRecord::paintEvent(QPaintEvent *event )
{
    //! paint frame
    RModalWnd::paintEvent( event );

    //! paint child
    QWidget::paintEvent( event );
}

void WndRecord::setupUi( const QString &path,
                         const r_meta::CMeta &meta )
{
    //! create the wnd
    QWidget *parent;
    parent = this;

    m_pTimeStamp = new QLabel(parent);
    Q_ASSERT( NULL != m_pTimeStamp );

    m_pProgBar = new QProgressBar( parent );
    Q_ASSERT( NULL != m_pProgBar );

    m_pBtnRecStartStop = new menu_res::RImageButton( parent );
    Q_ASSERT( NULL != m_pBtnRecStartStop );

    m_pBtnPlayBack = new menu_res::RImageButton( parent );
    Q_ASSERT( NULL != m_pBtnPlayBack );
    m_pBtnPlayNext = new menu_res::RImageButton( parent );
    Q_ASSERT( NULL != m_pBtnPlayNext );
    m_pBtnPlayPause = new menu_res::RImageButton( parent );
    Q_ASSERT( NULL != m_pBtnPlayPause );

    m_pBtnPlayFirst = new menu_res::RImageButton( parent );
    Q_ASSERT( NULL != m_pBtnPlayFirst );
    m_pBtnPlayLast = new menu_res::RImageButton( parent );
    Q_ASSERT( NULL != m_pBtnPlayLast );

    //! init the ui
    m_pProgBar->setFormat( "%v/%m" );

    //! style && geometry
    QString style;
    meta.getMetaVal( path + "ui/rec/style", style );
    this->setStyleSheet( style );

    QRect geoRect;
    meta.getMetaVal( path + "ui/rec/geo/wnd", geoRect );
    setGeometry( geoRect );

    meta.getMetaVal( path + "ui/rec/geo/progress_bar", geoRect );
    m_pProgBar->setGeometry( geoRect );

    meta.getMetaVal( path + "ui/rec/geo/rec", geoRect );
    m_pBtnRecStartStop->setGeometry( geoRect );

    meta.getMetaVal( path + "ui/rec/geo/time", geoRect );
    m_pTimeStamp->setGeometry( geoRect );

    //! play
    meta.getMetaVal( path + "ui/play/geo/first", geoRect );
    m_pBtnPlayFirst->setGeometry( geoRect );

    meta.getMetaVal( path + "ui/play/geo/back", geoRect );
    m_pBtnPlayBack->setGeometry( geoRect );

    meta.getMetaVal( path + "ui/play/geo/next", geoRect );
    m_pBtnPlayNext->setGeometry( geoRect );

    meta.getMetaVal( path + "ui/play/geo/last", geoRect );
    m_pBtnPlayLast->setGeometry( geoRect );

    meta.getMetaVal( path + "ui/play/geo/pause", geoRect );
    m_pBtnPlayPause->setGeometry( geoRect );

    //! image
    QString str, sep;

    sep = QLatin1Literal(",");

    meta.getMetaVal( path + "ui/rec/image/start", str );
    m_pBtnRecStartStop->setImage( str, sep );
    meta.getMetaVal( path + "ui/rec/image/stop", str );
    m_pBtnRecStartStop->setImage( str, sep, true );

    meta.getMetaVal( path + "ui/play/image/first", str );
    m_pBtnPlayFirst->setImage( str, sep );

    meta.getMetaVal( path + "ui/play/image/back", str );
    m_pBtnPlayBack->setImage( str, sep );

    meta.getMetaVal( path + "ui/play/image/next", str );
    m_pBtnPlayNext->setImage( str, sep );

    meta.getMetaVal( path + "ui/play/image/last", str );
    m_pBtnPlayLast->setImage( str, sep );

    meta.getMetaVal( path + "ui/play/image/play", str );
    m_pBtnPlayPause->setImage( str, sep );
    meta.getMetaVal( path + "ui/play/image/stop", str );
    m_pBtnPlayPause->setImage( str, sep, true );

    //! mode
    setMode( wnd_record );

    //! fill map;
    mBtns[ btn_play_first ] = m_pBtnPlayFirst;
    mBtns[ btn_play_back ] = m_pBtnPlayBack;
    mBtns[ btn_play_next ] = m_pBtnPlayNext;
    mBtns[ btn_play_last ] = m_pBtnPlayLast;
    mBtns[ btn_play_start ] = m_pBtnPlayPause;

    mBtns[ btn_rec_start ] = m_pBtnRecStartStop;

    //! connect
    m_pSigMap = new QSignalMapper(this);
    Q_ASSERT( m_pSigMap != NULL );

    //! foreach buttons
    QMap<wndSubButton, QWidget*>::const_iterator i = mBtns.constBegin();
    while (i != mBtns.constEnd())
    {
        connect( i.value(), SIGNAL(clicked(bool)),
                 m_pSigMap, SLOT(map()) );
        m_pSigMap->setMapping( i.value(), i.key() );

        i++;
    };

    connect( m_pSigMap, SIGNAL(mapped(int)),
             this, SLOT(on_btn_clicked(int)) );
}

void WndRecord::on_btn_clicked( int sub )
{
    emit sig_btn_clicked( (WndRecord::wndSubButton)sub );
}

void WndRecord::setMode( wndMode mode )
{
    mWndMode = mode;

    if ( mode == wnd_record )
    { enterRecord();}
    else
    { enterPlay(); }
}

void WndRecord::enterRecord()
{
    m_pProgBar->show();
    m_pBtnRecStartStop->show();

    m_pBtnPlayFirst->hide();
    m_pBtnPlayLast->hide();
    m_pBtnPlayBack->hide();
    m_pBtnPlayNext->hide();
    m_pBtnPlayPause->hide();
}
void WndRecord::enterPlay()
{
    m_pProgBar->show();
    m_pBtnRecStartStop->show();

    m_pBtnPlayFirst->show();
    m_pBtnPlayLast->show();
    m_pBtnPlayBack->show();
    m_pBtnPlayNext->show();
    m_pBtnPlayPause->show();
}

WndRecord::wndMode WndRecord::getMode()
{
    return mWndMode;
}

void WndRecord::setSubEnable( wndSubButton btn, bool b )
{
    Q_ASSERT( mBtns.contains(btn) );
    Q_ASSERT( mBtns[ btn ] != NULL );

    mBtns[ btn ]->setEnabled( b );
}
void WndRecord::setSubVisible( wndSubButton btn, bool b )
{
    Q_ASSERT( mBtns.contains(btn) );
    Q_ASSERT( mBtns[ btn ] != NULL );

    mBtns[ btn ]->setVisible( b );
    update();
}

void WndRecord::updateRecord( int now, int total )
{
    if ( total == 0 )
    {
        m_pProgBar->reset();
    }
    else
    {
        m_pProgBar->setRange( 0, total );
        m_pProgBar->setValue( now );
    }
    update();
    //qDebug()<<__FUNCTION__<<__LINE__<<now<<total;
}

void WndRecord::updatePlay(int now, int total)
{
    if ( total == 0 )
    {
        m_pProgBar->reset();
    }
    else
    {
        m_pProgBar->setRange( 0, total );
        m_pProgBar->setValue( now );
    }
    update();
    //qDebug()<<__FUNCTION__<<__LINE__<<now<<total;
}

void WndRecord::updateTimeStamp(qlonglong time)
{
    QString str = "";
    double fTime = (double)time/time_s(1);

    DsoType::DsoViewFmt fmt = dso_fmt_trim(fmt_float,fmt_width_9,fmt_no_trim_0);
    gui_fmt::CGuiFormatter::format( str, fTime, fmt, Unit_s);

    m_pTimeStamp->setText("ΔT = " + str);
    m_pTimeStamp->setAlignment(Qt::AlignRight);
    update();
}
