#ifndef CAppRecord_H
#define CAppRecord_H

#include <QtWidgets>

#include "../capp.h"
#include "wndrecord.h"
class  CAppRecord : public CApp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    CAppRecord();

public:
    enum appcmd
    {
        cmd_base = CMD_APP_BASE,

    };

public:
    virtual void setupUi( CAppMain *pMain );
    virtual void retranslateUi();
    virtual void buildConnection();

protected:
    virtual  void registerSpy();

protected Q_SLOTS:
    void on_sig_btn_click( WndRecord::wndSubButton subBtn );

private:
    int onOpenRecord();

    int onInitUpdate();
    int onRecUpdate();
    int onPlayUpdate();

    void onStatUpdate();

private:
    WndRecord *m_pWnd;
};
#endif // CAppRecord_H

