#include "capprecord.h"
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"
#include "../../gui/cguiformatter.h"
#include "../appmain/cappmain.h"

IMPLEMENT_CMD( CApp, CAppRecord )
start_of_entry()
on_set_int_void  (MSG_RECORD_ONOFF,            &CAppRecord::onOpenRecord),

on_set_int_void  (servRecord::cmd_reced_count, &CAppRecord::onRecUpdate),

on_set_int_void  (MSG_RECORD_PLAY,             &CAppRecord::onStatUpdate),
on_set_int_void  (MSG_RECORD_CURRENT,          &CAppRecord::onStatUpdate),

//!spy
on_set_int_void  (MSG_HORI_MAIN_SCALE,      &CAppRecord::onRecUpdate),
on_set_int_void  (MSG_HORI_ZOOM_SCALE,      &CAppRecord::onRecUpdate),
on_set_int_void  (MSG_RECORD_FRAMES,        &CAppRecord::onRecUpdate),


on_set_void_void( servRecord::cmd_inner_state, &CAppRecord::onStatUpdate ),

end_of_entry()

CAppRecord::CAppRecord()
{
    mservName =  serv_name_wrec;

    addQuick(   MSG_APP_RECORD,
                QStringLiteral("ui/desktop/record/icon/") );

    m_pWnd = NULL;
}

void CAppRecord::setupUi( CAppMain *pMain )
{
    m_pWnd   = new WndRecord(pMain->getCentBar());
    Q_ASSERT(m_pWnd);
}

void CAppRecord::retranslateUi()
{
    QString titleStyle = QString("font:12pt;font-family:%1").arg(qApp->font().family());
    m_pWnd->setTitleStyle(titleStyle);
    if(m_pWnd->isVisible())
    {
        m_pWnd->redraw();
    }
}

void CAppRecord::buildConnection()
{
    //! build connect
    connect( m_pWnd, SIGNAL(sig_btn_clicked(WndRecord::wndSubButton)),
             this, SLOT(on_sig_btn_click(WndRecord::wndSubButton) ) );
}

void CAppRecord::registerSpy()
{
    spyOn(serv_name_hori, MSG_HORI_MAIN_SCALE);
    spyOn(serv_name_hori, MSG_HORI_ZOOM_SCALE);
}

void CAppRecord::on_sig_btn_click( WndRecord::wndSubButton subBtn )
{
    enter_service_context();

    //! frame range
    int from, to, now;
    query( mservName, MSG_RECORD_STARTFRAME, from );
    query( mservName, MSG_RECORD_ENDFRAME, to );
    query( mservName, MSG_RECORD_CURRENT, now );

    //! state
    query_service_enum( mservName, servRecord::cmd_inner_state,
                        servRecord::RecordState, state );
    if ( !_bOK )
    { return; }

    if ( subBtn == WndRecord::btn_play_back )
    {
        if ( now > from )
        { now--; }

        serviceExecutor::post( mservName, MSG_RECORD_CURRENT, now );
    }
    else if ( subBtn == WndRecord::btn_play_first )
    {
        serviceExecutor::post( mservName, MSG_RECORD_CURRENT, from );
    }
    else if ( subBtn == WndRecord::btn_play_next )
    {
        if ( now < to )
        { now++; }

        serviceExecutor::post( mservName, MSG_RECORD_CURRENT, now );
    }
    else if ( subBtn == WndRecord::btn_play_last )
    {
        serviceExecutor::post( mservName, MSG_RECORD_CURRENT, to );
    }
    else if ( subBtn == WndRecord::btn_play_start )
    {
        //! stop play
        if ( state == servRecord::Play_ing )
        {  serviceExecutor::post( mservName, MSG_RECORD_PLAY, false ); }
        else
        { serviceExecutor::post( mservName, MSG_RECORD_PLAY, true ); }
    }
    else if ( subBtn == WndRecord::btn_rec_start )
    {
        //! stop record
        if ( state == servRecord::Record_ing )
        {  serviceExecutor::post( mservName, MSG_RECORD_START, false ); }
        else
        { serviceExecutor::post( mservName, MSG_RECORD_START, true ); }
    }
    else
    {}

    exit_service_context();
}

int CAppRecord::onOpenRecord()
{
    bool b = false;
    query(MSG_RECORD_ONOFF, b);

    if(b)
    {
        //! create ui
        if ( m_pWnd->setup("apprecord/", r_meta::CAppMeta() ) )
        {
            m_pWnd->setTitle( MSG_APP_RECORD );
        }

        m_pWnd->setMode( WndRecord::wnd_record );

        m_pWnd->show();
    }
    else
    {
        m_pWnd->hide();
    }

    onRecUpdate();
    return ERR_NONE;
}

int CAppRecord::onInitUpdate()
{
    if ( !m_pWnd->isVisible() )
    { return ERR_NONE; }

    int recSize = 0;

    query( mservName, MSG_RECORD_FRAMES, recSize );

    m_pWnd->updateRecord( 0, recSize );
    m_pWnd->updateTimeStamp(0);
    return ERR_NONE;
}

int CAppRecord::onRecUpdate()
{
    if ( !m_pWnd->isVisible() )
    { return ERR_NONE; }

    int recCnt, recSize;
    qlonglong time = 0;

    query( mservName, MSG_RECORD_CURRENT, recCnt );
    query( mservName, MSG_RECORD_FRAMES, recSize );
    query( mservName, servRecord::cmd_time_stamp, time );

    m_pWnd->updateRecord( recCnt, recSize );
    m_pWnd->updateTimeStamp(time);

    return ERR_NONE;
}

int CAppRecord::onPlayUpdate()
{
    int recedCount, playNow;
    qlonglong time = 0;

    if ( !m_pWnd->isVisible() )
    { return ERR_NONE; }

    query( mservName, MSG_RECORD_CURRENT, playNow );
    query( mservName, servRecord::cmd_reced_count, recedCount );
    query( mservName, servRecord::cmd_time_stamp, time );

    m_pWnd->updatePlay( playNow, recedCount );
    m_pWnd->updateTimeStamp(time);

    return ERR_NONE;
}

void CAppRecord::onStatUpdate()
{

    enter_app_context();

    query_service_enum( mservName, servRecord::cmd_inner_state,
                        servRecord::RecordState, state );
    if ( !_bOK )
    { return; }

    if ( !m_pWnd->isVisible() )
    { return; }

    if ( state == servRecord::Record_disable )
    {
        m_pWnd->m_pBtnRecStartStop->setChecked( false );
        m_pWnd->m_pBtnPlayPause->setChecked( false );
        onInitUpdate();
    }
    else if ( state == servRecord::Record_empty )
    {
        m_pWnd->setMode( WndRecord::wnd_record );
        onInitUpdate();
    }
    else if ( state == servRecord::Record_ing )
    {
        m_pWnd->m_pBtnRecStartStop->setChecked( true );

        m_pWnd->setSubEnable( WndRecord::btn_play_first, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_back, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_next, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_last, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_start, false );

        m_pWnd->setSubEnable( WndRecord::btn_rec_start, true );

        //! frames
        onRecUpdate();
    }
    else if ( state == servRecord::Record_end )
    {
        m_pWnd->setMode( WndRecord::wnd_play );

        m_pWnd->m_pBtnRecStartStop->setChecked( false );
        m_pWnd->m_pBtnPlayPause->setChecked( false );

        m_pWnd->setSubEnable( WndRecord::btn_play_first, true );
        m_pWnd->setSubEnable( WndRecord::btn_play_back, true );
        m_pWnd->setSubEnable( WndRecord::btn_play_next, true );
        m_pWnd->setSubEnable( WndRecord::btn_play_last, true );
        m_pWnd->setSubEnable( WndRecord::btn_play_start, true );

        m_pWnd->setSubEnable( WndRecord::btn_rec_start, true );
        //! frames
        onRecUpdate();
    }
    else if ( state == servRecord::Play_ing )
    {
        m_pWnd->m_pBtnPlayPause->setChecked( true );

        m_pWnd->setSubEnable( WndRecord::btn_play_first, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_back, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_next, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_last, false );
        m_pWnd->setSubEnable( WndRecord::btn_play_start, true );

        m_pWnd->setSubEnable( WndRecord::btn_rec_start, false );

        //! frames
        onPlayUpdate();
    }
    else
    {onInitUpdate();}


    exit_app_context();
}
