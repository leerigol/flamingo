#include <QPainter>
#include "cselectline.h"
#include "../appdisplay/cgrid.h"

///
/// \brief CSelectLine::CSelectLine
/// \param parent
///
CSelectLine::CSelectLine(QWidget *parent) : QWidget(parent)
{
    m_nColor = Trig_Color;

    m_nStart = 0;
    m_nRange = CGrid::getWaveHeight();


    mPos = 240;

    m_viewRect.setRect(0,0,1000,m_nRange);
    m_phyRect = m_viewRect;

    m_pTimer = new QTimer(this);
    Q_ASSERT(m_pTimer != NULL);
}

void CSelectLine::setupUi()
{
    this->move(0, m_nRange/2);
    this->resize(CGrid::getWaveWidth(), 1);

    connect( m_pTimer, SIGNAL(timeout()), this, SLOT(onTimeout()) );
}

void CSelectLine::setWidgetVisible( bool b )
{
    setVisible( b );
}

void CSelectLine::refresh()
{
    //! update pos
    viewPos( mPos );

    update();
}


void CSelectLine::viewPos( int posView )
{
    int phyPos;
    int nMiddle = m_phyRect.height() >> 1;
    phyPos = nMiddle - ( posView - m_viewRect.top() ) * m_phyRect.height() / m_viewRect.height() + m_phyRect.top();
    if ( phyPos < m_phyRect.top() )
    {
        phyPos = m_phyRect.top();
    }
    else if ( phyPos > m_phyRect.bottom() )
    {
        phyPos = m_phyRect.bottom();
    }
    else
    {}

    move( m_phyRect.left(), phyPos );
}

void CSelectLine::setColor(QColor color)
{
    m_nColor = color;
    update();
}


void CSelectLine::setPos(int pos)
{
//    int nRealPos;
//    int nMiddle = m_nRange >> 1;

//    int nHeight = CGrid::getWaveHeight();

    if(this->isHidden())
    {
        this->show();
    }

    viewPos(pos);
//    nRealPos = m_nStart + nMiddle - (pos * m_nRange / nHeight);
//    if(nRealPos < m_nStart)
//    {
//        nRealPos = m_nStart;
//    }
//    else if(nRealPos > (m_nStart+m_nRange - 1))
//    {
//        nRealPos = m_nStart+m_nRange - 1;
//    }

//    this->move(0, nRealPos);

    if(m_pTimer)
    {
        m_pTimer->start(3000);
    }
}


void CSelectLine::setRange(int start, int range)
{
    Q_ASSERT(start >= 0 && start <  CGrid::getWaveHeight());
    Q_ASSERT(range >= 0 && range <= CGrid::getWaveHeight());

    m_nStart = start;
    m_nRange = range;
}

void CSelectLine::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPen pen;

    pen.setStyle(Qt::DashDotLine);
    pen.setColor( m_nColor );
    painter.setPen(pen);

    painter.drawLine(0,0,0+CGrid::getWaveWidth(),0);

    //qDebug() << this->geometry();
}

void CSelectLine::onTimeout()
{
    if(m_pTimer)
    {
        m_pTimer->stop();
    }
    this->hide();
}
