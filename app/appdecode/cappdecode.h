#ifndef CAPPDECODE_H
#define CAPPDECODE_H

#include "../capp.h"


#include "../../menu/dsowidget/rdsogndtag.h"
#include "../../service/servdecode/eventTable/reventtable.h"

#include "rbuswidget.h"
#include "eventwidget.h"
#include "cselectline.h"

class CAppDecode : public CApp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    CAppDecode( int id );

public:
    void setupUi( CAppMain *pMain );
    void retranslateUi();

    void buildConnection();
    void registerSpy();

protected Q_SLOTS:
    void onEventClose();
    void on_gnd_click();
    void on_enter_active();
    void setViewMode(int num);

protected:
    void onDecodeEnable();
    void onDecodePos();
    void onDecodeOK();
    void onDecodeEvt();
    void onViewMode();
    void onDecodeLabel();
    void onFormat();

    int  onThreshold(int);
    void onSyncEvt();

    void onEventFormat();
    void setJumpEvent();
    int  onViewChanged();
    int  onDisplayClear();

    int  onEvtBusX();

private:
    int mDecoderId;

    menu_res::RDsoVGnd *m_pGnd;
    menu_res::RDsoVGnd *m_pZoomGnd;
//    static QList<menu_res::RDsoGnd *> mGndList;

    RBusWidget *m_pBusWidget;
    RBusWidget *m_pBusWidgetZoom;

    /* One event table window for all decoders */
    static EventWidget *m_pEventTable;
    static CSelectLine *m_pLine;
    static CSelectLine *m_pLineZoom;
    static int m_nLabelPos;

    bool        m_bShowEvt;
    int         m_nScrMode;
};

#endif // CAPPDECODE_H
