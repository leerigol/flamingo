#include "eventwidget.h"
#include "../../menu/rmenus.h"
#include "../../meta/crmeta.h"
menu_res::RInfoWnd_Style EventWidget::_style;
EventWidget::EventWidget(QWidget *parent) : menu_res::uiWnd(parent)
{
    mInited = false;
    this->loadResource( "event/" );
    _style.loadResource("measure/");

    creatItems();
    buildConnection();

    init();

    addTuneKey();
    keyChanged();

    mColumn = 0;
    mRow = 0;
    horiOffset = 0;

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void EventWidget::retranslateUi()
{

}

void EventWidget::buildConnection()
{
    connect(packetsBtn,SIGNAL(pressed()),this,SLOT(packetsShow()));
    connect(detailsBtn,SIGNAL(pressed()),this,SLOT(detailsShow()));
    connect(payloadBtn,SIGNAL(pressed()),this,SLOT(payloadShow()));
    connect(closeBtn,SIGNAL(pressed()),this,SLOT(closeThis()));
    connect(packetsTable,SIGNAL(itemSelectionChanged()),this,SLOT(getPacketsChoosedNum()));
}

void EventWidget::init()
{
    packetsTable->clear();
    detailsTable->clear();
    payloadTable->clear();
    packetsTableSetting();
    detailsTableSetting();
    payloadTableSetting();
}

void EventWidget::setTableValue(CTable *table)
{
    pTable = table;
    Q_ASSERT( NULL != pTable );

    QString str;
    //Q_ASSERT( NULL != pTable->getTitle() );

    if( pTable->getTitle() != NULL )
    {
        pTable->getTitle()->format( pTable->getContext(), mTitle );//rs232
    }
    else
    {
        qDebug() << "Title error";
    }

    mColumn = pTable->column();
    CCell   *pCell;
    CCellDW *pData;
    quint32  number;
    Q_ASSERT( NULL != pTable->getLayout() );
    pCell = pTable->getLayout()->head();
    Q_ASSERT( NULL != pCell );
    mColWidth.clear();
    for(int i=0; i<pTable->getLayout()->size(); i++)
    {
        if(pCell != NULL)
        {
            pData = (CCellDW *)(pCell->data());
            number = pData->getData();
            mColWidth.append(number);
            pCell = pCell->next();
        }
    }

    colTitle.clear();
    Q_ASSERT( NULL != pTable->getHead() );
    pCell = pTable->getHead()->head();
    Q_ASSERT( NULL != pCell );
    while( pCell != NULL ){
        Q_ASSERT( NULL != pCell->data() );
        pCell->data()->format( pTable->getContext(), str );
        colTitle.append(str);
        pCell = pCell->next();
    }

    //row
    if(!pTable->getSheet())
        return;

    pCell = pTable->getSheet()->head();
    if(!pCell)
        return;

    mDataCol.clear();
    mDataSheet.clear();
    int tCol = 0;
    int row = 0;
    while( pCell != NULL )
    {
        pCell->format(pTable->getContext(),str);
        if( pCell->data() == NULL )
        {
            qDebug() << "Warnning.......EventTable";
            break;
        }


        if( pCell->data()->getIsData() )
        {
            QStringList list = str.split(" ", QString::SkipEmptyParts);

            QString temp="";
            foreach(QString s, list)
            {
                if(!s.isEmpty())
                {
                    bool b;
                    int dataVal = s.toInt(&b,16);
                    if(F_ASC == mCurrFormat)
                    {
                        int index = dataVal & 0x7f;
                        temp.append( CDecodeFormat::_ascTable[index]+" " );
                    }
                    else if(F_DEC == mCurrFormat)
                    {
                        temp.append(QString::number(dataVal)+" ");
                    }
                    else if(F_BIN == mCurrFormat)
                    {
                        QString binStr = QString("%1").arg(dataVal, 8, 2, QChar('0'));
                        temp.append(binStr);
                    }
                }
            }
            if( temp.size() > 0)
            {
                str.clear();
                str = temp;
            }

            if(!mDataCol.contains(tCol))
            {
                mDataCol.append(tCol);
            }
        }
        mDataSheet.append(str);

        tCol++;
        if ( tCol >= pTable->column() )
        {
            tCol = 0;
            row++;
        }
        pCell = pCell->next();
    }
    mRow = row;

    packetsTableSetValue();
    detailsTableSetValue();
    payloadTableSetValue();
}

void EventWidget::setCurrFormat(DecodeFormat format)
{
    mCurrFormat = format;

    if(pTable != NULL)
    {
        setTableValue(pTable);
    }
}

void EventWidget::loadResource(const QString &root)
{
    if ( mInited ) return;

    r_meta::CRMeta rMeta;

    rMeta.getMetaVal( root + "tclose", mCloseQss );
    rMeta.getMetaVal( root + "qss", mTableStyle );

    int mRectx=0,mRecty=0,mRectw=0,mRecth=0;
    rMeta.getMetaVal( root + "x", mRectx );
    rMeta.getMetaVal( root + "y", mRecty );
    rMeta.getMetaVal( root + "w", mRectw );
    rMeta.getMetaVal( root + "h", mRecth );
    this->setGeometry(mRectx, mRecty, mRectw, mRecth);

    mInited = true;
}

void EventWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
     QPainter painter(this);

     QRect r = rect();
     _style.paint( painter, r );

     QRect rLeftBox;
     rLeftBox.setX(r.x()+8);
     rLeftBox.setY(r.y()+37);
     rLeftBox.setWidth(r.width()-15);
     rLeftBox.setHeight(r.height()-37-10);

     painter.fillRect(rLeftBox, QColor(0,88,176,255));

     rLeftBox.setX(rLeftBox.x()+1);
     rLeftBox.setY(rLeftBox.y()+1);
     rLeftBox.setWidth(rLeftBox.width()-1);
     rLeftBox.setHeight(rLeftBox.height()-1);

     const qreal radius = 5;
     QPainterPath path;
     path.moveTo(rLeftBox.topRight() - QPointF(radius, 0));
     path.lineTo(rLeftBox.topLeft() + QPointF(radius, 0));
     path.quadTo(rLeftBox.topLeft(), rLeftBox.topLeft() + QPointF(0, radius));
     path.lineTo(rLeftBox.bottomLeft() + QPointF(0, -radius));
     path.quadTo(rLeftBox.bottomLeft(), rLeftBox.bottomLeft() + QPointF(radius, 0));
     path.lineTo(rLeftBox.bottomRight() - QPointF(radius, 0));
     path.quadTo(rLeftBox.bottomRight(), rLeftBox.bottomRight() + QPointF(0, -radius));
     path.lineTo(rLeftBox.topRight() + QPointF(0, radius));
     path.quadTo(rLeftBox.topRight(), rLeftBox.topRight() + QPointF(-radius, -0));
     painter.fillPath(path, QColor(0,0,0,255));
     painter.drawPath(path);

}

void EventWidget::tuneInc(int )
{
    if(stackedWidget->currentIndex() == 0)
    {
        if(packetsTable->currentRow())
        {
            packetsTable->setCurrentCell(packetsTable->currentRow()-1, 0);
        }
    }
}

void EventWidget::tuneDec(int  )
{
    if(stackedWidget->currentIndex() == 0)
    {
        if(packetsTable->currentRow() != packetsTable->rowCount()-1)
        {
            packetsTable->setCurrentCell(packetsTable->currentRow()+1, 0);
        }
    }
}

void EventWidget::creatItems()
{
    packetsBtn = new QPushButton(tr("Packets"),this);
    detailsBtn = new QPushButton(tr("Details"),this);
    payloadBtn = new QPushButton(tr("Payload"),this);
    closeBtn = new QPushButton(this);
    pLabel   = new QLabel( this);

    packetsBtn->setGeometry(8,8,87,30);
    detailsBtn->setGeometry(96,8,87,30);
    payloadBtn->setGeometry(185,8,87,30);
    closeBtn->setGeometry(550,8,38,29);
    pLabel->setGeometry(380,8,100,30);

    packetsBtn->setFocusPolicy(Qt::TabFocus);
    detailsBtn->setFocusPolicy(Qt::TabFocus);
    payloadBtn->setFocusPolicy(Qt::TabFocus);
    closeBtn->setFocusPolicy(Qt::TabFocus);

    packetsBtn->setFlat(true);
    detailsBtn->setFlat(true);
    payloadBtn->setFlat(true);

    packetsBtn->setStyleSheet("background-color:rgb(0,88,176);border:none;color: rgb(255, 255, 255);");
    detailsBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");
    payloadBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");
    closeBtn->setStyleSheet(mCloseQss);
    pLabel->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");

    stackedWidget = new QStackedWidget(this);
    stackedWidget->setGeometry(11, 41, 579, 226);
    stackedWidget->setAutoFillBackground(false);
    stackedWidget->setFrameShape(QFrame::NoFrame);

    packetsPage = new QWidget();
    packetsTable = new TableWidget(packetsPage);
    packetsTable->resize( 579, 226);
    stackedWidget->addWidget(packetsPage);

    detailsPage = new QWidget();
    detailsTable = new TableWidget(detailsPage);
    detailsTable->resize( 579, 226);
    stackedWidget->addWidget(detailsPage);

    payloadPage = new QWidget();
    payloadTable = new TableWidget(payloadPage);
    payloadTable->resize( 579, 226);
    stackedWidget->addWidget(payloadPage);

    stackedWidget->setCurrentIndex(0);
}

void EventWidget::packetsTableSetting()
{
    packetsTable->horizontalHeader()->setStretchLastSection(true);
    packetsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
    packetsTable->setStyleSheet(mTableStyle);

    packetsTable->setColumnCount( 4 );

    QTableWidgetItem *pTableItem = new QTableWidgetItem(" ");
    packetsTable->setHorizontalHeaderItem(0, pTableItem);

    QTableWidgetItem *pTableItem1 = new QTableWidgetItem("Time");
    packetsTable->setHorizontalHeaderItem(1, pTableItem1);

    QTableWidgetItem *pTableItem2 = new QTableWidgetItem("Data");
    packetsTable->setHorizontalHeaderItem(2, pTableItem2);

    QTableWidgetItem *pTableItem3 = new QTableWidgetItem("Err");
    packetsTable->setHorizontalHeaderItem(3, pTableItem3);

    packetsTable->setColumnWidth(0, 50);
    packetsTable->setColumnWidth(1, 100);
    packetsTable->setColumnWidth(2, 250);

    if (packetsTable->rowCount() < 10)
    {
        packetsTable->setRowCount(10);
    }

    for(int row = 0; row < packetsTable->rowCount(); row++)
    {
        QTableWidgetItem *tempItem = new QTableWidgetItem();
        tempItem->setTextAlignment(Qt::AlignRight);
        packetsTable->setItem(row, 0, tempItem);
        packetsTable->setRowHeight(row,20);
        packetsTable->item(row, 0)->setText(QString(" %1 ")
                                            .arg(row+1, 0, 10, QChar(' ')));
    }

}

void EventWidget::detailsTableSetting()
{
    detailsTable->setFocusPolicy(Qt::NoFocus);
    detailsTable->setSelectionMode(QAbstractItemView::NoSelection);
    detailsTable->setShowGrid(false);
    detailsTable->setStyleSheet(mTableStyle);

    if (detailsTable->columnCount() < 17)
    {
        detailsTable->setColumnCount(17);
    }

    QTableWidgetItem *tempItem = new QTableWidgetItem(" ");
    detailsTable->setHorizontalHeaderItem(0, tempItem);
    detailsTable->setColumnWidth(0, 100);

    for(int column = 0; column < 16; column++){
        QTableWidgetItem *tempItem = new QTableWidgetItem(QString("%1")
                                                          .arg(column, 0, 16));
        detailsTable->setHorizontalHeaderItem(column+1, tempItem);
    }

    if (detailsTable->rowCount() < 10)
        detailsTable->setRowCount(10);

    for(int row = 0; row < detailsTable->rowCount(); row++)
    {
        QTableWidgetItem *tempItem = new QTableWidgetItem(QString("%1h: ")
                                                          .arg(row, 8, 16, QChar('0')));
        tempItem->setTextAlignment(Qt::AlignRight);
        detailsTable->setItem(row, 0, tempItem);
        detailsTable->setRowHeight(row,20);
    }
}

void EventWidget::payloadTableSetting()
{
    payloadTable->setFocusPolicy(Qt::NoFocus);
    payloadTable->setSelectionMode(QAbstractItemView::NoSelection);
    payloadTable->setShowGrid(false);
    payloadTable->setStyleSheet(mTableStyle);

    if (payloadTable->columnCount() < 17)
    {
        payloadTable->setColumnCount(17);
    }

    QTableWidgetItem *item = new QTableWidgetItem(" ");
    payloadTable->setHorizontalHeaderItem(0, item);
    payloadTable->setColumnWidth(0, 100);

    for(int column = 0; column < 16; column++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(QString("%1")
                                                      .arg(column, 0, 16));
        payloadTable->setHorizontalHeaderItem(column+1, item);
    }

    if(payloadTable->rowCount() < 10)
    {
        payloadTable->setRowCount(10);
    }

    for(int row = 0; row < payloadTable->rowCount(); row++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(QString("%1h: ")
                                                      .arg(row, 8, 16, QChar('0')));
        item->setTextAlignment(Qt::AlignRight);
        payloadTable->setItem(row, 0 ,item);
        payloadTable->setRowHeight(row,20);
    }
}


void EventWidget::packetsTableSetValue()
{
    packetsTable->clear();

    packetsTable->setColumnCount(mColumn + 1);// 4 column
    for(int i=0; i<mColWidth.count(); i++)
    {
        packetsTable->setColumnWidth(i, mColWidth.at(i));
    }

    if(!packetsTable->horizontalHeaderItem(0))
    {
        QTableWidgetItem *pTableItem = new QTableWidgetItem(" ");// 1 title
        packetsTable->setHorizontalHeaderItem(0, pTableItem);
    }

    for(int i=1; i<=colTitle.size(); i++)
    {
        if(!packetsTable->horizontalHeaderItem(i)){
            QTableWidgetItem *pTableItem = new QTableWidgetItem();
            packetsTable->setHorizontalHeaderItem(i, pTableItem);
        }
        packetsTable->horizontalHeaderItem(i)->setText(colTitle.at(i-1));
    }

    if (packetsTable->rowCount() <= mRow && mRow >= 10)
    {
        packetsTable->setRowCount(mRow+1);
    }
    else if(packetsTable->rowCount() <= mRow)
    {
        packetsTable->setRowCount(10);
    }

    for(int i=0; i<packetsTable->rowCount(); i++)
    {
        packetsTable->setRowHeight(i, 20);

        for(int colNum=1; colNum<=mColumn; colNum++)
        {
            if(!packetsTable->item(i,colNum))
            {
                QTableWidgetItem *pTableItem = new QTableWidgetItem();
                pTableItem->setTextAlignment(Qt::AlignCenter);
                packetsTable->setItem(i, colNum, pTableItem);
            }
            if(mDataSheet.size()> mColumn*i+colNum-1)
            {
                if(!packetsTable->item(i,0))
                {
                    QTableWidgetItem *pTableItem = new QTableWidgetItem();
                    pTableItem->setTextAlignment(Qt::AlignCenter);
                    packetsTable->setItem(i, 0, pTableItem);
                }
                packetsTable->item(i,0)->setText(QString(" %1 ").arg(i+1, 0, 10, QChar(' ')));

                packetsTable->item(i,colNum)->setText(mDataSheet.at(mColumn*i+colNum-1));
            }
        }
    }
    packetsTable->setCurrentCell(0, 0);
}

void EventWidget::detailsTableSetValue()
{
    if (detailsTable->columnCount() < 17)
    {
        detailsTable->setColumnCount(17);
    }

    QTableWidgetItem *tempItem = new QTableWidgetItem(" ");
    detailsTable->setHorizontalHeaderItem(0, tempItem);
    detailsTable->setColumnWidth(0, 100);

    for(int column = 0; column < 16; column++)
    {
        QTableWidgetItem *tempItem = new QTableWidgetItem(QString("%1")
                                                          .arg(column, 0, 16));
        detailsTable->setHorizontalHeaderItem(column+1, tempItem);
    }

    detailsDataChanged();
}

void EventWidget::payloadTableSetValue()
{
    payloadTable->clearContents();
    QString strData;
    if(mDataCol.size()>0)
    {
        for(int i=0; i<=mRow; i++)
        {
            QTableWidgetItem *pTableItem = packetsTable->item(i, mDataCol.at(0)+1);
            if(pTableItem != NULL)
            {
                if(pTableItem->text().size() != 0)
                {
                    strData.append( pTableItem->text() + " ");
                }
            }
        }

        QStringList list = strData.split(" ", QString::SkipEmptyParts);
        if(list.size()>0)
        {
            int rows = (list.size()+15)/16;
            if (payloadTable->rowCount() <= rows  && rows >= 10)
            {
                payloadTable->setRowCount(rows);
            }else if(payloadTable->rowCount() <= 10 && rows <= 10)
            {
                payloadTable->setRowCount(10);
            }

            for(int row = 0; row < payloadTable->rowCount(); row++)
            {
                if(!payloadTable->item(row, 0))
                {
                    QTableWidgetItem *item = new QTableWidgetItem();
                    item->setTextAlignment(Qt::AlignRight);
                    payloadTable->setItem(row, 0 ,item);
                    payloadTable->setRowHeight(row,20);
                }
            }

            for(int row=0; row<rows; row++)
            {
                payloadTable->item(row,0)->setText(QString("%1h: ").arg(row, 8, 16, QChar('0')));
            }

            for(int i=0; i<list.count(); i++)
            {
                if(!payloadTable->item(i/16,i%16+1))
                {
                    QTableWidgetItem *pTableItem = new QTableWidgetItem(list.at(i));
                    pTableItem->setTextAlignment(Qt::AlignCenter);
                    payloadTable->setItem(i/16, i%16+1, pTableItem);
                }
                payloadTable->item(i/16,i%16+1)->setText(list.at(i));
            }
        }
    }
}

void EventWidget::detailsDataChanged()
{
    detailsTable->clearContents();
    if (detailsTable->rowCount() < 10)
    {
        detailsTable->setRowCount(10);
    }

    for(int row = 0; row < detailsTable->rowCount(); row++)
    {
        if(!detailsTable->item(row,0))
        {
            QTableWidgetItem *tempItem = new QTableWidgetItem();
            tempItem->setTextAlignment(Qt::AlignRight);
            detailsTable->setItem(row, 0, tempItem);
            detailsTable->setRowHeight(row,20);
        }
    }

    int row = packetsTable->currentRow();
    int col = 0;
    if(mDataCol.size() > 0)
        col = mDataCol.at(0)+1;

    if(mDataCol.size()>0)
    {
        QTableWidgetItem *pTableItem = packetsTable->item(row, col);
        if(!pTableItem)
        {
            return;
        }

        if(pTableItem->text().size())
        {
            QString strData = pTableItem->text();
            QStringList list = strData.split(" ");

            for(int row=0; row<(list.size()+15)/16; row++)
            {
                detailsTable->item(row,0)->setText(QString("%1h: ").arg(row, 8, 16, QChar('0')));
            }

            for(int i=0; i<list.count(); i++)
            {
                if(!detailsTable->item(i/16,i%16+1))
                {
                    QTableWidgetItem *pTableItem = new QTableWidgetItem();
                    pTableItem->setTextAlignment(Qt::AlignCenter);
                    detailsTable->setItem(i/16, i%16+1, pTableItem);
                }
                detailsTable->item(i/16, i%16+1)->setText(list.at(i));
            }
        }
    }
}

void EventWidget::tableClear()
{
    packetsTable->clear();
    packetsTable->setRowCount(0);
    packetsTable->setColumnCount(0);

    detailsTable->clear();
    detailsTable->setRowCount(0);
    detailsTable->setColumnCount(0);

    payloadTable->clear();
    payloadTable->setRowCount(0);
    payloadTable->setColumnCount(0);
}

bool EventWidget::isPacketIsNULL()
{
    return mDataSheet.count();
}

int EventWidget::getPacketsChoosedNum()
{
    raise();
    detailsDataChanged();

    return packetsTable->currentRow();
}

DsoErr EventWidget::setPacketsJustTo()
{
    int stat;
    serviceExecutor::query( serv_name_hori, MSG_HORIZONTAL_RUN, stat );
    if((ControlStatus)stat == Control_Stoped )
    {
        int col = 0;
        for( ; col<colTitle.count(); col++)
        {
            QString str = colTitle.at(col);
            if(str.contains("time",Qt::CaseInsensitive))
            {
                col++;
                break;
            }
        }

        QString horiTime = packetsTable->item(packetsTable->currentRow(), col)->text().simplified();
        if(horiTime != "")
        {
            horiTime = horiTime.remove("s",Qt::CaseInsensitive);

            if(horiTime.contains("p",Qt::CaseInsensitive))
            {
                horiTime = horiTime.remove("p",Qt::CaseInsensitive);
                horiOffset = horiTime.toFloat();
            }
            else if(horiTime.contains("n",Qt::CaseInsensitive))
            {
                horiTime = horiTime.remove("n",Qt::CaseInsensitive);
                horiOffset = horiTime.toFloat()*(1e3);
            }
            else if(horiTime.contains("u",Qt::CaseInsensitive))
            {
                horiTime = horiTime.remove("u",Qt::CaseInsensitive);
                horiOffset = horiTime.toFloat()*(1e6);
            }
            else if(horiTime.contains("m",Qt::CaseInsensitive))
            {
                horiTime = horiTime.remove("m",Qt::CaseInsensitive);
                horiOffset = horiTime.toFloat()*(1e9);
            }
            else if(horiTime.contains("k",Qt::CaseInsensitive))
            {
                horiTime = horiTime.remove("k",Qt::CaseInsensitive);
                horiOffset = horiTime.toFloat()*(1e15);
            }
            else
            {
                horiOffset = horiTime.toFloat()*(1e12);
            }

            serviceExecutor::post(serv_name_hori, MSG_HORI_MAIN_OFFSET, horiOffset );
        }
    }

    return ERR_NONE;
}


void EventWidget::packetsShow()
{
    if(stackedWidget->currentIndex() != 0)
    {
        packetsBtn->setStyleSheet("background-color:rgb(0,88,176);border:none;color: rgb(255, 255, 255);");
        detailsBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");
        payloadBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");

        stackedWidget->setCurrentIndex(0);
        emit sigViewMode(0);
        update();
    }
}

void EventWidget::detailsShow()
{
    if(stackedWidget->currentIndex() != 1)
    {
        packetsBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");
        detailsBtn->setStyleSheet("background-color:rgb(0,88,176);border:none;color: rgb(255, 255, 255);");
        payloadBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");

        stackedWidget->setCurrentIndex(1);
        emit sigViewMode(1);
        update();
    }
}

void EventWidget::payloadShow()
{
    if(stackedWidget->currentIndex() != 2)
    {
        packetsBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");
        detailsBtn->setStyleSheet("background-color:transparent;border:none;color: rgb(100, 100, 100);");
        payloadBtn->setStyleSheet("background-color:rgb(0,88,176);border:none;color: rgb(255, 255, 255);");

        stackedWidget->setCurrentIndex(2);
        emit sigViewMode(2);
        update();
    }
}

void EventWidget::closeThis()
{
    emit sigEventClose();
    this->close();
}

void EventWidget::setBusX(QString str)
{
    pLabel->setText(str);
}
