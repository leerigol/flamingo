#ifndef CAPPDECODEGP_H
#define CAPPDECODEGP_H

#include "../capp.h"

class CAppDecodeGp : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
    CAppDecodeGp();
};

#endif // CAPPDECODEGP_H
