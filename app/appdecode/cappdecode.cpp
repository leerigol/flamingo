#include "cappdecode.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../appmain/cappmain.h"
#include "../../service/servdecode/servdecode.h"

#define  LABEL_POS 25


//! msg table
IMPLEMENT_CMD( CApp, CAppDecode )
start_of_entry()

on_set_void_void( MSG_DECODE_ONOFF,             &CAppDecode::onDecodeEnable ),
on_set_void_void( CMD_SERVICE_STARTUP,          &CAppDecode::onDecodeEnable ),

on_set_void_void( MSG_DECODE_POS,               &CAppDecode::onDecodePos ),
on_set_void_void( MSG_DECODE_EVT,               &CAppDecode::onDecodeEvt),
on_set_void_void( MSG_DECODE_FORMAT,            &CAppDecode::onFormat),
on_set_void_void( MSG_DECODE_LABEL,             &CAppDecode::onDecodeLabel),
on_set_void_void( servDec::cmd_bus_completed,   &CAppDecode::onDecodeOK ),

on_set_int_int  ( MSG_DECODE_PAL_CLK_THRE,      &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_PAL_DAT_THRE,      &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_TX_THRE,           &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_RX_THRE,           &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_SCL_THRE,          &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_SDA_THRE,          &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_CS_THRE,           &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_CLK_THRE,          &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_MISO_THRE,         &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_MOSI_THRE,         &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_LIN_THRE,          &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_CAN_THRE,          &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_FLEX_THRE,         &CAppDecode::onThreshold),

on_set_int_int  ( MSG_DECODE_I2S_SCLK_THRE,     &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_I2S_DATA_THRE,     &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_I2S_WS_THRE,       &CAppDecode::onThreshold),

on_set_int_int  ( MSG_DECODE_1553B_THRE1,       &CAppDecode::onThreshold),
on_set_int_int  ( MSG_DECODE_1553B_THRE2,       &CAppDecode::onThreshold),

on_set_void_void( MSG_DECODE_EVT_VIEW,          &CAppDecode::onViewMode),
on_set_void_void( MSG_DECODE_EVT_FORMAT,        &CAppDecode::onEventFormat),
on_set_void_void( MSG_DECODE_EVT_JUMP,          &CAppDecode::setJumpEvent),

on_set_int_void ( CMD_SERVICE_VIEW_CHANGE,      &CAppDecode::onViewChanged ),
on_set_int_void ( MSG_DISPLAY_CLEAR,            &CAppDecode::onDisplayClear),

on_set_void_void( CMD_SERVICE_ENTER_ACTIVE,     &CAppDecode::on_enter_active ),
on_set_int_int  ( MSG_DECODE_EVT_BUSX,          &CAppDecode::onEvtBusX ),
end_of_entry()

CSelectLine* CAppDecode::m_pLine = NULL;
CSelectLine* CAppDecode::m_pLineZoom = NULL;

EventWidget* CAppDecode::m_pEventTable = NULL;

int CAppDecode::m_nLabelPos = 0;
//QList<menu_res::RDsoGnd *> CAppDecode::mGndList;
CAppDecode::CAppDecode( int id  )
{
    mDecoderId = id;

    mservName = QString("decode%1").arg( id );

    m_bShowEvt = false;
    m_nScrMode = screen_yt_main;

    m_pBusWidget = NULL;
    m_pBusWidgetZoom = NULL;
}

void CAppDecode::setupUi( CAppMain *pMain )
{

    QString str;

    str = QString("%1").arg( mDecoderId );

    //! gnd
    m_pGnd = new menu_res::RDsoVGnd( str );
    Q_ASSERT( NULL != m_pGnd );
    m_pGnd->setColor( Qt::green );
    m_pGnd->setParent( pMain->getLeftBar() );
    m_pGnd->setView(13,480);
    m_pGnd->setViewGnd( 240 -1);

    m_pZoomGnd = new menu_res::RDsoVGnd( str );
    Q_ASSERT( NULL != m_pZoomGnd );
    m_pZoomGnd->setColor( Qt::green );
    m_pZoomGnd->setParent( pMain->getLeftBar() );
    m_pZoomGnd->setView(13,480);
    m_pZoomGnd->setViewGnd( 240 -1);

    sysAttachUIView( m_pGnd, view_ygnd_main );
    sysAttachUIView( m_pZoomGnd, view_ygnd_zoom );

    m_pGnd->offset( 0 );
    m_pZoomGnd->offset( 0 );

    menu_res::RDsoGnd::mGndList.append(m_pGnd);
    menu_res::RDsoGnd::mGndList.append(m_pZoomGnd);

    //! bus line widget
    m_pBusWidget = new RBusWidget();
    Q_ASSERT( NULL != m_pBusWidget );
    m_pBusWidgetZoom = new RBusWidget();
    Q_ASSERT( NULL != m_pBusWidgetZoom );

    m_pBusWidget->setParent( pMain->getCentBar() );
    m_pBusWidgetZoom->setParent( pMain->getCentBar() );

    //! \todo from app meta
    m_pBusWidget->setGeometry(0,100,1000,120 );
    m_pBusWidget->setView( 1000,480 );
    m_pBusWidget->setViewGnd( 240 );
    m_pBusWidget->offset(0);

    m_pBusWidgetZoom->setGeometry(0,100,1000,120 );
    m_pBusWidgetZoom->setView( 1000, 480 );
    m_pBusWidgetZoom->setViewGnd( 240 );
    m_pBusWidgetZoom->offset(0);

    sysAttachUIView( m_pBusWidget, view_yt_main );
    sysAttachUIView( m_pBusWidgetZoom, view_yt_zoom );

    if( m_pEventTable == NULL )
    {
        //! event table
        m_pEventTable = new EventWidget(pMain->getCentBar());
        Q_ASSERT( NULL != m_pEventTable );
        m_pEventTable->setVisible( false );

        connect( m_pEventTable, SIGNAL(sigEventClose()),
                 this, SLOT(onEventClose()) );

        connect( m_pEventTable, SIGNAL(sigViewMode(int)),
                 this, SLOT(setViewMode(int)) );
    }


    if(m_pLine == NULL)
    {
        m_pLine = new CSelectLine( pMain->getCentBar() );
        m_pLineZoom = new CSelectLine( pMain->getCentBar() );

        m_pLine->hide();
        m_pLine->setupUi();

        m_pLineZoom->hide();
        m_pLineZoom->setupUi();

        sysAttachUIView( m_pLine, view_yt_main );
        sysAttachUIView( m_pLineZoom, view_yt_zoom );
    }
}
void CAppDecode::retranslateUi()
{}

void CAppDecode::buildConnection()
{
    Q_ASSERT( NULL != m_pGnd );
    connect( m_pGnd,SIGNAL(clicked(bool)), this, SLOT(on_gnd_click()) );
    Q_ASSERT( NULL != m_pZoomGnd );
    connect( m_pZoomGnd,SIGNAL(clicked(bool)), this, SLOT(on_gnd_click()) );

}
void CAppDecode::registerSpy()
{
    spyOn(serv_name_display,MSG_DISPLAY_CLEAR);
}

void CAppDecode::on_gnd_click()
{
    serviceExecutor::post( mservName, CMD_SERVICE_ACTIVE, (int)1 );
}

void CAppDecode::on_enter_active()
{
    bool bVal = false;

    query( MSG_DECODE_ONOFF, bVal );
    if(bVal)
    {
        m_pGnd->setAllInactive(menu_res::RDsoGnd::mGndList);
        m_pZoomGnd->setActive();
        m_pGnd->setActive();
    }
}

void CAppDecode::onEventClose()
{
    serviceExecutor::post(mservName, MSG_DECODE_EVT, (int)0);
}

void CAppDecode::setViewMode(int num)
{
    serviceExecutor::post( mservName, MSG_DECODE_EVT_VIEW, num );
}

void CAppDecode::onDecodeEnable()
{
    bool bVal = false;

    query( MSG_DECODE_ONOFF, bVal );

    m_pGnd->setShow( bVal );    
    m_pBusWidget->setShow( bVal );


    m_pZoomGnd->setShow( bVal );
    m_pBusWidgetZoom->setShow( bVal );

    if(bVal)
    {
        m_pGnd->setAllInactive(menu_res::RDsoGnd::mGndList);
        m_pZoomGnd->setActive();
        m_pGnd->setActive();
    }
    else
    {

    }
}

int CAppDecode::onViewChanged(void)
{
    query(serv_name_dso, servDso::CMD_SCREEN_MODE, m_nScrMode );
    return ERR_NONE;
}

int CAppDecode::onDisplayClear()
{
    if( m_pBusWidget && m_pBusWidget->isVisible() )
    {
        CDecBus*p = m_pBusWidget->getBus();
        if( p )
        {
            p->clear();
            m_pBusWidget->update();
        }
        if(m_nScrMode == screen_yt_main_zoom)
        {
            p = m_pBusWidgetZoom->getBus();
            if( p )
            {
                p->clear();
                m_pBusWidgetZoom->update();
            }
        }
    }
    return ERR_NONE;
}

int CAppDecode::onEvtBusX()
{
    int busx = 0;
    serviceExecutor::query( mservName, MSG_DECODE_EVT_BUSX, busx );

    QString strName = "Decode";
    strName = strName.append(QString::number(busx+1));
    m_pEventTable->setBusX(strName);

    m_pEventTable->init();

    return ERR_NONE;
}

void CAppDecode::onDecodeOK()
{
    if( !m_pBusWidget->isVisible() )
    {
        return;
    }

    CDecBus* bus= NULL;

    void *ptr = NULL;
    //! get bus
    query( servDec::cmd_bus_data, ptr );
    bus = (CDecBus*)ptr;
    if( bus == NULL )
    {
        return;
    }

    m_pBusWidget->setBus( bus );

    m_pBusWidget->update();

    if(m_nScrMode == screen_yt_main_zoom)
    {
        //! get bus zoom
        query( servDec::cmd_bus_zoom_data, ptr );
        m_pBusWidgetZoom->setBus( (CDecBus *)ptr );
        m_pBusWidgetZoom->update();
    }

    if(m_bShowEvt)
    {
//        onEvtBusX();
        onSyncEvt();
    }

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void CAppDecode::onSyncEvt()
{
    //! get table
    void *ptr = NULL;
    query( servDec::cmd_bus_evt, ptr );
    CTable *pTable = (CTable*)ptr;
    if(pTable != NULL && pTable->getLayout()->size() > 0)
    {
        m_pEventTable->setTableValue(pTable);
    }
}

void CAppDecode::onDecodeLabel()
{
    bool b = false;
    query(MSG_DECODE_LABEL, b);
    if( b )
    {
        m_nLabelPos = LABEL_POS;
        onDecodePos();
        m_pBusWidget->update();
        if(m_nScrMode == screen_yt_main_zoom)
        {
            m_pBusWidgetZoom->update();
        }
    }
    else
    {
        m_nLabelPos = 0;
        onDecodePos();
    }
}

void CAppDecode::onFormat()
{
    if( m_pBusWidget != NULL && m_pBusWidget->isVisible() )
    {
        m_pBusWidget->update();
        if(m_nScrMode == screen_yt_main_zoom)
        {
            m_pBusWidgetZoom->update();
        }
    }
}

void CAppDecode::onDecodePos()
{
    int pos = 0;
    query( MSG_DECODE_POS, pos );

    m_pGnd->offset( pos );
    m_pBusWidget->offset( pos + m_nLabelPos);


    if(m_nScrMode == screen_yt_main_zoom)
    {
        m_pZoomGnd->offset( pos );
        m_pBusWidgetZoom->offset( pos + m_nLabelPos);
    }
}

void CAppDecode::onDecodeEvt()
{
    query( MSG_DECODE_EVT,  m_bShowEvt);

    if( m_bShowEvt )
    {
        if(m_pBusWidget->isVisible())
        {
            onSyncEvt();//added by hxh for bug1953
        }

        QString strName = "Decode"+QString::number(mDecoderId);

        m_pEventTable->setBusX(strName);
        m_pEventTable->show();
    }
    else if(m_pEventTable->isVisible())
    {
        m_pEventTable->hide();
    }
}

void CAppDecode::onViewMode()
{
    int viewMode = 0;
    query( mservName, MSG_DECODE_EVT_VIEW, viewMode );

    switch (viewMode)
    {
    case 0:
        m_pEventTable->packetsShow();
        break;
    case 1:
        m_pEventTable->detailsShow();
        break;
    case 2:
        m_pEventTable->payloadShow();
        break;
    default:
        break;
    }
}

int CAppDecode::onThreshold(int cause)
{
    if( m_pLine && cause == cause_value)
    {
        CArgument arg;
        query(mservName, servDec::cmd_bus_src_param,arg);
        if(arg.size() == 2)
        {
            qint64 thre = 0;
            int    s = 0;
            int    nScale = 0;

            arg.getVal(s,0);
            arg.getVal(thre,1);

            if( (chan1 <= s) && (s <=  chan4) )
            {
                float fRatio;
                int nOffset = 0;
                QString name = QString("chan%1").arg(s);
                serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);
                serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);
                serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);

                nScale = nScale * fRatio;
                nOffset= nOffset* fRatio;
                thre = thre + nOffset;
            }

            if( nScale != 0)
            {
                qint64 offset = thre * scr_vdiv_dots / nScale;
                m_pLine->setPos( offset );
                m_pLine->setColor(sysGetChColor((Chan)s));

                if( m_nScrMode == screen_yt_main_zoom)
                {
                    m_pLineZoom->setPos( offset );
                    m_pLineZoom->setColor(sysGetChColor((Chan)s));
                }
            }
        }
    }

    return ERR_NONE;
}

void CAppDecode::onEventFormat()
{
    if( m_bShowEvt )
    {
        int format = 0;
        query( MSG_DECODE_EVT_FORMAT, format );

        m_pEventTable->setCurrFormat((DecodeFormat)format);
    }
}

void CAppDecode::setJumpEvent()
{
    if(m_pBusWidget->isVisible())
    {
        if(m_pEventTable->isVisible() && m_pEventTable->isPacketIsNULL())
        {
            m_pEventTable->setPacketsJustTo();
        }
    }
}
