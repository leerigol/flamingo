#ifndef RBUSWIDGET_H
#define RBUSWIDGET_H

#include <QWidget>

#include "../../service/servdecode/servBlock/DecBus.h"
#include "../../menu/arch/rdsoui.h"

class RBusWidget : public QWidget, public menu_res::RDsoUIY
{
    Q_OBJECT
public:
    RBusWidget( QWidget *parent = 0 );

    void setBus( CDecBus *pBus );
    CDecBus * getBus();

protected:
    virtual void paintEvent( QPaintEvent *event );

protected:
    virtual void setWidgetVisible( bool b );
    virtual void refresh();

Q_SIGNALS:
    void sigPaintStarted();
    void sigPainCompleted();

private:
    CDecBus *m_pDecBus;
};

#endif // RBUSWIDGET_H
