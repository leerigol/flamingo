#include "tableWidget.h"
#include <QFont>
#include <QEvent>
#include <math.h>
#include <QHeaderView>
TableWidget::TableWidget(QWidget *parent) :
    QTableWidget(parent)
{
    boolMoved = false;
    boolFirst = true;
    value =  this->verticalScrollBar()->value();
    this->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

//    this->setIconSize(QSize(53,53));
    this->scrollToBottom();
    this->scrollToTop();

    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->setSelectionMode(QAbstractItemView::SingleSelection);
    this->setAlternatingRowColors(true);
    this->horizontalHeader()->setHighlightSections(false);
    this->horizontalHeader()->setMinimumSectionSize(0);
    this->horizontalHeader()->setDefaultSectionSize(30);

    this->verticalHeader()->setSectionsClickable(false);
    this->verticalHeader()->setVisible(false);
    this->verticalHeader()->setDefaultAlignment(Qt::AlignRight | Qt::AlignVCenter);
    setScrollBarStyle();
}

TableWidget::~TableWidget()
{

}

void TableWidget::setScrollBarStyle()
{
    this->verticalScrollBar()->setStyleSheet("QScrollBar:vertical\
    {\
        width:8px;\
        background:rgba(40,40,40,100%);\
        margin:0px,0px,0px,0px;\
        padding-top:5px;   \
        padding-bottom:5px;\
    }\
    QScrollBar::handle:vertical\
    {\
        width:8px;\
        background:rgba(100,100,100,50%);\
        border-radius:4px;  \
        min-height:10;\
    }\
    QScrollBar::handle:vertical:hover\
    {\
        width:8px;\
        background:rgba(100,100,100,25%);  \
        border-radius:4px;\
        min-height:10;\
    }\
    QScrollBar::add-line:vertical  \
    {\
        height:6px;width:8px;\
        border:0px;\
        subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical  \
    {\
        height:6px;width:8px;\
        border:0px;\
        subcontrol-position:top;\
    }\
    QScrollBar::add-line:vertical:hover \
    {\
        height:6px;width:8px;\
        border:0px;\
        subcontrol-position:bottom;\
    }\
    QScrollBar::sub-line:vertical:hover \
    {\
        height:9px;width:8px;\
        border:0px;\
        subcontrol-position:top;\
    }\
    QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical   \
    {\
        background:rgba(40,40,40,50%);\
        border-radius:4px;\
    }");
}

void TableWidget::mouseReleaseEvent(QMouseEvent *event )
{
    boolFirst = true;
    if(!boolMoved)
        QTableWidget::mouseReleaseEvent(event);
}

int TableWidget::getValue()
{
    value = verticalScrollBar()->value();
    return value;
}

void TableWidget::setValue(int val)
{
    value = val;
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + value);
}

void TableWidget::mouseMoveEvent(QMouseEvent *event )
{
    if(event->buttons() & Qt::LeftButton){
        int y = event->y();
        int numberSteps = (point.y()-y);
        if(numberSteps < 0){
            if(abs(numberSteps)/10 > 0 ){
                if(boolFirst){
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + numberSteps);
                    boolFirst = false;
                }else{
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + point2.y()-y);
                }
                boolMoved = true;
            }
        }else{
            if(abs(numberSteps)/10 > 0 ){
                if(boolFirst){
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + numberSteps);
                    boolFirst = false;
                }else{
                    this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() + point2.y()-y);
                }
                boolMoved = true;
            }
        }
        point2.setY(event->y());
    }
}

void TableWidget::mousePressEvent(QMouseEvent *event )
{
    if(event->buttons() & Qt::LeftButton){
        point.setY(event->y());
        boolMoved = false;
        event->accept();
    }

    QTableWidget::mousePressEvent(event);
}
