#ifndef EVENTWIDGET_H
#define EVENTWIDGET_H

#include <QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

#include "tableWidget.h"
#include "../../service/servdecode/eventTable/ctable.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../menu/wnd/uiwnd.h"

class EventWidget : public menu_res::uiWnd
{
    Q_OBJECT
public:
    explicit    EventWidget(QWidget *parent = 0);

    void        retranslateUi();
    void        buildConnection();
    void        init();

    void        setTableValue(CTable *table);
    void        setCurrFormat(DecodeFormat format);

    static menu_res::RInfoWnd_Style _style;

protected:
    void        loadResource( const QString &root );
    void        paintEvent( QPaintEvent *event );

    void        tuneInc(int keyCnt );
    void        tuneDec(int keyCnt );

private:
    void        creatItems();
    void        packetsTableSetting();
    void        detailsTableSetting();
    void        payloadTableSetting();

public:
    void        packetsTableSetValue();
    void        detailsTableSetValue();
    void        payloadTableSetValue();

    void        detailsDataChanged();
    void        tableClear();
    bool        isPacketIsNULL();

public slots:
    int         getPacketsChoosedNum();
    DsoErr      setPacketsJustTo();

    void        packetsShow();
    void        detailsShow();
    void        payloadShow();
    void        closeThis();

    void        setBusX(QString str);

signals:
    void        sigEventClose();
    void        sigViewMode(int );

public:
    CTable      *pDetailTable;

private:
    QPushButton *packetsBtn;
    QPushButton *detailsBtn;
    QPushButton *payloadBtn;
    QSpacerItem *hSpacer;
    QPushButton *closeBtn;
    QStackedWidget *stackedWidget;
    QWidget     *packetsPage;
    QWidget     *detailsPage;

    QWidget     *payloadPage;
    TableWidget *packetsTable;
    TableWidget *detailsTable;
    TableWidget *payloadTable;

    QLabel      *pLabel;

    CTable      *pTable;

    bool         mInited;

    QString      mTableStyle;
    QString      mCloseQss;


    QString     mTitle;
    int         mColumn;
    QList<int>  mColWidth;
    QStringList colTitle;
    int         mRow ;
    QList<int>  mDataCol;
    QStringList mDataSheet;

    DecodeFormat mCurrFormat;
    qlonglong   horiOffset;


};

#endif // EVENTWIDGET_H
