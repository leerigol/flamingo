#include "rbuswidget.h"

RBusWidget::RBusWidget( QWidget *parent ) : QWidget( parent )
{
    m_pDecBus = NULL;
}

void RBusWidget::setBus( CDecBus *pBus )
{
    m_pDecBus = pBus;
}

CDecBus * RBusWidget::getBus()
{
    return m_pDecBus;
}

void RBusWidget::paintEvent( QPaintEvent *event )
{
    Q_UNUSED(event);

    if ( m_pDecBus == NULL )
    {
        return;
    }

    QPainter painter(this);
    m_pDecBus->paint( painter );
}

void RBusWidget::setWidgetVisible( bool b )
{
    setVisible( b );

    refresh();
}
void RBusWidget::refresh()
{
    if ( !mbVisible ) return;

    updatePhy();

    int y = mPhyY - CDecBlock::_style.mBlockHeight/2;
    if( y < 0 )
    {
        y = 0;
    }
    QWidget::move( 0, y);

    //qDebug() << "refresh:" <<  mPhyY << y << m_phyRect << m_viewRect << this->geometry();
    if(m_pDecBus)
    {
        m_pDecBus->setYPos( y );
    }

}
