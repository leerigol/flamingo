#ifndef TABLEWIDGET_H
#define TABLEWIDGET_H
#include <QWidget>
#include <QTableWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QScrollBar>

class TableWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit TableWidget(QWidget *parent = 0);
    ~TableWidget();

    int getValue();
    void setValue(int val);

protected:
    void mouseMoveEvent(QMouseEvent * event);
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent *event );



    void setScrollBarStyle();
private:
    QPoint point,point2;
    bool boolMoved,boolFirst;
    int value;
    
};

#endif // TABLEWIDGET_H
