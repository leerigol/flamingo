#ifndef CSELECTLINE
#define CSELECTLINE

#include <QWidget>
#include <QColor>
#include <QTimer>
#include "../../menu/arch/rdsoui.h"

class CSelectLine : public QWidget,
                    public menu_res::RDsoUI
{
    Q_OBJECT

public:
    explicit CSelectLine(QWidget *parent = 0);

public:
    void setPos( int pos );
    void setColor( QColor color );
    void setRange(int start, int range);
    void setupUi(void);

protected:
    void paintEvent( QPaintEvent *event );
    void viewPos( int posView );
    void setWidgetVisible( bool b );
    void refresh();

private Q_SLOTS:
    void      onTimeout(void);
private:
    int       mPos;
    int       m_nStart; /* More than one GRID. level drawing start with*/
    int       m_nRange; /* Save as up. level drawing range */
    QColor    m_nColor;
    QTimer*   m_pTimer;
};

#endif // CSELECTLINE

