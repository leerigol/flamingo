#include "capphisto.h"
#include "../appdisplay/appdisplay.h"
//! msg table
IMPLEMENT_CMD( CExecutor, cappHisto )
start_of_entry()
on_set_void_void(MSG_HISTO_EN,       &cappHisto::setShow),
on_set_void_void(MSG_HISTO_SOURCE,   &cappHisto::setShow),
on_set_void_void(MSG_HISTO_TYPE,     &cappHisto::setShow),
on_set_void_void(MSG_HISTO_STATISEN, &cappHisto::setStatisEn),

on_set_void_void(MSG_HISTO_HIGHPOS,  &cappHisto::setRange),
on_set_void_void(MSG_HISTO_LOWPOS,   &cappHisto::setRange),
on_set_void_void(MSG_HISTO_LEFTPOS,  &cappHisto::setRange),
on_set_void_void(MSG_HISTO_RIGHTPOS, &cappHisto::setRange),

on_set_void_void(servHisto::cmd_histoRes_update, &cappHisto::updateRes),
on_set_void_void(servHisto::cmd_histo_updata, &cappHisto::updateHistos),
on_set_void_void(servHisto::cmd_meas_clean,   &cappHisto::setShow),
on_set_int_void(servHisto::cmd_display_clean,&cappHisto::setClean),
on_set_int_void(servHisto::cmd_meas_item_change,&cappHisto::setShow),
on_set_void_void(servHisto::cmd_work_clean,&cappHisto::ResetRes),
end_of_entry()

cappHisto::cappHisto()
{
    mservName = serv_name_histo;

//#ifdef FLAMINGO_TR5
    addQuick( MSG_HISTO,
              QString("ui/desktop/histo/icon/") );
//#endif

}

void cappHisto::setupUi(CAppMain *pMain)
{
    LOG_DBG();
    phistoWnd = new CHistoWnd(pMain->getTouchZoneBar(), pMain->getCentBar());
    Q_ASSERT(NULL != phistoWnd);
    phistoWnd->hide();

    pResWnd = new HistoResWnd(pMain->getCentBar());
    Q_ASSERT(NULL != pResWnd);
    pResWnd->hide();
    LOG_DBG();

    Q_ASSERT( phistoWnd != NULL );
    connect( phistoWnd, SIGNAL( sig_serv_active(void) ),
             this, SLOT( on_serv_active(void) ) );
}

void cappHisto::retranslateUi()
{

}

void cappHisto::buildConnection()
{
}

void cappHisto::resize()
{

}

void cappHisto::setClean()
{
    phistoWnd->emptyHistos();
}
void cappHisto::setShow()
{
    bool enHisto;
    query(MSG_HISTO_EN,enHisto);

    int type;
    query(MSG_HISTO_TYPE, type);
    if(enHisto)
    {
        if((HistoType)type == histoMeas)
        {
            phistoWnd->emptyHistos();
            phistoWnd->setRangeVis(false);   
        }
        else
        {
            phistoWnd->setRangeVis(true);
        }
        setRange();
        phistoWnd->show();
        //phistoWnd->raise();
        setStatisEn();
    }
    else
    {
        phistoWnd->hide();
        phistoWnd->clearHistos();

        pResWnd->hide();
    }

#if 1
    int ch;
    query(MSG_HISTO_SOURCE, ch);
    if(!ch)
    {
       phistoWnd->emptyHistos();
    }
#endif
}

void cappHisto::setRange()
{
    int left,right,top,low;
    query(MSG_HISTO_LEFTPOS,left);
    query(MSG_HISTO_RIGHTPOS,right);
    query(MSG_HISTO_HIGHPOS,top);
    query(MSG_HISTO_LOWPOS,low);
    phistoWnd->changeRangeRect(left,right,top,low);
}

void cappHisto::setStatisEn()
{
    bool statisEn;
    query(MSG_HISTO_STATISEN, statisEn);
    if(statisEn)
    {
        pResWnd->show();
    }
    else
    {
        pResWnd->hide();
    }
}

void cappHisto::updateHistos()
{
    if( phistoWnd != NULL )
    {
        int type;
        query(MSG_HISTO_TYPE, type);
        int ch;
        query(MSG_HISTO_SOURCE, ch);
        void* ptr;
        serviceExecutor::query( serv_name_measure,
                                servMeasure::cmd_get_histo_meas, ptr);
        QQueue<MeasValue>* pMeasQue = static_cast<QQueue<MeasValue>* > (ptr);

        //int iStat;
        //query(serv_name_hori,servHori::cmd_control_status,iStat);

        if((HistoType)type == histoMeas)
        {
            ch = chan_none;
        }
        if( ch ||
            (
                (ptr != NULL)  &&
                ((HistoType)type == histoMeas) &&
                (pMeasQue->at(0).mVal)
             )
          )
        {
           void* ptr;
           query(servHisto::cmd_histo_updata,ptr);
           QVector<int>* pHisto = static_cast<QVector<int>* > (ptr);
           if(pHisto->isEmpty() == false)
           {
                phistoWnd->setHistos(pHisto);

                //bug2627. by hexiaohua
                if( sysGetStarted() )
                {
                    QCoreApplication::processEvents();
                }
           }
           else
           {
               LOG_DBG()<<" appHisto:the histPix is empty";
           }
           LOG_DBG();
        }
    }
}

void cappHisto::updateRes()
{
    void* ptr;
    query(servHisto::cmd_histoRes_update,ptr);
    HistoStatisRes*pRes = static_cast<HistoStatisRes* > (ptr);

    QString xorigin;
    gui_fmt::CGuiFormatter::format( xorigin, pRes->centVal,
                                    fmt_def, pRes->getUnit());
    QString xscale;
    gui_fmt::CGuiFormatter::format(xscale, pRes->BinWidth * 100,
                                    fmt_def, pRes->getUnit());

    phistoWnd->m_Origin = xorigin + "  " + xscale;

    pResWnd->upDateValue(pRes);
    LOG_DBG();
}

void cappHisto::ResetRes()
{
    pResWnd->ResetValue();
    LOG_DBG();
}

void cappHisto::on_serv_active()
{
    int left,right,top,low;
    int value = 0;
    phistoWnd->getRangeRect(left,right,top,low);
    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<left<<right<<low<<top;

    value = /*left*/qMin(left, right);
    //qDebug()<<value;
    serviceExecutor::post(serv_name_histo, MSG_HISTO_LEFTPOS,  value );

    value = /*right*/qMax(left, right);
    //qDebug()<<value;
    serviceExecutor::post(serv_name_histo, MSG_HISTO_RIGHTPOS, value );

    value = /*top*/qMin(low, top);
    //qDebug()<<value;
    serviceExecutor::post(serv_name_histo, MSG_HISTO_HIGHPOS,  value );

    value = /*low*/qMax(low, top);
    //qDebug()<<value;
    serviceExecutor::post(serv_name_histo, MSG_HISTO_LOWPOS,   value  );

    serviceExecutor::post(serv_name_histo, CMD_SERVICE_ACTIVE, 1);
    serviceExecutor::post(serv_name_histo, MSG_HISTO_EN,       true);
}
