#ifndef HISTORESWND_H
#define HISTORESWND_H
#include <QWidget>
#include <QVector>
#include <QString>

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"

#include "../../menu/wnd/rmodalwnd.h"
#include "../../menu/menustyle/rui_style.h"
#include "../../menu/wnd/uiwnd.h"
class resWndStyle : public menu_res ::RUI_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );
public:
    QPoint leftTopPt;
    int widthName;
    int widthValue;
    int heightItem;
    QRect rectHistoResWnd;
    QRect rectTable;

    QString style;
};

//class HistoResWnd : public menu_res ::uiWnd
class HistoResWnd : public menu_res::RModalWnd
{
    Q_OBJECT
public:
    explicit HistoResWnd(QWidget *parent = 0);

    void setupHistoUi();
    void initItemName();
    void upDateValue(HistoStatisRes* res);
    void ResetValue();
    void paintEvent(QPaintEvent *event);
private:
    QTableWidget *m_pResTable;
    resWndStyle   _style;
    QVector<QString> m_ItemName;
    QVector<QString> m_ItemValue;
};

#endif // HISTORESWND_H
