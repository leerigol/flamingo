#ifndef HISTOWND_H
#define HISTOWND_H

#include <QWidget>
#include <QPainter>
#include "../../include/dsocfg.h"
#include "../appmain/touchzone.h"

class CHistoWnd : public QWidget
{
    Q_OBJECT
public:
    explicit CHistoWnd(TouchZone *touch, QWidget *parent = 0);
    void setHistos(QVector<int>* histos);
    void emptyHistos();
    void clearHistos();
    void setRangeVis(bool isVisible);


public slots:
    void paintEvent(QPaintEvent*);
    void changeRangeRect(int left,int right,int top,int low);
    void getRangeRect(int &left,int &right,int &top,int &low);

public:
     QString m_Origin;
private:
    TouchZone     *m_pTouchZone;
    bool           m_RangeVisible;
    QVector<int>   m_histos;
    QRect          m_rangeRect;

    //原点坐标与scale
    QLabel  *m_pLabel;

protected Q_SLOTS:
    void  on_zone_selcet(int index);
    void  on_mouse_move_event(QPoint );
    void  on_mouse_pressEvent(QPoint );
    void  on_mouse_releaseEvent(QPoint );

Q_SIGNALS:
    void  sig_serv_active(void);

};

#endif // HISTOWND_H
