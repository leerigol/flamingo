#ifndef CAPPHISTO_H
#define CAPPHISTO_H

#include <QtCore>
#include <QtWidgets>
#include "histownd.h"
#include "historeswnd.h"

#include "../capp.h"
#include "../appmain/cappmain.h"

#include "../../menu/rmenus.h"
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"

class cappHisto : public CApp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    cappHisto();
    virtual void setupUi(CAppMain *pMain);

    virtual void retranslateUi();
    virtual void buildConnection();
    virtual void resize();

    void setClean();
    void setShow();
    void setRange();
    void setStatisEn();

    void updateHistos();
    void updateRes();
    void ResetRes();

private:
    HistoResWnd* pResWnd;
    CHistoWnd*   phistoWnd;

protected Q_SLOTS:
    void  on_serv_active(void);

};

#endif // CAPPHISTO_H
