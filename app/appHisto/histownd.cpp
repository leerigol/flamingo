#include "../../include/dsodbg.h"
#include "histownd.h"

CHistoWnd::CHistoWnd(TouchZone *touch, QWidget *parent) : QWidget(parent)
{
    //setGeometry(24,55,1000,480);// 以网格为坐标系
    setGeometry(0,0,1000,480); //! 以主窗口为坐标系
    Q_ASSERT(touch != NULL);
    m_pTouchZone = touch;
    m_pTouchZone->setMenuVisible(ZONE_MENU_HISTO, true);

    connect( m_pTouchZone, SIGNAL( sig_mouse_move_event(QPoint) ),
             this, SLOT( on_mouse_move_event(QPoint) ) );

    connect( m_pTouchZone, SIGNAL( sig_mouse_pressEvent(QPoint ) ),
             this, SLOT( on_mouse_pressEvent(QPoint) ) );

    connect( m_pTouchZone, SIGNAL( sig_mouse_releaseEvent(QPoint) ),
             this, SLOT( on_mouse_releaseEvent(QPoint ) ) );

    connect( m_pTouchZone, SIGNAL( sig_menu_activated(int) ),
             this, SLOT( on_zone_selcet(int) ) );

    m_pLabel = new QLabel(this);
    Q_ASSERT(m_pLabel != NULL);


    m_pLabel->setGeometry(QRect(0, 0, 150, 30)); //设置大小和位置
    m_pLabel->setStyleSheet("color: white");

}

void CHistoWnd::paintEvent(QPaintEvent *)
{
    QPainter painters(this);
    painters.fillRect(rect(), Qt::transparent);//QColor(0xC8,0x98,0x60)
    painters.setPen(Qt::SolidLine);
    painters.setPen(Qt::white);
    //black
    QPen pen = painters.pen();

    if(m_RangeVisible)
    {
        pen.setWidth(1);
        painters.setPen(pen);
        painters.drawPolygon(m_rangeRect);
    }

    if(m_histos.size() == 0)
    {
        return;
    }

    pen.setWidth(1);
    pen.setColor(QColor(248,170,89,255));
    painters.setPen(pen);
    //! hori type
    if(m_histos.size() == 480)
    {
        //! vert type
        for(int i = 0; i < wave_height; i++)
        {
            if(m_histos[i] > 0)
            {
                painters.drawLine(0, 480-i, m_histos[i], 480-i);
            }
        }
    }
    else
    {
        //! hori type
        for(int i = 0; i < wave_width;i++)
        {
            if(m_histos[i] > 0)
            {
                painters.drawLine(i,wave_height,i,wave_height - m_histos[i]);
            }
        }
    }

#if 0
     m_pLabel->setText(m_Origin);
#endif
}

void CHistoWnd::changeRangeRect(int left,int right,int top,int low)
{
    m_rangeRect.setRect(left,top,right-left,low-top);
    update();
}

void CHistoWnd::getRangeRect(int &left, int &right, int &top, int &low)
{
    m_rangeRect.getCoords(&left, &top, &right, &low);
}

void CHistoWnd::on_zone_selcet(int index)
{
    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__<<index<<ZONE_MENU_HISTO;
    if(ZONE_MENU_HISTO == index)
    {
        m_pTouchZone->rectHide();

        const QRect &rect = m_pTouchZone->getZone();

        m_rangeRect.setCoords(rect.topLeft().x(),
                              rect.topLeft().y(),
                              rect.bottomRight().x(),
                              rect.bottomRight().y()
                              );
        update();
        emit sig_serv_active();
    }
}

void CHistoWnd::on_mouse_move_event(QPoint /*event*/)
{
    m_pTouchZone->show();
}

void CHistoWnd::on_mouse_pressEvent(QPoint /*event*/)
{
}

void CHistoWnd::on_mouse_releaseEvent(QPoint /*event*/)
{
}

void CHistoWnd::setHistos(QVector<int> *histos)
{
    LOG_DBG();
    m_histos.resize(histos->size());
//    m_histos = *histos;
    for(int i = 0;i < m_histos.size();i++)
    {
        m_histos[i] = histos->at(i);
    }

    LOG_DBG();
    update();
}

void CHistoWnd::emptyHistos()
{
    LOG_DBG();
  //  m_histos.resize(histos->size());
//    m_histos = *histos;
    for(int i = 0;i < m_histos.size();i++)
    {
        m_histos[i] = 0;
    }

    LOG_DBG();
    update();
}


void CHistoWnd::clearHistos()
{
    m_histos.clear();
}

void CHistoWnd::setRangeVis(bool isVisible)
{
    m_RangeVisible = isVisible;


    //m_pTouchZone->setMenuVisible(ZONE_MENU_HISTO, isVisible);
}
