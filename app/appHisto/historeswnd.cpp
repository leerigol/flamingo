#include "historeswnd.h"

void resWndStyle::load(const QString &path, const r_meta::CMeta &meta)
{
    meta.getMetaVal(path + "ptTop", leftTopPt);
    meta.getMetaVal(path + "wName", widthName);
    meta.getMetaVal(path + "wValue", widthValue);
    meta.getMetaVal(path + "hei", heightItem);
    meta.getMetaVal(path + "wnd", rectHistoResWnd);
    meta.getMetaVal(path + "tableWnd", rectTable);

    //meta.getMetaVal( path + "style/wnd", style);
}

HistoResWnd::HistoResWnd(QWidget *parent) : RModalWnd(parent)
{
    _style.loadResource("histo/");
    move(_style.leftTopPt);
    m_ItemName.resize(HISTOITEM_END + 1);
    m_ItemValue.resize(HISTOITEM_END + 1);
    resize(_style.widthName + _style.widthValue, (int) HISTOITEM_END * _style.heightItem);
    set_attr( wndAttr, mWndAttr, wnd_moveable );
    m_pResTable = NULL;
    setupHistoUi();
#if 1
     _style.style += "QTableWidget{background-color: rgb(28,39,49);outline: none;alternate-background-color: rgb(35,35,35);";
     _style.style += "color: rgb(255, 255, 255); border: 0px; gridline-color: rgb(50, 50, 50);}";
     this->setStyleSheet(_style.style);
 #endif
}

void HistoResWnd::setupHistoUi()
{
    m_pResTable = new QTableWidget(this);
    Q_ASSERT( m_pResTable );

    this->setGeometry(_style.rectHistoResWnd);
    m_pResTable->setGeometry(_style.rectTable);

    m_pResTable->setColumnCount(2);
    m_pResTable->setColumnWidth(0, _style.widthName);
    m_pResTable->setColumnWidth(1, _style.widthValue);

    m_pResTable->setRowCount((int) HISTOITEM_END + 1);
    for(int i = 0;i < (int) HISTOITEM_END + 1;i++)
    {
        m_pResTable->setItem(i, 0, new QTableWidgetItem(""));
        m_pResTable->setItem(i, 1, new QTableWidgetItem(""));
        m_pResTable->setRowHeight(i, _style.heightItem);
    }

    this->resize(width(),m_pResTable->geometry().top() +
                 _style.heightItem * ((int)HISTOITEM_END + 1));

    m_pResTable->setEditTriggers(QAbstractItemView::NoEditTriggers);   //设置不可编辑
    m_pResTable->setSelectionBehavior(QAbstractItemView::SelectRows);  //!选中模式, 行为单位
#if 0
     m_pResTable->setSelectionMode(QAbstractItemView::SingleSelection); //!只能选中单行
 #else
    m_pResTable->setSelectionMode(QAbstractItemView::NoSelection); //!不能选中
#endif
    m_pResTable->setFocusPolicy(Qt::NoFocus);                          //!去除选中虚线框
    m_pResTable->horizontalHeader()->setVisible(false);                //!不显示列表头
    m_pResTable->verticalHeader()->setVisible(false);                  //!不显示行表头
    m_pResTable->horizontalScrollBar()->setVisible(false);             //!不显示水平滚动条
    m_pResTable->horizontalScrollBar()->setEnabled(false);
    m_pResTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pResTable->verticalScrollBar()->setVisible(false);               //!不显示垂直滚动条
    m_pResTable->verticalScrollBar()->setEnabled(false);
    m_pResTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

   // histogram
   // setTitle(QString("histo result"));
    setTitle(QString("Histogram Result"));
    initItemName();
    setCloseEnable(false);
    setCloseVisible(false);
}

void HistoResWnd::initItemName()
{
    m_ItemName[SUM_HITS] = "Sum";
    m_ItemName[PEAK_HITS] = "Peaks";
    m_ItemName[MAX_VAL] = "Max";
    m_ItemName[MIN_VAL] = "Min";
    m_ItemName[MAXMIN_VAL] = "Pk_Pk";
    m_ItemName[MEAN_VAL] = "Mean";
    m_ItemName[MEDIAN_VAL] = "Median";
    m_ItemName[MODE_VAL] = "Mode";
    m_ItemName[BINWIDTH_VAL] = "Bin width";
    m_ItemName[SIGMA_VAL] = "Sigma";
    m_ItemName[XSCALE_VAL] = "XScale";
    //m_ItemName[YSCALE_VAL] = "yScale";
}

void HistoResWnd::upDateValue(HistoStatisRes *res)
{
    LOG_DBG();
    gui_fmt::CGuiFormatter::format( m_ItemValue[SUM_HITS], res->sumHits,
                                    fmt_def,Unit_none);
    m_ItemValue[SUM_HITS] = m_ItemValue[SUM_HITS] + "hits";
    gui_fmt::CGuiFormatter::format( m_ItemValue[PEAK_HITS], res->peakHits,
                                    fmt_def,Unit_none);
    m_ItemValue[PEAK_HITS] = m_ItemValue[PEAK_HITS] + "hits";

    if(!res->Valid)
    {
        for(int i = (int) MAX_VAL;i < (int) SIGMA_VAL;i++)
        {
            m_ItemValue[i] == QString("****");
        }
        update();
        return;
    }
    Unit unit = res->getUnit();
    if(unit == Unit_percent)
    {
        res->maxVal *= 100;
        res->minVal *= 100;
        res->pKpK *= 100;
        res->mean *= 100;
        res->median *= 100;
        res->mode *= 100;
        res->BinWidth *= 100;
        res->sigma *= 100;
        res->xScale *= 100;

        m_ItemValue[MAX_VAL] = QString("%1%").arg(res->maxVal,0,'f',2);
        m_ItemValue[MIN_VAL] = QString("%1%").arg(res->minVal,0,'f',3);
        m_ItemValue[MAXMIN_VAL] = QString("%1%").arg(res->pKpK,0,'f',3);
        m_ItemValue[MEAN_VAL] = QString("%1%").arg(res->mean,0,'f',2);
        m_ItemValue[MEDIAN_VAL] = QString("%1%").arg(res->median,0,'f',2);
        m_ItemValue[MODE_VAL] = QString("%1%").arg(res->mode,0,'f',2);
        m_ItemValue[BINWIDTH_VAL] = QString("%1%").arg(res->BinWidth,0,'f',3);
        m_ItemValue[SIGMA_VAL] = QString("%1%").arg(res->sigma,0,'f',2);
        m_ItemValue[XSCALE_VAL] = QString("%1%").arg(res->xScale,0,'f',2);
    }
    else
    {
        if(unit == Unit_db)
        {
            unit = Unit_none;
        }
        gui_fmt::CGuiFormatter::format( m_ItemValue[MAX_VAL], res->maxVal,
                                        fmt_def, unit);
        gui_fmt::CGuiFormatter::format( m_ItemValue[MIN_VAL], res->minVal,
                                        fmt_def, unit);
        gui_fmt::CGuiFormatter::format( m_ItemValue[MAXMIN_VAL], res->pKpK,
                                        fmt_def, unit);
        if(!qIsNaN(res->mean))
        {
            gui_fmt::CGuiFormatter::format( m_ItemValue[MEAN_VAL], res->mean,
                                            fmt_def, unit);
        }
        gui_fmt::CGuiFormatter::format( m_ItemValue[MEDIAN_VAL], res->median,
                                        fmt_def, unit);
        gui_fmt::CGuiFormatter::format( m_ItemValue[MODE_VAL], res->mode,
                                        fmt_def, unit);
        gui_fmt::CGuiFormatter::format( m_ItemValue[BINWIDTH_VAL], res->BinWidth,
                                        fmt_def, unit);
        if(!qIsNaN(res->sigma))
        {
            gui_fmt::CGuiFormatter::format( m_ItemValue[SIGMA_VAL], res->sigma,
                                            fmt_def, unit);
        }
        gui_fmt::CGuiFormatter::format( m_ItemValue[XSCALE_VAL], res->xScale,
                                        fmt_def, unit);
    }

    update();
}

void HistoResWnd::ResetValue()
{
    LOG_DBG();
    gui_fmt::CGuiFormatter::format( m_ItemValue[SUM_HITS], 0,
                                    fmt_def,Unit_none);
    m_ItemValue[SUM_HITS] = m_ItemValue[SUM_HITS] + "hits";
    gui_fmt::CGuiFormatter::format( m_ItemValue[PEAK_HITS], 0,
                                    fmt_def,Unit_none);
    m_ItemValue[PEAK_HITS] = m_ItemValue[PEAK_HITS] + "hits";


    Unit unit = Unit_none;
    gui_fmt::CGuiFormatter::format( m_ItemValue[MAX_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[MIN_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[MAXMIN_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[MEAN_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[MEDIAN_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[MODE_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[BINWIDTH_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[SIGMA_VAL], 0,
                                    fmt_def, unit);
    gui_fmt::CGuiFormatter::format( m_ItemValue[XSCALE_VAL],0,
                                    fmt_def, unit);
    update();
}


void HistoResWnd::paintEvent(QPaintEvent *event)
{
    int type;
    serviceExecutor::query(QString(serv_name_histo),MSG_HISTO_TYPE, type);
    int resSize;
    if((HistoType) type == histoMeas)
    {
        resSize = (int) HISTOITEM_END + 1;
    }
    else
    {
        resSize = (int) HISTOITEM_END;
    }

    for(int i = 0; i < resSize;i ++)
    {
        m_pResTable->item(i, 0)->setText(m_ItemName[i]);
        m_pResTable->item(i, 1)->setText(m_ItemValue[i]);
    }

#if 1
    this->resize(width(),m_pResTable->geometry().top() +
                 _style.heightItem * resSize + 8);
    m_pResTable->resize(m_pResTable->width(), _style.heightItem * resSize);
#endif

    RModalWnd::paintEvent(event);
//    LOG_DBG();
//    QPainter painter(this);
//    painter.fillRect(rect(),QColor(0,0,0,255));
//    QPen     pen;
//    pen.setColor(QColor(200,200,200,255));
//    painter.setPen(pen);

//    int x = 0;
//    int y = 0;
//    QRect rectNameItem(x,y,_style.widthName,_style.heightItem);
//    QRect rectValueItem(x + _style.widthName,y,_style.widthValue,_style.heightItem);


//    for(int i = 0; i < resSize;i++)
//    {
//        rectNameItem.moveTop(y + i * _style.heightItem);
//        painter.drawText(rectNameItem, Qt::AlignCenter,m_ItemName[i]);

//        rectValueItem.moveTop(y + i * _style.heightItem);
//        painter.drawText(rectValueItem, Qt::AlignCenter,m_ItemValue[i]);
//    }
}
