#-------------------------------------------------
#
# Project created by QtCreator 2016-11-17T10:49:42
#
#-------------------------------------------------
QT       += core
QT       += gui
QT       += widgets
QT       += network
QT       += xml

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/apps/apphisto
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

#DEFINES += _DEBUG

HEADERS += \
    capphisto.h \
    histownd.h \
    historeswnd.h

SOURCES += \
    capphisto.cpp \
    histownd.cpp \
    historeswnd.cpp

