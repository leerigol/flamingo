#ifndef APPQUICK_H
#define APPQUICK_H

#include "../capp.h"

class CAppQuick : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

public:
        CAppQuick();
};


#endif // APPQUICK_H
