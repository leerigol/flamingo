QT       += core
QT       += gui
QT       += widgets

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/apps/appquick
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


SOURCES += \
    appquick.cpp

HEADERS += \
    appquick.h

