#ifndef CMEASITEM_H
#define CMEASITEM_H

#include "../../include/dsostd.h"
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"

class CMeasItem
{
public:
    CMeasItem(MeasType type = Meas_Freq);
    void loadResource( const QString &path);

    void setSrcA(Chan a);
    void setSrcB(Chan b);

    bool isDsrcType();
    bool isLaMeasType();

    void setIDS(int ids);
    void getIDSName();

private:
    QColor setSrcColor(Chan src);

private:
    static QMap<Chan,QString> srcName;
    static QMap<Chan, QString> InitSrcNameMap();

public:
    MeasType mType;
    Chan     mSrcA;
    Chan     mSrcB;

    QString mName;
    QString mSymbol;
    QString mSrcAStr;
    QString mSrcAEdgeIcon;
    QString mSrcBStr;
    QString mSrcBEdgeIcon;
    QString mBracketL;
    QString mConnector;
    QString mBracketR;
    QString mHelpPic;

    QColor mSrcAColor;
    QColor mSrcBColor;
    QColor mSymbolColor;

private:
    int    m_nIDS;
};

#endif // CMEASITEM_H
