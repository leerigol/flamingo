#ifndef MEASITEMBOX_H
#define MEASITEMBOX_H

#include "measButton.h"
#include "measStatValueBox.h"

#include "../../menu/rmenus.h"
#include "measitemlabel.h"
#include "../apphelp/iapphelp.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../menu/wnd/uiwnd.h"

using namespace menu_res;
class CMeasItemBox : public uiWnd
{
    Q_OBJECT

public:
    enum FocusType
    {
        HORFOCUS,
        VERFOCUS,
        DSRCFOCUS,
        ALYFOCUS,
        MEASBUTTONFOCUS,
        CLOSEFOCUS,
    };

    CMeasItemBox(QWidget *parent = 0);
    ~CMeasItemBox();

    static menu_res::RInfoWnd_Style _style;

    void setupUi(QWidget *parent);
    void retranslateUi();

     void rst();

    void setChoosed(bool b);

    void setUPAShow(bool);

public:
    void setSrcA(Chan src);
    void setSrcB(Chan src);

    CMeasItem *findItem(MeasType type);

    void setServName(QString str);
    void setMeasHelpList(QList<HelpItem *> list);
signals:
    void clickItem(CMeasItem *);

    void raiseItemBox();

public slots:
    void setHorType();
    void setVerType();
    void setDsrcType();
    void upCurrFocus(QList<CMeasItemButton*> itemList);

    void setAlyType();
    void itemClickedSlot(CMeasItemButton *itemButton);
    void showCounterA();
    void showCounterB();
    void showDvm();
    void showUPA();

protected:
    void loadResource(const QString&);
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent( QMouseEvent *event);
    void mousePressEvent( QMouseEvent *event);

    void tuneInc(int keyCnt );
    void tuneDec(int keyCnt );
    void tuneZ( int keyCnt );

    void measButtonTuneInc(QList<CMeasItemButton *> &measButtonList);
    void measButtonTuneDec(QList<CMeasItemButton *> &measButtonList);

public:
    QList<HelpItem *>   measHelpList;
    QString             measHelpPic;

private:
    QList<CMeasItemButton*>    m_ItemButtonHor;
    QList<CMeasItemButton*>    m_ItemButtonVer;
    QList<CMeasItemButton*>    m_ItemButtonDsrc;
    QList<QPushButton*>        m_ItemButtonAly;

    QPushButton     *mHorButton;
    QPushButton     *mVerButton;
    QPushButton     *mDsrcButton;
    QPushButton     *mAlyButton;
    QPushButton     *mCloseButton;
    QStackedWidget  *mStackedWidget;

    QPushButton     *mCounterABtn;
    QPushButton     *mCounterBBtn;
    QPushButton     *mDvmBtn;
    QPushButton     *mPowerBtn;
    //QPushButton     *mEyeBtn;

    MeasType  m_MeasType;
    CMeasItemButton *m_CurrItemButton;
    bool mAlyHasFocus;

private:
    int m_nCurrItemNum;
    int m_nItemCount;

    FocusType  currFocus;
    int m_BoxX, m_BoxY;
    int m_BoxW, m_BoxH;

    int m_ItemStartX;
    int m_ItemStartY;
    int m_ItemW;
    int m_ItemH;

    bool    m_bShowHelpImage;

    QString m_HorBack;
    QString m_VerBack;
    QString m_DsrcBack;
    QString servName;
    QString m_measHelpHor;

    QString m_typeSelect;
    QString m_typeNormal;
    QString m_typeClose;

};

#endif // MEASITEMBOX_H

