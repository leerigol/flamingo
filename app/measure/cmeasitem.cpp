#include "cmeasitem.h"
#include "../../meta/crmeta.h"

QMap<Chan,QString> CMeasItem::srcName = CMeasItem::InitSrcNameMap();

QMap<Chan, QString> CMeasItem::InitSrcNameMap()
{
    QMap<Chan,QString> name;
    name[chan_none] = "";
    name[chan1]     = "1";
    name[chan2]     = "2";
    name[chan3]     = "3";
    name[chan4]     = "4";

    name[d0]        = "0";
    name[d1]        = "1";
    name[d2]        = "2";
    name[d3]        = "3";
    name[d4]        = "4";
    name[d5]        = "5";
    name[d6]        = "6";
    name[d7]        = "7";
    name[d8]        = "8";
    name[d9]        = "9";
    name[d10]       = "10";
    name[d11]       = "11";
    name[d12]       = "12";
    name[d13]       = "13";
    name[d14]       = "14";
    name[d15]       = "15";

    name[r1]        = "1";
    name[r2]        = "2";
    name[r3]        = "3";
    name[r4]        = "4";
    name[r5]        = "5";
    name[r6]        = "6";
    name[r7]        = "7";
    name[r8]        = "8";
    name[r9]        = "9";
    name[r10]       = "10";

    name[m1]        = "1";
    name[m2]        = "2";
    name[m3]        = "3";
    name[m4]        = "4";
    return name;
}

CMeasItem::CMeasItem(MeasType type)
{
    mType = type;

    if((mType >= Meas_DOUBLE_SRC_TYPE) && (mType < Meas_DOUBLE_SRC_TYPE_END))
    {
        setSrcA(chan1);
        setSrcB(chan2);
    }
    else
    {
        setSrcA(chan1);
        setSrcB(chan_none);
    }

    mBracketL  = "(";
    mConnector = "-";
    mBracketR  = ")";

    mSymbolColor = QColor(0xc0,0xc0,0xc0);

    loadResource("measure/");
    m_nIDS = 0;
}

void CMeasItem::loadResource( const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path;
    QString index = "";

    str += "item_";
    int i = (int)mType;
    index = str + QString("%1").arg(i, 2, 10, QChar('0'));
    meta.getMetaVal( index+"/name",   mName);
    meta.getMetaVal( index+"/symbol", mSymbol);
    meta.getMetaVal( index+"/icon",    mHelpPic);

    str = path;
    QString riseEdge = "";
    QString fallEdge = "";
    meta.getMetaVal( str+"rise_edge_icon", riseEdge);
    meta.getMetaVal( str+"fall_edge_icon", fallEdge);

    switch (mType)
    {
    case Meas_DelayRR: case Meas_PhaseRR:
        mSrcAEdgeIcon = riseEdge;
        mSrcBEdgeIcon = riseEdge;
        break;

    case Meas_DelayRF: case Meas_PhaseRF:
        mSrcAEdgeIcon = riseEdge;
        mSrcBEdgeIcon = fallEdge;
        break;

    case Meas_DelayFR: case Meas_PhaseFR:
        mSrcAEdgeIcon = fallEdge;
        mSrcBEdgeIcon = riseEdge;
        break;

    case Meas_DelayFF: case Meas_PhaseFF:
        mSrcAEdgeIcon = fallEdge;
        mSrcBEdgeIcon = fallEdge;
        break;
    default:
        mSrcAEdgeIcon = "";
        mSrcBEdgeIcon = "";
        break;
    }
}

void CMeasItem::setSrcA(Chan a)
{
    mSrcA = a;
    mSrcAStr = srcName.value(mSrcA);
    mSrcAColor = setSrcColor(mSrcA);
}

void CMeasItem::setSrcB(Chan b)
{
    mSrcB = b;
    mSrcBStr = srcName.value(mSrcB);
    mSrcBColor = setSrcColor(mSrcB);
}

bool CMeasItem::isDsrcType()
{
    if(  ((mType >= Meas_DOUBLE_SRC_TYPE) && (mType < Meas_DOUBLE_SRC_TYPE_END))
       ||(mType == UPA_Apparent_P)
       ||(mType == UPA_Reactive_P)
       ||(mType == UPA_P_Factor)
       ||(mType == UPA_Phase_Angle)
       ||(mType == UPA_Imp)  )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CMeasItem::isLaMeasType()
{
    if(  (mType == Meas_Period)
       ||(mType == Meas_Freq)
       ||(mType == Meas_PWidth)
       ||(mType == Meas_NWidth)
       ||(mType == Meas_PDuty)
       ||(mType == Meas_NDuty) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

QColor CMeasItem::setSrcColor(Chan src)
{
    QColor color = Qt::white;
    switch (src)
    {
    case chan1:
        color = CH1_color;
        break;
    case chan2:
        color = CH2_color;
        break;
    case chan3:
        color = CH3_color;
        break;
    case chan4:
        color = CH4_color;
        break;
    case m1: case m2: case m3: case m4:
        color = Math_color;
        break;
    case d0: case d1: case d2: case d3:
    case d4: case d5: case d6: case d7:
    case d8: case d9: case d10: case d11:
    case d12: case d13: case d14: case d15:
        color = QColor::fromRgb(0x00,0xff,0x00);
        break;

    default:
        color = Qt::white;
        break;
    }
    return color;
}

void CMeasItem::setIDS(int ids)
{
    m_nIDS = ids;
}

void CMeasItem::getIDSName()
{
    if( m_nIDS != 0 )
    {
        mName = sysGetString(m_nIDS);
    }    
}
