#include "appmeasure.h"

#include "../../service/servicefactory.h"
#include "../../service/service_name.h"

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";

#define def_time()
#define start_time()
#define end_time()

//! msg table
IMPLEMENT_CMD( CApp, CAppMeasure )
start_of_entry()
on_set_int_void(  MSG_APP_MEASURE,               &CAppMeasure::onActive ),

on_set_void_void( MSG_APP_MEAS_SRCA,             &CAppMeasure::setSrcA ),
on_set_void_void( MSG_APP_MEAS_SRCB,             &CAppMeasure::setSrcB ),
on_set_int_int(   MSG_APP_MEAS_CAT,              &CAppMeasure::setCategory ),
on_set_int_int(   MSG_APP_MEAS_ANALYZE_MENU,     &CAppMeasure::setAnalyze ),

on_set_void_void( MSG_APP_MEAS_CLEAR_ONE,        &CAppMeasure::delItem ),
on_set_void_void( MSG_APP_MEAS_CLEAR_ALL,        &CAppMeasure::delAllItem ),

on_set_void_void( MSG_APP_MEAS_ALL_SRC,          &CAppMeasure::onShowAll ),

on_set_void_void( MSG_APP_MEAS_SET_MENU,         &CAppMeasure::testtest ),
on_set_void_void( MSG_APP_MEAS_STAT_ENABLE,      &CAppMeasure::setStatBoxShow ),

on_set_void_void( MSG_APP_MEAS_INDICATOR,        &CAppMeasure::onIndicator ),
on_set_void_void( MSG_APP_MEAS_REGION,           &CAppMeasure::onRegionSet ),
on_set_void_void( MSG_APP_MEAS_RANGE_CURSOR_AX,  &CAppMeasure::onRangeABx ),
on_set_void_void( MSG_APP_MEAS_RANGE_CURSOR_BX,  &CAppMeasure::onRangeABx ),
on_set_void_void( MSG_APP_MEAS_RANGE_CURSOR_ABX,  &CAppMeasure::onRangeABx ),

on_set_void_void( MSG_APP_MEAS_TH_HIGH_TYPE_SUB_1, &CAppMeasure::onShowThresholdPos ),
on_set_void_void( MSG_APP_MEAS_TH_MID_TYPE_SUB_1,  &CAppMeasure::onShowThresholdPos ),
on_set_void_void( MSG_APP_MEAS_TH_LOW_TYPE_SUB_1,  &CAppMeasure::onShowThresholdPos ),
on_set_void_void( MSG_APP_MEAS_TH_DEFAULT,         &CAppMeasure::onShowThresholdPos ),

on_set_void_void( servMeasure::MSG_CMD_ADD_VALUE_ITEM,  &CAppMeasure::cmd_AddValueItem),
on_set_void_void( servMeasure::MSG_SHOW_ITEMBOX,        &CAppMeasure::onShowItemBox ),
on_set_void_void( servMeasure::MSG_UPDATE_MEAS,         &CAppMeasure::updateMeas ),

on_set_void_void( servMeasure::MSG_CMD_SEL_ITEM,        &CAppMeasure::cmd_SetSelItem),

//! spyon
on_set_void_void( MSG_HOR_ZOOM_ON,                      &CAppMeasure::onZoomOn ),

on_set_int_int (  CMD_SERVICE_SUB_ENTER,                &CAppMeasure::onEnterSubMenu ),
on_set_int_int (  CMD_SERVICE_SUB_RETURN,               &CAppMeasure::onReturnMainMenu ),
on_set_void_void( CMD_SERVICE_EXIT_ACTIVE,              &CAppMeasure::exitActive ),

//on_set_void_void( CMD_SERVICE_RST,                      &CAppMeasure::onRst),
on_set_void_void( CMD_SERVICE_STARTUP,                  &CAppMeasure::onStartup ),

//on_set_int_void( CMD_SERVICE_ACTIVE,                   &CAppMeasure::onActive ),

on_set_void_void( CAppMeasure::cmd_meas_opt_active,     &CAppMeasure::cmd_opt_active),
on_set_void_void( CAppMeasure::cmd_meas_opt_exp,        &CAppMeasure::cmd_opt_exp),
on_set_void_void( CAppMeasure::cmd_meas_opt_invalid,    &CAppMeasure::cmd_opt_invalid),

end_of_entry()

CAppMeasure::CAppMeasure()
{
    mservName   = serv_name_measure;
    m_pMeasItemBox  = new CMeasItemBox();
    m_pMeasItemBox->setServName(mservName);
    Q_ASSERT(m_pMeasItemBox != NULL);
    m_pMeasItemBox->setVisible(false);

    m_pValueBox = new CMeasValueBox();
    Q_ASSERT(m_pValueBox != NULL);
    m_pValueBox->setVisible(false);

    m_pStatValueBox = new CMeasStatValueBox();
    Q_ASSERT(m_pStatValueBox != NULL);
    m_pStatValueBox->setVisible(false);

    m_pAllBox = new CMeasAllBox();
    Q_ASSERT(m_pAllBox != NULL);
    m_pAllBox->setVisible(false);

    m_pStartButton = new CMeasStartButton();
    Q_ASSERT(m_pStartButton != NULL);

    m_MeasType = Meas_HOR_TYPE;
    m_MeasItems.clear();
    mChooseRow = 0;

    m_IndicatorStatus = false;
    m_Region = REGION_MAIN;

    m_ZoomOn = false;
    m_pIdcAx_Main = NULL;
    m_pIdcAy_Main = NULL;
    m_pIdcBx_Main = NULL;
    m_pIdcBy_Main = NULL;

    m_pIdcAx_Zoom = NULL;
    m_pIdcAy_Zoom = NULL;
    m_pIdcBx_Zoom = NULL;
    m_pIdcBy_Zoom = NULL;

    m_pRangeAx = NULL;
    m_pRangeBx = NULL;

    m_pRangeTimer = new QTimer( this);
    Q_ASSERT( m_pRangeTimer != NULL);
    m_pRangeTimer->setSingleShot( true);

    m_pThHighY_Main = NULL;
    m_pThMidY_Main  = NULL;
    m_pThLowY_Main  = NULL;

    m_pThHighY_Zoom = NULL;
    m_pThMidY_Zoom  = NULL;
    m_pThLowY_Zoom  = NULL;

    m_pThTimer = new QTimer( this);
    Q_ASSERT( m_pThTimer != NULL);
    m_pThTimer->setSingleShot( true);
}

CAppMeasure::~CAppMeasure()
{
}


void CAppMeasure::setupUi( CAppMain *pMain )
{
    m_pMeasItemBox->setupUi(pMain->getCentBar());

    m_pValueBox->setupUI(pMain->getCentBar());
    m_pStatValueBox->setupUI(pMain->getCentBar());

    m_pValueBox->updateMeasValue(&m_MeasItems);
//    m_pStatValueBox->updateMeasValue(&m_MeasItems);

    m_pAllBox->setupUi(pMain->getCentBar());

    m_pStartButton->setParent(pMain->getTopBar());
#if 1
    m_pStartButton->loadResource("measure/start_button/");
#else
    {
        r_meta::CRMeta rMeta;
        int x,y,w,h;
        QString strPath = "measure/start_button/";
        rMeta.getMetaVal( strPath + "x", x );
        rMeta.getMetaVal( strPath + "y", y );
        rMeta.getMetaVal( strPath + "width", w );
        rMeta.getMetaVal( strPath + "height", h );

        //rMeta.getMetaVal( strPath + "style", m_pBtnStyle );
        //rMeta.getMetaVal( strPath + "styleopen", m_pBtnStyleOpen );
        //m_pStartButton->setStyleSheet(m_pBtnStyle);

        m_pStartButton->setGeometry( x, y, w, h );
    }
#endif

    m_pStartButton->show();

    getHelpContent(mservName,"");
    m_pMeasItemBox->measHelpPic = getHelpPicPath(mservName);
    m_pMeasItemBox->setMeasHelpList( getHelpItemsList());

    //! Meas Indicator
    //! Main Indicator
    m_pIdcAx_Main = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x);
    Q_ASSERT( NULL != m_pIdcAx_Main);
    m_pIdcAx_Main->setStyle( cursor_ui::cursor_style_solid);
    m_pIdcAx_Main->setParent( pMain->getCentBar());
    m_pIdcAx_Main->setShow( false);
    m_pIdcAx_Main->setColor(INDICATOR_COLOR);

    m_pIdcAy_Main = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pIdcAy_Main);
    m_pIdcAy_Main->setStyle( cursor_ui::cursor_style_solid);
    m_pIdcAy_Main->setParent( pMain->getCentBar());
    m_pIdcAy_Main->setShow( false);
    m_pIdcAy_Main->setColor(INDICATOR_COLOR);

    m_pIdcBx_Main = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x);
    Q_ASSERT( NULL != m_pIdcBx_Main);
    m_pIdcBx_Main->setStyle( cursor_ui::cursor_style_dot);
    m_pIdcBx_Main->setParent( pMain->getCentBar());
    m_pIdcBx_Main->setShow( false);
    m_pIdcBx_Main->setColor(INDICATOR_COLOR);

    m_pIdcBy_Main = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pIdcBy_Main);
    m_pIdcBy_Main->setStyle( cursor_ui::cursor_style_dot);
    m_pIdcBy_Main->setParent( pMain->getCentBar());
    m_pIdcBy_Main->setShow( false);
    m_pIdcBy_Main->setColor(INDICATOR_COLOR);

    //! Zoom Indicator
    m_pIdcAx_Zoom = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x);
    Q_ASSERT( NULL != m_pIdcAx_Zoom);
    m_pIdcAx_Zoom->setStyle( cursor_ui::cursor_style_solid);
    m_pIdcAx_Zoom->setParent( pMain->getCentBar());
    m_pIdcAx_Zoom->setShow( false);
    m_pIdcAx_Zoom->setColor(INDICATOR_COLOR);

    m_pIdcAy_Zoom = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pIdcAy_Zoom);
    m_pIdcAy_Zoom->setStyle( cursor_ui::cursor_style_solid);
    m_pIdcAy_Zoom->setParent( pMain->getCentBar());
    m_pIdcAy_Zoom->setShow( false);
    m_pIdcAy_Zoom->setColor(INDICATOR_COLOR);

    m_pIdcBx_Zoom = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x);
    Q_ASSERT( NULL != m_pIdcBx_Zoom);
    m_pIdcBx_Zoom->setStyle( cursor_ui::cursor_style_dot);
    m_pIdcBx_Zoom->setParent( pMain->getCentBar());
    m_pIdcBx_Zoom->setShow( false);
    m_pIdcBx_Zoom->setColor(INDICATOR_COLOR);

    m_pIdcBy_Zoom = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pIdcBy_Zoom);
    m_pIdcBy_Zoom->setStyle( cursor_ui::cursor_style_dot);
    m_pIdcBy_Zoom->setParent( pMain->getCentBar());
    m_pIdcBy_Zoom->setShow( false);
    m_pIdcBy_Zoom->setColor(INDICATOR_COLOR);

    //! Meas Range Cursor
    m_pRangeAx = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x);
    Q_ASSERT( NULL != m_pRangeAx);
    m_pRangeAx->setStyle( cursor_ui::cursor_style_solid);
    m_pRangeAx->setParent( pMain->getCentBar());
    m_pRangeAx->setShow( false);

    m_pRangeBx = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x);
    Q_ASSERT( NULL != m_pRangeBx);
    m_pRangeBx->setStyle( cursor_ui::cursor_style_solid);
    m_pRangeBx->setParent( pMain->getCentBar());
    m_pRangeBx->setShow( false);

    //! Meas Threshold Cursor
    m_pThHighY_Main = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pThHighY_Main);
    m_pThHighY_Main->setStyle( cursor_ui::cursor_style_solid);
    m_pThHighY_Main->setParent( pMain->getCentBar());
    m_pThHighY_Main->setShow( false);

    m_pThMidY_Main = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pThMidY_Main);
    m_pThMidY_Main->setStyle( cursor_ui::cursor_style_solid);
    m_pThMidY_Main->setParent( pMain->getCentBar());
    m_pThMidY_Main->setShow( false);

    m_pThLowY_Main = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pThLowY_Main);
    m_pThLowY_Main->setStyle( cursor_ui::cursor_style_solid);
    m_pThLowY_Main->setParent( pMain->getCentBar());
    m_pThLowY_Main->setShow( false);

    m_pThHighY_Zoom = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pThHighY_Zoom);
    m_pThHighY_Zoom->setStyle( cursor_ui::cursor_style_solid);
    m_pThHighY_Zoom->setParent( pMain->getCentBar());
    m_pThHighY_Zoom->setShow( false);

    m_pThMidY_Zoom = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pThMidY_Zoom);
    m_pThMidY_Zoom->setStyle( cursor_ui::cursor_style_solid);
    m_pThMidY_Zoom->setParent( pMain->getCentBar());
    m_pThMidY_Zoom->setShow( false);

    m_pThLowY_Zoom = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y);
    Q_ASSERT( NULL != m_pThLowY_Zoom);
    m_pThLowY_Zoom->setStyle( cursor_ui::cursor_style_solid);
    m_pThLowY_Zoom->setParent( pMain->getCentBar());
    m_pThLowY_Zoom->setShow( false);


    //for bug1749 by hxh
    //! Set Indicator View
    sysAttachUIView( m_pIdcAx_Main, view_yt_main);
    sysAttachUIView( m_pIdcAy_Main, view_yt_main);
    sysAttachUIView( m_pIdcBx_Main, view_yt_main);
    sysAttachUIView( m_pIdcBy_Main, view_yt_main);

    sysAttachUIView( m_pIdcAx_Zoom, view_yt_zoom);
    sysAttachUIView( m_pIdcAy_Zoom, view_yt_zoom);
    sysAttachUIView( m_pIdcBx_Zoom, view_yt_zoom);
    sysAttachUIView( m_pIdcBy_Zoom, view_yt_zoom);

}

void CAppMeasure::retranslateUi( )
{

    for(int i=0; i<m_MeasItems.size(); i++)
    {
        CMeasValueItem *pItemValue = list_at(m_MeasItems, i);
        if( pItemValue != NULL )
        {
            pItemValue->getItemInfo()->getIDSName();
        }
    }

    if( m_pMeasItemBox != NULL )
    {
        m_pMeasItemBox->retranslateUi();
    }

    if( m_pValueBox != NULL && m_pValueBox->isVisible())
    {
        m_pValueBox->update();
    }

    if( m_pAllBox != NULL  && m_pAllBox->isVisible())
    {
        m_pAllBox->retranslateUi();
    }
}

void CAppMeasure::buildConnection()
{
    connect(m_pStartButton, SIGNAL(released()), this, SLOT(onClicked()));

    connect(m_pMeasItemBox,
            SIGNAL(clickItem(CMeasItem *)),
            this,
            SLOT(addValueItem(CMeasItem *)));

    connect(m_pStatValueBox, SIGNAL(delItemSig()), this, SLOT(delItem()));

    connect(m_pValueBox, SIGNAL(rowChoosed(int)), this, SLOT(setChooseRow(int)));
    connect(m_pValueBox, SIGNAL(delItemSig()), this, SLOT(delItem()));

    connect(m_pMeasItemBox, SIGNAL(raiseItemBox()), this, SLOT(raiseItemBox()));

    connect(m_pValueBox, SIGNAL(raiseValueBox()), this, SLOT(raiseValueBox()));

    connect(m_pRangeTimer, SIGNAL(timeout()), this, SLOT(onRangeTimeout()) );
    connect(m_pThTimer, SIGNAL(timeout()), this, SLOT(onThTimeout()) );
}

void CAppMeasure::registerSpy()
{
    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON );

    spyOn(serv_name_license,
          servLicense::cmd_Opt_Active ,
          CAppMeasure::cmd_meas_opt_active);

    spyOn(serv_name_license,
          servLicense::cmd_Opt_Exp,
          CAppMeasure::cmd_meas_opt_exp);

    spyOn(serv_name_license,
          servLicense::cmd_Opt_Invalid,
          CAppMeasure::cmd_meas_opt_invalid);
}

void CAppMeasure::onStartup()
{
    onRst();
}

void CAppMeasure::onRst()
{
    delAllItem();

    if( m_pAllBox->isVisible())
    {
        m_pAllBox->hide();
    }

    if( m_pValueBox != NULL  )
    {
        m_pValueBox->setStatShow( false );
    }

    m_pMeasItemBox->rst();
    m_pAllBox->rst();
    m_pValueBox->rst();

    on_rst();
}

void CAppMeasure::onZoomOn()
{
    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON, m_ZoomOn);
}

void CAppMeasure::onClicked()
{
    if(m_pMeasItemBox->isHidden())
    {
        //! Enter Add Menu
        //m_pStartButton->setStyleSheet(m_pBtnStyleOpen);
        m_pStartButton->setActive( true );
        serviceExecutor::post( serv_name_measure,
                               CMD_SERVICE_ACTIVE,
                               MSG_APP_MEAS_CAT);
    }
    else
    {
        //! Close ItemBox
        m_pMeasItemBox->setChoosed(false);
        m_pMeasItemBox->unload();
        m_pMeasItemBox->hide();
        m_IndicatorStatus = false;
    }
    serviceExecutor::query( serv_name_measure,
                            MSG_APP_MEAS_INDICATOR,
                            m_IndicatorStatus);
}

void CAppMeasure::onRangeTimeout()
{
    if( (m_pRangeAx != NULL)&&(m_pRangeBx != NULL))
    {
        m_pRangeAx->setShow( false);
        m_pRangeBx->setShow( false);
    }
}

void CAppMeasure::onThTimeout()
{
    if( (m_pThHighY_Main != NULL)&&
        (m_pThMidY_Main != NULL) &&
        (m_pThLowY_Main != NULL))
    {
        m_pThHighY_Main->setShow( false);
        m_pThMidY_Main->setShow( false);
        m_pThLowY_Main->setShow( false);
    }

    if( (m_pThHighY_Zoom != NULL) &&
        (m_pThMidY_Zoom != NULL) &&
        (m_pThLowY_Zoom != NULL))
    {
        m_pThHighY_Zoom->setShow( false);
        m_pThMidY_Zoom->setShow( false);
        m_pThLowY_Zoom->setShow( false);
    }
}

void CAppMeasure::addValueItem(CMeasItem *item)
{
    if( item != 0 )
    {
        m_pValueBox->show();
        addValueItemRaw(*item, false);
    }
}

void CAppMeasure::cmd_AddValueItem( void )
{
#if 0
    CArgument arg;
    serviceExecutor::query( serv_name_measure, servMeasure::MSG_CMD_GET_VALUE_ITEM, arg);

    MeasType type = (MeasType)arg[0].iVal;
    Chan srcA     = (Chan)arg[1].iVal;
    Chan srcB     = (Chan)arg[2].iVal;
    qDebug() << "------------------AppMeas: cmd_AddValueItem  arg[type] = "<<type;
    CMeasItem *pitem = m_pMeasItemBox->findItem(type);
    if( NULL != pitem)
    {
        CMeasItem item = *pitem;
        item.setSrcA(srcA);
        item.setSrcB(srcB);
        addValueItemRaw(item, true);
    }

#else   //! for bug 2442
    typedef QList<measStatData*> MeasStatDataList;
    void *ptr = NULL;
    serviceExecutor::query( serv_name_measure,
                            servMeasure::MSG_STAT_MEAS_DATA, ptr);
    Q_ASSERT( NULL != ptr);
    MeasStatDataList *pMeasList = (MeasStatDataList *)ptr;

    if( pMeasList->size() > 0)
    {
        CMeasItem *pitem = NULL;
        MeasType type = (MeasType)0;
        Chan srcA = chan_none;
        Chan srcB = chan_none;

        for( int i=0; i<pMeasList->size(); i++)
        {
            type = pMeasList->at(i)->getType();
            srcA = pMeasList->at(i)->getSrcA();
            srcB = pMeasList->at(i)->getSrcB();

            pitem = m_pMeasItemBox->findItem(type);
            if( NULL != pitem)
            {
                CMeasItem item = *pitem;
                item.setSrcA(srcA);
                item.setSrcB(srcB);
                addValueItemRaw(item, true);
            }
        }
    }
#endif
}

void CAppMeasure::addValueItemRaw(CMeasItem item, bool fromCmd)
{
    bool itemExist = false;

    //! exist in list
    for(int i=0; i<m_MeasItems.count(); i++)
    {
        if( list_at( m_MeasItems,i)->itemExist(item) )
        {
            itemExist = true;
            break;
        }
    }

    if(!itemExist)
    {
        //! a new item
        CMeasValueItem* pItem = new CMeasValueItem(item);
        Q_ASSERT( pItem != NULL );
        pItem->getItemInfo()->getIDSName();

        int type = (int)(item.mType);
        int srcA = (int)(item.mSrcA);
        int srcB = (int)(item.mSrcB);
        CArgument arg;
        arg.setVal( type, 0);
        arg.setVal( srcA, 1);
        arg.setVal( srcB, 2);

        //! not full
        if( m_MeasItems.count() < MAX_COUNT )
        {
            m_MeasItems.append(pItem);
            if( !fromCmd)
            {
                serviceExecutor::post(serv_name_measure,
                                      servMeasure::MSG_ADD_STAT_ITEM, arg);
            }
        }
        //! full
        else
        {
            //! remove head
            Q_ASSERT( m_MeasItems.at( 0 ) != NULL );
            delete m_MeasItems.at( 0 );
            m_MeasItems.removeAt(0);

            //! add tail
            m_MeasItems.append( pItem );

            if( !fromCmd)
            {
                serviceExecutor::post(serv_name_measure,
                                      servMeasure::MSG_DEL_STAT_ITEM, 0);

                serviceExecutor::post(serv_name_measure,
                                      servMeasure::MSG_ADD_STAT_ITEM, arg);
            }
        }

        m_pValueBox->updateMeasValue(&m_MeasItems);
        m_pValueBox->raise();
        m_pValueBox->show();
        mChooseRow = m_MeasItems.size()-1;
        m_pValueBox->setChooseRow( mChooseRow);  //! Choose new MeasValItem

        //for bug 2493 by zy
        serviceExecutor::post(serv_name_measure,
                              servMeasure::cmd_set_select_item,
                              mChooseRow);
    }
}

void CAppMeasure::setChooseRow(int index)
{
    //no measure item. return. by hxh 2018-05-17
    if( m_MeasItems.size() == 0 )
    {
        return;
    }

    int lastRow = mChooseRow;
    m_pValueBox->setChooseRow(index);
    mChooseRow = index;
    setIndicator(m_IndicatorStatus, list_at( m_MeasItems,mChooseRow));

    //add by lidongming for bug2493
    if( lastRow != mChooseRow)
    {
        //bug 2493 by zy
        serviceExecutor::post( serv_name_measure,
                               servMeasure::cmd_set_select_item,mChooseRow);
    }

}

int CAppMeasure::onActive()
{
    bool b;
    serviceExecutor::query( mservName, MSG_APP_MEASURE,  b);
    if( b)
    {
        //m_pStartButton->setStyleSheet(m_pBtnStyleOpen);

        if( m_MeasItems.size() > 0)
        {
            //! Display ValueBox
            m_pValueBox->show();
            m_pValueBox->setChoosed(true);
            m_pValueBox->load();
        }

        int ch = 0;
        serviceExecutor::query( mservName, MSG_APP_MEAS_ALL_SRC, ch );
        if( (Chan)ch != chan_none)
        {
            //! Display AllBox
            m_pAllBox->show();
        }
        serviceExecutor::query( serv_name_measure,
                                MSG_APP_MEAS_INDICATOR,
                                m_IndicatorStatus);
    }
    else //! Close All Meas Window
    {
        //m_pStartButton->setStyleSheet(m_pBtnStyle);
        //! Close ItemBox
        m_pMeasItemBox->setChoosed(false);
        m_pMeasItemBox->unload();
        m_pMeasItemBox->hide();
        //! Close ValueBox
        m_pValueBox->setChoosed(false);
        m_pValueBox->unload();
        m_pValueBox->hide();
        //! Close All Box
        if( m_pAllBox->isVisible())
        {
            m_pAllBox->hide();
        }
        m_IndicatorStatus = false;
    }

    m_pStartButton->setActive( b );


    return ERR_NONE;
}

int CAppMeasure::onEnterSubMenu(int)
{
    int subMenuMsg = 0;
    serviceExecutor::query( serv_name_measure,
                            servMeasure::MSG_GET_SUB_MENU,  subMenuMsg);

    int ch = 0;
    serviceExecutor::query( mservName, MSG_APP_MEAS_ALL_SRC, ch );

    switch (subMenuMsg)
    {
    case MSG_APP_MEAS_ADD_MENU:
        if(work_help == sysGetWorkMode())
        {
            return ERR_NONE;
        }
        m_pValueBox->unload();
        if( m_MeasItems.count()>0)
        {
            m_pValueBox->show();
        }
        m_pValueBox->setChoosed(true);
        if( (Chan)ch != chan_none)
        {
            //! Display AllBox
            m_pAllBox->show();
        }
        m_pMeasItemBox->setShow( true );
        m_pMeasItemBox->setChoosed(true);
        m_pMeasItemBox->load();
        m_pMeasItemBox->raise();
        break;

    case MSG_APP_MEAS_REMOVE_MENU:
        m_pMeasItemBox->setChoosed(false);
        m_pMeasItemBox->unload();
        m_pMeasItemBox->hide();
        if( (Chan)ch != chan_none)
        {
            //! Display AllBox
            m_pAllBox->show();
        }
        if(m_MeasItems.count()>0)
        {
            m_pValueBox->show();
            m_pValueBox->setChoosed(true);
            m_pValueBox->load();
        }
        break;

    case MSG_APP_MEAS_SET_MENU:
        m_pMeasItemBox->setChoosed(false);
        m_pMeasItemBox->unload();
        m_pMeasItemBox->hide();
        m_pValueBox->setChoosed(true);
        m_pValueBox->unload();
        if( (Chan)ch != chan_none)
        {
            //! Display AllBox
            m_pAllBox->show();
        }
        break;

    case MSG_APP_MEAS_STAT_MENU:
        m_pMeasItemBox->setChoosed(false);
        m_pMeasItemBox->unload();
        m_pMeasItemBox->hide();
        m_pValueBox->setChoosed(true);
        m_pValueBox->unload();
        if( (Chan)ch != chan_none)
        {
            //! Display AllBox
            m_pAllBox->show();
        }
        break;

    case MSG_APP_MEAS_ANALYZE_MENU:

        break;

    default:
        break;
    }

    return 0;
}

int CAppMeasure::onReturnMainMenu(int)
{
    m_pMeasItemBox->setChoosed(false);
    m_pMeasItemBox->unload();
    m_pMeasItemBox->hide();

    if(m_MeasItems.count()>0)
    {
        m_pValueBox->show();
        m_pValueBox->setChoosed(true);
        m_pValueBox->load();
    }

    return 0;
}

void CAppMeasure::onIndicator( void )
{
    serviceExecutor::query( serv_name_measure,
                            MSG_APP_MEAS_INDICATOR,
                            m_IndicatorStatus);

    if( m_IndicatorStatus)
    {
        m_pValueBox->setChoosed(true);
        m_pValueBox->setActive();
    }
    else
    {
        m_pValueBox->setChoosed(true);
        m_pValueBox->unload();
    }


    if( m_MeasItems.size() > 0)
    {
        CMeasValueItem *pItem = list_at( m_MeasItems,mChooseRow);
        MeasType type = pItem->getItemInfo()->mType;
        Chan src      = pItem->getItemInfo()->mSrcA;

        if(  (type == Meas_PPulses)||(type == Meas_NPulses)
           ||(type == Meas_PEdges) ||(type == Meas_NEdges) )
        {
            CArgument arg;
            arg.setVal( (int)type, 0);
            arg.setVal( (int)src, 1);
            serviceExecutor::post( serv_name_measure,
                                   servMeasure::cmd_set_FPGA_CntType, arg);
        }
        setIndicator(m_IndicatorStatus, pItem);
    }
}

void CAppMeasure::onRegionSet( void )
{
    int region = (int)REGION_MAIN;

    serviceExecutor::query( serv_name_measure, MSG_APP_MEAS_REGION, region);

    m_Region = (RegionType)region;

    if( m_Region == REGION_CURSOR)
    {
        if( isServiceActive())
        {
            onRangeABx();
        }
    }
}

void CAppMeasure::onRangeABx( void )
{
    int rangeAx = 0;
    int rangeBx = 0;

    serviceExecutor::query( serv_name_measure, MSG_APP_MEAS_RANGE_CURSOR_AX, rangeAx);
    serviceExecutor::query( serv_name_measure, MSG_APP_MEAS_RANGE_CURSOR_BX, rangeBx);

    if( m_Region != REGION_CURSOR)
    {
        return;
    }

    if( m_ZoomOn)
    {
        m_pRangeAx->setPos( rangeAx);
        sysAttachUIView( m_pRangeAx, view_yt_zoom);
        m_pRangeAx->setShow( true);

        m_pRangeBx->setPos( rangeBx);
        sysAttachUIView( m_pRangeBx, view_yt_zoom);
        m_pRangeBx->setShow( true);
    }
    else
    {
        m_pRangeAx->setPos( rangeAx);
        sysAttachUIView( m_pRangeAx, view_yt_main);
        m_pRangeAx->setShow( true);

        m_pRangeBx->setPos( rangeBx);
        sysAttachUIView( m_pRangeBx, view_yt_main);
        m_pRangeBx->setShow( true);
    }

    //! Start Timer
    m_pRangeTimer->start( CURSOR_TIMEOUT);
}

void CAppMeasure::onShowThresholdPos( void )
{
    int thType = (int)meas_algorithm::TH_TYPE_PER;
    serviceExecutor::query( serv_name_measure,
                            MSG_APP_MEAS_TH_HIGH_TYPE,
                            thType);

    int src    = chan1;
    serviceExecutor::query( serv_name_measure,
                            MSG_APP_MEAS_TH_SRC,
                            src);

    QColor color = sysGetChColor((Chan)src);
    m_pThHighY_Main->setColor( color );
    m_pThMidY_Main->setColor(  color );
    m_pThLowY_Main->setColor(  color );
    m_pThHighY_Zoom->setColor( color );
    m_pThMidY_Zoom->setColor(  color );
    m_pThLowY_Zoom->setColor(  color );

    if( thType == (int)meas_algorithm::TH_TYPE_ABS)
    {
        int highPos = 0;
        int midPos = 0;
        int lowPos = 0;

        serviceExecutor::query( serv_name_measure, servMeasure::cmd_get_Th_HighPos, highPos);
        serviceExecutor::query( serv_name_measure, servMeasure::cmd_get_Th_MidPos,  midPos);
        serviceExecutor::query( serv_name_measure, servMeasure::cmd_get_Th_LowPos,  lowPos);

        if( m_ZoomOn)
        {
            m_pThHighY_Main->setPos( highPos);
            m_pThMidY_Main->setPos( midPos);
            m_pThLowY_Main->setPos( lowPos);
            m_pThHighY_Zoom->setPos( highPos);
            m_pThMidY_Zoom->setPos( midPos);
            m_pThLowY_Zoom->setPos( lowPos);

            sysAttachUIView( m_pThHighY_Main, view_yt_main);
            sysAttachUIView( m_pThMidY_Main, view_yt_main);
            sysAttachUIView( m_pThLowY_Main, view_yt_main);
            sysAttachUIView( m_pThHighY_Zoom, view_yt_zoom);
            sysAttachUIView( m_pThMidY_Zoom, view_yt_zoom);
            sysAttachUIView( m_pThLowY_Zoom, view_yt_zoom);

            m_pThHighY_Main->setShow( true);
            m_pThMidY_Main->setShow( true);
            m_pThLowY_Main->setShow( true);
            m_pThHighY_Zoom->setShow( true);
            m_pThMidY_Zoom->setShow( true);
            m_pThLowY_Zoom->setShow( true);
        }
        else
        {
            m_pThHighY_Main->setPos( highPos);
            m_pThMidY_Main->setPos( midPos);
            m_pThLowY_Main->setPos( lowPos);

            sysAttachUIView( m_pThHighY_Main, view_yt_main);
            sysAttachUIView( m_pThMidY_Main, view_yt_main);
            sysAttachUIView( m_pThLowY_Main, view_yt_main);

            m_pThHighY_Main->setShow( true);
            m_pThMidY_Main->setShow( true);
            m_pThLowY_Main->setShow( true);
            m_pThHighY_Zoom->setShow( false);
            m_pThMidY_Zoom->setShow( false);
            m_pThLowY_Zoom->setShow( false);
        }
        //! Start Timer
        m_pThTimer->start( CURSOR_TIMEOUT);
    }
    else
    {
        m_pThHighY_Main->setShow( false);
        m_pThMidY_Main->setShow( false);
        m_pThLowY_Main->setShow( false);
        m_pThHighY_Zoom->setShow( false);
        m_pThMidY_Zoom->setShow( false);
        m_pThLowY_Zoom->setShow( false);
    }
}

void CAppMeasure::setIndicator(bool on, CMeasValueItem *valItem)
{

    int posAx = 0;
    int posAy = 0;
    int posBx = 0;
    int posBy = 0;

    bool bAx = false;
    bool bAy = false;
    bool bBx = false;
    bool bBy = false;

    if( on)  //! Display Meas Indicator
    {
        posAx = valItem->getAx().mPos;
        bAx   = valItem->getAx().bVisible;

        posAy = valItem->getAy().mPos;
        bAy   = valItem->getAy().bVisible;

        posBx = valItem->getBx().mPos;
        bBx   = valItem->getBx().bVisible;

        posBy = valItem->getBy().mPos;
        bBy   = valItem->getBy().bVisible;
    }
    else   //!No Meas Indicator
    {
        bAx = false;
        bAy = false;
        bBx = false;
        bBy = false;
    }

    if( m_Region == REGION_MAIN)
    {
        m_pIdcAx_Main->setShow( bAx);
        m_pIdcAx_Main->setPos( posAx);
        m_pIdcAy_Main->setShow( bAy);
        m_pIdcAy_Main->setPos( posAy);
        m_pIdcBx_Main->setShow( bBx);
        m_pIdcBx_Main->setPos( posBx);
        m_pIdcBy_Main->setShow( bBy);
        m_pIdcBy_Main->setPos( posBy);

        m_pIdcAx_Zoom->setShow( false);
        m_pIdcAy_Zoom->setShow( false);
        m_pIdcBx_Zoom->setShow( false);
        m_pIdcBy_Zoom->setShow( false);
    }
    else if( m_Region == REGION_ZOOM)
    {
        m_pIdcAx_Zoom->setShow( bAx);
        m_pIdcAx_Zoom->setPos( posAx);
        m_pIdcAy_Zoom->setShow( bAy);
        m_pIdcAy_Zoom->setPos( posAy);
        m_pIdcBx_Zoom->setShow( bBx);
        m_pIdcBx_Zoom->setPos( posBx);
        m_pIdcBy_Zoom->setShow( bBy);
        m_pIdcBy_Zoom->setPos( posBy);

        m_pIdcAx_Main->setShow( false);
        m_pIdcAy_Main->setShow( false);
        m_pIdcBx_Main->setShow( false);
        m_pIdcBy_Main->setShow( false);
    }
    else if( m_Region == REGION_CURSOR)
    {
        if( m_ZoomOn)
        {
            //! Zoom Indicator
            m_pIdcAx_Zoom->setShow( bAx);
            m_pIdcAx_Zoom->setPos( posAx);
            m_pIdcAy_Zoom->setShow( bAy);
            m_pIdcAy_Zoom->setPos( posAy);
            m_pIdcBx_Zoom->setShow( bBx);
            m_pIdcBx_Zoom->setPos( posBx);
            m_pIdcBy_Zoom->setShow( bBy);
            m_pIdcBy_Zoom->setPos( posBy);

            m_pIdcAx_Main->setShow( false);
            m_pIdcAy_Main->setShow( false);
            m_pIdcBx_Main->setShow( false);
            m_pIdcBy_Main->setShow( false);
        }
        else
        {   //! Main Indicator
            m_pIdcAx_Main->setShow( bAx);
            m_pIdcAx_Main->setPos( posAx);
            m_pIdcAy_Main->setShow( bAy);
            m_pIdcAy_Main->setPos( posAy);
            m_pIdcBx_Main->setShow( bBx);
            m_pIdcBx_Main->setPos( posBx);
            m_pIdcBy_Main->setShow( bBy);
            m_pIdcBy_Main->setPos( posBy);

            m_pIdcAx_Zoom->setShow( false);
            m_pIdcAy_Zoom->setShow( false);
            m_pIdcBx_Zoom->setShow( false);
            m_pIdcBy_Zoom->setShow( false);
        }
    }
}

void CAppMeasure::setFocus()
{
    bool b =false;
    if(b == true)
    {
        if(m_MeasItems.count()>0)
        {
            m_pValueBox->show();
            m_pValueBox->setChoosed(true);
            m_pValueBox->setActive();
            m_pMeasItemBox->setChoosed(false);
        }
        else
        {
            m_pValueBox->hide();
        }
    }
    else
    {
        if(work_help == sysGetWorkMode())
        {
            return;
        }
        m_pMeasItemBox->setShow( true );
        m_pMeasItemBox->raise();
        m_pMeasItemBox->setChoosed(true);
        m_pMeasItemBox->setActive();
        m_pValueBox->setChoosed(true);
        m_pValueBox->unload();
        m_pValueBox->raise();
    }
}

void CAppMeasure::exitActive()
{
    m_pMeasItemBox->unload();
    m_pMeasItemBox->hide();
    m_pValueBox->unload();
    m_pValueBox->setChoosed(true);
}

void CAppMeasure::onShowItemBox( void )
{   
    if(work_help == sysGetWorkMode())
    {
        return;
    }
    m_pMeasItemBox->setShow( true);
    m_pMeasItemBox->setChoosed( true);
    m_pMeasItemBox->load();
    m_pMeasItemBox->raise();
}

void CAppMeasure::setSrcA()
{
    int src = 0;
    serviceExecutor::query( mservName, MSG_APP_MEAS_SRCA, src);
    m_pMeasItemBox->setSrcA(Chan(src));
    m_pMeasItemBox->update();
    m_pAllBox->update();
    m_pValueBox->update();
//    m_pStatValueBox->update();
}

void CAppMeasure::setSrcB()
{
    int src = 0;
    serviceExecutor::query( mservName, MSG_APP_MEAS_SRCB, src);
    m_pMeasItemBox->setSrcB(Chan(src));
    m_pMeasItemBox->update();
    m_pAllBox->update();
    m_pValueBox->update();
//    m_pStatValueBox->update();
}

int CAppMeasure::setCategory(AppEventCause aec)
{
    if(aec != cause_value)
    {
        return ERR_NONE;
    }
    int cat = 0;
    serviceExecutor::query( mservName, MSG_APP_MEAS_CAT, cat);
    if(cat == 0)
    {
        m_pMeasItemBox->setHorType();
    }
    else if(cat == 1)
    {
        m_pMeasItemBox->setVerType();
    }
    else
    {
        m_pMeasItemBox->setDsrcType();
    }

    onShowItemBox();
    return ERR_NONE;
}

int CAppMeasure::setAnalyze(AppEventCause aec)
{
    if(aec != cause_value)
    {
        return ERR_NONE;
    }
    m_pMeasItemBox->setAlyType();
    return ERR_NONE;
}

void CAppMeasure::cmd_SetSelItem( void )
{
    int item;
    serviceExecutor::query( mservName, servMeasure::MSG_CMD_SEL_ITEM, item);

    if( (item >= 0)&&(item<m_MeasItems.size()) )
    {
        mChooseRow = item;
        m_pValueBox->setChooseRow(mChooseRow);
    }
}

void CAppMeasure::onOptDetect()
{
    bool b = false;
    b = sysCheckLicense(OPT_PWR);
    m_pMeasItemBox->setUPAShow(b);
    serviceExecutor::post( serv_name_measure,
                           servMeasure::cmd_meas_upa_show, b);
}
void CAppMeasure::cmd_opt_active()
{
    onOptDetect();
}
void CAppMeasure::cmd_opt_exp()
{
    onOptDetect();
}
void CAppMeasure::cmd_opt_invalid()
{
    onOptDetect();
}

void CAppMeasure::delItem( void )
{
    int delRow = mChooseRow;
    if( m_MeasItems.count() == 1)
    {
//        m_pStatValueBox->hide();
        m_pValueBox->hide();
        mChooseRow = 0;
        delete m_MeasItems.at( mChooseRow);
        m_MeasItems.removeAt(mChooseRow);

        setIndicator(false, 0);  //! Close Indicator
        delRow = mChooseRow;
    }
    else if(mChooseRow < m_MeasItems.count()-1)
    {
        delete m_MeasItems.at( mChooseRow);
        m_MeasItems.removeAt(mChooseRow);
        m_pValueBox->setChooseRow(mChooseRow);
        delRow = mChooseRow;
    }
    else if(mChooseRow == m_MeasItems.count()-1)
    {
        delete m_MeasItems.at( mChooseRow);
        m_MeasItems.removeAt(mChooseRow);
        delRow = mChooseRow;
        m_pValueBox->setChooseRow(--mChooseRow);
    }    

    m_pValueBox->updateMeasValue(&m_MeasItems);
    serviceExecutor::post(mservName, servMeasure::MSG_DEL_STAT_ITEM, delRow);

    //bug 3307 by zy
    if( delRow != mChooseRow)
    {
        serviceExecutor::post( serv_name_measure,
                               servMeasure::cmd_set_select_item,mChooseRow);
    }
}

void CAppMeasure::raiseItemBox()
{
    m_pMeasItemBox->raise();
    m_pMeasItemBox->setActive();
    m_pMeasItemBox->setChoosed(true);
    m_pValueBox->setChoosed(true);
    m_pValueBox->unload();
    m_pValueBox->setDeactive();
//    m_pStatValueBox->setChoosed(false);
//    m_pValueBox->raise();
    serviceExecutor::post(mservName, MSG_APP_MEAS_ADD_MENU, 0);   
}

void CAppMeasure::raiseAllBox()
{
    m_pAllBox->raise();
    m_pAllBox->setActive();
    m_pMeasItemBox->setChoosed(false);
    m_pMeasItemBox->unload();
    m_pMeasItemBox->setDeactive();

    m_pValueBox->setChoosed(true);
//    m_pStatValueBox->setChoosed(false);
//    m_pValueBox->raise();
    m_pValueBox->unload();
    m_pValueBox->setDeactive();
}

void CAppMeasure::raiseValueBox()
{
    m_pValueBox->setActive();
    m_pValueBox->setChoosed(true);
//    m_pStatValueBox->setChoosed(false);
    m_pValueBox->raise();

    m_pMeasItemBox->setChoosed(false);
    m_pMeasItemBox->unload();
    m_pMeasItemBox->setDeactive();
    serviceExecutor::post(mservName, MSG_APP_MEAS_REMOVE_MENU, 0);

}

void CAppMeasure::raiseStatBox()
{
//    m_pStatValueBox->raise();
//    m_pStatValueBox->setActive();

    m_pMeasItemBox->setChoosed(false);
    m_pMeasItemBox->unload();

    m_pValueBox->setChoosed(true);

    m_pValueBox->raise();
}

void CAppMeasure::delAllItem( void )
{
    //! delete all
    foreach( CMeasValueItem* pItem, m_MeasItems )
    {
        Q_ASSERT( NULL != pItem );
        delete pItem;
    }
    //! clear
    m_MeasItems.clear();


    mChooseRow = 0;
    setIndicator(false, 0);  //! Close Indicator

    m_pValueBox->hide();
}

void CAppMeasure::setMeasHistShow()
{
    bool isHistShow;
    serviceExecutor::query( mservName, MSG_APP_MEAS_TREND_ENABLE, isHistShow);
}

void CAppMeasure::setStatBoxShow()
{
    bool stat = false;
    serviceExecutor::query( mservName, MSG_APP_MEAS_STAT_ENABLE, stat);

    if( stat == true)
    {
        m_pValueBox->setStatShow( true );
        if( m_MeasItems.size() > 0)
        {
            m_pValueBox->raise();
            m_pValueBox->show();
        }
    }
    else
    {
        m_pValueBox->setStatShow( false );
    }
}

void CAppMeasure::testtest( void )
{
}

void CAppMeasure::onShowAll( void )
{
    Chan ch = chan_none;
    int index = 0;

    serviceExecutor::query( mservName, MSG_APP_MEAS_ALL_SRC, index );
    ch = Chan(index);

    if(ch != chan_none)
    {
        m_pAllBox->setMeasSrc(ch);
        m_pAllBox->raise();
        m_pValueBox->raise();
        m_pAllBox->retranslateUi();
        m_pAllBox->show();
    }
    else  //chan_none
    {
        if( m_pAllBox->isVisible())
        {
            m_pAllBox->hide();
        }
    }
}

void CAppMeasure::updateMeas( void )
{
    def_time();
    typedef QList<measStatData*> MeasStatDataList;
    void *ptr = NULL;

    start_time();
    serviceExecutor::query( serv_name_measure, servMeasure::MSG_STAT_MEAS_DATA, ptr);
    if(m_MeasItems.size() > 0)
    {
        Q_ASSERT( NULL != ptr );
        MeasStatDataList *pStatDataList = (MeasStatDataList *)ptr;

        //! foreach visible item
        for(int i=0; i< m_MeasItems.size(); i++ )
        {
            list_at( m_MeasItems,i)->setValueFromList(pStatDataList);
        }
        setIndicator(m_IndicatorStatus, list_at( m_MeasItems,mChooseRow));
    }

    m_pValueBox->update();

    if( m_pAllBox->isVisible())
    {
        m_pAllBox->updateAllValue();
    }

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
    end_time();
//    m_pStatValueBox->update();

}
