#ifndef MEASITEM
#define MEASITEM

#include "../../include/dsostd.h"
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"
#include "../../service/servmeasure/meas_dsrc.h"
#include "../../service/servmeasure/meas_interface.h"
#include "../../service/servmeasure/meas_stat.h"
#include "cmeasitem.h"

typedef QList<measDataDSrc*>* MeasDataDSrcList;
typedef QList<measStatData*>  MeasStatDataList;

class CMeasValueItem
{
public:
    CMeasValueItem(CMeasItem item);

public:
    QString& getCurr(void)   { return m_Curr;}
    QString& getAvg(void)    { return m_Avg;}
    QString& getMax(void)    { return m_Max;}
    QString& getMin(void)    { return m_Min;}
    QString& getDev(void)    { return m_Dev;}
    ulong    getCnt(void)    { return m_Cnt;}

    MeasIndicator getAx(void) { return m_Ax;}
    MeasIndicator getBx(void) { return m_Bx;}
    MeasIndicator getAy(void) { return m_Ay;}
    MeasIndicator getBy(void) { return m_By;}

    double   getCurrValue()  { return m_CurrValue;}
    QString& getUnit(void)   { return m_Curr;}

    void    setValueFromList(MeasStatDataList *ptr);
    void    setValue(measStatData *ptr);

    void    setCurr(float val);
    void    setCurr(measData *pMeasData);
    void    setAvg(float val);
    void    setMax(float val);
    void    setMin(float val);
    void    setDev(float val);
    void    setCnt(ulong val);

    void    measUiFormatter(QString &str, float val, bool bUnit);

    void    unitInit(void);
    bool    isUnitS();
    bool    isUnitHz();
    bool    isUnitPercent();
    bool    isUnitCh();
    bool    isUnitCh2();
    bool    isUnitChS();
    bool    isUnitChDivS();
    bool    isUnitDegree();
    bool    isUnitA();
    bool    isUnitW();
    bool    isUnitVA();
    bool    isUnitVAR();

    CMeasItem *getItemInfo() { return &m_Item;}
    bool itemExist(CMeasItem &item);
    void    setSrcA(Chan s);
    void    setSrcB(Chan s);

private:
    CMeasItem m_Item;

    double    m_CurrValue;
    QString   m_Curr;
    QString   m_Avg;
    QString   m_Max;
    QString   m_Min;
    QString   m_Dev;//deviation .
    ulong     m_Cnt;
    Unit      m_Unit;

    MeasIndicator m_Ax;
    MeasIndicator m_Bx;
    MeasIndicator m_Ay;
    MeasIndicator m_By;

    MeasStatus m_Status;
};

#endif // MEASITEM

