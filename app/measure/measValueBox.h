#ifndef CMEASVALUEBOX_H
#define CMEASVALUEBOX_H

#include <QWidget>
#include "cmeasValueItem.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/menustyle/rinfownd_style.h"

#define CHOOSE_COLOR 0xff8200
//#define CHOOSE_COLOR_BG (QColor(255,199,142,100))
//#define CHOOSE_COLOR_BG (QColor(50,30,10,255))

using namespace menu_res;
class CMeasValueBox : public uiWnd
{
    Q_OBJECT
public:
    explicit CMeasValueBox(QWidget *parent = 0);

    void setupUI(QWidget* parent);    
    void updateMeasValue(QList<CMeasValueItem *> *pData);

    void setStatShow(bool b );

    void setChooseRow(int row);
    void setChoosed(bool b);

     void rst();

    static menu_res::RInfoWnd_Style _style;

private slots:
    void activeTimeOut();

protected:
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString&);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent * event);

    void tuneInc( int keyCnt );
    void tuneDec( int keyCnt );
    void tuneZ( int keyCnt );

signals:
    void rowChoosed(int);
    void delItemSig();
    void raiseValueBox();

public slots:
    void delItem();

private:
    QPushButton *delButton;
    QList<CMeasValueItem*> *m_MeasItems;

    int  m_nBoxX;
    int  m_nBoxY;
    int  m_nBoxW;
    int  m_nBoxH;
    int  m_nBoxH2;

    int  m_nRowW;

    bool isStatShow;


    int  m_nRowH;

    bool isActive;
    int  mChooseRow;

    bool isMeasAppActive;

};

#endif // CMEASVALUEBOX_H
