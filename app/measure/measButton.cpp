#include "measButton.h"
#include "measItemBox.h"
#include "../../meta/crmeta.h"

CMeasStartButton::CMeasStartButton()
{
}

CMeasStartButton::~CMeasStartButton()
{

}

void CMeasStartButton::paintEvent( QPaintEvent *event )
{
    QPainter painter(this);
    if( bActive )
    {
        if(isDown())
        {
            //image.load(":/pictures/measure/meas_down.bmp");
            painter.drawImage( 6, 3, mMeasActiveDown );
        }
        else
        {
            //image.load(":/pictures/measure/meas_enable.bmp");
            painter.drawImage( 6, 3, mMeasActive );
        }
    }
    else
    {
        if(isDown())
        {
            //image.load(":/pictures/measure/meas_down.bmp");
            painter.drawImage( 6, 3, mMeasDown );
        }
        else
        {
            //image.load(":/pictures/measure/meas_enable.bmp");
            painter.drawImage( 6, 3, mMeasEnable );
        }
    }
    event->ignore();
}

void CMeasStartButton::loadResource( const QString &strPath )
{

    r_meta::CRMeta rMeta;
    QString s;

    rMeta.getMetaVal( strPath + "mMeasEnable", s );
    mMeasEnable.load(s);

    rMeta.getMetaVal( strPath + "mMeasDown", s );
    mMeasDown.load(s);

    rMeta.getMetaVal( strPath + "mMeasActive", s );
    mMeasActive.load(s);

    rMeta.getMetaVal( strPath + "mMeasActiveDown", s );
    mMeasActiveDown.load(s);

    rMeta.getMetaVal( strPath + "x", mX );
    rMeta.getMetaVal( strPath + "y", mY );
    rMeta.getMetaVal( strPath + "width", mWidth );
    rMeta.getMetaVal( strPath + "height", mHeight );


//
//    rMeta.getMetaVal( strPath + "style", s );
//    this->setStyleSheet(s);

    this->setGeometry( mX, mY, mWidth, mHeight );
}

void CMeasStartButton::setActive(bool a)
{
    bActive = a;
    update();
}

//bool CMeasStartButton::event(QEvent *event)
//{
//    if ( doHelp( event ) )
//    {
//        event->accept();
//        return true;
//    }
//    return QPushButton::event(event);
//}



CMeasItemButton::CMeasItemButton(QWidget *parent) : QPushButton(parent)
{
    m_ItemID    = 0;
    m_Item      = NULL;    

    m_bSelected = false;
    m_bPressed  = false;

    connect(this, SIGNAL(released()), this, SLOT(onClicked()));
}

CMeasItemButton::~CMeasItemButton()
{
    if ( NULL != m_Item )
    { delete m_Item; }
}

void CMeasItemButton::setItemID(int n)
{
    m_ItemID  = n;
    m_Item = new CMeasItem( (MeasType)n);
    Q_ASSERT( NULL != m_Item );
}

void CMeasItemButton::setSrcA(Chan a)
{
    m_Item->setSrcA(a);
}

void CMeasItemButton::setSrcB(Chan b)
{
    m_Item->setSrcB(b);
}

void CMeasItemButton::retranslateUi()
{
    m_Item->getIDSName();
}

void CMeasItemButton::setIDS(int ids)
{
    if( m_Item )
    {
        m_Item->setIDS( ids );;
    }
}

//void CMeasItemButton::setItem( MeasType type)
//{
////    m_Item = new CMeasItem(type);
////    Q_ASSERT( NULL != m_Item );
//}

void CMeasItemButton::onClicked()
{
    emit btnClick(this);
}

void CMeasItemButton::enterEvent(QEvent *)
{
    setFocus();
    emit focusIn(m_Item->mType);
}

void CMeasItemButton::mousePressEvent(QMouseEvent *e)
{
    m_bPressed = true;
    update();
    QPushButton::mousePressEvent(e);
}

void CMeasItemButton::mouseReleaseEvent(QMouseEvent *e)
{
    m_bPressed = false;
    update();
    QPushButton::mouseReleaseEvent(e);
    setSelected(true);
}

void CMeasItemButton::paintEvent( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QRect rLeftBox = this->rect();//0 0 160 40

    rLeftBox.setWidth(rLeftBox.width()-3);
    rLeftBox.setHeight(rLeftBox.height()-3);
    painter.setRenderHint(QPainter::Antialiasing, true);

    if(m_bPressed == false)
    {
        if( (this->isSelected())&&(this->isEnabled()) )
        {
            painter.setPen(QPen(m_Item->mSrcAColor,2,Qt::SolidLine));
        }
        else
        {
            painter.setPen(QPen(QColor(20,20,20,255),2,Qt::SolidLine));
        }

        if(m_Item->isDsrcType())
        {
            painter.drawRoundRect(rLeftBox,10,50);
        }
        else
        {
            painter.drawRoundRect(rLeftBox,20,40);
        }
    }
    else
    {
        QPainterPath painterPath;
        painter.setPen(Qt::transparent);
        painter.setBrush(QBrush(m_Item->mSrcAColor));
        painterPath.addRoundedRect(rLeftBox, 4, 4);
        painter.drawPath(painterPath);
    }



    QFont font;
    font.setPointSize(12);
    painter.setFont(font);
    QRect r = rect();
    r.setX(0);
    r.setY(0);
    r.setWidth(88);
    int textHeight = 40;
    r.setHeight(textHeight);
    if(this->isSelected() )
    {
        painter.setPen(QColor(255,255,255,255));
    }
    else
    {
        painter.setPen(QColor(150,150,150,255));
    }

    if( !(this->isEnabled()) )
    {
        painter.setPen(DISABLE_TEXT_COLOR);
    }

    if( m_bPressed )
    {
        painter.setPen(QColor(0,0,0,255));
    }

    if(m_Item->isDsrcType())
    {
        QRect rName = r;
        rName.setX(40);
        rName.setWidth(160);

        //Draw MeasItem Name
        if( isSelected() || m_bPressed)
        {
        }
        else
        {
            painter.setPen(QPen(m_Item->mSymbolColor,2,Qt::SolidLine));
        }

//        QString nameTemp = m_Item->getIDSName() + "(";
        QString nameTemp = m_Item->mName + "(";

        painter.drawText(rName, Qt::AlignLeft | Qt::AlignVCenter, nameTemp);

        QRect   nameRect, rectType1, rectType_2;
        QFontMetrics fm(font);
        nameRect  = fm.boundingRect(rName,Qt::TextWrapAnywhere, nameTemp);

        QString str = nameTemp+m_Item->mSrcAStr;

        rectType1 = fm.boundingRect(rName,Qt::TextWrapAnywhere, str);
        str.append( m_Item->mSrcBStr);

        rectType_2 = fm.boundingRect(rName,Qt::TextWrapAnywhere, str);

        if( m_bPressed )
        {
        }
        else
        {
            painter.setPen(QPen(m_Item->mSrcAColor,2,Qt::SolidLine));
        }

        painter.drawText(nameRect.x()+nameRect.width(), rName.y(), nameRect.width(), rName.height(),
                         Qt::AlignLeft | Qt::AlignVCenter, m_Item->mSrcAStr);

        QImage imgA;
        imgA.load(m_Item->mSrcAEdgeIcon);
        if( m_bPressed )
        {
            RControlStyle::renderBg(imgA, Qt::black, Qt::white);
        }
        painter.drawImage(rectType1.x()+rectType1.width()+1, rName.y()+16, imgA);

        if( m_bPressed)
        {
        }
        else
        {
            painter.setPen(QPen(m_Item->mSymbolColor,1,Qt::SolidLine));
        }

        painter.drawText(rectType1.x()+rectType1.width()+3+imgA.width(),rName.y(),rectType1.width(),rName.height(),
                         Qt::AlignLeft | Qt::AlignVCenter, "-");

        if( m_bPressed)
        {
        }
        else
        {
            painter.setPen(QPen(m_Item->mSrcBColor,1,Qt::SolidLine));
        }
        painter.drawText(rectType1.x()+rectType1.width()+11+imgA.width(),rName.y(),rectType1.width(),rName.height(),
                         Qt::AlignLeft | Qt::AlignVCenter, m_Item->mSrcBStr);

        QImage imgB;
        imgB.load(m_Item->mSrcBEdgeIcon);
        if( m_bPressed )
        {
            RControlStyle::renderBg(imgB, Qt::black, Qt::white);
        }
        painter.drawImage(rectType_2.x()+rectType_2.width()+12+imgA.width(), rName.y()+16, imgB);

        if( m_bPressed)
        {
        }
        else
        {
            painter.setPen(QPen(m_Item->mSymbolColor, 1, Qt::SolidLine));
        }

        painter.drawText(rectType_2.x()+rectType_2.width()+12+imgA.width()+imgB.width(), rName.y(), rectType_2.width(), rName.height(),
                         Qt::AlignLeft | Qt::AlignVCenter, ")");

    }else
    {
//        painter.drawText(r, Qt::AlignCenter, m_Item->getIDSName());
        painter.drawText(r, Qt::AlignCenter, m_Item->mName);
    }

}
