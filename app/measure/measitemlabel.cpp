#include "measitemlabel.h"
#include "../../meta/crmeta.h"
MeasItemLabel::MeasItemLabel(CMeasValueItem *cMeasItem, QWidget *parent):QLabel(parent)
{
    mCMeasItem = cMeasItem;
    mInited = false;

    loadResource("measure/");
}

void MeasItemLabel::loadResource( const QString &path)
{
    if ( mInited ) return;

    r_meta::CRMeta rMeta;
    rMeta.getMetaVal( path + "meas_spliter", mSpliter );

    mInited = true;
}

void MeasItemLabel::paintEvent(QPaintEvent *event)// 21  add color  ch_color  white
{
    QPainter painter(this);
    painter.fillRect(0,0,152,72,Qt::black);

    QImage img;
    img.load(mCMeasItem->getItemInfo()->mHelpPic);
    painter.drawImage(10,5,img);

    img.load(mSpliter);
    painter.drawImage(0,68,img);

    QPen     pen;
    pen.setColor(mCMeasItem->getItemInfo()->mSymbolColor);
    painter.setPen(pen);

    painter.drawText(70,25,mCMeasItem->getItemInfo()->mName);
    painter.drawText(80,55,mCMeasItem->getCurr());

    event->ignore();
}

