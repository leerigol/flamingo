#ifndef MEASBUTTON
#define MEASBUTTON

//#include "../../menu/rmenus.h"
//#include <QtCore>
#include <QtWidgets>
#include "../../include/dsotype.h"
#include "../../interface/ihelp/ihelp.h"
#include "cmeasitem.h"
using namespace DsoType;

#define DISABLE_TEXT_COLOR (QColor(66,65,66,255))


class CMeasStartButton : public QToolButton, public IHelp
{
public:
    CMeasStartButton();
    virtual ~CMeasStartButton();

public:
    void paintEvent( QPaintEvent *event );
    void loadResource( const QString &path );
    void setActive( bool a = true );

public:
    QImage mMeasEnable;
    QImage mMeasDown;
    QImage mMeasActive;
    QImage mMeasActiveDown;

    int mX, mY;
    int mWidth;
    int mHeight;

protected:
//    virtual bool event( QEvent *event );

    bool bActive;

};

class CMeasItemButton : public QPushButton
{
     Q_OBJECT
public:
    CMeasItemButton( QWidget *parent = 0 );
    virtual ~CMeasItemButton();
public:
    void setItemID(int n);
    int  getItemID(void)            { return m_ItemID;}

    void setSelected(bool s)        { m_bSelected = s;}
    bool isSelected()               {return m_bSelected;}

    void setSrcA(Chan a);
    void setSrcB(Chan b);

    void setIDS(int ids);
    void retranslateUi();

//    void setItem( MeasType type);
    CMeasItem *getItemInfo()        { return m_Item;}
    QString    getItemPic()         { return m_Item->mHelpPic;}

protected:
    void paintEvent( QPaintEvent *event );
    void enterEvent(QEvent *);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);

Q_SIGNALS:
    void btnClick(CMeasItemButton *);
    void focusIn(MeasType);

public slots:
    void onClicked();

private:
    int        m_ItemID;
    CMeasItem *m_Item;
    bool       m_bPressed;
    bool       m_bSelected;
};

#endif // MEASBUTTON

