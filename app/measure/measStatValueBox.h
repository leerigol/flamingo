#ifndef MEASVAL
#define MEASVAL

#include "cmeasValueItem.h"
#include "measHistBox.h"
#include "../../menu/menustyle/rinforowlist_style.h"

using namespace menu_res;
class CMeasStatValueBox : public uiWnd
{
    Q_OBJECT

public:
    explicit CMeasStatValueBox(QWidget *parent = 0);

    void setupUI(QWidget* parent);
    void delAllItem(void);
    void updateMeasValue(QList<CMeasValueItem *> *pData);
    void setChooseRow(int row);

    void setChoosed(bool b);

signals:
    void delItemSig();
    void raiseValueBox();

public slots:
     void measHistShow();

protected:
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString&);
//    void mousePressEvent(QMouseEvent * event);
//    void mouseMoveEvent(QMouseEvent *event);
//    void mouseReleaseEvent(QMouseEvent *);

//    void tuneInc( int keyCnt );
//    void tuneDec( int keyCnt );
//    void tuneZ( int keyCnt );

private:
    QList<CMeasValueItem*> *m_MeasItems;
    CMeasHistBox*      mMeasHistBox;

    static menu_res::RInfoRowList_Style _style;

private:
    bool isMove;
    int	xOffset;
    int	yOffset;

    int  m_nBoxX;
    int  m_nBoxY;
    int  m_nBoxDefaultY;
    int  m_nBoxW;
    int  m_nBoxH;
    int  m_nBoxDefaultH;

    int  m_nRowH;

    bool isActive;
    int  mChooseRow;

    int  mRowNum;
};
#endif // MEASVAL

