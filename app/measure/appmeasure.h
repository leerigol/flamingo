#ifndef APPMEASURE_H
#define APPMEASURE_H

#include "measItemBox.h"
#include "measAllBox.h"
#include "measValueBox.h"
#include "../appmain/cappmain.h"
#include "../apphelp/iapphelp.h"
#include "../appcursor/ccursorline.h"

#define INDICATOR_COLOR   0xff8d47
#define CURSOR_TIMEOUT 1000   //! Cursor(Range) Timeout: 1s

class CAppMeasure : public CApp,public IAppHelp
{
    Q_OBJECT

    DECLARE_CMD()
public:
    enum ListenerEvents
    {
        cmd_base = CMD_APP_BASE,
        cmd_meas_opt_active,
        cmd_meas_opt_exp,
        cmd_meas_opt_invalid,
    };

    CAppMeasure();
    ~CAppMeasure();

public:
    void setupUi( CAppMain *pMain );
    void retranslateUi();
    void buildConnection();
    void registerSpy();

    void onZoomOn();
    int  onActive();

    int  onEnterSubMenu(int);
    int  onReturnMainMenu(int);
    void setFocus();
    void onIndicator();
    void onRegionSet();
    void onRangeABx();
    void onShowThresholdPos();
    void setIndicator(bool on, CMeasValueItem *valItem);

    void exitActive();

    void onShowItemBox();
    void setSrcA();
    void setSrcB();
    int  setCategory(AppEventCause);
    int  setAnalyze(AppEventCause);
//    void setAddItem();


    void delAllItem();

    void onShowAll();
    void onRst();
    void onStartup();

    void testtest();
    void setMeasHistShow();
    void setStatBoxShow();

    void updateMeas();
    void addValueItemRaw(CMeasItem item, bool fromCmd);
    void cmd_AddValueItem();

    void cmd_SetSelItem();

    void onOptDetect();
    void cmd_opt_active();
    void cmd_opt_exp();
    void cmd_opt_invalid();

public Q_SLOTS:
    void onClicked();
    void addValueItem(CMeasItem *item);
    void setChooseRow(int index);//////////////////////
    void delItem();//

    void raiseItemBox();
    void raiseAllBox();
    void raiseValueBox();
    void raiseStatBox();

    void onRangeTimeout();
    void onThTimeout();

private:
    const static int MAX_COUNT = 10;
    QString             m_pBtnStyle, m_pBtnStyleOpen;

    /* 所有可选择的测量项窗口 */
    CMeasItemBox*       m_pMeasItemBox;

    /* 当前选择的测量项窗口 */
    CMeasValueBox*      m_pValueBox;

    /* 当前选择的统计测量项窗口 */
    CMeasStatValueBox*  m_pStatValueBox;

    /* 所有测量项窗口 */
    CMeasAllBox*        m_pAllBox;

    CMeasStartButton*   m_pStartButton;

    /* 所打开的测量结果列表 */
    QList<CMeasValueItem*>   m_MeasItems;
    MeasType            m_MeasType;
    bool                m_IndicatorStatus;

    int  mChooseRow;
    RegionType m_Region;    //! Meas Region set

    bool m_ZoomOn;
    //! Meas Indicator
    //! Main Indicator
    cursor_ui::CCursorLine *m_pIdcAx_Main;
    cursor_ui::CCursorLine *m_pIdcAy_Main;
    cursor_ui::CCursorLine *m_pIdcBx_Main;
    cursor_ui::CCursorLine *m_pIdcBy_Main;
    //! Zoom Indicator
    cursor_ui::CCursorLine *m_pIdcAx_Zoom;
    cursor_ui::CCursorLine *m_pIdcAy_Zoom;
    cursor_ui::CCursorLine *m_pIdcBx_Zoom;
    cursor_ui::CCursorLine *m_pIdcBy_Zoom;

    //! Meas Range Cursor
    cursor_ui::CCursorLine *m_pRangeAx;
    cursor_ui::CCursorLine *m_pRangeBx;
    QTimer *m_pRangeTimer;

    //! Meas Threshold Cursor
    cursor_ui::CCursorLine *m_pThHighY_Main;  //! High Threshold Cursor(Main)
    cursor_ui::CCursorLine *m_pThMidY_Main;   //! Mid Threshold Cursor(Main)
    cursor_ui::CCursorLine *m_pThLowY_Main;   //! Low Threshold Cursor(Main)

    cursor_ui::CCursorLine *m_pThHighY_Zoom;  //! High Threshold Cursor(Zoom)
    cursor_ui::CCursorLine *m_pThMidY_Zoom;   //! Mid Threshold Cursor(Zoom)
    cursor_ui::CCursorLine *m_pThLowY_Zoom;   //! Low Threshold Cursor(Zoom)
    QTimer *m_pThTimer;
};
#endif // APPMEASURE_H

