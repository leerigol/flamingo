#ifndef MEASITEMLABEL_H
#define MEASITEMLABEL_H

#include "cmeasValueItem.h"
#include <QLabel>
#include <QWidget>
class MeasItemLabel : public QLabel
{
public:
    MeasItemLabel(CMeasValueItem *cMeasItem, QWidget *parent = 0);

    void loadResource(const QString &root);
    void paintEvent(QPaintEvent *event);

public:
//    QRect mBoxRect;//

    CMeasValueItem *mCMeasItem;
    QString mSpliter;

    bool mInited;
    QColor mColor;// _ch  white
};

#endif // MEASITEMLABEL_H
