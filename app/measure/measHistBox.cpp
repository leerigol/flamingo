#include <QPainter>
#include "measHistBox.h"
#include "../../meta/crmeta.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"

int       CMeasHistBox::waveMaxWidth = 432;
double    CMeasHistBox::waveMaxHight = 88;

CMeasHistBox::CMeasHistBox(QWidget *parent) :
    QWidget(parent)
{
    mChooseNum = 0;
    itemNum = 0;

    for(int i=0; i<12; i++){
        valueNums[i]=0;
        mListMaxValues[i]=0;
        mListMinValues[i]=0;
        isFirstTime[i] = true;
    }

    valueLists = new double*[12];

    startPoint.setX(85);
    startPoint.setY(20+88);
}

CMeasHistBox::~CMeasHistBox()
{
    delAllItem();
}

void CMeasHistBox::setupUi(QWidget *parent)
{
    loadResource("measure/");
    setParent(parent);
}

void CMeasHistBox::loadResource(const QString &path)
{
    r_meta::CRMeta meta;
    QString strHistBack = path + "hist_back/" ;

    meta.getMetaVal(strHistBack + "box_w", mBoxW);
    meta.getMetaVal(strHistBack + "box_h", mBoxH);

    meta.getMetaVal(strHistBack + "x", mRectX);
    meta.getMetaVal(strHistBack + "y", mRectY);
    meta.getMetaVal(strHistBack + "w", mRectW);
    meta.getMetaVal(strHistBack + "h", mRectH);

    this->setGeometry(mRectX, mRectY, mRectW, mRectH);

    int grayColor,blueColor;
    meta.getMetaVal(strHistBack + "gray27", grayColor);
    meta.getMetaVal(strHistBack + "blue", blueColor);
    setColor(gray27, grayColor);
    setColor(blue, blueColor);

    meta.getMetaVal(strHistBack + "mark", mMark);
    meta.getMetaVal(strHistBack + "topleft", mTopLeft);
    meta.getMetaVal(strHistBack + "topright", mTopRight);
    meta.getMetaVal(strHistBack + "top", mTop);
    meta.getMetaVal(strHistBack + "left", mLeft);
    meta.getMetaVal(strHistBack + "right", mRight);
    meta.getMetaVal(strHistBack + "bottom", mBottom);
    meta.getMetaVal(strHistBack + "bottomleft", mBottomLeft);
    meta.getMetaVal(strHistBack + "bottomright", mBottomRight);
}

void CMeasHistBox::paintEvent(QPaintEvent *event)
{
    mRectX = mRectY = 0;
    QPainter painter(this);
    QImage img, imgTopLeft, imgBottomRight;
    QImage imgTopRight, imgBottomLeft;

    imgTopLeft.load(mTopLeft);
    int widthTemp1 = imgTopLeft.width();//topleft 17 17
    int heightTemp1 = imgTopLeft.height();

    imgTopRight.load(mTopRight);
    int widthTemp2 = imgTopRight.width();//topright 4 4
    int heightTemp2 = imgTopRight.height();

    imgBottomLeft.load(mBottomLeft);
    int widthTemp3 = imgBottomLeft.width();//bottomleft 4 4
    int heightTemp3 = imgBottomLeft.height();

    imgBottomRight.load(mBottomRight);
    int widthTemp4 = imgBottomRight.width();//bottomright 14 14
    int heightTemp4 = imgBottomRight.height();
    img.load(mTop);
    for( int i = 0; i<mRectW-widthTemp1-widthTemp2; i++ ){
        painter.drawImage(mRectX+widthTemp1+i, mRectY, img );
    }
    img.load(mLeft);
    for( int i = 0; i<mRectH-heightTemp1-heightTemp3; i++ ){
        painter.drawImage(mRectX, mRectY+heightTemp1+i, img);
    }
    img.load(mBottom);
    for(int i = 0; i < mRectW-widthTemp3-widthTemp4; i++){
        painter.drawImage(mRectX+widthTemp3+i, mRectY+mRectH-5, img);
    }
    img.load(mRight);
    for(int i = 0; i < mRectH-heightTemp2-heightTemp4; i++){
        painter.drawImage(mRectX+mRectW-widthTemp2, mRectY+heightTemp2+i, img);
    }
    QPen     pen;
    pen.setColor(gray27);
    painter.setPen(pen);

    painter.fillRect(mRectX+widthTemp2, mRectY+heightTemp2,mRectW-widthTemp2*2,mRectH-heightTemp2*2,gray27);

    painter.drawImage(mRectX, mRectY, imgTopLeft);
    painter.drawImage(mRectX+mRectW-widthTemp2, mRectY, imgTopRight);//
    painter.drawImage(mRectX, mRectY+mRectH-heightTemp3-1, imgBottomLeft);
    painter.drawImage(mRectX+mRectW-widthTemp4, mRectY+mRectH-heightTemp4-1, imgBottomRight);

//    QRect rect;
//    rect.setRect(80,20,432,88);
    pen.setColor(blue);
    painter.setPen(pen);

    CMeasValueItem *   mMeasItem;
    mMeasItem = list_at( mListMeasItems, mChooseNum );

    painter.drawText(6, 15, 78, 29, Qt::AlignLeft |Qt::TextWrapAnywhere| Qt::AlignVCenter, mMeasItem->getItemInfo()->mName);

//    painter.drawText(6, 15, 70, 29, Qt::AlignLeft | Qt::AlignVCenter, mMeasItem->getName());// Max
    pen.setColor(Qt::white);
    painter.setPen(pen);
    painter.drawText(6, 55, 50, 12, Qt::AlignLeft | Qt::AlignVCenter, "Cnt:");
    painter.drawText(6, 85, 50, 12, Qt::AlignLeft | Qt::AlignVCenter, "Time:");

    QString stringAve;
    float ave = float((mListMaxValues[mChooseNum]+mListMinValues[mChooseNum])/2.0);

    painter.drawText(535, 15, 60, 12, Qt::AlignLeft | Qt::AlignVCenter, mMeasItem->getMax());//

    gui_fmt::CGuiFormatter::format( stringAve, float((mListMaxValues[mChooseNum]+ave)/2.0) );
    painter.drawText(535, 36, 60, 12, Qt::AlignLeft | Qt::AlignVCenter, stringAve + mMeasItem->getUnit());

    gui_fmt::CGuiFormatter::format( stringAve, ave );
    painter.drawText(535, 57, 60, 12, Qt::AlignLeft | Qt::AlignVCenter, stringAve + mMeasItem->getUnit());//

    gui_fmt::CGuiFormatter::format( stringAve, float((ave+mListMinValues[mChooseNum])/2.0) );
    painter.drawText(535, 78, 60, 12, Qt::AlignLeft | Qt::AlignVCenter, stringAve + mMeasItem->getUnit());

    painter.drawText(535, 99, 60, 12, Qt::AlignLeft | Qt::AlignVCenter, mMeasItem->getMin());

    painter.fillRect(85, 20, 432, 88,Qt::black );

    pen.setColor(QColor(68,68,68,255));
    painter.setPen(pen);
    painter.drawLine(84, 19, 518, 19);
    painter.drawLine(84, 109, 518, 109);
    painter.drawLine(518, 19, 518, 109);
    painter.drawLine(84, 19, 84, 109);

    pen.setBrush(QBrush(QColor(40,40,40,255)));
    QVector<qreal> dashes;
    qreal space = 1;
    dashes << 2 << space << 1 <<space << 2 << space;
    pen.setDashPattern(dashes);
    pen.setWidth(1);
    painter.setPen(pen);

    for(int i=0; i<7; i++){
        painter.drawLine(85+54*(i+1), 20, 85+54*(i+1), 108);
    }
    for(int i=0; i<3; i++){
        painter.drawLine(85, 20+22*(i+1), 512, 20+22*(i+1));
    }
    img.load(mMark);
    painter.drawImage(298, 16, img);

    painterWave();  //    wave draw

    event->ignore();
}

void CMeasHistBox::removeAt(int num)
{
    for(int i=num; i<itemNum-1; i++){
        for(int j=0; j<432; j++)
            valueLists[i][j] = valueLists[i+1][j];

        isFirstTime[i] = isFirstTime[i+1];
        valueNums[i] = valueNums[i+1];
        mListMaxValues[i] = mListMaxValues[i+1];
        mListMinValues[i] = mListMinValues[i+1];
    }
    valueNums[itemNum-1] = 0;
    mListMaxValues[itemNum-1] = 0;
    mListMinValues[itemNum-1] = 0;
    isFirstTime[itemNum-1] = true;
}

void CMeasHistBox::delAllItem()
{
    for(int i=0; i<itemNum; i++)
        delete []valueLists[i];

    for(int i=0; i<12; i++){
        valueNums[i]=0;
        mListMaxValues[i]=0;
        mListMinValues[i]=0;
        isFirstTime[i] = true;
    }
    itemNum = 0;
}

void CMeasHistBox::compressWave(double *tPointIn, int length, int width, int m,int n, double *tPointOut )
{
    struMem mem;
    struView view;

    //! memory
    mem.setLength( length );
    mem.setCenter( length/2 );
    //! view
    view.setWidth( width );
    //view.setOffset( vOffs );
    view.setRatio( m, n );

    reSample( &mem, &view, tPointIn, tPointOut );
}

void CMeasHistBox::addItemNum()
{
    for(int i=0; i<12; i++){
        if(itemNum == i){
            valueLists[i] = new double[432];
            break;
        }
    }
    itemNum++;
}
void CMeasHistBox::updateValueNum(QList<CMeasValueItem *> list, int num)
{
    mListMeasItems = list;
    mChooseNum = num;
}
void CMeasHistBox::updateValue(QList<CMeasValueItem *> list, int num)
{
    mListMeasItems = list;
    mChooseNum = num;

    for(int i=0; i<list.count(); i++){

        if(list[i]->getCurr() == "-")
            return;
        //add value
        double tempValue = list[i]->getCurrValue();

        if(isFirstTime[i]){
            isFirstTime[i] = false;
            mListMaxValues[i] = tempValue;
            mListMinValues[i] = tempValue;
        }else{
            mListMaxValues[i] = mListMaxValues[i] > tempValue ? mListMaxValues[i]:tempValue;
            mListMinValues[i] = mListMinValues[i] < tempValue ? mListMinValues[i]:tempValue;
        }

        if(valueNums[i] < 432){
            valueLists[i][valueNums[i]] = tempValue;
            valueNums[i] = valueNums[i] + 1;
        }else{

            double output[432] = {0};
            int len = 432, width = 432, lm=432, ln=431;

            compressWave(valueLists[i], len, width, lm, ln, output );

            for(int w=0; w<valueNums[i]-1; w++)
                valueLists[i][w] = output[w];
            valueLists[i][valueNums[i]-1] = tempValue;/////////////
        }
    }
}

void CMeasHistBox::painterWave()
{
    QPainter painter(this);
    QPen pen;
    pen.setColor(Qt::white);
    pen.setWidth(1);

    if(mListMaxValues[mChooseNum]-mListMinValues[mChooseNum]){
//        painter.drawLine(startPoint.x(), startPoint.y(),
//                         startPoint.x()+1, startPoint.y() - (valueLists[mChooseNum][1]-mListMinValues[mChooseNum])/(mListMaxValues[mChooseNum]-mListMinValues[mChooseNum])*88);
        for(int i=1; i<valueNums[mChooseNum]-1; i++){
            painter.drawLine(startPoint.x()+i, startPoint.y() - (valueLists[mChooseNum][i]-mListMinValues[mChooseNum])/(mListMaxValues[mChooseNum]-mListMinValues[mChooseNum])*88,
                             startPoint.x()+i+1, startPoint.y() - (valueLists[mChooseNum][i+1]-mListMinValues[mChooseNum])/(mListMaxValues[mChooseNum]-mListMinValues[mChooseNum])*88);
        }
    }else{
        for(int i=0; i<valueNums[mChooseNum]-1; i++){
            painter.drawLine(startPoint.x()+i, startPoint.y(), startPoint.x()+i+1, startPoint.y() );
        }
    }
}

void CMeasHistBox::setColor( QColor &color, int hexRgb )
{
    color.setRed( (hexRgb >>16)&0xff );
    color.setGreen( (hexRgb >>8)&0xff );
    color.setBlue( (hexRgb >>0)&0xff );
}


