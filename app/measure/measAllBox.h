#ifndef MEASALLBOX
#define MEASALLBOX

#include "cmeasValueItem.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../menu/menustyle/rinfownd_style.h"
#include "../../widget/rimagebutton.h"

using namespace menu_res;
class CMeasAllBox : public uiWnd
{
    Q_OBJECT

public:
     CMeasAllBox(QWidget *parent = 0);
    ~CMeasAllBox();
     void setupUi(QWidget* parent);
     void retranslateUi();
     bool clearAllItem(void);
     void setMeasSrc(Chan src);
     void updateAllValue(void);
     void rst();

     static menu_res::RModelWnd_Style _style;

public slots:
     void closeAllBox();


protected:
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString&);

private:
    QList<CMeasValueItem*>    m_MeasValue;
    RImageButton   *m_pCloseButton;
    QString m_Title;

private:
    int  m_nBoxX;
    int  m_nBoxY;
    int  m_nBoxW;
    int  m_nBoxH;

    int  m_nRowH;

    QRect mCloseRect;
};

#endif // MEASALLBOX

