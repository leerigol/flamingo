#include "measValueBox.h"
#include "../../meta/crmeta.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<"ValueBox"<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";

#define def_time()
#define start_time()
#define end_time()

menu_res::RInfoWnd_Style CMeasValueBox::_style;
CMeasValueBox::CMeasValueBox(QWidget *parent) : uiWnd(parent)
{
    mChooseRow  = 0;
    m_MeasItems = 0;
    m_nRowW     = 0;

    isActive = false;
    isStatShow = false;
    set_attr( wndAttr, mWndAttr, wnd_moveable );

    this->setObjectName("measvaluebox");

    addTuneKey();
}

void CMeasValueBox::setupUI(QWidget *parent)
{
    loadResource("measure/values/");
    _style.loadResource("ui/wnd/cursor_value/");

    setParent(parent);
    delButton = new QPushButton(this);
    connect(delButton, SIGNAL(clicked(bool)),this,SLOT(delItem()));
    delButton->setStyleSheet("QPushButton{background-image:url(:/pictures/measure/del.png);border:0px;background-repeat:none;}");
    delButton->setGeometry(162,1,28,38);
    delButton->setFocusPolicy(Qt::NoFocus);
    delButton->hide();
}

void CMeasValueBox::rst()
{
    this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);
}

void CMeasValueBox::loadResource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path ;

    meta.getMetaVal(str + "row_h", m_nRowW);//93

    meta.getMetaVal(str + "x", m_nBoxX);//0
    meta.getMetaVal(str + "y", m_nBoxY);//0
    meta.getMetaVal(str + "w", m_nBoxW);//979
    meta.getMetaVal(str + "h", m_nBoxH);//52
    meta.getMetaVal(str + "h2", m_nBoxH2);//52

    if(!isStatShow)
        this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);
    else
        this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH2);
}

void CMeasValueBox::updateMeasValue(QList<CMeasValueItem*> *pData)
{
    m_MeasItems = pData;

    if(!isStatShow){
        this->resize(m_MeasItems->count()*m_nRowW+49, m_nBoxH);
    }else{
        this->resize(m_MeasItems->count()*m_nRowW+49, m_nBoxH2);
    }
    update();
}

void CMeasValueBox::setStatShow(bool b)
{
    if( isStatShow != b)
    {
        isStatShow = b;
        if(!isStatShow){
            if(geometry().y()+m_nBoxH2-m_nBoxH > 480-m_nBoxH)
                this->move(geometry().x(), 480-m_nBoxH);
            else
                this->move(geometry().x(), geometry().y()+m_nBoxH2-m_nBoxH);
            this->resize(m_MeasItems->count()*m_nRowW+49, m_nBoxH);
        }else{
            if(geometry().y()-m_nBoxH2+m_nBoxH < 0 )
                this->move(geometry().x(), 0);
            else
                this->move(geometry().x(),geometry().y()-m_nBoxH2+m_nBoxH);
            this->resize(m_MeasItems->count()*m_nRowW+49, m_nBoxH2);
        }
        update();
    }
}

void CMeasValueBox::mousePressEvent(QMouseEvent * event)
{
    raise();
    if(event->button() & Qt::LeftButton)
    {    
//        if( sysGetMenuWindow()->isVisible())
//        {
//            serviceExecutor::post( E_SERVICE_ID_MEASURE,
//                                   CMD_SERVICE_ACTIVE,
//                                   MSG_APP_MEAS_CLEAR_ALL);
//        }

        if( m_MeasItems->count() > 0 )
        {
            int column = 0;
            while(event->x() < (m_MeasItems->count()-1-column)*m_nRowW+45 )
            {
                if(column < m_MeasItems->count()-1)
                    column++;
                else
                    break;
            }
            setChooseRow(column);
            emit rowChoosed(column);
        }else{
            setChoosed(false);
        }

        if ( !is_attr(mWndAttr, wnd_moveable) )
        {
            QWidget::mousePressEvent( event );
            event->ignore();
            return;
        }

        mptMoveOrig.setX( event->globalPos().rx() - pos().rx() );
        mptMoveOrig.setY( event->globalPos().ry() - pos().ry() );
    }
}

void CMeasValueBox::mouseMoveEvent(QMouseEvent * event)
{
    if ( !is_attr(mWndAttr,wnd_moveable) )
    {
        event->ignore();
        return;
    }

    if (event->buttons() == Qt::LeftButton)
    {
        QPoint pt = event->globalPos();
        QPoint next = pt - mptMoveOrig;
        this->move( next );

        if(geometry().right()<100 || geometry().left()>900)
        {
            next.setX(0);
            next.setY(480-geometry().height());
            this->hide();
        }

        if(geometry().top()>450 || geometry().bottom()<30)
        {
            next.setX(0);
            next.setY(480-geometry().height());
            this->hide();
        }

        this->move( next );
    }
}

void CMeasValueBox::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() & Qt::LeftButton)
    {
        QPoint mpt;
        mpt.setX( event->globalPos().rx() - pos().rx() );
        mpt.setY( event->globalPos().ry() - pos().ry() );

        if( sysGetMenuWindow()->isVisible() && mpt == mptMoveOrig )
        {
            serviceExecutor::post( E_SERVICE_ID_MEASURE,
                                   CMD_SERVICE_ACTIVE,
                                   MSG_APP_MEAS_CLEAR_ALL);
        }
    }
}

void CMeasValueBox::tuneDec(int keyCnt)
{
    Q_UNUSED(keyCnt);
    if(mChooseRow  < m_MeasItems->size()-1){
        setChooseRow(++mChooseRow);
        emit rowChoosed(mChooseRow);
    }else if(mChooseRow >= m_MeasItems->count()-1){
        setChooseRow(m_MeasItems->count()-1);
        emit rowChoosed(mChooseRow);
    }
}

void CMeasValueBox::tuneInc(int keyCnt)
{
    Q_UNUSED(keyCnt);
    if(m_MeasItems->count()>1 && mChooseRow>0 ){
        setChooseRow(--mChooseRow);
        emit rowChoosed(mChooseRow);
    }else if(m_MeasItems->count()>0 && mChooseRow <= 0 ){
        setChooseRow(0);
        emit rowChoosed(mChooseRow);
    }
}

void CMeasValueBox::tuneZ(int keyCnt)
{
    Q_UNUSED(keyCnt);
    if(isActive)
        delItem();
}

void CMeasValueBox::delItem()
{
    emit delItemSig();

    update();
}

void CMeasValueBox::setChooseRow(int row)
{
    mChooseRow = row;
    isActive = true;
    update();
}

void CMeasValueBox::setChoosed(bool b)
{
    isActive = b;
    update();
}

void CMeasValueBox::activeTimeOut()
{
    setChoosed(false);
}

void CMeasValueBox::paintEvent(QPaintEvent */*event*/)
{
    def_time();
    QPainter painter(this);
    QPen pen;

    start_time();
    QColor text_color = QColor(0xc0,0xc0,0xc0);

    QFont font;
    font.setPointSize(9);
    painter.setFont(font);

    pen.setColor(text_color);
    painter.setPen(pen);

    QRect r;
    r.setX(0);
    r.setY(0);
    r.setWidth(geometry().width());
    r.setHeight(geometry().height());

    _style.paint( painter, r );

    QRect rect;
    rect.setX(5);
    rect.setY(5);
    rect.setWidth(39);
    rect.setHeight(22);

    if(isStatShow){
        QRect rec = rect;
        for(int i=0; i<6; i++){
            rec.moveTo(rec.bottomLeft());
            switch ( i ) {
            case 0:
                painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  "Cur:");
                break;
            case 1:
                painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  "Avg:");
                break;
            case 2:
                painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  "Max:");
                break;
            case 3:
                painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  "Min:");
                break;
            case 4:
                painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  "Dev:");
                break;
            case 5:
                painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  "Cnt:");
                break;
            default:
                break;
            }
        }
    }

    QRect rItem = rect;
    rItem.setWidth(m_nRowW+1);
    rItem.setHeight(22);
    rItem.moveTo(rect.topRight());
    for(int i=m_MeasItems->count()-1; i>=0; i--)
    {
        CMeasValueItem *p = list_at((*m_MeasItems),i);

        //Draw MeasItem Name

        QRect rName =rItem;

//        QString itemTitle = p->getItemInfo()->getIDSName()+p->getItemInfo()->mSrcAStr;
        QString itemTitle = p->getItemInfo()->mName+p->getItemInfo()->mSrcAStr;
        //choose
        if(i == mChooseRow)
        {
            if(isActive)
            {
                QRect bgRect = rItem;

//                QFontMetrics fm(font);

//                QRect rec = fm.boundingRect ( itemTitle );
//                if( p->getItemInfo()->isDsrcType() )
//                {
//                    bgRect.setWidth(  rec.width() + 40 );
//                }
//                else
//                {
//                    bgRect.setWidth(  rec.width() + 8);
//                }

                bgRect.setWidth( rItem.width() - 4);
                bgRect.setLeft(bgRect.left()-4);

                bgRect.setHeight(geometry().height()-10);
//                bgRect.setHeight( rec.height());

                painter.fillRect(bgRect,QColor(20,20,20,255));
            }
        }

//        QString nameTemp = p->getItemInfo()->getIDSName();
        QString nameTemp = p->getItemInfo()->mName;
        if(p->getItemInfo()->isDsrcType())
        {
            pen.setColor(p->getItemInfo()->mSymbolColor);
            painter.setPen(pen);

//            nameTemp = p->getItemInfo()->mSymbol;
            painter.drawText(rName, Qt::AlignLeft | Qt::AlignVCenter, nameTemp);
        }else
        {
            pen.setColor(p->getItemInfo()->mSrcAColor);
            painter.setPen(pen);

//            nameTemp = p->getItemInfo()->mName;
            painter.drawText(rName, Qt::AlignLeft | Qt::AlignVCenter, nameTemp);
        }

        QRect   nameRect, rectType1, rectType_2;
        QFontMetrics fm(font);
        nameRect  = fm.boundingRect(rName,Qt::TextWrapAnywhere, nameTemp);

        rectType1 = fm.boundingRect(rName,Qt::TextWrapAnywhere, itemTitle);
        rectType_2 = fm.boundingRect(rName,Qt::TextWrapAnywhere, itemTitle + p->getItemInfo()->mSrcBStr);

        if(p->getItemInfo()->isDsrcType()){
            painter.setPen(QPen(p->getItemInfo()->mSrcAColor,1,Qt::SolidLine));
            painter.drawText(nameRect.x()+nameRect.width(),rName.y(),nameRect.width(),rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mSrcAStr);

            QImage imgA;
            imgA.load(p->getItemInfo()->mSrcAEdgeIcon);
            painter.drawImage(rectType1.x()+rectType1.width()+1, rName.y()+6, imgA);

            painter.setPen(QPen(p->getItemInfo()->mSymbolColor,1,Qt::SolidLine));
            painter.drawText(rectType1.x()+rectType1.width()+3+imgA.width(),rName.y(),rectType1.width(),rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, "-");

            painter.setPen(QPen(p->getItemInfo()->mSrcBColor,1,Qt::SolidLine));
            painter.drawText(rectType1.x()+rectType1.width()+11+imgA.width(),rName.y(),rectType1.width(),rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mSrcBStr);

            QImage imgB;
            imgB.load(p->getItemInfo()->mSrcBEdgeIcon);
            painter.drawImage(rectType_2.x()+rectType_2.width()+12+imgA.width(), rName.y()+6, imgB);

            painter.setPen(QPen(p->getItemInfo()->mSymbolColor, 1, Qt::SolidLine));
//            painter.drawText(rectType_2.x()+rectType_2.width()+12+imgA.width()+imgB.width(),rName.y(), rectType_2.width(), rName.height(),
//                             Qt::AlignLeft | Qt::AlignVCenter, ")");
        }else{
            painter.setPen(QPen(p->getItemInfo()->mSrcAColor,1,Qt::SolidLine));
            painter.drawText(nameRect.x()+nameRect.width(),rName.y(),nameRect.width(),rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, p->getItemInfo()->mSrcAStr);
//            painter.drawText(rectType1.x()+rectType1.width(),rName.y(),rectType1.width(),rName.height(),
//                             Qt::AlignLeft | Qt::AlignVCenter, ")");
        }


        QRect rec = rItem;
        rec.moveTo(rItem.bottomLeft());

//        if( i == mChooseRow)
//        {
//            painter.setPen(QPen(QColor(CHOOSE_COLOR),1,Qt::SolidLine));
//        }
//        else
        {
            painter.setPen(QPen(text_color,1,Qt::SolidLine));
        }
        painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter, p->getCurr());
        if(isStatShow)
        {
            for(int i=0; i<5; i++)
            {
                rec.moveTo(rec.bottomLeft());
                switch ( i ) {
                case 0:
                    painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  p->getAvg() );
                    break;
                case 1:
                    painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  p->getMax() );
                    break;
                case 2:
                    painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  p->getMin() );
                    break;
                case 3:
                    painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  p->getDev() );
                    break;
                case 4:
                    painter.drawText(rec, Qt::AlignLeft | Qt::AlignVCenter,  QString::number(p->getCnt()) );
                    break;
                default:
                    break;
                }
            }
        }

        rItem.moveTo(rItem.topRight());
    }
    end_time();
}
