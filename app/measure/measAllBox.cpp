#include <QPainter>

#include "measAllBox.h"
#include "measItemBox.h"
#include "../../meta/crmeta.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"
#include <QFontMetrics>

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<"AllBox"<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";

#define def_time()
#define start_time()
#define end_time()

menu_res::RModelWnd_Style CMeasAllBox::_style;
CMeasAllBox::CMeasAllBox(QWidget *parent) : uiWnd(parent)
{
    set_attr( wndAttr, mWndAttr, wnd_moveable );
    m_Title = "All Measure";
    setWindowTitle("All Measure");
}

CMeasAllBox::~CMeasAllBox()
{
    clearAllItem();
}

void CMeasAllBox::rst()
{
    this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);
}

void CMeasAllBox::setupUi(QWidget *parent)
{
    setParent(parent);

    loadResource("measure/");
    _style.loadResource("ui/wnd/model/");

    m_pCloseButton = new RImageButton(this);
    Q_ASSERT( m_pCloseButton != NULL );
    m_pCloseButton->setGeometry(mCloseRect);
    m_pCloseButton->setImage( _style.mNormalImg,
                               _style.mDownImg,
                               _style.mDisableImg );

    connect( m_pCloseButton, SIGNAL(clicked()),
             this, SLOT(closeAllBox()) );
}

void CMeasAllBox::retranslateUi()
{
    if( m_MeasValue.size() > 0)
    {
        for( int i=0; i<m_MeasValue.size(); i++)
        {
            list_at(m_MeasValue, i)->getItemInfo()->getIDSName();
        }
    }
    m_Title = sysGetString(MSG_APP_MEAS_ALL_SRC,"All Measure");

    update();
}

void CMeasAllBox::closeAllBox()
{
    close();
    serviceExecutor::post( serv_name_measure, MSG_APP_MEAS_ALL_SRC, (int)chan_none );
}

bool CMeasAllBox::clearAllItem(void)
{
    foreach( CMeasValueItem *pItem, m_MeasValue )
    {
        Q_ASSERT( NULL != pItem );

        delete pItem;
    }

    m_MeasValue.clear();
    return true;
}

void CMeasAllBox::loadResource(const QString &path)
{
    r_meta::CRMeta meta;
    QString strAllValue = path + "all_values/" ;

    meta.getMetaVal(strAllValue + "closer", mCloseRect);

    meta.getMetaVal(strAllValue + "row_h", m_nRowH);//22

    meta.getMetaVal(strAllValue + "x", m_nBoxX);//200
    meta.getMetaVal(strAllValue + "y", m_nBoxY);//120
    meta.getMetaVal(strAllValue + "w", m_nBoxW);//490
    meta.getMetaVal(strAllValue + "h", m_nBoxH);//360

    this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);

    // Meas Horizontal Type
    for(int i=Meas_HOR_TYPE; i<Meas_HOR_TYPE_END; i++)
    {
        CMeasItem item = CMeasItem( (MeasType)i );
        item.setIDS( INF_ITEM_PERIOD + i );
        m_MeasValue.append( new CMeasValueItem( item ) );
    }
    // Meas Vertical Type
    for(int i=Meas_VER_TYPE; i<Meas_VER_TYPE_END; i++)
    {
        CMeasItem item = CMeasItem( (MeasType)i );
        item.setIDS( INF_ITEM_VMAX + i -Meas_VER_TYPE );
        m_MeasValue.append( new CMeasValueItem( item ) );
    }
}

void CMeasAllBox::updateAllValue(void)
{
    def_time();
    void *pData = 0;
    measData* pMeasData = 0;

    serviceExecutor::query( serv_name_measure,
                            servMeasure::MSG_ALL_MEAS_DATA, pData );
    if(pData != 0)
    {
        pMeasData = (measData*)pData;
        for(int i=0; i< m_MeasValue.size(); ++i)
        {
            list_at(m_MeasValue,i)->setCurr(pMeasData);
        }
        update();
    }
}

void CMeasAllBox::setMeasSrc(Chan src)
{
    if(m_MeasValue.size() != 0)
    {
        for(int i=0; i<m_MeasValue.size(); i++)
        {
            m_MeasValue.at(i)->setSrcA(src);
        }
    }
}

void CMeasAllBox::paintEvent(QPaintEvent */*event*/)
{
    def_time();
    start_time();
    int nCount = m_MeasValue.count();

    if(nCount > 0)
    {

        QPainter painter(this);

        QRect r = this->geometry();
        int nWidth  = r.width();
        int nStartX = 15;
        int nStartY = 5;

        r.setX(0);
        r.setY(0);
        r.setWidth(nWidth);
        r.setHeight(m_nBoxH);

        _style.paint( painter, r );

        r.setX(nStartX);
        r.setY(nStartY);
        r.setWidth(nWidth);
        r.setHeight(m_nBoxH);

        QPen pen;
        CMeasValueItem* pFirstItem = m_MeasValue.at(0);

        pen.setColor( pFirstItem->getItemInfo()->mSymbolColor );
        painter.setPen(pen);

        int nRow  = 11;
        int nCol  = nCount/nRow;
        if( (nCount % nRow) != 0)
        {
            nCol ++;
        }

        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop,  m_Title);

        QRect wordRect;
        QFont font;
        QFontMetrics fm(font);
        wordRect = fm.boundingRect(r,Qt::TextWrapAnywhere, m_Title);

        pen.setColor( pFirstItem->getItemInfo()->mSrcAColor );
        painter.setPen(pen);

        QRect rec2  = r;
        rec2.setX(r.x()+wordRect.width());
        rec2.setY(r.y());
        rec2.setHeight(r.height());

        QString str = m_MeasValue.at(0)->getItemInfo()->mSrcAStr;

        painter.drawText(rec2, Qt::AlignLeft | Qt::AlignTop,  "("+str+")");

        nStartY = 40;
        r.setY(nStartY);
        nStartX = nStartX;
        r.setX(nStartX);

        int index = 0;

        pen.setColor( pFirstItem->getItemInfo()->mSymbolColor );
        painter.setPen(pen);


        for(int j=0; j<nCol; j++)
        {
            for(int i=0; i<nRow; i++)
            {
                CMeasValueItem* p = list_at(m_MeasValue,index);

//                painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getItemInfo()->getIDSName());
                painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getItemInfo()->mName);

                QRect rect = r;
                rect.setWidth(nWidth/nCol/5*2+10);

                painter.drawText(rect, Qt::AlignRight | Qt::AlignTop, ":");

                r.setX(r.x()+ (nWidth/nCol)/5*2+13);

                painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getCurr());

                r.setY(r.y() + m_nRowH);
                r.setX(nStartX);

                index++;
                if(index >= nCount)
                {
                    break;
                }
            }

            if(index >= nCount)
            {
                break;
            }
            nStartX += (nWidth-20)/nCol;
            r.setX(nStartX);
            r. setY(nStartY);

        }        
    }
    end_time();
}

