
#ifndef MEASHISTBOX
#define MEASHISTBOX

#include "cmeasValueItem.h"

#include "../../baseclass/resample/reSample.h"

class CMeasHistBox : public QWidget
{
    Q_OBJECT

public:
     CMeasHistBox(QWidget *parent = 0);
    ~CMeasHistBox();
     void setupUi(QWidget* parent);

     void setColor(QColor &color, int hexRgb);

     void addItemNum();

     void updateValue(QList<CMeasValueItem *> list, int num);// a new value

     void updateValueNum(QList<CMeasValueItem *> list, int num);

     void compressWave(double *tPointIn, int length, int width, int m, int n, double *tPointOut);

     void delAllItem();

     void painterWave();

     void removeAt(int num);

protected:
    void paintEvent(QPaintEvent *event);
    void loadResource(const QString&);

private:
    int     mBoxW, mBoxH;
    int     mRectX, mRectY, mRectW, mRectH;

    QColor  gray27, blue;

    QString mMark, mTopLeft, mTopRight, mTop, mLeft;
    QString mRight, mBottom, mBottomLeft, mBottomRight;

    /////////////////////////////
    static int       waveMaxWidth;
    static double    waveMaxHight;

    int             mChooseNum;
    int             itemNum;
    int             valueNums[12];
    bool            isFirstTime[12];
    double          mListMaxValues[12];
    double          mListMinValues[12];
    double          **valueLists;

    QPoint  startPoint;
    QList<CMeasValueItem *>  mListMeasItems;
};

#endif // MEASHISTBOX

