#include "measItemBox.h"
#include "../../meta/crmeta.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"
#include <QListWidgetItem>

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<"ItemBox"<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";


#define def_time()
#define start_time()
#define end_time()

menu_res::RInfoWnd_Style CMeasItemBox::_style;
CMeasItemBox::CMeasItemBox(QWidget *parent) : uiWnd(parent)
{
    this->setObjectName("measitembox");

    addTuneKey();

    m_bShowHelpImage = true;
    set_attr( wndAttr, mWndAttr, wnd_moveable );

    addToShowQueue( this );
    mAlyHasFocus = false;

}

CMeasItemBox::~CMeasItemBox()
{
    CMeasItemButton* pButton;

    int size = m_ItemButtonHor.count();
    for(int i=0; i<size; i++)
    {
        pButton = list_at(m_ItemButtonHor, i);
        delete pButton;
        pButton = NULL;
    }
    m_ItemButtonHor.clear();

    size = m_ItemButtonVer.count();
    for(int i=0; i<size; i++)
    {
        pButton = list_at(m_ItemButtonVer, i);
        delete pButton;
        pButton = NULL;
    }
    m_ItemButtonVer.clear();

    size = m_ItemButtonDsrc.count();
    for(int i=0; i<size; i++)
    {
        pButton = list_at(m_ItemButtonDsrc, i);
        delete pButton;
        pButton = NULL;
    }
    m_ItemButtonDsrc.clear();
}

void CMeasItemBox::setupUi(QWidget *parent)
{
    loadResource("measure/");
    _style.loadResource("measure/");
    this->resize( m_BoxW, m_BoxH-45);
    mHorButton = new QPushButton(this);
    mVerButton = new QPushButton(this);
    mDsrcButton = new QPushButton(this);
    mAlyButton = new QPushButton(this);
    mCloseButton = new QPushButton(this);


    connect(mHorButton,SIGNAL(pressed()),this,SLOT(setHorType()));
    connect(mVerButton,SIGNAL(pressed()),this,SLOT(setVerType()));
    connect(mDsrcButton,SIGNAL(pressed()),this,SLOT(setDsrcType()));
    connect(mAlyButton,SIGNAL(pressed()),this,SLOT(setAlyType()));
    connect(mCloseButton,SIGNAL(pressed()),this,SLOT(close()));

    mHorButton->setGeometry(8,8,87,39);
    mVerButton->setGeometry(96,8,87,39);
    mDsrcButton->setGeometry(185,8,87,39);
    mAlyButton->setGeometry(274,8,87,39);
    mCloseButton->setGeometry(520,9,38,29);

    mHorButton->setFocusPolicy(Qt::TabFocus);
    mVerButton->setFocusPolicy(Qt::TabFocus);
    mDsrcButton->setFocusPolicy(Qt::TabFocus);
    mAlyButton->setFocusPolicy(Qt::TabFocus);
    mCloseButton->setFocusPolicy(Qt::TabFocus);

    mHorButton->setFlat(true);
    mVerButton->setFlat(true);
    mDsrcButton->setFlat(true);
    mAlyButton->setFlat(true);

    mHorButton->setStyleSheet(m_typeSelect);
    mVerButton->setStyleSheet(m_typeNormal);
    mDsrcButton->setStyleSheet(m_typeNormal);
    mAlyButton->setStyleSheet(m_typeNormal);
    mCloseButton->setStyleSheet(m_typeClose);

    mHorButton->setText("Horizontal");
    mVerButton->setText("Vertical");
    mDsrcButton->setText("Other");
    mAlyButton->setText("Analyze");

    mStackedWidget = new QStackedWidget(this);
    mStackedWidget->setGeometry(11,38,390,250);
    QWidget *hWidget = new QWidget(this);
    QWidget *vWidget = new QWidget(this);
    QWidget *dWidget = new QWidget(this);
    QWidget *alyWidget = new QWidget(this);

    mStackedWidget->addWidget(hWidget);
    mStackedWidget->addWidget(vWidget);
    mStackedWidget->addWidget(dWidget);
    mStackedWidget->addWidget(alyWidget);

    int x = m_ItemStartX;
    int y = m_ItemStartY;
    int w = m_ItemW;
    int h = m_ItemH;

    for(int i = Meas_Period; i<Meas_HOR_TYPE_END; i++)
    {
        CMeasItemButton* pItemButton = new CMeasItemButton(hWidget);
        pItemButton->setGeometry(x,y,w,h);
        pItemButton->setItemID(i);
        pItemButton->setIDS( INF_ITEM_PERIOD + i);

        connect(pItemButton,
                SIGNAL(btnClick(CMeasItemButton *)),
                this,
                SLOT(itemClickedSlot(CMeasItemButton *)));

        if((i+1)%4 != 0)
        {
            x += m_ItemW+3;
        }
        else
        {
            x = m_ItemStartX;
            y += m_ItemH+2;
        }
        m_ItemButtonHor.append(pItemButton);
    }
    m_CurrItemButton = m_ItemButtonHor.at(0);
    m_CurrItemButton->setSelected(true);
    m_MeasType = Meas_HOR_TYPE;
    currFocus = MEASBUTTONFOCUS;

    x = m_ItemStartX;
    y = m_ItemStartY;
    w = m_ItemW;
    h = m_ItemH;
    for(int i = 0; i<Meas_VER_TYPE_END-Meas_Vmax; i++)
    {
        CMeasItemButton* pItemButton = new CMeasItemButton(vWidget);
        pItemButton->setGeometry(x,y,w,h);
        pItemButton->setItemID(i+Meas_Vmax);
        pItemButton->setIDS( INF_ITEM_VMAX + i );


        connect(pItemButton,
                SIGNAL(btnClick(CMeasItemButton *)),
                this,
                SLOT(itemClickedSlot(CMeasItemButton *)));

        if((i+1)%4 != 0)
        {
            x += m_ItemW+3;
        }
        else
        {
            x = m_ItemStartX ;
            y = y + m_ItemH + 2;
        }
        m_ItemButtonVer.append(pItemButton);
    }

    x = m_ItemStartX +2;
    y = m_ItemStartY;
    w = m_ItemW+90;
    h = m_ItemH;
    for(int i = 0; i<Meas_DOUBLE_SRC_TYPE_END-Meas_DOUBLE_SRC_TYPE; i++)
    {
        CMeasItemButton* pItemButton = new CMeasItemButton(dWidget);
        pItemButton->setGeometry(x,y,w,h);
        pItemButton->setItemID(i+Meas_DOUBLE_SRC_TYPE);

        if( i < 4 )
        {
            pItemButton->setIDS( INF_ITEM_DELAY );
        }
        else
        {
            pItemButton->setIDS( INF_ITEM_PHASE );
        }

        connect(pItemButton,
                SIGNAL(btnClick(CMeasItemButton *)),
                this,
                SLOT(itemClickedSlot(CMeasItemButton *)));

        if((i+1)%2 != 0)
        {
            x += m_ItemW+94;
        }
        else
        {
            x = m_ItemStartX+2;
            y = y + m_ItemH + 2;
        }

        m_ItemButtonDsrc.append(pItemButton);
    }

    mCounterABtn = new QPushButton(alyWidget);

    m_ItemButtonAly.append(mCounterABtn);
    connect(mCounterABtn,SIGNAL(released()),this,SLOT(showCounterA()));

    mDvmBtn = new QPushButton(alyWidget);
    m_ItemButtonAly.append(mDvmBtn);
    connect(mDvmBtn,SIGNAL(released()),this,SLOT(showDvm()));

    mPowerBtn = new QPushButton(alyWidget);
    m_ItemButtonAly.append(mPowerBtn);
    connect(mPowerBtn,SIGNAL(released()),this,SLOT(showUPA()));

    bool bNow = sysCheckLicense(OPT_PWR);
    if(!bNow)
    {
        mPowerBtn->hide();
    }
    else
    {
        mPowerBtn->show();
    }


    //mEyeBtn = new QPushButton(alyWidget);
    //m_ItemButtonAly.append(mEyeBtn);
    //connect(mEyeBtn,SIGNAL(released()),this,SLOT(showDvm()));
    //mEyeBtn->setText("Eye");

    mCounterABtn->setText("Counter");
    mDvmBtn->setText("DVM");
    mPowerBtn->setText("Power");

    x = m_ItemStartX;
    y = m_ItemStartY;
    w = m_ItemW+5;
    h = m_ItemH;
    for(int i=0; i<m_ItemButtonAly.count(); i++){
        QPushButton *p = m_ItemButtonAly.at(i);
        p->setGeometry(x,y,w,h);
        p->setFocusPolicy(Qt::TabFocus);

        x += m_ItemW+8;
    }

    alyWidget->setStyleSheet("QPushButton{ \
                         background-color:rgb(0,0,0); \
                         border:1px solid rgb(20,20,20);\
                         border-style: inset;\
                         border-radius:6px;\
                         color:rgb(200, 200, 200);\
                         outline: none;\
                         font-family:Droid Sans Fallback;}\
                     QPushButton::pressed{ \
                        color:rgb(200,200,200); \
                        background-color: rgb(0, 0, 255); \
                        border-radius: 6px;border: 1px solid rgb(30, 182, 255);} \
                     QPushButton::focus{ \
                         border:2px solid rgb(0,0,255);\
                         border-radius:6px;\
                         color:rgb(200, 200, 200);outline: none;font-family:Droid Sans Fallback;}");



    setParent(parent);
}

void CMeasItemBox::retranslateUi()
{
    mHorButton->setText(sysGetString(INF_APP_HORIZONTAL));
    mVerButton->setText(sysGetString(INF_APP_VERTICAL));
    mDsrcButton->setText(sysGetString(INF_APP_OTHER));
    mAlyButton->setText(sysGetString(INF_APP_ANALYZE));

    mCounterABtn->setText("Counter");
    mDvmBtn->setText(sysGetString(MSG_DVM));
    mPowerBtn->setText(sysGetString(MSG_APP_UPA));
    //mEyeBtn->setText(sysGetString(MSG_EYEJIT));

    for(int i=0; i<m_ItemButtonHor.count(); i++)
    {
        list_at(m_ItemButtonHor, i)->retranslateUi();
    }

    for(int i=0; i<m_ItemButtonVer.count(); i++)
    {
        list_at(m_ItemButtonVer, i)->retranslateUi();
    }

    for(int i=0; i<m_ItemButtonDsrc.count(); i++)
    {
        list_at(m_ItemButtonDsrc, i)->retranslateUi();
    }

    if( sysGetLanguage() == language_korean)
    {
//        qDebug()<<"Font = "<<qApp->font().family();
        mStackedWidget->widget(3)->setStyleSheet( "QPushButton{ \
                                                  background-color:rgb(0,0,0); \
                                                  border:1px solid rgb(20,20,20);\
                                                  border-style: inset;\
                                                  border-radius:6px;\
                                                  color:rgb(200, 200, 200);outline: none;\
                                                  font: 12pt \"Malgun Gothic\";}\
                                              QPushButton::pressed{ \
                                                 color:rgb(200,200,200); \
                                                 background-color: rgb(0, 0, 255); \
                                                 border-radius: 6px;border: 1px solid rgb(30, 182, 255);} \
                                              QPushButton::focus{ \
                                                  border:2px solid rgb(0,0,255);\
                                                  border-radius:6px;\
                                                  color:rgb(200, 200, 200);outline: none;}");
    }
    else
    {
        mStackedWidget->widget(3)->setStyleSheet( "QPushButton{ \
                                                  background-color:rgb(0,0,0); \
                                                  border:1px solid rgb(20,20,20);\
                                                  border-style: inset;\
                                                  border-radius:6px;\
                                                  color:rgb(200, 200, 200);outline: none;\
                                                  font-family:Droid Sans Fallback;}\
                                              QPushButton::pressed{ \
                                                 color:rgb(200,200,200); \
                                                 background-color: rgb(0, 0, 255); \
                                                 border-radius: 6px;border: 1px solid rgb(30, 182, 255);} \
                                              QPushButton::focus{ \
                                                  border:2px solid rgb(0,0,255);\
                                                  border-radius:6px;\
                                                  color:rgb(200, 200, 200);outline: none;\
                                                  font-family:Droid Sans Fallback;}");
    }

    if( this->isVisible())
    {
        update();
    }

}

void CMeasItemBox::setChoosed(bool b)
{
    switch (currFocus) {
    case HORFOCUS:
        if(!b){
            mHorButton->clearFocus();
        }else{
            mHorButton->setFocus();
        }
        break;
    case VERFOCUS:
        if(!b){
            mHorButton->clearFocus();
        }else{
            mVerButton->setFocus();
        }
        break;
    case DSRCFOCUS:
        if(!b)
            mVerButton->clearFocus();
        else{
            mDsrcButton->setFocus();
        }
        break;
    case MEASBUTTONFOCUS:
        //m_CurrItemButton->setSelected(b);
        break;
    case ALYFOCUS:
        if(!b)
            mAlyButton->clearFocus();
        else{
            mAlyButton->setFocus();
        }
        break;
    default:
        break;
    }
    update();
}

void CMeasItemBox::setUPAShow(bool b)
{
    mPowerBtn->setVisible(b);
}

void CMeasItemBox::setSrcA(Chan src)
{
    for(int i=0; i<m_ItemButtonHor.size(); i++)
    {
        list_at( m_ItemButtonHor,i)->setSrcA(src);
    }

    for(int i=0; i<m_ItemButtonVer.size(); i++)
    {
        list_at( m_ItemButtonVer,i)->setSrcA(src);
    }

    for(int i=0; i<m_ItemButtonDsrc.size(); i++)
    {
        list_at( m_ItemButtonDsrc,i)->setSrcA(src);
    }

    if( (src >= d0)&&(src <= d15) )
    {
        for(int i=0; i<m_ItemButtonVer.size(); i++)
        {
            list_at( m_ItemButtonVer,i)->setEnabled( false);
        }

        for(int i=0; i<m_ItemButtonHor.size(); i++)
        {
            if( m_ItemButtonHor.at(i)->getItemInfo()->isLaMeasType() )
            {
                list_at( m_ItemButtonHor,i)->setEnabled( true);
            }
            else
            {
                list_at( m_ItemButtonHor,i)->setEnabled( false);
            }
        }

    }
    else
    {
        for(int i=0; i<m_ItemButtonVer.size(); i++)
        {
            list_at( m_ItemButtonVer,i)->setEnabled( true);
        }

        for(int i=0; i<m_ItemButtonHor.size(); i++)
        {
            list_at( m_ItemButtonHor,i)->setEnabled( true);
        }
    }
}

void CMeasItemBox::setSrcB(Chan src)
{
    for(int i=0; i<m_ItemButtonDsrc.size(); i++)
    {
        list_at( m_ItemButtonDsrc,i)->setSrcB(src);
    }
}

void CMeasItemBox::setServName(QString str)
{
    servName = str;
}

void CMeasItemBox::setMeasHelpList(QList<HelpItem *> list)
{
    for(int i=0; i<list.count(); i++)
    {
        HelpItem *item = new HelpItem;
        *item = *(list.at(i));
        measHelpList.append(item);
    }
}
void CMeasItemBox::setHorType()
{
    int cat = 0;
    serviceExecutor::query( serv_name_measure, MSG_APP_MEAS_CAT, cat);
    if(cat != 0)
        serviceExecutor::post( serv_name_measure, MSG_APP_MEAS_CAT, 0);

    m_MeasType = Meas_HOR_TYPE;
    m_bShowHelpImage = true;
    mCloseButton->setGeometry(520,9,38,29);
    upCurrFocus(m_ItemButtonHor);
    mVerButton->setStyleSheet(m_typeNormal);
    mDsrcButton->setStyleSheet(m_typeNormal);
    mAlyButton->setStyleSheet(m_typeNormal);
    mHorButton->setStyleSheet(m_typeSelect);

    mStackedWidget->setCurrentIndex(0);
    this->resize( m_BoxW, m_BoxH-45);
    mAlyHasFocus = false;
    //update();

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void CMeasItemBox::setVerType()
{
    int cat = 0;
    serviceExecutor::query( serv_name_measure, MSG_APP_MEAS_CAT, cat);
    if(cat != 1)
        serviceExecutor::post( serv_name_measure, MSG_APP_MEAS_CAT, 1);

    m_MeasType = Meas_VER_TYPE;
    m_bShowHelpImage = true;
    mCloseButton->setGeometry(520,9,38,29);
    upCurrFocus(m_ItemButtonVer);
    mHorButton->setStyleSheet(m_typeNormal);
    mDsrcButton->setStyleSheet(m_typeNormal);
    mAlyButton->setStyleSheet(m_typeNormal);
    mVerButton->setStyleSheet(m_typeSelect);

    mStackedWidget->setCurrentIndex(1);
    this->resize( m_BoxW, m_BoxH);
    mAlyHasFocus = false;
    update();

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}
void CMeasItemBox::setDsrcType()
{
    int cat = 0;
    serviceExecutor::query( serv_name_measure, MSG_APP_MEAS_CAT, cat);
    if(cat != 2)
        serviceExecutor::post( serv_name_measure, MSG_APP_MEAS_CAT, 2);

    m_MeasType = Meas_DOUBLE_SRC_TYPE;
    m_bShowHelpImage = true;
    mCloseButton->setGeometry(520,9,38,29);
    upCurrFocus(m_ItemButtonDsrc);
    mHorButton->setStyleSheet(m_typeNormal);
    mVerButton->setStyleSheet(m_typeNormal);
    mAlyButton->setStyleSheet(m_typeNormal);
    mDsrcButton->setStyleSheet(m_typeSelect);

    mStackedWidget->setCurrentIndex(2);
    this->resize( m_BoxW, m_BoxH-45);
    mAlyHasFocus = false;
    update();

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void CMeasItemBox::upCurrFocus(QList<CMeasItemButton*> itemList)
{
    mHorButton->clearFocus();
    mVerButton->clearFocus();
    mDsrcButton->clearFocus();
    mAlyButton->clearFocus();
    currFocus = MEASBUTTONFOCUS;
    for(int i=0;i<itemList.count();i++)
    {
        itemList.at(i)->setSelected(false);
    }
    m_CurrItemButton = itemList.at(0);
    m_CurrItemButton->setSelected(true);

    emit raiseItemBox();
}

void CMeasItemBox::setAlyType()
{
    mStackedWidget->setCurrentIndex(3);
    this->resize( m_BoxW-150, m_BoxH-183);
    mCloseButton->setGeometry(374,10,38,29);
    m_bShowHelpImage = false;

    mHorButton->clearFocus();
    mVerButton->clearFocus();
    mDsrcButton->clearFocus();
    mAlyButton->clearFocus();
    for(int i=0; i< m_ItemButtonAly.count(); i++){
        QPushButton *p = m_ItemButtonAly.at(i);
        p->clearFocus();
    }
    m_ItemButtonAly.at(0)->setFocus();
    mAlyHasFocus = true;

    mHorButton->setStyleSheet(m_typeNormal);
    mVerButton->setStyleSheet(m_typeNormal);
    mDsrcButton->setStyleSheet(m_typeNormal);
    mAlyButton->setStyleSheet(m_typeSelect);

    update();

    if( sysGetStarted() )
    {
        QCoreApplication::processEvents();
    }
}

void CMeasItemBox::showCounterA()
{
    serviceExecutor::post( serv_name_counter, CMD_SERVICE_ACTIVE, (int)1 );
//    serviceExecutor::post( serv_name_counter, MSG_COUNTER_1_ENABLE,  counterStatus);
}

void CMeasItemBox::showCounterB()
{
//    int counterStatus = true;
//    serviceExecutor::post( serv_name_counter, CMD_SERVICE_ACTIVE, (int)1 );
//    serviceExecutor::post( serv_name_counter, MSG_COUNTER_2_ENABLE,  counterStatus);
}

void CMeasItemBox::showDvm()
{
    serviceExecutor::post( serv_name_dvm, CMD_SERVICE_ACTIVE, (int)1 );
//    serviceExecutor::post( serv_name_dvm, MSG_DVM_ENABLE,  true);
}

void CMeasItemBox::showUPA()
{
    serviceExecutor::post( serv_name_upa, CMD_SERVICE_ACTIVE, (int)1 );
}

void CMeasItemBox::itemClickedSlot(CMeasItemButton *itemButton)
{
    m_CurrItemButton->setSelected(false);
    currFocus = MEASBUTTONFOCUS;
    m_CurrItemButton = itemButton;
    m_CurrItemButton->setSelected(true);
    update();
    emit clickItem(itemButton->getItemInfo());
    emit raiseItemBox();

//    if( sysGetStarted() )
//    {
//        QCoreApplication::processEvents();
//    }
}

CMeasItem* CMeasItemBox::findItem(MeasType type)
{
    CMeasItem *item = NULL;

    if( (type>=Meas_HOR_TYPE)&&(type<Meas_HOR_TYPE_END))
    {
        for(int i=0; i<m_ItemButtonHor.size(); i++)
        {
            if( type == list_at(m_ItemButtonHor,i)->getItemInfo()->mType)
            {
                item = list_at(m_ItemButtonHor,i)->getItemInfo();
                return item;
            }
        }
        return item;
    }
    else if( (type>=Meas_VER_TYPE)&&(type<Meas_VER_TYPE_END))
    {
        for(int i=0; i<m_ItemButtonVer.size(); i++)
        {
            if( type == list_at(m_ItemButtonVer,i)->getItemInfo()->mType)
            {
                item = list_at(m_ItemButtonVer,i)->getItemInfo();
                return item;
            }
        }
        return item;
    }
    else if( (type>=Meas_DOUBLE_SRC_TYPE)&&(type<Meas_DOUBLE_SRC_TYPE_END))
    {
        for(int i=0; i<m_ItemButtonDsrc.size(); i++)
        {
            if( type == list_at(m_ItemButtonDsrc,i)->getItemInfo()->mType)
            {
                item = list_at(m_ItemButtonDsrc,i)->getItemInfo();
                return item;
            }
        }
        return item;
    }
    else
    {
        return item;
    }
}

void CMeasItemBox::rst()
{
    this->setGeometry(m_BoxX, m_BoxY, m_BoxW, m_BoxH);
}

void CMeasItemBox::loadResource(const QString & res_name)
{
    r_meta::CRMeta meta;

    QString str = res_name;

    meta.getMetaVal( str + "item_box_x", m_BoxX );//100
    meta.getMetaVal( str + "item_box_y", m_BoxY );//0
    meta.getMetaVal( str + "item_box_w", m_BoxW );//566
    meta.getMetaVal( str + "item_box_h", m_BoxH );//272
    this->setGeometry(m_BoxX, m_BoxY, m_BoxW, m_BoxH);

    meta.getMetaVal( str + "item_start_x", m_ItemStartX);
    meta.getMetaVal( str + "item_start_y", m_ItemStartY);
    meta.getMetaVal( str + "item_w", m_ItemW);
    meta.getMetaVal( str + "item_h", m_ItemH);

    meta.getMetaVal( str + "horback", m_HorBack);
    meta.getMetaVal( str + "verback", m_VerBack);
    meta.getMetaVal( str + "dsrcback", m_DsrcBack);


    meta.getMetaVal( str + "tselect", m_typeSelect);
    meta.getMetaVal( str + "tnormal", m_typeNormal);
    meta.getMetaVal( str + "tclose", m_typeClose);

}

void CMeasItemBox::mousePressEvent( QMouseEvent *event)
{
    tryActiveIn();

    this->raise();

    if ( !is_attr(mWndAttr, wnd_moveable ))
    {
        QWidget::mousePressEvent( event );
        event->ignore();
        return;
    }
    mptMoveOrig.setX( event->globalX()-pos().rx() );
    mptMoveOrig.setY( event->globalY()-pos().ry() );
}

void CMeasItemBox::mouseMoveEvent( QMouseEvent *event)
{
    if ( !is_attr(mWndAttr, wnd_moveable ))
    {
        event->ignore();
        return;
    }

    if (event->buttons() == Qt::LeftButton)
    {
        QPoint pt = event->globalPos();
        QPoint next = pt - mptMoveOrig;

        if ( next.x() < 0 )
        { next.setX(0);}

        if ( next.y() < 0 )
        { next.setY(0); }

        QWidget *pParent = (QWidget*)parent();
        if ( NULL != pParent )
        {
            QRect parRect = pParent->rect();
            QRect myRect = rect();

            if ( next.x() > parRect.width() - myRect.width() )
            { next.setX(parRect.width() - myRect.width());}

            if ( next.y() > parRect.height() - myRect.height() )
            { next.setY(parRect.height() - myRect.height()); }
        }
        this->move( next );
    }
}

void CMeasItemBox::paintEvent(QPaintEvent *)
{
    QImage image;
    QPainter painter(this);

    QRect r = rect();
    _style.paint( painter, r );

    QRect rLeftBox;
    rLeftBox.setX(r.x()+8);
    rLeftBox.setY(r.y()+46);
    rLeftBox.setWidth(r.width()-15);
    rLeftBox.setHeight(r.height()-56);

    painter.fillRect(rLeftBox, QColor(0,88,176,255));

    rLeftBox.setX(rLeftBox.x()+1);
    rLeftBox.setY(rLeftBox.y()+1);
    rLeftBox.setWidth(rLeftBox.width()-1);
    rLeftBox.setHeight(rLeftBox.height()-1);

    const qreal radius = 5;
    QPainterPath path;
    path.moveTo(rLeftBox.topRight() - QPointF(radius, 0));
    path.lineTo(rLeftBox.topLeft() + QPointF(radius, 0));
    path.quadTo(rLeftBox.topLeft(), rLeftBox.topLeft() + QPointF(0, radius));
    path.lineTo(rLeftBox.bottomLeft() + QPointF(0, -radius));
    path.quadTo(rLeftBox.bottomLeft(), rLeftBox.bottomLeft() + QPointF(radius, 0));
    path.lineTo(rLeftBox.bottomRight() - QPointF(radius, 0));
    path.quadTo(rLeftBox.bottomRight(), rLeftBox.bottomRight() + QPointF(0, -radius));
    path.lineTo(rLeftBox.topRight() + QPointF(0, radius));
    path.quadTo(rLeftBox.topRight(), rLeftBox.topRight() + QPointF(-radius, -0));
    painter.fillPath(path, QColor(0,0,0,255));
    painter.drawPath(path);


    if(m_bShowHelpImage)
    {
        QString mPicName = m_CurrItemButton->getItemPic();
        if(image.load(measHelpPic + mPicName))
        {
            painter.drawImage( 380, 50, image );
        }

        QRect r = rLeftBox;
        r.setX(380);
        r.setBottom(r.bottom()-5);
        //qDebug() << r << rLeftBox << m_CurrItemButton->getItemInfo()->getIDSName();

        QColor text_color = QColor(0xc0,0xc0,0xc0);
        painter.setPen( QPen(text_color));
        //        painter.drawText(r, m_CurrItemButton->getItemInfo()->getIDSName(), Qt::AlignBottom | Qt::AlignHCenter);
        painter.drawText(r, m_CurrItemButton->getItemInfo()->mName, Qt::AlignBottom | Qt::AlignHCenter);
    }
}

void CMeasItemBox::tuneInc(int )
{
    if( MEASBUTTONFOCUS != currFocus)   //! Focus on Category Range
    {
        if( mHorButton->hasFocus())
        {
            mVerButton->setFocus();
            currFocus = VERFOCUS;
            mAlyHasFocus = false;
        }
        else if( mVerButton->hasFocus())
        {
            mDsrcButton->setFocus();
            currFocus = DSRCFOCUS;
            mAlyHasFocus = false;
        }
        else if( mDsrcButton->hasFocus())
        {
            mAlyButton->setFocus();
            currFocus = ALYFOCUS;
            mAlyHasFocus = true;
        }
        else if( mAlyButton->hasFocus())
        {
            mCloseButton->setFocus();
            currFocus = CLOSEFOCUS;
            mAlyHasFocus = false;
        }
        else
        {
            mAlyHasFocus = false;
            //! Focus on CloseButton, do nothing
        }
    }
    else                                //! Focus on ItemButton Range
    {
        if( mHorButton->hasFocus())
        {
            m_CurrItemButton->setSelected(false);
            m_CurrItemButton = m_ItemButtonHor.at(0); //! Set Focus on 1st Hor.Item Button
            m_CurrItemButton->setFocus();
            m_CurrItemButton->setSelected(true);
            mAlyHasFocus = false;
        }
        else if( mVerButton->hasFocus())
        {
            m_CurrItemButton->setSelected(false);
            m_CurrItemButton = m_ItemButtonVer.at(0); //! Set Focus on 1st Ver.Item Button
            m_CurrItemButton->setFocus();
            m_CurrItemButton->setSelected(true);
            mAlyHasFocus = false;
        }
        else if( mDsrcButton->hasFocus())
        {
            m_CurrItemButton->setSelected(false);
            m_CurrItemButton = m_ItemButtonDsrc.at(0); //! Set Focus on 1st Dsrc.Item Button
            m_CurrItemButton->setFocus();
            m_CurrItemButton->setSelected(true);
            mAlyHasFocus = false;
        }
        else if( mAlyButton->hasFocus())
        {
            m_CurrItemButton->setSelected(false);
            m_ItemButtonAly.at(0)->setFocus(); //! Set Focus on 1st Aly.Item Button
            mAlyHasFocus = true;
        }
        else
        {
            if(m_ItemButtonAly.at(0)->hasFocus())
            {
                m_ItemButtonAly.at(1)->setFocus();
                mAlyHasFocus = true;
            }
            else if(m_ItemButtonAly.at(1)->hasFocus())
            {
                m_ItemButtonAly.at(2)->setFocus();
                mAlyHasFocus = true;
            }
            /* by hxh.
            else if(m_ItemButtonAly.at(2)->hasFocus())
            {
                m_ItemButtonAly.at(3)->setFocus();
                mAlyHasFocus = true;
            }*/
            else
            {
                mAlyHasFocus = false;
                switch( m_MeasType)
                {
                case Meas_HOR_TYPE:
                    measButtonTuneInc(m_ItemButtonHor);
                    break;
                case Meas_VER_TYPE:
                    measButtonTuneInc(m_ItemButtonVer);
                    break;
                case Meas_DOUBLE_SRC_TYPE:
                    measButtonTuneInc(m_ItemButtonDsrc);
                    break;
                default:
                    break;
                }
            }
        }

    }  //! Focus on ItemButton Range
    update();
}

void CMeasItemBox::measButtonTuneInc( QList<CMeasItemButton*> &measButtonList)
{
    for(int i=0; i<measButtonList.count(); i++)
    {
        if(measButtonList.at(i)->getItemID() == m_CurrItemButton->getItemID())
        {
            if(measButtonList.count()-1 != i)
            {
                currFocus = MEASBUTTONFOCUS;
                m_CurrItemButton->setSelected(false);

                for( int j=1; j<(measButtonList.count()-i); j++)
                {
                    if( measButtonList.at(i+j)->isEnabled())
                    {
                        m_CurrItemButton = measButtonList.at(i+j);
                        break;
                    }
                }
                m_CurrItemButton->setSelected(true);
                break;
            }
        }
    }
}

void CMeasItemBox::measButtonTuneDec(QList<CMeasItemButton *> &measButtonList)
{
    for(int i=0; i<measButtonList.count(); i++)
    {
        if(measButtonList.at(i)->getItemID() == m_CurrItemButton->getItemID())
        {
            if(0 != i)
            {
                currFocus = MEASBUTTONFOCUS;
                m_CurrItemButton->setSelected(false);

                for( int j=1; j<=i; j++)
                {
                    if(  (measButtonList.at(i-j)->isEnabled())
                       ||((i-j)==0) )
                    {
                        m_CurrItemButton = measButtonList.at(i-j);
                        break;
                    }
                }
                m_CurrItemButton->setSelected(true);
                break;
            }
            else
            {
                m_CurrItemButton->setSelected(false);

                if( m_MeasType == Meas_HOR_TYPE)
                {
                    mHorButton->setFocus();
                }
                else if( m_MeasType == Meas_VER_TYPE)
                {
                    mVerButton->setFocus();
                }
                else if( m_MeasType == Meas_DOUBLE_SRC_TYPE)
                {
                    mDsrcButton->setFocus();
                }
            }
        }
    }
}

void CMeasItemBox::tuneDec(int  )
{
    if( MEASBUTTONFOCUS != currFocus)   //! Focus on Category Range
    {
        if(mVerButton->hasFocus())
        {
            mHorButton->setFocus();
            currFocus = HORFOCUS;
            mAlyHasFocus = false;
        }
        else if(mDsrcButton->hasFocus())
        {
            mVerButton->setFocus();
            currFocus = VERFOCUS;
            mAlyHasFocus = false;
        }
        else if(mAlyButton->hasFocus())
        {
            mDsrcButton->setFocus();
            currFocus = DSRCFOCUS;
            mAlyHasFocus = false;
        }
        else if(mCloseButton->hasFocus())
        {
            mAlyButton->setFocus();
            currFocus = ALYFOCUS;
            mAlyHasFocus = true;
        }
        else
        {
            mAlyHasFocus = false;
            //! Focus on HorButton, do nothing

        }
    }
    else                                //! Focus on ItemButton Range
    {
        /* by hxh
         * if( m_ItemButtonAly.at(3)->hasFocus())
        {
            m_ItemButtonAly.at(2)->setFocus();
            mAlyHasFocus = true;
        }
        else */
        if( m_ItemButtonAly.at(2)->hasFocus())
        {
            m_ItemButtonAly.at(1)->setFocus();
            mAlyHasFocus = true;
        }
        else if( m_ItemButtonAly.at(1)->hasFocus())
        {
            m_ItemButtonAly.at(0)->setFocus();
            mAlyHasFocus = true;
        }
        else if( m_ItemButtonAly.at(0)->hasFocus())
        {
            mAlyButton->setFocus();
            mAlyHasFocus = true;
        }
        else if( mAlyButton->hasFocus() || mAlyHasFocus)
        {
            mAlyButton->setFocus();
        }
        else
        {
            mAlyHasFocus = false;
            switch(m_MeasType)
            {
            case Meas_HOR_TYPE:
                measButtonTuneDec(m_ItemButtonHor);
                break;
            case Meas_VER_TYPE:
                measButtonTuneDec(m_ItemButtonVer);
                break;
            case Meas_DOUBLE_SRC_TYPE:
                measButtonTuneDec(m_ItemButtonDsrc);
                break;
            default:
                break;
            }
        }
    }   //! Focus on ItemButton Range
    update();
}

void CMeasItemBox::tuneZ(int)
{
    if( MEASBUTTONFOCUS != currFocus)   //! Focus on Category Range
    {
        currFocus = MEASBUTTONFOCUS;
        if( mHorButton->hasFocus())
        {
            setHorType();
            mAlyHasFocus = false;
        }
        else if( mVerButton->hasFocus())
        {
            setVerType();
            mAlyHasFocus = false;
        }
        else if( mDsrcButton->hasFocus())
        {
            setDsrcType();
            mAlyHasFocus = false;
        }
        else if( mAlyButton->hasFocus())
        {
            setAlyType();
            mAlyHasFocus = true;
        }else if( mCloseButton->hasFocus())
        {
            close();
        }
    }
    else                                //! Focus on ItemButton Range
    {
        if( mHorButton->hasFocus())
        {
            currFocus = HORFOCUS;
            mAlyHasFocus = false;
        }
        else if( mVerButton->hasFocus())
        {
            currFocus = VERFOCUS;
            mAlyHasFocus = false;
        }
        else if( mDsrcButton->hasFocus())
        {
            currFocus = DSRCFOCUS;
            mAlyHasFocus = false;
        }
        else if( mAlyButton->hasFocus())
        {
            currFocus = ALYFOCUS;
            mAlyHasFocus = true;
        }
        else if( mCloseButton->hasFocus())
        {
            currFocus = CLOSEFOCUS;
            close();
        }
        else     //! Click ItemButton, Add MeasItem
        {
            if( m_CurrItemButton->isEnabled())
            {
                emit clickItem(m_CurrItemButton->getItemInfo());
                emit raiseItemBox();
            }
        }
    }

//    if(mHorButton->hasFocus()){
//        setHorType();
//    }else if(mVerButton->hasFocus()){
//        setVerType();
//    }else if(mDsrcButton->hasFocus()){
//        setDsrcType();
//    }else if(mAlyButton->hasFocus()){
//        setAlyType();
//    }else if(mCloseButton->hasFocus()){
//        close();
//    }else if(ALYFOCUS == currFocus){
//        for(int i=0; i< m_ItemButtonAly.count(); i++){
//            QPushButton *p = m_ItemButtonAly.at(i);
//            if(p->hasFocus()){
//                switch (i) {
//                case 0:
//                    showCounterA();
//                    break;
//                case 1:
//                    showDvm();
//                    break;
//                case 2:
//                    break;
//                case 3:
//                    break;
//                default:
//                    break;
//                }
//            }
//        }
//    }else{
//        emit clickItem(m_CurrItemButton->getItemInfo());
//        emit raiseItemBox();
//    }
}




