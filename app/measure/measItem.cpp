#include "measItem.h"
#include "../../gui/cguiformatter.h"

QMap<Chan,QString> CMeasItem::srcName = CMeasItem::InitSrcNameMap();

QMap<Chan,QString> CMeasItem::InitSrcNameMap()
{
    QMap<Chan,QString> name;
    name[chan_none] = "";
    name[chan1]     = "1";
    name[chan2]     = "2";
    name[chan3]     = "3";
    name[chan4]     = "4";

    name[d0]        = "d0";
    name[d1]        = "d1";
    name[d2]        = "d2";
    name[d3]        = "d3";
    name[d4]        = "d4";
    name[d5]        = "d5";
    name[d6]        = "d6";
    name[d7]        = "d7";
    name[d8]        = "d8";
    name[d9]        = "d9";
    name[d10]       = "d10";
    name[d11]       = "d11";
    name[d12]       = "d12";
    name[d13]       = "d13";
    name[d14]       = "d14";
    name[d15]       = "d15";

    name[r1]        = "r1";
    name[r2]        = "r2";
    name[r3]        = "r3";
    name[r4]        = "r4";
    name[r5]        = "r5";
    name[r6]        = "r6";
    name[r7]        = "r7";
    name[r8]        = "r8";
    name[r9]        = "r9";
    name[r10]       = "r10";

    name[m1]        = "m1";
    name[m2]        = "m2";
    name[m3]        = "m3";
    name[m4]        = "m4";
    return name;
}

CMeasItem::CMeasItem(QString &n, QString& ic, MeasType t)  //for MeasButton,MeasAllBox
{
    m_Name = n;
    m_DispName = m_Name;
    m_Icon = ic;
    m_Type = t;

    setDefaultSrc();

    m_Curr = "-";
    m_Avg  = "-";
    m_Max  = "-";
    m_Min  = "-";
    m_Dev  = "-";
    m_Cnt  = 0;
    m_CurrValue = 0.0;
    unitInit();
 }

CMeasItem::CMeasItem(QString &n, MeasType t, Chan a, Chan b)  //for MeasValueItems
{
    m_Name = n;
    m_DispName = m_Name;
    m_Type = t;
    m_Icon = "-";

    m_SrcA = a;
    m_SrcB = b;
    setDispName();
    setItemColor();

    m_Curr = "-";
    m_Avg  = "-";
    m_Max  = "-";
    m_Min  = "-";
    m_Dev  = "-";
    m_CurrValue = 0.0;
    m_Cnt  = 0;
    unitInit();
 }

void  CMeasItem::setDispName(void)
{
    if( (m_Type >= Meas_DOUBLE_SRC_TYPE) && (m_Type < Meas_DOUBLE_SRC_TYPE_END))
    {
        m_DispName = m_Name +"("+srcName.value(m_SrcA)+"→"+srcName.value(m_SrcB)+")";
    }
    else
    {
        m_DispName = m_Name + "("+srcName.value(m_SrcA) +")";
    }
}

void CMeasItem::setDefaultSrc(void)
{
    if( (m_Type >= Meas_DOUBLE_SRC_TYPE) && (m_Type < Meas_DOUBLE_SRC_TYPE_END))
    {
        m_SrcA = chan1;
        m_SrcB = chan2;
    }
    else
    {
        m_SrcA = chan1;
        m_SrcB = chan_none;
    }
    setDispName();
    setItemColor();
}

void CMeasItem::setSrcA(Chan s)
{
    m_SrcA = s;
    setDispName();
    setItemColor();
}

void CMeasItem::setSrcB(Chan s)
{
    m_SrcB = s;
}

void   CMeasItem::setDelaySrc(Chan a, Chan b)
{
    if( (m_Type == Meas_DelayFall)||(m_Type == Meas_DelayRise) )
    {
        setSrcA(a);
        setSrcB(b);
        setDispName();
    }
}

void   CMeasItem::setPhaseSrc(Chan a, Chan b)
{
    if( (m_Type == Meas_PhaseFall)||(m_Type == Meas_PhaseRise) )
    {
        setSrcA(a);
        setSrcB(b);
        setDispName();
    }
}


void  CMeasItem::setItemColor(void)
{
    if( (m_Type >= Meas_DOUBLE_SRC_TYPE) && (m_Type < Meas_DOUBLE_SRC_TYPE_END))
    {
        itemColor = Qt::white;
    }
    else
    {
        switch (m_SrcA)
        {
        case chan1:
            itemColor = QColor::fromRgb(0xB2,0xB2,0x00);
            break;
        case chan2:
            itemColor = QColor::fromRgb(0x00,0xF9,0xF6);
            break;
        case chan3:
            itemColor = QColor::fromRgb(0xF4,0x00,0xBF);
            break;
        case chan4:
            itemColor = QColor::fromRgb(0x00,0x78,0xF4);
            break;
        case m1: case m2: case m3: case m4:
            itemColor = Math_color;
            break;

        default:
            itemColor = Qt::white;
            break;
        }
    }

}

void CMeasItem::setCurr(measData* pMeasData)
{
    float value = 0;

    value = pMeasData->getRealMeasData(m_Type);
    measUiFormatter(m_Curr, value, true);

}

void CMeasItem::setCurr(MeasDataDSrcList dataList)
{
    MeasType temptype = MeasType(0);
    float value = 0;

    switch (m_Type)
    {
    case Meas_DelayFall: case Meas_DelayRise:
        temptype = m_Type;
        break;
    case Meas_PhaseFall:
        temptype = Meas_DelayFall;
        break;
    case Meas_PhaseRise:
        temptype = Meas_DelayRise;
        break;
    default:
        temptype = m_Type;
        break;
    }

    if(dataList->size() != 0 )
    {
        for(int i=0; i<dataList->size(); ++i)
        {
            if(  (dataList->at(i)->getMeasSrcA() == m_SrcA)
               &&(dataList->at(i)->getMeasSrcB() == m_SrcB))
            {
                if(temptype == (dataList->at(i)->getMeasType()) )
                {
                    //qDebug() << "Type=" << dataList->at(i)->getMeasType();
                    value = dataList->at(i)->getRealMeasData(m_Type);
                    measUiFormatter(m_Curr, value, true);
                    break;
                }

            }
        }
    }
    else
    {
        m_Curr = "-";
    }

}

void CMeasItem::setValue(MeasStatDataList *ptr)
{
    if( ptr != 0)
    {
        if( ptr->size() != 0)
        {
            for( int i=0; i<ptr->size(); i++)
            {
                measStatData *pData = list_at( (*ptr),i);
                if(  (pData->getType() == m_Type)
                   &&(pData->getSrcA() == m_SrcA)
                   &&(pData->getSrcB() == m_SrcB) )
                {
                    m_CurrValue = pData->getCurr();
                    setCurr(pData->getCurr());
                    setAvg( pData->getAvg());
                    setMax( pData->getMax());
                    setMin( pData->getMin());
                    setDev( pData->getDev());
                    setCnt( pData->getCnt());
                    break;
                }
            }
        }
    }
}

#include <math.h> // for isinf
void CMeasItem::setCurr(float val)
{
    measUiFormatter(m_Curr, val, true);
}

void CMeasItem::setAvg(float val)
{
    measUiFormatter(m_Avg, val, true);
}

void CMeasItem::setMax(float val)
{
    measUiFormatter(m_Max, val, true);
}

void CMeasItem::setMin(float val)
{
    measUiFormatter(m_Min, val, true);
}

void CMeasItem::setDev(float val)
{
    measUiFormatter(m_Dev, val, false);
}

void CMeasItem::setCnt(ulong val)
{
    m_Cnt = val;
}


void CMeasItem::measUiFormatter(QString &str, float val, bool bUnit)
{
    //val = QString::arg(value, 0,0);
    //QString::number(getMeasItemCurValue(),'g',precision) + "mV"
    Unit u = Unit_none;
    if(bUnit)
    {
        if     (m_Unit == "V")   u = Unit_V;
        else if(m_Unit == "V2")  u = Unit_V;
        else if(m_Unit == "V/s") u = Unit_VdivS;
        else if(m_Unit == "Vs")  u = Unit_VmulS;
        else if(m_Unit == "d")   u = Unit_degree;
        else if(m_Unit == "Hz")  u = Unit_hz;
        else if(m_Unit == "%")   u = Unit_percent;
        else if(m_Unit == "s")   u = Unit_s;
        else                     u = Unit_none;
    }
    else
    {
        u = Unit_none;
    }

    //qDebug() << "__________" << val;
    if( !isinf(val) && !isnan(val))
    {
        gui_fmt::CGuiFormatter::format( str, val, fmt_def,  u);
    }
    else
    {
        gui_fmt::CGuiFormatter::format( str, 0, fmt_def,  u);
//        qDebug() << "++++++++Fix HERE:++++++++++" << __FILE__<<__FUNCTION__<<__LINE__;
    }
}

void CMeasItem::unitInit(void)
{
    if     (isUnitCh())      m_Unit = "V";
    else if(isUnitCh2())     m_Unit = "V2";
    else if(isUnitChDivS())  m_Unit = "V/s";
    else if(isUnitChS())     m_Unit = "Vs";
    else if(isUnitDegree())  m_Unit = "d";
    else if(isUnitHz())      m_Unit = "Hz";
    else if(isUnitPercent()) m_Unit = "%";
    else if(isUnitS())       m_Unit = "s";
    else                     m_Unit = "";
}

bool CMeasItem::isUnitS()
{
    if(  (m_Type == Meas_Period)
       ||((m_Type >= Meas_RiseTime)&&(m_Type <= Meas_NWidth))
       ||(m_Type == Meas_Tvmax)||(m_Type == Meas_Tvmin)
       ||(m_Type == Meas_DelayFall)||(m_Type == Meas_DelayRise) )
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasItem::isUnitHz()
{
    if(m_Type == Meas_Freq)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CMeasItem::isUnitPercent()
{
    if(  (m_Type == Meas_PDuty)||(m_Type == Meas_NDuty)
       ||(m_Type == Meas_Overshoot)||(m_Type == Meas_Preshoot) )
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasItem::isUnitCh()
{
    if((m_Type >= Meas_Vmax)&&(m_Type <= Meas_Vrms_S))
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasItem::isUnitCh2()
{
    if(m_Type == Meas_Variance)
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasItem::isUnitChS()
{
    if((m_Type == Meas_Area)||(m_Type == Meas_Area_S))
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasItem::isUnitChDivS()
{
    if((m_Type == Meas_Pslew_rate)||(m_Type == Meas_Nslew_rate))
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasItem::isUnitDegree()
{
    if((m_Type == Meas_PhaseFall)||(m_Type == Meas_PhaseRise))
    {
        return true;
    }
    else
    {
        return false;
    }

}
