#include "cmeasValueItem.h"
#include "../../gui/cguiformatter.h"

CMeasValueItem::CMeasValueItem(CMeasItem item)
{
    m_Item = item;

    m_Curr = "- - -";
    m_Avg  = "";
    m_Max  = "";
    m_Min  = "";
    m_Dev  = "";
    m_Cnt  = 0;
    m_CurrValue = 0.0;
    unitInit();

    m_Ax.bVisible = false;
    m_Ax.mPos = 0;

    m_Bx.bVisible = false;
    m_Bx.mPos = 0;

    m_Ay.bVisible = false;
    m_Ay.mPos = 0;

    m_By.bVisible = false;
    m_By.mPos = 0;

    m_Status = INVALID;
 }

bool CMeasValueItem::itemExist(CMeasItem &item)
{
    if(  (m_Item.mType  == item.mType)
       &&(m_Item.mSrcA  == item.mSrcA)
       &&(m_Item.mSrcB  == item.mSrcB))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void CMeasValueItem::setSrcA(Chan s)
{
    m_Item.setSrcA(s);
}

void CMeasValueItem::setSrcB(Chan s)
{
    m_Item.setSrcB(s);
}

void CMeasValueItem::setCurr(measData* pMeasData)
{
    if( pMeasData != NULL)
    {
        float value = 0;

        m_Status = pMeasData->getMeasStatus(m_Item.mType);
        m_Unit   = pMeasData->getRealMeasData(m_Item.mType).mUnit;

        if(  (m_Item.mType == Meas_PDuty)||(m_Item.mType == Meas_NDuty)
             ||(m_Item.mType == Meas_Overshoot)||(m_Item.mType == Meas_Preshoot) )  //! Unit: %
        {
            value = pMeasData->getRealMeasData(m_Item.mType).mVal * 100;
        }
        else
        {
            value = pMeasData->getRealMeasData(m_Item.mType).mVal;
        }

        measUiFormatter(m_Curr, value, true);
    }

#if 0
    {
        switch (status)
        {
        case LOW_SIGNAL:
            m_Curr = "Low signal";
            break;

        case GREATER_CLIPPED:
            m_Curr = "Clipped";
            break;

        case NO_PERIOD:
            m_Curr = "No Period";
            break;

        case NO_EDGE:
            m_Curr = "No Edge";
            break;

        case NO_SIGNAL:
            m_Curr = "No signal";
            break;

        default:
            m_Curr = "- - -";
            break;
        }
    }
#endif
}

void CMeasValueItem::setValueFromList(MeasStatDataList *ptr)
{
    if( ptr != 0)
    {
        if( ptr->size() != 0)
        {
            for( int i=0; i<ptr->size(); i++)
            {               
                measStatData *pData = list_at( (*ptr), i );

                if( NULL != pData)
                {
                    //! item match
                    if(  (pData->getType() == m_Item.mType)
                         &&(pData->getSrcA() == m_Item.mSrcA)
                         &&(pData->getSrcB() == m_Item.mSrcB) )
                    {
                        setValue(pData);
                        break;
                    }
                }
            }  //! for
        }  //! if( ptr->size() != 0)
    }
}

void CMeasValueItem::setValue(measStatData *ptr)
{
    if( ptr != 0)
    {
        m_CurrValue = ptr->getCurr();
        m_Status = ptr->getValStatus();
//        qDebug()<<"CMeasValueItem::setValue m_Status: "<<m_Status;
//        qDebug()<<"CMeasValueItem::setValue m_CurrValue: "<<m_CurrValue;
        m_Unit = ptr->getUnit();
        MeasType type = ptr->getType();
        if(  (type == Meas_PDuty)||(type == Meas_NDuty)
             ||(type == Meas_Overshoot)||(type == Meas_Preshoot) )  //! Unit: %
        {
            setCurr(ptr->getCurr() * 100);
            setAvg( ptr->getAvg() * 100);
            setMax( ptr->getMax() * 100);
            setMin( ptr->getMin() * 100);
            setDev( ptr->getDev());
            setCnt( ptr->getCnt());
        }
        else
        {
            setCurr(ptr->getCurr());
            setAvg( ptr->getAvg());
            setMax( ptr->getMax());
            setMin( ptr->getMin());
            setDev( ptr->getDev());
            setCnt( ptr->getCnt());
        }

        m_Ax = ptr->getAx();
        m_Bx = ptr->getBx();
        m_Ay = ptr->getAy();
        m_By = ptr->getBy();

#if 0
        {
            m_Avg = "";
            m_Max = "";
            m_Min = "";
            m_Dev = "";
            m_Cnt = 0;

            m_Ax.bVisible = false;
            m_Bx.bVisible = false;
            m_Ay.bVisible = false;
            m_By.bVisible = false;

            switch (status)
            {
            case LOW_SIGNAL:
                m_Curr = "Low signal";
                break;

            case GREATER_CLIPPED:
                m_Curr = "Clipped";
                break;

            case NO_PERIOD:
                m_Curr = "No Period";
                break;

            case NO_EDGE:
                m_Curr = "No Edge";
                break;

            case NO_SIGNAL:
                m_Curr = "No signal";
                break;

            default:
                m_Curr = "- - -";
                break;
            }
        }
#endif
    } //if( ptr != 0)
}

#include <math.h> // for isinf
void CMeasValueItem::setCurr(float val)
{
    measUiFormatter(m_Curr, val, true);
}

void CMeasValueItem::setAvg(float val)
{
    if( m_Status == VALID)
    {
        measUiFormatter(m_Avg, val, true);
    }
    else
    {
        m_Avg = "";
    }
}

void CMeasValueItem::setMax(float val)
{
    if( m_Status == VALID)
    {
        measUiFormatter(m_Max, val, true);
    }
    else
    {
        m_Max = "";
    }
}

void CMeasValueItem::setMin(float val)
{
    if( m_Status == VALID)
    {
        measUiFormatter(m_Min, val, true);
    }
    else
    {
        m_Min = "";
    }
}

void CMeasValueItem::setDev(float val)
{
    if( m_Status == VALID)
    {
        measUiFormatter(m_Dev, val, true);
    }
    else
    {
        m_Dev = "";
    }
}

void CMeasValueItem::setCnt(ulong val)
{
    if( m_Status == VALID)
    {
        m_Cnt = val;
    }
    else
    {
        m_Cnt = 0;
    }
}


void CMeasValueItem::measUiFormatter(QString &str, float val, bool bUnit)
{
    //val = QString::arg(value, 0,0);
    //QString::number(getMeasItemCurValue(),'g',precision) + "mV"
    Unit u = Unit_none;
    if(bUnit)
    {
        u = m_Unit;
    }
    else
    {
        u = Unit_none;
    }

    //qDebug() << "__________" << val;
    if( !isinf(val) && !isnan(val))
    {
        if( u == Unit_percent && abs(val) < 1.0 )
        {
            int abc = (int)(val * 10000);
            str = QString("0.%1%").arg(abc);
        }
        else
        {
            gui_fmt::CGuiFormatter::format( str,
                                            val,
                                            dso_fmt_trim(fmt_float, fmt_width_4, fmt_no_trim_0),
                                            u);
        }
    }
    else
    {
        str = "*****";
    }
    switch( m_Status)
    {
    case VALID:
        break;

    case LOW_SIGNAL:
//        str = "Low signal";
        str = "*****";
        break;

    case GREATER_CLIPPED:
//        str = QString(">") + str;
        str = "*****";
        break;

    case LESS_CLIPPED:
//        str = QString("<") + str;
        str = "*****";
        break;

    case CLIPPED:
//        str = "Clipped";
        str = "*****";
        break;

    case NO_PERIOD:
//        str = "No Period";
        str = "*****";
        break;

    case NO_EDGE:
//        str = "No Edge";
        str = "*****";
        break;

    case NO_SIGNAL:
//        str = "No signal";
        str = "*****";
        break;

    default:
        str = "*****";
        break;
    }
}

void CMeasValueItem::unitInit(void)
{
    if     (isUnitCh())      m_Unit = Unit_V;
    else if(isUnitCh2())     m_Unit = Unit_V2;
    else if(isUnitChDivS())  m_Unit = Unit_VdivS;
    else if(isUnitChS())     m_Unit = Unit_VmulS;
    else if(isUnitDegree())  m_Unit = Unit_degree;
    else if(isUnitHz())      m_Unit = Unit_hz;
    else if(isUnitPercent()) m_Unit = Unit_percent;
    else if(isUnitS())       m_Unit = Unit_s;
    else if(isUnitA())       m_Unit = Unit_A;
    else if(isUnitW())       m_Unit = Unit_W;
    else if(isUnitVA())      m_Unit = Unit_VA;
    else if(isUnitVAR())     m_Unit = Unit_VAR;
    else                     m_Unit = Unit_none;
}

bool CMeasValueItem::isUnitS()
{
    if(  (m_Item.mType == Meas_Period)
       ||((m_Item.mType >= Meas_RiseTime)&&(m_Item.mType <= Meas_NWidth))
       ||(m_Item.mType == Meas_Tvmax)||(m_Item.mType == Meas_Tvmin)
       ||(m_Item.mType == Meas_DelayFF)||(m_Item.mType == Meas_DelayRR)
       ||(m_Item.mType == Meas_DelayFR)||(m_Item.mType == Meas_DelayRF) )
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasValueItem::isUnitHz()
{
    if(  (m_Item.mType == Meas_Freq)
       ||(m_Item.mType == UPA_Ref_Freq) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CMeasValueItem::isUnitPercent()
{
    if(  (m_Item.mType == Meas_PDuty)||(m_Item.mType == Meas_NDuty)
       ||(m_Item.mType == Meas_Overshoot)||(m_Item.mType == Meas_Preshoot) )
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasValueItem::isUnitCh()
{
    if(  ((m_Item.mType >= Meas_Vmax)&&(m_Item.mType <= Meas_Vrms_S))
       ||(m_Item.mType == UPA_Vrms)
       ||(m_Item.mType == Meas_Variance) )
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasValueItem::isUnitCh2()
{
//    if(m_Item.mType == Meas_Variance)
//    {
//        return true;
//    }
//    else
//    {
//        return false;
//    }

    return false;
}

bool CMeasValueItem::isUnitChS()
{
    if((m_Item.mType == Meas_Area)||(m_Item.mType == Meas_Area_S))
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasValueItem::isUnitChDivS()
{
    if((m_Item.mType == Meas_Pslew_rate)||(m_Item.mType == Meas_Nslew_rate))
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasValueItem::isUnitDegree()
{
    if(  (m_Item.mType == Meas_PhaseFF)||(m_Item.mType == Meas_PhaseRR)
       ||(m_Item.mType == Meas_PhaseFR)||(m_Item.mType == Meas_PhaseRF)
       ||(m_Item.mType == UPA_Phase_Angle) )
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool CMeasValueItem::isUnitA()
{
    if( m_Item.mType == UPA_Irms)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CMeasValueItem::isUnitW()
{
    if( m_Item.mType == UPA_Real_P)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CMeasValueItem::isUnitVA()
{
    if( m_Item.mType == UPA_Apparent_P)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CMeasValueItem::isUnitVAR()
{
    if( m_Item.mType == UPA_Reactive_P)
    {
        return true;
    }
    else
    {
        return false;
    }
}


