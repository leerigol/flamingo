#include <QPainter>
#include "measStatValueBox.h"
#include "measItemBox.h"
#include "../../meta/crmeta.h"
#include "../../service/service_name.h"
#include "../../service/servicefactory.h"

menu_res::RInfoRowList_Style CMeasStatValueBox::_style;
CMeasStatValueBox::CMeasStatValueBox(QWidget *parent) : uiWnd(parent)
{
    yOffset = 0;
    xOffset = 0;
    isMove = false;

    mChooseRow = 0;
    m_MeasItems = 0;

    isActive = false;

    mRowNum = 0;

    this->setObjectName("measstatvalue");
    set_attr( wndAttr, mWndAttr, wnd_moveable );

//    mKeyList.append( R_Pkey_FInc );
//    mKeyList.append( R_Pkey_FDec );
//    mKeyList.append( R_Pkey_FZ );
}

void CMeasStatValueBox::setupUI(QWidget *parent)
{
    loadResource("measure/stat_values/");
    _style.loadResource("rframe/");
    _style.setColumnsNum(7);
    _style.setHeightPerRow(m_nRowH);
    QList<int> widthList;
    widthList<<110<<86<<86<<86<<86<<86<<40;
    _style.setColumnsWidth(widthList);
    _style.setChoosedRow(-1);

    mMeasHistBox = new CMeasHistBox();
    mMeasHistBox->setupUi(parent);
    mMeasHistBox->hide();

    setParent(parent);
}

void CMeasStatValueBox::delAllItem(void)
{
    this->hide();
    mMeasHistBox->hide();
//    mMeasHistBox->delAllItem();
    mChooseRow = 0;
    m_nBoxY = m_nBoxDefaultY;
    m_nBoxH = m_nBoxDefaultH;
}

void CMeasStatValueBox::updateMeasValue(QList<CMeasValueItem*> *pData)
{
    m_MeasItems = pData;

    int rowNum  = m_MeasItems->size();
    m_nBoxH = m_nBoxDefaultH + m_nRowH * rowNum;
    this->resize(m_nBoxW, m_nBoxH);

    if(mRowNum != rowNum){
        int tempH = geometry().y()+(mRowNum-rowNum)*m_nRowH;
        mRowNum = rowNum;
        if(tempH < 0)
            tempH = 0;
        if(tempH > 450)
            tempH = 450;
        move(geometry().x(), tempH);
    }
    update();
}

void CMeasStatValueBox::setChooseRow(int row)
{
    isActive = true;
    mChooseRow = row;
    update();
}

void CMeasStatValueBox::setChoosed(bool b)
{
    isActive = b;
    update();
}

void CMeasStatValueBox::measHistShow()
{
    mMeasHistBox->updateValueNum(*m_MeasItems, mChooseRow);

    if(mMeasHistBox->isHidden())
        mMeasHistBox->show();
}

void CMeasStatValueBox::loadResource(const QString &path)
{
    r_meta::CRMeta meta;
    QString str = path ;

    meta.getMetaVal(str + "row_h", m_nRowH);//16

    meta.getMetaVal(str + "x", m_nBoxX);//200
    meta.getMetaVal(str + "y", m_nBoxY);//451
    meta.getMetaVal(str + "w", m_nBoxW);//590
    meta.getMetaVal(str + "h", m_nBoxH);//29
    m_nBoxDefaultY = m_nBoxY;
    m_nBoxDefaultH = m_nBoxH;

    QRect m_nRect;
    m_nRect.setRect(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);

    this->setGeometry(m_nBoxX,m_nBoxY,m_nBoxW,m_nBoxH);
}

//void CMeasStatValueBox::mousePressEvent(QMouseEvent *event)
//{
//    QRect rect = geometry();
//    rect.setX(0);
//    rect.setY(0);
//    if(rect.contains(event->pos())){
//        qDebug()<<__FUNCTION__<<__LINE__;
//        emit raiseValueBox();
//        qDebug()<<__FUNCTION__<<__LINE__;
//        isMove = true;
//        this->xOffset = event->globalPos().rx() - this->pos().rx();
//        this->yOffset = event->globalPos().ry() - this->pos().ry();
//    }else
//        isMove = false;

//    return uiWnd::mousePressEvent(event);
//}

//void CMeasStatValueBox::mouseMoveEvent(QMouseEvent * event)
//{
//    if (event->buttons() == Qt::LeftButton && isMove ) {

//        int toX = event->globalX()-xOffset;
//        int toY = event->globalY()-yOffset;

//        if( event->globalX()-xOffset < 0 )
//            toX = 0;
//        else if(event->globalX()-xOffset > 632)
//            toX = 632;

//        if( event->globalY()-yOffset < 0 )
//            toY = 0;
//        else if(event->globalY()-yOffset > 450)
//            toY = 450;
//        move(toX, toY);
//        update();
//    }
//}
//void CMeasStatValueBox::mouseReleaseEvent(QMouseEvent *)
//{
//    isMove = false;
//    xOffset = 0;
//    yOffset = 0;
//}
void CMeasStatValueBox::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    QFont font;
    font.setPointSize(10);
    painter.setFont(font);

    QRect r = this->geometry();
    int nWidth  = r.width();
    int nStartX = 8;//in central

    r.setX(0);
    r.setY(0);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

//    if(isActive)
//        _style.setChoosedRow(mChooseRow);
//    else
    _style.setChoosedRow(-1);
    _style.paintEvent(painter, r);

    r.setX(30);
    r.setY(5);
    r.setWidth(nWidth);
    r.setHeight(m_nBoxH);

    QString  temp("%1"),val;
    QPen     pen;

//    pen.setColor(Qt::white);
    pen.setColor(QColor(200,200,200,255));
    painter.setPen(pen);

    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Item");

    r.setX(r.x()+105);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Curr");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Avg");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Max");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Min");

    r.setX(r.x()+86);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Dev");

    r.setX(r.x()+76);
    painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, "Cnt");

    r.setY(8);
    r.setY(r.y() + m_nRowH);
    r.setX(nStartX);

    for(int i=0; i<m_MeasItems->count(); i++)
    {
        CMeasValueItem *p = list_at((*m_MeasItems),i);

        pen.setColor(p->getItemInfo()->mSymbolColor);
        painter.setPen(pen);

//        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getItemInfo()->mName);


        QRect rName = r;
        rName.setHeight(m_nRowH);
        //Draw MeasItem Name
        pen.setColor(p->getItemInfo()->mSymbolColor);
        painter.setPen(pen);

        QString nameTemp;
        if(p->getItemInfo()->isDsrcType()){
            nameTemp = p->getItemInfo()->mSymbol + "(";
            painter.drawText(rName, Qt::AlignLeft | Qt::AlignVCenter, nameTemp);
        }else{
            nameTemp = p->getItemInfo()->mName + "(";
            painter.drawText(rName, Qt::AlignLeft | Qt::AlignVCenter, nameTemp);
        }

        QRect   nameRect, rectType1, rectType_2;
        QFontMetrics fm(font);
        nameRect  = fm.boundingRect(rName,Qt::TextWrapAnywhere, nameTemp);
        QString str = nameTemp+p->getItemInfo()->mSrcAStr;
        rectType1 = fm.boundingRect(rName,Qt::TextWrapAnywhere, str);
        rectType_2 = fm.boundingRect(rName,Qt::TextWrapAnywhere, str + p->getItemInfo()->mSrcBStr);

        if(p->getItemInfo()->isDsrcType()){
            painter.setPen(QPen(p->getItemInfo()->mSrcAColor,1,Qt::SolidLine));
            painter.drawText(nameRect.x()+nameRect.width(),
                             rName.y(),
                             nameRect.width(),
                             rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter,
                             p->getItemInfo()->mSrcAStr);

            QImage imgA;
            imgA.load(p->getItemInfo()->mSrcAEdgeIcon);
            painter.drawImage(rectType1.x()+rectType1.width()+1, rName.y()+4, imgA);

            painter.setPen(QPen(p->getItemInfo()->mSymbolColor,1,Qt::SolidLine));

            painter.drawText(rectType1.x()+rectType1.width()+3+imgA.width(),
                             rName.y(),rectType1.width(),
                             rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter,
                             "-");

            painter.setPen(QPen(p->getItemInfo()->mSrcBColor,1,Qt::SolidLine));
            painter.drawText(rectType1.x()+rectType1.width()+11+imgA.width(),
                             rName.y(),rectType1.width(),
                             rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter,
                             p->getItemInfo()->mSrcBStr);

            QImage imgB;
            imgB.load(p->getItemInfo()->mSrcBEdgeIcon);
            painter.drawImage(rectType_2.x()+rectType_2.width()+12+imgA.width(),
                              rName.y()+4, imgB);

            painter.setPen(QPen(p->getItemInfo()->mSymbolColor, 1, Qt::SolidLine));

            painter.drawText(rectType_2.x()+rectType_2.width()+12+imgA.width()+imgB.width(),
                             rName.y(),
                             rectType_2.width(),
                             rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, ")");
        }else{
            painter.setPen(QPen(p->getItemInfo()->mSrcAColor,1,Qt::SolidLine));
            painter.drawText(nameRect.x()+nameRect.width(),
                             rName.y(),
                             nameRect.width(),
                             rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter,
                             p->getItemInfo()->mSrcAStr);

            painter.setPen(QPen(p->getItemInfo()->mSymbolColor,1,Qt::SolidLine));
            painter.drawText(rectType1.x()+rectType1.width(),
                             rName.y(),
                             rectType1.width(),
                             rName.height(),
                             Qt::AlignLeft | Qt::AlignVCenter, ")");
        }


        pen.setColor(Qt::white);
        painter.setPen(pen);

        r.setX(r.x()+112);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getCurr());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getAvg());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getMax());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getMin());

        r.setX(r.x()+86);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, p->getDev());

        val = temp.arg(p->getCnt());
        r.setX(r.x()+82);
        painter.drawText(r, Qt::AlignLeft | Qt::AlignTop, val);

        r.setY(r.y() + m_nRowH);
        r.setX(nStartX);
    }
}
