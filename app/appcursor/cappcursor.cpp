#include "../../include/dsodbg.h"
#include "../../service/servicefactory.h"
#include "../../service/service_name.h"
#include "../../service/service.h"

#include "../../gui/cguiformatter.h"

#include "../appmain/cappmain.h"

#include "cappcursor.h"

IMPLEMENT_CMD( CApp, CAppCursor )

start_of_entry()
on_set_void_void( MSG_CURSOR_S32CURSORMODE, &CAppCursor::on_cursor_mode ),

on_set_void_void( MSG_CURSOR_S23MAREA, &CAppCursor::on_cursor_manual_region ),

on_set_void_void( MSG_CURSOR_S32MTIMEUNIT, &CAppCursor::on_cursor_manual_hunit ),
on_set_void_void( MSG_CURSOR_S32MVUNIT,    &CAppCursor::on_cursor_manual_vunit ),
on_set_void_void( MSG_CURSOR_PMHRANGE, &CAppCursor::on_cursor_manual_hrange ),
on_set_void_void( MSG_CURSOR_PMVRANGE, &CAppCursor::on_cursor_manual_vrange ),

on_set_void_void( MSG_CURSOR_S32MANUALSRC, &CAppCursor::on_cursor_manual_src),
on_set_void_void( MSG_CURSOR_LATYPE,       &CAppCursor::on_cursor_manual_src),

on_set_void_void( MSG_CURSOR_S32TASRC, &CAppCursor::on_cursor_track_srca ),
on_set_void_void( MSG_CURSOR_S32TBSRC, &CAppCursor::on_cursor_track_srcb ),

on_set_void_void( MSG_CURSOR_TRACK_MODE, &CAppCursor::on_cursor_track_srca ),
on_set_void_void( MSG_CURSOR_TRACK_MODE, &CAppCursor::on_cursor_track_srcb ),

on_set_void_void( MSG_CURSOR_XY_MODE,  &CAppCursor::on_cursor_xy_mode ),
on_set_void_void( MSG_CURSOR_LISAJOUS, &CAppCursor::on_cursor_xy_lisajous ),

on_set_void_void( servCursor::cmd_cursor_raise_ax, &CAppCursor::on_cursor_raise_ha ),
on_set_void_void( servCursor::cmd_cursor_raise_bx, &CAppCursor::on_cursor_raise_hb ),
on_set_void_void( servCursor::cmd_cursor_raise_ay, &CAppCursor::on_cursor_raise_va ),
on_set_void_void( servCursor::cmd_cursor_raise_by, &CAppCursor::on_cursor_raise_vb ),

on_set_void_void( servCursor::cmd_manual_pos_update, &CAppCursor::on_cursor_manual_pos_update ),
on_set_void_void( servCursor::cmd_manual_la_update,  &CAppCursor::on_cursor_manual_la_update),

on_set_void_void( servCursor::cmd_track_ya_value_update, &CAppCursor::on_cursor_track_ya_value_update ),
on_set_void_void( servCursor::cmd_track_yb_value_update, &CAppCursor::on_cursor_track_yb_value_update ),

on_set_void_void( servCursor::cmd_track_xa_value_update, &CAppCursor::on_cursor_track_xa_value_update ),
on_set_void_void( servCursor::cmd_track_xb_value_update, &CAppCursor::on_cursor_track_xb_value_update ),

on_set_void_void( servCursor::cmd_track_delta_value_update, &CAppCursor::on_cursor_track_delta_value_update ),
on_set_void_void( servCursor::cmd_track_delta_attr_changed, &CAppCursor::on_cursor_track_delta_useable_update ),

on_set_void_void( servCursor::cmd_xy_value_update, &CAppCursor::on_cursor_xy_value_update ),

on_set_void_void( servCursor::cmd_xy_track_value_update, &CAppCursor::on_cursor_xy_track_value_update ),

on_set_void_void( servCursor::cmd_xy_pos_changed, &CAppCursor::on_cursor_xy_pos_changed ),
on_set_void_void( servCursor::cmd_xy_hpos_changed, &CAppCursor::on_cursor_xy_hpos_changed ),

on_set_void_void( servCursor::cmd_auto_x_track, &CAppCursor::on_cursor_auto_x_changed ),
on_set_void_void( servCursor::cmd_auto_y_track, &CAppCursor::on_cursor_auto_y_changed ),

on_set_void_void( servCursor::cmd_zoom_en_change, &CAppCursor::on_cursor_zoom_change),
on_set_void_void( servCursor::cmd_screen_mode_change,&CAppCursor::on_scr_mode_change),
on_set_void_void( servCursor::cmd_set_item_update,&CAppCursor::on_set_item_update ),
//! system
on_set_void_void( CMD_SERVICE_ITEM_FOCUS, &CAppCursor::onFocus ),

on_set_void_void( CMD_SERVICE_RST,       &CAppCursor::rst ),

end_of_entry()

void appCursor_style::load(const QString &path,
                           const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "wnd", mRect );
}

CAppCursor::CAppCursor()
{
    mservName = serv_name_cursor;

    m_pLisajous = NULL;
    attachPopupWindow( (QWidget**)(&m_pLisajous), MSG_CURSOR_LISAJOUS );

    m_pModalWnd = NULL;

    _style.loadResource("appcursor/", r_meta::CAppMeta() );

    //! fill map
    //! manual
    mMsgFocusMap.insert( MSG_CURSOR_S32MHAPOS,
                         cursor_ui::cursor_control_ax );
    mMsgFocusMap.insert( MSG_CURSOR_S32MHBPOS,
                         cursor_ui::cursor_control_bx );
    mMsgFocusMap.insert( MSG_CURSOR_S32MHABPOS,
                         cursor_ui::cursor_control_abx );

    mMsgFocusMap.insert( MSG_CURSOR_S32MVAPOS,
                         cursor_ui::cursor_control_ay );
    mMsgFocusMap.insert( MSG_CURSOR_S32MVBPOS,
                         cursor_ui::cursor_control_by );
    mMsgFocusMap.insert( MSG_CURSOR_S32MVABPOS,
                         cursor_ui::cursor_control_aby );

    //! track
    mMsgFocusMap.insert( MSG_CURSOR_S32TAPOS,
                         cursor_ui::cursor_control_ax );
    mMsgFocusMap.insert( MSG_CURSOR_S32TBPOS,
                         cursor_ui::cursor_control_bx );
    mMsgFocusMap.insert( MSG_CURSOR_S32TABPOS,
                         cursor_ui::cursor_control_abx );

    mMsgFocusMap.insert( MSG_CURSOR_S32TAPOS_V,
                         cursor_ui::cursor_control_ay );
    mMsgFocusMap.insert( MSG_CURSOR_S32TBPOS_V,
                         cursor_ui::cursor_control_by );
    mMsgFocusMap.insert( MSG_CURSOR_S32TABPOS_V,
                         cursor_ui::cursor_control_aby );

    //! xy
    mMsgFocusMap.insert( MSG_CURSOR_S32XY_XA,
                         cursor_ui::cursor_control_ax );
    mMsgFocusMap.insert( MSG_CURSOR_S32XY_XB,
                         cursor_ui::cursor_control_bx );
    mMsgFocusMap.insert( MSG_CURSOR_S32XY_XAB,
                         cursor_ui::cursor_control_abx );

    mMsgFocusMap.insert( MSG_CURSOR_S32XY_YA,
                         cursor_ui::cursor_control_ay );
    mMsgFocusMap.insert( MSG_CURSOR_S32XY_YB,
                         cursor_ui::cursor_control_by );
    mMsgFocusMap.insert( MSG_CURSOR_S32XY_YAB,
                         cursor_ui::cursor_control_aby );

    mMsgFocusMap.insert( MSG_CURSOR_S32XY_TRACK_AX,
                         cursor_ui::cursor_control_t1 );
    mMsgFocusMap.insert( MSG_CURSOR_S32XY_TRACK_BX,
                         cursor_ui::cursor_control_t2 );
    mMsgFocusMap.insert( MSG_CURSOR_S32XY_TRACK_ABX,
                         cursor_ui::cursor_control_abt );
}

void CAppCursor::setupUi( CAppMain *pMain )
{
    Q_ASSERT( NULL != pMain );

    m_pCent = pMain->getCentBar();
    Q_ASSERT( NULL != m_pCent );

    //! quick link config
    addQuick( MSG_CURSOR,
              QStringLiteral("ui/desktop/cursor/icon/") );

    //! cursor wnd
    m_pCursorWnd = new cursor_ui::CCursorWnd();
    Q_ASSERT( NULL != m_pCursorWnd );
    m_pCursorWnd->setupUi();

    //! ref line
    m_pLineRefAx = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x );
    Q_ASSERT( NULL != m_pLineRefAx );
    m_pLineRefAx->setColor(Qt::blue);

    m_pLineRefBx = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x );
    Q_ASSERT( NULL != m_pLineRefBx );
    m_pLineRefBx->setColor(Qt::blue);

    m_pLineRefAy = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y );
    Q_ASSERT( NULL != m_pLineRefAy );
    m_pLineRefAy->setColor(Qt::blue);

    m_pLineRefBy = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y );
    Q_ASSERT( NULL != m_pLineRefBy );
    m_pLineRefBy->setColor(Qt::blue);

    //! style
    m_pLineRefAx->setStyle( cursor_ui::cursor_style_solid );
    m_pLineRefAy->setStyle( cursor_ui::cursor_style_solid );

    m_pLineRefBx->setStyle( cursor_ui::cursor_style_dot );
    m_pLineRefBy->setStyle( cursor_ui::cursor_style_dot );

    //! parent
    m_pLineRefAx->setParent( m_pCent );
    m_pLineRefAy->setParent( m_pCent );

    m_pLineRefBx->setParent( m_pCent );
    m_pLineRefBy->setParent( m_pCent );

    //! cursor line
    m_pLineAX = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x );
    Q_ASSERT( NULL != m_pLineAX );
    m_pLineAX->setHasTab(true);
    m_pLineBX = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x );
    Q_ASSERT( NULL != m_pLineBX );
    m_pLineBX->setHasTab(true);

    m_pLineAY = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y );
    Q_ASSERT( NULL != m_pLineAY );
    m_pLineAY->setHasTab(true);
    m_pLineBY = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y );
    Q_ASSERT( NULL != m_pLineBY );
    m_pLineBY->setHasTab(true);

    //! style
    m_pLineAX->setStyle( cursor_ui::cursor_style_solid );
    m_pLineAY->setStyle( cursor_ui::cursor_style_solid );

    m_pLineBX->setStyle( cursor_ui::cursor_style_dot );
    m_pLineBY->setStyle( cursor_ui::cursor_style_dot );

    //! parent
    m_pLineAX->setParent( m_pCent );
    m_pLineAY->setParent( m_pCent );

    m_pLineBX->setParent( m_pCent );
    m_pLineBY->setParent( m_pCent );

    //!
    m_pCrossXA = new cursor_ui::CCursorCross( cursor_ui::cursor_line_x, m_pCent);
    Q_ASSERT( NULL != m_pCrossXA );
    m_pCrossXB = new cursor_ui::CCursorCross( cursor_ui::cursor_line_x, m_pCent);
    Q_ASSERT( NULL != m_pCrossXB );

    m_pCrossYA = new cursor_ui::CCursorCross( cursor_ui::cursor_line_y, m_pCent);
    Q_ASSERT( NULL != m_pCrossYA );
    m_pCrossYB = new cursor_ui::CCursorCross( cursor_ui::cursor_line_y, m_pCent );
    Q_ASSERT( NULL != m_pCrossYB );

    //! style
    m_pCrossXA->setStyle( cursor_ui::cursor_style_solid );
    m_pCrossXB->setStyle( cursor_ui::cursor_style_dot );

    //! parent
    m_pCrossXA->setParent( m_pCent );
    m_pCrossXB->setParent( m_pCent );

    //! style
    m_pCrossYA->setStyle( cursor_ui::cursor_style_solid );
    m_pCrossYB->setStyle( cursor_ui::cursor_style_dot );

    //! parent
    m_pCrossYA->setParent( m_pCent );
    m_pCrossYB->setParent( m_pCent );

    //! wnd at top
    m_pCursorWnd->setParent( m_pCent );

    //! hide all
    m_pLineAX->setShow( false );
    m_pLineAY->setShow( false );

    m_pLineBX->setShow( false );
    m_pLineBY->setShow( false );

    m_pLineRefAx->setShow( false );
    m_pLineRefAy->setShow( false );

    m_pLineRefBx->setShow( false );
    m_pLineRefBy->setShow( false );

    m_pCrossXA->setShow( false );
    m_pCrossXA->setServEnabled( false );
    m_pCrossXB->setShow( false );
    m_pCrossXB->setServEnabled( false );

    m_pCrossYA->setShow( false );
    m_pCrossYA->setServEnabled( false );
    m_pCrossYB->setShow( false );
    m_pCrossYB->setServEnabled( false );

    sysAttachUIView( m_pLineAY, view_yt_main );
    sysAttachUIView( m_pLineBY, view_yt_main );
    sysAttachUIView( m_pLineAX, view_yt_main );
    sysAttachUIView( m_pLineBX, view_yt_main );
    sysAttachUIView( m_pLineRefAx, view_yt_main );
    sysAttachUIView( m_pLineRefBx, view_yt_main );
    sysAttachUIView( m_pLineRefAy, view_yt_main );
    sysAttachUIView( m_pLineRefBy, view_yt_main );
    sysAttachUIView( m_pCrossXA, view_yt_main );
    sysAttachUIView( m_pCrossXB, view_yt_main );
    sysAttachUIView( m_pCrossYA, view_yt_main );
    sysAttachUIView( m_pCrossYB, view_yt_main );

    m_pLisajous = new cursor_ui::CCursorLisajous();
    Q_ASSERT( m_pLisajous != NULL );
    m_pLisajous->hide();

    m_pCursorWnd->hide( );
}

void CAppCursor::buildConnection()
{}

void CAppCursor::resize()
{
    m_pCursorWnd->setGeometry( _style.mRect );
}

void CAppCursor::rst()
{
    m_pCursorWnd->setGeometry( _style.mRect );
}

void CAppCursor::on_cursor_mode(  )
{
    int cursorMode;
    query( MSG_CURSOR_S32CURSORMODE, cursorMode );

    //! wnd
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_ax, true );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay, true );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_bx, true );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_by, true );

    m_pCursorWnd->setVisible( cursor_ui::cursor_control_dx, true );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy, true );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx,   true );
    //! manual la
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_a_d7_d0,  false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_a_d15_d8, false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_b_d7_d0,  false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_b_d15_d8, false );

    m_pCursorWnd->setVisible( cursor_ui::cursor_control_t1,       false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_t2,       false );

    //! enter
    if ( cursorMode == cursor_mode_manual )
    {   
        //! cross
        m_pCrossXA->setShow( false );
        m_pCrossXA->setServEnabled( false );
        m_pCrossXB->setShow( false );
        m_pCrossXB->setServEnabled(false);
        m_pCrossYA->setShow( false );
        m_pCrossYA->setServEnabled( false );
        m_pCrossYB->setShow( false );
        m_pCrossYB->setServEnabled(false);

        //! abx
        on_cursor_manual_region();
        on_cursor_manual_src();
        //! ref
        on_cursor_manual_hunit();
        on_cursor_manual_vunit();
    }
    else if ( cursorMode == cursor_mode_track )
    {
        //! ref
        m_pLineRefAx->setShow( false );
        m_pLineRefAy->setShow( false );

        m_pLineRefBx->setShow( false );
        m_pLineRefBy->setShow( false );

        m_pCursorWnd->changeTitle( chan1 );//! 防止从手动的la光标切换到追踪时显示错误

        m_pCursorWnd->show();

        on_zoom_track_en();

        //! a
        on_cursor_track_srca();
        //! b
        on_cursor_track_srcb();

        on_cursor_track_delta_useable_update();
    }
    else if ( cursorMode == cursor_mode_xy )
    {
        //! ref
        m_pLineRefAx->setShow( false );
        m_pLineRefAy->setShow( false );

        m_pLineRefBx->setShow( false );
        m_pLineRefBy->setShow( false );

        m_pCursorWnd->changeTitle( chan1 );//! 防止从手动的la光标切换到追踪时显示错误
        on_cursor_xy_mode();
        m_pCursorWnd->show();
    }
    else
    {
        m_pLineAX->setShow( false );
        m_pLineAY->setShow( false );

        m_pLineBX->setShow( false );
        m_pLineBY->setShow( false );

        m_pLineRefAx->setShow( false );
        m_pLineRefAy->setShow( false );

        m_pLineRefBx->setShow( false );
        m_pLineRefBy->setShow( false );

        m_pCrossXA->setShow( false );
        m_pCrossXA->setServEnabled( false );
        m_pCrossXB->setShow( false );
        m_pCrossXB->setServEnabled( false );
        m_pCrossYA->setShow( false );
        m_pCrossYA->setServEnabled( false );
        m_pCrossYB->setShow( false );
        m_pCrossYB->setServEnabled( false );

//        m_pCursorWnd->changeLayout();
        m_pCursorWnd->hide();
        m_pLisajous->hide();
    }
}

void CAppCursor::on_cursor_manual_hunit(  )
{
    if ( !isMatch( cursor_mode_manual ) )
    {
        return;
    }

    int src;
    query( MSG_CURSOR_S32MANUALSRC, src );
    if ( src == chan_none )
    {
        return;
    }

    on_manual_hRef_Visible();
}

void CAppCursor::on_manual_hRef_Visible()
{
    int unit = 0;
    query( MSG_CURSOR_S32MTIMEUNIT, unit );
    if ( unit == Unit_degree || unit == Unit_percent )
    {
        m_pLineRefAx->setShow( true );
        m_pLineRefBx->setShow( true );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx, false );
        on_cursor_manual_hrange( );
    }
    else
    {
        m_pLineRefAx->setShow( false );
        m_pLineRefBx->setShow( false );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx, true );
    }
}

void CAppCursor::on_cursor_manual_vunit(  )
{
    int src;
    query( MSG_CURSOR_S32MANUALSRC, src );
    if ( src == chan_none )
    {
        return;
    }

    on_manual_vRef_Visible();
}

void CAppCursor::on_manual_vRef_Visible()
{
    int unit;
    query( MSG_CURSOR_S32MVUNIT, unit );
    if ( unit == Unit_percent )
    {
        m_pLineRefAy->setShow( true );
        m_pLineRefBy->setShow( true );
        on_cursor_manual_vrange( );
    }
    else
    {
        m_pLineRefAy->setShow( false );
        m_pLineRefBy->setShow( false );
    }
}


void CAppCursor::on_cursor_manual_src()
{
    if  ( !isMatch( cursor_mode_manual ) ) return;

    int src;
    query( MSG_CURSOR_S32MANUALSRC, src );

    //! manual la
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay,       true);
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_by,       true);
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_a_d7_d0,  false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_a_d15_d8, false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_b_d7_d0,  false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_b_d15_d8, false );
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx,   true);
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy,       true);

    if ( src == chan_none )
    {
        m_pLineAX->setShow( false );
        m_pLineAY->setShow( false );
        m_pLineBX->setShow( false );
        m_pLineBY->setShow( false );
        m_pLineRefAx->setShow( false );
        m_pLineRefAy->setShow( false );
        m_pLineRefBx->setShow( false );
        m_pLineRefBy->setShow( false );

        m_pCursorWnd->changeTitle((Chan) src);
        m_pCursorWnd->hide();
    }
    else if( src == la)
    {
        m_pLineAX->setShow( true );
        m_pLineAY->setShow( false );
        m_pLineBX->setShow( true );
        m_pLineBY->setShow( false );

        on_manual_hRef_Visible();
        on_manual_vRef_Visible();

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy,       false);
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx,   false );

        bool laResType;
        query( MSG_CURSOR_LATYPE, laResType );
        if(laResType == LA_BIN_Val)
        {
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay,       false);
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_by,       false);
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_a_d7_d0,  true );
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_a_d15_d8, true );
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_b_d7_d0,  true );
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_b_d15_d8, true );
        }
        m_pCursorWnd->changeTitle(la);
        m_pCursorWnd->show();
    }
    else
    {
        m_pLineAX->setShow( true );
        m_pLineAY->setShow( true );
        m_pLineBX->setShow( true );
        m_pLineBY->setShow( true );

        on_manual_hRef_Visible();
        on_manual_vRef_Visible();

        m_pCursorWnd->changeTitle( (Chan) src);
        m_pCursorWnd->show();
    }
    //! add_by_zx for bug 2560:service中的src使能变化触发了这个函数导致窗口显示错误
    on_cursor_manual_pos_update();
}

void CAppCursor::on_cursor_manual_region()
{
    if ( !isMatch( cursor_mode_manual ) )
    {
        return;
    }

    bool area;
    query( MSG_CURSOR_S23MAREA, area );

    if ( area == cursor_area_zoom )
    {
        sysAttachUIView( m_pLineAY, view_yt_zoom );
        sysAttachUIView( m_pLineBY, view_yt_zoom );
        sysAttachUIView( m_pLineAX, view_yt_zoom );
        sysAttachUIView( m_pLineBX, view_yt_zoom );

        sysAttachUIView( m_pLineRefAx, view_yt_zoom );
        sysAttachUIView( m_pLineRefBx, view_yt_zoom );
        sysAttachUIView( m_pLineRefAy, view_yt_zoom );
        sysAttachUIView( m_pLineRefBy, view_yt_zoom );
    }
    else
    {
        LOG_DBG();
        sysAttachUIView( m_pLineAY, view_yt_main );
        sysAttachUIView( m_pLineBY, view_yt_main );
        sysAttachUIView( m_pLineAX, view_yt_main );
        sysAttachUIView( m_pLineBX, view_yt_main );

        sysAttachUIView( m_pLineRefAx, view_yt_main );
        sysAttachUIView( m_pLineRefBx, view_yt_main );
        sysAttachUIView( m_pLineRefAy, view_yt_main );
        sysAttachUIView( m_pLineRefBy, view_yt_main );
    }
    LOG_DBG();
#if 1
    m_pLineAY->setShow( m_pLineAY->getShow() );
    m_pLineBY->setShow( m_pLineBY->getShow() );
    m_pLineAX->setShow( m_pLineAX->getShow() );
    m_pLineBX->setShow( m_pLineBX->getShow() );
    m_pLineRefAx->setShow( m_pLineRefAx->getShow() );
    m_pLineRefBx->setShow( m_pLineRefBx->getShow() );
    m_pLineRefAy->setShow( m_pLineRefAy->getShow() );
    m_pLineRefBy->setShow( m_pLineRefBy->getShow() );
#endif
}

void CAppCursor::on_cursor_manual_vrange(  )
{
    int pos;
    query( servCursor::cmd_ref_ay, pos );
    m_pLineRefAy->setPos( pos );
    query( servCursor::cmd_ref_by, pos );
    m_pLineRefBy->setPos( pos );
}

void CAppCursor::on_cursor_manual_hrange(  )
{
    int pos;

    query( servCursor::cmd_ref_ax, pos );
    m_pLineRefAx->setPos( pos );

    query( servCursor::cmd_ref_bx, pos );
    m_pLineRefBx->setPos( pos );
}

void CAppCursor::on_cursor_manual_pos_update(  )
{
    //! line
    cursorUpdate( cursor_ui::cursor_control_ax,
                  m_pLineAX,
                  MSG_CURSOR_S32MHAPOS,
                  servCursor::cmd_ax_real );

    cursorUpdate( cursor_ui::cursor_control_bx,
                  m_pLineBX,
                  MSG_CURSOR_S32MHBPOS,
                  servCursor::cmd_bx_real );



    cursorUpdate( cursor_ui::cursor_control_ay,
                  m_pLineAY,
                  MSG_CURSOR_S32MVAPOS,
                  servCursor::cmd_ay_real );

    cursorUpdate( cursor_ui::cursor_control_by,
                  m_pLineBY,
                  MSG_CURSOR_S32MVBPOS,
                  servCursor::cmd_by_real );

    //! dx
    cursorUpdate( cursor_ui::cursor_control_dx,
                  NULL,
                  0,
                  servCursor::cmd_dx_real );

    cursorUpdate( cursor_ui::cursor_control_dy,
                  NULL,
                  0,
                  servCursor::cmd_dy_real );

    cursorUpdate( cursor_ui::cursor_control_inv_dx,
                  NULL,
                  0,
                  servCursor::cmd_inv_dx_real );
}

void CAppCursor::on_cursor_manual_la_update()
{
    if ( !isMatch( la, MSG_CURSOR_S32MANUALSRC ) )
    {
        return;
    }

    cursorLaUpdate( cursor_ui::cursor_control_ay,
                    servCursor::cmd_ay_real);
    cursorLaUpdate( cursor_ui::cursor_control_by,
                    servCursor::cmd_by_real);
}

void CAppCursor::on_zoom_track_en()
{
    LOG_DBG();
    //! only for track mode
    bool zoomEn;
    query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);
    if(zoomEn)
    {
        sysAttachUIView( m_pLineAY, view_yt_zoom );
        sysAttachUIView( m_pLineBY, view_yt_zoom );
        sysAttachUIView( m_pLineAX, view_yt_zoom );
        sysAttachUIView( m_pLineBX, view_yt_zoom );
        sysAttachUIView( m_pCrossXA, view_yt_zoom );
        sysAttachUIView( m_pCrossXB, view_yt_zoom );
        sysAttachUIView( m_pCrossYA, view_yt_zoom );
        sysAttachUIView( m_pCrossYB, view_yt_zoom );
    }
    else
    {
        sysAttachUIView( m_pLineAY, view_yt_main );
        sysAttachUIView( m_pLineBY, view_yt_main );
        sysAttachUIView( m_pLineAX, view_yt_main );
        sysAttachUIView( m_pLineBX, view_yt_main );
        sysAttachUIView( m_pCrossXA, view_yt_main );
        sysAttachUIView( m_pCrossXB, view_yt_main );
        sysAttachUIView( m_pCrossYA, view_yt_main );
        sysAttachUIView( m_pCrossYB, view_yt_main );
    }
}

//! fft半屏
void CAppCursor::on_scr_mode_change()
{
    int temp;
    DsoScreenMode scrMode;
    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, temp);
    scrMode = (DsoScreenMode) temp;

    if(scrMode == screen_yt_main_zfft)
    {
        int srcA;
        query(MSG_CURSOR_S32TASRC, srcA);
        if( srcA > chan4)
        {
            sysAttachUIView(m_pLineAX, view_ygnd_zoom);
            sysAttachUIView(m_pLineAY, view_ygnd_zoom);
            sysAttachUIView(m_pCrossXA, view_ygnd_zoom);
            sysAttachUIView(m_pCrossYA, view_ygnd_zoom);
        }
        else
        {
            sysAttachUIView(m_pLineAX, view_ygnd_main);
            sysAttachUIView(m_pLineAY, view_ygnd_main);
            sysAttachUIView(m_pCrossXA, view_ygnd_main);
            sysAttachUIView(m_pCrossYA, view_ygnd_main);
        }

        int srcB;
        query(MSG_CURSOR_S32TASRC, srcB);
        if( srcB > chan4)
        {
            sysAttachUIView(m_pLineBX, view_ygnd_zoom);
            sysAttachUIView(m_pLineBY, view_ygnd_zoom);
            sysAttachUIView(m_pCrossXB, view_ygnd_zoom);
            sysAttachUIView(m_pCrossYB, view_ygnd_zoom);
        }
        else
        {
            sysAttachUIView(m_pLineBX, view_ygnd_main);
            sysAttachUIView(m_pLineBY, view_ygnd_main);
            sysAttachUIView(m_pCrossXB, view_ygnd_main);
            sysAttachUIView(m_pCrossYB, view_ygnd_main);
        }
    }
    else
    {
        on_cursor_zoom_change();
    }
}

void CAppCursor::on_cursor_track_srca(  )
{
    if  ( !isMatch( cursor_mode_track ) ) return;

    int src;
    query( MSG_CURSOR_S32TASRC, src );

    m_pCrossXA->setShow( false );
    m_pCrossXA->setServEnabled( false );
    m_pCrossYA->setShow( false );
    m_pCrossYA->setServEnabled( false );

    //! no souce
    if ( src == chan_none )
    {
        m_pLineAX->setShow( false );
        m_pLineAY->setShow( false );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_ax, false );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay, false );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dx, false );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy, false );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx, false );
    }
    else
    {
        bool mode;
        query( MSG_CURSOR_TRACK_MODE, mode );
        if ( mode == cursor_lib::cursor_track_y )
        {   
            m_pLineAX->setShow( true );
            m_pLineAY->setShow( false );

            m_pCursorWnd->setVisible( cursor_ui::cursor_control_ax, true );
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay, true );

            m_pCrossYA->setShow( true );
            m_pCrossYA->setServEnabled( true );
        }
        else
        {
            m_pLineAX->setShow( false );
            m_pLineAY->setShow( true );

            m_pCursorWnd->setVisible( cursor_ui::cursor_control_ax, true );
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay, true );

            m_pCrossXA->setShow( true );
            m_pCrossXA->setServEnabled( true );
        }

        m_pCursorWnd->show();
    }

    int temp;
    DsoScreenMode scrMode;
    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, temp);
    scrMode = (DsoScreenMode) temp;

    if(scrMode == screen_yt_main_zfft)
    {
        int srcA;
        query(MSG_CURSOR_S32TASRC, srcA);
        if( srcA > chan4)
        {
            sysAttachUIView(m_pLineAX, view_ygnd_zoom);
            sysAttachUIView(m_pLineAY, view_ygnd_zoom);
            sysAttachUIView(m_pCrossXA, view_ygnd_zoom);
            sysAttachUIView(m_pCrossYA, view_ygnd_zoom);
        }
        else
        {
            sysAttachUIView(m_pLineAX, view_ygnd_main);
            sysAttachUIView(m_pLineAY, view_ygnd_main);
            sysAttachUIView(m_pCrossXA, view_ygnd_main);
            sysAttachUIView(m_pCrossYA, view_ygnd_main);
        }
    }
}

void CAppCursor::on_cursor_track_srcb(  )
{
    if  ( !isMatch( cursor_mode_track ) ) return;

    int src;
    query( MSG_CURSOR_S32TBSRC, src );

    m_pCrossXB->setShow( false );
    m_pCrossXB->setServEnabled( false );
    m_pCrossYB->setShow( false );
    m_pCrossYB->setServEnabled( false );

    if ( src == chan_none )
    {
        m_pLineBX->setShow( false );
        m_pLineBY->setShow( false );
        m_pCrossXB->setShow( false );
        m_pCrossYB->setShow( false );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_bx, false );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_by, false );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dx, false );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy, false );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx, false );
    }
    else
    {
        bool mode;
        query( MSG_CURSOR_TRACK_MODE, mode );
        if ( mode == cursor_lib::cursor_track_y )
        {
            m_pLineBX->setShow( true );
            m_pLineBY->setShow( false );

            m_pCursorWnd->setVisible( cursor_ui::cursor_control_bx, true );
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_by, true );
//            m_pCursorWnd->changeLayout();
            m_pCrossYB->setShow( true );
            m_pCrossYB->setServEnabled( true );
        }
        else
        {
            m_pLineBX->setShow( false );
            m_pLineBY->setShow( true );

            m_pCursorWnd->setVisible( cursor_ui::cursor_control_bx, true );
            m_pCursorWnd->setVisible( cursor_ui::cursor_control_by, true );
//          m_pCursorWnd->changeLayout();
            m_pCrossXB->setShow( true );
            m_pCrossXB->setServEnabled( true );
        }

        m_pCursorWnd->show();
    }

    int temp;
    DsoScreenMode scrMode;
    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, temp);
    scrMode = (DsoScreenMode) temp;

    if(scrMode == screen_yt_main_zfft)
    {
        int srcB;
        query(MSG_CURSOR_S32TASRC, srcB);
        if( srcB > chan4)
        {
            sysAttachUIView(m_pLineBX, view_ygnd_zoom);
            sysAttachUIView(m_pLineBY, view_ygnd_zoom);
            sysAttachUIView(m_pCrossXB, view_ygnd_zoom);
            sysAttachUIView(m_pCrossYB, view_ygnd_zoom);
        }
        else
        {
            sysAttachUIView(m_pLineBX, view_ygnd_main);
            sysAttachUIView(m_pLineBY, view_ygnd_main);
            sysAttachUIView(m_pCrossXB, view_ygnd_main);
            sysAttachUIView(m_pCrossYB, view_ygnd_main);
        }
    }
}

void CAppCursor::on_cursor_track_ya_value_update(  )
{
    //! line
    cursorUpdate( cursor_ui::cursor_control_ax,
                  m_pLineAX,
                  MSG_CURSOR_S32TAPOS,
                  servCursor::cmd_track_ax_real );

    cursorUpdate( cursor_ui::cursor_control_ay,
                  NULL,
                  0,
                  servCursor::cmd_track_ay_real );

    //! cross
    crossYUpdate( m_pCrossYA,
                  MSG_CURSOR_S32TAPOS,
                  servCursor::cmd_track_ay_track );
}

void CAppCursor::on_cursor_track_yb_value_update(  )
{
    cursorUpdate( cursor_ui::cursor_control_bx,
                  m_pLineBX,
                  MSG_CURSOR_S32TBPOS,
                  servCursor::cmd_track_bx_real );

    cursorUpdate( cursor_ui::cursor_control_by,
                  NULL,
                  0,
                  servCursor::cmd_track_by_real );

    crossYUpdate( m_pCrossYB,
                  MSG_CURSOR_S32TBPOS,
                  servCursor::cmd_track_by_track );
}

void CAppCursor::on_cursor_track_xa_value_update(  )
{
    cursorUpdate( cursor_ui::cursor_control_ax,
                  NULL,
                  0,
                  servCursor::cmd_track_ax_real );

    cursorUpdate( cursor_ui::cursor_control_ay,
                  m_pLineAY,
                  MSG_CURSOR_S32TAPOS_V,
                  servCursor::cmd_track_ay_real );

    crossXUpdate( m_pCrossXA,
                  MSG_CURSOR_S32TAPOS_V,
                  servCursor::cmd_track_ax_track );
}
void CAppCursor::on_cursor_track_xb_value_update(  )
{
    cursorUpdate( cursor_ui::cursor_control_bx,
                  NULL,
                  0,
                  servCursor::cmd_track_bx_real );

    cursorUpdate( cursor_ui::cursor_control_by,
                  m_pLineBY,
                  MSG_CURSOR_S32TBPOS_V,
                  servCursor::cmd_track_by_real );

    crossXUpdate( m_pCrossXB,
                  MSG_CURSOR_S32TBPOS_V,
                  servCursor::cmd_track_bx_track );
}

void CAppCursor::on_cursor_track_delta_value_update(  )
{
    cursorUpdate( cursor_ui::cursor_control_dx,
                  NULL,
                  0,
                  servCursor::cmd_track_dx_real );

    cursorUpdate( cursor_ui::cursor_control_dy,
                  NULL,
                  0,
                  servCursor::cmd_track_dy_real );

    cursorUpdate( cursor_ui::cursor_control_inv_dx,
                  NULL,
                  0,
                  servCursor::cmd_track_inv_dx_real );
}

void CAppCursor::on_cursor_track_delta_useable_update()
{
    void *pSmartPtr;
    cursor_lib::deltaUseable *pUseable;

    query( servCursor::cmd_track_delta_attr_changed, pSmartPtr );

    pUseable = (cursor_lib::deltaUseable *)pSmartPtr;

    m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx, pUseable->invXUsable);
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_dx, pUseable->xUsable);
    m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy, pUseable->yUsable);
}

//! id
//! arg
//! bool isVisible true:visable
//! QColor
void CAppCursor::on_cursor_auto_x_changed()
{
    LOG_DBG();
    void* ptr;
    query(mservName,servCursor::cmd_auto_x_changed, ptr);
    QVector<cursor_lib::AutoLineAttr* >* xAutoLineAttr = static_cast<QVector<cursor_lib::AutoLineAttr*>* > (ptr);

    while(xAutoLine.size() < xAutoLineAttr->size())
    {
        xAutoLine.push_back( createAutoXLine() );
    }

    for(int i = 0;i < xAutoLine.size();i++)
    {
        xAutoLine[i]->setShow(xAutoLineAttr->at(i)->bVisible);
        xAutoLine[i]->setPos(xAutoLineAttr->at(i)->mPos);
    }
}

void CAppCursor::on_cursor_auto_y_changed()
{
    LOG_DBG();
    void* ptr;
    query(mservName,servCursor::cmd_auto_y_changed, ptr);
    QVector<cursor_lib::AutoLineAttr* >* yAutoLineAttr = static_cast<QVector<cursor_lib::AutoLineAttr*>* > (ptr);

    while(yAutoLine.size() < yAutoLineAttr->size())
    {
        yAutoLine.push_back( createAutoYLine() );
    }

    for(int i = 0;i < yAutoLine.size();i++)
    {
        yAutoLine[i]->setShow(yAutoLineAttr->at(i)->bVisible);
        yAutoLine[i]->setPos(yAutoLineAttr->at(i)->mPos);
    }
}

void CAppCursor::on_cursor_zoom_change()
{
    LOG_DBG();
    if  ( !isMatch( cursor_mode_track ) ) return;
    LOG_DBG();
    on_zoom_track_en();
}

cursor_ui::CCursorLine *CAppCursor::createAutoXLine()
{
    cursor_ui::CCursorLine* pXline = new cursor_ui::CCursorLine( cursor_ui::cursor_line_x );
    Q_ASSERT( NULL != pXline );
    pXline->setStyle( cursor_ui::cursor_style_solid );
    pXline->setParent( m_pCent );
    pXline->setVisible(false);
    sysAttachUIView(pXline, view_yt_main);
    return pXline;
}

cursor_ui::CCursorLine *CAppCursor::createAutoYLine()
{
    cursor_ui::CCursorLine* pYline = new cursor_ui::CCursorLine( cursor_ui::cursor_line_y );
    Q_ASSERT( NULL != pYline );
    pYline->setStyle( cursor_ui::cursor_style_solid );
    pYline->setParent( m_pCent );
    pYline->setVisible(false);
    sysAttachUIView(pYline, view_yt_main);
    return pYline;
}

void CAppCursor::on_cursor_xy_mode()
{   
    bool xyMode;
    query( MSG_CURSOR_XY_MODE, xyMode );
    if ( xyMode == cursor_xy_track )
    {
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_ax, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_bx, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_by, true );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dx, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx, true );

        sysAttachUIView( m_pLineAX, view_xy );
        sysAttachUIView( m_pLineBX, view_xy );

        sysAttachUIView( m_pCrossYA, view_xy );
        sysAttachUIView( m_pCrossYB, view_xy );

        m_pLineAX->setShow( true );
        m_pLineBX->setShow( true );

        m_pLineAY->setShow( false );
        m_pLineBY->setShow( false );

        m_pCrossYA->setShow( true );
        m_pCrossYA->setServEnabled( true );
        m_pCrossYB->setShow( true );
        m_pCrossYB->setServEnabled( true );
    }
    else
    {
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_ax, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_bx, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_ay, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_by, true );

        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dx, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_dy, true );
        m_pCursorWnd->setVisible( cursor_ui::cursor_control_inv_dx, false );

        sysAttachUIView( m_pLineAX, view_xy );
        sysAttachUIView( m_pLineBX, view_xy );

        sysAttachUIView( m_pLineAY, view_xy );
        sysAttachUIView( m_pLineBY, view_xy );

        m_pLineAX->setShow( true );
        m_pLineBX->setShow( true );

        m_pLineAY->setShow( true );
        m_pLineBY->setShow( true );

        m_pCrossYA->setShow( false );
        m_pCrossYA->setServEnabled( false );
        m_pCrossYB->setShow( false );
        m_pCrossYB->setServEnabled( false );
    }
}

void CAppCursor::on_cursor_xy_lisajous()
{
    qDebug()<<"/*****cursor map end******/"<<getDebugTime();
    m_pLisajous->setVisible( !m_pLisajous->isVisible() );
}

void CAppCursor::on_cursor_xy_value_update()
{
    cursorUpdate( cursor_ui::cursor_control_ax,
                  NULL,
                  0,
                  servCursor::cmd_xy_ax_real );

    cursorUpdate( cursor_ui::cursor_control_bx,
                  NULL,
                  0,
                  servCursor::cmd_xy_bx_real );

    cursorUpdate( cursor_ui::cursor_control_ay,
                  NULL,
                  0,
                  servCursor::cmd_xy_ay_real );

    cursorUpdate( cursor_ui::cursor_control_by,
                  NULL,
                  0,
                  servCursor::cmd_xy_by_real );

    //! delta
    cursorUpdate( cursor_ui::cursor_control_dx,
                  NULL,
                  0,
                  servCursor::cmd_xy_dx_real );

    cursorUpdate( cursor_ui::cursor_control_dy,
                  NULL,
                  0,
                  servCursor::cmd_xy_dy_real );
}
void CAppCursor::on_set_item_update()
{
    m_pCursorWnd->changeFocus();
}
void CAppCursor::on_cursor_xy_track_value_update()
{
    for(int i = 0;i < 9;i++)
    {
        cursor_ui::CursorControls cont = cursor_ui::CursorControls
                (cursor_ui::cursor_control_begin + i);
        cursorUpdate( cont,
                    NULL,
                    0,
                    servCursor::cmd_xy_track_begin + i );
    }
    crossXYUpdate( m_pCrossYA,
                  servCursor::cmd_xy_t1x_track,
                  servCursor::cmd_xy_t1y_track );
    crossXYUpdate( m_pCrossYA,
                  servCursor::cmd_xy_t1x_track,
                  servCursor::cmd_xy_t1y_track );
    crossXYUpdate( m_pCrossYB,
                  servCursor::cmd_xy_t2x_track,
                  servCursor::cmd_xy_t2y_track );
    crossXYUpdate( m_pCrossYB,
                  servCursor::cmd_xy_t2x_track,
                  servCursor::cmd_xy_t2y_track );
}

void CAppCursor::on_cursor_xy_pos_changed()
{
    cursorUpdate( cursor_ui::cursor_control_ax,
                  m_pLineAX,
                  MSG_CURSOR_S32XY_XA,
                  servCursor::cmd_xy_ax_real );

    cursorUpdate( cursor_ui::cursor_control_bx,
                  m_pLineBX,
                  MSG_CURSOR_S32XY_XB,
                  servCursor::cmd_xy_bx_real );

    cursorUpdate( cursor_ui::cursor_control_ay,
                  m_pLineAY,
                  MSG_CURSOR_S32XY_YA,
                  servCursor::cmd_xy_ay_real );


    cursorUpdate( cursor_ui::cursor_control_by,
                  m_pLineBY,
                  MSG_CURSOR_S32XY_YB,
                  servCursor::cmd_xy_by_real );
}

void CAppCursor::on_cursor_xy_hpos_changed()
{
    cursorUpdate( cursor_ui::cursor_control_ax,
                  m_pLineAX,
                  MSG_CURSOR_S32XY_TRACK_AX,
                  servCursor::cmd_xy_t1x_real );

    cursorUpdate( cursor_ui::cursor_control_bx,
                  m_pLineBX,
                  MSG_CURSOR_S32XY_TRACK_BX,
                  servCursor::cmd_xy_t2x_real );
}

void CAppCursor::on_cursor_raise_ha()
{
    m_pLineAY->lower();
    m_pLineBY->lower();
    m_pLineBX->lower();
    m_pLineRefAx->lower();
    m_pLineRefBx->lower();
    m_pLineRefAy->lower();
    m_pLineRefBy->lower();
}

void CAppCursor::on_cursor_raise_hb()
{
    m_pLineAY->lower();
    m_pLineBY->lower();
    m_pLineAX->lower();
    m_pLineRefAx->lower();
    m_pLineRefBx->lower();
    m_pLineRefAy->lower();
    m_pLineRefBy->lower();
}

void CAppCursor::on_cursor_raise_va()
{
    m_pLineAX->lower();
    m_pLineBX->lower();
    m_pLineBY->lower();
    m_pLineRefAx->lower();
    m_pLineRefBx->lower();
    m_pLineRefAy->lower();
    m_pLineRefBy->lower();
}

void CAppCursor::on_cursor_raise_vb()
{
    m_pLineAX->lower();
    m_pLineBX->lower();
    m_pLineAY->lower();
    m_pLineRefAx->lower();
    m_pLineRefBx->lower();
    m_pLineRefAy->lower();
    m_pLineRefBy->lower();
}

void CAppCursor::cursorUpdate( cursor_ui::CursorControls cont,
                   cursor_ui::CCursorLine *pLine,
                   int posMsg,
                   int valMsg )
{
    int pos;
    query( posMsg, pos );
    if ( NULL != pLine )
    {
//        if ( pVal->valid )
//        {
//            pLine->show();
        pLine->setPos( pos);
//        }
//        else
//        {
//            pLine->hide();
//        }
    }


    cursor_lib::CCursorVal *pVal;
    void *pSmartVal;
    query( valMsg, pSmartVal );
    pVal = (cursor_lib::CCursorVal*)pSmartVal;

    QString str;
    servCursor::format( pVal, str );
    m_pCursorWnd->setValue( cont, str );
}

void CAppCursor::cursorLaUpdate(cursor_ui::CursorControls cont, int valMsg)
{
    void *pSmartVal;
    query( valMsg, pSmartVal );
    cursor_lib::CCursorVal* pVal = (cursor_lib::CCursorVal*)pSmartVal;

    QString str16;
    QString str2h;
    QString str2l;
    servCursor::format( pVal, str16, str2h, str2l);
    m_pCursorWnd->setValue( cont, str16);
    m_pCursorWnd->setValue( (cursor_ui::CursorControls) (cont + 1), str2h);
    m_pCursorWnd->setValue( (cursor_ui::CursorControls) (cont + 2), str2l);
}

void CAppCursor::crossYUpdate(
                   cursor_ui::CCursorCross *pCross,
                   int posMsg,
                   int valMsg )
{
    void *pSmartVal;
    cursor_lib::CCursorVal *pVal;
    int pos;

    if ( NULL == pCross )
    {
        return;
    }
    //! query adc
    query( valMsg, pSmartVal );
    pVal = (cursor_lib::CCursorVal*)pSmartVal;

    if ( !pVal->getValid() )
    {
        pCross->setShow( false );
        return;
    }
    else
    {
        if(pCross->getServEnabled())
        {
            pCross->setShow( true );
        }
    }
    if ( pVal->getType() != cursor_value_scr )
    {
        pCross->setShow( false );
        return;
    }

    //! pos
    query( posMsg, pos );
    //bug 1929 by zy
    if(pVal->hexValue <= 0 )
    {
        pVal->hexValue = 0;
    }
    pCross->setPos( pos, pVal->hexValue );
}

void CAppCursor::crossXUpdate(
                   cursor_ui::CCursorCross *pCross,
                   int posMsg,
                   int valMsg )
{
    void *pSmartVal;
    cursor_lib::CCursorVal *pVal;
    int pos;

    if ( NULL == pCross )
    {
        return;
    }
    //! query scr
    query( valMsg, pSmartVal );
    pVal = (cursor_lib::CCursorVal*)pSmartVal;

    if ( !pVal->getValid() )
    {
        pCross->setShow( false );
        return;
    }

    if ( pVal->getType() != cursor_value_scr )
    {
        pCross->setShow( false );
        return;
    }

    //! pos
    query( posMsg, pos );
    if(pCross->getServEnabled())
    {
        pCross->setShow( true );
    }
    pCross->setPos( pVal->hexValue, pos );
}

void CAppCursor::crossXYUpdate(cursor_ui::CCursorCross *pCross, int xMsg, int yMsg)
{
    void *pxSmartVal;
    cursor_lib::CCursorVal *pxVal;
    void *pySmartVal;
    cursor_lib::CCursorVal *pyVal;
    if ( NULL == pCross )
    {
        return;
    }

    //! query scr
    query( xMsg, pxSmartVal );
    pxVal = (cursor_lib::CCursorVal*)pxSmartVal;

    if ( !pxVal->getValid() )
    {
        pCross->setShow( false );
        return;
    }

    if ( pxVal->getType() != cursor_value_adc )
    {
        pCross->setShow( false );
        return;
    }

    //! query adc
    query( yMsg, pySmartVal );
    pyVal = (cursor_lib::CCursorVal*)pySmartVal;

    if ( !pyVal->getValid() )
    {
        pCross->setShow( false );
        return;
    }

    if ( pyVal->getType() != cursor_value_adc )
    {
        pCross->setShow( false );
        return;
    }

    pCross->setShow( true );
    pCross->setPos( pxVal->hexValue, pyVal->hexValue );
}

void CAppCursor::onFocus()
{
    int focusMsg;

    query( CMD_SERVICE_ITEM_FOCUS, focusMsg );

    if ( focusMsg == 0 )
    { return; }

    //! setFocus
    QMap<int, int>::const_iterator iter = mMsgFocusMap.constBegin();
    while ( iter != mMsgFocusMap.constEnd())
    {
        if ( iter.key() == focusMsg )
        {
            Q_ASSERT( m_pCursorWnd != NULL );
            m_pCursorWnd->setFocus(
                        (cursor_ui::CursorControls)iter.value()
                        );

            if( iter.value() == cursor_ui::cursor_control_ax  )
            {
                on_cursor_raise_ha();
            }
            else if( iter.value() == cursor_ui::cursor_control_bx  )
            {
                on_cursor_raise_hb();
            }
            else if( iter.value() == cursor_ui::cursor_control_ay  )
            {
                on_cursor_raise_va();
            }
            else if( iter.value() == cursor_ui::cursor_control_by  )
            {
                on_cursor_raise_vb();
            }

            return;
        }
        iter++;
    }

    //! invalid focus wnd item
}

bool CAppCursor::isMatch( int val, int msg )
{
    int qVal;

    query( msg, qVal );

    return qVal == val;
}
