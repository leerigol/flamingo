#ifndef CAPPCURSOR_H
#define CAPPCURSOR_H

#include <QtWidgets>

#include "../capp.h"

#include "../../service/service_msg.h"
#include "../../service/servcursor/servcursor.h"

#include "../../menu/wnd/rmodalwnd.h"

#include "ccursorwnd.h"
#include "ccursorline.h"
#include "ccursorcross.h"
#include "ccursorlisajous.h"

class appCursor_style : public menu_res::RUI_Style
{
protected:
    virtual void load(const QString &path,
                      const r_meta::CMeta &meta );
public:
    QRect mRect;
};

class CAppCursor : public CApp
{
    Q_OBJECT

    DECLARE_CMD()

    enum appCmd
    {
        cmd_base = CMD_APP_BASE,
    };

public:
    CAppCursor();

public:
    virtual void setupUi( CAppMain *pMain );

    virtual void buildConnection();
    virtual void resize();
    void    rst();

protected:
    void on_cursor_mode( );

    void on_cursor_manual_src();

    void on_cursor_manual_region();

    void on_cursor_manual_hunit();
    void on_manual_hRef_Visible();
    void on_cursor_manual_vunit();
    void on_manual_vRef_Visible();

    void on_cursor_manual_vrange();
    void on_cursor_manual_hrange();

    void on_cursor_manual_pos_update();
    void on_cursor_manual_la_update();

    //! track
    void on_cursor_zoom_change(void);
    void on_zoom_track_en();
    void on_scr_mode_change();

    void on_cursor_track_srca();
    void on_cursor_track_srcb();

    void on_cursor_track_ya_value_update();
    void on_cursor_track_yb_value_update();

    void on_cursor_track_xa_value_update();
    void on_cursor_track_xb_value_update();

    void on_cursor_track_delta_value_update();
    void on_cursor_track_delta_useable_update( );

    //! auto
    void on_cursor_auto_x_changed(void);
    void on_cursor_auto_y_changed(void);

    cursor_ui::CCursorLine* createAutoXLine();
    cursor_ui::CCursorLine* createAutoYLine();

    void on_set_item_update();
    //! xy
    void on_cursor_xy_mode();
    void on_cursor_xy_lisajous();

    void on_cursor_xy_value_update();
    void on_cursor_xy_track_value_update();

    void on_cursor_xy_pos_changed();
    void on_cursor_xy_hpos_changed();

    void on_cursor_raise_ha();
    void on_cursor_raise_hb();
    void on_cursor_raise_va();
    void on_cursor_raise_vb();

protected:
    void cursorUpdate( cursor_ui::CursorControls cont,
                       cursor_ui::CCursorLine *pLine,
                       int posMsg,
                       int valMsg );

    void cursorLaUpdate( cursor_ui::CursorControls cont,
                         int valMsg );

    void crossYUpdate( cursor_ui::CCursorCross *pCross,
                       int posMsg,
                       int valMsg );

    void crossXUpdate( cursor_ui::CCursorCross *pCross,
                       int posMsg,
                       int valMsg );

    void crossXYUpdate(cursor_ui::CCursorCross *pCross,
                       int xMsg,
                       int yMsg );

    void onFocus();
protected:
#ifndef _GUI_CASE
    bool isMatch( int val, int msg = MSG_CURSOR_S32CURSORMODE );
#endif

private:
    appCursor_style _style;

    cursor_ui::CCursorWnd *m_pCursorWnd;

    QVector<cursor_ui::CCursorLine*>  xAutoLine;
    QVector<cursor_ui::CCursorLine*>  yAutoLine;

    cursor_ui::CCursorLine *m_pLineAutoAX;
    cursor_ui::CCursorLine *m_pLineAutoBX;
    cursor_ui::CCursorLine *m_pLineAutoAY;
    cursor_ui::CCursorLine *m_pLineAutoBY;

    cursor_ui::CCursorLine *m_pLineAX;
    cursor_ui::CCursorLine *m_pLineBX;

    cursor_ui::CCursorLine *m_pLineAY;
    cursor_ui::CCursorLine *m_pLineBY;

    cursor_ui::CCursorLine *m_pLineRefAx;
    cursor_ui::CCursorLine *m_pLineRefBx;

    cursor_ui::CCursorLine *m_pLineRefAy;
    cursor_ui::CCursorLine *m_pLineRefBy;

    cursor_ui::CCursorCross *m_pCrossYA;
    cursor_ui::CCursorCross *m_pCrossYB;

    cursor_ui::CCursorCross *m_pCrossXA;
    cursor_ui::CCursorCross *m_pCrossXB;

    cursor_ui::CCursorLisajous *m_pLisajous;

    menu_res::RModalWnd *m_pModalWnd;

    QWidget* m_pCent;
    QMap<int,int> mMsgFocusMap;
};

#endif // CAPPCURSOR_H
