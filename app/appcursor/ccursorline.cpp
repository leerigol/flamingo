#include "ccursorline.h"
#define  HALFTABWIDTH   0

namespace cursor_ui {

QVector<qreal> CCursorLine::_dashPattern;
CCursorLine::CCursorLine(CursorLineXoY xy, QWidget *parent)
            : QWidget(parent)
{
    mXoY = xy;
    mPos = 100;

    mColor = QColor::fromRgb(0xbb,0xbb,0xbb);
    mStyle = cursor_style_solid;
    isHasTab = false;

    if ( mXoY == cursor_line_x )
    {
        setGeometry(0, 0, 1, 480 );
    }
    else
    {
        setGeometry(0, 0, 1000, 1 );
    }

    m_viewRect.setRect(0,0,1000,480);

    //! create pattern
    if ( _dashPattern.isEmpty() )
    {
        _dashPattern.append( 10 );
        _dashPattern.append( 3 );
        _dashPattern.append( 1 );
        _dashPattern.append( 3 );
    }
}

void CCursorLine::setWidgetVisible( bool b )
{
    setVisible( b );
}

void CCursorLine::refresh()
{
    //! update pos
    viewPos( mPos );

    update();
}

void CCursorLine::setView( int w, int h )
{
    menu_res::RDsoUI::setView( w, h );
    if(isHasTab)
    {
        if ( mXoY == cursor_line_x )
        {
            setGeometry(0, 0, 31, w);
        }
        else
        {
            setGeometry(0, 0, h, 31);
        }
    }
    else
    {
        if ( mXoY == cursor_line_x )
        {
            setGeometry(0, 0, 1, w);
        }
        else
        {
            setGeometry(0, 0, h, 1);
        }
    }
}

void CCursorLine::setView(QRect &rect)
{
    menu_res::RDsoUI::setView(rect);
    if(isHasTab)
    {
        if ( mXoY == cursor_line_x )
        {
            setGeometry(0, 0, 31, rect.height());
        }
        else
        {
            setGeometry(0, 0, rect.width(), 31 );
        }
    }
    else
    {
        if ( mXoY == cursor_line_x )
        {
            setGeometry(0, 0, 1, rect.height());
        }
        else
        {
            setGeometry(0, 0, rect.width(), 1 );
        }
    }
}

void CCursorLine::setPos( int pos )
{
    if(isHasTab)
    {
        pos = pos;
        if( mXoY == cursor_line_x)
        {
            pos = limitRange(pos, 0, 999);
        }
        if( mXoY == cursor_line_y)
        {
            pos = limitRange(pos, 0, 479);
        }
    }
    else
    {
        if( mXoY == cursor_line_x)
        {
            pos = limitRange(pos, 0, 999);
        }
        if( mXoY == cursor_line_y)
        {
            pos = limitRange(pos, 0, 479);
        }
    }
    mPos = pos;
    viewPos( pos );

    update();
}

int CCursorLine::limitRange(int pos, int left, int right)
{
    if(pos > right)
    {
        pos = right;
    }
    if(pos < left)
    {
        pos = left;
    }
    return pos;
}

void CCursorLine::setStyle( CursorStyle style )
{
    mStyle = style;
}

void CCursorLine::setLine(CursorLineXoY xy )
{
    mXoY = xy;
}

CursorLineXoY CCursorLine::getXoy()
{
    return mXoY;
}

void CCursorLine::setColor( QColor color )
{
    mColor = color;
}

void CCursorLine::setHasTab(bool b)
{
    isHasTab = b;

    if(isHasTab)
    {
        if ( mXoY == cursor_line_x )
        {
            setGeometry(-HALFTABWIDTH, 0, 31, 480 );
        }
        else
        {
            setGeometry(0, -HALFTABWIDTH, 1000, 31 );
        }
    }
    else
    {
        if ( mXoY == cursor_line_x )
        {
            setGeometry(0, 0, 1, 480 );
        }
        else
        {
            setGeometry(0, 0, 1000, 1 );
        }
    }
}

void CCursorLine::paintEvent( QPaintEvent *event )
{
    if ( !isVisible() ) return;

    if ( mXoY == cursor_line_x )
    {
        xPaint( event );
    }
    else
    {
        yPaint( event );
    }
}

void CCursorLine::yPaint( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QPen pen;
    pen.setColor( mColor );

    if(isHasTab)
    {
        QPainterPath path;
        path.moveTo(m_phyRect.width()-40, 15);
        path.lineTo(m_phyRect.width()-25, 30 );
        path.lineTo(m_phyRect.width(), 30 );
        path.lineTo(m_phyRect.width(), 0);
        path.lineTo(m_phyRect.width()-25, 0);
        path.lineTo(m_phyRect.width()-40, 15);

        if ( mStyle == cursor_style_solid )
        {
            pen.setStyle( Qt::SolidLine );
            painter.setPen( pen );

            if(mPos<15  )
            {
                painter.drawLine(0, mPos, m_phyRect.width(), mPos );
            }
            else if( mPos>464 )
            {
                painter.drawLine(0, mPos-449, m_phyRect.width(), mPos-449 );
            }
            else
            {
                painter.drawLine(0, 15, m_phyRect.width(), 15 );
            }

            painter.fillPath(path, Qt::black);
            painter.drawPath(path);

            painter.drawText(m_phyRect.width()-20, 23, "AY");
        }
        else if ( mStyle == cursor_style_dot )
        {
            pen.setStyle( Qt::CustomDashLine );
            pen.setDashPattern( _dashPattern );
            painter.setPen( pen );

            if(mPos<15 )
            {
                painter.drawLine(0, mPos, m_phyRect.width(), mPos );
            }
            else if( mPos>464 )
            {
                painter.drawLine(0, mPos-449, m_phyRect.width(), mPos-449 );
            }
            else
            {
                painter.drawLine(0, 15, m_phyRect.width(), 15 );
            }

            pen.setStyle( Qt::SolidLine );
            painter.setPen( pen );

            painter.fillPath(path, Qt::black);
            painter.drawPath(path);

            painter.drawText(m_phyRect.width()-20, 23, "BY");
        }
        else
        {
            return;
        }
    }
    else
    {
        if ( mStyle == cursor_style_solid )
        {
            pen.setStyle( Qt::SolidLine );
            painter.setPen( pen );
        }
        else if ( mStyle == cursor_style_dot )
        {
            pen.setStyle( Qt::CustomDashLine );
            pen.setDashPattern( _dashPattern );
            painter.setPen( pen );
        }
        else
        {
            return;
        }

        painter.drawLine(0,0, m_phyRect.width(),0 );
    }
}

void CCursorLine::xPaint( QPaintEvent */*event*/ )
{
    QPainter painter(this);
    QPen pen;
    pen.setColor(mColor);

    if(isHasTab)
    {
        QPainterPath path;
        path.moveTo(15, m_phyRect.height()-40);
        path.lineTo(30, m_phyRect.height()-25 );
        path.lineTo(30, m_phyRect.height() );
        path.lineTo(0, m_phyRect.height());
        path.lineTo(0, m_phyRect.height()-25);
        path.lineTo(15, m_phyRect.height()-40);

        if ( mStyle == cursor_style_solid )
        {
            pen.setStyle( Qt::SolidLine );
            painter.setPen( pen );

            if(mPos<15 )
            {
                painter.drawLine( mPos, 0, mPos , m_phyRect.height());
            }
            else if( mPos>984 )
            {
                painter.drawLine( mPos-969, 0, mPos-969, m_phyRect.height());
            }
            else
            {
                painter.drawLine(15, 0, 15, m_phyRect.height() );
            }

            painter.fillPath(path, Qt::black);
            painter.drawPath(path);

            painter.drawText(5, m_phyRect.height()-5, "AX");
        }
        else if ( mStyle == cursor_style_dot )
        {
            pen.setStyle( Qt::CustomDashLine );
            pen.setDashPattern( _dashPattern );
            painter.setPen( pen );

            if(mPos<15 )
            {
                painter.drawLine( mPos, 0, mPos , m_phyRect.height());
            }
            else if( mPos>984 )
            {
                painter.drawLine( mPos-969, 0, mPos-969, m_phyRect.height());
            }
            else
            {
                painter.drawLine(15, 0, 15, m_phyRect.height() );
            }

            pen.setStyle( Qt::SolidLine );
            painter.setPen( pen );

            painter.fillPath(path, Qt::black);
            painter.drawPath(path);

            painter.drawText(5, m_phyRect.height()-5, "BX");
        }
    }
    else
    {
        if ( mStyle == cursor_style_solid )
        {
            pen.setStyle( Qt::SolidLine );
            painter.setPen( pen );
        }
        else if ( mStyle == cursor_style_dot )
        {
            pen.setStyle( Qt::CustomDashLine );
            pen.setDashPattern( _dashPattern );
            painter.setPen( pen );
        }
        else
        {
            return;
        }
        painter.drawLine(0,0,0, m_phyRect.height() );
    }
}

void CCursorLine::viewPos( int posView )
{
    int phyPos;

    if(isHasTab)
    {
        //! x or y
        if ( mXoY == cursor_line_x )
        {
            //phyPos = ( posView - m_viewRect.left() ) * m_phyRect.width() / m_viewRect.width() + m_phyRect.left();
            phyPos = posView + m_phyRect.left();

            if ( phyPos-15 <= m_phyRect.left() )
            {
                phyPos = m_phyRect.left();
            }
            else if ( phyPos >= m_phyRect.right()-15 )
            {
                phyPos = m_phyRect.right()-30;
            }
            else
            {
                phyPos -= 15;
            }

            move( phyPos, m_phyRect.top() );
        }
        else
        {
            phyPos = ( posView - m_viewRect.top() ) * m_phyRect.height() / m_viewRect.height() + m_phyRect.top();

            if ( phyPos-15 <= m_phyRect.top() )
            {
                phyPos = m_phyRect.top();
            }
            else if ( phyPos >= m_phyRect.bottom()-15 )
            {
                phyPos = m_phyRect.bottom()-30;
            }
            else
            {
                phyPos -= 15;
            }

            move( m_phyRect.left(), phyPos );
        }
    }
    else
    {
        if ( mXoY == cursor_line_x )
        {
            //phyPos = ( posView - m_viewRect.left() ) * m_phyRect.width() / m_viewRect.width() + m_phyRect.left();
            phyPos = posView + m_phyRect.left();
            if ( phyPos < m_phyRect.left() )
            {
                phyPos = m_phyRect.left();
            }
            else if ( phyPos > m_phyRect.right() )
            {
                phyPos = m_phyRect.right();
            }
            else
            {}
            move( phyPos, m_phyRect.top() );
        }
        else
        {
            phyPos = ( posView - m_viewRect.top() ) * m_phyRect.height() / m_viewRect.height() + m_phyRect.top();
            if ( phyPos < m_phyRect.top() )
            {
                phyPos = m_phyRect.top();
            }
            else if ( phyPos > m_phyRect.bottom() )
            {
                phyPos = m_phyRect.bottom();
            }
            else
            {}

            move( m_phyRect.left(), phyPos );
        }
    }
}

}
