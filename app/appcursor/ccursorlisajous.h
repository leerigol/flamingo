#ifndef CCURSORLISAJOUS_H
#define CCURSORLISAJOUS_H

#include "../../menu/wnd/rinfownd.h"
#include "../../menu/wnd/rmodalwnd.h"

using namespace menu_res;
namespace cursor_ui {

class CCursorLisajous : public menu_res::RModalWnd
{
    Q_OBJECT
public:
    explicit CCursorLisajous(QWidget *parent = 0);
    ~CCursorLisajous();

protected:
    virtual void paintEvent( QPaintEvent *evt );

private:
    QImage *mpImage;

};
}

#endif // CCURSORLISAJOUS_H
