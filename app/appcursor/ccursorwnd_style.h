#ifndef CCURSORWND_STYLE_H
#define CCURSORWND_STYLE_H

#include "../../menu/menustyle/rui_style.h"

class CCursorWnd_Style : public menu_res::RUI_Style
{

protected:
    virtual void load( const QString &path,
                       const r_meta::CMeta &meta );

public:
    QColor mLineColorA, mLineColorB;
    QColor mLineColorActive;

};

#endif // CCURSORWND_STYLE_H
