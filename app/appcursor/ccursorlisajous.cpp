#include "ccursorlisajous.h"
namespace cursor_ui {
CCursorLisajous::CCursorLisajous(QWidget *parent) : RModalWnd(parent)
{
    mpImage = NULL;
    setAttr(wnd_moveable);

    if ( NULL == mpImage )
    {
        mpImage = new QImage(":/pictures/cursor/XyDemo_Help.bmp");
        Q_ASSERT( NULL != mpImage );
    }

    resize( mpImage->width()+26, mpImage->height()+48 );
}

CCursorLisajous::~CCursorLisajous()
{
    if ( NULL != mpImage )
    { delete mpImage; }
}

void CCursorLisajous::paintEvent( QPaintEvent *evt )
{
    //! parent repaint
    RModalWnd::paintEvent( evt );

    QPainter painter(this);
    painter.setPen(QPen(QColor(200,200,200,255)));
    painter.drawText(15,4,100,30,
                     Qt::AlignLeft|Qt::AlignVCenter,
                     "Lissajous");
    Q_ASSERT( NULL != mpImage );
    painter.drawImage( 12,
                       42,
                       *mpImage);

}

}
