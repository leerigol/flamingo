#ifndef CCURSORCROSS_H
#define CCURSORCROSS_H

#include <QtWidgets>

#include "../../menu/arch/rdsoui.h"

#include "cursorui.h"
#include "ccursorline.h"

namespace cursor_ui {

/*!
 * \brief The CCursorCross class
 * 追踪点
 */
class CCursorCross : public QWidget,
                     public menu_res::RDsoUI
{
    Q_OBJECT

private:
    static QPainterPath _crossPath;

public:
    explicit CCursorCross(CursorLineXoY xy, QWidget *parent = 0);
    virtual void setShow(bool b);

protected:
    virtual void setWidgetVisible( bool b );
    virtual void refresh();

public:
    void setColor( QColor color );
    void setStyle( CursorStyle style );
    void setPos( int x, int y );
    void setServEnabled( bool enabled );
    bool getServEnabled();

    cursor_ui::CCursorLine *m_pCrossLine;
protected:
    virtual void paintEvent( QPaintEvent *evt );
    void viewPos( int viewX, int viewY );

    QColor mColor;
    CursorStyle mStyle;
    bool mServEnabled;   //!

    int mX, mY;          //! view x, y
};

}

#endif // CCURSORCROSS_H
