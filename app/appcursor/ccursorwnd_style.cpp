#include "../../meta/crmeta.h"
#include "../../menu/menustyle/rcontrolstyle.h"

#include "ccursorwnd_style.h"

void CCursorWnd_Style::load( const QString &path,
                             const r_meta::CMeta &meta )
{
    meta.getMetaVal( path + "color/a", mLineColorA );
    meta.getMetaVal( path + "color/b", mLineColorB );
    meta.getMetaVal( path + "color/active", mLineColorActive );
}
