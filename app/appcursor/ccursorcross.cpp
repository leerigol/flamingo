#include "ccursorcross.h"

namespace cursor_ui {

QPainterPath CCursorCross::_crossPath;

CCursorCross::CCursorCross(CursorLineXoY xy, QWidget *parent) : QWidget(parent)
{
    setGeometry( QRect(0,0,25,11) );

    mStyle = cursor_style_solid;
    mColor = Qt::white;

    m_viewRect.setRect(0,0,1000,480);
    //! create path
    if ( _crossPath.isEmpty() )
    {
        _crossPath.moveTo(0,5);
        _crossPath.lineTo(8,5);

        _crossPath.moveTo(4,0);
        _crossPath.lineTo(3,0);
        _crossPath.lineTo(3,10);
        _crossPath.lineTo(4,10);

        _crossPath.moveTo(20,0);
        _crossPath.lineTo(21,0);
        _crossPath.lineTo(21,10);
        _crossPath.lineTo(20,10);

        _crossPath.moveTo(16,5);
        _crossPath.lineTo(24,5);
    }

    m_pCrossLine = new cursor_ui::CCursorLine( xy, parent);
    Q_ASSERT(m_pCrossLine != NULL);
    m_pCrossLine->setView(1000,480);
    m_pCrossLine->setHasTab(false);
    m_pCrossLine->setShow(false);
}

void CCursorCross::setWidgetVisible( bool b )
{
    setVisible( b );
}

void CCursorCross::refresh()
{
    viewPos( mX, mY );

    if(getDsoView() != NULL)
    {
        m_pCrossLine->setDsoView(getDsoView());
    }

    m_pCrossLine->setPhy(m_phyRect);

    if(m_pCrossLine->getXoy() == cursor_line_x)
    {
        m_pCrossLine->setPos( mX );
    }
    else
    {
        m_pCrossLine->setPos( mY );
    }
}

void CCursorCross::setShow(bool b)
{
    RDsoUI::setShow(b);
    m_pCrossLine->setShow(b);
}

void CCursorCross::setColor( QColor color )
{
    mColor = color;
}

void CCursorCross::setStyle( CursorStyle style )
{
    mStyle = style;
    m_pCrossLine->setStyle( style );
}
void CCursorCross::setPos( int x, int y )
{
    mX = x;
    mY = y;

    viewPos( mX, mY );

    if(m_pCrossLine->getXoy() == cursor_line_x)
    {
        m_pCrossLine->setPos( mX );
    }
    else
    {
        m_pCrossLine->setPos( mY );
    }
}

void CCursorCross::setServEnabled(bool enabled)
{
    mServEnabled = enabled;
}

bool CCursorCross::getServEnabled()
{
    return mServEnabled;
}

void CCursorCross::paintEvent( QPaintEvent */*event*/ )
{
    QPen pen;

    pen.setColor( mColor );
    if ( mStyle == cursor_style_dot )
    {
        pen.setStyle( Qt::DashDotLine );
    }
    else if ( mStyle == cursor_style_solid )
    {
        pen.setStyle( Qt::SolidLine );
    }
    else
    {}

    QPainter painter(this);
    painter.setPen( pen );

    painter.drawPath( CCursorCross::_crossPath );
}

/*!
 * \brief CCursorCross::viewPos
 * \param viewX
 * \param viewY
 * 移动追踪交叉点位置
 */
void CCursorCross::viewPos( int viewX, int viewY )
{
    int phyX, phyY;

    phyX = ( viewX - m_viewRect.left() ) * m_phyRect.width() / m_viewRect.width() + m_phyRect.left();
    phyY = ( viewY - m_viewRect.top() ) * m_phyRect.height() / m_viewRect.height() + m_phyRect.top();

    //! adjust to the lt
    QRect selfRect;

    selfRect = rect();

    phyX -= selfRect.width() / 2;
    phyY -= selfRect.height() / 2;

    move( phyX, phyY );
}

}

