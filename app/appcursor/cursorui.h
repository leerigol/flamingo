#ifndef CURSORUI
#define CURSORUI

namespace cursor_ui {

enum CursorLineXoY
{
    cursor_line_x,
    cursor_line_y,
};

enum CursorStyle
{
    cursor_style_solid,
    cursor_style_dot,
};


}

#endif // CURSORUI

