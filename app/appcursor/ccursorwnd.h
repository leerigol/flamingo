#ifndef CCURSORWND_H
#define CCURSORWND_H

#include <QtWidgets>

#include "../../menu/menustyle/rinfownd_style.h"
#include "../../menu/dsowidget/rdsodragdrop.h"
#include "../../menu/wnd/uiwnd.h"
#include "../../service/servcursor/cursorlib/cursorlib.h"

#include "ccursorwnd_style.h"

namespace cursor_ui {

//! config
#define wnd_round_radius    10

enum CursorControls
{
    cursor_control_begin,

    cursor_control_t1 = cursor_control_begin,
    cursor_control_ax,
    cursor_control_ay,
    cursor_control_a_d15_d8,
    cursor_control_a_d7_d0,

    cursor_control_t2,
    cursor_control_bx,
    cursor_control_by,
    cursor_control_b_d15_d8,
    cursor_control_b_d7_d0,

    cursor_control_dx,
    cursor_control_dy,
    cursor_control_dy2,
    cursor_control_inv_dx,
                            //! focus item
    cursor_control_abx,
    cursor_control_aby,
    cursor_control_abt,
};

/*!
 * \brief The CCursorWnd class
 * 光标数值显示窗口
 */
class CCursorWnd : public menu_res::uiWnd
{
    static menu_res::RInfoWnd_Style _style;
    static CCursorWnd_Style _styleWnd;
public:
    CCursorWnd( QWidget *parent = 0 );

    void setVisible( CursorControls cont, bool b );
    bool getVisible( CursorControls cont );

    void setTitle( CursorControls cont, QString str );
    void setValue( CursorControls cont, QString str );

    void setFocus( CursorControls cont );

    void setupUi();
    void changeTitle(Chan ch);
    void changeFocus(void);
protected:
    void setLaTitle();
    void setAnaTitle();
    void setLaType();


    virtual void doPaint( QPaintEvent *evt,
                          QPainter &painter );
    QRect getRect( CursorControls cont );

protected:
    QList<QLabel*> mTitleList;
    QList<QLabel*> mValueList;

    QFormLayout *m_pLayout;
    CursorControls mFocusItem;
    cursor_lib::CursorValueType mLaType;

    int m_nRowH;
    int mxMove, myMove;
private:
    QLabel *m_pLabelt1;
    QLabel *m_pLabelAx;
    QLabel *m_pLabelAy;
    QLabel *m_pLabelAd15d8;
    QLabel *m_pLabelAd7d0;

    QLabel *m_pLabelt2;
    QLabel *m_pLabelBx;
    QLabel *m_pLabelBy;
    QLabel *m_pLabelBd15d8;
    QLabel *m_pLabelBd7d0;

    QLabel *m_pLabelDx;
    QLabel *m_pLabelDy;
    QLabel *m_pLabelDy2;
    QLabel *m_pLabelInvDx;

    //!value
    QLabel *m_pValuet1;
    QLabel *m_pValueAx;
    QLabel *m_pValueAy;
    QLabel *m_pValueAd15d8;
    QLabel *m_pValueAd7d0;

    QLabel *m_pValuet2;
    QLabel *m_pValueBx;
    QLabel *m_pValueBy;
    QLabel *m_pValueBd15d8;
    QLabel *m_pValueBd7d0;

    QLabel *m_pValueDx;
    QLabel *m_pValueDy;
    QLabel *m_pValueDy2;
    QLabel *m_pValueInvDx;
};
}

#endif // CCURSORWND_H
