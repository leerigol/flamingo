#ifndef CCURSORLINE_H
#define CCURSORLINE_H

#include <QtWidgets>

#include "../../menu/arch/rdsoui.h"

#include "cursorui.h"

namespace cursor_ui {

class CCursorLine : public QWidget,
                    public menu_res::RDsoUI
{
    Q_OBJECT

    static QVector<qreal> _dashPattern;

public:
    explicit CCursorLine(CursorLineXoY line, QWidget *parent = 0);

protected:
    virtual void setWidgetVisible( bool b );
    virtual void refresh();

public:
    void setView( int w, int h );
    void setView( QRect &rect );
public:
    void setPos( int pos );                 //! line pos

    int limitRange(int pos, int left, int right);
    void setStyle( CursorStyle style );
    void setLine( CursorLineXoY align );
    CursorLineXoY getXoy( );
    void setColor( QColor color );
    void setHasTab(bool b);

protected:
    virtual void paintEvent( QPaintEvent *event );

    void yPaint( QPaintEvent *event );
    void xPaint( QPaintEvent *event );

    void viewPos( int posView );

private:
    int mPos;
    CursorStyle mStyle;
    CursorLineXoY mXoY;
    QColor mColor;

    bool isHasTab;
//    int     mTabPos;
//    int mPassNum; // out limit num
};

}

#endif // CCURSORLINE_H
