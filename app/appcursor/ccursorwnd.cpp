#include "ccursorwnd.h"

namespace cursor_ui {
menu_res::RInfoWnd_Style CCursorWnd::_style;
CCursorWnd_Style CCursorWnd::_styleWnd;

CCursorWnd::CCursorWnd(QWidget *parent) :
            menu_res::uiWnd( parent )
{
//    setWindowFlags( Qt::FramelessWindowHint );

    mFocusItem = cursor_control_ax;

    m_nRowH = 20;
    _style.loadResource("ui/wnd/model/");
    _styleWnd.loadResource("ui/wnd/cursor_value/");

    set_attr( wndAttr, mWndAttr, wnd_moveable );
}

void CCursorWnd::setVisible( CursorControls cont, bool b )
{
    list_at( mTitleList,cont - cursor_control_begin)->setVisible(b);
    list_at( mValueList,cont - cursor_control_begin)->setVisible(b);
}

bool CCursorWnd::getVisible( CursorControls cont )
{
    Q_ASSERT(list_at( mTitleList,cont-cursor_control_begin));
    return list_at( mTitleList,cont-cursor_control_begin)->isVisible() &&
           list_at( mValueList,cont-cursor_control_begin)->isVisible();
}

void CCursorWnd::setTitle( CursorControls cont, QString str )
{
    Q_ASSERT( (cont - cursor_control_begin ) >= 0 );
    Q_ASSERT( (cont - cursor_control_begin ) < mTitleList.size() );

    list_at( mTitleList,cont - cursor_control_begin )->setText( str );
}

void CCursorWnd::setValue( CursorControls cont, QString str )
{
    Q_ASSERT( (cont - cursor_control_begin ) >= 0 );
    Q_ASSERT( (cont - cursor_control_begin ) < mTitleList.size() );

    list_at( mValueList,cont-cursor_control_begin)->setText( str );
}
void CCursorWnd::changeFocus(void)
{
     update();
}
void CCursorWnd::setFocus( CursorControls cont )
{
    if ( mFocusItem != cont )
    {
        mFocusItem = cont;
        update();
    }
}

QRect CCursorWnd::getRect( CursorControls cont )
{
    QRect rect1, rect2;

    rect1 = mTitleList.at(cont - cursor_control_begin)->geometry();
    rect2 = mValueList.at(cont - cursor_control_begin)->geometry();

    rect1.setRight( rect2.right() );
    return rect1;
}

void CCursorWnd::setupUi()
{
    //! controls
    m_pLabelt1 = new QLabel("t1:"); mTitleList.append( m_pLabelt1);
    m_pLabelAx = new QLabel("AX:"); mTitleList.append( m_pLabelAx );
    m_pLabelAy = new QLabel("AY:"); mTitleList.append( m_pLabelAy );
    m_pLabelAd15d8 = new QLabel("D15-D8:"); mTitleList.append( m_pLabelAd15d8 );
    m_pLabelAd7d0 = new QLabel("D7-D0:"); mTitleList.append( m_pLabelAd7d0 );

    m_pLabelt2 = new QLabel("t2:"); mTitleList.append( m_pLabelt2);
    m_pLabelBx = new QLabel("BX:"); mTitleList.append( m_pLabelBx );
    m_pLabelBy = new QLabel("BY:"); mTitleList.append( m_pLabelBy );
    m_pLabelBd15d8 = new QLabel("D15-D8:"); mTitleList.append( m_pLabelBd15d8 );
    m_pLabelBd7d0 = new QLabel("D7-D0:"); mTitleList.append( m_pLabelBd7d0 );

    m_pLabelDx = new QLabel(QString(QChar(0x0394))+"X:"); mTitleList.append( m_pLabelDx );
    m_pLabelDy = new QLabel(QString(QChar(0x0394))+"Y:"); mTitleList.append( m_pLabelDy );
    m_pLabelDy2 = new QLabel(QString(QChar(0x0394))+"Y2:"); mTitleList.append( m_pLabelDy2);
    m_pLabelInvDx = new QLabel("1/"+QString(QChar(0x0394))+"X:"); mTitleList.append( m_pLabelInvDx );

    m_pValuet1 = new QLabel("1.2ms"); mValueList.append( m_pValuet1);
    m_pValueAx = new QLabel("1.2ms"); mValueList.append( m_pValueAx );
    m_pValueAy = new QLabel("1.2ms"); mValueList.append( m_pValueAy );
    m_pValueAd15d8 = new QLabel("0000 0000"); mValueList.append( m_pValueAd15d8);
    m_pValueAd7d0 = new QLabel("0000 0000"); mValueList.append( m_pValueAd7d0);

    m_pValuet2 = new QLabel("1.2ms"); mValueList.append( m_pValuet2);
    m_pValueBx = new QLabel("1.2ms"); mValueList.append( m_pValueBx );
    m_pValueBy = new QLabel("1.2ms"); mValueList.append( m_pValueBy );
    m_pValueBd15d8 = new QLabel("0000 0000"); mValueList.append( m_pValueBd15d8);
    m_pValueBd7d0 = new QLabel("0000 0000"); mValueList.append( m_pValueBd7d0);

    m_pValueDx = new QLabel("1.2ms"); mValueList.append( m_pValueDx );
    m_pValueDy = new QLabel("1.2ms"); mValueList.append( m_pValueDy );
    m_pValueDy2 = new QLabel("1.2ms"); mValueList.append( m_pValueDy2);
    m_pValueInvDx = new QLabel("1.2ms"); mValueList.append( m_pValueInvDx );

    //! layout
    m_pLayout = new QFormLayout();

    m_pLayout->setVerticalSpacing( 0 );
    m_pLayout->setContentsMargins(9,39,9,9);

    //! apply layout
    setLayout( m_pLayout );

    //! change palette
    QPalette pal;
    pal.setColor( QPalette::WindowText, Qt::white );

    QWidget *pWig;
    foreach( pWig, mTitleList )
    {
        Q_ASSERT( pWig != NULL );
        pWig->setPalette( pal );
    }

    foreach( pWig, mValueList )
    {
        Q_ASSERT( pWig != NULL );
        pWig->setPalette( pal );
    }

    for(int i = 0;i < mTitleList.size();i++)
    {
        m_pLayout->addRow( mTitleList.at(i), mValueList.at(i));
    }

    //! visible
    m_pLabelt1->setVisible( false );
    m_pValuet1->setVisible( false );
    m_pLabelt2->setVisible( false );
    m_pValuet2->setVisible( false );

    m_pLabelAy->setVisible( false );
    m_pValueAy->setVisible( false );
    m_pLabelAd15d8->setVisible( false );
    m_pValueAd15d8->setVisible( false );
    m_pLabelAd7d0->setVisible( false );
    m_pValueAd7d0->setVisible( false );

    m_pLabelBy->setVisible( false );
    m_pValueBy->setVisible( false );
    m_pLabelBd15d8->setVisible( false );
    m_pValueBd15d8->setVisible( false );
    m_pLabelBd7d0->setVisible( false );
    m_pValueBd7d0->setVisible( false );

    m_pLabelDy2->setVisible( false );
    m_pValueDy2->setVisible( false );
}

void CCursorWnd::changeTitle(Chan ch)
{
    if(ch == la)
    {
        setLaTitle();
    }
    else
    {
        setAnaTitle();
    }
}

void CCursorWnd::setLaTitle()
{
    mTitleList.at(cursor_control_ay)->setText("D15-D0:");
    mValueList.at(cursor_control_ay)->setText("0x0000");
    mTitleList.at(cursor_control_by)->setText("D15-D0:");
    mValueList.at(cursor_control_by)->setText("0x0000");
}

void CCursorWnd::setAnaTitle()
{
    mTitleList.at(cursor_control_ay)->setText("AY:");
    mValueList.at(cursor_control_ay)->setText("0V");
    mTitleList.at(cursor_control_by)->setText("BY:");
    mValueList.at(cursor_control_by)->setText("0V");
}

void CCursorWnd::doPaint( QPaintEvent *evt,
                          QPainter &painter )
{
    QRect itemRect;

    int row = 0;
    int i;
    for ( i = cursor_control_begin; i <= cursor_control_inv_dx; i++ )
    {
        if(getVisible( (CursorControls)i ))
        {
            row++;
        }
    }
    if(row == 0)
    {
        hide();
        return;
    }
    else
    {
        resize(width(),row*m_nRowH+42);
    }

    itemRect = rect();
    //! paint wnd bg
    _style.paint( painter, itemRect );
    painter.setPen(QPen(QColor(200,200,200,255)));
    QString wndTitle = sysGetString(MSG_CURSOR, "Cursor");
    painter.drawText(15,6,70,40, Qt::AlignLeft, wndTitle);
    //! paint line bg
    QColor colors[2] = { _styleWnd.mLineColorA, _styleWnd.mLineColorB };
    int actColor = 0;
    int cont;
    for ( cont = cursor_control_begin; cont <= cursor_control_inv_dx; cont++ )
    {
        if ( getVisible( (CursorControls)cont ) && cont != mFocusItem )
        {
            itemRect = getRect( (CursorControls)cont );

            painter.fillRect( itemRect, colors[actColor] );

            actColor = (actColor + 1 ) & 0x01;
        }
    }

    //! focus line
    if(mFocusItem == cursor_control_abx)
    {
        itemRect = getRect( cursor_control_ax );
        painter.fillRect( itemRect, _styleWnd.mLineColorActive);
        itemRect = getRect( cursor_control_bx );
        painter.fillRect( itemRect, _styleWnd.mLineColorActive);
    }
    else if(mFocusItem == cursor_control_aby)
    {
        itemRect = getRect( cursor_control_ay );
        painter.fillRect( itemRect, _styleWnd.mLineColorActive);
        itemRect = getRect( cursor_control_by );
        painter.fillRect( itemRect, _styleWnd.mLineColorActive);
    }
    else if(mFocusItem == cursor_control_abt)
    {
        itemRect = getRect( cursor_control_t1 );
        painter.fillRect( itemRect, _styleWnd.mLineColorActive);
        itemRect = getRect( cursor_control_t2 );
        painter.fillRect( itemRect, _styleWnd.mLineColorActive);
    }
    else
    {
        itemRect = getRect( mFocusItem );
        painter.fillRect( itemRect, _styleWnd.mLineColorActive);
    }

    //! paint fg
    QWidget::paintEvent( evt );
}

}
