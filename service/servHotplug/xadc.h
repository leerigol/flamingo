#ifndef XADC_H
#define XADC_H

#include "servhotplug.h"

struct stProbeID
{
    stProbeID(Chan c)
    {
        chan = c;
        inserted = false;
        voltage  = 2283;//default value read from XADC

        input_path = QString("/sys/bus/i2c/devices/0-0037/in%1_input").arg(chan4-c);
        alarm_path = QString("/sys/bus/i2c/devices/0-0037/irq_clear");
        limit_high = QString("/sys/bus/i2c/devices/0-0037/in%1_max").arg(chan4-c);
        limit_low  = QString("/sys/bus/i2c/devices/0-0037/in%1_min").arg(chan4-c);

        lower      = THRESHOLD_LOW;
        upper      = THRESHOLD_UPP;
    }

    Chan    chan;
    bool    inserted;
    int     voltage;
    int     lower;
    int     upper;
    QString input_path;
    QString alarm_path;
    QString limit_high;
    QString limit_low;
};

struct stProbeData
{
    stProbeData(Impedance imp,
                int ratio,
                int should,
                int lower,
                int upper)
    {
        bImp    = imp;
        nRatio  = ratio;
        nShould = should;
        nLower  = lower;
        nUpper  = upper;
        nHead   = Probe_Single;
        nType   = Probe_V;
    }

    Impedance   bImp;
    int         nRatio;
    int         nShould;
    int         nLower;
    int         nUpper;
    ProbeHead   nHead;
    ProbeType   nType;
};


class CXADCListener: public QThread

{
    Q_OBJECT
public:
    CXADCListener( QObject *parent );

protected:
    void    run();

private:
    void    checkProbeStatus();
    bool    getProbeInfo(int volt, CCHProbe *probe);

private:
    QList<stProbeID*>    m_ProbeID;
    QList<stProbeData*>  m_ProbeData;
};

#endif // XADC

