#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "udisk.h"
#include "../servscpi/servscpidso.h"

#define UEVENT_BUFFER_SIZE 2048

CUSBListener::CUSBListener( QObject *parent ): QThread()
{
    m_pHotplug = dynamic_cast<servHotplug*>( parent );
}


/* Kernel Netlink */

int CUSBListener::initSock()
{
    const int buffersize = UEVENT_BUFFER_SIZE;
    int ret;

    struct sockaddr_nl snl;
    bzero(&snl, sizeof(struct sockaddr_nl));
    snl.nl_family = AF_NETLINK;
    snl.nl_pid = getpid();
    snl.nl_groups = 1|RTNLGRP_LINK;

    int s = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
    if (s == -1)
    {
        return -1;
    }
    setsockopt(s, SOL_SOCKET, SO_RCVBUF, &buffersize, sizeof(buffersize));

    ret = bind(s, (struct sockaddr *)&snl, sizeof(struct sockaddr_nl));
    if (ret < 0)
    {
        return -2;
    }

    return s;
}

/* listener for USB Event message*/
void CUSBListener::run()
{
    char buf[UEVENT_BUFFER_SIZE * 2] = {0};

    int sock = initSock();

    m_UsbPools.append("/block/sda/");
    m_UsbPools.append("/block/sdb/");
    m_UsbPools.append("/block/sdc/");
    m_UsbPools.append("/block/sdd/");
    m_UsbPools.append("/block/sde/");
    m_UsbPools.append("/block/sdf/");
    m_UsbPools.append("/block/sdg/");

//    qDebug() <<__FILE__<<__FUNCTION__<<__LINE__<< "Create Netlink..."<<sock;
    while(sock > 0)
    {
        /* Netlink message buffer */
        int b = recv(sock, &buf, sizeof(buf),0);
        if(b > 0)
        {
            onUSB(buf);
        }
    }
    qDebug() <<__FILE__<<__FUNCTION__<<__LINE__<< "Create Netlink failed:" << sock;
}


void CUSBListener::onUSB(const char *msg)
{
    QString recvMsg(msg);

    QString addPrefix = "add@";
    QString remPrefix = "remove@";
    //QStringList arg;

    //qDebug() << msg;
    int poolSize = m_UsbPools.size();
    for(int i=0; i<poolSize; i++)
    {
        if( recvMsg.contains( list_at( m_UsbPools,i)) )
        {
            QThread::sleep(2); //wait shell executed;
            ///Fix me
            if(recvMsg.startsWith(addPrefix))
            {                
                addPrefix = addPrefix +  m_UsbPools.at(i);
                QString devName = recvMsg.remove(0,addPrefix.size());

                if(QFile::exists("/dev/" + devName))
                {
                    QString dirName = QString("/media/%1").arg(devName);
                    if( !QFile::exists(dirName) )
                    {
                        servGui::showInfo( sysGetString(INF_UNKNOWN_USB,
                                                        "USB device"));
                    }
                    else
                    {
                        serviceExecutor::post( E_SERVICE_ID_STORAGE,
                                              MSG_STORAGE_USB_INSERT, dirName);
                    }
                }
                else
                {
                    qDebug() << devName << "not exist";
                }
            }
            else
            {
                addPrefix = remPrefix +  m_UsbPools.at(i);
                QString path = recvMsg.remove(0,addPrefix.size());

                serviceExecutor::post(E_SERVICE_ID_STORAGE,
                                      MSG_STORAGE_USB_REMOVE,
                                      path);
            }
        }
    }

    //!check usb-gpib link
    serviceExecutor::post(E_SERVICE_ID_SCPI,
                          dsoServScpi::cmd_usbgpib_check_link,0);
}


//void CUSBListener::onUSB(const char *msg)
//{
//    QString recvMsg(msg);

//    QString addPrefix = "add@";
//    QString remPrefix = "remove@";
//    //QStringList arg;

//    //qDebug() << msg;
//    int poolSize = m_UsbPools.size();
//    for(int i=0; i<poolSize; i++)
//    {
//        if( recvMsg.contains( list_at( m_UsbPools,i)) )
//        {
//            ///Fix me
//            if(recvMsg.startsWith(addPrefix))
//            {
//                QThread::sleep(1);
//                addPrefix = addPrefix +  m_UsbPools.at(i);

//                QString devName = recvMsg.remove(0,addPrefix.size());
//                if(QFile::exists("/dev/" + devName))
//                {
//                    //QString result;
//                    //mkdir -p /media/$MDEV
//                    //mount /dev/$MDEV /media/$MDEV

//                    QString dirName = QString("/media/%1").arg(devName);
//                    QString cmd     = QString("/bin/mkdir -p %1").arg(dirName);

//                    int nResult = QProcess::execute(cmd);

//                    cmd    = QString("/bin/mount /dev/%1 %2").arg(devName).arg(dirName);
//                    nResult = QProcess::execute(cmd);
//                    if( nResult != 0 )
//                    {
//                        cmd = QString("/bin/rm -fr %1").arg(dirName);
//                        QProcess::execute(cmd);
//                        servGui::showInfo( sysGetString(INF_UNKNOWN_USB,
//                                                        "UNKNOWN USB Mass Storage device"));
//                    }
//                    else
//                    {
//                        serviceExecutor::post( E_SERVICE_ID_STORAGE,
//                                              MSG_STORAGE_USB_INSERT, dirName);
//                    }
//                }
//                else
//                {
//                    qDebug() << devName << "not exist";
//                }
//            }
//            else
//            {
//                addPrefix = remPrefix +  m_UsbPools.at(i);
//                QString path = recvMsg.remove(0,addPrefix.size());

//                //umount -l /media/$MDEV*
//                //rm -rf /media/$MDEV*
//                if(path.size())
//                {
//                    QString cmd = QString("/bin/umount -l /media/%1").arg(path);
//                    //here will cost some seconds sometimes
//                    QProcess::execute(cmd);

//                    cmd = QString("/bin/rm -rf /media/%1").arg(path);
//                    QProcess::execute(cmd);
//                }

//                serviceExecutor::post(E_SERVICE_ID_STORAGE,
//                                      MSG_STORAGE_USB_REMOVE,
//                                      path);
//            }
//        }
//    }

//    //!check usb-gpib link
//    serviceExecutor::post(serv_name_scpi,
//                          dsoServScpi::cmd_usbgpib_check_link,0);
//}
