#ifndef _U_DISK_H
#define _U_DISK_H

#include "servhotplug.h"

class CUSBListener : public QThread
{
    Q_OBJECT
public:
    CUSBListener( QObject *parent );

protected:
    void run();

private:
    int     initSock(void);
    void    onUSB(const char *msg);

private:
    servHotplug*   m_pHotplug;

    QList<QString>  m_UsbPools;
};


#endif // _U_DISK_H

