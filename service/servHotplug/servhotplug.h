#ifndef SERVHOTPLUG_H
#define SERVHOTPLUG_H

#include "../service_name.h"
#include "../service_msg.h"
#include "../../include/dsoassert.h"

#include "../servgui/servgui.h"
#include "../servch/servch.h"
#include "../service.h"

#define HDMI_IRQ (1<<8)
#define RTC0_IRQ (1<<7)
#define XADC_IRQ (1<<6)
#define SRC1_IRQ (1<<5)
#define SRC2_IRQ (1<<4)
#define RMS1_IRQ (1<<3)
#define RMS2_IRQ (1<<2)
#define RMS3_IRQ (1<<1)
#define RMS4_IRQ (1<<0)
#define RMS_IRQ  (RMS1_IRQ | RMS2_IRQ | RMS3_IRQ | RMS4_IRQ)
#define THRESHOLD_LOW (0)
#define THRESHOLD_MID (2000)
#define THRESHOLD_UPP (2550)


class servHotplug : public serviceExecutor
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:
    enum
    {
       cmd_none,
       cmd_mask_irq,

       cmd_source1_enable,
       cmd_source2_enable,
       cmd_chan1_impedance,
       cmd_chan2_impedance,
       cmd_chan3_impedance,
       cmd_chan4_impedance,
    };
    servHotplug();
    ~servHotplug();

    DsoErr      start();
    void        registerSpy();

private:

    void        onMaskIRQ(void);

    int         maskFG1(bool);
    int         maskFG2(bool);

    void        maskRMS(int chan, bool pre_imp = IMP_50);
    void        maskCH1RMS(void);
    void        maskCH2RMS(void);
    void        maskCH3RMS(void);
    void        maskCH4RMS(void);

private:

    QThread*    m_pUSBListener;
    QThread*    m_pIRQListener;
    QThread*    m_pADCListener;

    bool        m_bSourceFG1;
    bool        m_bSourceFG2;
};



#endif // SERVHOTPLUG_H
