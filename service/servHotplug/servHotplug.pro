#-------------------------------------------------
#
# Project created by QtCreator 2016-11-09T10:09:44
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets
TARGET = ../../lib$$(PLATFORM)/services/servHotplug
TEMPLATE = lib
CONFIG += staticlib

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


SOURCES += servhotplug.cpp \
    udisk.cpp \
    irq.cpp \
    xadc.cpp

HEADERS += servhotplug.h \
    udisk.h \
    irq.h \
    xadc.h

