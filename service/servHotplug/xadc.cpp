#include "xadc.h"

CXADCListener::CXADCListener( QObject * ): QThread()
{
    m_ProbeID.append( new stProbeID(chan1) );
    m_ProbeID.append( new stProbeID(chan2) );
    m_ProbeID.append( new stProbeID(chan3) );
    m_ProbeID.append( new stProbeID(chan4) );

    //1:1
    m_ProbeData.append( new stProbeData(IMP_1M,  6,   24,   5,   45));
    m_ProbeData.append( new stProbeData(IMP_50,  6,   77,  55,  100));
    //10:1
    m_ProbeData.append( new stProbeData(IMP_1M,  9,  220, 190,  250));
    m_ProbeData.append( new stProbeData(IMP_50,  9,  356, 330,  390));
    //100:1
    m_ProbeData.append( new stProbeData(IMP_1M, 12,  475, 440,  490));
    m_ProbeData.append( new stProbeData(IMP_50, 12,  521, 500,  550));
    //5:1
    m_ProbeData.append( new stProbeData(IMP_50,  8,  695, 660,  720));
    //20:1
    m_ProbeData.append( new stProbeData(IMP_50, 10,  766, 730,  790));
    //50:1
    m_ProbeData.append( new stProbeData(IMP_50, 11,  843, 810,  870));
    //200:1
    m_ProbeData.append( new stProbeData(IMP_50, 14,  928, 900,  960));
    //1000:1
    m_ProbeData.append( new stProbeData(IMP_1M, 15, 1023, 990, 1050));
    //10:1
    m_ProbeData.append( new stProbeData(IMP_50,  9, 1124,1080, 1160));
    m_ProbeData.last()->nHead = Probe_Diff;
    //20:1
    m_ProbeData.append( new stProbeData(IMP_1M, 11, 1500,1460, 1540));
    m_ProbeData.append( new stProbeData(IMP_1M,  6, 3500,3000,3500));
}

#include "../servutility/servutility.h"
void CXADCListener::run()
{   
    QThread::sleep(15);//delay to start

    /* Query XADC event */
    while( true )
    {
        checkProbeStatus();
        QThread::sleep(1);
    }
}


void CXADCListener::checkProbeStatus()
{
    int size = m_ProbeID.size();
    for(int i=0; i<size; i++)
    {
        stProbeID* pProbeID =  list_at(m_ProbeID, i);
        QFile file(pProbeID->input_path);
        int voltage1 = 0;
        int voltage2 = 0;

        /****A. Reading sampling voltage***/
        if(!file.exists())
        {
            qDebug()<<__FILE__<<__LINE__<<"err:"<<file.fileName()<<" inexistence!!!";
            return;
        }

        if( file.open(QIODevice::ReadOnly) )
        {
            voltage1 = file.readAll().toInt();
            //qDebug()<<__FILE__<<__LINE__<<"voltage_1:"<<voltage1;
            file.close();
        }
        else
        {
            file.close();
            return;
        }

        pProbeID->voltage = (voltage1 * 15 / 10);

        /*! [dba, 2018-04-12], bug:探头经常识别失败*/
        if( pProbeID->voltage <= 3000)
        {
            QThread::msleep(500);  //! 消抖

            if( file.open(QIODevice::ReadOnly) )
            {
                voltage2 = file.readAll().toInt();
                //qDebug()<<__FILE__<<__LINE__<<"voltage_2:"<<voltage2;
                file.close();
            }
            else
            {
                file.close();
                return;
            }

            if( qAbs(voltage2 - voltage1) > 5)
            {
                return;
            }

            pProbeID->voltage = ((voltage1 + voltage2) / 2) * 15 / 10;
        }

        ServiceId chanId = (ServiceId)(E_SERVICE_ID_CH1+(pProbeID->chan-chan1));
        if(pProbeID->voltage > 3000) // 3000 / 1.5
        {

            if(pProbeID->inserted)
            {
                QString str = QString("%1 CH%2").
                        arg(sysGetString(INF_PROBE_DISCONNECT,
                                         "Probe disconnected with")).
                        arg(pProbeID->chan);
                servGui::showInfo(str);

                pProbeID->inserted = false;

                serviceExecutor::post(chanId,
                                      servCH::cmd_disconnect_probe,0);

                serviceExecutor::post(E_SERVICE_ID_UTILITY,
                                      servUtility::cmd_dso_beeper,1);
            }
        }
        else /*if( pProbeID->voltage > 3 && pProbeID->voltage < 1500)*/
        {
            if(!pProbeID->inserted &&
                pProbeID->voltage > 3 &&
                pProbeID->voltage < 1500)
            {

                QString str = QString("%1 CH%2").
                        arg(sysGetString(INF_PROBE_CONNECTED,
                                         "Probe connected with")).
                        arg(pProbeID->chan);
                servGui::showInfo(str);

                pProbeID->inserted = true;

                //by hxh
                //serviceExecutor::post(chanId, CMD_SERVICE_ACTIVE,1);

                CCHProbe *probe = new CCHProbe();
                if( getProbeInfo(pProbeID->voltage, probe))
                {
                    //qDebug()<< __FUNCTION__ << __LINE__ << "servCH::cmd_connect_probes";
                    QThread::sleep(1); //!等待连接稳定
                    serviceExecutor::post( chanId,
                                           servCH::cmd_connect_probe, probe);

                    serviceExecutor::post(E_SERVICE_ID_UTILITY,
                                          servUtility::cmd_dso_beeper,1);
                }
                else
                {
                    delete probe;
                    probe = NULL;
                }
            }
        }
    }
}

bool CXADCListener::getProbeInfo(int volt, CCHProbe *probe)
{
    //qDebug()<<__FILE__<<__LINE__<<"volt:"<<volt;
    foreach(stProbeData *p, m_ProbeData)
    {
        if( volt >= p->nLower && volt <= p->nUpper )
        {
            probe->setImpedance( p->bImp );
            probe->setRatio( p->nRatio);
            probe->setHead( p->nHead );
            probe->setType( p->nType );

            //            qDebug()<<__FILE__<<__LINE__
            //                    <<p->bImp
            //                    <<p->nRatio
            //                    <<p->nHead
            //                    <<p->nType;
            return true;
        }
    }
    return false;
}
