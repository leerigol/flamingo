#include "irq.h"
#include "udisk.h"
#include "xadc.h"

IMPLEMENT_CMD( serviceExecutor, servHotplug  )
start_of_entry()
on_set_void_void( servHotplug::cmd_mask_irq,       &servHotplug::onMaskIRQ),

on_set_int_bool( servHotplug::cmd_source1_enable, &servHotplug::maskFG1),
on_set_int_bool( servHotplug::cmd_source2_enable, &servHotplug::maskFG2),

on_set_void_void( servHotplug::cmd_chan1_impedance,&servHotplug::maskCH1RMS),
on_set_void_void( servHotplug::cmd_chan2_impedance,&servHotplug::maskCH2RMS),
on_set_void_void( servHotplug::cmd_chan3_impedance,&servHotplug::maskCH3RMS),
on_set_void_void( servHotplug::cmd_chan4_impedance,&servHotplug::maskCH4RMS),

end_of_entry()


servHotplug::servHotplug():
                serviceExecutor( QString(serv_name_hotplug),
                                 E_SERVICE_ID_NONE )
{
    serviceExecutor::baseInit();
}

servHotplug::~servHotplug()
{
    m_pUSBListener->deleteLater();
    m_pIRQListener->deleteLater();
    m_pADCListener->deleteLater();
}

void servHotplug::registerSpy()
{
    spyOn( serv_name_ch1, MSG_CHAN_IMPEDANCE,  cmd_chan1_impedance );//,e_spy_pre
    spyOn( serv_name_ch2, MSG_CHAN_IMPEDANCE,  cmd_chan2_impedance );
    spyOn( serv_name_ch3, MSG_CHAN_IMPEDANCE,  cmd_chan3_impedance );
    spyOn( serv_name_ch4, MSG_CHAN_IMPEDANCE,  cmd_chan4_impedance );
}

DsoErr servHotplug::start()
{
#if(1)

    m_pUSBListener = new CUSBListener(this);
    Q_ASSERT(m_pUSBListener);

    if(m_pUSBListener)
    {
        m_pUSBListener->start();
    }

    m_pIRQListener = new CIRQListener(this);
    Q_ASSERT(m_pIRQListener);

    if(m_pIRQListener)
    {
        m_pIRQListener->start();
    }

    m_pADCListener = new CXADCListener(this);
    Q_ASSERT(m_pADCListener);

    if(m_pADCListener)
    {
        m_pADCListener->start();
    }

    m_bSourceFG1  = false;
    m_bSourceFG2  = false;
#endif

    return ERR_NONE;
}



void servHotplug::onMaskIRQ()
{
    //1.mask 50ou overload protected
    maskRMS(chan1, IMP_1M);
    maskRMS(chan2, IMP_1M);
    maskRMS(chan3, IMP_1M);
    maskRMS(chan4, IMP_1M);

     CIRQListener* irq = (CIRQListener*)m_pIRQListener;

     irq->maskIRQ( HDMI_IRQ, 1);
     irq->maskIRQ( RTC0_IRQ, 0);
     irq->maskIRQ( XADC_IRQ, 0);
     irq->maskIRQ( SRC1_IRQ, 0);
     irq->maskIRQ( SRC2_IRQ, 0);
    //maskFG1();
    //maskFG2();

}

void servHotplug::maskCH1RMS(void)
{
    maskRMS(chan1);
}

void servHotplug::maskCH2RMS(void)
{
    maskRMS(chan2);
}

void servHotplug::maskCH3RMS(void)
{
    maskRMS(chan3);
}

void servHotplug::maskCH4RMS(void)
{
    maskRMS(chan4);
}

int servHotplug::maskFG1(bool en)
{

    CIRQListener* irq = (CIRQListener*)m_pIRQListener;

    irq->maskIRQ( SRC1_IRQ, (int)en);

    LOG_DBG() << "DG1 protection ENABLED" << en;
    return ERR_NONE;
}

int servHotplug::maskFG2(bool en)
{
    CIRQListener* irq = (CIRQListener*)m_pIRQListener;
    irq->maskIRQ( SRC2_IRQ, (int)en);
    LOG_DBG() << "DG2 protection ENABLED"<< en;
    return ERR_NONE;
}

void servHotplug::maskRMS(int /*chan*/, bool /*pre_imp*/)
{
    CIRQListener* irq = (CIRQListener*)m_pIRQListener;
    irq->readIRQ();
}


