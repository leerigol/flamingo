#ifndef _IRQ_DEF_H_
#define _IRQ_DEF_H_

#include "servhotplug.h"

enum
{
    DEV_IRQ_CLEAR	= 0xffffabcd,
    DEV_IRQ_MASK
};

class CIRQListener: public QThread
{
    Q_OBJECT
public:
    CIRQListener( QObject *parent );

public:
    void    clearIRQ(void);
    void    parseIRQ(void);
    void    maskIRQ(int , int );
    int     readIRQ();

protected:
    void    run();

private:
    static  void  sigHandler(int);
    static QSemaphore* m_pSem;

private:
    int     initDev(void);
    void    setupRTC(void);

    int     m_nDevFd;
    int     m_nMaskIRQ; //bit mask: 1 enable, 0 disable
    bool    m_bInsertHDMI;

};

#endif // _IRQ_DEF_H_

