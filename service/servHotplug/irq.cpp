#include <stdio.h>
#include <fcntl.h>       //!文件操作
#include <signal.h>
#include <unistd.h>     //getpid/getppid系统调用
#include <sys/ioctl.h>  //ioctl
#include <linux/rtc.h>
#include <sys/time.h>

#include <QDebug>
#include "../service_msg.h"
#include "../service_name.h"
#include "../servutility/servutility.h"
#include "irq.h"

#define DEV_IRQ_SIG  (SIGRTMIN + 12)


//int         CIRQListener::m_nDevFd = -1;
QSemaphore* CIRQListener::m_pSem   = NULL;

CIRQListener::CIRQListener( QObject * ): QThread()
{
    m_bInsertHDMI = false;
    m_nMaskIRQ    = 0;
}

/* this signal from kernel by /dev/devIRQ */
void CIRQListener::sigHandler(int sig)
{
    if(sig == DEV_IRQ_SIG)
    {
        if(m_pSem)
        {
            m_pSem->release();
        }
    }
}

#include "../../engine/cplatform.h"
void CIRQListener::parseIRQ(void)
{
    int irq_src = cplatform::sysGetDsoPlatform()->m_pPhy->mBoard.readIntStat();
    if( irq_src == 0xffff )
    {
        qDebug() << __FILE__ << __LINE__ << "read irq error";
        return;
    }
    //qDebug() << "OnIRQ" << QString().setNum(irq_src,16) << QString().setNum(m_nMaskIRQ,16);
    {
        //high priority IRQ
        int rms_irq = (~irq_src) & 0xf;
        if( rms_irq)
        {
            int irq[] = {RMS1_IRQ,RMS2_IRQ,RMS3_IRQ,RMS4_IRQ};
            int ch[]  = {E_SERVICE_ID_CH1,
                         E_SERVICE_ID_CH2,
                         E_SERVICE_ID_CH3,
                         E_SERVICE_ID_CH4};

            for(int i=0; i<4; i++)
            {
                if((irq[i] & m_nMaskIRQ) &&  rms_irq & irq[i])
                {
                    cplatform::sysGetDsoPlatform()->m_pPhy->m_ch[ i ].setImpedance( IMP_1M );
                    cplatform::sysGetDsoPlatform()->m_pPhy->m_ch[ i ].flushWCache();

                    serviceExecutor::post(ch[i], MSG_CHAN_IMPEDANCE, false);

                    QString str = QString("%1 CH%2").arg(sysGetString(INF_50_OVERLOAD,"Overload")).arg(ch[i]);
                    servGui::showInfo(str);
                }
            }

            irq_src &= (~RMS_IRQ);//Remove RMS interrupt
        }

        if(irq_src & HDMI_IRQ & m_nMaskIRQ)
        {
            if( !m_bInsertHDMI )
            {
                //open hdmi output
                /*serviceExecutor::post( E_SERVICE_ID_UTILITY,
                                       servUtility::cmd_hdmi_event, true);*/
                servGui::showInfo( sysGetString(INF_HDMI_INSERT,
                                                "HDMI is inserted"));
            }
            m_bInsertHDMI = true;
        }
        else
        {
            if(m_bInsertHDMI)
            {
                servGui::showInfo( sysGetString(INF_HDMI_UNINSERT,
                                                "HDMI is uninserted"));
                /*serviceExecutor::post( E_SERVICE_ID_UTILITY,
                                       servUtility::cmd_hdmi_event, false);*/
            }
            m_bInsertHDMI = false;
        }

        irq_src = ~irq_src;
        if( irq_src & SRC1_IRQ & m_nMaskIRQ)
        {
            servGui::showInfo( sysGetString(INF_DG_PROTECTED_G1,
                                            "Over voltage protection for G1"));
            serviceExecutor::post( E_SERVICE_ID_SOURCE1, MSG_APP_DG, false);
        }
        if( irq_src & SRC2_IRQ & m_nMaskIRQ)
        {
            servGui::showInfo( sysGetString(INF_DG_PROTECTED_G2,
                                            "Over voltage protection for G2"));
            serviceExecutor::post( E_SERVICE_ID_SOURCE2, MSG_APP_DG, false);
        }

    }

    //for SP6
    clearIRQ();
}

/**
 * @brief H-16:the IRQ source bit
 *        L_16:mask or not bit
 * @param mask
 */
void CIRQListener::maskIRQ(int irq_src, int irq_en)
{
        m_nMaskIRQ = cplatform::sysGetDsoPlatform()->m_pPhy->mBoard.readIntEn();
        //qDebug() << "IRQ MASK" << m_nMaskIRQ;

        if(irq_en)
        {
            m_nMaskIRQ |= (irq_src);
        }
        else
        {
            m_nMaskIRQ &= ~(irq_src);
        }
        cplatform::sysGetDsoPlatform()->m_pPhy->mBoard.maskInt(m_nMaskIRQ);
}

void CIRQListener::clearIRQ(void)
{
    cplatform::sysGetDsoPlatform()->m_pPhy->mBoard.clrInt();
}

int CIRQListener::readIRQ()
{
    m_nMaskIRQ = cplatform::sysGetDsoPlatform()->m_pPhy->mBoard.readIntEn();
    return m_nMaskIRQ;
}

int CIRQListener::initDev()
{
    const char* pDevName = "/dev/devIRQ";
    //!open the device.
    int fd = open((const char*)pDevName, O_RDWR|O_NONBLOCK|O_NDELAY);
    if (fd < 0)
    {
        qDebug() << "IRQ device error:" <<pDevName;
        return -1;
    }
    else
    {
       // qDebug() << "Open Interrupt device OK";
    }

    //!信号绑定
    struct sigaction saio;

    //!sigaction结构初始化
    saio.sa_handler  = sigHandler; //!信号处理函数
    sigemptyset(&saio.sa_mask);
    saio.sa_flags    = 0;
    saio.sa_restorer = NULL;

    //!sigaction函数的功能是检查或修改与指定信号相关联的处理动作（或同时执行这两种操作）。
    sigaction(DEV_IRQ_SIG, &saio, NULL);   //register the handle of the signal.

    //!将信号与本进程绑定;   getpid():取得进程识别码 getppid():取得父进程识别码
    fcntl(fd, F_SETOWN, getpid());
    //!切换到设备（fd）的异步操作模式
    fcntl(fd, F_SETFL , FASYNC | fcntl(fd, F_GETFL));
    //!设置标识输入输出可进行的信号
    fcntl(fd, F_SETSIG, DEV_IRQ_SIG);
    return fd;
}


void CIRQListener::run()
{
    QThread::sleep(5);//delay to start
    serviceExecutor::post(serv_name_hotplug,
                          servHotplug::cmd_mask_irq, 0);
    QThread::sleep(2);//delay to start

    m_nDevFd = initDev();
    if(m_nDevFd > 0)
    {
        m_pSem = new QSemaphore(1);
    }


    while( m_nDevFd > 0 )
    {
        m_pSem->acquire();
        parseIRQ();
    }
}

void CIRQListener::setupRTC()
{
    const char* pDevName = "/dev/rtc0";
    int rtc = open(pDevName, O_RDONLY);
    if( rtc < 0 )
    {

    }
    else
    {
        struct rtc_time rtc_tm;
        ioctl(rtc, RTC_RD_TIME, &rtc_tm);
        rtc_tm.tm_sec+=1;
        ioctl(rtc, RTC_ALM_SET, &rtc_tm);
        ioctl(rtc, RTC_AIE_ON, 0);
        close(rtc);
    }
}
