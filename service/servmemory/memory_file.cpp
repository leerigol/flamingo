#include "servmemory_file.h"
#include <QTextStream>
#include <QDebug>
#include <QFileInfo>
#include "../servdso/servdso.h"
#include "../servstorage/servstorage.h"

QFile*   CMemoryFile::m_dbg_pFile = NULL;
CMemoryFile::fileType  CMemoryFile::m_fileType = CMemoryFile::mem_file_csv;

vertAttr  CMemoryFile::m_vAttr;
horiAttr  CMemoryFile::m_hAttr;

CWFMFile   CMemoryFile::m_wfmFile;
CBinFile   CMemoryFile::m_binFile;
CCSVFile   CMemoryFile::m_csvFile;

quint64    CMemoryFile::m_totalCount = 0;

CMemoryFile::CMemoryFile()
{
}

qint32 CMemoryFile::s_fOpen(QString fileName)
{
#ifdef MEM_FILE_DBUG
    s_dbg_fOpen(fileName);
#endif

    //! type open
    QFileInfo  fileInfo(fileName);
    if(fileInfo.suffix() == "csv")
    {
        m_fileType = mem_file_csv;
        if( !m_csvFile.create(fileName) )
        {
            return ERR_FILE_SAVE_FAIL;
        }
    }
    else if(fileInfo.suffix() == "bin")
    {
        m_fileType = mem_file_bin;
        if( !m_binFile.create(fileName) )
        {
            return ERR_FILE_SAVE_FAIL;
        }
    }
    else if(fileInfo.suffix() == "wfm")
    {
        m_fileType = mem_file_wfm;
        if( !m_wfmFile.create(fileName) )
        {
            return ERR_FILE_SAVE_FAIL;
        }
    }
    else
    {
        LOG_DBG()<<fileName<<fileInfo.suffix();
        return ERR_FILE_SAVE_FAIL;
    }
    return ERR_NONE;
}

qint32 CMemoryFile::s_fClose()
{
#ifdef MEM_FILE_DBUG
    s_dbg_fClose();
#endif

    if (mem_file_csv == m_fileType)
    {
        m_csvFile.close();
    }
    else if (mem_file_bin == m_fileType)
    {
        m_binFile.close();
    }
    else if (mem_file_wfm == m_fileType)
    {
        m_wfmFile.close();
    }
    else
    {
        return ERR_FILE_SAVE_FAIL;
    }

    return 0;
}


qint32 CMemoryFile::s_fOut(quint8 *pBuf, int size)
{
    Q_ASSERT( NULL != pBuf && size > 0 );

#ifdef MEM_FILE_DBUG
    s_dbg_fOut(pBuf, size);
#endif

    if (mem_file_csv == m_fileType)
    {
        if( (-1) != s_fOutCsv(pBuf, size) )
        {
            return size;
        }
    }
    else if (mem_file_bin == m_fileType)
    {
        if( (-1) != s_fOutBin(pBuf, size))
        {
            return size;
        }
    }
    else if (mem_file_wfm == m_fileType)
    {
        if( (-1) != s_fOutWfm( pBuf, size))
        {
            return size;
        }
    }


    return -1;
}


qint64 CMemoryFile::s_fIn( quint8 *pBuf, quint64 size)
{
    quint64 retSize = 0;
    if (mem_file_wfm == m_fileType)
    {
        retSize = s_fInWfm(pBuf, size);
    }
    else
    {
        return -1;
    }

    return retSize;
}

qint32 CMemoryFile::s_setVHAttr(vertAttr vattr,
                                        horiAttr hAttr)
{
    m_vAttr = vattr;
    m_hAttr = hAttr;
    return 0;
}

qint32 CMemoryFile::s_fSetOutCount(qint64 totalCount)
{
    m_totalCount = totalCount;

    //! 检查磁盘空间是否足够。
    qint64   diskAvailable = 0;
    qint64   diskNeed      = 0;
    QString  filePathName  = "";

    if (mem_file_csv == m_fileType)
    {
        filePathName = m_csvFile.getFileName();

        bool timeEn = false;
        serviceExecutor::query(serv_name_storage, MSG_STORAGE_MEM_CSV_TIME, timeEn);

        diskNeed     = totalCount* (timeEn? (15*2) : 15);
    }
    else if (mem_file_bin == m_fileType)
    {
        filePathName = m_binFile.getFileName();
        diskNeed     = totalCount + 1E3;
    }
    else if (mem_file_wfm == m_fileType)
    {
        filePathName = m_wfmFile.getFileName();
        diskNeed     = totalCount + 5E6;
    }
    else
    {
        return ERR_FILE_SAVE_FAIL;
    }

    diskAvailable = servStorage::getFreeSpace(filePathName);

    qDebug() << filePathName <<"Need:"<<diskNeed<<"Available:"<<diskAvailable ;
    if(diskNeed >= diskAvailable)
    {
        return ERR_FILE_SAVE_FAIL;
    }
    else
    {
        return ERR_NONE;
    }
}

qint32 CMemoryFile::s_fSetOutHeader(Chan ch)
{
    if (mem_file_csv == m_fileType)
    {
        s_fSetCsvHeader(ch);
    }
    else if (mem_file_bin == m_fileType)
    {
        s_fSetBinHeader(ch);
    }
    else if (mem_file_wfm == m_fileType)
    {
        s_fSetWfmHeader();
    }
    else
    {
        //Q_ASSERT(false);
        return ERR_FILE_SAVE_FAIL;
    }

    return 0;
}

qint32 CMemoryFile::s_fGetInHeader(QByteArray &setupData)
{
    return s_fGetWfmHeader(setupData);
}

bool CMemoryFile::s_foutFrmInfo(stFrmInfo &frameInfo)
{
    if (mem_file_wfm != m_fileType)
    {
        return false;
    }

    return m_wfmFile.writeFrameInfo(frameInfo);
}

bool CMemoryFile::s_fInFrmInfo(stFrmInfo &frameInfo)
{
    if (mem_file_wfm != m_fileType)
    {
        return false;
    }

    return m_wfmFile.readFrameInfo(frameInfo);
}

qint32 CMemoryFile::s_fSetCsvHeader(Chan ch)
{
    m_csvFile.writeCSVHeader(ch, m_vAttr, m_hAttr);
    return ERR_NONE;
}

qint32 CMemoryFile::s_fSetBinHeader(Chan ch)
{
    int     unit  = BIN_UNIT_UNKNOW;
    QString label = "LA";

    m_binFile.writeBinHeader(1);

    if( (chan1<=ch) && (ch <= chan4) )
    {
        dsoVert* pData  = dsoVert::getCH( ch);
        Q_ASSERT(pData != NULL);

        if(pData->getUnit() == Unit_A)
        {
            unit = BIN_UNIT_A;
        }
        else if(pData->getUnit() == Unit_V)
        {
            unit = BIN_UNIT_V;
        }
        label = pData->getCHLabel();
    }

    //!仅用于确定inc和t0
    DsoWfm   wfm;
    //pData->getTrace(wfm);
    m_binFile.writeWaveHeader(ch, unit, label, wfm);

    return ERR_NONE;
}

qint32 CMemoryFile::s_fSetWfmHeader()
{
    QByteArray setupData;

    servDso::pullSession(setupData);
    m_wfmFile.writeHeader(setupData);
    return ERR_NONE;
}

qint32 CMemoryFile::s_fGetWfmHeader(QByteArray &setupData)
{
    setupData.clear();
    m_wfmFile.readHeader(setupData);
    return setupData.size();
}

qint32 CMemoryFile::s_fOutCsv(quint8 *pBuf, int size)
{
    Q_ASSERT( NULL != pBuf && size > 0 );

    if( m_csvFile.writeWaveData(pBuf, size, m_totalCount) )
    {
        return size;
    }
    return (-1);
}

qint32 CMemoryFile::s_fOutBin(quint8 *pBuf, int size)
{
    Q_ASSERT( NULL != pBuf && size > 0 );

    if( m_binFile.writeData(pBuf, size, m_totalCount) )
    {
        return size;
    }
    return (-1);
}

qint32 CMemoryFile::s_fOutWfm(quint8 *pBuf, int size)
{
    Q_ASSERT( NULL != pBuf );
    Q_ASSERT( size > 0 );

    if( m_wfmFile.writeData(pBuf, size, m_totalCount) )
    {
        return size;
    }
    return (-1);
}


qint32 CMemoryFile::s_fOutCsv(QList<DsoWfm *> dataList)
{
    m_csvFile.writeWaveData(dataList);
    return ERR_NONE;
}

qint32 CMemoryFile::s_fOutBin(DsoWfm &data)
{
    m_binFile.writeWaveData(data);
    return ERR_NONE;
}

qint64 CMemoryFile::s_fInWfm( quint8 *pBuf,
                              quint64 size)
{
    Q_ASSERT(NULL != pBuf);
    return m_wfmFile.readData(pBuf, size);
}

/*!---------------------------------dbg-----------------------------------------*/
qint32 CMemoryFile::s_dbg_fOpen(QString fileName)
{
    Q_ASSERT(m_dbg_pFile == NULL);

    m_dbg_pFile = new QFile(fileName + "_debug.bin");
    if( !m_dbg_pFile->open(QIODevice::Append) )
    {
        qDebug()<<fileName<<"open fail";
    }
    return 0;
}

qint32 CMemoryFile::s_dbg_fClose()
{
    Q_ASSERT(m_dbg_pFile != NULL);
    m_dbg_pFile->close();
    delete m_dbg_pFile;
    m_dbg_pFile = NULL;
    return 0;
}

qint32 CMemoryFile::s_dbg_fOut(quint8 *pBuf, int size)
{
    Q_ASSERT( NULL != pBuf && size > 0 );
    Q_ASSERT(m_dbg_pFile != NULL);

#if 0 //! csv
    QTextStream out(m_dbg_pFile);
    for(int i = 0 ; i < size; i++)
    {
        quint8 temp = pBuf[i];
        out<<QString::number(temp, 10)<<"\n";
    }

    qint32 fileSize  = m_dbg_pFile->size();
    qDebug()<<"file size: "<<fileSize;
    return fileSize;
#else //! bin
    m_dbg_pFile->write((char*)pBuf, size);
    qint32 fileSize  = m_dbg_pFile->size();
    qDebug()<<__FILE__<<__LINE__<<"debug file size: "<<fileSize;
    return fileSize;
#endif
}

qint32 CMemoryFile::s_dbg_fIn(quint8 *pBuf, int size)
{
    Q_ASSERT( NULL != pBuf && size > 0 );
    return 0;
}

