#ifndef SERVMEMORY_SAVEFILE_H
#define SERVMEMORY_SAVEFILE_H
#include <QString>
#include <QFile>
#include "../servdso/sysdso.h"
#include "../servstorage/storage_wav.h"

using namespace DsoType;


class CMemoryFile
{

private:
    enum fileType
    {
        mem_file_csv,
        mem_file_bin,
        mem_file_wfm,
    };
public:
    CMemoryFile();

public:
    static qint32 s_fOpen(QString fileName);
    static qint32 s_fClose();

    static qint32 s_fOut(quint8 *pBuf,
                         int size);


    static qint64 s_fIn(quint8    *pBuf,
                         quint64    size);

    static qint32 s_setVHAttr(vertAttr vattr, horiAttr hAttr);
    static qint32 s_fSetOutCount(qint64 totalCount);
    static qint32 s_fSetOutHeader(Chan ch = chan1);
    static qint32 s_fGetInHeader(QByteArray &setupData);

    static bool s_foutFrmInfo(stFrmInfo &frameInfo);
    static bool s_fInFrmInfo( stFrmInfo &frameInfo);

private:
    static qint32 s_fSetCsvHeader(Chan ch);
    static qint32 s_fSetBinHeader(Chan ch);
    static qint32 s_fSetWfmHeader();
    static qint32 s_fGetWfmHeader(QByteArray &setupData);


    static qint32 s_fOutCsv( quint8 *pBuf, int size);
    static qint32 s_fOutBin( quint8 *pBuf, int size);
    static qint32 s_fOutWfm(quint8 *pBuf, int size);


    static qint32 s_fOutCsv( QList<DsoWfm*> dataList );
    static qint32 s_fOutBin(DsoWfm &data );


    static qint64 s_fInWfm(quint8    *pBuf,
                            quint64   size);

private:
    static qint32 s_dbg_fOpen(  QString fileName);
    static qint32 s_dbg_fClose();
    static qint32 s_dbg_fOut(quint8 *pBuf, int size);
    static qint32 s_dbg_fIn( quint8 *pBuf, int size);

private:
    static QFile     *m_dbg_pFile;
    static fileType   m_fileType;
    static CBinFile   m_binFile;
    static CCSVFile   m_csvFile;
    static CWFMFile   m_wfmFile;
    static quint64    m_totalCount; //!为了显示进度条用
private:
    static vertAttr  m_vAttr;
    static horiAttr  m_hAttr;
};

#endif // SERVMEMORY_SAVEFILE_H
