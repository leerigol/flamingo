#ifndef SERVMEMORY_H
#define SERVMEMORY_H
#include "../service.h"

class servMemory: public serviceExecutor
{
    Q_OBJECT
    DECLARE_CMD()

public:
    enum servCmd
    {
        CMD_MEM_CHANNEL = 1,
        CMD_MEM_EXPORT,
        CMD_MEM_EXPORT_CH,
        CMD_MEM_IMPORT,
        CMD_MEM_IMPORT_CH,
        CMD_MEM_IMPORT_SETUP,
    };
public:
    servMemory( QString name, ServiceId id=E_SERVICE_ID_MEMORY);
    ~servMemory();
    virtual DsoErr start();

public:
   DsoErr applySetup();

public:
   DsoErr exportChMemory(QString fileName);
   DsoErr importChMemory(QString fileName);

   DsoErr exportAllMemory(QString fileName,
                          qint64 frameStart,
                          qint64 frameEnd );

   DsoErr importAllMemory(QString fileName);

   vertAttr getVAttr(Chan ch);
   horiAttr getHAttr(Chan ch);

private:
   DsoErr exportMemoryData(quint64 frame, quint64 start, quint64 end);

   bool   importMemoryData();

public:
   static DsoErr waitHorStop();
   static DsoErr s_exportChMemory(QString fileName);
   static DsoErr s_importChMemory(QString fileName);

   static DsoErr s_exportAllMemory(QString fileName,
                                   qint64  frameStart,
                                   qint64  frameEnd );

   static DsoErr s_importAllMemory(QString fileName);

   static vertAttr s_getVAttr(Chan ch);
   static horiAttr s_getHAttr(Chan ch);

private:
   QByteArray      mSetupData;
};

#endif // SERVMEMORY_H
