QT       += core
QT       += gui
QT       += widgets
QT       += network

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/services/servMemory
INCLUDEPATH += .

platform = $$(_FILE_DBUG)
contains( platform, ON ){
DEFINES += MEM_FILE_DBUG
}

#DEFINES += _DEBUG

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
DEFINES -= _DSO_KEYBOARD
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


SOURCES += \
    servmemory.cpp \
    memory_file.cpp

HEADERS += \
    servmemory.h \
    servmemory_file.h

