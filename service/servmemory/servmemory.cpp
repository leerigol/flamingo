//#define _DEBUG 1

#include "servmemory.h"
#include "../../engine/base/dsoengine.h"
#include "../../engine/cplatform.h"
#include "../../engine/base/adt/patchrequest.h"
#include "servmemory_file.h"
#include "../servdso/servdso.h"
#include "../servtrace/servtrace.h"
#include "../../service/servicefactory.h"

IMPLEMENT_CMD( serviceExecutor, servMemory )
start_of_entry()
on_set_int_void(servMemory::CMD_MEM_EXPORT,       &servMemory::exportAllMemory),
on_set_int_void(servMemory::CMD_MEM_IMPORT_SETUP, &servMemory::applySetup),

end_of_entry()

#define batch_size  (2*patch_size*sub_patch)

static servMemory *pServPtr = NULL;

servMemory::servMemory(QString name, ServiceId id)
    :serviceExecutor(name,id)
{
    serviceExecutor::baseInit();
    pServPtr = this;
}

servMemory::~servMemory()
{
}

DsoErr servMemory::start()
{
    return ERR_NONE;
}

DsoErr servMemory::waitHorStop()
{
    bool bRun = true;

    serviceExecutor::query(serv_name_trace, servTrace::cmd_trace_run, bRun);

    //!stop
    serviceExecutor::post(E_SERVICE_ID_HORI, MSG_HORIZONTAL_RUN, Control_Stop);

    int  count = 0;
    do
    {
        QThread::msleep(1000); //!  等1s，保证引擎状态切换完成。
        if(count > 5)
        {
            LOG_DBG()<<"wait trace stop timeout";
            return ERR_FILE_SAVE_FAIL;
        }
        count++;
        serviceExecutor::query(serv_name_trace, servTrace::cmd_trace_run, bRun);
    }while(bRun);

    return ERR_NONE;
}

DsoErr servMemory::applySetup()
{
#if 1
    if(!mSetupData.isEmpty())
    {
        servDso::pushSession(mSetupData);
        LOG_DBG()<<"startup";

        //! create context
        struActionContextRmt context( false, false );
        context.isUser = false;
        QSemaphore sema;

        context.m_pSema = &sema;

        //! startup
        QList<service*>* pServList = serviceFactory::getServiceList();

        bool bmso     = sysHasLA();
        int  count    = pServList->size() ;
        int  postSize = 0;

        servMath::resetData();
        for(int i=0; i<count; i++)
        {
            //mso setting can not apply to ds
            if( !bmso && pServList->at(i)->getName().compare(serv_name_la) == 0 )
            {
                continue;
            }

            serviceExecutor::post(pServList->at(i)->getId(),
                                  CMD_SERVICE_STARTUP,
                                  (int)0,
                                  &context );
            postSize++;
        }

        serviceExecutor::post(E_SERVICE_ID_HORI,
                              MSG_HORIZONTAL_RUN,
                              Control_Run,
                              &context);
        postSize++;

        //! check progress
        while( sema.available() != postSize)
        {
            QThread::msleep( 30 );
        }

        QThread::msleep( 500 );
    }
#endif
    return ERR_NONE;
}

DsoErr servMemory::exportChMemory(QString fileName)
{
    /*! ----------------------------------*/
    QTime timeTick;
    timeTick.start();
    /*! ----------------------------------*/

    DsoErr err = ERR_NONE;

    //!get ch
    int  ch = chan1;
    serviceExecutor::query(serv_name_storage,
                           MSG_STORAGE_CHANNEL, ch);

    //!stop
    err = waitHorStop();
    if(err != ERR_NONE)
    {
        return err;
    }

    //! open
    dsoEngine *pEngine = NULL;
    pEngine = cplatform::sysGetDsoPlatform()->getPlayEngine();
    Q_ASSERT(pEngine != NULL);

    handleMemory *handle = pEngine->openMemory((Chan)ch);
    if ( NULL == handle || !handle->getValid() )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    //qDebug()<<"FullSize:"<<handle->getFullSize();

    //! Engine export init
    pEngine->exportInit(handle);

    if( ERR_NONE != CMemoryFile::s_fOpen(fileName) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    CMemoryFile::s_setVHAttr( getVAttr((Chan)ch), getHAttr((Chan)ch) );
    CMemoryFile::s_fSetOutHeader((Chan)ch);

    if( ERR_NONE != CMemoryFile::s_fSetOutCount(handle->getFullSize()) )
    {
        CMemoryFile::s_fClose();
        return ERR_FILE_SAVE_FAIL;
    }

    //! batch reading
    int ret;
    quint8 *pBuf = new quint8[ batch_size ];
    Q_ASSERT( NULL != pBuf );

    //! split package
    int j;
    for ( j = 0; j < (handle->getFullSize()/batch_size); j++ )
    {
        //! full reading
        ret = pEngine->readMemory( handle, pBuf, j*batch_size, batch_size );
        LOG_DBG()<<ret;
        if(ret > 0)
        {
            ret = CMemoryFile::s_fOut(pBuf, ret);
        }

        if(ret < 0)
        {
            err = ERR_FILE_SAVE_FAIL;
            j   = (handle->getFullSize()/batch_size);
            break;
        }
    }

    //! last package
    int lastPackage = handle->getFullSize() - batch_size*j;
    if ( lastPackage > 0 )
    {
        ret = pEngine->readMemory( handle, pBuf, j*batch_size, lastPackage );
        LOG_DBG()<<ret;
        if(ret > 0)
        {
            ret = CMemoryFile::s_fOut(pBuf, ret);
        }
        else if(ret < 0)
        {
            err = ERR_FILE_SAVE_FAIL;
        }
    }

    delete []pBuf;
    pBuf = NULL;

    //! close
    pEngine->closeMemory( handle );
    CMemoryFile::s_fClose();

    LOG_DBG()<<"time:"<<timeTick.elapsed()<<"ms";
    return err;
}

DsoErr servMemory::importChMemory(QString fileName)
{
    qDebug()<<__FILE__<<__FUNCTION__<<fileName;
    return ERR_NONE;
}

DsoErr servMemory::exportAllMemory(QString fileName, qint64 frameStart, qint64 frameEnd)
{
    DsoErr err = ERR_NONE;

    dsoEngine *pEngine = NULL;
    pEngine = cplatform::sysGetDsoPlatform()->getPlayEngine();
    Q_ASSERT(pEngine != NULL);

    //!stop
    err = waitHorStop();
    if(err != ERR_NONE)
    {
        return err;
    }

    //! open
    handleMemory *handleAna = pEngine->openMemory( ana );
    handleMemory *handleLa  = pEngine->openMemory( la );
    if ( NULL == handleAna || !handleAna->getValid() ||
         NULL == handleLa  || !handleLa->getValid() )
    { return ERR_FILE_SAVE_FAIL; }

    qint64 totalCount  = (handleAna->getFullSize() + handleLa->getFullSize())*(frameEnd - frameStart + 1);
    pEngine->closeMemory( handleAna );
    pEngine->closeMemory( handleLa );

    if( ERR_NONE !=  CMemoryFile::s_fOpen(fileName) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    CMemoryFile::s_fSetOutHeader();
    if( ERR_NONE != CMemoryFile::s_fSetOutCount(totalCount) )
    {
        CMemoryFile::s_fClose();
        return ERR_FILE_SAVE_FAIL;
    }

    LOG_DBG()<<"totalCount:"<<totalCount;

    for(int frame = frameStart; frame <= frameEnd; frame++)
    {
        err = exportMemoryData(frame, frameStart, frameEnd);

        if (ERR_NONE != err)
        {
            break;
        }
    }

    CMemoryFile::s_fClose();

    return err;
}

DsoErr servMemory::importAllMemory(QString fileName)
{
    DsoErr err = ERR_NONE;

    //!open file
    CMemoryFile::s_fOpen(fileName);

    //!setup
    int headerSize = CMemoryFile::s_fGetInHeader(mSetupData);
    LOG_DBG()<<"header Size:"<<headerSize;
    applySetup();

    //!stop
    err = waitHorStop();
    if(err != ERR_NONE)
    {
        return err;
    }

    //!import Data
    bool b = true;
    do{
        b = importMemoryData();
    }while(b);


    //! close
    CMemoryFile::s_fClose();

    return ERR_NONE;
}

vertAttr servMemory::getVAttr(Chan ch)
{
    vertAttr  attr;
    if( (chan1<=ch) && (ch <= chan4) )
    {
        attr = getVertAttr(ch);
    }
    else
    {
        attr.yGnd  = 0;
        attr.yInc  = 1;
        attr.yUnit = Unit_V;
    }
    return attr;
}

horiAttr servMemory::getHAttr(Chan ch)
{
    horiAttr hAttr;
    CSaFlow  saAttr;
    if( (chan1<=ch) && (ch<=chan4))
    {
        queryEngine( qENGINE_ACQ_CH_MAIN_SAFLOW, &saAttr);
    }
    else if( (d0<=ch) && (ch<=d15) )
    {
        queryEngine( qENGINE_ACQ_LA_MAIN_SAFLOW, &saAttr);
    }
    else if( (d0d7==ch) || (d8d15 == ch)|| (d0d15 == ch)|| (la == ch) )
    {
        queryEngine( qENGINE_ACQ_LA_MAIN_SAFLOW, &saAttr);
    }
    else
    {Q_ASSERT(false);}

    hAttr.tInc = saAttr.getDotTime().toLonglong();
    hAttr.inc  = (float)hAttr.tInc/time_s(1);
    hAttr.gnd  = (saAttr.getTLength().toLonglong()/2 - saAttr.mSaOffset)/hAttr.tInc;

    hAttr.unit = Unit_s;
    hAttr.zone = horizontal_time_zone;

    LOG_DBG()<<"t0   :"<<saAttr.getT0().toLonglong()
            <<"tEnd :"<<saAttr.getTEnd().toLonglong()
           <<"offs :"<<saAttr.mOffset;
    return hAttr;
}

DsoErr servMemory::exportMemoryData(quint64  frame, quint64 start,quint64  end)
{
    DsoErr  err  = ERR_NONE;

    dsoEngine *pEngine = cplatform::sysGetDsoPlatform()->getPlayEngine();
    Q_ASSERT(pEngine != NULL);

    stFrmInfo frameInfo;
    frameInfo.frameStart = start;
    frameInfo.frameEnd   = end;
    frameInfo.frame      = frame;

    //! new buf
    quint8 *pBuf = new quint8[ batch_size ];
    Q_ASSERT( NULL != pBuf );
    int ret = -1;

    int  chSize  = 2;
    do{ //! 先导出LA，再导出模拟。
        Chan ch = (chSize>1)? la : ana;

        //!open
        handleMemory *handle = pEngine->openMemory(ch);
        if ( NULL == handle || !handle->getValid() )
        { return ERR_FILE_SAVE_FAIL; }
        LOG_DBG()<<"FullSize:"<<handle->getFullSize();

        //! Engine export init
        frameAttr attr;
        attr.mFrameId = frame;
        pEngine->exportInit(handle, attr);

        //! save frameInfo
        frameInfo.ch    = (quint32)ch;
        frameInfo.size  = handle->getFullSize();
        frameInfo.saChLen = attr.saChLen;
        frameInfo.saLaLen = attr.saLaLen;
        frameInfo.saValid = attr.saValid;

        if(!CMemoryFile::s_foutFrmInfo(frameInfo))
        {
            err = ERR_FILE_SAVE_FAIL;
            break;
        }

        //! split package
        int j = 0;
        for ( j = 0; j < (handle->getFullSize() / batch_size); j++ )
        {
            //! full reading
            ret = pEngine->readMemory( handle, pBuf, j*batch_size, batch_size );
            LOG_DBG()<<"from:"<<j*batch_size<<"len:"<<batch_size<<"ret:"<<ret<<"\n";

            if(ret > 0)
            {
                ret = CMemoryFile::s_fOut(pBuf, ret);
            }

            if(ret < 0)//!如果有一次请求read失败，退出整个循环。
            {
                err = ERR_FILE_SAVE_FAIL;
                j   = handle->getFullSize()/batch_size;
                break;
            }
        }

        //! last package
        int lastPackage = handle->getFullSize() - batch_size*j;
        if ( lastPackage > 0 )
        {
            ret = pEngine->readMemory( handle, pBuf, j*batch_size, lastPackage );
            LOG_DBG()<<"from:"<<j*batch_size<<"len:"<<lastPackage<<"ret:"<<ret<<"\n";

            if(ret > 0)
            {
                ret = CMemoryFile::s_fOut(pBuf, ret);
            }

            if(ret < 0)
            {
                err = ERR_FILE_SAVE_FAIL;
            }
        }
        //! close
        pEngine->closeMemory( handle );

        chSize--;

    }while( (chSize>0) && (ERR_NONE == err) );

    //! delete buf
    delete []pBuf;
    pBuf = NULL;

    return err;
}

bool servMemory::importMemoryData()
{
    int    ret = 0;

    //! open handleMemory
    dsoEngine *pEngine = cplatform::sysGetDsoPlatform()->getPlayEngine();
    Q_ASSERT( NULL != pEngine);

    stFrmInfo frameInfo;
    if(!CMemoryFile::s_fInFrmInfo(frameInfo))
    {
        LOG_DBG()<<"read frmInfo failure.";
        return false;
    }

    frameAttr attr;
    attr.mFrameStart = 0;
    attr.mCh         = frameInfo.ch;
    attr.mFrameId    = frameInfo.frame;
    attr.mFrameEnd   = frameInfo.frameEnd - frameInfo.frameStart;
    attr.mFrameLen   = frameInfo.size;

    attr.saValid    = frameInfo.saValid;
    attr.saChLen    = frameInfo.saChLen;
    attr.saLaLen    = frameInfo.saLaLen;

    Q_ASSERT(attr.mFrameEnd >= attr.mFrameStart);


    handleMemory *handle = pEngine->openMemory( (Chan)frameInfo.ch );
    if( NULL == handle)
    {
        return ERR_FILE_MAGIC;
    }

    //! Engine export init
    pEngine->importInit(attr);

    //! new buf
    quint8 *pBuf = new quint8[ batch_size ];
    Q_ASSERT( NULL != pBuf );

    //! import
    qint64 importSize = 0;
    qint64 inSize     = 0;

    bool eof = false;
    do
    {   //!read file
        inSize = CMemoryFile::s_fIn(pBuf, batch_size);

        LOG_DBG()<<"frame:"    <<frameInfo.frame
                <<"Info size:"<<frameInfo.size
               <<"in size:"  <<inSize
              <<"import:"   <<importSize;

        if(inSize <= 0)
        {
            break;
        }

        if(importSize + inSize >= (qint64)frameInfo.size)
        {
            eof = true;
        }

        //!write fpga ram
        ret = pEngine->writeMemory( handle, pBuf, (int)importSize, (int)inSize, eof );

        LOG_DBG()<<ret;
        if(ret < 0)
        {
            break;
        }

        importSize += inSize;

    }while( !eof );

    //! close
    pEngine->closeMemory( handle );

    delete []pBuf;
    pBuf = NULL;

    pEngine->importEnd();

    // end
    if( (ana == attr.mCh) && (attr.mFrameId == attr.mFrameEnd) )
    {
        pEngine->importPlay(attr);
        return false;
    }

    return true;
}

/*-----------------------------------------------------------------------------*/
DsoErr servMemory::s_exportChMemory(QString fileName)
{
    Q_ASSERT(pServPtr != NULL);
    return pServPtr->exportChMemory(fileName);
}

DsoErr servMemory::s_importChMemory(QString fileName)
{
    Q_ASSERT(pServPtr != NULL);
    return pServPtr->importChMemory(fileName);
}

DsoErr servMemory::s_exportAllMemory(QString fileName, qint64 frameStart, qint64 frameEnd)
{
    Q_ASSERT(pServPtr != NULL);
    return pServPtr->exportAllMemory(fileName, frameStart, frameEnd);

}

DsoErr servMemory::s_importAllMemory(QString fileName)
{
    Q_ASSERT(pServPtr != NULL);
    return pServPtr->importAllMemory(fileName);
}

vertAttr servMemory::s_getVAttr(Chan ch)
{
    Q_ASSERT(pServPtr != NULL);
    return pServPtr->getVAttr(ch);
}

horiAttr servMemory::s_getHAttr(Chan ch)
{
    Q_ASSERT(pServPtr != NULL);
    return pServPtr->getHAttr(ch);
}

