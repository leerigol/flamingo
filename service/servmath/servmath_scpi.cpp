#include "servmath.h"

#define FILTER_ORDER    150
#define H_FILTER        20

DsoErr servMath::scpiSetFilterBw1(qlonglong Bw1)
{
    qlonglong sa = static_cast<qlonglong>( getHSa(mSrc.mAnaSrc.mSrcA) );
    qlonglong pBase = 1e6*sa/(200.0);
    switch (mOperator) {
    case operator_lp:
        mOption.mFilterOpt.mLpW = Bw1 / pBase;
        async(MSG_MATH_S32FILTER_LP, mOption.mFilterOpt.mLpW);
        break;
    case operator_hp:
        mOption.mFilterOpt.mHpW = Bw1 / pBase;
        async(MSG_MATH_S32FILTER_HP, mOption.mFilterOpt.mHpW);
        break;
    case operator_bp:
        mOption.mFilterOpt.mBpW1 = Bw1 / pBase;
        async(MSG_MATH_S32FILTER_BP1, mOption.mFilterOpt.mBpW1);
        break;
    case operator_bt:
        mOption.mFilterOpt.mBtW1 = Bw1 / pBase;
        async(MSG_MATH_S32FILTER_BT1, mOption.mFilterOpt.mBtW1);
        break;
    default:
        break;
    }
    return ERR_NONE;
}

qlonglong servMath::scpiGetFilterBw1()
{
    qlonglong sa = static_cast<qlonglong>( getHSa(mSrc.mAnaSrc.mSrcA) );
    qlonglong pBase = 1e6*sa/(200.0);
    switch (mOperator) {
    case operator_lp:
        return mOption.mFilterOpt.mLpW * pBase;
        break;
    case operator_hp:
        return (mOption.mFilterOpt.mHpW * pBase);
        break;
    case operator_bp:
        return (mOption.mFilterOpt.mBpW1 * pBase);
        break;
    case operator_bt:
        return (mOption.mFilterOpt.mBtW1 * pBase);
        break;
    default:
        break;
    }
    return ERR_NONE;
}

DsoErr servMath::scpiSetFilterBw2(qlonglong Bw2)
{
    qlonglong sa = static_cast<qlonglong>( getHSa(mSrc.mAnaSrc.mSrcA) );
    qlonglong pBase = 1e6*sa/(200.0);
    switch (mOperator) {
    case operator_bp:
        mOption.mFilterOpt.mBpW2 = Bw2 / pBase;
        async(MSG_MATH_S32FILTER_BP2, mOption.mFilterOpt.mBpW2);
        break;
    case operator_bt:
        mOption.mFilterOpt.mBtW2 = Bw2 / pBase;
        async(MSG_MATH_S32FILTER_BT2, mOption.mFilterOpt.mBtW2);
        break;
    default:
        return ERR_INVALID_CONFIG;
        break;
    }
    return ERR_NONE;
}

qlonglong servMath::scpiGetFilterBw2()
{
    qlonglong sa = static_cast<qlonglong>( getHSa(mSrc.mAnaSrc.mSrcA) );
    qlonglong pBase = 1e6*sa/(200.0);
    switch (mOperator) {
    case operator_bp:
        return (mOption.mFilterOpt.mBpW2 * pBase);
        break;
    case operator_bt:
        return (mOption.mFilterOpt.mBtW2 * pBase);
        break;
    default:
        return ERR_INVALID_CONFIG;
        break;
    }
}
