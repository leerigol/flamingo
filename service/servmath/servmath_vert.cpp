#include "servmath.h"
#include "../../service/servhori/servhori.h"

void servMath::getPixel(  DsoWfm &wfm, Chan /*subChan*/, HorizontalView view)
{
    mWfmSema.acquire();
    Q_ASSERT( NULL != m_pWfmMainScr );
    if ( mMathEn )
    {
    }
    else
    {
        mWfmSema.release();
        //! no data
        return wfm.setRange( 0, 0 );
    }
    if(view == horizontal_view_main)
    {
        wfm = *m_pWfmMainScr;
    }
    else
    {
        wfm = *m_pWfmZoomScr;
    }

    qlonglong viewScale = getViewScale();
    wfm.setyGnd( dsoVert::getyGnd() );
    if( getOperator() == operator_trend)
    {
        wfm.setyInc( dsoFract( viewScale, adc_vdiv_dots),
                     DsoReal(1,E_N12));
    }
    else
    {
        wfm.setyInc( dsoFract( viewScale, adc_vdiv_dots),
                     DsoReal(1,E_N9) );
    }
    wfm.setInvert( getViewInvert() );
    /////////////////////  add by lidongming  fix Bug 1577  /////////////////
    wfm.setyRefBase(  dsoVert::getYRefBase() );
    wfm.setyRefOffset( dsoVert::getYRefOffset() );
    wfm.setyRefScale( dsoVert::getYRefScale() );
    wfm.setAttrId( servHori::getAttrId() );
    mWfmSema.release();
}

void servMath::getTrace(  DsoWfm &wfm, Chan /*subChan*/, HorizontalView view )
{
    Q_ASSERT( NULL != m_pWfmMainResult );
    if ( mMathEn )
    {
    }
    else
    {
        //! no data
        return wfm.setRange( 0, 0 );
    }

    if( view == horizontal_view_main)
    {
        m_pWfmMainResult->m_wfmMutex.lock();
        wfm = *m_pWfmMainResult;
        m_pWfmMainResult->m_wfmMutex.unlock();
    }
    else
    {
        m_pWfmZoomResult->m_wfmMutex.lock();
        wfm = *m_pWfmZoomResult;
        m_pWfmZoomResult->m_wfmMutex.unlock();
    }
    ////////////////////  add by lidongming  fix bug 1577  ///////////////////
    //! 原使用m_pWfmScr作为Math的Trace输出，m_pWfmScr中的数据最多只有1000点
    //! 与模拟通道的trace数据点数不同，将导致双源测量、UPA等测量出错，
    //! 现改为使用m_pWfmResult作为Math的Trace输出，
    //! m_pWfmResult中保存的是原始运算结果，数据点数与模拟通道相同
    //! 原始运算结果是浮点数，下面的代码转换成adc值
    qlonglong viewScale = getViewScale();

    wfm.setyGnd( dsoVert::getyGnd() );
    if( getOperator() == operator_trend)
    {
        wfm.setyInc( dsoFract( viewScale, adc_vdiv_dots),
                     DsoReal(1,E_N12));
    }
    else
    {
        wfm.setyInc( dsoFract( viewScale, adc_vdiv_dots),
                     DsoReal(1,E_N9) );
    }
    wfm.setInvert( getViewInvert() );
    wfm.toPoint( wfm.start(),
                 wfm.size() );

    /////////////////////  add by lidongming  fix Bug 1577  /////////////////
    wfm.setyRefBase(  dsoVert::getYRefBase() );
    wfm.setyRefOffset( dsoVert::getYRefOffset() );
    wfm.setyRefScale( dsoVert::getYRefScale() );
    wfm.setAttrId( servHori::getAttrId() );
}

void servMath::getMemory( DsoWfm &wfm, Chan subChan )
{
    getTrace( wfm, subChan );
}
