#include <cmath>
#include "mathwork.h"
#include "servmath.h"

#include "../../include/dsocfg.h"

#include "../../baseclass/dsoreal.h"
#include "../../baseclass/resample/reSample.h"

#include "./operator/math_opers.h"

#include "../../engine/base/enginecfg.h"
#include "../../service/servch/servch.h"
#include "../../service/servhori/servhori.h"
#include "../../service/serveyejit/serveyejit.h"

mathWork::mathWork()
{

}

void mathWork::expandLa(DsoWfm &wfm)
{
    DsoPoint* pPoint;
    pPoint = new DsoPoint[ wfm.size() * 8 ];
    for(int i = 0; i < wfm.size();i++)
    {
        for(int j = 0; j < 8;j++)
        {
            pPoint[i*8 + j] = (wfm.at(i) << j) && 0x80;
        }
    }
    LOG_DBG() << wfm.size() << wfm.m_start;
    wfm.setPoint(pPoint, wfm.size()*8, wfm.size() * 8, wfm.m_start * 8);
    LOG_DBG() << wfm.size() << wfm.m_start;
    delete[] pPoint;
}

void mathWork::digitlize( mathWorkContext *pContext,
                          DsoWfm &wfm,
                          Chan aCh )
{
    float fLev;
    //! get level by ch
    switch( aCh )
    {
    //test
    case chan1:
        fLev = pContext->pServMath->mOption.mLaOpt.mThresholds[0] * math_threshold_base;
        break;

    case chan2:
        fLev = pContext->pServMath->getThreshold_CH2() * math_threshold_base;
        break;

    case chan3:
        fLev = pContext->pServMath->getThreshold_CH3() * math_threshold_base;
        break;

    case chan4:
        fLev = pContext->pServMath->mOption.mLaOpt.mThresholds[3] * math_threshold_base;
        break;

        //! digital already
    case d0: case d1: case d2: case d3:
    case d4: case d5: case d6: case d7:
    case d8: case d9: case d10: case d11:
    case d12: case d13: case d14: case d15:
        return;

    default:
        fLev = 0;
        Q_ASSERT( false );
        break;
    }

    //! now convert to digital
    float ratio;
    if ( ERR_NONE != dsoVert::getCH( aCh )->getProbe().toReal( ratio ) )
    { ratio = 1.0f; }
    fLev *= ratio;

    float scale=0.0;
    if(aCh >= chan1 &&aCh <= chan4)
    {
        QString name = QString("chan%1").arg(aCh);
        serviceExecutor::query(name,servCH::cmd_scale_real,scale);
    }

    //! logic
    wfm.toLogic( fLev,
                 pContext->pServMath->getThresholdSens()*math_threshold_sens_base * scale
                 );
}

void mathWork::begin()
{
}

DsoErr mathWork::calc( mathWorkContext *pContext,
                       DsoWfm &wfm ,HorizontalView view)
{
    DsoWfm wfmA, wfmB;
    dsoVert *pVertA, *pVertB;

    servMath *pServMath;

    pServMath = pContext->pServMath;
    Q_ASSERT( NULL != pServMath );

    //! operators
    MathOperArith arith( pServMath->mOperator );
    MathOperLogic logic( pServMath->mOperator );
    MathOperFunc func( pServMath->mOperator );
    MathOperFft fft( pServMath->mOperator );

    //! context
    opIntgContext intgContext;
    opDiffContext diffContext;
    opAxbContext axbContext;
    opFftContext fftContext;
    opFilterContext filterContext;

    DsoErr err = ERR_NONE;

    switch( pServMath->mOperator )
    {
    case operator_add:
    case operator_sub:
    case operator_mul:
    case operator_div:
        //! get data
        pVertA = dsoVert::getCH( pServMath->mSrc.mAnaSrc.mSrcA );
        pVertB = dsoVert::getCH( pServMath->mSrc.mAnaSrc.mSrcB );

        pVertA->getTrace( wfmA, chan_none, view);
        pVertB->getTrace( wfmB, chan_none, view);

        if(pVertA->getZone() != pVertB->getZone())
        {
            wfm.setRange(1000, 0);
            return math_err_warning;
        }

        if(pServMath->mSrc.mAnaSrc.mSrcA == pServMath->mSrc.mAnaSrc.mSrcB)
        {
            err = arith.calc( wfmA, wfmA, &wfm, NULL );
        }
        else
        {
            err = arith.calc( wfmA, wfmB, &wfm, NULL );
        }
        if(err != 0) {return err;}
        break;

    case operator_and:
    case operator_or:
    case operator_xor:

        //! get data
        if( pServMath->mSrc.mLaSrc.mSrcA >= chan1 &&
                pServMath->mSrc.mLaSrc.mSrcA <= chan4 )
        {
            pVertA = dsoVert::getCH( pServMath->mSrc.mLaSrc.mSrcA );
            pVertA->getTrace( wfmA, chan_none, view);
            //! to digital
            digitlize( pContext,
                       wfmA,
                       pServMath->mSrc.mLaSrc.mSrcA );
#ifdef _DIGI_DEBUG
            pVertA = dsoVert::getCH( pServMath->mSrc.mLaSrc.mSrcA );
            Q_ASSERT(pVertA);
            pVertA->getDigi(wfmA);
            wfmA.expandLa();
            LOG_DBG();
#endif
        }
        else
        {
            pVertA = dsoVert::getCH( la );
            pVertA->getDigi( wfmA , pServMath->mSrc.mLaSrc.mSrcA, view);
            wfmA.expandLa();
        }

        if( pServMath->mSrc.mLaSrc.mSrcB >= chan1 &&
                pServMath->mSrc.mLaSrc.mSrcB <= chan4 )
        {
            pVertB = dsoVert::getCH( pServMath->mSrc.mLaSrc.mSrcB );
            pVertB->getTrace( wfmB, chan_none, view);
            digitlize( pContext,
                       wfmB,
                       pServMath->mSrc.mLaSrc.mSrcB );
#ifdef _DIGI_DEBUG
            pVertB = dsoVert::getCH( pServMath->mSrc.mLaSrc.mSrcB );
            Q_ASSERT(pVertB);
            pVertB->getDigi(wfmB);
            wfmB.expandLa();
            LOG_DBG();
#endif
        }
        else
        {
            pVertB = dsoVert::getCH( la );
            LOG_DBG();
            pVertB->getDigi( wfmB , pServMath->mSrc.mLaSrc.mSrcB, view);
            wfmB.expandLa();
        }

        //BUG 961 by zy
        if(pServMath->mSrc.mLaSrc.mSrcA == pServMath->mSrc.mLaSrc.mSrcB)
        {
            err = logic.calc( wfmA, wfmA, &wfm, NULL );
        }
        else
        {
            err = logic.calc( wfmA, wfmB, &wfm, NULL );
        }
        if(err != ERR_NONE) { return err;}
        //! to value
        wfm.setyGnd( 0 );
        wfm.setyInc( 1e6 );
        wfm.toValue();
        break;

    case operator_not:

        //! get data
        if( pServMath->mSrc.mLaSrc.mSrcA >= chan1 &&
                pServMath->mSrc.mLaSrc.mSrcA <= chan4 )
        {
            pVertA = dsoVert::getCH( pServMath->mSrc.mLaSrc.mSrcA );
            pVertA->getTrace( wfmA, chan_none, view);
            //! to digital
            digitlize( pContext,
                       wfmA,
                       pServMath->mSrc.mLaSrc.mSrcA );
        }
        else
        {
            pVertA = dsoVert::getCH( la );
            Q_ASSERT(pVertA);
            pVertA->getDigi( wfmA, pServMath->mSrc.mLaSrc.mSrcA, view);
            wfmA.expandLa();
        }

        err = logic.calc(wfmA, &wfm, NULL);
        if(err != 0) {return err;}
        //! to value
        wfm.setyGnd( 0 );
        wfm.setyInc( 1e6 );
        wfm.toValue();
        break;

        //! function
    case operator_abs:
    case operator_exp:
    case operator_lg:
    case operator_ln:
    case operator_root:
        //! get data
        pVertA = dsoVert::getCH( pServMath->mSrc.mAnaSrc.mSrcA );

        pVertA->getTrace( wfmA, chan_none, view);

        err = func.calc( wfmA, &wfm, NULL );
        if(err != 0) {return err;}
        break;

    case operator_lp:
    case operator_hp:
    case operator_bp:
    case operator_bt:
        //! get data
        pVertA = dsoVert::getCH( pServMath->mSrc.mAnaSrc.mSrcA );

        pVertA->getTrace( wfmA, chan_none, view);
        LOG_DBG()<<wfmA.size()<<wfmA.getlltInc();
        switch (pServMath->mOperator) {
        case operator_lp:
            filterContext.filter = filter_lp;
            filterContext.leftFreq = pServMath->mOption.mFilterOpt.mLpW;
            filterContext.rightFreq = -1;
            break;
        case operator_hp:
            filterContext.filter = filter_hp;
            filterContext.leftFreq = pServMath->mOption.mFilterOpt.mHpW;
            filterContext.rightFreq = -1;
            break;
        case operator_bp:
            filterContext.filter = filter_bp;
            filterContext.leftFreq = pServMath->mOption.mFilterOpt.mBpW1;
            filterContext.rightFreq = pServMath->mOption.mFilterOpt.mBpW2;
            break;
        case operator_bt:
            filterContext.filter = filter_bt;
            filterContext.leftFreq = pServMath->mOption.mFilterOpt.mBtW1;
            filterContext.rightFreq = pServMath->mOption.mFilterOpt.mBtW2;
            break;
        default:
            break;
        }

        err = func.calc( wfmA, &wfm, &filterContext);
        if(err != 0) {return err;}
        break;

        //! intg diff
    case operator_intg:
        //! get data
        pVertA = dsoVert::getCH( pServMath->mSrc.mAnaSrc.mSrcA );
        pVertA->getTrace( wfmA, chan_none, view);

        //! build context
        pServMath->mOption.mIntgOpt.mBias.toReal( intgContext.bias );

        intgContext.tInc = wfmA.gettInc().toDouble();

        func.calc( wfmA, &wfm, &intgContext );
        break;

    case operator_diff:
        //! get data
        pVertA = dsoVert::getCH( pServMath->mSrc.mAnaSrc.mSrcA );
        pVertA->getTrace( wfmA, chan_none, view);

        //! build context
        diffContext.window = pServMath->mOption.mDiffOpt.mWindowSize;
        diffContext.tInc = wfmA.gettInc().toDouble();
        func.calc( wfmA, &wfm, &diffContext );
        break;

    case operator_ax_b:
        //! get data
        pVertA = dsoVert::getCH( pServMath->mSrc.mAnaSrc.mSrcA );
        pVertA->getTrace( wfmA, chan_none, view);

        pServMath->mOption.mLineOpt.a.toReal( axbContext.a );
        pServMath->mOption.mLineOpt.b.toReal( axbContext.b );
        func.calc( wfmA, &wfm, &axbContext );
        break;

    case operator_fft:
    {
        //! get data
        pVertA = dsoVert::getCH( pServMath->mSrc.mFftSrc.mSrc );
        pVertA->getTrace( wfmA);

        if(wfmA.size() == 0)
        {
            wfm.setRange(0,0);
            return -1;
        }

        //! fft size
        int power = 0;
        int size = wfmA.size()/1024;
        while( size >= 1 )
        {
            size = size/2;
            power++;
        }
        fftContext.fftSize = 1024 * pow(2,power);

        fftContext.specUnit = pServMath->mOption.mFftOpt.myUnit;
        if(pServMath->getUnit() == Unit_dbm)
        {
            fftContext.specUnit = fft_spec_dbm;
        }

        fftContext.window = pServMath->mOption.mFftOpt.mWindow;

        fft.calc( wfmA, &wfm, &fftContext );
        wfm.setAttrId( wfmA.getAttrId() );
        LOG_DBG()<<wfm.size()<<wfm.getlltInc();
        if(wfm.size() > 0 && pServMath->mPeakCfg.m_peakEnable == true)
        {
            pServMath->mPeakCfg.peakAction(wfm, pServMath->mCfg.mFreqHCfg.mStart.mVal,
                        pServMath->mCfg.mFreqHCfg.mEnd.mVal );
            if( wfm.getAttrId() == servHori::getAttrId() )
            {
                serviceExecutor::post(pServMath->getName(),servMath::cmd_q_fft_peak,0);
            }
        }

        if(pServMath->mCfg.mFreqHCfg.mSaRate.mVal != 1e18/wfmA.getlltInc())
        {
            pServMath->mCfg.mFreqHCfg.mSaRate.mVal = 1e18/wfmA.getlltInc();
            if(wfmA.size() != fftContext.fftSize)
            {
                float mul = 1.0*wfmA.size() / fftContext.fftSize;
                pServMath->mCfg.mFreqHCfg.mSaRate.mVal =
                        round(pServMath->mCfg.mFreqHCfg.mSaRate.mVal/(mul));
            }
            pServMath->mCfg.mFreqHCfg.mSize = fftContext.fftSize;
            //! update resolution
            serviceExecutor::post(pServMath->getName(),servMath::cmd_q_real_hresolution,0);
        }
    }
        break;
    case operator_trend:
    {
        //! 查询计算得出的抖动曲线
        void* ptr;
        serviceExecutor::query(QString(serv_name_eyejit),servEyeJit::cmd_jitter_jitterwfm,ptr);

        DsoWfm* pWfm = static_cast<DsoWfm*> (ptr);
        wfm = *pWfm;
    }
        break;
    default:
        break;
    }
    wfm.setAttrId( wfmA.getAttrId() );

    return ERR_NONE;
}

void mathWork::end()
{
}

DsoErr mathWork::hScaleProc( mathWorkContext *pContext,
                             DsoWfm &wfmIn,
                             DsoWfm &wfmOut,
                             HorizontalView view)
{
    Q_ASSERT( NULL != pContext );
    MathOperator oper = pContext->pServMath->getOperator();

    if ( operator_fft == oper )
    {
        return hScaleProc_fft( pContext, wfmIn, wfmOut, view);
    }
    else
    {
        return hScaleProc_vector( pContext, wfmIn, wfmOut, view);
    }
}

//! fft
DsoErr mathWork::hScaleProc_fft( mathWorkContext *pContext,
                                 DsoWfm &wfmIn,
                                 DsoWfm &wfmOut,
                                 HorizontalView hview)
{
    Q_ASSERT( NULL != pContext );

    //! fft sample
    struMem mem;
    struView view;
    HoriCfg *pHoriCfg;
    DsoErr err = ERR_NONE;

    pHoriCfg = &pContext->pServMath->mCfg.mFreqHCfg;

    //! view
    if(hview == horizontal_view_main)
    {
        //! memory
        mem.sett( 0, wfmIn.getlltInc() );
        LOG_DBG()<<wfmIn.size()<<wfmIn.getllt0();
        mem.setLength( wfmIn.size() );

        LOG_DBG();
        view.sett( pHoriCfg->mStart.mVal,
                   pHoriCfg->mSpan.mVal/1000);
        LOG_DBG() << "pHoriCfg->mStart.mVal = " << pHoriCfg->mStart.mVal << "pHoriCfg->mSpan.mVal = " << pHoriCfg->mSpan.mVal;
    }
    else
    {
        LOG_DBG();
        horiAttr hMain;
        hMain = getHoriAttr(chan1, horizontal_view_main );
        horiAttr hZoom;
        hZoom = getHoriAttr(chan1, horizontal_view_zoom );

        LOG_DBG()<<hMain.tInc<<hMain.gnd<<hZoom.tInc<<hZoom.gnd;
        qlonglong hMainSpan = hMain.tInc * wave_width;
        qlonglong hZoomOffset = hMain.tInc * hMain.gnd - hZoom.tInc * hZoom.gnd ;
        //! 数字太大，相乘会溢出，转为double
        qlonglong vStart = pHoriCfg->mStart.mVal + hZoomOffset *  (double) pHoriCfg->mSpan.mVal / hMainSpan;
        LOG_DBG()<<hZoom.tInc<<pHoriCfg->mSpan.mVal<<hMain.tInc;
        qlonglong vSpan = hZoom.tInc * (double) pHoriCfg->mSpan.mVal / hMain.tInc ;

        pHoriCfg->mZoomStart.mVal = vStart;
        pHoriCfg->mZoomScale.mVal = vSpan/10;
        //! memory
        mem.sett( 0, wfmIn.getlltInc() );
        LOG_DBG()<<wfmIn.size()<<wfmIn.getllt0();
        mem.setLength(wfmIn.size() );

        LOG_DBG()<<hMainSpan<<hZoomOffset<<vStart<<vSpan;
        view.sett( vStart, vSpan / 1000);
        LOG_DBG();
    }

    //    LOG_DBG()<<"pHoriCfg->mStart.mVal"<<pHoriCfg->mStart.mVal
    //           <<"pHoriCfg->mSpan.mVal"<<pHoriCfg->mSpan.mVal;
    view.setWidth( 1000 );
    //! sample
    wfmOut.init( 1000 );

    err = reSampleExt( &mem,
                 &view,
                 wfmIn.getValue() + wfmIn.start(),
                 wfmOut.getValue(),
                 //bug 927 by zy
                 e_sample_peak);

    wfmOut.sett(view.mvt0,view.mvtInc,DsoReal(1,E_N6) );
    LOG_DBG()<<view.vLeft<<view.vLen;
    wfmOut.setRange( view.vLeft, view.vLen );
    return err;
}

DsoErr mathWork::hScaleProc_fft_Color(mathWorkContext *pContext, DsoWfm &wfmIn,
                                      DsoWfm &wfmOut, HorizontalView /*hview*/)
{
    Q_ASSERT( NULL != pContext );

    //! fft sample
    struMem mem;
    struView view;
    HoriCfg *pHoriCfg;

    pHoriCfg = &pContext->pServMath->mCfg.mFreqHCfg;

    //! memory
    mem.sett( 0, wfmIn.getlltInc() );
    LOG_DBG()<<wfmIn.size()<<wfmIn.getllt0();
    mem.setLength( wfmIn.size() );

    view.sett( pHoriCfg->mStart.mVal,
               wfmIn.getlltInc());
    LOG_DBG() << "pHoriCfg->mStart.mVal = " << pHoriCfg->mStart.mVal << "pHoriCfg->mSpan.mVal = " << pHoriCfg->mSpan.mVal;

    view.setWidth( pHoriCfg->mSpan.mVal / wfmIn.getlltInc() + 1 );
    //! sample
    wfmOut.init( pHoriCfg->mSpan.mVal / wfmIn.getlltInc() + 1 );

    DsoErr err = reSampleExt( &mem,
                 &view,
                 wfmIn.getValue() + wfmIn.start(),
                 wfmOut.getValue(),
                 e_sample_peak);

    wfmOut.sett(view.mvt0,view.mvtInc,DsoReal(1,E_N6) );
    LOG_DBG()<<view.vLeft<<view.vLen;
    wfmOut.setRange( view.vLeft, view.vLen );
    return err;
}

//! -- vector
//! +/-/*/div
DsoErr mathWork::hScaleProc_vector( mathWorkContext *pContext,
                                    DsoWfm &wfmIn,
                                    DsoWfm &wfmOut,
                                    HorizontalView horizontalView)
{
    Q_ASSERT( NULL != pContext );

    DsoErr err = ERR_NONE;

    struMem mem;
    struView view;

    //! mem
    //! 开头有一部分无效数据，改变t0
    mem.sett( wfmIn.getllt0() + wfmIn.start() * wfmIn.getlltInc(), wfmIn.getlltInc() );
    mem.setLength( wfmIn.size() );

    //! view
    horiAttr hAttr;
    if(horizontalView == horizontal_view_main)
    {
        hAttr = sysGetHoriAttr(chan1, horizontal_view_main);
    }
    else
    {
        hAttr = sysGetHoriAttr(chan1, horizontal_view_zoom);
    }

    LOG_DBG()<<hAttr.gnd<<hAttr.tInc;

    view.sett( - hAttr.gnd * hAttr.tInc, hAttr.tInc );
    view.setWidth( wave_width );

    //! info
    LOG_DBG()<<mem.mt0<<mem.mtInc<<mem.mLen;
    LOG_DBG()<<view.mvt0<<view.mvtInc<<view.mWidth;

    //! cap
    wfmOut.init( view.mWidth );

    //! 当size为0时，如对一个偏移为负数的直流信号开根号
    //! 也需要刷新一下界面
    if(wfmIn.size() == 0)
    {
        wfmOut.setRange( 1000, 0 );
    }

    //! resample
    int ret = reSampleExt( &mem,
                           &view,
                           wfmIn.getValue() + wfmIn.start(),
                           wfmOut.getValue(),
                           e_sample_peak|e_sample_head|e_sample_tail);

    wfmOut.sett( view.mvt0, view.mvtInc, DsoReal(1,E_N12) );
    if ( ret != 0 )
    {
        LOG_DBG() << "view" << view.mvt0 << view.mvtInc;
        LOG_DBG() << "mem" << mem.mt0 << mem.mtInc;
        err = ERR_MATH_RESAMPLE_FAIL;
    }
    else
    {
        wfmOut.setRange( view.vLeft, view.vLen );
    }
    return err;
}

DsoErr mathWork::vScaleProc( mathWorkContext *pContext, DsoWfm &wfm )
{
    qlonglong viewScale;
    qlonglong viewOffset;


    pContext->pServMath->query( pContext->pServMath->mpItem->servName,
                                servMath::cmd_q_adc_offset,
                                viewOffset );

    pContext->pServMath->query( pContext->pServMath->mpItem->servName,
                                servMath::cmd_q_view_scale,
                                viewScale );

    bool bInv = pContext->pServMath->getViewInvert();

    //! convert to point
    wfm.setyGnd( viewOffset + adc_center );
    if(pContext->pServMath->getOperator() == operator_trend)
    {
        wfm.setyInc( dsoFract( viewScale, adc_vdiv_dots),
                     DsoReal(1,E_N12) );
    }
    else
    {
        wfm.setyInc( dsoFract( viewScale, adc_vdiv_dots),
                     DsoReal(1,E_N9) );
    }

    wfm.setInvert( bInv );

    //! to adc now
    wfm.toPoint( wfm.start(),
                 wfm.size() );

    return ERR_NONE;
}

int mathWork::process(mathWorkContext *pContext,
                       DsoWfm &wfmScr , HorizontalView view)
{
    begin();

    Q_ASSERT( pContext!=NULL );
    //Q_ASSERT( pContext->provider!=NULL );

    int err = 0;
    DsoWfm wfm;
    err = calc( pContext, wfm ,view);
    LOG_DBG()<<wfm.size()<<wfm.getlltInc();

    if( err == math_err_warning )
    {
        wfmScr.setRange(1000,0);
        return math_err_warning;
    }

    //! 起点和长度都为0的情况下，不更新界面；
    //! 起点为1000，长度为0的情况下，清空界面；
    if(wfm.start() == 0 && wfm.size() == 0)
    {
        wfmScr.setRange(1000,0);
        return math_err_warning;
    }

    reScaleScr( pContext, wfm, wfmScr, view );
    wfmScr.setAttrId(wfm.getAttrId());

    end();
    return err;    
}

int mathWork::testMathColor(mathWorkContext *pContext,
                             DsoWfm &wfmScr , HorizontalView view)
{
    begin();
    Q_ASSERT( pContext!=NULL );

    DsoWfm wfm;
    calc( pContext, wfm ,view);
    LOG_DBG()<<wfm.size()<<wfm.getlltInc();

    //! 起点和长度都为0的情况下，不更新界面；
    //! 起点为1000，长度为0的情况下，清空界面；
    if(wfm.start() == 0 && wfm.size() == 0)
    {
        wfmScr.setRange(0,0);
        return math_err_no_update;
    }

    DsoWfm wfmIn = wfm;
    servMath* pMath = pContext->pServMath;
    if ( operator_fft == pMath->getOperator() )
    {
        hScaleProc_fft_Color( pContext, wfmIn, wfm, view);
    }

    vScaleProc( pContext, wfm );

    pMath->m_pMathGrade->setColorGrade( wfm.getPoint(), wfm.size() );

    end();

    return ERR_NONE;
}

void mathWork::reScaleScr(mathWorkContext *pContext, DsoWfm &wfm, DsoWfm &wfmScr, HorizontalView view)
{
    DsoWfm wfmOut;

    if( view == horizontal_view_main)
    {
        pContext->pServMath->m_pWfmMainResult->m_wfmMutex.lock();
        *(pContext->pServMath->m_pWfmMainResult) = wfm;
        pContext->pServMath->m_pWfmMainResult->m_wfmMutex.unlock();
    }
    else
    {
        pContext->pServMath->m_pWfmZoomResult->m_wfmMutex.lock();
        *(pContext->pServMath->m_pWfmZoomResult) = wfm;
        pContext->pServMath->m_pWfmZoomResult->m_wfmMutex.unlock();
    }

    hScaleProc( pContext, wfm, wfmOut, view);
    LOG_DBG()<<"in(s,l)/out(s,l)"<<wfm.start()<<wfm.size()<<wfmOut.start()<<wfmOut.size();

    vScaleProc( pContext, wfmOut );
    LOG_DBG()<<wfmOut.start()<<wfmOut.size();

#if 0
    static int i ;
    QString name1 = QString("/mnt/in%1").arg(i++);
    QString name2 = QString("/mnt/out%1").arg(i++);
    //wfm.save(name);
    wfmIn.saveValue(name1);
    wfmOut.saveValue(name2);
#endif


#ifdef _DEBUG
    if(view== horizontal_view_zoom)
    {
        QString strOrigin("/rigol/traceMath/mathZoomOut");
        wfmOut.save(strOrigin);
        wfmOut.saveValue(strOrigin);
    }
#endif

    wfmScr = wfmOut;
}

