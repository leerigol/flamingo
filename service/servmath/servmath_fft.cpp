#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "../../arith/gcd.h"
#include "../../arith/scalestep.h"
#include "servmath.h"
#include "../servstorage/servstorage.h"

#define MAX_FFT_HSCALE  (500 * math_Mhz)
#define MIN_FFT_HSCALE  (1*math_hz)

#define MAX_FFT_SPAN    (10*MAX_FFT_HSCALE)
#define MIN_FFT_SPAN    (10*MIN_FFT_HSCALE)

#define MAX_FFT_END     (5*MAX_FFT_HSCALE)
#define MIN_FFT_START   (-5*MAX_FFT_HSCALE)

#define FFT_REAL_BASE   1,E_N6

DsoErr servMath::setFftScale( qlonglong scale )
{
    DsoErr err = ERR_NONE;
    if ( isDbSpec(mOption.mFftOpt.myUnit ) )
    {
        err = mCfg.mFreqCfgDb.mScale.checkRange( scale );
    }
    else
    {
        err = mCfg.mFreqCfgVrms.mScale.checkRange( scale );
    }

    Q_ASSERT( scale > 0 );
    //! offset setting
    if ( getVertExpand() == vert_expand_gnd )
    {
        setuiChange( MSG_MATH_FFT_OFFSET );
    }
    else
    {
        qlonglong viewOffset;

        //! view then
        viewOffset = getFftScale() * mViewOffset.mVal / scale;

        async( MSG_MATH_FFT_OFFSET, viewOffset );
    }

    //! to db or rms
    if ( isDbSpec( mOption.mFftOpt.myUnit ) )
    {
        mCfg.mFreqCfgDb.mScale.mVal = scale;
    }
    else
    {
        mCfg.mFreqCfgVrms.mScale.mVal = scale;
    }
    dsoVert::setyRefScale(scale);
    dsoVert::setyInc(scale * 1e-9 / adc_vdiv_dots);
    releaseDataSema();
    return err;
}
qlonglong servMath::getFftScale()
{
    setuiBase( 1,E_N9 );
    setuiAcc( key_acc::e_key_one_125 );

    QString unitStr;
    Unit unit = getUnit();
    setuiUnit(unit);

    if(mVertFineOn)
    {
        mUiAttr.setAcc( MSG_MATH_FFT_SCALE, key_acc::e_key_raw );
        if ( isDbSpec ( mOption.mFftOpt.myUnit ) )
        {
            mUiAttr.setIStep( MSG_MATH_FFT_SCALE, scaleToStep(mCfg.mFreqCfgDb.mScale.mZVal,1) );
            mUiAttr.setDStep(MSG_MATH_FFT_SCALE, scaleToStep(mCfg.mFreqCfgDb.mScale.mZVal,-1) );
        }
        else
        {
            mUiAttr.setIStep( MSG_MATH_FFT_SCALE, scaleToStep(mCfg.mFreqCfgVrms.mScale.mZVal,1) );
            mUiAttr.setDStep(MSG_MATH_FFT_SCALE, scaleToStep(mCfg.mFreqCfgVrms.mScale.mZVal,-1) );
        }
    }
    else
    {
        mUiAttr.setDStep( MSG_MATH_FFT_SCALE, -1);
        mUiAttr.setIStep( MSG_MATH_FFT_SCALE, 1);
        mUiAttr.setAcc( MSG_MATH_FFT_SCALE, key_acc::e_key_one_125 );
    }

    unitStr = gui_fmt::CGuiFormatter::toString( unit );
    setuiPostStr( unitStr );

    setScaleRange();
    if ( isDbSpec ( mOption.mFftOpt.myUnit ) )
    {
        mUiAttr.setMaxVal( MSG_MATH_FFT_SCALE, mCfg.mFreqCfgDb.mScale.mMax );
        mUiAttr.setMinVal( MSG_MATH_FFT_SCALE, mCfg.mFreqCfgDb.mScale.mMin );
        mUiAttr.setZVal( MSG_MATH_FFT_SCALE, mCfg.mFreqCfgDb.mScale.mZVal );
        return mCfg.mFreqCfgDb.mScale.mVal;
    }
    else
    {
        mUiAttr.setMaxVal( MSG_MATH_FFT_SCALE, mCfg.mFreqCfgVrms.mScale.mMax );
        mUiAttr.setMinVal( MSG_MATH_FFT_SCALE, mCfg.mFreqCfgVrms.mScale.mMin );
        mUiAttr.setZVal( MSG_MATH_FFT_SCALE, mCfg.mFreqCfgVrms.mScale.mZVal );
        return mCfg.mFreqCfgVrms.mScale.mVal;
    }
}

DsoErr servMath::setFftOffset(qlonglong offset)
{
    mViewOffset.mVal = offset;

    releaseDataSema();

    dsoVert::setyRefOffset( offset );
    dsoVert::setyGnd( adc_center + offset * adc_vdiv_dots / getYRefScale() );
    return ERR_NONE;
}

qlonglong servMath::getFftOffset()
{
    setuiZVal( 0ll );

    qlonglong scale;
    if ( isDbSpec ( mOption.mFftOpt.myUnit ) )
    {
        scale = mCfg.mFreqCfgDb.mScale.mVal;
    }
    else
    {
        scale = mCfg.mFreqCfgVrms.mScale.mVal;
    }

    setuiBase( 1,E_N9 );
    setuiStep( scale / adc_vdiv_dots );
    //! 由qint64限制:范围在-10^18 -- +10^18
    mUiAttr.setMaxVal(MSG_MATH_FFT_OFFSET, math_Gv );
    mUiAttr.setMinVal(MSG_MATH_FFT_OFFSET, -math_Gv );

    QString unitStr;
    Unit unit = getUnit();
    setuiUnit(unit);

    unitStr = gui_fmt::CGuiFormatter::toString( unit );
    setuiPostStr( unitStr );

    return mViewOffset.mVal;
}

//! \todo step
DsoErr servMath::setFftHStart( qlonglong fStart )
{
    DsoErr err;
    if ( fStart > mCfg.mFreqHCfg.mEnd.mVal - MIN_FFT_SPAN )
    {
        mCfg.mFreqHCfg.mStart.mVal = mCfg.mFreqHCfg.mEnd.mVal - MIN_FFT_SPAN;
        err = ERR_OVER_UPPER_RANGE;
    }
    else if ( fStart < MIN_FFT_START )
    {
        mCfg.mFreqHCfg.mStart.mVal = MIN_FFT_START;
        err = ERR_OVER_LOW_RANGE;
    }
    else
    {
        mCfg.mFreqHCfg.mStart.mVal = fStart;
        err = ERR_NONE;
    }

    mOption.mFftOpt.mFFtAutoScale = false;
    updateFftSpanCenter();
    releaseDataSema();
    getFftHEnd();
//    mUiAttr.setMaxVal( MSG_MATH_FFT_H_END, MAX_FFT_END );
//    mUiAttr.setMinVal( MSG_MATH_FFT_H_END, mCfg.mFreqHCfg.mStart.mVal);
    return err;
}
qlonglong servMath::getFftHStart()
{
    setuiBase( 1, E_N6 );
    setuiZVal(freq_Hz(1));
    setuiUnit( Unit_hz );
    setuiStep( getHStartEndStep() );
    setuiIStep( getNumStep(mCfg.mFreqHCfg.mStart.mVal, INC_STEP) );
    setuiDStep( getNumStep(mCfg.mFreqHCfg.mStart.mVal, DEC_STEP) );

    qlonglong step = getFftFreqStep(mCfg.mFreqHCfg.mEnd.mVal);
    mUiAttr.setMaxVal( MSG_MATH_FFT_H_START,  mCfg.mFreqHCfg.mEnd.mVal - step  );
    mUiAttr.setMinVal( MSG_MATH_FFT_H_START,  MIN_FFT_START);

    return mCfg.mFreqHCfg.mStart.mVal;
}

DsoErr servMath::setFftHEnd( qlonglong fEnd )
{
    DsoErr err;
    if ( fEnd > MAX_FFT_END )
    {
        mCfg.mFreqHCfg.mEnd.mVal = MAX_FFT_END;
        err = ERR_OVER_UPPER_RANGE;
    }
    else if (  fEnd <  mCfg.mFreqHCfg.mStart.mVal + MIN_FFT_SPAN )
    {
        mCfg.mFreqHCfg.mEnd.mVal = mCfg.mFreqHCfg.mStart.mVal + MIN_FFT_SPAN;
        err = ERR_OVER_LOW_RANGE;
    }
    else
    {
        mCfg.mFreqHCfg.mEnd.mVal = fEnd;
        err = ERR_NONE;
    }

    mOption.mFftOpt.mFFtAutoScale = false;
    updateFftSpanCenter();
    releaseDataSema();
    getFftHStart();
//    mUiAttr.setMaxVal( MSG_MATH_FFT_H_START,  mCfg.mFreqHCfg.mEnd.mVal - MIN_FFT_SPAN );
//    mUiAttr.setMinVal( MSG_MATH_FFT_H_START,  MIN_FFT_START);
    return err;
}

qlonglong servMath::getFftHEnd()
{
    setuiBase( 1, E_N6 );
    setuiZVal( MAX_FFT_SPAN );
    setuiUnit( Unit_hz );
    setuiIStep( getNumStep(mCfg.mFreqHCfg.mEnd.mVal, INC_STEP) );
    setuiDStep( getNumStep(mCfg.mFreqHCfg.mEnd.mVal, DEC_STEP) );

    mUiAttr.setMaxVal( MSG_MATH_FFT_H_END, MAX_FFT_END );

    qlonglong step = getFftFreqStep(mCfg.mFreqHCfg.mStart.mVal);
    mUiAttr.setMinVal( MSG_MATH_FFT_H_END, mCfg.mFreqHCfg.mStart.mVal+step );

    return mCfg.mFreqHCfg.mEnd.mVal;
}

qlonglong servMath::getFftFreqStep(qlonglong val)
{
    int countBit = 0;
    while(val/10)
    {
        countBit++;
        val/=10;
    }
    qlonglong step = 0;
    if(countBit>4)
    {
        step = qPow(10,countBit-3);
    }
    else
    {
        step = MIN_FFT_SPAN;
    }
    return step;
}
qlonglong servMath::getHStartEndStep()
{
    //! calc step
    //! 缺陷：当start和end比较接近且比较大时，span过小导致step过小
    //! 如：start为2.49G，end为2.5G
    qlonglong span;
    span = mCfg.mFreqHCfg.mEnd.mVal
           - mCfg.mFreqHCfg.mStart.mVal;
    span = ll_floor125( span );
    Q_ASSERT( span >= 1000 );
    return span / 1000;
}

//! max 10g
//! min 10hz
DsoErr servMath::setFftSpan( qlonglong span )
{
    DsoErr err = ERR_NONE;

    mCfg.mFreqHCfg.mSpan.mMax = MAX_FFT_SPAN;
    mCfg.mFreqHCfg.mSpan.mMin = MIN_FFT_SPAN;
    mCfg.mFreqHCfg.mSpan.checkRange(span);

    updateFftStartEnd();
    mOption.mFftOpt.mFFtAutoScale = false;
    releaseDataSema();
    return err;
}

qlonglong servMath::getFftSpan()
{
    setuiBase( 1, E_N6 );
//    setuiAcc( key_acc::e_key_one_125 );
    setuiUnit( Unit_hz );
    setuiIStep( getNumStep(mCfg.mFreqHCfg.mSpan.mVal, INC_STEP) );
    setuiDStep( getNumStep(mCfg.mFreqHCfg.mSpan.mVal, DEC_STEP) );

    mUiAttr.setMaxVal( MSG_MATH_FFT_H_SPAN, MAX_FFT_SPAN );
    mUiAttr.setMinVal( MSG_MATH_FFT_H_SPAN, MIN_FFT_SPAN);
    mUiAttr.setZVal( MSG_MATH_FFT_H_SPAN, math_Mhz);
    return mCfg.mFreqHCfg.mSpan.mVal;
}

DsoErr servMath::setFftCenter( qlonglong center )
{
    mCfg.mFreqHCfg.mCenter.mVal = center;

    updateFftStartEnd();
    mOption.mFftOpt.mFFtAutoScale = false;
    releaseDataSema();
    return ERR_NONE;
}
qlonglong servMath::getFftCenter()
{
    setuiBase( FFT_REAL_BASE );
    setuiUnit( Unit_hz );
    setuiIStep( getNumStep(mCfg.mFreqHCfg.mCenter.mVal, INC_STEP) );
    setuiDStep( getNumStep(mCfg.mFreqHCfg.mCenter.mVal, DEC_STEP) );
    //! max: maxEnd/2 - currentSpan / 2;
    mUiAttr.setMaxVal( MSG_MATH_FFT_H_CENTER, MAX_FFT_END - mCfg.mFreqHCfg.mSpan.mVal / 2);
    mUiAttr.setMinVal( MSG_MATH_FFT_H_CENTER, MIN_FFT_START + mCfg.mFreqHCfg.mSpan.mVal / 2 );
    mUiAttr.setZVal( MSG_MATH_FFT_H_CENTER, 0ll);
    return mCfg.mFreqHCfg.mCenter.mVal;
}

float servMath::getFftHScale()
{
    return (mCfg.mFreqHCfg.mSpan.mVal* math_fft_hz_base) /10;
}

float servMath::getFftHCenter()
{
    return (mCfg.mFreqHCfg.mCenter.mVal * math_fft_hz_base);
}

qlonglong servMath::getFftZoomHstart()
{
    return mCfg.mFreqHCfg.mZoomStart.mVal;
}

qlonglong servMath::getFftZoomHScale()
{
    return mCfg.mFreqHCfg.mZoomScale.mVal;
}

DsoErr servMath::setFftHResolution(int)
{
    return ERR_NONE;
}

float servMath::getFftHResolution()
{
    Q_ASSERT( mCfg.mFreqHCfg.mSize != 0 );

    return (mCfg.mFreqHCfg.mSaRate.mVal * math_fft_hz_base) / mCfg.mFreqHCfg.mSize;
}

bool  servMath::isDbSpec( fftSpecUnit unit )
{
    if ( unit == fft_spec_db ||
         unit == fft_spec_dbm )
    { return true; }
    else
    { return false; }
}

void servMath::updateFftSpanCenter()
{
    mCfg.mFreqHCfg.mSpan.mVal = mCfg.mFreqHCfg.mEnd.mVal - mCfg.mFreqHCfg.mStart.mVal;
    mCfg.mFreqHCfg.mCenter.mVal = ( mCfg.mFreqHCfg.mEnd.mVal + mCfg.mFreqHCfg.mStart.mVal ) / 2;
//    qDebug()<<mCfg.mFreqHCfg.mSpan.mVal;
}

void servMath::updateFftStartEnd()
{
    mCfg.mFreqHCfg.mStart.mVal = mCfg.mFreqHCfg.mCenter.mVal - mCfg.mFreqHCfg.mSpan.mVal / 2;
    mCfg.mFreqHCfg.mEnd.mVal = mCfg.mFreqHCfg.mCenter.mVal + mCfg.mFreqHCfg.mSpan.mVal / 2;
}

DsoErr servMath::setPeakEnable(bool offOn)
{
    mPeakCfg.m_peakEnable = offOn;
    releaseDataSema();
    return ERR_NONE;
}

bool servMath::getPeakEnable()
{
    return mPeakCfg.m_peakEnable;
}

DsoErr servMath::setPeakMaxPoints(int points)
{
    DsoErr err;
    qlonglong num = points;
    err = mPeakCfg.m_peakMaxCount.checkRange(num);
    releaseDataSema();
    return err;
}

int servMath::getPeakMaxPoints()
{
     setuiZVal(5);
     setuiMinVal(1);
     setuiMaxVal(15);
//    mUiAttr.setMaxVal(MSG_MATH_FFT_PEAK_MAXPEAKS, 50);
//    mUiAttr.setMinVal(MSG_MATH_FFT_PEAK_MAXPEAKS, 0);
    return mPeakCfg.m_peakMaxCount.mVal;
}

DsoErr servMath::setPeakThreshold(qlonglong threshold)
{
    mPeakCfg.m_thresHold = threshold;
    releaseDataSema();
    return ERR_NONE;
}

qlonglong servMath::getPeakThreshold()
{
    qlonglong scale;
    if ( isDbSpec ( mOption.mFftOpt.myUnit ) )
    {
        scale = mCfg.mFreqCfgDb.mScale.mVal;
        setuiUnit(Unit_db);
    }
    else
    {
        scale = mCfg.mFreqCfgVrms.mScale.mVal;
        setuiUnit(Unit_V);
    }
    qlonglong offset = mViewOffset.mVal;

    setuiBase( 1, E_N9);
    setuiStep( scale / adc_vdiv_dots );
    setuiZVal(0);

    mUiAttr.setMaxVal( MSG_MATH_FFT_PEAK_THRESHOLD, scale * 4 - offset );
    mUiAttr.setMinVal( MSG_MATH_FFT_PEAK_THRESHOLD,  -scale * 4 - offset);
    return mPeakCfg.m_thresHold;
}

DsoErr servMath::setPeakExcursion(qlonglong excursion)
{
    mPeakCfg.m_excursion = excursion;
    releaseDataSema();
    return ERR_NONE;
}

qlonglong servMath::getPeakExcursion()
{
    qlonglong scale;
    if ( isDbSpec ( mOption.mFftOpt.myUnit ) )
    {
        scale = mCfg.mFreqCfgDb.mScale.mVal;
        setuiUnit(Unit_db);
    }
    else
    {
        scale = mCfg.mFreqCfgVrms.mScale.mVal;
        setuiUnit(Unit_V);
    }
    setuiBase( 1, E_N9);
    setuiStep( scale / adc_vdiv_dots );

    mUiAttr.setMaxVal(MSG_MATH_FFT_PEAK_EXCURSION, scale * vert_div);
    mUiAttr.setMinVal(MSG_MATH_FFT_PEAK_EXCURSION, 0);
    return mPeakCfg.m_excursion;
}

void *servMath::getPeak()
{
    return &mPeakCfg.m_peakBuffer;
}

DsoErr servMath::setPeakOrder(enPeakOrder order)
{
    mPeakCfg.peakOrder = order;
    mPeakCfg.changeOrder();
    async(servMath::cmd_q_fft_peak,0);
    return ERR_NONE;
}

enPeakOrder servMath::getPeakOrder()
{
    return mPeakCfg.peakOrder;
}

void servMath::setPeak()
{
    return;
}

DsoErr servMath::exportEventData()
{
    mServName = this->getName();

    //export event
    CIntent intent;
    intent.setIntent( MSG_STORAGE_SUB_SAVE,
                      servStorage::FUNC_SAVE_FFT,
                      1,
                      getId());

    startIntent( serv_name_storage, intent );
    return  ERR_NONE;
}

DsoErr servMath::saveTable(QString fileName)
{
    QVector<QString> m_freqRes;
    QVector<QString> m_ampRes;

    m_freqRes.reserve(20);
    m_ampRes.reserve(20);
    if(!m_freqRes.isEmpty())
    {
        m_freqRes.clear();
        m_ampRes.clear();
    }

    void* p = NULL;
    serviceExecutor::query(mServName,servMath::cmd_q_fft_peak, p);
    QVector<measPeakStru>* stPeak = static_cast <QVector<measPeakStru>* > (p);

    qlonglong freqStart = 0;
    qlonglong freqEnd = 0;
    float ampScale = 0;
    float ampOffset = 0;
    serviceExecutor::query(mServName,MSG_MATH_FFT_H_START, freqStart);
    serviceExecutor::query(mServName,MSG_MATH_FFT_H_END, freqEnd);
    serviceExecutor::query(mServName, servMath::cmd_q_real_scale, ampScale);
    serviceExecutor::query(mServName,servMath::cmd_q_real_offset, ampOffset);

    int xPoint;
    QString freq;
    QString ampl;
    for(int i = 0;i < stPeak->size();i++)
    {
        xPoint = (stPeak->at(i).freq - freqStart)/(freqEnd - freqStart)*1000;

        if(xPoint < 1000 && xPoint > 0)
        {
            gui_fmt::CGuiFormatter::format(freq,stPeak->at(i).freq*math_fft_hz_base,fmt_def,Unit_hz);
            int unit;
            serviceExecutor::query(mServName, servMath::cmd_math_unit, unit );
            gui_fmt::CGuiFormatter::format(ampl,stPeak->at(i).ampl,fmt_def, (Unit)unit);
            m_freqRes.push_back(freq);
            m_ampRes.push_back(ampl);
        }
    }

    QFile file(fileName);
    if( !file.open(QIODevice::WriteOnly | QIODevice::Text) )
    {
        return ERR_FILE_SAVE_FAIL;
    }
    QTextStream out(&file);
    out<<QString(mServName+", Freq, Amp,,\n"); //! Header
    for(int i = 0; i < m_freqRes.count(); i++)
    {
        out<<QString("%1,%2,%3,,\n").arg(i+1).arg(m_freqRes.at(i)).arg(m_ampRes.at(i));
    }
    file.flush();
    fsync(file.handle());
    file.close();

    return ERR_NONE;
}
