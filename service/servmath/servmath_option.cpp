#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "../../service/servplot/servplot.h"
#include "servmath.h"

/*!
 * \brief servMath::setThreshold_CH1
 * \param thre
 * \return
 */
DsoErr servMath::setThreshold_CH1(qlonglong thre )
{
    //! \todo check range
    mOption.mLaOpt.mThresholds[0] = thre;
    defer(cmd_ch1_threshold_change);
    releaseDataSema();
    return ERR_NONE;
}

//! 因为这个函数会在线程中调用，所以mUiAttr中的设置属性用消息设置
qlonglong servMath::getThreshold_CH1()
{
    setuiZVal( 0 );

    float fRatio;
    dsoVert * pVert1 = dsoVert::getCH( chan1 );
    pVert1->getProbe().toReal( fRatio, 1.0f );
    mUiAttr.setBase( MSG_MATH_S32LOGIC1THRE, math_threshold_base * fRatio );

    qlonglong chScale = pVert1->getYRefScale();
    qlonglong chOffset = pVert1->getYRefOffset();
    mUiAttr.setStep( MSG_MATH_S32LOGIC1THRE, ll_floor125(chScale/10));
    mUiAttr.setMaxVal( MSG_MATH_S32LOGIC1THRE, - chOffset + chScale * 4 );
    mUiAttr.setMinVal( MSG_MATH_S32LOGIC1THRE, - chOffset - chScale * 4 );

    mUiAttr.setUnit( MSG_MATH_S32LOGIC1THRE , pVert1->getUnit() );
    return mOption.mLaOpt.mThresholds[0];
}

DsoErr servMath::setThreshold_CH2( qlonglong thre )
{
    mOption.mLaOpt.mThresholds[1] = thre;
    defer(cmd_ch2_threshold_change);
    releaseDataSema();
    return ERR_NONE;
}

qlonglong servMath::getThreshold_CH2()
{
    setuiZVal( 0 );

    float fRatio;
    dsoVert * pVert2 = dsoVert::getCH( chan2 );
    pVert2->getProbe().toReal( fRatio, 1.0f );
    mUiAttr.setBase( MSG_MATH_S32LOGIC2THRE, math_threshold_base * fRatio );

    qlonglong chScale = pVert2->getYRefScale();
    qlonglong chOffset = pVert2->getYRefOffset();
    mUiAttr.setStep( MSG_MATH_S32LOGIC2THRE, ll_floor125(chScale/10));
    mUiAttr.setMaxVal( MSG_MATH_S32LOGIC2THRE, - chOffset + chScale * 4 );
    mUiAttr.setMinVal( MSG_MATH_S32LOGIC2THRE, - chOffset - chScale * 4 );
    mUiAttr.setUnit( MSG_MATH_S32LOGIC2THRE, pVert2->getUnit() );

    return mOption.mLaOpt.mThresholds[1];
}

DsoErr servMath::setThreshold_CH3( qlonglong thre )
{
    mOption.mLaOpt.mThresholds[2] = thre;
    defer(cmd_ch3_threshold_change);
    releaseDataSema();
    return ERR_NONE;
}

qlonglong servMath::getThreshold_CH3()
{
    setuiZVal( 0 );

    float fRatio;
    dsoVert * pVert3 = dsoVert::getCH( chan3 );
    pVert3->getProbe().toReal( fRatio, 1.0f );
    mUiAttr.setBase( MSG_MATH_S32LOGIC3THRE, math_threshold_base * fRatio );

    qlonglong chScale = pVert3->getYRefScale();
    qlonglong chOffset = pVert3->getYRefOffset();
    mUiAttr.setStep( MSG_MATH_S32LOGIC3THRE, ll_floor125(chScale/10));
    mUiAttr.setMaxVal( MSG_MATH_S32LOGIC3THRE, - chOffset + chScale * 4 );
    mUiAttr.setMinVal( MSG_MATH_S32LOGIC3THRE, - chOffset - chScale * 4 );
    mUiAttr.setUnit( MSG_MATH_S32LOGIC3THRE , pVert3->getUnit() );

    return mOption.mLaOpt.mThresholds[2];
}

DsoErr servMath::setThreshold_CH4( qlonglong thre )
{
    mOption.mLaOpt.mThresholds[3] = thre;
    defer(cmd_ch4_threshold_change);
    releaseDataSema();
    return ERR_NONE;
}

qlonglong servMath::getThreshold_CH4()
{
    setuiZVal( 0 );

    float fRatio;
    dsoVert * pVert4 = dsoVert::getCH( chan4 );
    pVert4->getProbe().toReal( fRatio, 1.0f );
    mUiAttr.setBase( MSG_MATH_S32LOGIC4THRE, math_threshold_base * fRatio );

    qlonglong chScale = pVert4->getYRefScale();
    qlonglong chOffset = pVert4->getYRefOffset();
    mUiAttr.setStep( MSG_MATH_S32LOGIC4THRE, ll_floor125(chScale/10));
    mUiAttr.setMaxVal( MSG_MATH_S32LOGIC4THRE, - chOffset + chScale * 4 );
    mUiAttr.setMinVal( MSG_MATH_S32LOGIC4THRE, - chOffset - chScale * 4 );
    mUiAttr.setUnit( MSG_MATH_S32LOGIC4THRE , pVert4->getUnit() );

    return mOption.mLaOpt.mThresholds[3];
}

DsoErr servMath::setThresholdSens( int sens )
{
    DsoErr err;

    err = checkRange( sens, 1, 10 );

    mOption.mLaOpt.mSens = sens;
    releaseDataSema();
    return err;
}
//! 0.1,0.2 ....
int servMath::getThresholdSens()
{
    mUiAttr.setBase( MSG_MATH_S32LOGICSENS, math_threshold_sens_base );
    mUiAttr.setRange( MSG_MATH_S32LOGICSENS, 1, 10, 3);
    mUiAttr.setFormat( MSG_MATH_S32LOGICSENS, f_nf( fmt_1f) );
    mUiAttr.setMaxVal( MSG_MATH_S32LOGICSENS, 10);
    mUiAttr.setMinVal( MSG_MATH_S32LOGICSENS, 1);
    mUiAttr.setZVal( MSG_MATH_S32LOGICSENS, 3);
    mUiAttr.setUnit( MSG_MATH_S32LOGICSENS, Unit_Div );
    QString unitStr;
    unitStr = "Div";
    mUiAttr.setPostStr( MSG_MATH_S32LOGICSENS, unitStr );

    return mOption.mLaOpt.mSens;
}

//! Diff
DsoErr servMath::setDiffWindow( int width )
{
    //! to do:the length of window depends on the acquire length;
    DsoErr err;

    err = checkRange( width,
                      math_diff_window_min,
                      math_diff_window_max );

    mOption.mDiffOpt.mWindowSize = width;
    releaseDataSema();
    return err;
}
int servMath::getDiffWindow()
{
    setuiRange( math_diff_window_min,
                math_diff_window_max,
                math_diff_window_min );
    return mOption.mDiffOpt.mWindowSize;
}

//! Intg
DsoErr servMath::setIntgBias( DsoReal bias )
{
    mOption.mIntgOpt.mBias = bias;
    releaseDataSema();
    return ERR_NONE;
}
DsoReal servMath::getIntgBias()
{
    setuiZVal( 0ll, E_0 );

    //! 临时设置的最大最小值
    setuiMaxVal( 1e9, E_0);
    setuiMinVal( -1e9, E_0);

    return mOption.mIntgOpt.mBias;
}

//! fft
DsoErr servMath::setFftDcSuppress( bool b )
{
    mOption.mFftOpt.mDcSuppress = b;
    return ERR_NONE;
}

bool servMath::getFftDcSuppress()
{
    return mOption.mFftOpt.mDcSuppress;
}

DsoErr servMath::setFftWindow( fftWindow wnd )
{
    mOption.mFftOpt.mWindow = wnd;
    releaseDataSema();
    return ERR_NONE;
}

fftWindow servMath::getFftWindow()
{
    return mOption.mFftOpt.mWindow;
}

DsoErr servMath::setyUnit( fftSpecUnit yUnit )
{
    mOption.mFftOpt.myUnit = yUnit;
    mbNeedFftVertScale = true;
    return ERR_NONE;
}
//! 对ui只有两个单位值
fftSpecUnit servMath::getyUnit()
{
    //! ui view db/dbm
    if ( mOption.mFftOpt.myUnit == fft_spec_dbm )
    {
        return fft_spec_db;
    }
    return mOption.mFftOpt.myUnit;
}

//! raw unit
fftSpecUnit servMath::getyUnitRaw()
{
    return mOption.mFftOpt.myUnit;
}

DsoErr servMath::setxType( FftxType xType )
{
    mOption.mFftOpt.mxType = xType;
    return ERR_NONE;
}

FftxType servMath::getxType()
{
    return mOption.mFftOpt.mxType;
}

DsoErr servMath::setScreen( FftScreen screen )
{
    mScreenFFT = screen;
//    if(mScreenFFT == fft_screen_half && mFFTCount > 0)
//    {
//        if()
//        {

//        }
//        mUiAttr.setEnable(MSG_MATH_COLOR_ONOFF, false);
//    }
//    else
//    {
//        mUiAttr.setEnable(MSG_MATH_COLOR_ONOFF, true);
//    }
    return ERR_NONE;
}

FftScreen servMath::getFftScreen()
{
    return mScreenFFT;
}

FftScreen servMath::getRealFftScreen()
{
    if(mScreenFFT == fft_screen_half && mFFTCount > 0)
    {
        return fft_screen_half;
    }
    else
    {
        return fft_screen_full;
    }
}

DsoErr servMath::setScpiScreen(bool screen )
{
    if(screen)
    {
        async(MSG_MATH_S32FFTSCR, fft_screen_half);
    }
    else
    {
        async(MSG_MATH_S32FFTSCR, fft_screen_full);
    }

    return ERR_NONE;
}

bool servMath::getScpiFftScreen()
{
    return !mScreenFFT;
}

//! line opt
DsoErr servMath::setLineA( DsoReal a )
{
    mOption.mLineOpt.a = a;
    releaseDataSema();
    return ERR_NONE;
}
DsoReal servMath::getLineA()
{
    setuiZVal( 0ll, E_0 );
    //! 最大值
    mUiAttr.setMaxVal( MSG_MATH_MATH_LINEOPT_A,  1000);
    mUiAttr.setMinVal( MSG_MATH_MATH_LINEOPT_A,  -1000);
    return mOption.mLineOpt.a;
}

DsoErr servMath::setLineB( DsoReal b )
{
    mOption.mLineOpt.b = b;
    releaseDataSema();
    return ERR_NONE;
}

DsoReal servMath::getLineB()
{
    setuiZVal( 0ll, E_0 );
    mUiAttr.setMaxVal( MSG_MATH_MATH_LINEOPT_B, 1000);
    mUiAttr.setMinVal( MSG_MATH_MATH_LINEOPT_B, -1000);
    return mOption.mLineOpt.b;
}

void servMath::onZoomEn()
{
    mZoomEnBefore = mZoomEn;
    bool en;
    serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, en);
    mZoomEn = en;

    m_bFreezedMath = true;
    mDataSema.tryAcquire();
    m_pWorkThread->disconnectClient(this);
    defer(cmd_unfreezed_math);

    if( mZoomEn )
    {
        if(getOperator() >= operator_lp && getOperator() <= operator_bt)
        {
            setActive();
            async(MSG_MATH_EN, false);
            async(MSG_MATH_S32MATHOPERATOR, operator_add);
        }
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_lp, false);
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_hp, false);
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_bp, false);
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_bt, false);
        if(getColorOnOff())
        {
            id_async(getId(), MSG_MATH_COLOR_ONOFF, false);
        }
        mUiAttr.setEnable(MSG_MATH_COLOR_ONOFF, false);
        mUiAttr.setEnable(MSG_MATH_COLOR_RESET, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_lp, true);
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_hp, true);
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_bp, true);
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_bt, true);
        mUiAttr.setEnable(MSG_MATH_COLOR_ONOFF, true);
        mUiAttr.setEnable(MSG_MATH_COLOR_RESET, true);
    }

    if(mHorMode == Acquire_YT || mHorMode == Acquire_SCAN)
    {
        if( en )
        {
            mUiAttr.setEnable(MSG_MATH_S32FFTSCR,false);
        }
        else
        {
            mUiAttr.setEnable(MSG_MATH_S32FFTSCR,true);
        }
        setuiChange(MSG_MATH_S32FFTSCR);
    }
}

DsoErr servMath::setVertExpand(VertExpand expandType)
{
    mVertExpand = expandType;
    return ERR_NONE;
}

VertExpand servMath::getVertExpand()
{
    return mVertExpand;
}

DsoErr servMath::setColorOnOff(bool onOff)
{
    mbColorOnOff = onOff;
    m_pWorkThread->disconnectClient( this );
    return ERR_NONE;
}

bool servMath::getColorOnOff()
{
    return mbColorOnOff;
}

DsoErr servMath::resetColor()
{
    m_pMathGrade->clearColorGrade();
    serviceExecutor::post( getServId(), servMath::cmd_math_grade_update, 0);
    return ERR_NONE;
}

pointer servMath::getColorCurve()
{
    return m_pMathGrade->getColorGrade();
}
