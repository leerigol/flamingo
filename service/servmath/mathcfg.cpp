#include "mathcfg.h"

RangeCfg::RangeCfg()
{
    mMin = 1;
    mMax = 1000000000000000000ll;
}

DsoErr RangeCfg::checkRange( qlonglong &val )
{
    if ( val < mMin )
    {
        val = mMin;
        return ERR_OVER_LOW_RANGE;
    }
    else if ( val > mMax )
    {
        val = mMax;
        return ERR_OVER_UPPER_RANGE;
    }
    else
    {
        mVal = val;
        return ERR_NONE;
    }
}

MathCfg::MathCfg()
{
}

