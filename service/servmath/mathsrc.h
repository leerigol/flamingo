#ifndef MATHSRC_H
#define MATHSRC_H

#include "../../include/dsotype.h"
using namespace DsoType;

struct AnalogSrc
{
    Chan mSrcA;
    Chan mSrcB;
};

struct LaSrc
{
    Chan mSrcA;
    Chan mSrcB;
};

struct FftSrc
{
    Chan mSrc;
};

struct MathSrc
{
    AnalogSrc mAnaSrc;
    LaSrc mLaSrc;
    FftSrc mFftSrc;
};

#endif // MATHSRC_H
