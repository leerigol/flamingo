#ifndef MATHCFG_H
#define MATHCFG_H

#include <QtCore>
#include "../../include/dsoerr.h"
//! config
#define MATH_VIEW_OFFSET_MAX    10000000ll
#define MATH_VIEW_OFFSET_MIN    -(MATH_VIEW_OFFSET_MAX)

/*!
 * \brief The RangeCfg struct
 * 范围配置
 * - 最大
 * - 最小
 * - 当前
 */
struct RangeCfg
{
    qlonglong mVal;     //! current
    qlonglong mMax;
    qlonglong mMin;
    qlonglong mZVal;

    RangeCfg();
    DsoErr checkRange( qlonglong &val );
};

/*!
 * \brief The VertCfg struct
 * 垂直配置
 */
struct VertCfg
{
    RangeCfg mScale;
//    RangeCfg mOffset;
};

/*!
 * \brief The HoriCfg struct
 * 水平配置
 */
struct HoriCfg
{
    RangeCfg mOffset;
    RangeCfg mScale;

    RangeCfg mStart;
    RangeCfg mEnd;

    RangeCfg mZoomStart;
    RangeCfg mZoomScale;

    RangeCfg mSpan;
    RangeCfg mCenter;

    //! depends var
    RangeCfg mSaRate;
    int mSize;          //! fft dots
};

class MathCfg
{
public:
    MathCfg();

public:
    VertCfg mAnaCfg;        /*!< 模拟垂直配置 */

    VertCfg mFreqCfgDb;     /*!< 频率垂直分贝配置 */
    VertCfg mFreqCfgVrms;   /*!< 频率垂直有效值配置 */
    HoriCfg mFreqHCfg;      /*!< 频率水平配置 */
};

#endif // MATHCFG_H
