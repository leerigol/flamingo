#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "servmath.h"

#define FILTER_ORDER    150
#define H_FILTER        20

void servMath::setuiFilterAttr()
{
    float sa;
    setuiUnit( Unit_hz);

    sa = getHSa(mSrc.mAnaSrc.mSrcA);
    setuiBase( sa/200 );
}

DsoErr servMath::setFilterLp( int lp )
{
    mOption.mFilterOpt.mLpW = lp;
    return ERR_NONE;
}
int servMath::getFilterLp()
{
    setuiRange( 1, H_FILTER, 1 );
    setuiFilterAttr();

    return mOption.mFilterOpt.mLpW;
}

DsoErr servMath::setFilterHp( int hp )
{
    mOption.mFilterOpt.mHpW = hp;
    return ERR_NONE;
}
int servMath::getFilterHp()
{
    setuiRange( 1, H_FILTER, 1 );
    setuiFilterAttr();

    return mOption.mFilterOpt.mHpW;
}

DsoErr servMath::setFilterBp1(int bp1 )
{
    mOption.mFilterOpt.mBpW1 = bp1;

    return ERR_NONE;
}
int servMath::getFilterBp1( )
{
    setuiRange( 1, mOption.mFilterOpt.mBpW2 - 1, 1 );
    setuiFilterAttr();

    return mOption.mFilterOpt.mBpW1;
}

DsoErr servMath::setFilterBp2(int bp2 )
{
    mOption.mFilterOpt.mBpW2 = bp2;

    return ERR_NONE;
}
int servMath::getFilterBp2()
{
    setuiRange( mOption.mFilterOpt.mBpW1 + 1, H_FILTER, 1 );
    setuiFilterAttr();

    return mOption.mFilterOpt.mBpW2;
}

DsoErr servMath::setFilterBt1( int bt1 )
{
    mOption.mFilterOpt.mBtW1 = bt1;
    return ERR_NONE;
}
int servMath::getFilterBt1( )
{
    setuiRange( 1, mOption.mFilterOpt.mBtW2 - 1, 1 );
    setuiFilterAttr();

    return mOption.mFilterOpt.mBtW1;
}

DsoErr servMath::setFilterBt2( int bt2 )
{
    mOption.mFilterOpt.mBtW2 = bt2;

    return ERR_NONE;
}
int servMath::getFilterBt2( )
{
    setuiRange( mOption.mFilterOpt.mBtW1 + 1, H_FILTER, 1 );
    setuiFilterAttr();

    return mOption.mFilterOpt.mBtW2;
}
