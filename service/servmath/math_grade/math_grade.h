#ifndef MATH_GRADE_H
#define MATH_GRADE_H

#include <QVector>
#include <QPoint>
#include <QMutex>

class CMathGrade
{
public:
    CMathGrade();
    void   setColorGrade(unsigned char* point, int size);

    void*  getColorGrade();
    void   clearColorGrade();
    static QMutex m_cacheLock;

private:
    QVector<QVector<QPoint> > m_curveScreen;
    //QVector<QVector<QPoint> > m_cache;
    //QMutex m_curveLock;

    int**  colorGrade;

    void   initTable();
    int    mMapTable[257];
};

#endif // MATH_GRADE_H
