#include "math_grade.h"
#include "../../include/dsodbg.h"
#include "../../include/dsocfg.h"
#include <cmath>
#include <QPoint>
#include <../../baseclass/dsofract.h>

#define curve_num 7

QMutex CMathGrade::m_cacheLock;

CMathGrade::CMathGrade()
{
    colorGrade = new int* [1000];
    for(int i = 0; i < 1000;i++)
    {
        colorGrade[i] = new int [481] ();
    }

    m_curveScreen.resize(curve_num);
    initTable();
}

void CMathGrade::initTable()
{
    for(int i = 0; i < 480;i++)
    {
        //! 28 -- 227
        mMapTable[i*200/480 + 28] = i;
    }
    for(int i = 0;i < 28;i++)
    {
        mMapTable[i] = 0;
    }
    for(int i = 227; i < 257; i++)
    {
        mMapTable[i] = 479;
    }
}

void CMathGrade::setColorGrade(unsigned char *point, int size)
{
    dsoFract multi = dsoFract( size, 1000);
    int M = multi.mM;
    int N = multi.mN;

    m_cacheLock.lock();
    for(int i = 0; i < size - 1; i++)
    {
        for( int j = mMapTable[point[i]]; j < mMapTable[point[i]+1] ; j++)
        {
            colorGrade[i*N/M][j] =  colorGrade[i*N/M][j] + 10;
        }
#if 0
        //! 带有连线信息
        if(point[i] > point[i+1])
        {
            for( int j = mMapTable[point[i+1]+1]; j < mMapTable[point[i]] ; j++)
            {
                colorGrade[i/multi][j] ++ ;
            }
        }
        else
        {
            for( int j = mMapTable[point[i]+1]; j < mMapTable[point[i+1]] ; j++)
            {
                colorGrade[i/multi][j] ++ ;
            }
        }
#endif
    }

    int maxCount = 0;
    int secondMax = 0;
    for(int i = 0;i < wave_width;i++)
    {
        for(int j = 0;j < wave_height;j++)
        {
            if( maxCount < colorGrade[i][j] )
            {
                secondMax = maxCount;
                maxCount = colorGrade[i][j];
            }
        }
    }

    LOG_DBG()<<getDebugTime();

    for(int i = 0;i < curve_num;i++)
    {
        m_curveScreen[i].clear();
    }

    for (int i = 0; i < curve_num;i++)
    {
        m_curveScreen[i].reserve(1000*480/10);
    }

    int Edge[curve_num + 1];
    LOG_DBG()<<"secondMax"<<secondMax;
    Edge[curve_num] = secondMax;
    for (int i = curve_num - 1; i > 0; i--)  //Edge value from big to small
    {
        Edge[i] = round(Edge[i+1]/2);
    }
    Edge[0] = 1;

    LOG_DBG()<<getDebugTime();

    for(int j = 0;j < 1000;j++)
    {
        for(int i = 0;i < 480;i++)
        {
            //! 最大值不作为分布指标，但是画图的时候需要画进去
            if(colorGrade[j][i] > Edge[curve_num])
            {
                //!注意QPointF中i和j的顺序，行值i对应的是纵坐标
                m_curveScreen[curve_num - 1].push_back(QPoint(j,479 - i));
                continue;
            }

            for(int k = 0;k < curve_num;k++)
            {
                if(colorGrade[j][i] > Edge[k]&&colorGrade[j][i] <= Edge[k+1])
                {
                    m_curveScreen[k].push_back(QPoint(j,479 - i));
                    continue;
                }
            }
        }
    }
    m_cacheLock.unlock();
}

void *CMathGrade::getColorGrade()
{
    return &m_curveScreen;
}

void CMathGrade::clearColorGrade()
{
    for(int i = 0; i < 1000;i++)
    {
        memset(colorGrade[i], 0, 481*4);
    }

    m_cacheLock.lock();
    for(int i = 0;i < curve_num;i++)
    {
        m_curveScreen[i].clear();
    }
    m_cacheLock.unlock();
}
