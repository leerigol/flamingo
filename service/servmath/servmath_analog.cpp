#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

//#include "../../com/mathlib/mathlib.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"
#include "../../arith/scalestep.h"
#include "servmath.h"
#include "../serveyejit/serveyejit.h"

//! src
DsoErr servMath::setAnaSrcA( Chan chan )
{
    mSrc.mAnaSrc.mSrcA = chan;
    upDateUnit();
    releaseDataSema();
    dsoVert::setyUnit( getUnit() );
    return ERR_NONE;
}
Chan servMath::getAnaSrcA()
{
    setuiEnable( m_nNormalChanA != 0 );
    return mSrc.mAnaSrc.mSrcA;
}

DsoErr servMath::setAnaSrcB( Chan chan )
{
    mSrc.mAnaSrc.mSrcB = chan;
    upDateUnit();
    releaseDataSema();
    dsoVert::setyUnit( getUnit() );
    return ERR_NONE;
}
Chan servMath::getAnaSrcB()
{
    setuiEnable( m_nNormalChanB != 0 );
    return mSrc.mAnaSrc.mSrcB;
}

DsoErr servMath::setLaSrcA( Chan chan )
{
    mSrc.mLaSrc.mSrcA = chan;
    releaseDataSema();
    dsoVert::setyUnit( getUnit() );
    return ERR_NONE;
}
Chan servMath::getLaSrcA()
{
    setuiEnable( m_nLogicChanA != 0 );
    return mSrc.mLaSrc.mSrcA;
}

DsoErr servMath::setLaSrcB( Chan chan )
{
    mSrc.mLaSrc.mSrcB = chan;
    releaseDataSema();
    dsoVert::setyUnit( getUnit() );
    return ERR_NONE;
}
Chan servMath::getLaSrcB()
{
    setuiEnable( m_nLogicChanB != 0 );
    return mSrc.mLaSrc.mSrcB;
}

DsoErr servMath::setFftSrc( Chan chan )
{
    mSrc.mFftSrc.mSrc = chan;
    dsoVert::setyUnit( getUnit() );
    releaseDataSema();
    return ERR_NONE;
}
Chan servMath::getFftSrc()
{
    setuiEnable( m_nFFTChan != 0 );
    return mSrc.mFftSrc.mSrc;
}

DsoErr servMath::setScaleRange()
{
    setAutoScale(false);
    return ERR_NONE;
}

//! cfg
//! 自动根据当前的输入状态更新最大最小档位,同时根据resetVal决定是否重置当前档位
DsoErr servMath::setAutoScale(bool bReset)
{
    //! get scale range
    MathZone zone = getZone();

    RangeCfg range;
    if(zone != math_logic_zone)
    {
        range = updateVScale();
    }
    //! logic to medium
    switch( zone )
    {
        case math_logic_zone:
            if(bReset)
            {
                ssync( MSG_MATH_LOGIC_SCALE, Medium );
            }
            break;

        case math_analog_zone:
            //! save cfg
            mCfg.mAnaCfg.mScale.mMax = range.mMax;
            mCfg.mAnaCfg.mScale.mMin = range.mMin;
            mCfg.mAnaCfg.mScale.mZVal = range.mZVal;
            if(bReset)
            {
                ssync( MSG_MATH_S32FUNCSCALE, range.mZVal );
            }
            break;

        case math_freq_zone:
            if ( mOption.mFftOpt.myUnit == fft_spec_db
                 || mOption.mFftOpt.myUnit == fft_spec_dbm )
            {
                mCfg.mFreqCfgDb.mScale.mMax = range.mMax;
                mCfg.mFreqCfgDb.mScale.mMin = range.mMin;
                mCfg.mFreqCfgDb.mScale.mZVal = range.mZVal;
                if(bReset)
                {
                    ssync( MSG_MATH_FFT_SCALE, range.mZVal );
                }
            }
            else
            {
                mCfg.mFreqCfgVrms.mScale.mMax = range.mMax;
                mCfg.mFreqCfgVrms.mScale.mMin = range.mMin;
                mCfg.mFreqCfgVrms.mScale.mZVal = range.mZVal;
                if(bReset)
                {
                    ssync( MSG_MATH_FFT_SCALE, range.mZVal );
                }
            }
            break;
    }

    return ERR_NONE;
}

//! 根据上一次运算结果自动设置偏移
DsoErr servMath::setAutoOffset()
{
    bool needScale = true;
//    switch (mOperator) {
//    case operator_add:
//    case operator_sub:
//    case operator_mul:
//    case operator_div:
//    case operator_root:
//    case operator_lg:
//    case operator_ln:
//    case operator_exp:
//    case operator_abs:
//        needScale = false;
//        break;
//    default:
//        break;
//    }

    if(isWfmValid == true)
    {
        float max = -1e12;
        float min = 1e12;

        mWfmSema.acquire();
        for(int i = 0;i < m_pWfmMainScr->size();i++)
        {
            max = max > m_pWfmMainScr->valueAt(i) ? max:m_pWfmMainScr->valueAt(i);
            min = min < m_pWfmMainScr->valueAt(i) ? min:m_pWfmMainScr->valueAt(i);
        }
        mWfmSema.release();

        float mScaleBase = (mOperator == operator_trend) ? 1e-12:math_v_base ;

        qlonglong offset = (qlonglong)((max+min)/2/mScaleBase);
        offset = -offset;

        qlonglong scale = (qlonglong)( ((max-min)/mScaleBase)/6 );

        MathZone zone = getZone();
        //! logic to medium
        switch( zone )
        {
        case math_logic_zone:
            //async( MSG_MATH_LOGIC_OFFSET, Medium );
            break;

        case math_analog_zone:
            if(needScale && scale > mCfg.mAnaCfg.mScale.mZVal)
            {
                ssync( MSG_MATH_S32FUNCSCALE, scale );
            }
            ssync( MSG_MATH_VIEW_OFFSET,  offset );
            break;

        case math_freq_zone:
            if(needScale)
            {
                ssync( MSG_MATH_FFT_SCALE, scale );
            }
            ssync( MSG_MATH_FFT_OFFSET, offset);
            break;
        }
    }
    return ERR_NONE;
}

DsoErr servMath::autoFftHoriScale()
{
    CArgument argI;
    argI.setVal(chan1 ,0);
    argI.setVal(horizontal_view_main, 1);
    CArgument argO;
    serviceExecutor::query(serv_name_trace, servTrace::cmd_query_trace_Sa, argI,argO);
    qlonglong llSa = 0;
    argO.getVal(llSa,0);
    ssync( MSG_MATH_FFT_H_START, 0ll );
    if( llSa <= math_Ghz )
    {
        ssync( MSG_MATH_FFT_H_END, llSa/2 );
    }
    else
    {
        ssync( MSG_MATH_FFT_H_END, math_Mhz*500 );
    }

    return ERR_NONE;
}

DsoErr servMath::autoScaleOffset()
{
    mOption.mFftOpt.mFFtAutoScale = false;
    if(getOperator() == operator_trend)
    {
        serviceExecutor::post(serv_name_eyejit,servEyeJit::cmd_jitter_autoScale,0);
    }
    else
    {
        setAutoScale(true);
        setAutoOffset( );
        autoFftHoriScale( );
    }
    return ERR_NONE;
}

DsoErr servMath::setAnaScale( qlonglong scale )
{
    Q_ASSERT( scale > 0 );

    dsoVert::setyInc( scale*math_v_base / adc_vdiv_dots );
    dsoVert::setyRefScale( scale );
    //! 显示位置不变
    if ( getVertExpand() == vert_expand_gnd )
    {
        qlonglong viewOffset = getADCOffset() * scale / 25;
        //! 显示数值改变
        ssync( MSG_MATH_VIEW_OFFSET, viewOffset );
    }
    //! 显示位置改变
    else
    {
        qlonglong viewOffset;
        //! view then
        viewOffset = mCfg.mAnaCfg.mScale.mVal * mViewOffset.mVal / scale;
        ssync( MSG_MATH_VIEW_OFFSET, viewOffset );
    }

    mCfg.mAnaCfg.mScale.mVal = scale;

    releaseDataSema();
    return ERR_NONE;
}

qlonglong servMath::getAnaScale()
{
    QString unitStr;
    Unit unit = getUnit();
    setuiUnit(unit);

    if(unit == Unit_s)
    {
        mUiAttr.setBase( MSG_MATH_S32FUNCSCALE, 1, E_N12);
        mUiAttr.setMinVal( MSG_MATH_S32FUNCSCALE, time_ps(1));
        mUiAttr.setMaxVal( MSG_MATH_S32FUNCSCALE, time_s(1));
    }
    else
    {
        mUiAttr.setBase( MSG_MATH_S32FUNCSCALE, 1, E_N9);
        setScaleRange();
        mUiAttr.setMaxVal( MSG_MATH_S32FUNCSCALE, mCfg.mAnaCfg.mScale.mMax);
        mUiAttr.setMinVal( MSG_MATH_S32FUNCSCALE, mCfg.mAnaCfg.mScale.mMin);
        mUiAttr.setZVal( MSG_MATH_S32FUNCSCALE, mCfg.mAnaCfg.mScale.mZVal);
    }

    if(mVertFineOn)
    {
        mUiAttr.setAcc( MSG_MATH_S32FUNCSCALE, key_acc::e_key_raw );
        mUiAttr.setIStep( MSG_MATH_S32FUNCSCALE, scaleToStep(mCfg.mAnaCfg.mScale.mZVal,1) );
        mUiAttr.setDStep( MSG_MATH_S32FUNCSCALE, scaleToStep(mCfg.mAnaCfg.mScale.mZVal,-1) );
    }
    else
    {
        mUiAttr.setDStep( MSG_MATH_S32FUNCSCALE, -1);
        mUiAttr.setIStep( MSG_MATH_S32FUNCSCALE, 1);
        mUiAttr.setAcc( MSG_MATH_S32FUNCSCALE, key_acc::e_key_one_125 );
    }

    unitStr = gui_fmt::CGuiFormatter::toString( unit );
    mUiAttr.setPostStr( MSG_MATH_S32FUNCSCALE, unitStr );

    return mCfg.mAnaCfg.mScale.mVal;
}

qlonglong servMath::getAnaOffset()
{
    setuiZVal( 0ll );
    if( mCfg.mAnaCfg.mScale.mVal > adc_vdiv_dots)
    {
        setuiStep( mCfg.mAnaCfg.mScale.mVal / adc_vdiv_dots );
    }
    else
    {
        setuiStep( 1 );
    }

    QString unitStr;
    Unit unit = getUnit();
    setuiUnit(unit);

    if(unit == Unit_s)
    {
        setuiBase( 1, E_N12);
        //! qint64: 10^16
        setuiMaxVal(time_s(10000));
        setuiMinVal(time_s(-10000));
    }
    else
    {
        setuiBase( 1, E_N9);
        //! qint64: 1e16
        setuiMaxVal(math_Gv);
        setuiMinVal(-math_Gv);
    }

    unitStr = gui_fmt::CGuiFormatter::toString( unit );
    setuiPostStr( unitStr );

    return mViewOffset.mVal;
}


DsoErr servMath::setLaScale( LaScale scale )
{
    //! la 只有零点扩展
    mOption.mLaOpt.mScale = scale;
    releaseDataSema();
    return ERR_NONE;
}

LaScale servMath::getLaScale()
{
    setuiZVal( (int)Small );
    return mOption.mLaOpt.mScale;
}

DsoErr servMath::setLaOffset( int laKey )
{
    mLaOffset.mVal = mLaOffset.mVal + laKey;
    releaseDataSema();
    return ERR_NONE;
}

qlonglong servMath::getLaOffset()
{
    mUiAttr.setMaxVal( MSG_MATH_LOGIC_OFFSET,  math_view_offset_max);
    mUiAttr.setZVal( MSG_MATH_LOGIC_OFFSET, 0ll );
    mUiAttr.setMinVal( MSG_MATH_LOGIC_OFFSET, math_view_offset_min);

    return mLaOffset.mVal;
}
