#ifndef MATHERR
#define MATHERR

//! math的异常输入处理分为
// 1、不更新屏幕
#define math_err_no_update    -1
// 2、只清掉屏幕
#define math_err_clear_screen -2
// 3、界面提示标语
#define math_err_warning      -3

#endif // MATHERR

