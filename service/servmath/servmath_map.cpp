#include "servmath.h"
#include "../servdso/servdso.h"

/*! \var 定义消息映射表
*
* 定义消息映射表
*/
IMPLEMENT_CMD( serviceExecutor, servMath  )
start_of_entry()

//! menu msg
on_set_int_int( MSG_MATH_S32MATHOPERATOR, &servMath::setOperator ),
on_get_int( MSG_MATH_S32MATHOPERATOR, &servMath::getOperator ),

on_set_int_bool( MSG_MATH_EN, &servMath::setMathEn ),
on_get_bool( MSG_MATH_EN, &servMath::getMathEn ),

on_set_int_int( MSG_MATH_S32ARITHA, &servMath::setAnaSrcA ),
on_get_int( MSG_MATH_S32ARITHA, &servMath::getAnaSrcA ),

on_set_int_int( MSG_MATH_S32ARITHB, &servMath::setAnaSrcB ),
on_get_int( MSG_MATH_S32ARITHB, &servMath::getAnaSrcB ),

on_set_int_ll( MSG_MATH_VIEW_OFFSET, &servMath::setViewOffset ),
on_get_ll( MSG_MATH_VIEW_OFFSET, &servMath::getAnaOffset ),

on_set_int_ll( MSG_MATH_FFT_OFFSET, &servMath::setFftOffset ),
on_get_ll( MSG_MATH_FFT_OFFSET, &servMath::getFftOffset ),

on_set_int_int( MSG_MATH_LOGIC_OFFSET, &servMath::setLaOffset ),
on_get_ll( MSG_MATH_LOGIC_OFFSET, &servMath::getLaOffset ),

on_set_int_ll( MSG_MATH_S32FUNCSCALE, &servMath::setAnaScale ),
on_get_ll( MSG_MATH_S32FUNCSCALE, &servMath::getAnaScale ),

on_set_int_void( MSG_MATH_S32RSTVSCALE, &servMath::autoScaleOffset ),

on_set_int_bool( MSG_MATH_S32MATHINVERT, &servMath::setInvert ),
on_get_bool( MSG_MATH_S32MATHINVERT, &servMath::getInvert ),

on_set_int_bool( MSG_MATH_S32SHOWLABEL, &servMath::setLabelOn ),
on_get_bool( MSG_MATH_S32SHOWLABEL, &servMath::getLabelOn ),

on_set_int_int( MSG_MATH_S32PRESETINDEX, &servMath::setLabelIndex ),
on_get_int( MSG_MATH_S32PRESETINDEX, &servMath::getLabelIndex ),

on_set_int_string( MSG_MATH_S32LABELEDIOR, &servMath::setLabel ),
on_get_string( MSG_MATH_S32LABELEDIOR, &servMath::getLabel ),

on_set_int_int( MSG_MATH_S32LOGICA, &servMath::setLaSrcA ),
on_get_int( MSG_MATH_S32LOGICA, &servMath::getLaSrcA ),

on_set_int_int( MSG_MATH_S32LOGICB, &servMath::setLaSrcB ),
on_get_int( MSG_MATH_S32LOGICB, &servMath::getLaSrcB ),

on_set_int_int( MSG_MATH_LOGIC_SCALE, &servMath::setLaScale ),
on_get_int( MSG_MATH_LOGIC_SCALE, &servMath::getLaScale ),

on_set_int_ll( MSG_MATH_S32LOGIC1THRE, &servMath::setThreshold_CH1 ),
on_get_ll( MSG_MATH_S32LOGIC1THRE, &servMath::getThreshold_CH1 ),

on_set_int_ll( MSG_MATH_S32LOGIC2THRE, &servMath::setThreshold_CH2 ),
on_get_ll( MSG_MATH_S32LOGIC2THRE, &servMath::getThreshold_CH2 ),

on_set_int_ll( MSG_MATH_S32LOGIC3THRE, &servMath::setThreshold_CH3 ),
on_get_ll( MSG_MATH_S32LOGIC3THRE, &servMath::getThreshold_CH3 ),

on_set_int_ll( MSG_MATH_S32LOGIC4THRE, &servMath::setThreshold_CH4 ),
on_get_ll( MSG_MATH_S32LOGIC4THRE, &servMath::getThreshold_CH4 ),

on_set_int_int( MSG_MATH_S32LOGICSENS, &servMath::setThresholdSens ),
on_get_int( MSG_MATH_S32LOGICSENS, &servMath::getThresholdSens ),

on_set_int_real( MSG_MATH_MATH_INTGOPT_BIAS, &servMath::setIntgBias ),
on_get_real( MSG_MATH_MATH_INTGOPT_BIAS, &servMath::getIntgBias ),

on_set_int_int( MSG_MATH_S32DIFFSENS, &servMath::setDiffWindow ),
on_get_int( MSG_MATH_S32DIFFSENS, &servMath::getDiffWindow ),

on_set_int_real( MSG_MATH_MATH_LINEOPT_A, &servMath::setLineA ),
on_get_real( MSG_MATH_MATH_LINEOPT_A, &servMath::getLineA ),

on_set_int_real( MSG_MATH_MATH_LINEOPT_B, &servMath::setLineB ),
on_get_real( MSG_MATH_MATH_LINEOPT_B, &servMath::getLineB ),

on_set_int_int( MSG_MATH_S32FILTER_LP, &servMath::setFilterLp ),
on_get_int( MSG_MATH_S32FILTER_LP, &servMath::getFilterLp ),

on_set_int_int( MSG_MATH_S32FILTER_HP, &servMath::setFilterHp ),
on_get_int( MSG_MATH_S32FILTER_HP, &servMath::getFilterHp ),

on_set_int_int( MSG_MATH_S32FILTER_BP1, &servMath::setFilterBp1 ),
on_get_int( MSG_MATH_S32FILTER_BP1, &servMath::getFilterBp1 ),

on_set_int_int( MSG_MATH_S32FILTER_BP2, &servMath::setFilterBp2 ),
on_get_int( MSG_MATH_S32FILTER_BP2, &servMath::getFilterBp2 ),

on_set_int_int( MSG_MATH_S32FILTER_BT1, &servMath::setFilterBt1 ),
on_get_int( MSG_MATH_S32FILTER_BT1, &servMath::getFilterBt1 ),

on_set_int_int( MSG_MATH_S32FILTER_BT2, &servMath::setFilterBt2 ),
on_get_int( MSG_MATH_S32FILTER_BT2, &servMath::getFilterBt2 ),

on_set_int_int( MSG_MATH_S32FFTSRC, &servMath::setFftSrc ),
on_get_int( MSG_MATH_S32FFTSRC, &servMath::getFftSrc ),

on_set_int_ll( MSG_MATH_FFT_SCALE, &servMath::setFftScale ),
on_get_ll( MSG_MATH_FFT_SCALE, &servMath::getFftScale ),

on_set_int_bool( MSG_MATH_S32FFTUNIT, &servMath::setyUnit ),
on_get_bool( MSG_MATH_S32FFTUNIT, &servMath::getyUnit ),

on_set_int_bool( MSG_MATH_FFT_X_TYPE, &servMath::setxType ),
on_get_bool( MSG_MATH_FFT_X_TYPE, &servMath::getxType ),

on_set_int_bool( MSG_MATH_S32FFTSCR, &servMath::setScreen ),
on_get_bool( MSG_MATH_S32FFTSCR, &servMath::getFftScreen ),

on_set_int_ll( MSG_MATH_FFT_H_SPAN, &servMath::setFftSpan ),
on_get_ll( MSG_MATH_FFT_H_SPAN, &servMath::getFftSpan ),

on_set_int_ll( MSG_MATH_FFT_H_CENTER, &servMath::setFftCenter ),
on_get_ll    ( MSG_MATH_FFT_H_CENTER, &servMath::getFftCenter ),

on_set_int_int( MSG_MATH_S32FFTWINDOW, &servMath::setFftWindow ),
on_get_int    ( MSG_MATH_S32FFTWINDOW, &servMath::getFftWindow ),

on_set_int_bool( MSG_MATH_FFT_DC_SUPPRESS, &servMath::setFftDcSuppress ),
on_get_bool    ( MSG_MATH_FFT_DC_SUPPRESS, &servMath::getFftDcSuppress ),

on_set_int_ll( MSG_MATH_FFT_H_START, &servMath::setFftHStart ),
on_get_ll    ( MSG_MATH_FFT_H_START, &servMath::getFftHStart ),

on_set_int_ll( MSG_MATH_FFT_H_END, &servMath::setFftHEnd ),
on_get_ll    ( MSG_MATH_FFT_H_END, &servMath::getFftHEnd ),

on_set_int_bool( MSG_MATH_FFT_PEAK_ENABLE, &servMath::setPeakEnable),
on_get_bool    ( MSG_MATH_FFT_PEAK_ENABLE, &servMath::getPeakEnable),

on_set_int_int( MSG_MATH_FFT_PEAK_MAXPEAKS, &servMath::setPeakMaxPoints),
on_get_int    ( MSG_MATH_FFT_PEAK_MAXPEAKS, &servMath::getPeakMaxPoints),

on_set_int_ll ( MSG_MATH_FFT_PEAK_THRESHOLD, &servMath::setPeakThreshold),
on_get_ll     ( MSG_MATH_FFT_PEAK_THRESHOLD, &servMath::getPeakThreshold),

on_set_int_ll ( MSG_MATH_FFT_PEAK_EXCURSION, &servMath::setPeakExcursion),
on_get_ll     ( MSG_MATH_FFT_PEAK_EXCURSION, &servMath::getPeakExcursion),

on_set_int_int (MSG_MATH_FFT_PEAK_TABELORDER, &servMath::setPeakOrder),
on_get_int     (MSG_MATH_FFT_PEAK_TABELORDER, &servMath::getPeakOrder),

on_set_int_bool (MSG_MATH_EXPAND, &servMath::setVertExpand),
on_get_bool     (MSG_MATH_EXPAND, &servMath::getVertExpand),

on_set_int_void (MSG_MATH_FFT_PEAK_EXPORT, &servMath::exportEventData),

on_set_int_bool (MSG_MATH_COLOR_ONOFF, &servMath::setColorOnOff),
on_get_bool     (MSG_MATH_COLOR_ONOFF, &servMath::getColorOnOff),

on_set_int_void (MSG_MATH_COLOR_RESET, &servMath::resetColor),

//! user msg
on_get_string( servMath::cmd_expr, &servMath::getExpression ),
on_get_string( servMath::cmd_calc_expr, &servMath::getCalcExpression ),

on_set_void_void( servMath::cmd_math_unit_update, &servMath::on_cmd_math_unit_update),

on_set_void_void( servMath::cmd_ch1_unit_change, &servMath::on_cmd_ch1_unit_change),
on_set_void_void( servMath::cmd_ch2_unit_change, &servMath::on_cmd_ch2_unit_change),
on_set_void_void( servMath::cmd_ch3_unit_change, &servMath::on_cmd_ch3_unit_change),
on_set_void_void( servMath::cmd_ch4_unit_change, &servMath::on_cmd_ch4_unit_change),

on_set_void_void( servMath::cmd_ch1_onoff_change, &servMath::on_cmd_ch1_onoff_change),
on_set_void_void( servMath::cmd_ch2_onoff_change, &servMath::on_cmd_ch2_onoff_change),
on_set_void_void( servMath::cmd_ch3_onoff_change, &servMath::on_cmd_ch3_onoff_change),
on_set_void_void( servMath::cmd_ch4_onoff_change, &servMath::on_cmd_ch4_onoff_change),
on_set_void_void( servMath::cmd_la_onoff_change,  &servMath::on_cmd_la_onoff_change),
on_set_void_void( servMath::cmd_mathref_onoff_change, &servMath::changeSrcMenuEn),

on_set_void_void( servMath::cmd_trace_sa_change, &servMath::on_cmd_trace_sa_change),

//! app used
on_get_bool ( servMath::cmd_q_input_valid, &servMath::getInputValid ),

on_get_float( servMath::cmd_ch1_threshold_change, &servMath::on_cmd_q_ch1_threshold_float),
on_get_float( servMath::cmd_ch2_threshold_change, &servMath::on_cmd_q_ch2_threshold_float),
on_get_float( servMath::cmd_ch3_threshold_change, &servMath::on_cmd_q_ch3_threshold_float),
on_get_float( servMath::cmd_ch4_threshold_change, &servMath::on_cmd_q_ch4_threshold_float),

on_get_string( servMath::cmd_q_ch1_threshold_unit_string, &servMath::on_cmd_q_ch1_threshold_unit_string),
on_get_string( servMath::cmd_q_ch2_threshold_unit_string, &servMath::on_cmd_q_ch2_threshold_unit_string),
on_get_string( servMath::cmd_q_ch3_threshold_unit_string, &servMath::on_cmd_q_ch3_threshold_unit_string),
on_get_string( servMath::cmd_q_ch4_threshold_unit_string, &servMath::on_cmd_q_ch4_threshold_unit_string),

on_get_int( servMath::cmd_math_unit, &servMath::getUnit ),
on_get_string( servMath::cmd_q_unit_string, &servMath::getUnitString ),

on_get_ll( servMath::cmd_q_view_scale, &servMath::getViewScale ),

on_get_ll( servMath::cmd_q_adc_offset, &servMath::getADCOffset ),
on_get_float( servMath::cmd_q_real_scale, &servMath::getRealScale ),
on_get_float( servMath::cmd_q_real_offset, &servMath::getRealOffset ),
on_get_string( servMath::cmd_q_operator_string, &servMath::getExpression ),
on_get_int( servMath::cmd_q_zone, &servMath::getZone ),

on_get_float( servMath::cmd_q_real_hscale, &servMath::getFftHScale ),
on_get_float( servMath::cmd_q_real_hcenter, &servMath::getFftHCenter ),

on_get_ll( servMath::cmd_q_zoomfft_start, &servMath::getFftZoomHstart),
on_get_ll( servMath::cmd_q_zoomfft_scale, &servMath::getFftZoomHScale),

on_get_float( servMath::cmd_q_real_hresolution, &servMath::getFftHResolution ),
on_set_int_int( servMath::cmd_q_real_hresolution, &servMath::setFftHResolution ),

on_get_float( servMath::cmd_q_fft_real_peakThres, &servMath::getRealFftPeakThres),

on_set_void_void( servMath::cmd_q_fft_peak, &servMath::setPeak),
on_get_pointer( servMath::cmd_q_fft_peak, &servMath::getPeak),

on_set_void_void( servMath::cmd_on_zoom_en, &servMath::onZoomEn),
on_set_void_void( servMath::cmd_on_clear, &servMath::onClear),
on_set_void_void( servMath::cmd_unfreezed_math, &servMath::unFreezeMath),

on_set_void_void  ( servMath::cmd_trend_scale,     &servMath::setAutoScale),
on_set_int_pointer( servMath::cmd_trace_updated,   &servMath::setDataProvider ),
on_set_int_pointer( servMath::cmd_jitter_updated,  &servMath::setJitterProvider ),
on_set_int_int    ( servMath::cmd_hor_mode_change, &servMath::on_cmd_hori_mode_change ),

on_set_int_bool(servMath::cmd_vertScale_fineon, &servMath::setVertFineOn),
on_get_bool    (servMath::cmd_vertScale_fineon, &servMath::getVertFineOn),

on_get_float( servMath::cmd_q_real_disp_offset, &servMath::getfViewOffset ),//![dba+, bug:524]
on_get_pointer( servMath::cmd_math_grade_update, &servMath::getColorCurve),

//! regist
on_set_void_void( servDso::CMD_SCREEN_MODE, &servMath::on_scr_mode_change),

//! scpi
on_set_int_ll (servMath::cmd_scpi_filter_w1,  &servMath::scpiSetFilterBw1),
on_get_ll     (servMath::cmd_scpi_filter_w1,  &servMath::scpiGetFilterBw1),

on_set_int_ll (servMath::cmd_scpi_filter_w2,  &servMath::scpiSetFilterBw2),
on_get_ll     (servMath::cmd_scpi_filter_w2,  &servMath::scpiGetFilterBw2),

on_set_int_bool( servMath::cmd_scpi_fft_split, &servMath::setScpiScreen ),
on_get_bool( servMath::cmd_scpi_fft_split, &servMath::getScpiFftScreen ),

//! system msg
on_set_int_int( CMD_SERVICE_ACTIVE, &servMath::onActive),
on_get_int    ( CMD_SERVICE_ACTIVE, &servMath::isActive),
on_set_void_void( CMD_SERVICE_RST,  &servMath::rst ),
on_set_int_void( CMD_SERVICE_STARTUP, &servMath::startup ),
on_set_void_void( CMD_SERVICE_INIT, &servMath::init ),

end_of_entry()
