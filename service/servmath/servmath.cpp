#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "servmath.h"
#include "../servcursor/servcursor.h"
int servMath::mathOn = 0;
bool servMath::_singleInit = false;

FftScreen servMath::mScreenFFT = fft_screen_full;
int servMath::mFFTCount = 0;//for bug1705 by hxh

//! single thread
mathThread *servMath::m_pWorkThread = NULL;
bool servMath::_windowInited = false;
QStringList servMath::_labelList;

//! 键映射表,分别处理偏移和档位
serviceKeyMap servMath::_arithMap;
serviceKeyMap servMath::_laMap;
serviceKeyMap servMath::_fftMap;

qlonglong LaOpt::mThresholds[4] = {0,0,0,0};
QString servMath::mServName = serv_name_math1;

servMath::servMath( QString name, Chan ch, ServiceId eId ) :
    servVert(name, eId ), dsoVert( ch )
{
    serviceExecutor::baseInit();

    mVersion = 6;

    dsoVert::setServId( mpItem->servId );

    //! 从属于math类的成员，只执行一次初始化
    servMath::singleInit();

    //! only one thread for all MATHes
    if ( NULL == m_pWorkThread )
    {
        m_pWorkThread = new mathThread();
        Q_ASSERT( m_pWorkThread != NULL );
    }

    m_pWorkThread->attachClient( this );

    m_pActiveProvider = NULL;
    mWfmSema.release();

    m_pWfmMainScr = new DsoWfm();
    Q_ASSERT( NULL != m_pWfmMainScr );
    m_pWfmZoomScr = new DsoWfm();
    Q_ASSERT( NULL != m_pWfmZoomScr );
    m_pWfmMainResult = new DsoWfm();
    Q_ASSERT( NULL != m_pWfmMainResult);
    m_pWfmZoomResult = new DsoWfm();
    Q_ASSERT( NULL != m_pWfmZoomResult);

    m_pMathGrade = new CMathGrade;
    Q_ASSERT( NULL != m_pMathGrade);

    isWfmValid = false;
    m_bInPutValid = true;
    mVertFineOn = false;
    appendCacheList(this);
}

servMath::~servMath()
{
//    if ( NULL != m_pWfmScr )
//    {
//        delete m_pWfmScr;
//    }
}

void servMath::singleInit()
{
    if ( !servMath::_singleInit )
    {
        servMath::_singleInit = true;
    }
    else
    { return; }

    fillLabelList();

    fillArithKeyMap();
    fillLaKeyMap();
    fillFftKeyMap();
}

//! 标签顺序需要和菜单对应的序号保持一致
void servMath::fillLabelList()
{
    servMath::_labelList.append( QStringLiteral("ADD") );
    servMath::_labelList.append( QStringLiteral("SUB") );
    servMath::_labelList.append( QStringLiteral("MUL") );
    servMath::_labelList.append( QStringLiteral("DIV") );

    servMath::_labelList.append( QStringLiteral("FFT") );

    servMath::_labelList.append( QStringLiteral("AND") );
    servMath::_labelList.append( QStringLiteral("OR") );
    servMath::_labelList.append( QStringLiteral("XOR") );
    servMath::_labelList.append( QStringLiteral("NOT") );

    servMath::_labelList.append( QStringLiteral("Intg") );
    servMath::_labelList.append( QStringLiteral("Diff") );
    servMath::_labelList.append( QStringLiteral("Sqrt") );
    servMath::_labelList.append( QStringLiteral("Lg") );
    servMath::_labelList.append( QStringLiteral("Ln") );
    servMath::_labelList.append( QStringLiteral("Exp") );
    servMath::_labelList.append( QStringLiteral("Abs") );

//#ifndef FLAMINGO_TR5
    servMath::_labelList.append( QStringLiteral("LPas") );
    servMath::_labelList.append( QStringLiteral("HPas") );
    servMath::_labelList.append( QStringLiteral("BPas") );
    servMath::_labelList.append( QStringLiteral("BStop") );
//#endif
    servMath::_labelList.append( QStringLiteral("AX+B") );
}


void servMath::resetData()
{
    mFFTCount = 0;
}
