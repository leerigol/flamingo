#ifndef MATHOPTIONS_H
#define MATHOPTIONS_H

#include <QtCore>

#include "../../include/dsotype.h"
#include "../../baseclass/dsoreal.h"

#include "../../com/fftlib/rfftc.h" //! y unit && window


using namespace DsoType;

/*!
 * \brief The vOpt struct
 * virtual option
 */
struct vOpt
{
    virtual ~vOpt(){}
};

struct DiffOpt : public vOpt
{
    int mWindowSize;
};

struct LaOpt : public vOpt
{
    static qlonglong mThresholds[4];
    int mSens;      //! 0.1 unit

    LaScale mScale;
};

struct IntgOpt : public vOpt
{
    DsoReal mBias;
};

struct FftOpt : public vOpt
{
    bool mDcSuppress;
    bool mFFtAutoScale;
    fftWindow mWindow;
    fftSpecUnit myUnit;
    FftxType mxType;
    FftScreen mScreen;
};

struct LineOpt : public vOpt
{
    DsoReal a;
    DsoReal b;
};

struct VarOpt : public vOpt
{
    DsoReal mVar1;
    DsoReal mVar2;
};

struct FilterOpt : public vOpt
{
    int mLpW;
    int mHpW;
    int mBpW1, mBpW2;
    int mBtW1, mBtW2;
};

class MathOptions
{

public:
    DiffOpt mDiffOpt;
    LaOpt mLaOpt;
    IntgOpt mIntgOpt;
    FftOpt mFftOpt;

    FilterOpt mFilterOpt;
    LineOpt mLineOpt;
};

#endif // MATHOPTIONS_H
