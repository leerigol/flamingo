#include <QTime>
#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "../servplot/servplot.h"
#include "../servhori/servhori.h"

#include "servmath.h"

#define math_no_sema -1

quint64 mathThread::m_u64DelayStart = 0;

mathThread::mathThread( QObject */*parent*/ ) : QThread(  )
{

}

/*!
 * \brief mathThread::attachClient
 * \param pMath
 * math 客户端连接
 * 所有的MATH共用一个计算线程
 */
void mathThread::attachClient( servMath *pMath )
{
    mClients.append( pMath );
}

servMath* mathThread::getClient(int i)
{
    if( i < mClients.size() )
    {
        return mClients.at(i);
    }
    return NULL;
}

void mathThread::run()
{
    QTime fftTime;
    fftTime.start();
    servMath *pMath;

    bool bm[ mClients.count() ];
    int i, activeCnt;

    //! init
    for ( i = 0; i < mClients.count(); i++ )
    {
        bm[ i ] = false;
    }

    if( m_u64DelayStart > 0 )
    {
        //waiting for the system started
        QThread::sleep(21);
    }

    forever
    {
        LOG_DBG()<<"beginTime"<<fftTime.elapsed()<<"ms";
        //! service
        i = 0;
        activeCnt = 0;
        foreach( pMath, mClients )
        {
            Q_ASSERT(pMath!= NULL);
            if ( pMath->getMathEn() )
            {
                if(pMath->getColorOnOff())
                {
                    servClientColor( pMath );
                }
                else
                {
                    servClient( pMath );
                }
                bm[i] = true;
                activeCnt++;
            }
            i++;
        }
        //! 检测到开关状态变化
        //! disconnect
        i = 0;
        foreach( pMath, mClients )
        {
            if ( bm[i] && pMath->getMathEn() != bm[i] )
            {
                disconnectClient( pMath );
                bm[i] = false;
            }
            i++;
        }
        LOG_DBG();
        //! no client now
        if ( activeCnt < 1 )
        {
            QThread::msleep(200);
        }
        LOG_DBG()<<"endTime"<<fftTime.elapsed()<<"ms";
        QThread::msleep( 10 );
    }
}

int mathThread::servClient( servMath * pMath )
{
    Q_ASSERT( pMath );

    int err = 0;

    DsoWfm wfmMainScr;
    DsoWfm wfmZoomScr;

    mathWorkContext wContext;
    mathWork clientWork;

    bool zoomEn = pMath->getZoomEn();
    //! acquire
    MathOperator opMath = pMath->getOperator();
    fftSpecUnit fftUnit = pMath->getyUnit();
    if(pMath->getOperator() != operator_trend)
    {
        //! add by zx for bug 1784
        if( ! pMath->mDataSema.tryAcquire() )
        {
            QThread::msleep(50);
            return math_no_sema;
        }

        wContext.pServMath = pMath;

        err = clientWork.process( &wContext, wfmMainScr );
        if(zoomEn)
        {
            clientWork.process( &wContext, wfmZoomScr, horizontal_view_zoom);
        }
    }
    else
    {
        pMath->mJitterSema.acquire();
        wContext.pServMath = pMath;
        err = clientWork.process( &wContext, wfmMainScr );
    }

    //! 起点和长度都为0的情况下，不更新界面；
    //! 起点为1000，长度为0的情况下，需要清空界面(正常post)；
    if( wfmMainScr.start() == 0 && wfmMainScr.size() == 0 &&  err == math_err_no_update  )
    {
        return ERR_NO_TRACE;
    }

    if( (wfmMainScr.start() == 1000 || err == math_err_warning) )
    {
        pMath->setInputValid(false);
        serviceExecutor::post(pMath->getId(), servMath::cmd_q_input_valid, false);

        pMath->m_pWfmMainScr->setRange(0,0);
        pMath->m_pWfmZoomScr->setRange(0,0);
    }
    else
    {
        pMath->setInputValid(true);
        serviceExecutor::post(pMath->getId(), servMath::cmd_q_input_valid, true);

        pMath->mWfmSema.acquire();
        *(pMath->m_pWfmMainScr) = wfmMainScr;
        *(pMath->m_pWfmZoomScr) = wfmZoomScr;
        pMath->mWfmSema.release();
        pMath->isWfmValid = true;
    }


    if( pMath->mbNeedAutoScale )
    {
        if( opMath == pMath->getOperator() )
        {
            pMath->mbNeedAutoScale = false;
            pMath->autoScaleOffset();
        }
        return ERR_NONE;
    }

    if( pMath->mbNeedFftVertScale )
    {
        if( pMath->getyUnit() == fftUnit )
        {
            pMath->mbNeedFftVertScale = false;
            pMath->setAutoScale(true);
            pMath->setAutoOffset(  );
            return ERR_NONE;
        }
        else
        {
            return ERR_NONE;
        }
    }

    //! 防止在开关zoom的过程中出现的错误
    if(pMath->m_bFreezedMath)
    {
        return math_err_no_update;
    }

    if(pMath->m_pWfmMainScr->getAttrId() != servHori::getAttrId() &&
            pMath->m_pWfmMainScr->getAttrId() != (servHori::getAttrId() - 1) % 4096
            && err!= math_err_warning)
    {
        return math_err_no_update;
    }

    if(zoomEn)
    {
        QSemaphore mainSema;
        QSemaphore zoomSema;

        plotContext pMainContext;
        plotContext pZoomContext;

        //! 防止由于异步导致的刚关闭又被打开的问题
        if(pMath->getMathEn())
        {
            if(pMath->getColorOnOff())
            {
                return ERR_NONE;
            }

            if( pMath->mFFTCount == 0 )
            {
                serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                                       (int)(PLOT_ID_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                       NULL );
            }

            pMainContext.set( Math_color,
                              (PlotId)(PLOT_ID_MAIN_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                              &wfmMainScr, &mainSema );

            serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_plot_sync,
                                   (pointer)&pMainContext , NULL );

            pZoomContext.set( Math_color,
                              (PlotId)(PLOT_ID_ZOOM_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                              &wfmZoomScr, &zoomSema );
            serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_plot_sync,
                                   (pointer)&pZoomContext , NULL );
            //! wait plot end
            mainSema.acquire();
            zoomSema.acquire();
        }
    }
    else
    {
        if(pMath->getColorOnOff())
        {
            return ERR_NONE;
        }
        //added by hxh for1317
        if( pMath->mFFTCount==0 )
        {
            if(pMath->getZoomEnBefore())
            {
                serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                                       (int)(PLOT_ID_ZOOM_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                       NULL );
            }

            QSemaphore sema;
            plotContext pContext;

            //! 防止由于异步导致的刚关闭又被打开的问题
            if(pMath->getMathEn())
            {
                if(pMath->m_mathPlotChanMode != servMath::Math_Plot_Main)
                {
                    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                                           (int)(PLOT_ID_ZOOM_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                           NULL );
                }

                pContext.set( Math_color,
                              (PlotId)(PLOT_ID_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                              &wfmMainScr, &sema );
                serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_plot_sync,
                                       (pointer)&pContext , NULL );
                pMath->m_mathPlotChanMode = servMath::Math_Plot_Main;
                //! wait plot end
                LOG_DBG();
                sema.acquire();
                LOG_DBG();
            }
        }
        else
        {
            QSemaphore sema;
            plotContext pContext;

            //! 防止由于异步导致的刚关闭又被打开的问题
            if(pMath->getMathEn())
            {
                if( pMath->mScreenFFT == fft_screen_full )
                {
                    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                                           (int)(PLOT_ID_ZOOM_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                           NULL );
                    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                                           (int)(PLOT_ID_MAIN_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                           NULL );
                    pContext.set( Math_color,
                                  (PlotId)(PLOT_ID_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                  &wfmMainScr, &sema );

                    pMath->m_mathPlotChanMode = servMath::Math_Plot_Main;
                }
                else
                {
                    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                                           (int)(PLOT_ID_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                           NULL );
                    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                                           (int)(PLOT_ID_MAIN_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                           NULL );
                    pContext.set( Math_color,
                                  (PlotId)(PLOT_ID_ZOOM_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                                  &wfmMainScr, &sema );

                    pMath->m_mathPlotChanMode = servMath::Math_Plot_Zoom;
                }

                serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_plot_sync,
                                       (pointer)&pContext , NULL );

                //! wait plot end
                LOG_DBG();
                sema.acquire();
                LOG_DBG();
            }
        }
    }

    return ERR_NONE;
}

int mathThread::servClientColor(servMath *pMath)
{
    Q_ASSERT( pMath );

    DsoWfm wfmMainScr;
    mathWorkContext wContext;
    mathWork clientWork;

    LOG_DBG();
    //! acquire
    if(pMath->getOperator() != operator_trend)
    {
        //! add by zx for bug 1784
        if( ! pMath->mDataSema.tryAcquire() )
        {
            QThread::msleep(50);
            return math_no_sema;
        }

        wContext.pServMath = pMath;
        clientWork.testMathColor( &wContext, wfmMainScr );
    }

    if(pMath->getColorOnOff())
    {
        serviceExecutor::post( pMath->getServId(), servMath::cmd_math_grade_update, 0);
    }
    return 0;
}

/*!
 * \brief mathThread::disconnectClient
 * \param pMath
 * 关闭 smath
 */
void mathThread::disconnectClient( servMath * pMath )
{
    Q_ASSERT( pMath );
    pMath->isWfmValid = false;
    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                           (int)(PLOT_ID_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                           NULL );
    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                           (int)(PLOT_ID_MAIN_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                           NULL );
    serviceExecutor::post( E_SERVICE_ID_PLOT, servPlot::cmd_close_polt,
                           (int)(PLOT_ID_ZOOM_MATH1 + pMath->getId() - E_SERVICE_ID_MATH1),
                           NULL ); 
    pMath->m_pMathGrade->clearColorGrade();

    pMath->m_pWfmMainResult->setRange(0,0);
    pMath->m_pWfmZoomResult->setRange(0,0);

    serviceExecutor::post( pMath->getServId(), servMath::cmd_math_grade_update, 0);
}
