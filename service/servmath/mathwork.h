#ifndef MATHWORK_H
#define MATHWORK_H

#include <QtGlobal>

#include "../../include/dsotype.h"

#include "../../baseclass/dsowfm.h"

#include "../../service/servtrace/dataprovider.h"
#include "matherr.h"

using namespace DsoType;

class servMath;

struct mathWorkContext
{
    dataProvider *provider;

    servMath *pServMath;
};

class mathWork
{
public:
    mathWork();

protected:
    void expandLa(DsoWfm &wfm);

    void digitlize( mathWorkContext *pContext,
                    DsoWfm &wfm,
                    Chan aCh );
protected:
    void begin();

    DsoErr calc( mathWorkContext *pContext, DsoWfm &wfm ,HorizontalView = horizontal_view_main);

    void end();

    DsoErr hScaleProc(mathWorkContext *pContext,
                       DsoWfm &wfmIn,
                       DsoWfm &wfmOut ,
                      HorizontalView view);

    DsoErr hScaleProc_fft(mathWorkContext *pContext,
                           DsoWfm &wfmIn,
                           DsoWfm &wfmOut ,
                          HorizontalView hview);
    DsoErr hScaleProc_fft_Color(mathWorkContext *pContext,
                          DsoWfm &wfmIn,
                          DsoWfm &wfmOut,
                          HorizontalView hview);

    DsoErr hScaleProc_vector(mathWorkContext *pContext,
                              DsoWfm &wfmIn,
                              DsoWfm &wfmOut,
                              HorizontalView horizontalView);

    DsoErr vScaleProc( mathWorkContext *pContext,
                       DsoWfm &wfm );
public:
    int  process(mathWorkContext *pContext, DsoWfm &wfmScr,
                 HorizontalView view = horizontal_view_main);
    void reScaleScr(mathWorkContext *pContext, DsoWfm &wfm, DsoWfm &wfmScr,
                    HorizontalView view = horizontal_view_main);

    int testMathColor(mathWorkContext *pContext,
                       DsoWfm &wfmScr , HorizontalView view = horizontal_view_main);
};

#endif // MATHWORK_H
