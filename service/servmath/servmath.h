#ifndef SERVMATH_H
#define SERVMATH_H

#include "../../include/dsostd.h"

#include "../../baseclass/iserial.h"

#include "../service.h"

#include "../servvert/servvert.h"

#include "mathwork.h"
#include "mathsrc.h"
#include "mathcfg.h"
#include "mathoptions.h"

#include "./operator/math_opers.h"
#include "./operator/rfft/fftpeaksearch.h"

#include "./math_grade/math_grade.h"

#define LOG_MATH()   if(sysHasArg("-log_math"))   qDebug()<<__FUNCTION__<<__FILE__<<__LINE__

class mathThread;
class mathWork;
class servMath : public servVert,
                 public ISerial,
                 public dsoVert
{
    Q_OBJECT
    DECLARE_CMD()
public:
    enum enumCmd
    {
        cmd_none = 0,

        cmd_expr,
        cmd_calc_expr,
        cmd_trace_updated,  /*!< ptr */
        cmd_jitter_updated,
        cmd_trend_scale,

        cmd_math_unit,
        cmd_math_unit_update,

        cmd_ch1_unit_change,
        cmd_ch2_unit_change,
        cmd_ch3_unit_change,
        cmd_ch4_unit_change,

        cmd_ch1_onoff_change,
        cmd_ch2_onoff_change,
        cmd_ch3_onoff_change,
        cmd_ch4_onoff_change,
        cmd_la_onoff_change,

        cmd_mathref_onoff_change,

        cmd_trace_sa_change,
        cmd_hor_mode_change,
        cmd_on_zoom_en,
        cmd_on_clear,

        cmd_q_input_valid,
        cmd_unfreezed_math,

        cmd_q_real_scale,
        cmd_q_real_offset,
        cmd_q_real_disp_offset, //![dba, bug:524]
        cmd_q_unit_string,
        cmd_q_operator_string,
        cmd_q_zone,
        cmd_q_adc_offset,
        cmd_q_view_scale,
                            /*!< fft */
        cmd_q_real_hscale,
        cmd_q_real_hcenter,
        cmd_q_zoomfft_start,
        cmd_q_zoomfft_scale,
        cmd_q_real_hresolution,

        cmd_q_fft_real_peakThres,
        cmd_q_fft_peak,
                                //! threashold
        cmd_ch1_threshold_change,
        cmd_ch2_threshold_change,
        cmd_ch3_threshold_change,
        cmd_ch4_threshold_change,

        cmd_q_ch1_threshold_unit_string,
        cmd_q_ch2_threshold_unit_string,
        cmd_q_ch3_threshold_unit_string,
        cmd_q_ch4_threshold_unit_string,

        cmd_vertScale_fineon,
        cmd_math_grade_update,

        //! scpi cmd
        cmd_scpi_filter_w1,
        cmd_scpi_filter_w2,

        cmd_scpi_fft_split,

        cmd_scpi_thre1,
        cmd_scpi_thre2,
        cmd_scpi_thre3,
        cmd_scpi_thre4,
    };

    enum MathPlotChanMode
    {
        Math_Plot_Main,
        Math_Plot_Zoom,
        Math_Plot_Main_Zoom,
    };

public:
    servMath( QString name, Chan ch, ServiceId eId = E_SERVICE_ID_MATH1 );
    ~servMath();

    virtual DsoErr start();    
    virtual void registerSpy();

    int serialOut( CStream &stream, serialVersion &ver );
    int serialIn( CStream &stream, serialVersion ver );
    void rst();
    int onActive(int onoff);
    int startup();
    void init();

    virtual bool keyTranslate(int key, int &msg, int &controlAction);

public:
    virtual void getPixel(  DsoWfm &wfm, Chan subChan = chan_none, HorizontalView view = horizontal_view_main );
    virtual void getTrace(  DsoWfm &wfm, Chan subChan = chan_none, HorizontalView view = horizontal_view_main );
    virtual void getMemory( DsoWfm &wfm, Chan subChan = chan_none );

protected:
    static bool _singleInit;

    static bool _windowInited;
    static QStringList _labelList;

    static serviceKeyMap _arithMap;
    static serviceKeyMap _laMap;
    static serviceKeyMap _fftMap;
    static QString       mServName;

private:
    static void singleInit();

    static void fillLabelList();
    static void fillArithKeyMap();
    static void fillLaKeyMap();
    static void fillFftKeyMap();

protected:
    bool servKeyMapFilter(  serviceKeyMap &keyMap,
                            int key,
                            int &msg,
                            int &controlAction);
protected:
    QSemaphore mDataSema;
    QSemaphore mJitterSema;
    //! 给math的运算结果上锁是为了防止线程中和菜单上计算自动偏移时同时访问了计算结果
    QSemaphore mWfmSema;

    dataProvider* m_pActiveProvider;
    DsoWfm *m_pWfmMainScr;
    DsoWfm *m_pWfmZoomScr;
    DsoWfm *m_pWfmMainResult;
    DsoWfm *m_pWfmZoomResult;
    bool m_bFreezedMath;
    bool isWfmValid;
    bool m_bInPutValid;

private:
    //! update
    void setInputValid(bool valid);
    bool getInputValid();
    void on_cmd_math_unit_update();

    void on_cmd_ch1_unit_change();
    void on_cmd_ch2_unit_change();
    void on_cmd_ch3_unit_change();
    void on_cmd_ch4_unit_change();

    void on_cmd_ch1_onoff_change();
    void on_cmd_ch2_onoff_change();
    void on_cmd_ch3_onoff_change();
    void on_cmd_ch4_onoff_change();

    void findChanA(int except, bool en);
    void findChanB(int except, bool en);

    void findLogicChanA(int except, bool en);
    void findLogicChanB(int except, bool en);

    void findFFTChan(int except, bool en);

    void on_cmd_la_onoff_change();
    void onMathSrcEnChange(Chan ch);

    void on_cmd_trace_sa_change();

    void on_scr_mode_change();

    float on_cmd_q_ch1_threshold_float();
    float on_cmd_q_ch2_threshold_float();
    float on_cmd_q_ch3_threshold_float();
    float on_cmd_q_ch4_threshold_float();

    QString on_cmd_q_ch1_threshold_unit_string();
    QString on_cmd_q_ch2_threshold_unit_string();
    QString on_cmd_q_ch3_threshold_unit_string();
    QString on_cmd_q_ch4_threshold_unit_string();

    int on_cmd_hori_mode_change(int mode);

public:
    DsoErr setDataProvider( void *ptr );
    DsoErr releaseDataSema();
    DsoErr setJitterProvider(void );
    //! get
    QString getExpression();
    QString getCalcExpression();

    QString toString( Chan ch );
    QString toCalcString( Chan ch );
    QString toDString( Chan ch );

    float getRealScale();
    float getRealOffset();
    float getRealFftPeakThres();

    bool getViewInvert();

    void doTest();

    void fillWfmA( DsoWfm &wfm );
    void fillWfmB( DsoWfm &wfm );
    void fillWfmN( DsoWfm &wfm );

    void saveWfm( DsoWfm &wfm,
                  const QString &fileName );
    void showWfm( DsoWfm &wfm );

    void savePoint( DsoWfm &wfm,
                  const QString &fileName );
    void logPoint( DsoWfm &wfm,
                   const QString &fileName );
    void showPoint( DsoWfm &wfm );

Q_SIGNALS:

public:
    //! common
    DsoErr setOperator( MathOperator oper );
    MathOperator getOperator();

    DsoErr setMathEn( bool bEn );
    bool   getMathEn();

    DsoErr changeSrcMenuEn( );

    void plot(bool en);

    DsoErr setInvert( bool bInv );
    bool getInvert();

    DsoErr setLabelOn( bool b );
    bool getLabelOn();

    DsoErr setLabelIndex( int index );
    int getLabelIndex();

    DsoErr setLabel( QString str );
    QString getLabel();

    DsoErr setVertFineOn( bool fineOn );
    bool   getVertFineOn();

    //! src
    DsoErr setAnaSrcA( Chan chan );
    Chan getAnaSrcA();

    DsoErr setAnaSrcB( Chan chan );
    Chan getAnaSrcB();

    DsoErr setLaSrcA( Chan chan );
    Chan getLaSrcA();

    DsoErr setLaSrcB( Chan chan );
    Chan getLaSrcB();

    DsoErr setFftSrc( Chan chan );
    Chan getFftSrc();

    //! cfg
    DsoErr setScaleRange();
    DsoErr setAutoScale( bool bReset );
    DsoErr setAutoOffset();
    DsoErr autoFftHoriScale();

    DsoErr autoScaleOffset();//! auto cfg offset

    DsoErr setAnaScale( qlonglong scale );
    qlonglong getAnaScale();

    DsoErr setViewOffset( qlonglong offset );
    qlonglong getAnaOffset();
    float     getfViewOffset(); //![dba, bug:524]

    qlonglong getADCOffset();

    DsoErr setLaScale( LaScale scale );
    LaScale getLaScale();

    DsoErr setLaOffset( int keyInc );
    qlonglong getLaOffset();

    DsoErr setTrendScale( qlonglong scale );
    qlonglong getTrendScale();

    DsoErr setTrendOffset();
    //! fft
    DsoErr setFftScale( qlonglong scale );
    qlonglong getFftScale();

    DsoErr    setFftOffset( qlonglong offset );
    qlonglong getFftOffset();

    DsoErr setFftHStart( qlonglong fStart );
    qlonglong getFftHStart();

    DsoErr setFftHEnd( qlonglong fEnd );
    qlonglong getFftHEnd();
    qlonglong getHStartEndStep();

    DsoErr setFftSpan( qlonglong span );
    qlonglong getFftSpan();

    DsoErr setFftCenter( qlonglong center );
    qlonglong getFftCenter();
    
    DsoErr setPeakEnable(bool offOn);
    bool getPeakEnable();

    DsoErr setPeakMaxPoints(int points);
    int getPeakMaxPoints();

    DsoErr setPeakThreshold(qlonglong threshold);
    qlonglong getPeakThreshold();

    DsoErr setPeakExcursion(qlonglong excursion);
    qlonglong getPeakExcursion();
    
    DsoErr setPeakOrder(enPeakOrder order);
    enPeakOrder getPeakOrder();

    DsoErr exportEventData();
    static DsoErr saveTable(QString fileName);

    DsoErr setVertExpand(VertExpand expandType);
    VertExpand getVertExpand();

    DsoErr setColorOnOff(bool onOff);
    bool   getColorOnOff();
    DsoErr resetColor();

    pointer getColorCurve();

    void  setPeak();
    void* getPeak();
    void  onZoomEn();
    void  onClear();
    void  unFreezeMath();
    bool  getZoomEn();
    bool  getZoomEnBefore();
    void  resetZoomEnBefore();

protected:
    float getFftHScale();
    float getFftHCenter();
    qlonglong getFftZoomHstart();
    qlonglong getFftZoomHScale();

    DsoErr setFftHResolution(int );
    float getFftHResolution();
    bool  isDbSpec( fftSpecUnit unit );

public:
    //! la
    DsoErr setThreshold_CH1( qlonglong thre );
    qlonglong getThreshold_CH1();

    DsoErr setThreshold_CH2(qlonglong thre );
    qlonglong getThreshold_CH2();

    DsoErr setThreshold_CH3(qlonglong thre );
    qlonglong getThreshold_CH3();

    DsoErr setThreshold_CH4(qlonglong thre );
    qlonglong getThreshold_CH4();

    DsoErr setThresholdSens( int sens );
    int getThresholdSens();

    //! Diff
    DsoErr setDiffWindow( int width );
    int getDiffWindow();

    //! Intg
    DsoErr setIntgBias( DsoReal bias );
    DsoReal getIntgBias();

    //! fft
    DsoErr setFftDcSuppress( bool b );
    bool getFftDcSuppress();

    DsoErr setFftWindow( fftWindow wnd );
    fftWindow getFftWindow();

    DsoErr setyUnit( fftSpecUnit yUnit );
    fftSpecUnit getyUnit();

    fftSpecUnit getyUnitRaw();

    DsoErr setxType( FftxType xType );
    FftxType getxType();

    DsoErr setScreen( FftScreen screen );
    FftScreen getFftScreen();
    static FftScreen getRealFftScreen();

    DsoErr setScpiScreen( bool screen );
    bool getScpiFftScreen();

    //! line opt
    DsoErr setLineA( DsoReal a );
    DsoReal getLineA();

    DsoErr setLineB( DsoReal b );
    DsoReal getLineB();

    //! filter
    DsoErr setFilterLp( int lp );
    int getFilterLp();

    DsoErr setFilterHp(int hp );
    int getFilterHp();

    DsoErr setFilterBp1( int bp1 );
    int getFilterBp1();

    DsoErr setFilterBp2( int bp2 );
    int getFilterBp2();

    DsoErr setFilterBt1( int bt1 );
    int getFilterBt1();

    DsoErr setFilterBt2(int bt2 );
    int getFilterBt2();

protected:
    qlonglong getViewScale();

    void setuiFilterAttr();

    Unit     getUnit();
    DsoErr   upDateUnit();
    QString  getUnitString();
    MathZone getZone();
    chZone   getDsoVertyZone();

    void updateFftSpanCenter();
    void updateFftStartEnd();

    RangeCfg updateVScale();
    opRange getOpRange( Chan ch );

    //! range check
    DsoErr limitRange( int valmin, int valmax, int &val);

    float getHSa(Chan ch);

    DsoErr    scpiSetFilterBw1(qlonglong Bw1);
    qlonglong scpiGetFilterBw1();
    DsoErr    scpiSetFilterBw2(qlonglong Bw2);
    qlonglong scpiGetFilterBw2();

    opRange   getOpRange_test( Chan ch );
    qlonglong getFftFreqStep(qlonglong val);

public:
    static void  resetData();//for bug3427

public:
    static int mathOn;
    static FftScreen mScreenFFT;
    static int mFFTCount;

    static mathThread *m_pWorkThread;


    bool mbNeedAutoScale; //! 在重新开关，切换运算符后的第一次结果要重新设置垂直方向上的档位和偏移
    bool mbNeedFftVertScale;
    MathPlotChanMode m_mathPlotChanMode;


protected:
    MathSrc mSrc;           //! src mgr
    MathCfg mCfg;           //! cfg mgr
    MathOptions mOption;    //! opt mgr
    
    fftPeakSearch mPeakCfg;
    MathOperator mOperator;

    bool mMathEn;
    AcquireTimemode mHorMode;

    bool mVertFineOn;
    bool mbColorOnOff;
    bool mbMathStarted;

    bool mInvert;
    VertExpand mVertExpand;
    bool mZoomEn;
    bool mZoomEnBefore;   //! 标记上一次绘图时的zoom状态

    RangeCfg mLaOffset;
    RangeCfg mViewOffset;   /*!< view offset */

    bool mLabelOn;          //! label cfg
    QString mLabel;

    CMathGrade* m_pMathGrade;

    qint64     m_nNormalChanA;
    qint64     m_nNormalChanB;
    qint64     m_nFFTChan;
    qint64     m_nLogicChanA;
    qint64     m_nLogicChanB;

friend class mathThread;
friend class mathWork;
};

/*!
 * \brief The mathThread class
 * math计算线程
 * - 4条math在一个线程中进行依次计算
 */
class mathThread : public QThread
{
    Q_OBJECT
public:
    mathThread( QObject *parent=NULL  );

    void attachClient( servMath *pMath );

    servMath*  getClient(int i=0);

public:
    void run();

    void disconnectClient( servMath *pMath );

    //for bug3425. by hxh
    static quint64 m_u64DelayStart;

private:
    int servClient( servMath * pMath );
    int servClientColor( servMath * pMath );

protected:
    QList < servMath * > mClients;

};

#endif
