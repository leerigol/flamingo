#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "servmath.h"

Unit servMath::getUnit()
{
    Unit unitA, unitB;
    Impedance imp;
    dsoVert *pVert;

    switch( mOperator )
    {
    //! arith
    case operator_add:
    case operator_sub:
        pVert = dsoVert::getCH( mSrc.mAnaSrc.mSrcA );
        unitA = pVert->getUnit();
        pVert = dsoVert::getCH( mSrc.mAnaSrc.mSrcB );
        unitB = pVert->getUnit();

        if ( unitA == unitB )
        {
            return unitA;
        }
        else
        {
            return Unit_U;
        }

    case operator_mul:
        pVert = dsoVert::getCH( mSrc.mAnaSrc.mSrcA );
        unitA = pVert->getUnit();
        pVert = dsoVert::getCH( mSrc.mAnaSrc.mSrcB );
        unitB = pVert->getUnit();

        if ( (unitA == Unit_V && unitB == Unit_A)
             || (unitA == Unit_A && unitB == Unit_V) )
        {
            return Unit_W;
        }
        else
        {
            return Unit_U;
        }

    case operator_div:
        pVert = dsoVert::getCH( mSrc.mAnaSrc.mSrcA );
        unitA = pVert->getUnit();
        pVert = dsoVert::getCH( mSrc.mAnaSrc.mSrcB );
        unitB = pVert->getUnit();
        if ( (unitA == Unit_W && unitB == Unit_A) )
        {
            return Unit_V;
        }
        else if ( (unitA == Unit_W && unitB == Unit_V) )
        {
            return Unit_A;
        }
        else
        {
            return Unit_U;
        }

    //! logic
    case operator_and: 
    case operator_or:
    case operator_xor:
    case operator_not:
        return Unit_Div;

    //! fft
    case operator_fft:
        if ( mOption.mFftOpt.myUnit == fft_spec_db )
        {
            pVert = dsoVert::getCH( mSrc.mFftSrc.mSrc );
            imp = pVert->getImpedance();
            unitA = pVert->getUnit();

            if ( imp == IMP_50 )
            {
                return Unit_dbm;
            }
            else if( unitA == Unit_V )
            {
                return Unit_dbV;
            }
            else
            {
                return Unit_db;
            }
        }
        else
        {
            return Unit_Vrms;
        }

    //! function
    case operator_intg:
        unitA = dsoVert::getCH( mSrc.mAnaSrc.mSrcA )->getUnit();
        if ( unitA == Unit_V )
        {
            return Unit_VmulS;
        }
        else
        {
            return Unit_U;
        }

    case operator_diff:
        unitA = dsoVert::getCH( mSrc.mAnaSrc.mSrcA )->getUnit();
        if ( unitA == Unit_V )
        {
            return Unit_VdivS;
        }
        else
        {
            return Unit_U;
        }

    case operator_root:
    case operator_lg:
    case operator_ln:
    case operator_exp:
        return Unit_U;

    case operator_abs:
    case operator_lp:
    case operator_hp:
    case operator_bp:
    case operator_bt:
    case operator_ax_b:
        unitA = dsoVert::getCH( mSrc.mAnaSrc.mSrcA )->getUnit();
        return unitA;
    case operator_trend:
        return Unit_s;

    default:
        return Unit_U;
    }
}

//! 在切换运算符或者运算源时，更新单位
DsoErr servMath::upDateUnit()
{
    if(getZone() == math_freq_zone)
    {
        setuiChange(MSG_MATH_FFT_SCALE);
        setuiChange(MSG_MATH_FFT_OFFSET);
    }
    else if(getZone() == math_logic_zone)
    {
        setuiChange(MSG_MATH_LOGIC_SCALE);
        setuiChange(MSG_MATH_LOGIC_OFFSET);
    }
    else
    {
        setuiChange(MSG_MATH_S32FUNCSCALE);
        setuiChange(MSG_MATH_VIEW_OFFSET);
    }
    return ERR_NONE;
}

QString servMath::getUnitString()
{
    return gui_fmt::CGuiFormatter::toString( getUnit() );
}

MathZone servMath::getZone()
{
    switch( mOperator )
    {
    case operator_fft:
        return math_freq_zone;

    case operator_and:
    case operator_or:
    case operator_not:
    case operator_xor:
        return math_logic_zone;

    default:
        return math_analog_zone;
    }
}

chZone servMath::getDsoVertyZone()
{
    switch( mOperator )
    {
    case operator_fft:
        return freq_zone;

    case operator_and:
    case operator_or:
    case operator_not:
    case operator_xor:
        return logic_zone;

    default:
        return time_zone;
    }
}

DsoErr servMath::limitRange( int valmin,
                             int valmax,
                             int &val)
{
    if ( val < valmin )
    {
        val = valmin;
        return ERR_OVER_LOW_RANGE;
    }
    else if ( val > valmax )
    {
        val = valmax;
        return ERR_OVER_UPPER_RANGE;
    }
    else
    {
        return ERR_NONE;
    }
}


