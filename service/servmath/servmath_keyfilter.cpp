
//#include "../../com/mathlib/mathlib.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"
//#include "../../com/fftlib/rfft.h"
//#include "../../com/mathlib/mathlib.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "servmath.h"


void servMath::fillArithKeyMap()
{
    //! offset
    _arithMap.append( encode_inc(R_Pkey_WAVE_POS),
                      MSG_MATH_VIEW_OFFSET,
                      R_Skey_TuneInc );

    _arithMap.append( encode_dec(R_Pkey_WAVE_POS),
                      MSG_MATH_VIEW_OFFSET,
                      R_Skey_TuneDec );

    _arithMap.append( (R_Pkey_WAVE_POS_Z),
                      MSG_MATH_VIEW_OFFSET,
                      R_Skey_Zval );
    //! scale
    _arithMap.append( encode_inc(R_Pkey_WAVE_VOLT),
                      MSG_MATH_S32FUNCSCALE,
                      R_Skey_TuneDec );

    _arithMap.append( encode_dec(R_Pkey_WAVE_VOLT),
                      MSG_MATH_S32FUNCSCALE,
                      R_Skey_TuneInc );

    _arithMap.append( (R_Pkey_WAVE_VOLT_Z),
                      servMath::cmd_vertScale_fineon,
                      R_Skey_Active );
}

void servMath::fillLaKeyMap()
{
    //! offset
    _laMap.append( encode_inc(R_Pkey_WAVE_POS),
                      MSG_MATH_LOGIC_OFFSET,
                      R_Skey_TuneInc );

    _laMap.append( encode_dec(R_Pkey_WAVE_POS),
                      MSG_MATH_LOGIC_OFFSET,
                      R_Skey_TuneDec );

    _laMap.append( (R_Pkey_WAVE_POS_Z),
                      MSG_MATH_LOGIC_OFFSET,
                      R_Skey_Zval );
    //! scale
    _laMap.append( encode_inc(R_Pkey_WAVE_VOLT),
                      MSG_MATH_LOGIC_SCALE,
                      R_Skey_TuneDec );

    _laMap.append( encode_dec(R_Pkey_WAVE_VOLT),
                      MSG_MATH_LOGIC_SCALE,
                      R_Skey_TuneInc );

    _laMap.append( (R_Pkey_WAVE_VOLT_Z),
                    servMath::cmd_vertScale_fineon,
                      R_Skey_Active );
}
void servMath::fillFftKeyMap()
{
    //! offset
    _fftMap.append( encode_inc(R_Pkey_WAVE_POS),
                      MSG_MATH_FFT_OFFSET,
                      R_Skey_TuneInc );

    _fftMap.append( encode_dec(R_Pkey_WAVE_POS),
                      MSG_MATH_FFT_OFFSET,
                      R_Skey_TuneDec );

    _fftMap.append( (R_Pkey_WAVE_POS_Z),
                      MSG_MATH_FFT_OFFSET,
                      R_Skey_Zval );

    //! scale
    _fftMap.append( encode_inc(R_Pkey_WAVE_VOLT),
                      MSG_MATH_FFT_SCALE,
                      R_Skey_TuneDec );

    _fftMap.append( encode_dec(R_Pkey_WAVE_VOLT),
                      MSG_MATH_FFT_SCALE,
                      R_Skey_TuneInc );

    _fftMap.append( (R_Pkey_WAVE_VOLT_Z),
                     servMath::cmd_vertScale_fineon,
                      R_Skey_Active );
}


bool servMath::keyTranslate(int key,
               int &msg,
               int &controlAction)
{
    if ( !getMathEn() ) return false;

    MathZone zone = getZone();
    if ( zone == math_logic_zone )
    {
        return servKeyMapFilter( servMath::_laMap,
                          key, msg, controlAction );
    }
    else if ( zone == math_freq_zone )
    {
        return servKeyMapFilter( servMath::_fftMap,
                          key, msg, controlAction );
    }
    else
    {
        return servKeyMapFilter( servMath::_arithMap,
                          key, msg, controlAction );
    }

    return true;
}

bool servMath::servKeyMapFilter(  serviceKeyMap &keyMap,
                        int key,
                        int &msg,
                        int &controlAction)
{
    serviceKey servKey;
    if ( keyMap.find( key, servKey) )
    {
        msg = servKey.mMsg;
        controlAction = servKey.mAction;

        return true;
    }

    return false;
}

void servMath::setInputValid(bool valid)
{
    m_bInPutValid = valid;
}

bool servMath::getInputValid()
{
    return m_bInPutValid;
}
