#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"
#include "../../meta/crmeta.h"
#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "servmath.h"
#include <QThread>

float servMath::getHSa(Chan ch)
{
    CArgument argI;
    argI.setVal((int)ch,0);
    argI.setVal(horizontal_view_main, 1);
    CArgument argO;
    serviceExecutor::query(serv_name_trace, servTrace::cmd_query_trace_Sa, argI,argO);
    qlonglong llSa = 0;
    argO.getVal(llSa,0);

    float sa;
    sa = llSa * sa_unit;
    return sa;
}
