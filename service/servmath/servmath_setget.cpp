
#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

//#include "../../com/mathlib/mathlib.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"
//#include "../../com/fftlib/rfft.h"
//#include "../../com/mathlib/mathlib.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"
#include "../../service/servplot/servplot.h"
#include "../../arith/scalestep.h"
#include "servmath.h"

DsoErr servMath::setDataProvider( void *ptr )
{
    Q_UNUSED(ptr);
    if ( mDataSema.available() < 1 )
    {
        mDataSema.release();
    }
    return ERR_NONE;
}

DsoErr servMath::releaseDataSema()
{
    if ( mDataSema.available() < 1)
    {
        mDataSema.release();
    }
    return ERR_NONE;
}

DsoErr servMath::setJitterProvider(void/* *ptr */)
{
    if ( mJitterSema.available() < 1)
    {
        mJitterSema.release();
    }

    return ERR_NONE;
}

/*!
 * \brief servMath::getExpression
 * \return
 * 显示用的计算表达，显示的表达式和实际使用的表达式有分别，
 * 实际的表达式更严格
 */
QString servMath::getExpression()
{
    switch( mOperator )
    {
    //! arith
    case operator_add:
        return toString(mSrc.mAnaSrc.mSrcA)
                + "+" + toString(mSrc.mAnaSrc.mSrcB);

    case operator_sub:
        return toString(mSrc.mAnaSrc.mSrcA)
                + "-" + toString(mSrc.mAnaSrc.mSrcB);

    case operator_mul:
        return toString(mSrc.mAnaSrc.mSrcA)
                + "*" + toString(mSrc.mAnaSrc.mSrcB);

    case operator_div:
        return toString(mSrc.mAnaSrc.mSrcA)
                + "/" + toString(mSrc.mAnaSrc.mSrcB);

    //! logic
    case operator_and:
        return toString(mSrc.mLaSrc.mSrcA)
                + "&&" + toString(mSrc.mLaSrc.mSrcB);

    case operator_or:
        return toString(mSrc.mLaSrc.mSrcA)
                + "||" + toString(mSrc.mLaSrc.mSrcB);

    case operator_xor:
        return toString(mSrc.mLaSrc.mSrcA)
                + "^" + toString(mSrc.mLaSrc.mSrcB);

    case operator_not:
        return "!" + toString(mSrc.mLaSrc.mSrcA);

    //! fft
    case operator_fft:
        return "fft(" + toString(mSrc.mFftSrc.mSrc) + ")";

    //! function
    case operator_intg:
        return "intg(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_diff:
        return "diff(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_root:
        return "sqrt(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_lg:
        return "lg(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_ln:
        return "ln(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_exp:
        return "exp(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_abs:
        return "abs(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_lp:
        return "lp(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_hp:
        return "hp(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_bp:
        return "bp(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_bt:
        return "bt(" + toString(mSrc.mAnaSrc.mSrcA) + ")";

    case operator_ax_b:
        return "a*" + toString(mSrc.mAnaSrc.mSrcA) + " + b";

    case operator_trend:
        return "trend";

    default:
        return "";
    }
}
/*!
 * \brief servMath::getCalcExpression
 * \return
 * 实际的计算表达式
 * 都带有括号，以便进行级联，例如: m2 = m1 + ch1de
 */
QString servMath::getCalcExpression()
{
    switch( mOperator )
    {
    //! arith
    case operator_add:
        return "(" + toCalcString(mSrc.mAnaSrc.mSrcA)
                + "+" + toCalcString(mSrc.mAnaSrc.mSrcB) + ")";

    case operator_sub:
        return "(" + toCalcString(mSrc.mAnaSrc.mSrcA)
                + "-" + toCalcString(mSrc.mAnaSrc.mSrcB) + ")";

    case operator_mul:
        return "(" + toCalcString(mSrc.mAnaSrc.mSrcA)
                + "*" + toCalcString(mSrc.mAnaSrc.mSrcB) + ")";

    case operator_div:
        return "(" + toCalcString(mSrc.mAnaSrc.mSrcA)
                + "/" + toCalcString(mSrc.mAnaSrc.mSrcB) + ")";

    //! logic
    case operator_and:
        return "(land(" + toDString(mSrc.mLaSrc.mSrcA)
                + "," + toDString(mSrc.mLaSrc.mSrcB) + "))";

    case operator_or:
        return "(lor(" + toDString(mSrc.mLaSrc.mSrcA)
                + "," + toDString(mSrc.mLaSrc.mSrcB) + "))";

    case operator_xor:
        return "(lxor(" + toDString(mSrc.mLaSrc.mSrcA)
                + "," + toDString(mSrc.mLaSrc.mSrcB) + "))";

    case operator_not:
        return "(lnot(" + toDString(mSrc.mLaSrc.mSrcA) + "))";

    //! fft
    case operator_fft:
        return "(fft(" + toCalcString(mSrc.mFftSrc.mSrc) + "))";

    //! function
    case operator_intg:
        return "(intg(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_diff:
        return "(diff(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_root:
        return "(root(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_lg:
        return "(lg(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_ln:
        return "(ln(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_exp:
        return "(exp(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_abs:
        return "(abs(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_lp:
        return "(lp(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_hp:
        return "(hp(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_bp:
        return "(bp(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_bt:
        return "(bt(" + toCalcString(mSrc.mAnaSrc.mSrcA) + "))";

    case operator_ax_b:
        return "(a*" + toCalcString(mSrc.mAnaSrc.mSrcA) + " + b)";

    default:
        return "";
    }
}

/*!
 * \brief servMath::toString
 * \param ch
 * \return
 * 输入通道转换为字符名称
 */
QString servMath::toString( Chan ch )
{
    if ( ch >= chan1 && ch <= chan4 )
    {
        return gui_fmt::CGuiFormatter::toString( ch );
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        return gui_fmt::CGuiFormatter::toString( ch );
    }
    else if ( ch >= r1 && ch <= r10 )
    {
        return gui_fmt::CGuiFormatter::toString( ch );
    }
    //! cascade ch
    else if ( ch >= m1 && ch <= m4 )
    {
        return gui_fmt::CGuiFormatter::toString( ch );
        //! check recurrisive
//        if ( (getId() - E_SERVICE_ID_MATH1) == (ch-m1) )
//        {
//            Q_ASSERT( false );
//        }

//        QString str;
//        serviceExecutor::query( (ServiceId)(E_SERVICE_ID_MATH1 + ch-m1),
//               servMath::cmd_expr,
//               str );

//        return str;
    }
    else
    {
        return "";
    }
}

QString servMath::toCalcString( Chan ch )
{
    if ( ch >= chan1 && ch <= chan4 )
    {
        return QString("CH%1").arg( ch-chan1 + 1 );
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        return QString("D%1").arg( ch-d0 );
    }
    else if ( ch >= r1 && ch <= r10 )
    {
        return QString("R%1").arg( ch-r1+1 );
    }
    //! cascade ch
    else if ( ch >= m1 && ch <= m4 )
    {
        //! check recurrisive
        if ( (getId() - E_SERVICE_ID_MATH1) == (ch-m1) )
        {
            Q_ASSERT( false );
        }

        QString str;
        serviceExecutor::query( (ServiceId)(E_SERVICE_ID_MATH1 + ch-m1),
               servMath::cmd_calc_expr,
               str );

        return str;
    }
    else
    {
        return "";
    }
}

/*!
 * \brief servMath::toDString
 * \param ch
 * \return
 * 输入通道数字名称
 * - 数字名称指的是，经过数字化后的通道名称
 * - 在进行计算时，数字化后的通道数据和原始数据不同
 */
QString servMath::toDString( Chan ch )
{
    if ( ch >= chan1 && ch <= chan4 )
    {
        return QString("DC%1").arg( ch-chan1 + 1 );
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        return QString("D%1").arg( ch-d0 );
    }
    else if ( ch >= r1 && ch <= r10 )
    {
        return QString("DR%1").arg( ch-r1+1 );
    }
    //! cascade ch
    else if ( ch >= m1 && ch <= m4 )
    {
        //! check recurrisive
        if ( (getId() - E_SERVICE_ID_MATH1) == (ch-m1) )
        {
            Q_ASSERT( false );
        }

        QString str;
        serviceExecutor::query( (ServiceId)(E_SERVICE_ID_MATH1 + ch-m1),
               servMath::cmd_calc_expr,
               str );

        return str;
    }
    else
    {
        return "";
    }
}

float servMath::getRealScale()
{
    MathZone zone = getZone();

    if  ( zone == math_analog_zone )
    {
        return getAnaScale() * math_v_base;
    }
    else if ( zone == math_logic_zone )
    {
        LaScale laScale = getLaScale();

        switch( laScale )
        {
        case Large:
            return 0.5f;

        case Medium:
            return 1.0f;

        case Small:
            return 2.0f;

        default:
            return 1.0f;
        }
    }
    else if ( zone == math_freq_zone )
    {
        return getFftScale() * math_v_base;
    }
    else
    {}

    return getAnaScale() * math_v_base;
}

float servMath::getRealOffset()
{
    MathZone zone = getZone();
    if ( zone != math_logic_zone )
    {
        return getADCOffset() * getRealScale() / adc_vdiv_dots;
    }
    else
    {
        return (float)getADCOffset() / adc_vdiv_dots;
    }
}

float servMath::getRealFftPeakThres()
{
    return getPeakThreshold() * math_v_base;
}

bool servMath::getViewInvert()
{
    MathZone zone;

    zone = getZone();
    if ( math_freq_zone == zone
         || math_logic_zone == zone )
    {
        return false;
    }
    else
    {
        return mInvert;
    }
}

DsoErr servMath::setOperator( MathOperator oper )
{
    if( mMathEn )
    {
        if( mOperator != operator_fft &&
                 oper == operator_fft )
        {
            mFFTCount++;
        }
        else if(oper != operator_fft &&
                mOperator == operator_fft )
        {
            Q_ASSERT(mFFTCount > 0);
            mFFTCount--;
            if(getFftScreen() == fft_screen_half)
            {
                ssync( MSG_MATH_S32FFTSCR, fft_screen_full );
            }
        }
        mbNeedAutoScale = true;
    }

    mOperator = oper;

    upDateUnit();
    releaseDataSema();
    changeSrcMenuEn();
    dsoVert::setyZone( getDsoVertyZone() );
    dsoVert::setyUnit( getUnit() );
    return ERR_NONE;
}

MathOperator servMath::getOperator()
{
    return mOperator;
}
/*!
 * \brief servMath::setEnable
 * \param bEn
 * \return
 * 根据打开状态启动MATH线程
 * \note 所有的MATH使用一个线程
 */
DsoErr servMath::setMathEn( bool bEn )
{
    if( !mMathEn && bEn )
    {
        if( mbMathStarted )
        {
            mbNeedAutoScale = true;
            if( mOperator == operator_fft )
            {
                mOption.mFftOpt.mFFtAutoScale = true;
            }
        }
    }

    int index = getId() - E_SERVICE_ID_MATH1;
    if(bEn)
    {
        mathOn |= (1 << index);
    }
    else
    {
        mathOn &= ~(1 << index);
    }

    if(!bEn && mbMathStarted)//bug3427 by hxh
    {
        if( mOperator == operator_fft && mFFTCount > 0)
        {
            mFFTCount--;
        }
    }

    if(bEn)
    {
        if( mOperator == operator_fft )
        {
            mFFTCount++;
        }
    }

    mMathEn = bEn;

    syncEngine( mathOn > 0 ? ENGINE_LED_ON : ENGINE_LED_OFF,
                DsoEngine::led_math);

    mUiAttr.setEnable(MSG_MATH_S32SHOWLABEL, mMathEn);
    if ( mMathEn  )
    {
        Q_ASSERT( NULL != servMath::m_pWorkThread );
        servMath::m_pWorkThread->start(QThread::NormalPriority);
    }
    else
    {
        setuiChange(MSG_MATH_S32SHOWLABEL);
    }

    plot(bEn);
    releaseDataSema();
    dsoVert::setyOnOff( bEn );
    dsoVert::setyUnit(getUnit());
    return ERR_NONE;
}

bool servMath::getMathEn()
{
    return mMathEn;
}

DsoErr servMath::changeSrcMenuEn()
{
    if( getDsoVertyZone() == time_zone )
    {
        for ( int i = 0; i < 10; i++ )
        {
            if( dsoVert::getCH( Chan (r1+i) )->getOnOff() )
            {
                mUiAttr.setEnable( MSG_MATH_S32ARITHA, r1 + i, true );
                mUiAttr.setEnable( MSG_MATH_S32ARITHB, r1 + i, true );
            }
            else
            {
                mUiAttr.setEnable( MSG_MATH_S32ARITHA, r1 + i, false );
                mUiAttr.setEnable( MSG_MATH_S32ARITHB, r1 + i, false );
            }

            findChanA( Chan (r1+i), dsoVert::getCH( Chan (r1+i) )->getOnOff() );
            findChanB( Chan (r1+i), dsoVert::getCH( Chan (r1+i) )->getOnOff() );
        }
        //! m1/2/3/4 disable
        for (int i = 0; (i + (int) E_SERVICE_ID_MATH1) < getId(); i++ )
        {
            if( dsoVert::getCH( Chan (m1+i) )->getOnOff() )
            {
                mUiAttr.setEnable( MSG_MATH_S32ARITHA, m1 + i, true );
                mUiAttr.setEnable( MSG_MATH_S32ARITHB, m1 + i, true );
            }
            else
            {
                mUiAttr.setEnable( MSG_MATH_S32ARITHA, m1 + i, false );
                mUiAttr.setEnable( MSG_MATH_S32ARITHB, m1 + i, false );
            }

            findChanA( Chan (m1+i), dsoVert::getCH( Chan (m1+i) )->getOnOff() );
            findChanB( Chan (m1+i), dsoVert::getCH( Chan (m1+i) )->getOnOff() );
        }
    }
    else if( getDsoVertyZone() == freq_zone )
    {
//        for ( int i = 0; i < 10; i++ )
//        {
//            mUiAttr.setEnable( MSG_MATH_S32ARITHA, r1 + i, false );
//            mUiAttr.setEnable( MSG_MATH_S32ARITHB, r1 + i, false );
//        }
//        //! m1/2/3/4 disable
//        for (int i = 0; i < 4; i++ )
//        {
//            mUiAttr.setEnable( MSG_MATH_S32ARITHA, m1 + i, false );
//            mUiAttr.setEnable( MSG_MATH_S32ARITHB, m1 + i, false );
//        }
    }
    return ERR_NONE;
}

void servMath::plot(bool en)
{
    if(!en && m_pWorkThread)
    {
        m_pWorkThread->disconnectClient(this);
    }
}

DsoErr servMath::setInvert( bool bInv )
{
    mInvert = bInv;
    releaseDataSema();
    return ERR_NONE;
}
bool servMath::getInvert()
{
    return mInvert;
}

DsoErr servMath::setViewOffset( qlonglong offset )
{
    mViewOffset.mVal = offset;

    releaseDataSema();
    dsoVert::setyRefOffset( offset );
    dsoVert::setyGnd( adc_center + offset * adc_vdiv_dots / getYRefScale() );
    return ERR_NONE;
}

//![dba+, bug:524]
float servMath::getfViewOffset()
{
    return mViewOffset.mVal/(float)(1E6)/(1E3); //nf
}

qlonglong servMath::getADCOffset()
{
    setuiZVal( 0ll );

    MathZone zone = getZone();
    if ( zone == math_freq_zone )
    {
        return round(getFftOffset()/(getFftScale()*1.0/adc_vdiv_dots));
    }
    else if ( zone == math_logic_zone )
    {
        return getLaOffset();
    }
    else
    {
        return round(getAnaOffset()/(getAnaScale()*1.0/adc_vdiv_dots));
    }
}

DsoErr servMath::setLabelOn( bool b )
{
    mLabelOn = b;
    return ERR_NONE;
}
bool servMath::getLabelOn()
{
    return mLabelOn;
}

DsoErr servMath::setLabelIndex( int index )
{
    if ( index < 0 || index >= servMath::_labelList.size() )
    { return ERR_OVER_RANGE; }

    async( MSG_MATH_S32LABELEDIOR, servMath::_labelList[index] );

    return ERR_NONE;
}

int servMath::getLabelIndex()
{
    int index;

    index = servMath::_labelList.indexOf( mLabel );
    if ( index < 0 )
    { return 0; }
    else
    { return index; }
}

DsoErr servMath::setLabel( QString str )
{
    mLabel = str;
    return ERR_NONE;
}

QString servMath::getLabel()
{
    setuiMaxLength(22);
    return mLabel;
}

DsoErr servMath::setVertFineOn(bool fineOn)
{
    Q_UNUSED(fineOn);
    mVertFineOn = !mVertFineOn;
    return ERR_NONE;
}

bool servMath::getVertFineOn()
{
    return mVertFineOn;
}

qlonglong servMath::getViewScale()
{
    MathZone zone = getZone();
    if ( zone == math_freq_zone )
    {
        return getFftScale();
    }
    else if ( zone == math_logic_zone )
    {
        switch( getLaScale() )
        {
        case Large:
            return 500000000ll;

        case Medium:
            return 1000000000ll;

        case Small:
            return 2000000000ll;

        default:
            return 1000000000ll;
        }
    }
    else
    {
        return getAnaScale();
    }
}
