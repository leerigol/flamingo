#include "servmath.h"
#include "../serveyejit/serveyejit.h"
#include "../../include/dsoarith.h"

//! 最小有5个档位可以调节
#define DEFAULT_SCALE_SPAN      5

RangeCfg servMath::updateVScale()
{
    opRange rangeA, rangeB, rangeC;

    //! rangeA, B
    rangeA = getOpRange( mSrc.mAnaSrc.mSrcA );
    rangeB = getOpRange( mSrc.mAnaSrc.mSrcB );

    //! context
    opIntgContext intgContext;
    opDiffContext diffContext;
    opAxbContext  axbContext;

    horiAttr hAttr;

    //! chan hori attr
    hAttr = getHoriAttr( chan1, horizontal_view_main );

    switch( mOperator )
    {
        //! arith
        case operator_add:
            math_arith_add( &rangeA,
                            &rangeB,
                            &rangeC );
        break;
        case operator_sub:
            math_arith_sub( &rangeA,
                            &rangeB,
                            &rangeC );
        break;
        case operator_mul:
            math_arith_mul( &rangeA,
                            &rangeB,
                            &rangeC );
        break;

        case operator_div:
            math_arith_div( &rangeA,
                            &rangeB,
                            &rangeC );
        break;

        //! function
        case operator_intg:
            //! context
            mOption.mIntgOpt.mBias.toReal( intgContext.bias, 0.0f );

            //! user screen tInc
            intgContext.size = wave_width;
            intgContext.tInc = hAttr.inc;

            math_func_intg( &rangeA,
                            &rangeC,
                            &intgContext
                            );
        break;

        case operator_diff:
            //! context
            diffContext.tInc = hAttr.inc;

            math_func_diff( &rangeA,
                            &rangeC,
                            &diffContext
                            );
        break;

        case operator_abs:
            math_func_abs( &rangeA, &rangeC );
        break;

        case operator_exp:
            math_func_exp( &rangeA, &rangeC );
        break;

        case operator_lg:
            math_func_lg( &rangeA, &rangeC );
        break;

        case operator_ln:
            math_func_ln( &rangeA, &rangeC );
        break;

        case operator_root:
            math_func_root( &rangeA, &rangeC );
        break;

        case operator_lp:
        case operator_hp:
        case operator_bp:
        case operator_bt:
            math_func_filter( &rangeA, &rangeC);
        break;

        case operator_ax_b:
            //! context
            mOption.mLineOpt.a.toReal( axbContext.a);
            mOption.mLineOpt.b.toReal( axbContext.b);
            math_func_axb( &rangeA, &rangeC, &axbContext );
        break;

        //! fft
        case operator_fft:
            if ( mOption.mFftOpt.myUnit == fft_spec_db )
            {
                math_freq_fft_db( &rangeA, &rangeC );
            }
            else if ( mOption.mFftOpt.myUnit == fft_spec_dbm )
            {
                math_freq_fft_dbm( &rangeA, &rangeC );
            }
            else if ( mOption.mFftOpt.myUnit == fft_spec_rms )
            {
                math_freq_fft_vrms( &rangeA, &rangeC );
            }
            else
            { Q_ASSERT(false); }
        break;

        case operator_trend:
        {
            CArgument argo;
            serviceExecutor::query(serv_name_eyejit, servEyeJit::cmd_jitter_MaxMin,argo);
            math_trend(argo[0].fVal, argo[1].fVal, &rangeC);
        }
        break;

        default:
            Q_ASSERT( false );
        break;
    }

    //! to scale range
    qlonglong normMin, normMax;

    float currentBase;
    if(mOperator == operator_trend)
    {
        currentBase = 1e-12;
    }
    else
    {
        currentBase = math_v_base;
    }

    //! to protect overflow
    if ( rangeC.minV < currentBase )
    { rangeC.minV = currentBase; }
    else if ( rangeC.minV > __INT64_MAX__ / currentBase )
    { rangeC.minV = __INT64_MAX__ / currentBase; }
    else
    {}

    if ( rangeC.maxV < currentBase )
    { rangeC.maxV = currentBase; }
    else if ( rangeC.maxV > __INT64_MAX__ / currentBase )
    { rangeC.maxV = __INT64_MAX__ / currentBase; }
    else
    {}

    //! can not overflow now
    normMin = rangeC.minV / currentBase;
    normMax = rangeC.maxV / currentBase;

    //! norm scale
    normMin = ll_floor125( normMin );
    normMax = ll_floor125( normMax );

    //! now cfg range
    RangeCfg cfg;
    cfg.mMin = normMin;
    cfg.mMax = normMax;

    //! auto set to medium
    int seqMin, seqMax;
    seqMin = norm125ToSeq( cfg.mMin );
    seqMax = norm125ToSeq( cfg.mMax );

    cfg.mZVal = seqToNorm125( (seqMin + seqMax )/2 );

    //! 如果档位范围相等，则调整档位范围
    if ( seqMax == seqMin )
    {
        int left, right;
        left = seqMin - DEFAULT_SCALE_SPAN/2;
        right = seqMin + DEFAULT_SCALE_SPAN - DEFAULT_SCALE_SPAN/2;

        Q_ASSERT( DEFAULT_SCALE_SPAN <=
                  (__MAX_125_64_SEQ__ - __MIN_125_64_SEQ__ + 1) );

        //! 计算左右档位的边界
        if ( left < __MIN_125_64_SEQ__ )
        {
            left = __MIN_125_64_SEQ__;
            right = left + (DEFAULT_SCALE_SPAN - 1);
        }

        if ( right > __MAX_125_64_SEQ__ )
        {
            right = __MAX_125_64_SEQ__;
            left = right - (DEFAULT_SCALE_SPAN + 1);
        }

        //! 设置档位范围
        cfg.mMin = seqToNorm125( left );
        cfg.mMax = seqToNorm125( right );
    }
    else
    {}

    return cfg;
}

//! 最小档位为ADC的分辨率，最大为3格表示的电压范围
opRange servMath::getOpRange( Chan ch )
{
    opRange range;

    if ( (ch >= chan1 && ch <= chan4) || (ch >= m1 && ch <= m4) )
    {
        range.minV = dsoVert::getCH( ch )->getyInc() * dsoVert::getCH( ch )->getProbe().toFloat();
        range.maxV = range.minV * adc_vdiv_dots * pref_view_div / 2;
    }
    else
    {
        range.minV = 0.1;
        range.maxV = 1.0;
        //qWarning()<<"invalid range"<<__FUNCTION__;
    }

    return range;
}
