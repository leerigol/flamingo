#ifndef MATH_OPERS
#define MATH_OPERS

#include "mathoper.h"
#include "mathoperarith.h"
#include "mathoperfunc.h"
#include "mathoperlogic.h"
#include "mathoperfft.h"

#include "operator_stub.h"

#endif // MATH_OPERS

