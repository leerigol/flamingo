#ifndef MATHOPER_H
#define MATHOPER_H

#include "../../../include/dsotype.h"
#include "../../baseclass/dsowfm.h"

#include "operator_stub.h"

using namespace DsoType;
//! base class for op
class MathOper
{
public:
    MathOper( MathOperator op );

public:
    DsoErr calc( DsoWfm &wfmA,
                 DsoWfm &wfmB,
                 DsoWfm *pWfmC,
                 opContext *pContext );

    DsoErr calc( DsoWfm &wfmA,
                 DsoWfm *pWfmC,
                 opContext *pContext );

    virtual DsoErr op(
                     DsoWfm &wfmA,
                     DsoWfm &wfmB,
                     int from, int len,
                     DsoWfm *pWfmC,
                     opContext *pContext );

    virtual DsoErr op( DsoWfm &pWfm,
                     int from, int len,
                     DsoWfm *pWfmOut,
                     opContext *pContext );

protected:
    //! return length
    int rangeCheck( DsoWfm &wfmA,
                     DsoWfm &wfmB,
                     int &a );

protected:
    MathOperator mOperator;

};


#endif // MATHOPER_H
