#include "../../include/dsodbg.h"
#include "matherr.h"
#include "mathoper.h"
#include "operator_stub.h"
MathOper::MathOper( MathOperator op )
{
    mOperator = op;
}

DsoErr MathOper::calc( DsoWfm &wfmA,
             DsoWfm &wfmB,
             DsoWfm *pWfmC,
             opContext *pContext )
{
    Q_ASSERT( NULL != pWfmC );

    //! check range
    int a;
    int len = rangeCheck( wfmA,
                          wfmB,
                          a );

    if ( len <= 0 )
    {
        pWfmC->setRange(1000,0);
        return math_err_warning;
    }

    if ( wfmA.getllt0() != wfmB.getllt0() )
    {
        pWfmC->setRange(1000,0);
        return math_err_warning;
    }

    if ( wfmA.getlltInc() != wfmB.getlltInc() )
    {
        pWfmC->setRange(1000,0);
        return math_err_warning;
    }

    return op( wfmA, wfmB, a, len, pWfmC, pContext );
}

DsoErr MathOper::calc( DsoWfm &wfmA,
                       DsoWfm *pWfmC,
                       opContext *pContext )
{
    int a, len;

    a = wfmA.start();
    len = wfmA.size();

    if ( len <= 0 )
    {
        qDebug()<<"trace is empty";
        pWfmC->setRange(1000,0);
        return ERR_OVER_RANGE;
    }
    LOG_DBG()<<wfmA.size()<<wfmA.getlltInc();
    return op( wfmA, a, len, pWfmC, pContext );
}

DsoErr MathOper::op( DsoWfm &/*wfmA*/,
                 DsoWfm &/*wfmB*/,
                 int /*from*/, int /*len*/,
                 DsoWfm */*pWfmC*/,
                 opContext */*pContext*/)
{
    Q_ASSERT( false );
    return ERR_INVALID_CONFIG;
}

DsoErr MathOper::op( DsoWfm &/*pWfm*/,
                 int /*from*/, int /*len*/,
                 DsoWfm */*pWfmOut*/,
                 opContext */*pContext*/    )
{
    Q_ASSERT( false );
    return ERR_INVALID_CONFIG;
}

//! a - start
//! return - length >=0
int MathOper::rangeCheck( DsoWfm &wfmA,
                 DsoWfm &wfmB,
                 int &a )
{
    int b;
    int aL, aLen;
    int bL, bLen;

    aL = wfmA.start();
    aLen = wfmA.size();

    bL = wfmB.start();
    bLen = wfmB.size();

    //! left
    a = aL < bL ? bL : aL;
    //! right
    b = ( aL + aLen ) < (bL+bLen) ? (a+aLen) : (bL + bLen);

    //! range
    if ( ( b - a ) <= 0 )
    {
        return 0;
    }
    else
    { return (b-a); }
}
