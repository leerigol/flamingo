#include <QtAlgorithms>
#include "../../include/dsodbg.h"
#include "fftpeaksearch.h"
#include "../../include/dsocfg.h"

fftPeakSearch::fftPeakSearch()
{
    m_peakEnable = false;

    m_peakMaxCount.mMax = 15;
    m_peakMaxCount.mMin = 1;
    m_peakMaxCount.mVal = 5;

    m_thresHold = 5.5*math_v;
    m_excursion = 1.8*math_v;
    peakOrder = fft_peak_AmpOrder;
}

void fftPeakSearch::peakAction(DsoWfm& wfm, qlonglong startFreq, qlonglong endFreq)
{
    m_stMeasPeakManage.stCurrentMaxPeak.ampl = MEAS_PEAK_MAX_AMPL; 	//初始化当前最大值幅度,以进行查找
    m_stMeasPeakManage.stCurrentMaxPeak.s32Index = MEAS_PEAK_MAX_INDEX;	//初始化一个不会取到的值，防止在NextPeak重复

    if( wfm.size() != 0)
    {
        m_wfmFreq = wfm;
    }
    else
    {
        return;
        //Q_ASSERT(false);
    }

    findPeak( (startFreq - m_wfmFreq.getllt0() ) / m_wfmFreq.getlltInc(),
              (endFreq - m_wfmFreq.getllt0() ) / m_wfmFreq.getlltInc() );

    m_stPeakTable.m_peakPoint.clear();
    m_stPeakTable.m_peakPoint.reserve(m_stPeakTable.s32PeakTableCount);

    measPeakStru peakTemp;
    //! 这里没有采用排序算法的原因是：
    //! 当搜索结果数据量较少时，运行时间可以忽略不计
    //! 当搜索结果数据量比较大的时候，排序算法时间复杂度为O(nlogn)，而直接找前面最大结果时间复杂度为O(n),即15*n
    float lastMaxVal = 1e99;
    for(int i = 0; i < m_peakMaxCount.mVal && i < m_stPeakTable.s32PeakTableCount; i++)
    {
        float tempMaxVal = -1e20;
        int indexMaxVal = 0;
        for(int j = 0;j < m_stPeakTable.s32PeakTableCount; j++)
        {
            if( wfm.valueAt( m_stPeakTable.s32PeakTableIndex[j] ) > tempMaxVal &&
                    wfm.valueAt( m_stPeakTable.s32PeakTableIndex[j] ) < lastMaxVal )
            {
                qlonglong freq = m_stPeakTable.s32PeakTableIndex[j] *
                        m_wfmFreq.getlltInc() + m_wfmFreq.getllt0();

                if( freq >= startFreq && freq <= endFreq )
                {
                    tempMaxVal = wfm.valueAt( m_stPeakTable.s32PeakTableIndex[j] );
                    indexMaxVal = m_stPeakTable.s32PeakTableIndex[j];
                }
            }
        }

        if( indexMaxVal != 0 )
        {
            peakTemp.s32Index = indexMaxVal;
            peakTemp.ampl = tempMaxVal;
            peakTemp.freq = indexMaxVal * m_wfmFreq.getlltInc() + m_wfmFreq.getllt0();
            m_stPeakTable.m_peakPoint.push_back(peakTemp);
            lastMaxVal = tempMaxVal;
        }
        else
        {
            //qDebug()<<__FUNCTION__<<__LINE__<<"find peak fail";
        }
    }


    m_peakBuffer.peakDataMutex.lock();

    m_peakBuffer.pointBuffer.clear();
    if(peakOrder == fft_peak_AmpOrder)
    {
        m_peakBuffer.pointBuffer = m_stPeakTable.m_peakPoint;
    }
    else
    {
      for( int i = 0;i < m_stPeakTable.s32PeakTableCount &&
           m_peakBuffer.pointBuffer.size() < m_peakMaxCount.mVal;i++)
      {
          qlonglong freq = m_stPeakTable.s32PeakTableIndex[i] *
                  m_wfmFreq.getlltInc() + m_wfmFreq.getllt0();


          if( freq >= startFreq && freq <= endFreq )
          {
              float tempMaxVal = wfm.valueAt( m_stPeakTable.s32PeakTableIndex[i] );
              qlonglong indexMaxVal = m_stPeakTable.s32PeakTableIndex[i];

              peakTemp.s32Index = indexMaxVal;
              peakTemp.ampl = tempMaxVal;
              peakTemp.freq = indexMaxVal * m_wfmFreq.getlltInc() + m_wfmFreq.getllt0();

              m_peakBuffer.pointBuffer.push_back( peakTemp );
          }
      }
//        double lastMinVal = -1e99;
//        for(int i = 0; i < m_stPeakTable.m_peakPoint.size(); i++)
//        {
//            double tempMinVal = 1e99;
//            int tempIndex = 0;

//            for(int j = 0;j < m_stPeakTable.m_peakPoint.size(); j++)
//            {
//                if( m_stPeakTable.m_peakPoint.at(j).freq < tempMinVal
//                        && m_stPeakTable.m_peakPoint.at(j).freq > lastMinVal )
//                {
//                    tempMinVal = m_stPeakTable.m_peakPoint.at(j).freq;
//                    tempIndex = j;
//                }
//            }
//            m_peakBuffer.pointBuffer.push_back( m_stPeakTable.m_peakPoint.at(tempIndex) );
//            lastMinVal = tempMinVal;
//        }
    }
    m_peakBuffer.peakDataMutex.unlock();
}

void fftPeakSearch::findPeak(int startIndex, int endIndex)
{   
    if( startIndex < 0 )
    {
        startIndex = 0;
    }

    if( startIndex > m_wfmFreq.size() )
    {
        startIndex = m_wfmFreq.size();
    }

    if( endIndex < 0 )
    {
        endIndex = 0;
    }

    if( endIndex > m_wfmFreq.size() )
    {
        endIndex = m_wfmFreq.size();
    }

     bool   bFound = false;
     double f64PeakAmp = 0;			//显示Trace数组极大幅值
     int    s32PeakIndex = 0;		//显示Trace数组极大幅值对应下标
     double f64LeftMinAmp = 0;		//显示Trace数组极大幅值左边极小值
     int    s32LeftMinIndex = startIndex;	//对应下标
     double f64RightMinAmp = 0;		//显示Trace数组极大幅值右边极小值
     int    s32RightMinIndex = startIndex;	//对应下标
     int    s32StopIndex = 0;		//查找的终止范围
     bool   bCondMeet = false;		//判断源数据类型是否满足极值查找条件

     double f64NextPeakAmp = 0;
     int    s32NextPeakIndex = 0;
     double f64CurPeakAmp = 0;			//当前Peak的幅度

     double thresHold = m_thresHold * math_v_base;

     m_stPeakTable.s32PeakTableCount = 0;

     s32StopIndex = endIndex;

     f64NextPeakAmp = thresHold;
     s32LeftMinIndex  = 0;
     s32RightMinIndex = 0;

     /*在显示Trace数组中找到极大值*/
     while (s32RightMinIndex < s32StopIndex) 					//右极小值小于终止索引
     {
         if(m_stPeakTable.s32PeakTableCount == MEAS_PEAK_MAX_INDEX)
         {
             break;
         }
         //查找一个峰值
         measFindRightPeak(&s32PeakIndex,&s32LeftMinIndex,&s32RightMinIndex);

         //根据索引值取出幅度判断是否满足搜索条件
         f64PeakAmp = m_wfmFreq.valueAt(s32PeakIndex);
         f64LeftMinAmp = m_wfmFreq.valueAt(s32LeftMinIndex);
         f64RightMinAmp = m_wfmFreq.valueAt(s32RightMinIndex);
         bCondMeet = measIsParaPeak(f64PeakAmp,f64LeftMinAmp,f64RightMinAmp);
         //满足偏移条件(显示Trace)
         if (bCondMeet)
         {
             double f64PeakAmpMeas = m_wfmFreq.valueAt(s32PeakIndex); 			//获取幅值
             f64CurPeakAmp = m_stMeasPeakManage.stCurrentMaxPeak.ampl;

             //如果这里不取等号，两个幅度相等的信号无法被找到 (待完善)
             //如果取等号，则会在两个幅度相等的信号上面来回的查找
             if (f64PeakAmpMeas <  f64CurPeakAmp
                     && s32PeakIndex != m_stMeasPeakManage.stCurrentMaxPeak.s32Index)
             {
                 bFound = true;
                 if(f64PeakAmpMeas > f64NextPeakAmp)
                 {
                     f64NextPeakAmp = f64PeakAmpMeas;
                     s32NextPeakIndex = s32PeakIndex;
                 }
             }
             //保存疑似峰值的下标
             m_stPeakTable.s32PeakTableIndex[m_stPeakTable.s32PeakTableCount++] = s32PeakIndex;
         }
         //把右极小值给左极小值，为下次查找做准备
         s32LeftMinIndex = s32RightMinIndex;
     }//end of "while (s32RightMinIndex <s32StopIndex)"

     if (bFound)
     {
//         if (1 == m_s32DetTraceDataType)
//         {
//             //            f32NextPeakAmpMeas = samVtodBm(funcGetInputZVal(), f32NextPeakAmpMeas);
//         }
         //保存极大值
         m_stMeasPeakManage.stCurrentMaxPeak.s32Index = s32NextPeakIndex;		//Peak来源数组中的下标
         m_stMeasPeakManage.stCurrentMaxPeak.ampl = f64NextPeakAmp;			//Peak幅值
         m_stMeasPeakManage.stCurrentMaxPeak.freq =
                s32NextPeakIndex * m_wfmFreq.getlltInc()+m_wfmFreq.getllt0();	//Peak频率
         //        stMeasPeakManage.stCurrentMaxPeak.u16X = s32NextPeakIndexShow;			//Peak在屏幕上的横坐标
         //        stMeasPeakManage.stCurrentMaxPeak.u16Y = f32PeakAmpShow;				//Peak在屏幕上的纵坐标
         //        stMeasPeakManage.stCurrentMaxPeak.bView = true;							//1－显示Peak  0－关闭Peak
     }
}

bool fftPeakSearch::measFindRightPeak(int *ps32PeakIndex, int *ps32LeftIndex, int *ps32RightIndex)
{
    double excursion = m_excursion * math_v_base;

    bool   bSuccess = false;
    int    s32TmpIndex = 0;
    double f64Derivate = 0;
    unsigned int u32Counts = 0;			//查找不符合条件的次数
    unsigned int u32PeakTolerateCounts = 0;	//
    bool bSusPeak = false;		//1－怀疑是峰顶 0－非峰顶
    int  s32PeakIndex = 0;		//峰顶的下标
    bool bSusVale = false;		//1－怀疑是峰谷 0－非峰谷
    int  s32ValeIndex = 0;		//峰谷的下标
    int  s32StopIndex = 0;

    if(0 == excursion)
    {
        //如果峰值偏移量为0，则不进行噪声容错判断
        u32PeakTolerateCounts = 1;
    }
    else
    {
        u32PeakTolerateCounts = MEAS_PEAK_TOLERATE_COUNTS;
    }

    /*1.寻找左边峰谷*/
    u32Counts = 0;
    bSusVale = false;
    s32TmpIndex = *ps32LeftIndex;

    s32StopIndex = m_wfmFreq.size();

    while (s32TmpIndex < s32StopIndex)
    {
        if (false == bSusVale)
        {
            //如果当前点小于下一个点，则该点被疑视认为是峰谷
            f64Derivate = m_wfmFreq.valueAt(s32TmpIndex) - m_wfmFreq.valueAt(s32TmpIndex + 1);
            if (f64Derivate < 0)
            {
                bSusVale = true;
                s32ValeIndex = s32TmpIndex;
                //如果当前点小于下一个点,且值的绝对值大于峰值偏移量，则疑似峰谷为峰谷(因为该点将是下一个峰的起点了)
//                if (m_unit == fft_spec_db)
//                {
//                    f64Derivate =  m_wfmFreq.valueAt(s32TmpIndex+1) / m_wfmFreq.valueAt(s32TmpIndex);
//                }

                if (fabs(f64Derivate) >= excursion)
                {
                    s32TmpIndex = s32ValeIndex;
                    break;
                }
            }//end of "if (f64Derivate < 0)"
        }//end of "if (false == bSusVale)"
        else
        {
            f64Derivate = m_wfmFreq.valueAt(s32ValeIndex) - m_wfmFreq.valueAt(s32TmpIndex);
            if (f64Derivate < 0)
            {
                //如果当前点继续小于疑似峰谷，且值大于峰值偏移量，则疑似峰谷为峰谷(因为该点将是下一个峰的起点了-增加肩峰)
                //如果不需要肩峰，则这部分代码需要拿到外面去，这时需要区分陡峰和缓峰
//                if (m_unit == fft_spec_db)
//                {
//                    f64Derivate = m_wfmFreq.valueAt(s32TmpIndex) / m_wfmFreq.valueAt(s32ValeIndex);
//                }
                if (fabs(f64Derivate) >= excursion && excursion != 0)
                {
                    s32TmpIndex = s32ValeIndex;
                    break;
                }

                //如果疑似峰谷连续小于后面PEAK_TOLERATE_NUM个点，则该点为峰谷(这样处理是为了防止噪声)
                if (u32Counts >= u32PeakTolerateCounts)
                {
                    s32TmpIndex = s32ValeIndex;
                    break;
                }
                else
                {
                    u32Counts++;
                }
            }
            else
            {
                //如果当前点小于下一个点，则该点被疑视认为是峰谷
                f64Derivate = m_wfmFreq.valueAt(s32TmpIndex) - m_wfmFreq.valueAt(s32TmpIndex + 1);
                if (f64Derivate < 0)
                {
                    bSusVale = true;
                    s32ValeIndex = s32TmpIndex;
                    //如果当前点小于下一个点,且值的绝对值大于峰值偏移量，则疑似峰谷为峰谷(因为该点将是下一个峰的起点了)

                    if (fabs(f64Derivate) >= excursion)
                    {
                        s32TmpIndex = s32ValeIndex;
                        break;
                    }
                }
                else
                {
                    bSusVale = false;
                }
                u32Counts = 0;
            }//end of "if (f64Derivate < 0)"
        }//end of "else of if (false == bSusVale)"
        s32TmpIndex++;
    }
    *ps32LeftIndex = s32TmpIndex;

    /*2.寻找峰顶*/
    u32Counts = 0;
    bSusPeak = false;
    while (s32TmpIndex < s32StopIndex)
    {
        if (false == bSusPeak)
        {
            //如果当前点大于下一个点则该点为疑似峰顶
            if (m_wfmFreq.valueAt(s32TmpIndex) > m_wfmFreq.valueAt(s32TmpIndex + 1))
            {
                bSusPeak = true;
                s32PeakIndex = s32TmpIndex;
            }
        }
        else
        {
            f64Derivate = m_wfmFreq.valueAt(s32PeakIndex) - m_wfmFreq.valueAt(s32TmpIndex);
            if (f64Derivate > 0)
            {
                //如果疑似峰顶大于当前点，且值大于峰值偏移量，则疑似峰顶为峰顶(因为该点将是下一个峰的起点了-增加肩峰)
                //如果不需要肩峰，则这部分代码需要拿到外面去，这时需要区分陡峰和缓峰

                if (f64Derivate >= excursion && excursion != 0)
                {
                    s32TmpIndex = s32PeakIndex;
                    break;
                }

                //如果疑似峰顶连续大于后面PEAK_TOLERATE_NUM个点，则该点为峰顶(这样处理是为了防止噪声)
                if (u32Counts >= u32PeakTolerateCounts)
                {
                    s32TmpIndex = s32PeakIndex;
                    break;
                }
                else
                {
                    u32Counts++;
                }
            }
            else
            {
                //如果疑似峰顶小于当前点，则该疑似峰顶不是峰顶，重新搜索疑似峰顶
                if (m_wfmFreq.valueAt(s32TmpIndex) > m_wfmFreq.valueAt(s32TmpIndex + 1))
                {
                    bSusPeak = true;
                    s32PeakIndex = s32TmpIndex;
                }
                else
                {
                    bSusPeak = false;
                }
                u32Counts = 0;
            }//end of "else of if (f32BaseTrace[s32PeakIndex] > f32BaseTrace[i])"
        }//end of "else of if (false == bSusPeak)"
        s32TmpIndex++;
    }
    *ps32PeakIndex = s32TmpIndex;

    /*3.寻找右峰谷*/
    u32Counts = 0;
    bSusVale = false;
    while (s32TmpIndex < s32StopIndex)
    {
        if (false == bSusVale)
        {
            //如果当前点小于下一个点，则该点被疑视认为是峰谷
            f64Derivate = m_wfmFreq.valueAt(s32TmpIndex) - m_wfmFreq.valueAt(s32TmpIndex + 1);
            if (f64Derivate < 0)
            {
                bSusVale = true;
                s32ValeIndex = s32TmpIndex;
                //如果当前点小于下一个点,且值的绝对值大于峰值偏移量，则疑似峰谷为峰谷(因为该点将是下一个峰的起点了)

                if (fabs(f64Derivate) >= excursion)
                {
                    s32TmpIndex = s32ValeIndex;
                    break;
                }
            }//end of "if (f64Derivate < 0)"
        }//end of "if (false == bRightStart)"
        else
        {
            f64Derivate = m_wfmFreq.valueAt(s32ValeIndex) - m_wfmFreq.valueAt(s32TmpIndex);
            if (f64Derivate < 0)
            {
                //如果当前点继续小于疑似峰谷，且值大于峰值偏移量，则疑似峰谷为峰谷(因为该点将是下一个峰的起点了-增加肩峰)
                //如果不需要肩峰，则这部分代码需要拿到外面去，这时需要区分陡峰和缓峰
//                if (m_unit == fft_spec_db)
//                {
//                    f64Derivate = m_wfmFreq.valueAt(s32TmpIndex) / m_wfmFreq.valueAt(s32ValeIndex);
//                }

                if (fabs(f64Derivate) >= excursion && excursion != 0)
                {
                    s32TmpIndex = s32ValeIndex;
                    break;
                }

                //如果疑似峰谷连续小于后面PEAK_TOLERATE_NUM个点，则该点为峰谷(这样处理是为了防止噪声)
                if (u32Counts >= u32PeakTolerateCounts)
                {
                    s32TmpIndex = s32ValeIndex;
                    break;
                }
                else
                {
                    u32Counts++;
                }
            }
            else
            {
                //如果当前点小于下一个点，则该点被疑视认为是峰谷
                f64Derivate = m_wfmFreq.valueAt(s32TmpIndex) - m_wfmFreq.valueAt(s32TmpIndex + 1);
                if (f64Derivate < 0)
                {
                    bSusVale = true;
                    s32ValeIndex = s32TmpIndex;
                    //如果当前点小于下一个点,且值的绝对值大于峰值偏移量，则疑似峰谷为峰谷(因为该点将是下一个峰的起点了)
//                    if (m_unit == fft_spec_db)
//                    {
//                        f64Derivate = m_wfmFreq.valueAt(s32TmpIndex + 1) / m_wfmFreq.valueAt(s32TmpIndex);
//                    }

                    if (fabs(f64Derivate) >= excursion)
                    {
                        s32TmpIndex = s32ValeIndex;
                        break;
                    }
                }
                else
                {
                    bSusVale = false;
                }
                u32Counts = 0;
            }//end of "if (f64Derivate < 0)"
        }//end of "else of if (false == bSusVale)"
        s32TmpIndex++;
    }
    *ps32RightIndex = s32TmpIndex;
    return bSuccess;
}

bool fftPeakSearch::measIsParaPeak(double f64PeakAmpl, double f64LeftMinAmpl, double f64RightMinAmpl)
{
    bool bSuccess = false;

    //test
    double thresHold = m_thresHold * math_v_base;
    double excursion = m_excursion * math_v_base;

    m_unit = fft_spec_db;
    //极小值要大于等于阈值(显示Trace)

    // add_by_zx 那么实际找出来的峰值就要大于峰值极限+峰值偏移

    thresHold = thresHold - excursion;
    if (f64LeftMinAmpl < thresHold)
    {
        f64LeftMinAmpl = thresHold;
    }

    if (f64RightMinAmpl < thresHold)
    {
        f64RightMinAmpl = thresHold;
    }

    bSuccess = (f64PeakAmpl - f64LeftMinAmpl) > excursion 				//不能取等于，会导致死循环
                && (f64PeakAmpl - f64RightMinAmpl) > excursion ;

    return bSuccess;
}

bool fftPeakSearch::cmpAmpOrder(const measPeakStru& left,const measPeakStru& right)
{
    return left.ampl > right.ampl;
}

bool fftPeakSearch::cmpFreqOrder(const measPeakStru& left,const measPeakStru& right)
{
    return left.freq < right.freq;
}

//! math中的峰值点顺序：按幅度从大到小，按频率从小到大。
void fftPeakSearch::changeOrder()
{
    if(!m_stPeakTable.m_peakPoint.isEmpty())
    {
        if(peakOrder == fft_peak_AmpOrder)
        {
            qSort(m_stPeakTable.m_peakPoint.begin(),m_stPeakTable.m_peakPoint.end(),cmpAmpOrder);
        }
        else
        {
            qSort(m_stPeakTable.m_peakPoint.begin(),m_stPeakTable.m_peakPoint.end(),cmpFreqOrder);
        }
    }
}
