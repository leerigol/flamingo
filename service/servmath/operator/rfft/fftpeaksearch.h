#ifndef FFTPEAKSEARCH_H
#define FFTPEAKSEARCH_H

#include "../../mathcfg.h"
#include "../../baseclass/dsowfm.h"
#include "../../com/fftlib/rfftc.h"

#define ST_EXCURSN_LOG   3		//信号追踪时两个信号的差为3dB
#define MEAS_PEAK_TOLERATE_COUNTS	10		//峰值搜索确定峰顶和峰谷时容错的个数

#define MEAS_MAX_PEAK_TH	 -90				//最大峰值偏阈值(最小幅度) 单位dbm
#define MEAS_MIN_PEAK_TH	 -200			//最大峰值偏阈值(最小幅度)
#define MEAS_PEAK_MAX_AMPL	300	//峰值搜素时初始化最大的幅度
#define MEAS_PEAK_MAX_INDEX	20000	//峰值搜素时初始化最大的下标

#define MEAS_ZERO_EXCURSN_LOG 6				//抛出零频时对数刻度极限
#define MEAS_ZERO_EXCURSN_LIN 1.99526		//抛出零频时线性刻度极限


//peak属性
typedef struct
{
    double   ampl;		//Peak幅值
    double   freq;		//Peak频率
    int     s32Index;    //Peak来源数组中的下标 在10K数组中的下标
}measPeakStru;

enum enPeakOrder
{
    fft_peak_AmpOrder,
    fft_peak_FreqOrder,
};
//峰值表结构体
typedef struct
{
    QVector<measPeakStru> m_peakPoint;
    int s32PeakTableIndex[MEAS_PEAK_MAX_INDEX];
    int s32PeakTableCount;							   //疑似Peak的个数
}PeakTableStru;

struct PeakBuffer
{
    QVector<measPeakStru> pointBuffer;                 //for app draw
    QMutex peakDataMutex;
};

//peak管理
struct SMeasPeakManageStru
{
    measPeakStru stCurrentMaxPeak;	//当前最大的Peak
    measPeakStru stCurrentMinPeak;	//当前最小的Peak
};

class fftPeakSearch
{
public:
    fftPeakSearch();
    void peakAction(DsoWfm &wfm, qlonglong startFreq, qlonglong endFreq);
    void changeOrder();

private:
    void findPeak(int startIndex, int endIndex);
    bool measFindRightPeak(int *ps32PeakIndex, int *ps32LeftIndex, int *ps32RightIndex);
    bool measIsParaPeak(double f64PeakAmpl, double f64LeftMinAmpl, double f64RightMinAmpl);

    static bool cmpAmpOrder(const measPeakStru &left, const measPeakStru &right);
    static bool cmpFreqOrder(const measPeakStru &left,const measPeakStru &right);
public:
    enPeakOrder         peakOrder;
    SMeasPeakManageStru m_stMeasPeakManage;
    qlonglong           m_thresHold;
    qlonglong           m_excursion;
    bool                m_peakEnable;
    RangeCfg            m_peakMaxCount;
    fftSpecUnit         m_unit;
    DsoWfm              m_wfmFreq;

    PeakBuffer          m_peakBuffer;
private:
    PeakTableStru       m_stPeakTable;		//峰值表
};

#endif // FFTPEAKSEARCH_H
