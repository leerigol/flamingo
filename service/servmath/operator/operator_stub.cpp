#include "../../include/dsodbg.h"
#include <QtCore>

#include "operator_stub.h"

#include "../../com/fftlib/rfft.h"
#include "./mfilter/mfilter.h"

opStatus::opStatus()
{
    offset = 0;
    len = 0;
}

//! arith
void math_arith_add( float *a, float *b, float *c, int len, opStatus *stat )
{
    Q_ASSERT( NULL != stat );

    int i;
    for ( i = 0; i < len; i++ )
    {
        c[i] = a[i] + b[i];
    }

    stat->offset = 0;
    stat->len = len;
}
void math_arith_sub( float *a, float *b, float *c, int len, opStatus *stat )
{
    Q_ASSERT( NULL != stat );

    int i;
    for ( i = 0; i < len; i++ )
    {
        c[i] = a[i] - b[i];
    }

    stat->offset = 0;
    stat->len = len;
}
void math_arith_mul( float *a, float *b, float *c, int len, opStatus *stat )
{
    Q_ASSERT( NULL != stat );

    int i;
    for ( i = 0; i < len; i++ )
    {
        c[i] = a[i] * b[i];
    }

    stat->offset = 0;
    stat->len = len;
}
void math_arith_div( float *a, float *b, float *c, int len, opStatus *stat )
{
    Q_ASSERT( NULL != stat );

    bool bPreValid;
    bPreValid = false;

    int i;

    stat->offset = 0;
    for ( i = 0; i < len; i++ )
    {
        if ( b[i] == 0 )
        {
            //! move to next
            if ( bPreValid == false )
            {
                stat->offset = i+1;
            }
            else
            {
                //! 非法值统一处理为0
                //c[i] = fPreV;
                c[i] = 0;
            }
        }
        else
        {
            c[i] = a[i] / b[i];
            bPreValid = true;
        }
    }
    stat->len = (len - stat->offset);
}

void math_logic_and( DsoPoint *a, DsoPoint *b, DsoPoint *c, int len, opStatus *stat )
{
    int i;
    for ( i = 0; i < len; i++ )
    {
        c[i] = a[i] && b[i];
    }

    stat->offset = 0;
    stat->len = len;
}
void math_logic_or( DsoPoint *a, DsoPoint *b, DsoPoint *c, int len, opStatus *stat )
{
    int i;
    for ( i = 0; i < len; i++ )
    {
        c[i] = a[i] || b[i];
    }

    stat->offset = 0;
    stat->len = len;
}

void math_logic_xor( DsoPoint *a, DsoPoint *b, DsoPoint *c, int len, opStatus *stat )
{
    int i;
    for ( i = 0; i < len; i++ )
    {
        c[i] = a[i] ^ b[i];
    }

    stat->offset = 0;
    stat->len = len;
}

void math_logic_not( DsoPoint *a, DsoPoint *c, int len, opStatus *stat )
{
    int i;
    for ( i = 0; i < len; i++ )
    {
        c[i] = !a[i];
    }

    stat->offset = 0;
    stat->len = len;
}

//! function
void math_func_abs( float *a,  float *c, int len, opStatus *stat )
{
    int i;

    for ( i = 0; i < len; i++ )
    {
        c[i] = fabs( a[i] );
    }

    stat->offset = 0;
    stat->len = len;
}
void math_func_exp( float *a,  float *c, int len, opStatus *stat )
{
    int i;

    for ( i = 0; i < len; i++ )
    {
        c[i] = exp( a[i] );
    }

    stat->offset = 0;
    stat->len = len;
}
void math_func_lg( float *a,  float *c, int len, opStatus *stat )
{
    int i;

    bool bPreValid;
    float fPreV;
    Q_UNUSED(fPreV);
    bPreValid = false;

    stat->offset = 0;
    for ( i = 0; i < len; i++ )
    {
        if ( a[i] <= 0 )
        {
            //! move to next
            if ( bPreValid == false )
            {
                stat->offset = i+1;
            }
            else
            {
                //! 非法值统一处理为0
                //c[i] = fPreV;
                c[i] = 0;
             }
        }
        else
        {
            c[i] = log10(a[i]);
            fPreV = c[i];
            bPreValid = true;
        }
    }

    stat->len = (len - stat->offset);
}
void math_func_ln( float *a, float *c, int len, opStatus *stat )
{
    int i;

    bool bPreValid;
    float fPreV;
    Q_UNUSED(fPreV);
    bPreValid = false;

    stat->offset = 0;
    for ( i = 0; i < len; i++ )
    {
        if ( a[i] <= 0 )
        {
            //! move to next
            if ( bPreValid == false )
            {
                stat->offset = i+1;
            }
            else
            {
                //! 非法值统一处理为0
                //c[i] = fPreV;
                c[i] = 0;
            }
        }
        else
        {
            c[i] = log(a[i]);
            fPreV = c[i];
            bPreValid = true;
        }
    }

    stat->len = (len - stat->offset);
}

void math_func_root( float *a, float *c, int len, opStatus *stat )
{
    int i;

    bool bPreValid;
    float fPreV;
    Q_UNUSED(fPreV);
    bPreValid = false;

    stat->offset = 0;
    for ( i = 0; i < len; i++ )
    {
        if ( a[i] < 0 )
        {
            //! move to next
            if ( bPreValid == false )
            {
                stat->offset = i+1;
            }
            else
            {
                //! 非法值统一处理为0
                //c[i] = fPreV;
                c[i] = 0;
            }
        }
        else
        {
            c[i] = sqrt(a[i]);
            fPreV = c[i];
            bPreValid = true;
        }
    }

    stat->len = (len - stat->offset);
}

void math_func_intg( float *a, float *c, int len,
                     opIntgContext *pContext,
                     opStatus *stat )
{
    Q_ASSERT( NULL != pContext );
    Q_ASSERT( NULL != stat );

    int i;

    float fSum;
    float tInc;

    stat->offset = 0;

    fSum = pContext->bias;
    tInc = pContext->tInc;

    for ( i = 0; i < len; i++ )
    {
        fSum += a[i] * tInc;
        c[i] = fSum;
    }

    stat->len = (len - stat->offset);
}

//! d[i] = sum( d[i+1..i+gap ] ) - sum ( d[ i-gap..i-1] )
//! window is an odd value
void math_func_diff( float *a, float *c, int len,
                     opDiffContext *pContext,
                     opStatus *stat )
{
    Q_ASSERT( NULL != pContext );
    Q_ASSERT( NULL != stat );

    float tInc = pContext->tInc;
    int wnd = pContext->window;

    Q_ASSERT( wnd > 2 );

    int gap = wnd / 2;

    Q_ASSERT( tInc != 0 );
    float fbase = gap*tInc;

    float sumL, sumH;
    int i, j;
    for ( i = gap; i < len - gap; i++ )
    {
        //! pre gap
        sumL = 0;
        for ( j = i - gap; j < i; j++ )
        {
            sumL += a[j];
        }

        //! post gap
        sumH = 0;
        for ( j = i + 1; j < i + gap + 1; j++ )
        {
            sumH += a[j];
        }

        c[i] = ( sumH - sumL ) / fbase / gap;//! 除以n个时间间隔，再除以n个点
    }

    stat->offset = gap;
    stat->len = len - 2*gap;

    if ( stat->len < 0 )
    { stat->len = 0; }
}

void math_func_axb( float *a, float *c, int len,
                    opAxbContext *pContext,
                    opStatus *stat )
{
    Q_ASSERT( NULL != pContext );

    int i;
    float ka, kb;

    ka = pContext->a;
    kb = pContext->b;

    for ( i = 0; i < len; i++ )
    {
        c[i] = ka * ( a[i] ) + kb;
    }

    stat->offset = 0;
    stat->len = len;
}

void math_func_filter(float *a, float *c, int len,
                      opFilterContext *pContext
                      , opStatus *stat)
{
    Q_ASSERT( NULL != pContext );
    LOG_DBG();
    filterCalc(a, len, pContext->filter,
                       pContext->leftFreq,
                       pContext->rightFreq,
                       c);
    //! 滤波运算截掉前面一段和后面一段
    stat->offset = 125;
    stat->len = len - 2 * 125 - 2;
}


void math_freq_fft( float *a, float *c, int len,
                    opFftContext *pContext,
                    opStatus *stat )
{
    Q_ASSERT( NULL != pContext );

    int ret = rfft::RFFT::fft( a, len,
                     pContext->fftSize,
                     pContext->window,
                     pContext->specUnit,
                     c );
    LOG_DBG()<<ret;
    stat->offset = 0;
    stat->len = pContext->fftSize / 2;
}
