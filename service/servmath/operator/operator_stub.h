#ifndef OPERATOR_STUB
#define OPERATOR_STUB

#include "../../baseclass/dsowfm.h"
#include "../../com/fftlib/rfftc.h"
#include "./rfft/fftpeaksearch.h"
#include "./mfilter/mfilter.h"
//! 计算状态
struct opStatus
{
    opStatus();

    int offset;
    int len;
};

struct opContext
{
};

struct opIntgContext : public opContext
{
    float tInc;
    float bias;
    int size;
};

struct opDiffContext : public opContext
{
    float tInc;
    int window;
};

struct opAxbContext : public opContext
{
    float a;
    float b;
};

struct opFftContext : public opContext
{
    fftWindow window;
    fftSpecUnit specUnit;
    int fftSize;        //! 1024 to 1M
};

struct opFilterContext : public opContext
{
    FilterType filter;
    int leftFreq;
    int rightFreq;    //! lp and hp has no rightFreq
};

//! arith
void math_arith_add( float *a, float *b, float *c, int len, opStatus *stat);
void math_arith_sub( float *a, float *b, float *c, int len, opStatus *stat);
void math_arith_mul( float *a, float *b, float *c, int len, opStatus *stat);
void math_arith_div( float *a, float *b, float *c, int len, opStatus *stat);

//! logic
void math_logic_and( DsoPoint *a, DsoPoint *b, DsoPoint *c, int len, opStatus *stat );
void math_logic_or( DsoPoint *a, DsoPoint *b, DsoPoint *c, int len, opStatus *stat );
void math_logic_xor( DsoPoint *a, DsoPoint *b, DsoPoint *c, int len, opStatus *stat );
void math_logic_not( DsoPoint *a, DsoPoint *c, int len, opStatus *stat );

//! function
void math_func_abs( float *a,  float *c, int len, opStatus *stat );
void math_func_exp( float *a,  float *c, int len, opStatus *stat );
void math_func_lg( float *a,  float *c, int len, opStatus *stat );
void math_func_ln( float *a, float *c, int len, opStatus *stat );

void math_func_root( float *a, float *c, int len, opStatus *stat );
void math_func_intg( float *a, float *c, int len,
                     opIntgContext *pContext,
                     opStatus *stat );
void math_func_diff( float *a, float *c, int len,
                     opDiffContext *pContext,
                     opStatus *stat );
void math_func_filter(float *a, float *c, int len,
                      opFilterContext *pContext,
                      opStatus *stat );

void math_func_axb( float *a, float *c, int len,
                    opAxbContext *pContext,
                    opStatus *stat );

void math_freq_fft( float *a, float *c, int len,
                    opFftContext *pContext,
                    opStatus *stat );


//! range
struct opRange
{
    float minV, maxV;   //! !!!value must be positive
};

//! arith range
void math_arith_add( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC);

void math_arith_sub( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC );
void math_arith_mul( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC );

void math_arith_div( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC );

//! func range
void math_func_abs( opRange *opRangeA,
                    opRange *opRangeC );
void math_func_exp( opRange *opRangeA,
                    opRange *opRangeC );
void math_func_lg( opRange *opRangeA,
                   opRange *opRangeC );
void math_func_ln( opRange *opRangeA,
                   opRange *opRangeC );

void math_func_root( opRange *opRangeA,
                     opRange *opRangeC );
void math_func_axb( opRange *opRangeA,
                    opRange *opRangeC,
                    opAxbContext *pContext );

void math_func_intg( opRange *opRangeA,
                     opRange *opRangeC,
                     opIntgContext *pContext);
void math_func_diff( opRange *opRangeA,
                     opRange *opRangeC,
                     opDiffContext *pContext);

void math_func_filter( opRange *opRangeA,
                       opRange *opRangeC );

void math_freq_fft_db( opRange *opRangeA,
                       opRange *opRangeC );

void math_freq_fft_dbm( opRange *opRangeA,
                       opRange *opRangeC );

void math_freq_fft_vrms( opRange *opRangeA,
                       opRange *opRangeC );

void math_trend(float max,float min,opRange *opRangeC);
#endif // OPERATOR_STUB

