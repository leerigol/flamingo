#ifndef MATHOPERFUNC_H
#define MATHOPERFUNC_H


#include "mathoper.h"

class MathOperFunc : public MathOper
{
public:
    MathOperFunc( MathOperator op );
public:
    virtual DsoErr op( DsoWfm &wfmA,
                     int from, int len,
                     DsoWfm *pWfmC,
                     opContext *pContext);
};

#endif // MATHOPERFUNC_H
