#include "mathoperfunc.h"
#include "operator_stub.h"

MathOperFunc::MathOperFunc( MathOperator op  ) : MathOper( op )
{

}

#define LEFT_VAL    wfmA.getValue() + from
#define RIGHT_VAL   pWfmC->getValue() + from

DsoErr MathOperFunc::op( DsoWfm &wfmA,
                 int from, int len,
                 DsoWfm *pWfmC,
                 opContext *pContext )
{
    pWfmC->init( from + len );

    //! set attr
    pWfmC->settAttr( wfmA.getAttr() );

    opStatus status;

    switch( mOperator )
    {
        case operator_intg:
            Q_ASSERT( NULL != pContext );
            math_func_intg( LEFT_VAL,
                            RIGHT_VAL,
                            len,
                            (opIntgContext*)pContext,
                            &status );
        break;

        case operator_diff:
            Q_ASSERT( NULL != pContext );
            math_func_diff( LEFT_VAL,
                        RIGHT_VAL,
                        len,
                        (opDiffContext*)pContext,
                        &status );
        break;

        case operator_abs:
            math_func_abs( LEFT_VAL,
                           RIGHT_VAL,
                           len,
                           &status );
        break;

        case operator_exp:
            math_func_exp( LEFT_VAL,
                           RIGHT_VAL,
                           len,
                           &status );
        break;

        case operator_lg:
            math_func_lg( LEFT_VAL,
                           RIGHT_VAL,
                           len,
                           &status );
        break;

        case operator_ln:
            math_func_ln( LEFT_VAL,
                           RIGHT_VAL,
                           len,
                           &status );
        break;

        case operator_root:
            math_func_root( LEFT_VAL,
                           RIGHT_VAL,
                           len,
                           &status );
        break;

        case operator_ax_b:
            math_func_axb( LEFT_VAL,
                           RIGHT_VAL,
                           len,
                           (opAxbContext*)pContext,
                           &status );
        break;

        case operator_lp:
        case operator_hp:
        case operator_bp:
        case operator_bt:
             math_func_filter( LEFT_VAL,
                               RIGHT_VAL,
                               len,
                               (opFilterContext*)pContext,
                               &status );
        break;

        default:
            Q_ASSERT( false );
        break;
    }

    pWfmC->setRange( status.offset + wfmA.start(), status.len );
//    qDebug()<<__FUNCTION__<<__LINE__<<status.offset<<status.len;
    return ERR_NONE;
}
