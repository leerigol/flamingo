#ifndef MATHOPERLOGIC_H
#define MATHOPERLOGIC_H

#include "mathoper.h"

class MathOperLogic : public MathOper
{
public:
    MathOperLogic( MathOperator op );
public:
    virtual DsoErr op( DsoWfm &wfmA,
                     DsoWfm &wfmB,
                     int offset, int len,
                     DsoWfm *pWfmC,
                     opContext *pContext );

    virtual DsoErr op(DsoWfm &wfmA,
                     int offset, int len,
                     DsoWfm *pWfmC, opContext *);
};

#endif // MATHOPERLOGIC_H
