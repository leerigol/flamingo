#ifndef MATHOPERFFT_H
#define MATHOPERFFT_H

#include "mathoper.h"

class MathOperFft : public MathOper
{
public:
    MathOperFft( MathOperator op );

public:
    virtual DsoErr op( DsoWfm &wfmA,
                     int from, int len,
                     DsoWfm *pWfmC,
                     opContext *pContext);

};

#endif // MATHOPERFFT_H
