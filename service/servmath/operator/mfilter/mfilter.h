#ifndef MFILTERH_H
#define MFILTERH_H
#include <QtCore>
#include <QString>
#include "../../com/neon/projectNe10-Ne10-a08b29d/inc/NE10.h"

enum FilterType
{
    filter_lp,
    filter_hp,
    filter_bp,
    filter_bt,
};

namespace MathFilter {

class FilterContext
{
public:
    FilterContext();
    ~FilterContext();
    void config(float* dataIn,int size);
    int loadWindow( FilterType type, int freqLeft, int freqRight );

    ne10_float32_t *pSrc;
    ne10_float32_t *pDst;
    ne10_fir_instance_f32_t *pInstance;
    const static int filterOrder;
private:
    int load(const QString &fileName );
    int mSize;
};

class Filter
{
public:
    Filter();
    static void filterCalc(float* dataIn, int len, FilterType filterType,
                           int left, int right, float *dataOut);

private:
    static FilterContext filterContext;
};
}

void filterCalc(float* dataIn, int len, FilterType filterType,
                int left,
                int right, float *dataOut);
#endif // RFILTERH_H
