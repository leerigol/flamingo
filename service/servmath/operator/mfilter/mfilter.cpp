#include "../../include/dsodbg.h"
#include "mfilter.h"
#include "NE10.h"

namespace MathFilter {

const int FilterContext::filterOrder = 250;

FilterContext::FilterContext()
{
    pSrc = NULL;
    pDst = NULL;
    pInstance = new ne10_fir_instance_f32_t;
    Q_ASSERT(NULL != pInstance);

    pInstance->numTaps = filterOrder + 1;
    pInstance->pCoeffs = new ne10_float32_t[pInstance->numTaps];
    Q_ASSERT(NULL != pInstance->pCoeffs);

    pInstance->pState = new ne10_float32_t[pInstance->numTaps+1000000];
    Q_ASSERT(NULL != pInstance->pState);
}

FilterContext::~FilterContext()
{
    if(NULL != pInstance)
    {
        if(NULL != pInstance->pCoeffs)
        {
            delete[] pInstance->pCoeffs;
        }
        if(NULL != pInstance->pState)
        {
            delete[] pInstance->pState;
        }
        delete pInstance;
    }
    if(NULL != pSrc)
    {
        delete[] pSrc;
    }
    if(NULL != pDst)
    {
        delete[] pDst;
    }
}

void FilterContext::config(float *dataIn, int size)
{
    if( size != mSize)
    {
        if(pSrc != NULL) { delete []pSrc; pSrc = NULL;}
        if(pDst != NULL) { delete []pDst; pDst = NULL;}
        mSize = size;

        pSrc = new float[mSize + filterOrder/ 2];
        Q_ASSERT(pSrc != NULL);

        pDst = new float[mSize + filterOrder/ 2];
        Q_ASSERT(pDst != NULL);

    }
    memcpy( pSrc, dataIn, mSize * sizeof(float));
}

int FilterContext::loadWindow(FilterType type, int freqLeft, int freqRight)
{
    QString path = QString("/rigol/resource/filter");
    QString fileName;
    switch (type) {
    case filter_lp:
        path = path + QString("/lp/");
        fileName = QString("lp_") + QString::number(freqLeft*1.0/200)
                + ".bin";
        break;

    case filter_hp:
        path = path + QString("/hp/");
        fileName = QString("hp_") + QString::number(freqLeft*1.0/200)
                + ".bin";
        break;

    case filter_bp:
        path = path + QString("/bp/");
        fileName = QString("bp_") + QString::number(freqLeft*1.0/200)+"_"+
                  QString::number(freqRight*1.0/200) + ".bin";
        break;

    case filter_bt:
        path = path + QString("/bt/");
        fileName = QString("bt_") + QString::number(freqLeft*1.0/200)+"_"+
                  QString::number(freqRight*1.0/200) + ".bin";
        break;

    default:
        LOG_DBG()<<type;
        Q_ASSERT(false);
        break;
    }
    load(path+fileName);
    return 0;
}

int FilterContext::load(const QString &fileName)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
    {
       qDebug()<<fileName;
       Q_ASSERT(false);
    }

    QDataStream in(&file);
    for(int i = 0; i <= filterOrder;i++)
    {
        in>>pInstance->pCoeffs[i];
    }

    return 0;
}

FilterContext Filter::filterContext;

Filter::Filter()
{

}

void Filter::filterCalc(float* dataIn, int len, FilterType filterType,
                        int left, int right, float* dataOut)
{
    filterContext.config(dataIn, len);
    filterContext.loadWindow(filterType, left, right);

    ne10_fir_float_neon(filterContext.pInstance, filterContext.pSrc,
                         filterContext.pDst, len + FilterContext::filterOrder/ 2);
    LOG_DBG();
    memcpy(dataOut, filterContext.pDst + (FilterContext::filterOrder/ 2) + 1, len*sizeof(float));
}

}
void filterCalc(float *dataIn, int len, FilterType filterType,
                int left, int right, float* dataOut)
{
    LOG_DBG();
    MathFilter::Filter::filterCalc(dataIn, len, filterType, left, right, dataOut);
}
