#include "mathoperarith.h"
#include "operator_stub.h"

MathOperArith::MathOperArith( MathOperator op ) : MathOper( op )
{}

#define L1_VALUE    wfmA.getValue() + from
#define L2_VALUE    wfmB.getValue() + from
#define R_VALUE     pWfmC->getValue() + from

DsoErr MathOperArith::op( DsoWfm &wfmA,
                          DsoWfm &wfmB,
                          int from, int len,
                          DsoWfm *pWfmC,
                          opContext */*pContext*/ )
{
    Q_ASSERT( NULL != pWfmC );

    //! init memory
    pWfmC->init( from + len );

    //! set attr
    pWfmC->settAttr( wfmA.getAttr() );

    opStatus status;

    if ( mOperator == operator_add )
    {
        math_arith_add( L1_VALUE,
                        L2_VALUE,
                        R_VALUE,
                        len,
                        &status );
    }
    else if ( mOperator == operator_sub )
    {
        math_arith_sub( L1_VALUE,
                        L2_VALUE,
                        R_VALUE,
                        len,
                        &status );
    }
    else if ( mOperator == operator_mul )
    {
        math_arith_mul( L1_VALUE,
                        L2_VALUE,
                        R_VALUE,
                        len,
                        &status );
    }
    else if ( mOperator == operator_div )
    {
        math_arith_div( L1_VALUE,
                        L2_VALUE,
                        R_VALUE,
                        len,
                        &status );
    }
    else
    {
        Q_ASSERT( false);
    }

    pWfmC->setRange( status.offset + wfmA.start(), status.len );

    return ERR_NONE;
}
