#include "../../include/dsodbg.h"

#include "mathoperfft.h"
#include "../../com/fftlib/rfft.h"
#include "../../baseclass/resample/reSample.h"

MathOperFft::MathOperFft( MathOperator op  ) : MathOper( op )
{

}

#define LEFT_VAL    wfmA.getValue() + from
#define RIGHT_VAL   pWfmC->getValue()
DsoErr MathOperFft::op( DsoWfm &wfmA,
                     int from, int len,
                     DsoWfm *pWfmC,
                     opContext *pContext)
{
    Q_ASSERT( NULL != pContext );
    Q_ASSERT( NULL != pWfmC );
    opFftContext *pfftContext;

    pfftContext = (opFftContext*)pContext;
    int fftSize = pfftContext->fftSize;
    pWfmC->init( fftSize );

     //qDebug()<< "compress wfmA " << "len = " << len << "fftSize = " << fftSize;
    //! compress wfmA
    if ( len > fftSize )
    {
        //! compress,if expand dataOut can't use "dataIn"
        reSampleExt( len,
                     fftSize,
                     wfmA.getValue(),
                     wfmA.getValue()
                     );
        float mul = 1.0*len/fftSize;
        wfmA.setlltInc(round(wfmA.getlltInc() * mul));
        len = fftSize;
        LOG_DBG()<<"len"<<"fftSize";
    }

    qlonglong tInc = wfmA.getlltInc();
    Q_ASSERT( 0 != tInc );

    Q_ASSERT( 0 != pfftContext->fftSize );

    qlonglong alignSize;
    //! aligned to 1000
    alignSize = ( ( pfftContext->fftSize / 2 + 1023 ) / 1024 ) * 1000;

    //! fft sa
    //! sa in 1e-6 hz unit
    pWfmC->sett( 0,
                 1000000000000000000ll /tInc / 2 / alignSize, //fs/2/alignSize
                 E_N6 );

    LOG_DBG()<<"alignSize"<<alignSize<<"pWfmC->getlltInc()"<<pWfmC->getlltInc();

    opStatus status;
    math_freq_fft( LEFT_VAL,
                   RIGHT_VAL,
                   len,
                   pfftContext,
                   &status );

    //! 0, fft size/2
    pWfmC->setRange( status.offset, status.len );

    //! if expand, the size of pWfmC will expand
    DsoWfm in = *pWfmC;
    pWfmC->init(alignSize);
    //! compress or expand to 1000 aligned
    reSampleExt( pfftContext->fftSize / 2,
                 alignSize,
                 in.getValue(),
                 pWfmC->getValue());
    LOG_DBG()<<pWfmC->getlltInc();
    pWfmC->setRange( status.offset, alignSize );
    return ERR_NONE;
}
