#include "mathoperlogic.h"
#include "operator_stub.h"
MathOperLogic::MathOperLogic( MathOperator op ) : MathOper( op )
{
}

DsoErr MathOperLogic::op( DsoWfm &wfmA,
                 DsoWfm &wfmB,
                 int from, int len,
                 DsoWfm *pWfmC,
                 opContext */*pContext*/ )
{

    opStatus status;

    pWfmC->init( from + len );

    //! set attr
    pWfmC->settAttr( wfmA.getAttr() );

    switch( mOperator )
    {
        case operator_and:
            math_logic_and( wfmA.getPoint() + from,
                            wfmB.getPoint() + from,
                            pWfmC->getPoint() + from,
                            len,
                            &status );
            break;

        case operator_or:
            math_logic_or( wfmA.getPoint() + from,
                            wfmB.getPoint() + from,
                            pWfmC->getPoint() + from,
                            len,
                            &status );
            break;

        case operator_xor:
            math_logic_xor( wfmA.getPoint() + from,
                            wfmB.getPoint() + from,
                            pWfmC->getPoint() + from,
                            len,
                            &status );
            break;

        default:
            Q_ASSERT( false );
        break;
    }

    pWfmC->setRange( status.offset + wfmA.start(), status.len );

    return ERR_NONE;
}

DsoErr MathOperLogic::op( DsoWfm &wfmA,
                 int from, int len,
                 DsoWfm *pWfmC,
                 opContext */*pContext*/ )
{
    opStatus status;
    //! init memory
    pWfmC->init( from + len );

    pWfmC->settAttr( wfmA.getAttr() );
    if ( mOperator == operator_not )
    {
        math_logic_not( wfmA.getPoint() + from,
                        pWfmC->getPoint() + from,
                        len,
                        &status );

        pWfmC->setRange( status.offset + wfmA.start(), status.len );
    }
    else
    {
        pWfmC->setRange( 0, 0 );
    }
    return ERR_NONE;
}
