
#include "operator_stub.h"
#include "../../include/dsodbg.h"

static float quad_min( float a1, float a2, float a3, float a4 )
{
    float mmin;

    mmin = qMin( a1, a2 );
    mmin = qMin( mmin, a3 );
    mmin = qMin( mmin, a4 );

    return mmin;
}

static float quad_max( float a1, float a2, float a3, float a4 )
{
    float mmax;

    mmax = qMax( a1, a2 );
    mmax = qMax( mmax, a3 );
    mmax = qMax( mmax, a4 );

    return mmax;
}

static void peak_add( float a1, float a2,
                       float b1, float b2,
                       float *pmin, float *pmax )
{
    *pmin = quad_min( a1 + b1,
                     a1 + b2,
                     a2 + b1,
                     a2 + b2 );

    *pmax = quad_max( a1 + b1,
                     a1 + b2,
                     a2 + b1,
                     a2 + b2 );
}

static void peak_sub( float a1, float a2,
                       float b1, float b2,
                       float *pmin, float *pmax )
{
    *pmin = quad_min( fabs(a1 - b1),
                      fabs(a1 - b2),
                      fabs(a2 - b1),
                      fabs(a2 - b2) );

    LOG_DBG()<<abs(a1-b1)<<abs(a1-b2);
    LOG_DBG()<<abs(a2-b1)<<abs(a2-b2);

    *pmax = quad_max( fabs(a1 - b1),
                      fabs(a1 - b2),
                      fabs(a2 - b1),
                      fabs(a2 - b2) );
}

static void peak_mul( float a1, float a2,
                       float b1, float b2,
                       float *pmin, float *pmax )
{
    *pmin = quad_min( a1 * b1,
                     a1 * b2,
                     a2 * b1,
                     a2 * b2 );

    *pmax = quad_max( a1 * b1,
                     a1 * b2,
                     a2 * b1,
                     a2 * b2 );
}

static void peak_div( float a1, float a2,
                       float b1, float b2,
                       float *pmin, float *pmax )
{
    Q_ASSERT( b1 != 0 && b2 != 0 );

    *pmin = quad_min( a1 / b1,
                     a1 / b2,
                     a2 / b1,
                     a2 / b2 );

    *pmax = quad_max( a1 / b1,
                     a1 / b2,
                     a2 / b1,
                     a2 / b2 );
}

void math_arith_add( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC)
{
    opRangeC->minV = opRangeA->minV + opRangeB->minV;
    opRangeC->maxV = opRangeA->maxV + opRangeB->maxV;

    peak_add( opRangeA->minV, opRangeA->maxV,
              opRangeB->minV, opRangeB->maxV,
              &opRangeC->minV, &opRangeC->maxV );
}

void math_arith_sub( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC )
{
    peak_sub( opRangeA->minV, opRangeA->maxV,
              opRangeB->minV, opRangeB->maxV,
              &opRangeC->minV, &opRangeC->maxV );
    LOG_DBG()<<opRangeA->minV<<opRangeA->maxV;
    LOG_DBG()<<opRangeB->minV<<opRangeB->maxV;
    LOG_DBG()<<opRangeC->minV<<opRangeC->maxV;
}

void math_arith_mul( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC )
{
    peak_mul( opRangeA->minV, opRangeA->maxV,
              opRangeB->minV, opRangeB->maxV,
              &opRangeC->minV, &opRangeC->maxV );
}

void math_arith_div( opRange *opRangeA,
                     opRange *opRangeB,
                     opRange *opRangeC )
{
    peak_div( opRangeA->minV, opRangeA->maxV,
              opRangeB->minV, opRangeB->maxV,
              &opRangeC->minV, &opRangeC->maxV );
}

void math_func_abs( opRange *opRangeA,
                    opRange *opRangeC )
{
    *opRangeC = *opRangeA;
}
void math_func_exp( opRange *opRangeA,
                    opRange *opRangeC )
{
    opRangeC->minV = exp( opRangeA->minV );

    //bug3103 by hxh
    opRangeC->maxV = exp( opRangeA->maxV )*10;
}
void math_func_lg( opRange *opRangeA,
                   opRange *opRangeC )
{
    opRangeC->minV = fabs( log10( opRangeA->minV ) );
    opRangeC->maxV = fabs( log10( opRangeA->maxV ) );

    //! swap
    if ( opRangeC->minV > opRangeC->maxV )
    {
        float temp;
        temp = opRangeC->minV;
        opRangeC->minV = opRangeC->maxV;
        opRangeC->maxV = temp;
    }
    else
    {}
}
void math_func_ln( opRange *opRangeA,
                   opRange *opRangeC )
{
    Q_ASSERT( opRangeA->minV > 0 );
    Q_ASSERT( opRangeA->maxV > 0 );

    opRangeC->minV = fabs( log( opRangeA->minV ) );
    opRangeC->maxV = fabs( log( opRangeA->maxV ) );

    //! swap
    if ( opRangeC->minV > opRangeC->maxV )
    {
        float temp;
        temp = opRangeC->minV;
        opRangeC->minV = opRangeC->maxV;
        opRangeC->maxV = temp;
    }
    else
    {}
}

void math_func_root( opRange *opRangeA,
                     opRange *opRangeC )
{
    Q_ASSERT( opRangeA->minV > 0 );
    Q_ASSERT( opRangeA->maxV > 0 );

    opRangeC->minV = fabs( sqrt( opRangeA->minV ) );
    opRangeC->maxV = fabs( sqrt( opRangeA->maxV ) );

    //! swap
    if ( opRangeC->minV > opRangeC->maxV )
    {
        float temp;
        temp = opRangeC->minV;
        opRangeC->minV = opRangeC->maxV;
        opRangeC->maxV = temp;
    }
    else
    {}
}

void math_func_axb( opRange *opRangeA,
                    opRange *opRangeC,
                    opAxbContext *pContext )
{
    Q_ASSERT( NULL != pContext );

    opRangeC->minV = fabs( pContext->a * opRangeA->minV
                           + pContext->b );

    opRangeC->maxV = fabs( pContext->a * opRangeA->maxV
                           + pContext->b );

    //! use default range
    if ( opRangeC->minV == 0 ||
         opRangeC->maxV == 0 )
    {
        *opRangeC = *opRangeA;
    }
    else
    {}
}

void math_func_intg( opRange *opRangeA,
                     opRange *opRangeC,
                     opIntgContext *pContext)
{
    Q_ASSERT( NULL != pContext );
    Q_ASSERT( pContext->size > 0 );
    Q_ASSERT( pContext->tInc > 0 );

    //! min -- max
    opRangeC->minV = opRangeA->minV * pContext->size * pContext->tInc;
    opRangeC->maxV = opRangeA->maxV * pContext->size * pContext->tInc;
}
void math_func_diff( opRange *opRangeA,
                     opRange *opRangeC,
                     opDiffContext *pContext)
{
    Q_ASSERT( NULL != pContext );
    Q_ASSERT( pContext->tInc > 0 );

    opRangeC->minV = opRangeA->minV / pContext->tInc ;
    opRangeC->maxV = opRangeA->maxV /pContext->tInc ;
    opRangeC->maxV = 100 * opRangeC->maxV;
}

void math_func_filter( opRange *opRangeA,
                       opRange *opRangeC )
{
    *opRangeC = *opRangeA;
}

void math_freq_fft_db( opRange *opRangeA,
                       opRange *opRangeC )
{
    float dbMin, dbMax;

    dbMin = 20*log10( opRangeA->minV );
    dbMax = 20*log10( opRangeA->maxV );

    float range = dbMax - dbMin;

    opRangeC->minV = range/480;
    opRangeC->maxV = range;
}

//! dbm
//! 10*log10( v2/50/1mw ) = 20*log10(v) + 10*log10(1000/50)
//! = 20*log10(v) + 10 * log10( 20 )
//! = 20*log10(v) + 13.0103
#define dbm_const   (13.0103f)
void math_freq_fft_dbm( opRange *opRangeA,
                       opRange *opRangeC )
{
    math_freq_fft_db( opRangeA,
                      opRangeC );

    opRangeC->minV += dbm_const;
    opRangeC->maxV += dbm_const;
}

void math_freq_fft_vrms( opRange *opRangeA,
                       opRange *opRangeC )
{
    *opRangeC = *opRangeA;
}

void math_trend(float max, float min, opRange *opRangeC)
{
    float range = max - min;
    opRangeC->minV = range/480;  //!  显示1/60屏
    opRangeC->maxV = range*60;  //! 显示为一个像素
}
