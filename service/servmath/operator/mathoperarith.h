#ifndef MATHOPERARITH_H
#define MATHOPERARITH_H

#include "mathoper.h"

class MathOperArith : public MathOper
{
public:
    MathOperArith( MathOperator op );
public:
    virtual DsoErr op( DsoWfm &wfmA,
                     DsoWfm &wfmB,
                     int offset, int len,
                     DsoWfm *pWfmC,
                     opContext *pContext );
};


#endif // MATHOPERARITH_H
