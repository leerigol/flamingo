#ifndef SERVMATHSEL_H
#define SERVMATHSEL_H

#include "../service.h"

/*!
 * \brief The servMathSel class
 * math选择服务
 * - n条math通过math选择服务进入
 * - 进入具体math服务的入口
 */
class servMathSel : public serviceExecutor
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:
    enum enumCmd
    {
         cmd_none = 0,
         cmd_hor_mode_change ,
    };

    servMathSel(QString name );
    int onActive(int onoff);
    virtual void registerSpy();


    int     enterMath1();
    int     enterMath2();
    int     enterMath3();
    int     enterMath4();

    QString getMathStr1();
    QString getMathStr2();
    QString getMathStr3();
    QString getMathStr4();

private:
    int     enterMath(int i = 0);
    QString getMathStr(int i = 0);

private:
    AcquireTimemode mHorMode;
};


class servFFTSel : public serviceExecutor
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:

    servFFTSel(QString name );
    int onActive(int onoff);
    virtual void registerSpy();

    DsoErr  setMathFFT1();
    DsoErr  setMathFFT2();
    DsoErr  setMathFFT3();
    DsoErr  setMathFFT4();

private:
    AcquireTimemode mHorMode;

};
#endif // SERVMATHSEL_H
