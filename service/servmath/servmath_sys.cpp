#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/serveyejit/serveyejit.h"

#include "../../service/servgui/servgui.h"

#include "../../com/fftlib/rfft.h"

#include "../../service/servref/servref.h"
#include "../../service/servdso/servdso.h"
#include "servmath.h"


DsoErr servMath::start()
{
    setMsgAttr( (int)servMath::cmd_trace_updated );

    return ERR_NONE;
}

void servMath::registerSpy()
{
    serviceExecutor::spyOn( E_SERVICE_ID_TRACE,
                            servTrace::cmd_set_provider,
                            mpItem->servId,
                            servMath::cmd_trace_updated );

    serviceExecutor::spyOn( E_SERVICE_ID_EYEJIT,
                            servEyeJit::cmd_jitter_provider,
                            mpItem->servId,
                            servMath::cmd_jitter_updated );

    QList< nameCmdPair *> list;

    nameCmdPair ch1( serv_name_ch1, cmd_ch1_unit_change );
    nameCmdPair ch2( serv_name_ch2, cmd_ch2_unit_change );
    nameCmdPair ch3( serv_name_ch3, cmd_ch3_unit_change );
    nameCmdPair ch4( serv_name_ch4, cmd_ch4_unit_change );

    list.append(&ch1);
    list.append(&ch2);
    list.append(&ch3);
    list.append(&ch4);

    nameCmdPair *pItem;
    foreach( pItem, list )
    {
        spyOn( pItem->first, MSG_CHAN_UNIT, pItem->second );
        spyOn( pItem->first, MSG_CHAN_PROBE, pItem->second );
    }

    spyOn( serv_name_ch1, MSG_CHAN_ON_OFF, cmd_ch1_onoff_change);
    spyOn( serv_name_ch2, MSG_CHAN_ON_OFF, cmd_ch2_onoff_change);
    spyOn( serv_name_ch3, MSG_CHAN_ON_OFF, cmd_ch3_onoff_change);
    spyOn( serv_name_ch4, MSG_CHAN_ON_OFF, cmd_ch4_onoff_change);

    spyOn( serv_name_ref, MSG_REF_ONOFF, cmd_mathref_onoff_change);
    spyOn( serv_name_ref, MSG_REF_SAVE,  cmd_mathref_onoff_change);
    spyOn( serv_name_ref, servRef::MSG_REF_CLEAR_ALL, cmd_mathref_onoff_change);

    for (int i = 0; i < 4; i++ )
    {
        if ( i < (getId() - E_SERVICE_ID_MATH1) )
        {
            spyOn( QString("math%1").arg(i + 1), MSG_MATH_EN, cmd_mathref_onoff_change);
        }
    }

    //for bug1695 by hxh
    spyOn( serv_name_ch1, servVert::user_cmd_on_off, cmd_ch1_onoff_change);
    spyOn( serv_name_ch2, servVert::user_cmd_on_off, cmd_ch2_onoff_change);
    spyOn( serv_name_ch3, servVert::user_cmd_on_off, cmd_ch3_onoff_change);
    spyOn( serv_name_ch4, servVert::user_cmd_on_off, cmd_ch4_onoff_change);

    spyOn( serv_name_la, MSG_LA_ENABLE,         cmd_la_onoff_change);
    spyOn( serv_name_la, MSG_LA_D0D7_ONOFF,     cmd_la_onoff_change);
    spyOn( serv_name_la, MSG_LA_D8D15_ONOFF,    cmd_la_onoff_change);
    spyOn( serv_name_la, MSG_LA_SELECT_CHAN,    cmd_la_onoff_change);
    spyOn( serv_name_la, MSG_LA_SELECT_GROUP,   cmd_la_onoff_change);

    spyOn( serv_name_hori, MSG_HORI_MAIN_SCALE, cmd_trace_sa_change );
    spyOn( serv_name_hori, MSG_HOR_TIME_MODE,   cmd_hor_mode_change );
    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON,     cmd_on_zoom_en );

    spyOn( serv_name_display, MSG_DISPLAY_CLEAR, cmd_on_clear );

    //! m1/2/3/4 disable
    spyOn( serv_name_dso, servDso::CMD_SCREEN_MODE);

    //!  add_by_zx:delete for bug 2541
//    //! math offset
//    servGui::registerNotify( getName(),
//                             MSG_MATH_VIEW_OFFSET,
//                             Math_color,
//                             tr("Offset:"),
//                             /*cmd_q_real_offset*/cmd_q_real_disp_offset, //![dba, bug:524]
//                             "",
//                             cmd_q_unit_string );

//    servGui::registerNotify( getName(),
//                             MSG_MATH_FFT_OFFSET,
//                             Math_color,
//                             tr("Offset:"),
//                             cmd_q_real_offset,
//                             "",
//                             cmd_q_unit_string );      

    //! threshold1
    servGui::registerNotify( getName(),
                             MSG_MATH_S32LOGIC1THRE,
                             CH1_color,
                             tr("Threshold:"),
                             cmd_ch1_threshold_change,
                             "",
                             cmd_q_ch1_threshold_unit_string );

    servGui::registerNotify( getName(),
                             MSG_MATH_S32LOGIC2THRE,
                             CH2_color,
                             tr("Threshold:"),
                             cmd_ch2_threshold_change,
                             "",
                             cmd_q_ch2_threshold_unit_string );

    servGui::registerNotify( getName(),
                             MSG_MATH_S32LOGIC3THRE,
                             CH3_color,
                             tr("Threshold:"),
                             cmd_ch3_threshold_change,
                             "",
                             cmd_q_ch3_threshold_unit_string );

    servGui::registerNotify( getName(),
                             MSG_MATH_S32LOGIC4THRE,
                             CH4_color,
                             tr("Threshold:"),
                             cmd_ch4_threshold_change,
                             "",
                             cmd_q_ch4_threshold_unit_string );
}

#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

#define SERIAL_OP   stream.write

/*!
 * \brief servMath::serialOut
 * \param stream
 * \param ver
 * \return
 * 对称写输入／输出序列化
 */
int servMath::serialOut( CStream &stream, serialVersion &ver )
{
    //! out the data version
    ver = mVersion;

    SERIAL_OP( "operator", (quint8)mOperator );
    SERIAL_OP( "enable", mMathEn );

    SERIAL_OP("invert", mInvert );
    SERIAL_OP("view_offset", mViewOffset.mVal );

    SERIAL_OP("label", mLabelOn );
    SERIAL_OP("label_str", mLabel );
    SERIAL_OP("color_onoff", mbColorOnOff );

    SERIAL_OP("analog_a", (quint8)mSrc.mAnaSrc.mSrcA );
    SERIAL_OP("analog_b", (quint8)mSrc.mAnaSrc.mSrcB );

    SERIAL_OP("logic_a", (quint8)mSrc.mLaSrc.mSrcA );
    SERIAL_OP("logic_a", (quint8)mSrc.mLaSrc.mSrcB );

    SERIAL_OP("fft_a", (quint8)mSrc.mFftSrc.mSrc);

    //! cfg
    SERIAL_OP("analog_scale", mCfg.mAnaCfg.mScale.mVal );
    SERIAL_OP("freq_db_scale", mCfg.mFreqCfgDb.mScale.mVal );
    SERIAL_OP("freq_rms_scale", mCfg.mFreqCfgVrms.mScale.mVal );

    SERIAL_OP("freq_h_offset", mCfg.mFreqHCfg.mOffset.mVal );
    SERIAL_OP("freq_h_scale", mCfg.mFreqHCfg.mScale.mVal );
    SERIAL_OP("freq_h_center", mCfg.mFreqHCfg.mCenter.mVal );
    SERIAL_OP("freq_h_span", mCfg.mFreqHCfg.mSpan.mVal );

    //! peak cfg
    SERIAL_OP( "peakOrder", (quint8) mPeakCfg.peakOrder );
    SERIAL_OP( "peakthres", mPeakCfg.m_thresHold );
    SERIAL_OP( "peakExcur", mPeakCfg.m_excursion );
    SERIAL_OP( "peakEnable", mPeakCfg.m_peakEnable );
    SERIAL_OP( "m_peakMaxCount", mPeakCfg.m_peakMaxCount.mVal );

    //! option
    SERIAL_OP("diff_window", mOption.mDiffOpt.mWindowSize );

    SERIAL_OP("fft_dc_suppress", mOption.mFftOpt.mDcSuppress );
    SERIAL_OP("fft_screen", (quint8)mScreenFFT );
    SERIAL_OP("fft_window", (quint8)mOption.mFftOpt.mWindow );
    SERIAL_OP("fft_xtype", (quint8)mOption.mFftOpt.mxType );
    SERIAL_OP("fft_yunit", (quint8)mOption.mFftOpt.myUnit );

    SERIAL_OP("intg_bias.a", (qlonglong)mOption.mIntgOpt.mBias.mA );
    SERIAL_OP("intg_bias.e", (int)mOption.mIntgOpt.mBias.mE );

    SERIAL_OP("la_scale", (quint8)mOption.mLaOpt.mScale );
    SERIAL_OP("la_sens", (quint8)mOption.mLaOpt.mSens );
    SERIAL_OP("la_th1", mOption.mLaOpt.mThresholds[0] );
    SERIAL_OP("la_th2", mOption.mLaOpt.mThresholds[1] );
    SERIAL_OP("la_th3", mOption.mLaOpt.mThresholds[2] );
    SERIAL_OP("la_th4", mOption.mLaOpt.mThresholds[3] );

    SERIAL_OP("line_a.a", (qlonglong)mOption.mLineOpt.a.mA );
    SERIAL_OP("line_a.e", (int)mOption.mLineOpt.a.mE );
    SERIAL_OP("line_b.a", (qlonglong)mOption.mLineOpt.b.mA );
    SERIAL_OP("line_b.e", (int)mOption.mLineOpt.b.mE );


    return 0;
}

#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

/*!< \brief serial operation
 *
 * serial OPeration
 * serial OPeration Converter
*/
#define SERIAL_OP   stream.read
#define SERIAL_OP_C( a, b, c, type )   stream.read( a, c ); \
                                       memset( &b, 0, sizeof(b));\
                                       memcpy( &b, &c, sizeof(type) );
int servMath::serialIn( CStream &stream, serialVersion ver )
{
    //! version compatible check

    quint8 qu8Dat;
    qint32 qi32Data;
    if( ver == mVersion )
    {
        //! serial in
        SERIAL_OP_C( "operator", mOperator, qu8Dat, quint8 );
        SERIAL_OP( "enable", mMathEn );

        if( mMathEn )
        {
            //for bug3425. by hxh
            mathThread::m_u64DelayStart = 21;
        }

        SERIAL_OP("invert", mInvert );
        SERIAL_OP("view_offset", mViewOffset.mVal );

        SERIAL_OP("label", mLabelOn );
        SERIAL_OP("label_str", mLabel );
        SERIAL_OP("color_onoff", mbColorOnOff );

        SERIAL_OP_C("analog_a", mSrc.mAnaSrc.mSrcA, qu8Dat, quint8 );
        SERIAL_OP_C("analog_b", mSrc.mAnaSrc.mSrcB, qu8Dat, quint8 );

        SERIAL_OP_C("logic_a", mSrc.mLaSrc.mSrcA, qu8Dat, quint8 );
        SERIAL_OP_C("logic_a", mSrc.mLaSrc.mSrcB, qu8Dat, quint8 );

        SERIAL_OP_C("fft_a", mSrc.mFftSrc.mSrc, qu8Dat, quint8 );

        //! cfg
        SERIAL_OP("analog_scale", mCfg.mAnaCfg.mScale.mVal );
        SERIAL_OP("freq_db_scale", mCfg.mFreqCfgDb.mScale.mVal );
        SERIAL_OP("freq_rms_scale", mCfg.mFreqCfgVrms.mScale.mVal );

        SERIAL_OP("freq_h_offset", mCfg.mFreqHCfg.mOffset.mVal );
        SERIAL_OP("freq_h_scale", mCfg.mFreqHCfg.mScale.mVal );
        SERIAL_OP("freq_h_center", mCfg.mFreqHCfg.mCenter.mVal );
        SERIAL_OP("freq_h_span", mCfg.mFreqHCfg.mSpan.mVal );

        //! peak cfg
        SERIAL_OP_C( "peakOrder", mPeakCfg.peakOrder, qu8Dat, quint8 );
        SERIAL_OP( "peakthres", mPeakCfg.m_thresHold );
        SERIAL_OP( "peakExcur", mPeakCfg.m_excursion );
        SERIAL_OP( "peakEnable", mPeakCfg.m_peakEnable );
        SERIAL_OP( "m_peakMaxCount", mPeakCfg.m_peakMaxCount.mVal );

        //! option
        SERIAL_OP("diff_window", mOption.mDiffOpt.mWindowSize );

        SERIAL_OP("fft_dc_suppress", mOption.mFftOpt.mDcSuppress );
        SERIAL_OP_C("fft_screen", mScreenFFT, qu8Dat, quint8 );
        SERIAL_OP_C("fft_window", mOption.mFftOpt.mWindow, qu8Dat, quint8 );
        SERIAL_OP_C("fft_xtype", mOption.mFftOpt.mxType, qu8Dat, quint8 );
        SERIAL_OP_C("fft_yunit", mOption.mFftOpt.myUnit, qu8Dat, quint8 );

        SERIAL_OP("intg_bias.a", mOption.mIntgOpt.mBias.mA );
        SERIAL_OP_C("intg_bias.e", mOption.mIntgOpt.mBias.mE, qi32Data, qint32 );

        SERIAL_OP_C("la_scale", mOption.mLaOpt.mScale, qu8Dat, quint8 );
        SERIAL_OP_C("la_sens", mOption.mLaOpt.mSens, qu8Dat, quint8 );
        SERIAL_OP("la_th1", mOption.mLaOpt.mThresholds[0] );
        SERIAL_OP("la_th2", mOption.mLaOpt.mThresholds[1] );
        SERIAL_OP("la_th3", mOption.mLaOpt.mThresholds[2] );
        SERIAL_OP("la_th4", mOption.mLaOpt.mThresholds[3] );

        SERIAL_OP("line_a.a", mOption.mLineOpt.a.mA );
        SERIAL_OP_C("line_a.e", mOption.mLineOpt.a.mE, qi32Data, qint32 );
        SERIAL_OP("line_b.a", mOption.mLineOpt.b.mA );
        SERIAL_OP_C("line_b.e", mOption.mLineOpt.a.mE, qi32Data, qint32 );
    }
    else
    {
        rst();
    }
    return 0;
}

void servMath::rst()
{
    //! common
//    mOperator = operator_fft;
    mOperator = operator_add;
    mMathEn = false;

    mHorMode = Acquire_YT;
    mInvert = false;

    mLaOffset.mVal = 0;
    mLaOffset.mMax = math_view_offset_max;
    mLaOffset.mMin = math_view_offset_min;

    mViewOffset.mVal = 0;
    mViewOffset.mMax = math_view_offset_max;
    mViewOffset.mMin = math_view_offset_min;

    mLabelOn = false;
    mLabel = servMath::_labelList.at(0);

    //! src
    mSrc.mAnaSrc.mSrcA = chan1;
    mSrc.mAnaSrc.mSrcB = chan1;

    mSrc.mLaSrc.mSrcA = chan1;
    mSrc.mLaSrc.mSrcB = chan1;

    mSrc.mFftSrc.mSrc = chan1;

    for( int i=d0; i<=d15; i++)
    {
        mUiAttr.setEnable( MSG_MATH_S32LOGICA, i, false);
        mUiAttr.setEnable( MSG_MATH_S32LOGICB, i, false);
    }

    m_nNormalChanA   = 0x1e;;
    m_nNormalChanB   = 0x1e;;
    m_nFFTChan             = 0x1e;;
    m_nLogicChanA        = 0x1e;;
    m_nLogicChanB        = 0x1e;;


    //! cfg
    mCfg.mAnaCfg.mScale.mVal = 500*math_mv;
    mCfg.mAnaCfg.mScale.mMax = 10*math_v;
    mCfg.mAnaCfg.mScale.mMin = 1*math_mv;

    mCfg.mFreqCfgDb.mScale.mVal = 2*math_v;
    mCfg.mFreqCfgDb.mScale.mMax = 100*math_v;
    mCfg.mFreqCfgDb.mScale.mMin = 10*math_mv;

    mCfg.mFreqCfgVrms.mScale.mVal = 10*math_v;
    mCfg.mFreqCfgVrms.mScale.mMax = 100*math_v;
    mCfg.mFreqCfgVrms.mScale.mMin = 10*math_mv;

    //! hconfig
    mCfg.mFreqHCfg.mCenter.mVal = 5*math_Mhz;
    mCfg.mFreqHCfg.mCenter.mMin = 0;
    mCfg.mFreqHCfg.mCenter.mMax = 200*math_Mhz;

    mCfg.mFreqHCfg.mSpan.mVal = 10*math_Mhz;
    mCfg.mFreqHCfg.mSpan.mMin = 10*math_Mhz;
    mCfg.mFreqHCfg.mSpan.mMax = 200*math_Mhz;

    mCfg.mFreqHCfg.mStart.mVal = 0*math_Mhz;
    mCfg.mFreqHCfg.mEnd.mVal = 10*math_Mhz;

    //! fft dots
    mCfg.mFreqHCfg.mSize = 2048;
    mCfg.mFreqHCfg.mSaRate.mVal = 400*math_Mhz;

    mCfg.mFreqHCfg.mScale.mVal = 1*math_Mhz;
    mCfg.mFreqHCfg.mOffset.mVal = 0;
    mPeakCfg.peakOrder = fft_peak_AmpOrder;
    mPeakCfg.m_peakMaxCount.mVal = 5;
    mPeakCfg.m_peakEnable = false;
    mPeakCfg.m_thresHold = 5.5*math_v;
    mPeakCfg.m_excursion = 1.8*math_v;

    //! option
    mOption.mDiffOpt.mWindowSize = 5;

    mOption.mFftOpt.mDcSuppress = true;
    mOption.mFftOpt.mWindow = fft_hanning;
    mOption.mFftOpt.mxType = fft_start_end;
    mOption.mFftOpt.myUnit = fft_spec_db;
    mScreenFFT = fft_screen_full;
    mFFTCount  = 0;

    mOption.mIntgOpt.mBias = 0;

    mOption.mLaOpt.mScale = Medium;
    mOption.mLaOpt.mSens = 3;
    mOption.mLaOpt.mThresholds[0] = 0;
    mOption.mLaOpt.mThresholds[1] = 0;
    mOption.mLaOpt.mThresholds[2] = 0;
    mOption.mLaOpt.mThresholds[3] = 0;

    mOption.mLineOpt.a = 0;
    mOption.mLineOpt.b = 0;

    //! filter
    mOption.mFilterOpt.mLpW = 1;
    mOption.mFilterOpt.mHpW = 1;
    mOption.mFilterOpt.mBpW1 = 1;
    mOption.mFilterOpt.mBpW2 = 2;

    mOption.mFilterOpt.mBtW1 = 1;
    mOption.mFilterOpt.mBtW2 = 2;


    //add by hxh for bug1317
    mUiAttr.setEnable(MSG_MATH_S32FFTSCR, true);
    mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_lp, true);
    mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_hp, true);
    mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_bp, true);
    mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_bt, true);

    //bug3098 by hxh
    mUiAttr.setEnable( MSG_MATH_S32LOGIC1THRE, true);
    for(int i=chan1; i<=chan4; i++)
    {
        mUiAttr.setEnable( MSG_MATH_S32ARITHA, i, true);
        mUiAttr.setEnable( MSG_MATH_S32ARITHB, i, true);
        mUiAttr.setEnable( MSG_MATH_S32FFTSRC, i, true);
        mUiAttr.setEnable( MSG_MATH_S32LOGICA, i, true);
        mUiAttr.setEnable( MSG_MATH_S32LOGICB, i, true);
    }

    servMath::mScreenFFT = fft_screen_full;
    mZoomEn       = false;
    mZoomEnBefore = false;
    mbColorOnOff  = false;
}

int servMath::onActive(int onoff)
{
    if ( onoff )
    {
        if(this->isActive())
        {
           async(CMD_SERVICE_DEACTIVE,0);
        }
        else
        {
            if( mHorMode ==  Acquire_XY)
            {
                return ERR_ACTION_DISABLED;
            }
            else
            {
                setActive();
            }
        }
    }
    else
    {
        async(CMD_SERVICE_DEACTIVE,0);
    }
    return ERR_NONE;
}

int servMath::startup()
{
    mbMathStarted = false;

    int i;
    for (i = 0; i < 10; i++ )
    {
        mUiAttr.setVisible( MSG_MATH_S32FFTSRC, r1 + i, false );
    }
    mUiAttr.setVisible( MSG_MATH_S32MATHOPERATOR, operator_trend, false);

    //! m1/2/3/4 disable
    int id = getId();
    for (i = 0; i < 4; i++ )
    {
        if ( i >= (id - E_SERVICE_ID_MATH1) )
        {
            mUiAttr.setVisible( MSG_MATH_S32ARITHA, m1 + i, false );
            mUiAttr.setVisible( MSG_MATH_S32ARITHB, m1 + i, false );
        }
    }
    mUiAttr.setVisible( MSG_MATH_S32FFTSCR, false );

    for(i = 0; i < 4; i++)
    {
        mUiAttr.setVisible( MSG_MATH_S32FFTSRC, m1 + i, false);
    }


    //! add_by_zx : for Yt、Xy、Roll模式之间的切换速度比较慢
    //    updateAllUi( false );


    on_cmd_la_onoff_change();
    ssync(MSG_MATH_EN, mMathEn);
    ssync(MSG_MATH_S32MATHOPERATOR, mOperator);

    //by hxh for 1779
    {
        int mode;
        query(serv_name_hori, MSG_HOR_TIME_MODE, mode);
        if(mode == Acquire_ROLL)
        {
            mUiAttr.setEnable(MSG_MATH_S32FFTSCR, false);
        }
        mHorMode = (AcquireTimemode)mode;
    }

    dsoVert::yRefDiv = adc_vdiv_dots;
    dsoVert::setyInc( getAnaScale() * math_v_base / adc_vdiv_dots );
    dsoVert::setyRefScale( getAnaScale() );
    dsoVert::setyRefOffset( getAnaOffset() );
    dsoVert::setyGnd( adc_center + getAnaOffset() * adc_vdiv_dots / getYRefScale() );
    dsoVert::yUnit = getUnit();
    dsoVert::yRefBase.setVal(1, E_N9);

    mbMathStarted = true;
    return 0;
}

/*!
 * \brief servMath::init
 * 只在启动时运行一次
 *
 */
void servMath::init()
{
    if ( !servMath::_windowInited )
    {
        //! set window path
        r_meta::CBMeta meta;
        QString path, file;

        //! fft window
        meta.getMetaVal( "math/window/path", path );

        meta.getMetaVal( "math/window/blackman", file );
        rfft::RFFT::addWindow( fft_blackman, path + file );

        meta.getMetaVal( "math/window/hanning", file );
        rfft::RFFT::addWindow( fft_hanning, path + file );

        meta.getMetaVal( "math/window/hamming", file );
        rfft::RFFT::addWindow( fft_hamming, path + file );

        meta.getMetaVal( "math/window/flattop", file );
        rfft::RFFT::addWindow( fft_flattop, path + file );

        meta.getMetaVal( "math/window/triangle", file );
        rfft::RFFT::addWindow( fft_triangle, path + file );

        servMath::_windowInited = true;
    }

    bool en = sysCheckLicense(OPT_MSO);
    if(!en)
    {
        for(int i = d0; i <= d15; i++)
        {
            mUiAttr.setVisible(MSG_MATH_S32LOGICA, i, en);
            mUiAttr.setVisible(MSG_MATH_S32LOGICB, i, en);
        }
    }
}

void servMath::onClear()
{
    if(getMathEn())
    {
        m_pWorkThread->disconnectClient(this);
        m_pWfmMainResult->setRange(0,0);
        m_pWfmZoomResult->setRange(0,0);
        m_pWfmMainScr->setRange(0,0);
        m_pWfmZoomScr->setRange(0,0);

        if(sysGetStarted())
        {
            mPeakCfg.m_peakBuffer.peakDataMutex.lock();
            mPeakCfg.m_peakBuffer.pointBuffer.clear();
            defer(cmd_q_fft_peak);
            mPeakCfg.m_peakBuffer.peakDataMutex.unlock();
        }
    }
}

void servMath::unFreezeMath()
{
    m_bFreezedMath = false;
}

bool servMath::getZoomEn()
{
    return mZoomEn;
}

bool servMath::getZoomEnBefore()
{
    return mZoomEnBefore;
}

void servMath::resetZoomEnBefore()
{
    mZoomEnBefore = false;
}
