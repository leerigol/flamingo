
#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "../../service/servdso/servdso.h"
#include "servmath.h"

void servMath::on_cmd_math_unit_update()
{
    dsoVert::yUnit = getUnit();

    setuiChange( MSG_MATH_VIEW_OFFSET );
    setuiChange( MSG_MATH_S32FUNCSCALE );

    setuiChange( MSG_MATH_S32LOGIC1THRE );
    setuiChange( MSG_MATH_S32LOGIC2THRE );
    setuiChange( MSG_MATH_S32LOGIC3THRE );
    setuiChange( MSG_MATH_S32LOGIC4THRE );

    setuiChange( MSG_MATH_FFT_SCALE );
}

void servMath::on_cmd_ch1_unit_change()
{
    defer( cmd_math_unit_update );
}
void servMath::on_cmd_ch2_unit_change()
{
    defer( cmd_math_unit_update );
}
void servMath::on_cmd_ch3_unit_change()
{
    defer( cmd_math_unit_update );
}
void servMath::on_cmd_ch4_unit_change()
{
    defer( cmd_math_unit_update );
}

void servMath::on_cmd_ch1_onoff_change()
{
    bool b = false;
    query( serv_name_ch1,   MSG_CHAN_ON_OFF, b);
    findChanA(chan1, b);
    findChanB(chan1, b);
    findLogicChanA(chan1, b);
    findLogicChanB(chan1, b);
    findFFTChan(chan1, b);
}

void servMath::on_cmd_ch2_onoff_change()
{
    bool b = false;
    query( serv_name_ch2,   MSG_CHAN_ON_OFF, b);
    findChanA(chan2, b);
    findChanB(chan2, b);
    findLogicChanA(chan2, b);
    findLogicChanB(chan2, b);
    findFFTChan(chan2, b);
}

void servMath::on_cmd_ch3_onoff_change()
{
    bool b = false;
    query( serv_name_ch3,   MSG_CHAN_ON_OFF, b);
    findChanA(chan3, b);
    findChanB(chan3, b);
    findLogicChanA(chan3, b);
    findLogicChanB(chan3, b);
    findFFTChan(chan3, b);
}

void servMath::on_cmd_ch4_onoff_change()
{
    bool b = false;
    query( serv_name_ch4,   MSG_CHAN_ON_OFF, b);
    findChanA(chan4, b);
    findChanB(chan4, b);
    findLogicChanA(chan4, b);
    findLogicChanB(chan4, b);
    findFFTChan(chan4, b);
}

void servMath::findChanA(int except, bool en)
{
    qint64 mask = m_nNormalChanA;
    if( en )
    {
        m_nNormalChanA |=  (qint64)1 << except;
    }
    else
    {
        m_nNormalChanA &= ~((qint64)1 << except);
    }

    mUiAttr.setEnable( MSG_MATH_S32ARITHA, except, en);

    if( (mSrc.mAnaSrc.mSrcA == except && en == false) || mask == 0 )
    {
        bool bChanEnable = false;
        for(int c = chan1; c <= chan4; c++)
        {
            if( except == c && !en)
            {
                continue;
            }
            if( m_nNormalChanA & (1 << c) )
            {
                mSrc.mAnaSrc.mSrcA  = (Chan)c ;
                bChanEnable = true;
                break;
            }
        }

        if( !bChanEnable )
        {
            for(int c=m1; c<=m4; c++)
            {
                if( except == c && !en)
                {
                    continue;
                }
                if( m_nNormalChanA & ((qint64)1 << c) )
                {
                    mSrc.mAnaSrc.mSrcA  = (Chan)c ;
                    bChanEnable = true;
                    break;
                }
            }
        }

        if( !bChanEnable )
        {
            for(int c=r1; c<=r10; c++)
            {
                if( except == c && !en)
                {
                    continue;
                }
                if( m_nNormalChanA & ((qint64)1 << c) )
                {
                    mSrc.mAnaSrc.mSrcA  = (Chan)c ;
                    bChanEnable = true;
                    break;
                }
            }
        }
        async(MSG_MATH_S32ARITHA, mSrc.mAnaSrc.mSrcA);
    }
}

void servMath::findChanB(int except, bool en)
{
    qint64 mask = m_nNormalChanB;
    if( en )
    {
        m_nNormalChanB |=  (qint64)1 << except;
    }
    else
    {
        m_nNormalChanB &= ~((qint64)1 << except);
    }

    mUiAttr.setEnable( MSG_MATH_S32ARITHB, except, en);

    if( (mSrc.mAnaSrc.mSrcB == except && en == false) || mask == 0 )
    {
        bool bChanEnable = false;
        for(int c = chan1; c <= chan4; c++)
        {
            if( except == c && !en)
            {
                continue;
            }
            if( m_nNormalChanB & (1 << c) )
            {
                mSrc.mAnaSrc.mSrcB  = (Chan)c ;
                bChanEnable = true;
                break;
            }
        }

        if( !bChanEnable )
        {
            for(int c=m1; c<=m4; c++)
            {
                if( except == c && !en)
                {
                    continue;
                }
                if( m_nNormalChanB & ((qint64)1 << c) )
                {
                    mSrc.mAnaSrc.mSrcB  = (Chan)c ;
                    bChanEnable = true;
                    break;
                }
            }
        }    
    async(MSG_MATH_S32ARITHB, mSrc.mAnaSrc.mSrcB);
    }
}

void servMath::findLogicChanA(int except, bool en)
{
    qint64 mask = m_nLogicChanA;
    if( en )
    {
        m_nLogicChanA |=  (qint64)1 << except;
    }
    else
    {
        m_nLogicChanA &= ~((qint64)1 << except);
    }

    mUiAttr.setEnable( MSG_MATH_S32LOGICA, except, en);

    if( (mSrc.mLaSrc.mSrcA == except && en == false) || mask == 0 )
    {
        bool bChanEnable = false;
        for(int c = chan1; c <= chan4; c++)
        {
            if( except == c && !en)
            {
                continue;
            }
            if( m_nLogicChanA & (1 << c) )
            {
                mSrc.mLaSrc.mSrcA  = (Chan)c ;
                bChanEnable = true;
                break;
            }
        }

        if( !bChanEnable )
        {
            for(int c=m1; c<=m4; c++)
            {
                if( except == c && !en)
                {
                    continue;
                }
                if( m_nLogicChanA & ((qint64)1 << c) )
                {
                    mSrc.mLaSrc.mSrcA  = (Chan)c ;
                    bChanEnable = true;
                    break;
                }
            }
        }
    }

    async(MSG_MATH_S32LOGICA, mSrc.mLaSrc.mSrcA);
}

void servMath::findLogicChanB(int except, bool en)
{
    qint64 mask = m_nLogicChanB;
    if( en )
    {
        m_nLogicChanB |=  (qint64)1 << except;
    }
    else
    {
        m_nLogicChanB &= ~((qint64)1 << except);
    }

    mUiAttr.setEnable( MSG_MATH_S32LOGICB, except, en);

    if( (mSrc.mLaSrc.mSrcB == except && en == false) || mask == 0 )
    {
        bool bChanEnable = false;
        for(int c = chan1; c <= chan4; c++)
        {
            if( except == c && !en)
            {
                continue;
            }
            if( m_nLogicChanB & (1 << c) )
            {
                mSrc.mLaSrc.mSrcB  = (Chan)c ;
                bChanEnable = true;
                break;
            }
        }

        if( !bChanEnable )
        {
            for(int c=m1; c<=m4; c++)
            {
                if( except == c && !en)
                {
                    continue;
                }
                if( m_nLogicChanB & ((qint64)1 << c) )
                {
                    mSrc.mLaSrc.mSrcB  = (Chan)c ;
                    bChanEnable = true;
                    break;
                }
            }
        }
    }

    async(MSG_MATH_S32LOGICB, mSrc.mLaSrc.mSrcB);
}

void servMath::findFFTChan(int except, bool en)
{
    qint64 mask = m_nFFTChan;
    if( en )
    {
        m_nFFTChan |=  (qint64)1 << except;
    }
    else
    {
        m_nFFTChan &= ~((qint64)1 << except);
    }

    mUiAttr.setEnable( MSG_MATH_S32FFTSRC, except, en);

    if( (mSrc.mFftSrc.mSrc == except && en == false) || mask == 0 )
    {
        for(int c = chan1; c <= chan4; c++)
        {
            if( except == c && !en)
            {
                continue;
            }
            if( m_nFFTChan & (1 << c) )
            {
                mSrc.mFftSrc.mSrc  = (Chan)c ;
                break;
            }
        }
    }

    async(MSG_MATH_S32FFTSRC, mSrc.mFftSrc.mSrc);
}

void servMath::onMathSrcEnChange(Chan ch)
{
    switch( mOperator)
    {
    case operator_add:
    case operator_sub:
    case operator_mul:
    case operator_div:
        if( mSrc.mAnaSrc.mSrcA == ch || mSrc.mAnaSrc.mSrcB == ch)
        {
            async(MSG_MATH_EN, false);
        }
        break;
    case operator_and:
    case operator_or:
    case operator_xor:
        if( mSrc.mLaSrc.mSrcA == ch || mSrc.mLaSrc.mSrcB == ch)
        {
            async(MSG_MATH_EN, false);
        }
        break;
    case operator_not:
        if( mSrc.mLaSrc.mSrcA == ch)
        {
            async(MSG_MATH_EN, false);
        }
        break;
    case operator_fft:
        if( mSrc.mFftSrc.mSrc == ch)
        {
            async(MSG_MATH_EN, false);
        }
        break;
    default:
        if( mSrc.mAnaSrc.mSrcA == ch)
        {
            async(MSG_MATH_EN, false);
        }
        break;
    }
}


void servMath::on_cmd_la_onoff_change(void)
{
    bool laEn = false;
    serviceExecutor::query( serv_name_la, MSG_LA_ENABLE, laEn );
    if( laEn)
    {
        int dxOnOff;
        serviceExecutor::query( serv_name_la, MSG_LA_SELECT_CHAN, dxOnOff );
        LOG_DBG()<<"dxOnOff = "<<dxOnOff;
        for( int i=d0; i<=d15; i++)
        {
            int labit = get_bit(dxOnOff, i);
            if( labit)
            {
                m_nLogicChanA |= (qint64)1 << i;
                m_nLogicChanB |= (qint64)1 << i;
            }
            else
            {
                m_nLogicChanA &= ~((qint64)1 << i);
                m_nLogicChanB &= ~((qint64)1 << i);
            }
            mUiAttr.setEnable( MSG_MATH_S32LOGICA, i, labit);
            mUiAttr.setEnable( MSG_MATH_S32LOGICB, i, labit);
            if( (mSrc.mLaSrc.mSrcA == i && labit == false) || m_nLogicChanA == 0 )
            {
                for(int c = chan1; c <= chan4; c++)
                {
                    if( i == c && !labit)
                    {
                        continue;
                    }
                    if( m_nLogicChanA & (1 << c) )
                    {
                        mSrc.mLaSrc.mSrcA = (Chan)c;
                        break;
                    }
                }
            }

            if( (mSrc.mLaSrc.mSrcB == i && labit == false) || m_nLogicChanB == 0 )
            {
                for(int c = chan1; c <= chan4; c++)
                {
                    if( i == c && !labit)
                    {
                        continue;
                    }
                    if( m_nLogicChanB & (1 << c) )
                    {
                        mSrc.mLaSrc.mSrcB = (Chan)c;
                        break;
                    }
                }
            }
        }
    }
    else
    {
        for( int i=d0; i<=d15; i++)
        {
            m_nLogicChanA &= ~((qint64)1 << i);
            m_nLogicChanB &= ~((qint64)1 << i);
            mUiAttr.setEnable( MSG_MATH_S32LOGICA, i, laEn);
            mUiAttr.setEnable( MSG_MATH_S32LOGICB, i, laEn);

            for(int c = chan1; c <= chan4; c++)
            {
                if( m_nLogicChanA & (1 << c) )
                {
                    mSrc.mLaSrc.mSrcA = (Chan)c;
                    break;
                }
            }

            for(int c = chan1; c <= chan4; c++)
            {
                if( m_nLogicChanB & (1 << c) )
                {
                    mSrc.mLaSrc.mSrcB = (Chan)c;
                    break;
                }
            }
        }
    }

    async(MSG_MATH_S32LOGICA, mSrc.mLaSrc.mSrcA);
    async(MSG_MATH_S32LOGICB, mSrc.mLaSrc.mSrcB);
}

void servMath::on_cmd_trace_sa_change()
{
    if(getColorOnOff())
    {
        defer(MSG_MATH_COLOR_RESET);
    }

    switch( mOperator)
    {
    case operator_lp:
        async(MSG_MATH_S32FILTER_LP, mOption.mFilterOpt.mLpW);
        break;
    case operator_hp:
        async(MSG_MATH_S32FILTER_HP, mOption.mFilterOpt.mHpW);
        break;
    case operator_bp:
        async(MSG_MATH_S32FILTER_BP1, mOption.mFilterOpt.mBpW1);
        async(MSG_MATH_S32FILTER_BP2, mOption.mFilterOpt.mBpW2);
        break;
    case operator_bt:
        async(MSG_MATH_S32FILTER_BT1, mOption.mFilterOpt.mBtW1);
        async(MSG_MATH_S32FILTER_BT2, mOption.mFilterOpt.mBtW2);
        break;
    case operator_fft:
        if(mOption.mFftOpt.mFFtAutoScale)
        {
            mbNeedAutoScale = true;
        }
        break;
    default:
        break;
    }
}

void servMath::on_scr_mode_change()
{
    int scrMode;
    serviceExecutor::query( serv_name_dso,
                            servDso::CMD_SCREEN_MODE,
                            scrMode);
    if( scrMode == screen_yt_main )
    {
        mUiAttr.setEnable( MSG_MATH_COLOR_ONOFF, true );
    }
    else
    {
        if(getColorOnOff())
        {
           id_async(getId(), MSG_MATH_COLOR_ONOFF, false );
        }
        mUiAttr.setEnable( MSG_MATH_COLOR_ONOFF, false );
    }
}

float servMath::on_cmd_q_ch1_threshold_float()
{
    float fRatio;
    dsoVert::getCH( chan1 )->getProbe().toReal( fRatio, 1.0f );

    return mOption.mLaOpt.mThresholds[0]*fRatio*math_threshold_base;
}
float servMath::on_cmd_q_ch2_threshold_float()
{
    float fRatio;
    serviceExecutor::query( serv_name_ch2,
                            servCH::qcmd_probe_real,
                            fRatio );

    return mOption.mLaOpt.mThresholds[1]*fRatio*math_threshold_base;
}
float servMath::on_cmd_q_ch3_threshold_float()
{
    float fRatio;
    serviceExecutor::query( serv_name_ch3,
                            servCH::qcmd_probe_real,
                            fRatio );

    return mOption.mLaOpt.mThresholds[2]*fRatio*math_threshold_base;
}
float servMath::on_cmd_q_ch4_threshold_float()
{
    float fRatio;
    dsoVert::getCH( chan1 )->getProbe().toReal( fRatio, 1.0f );
//    serviceExecutor::query( serv_name_ch4,
//                            servCH::qcmd_probe_real,
//                            fRatio );

    return mOption.mLaOpt.mThresholds[3]*fRatio*math_threshold_base;
}

QString servMath::on_cmd_q_ch1_threshold_unit_string()
{
    Unit unit;

    unit = dsoVert::getCH(chan1)->getUnit();

    return gui_fmt::CGuiFormatter::toString( unit );
}
QString servMath::on_cmd_q_ch2_threshold_unit_string()
{
    Unit unit;

    unit = dsoVert::getCH(chan2)->getUnit();

    return gui_fmt::CGuiFormatter::toString( unit );
}
QString servMath::on_cmd_q_ch3_threshold_unit_string()
{
    Unit unit;

    unit = dsoVert::getCH(chan3)->getUnit();

    return gui_fmt::CGuiFormatter::toString( unit );
}
QString servMath::on_cmd_q_ch4_threshold_unit_string()
{
    Unit unit;

    unit = dsoVert::getCH(chan4)->getUnit();

    return gui_fmt::CGuiFormatter::toString( unit );
}

int servMath::on_cmd_hori_mode_change(int mode)
{
    if( mode == Acquire_XY )
    {
        if( mMathEn )
        {
            async(MSG_MATH_EN, false);
            setMathEn(false);
        }
    }
    else
    {
        //by hxh for 1779
        bool enZoom = (mode == Acquire_ROLL);
        mUiAttr.setEnable(MSG_MATH_S32FFTSCR, !enZoom);
    }

    if(mode == Acquire_ROLL)
    {
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_fft, false);
        if(mOperator == operator_fft && mMathEn)
        {
            async(MSG_MATH_EN, false);
            async(MSG_MATH_S32MATHOPERATOR, operator_add);
        }
    }
    else
    {
        mUiAttr.setEnable(MSG_MATH_S32MATHOPERATOR, operator_fft, true);
    }
    mHorMode = (AcquireTimemode) mode;
    return ERR_NONE;
}

