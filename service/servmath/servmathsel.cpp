#include "../service_msg.h"
#include "../service_name.h"
#include "servmath.h"
#include "servmathsel.h"

IMPLEMENT_CMD( serviceExecutor, servMathSel  )
start_of_entry()
on_set_int_int( CMD_SERVICE_ACTIVE, &servMathSel::onActive),


on_set_int_void ( MSG_APP_MATH1, &servMathSel::enterMath1),
on_get_string   ( MSG_APP_MATH1, &servMathSel::getMathStr1),

on_set_int_void ( MSG_APP_MATH2, &servMathSel::enterMath2),
on_get_string   ( MSG_APP_MATH2, &servMathSel::getMathStr2),

on_set_int_void ( MSG_APP_MATH3, &servMathSel::enterMath3),
on_get_string   ( MSG_APP_MATH3, &servMathSel::getMathStr3),

on_set_int_void ( MSG_APP_MATH4, &servMathSel::enterMath4),
on_get_string   ( MSG_APP_MATH4, &servMathSel::getMathStr4),

end_of_entry()

servMathSel::servMathSel( QString name) :
                        serviceExecutor(name)
{
    serviceExecutor::baseInit();
}

int servMathSel::onActive(int onoff)
{
    if ( onoff )
    {
        if(this->isActive())
        {
           async(CMD_SERVICE_DEACTIVE,0);
        }
        else
        {
            if( mHorMode == Acquire_XY)
            {
                return ERR_ACTION_DISABLED;
            }
            else
            {
                setActive();
            }
        }
    }
    else
    {
        async(CMD_SERVICE_DEACTIVE,0);
    }
    return ERR_NONE;
}

void servMathSel::registerSpy()
{
}


QString servMathSel::getMathStr1()
{
    return getMathStr(0);
}

QString servMathSel::getMathStr2()
{
    return getMathStr(1);
}

QString servMathSel::getMathStr3()
{
    return getMathStr(2);
}

QString servMathSel::getMathStr4()
{
    return getMathStr(3);
}

QString servMathSel::getMathStr( int i )
{
    if( servMath::m_pWorkThread != NULL )
    {
        servMath *pMath = servMath::m_pWorkThread->getClient(i);
        if( pMath != NULL )
        {
            QString str;

            QString onoff ;
            if( pMath->getMathEn() )
            {
                onoff = "ON";
            }
            else
            {
                onoff = "OFF";
            }

            int nType = pMath->getOperator();
            str = QString("%1$%2").arg(sysGetOptionString(MSG_MATH_S32MATHOPERATOR,nType,"Math"))
                                  .arg(onoff);

//                str = QString("%1(%2)").arg(sysGetString(mBusStrID[bt],"Bus"))
//                                       .arg(onoff);
            return str;
        }
    }

    return "OFF";
}


int servMathSel::enterMath1()
{
    return enterMath(0);
}

int servMathSel::enterMath2()
{
    return enterMath(1);
}

int servMathSel::enterMath3()
{
    return enterMath(2);
}

int servMathSel::enterMath4()
{
    return enterMath(3);
}

int servMathSel::enterMath( int i)
{
    serviceExecutor::post(E_SERVICE_ID_MATH1 + i,
                          CMD_SERVICE_ACTIVE,
                          1);
    return ERR_NONE;
}

///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
IMPLEMENT_CMD( serviceExecutor, servFFTSel  )
start_of_entry()
on_set_int_int( CMD_SERVICE_ACTIVE, &servFFTSel::onActive),

on_set_int_void( MSG_MATHFFTSEL_FFT1, &servFFTSel::setMathFFT1 ),
on_set_int_void( MSG_MATHFFTSEL_FFT2, &servFFTSel::setMathFFT2 ),
on_set_int_void( MSG_MATHFFTSEL_FFT3, &servFFTSel::setMathFFT3 ),
on_set_int_void( MSG_MATHFFTSEL_FFT4, &servFFTSel::setMathFFT4 ),
end_of_entry()

servFFTSel::servFFTSel(QString name  )
    : serviceExecutor( name )
{
    serviceExecutor::baseInit();
}
int servFFTSel::onActive(int onoff)
{
    if ( onoff )
    {
        if(this->isActive())
        {
           async(CMD_SERVICE_DEACTIVE,0);
        }
        else
        {
            if( mHorMode == Acquire_XY)
            {
                return ERR_ACTION_DISABLED;
            }
            else
            {
                setActive();
            }
        }
    }
    else
    {
        async(CMD_SERVICE_DEACTIVE,0);
    }
    return ERR_NONE;
}
void servFFTSel::registerSpy()
{
}

DsoErr servFFTSel::setMathFFT1()
{
    serviceExecutor::post(serv_name_math1,MSG_MATH_S32MATHOPERATOR,operator_fft );
    serviceExecutor::post(serv_name_math1,CMD_SERVICE_ACTIVE,1 );
    return ERR_NONE;
}
DsoErr servFFTSel::setMathFFT2()
{
    serviceExecutor::post(serv_name_math2,MSG_MATH_S32MATHOPERATOR,operator_fft );
    serviceExecutor::post(serv_name_math2,CMD_SERVICE_ACTIVE,1 );
    return ERR_NONE;
}
DsoErr servFFTSel::setMathFFT3()
{
    serviceExecutor::post(serv_name_math3,MSG_MATH_S32MATHOPERATOR,operator_fft );
    serviceExecutor::post(serv_name_math3,CMD_SERVICE_ACTIVE,1 );
    return ERR_NONE;
}
DsoErr servFFTSel::setMathFFT4()
{
    serviceExecutor::post(serv_name_math4,MSG_MATH_S32MATHOPERATOR,operator_fft );
    serviceExecutor::post(serv_name_math4,CMD_SERVICE_ACTIVE,1 );
    return ERR_NONE;
}
