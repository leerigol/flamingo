#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

//#include "../../com/mathlib/mathlib.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

#include "../../meta/crmeta.h"
//#include "../../com/fftlib/rfft.h"
//#include "../../com/mathlib/mathlib.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servch/servch.h"
#include "../../service/servgui/servgui.h"

#include "servmath.h"

#include "../../baseclass/resample/reSample.h"

#define EXPORT_PATH "/home/wzy/trash/"
void servMath::doTest()
{
    //! test
    rst();

    MathOperArith arith( operator_div );

    DsoWfm wfmA, wfmB, wfmC;
    DsoWfm wfmN;

    fillWfmA( wfmA );
    fillWfmB( wfmB );
    fillWfmN( wfmN );

    wfmA.toValue();
    wfmB.toValue();
    wfmN.toValue();

// == arith test add ==
//    while(1)
//    {
//        arith.calc( wfmB, wfmA, &wfmC, NULL );

//        showWfm( wfmA );
//        showWfm( wfmB );

//        showWfm( wfmC);
//    }

// == function
//    MathOperFunc func( operator_exp );
//    while( 1 )
//    {
////        func.calc( wfmB, &wfmC, NULL );
//        func.calc( wfmN, &wfmC, NULL );
//        showWfm( wfmN );

//        showWfm( wfmC);
//    }

// == function
//    opIntgContext intgContext;
//    opDiffContext diffContext;

//    intgContext.bias = 1.0f;
//    intgContext.tInc = 0.1;
//    intgContext.size = 1000;

//    diffContext.tInc = 10;
//    diffContext.window = 5;
//    MathOperFunc func( operator_diff );
//    while( 1 )
//    {
////        func.calc( wfmB, &wfmC, &intgContext );
//        func.calc( wfmB, &wfmC, &diffContext );
//        showWfm( wfmB );

//        showWfm( wfmC);
//    }

//! == resmple
//    struMem mem;
//    struView view;

//    mem.sett( 0, 5 );
//    mem.setLength( wfmB.size() );

//    wfmC.init( 20 );

//    savePoint( wfmB, EXPORT_PATH"b.csv" );
//    int zoom;
//    while ( 1 )
//    {
////        for ( int i = -25; i < 25; i++ )
//        for ( int i = -200; i < 200; i++ )
//        {
////            view.sett( i*1, 1 );zoom=1;
////            view.sett( i*2, 2 );zoom=2;
////            view.sett( i*3, 3 );zoom=3;
////            view.sett( i*4, 4 );zoom=4;

////            view.sett( i*5, 5 );zoom=5;
////            view.sett( i*6, 6 );zoom=6;

////            view.sett( i*10, 10 );zoom=10;
////            view.sett( i*50, 50 );zoom=50;

//            view.sett( i*100, 100 );zoom=100;
////            view.sett( i*200, 200 );zoom=200;
////            view.sett( i*500, 500 );zoom=500;

//            view.setWidth( 24 );

//            view.mapOfMem( &mem );

//            qDebug()<<mem.mPtL<<mem.mPtR;
//            qDebug()<<view.vLeft<<view.vLen;

//            if ( view.vLen > 0 )
//            {
//                reSampleExt( &mem, &view,
//                             wfmB.getPoint(),
//                             wfmC.getPoint() + view.vLeft,
//                             e_sample_peak );

//                showPoint( wfmB );
//                wfmC.setRange( view.vLeft,view.vLen );
//                showPoint( wfmC );
//    //            savePoint( wfmC, EXPORT_PATH"ret.csv");
//            }
//            else
//            {
//                qDebug()<<"!!!no view"<<i*zoom;
//            }

//        }
//    }

//! == resmple by length
    wfmC.init( 20 );
    while ( 1 )
    {
        reSampleExt( 24, 20,
                     wfmB.getPoint(),
                     wfmC.getPoint(),
                     e_sample_peak );

        showPoint( wfmB );
        wfmC.setRange( 0,20 );
        showPoint( wfmC );
    }

//  == range test ==
//    setOperator( operator_mul );
//    while( 1 )
//    {
//        updateVScale();
//    }
}

opRange servMath::getOpRange_test( Chan /*ch*/ )
{
    opRange range;

    range.minV = 0.1;
    range.maxV = 1.0;

    return range;
}

void servMath::fillWfmA( DsoWfm &wfm )
{
    wfm.sett( -10, 1, E_N9 );

    wfm.setyInc( 0.1 );
    wfm.setyGnd( 100 );

    DsoPoint points[]={ 100,110,120,130,140,150,
                        160,170,180,190,200,210,
                        200,190,180,170,160,150,
                        140,130,120,110,100,90,
                       };

    wfm.init( array_count(points) );
    wfm.setPoint( points, array_count(points), array_count(points) );
}
void servMath::fillWfmB( DsoWfm &wfm )
{
    wfm.sett( -10, 1, E_N9 );

    wfm.setyInc( 0.1 );
    wfm.setyGnd( 160 );

    DsoPoint points[]={ 100,110,120,130,140,150,
                        160,170,180,190,200,210,
                        200,190,180,170,160,150,
                        140,130,120,110,100,90,
                       };

    wfm.init( array_count(points) );
    wfm.setPoint( points, array_count(points), array_count(points) );
}

void servMath::fillWfmN( DsoWfm &wfm )
{
    wfm.sett( -10, 1, E_N9 );

    wfm.setyInc( 0.1 );
    wfm.setyGnd( 210 );

    DsoPoint points[]={ 100,110,120,130,140,150,
                        160,170,180,190,200,210,
                        200,190,180,170,160,150,
                        140,130,120,110,100,90,
                       };

    wfm.init( array_count(points) );
    wfm.setPoint( points, array_count(points), array_count(points) );
}

void servMath::saveWfm( DsoWfm &wfm,
              const QString &fileName )
{
    QFile file(fileName);

    bool b = file.open( QIODevice::WriteOnly);
    Q_ASSERT( b );

    QTextStream stream( &file );

    int i;
    stream<<","<<wfm.start()<<","<<wfm.size()<<endl;
    for ( i = wfm.start(); i < wfm.start() + wfm.size(); i++ )
    {
        stream<<wfm.valueAt(i);
    }

    file.close();
}

void servMath::showWfm( DsoWfm &wfm )
{
//    qDebug()<<wfm.start()<<wfm.size();
    QString str;
    int i,j;
    j = 0;
    for ( i = wfm.start(); i < wfm.start() + wfm.size(); i++ )
    {
        str += QString("%1,").arg( wfm.valueAt(i) );

        j++;
        if ( j%24 == 0 )
        {
//            qDebug()<<str;
            str.clear();
        }
    }

    //! end value
    if ( str.length() > 0 )
    {
//        qDebug()<<str;
    }
}

void servMath::savePoint( DsoWfm &wfm,
              const QString &fileName )
{
    QFile file(fileName);

    bool b = file.open( QIODevice::WriteOnly);
    Q_ASSERT( b );

    QTextStream stream( &file );

    int i;
    stream<<","<<wfm.start()<<","<<wfm.size()<<endl;
    for ( i = wfm.start(); i < wfm.start() + wfm.size(); i++ )
    {
        stream<<QString("%1,").arg( wfm.at(i) );
    }

    file.close();
}

void servMath::logPoint( DsoWfm &wfm,
               const QString &fileName )
{
    QFile file(fileName);

    bool b = file.open( QIODevice::WriteOnly|QIODevice::Append);
    Q_ASSERT( b );

    QTextStream stream( &file );

    int i,j;
    QString str;
    stream<<","<<wfm.start()<<","<<wfm.size()<<endl;
    j = 0;
    for ( i = wfm.start(); i < wfm.start() + wfm.size(); i++ )
    {
        j++;

        str += QString("%1,").arg( wfm.at(i) );

        if ( j%24 == 0 )
        {
            stream<<str;
            str.clear();
        }
    }

    if ( str.length() > 0 )
    {
        stream<<str;
    }

    file.close();
}

void servMath::showPoint( DsoWfm &wfm )
{
    qDebug()<<wfm.start()<<wfm.size();
    QString str;
    int i,j;
    j = 0;
    for ( i = wfm.start(); i < wfm.start() + wfm.size(); i++ )
    {
        str += QString("%1,").arg( wfm.at(i) );

        j++;
        if ( j%24 == 0 )
        {
//            qDebug()<<str;
            str.clear();
        }
    }

    //! end value
    if ( str.length() > 0 )
    {
//        qDebug()<<str;
    }
}
