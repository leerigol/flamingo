#include "servch.h"

/*! \var 定义消息映射表
*
* 定义消息映射表
*/
IMPLEMENT_CMD( servVert, servCH )
start_of_entry()

on_set_int_int( MSG_CHAN_COUP, &servCH::setCoupling ),
on_get_int( MSG_CHAN_COUP, &servCH::getCoupling ),

on_set_int_int( MSG_CHAN_BWLIMIT, &servCH::setBandLimit ),
on_get_int( MSG_CHAN_BWLIMIT, &servCH::getBandLimit ),
on_set_void_void( servCH::cmd_bw_apply, &servCH::applyBandwidth ),

on_set_int_bool( MSG_CHAN_IMPEDANCE, &servCH::setImpedance ),
on_get_bool( MSG_CHAN_IMPEDANCE, &servCH::getImpedance ),

on_set_int_int( MSG_CHAN_PROBE, &servCH::setProbeRatioIndex ),
on_get_int( MSG_CHAN_PROBE, &servCH::getProbeRatioIndex ),

on_get_pointer( servCH::cmd_probe_info_ptr,  &servCH::getProbeInfoPtr ),
on_set_int_void( MSG_CHAN_PROBE_CAL,         &servCH::probeCal),

on_set_int_int( MSG_CHAN_PROBE_MODEL, &servCH::setProbeModel ),
on_get_int( MSG_CHAN_PROBE_MODEL, &servCH::getProbeModel ),

//on_set_int_int( MSG_CHAN_PROBE_DELAY, &servCH::setProbeDelay ),
//on_get_int_attr( MSG_CHAN_PROBE_DELAY, &servCH::getProbeDelay, &servCH::getProbeDelayAttr ),

on_set_int_ll( MSG_CHAN_PROBE_DELAY, &servCH::setDelay ),
on_get_ll_attr( MSG_CHAN_PROBE_DELAY, &servCH::getDelay, &servCH::getDelayAttr ),

on_set_int_int( MSG_CHAN_PROBE_BIAS, &servCH::setProbeBias ),
on_get_int_attr( MSG_CHAN_PROBE_BIAS, &servCH::getProbeBias, &servCH::getBiasAttr),

on_set_int_int( MSG_CHAN_UNIT, &servCH::setUnit ),
on_get_int( MSG_CHAN_UNIT, &servCH::getUnit ),

on_set_int_bool( MSG_CHAN_INVERT, &servCH::setInvert ),
on_get_bool( MSG_CHAN_INVERT, &servCH::getInvert ),

on_set_int_bool( MSG_CHAN_FINE, &servCH::setVernier ),
on_get_bool( MSG_CHAN_FINE, &servCH::getVernier ),

on_set_int_ll( MSG_CHAN_DLY_CAL, &servCH::setDelay ),
on_get_ll_attr( MSG_CHAN_DLY_CAL, &servCH::getDelay, &servCH::getDelayAttr ),

on_set_int_int( MSG_CHAN_TUNE_ZERO, &servCH::setNull ),
on_get_int_attr( MSG_CHAN_TUNE_ZERO, &servCH::getNull, &servCH::getNullAttr ),

on_set_int_string( MSG_CHAN_LABEL_EDIT, &servCH::setLabel ),
on_get_string( MSG_CHAN_LABEL_EDIT, &servCH::getLabel ),

on_set_int_bool( MSG_CHAN_LABEL_SHOW, &servCH::setLabelOnOff ),
on_get_bool( MSG_CHAN_LABEL_SHOW, &servCH::getLabelOnOff ),

on_set_int_int( MSG_CHAN_LABEL_NAME, &servCH::setLabelTemplate ),
on_get_int( MSG_CHAN_LABEL_NAME, &servCH::getLabelTemplate ),

on_set_int_bool( MSG_CHAN_ON_OFF, &servCH::onUserOnOff ),
//on_set_int_bool( MSG_CHAN_ON_OFF, &servCH::setOnOff ),
on_get_bool( MSG_CHAN_ON_OFF, &servCH::getOnOff ),

on_set_int_bool( servVert::user_cmd_on_off, &servCH::onUserOnOff ),

on_set_int_int( MSG_CHAN_SCALE_VALUE, &servCH::setScale ),
on_get_int_attr( MSG_CHAN_SCALE_VALUE, &servCH::getScale, &servCH::getScaleAttr ),

on_set_int_int( MSG_CHAN_SCALE, &servCH::setScaleKnob ),
on_get_float_attr( MSG_CHAN_SCALE, &servCH::getScaleReal, &servCH::getScaleRealAttr ),

on_set_int_int( MSG_CHAN_OFFSET, &servCH::setOffset),
on_get_int_attr( MSG_CHAN_OFFSET, &servCH::getOffset, &servCH::getOffsetAttr ),
on_set_int_int( servCH::cmd_cfg_offset, &servCH::cfgOffset),

on_set_int_bool( MSG_CHAN_VER_EXPAND, &servCH::setVerExpand ),
on_get_bool( MSG_CHAN_VER_EXPAND, &servCH::getVertExpand ),

//! system msg
//!
on_set_void_void( CMD_SERVICE_RST, &servCH::rst ),
on_set_int_void( CMD_SERVICE_STARTUP, &servCH::startup ),
on_set_void_void( CMD_SERVICE_INIT, &servCH::init ),

on_set_int_int( CMD_SERVICE_VIEW_CHANGE, &servCH::onViewChange ),

//! spy
on_set_void_void( EVT_TIME_MODE_CHANGED, &servCH::on_time_mode_changed ),
on_set_void_void( servCH::cmd_lic_changed, &servCH::on_lic_changed ),
//on_set_void_void( servCH::cmd_runstop_changed, &servCH::on_runstop_changed),

//! user msg
on_set_int_obj( servCH::cmd_connect_probe, &servCH::connectProbe ),
on_set_int_void( servCH::cmd_disconnect_probe, &servCH::disconnectProbe ),

on_get_float( servCH::cmd_offset_real, &servCH::getOffsetReal ),
on_get_float( servCH::cmd_scale_real, &servCH::getScaleReal ),

on_get_pointer( servCH::cmd_ch_attr, &servCH::getAttr ),
on_get_int( servCH::cmd_ch_id, &servCH::getChId ),

on_get_float_arg( servCH::qcmd_real_volt, &servCH::getRealVolt ),
on_get_int( servCH::qcmd_max_volt, &servCH::getMaxVolt ),
on_get_int( servCH::qcmd_min_volt, &servCH::getMinVolt ),
on_get_float( servCH::qcmd_probe_real, &servCH::getProbeRatioReal ),

on_get_string( servCH::qcmd_string_unit, &servCH::getStringUnit ),

on_set_int_real( servCH::cmd_scale_ui_dsoreal, &servCH::setScaleDsoReal),
on_get_real( servCH::cmd_scale_ui_dsoreal, &servCH::getScaleDsoReal ),

on_set_int_real( servCH::cmd_offset_ui_dsoreal, &servCH::setOffsetDsoReal),
on_get_real( servCH::cmd_offset_ui_dsoreal, &servCH::getOffsetDsoReal ),

on_set_int_real( servCH::cmd_tune_zero_ui_dsoreal, &servCH::setTuneZeroDsoReal),
on_get_real( servCH::cmd_tune_zero_ui_dsoreal, &servCH::getTuneZeroDsoReal ),

on_set_int_obj( servCH::cmd_ch_cfg, &servCH::setCHCfg ),

on_set_int_ll( servCH::cmd_delay_cal, &servCH::setCHDelayCal),
on_get_ll( servCH::cmd_delay_cal, &servCH::getCHDelayCal),

////scpi tcal
//on_set_int_ll( servCH::cmd_delay_cal, &servCH::setDelayCal ),

end_of_entry()
