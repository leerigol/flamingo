#include "servch.h"
#include "../../arith/norm125.h"
#include "../../include/dsocfg.h"

//! scale can not be any value
int servCH::alignScale( int scale )
{
    int roofScale;
    int floorScale;

    //! roof, floor
    roof125( scale, roofScale );
    floor125( scale, floorScale );

    //! align
    int alignScale;

    int step;
    step = roofScale / 100;

    Q_ASSERT( roofScale % 100 == 0 );
    if( step > 0 )
    {}
    else
    {
        LOG_DBG()<<scale; Q_ASSERT(false);
    }

    alignScale = floorScale + ((scale - floorScale)/step)*step;

    return alignScale;
}
int servCH::alignOffset( int offset, int refScale )
{
    if ( refScale % adc_vdiv_dots == 0 )
    {
        int step;
        int alignOffset;

        step = refScale / adc_vdiv_dots;
        Q_ASSERT( step > 0 );
        alignOffset = (offset / step ) * step;

        return alignOffset;
    }
    else
    {
        return offset;
    }
}

