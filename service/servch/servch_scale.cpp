#include "../../include/dsostd.h"
#include "../../include/dsocfg.h"

#include "../../meta/crmeta.h"
#include "../servdso/servdso.h" //! checkRange

#include "../../arith/norm125.h"

#include "servch.h"

#include "stdint.h"

DsoErr servCH::setScale( int scale )
{
    DsoErr err = ERR_NONE;

    //added by hxh. for derek's question
    if( getOnOff() )
    {
        //! limit
        err = servDso::checkRange( ( m_eImpedance == IMP_1M ?
                            "ch/scale/onemegaou" : "ch/scale/fiftyou" ),
                          scale
                        );

        //! expand mode
        int scaleLast, offsetNow;

        scaleLast = m_s32Scale;

        //! align
        scale = servCH::alignScale( scale );

        CCH::setScale( scale );

        //! set offset
        if ( sysGetVertExpand() == vert_expand_center )
        {
            ssync( MSG_CHAN_OFFSET, m_s32Offset );
        }
        //! keep the view offset
        else
        {
            int offsetDots;

            offsetDots = m_s32Offset / (scaleLast/adc_vdiv_dots);
            offsetNow = offsetDots * ( m_s32Scale / adc_vdiv_dots );

            ssync( MSG_CHAN_OFFSET, offsetNow );
        }

        //! lock engine

        //! offset verify

        //! unlock engine

        //! scale changed
        postEngine( ENGINE_CH_SCALE, scale );

        defer( cmd_bw_apply );

        //! reconfig offset
        ssync( MSG_CHAN_OFFSET, m_s32Offset );

        //! dsoVert
        dsoVert::setyRefScale( scale );
        dsoVert::yInc = getScaleReal()/adc_vdiv_dots;


        err = ERR_NONE;
    }

    return err;
}

int servCH::getScale()
{
    return CCH::getScale();
}

void servCH::getScaleAttr( enumAttr /*attr*/ )
{
    setuiBase( realizeVolt(1) );
    setuiUnit( getUnit() );
}

DsoErr servCH::setScaleKnob( int inc )
{
    int scale;

    //! knob z
    if ( inc == 0 )
    {
        async( MSG_CHAN_FINE, !m_vernier );
        return ERR_NONE;
    }

    if ( m_vernier )
    {
        scale = scaleTuneFine( m_s32Scale, inc );
    }
    else
    {
        scale = scaleTuneCoarse( m_s32Scale, inc );
    }

    return setScale( scale );
}

float servCH::getScaleReal()
{
    return realizeVolt( m_s32Scale );
}

void servCH::getScaleRealAttr( enumAttr )
{
    //! z = 0 用于表示vernier设置
    setuiZVal( 0 );
}

float servCH::getRealVolt( CArgument &arg )
{
    Q_ASSERT( arg.size() > 0 );
    Q_ASSERT( arg[0].vType == val_int );

    return realizeVolt( arg[0].iVal );
}

/*!
 * \brief servCH::getMaxVolt
 * \return
 * 当前档位，偏移下的最大值
 */
int servCH::getMaxVolt()
{
    r_meta::CBMeta meta;
    float halfRange;
    meta.getMetaVal( "ch/volt_dot_range", halfRange );

    return halfRange * m_s32Scale - m_s32Offset;
}
/*!
 * \brief servCH::getMinVolt
 * \return
 * 当前档位，偏移下的最小值
 */
int servCH::getMinVolt()
{
    r_meta::CBMeta meta;
    float halfRange;
    meta.getMetaVal( "ch/volt_dot_range", halfRange );

    return -halfRange * m_s32Scale - m_s32Offset;
}

/*!
 * \brief servCH::realizeVolt
 * \param volt
 * \return 真实的物理值
 * 转换位真实值
 * -乘以当前的探头比系数
 * -电路控制中使用的是1:1下的模式
 */
float servCH::realizeVolt( int volt )
{
    return volt * ch_volt_base * getProbeRatioReal();
}

/*!
 * \brief servCH::scaleTuneCoarse
 * \param scale
 * \param keyCnt
 * \return
 * 对当前档位进行粗调
 * -粗调到整数档位
 */
int servCH::scaleTuneCoarse( int scale, int keyCnt )
{
    int scaleIndex;

    int roofIndex, floorIndex;

    roofIndex = roof125Seq( scale );
    floorIndex = floor125Seq( scale );

    if ( keyCnt < 0 )
    {
        scaleIndex = roofIndex + keyCnt;
    }
    else if ( keyCnt > 0 )
    {
        scaleIndex = floorIndex + keyCnt;
    }
    else
    {
        scaleIndex = floorIndex;
    }

    qlonglong normScale;
    normScale = seqToNorm125( scaleIndex );

    if ( normScale >= __INT32_MAX__ )
    {  normScale = int_floor125( __INT32_MAX__); }
    else if ( normScale <= 0 )
    {  normScale = 1; }
    else
    {}

    return (int)normScale;
}
/*!
 * \brief servCH::scaleTuneFine
 * \param scale
 * \param keyCnt
 * \return
 * -档位微调累计次数
 * -对当前档位进行多次的微调操作
 * -微调的步进会根据当前的档位进行调整
 * -微调步进在整数档位时还和调整方向有关
 */
int servCH::scaleTuneFine( int scale, int keyCnt )
{
    int scaleCache;
    int dir;
    int i;

    //! direction
    if ( keyCnt > 0 )
    {
        dir = 1;
    }
    else if ( keyCnt < 0 )
    {
        dir = -1;
    }
    else
    {
        return scale;
    }

    scaleCache = scale;
    //! tune the scale by step
    //! the step is related to the scale
    for ( i = 0; i < keyCnt*dir; i++ )
    {
        scaleCache += dir * scaleToStep( scaleCache, dir );
    }

    return scaleCache;
}

/*!
 * \brief servCH::scaleToStep
 * \param scale
 * \return
 * -加的步进和减的步进不同
 * -两个整数档位间的步进数是100
 */
int servCH::scaleToStep( int scale, int dir )
{
    int scaleRoof, scaleFloor;

    //! roof, floor
    scaleRoof = int_roof125( scale );
    scaleFloor = int_floor125( scale );

    //! 当前是整数档位
    if ( scaleRoof == scaleFloor )
    {
        //! 整数档位在减档位时使用的是小一档步进
        if ( dir < 0 )
        {
            //! roof not change
        }
        else if ( dir > 0 )
        {
            scaleRoof = int_roof125( scale + 1 );
        }
        else
        {
            Q_ASSERT( false );
        }
    }
    else
    {
        //! correct roof
    }

    return scaleRoof / 100;
}

DsoErr servCH::setScaleDsoReal( DsoReal &scale )
{
    if(scale.toFloat() < 0)
    {
        scale = 0;
    }

    DsoReal probeRatio = getProbeRatio();
    DsoReal phyScale;

    //! no ratio
    phyScale = scale / probeRatio;

    //! align to uv
    phyScale.alignBase( E_N6 );

    DsoRealType_A a;
    a = phyScale.getInteger();

    async( MSG_CHAN_SCALE_VALUE, (int)a );

    return ERR_NONE;
}
DsoReal servCH::getScaleDsoReal()
{
    //! probe
    DsoReal pb = getProbeRatio();
    DsoE e;
    DsoRealType_A a;
    pb.getVal( a, e );

    //! scale
    DsoReal scale;
    scale.setVal( getScale(), E_N6 );

    scale.getVal( a, e );

    //! * probe
    scale = scale * getProbeRatio();
    scale.getVal( a, e );

    return scale;
}
