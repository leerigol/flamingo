#include "../../include/dsostd.h"
#include "../../include/dsocfg.h"

#include "../servdso/servdso.h" //! checkRange
#include "servch.h"
#include "../servgui/servgui.h"
#include "../../engine/base/dsoengine.h"
#include "../../service/servcal/servcal.h"
#define BIAS_VAL 12e6
const float servCH::_probeRatio[]=
{
    0.01f, 0.02f, 0.05f,
    0.1f , 0.2f , 0.5f,
    1.0f , 2.0f , 5.0f,
    10.0f, 20.0f, 50.0f,
    100.0f,200.0f,500.0f,
    1000.0f,2000.0f,5000.0f,
    10000.0f,20000.0f,50000.0f
};

static dsoReal _probeRatioReal[]=
{
    {1,E_N2},{2,E_N2},{5,E_N2},
    {1,E_N1},{2,E_N1},{5,E_N1},
    {1,E_0}, {2,E_0}, {5,E_0},

    {1,E_P1},{2,E_P1},{5,E_P1},
    {1,E_P2},{2,E_P2},{5,E_P2},
    {1,E_P3},{2,E_P3},{5,E_P3},
    {1,E_P4},{2,E_P4},{5,E_P4},
};

CCHProbe* servCH::getProbe()
{
   if ( m_ProbeOuter.getOnline() )
   {
       return &m_ProbeOuter;
   }
   else
   {
       return &m_ProbeInner;
   }
}

/*!
 * \brief servCH::getProbeRatioReal
 * \return
 * 真实的物理探头比数值
 */
float servCH::getProbeRatioReal()
{
    int ratio;

    //! ratio index
    ratio = getProbe()->getRatio();

    if ( ratio < 0 || ratio >= array_count(_probeRatio) )
    {
        Q_ASSERT( false );
        ratio = 0;
    }

    return _probeRatio[ ratio ];
}

DsoReal servCH::getProbeRatio()
{
    int ratio;

    //! ratio index
    ratio = getProbe()->getRatio();

    if ( ratio < 0 || ratio >= array_count(_probeRatioReal) )
    {
        Q_ASSERT( false );
        ratio = 0;
    }

    return _probeRatioReal[ ratio ];
}

DsoErr servCH::setProbeRatioIndex( int ratio )
{
    DsoErr err = getProbe()->setRatio( ratio );

    //! y ratio
    dsoVert::setyProbe( getProbeRatio() );

    postEngine( ENGINE_CH_PROBE_REAL, getProbeRatio() );

    return err;
}

int servCH::getProbeRatioIndex()
{
    return getProbe()->getRatio();
}

DsoErr servCH::setProbeDelay( int dly )
{
    DsoErr err;

    err = servDso::checkRange( "ch/ch_delay_range", dly );

    getProbe()->setDelay( dly );

    postEngine( ENGINE_PROBE_DELAY, dly );

    m_ProbeInner.setDelay( dly );

    return err;
}

int servCH::getProbeDelay()
{
    return getProbe()->getDelay();
}

void servCH::getProbeDelayAttr( enumAttr /*attr*/ )
{
    setuiBase(1,E_N12);
    setuiUnit( Unit_s );
    setuiStep( ch_delay_step );
}

DsoErr servCH::setProbeBias( int bias )
{
#if 0
    if(bias>(BIAS_VAL))
    {
        bias = BIAS_VAL;
    }
    else if(bias<-BIAS_VAL)
    {
        bias = -BIAS_VAL;
    }
    DsoErr err;
    err = servDso::checkRange( "ch/ch_bias_range", bias );
    if(err != ERR_NONE) return err;

    m_ProbeInner.setBias(bias);
    int probe_offs_dac = bias/probe_adc_vdiv_dots;
    probe_offs_dac += m_OneWireProbeInfo.zeroVal;
    probe_offs_dac = qBound(0, probe_offs_dac, 65535);


    return  postEngine( ENGINE_1WIRE_PROBE_DAC, getChId(), probe_offs_dac);
#else
    int adcDotsVdiv = (m_OneWireProbeInfo.offGain[0]<<16) + m_OneWireProbeInfo.offGain[1];
    //qDebug()<<__FILE__<<__LINE__<<"bias:"<<bias <<"adcDotsVdiv:"<< adcDotsVdiv;
    if(adcDotsVdiv != 0)
    {
        m_ProbeInner.setBias(bias);

        int probe_offs_dac = bias/adcDotsVdiv;
        //qDebug()<<__FILE__<<__LINE__<<"probe_offs_dac:"<<probe_offs_dac;

        probe_offs_dac += m_OneWireProbeInfo.zeroVal;
        probe_offs_dac = qBound(0, probe_offs_dac, 65535);

        //qDebug()<<__FILE__<<__LINE__<<"probe_offs_dac:"<<probe_offs_dac;
        return  postEngine( ENGINE_1WIRE_PROBE_DAC, getChId(), probe_offs_dac);
    }
    else
    {
        return ERR_NONE;
    }

#endif
}
int servCH::getProbeBias()
{
    //! probe_adc_vdiv_dots
    quint32 adcDotsVdiv = (m_OneWireProbeInfo.offGain[0]<<16) + m_OneWireProbeInfo.offGain[1];

    int gnd = m_OneWireProbeInfo.zeroVal * adcDotsVdiv;
    int max = 65535 * adcDotsVdiv - gnd;
    int min = 0 - gnd;

    /*qDebug()<<__FILE__<<__LINE__
            <<"adcDotsVdiv:"<< adcDotsVdiv
            <<"zeroVal:"<< m_OneWireProbeInfo.zeroVal
            <<"max:"<< max
            <<"min:"<< min
            <<"gnd:"<< gnd;*/

    max = qMin(max, vv(5));
    min = qMax(min, -vv(5));

    setuiRange(min, max, 0);
    return m_ProbeInner.getBias();
}

DsoErr servCH::setProbeModel( ProbeModel model )
{
    return m_ProbeInner.setModel( model );
}
ProbeModel servCH::getProbeModel()
{
    return m_ProbeInner.getModel();
}

DsoErr servCH::connectProbe( CCHProbe *probe )
{
    Q_ASSERT( NULL != probe );

    m_ProbeOuter = *probe;

    //! online
    m_ProbeOuter.setOnline( true );

    //! copy delay
    m_ProbeOuter.setDelay( m_ProbeInner.getDelay() );

    //! save imp
    m_ProbeInner.setImpedance( getImpedance() );

    //! ratio changed
    //setuiChange( MSG_CHAN_PROBE );

    //! impedance changed
    async( MSG_CHAN_IMPEDANCE, m_ProbeOuter.m_Imp );
    async( MSG_CHAN_PROBE,     m_ProbeOuter.getRatio());

    //! ui en
    mUiAttr.setEnable( MSG_CHAN_PROBE,     false );
    mUiAttr.setEnable( MSG_CHAN_IMPEDANCE, false );

    //!1-wire类型的有源探头,获取探头信息
    if(ERR_NONE == queryEngine(qENGINE_1WIRE_PROBE_INFO, &m_OneWireProbeInfo))
    {
        if(m_OneWireProbeInfo.checkInfoValid())
        {
            mUiAttr.setEnable( MSG_CHAN_PROBE_DETAIL, true );
            mUiAttr.setEnable( MSG_CHAN_PROBE_CAL,    true );
            mUiAttr.setEnable( MSG_CHAN_PROBE_BIAS,   true );
            mUiAttr.setEnable( MSG_CHAN_PROBE_DELAY,  true );
            async(MSG_CHAN_PROBE_DETAIL,1);
            post(serv_name_cal, servCal::CMD_APPLY_PROBE_DATA, getChId() );
        }
    }

    return ERR_NONE;
}

DsoErr servCH::disconnectProbe()
{
    //! offline
    m_ProbeOuter.setOnline( false );

    //! return Ratio
    async( MSG_CHAN_PROBE,     m_ProbeInner.getRatio());

    //! return imp
    async( MSG_CHAN_IMPEDANCE, m_ProbeInner.m_Imp );
    setuiChange( MSG_CHAN_PROBE );

    mUiAttr.setEnable( MSG_CHAN_PROBE,     true );
    mUiAttr.setEnable( MSG_CHAN_IMPEDANCE, true );

    mUiAttr.setEnable( MSG_CHAN_PROBE_DETAIL, false );
    mUiAttr.setEnable( MSG_CHAN_PROBE_CAL,    false );

    mUiAttr.setEnable( MSG_CHAN_PROBE_BIAS,   false );
    mUiAttr.setEnable( MSG_CHAN_PROBE_DELAY,  false );

    id_async(getId(), MSG_CHAN_PROBE_BIAS, 0);
    id_async(getId(), MSG_CHAN_PROBE_DELAY, 0);
    return ERR_NONE;
}

OneWireProbeInfo writeInfo;
DsoErr servCH::probeCal()
{
#if 1
    return post(serv_name_cal, servCal::CMD_CAL_PROBE, getChId() );
#else//!校准LSB用
    static bool b = false;
    b = !b;
    return  postEngine( ENGINE_1WIRE_PROBE_DAC,getChId(), (b?50000:10000));
#endif
}

void *servCH::getProbeInfoPtr()
{
#if  0
    qDebug()<<__FILE__<<__LINE__<<"sn:"         <<QByteArray((char*)m_OneWireProbeInfo.sn,        16);
    qDebug()<<__FILE__<<__LINE__<<"model:"      <<QByteArray((char*)m_OneWireProbeInfo.model,     8);
    qDebug()<<__FILE__<<__LINE__<<"date:"       <<QByteArray((char*)m_OneWireProbeInfo.date,      4);
    qDebug()<<__FILE__<<__LINE__<<"delay:"      <<QByteArray((char*)&m_OneWireProbeInfo.delay,    2);
    qDebug()<<__FILE__<<__LINE__<<"zeroOff:"    <<QByteArray((char*)&m_OneWireProbeInfo.zeroOff,  2);
    qDebug()<<__FILE__<<__LINE__<<"offGain:"    <<QByteArray((char*)m_OneWireProbeInfo.offGain,   4);
    qDebug()<<__FILE__<<__LINE__<<"zeroVal:"    <<QByteArray((char*)&m_OneWireProbeInfo.zeroVal,  2);
    qDebug()<<__FILE__<<__LINE__<<"defDelay:"   <<QByteArray((char*)&m_OneWireProbeInfo.defDelay, 2);
    qDebug()<<__FILE__<<__LINE__<<"defZeroOff:" <<QByteArray((char*)&m_OneWireProbeInfo.defZeroOff,2);
    qDebug()<<__FILE__<<__LINE__<<"defOffGain:" <<QByteArray((char*)m_OneWireProbeInfo.defOffGain,4);
#endif
    return  &m_OneWireProbeInfo;
}
