
#include "../../include/dsostd.h"
#include "../../include/dsocfg.h"

#include "../servdso/servdso.h" //! checkRange

#include "../../meta/crmeta.h"

#include "../service_name.h"
#include "../servgui/servgui.h"

#include "../../include/dsoarith.h"

#include "../../service/servcal/servcal.h"
#include "servch.h"

const int servCH::_scale_offset_msg_matrix[] =
{
    MSG_CHAN_SCALE,
    MSG_CHAN_OFFSET,
    MSG_CHAN_UNIT,
    MSG_CHAN_PROBE,
    MSG_CHAN_TUNE_ZERO,
    0
};
QStringList servCH::_labelList;

void servCH::genLabelList()
{
    //! not filled
    if ( servCH::_labelList.size() < 1 )
    { }
    else
    { return; }

    //! fill the label
    servCH::_labelList.append( QStringLiteral("CH1") );
    servCH::_labelList.append( QStringLiteral("CH2") );
    servCH::_labelList.append( QStringLiteral("CH3") );
    servCH::_labelList.append( QStringLiteral("CH4") );

    servCH::_labelList.append( QStringLiteral("ACK") );
    servCH::_labelList.append( QStringLiteral("ADDR") );
    servCH::_labelList.append( QStringLiteral("BIT") );
    servCH::_labelList.append( QStringLiteral("CLK") );

    servCH::_labelList.append( QStringLiteral("CS") );
    servCH::_labelList.append( QStringLiteral("DATA") );
    servCH::_labelList.append( QStringLiteral("IN") );
    servCH::_labelList.append( QStringLiteral("MISO") );

    servCH::_labelList.append( QStringLiteral("MOSI") );
    servCH::_labelList.append( QStringLiteral("OUT") );
    servCH::_labelList.append( QStringLiteral("RX") );
    servCH::_labelList.append( QStringLiteral("TX") );
}

servCH::servCH( QString name,
                Chan chid,
                QColor color,
                ServiceId eId ) :
                servVert( name, eId ), dsoVert( chid )
{
    serviceExecutor::baseInit();

    //! 设置版本
    mVersion = 0x01;

    mDefLabelIndex = 0;

    servCH::genLabelList();

    //! link changes
    linkChange( MSG_CHAN_SCALE, MSG_CHAN_OFFSET, MSG_CHAN_TUNE_ZERO, MSG_CHAN_BWLIMIT );
    linkChange( MSG_CHAN_SCALE_VALUE, MSG_CHAN_BWLIMIT );
    linkChange( MSG_CHAN_OFFSET, MSG_CHAN_SCALE );
    linkChange( MSG_CHAN_PROBE, MSG_CHAN_SCALE, MSG_CHAN_OFFSET, MSG_CHAN_TUNE_ZERO );
    linkChange( MSG_CHAN_UNIT, MSG_CHAN_SCALE, MSG_CHAN_OFFSET, MSG_CHAN_TUNE_ZERO );

    //! attr
    dsoVert::yRefDiv = adc_vdiv_dots;

    dsoVert::setServId( mpItem->servId );
    dsoVert::yColor = color;
    m_OneWireProbeInfo.setId(chid);
}

DsoErr servCH::setOnOff( bool b )
{
    b = b;
//    if ( b )
//    {
//        async( CMD_SERVICE_ACTIVE, (int)1 );
//    }
//    else
//    {   if ( isActive() )
//        { async( CMD_SERVICE_DEACTIVE, (int)0 ); }
//    }

    return ERR_NONE;
}

DsoErr servCH::onUserOnOff( bool b )
{
    CCH::setShowOnOff( b );

    dsoVert::setyOnOff( b );

    if (b)
    {}
    else
    {
       if ( isActive() )
       {
             ssync( CMD_SERVICE_DEACTIVE, (int)0 );
       }
    }

    //! display
    postEngine( ENGINE_DISP_CH_EN, yChId, b );

    if( isActive() && !b)
    {
        bool onoff = false;
        int chOncnt = 0;
        for(int i = 0;i < 4;i++)
        {
            serviceExecutor::query( QString("chan") + QString::number(i+1),
                                    MSG_CHAN_ON_OFF,
                                    onoff);
            if(onoff)
            {
                chOncnt ++;
            }
        }

        if(chOncnt >= 1)
        {
            //! for bug 1687
            postEngine( ENGINE_CH_ONOFF_NOPLAY, b );
        }
        else
        {
            postEngine( ENGINE_CH_ON, b );
        }
    }
    else
    {
        postEngine( ENGINE_CH_ON, b );
    }

    //! led
    syncEngine( b ? ENGINE_LED_ON : ENGINE_LED_OFF,
                DsoEngine::led_ch1 + yChId - chan1 );

    return ERR_NONE;
}

bool servCH::getOnOff()
{
    return CCH::getShowOnOff();
}

DsoErr servCH::setInvert( bool bInv )
{
    CCH::setInvert( bInv );

    return postEngine( ENGINE_CH_INVERT, bInv );
}

bool servCH::getInvert()
{
    return CCH::getInvert();
}

DsoErr servCH::setImpedance( Impedance imp )
{
    CCH::setImpedance( imp );

    postEngine( ENGINE_CH_IMPEDANCE, (int)imp );

    //! 50欧的档位范围和1兆欧的档位范围不同
    ssync( MSG_CHAN_SCALE_VALUE, m_s32Scale );
    //! 50欧和1M欧通道延迟的校准参数不同
    setDelay(CCH::getDelay());

    if ( imp == IMP_50 )
    {
        //! 禁用交流耦合
        if ( getCoupling() == AC )
        {
            async( MSG_CHAN_COUP, DC );
        }
    }

    mUiAttr.setEnable( MSG_CHAN_COUP, imp == IMP_1M );
    dsoVert::setyImpedance( m_eImpedance );
    return ERR_NONE;
}

Impedance servCH::getImpedance()
{
    return CCH::getImpedance();
}

Bandwidth servCH::getBandLimit()
{

    if ( m_eImpedance == IMP_50 )
    { return CCH::getBandwidth(); }
    else
    {
        if ( m_s32Scale < noise_scale )
        { return getNoiseBandwidth(); }
        else
        { return CCH::getBandwidth(); }
    }
}
//! by user
DsoErr servCH::setBandLimit( Bandwidth bw )
{
    if( !mFullBwList.contains(bw) )
    {
        return ERR_INVALID_INPUT;
    }

    Bandwidth real = sysGetBw();
    if( real == BW_200M && bw >= BW_250M )
    {
        return ERR_INVALID_INPUT;
    }

    if( real == BW_100M && bw >= BW_100M )
    {
        return ERR_INVALID_INPUT;
    }

    m_eBandwidth = bw;
    CCH::setBandwidth( bw );

    setNoiseBandwidth( bw );

    applyBandwidth();

    return ERR_NONE;
}

void servCH::updateBandwidthAttr()
{
    if ( m_eImpedance == IMP_50 )
    {
        //! invalid all
        foreach( Bandwidth ebw, mFullBwList )
        {
            mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, false );
            mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, false );
        }

        //! valid
        foreach( Bandwidth ebw, mBwList )
        {
            mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, true);
            mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, true);
        }
    }
    //! 1M
    else
    {
        //! < not <=
        if ( m_s32Scale < noise_scale )
        {
            //! invalid all
            foreach( Bandwidth ebw, mFullBwList )
            {
                mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, false );
                mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, false );
            }

            //! valid
            foreach( Bandwidth ebw, mBwList )
            {
                if ( ebw <= BW_250M )
                {
                    mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, true);
                    mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, true);
                }
            }

            //! can not off
            mUiAttr.setEnable( MSG_CHAN_BWLIMIT, BW_OFF, false );
        }
        else
        {
            //! invalid all
            foreach( Bandwidth ebw, mFullBwList )
            {
                mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, false );
                mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, false );
            }

            //! valid
            foreach( Bandwidth ebw, mBwList )
            {
                mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, true);
                mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, true);
            }
        }
    }
}

void servCH::applyBandwidth()
{
    Bandwidth phyBand;
    phyBand = getPhyBandwidth();

    postEngine( ENGINE_CH_BANDLIMIT, phyBand );    
}

DsoErr servCH::setNoiseBandwidth( Bandwidth band )
{
    //! only a few bws
    QList< Bandwidth > bws;
    bws.append( BW_20M );
    //bws.append( BW_100M );
    bws.append( BW_250M );

    if ( bws.contains( band ) )
    {
        mNoiseBw = band;
        return ERR_NONE;
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
}
Bandwidth servCH::getNoiseBandwidth()
{
    return mNoiseBw;
}

Bandwidth servCH::getPhyBandwidth()
{
    Bandwidth sysBand, chBand;
    Bandwidth phyBand;

    sysBand = sysGetBw();
    chBand = getBandLimit();

    //! by ch band
    if ( chBand == BW_OFF )
    { phyBand = sysBand; }
    else
    { phyBand = chBand; }

    return phyBand;
}

DsoErr servCH::setCoupling( Coupling coup )
{
    CCH::setCoupling( coup );

    dsoVert::yCoupling = coup;

    //bug 2410
    if( coup == AC )
    {
        mUiAttr.setEnable(MSG_CHAN_IMPEDANCE, false);
    }
    else
    {
        if( m_ProbeOuter.getOnline() )
        {
        }
        else
        {
            mUiAttr.setEnable(MSG_CHAN_IMPEDANCE, true);
        }
    }

    return postEngine( ENGINE_CH_COUPLING, (int)coup );
}

Coupling servCH::getCoupling()
{
    return CCH::getCoupling();
}

DsoErr servCH::setUnit(Unit unit)
{
    dsoVert::yUnit = unit;

    return CCH::setUnit(unit);
}

Unit servCH::getUnit()
{
    return CCH::getUnit();
}

DsoErr servCH::setVernier( bool b )
{
    m_vernier = b;

    return ERR_NONE;
}

bool servCH::getVernier()
{
    return m_vernier;
}

DsoErr servCH::setDelay( long long dly )
{
    DsoErr err = ERR_NONE;

    CCH::setDelay( dly );

    void * ptr = NULL;
    serviceExecutor::query(serv_name_cal, servCal::qCMD_CH_DELAY_PTR, ptr);
    calChDelayPayload*  pChDelayData = (calChDelayPayload*) ptr;

    bool imp;
    serviceExecutor::query(serv_name_ch1, MSG_CHAN_IMPEDANCE, imp);
    qlonglong calDelay = pChDelayData->mToCH1Delay[imp]
            .chiToCh1[mpItem->servId - E_SERVICE_ID_CH1][getImpedance()];

    err = postEngine( ENGINE_CH_DELAY, (int)(dly + calDelay) );
    return err;
}

long long servCH::getDelay()
{
    return CCH::getDelay();
}

void servCH::getDelayAttr( enumAttr /*subAttr*/ )
{
#if 0 //by hxh
    setuiBase(1,E_N12);
    setuiMaxVal(time_ns(100));
    setuiMinVal(time_ns(-100));
    setuiZVal(time_ns(0));
    setuiUnit( Unit_s );
    setuiStep( time_ns(10) );
#else
    setuiBase( 1, E_N12 );
    setuiUnit( Unit_s );

    //! -100ns ~ 100ns
    setuiRange( -time_ns(100), time_ns(100), 0ll );

    //! 1/100 ceil(scale)
    horiAttr hAttr = sysGetHoriAttr( chan1, horizontal_view_active );
    int step = hAttr.tInc/time_ps(400) ? (hAttr.tInc/time_ps(400)):1;

    if(step > 50)//bug 2581 qxl 1.5.3
    {
        step = 50;
    }
    setuiStep( step * time_ps(400) );
#endif
}


DsoErr servCH::setCHDelayCal(qlonglong dly)
{
    DsoErr err = ERR_NONE;

    void * ptr = NULL;
    serviceExecutor::query(serv_name_cal, servCal::qCMD_CH_DELAY_PTR, ptr);
    calChDelayPayload*  pChDelayData = (calChDelayPayload*) ptr;

    bool imp;
    serviceExecutor::query(serv_name_ch1, MSG_CHAN_IMPEDANCE, imp);
    pChDelayData->mToCH1Delay[imp]
            .chiToCh1[mpItem->servId - E_SERVICE_ID_CH1][getImpedance()] = dly;

    err = postEngine( ENGINE_CH_DELAY, (int) dly);
    return err;
}

qlonglong servCH::getCHDelayCal()
{
    void* ptr = NULL;
    serviceExecutor::query(serv_name_cal,servCal::qCMD_CH_DELAY_PTR, ptr);
    calChDelayPayload*  pChDelayData = (calChDelayPayload*) ptr;

    bool imp;
    serviceExecutor::query(serv_name_ch1, MSG_CHAN_IMPEDANCE, imp);
    int dly = pChDelayData->mToCH1Delay[imp]
            .chiToCh1[mpItem->servId - E_SERVICE_ID_CH1][getImpedance()];
    return dly;
}

DsoErr servCH::setNull( int null )
{
    CCH::setNull( null );
    //! real offset
    postEngine( ENGINE_CH_NULL, m_s32Null * m_s32Scale/adc_vdiv_dots );

    return ERR_NONE;
}
int servCH::getNull()
{
    return CCH::getNull();
}

void servCH::getNullAttr( enumAttr /*subAttr*/ )
{
    setuiUnit( getUnit() );
    setuiStep( 1 );

    //! real
    DsoReal realBase = getProbeRatio();
    realBase.addE( E_N6 );
    realBase.mA = realBase.mA * m_s32Scale / adc_vdiv_dots;
    realBase.alignBase( E_N6 );
    setuiBase( realBase );

    //! lsb range
    r_meta::CServMeta meta;
    r_meta::CMetaRange zeroRange;
    meta.getMetaRange( "chan/zero_range", &zeroRange );
    setuiRange( zeroRange.mValA.intVal,
                zeroRange.mValB.intVal,
                0 );
}

DsoErr servCH::setVerExpand( VertExpand expand )
{
    sysSetVertExpand( expand );
    return ERR_NONE;
}
VertExpand servCH::getVertExpand()
{
    return sysGetVertExpand();
}

QString servCH::getStringUnit()
{
    return gui_fmt::CGuiFormatter::toString( m_eUnit );
}

QColor servCH::getColor()
{
    return dsoVert::getColor();
}

DsoErr servCH::setCHCfg( CCH *pCH )
{
    Q_ASSERT( NULL != pCH );

    //! recover value
    CCH::setShowOnOff( pCH->getOnOff() );
    CCH::setScale( pCH->getScale() );
    CCH::setOffset( pCH->getOffset() );
    CCH::setImpedance( pCH->getImpedance() );
    CCH::setInvert( pCH->getInvert() );
    CCH::setCoupling( pCH->getCoupling() );

    //! [dba+ bug:2114]
    CCH::setBandwidth(pCH->getBandwidth());

    //! apply
    startup();

    return ERR_NONE;
}

void *servCH::getAttr()
{
    float scaleReal;
    float offsetReal;

    scaleReal = getScaleReal();
    offsetReal = getOffsetReal();

    mAttr.yGnd = offsetReal*adc_vdiv_dots/scaleReal + adc_center;
    mAttr.yInc = scaleReal/adc_vdiv_dots;
    mAttr.yUnit = CCH::getUnit();

    return &mAttr;
}




