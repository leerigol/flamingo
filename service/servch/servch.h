#ifndef SERVCH_H
#define SERVCH_H

#include "../service.h"
#include "../service_msg.h"

#include "../../baseclass/iserial.h"
#include "../../baseclass/dsovert.h"

#include "../../engine/module/cch.h"
#include "../../engine/module/cprobe.h"
#include "../../engine/module/cfilter.h"
#include "../../engine/base/enginecfg.h"
#include "../../engine/base/1wireProbe/1wireprobe.h"

#include "../../service/servvert/servvert.h"

#define noise_scale mv(2)

/*!
 * \brief The servCH class
 * 模拟通道
 */
class servCH : public servVert,
               public ISerial,
               public CCH,
               public dsoVert
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:
    enum enumUserCmd
    {
        cmd_none = 16,
                            //! connect && disconnect
        cmd_connect_probe,  //! CCHProbe*
        cmd_disconnect_probe,

        cmd_ch_id,         //! query

        cmd_offset_real,   /*! float */
        cmd_scale_real,    /*! float */
        cmd_ch_attr,       /*! yGnd, yInc */

        qcmd_real_volt,    /*!< get real volt */
        qcmd_max_volt,
        qcmd_min_volt,
        qcmd_string_unit,

        qcmd_probe_real,   /*!< get real probe */

                           //! probe ratio
        cmd_offset_ui_dsoreal,
        cmd_scale_ui_dsoreal,
        cmd_tune_zero_ui_dsoreal,
        cmd_bias_ui_dsoreal,

                            //! cch
        cmd_ch_cfg,
        cmd_lic_changed,
        cmd_runstop_changed,
        cmd_bw_apply,

        cmd_probe_info_ptr,
        cmd_delay_cal,
        cmd_cfg_offset,
    };

protected:
    static const char* ary_labelTemplate[];
    static const float _probeRatio[];
    static const int  _scale_offset_msg_matrix[];
    static QStringList _labelList;

protected:
    static void genLabelList();

public:
    static int alignScale( int scale );
    static int alignOffset( int offset, int refScale );

public:
    servCH( QString name,
            Chan chId,
            QColor color,
            ServiceId eId = E_SERVICE_ID_NONE );

    virtual void registerSpy();

public:
    virtual DsoErr start();

public:
    virtual int serialOut( CStream &stream, serialVersion &ver );
    virtual int serialIn( CStream &stream, serialVersion ver );
    virtual void rst();
    virtual int startup();
    virtual void init();

public:
    virtual void getPixel( DsoWfm &wfm,
                           Chan subChan = chan_none,
                           HorizontalView hview = horizontal_view_main );
    virtual void getTrace( DsoWfm &wfm,
                           Chan subChan = chan_none,
                           HorizontalView view = horizontal_view_main );
    virtual void getMemory( DsoWfm &wfm, Chan subChan = chan_none );
    virtual void getDigi( DsoWfm &wfm,
                          Chan subChan = chan_none,
                          HorizontalView view = horizontal_view_main );
    virtual void getEye( DsoWfm &wfm, Chan subChan = chan_none );

public:
    virtual int  getyAfeScale();

protected:
    bool m_vernier;         /*!< 微调*/
    bool m_labelOnOff;      /*!< 标签显示*/
    QString m_label;        /*!< 标签*/
    int mDefLabelIndex;

    CCHProbe m_ProbeOuter;  /*!< 外部探头*/
    CCHProbe m_ProbeInner;  /*!< 内部探头*/

    vertAttr mAttr;

    Bandwidth mNoiseBw;     //! noise scale
    QList< Bandwidth > mBwList;

    QList< Bandwidth > mFullBwList;

    /*!< 1-wire 探头 信息>*/
    OneWireProbeInfo  m_OneWireProbeInfo;

protected:
    CCHProbe *getProbe();
    float getProbeRatioReal();
    DsoReal getProbeRatio();

public:
    //! ch
    DsoErr setOnOff( bool b );
    bool getOnOff( );

    DsoErr onUserOnOff( bool b );

    DsoErr setScale( int scale );
    int getScale();
    void getScaleAttr( enumAttr attr );

    DsoErr setScaleKnob( int inc );
    float getScaleReal();
    void getScaleRealAttr( enumAttr );

    DsoErr cfgOffset( int offset );
    DsoErr setOffset( int offset );
    int    getOffset();
    void getOffsetAttr( enumAttr attr );
    void getBiasAttr( enumAttr attr );

    DsoErr setOffsetKnob( int inc );
    float getOffsetReal();

    DsoErr setCHCfg( CCH *pCH );

    void* getAttr();

    DsoErr setInvert( bool bInv );
    bool getInvert();

    DsoErr setImpedance( Impedance imp );
    Impedance getImpedance();

    DsoErr    setBandLimit( Bandwidth band );
    Bandwidth getBandLimit();
    Bandwidth getPhyBandwidth();


    void updateBandwidthAttr();
    void applyBandwidth();

    DsoErr setNoiseBandwidth( Bandwidth band );
    Bandwidth getNoiseBandwidth();

    DsoErr setCoupling( Coupling coup );
    Coupling getCoupling();

    DsoErr setUnit(Unit unit);
    Unit getUnit();

    DsoErr setVernier( bool b );
    bool getVernier();

    DsoErr setDelay(long long dly );
    long long getDelay();
    void getDelayAttr( enumAttr subAttr );

    DsoErr setDelayKnob( int inc );
    float getDelayReal();

    DsoErr setNull( int null );
    int getNull();
    void getNullAttr( enumAttr subAttr );

    //! label
    DsoErr setLabel( QString label );
    QString getLabel();

    DsoErr setLabelOnOff( bool b );
    bool getLabelOnOff();

    DsoErr setLabelTemplate( int temp );
    int getLabelTemplate();

    //! probe
    DsoErr setProbeRatioIndex( int ratio );
    int getProbeRatioIndex();

    DsoErr setProbeDelay( int dly );
    int getProbeDelay();
    void getProbeDelayAttr( enumAttr attr );

    DsoErr setProbeBias( int bias );
    int getProbeBias();

    void getProbeHeadAttr( enumAttr attr );

    DsoErr setProbeModel( ProbeModel model );
    ProbeModel getProbeModel();

    Impedance getProbeImpedance();

    DsoErr connectProbe( CCHProbe *probe );
    DsoErr disconnectProbe();
    DsoErr probeCal();
    void*  getProbeInfoPtr();

    //! expand
    DsoErr setVerExpand( VertExpand expand );
    VertExpand getVertExpand();

    QString getStringUnit();

    //! ui
    DsoErr setScaleDsoReal( DsoReal &scale );
    DsoReal getScaleDsoReal();

    DsoErr setOffsetDsoReal( DsoReal &offset );
    DsoReal getOffsetDsoReal();

    DsoErr setTuneZeroDsoReal( DsoReal &scale );
    DsoReal getTuneZeroDsoReal();

    void getOffsetRange( int scale,
                         Impedance imp,
                         int *pOffMax );

    //! by scale, imp
    void getOffsetRange( DsoRealGp *gp );
    void getScaleRange( DsoRealGp *gp );

    //! spy
    void on_time_mode_changed();
//    void on_runstop_changed();
    void on_lic_changed();

    DsoErr setCHDelayCal(qlonglong dly);
    qlonglong getCHDelayCal( );

public:
    float getRealVolt( CArgument &arg );
    int getMaxVolt();
    int getMinVolt();
    QColor getColor();

protected:
    float realizeVolt( int volt );

    int scaleTuneCoarse( int scale, int keyCnt );
    int scaleTuneFine( int scale, int keyCnt );

//    int scaleToIndex( int scale );
//    int indexToScale( int index );
    int scaleToStep( int scale, int dir );

    Chan getChId();

};
#endif // SERVCH_H
