#include "../../include/dsostd.h"
#include "../../include/dsocfg.h"

#include "../../meta/crmeta.h"
#include "../servdso/servdso.h" //! checkRange

#include "servch.h"
#define BIAS_VAL 12e6

DsoErr servCH::cfgOffset( int /*offset*/ )
{
    postEngine( ENGINE_CH_OFFSET, getOffset()  );
    return ERR_NONE;
}

DsoErr servCH::setOffset( int offset )
{
    DsoErr err = ERR_NONE;

    //added by hxh. for derek's question
    if( getOnOff() )
    {
        int offMax;
        getOffsetRange( m_s32Scale,
                        m_eImpedance,
                        &offMax );

        err = checkRange( offset, -offMax, offMax );

        //! align
    //    offset = servCH::alignOffset( offset, m_s32Scale );

        CCH::setOffset( offset );

        //! real offset
        postEngine( ENGINE_CH_OFFSET, offset  );

        //! gnd
        dsoVert::setyRefOffset( offset );
    }

    return err;
}

int servCH::getOffset()
{
    return CCH::getOffset();
}

void servCH::getOffsetAttr( enumAttr /*attr*/ )
{
    //! update attr
    float a = realizeVolt(1) ;

    setuiBase( a );

    int s = getScale() / adc_vdiv_dots ;

    setuiStep( s );
    setuiZVal( 0 );
    setuiUnit( getUnit() );
    setuiAcc( key_acc::e_key_square );

    setuiFmt( dso_fmt_trim( fmt_float, fmt_width_3,fmt_no_trim_0 ) );

    int offMax;
    getOffsetRange( m_s32Scale,
                    m_eImpedance,
                    &offMax );

    setuiRange( -offMax, offMax, 0 );
}

void servCH::getBiasAttr( enumAttr /*attr*/ )
{
    //! update attr
    setuiBase( ch_volt_base );
    int s = getScale() / adc_vdiv_dots ;
    setuiStep( s );
    setuiZVal( 0 );
    setuiUnit( getUnit() );
    setuiAcc( key_acc::e_key_square );
    setuiFmt( dso_fmt_trim( fmt_float, fmt_width_3,fmt_no_trim_0 ) );

    setuiRange( -BIAS_VAL, BIAS_VAL, 0 );
}

float servCH::getOffsetReal()
{
    return realizeVolt( m_s32Offset );
}

DsoErr servCH::setOffsetDsoReal( DsoReal &offset )
{
    if(offset.toFloat()<-1000000)
    {
        offset = -1000000;
    }

    DsoReal phyVal;

    //! no ratio
    phyVal = offset / getProbeRatio();

    //! align to uv
    phyVal.alignBase( E_N6 );

    DsoRealType_A a;
    a = phyVal.getInteger();

    int intA = 0;
    if( a >= INT_MAX )
    {
        intA = INT_MAX;
    }
    else if( a <= INT_MIN )
    {
        intA = INT_MIN;
    }
    else
    {
        intA = a;
    }

    async( MSG_CHAN_OFFSET, intA );

    return ERR_NONE;
}
DsoReal servCH::getOffsetDsoReal()
{
    DsoReal realVal;

    realVal.setVal( getOffset(), E_N6 );

    realVal = realVal * getProbeRatio();

    return realVal;
}

DsoErr servCH::setTuneZeroDsoReal( DsoReal &/*zero*/ )
{
    return ERR_NONE;
}
DsoReal servCH::getTuneZeroDsoReal()
{
    return DsoReal( 0, E_0 );
}

void servCH::getOffsetRange( int scale,
                             Impedance imp,
                             int *pOffMax )
{
    //! 1M ohm
    //! ~50mv]: 1
    //! ~200mv]: 30
    //! ~10V]: 100
    if ( imp == IMP_1M )
    {
        if ( scale <= 50000 )
        { *pOffMax = 1000000; }
        else if ( scale <= 260000 )
        { *pOffMax = 30000000; }
        else
        { *pOffMax = 100000000; }
    }
    //! 50 ohm
    //! ~100mv]: 1
    //! ~1v]: 4
    else
    {
        if ( scale <= 100000 )
        { *pOffMax = 1000000; }
        else
        { *pOffMax = 4000000; }
    }
}

void servCH::getOffsetRange( DsoRealGp *gp )
{
    Q_ASSERT( NULL != gp );

    int offMax;

    getOffsetRange( m_s32Scale,
                    m_eImpedance,
                    &offMax );

    //! fill group
    DsoReal realNow, realMax, realMin, realDef;

    realNow.setVal( m_s32Offset, E_N6 );
    realDef.setVal( 0, E_N6 );
    realMax.setVal( offMax, E_N6 );
    realMin.setVal( -offMax, E_N6 );

    //! probe
    DsoReal realPb;
    realPb = getProbeRatio();

    //! gp set
    gp->setValue( realNow,
                  realMax,
                  realMin,
                  realDef );

    gp->mul( realPb );

}
void servCH::getScaleRange( DsoRealGp *gp )
{
    Q_ASSERT( NULL != gp );

    int maxScale, minScale;
    minScale = 1000;

    //! 10V
    if ( m_eImpedance == IMP_1M )
    { maxScale = 10000000; }
    else
    { maxScale = 1000000; }

    //! fill group
    DsoReal realNow, realMax, realMin, realDef;

    realNow.setVal( m_s32Scale, E_N6 );
    realDef.setVal( 100000, E_N6 );
    realMax.setVal( maxScale, E_N6 );
    realMin.setVal( minScale, E_N6 );

    //! probe
    DsoReal realPb;
    realPb = getProbeRatio();

    //! gp set
    gp->setValue( realNow,
                  realMax,
                  realMin,
                  realDef );

    gp->mul( realPb );
}
