#include "servch.h"
#include "../service_name.h"
#include "../servhori/servhori.h"
#include "../servdso/sysdso.h"


void servCH::on_time_mode_changed()
{
    enter_service_context();

    //! time mode
    query_enum(  serv_name_hori,
                 MSG_HOR_TIME_MODE,
                 AcquireTimemode,
                 timeMode
              );
    if ( !_bOK ) return;


    if ( timeMode == Acquire_XY )
    {
        mUiAttr.setEnable( MSG_CHAN_ON_OFF, false );
    }
    else
    {
        mUiAttr.setEnable( MSG_CHAN_ON_OFF, true );
    }

    exit_service_context();
}

//void servCH::on_runstop_changed()
//{
//    int conAction = -1;
//    serviceExecutor::query(serv_name_hori, MSG_HORIZONTAL_RUN, conAction);
//    if(conAction == Control_Stop)
//    {
//        mUiAttr.setEnable( MSG_CHAN_DLY_CAL, false );
//    }
//    else
//    {
//        mUiAttr.setEnable( MSG_CHAN_DLY_CAL, true );
//    }
//}

void servCH::on_lic_changed()
{
    Bandwidth bw = sysGetBw();

    //! deduce the bw options
    mBwList.clear();
    int i;
    for( i = 0; i < mFullBwList.size(); i++ )
    {
        if ( mFullBwList[i] < bw )
        {
            mBwList.append( mFullBwList[i]);
        }
    }

    //! invalid all
    foreach( Bandwidth ebw, mFullBwList )
    {
        mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, false );
        mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, false );
    }

    //! valid
    foreach( Bandwidth ebw, mBwList )
    {
        mUiAttr.setEnable( MSG_CHAN_BWLIMIT, ebw, true);
        mUiAttr.setVisible( MSG_CHAN_BWLIMIT, ebw, true);
    }
}

////scpi
//DsoErr servCH::setDelayCal(qlonglong dly)
//{
//    DsoErr err = ERR_NONE;

//    //! 1/100 ceil(scale)
//    qlonglong tScale;
//    horiAttr hAttr = sysGetHoriAttr( chan1, horizontal_view_active );
//    tScale = hAttr.tInc * adc_hdiv_dots;

//    if(tScale)
//    {
//        tScale =  tScale/adc_hdiv_dots ;
//        dly = (dly/tScale)*tScale;
//        async( MSG_CHAN_DLY_CAL, dly);
//    }

//    return err;
//}
