#include "../../include/dsostd.h"
#include "../../include/dsocfg.h"

#include "../service_name.h"
#include "../servtrace/servtrace.h"

#include "../../baseclass/resample/reSample.h"

#include "servch.h"

/*!
 * \brief servCH::getPixel
 * \param wfm
 * 读取通道的像素数据
 * -像素数据对应屏幕宽度
 * \todo
 */
void servCH::getPixel(DsoWfm &wfm, Chan /*subChan*/, HorizontalView hview )
{
    //! -- trace
    DsoWfm wfmTrace;
    getTrace( wfmTrace,chan_none,hview);
    /*! [dba, 2018-04-16], bug: 没有trace时，m_pPoint == NULL，UltraScope读数据时断言*/
    wfm.init(1000);

    if ( wfmTrace.size() > 0 )
    {}
    else
    { return; }

    //! -- to pixel
    struMem mem;
    struView view;

    //! ---- trace to screen
    //! mem
    mem.sett( wfmTrace.getllt0() + wfmTrace.start() * wfmTrace.getlltInc(), wfmTrace.getlltInc() );
    mem.setLength( wfmTrace.size() );

    //! view
    horiAttr hAttr;
    hAttr = sysGetHoriAttr(chan1, hview);

    view.sett( - hAttr.gnd * hAttr.tInc, hAttr.tInc );
    view.setWidth( wave_width );
    //! resample
    int ret = reSampleExt( &mem,
                           &view,
                           wfmTrace.getPoint() + wfmTrace.start(),
                           wfm.getPoint(),
                           e_sample_norm|e_sample_head|e_sample_tail );
    if ( ret != 0 )
    {
        wfm.setRange( 0, 0 );
        return;
    }
    else
    {
        wfm.setvAttr( &wfmTrace );
        wfm.settAttr( view.mvt0,
                      view.mvtInc,
                      view.mWidth );

        wfm.setRange( view.vLeft, view.vLeft + view.vLen );
    }

    wfm.toValue(wfm.start(),wfm.size());
#ifdef _DEBUG
    wfm.dbgShow();
    wfm.save( "/rigol/log/chx_pixel.dat" );
#endif

}
/*!
 * \brief servCH::getTrace
 * \param wfm
 * max: 1M points
 */
void servCH::getTrace( DsoWfm &wfm ,
                       Chan /*subChan*/,
                       HorizontalView view )
{
    //! -- query
    CArgument argIn,argOut;

    argIn.setVal( (int)dsoVert::yChId );
    argIn.setVal( &wfm, 1 );
    argIn.setVal( view, 2 );

    DsoErr err;
    err = serviceExecutor::query( serv_name_trace,
                                  servTrace::cmd_query_trace_xxx,
                                  argIn,
                                  argOut);
    if ( err != ERR_NONE )
    { return; }
}
/*!
 * \brief servCH::getMemory
 * \param wfm
 * \todo not implemented
 */
void servCH::getMemory( DsoWfm &/*wfm*/, Chan /*subChan*/ )
{
    Q_ASSERT( false );
}

//! digital data for decode
void servCH::getDigi( DsoWfm &wfm, Chan subChan, HorizontalView /*view*/ )
{
    //! -- query
    CArgument argIn,argOut;

    if( !getOnOff() )
    {
        return;
    }
    if( subChan == chan_none)
    {
        //! digi ch
        argIn.setVal( (int)( ( dsoVert::yChId - chan1) + digi_ch1 )  );
    }
    else
    {
        argIn.setVal( (int)( ( dsoVert::yChId - chan1) + digi_ch1_l )  );
    }
    argIn.setVal( (pointer)&wfm, 1 );
    //! only main
    argIn.setVal( horizontal_view_main, 2 );

    DsoErr err;
    err = serviceExecutor::query( serv_name_trace,
                                  servTrace::cmd_query_trace_xxx,
                                  argIn,
                                  argOut);
    if ( err != ERR_NONE )
    { return; }
}

void servCH::getEye( DsoWfm &wfm, Chan /*subChan*/ )
{
    //! -- query
    CArgument argIn,argOut;

    //! eye ch
    argIn.setVal( (int)( ( dsoVert::yChId - chan1) + eye_ch1 )  );
    argIn.setVal( (pointer)&wfm, 1 );
    argIn.setVal( horizontal_view_main, 2 );

    DsoErr err;
    err = serviceExecutor::query( serv_name_trace,
                                  servTrace::cmd_query_trace_xxx,
                                  argIn,
                                  argOut);
    if ( err != ERR_NONE )
    { return; }
}

int servCH::getyAfeScale()
{
    int afeScale;

    if ( ERR_NONE != queryEngine( qENGINE_CH_AFE_SCALE, getId(), afeScale ) )
    {
        return 100000;
    }

    LOG_DBG()<<afeScale;

    return afeScale;
}

Chan servCH::getChId()
{
    return yChId;
}

