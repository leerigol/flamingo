#include "../servgui/servgui.h"
#include "servch.h"

#include "../servvertmgr/servvertmgr.h"
#include "../service_name.h"

void servCH::registerSpy()
{
    spyOn( serv_name_hori,
           EVT_TIME_MODE_CHANGED );

    //! license
    spyOn( serv_name_license,
           servLicense::cmd_Opt_Active,
           servCH::cmd_lic_changed );

    spyOn( serv_name_license,
           servLicense::cmd_Opt_Exp,
           servCH::cmd_lic_changed );

    spyOn( serv_name_license,
           servLicense::cmd_Opt_Invalid,
           servCH::cmd_lic_changed );

    for( int i = 1; i < 5; i++ )
    {
        if((i - 1) != (getId() - E_SERVICE_ID_CH1) )
        {
            spyOn( QString("chan")+QString::number(i),
                   MSG_CHAN_IMPEDANCE,
                   MSG_CHAN_DLY_CAL );
        }
    }
}

DsoErr servCH::start()
{
    mUiAttr.setVisible(MSG_CHAN_PROBE_MODEL,false);
    return ERR_NONE;
}

/*!
 * \brief servCH::serialOutf
 * \param [out] stream
 * \param [out] ver
 * \return
 * \note 需要调用基类处理函数，输出版本号
 * 将输出的数据流和版本号分开，是为了更加明确数据是有版本控制的
 *
 * 输出数据时，需要以“名称-值”的格式
 */
//#define ar  ar_pack_out
//#define ar_pack_e ar_pack_e_out
#define ar ar_out
#define ar_pack_e ar_out
int servCH::serialOut( CStream &stream, unsigned char &ver )
{
    ISerial::serialOut(stream, ver );

    ar( "on", m_bShowOnOff );

    ar( "scale", m_s32Scale );
    ar( "offset", m_s32Offset );
    ar( "invert", m_bInvert );
    ar_pack_e( "imp", m_eImpedance );

    ar_pack_e( "band", m_eBandwidth );
    ar_pack_e( "coup", m_eCoupling );
    ar( "delay", m_s32Delay );
    ar( "null", m_s32Null );

    ar( "vernier", m_vernier );
    ar_pack_e( "unit", m_eUnit );

    //! label
    ar_out( "label", m_label );
    ar( "l_on", m_labelOnOff );

    //! probe
    ar( "probe", m_ProbeInner.m_Ratio );
    ar( "p_bias", m_ProbeInner.m_Bias );
    ar( "p_delay", m_ProbeInner.m_Delay );

    return 0;
}
/*!
 * \brief servCH::serialIn
 * \param stream
 * \param ver
 * \return
 * \note 需要调用基类处理函数
 * 需要进行版本检测，如果数据版本不兼容，则需要兼容处理。
 * - 对于BIN文件格式尤其如此，因为BIN格式严格按照顺序关系
 *   恢复数据。
 * - BIN格式中并没有存储数据项的名称
 *
 */
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servCH::serialIn( CStream &stream, unsigned char ver )
{
   ISerial::serialIn(stream, ver );

   ar( "on", m_bShowOnOff );

   ar( "scale", m_s32Scale );
   ar( "offset", m_s32Offset );
   ar( "invert", m_bInvert );
   ar_pack_e( "imp", m_eImpedance );

   ar_pack_e( "band", m_eBandwidth );
   ar_pack_e( "coup", m_eCoupling );
   ar( "delay", m_s32Delay );
   ar( "null", m_s32Null );

   ar( "vernier", m_vernier );
   ar_pack_e( "unit", m_eUnit );

   //! label
   ar_in( "label", m_label );
   ar( "l_on", m_labelOnOff );

   if( m_ProbeOuter.getOnline() == false )
   {
       //! probe
       ar( "probe", m_ProbeInner.m_Ratio );
       ar( "p_bias", m_ProbeInner.m_Bias );
       ar( "p_delay", m_ProbeInner.m_Delay );
   }

   return 0;
}
/*!
 * \brief servCH::rst
 * \todo reset
 * 将数值恢复成默认值
 * \note 只恢复数值，并没有进行配置
 */
void servCH::rst()
{
    int id;
    id = getId();

    //! CVertical
    if ( sysHasArg( "-ds8000") )
    {
        //! only one channnel
        if ( id == E_SERVICE_ID_CH1 )
        {
            m_bShowOnOff = true;
        }
        else
        {
            m_bShowOnOff = false;
        }
    }
    else
    {
        m_bShowOnOff = true;
    }

    //! CCH
    m_s32Scale = 100000;
    m_s32Offset = 0;
    m_bInvert = false;



    m_s32Null = 0;

    m_eBandwidth = BW_OFF;
    m_eCoupling = DC;
    m_eUnit = Unit_V;

    mNoiseBw = BW_20M;

    //! servCH
    m_vernier = false;
    m_labelOnOff = false;
    m_label = QString("CH%1").arg( mpItem->servId - E_SERVICE_ID_CH1 + 1 );

    /* 外接探头时复位，不对阻抗，探头比等进行复位. by hxh */
    if( m_ProbeOuter.getOnline() == false )
    {
        //! probe
        m_ProbeInner.m_Bias = 0;
        m_ProbeInner.m_Delay = 0;
        m_ProbeInner.m_Ratio = Probe_X1;
        m_ProbeInner.setModel( PROBE_BNC );
        m_eImpedance = IMP_1M;                
    }

    //! special setting
    switch( id )
    {
        case E_SERVICE_ID_CH1:
            m_s32Offset = 0;
            break;

        case E_SERVICE_ID_CH2:
            m_s32Offset = 0*m_s32Scale;
            break;

        case E_SERVICE_ID_CH3:
            m_s32Offset = -0*m_s32Scale;
            break;

        case E_SERVICE_ID_CH4:
            m_s32Offset = 0*m_s32Scale;
            break;

        default:
            break;
    }

    //! zone
    dsoVert::yZone = time_zone;
}
/*!
 * \brief servCH::startup
 * \return
 * \todo config all
 * 配置更底层的实现，应用设置
 */
int servCH::startup()
{
    //! apply setting
    ssync( MSG_CHAN_ON_OFF, m_bShowOnOff );

    setScale( m_s32Scale );
    setOffset( m_s32Offset );


    setInvert( m_bInvert );

    setImpedance( m_eImpedance );
    setBandLimit( m_eBandwidth );
    setCoupling( m_eCoupling );
    setDelay( m_s32Delay );

    setUnit(m_eUnit);

    setNull( m_s32Null );

    setProbeRatioIndex( getProbe()->m_Ratio );

    defer( servCH::cmd_lic_changed );

    if( !m_ProbeOuter.getOnline() && (AC != m_eCoupling) )
    {
        //bug2629
        mUiAttr.setEnable(MSG_CHAN_IMPEDANCE, true );
    }

    //! update menu ui
    updateAllUi();

    dsoVert::setyZone( time_zone );
    return ERR_NONE;
}

void servCH::init()
{
    if ( mpItem->servId == E_SERVICE_ID_CH1 )
    {
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 1, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 2, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 3, false );
        mDefLabelIndex = 0;
    }
    else if ( mpItem->servId == E_SERVICE_ID_CH2 )
    {
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 0, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 2, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 3, false );
        mDefLabelIndex = 1;
    }
    else if ( mpItem->servId == E_SERVICE_ID_CH3 )
    {
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 0, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 1, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 3, false );
        mDefLabelIndex = 2;
    }
    else if ( mpItem->servId == E_SERVICE_ID_CH4 )
    {
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 0, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 1, false );
        mUiAttr.setVisible( MSG_CHAN_LABEL_NAME, 2, false );
        mDefLabelIndex = 3;
    }
    else
    { Q_ASSERT(false); }

    //! full bw list
    mFullBwList.append( BW_OFF );
    mFullBwList.append( BW_20M );
    mFullBwList.append( BW_250M );

    //! bw limit
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_100M, false, false );
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_200M, false, false );
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_300M, false, false );
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_350M, false, false );
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_500M, false, false );
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_750M, false, false );
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_1G, false, false );
    mUiAttr.setEnableVisible( MSG_CHAN_BWLIMIT, BW_2G, false, false );

    //mUiAttr.setVisible( MSG_CHAN_BWLIMIT, BW_350M, true );
    on_lic_changed();
    updateBandwidthAttr();

    //add offset stick
    {
        QList<qint64> stick;
//        stick.append(-3*scale );
//        stick.append(-2*scale );
//        stick.append(-scale );
        stick.append( 0 );
//        stick.append( scale );
//        stick.append( 2*scale );
//        stick.append( 3*scale );
        mUiAttr.setStick(MSG_CHAN_OFFSET, stick);
    }
}

