#include "servch.h"
#include "../../include/dsocfg.h"
DsoErr servCH::setLabel( QString label )
{
    m_label = label;
    return ERR_NONE;
}

QString servCH::getLabel()
{
    mUiAttr.setMaxLength( MSG_CHAN_LABEL_EDIT, max_label_length );

    return m_label;
}

DsoErr servCH::setLabelOnOff( bool b )
{
    m_labelOnOff = b;

    return ERR_NONE;
}

bool servCH::getLabelOnOff()
{
    return m_labelOnOff;
}

DsoErr servCH::setLabelTemplate( int temp )
{
    if ( temp < 0 ||
         temp >= servCH::_labelList.size() )
    {
        return ERR_INVALID_INPUT;
    }

    //! set label str
    async( MSG_CHAN_LABEL_EDIT, servCH::_labelList[ temp ] );

    return ERR_NONE;
}
int servCH::getLabelTemplate()
{
    int index;

    //! default to
    index = servCH::_labelList.indexOf( m_label );
    if ( index < 0 )
    {
        return mDefLabelIndex;
    }
    else
    { return index; }
}
