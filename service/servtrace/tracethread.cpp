
#include "servtrace.h"

#include "dataprovider.h"

#include "../servplot/servplot.h"
#include "../../engine/base/enginecfg.h"

traceThread::traceThread( QObject *parent ): QThread( )
{
    m_pTrace = dynamic_cast<servTrace*>( parent );
}

void traceThread::run()
{
    DsoErr err = ERR_NONE;

    dataProvider providerA;     //! double buffer
//    dataProvider providerB;

    //qDebug()<<__FUNCTION__<<QThread::currentThread();
    for(int i = 1; i < 14;i++)
    {
        signal(i, handler);
    };


    if(sysHasArg("-no_trace"))
    {
        qDebug()<<"no trace";
        return;
    }

    //! set provider
    providerA.setData( chan1, m_pTrace->mWfmCH + 0 );
    providerA.setData( chan2, m_pTrace->mWfmCH + 1 );
    providerA.setData( chan3, m_pTrace->mWfmCH + 2 );
    providerA.setData( chan4, m_pTrace->mWfmCH + 3 );

//    providerB.setData( chan1, m_pTrace->mWfmCH + 0 );
//    providerB.setData( chan2, m_pTrace->mWfmCH + 1 );
//    providerB.setData( chan3, m_pTrace->mWfmCH + 2 );
//    providerB.setData( chan4, m_pTrace->mWfmCH + 3 );

    //! 16 resource
    providerA.release( 16 );
//    providerB.release( 16 );

#ifdef _DEBUG
    //! request
    qint64 cnt = 0;
    qint64 dateTimeFrom = QDateTime::currentMSecsSinceEpoch();
    qint64 dateTimeNow;
    qint64 dateTimePre;
#endif
    while( true )
    {
        if( !sysGetStarted() )
        {
            //! wait app completed
            QThread::sleep(2);
            continue;
        }
        m_pTrace->m_pActProvider = &providerA;

        //! zx add for bug 2997
        bool mbNeedClear = m_pTrace->mbNeedClear;
        m_pTrace->mbNeedClear = false;

        if( mbNeedClear && (!dsoEngine::getTraceOnceOnOff()))
        {
//            m_pTrace->clearTrace();
//            err = ERR_NONE;
        }
        else if ( !m_pTrace->mbRun )
        {
            //! trace not run
            QThread::usleep( m_pTrace->mTmous );
            continue;
        }
        else
        {
            servTrace::lock();
            err = m_pTrace->pullData();
            servTrace::unlock();
            if ( err == ERR_INVALID_CONTEXT || err == ERR_NO_TRACE)
            {
                QThread::msleep( 10 );
                QThread::yieldCurrentThread();
                continue;
            }
        }

        //! fetch fail
        if ( err != ERR_NONE )
        {
            QThread::msleep( 10 );
            QThread::yieldCurrentThread();
#ifdef _DEBUG
//            QThread::sleep( 10 );
            LOG_DBG()<<"!!!Trace fetch fail!";
            LOG_DBG()<<"err"<<err;
#endif
            continue;
        }
        else
        {
            for(int i = 0;i < 8;i++)
            {
                m_pTrace->mWfmCH[i].setFloatValid(false);
            }
//            //! mimic screen range
//            m_pTrace->mWfmCH[0].toValue();
//            m_pTrace->mWfmCH[1].toValue();
//            m_pTrace->mWfmCH[2].toValue();
//            m_pTrace->mWfmCH[3].toValue();

//            m_pTrace->mWfmCH[4].toValue();
//            m_pTrace->mWfmCH[5].toValue();
//            m_pTrace->mWfmCH[6].toValue();
//            m_pTrace->mWfmCH[7].toValue();
            m_pTrace->m_pActProvider->acquire(16);
            m_pTrace->copyToBuffer();
        }

        providerA.release(16);
        serviceExecutor::post( E_SERVICE_ID_TRACE,
                               servTrace::cmd_set_provider,
                               (pointer) NULL );

        //! wait for proc
        QThread::msleep( m_pTrace->mSleepms );

#ifdef _DEBUG
        //! meas the trace interval
        {
            cnt++;
            dateTimePre = dateTimeNow;
            dateTimeNow = QDateTime::currentMSecsSinceEpoch();

            if ( ( cnt % 1000 ) == 0 && dateTimeNow != dateTimeFrom)
            {
                LOG_DBG()<< (float)cnt/(dateTimeNow - dateTimeFrom)<<(dateTimeNow-dateTimePre);
            }
        }
#endif
    }
}
