#include "../../include/dsocfg.h"
#include "../servmath/servmath.h"

#include "../../com/fftlib/rfftc.h"
#include "../../arith/norm125.h"
#include "servtrace.h"
#include "dataprovider.h"


IMPLEMENT_CMD( serviceExecutor, servTrace  )
start_of_entry()

//! sys
on_set_void_void( CMD_SERVICE_INIT, &servTrace::on_init ),


on_set_void_void( MSG_CHAN_PROBE, &servTrace::on_probe_update ),
//! user
on_set_int_pointer( servTrace::cmd_set_provider, &servTrace::setDataProvider ),

on_get_void_argi_argo( servTrace::cmd_query_trace_xxx, &servTrace::getTraceXxx ),
on_get_void_argi_argo( servTrace::cmd_query_trace_Sa, &servTrace::getTraceSa ),

on_set_int_bool( servTrace::cmd_trace_run, &servTrace::on_trace_run ),
on_get_bool( servTrace::cmd_trace_run, &servTrace::get_trace_run),

on_set_int_void( servTrace::cmd_clear_trace, &servTrace::on_trace_clear),

on_set_int_pointer( servTrace::cmd_request_phase, &servTrace::on_phase_request ),

on_set_int_int( servTrace::cmd_open_data, &servTrace::onOpenData ),
on_set_int_int( servTrace::cmd_close_data, &servTrace::onCloseData ),

on_set_int_int( servTrace::cmd_open_digi,  &servTrace::onOpenDigi ),
on_set_int_int( servTrace::cmd_close_digi, &servTrace::onCloseDigi ),

end_of_entry()

QMutex servTrace::_traceMutex;
void servTrace::lock()
{
    //servTrace::_traceMutex.lock();
}
void servTrace::unlock()
{
    //servTrace::_traceMutex.unlock();
}


servTrace::servTrace( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    m_pPlatform = NULL;
    mbRun = true;

    mTmous = 2000000;       //! 2s
    mSleepms = 10;          //! 10ms
    m_pActProvider = NULL;
                            //! data en map
    //! ana
    for( int ch = chan1; ch <= chan4; ch++ )
    { mDataEnMap.insert( (Chan)(ch), true ); }

    //! dx
    for( int ch = d0; ch <= d15; ch++ )
    { mDataEnMap.insert( (Chan)(ch), true ); }

    //! la
    for( int ch = la; ch <= la; ch++ )
//    { mDataEnMap.insert( (Chan)(ch), false ); }
    { mDataEnMap.insert( (Chan)(ch), true ); }

    //! digH
    for( int ch = digi_ch1; ch <= digi_ch4; ch++ )
    { mDataEnMap.insert( (Chan)(ch), false ); }

    //! digL
    for( int ch = digi_ch1_l; ch <= digi_ch4_l; ch++ )
    { mDataEnMap.insert( (Chan)(ch), false ); }

    //! eye
    for( int ch = eye_ch1; ch <= eye_ch4; ch++ )
    { mDataEnMap.insert( (Chan)(ch), false ); }
}

DsoErr servTrace::start()
{
    qRegisterMetaType<DsoWfm>( "DsoWfm" );
    qRegisterMetaType<DsoWfm>( "DsoWfm&" );

    QThread *pThread = new traceThread( this );
    Q_ASSERT(pThread != NULL);

    if ( pThread != NULL )
    {
        if ( sysHasArg("-notrace") )
        {
            qWarning()<<"!!!trace not running";
        }
        else
        {
            pThread->start( QThread::LowPriority );
        }
    }
    else
    {
        qWarning()<<"fail to create thread";
    }

    //! do not change setting
    setMsgAttr( (int)cmd_set_provider );

    return ERR_NONE;
}


void servTrace::registerSpy()
{
    spyOn( E_SERVICE_ID_DISPLAY, MSG_DISPLAY_CLEAR, cmd_clear_trace );
    for(int i = E_SERVICE_ID_CH1; i < E_SERVICE_ID_CH4; i++)
    {
        spyOn( (ServiceId) i, MSG_CHAN_PROBE );
    }
}

void servTrace::on_init()
{
    m_pPlatform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != m_pPlatform );

    r_meta::CServMeta meta;
    meta.getMetaVal( "trace/tmous", mTmous );
    meta.getMetaVal( "trace/sleepms", mSleepms );

    meta.getMetaVal( "trace/peak", mPeakSpec );
    meta.getMetaVal( "trace/window", mWindow );
}

DsoErr servTrace::on_trace_run( bool b )
{
    mbRun = b;

    return ERR_NONE;
}

bool servTrace::get_trace_run()
{
    return dsoEngine::getTraceOnceOnOff() || dsoEngine::getTraceRequestOnOff();
}

DsoErr servTrace::on_trace_clear()
{
    mbNeedClear = true;  
    clearTrace();
    return ERR_NONE;
}

DsoErr servTrace::on_probe_update()
{
    if( m_pActProvider == NULL )
    {
        return ERR_NONE;
    }

    m_pActProvider->acquire(16);
    //! 手动按下了屏幕上的clear键
    for(int i = 0; i < 4;i++)
    {
        dsoEngine::setTrace_probe( (Chan) (i + chan_none), &mWfmCH[i] );
        dsoEngine::setTrace_probe( (Chan) (i + chan_none), &mWfmCHBuffer[i] );
        dsoEngine::setTrace_probe( (Chan) (i + chan_none), &mWfmCH[i+4] );
        dsoEngine::setTrace_probe( (Chan) (i + chan_none), &mWfmCHBuffer[i+4] );
    }
    m_pActProvider->release(16);
    serviceExecutor::post( E_SERVICE_ID_TRACE,
                           servTrace::cmd_set_provider,
                           (pointer) NULL );

    return ERR_NONE;
}

DsoErr servTrace::clearTrace( )
{
    m_pActProvider->acquire(16);
    //! 手动按下了屏幕上的clear键
    for(int i = 0; i < 4;i++)
    {
        mWfmCH[i].setRange(0,0);
        mWfmCH[i+4].setRange(0,0);
        mDigiCH[i].setRange(0,0);
        mDigiCHL[i].setRange(0,0);
        mEye[i].setRange(0,0);
    }

    for(int i = 0;i < 16;i++)
    {
        mDx[i].setRange(0,0);
        mDx[i+16].setRange(0,0);
    }
    mLa[0].setRange(0,0);
    mLa[1].setRange(0,0);
    mbNeedClear = false;
    m_pActProvider->release(16);
    return ERR_NONE;
}

DsoErr servTrace::setDataProvider( void* /*ptr*/ )
{
    return ERR_NONE;
}

//! argI: ch, ptr
//! argO: ptr
void servTrace::getTraceXxx( CArgument &argI, CArgument &/*argO*/ )
{
    //! deload
    Q_ASSERT( argI.size() == 3 );
    Chan ch = (Chan)argI[0].iVal;
    DsoWfm *pDst = (DsoWfm*)argI[1].pVal;
    Q_ASSERT( pDst != NULL );
    HorizontalView view = (HorizontalView)argI[2].iVal;

    if(m_pActProvider == NULL)
    {
        pDst->setRange(0,0);
        return;
    }

    DsoWfm *pWfm = getWfmXxx( ch, view );
    Q_ASSERT( pWfm );

    m_pActProvider->acquire();
    if(!pWfm->getFloatValid())
    {
        pWfm->setFloatValid(true);
        pWfm->toValue();
    }
    //! copy
    *pDst = *pWfm;
    m_pActProvider->release();
}

//! 直接获取到trace的采样率
void servTrace::getTraceSa(CArgument &argI, CArgument & argOut )
{
    Chan ch = (Chan)argI[0].iVal;
    HorizontalView view = (HorizontalView)argI[1].iVal;

    dsoVert *pVert;
    pVert = dsoVert::getCH( ch );
    Q_ASSERT( NULL != pVert );

    VertZone zone;
    zone = pVert->getVertZone();

    dso_phy::CFlowView *pView;
    CDsoSession *pSes;
    pSes = dsoEngine::getViewSesion();
    Q_ASSERT( NULL != pSes );

    //! get view sa
    if ( view == horizontal_view_main )
    {
        if ( zone == vert_analog )
        { pView = &pSes->m_acquire.mMainCHView; }
        else if ( zone == vert_la )
        { pView = &pSes->m_acquire.mMainLAView; }
        else
        { pView = &pSes->m_acquire.mMainCHView; }
    }
    //! view zoom
    else
    {
        if ( zone == vert_analog )
        { pView = &pSes->m_acquire.mZoomCHView; }
        else if ( zone == vert_la )
        { pView = &pSes->m_acquire.mZoomLAView; }
        else
        { pView = &pSes->m_acquire.mZoomCHView; }
    }

    Q_ASSERT( NULL != pView );
    argOut.setVal( pView->getFlow().getSa(), 0 );
    return;
}

DsoErr servTrace::on_phase_request( phaseRequest *pRequest )
{
    Q_ASSERT( NULL != pRequest );

    addRequest( servTrace::cmd_request_phase, pRequest);

    return ERR_NONE;
}

DsoErr servTrace::onOpenData( Chan ch )
{
    if ( mDataEnMap.contains( ch) )
    {}
    else
    {
        return ERR_INVALID_INPUT;
    }

    mDataEnMap[ ch ] = true;

    return ERR_NONE;
}

DsoErr servTrace::onCloseData( Chan ch )
{
    if ( mDataEnMap.contains( ch) )
    {}
    else
    { return ERR_INVALID_INPUT; }

    mDataEnMap[ ch ] = false;

    return ERR_NONE;
}


DsoErr servTrace::onOpenDigi( Chan  )
{
    onOpenData(digi_ch1);
    onOpenData(digi_ch2);
    onOpenData(digi_ch3);
    onOpenData(digi_ch4);
    return ERR_NONE;
}

DsoErr servTrace::onCloseDigi( Chan  )
{
    onCloseData(digi_ch1);
    onCloseData(digi_ch2);
    onCloseData(digi_ch3);
    onCloseData(digi_ch4);
    return ERR_NONE;
}

DsoWfm *servTrace::getWfmXxx( Chan ch, HorizontalView view )
{
    int viewShift;

    //! check ch
    if ( ch >= chan1 && ch <= chan4 )
    {
        viewShift = ( view == horizontal_view_main ? 0 : 4 );

        return mWfmCHBuffer + viewShift + ch - chan1;
    }
    else if ( ch >= digi_ch1 && ch <= digi_ch4 )
    {
        return mDigiCHBuffer + ch - digi_ch1;
    }
    else if ( ch >= digi_ch1_l && ch <= digi_ch4_l )
    {
        return mDigiCHLBuffer + ch - digi_ch1_l;
    }
    else if ( ch >= eye_ch1 && ch <= eye_ch4 )
    {
        return mEyeBuffer + ch - eye_ch1;
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        viewShift = ( view == horizontal_view_main ? 0 : 16 );

        return mDxBuffer + viewShift + ch - d0;
    }
    else if ( ch == la )
    {
        viewShift = ( view == horizontal_view_main ? 0 : 1 );

        return mLaBuffer + viewShift;
    }
    else
    {
        Q_ASSERT(false);
        return NULL;
    }
}

void servTrace::addRequest( int msg, serviceRequest* ptr )
{
    servTrace::lock();

    if ( !mRequests.contains(msg) )
    {
        mRequests.insert(msg,ptr);
    }
    else
    {
        mRequests[msg] = ptr;
    }

    servTrace::unlock();
}
void servTrace::removeRequest( int msg )
{
    servTrace::lock();

    if ( mRequests.contains(msg) )
    {
        mRequests.remove(msg);
    }

    servTrace::unlock();
}

DsoErr servTrace::pullData()
{
    Q_ASSERT(m_pPlatform);

    DsoErr err;

    //! request
    traceRequest req;

    //! -- anallog
    {
        if ( !sysHasArg("-notrace_ch") )
        {
            //! attach ch
            for ( int ch = chan1;  ch <= chan4; ch++ )
            {
                if ( mDataEnMap[(Chan)(ch)] )
                {
                    //! main
                    req.attachChan( (Chan)(ch), mWfmCH + ch - chan1, 0 );
                    //! zoom
                    req.attachChan( (Chan)(ch), mWfmCH + 4 + ch - chan1, 1 );
//                    req.attachChan( (Chan)(ch), mWfmCH + ch - chan1, 1 );
                }
            }
        }

        //! attach digi
        if ( !sysHasArg("-notrace_digi") )
        {
            for ( int ch = digi_ch1; ch <= digi_ch4; ch++ )
            {
                if ( mDataEnMap[(Chan)(ch)] )
                {
                    req.attachChan( (Chan)(ch), mDigiCH + ch - digi_ch1);
                }
            }
        }

        //! attach eye
        if ( !sysHasArg("-notrace_eye") )
        {
            for( int ch = eye_ch1; ch <= eye_ch4; ch++ )
            {
                if ( mDataEnMap[(Chan)(ch)] )
                { req.attachChan( (Chan)(ch), mEye + ch - eye_ch1 ); }
            }
        }
    }

    //! -- la main
    if ( dsoVert::getCH(la)->getOnOff() )
    {
        //! attach la
        if ( !sysHasArg("-notrace_dx") )
        {
            for ( int ch = d0; ch <= d15; ch++ )
            {
                if ( mDataEnMap[(Chan)(ch)] )
                {
                    req.attachChan( (Chan)(ch), mDx + ch - d0, 0 );
                    req.attachChan( (Chan)(ch), mDx + 16 + ch - d0, 1 );
                }
            }
        }
        if ( !sysHasArg("-notrace_la") )
        {
            if ( mDataEnMap[la] )
            {
                req.attachChan( la, mLa + 0, 0 );
                req.attachChan( la, mLa + 1, 1 );
            }
        }
    }

    //! request
    Q_ASSERT( NULL != m_pPlatform );
    Q_ASSERT( NULL != m_pPlatform->getActiveEngine() );
    err = m_pPlatform->getActiveEngine()->requestTrace( req, mTmous );

    if ( err != ERR_NONE )
    {
        return err;
    }

    //! ack service request
    err = ackServiceRequest();
    if ( err != ERR_NONE )
    {
        return err;
    }

    if ( !sysHasArg("-log_trace") )
    {
        return ERR_NONE;
    }

#if 1
    QString str;
    if ( sysHasArg("-log_ch") )
    {
        LOG_DBG()<<"log ch";
        if ( mWfmCH[0].getLength() > 0)
        {
            LOG_DBG()<<"pull ch1";
            static int _ch1_trace_id = 0;
//            str = QString("/rigol/trace/mch1_%1_%2.dat").arg( mWfmCH[0].getLength() ).arg(_ch1_trace_id);
            str = QString("/rigol/trace/mch1_%1.dat").arg( mWfmCH[0].getLength() );
            mWfmCH[0].save( str );
            mWfmCH[0].dbgShow();
            _ch1_trace_id++;
        }

        //! zoom
        if ( mWfmCH[4].getLength() > 0)
        {
            LOG_DBG()<<"pull zch1";
//            str = QString("/rigol/trace/zch1_%1_%2.dat").arg( mWfmCH[5].getLength() ).arg(_ch1_trace_id);
            str = QString("/rigol/trace/zch1_%1.dat").arg( mWfmCH[4].getLength() );
            mWfmCH[4].save( str );
            mWfmCH[4].dbgShow();
        }

//        if ( mWfmCH[1].getLength() > 0)
//        {
//            str = QString("/rigol/trace/ch2_%1.dat").arg( mWfmCH[1].getLength() );
//            mWfmCH[1].save( str );
//        }
//        if ( mWfmCH[2].getLength() > 0)
//        {
//            str = QString("/rigol/trace/ch3_%1.dat").arg( mWfmCH[2].getLength() );
//            mWfmCH[2].save( str );
//        }
//        if ( mWfmCH[3].getLength() > 0)
//        {
//            LOG_DBG()<<"pull ch4";
//            str = QString("/rigol/trace/ch4_%1.dat").arg( mWfmCH[3].getLength() );
//            mWfmCH[3].save( str );
//        }
    }

//    ackPhaseRequest( NULL );

//    if ( mWfmCH[1].getLength() > 0 )
//    {
//        str = QString("/rigol/trace/ch2_%1.dat").arg( mWfmCH[1].getLength() );
//        mWfmCH[1].save( str );
//    }

//    if ( mWfmCH[2].getLength() > 0 )
//    {
//        str = QString("/rigol/trace/ch3_%1.dat").arg( mWfmCH[2].getLength() );
//        mWfmCH[2].save( str );
//    }

//    if ( mDigiCH[0].getLength() > 0 )
//    {
//        str = QString("/rigol/trace/digi0_%1.dat").arg( mDigiCH[0].getLength() );
//        mDigiCH[0].save(str);
//        mDigiCH[0].dbgShow();
//        LOG_DBG()<<str;
//    }
    if ( mDigiCH[0].getLength() > 0 )
    {
        str = QString("/rigol/trace/digi1_%1.dat").arg( mDigiCH[1].getLength() );
        mDigiCH[0].save(str);
        mDigiCH[0].dbgShow();
    }

    //! main
    if ( mDx[0].getLength() > 0 )
    {
        str = QString("/rigol/trace/mDx0_%1.dat").arg( mDx[0].getLength() );
        mDx[0].save(str);
        mDx[0].dbgShow();
    }

    //! zoom
    if ( mDx[16].getLength() > 0 )
    {
        str = QString("/rigol/trace/zla0_%1.dat").arg( mDx[16].getLength() );
        mDx[16].save(str);
        mDx[16].dbgShow();
        LOG_DBG()<<str;
    }

    if( sysHasArg("-log_la") )
    {
        if ( mLa[0].getLength() > 0 )
        {
            str = QString("/mnt/rigol/trace/la_%1.dat").arg( mLa[0].getLength() );
            mLa[0].save(str);
            mLa[0].dbgShow();
        }
    }

    if ( sysHasArg("-log_eye") )
    {
        if ( mEye[0].getLength() > 0 )
        {
            str = QString("/mnt/rigol/trace/eye1_%1.dat").arg( mEye[0].getLength() );
            mEye[0].save(str);
            mEye[0].dbgShow();
            LOG_DBG()<<str;
        }
    }
#endif

    return ERR_NONE;
}

DsoErr servTrace::copyToBuffer()
{
    for(int i = 0;i < 8;i++)
    {
        mWfmCHBuffer[i] = mWfmCH[i];
    }
    for(int i = 0;i < 4;i++)
    {
        mDigiCHBuffer[i] = mDigiCH[i];
        mDigiCHLBuffer[i] = mDigiCHL[i];
        mEyeBuffer[i] = mEye[i];
    }
    for(int i = 0;i < 32;i++)
    {
        mDxBuffer[i] = mDx[i];
    }
    for(int i = 0;i < 2;i++)
    {
        mLaBuffer[i] = mLa[i];
    }

    return ERR_NONE;
}

DsoErr servTrace::ackServiceRequest()
{
    //! for each request
    QMapIterator<int, serviceRequest*> iter(mRequests);
    while (iter.hasNext())
    {
        iter.next();

        //! phase
        if ( iter.key() == servTrace::cmd_request_phase )
        {
            ackPhaseRequest( iter.value() );
        }
    }

    //! acked
    iter.toFront();
    while (iter.hasNext())
    {
        iter.next();

        if ( iter.value() != NULL )
        {
            if ( iter.value()->mAcked )
            {
                //! \notice in mutext
                mRequests.remove( iter.key() );
            }
        }
    }

    return ERR_NONE;
}

#define delta( a, b)    ( (a-b) < -180 ? (a-b+360) : (a-b) )
#define phase_point     (7209)     //! 1.1G/2.5G ~~~ 16k
//#define phase_point     (1311)     //! 100M/2.5G ~~~ 32k

static float _max[3];
static float _min[3];
static int _cnt = 0;
DsoErr servTrace::ackPhaseRequest( serviceRequest* ptr )
{
    //! calc the phase
    float phase[4];
    for ( int i = 0; i < array_count(phase) ; i++ )
    {
        fft_c_point(
               mWfmCH[i].getPoint(),
//               mWfmCH[0].getLength(),
               int_floor2N( mWfmCH[i].getLength() ),
               int_floor2N( mWfmCH[i].getLength() ),
//               fft_rectangle,
               (fftWindow)mWindow,
               fft_spec_phase,
               phase+i,
               mPeakSpec,
               1 );

#if 0
        static int cali = 0;
        if(cali >= 100 && cali < 150)
        {
            QString str = QString("/rigol/trace/cal%1.dat").arg(cali);
            mWfmCH[i].save(str);
            qDebug()<<cali;
        }
        cali++;
#endif

        //! to degree
        phase[i] = phase[i]*180/3.14;
    }

    //! align phase
    alignPhase( phase );

    if ( ptr == NULL )
    {
        LOG_DBG()<<delta(phase[1],phase[0])<<delta(phase[2],phase[0])<<delta(phase[3],phase[0]);
        LOG_DBG()<<phase[0]<<phase[1]<<phase[2]<<phase[3];
        _cnt++;

        //! init
        if ( _cnt == 5 )
        {
            _max[0] = _min[0] = delta(phase[1],phase[0]);
            _max[1] = _min[1] = delta(phase[2],phase[0]);
            _max[2] = _min[2] = delta(phase[3],phase[0]);
            return ERR_NONE;
        }

        //! max-min0
        if ( delta(phase[1],phase[0]) > _max[0] )
        { _max[0] = delta(phase[1],phase[0]); }
        if ( delta(phase[1],phase[0]) < _min[0] )
        { _min[0] = delta(phase[1],phase[0]); }

        //! max-min1
        if ( delta(phase[2],phase[0]) > _max[1] )
        { _max[1] = delta(phase[2],phase[0]); }
        if ( delta(phase[2],phase[0]) < _min[1] )
        { _min[1] = delta(phase[2],phase[0]); }

        //! max-min2
        if ( delta(phase[3],phase[0]) > _max[2] )
        { _max[2] = delta(phase[3],phase[0]); }
        if ( delta(phase[3],phase[0]) < _min[2] )
        { _min[2] = delta(phase[3],phase[0]); }

//        LOG_DBG()<<_max[0]-_min[0]<<_max[1]-_min[1]<<_max[2]-_min[2];

        return ERR_NONE;
    }

    //! set the request
    phaseRequest *pReq;
    pReq = (phaseRequest*)( ptr );
    Q_ASSERT( NULL != pReq );

//    LOG_DBG()<<delta(phase[1],phase[0])<<delta(phase[2],phase[0])<<delta(phase[3],phase[0]);

    //! deltas
    pReq->setDeltas( delta(phase[1],phase[0]),
                     delta(phase[2],phase[0]),
                     delta(phase[3],phase[0]) );

    //! ack the request
    pReq->ackRequest();

    return ERR_NONE;
}

//! in degree
void servTrace::alignPhase( float phases[4] )
{
    //! 1. find the min & max
    float minP, maxP;

    minP = phases[0];
    maxP = phases[0];
    for ( int i = 1; i < 4; i++ )
    {
        minP = qMin( minP, phases[i] );
        maxP = qMax( maxP, phases[i] );
    }

    //! 2. find the dist
    float dist = maxP - minP;
    if ( dist > 180 )   //! roll back
    {
        for ( int i = 0; i < 4; i++ )
        {
            if ( phases[i] < 0 )
            { phases[i] += 360; }
        }
    }
    else
    {}
}

