#ifndef SERVTRACE_H
#define SERVTRACE_H

#include "../service.h"
#include "../../baseclass/dsowfm.h"

#include "../../engine/cplatform.h" //! platform

#include "../servicerequest.h"
#include "dataprovider.h"

using namespace DsoType;
class traceThread;
class servTrace : public serviceExecutor
{
    Q_OBJECT
    DECLARE_CMD()

public:
    enum enumCmd
    {
        cmd_none = 0,
        cmd_set_provider,   /*!< data provider */

        cmd_query_trace_xxx,
        cmd_query_trace_Sa, /*! get trace Sa */

        cmd_request_phase,

        cmd_clear_trace,

        cmd_trace_run,      //! bool

        cmd_open_data,      //! chan
        cmd_close_data,


        cmd_open_digi,
        cmd_close_digi
    };

public:
    static void lock();
    static void unlock();
    virtual void registerSpy();
    virtual DsoErr start();
    void on_init();

public:
    servTrace( QString name, ServiceId eId = E_SERVICE_ID_TRACE );

    DsoErr on_trace_run( bool b );
    bool   get_trace_run();

    DsoErr on_trace_clear();
    DsoErr on_probe_update();
    DsoErr clearTrace();

    DsoErr setDataProvider( void* ptr );

    void getTraceXxx( CArgument &argIn, CArgument &argOut );
    void getTraceSa( CArgument &argIn, CArgument &argOut );

    DsoErr on_phase_request( phaseRequest *pRequest );

    DsoErr onOpenData( Chan ch );
    DsoErr onCloseData( Chan ch );

    DsoErr onOpenDigi( Chan ch );
    DsoErr onCloseDigi( Chan ch );

    DsoWfm *getWfmXxx( Chan ch, HorizontalView view );

    void addRequest( int msg, serviceRequest * ptr=NULL);
    void removeRequest( int msg );

    DsoErr pullData();
    DsoErr copyToBuffer();

    DsoErr ackServiceRequest();

    DsoErr ackPhaseRequest( serviceRequest * ptr );
    void   alignPhase( float phaases[4] );

public:
    dataProvider* m_pActProvider;

protected:
    //! channel
    DsoWfm mWfmCH[4+4];     //! chan1/2/3/4 main + ch1/2/3/4 zoom

    //! digi
    DsoWfm mDigiCH[4];      //! digH
    DsoWfm mDigiCHL[4];     //! digL

    //! la
    DsoWfm mDx[16+16];      //! main dx + zoom dx
    DsoWfm mLa[1+1];        //! main la + zoom la
    //! eye
    DsoWfm mEye[4];         //! eyech1/2/3/4

    DsoWfm mWfmCHBuffer[4+4];
    DsoWfm mDigiCHBuffer[4];
    DsoWfm mDigiCHLBuffer[4];
    DsoWfm mDxBuffer[16+16];
    DsoWfm mLaBuffer[1+1];
    DsoWfm mEyeBuffer[4];

protected:
    cplatform *m_pPlatform;

    int mTmous;
    int mSleepms;
    int mPeakSpec, mWindow;

    QMap<int,serviceRequest*> mRequests;
    QMap<Chan, bool > mDataEnMap;
    bool mbRun;
    bool mbNeedClear;
Q_SIGNALS:
    void sigWfm( Chan id, DsoWfm &wfm );

private:
    static QMutex _traceMutex;

friend class traceThread;
};

class traceThread : public QThread
{
    Q_OBJECT
public:
    traceThread( QObject *parent );

protected:
    servTrace *m_pTrace;

protected:
    virtual void run();
};

#endif // SERVTRACE_H
