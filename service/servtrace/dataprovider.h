#ifndef DATAPROVIDER
#define DATAPROVIDER

#include <QtCore>
#include "../../include/dsotype.h"
#include "../../baseclass/dsowfm.h"

using namespace DsoType;

#define DATA_CAP    1000

class dataProvider : public QSemaphore
{
public:
    dataProvider() : QSemaphore() { }

private:
    DsoWfm * mListWfm[chan_all];

    QSemaphore mSema;

public:
    int getCap()
    {
        return DATA_CAP;
    }

public:
    DsoErr getData( Chan id, DsoWfm &wfm )
    {
        if ( mListWfm[(int)id] == NULL )
        {
            Q_ASSERT( false );
            return ERR_NULL_IN_PROVIDER;
        }
        //! assign
        wfm = mListWfm[(int)id];

        return ERR_NONE;
    }

    DsoErr setData( Chan id, DsoWfm *pWfm )
    {
        mListWfm[ (int)id ] = pWfm;
        return ERR_NONE;
    }

    DsoWfm* getData(Chan id)
    {
        if ( mListWfm[(int)id] == NULL )
        {
            return NULL;
        }

        //! assign
        return mListWfm[(int)id];
    }

};

#endif // DATAPROVIDER
