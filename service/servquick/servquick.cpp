#include "../servstorage/storage_api.h"
#include "../servstorage/servstorage.h"
#include "servquick.h"

IMPLEMENT_CMD( serviceExecutor, servQuick  )

start_of_entry()

on_set_int_int   (        MSG_QUICK_OPERATION,          &servQuick::setOperation ),
on_get_int       (        MSG_QUICK_OPERATION,          &servQuick::getOperation ),

on_set_int_int   (        MSG_APP_QUICK,                &servQuick::quickAction ),


//on_set_int_string(        MSG_QUICK_PREFIX,             &servQuick::setPrefixName),
//on_get_string    (        MSG_QUICK_PREFIX,             &servQuick::getPrefixName),
///////////////////////////////////ABOUT IMAGE//////////////////////////////////////////
on_set_int_int   (        MSG_QUICK_IMAGE_FORMAT,       &servQuick::setImageFormat ),
on_get_int       (        MSG_QUICK_IMAGE_FORMAT,       &servQuick::getImageFormat ),


on_set_int_bool  (        MSG_QUICK_IMAGE_INVERT,       &servQuick::setInvert),
on_get_bool      (        MSG_QUICK_IMAGE_INVERT,       &servQuick::getInvert),

on_set_int_bool  (        MSG_QUICK_IMAGE_COLOR,        &servQuick::setColorType ),
on_get_bool      (        MSG_QUICK_IMAGE_COLOR,        &servQuick::getColorType ),

on_set_int_int   (        MSG_QUICK_CHANNEL,            &servQuick::setChannelSelected ),
on_get_int       (        MSG_QUICK_CHANNEL,            &servQuick::getChannelSelected ),

on_set_int_bool  (        MSG_QUICK_WAVE_FROM,          &servQuick::setWaveSource ),
on_get_bool      (        MSG_QUICK_WAVE_FROM,          &servQuick::getWaveSource ),

on_set_int_int   (        MSG_QUICK_WAVE_MEM_FORMAT,    &servQuick::setWaveFormat ),
on_get_int       (        MSG_QUICK_WAVE_MEM_FORMAT,    &servQuick::getWaveFormat ),

on_set_int_int   (        MSG_QUICK_WAVE_SCR_FORMAT,    &servQuick::setWaveFormat ),
on_get_int       (        MSG_QUICK_WAVE_SCR_FORMAT,    &servQuick::getWaveFormat ),

on_set_int_int   (        MSG_QUICK_WAVE_SIZE,          &servQuick::setWaveSize ),
on_get_int       (        MSG_QUICK_WAVE_SIZE,          &servQuick::getWaveSize ),

on_set_int_int   (        MSG_QUICK_MEAS_ALL_SRC,       &servQuick::setMeasallsrc ),
on_get_int_attr  (        MSG_QUICK_MEAS_ALL_SRC,       &servQuick::getMeasallsrc,  &servQuick::onMeasAllSrc ),

on_set_int_int   (        MSG_QUICK_STAT_RESET,         &servQuick::setStatReset ),
on_get_int       (        MSG_QUICK_STAT_RESET,         &servQuick::getStatReset ),

on_set_int_int   (        MSG_QUICK_SELECT_SAVE,         &servQuick::setSaveSelect ),
on_get_int       (        MSG_QUICK_SELECT_SAVE,         &servQuick::getSaveSelect ),

on_set_void_void (        CMD_SERVICE_RST,              &servQuick::rst ),
on_set_int_void  (        CMD_SERVICE_STARTUP,          &servQuick::startup ),
//on_set_int_int   (        CMD_SERVICE_ACTIVE,           &servQuick::setActive ),

end_of_entry()



servQuick::servQuick( QString name, ServiceId id ) : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();

    ISerial::mVersion = 0xA;

    for(int i=0; i<12; i++)
    {
        mUiAttr.setVisible(MSG_QUICK_OPERATION, i, false);
    }

    mUiAttr.setVisible(MSG_QUICK_OPERATION,  SaveImage,  true);
    mUiAttr.setVisible(MSG_QUICK_OPERATION,  SaveWave,   true);
    mUiAttr.setVisible(MSG_QUICK_OPERATION,  SaveSetup,  true);
    mUiAttr.setVisible(MSG_QUICK_OPERATION,  AllMeasure, true);
    mUiAttr.setVisible(MSG_QUICK_OPERATION,  StatReset,  true);
    mUiAttr.setVisible(MSG_QUICK_OPERATION,  Print,      true);
    mUiAttr.setVisible(MSG_QUICK_OPERATION,  Email,      true);
    mUiAttr.setVisible(MSG_QUICK_OPERATION,  SelectSave, false);
}

int servQuick::serialOut( CStream &stream, serialVersion &ver )
{
    ver = mVersion;
    stream.write(QStringLiteral("m_nOper"),         m_nOper);
    stream.write(QStringLiteral("m_nMeasSrc"),      m_nMeasSrc);
    return 0;
}
int servQuick::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("m_nOper"),         m_nOper);
        stream.read(QStringLiteral("m_nMeasSrc"),      m_nMeasSrc);
    }
    return 0;
}
void servQuick::rst()
{
    m_nOper        =   SaveImage;
    m_nMeasSrc     =   chan1;
    m_nSaveSelect  =   0x0;
}
int servQuick::startup()
{
    updateAllUi( false );

    mUiAttr.setVisible(MSG_QUICK_WAVE_SIZE, false);

    bool bMso = sysCheckLicense(OPT_MSO);

    for(int src=d0; src<=d15; src++)
    {
        mUiAttr.setEnableVisible(MSG_QUICK_CHANNEL, src, bMso,bMso);
    }
    return 0;
}

void servQuick::registerSpy()
{

}

int servQuick::getOperation()
{
    return m_nOper;
}

int servQuick::setOperation(int o)
{
    m_nOper = o;

    if(m_nOper == SaveWave)
    {
        defer(MSG_QUICK_WAVE_FROM );
        defer(MSG_QUICK_WAVE_MEM_FORMAT );
    }

    return ERR_NONE;
}

int servQuick::quickAction(int)
{
    int ret = 0;
    switch(m_nOper)
    {
        case SaveImage:
            ret = post(serv_name_storage, servStorage::FUNC_QUICK_SAVE_IMG, m_nOper);
        break;

        case SaveWave:
            ret = post(serv_name_storage, servStorage::FUNC_QUICK_SAVE_WAV, m_nOper);
        break;

        case SaveSetup:
            ret = post(serv_name_storage, servStorage::FUNC_QUICK_SAVE_STP, m_nOper);
            break;

        case LoadSetup:
            qDebug() << "Load setup by quick";
        break;

        case LoadWave:
            qDebug() << "Load wave by quick";
        break;

        case FreezeWave:
            qDebug() << "freeze wave by quick";
        break;

        case AllMeasure:
        {
            int src = 0;
            query(serv_name_measure,MSG_APP_MEAS_ALL_SRC, src);
            if( src == 0 )
            {
                post(serv_name_measure, MSG_APP_MEAS_ALL_SRC, m_nMeasSrc);
            }
            else
            {
                post(serv_name_measure, MSG_APP_MEAS_ALL_SRC, 0);
            }
            break;
        }

        case StatReset:
            {
                if(m_nStatReset == 0)
                {
                    ret = post(serv_name_measure, MSG_APP_MEAS_STAT_RESET, 0);
                }
                else if(m_nStatReset == 1)
                {
                    ret = post(serv_name_mask, MSG_MASK_RESET_STAT, 0);
                }
                else
                {Q_ASSERT(false);}

                break;
            }
        case Print:
            ret = post(serv_name_utility_Print, MSG_PRINTER_START, (int)1);
        break;

        case Email:
            ret = post(serv_name_utility_Email, MSG_EMAIL_SEND, (int)1);
        break;

        case LoadMask:
            qDebug() << "Load mask by quick";
        break;

        case LoadArb:
            qDebug() << "Load arb by quick";
        break;
        case Record:
            {
                qDebug() << "Load arb by quick";
                bool bOn = false;
                ret = send(serv_name_wrec,    MSG_RECORD_ONOFF, true);
                ret = query(serv_name_wrec, MSG_RECORD_START, bOn);
                bOn = !bOn;
                ret = post(serv_name_wrec, MSG_RECORD_START, bOn);
                break;
            }
        case SelectSave:
            {
                qDebug() << "Select save:"<< m_nSaveSelect;

                if( get_bit(m_nSaveSelect,0) )
                {
                    post(serv_name_storage, servStorage::FUNC_QUICK_SAVE_IMG, (int)SaveImage);
                }
                if( get_bit(m_nSaveSelect,1) )
                {
                    post(serv_name_storage, servStorage::FUNC_QUICK_SAVE_STP, (int)SaveSetup);
                }
                if( get_bit(m_nSaveSelect,2) )
                {
                    post(serv_name_storage, servStorage::FUNC_QUICK_SAVE_WAV, (int)SaveWave);
                }

                break;
            }
    default:
        qDebug() << "Quick:" << m_nOper;
        break;
    }

    return ret;
}



///**
// * @brief servQuick::setPrefixName
// * @param f
// * @return
// */
//DsoErr servQuick::setPrefixName(QString& f)
//{
//    post(serv_name_storage, MSG_STORAGE_PREFIX, f);
//    return ERR_NONE;
//}

//QString servQuick::getPrefixName()
//{
//    QString prefix;
//    query(serv_name_storage, MSG_STORAGE_PREFIX, prefix);
//    return prefix;
//}


///
/// \brief servQuick::setImageFormat
/// \param fmt
/// \return
///
DsoErr servQuick::setImageFormat(int fmt)
{
    post(serv_name_storage, MSG_STORAGE_IMAGE_FORMAT, fmt);
    return ERR_NONE;
}

///
/// \brief servQuick::getImageFormat
/// \return
///
int servQuick::getImageFormat()
{
    int f;
    query(serv_name_storage, MSG_STORAGE_IMAGE_FORMAT, f);
    return f;
}


///
/// \brief servQuick::setChannelSelected
/// \param opt
/// \return
///
DsoErr servQuick::setChannelSelected( int opt)
{
    post(serv_name_storage, MSG_STORAGE_CHANNEL, opt);
    return ERR_NONE;
}

///
/// \brief servQuick::getChannelSelected
/// \return
///
int servQuick::getChannelSelected()
{
    int opt;
    query(serv_name_storage, MSG_STORAGE_CHANNEL, opt);
    return opt;
}

///
/// \brief servQuick::setWaveSource
/// \param f
/// \return
///
DsoErr servQuick::setWaveSource( bool b )
{
    m_waveSource =   b;

    return post(serv_name_storage, MSG_STORAGE_WAVE_DEPTH, (bool)b);;
}

///
/// \brief servQuick::getWaveSource
/// \return
///
bool servQuick::getWaveSource()
{
    return  m_waveSource;
}

///
/// \brief servQuick::setWaveFormat
/// \param f
/// \return
///
DsoErr servQuick::setWaveFormat( int f )
{ 
   post(serv_name_storage, MSG_STORAGE_WAVE_FORMAT, f);

   if( (f + servFile::FILETYPE_BIN) == servFile::FILETYPE_WFM)
   {
       mUiAttr.setVisible(MSG_QUICK_CHANNEL, false);
   }
   else
   {
       mUiAttr.setVisible(MSG_QUICK_CHANNEL,true);
   }

   return ERR_NONE;
}


///
/// \brief servQuick::getWaveFormat
/// \return
///
int servQuick::getWaveFormat()
{
    int f;
    query(serv_name_storage, MSG_STORAGE_WAVE_FORMAT, f);
    return  f;
}

///
/// \brief The size MUST not large than the current memory depth
/// \param size
/// \return
///
DsoErr servQuick::setWaveSize(int size)
{
    post(serv_name_storage, MSG_STORAGE_WAVE_SIZE, size);
    return ERR_NONE;
}

///
/// \brief servQuick::getWaveSize
/// \return
///
int servQuick::getWaveSize()
{
    int size = 0;
    query(serv_name_storage, MSG_STORAGE_WAVE_SIZE, size);
    setuiPostStr(" (byte)");
    return size;
}


///
/// \brief servQuick::setInvert
/// \param b
/// \return
///For Image
DsoErr servQuick::setInvert(bool b)
{
    post(serv_name_storage, MSG_STORAGE_IMAGE_INVERT, b);
    return ERR_NONE;
}

///
/// \brief servQuick::getInvert
/// \return
///
bool servQuick::getInvert()
{
    bool b;
    query(serv_name_storage, MSG_STORAGE_IMAGE_INVERT, b);
    return b;
}

///
/// \brief servQuick::setColorType
/// \param b
/// \return
///For Image
DsoErr servQuick::setColorType(bool b)
{
    post(serv_name_storage, MSG_STORAGE_IMAGE_COLOR, b);
    return ERR_NONE;
}

///
/// \brief servQuick::getColorType
/// \return
///
bool servQuick::getColorType()
{
    bool b;
    query(serv_name_storage, MSG_STORAGE_IMAGE_COLOR, b);
    return b;
}


DsoErr servQuick::setMeasallsrc(int v)
{
    m_nMeasSrc = v;
    return ERR_NONE;
}

int servQuick::getMeasallsrc()
{
    return m_nMeasSrc;
}

DsoErr servQuick::setStatReset(int value)
{
    m_nStatReset = value;
    return ERR_NONE;
}

int servQuick::getStatReset()
{
    return m_nStatReset;
}

DsoErr servQuick::setSaveSelect(int value)
{
    m_nSaveSelect = value;
    if( get_bit(m_nSaveSelect,2) )
    {
        defer(MSG_QUICK_WAVE_FROM );
        defer(MSG_QUICK_WAVE_MEM_FORMAT );
    }
    return ERR_NONE;
}

int servQuick::getSaveSelect()
{
    return m_nSaveSelect;
}

int servQuick::onMeasAllSrc()
{
    serviceExecutor* pServ = (serviceExecutor*)findService(serv_name_measure);
    Q_ASSERT(NULL != pServ);

    ui_attr::uiAttr *pAttr = pServ->getAttr();
    Q_ASSERT(NULL != pAttr);

    bool enable  = true;
    bool visible = false;

    for( int i = (int)chan1; i<=(int)chan4; i++)
    {
        //enable  = pAttr->getOption(MSG_APP_MEAS_ALL_SRC, i)->getEnable();
        visible = pAttr->getOption(MSG_APP_MEAS_ALL_SRC, i)->getVisible();

        mUiAttr.setEnableVisible( MSG_QUICK_MEAS_ALL_SRC, i, enable, visible);
    }

    for( int i = (int)m1; i<=(int)m4; i++)
    {
        //enable  = pAttr->getOption(MSG_APP_MEAS_ALL_SRC, i)->getEnable();
        visible = pAttr->getOption(MSG_APP_MEAS_ALL_SRC, i)->getVisible();

        mUiAttr.setEnableVisible( MSG_QUICK_MEAS_ALL_SRC, i, enable, visible);
    }

    for( int i = (int)r1; i<=(int)r10; i++)
    {
        //enable  = pAttr->getOption(MSG_APP_MEAS_ALL_SRC, i)->getEnable();
        visible = pAttr->getOption(MSG_APP_MEAS_ALL_SRC, i)->getVisible();

        mUiAttr.setEnableVisible( MSG_QUICK_MEAS_ALL_SRC, i, enable, visible);
    }

    //setuiChange(MSG_QUICK_MEAS_ALL_SRC);
    return ERR_NONE;
}

