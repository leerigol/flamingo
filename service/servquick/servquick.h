#ifndef ULTRAKEY
#define ULTRAKEY

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"

class servQuick        : public serviceExecutor,
                         public ISerial
{
    Q_OBJECT

    DECLARE_CMD()
public:
    servQuick(QString name = serv_name_quick, ServiceId eId = E_SERVICE_ID_QUICK);
    enum
    {
        SaveImage,
        SaveWave,
        SaveSetup,
        LoadSetup,
        LoadWave,
        FreezeWave,
        AllMeasure,
        StatReset,
        Print,
        Email,
        LoadMask,
        LoadArb,
        Record,
        SelectSave,
    };
public:
    int         serialOut( CStream &stream, serialVersion &ver );
    int         serialIn( CStream &stream, unsigned char ver );
    void        rst();
    int         startup();
    void        registerSpy();

private:
    int         getOperation(void);
    int         setOperation(int);
    int         quickAction(int);

    DsoErr      setPrefixName(QString&);
    QString     getPrefixName(void);

    DsoErr      setImageFormat(int );
    int         getImageFormat();

    DsoErr      setWaveFormat(int );
    int         getWaveFormat(void);

    DsoErr      setInvert(bool );
    bool        getInvert();

    DsoErr      setColorType(bool );
    bool        getColorType();

    DsoErr      setCsvSequence(bool );
    bool        getCsvSequence();

    DsoErr      setFileFormat(int );
    int         getFileFormat();

    DsoErr      setWaveSource(bool  );
    bool        getWaveSource();

    DsoErr      setChannelSelected( int );
    int         getChannelSelected();

    DsoErr      setWaveSize( int );
    int         getWaveSize( void );

    DsoErr      setMeasallsrc(int src);
    int         getMeasallsrc();

    DsoErr      setStatReset(int value);
    int         getStatReset();

    DsoErr      setSaveSelect(int value);
    int         getSaveSelect();

private:
    int          onMeasAllSrc();

private:
    int       m_nOper;
    int       m_nMeasSrc;
    bool      m_waveSource;
    int       m_nStatReset;
    int       m_nSaveSelect;
};
#endif // ULTRAKEY

