#ifndef _SERVICE_ID_H_
#define _SERVICE_ID_H_

/*! \enum ServiceId
 * 通道1／2／3／4必须使用:
 * - E_SERVICE_ID_CH1
 * - E_SERVICE_ID_CH2
 * - E_SERVICE_ID_CH3
 * - E_SERVICE_ID_CH4
 * 引擎使用该id来区分硬件通道
 *
 * 禁止在中间插入ID，不管那类，如果要保存参数到FRAM，
 * 必须紧挨E_SERVICE_ID_ALL_FRAM 前加入，
   属于最后一个ID，不影响之前的参数
*/
enum ServiceId
{
    E_SERVICE_ID_NONE = 0,
    E_SERVICE_ID_CH1 = 1,
    E_SERVICE_ID_CH2,
    E_SERVICE_ID_CH3,
    E_SERVICE_ID_CH4,

    E_SERVICE_ID_CAL,

    E_SERVICE_ID_HORI,

    E_SERVICE_ID_TRIG,
    E_SERVICE_ID_TRIG_ZONE,
    E_SERVICE_ID_TRIG_Edge,
    E_SERVICE_ID_TRIG_Pulse,
    E_SERVICE_ID_TRIG_Slope,
    E_SERVICE_ID_TRIG_Video,
    E_SERVICE_ID_TRIG_Pattern,
    E_SERVICE_ID_TRIG_Duration,
    E_SERVICE_ID_TRIG_Runt,
    E_SERVICE_ID_TRIG_Over,
    E_SERVICE_ID_TRIG_Window,
    E_SERVICE_ID_TRIG_Timeout,
    E_SERVICE_ID_TRIG_Delay,
    E_SERVICE_ID_TRIG_SetupHold,
    E_SERVICE_ID_TRIG_Nth,
    E_SERVICE_ID_TRIG_RS232,
    E_SERVICE_ID_TRIG_I2C,
    E_SERVICE_ID_TRIG_SPI,
    E_SERVICE_ID_TRIG_AB,

    E_SERVICE_ID_TRIG_CAN,
    E_SERVICE_ID_TRIG_LIN,
    E_SERVICE_ID_TRIG_FLEXRAY,
    E_SERVICE_ID_TRIG_IIS,
    E_SERVICE_ID_TRIG_USB,
    E_SERVICE_ID_TRIG_1553B,
    E_SERVICE_ID_TRIG_1WIRE,

    E_SERVICE_ID_UTILITY,
    E_SERVICE_ID_UTILITY_PRINTER,
    E_SERVICE_ID_UTILITY_EMAIL,
    E_SERVICE_ID_UTILITY_WIFI,
    E_SERVICE_ID_UTILITY_IOSET,

    E_SERVICE_ID_STORAGE,


    E_SERVICE_ID_SOURCE1,
    E_SERVICE_ID_SOURCE2,

    E_SERVICE_ID_RECORD,

    E_SERVICE_ID_SCPI,

    E_SERVICE_ID_MATH1,
    E_SERVICE_ID_MATH2,
    E_SERVICE_ID_MATH3,
    E_SERVICE_ID_MATH4,

    E_SERVICE_ID_CURSOR,

    E_SERVICE_ID_REF,

    E_SERVICE_ID_LA,

    E_SERVICE_ID_DEC1,
    E_SERVICE_ID_DEC2,
    E_SERVICE_ID_DEC3,
    E_SERVICE_ID_DEC4,

    E_SERVICE_ID_SEARCH,


    E_SERVICE_ID_PLOT,

    E_SERVICE_ID_TRACE,
    E_SERVICE_ID_DISPLAY,
    E_SERVICE_ID_QUICK,

    E_SERVICE_ID_MEASURE,
    E_SERVICE_ID_COUNTER,
    E_SERVICE_ID_DVM,
    E_SERVICE_ID_UPA,
    E_SERVICE_ID_UPA_POWERQ,
    E_SERVICE_ID_UPA_RIPPLE,

    E_SERVICE_ID_EYEJIT,
    E_SERVICE_ID_HISTO,

    E_SERVICE_ID_MASK,

    E_SERVICE_ID_DG,
    E_SERVICE_ID_AUTO_SET,  //! auto set

    E_SERVICE_ID_LICENSE,

    E_SERVICE_ID_MEMORY,
    E_SERVICE_ID_ALL_FRAM, //Following no saving fram

                            //! 系统辅助服务
    E_SERVICE_ID_SYSTEM = 96,
    E_SERVICE_ID_DSO,       //! 示波器通用服务
    E_SERVICE_ID_ERR_LOGER, //! UI服务
    E_SERVICE_ID_UI = E_SERVICE_ID_ERR_LOGER,
    E_SERVICE_ID_HELP,      //! HELP

    E_SERVICE_ID_AUTO = 128,

    E_SERVICE_ID_MAX = 256
};

#endif // SERVICE_ID_H

