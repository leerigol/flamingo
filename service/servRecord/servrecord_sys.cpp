
#include "servrecord.h"
#include "../../com/keyevent/rdsokey.h"

#define ar ar_out
#define ar_pack_e ar_out
int  servRecord::serialOut( CStream &stream, serialVersion &ver )
{
    ISerial::serialOut( stream, ver );

    ar("on",     bStatus);
    ar("beeper", bBeeper );
    ar("repeat", bPlayRepeat );
    ar("dir", bPlayDec );

    ar("rec_int", nRecordInterval );
    ar("rec_ed", nRecordFrames );

    ar("play_int", nPlayInterval );
    ar("play_cur", nPlayCurrent );
    ar("play_start", nStartFrame );
    ar("play_end", nPlayCurrent );

    ar("play_from", nFromFrame );
    ar("play_to", nToFrame );

    return ERR_NONE;
}

#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int  servRecord::serialIn( CStream &stream, serialVersion ver )
{
    ISerial::serialIn( stream, ver );
    if(ver == mVersion)
    {
        ar("on",     bStatus);
        ar("beeper", bBeeper );
        ar("repeat", bPlayRepeat );
        ar("dir", bPlayDec );

        ar("rec_int", nRecordInterval );
        ar("rec_ed", nRecordFrames );

        ar("play_int", nPlayInterval );
        ar("play_cur", nPlayCurrent );
        ar("play_start", nStartFrame );
        ar("play_end", nPlayCurrent );

        ar("play_from", nFromFrame );
        ar("play_to", nToFrame );
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

void servRecord::rst()
{
    //! inner
    m_State = Record_disable;

    mbSaveed = false;
    mbBackFast = false;
    mbForwardFast = false;

    bStatus     = false;
    bPlaying    = false;
    bRecording  = false;
    bFrameStart = false;
    bOption     = false;
    bBeeper     = true;
    bPlayRepeat = false;
    bPlayDec    = false;

    nRecordInterval = def_rec_interval;

    nRecordFrames   = 0;
    nRecordMax      = 1000;

    nPlayInterval   = def_play_interval;
    nPlayCurrent    = 0;

    nStartFrame     = 0;
    nEndFrame       = 0;

    nFromFrame      = 1;
    nToFrame        = 10;

    mbInSession = false;

    changeState( Record_disable );
}

int  servRecord::startup()
{
    async(MSG_RECORD_ONOFF, bStatus);

    mUiAttr.setVisible(MSG_RECORD_SAVE2FILE, false);
    mUiAttr.setVisible(MSG_RECORD_SAVE,      false);
    updateAllUi();
    return ERR_NONE;
}

void servRecord::on_init()
{
    linkChange( MSG_RECORD_ONOFF, MSG_RECORD_START );

    linkChange( MSG_REC_PLAY_BACK, MSG_RECORD_PLAYDIR, MSG_RECORD_PLAYMODE );
    linkChange( MSG_REC_PLAY_NEXT, MSG_RECORD_PLAYDIR, MSG_RECORD_PLAYMODE );
}

void servRecord::onTimeout(int timerid )
{
    if ( timerid == record_tick_timer_id )
    {
        on_record_timeout( timerid );
    }
    else if ( timerid == play_tick_timer_id )
    {
        on_play_timeout( timerid );
    }
    else
    {}
}

void servRecord::on_record_timeout( int id )
{
    //! get now
    queryEngine( qENGINE_RECORD_PLAY_FRAME_ID, nRecordFrames );
    nPlayCurrent = nRecordFrames;

    setuiChange(MSG_RECORD_CURRENT);
    setuiChange(servRecord::cmd_reced_count );

    bool bIdle;
    queryEngine( qENGINE_RECORD_IDLE, bIdle );

    //! stoped || record auto completed
    if ( bIdle || nRecordFrames >= nRecordMax )
    {
        async( MSG_RECORD_START, false );
        stopTimer( id );

        if ( bBeeper )
        {
            syncEngine( ENGINE_BEEP_SHORT );
        }
    }
}
void servRecord::on_play_timeout( int id )
{
    //! get now
    queryEngine( qENGINE_RECORD_PLAY_FRAME_ID, nPlayCurrent );
    setuiChange( MSG_RECORD_CURRENT );

    bool bIdle;
    queryEngine( qENGINE_RECORD_IDLE, bIdle );

    //! stoped
    if ( bIdle )
    {
        async( MSG_RECORD_PLAY, false  );
        stopTimer( id );
    }

    if( (!bPlayRepeat))
    {
        if( (!bPlayDec) && (nPlayCurrent >= nEndFrame) )
        {
            nPlayCurrent = nEndFrame;
            async( MSG_RECORD_PLAY, false  );
            stopTimer( id );
        }
        else if( (bPlayDec) && (nPlayCurrent <= nStartFrame) )
        {
            nPlayCurrent = nEndFrame;
            async( MSG_RECORD_PLAY, false  );
            stopTimer( id );
        }
        else
        {return;}
    }
}
