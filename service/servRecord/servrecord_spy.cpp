#include "servrecord.h"
#include "../servutility/servutility.h"
#include "../../service/servdso/sysdso.h"

void servRecord::on_hori_changed()
{ 
    bool bRecEnable;

    bRecEnable = checkRecEnable();

    if( (!bRecEnable) && bStatus)
    {
        id_async(E_SERVICE_ID_RECORD, MSG_RECORD_ONOFF, false);
    }

    mUiAttr.setEnable( MSG_RECORD_ONOFF, bRecEnable );

    int maxFrames = getMaxFrames();
    if(nRecordMax > maxFrames)
    {
        async( MSG_RECORD_FRAMES, maxFrames );
    }

    setuiChange(MSG_RECORD_MAXFRAMES);
}

void servRecord::on_hori_run( int controlAction )
{
#if 1
    //! by user run
    if ( (controlAction == Control_Run) && (m_State != Record_ing) )
    {
        if ( getStatus()  )
        { changeState( Record_empty ); }
        else
        { changeState( Record_disable ); }
    }
    //! do nothing
    else
    {}
#endif
}

bool servRecord::checkRecEnable()
{
    enter_service_context();

    //! time mode
    query_service_enum( serv_name_hori,
                        MSG_HORI_TIME_VIEW_MODE,
                        AcquireTimemode, timeMode );
    if ( !_bOK )
    { return false; }

    if ( !((timeMode == Acquire_YT) ||
           (timeMode == Acquire_SCAN))  )
    { return false; }

    exit_service_context();

    return true;
}

qlonglong servRecord::accInterval( bool bAcc,
                                   qlonglong interval )
{
    qlonglong acced;
    if ( bAcc )
    { acced = interval / 10; }
    else
    { acced = interval; }

    if ( acced < min_play_interval )
    { acced = min_play_interval; }
    else if ( acced > max_play_interval )
    { acced = max_play_interval; }
    else
    {}

    return acced;
}

void servRecord::changeState( RecordState stat )
{
    ssync( cmd_inner_state, stat );
}

void servRecord::setState( RecordState stat )
{
    if ( m_State == Record_disable )
    { disable_xxx( stat ); }
    else if ( m_State == Record_empty )
    { empty_xxx( stat ); }
    else if ( m_State == Record_ing )
    { recording_xxx( stat );}
    else if ( m_State == Record_end )
    { recorded_xxx( stat );}
    else if ( m_State == Play_ing )
    { playing_xxx( stat );}
    else
    { Q_ASSERT( false ); }

    m_State = stat;
}

servRecord::RecordState servRecord::getState()
{
    return m_State;
}

void servRecord::disable_xxx( RecordState stat )
{
    if ( stat == Record_empty )
    { in_empty(); }
    else if ( stat == Record_disable )
    { in_disable(); }
    else
    {
        in_disable();
        qDebug()<<__FILE__<<__LINE__<<"stat:"<<stat;
    }
}
void servRecord::empty_xxx( RecordState stat )
{
    if ( stat == Record_disable )
    { in_disable(); }
    else if ( stat == Record_ing )
    { in_recording(); }
    else if ( stat == Record_empty )
    { in_empty(); }
    else if ( stat == Record_end )
    { in_recorded(); }
    else
    { qDebug()<<stat; Q_ASSERT( false ); }
}
void servRecord::recording_xxx( RecordState stat )
{
    if ( stat == Record_ing )
    { return; }
    else if ( stat == Record_empty )
    { in_empty(); }
    else if ( stat == Record_end )
    { in_recorded(); }
    else if ( stat == Record_disable )
    { in_disable(); }
    else if ( stat == Record_ing )
    {
        //! do nothing
    }
    else
    { qDebug()<<stat; Q_ASSERT( false ); }
}
void servRecord::recorded_xxx( RecordState stat )
{
    if ( stat == Record_end )
    { return; }
    else if ( stat == Play_ing )
    { in_playing(); }
    else if ( stat == Record_ing )
    { in_recording(); }
    else if ( stat == Record_disable )
    { in_disable(); }
    else if ( stat == Record_empty )
    { in_empty(); }
    else if ( stat == Record_end )
    {
        //! do nothing
    }
    else
    { qDebug()<<m_State<<stat; Q_ASSERT( false ); }
}

void servRecord::playing_xxx( RecordState stat )
{
    if ( stat == Play_ing )
    { return; }
    else if ( stat == Record_disable )
    { in_disable(); }
    else if ( stat == Record_end )
    { in_recorded(); }
    else if ( stat == Play_ing )
    {
        //! do nothing
    }
    else
    { qDebug()<<stat; Q_ASSERT( false ); }
}

/*![dba] *UIEN = 1 直接禁用其他服务消息，但会产生bug，恢复不该恢复的菜单。*/
#define UIEN 0

void servRecord::in_disable()
{
    //! ui attr
    mUiAttr.setEnable( MSG_RECORD_START, false );

    mUiAttr.setEnable( MSG_RECORD_PLAY, false );
    mUiAttr.setEnable( MSG_REC_PLAY_BACK, false );
    mUiAttr.setEnable( MSG_REC_PLAY_NEXT, false );

    mUiAttr.setEnable( MSG_RECORD_CURRENT, false );
    mUiAttr.setEnable( MSG_RECORD_FRAMESTART, false );

    mUiAttr.setEnable( MSG_RECORD_FRAMES, false );
    mUiAttr.setEnable( MSG_RECORD_MAXFRAMES, false );
    mUiAttr.setEnable( MSG_RECORD_SET2MAX, false );

    mUiAttr.setVisible( MSG_RECORD_STARTFRAME, false );
    mUiAttr.setVisible( MSG_RECORD_ENDFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_STARTFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_ENDFRAME, false );

    mUiAttr.setVisible( MSG_RECORD_FROMFRAME, false );
    mUiAttr.setVisible( MSG_RECORD_TOFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_FROMFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_TOFRAME, false );

    mUiAttr.setEnable( MSG_RECORD_SAVE2FILE, false );
    mUiAttr.setEnable( MSG_RECORD_SAVE, false );

#if UIEN
    //! state
    restoreUiEns( mEnIdPre, mDependsServices );
    deleteUiEns( mEnIdPre, mDependsServices );
    deleteUiEns( mEnIdPost, mDependsServices );
    mbSaveed = false;
#else
    serviceExecutor::send(serv_name_utility,
                          servUtility::cmd_lock_input,
                           false);
    //! 使能scpi指令
    sysScpiExecuteEn(true);
#endif
}
void servRecord::in_empty()
{
    //! paras
    nPlayCurrent  = 0;
    nRecordFrames = 0;
    nStartFrame   = 0;
    nEndFrame     = 0;

    //! ui attr
    mUiAttr.setEnable( MSG_RECORD_START, true );

    mUiAttr.setEnable( MSG_RECORD_PLAY, false );
    mUiAttr.setEnable( MSG_REC_PLAY_BACK, false );
    mUiAttr.setEnable( MSG_REC_PLAY_NEXT, false );

    mUiAttr.setEnable( MSG_RECORD_CURRENT, false );
    mUiAttr.setEnable( MSG_RECORD_FRAMESTART, false );

    mUiAttr.setEnable( MSG_RECORD_FRAMES, true );
    mUiAttr.setEnable( MSG_RECORD_MAXFRAMES, true );
    mUiAttr.setEnable( MSG_RECORD_SET2MAX, true );

    mUiAttr.setVisible( MSG_RECORD_STARTFRAME, false );
    mUiAttr.setVisible( MSG_RECORD_ENDFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_STARTFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_ENDFRAME, false );

    mUiAttr.setVisible( MSG_RECORD_FROMFRAME, false );
    mUiAttr.setVisible( MSG_RECORD_TOFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_FROMFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_TOFRAME, false );

    mUiAttr.setEnable( MSG_RECORD_SAVE2FILE, false );
    mUiAttr.setEnable( MSG_RECORD_SAVE, false );

    mUiAttr.setEnable( MSG_RECORD_MORE, true );

#if UIEN
    //! save pre
    if ( !mbSaveed )
    { saveUiEns( mEnIdPre, mDependsServices ); }
    else
    { restoreUiEns( mEnIdPost, mDependsServices); }

    //! save post
    if ( !mbSaveed )
    {
        saveUiEns( mEnIdPost, mDependsServices  );
        mbSaveed = true;
    }
#else
    serviceExecutor::send(serv_name_utility,
                          servUtility::cmd_lock_input,
                           false);
    sysScpiExecuteEn(true);
#endif

}

void servRecord::in_recording()
{
    //! ui attr
    mUiAttr.setEnable( MSG_RECORD_START, true );

    mUiAttr.setEnable( MSG_RECORD_PLAY, false );
    mUiAttr.setEnable( MSG_REC_PLAY_BACK, false );
    mUiAttr.setEnable( MSG_REC_PLAY_NEXT, false );

    mUiAttr.setEnable( MSG_RECORD_CURRENT, false );
    mUiAttr.setEnable( MSG_RECORD_FRAMESTART, false );

    mUiAttr.setEnable( MSG_RECORD_FRAMES, false );
    mUiAttr.setEnable( MSG_RECORD_MAXFRAMES, false );
    mUiAttr.setEnable( MSG_RECORD_SET2MAX, false );

    mUiAttr.setVisible( MSG_RECORD_STARTFRAME, false );
    mUiAttr.setVisible( MSG_RECORD_ENDFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_STARTFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_ENDFRAME, false );

    mUiAttr.setVisible( MSG_RECORD_FROMFRAME, false );
    mUiAttr.setVisible( MSG_RECORD_TOFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_FROMFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_TOFRAME, false );

    mUiAttr.setEnable( MSG_RECORD_SAVE2FILE, false );
    mUiAttr.setEnable( MSG_RECORD_SAVE, false );

    mUiAttr.setEnable( MSG_RECORD_MORE, false );
#if UIEN
    //! acquire
    setUiEnables( mDependsServices, false );
#else
    serviceExecutor::send(serv_name_utility,
                          servUtility::cmd_lock_input,
                          true);
    //! 禁用scpi指令
    QList<int> cmdList;
    cmdList<<MSG_RECORD_START<<MSG_RECORD_ONOFF;
    sysScpiAddMap(getName(), cmdList);
    cmdList.clear();
    cmdList<<MSG_STORAGE_PRNTSCR;
    sysScpiAddMap(serv_name_storage, cmdList);

    sysScpiExecuteEn(false);
#endif
}
void servRecord::in_recorded()
{
    //! ui attr
    mUiAttr.setEnable( MSG_RECORD_START, true );

    mUiAttr.setEnable( MSG_RECORD_PLAY, true );
    mUiAttr.setEnable( MSG_REC_PLAY_BACK, true );
    mUiAttr.setEnable( MSG_REC_PLAY_NEXT, true );

    mUiAttr.setEnable( MSG_RECORD_CURRENT, true );
    mUiAttr.setEnable( MSG_RECORD_FRAMESTART, true );

    mUiAttr.setEnable( MSG_RECORD_FRAMES, true );
    mUiAttr.setEnable( MSG_RECORD_MAXFRAMES, true );
    mUiAttr.setEnable( MSG_RECORD_SET2MAX, true );

    mUiAttr.setVisible( MSG_RECORD_STARTFRAME, true );
    mUiAttr.setVisible( MSG_RECORD_ENDFRAME, true );
    mUiAttr.setEnable( MSG_RECORD_STARTFRAME, true );
    mUiAttr.setEnable( MSG_RECORD_ENDFRAME, true );

    mUiAttr.setVisible( MSG_RECORD_FROMFRAME, true );
    mUiAttr.setVisible( MSG_RECORD_TOFRAME, true );
    mUiAttr.setEnable( MSG_RECORD_FROMFRAME, true );
    mUiAttr.setEnable( MSG_RECORD_TOFRAME, true );

    mUiAttr.setEnable( MSG_RECORD_SAVE2FILE, true );
    mUiAttr.setEnable( MSG_RECORD_SAVE, true );

    mUiAttr.setEnable( MSG_RECORD_MORE, true );
#if UIEN
    //! acquire
    restoreUiEns( mEnIdPost, mDependsServices );

    mbForwardFast = false;
    mbBackFast = false;
#else
    serviceExecutor::send(serv_name_utility,
                          servUtility::cmd_lock_input,
                          false);

    sysScpiExecuteEn(true);
#endif
}
void servRecord::in_playing()
{
    //! ui attr
    mUiAttr.setEnable( MSG_RECORD_START, false );

    mUiAttr.setEnable( MSG_RECORD_PLAY, true );
    mUiAttr.setEnable( MSG_REC_PLAY_BACK, true );
    mUiAttr.setEnable( MSG_REC_PLAY_NEXT, true );

    mUiAttr.setEnable( MSG_RECORD_CURRENT, false );
    mUiAttr.setEnable( MSG_RECORD_FRAMESTART, false );

    mUiAttr.setEnable( MSG_RECORD_FRAMES, false );
    mUiAttr.setEnable( MSG_RECORD_MAXFRAMES, false );
    mUiAttr.setEnable( MSG_RECORD_SET2MAX, false );

    mUiAttr.setEnable( MSG_RECORD_STARTFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_ENDFRAME, false );

    mUiAttr.setEnable( MSG_RECORD_FROMFRAME, false );
    mUiAttr.setEnable( MSG_RECORD_TOFRAME, false );

    mUiAttr.setEnable( MSG_RECORD_SAVE2FILE, false );
    mUiAttr.setEnable( MSG_RECORD_SAVE, false );

    mUiAttr.setEnable( MSG_RECORD_MORE, false );

#if UIEN
    //! acquire
    setUiEnables( mDependsServices, false,
                  MSG_CHAN_PLAY_PRE,
                  MSG_CHAN_PLAY_NEXT,
                  MSG_CHAN_PLAY_STOP);
#else
    serviceExecutor::send(serv_name_utility,
                          servUtility::cmd_lock_input,
                          true);

    //! 禁用scpi指令
    QList<int> cmdList;
    cmdList <<MSG_RECORD_PLAY
            <<MSG_REC_PLAY_BACK
            <<MSG_REC_PLAY_NEXT
            <<MSG_RECORD_ONOFF;
    sysScpiAddMap(getName(), cmdList);

    cmdList.clear();
    cmdList<<MSG_STORAGE_PRNTSCR;
    sysScpiAddMap(serv_name_storage, cmdList);

    sysScpiExecuteEn(false);
#endif

}


