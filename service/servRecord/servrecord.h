#ifndef ServRecord_H
#define ServRecord_H
#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"

#include "../../include/dsocfg.h"
#include "../../include/dsodbg.h"

//! record config
#define record_tick_timer_id    1
#define play_tick_timer_id      2

#define record_tick_tmo         100
#define play_tick_tmo           100

#define def_rec_interval        time_ns(10)
#define min_rec_interval        time_ns(10)
#define max_rec_interval        time_s(10)

#define def_play_interval       time_ns(10)
#define min_play_interval       time_ns(10)
#define max_play_interval       time_s(10)
#define interval_fmt            (DsoViewFmt)(fmt_float | fmt_width_3 | fmt_no_trim_0)

class servRecord : public serviceExecutor,
                   public ISerial
{
    Q_OBJECT

    DECLARE_CMD()
public:
    enum ServCmd
    {
        cmd_none = 0,
        MSG_GET_PROC = 1,
        MSG_GET_RECORDED,

        cmd_reced_count,
        cmd_inner_state,
        cmd_hori_changed,
        cmd_time_stamp,
        cmd_play_current,
    };

    enum RecordState
    {
        Record_disable,
        Record_empty,
        Record_ing,
        Record_end,
        Play_ing
    };

    servRecord(QString name, ServiceId eId = E_SERVICE_ID_RECORD);
    ~servRecord();

public:
    virtual void registerSpy();
    virtual bool keyEat(int key, int count, bool bRelease );

public:
    int  serialOut( CStream &stream, serialVersion &ver );
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();

protected:
    void on_init();

    void onTimeout(int);
    void on_record_timeout( int id );
    void on_play_timeout( int id );

public:
    DsoErr         setStatus(bool b);
    bool           getStatus();

    DsoErr         setStarting(bool b);
    bool           getStarting();

    int            getRecedFrames();

    DsoErr         setPlaying(bool b);
    bool           getPlaying();

    DsoErr         setPlayback( bool bFast );
    bool           getPlayback();

    DsoErr         setPlayForward( bool bFast );
    bool           getPlayForward();

    DsoErr         setPlayCurrent(int b);
    int            getPlayCurrent();
    DsoErr         playCurrent();

    DsoErr         setFrameStart(bool b);
    bool           getFrameStart();

    DsoErr         setOption(bool b);
    bool           getOption();

    DsoErr         setRecordInterval(qint64);
    qint64         getRecordInterval();

    DsoErr         setRecordLimit(int);
    int            getRecordLimit();
    int            getMaxFrames();

    DsoErr         setRecordMax(int);

    DsoErr         setBeeper(bool b);
    bool           getBeeper();

    DsoErr         setPlayDir(bool );
    bool           getPlayDir();

    DsoErr         setPlayRepeat(bool);
    bool           getPlayRepeat();

    DsoErr         setPlayInterval(qint64);
    qint64         getPlayInterval();

    DsoErr         setPlayStart(int);
    int            getPlayStart();

    DsoErr         setPlayEnd(int);
    int            getPlayEnd();

    qlonglong      getTimeStamp();

    //! export
    DsoErr         setFromFrame(int);
    int            getFromFrame();

    DsoErr         setToFrame(int);
    int            getToFrame();

    int            save(int);

protected:
    void on_hori_changed();
    void on_hori_run( int controlAction );

    bool checkRecEnable();

protected:
    qlonglong accInterval( bool bAcc, qlonglong interval );

    //! fsm status
    void changeState( RecordState stat );
    void setState( RecordState stat );
    RecordState getState();

    void disable_xxx( RecordState stat );
    void empty_xxx( RecordState stat );
    void recording_xxx( RecordState stat );
    void recorded_xxx( RecordState stat );
    void playing_xxx( RecordState stat );

    void in_disable();
    void in_empty();
    void in_recording();
    void in_recorded();
    void in_playing();

private:
    bool    bStatus;
    bool    bRecording;
    bool    bPlaying;
    bool    bPlayRepeat;
    bool    bPlayDec;

    bool    bFrameStart;
    bool    bOption;
    bool    bBeeper;


    //! record opt
    qint64  nRecordInterval;
    int     nRecordFrames;  //! recorded frame count
    int     nRecordMax;     //! recored size

    //! play opt
    qint64  nPlayInterval;
    int     nPlayCurrent;
    int     nStartFrame;
    int     nEndFrame;

    int     nFromFrame;
    int     nToFrame;

protected:
    RecordState m_State;
    QStringList mDependsServices;

    int mEnIdPre, mEnIdPost;
    bool mbSaveed;

    bool mbBackFast, mbForwardFast;
    bool mbInSession;
};

#endif // servRecord_H

