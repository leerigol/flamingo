#include "servrecord.h"

/*! \var 定义消息映射表
* 定义消息映射表
*/
IMPLEMENT_CMD( serviceExecutor, servRecord )
start_of_entry()

on_set_int_bool( MSG_RECORD_ONOFF,                &servRecord::setStatus),
on_get_bool    ( MSG_RECORD_ONOFF,                &servRecord::getStatus),
                                                  
on_set_int_bool( MSG_RECORD_START,                &servRecord::setStarting),
on_get_bool    ( MSG_RECORD_START,                &servRecord::getStarting),
                                                  
on_get_int( servRecord::cmd_reced_count,          &servRecord::getRecedFrames ),
                                                  
on_set_int_bool( MSG_RECORD_PLAY,                 &servRecord::setPlaying),
on_get_bool    ( MSG_RECORD_PLAY,                 &servRecord::getPlaying),
                                                  
on_set_int_int ( MSG_RECORD_CURRENT,              &servRecord::setPlayCurrent),
on_get_int     ( MSG_RECORD_CURRENT,              &servRecord::getPlayCurrent),
                                                  
on_set_int_bool( MSG_RECORD_FRAMESTART,           &servRecord::setFrameStart),
on_get_bool    ( MSG_RECORD_FRAMESTART,           &servRecord::getFrameStart),
                                                  
on_set_int_bool( MSG_RECORD_OPTION,               &servRecord::setOption),
on_get_bool    ( MSG_RECORD_OPTION,               &servRecord::getOption),
                                                  
on_set_int_ll  ( MSG_RECORD_INTERVAL,             &servRecord::setRecordInterval),
on_get_ll      ( MSG_RECORD_INTERVAL,             &servRecord::getRecordInterval),
                                                  
on_set_int_int(  MSG_RECORD_FRAMES,               &servRecord::setRecordLimit),
on_get_int    (  MSG_RECORD_FRAMES,               &servRecord::getRecordLimit),
                                                  
on_get_int    (  MSG_RECORD_MAXFRAMES,            &servRecord::getMaxFrames),
                                                  
on_set_int_int(  MSG_RECORD_SET2MAX,              &servRecord::setRecordMax),
                                                  
on_set_int_bool( MSG_RECORD_BEEPER,               &servRecord::setBeeper),
on_get_bool    ( MSG_RECORD_BEEPER,               &servRecord::getBeeper),
                                                  
on_set_int_bool( MSG_RECORD_PLAYMODE,             &servRecord::setPlayRepeat),
on_get_bool    ( MSG_RECORD_PLAYMODE,             &servRecord::getPlayRepeat),
                                                  
on_set_int_bool( MSG_RECORD_PLAYDIR,              &servRecord::setPlayDir),
on_get_bool    ( MSG_RECORD_PLAYDIR,              &servRecord::getPlayDir),
                                                  
on_set_int_ll(  MSG_RECORD_PLAYINTERVAL,          &servRecord::setPlayInterval),
on_get_ll    (  MSG_RECORD_PLAYINTERVAL,          &servRecord::getPlayInterval),
                                                  
on_set_int_int(  MSG_RECORD_STARTFRAME,           &servRecord::setPlayStart),
on_get_int    (  MSG_RECORD_STARTFRAME,           &servRecord::getPlayStart),
                                                  
on_set_int_int(  MSG_RECORD_ENDFRAME,             &servRecord::setPlayEnd),
on_get_int    (  MSG_RECORD_ENDFRAME,             &servRecord::getPlayEnd),
                                                  
on_set_int_int(  MSG_RECORD_FROMFRAME,            &servRecord::setFromFrame),
on_get_int    (  MSG_RECORD_FROMFRAME,            &servRecord::getFromFrame),
                                                  
on_set_int_int(  MSG_RECORD_TOFRAME,              &servRecord::setToFrame),
on_get_int    (  MSG_RECORD_TOFRAME,              &servRecord::getToFrame),
                                                  
on_set_int_int(  MSG_RECORD_SAVE2FILE,            &servRecord::save),
                                                  
on_set_int_bool( MSG_REC_PLAY_BACK,               &servRecord::setPlayback ),
on_get_bool( MSG_REC_PLAY_BACK,                   &servRecord::getPlayback ),
                                                  
on_set_int_bool( MSG_REC_PLAY_NEXT,               &servRecord::setPlayForward ),
on_get_bool( MSG_REC_PLAY_NEXT,                   &servRecord::getPlayForward ),

on_set_int_void ( servRecord::cmd_play_current,   &servRecord::playCurrent),
on_get_ll(        servRecord::cmd_time_stamp,     &servRecord::getTimeStamp ),

//! sys msg                                       
on_set_void_void(CMD_SERVICE_RST,                 &servRecord::rst ),
                                                  
on_set_int_void (CMD_SERVICE_STARTUP,             &servRecord::startup ),
//on_set_int_int  (CMD_SERVICE_ACTIVE,            &servRecord::setActive ),
                                                  
on_set_void_int(CMD_SERVICE_TIMEOUT,              &servRecord::onTimeout ),
                                                  
on_set_void_void( CMD_SERVICE_INIT,               &servRecord::on_init ),

//spy msg
on_set_void_int( MSG_HORIZONTAL_RUN,              &servRecord::on_hori_run ),
on_set_void_void ( servRecord::cmd_hori_changed,  &servRecord::on_hori_changed ),
on_set_void_int( servRecord::cmd_inner_state,     &servRecord::setState ),
on_get_int( servRecord::cmd_inner_state,          &servRecord::getState ),

end_of_entry()

