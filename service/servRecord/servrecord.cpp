#include "../service_msg.h"

#include "servrecord.h"

#include "../servstorage/servstorage.h"
#include "../../arith/norm125.h"
#include "../servhori/servhori.h"

servRecord::servRecord(QString name, ServiceId eId):serviceExecutor(name, eId)
{
    serviceExecutor::baseInit();

    ISerial::mVersion = 1;

    //! stack ids
    mEnIdPre = getId();
    mEnIdPost = getId() + E_SERVICE_ID_MAX * 1;

    //! depends
    mDependsServices.append( serv_name_hori );
    //mDependsServices.append( serv_name_menutester );

    mDependsServices.append( serv_name_ch1 );
    mDependsServices.append( serv_name_ch2 );
    mDependsServices.append( serv_name_ch3 );
    mDependsServices.append( serv_name_ch4 );

    mDependsServices.append( serv_name_display );
    mDependsServices.append( serv_name_autoset );

    mDependsServices.append( serv_name_trigger );
    mDependsServices.append( serv_name_trigger_edge );
    mDependsServices.append( serv_name_trigger_pulse );
    mDependsServices.append( serv_name_trigger_slope );
    mDependsServices.append( serv_name_trigger_video );

    mDependsServices.append( serv_name_trigger_pattern );
    mDependsServices.append( serv_name_trigger_duration );
    mDependsServices.append( serv_name_trigger_timeout );
    mDependsServices.append( serv_name_trigger_runt );

    mDependsServices.append( serv_name_trigger_pattern );
    mDependsServices.append( serv_name_trigger_duration );
    mDependsServices.append( serv_name_trigger_timeout );
    mDependsServices.append( serv_name_trigger_runt );

    mDependsServices.append( serv_name_trigger_over );
    mDependsServices.append( serv_name_trigger_window );
    mDependsServices.append( serv_name_trigger_delay );
    mDependsServices.append( serv_name_trigger_setup );

    mDependsServices.append( serv_name_trigger_nth );
    mDependsServices.append( serv_name_trigger_rs232 );
    mDependsServices.append( serv_name_trigger_i2c );
    mDependsServices.append( serv_name_trigger_spi );

    mDependsServices.append( serv_name_trigger_lin );
    mDependsServices.append( serv_name_trigger_1553b );
    mDependsServices.append( serv_name_trigger_can );
//    mDependsServices.append( serv_name_trigger_ab );
}

servRecord::~servRecord()
{

}

void servRecord::registerSpy()
{
    spyOn( serv_name_mask, MSG_MASK_ENABLE );

    spyOn( serv_name_hori, MSG_HOR_TIME_MODE,          servRecord::cmd_hori_changed );
    spyOn( serv_name_hori, MSG_HORI_MAIN_SCALE,        servRecord::cmd_hori_changed );
    spyOn( serv_name_hori, servHori::cmd_hori_changed, servRecord::cmd_hori_changed );

    spyOn( serv_name_hori, MSG_HORIZONTAL_RUN );
}

/*![dba] 录制或播放时禁用无关按键,为了解决直接禁用服务时的bug

   bug2015 by hxh. enable Menuoff
*/
bool servRecord::keyEat(int key,
                        int  /*count*/,
                        bool /*bRelease*/)
{
    if(!bStatus)
    {
        return false;
    }

    if(m_State == Record_ing)
    {
        if( (key != R_Pkey_F1)&&
            (key != R_Pkey_F2) &&
            (key != R_Pkey_PageOff)&&
            (key != R_Pkey_TRIG_FORCE)&&
            (key != R_Pkey_QUICK)
                )
        {
            servGui::showErr( ERR_ACTION_DISABLED );
            return true;
        }
    }

    if(m_State == Play_ing)
    {
        if( (key != R_Pkey_F1)        &&
            (key != R_Pkey_F3)        &&
            (key != R_Pkey_PLAY_PRE)  &&
            (key != R_Pkey_PLAY_NEXT) &&
            (key != R_Pkey_PLAY_STOP) &&
            (key != R_Pkey_PageOff)   &&
            (key != R_Pkey_QUICK)
                )
        {
            servGui::showErr( ERR_ACTION_DISABLED );
            return true;
        }
    }

    return false;
}

DsoErr servRecord::setStatus(bool b)
{
    bStatus = b;

    if ( !bStatus )
    {
        if(m_State == Record_ing)
        {
            ssync(MSG_RECORD_START, false);
        }
        else if(m_State == Play_ing)
        {
            ssync(MSG_RECORD_PLAY, false);
        }

         changeState( Record_disable );
    }
    else
    {
        changeState( Record_empty );
    }

    //! [dba+, 2018-04-02, bug:1862,2376 ]
    return post(serv_name_hori,  servHori::cmd_apply_engine, 1);
}

bool servRecord::getStatus()
{
    return bStatus;
}

DsoErr servRecord::setStarting(bool b)
{
    bRecording = b;
LOG_DBG()<<b;
    if(b)
    {
        startTimer( record_tick_tmo,record_tick_timer_id,timer_repeat);

        nRecordFrames = 0;
        setuiChange( servRecord::cmd_reced_count );

        //! start recording
        postEngine( ENGINE_RECORD_FRAMES, nRecordMax );
        postEngine( ENGINE_RECORD_INTERVAL, nRecordInterval );
        postEngine( ENGINE_RECORD_ACTION, Rec_start );

        changeState( Record_ing );
    }
    else
    {
        stopTimer( record_tick_timer_id );

        //! reced frames
        int preRecorded = nRecordFrames;

        //! stop recording
        postEngine( ENGINE_RECORD_ACTION, Rec_stop );

        //! update the frames
        queryEngine( qENGINE_RECORD_PLAY_FRAME_ID, nRecordFrames );

        qDebug()<<__FILE__<<__FUNCTION__<<nRecordFrames;
        if ( nRecordFrames < preRecorded )
        { nRecordFrames = preRecorded; }

        //! record completed
        if ( nRecordFrames > 0 )
        {
            nStartFrame = 1;
            nEndFrame = nRecordFrames;

            nFromFrame = 1;
            nToFrame = nRecordFrames;

            nPlayCurrent = nRecordFrames;

            changeState( Record_end );
            /*! [dba, 2018-04-12], bug:2369 */
            postEngine( ENGINE_RECORD_DONE, true);

            setuiChange( MSG_RECORD_CURRENT );
        }
        else
        {
            changeState( Record_empty );
        }
    }

    return ERR_NONE;
}

bool servRecord::getStarting()
{   
    return bRecording;
}

int servRecord::getRecedFrames()
{ return nRecordFrames; }

DsoErr servRecord::setPlaying(bool b)
{

   bPlaying = b;

   changeState( b ? Play_ing : Record_end );

   if(b)
   {
       //! decrease
       if ( bPlayDec )
       {
           if(nPlayCurrent == nStartFrame )
           {
               nPlayCurrent = nEndFrame;
           }
       }
       //! increase
       else
       {
           if(nPlayCurrent == nEndFrame )
           {
               nPlayCurrent = nStartFrame;
           }
       }

       //! engine cfg
       postEngine( ENGINE_RECORD_PLAY_RANGE, nStartFrame, nEndFrame );
       postEngine( ENGINE_RECORD_PLAY_FRAME_ID, nPlayCurrent );
       postEngine( ENGINE_RECORD_PLAY_INTERVAL, nPlayInterval );

       postEngine( ENGINE_RECORD_PLAY_DIRECTION, bPlayDec ? Play_Dec : Play_Inc );
       postEngine( ENGINE_RECORD_PLAY_LOOP, bPlayRepeat );

       postEngine( ENGINE_RECORD_PLAY_ACTION, Play_start );

       startTimer( play_tick_tmo,play_tick_timer_id,timer_repeat);
   }
   else
   {
       postEngine( ENGINE_RECORD_PLAY_ACTION, Play_stop );
       queryEngine( qENGINE_RECORD_PLAY_FRAME_ID, nPlayCurrent );

       nPlayCurrent = qBound(nStartFrame, nPlayCurrent, nEndFrame);

       mbInSession = false;

       stopTimer( play_tick_timer_id );
   }

   return ERR_NONE;
}

bool servRecord::getPlaying()
{
   return bPlaying;
}

DsoErr servRecord::setPlayback( bool bFast )
{
    //! single play
    bPlayRepeat = false;

    //! start play
    if ( m_State != Play_ing )
    {
        bFast = false;

        bPlayDec = true;
        async( MSG_RECORD_PLAY, true );
    }
    //! in playing
    else
    {
        //! increase
        if ( bPlayDec != true )
        {
            bFast = false;
            bPlayDec = true;

            mbInSession = true;
            async( MSG_RECORD_PLAY, false );
            async( MSG_RECORD_PLAY, true );
        }
        //! decrease
        else
        {
            qlonglong nowInt;
            nowInt = accInterval( bFast, nPlayInterval );
            bFast = nowInt < nPlayInterval;

            postEngine( ENGINE_RECORD_PLAY_INTERVAL, nowInt );
        }
    }

    mbBackFast = bFast;

    if ( mbBackFast )
    { servGui::showImage( ":/pictures/record/back_fast_normal.bmp"); }
    else
    { servGui::showImage( ":/pictures/record/back_slow_normal.bmp"); }

    return ERR_NONE;
}
bool servRecord::getPlayback()
{ return mbBackFast; }

DsoErr servRecord::setPlayForward( bool bFast )
{
    //! single play
    bPlayRepeat = false;

    //! start play
    if ( m_State != Play_ing )
    {
        bFast = false;

        bPlayDec = false;
        async( MSG_RECORD_PLAY, true );
    }
    //! in playing
    else
    {
        //! decreast
        if ( bPlayDec == true )
        {
            bFast = false;
            bPlayDec = false;

            mbInSession = true;
            async( MSG_RECORD_PLAY, false );
            async( MSG_RECORD_PLAY, true );
        }
        //! increase
        else
        {
            qlonglong nowInt;
            nowInt = accInterval( bFast, nPlayInterval );
            bFast = nowInt < nPlayInterval;

            postEngine( ENGINE_RECORD_PLAY_INTERVAL, nowInt );
        }
    }

    mbForwardFast = bFast;

    if ( mbForwardFast )
    { servGui::showImage( ":/pictures/record/forw_fast_normal.bmp"); }
    else
    { servGui::showImage( ":/pictures/record/forw_slow_normal.bmp"); }

    return ERR_NONE;
}
bool servRecord::getPlayForward()
{ return mbForwardFast; }

DsoErr servRecord::setPlayCurrent(int b)
{
   nPlayCurrent = b;
   defer(cmd_play_current);
   return ERR_NONE;
}

int servRecord::getPlayCurrent()
{
    setuiRange( nStartFrame, nEndFrame, nStartFrame );
    setuiUnit(Unit_none);
    setuiFmt( fmt_int );
    setuiBase(1, E_0);
    return nPlayCurrent;
}

DsoErr servRecord::playCurrent()
{
    //! sigle frame play
    return postEngine( ENGINE_RECORD_PLAY_STEP, nPlayCurrent);
}

DsoErr servRecord::setFrameStart(bool b)
{
    bFrameStart = b;

    if ( b )
    { async( MSG_RECORD_CURRENT, nStartFrame ); }
    else
    { async( MSG_RECORD_CURRENT, nEndFrame ); }

    return ERR_NONE;
}

bool servRecord::getFrameStart()
{
    return bFrameStart;
}

DsoErr servRecord::setOption(bool b)
{
    bOption = b;
    return ERR_NONE;
}

bool servRecord::getOption()
{
    return bOption;
}

DsoErr servRecord::setRecordInterval(qint64 i)
{
    nRecordInterval = i;

    return ERR_NONE;
}

qint64 servRecord::getRecordInterval()
{
    qlonglong roofU, roofL;
    qlonglong stepI, stepD;

    roofU = ll_roof10( nRecordInterval );
    roofL = ll_roof10L( nRecordInterval );;

    stepI = roofU/1000;
    stepD = roofL/1000;

    if ( stepI < 1 ){ stepI = 1; }
    if ( stepD < 1 ){ stepD = 1; }

    setuiIStep( stepI );
    setuiDStep( stepD );

    setuiRange( min_rec_interval,
                max_rec_interval,
                def_rec_interval );
    setuiFmt( interval_fmt );
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);

    return nRecordInterval;
}

//! user set max frame
DsoErr servRecord::setRecordLimit(int l)
{
    nRecordMax = l;
    return ERR_NONE;
}
int servRecord::getRecordLimit()
{
    //! capacity
    int recCap = getMaxFrames();

    setuiRange( 1, recCap, 1 );
    setuiBase(1, E_0);
    setuiFmt( fmt_int );

    return nRecordMax > recCap ? recCap : nRecordMax;
}

DsoErr servRecord::setRecordMax(int)
{
    int cap = getMaxFrames();

    async( MSG_RECORD_FRAMES, cap );
    return ERR_NONE;
}

int servRecord::getMaxFrames()
{
    setuiFmt( fmt_int );

    int maxFrame;
    queryEngine( qENGINE_RECORD_MAX_FRAMES, maxFrame );
    //qDebug()<<__FUNCTION__<<__LINE__<<maxFrame;
    return maxFrame;
}

DsoErr servRecord::setBeeper(bool b)
{
    bBeeper = b;
    return ERR_NONE;
}

bool servRecord::getBeeper()
{
    return bBeeper;
}

DsoErr servRecord::setPlayDir(bool b)
{
    bPlayDec = b;
    return ERR_NONE;
}

bool servRecord::getPlayDir()
{
    return bPlayDec;
}

DsoErr servRecord::setPlayRepeat(bool b)
{
    bPlayRepeat = b;
    return ERR_NONE;
}

bool servRecord::getPlayRepeat()
{
    return bPlayRepeat;
}

DsoErr servRecord::setPlayInterval(qint64 i)
{
    nPlayInterval = i;
    return ERR_NONE;
}

qint64 servRecord::getPlayInterval()
{
    qlonglong roofU, roofL;
    qlonglong stepI, stepD;

    roofU = ll_roof10( nPlayInterval );
    roofL = ll_roof10L( nPlayInterval );;

    stepI = roofU/1000;
    stepD = roofL/1000;

    if ( stepI < 1 ){ stepI = 1; }
    if ( stepD < 1 ){ stepD = 1; }

    setuiIStep( stepI );
    setuiDStep( stepD );

    setuiRange( min_play_interval,
                max_play_interval,
                def_play_interval );

    setuiUnit(Unit_s);
    setuiFmt( interval_fmt );
    setuiBase(1, E_N12);

    return nPlayInterval;
}

DsoErr servRecord::setPlayStart(int f)
{
    nStartFrame = f;

    //! check cur
    if ( nPlayCurrent < nStartFrame )
    { async(MSG_RECORD_CURRENT, nStartFrame); }

    return ERR_NONE;
}

int servRecord::getPlayStart()
{
    setuiRange( 1, nEndFrame, 1 );
    setuiBase(1, E_0);
    setuiFmt( fmt_int );

    return nStartFrame;
}

DsoErr servRecord::setPlayEnd(int f)
{
    nEndFrame = f;

    //! check cur
    if ( nPlayCurrent > nEndFrame )
    { async(MSG_RECORD_CURRENT, nEndFrame); }

    return ERR_NONE;
}

int servRecord::getPlayEnd()
{
    setuiRange( nStartFrame, nRecordFrames, nRecordFrames );
    setuiBase(1, E_0);
    setuiFmt( fmt_int );

    return nEndFrame;
}

qlonglong servRecord::getTimeStamp()
{
    quint64 tag = 0;
    queryEngine( qENGINE_RECORD_PLAY_FRAME_TAG, nPlayCurrent, tag);

    //! 100ps/tag
    return  tag * time_ps(100);

}

DsoErr servRecord::setFromFrame(int f)
{
    nFromFrame = f;
    return ERR_NONE;
}

int servRecord::getFromFrame()
{
    setuiRange( 1, nToFrame, 1 );
    setuiFmt( fmt_int );

    return nFromFrame;
}

DsoErr servRecord::setToFrame(int f)
{
    nToFrame = f;
    return ERR_NONE;
}

int servRecord::getToFrame()
{
    setuiRange( nFromFrame, nRecordFrames, nRecordFrames );
    setuiFmt( fmt_int );

    return nToFrame;
}

int servRecord::save(int)
{
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_SAVE,
                      servStorage::FUNC_SAVE_REC,
                      1,
                      getId());
    startIntent( serv_name_storage, intent );
    return ERR_NONE;
}




