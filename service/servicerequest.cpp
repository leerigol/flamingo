#include "servicerequest.h"
#include "service.h"
serviceRequest::serviceRequest()
{
    mServId = 0;
    mAckMsg = 0;

    mAcked = false;
}

void serviceRequest::setClient( int clientId, int ackMsg )
{
    mServId = clientId;
    mAckMsg = ackMsg;
}

bool serviceRequest::waitAcked( int tmoms, int tickms )
{
    Q_ASSERT( tickms > 0 );

    //! timeout valid
    if ( tmoms >= 0 )
    {
        while( tmoms > 0 )
        {
            if ( mAcked )
            { return true; }
            else
            { }

            QThread::msleep( tickms );

            tmoms -= tickms;
        }

        return mAcked;
    }
    else
    {
        while( !mAcked )
        {
            QThread::msleep( tickms );
        }

        return true;
    }
}

DsoErr serviceRequest::request( const QString &name, int msg )
{
    mAcked = false;
    return serviceExecutor::post( name, msg, (pointer)this, NULL );
}
DsoErr serviceRequest::ackRequest()
{
    mAcked = true;
    return serviceExecutor::post( serviceExecutor::findItem(mServId)->servName,
                                  mAckMsg,
                                  (pointer)this,
                                  NULL );
}

//! phase request
phaseRequest::phaseRequest()
{
    for ( int i = 0; i < array_count(mRef); i++ )
    { mRef[i]= 0; }
}

void phaseRequest::setDeltas( float deltas[3] )
{
    for ( int i = 0; i < array_count(mDeltas); i++ )
    {
        mDeltas[i].push( deltas[i] - mRef[i] );
    }
}

void phaseRequest::setDeltas( float delta10,
                float delta20,
                float delta30 )
{
    mDeltas[0].push( delta10 - mRef[0] );
    mDeltas[1].push( delta20 - mRef[1] );
    mDeltas[2].push( delta30 - mRef[2] );

    //! averaged
    if ( mDeltas[0].getCount() == mDeltas[0].getAvgCount() )
    {
//        qDebug()<<delta10<<delta20<<delta30;
//        qDebug()<<mDeltas[0].getAverge()<<mDeltas[0].getMax()<<mDeltas[0].getMin();
//        qDebug()<<mDeltas[1].getAverge()<<mDeltas[1].getMax()<<mDeltas[1].getMin();
//        qDebug()<<mDeltas[2].getAverge()<<mDeltas[2].getMax()<<mDeltas[2].getMin();
    }
}

void phaseRequest::setRef( float refs[3] )
{
    for ( int i = 0; i < array_count(mRef); i++ )
    {
        mRef[i] = refs[i];
    }
}
void phaseRequest::setRef( float ref1, float ref2, float ref3 )
{
    mRef[0] = ref1;
    mRef[1] = ref2;
    mRef[2] = ref3;
}

void phaseRequest::setAvgCount( int avg )
{
    for ( int i = 0; i < array_count(mDeltas); i++ )
    {
        mDeltas[i].setAvgCount( avg );
    }
}

DsoErr phaseRequest::request( const QString &name, int msg )
{
    //! rst average count
    for ( int i = 0; i < array_count(mDeltas); i++ )
    {
        mDeltas[i].rst();
    }

    return serviceRequest::request( name, msg );
}

//! full
DsoErr phaseRequest::ackRequest()
{
    if ( mDeltas[0].getCount() != mDeltas[0].getAvgCount() )
    { return ERR_NONE; }

//    qDebug()<<mDeltas[0].getAverge()<<mDeltas[1].getAverge()<<mDeltas[2].getAverge();

    return serviceRequest::ackRequest();

}
