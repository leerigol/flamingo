
#include "../../service/service_name.h"
#include "../../include/dsodbg.h"

#include "../../baseclass/dsovert.h"

#include "servvert.h"

IMPLEMENT_CMD( serviceExecutor, servVert )
start_of_entry()

//! system msg
on_set_int_int( CMD_SERVICE_DEACTIVE, &servVert::onDeActive ),

end_of_entry()

servVert::servVert( QString name, ServiceId eId )
                   : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();
}

DsoErr servVert::onDeActive(int para)
{
    //! active chan now
    Chan actChan = dsoVert::getActiveChan();


    //! no chan
    if ( actChan == chan_none )
    { return serviceExecutor::setdeActive( para ); }

    //! to service id
    dsoVert *pCH = dsoVert::getCH( actChan );
    Q_ASSERT( NULL != pCH );

    //! active next
    int servId = pCH->getServId();
    if ( servId == -1 )
    { return ERR_INVALID_CONFIG; }

    return serviceExecutor::deactiveActive( servId );
}



