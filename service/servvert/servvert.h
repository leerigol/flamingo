#ifndef _SERVVERT_H
#define _SERVVERT_H

#include "../../include/dsotype.h"
#include "../service.h"
#include "../service_msg.h"

class servVert: public serviceExecutor
{
    Q_OBJECT
protected:
    DECLARE_CMD()
public:
    enum userCmd
    {
        user_cmd_none = 0,

        user_cmd_on_off,
    };

public:
    servVert( QString name, ServiceId eId = E_SERVICE_ID_NONE );

public:
    DsoErr onDeActive(int para);

};

#endif // SERVVERT_H
