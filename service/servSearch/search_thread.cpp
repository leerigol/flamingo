#include "search_thread.h"
#include "searchdso.h"

searchThread::searchThread(QObject *parent): QThread()
{
    Q_ASSERT( NULL != parent );
    m_pSearch = dynamic_cast<servSearch*>(parent);
    Q_ASSERT( NULL != m_pSearch );
}

void searchThread::run()
{
    m_pSearch->updateData();
/*
    if(m_pSearch->getSearchOnOff())
    {
        m_pSearch->startTimer(SEARCH_TIMER_TMO, SEARCH_TIMER_ID);
    }
*/
}

