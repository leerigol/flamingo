#include "servsearch.h"
#include "../servstorage/servstorage.h"
#include <unistd.h>

static DsoErr searchSave(QString fileName,
                         QString type,
                         QList<qlonglong> &list);

#if _SEARCH_DEBUG_ON
static DsoErr searchDebugSave(QString fileName,
                              QList<QString> &list);
#endif

DsoErr servSearch::save()
{
    CIntent intent;
    intent.setIntent( MSG_STORAGE_SUB_SAVE,
                      servStorage::FUNC_SAVE_SEARCH,
                      1,
                      getId());
    startIntent( serv_name_storage, intent );
    return    ERR_NONE;
}

DsoErr servSearch::saveTable(QString fileName)
{
    qDebug()<<__FILE__<<__LINE__<<fileName;
    DsoErr       err    = ERR_NONE;
    searchData  *pData  = NULL;
    int          nType  = SEARCH_TYPE_EDGE;
    QString      sType  = "edge";

    //!get data ptr
    pointer dataPtr = NULL;
    serviceExecutor::query( serv_name_search, CMD_SEARCH_GET_DATA, dataPtr);
    Q_ASSERT(dataPtr != NULL );

    pData = static_cast<searchData*>(dataPtr);
    Q_ASSERT(pData != NULL );

    //!get search type
    serviceExecutor::query( serv_name_search, MSG_SEARCH_TYPE, nType);


    //!get data
    QList<qlonglong> list;
    pData->m_data.getOriginTimeValue(list);

    //!get type str
    QMap<enumSearchType, QString> typeMap;
    typeMap[SEARCH_TYPE_EDGE]  = "edge";
    typeMap[SEARCH_TYPE_PULSE] = "pulse";
    typeMap[SEARCH_TYPE_RUNT]  = "runt";
    typeMap[SEARCH_TYPE_SLOPE] = "slope";
    typeMap[SEARCH_TYPE_RS232] = "rs232";
    sType = typeMap.value((enumSearchType)nType);

    err = searchSave(fileName, sType, list);
    list.clear();


#if _SEARCH_DEBUG_ON
    QList<QString> debugList;
    pData->m_data.getDebugData(debugList);
    err = searchDebugSave(fileName, debugList);
    debugList.clear();
    //servGui::showInfo(sysGetString( ERR_FILE_SAVE_OK, "save ok") );
#endif

    return err;
}

static DsoErr searchSave(QString fileName, QString type, QList<qlonglong> &list)
{
    QFile file(fileName);
    if( !file.open(QIODevice::WriteOnly | QIODevice::Text) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    QTextStream out(&file);

    out<<QString("number, type, time (s), ,\n"); //! Header

    for(int i = 0; i < list.count(); i++)
    {
        out<<QString("%1,%2,%3,,\n").arg(i).arg(type).arg(list.at(i)*hori_time_base);
    }

    file.flush();
    fsync(file.handle());
    file.close();
    return ERR_NONE;
}

#if _SEARCH_DEBUG_ON
static DsoErr searchDebugSave(QString fileName, QList<QString> &list)
{
    fileName = fileName.replace(QRegExp("\\.csv"), "_debug.csv");

    QFile file(fileName);
    if( !file.open(QIODevice::WriteOnly | QIODevice::Text) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    QTextStream out(&file);

    out<<QString("column, count, data[0], ,\n"); //! Header

    for(int i = 0; i < list.count(); i++)
    {
        out<<list.at(i)<<",\n";
    }
    file.flush();
    fsync(file.handle());
    file.close();
    return ERR_NONE;
}
#endif
