#include "servsearch.h"

/*--------------------------------------------------------------------------------------------------------------------------------------*/
//!time range 表
const _search_time_range servSearch::stp_time_range[]={
    {SEARCH_TYPE_PULSE, time_ps(800),  time_s(10), &servSearch::cfg_pulse_getTimeRange, &servSearch::cfg_pulse_setTimeRange},
    {SEARCH_TYPE_SLOPE, time_ps(800),  time_s(10), &servSearch::cfg_slope_getTimeRange, &servSearch::cfg_slope_setTimeRange},
    {SEARCH_TYPE_RUNT,  time_ps(800),  time_s(10), &servSearch::cfg_runt_getTimeRange,  &servSearch::cfg_runt_setTimeRange},
    {-1, time_ps(0),  time_s(0), NULL, NULL},
};

_search_time_range servSearch::timeRangeIndex(enumSearchType type)
{
    for(int i = 0; i < array_count(stp_time_range); i++)
    {
        if(stp_time_range[i].serachType == type)
        {
            return stp_time_range[i];
        }
    }
    return stp_time_range[ array_count(stp_time_range) ];
}

DsoErr servSearch::checkTimeRange(enumSearchType  type,
                                  TrigTimeRule    rule,
                                  const EMoreThan w0)
{
    _search_time_range timeRange = timeRangeIndex(type);

    qint64         tMin      = timeRange.tMin;
    qint64         tMax      = timeRange.tMax;
    _fGetTimeRange pGteRange = timeRange.fGetTimeRange;
    _fSetTimeRange pSteRange = timeRange.fSetTimeRange;

    Q_ASSERT(pGteRange != NULL);
    Q_ASSERT(pSteRange != NULL);

    EMoreThan w  = Trigger_When_None;
    qint64 tL = 0, tH = 0;
    //！获取规则检查需要的参数
    (this->*pGteRange)(tL,tH,w);

    //!进行规则检查
    trigrules::timeRangeRule(rule, tL, tH,
                             w, w0, tMin, tMax);
    //！检查后的参数重新配置
    return (this->*pSteRange)(tL,tH);
}

void servSearch::getUpperAttr(enumSearchType type)
{
    EMoreThan w  = Trigger_When_None;
    qint64    tL = 0, tH = 0;
    _fGetTimeRange pGteRange = NULL;

    _search_time_range timeRange = timeRangeIndex(type);
    pGteRange = timeRange.fGetTimeRange;
    Q_ASSERT(pGteRange != NULL);

    (this->*pGteRange)(tL,tH,w);
    SET_TRIG_TIME_ATTR(tH, timeRange.tMin, timeRange.tMax, time_us(1));
}

void servSearch::getLowerAttr(enumSearchType type)
{
    EMoreThan w  = Trigger_When_None;
    qint64 tL = 0, tH = 0;
    _fGetTimeRange pGteRange = NULL;

    _search_time_range timeRange = timeRangeIndex(type);
    pGteRange = timeRange.fGetTimeRange;
    Q_ASSERT(pGteRange != NULL);

    (this->*pGteRange)(tL,tH,w);
    SET_TRIG_TIME_ATTR(tL, timeRange.tMin, timeRange.tMax, time_us(1));
}

/*--------------------------------------------------------------------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------------------------------------------------------------------*/
void servSearch::onChOnOff()
{
    QList <int> onChList;
    QList <int> offChList;
    if(!bSearchOnOff)
    {return;}

    onChList.clear();
    offChList.clear();
    for(int ch = chan1; ch<=chan4; ch++ )
    {
        bool chEn = false;
        QString servName =  getServiceName(ch - chan1 + E_SERVICE_ID_CH1);
        serviceExecutor::query(servName, MSG_CHAN_ON_OFF, chEn);

        if(chEn)
        {
            onChList<<ch;
        }
        else
        {
            offChList<<ch;
        }
    }

    bool bEnable = (!onChList.isEmpty());
    mUiAttr.setEnable( MSG_SEARCH_EDGE_SOURCE,  bEnable);
    mUiAttr.setEnable( MSG_SEARCH_PULSE_SOURCE, bEnable);
    mUiAttr.setEnable( MSG_SEARCH_RUNT_SOURCE,  bEnable);
    mUiAttr.setEnable( MSG_SEARCH_SLOPE_SOURCE, bEnable);
    mUiAttr.setEnable( MSG_SEARCH_RS232_SOURCE, bEnable);
    if(bEnable)
    {
        int ch = chan_none;
        foreach (ch, onChList)
        {
            //qDebug()<<__FILE__<<__LINE__<<ch;
            mUiAttr.setEnable( MSG_SEARCH_EDGE_SOURCE,  ch, true);
            mUiAttr.setEnable( MSG_SEARCH_PULSE_SOURCE, ch, true);
            mUiAttr.setEnable( MSG_SEARCH_RUNT_SOURCE,  ch, true);
            mUiAttr.setEnable( MSG_SEARCH_SLOPE_SOURCE, ch, true);
            mUiAttr.setEnable( MSG_SEARCH_RS232_SOURCE, ch, true);
        }

        foreach (ch, offChList)
        {
            mUiAttr.setEnable( MSG_SEARCH_EDGE_SOURCE,  ch, false);
            mUiAttr.setEnable( MSG_SEARCH_PULSE_SOURCE, ch, false);
            mUiAttr.setEnable( MSG_SEARCH_RUNT_SOURCE,  ch, false);
            mUiAttr.setEnable( MSG_SEARCH_SLOPE_SOURCE, ch, false);
            mUiAttr.setEnable( MSG_SEARCH_RS232_SOURCE, ch, false);

            if(ch == getCurrSource())
            {
                ch = onChList.at(0);
                async( MSG_SEARCH_EDGE_SOURCE,  ch);
                async( MSG_SEARCH_PULSE_SOURCE, ch);
                async( MSG_SEARCH_RUNT_SOURCE,  ch);
                async( MSG_SEARCH_SLOPE_SOURCE, ch);
                async( MSG_SEARCH_RS232_SOURCE, ch);
            }
        }
    }
}
/*--------------------------------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------------------------------------*/
static int opt_search_Type[][2] = {
    {OPT_COMP,  SEARCH_TYPE_RS232   },
    {OPT_EMBD,  SEARCH_TYPE_IIC     },
    {OPT_EMBD,  SEARCH_TYPE_SPI     },
};

void servSearch::onLicenseOpt()
{
    bool visible = false;

    for(int i = 0; i < array_count(opt_search_Type); i++)
    {
        visible = sysCheckLicense((OptType)opt_search_Type[i][0]);

        mUiAttr.setVisible(MSG_SEARCH_TYPE, opt_search_Type[i][1], visible);

        if( (!visible) && (getSearchType() == opt_search_Type[i][1]) )
        {
            setSearchType(SEARCH_TYPE_EDGE);
        }
    }

    //mUiAttr.setEnable(MSG_SEARCH_TYPE, bSearchOnOff);
    setuiChange(MSG_SEARCH_TYPE);
}
/*--------------------------------------------------------------------------------------------------------------------------------------*/
