#ifndef SEARCHTHREAD_H
#define SEARCHTHREAD_H

#include <QtCore>
#include "servsearch.h"

class searchThread: public QThread
{
    Q_OBJECT
public:
    searchThread(QObject *parent);

protected:
    virtual void run();

private:
    servSearch *m_pSearch;
};

#endif // SEARCHTHREAD_H
