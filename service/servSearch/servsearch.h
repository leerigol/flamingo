#ifndef SERVSEARCH_H
#define SERVSEARCH_H

#include "searchdso.h"

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../../include/dsostd.h"
#include "../service_msg.h"
#include "../../baseclass/dsovert.h"
#include "../servtrig/servtrig.h"

using namespace search_dso;

class searchThread;

class servSearch : public serviceExecutor, public ISerial,
        servEdgeCfg, servPulseCfg,servRuntCfg,servSlopeCfg,
        servRs232Cfg, servI2cCfg, servSpiCfg
{
    Q_OBJECT

    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servSearch(QString name, ServiceId eId = E_SERVICE_ID_SEARCH);

public:
    int  serialOut( CStream &stream, serialVersion &ver);
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();
    void registerSpy();
    virtual DsoErr  apply();
    virtual DsoErr  setActive( int active=1 );
    virtual DsoErr  setdeActive( int para=0 );
    virtual bool    actionFilter(const struServAction &action);
    virtual bool    keyTranslate(int key, int &msg, int &controlAction);

public:
    DsoErr              setSearchOnOff(bool b);
    bool                getSearchOnOff();

    DsoErr              setMarkTabelOnOff(bool b);
    bool                getMarkTabelOnOff();

    DsoErr              setSearchType(int enType);
    int                 getSearchType();

    DsoErr              setSearchSRC(Chan enSrc);
    Chan                getSearchSRC();

    //!thre
    void                setSourceThre(Chan ch,
                                      qint64 threA,
                                      qint64 threB = 0,
                                      bool   a     = true,
                                      bool   b     = false);

    void                getSourceThre(Chan ch,
                                      int  ab,
                                      qint64 &thre,
                                      qint64 &step,
                                      qint64 &max,
                                      qint64 &min);

    int                 getChSlope(Chan ch);

    DsoErr              initLevelEngine();
    DsoErr              applyThreshold(Chan chan = chan_none);

    DsoErr              setSearchThre(int thre);
    int                 getSearchThre();

    DsoErr              setSearchThreA(int thre);
    int                 getSearchThreA();

    DsoErr              setSearchThreB(int thre);
    int                 getSearchThreB();

    DsoErr              setThreI2cScl(int thre);
    int                 getThreI2cScl();

    DsoErr              setThreI2cSda(int thre);
    int                 getThreI2cSda();

    DsoErr              setThreSpiScl(int thre);
    int                 getThreSpiScl();

    DsoErr              setThreSpiSda(int thre);
    int                 getThreSpiSda();

    DsoErr              setThreSpiCs(int thre);
    int                 getThreSpiCs();

    //! code
    bool                updateCodePtr();
    int                 getCodeCurBit(void);
    int                 setCodeCurBit(int bit);
    int                 getCodeValue(void);
    int                 setCodeValue(int value);
    int                 setCodeAll(int value);
    void                getCodeArg(CArgument& arg);

    //! copy Trig
    DsoErr              copyToTrig();
    DsoErr              copyFormTrig();

    /*!-----------------edge-----------------------------*/
    DsoErr              setEdgeSource(int ch);
    int                 getEdgeSource();

    DsoErr              setEdgeSlope(int slope);
    int                 getEdgeSlope();

    DsoErr              setEdgeThre(int thre);
    int                 getEdgeThre();

    /*!------------------Pulse---------------------------*/
    DsoErr              setPulseSource(int ch);
    int                 getPulseSource();

    DsoErr              setPulsePolarty(bool pol);
    bool                getPulsePolarty();

    DsoErr              setPulseWhen(int when);
    int                 getPulseWhen();

    DsoErr              setPulseLower(qint64 value);
    qint64              getPulseLower();

    DsoErr              setPulseUpper(qint64 value);
    qint64              getPulseUpper();

    DsoErr              setPulseThre(int Thre);
    int                 getPulseThre();

    void                getPulseTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w);
    DsoErr              setPulseTimeRange(qint64 &tL, qint64 &tH);

    /*!---------------------Runt-------------------------*/
    DsoErr              setRuntPolarity(bool b);
    bool                getRuntPolarity(void);

    DsoErr              setRuntWhen(int w);
    int                 getRuntWhen(void);

    DsoErr              setRuntLower(qint64 t);
    qint64              getRuntLower(void);

    DsoErr              setRuntUpper(qint64 t);
    qint64              getRuntUpper(void);

    DsoErr              setRuntSource(int src);
    int                 getRuntSource();

    /*!---------------------Slope-------------------------*/
    DsoErr              setSlopePolarty(int pol);
    int                 getSlopePolarty();

    DsoErr              setSlopeWhen(int w);
    int                 getSlopeWhen(void);

    DsoErr              setSlopeLower(qint64 t);
    qint64              getSlopeLower(void);

    DsoErr              setSlopeUpper(qint64 t);
    qint64              getSlopeUpper(void);

    DsoErr              setSlopeSource(int src);
    int                 getSlopeSource();

    /*!--------------------Rs232---------------------------*/
    bool               getRs232Polarity(void);
    DsoErr             setRs232Polarity(bool b);

    int                getRs232When(void);
    DsoErr             setRs232When(int w);

    int                getRs232BaudRate(void);
    DsoErr             setRs232BaudRate(int baud);

    int                getRs232Parity(void);
    DsoErr             setRs232Parity(int pari);

    int                getRs232StopBits(void);
    DsoErr             setRs232StopBits(int bit);

    int                getRs232DataBits(void);
    DsoErr             setRs232DataBits(int bit);

    int                getRs232Data(void);
    DsoErr             setRs232Data(int data);

    int                getRs232Source();
    DsoErr             setRs232Source(int src);

    /*!-----------------------IIC-----------------------*/
    int                getI2cWhen(void);
    int                setI2cWhen(int s);
                   
    int                getI2cDir(void);
    int                setI2cDir(int b);
                   
    int                getI2cAddrBits(void);
    int                setI2cAddrBits(int a);
                   
    int                getI2cAddress(void);
    int                setI2cAddress(int a);
                   
    int                getI2cSCL(void);
    int                setI2cSCL(int src = chan1);
                   
    int                getI2cSDA(void);
    int                setI2cSDA(int src = chan2);
                   
    int                getI2cByteCount(void);
    int                setI2cByteCount(int byt);
                   
    qint64             getI2cData();
    int                setI2cData(qint64 data);

    /*!--------------------SPI--------------------------*/
    bool               getSpiWhen(void);
    int                setSpiWhen(bool b);
                    
    bool               getSpiCSMode(void);
    int                setSpiCSMode(bool b);
                    
    int                getSpiSCL(void);
    int                setSpiSCL(int src = chan1);
                    
    bool               getSpiEdge(void);
    int                setSpiEdge(bool b);
                    
    int                getSpiSDA(void);
    int                setSpiSDA(int src = chan2);
                    
    int                getSpiCS(void);
    int                setSpiCS(int src = chan3);
                    
    int                getSpiBitCount(void);
    int                setSpiBitCount(int count);
                    
    qint64             getSpiTimeout(void);
    int                setSpiTimeout(qint64 t);
                    
    qint64             getSpiData(void);
    int                setSpiData(qint64 data);

    /*!------------------Navigation--------------------*/
    DsoErr             setNavigationEvent(int event);
    int                getNavigationEvent();

    /*----------------------scpi------------------------*/
    DsoErr             setScpiRuntLower(qint64 t);
    qint64             getScpiRuntLower(void);
    DsoErr             setScpiRuntUpper(qint64 t);
    qint64             getScpiRuntUpper(void);
    int                setScpiRuntTime(qint64 time);
    qint64             getScpiRuntTime();


    DsoErr             setScpiSlopeLower(qint64 t);
    qint64             getScpiSlopeLower(void);
    DsoErr             setScpiSlopeUpper(qint64 t);
    qint64             getScpiSlopeUpper(void);
    int                setScpiSlopeTime(qint64 time);
    qint64             getScpiSlopeTime();


    DsoErr             setScpiPulseLower(qint64 t);
    qint64             getScpiPulseLower(void);
    DsoErr             setScpiPulseUpper(qint64 t);
    qint64             getScpiPulseUpper(void);
    int                setScpiPulseTime(qint64 time);
    qint64             getScpiPulseTime();

    qint64             getEventAllCount();
    DsoErr             getEventItemTime(CArgument argI, CArgument argO);

public:
     CSource*          getSourcePtr(Chan ch);
     CSource*          getCurrSourcePtr();
     Chan              getCurrSource();
     DsoErr            setCurrSource(Chan ch);

public:
     void              onHoriChange();
     void              onVertChange();
     void              onRunStopChange();
     void              onDisplayClear();
     void              onChOnOff();
     void              onLicenseOpt();

private:
     //! time range
     _search_time_range  timeRangeIndex(enumSearchType type);
     virtual DsoErr      checkTimeRange(enumSearchType type,
                                        TrigTimeRule   rule,
                                        const EMoreThan w0 = Trigger_When_None);

     virtual  void       getUpperAttr( enumSearchType type );
     virtual  void       getLowerAttr( enumSearchType type );

     void                getPulseUpperAttr( enumAttr ){getUpperAttr(SEARCH_TYPE_PULSE);}
     void                getPulseLowerAttr( enumAttr ){getLowerAttr(SEARCH_TYPE_PULSE);}
     void                getRuntUpperAttr(  enumAttr ){getUpperAttr(SEARCH_TYPE_RUNT); }
     void                getRuntLowerAttr(  enumAttr ){getLowerAttr(SEARCH_TYPE_RUNT); }
     void                getSlopeUpperAttr( enumAttr ){getUpperAttr(SEARCH_TYPE_SLOPE);}
     void                getSlopeLowerAttr( enumAttr ){getLowerAttr(SEARCH_TYPE_SLOPE);}

private:
     static const  int            SEARCH_BIT_Z = 0xABCDEF00;
     static CSource*              m_pSources[all_trig_src];

     bool                         bSearchOnOff;
     bool                         bMarkTabelOnOff;
     int                          mNavigationEvent;
     enumSearchType               enSearchType;

     ControlAction                m_runStop;
     AcquireTimemode              m_TimeMode;
     bool                         m_bZoom;
     qint64                       m_timeOut_ms;
                                 
     CSource*                     m_pCurrSource;

     bool                         m_bCodeBit;
     int                          m_curBit;
     int                          m_bytMax;
     int                          m_bitMax;
     int                          m_bitbytMax;
     QBitArray                    *pBitCode;
     QBitArray                    *pBitCodeMask;
     QByteArray                   *pBytCode;
     QByteArray                   *pBytCodeMask;

/*! ------------------------------------------------------------*/
private:
    searchData          m_searchData;
public:
    pointer getsearchData();
    DsoErr  updateDataCfg();
    DsoErr  updateData();
    DsoErr  save();

public:
    static DsoErr  saveTable(QString fileName);

public:
    static const _copy_search_trig   stp_search_trig[];
    static const _search_time_range stp_time_range[];

public Q_SLOTS:
    void  onTimeout(int id);
private:
    searchThread *m_pUpdateWorker;
friend class searchThread;
};
#endif // SERVSEARCH_H

