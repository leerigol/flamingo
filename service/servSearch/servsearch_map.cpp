#include "servsearch.h"
/*! \var 定义消息映射表
* 定义消息映射表
*/

IMPLEMENT_POST_DO( serviceExecutor, servSearch)
start_of_postdo()
on_postdo( MSG_SEARCH_EN,                &servSearch::apply),
on_postdo( MSG_SEARCH_EN,                &servSearch::onChOnOff),

end_of_postdo()


IMPLEMENT_CMD( serviceExecutor, servSearch )
start_of_entry()

on_set_int_bool(MSG_SEARCH_EN,                    &servSearch::setSearchOnOff),
on_get_bool    (MSG_SEARCH_EN,                    &servSearch::getSearchOnOff),

on_set_int_bool(MSG_SEARCH_MARK_TABEL_EN,         &servSearch::setMarkTabelOnOff),
on_get_bool    (MSG_SEARCH_MARK_TABEL_EN,         &servSearch::getMarkTabelOnOff),


on_set_int_int(MSG_SEARCH_TYPE,                   &servSearch::setSearchType),
on_get_int    (MSG_SEARCH_TYPE,                   &servSearch::getSearchType),

on_set_int_int(MSG_SEARCH_NAVIGATION_EVENT,       &servSearch::setNavigationEvent),
on_get_int    (MSG_SEARCH_NAVIGATION_EVENT,       &servSearch::getNavigationEvent),

on_set_int_void(MSG_SEARCH_CPY_TO_TRIGGER,        &servSearch::copyToTrig),
on_set_int_void(MSG_SEARCH_CPY_FROM_TRIGGER,      &servSearch::copyFormTrig),

on_set_int_int(MSG_SEARCH_THRE,                   &servSearch::setSearchThre),
on_get_int    (MSG_SEARCH_THRE,                   &servSearch::getSearchThre),

on_set_int_int(MSG_SEARCH_THRE_A,                 &servSearch::setSearchThreA),
on_get_int    (MSG_SEARCH_THRE_A,                 &servSearch::getSearchThreA),

on_set_int_int(MSG_SEARCH_THRE_B,                 &servSearch::setSearchThreB),
on_get_int    (MSG_SEARCH_THRE_B,                 &servSearch::getSearchThreB),

on_set_int_int(MSG_SEARCH_THRE_IIC_SCL,           &servSearch::setThreI2cScl),
on_get_int    (MSG_SEARCH_THRE_IIC_SCL,           &servSearch::getThreI2cScl),

on_set_int_int(MSG_SEARCH_THRE_IIC_SDA,           &servSearch::setThreI2cSda),
on_get_int    (MSG_SEARCH_THRE_IIC_SDA,           &servSearch::getThreI2cSda),

on_set_int_int(MSG_SEARCH_THRE_SPI_SCL,           &servSearch::setThreSpiScl),
on_get_int    (MSG_SEARCH_THRE_SPI_SCL,           &servSearch::getThreSpiScl),

on_set_int_int(MSG_SEARCH_THRE_SPI_SDA,           &servSearch::setThreSpiSda),
on_get_int    (MSG_SEARCH_THRE_SPI_SDA,           &servSearch::getThreSpiSda),

on_set_int_int(MSG_SEARCH_THRE_SPI_CS,            &servSearch::setThreSpiCs),
on_get_int    (MSG_SEARCH_THRE_SPI_CS,            &servSearch::getThreSpiCs),

on_set_int_int(CMD_SEARCH_CODE_CURR_BIT,         &servSearch::setCodeCurBit),
on_get_int    (CMD_SEARCH_CODE_CURR_BIT,         &servSearch::getCodeCurBit),
on_set_int_int(CMD_SEARCH_CODE_VALUE,            &servSearch::setCodeValue),
on_get_int    (CMD_SEARCH_CODE_VALUE,            &servSearch::getCodeValue),
on_set_int_int (CMD_SEARCH_CODE_ALL,             &servSearch::setCodeAll),
on_get_void_argo(CMD_SEARCH_CODE_ARG,            &servSearch::getCodeArg),

on_set_void_void( CMD_TRIGGER_ALL_SPY_LICENSE,   &servSearch::onLicenseOpt),

//!edge
on_set_int_int(MSG_SEARCH_EDGE_SOURCE,            &servSearch::setEdgeSource),
on_get_int    (MSG_SEARCH_EDGE_SOURCE,            &servSearch::getEdgeSource),

on_set_int_int(MSG_SEARCH_EDGE_SLOPE,             &servSearch::setEdgeSlope),
on_get_int    (MSG_SEARCH_EDGE_SLOPE,             &servSearch::getEdgeSlope),

//!Pulse
on_set_int_int(MSG_SEARCH_PULSE_SOURCE,           &servSearch::setPulseSource),
on_get_int    (MSG_SEARCH_PULSE_SOURCE,           &servSearch::getPulseSource),

on_set_int_int(MSG_SEARCH_PULSE_POLARITY,         &servSearch::setPulsePolarty),
on_get_int    (MSG_SEARCH_PULSE_POLARITY,         &servSearch::getPulsePolarty),

on_set_int_int(MSG_SEARCH_PULSE_WHEN,             &servSearch::setPulseWhen),
on_get_int    (MSG_SEARCH_PULSE_WHEN,             &servSearch::getPulseWhen),

on_set_int_ll(MSG_SEARCH_PULSE_UPPER,            &servSearch::setPulseUpper),
on_get_ll_attr    (MSG_SEARCH_PULSE_UPPER,       &servSearch::getPulseUpper, &servSearch::getPulseUpperAttr),

on_set_int_ll(MSG_SEARCH_PULSE_LOWER,            &servSearch::setPulseLower),
on_get_ll_attr    (MSG_SEARCH_PULSE_LOWER,       &servSearch::getPulseLower, &servSearch::getPulseLowerAttr),

//!runt
on_set_int_int(MSG_SEARCH_RUNT_SOURCE,            &servSearch::setRuntSource),
on_get_int    (MSG_SEARCH_RUNT_SOURCE,            &servSearch::getRuntSource),

on_set_int_bool(MSG_SEARCH_RUNT_POLARITY,         &servSearch::setRuntPolarity),
on_get_bool    (MSG_SEARCH_RUNT_POLARITY,         &servSearch::getRuntPolarity),

on_set_int_int(MSG_SEARCH_RUNT_QUALIFIER,         &servSearch::setRuntWhen),
on_get_int    (MSG_SEARCH_RUNT_QUALIFIER,         &servSearch::getRuntWhen),

on_set_int_ll (MSG_SEARCH_RUNT_UPPER,             &servSearch::setRuntUpper),
on_get_ll_attr(MSG_SEARCH_RUNT_UPPER,             &servSearch::getRuntUpper, &servSearch::getRuntUpperAttr),

on_set_int_ll (MSG_SEARCH_RUNT_LOWER,             &servSearch::setRuntLower),
on_get_ll_attr(MSG_SEARCH_RUNT_LOWER,             &servSearch::getRuntLower, &servSearch::getRuntLowerAttr),

//!slope
on_set_int_int(MSG_SEARCH_SLOPE_SOURCE,           &servSearch::setSlopeSource),
on_get_int    (MSG_SEARCH_SLOPE_SOURCE,           &servSearch::getSlopeSource),

on_set_int_int(MSG_SEARCH_SLOPE_POLARITY,         &servSearch::setSlopePolarty),
on_get_int    (MSG_SEARCH_SLOPE_POLARITY,         &servSearch::getSlopePolarty),

on_set_int_int(MSG_SEARCH_SLOPE_WHEN,             &servSearch::setSlopeWhen),
on_get_int    (MSG_SEARCH_SLOPE_WHEN,             &servSearch::getSlopeWhen),

on_set_int_ll (MSG_SEARCH_SLOPE_UPPER,            &servSearch::setSlopeUpper),
on_get_ll_attr(MSG_SEARCH_SLOPE_UPPER,            &servSearch::getSlopeUpper, &servSearch::getSlopeUpperAttr),

on_set_int_ll (MSG_SEARCH_SLOPE_LOWER,            &servSearch::setSlopeLower),
on_get_ll_attr(MSG_SEARCH_SLOPE_LOWER,            &servSearch::getSlopeLower, &servSearch::getSlopeLowerAttr),

//!rs232
on_set_int_int (MSG_SEARCH_RS232_SOURCE,          &servSearch::setRs232Source),
on_get_int     (MSG_SEARCH_RS232_SOURCE,          &servSearch::getRs232Source),

on_set_int_bool(MSG_SEARCH_RS232_POLARITY,        &servSearch::setRs232Polarity),
on_get_bool    (MSG_SEARCH_RS232_POLARITY,        &servSearch::getRs232Polarity),

on_set_int_int (MSG_SEARCH_RS232_BAUDRATE,        &servSearch::setRs232BaudRate),
on_get_int     (MSG_SEARCH_RS232_BAUDRATE,        &servSearch::getRs232BaudRate),

on_set_int_int (MSG_SEARCH_RS232_WHEN,            &servSearch::setRs232When),
on_get_int     (MSG_SEARCH_RS232_WHEN,            &servSearch::getRs232When),

on_set_int_int (MSG_SEARCH_RS232_CHECK,           &servSearch::setRs232Parity),
on_get_int     (MSG_SEARCH_RS232_CHECK,           &servSearch::getRs232Parity),

on_set_int_int (MSG_SEARCH_RS232_STOPBIT,         &servSearch::setRs232StopBits),
on_get_int     (MSG_SEARCH_RS232_STOPBIT,         &servSearch::getRs232StopBits),

on_set_int_int (MSG_SEARCH_RS232_DATAWIDTH,       &servSearch::setRs232DataBits),
on_get_int     (MSG_SEARCH_RS232_DATAWIDTH,       &servSearch::getRs232DataBits),

on_set_int_int (MSG_SEARCH_RS232_DATA,            &servSearch::setRs232Data),
on_get_int     (MSG_SEARCH_RS232_DATA,            &servSearch::getRs232Data),

//!IIC
on_set_int_int  ( MSG_SEARCH_I2C_SCL,            &servSearch::setI2cSCL),
on_get_int      ( MSG_SEARCH_I2C_SCL,            &servSearch::getI2cSCL),
              
on_set_int_int  ( MSG_SEARCH_I2C_SDA,            &servSearch::setI2cSDA),
on_get_int      ( MSG_SEARCH_I2C_SDA,            &servSearch::getI2cSDA),
              
on_set_int_int  ( MSG_SEARCH_I2C_WHEN,           &servSearch::setI2cWhen),
on_get_int      ( MSG_SEARCH_I2C_WHEN,           &servSearch::getI2cWhen),
                
on_set_int_int  ( MSG_SEARCH_I2C_ADDRTYPE,       &servSearch::setI2cAddrBits),
on_get_int      ( MSG_SEARCH_I2C_ADDRTYPE,       &servSearch::getI2cAddrBits),
                
on_set_int_int  ( MSG_SEARCH_I2C_ADDRDATA_BITS,  &servSearch::setI2cAddrBits),
on_get_int      ( MSG_SEARCH_I2C_ADDRDATA_BITS,  &servSearch::getI2cAddrBits),
                
on_set_int_int  ( MSG_SEARCH_I2C_ADDRESS,        &servSearch::setI2cAddress),
on_get_int      ( MSG_SEARCH_I2C_ADDRESS,        &servSearch::getI2cAddress),
                
on_set_int_int  ( MSG_SEARCH_I2C_DIRECTION,      &servSearch::setI2cDir),
on_get_int      ( MSG_SEARCH_I2C_DIRECTION,      &servSearch::getI2cDir),
                 
on_set_int_int  ( MSG_SEARCH_I2C_BYTELENGTH,     &servSearch::setI2cByteCount),
on_get_int      ( MSG_SEARCH_I2C_BYTELENGTH,     &servSearch::getI2cByteCount),
                 
on_set_int_ll   ( MSG_SEARCH_I2C_DATA,           &servSearch::setI2cData),
on_get_ll       ( MSG_SEARCH_I2C_DATA,           &servSearch::getI2cData),

//! SPI
on_set_int_int  (       MSG_SEARCH_SPI_SCL,            &servSearch::setSpiSCL),
on_get_int      (       MSG_SEARCH_SPI_SCL,            &servSearch::getSpiSCL),

on_get_bool     (       MSG_SEARCH_SLOPE_POLARITY,     &servSearch::getSpiEdge),
on_set_int_bool (       MSG_SEARCH_SLOPE_POLARITY,     &servSearch::setSpiEdge),

on_set_int_int  (       MSG_SEARCH_SPI_SDA,            &servSearch::setSpiSDA),
on_get_int      (       MSG_SEARCH_SPI_SDA,            &servSearch::getSpiSDA),

on_set_int_bool (       MSG_SEARCH_SPI_WHEN,           &servSearch::setSpiWhen),
on_get_bool     (       MSG_SEARCH_SPI_WHEN,           &servSearch::getSpiWhen),

on_set_int_int  (       MSG_SEARCH_SPI_CS,             &servSearch::setSpiCS),
on_get_int      (       MSG_SEARCH_SPI_CS,             &servSearch::getSpiCS),

on_set_int_int  (       MSG_SEARCH_SPI_CSMODE,         &servSearch::setSpiCSMode),
on_get_int      (       MSG_SEARCH_SPI_CSMODE,         &servSearch::getSpiCSMode),

on_set_int_ll   (       MSG_SEARCH_SPI_TIMEOUT,        &servSearch::setSpiTimeout),
on_get_ll       (       MSG_SEARCH_SPI_TIMEOUT,        &servSearch::getSpiTimeout),

on_set_int_ll   (       MSG_SEARCH_SPI_DATA,           &servSearch::setSpiData),
on_get_ll       (       MSG_SEARCH_SPI_DATA,           &servSearch::getSpiData),

on_set_int_int  (       MSG_SEARCH_SPI_DATABITS,       &servSearch::setSpiBitCount),
on_get_int      (       MSG_SEARCH_SPI_DATABITS,       &servSearch::getSpiBitCount),

//!读取结果
on_get_pointer(  CMD_SEARCH_GET_DATA,             &servSearch::getsearchData),

//!app
on_get_pointer(  CMD_SEARCH_SOURCE_CURR_PTR,      &servSearch::getCurrSourcePtr),

//!spy
on_set_void_void(MSG_HOR_ACQ_MEM_DEPTH,            &servSearch::onHoriChange),
on_set_void_void(MSG_HORI_MAIN_SCALE  ,            &servSearch::onHoriChange),
on_set_void_void(MSG_HORI_MAIN_OFFSET ,            &servSearch::onHoriChange),
on_set_void_void(MSG_HORI_ZOOM_SCALE  ,            &servSearch::onHoriChange),
on_set_void_void(MSG_HORI_ZOOM_OFFSET ,            &servSearch::onHoriChange),
on_set_void_void(MSG_HOR_TIME_MODE    ,            &servSearch::onHoriChange),
on_set_void_void ( MSG_HOR_ZOOM_ON,                &servSearch::onHoriChange),
on_set_void_void(MSG_HORIZONTAL_RUN ,              &servSearch::onRunStopChange),

on_set_void_void( MSG_DISPLAY_CLEAR,               &servSearch::onDisplayClear),
on_set_void_void( MSG_CHAN_ON_OFF,                 &servSearch::onChOnOff),

on_set_void_void( MSG_CHAN_SCALE,                  &servSearch::onVertChange),
on_set_void_void( MSG_CHAN_OFFSET,                 &servSearch::onVertChange),

//!save
on_set_int_void (MSG_SEARCH_MARK_TABEL_SAVE,       &servSearch::save),

//! sys msg
on_set_void_void( CMD_SERVICE_RST,          &servSearch::rst ),
on_set_int_void(  CMD_SERVICE_STARTUP,      &servSearch::startup ),
on_set_int_int(   CMD_SERVICE_ACTIVE,       &servSearch::setActive ),
on_set_int_int(   CMD_SERVICE_DEACTIVE,     &servSearch::setdeActive ),
on_set_void_int(  CMD_SERVICE_TIMEOUT,      &servSearch::onTimeout ),

/********scpi***********/

on_set_int_ll   (       search_dso::cmd_slope_time,     &servSearch::setScpiSlopeTime),
on_get_ll       (       search_dso::cmd_slope_time,     &servSearch::getScpiSlopeTime),
on_set_int_ll   (       search_dso::cmd_slope_lower,    &servSearch::setScpiSlopeLower),
on_get_ll       (       search_dso::cmd_slope_lower,    &servSearch::getScpiSlopeLower),
on_set_int_ll   (       search_dso::cmd_slope_upper,    &servSearch::setScpiSlopeUpper),
on_get_ll       (       search_dso::cmd_slope_upper,    &servSearch::getScpiSlopeUpper),

on_set_int_ll   (       search_dso::cmd_runt_time,      &servSearch::setScpiRuntTime),
on_get_ll       (       search_dso::cmd_runt_time,      &servSearch::getScpiRuntTime),
on_set_int_ll   (       search_dso::cmd_runt_lower,     &servSearch::setScpiRuntLower),
on_get_ll       (       search_dso::cmd_runt_lower,     &servSearch::getScpiRuntLower),
on_set_int_ll   (       search_dso::cmd_runt_upper,     &servSearch::setScpiRuntUpper),
on_get_ll       (       search_dso::cmd_runt_upper,     &servSearch::getScpiRuntUpper),


on_set_int_ll   (       search_dso::cmd_pulse_time,     &servSearch::setScpiPulseTime),
on_get_ll       (       search_dso::cmd_pulse_time,     &servSearch::getScpiPulseTime),
on_set_int_ll   (       search_dso::cmd_pulse_lower,     &servSearch::setScpiPulseLower),
on_get_ll       (       search_dso::cmd_pulse_lower,     &servSearch::getScpiPulseLower),
on_set_int_ll   (       search_dso::cmd_pulse_upper,     &servSearch::setScpiPulseUpper),
on_get_ll       (       search_dso::cmd_pulse_upper,     &servSearch::getScpiPulseUpper),

//event
on_get_ll       (       search_dso::cmd_event_all_count, &servSearch::getEventAllCount),
on_get_void_argi_argo(  search_dso::cmd_event_item_time, &servSearch::getEventItemTime),

end_of_entry()




















