#ifndef SEARCHDSO_H
#define SEARCHDSO_H
#include "../../engine/base/search/enginecfg_search.h"
#define  REPORT_BUFF_MAX  1000
#define  SEARCH_TIMER_TMO 20 //!ms
#define  SEARCH_TIMER_ID  10

class servSearch;

namespace search_dso{

enum enumSearcEnab
{
    SEARCH_ENAB_MAIN_ON   = 0X00000006,
    SEARCH_ENAB_MAIN_OFF  = 0X00000000,

    SEARCH_ENAB_ZOOM_ON   = 0X00000007,
    SEARCH_ENAB_ZOOM_OFF  = 0X00000000,
};

enum searcCmd
{
    CMD_START_SEARCH = 1,
    CMD_STOP_SEARCH,
    CMD_UPDATE_UI,

    CMD_CODE_CHANGE,

    /*scpi***************/
    cmd_pulse_time,
    cmd_pulse_lower,
    cmd_pulse_upper,

    cmd_slope_time,
    cmd_slope_lower,
    cmd_slope_upper,

    cmd_runt_time,
    cmd_runt_lower,
    cmd_runt_upper,

    cmd_event_all_count,
    cmd_event_item_time,
};


class searchData
{
public:
    horiAttr            m_HoriAttr;
    horiAttr            m_ZoomHoriAttr;
    EngineSearchData    m_data;
};

/**copy trig*/
typedef void*   (servSearch::*_fGetCfg)(void);
typedef int     (servSearch::*_fSetCfg)(void *);
struct _copy_search_trig
{
    int      serachType;
    QString  trigName;
    int      cfgCmd;
    _fGetCfg fGetCfg;
    _fSetCfg fSetCfg;
};


typedef void (servSearch::*_fGetTimeRange)(qint64 &tL, qint64 &tH, EMoreThan &w);
typedef int  (servSearch::*_fSetTimeRange)(qint64 &tL, qint64 &tH);
struct _search_time_range
{
    int            serachType;
    qint64         tMin;
    qint64         tMax;
    _fGetTimeRange fGetTimeRange;
    _fSetTimeRange fSetTimeRange;
};

}

#endif // SEARCHDSO_H
