#include "servsearch.h"

int servSearch::getChSlope(Chan /*ch*/)
{
    if(SEARCH_TYPE_EDGE == enSearchType)
    {
        int slope = getEdgeSlope();
        if(slope == Trigger_Edge_Any)
        {
            slope = Trig_View_Any_L_H;
        }
        if(slope == Trigger_Edge_Alternating)
        {
            slope = Trig_View_Alternating_L_H;
        }
        return slope;
    }
    else if(SEARCH_TYPE_PULSE == enSearchType)
    {
            return (int)(!getPulsePolarty());
    }
    else if(SEARCH_TYPE_RUNT == enSearchType)
    {
            return (int)(!getRuntPolarity());
    }
    else if(SEARCH_TYPE_SLOPE == enSearchType)
    {
        return (int)(getSlopePolarty());
    }
    else if(SEARCH_TYPE_RS232 == enSearchType)
    {
        return (int)(!getRs232Polarity());
    }
    else
    { return Trig_View_Rising_H; }
}

DsoErr servSearch::initLevelEngine()
{
    DsoErr err = ERR_NONE;
    for ( int ch = chan1; ch <= chan4; ch++ )
    {
        err = applyThreshold((Chan)ch);

        if(ERR_NONE != err)
        {
            return err;
        }
    }
    return err;
}

DsoErr servSearch::applyThreshold(Chan chan)
{
    DsoErr err;
    Chan ch = chan_none;
    if(chan_none == chan)
    {
       ch = getCurrSource();
    }
    else
    {
        ch = chan;
    }

    CSource *pSource = getSourcePtr(ch);
    Q_ASSERT(NULL != pSource);

    qint64 nLeVelA = pSource->getLevel(0);
    qint64 nLeVelB = pSource->getLevel(1);
    int    nScale  = pSource->get_nScale();

    int    slope   = getChSlope(ch);
    int    nNoise  = nScale*0.3;

    int   aMin = 0, aMax = 0;
    int   bMin = 0, bMax = 0;

    for(int i = 0; i < array_count(TriggerBase::s_a_levelSlopeInc); i++)
    {
        if(slope == TriggerBase::s_a_levelSlopeInc[i].slope)
        {
            aMax = (int)(nLeVelA + TriggerBase::s_a_levelSlopeInc[i].aMaxInc * nNoise);
            aMin = (int)(nLeVelA + TriggerBase::s_a_levelSlopeInc[i].aMinInc * nNoise);
            bMax = (int)(nLeVelB + TriggerBase::s_a_levelSlopeInc[i].bMaxInc * nNoise);
            bMin = (int)(nLeVelB + TriggerBase::s_a_levelSlopeInc[i].bMinInc * nNoise);
            break;
        }
    }

    //!配置到引擎
    err = postEngine( ENGINE_SEARCH_LEVEL_A,
                      ch, aMin, aMax);
    if(err != ERR_NONE) return err;

    err = postEngine( ENGINE_SEARCH_LEVEL_B,
                      ch, bMin, bMax );
    return err;
}

void servSearch::setSourceThre(Chan ch,
                               qint64 threA,
                               qint64 threB,
                               bool a,
                               bool b
                               )
{
    CSource *pSource = getSourcePtr( ch );
    Q_ASSERT(NULL != pSource);

    pSource->setLevel(0,threA);
    pSource->setLevel(1,threB);

    pSource->setTwoLevel(a, 0);
    pSource->setTwoLevel(b, 1);

    setCurrSource(ch);
}

void servSearch::getSourceThre(Chan ch, int ab,
                               qint64 &thre,
                               qint64 &step,
                               qint64 &max,
                               qint64 &min)
{
    CSource *pSource = getSourcePtr( ch );
    Q_ASSERT(NULL != pSource);

    thre   = pSource->getLevel(ab);
    step   = pSource->get_nLevelStep();
    max    = pSource->get_nLevelMax();
    min    = pSource->get_nLevelMin();
}

DsoErr servSearch::setSearchThre(int thre)
{
    Chan ch = getCurrSource();
    setSourceThre( ch, thre);

    return applyThreshold();
}

int servSearch::getSearchThre()
{
    Chan   ch   = getCurrSource();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 0, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );
    return thre;
}

DsoErr servSearch::setSearchThreA(int thre)
{
    CSource *src   = getCurrSourcePtr();
    Q_ASSERT(src != NULL);

    int threMax = src->getLevel(1);
    thre = qMax(thre, threMax);

    src->setLevel(0,thre);
    src->setTwoLevel(true, 0);
    src->setTwoLevel(true, 1);
    return applyThreshold();
}

int servSearch::getSearchThreA()
{
    Chan   ch   = getCurrSource();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 0, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );
    return thre;
}

DsoErr servSearch::setSearchThreB(int thre)
{
    CSource *src   = getCurrSourcePtr();
    Q_ASSERT(src != NULL);
    int threMin = src->getLevel(0);
    thre = qMin(thre, threMin);

    src->setLevel(1,thre);
    src->setTwoLevel(true, 0);
    src->setTwoLevel(true, 1);
    return applyThreshold();
}

int servSearch::getSearchThreB()
{
    Chan   ch   = getCurrSource();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 1, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );
    return thre;
}

DsoErr servSearch::setThreI2cScl(int thre)
{
    Chan ch = (Chan)cfg_get_i2c_SCL();

    setSourceThre( ch, thre);

    return applyThreshold(ch);
}

int servSearch::getThreI2cScl()
{
    Chan   ch   = (Chan)cfg_get_i2c_SCL();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 0, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );

    return thre;
}

DsoErr servSearch::setThreI2cSda(int thre)
{
    Chan ch = (Chan)cfg_get_i2c_SDA();

    setSourceThre( ch, thre);

    return applyThreshold(ch);
}

int servSearch::getThreI2cSda()
{
    Chan   ch   = (Chan)cfg_get_i2c_SDA();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 0, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );

    return thre;
}

DsoErr servSearch::setThreSpiScl(int thre)
{
    Chan ch = (Chan)cfg_get_spi_scl();

    setSourceThre( ch, thre);

    return applyThreshold(ch);
}

int servSearch::getThreSpiScl()
{
    Chan   ch   = (Chan)cfg_get_spi_scl();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 0, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );

    return thre;
}

DsoErr servSearch::setThreSpiSda(int thre)
{
    Chan ch = (Chan)cfg_get_spi_sda();

    setSourceThre( ch, thre);

    return applyThreshold(ch);
}

int servSearch::getThreSpiSda()
{
    Chan   ch   = (Chan)cfg_get_spi_sda();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 0, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );

    return thre;
}

DsoErr servSearch::setThreSpiCs(int thre)
{
    Chan ch = (Chan)cfg_get_spi_cs();

    setSourceThre( ch, thre);

    return applyThreshold(ch);
}

int servSearch::getThreSpiCs()
{
    Chan   ch   = (Chan)cfg_get_spi_cs();
    qint64 thre = 0;
    qint64 step = 0;
    qint64 max  = 0;
    qint64 min  = 0;

    getSourceThre(ch, 0, thre, step, max, min);

    setuiRange( min, max, (qint64)mv(0) );
    setuiStep(step);

    setuiBase( 1, E_N6 );
    setuiUnit( Unit_V );

    return thre;
}
