#include "servsearch.h"

/*******************scpi**************************/
DsoErr servSearch::setScpiRuntLower(qint64 t)
{
    cfg_runt_setLower(t);
    updateAllUi();
    return ERR_NONE;
}

qint64 servSearch::getScpiRuntLower()
{
    return cfg_runt_getLower();
}

DsoErr servSearch::setScpiRuntUpper(qint64 t)
{
    cfg_runt_setUpper(t);
    updateAllUi();
    return ERR_NONE;
}

qint64 servSearch::getScpiRuntUpper()
{
    return cfg_runt_getUpper();
}

int servSearch::setScpiRuntTime(qint64 time)
{
    DsoErr err  = ERR_NONE;
    qint64 diff = 0;

    switch ( cfg_runt_getWhen() )
    {
    case Trigger_When_None:
        break;

    case Trigger_When_Morethan:
        err = async(MSG_SEARCH_RUNT_LOWER, time);
        break;

    case Trigger_When_Lessthan:
        err = async(MSG_SEARCH_RUNT_UPPER, time);
        break;

    case Trigger_When_MoreLess:

        diff =   cfg_runt_getUpper() - cfg_runt_getLower();
        err = async(MSG_SEARCH_RUNT_LOWER, time);
        err = async(MSG_SEARCH_RUNT_UPPER, time + diff );
        break;

    default:
        err = ERR_INVALID_INPUT;
        break;
    }
    return err;
}

qint64 servSearch::getScpiRuntTime()
{
    qint64 time = 0;

    switch (cfg_runt_getWhen())
    {
    case Trigger_When_Morethan:
        time = cfg_runt_getLower();
        break;

    case Trigger_When_Lessthan:
        time = cfg_runt_getUpper();
        break;

    case Trigger_When_MoreLess:
        time = cfg_runt_getLower();
        break;

    default:
        time = 0;
        break;
    }
    return time;
}


DsoErr servSearch::setScpiSlopeLower(qint64 t)
{
    cfg_slope_setLower(t);
    updateAllUi();
    return ERR_NONE;
}

qint64 servSearch::getScpiSlopeLower()
{
    return cfg_slope_getLower();
}

DsoErr servSearch::setScpiSlopeUpper(qint64 t)
{
    cfg_slope_setUpper(t);
    updateAllUi();
    return ERR_NONE;
}

qint64 servSearch::getScpiSlopeUpper()
{
    return cfg_slope_getUpper();
}

int servSearch::setScpiSlopeTime(qint64 time)
{
    DsoErr err  = ERR_NONE;
    qint64 diff = 0;

    switch (cfg_slope_getCmp())
    {
    case Trigger_When_Morethan:
        err = async(MSG_SEARCH_SLOPE_LOWER, time);
        break;

    case Trigger_When_Lessthan:
        err = async(MSG_SEARCH_SLOPE_UPPER, time);
        break;

    case Trigger_When_MoreLess:

        diff =   cfg_slope_getUpper() - cfg_slope_getLower();
        err = async(MSG_SEARCH_SLOPE_LOWER, time);
        err = async(MSG_SEARCH_SLOPE_UPPER, time + diff );
        break;

    default:
        err = ERR_INVALID_INPUT;
        break;
    }
    return err;
}

qint64 servSearch::getScpiSlopeTime()
{
    qint64 time = 0;

    switch (cfg_slope_getCmp())
    {
    case Trigger_When_Morethan:
        time = cfg_slope_getLower();
        break;

    case Trigger_When_Lessthan:
        time = cfg_slope_getUpper();
        break;

    case Trigger_When_MoreLess:
        time = cfg_slope_getLower();
        break;

    default:
        time = 0;
        break;
    }
    return time;
}

DsoErr servSearch::setScpiPulseLower(qint64 t)
{
    cfg_pulse_setLower(t);
    updateAllUi();
    return ERR_NONE;
}

qint64 servSearch::getScpiPulseLower()
{
    return cfg_pulse_getLower() ;
}

DsoErr servSearch::setScpiPulseUpper(qint64 t)
{
    cfg_pulse_setUpper(t);
    updateAllUi();
    return ERR_NONE;
}

qint64 servSearch::getScpiPulseUpper()
{
    return cfg_pulse_getUpper() ;
}
int servSearch::setScpiPulseTime(qint64 time)
{
    DsoErr err  = ERR_NONE;
    qint64 diff = 0;

    if( Trigger_When_Morethan == pulseCfg.getCmp() )
    {
        err = async(MSG_SEARCH_PULSE_LOWER, time);
    }
    else if( Trigger_When_Lessthan == pulseCfg.getCmp() )
    {
        err = async(MSG_SEARCH_PULSE_UPPER, time);
    }
    else
    {
        diff =   pulseCfg.getWidthH() - pulseCfg.getWidthL();
        err = async(MSG_SEARCH_PULSE_LOWER, time);
        err = async(MSG_SEARCH_PULSE_UPPER, time + diff);
    }

    return err;
}

qint64 servSearch::getScpiPulseTime()
{
    qint64 time = 0;

    switch (pulseCfg.getCmp())
    {
    case Trigger_When_Morethan:
        time = pulseCfg.getWidthL();
        break;

    case Trigger_When_Lessthan:
        time = pulseCfg.getWidthH();
        break;

    case Trigger_When_MoreLess:
        time = pulseCfg.getWidthL();
        break;

    default:
        time = 0;
        break;
    }
    return time;
}

qint64 servSearch::getEventAllCount()
{
    if(getSearchOnOff())
    {
        EngineSearchData *p_data = &m_searchData.m_data;
        if(NULL == p_data)
        {
            qDebug()<<"NO SEACH DATA!";
            return ERR_NONE;
        }

        QList<qlonglong>   llTimeList;
        p_data->getViewTimeValue(llTimeList);

        return llTimeList.count();
    }
    return ERR_NONE;
}

DsoErr servSearch::getEventItemTime(CArgument argI, CArgument argO)
{
    if(getSearchOnOff() && argI.size()>0)
    {
        EngineSearchData *p_data = &m_searchData.m_data;
        if( NULL == p_data)
        {
            qDebug()<<"NO SEACH DATA!";
            argO.setVal(0, 0);
            return ERR_NONE;
        }

        int n = 0;
        argI.getVal(n, 0);
        QList<qlonglong>   llTimeList;
        p_data->getViewTimeValue(llTimeList);

        if( n<llTimeList.count() )
        {
            argO.setVal( llTimeList.at(n) );
        }
    }
    return ERR_NONE;
}


