#include "servsearch.h"
#include "../servhori/servhori.h"
#include "search_thread.h"

CSource*  servSearch::m_pSources[all_trig_src] = {0};

servSearch::servSearch(QString name, ServiceId eId):serviceExecutor(name, eId)
{
    mVersion = 0x03;

    pBitCode     = NULL;
    pBitCodeMask = NULL;
    pBytCode     = NULL;
    pBytCodeMask = NULL;
    m_bCodeBit   = false;
    m_curBit     = 0;
    m_bytMax     = 0;
    m_bitMax     = 0;
    m_bitbytMax  = 0;

    serviceExecutor::baseInit();

    if(m_pSources[0] == NULL)
    {
        for(int key=chan1; key<all_trig_src; key++)
        {
            m_pSources[key] = new CSource(key, CSource::search_source);
            Q_ASSERT(m_pSources[key]);
        }
        m_pSources[0] = m_pSources[chan1];
    }
    m_pCurrSource = getSourcePtr(chan1);

    m_pUpdateWorker = new searchThread( this );
    Q_ASSERT( NULL != m_pUpdateWorker );

    linkChange( MSG_SEARCH_EN, MSG_SEARCH_MARK_TABEL_EN);
    linkChange( MSG_SEARCH_EN, MSG_SEARCH_NAVIGATION_EVENT);

    linkChange( MSG_SEARCH_PULSE_WHEN,     MSG_SEARCH_PULSE_LOWER, MSG_SEARCH_PULSE_UPPER);
    linkChange( MSG_SEARCH_PULSE_UPPER,    MSG_SEARCH_PULSE_LOWER);
    linkChange( MSG_SEARCH_PULSE_LOWER,    MSG_SEARCH_PULSE_UPPER);

    linkChange( MSG_SEARCH_RUNT_QUALIFIER, MSG_SEARCH_RUNT_LOWER, MSG_SEARCH_RUNT_UPPER);
    linkChange( MSG_SEARCH_RUNT_UPPER,     MSG_SEARCH_RUNT_LOWER);
    linkChange( MSG_SEARCH_RUNT_LOWER,     MSG_SEARCH_RUNT_UPPER);

    linkChange( MSG_SEARCH_SLOPE_WHEN,     MSG_SEARCH_SLOPE_LOWER, MSG_SEARCH_SLOPE_UPPER);
    linkChange( MSG_SEARCH_SLOPE_UPPER,    MSG_SEARCH_SLOPE_LOWER);
    linkChange( MSG_SEARCH_SLOPE_LOWER,    MSG_SEARCH_SLOPE_UPPER);
}

int servSearch::serialOut(CStream &stream, serialVersion &ver)
{
    ISerial::serialOut(stream, ver );
#define ar ar_out
#define ar_pack_e ar_out
#define ar_pack_s(name, val) stream.write(name, val)

    ar("bSearchOnOff",        bSearchOnOff);
    ar("bMarkTabelOnOff",     bMarkTabelOnOff);
    ar_pack_e("enSearchType", enSearchType);
    ar("mNavigationEvent",    mNavigationEvent);
    ar_pack_e("m_TimeMode",   m_TimeMode);

    int curCh = getCurrSource();
    ar("curCh",        curCh);

    qint64 i64_value = 0;
    for(int i = 0; i < array_count(m_pSources); i++)
    {
        i64_value = m_pSources[i]->getLevel(0);
        ar_pack_s(QString("level_%1_a").arg(i), i64_value);

        i64_value = m_pSources[i]->getLevel(1);
        ar_pack_s(QString("level_%1_b").arg(i), i64_value);
    }

#undef ar
#undef ar_pack_e
#undef ar_pack_s

    servEdgeCfg::cfg_edge_serialOut(stream, ver);
    servPulseCfg::cfg_pulse_serialOut(stream, ver);
    servRuntCfg::cfg_runt_serialOut(stream, ver);
    servSlopeCfg::cfg_Slope_serialOut(stream, ver);
    servRs232Cfg::cfg_rs232_serialOut(stream, ver);
    servI2cCfg::cfg_i2c_serialOut(stream, ver);
    servSpiCfg::cfg_spi_serialOut(stream, ver);

    return ERR_NONE;
}
int servSearch::serialIn(CStream &stream, serialVersion ver)
{
    if(mVersion == ver)
    {

#define ar ar_in
#define ar_pack_e ar_e_in
#define ar_pack_s(name, val) stream.read(name, val)

        ar("bSearchOnOff",        bSearchOnOff);
        ar("bMarkTabelOnOff",     bMarkTabelOnOff);
        ar_pack_e("enSearchType", enSearchType);
        ar("mNavigationEvent",    mNavigationEvent);
        ar_pack_e("m_TimeMode",   m_TimeMode);

        int curCh = chan1;
        ar("curCh",        curCh);
        setCurrSource((Chan)curCh);

        qint64 i64_value = 0;
        for(int i = 0; i < array_count(m_pSources); i++)
        {
            ar_pack_s(QString("level_%1_a").arg(i), i64_value);
            m_pSources[i]->setLevel(0,i64_value);

            ar_pack_s(QString("level_%1_b").arg(i), i64_value);
            m_pSources[i]->setLevel(1,i64_value);
        }
#undef ar
#undef ar_pack_e
#undef ar_pack_s

        servEdgeCfg::cfg_edge_serialIn(stream, ver);
        servPulseCfg::cfg_pulse_serialIn(stream, ver);
        servRuntCfg::cfg_runt_serialIn(stream, ver);
        servSlopeCfg::cfg_Slope_serialIn(stream, ver);
        servRs232Cfg::cfg_rs232_serialIn(stream, ver);
        servI2cCfg::cfg_i2c_serialIn(stream, ver);
        servSpiCfg::cfg_spi_serialIn(stream, ver);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}
void servSearch::rst()
{
    bSearchOnOff          = false;
    bMarkTabelOnOff       = false;
    enSearchType          = SEARCH_TYPE_EDGE;
    mNavigationEvent      = 0;

    m_TimeMode            = Acquire_YT;
    m_bZoom               = false;

    setCurrSource(chan1);

    servEdgeCfg::cfg_edge_init();
    servPulseCfg::cfg_pulse_init();
    servRuntCfg::cfg_runt_init();
    servSlopeCfg::cfg_slope_init();
    servRs232Cfg::cfg_rs232_init();
    servI2cCfg::cfg_i2c_init();
    servSpiCfg::cfg_spi_init();

//    initLevelEngine();
//    applyThreshold();
//    onHoriChange();
//    onRunStopChange();
}

int servSearch::startup()
{
    initLevelEngine();
    applyThreshold();
    onHoriChange();
    onRunStopChange();
    return async(MSG_SEARCH_EN,bSearchOnOff);;
}

void servSearch::registerSpy()
{
    spyOn(serv_name_hori, MSG_HOR_TIME_MODE);
    spyOn(serv_name_hori, MSG_HORI_MAIN_SCALE);
    spyOn(serv_name_hori, MSG_HORI_MAIN_OFFSET);
    spyOn(serv_name_hori, MSG_HORI_ZOOM_SCALE);
    spyOn(serv_name_hori, MSG_HORI_ZOOM_OFFSET);
    spyOn(serv_name_hori, MSG_HOR_ACQ_MEM_DEPTH);

    spyOn(serv_name_hori, MSG_HORIZONTAL_RUN);
    spyOn(serv_name_hori, MSG_HOR_ZOOM_ON   );

    spyOn(serv_name_display, MSG_DISPLAY_CLEAR   );


    spyOn( serv_name_ch1, MSG_CHAN_ON_OFF);
    spyOn( serv_name_ch2, MSG_CHAN_ON_OFF);
    spyOn( serv_name_ch3, MSG_CHAN_ON_OFF);
    spyOn( serv_name_ch4, MSG_CHAN_ON_OFF);


    spyOn( serv_name_ch1, MSG_CHAN_SCALE );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE );

    spyOn( serv_name_ch1, MSG_CHAN_OFFSET );
    spyOn( serv_name_ch2, MSG_CHAN_OFFSET );
    spyOn( serv_name_ch3, MSG_CHAN_OFFSET );
    spyOn( serv_name_ch4, MSG_CHAN_OFFSET );

    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Active , CMD_TRIGGER_ALL_SPY_LICENSE);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Exp    , CMD_TRIGGER_ALL_SPY_LICENSE);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Invalid, CMD_TRIGGER_ALL_SPY_LICENSE);

}

DsoErr servSearch::apply()
{
    switch (enSearchType){
    case SEARCH_TYPE_EDGE:
        return postEngine( ENGINE_SEARCH_EDGE_CFG,  &edgeCfg);
    case SEARCH_TYPE_PULSE:
        return postEngine( ENGINE_SEARCH_PULSE_CFG, &pulseCfg);
    case SEARCH_TYPE_RUNT:
        return postEngine( ENGINE_SEARCH_RUNT_CFG,  &runtCfg);;
    case SEARCH_TYPE_SLOPE:
        return postEngine( ENGINE_SEARCH_SLOPE_CFG, &slopeCfg);
    case SEARCH_TYPE_RS232:
        return postEngine( ENGINE_SEARCH_UART_CFG,  &rs232Cfg);
    case SEARCH_TYPE_IIC:
        return postEngine( ENGINE_SEARCH_I2C_CFG,   &i2cCfg);
    case SEARCH_TYPE_SPI:
        return postEngine( ENGINE_SEARCH_SPI_CFG,   &spiCfg);
    default:
        return ERR_NONE;
    }
}

DsoErr servSearch::setActive(int active)
{
    return serviceExecutor::setActive(active);
}

DsoErr servSearch::setdeActive(int para)
{
    return serviceExecutor::setdeActive(para);
}

bool servSearch::actionFilter(const struServAction &action)
{
    return serviceExecutor::actionFilter(action);
}

bool servSearch::keyTranslate(int key, int &msg, int &controlAction)
{
    return serviceExecutor::keyTranslate(key, msg, controlAction);
}

DsoErr servSearch::setSearchOnOff(bool b)
{
    bSearchOnOff = b;

    mUiAttr.setEnable(MSG_SEARCH_TYPE,         b);
    mUiAttr.setEnable(MSG_SEARCH_MARK_TABEL_EN,b);
    mUiAttr.setEnable(MSG_SEARCH_MORE,         b);

    mUiAttr.setEnable(MSG_SEARCH_NAVIGATION_EVENT,b);

    mUiAttr.setEnable(MSG_SEARCH_THRE_ONE,   b);
    mUiAttr.setEnable(MSG_SEARCH_THRE_DOUBLE,   b);
    mUiAttr.setEnable(MSG_SEARCH_THRE_IIC,   b);
    mUiAttr.setEnable(MSG_SEARCH_THRE_SPI,   b);

    mUiAttr.setEnable(MSG_SEARCH_EDGE_SETUP,   b);
    mUiAttr.setEnable(MSG_SEARCH_PULSE_SETUP,  b);
    mUiAttr.setEnable(MSG_SEARCH_RUNT_SETUP ,  b);
    mUiAttr.setEnable(MSG_SEARCH_SLOPE_SETUP,  b);
    mUiAttr.setEnable(MSG_SEARCH_RS232_SETUP,  b);

    if(b)
    {
        startTimer(SEARCH_TIMER_TMO, SEARCH_TIMER_ID, timer_repeat);
    }
    else
    {
        stopTimer(SEARCH_TIMER_ID);
    }

    setuiChange(CMD_CODE_CHANGE);

    quint32 nEnab = b?SEARCH_ENAB_MAIN_ON:SEARCH_ENAB_MAIN_OFF;
    return postEngine(ENGINE_SEARCH_EN, nEnab);
}

bool servSearch::getSearchOnOff()
{
    return bSearchOnOff;
}

DsoErr servSearch::setMarkTabelOnOff(bool b)
{
    bMarkTabelOnOff = b;
    return ERR_NONE;
}

bool servSearch::getMarkTabelOnOff()
{
    return bMarkTabelOnOff;
}

DsoErr servSearch::setSearchType(int enType)
{
    enSearchType = (enumSearchType)enType;

    setuiChange(CMD_CODE_CHANGE);
    return apply();
}

int servSearch::getSearchType()
{
    return enSearchType;
}






DsoErr servSearch::setEdgeSource(int ch)
{
    setCurrSource((Chan)ch);
    return cfg_edge_setCh(ch);
}

int servSearch::getEdgeSource()
{
    return cfg_edge_getCh();
}

DsoErr servSearch::setEdgeSlope(int slope)
{
    return cfg_edge_setWhen(slope);
}

int servSearch::getEdgeSlope()
{
    return cfg_edge_getWhen();
}

DsoErr servSearch::setPulseSource(int ch)
{
    setCurrSource((Chan)ch);
    return cfg_pulse_setSource(ch);
}

int servSearch::getPulseSource()
{
    return cfg_pulse_getSource();
}

DsoErr servSearch::setPulsePolarty(bool pol)
{
    return cfg_pulse_setPolarity(pol);
}

bool servSearch::getPulsePolarty()
{
    return cfg_pulse_getPolarity();
}

DsoErr servSearch::setPulseWhen(int when)
{
    EMoreThan w0 = (EMoreThan)getPulseWhen();

    DsoErr err = cfg_pulse_setCmp(when);
    checkTimeRange(SEARCH_TYPE_PULSE, T_Rule_None, w0);
    return err;
}

int servSearch::getPulseWhen()
{
    return cfg_pulse_getCmp();
}

DsoErr servSearch::setPulseLower(qint64 value)
{
    DsoErr err = cfg_pulse_setLower(value);
    checkTimeRange(SEARCH_TYPE_PULSE, T_Rule_L);
    return err;
}

qint64 servSearch::getPulseLower()
{
    qint64 value = cfg_pulse_getLower() ;
    return value;
}

DsoErr servSearch::setPulseUpper(qint64 value)
{
    DsoErr err = cfg_pulse_setUpper(value);
    checkTimeRange(SEARCH_TYPE_PULSE, T_Rule_H);
    return err;
}

qint64 servSearch::getPulseUpper()
{
    qint64 value = cfg_pulse_getUpper() ;
    return value;
}

void servSearch::getPulseTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    tL = cfg_pulse_getLower();
    tH = cfg_pulse_getUpper();
    w  = (EMoreThan)cfg_pulse_getCmp();
}

DsoErr servSearch::setPulseTimeRange(qint64 &tL, qint64 &tH)
{
    cfg_pulse_setLower(tL);
    return cfg_pulse_setUpper(tH);
}


DsoErr servSearch::setRuntPolarity(bool b)
{
    return cfg_runt_setPolarity(b);
}

bool servSearch::getRuntPolarity()
{
    return cfg_runt_getPolarity();
}

DsoErr servSearch::setRuntWhen(int w)
{
    EMoreThan w0 = (EMoreThan)getRuntWhen();
    DsoErr    err = cfg_runt_setWhen(w);

    checkTimeRange(SEARCH_TYPE_RUNT, T_Rule_None,w0);
    return err;
}

int servSearch::getRuntWhen()
{
    return cfg_runt_getWhen();
}

DsoErr servSearch::setRuntLower(qint64 t)
{
    cfg_runt_setLower(t);
    return checkTimeRange(SEARCH_TYPE_RUNT, T_Rule_L);
}

qint64 servSearch::getRuntLower()
{
    return cfg_runt_getLower();
}

DsoErr servSearch::setRuntUpper(qint64 t)
{
    cfg_runt_setUpper(t);
    return checkTimeRange(SEARCH_TYPE_RUNT, T_Rule_H);
}

qint64 servSearch::getRuntUpper()
{
    return cfg_runt_getUpper();
}

DsoErr servSearch::setRuntSource(int src)
{
    if( !(src >= chan1 && src <= all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }

    setCurrSource((Chan)src);
    return cfg_runt_setSource(src);
}

int servSearch::getRuntSource()
{
    return cfg_runt_getSource();
}

DsoErr servSearch::setSlopePolarty(int pol)
{
    return cfg_slope_setSlope(pol);
}

int servSearch::getSlopePolarty()
{
    return cfg_slope_getSlope();
}


DsoErr servSearch::setSlopeWhen(int w)
{
    EMoreThan w0 = (EMoreThan)getSlopeWhen();

    DsoErr err = cfg_slope_setCmp(w);

    checkTimeRange(SEARCH_TYPE_SLOPE, T_Rule_None,w0);
    return err;
}

int servSearch::getSlopeWhen()
{
    return cfg_slope_getCmp();
}

DsoErr servSearch::setSlopeLower(qint64 t)
{
    DsoErr err = cfg_slope_setLower(t);
    checkTimeRange(SEARCH_TYPE_SLOPE, T_Rule_L);
    return err;
}

qint64 servSearch::getSlopeLower()
{
    return cfg_slope_getLower();
}

DsoErr servSearch::setSlopeUpper(qint64 t)
{
    DsoErr err = cfg_slope_setUpper(t);
    checkTimeRange(SEARCH_TYPE_SLOPE, T_Rule_H);
    return err;
}

qint64 servSearch::getSlopeUpper()
{
    return cfg_slope_getUpper();
}

DsoErr servSearch::setSlopeSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }

    setCurrSource((Chan)src);
    return cfg_slope_setSource(src);
}

int servSearch::getSlopeSource()
{
    return cfg_slope_getSource();
}

bool servSearch::getRs232Polarity()
{
    return cfg_rs232_getPolarity();
}

DsoErr servSearch::setRs232Polarity(bool b)
{
    return cfg_rs232_setPolarity(b);
}

int servSearch::getRs232When()
{
    return cfg_rs232_getWhen();
}

DsoErr servSearch::setRs232When(int w)
{
    return cfg_rs232_setWhen(w);
}

int servSearch::getRs232BaudRate()
{
    setuiRange( 1, (int)2E7, 9600 );
    setuiPostStr("bps");
    return cfg_rs232_getBaudRate();
}

DsoErr servSearch::setRs232BaudRate(int baud)
{
    return cfg_rs232_setBaudRate( baud);
}

int servSearch::getRs232Parity()
{
    return cfg_rs232_getParity();
}

DsoErr servSearch::setRs232Parity(int pari)
{
    return  cfg_rs232_setParity(pari);
}

int servSearch::getRs232StopBits()
{
    return cfg_rs232_getStopBits();
}

DsoErr servSearch::setRs232StopBits(int bit)
{
    return cfg_rs232_setStopBits(bit);
}

int servSearch::getRs232DataBits()
{
    return cfg_rs232_getDataBits();
}

DsoErr servSearch::setRs232DataBits(int bit)
{
    return cfg_rs232_setDataBits(bit);
}

int servSearch::getRs232Data()
{
    int data = cfg_rs232_getData();

    setuiRange(0, 255, 0);
    setuiOutStr(QString("%1(0x%2)").arg(data).arg(data,0,16) );
    return data;
}

DsoErr servSearch::setRs232Data(int data)
{
    return cfg_rs232_setData(data);
}

int servSearch::getRs232Source()
{
    return cfg_rs232_getSource();
}

DsoErr servSearch::setRs232Source(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }

    setCurrSource((Chan)src);
    return cfg_rs232_setSource(src);
}
/*! ---------------------------------------------iic-----------------------------------*/
int servSearch::getI2cWhen()
{
    return cfg_get_i2c_When();
}

int servSearch::setI2cWhen(int s)
{
    DsoErr err = cfg_set_i2c_When(s);
    setuiChange(CMD_CODE_CHANGE);
    return err;
}

int servSearch::getI2cDir()
{
    return cfg_get_i2c_Dir();
}

int servSearch::setI2cDir(int b)
{
    return cfg_set_i2c_Dir(b);
}

int servSearch::getI2cAddrBits()
{
    return cfg_get_i2c_AddrBits();
}

int servSearch::setI2cAddrBits(int a)
{
  int max = 0;
  if (trig_i2c_addr7 == a) {
    max = 127;
  } else if (trig_i2c_addr8 == a) {
    max = 256;
  } else if (trig_i2c_addr10 == a) {
    max = 1023;
  } else {
    Q_ASSERT(false);
  }

  mUiAttr.setMaxVal(MSG_SEARCH_I2C_ADDRESS, max);
  async(MSG_SEARCH_I2C_ADDRESS, qMin(cfg_get_i2c_Address(), max));

  return cfg_set_i2c_AddrBits(a);
}

int servSearch::getI2cAddress()
{
     int addr = cfg_get_i2c_Address();

     setuiMinVal(0);
     setuiZVal( 0 );

     setuiFmt(fmt_int);
     setuiUnit(Unit_none);
     setuiBase(1, E_0);
     setuiOutStr(QString("%1(0x%2)").arg(addr).arg(addr,0,16) );

     return addr;
}

int servSearch::setI2cAddress(int a)
{
    return cfg_set_i2c_Address(a);
}
int servSearch::getI2cSCL()
{
    return cfg_get_i2c_SCL();
}

int servSearch::setI2cSCL(int src)
{
    return cfg_set_i2c_SCL(src);
}

int servSearch::getI2cSDA()
{
    return cfg_get_i2c_SDA();
}

int servSearch::setI2cSDA(int src)
{
    return cfg_set_i2c_SDA(src);
}

int servSearch::getI2cByteCount()
{
    setuiZVal(  1 );
    setuiMaxVal(5);
    setuiMinVal(1);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return cfg_get_i2c_data_width();
}

int servSearch::setI2cByteCount(int byt)
{
    setuiChange(CMD_CODE_CHANGE);
    return cfg_set_i2c_data_width(byt);
}

qint64 servSearch::getI2cData()
{
    return ERR_NONE;
}

int servSearch::setI2cData(qint64 /*data*/)
{
    return ERR_NONE;
}

/*! -----------------------------------spi-----------------------------------*/
bool servSearch::getSpiWhen()
{
    return cfg_get_spi_when();
}

int servSearch::setSpiWhen(bool b)
{
    return cfg_set_spi_when(b);
}

bool servSearch::getSpiCSMode()
{
    return cfg_get_spi_cs_mode();
}

int servSearch::setSpiCSMode(bool b)
{
    return cfg_set_spi_cs_mode(b);
}

int servSearch::getSpiSCL()
{
    return cfg_get_spi_scl();
}

int servSearch::setSpiSCL(int src)
{
    return cfg_set_spi_scl(src);
}

bool servSearch::getSpiEdge()
{
    return cfg_get_spi_edge();
}

int servSearch::setSpiEdge(bool b)
{
    return cfg_set_spi_edge(b);
}

int servSearch::getSpiSDA()
{
    return cfg_get_spi_sda();
}

int servSearch::setSpiSDA(int src)
{
    return cfg_set_spi_sda(src);
}

int servSearch::getSpiCS()
{
    return cfg_get_spi_cs();
}

int servSearch::setSpiCS(int src)
{
    return cfg_set_spi_cs(src);
}

int servSearch::getSpiBitCount()
{
    setuiZVal(  8 );
    setuiMaxVal(32);
    setuiMinVal(4);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);

    return cfg_get_spi_bitCount();
}

int servSearch::setSpiBitCount(int count)
{
    cfg_set_spi_bitCount(count);
    setuiChange(CMD_CODE_CHANGE);
    return ERR_NONE;
}

qint64 servSearch::getSpiTimeout()
{
    return cfg_get_spi_timeout();
}

int servSearch::setSpiTimeout(qint64 t)
{
    return cfg_set_spi_timeout(t);
}

qint64 servSearch::getSpiData()
{
    return ERR_NONE;
}

int servSearch::setSpiData(qint64 /*data*/)
{
    return ERR_NONE;
}

/*! ----------------------------------------------------------------------*/
DsoErr servSearch::setNavigationEvent(int event)
{
    EngineSearchData *p_data = &m_searchData.m_data;
    Q_ASSERT(p_data  != NULL);

    QList<qlonglong>   llTimeList;
    p_data->getViewTimeValue(llTimeList);

    mNavigationEvent = qBound(0, event, llTimeList.count()-1 );

    //!修改导航位置。
    if(m_runStop == Control_Stop)
    {
        qlonglong offs = llTimeList.at(mNavigationEvent);

        if(m_bZoom)
        {
            return serviceExecutor::post(serv_name_hori, MSG_HORI_ZOOM_OFFSET, offs );
        }
        else
        {
            return serviceExecutor::post(serv_name_hori, MSG_HORI_MAIN_OFFSET, offs );
        }
    }
    return ERR_NONE;
}

int servSearch::getNavigationEvent()
{
    setuiRange( 0, 999, 0 );
    setuiOutStr(QString("%1").arg( mNavigationEvent+1 ));
    return mNavigationEvent;
}


CSource *servSearch::getSourcePtr(Chan ch)
{
    if( (chan_none < ch) && (ch < all_trig_src) )
    {
        return m_pSources[ch];
    }
    return NULL;
}

CSource *servSearch::getCurrSourcePtr()
{
    Q_ASSERT(m_pCurrSource !=   NULL);
    return m_pCurrSource;
}

Chan servSearch::getCurrSource()
{
    return (Chan)m_pCurrSource->getChan();
}

DsoErr servSearch::setCurrSource(Chan ch)
{
    m_pCurrSource = getSourcePtr(ch);
    Q_ASSERT(m_pCurrSource !=   NULL);
    return ERR_NONE;
}


void servSearch::onHoriChange()
{
    //！更新参数
    updateDataCfg();

    //!XY模式关闭search
    if(Acquire_XY == m_TimeMode)
    {
        id_async(E_SERVICE_ID_SEARCH, MSG_SEARCH_EN, false);
    }
    mUiAttr.setEnable(MSG_SEARCH_EN, (Acquire_XY != m_TimeMode) );

    //! 更新界面显示
    setuiChange(search_dso::CMD_UPDATE_UI);
}

void servSearch::onVertChange()
{
    applyThreshold();
}

void servSearch::onRunStopChange()
{
    int st;
    query( serv_name_hori, MSG_HORIZONTAL_RUN, st );

    m_runStop = (ControlAction)st;
}

void servSearch::onDisplayClear()
{
    EngineSearchData &engData = m_searchData.m_data;
    postEngine(ENGINE_SEARCH_CLEAR_DATA, &engData);
}


pointer servSearch::getsearchData()
{
    return &m_searchData;
}

DsoErr servSearch::updateDataCfg()
{
    //!获取参数
    int st = Control_Stop;
    query( serv_name_hori, MSG_HORIZONTAL_RUN, st );
    m_runStop = (ControlAction)st;

    m_searchData.m_HoriAttr     = getHoriAttr(chan1, horizontal_view_main);
    m_searchData.m_ZoomHoriAttr = getHoriAttr(chan1, horizontal_view_zoom);

    int nMode = Acquire_YT;
    serviceExecutor::query( serv_name_hori, MSG_HOR_TIME_MODE, nMode);
    m_TimeMode = (AcquireTimemode)nMode;

    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON, m_bZoom);

    qlonglong timScale = 0;
    serviceExecutor::query( serv_name_hori, MSG_HORI_MAIN_SCALE, timScale);
    m_timeOut_ms = qMax( (timScale/time_ms(1))*15, (qlonglong)100);
    //qDebug()<<__FILE__<<__LINE__<<"time out ms:"<<m_timeOut_ms;

    EngineSearchData &engData = m_searchData.m_data;
    //!设置参数
    qlonglong tInc = 1;
    qlonglong t0   = 0;
    tInc = m_searchData.m_HoriAttr.tInc;
    t0   = (-m_searchData.m_HoriAttr.gnd)*tInc;
    engData.setViewAttr(t0, tInc);

    tInc = m_searchData.m_ZoomHoriAttr.tInc;
    t0   = (-m_searchData.m_ZoomHoriAttr.gnd)*tInc;
    engData.setZoomViewAttr(t0, tInc);

    engData.setTimeMode(m_TimeMode);
    engData.setZoomEnable(m_bZoom);

    engData.setStop( (Control_Stop == m_runStop)? true : false );

    return ERR_NONE;
}


DsoErr servSearch::updateData()
{
    if( m_TimeMode == Acquire_XY )
    {
        return ERR_NONE;
    }

    DsoErr err = ERR_NONE;
    //!更新参数
    err = updateDataCfg();

    EngineSearchData &engData = m_searchData.m_data;

    do {
        queryEngine(qENGINE_SEARCH_REQ_DATA, &engData);

        if( bSearchOnOff && (!engData.getDone()) )
        {
            QThread::msleep(10);
        }
        else
        {break;}


        m_timeOut_ms -= 10;

        if( m_timeOut_ms < 0)
        {
            engData.setDone(true);
            qDebug()<<__FILE__<<__LINE__<<"!!! wait done failure";
            return ERR_NONE;
        }
    }while(1);

    //！读取结果
    err = queryEngine(qENGINE_SEARCH_GET_DATA, &engData);

    //! 将app中的格式化过程放在线程中完成
    engData.updateViewData();

    //! [dba 2018-03-25, bug:2328 ]
    //! defer会给服务不停的发消息导致系统保存设置定时器不停复位。
    setuiChange(search_dso::CMD_UPDATE_UI);

    return err;
}


void servSearch::onTimeout(int id)
{
    if(SEARCH_TIMER_ID != id)
    {
        return ;
    }

    if (!m_pUpdateWorker->isRunning() )
    {
        //stopTimer(SEARCH_TIMER_ID);
    }
    m_pUpdateWorker->start();
}

