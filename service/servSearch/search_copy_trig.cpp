#include "servsearch.h"

//!copy trig 表
const _copy_search_trig servSearch::stp_search_trig[]={
    {SEARCH_TYPE_EDGE,  serv_name_trigger_edge,  CMD_TRIGGER_EDGE_CFG,
     &servEdgeCfg::cfg_edge_getCfg,   &servEdgeCfg::cfg_edge_setCfg},

    {SEARCH_TYPE_PULSE, serv_name_trigger_pulse, CMD_TRIGGER_PULSE_CFG,
     &servPulseCfg::cfg_pulse_getCfg, &servPulseCfg::cfg_pulse_setCfg},

    {SEARCH_TYPE_RUNT,  serv_name_trigger_runt,  CMD_TRIGGER_RUNT_CFG,
     &servRuntCfg::cfg_runt_getCfg,   &servRuntCfg::cfg_runt_setCfg},

    {SEARCH_TYPE_SLOPE, serv_name_trigger_slope, CMD_TRIGGER_SLOPE_CFG,
     &servSlopeCfg::cfg_slope_getCfg, &servSlopeCfg::cfg_slope_setCfg},

    {SEARCH_TYPE_RS232, serv_name_trigger_rs232, CMD_TRIGGER_RS232_CFG,
     &servRs232Cfg::cfg_rs232_getCfg, &servRs232Cfg::cfg_rs232_setCfg},

    {SEARCH_TYPE_IIC,   serv_name_trigger_i2c,   CMD_TRIGGER_I2S_CFG,
     &servI2cCfg::cfg_i2c_getCfg,     &servI2cCfg::cfg_i2c_setCfg},

    {SEARCH_TYPE_SPI,   serv_name_trigger_spi,   CMD_TRIGGER_SPI_CFG,
     &servSpiCfg::cfg_spi_getCfg,     &servSpiCfg::cfg_spi_setCfg},
};

DsoErr servSearch::copyToTrig()
{
    DsoErr err = ERR_NONE;
    //!cfg参数
    for(int i = 0; i < array_count(stp_search_trig); i++)
    {
        if(stp_search_trig[i].serachType == enSearchType)
        {

            err =  serviceExecutor::post( stp_search_trig[i].trigName,
                                          stp_search_trig[i].cfgCmd ,
                                          (this->*stp_search_trig[i].fGetCfg)() );
            if(ERR_NONE != err)
            {
                return err;
            }

            break;
        }
    }

    //! 同步个别需要单独配置的参数
    switch (enSearchType) {
    case SEARCH_TYPE_RS232:
        post( serv_name_trigger_rs232, MSG_TRIGGER_RS232_WHEN, getRs232When());
    default:
        break;
    }

    //!阈值电平
    CSource *pSrc = NULL;
    Chan     ch   = chan1;
    switch (enSearchType) {
    case SEARCH_TYPE_EDGE:
    case SEARCH_TYPE_PULSE:
    case SEARCH_TYPE_RUNT:
    case SEARCH_TYPE_SLOPE:
    case SEARCH_TYPE_RS232:
        pSrc   = getCurrSourcePtr();
        Q_ASSERT(NULL != pSrc );
        pSrc->levelCopy(CSource::trig_source, CSource::search_source);

        break;

    case SEARCH_TYPE_IIC:
        ch   = (Chan)getI2cSCL();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::trig_source, CSource::search_source);

        ch   = (Chan)getI2cSDA();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::trig_source, CSource::search_source);
        break;

    case SEARCH_TYPE_SPI:
        ch   = (Chan)getSpiSCL();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::trig_source, CSource::search_source);

        ch   = (Chan)getSpiSDA();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::trig_source, CSource::search_source);

        ch   = (Chan)getSpiCS();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::trig_source, CSource::search_source);
        break;

    default:
        return ERR_TARGET_INVALID_CFG;
        break;
    }

    return serviceExecutor::post( serv_name_trigger,
                                  MSG_TRIGGER_APPLY_LEVEL,0);
}

//!阈值电平
DsoErr servSearch::copyFormTrig()
{
    DsoErr err = ERR_NONE;
    //!cfg参数
    for(int i = 0; i < array_count(stp_search_trig); i++)
    {
        if(stp_search_trig[i].serachType == enSearchType)
        {
            int   curCh = chan1;
            serviceExecutor::query( stp_search_trig[i].trigName, MSG_TRIGGER_SOURCE, curCh);

            bool  bChOn = false;
            serviceExecutor::query( getServiceName( curCh - chan1 + E_SERVICE_ID_CH1 ),
                                    MSG_CHAN_ON_OFF, bChOn);

            if( (!bChOn) || (curCh<chan1) || (curCh > chan4) )
            {
                return ERR_TARGET_INVALID_CFG;
            }

            pointer p = NULL;
            err = serviceExecutor::query( stp_search_trig[i].trigName,
                                          stp_search_trig[i].cfgCmd ,
                                          p );
            Q_ASSERT(NULL != p);
            if(ERR_NONE != err) return err;

            err = (this->*stp_search_trig[i].fSetCfg)(p);
            if(ERR_NONE != err) return err;
        }
    }

    //! 同步个别需要单独配置的参数
    if( SEARCH_TYPE_RS232 == enSearchType)
    {
        int when = rs232_when_start;
        query( serv_name_trigger_rs232, MSG_TRIGGER_RS232_WHEN, when);
        ssync(MSG_SEARCH_RS232_WHEN,   when);
    }

    //!同步信源
    switch (enSearchType) {
    case SEARCH_TYPE_EDGE:
        ssync(MSG_SEARCH_EDGE_SOURCE, getEdgeSource());
        break;
    case SEARCH_TYPE_PULSE:
        ssync(MSG_SEARCH_PULSE_SOURCE, getPulseSource());
        break;
    case SEARCH_TYPE_RUNT:
        ssync(MSG_SEARCH_RUNT_SOURCE,  getRuntSource());
        break;
    case SEARCH_TYPE_SLOPE:
        ssync(MSG_SEARCH_SLOPE_SOURCE, getSlopeSource());
        break;
    case SEARCH_TYPE_RS232:
        ssync(MSG_SEARCH_RS232_SOURCE, getRs232Source());
        break;
    case SEARCH_TYPE_IIC:
        ssync(MSG_SEARCH_I2C_SCL, getI2cSCL());
        ssync(MSG_SEARCH_I2C_SDA, getI2cSDA());
        break;
    case SEARCH_TYPE_SPI:
        ssync(MSG_SEARCH_SPI_SCL, getSpiSCL());
        ssync(MSG_SEARCH_SPI_SDA, getSpiSDA());
        ssync(MSG_SEARCH_SPI_CS,  getSpiCS());
        break;

    default:
        return ERR_TARGET_INVALID_CFG;
        break;
    }

    //!阈值电平
    CSource *pSrc = NULL;
    Chan     ch   = chan1;
    switch (enSearchType) {
    case SEARCH_TYPE_EDGE:
    case SEARCH_TYPE_PULSE:
    case SEARCH_TYPE_RUNT:
    case SEARCH_TYPE_SLOPE:
    case SEARCH_TYPE_RS232:
        pSrc   = getCurrSourcePtr();
        Q_ASSERT(NULL != pSrc );
        pSrc->levelCopy(CSource::search_source , CSource::trig_source);

        break;

    case SEARCH_TYPE_IIC:
        ch   = (Chan)getI2cSCL();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::search_source , CSource::trig_source);

        ch   = (Chan)getI2cSDA();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::search_source , CSource::trig_source);
        break;

    case SEARCH_TYPE_SPI:
        ch   = (Chan)getSpiSCL();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::search_source , CSource::trig_source);

        ch   = (Chan)getSpiSDA();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::search_source , CSource::trig_source);

        ch   = (Chan)getSpiCS();
        pSrc = getSourcePtr(ch);
        Q_ASSERT(NULL != pSrc);
        pSrc->levelCopy(CSource::search_source , CSource::trig_source);
        break;

    default:
        return ERR_TARGET_INVALID_CFG;
        break;
    }

    return initLevelEngine();
}
