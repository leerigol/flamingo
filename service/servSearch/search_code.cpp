#include "servsearch.h"
#include "../../service/servtrig/trigcode.h"

int servSearch::getCodeCurBit()
{
    updateCodePtr();
    mUiAttr.setZVal(CMD_SEARCH_CODE_CURR_BIT  , SEARCH_BIT_Z);
    return m_curBit;
}

int servSearch::setCodeCurBit(int bit)
{
    updateCodePtr();

    //!按下 切换当前位的值
    if(SEARCH_BIT_Z == bit)
    {
        int    curValue = getCodeValue();
        bool   binHex   = true;

        if( m_curBit >= m_bitMax )
        {
            binHex = false;
        }
        return setCodeValue(trigCode::get_z_value(curValue,binHex));
    }
    else //!旋转 切换当前位置
    {
        int b = bit  + m_curBit;
        b = qMax(b, 0);
        b = qMin(b, m_bitbytMax-1 );

        m_curBit = b;

        setuiChange(CMD_CODE_CHANGE);
        return ERR_NONE;
    }
}

int servSearch::getCodeValue()
{
    updateCodePtr();

    if(m_bCodeBit)
    {
        return trigCode::getBitValue(*pBitCode,
                                     *pBitCodeMask,
                                      m_curBit,
                                      m_bitMax);
    }
    else
    {
       return  trigCode::getBitValue(*pBytCode,
                                     *pBytCodeMask,
                                      m_curBit,
                                      m_bytMax);
    }
}

int servSearch::setCodeValue(int value)
{
    updateCodePtr();

    if(m_bCodeBit)
    {
        trigCode::setBitValue(*pBitCode,
                              *pBitCodeMask,
                               m_curBit,
                               value,
                               m_bitMax);
    }
    else
    {
        trigCode::setBitValue(*pBytCode,
                              *pBytCodeMask,
                               m_curBit,
                               value,
                               m_bytMax);
    }

    setuiChange(CMD_CODE_CHANGE);
    return apply();
}

int servSearch::setCodeAll(int value)
{
    updateCodePtr();

    if(m_bCodeBit)
    {
        trigCode::setAllValue(   *pBitCode,
                                 *pBitCodeMask,
                                 m_curBit,
                                 value,
                                 m_bitMax);
    }
    else
    {
        trigCode::setAllValue(   *pBytCode,
                                 *pBytCodeMask,
                                 m_curBit,
                                 value,
                                 m_bytMax);
    }

    return apply();
}

void servSearch::getCodeArg(CArgument &arg)
{
    updateCodePtr();

    arg.clear();
    if(m_bCodeBit)
    {
        for(int i = 0; i < m_bitMax; i++)
        {
            int value = Trigger_Code_X;
            value = trigCode::getBitValue( *pBitCode,
                                           *pBitCodeMask,
                                           i,
                                           m_bitMax);
            arg.append(value);
        }
    }
    else
    {
        for(int i = 0; i < m_bytMax*8; i++)
        {
            int value = Trigger_Code_X;
            value = trigCode::getBitValue( *pBytCode,
                                           *pBytCodeMask,
                                           i,
                                           m_bytMax);
            arg.append(value);
        }
    }
}


bool servSearch::updateCodePtr()
{
    if(SEARCH_TYPE_IIC == enSearchType)
    {
        pBytCode     = &(cfg_get_i2c_Data_Min());
        pBytCodeMask = &(cfg_get_i2c_Data_Mask());

        m_bytMax     = cfg_get_i2c_data_width();
        m_bCodeBit   = false;
    }
    else if(SEARCH_TYPE_SPI == enSearchType)
    {

        pBitCode     = &(cfg_get_spi_data_Min());
        pBitCodeMask = &(cfg_get_spi_data_Mask());

        m_bitMax     = cfg_get_spi_bitCount();
        m_bCodeBit   = true;
    }
    else
    {
        Q_ASSERT(false);
       return false;
    }

    //! 一个bit占一个字符，一个byt占10个字符。
    if(m_bCodeBit)
    {
        m_bitbytMax = m_bitMax + (m_bitMax+3)/4;

    }
    else
    {
        m_bitbytMax = m_bytMax*8 + m_bytMax*2;
    }

    return true;
}

