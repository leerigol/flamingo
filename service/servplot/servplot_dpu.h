#ifndef SERVPLOT_DPU
#define SERVPLOT_DPU

#include "servplot.h"

enum
{
// set layer index,
    DPU_SET_LAYER_ID   =	0x0F000000,
    DPU_GET_LAYER_ID   ,

// set trace
    DPU_SET_TRACE_MASK      ,
    DPU_SET_TRACE_COLOR     ,
    DPU_SET_TRACE_DATA      ,
    DPU_SET_TRACE_VECTOR    ,

    DPU_SET_LAYER_RST 		,
    DPU_SET_LAYER_ALPHA		,
    DPU_SET_LAYER_OPEN		,
    //SET the x and y position of wave layer
    DPU_SET_WAVE_XY         ,
    DPU_SET_LAYER_CLOSE     ,
    DPU_SET_HDMI            ,
    DPU_SCR_PRINT           ,
    DPU_SEL_SIG
};

class CDPU
{
public:
    CDPU();
    ~CDPU();

public:
    bool    initialize( void );
    void    drawTrace(plotContext* pctx);
    void    clearTrace(int id);

    static void*   getFrameBuffer( int layer = 0 );
    void    setLayerAlpha(unsigned char);
    void    openLayer(int);
    void    closeLayer(int);
    void    setWaveXY(int);
    void    configHDMI(int);
    void    setSigFormat(int);

private:
    void    setTraceData(int upScr, int belowScr ,DsoWfm* pWfm);
    void    getScrRange(PlotId id,int& up,int& below);
    int     getPhyChan(PlotId id);

    uchar*  m_pTraceBuffer;

    static int     m_nDevice;
    static void*   m_nFrameBuffer;
};

#endif // SERVPLOT_DPU

