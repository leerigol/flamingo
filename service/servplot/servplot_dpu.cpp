
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

#include "../../include/dsodbg.h"
#include "servplot_dpu.h"

int    CDPU::m_nDevice      = -1;
void*  CDPU::m_nFrameBuffer = NULL;

#define main_scr_below 144
#define zoom_scr_up    160

CDPU::CDPU()
{
}

CDPU::~CDPU()
{
    munmap((void*)m_nFrameBuffer, PLOT_SIZE);
    close(m_nDevice);

    delete []m_pTraceBuffer;
    m_pTraceBuffer = NULL;
}

bool CDPU::initialize()
{
    m_nDevice = open("/dev/fb0", O_RDWR);
    Q_ASSERT( m_nDevice >= 0 );
    if(m_nDevice < 0)
    {
        return  false;
    }

    m_pTraceBuffer = new uchar[ PLOT_WIDTH * 2 ];
    Q_ASSERT( m_pTraceBuffer != NULL);

    //! -- frame buffer
    ioctl(m_nDevice, DPU_SET_LAYER_ID, DPU_Layer_Print);
    m_nFrameBuffer = mmap(0,PLOT_SIZE, PROT_READ, MAP_PRIVATE, m_nDevice, 0);
    Q_ASSERT( m_nFrameBuffer != NULL);

    if(m_nFrameBuffer == NULL)
    {
        return false;
    }
    return true;
}

void* CDPU::getFrameBuffer( int )
{
    int ret = 0;
    ioctl(m_nDevice, DPU_SCR_PRINT, &ret);
    if( ret == 0 )
    {
        return m_nFrameBuffer;
    }
    else
    {
        return NULL;
    }
}

void CDPU::clearTrace(int id)
{
    unsigned int trace_chan = getPhyChan( (PlotId)id);
    unsigned int trace_mask = (trace_chan << 16 );
    ioctl(m_nDevice, DPU_SET_TRACE_MASK, trace_mask);
}

/*
    1、ref层默认在波形的下方
     2、math波形默认在波形的上方
    （1）4个math通道的显示叠加顺序可控制  not used
*/
void CDPU::drawTrace(plotContext* pctx)
{
    if( pctx->mPlotId >= PLOT_ID_REF1 && pctx->mPlotId <= PLOT_ID_MAIN_FFT4 )
    {
        unsigned int trace_chan = getPhyChan(pctx->mPlotId);

        unsigned int trace_mask = (trace_chan << 16 ) | 1;
        ioctl(m_nDevice, DPU_SET_TRACE_MASK, trace_mask);

        //! rgb888->rgb565
        unsigned short col = 0;
        {
            int R = pctx->mColor.red();
            int G = pctx->mColor.green();
            int B = pctx->mColor.blue();
            col = (R >> 3) << 11;
            col |= (G >> 2) << 5;
            col |= (B >> 3);
        }

        unsigned int trace_color = (trace_chan << 16) | col;
        ioctl(m_nDevice, DPU_SET_TRACE_COLOR, trace_color);

        //! attribute
        m_pTraceBuffer[0] = trace_chan & 0xff;

        if( pctx->m_pWfm)
        {
            int upScr = 0;
            int belowScr = wave_height;
            getScrRange(pctx->mPlotId,upScr,belowScr);
            setTraceData(upScr,belowScr,pctx->m_pWfm);
        }
        else
        {
            qDebug() << "trace data error";
        }
    }
}

void CDPU::setLayerAlpha(unsigned char alpha)
{
    ioctl(m_nDevice, DPU_SET_LAYER_ALPHA, alpha);
}

void CDPU::openLayer(int layer)
{
    ioctl(m_nDevice, DPU_SET_LAYER_OPEN, layer);
}

void CDPU::closeLayer(int layer)
{
    ioctl(m_nDevice, DPU_SET_LAYER_CLOSE, layer);
}

void CDPU::setWaveXY(int xy)
{
    ioctl(m_nDevice, DPU_SET_WAVE_XY, xy);
}

void CDPU::configHDMI(int b)
{
    ioctl(m_nDevice, DPU_SET_HDMI, b);
}

void CDPU::setSigFormat(int s)
{
    ioctl(m_nDevice, DPU_SEL_SIG, s);
}

void CDPU::getScrRange(PlotId id, int &up, int &below)
{
    if(id >= PLOT_ID_REF1 && id <= PLOT_ID_MATH4)
    {
        up = 0;
        below = wave_height;
    }
    else if(id >= PLOT_ID_MAIN_REF1 && id <= PLOT_ID_MAIN_MATH4)
    {
        up = 0;
        below = main_scr_below;
    }
    else if(id >= PLOT_ID_HALF_FFT1 && id <= PLOT_ID_HALF_FFT4)
    {
        up = 0;
        below = 240;
    }
    else if(id >= PLOT_ID_ZOOM_REF1 && id <= PLOT_ID_ZOOM_MATH4)
    {
        up = zoom_scr_up;
        below = wave_height;
    }
    else if(id >= PLOT_ID_MAIN_FFT1 && id <= PLOT_ID_MAIN_FFT4)
    {
        up = 240;
        below = wave_height;
    }
}

int CDPU::getPhyChan(PlotId id)
{
    /* 0~9  main ref */
    if( id >= PLOT_ID_REF1 && id <= PLOT_ID_REF10 )
    {
        return id - PLOT_ID_REF1;
    }

    if( id>= PLOT_ID_MAIN_REF1 && id <= PLOT_ID_MAIN_REF10 )
    {
         return id - PLOT_ID_MAIN_REF1;
    }

    /* 9~19 zoom ref */
    if( id>= PLOT_ID_ZOOM_REF1 && id <= PLOT_ID_ZOOM_REF10 )
    {
         return id - PLOT_ID_ZOOM_REF1 + 10;
    }

    /* 20~23  main math */
    if( id >= PLOT_ID_MATH1 && id <= PLOT_ID_MATH4 )
    {
        return id - PLOT_ID_MATH1 + 20;
    }

    if( id>= PLOT_ID_MAIN_MATH1 && id <= PLOT_ID_MAIN_MATH4 )
    {
         return id - PLOT_ID_MAIN_MATH1 + 20;
    }

    /* 24~27 zoom math */
    if( id>= PLOT_ID_ZOOM_MATH1 && id <= PLOT_ID_ZOOM_MATH4 )
    {
         return id - PLOT_ID_ZOOM_MATH1 + 24;
    }

    return 0;
}

void CDPU::setTraceData(int upScr, int belowScr ,DsoWfm* pWfm)
{
    int high = lround((wave_height - upScr)/2.4);
    int low = lround((wave_height - belowScr)/2.4);

    int size = pWfm->size();
    int start = pWfm->start();

//    LOG_DBG()<<start<<size;

    //! check range

    if ( start >= 0  && size >= 0 &&
         (start + size) <= trace_width )
    {
    }
    else
    {
        LOG_DBG()<<start<<size;
        return;
    }

    //! canvas
    uchar *pCanvas = m_pTraceBuffer + 1;
    //! -- clear head
    int i = 0;
    for( i = 0; i < start && i < trace_width;i++ )
    {
        pCanvas[i] = 0XFF;
    }

    //! -- fill middle
    for( i = start; i<(start + size) && i < trace_width; i++)
    {
        if( pWfm->at(i) >= 228 )
        {
            pCanvas[i] = high;
        }
        else if( pWfm->at(i) > 28 )
        {
            pCanvas[i] =  (pWfm->at(i) - 28)*(high - low)/200 + low;
        }
        else
        {
            pCanvas[i] = low;
        }
    }

    //! -- clear tail
    for ( ; i < trace_width; i++ )
    {
        pCanvas[i] = 0XFF;
    }

    //! 等于0时可以清空界面
    if(size >= 0)
    {
        ioctl(m_nDevice, DPU_SET_TRACE_DATA, &m_pTraceBuffer[0]);
    }
}
