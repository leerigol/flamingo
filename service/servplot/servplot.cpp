#include "../service_msg.h"

#include "servplot.h"
#include "servplot_dpu.h"

IMPLEMENT_CMD( serviceExecutor, servPlot  )
start_of_entry()
on_set_int_arg(     servPlot::cmd_plot,         &servPlot::setPlot ),
on_set_int_pointer( servPlot::cmd_plot_sync,    &servPlot::setPlotPointer ),
on_set_int_int(     servPlot::cmd_close_polt,   &servPlot::closePlot ),
on_get_pointer(     servPlot::cmd_get_polt,     &servPlot::getPlot ),
on_set_int_int(     servPlot::cmd_set_alpha,    &servPlot::setAlpha),

on_set_int_int(     servPlot::cmd_open_layer,   &servPlot::openLayer),
on_set_int_int(     servPlot::cmd_close_layer,  &servPlot::closeLayer),
on_set_int_int(     servPlot::cmd_config_hdmi,  &servPlot::configHDMI),
on_set_int_int(     servPlot::cmd_select_signal,&servPlot::selectSignal),

on_set_void_void(   CMD_SERVICE_INIT,           &servPlot::init),
on_set_void_void(   CMD_SERVICE_RST,            &servPlot::rst),
on_set_int_void(    CMD_SERVICE_STARTUP,        &servPlot::startup),
end_of_entry()

void servPlot::init()
{
#ifndef _SIMULATE
    m_pDPU = new CDPU();
    Q_ASSERT(m_pDPU != NULL);

    if(m_pDPU)
    {
        m_pDPU->initialize();
    }
#endif

}

void servPlot::rst()
{
#ifndef _SIMULATE
      for(int id=PLOT_ID_REF1; id<=PLOT_ID_MASK_DOWN; id++)
      {
         m_pDPU->clearTrace(id);
      }
#endif
}

int servPlot::startup()
{
    return 0;
}

servPlot::servPlot( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    setMsgAttr( servPlot::cmd_plot );
    setMsgAttr( servPlot::cmd_plot_sync );
    setMsgAttr( servPlot::cmd_close_polt );
}

/*!
 * \brief servPlot::setPlot
 * \param pObj
 * \return
 * 完成后释放资源 pObj
 */
DsoErr servPlot::setPlot( CArgument *pArg )
{
    Q_ASSERT( pArg != NULL );

    CObj *pObj;
    pArg->getVal( pObj );
    Q_ASSERT( NULL != pObj );

    plotContext *pContext;
    pContext = dynamic_cast<plotContext *>(pObj);
    Q_ASSERT( pContext != NULL );

    setPlotPointer( pContext );

    return ERR_NONE;
}

/*!
 * \brief setPlotPointer
 * \param pContext
 * \return
 * 资源由客户端管理
 */
DsoErr servPlot::setPlotPointer( plotContext *pContext )
{
    Q_ASSERT( pContext != NULL );

#ifndef _SIMULATE
    pContext->enter();

    m_pDPU->drawTrace(pContext);

    pContext->exit();
#else
    sigPlot( (int)pContext->mPlotId, *pContext );
#endif
    return ERR_NONE;
}

void* servPlot::getPlot()
{
#ifndef _SIMULATE
    return m_pDPU->getFrameBuffer();
#else
    return NULL;
#endif
}

DsoErr servPlot::setAlpha(int a)
{
    if(a < 0 || a > 255)
    {
        return ERR_INVALID_INPUT;
    }
#ifndef _SIMULATE
    m_pDPU->setLayerAlpha(a);
#endif
    return ERR_NONE;
}

DsoErr servPlot::openLayer(int v)
{
#ifndef _SIMULATE
    m_pDPU->openLayer(v);
#endif
    return ERR_NONE;
}

DsoErr servPlot::closeLayer(int v)
{
#ifndef _SIMULATE
    m_pDPU->closeLayer(v );
#endif
    return ERR_NONE;
}

DsoErr servPlot::setWaveXY(int xy)
{
#ifndef _SIMULATE
    m_pDPU->setWaveXY(xy);
#endif
    return ERR_NONE;
}

DsoErr servPlot::selectSignal(int p)
{
#ifndef _SIMULATE
    m_pDPU->setSigFormat(p);
#endif
    return ERR_NONE;

}

DsoErr servPlot::configHDMI(int p)
{
#ifndef _SIMULATE
    m_pDPU->configHDMI(p);
#endif
    return ERR_NONE;
}

DsoErr servPlot::closePlot(int id)
{
#ifndef _SIMULATE
    m_pDPU->clearTrace(id);
#else
    sigClosePlot( id );
#endif

    return ERR_NONE;
}

//! -- plotcontext
void plotContext::set( QColor color,
                       PlotId id,
                       DsoWfm *pWfm,
                       QSemaphore *pSema )
{
    mColor = color;
    mPlotId = id;
    m_pWfm = pWfm;

    m_pSema = pSema;
}

void plotContext::enter()
{
}
void plotContext::exit()
{
    m_pSema->release(1);
}
