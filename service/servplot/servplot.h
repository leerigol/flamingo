#ifndef SERVPLOT_H
#define SERVPLOT_H

#include "../../baseclass/iserial.h"
#include "../../baseclass/cargument.h"
#include "../../baseclass/dsowfm.h"
#include "../service.h"
#include "../service_name.h"
#include "../../include/dsocfg.h"

#include <QColor>

#define PLOT_WIDTH  screen_width
#define PLOT_HEIGHT screen_height
#define PLOT_BPP    2       /* Byte Per Pixel*/
#define PLOT_BPL    2048    /* Byte Per Line */
#define PLOT_SIZE   (PLOT_WIDTH*PLOT_HEIGHT*PLOT_BPP)

enum PlotId
{
    PLOT_ID_CH1 = 0,
    PLOT_ID_CH2,
    PLOT_ID_CH3,
    PLOT_ID_CH4,

/*--Channels drawed by DPU start---*/
    PLOT_ID_REF1,
     PLOT_ID_REF2,
     PLOT_ID_REF3,
     PLOT_ID_REF4,
     PLOT_ID_REF5,
     PLOT_ID_REF6,
     PLOT_ID_REF7,
     PLOT_ID_REF8,
     PLOT_ID_REF9,
     PLOT_ID_REF10,

     PLOT_ID_MATH1,
     PLOT_ID_MATH2,
     PLOT_ID_MATH3,
     PLOT_ID_MATH4,

     PLOT_ID_ZOOM_BEGIN,
     PLOT_ID_ZOOM_REF1 = PLOT_ID_ZOOM_BEGIN,
     PLOT_ID_ZOOM_REF2,
     PLOT_ID_ZOOM_REF3,
     PLOT_ID_ZOOM_REF4,
     PLOT_ID_ZOOM_REF5,
     PLOT_ID_ZOOM_REF6,
     PLOT_ID_ZOOM_REF7,
     PLOT_ID_ZOOM_REF8,
     PLOT_ID_ZOOM_REF9,
     PLOT_ID_ZOOM_REF10,

     PLOT_ID_ZOOM_MATH1,
     PLOT_ID_ZOOM_MATH2,
     PLOT_ID_ZOOM_MATH3,
     PLOT_ID_ZOOM_MATH4,

     PLOT_ID_MAIN_BEGIN,
     PLOT_ID_MAIN_REF1 = PLOT_ID_MAIN_BEGIN,
     PLOT_ID_MAIN_REF2,
     PLOT_ID_MAIN_REF3,
     PLOT_ID_MAIN_REF4,
     PLOT_ID_MAIN_REF5,
     PLOT_ID_MAIN_REF6,
     PLOT_ID_MAIN_REF7,
     PLOT_ID_MAIN_REF8,
     PLOT_ID_MAIN_REF9,
     PLOT_ID_MAIN_REF10,

     PLOT_ID_MAIN_MATH1,
     PLOT_ID_MAIN_MATH2,
     PLOT_ID_MAIN_MATH3,
     PLOT_ID_MAIN_MATH4,

     PLOT_ID_HALF_FFT1,
     PLOT_ID_HALF_FFT2,
     PLOT_ID_HALF_FFT3,
     PLOT_ID_HALF_FFT4,

     PLOT_ID_MAIN_FFT1,
     PLOT_ID_MAIN_FFT2,
     PLOT_ID_MAIN_FFT3,
     PLOT_ID_MAIN_FFT4,
/*--Channels drawed by DPU end---*/

    PLOT_ID_MASK_UPPER,
    PLOT_ID_MASK_DOWN,
};

/*!
 * \brief The plotContext struct
 * - 支持堆上分配空间
 * - 参数进入消息队列缓存
 * - 进行消息投递时需要创建新的对象
 */
struct plotContext : public CObj
{
    void set( QColor color,
              PlotId id,
              DsoWfm *pWfm,
              QSemaphore *pSema );

    void enter();
    void exit();

    QColor mColor;      /*!< color */
    PlotId mPlotId;     /*!< id */
    DsoWfm *m_pWfm;     /*!< data */
    QSemaphore *m_pSema;
};
Q_DECLARE_METATYPE( plotContext )

class CDPU;
enum
{
    DPU_Layer_Back,
    DPU_Layer_Wave,
    DPU_Layer_Eye,
    DPU_Layer_Fore,
    DPU_Layer_Print,
    DPU_Layer_Comp,
    DPU_Layer_All
};
class servPlot : public serviceExecutor
{
    Q_OBJECT

    DECLARE_CMD()

public:
    static QString singleName()
    {
        return serv_name_plot;
    }

public:
    enum enumCmd
    {
        cmd_none = 0,

        cmd_plot,       /*< 参数通过CObj进行传递，*/
        cmd_plot_sync,  /*< 同步绘制，不需要缓存绘制参数*/
        cmd_close_polt,

        cmd_get_polt,
        cmd_set_alpha,
        cmd_open_polt,
        cmd_wave_xy,
        cmd_open_layer,
        cmd_close_layer,
        cmd_config_hdmi,
        cmd_select_signal,
    };

public:
    servPlot( QString name, ServiceId eId = E_SERVICE_ID_PLOT );

    void   init();
    void   rst();
    int    startup();

public:
    DsoErr setPlot( CArgument *pArg );
    DsoErr setPlotPointer( plotContext *ptr );

    DsoErr closePlot( int id );
    void*  getPlot(void);

    DsoErr setAlpha(int);
    DsoErr openLayer(int);
    DsoErr closeLayer(int);
    DsoErr setWaveXY(int);


    DsoErr configHDMI(int);// high 16 bit: enable or not; low 16 bit: definition
    DsoErr selectSignal(int);

Q_SIGNALS:
    void sigPlot( int id, plotContext &context );
    void sigClosePlot( int id );

private:
    CDPU* m_pDPU;
};

#endif // SERVPLOT_H
