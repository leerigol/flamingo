#ifndef SERVMENUTEST_H
#define SERVMENUTEST_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../../baseclass/chelprequest.h"
#include "../service_msg.h"

#include "../servgui/cctrlprogress.h"
#include "../servdso/sysdso.h"
#include <QThread>

class testThread : public QThread
{
    Q_OBJECT
public:
    testThread( QObject *obj = NULL );

protected:
    virtual void run();
};

class servMenuTest : public serviceExecutor
{
    Q_OBJECT

protected:
    DECLARE_CMD()
public:
    enum cmdMsg
    {
        cmd_none = 0,
        cmd_thread_started,
        cmd_thread_ended,

        cmd_lcd_light,
        cmd_dso_fanspeed,
        cmd_beeper_short,

        cmd_cpld_ver,
        cmd_mb_ver,

        cmd_adc,
        cmd_sadc,
        cmd_pll5,
        cmd_pll4,

        cmd_relay,
        cmd_vga1,
        cmd_vga2,
        cmd_vga3,

        cmd_vga4,

        cmd_progress_cancel,

        cmd_set_arg,
        cmd_get_arg,

        cmd_get_argIO,
        cmd_get_argO,

        cmd_progress_cfg,

        cmd_msg_box_ok,

        cmd_intent_return,

        cmd_quick_key,

        cmd_snap_avg,
    };

public:
    servMenuTest( QString name, ServiceId eId = E_SERVICE_ID_NONE );
    virtual DsoErr start();
    virtual void registerSpy();
private:
    #include "./serv_menutest_var.h"

public:
    #include "./serv_menutest_decl.h"

    DsoErr  onTimeOut( int timerId );
    DsoErr  onSubEnter( int subId );
    DsoErr  onSubReturn( int subId );
    DsoErr  onFocusIn( int msgId );
    DsoErr  onMSG_GUI_MENU_SHOW( bool b );

    DsoErr startup();

public:
    DsoErr setBkLight( int light );
    DsoErr setFanSpeed( int speed );

    DsoErr setBeeper();

    quint16 getCpldVer();
    quint16 getMbVer();

    //! test
    DsoErr setAdc( int v );
    DsoErr setSAdc( int v );
    DsoErr setPll5( int v );
    DsoErr setPll4( int v );

    DsoErr setRelay( int v );
    DsoErr setVga1( int v );
    DsoErr setVga2( int v );
    DsoErr setVga3( int v );

    DsoErr setVga4( int v );

    DsoErr onProgCancel( int para );

    //! multi para test
    DsoErr onSetArg( CArgument &arg );
    int onGetArg( CArgument &arg );

    void onGetArgIO( CArgument &argI, CArgument &argO );
    void onGetArgO( CArgument &argO );

    DsoErr setProgressCfg( CtrlProgress *pCtrl );

    DsoErr onEnterActive( int arg );
    DsoErr onExitActive( int arg );

    DsoErr onIntent(CIntent *pIntent) ;
    DsoErr onIntentReturn( CArgument &arg );

    DsoErr onMsgBoxOk();
    DsoErr onQuickKey();

    void on_timescale_changed();
    void on_phy_key( CArgument &arg );
private:
    QString mStr;
    DsoErr setString( QString str );
    QString getString();

protected Q_SLOTS:
    void on_started();
    void on_finished();

private:
    void testSetArg();
    void testGetArg();
    void testIntent();
    void testProgress();
    void testNotify();

    void testPreCal();
    void testCalQuery();

    void testMisc( int id );
    void testTrigOut( int id );

    void test1_1g( bool b );

    void testChLabel( bool bVisible );
    void testHLine( bool bVisible );
    void testVLine( bool bVisible );

    DsoErr testHelpRequest( CHelpRequest *request );

    void testQueryMath();
    void testShowInfoXy();
    void testRecPlay( bool bRec );
    void testShowPrivate( );
    void testQueryTrace();
    void testAdcPhase();
    void testLogInfo();

    void testMask( bool b );
    void queryMask();

    void testWriteReg();
    void testReadReg();

    void testSnapAvg();
    void testChAdcCvt();
    void testDxPos();
    void testChMemory();

    void onBtn1();
    void onBtn2();
    void onBtn3();
    void onBtn4();

    void onBtn5();
    void onBtn6();
    void onBtn7();
private:
    QThread *m_pThread;
    QString m_Info;
    int mProgStep;

    testThread mTestThread;
};


#endif // SERVMENUTEST_H
