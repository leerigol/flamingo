#ifndef CMD_TABLE
#define CMD_TABLE

/*! \var servCh_CmdItem
*
* scpi命令表
*/
static struCmdItem servCh_CmdItem[]=
{
start_of_cmd_item()

    {":BEEPer", ":BEEP", 0, 0,
      servMenuTest::cmd_beeper_short, 0 },

    {":LIGHT", ":LIGH", 0, 0,
      servMenuTest::cmd_lcd_light, &intConverter },

    {":FAN", 0, 0, 0,
      servMenuTest::cmd_fan_speed, &intConverter },

    {":CPLD?", 0, 0, 0,
      servMenuTest::cmd_cpld_ver, 0, &numericFormater },

    {":MAIN?", "MAINBOARD?", 0, 0,
      servMenuTest::cmd_mb_ver, 0, &numericFormater },

    {":ADC", 0, 0, 0,
      servMenuTest::cmd_adc, &intConverter },

    {":SADC", 0, 0, 0,
      servMenuTest::cmd_sadc, &intConverter },

    {":PLL5", 0, 0, 0,
      servMenuTest::cmd_pll5, &intConverter },

    {":PLL4", 0, 0, 0,
      servMenuTest::cmd_pll4, &intConverter },

    {":REL", ":RELAY", 0, 0,
      servMenuTest::cmd_relay, &intConverter },

    {":VGA1", 0, 0, 0,
      servMenuTest::cmd_vga1, &intConverter },
    {":VGA2", 0, 0, 0,
      servMenuTest::cmd_vga2, &intConverter },
    {":VGA3", 0, 0, 0,
      servMenuTest::cmd_vga3, &intConverter },
    {":VGA4", 0, 0, 0,
      servMenuTest::cmd_vga4, &intConverter },

end_of_cmd_item()
};

/*! \var servCh_CmdItem
*
* scpi命令表根
*/
static struCmdItem top_CmdItem[]=
{
start_of_cmd_item()

    {":test", 0, 0, servCh_CmdItem },


end_of_cmd_item()
};

#endif // CMD_TABLE

