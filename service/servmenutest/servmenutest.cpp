#include "servmenutest.h"

#include "../servgui/servgui.h"     //! gui ctrls
#include "../service_name.h"
#include "../servdso/servdso.h"
#include "../../com/keyevent/rdsokey.h"

#include "../../service/servch/servch.h"

//#define dbg_out()   qDebug()<<__LINE__;
#define dbg_out()

IMPLEMENT_CMD( serviceExecutor, servMenuTest )
start_of_entry()

on_set_int_int( CMD_SERVICE_ACTIVE, &servMenuTest::setActive ),
//on_set_void_void( CMD_SERVICE_ACTIVE, &servMenuTest::onBtn7 ),
//on_set_int_int( CMD_SERVICE_DEACTIVE, &servMenuTest::setdeActive ),

on_set_int_obj( CMD_SERVICE_INTENT, &servMenuTest::onIntent ),
on_set_int_arg( servMenuTest::cmd_intent_return, &servMenuTest::onIntentReturn ),

on_set_int_int( CMD_SERVICE_TIMEOUT, &servMenuTest::onTimeOut ),

on_set_int_int( CMD_SERVICE_SUB_ENTER, &servMenuTest::onSubEnter ),
on_set_int_int( CMD_SERVICE_SUB_RETURN, &servMenuTest::onSubReturn ),

on_set_int_int( CMD_SERVICE_ITEM_FOCUS, &servMenuTest::onFocusIn ),

on_set_int_void( CMD_SERVICE_STARTUP, &servMenuTest::startup ),

on_set_int_int( servMenuTest::cmd_lcd_light, &servMenuTest::setBkLight ),
on_set_int_int( servMenuTest::cmd_dso_fanspeed, &servMenuTest::setFanSpeed ),
on_set_int_void( servMenuTest::cmd_beeper_short, &servMenuTest::setBeeper ),

on_get_int( servMenuTest::cmd_cpld_ver, &servMenuTest::getCpldVer ),
on_get_int( servMenuTest::cmd_mb_ver, &servMenuTest::getMbVer  ),

//! test
on_set_int_int( servMenuTest::cmd_adc, &servMenuTest::setAdc ),
on_set_int_int( servMenuTest::cmd_sadc, &servMenuTest::setSAdc ),
on_set_int_int( servMenuTest::cmd_pll5, &servMenuTest::setPll5 ),
on_set_int_int( servMenuTest::cmd_pll4, &servMenuTest::setPll4 ),

on_set_int_int( servMenuTest::cmd_relay, &servMenuTest::setRelay ),
on_set_int_int( servMenuTest::cmd_vga1, &servMenuTest::setVga1 ),
on_set_int_int( servMenuTest::cmd_vga2, &servMenuTest::setVga2 ),
on_set_int_int( servMenuTest::cmd_vga3, &servMenuTest::setVga3 ),

on_set_int_int( servMenuTest::cmd_vga4, &servMenuTest::setVga4 ),

on_set_int_int( servMenuTest::cmd_progress_cancel, &servMenuTest::onProgCancel ),

on_set_int_arg( servMenuTest::cmd_set_arg, &servMenuTest::onSetArg ),
on_get_int_arg( servMenuTest::cmd_get_arg, &servMenuTest::onGetArg ),

on_get_void_argo( servMenuTest::cmd_get_argO, &servMenuTest::onGetArgO ),
on_get_void_argi_argo( servMenuTest::cmd_get_argIO, &servMenuTest::onGetArgIO ),

on_set_int_obj( servMenuTest::cmd_progress_cfg, &servMenuTest::setProgressCfg ),

on_set_int_int( CMD_SERVICE_ENTER_ACTIVE, &servMenuTest::onEnterActive ),
on_set_int_int( CMD_SERVICE_EXIT_ACTIVE, &servMenuTest::onExitActive ),

on_set_void_void( servMenuTest::cmd_thread_started, &servMenuTest::on_started ),
on_set_void_void( servMenuTest::cmd_thread_ended, &servMenuTest::on_finished ),

on_set_int_obj( CMD_SERVICE_HELP_REQUESST, &servMenuTest::testHelpRequest),

on_set_int_void( servMenuTest::cmd_msg_box_ok, &servMenuTest::onMsgBoxOk ),
on_set_int_void( servMenuTest::cmd_quick_key, &servMenuTest::onQuickKey ),

on_set_void_arg( CMD_SERVICE_PHY_KEY, &servMenuTest::on_phy_key ),
#include "./serv_menutest_map.cpp"

on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_CHILD, &servMenuTest::setComboxKnobChild ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_CHILD, &servMenuTest::getComboxKnobChild),

on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_CHILD_SUB_1, &servMenuTest::setComboxKnobChildV ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_CHILD_SUB_1, &servMenuTest::getComboxKnobChildV ),

on_set_void_void( MSG_HORIZONTAL_TIMESCALE, &servMenuTest::on_timescale_changed ),

on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE1, &servMenuTest::onBtn1 ),
on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE2, &servMenuTest::onBtn2 ),
on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE3, &servMenuTest::onBtn3 ),
on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE4, &servMenuTest::onBtn4 ),

on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE5, &servMenuTest::onBtn5 ),
on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE6, &servMenuTest::onBtn6 ),
on_set_void_void( MSG_MENUTEST_CONTROL_BUTTON_ONE7, &servMenuTest::onBtn7 ),

on_set_int_bool( CMD_SERVICE_PAGE_SHOW, &servMenuTest::onMSG_GUI_MENU_SHOW ),

end_of_entry()

servMenuTest::servMenuTest( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    m_combox_func = 0;

    m_Button_Two = false;

    m_label_float = 0;

    m_combox = 0;

    m_label_icon = 0;

     m_checkbox = 0;

     m_label_int = 100000;

     m_label_str = 0;

     m_submenu = 0;

     m_button_two = false;

     m_combox_menu = 0;

     m_A = false;

     m_B = 0;

     m_F = false;

     m_C = 0;

     m_D = 0;

     m_E = 0;

     m_G = 0;

     m_H = 0;

     m_pThread = NULL;

     m_label_fkk = 1;

     mIp = 0x12345678;
     mTime = 0x102030;
     mDate = 0x10240312;
     mSeekBar = 50;

     mCombox = 1;
     mComboxKnob = 100;

     mStr = "hello";

     mComboxKnobChild = 0;
     mComboxKnobChildV = 0;

     QList<int> listQuick;
     listQuick.append( R_Pkey_CH1 );
     listQuick.append( R_Pkey_CH1 );
     listQuick.append( R_Pkey_CH2 );
     listQuick.append( R_Pkey_CH2 );

     appendQuickKey( listQuick, servMenuTest::cmd_quick_key );
}

DsoErr servMenuTest::start()
{
    return ERR_NONE;
}
void servMenuTest::registerSpy()
{
//    spyOn( E_SERVICE_ID_HORI, MSG_HORIZONTAL_TIMESCALE );

//    spyOn( E_SERVICE_ID_UTILITY, CMD_SERVICE_PHY_KEY );

    spyOn( E_SERVICE_ID_UI, CMD_SERVICE_PAGE_SHOW );
}
DsoErr servMenuTest::onTimeOut( int timerId )
{
    if ( timerId == 1001 )
    {
        m_C++;
        serviceExecutor::post( serv_name_gui, servGui::cmd_progress_step, 1 );

        if ( m_C >= 10 )
        {
            stopTimer( timerId );
            serviceExecutor::post( serv_name_gui, servGui::cmd_progress_visible, false );
            m_C = 0;
        }

        return ERR_NONE;
    }

    qDebug()<<__FUNCTION__<<timerId;

    m_C++;

    strC=QString("time:%1").arg( m_C );

//    mUiAttr.
    setuiChange( MSG_MENUTEST_B1_C );

    return ERR_NONE;
}

DsoErr  servMenuTest::onSubEnter( int subId )
{
    qDebug()<<__FUNCTION__<<subId;
    return ERR_NONE;
}
DsoErr  servMenuTest::onSubReturn( int subId )
{
    qDebug()<<__FUNCTION__<<subId;
    return ERR_NONE;
}

DsoErr  servMenuTest::onFocusIn( int /*msgId*/ )
{
    //qDebug()<<__FUNCTION__<<msgId;
    return ERR_NONE;
}

DsoErr servMenuTest::onMSG_GUI_MENU_SHOW( bool b )
{
    LOG_DBG()<<b;

    return ERR_NONE;
}

#include "../servplot/servplot.h"
DsoErr servMenuTest::startup()
{
    updateAllUi( false );

//    syncEngine( ENGINE_LCD_LIGHT, 50 );

//    onBtn7();

//    serviceExecutor::post( serv_name_plot,
//                           servPlot::cmd_set_alpha,
//                           255 );

//    serviceExecutor::post( serv_name_utility,
//                           MSG_APP_UTILITY_LANGUAGE,
//                           language_english );

//    syncEngine( ENGINE_FAN_SPEED, 99 );

    return ERR_NONE;
}

DsoErr servMenuTest::setBkLight( int light )
{
    qWarning()<<__FUNCTION__<<light;
    syncEngine( ENGINE_LCD_LIGHT, light );

    return ERR_NONE;
}
DsoErr servMenuTest::setFanSpeed( int speed )
{
    qWarning()<<__FUNCTION__<<speed;
    syncEngine( ENGINE_FAN_SPEED, speed );

    return ERR_NONE;
}

DsoErr servMenuTest::setBeeper()
{

    syncEngine( ENGINE_BEEP_SHORT );

    return ERR_NONE;
}


quint16 servMenuTest::getCpldVer()
{
    quint16 ver;
    queryEngine( qENGINE_CPLD_VER, ver );
    return ver;
}
quint16 servMenuTest::getMbVer()
{
    quint16 ver;
    queryEngine( qENGINE_MB_VER, ver );
    return ver;
}

DsoErr servMenuTest::setAdc( int /*v*/ )
{
//    return syncEngine( ENGINE_ADC_CFG, v );
    return ERR_NONE;
}
DsoErr servMenuTest::setSAdc( int /*v*/ )
{
//    return syncEngine( ENGINE_SADC_CFG, v );
    return ERR_NONE;
}
DsoErr servMenuTest::setPll5( int /*v*/ )
{
//    return syncEngine( ENGINE_PLL5_CFG, v );
    return ERR_NONE;
}
DsoErr servMenuTest::setPll4( int /*v*/ )
{
//    return syncEngine( ENGINE_PLL4_CFG, v );
    return ERR_NONE;
}
DsoErr servMenuTest::setRelay( int /*v*/ )
{
//    return syncEngine( ENGINE_RELAY_CFG, v );
    return ERR_NONE;
}
DsoErr servMenuTest::setVga1( int /*v*/ )
{
//    return syncEngine( ENGINE_VGA1_CFG, v );
    return ERR_NONE;
}

DsoErr servMenuTest::setVga2( int /*v*/ )
{
//    return syncEngine( ENGINE_VGA2_CFG, v );
    return ERR_NONE;
}

DsoErr servMenuTest::setVga3( int /*v*/ )
{
//    return syncEngine( ENGINE_VGA3_CFG, v );
    return ERR_NONE;
}

DsoErr servMenuTest::setVga4( int /*v*/ )
{
//    return syncEngine( ENGINE_VGA4_CFG, v );
    return ERR_NONE;
}

DsoErr servMenuTest::onProgCancel( int para )
{
    qDebug()<<__FUNCTION__<<para;

    serviceExecutor::post( serv_name_gui, servGui::cmd_progress_visible, false );

    return ERR_NONE;
}

DsoErr servMenuTest::onSetArg( CArgument &arg )
{
    qDebug()<<__FUNCTION__<<arg.size();

    return ERR_NONE;
}

int servMenuTest::onGetArg( CArgument &arg )
{
    qDebug()<<__FUNCTION__;
    return arg.size();
}

void servMenuTest::onGetArgIO( CArgument &argI, CArgument &argO )
{
    qDebug()<<argI.size();
    argO.setVal( 1 );
    argO.setVal( 2 );
}
void servMenuTest::onGetArgO( CArgument &argO )
{
    argO.setVal( 10 );
}

DsoErr servMenuTest::setProgressCfg( CtrlProgress *pCtrl )
{
    qDebug()<<__FUNCTION__<<__LINE__<<pCtrl->mInfo;

    return ERR_NONE;
}

DsoErr servMenuTest::onEnterActive( int /*arg*/ )
{
    return ERR_NONE;
}
DsoErr servMenuTest::onExitActive( int /*arg*/ )
{
    return ERR_NONE;
}

DsoErr servMenuTest::onIntent(CIntent *pIntent)
{
    Q_ASSERT( NULL != pIntent );

    int val;
    pIntent->mArgs.getVal(val);
    qDebug()<<__FUNCTION__<<__LINE__<<val<<pIntent->mClientId;

    returnIntent( *pIntent, 100 );

    serviceExecutor::onIntent( pIntent );

    return ERR_NONE;
}

DsoErr servMenuTest::onIntentReturn( CArgument &val )
{
    int arg;
    qDebug()<<__FUNCTION__<<__LINE__<<val.size();

    val.getVal( arg ); //qDebug()<<arg;
    val.getVal( arg, 1 );// qDebug()<<arg;

    return ERR_NONE;
}

DsoErr servMenuTest::onMsgBoxOk()
{
    qDebug()<<__FUNCTION__<<__LINE__;
    return ERR_NONE;
}

DsoErr servMenuTest::onQuickKey()
{
    qDebug()<<__FUNCTION__<<__LINE__;
    return ERR_NONE;
}


void servMenuTest::on_timescale_changed()
{
    LOG_DBG();
}

void servMenuTest::on_phy_key( CArgument &arg )
{
    LOG_DBG()<<arg.size()<<arg[0].iVal<<arg[1].iVal<<arg[2].bVal;
}

DsoErr servMenuTest::setString( QString str )
{
    mStr = str;
    qDebug()<<__FUNCTION__<<__LINE__;
    return ERR_NONE;
}
QString servMenuTest::getString()
{
    //qDebug()<<__FUNCTION__<<__LINE__;
    return mStr;
}


void servMenuTest::on_started()
{
    qDebug()<<"thread started"<<__FUNCTION__<<__LINE__;
}
void servMenuTest::on_finished()
{
    qDebug()<<"thread finished";

    delete m_pThread;
    m_pThread = NULL;
}

void servMenuTest::testSetArg()
{
        CArgument arg;

        arg.setVal( 1 );
        arg.setVal( 2, 1 );

        serviceExecutor::post( servMenuTest::cmd_set_arg,
                               arg );
}

void servMenuTest::testGetArg()
{
    CArgument arg;
    CArgument argOut;

    arg.setVal( 1 );
    arg.setVal( 2, 1 );
    arg.setVal( 3, 2 );

    serviceExecutor::query( servMenuTest::cmd_get_arg,
                            arg,
                            argOut );

//    qDebug()<<argOut.size()<<argOut[0].iVal;
}

void servMenuTest::testIntent()
{
//    CIntent intent;

//    intent.setIntent( MSG_MENUTEST_CHILDMENU1_BUTTON_TWO,
//    intent.setIntent( MSG_STORAGE_SUB_SAVE,
//                                 1,
//                                 cmd_intent_return,
//                                  1);

//    intent.mArgs.setVal( 10 );

//    qDebug()<<__FUNCTION__<<__LINE__;
//    startIntent( "test", intent );
//    startIntent( serv_name_storage, intent );
}
void servMenuTest::testProgress()
{
    CtrlProgress *prog = new CtrlProgress();

    mProgStep = 0;

    prog->mInfo = "hello progress";
    prog->mbShow = true;

    //! cancel
//    prog->mClientId = this->mpItem->servId;
//    prog->mClientMsg = cmd_progress_cancel;
//    prog->mClientPara = 1;
    //! modal
    prog->mbModal = false;

    CArgument arg;
    arg.setVal( prog );

    serviceExecutor::post( serv_name_gui,
                           servGui::cmd_progress_cfg,
                           arg );

}

void servMenuTest::testNotify()
{
//    CtrlParagraph *notify = new CtrlParagraph();

//    notify->append( Qt::red, "A" );
//    notify->append( Qt::white, "B" );
//    notify->append( Qt::green, "Notify" );

//    CArgument arg;
//    arg.append( notify );

//    serviceExecutor::post( serv_name_gui,
//                           servGui::cmd_info_cfg,
//                           arg );

//    servGui::showInfo( "Hello" );

    QStringList strings;
    strings.append( "hello" );
    strings.append( "A" );
    strings.append( "B" );

    QList<QColor> colorList;
    colorList.append( Qt::red );
    colorList.append( Qt::blue );
    servGui::showInfo( strings, colorList );
}

#include "../servcal/servcal.h"
void servMenuTest::testPreCal()
{
    CArgument argIn;

    //! enter
//    serviceExecutor::post( serv_name_cal,
//                           servCal::CMD_RMT_ENTER,
//                           4 );

    //! path
    argIn.append( 4 );
    argIn.append( servCal::cal_path_1m_x1 );
    serviceExecutor::post( serv_name_cal,
                           servCal::CMD_RMT_AFE_PATH,
                           argIn );

    //! k
    argIn.setVal( 4 );
    argIn.setVal( 15, 1 );
    serviceExecutor::post( serv_name_cal,
                           servCal::CMD_RMT_AFE_K,
                           argIn );

    //! zero
//    serviceExecutor::post( serv_name_cal,
//                           servCal::CMD_RMT_AFE_ZERO,
//                           4 );

    //! lsb
    argIn.setVal( 4 );
    argIn.setVal( servCal::cal_path_1m_x1, 1 );
    argIn.setVal( DsoReal(1,E_N2), 2 );
    serviceExecutor::post( serv_name_cal,
                           servCal::CMD_RMT_AFE_LSB,
                           argIn );

    //! kx
    argIn.setVal( 15, 1 );
    argIn.setVal( DsoReal(5,E_N1), 2 );
    serviceExecutor::post( serv_name_cal,
                           servCal::CMD_RMT_AFE_KX,
                           argIn );

}

void servMenuTest::testCalQuery()
{
    CArgument argIn, argOut;

    //! status
    argIn.setVal( 4 );
    serviceExecutor::query( serv_name_cal,
                           servCal::qCMD_RMT_STATUS,
                           argIn,
                           argOut );
//    qDebug()<<argOut[0].iVal;

    //! query zero
    serviceExecutor::query( serv_name_cal,
                           servCal::qCMD_RMT_AFE_ZERO,
                           argIn,
                           argOut );
//    qDebug()<<argOut[0].iVal;

    //! query lsb
    argIn.setVal( 4 );
    argIn.setVal( servCal::cal_path_1m_x1, 1 );
    serviceExecutor::query( serv_name_cal,
                           servCal::qCMD_RMT_AFE_LSB,
                           argIn,
                           argOut );
    float fVal;
    argOut[0].realVal.toReal( fVal );
//    qDebug()<<fVal;

    //! query k
    argIn.setVal( 4 );
    argIn.setVal( 15, 1 );
    serviceExecutor::query( serv_name_cal,
                           servCal::qCMD_RMT_AFE_KX,
                           argIn,
                           argOut );
    argOut[0].realVal.toReal( fVal );
//    qDebug()<<fVal;

}

void servMenuTest::testMisc( int id )
{
    if ( id == 0 )
    {
        postEngine( ENGINE_LA_H_THRESHOLD, 10000000 );
        postEngine( ENGINE_LA_L_THRESHOLD, 10000000 );
    }
    else
    {
        postEngine( ENGINE_LA_H_THRESHOLD, -10000000 );
        postEngine( ENGINE_LA_L_THRESHOLD, -10000000 );
    }

    if ( id == 0 )
    {
        syncEngine( ENGINE_TRIG_OUT_MUX, 0 );
//        syncEngine( ENGINE_TRIG_OUT_MUX, 2 );
    }
    else
    {
        syncEngine( ENGINE_TRIG_OUT_MUX, 1 );
//        syncEngine( ENGINE_TRIG_OUT_MUX, 3 );
    }

    if ( id == 0 )
    {
        syncEngine( ENGINE_PROBE_1K_EN, false );
    }
    else
    {
        syncEngine( ENGINE_PROBE_1K_EN, true );
    }

}

void servMenuTest::testTrigOut( int /*id*/ )
{

}

void servMenuTest::test1_1g( bool b )
{
    syncEngine( ENGINE_1_1G_OUT, b );
}

void servMenuTest::testChLabel( bool bVisible )
{
    servGui::showLabel( "a",
                        bVisible,
                        Qt::red,
                        "hello",
                        100,100 );
}

void servMenuTest::testHLine( bool bVisible )
{
    servGui::showHLine( "a", bVisible );
}
void servMenuTest::testVLine( bool bVisible )
{
    servGui::showVLine( "a", bVisible );
}

DsoErr servMenuTest::testHelpRequest( CHelpRequest *pReq )
{
    Q_ASSERT( NULL != pReq );

    qDebug()<<"testhelprequest:"<<pReq->getName()<<pReq->getMsg();

    return ERR_NONE;
}

void servMenuTest::testQueryMath()
{
    dsoVert *pVert = dsoVert::getCH( m1 );
    if ( pVert == NULL )return;

    DsoWfm wfm;
    pVert->getTrace( wfm );

    wfm.dbgShow();
}


void servMenuTest::testShowInfoXy()
{
//    servGui::showInfo( "hello", Qt::blue, NULL, 10, 30 );

    servGui::showInfo(  MSG_RECORD_FRAMESTART );
}
void servMenuTest::testRecPlay( bool bRec )
{
    cplatform *platForm = cplatform::sysGetDsoPlatform();

    Q_ASSERT( platForm != NULL );

    //! -- record
    if ( bRec )
    {
        platForm->m_pPhy->mRecPlay.setRecInterval( time_ms(1) );
        platForm->m_pPhy->mRecPlay.setNow( 0 );
        platForm->m_pPhy->mRecPlay.setRange( 0, 1000 );
        platForm->m_pPhy->mRecPlay.startRec();
    }
    //! -- play
    else
    {
        platForm->m_pPhy->mRecPlay.setPlayInterval( time_ms(10) );
        platForm->m_pPhy->mRecPlay.setNow( 0 );
        platForm->m_pPhy->mRecPlay.setRange( 0, 1000 );
        platForm->m_pPhy->mRecPlay.startPlay(  );
    }

    platForm->m_pPhy->flushWCache();
}

void servMenuTest::testShowPrivate()
{
    int size = servDso::getLen(1);
    LOG_DBG()<<size;
    if ( size > 0 )
    {
        char buf[ size ];

        servDso::load( 1, buf, size );

//        LOG_DBG()<<buf;
    }
    else
    {
        servDso::save( 1, (void*)"abc", 4 );

        serviceExecutor::post( serv_name_dso, servDso::CMD_FLUSH_CACHE, (int)1 );
    }
}

void servMenuTest::testQueryTrace()
{
    dsoVert *pVert;
    DsoWfm wfm;

    //! ch
    {
        for( int ch = chan1; ch <= chan1; ch++ )
        {
            pVert = dsoVert::getCH( (Chan)ch );
            Q_ASSERT( NULL != pVert );

            pVert->getTrace( wfm );
            wfm.save("/rigol/trace/ch1_trace.dat");

            pVert->getEye( wfm );
            wfm.save("/rigol/trace/ch1_eye.dat");

            pVert->getDigi( wfm );
            wfm.save("/rigol/trace/ch1_digi.dat");
        }
    }

    //! la
    {
        pVert = dsoVert::getCH( la );
        Q_ASSERT( NULL != pVert );

//        for ( int ch = d0; ch <= d15; ch++ )
        for ( int ch = d0; ch <= d0; ch++ )
        {
            pVert->getDigi( wfm, d0 );
            wfm.save("/rigol/trace/d0_digi.dat");
        }

        //! trace
        pVert->getTrace( wfm );
        wfm.save("/rigol/trace/la_trace.dat");
    }
}

void servMenuTest::testAdcPhase()
{
    serviceExecutor::post( serv_name_cal, MSG_SELF_CAL_ADC, 0 );
}
void servMenuTest::testLogInfo()
{
    servGui::logInfo( __FILE__ );
    servGui::logInfo( __FILE__, 10 );
}

#include "../servmask/servmask.h"
void servMenuTest::testMask( bool b )
{
    serviceExecutor::post( serv_name_mask,
                           servMask::cmd_mask_test,
                           b );
}
void servMenuTest::queryMask()
{
    serviceExecutor::post( serv_name_mask,
                           servMask::cmd_mask_report,
                           0 );
}

void servMenuTest::testWriteReg()
{
    postEngine( ENGINE_REG,
                (quint32)engine_bus_axi,
                (quint32)0x0460,
                (quint32)10 );
}
void servMenuTest::testReadReg()
{
    quint32 val;
    queryEngine( qENGINE_REG,
                engine_bus_axi,
                (quint32)0x0400,
                val );
    LOG_DBG()<<val;

    queryEngine( qENGINE_REG,
                engine_bus_axi,
                (quint32)0x0460,
                val );
    LOG_DBG()<<val;
}

void servMenuTest::testSnapAvg()
{
//    serviceExecutor::post( serv_name_cal,
//                           servCal::CMD_SNAP_CH_AVG,
//                           0 );

    serviceExecutor::post( serv_name_cal,
                           CMD_SERVICE_ACTIVE,
                           1 );

    serviceExecutor::post( serv_name_cal,
                           MSG_SELF_CAL_START,
                           0 );


}

void servMenuTest::testChAdcCvt()
{
    dsoVert *pVert;

    pVert = dsoVert::getCH( chan1 );

    int iVal;
    float fVal;

    fVal = pVert->chPtToVolt( 128 );LOG_DBG()<<fVal;
    fVal = pVert->chPtToVolt( 150 );LOG_DBG()<<fVal;
    fVal = pVert->chPtToVolt( 128+25 );LOG_DBG()<<fVal;
    fVal = pVert->chPtToVolt( 128-25 );LOG_DBG()<<fVal;

    fVal = pVert->adcToVolt( 128 );LOG_DBG()<<fVal;
    fVal = pVert->adcToVolt( 150 );LOG_DBG()<<fVal;
    fVal = pVert->adcToVolt( 100 );LOG_DBG()<<fVal;

    iVal = pVert->voltToChPt( 0.1f );LOG_DBG()<<iVal;
    iVal = pVert->voltToChPt( -0.1f );LOG_DBG()<<iVal;
    iVal = pVert->voltToChPt( 0.0f );LOG_DBG()<<iVal;

    iVal = pVert->voltToAdc( 0.1f );LOG_DBG()<<iVal;
    iVal = pVert->voltToAdc( -0.1f );LOG_DBG()<<iVal;
    iVal = pVert->voltToAdc( 0.0f );LOG_DBG()<<iVal;
}

#include "../servla/servla.h"
void servMenuTest::testDxPos()
{
    CArgument arg;

    arg.append( d0 );
    arg.append( 8 );

    serviceExecutor::post( serv_name_la,
                           servLa::cmd_dx_pos,
                           arg );

    dsoVert *pVert;

    pVert = dsoVert::getCH(la);

    LOG_DBG()<<pVert->getyMaxPos();
    LOG_DBG()<<pVert->getyPos( d0 );
}

#include "../../engine/base/dsoengine.h"
#define batch_size  (0x100000*10)
void servMenuTest::testChMemory()
{
    dsoEngine *pEngine;
    pEngine = cplatform::sysGetDsoPlatform()->getPlayEngine();

    Q_ASSERT( NULL != pEngine );

    for ( int i = chan1; i <= chan4; i++ )
    {
        //! open
        handleMemory *handle = pEngine->openMemory( (Chan)i );
        if ( NULL == handle || !handle->getValid() )
        { return; }
        LOG_DBG()<<"FullSize:"<<handle->getFullSize();

        //! Engine export init
        pEngine->exportInit(handle);

        //! batch reading
        int ret;
        quint8 *pBuf = new quint8[ batch_size ];
        Q_ASSERT( NULL != pBuf );

        //! split package
        int j;
        for ( j = 0; j < handle->getFullSize()/batch_size; j ++ )
        {
        //! full reading
        //    ret = pEngine->readMemory( handle, pBuf, 1024, handle->getFullSize()-1024 );
            ret = pEngine->readMemory( handle, pBuf, j*batch_size, batch_size );
        //    ret = pEngine->readMemory( handle, pBuf, 0, 7*1024 );
        //    ret = pEngine->readMemory( handle, pBuf, 1024*22, 128 );

            LOG_DBG()<<ret;

//            if ( ret > 0 )
//            { dbgLog( gui_fmt::CGuiFormatter::toString( (Chan)i ) + QString("_memory_%1.dat").arg(j), pBuf, ret ); }
        }

        //! last package
        int lastPackage = handle->getFullSize() - batch_size*j;
        if ( lastPackage > 0 )
        {
            ret = pEngine->readMemory( handle, pBuf, j*batch_size, lastPackage );

//            if ( ret > 0 )
//            { dbgLog( gui_fmt::CGuiFormatter::toString( (Chan)i ) + QString("_memory_%1.dat").arg(j), pBuf, ret ); }
        }

        delete []pBuf;

        //! close
        pEngine->closeMemory( handle );
    }
}

void servMenuTest::onBtn1()
{
//    postEngine( ENGINE_TRIGGER_SWEEP, Trigger_Sweep_Auto );

//    servGui::showInfo("auto");

    serviceExecutor::post( serv_name_math1, MSG_MATH_S32MATHOPERATOR, operator_fft );
    serviceExecutor::post( serv_name_math1, CMD_SERVICE_ACTIVE, 1 );

}
void servMenuTest::onBtn2()
{
    postEngine( ENGINE_TRIGGER_SWEEP, Trigger_Sweep_Normal );

    servGui::showInfo("normal");
}
void servMenuTest::onBtn3()
{
    postEngine( ENGINE_TRIGGER_SWEEP, Trigger_Sweep_Single );

    serviceExecutor::post( serv_name_hori, MSG_HORIZONTAL_RUN, Control_Single );

    servGui::showInfo("single");
}
void servMenuTest::onBtn4()
{
    postEngine( ENGINE_MEAS_RANGE,
                0ll,
                time_s(10) );

    postEngine( ENGINE_MEAS_EN, true );

//    serviceExecutor::post( serv_name_cal, servCal::CMD_CAL_DATA_LINE, 0  );
//    testChMemory();
}

void servMenuTest::onBtn5()
{
//   postEngine( ENGINE_TRIGGER_FORCE );

//    EngineMeasRet measRet;

//    queryEngine( qENGINE_MEAS_VALUE, &measRet );

//    //! hori
//    LOG_DBG()<<measRet.mMeta.mHMeta.mOrigin<<measRet.mMeta.mHMeta.mLength;
//    LOG_DBG()<<measRet.mMeta.mHMeta.mTOrigin.mM<<measRet.mMeta.mHMeta.mTOrigin.mN;
//    LOG_DBG()<<measRet.mMeta.mHMeta.mTInc.mM<<measRet.mMeta.mHMeta.mTInc.mN;

//    //! vert
//    for ( int i = 0; i < 4; i++ )
//    {
//        LOG_DBG()<<measRet.mMeta.mVMetas[i].myGnd<<measRet.mMeta.mVMetas[i].myInc.mM<<measRet.mMeta.mVMetas[i].myInc.mN;
//    }

    qint32 peak;
    queryEngine( qENGINE_TRACE_PEAK, 0, peak ); LOG_DBG()<<(peak>>16)<<(peak&0xffff);
    queryEngine( qENGINE_TRACE_PEAK, 1, peak ); LOG_DBG()<<(peak>>16)<<(peak&0xffff);
    queryEngine( qENGINE_TRACE_PEAK, 2, peak ); LOG_DBG()<<(peak>>16)<<(peak&0xffff);
    queryEngine( qENGINE_TRACE_PEAK, 3, peak ); LOG_DBG()<<(peak>>16)<<(peak&0xffff);
}
void servMenuTest::onBtn6()
{
    postEngine( ENGINE_DISP_SCREEN_PERSIST, true );
}
void servMenuTest::onBtn7()
{
    //postEngine( ENGINE_DISP_SCREEN_PERSIST, false );

    mTestThread.start();
}

#include "../../service/service_name.h"
#include "../servgui/servgui.h"

#include "./serv_menutest_imp.cpp"

