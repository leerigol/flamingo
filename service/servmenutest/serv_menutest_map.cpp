
on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX_FUNC, &servMenuTest::set_combox_func ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX_FUNC, &servMenuTest::get_combox_func ),

on_set_int_bool( MSG_MENUTEST_CONTROL_BUTTON_TWO, &servMenuTest::set_Button_Two ),
on_get_bool( MSG_MENUTEST_CONTROL_BUTTON_TWO, &servMenuTest::get_Button_Two ),

on_set_int_void( MSG_MENUTEST_CONTROL_BUTTON_ONE, &servMenuTest::set_Button_one ),

on_set_int_ll( MSG_MENUTEST_CONTROL_LABEL_FLOAT, &servMenuTest::set_label_float ),
on_get_ll( MSG_MENUTEST_CONTROL_LABEL_FLOAT, &servMenuTest::get_label_float ),


on_set_int_int( MSG_MENUTEST_CONTROL_LABEL_INT_FLOAT_FLOAT_KNOB, &servMenuTest::set_label_int_float_float_knob ),
on_get_int( MSG_MENUTEST_CONTROL_LABEL_INT_FLOAT_FLOAT_KNOB, &servMenuTest::get_label_int_float_float_knob ),

on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX, &servMenuTest::set_combox ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX, &servMenuTest::get_combox ),

on_set_int_int( MSG_MENUTEST_CONTROL_LABEL_ICON, &servMenuTest::set_label_icon ),
on_get_int( MSG_MENUTEST_CONTROL_LABEL_ICON, &servMenuTest::get_label_icon ),

on_set_int_int( MSG_MENUTEST_CONTROL_CHECKBOX, &servMenuTest::set_checkbox ),
on_get_int( MSG_MENUTEST_CONTROL_CHECKBOX, &servMenuTest::get_checkbox ),

on_set_int_int( MSG_MENUTEST_CONTROL_LABEL_INT, &servMenuTest::set_label_int ),
on_get_int( MSG_MENUTEST_CONTROL_LABEL_INT, &servMenuTest::get_label_int ),

on_get_string( MSG_MENUTEST_CONTROL_LABEL_STR, &servMenuTest::getString ),

on_set_int_string( MSG_MENUTEST_CONTROL_LABEL_IME, &servMenuTest::setString ),
on_get_string( MSG_MENUTEST_CONTROL_LABEL_IME, &servMenuTest::getString ),

on_get_string( MSG_MENUTEST_SUBMENU_SUBMENU, &servMenuTest::get_submenu ),

on_set_int_bool( MSG_MENUTEST_CHILDMENU1_BUTTON_TWO, &servMenuTest::set_button_two ),
on_get_bool( MSG_MENUTEST_CHILDMENU1_BUTTON_TWO, &servMenuTest::get_button_two ),

on_set_int_int( MSG_MENUTEST_CHILDMENU1_COMBOX_MENU, &servMenuTest::set_combox_menu ),
on_get_int( MSG_MENUTEST_CHILDMENU1_COMBOX_MENU, &servMenuTest::get_combox_menu ),

on_set_int_bool( MSG_MENUTEST_A1_A, &servMenuTest::set_A ),
on_get_bool( MSG_MENUTEST_A1_A, &servMenuTest::get_A ),

on_set_int_int( MSG_MENUTEST_A1_B, &servMenuTest::set_B ),
on_get_int( MSG_MENUTEST_A1_B, &servMenuTest::get_B ),

on_set_int_bool( MSG_MENUTEST_A2_F, &servMenuTest::set_F ),
on_get_bool( MSG_MENUTEST_A2_F, &servMenuTest::get_F ),

on_get_string( MSG_MENUTEST_B1_C, &servMenuTest::get_C ),

on_get_string( MSG_MENUTEST_B2_D, &servMenuTest::get_D ),

on_get_string( MSG_MENUTEST_B3_E, &servMenuTest::get_E ),

on_get_string( MSG_MENUTEST_F1_G, &servMenuTest::get_G ),

on_get_string( MSG_MENUTEST_F2_H, &servMenuTest::get_H ),

//#ifdef _GUI_CASE
on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB, &servMenuTest::setCombox ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB, &servMenuTest::getCombox ),

on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_SUB_1, &servMenuTest::setComboxKnob ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_SUB_1, &servMenuTest::getComboxKnob ),

on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_ICON, &servMenuTest::setCombox ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_ICON, &servMenuTest::getCombox ),

on_set_int_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_ICON_SUB_1, &servMenuTest::setComboxKnob ),
on_get_int( MSG_MENUTEST_CONTROL_COMBOX_KNOB_ICON_SUB_1, &servMenuTest::getComboxKnob ),

on_set_int_int( MSG_MENUTEST_CONTROL_TIME_EDIT, &servMenuTest::setTime ),
on_get_int( MSG_MENUTEST_CONTROL_TIME_EDIT, &servMenuTest::getTime ),

on_set_int_int( MSG_MENUTEST_CONTROL_DATE_EDIT, &servMenuTest::setDate ),
on_get_int( MSG_MENUTEST_CONTROL_DATE_EDIT, &servMenuTest::getDate ),

on_set_int_int( MSG_MENUTEST_CONTROL_IP_EDIT, &servMenuTest::setIp ),
on_get_int( MSG_MENUTEST_CONTROL_IP_EDIT, &servMenuTest::getIp ),

on_set_int_int( MSG_MENUTEST_CONTROL_SEEK_BAR, &servMenuTest::setSeekBar ),
on_get_int( MSG_MENUTEST_CONTROL_SEEK_BAR, &servMenuTest::getSeekBar ),

on_set_int_int( MSG_MENUTEST_CONTROL_LABEL_INT_FLOAT_FLOAT_KNOB_SUB_1, &servMenuTest::set_label_int_float_float_knob_val ),
on_get_int( MSG_MENUTEST_CONTROL_LABEL_INT_FLOAT_FLOAT_KNOB_SUB_1, &servMenuTest::get_label_int_float_float_knob_val ),

//#endif
