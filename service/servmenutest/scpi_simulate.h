#ifndef SCPI_SIMULATE
#define SCPI_SIMULATE

#include "servmenutest.h"

struct simulateCmdItem
{
    QString mName;
    int mMsg;
    QString mCvt;
    qlonglong paraA, paraB;
};

//! converters
class CSimulateCvt
{
protected:
    QString mName;

public:
    void setName( const QString &name )
    { mName = name; }
    QString getName() const
    { return mName; }

    virtual bool cvt( CArgument &/*arg*/, qlonglong /*paraA*/, qlonglong /*paraB*/ )
    { return false; }
};

class CBoolCvt : public CSimulateCvt
{
public:
    virtual bool cvt( CArgument &arg, qlonglong paraA, qlonglong /*paraB*/ )
    {
        arg.setVal( (bool)paraA );
        return true;
    }
};

class CIntCvt : public CSimulateCvt
{
public:
    virtual bool cvt( CArgument &arg, qlonglong paraA, qlonglong /*paraB*/ )
    {
        arg.setVal( (qint32)paraA );
        return true;
    }
};

class CLonglongCvt : public CSimulateCvt
{
public:
    virtual bool cvt( CArgument &arg, qlonglong paraA, qlonglong /*paraB*/ )
    {
        arg.setVal( paraA );
        return true;
    }
};

class CRealCvt : public CSimulateCvt
{
public:
    virtual bool cvt( CArgument &arg, qlonglong paraA, qlonglong paraB )
    {
        arg.setVal( DsoReal( paraA, (DsoE)paraB) );
        return true;
    }
};

class CCvtFactory
{
private:
    static QList< CSimulateCvt *> _cvtList;
private:
    static bool _bBuilded;
    static void build()
    {
        CSimulateCvt *pCvt;

        //! converters
        pCvt = new CBoolCvt();
        Q_ASSERT( NULL != pCvt );
        pCvt->setName( "bool" );
        _cvtList.append( pCvt );

        pCvt = new CIntCvt();
        Q_ASSERT( NULL != pCvt );
        pCvt->setName( "int" );
        _cvtList.append( pCvt );

        pCvt = new CLonglongCvt();
        Q_ASSERT( NULL != pCvt );
        pCvt->setName( "longlong" );
        _cvtList.append( pCvt );

        pCvt = new CRealCvt();
        Q_ASSERT( NULL != pCvt );
        pCvt->setName( "real" );
        _cvtList.append( pCvt );
    }
private:
    CCvtFactory()
    {}
public:
    static CSimulateCvt *createCvt( const QString &name )
    {
        //! check build
        if ( !CCvtFactory::_bBuilded )
        {
            CCvtFactory::build();
            CCvtFactory::_bBuilded = true;
        }

        //! find the converter
        foreach( CSimulateCvt *pCvt, _cvtList )
        {
            Q_ASSERT( NULL != pCvt );

            if ( pCvt->getName() == name )
            { return pCvt; }
        }

        return NULL;
    }
};

#endif // SCPI_SIMULATE

