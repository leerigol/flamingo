    DsoErr set_combox_func( int v );
    int get_combox_func();

    DsoErr set_Button_Two(bool b);
    bool get_Button_Two();


    DsoErr set_Button_one();
    DsoErr set_label_float(qlonglong v);
    qlonglong get_label_float();

    DsoErr set_label_int_float_float_knob(int keyInc);
    int get_label_int_float_float_knob();

    DsoErr set_label_int_float_float_knob_val( int val );
    float get_label_int_float_float_knob_val();

    DsoErr set_combox(int v);
    int get_combox();

    DsoErr set_label_icon(int v);
    int get_label_icon();

    DsoErr set_checkbox(int v);
    int get_checkbox();

    DsoErr set_label_int(int v);
    int get_label_int();

    QString get_submenu();

    DsoErr set_button_two(bool b);
    bool get_button_two();

    DsoErr set_combox_menu(int v);
    int get_combox_menu();

    DsoErr set_A(bool b);
    bool get_A();

    DsoErr set_B(int v);
    int get_B();

    DsoErr set_F(bool b);
    bool get_F();

    QString get_C();

    QString get_D();

    QString get_E();

    QString get_G();

    QString get_H();

    DsoErr setTime( int time ){ mTime = time; return ERR_NONE; }
    DsoErr setDate( int date ) { mDate = date; return ERR_NONE; }
    DsoErr setIp( int ip ) { mIp = ip; return ERR_NONE; }
    DsoErr setSeekBar( int bar ) { mSeekBar = bar; return ERR_NONE; }

    DsoErr setCombox( int combox )
    {
        mCombox = combox;
#ifdef _GUI_CASE
        mUiAttr.setBase( MSG_MENUTEST_CONTROL_COMBOX_KNOB_SUB_1, (mCombox+1)*0.1f );
        if ( mCombox == 1 )
        {
            mUiAttr.setPostStr( MSG_MENUTEST_CONTROL_COMBOX_KNOB_SUB_1, "V" );
        }
        else
        {_GUI_CASE
            mUiAttr.setPostStr( MSG_MENUTEST_CONTROL_COMBOX_KNOB_SUB_1, "A" );
        }
#endif

        return ERR_NONE;
    }
    DsoErr setComboxKnob( int knob )
    { mComboxKnob = knob;
        return ERR_NONE; }

    DsoErr setComboxKnobChild( int child )
    { mComboxKnobChild = child;
      mComboxKnobChildV = child;
        return ERR_NONE; }
    DsoErr setComboxKnobChildV( int v )
    { mComboxKnobChildV = v; return ERR_NONE; }

    int getTime() { return mTime; }
    int getDate() { return mDate; }
    int getIp() { return mIp; }
    int getSeekBar() {  setuiMinVal(10); setuiMaxVal(200); return mSeekBar; }

    int getCombox() { return mCombox; }
    int getComboxKnob() { return mComboxKnob; }

    int getComboxKnobChild()
    { return mComboxKnobChild; }
    int getComboxKnobChildV()
    { return mComboxKnobChildV; }
