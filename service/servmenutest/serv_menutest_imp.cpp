
#include "../../menu/rmenus.h"
#include "../../service/servch/servch.h"
DsoErr servMenuTest::set_combox_func( int v )
{
    m_combox_func = v;

    qDebug()<<__FUNCTION__<<v;

    setuiVisible( 2, m_Button_Two );

    return ERR_NONE;
}
int servMenuTest::get_combox_func()
{
    return m_combox_func;
}

#include "../../baseclass/cbinstream.h"
#include "../../service/servmath/servmath.h"
#include "../service_name.h"
DsoErr servMenuTest::set_Button_Two(bool b)
{
    m_Button_Two = b;
//    testSetArg();
//    testGetArg();
//    if(b)
//    {
//        testHLine( true);
//    }
//    else
//    {
//        testHLine( false);
//    }

//    test1_1g( b );
//    if ( b )
//    {
//        postEngine( ENGINE_TRIGGER_SWEEP, Trigger_Sweep_Normal );
//    }
//    else
//    {
//        postEngine( ENGINE_TRIGGER_SWEEP, Trigger_Sweep_Auto );
//    }

//    if ( b )
//    {
//        postEngine( ENGINE_DISP_PALETTE, Wfm_color );
//    }
//    else
//    {
//        postEngine( ENGINE_DISP_PALETTE, Wfm_grade );
//    }

//    //! trig ch en
//    postEngine( ENGINE_TRIGGER_CH_EN, chan1, b );
//    postEngine( ENGINE_TRIGGER_CH_EN, chan2, b );
//    postEngine( ENGINE_TRIGGER_CH_EN, chan3, b );
//    postEngine( ENGINE_TRIGGER_CH_EN, chan4, b );

//    serviceExecutor::post( serv_name_gui,
//                           servGui::cmd_logo_animate_run,
//                           m_Button_Two );


//      testRecPlay( b );
//    testLogInfo();
    testMask( b );

//    if ( b )
//    { testWriteReg(); }
//    else
//    { testReadReg(); }

//    mUiAttr.setEnable( MSG_MENUTEST_CONTROL_COMBOX_FUNC, 1, b );
//    mUiAttr.setVisible( MSG_MENUTEST_CONTROL_COMBOX_FUNC, 1, b );

//    serviceExecutor::post("chan1", CMD_SERVICE_ACTIVE, MSG_CHAN_PROBE_VA );
//    serviceExecutor::post( "menutest", CMD_SERVICE_ACTIVE, 3 );

//    mUiAttr.setVisible( MSG_MENUTEST_CONTROL_BUTTON_ONE, b );
//    mUiAttr.setEnable( MSG_MENUTEST_CONTROL_LABEL_FLOAT, b );

//    if ( b )
//    {
//        startTimer( 100, 1 );
//    }
//    else
//    {
//        stopTimer( 1 );
//    }

//    syncEngine( ENGINE_BEEP_SHORT );
//    qDebug()<<__FUNCTION__;

    //! language selection
//    if ( b )
//    {
//        menu_res::CResData::setLanguageSeq( 1 );
//    }
//    else
//    {
//        menu_res::CResData::setLanguageSeq( 0 );
//    }

//      sysSetLanguage( (SystemLanguage)(b) );

//      syncEngine( ENGINE_BEEP_SHORT );

//    m_pThread = new testThread(this);
//    m_pThread->start();

//    quint8 buf[1024];

//    CBinStream binStream( buf, sizeof(buf) );
//    serialVersion ver;

//    ISerial *pSerial;
//    pSerial = dynamic_cast<ISerial*> ( service::findService( serv_name_math1 ) );
//    pSerial->serialOut( binStream, ver );

//    binStream.setSeek( 0 );
//    pSerial->serialIn( binStream, ver );


//    CArgument arg;

//    arg.setVal( 1 );
//    arg.setVal( 2, 1 );

//    serviceExecutor::post( servMenuTest::cmd_set_arg,
//                           arg );

//    CArgument argOut;
//    serviceExecutor::query( servMenuTest::cmd_get_arg,
//                            arg,
//                            argOut );
//    qDebug()<<argOut.size()<<argOut[0].iVal;

//    CArgument argOut, argI;
//    argI.setVal( 1 );
//    serviceExecutor::query( servMenuTest::cmd_get_argO,
//                        argOut );
//    qDebug()<<argOut.size()<<argOut[0].iVal;

//    serviceExecutor::query( servMenuTest::cmd_get_argIO,
//                        argI,
//                        argOut );
//    qDebug()<<argOut.size()<<argOut[0].iVal;

//    DsoReal real( 1, E_0 );

//    serviceExecutor::query( serv_name_ch1,
//                           servCH::cmd_scale_ui_dsoreal,
//                           real );
//    query( serv_name_ch1,
//           servCH::cmd_scale_ui_dsoreal,
//           real );

//    DsoRealType_A a;
//    DsoE e;
//    real.getVal( a, e );
//    qDebug()<<a<<(int)e;

//    real.setVal( 1, E_0 );
//    serviceExecutor::post( serv_name_ch1,
//                           servCH::cmd_scale_ui_dsoreal,
//                           real );

//    serviceExecutor::query( serv_name_ch1,
//                            servCH::cmd_scale_ui_dsoreal,
//                            real );
//    real.getVal( a, e );
//    qDebug()<<a<<(int)e;

//    serviceExecutor::setdeActive( 0 );

//    mProgStep++;
//    serviceExecutor::post( serv_name_gui,
//                           servGui::cmd_progress_step,
//                           1 );

//    if ( mProgStep > 100 )
//    {
//        serviceExecutor::post( serv_name_gui,
//                               servGui::cmd_progress_visible,
//                               false );
//    }

//    testChLabel( m_Button_Two );
//    testVLine( m_Button_Two );
//    testHLine( m_Button_Two );

//    testCalQuery();
//      testMisc( 1 );

//    sysSetWorkMode( work_help );

//    service::syncEngine( ENGINE_POWER_SWITCH, m_Button_Two );
//    service::syncEngine( ENGINE_POWER_SHUTDOWN );

    return ERR_NONE;
}

bool servMenuTest::get_Button_Two()
{
    return m_Button_Two;
}

#include "../servcal/servcal.h"
DsoErr servMenuTest::set_Button_one()
{
    //! error
//    serviceExecutor::post( serv_name_gui, CMD_SERVICE_DO_ERR, (int)1 );

    //! info
//    struParagraph para1, para2;

//    serviceExecutor::send( serv_name_gui, servGui::cmd_clear_info,(int)1 );

//    para1.mColor = Qt::red;
//    para1.mContent = "ABC";

//    para2.mColor = Qt::green;
//    para2.mContent = "def";

//    serviceExecutor::send( serv_name_gui, servGui::cmd_append_info_paragraph, &para1 );

//    serviceExecutor::send( serv_name_gui, servGui::cmd_append_info_paragraph, &para2 );

//    serviceExecutor::post( serv_name_gui, servGui::cmd_show_info, (int)1 );


//    arg.setVal( 0 );
//    arg.setVal( 1, 1 );

//    qDebug()<<__FUNCTION__<<arg.size();

//    serviceExecutor::post( servMenuTest::cmd_set_arg,
//                           arg );


//    serviceExecutor::post( serv_name_menutester2, CMD_SERVICE_ACTIVE, 1 );
//    serviceExecutor::post( serv_name_menutester,
//                           CMD_SERVICE_ACTIVE,
//                           MSG_MENUTEST_CHILDMENU1_BUTTON_TWO );


//      testShowInfoXy();
//    testShowPrivate();
//    testQueryTrace();

//    testAdcPhase();
//    testSnapAvg();

//    queryMask();

//    testChAdcCvt();
//    testDxPos();

    testChMemory();

//    testIntent();
//    testProgress();
//    testNotify();

//    testPreCal();
//    testMisc( 0 );

//    int conStat;
//    queryEngine( qENGINE_CONTROL_STATUS, conStat );
//    qDebug()<<__FUNCTION__<<__LINE__<<conStat;

//    testSetArg();
//    testGetArg();

//    servGui::showMsgBox( serv_name_menutester,
//                         "hello",
//                         servMenuTest::cmd_msg_box_ok);

//    qDebug()<<__FUNCTION__<<__LINE__<<serv_name_cal;
//    serviceExecutor::post( serv_name_cal,
//                           servCal::CMD_CAL_START,
//                           (int)1 );

//    qDebug()<<__FUNCTION__<<__LINE__;

//    service::syncEngine( ENGINE_POWER_RESTART );
//    service::syncEngine( ENGINE_POWER_SHUTDOWN );

    return ERR_NONE;
}

DsoErr servMenuTest::set_label_float(qlonglong v)
{
    serviceExecutor::post( serv_name_gui, CMD_SERVICE_DO_ERR, (int)2 );

    m_label_float = v;
    return ERR_NONE;
}

qlonglong servMenuTest::get_label_float()
{
//    setuiAcc( key_acc::e_key_one_125 );
    setuiZVal( -1 );

    return m_label_float;
}

DsoErr servMenuTest::set_label_int_float_float_knob(int keyInc)
{
    qDebug()<<__FUNCTION__<<"key"<<keyInc;

    m_label_fkk += keyInc;

    return ERR_NONE;
}

int servMenuTest::get_label_int_float_float_knob()
{
    qDebug()<<__FUNCTION__<<__LINE__;
    return m_label_fkk;
}

DsoErr servMenuTest::set_label_int_float_float_knob_val( int val )
{
    qDebug()<<__FUNCTION__<<val;

    m_label_fkk = val;

    return ERR_NONE;
}
float servMenuTest::get_label_int_float_float_knob_val()
{
    return m_label_fkk*0.001f;
}

DsoErr servMenuTest::set_combox(int v)
{
    m_combox = v;
    qDebug()<<v<<__FUNCTION__;
    return ERR_NONE;
}

int servMenuTest::get_combox()
{
    return m_combox;
}

DsoErr servMenuTest::set_label_icon(int v)
{
    m_label_icon = v;
    return ERR_NONE;
}

int servMenuTest::get_label_icon()
{
    return m_label_icon;
}

DsoErr servMenuTest::set_checkbox(int v)
{
    m_checkbox = v;

    qDebug()<<v<<__FUNCTION__;

    return ERR_NONE;
}

int servMenuTest::get_checkbox()
{
    //qDebug()<<__FUNCTION__;
    return m_checkbox;
}

DsoErr servMenuTest::set_label_int(int v)
{
    m_label_int = v;
    qDebug()<<v<<__FUNCTION__;
    return ERR_NONE;
}

int servMenuTest::get_label_int()
{
    setuiFmt( dso_view_format(fmt_int, fmt_width_4 ) );
    return m_label_int;
}

QString servMenuTest::get_submenu()
{
    return "";
}

DsoErr servMenuTest::set_button_two(bool b)
{
    m_button_two = b;
    return ERR_NONE;
}

bool servMenuTest::get_button_two()
{
    return m_button_two;
}

DsoErr servMenuTest::set_combox_menu(int v)
{
    m_combox_menu = v;
    return ERR_NONE;
}

int servMenuTest::get_combox_menu()
{
    return m_combox_menu;
}

DsoErr servMenuTest::set_A(bool b)
{
    m_A = b;
    return ERR_NONE;
}

bool servMenuTest::get_A()
{
    return m_A;
}

DsoErr servMenuTest::set_B(int v)
{
    m_B = v;
    return ERR_NONE;
}

int servMenuTest::get_B()
{
    return m_B;
}

DsoErr servMenuTest::set_F(bool b)
{
    m_F = b;
    return ERR_NONE;
}

bool servMenuTest::get_F()
{
    return m_F;
}

QString servMenuTest::get_C()
{
//    return "";
    return strC;
}

QString servMenuTest::get_D()
{
    return "";
}

QString servMenuTest::get_E()
{
    return "";
}

QString servMenuTest::get_G()
{
    return "";
}

QString servMenuTest::get_H()
{
    return "";
}

//testThread::testThread( QObject *obj ) : QThread( obj )
//{
//}

//void testThread::run()
//{
//    serviceExecutor::post( serv_name_menutester, servMenuTest::cmd_thread_started, 0 );

//    qDebug()<<"thread run.";
//    QThread::sleep( 1 );
//    qDebug()<<"..";
//    QThread::sleep( 1 );
//    qDebug()<<"....";

//    serviceExecutor::post( serv_name_menutester, servMenuTest::cmd_thread_ended, 0 );
//}
