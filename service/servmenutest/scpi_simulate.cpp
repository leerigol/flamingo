
#include "scpi_simulate.h"

#include "../servch/servch.h"

#define build_item( name, msg, type, p1, p2) \
            QStringLiteral(name), msg, \
            QStringLiteral(type), p1, p2,

static simulateCmdItem _simulate_items[] =
{
//    { build_item( serv_name_ch1, MSG_CHAN_INVERT, "bool", true, 0) },
//    { build_item( serv_name_ch1, MSG_CHAN_INVERT, "bool", false, 0) },

    { build_item( serv_name_ch1, MSG_CHAN_ON_OFF, "bool", false, 0) },
    { build_item( serv_name_ch2, MSG_CHAN_ON_OFF, "bool", false, 0) },
    { build_item( serv_name_ch3, MSG_CHAN_ON_OFF, "bool", false, 0) },
    { build_item( serv_name_ch4, MSG_CHAN_ON_OFF, "bool", false, 0) },
    { build_item( serv_name_ch4, MSG_CHAN_ON_OFF, "bool", false, 0) },

    { build_item( serv_name_ch1, MSG_CHAN_ON_OFF, "bool", true, 0) },
    { build_item( serv_name_ch1, MSG_CHAN_IMPEDANCE, "int", IMP_1M, 0) },
    { serv_name_ch1, MSG_CHAN_PROBE, "int", 6, 0 },
    { serv_name_ch1, servCH::cmd_scale_ui_dsoreal, "real", 2, E_N1 },

    { serv_name_ch1, servCH::cmd_offset_ui_dsoreal, "real", 0, 0 },
    { serv_name_hori, MSG_HORI_MAIN_SCALE, "longlong", time_ms(10), 0 },
    { serv_name_hori, MSG_HOR_ACQ_MODE_YT, "int", Acquire_Normal, 0  },

    { serv_name_trigger_edge, MSG_TRIGGER_TYPE, "int", Trigger_Edge, 0 },
    { serv_name_trigger_edge, MSG_TRIGGER_SOURCE_LA_EXT_AC, "int", chan1, 0},
    { serv_name_trigger_edge, MSG_TRIGGER_SOURCE_LA_EXT_AC, "int", chan2, 0},
    { serv_name_trigger_edge, CMD_SCPI_TRIGGER_LEVEL, "int", 330000ll, 0 },
    { serv_name_trigger, MSG_TRIGGER_SWEEP, "int", Trigger_Sweep_Normal, 0 },
    { serv_name_trigger, MSG_TRIGGER_COUPLING, "int", AC, 0 },

    { serv_name_hori, MSG_HORIZONTAL_RUN, "int", Control_Run, 0 },
};

QList< CSimulateCvt *> CCvtFactory::_cvtList;
bool CCvtFactory::_bBuilded = false;

static DsoErr simulateItem( simulateCmdItem *pCmd )
{
    Q_ASSERT( NULL != pCmd );

    //! convert
    CArgument arg;
    CSimulateCvt *pCvt = CCvtFactory::createCvt( pCmd->mCvt );
    Q_ASSERT( NULL != pCvt );

    if ( !pCvt->cvt( arg, pCmd->paraA, pCmd->paraB ) )
    { return ERR_INVALID_INPUT; }

    //! post
    DsoErr err;
    err =  serviceExecutor::post(pCmd->mName,
                                 pCmd->mMsg,
                                 arg,
                                 &serviceExecutor::_contextRmt
                                 );
    if( err !=  ERR_NONE)
    { return err; }

    //! wait
    Q_ASSERT( NULL != serviceExecutor::_contextRmt.m_pSema );
    Q_ASSERT( serviceExecutor::_contextRmt.m_pSema->available() <= 1 );

    serviceExecutor::_contextRmt.m_pSema->acquire();

    //! err code
    if ( ERR_NONE != serviceExecutor::_contextRmt.mErr )
    {
        return ERR_NONE;
    }
    else
    {
        return ERR_NONE;
    }

    return ERR_NONE;
}

static DsoErr simulateCmd( )
{
    for ( int i = 0; i < array_count(_simulate_items ); i++ )
    {
        simulateItem( _simulate_items + i );
    }
    return ERR_NONE;
}

testThread::testThread(QObject *obj):QThread( obj )
{}

void testThread::run()
{
    QThread::sleep( 20 );

    LOG_DBG();

    for ( int i = 0; i < 100000; i++ )
    {
        if ( i%500 == 0 )
        {
            qDebug()<<__FUNCTION__<<__LINE__<<i<<"-------";
        }
        simulateCmd();
    }

    LOG_DBG();
}
