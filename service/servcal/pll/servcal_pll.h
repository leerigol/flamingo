#ifndef SERVCAL_PLL
#define SERVCAL_PLL

#include "../../baseclass/checkedstream/ccheckedstream.h"

struct pllRequest
{
    quint16 mReg;
    quint16 mData;
};

class calPllPayload
{
public:
    quint32    mLength;
    pllRequest mRequest[128];
};

//! pll config data
class calPllData : public CCheckedStream
{
public:
    calPllPayload mPayload;
};

#endif // SERVCAL_PLL

