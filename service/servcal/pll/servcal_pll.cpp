
#include "./servcal.h"
#include "servcal_pll.h"

void servCal::tune5g()
{
    calPllData pllData;

    do
    {
        if ( pllData.load(mCalFileDbs[1].mPll5g,
                          &pllData.mPayload,
                          sizeof(pllData.mPayload)) )
        { break; }

        if ( pllData.load(mCalFileDbs[0].mPll5g,
                          &pllData.mPayload,
                          sizeof(pllData.mPayload)) )
        { break; }

        //! no data
        return;
    }while(0);

    //! apply setting
    pllRequest *pRequest;
    IPhyPLL *pPll = getPll();

    pRequest = pllData.mPayload.mRequest;
    for ( uint i = 0; i < pllData.mPayload.mLength/sizeof(pllRequest); i++ )
    {
        pPll->write5g( pRequest[i].mReg, pRequest[i].mData );
    }
}

void servCal::tune1_1g()
{
    calPllData pllData;

    do
    {
        if ( pllData.load(mCalFileDbs[1].mPll1_1g,
                          &pllData.mPayload,
                          sizeof(pllData.mPayload)) )
        { break; }

        if ( pllData.load(mCalFileDbs[0].mPll1_1g,
                          &pllData.mPayload,
                          sizeof(pllData.mPayload)) )
        { break; }

        //! no data
        return;
    }while(0);

    //! apply setting
    pllRequest *pRequest;
    IPhyPLL *pPll = getPll();

    pRequest = pllData.mPayload.mRequest;
    for ( uint i = 0; i < pllData.mPayload.mLength/sizeof(pllRequest); i++ )
    {
        pPll->write1_1g( pRequest[i].mReg, pRequest[i].mData );
    }
}

//! can not be off
DsoErr servCal::onPll5Output( bool /*b*/ )
{
    LOG_DBG();
    return ERR_NONE;
}

DsoErr servCal::onPll5Reg( CArgument &arg )
{
    //! check type and size
    if ( !arg.checkType( val_int, val_int) )
    {
        return ERR_INVALID_INPUT;
    }


    getPll()->write5g( arg[0].uVal, arg[1].uVal );
    return ERR_NONE;
}
DsoErr servCal::onPll5Cfg( CArgument &arg )
{
    //! check type
    if ( !arg.checkType(val_arb_bin) )
    {
        return ERR_INVALID_INPUT;
    }

    //! check ptr
    if ( arg[0].arbVal.mPtr == NULL )
    {
        return ERR_INVALID_INPUT;
    }

    //! check size
    if ( arg[0].arbVal.mBlockLen > 0 &&
         arg[0].arbVal.mBlockLen % sizeof(pllRequest) == 0 )
    {}
    else
    {
        return ERR_INVALID_INPUT;
    }

    //! check size
    calPllData cfgData;
    if ( (uint)(arg[0].arbVal.mBlockLen) > sizeof(cfgData.mPayload.mRequest) )
    {
        return ERR_INVALID_INPUT;
    }


    //! deload data
    memcpy( cfgData.mPayload.mRequest,
            arg[0].arbVal.mPtr,
            arg[0].arbVal.mBlockLen );
    cfgData.mPayload.mLength = arg[0].arbVal.mBlockLen;

    //! save
    cfgData.save( mCalFileDbs[1].mPll5g,
                  &cfgData.mPayload,
                  sizeof(cfgData.mPayload) );

    return ERR_NONE;
}
DsoErr servCal::qPll5Cfg( CArgument &arg )
{
    calPllData cfgData;

    //! load from file
    if ( !cfgData.load( mCalFileDbs[1].mPll5g,
                        &cfgData.mPayload,
                        sizeof(cfgData.mPayload)) )
    {
        return ERR_CAL_READ_FAIL;
    }


    ArbBin abin( cfgData.mPayload.mLength,
                 cfgData.mPayload.mRequest );

    arg.setVal( abin );

    return ERR_NONE;
}

DsoErr servCal::onPll1_1Output( bool b )
{
    getPll()->output1_1G( b );
    return ERR_NONE;
}
DsoErr servCal::onPll1_1Reg( CArgument &arg )
{
    //! check type and size
    if ( !arg.checkType( val_int, val_int) )
    {
        return ERR_INVALID_INPUT;
    }

    getPll()->write1_1g( arg[0].uVal, arg[1].uVal );
    return ERR_NONE;
}
DsoErr servCal::onPll1_1Cfg( CArgument &arg )
{
    //! check type
    if ( !arg.checkType(val_arb_bin) )
    {
        return ERR_INVALID_INPUT;
    }

    //! check ptr
    if ( arg[0].arbVal.mPtr == NULL )
    {
        return ERR_INVALID_INPUT;
    }

    //! check size
    if ( arg[0].arbVal.mBlockLen > 0 &&
         arg[0].arbVal.mBlockLen % sizeof(pllRequest) == 0 )
    {}
    else
    {
        return ERR_INVALID_INPUT;
    }

    //! check size
    calPllData cfgData;
    if ( (uint)(arg[0].arbVal.mBlockLen) > sizeof(cfgData.mPayload.mRequest) )
    {
        return ERR_INVALID_INPUT;
    }

    //! deload data
    memcpy( cfgData.mPayload.mRequest,
            arg[0].arbVal.mPtr,
            arg[0].arbVal.mBlockLen );
    cfgData.mPayload.mLength = arg[0].arbVal.mBlockLen;

    //! save
    cfgData.save( mCalFileDbs[1].mPll1_1g,
                  &cfgData.mPayload,
                  sizeof(cfgData.mPayload) );

    return ERR_NONE;
}
DsoErr servCal::qPll1_1Cfg( CArgument &arg )
{
    calPllData cfgData;

    //! load from file
    if ( !cfgData.load( mCalFileDbs[1].mPll1_1g,
                        &cfgData.mPayload,
                        sizeof(cfgData.mPayload)) )
    {
        return ERR_CAL_READ_FAIL;
    }

    ArbBin abin( cfgData.mPayload.mLength,
                 cfgData.mPayload.mRequest );

    arg.setVal( abin );

    return ERR_NONE;
}

