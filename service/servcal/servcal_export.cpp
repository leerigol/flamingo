#include <unistd.h>
#include "servcal.h"


#define EXPORT_PATH  "/media/sda1/cal/"


DsoErr servCal::onExportUser()
{
    QFileInfo fi(EXPORT_PATH);

    if( !fi.exists() )
    {
        return ERR_CAL_EXPORT_FAIL;
    }

    iDelayExport();

    exportLf();

    for(int ch=0; ch<4; ch++)
    {
        exportHzScales(ch);
        exportHzGnds(ch);

        exportLzScales(ch);
        exportLzGnds(ch);
    }

    calAdcGoExport();

    return ERR_CAL_EXPORT_SUCCESS;
}
//k
void servCal::exportLf()
{
    calLfData data;
    QString inFile = "/rigol/data/lfcal.hex";
    QString outFile = QString("%1lf.csv").arg(EXPORT_PATH);

    if ( data.load( inFile, &data.mPayload, sizeof(data.mPayload) )
         != sizeof(data.mPayload) )
    {
        return;
    }

    QFile file(outFile);
    if ( !file.open( QIODevice::WriteOnly ) )
    {
        return;
    }

    QTextStream stream( &file );
    dso_phy::chHizLfTrim *pTrim;
    hizLfTrim *pLf;
    QString str;

    for ( int i = 0; i < 4; i++ )
    {
        pTrim = data.mPayload.mChHizTrim + i;
        str = QString("CH%1,%2").arg( i+1).arg( pTrim->mTrimCount );
        stream<<str<<endl;
        for( int j = 0; j < pTrim->mTrimCount; j++ )
        {
            pLf = pTrim->mLfTrims + j;
            str = QString("K,%1,R,%2,C,%3").arg( pLf->mK ).arg(pLf->mRSel).arg(pLf->mCSel);
            stream<<str<<endl;
            str = QString("dc,%1,lf,%2,mfc,%3,hf,%4").arg( pLf->mTrims[0] ).
                                                      arg( pLf->mTrims[1] ).
                                                      arg( pLf->mTrims[2] ).
                                                      arg( pLf->mTrims[3] );
            stream<<str<<endl;
        }
    }

    fsync(file.handle());
    file.close();
}

void servCal::exportHzScales( int chId )
{
    QFile file( QString("%1hzscale%2.csv").arg(EXPORT_PATH).arg(chId) );

    if ( !file.open( QIODevice::WriteOnly ) )
    {
        qWarning()<<"!!!Fail write exportHzScales";
        return;
    }

    QTextStream stream( &file );
    afeCfgPair *pCfgPair;

    stream << "hzK,hzVos,hzGs,hzAtten,hzSga4,hzSga4Trim,hzViewScale,hzRealScale" << endl;
    for ( int i = 0; i < mCalPara.mPayload.mCHs[chId].mHzScales; i++ )
    {
        pCfgPair = mCalPara.mPayload.mCHs[chId]._hzScales + i;

        pCfgPair->exportHz( stream );
    }

    fsync(file.handle());
    file.close();
}

void servCal::exportHzGnds( int chId )
{
    QFile file( QString("%1hzgnd%2.csv").arg(EXPORT_PATH).arg(chId));

    if ( false == file.open( QIODevice::WriteOnly ) )
    {
        qWarning()<<"!!!Fail write exportHzGnds";
        return;
    }

    QTextStream stream( &file );
    afeCfgPair *pCfgPair;
    stream << "hzK,hzVos,hzGs,hzAtten,hzSga4,hzSga4Trim,hzViewScale,hzRealScale" << endl;
    for ( int i = 0; i < mCalPara.mPayload.mCHs[chId].mHzGnds; i++ )
    {
        pCfgPair = mCalPara.mPayload.mCHs[chId]._hzGnds + i;

        pCfgPair->exportHz( stream );
    }

    fsync(file.handle());
    file.close();
}

void servCal::exportLzScales( int chId )
{
    exportLzScales( QString("%1lzscale_2x_normal%2.csv").arg(EXPORT_PATH).arg(chId),
                    mCalPara.mPayload.mCHs[chId].mLzScales_2x_normal,
                    mCalPara.mPayload.mCHs[chId]._lzScales_2x_normal );

    exportLzScales( QString("%1lzscale_2x_flt%2.csv").arg(EXPORT_PATH).arg(chId),
                    mCalPara.mPayload.mCHs[chId].mLzScales_2x_flt,
                    mCalPara.mPayload.mCHs[chId]._lzScales_2x_flt );

    exportLzScales( QString("%1lzscale_20x_normal%2.csv").arg(EXPORT_PATH).arg(chId),
                    mCalPara.mPayload.mCHs[chId].mLzScales_20x_normal,
                    mCalPara.mPayload.mCHs[chId]._lzScales_20x_normal );

    exportLzScales( QString("%1lzscale_20x_flt%2.csv").arg(EXPORT_PATH).arg(chId),
                    mCalPara.mPayload.mCHs[chId].mLzScales_20x_flt,
                    mCalPara.mPayload.mCHs[chId]._lzScales_20x_flt );
}

void servCal::exportLzScales(
                              const QString &fileName,
                              int count,
                              afeCfgPair *pPairs )
{
    QFile file( fileName );

    if  ( !file.open( QIODevice::WriteOnly ) )
    {
        qWarning()<<"!!!Fail write exportLzScales";
        return;
    }

    QTextStream stream( &file );
    afeCfgPair *pCfgPair;
    stream << "lzAtt,lzSga1,lzSga1Trim,lzSga2,lzSga2Trim,lzSga3,lzSga3Trim,lzFlt,lzSga4,lzSga4Trim,viewScale,realView" << endl;
    for ( int i = 0;
          i < count;
          i++ )
    {
        pCfgPair = pPairs + i;

        pCfgPair->exportLz( stream );
    }

    fsync(file.handle());
    file.close();
}

void servCal::exportLzGnds( int chId )
{
    QFile file( QString("%1lzgnd%2.csv").arg(EXPORT_PATH).arg(chId) );

    if ( false == file.open( QIODevice::WriteOnly ) )
    {
        qWarning()<<"!!!Fail write exportLzGnds";
        return;
    }

    QTextStream stream( &file );
    afeCfgPair *pCfgPair;
    stream << "lzAtt,lzSga1,lzSga1Trim,lzSga2,lzSga2Trim,lzSga3,lzSga3Trim,lzFlt,lzSga4,lzSga4Trim,viewScale,realView" << endl;
    for ( int i = 0; i < mCalPara.mPayload.mCHs[chId].mLzGnds; i++ )
    {
        pCfgPair = mCalPara.mPayload.mCHs[chId]._lzGnds + i;

        pCfgPair->exportLz( stream );
    }

    fsync(file.handle());
    file.close();
}

void servCal::exportvScales( const QString &fileName,
                    vScales &scales )
{
    QFile file( fileName );

    if ( file.open(QIODevice::WriteOnly) )
    {}
    else
    { return; }

    QTextStream stream(&file);

    vScale *pScale;
    foreach( pScale, scales )
    {
        pScale->exportOut( stream );
    }

    fsync(file.handle());
    file.close();
}

//adc core
DsoErr servCal::calAdcGoExport()
{
    QString filename = QString("%1go.csv").arg(EXPORT_PATH);
    QFile file( filename );
    if ( file.open(QIODevice::WriteOnly) )
    {}
    else
    { return ERR_CAL_WRITE_FAIL; }

    QTextStream stream( &file );

    mCalAdcGo.mPayload.mCalGp.exportOut( stream );

    fsync(file.handle());
    file.close();

    return ERR_NONE;
}

DsoErr servCal::iDelayExport()
{
    //max two adc
    for(int i=0; i<2; i++)
    {
        QString filename = QString("%1ADC%2_iDelay.csv").arg(EXPORT_PATH).arg(i+1);
        QFile file( filename );
        if ( file.open(QIODevice::WriteOnly) )
        {}
        else
        {
            return ERR_CAL_WRITE_FAIL;
        }

        QTextStream stream( &file );

        mCalHori.mPayload.mIcIDelay[i].exportOut(stream);

        fsync(file.handle());
        file.close();
    }
    return ERR_NONE;
}
