#ifndef SERVCAL_LF
#define SERVCAL_LF

#include "../../phy/ch/chcal.h"

#include "../../baseclass/checkedstream/ccheckedstream.h"

struct calLfDataPayload
{
    dso_phy::chHizLfTrim mChHizTrim[ 4 ];
};

struct calLfData : public CCheckedStream
{
    calLfDataPayload mPayload;
};

#endif // SERVCAL_LF

