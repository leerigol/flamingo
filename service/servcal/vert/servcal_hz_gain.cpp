
#include "../servcal.h"

void servCal::preHizCal( int chId, int vosExp )
{
    IPhyAfe *pAfe;

    pAfe = getAfe( chId );

    pAfe->setOlEn( 0 );
    pAfe->setHzNpd( 1 );
    pAfe->setLzNpd( 0 );

    pAfe->setOSel( 2 ); // HighZ

    pAfe->setK( 31 );           //! turn off k
    pAfe->setVosExp( vosExp );
    pAfe->setAtten( 0 );

    pAfe->flushWCache();
}

void servCal::preHizSga1Gain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setAtten( 0 );

    pAfe->setGtrim4( 0, 15 );
    pAfe->setGtrim5( 0, 15 );

    pAfe->flushWCache();
}

DsoErr servCal::hizSga1Gain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    int deltas[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(deltas); i++ )
    {
        pAfe->setHzSga1Gs( i );
        pAfe->flushWCache();

        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, deltas + i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    for ( int i = 0; i < array_count(deltas); i++ )
    {
        Q_ASSERT( deltas[i] != 0 );

        fRatio = (float)deltas[0]/deltas[i];

        pChCal->mHzLgHg[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::preHizGainAtten( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setHzSga1Gs( 0 );

    pAfe->setGtrim4( 0, 15 );
    pAfe->setGtrim5( 0, 15 );

    pAfe->flushWCache();
}
DsoErr servCal::hizGainAtten( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    int attens[]={ 0, 1, 2 ,3, 4, 5 };
    int delta;
    float fDelta;
    for ( int i = 0; i < array_count(attens); i++ )
    {
        pAfe->setAtten( attens[i] );
        pAfe->flushWCache();

        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta, fDelta );
        if ( err != ERR_NONE )
        {
            LOG_DBG()<<attens[i];
            return err;
        }

        Q_ASSERT( 0 != delta );

        //! save delta
        pChCal->_hzAtts[ i ].mKey = attens[i];
        pChCal->_hzAtts[i].mVal = delta;
    }

    //! save count
    pChCal->mhzAtts = array_count( attens );

    //! calc the ratio
    float base;
    base = pChCal->_hzAtts[0].mVal;
    for ( int i = 0; i < array_count(attens); i++ )
    {
        pChCal->_hzAtts[i].mVal =  base / pChCal->_hzAtts[i].mVal;

        LOG()<<pChCal->_hzAtts[i].mVal<<i;
    }

    return ERR_NONE;
}

void servCal::preHizSga45Gain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    //! select lg
    pAfe->setHzSga1Gs( 0 );

    pAfe->setAtten( 0 );
}

DsoErr servCal::hizSga4Gain( int chId )
{
    DsoErr err;
    float ratio;
    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    err = sga4Sel( chId, ratio );
    if ( ERR_NONE != err )
    { return err; }

    pChCal->mHzSga4Sel[0] = 1.0f;
    pChCal->mHzSga4Sel[1] = 1 / ratio;

    LOG()<<pChCal->mHzSga4Sel[1];

    return ERR_NONE;
}

DsoErr servCal::hizSga5Gain( int chId )
{
    DsoErr err;
    float ratio;
    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    err = sga5Sel( chId, ratio );
    if ( ERR_NONE != err )
    { return err; }

    pChCal->mHzSga5Sel[0] = 1.0f;
    pChCal->mHzSga5Sel[1] = 1 / ratio;

    LOG()<<pChCal->mHzSga5Sel[1];

    return ERR_NONE;
}

DsoErr servCal::hizSga4Trim( int chId )
{
    DsoErr err;
    float ratio;
    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    err = sga4Trim( chId, 0, 15, ratio );
    if ( ERR_NONE != err )
    { return err; }

    pChCal->mHzSga4Trim[0] = ratio;
    pChCal->mHzSga4Trim[1] = 1.0f;
    LOG_DBG()<<ratio;

    return ERR_NONE;
}
DsoErr servCal::hizSga5Trim( int chId )
{
    DsoErr err;
    float ratio;
    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    err = sga5Trim( chId, 0, 15, ratio );
    if ( ERR_NONE != err )
    { return err; }

    pChCal->mHzSga5Trim[0] = ratio;
    pChCal->mHzSga5Trim[1] = 1.0f;
    LOG_DBG()<<ratio;

    return ERR_NONE;
}

void servCal::hizSga4Clone( int chId )
{
    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    memcpy( pChCal->mHzSga4Sel,
            pChCal->mHzSga5Sel,
            array_count( pChCal->mHzSga4Sel ) *
            sizeof( pChCal->mHzSga4Sel[0]) );

    memcpy( pChCal->mHzSga4Trim,
            pChCal->mHzSga5Trim,
            array_count( pChCal->mHzSga4Trim ) *
            sizeof( pChCal->mHzSga4Trim[0]) );

}
void servCal::hizSga5Clone( int chId )
{
    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    memcpy( pChCal->mHzSga5Sel,
            pChCal->mHzSga4Sel,
            array_count( pChCal->mHzSga4Sel ) *
            sizeof( pChCal->mHzSga4Sel[0]) );

    memcpy( pChCal->mHzSga5Trim,
            pChCal->mHzSga4Trim,
            array_count( pChCal->mHzSga4Trim ) *
            sizeof( pChCal->mHzSga4Trim[0]) );
}

void servCal::preHizRefScale( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    //! select lg
    pAfe->setHzSga1Gs( 0 );

    pAfe->setAtten( 0 );

    pAfe->setGtrim4( 0, 15 );
    pAfe->setGtrim5( 0, 15 );

    pAfe->flushWCache();
}

DsoErr servCal::hizRefScale( int chId )
{
    DsoErr err;

    dso_phy::chCal* pChCal = &(mCalPara.mPayload.mCHs[chId]);

    int delta;
    float fDelta;

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta, fDelta );
    if ( ERR_NONE != err )
    { return err; }

    pChCal->mHzRefScale = delta * pChCal->mLsb1M / AMP_DIV;

    LOG()<<pChCal->mHzRefScale<<pChCal->mLsb1M<<delta;

    return ERR_NONE;
}

DsoErr servCal::hizScaleDeduce( int chId )
{
    DsoErr err;
    vScales scales;

    err = hizScaleDeduce_fill( chId, scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    scales.sort( vScaleLessThan );
    scales.normalize();

    //! export to rigol
#ifdef _DEBUG
    LOG()<<scales.size();
    exportvScales("/tmp/all_hz_scale.csv", scales );
#endif

    //! clear incorrect K
    err = hizScale_KFilter( scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

#ifdef _DEBUG
    LOG()<<scales.size();
    exportvScales("/tmp/filter_k_scale.csv", scales );
#endif

    //! trim by snr
    err = hizScaleDeduce_trim( scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

#ifdef _DEBUG
    LOG()<<scales.size();
    exportvScales("/tmp/trim_scale.csv", scales );
#endif

    vScale *pViewScale = scales.at(0);
    if( 5000 < pViewScale->trimVal )
    {
        vScale *pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 5000;
        pScale->mBw = BW_OFF;
        scales.insert(0,pScale);

    }

    if( 2000 < pViewScale->trimVal )
    {
        vScale *pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 2000;
        pScale->mBw = BW_OFF;
        scales.insert(0,pScale);
    }

    if( 1000 < pViewScale->trimVal )
    {
        vScale *pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 1000;
        pScale->mBw = BW_OFF;
        scales.insert(0,pScale);
    }

    err = hizScaleDeduce_export( chId, scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    scales.gc();

    return ERR_NONE;
}

DsoErr servCal::hizScaleDeduce_fill( int chId,
                                     vScales &scales )
{
    chCal *pChCal;
    int i;
    Q_ASSERT( chId >= 0 && chId < 4 );

    pChCal = mCalPara.mPayload.mCHs + chId;

    //! data init
    QList<vMul> ks;
    for ( i= 0; i < pChCal->mhzKs; i++ )
    {
        ks.append( vMul( pChCal->_hzKs[i].mKey,
                         pChCal->_hzKs[i].mVal) );
    }

    QList<vMul> lgHg;
    lgHg.append( vMul(0, pChCal->mHzLgHg[0]) );
    lgHg.append( vMul(1, pChCal->mHzLgHg[1]) );

    QList<vMul> atten;
    for( i = 0; i < pChCal->mhzAtts; i++ )
    {
        atten.append( vMul( pChCal->_hzAtts[i].mKey,
                            pChCal->_hzAtts[i].mVal) );
    }

    //! deduce from sga4
    QList<vMul> sga4;
    sga4.append( vMul(0,pChCal->mHzSga4Sel[0]) );
    sga4.append( vMul(1,pChCal->mHzSga4Sel[1]) );

    QList<vMul> sga4Trim;
    for ( int i = 0; i < 16 ; i++ )
    {
        sga4Trim.append( vMul( i,
                               i*(1.0 - pChCal->mHzSga4Trim[0])/15
                               + pChCal->mHzSga4Trim[0])
                         );
    }

    vScale *pBaseScale = new vScale();
    Q_ASSERT( NULL != pBaseScale );
    pBaseScale->mVal = pChCal->mHzRefScale*lsb_base;

    //! seed
    vScales baseList;
    baseList.append( pBaseScale );

    //! expand
    vScales dst;

    vCascade::cascade_div( baseList,
                           sga4Trim,
                           dst );
    baseList = dst;
    dst.clear();

    vCascade::cascade_div( baseList,
                           sga4,
                           dst );
    baseList = dst;
    dst.clear();

    vCascade::cascade_div( baseList,
                           atten,
                           dst,
                           opab_ov_range );
    baseList = dst;
    dst.clear();

    float dsaAtt;
    dsaAtt = pChCal->_hzAtts[5].mVal;
    vCascade::cascade_div( baseList,
                           lgHg,
                           dst,
                           opab_ov_range/dsaAtt );
    baseList = dst;
    dst.clear();

    vCascade::cascade_div( baseList,
                           ks,
                           dst,
                           (opab_ov_range/dsaAtt)/hzsga1_mul0 );
    baseList = dst;
    dst.clear();

    scales = baseList;

    return ERR_NONE;
}

DsoErr servCal::hizScaleDeduce_trim(
                     vScales &scales )
{
    QList<int> allScale;

    genvScale::genScale( allScale, CAL_MIN_SCALE, CAL_MAX_SCALE );

    //! trim scales
    scales.trim( allScale, hzvScaleSnr );

    int i = scales.size() - 1;

    vScale *s = NULL;
    for(i=i; i>0; i--)
    {
        s = scales.at(i);
        if( s->trimVal > CAL_MAX_SCALE || s->trimVal == 0)
        {
            scales.removeAt(i);
        }
    }

    return ERR_NONE;
}

struct kFilter
{
    int scaleL, scaleH; //! (L, H]
    int k;
};
static kFilter _KFilters[] =
{
    { 0,      50000, 0 },       //! ~50mv
    { 50000,  260000, 16 },     //! ~260mv
    { 260000, 1000000, 24 },    //! ~1v
    { 1000000, 3500000, 28 },   //! ~ 3.5v
    { 3500000, 6000000, 29 },   //! ~ 6.0v
    { 6000000, 10000000, 30 },  //! ~ 10.0v
};

DsoErr servCal::hizScale_KFilter( vScales &scales )
{
    vScale *pScale;
    int i;

    foreach( pScale, scales )
    {
        Q_ASSERT( NULL != pScale );

        for ( i = 0; i < array_count(_KFilters); i++ )
        {
            //! in range
            if ( pScale->miVal > _KFilters[i].scaleL
                 && pScale->miVal <= _KFilters[i].scaleH )
            {
                //! k not match
                if ( pScale->keys[0] != _KFilters[i].k )
                {
                    scales.removeAll( pScale );
                    delete pScale;
                    //! for next find
                    break;
                }
            }
        }
    }

    return ERR_NONE;
}

DsoErr servCal::hizScaleDeduce_export( int chId,
                            vScales &scales)
{
    chCal *pChCal;
    int i;
    Q_ASSERT( chId >= 0 && chId < 4 );

    pChCal = mCalPara.mPayload.mCHs + chId;

    vScale *pVScale;
    afeCfgPair *pScaleCfgPair;
    i = 0;
    foreach( pVScale, scales )
    {
        Q_ASSERT( NULL != pVScale );

        pScaleCfgPair = pChCal->_hzScales + i;

        pScaleCfgPair->setHzCfg(
                    pVScale->keys[0],
                    0,                  //! vos no impact
                    pVScale->keys[1],
                    pVScale->keys[2],
                    pVScale->keys[3],
                    pVScale->keys[4],

                    pVScale->keys[3],   //! sga5
                    pVScale->keys[4]    //! sga5 trim
                    );

        //! scale value
        pScaleCfgPair->setValue( pVScale->mVal );
        pScaleCfgPair->setValue( pVScale->trimVal );

        i++;
    }

    pChCal->mHzScales = i;

    return ERR_NONE;
}
