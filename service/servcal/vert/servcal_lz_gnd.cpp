#include "../servcal.h"

#define LZ_MID  ( 255-128 )


DsoErr servCal::lozGnd( int chId )
{
    DsoErr err;
    int g;

    g = 0;
    //! foreach scale
    {
        //! normal
        err = lozGnd( chId,
                      mCalPara.mPayload.mCHs[ chId ]._lzScales_2x_normal,
                      mCalPara.mPayload.mCHs[ chId ].mLzScales_2x_normal,
                      0,
                      g,
                      1000, 100000 );
        if ( err != ERR_NONE )
        {
            return err;
        }

        //return ERR_NONE;/////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        err = lozGnd( chId,
                      mCalPara.mPayload.mCHs[ chId ]._lzScales_20x_normal,
                      mCalPara.mPayload.mCHs[ chId ].mLzScales_20x_normal,
                      0,
                      g,
                      100001, 1000000 );
        if ( err != ERR_NONE )
        {
            return err;
        }


        //! filtered
        err = lozGnd( chId,
                      mCalPara.mPayload.mCHs[ chId ]._lzScales_2x_flt,
                      mCalPara.mPayload.mCHs[ chId ].mLzScales_2x_flt,
                      1,
                      g,
                      1000, 100000 );
        if ( err != ERR_NONE )
        {
            return err;
        }

        err = lozGnd( chId,
                      mCalPara.mPayload.mCHs[ chId ]._lzScales_20x_flt,
                      mCalPara.mPayload.mCHs[ chId ].mLzScales_20x_flt,
                      1,
                      g,
                      100001, 1000000 );
        if ( err != ERR_NONE )
        {
            return err;
        }
    }

    //! save count
    mCalPara.mPayload.mCHs[ chId ].mLzGnds = g;
    LOG_DBG()<<g;

    return ERR_NONE;
}

DsoErr servCal::lozGnd( int chId,
                        afeCfgPair *pScales,
                        int scaleCnt,
                        int flt,
                        int &g,
                        int scaleMin,
                        int scaleMax )
{
    Q_ASSERT( NULL != pScales );

    IPhyCH *pCH;
    pCH = getCH( chId );

    DsoErr err;
    int dac;
    float val;

    afeCfgPair *pLzPair, *pGndPair;
    afeCfg *pLzCfg;

    //! foreach scale
    dac = 32768;
    for ( int i = 0; i < scaleCnt; i++ )
    {
        //! scale
        pLzPair = pScales + i;

        //! check range
        if ( pLzPair->mVal >= scaleMin && pLzPair->mVal <= scaleMax )
        {}
        else
        {
            continue;
        }

        //! config
        pCH->setLzScaleOnly( pLzPair->mVal, flt, pLzPair );
        pCH->flushWCache();

        if( pLzPair->mVal > 2000 ) //more than 10mV use normal
        {
            //return ERR_NONE;//!!!!!!!!!!!!!!!!!!!!!!!!!!!

            //! get gnd
            err = iterDac( 0,
                           chId,
                           chId,
                           ADC_MID,
                           dac,
                           val,
                           gnd_mid - gnd_bias,
                           gnd_mid + gnd_bias,
                           dac );
        }
        else
        {
            IPhyCH *pCH = getCH( chId );
            if(pLzPair->mCfg.lzBw == BW_OFF)
            {
                pCH->setCalBw( BW_OFF, IMP_50 );
            }
            else
            {
                pCH->setCalBw( BW_20M, IMP_50 );
            }
            pCH->flushWCache();

            //! get gnd
            err = iterDac1( 0,
                           chId,
                           chId,
                           ADC_MID,
                           dac,
                           val,
                           gnd_mid - gnd_bias,
                           gnd_mid + gnd_bias,
                           dac,
                           pLzPair);

            if(pLzPair->mCfg.lzBw == BW_OFF)
            {
                pCH->setCalBw( BW_20M, IMP_50 );
                pCH->flushWCache();
            }
        }

        if ( err != ERR_NONE )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3/%4").arg(chId).arg(flt).arg(pLzPair->mVal).arg(dac) );
            return err;
        }

        //! save gnd
        Q_ASSERT( g < array_count(mCalPara.mPayload.mCHs[ chId ]._lzGnds) );
        pGndPair = mCalPara.mPayload.mCHs[ chId ]._lzGnds + g;
        pGndPair->clear();
        g++;

        pLzCfg = &pLzPair->mCfg;

        //! save result
        pGndPair->setValue( dac );
        pGndPair->setValue( val );
        pGndPair->setLzCfg( pLzCfg->lzAtt,
                            pLzCfg->lzSga1, pLzCfg->lzSga1Trim,
                            pLzCfg->lzSga2, pLzCfg->lzSga2Trim,
                            pLzCfg->lzSga3, pLzCfg->lzSga3Trim,
                            flt,
                            pLzCfg->lzSga4, pLzCfg->lzSga4Trim,
                            pLzCfg->lzSga5, pLzCfg->lzSga5Trim,
                            pLzCfg->lzBw);
    }

    return ERR_NONE;
}


//no used
void servCal::preLozOffset( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    //! to lz
    pAfe->setOlEn( 1 );
    pAfe->setVosExp( 1 );

}

// no used
DsoErr servCal::lozOffset( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int atts[] = {1,0};

    int sga1[] = {0,1};
    int sga1Trim[]={/*0,*/1};

    int sga2[] = {0,1,2};
    int sga2Trim[]={/*0,1,2,*/3};

    int sga3[] = {0,1};
    int sga3Trim[]={/*0,1,2,*/3};

    int flt[] = {0,1};
    int sga45[] = {0,1};

    int i, j, k, m, n, p, g;
    int dac;
    float val;

    afeCfgPair *pCfgPair;
    g = 0;
    dac = 32767;
    for ( i = 0; i < array_count(atts); i++ )
    {
        pAfe->setGpo1( atts[i] );
        for ( j = 0; j < array_count(sga1); j++ )
        {
            for( int jj = 0; jj < array_count(sga1Trim); jj++ )
            {
                pAfe->setGtrim1( sga1[j], sga1Trim[jj] );
                for ( k = 0; k < array_count(sga2); k++ )
                {
                    for ( int kk = 0; kk < array_count(sga2Trim); kk++ )
                    {
                        pAfe->setGtrim2( sga2[k], sga2Trim[kk] );
                        for ( m = 0; m < array_count(sga3); m++ )
                        {
                            for ( int mm = 0; mm < array_count(sga3Trim); mm++ )
                            {
                                pAfe->setGtrim3( sga3[m], 3 );
                                for ( n = 0; n < array_count(flt); n++ )
                                {
                                    pAfe->setOSel( flt[n] );
                                    for ( p = 0; p < array_count(sga45); p++ )
                                    {
                                        pAfe->setGtrim4( sga45[p],
                                                         15 );
                                        pAfe->flushWCache();

                                        err = iterDac( 0, chId, chId, ADC_MID, dac, val,
                                                       32767 - gnd_bias,
                                                       32767 + gnd_bias,
                                                       dac );
                                        if ( err != ERR_NONE )
                                        { LOG_DBG()<<i<<j<<jj<<k<<kk<<m<<mm; return err; }

                                        //! overflow?
                                        Q_ASSERT( g < array_count(mCalPara.mPayload.mCHs[ chId ]._lzGnds) );

                                        //! pcfg pair
                                        pCfgPair = mCalPara.mPayload.mCHs[ chId ]._lzGnds + g;

                                        pCfgPair->clear();

                                        //! save result
                                        pCfgPair->setValue( dac );
                                        pCfgPair->setValue( val );
                                        pCfgPair->setLzCfg( atts[i],
                                                            sga1[j], sga1Trim[jj],
                                                            sga2[k], sga2Trim[kk],
                                                            sga3[m], sga3Trim[mm],
                                                            flt[n],
                                                            sga45[p], 15,
                                                            sga45[p], 15 );

                                        g++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //! for norm scales
    int intScales[]={ 2000, 5000, 10000,
                      20000, 50000, 100000,
                      200000, 500000, 1000000,
                    };
    chCalProxy proxy( &mCalPara.mPayload.mCHs[ chId ] );

    IPhyCH *pCH;
    pCH = getCH( chId );

    afeCfgPair *pLzPair;
    afeCfg *pLzCfg;

    dac = 32768;
    for ( i = 0; i < array_count(flt); i++ )
    {
        for ( j = 0; j < array_count(intScales); j++ )
        {
            //! int scales
            pCfgPair = proxy.findLzScale( intScales[j], flt[i] );
            if ( NULL == pCfgPair )
            { continue; }

            //! apply
            pCH->setLzScaleOnly( intScales[j], flt[i], pCfgPair );
            pCH->flushWCache();

            //! get gnd
            err = iterDac( 0, chId, chId, ADC_MID, dac, val,
                           32767 - gnd_bias,
                           32767 + gnd_bias,
                           dac );
            if ( err != ERR_NONE )
            { return err; }

            //! save gnd
            Q_ASSERT( g < array_count(mCalPara.mPayload.mCHs[ chId ]._lzGnds) );
            pLzPair = mCalPara.mPayload.mCHs[ chId ]._lzGnds + g;
            pLzPair->clear();

            pLzCfg = &pCfgPair->mCfg;

            //! save result
            pLzPair->setValue( dac );
            pLzPair->setValue( val );
            pLzPair->setLzCfg(  pLzCfg->lzAtt,
                                pLzCfg->lzSga1, pLzCfg->lzSga1Trim,
                                pLzCfg->lzSga2, pLzCfg->lzSga2Trim,
                                pLzCfg->lzSga3, pLzCfg->lzSga3Trim,
                                flt[i],
                                pLzCfg->lzSga4, pLzCfg->lzSga4Trim,
                                pLzCfg->lzSga5, pLzCfg->lzSga5Trim );

            g++;
        }
    }

    //! save count
    mCalPara.mPayload.mCHs[ chId ].mLzGnds = g;

    return ERR_NONE;
}
