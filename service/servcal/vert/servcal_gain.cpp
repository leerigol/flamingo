
#include "../servcal.h"

DsoErr servCal::sga4Sel( int chId, float &ratio )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta1, delta2;
    float fDelta1, fDelta2;

    pAfe->setGtrim4( 0, 15 );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta1, fDelta1 );
    if ( err != ERR_NONE ) return err;

    pAfe->setGtrim4( 1, 15 );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta2, fDelta2 );
    if ( err != ERR_NONE ) return err;

    //! Gain1/Gain0
    Q_ASSERT( delta1 != 0 );
    ratio = fDelta2/fDelta1;

    return ERR_NONE;
}
DsoErr servCal::sga4Trim( int chId, int a, int b, float &ratio )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta1, delta2;
    float fDelta1, fDelta2;

    pAfe->setGtrim4( 0, a );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta1, fDelta1 );
    if ( err != ERR_NONE ) return err;

    pAfe->setGtrim4( 0, b );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta2, fDelta2 );
    if ( err != ERR_NONE ) return err;

    Q_ASSERT( delta1 != 0 );
    ratio = fDelta2/fDelta1;

    return ERR_NONE;
}

DsoErr servCal::sga5Sel( int chId, float &ratio )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta1, delta2;
    float fDelta1, fDelta2;

    pAfe->setGtrim5( 0, 15 );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta1, fDelta1 );
    if ( err != ERR_NONE ) return err;

    pAfe->setGtrim5( 1, 15 );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta2, fDelta2 );
    if ( err != ERR_NONE ) return err;

    Q_ASSERT( delta1 != 0 );
    ratio = fDelta2/fDelta1;

    return ERR_NONE;
}

DsoErr servCal::sga5Trim( int chId, int a, int b, float &ratio )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta1, delta2;
    float fDelta1, fDelta2;

    pAfe->setGtrim5( 0, a );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta1, fDelta1 );
    if ( err != ERR_NONE ) return err;

    pAfe->setGtrim5( 0, b );
    pAfe->flushWCache();

    err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta2, fDelta2 );
    if ( err != ERR_NONE ) return err;

    Q_ASSERT( delta1 != 0 );
    ratio = fDelta2/fDelta1;

    return ERR_NONE;
}



