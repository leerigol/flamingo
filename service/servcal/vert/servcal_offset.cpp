
#include "../servcal.h"

void servCal::preHizLsbLg( int chId )
{
    cplatform *platform;
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    platform->m_pPhy->mVGA[ chId ].setK( 0 );
    platform->m_pPhy->mVGA[ chId ].setHzSga1Gs( 0 );
    platform->m_pPhy->mVGA[ chId ].flushWCache();
}
void servCal::preHizLsbHg( int chId )
{
    cplatform *platform;
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    platform->m_pPhy->mVGA[ chId ].setK( 0 );
    platform->m_pPhy->mVGA[ chId ].setHzSga1Gs( 1 );
    platform->m_pPhy->mVGA[ chId ].flushWCache();
}

DsoErr servCal::dacLsb( int chId )
{
    DsoErr err;
    int dac;
    float val;

    err = iterDac( 0, chId, chId, ADC_MID, dac, val );
    if ( err != ERR_NONE ) return err;

//    qDebug()<<dac<<val;

    return ERR_NONE;
}

void servCal::preLozLsb1_2( int chId, int sel, int trim )
{
    cplatform *platform;
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    platform->m_pPhy->mVGA[ chId ].setGpo1( 1 );
    platform->m_pPhy->mVGA[ chId ].setISel( 2 );

    platform->m_pPhy->mVGA[ chId ].setGtrim1( sel, trim );
}
void servCal::preLozLsb1_20( int chId, int sel, int trim  )
{
    cplatform *platform;
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    platform->m_pPhy->mVGA[ chId ].setGpo1( 0 );
    platform->m_pPhy->mVGA[ chId ].setISel( 0 );

    platform->m_pPhy->mVGA[ chId ].setGtrim1( sel, trim );
}

