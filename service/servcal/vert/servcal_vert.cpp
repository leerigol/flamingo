#include "../servcal.h"

DsoErr servCal::vertSave()
{
    qint32 size = sizeof(mCalPara.mPayload );
    if ( size != mCalPara.save( mCalFileDbs[1].mVert, &mCalPara.mPayload, size ) )
    {
        return ERR_CAL_WRITE_FAIL;
    }
    return ERR_NONE;
}
DsoErr servCal::vertLoad( const QString &fileName )
{
    //! new
    calData *temp=new calData;
    if ( NULL == temp )
    { return ERR_CAL_READ_FAIL; }

    //! load
    qint32 size = sizeof( mCalPara.mPayload );
    if ( size != temp->load( fileName, &temp->mPayload, size ) )
    {
        delete temp;
        return ERR_CAL_READ_FAIL;
    }

    //! export
    mCalPara = *temp;
    delete temp;

    return ERR_NONE;
}

//! 4 channel mode
#define wpu_gain    (2457)
#define wpu_offset  (-67)
void servCal::preVertCal()
{
    logInfo("enter VERT");

    {
        //! Reset important paramaters
        for ( int id = E_SERVICE_ID_CH4; id >= E_SERVICE_ID_CH1; id-- )
        {
            serviceExecutor::post( id, CMD_SERVICE_RST, 0 );
        }

        for ( int id = E_SERVICE_ID_CH4; id >= E_SERVICE_ID_CH1; id-- )
        {
            serviceExecutor::post( id, CMD_SERVICE_STARTUP, 0 );
        }
        QThread::sleep(2);
    }
    IPhyWpu *pWpu;

    pWpu = getWpu();
    //! 200~480
    pWpu->setanalog_ch0_gain( wpu_gain );
    pWpu->setanalog_ch0_offset( wpu_offset );

    pWpu->setanalog_ch1_gain( wpu_gain );
    pWpu->setanalog_ch1_offset( wpu_offset );

    pWpu->setanalog_ch2_gain( wpu_gain );
    pWpu->setanalog_ch2_offset( wpu_offset );

    pWpu->setanalog_ch3_gain( wpu_gain );
    pWpu->setanalog_ch3_offset( wpu_offset );

    pWpu->flushWCache();

#ifdef DAMREY_VER_1_5_x
    setSpuInvert(true);
#else
    setSpuInvert(false);
    qDebug() << "version";
#endif
    //! ch bw on
    IPhyCH *pCH;
    for ( int i = 0; i < 4; i++ )
    {
        pCH = getCH( i );

        pCH->setCalBw( BW_20M, IMP_1M );

        pCH->flushWCache();
    }

}
void servCal::postVertCal()
{
    logInfo("exit VERT");
}

//! -1 -- all channel
DsoErr servCal::vertCalProc( int progFrom, int steps,
                             int chId )
{
    preVertCal();

    DsoErr err = vertCal( progFrom, steps, chId );

    postVertCal();

    if ( err != ERR_NONE )
    {
        return err;
    }

    err = vertSave();

    return err;
}

DsoErr servCal::vertCal( int progFrom, int steps, int chId )
{
    int calFrom, calTo;

    if ( chId == -1 )
    {
        calFrom = 0;
        calTo = 3;
    }
    else if ( chId >= 0 || chId < 4 )
    {
        calFrom = chId;
        calTo = chId;
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    for ( int i = calFrom; i <= calTo; i++ )
    {
        m_nResult = i + cal_vert_ch1;
        if ( get_bit(mCalItems, i + cal_vert_ch1) )
        {}
        else
        {
            continue;
        }

        logInfo( __FUNCTION__, QString::number(i+1) );

        preSet( i );
        setProgNow( progFrom + ( i*4 + 0)*steps/16 );


        if( ERR_NONE != zeroCal( i ) )
        {
            logInfo(__FUNCTION__, "zeroCal");
        }
        setProgNow( progFrom + ( i*4 + 1)*steps/16 );

        if ( get_bit(mCalItems, cal_vert_1M) )
        {
            #ifdef DAMREY_VER_1_5_x
            setSpuInvert(true);
            #endif

            hizCal( i );
            setProgNow( progFrom + ( i*4 + 2)*steps/16 );

            //restore default;
            IPhyCH *pCH = getCH( i );
            pCH->setImpedance( IMP_1M );
            pCH->setScale( mv(100) );
            pCH->setOffset( 0 );
            QThread::msleep(100);
        }

        if ( get_bit(mCalItems, cal_vert_50) )
        {
            #ifdef DAMREY_VER_1_5_x
            //! spu invert off
            setSpuInvert(false);
            #endif

            lozCal( i );
            setProgNow( progFrom + ( i*4 + 3)*steps/16 );

            //restore default;
            IPhyCH *pCH = getCH( i );
            pCH->setImpedance( IMP_1M );
            pCH->setScale( mv(100) );
            pCH->setOffset( 0 );
            QThread::msleep(100);
        }
    }

    return ERR_NONE;
}

DsoErr servCal::zeroCal( int chId )
{
    DsoErr err;

    preZero( chId );

    err = zeroProc( chId );

    postZero( chId );

    return err;

}

void servCal::lozCal( int chId )
{
    //setAcqAvg(true);

    preLozCal( chId );

    //! gain
    preLozSga1_2xGain( chId );
    lozSga1_2xGain( chId );

    preLozSga1_20xGain( chId );
    lozSga1_20xGain( chId );

    preLozSga1Trim( chId );
    lozSga1Trim( chId );

    preLozSga2Gain( chId );
    lozSga2Gain( chId );
    lozSga2Trim( chId );

    preLozSga3Gain( chId );
    lozSga3Gain( chId );
    lozSga3Trim( chId );

    preLozSga45Gain( chId );
    lozSga4_4GGain( chId );
    lozSga5_4GGain( chId );

    preLozSga45FltGain( chId );
    lozSga4_FltGain( chId );
    lozSga5_FltGain( chId );

    preLozRefScale( chId );
    lozRefScale( chId );

#ifdef _DS8000
    lozSga4_4GClone( chId );
    lozSga4_FltClone( chId );
#endif

    //setAcqAvg(false);

    //generate all scale config
    lozScaleDeduce( chId );

    lozGnd( chId );
}

void servCal::hizCal( int chId )
{
    //setAcqAvg(true);

    //! pre set
    preHizCal( chId, 1 );

    //! gain
    preHizSga1Gain( chId );
    hizSga1Gain( chId );

    preHizGainAtten( chId );
    hizGainAtten( chId );

    preHizSga45Gain( chId );
    hizSga4Gain( chId );        //! get from vop_a
    hizSga5Gain( chId );        //! get from vop_b

    hizSga4Trim( chId );
    hizSga5Trim( chId );

    //! \todo ds8000-demo- sga5-> outb
    #ifdef _DS8000
    hizSga4Clone( chId );
    qWarning()<<"clone sga4";
    #endif

    //! scale
    preHizRefScale( chId );
    hizRefScale( chId );
    hizScaleDeduce( chId );


    //! offset
    preHizOffset( chId );

    DsoErr err = hizGnd( chId );
    if ( err != ERR_NONE )
    {
        logInfo(__FUNCTION__,__LINE__);
    }
}

void servCal::preSet( int chId )
{
    IPhyAfe *pAfe;

    pAfe = getAfe( chId );

    pAfe->setK( 31 );       //! turn off array
    pAfe->setAtten( 0 );
}

//! ---- lsb calcs
void servCal::calc1M_K50Lsb( int chid )
{
    preHizCal( chid, 0 );

    preHizLsbLg( chid );
    dacLsb( chid );

}
void servCal::calc1M_K51Lsb( int chid )
{
    preHizCal( chid, 1 );

    preHizLsbLg( chid );
    dacLsb( chid );
}
void servCal::calc50_2Lsb( int chId )
{
    preLozCal( chId );

    preLozSga1_2xGain( chId );

    dacLsb( chId );
}
void servCal::calc50_20Lsb( int chId )
{
    preLozCal( chId );

    preLozSga1_20xGain( chId );

    dacLsb( chId );
}
