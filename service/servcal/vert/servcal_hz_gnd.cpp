
#include "../servcal.h"

void servCal::preHizOffset( int chId )
{
    IPhyAfe *pAfe;

    pAfe = getAfe( chId );

    pAfe->setAtten( 0 );
}

DsoErr servCal::hizOffset( int /*chId*/ )
{
    return ERR_NONE;
}

DsoErr servCal::hizGnd( int chId )
{

    IPhyCH *pCH;
    pCH = getCH( chId );

    DsoErr err;
    int dac;
    float val;

    int g = 0;
    int vos = 0;

    //qDebug() << "All HiZ scale count:" << mCalPara.mPayload.mCHs[chId].mHzScales;

    afeCfgPair *pScalePair, *pGndPair;
    //! foreach scale
    dac = gnd_mid;
    for ( int i = 0; i < mCalPara.mPayload.mCHs[chId].mHzScales; i++ )
    {
        //! scale
        pScalePair = mCalPara.mPayload.mCHs[chId]._hzScales+i;

        //! check range
        if ( pScalePair->mVal >= 1000 && pScalePair->mVal <= 10000000 )
        {  }
        else
        { continue; }

        //! config
        vos = pCH->getHzVos( pScalePair->mVal );
        pCH->setHzScaleOnly( pScalePair->mVal, pScalePair );
        pCH->flushWCache();

        //! \note ic errant config again for vos fail
        pCH->setHzScaleOnly( pScalePair->mVal, pScalePair );
        pCH->flushWCache();

        if( pScalePair->mVal > 2000 )
        {
            int adc_mid = ADC_MID;
//            if( (pScalePair->mVal>5000 && pScalePair->mVal < 200000) /*||
//                pScalePair->mVal == 10000000*/ )
//            {
//                adc_mid += 1;
//            }
            err = iterDac( 0,
                           chId,
                           chId,
                           adc_mid,
                           dac,
                           val,
                           gnd_mid - gnd_bias,
                           gnd_mid + gnd_bias,
                           dac );
        }
        else
        {
            err = iterDac1( 0,
                            chId,
                            chId,
                            ADC_MID,
                            dac,
                            val,
                            gnd_mid - gnd_bias,
                            gnd_mid + gnd_bias,
                            dac );
        }
        if ( err != ERR_NONE )
        {
            logInfo( __FUNCTION__ );
            return err;
        }

        //! save the gnd
        //! overflow?
        Q_ASSERT( g < array_count(mCalPara.mPayload.mCHs[ chId ]._hzGnds) );

        //! pcfg pair
        pGndPair = mCalPara.mPayload.mCHs[ chId ]._hzGnds + g;
        pGndPair->clear();
        g++;

        //! save result
        pGndPair->setValue( dac );
        pGndPair->setValue( val );
        pGndPair->setHzCfg( pScalePair->mCfg.hzK,
                            vos,
                            pScalePair->mCfg.hzGs,
                            pScalePair->mCfg.hzAtten,
                            pScalePair->mCfg.hzSga4, pScalePair->mCfg.hzSga4Trim,
                            pScalePair->mCfg.hzSga5, pScalePair->mCfg.hzSga5Trim );

    }

    //! save count
    mCalPara.mPayload.mCHs[ chId ].mHzGnds = g;
    LOG_DBG()<<g;

    return ERR_NONE;
}

bool servCal::hzGndExist(
               afeCfgPair *pScale,
               int vos,
               afeCfgPair *pPairs,
               int count )
{
    afeCfgPair *pGndNow;

    for ( int i = 0; i < count; i++ )
    {
        pGndNow = pPairs + i;

        if ( vos != pGndNow->mCfg.hzVos )
        { continue; }

        if ( pScale->mCfg.hzK != pGndNow->mCfg.hzK )
        { continue; }

        if ( pScale->mCfg.hzGs != pGndNow->mCfg.hzGs )
        { continue; }

        if ( pScale->mCfg.hzAtten != pGndNow->mCfg.hzAtten )
        { continue; }

        if ( pScale->mCfg.hzSga4 != pGndNow->mCfg.hzSga4 )
        { continue; }

        if ( pScale->mCfg.hzSga4Trim != pGndNow->mCfg.hzSga4Trim )
        { continue; }

        return true;
    }

    return false;
}
