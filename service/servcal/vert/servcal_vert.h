#ifndef SERVCAL_VERT
#define SERVCAL_VERT

#include <QtCore>

#include "../../../phy/ch/chcal.h"

#include "../../baseclass/checkedstream/ccheckedstream.h"

struct calDataPayload
{
    dso_phy::chCal mCHs[4];
};

struct calData : public CCheckedStream
{    
    calDataPayload mPayload;
};

struct kAttenX
{
    int k;
    float x;
};

#endif // SERVCAL_VERT

