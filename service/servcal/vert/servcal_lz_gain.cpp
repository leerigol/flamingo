
#include "../servcal.h"

void servCal::preLozCal( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    //! to lz
    pAfe->setOlEn( 1 );
    pAfe->setHzNpd( 0 );
    pAfe->setLzNpd( 1 );

    //! 1/2 x
    pAfe->setGpo1( 0 );

    pAfe->setOSel( 0 );     //! 4g

    pAfe->setGtrim2( 0, 3 );
    pAfe->setGtrim3( 0, 3 );

    pAfe->setGtrim4( 0, 15 );
    pAfe->setGtrim5( 0, 15 );

    pAfe->flushWCache();
}

void servCal::preLozSga1_2xGain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setGpo1( 1 );     //! 6db
    pAfe->setISel( 2 );     //! Lz C

    pAfe->flushWCache();
}
DsoErr servCal::lozSga1_2xGain( int chId )
{
    DsoErr err;
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int deltas[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(deltas); i++ )
    {
        pAfe->setGtrim1( i, 1 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, deltas + i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(deltas); i++ )
    {
        Q_ASSERT( deltas[i] != 0 );

        fRatio = (float)deltas[0] / deltas[i];

        mCalPara.mPayload.mCHs[chId].mLzSga1[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::preLozSga1_20xGain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setGpo1( 0 );     //! 26db
    pAfe->setISel( 0 );     //! Lz A

    pAfe->flushWCache();
}
DsoErr servCal::lozSga1_20xGain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int deltas[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(deltas); i++ )
    {
        pAfe->setGtrim1( i, 1 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, deltas + i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(deltas); i++ )
    {
        Q_ASSERT( deltas[i] != 0 );

        fRatio = (float)deltas[0] / deltas[i];

        mCalPara.mPayload.mCHs[chId].mLzSga1_att[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::preLozSga1Trim( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setGpo1( 1 );     //! 6db
    pAfe->setISel( 2 );     //! Lz c

    pAfe->flushWCache();
}
DsoErr servCal::lozSga1Trim( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int deltas[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(deltas); i++ )
    {
        pAfe->setGtrim1( 0, i );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, deltas + i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(deltas); i++ )
    {
        Q_ASSERT( deltas[0] != 0 );

        fRatio = (float)deltas[array_count(deltas)-1] / deltas[i];

        mCalPara.mPayload.mCHs[chId].mLzSga1Trim[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::preLozSga2Gain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setGpo1( 1 );     //! 6db
    pAfe->setISel( 2 );     //! Lz c

    pAfe->flushWCache();
}

DsoErr servCal::lozSga2Gain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[3];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        //! sga2
        pAfe->setGtrim2( i, 3 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[0] != 0 );

        fRatio = (float)delta[0]/delta[i];
        mCalPara.mPayload.mCHs[chId].mLzSga2[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

DsoErr servCal::lozSga2Trim( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[4];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        pAfe->setGtrim2( 0, i );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[i] != 0 );

        fRatio = (float)delta[3]/delta[i];

        mCalPara.mPayload.mCHs[chId].mLzSga2Trim[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::preLozSga3Gain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setGpo1( 1 );     //! 6db
    pAfe->setISel( 2 );     //! Lz c

    pAfe->setGtrim1( 0, 1 );
    pAfe->setGtrim2( 0, 3 );

    pAfe->flushWCache();
}
DsoErr servCal::lozSga3Gain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        pAfe->setGtrim3( i, 3 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[i] != 0 );
        fRatio = (float)delta[0] / delta[i];

        mCalPara.mPayload.mCHs[chId].mLzSga3[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}
DsoErr servCal::lozSga3Trim( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[4];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        pAfe->setGtrim3( 0, i );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[i] != 0 );
        fRatio = (float)delta[3] / delta[i];

        mCalPara.mPayload.mCHs[chId].mLzSga3Trim[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::preLozSga45Gain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setGpo1( 1 );     //! 6db
    pAfe->setISel( 2 );     //! Lz c

    pAfe->setGtrim1( 0, 1 );
    pAfe->setGtrim2( 0, 3 );

    pAfe->setGtrim3( 0, 3 );

    pAfe->setOSel( 0 );     //! 4g

    pAfe->flushWCache();
}
DsoErr servCal::lozSga4_4GGain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        pAfe->setGtrim4( i, 15 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[i] != 0 );
        fRatio = (float)delta[0] / delta[i];

        mCalPara.mPayload.mCHs[chId].mLzSga4[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

DsoErr servCal::lozSga5_4GGain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        pAfe->setGtrim5( i, 15 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[i] != 0 );
        fRatio = (float)delta[0] / delta[i];

        mCalPara.mPayload.mCHs[chId].mLzSga5[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::preLozSga45FltGain( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setGpo1( 1 );     //! 6db
    pAfe->setISel( 2 );     //! Lz c

    pAfe->setGtrim1( 0, 1 );
    pAfe->setGtrim2( 0, 3 );

    pAfe->setGtrim3( 0, 3 );

    pAfe->setOSel( 0 );     //! 1g

    pAfe->flushWCache();
}
DsoErr servCal::lozSga4_FltGain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        pAfe->setGtrim4( i, 15 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[i] != 0 );
        fRatio = (float)delta[0] / delta[i];

        mCalPara.mPayload.mCHs[chId].mLzSga4Flt[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

DsoErr servCal::lozSga5_FltGain( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta[2];
    float fDelta;
    float fRatio;

    for ( int i = 0; i < array_count(delta); i++ )
    {
        pAfe->setGtrim5( i, 15 );
        err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta+i, &fDelta );
        if ( err != ERR_NONE ) return err;
    }

    //! save
    for ( int i = 0; i < array_count(delta); i++ )
    {
        Q_ASSERT( delta[i] != 0 );
        fRatio = (float)delta[0] / delta[i];

        mCalPara.mPayload.mCHs[chId].mLzSga5Flt[ i ] = fRatio;

        LOG()<<fRatio;
    }

    return ERR_NONE;
}

void servCal::lozSga4_4GClone( int chId )
{
    memcpy( mCalPara.mPayload.mCHs[chId].mLzSga4,
            mCalPara.mPayload.mCHs[chId].mLzSga5,
            array_count(mCalPara.mPayload.mCHs[chId].mLzSga4)*
            sizeof(mCalPara.mPayload.mCHs[chId].mLzSga4) );
}
void servCal::lozSga4_FltClone( int chId )
{
    memcpy( mCalPara.mPayload.mCHs[chId].mLzSga4Flt,
            mCalPara.mPayload.mCHs[chId].mLzSga5Flt,
            array_count(mCalPara.mPayload.mCHs[chId].mLzSga4Flt)*
            sizeof(mCalPara.mPayload.mCHs[chId].mLzSga4Flt) );
}

void servCal::lozSga5_4GClone( int chId )
{
    memcpy( mCalPara.mPayload.mCHs[chId].mLzSga5,
            mCalPara.mPayload.mCHs[chId].mLzSga4,
            array_count(mCalPara.mPayload.mCHs[chId].mLzSga4)*
            sizeof(mCalPara.mPayload.mCHs[chId].mLzSga4) );
}
void servCal::lozSga5_FltClone( int chId )
{
    memcpy( mCalPara.mPayload.mCHs[chId].mLzSga5Flt,
            mCalPara.mPayload.mCHs[chId].mLzSga4Flt,
            array_count(mCalPara.mPayload.mCHs[chId].mLzSga4Flt)*
            sizeof(mCalPara.mPayload.mCHs[chId].mLzSga4Flt) );
}

void servCal::preLozRefScale( int chId )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    pAfe->setOlEn( 1 ); //! lz

    pAfe->setGtrim1( 0, 1 );
    pAfe->setGtrim2( 0, 3 );
    pAfe->setGtrim3( 0, 3 );

    pAfe->setGtrim4( 0, 15 );
    pAfe->setGtrim5( 0, 15 );

    pAfe->flushWCache();
}
DsoErr servCal::lozRefScale( int chId )
{
    DsoErr err;

    IPhyAfe *pAfe;
    pAfe = getAfe( chId );

    int delta;
    float fDelta;

    int attx[]={1,0};   //! 2x, 20x
    int flt[]={0,1};
    float lsbs[2]={ mCalPara.mPayload.mCHs[ chId ].mLsb50,
                    mCalPara.mPayload.mCHs[ chId ].mLsb50_att };
    int i, j, g;

    g = 0;
    for ( i = 0; i < array_count(attx); i++ )
    {
        pAfe->setGpo1( attx[i] );

        for ( j = 0; j < array_count(flt); j++ )
        {
            pAfe->setOSel( flt[j] );

            err = iterDelta( 0, chId, chId, ADC_TOP, ADC_BASE, delta, fDelta );
            if ( ERR_NONE != err )
            { return err; }

            Q_ASSERT( g < array_count(mCalPara.mPayload.mCHs[ chId ].mLzRefScale ) );

            mCalPara.mPayload.mCHs[ chId ].mLzRefScale[g] =
                    delta * lsbs[i] / AMP_DIV;

            LOG()<<mCalPara.mPayload.mCHs[ chId ].mLzRefScale[g];

            g++;
        }
    }

    return ERR_NONE;
}

DsoErr servCal::lozScaleDeduce( int chId )
{
    DsoErr err;

    err = lozScaleDeduce_2x_normal( chId );
    if ( ERR_NONE != err )
    {
        return err;
    }

    err = lozScaleDeduce_2x_flt( chId );
    if ( ERR_NONE != err )
    {
        return err;
    }

    err = lozScaleDeduce_20x_normal( chId );
    if ( ERR_NONE != err )
    {
        return err;
    }

    err = lozScaleDeduce_20x_flt( chId );
    if ( ERR_NONE != err )
    {
        return err;
    }

    return ERR_NONE;
}

DsoErr servCal::lozScaleDeduce_2x_normal( int chId )
{
    DsoErr err;

    vScales scales;

    //! 2X
    err = lozScaleDeduce_fill( chId,
                               2, mCalPara.mPayload.mCHs[chId].mLzSga1,
                               mCalPara.mPayload.mCHs[chId].mLzRefScale[0],
                               scales,
                               opab_ov_range_loz_2x);
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    err = lozScaleDeduce_pack( scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    vScale *pViewScale = scales.at(0);
    if( 5000 < pViewScale->trimVal )
    {
        vScale *pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 5000;
        pScale->mBw = BW_20M;
        scales.insert(0,pScale);

        pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 5000;
        pScale->mBw = BW_OFF;
        scales.insert(0,pScale);

    }

    if( 2000 < pViewScale->trimVal )
    {
        vScale *pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 2000;
        pScale->mBw = BW_20M;
        scales.insert(0,pScale);

        pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 2000;
        pScale->mBw = BW_OFF;
        scales.insert(0,pScale);
    }

    if( 1000 < pViewScale->trimVal )
    {
        vScale *pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 1000;
        pScale->mBw = BW_20M;
        scales.insert(0,pScale);

        pScale = new vScale();
        pScale->keys = pViewScale->keys;
        pScale->miVal= pViewScale->miVal;
        pScale->mVal = pViewScale->mVal;
        pScale->trimVal = 1000;
        pScale->mBw = BW_OFF;
        scales.insert(0,pScale);
    }

    err = lozScaleDeduce_export( 1,
                                 scales,
                                 0,
                                 array_count(mCalPara.mPayload.mCHs[chId]._lzScales_2x_normal),
                                 mCalPara.mPayload.mCHs[chId]._lzScales_2x_normal,
                                 &mCalPara.mPayload.mCHs[chId].mLzScales_2x_normal );

    LOG()<<mCalPara.mPayload.mCHs[chId].mLzScales_2x_normal;


    scales.gc();

    return ERR_NONE;
}
DsoErr servCal::lozScaleDeduce_2x_flt( int chId )
{
    DsoErr err;

    vScales scales;

    //! 2X
    err = lozScaleDeduce_fill( chId,
                               2,
                               mCalPara.mPayload.mCHs[chId].mLzSga1,
                               mCalPara.mPayload.mCHs[chId].mLzRefScale[1],
                               scales,
                               opab_ov_range_loz_2x );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    err = lozScaleDeduce_pack( scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    err = lozScaleDeduce_export( 1,
                                 scales,
                                 1,
                                 array_count(mCalPara.mPayload.mCHs[chId]._lzScales_2x_flt),
                                 mCalPara.mPayload.mCHs[chId]._lzScales_2x_flt,
                                 &mCalPara.mPayload.mCHs[chId].mLzScales_2x_flt );

    scales.gc();

    return ERR_NONE;
}
DsoErr servCal::lozScaleDeduce_20x_normal( int chId )
{
    DsoErr err;

    vScales scales;

    //! 20X
    err = lozScaleDeduce_fill( chId,
                               2,
                               mCalPara.mPayload.mCHs[chId].mLzSga1_att,
                               mCalPara.mPayload.mCHs[chId].mLzRefScale[2],
                               scales,
                               opab_ov_range_loz_20x );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    err = lozScaleDeduce_pack( scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    err = lozScaleDeduce_export( 0,
                                 scales,
                                 0,
                                 array_count(mCalPara.mPayload.mCHs[chId]._lzScales_20x_normal),
                                 mCalPara.mPayload.mCHs[chId]._lzScales_20x_normal,
                                 &mCalPara.mPayload.mCHs[chId].mLzScales_20x_normal );

    scales.gc();

    return ERR_NONE;
}
DsoErr servCal::lozScaleDeduce_20x_flt( int chId )
{
    DsoErr err;

    vScales scales;

    //! 20X
    err = lozScaleDeduce_fill( chId,
                               2,
                               mCalPara.mPayload.mCHs[chId].mLzSga1_att,
                               mCalPara.mPayload.mCHs[chId].mLzRefScale[3],
                               scales,
                               opab_ov_range_loz_20x );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    err = lozScaleDeduce_pack( scales );
    if ( err != ERR_NONE )
    {
        scales.gc();
        return err;
    }

    err = lozScaleDeduce_export( 0,
                                 scales,
                                 1,
                                 array_count(mCalPara.mPayload.mCHs[chId]._lzScales_20x_flt),
                                 mCalPara.mPayload.mCHs[chId]._lzScales_20x_flt,
                                 &mCalPara.mPayload.mCHs[chId].mLzScales_20x_flt );

    scales.gc();

    return ERR_NONE;
}

DsoErr servCal::lozScaleDeduce_pack( vScales &scales )
{
    scales.sort( vScaleLessThan );

    scales.normalize();

    return lozScaleDeduce_trim( scales );
}

DsoErr servCal::lozScaleDeduce_fill( int chId,
                                     int sga1Cnt, float *pSga1,
                                     float refScale,
                                     vScales &scales,
                                     float ovRng )
{
    chCal *pChCal;
    int i;
    Q_ASSERT( chId >= 0 && chId < 4 );

    pChCal = mCalPara.mPayload.mCHs + chId;

    //! data init

    //! sga1
    QList<vMul> sga1;
    for ( i = 0; i < sga1Cnt; i++ )
    {
        sga1.append( vMul(
                         i,
                         pSga1[i]
                         ) );
    }

    QList<vMul> sga1Trim;
    for ( i = 0;
          i < array_count(pChCal->mLzSga1Trim);
          i++ )
    {
        sga1Trim.append( vMul(
                             i,
                             pChCal->mLzSga1Trim[i]
                             ));
    }

    //! sga2
    QList<vMul> sga2;
    for( i = 0;
         i < array_count(pChCal->mLzSga2);
         i++ )
    {
        sga2.append( vMul(
                         i,
                         pChCal->mLzSga2[i]
                         )
                    );
    }

    QList<vMul> sga2Trim;
    for( i = 0;
         i < array_count(pChCal->mLzSga2Trim);
         i++ )
    {
        sga2Trim.append( vMul(
                         i,
                         pChCal->mLzSga2Trim[i]
                         )
                    );
    }

    //! sga3
    QList<vMul> sga3;
    for( i = 0;
         i < array_count(pChCal->mLzSga3);
         i++ )
    {
        sga3.append( vMul(
                         i,
                         pChCal->mLzSga3[i]
                         )
                    );
    }

    QList<vMul> sga3Trim;
    for( i = 0;
         i < array_count(pChCal->mLzSga3Trim);
         i++ )
    {
        sga3Trim.append( vMul(
                         i,
                         pChCal->mLzSga3Trim[i]
                         )
                    );
    }

    //! sga4
    QList<vMul> sga4;
    for( i = 0;
         i < array_count(pChCal->mLzSga4);
         i++ )
    {
        sga4.append( vMul(
                         i,
                         pChCal->mLzSga4[i]
                         )
                    );
    }

    //! user hz sga4 trim
    QList<vMul> sga4Trim;
    for( i = 0;
         i < 16;
         i++ )
    {
        sga4Trim.append( vMul(
                         i,
                         i*(1.0 - pChCal->mHzSga4Trim[0])/15
                         + pChCal->mHzSga4Trim[0]
                         )
                        );
    }

    //! \todo sga5


    //! base scale
    //! 2x, normal
    vScale *pBaseScale = new vScale();
    Q_ASSERT( NULL != pBaseScale );
    pBaseScale->mVal = refScale*lsb_base;

    //! seed
    vScales baseList;
    baseList.append( pBaseScale );

    //! expand
    vScales dst;

    vCascade::cascade_div( baseList,
                           sga4Trim,
                           dst,
                           ovRng );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    vCascade::cascade_div( baseList,
                           sga4,
                           dst,
                           ovRng );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    vCascade::cascade_div( baseList,
                           sga3Trim,
                           dst,
                           ovRng / sga3Trim[0].mul );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    vCascade::cascade_div( baseList,
                           sga3,
                           dst,
                           ovRng / 1.2f );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    vCascade::cascade_div( baseList,
                           sga2Trim,
                           dst,
                           ovRng / sga2Trim[0].mul );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    vCascade::cascade_div( baseList,
                           sga2,
                           dst,
                           ovRng / 1.0f );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    vCascade::cascade_div( baseList,
                           sga1Trim,
                           dst,
                           ovRng / sga1Trim[0].mul );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    vCascade::cascade_div( baseList,
                           sga1,
                           dst,
                           ovRng / 1.2f );
    baseList = dst;
    dst.clear();
    LOG()<<baseList.size();

    //! result
    scales = baseList;
    LOG()<<scales.size();

    return ERR_NONE;
}

DsoErr servCal::lozScaleDeduce_trim(
                     vScales &scales
                      )
{
    QList<int> allScale;

    genvScale::genScale( allScale );
    LOG()<<allScale.size()<<scales.size();
    //! trim scales
    scales.trim( allScale, lzvScaleSnr );
    LOG()<<scales.size();

    //! snr search fail
    if ( scales.size() > allScale.size() )
    {
        logInfo( __FUNCTION__, QString::number(scales.size()) );
        return ERR_CAL_LZ_GAIN;
    }

    return ERR_NONE;
}

DsoErr servCal::lozScaleDeduce_export(
                            int att,
                            vScales &scales,
                            int flt,

                            int cap,
                            afeCfgPair *outAry,
                            int *pCnt
                                       )
{
    int i;

    vScale *pVScale;
    afeCfgPair *pScaleCfgPair;
    i = 0;

    foreach( pVScale, scales )
    {
        Q_ASSERT( NULL != pVScale );

        if ( i < cap )
        {}
        else
        { qWarning()<<i<<cap<<scales.size(); Q_ASSERT(false);}

        pScaleCfgPair = outAry + i;

        pScaleCfgPair->setLzCfg(
                    att,
                    pVScale->keys[0],pVScale->keys[1],
                    pVScale->keys[2],pVScale->keys[3],
                    pVScale->keys[4],pVScale->keys[5],

                    flt,

                    pVScale->keys[6],   //! sga4
                    pVScale->keys[7],   //! sga4 trim
                    pVScale->keys[6],   //! sga4
                    pVScale->keys[7],   //! sga4 trim
                    pVScale->mBw
                    );

        //! scale value
        pScaleCfgPair->setValue( pVScale->mVal );
        pScaleCfgPair->setValue( pVScale->trimVal );

        i++;
    }

    *pCnt = i;

    return ERR_NONE;
}
