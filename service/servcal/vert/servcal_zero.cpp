
#include "../servcal.h"

DsoErr servCal::zeroProc( int chId )
{
    DsoErr err;
    int azVos    = mCalPara.mPayload.mCHs[ chId ].mAzVosTrim;
    int vosTrim2 = mCalPara.mPayload.mCHs[ chId ].mVosTrim2;
    int vosTrim3 = mCalPara.mPayload.mCHs[ chId ].mVosTrim3;


    //! cal
    err = zeroHizAmp( chId, azVos );
    if ( err != ERR_NONE )
    {
        return err;
    }

    err = zeroHizSga1Lg( chId, vosTrim3 );
    if ( err != ERR_NONE )
    {
        return err;
    }

    err = zeroHizSga1Hg( chId, vosTrim2 );
    if ( err != ERR_NONE )
    {
        return err;
    }

    //! workaround
    azVos = workaroundHizZero( azVos );

    IPhyAfe *pAfe = getAfe( chId );
    //! apply
    pAfe->setAzVos( azVos );
    pAfe->setHzVosTrim2( vosTrim2 );
    pAfe->setHzVosTrim3( vosTrim3 );
    pAfe->flushWCache();

    //! save
    mCalPara.mPayload.mCHs[ chId ].mAzVosTrim = ( azVos );
    mCalPara.mPayload.mCHs[ chId ].mVosTrim2 = vosTrim2;
    mCalPara.mPayload.mCHs[ chId ].mVosTrim3 = vosTrim3;

    LOG()<<azVos<<vosTrim2<<vosTrim3;

    return ERR_NONE;
}

void servCal::preZero( int chId )
{
    IPhyAfe *pAfe = getAfe( chId );

    pAfe->setAzIsel( 2 );//0 INVERT , 2 gnd
    pAfe->setEnZd( 1 );// Enable Zero Detect
    pAfe->setK( 31 );//

    pAfe->setHzNpd( 1 ); //HZ_NPD：高阻通路掉电模式控制。0：待机；1：使能
    pAfe->flushWCache();
}
void servCal::postZero( int chId )
{
    IPhyAfe *pAfe = getAfe( chId );

    pAfe->setAzIsel( 0 );
    pAfe->setEnZd( 0 );
    pAfe->setK( 0 );

    pAfe->setTp1Typ( 0 );
    pAfe->setTpSw( 0 );

    pAfe->setHzNpd( 0 );

    pAfe->flushWCache();
}

#define work_around_hiz_bias    37
//! 1023~0 -- 1024~2047
int servCal::workaroundHizZero( int vosTrim )
{
    if ( vosTrim < work_around_hiz_bias )
    {
        return 1024 + work_around_hiz_bias - vosTrim;
    }
    else if ( vosTrim <= 1023 )
    {
        return vosTrim - work_around_hiz_bias;
    }
    else if ( vosTrim <= (2047 - work_around_hiz_bias) )
    {
        return vosTrim + work_around_hiz_bias;
    }
    else
    { return 2047; }
}

#if 1

//! negative: 0~1023(max)
//! positive: 1024~2048(max)
//! \todo iter
DsoErr servCal::zeroHizAmp( int chId, int &val )
{
    IPhyAfe *pAfe;

    pAfe = getAfe( chId );

    int vos;
    damrey_reg zDets[4];
    damrey_reg zDet;
    int azVos[4] = {0,1023,1024,2047};
    int vosMin, vosMax;

    //! for control point
    for( int i = 0; i < array_count(azVos); i++ )
    {
        pAfe->setAzVos( azVos[i] );
        pAfe->flushWCache();
        wait_n( 10 );
        zDets[i] = pAfe->getZDet();
    }

    //! 0~1023
    if ( zDets[0] != zDets[1] )
    {
        vosMin = 0;
        vosMax = 1023;
        if( zDets[0] )
        {
            vosMin = 1023;
            vosMax = 0;
        }
        vos = (vosMin + vosMax + 1) / 2;
    }
    //! 1024~2047
    else if ( zDets[2] != zDets[3] )
    {
        vosMin = 1024;
        vosMax = 2047;
        if( zDets[2] )
        {
            vosMin = 2047;
            vosMax = 1024;
        }

        vos = (vosMin + vosMax + 1) / 2;
    }
    else
    {
        logInfo(QString("CH%1 AFE error1").arg(chId+1));
        return ERR_CAL_HIZ_AMP;
    }


    //! vos in 1023
    while( abs(vosMax-vosMin) > 3 )
    {
        pAfe->setAzVos( vos );

        //qDebug() << "vos=" << vos;
        pAfe->flushWCache();
        wait_n( 100 );

        zDet = pAfe->getZDet();

        if ( zDet )
        {
            int tmp = vos;
            vos = (vos + vosMin) / 2;
            vosMax = tmp;
        }
        else
        {
            int tmp = vos;
            vos = (vos + vosMax) / 2;
            vosMin = tmp;
        }

//        LOG()<<vos<<step<<zDet;
        //logInfo(QString("CH%1 AFE VOS:%2, step=%3").arg(chId+1).arg(vos).arg(step));
    }

    val = vos;
    //qDebug() << "chan=" << chId << "vos=" << vos;

    return ERR_NONE;
}
#else
//! negative: 0~1023(max)
//! positive: 1024~2048(max)
//! \todo iter
DsoErr servCal::zeroHizAmp( int chId, int &val )
{
    IPhyAfe *pAfe;

    pAfe = getAfe( chId );

    int step, vos;
    damrey_reg zDets[4];
    damrey_reg zDet;
    int azVos[4] = {0,1023,1024,2047};
    int vosMin, vosMax;

    //! for control point
    for( int i = 0; i < array_count(azVos); i++ )
    {
        pAfe->setAzVos( azVos[i] );
        pAfe->flushWCache();
        wait_n( 10 );
        zDets[i] = pAfe->getZDet();
    }

    //! 0~1023
    if ( zDets[0] != zDets[1] )
    {
        vosMin = 0;
        vosMax = 1023;

        if ( zDets[1] )
        {
            vos = 1023;
            step = 1023;
        }
        else
        {
            vos = 0;
            step = -1023;
        }
    }
    //! 1024~2047
    else if ( zDets[2] != zDets[3] )
    {
        vosMin = 1024;
        vosMax = 2047;

        if ( zDets[3] )
        {
            vos = 2047;
            step = 1023;
        }
        else
        {
            vos = 1024;
            step = -1023;
        }
    }
    else
    {
        logInfo(QString("CH%1 AFE error1").arg(chId+1));
        return ERR_CAL_HIZ_AMP;
    }


    //! vos in 1023
    while( step != 0 )
    {
        pAfe->setAzVos( vos );

        pAfe->flushWCache();
        wait_n( 1 );

        zDet = pAfe->getZDet();

        if ( zDet )
        {
            vos -= step;
        }
        else
        {
            vos += step;
            step /= 2;
            vos -= step;
        }

//        LOG()<<vos<<step<<zDet;
        logInfo(QString("CH%1 AFE VOS:%2, step=%3").arg(chId+1).arg(vos).arg(step));

        if ( vos < vosMin || vos >vosMax )
        {
            qDebug() << "afe value=" << val;
            logInfo(QString("CH%1 AFE error2").arg(chId+1));
            return ERR_CAL_HIZ_AMP;
        }
    }

    qDebug() << "chan=" << chId << "vos=" << vos;
    val = vos;

    return ERR_NONE;
}
#endif

#define HZ_VOS_TRIM3_MAX    7
DsoErr servCal::zeroHizSga1Lg( int chId, int &val )
{
    IPhyAfe *pAfe;

    pAfe = getAfe( chId );

    pAfe->setK( 31 );
    pAfe->setAzIsel( 1 );
    pAfe->setEnZd( 1 );
    pAfe->setTp1Typ( 1 );
    pAfe->setTpSw( 1 );

    int step, vos;
    int vosMin, vosMax;
    damrey_reg zDet;

    int zDets[2];

    //! dir
    pAfe->setHzVosTrim3( 0 );
    pAfe->flushWCache();
    wait_n( 1 );
    zDets[0] = pAfe->getZDet();

    pAfe->setHzVosTrim3( 7 );
    pAfe->flushWCache();
    wait_n( 1 );
    zDets[1] = pAfe->getZDet();

    if ( zDets[0] == zDets[1] )
    {
        if ( zDets[0] == 1 )
        {
            val = 0;
        }
        else
        {
            val = HZ_VOS_TRIM3_MAX;
        }
        return ERR_NONE;
    }
    else
    {
        vosMin = 0;
        vosMax = HZ_VOS_TRIM3_MAX;

        if ( zDets[1] )
        {
            vos = HZ_VOS_TRIM3_MAX;
            step = HZ_VOS_TRIM3_MAX;
        }
        else
        {
            vos = 0;
            step = -HZ_VOS_TRIM3_MAX;
        }
    }

    while( step != 0 )
    {
        pAfe->setHzVosTrim3( vos );

        pAfe->flushWCache();
        wait_n( 1 );

        zDet = pAfe->getZDet();

        if ( zDet )
        { vos -= step; }
        else
        {
            vos -= step;
            step /= 2;
            vos += step;
        }

        if ( vos < vosMin || vos > vosMax )
        {
            return ERR_CAL_HIZ_AMP;
        }

        LOG_DBG()<<step<<vos;

    }

    val = vos;

    return ERR_NONE;
}

#define HZ_VOS_TRIM2_MAX    7
DsoErr servCal::zeroHizSga1Hg( int chId, int &val )
{
    IPhyAfe *pAfe = getAfe( chId );

    pAfe->setK( 31 );
    pAfe->setAzIsel( 1 );
    pAfe->setEnZd( 1 );
    pAfe->setTp1Typ( 1 );
    pAfe->setTpSw( 0 );

    int step, vos;
    int vosMin, vosMax;

    damrey_reg zDet;
    int zDets[2];

    //! dir
    pAfe->setHzVosTrim2( 0 );
    pAfe->flushWCache();
    wait_n( 1 );
    zDets[0] = pAfe->getZDet();

    pAfe->setHzVosTrim2( HZ_VOS_TRIM2_MAX );
    pAfe->flushWCache();
    wait_n( 1 );
    zDets[1] = pAfe->getZDet();

    if( zDets[0] == zDets[1] )
    {
        if ( zDets[0] ==  1 )
        {
            val = 0;
        }
        else
        {
            val = HZ_VOS_TRIM2_MAX;
        }
        return ERR_NONE;
    }
    else
    {
        vosMin = 0;
        vosMax = HZ_VOS_TRIM2_MAX;

        if ( zDets[1] )
        {
            vos = HZ_VOS_TRIM2_MAX;
            step = HZ_VOS_TRIM2_MAX;
        }
        else
        {
            vos = 0;
            step = -HZ_VOS_TRIM2_MAX;
        }
    }

    while( step != 0 )
    {
        pAfe->setHzVosTrim2( vos );

        pAfe->flushWCache();
        wait_n( 1 );

        zDet = pAfe->getZDet();

        if ( zDet )
        { vos -= step; }
        else
        {
            vos -= step;
            step /= 2;
            vos += step;
        }

        if ( vos < vosMin || vos > vosMax )
        {
            return ERR_CAL_HIZ_AMP;
        }
    }

    val = vos;

    return ERR_NONE;
}

#define wpu_gain    (2457)
#define wpu_offset  (-67)
DsoErr servCal::onAzVosEnter(Chan ch)
{
    //! wpu
    //! 200~480
    getWpu()->setanalog_ch0_gain( wpu_gain );
    getWpu()->setanalog_ch0_offset( wpu_offset );

    getWpu()->setanalog_ch1_gain( wpu_gain );
    getWpu()->setanalog_ch1_offset( wpu_offset );

    getWpu()->setanalog_ch2_gain( wpu_gain );
    getWpu()->setanalog_ch2_offset( wpu_offset );

    getWpu()->setanalog_ch3_gain( wpu_gain );
    getWpu()->setanalog_ch3_offset( wpu_offset );

    getWpu()->flushWCache();

    //! adc
    //! gain offset to 512
    getAdc()->setGain( 1, 511 );
    getAdc()->setGain( 2, 511 );
    getAdc()->setGain( 3, 511 );
    getAdc()->setGain( 4, 511 );

    getAdc()->setOffset( 1, 511 );
    getAdc()->setOffset( 2, 511 );
    getAdc()->setOffset( 3, 511 );
    getAdc()->setOffset( 4, 511 );

    //! trig
    //! use auto trig
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan1, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan2, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan3, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan4, 0, 255 );

    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan1, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan2, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan3, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan4, 0, 255 );

    getCcu()->setAUTO_TRIG_TIMEOUT( 0 );


    #ifdef DAMREY_VER_1_5_x
        setSpuInvert(true);
    #else
        setSpuInvert(false);
        qDebug() << "version";
    #endif
        //! ch bw on
        IPhyCH *pCH;
        for ( int i = 0; i < 4; i++ )
        {
            pCH = getCH( i );

            pCH->setCalBw( BW_20M, IMP_1M );

            pCH->flushWCache();
        }

    preZero( ch - chan1 );
    return ERR_NONE;
}

DsoErr servCal::onAzVosExit(Chan ch)
{
    postZero( ch - chan1 );
    return ERR_NONE;
}

DsoErr servCal::onRmtAzVos(CArgument &arg)
{
    Q_ASSERT( 2 == arg.size() );

    int chan, val;

    chan = arg[0].iVal;
    val  = arg[1].iVal;

    if ( chan >= chan1 && chan < chan4 )
    {}
    else
    {
        return ERR_INVALID_INPUT;
    }

    if ( val >= 0 && val <= 2047 )
    {}
    else
    {
        return ERR_INVALID_INPUT;
    }

    int ch = chan - chan1;
    IPhyAfe *pAfe = getAfe( ch );

    pAfe->setAzVos( val );
    pAfe->flushWCache();

    mCalPara.mPayload.mCHs[ ch ].mAzVosTrim = val;

    return ERR_NONE;
}
