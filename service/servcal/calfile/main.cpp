#include <QCoreApplication>
#include "../../../baseclass/checkedstream/ccheckedstream.h"
#include "servcal_datadelay.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    calHori theCalHori;

    //! load
    int size = sizeof( theCalHori.mPayload);
    if ( size != theCalHori.load( "/rigol/data/hcal.hex", &theCalHori.mPayload, size ) )
    {
        qDebug()<<"read file fail";
        return 0;
    }

    //! show file
    bitWindow wnd;
    qDebug()<<"ilvds"<<theCalHori.mPayload.mIcIDelay[0].mILVDS;
    for ( int i = 0; i < 4; i++ )
    {
        qDebug()<<"*****L";
        for ( int j = 0; j < 8; j++ )
        {
            wnd = theCalHori.mPayload.mIcIDelay[0].coreDelay[i].mLBitWnd[j];
            qDebug()<<"core/bit/L"<<i<<j<<"["<<wnd.mLeft<<","<<wnd.mLeft+wnd.mLen<<"]";
        }

        qDebug()<<"*****H";
        for ( int j = 0; j < 8; j++ )
        {
            wnd = theCalHori.mPayload.mIcIDelay[0].coreDelay[i].mHBitWnd[j];
            qDebug()<<"core/bit/h"<<i<<j<<"["<<wnd.mLeft<<","<<wnd.mLeft+wnd.mLen<<"]";
        }
    }

//    return a.exec();
}
