#-------------------------------------------------
#
# Project created by QtCreator 2017-08-24T15:42:09
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = calfile
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../datadelay
INCLUDEPATH += ../../../baseclass/checkedstream

HEADERS =  \
    ../datadelay/servcal_datadelay.h \
    ../../../baseclass/checkedstream/ccheckedstream.h \
    ../../../com/crc32/ccrc32.h
SOURCES += main.cpp \
    ../../../baseclass/checkedstream/ccheckedstream.cpp \
    ../../../com/crc32/ccrc32.cpp
