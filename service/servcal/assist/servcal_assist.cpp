
#include "../servcal.h"

#include "../../../baseclass/dsovert.h"
#include "../../../baseclass/math/rarith.h"


DsoErr servCal::getAvg( int spuId, int chId, float &val )
{
    return getCoreAvg( spuId, chId, val );
}

DsoErr servCal::getTraceAvg( int /*spuId*/, int chId, float &val )
{
     dsoVert *pVert = dsoVert::getCH( (Chan)(chId+1) );
     if( pVert != NULL )
     {
         DsoWfm wfm;
         pVert->getTrace(wfm, chan_none, horizontal_view_main);
         if( wfm.size() > 0 )
         {
             int sum = 0;
             for(int i=0; i<wfm.size(); i++)
             {
                 sum += wfm.at(i);
             }
             //qDebug() << "Trace avg:"<< sum/wfm.size();
             val = sum/wfm.size();
         }
     }
     return ERR_NONE;
}

//! read core avg directly
DsoErr servCal::getCoreAvg( int spuId, int coreId, float &val )
{
    IPhySpu *pSpu;

    pSpu = getSpu( spuId );

    //! max 1M size, 2^24
    //! 16M/312.5M = 51ms
    //! 32bit
    //! shift 16  = 24 - 8
    //! ret = 16 bit
//    pSpu->startAdcAvg( 20 );    //! 3ms
    pSpu->startAdcAvg( 21 );    //! 6ms
//    pSpu->startAdcAvg( 24 );    //! 48ms
    pSpu->flushWCache();
    while( !pSpu->getAdcAvgDone() )
    {
        wait_n(1);
    }
    pSpu->stopAdcAvg();
    pSpu->flushWCache();

    //! read
    quint32 ab, cd;
    quint32 avg;
    pSpu->getAvg( &ab, &cd );

    //! split to core
    if ( coreId == 0 )      //! a
    {
        avg = (ab>>16)&0xffff;
    }
    else if ( coreId == 1 ) //! b
    {
        avg = (ab>>0)&0xffff;
    }
    else if ( coreId == 2 ) //! c
    {
        avg = (cd>>16)&0xffff;
    }
    else if ( coreId == 3 ) //! d
    {
        avg = (cd>>0)&0xffff;
    }
    else
    {
        LOG_DBG();
        return ERR_INVALID_INPUT;
    }

//    val = avg / 256.0;
    val = avg >> 8;
    val = val + 0.01 * ( avg & 0xff) ;

    //qDebug() << "adc value:" << val;
    return ERR_NONE;
}

/* use more times to compute average */
DsoErr servCal::getCoreAvg1( int spuId, int coreId, float &val )
{
    IPhySpu *pSpu;

    pSpu = getSpu( spuId );

    //! max 1M size, 2^24
    //! 16M/312.5M = 51ms
    //! 32bit
    //! shift 16  = 24 - 8
    //! ret = 16 bit
//    pSpu->startAdcAvg( 20 );    //! 3ms
//    pSpu->startAdcAvg( 21 );    //! 6ms
    pSpu->startAdcAvg( 24 );    //! 48ms
    pSpu->flushWCache();
    while( !pSpu->getAdcAvgDone() )
    {
        wait_n(1);
    }
    pSpu->stopAdcAvg();
    pSpu->flushWCache();

    //! read
    quint32 ab, cd;
    quint32 avg;
    pSpu->getAvg( &ab, &cd );

    //! split to core
    if ( coreId == 0 )      //! a
    {
        avg = (ab>>16)&0xffff;
    }
    else if ( coreId == 1 ) //! b
    {
        avg = (ab>>0)&0xffff;
    }
    else if ( coreId == 2 ) //! c
    {
        avg = (cd>>16)&0xffff;
    }
    else if ( coreId == 3 ) //! d
    {
        avg = (cd>>0)&0xffff;
    }
    else
    {
        LOG_DBG();
        return ERR_INVALID_INPUT;
    }

    val = avg >> 8;
    val = val + 0.01 * ( avg & 0xff) ;
    //qDebug() << "adc avg:" << val;

    return ERR_NONE;
}
DsoErr servCal::getVpp( int spuId, int chId, int &vpp )
{
    Q_ASSERT( chId >= 0 && chId < 4 );

    return getVpp( spuId, (Chan)(chId + chan1), vpp );
}

DsoErr servCal::getVpp( int spuId, Chan traceCh, int &vpp )
{
    Q_ASSERT( traceCh >= chan1 && traceCh <= chan4 );

    int vpps[4];

    getVpp( spuId, vpps );

    vpp = vpps[ traceCh - chan1 ];

    return ERR_NONE;
}

DsoErr servCal::getVpp( int spuId, int vpps[4] )
{
    //! read peak
    int peaks[8];
    getPeak( spuId, peaks );

    for ( int i = 0; i < 4; i++ )
    {
        vpps[i] = peaks[i*2] - peaks[i*2+1];    //! max - min
    }

    return ERR_NONE;
}

DsoErr servCal::getPeak( int spuId, int peaks[8] )
{
    IPhySpu *pSpu;

    pSpu = getSpu( spuId );

    pSpu->startAdcPeak( 22 );   //! 3ms*4 > 10ms for 100Hz
    pSpu->flushWCache();

    while ( !pSpu->getAdcPeakDone() )
    { QThread::msleep( 1 ); }

    pSpu->stopAdcPeak();
    pSpu->flushWCache();

    //! read peak
    pSpu->getAdcPeaks( peaks );

    return ERR_NONE;
}

#define ITER_WAIT_TIME  10
//#define LSB_ERR         (0.5f/4)
#define LSB_ERR         (0.1)


DsoErr servCal::iterDac( int spuId,
                         int chId, int masterChId,
                         int adc,
                         int &outDac,
                         float &outVal,
                         int rmin,
                         int rmax,
                         int attempt
                         )
{
    cplatform *platform;
    IPhyCH *pCH;
    Q_ASSERT( rmin < rmax );

    //! get platform
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    pCH = &platform->m_pPhy->m_ch[masterChId];

    float fVal1, fVal2, fVal;

    //! attempt
    if ( pCH->getDac() != attempt )
    {
        pCH->setDac( attempt );
        pCH->flushWCache();
        wait_n( ITER_WAIT_TIME );
    }

    if ( ERR_NONE != getAvg( spuId, chId, fVal ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    //! check end
    if ( fabs(fVal - adc) < LSB_ERR )
    {
        outVal = fVal;
        outDac = attempt;
        return ERR_NONE;
    }

    //! min max
    pCH->setDac( rmin );
    pCH->flushWCache();
    wait_n( ITER_WAIT_TIME );
    if ( ERR_NONE != getAvg( spuId, chId, fVal1 ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    dac_reg dacReg = pCH->getDacReg();
    if ( dacReg != rmin )
    {
        return ERR_CAL_DAC;
    }
    if ( fVal1 > 225 )
    {
        getAvg( spuId, chId, fVal1 );
        if ( fVal1 > 225 )
        {
            return ERR_CAL_DAC;
        }
    }


    pCH->setDac( rmax );
    pCH->flushWCache();
    wait_n( ITER_WAIT_TIME );
    if ( ERR_NONE != getAvg( spuId, chId, fVal2 ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    dacReg = pCH->getDacReg();
    if ( dacReg != rmax )
    {
        return ERR_CAL_DAC;
    }

    if ( fVal2 < 25 )
    {
        getAvg( spuId, chId, fVal2 );
        if ( fVal2 < 25 )
        {
            return ERR_CAL_DAC;
        }
    }



    int a, b;

    if ( fVal1 < fVal2 && ( fVal2 - fVal1 ) > 25 )
    {
        if ( adc < fVal1 || adc > fVal2 )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }

        a = rmin;
        b = rmax;
    }
    else if ( fVal1 > fVal2 && ( fVal1 - fVal2 ) > 25 )
    {
        if ( adc < fVal2 || adc > fVal1 )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }

        a = rmax;
        b = rmin;
    }
    else
    {
        logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
        LOG_DBG();
        return ERR_CAL_DAC;
    }

    int dac;
    int step = (b - a) / 2;

    //! upper range
    dac = b;
    bool bDec = false;
    int  tmo = 0;
    while ( step != 0 && tmo < 200)
    {
        tmo++;
        pCH->setDac( dac );
        pCH->flushWCache();
        wait_n( ITER_WAIT_TIME );
        if ( ERR_NONE != getAvg( spuId, chId, fVal ) )
        {
            return ERR_CAL_FAIL_DATA;
        }

        //! check end
        if ( fabs(fVal - adc) < LSB_ERR )
        {
            getAvg( spuId, chId, fVal );
            if ( fabs(fVal - adc) < LSB_ERR )
            {
                //getAvg( spuId, chId, fVal );
                //if ( fabs(fVal - adc) < LSB_ERR )
                {
                    outVal = fVal;
                    outDac = dac;
                    return ERR_NONE;
                }
            }

            //qDebug() << "Dac:" << dac << "ADC:" << fVal << "goal:" << adc;

        }

        if ( fVal > adc )
        {
            dac -= step;
            bDec = true;
        }
        else if ( fVal < adc )
        {
            if ( bDec )
            {
                step /= 2;
                bDec = false;
                if( step == 0 )
                {
                    if( b > a )
                    {
                        step = 2;
                    }
                    else
                    {
                        step = -2;
                    }
                    //qDebug() << "Dac:" << dac << "ADC:" << fVal << "tmo:" << tmo;
                }
            }

            if( (dac+step)> rmax)
            {                
                dac = attempt-1000;
                qDebug() << "need more time to try";
            }
            else
            {
                dac += step;
            }
        }
        else
        {
            break;
        }

        if ( dac < rmin || dac > rmax )
        {
            logInfo( __FUNCTION__, QString("%1-%2-%3").arg(dac).arg(rmin).arg(rmax));
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }
    }

    outVal = fVal;
    outDac = dac;

    //qDebug() << "Goal:" << adc << "Real:" << fVal << "tmo:" << tmo;

    return ERR_NONE;
}

#define ITER_WAIT_LONG_TIME 50

//for small scale
DsoErr servCal::iterDac1( int spuId,
                         int chId, int masterChId,
                         int adc,
                         int &outDac,
                         float &outVal,
                         int rmin,
                         int rmax,
                         int attempt,
                         afeCfgPair */*pCfg*/

                         )
{
    cplatform *platform;
    IPhyCH *pCH;
    Q_ASSERT( rmin < rmax );

    //! get platform
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    pCH = &platform->m_pPhy->m_ch[masterChId];

    float fVal1;
    float fVal2;
    float fVal;

    //float f32Ratio = 1;//pCfg->mfVal/(pCfg->mVal*1.0e-6f);
    //qDebug() << "wpu gain:" << f32Ratio;


    //! attempt
    if ( pCH->getDac() != attempt )
    {
        pCH->setDac( attempt );
        pCH->flushWCache();
        wait_n( ITER_WAIT_LONG_TIME );
    }

    if ( ERR_NONE != getCoreAvg1( spuId, chId, fVal ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    //! check end
    if ( fabs(fVal - adc) < LSB_ERR )
    {
        outVal = fVal;
        outDac = attempt;
        //qDebug() << "find attempt result" << fVal << adc;
        return ERR_NONE;
    }


    /*************************************************************
     *      check min dac is right
     * **********************************************************/
    pCH->setDac( rmin );
    pCH->flushWCache();
    wait_n( ITER_WAIT_LONG_TIME );
    if ( ERR_NONE != getCoreAvg1( spuId, chId, fVal1 ) )
    {
        return ERR_CAL_FAIL_DATA;
    }
    dac_reg dacReg = pCH->getDacReg();
    if ( dacReg != rmin )
    {
        LOG_DBG()<<dacReg<<rmin;
        return ERR_CAL_DAC;
    }

    if ( fVal1 > 225 )
    {
        getCoreAvg1( spuId, chId, fVal1 );
        if ( fVal1 > 225 )
        {
            LOG_DBG()<<fVal1<<rmin;
            return ERR_CAL_DAC;
        }
    }


    /*************************************************************
     *      check max dac is right
     * **********************************************************/
    pCH->setDac( rmax );
    pCH->flushWCache();
    wait_n( ITER_WAIT_LONG_TIME );
    if ( ERR_NONE != getCoreAvg1( spuId, chId, fVal2 ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    dacReg = pCH->getDacReg();
    if ( dacReg != rmax )
    {
        LOG_DBG()<<dacReg<<rmax;
        return ERR_CAL_DAC;
    }

    if ( fVal2 < 25 )
    {
        getCoreAvg1( spuId, chId, fVal2 );
        if ( fVal2 < 25 )
        {
            LOG_DBG()<<fVal2<<rmax;
            return ERR_CAL_DAC;
        }
    }

    int a, b;

    if ( fVal1 < fVal2 && ( fVal2 - fVal1 ) > 25 )
    {
        if ( adc < fVal1 || adc > fVal2 )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }

        a = rmin;
        b = rmax;
    }
    else if ( fVal1 > fVal2 && ( fVal1 - fVal2 ) > 25 )
    {
        if ( adc < fVal2 || adc > fVal1 )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }

        a = rmax;
        b = rmin;
    }
    else
    {
        logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
        LOG_DBG();
        return ERR_CAL_DAC;
    }

    int dac;
    int step = (b - a) / 2;

    //! upper range
    dac = b;
    bool bDec = false;
    int  tmo  = 0;
    while ( tmo < 200 && step != 0)
    {
        tmo++;

        pCH->setDac( dac );
        pCH->flushWCache();
        wait_n( ITER_WAIT_LONG_TIME );

        if ( ERR_NONE != getCoreAvg1( spuId, chId, fVal ) )
        {
            return ERR_CAL_FAIL_DATA;
        }

        //! check end
        if ( fabs(fVal - adc) < LSB_ERR )
        {
            outVal = fVal;
            outDac = dac;
            //wait_n( 500 );
            //qDebug() << "find result" << fVal << adc;
            return ERR_NONE;
        }

        if ( fVal > adc )
        {
            dac -= step;
            bDec = true;
        }
        else if ( fVal < adc )
        {
            if ( bDec )
            {
                step /= 2;
                bDec = false;

                if( step == 0 )
                {
                    if( b > a )
                    {
                        step = 1;
                    }
                    else
                    {
                        step = -1;
                    }
                }
            }

            dac += step;
        }
        else
        {
            break;
        }

        if ( dac < rmin || dac > rmax )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3/%4/%5/%6/%7").arg(dac).arg(rmin).arg(rmax).arg(fVal).arg(a).arg(b).arg(step) );
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }
    }


    outVal = fVal;
    outDac = dac;

    return ERR_NONE;
}

#define TRACE_WAIT_DATA 240
DsoErr servCal::traceDac( int spuId,
                         int chId, int masterChId,
                         int adc,
                         int &outDac,
                         float &outVal,
                         int rmin,
                         int rmax,
                         int attempt
                         )
{
    cplatform *platform;
    IPhyCH *pCH;
    Q_ASSERT( rmin < rmax );

    //! get platform
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    pCH = &platform->m_pPhy->m_ch[masterChId];

    float fVal1, fVal2, fVal;

    //! attempt
    if ( pCH->getDac() != attempt )
    {
        pCH->setDac( attempt );
        pCH->flushWCache();
        wait_n( TRACE_WAIT_DATA );
    }

    if ( ERR_NONE != getTraceAvg( spuId, chId, fVal ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    //! check end
    if ( fabs(fVal - adc) < LSB_ERR )
    {
        outVal = fVal;
        outDac = attempt;
        return ERR_NONE;
    }

    //! min max
    pCH->setDac( rmin );
    pCH->flushWCache();
    wait_n( TRACE_WAIT_DATA );
    if ( ERR_NONE != getTraceAvg( spuId, chId, fVal1 ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    dac_reg dacReg = pCH->getDacReg();
    if ( dacReg != rmin )
    {
        LOG_DBG()<<dacReg<<rmin;
        return ERR_CAL_DAC;
    }
    if ( fVal1 > 225 )
    {
        LOG_DBG()<<fVal1<<rmin;
        getTraceAvg( spuId, chId, fVal1 );
        LOG_DBG()<<fVal1<<rmin;
    }
    if ( fVal1 > 225 )
    {
        LOG_DBG()<<fVal1<<rmin;

        return ERR_CAL_DAC;
    }

    pCH->setDac( rmax );
    pCH->flushWCache();
    wait_n( TRACE_WAIT_DATA );
    if ( ERR_NONE != getTraceAvg( spuId, chId, fVal2 ) )
    {
        return ERR_CAL_FAIL_DATA;
    }

    dacReg = pCH->getDacReg();
    if ( dacReg != rmax )
    {
        LOG_DBG()<<dacReg<<rmax;
        return ERR_CAL_DAC;
    }
    if ( fVal2 < 25 )
    {
        LOG_DBG()<<fVal2<<rmax;
        getTraceAvg( spuId, chId, fVal2 );
        LOG_DBG()<<fVal2<<rmax;
    }

    if ( fVal2 < 25 )
    {
        LOG_DBG()<<fVal2<<rmax;
        return ERR_CAL_DAC;
    }

    int a, b;

    if ( fVal1 < fVal2 && ( fVal2 - fVal1 ) > 25 )
    {
        if ( adc < fVal1 || adc > fVal2 )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }

        a = rmin;
        b = rmax;
    }
    else if ( fVal1 > fVal2 && ( fVal1 - fVal2 ) > 25 )
    {
        if ( adc < fVal2 || adc > fVal1 )
        {
            logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }

        a = rmax;
        b = rmin;
    }
    else
    {
        logInfo( __FUNCTION__, QString("%1/%2/%3").arg(adc).arg(fVal1).arg(fVal2) );
        LOG_DBG();
        return ERR_CAL_DAC;
    }

    int dac;
    int step = (b - a) / 2;

    //! upper range
    dac = b;
    bool bDec = false;
    while ( step != 0 )
    {
        pCH->setDac( dac );
        pCH->flushWCache();
        wait_n( TRACE_WAIT_DATA );
        if ( ERR_NONE != getTraceAvg( spuId, chId, fVal ) )
        {
            return ERR_CAL_FAIL_DATA;
        }

        //! check end
        if ( fabs(fVal - adc) < LSB_ERR )
        {
            outVal = fVal;
            outDac = dac;
            return ERR_NONE;
        }

        if ( fVal > adc )
        {
            dac -= step;
            bDec = true;
        }
        else if ( fVal < adc )
        {
            if ( bDec )
            {
                step /= 2;
                bDec = false;
            }

            if( (dac+step)> rmax)
            {
                dac = attempt-1000;
                qDebug() << "need more time to cal";
            }
            else
            {
                dac += step;
            }
        }
        else
        {
            break;
        }

        if ( dac < rmin || dac > rmax )
        {
            logInfo( __FUNCTION__, QString("%1-%2-%3").arg(dac).arg(rmin).arg(rmax));
            LOG_DBG();
            return ERR_CAL_FAIL_DATA;
        }
    }

    outVal = fVal;
    outDac = dac;

    return ERR_NONE;
}
DsoErr servCal::sampDac( int spuId,
                         int chId,
                         int masterChId,
                         int dac,
                         float *pSamp )
{
    Q_ASSERT( NULL != pSamp );

    cplatform *platform;
    IPhyCH *pCH;

    //! get platform
    platform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != platform );

    pCH = &platform->m_pPhy->m_ch[masterChId];

    pCH->setDac( dac );
    pCH->flushWCache();
    wait_n( ITER_WAIT_TIME );
    float fVal;
    if ( ERR_NONE != getAvg( spuId, chId, fVal ) )
    {
        LOG()<< "ERROR" << spuId << chId << dac << fVal;
        return ERR_CAL_FAIL_DATA;
    }

    *pSamp = fVal;

    return ERR_NONE;
}


DsoErr servCal::iterDelta(
                  int spuId,
                  int chId, int masterChId,
                  int adcTop,
                  int adcBase,
                  int &adcDelta,
                  float &fDelta )
{
    DsoErr err;
    int dacH, dacL;
    float h, l;

    err = iterDac( spuId, chId, masterChId, adcTop, dacH, h );
    if ( err != ERR_NONE ) return err;

    err = iterDac( spuId, chId, masterChId, adcBase, dacL, l );
    if ( err != ERR_NONE ) return err;

    Q_ASSERT( adcTop!= adcBase );
    Q_ASSERT( h!=l );
    float rat = (h-l)/(adcTop-adcBase);

    fDelta = ( dacH - dacL )/rat;
    adcDelta = fDelta;

    return ERR_NONE;
}

DsoErr servCal::iterDelta(
                  int spuId,
                  int chId, int masterChId,
                  int adcTop,
                  int adcBase,
                  int *pAdcDelta,
                  float *pfDelta )
{
    DsoErr err;
    int dacH, dacL;
    float h, l;

    err = iterDac( spuId, chId, masterChId, adcTop, dacH, h );
    if ( err != ERR_NONE ) return err;

    err = iterDac( spuId, chId, masterChId, adcBase, dacL, l );
    if ( err != ERR_NONE ) return err;

    Q_ASSERT( adcTop!= adcBase );
    Q_ASSERT( h!=l );
    float rat = (h-l)/(adcTop-adcBase);

    *pfDelta = ( dacH - dacL )/rat;
    *pAdcDelta = *pfDelta;

    return ERR_NONE;
}


