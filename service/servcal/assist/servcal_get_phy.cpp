
#include "../servcal.h"

CDsoRecEngine *servCal::getRecEngine()
{
    CDsoRecEngine *pEngine;

    pEngine = (CDsoRecEngine*)m_pDsoPlatform->getRecEngine();
    Q_ASSERT( NULL != pEngine );

    return pEngine;
}

IPhyCH* servCal::getCH( int chId )
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( chId >= 0 && chId <= 3 );

    return &m_pDsoPlatform->m_pPhy->m_ch[ chId ];
}
IPhyCH* servCal::getCH( Chan ch )
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    return &m_pDsoPlatform->m_pPhy->m_ch[ ch - chan1 ];
}

IPhyAfe* servCal::getAfe( Chan ch )
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    return &m_pDsoPlatform->m_pPhy->mVGA[ ch - chan1 ];
}

IPhyAfe* servCal::getAfe( int chId )
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( chId >= 0 && chId <= 3 );

    return &m_pDsoPlatform->m_pPhy->mVGA[ chId  ];
}

IPhyScu *servCal::getScu( int id )
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return m_pDsoPlatform->m_pPhy->mScuGp[ id ];
}
IPhySpu *servCal::getSpu( int id )
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return m_pDsoPlatform->m_pPhy->mSpuGp[ id ];
}
IPhyScuGp *servCal::getScuGp()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mScuGp;
}
IPhySpuGp *servCal::getSpuGp()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mSpuGp;
}
IPhyCcu *servCal::getCcu()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mCcu;
}
int servCal::getSpuCnt()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return m_pDsoPlatform->m_pPhy->getSpuCnt();
}
IPhyADC *servCal::getAdc( int id )
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( id >= 0 && id < 2 );

    return &m_pDsoPlatform->m_pPhy->mAdc[id];
}
IPhyADC *servCal::getAdcGp()
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return m_pDsoPlatform->m_pPhy->mAdc;
}
int servCal::getAdcCnt()
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return m_pDsoPlatform->m_pPhy->getAdcCnt();
}

IPhyWpu *servCal::getWpu()
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return &m_pDsoPlatform->m_pPhy->mWpu;
}

IPhyDac *servCal::getDac()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mDac;
}
IPhyPLL *servCal::getPll()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mPll;
}
IPhyPath* servCal::getLaL()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mLa[0];
}
IPhyPath* servCal::getLaH()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mLa[1];
}

IPhyPath *servCal::getExt()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mExt;
}

quint32 servCal::getLaPattern()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return m_pDsoPlatform->m_pPhy->mSpu.getLaPattern();
}

void servCal::flushPhy()
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return m_pDsoPlatform->m_pPhy->flushWCache();
}

void servCal::select1M_1( Chan ch )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( ch );

    pAfe->setOlEn( 0 );
    pAfe->setVosExp( 1 );

    pAfe->setOSel( 2 );

    pAfe->setGpo0( 1 );     //! dc

    pAfe->setHzNpd( 1 );
    pAfe->setLpp( 0 );
    pAfe->setLzNpd( 0 );

    pAfe->flushWCache();
}
void servCal::select1M_7( Chan ch )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( ch );

    pAfe->setOlEn( 0 );
    pAfe->setVosExp( 0 );

    pAfe->setOSel( 2 );//Hiz

    pAfe->setGpo0( 1 );

    pAfe->setHzNpd( 1 );
    pAfe->setLpp( 0 );
    pAfe->setLzNpd( 0 );

    pAfe->flushWCache();
}
void servCal::select50_2( Chan ch )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( ch );

    pAfe->setOlEn( 1 );
    pAfe->setVosExp( 1 );
    pAfe->setGpo1( 1 );

    pAfe->setISel( 2 );
    pAfe->setOSel( 0 );

    pAfe->setLzNpd( 1 );
    pAfe->setHzNpd( 0 );

    pAfe->flushWCache();
}
void servCal::select50_20( Chan ch )
{
    IPhyAfe *pAfe;
    pAfe = getAfe( ch );

    pAfe->setOlEn( 1 );
    pAfe->setVosExp( 1 );
    pAfe->setGpo1( 0 );

    pAfe->setISel( 0 );
    pAfe->setOSel( 0 );

    pAfe->setLzNpd( 1 );
    pAfe->setHzNpd( 0 );

    pAfe->flushWCache();
}

void servCal::selectAdcPath( calAdcPath adcPath )
{
    IPhyADC *pAdc = getAdcGp();

    getCcu()->setCcuRun( 0 );

    pAdc[0].setMode( (quint32)adcPath, aim_calibrate );
    if ( getAdcCnt() > 1 )
    { pAdc[1].setMode( (quint32)adcPath, aim_calibrate ); }

    getSpuGp()->setCtrl_adcsync();

    getSpuGp()->setSampClr();

    getCcu()->setCcuRun( 1 );
    m_pDsoPlatform->m_pPhy->flushWCache();
}

IPhyPath *servCal::getProbePath(int id)
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( id >= 0 && id <= 3 );
    return &m_pDsoPlatform->m_pPhy->mProbe[id];
}

IphyProbe *servCal::getProbe(int id)
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( id >= 0 && id <= 3 );
    return &m_pDsoPlatform->m_pPhy->m1WireProbe[id];
}

void servCal::setSpuInvert(bool invert)
{
    //! spu invert on
    IPhySpuGp *pSpuGp = getSpuGp();
    if( invert )
    {
        pSpuGp->setCTRL_adc_data_inv( 0xf );
    }
    else
    {
        pSpuGp->setCTRL_adc_data_inv( 0x0 );
    }
    pSpuGp->flushWCache();
}
