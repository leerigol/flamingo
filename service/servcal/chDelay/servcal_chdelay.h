#ifndef SERVCAL_CHDELAY
#define SERVCAL_CHDELAY

#include "../../include/dsotype.h"
#include "../../baseclass/checkedstream/ccheckedstream.h"

struct DelayToCh1
{
    int chiToCh1[4][2];
};

//! 分别以通道1的50ohm和1Mohm作为基准
class calChDelayPayload
{
public:
    DelayToCh1 mToCH1Delay[2];
};

//! ch delay data
class calChDelayData : public CCheckedStream
{
public:
    calChDelayPayload mPayload;
};

#endif // SERVCAL_CHDELAY
