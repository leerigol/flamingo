#include "servcal_chdelay.h"

#include "servcal.h"

DsoErr servCal::chDelaySave()
{
    qint32 size = sizeof(mCalChDelay.mPayload );
    if ( size != mCalPara.save( mCalFileDbs[1].mCHDelay, &mCalChDelay.mPayload, size ) )
    {
        return ERR_CAL_WRITE_FAIL;
    }
    return ERR_NONE;
}

DsoErr servCal::chDelayLoad(const QString &fileName)
{
    //! new
    calChDelayData *pData;
    pData = new calChDelayData();
    if ( NULL == pData )
    { return ERR_CAL_READ_FAIL; }

    //! read
    int size = sizeof(pData->mPayload);
    if ( pData->load( fileName, &pData->mPayload, size ) != size )
    {
        delete pData;
        return ERR_CAL_READ_FAIL;
    }

    //! load data
    mCalChDelay = *pData;
    delete pData;

    return ERR_NONE;
}

void *servCal::getChDalayData()
{
    return &mCalChDelay.mPayload;
}
