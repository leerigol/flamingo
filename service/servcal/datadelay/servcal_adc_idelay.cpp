
#include "../servcal.h"
#include "../../include/dsodbg.h"

#define is_pattern_match( patt, ret )  ((patt)&(ret))==(ret)
#define window_min_size 5

#if 0
// test data delay calibration for 100 times
DsoErr servCal::horiCalProc( int /*progFrom*/, int /*steps*/ )
{
    DsoErr err = ERR_NONE;

    preIDelay();

    int passCnt=0;
    QFile fileStream("/user/data/idelay.csv");
    if ( fileStream.open(QIODevice::WriteOnly) )
    { }
    else
    { return ERR_NONE; }
    QByteArray stream;

    for ( int i = 0; i < 100; i++ )
    {
        err = iDelay();

        if ( err == ERR_NONE )
        {
            passCnt++;
            mCalHori.mPayload.mIcIDelay[0].exportOut( stream );
        }
        else
        {
        }
        logInfo( QString("p/t: %1/%2").arg(passCnt).arg(i+1) );
    }

    fileStream.write( stream );
    fileStream.close();

    postIDelay();

    return err;
}
#else
DsoErr servCal::horiCalProc( int progFrom, int steps )
{
    DsoErr err = ERR_NONE;

    preIDelay();

    setProgNow( progFrom );

    err = iDelay();

    setProgNow( progFrom + steps );

    postIDelay();

    return err;
}
#endif


void servCal::preIDelay()
{
    logInfo("enter IDELAY");

    //! var
    IPhyWpu *pWpu;
    IPhyADC *pAdc;

    IPhySpuGp *pSpuGp;
    IPhyScuGp *pScuGp;

    //! get
    pScuGp = getScuGp();
    pSpuGp = getSpuGp();

    pWpu = getWpu();
    pAdc = getAdcGp();

    //! stop
    getCcu()->setCcuRun( 0 );

    //SCU open ch1/2/3/4. 4 chan mode
    pScuGp->setMODE_10G_chn_10G( 0xf );

    //SPU Gray code
    pSpuGp->setDEBUG_gray_bypass( 1 );

    //! for each adc
    for ( int i = 0; i < getAdcCnt(); i++ )
    {
        pAdc[i].setRMOD( 0 );
        pAdc[i].setMode( 3, aim_calibrate );
        pAdc[i].setTest( true );
    }

    //! wpu gain, offset
    pWpu->setanalog_ch0_gain( 420*4 );
    pWpu->setanalog_ch1_gain( 420*4 );
    pWpu->setanalog_ch2_gain( 420*4 );
    pWpu->setanalog_ch3_gain( 420*4 );

    pWpu->setNoise( 0 );

    pWpu->setanalog_ch0_offset( 0 );
    pWpu->setanalog_ch1_offset( 20 );
    pWpu->setanalog_ch2_offset( 40 );
    pWpu->setanalog_ch3_offset( 60 );

    //! sync is needed
    IPhySpu *pSpu = getSpu();
    pSpu->setCtrl_adcsync();

    getCcu()->setFRAME_PLOT_NUM( 1 );

    pWpu->flushWCache();

    //! flush
    pScuGp->flushWCache();
    pSpuGp->flushWCache();

#ifdef DAMREY_VER_1_5_x
    setSpuInvert( true );
#else
    setSpuInvert( false );
#endif

    //! run again
    getCcu()->setCcuRun( 1 );
    getCcu()->flushWCache();
}

void servCal::postIDelay()
{
    IPhyADC *pAdcGp = getAdcGp();

    Q_ASSERT( NULL != pAdcGp );
    for ( int i = 0; i < getAdcCnt(); i++ )
    {
        pAdcGp[i].setTest( false );
        pAdcGp[i].flushWCache();
    }

    getSpuGp()->setDEBUG_gray_bypass( 0 );
    getSpuGp()->flushWCache();

    //! plot num
    getCcu()->setFRAME_PLOT_NUM( 1000 );

    logInfo("exit IDELAY");
}

DsoErr servCal::iDelay()
{
    DsoErr err;

    //! two adcs
    packageIDelay packDelay[2];

    //! for each spu
    for ( int i = 0; i < getSpuCnt(); i++ )
    {
        //! adc1/2
        err = iDelay_scan_lvds( i, packDelay + i );
        if ( err != ERR_NONE )
        {
            return err;
        }
    }


    err = iDelay_save( packDelay );

    return err;
}

//! for each lvds
DsoErr servCal::iDelay_scan_lvds( int spuId,
                         packageIDelay *pPackDelay
                         )
{
    Q_ASSERT( NULL != pPackDelay );

    DsoErr err;
    IPhyADC *pADC;

    pADC = getAdc( spuId );

    //! from ilvds
    int ilvdsFrom, ilvdsTo;

    r_meta::CServMeta meta;
    meta.getMetaVal("servcal/ilvds/from", ilvdsFrom );
    meta.getMetaVal("servcal/ilvds/to", ilvdsTo );

    //! 0/2/1/3
    for ( ; ilvdsFrom <=ilvdsTo; ilvdsFrom++ )
    {
        pADC->setIlvds( ilvdsFrom );

        err = iDelay_scan( spuId, pPackDelay );
        if ( err == ERR_NONE )
        {
            pPackDelay->mILVDS = ilvdsFrom;
            LOG_DBG()<<"spuid/ilvds"<<spuId<<ilvdsFrom;
            return ERR_NONE;
        }

        LOG_DBG()<<ilvdsFrom<<" not OK";
    }

    return ERR_CAL_IDELAY_WND;
}

DsoErr servCal::iDelay_scan( int spuId,
                             packageIDelay *pPackDelay
                             )
{
    DsoErr err;
    IPhySpu *pSpu;

    QList< Spu_ADC_DATA_CHECK > bitCheckList;

    //! for 4 cores
    adcIDelay iDelays[4];

    pSpu = getSpu( spuId );

    //! foreach bit
    for( int i = 0; i < 8; i++ )
    {
        //! for all core
        pSpu->setADC_DATA_IDELAY_core_ABCD( 0xff );
        pSpu->setADC_DATA_IDELAY_l( 1 );
        pSpu->setADC_DATA_IDELAY_h( 1 );

        //! scan a bit
        bitCheckList.clear();
        iDelay_scan_x( spuId, i, bitCheckList );

        //! detect the window
        //! scan all window, do not check error
        err = iDelay_wnd( bitCheckList,
                          i,
                          iDelays );

        //! fail
        if ( err != ERR_NONE )
        {
            //logInfo( __FUNCTION__, QString("bit%1 fail").arg(i) );
            return err;
        }

        //! apply the window
        iDelay_apply( spuId, i, iDelays );
    }

    //! export to core
    for ( int i = 0; i < 4; i++ )
    {
        pPackDelay->coreDelay[i] = iDelays[i];
    }

    //! verify the window
    if ( ERR_NONE != iDelay_verify( spuId ) )
    {
        return ERR_CAL_IDELAY_WND;
    }

    return ERR_NONE;
}


void servCal::iDelay_scan_x(
                      int spuId,
                      int bitX,
                      QList<Spu_ADC_DATA_CHECK> &checkList )
{
    IPhySpu *pSpu;

    Spu_ADC_DATA_CHECK check;

    //! init checklist
    checkList.clear();

    //! device
    pSpu = getSpu( spuId );

    //! some bit
    for( int j = 0; j < 32; j++ )
    {
        //! deselect
        pSpu->setADC_DATA_IDELAY_chn_bit( 0 );
        pSpu->flushWCache();


        //! select, set delay
        pSpu->setADC_DATA_IDELAY_chn_bit( 1<<bitX );
        pSpu->setADC_DATA_IDELAY_idelay_var( j );
        pSpu->flushWCache();

        iDelay_clr( spuId );
        //! wait a few time
        QThread::usleep( (1<<bitX) );

        //! get check status
        check.payload = pSpu->getADC_DATA_CHECK();

        checkList.append( check );

//        LOG()<<"bit/tap/check"<<bitX<<j<<QString::number( check.payload, 16 );
    }
}

DsoErr servCal::iDelay_wnd(
                   QList< Spu_ADC_DATA_CHECK > &checkList,
                   int bitX,
                   adcIDelay iDelays[4]
                   )
{
    DsoErr err;
    //! bit0 : 1
    //! bit1 : 2
    //! bit2~7 : 4
    quint32 checkPatterns[]={ 0x1,
                              0x2,
                              0x4 };

    quint32 patt;
    if ( bitX == 0 )
    {
        patt = checkPatterns[0];
    }
    else if ( bitX == 1 )
    {
        patt = checkPatterns[1];
    }
    else
    {
        patt = checkPatterns[2];
    }

    int shift = 0;
    for ( int i = 0; i < 4; i++ )
    {
        //! low byte 4 bits
        err = iDelay_wnd( checkList, shift, 0xf, patt,
                    iDelays[i].mLBitWnd + bitX );
        shift += 4;
        if ( err != ERR_NONE )
        {
            //logInfo(__FUNCTION__, QString("fail core%1 LB").arg(i) );
            return err;
        }

        //! high byte 4 bits
        err = iDelay_wnd( checkList, shift, 0xf, patt,
                    iDelays[i].mHBitWnd + bitX );
        shift += 4;
        if ( err != ERR_NONE )
        {
            //logInfo(__FUNCTION__, QString("fail core%1 HB").arg(i) );
            return err;
        }
    }

    return ERR_NONE;
}

DsoErr servCal::iDelay_wnd(
                   QList< Spu_ADC_DATA_CHECK > &checkList,
                   int offs,
                   quint32 mask,
                   quint32 pattern,
                   bitWindow *pWnd )
{
    //! foreach step
    quint32 check;

    int left = -1;
    int len = 0;

    for ( int i = 0; i < checkList.size(); i++ )
    {
        //! extract the check val
        check = (checkList[i].payload >> offs) & mask;

        //! match pattern
        if ( ((pattern)&(check)) == (pattern) )
        {
            //! save the start
            if ( len < 1 )
            {
                left = i;
                len = 1;
            }
            else
            {
                len++;
            }
        }
        //! not match
        else
        {
            //! has wnd now
            if ( len >= window_min_size )
            {
                break;
            }
            //! window fail
            else
            {
                len = 0;
            }
        }
    }

    //! check window
    if ( len >= window_min_size  )
    {
    }
    else
    {
        logInfo(__FUNCTION__, QString("%1 %2 %3").arg(offs).arg(left).arg(len) );
        return ERR_CAL_IDELAY_WND;
    }

    pWnd->setWindow( left, len );

    return ERR_NONE;
}

void servCal::iDelay_apply( int spuId,  //! spu1/2
                            int bitX,
                            adcIDelay iDelays[4],bool cal )
{
    IPhySpu *pSpu;
    IPhyScu *pScu;

    pSpu = getSpu( spuId );
    Q_ASSERT( NULL != pSpu );
    pScu = getScu( spuId );
    Q_ASSERT( NULL != pScu );

    //! foreach core
    for ( int i = 0; i < 4; i++ )
    {
        //! select a core
        pSpu->setADC_DATA_IDELAY_core_ABCD( 0x8>>i );

        //! ---- low byte
        pSpu->setADC_DATA_IDELAY_l( 1 );
        pSpu->setADC_DATA_IDELAY_h( 0 );

        //! deselct
        pSpu->setADC_DATA_IDELAY_chn_bit( 0 );
        pSpu->setADC_DATA_IDELAY_idelay_var( iDelays[i].mLBitWnd[bitX].mIDelay );
        pSpu->flushWCache();

        //! select
        pSpu->setADC_DATA_IDELAY_chn_bit( 1<<bitX );
        pSpu->setADC_DATA_IDELAY_idelay_var( iDelays[i].mLBitWnd[bitX].mIDelay );
        pSpu->flushWCache();

        //! ---- high byte
        pSpu->setADC_DATA_IDELAY_l( 0 );
        pSpu->setADC_DATA_IDELAY_h( 1 );

        pSpu->setADC_DATA_IDELAY_chn_bit( 0 );
        pSpu->setADC_DATA_IDELAY_idelay_var( iDelays[i].mHBitWnd[bitX].mIDelay );
        pSpu->flushWCache();

        pSpu->setADC_DATA_IDELAY_chn_bit( 1<<bitX );
        pSpu->setADC_DATA_IDELAY_idelay_var( iDelays[i].mHBitWnd[bitX].mIDelay );
        pSpu->flushWCache();

//        LOG()<<"core/bit/L/from/len"<<i<<bitX<<iDelays[i].mLBitWnd[bitX].mIDelay<<iDelays[i].mLBitWnd[bitX].mLeft<<iDelays[i].mLBitWnd[bitX].mLen;
//        LOG()<<"core/bit/H/from/len"<<i<<bitX<<iDelays[i].mHBitWnd[bitX].mIDelay<<iDelays[i].mHBitWnd[bitX].mLeft<<iDelays[i].mHBitWnd[bitX].mLen;
    }

    iDelay_clr( spuId, cal );

    //! deselect
    pSpu->setADC_DATA_IDELAY_chn_bit( 0 );

}

//! scan for each bit
DsoErr servCal::iDelay_verify( int spuId )
{
    IPhySpu *pSpu;

    pSpu = getSpu( spuId );

    //! select all core
    pSpu->setADC_DATA_IDELAY_core_ABCD( 0xff );
    pSpu->setADC_DATA_IDELAY_l( 1 );
    pSpu->setADC_DATA_IDELAY_h( 1 );
    pSpu->setADC_DATA_IDELAY_chn_bit( 0xff );

    Spu_ADC_DATA_CHECK check;
    check.payload = pSpu->getADC_DATA_CHECK();
//    logInfo( QString::number(check.payload,16) );

    //! all pass
    if ( check.payload == 0xffffffff )
    {}
    else
    {
        return ERR_CAL_IDELAY_WND;
    }

    return ERR_NONE;
}

void servCal::iDelay_clr( int spuId, bool cal )
{
    IPhySpuGp *pSpu;
    IPhyScu *pScu;

    pSpu = getSpuGp();
    Q_ASSERT( NULL != pSpu );
    pScu = getScu( spuId );
    Q_ASSERT( NULL != pScu );

    //! now sync
    getCcu()->setCcuRun( 0 );

    pSpu->setSampClr();

    if( cal )
    {
        getCcu()->setCcuRun( 1 );
    }


    pSpu->flushWCache();
    pScu->flushWCache();
    getCcu()->flushWCache();
}

DsoErr servCal::iDelay_save( packageIDelay packDelay[2] )
{
    //! trans data
    mCalHori.mPayload.mIcIDelay[0] = packDelay[0];
    mCalHori.mPayload.mIcIDelay[1] = packDelay[1];

    int size = sizeof( mCalHori.mPayload );
    if ( size != mCalHori.save( mCalFileDbs[1].mLineDelay[0], &mCalHori.mPayload, size ) )
    {
        return ERR_CAL_WRITE_FAIL;
    }

    return ERR_NONE;
}

DsoErr servCal::iDelay_load( const QString &fileName0,
                             const QString &/*fileName1*/ )
{
    //! new
    calHori *theCalHori = new calHori;
    if ( NULL == theCalHori )
    { return ERR_CAL_READ_FAIL; }

    //! load
    int size = sizeof( theCalHori->mPayload);
    if ( size != theCalHori->load( fileName0, &theCalHori->mPayload, size ) )
    {
        delete theCalHori;
        return ERR_CAL_READ_FAIL;
    }

    //! export
    mCalHori = *theCalHori;
    delete theCalHori;

    return ERR_NONE;
}


