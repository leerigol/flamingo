#ifndef SERVCAL_DATADELAY
#define SERVCAL_DATADELAY

#include <QtCore>

#include "../../baseclass/checkedstream/ccheckedstream.h"

class bitWindow
{
public:
    int mLeft, mLen;
    int mIDelay;

public:
    bitWindow()
    {
        mLeft = -1;
        mLen = 0;

        mIDelay = 0;
    }

    void setWindow( int left, int len )
    {
        mLeft = left;
        mLen = len;

        //! save
        mIDelay = mLeft + mLen/2;
    }

    void exportOut( QTextStream &stream )
    {
        QString str;

        str = QString("%1,%2,%3,").arg( mLeft ).arg(mLen).arg(mIDelay);

        stream << str ;
    }
};

//! has 16 bit
class adcIDelay
{
public:
    bitWindow mLBitWnd[8];
    bitWindow mHBitWnd[8];

public:
    adcIDelay()
    {
    }

    void exportOut( QTextStream &stream )
    {
        for ( int i = 0; i< 8; i++ )
        {
            mLBitWnd[i].exportOut( stream );
        }
        for ( int i = 0; i< 8; i++ )
        {
            mHBitWnd[i].exportOut( stream );
        }

        stream << "\n" ;
    }

    bool isOk()
    {
        for ( int i = 0; i < 8; i++ )
        {
            if ( mLBitWnd[i].mLen == 0 ) return false;
            if ( mHBitWnd[i].mLen == 0 ) return false;
        }

        return true;
    }
};

class packageIDelay
{
public:
    packageIDelay()
    { mILVDS = 0; }         //! default
public:
    adcIDelay coreDelay[4]; //! has 4 internal core
    int mILVDS;
public:
    void exportOut( QTextStream &stream )
    {        
        for( int i = 0; i < 4; i++ )
        {
            stream << QString("CORE%1:,").arg(i+1) <<  QString("LDVS=%1").arg(mILVDS) << endl;

            stream << "L.Left,L.Len,L.Delay,H.Left,H.Len,H.Delay" << endl;
            for ( int j = 0; j< 8; j++ )
            {
                coreDelay[i].mLBitWnd[j].exportOut( stream );
                coreDelay[i].mHBitWnd[j].exportOut( stream );
                stream <<endl;
            }
        }
    }

    bool isOk()
    {
        for( int i = 0; i < 4; i++ )
        {
            if ( !coreDelay[i].isOk() )
            { return false; }
        }

        return true;
    }
};

struct calHoriPayload
{
    //! for each adc
    packageIDelay mIcIDelay[2];
};

class calHori : public CCheckedStream
{
public:
    calHoriPayload mPayload;
};


#endif // SERVCAL_DATADELAY

