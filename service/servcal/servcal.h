#ifndef SERVCAL_H
#define SERVCAL_H

#include "../../engine/cplatform.h"
#include "../../engine/record/cdsorecengine.h"
#include "../service.h"


#include "../../phy/ch/pathcal.h"
#include "../../phy/scu/iphyscu.h"
#include "../../phy/spu/iphyspu.h"
#include "../../phy/adc/iphyadc.h"
#include "../../phy/iphypll.h"

#include "./arith/vscale.h"

#include "./datadelay/servcal_datadelay.h"
#include "./vert/servcal_vert.h"
#include "./vert/servcal_lf.h"
#include "./adccore/servcal_adccore.h"

#include "./phase/servcal_phase.h"
#include "./ext/servcal_path.h"
#include "./pll/servcal_pll.h"
#include "./adccore/sampcase.h"

#include "../servicerequest.h"
#include "../servgui/servgui.h"

#include "chDelay/servcal_chdelay.h"
#define wait_n( n )    QThread::msleep( (n) );

//! chan lsb
#define lsb_base        (1.0e-6f)
#define lsb_base_scale  (1/lsb_base)    //! 1e6

#define LSB_1M_OHM         (3.446e-5f*lsb_base_scale)
#define LSB_1M_OHM_X         (2.638e-4f*lsb_base_scale)

#define afe_50_lsb_1_2     (3.672e-5f*lsb_base_scale)
#define afe_50_lsb_1_20    (3.697e-4f*lsb_base_scale)

#define opab_ov_range   (0.059f*1.2)
#define hzsga1_mul0     (3.16)

#define opab_ov_range_loz_2x    (0.13f) //! 0.059*2
#define opab_ov_range_loz_20x   (1.3f)  //! 0.059*20

#define gnd_bias    10000
#define gnd_mid     32767

//! la lsb
#define LA_LSB  (0.77e-3*lsb_base_scale )

//! ext lsb
#define EXT_LSB     (0.382e-3*lsb_base_scale)

//! probe lsb
#define PROBE_LSB   (0.19e-3*lsb_base_scale)

//! dg lsb
#define DG_OFFSET_LSB   (0.19e-3*lsb_base_scale)
#define DG_REF_LSB      (0.19e-3*lsb_base_scale)

#define AMP_DIV         (8)
#define ADC_TOP         (128 + AMP_DIV*25/2)
#define ADC_BASE        (128 - AMP_DIV*25/2)

#define HZ_VOS_SCALE    (100000)

#define check_pattern   0x03

//! core cal
#define ADC_CORE_OFFSET_MID 512
#define ADC_CORE_GAIN_MID   512

#define ADC_CORE_SIDE       64

#define ADC_CORE_OFFSET_A   (ADC_CORE_OFFSET_MID+ADC_CORE_SIDE)
#define ADC_CORE_GAIN_A     (ADC_CORE_GAIN_MID+ADC_CORE_SIDE)

#define ADC_CORE_OFFSET_B   (ADC_CORE_OFFSET_MID-ADC_CORE_SIDE)
#define ADC_CORE_GAIN_B     (ADC_CORE_GAIN_MID-ADC_CORE_SIDE)

#define CAL_MAX_SCALE       (10000000) //10V
#define CAL_MIN_SCALE       (500)//500uV


#define LOG()   LOG_DBG()

#define CAL_SUCCESS "Success"

class calFileDb
{
public:
    calFileDb();

public:
    void load( const r_meta::CMeta &meta, const QString &path );

public:
    QString mVert;
    QString mLineDelay[1];
    QString mLF;
    QString mAdcGo[2];

    QString mExt;
    QString mLa[2];

    QString mAdcSamp[2];

    QString mPll5g, mPll1_1g;

    QString mCHDelay;

public:
    QString mPath;

public:
    QStringList mFileList;
};

class calThread;
class servCal : public serviceExecutor
{
    Q_OBJECT
protected:
    DECLARE_CMD()
public:
    enum servCmd
    {
        CMD_NONE,

        CMD_CAL_START,

        CMD_SAVE,
        CMD_LOAD,

        qCMD_CAL_PROGRESS_NOW,
        qCMD_CAL_PROGRESS_TOTAL,

        qCMD_CAL_STATUS,
        qCMD_CAL_DATE,

        qCMD_CAL_NOW,

        CMD_CAL_DETAIL,     //! string
        qCMD_CAL_DETAIL,

        CMD_CAL_UPDATED,

        CMD_RMT_ENTER,      //! ch
        CMD_RMT_EXIT,       //! ch
        qCMD_RMT_STATUS,    //! ch

        CMD_RMT_AFE_PATH,   //! ch, path

        CMD_RMT_AFE_K,      //! ch,k
        qCMD_RMT_AFE_K,     //! ch

        CMD_RMT_AFE_KX,     //! ch, kx
        qCMD_RMT_AFE_KX,    //! ch

        CMD_RMT_AFE_ZERO,   //! ch
        qCMD_RMT_AFE_ZERO,  //! ch

        CMD_RMT_AFE_LSB,    //! ch, path, lsb
        qCMD_RMT_AFE_LSB,   //! ch

        CMD_RMT_AFE_REG,    //! ch, addr, val
        qCMD_RMT_AFE_REG,   //! ch, addr

        CMD_RMT_DAC,        //! ch, slot, value

        CMD_RMT_AFE_1M_PATH,    //! ch, k, sga1
        CMD_RMT_AFE_VPP,        //! ? ch
        CMD_RMT_AFE_FPP,        //! ? ch
        CMD_RMT_AFE_AVG,        //  AVG OF ADC
        CMD_RMT_AFE_AVG1,        //  AVG OF ADC
        CMD_RMT_AFE_TRIM,       //! ch, k, trim, value

        CMD_RMT_AFE_RCSEL,      //! ch, k, r,c
        qCMD_RMT_AFE_RSEL,      //! ? ch, k
        qCMD_RMT_AFE_CSEL,      //! ? ch, k
        CMD_RMT_AFE_OSEL,       // added by hxh.
        CMD_RMT_AFE_DSEL,

        CMD_RMT_AFE_BEGIN_HIZAMP,
        CMD_RMT_AFE_STOP_HIZAMP,
        CMD_RMT_AFE_AZVOS,



        CMD_PHASE_REQUEST,      //! phase request *

        CMD_CAL_SELF,           //! 远程自校准
        CMD_CAL_DATA_LINE,      //! rmt data line

        CMD_SNAP_CH_AVG,        //! int chid

        CMD_CAL_PROBE,          //! 探头校准
        CMD_APPLY_PROBE_DATA,   //! 探头校准

        qCMD_CH_DELAY_PTR,               //! 通道延迟校准数据

        //! 5G
        CMD_CAL_PLL5_OUTPUT,    //! bool
        CMD_CAL_PLL5_REG,       //! reg, val
                                //! quint16, quint16
        CMD_CAL_PLL5_CFG,       //! bin stream

        //! 1.1G
        CMD_CAL_PLL1_1_OUTPUT,
        CMD_CAL_PLL1_1_REG,
        CMD_CAL_PLL1_1_CFG,

        /***********scpi***************/
        CMD_CAL_DATE,
        CMD_CAL_TIME,

        CMD_CAL_RESULT,
        CMD_BOOL_RESULT,
        CMD_SELF_START,
    };

    enum calItem
    {
        cal_item_base = 0,
        cal_item_all = 0,
        cal_idelay,
        cal_vert_ch1,
        cal_vert_ch2,
        cal_vert_ch3,
        cal_vert_ch4,

        cal_vert_50,
        cal_vert_1M,

        cal_adc_go,
        cal_adc_phase,

        cal_la,
        cal_ext,

        cal_item_top = cal_ext,
    };

    enum CalStatus
    {
        cal_deactive,
        cal_active,

        cal_running,
        cal_completed,
        cal_aborted,
    };

    enum CalPath
    {
        cal_path_50_x2,
        cal_path_50_x20,
        cal_path_1m_x1,
        cal_path_1m_x7,
    };

    enum LfTrim
    {
        dc_trim,
        lf_trim,
        mf_c_trim,
        hf_trim,
    };

    enum calFile
    {
        cal_file_gain_gnd,
        cal_file_data_delay,
        cal_file_ch_delay,
        cal_file_lf,
    };

    enum calAdcPath
    {
        cal_adc_abcd = 0x3,

        cal_adc_ac = 0x4,
        cal_adc_bc = 0x5,
        cal_adc_ad = 0x6,
        cal_adc_bd = 0x7,

        cal_adc_a = 0x8,
        cal_adc_b = 0x9,
        cal_adc_c = 0xa,
        cal_adc_d = 0xb,
    };

    enum calType
    {
        cal_self,
        cal_probe
    };

public:
    servCal( QString name, ServiceId id=E_SERVICE_ID_NONE );
    ~servCal();

public:
     void   registerSpy();
     DsoErr start();
     void   init();

     static bool m_bPreparing;

protected Q_SLOTS:
    void on_finished();

protected:
    virtual bool keyEat(int key, int count, bool bRelease );

public:
    void logInfo( const QString &str, int level = 0 );
    void logInfo( const QString &str, const QString &str2 );
protected:
    void startup();

    DsoErr onCalStart();
    DsoErr onCalStop();

    DsoErr setCalItems( qint32 items );
    qint32 getCalItems();

    DsoErr setWindow(bool);
    bool   getWindow();
    bool   m_bWindow;

    DsoErr onCalDefault();
    DsoErr onCalUser();
    DsoErr onExportUser();
    DsoErr onSelfStart();

    DsoErr on_project_changed( bool b );
    bool   isProjectMode();
    bool   m_bProject;
    void   setMenuEn(bool);

    void setProgNow( int now );
    int getProgNow();

    void setProgTotal( int total );
    int getProgTotal();

    void setStatus( CalStatus stat );
    CalStatus getStatus();

    qint64 getCalDate();
    DsoErr setCalDate(qint64 dt);

    void onCalUpdated();

    DsoErr onCalDetail( const QString &str );
    void* getCalDetail();

    //!远程 自校准
    DsoErr onCalSelf();

    DsoErr onCalDataLine();
    bool   getCalDataLine();
    int    getKIndex( int k );

    /*********scpi************/
    QString getLastCalDate();
    QString getLastCalTime();

    QString getResult();
    bool    getBoolResult();
    int     m_nResult;

protected:
    DsoErr onSave( calFile file );
    DsoErr onLoad( calFile file );

    //! lsbs
    DsoErr onRmtEnter( Chan ch );
    DsoErr onRmtExit( Chan ch );

    int onqRmtCalStatus( Chan ch );

    DsoErr onRmtAfePath( CArgument &arg );

    DsoErr onRmtAfeK( CArgument &arg );
    int onqRmtAfeK( Chan ch );

    DsoErr onRmtAfeKx( CArgument &arg );
    DsoReal onqRmtAfeKx( CArgument &arg );

    DsoErr onRmtAfeZero( Chan ch );
    int onqRmtAfeZero( Chan ch );

    DsoErr onRmtAfeLsb( CArgument &arg );
    DsoReal onqRmtAfeLsb( CArgument &arg );

    DsoErr onRmtAfeReg( CArgument &arg );
    int onqRmtAfeReg( CArgument &arg );

    DsoErr onRmtAfeOsel(CArgument &arg );
    DsoErr onRmtAfeDsel(CArgument &arg );

    DsoErr onRmtDac( CArgument &arg );

    //aa
    DsoErr onAzVosEnter(Chan ch);
    DsoErr onAzVosExit(Chan ch);
    DsoErr onRmtAzVos(CArgument &arg);


    //! lf
    DsoErr onRmtAfe1M( CArgument &arg );
    int onRmtVpp( Chan ch );
    int onRmtAvg( Chan ch );
    DsoReal onRmtAvg1( Chan ch );
    DsoReal onRmtFpp( Chan ch );

    DsoErr onRmtLfTrim( CArgument &arg );
    int onqRmtLfTrim( CArgument &arg );

    DsoErr onRmtLfRCSel( CArgument &arg );
    int onqRmtLfRSel( CArgument &arg );
    int onqRmtLfCSel( CArgument &arg );

    DsoErr setLfTrim(Chan ch,
                  int k,
                  LfTrim trim,
                  int val );
    int getLfTrim( Chan ch,
                   int k,
                   LfTrim trim );

    DsoErr setLfRCSel( Chan ch, int k,
                       int rSel, int cSel );
    int getLfRSel( Chan ch, int k );
    int getLfCSel( Chan ch, int k );

    DsoErr lfSave();
    DsoErr lfLoad( const QString &fileName );

    //! pll
    void   tune5g();
    void   tune1_1g();

    DsoErr onPll5Output( bool b );
    DsoErr onPll5Reg( CArgument &arg );
    DsoErr onPll5Cfg( CArgument &arg );
    DsoErr qPll5Cfg( CArgument &arg );

    DsoErr onPll1_1Output( bool b );
    DsoErr onPll1_1Reg( CArgument &arg );
    DsoErr onPll1_1Cfg( CArgument &arg );
    DsoErr qPll1_1Cfg( CArgument &arg );

    //! adc core
    DsoErr calAdcProc( int progFrom, int steps );

    void preAdcGO();
    void postAdcGO();

    DsoErr calAdcGO( int progFrom, int steps );

    DsoErr calAdcSave();
    DsoErr calAdcLoad( const QString &fileName0,
                       const QString &fileName1 );

    DsoErr calAdcGoExport();

    //! adc scan
    DsoErr calAdcScan( int spuCnt, int progFrom, int steps );

    DsoErr calAdcSamp( int spuId, SampleCase *pSampCase );

    DsoErr calAdcCoreScan(
                           int spuId,
                           SampleCase &sampCase,
                           int chId,
                           int masterId,
                           calLinear *pLine );

    DsoErr iterAdcCoreX(
                            int spuId,
                            SampleCase &sampCase,
                            int chId,
                            int masterId,
                            calLinear &lineRef,
                            adcCoreCal *pCoreCal );

    DsoErr searchAdcCoreX(
                           int spuId,
                           SampleCase &sampCase,
                           int chId,
                           int masterId,
                           int gain,            //! init gain
                           int offset,          //! init offset
                           calLinear &lineRef,  //! ref line
                           int *pOutGain,
                           int *pOutOff,
                           int gainRange = 8,
                           int offRange = 8
                           );
    //! two chs
    DsoErr calAdcScan_ac( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_ad( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_bc( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_bd( int spuCnt, SampleCase &sampCase );

    DsoErr calAdcScan_xx(     int spuCnt,
                              SampleCase &sampCase,
                              calAdcPath adcPath,
                              int masterId1,
                              int masterId2,
                              adcCal *pCal );

    //! 1 chs
    DsoErr calAdcScan_a_abcd( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_b_abcd( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_c_abcd( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_d_abcd( int spuCnt, SampleCase &sampCase );

    DsoErr calAdcScan_x_abcd( int spuCnt, SampleCase &sampCase,
                              calAdcPath adcPath,
                              int masterId,
                              adcCal *pCal );

    //! 4 chs
    DsoErr calAdcScan_a_a( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_b_b( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_c_c( int spuCnt, SampleCase &sampCase );
    DsoErr calAdcScan_d_d( int spuCnt, SampleCase &sampCase );

    DsoErr calAdcScan_x_x( int spuCnt,
                           SampleCase &sampCase,
                           calAdcPath adcPath,
                           int id,
                           adcCal *pCal );


    //! ----phase
    DsoErr calPhaseProc( int from, int steps );

    void enterCalPhase();
    void exitCalPhase();
    DsoErr balanceFullScale();
    DsoErr balanceScale();
    DsoErr balanceOffset();
    void switchISel();

    DsoErr calPhase( int from, int steps );

    DsoErr calPhaseReset();
    DsoErr calPhase4s( int chipId );
    DsoErr calPhase2s( int chipId );
    DsoErr calPhase1s( int chipId );

    DsoErr calPhase4( int chipId,
                      dso_phy::IPhyADC::eAdcInterleaveMode eMode,
                      coreSmpDelay *pCoreDelay );
    DsoErr calPhase2( int chipId,
                      dso_phy::IPhyADC::eAdcInterleaveMode eMode,
                      coreSmpDelay *pCoreDelay );
    DsoErr calPhase1( int chipId,
                      dso_phy::IPhyADC::eAdcInterleaveMode eMode,
                      coreSmpDelay *pCoreDelay );

    DsoErr iterPhase( int chipId,
                      coreSmpDelay *pCoreDelay,
                      float refs[3],
                      int inteCnt = 4 );

    void changeAdcMode( dso_phy::IPhyADC::eAdcInterleaveMode eMode,
                        DsoWorkAim aim = aim_calibrate );
    void startTrace();
    void stopTrace();

    //open or close AVG sample
    void setAcqAvg( bool b);

    void rstPhase();
    DsoErr getPhase( float refs[3], int inteCnt = 4 );
    qint32 phaseToTime( float phase );
    adcSMPDLY phaseToadcSMPDLY( phasePoint &pt );

    DsoErr phaseSave();
    DsoErr phaseLoad( const QString &fileName0,
                      const QString &fileName1 );

    DsoErr on_ack_phase_request( phaseRequest *pReq );

    DsoErr test_calPhase();
    DsoErr test_iterPhase();

    //! ----ext
    void preCalExt();
    void postCalExt();

    DsoErr extSave();
    DsoErr extLoad( const QString &fileName );

    DsoErr calExtProc( int from, int steps );
    DsoErr calExt( int from, int steps );
    DsoErr calExtRange( quint32 a,
                        quint32 b,
                        quint32 *pOutDac );

    //! ---- la
    void preCalLa();
    void postCalLa();
    DsoErr calLa( int from, int steps );

    DsoErr laSave();
    DsoErr laLoad( const QString &fileNameL,
                   const QString &fileNameH );

    DsoErr calLaProc( int from, int steps );

    int iterLa00( IPhyPath *pPath,
                int range1,
                int range2,
                quint32 mask,
                quint32 aim,
                quint32 base
                );
    int iterLaFF( IPhyPath *pPath,
                int range1,
                int range2,
                quint32 mask,
                quint32 aim,
                quint32 base
                );

    //! probe
    void    setCalType(calType type);
    calType getCalType();

    void  enterCalProbe(Chan ch);
    int   startCalProbe();

    void  applyProbeCalData(Chan ch);
    bool  probeZeroOffsCal(quint32 &value);
    bool  probeOffsGainCal( quint32 &value);
    bool  probeZeroValueCal(quint32 &value);
    bool  probeAdjustDacVal( quint32 &value);
    bool  probeDacCal(quint32 &value);
private:
    calType m_calType;  //!自矫正 或 探头校准
    Chan    m_probeCh;

    calThread *m_pWorker;
    QStringList mLogger, mViewLogger;

    //! phase cal
    phaseRequest mPhaseRequest;
    qint32 mPhaseDeltaTimefs[3];

    //! >>>> cal datas
    calData mCalPara; //vertical
    calHori mCalHori; // data line

    calLfData mCalLf;   //! lf trim data

                        //! adc cal data
    calAdcGOData mCalAdcGo;
    calPhaseData mCalAdcPhase[2];

    calChDelayData mCalChDelay;
                        //! path cal
    calPathData mExtCal;
    calPathData mLaCal[2];
    calPathData mProbeCal[4];
                        //! 0 -- default
    calFileDb mCalFileDbs[2];

    //! ui status
    int mProgNow;        //! current progress
    int mProgTotal;      //! total progress

    CalStatus mStatus;
    CalStatus mDataLineStatus;
    qint32 mCalItems, mCalFullItems;

                         //! dso platform
    cplatform *m_pDsoPlatform;
    int mRmtCalDac;

                         //! inner variables
    calPhaseIter mPhaseIters[3];

protected:
    DsoErr calProc(int progFrom, int steps );

    void preCal();
    void postCal();

    //! vertical
    DsoErr vertSave();
    DsoErr vertLoad( const QString &fileName );

    //! chDelayCal
    DsoErr chDelaySave();
    DsoErr chDelayLoad( const QString &fileName );
    void*  getChDalayData();

    void preVertCal();
    void postVertCal();
    DsoErr vertCalProc( int progFrom, int steps,
                        int chId = -1 );

    DsoErr vertCal( int progFrom, int steps, int chId );

    DsoErr zeroCal( int chId );
    void lozCal( int chId );
    void hizCal( int chId );

    //! lsb calc
    void calc1M_K50Lsb( int chid );
    void calc1M_K51Lsb( int chid );
    void calc50_2Lsb( int chid );
    void calc50_20Lsb( int chid );

    //! do test
    void calcTest();
    DsoErr snapCHAvg( int chid );


    void exportLf();
    void exportHzScales( int chId );
    void exportHzGnds( int chId );

    void exportLzScales( int chId );
    void exportLzScales(
                       const QString &fileName,
                       int count,
                       afeCfgPair *pPairs );

    void exportLzGnds( int chId );

    void exportvScales( const QString &fileName,
                        vScales &scales );

protected:
    void preSet( int chId );

    //! common
    DsoErr zeroProc( int chId );

    void preZero( int chId );
    void postZero( int chId );
    int workaroundHizZero( int vos );

    DsoErr zeroHizAmp( int chId, int &val );
    DsoErr zeroHizSga1Lg( int chId, int &val );
    DsoErr zeroHizSga1Hg( int chId, int &val );

    //! hiz
    void preHizCal( int chId, int vosExp );

    void preHizSga1Gain( int chId );
    DsoErr hizSga1Gain( int chId );

    void preHizGainAtten( int chId );
    DsoErr hizGainAtten( int chId );

    void preHizSga45Gain( int chId );
    DsoErr hizSga4Gain( int chId );
    DsoErr hizSga5Gain( int chId );

    DsoErr hizSga4Trim( int chId );
    DsoErr hizSga5Trim( int chId );

    //! clone from 5
    void hizSga4Clone( int chId );
    //! clone from 4
    void hizSga5Clone( int chId );

    void preHizRefScale( int chId );
    DsoErr hizRefScale( int chId );

    DsoErr hizScaleDeduce( int chId );

    DsoErr hizScaleDeduce_fill( int chId,
                                vScales &scales );

    DsoErr hizScaleDeduce_trim(
                         vScales &scales );
    DsoErr hizScale_KFilter( vScales &scales );

    DsoErr hizScaleDeduce_export( int chId,
                                vScales &scales);

    //! offset
    void preHizOffset( int chId );
    DsoErr hizOffset( int chId );
    DsoErr hizGnd( int chId );

    bool hzGndExist(
                   afeCfgPair *pScale,
                   int vos,
                   afeCfgPair *pPairs,
                   int count );

    void preHizLsbLg( int chId );
    void preHizLsbHg( int chId );
    DsoErr dacLsb( int chId );

    //! loz
    void preLozCal( int chId );

    void preLozSga1_2xGain( int chId );
    DsoErr lozSga1_2xGain( int chId );

    void preLozSga1_20xGain( int chId );
    DsoErr lozSga1_20xGain( int chId );

    void preLozSga1Trim( int chId );
    DsoErr lozSga1Trim( int chId );

    void preLozSga2Gain( int chId );
    DsoErr lozSga2Gain( int chId );
    DsoErr lozSga2Trim( int chId );

    void preLozSga3Gain( int chId );
    DsoErr lozSga3Gain( int chId );
    DsoErr lozSga3Trim( int chId );

    //! 4g
    void preLozSga45Gain( int chId );
    DsoErr lozSga4_4GGain( int chId );
    DsoErr lozSga5_4GGain( int chId );

    //! Flt
    void preLozSga45FltGain( int chId );
    DsoErr lozSga4_FltGain( int chId );
    DsoErr lozSga5_FltGain( int chId );

    //! clone from 5
    void lozSga4_4GClone( int chId );
    void lozSga4_FltClone( int chId );
    //! clone from 4
    void lozSga5_4GClone( int chId );
    void lozSga5_FltClone( int chId );

    void preLozRefScale( int chId );
    DsoErr lozRefScale( int chId );

    //! deduce
    DsoErr lozScaleDeduce( int chId );

    DsoErr lozScaleDeduce_2x_normal( int chId );
    DsoErr lozScaleDeduce_2x_flt( int chId );
    DsoErr lozScaleDeduce_20x_normal( int chId );
    DsoErr lozScaleDeduce_20x_flt( int chId );

    DsoErr lozScaleDeduce_pack( vScales &scales );

    DsoErr lozScaleDeduce_fill( int chId,
                                int sga1Cnt, float *pSga1,
                                float refScale,
                                vScales &scales,
                                float ovRng );

    DsoErr lozScaleDeduce_trim(
                         vScales &scales );

    DsoErr lozScaleDeduce_export( int att,
                                  vScales &scales,
                                  int flt,

                                  int cap,
                                  afeCfgPair *outAry,
                                  int *pCnt );

    //! gnd
    void preLozOffset( int chId );
    DsoErr lozOffset( int chId );
    DsoErr lozGnd( int chId );
    DsoErr lozGnd( int chId,
                   afeCfgPair *pScales,
                   int scaleCnt,
                   int flt,
                   int &g,
                   int scaleMin,int scaleMax );

    void preLozLsb1_2( int chId, int sel, int trim  );
    void preLozLsb1_20( int chId, int sel, int trim );

    //! assist
    DsoErr sga4Sel( int chId, float &ratio );
    DsoErr sga4Trim( int chId, int a, int b, float &ratio );

    DsoErr sga5Sel( int chId, float &ratio );
    DsoErr sga5Trim( int chId, int a, int b, float &ratio );

    DsoErr getAvg( int spuId, int chId, float &val );
    DsoErr getCoreAvg( int spuId, int coreId, float &val );
    DsoErr getCoreAvg1( int spuId, int coreId, float &val );
    DsoErr getTraceAvg( int spuId, int chId, float &val );

    DsoErr getVpp( int spuId, int chId, int &vpp );
    DsoErr getVpp( int spuId, Chan ch, int &vpp );
    DsoErr getVpp( int spuId, int vpps[4] );
    DsoErr getPeak( int spuId, int peaks[8] );
    DsoErr iterDac( int spuId,
                    int chId, int masterChId, int adc,
                    int &dac, float &val,
                    int rmin = 0,
                    int rmax=65535,
                    int attempt=32767 );

    DsoErr iterDac1(int spuId,
                    int chId, int masterChId, int adc,
                    int &dac, float &val,
                    int rmin = 0,
                    int rmax=65535,
                    int attempt=32767,
                    afeCfgPair *pCfg = NULL);

    DsoErr traceDac( int spuId,
                    int chId, int masterChId, int adc,
                    int &dac, float &val,
                    int rmin = 0,
                    int rmax=65535,
                    int attempt=32767 );

    DsoErr sampDac(
                   int spuId,
                   int chId,
                   int masterId,
                   int dac,
                   float *pSamp);


    DsoErr iterDelta( int spuId, int chId, int masterChId,
                      int adcTop,
                      int adcBase,
                      int &adcDelta,
                      float &fDelta );
    DsoErr iterDelta( int spuId, int chId, int masterChId,
                      int adcTop,
                      int adcBase,
                      int *adcDelta,
                      float *pfDelta );

    //! hori
    DsoErr horiCalProc( int progFrom, int steps );

    void preIDelay();
    void postIDelay();

    DsoErr iDelay();
    DsoErr iDelay_scan( int spuId,
                        packageIDelay *pPackDelay );
    DsoErr iDelay_scan_lvds( int spuId,
                             packageIDelay *pPackDelay
                             );
    void iDelay_scan_x( int spuId,
                        int bitX,
                        QList<Spu_ADC_DATA_CHECK> &checkList );

    //! find the window
    DsoErr iDelay_wnd( QList< Spu_ADC_DATA_CHECK > &checkList,
                       int bitX,
                       adcIDelay iDelays[4]
                       );

    DsoErr iDelay_wnd( QList< Spu_ADC_DATA_CHECK > &checkList,
                       int offs,
                       quint32 mask,
                       quint32 pattern,
                       bitWindow *pWnd );

    void iDelay_apply( int spuId,
                       int bitX,
                       adcIDelay iDelays[4],
                       bool cal = true
                       );

    DsoErr iDelay_verify( int spuId );

    void iDelay_clr( int spuId , bool cal = true);

    DsoErr iDelay_save(  packageIDelay packDelay[2] );
    DsoErr iDelay_load( const QString &fileName0,
                        const QString &fileName1 );
    DsoErr iDelayExport();

private:
    void loadInitData();

    DsoErr loadCalData( calFileDb &fileUser,
                        calFileDb &fileDef );

    void applyCalData( bool cal = true );

    //! assist
    CDsoRecEngine *getRecEngine();

    IPhyCH* getCH( int chId );
    IPhyCH* getCH( Chan ch );
    IPhyAfe* getAfe( Chan ch );
    IPhyAfe* getAfe( int chId );

    IPhyScu *getScu( int id = 0 );
    IPhySpu *getSpu( int id = 0 );
    IPhyScuGp *getScuGp();
    IPhySpuGp *getSpuGp();
    void       setSpuInvert(bool);

    IPhyCcu *getCcu();

    int getSpuCnt();

    IPhyADC *getAdc( int id = 0 );
    IPhyADC *getAdcGp();
    int getAdcCnt();

    IPhyWpu *getWpu();

    IPhyDac *getDac();

    IPhyPLL *getPll();

    IPhyPath* getLaL();
    IPhyPath* getLaH();

    IPhyPath *getExt();

    quint32 getLaPattern();

    void flushPhy();

    //! select path
    void select1M_1( Chan ch );
    void select1M_7( Chan ch );
    void select50_2( Chan ch );
    void select50_20( Chan ch );

    //! set adc mode
    void selectAdcPath( calAdcPath adcPath );

    IPhyPath  *getProbePath(int id = 0);
    IphyProbe *getProbe(int id = 0);

private:
    const static int _delayUnit = 10;    //ms
    static kAttenX _HizK[];
    static kAttenX _Dsa[];
friend class calThread;

};


#endif // SERVCAL_H
