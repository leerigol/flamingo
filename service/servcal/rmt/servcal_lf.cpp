
#include "../servcal.h"

//! ch, k, sga1
DsoErr servCal::onRmtAfe1M( CArgument &arg )
{
    Chan ch;
    int k, sga1;

    if ( arg.size() < 3 ) return ERR_INVALID_INPUT;

    ch = (Chan)arg[0].iVal;
    k = arg[1].iVal;
    sga1 = arg[2].iVal;

    select1M_1( ch );

    IPhyAfe *pAfe = getAfe( ch );

    pAfe->setK( k );
    pAfe->setHzSga1Gs( sga1 );

    pAfe->flushWCache();

    return ERR_NONE;
}
//! -1 -- get data fail
int servCal::onRmtVpp( Chan ch )
{
    DsoErr err;
    int vpp;

    err = getVpp( 0, ch-chan1, vpp );
    if ( err != ERR_NONE )
    { return -1; }

    return vpp;
}

int servCal::onRmtAvg( Chan ch )
{
    DsoErr err;
    int vpp;
    float avg;

    err = getAvg( 0, ch-chan1, avg );
    if ( err != ERR_NONE )
    { return -1; }

    vpp = (int)avg;
    return vpp;
}

DsoReal servCal::onRmtAvg1( Chan ch )
{
    DsoErr err;

    float avg;

    err = getAvg( 0, ch-chan1, avg );
    if ( err != ERR_NONE )
    {
        float f = 9.9e37;
        return DsoReal(f);
    }

    return  DsoReal(avg);
}

DsoReal servCal::onRmtFpp( Chan /*ch*/ )
{
    return DsoReal(0, E_0);
}

//! chan, k, lf_trim, value
DsoErr servCal::onRmtLfTrim( CArgument &arg )
{
    if ( arg.size() < 4 ) return ERR_INVALID_INPUT;

    Chan ch = (Chan)arg[0].iVal;
    int k = arg[1].iVal;
    LfTrim trim = (LfTrim)arg[2].iVal;
    int val = arg[3].iVal;

    //! get Afe
    IPhyAfe *pAfe = getAfe( ch );

    switch( trim )
    {
    case dc_trim:
        pAfe->setDcTrim( val );
        break;

    case lf_trim:
        pAfe->setLfTrim( val );
        break;

    case mf_c_trim:
        pAfe->setMfcTrim( val );
        break;

    case hf_trim:
        pAfe->setHfTrim( val );
        break;

    default:
        return ERR_INVALID_INPUT;
    }

    pAfe->flushWCache();

    //! set trim
    return setLfTrim( ch, k, trim, val );
}
//! chan, k, lf_trim
//! -1 -- error
int servCal::onqRmtLfTrim( CArgument &arg )
{
    if ( arg.size() < 3 ) return -1;

    Chan ch = (Chan)arg[0].iVal;
    int k = arg[1].iVal;
    LfTrim trim = (LfTrim)arg[2].iVal;

    return getLfTrim( ch, k, trim );
}

//! chan,k, r, c
DsoErr servCal::onRmtLfRCSel( CArgument &arg )
{
    if ( arg.size() < 4 ) return ERR_INVALID_INPUT;

    Chan ch = (Chan)arg[0].iVal;
    int k = arg[1].iVal;
    int rSel = arg[2].iVal;
    int cSel = arg[3].iVal;

    //! config afe rc
    IPhyAfe *pAfe = getAfe( ch );
    pAfe->setHzFilter( rSel, cSel );

    pAfe->flushWCache();

    return setLfRCSel( ch, k, rSel, cSel );
}


DsoErr servCal::onRmtAfeDsel(CArgument &arg )
{
    if ( arg.size() < 2 ) return ERR_INVALID_INPUT;

    Chan ch  = (Chan)arg[0].iVal;
    int dsel = arg[1].iVal;

    if( dsel == 0 || dsel == 1)
    {
        IPhyAfe *pAfe = getAfe( ch );
        pAfe->setDsel(dsel);
        pAfe->flushWCache();
        return ERR_NONE;
    }

    return ERR_INVALID_INPUT;
}

DsoErr servCal::onRmtAfeOsel(CArgument &arg )
{
    if ( arg.size() < 2 ) return ERR_INVALID_INPUT;

    Chan ch  = (Chan)arg[0].iVal;
    int osel = arg[1].iVal;

    if( osel == 0 || osel == 1 || osel == 2)
    {
        IPhyAfe *pAfe = getAfe( ch );
        pAfe->setOSel(osel);
        pAfe->flushWCache();
        return ERR_NONE;
    }

    return ERR_INVALID_INPUT;
}

//! chan,k
int servCal::onqRmtLfRSel( CArgument &arg )
{
    if ( arg.size() < 2 ) return -1;

    Chan ch = (Chan)arg[0].iVal;
    int k = arg[1].iVal;

    return getLfRSel( ch, k );
}
int servCal::onqRmtLfCSel( CArgument &arg )
{
    if ( arg.size() < 2 ) return -1;

    Chan ch = (Chan)arg[0].iVal;
    int k = arg[1].iVal;

    return getLfCSel( ch, k );
}

DsoErr servCal::setLfTrim(
              Chan ch,
              int k,
              LfTrim trim,
              int val )
{
    if ( ch >= chan1 && ch <= chan4 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    int kIndex;
    kIndex = getKIndex( k );
    if ( kIndex < 0 )
    { return ERR_INVALID_INPUT; }

    mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mK = k;
    mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mTrims[ trim - dc_trim] = val;

    return ERR_NONE;
}
//! error -- -1
int servCal::getLfTrim(
               Chan ch,
               int k,
               LfTrim trim )
{
    if ( ch >= chan1 && ch <= chan4 )
    {}
    else
    { return -1; }

    int kIndex;
    kIndex = getKIndex( k );
    if ( kIndex < 0 )
    { return -1; }

    return mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mTrims[ trim - dc_trim];
}

DsoErr servCal::setLfRCSel( Chan ch, int k,
                   int rSel, int cSel )
{
    if ( ch >= chan1 && ch <= chan4 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    int kIndex;
    kIndex = getKIndex( k );
    if ( kIndex < 0 )
    { return ERR_INVALID_INPUT; }

    mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mK = k;
    mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mCSel = cSel;
    mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mRSel = rSel;

    return ERR_NONE;
}
int servCal::getLfRSel( Chan ch, int k )
{
    if ( ch >= chan1 && ch <= chan4 )
    {}
    else
    { return -1; }

    int kIndex;
    kIndex = getKIndex( k );
    if ( kIndex < 0 )
    { return -1; }

    return mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mRSel;
}
int servCal::getLfCSel( Chan ch, int k )
{
    if ( ch >= chan1 && ch <= chan4 )
    {}
    else
    { return -1; }

    int kIndex;
    kIndex = getKIndex( k );
    if ( kIndex < 0 )
    { return -1; }

    return mCalLf.mPayload.mChHizTrim[ ch - chan1 ].mLfTrims[ kIndex ].mCSel;
}

DsoErr servCal::lfSave()
{
    int size = sizeof( mCalLf.mPayload );
    if ( mCalLf.save( mCalFileDbs[1].mLF, &mCalLf.mPayload, size ) != size )
    {
        return ERR_CAL_WRITE_FAIL;
    }

    return ERR_NONE;
}
DsoErr servCal::lfLoad( const QString &fileName )
{
    //! new
    calLfData *pData;
    pData = new calLfData();
    if ( NULL == pData )
    { return ERR_CAL_READ_FAIL; }

    //! read
    int size = sizeof(pData->mPayload);
    if ( pData->load( fileName, &pData->mPayload, size ) != size )
    {
        delete pData;
        return ERR_CAL_READ_FAIL;
    }

    //! load data
    mCalLf = *pData;
    delete pData;

    return ERR_NONE;
}
