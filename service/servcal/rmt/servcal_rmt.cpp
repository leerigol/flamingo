
#include "../servcal.h"

DsoErr servCal::onSave( calFile file )
{
    switch( file )
    {
        case cal_file_gain_gnd:
            return vertSave();

        case cal_file_lf:
            return lfSave();

        case cal_file_ch_delay:
            return chDelaySave();

        default:
            return ERR_INVALID_INPUT;
    }
}
DsoErr servCal::onLoad( calFile file )
{
    switch( file )
    {
        case cal_file_gain_gnd:
            return vertLoad( mCalFileDbs[1].mVert );

        case cal_file_lf:
            return lfLoad( mCalFileDbs[1].mLF );

        case cal_file_ch_delay:
            return chDelayLoad( mCalFileDbs[1].mCHDelay );

        default:
            return ERR_INVALID_INPUT;
    }
}
