
#include "../servcal.h"

#define check_ch( ) if ( ch >= chan1 && ch <= chan4 ) \
                    {}\
                    else\
                    { return ERR_INVALID_INPUT; }

#define check_ch_ret( result_type ) if ( ch >= chan1 && ch <= chan4 ) \
                    {}\
                    else\
                    { return result_type; }

#define invalid_dso_real     DsoReal(99,E_P36 )

DsoErr servCal::onRmtEnter( Chan ch )
{
    check_ch();

    IPhyAfe *pAfe;
    pAfe = getAfe( ch );

    //! hg
    pAfe->setK( 0 );
    pAfe->setAtten( 0 );
    pAfe->setHzSga1Gs( 0 );

    pAfe->setGtrim4(0,15);
    pAfe->setGtrim5(0,15);

    //! lz
    pAfe->setGtrim1(0,1);
    pAfe->setGtrim2(0,3);
    pAfe->setGtrim3(0,3);

    //! dc
    pAfe->setGpo0( 1 );

    //! 1M
    pAfe->setOlEn( 0 );
    pAfe->setVosExp( 1 );

    //! all power on
    pAfe->setLzNpd( 0 );
    pAfe->setHzNpd( 1 );
    pAfe->setLpp( 0 );

    //! flush all
    pAfe->flushWCache();

    //! status
    mStatus = cal_deactive;

    syncEngine(ENGINE_ENTER_CAL, true);

    //qDebug() << "enter cal:" << QTime::currentTime();
    return ERR_NONE;
}
DsoErr servCal::onRmtExit( Chan ch )
{
    check_ch();

    mStatus = cal_deactive;

    syncEngine(ENGINE_ENTER_CAL, false);

    //qDebug() << "exit cal:" << QTime::currentTime();
    return ERR_NONE;
}

int servCal::onqRmtCalStatus( Chan /*ch*/ )
{
    return mStatus;
}

DsoErr servCal::onRmtAfePath( CArgument &arg )
{
    Q_ASSERT( arg.size() == 2 );

    Chan ch;
    CalPath path;

    ch = (Chan)arg[0].iVal;
    path = (CalPath)arg[1].iVal;

    check_ch();

    if ( path == cal_path_50_x2 )
    {
        #ifdef DAMREY_VER_1_5_x
        setSpuInvert(false);
        #endif
        select50_2( ch );
    }
    else if ( path == cal_path_50_x20 )
    {
        #ifdef DAMREY_VER_1_5_x
        setSpuInvert(false);
        #endif
        select50_20( ch );
    }
    else if ( path == cal_path_1m_x1 )
    {
        #ifdef DAMREY_VER_1_5_x
        setSpuInvert(true);
        #endif
        select1M_1( ch );
    }
    else if ( path == cal_path_1m_x7 )
    {
        #ifdef DAMREY_VER_1_5_x
        setSpuInvert(true);
        #endif
        select1M_7( ch );
    }
    else
    { return ERR_INVALID_INPUT; }

    return ERR_NONE;
}

DsoErr servCal::onRmtAfeK( CArgument &arg )
{
    Q_ASSERT( arg.size() == 2 );

    Chan ch;
    int k;

    ch = (Chan)arg[0].iVal;
    k = arg[1].iVal;

    check_ch();

    IPhyAfe *pAfe;
    pAfe = getAfe( ch );

    switch( k )
    {
        case 0: case 15: case 16: case 23: case 24:
            pAfe->setHzSga1Gs( 0 );
        break;

        case 28: case 29: case 30:
            pAfe->setHzSga1Gs( 1 );
        break;

        default:
            pAfe->setHzSga1Gs( 0 );
        break;
    }

    pAfe->setK( k );
    pAfe->flushWCache();

    return ERR_NONE;
}
int servCal::onqRmtAfeK( Chan ch )
{
    check_ch_ret( -1 );

    IPhyAfe *pAfe;
    pAfe = getAfe( ch );

    return pAfe->rs8.k;
}

DsoErr servCal::onRmtAfeKx( CArgument &arg )
{
    Q_ASSERT( arg.size() == 3 );

    Chan ch;
    int k;
    DsoReal real;

    ch = (Chan)arg[0].iVal;
    k = arg[1].iVal;
    real = arg[2].realVal;

    check_ch();

    //! check k
    int kIndex = getKIndex( k );
    if ( kIndex < 0 )
    { return ERR_INVALID_INPUT; }

    //! save k
    float val;
    if ( ERR_NONE != real.toReal(val) )
    { return ERR_INVALID_INPUT; }

    mCalPara.mPayload.mCHs[ ch-chan1 ]._hzKs[kIndex].mKey = k;
    mCalPara.mPayload.mCHs[ ch-chan1 ]._hzKs[kIndex].mVal = val;

    return ERR_NONE;
}
DsoReal servCal::onqRmtAfeKx( CArgument &arg )
{
    Q_ASSERT( arg.size() == 2 );

    Chan ch;
    int k;

    ch = (Chan)arg[0].iVal;
    k = arg[1].iVal;

    check_ch_ret( invalid_dso_real );

    int kIndex = getKIndex( k );
    if ( kIndex < 0 )
    { return invalid_dso_real; }

    //! get cal value
    return DsoReal ( mCalPara.mPayload.mCHs[ ch-chan1 ]._hzKs[ kIndex ].mVal );
}

DsoErr servCal::onRmtAfeZero( Chan ch )
{
    check_ch();
    DsoErr err;

    mStatus = cal_active;

    int dac;
    float valOut;
    err = iterDac( 0, ch - chan1,
                   ch - chan1,
                 ADC_MID,
                 dac,
                 valOut );
    if ( err != ERR_NONE )
    {
        mStatus = cal_aborted;
        qDebug() << "error of zero cal";
        return ERR_CAL_OFFSET_FAIL;
    }

    //! \todo check the range
    if ( abs(valOut - ADC_MID) > 10 )
    {
        mStatus = cal_aborted;
        qDebug() << "error of zero cal:" << valOut;
        return ERR_CAL_OFFSET_FAIL;
    }

    mRmtCalDac = dac;

    mStatus = cal_completed;

    return ERR_NONE;
}
int servCal::onqRmtAfeZero( Chan /*ch*/ )
{
    return mRmtCalDac;
}

//! chan, path, lsb
DsoErr servCal::onRmtAfeLsb( CArgument &arg )
{
    Q_ASSERT( arg.size() == 3 );

    Chan ch;
    CalPath path;
    DsoReal real;

    ch = (Chan)arg[0].iVal;
    path = (CalPath)arg[1].iVal;
    real = arg[2].realVal;

    check_ch();

    float value;

    if ( ERR_NONE != real.toReal(value) )
    { return ERR_INVALID_INPUT; }

    if ( cal_path_50_x2 == path )
    {
        mCalPara.mPayload.mCHs[ ch-chan1].mLsb50 = value*lsb_base_scale;
    }
    else if ( cal_path_50_x20 == path )
    {
        mCalPara.mPayload.mCHs[ ch-chan1].mLsb50_att = value*lsb_base_scale;
    }
    else if ( cal_path_1m_x1 == path )
    {
        mCalPara.mPayload.mCHs[ ch-chan1].mLsb1M = value*lsb_base_scale;
    }
    else if ( cal_path_1m_x7 == path )
    {
        mCalPara.mPayload.mCHs[ ch-chan1].mLsb1M_X = value*lsb_base_scale;
    }
    else
    { return ERR_INVALID_INPUT; }

    return ERR_NONE;
}
DsoReal servCal::onqRmtAfeLsb( CArgument &arg )
{
    Q_ASSERT( arg.size() ==2 );

    Chan ch;
    CalPath path;

    ch = (Chan)arg[0].iVal;
    path = (CalPath)arg[1].iVal;

    check_ch_ret( invalid_dso_real );

    float value;

    if ( cal_path_50_x2 == path )
    {
        value = mCalPara.mPayload.mCHs[ ch-chan1].mLsb50;
    }
    else if ( cal_path_50_x20 == path )
    {
        value = mCalPara.mPayload.mCHs[ ch-chan1].mLsb50_att;
    }
    else if ( cal_path_1m_x1 == path )
    {
        value = mCalPara.mPayload.mCHs[ ch-chan1].mLsb1M;
    }
    else if ( cal_path_1m_x7 == path )
    {
        value = mCalPara.mPayload.mCHs[ ch-chan1].mLsb1M_X;
    }
    else
    { return invalid_dso_real; }

    return DsoReal( (float)(value*lsb_base) );
}

//! ch, reg addr, reg value
DsoErr servCal::onRmtAfeReg( CArgument &arg )
{
    Q_ASSERT( arg.size() == 3 );

    Chan ch;
    int addr, val;

    ch = (Chan)arg[0].iVal;
    addr = arg[1].iVal;
    val = arg[2].iVal;

    check_ch();

    if ( addr >= 0 && addr <= 14 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    if ( val >=0 && val <= __UINT16_MAX__ )
    {}
    else
    { return ERR_INVALID_INPUT; }

    IPhyAfe *pAfe = getAfe( ch );

    pAfe->appendWCache( addr, &val );

    pAfe->flushWCache();

    return ERR_NONE;
}
int servCal::onqRmtAfeReg( CArgument &arg )
{
    Q_ASSERT( arg.size() == 2 );

    Chan ch;
    int addr;

    ch = (Chan)arg[0].iVal;
    addr = arg[1].iVal;

    check_ch();

    if ( addr == 0 || addr == 11 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    IPhyAfe *pAfe = getAfe( ch );
    damrey_reg reg;
    pAfe->readReg( reg, addr );

    return reg;
}

//! slot, dac
DsoErr servCal::onRmtDac( CArgument &arg )
{
    Q_ASSERT( 2 == arg.size() );

    int slot, val;

    slot = arg[0].iVal;
    val = arg[1].iVal;

    if ( slot >= 0 && slot < 15 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    if ( val >= 0 && val <= __UINT16_MAX__ )
    {}
    else
    { return ERR_INVALID_INPUT; }

    dac_reg dac = val;
    m_pDsoPlatform->m_pPhy->mDac.set( dac, slot );

    return ERR_NONE;
}
