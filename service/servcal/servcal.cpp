
#include "servcal.h"
#include "calthread.h"

#include "../service_msg.h"

IMPLEMENT_CMD( serviceExecutor, servCal )
start_of_entry()

on_set_int_void( servCal::CMD_CAL_START, &servCal::onCalStart ),

on_set_int_void( MSG_SELF_CAL_START, &servCal::onCalStart ),
on_set_int_void( MSG_SELF_CAL_QUIT, &servCal::onCalStop ),

on_set_int_int( MSG_CAL_ITEMS, &servCal::setCalItems ),
on_get_int( MSG_CAL_ITEMS, &servCal::getCalItems ),

on_set_int_int( MSG_CAL_LOAD_USER, &servCal::onCalUser ),
on_set_int_int( MSG_CAL_LOAD_DEFAULT, &servCal::onCalDefault ),
on_set_int_int( MSG_CAL_EXPORT_A, &servCal::onExportUser ),

on_set_int_bool( MSG_SELF_CAL_WINDOW, &servCal::setWindow),
on_get_bool( MSG_SELF_CAL_WINDOW, &servCal::getWindow),

on_set_int_void( MSG_SELF_CAL_ADC, &servCal::test_calPhase ),

on_set_int_bool( MSG_APP_UTILITY_PROJECT, &servCal::on_project_changed ),
on_get_bool    ( MSG_APP_UTILITY_PROJECT, &servCal::isProjectMode),

on_set_int_pointer( servCal::CMD_PHASE_REQUEST, &servCal::on_ack_phase_request ),

on_get_int( servCal::qCMD_CAL_PROGRESS_NOW, &servCal::getProgNow ),
on_get_int( servCal::qCMD_CAL_PROGRESS_TOTAL, &servCal::getProgTotal ),
on_get_int( servCal::qCMD_CAL_STATUS, &servCal::getStatus ),
on_get_ll ( servCal::qCMD_CAL_DATE, &servCal::getCalDate ),

on_set_void_void( servCal::CMD_CAL_UPDATED, &servCal::onCalUpdated ),

on_set_int_string( servCal::CMD_CAL_DETAIL, &servCal::onCalDetail ),
on_get_pointer( servCal::qCMD_CAL_DETAIL, &servCal::getCalDetail ),

//! remote
on_set_int_int( servCal::CMD_RMT_ENTER, &servCal::onRmtEnter ),
on_set_int_int( servCal::CMD_RMT_EXIT, &servCal::onRmtExit ),

on_get_int_int( servCal::qCMD_RMT_STATUS, &servCal::onqRmtCalStatus),

on_set_int_arg( servCal::CMD_RMT_AFE_PATH, &servCal::onRmtAfePath ),

on_set_int_arg( servCal::CMD_RMT_AFE_K, &servCal::onRmtAfeK ),
on_get_int_int( servCal::qCMD_RMT_AFE_K, &servCal::onqRmtAfeK ),

on_set_int_arg( servCal::CMD_RMT_AFE_KX, &servCal::onRmtAfeKx ),
on_get_dsoreal_arg( servCal::qCMD_RMT_AFE_KX, &servCal::onqRmtAfeKx ),

on_set_int_int( servCal::CMD_RMT_AFE_ZERO, &servCal::onRmtAfeZero ),
on_get_int_int( servCal::qCMD_RMT_AFE_ZERO, &servCal::onqRmtAfeZero ),

on_set_int_arg( servCal::CMD_RMT_AFE_LSB, &servCal::onRmtAfeLsb ),
on_get_dsoreal_arg( servCal::qCMD_RMT_AFE_LSB, &servCal::onqRmtAfeLsb ),

on_set_int_arg( servCal::CMD_RMT_AFE_REG, &servCal::onRmtAfeReg ),
on_get_int_arg( servCal::qCMD_RMT_AFE_REG, &servCal::onqRmtAfeReg ),

on_set_int_arg( servCal::CMD_RMT_DAC, &servCal::onRmtDac ),

//! lf
on_set_int_arg( servCal::CMD_RMT_AFE_1M_PATH, &servCal::onRmtAfe1M ),
on_get_int_int( servCal::CMD_RMT_AFE_VPP, &servCal::onRmtVpp ),
on_get_int_int( servCal::CMD_RMT_AFE_AVG, &servCal::onRmtAvg ),
on_get_dsoreal_int( servCal::CMD_RMT_AFE_AVG1, &servCal::onRmtAvg1 ),

on_get_dsoreal_int( servCal::CMD_RMT_AFE_FPP, &servCal::onRmtFpp ),
on_set_int_arg( servCal::CMD_RMT_AFE_TRIM, &servCal::onRmtLfTrim ),

on_get_int_arg( servCal::CMD_RMT_AFE_TRIM, &servCal::onqRmtLfTrim ),

on_set_int_arg( servCal::CMD_RMT_AFE_OSEL, &servCal::onRmtAfeOsel ),
on_set_int_arg( servCal::CMD_RMT_AFE_DSEL, &servCal::onRmtAfeDsel ),

on_set_int_arg( servCal::CMD_RMT_AFE_RCSEL, &servCal::onRmtLfRCSel ),
on_get_int_arg( servCal::qCMD_RMT_AFE_RSEL, &servCal::onqRmtLfRSel ),
on_get_int_arg( servCal::qCMD_RMT_AFE_CSEL, &servCal::onqRmtLfCSel ),

on_set_int_int( servCal::CMD_RMT_AFE_BEGIN_HIZAMP, &servCal::onAzVosEnter ),
on_set_int_int( servCal::CMD_RMT_AFE_STOP_HIZAMP,  &servCal::onAzVosExit ),
on_set_int_arg( servCal::CMD_RMT_AFE_AZVOS,        &servCal::onRmtAzVos ),


//! save
on_set_int_int( servCal::CMD_SAVE, &servCal::onSave ),
on_set_int_int( servCal::CMD_LOAD, &servCal::onLoad ),

//! data line
on_set_int_void( servCal::CMD_CAL_DATA_LINE, &servCal::onCalDataLine ),
on_get_bool( servCal::CMD_CAL_DATA_LINE,     &servCal::getCalDataLine ),

//! pll
on_set_int_bool( servCal::CMD_CAL_PLL5_OUTPUT, &servCal::onPll5Output ),
on_set_int_arg( servCal::CMD_CAL_PLL5_REG, &servCal::onPll5Reg ),
on_set_int_arg( servCal::CMD_CAL_PLL5_CFG, &servCal::onPll5Cfg ),
on_get_int_arg( servCal::CMD_CAL_PLL5_CFG, &servCal::qPll5Cfg ),

on_set_int_bool( servCal::CMD_CAL_PLL1_1_OUTPUT, &servCal::onPll1_1Output ),
on_set_int_arg( servCal::CMD_CAL_PLL1_1_REG, &servCal::onPll1_1Reg ),
on_set_int_arg( servCal::CMD_CAL_PLL1_1_CFG, &servCal::onPll1_1Cfg ),
on_get_int_arg( servCal::CMD_CAL_PLL1_1_CFG, &servCal::qPll1_1Cfg ),

//!scpi
on_set_int_void( servCal::CMD_CAL_SELF, &servCal::onCalSelf ),

//!probe
on_set_void_int( servCal::CMD_CAL_PROBE,        &servCal::enterCalProbe ),
on_set_void_int( servCal::CMD_APPLY_PROBE_DATA, &servCal::applyProbeCalData ),

//!get ch delay cal ptr for serv ch
on_get_pointer( servCal::qCMD_CH_DELAY_PTR,     &servCal::getChDalayData),

//! test
on_set_int_int( servCal::CMD_SNAP_CH_AVG, &servCal::snapCHAvg ),

on_set_int_void( servCal::CMD_SELF_START, &servCal::onSelfStart),

//! system
on_set_void_void( CMD_SERVICE_STARTUP, &servCal::startup ),

on_set_void_void( CMD_SERVICE_INIT, &servCal::init ),


/************SCPI*************/
on_get_string(  servCal::CMD_CAL_DATE,  &servCal::getLastCalDate ),
on_get_string(  servCal::CMD_CAL_TIME,  &servCal::getLastCalTime ),

on_get_string(  servCal::CMD_CAL_RESULT,  &servCal::getResult ),
on_get_bool(    servCal::CMD_BOOL_RESULT, &servCal::getBoolResult ),
on_get_bool  (  CMD_SERVICE_ACTIVE,      &servCal::isActive),

end_of_entry()

#include "./data/servcal_data.cpp"

calFileDb::calFileDb()
{}

bool servCal::m_bPreparing = false;

#define proc_file_name( fileName )  { mFileList.append(fileName); fileName = mPath + fileName; }
#define get_file_name( subPath, name ) meta.getMetaVal( path + subPath, name ); proc_file_name(name);
void calFileDb::load( const r_meta::CMeta &meta,  const QString &path )
{
    meta.getMetaVal( path + "path", mPath );

    //! attach path
    meta.getMetaVal( path + "vcal", mVert );
    get_file_name( "vcal", mVert );

    get_file_name( "ical", mLineDelay[0] );//出刺

    get_file_name( "lfcal", mLF );

    get_file_name( "adcgocal0", mAdcGo[0] );
    get_file_name( "adcgocal1", mAdcGo[1] );

    get_file_name( "extcal", mExt );

    get_file_name( "lal", mLa[0] );
    get_file_name( "lah", mLa[1] );

    get_file_name( "smp0", mAdcSamp[0] );//变粗， 单通道下fft谐波多
    get_file_name( "smp1", mAdcSamp[1] );

    get_file_name( "pll5g", mPll5g );
    get_file_name( "pll1_1g", mPll1_1g );

    get_file_name( "ch_delay", mCHDelay);
}

servCal::servCal( QString name, ServiceId id ) :
    serviceExecutor(name,id)
{
    serviceExecutor::baseInit();

    m_pWorker = NULL;

    mProgNow = 0;
    mProgTotal = 100;

    mStatus = cal_deactive;
    mDataLineStatus = cal_deactive;

    m_pDsoPlatform = NULL;

    mCalItems = 0;
    for ( int i = cal_item_base; i <= cal_item_top; i++ )
    {
        set_bit( mCalItems, i);
    }
    mCalFullItems = mCalItems;

    m_calType  = cal_self;
    m_probeCh  = chan1;
    m_bWindow  = true;
}
servCal::~servCal()
{
    if ( NULL != m_pWorker )
    {
        delete m_pWorker;
    }
}

void servCal::registerSpy()
{
    spyOn( serv_name_utility,
           MSG_APP_UTILITY_PROJECT );
}

///
/// \brief Disabled the MenuBack if progres is ongoing
/// \param key
/// \return
///
bool servCal::keyEat(int key, int /*count*/, bool /*bRelease*/ )
{
    if ( !isActive() )
    {
        return false;
    }
    if ( mStatus == cal_running &&
         key != R_Pkey_PageOff &&
         key != R_Pkey_F2 &&
         key != R_Pkey_F3 )
    {
        servGui::showErr( ERR_ACTION_DISABLED );
        return true;
    }

    return false;
}

DsoErr servCal::start()
{
    m_bProject   = false;
    m_bPreparing = false;
    return ERR_NONE;
}

//! do only once
void servCal::init()
{

    if( !sysCheckLicense(OPT_MSO))
    {
        mUiAttr.setVisible(MSG_CAL_ITEMS, cal_la, false);
    }

    //! get the platform
    m_pDsoPlatform = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != m_pDsoPlatform );

    //! load cal file db
    r_meta::CServMeta servMeta;
    mCalFileDbs[0].load( servMeta, QStringLiteral("servcal/default/"));
    mCalFileDbs[1].load( servMeta, QStringLiteral("servcal/user/"));

    //! load from ram
    loadInitData();

    //! load cal data
    loadCalData( mCalFileDbs[1], mCalFileDbs[0] );

    //! apply data
    applyCalData( false );

    on_project_changed(false);
}

void servCal::on_finished()
{
    setMenuEn(false);
}

void servCal::logInfo( const QString &str, int /*level*/ )
{
    async( servCal::CMD_CAL_DETAIL, str );
}
void servCal::logInfo( const QString &str1, const QString &str2 )
{
    //#ifdef _DEBUG
        QString str = str1 + "/" + str2;
        async( servCal::CMD_CAL_DETAIL, str );
    //#endif
}
void servCal::startup()
{

}


DsoErr servCal::onCalStart()
{
    if ( m_pWorker == NULL )
    {
        m_pWorker = new calThread( this );
        Q_ASSERT( NULL != m_pWorker );

        connect( m_pWorker, SIGNAL(finished()),
                 this, SLOT(on_finished()) );
    }


    setCalType(cal_self);    
    setMenuEn( true );
    installActionFilter();

    m_pWorker->start();
    return ERR_NONE;
}

DsoErr servCal::onCalStop()
{
    Q_ASSERT( m_pWorker != NULL );

    if ( m_pWorker->isRunning() )
    {
        m_pWorker->terminate();
        m_pWorker->wait( 1500 );
    }


    //! end
    setProgNow( 0 );
    setStatus( cal_aborted );

    uninstallActionFilter();

    postCal();

    logInfo( "aborted" );      
    on_finished();

    return ERR_NONE;
}

DsoErr servCal::setWindow(bool b)
{
    m_bWindow = b;
    return ERR_NONE;
}

bool servCal::getWindow()
{
    return m_bWindow;
}

DsoErr servCal::setCalItems( qint32 items )
{
    quint32 changedItem;

    changedItem = items ^ mCalItems;

    //! first bit changed
    if ( get_bit( changedItem, cal_item_all) )
    {
        if ( get_bit( items, cal_item_all) )
        {
            mCalItems = mCalFullItems;
        }
        else
        {
            mCalItems = 0;
        }
    }
    //! other bit
    else
    {
        mCalItems = items;

        //! full
        if ( (items >> 1) == (mCalFullItems>>1) )
        {
            set_bit( mCalItems, cal_item_all );
        }
        else
        {
            unset_bit( mCalItems, cal_item_all );
        }
    }

    return ERR_NONE;
}
qint32 servCal::getCalItems()
{
    return mCalItems;
}

DsoErr servCal::onCalDefault()
{
    DsoErr err;

    //! load cal data
    err = loadCalData( mCalFileDbs[0], mCalFileDbs[0] );

    //! apply data
    if ( err == ERR_NONE )
    {
        applyCalData();
        return ERR_CAL_LOAD_SUCCESS;
    }

    return ERR_CAL_LOAD_FAIL;
}

DsoErr servCal::onCalUser()
{
    DsoErr err;

    //! load cal data
    err = loadCalData( mCalFileDbs[1], mCalFileDbs[0] );

    //! apply data
    if ( err == ERR_NONE )
    {
        applyCalData();
        return ERR_CAL_LOAD_SUCCESS;
    }

    return ERR_CAL_LOAD_FAIL;
}

// true-running
// false not running
void servCal::setMenuEn(bool b)
{
    mUiAttr.setEnable( MSG_SELF_CAL_START, !b );
    mUiAttr.setEnable( MSG_SELF_CAL_QUIT, b);
    if( m_bProject )
    {
        mUiAttr.setEnable( MSG_CAL_ITEMS,!b);
        mUiAttr.setEnable( MSG_CAL_LOAD_USER,!b);
        mUiAttr.setEnable( MSG_CAL_LOAD_DEFAULT,!b);
        mUiAttr.setEnable( MSG_CAL_EXPORT_A, !b);
    }
}

DsoErr servCal::on_project_changed( bool b )
{
    m_bProject = b;
    mUiAttr.setVisible( MSG_CAL_ITEMS, b );
    mUiAttr.setVisible( MSG_CAL_LOAD_USER, b );
    mUiAttr.setVisible( MSG_CAL_LOAD_DEFAULT, b );
    mUiAttr.setVisible( MSG_CAL_EXPORT_A, b);
    return ERR_NONE;
}

bool servCal::isProjectMode()
{
    return m_bProject;
}

void servCal::setProgNow( int now )
{
    if ( now <= mProgTotal )
    {
        mProgNow = now;
        defer( CMD_CAL_UPDATED );
    }
    else return;
}
int servCal::getProgNow()
{
    return mProgNow;
}

void servCal::setProgTotal( int total )
{
    mProgTotal = total;
}

int servCal::getProgTotal()
{
    return mProgTotal;
}

void servCal::setStatus( servCal::CalStatus stat )
{
    mStatus = stat;

    defer( CMD_CAL_UPDATED );
}

servCal::CalStatus servCal::getStatus()
{
    return mStatus;
}

qint64 servCal::getCalDate()
{    
    return mCalPara.getEpoch();
}

DsoErr servCal::setCalDate( qint64 dt)
{
    mCalPara.setEpoch(dt);
    return ERR_NONE;
}

QString servCal::getLastCalDate()
{
    QDateTime dt;
    dt.setMSecsSinceEpoch( mCalPara.getEpoch() );
    return dt.toString("yyyy,MM,dd");
}

QString servCal::getLastCalTime()
{
    QDateTime dt;
    dt.setMSecsSinceEpoch( mCalPara.getEpoch() );
    return dt.toString("hh,mm,ss");
}

QString servCal::getResult()
{
    int step = m_nResult;
    if ( step == cal_idelay )
    {
        return "Data line";
    }
    else if (step == cal_vert_ch1)
    {
        return "CH1";
    }
    else if( step == cal_vert_ch2)
    {
        return "CH2";
    }
    else if( step == cal_vert_ch3)
    {
        return "CH3";
    }
    else if( step == cal_vert_ch4 )
    {
        return "CH4";
    }
    else if( step == cal_adc_go)
    {
        return "Vert core";
    }
    else if( step == cal_adc_phase)
    {
        return "Phase";
    }
    else if( step == cal_la)
    {
        return "LA";
    }
    else if( step == cal_ext)
    {
        return "ext";
    }
    return CAL_SUCCESS;
}

bool servCal::getBoolResult()
{
    //QString result =  CAL_SUCCESS;
    if( cal_completed == mStatus )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void servCal::onCalUpdated()
{
    switch( mStatus )
    {
        case servCal::cal_running:
            mUiAttr.setEnable( MSG_SELF_CAL_QUIT, true );
            break;

        case servCal::cal_completed:
            setMenuEn(false);
            uninstallActionFilter();

            /* save current time when success */
            if( getBoolResult() )
            {
                setCalDate( QDateTime::currentMSecsSinceEpoch());
            }
            break;

        default:
            break;
    }
}

DsoErr servCal::onCalDetail( const QString &str )
{
    mLogger.append( str );

    return ERR_NONE;
}
void* servCal::getCalDetail()
{
    mViewLogger = mLogger;
    mLogger.clear();

    return &mViewLogger;
}

DsoErr servCal::onCalSelf()
{
    post(serv_name_cal,CMD_SERVICE_ACTIVE,1);
    post(serv_name_cal,servCal::CMD_CAL_START,1);

    return ERR_NONE;
}

DsoErr servCal::onCalDataLine()
{
    preCal();

    mStatus = cal_deactive;

    DsoErr err = horiCalProc(0,0);
    if ( err != ERR_NONE )
    {
        mStatus = cal_aborted;
        mDataLineStatus = cal_aborted;
    }
    else
    {
        mStatus = cal_completed;
        mDataLineStatus = cal_completed;
    }

    postCal();

    return ERR_NONE;
}

bool servCal::getCalDataLine()
{
    return (cal_completed == mDataLineStatus);
}

int servCal::getKIndex( int k )
{
    for ( int i = 0; _HizK[i].x != 0 ; i++ )
    {
        if ( _HizK[i].k == k )
        { return i; }
    }

    return -1;
}
