
#include <QDateTime>
#include "servcal.h"

#include "../servtrace/servtrace.h"
#include "../servutility/servutility.h"
#include "../servstorage/servstorage.h"

#undef _DEBUG
#define step_from       ( progFrom + _stepFrom )
#define step_s( n )     (  _stepFrom = _stepFrom + n*steps/100 )


#define wpu_gain    (2457)
#define wpu_offset  (-67)
void servCal::preCal()
{
    logInfo("start");

    m_bPreparing = true;

    serviceExecutor::post( E_SERVICE_ID_STORAGE,
                           servStorage::FUNC_RESTORE_DEFAULT,
                           0 );

    //waiting for default
    while(1)
    {
        QThread::msleep(1000);
        if( !servStorage::isDefaulting() )
        {
            break;
        }
    }
    QThread::msleep(3000);
    async(CMD_SERVICE_ACTIVE,1);
    async(CMD_SELF_START,0);



    dso_phy::CDsoPhy* m_pPhy = m_pDsoPlatform->m_pPhy;
    if( m_pPhy )
    {
        unsigned short irq_mask = m_pPhy->mBoard.readIntEn() & 0xfff0;
        for(int id=0; id<4; id++)
        {
            m_pPhy->m_ch[id].setImpedance( IMP_1M );
        }
        m_pPhy->mBoard.maskInt( irq_mask) ;
        m_pPhy->mBoard.clrInt();
    }

    //! wpu
    //! 200~480
    getWpu()->setanalog_ch0_gain( wpu_gain );
    getWpu()->setanalog_ch0_offset( wpu_offset );

    getWpu()->setanalog_ch1_gain( wpu_gain );
    getWpu()->setanalog_ch1_offset( wpu_offset );

    getWpu()->setanalog_ch2_gain( wpu_gain );
    getWpu()->setanalog_ch2_offset( wpu_offset );

    getWpu()->setanalog_ch3_gain( wpu_gain );
    getWpu()->setanalog_ch3_offset( wpu_offset );

    getWpu()->flushWCache();

    //! adc
    //! gain offset to 512
    getAdc()->setGain( 1, 511 );
    getAdc()->setGain( 2, 511 );
    getAdc()->setGain( 3, 511 );
    getAdc()->setGain( 4, 511 );

//    getAdc()->setGain( 1, 0 );
//    getAdc()->setGain( 2, 0 );
//    getAdc()->setGain( 3, 0 );
//    getAdc()->setGain( 4, 0 );

//    getAdc()->setGain( 1, 1023 );
//    getAdc()->setGain( 2, 1023 );
//    getAdc()->setGain( 3, 1023 );
//    getAdc()->setGain( 4, 1023 );

    getAdc()->setOffset( 1, 511 );
    getAdc()->setOffset( 2, 511 );
    getAdc()->setOffset( 3, 511 );
    getAdc()->setOffset( 4, 511 );


    calPhaseReset();

    //! trig
    //! use auto trig
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan1, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan2, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan3, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_A_ADC, chan4, 0, 255 );

    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan1, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan2, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan3, 0, 255 );
    postEngine( ENGINE_TRIGGER_LEVEL_B_ADC, chan4, 0, 255 );

    getCcu()->setAUTO_TRIG_TIMEOUT( 0 );

    //stopTrace();
    mLogger.clear();

    serviceExecutor::send(serv_name_utility,
                          servUtility::cmd_lock_input,
                          true);

    QList<int> cmds;
    cmds << MSG_SELF_CAL_START;
    cmds << MSG_SELF_CAL_QUIT;
    cmds << servCal::CMD_BOOL_RESULT;
    sysScpiAddMap(this->getName(), cmds);
    sysScpiExecuteEn(false);
}

DsoErr servCal::calProc( int progFrom, int steps )
{
    DsoErr err;
    int stepFrom = 0;

    //! 1. cal the idelay
    if ( get_bit(mCalItems, cal_idelay ) )
    {
        m_nResult = cal_idelay;
        stepFrom = 10*steps/100;
        for(int i=0; i<3; i++)
        {            
            err = horiCalProc(progFrom + i*stepFrom/3,stepFrom/3 );
            if( err == ERR_NONE )
            {
                break;
            }
        }

        if ( err != ERR_NONE )
        {
            logInfo("fail IDELAY");
            return ERR_CAL_HORI;
        }
        progFrom = progFrom + stepFrom;
    }

    //! 2. cal the adc gain
    if ( get_bit(mCalItems, cal_adc_go ) )
    {
        m_nResult = cal_adc_go;
        stepFrom = 10*steps/100;
        for(int i=0; i<3; i++ )
        {
            err = calAdcProc( progFrom + i*stepFrom/3 , stepFrom/3 );
            if( err == ERR_NONE )
            {
                break;
            }
        }
        if ( err != ERR_NONE )
        {
            logInfo("fail ADC");
            return ERR_CAL_ADC;
        }

        progFrom = progFrom + stepFrom;
    }

    //! 2. phase cal
    if ( get_bit(mCalItems, cal_adc_phase ) )
    {
        m_nResult = cal_adc_phase;
        stepFrom = 10*steps/100;
        for(int i=0; i<3; i++ )
        {
            err = calPhaseProc( progFrom + i*stepFrom/3 , stepFrom/3 );
            if( err == ERR_NONE )
            {
                break;
            }
        }

        if ( err != ERR_NONE )
        {
            logInfo("fail phase");
            return ERR_CAL_PHASE;
        }
        progFrom = progFrom + stepFrom;
    }

#if 1    
    //! 4. cal vert
    if ( !sysHasArg("-novcal") )
    {
        stepFrom = 50*steps/100;
        err = vertCalProc( progFrom , stepFrom );
        if ( err != ERR_NONE )
        {
            logInfo("fail VERT");
            return ERR_CAL_VERT;
        }

        progFrom = progFrom + stepFrom;
    }



    //! 5. la cal
    if ( get_bit(mCalItems, cal_la ) && sysCheckLicense(OPT_MSO))
    {
        m_nResult = cal_la;
        stepFrom =  10*steps/100;
        err = calLaProc( progFrom , stepFrom );
        if ( err != ERR_NONE )
        {
            logInfo("fail LA");
            return ERR_CAL_LA_THRESHOLD;
        }
        progFrom = progFrom + stepFrom;
    }

    //! 6. cal ext
    if ( get_bit(mCalItems, cal_ext ) )
    {
        m_nResult = cal_ext;
        stepFrom =  10*steps/100;
        err = calExtProc( progFrom , stepFrom );
        if ( err != ERR_NONE )
        {
            logInfo("fail EXT");
            return ERR_CAL_EXT;
        }
    }
#endif
    m_nResult = 0;
    return ERR_NONE;
}

void servCal::postCal()
{
    logInfo("end");

    //! Reset important paramaters
    for ( int id = E_SERVICE_ID_CH4; id >= E_SERVICE_ID_CH1; id-- )
    {
        serviceExecutor::post( id, CMD_SERVICE_RST, 0 );
    }

    for ( int id = E_SERVICE_ID_CH4; id >= E_SERVICE_ID_CH1; id-- )
    {
        serviceExecutor::post( id, CMD_SERVICE_STARTUP, 0 );
    }

    //! run trace
    startTrace();
    serviceExecutor::post(E_SERVICE_ID_UTILITY,
                          servUtility::cmd_lock_input,
                          false);

    sysScpiExecuteEn(true);
}


DsoErr servCal::onSelfStart()
{
    m_bWindow = true;
    //setActive();
    return ERR_NONE;
}

void servCal::startTrace()
{
    serviceExecutor::post( E_SERVICE_ID_TRACE,
                           servTrace::cmd_trace_run,
                           true );
}
void servCal::stopTrace()
{
    serviceExecutor::send( serv_name_trace,
                           servTrace::cmd_trace_run,
                           false );
}

void servCal::setAcqAvg(bool b)
{
    int acq = Acquire_Normal;
    if( b )
    {
        acq = Acquire_Average;
    }
    serviceExecutor::post( E_SERVICE_ID_HORI,
                           MSG_HOR_ACQ_MODE_YT,
                           acq );
    //! wait main loop idle
    QThread::sleep( 1 );
}
