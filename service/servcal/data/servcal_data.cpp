
#include "servcal.h"

//! k index
kAttenX servCal::_HizK[]=
{
    {0,1.0f},
    {15,0.75f},
    {16,0.25f},
    {23,0.1875f},

    {24,0.0625f},
    {27,0.0469f},
    {28,0.0156f},
    {29,0.009375f},

    {30,0.006113f},

    {0,0},      //! end
};

kAttenX servCal::_Dsa[]=
{
    {0,1.0f},
    {1,0.8f},
    {2,0.63f},
    {3,0.5f},

    {4,0.4f},
    {5,0.316f},

    {0,0},      //! end
};

void servCal::loadInitData()
{
    int j;
    chCal *pChCal;
    //! default
    for ( int i = 0; i < array_count(mCalPara.mPayload.mCHs); i++ )
    {
        pChCal = mCalPara.mPayload.mCHs + i;

        //! ------ common
        pChCal->mAzVosTrim =  1024;
        pChCal->mVosTrim2 =  4;
        pChCal->mVosTrim3 =  4;

        //! ------ 1M
        //! lsb
        pChCal->mLsb1M = LSB_1M_OHM;
        pChCal->mLsb1M_X = LSB_1M_OHM_X;

        //! ---- hz
        //! K
        j = 0;
        for( j = 0; servCal::_HizK[j].x != 0; j++ )
        {
            pChCal->_hzKs[j].mKey = servCal::_HizK[j].k;
            pChCal->_hzKs[j].mVal = servCal::_HizK[j].x;
        }
        pChCal->mhzKs = j;

        //! gs
        pChCal->mHzLgHg[0] = 1.0f;
        pChCal->mHzLgHg[1] = 6.66f;

        //! attex
        j = 0;
        for( j = 0; servCal::_Dsa[j].x != 0; j++ )
        {
            pChCal->_hzAtts[j].mKey = servCal::_Dsa[j].k;
            pChCal->_hzAtts[j].mVal = servCal::_Dsa[j].x;
        }
        pChCal->mhzAtts = j;

        //! sga4
        pChCal->mHzSga4Sel[0] = 1.0f;
        pChCal->mHzSga4Sel[1] = 1.25f;

        pChCal->mHzSga4Trim[0] = 0.925f;
        pChCal->mHzSga4Trim[1] = 1.0f;

        //! sga5
        pChCal->mHzSga5Sel[0] = 1.0f;
        pChCal->mHzSga5Sel[1] = 1.25f;

        pChCal->mHzSga5Trim[0] = 0.925f;
        pChCal->mHzSga5Trim[1] = 1.0f;

        //! ref scale
        pChCal->mHzRefScale = 0.024f/lsb_base;

        //! scales
        pChCal->mHzScales = 0;

        //! gnds
        pChCal->mHzGnds = 0;

        //! ---- loz
        //! lsb
        pChCal->mLsb50 = afe_50_lsb_1_2;
        pChCal->mLsb50_att = afe_50_lsb_1_20;

        //! sga1
        pChCal->mLzSga1[0] = 1.0f;
        pChCal->mLzSga1[1] = 2.6f;

        pChCal->mLzSga1_att[0] = 1.0f;
        pChCal->mLzSga1_att[1] = 2.6f;

        pChCal->mLzSga1Trim[0] = 0.79f;
        pChCal->mLzSga1Trim[1] = 1.0f;

        //! sga2
        pChCal->mLzSga2[0] = 1.0f;
        pChCal->mLzSga2[1] = 1.7f;
        pChCal->mLzSga2[2] = 3.0f;

        pChCal->mLzSga2Trim[0] = 0.8f;
        pChCal->mLzSga2Trim[1] = 0.85f;
        pChCal->mLzSga2Trim[2] = 0.92f;
        pChCal->mLzSga2Trim[3] = 1.0f;

        //! sga3
        pChCal->mLzSga3[0] = 1.0f;
        pChCal->mLzSga3[1] = 2.5f;

        pChCal->mLzSga3Trim[0] = 0.8f;
        pChCal->mLzSga3Trim[1] = 0.85f;
        pChCal->mLzSga3Trim[2] = 0.92f;
        pChCal->mLzSga3Trim[3] = 1.0f;

        //! sga4
        pChCal->mLzSga4[0] = 1.0f;
        pChCal->mLzSga4[1] = 1.25f;

        //! sga5
        pChCal->mLzSga5[0] = 1.0f;
        pChCal->mLzSga5[1] = 1.25f;

        //! flt sga4
        pChCal->mLzSga4Flt[0] = 1.0f;
        pChCal->mLzSga4Flt[1] = 1.25f;

        //! flt sga5
        pChCal->mLzSga5Flt[0] = 1.0f;
        pChCal->mLzSga5Flt[1] = 1.25f;

        //! ref scale
        pChCal->mLzRefScale[ 0 ] = 0.114f/lsb_base;
        pChCal->mLzRefScale[ 1 ] = 0.114f/lsb_base;
        pChCal->mLzRefScale[ 2 ] = 1.16f/lsb_base;
        pChCal->mLzRefScale[ 3 ] = 1.16f/lsb_base;

        //! scales
        pChCal->mLzScales_2x_normal = 0;
        pChCal->mLzScales_2x_flt = 0;
        pChCal->mLzScales_20x_normal = 0;
        pChCal->mLzScales_20x_flt = 0;

        //! gnds
        pChCal->mLzGnds = 0;
    }

    //! lf
    for ( int i = 0; i < array_count(mCalLf.mPayload.mChHizTrim); i++ )
    {
        mCalLf.mPayload.mChHizTrim[i].mTrimCount = 9;

        for ( int j = 0; j < 9; j++ )
        {
            mCalLf.mPayload.mChHizTrim[i].mLfTrims[j].mK = _HizK[j].k;
            mCalLf.mPayload.mChHizTrim[i].mLfTrims[j].mTrims[0] = 127;   //! dc
            mCalLf.mPayload.mChHizTrim[i].mLfTrims[j].mTrims[1] = 0;     //! lf
            mCalLf.mPayload.mChHizTrim[i].mLfTrims[j].mTrims[2] = 45;    //! mfc
            mCalLf.mPayload.mChHizTrim[i].mLfTrims[j].mTrims[3] = 19;    //! hf

            mCalLf.mPayload.mChHizTrim[i].mLfTrims[j].mRSel = 7;
            mCalLf.mPayload.mChHizTrim[i].mLfTrims[j].mCSel = 0;
        }
    }

    //! ext
    mExtCal.mPayload.setCal( 22612, EXT_LSB );

    //! la
    mLaCal[0].mPayload.setCal( 32768, LA_LSB );
    mLaCal[1].mPayload.setCal( 32768, LA_LSB );

    //! probe
    mProbeCal[0].mPayload.setCal( 32768, PROBE_LSB );
    mProbeCal[1].mPayload.setCal( 32768, PROBE_LSB );
    mProbeCal[2].mPayload.setCal( 32768, PROBE_LSB );
    mProbeCal[3].mPayload.setCal( 32768, PROBE_LSB );

    //! adc core go

    //! adc phase
//    mCalAdcPhase
}

#define on_err( err, info )   if ( err != ERR_NONE )\
                              { LOG()<<info; \
                                if ( sumErr == ERR_NONE ) sumErr = err;\
                              }
DsoErr servCal::loadCalData( calFileDb &fileDb,
                             calFileDb &fileDef )
{
    DsoErr err, sumErr;
    sumErr = ERR_NONE;

    err = vertLoad( fileDb.mVert );
    if ( err != ERR_NONE )
    {
        err = vertLoad(fileDef.mVert);
    }
    on_err( err, "vert fail");

    err = iDelay_load( fileDb.mLineDelay[0], fileDb.mLineDelay[0] );
    if ( err != ERR_NONE )
    {
        err = iDelay_load( fileDef.mLineDelay[0], fileDef.mLineDelay[0] );
    }
    on_err( err, "idelay fail");

    err = lfLoad( fileDb.mLF );
    if ( err != ERR_NONE )
    {
        qDebug() << "!!!!!!!!!!!!!!!!!!Invalid LF CAL DATA!!!!!!!!!!!!!!!!!!" << err;
        err = lfLoad( fileDef.mLF );
    }
    on_err( err, "lf fail");

    err = calAdcLoad( fileDb.mAdcGo[0], fileDb.mAdcGo[1] );
    if ( err != ERR_NONE )
    {
        err = calAdcLoad( fileDef.mAdcGo[0], fileDef.mAdcGo[1] );
    }
    on_err( err, "adc go fail");

    err = extLoad( fileDb.mExt );
    if ( err != ERR_NONE )
    {
        err = extLoad( fileDef.mExt );
    }
    on_err( err, "ext fail");

    err = laLoad( fileDb.mLa[0], fileDb.mLa[1] );
    if ( err != ERR_NONE )
    {
        err = laLoad( fileDef.mLa[0], fileDef.mLa[1] );
    }
    on_err( err, "la fail");

    err = phaseLoad( fileDb.mAdcSamp[0], fileDb.mAdcSamp[1] );
    if ( err != ERR_NONE )
    {
        err = phaseLoad( fileDef.mAdcSamp[0], fileDef.mAdcSamp[1] );
    }
    on_err( err, "phase fail");

    err = chDelayLoad( fileDb.mCHDelay );
    if ( err != ERR_NONE )
    {
        err = chDelayLoad( fileDef.mCHDelay );
    }
    on_err( err, "ch delay fail");

    //! pll
    //! pll 5g
    calPllData cfgData;
    if ( cfgData.load( fileDb.mPll5g, &cfgData.mPayload, sizeof(cfgData.mPayload)) )
    {
//        LOG_DBG()<<cfgData.mPayload.mLength;
//        LOG_DBG()<<cfgData.mPayload.mRequest[0].mReg<<cfgData.mPayload.mRequest[0].mData;
    }

    return ERR_NONE;
}

void servCal::applyCalData( bool cal)
{
    r_meta::CServMeta meta;
    int iLoad;

    //! sync adc
    getSpu( 0 )->setCtrl_adcsync();

    //! now apply
    for( int spuId = 0; spuId < getSpuCnt(); spuId++ )
    {
        //! lvds
        getAdc( spuId)->setIlvds( mCalHori.mPayload.mIcIDelay[spuId].mILVDS );

        //! for each bit
        for ( int i = 0; i < 8; i++ )
        {
            iDelay_apply( spuId,
                          i,
                          mCalHori.mPayload.mIcIDelay[ spuId ].coreDelay,
                          cal
                         );
        }
    }

    //! apply cal para

    //! hiz trim
    postEngine( ENGINE_CH_HIZ_TRIM, chan1, &mCalLf.mPayload.mChHizTrim[0] );
    postEngine( ENGINE_CH_HIZ_TRIM, chan2, &mCalLf.mPayload.mChHizTrim[1] );
    postEngine( ENGINE_CH_HIZ_TRIM, chan3, &mCalLf.mPayload.mChHizTrim[2] );
    postEngine( ENGINE_CH_HIZ_TRIM, chan4, &mCalLf.mPayload.mChHizTrim[3] );

    //! adc core go
    postEngine( ENGINE_ADC_CORE_GO, 0, &mCalAdcGo.mPayload.mCalGp );
    postEngine( ENGINE_ADC_CORE_GO, 1, &mCalAdcGo.mPayload.mCalGp );

    //! adc core phase
    meta.getMetaVal("servcal/load_cal/phase", iLoad );
    if (  iLoad > 0 )
    {
        postEngine( ENGINE_ADC_CORE_PHASE, 0, &mCalAdcPhase[0].mPayload );
        postEngine( ENGINE_ADC_CORE_PHASE, 1, &mCalAdcPhase[1].mPayload );
    }

    //! la cal
    postEngine( ENGINE_LA_CAL, 0, &mLaCal[0].mPayload );
    postEngine( ENGINE_LA_CAL, 1, &mLaCal[1].mPayload );

    //! ext cal
    postEngine( ENGINE_EXT_CAL, 0, &mExtCal.mPayload );

    //! probe
    postEngine( ENGINE_PROBE_CAL, 0, &mProbeCal[0].mPayload );
    postEngine( ENGINE_PROBE_CAL, 1, &mProbeCal[1].mPayload );
    postEngine( ENGINE_PROBE_CAL, 2, &mProbeCal[2].mPayload );
    postEngine( ENGINE_PROBE_CAL, 3, &mProbeCal[3].mPayload );

    //! ch cal
    postEngine( ENGINE_CH_CAL, chan1, &mCalPara.mPayload.mCHs[0] );
    postEngine( ENGINE_CH_CAL, chan2, &mCalPara.mPayload.mCHs[1] );
    postEngine( ENGINE_CH_CAL, chan3, &mCalPara.mPayload.mCHs[2] );
    postEngine( ENGINE_CH_CAL, chan4, &mCalPara.mPayload.mCHs[3] );

}

