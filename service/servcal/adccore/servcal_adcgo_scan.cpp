
#include "servcal.h"

#include "../../arith/lesssquare.h"
#include "../../arith/linear.h"
#include "../../arith/sign.h"
#include "../../arith/compare.h"

#define gain_error_lower   0.0005f
#define gain_error_upper   0.002f
#define gain_error_iter    0.01f

#define offset_error_lower  0.1f
#define offset_error_upper  0.3f
#define offset_error_iter   3.0f

//#define gain_error_lower   0.0005f
//#define gain_error_upper   0.001f
//#define gain_error_iter    0.01f

//#define offset_error_lower  0.05f
//#define offset_error_upper  0.1f
//#define offset_error_iter   3.0f

#define gain_range      16
#define off_range       16

#define prog_n( n )     setProgNow( progFrom + prog * steps/total );\
                        prog += n;\
                        if ( prog > total ) prog=total;
DsoErr servCal::calAdcScan( int spuCnt, int progFrom, int steps )
{
    DsoErr err;
    SampleCase sampCase;
    int prog, total;

    prog = 0;
    total = 10;

    //! 4 channel
    selectAdcPath( cal_adc_abcd );

    //! samp -- ref 0
    err = calAdcSamp( 0, &sampCase );
    if ( ERR_NONE != err ) return err;
    prog_n(1);

    //! foreach core -- base line
    for ( int i = 0; i < 4; i++ )
    {
        err = calAdcCoreScan( 0,
                              sampCase,
                              i, i,
                              mCalAdcGo.mPayload.mLinears + i );
        if ( err != ERR_NONE )
        { return err; }
    }
    prog_n(1);

    //! for the interleave

    //! ---- 2 ch interleave
    err = calAdcScan_ac( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_ac");
        return err;
    }
    prog_n(1);

    err = calAdcScan_ad( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_ad");
        return err;
    }
    prog_n(1);

    err = calAdcScan_bc( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_bc");
        return err;
    }
    prog_n(1);

    err = calAdcScan_bd( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_bd");
        return err;
    }
    prog_n(1);

    //! ---- 4 ch interleave
    err = calAdcScan_a_abcd( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_a_abcd");
        return err;
    }
    prog_n(1);

    err = calAdcScan_b_abcd( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_b_abcd");
        return err;
    }
    prog_n(1);

    err = calAdcScan_c_abcd( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_c_abcd");
        return err;
    }
    prog_n(1);

    err = calAdcScan_d_abcd( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_d_abcd");
        return err;
    }
    prog_n(1);

    //! ----- 1 ch interleave with slave spu
    err = calAdcScan_a_a( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_a_a");
        return err;
    }
    prog_n(1);

    err = calAdcScan_b_b( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_b_b");
        return err;
    }
    prog_n(1);

    err = calAdcScan_c_c( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_c_c");
        return err;
    }
    prog_n(1);

    err = calAdcScan_d_d( spuCnt, sampCase );
    if ( err != ERR_NONE )
    {
        logInfo( __FUNCTION__, "calAdcScan_d_d");
        return err;
    }
    prog_n(1);


    return ERR_NONE;
}

//! sample by ch1
#define sample_from 16
#define sample_to   240

#define sample_step 32
//#define sample_step 16
//#define sample_step 12
//#define sample_step 8
//#define sample_step 4

#define sample_n    ( (sample_to-sample_from)/(sample_step) + 1 )
DsoErr servCal::calAdcSamp( int spuId, SampleCase *pSampCase )
{
    Q_ASSERT( NULL != pSampCase );
    DsoErr err;

    int adcs[ sample_n ];
    int dacs[ array_count(adcs) ];
    float vals[ array_count(adcs) ];

    //! fill sample n
    for ( int sample = sample_from, i = 0;
          sample <= sample_to;
          sample += sample_step )
    {
        adcs[i++] = sample;
    }

    //! scan line
    for ( int i = 0; i < array_count(adcs); i++ )
    {
        err = iterDac1( spuId, 0, 0,
                       adcs[i],
                       dacs[i],
                       vals[i]);
        if ( err != ERR_NONE )
        {
            logInfo( __FUNCTION__, QString::number(adcs[i]) );
            return err;
        }

        pSampCase->append(  dacs[i],
                            vals[i]
                         );

        LOG()<<vals[i]<<dacs[i];
    }

    return ERR_NONE;
}

//! scan the samp dac
//! adc = gain * dac + offset
//! dac = gain * adc + offset
DsoErr servCal::calAdcCoreScan(
                            int spuId,
                            SampleCase &sampCase,
                            int chId,
                            int masterId,
                            calLinear *pLine )
{

    Q_ASSERT( NULL != pLine );

    DsoErr err;
    float sampAdc;

    //! for each samp dac
    for ( int i = 0; i < sampCase.size(); i++ )
    {
        err = sampDac( spuId, chId, masterId,
                       sampCase.getDac(i),
                       &sampAdc
                       );
        if ( err != ERR_NONE )
        {
            logInfo( __FUNCTION__, QString().setNum(sampCase.getDac(i)));
            return err;
        }

        sampCase.setAdc( i, sampAdc );
    }

    //! set to middle
    if ( sampCase.size() > 0 )
    {
        sampDac( spuId, chId, masterId,
                 sampCase.getDac( sampCase.size()/2 ),
                 &sampAdc
                 );
    }

    //! slove the k,m
    int ret = sampCase.lineKM( &pLine->k, &pLine->m, &pLine->r);
    if ( 0 != ret )
    {
        LOG() << "ERROR:" << ret;
        return ERR_CAL_ADC;
    }
    else
    {
        //! tune m
        pLine->m2 = pLine->k * sampCase.midDac() + pLine->m;

        //LOG()<<pLine->k<<pLine->m<<pLine->m2<<pLine->r;
        return ERR_NONE;
    }
}

//! iter core x by ref line
DsoErr servCal::iterAdcCoreX( int spuId,
                              SampleCase &sampCase,
                              int chId,
                              int masterId,
                              calLinear &lineRef,
                              adcCoreCal *pCoreCal )
{
    DsoErr err;

    IPhyADC *pAdc = getAdc( spuId );

    calLinear theLineA, theLineB;

    //! adc = REF.k * dac + REF.m

    //! adc = a.k.768 * dac + a.m.768
    pAdc->setGain( chId + 1, ADC_CORE_GAIN_A );
    pAdc->setOffset( chId + 1, ADC_CORE_OFFSET_A );
    err = calAdcCoreScan( spuId, sampCase, chId, masterId, &theLineA );
    if ( err != ERR_NONE )
    {
        return err;
    }

    //! adc = b.k.256 * dac + b.m.256
    pAdc->setGain( chId + 1, ADC_CORE_GAIN_B );
    pAdc->setOffset( chId + 1, ADC_CORE_OFFSET_B );
    err = calAdcCoreScan( spuId, sampCase, chId, masterId, &theLineB );
    if ( err != ERR_NONE )
    {
        return err;
    }

    //! adc gain line
    //! a.k.768 = gain.a * 768 + gain.m
    //! b.k.256 = gain.a * 256 + gain.m
    calLinear gainLine;
    calLinear offLine;
    int ret;
    ret = linear( (float)ADC_CORE_GAIN_A, theLineA.k,
                  (float)ADC_CORE_GAIN_B, theLineB.k,
                  &gainLine.k, &gainLine.m );
    if( ret != 0 )
    {
        LOG()<< "ERROR" << gainLine.k<<gainLine.m;
        return ERR_CAL_ADC_LINEAR;
    }

    //! adc offset line
    //! a.m.768 = off.a * 768 + off.m
    //! b.m.256 = off.a * 256 + off.m
    ret = linear( (float)ADC_CORE_OFFSET_A, theLineA.m,
                  (float)ADC_CORE_OFFSET_B, theLineB.m,
                  &offLine.k, &offLine.m );
    if ( ret != 0 )
    {
        LOG()<< "ERROR" << offLine.k<<offLine.m;
        return ERR_CAL_ADC_LINEAR;
    }

    //! expect the gain, offset
    if ( gainLine.k == 0 || offLine.k == 0 )
    {
        LOG()<< "ERROR";
        return ERR_CAL_ADC_LINEAR;
    }

    int eGain, eOff;
    eGain = ( lineRef.k - gainLine.m ) / gainLine.k;
    eOff = ( lineRef.m - offLine.m ) / offLine.k;
    LOG()<<eGain<<eOff;

    //! to iterate again
    int coreGain, coreOff;
    err = searchAdcCoreX( spuId, sampCase,
                          chId, masterId,
                          eGain, eOff,
                          lineRef,

                          &coreGain,
                          &coreOff,

                          sign( gainLine.k ) * gain_range,
                          sign( offLine.k ) * off_range
                          );
    if ( err != ERR_NONE )
    { return err; }

    //! save the gain, off
    pCoreCal->setGain( coreGain );
    pCoreCal->setOffset( coreOff );

    return ERR_NONE;
}

DsoErr servCal::searchAdcCoreX(
                       int spuId,
                       SampleCase &sampCase,
                       int chId,
                       int masterId,
                       int gain,            //! init gain
                       int offset,          //! init offset
                       calLinear &lineRef,  //! ref line
                       int *pOutGain,
                       int *pOutOff,
                       int gainRange,
                       int offRange
                       )
{
    DsoErr err;
    IPhyADC *pAdc = getAdc( spuId );
    calLinear theLine;

    int gainStep = gainRange;
    int offStep = offRange;

    int cmp = 0;
    bool bGainFinish, bOffFinish;
    bool bGainInc, bOffInc;

    float gainErrLower, gainErrUpper, gainErrIter;

                                           //! gain error
    gainErrLower = lineRef.k * gain_error_lower;
    gainErrUpper = lineRef.k * gain_error_upper;
    gainErrIter = lineRef.k * gain_error_iter;

    bGainFinish = false;
    bOffFinish = false;

    bGainInc = true;
    bOffInc = true;
    do
    {
        //! config adc
        pAdc->setGain( chId + 1, gain );
        pAdc->setOffset( chId + 1, offset );

        //LOG()<<gain<<gainStep;
        //LOG()<<offset<<offStep;

        //! scan line
        err = calAdcCoreScan( spuId, sampCase, chId, masterId, &theLine );
        if ( err != ERR_NONE )
        { return err; }

        //! < ref
        cmp = value_compare( theLine.k, lineRef.k, gainErrUpper );
        if ( cmp == 0 )
        {
            bGainFinish = true;
            //LOG()<<"gain finish";
        }
        //! to iter
        else if ( value_compare(theLine.k, lineRef.k, gainErrIter) == 0 )
        {
            bGainFinish = false;
        }
        else
        {
            bGainFinish = false;
            gainStep = gainRange;
        }

        //! iter gain
        if ( !bGainFinish )
        {
            cmp = value_compare( theLine.k, lineRef.k, gainErrLower );
            if ( cmp < 0 )
            {
                gain += gainStep;
                bGainInc = true;
            }
            //! > ref
            else if ( cmp > 0 )
            {
                //! invert dir
                //! half step
                if ( bGainInc )
                {
                    gainStep /= 2;
                    bGainInc = false;
                }

                gain -= gainStep;
            }
            else
            {
                bGainFinish = true;
            }
        }

        //! offset
        cmp = value_compare( theLine.m2, lineRef.m2, offset_error_upper );
        if ( cmp == 0 )
        {
            bOffFinish = true;
            //LOG()<<"offset finish";
        }
        //! to iter
        else if ( value_compare( theLine.m2, lineRef.m2, offset_error_iter ) == 0 )
        {
            bOffFinish = false;
        }
        else
        {
            bOffFinish = false;
            offStep = offRange;
        }

        //! iter offset
        if ( !bOffFinish )
        {
            cmp = value_compare( theLine.m2, lineRef.m2, offset_error_lower );
            //qDebug() << "OFFSET:" <<offset<<offStep <<  theLine.m2 << lineRef.m2;
            if ( cmp < 0 )
            {
#ifdef DAMREY_VER_1_5_x
                offset -= offStep;
#else
                offset += offStep;
#endif
                bOffInc = true;
            }
            else if ( cmp > 0 )
            {
                if ( bOffInc )
                {
                    offStep /= 2;
                    bOffInc = false;
                }
#ifdef DAMREY_VER_1_5_x
                offset += offStep;
#else
                offset -= offStep;
#endif
            }
            else
            {
                bOffFinish = true;
            }
        }

        //! finish
        if ( bOffFinish || offStep == 0 )
        {
            if ( bGainFinish || gainStep == 0 )
            {
                break;
            }
        }

        //! over range
        if ( offset < 0 || offset > 1023 )
        {
            return ERR_CAL_ADC_LINEAR;
        }

        if ( gain < 0 || gain > 1023 )
        {
            return ERR_CAL_ADC_LINEAR;
        }

    }while( 1 );

    *pOutGain = gain;
    *pOutOff = offset;

    LOG()<<gain<<offset;

    return ERR_NONE;
}

//! a, c input
DsoErr servCal::calAdcScan_ac( int spuCnt, SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_xx( spuCnt,
                         sampCase,
                         cal_adc_ac,
                         0, 2,
                         &mCalAdcGo.mPayload.mCalGp.mAC );

    return err;
}

DsoErr servCal::calAdcScan_ad( int spuCnt, SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_xx( spuCnt,
                         sampCase,
                         cal_adc_ad,
                         0, 3,
                         &mCalAdcGo.mPayload.mCalGp.mAD );

    return err;
}

DsoErr servCal::calAdcScan_bc( int spuCnt, SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_xx( spuCnt,
                         sampCase,
                         cal_adc_bc,
                         1, 2,
                         &mCalAdcGo.mPayload.mCalGp.mBC );

    return err;
}

DsoErr servCal::calAdcScan_bd( int spuCnt, SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_xx( spuCnt,
                         sampCase,
                         cal_adc_bd,
                         1, 3,
                         &mCalAdcGo.mPayload.mCalGp.mBD );

    return err;
}

DsoErr servCal::calAdcScan_xx(
                          int spuCnt,
                          SampleCase &sampCase,
                          calAdcPath adcPath,
                          int masterId1,
                          int masterId2,
                          adcCal *pCal )
{
    DsoErr err;

    selectAdcPath( adcPath );

    for ( int i = 0; i < spuCnt; i++ )
    {
        //! group1
        //! a
        err = iterAdcCoreX( i,
                            sampCase,
                            0, masterId1,
                            mCalAdcGo.mPayload.mLinears[masterId1],
                            &pCal->mCores[i][0] );
        if ( err != ERR_NONE )
        {
            return err;
        }

        //! b
        err = iterAdcCoreX( i,
                            sampCase,
                            1, masterId1,
                            mCalAdcGo.mPayload.mLinears[masterId1],
                            &pCal->mCores[i][1] );
        if ( err != ERR_NONE )
        {
            return err;
        }
    }

    for ( int i = 0; i < spuCnt; i++ )
    {
        //! group2
        //! c
        err = iterAdcCoreX( i,
                            sampCase,
                            2, masterId2,
                            mCalAdcGo.mPayload.mLinears[masterId2],
                            &pCal->mCores[i][2] );
        if ( err != ERR_NONE ) return err;

        //! d
        err = iterAdcCoreX( i,
                            sampCase,
                            3, masterId2,
                            mCalAdcGo.mPayload.mLinears[masterId2],
                            &pCal->mCores[i][3] );
        if ( err != ERR_NONE ) return err;
    }

    return ERR_NONE;
}

DsoErr servCal::calAdcScan_a_abcd( int spuCnt, SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_x_abcd( spuCnt,
                             sampCase,
                             cal_adc_a,
                             0,
                             &mCalAdcGo.mPayload.mCalGp.mA );

    return err;
}

DsoErr servCal::calAdcScan_b_abcd( int spuCnt, SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_x_abcd( spuCnt,
                             sampCase,
                             cal_adc_b,
                             1,
                             &mCalAdcGo.mPayload.mCalGp.mB );

    return err;
}

DsoErr servCal::calAdcScan_c_abcd( int spuCnt,
                                   SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_x_abcd(    spuCnt,
                                sampCase,
                                cal_adc_c,
                                2,
                                &mCalAdcGo.mPayload.mCalGp.mC );

    return err;
}

DsoErr servCal::calAdcScan_d_abcd( int spuCnt, SampleCase &sampCase )
{
    DsoErr err;

    err = calAdcScan_x_abcd(    spuCnt,
                                sampCase,
                                cal_adc_d,
                                3,
                                &mCalAdcGo.mPayload.mCalGp.mD );

    return err;
}

DsoErr servCal::calAdcScan_x_abcd( int spuCnt,
                                   SampleCase &sampCase,
                                   calAdcPath adcPath,
                                   int masterId,
                                   adcCal *pCal )
{
    DsoErr err;

    selectAdcPath( adcPath );

    for( int spuId = 0; spuId < spuCnt; spuId++ )
    {
        for ( int i = 0; i < 4; i++ )
        {
            err = iterAdcCoreX( spuId,
                                sampCase,
                                i, masterId,
                                mCalAdcGo.mPayload.mLinears[masterId],
                                &pCal->mCores[spuId][i] );
            if ( err != ERR_NONE ) return err;
        }
    }

    return ERR_NONE;
}

DsoErr servCal::calAdcScan_a_a( int spuCnt,
                                SampleCase &sampCase )
{
    return calAdcScan_x_x( spuCnt,
                           sampCase,
                           cal_adc_abcd,
                           0,
                           &mCalAdcGo.mPayload.mCalGp.mABCD );
}
DsoErr servCal::calAdcScan_b_b( int spuCnt,
                                SampleCase &sampCase )
{
    return calAdcScan_x_x( spuCnt,
                           sampCase,
                           cal_adc_abcd,
                           1,
                           &mCalAdcGo.mPayload.mCalGp.mABCD );
}
DsoErr servCal::calAdcScan_c_c( int spuCnt,
                                SampleCase &sampCase )
{
    return calAdcScan_x_x( spuCnt,
                           sampCase,
                           cal_adc_abcd,
                           2,
                           &mCalAdcGo.mPayload.mCalGp.mABCD );
}
DsoErr servCal::calAdcScan_d_d( int spuCnt,
                                SampleCase &sampCase )
{
    return calAdcScan_x_x( spuCnt,
                           sampCase,
                           cal_adc_abcd,
                           3,
                           &mCalAdcGo.mPayload.mCalGp.mABCD );
}

DsoErr servCal::calAdcScan_x_x(
                       int spuCnt,
                       SampleCase &sampCase,
                       calAdcPath adcPath,
                       int id,
                       adcCal *pCal )
{
    DsoErr err;
    Q_ASSERT( NULL != pCal );

    selectAdcPath( adcPath );

    //! base
    pCal->mCores[0][id].setGain( ADC_CORE_GAIN_MID );
    pCal->mCores[0][id].setOffset( ADC_CORE_OFFSET_MID );

    if( spuCnt > 1 )
    {
        err = iterAdcCoreX( 1,
                            sampCase,
                            id, id,
                            mCalAdcGo.mPayload.mLinears[id],
                            &pCal->mCores[1][id] );
        if ( err != ERR_NONE ) return err;

        return err;
    }
    else
    { return ERR_NONE; }
}
