
#include "../servcal.h"

//! adc Gain & Offset
DsoErr servCal::calAdcProc( int progFrom, int steps )
{
    DsoErr err;

    preAdcGO();

    err = calAdcGO( progFrom, steps );

    postAdcGO();

    if ( err == ERR_NONE )
    {
        err = calAdcSave();
    }
    return err;
}

void servCal::preAdcGO()
{
    logInfo("enter ADC");

    IPhyADC *pAdc;

    //! foreach adc
    for ( int adcId = 0; adcId < getAdcCnt(); adcId++ )
    {
        pAdc = getAdc( adcId );

        //! foreach core
        //! offset && gain rst
        for ( int i = 1; i < 5; i++ )
        {
            pAdc->setOffset( i, ADC_CORE_OFFSET_MID );
            pAdc->setGain( i, ADC_CORE_GAIN_MID );
        }
    }

#if 0
    for(int i=chan1; i<=chan4; i++)
    {
        IPhyCH *pCh = getCH( (Chan)i);
        pCh->setHzScaleOnly(50000);
        pCh->flushWCache();
    }

#else
    //! for each ch to 1M ohm
    IPhyAfe *pAfe;
    for ( int i = 0; i < 4; i++ )
    {
        pAfe = getAfe( i );

        //! 1MOHM
        pAfe->setOlEn( 0 );
        pAfe->setOSel( 2 );

        pAfe->setLzNpd( 0 );
        pAfe->setHzNpd( 1 );
        pAfe->setLpp( 0 );

        //! lz
        pAfe->setGtrim1( 0, 1 );
        pAfe->setGtrim2( 0, 3 );
        pAfe->setGtrim3( 0, 3 );

        pAfe->setGtrim4( 1, 0 );
        pAfe->setGtrim5( 1, 0 );

        //! hz
        pAfe->setHzSga1Gs( 0 );
        pAfe->setAtten( 4 );        //! limit the noise
        pAfe->setK( 24 );
        pAfe->setVosExp( 1 );

        pAfe->flushWCache();
    }
#endif
    IPhySpuGp *pSpuGp;
    pSpuGp = getSpuGp();
                                    //! for each core
    pSpuGp->setCTRL_non_inter( 1 );
    pSpuGp->flushWCache();
#ifdef DAMREY_VER_1_5_x
    setSpuInvert( true );
#else
    setSpuInvert( false );
#endif
}
void servCal::postAdcGO()
{
    IPhySpuGp *pSpuGp;

    //! spu
    pSpuGp = getSpuGp();
    pSpuGp->setCTRL_non_inter( 0 );
    pSpuGp->flushWCache();

    logInfo("exit ADC");
}

DsoErr servCal::calAdcGO( int progFrom, int steps )
{
    DsoErr err;
    int spuCnt;

    //! spu cnt = 1,2
    spuCnt = getSpuCnt();
    Q_ASSERT( spuCnt > 0 );

    //! spu1/2
    err = calAdcScan( spuCnt, progFrom, steps/spuCnt );
    if ( err != ERR_NONE )
    {
        return err;
    }

    return err;
}

DsoErr servCal::calAdcSave()
{
    int size = sizeof(mCalAdcGo.mPayload);
    if ( size != mCalAdcGo.save( mCalFileDbs[1].mAdcGo[0],
                    &mCalAdcGo.mPayload, size ) )
    {
        return ERR_CAL_WRITE_FAIL;
    }

    return ERR_NONE;
}
DsoErr servCal::calAdcLoad( const QString &fileName0,
                            const QString &/*fileName1*/ )
{
    //! new
    calAdcGOData *pAdcCal = new calAdcGOData();
    if ( NULL == pAdcCal )
    { return ERR_CAL_READ_FAIL; }

    //! load
    int size = sizeof( pAdcCal->mPayload );
    if ( pAdcCal->load( fileName0,
                        &pAdcCal->mPayload,
                        size ) != size )
    {
        delete pAdcCal;
        return ERR_CAL_READ_FAIL;
    }

    //! export
    mCalAdcGo = *pAdcCal;
    delete pAdcCal;

    return ERR_NONE;
}


