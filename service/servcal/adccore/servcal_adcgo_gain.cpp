#include "../servcal.h"

//! 1 vpp1 > vpp2
//! 0 vpp1 == vpp2
//! -1 vpp1 < vpp2
#define gain_cmp_error 1
int servCal::adcGainCmp( float vpp1, float vpp2 )
{
    int delta;

    delta = vpp1 - vpp2;
    if ( delta < -gain_cmp_error )
    { return -1; }
    else if ( delta > gain_cmp_error )
    { return 1; }
    else
    { return 0; }
}

DsoErr servCal::iterGain( int chId, int masterChId,
                          float vppRef,
                          int &gainOut,
                          float &vppOut )
{
    DsoErr err;
    float vpp0, vpp1023;

    //! min-max
    err = coreGainDelta( chId, masterChId, 0, vpp0 );
    if ( err != ERR_NONE ) return err;

    err = coreGainDelta( chId, masterChId, 1023, vpp1023 );
    if ( err != ERR_NONE ) return err;

    LOG()<<vpp0<<vpp1023<<vppRef;

    //! a - low
    //! b - up
    int a, b;
    if ( vpp0 < vpp1023 )
    {
        //! ref in range
        if ( vppRef < vpp0 || vppRef > vpp1023 )
        { return ERR_CAL_ADC_GAIN; }

        a = 0;
        b = 1023;
    }
    else if ( vpp0 > vpp1023 )
    {
        if ( vppRef > vpp0 || vppRef < vpp1023 )
        { return ERR_CAL_ADC_GAIN; }

        a = 1023;
        b = 0;
    }
    else
    { return ERR_CAL_ADC_GAIN; }

    int gain;
    float vppMe;
    int cmp;
    int step;

    //! upper
    gain = b;
    step = ( b - a ) / 2;

    while( step != 0 )
    {
        //! gain now
        err = coreGainDelta( chId, masterChId, gain, vppMe );
        if ( err != ERR_NONE )
        { return ERR_CAL_ADC_GAIN; }

        cmp = adcGainCmp( vppMe, vppRef );
        LOG()<<gain<<vppMe<<vppRef;
        //! me > vpp
        if ( cmp > 0 )
        {
            gain -= step;
        }
        else if ( cmp  < 0 )
        {
            gain += step;
            step /= 2;
            gain -= step;
        }
        else
        { break; }

        if ( gain < 0 || gain > 1023 )
        { return ERR_CAL_ADC_GAIN;}

    }

    gainOut = gain;
    vppOut = vppMe;

    LOG()<<gainOut<<vppOut;

    return ERR_NONE;
}

DsoErr servCal::coreGainDelta( int chId, int masterChId,
                               int gain,
                               float &vpp )
{
    DsoErr err;

    int delta;
    float fDelta;
    IPhyADC *pAdc = getAdc( 0 );

    //! set gain
    //! core gain
    //! chid + 1 = page id
    pAdc->setGain( chId + 1, gain );
    wait_n( 1 );

    //! core delta
    err = iterDelta( chId, masterChId, ADC_TOP, ADC_BASE, delta, fDelta );
    if ( err != ERR_NONE )
    { return err; }

    vpp = fDelta;

    return ERR_NONE;
}

//! 2 chs
DsoErr servCal::calAdcGain_xx( int chId1,
                      int chId2,
                      calAdcPath adcPath,
                      adcCal &calOut )
{
    selectAdcPath( adcPath );

    DsoErr err;

    float ref;
    err = coreGainDelta( chId1, chId1, ADC_CORE_GAIN_MID, ref );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_GAIN;
    LOG();

    int gainOut;
    float vppOut;
    err = iterGain( chId2, chId1, ref, gainOut, vppOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_GAIN;

    calOut.mCores[ chId1 ].setGain( ADC_CORE_GAIN_MID );
    calOut.mCores[ chId2 ].setGain( gainOut );

    LOG()<<ref<<vppOut<<gainOut;

    return ERR_NONE;
}

DsoErr servCal::calAdcGain_ac()
{
    DsoErr err;

    //! a,b
    err = calAdcGain_xx( 0, 1, cal_adc_ac,
                          mCalAdc.mCalGp.mAC );
    if ( err != ERR_NONE ) return err;

    //! c,d
    err = calAdcGain_xx( 2, 3, cal_adc_ac,
                          mCalAdc.mCalGp.mAC );

    return err;
}

DsoErr servCal::calAdcGain_bc()
{
    DsoErr err;

    //! b, a
    err = calAdcGain_xx( 1, 0, cal_adc_bc,
                          mCalAdc.mCalGp.mBC );
    if ( err != ERR_NONE ) return err;

    //! c,d
    err = calAdcGain_xx( 2, 3, cal_adc_ac,
                          mCalAdc.mCalGp.mBC );

    return err;

}
DsoErr servCal::calAdcGain_ad()
{
    DsoErr err;

    //! a, b
    err = calAdcGain_xx( 0, 1, cal_adc_ad,
                          mCalAdc.mCalGp.mAD );
    if ( err != ERR_NONE ) return err;

    //! d,c
    err = calAdcGain_xx( 3, 2, cal_adc_ad,
                          mCalAdc.mCalGp.mAD );
    return err;
}
DsoErr servCal::calAdcGain_bd()
{
    DsoErr err;

    //! b,a
    err = calAdcGain_xx( 1, 0, cal_adc_bd,
                          mCalAdc.mCalGp.mBD );

    //! d,c
    err = calAdcGain_xx( 3, 2, cal_adc_bd,
                         mCalAdc.mCalGp.mBD );
    return err;
}

//! 1ch
DsoErr servCal::calAdcGain_x( int chIdRef,
                     int chIdSib0,
                     int chIdSib1,
                     int chIdSib2,
                     calAdcPath adcPath,
                     adcCal &calOut )
{
    selectAdcPath( adcPath );

    DsoErr err;
    float ref;
    err = coreGainDelta( chIdRef, chIdRef, ADC_CORE_GAIN_MID, ref );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_GAIN;
    calOut.mCores[ chIdRef ].setGain( ADC_CORE_GAIN_MID );

    int gainOut;
    float vppOut;
    err = iterGain( chIdSib0, chIdRef, ref, gainOut, vppOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_GAIN;
    calOut.mCores[ chIdSib0 ].setGain( gainOut );

    err = iterGain( chIdSib1, chIdRef, ref, gainOut, vppOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_GAIN;
    calOut.mCores[ chIdSib1 ].setGain( gainOut );

    err = iterGain( chIdSib2, chIdRef, ref, gainOut, vppOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_GAIN;
    calOut.mCores[ chIdSib2 ].setGain( gainOut );

    return ERR_NONE;
}

DsoErr servCal::calAdcGain_a()
{
    return calAdcGain_x( 0, 1, 2, 3, cal_adc_a, mCalAdc.mCalGp.mA );
}
DsoErr servCal::calAdcGain_b()
{
    return calAdcGain_x( 1, 0, 2, 3, cal_adc_b, mCalAdc.mCalGp.mB );
}
DsoErr servCal::calAdcGain_c()
{
    return calAdcGain_x( 2, 0, 1, 3, cal_adc_c, mCalAdc.mCalGp.mC );
}
DsoErr servCal::calAdcGain_d()
{
    return calAdcGain_x( 3, 0, 1, 2, cal_adc_d, mCalAdc.mCalGp.mD );
}

