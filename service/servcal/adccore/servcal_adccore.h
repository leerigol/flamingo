#ifndef SERVCAL_ADCCORE
#define SERVCAL_ADCCORE

#include <QtCore>
#include "../../phy/adc/adccal.h"

#include "../../baseclass/checkedstream/ccheckedstream.h"

struct calLinear
{
    float k, m;
    float m2;
    float r;
};

struct calAdcGoPayload
{
    calLinear mLinears[4];          //! 4 cores's base line
    dso_phy::adcCalGroup mCalGp;    //! intx line
};

struct calAdcGOData : public CCheckedStream
{
    calAdcGoPayload mPayload;
};


#endif // SERVCAL_ADCCORE

