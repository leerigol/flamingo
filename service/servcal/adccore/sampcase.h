#ifndef SAMPCASE
#define SAMPCASE

#include <QtCore>

class SampleCase
{
public:
    SampleCase();

public:
    int size();

    void append( int dac, float adc );

    int getDac( int id );

    float getAdc( int id );
    void setAdc( int id, float adc );

    //! 0 -- no err
    int lineKM( float *pK,
                 float *pM,
                 float *pC );
    int midDac();
private:
    QList<int> mDacList;
    QList<float> mAdcList;
};

#endif // SAMPCASE

