#include "sampcase.h"
#include "../../include/dsocfg.h"
#include "../../arith/lesssquare.h"

SampleCase::SampleCase()
{}

int SampleCase::size()
{
    Q_ASSERT( mDacList.size() == mAdcList.size() );

    return mDacList.size();
}

void SampleCase::append( int dac, float adc )
{
    mDacList.append( dac );
    mAdcList.append( adc );
}

int SampleCase::getDac( int id )
{
    Q_ASSERT( id >=0 && id < mDacList.size() );

    return mDacList[id];
}

float SampleCase::getAdc( int id )
{
    Q_ASSERT( id >=0 && id < mAdcList.size() );

    return mAdcList[id];
}
void SampleCase::setAdc( int id, float adc )
{
    Q_ASSERT( id >= 0 && id < mAdcList.size() );

    mAdcList[id] = adc;
}

//! find the valid adc && dac to linear
#define adc_tolerance   12
int SampleCase::lineKM( float *pK,
                         float *pM,
                         float *pC )
{
    Q_ASSERT( NULL != pK );
    Q_ASSERT( NULL != pM );
    Q_ASSERT( NULL != pC );

    Q_ASSERT( mAdcList.size() == mDacList.size() );

    float adcs[ mAdcList.size() ];
    float dacs[ mDacList.size() ];
    int count;

    count = 0;
    for ( int i = 0 ; i  < mAdcList.size(); i++ )
    {
        if ( mAdcList[i] > adc_tolerance && mAdcList[i] < adc_max - adc_tolerance )
        {
            adcs[ count ] = mAdcList[i];
            dacs[ count ] = mDacList[i];
            count++;
        }
    }

    //! linear
    if ( count < 2 )
    {        
        return -1;
    }

    //! k*dac + m = adc
    if ( 0 != lessSquare( dacs, adcs, count, pK, pM, pC ) )
    {
        return -2;
    }

    return 0;
}

int SampleCase::midDac()
{
    Q_ASSERT( mAdcList.size() == mDacList.size() );

    int count;
    float dacSum;

    count = 0;
    dacSum = 0;
    for ( int i = 0 ; i  < mAdcList.size(); i++ )
    {
        if ( mAdcList[i] > adc_tolerance
             && mAdcList[i] < adc_max - adc_tolerance )
        {
            dacSum  += mDacList[i];
            count++;
        }
    }

//    if ( count == 0 )
//    { return 32767; }
//    else
//    { return dacSum / count; }

    return 32767;
}
