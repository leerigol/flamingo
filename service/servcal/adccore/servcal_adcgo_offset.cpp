#include "../servcal.h"

#define offset_error    0.5
int servCal::adcOffCmp( float off1, float off2 )
{
    float delta;

    delta = off1 - off2;
    if ( delta < -offset_error )
    { return -1; }
    else if ( delta > offset_error )
    { return 1; }
    else
    { return 0; }
}
DsoErr servCal::iterOffset( int chId,
                            int masterChId,
                            float offRef,
                            int &offOut )
{
    DsoErr err;
    float off0, off1023;

    //! min-max
    err = coreOffset( chId, masterChId, 0, off0 );
    if ( err != ERR_NONE ) return err;

    err = coreOffset( chId, masterChId, 1023, off1023 );
    if ( err != ERR_NONE ) return err;

    LOG()<<off0<<off1023<<offRef;

    int a, b;
    if ( off0 < off1023 )
    {
        if (  offRef < off0 || offRef > off1023 )
        { return ERR_CAL_ADC_OFFSET; }

        a = 0;
        b = 1023;
    }
    else if ( off0 > off1023 )
    {
        if (  offRef > off0 || offRef < off1023 )
        { return ERR_CAL_ADC_OFFSET; }

        a = 1023;
        b = 0;
    }
    else
    { return ERR_CAL_ADC_OFFSET; }

    int off;
    float offMe;
    int cmp;

    int step = (b-a)/2;
    off = b;

    while( step != 0 )
    {
        err = coreOffset( chId, masterChId, off, offMe );
        if ( err != ERR_NONE ) return ERR_CAL_ADC_OFFSET;

        cmp = adcOffCmp( offMe, offRef );
        if ( cmp > 0 )
        {
            off -= step;
        }
        else if ( cmp < 0 )
        {
            off += step;
            step /= 2;
            off -= step;
        }
        else
        { break; }

        if ( off < 0 || off > 1023 )
        { return ERR_CAL_ADC_OFFSET; }
    }

    offOut = off;
//    LOG()<<off<<offMe;
    return ERR_NONE;
}
DsoErr servCal::coreOffset( int chId, int masterChId,
                            int off, float &offOut )
{
    IPhyADC *pAdc = getAdc( 0 );

    pAdc->setOffset( chId + 1, off );
    wait_n( 100 );

    return getAvg( chId, offOut );
}

//! 2chs
DsoErr servCal::calAdcOffset_xx( int chId1,
                      int chId2,
                      calAdcPath adcPath,
                      adcCal &calOut )
{
    selectAdcPath( adcPath );

    DsoErr err;

    int dac;
    float fAdc;
    err = iterDac( chId1, chId1, ADC_MID, dac, fAdc );
    if ( ERR_NONE != err ) return err;

    float ref;
    err = coreOffset( chId1, chId1, ADC_CORE_OFFSET_MID, ref );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_OFFSET;

    int offsetOut;
    err = iterOffset( chId2, chId1, ref, offsetOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_OFFSET;

    calOut.mCores[ chId1 ].setOffset( ADC_CORE_OFFSET_MID );
    calOut.mCores[ chId2 ].setOffset( offsetOut );

    LOG()<<ref<<offsetOut;

    return ERR_NONE;
}
DsoErr servCal::calAdcOffset_ac()
{
    DsoErr err;

    //! a,b
    err = calAdcOffset_xx( 0, 1, cal_adc_ac, mCalAdc.mCalGp.mAC );
    if ( err != ERR_NONE )
    { return err; }

    //! c,d
    err = calAdcOffset_xx( 2, 3, cal_adc_ac, mCalAdc.mCalGp.mAC );
    return err;
}
DsoErr servCal::calAdcOffset_bc()
{
    DsoErr err;

    //! b,a
    err = calAdcOffset_xx( 1, 0, cal_adc_bc, mCalAdc.mCalGp.mBC );
    if ( err != ERR_NONE )
    { return err; }

    //! c,d
    err = calAdcOffset_xx( 2, 3, cal_adc_ac, mCalAdc.mCalGp.mAC );
    return err;
}
DsoErr servCal::calAdcOffset_ad()
{
    DsoErr err;

    //! a, b
    err = calAdcOffset_xx( 0, 1, cal_adc_ad, mCalAdc.mCalGp.mAD );
    if ( err != ERR_NONE )
    { return err; }

    //! d, c
    err = calAdcOffset_xx( 3, 2, cal_adc_ad, mCalAdc.mCalGp.mAD );
    return err;
}
DsoErr servCal::calAdcOffset_bd()
{
    DsoErr err;

    //! b,a
    err = calAdcOffset_xx( 1, 0, cal_adc_bd, mCalAdc.mCalGp.mBD );
    if ( err != ERR_NONE ) return err;

    //! c, d
    err = calAdcOffset_xx( 2, 3, cal_adc_bd, mCalAdc.mCalGp.mBD );
    return err;
}

//! 1ch
DsoErr servCal::calAdcOffset_x( int chIdRef,
                     int chIdSib0,
                     int chIdSib1,
                     int chIdSib2,
                     calAdcPath adcPath,
                     adcCal &calOut )
{
    selectAdcPath( adcPath );

    DsoErr err;

    int dac;
    float fAdc;
    err = iterDac( chIdRef, chIdRef, ADC_MID, dac, fAdc );
    if ( ERR_NONE != err ) return err;

    float ref;
    err = coreOffset( chIdRef, chIdRef, ADC_CORE_OFFSET_MID, ref );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_OFFSET;
    calOut.mCores[ chIdRef ].setOffset( ADC_CORE_OFFSET_MID );

    int offsetOut;
    err = iterOffset( chIdSib0, chIdRef, ref, offsetOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_OFFSET;
    calOut.mCores[ chIdSib0 ].setOffset( offsetOut );

    err = iterOffset( chIdSib1, chIdRef, ref, offsetOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_OFFSET;
    calOut.mCores[ chIdSib1 ].setOffset( offsetOut );

    err = iterOffset( chIdSib2, chIdRef, ref, offsetOut );
    if ( err != ERR_NONE ) return ERR_CAL_ADC_OFFSET;
    calOut.mCores[ chIdSib2 ].setOffset( offsetOut );

    return ERR_NONE;
}
DsoErr servCal::calAdcOffset_a()
{
    return calAdcOffset_x( 0, 1, 2, 3, cal_adc_a,
                           mCalAdc.mCalGp.mA );
}
DsoErr servCal::calAdcOffset_b()
{
    return calAdcOffset_x( 1, 0, 2, 3, cal_adc_a,
                           mCalAdc.mCalGp.mB );
}
DsoErr servCal::calAdcOffset_c()
{
    return calAdcOffset_x( 2, 0, 1, 3, cal_adc_a,
                           mCalAdc.mCalGp.mC );
}
DsoErr servCal::calAdcOffset_d()
{
    return calAdcOffset_x( 3, 0, 1, 2, cal_adc_a,
                           mCalAdc.mCalGp.mD );
}

