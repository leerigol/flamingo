#include "../servutility/servutility.h"
#include "calthread.h"

calThread::calThread( QObject *parent ) : QThread()
{
    Q_ASSERT( NULL != parent );
    m_pCal = dynamic_cast<servCal*>(parent);
    Q_ASSERT( NULL != m_pCal );
}

void calThread::run()
{
    Q_ASSERT( m_pCal != NULL );

    if(m_pCal->getCalType() ==  servCal::cal_probe)
    {
        int info = MSG_CHAN_PROBE_CAL;
        servGui::showInfo(info, Qt::white,-1,-1,10000);

        info = m_pCal->startCalProbe();

        servGui::showInfo(info, Qt::white,-1,-1,1000);
    }
    else
    {
        m_pCal->setProgTotal( 100 );
        m_pCal->setProgNow( 0 );

        m_pCal->setStatus( servCal::cal_running );

        m_pCal->preCal();

        m_pCal->calProc( 0, 99 );

        m_pCal->postCal();

        m_pCal->setStatus( servCal::cal_completed );
    }
}

