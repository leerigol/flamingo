#ifndef SERVCAL_EXT
#define SERVCAL_EXT

#include "../../phy/ch/iphypath.h"
#include "../../baseclass/checkedstream/ccheckedstream.h"

struct calPathData : public CCheckedStream
{
    dso_phy::pathCal mPayload;
};

#endif // SERVCAL_EXT

