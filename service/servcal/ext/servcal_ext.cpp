

#include "../../engine/base/enginecfg.h"

#include "../servcal.h"
#include "./servcal_path.h"


void servCal::preCalExt()
{
    logInfo("enter EXT");

    //! auto trig off
    IPhyCcu *pCcu;
    pCcu = getCcu();

    //! single
    pCcu->setMODE_auto_trig( 0 );
    pCcu->setCTRL_run_single( 1 );

    //! ext trig level
    IPhyPath *pExt;
    pExt = getExt();

    pExt->setDac( 0 );

    //! flush cache
    flushPhy();
}
void servCal::postCalExt()
{
    IPhyCcu *pCcu;
    pCcu = getCcu();

    pCcu->setMODE_auto_trig( 1 );
    pCcu->setCTRL_run_single( 0 );
    flushPhy();

    //! single run
    if ( getCcu()->getCcuStop() )
    {
        getCcu()->setCcuRun( 1 );
        getCcu()->flushWCache();
    }

    logInfo("exit EXT");
}

DsoErr servCal::extSave()
{
    int size = sizeof(mExtCal.mPayload);
    if ( size != mExtCal.save( mCalFileDbs[1].mExt,
                    &mExtCal.mPayload, size ) )
    {
        return ERR_CAL_WRITE_FAIL;
    }

    return ERR_NONE;
}
DsoErr servCal::extLoad( const QString &fileName )
{
    calPathData *pPath;

    pPath = new calPathData();
    if ( NULL == pPath )
    { return ERR_CAL_READ_FAIL; }

    int size = sizeof(pPath->mPayload);
    if ( pPath->load( fileName, &pPath->mPayload, size ) != size )
    {
        delete pPath;
        return ERR_CAL_READ_FAIL;
    }

    //! export
    mExtCal = *pPath;
    delete pPath;

    return ERR_NONE;
}

DsoErr servCal::calExtProc( int from, int steps )
{
    DsoErr err;

    preCalExt();

    err = calExt( from, steps );

    postCalExt();

    return err;
}

//! wait for longer time
#define stable_time 20
DsoErr servCal::calExt( int from, int steps )
{
    DsoErr err;
    quint32 riseDac, fallDac;

    //! ext trig
    TrigEdgeCfg edgeCfg;

    edgeCfg.setChan( ext );

    //! rise
    edgeCfg.setSlope( Trigger_Edge_Falling );
    postEngine( ENGINE_TRIGGER_EDGE_CFG, &edgeCfg );
    err = calExtRange( 0, 65535, &riseDac );
    if ( err != ERR_NONE ) return err;
    setProgNow( from + steps * 1 / 3 );

    //! fall
    edgeCfg.setSlope( Trigger_Edge_Rising );
    postEngine( ENGINE_TRIGGER_EDGE_CFG, &edgeCfg );
    err = calExtRange( 65535, 0, &fallDac );
    if ( err != ERR_NONE ) return err;
    setProgNow( from + steps * 2 / 3 );

    //! path cal
    mExtCal.mPayload.mGnd = ( riseDac + fallDac )/2;
    mExtCal.mPayload.mLsb = EXT_LSB;

    //! save
    err = extSave();
    if ( err != ERR_NONE )
    { return err; }
    setProgNow( from + steps * 3 / 3 );

    return ERR_NONE;
}

DsoErr servCal::calExtRange( quint32 a,
                             quint32 b,
                             quint32 *pDac )
{
    Q_ASSERT( NULL != pDac );
    Q_ASSERT( a != b );

    //! ext
    IPhyPath *pExt;
    pExt = getExt();

    //! scu
    LOG()<<getCcu()->getCcuStop();

    quint32 dacA, dacB, dacNext;
    quint32 dacTd;
    bool bTrigD;

    dacA = a;
    dacB = b;

    flushPhy();

    //! init
    dacNext = b;
    bTrigD = false;
    pExt->setDac( a );
    pExt->flushWCache();
    wait_n( stable_time );

    do
    {
        LOG()<<getCcu()->getCcuStop()<<a<<dacNext;

        //! single run
        if ( getCcu()->getCcuStop() )
        {
            getCcu()->setCcuRun( 1 );
            getCcu()->flushWCache();
        }

        //! out dac
        pExt->setDac( dacNext );
        pExt->flushWCache();
        wait_n( stable_time );

        LOG()<<getCcu()->getCcuStop();

        //! t'd
        if ( getCcu()->getCcuStop() )
        {
            dacB = dacNext;
            dacTd = dacNext;
            dacNext = ( dacA + dacB ) / 2;

            bTrigD = true;
        }
        //! not t'd
        else
        {
            dacA = dacNext;
            dacNext = ( dacA + dacB ) / 2;

            bTrigD = false;
        }

        //! error
        if ( a < b && (dacB - dacA) <= 1 )
        {
            if ( bTrigD )
            {
                *pDac = dacNext;
                pExt->setDac( dacNext );
                pExt->flushWCache( );
                LOG()<<dacNext;
                return ERR_NONE;
            }
            //! 100mv/25 = 40mv
            //! 40mv / 0.38mv = 100
            else if ( (dacTd - dacA) <= 100 )
            {
                *pDac = dacTd;
                pExt->setDac( dacTd );
                pExt->flushWCache( );
                LOG()<<dacNext;
                return ERR_NONE;
            }
            else
            { return ERR_CAL_EXT; }
        }
        else if ( a > b && (dacA-dacB) <= 1 )
        {
            if ( bTrigD )
            {
                *pDac = dacNext;
                pExt->setDac( dacNext );
                pExt->flushWCache( );
                LOG()<<dacNext;
                return ERR_NONE;
            }
            else if ( (dacTd - dacB) <= 100 )
            {
                *pDac = dacTd;
                pExt->setDac( dacTd );
                pExt->flushWCache( );
                LOG()<<dacNext;
                return ERR_NONE;
            }
            else
            { return ERR_CAL_EXT; }
        }
        //! iter
        else
        {}

        //! set base dac
        pExt->setDac( a );
        pExt->flushWCache();
        wait_n( stable_time );

    }while( 1 );

    return ERR_NONE;
}
