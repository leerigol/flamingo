#include "../servcal.h"
#include "../../servch/servch.h"
#include "calthread.h"
#include "../../servDG/servdg.h"
#include "../../servgui/servgui.h"

#define MAX_DAC_CMP_TIMES  1000
#define MAX_GND_VAVG_VAL   128.1
#define MIN_GND_VAVG_VAL   127.9

#define PROBE_ZERO_DEF        ((quint16)35388)
#define PROBE_ZERO_MIN        ((quint16)30000)
#define PROBE_ZERO_MAX        ((quint16)40000)

#define PROBE_OFFS_GAIN_DEF   ((quint16)760)
#define PROBE_OFFS_GAIN_MIN   ((quint16)720)
#define PROBE_OFFS_GAIN_MAX   ((quint16)800)

static quint32 getCalTime()
{
    //!校准时间
    QTime   time = QTime::currentTime();
    QDate   date = QDate::currentDate();

    quint32 dateTime = 0;
    dateTime  =  ( (time.hour()        &0x1f )<<(32-5)           )
                +( (time.minute()      &0x3f )<<(32-5-6)         )
                +( (time.second()      &0x3f )<<(32-5-6-6)       )
                +( ((date.year()-1980) &0x3f )<<(32-5-6-6-6)     )
                +( (date.month()       &0x0f )<<(32-5-6-6-6-4)   )
                +( (date.day()         &0x1f )<<(32-5-6-6-6-4-5) );
    return dateTime;
}

void servCal::enterCalProbe(Chan ch)
{
    m_probeCh = ch;
    setCalType(cal_probe);

    if ( m_pWorker == NULL )
    {
        m_pWorker = new calThread( this );
        Q_ASSERT( NULL != m_pWorker );

        connect( m_pWorker, SIGNAL(finished()),
                 this, SLOT(on_finished()) );
    }

    Q_ASSERT( m_pWorker != NULL );
    m_pWorker->start();
}

int servCal::startCalProbe()
{
    //!bak ch cfg
    int servId = m_probeCh-chan1+E_SERVICE_ID_CH1;
    DsoReal scalBak;
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_scale_ui_dsoreal,
                          scalBak);

    DsoReal offsBak;
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_offset_ui_dsoreal,
                          offsBak);

#if 1 //!start
    quint32 zeroVal  = 0;
    quint32 offGain  = 0;
    quint32 dateTime = 0;

    //!获取探头信息
    DsoErr err = ERR_NONE;
    pointer ptr = NULL;
    err = serviceExecutor::query( getServiceName(servId),
                                  servCH::cmd_probe_info_ptr,
                                  ptr);
    if(err != ERR_NONE)
    {
        return ERR_PROBE_CAL_FAILURE;
    }

    Q_ASSERT(ptr != NULL );
    OneWireProbeInfo *pProbePtr = static_cast<OneWireProbeInfo*>(ptr);
    Q_ASSERT(pProbePtr != NULL );

    //!校准零点
    if( !probeZeroValueCal(zeroVal) )
    {
        return ERR_PROBE_CAL_FAILURE;
    }

    /*
    //!增益校准
    if( !probeOffsGainCal(offGain) )
    {
        return ERR_PROBE_CAL_FAILURE;
    }
    */

    //! dac 校准
    if( !probeDacCal(offGain) )
    {
        return ERR_PROBE_CAL_FAILURE;
    }

    //!获取校准时间
    dateTime = getCalTime();

    //!保存探头信息
    //memcpy(pProbePtr->sn,    "RP789123456", sizeof("RP789123456"));
    //memcpy(pProbePtr->model, "RP7170",      sizeof("RP7170"));
    qDebug()<<__FILE__<<__LINE__<<zeroVal;
    pProbePtr->zeroVal     = zeroVal;

    pProbePtr->offGain[0]  = offGain>>16;
    pProbePtr->offGain[1]  = offGain&0xFFFF;

    pProbePtr->date[0]  = dateTime>>16;    //!时分秒
    pProbePtr->date[1]  = dateTime&0xFFFF; //!年月日

    postEngine( ENGINE_1WIRE_PROBE_INFO, pProbePtr);
#endif

    //!restore ch cfg
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_scale_ui_dsoreal,
                          scalBak);
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_offset_ui_dsoreal,
                          offsBak);
    //!apply
    applyProbeCalData(m_probeCh);

    return ERR_PROBE_CAL_SUCCEED;
}

void servCal::applyProbeCalData(Chan ch)
{
    int servId = ch-chan1+E_SERVICE_ID_CH1;

    //!获取探头信息
    DsoErr err = ERR_NONE;
    pointer ptr = NULL;
    err = serviceExecutor::query( getServiceName(servId),
                                  servCH::cmd_probe_info_ptr,
                                  ptr);
    if(err != ERR_NONE) return;
    Q_ASSERT(ptr != NULL );
    OneWireProbeInfo *pProbePtr = static_cast<OneWireProbeInfo*>(ptr);
    Q_ASSERT(pProbePtr != NULL );


    /*!----------------------检查初始校准参数是否错误太离谱--------------------*/
    quint16 u16_zero  =  pProbePtr->zeroVal;
    quint16 u16_gainL  =  pProbePtr->offGain[1];
    quint16 u16_gainH  =  pProbePtr->offGain[0];
    if( u16_zero != qBound(PROBE_ZERO_MIN, u16_zero, PROBE_ZERO_MAX) )
    {
        pProbePtr->zeroVal = PROBE_ZERO_DEF;
        servGui::showInfo(ERR_PROBE_REQUEST_CAL);
    }

    if( u16_gainL != qBound(PROBE_OFFS_GAIN_MIN, u16_gainL, PROBE_OFFS_GAIN_MAX) )
    {
        pProbePtr->offGain[1] = PROBE_OFFS_GAIN_DEF;
        servGui::showInfo(ERR_PROBE_REQUEST_CAL);
    }

    if( u16_gainH != 0 )
    {
        pProbePtr->offGain[0] = 0;
        servGui::showInfo(ERR_PROBE_REQUEST_CAL);
    }
    /*!--------------------------------------------------------------------*/

    IPhyPath *pProbePath  = getProbePath(ch-chan1);
    Q_ASSERT(NULL != pProbePath);
    pProbePath->setDac(pProbePtr->zeroVal);

    qDebug()<<__FILE__<<__LINE__
            <<getServiceName(servId)<<pProbePtr->zeroVal;

    int bias = 0;
    query(getServiceName(servId), MSG_CHAN_PROBE_BIAS, bias);
    post(servId, MSG_CHAN_PROBE_BIAS, bias);
}


bool servCal::probeZeroOffsCal(quint32 &/*value*/)
{
    return false;
}

bool servCal::probeOffsGainCal(quint32 &value)
{
    quint32 offsH = 0, offsL = 0;
    int       chId     =  m_probeCh-chan1+E_SERVICE_ID_CH1;
    QString   servName = getServiceName(chId);

    DsoReal inOffs(0,E_0);
    send(servName, servCH::cmd_offset_ui_dsoreal, inOffs);
    DsoReal inScal(500,E_N3);
    send(servName, servCH::cmd_scale_ui_dsoreal, inScal);

    send(serv_name_source1, MSG_DG_WAVE, servDG::DG_WAVE_DC);
    send(serv_name_source1, MSG_DG_IMPEDANCE, true);
    send(serv_name_source1, MSG_APP_DG, true);

    send(serv_name_source1, servDG::MSG_DG_CMD_OFFSET, (qlonglong)vv(1));
    if( !probeAdjustDacVal(offsH) )
    {return false;}

    send(serv_name_source1, servDG::MSG_DG_CMD_OFFSET, (qlonglong)vv(-1));
    if( !probeAdjustDacVal(offsL) )
    {return false;}

    value = ((offsH - offsL)*mProbeCal[m_probeCh-chan1].mPayload.mLsb)/vv(2);
    return true;
}

bool servCal::probeZeroValueCal( quint32 &value)
{
#if 0
    post(serv_name_source1, MSG_DG_WAVE, servDG::DG_WAVE_DC);
    post(serv_name_source1, servDG::MSG_DG_CMD_OFFSET, (qlonglong)vv(0));
    post(serv_name_source1, MSG_APP_DG, true);
#endif
    //!设置档位10mv
    int servId = m_probeCh-chan1+E_SERVICE_ID_CH1;
    DsoReal inOffs(0,E_0);
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_offset_ui_dsoreal,
                          inOffs);

    DsoReal inScal(10,E_N3);
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_scale_ui_dsoreal,
                          inScal);

    return probeAdjustDacVal(value);
}

bool servCal::probeAdjustDacVal(quint32 &value)
{
    float  fDacAvgVal = 0.0;
    int    nDac       = 0;
    int    chId       = (m_probeCh-chan1);

    Q_ASSERT( (chId>=0)&&(chId<=3) );

    IPhyPath *pProbePath  = getProbePath(chId);
    Q_ASSERT(NULL != pProbePath);

    for(int i=0; i<MAX_DAC_CMP_TIMES; i++)
    {
        //!设置offs
        pProbePath->setDac(nDac);
        pProbePath->flushWCache();
        //!判断平均值等于0
        getAvg(0, chId, fDacAvgVal); //!获取DAC值

        if( (MIN_GND_VAVG_VAL<fDacAvgVal) && (fDacAvgVal< MAX_GND_VAVG_VAL) )
        {
            qDebug()<<__FILE__<<__LINE__<<"fDac:"<<fDacAvgVal<<"nDac:"<<nDac;
            value = nDac;
            return true;
        }

        //!先快速逼近，在精细调整
        if(qAbs(fDacAvgVal-128) > 5 )
        {
            nDac -=  (fDacAvgVal-128);
        }
        else
        {
            nDac -=  ((fDacAvgVal-128)>0) ? 1 : (-1);
        }
    }
    qDebug()<<__FILE__<<__LINE__<<"fDac:"<<fDacAvgVal<<"nDac:"<<nDac;
    return false;
}

bool servCal::probeDacCal(quint32 &value)
{
    quint32 dac_h  = 0;
    quint32 dac_l  = 0;

    //!设置档位10mv
    int servId = m_probeCh-chan1+E_SERVICE_ID_CH1;

    DsoReal inScal(1,E_0);
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_scale_ui_dsoreal,
                          inScal);

    DsoReal inOffsH(2,E_0);
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_offset_ui_dsoreal,
                          inOffsH);
    if(!probeAdjustDacVal(dac_h))
    {
        return false;
    }

    DsoReal inOffsL(-2,E_0);
    serviceExecutor::send(getServiceName(servId),
                          servCH::cmd_offset_ui_dsoreal,
                          inOffsL);
    if(!probeAdjustDacVal(dac_l))
    {
        return false;
    }

    //!单位: xv/dac
    value = ((quint32)vv(4))/( qMax(dac_h, dac_l) - qMin(dac_h, dac_l) );
    qDebug()<<__FILE__<<__LINE__<<"v/dac:"<<value << dac_h << dac_l;

    return  true;
}

void servCal::setCalType(servCal::calType type)
{
    m_calType = type;
}

servCal::calType servCal::getCalType()
{
    return m_calType;
}
