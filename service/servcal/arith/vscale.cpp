#include "vscale.h"
#include "../../include/dsodbg.h"

bool vScaleLessThan( const vScale *s1, const vScale *s2)
{
    return s1->mVal < s2->mVal;
}

//! hz scale snr
bool hzvScaleSnr( const vScale *s1, const vScale *s2 )
{
    Q_ASSERT( s1->keys.count() == 5 );
    Q_ASSERT( s2->keys.count() == 5 );

    //! K min
    if ( s1->keys[0] < s2->keys[0] )
    { return true;}

    //! gs max
    if ( s1->keys[1] > s2->keys[1] )
    { return true; }

    //! atten
    if ( s1->keys[2] < s2->keys[2] )
    { return true; }

    //! sga4
    if ( s1->keys[3] > s2->keys[3] )
    { return true; }

    //! sga4 trim
    if ( s1->keys[4] > s2->keys[4] )
    { return true; }

    return false;
}

bool lzvScaleSnr( const vScale *s1, const vScale *s2 )
{
    Q_ASSERT( s1->keys.count() == 8 );
    Q_ASSERT( s2->keys.count() == 8 );

    int i = 0;

    //! sga1
    if ( s1->keys[i] < s2->keys[i] )
    { return true; }
    i++;

    //! sga1 trim
    if ( s1->keys[i] > s2->keys[i] )
    { return true; }
    i++;

    //! sga2
    if ( s1->keys[i] < s2->keys[i] )
    { return true; }
    i++;

    //! sga2 trim
    if ( s1->keys[i] > s2->keys[i] )
    { return true; }
    i++;

    //! sga3
    if ( s1->keys[i] < s2->keys[i] )
    { return true; }
    i++;

    //! sga3 trim
    if ( s1->keys[i] > s2->keys[i] )
    { return true; }
    i++;

    //! sga4
    if ( s1->keys[i] < s2->keys[i] )
    { return true; }
    i++;

    //! sga4 trim
    if ( s1->keys[i] > s2->keys[i] )
    { return true; }
    i++;

    return false;
}

void vCascade::cascade_mul( QList< vScale*> &pList,
                     QList< vMul> &muls,
                     QList< vScale*> &outList,
                     float ovRng )
{
    vScale *pScale;
    vMul mul;
    vScale *pNewScale;
    foreach( pScale, pList )
    {
        foreach( mul, muls )
        {
            //! over range
            if ( pScale->mVal > ovRng )
            {
                LOG_DBG()<<pScale->mVal;
            }
            else
            {
                pNewScale = new vScale();

                *pNewScale = *pScale;

                *pNewScale = (*pNewScale) * (mul);

                outList.append( pNewScale );
            }
        }
    }
}

void vCascade::cascade_div( QList< vScale*> &pList,
                     QList< vMul> &muls,
                     QList< vScale*> &outList,
                     float ovRng )
{
    vScale *pScale;
    vMul mul;
    vScale *pNewScale;
    foreach( pScale, pList )
    {
        if ( pScale->mVal > ovRng )
        {
            //qDebug()<<"scale:" << pScale->mVal;
        }
        else
        {
            foreach( mul, muls )
            {
                pNewScale = new vScale();

                *pNewScale = *pScale;

                *pNewScale = (*pNewScale) / (mul);

                outList.append( pNewScale );
            }
        }

        delete pScale;
    }
}

//! extract (scale - step/2, scale + step/2 ]
void vScales::extract( int &scale,
                       vScales &outList )
{
    int step = genvScale::getStep( scale );

    int lBound, uBound;

    lBound = scale - step/2;
    uBound = scale + step/2;

    vScale *pScale;

    foreach( pScale, *this )
    {
        Q_ASSERT( NULL != pScale );

        if ( pScale->miVal > lBound
             && pScale->miVal <= uBound )
        {
            outList.append( pScale );
        }
    }
}

void vScales::trim( QList<int> &allScale,
                    compare lessthan )
{
    int aScale;

    vScales candidateList;
    vScale *pvScale;

    foreach( aScale, allScale )
    {
        //! clear candidate
        candidateList.clear();

        //! get the range
        extract(aScale, candidateList );

        if ( candidateList.size() > 1 )
        {
            //! snr sort
            candidateList.sort( lessthan );

            //! find the match
            foreach( pvScale, candidateList )
            {
                //! add to head
                if ( pvScale->miVal == aScale )
                {
                    candidateList.removeOne( pvScale );
                    candidateList.prepend( pvScale );
                    break;
                }
            }

            //! set trim val
            candidateList[0]->trimVal = aScale;

            //! keep the first one
            for( int i = 1; i < candidateList.size(); i++ )
            {
                removeOne( candidateList[i] );

                delete candidateList[i];
            }
        }
        else if ( candidateList.size() == 1 )
        {
            //! set trim val
            candidateList[0]->trimVal = aScale;
        }
    }
}

//! fract scale to int scale
void vScales::normalize( int mul )
{
    vScale *pVScale;

    foreach( pVScale, *this )
    {
        pVScale->normalize( mul  );
    }
}

//! delete the obj
void vScales::gc()
{
    vScale *pVScale;

    foreach( pVScale, *this )
    {
        delete pVScale;
    }
}

void vScales::saveAs( const QString &fileName )
{
    //! file b
    QFile file( fileName );
    file.open( QIODevice::WriteOnly );
    QTextStream stream( &file );

    //! show dst scale
    vScale *pScale;
    foreach( pScale, *this )
    {
        pScale->exportOut( stream );
    }

    file.close();
}
