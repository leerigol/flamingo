#ifndef MAIN_H
#define MAIN_H

#include <QtCore>
#include <QtAlgorithms>

class genvScale
{
public:
    //! from 500uv
    static int getStep( int now )
    {
        int baseScales[]={1000,2000,5000 };
        int i;
        int e;

        for ( e = 1; e < 100000; e*= 10 )
        {
            for ( i = 0; i < 3; i++ )
            {
                if ( now < baseScales[i] * e )
                {
                    return baseScales[i] * e/100;
                }
            }
        }

        Q_ASSERT( false );
        return 100000;
    }

    //! from 500uv
    //! 1mv,2mv
    //! to 10v
    static void genScale( QList<int> &allScale,
                     int from=500, int to=15000000 )
    {
        int now = from;
        while( now <= to )
        {
            allScale.append( now );
            now += getStep( now );
        }
    }
};

class vMul
{
public:
    int key;
    float mul;

public:
    vMul(){}
    vMul(int k, float m)
    {
        key = k;
        mul = m;
    }

    void set( int k, float m )
    {
        key = k;
        mul = m;
    }
};

class vScale
{
public:
    QList<int> keys;
    float mVal;
    int miVal;

    int trimVal;    //! trimval
    int mBw;
public:
    vScale( float val = 0 )
    {
        mVal = val;
        miVal = 0;
        trimVal = 0;
        mBw = -1; //default open 20M
    }
    vScale( const vScale &a )
    {
        keys = a.keys;
        mVal = a.mVal;

        miVal = a.miVal;

        trimVal = 0;
        mBw = a.mBw;
    }

    void show()
    {
        QString str;
        int key;
        foreach( key, keys )
        {
            str += QString("%1 ").arg(key);
        }
    }

    void exportOut( QTextStream  &stream )
    {
        QString str;
        int key;
        foreach( key, keys )
        {
            str += QString("%1,").arg(key);
        }

        stream<<str<<mVal<<","<<trimVal << "," << miVal <<endl;
    }

    //! to integer
    void normalize( int mul = 1000000 )
    {
        miVal = mVal * mul;
    }

public:
    vScale &operator*( vMul &mul )
    {
        mVal *= mul.mul;
        keys.prepend( mul.key );

        return *this;
    }

    vScale &operator/( vMul &mul )
    {
        Q_ASSERT( mul.mul != 0 );
        mVal /= mul.mul;
        keys.prepend( mul.key );

        return *this;
    }
};
bool vScaleLessThan( const vScale *s1, const vScale *s2);
bool hzvScaleSnr( const vScale *s1, const vScale *s2 );
bool lzvScaleSnr( const vScale *s1, const vScale *s2 );

typedef bool (*compare)( const vScale *a, const vScale *b );
class vScales : public QList< vScale * >
{
public:
    vScales()
    {}

public:
    //! sort list
    void sort( compare lessthan )
    {
        qSort( begin(),
               end(),
               lessthan );
    }

protected:
    void extract( int &scale,
                  vScales &outScales );
public:

    void trim(
               QList<int> &scales,
               compare lessthan );

    void normalize( int mul = 1000000 );

    void gc();

    void saveAs( const QString &fileName );

};

class vCascade
{
public:
    static void cascade_mul( QList< vScale*> &pList,
                         QList< vMul> &muls,
                         QList< vScale*> &outList,
                         float ovRng = 0.070f );

    static void cascade_div( QList< vScale*> &pList,
                         QList< vMul> &muls,
                         QList< vScale*> &outList,
                         float ovRng = 0.070f );

};




#endif // MAIN_H

