
#include "../servcal.h"

#define DAC_STABLE_TIME     10  //! ms

#define DAC_CH_H    0
#define DAC_CH_L    1

void servCal::preCalLa()
{
    logInfo("enter LA");
}
void servCal::postCalLa()
{
    logInfo("exit LA");
}

DsoErr servCal::calLa( int from, int steps )
{
    IPhyPath *pH, *pL;
    quint32 laPattH, laPattL, laPatt;

    pH = getLaH();
    pL = getLaL();

    int dacH = 65535;
    int dacL = 0;

    //! check pattern
    {
        pL->setDac( dacH );
        pH->setDac( dacH, DAC_STABLE_TIME );
        laPattH = getLaPattern();

        pL->setDac( dacL );
        pH->setDac( dacL, DAC_STABLE_TIME );
        laPattL = getLaPattern();
        LOG_DBG()<<QString::number( laPattH, 16 )<<QString::number( laPattL, 16 );
        laPatt = laPattH ^ laPattL;
        if ( laPatt != 0xffff )
        { return ERR_CAL_LA_THRESHOLD; }
    }
    setProgNow( from + steps*1/3 );

    //! for the LW
    int h, l;
    {
        h = iterLa00( pL, dacH, dacL, 0xff, 0x00, dacL );
        l = iterLaFF( pL, dacH, dacL, 0xff, 0xff, dacH );

        mLaCal[0].mPayload.mGnd = (h + l)/2;
        mLaCal[0].mPayload.mLsb = LA_LSB;
        LOG_DBG()<<h<<l;
    }
    setProgNow( from + steps*2/3 );

    //! fot the HW
    {
        h = iterLa00( pH, dacH, dacL, 0xff00, 0x0000, dacL );
        l = iterLaFF( pH, dacH, dacL, 0xff00, 0xff00, dacH );

        mLaCal[1].mPayload.mGnd = (h + l)/2;
        mLaCal[1].mPayload.mLsb = LA_LSB;
        LOG_DBG()<<h<<l;
    }
    setProgNow( from + steps*3/3 );

    return ERR_NONE;
}

int servCal::iterLa00( IPhyPath *pPath,
            int range00,
            int rangeFF,
            quint32 mask,
            quint32 aim,
            quint32 base
            )
{
    int dacNow;
    quint32 patt;

    while( range00 > (rangeFF+1) )
    {
        pPath->setDac( base, DAC_STABLE_TIME );

        dacNow = (rangeFF + range00) / 2;

        pPath->setDac( dacNow, DAC_STABLE_TIME );

        patt = getLaPattern();
        patt = patt & mask;
//LOG_DBG()<<QString::number(patt,16)<<dacNow<<range00<<rangeFF;
        //! check
        if ( patt != aim )
        { rangeFF = dacNow; }
        else
        { range00 = dacNow; }
    }

    return dacNow;
}

int servCal::iterLaFF( IPhyPath *pPath,
            int range00,
            int rangeFF,
            quint32 mask,
            quint32 aim,
            quint32 base
            )
{
    int dacNow;
    quint32 patt;

    while( range00 > (rangeFF+1) )
    {
        pPath->setDac( base, DAC_STABLE_TIME );

        dacNow = (rangeFF + range00) / 2;

        pPath->setDac( dacNow, DAC_STABLE_TIME );

        patt = getLaPattern();
        patt = patt & mask;
//LOG_DBG()<<QString::number(patt,16)<<dacNow<<range00<<rangeFF;
        //! check
        if ( patt != aim )
        { range00 = dacNow; }
        else
        { rangeFF = dacNow; }
    }

    return dacNow;
}

DsoErr servCal::laSave()
{
    int size;

    //! LA L
    size = sizeof(mLaCal[0].mPayload);
    if ( size != mLaCal[0].save( mCalFileDbs[1].mLa[0],
                    &mLaCal[0].mPayload, size ) )
    {
        return ERR_CAL_WRITE_FAIL;
    }

    //! LA H
    size = sizeof(mLaCal[1].mPayload);
    if ( size != mLaCal[1].save( mCalFileDbs[1].mLa[1],
                    &mLaCal[1].mPayload, size ) )
    {
        return ERR_CAL_WRITE_FAIL;
    }

    LOG_DBG();

    return ERR_NONE;
}
DsoErr servCal::laLoad( const QString &fileNameL,
                        const QString &fileNameH)
{
    calPathData *pPath;

    pPath = new calPathData();
    if ( NULL == pPath )
    { return ERR_CAL_READ_FAIL; }

    int size;
    //! LA L
    size = sizeof(pPath->mPayload);
    if ( pPath->load( fileNameL, &pPath->mPayload, size ) != size )
    {
        delete pPath;
        return ERR_CAL_READ_FAIL;
    }
    mLaCal[0] = *pPath;

    //! LA H
    size = sizeof(pPath->mPayload);
    if ( pPath->load( fileNameH, &pPath->mPayload, size ) != size )
    {
        delete pPath;
        return ERR_CAL_READ_FAIL;
    }
    mLaCal[1] = *pPath;

    delete pPath;

//    LOG_DBG()<<mLaCal[0].mPayload.mGnd<<mLaCal[0].mPayload.mLsb;
//    LOG_DBG()<<mLaCal[1].mPayload.mGnd<<mLaCal[1].mPayload.mLsb;

    return ERR_NONE;
}

DsoErr servCal::calLaProc( int from, int steps )
{
    preCalLa();

    DsoErr err;
    err = calLa( from, steps );

    postCalLa();

    if ( err != ERR_NONE )
    { return err; }

    err = laSave();
    if ( err != ERR_NONE )
    { return err; }

    return ERR_NONE;
}
