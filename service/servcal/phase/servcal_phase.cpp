#include "../servcal.h"
#include "../../service/servgui/servgui.h"

#include "./servcal_phase.h"

#include "../servtrace/servtrace.h"

#define wait_phsae_time     50000   //! ms
#define wait_idle_time      10      //! ms

#define phase_0     (0.0f)
#define phase_90    (39.6f)     //! 1.1G
#define phase_180   (79.2f)
#define phase_270   (118.8f)

#define base_scale  500000      //! 500mv

DsoErr servCal::calPhaseProc( int from, int steps )
{
    DsoErr err;
    enterCalPhase();

    err = calPhase( from, steps );

    exitCalPhase();

    if ( err != ERR_NONE )
    {
        return err;
    }

    err = phaseSave();
    if ( err != ERR_NONE )
    {
        return err;
    }

    return err;
}

void servCal::enterCalPhase()
{
    logInfo("Enter phase");


    //! all to 50 ohm
    for ( int i = 0; i < 4; i++ )
    {
        serviceExecutor::post( E_SERVICE_ID_CH1 + i,
                               MSG_CHAN_SCALE_VALUE,
                               base_scale );

        serviceExecutor::post( E_SERVICE_ID_CH1 + i,
                               MSG_CHAN_OFFSET,
                               0 );

        serviceExecutor::post( E_SERVICE_ID_CH1 + i,
                               MSG_CHAN_IMPEDANCE,
                               IMP_50 );
    }

    //! acq peak
    serviceExecutor::post( E_SERVICE_ID_HORI,
                           MSG_HOR_ACQ_MODE_YT,
                           Acquire_Normal );
    //! cal source
    syncEngine( ENGINE_1_1G_OUT, true );

    //! phase request
    mPhaseRequest.setClient( getId(), servCal::CMD_PHASE_REQUEST );
    //! wait main loop idle
    QThread::sleep( 3 );

    //! all to LozFlt
    //! select b input
    switchISel();

    //! meta
    r_meta::CServMeta meta;
    int err;
    meta.getMetaVal("servcal/error/phase", err );
    calPhaseIter::setPhsaeError( err );
}

void servCal::exitCalPhase()
{
    //! 1.1G out
    syncEngine( ENGINE_1_1G_OUT, false );

    logInfo("exit phase");
}

#define vpp_min     120
//#define vpp_full_scale  240
#define vpp_full_scale  220  // add_by_zx: change for phase cal err

//! get the vpp and avg
DsoErr servCal::balanceFullScale()
{
    DsoErr err;

    err = balanceScale();
    if ( err != ERR_NONE )
    {
        return err;
    }

    err = balanceOffset();
    if ( err != ERR_NONE )
    {
        return err;
    }

    return ERR_NONE;
}

DsoErr servCal::balanceScale()
{
    DsoErr err;
    int scales[4];
    int vpps[4];

    //! read vpp
    switchISel();

    getCH(0)->waitIdle( 100 );

    getVpp( 0, vpps );

    //! check min
    for ( int i = 0; i < array_count(scales); i++ )
    {
        if ( vpps[i] < vpp_min )
        {
            logInfo(QString("%1 Line:%2---%3").arg(__FUNCTION__).arg(__LINE__).arg(vpps[i]));
            return ERR_CAL_PHASE;
        }
    }

    //! banlance scale
    for ( int i = 0; i < array_count(scales); i++ )
    {
        scales[i] = getCH(i)->getAfeScale() * vpps[i] / vpp_full_scale;
    }

    //! config scale
    for ( int i = 0; i < array_count(scales); i++ )
    {
        err = getCH( i )->setScale( scales[i] );
        if ( err != ERR_NONE )
        {
            logInfo(QString("%1 Line:%2").arg(__FUNCTION__).arg(__LINE__));
            return ERR_CAL_PHASE;
        }

        err = getCH( i )->setOffset( 0 );
        if ( err != ERR_NONE )
        {
            logInfo(QString("%1 Line:%2").arg(__FUNCTION__).arg(__LINE__));
            return ERR_CAL_PHASE;
        }
    }

    return ERR_NONE;
}
DsoErr servCal::balanceOffset()
{
    //! config offset
    int peaks[8];

    switchISel();

    getPeak( 0, peaks );

    int offset;
    for ( int i = 0; i < array_count(peaks)/2; i++ )
    {
        offset = ( ( peaks[i*2] + peaks[i*2+1] )/2 - ADC_MID );
        offset = offset*getCH(i)->getAfeScale()/adc_vdiv_dots;
        getCH(i)->setOffset( offset );

        LOG_DBG()<<offset<<getCH(i)->getAfeScale();
    }

    switchISel();

    //! verify vpp
    getPeak( 0, peaks );
    LOG_DBG()<<peaks[0]<<peaks[1]<<peaks[2]<<peaks[3]<<peaks[4]<<peaks[5]<<peaks[6]<<peaks[7];

    return ERR_NONE;
}

void servCal::switchISel()
{
    //! all to LozFlt
    //! select b input
    for ( int i = 0; i < 4; i++ )
    {
        m_pDsoPlatform->m_pPhy->mVGA[i].setISel( 1 );
        m_pDsoPlatform->m_pPhy->flushWCache();
    }
}

DsoErr servCal::calPhaseReset()
{
    dso_phy::CDsoPhy* m_pPhy = m_pDsoPlatform->m_pPhy;
    m_pPhy->mSpuGp.setCHN_DELAY_AB_A(0);
    m_pPhy->mSpuGp.setCHN_DELAY_AB_B(0);
    m_pPhy->mSpuGp.setCHN_DELAY_CD_C(0);
    m_pPhy->mSpuGp.setCHN_DELAY_CD_D(0);

    //close anti aliasing
    m_pPhy->mSpu.setCTRL_anti_alias(0);

     //Acquire_Normal
    m_pPhy->mSpu.setCTRL_peak( 0 );
    m_pPhy->mSpu.setCTRL_hi_res( 0 );
    m_pPhy->mSpu.setCTRL_trace_avg( 0 );

    m_pPhy->flushWCache();
    return ERR_NONE;
}

DsoErr servCal::calPhase( int from, int steps )
{
    DsoErr err;

    //! init the groups
    phaseGp phaseGroups;
    for ( int i = 0; i < array_count(mPhaseIters); i++ )
    {
        mPhaseIters[i].attachGp(&phaseGroups);
    }

    err = balanceFullScale();
    if ( err != ERR_NONE )
    {
        return err;
    }

    logInfo("4 phase");
    calPhaseReset();
    err = calPhase4s( 0 );//use default value
    setProgNow( from + steps*1/3 );
    if ( err != ERR_NONE )
    {
        return err;
    }

    logInfo("2 phase");
    calPhaseReset();
    err = calPhase2s( 0 );
    setProgNow( from + steps*2/3 );
    if ( err != ERR_NONE )
    {
        return err;
    }

    logInfo("1 phase");
    calPhaseReset();
    err = calPhase1s( 0 );
    setProgNow( from + steps*3/3 );
    if ( err != ERR_NONE )
    {
        return err;
    }

    return ERR_NONE;
}

DsoErr servCal::calPhase4s( int chipId )
{
    DsoErr err;

    for ( int i = dso_phy::IPhyADC::ch_abcd;
          i <= dso_phy::IPhyADC::ch_chabcd;
          i++ )
    {
        err = calPhase4( chipId,
                         (dso_phy::IPhyADC::eAdcInterleaveMode)i,
                         mCalAdcPhase[chipId].mPayload.smpDelays + i - dso_phy::IPhyADC::ch_abcd );

        if ( err != ERR_NONE )
        { return err; }
    }

    for ( int i = dso_phy::IPhyADC::ch_com_a;
          i <= dso_phy::IPhyADC::ch_com_d;
          i++ )
    {
        err = calPhase4( chipId,
                         (dso_phy::IPhyADC::eAdcInterleaveMode)i,
                         mCalAdcPhase[chipId].mPayload.smpDelays + i - dso_phy::IPhyADC::ch_abcd );

        if ( err != ERR_NONE )
        { return err; }
    }

    return ERR_NONE;
}
DsoErr servCal::calPhase2s( int chipId )
{
    DsoErr err;

    for ( int i = dso_phy::IPhyADC::ch_ac;
          i <= dso_phy::IPhyADC::ch_bd;
          i++ )
    {
        err = calPhase2( chipId,
                         (dso_phy::IPhyADC::eAdcInterleaveMode)i,
                         mCalAdcPhase[chipId].mPayload.smpDelays + i - dso_phy::IPhyADC::ch_abcd
                         );

        if ( err != ERR_NONE )
        { return err; }

        //logInfo( __FUNCTION__, QString::number(i) );
    }

    return ERR_NONE;
}
DsoErr servCal::calPhase1s( int chipId )
{
    DsoErr err;

    for ( int i = dso_phy::IPhyADC::ch_a;
          i <= dso_phy::IPhyADC::ch_d;
          i++ )
    {
        err = calPhase1( chipId,
                         (dso_phy::IPhyADC::eAdcInterleaveMode)i,
                         mCalAdcPhase[chipId].mPayload.smpDelays + i - dso_phy::IPhyADC::ch_abcd );

        if ( err != ERR_NONE )
        {
            return err;
        }

        //logInfo( __FUNCTION__, QString::number(i) );
    }

    return ERR_NONE;
}

//! 4 channel mode
DsoErr servCal::calPhase4( int chipId,
                           dso_phy::IPhyADC::eAdcInterleaveMode /*eMode*/,
                           coreSmpDelay *pCoreDelay )
{
    Q_ASSERT( NULL != pCoreDelay );

    IPhyADC *pAdc;
    pAdc = getAdc( chipId );

    //! getDefault delay
    adcSMPDLY dly;
    dly = pAdc->getDefaultSmpDly( 1 );

    //! use default
    for ( int i = 0; i < array_count(pCoreDelay->coreDelay); i++ )
    {
        pCoreDelay->coreDelay[i].payload = dly.payload;
    }

    return ERR_NONE;
}

DsoErr servCal::calPhase2( int chipId,
                           dso_phy::IPhyADC::eAdcInterleaveMode eMode,
                           coreSmpDelay *pCoreDelay )
{
    Q_ASSERT( NULL != pCoreDelay );

    changeAdcMode( eMode );

    DsoErr err;
    float refs[] = { phase_180,phase_0, phase_180 };
    err = iterPhase( chipId, pCoreDelay, refs, 2 );

    return err;
}

DsoErr servCal::calPhase1( int chipId,
                           dso_phy::IPhyADC::eAdcInterleaveMode eMode,
                           coreSmpDelay *pCoreDelay )
{
    Q_ASSERT( NULL != pCoreDelay );

    changeAdcMode( eMode );

    DsoErr err;
    float refs[] = { phase_180, phase_90, phase_270 };
    err = iterPhase( chipId, pCoreDelay, refs, 4  );

    return err;
}

DsoErr servCal::iterPhase( int chipId,
                           coreSmpDelay *pCoreDelay,
                           float refs[3],
                           int inteCnt  )
{
    Q_ASSERT( NULL != pCoreDelay );

    Q_ASSERT( inteCnt == 4 || inteCnt == 2 );

    //! 0.pre
    DsoErr err;
    IPhyADC *pAdc;

    pAdc = getAdc( chipId );

    //! 1. rst
    rstPhase();
    startTrace();

    for( int i = 0; i < array_count(mPhaseIters); i++ )
    {
        mPhaseIters[i].rst();
    }

    //! bypass
    if ( inteCnt == 2 )
    {
        mPhaseIters[1].mbEnd = true;
    }
    else
    {}

    //! 2. iter
    int iterCnt;
    phasePoint phasePts[ array_count(mPhaseIters) ];
    do
    {
        //! iter config
        iterCnt = 0;
        for ( int i = 0; i < array_count(mPhaseIters); i++ )
        {
            if ( mPhaseIters[i].hasNext( phasePts[i] ) )
            {
                iterCnt++;

                pAdc->setSmpDly( i+2, phaseToadcSMPDLY(phasePts[i]).payload );

                //LOG_DBG()<<i+2<<phasePts[i].mCoa<<phasePts[i].mInt<<phasePts[i].mFine;
            }
            else
            {}
        }

        //! read val
        if ( iterCnt < 1 )
        {
            break;
        }

        //! wait
        QThread::msleep( 1 );

        err = getPhase( refs, inteCnt );
        if ( err != ERR_NONE )
        {
            return err;
        }

        //! feedback
        for ( int i = 0; i < array_count(mPhaseIters); i++ )
        {
            //! has failed
            if ( mPhaseIters[i].mbFail )
            {
                continue;
            }

            //! has completed
            if ( mPhaseIters[i].mbEnd )
            {
            }
            else
            {
                mPhaseIters[i].iter( phasePts[i],
                                     mPhaseDeltaTimefs[i] );
            }
        }

    }while( 1 );

    //! 3.end
    stopTrace();

    //! 4.check end
    for( int i = 0; i < array_count(mPhaseIters); i++ )
    {
        if ( mPhaseIters[i].mbFail )
        {
            logInfo( QString("core%1: phase fail").arg(i+1) );
            return ERR_CAL_PHASE;
        }
    }

    //! 5. save
    for( int i = 0; i < array_count(pCoreDelay->coreDelay); i++ )
    {
        //LOG_DBG()<<QString::number( pAdc->getSmpDly(i+1).payload, 16 );

        pCoreDelay->coreDelay[i].payload = pAdc->getSmpDly(i+1).payload;
    }

    return ERR_NONE;
}


void servCal::changeAdcMode( dso_phy::IPhyADC::eAdcInterleaveMode eMode,
                             DsoWorkAim aim )
{
    getCcu()->setCcuRun( 0 );

    getRecEngine()->calAdcGroup( eMode, aim );
    getRecEngine()->flushWCache();

    getCcu()->setCcuRun( 1 );
    getCcu()->flushWCache();
}

void servCal::rstPhase()
{
    IPhyADC *pAdc;

    pAdc = getAdc();

    //! to default delay
    adcSMPDLY dly;
    dly = pAdc->getDefaultSmpDly( 1 );

    for ( int i = 2; i <= 4; i++ )
    {
        pAdc->setSmpDly( i, dly.payload );
    }

    pAdc->waitIdle( wait_idle_time );
}


DsoErr servCal::getPhase( float refs[3], int inteCnt  )
{
    mPhaseRequest.setAvgCount( 16 );
    mPhaseRequest.setRef( refs );
    mPhaseRequest.request(serv_name_trace, servTrace::cmd_request_phase );

    //! wait endl
    if ( !mPhaseRequest.waitAcked( wait_phsae_time ) )
    {
        return ERR_CAL_PHASE;
    }

    //! to fs
    for ( int i = 0; i < 3; i++ )
    {
        mPhaseDeltaTimefs[i] = phaseToTime( mPhaseRequest.mDeltas[i].getAverge() );
    }

    //! intecnt convert
    if ( inteCnt == 2 )
    {
        mPhaseDeltaTimefs[2] = mPhaseDeltaTimefs[2] - mPhaseDeltaTimefs[1];
        mPhaseDeltaTimefs[1] = 0;
    }
    else
    {}

    return ERR_NONE;
}

//! phase < 0: add time
//! phase > 0: sub time
qint32 servCal::phaseToTime( float phase )
{
    qint32 fs = phase*1000000 / 360 / 1.1;
    return fs;
}

adcSMPDLY servCal::phaseToadcSMPDLY( phasePoint &pt )
{
    adcSMPDLY dly;

    dly.smp_dly_coa = pt.mCoa;
    dly.smp_dly_int = pt.mInt;
    dly.smp_dly_fine = pt.mFine;

    return dly;
}

DsoErr servCal::phaseSave()
{
    int size = sizeof( mCalAdcPhase[0].mPayload );

    if ( size != mCalAdcPhase[0].save( mCalFileDbs[1].mAdcSamp[0],
                    &mCalAdcPhase[0].mPayload, size ) )
    { return ERR_CAL_WRITE_FAIL; }

    if ( size != mCalAdcPhase[1].save( mCalFileDbs[1].mAdcSamp[1],
                    &mCalAdcPhase[1].mPayload, size ) )
    { return ERR_CAL_WRITE_FAIL; }

    return ERR_NONE;
}
DsoErr servCal::phaseLoad( const QString &fileName0,
                           const QString &fileName1 )
{
    calPhaseData *pPhase;

    pPhase = new calPhaseData();
    if ( NULL == pPhase )
    { return ERR_CAL_READ_FAIL; }

    int size = sizeof(pPhase->mPayload);

    //! phase1
    {
        if ( pPhase->load( fileName0, &pPhase->mPayload, size ) != size )
        {
            delete pPhase;
            return ERR_CAL_READ_FAIL;
        }

        mCalAdcPhase[0] = *pPhase;
    }

    //! phase2
    {
        if ( pPhase->load( fileName1, &pPhase->mPayload, size ) != size )
        {
            delete pPhase;
            return ERR_CAL_READ_FAIL;
        }

        mCalAdcPhase[1] = *pPhase;
    }

    delete pPhase;

    return ERR_NONE;
}

//! delta end
DsoErr servCal::on_ack_phase_request( phaseRequest *pReq )
{
    Q_ASSERT( NULL != pReq );

    LOG_DBG()<<mPhaseRequest.mDeltas[0].getAverge()
            <<mPhaseRequest.mDeltas[1].getAverge()
            <<mPhaseRequest.mDeltas[2].getAverge();

    return ERR_NONE;
}

static int _calStat = 0;
DsoErr servCal::test_calPhase()
{
    int iVerify;
    int iCalSignal;

    r_meta::CServMeta meta;
    meta.getMetaVal("servcal/verify", iVerify );

    meta.getMetaVal("servcal/cal_signal", iCalSignal );

    DsoWorkAim aim;
    aim = ( iVerify == 1 ? aim_normal : aim_calibrate );

    if ( _calStat == 0 )
    {
        if ( iCalSignal > 0 )
        { enterCalPhase(); }

        servGui::showInfo("Phase output: ON.");
    }
    else if ( _calStat == 1 )
    {
        if ( iCalSignal > 0 )
        {
            switchISel();

            balanceFullScale();
        }

        changeAdcMode( dso_phy::IPhyADC::ch_abcd, aim );
        servGui::showInfo("4-CH");
    }
    //! ---- 2ch
    else if ( _calStat == 2 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_ac, aim );
        servGui::showInfo("2-CH:ac");
    }
    else if ( _calStat == 3 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_bc, aim );
        servGui::showInfo("2-CH:bc");
    }
    else if ( _calStat == 4 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_ad, aim );
        servGui::showInfo("2-CH:ad");
    }
    else if ( _calStat == 5 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_bd, aim );
        servGui::showInfo("2-CH:bd");
    }
    //! ---- 1ch
    else if ( _calStat == 6 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_a, aim );
        servGui::showInfo("1-CH:a");
    }
    else if ( _calStat == 7 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_b, aim );
        servGui::showInfo("1-CH:b");
    }
    else if ( _calStat == 8 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_c, aim );
        servGui::showInfo("1-CH:c");
    }
    else if ( _calStat == 9 )
    {
        changeAdcMode( dso_phy::IPhyADC::ch_d, aim );
        servGui::showInfo("1-CH:d");
    }
    else
    {
        if ( iCalSignal > 0 )
        { exitCalPhase(); }

        servGui::showInfo("Phase output: OFF.");
    }

    //! stat changed
    _calStat++;
    if ( _calStat>=11 )
    { _calStat = 0; }

    return ERR_NONE;
}

DsoErr servCal::test_iterPhase()
{
    float refs[]={0,0,0};
    IPhyADC *pAdc = getAdc();

    changeAdcMode( dso_phy::IPhyADC::ch_d );

    startTrace();

    for ( quint32 smpdly = 0xa000; smpdly < 0xa3ff; smpdly++ )
    {
        for ( int page = 2; page <=4; page++ )
        {
            pAdc->setSmpDly( page, smpdly );
        }

        pAdc->waitIdle( wait_idle_time );

        getPhase( refs );

//        LOG_DBG()<<smpdly<<mPhaseDeltaTimefs[0]<<mPhaseDeltaTimefs[1]<<mPhaseDeltaTimefs[2];
          LOG_DBG()<<smpdly<<mPhaseRequest.mDeltas[0].getAverge()<<mPhaseRequest.mDeltas[1].getAverge()<<mPhaseRequest.mDeltas[2].getAverge();
    }

    stopTrace();

    return ERR_NONE;
}


