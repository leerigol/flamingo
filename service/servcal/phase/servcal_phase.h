#ifndef SERVCAL_PHASE
#define SERVCAL_PHASE

#include "../../baseclass/checkedstream/ccheckedstream.h"

#include "../../phy/adc/adccal.h"
#include "../../phy/adc/iphyadc.h"
class calPhaseData : public CCheckedStream
{
public:
    dso_phy::adcSmpDelay mPayload;
};

class smpDlyValue
{
public:
    static quint32 fsToReg( qint32 fs, bool *pOk );
    static qint32  regToFs( quint32 reg );
public:
    smpDlyValue();
    int guessValue( qint32 fs );

public:
    dso_phy::adcSMPDLY mSmpDly;
    quint32 mFs;
};


class phasePoint
{
public:
    int mCoa, mInt, mFine;
};

class phaseSeg
{
public:
    phaseSeg( int coa, int inte, int fineMin=0x3ff, int fineMax=0x00 );
public:

public:
    bool contains( const phasePoint &seg );
public:
    int mCoa, mInt, mFineMax, mFineMin;
};

class phaseGp
{
public:
    phaseGp();
    ~phaseGp();
protected:
    void createSegs();
public:
    QList<phaseSeg *> mPhaseList;
};

class calPhaseIter
{
protected:
    static int _phase_error;
public:
    static void setPhsaeError( int err );

public:
    enum iterState
    {
        iter_seg_min,
        iter_seg_max,
        iter_seg_fine,
    };

public:
    calPhaseIter();
    void attachGp( phaseGp *pGp );

    void rst();

    bool hasNext( phasePoint &phase );
    void iter( phasePoint &phase, qint32 val );

protected:
    bool isEnd( qint32 fs );

public:
    qint32 mMin, mMax;
    bool mbEnd, mbFail;

    phaseGp *m_pPhaseGp;
    int mGpIndex;
    iterState mState;
};

#endif // SERVCAL_PHASE

