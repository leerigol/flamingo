
#include "./servcal_phase.h"

//#define phase_error         100         //! fs 50fs = 0.02
#define phase_error         250         //! fs 50fs = 0.02
                                        //! err = 1/4000
                                        //! 360*1.1/4000 = 0.099degree

                                        //! 360*0.197/4000 = 0.0177degree
                                        //! 249fs
#define init_seg_index      23
#define fine_min_step       20

int calPhaseIter::_phase_error = phase_error;
void calPhaseIter::setPhsaeError( int err )
{
    calPhaseIter::_phase_error = err;
}

calPhaseIter::calPhaseIter()
{
    rst();
}

void calPhaseIter::attachGp( phaseGp *pGp )
{
    Q_ASSERT( NULL != pGp );
    m_pPhaseGp = pGp;
}

void calPhaseIter::rst()
{
    mbFail = false;
    mbEnd = false;

    mState = iter_seg_min;
    mGpIndex = init_seg_index;
}

bool calPhaseIter::hasNext( phasePoint &phase )
{
    //! has completed
    if ( mbFail || mbEnd )
    { return false; }

    //! current seg
    phaseSeg *pSeg;
    pSeg = m_pPhaseGp->mPhaseList[mGpIndex];
    Q_ASSERT( NULL != pSeg );

    //! seg.min
    if ( mState == iter_seg_min )
    {
        phase.mCoa = pSeg->mCoa;
        phase.mInt = pSeg->mInt;
        phase.mFine = pSeg->mFineMin;
    }
    //! seg.max
    else if ( mState == iter_seg_max )
    {
        phase.mCoa = pSeg->mCoa;
        phase.mInt = pSeg->mInt;
        phase.mFine = pSeg->mFineMax;
    }
    //! fine
    else
    {
        phase.mCoa = pSeg->mCoa;
        phase.mInt = pSeg->mInt;

        phase.mFine = ( mMin + mMax )/2;
    }

    return true;
}

void calPhaseIter::iter( phasePoint &phase, qint32 val )
{
    //! completed
    mbEnd = isEnd( val );
    if ( mbEnd )
    {
        return;
    }

    //! seg.min
    if ( mState == iter_seg_min )
    {
        if ( val < 0 )
        {
            mState = iter_seg_max;
            LOG_DBG()<<"now for max";
        }
        else
        {
            mGpIndex -= 1;
        }
    }
    //! seg.max
    else if ( mState == iter_seg_max )
    {
        if ( val > 0 )
        {
            mState = iter_seg_fine;

            //! current seg
            phaseSeg *pSeg;
            pSeg = m_pPhaseGp->mPhaseList[mGpIndex];
            Q_ASSERT( NULL != pSeg );

            mMin = pSeg->mFineMin;
            mMax = pSeg->mFineMax;
        }
        else
        {
            mState = iter_seg_min;
            mGpIndex += 1;
        }
    }
    //! fine
    else
    {
        if ( val < 0 )
        {
            mMin = phase.mFine;
        }
        else if ( val > 0 )
        {
            mMax = phase.mFine;
        }
        else
        {}

        LOG_DBG()<<mMax<<mMin;
    }

    //! check fail
    if ( mGpIndex < 0 || mGpIndex >= m_pPhaseGp->mPhaseList.size() )
    {
        mbFail = true;
    }

    if ( mState == iter_seg_fine )
    {
        if ( qAbs(mMin-mMax)<=fine_min_step )
        {
            mbFail = true;
        }
    }
}

bool calPhaseIter::isEnd( qint32 fs )
{
    if ( fs >= -calPhaseIter::_phase_error && fs <= calPhaseIter::_phase_error )
    { return true; }
    else
    { return false; }
}

phaseSeg::phaseSeg( int coa, int inte, int theMin, int theMax )
{
    mCoa = coa;
    mInt = inte;
    mFineMin = theMin;
    mFineMax = theMax;
}

bool phaseSeg::contains( const phasePoint &seg )
{
    if ( seg.mCoa == mCoa
         && seg.mInt == mInt
         && seg.mFine >= mFineMin
         && seg.mFine <= mFineMax )
    { return true; }
    else
    { return false; }
}

phaseGp::phaseGp()
{
    createSegs();
}

phaseGp::~phaseGp()
{
    foreach( phaseSeg * pSeg, mPhaseList )
    {
        Q_ASSERT( NULL != pSeg );
        delete pSeg;
    }
}

void phaseGp::createSegs()
{
    phaseSeg *pSeg;

    //! 0,0,
    pSeg = new phaseSeg(0,0);
    Q_ASSERT( NULL != pSeg );
    mPhaseList.append( pSeg );

    //! 1,0,
    //! 1,x,
    //! 1,F,
    for ( int inte = 0; inte < 16; inte++ )
    {
        pSeg = new phaseSeg( 1, inte );
        Q_ASSERT( NULL != pSeg );
        mPhaseList.append( pSeg );
    }

    //! 2,0,
    //! 2,x,
    //! 2,0,
    for ( int inte = 15; inte >= 0; inte-- )
    {
        pSeg = new phaseSeg( 2, inte );
        Q_ASSERT( NULL != pSeg );
        mPhaseList.append( pSeg );
    }

    //! 3,0,
    pSeg = new phaseSeg( 3, 0 );
    Q_ASSERT( NULL != pSeg );
    mPhaseList.append( pSeg );
}


