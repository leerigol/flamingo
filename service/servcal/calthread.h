#ifndef CALTHREAD_H
#define CALTHREAD_H

#include <QtCore>
#include "servcal.h"
class calThread : public QThread
{
    Q_OBJECT

public:
    calThread( QObject *parent );

protected:
    virtual void run();

private:
    servCal *m_pCal;
};

#endif // CALTHREAD_H
