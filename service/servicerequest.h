#ifndef _SERVICEREQUEST_
#define _SERVICEREQUEST_

#include "../include/dsotype.h"
#include "../arith/savg.h"

using namespace DsoType;
class serviceRequest
{
public:
    serviceRequest();

public:
    int mServId;
    int mAckMsg;

    bool mAcked;
public:
    void setClient( int clientId, int ackMsg );
    bool waitAcked( int tmoms = 10000, int tickms = 100 );

    DsoErr request( const QString &name, int msg );
    DsoErr ackRequest();
};

class phaseRequest : public serviceRequest
{
public:
    phaseRequest();
public:
    void setDeltas( float deltas[3] );
    void setDeltas( float delta10,
                    float delta20,
                    float delta30 );

    void setRef( float refs[3] );
    void setRef( float ref1, float ref2, float ref3 );

    void setAvgCount( int avg );

public:
    DsoErr request( const QString &name, int msg );
    DsoErr ackRequest();

public:
    sAvg mDeltas[3];
    float mRef[3];
};


class averageRequest : public serviceRequest
{
public:
    averageRequest();
public:
    void setDeltas( float delta10);

public:
    DsoErr request( const QString &name, int msg );
    DsoErr ackRequest();

public:
    sAvg mDeltas[3];
    float mRef[3];
};

#endif // _SERVICEREQUEST_

