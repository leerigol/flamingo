/*
 * cryptoRigol.c
 *
 *  Created on: Feb 16, 2017
 *      Author: rigolee
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <QDebug>

#include "../../com/openssl-1.1.0d/include/openssl/opensslconf.h"
#include "../../com/openssl-1.1.0d/include/openssl/crypto.h"
#include "../../com/openssl-1.1.0d/include/openssl/evp.h"
#include "../../com/openssl-1.1.0d/include/openssl/bn.h"
#include "../../com/openssl-1.1.0d/include/openssl/ec.h"
#include "../../com/openssl-1.1.0d/include/openssl/engine.h"
#include "../../com/openssl-1.1.0d/include/openssl/err.h"
#include "../../com/openssl-1.1.0d/include/openssl/rand.h"
#include "../../com/openssl-1.1.0d/include/openssl/ec_lcl.h"


//#include <openssl/opensslconf.h> /* To see if OPENSSL_NO_EC is defined */
//#include <openssl/crypto.h>
//#include <openssl/evp.h>
//#include <openssl/bn.h>
//#include <openssl/ec.h>
//#ifndef OPENSSL_NO_ENGINE
//#include <openssl/engine.h>
//#endif
//#include <openssl/err.h>
//#include <openssl/rand.h>
//#ifndef PC_MODE
//#include <openssl/ec_lcl.h>
//#endif

#include "cryptoRigol.h"


int cryptoRigolGenMessage( char *message, int *messageLen, rigolInstrumentInfo *info )
{
    char *ptrbuff = message;

    int model_len, sn_len, option_len, version_len, floating_len, expire_len;
    model_len = strlen( info->m_pModel );
    sn_len = strlen( info->m_pSN );
    option_len = strlen( info->m_pOption );
    version_len = strlen( info->m_pVersion );
    floating_len = sizeof( info->m_s8Floating );
    expire_len = sizeof( info->m_s8ExpireTag );
    *messageLen = model_len + sn_len + option_len + version_len + floating_len + expire_len;

    memcpy( ptrbuff, info->m_pModel, model_len );
    ptrbuff += model_len;
    memcpy( ptrbuff, info->m_pSN, sn_len );
    ptrbuff += sn_len;
    memcpy( ptrbuff, info->m_pOption, option_len );
    ptrbuff += option_len;
    memcpy( ptrbuff, info->m_pVersion, version_len );
    ptrbuff += version_len;
    memcpy( ptrbuff, &(info->m_s8Floating), floating_len );
    ptrbuff += floating_len;
    memcpy( ptrbuff, &(info->m_s8ExpireTag), expire_len );
    ptrbuff += expire_len;

    return 0;
}


/***************************************/
/*
 *功能：仪器信息摘要的生成
 *参数说明：
         输出参数：digest-仪器信息摘要
         输入参数：instrumentInfo-仪器信息
                  out-log信息打印位置：stdout/file
 *返回值：正确-返回信息摘要长度
         错误-返回-1
 *
 */
/***************************************/
int cryptoRigolGenDigest(unsigned char *instrumentInfoDigest,
                         rigolInstrumentInfo* instrumentInfo )
{
    int ret = 0;
    char instrumentInfoBuff[1024] = {0};
    int infoLen = 0;
    EVP_MD_CTX *md_ctx;
    unsigned int dgst_len;

    if(instrumentInfoDigest == NULL || instrumentInfo ==NULL ){
        qDebug("  Input parameter errors,some is NULL!\n");
        ret = -1;
        goto ERROR_PARA;
    }

    cryptoRigolGenMessage( instrumentInfoBuff, &infoLen, instrumentInfo );

    #ifndef PC_MODE
       md_ctx = EVP_MD_CTX_new();
    #else
        md_ctx = EVP_MD_CTX_create();
    #endif

    //printf("instrumentInfoBuff = %s,strlen(instrumentInfoBuff)=%d\n",instrumentInfoBuff,strlen(instrumentInfoBuff));
    //生成仪器信息摘要
    if (!EVP_DigestInit(md_ctx, EVP_sha256())
        || !EVP_DigestUpdate( md_ctx, (const void *)instrumentInfoBuff, infoLen )
        || !EVP_DigestFinal(md_ctx, instrumentInfoDigest, &dgst_len))
    {
        qDebug(" Generate instrument info digest failed\n");
        ret = -1;
        goto ERROR_DIGEST;
    }
    ret = dgst_len;


ERROR_DIGEST:
    #ifndef PC_MODE
    EVP_MD_CTX_free(md_ctx);
    #else
    EVP_MD_CTX_destroy( md_ctx );
    #endif
ERROR_PARA:
    return ret;
}
/***************************************/
/*
 *功能：导入公钥
 *参数说明：
         输入参数：publicKey-仪器已存公钥
                  curveNameNid-曲线名称
                  out-log信息打印位置：stdout/file
 *返回值：正确-返回公钥信息
         错误-返回 NULL
 *
 */
/***************************************/

EC_KEY * cryptoRigolGetPubkey(unsigned char *publicKey, int len, int curveNameNid )
{
    if(publicKey == NULL ){
        return NULL;
    }
    if(curveNameNid <= 0){
        return NULL;
    }

    EC_KEY *ec_key = NULL;
    unsigned char *pp = publicKey;

    ec_key = EC_KEY_new_by_curve_name(curveNameNid);
    EC_KEY_generate_key(ec_key);

    ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``problem
    /*导入公钥*/
    ec_key = o2i_ECPublicKey(&ec_key,(const unsigned char**)&pp,len);
    if (ec_key == NULL)
    {
        EC_KEY_free(ec_key);
        return NULL;
    }
    else
    {
        return ec_key;
    }
}


EC_KEY * cryptoRigolImportPubKey(unsigned char *publicKey, int curveNameNid )
{
    if(publicKey == NULL ){
        return NULL;
    }
    if(curveNameNid <= 0){
        return NULL;
    }

    EC_KEY *ec_key = NULL;
    EC_POINT *keypoint = NULL;
// --------------------------------------------------------------
//    EC_POINT *keypointNew = NULL;
//    BIGNUM *tmp_bn = NULL;
//    int r = 0;
//    size_t buf_len = 0;
//    unsigned char *buf;
//    EC_POINT *ret;
//---------------------------------------------------------------------
    EC_GROUP *keygroup = NULL;
    if ( ( ec_key = EC_KEY_new() ) == NULL )
    {
        goto ERROR;
    }
    keygroup = EC_GROUP_new_by_curve_name( curveNameNid );
    if ( keygroup == NULL )
    {
        goto ERROR;
    }
    if ( 1 != EC_KEY_set_group( ec_key, keygroup ) )
    {
        goto ERROR;
    }
//    ec_key = EC_KEY_new_by_curve_name(curveNameNid);
//    if( ec_key == NULL)
//    {
//        goto ERROR;
//    }
//    if(!EC_KEY_generate_key(ec_key))
//    {
//        goto ERROR;
//    }



//    if (!BN_hex2bn(&tmp_bn, (const char *)publicKey))
//    {
//       r = BN_hex2bn(&tmp_bn, (const char *)publicKey);
//    }

//    if ((buf_len = BN_num_bytes(tmp_bn)) == 0)
//            return NULL;
//        buf = OPENSSL_malloc(buf_len);
//        if (buf == NULL)
//            return NULL;

//        if (!BN_bn2bin(tmp_bn, buf)) {
//            OPENSSL_free(buf);
//            return NULL;
//        }

//        if (keypoint == NULL) {
//            if ((ret = EC_POINT_new(keygroup)) == NULL) {
//                OPENSSL_free(buf);
//                return NULL;
//            }
//        } else
//            ret = keypoint;

//        if (!EC_POINT_oct2point(keygroup, ret, buf, buf_len, NULL)) {
//            if (ret != keypoint)
//                EC_POINT_clear_free(ret);
//                        OPENSSL_free(buf);
//                        return NULL;
//                    }

//                    OPENSSL_free(buf);




    keypoint = EC_POINT_hex2point( keygroup, (const char *)publicKey, keypoint, NULL );
    if ( keypoint == NULL )
    {
        goto ERROR;
    }

    if ( 1 != EC_KEY_set_public_key( ec_key, keypoint ) )
    {
        goto ERROR;
    }

    if (ec_key == NULL)
    {
        goto ERROR;
    }
    else
    {
        return ec_key;
    }
ERROR:
    EC_KEY_free(ec_key);
    return NULL;
}


/***************************************/
/*
 *功能：验证签名
 *参数说明：
         输入参数：dgst-仪器摘要信息
                  dgst_len-仪器摘要信息长度
                  sigbuf-已签名的仪器摘要信息
                  sig_len-已签名的仪器摘要信息长度
                  eckey-公钥
                  out-log信息打印位置：stdout/file
 *返回值：正确签名-返回0
         错误签名-返回-1
 *
 */
/***************************************/
int cryptoRigolVerifyDigest(const unsigned char *dgst, int dgst_len,
        const unsigned char *sigbuf, int sig_len, EC_KEY *eckey)
{
    if(dgst == NULL || sigbuf == NULL || eckey == NULL){
        qDebug( "  Input parameter errors,some is NULL!\n");
        return -2;
    }
    if (ECDSA_verify(0, dgst, dgst_len, sigbuf, sig_len, eckey) != 1) {
//        qDebug( " cryptoRigolVerifyDigest failed\n");
        return -1;
    }
    else{
        return 0;
    }
}



/***************************************/
/*
 *功能：验证签名RS
 *参数说明：
         输入参数：dgst-仪器摘要信息
                  dgst_len-仪器摘要信息长度
                  license-已签名的仪器摘要信息
                  eckey-公钥
                  out-log信息打印位置：stdout/file
 *返回值：正确签名-返回0
         错误签名-返回-1
 *
 */
/***************************************/
int cryptoRigolVerifyDigestRS(const unsigned char *dgst, int dgst_len,
        const char *license, EC_KEY *eckey)
{

    ECDSA_SIG *signature = ( ECDSA_SIG* )malloc( sizeof( ECDSA_SIG ) );
    BIGNUM *r = NULL;
    BIGNUM *s = NULL;
    BIGNUM *kinv = NULL, *rp = NULL;
    char licenseR[65] = {0};
    char licenseS[65] = {0};
    int licenseLen = strlen(license);

    if(dgst == NULL || license == NULL || eckey == NULL)
    {
        qDebug( "  Input parameter errors,some is NULL!\n");
        if(signature != NULL)
        {
            ECDSA_SIG_free(signature);
        }
        return -1;
    }
    //OpenSSL_add_all_algorithms();
    strncpy(licenseR,license,licenseLen/2);
    licenseR[64] = '\0';
    strncpy(licenseS,license+64,licenseLen/2);
    licenseS[64] = '\0';

    if (!ECDSA_sign_setup(eckey, NULL, &kinv, &rp))
    {
        ECDSA_SIG_free(signature);
        qDebug("Error:ECDSA_sign_setup\n");
        BN_clear_free(kinv);
        BN_clear_free(rp);
        return  -1;
    }

//    signature = ECDSA_do_sign_ex(dgst, dgst_len, kinv, rp, eckey);
//    if (signature == NULL)
//    {
//        qDebug("Error:ECDSA_do_sign_ex");
//        ECDSA_SIG_free(signature);
//        BN_clear_free(kinv);
//        BN_clear_free(rp);
//        return  -1;
//    }

    BN_hex2bn(&r,licenseR);
    signature->r = r;
    BN_hex2bn(&s,licenseS);
    signature->s = s;

    int ret = 0;
    if (ECDSA_do_verify(dgst, dgst_len, signature, eckey) != 1)
    {
        ret = -1;
    }

    if ( signature != NULL )
    {
        ECDSA_SIG_free(signature);
    }
    if ( kinv != NULL )
    {
        BN_clear_free(kinv);
    }
    if ( rp != NULL )
    {
        BN_clear_free(rp);
    }
    return ret;

}

/***************************************/
/*
 *功能：16进制字符串转化成byte[]
 *参数说明：
         输入参数：source-16进制字符串
                  bits-转化后的byte[]
                  sourceLen-16进制字符串长度
 *返回值：转化次数
 *
 */
/***************************************/
int changeHexTobyte(char source[],char bits[],int sourceLen) {
    if(source == NULL|| bits == NULL){
        printf("  Input parameter errors,some is NULL!\n");
        return -1;
    }
    if(sourceLen <= 0){
        printf( "  Input parameter errors,sourceLen <= 0!\n");
        return -1;
    }

    int i,n = 0;
    for(i = 0; i<sourceLen; i += 2) {
        if(source[i] >= 'A' && source[i] <= 'F')
            bits[n] = source[i] - 'A' + 10;
        else bits[n] = source[i] - '0';
        if(source[i + 1] >= 'A' && source[i + 1] <= 'F')
            bits[n] = (bits[n] << 4) | (source[i + 1] - 'A' + 10);
        else bits[n] = (bits[n] << 4) | (source[i + 1] - '0');
        ++n;
    }
    return n;
}

int sign_itecc(void)
{
    EVP_MD_CTX *md_ctx = NULL;
    EC_KEY *key = NULL;
    ECDSA_SIG *signature = NULL;
    unsigned char digest[32];
    unsigned int dgst_len = 0;
    char *rdec,*sdec;
    BIGNUM *kinv = NULL, *rp = NULL;
    const char message[] = "RSA5000,RSA2A123456789,PA3";
    char buf[128];
    EC_KEY *keyTest = NULL;

    #ifndef PC_MODE
    md_ctx = EVP_MD_CTX_new();
    #endif
    if(NULL == md_ctx)
    {

        printf("Error:EVP_MD_CTX_create");
    }
    md_ctx = EVP_MD_CTX_create();
    int i = 0;
    while(i<5)
    {
        i++;
        sprintf(buf,"%s,%d",message,i);
        qDebug("%s",buf);
        if (!EVP_DigestInit(md_ctx, EVP_sha256())
                || !EVP_DigestUpdate(md_ctx, (const void *)buf, 64)
                || !EVP_DigestFinal(md_ctx, digest, &dgst_len))
        {
            qInfo("Error:EVP_DigestInit");
        }
        qDebug("digest:%s",digest);

        if ((key = EC_KEY_new_by_curve_name(NID_secp256k1)) == NULL)
        {
            qInfo("Error:EC_KEY_new_by_curve_name");
        }
        EC_KEY_generate_key(key);
        int pubkey_len = i2o_ECPublicKey(key,NULL);
        unsigned char* pubkey =(unsigned char*) malloc(sizeof(unsigned char)*pubkey_len);
        unsigned char *rdwTemp = pubkey;
        int er = i2o_ECPublicKey(key,&pubkey);

//        keyTest = o2i_ECPublicKey( &key, (const unsigned char **)&rdwTemp, er );
//        if ( keyTest == NULL )
//        {
//            qDebug() << "nu!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
//        }
        qDebug("pubkey:%d,%s\n",er,rdwTemp);
        keyTest = cryptoRigolGetPubkey( rdwTemp, er, NID_secp256k1 );
        if ( keyTest == NULL )
        {
//            qDebug() << "nulllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll";
        }

        /* Use ECDSA_sign_setup to avoid use of ECDSA nonces */
        if (!ECDSA_sign_setup(key, NULL, &kinv, &rp))
        {
            qInfo("Error:ECDSA_sign_setup");
        }
        signature = ECDSA_do_sign_ex(digest, dgst_len, kinv, rp, key);
        if (signature == NULL)
        {
            qInfo("Error:ECDSA_do_sign_ex");
        }
        rdec = BN_bn2hex(signature->r );
        if(rdec)
           qDebug("R:%s",rdec);
        sdec = BN_bn2hex(signature->s );
            qDebug("S:%s",sdec);
        if(ECDSA_do_verify(digest,dgst_len,signature,key))
        {
            qDebug("sign is verified");
        }
        else
        {
            qDebug("Bad signature");
        }
    }
    return 0;
}

