#ifndef SERVLICENSE_H
#define SERVLICENSE_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "../../include/dsotype.h"

#define LICENSE_EXPTIME       (36*60)       //! Option Expire Time:36 Hour
#define FAIL_EXPTIME          (12*60)       //! Option fail install Limit expire time: 12 Hour

#define TRIAL_LICENSE_MAX      3     //! trial license limit to: 3

#define LICENSE_RETRY_TIMES    10   //! Option install fail number limit: 10
#define ALL_OPTLIC_INST_NUM    200  //! All Options install fail number limit: 200

#define FRAM_ID_FAILED_MAX  ((int)NumOfOpt + 1)  //! Id for All options install fail number
#define FRAM_ID_PUB_KEY     ((int)NumOfOpt + 2)         //! Id for PublicKey
#define FRAM_ID_OPTIONS     ((int)NumOfOpt + 3)         //! Id for All options License

const static OptType OptTypeList[NumOfOpt] =
{
    OPT_BW1T2,
    OPT_BW1T3,
    OPT_BW1T5,
    OPT_BW2T3,
    OPT_BW2T5,
    OPT_BW3T5,

    OPT_MSO,
    OPT_2RL,
    OPT_5RL,

    OPT_BND,

    OPT_COMP,
    OPT_EMBD,
    OPT_AUTO,
    OPT_FLEX,
    OPT_AUDIO,
    OPT_SENSOR,
    OPT_AERO,
    OPT_ARINC,
    OPT_DG,
    OPT_JITTER,
    OPT_MASK,
    OPT_PWR,
    OPT_DVM,
    OPT_CTR,
    OPT_EDK
};

#define NumOfBndOpt 15  // BND Opt enable 15 app options

const static OptType BndOptList[NumOfBndOpt] =
{
    OPT_COMP,
    OPT_EMBD,
    OPT_AUTO,
    OPT_FLEX,
    OPT_AUDIO,
    OPT_SENSOR,
    OPT_AERO,
    OPT_ARINC,
    OPT_DG,
    OPT_JITTER,
    OPT_MASK,
    OPT_PWR,
    OPT_DVM,
    OPT_CTR,

    OPT_EDK
};

const static QString g_OptName[NumOfOpt] =
{
    "BW1T2",
    "BW1T3",
    "BW1T5",
    "BW2T3",
    "BW2T5",
    "BW3T5",

    "MSO",
    "2RL",
    "5RL",

    "BND",

    "COMP",
    "EMBD",
    "AUTO",
    "FLEX",
    "AUDIO",
    "SENSOR",
    "AERO",
    "ARINC",
    "DG",
    "JITTER",
    "MASK",
    "PWR",
    "DVM",
    "CTR",

    "EDK"
};

enum LicenseType
{
    LicType_Fix,
    LicType_Float,
    LicTypeNum,
};

enum LicenseTime
{
    LicTime_Forever,
    LicTime_Limit,//12h
    LicTime_3M,//2
    LicTime_6M,//3
    LicTime_9M,//4
    LicTimeNum,
};

class COptInfo
{
public:
    COptInfo(OptType type, int id);

    void setName(OptType type);
    void setVer();
    void setLicense(QString l);
    void setLicenseValid(bool b);
    void setLicenseType(LicenseType f);
    void setLicenseTime(LicenseTime t);
    void deleteLicense();
    void setBndEable(bool b);
    bool getBndEable();

    OptType getType();
    QString getName();
    int     getInfoID();
    QString getVer();
    QString getLicense();
    LicenseType getLicenseType();
    LicenseTime getLicenseTime();
    int  getLicInstNum();
    void setLicInstNum();

    signed char getLicTypeVal();
    signed char getLicTimeVal();
    void setRunTime(int run);  //! run Unit: min
    int  getRemainTime();
    void resetRunTime();
    bool isExpire();
    bool isEnable();

    void set_Opt_Fail_InstNum(bool reset);
    int  get_Opt_Fail_InstNum();

    void set_Opt_Fail_InstTime();
    int  get_Opt_Fail_InstTime();

    static QMap<OptType, int> _OptInfoMap;
    static QMap<OptType, int> _InitOptInfoMap();

private:
    bool verifyExp();

private:
    OptType        m_Type;
    int            m_IdBase;  //! Base Id for Private Data save&load
    QString        m_Name;
    int            m_InfoID;  //! Msg ID for Option description
    QString        m_Ver;
    QString        m_License;
    bool           m_LicenseValid;
    LicenseType    m_LicenseType;
    LicenseTime    m_LicenseTime;
    int            m_RunTime;                  //! Option Run Time Unit:minite
    int            m_LimitTime_InstNum;        //! Limit_time License install number
    int            m_Fail_InstNum;             //! Option install fail number
    int            m_Fail_InstTime;            //! Option install fail time: minite
    bool           m_BND_Enable;
};

class servLicense : public serviceExecutor
{
    Q_OBJECT

protected:
    DECLARE_CMD()

public:
    servLicense( QString name, ServiceId eId = E_SERVICE_ID_LICENSE );
    enum
    {
        cmd_base = 0,
        cmd_get_OptEnable,
        cmd_active_Opt,
        cmd_get_OptInfo,
        cmd_delete_Opt,
        cmd_delete_AllOpt,
        cmd_Opt_Exp,
        cmd_Opt_Invalid,
        cmd_Opt_Active,

        cmd_set_key,

        cmd_USB_Insert,
        cmd_USB_Remove,
        cmd_USB_OptSetup,
        cmd_sys_vendor,
    };

public:
    void rst();
    void init();
    int  startup();
    virtual DsoErr start();
    virtual void registerSpy();

    void getSysInfo();
    void getOptList();
    void *getOptInfo();
    void verifyLicense();

    DsoErr cmd_ActiveOpt(QString opt);
    void cmd_deleteOpt(CArgument &optInfo);
    void cmd_deleteAllOpt();
    bool cmd_GetOptEnable(OptType type);
    DsoErr cmd_setKey(CArgument arg);

    int activeOpt( OptType optType, QString optLic);

    void onOptActive();
    void onOptExp();
    void onOptInvalid();

    int onGetOptInvalid(CArgument arg);

    int onInsertUSB(const QString&dirName);
    int onRemoveUSB(const QString&path);
    void onOptSetup();
    OptType getOptType(QString &name);

    /*系统信息中型号，序列号发生变更时，License应该同步该信息*/
    void onSysVendor();


    static void setLicensePath( const QString &licensePath);
    static void setKeyPath( const QString &keyPath);

    static QString _licensePath;
    static QString _keyPath;

private:
    void getPublicKeyStr(QString &key, QString &ec_id);
    bool verifyOptLic(COptInfo *opt, QString &key, QString &ec_id);
    void writeLicense(OptType optType, QString &optLic);
    void clearOptLicData();

    bool checkOptSetupFile(QString& path);

    bool check_BandWidthOpt(OptType optType);
    bool check_LimitLic_InstNum(OptType optType, QString optLic, QString &key, QString &ec_id);
    bool check_AllLic_InstNum();
    void set_AllLic_InstNum();
    void reset_AllLic_InstNum();
    void set_BndOpt_Enable(OptType optType, bool enble);

    void createTestLic();
    void testPrivateSave();
    void testClearPrivate();

    bool decodeKeyData( QByteArray &data, unsigned int *pxxteaKey = NULL);

public Q_SLOTS:
    int checkExpTime(int);

private:
    QString m_Model;
    QString m_SN;
    OptType m_CmdOptType;

    QMap<OptType,COptInfo*> m_OptList;
    QMap<OptType,COptInfo*> m_OptInfoList;

//    QTimer *OptTimer;
};

#endif // SERVLICENSE_H
