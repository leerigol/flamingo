#include <unistd.h>
#include "servlicense.h"
#include "../servutility/servutility.h"
#include "cryptoRigol.h"
#include "../servdso/sysdso.h"
#include "../servdso/servdso.h"
#include "../servgui/servgui.h"
#include "../servstorage/servstorage.h"
#include "../servhelp/servhelp.h"
#include "../service_name.h"
#include "../../com/crypt/xxtea.h"
#include "../servstorage/storage_api.h"

#define dbg_out()

IMPLEMENT_CMD( serviceExecutor, servLicense )
start_of_entry()
on_get_bool_int(  servLicense::cmd_get_OptEnable,      &servLicense::cmd_GetOptEnable ),

on_set_int_string(servLicense::cmd_active_Opt,         &servLicense::cmd_ActiveOpt),

on_get_pointer(   servLicense::cmd_get_OptInfo,        &servLicense::getOptInfo),
on_set_void_arg(  servLicense::cmd_delete_Opt,         &servLicense::cmd_deleteOpt),
on_set_void_void( servLicense::cmd_delete_AllOpt,      &servLicense::cmd_deleteAllOpt),

on_set_int_arg(   servLicense::cmd_set_key,            &servLicense::cmd_setKey),

on_set_void_void( servLicense::cmd_Opt_Active,         &servLicense::onOptActive),
on_set_void_void( servLicense::cmd_Opt_Exp,            &servLicense::onOptExp),
on_set_void_void( servLicense::cmd_Opt_Invalid,        &servLicense::onOptInvalid),

on_set_int_string( servLicense::cmd_USB_Insert,        &servLicense::onInsertUSB),
on_set_int_string( servLicense::cmd_USB_Remove,        &servLicense::onRemoveUSB),
on_set_void_void(  servLicense::cmd_USB_OptSetup,      &servLicense::onOptSetup),

on_set_void_void ( servLicense::cmd_sys_vendor,        &servLicense::onSysVendor),

on_set_void_void( CMD_SERVICE_RST,                     &servLicense::rst ),
on_set_void_void( CMD_SERVICE_INIT,                    &servLicense::init ),
on_set_int_void ( CMD_SERVICE_STARTUP,                 &servLicense::startup ),
//on_set_int_void ( CMD_SERVICE_ACTIVE,                &servLicense::setActive ),
on_set_int_int(   CMD_SERVICE_TIMEOUT,                 &servLicense::checkExpTime ),
end_of_entry()

const static QString ECC_CURVENAME[ eEndOfEccCurve ] =
{
    "secp256k1",
    "brainpoolP256r1",
    "brainpoolP256t1"
};

QMap<OptType, int> COptInfo::_OptInfoMap = COptInfo::_InitOptInfoMap();

QMap<OptType, int> COptInfo::_InitOptInfoMap()
{
    QMap<OptType, int> optMap;
    optMap[OPT_BW1T2] = MSG_OPT_INFO_BW1T2;
    optMap[OPT_BW1T3] = MSG_OPT_INFO_BW1T3;
    optMap[OPT_BW1T5] = MSG_OPT_INFO_BW1T5;
    optMap[OPT_BW2T3] = MSG_OPT_INFO_BW2T3;
    optMap[OPT_BW2T5] = MSG_OPT_INFO_BW2T5;
    optMap[OPT_BW3T5] = MSG_OPT_INFO_BW3T5;

    optMap[OPT_MSO] = MSG_OPT_INFO_MSO;
    optMap[OPT_2RL] = MSG_OPT_INFO_2RL;
    optMap[OPT_5RL] = MSG_OPT_INFO_5RL;

    optMap[OPT_BND] = MSG_OPT_INFO_BND;

    optMap[OPT_COMP]   = MSG_OPT_INFO_COMP;
    optMap[OPT_EMBD]   = MSG_OPT_INFO_EMBD;
    optMap[OPT_AUTO]   = MSG_OPT_INFO_AUTO;
    optMap[OPT_FLEX]   = MSG_OPT_INFO_FLEX;
    optMap[OPT_AUDIO]  = MSG_OPT_INFO_AUDIO;
    optMap[OPT_SENSOR] = MSG_OPT_INFO_SENSOR;
    optMap[OPT_AERO]   = MSG_OPT_INFO_AERO;
    optMap[OPT_ARINC]  = MSG_OPT_INFO_ARINC;
    optMap[OPT_DG]     = MSG_OPT_INFO_DG;
    optMap[OPT_JITTER] = MSG_OPT_INFO_JITTER;
    optMap[OPT_MASK]   = MSG_OPT_INFO_MASK;
    optMap[OPT_PWR]    = MSG_OPT_INFO_PWR;
    optMap[OPT_DVM]    = MSG_OPT_INFO_DVM;
    optMap[OPT_CTR]    = MSG_OPT_INFO_CTR;
    optMap[OPT_EDK]    = MSG_OPT_INFO_EDK;

    return optMap;
}

COptInfo::COptInfo(OptType type, int id)
{
    m_Type = type;
    m_IdBase = id;
    setName(type);
    setVer();
    m_License = "";
    m_LicenseValid = false;
    m_LicenseType = LicType_Fix;
    m_LicenseTime = LicTime_Forever;
    m_RunTime = 0;
    m_LimitTime_InstNum = 0;
    m_Fail_InstNum = 0;
    m_Fail_InstTime = 0;

    m_BND_Enable = false;

    m_InfoID = _OptInfoMap.value(type);
}

void COptInfo::setName(OptType type)
{
    m_Name = g_OptName[type];
}

void COptInfo::setVer()
{
    m_Ver = "1.0";
}

void COptInfo::setLicense(QString l)
{
    m_License = l;
}

void COptInfo::deleteLicense()
{
    m_License = "";
    m_LicenseValid = false;
}

void COptInfo::setBndEable(bool b)
{
    m_BND_Enable = b;
}

bool COptInfo::getBndEable()
{
    return m_BND_Enable;
}

void COptInfo::setLicenseValid(bool b)
{
    m_LicenseValid = b;
}

void COptInfo::setLicenseType(LicenseType f)
{
    m_LicenseType = f;
}

void COptInfo::setLicenseTime(LicenseTime t)
{
    m_LicenseTime = t;
}

OptType COptInfo::getType()
{
    return m_Type;
}

QString COptInfo::getName()
{
    return m_Name;
}

int COptInfo::getInfoID()
{
    return m_InfoID;
}

QString COptInfo::getVer()
{
    return m_Ver;
}

QString COptInfo::getLicense()
{
    return m_License;
}

LicenseType COptInfo::getLicenseType()
{
    return m_LicenseType;
}

LicenseTime COptInfo::getLicenseTime()
{
    return m_LicenseTime;
}

signed char COptInfo::getLicTypeVal()
{
    signed char floatVal;
    if( m_LicenseType == LicType_Float )
    {
        floatVal = 0x01;
    }
    else if( m_LicenseType == LicType_Fix )
    {
        floatVal = 0x00;
    }
    else
    {
        floatVal = 0x00;
    }

    return floatVal;
}

signed char COptInfo::getLicTimeVal()
{
    signed char expTagVal = 0x01;
    /*
    if( m_LicenseTime == LicTime_Limit)
    {
        expTagVal = 0x01;
    }
    else if( m_LicenseTime == LicTime_Forever)
    {
        expTagVal = 0x00;
    }
    else
    {
        expTagVal = 0x01;
    }*/
    if( m_LicenseTime < LicTimeNum )
    {
        expTagVal = (signed char)m_LicenseTime;
    }
    return expTagVal;
}

bool COptInfo::isEnable()
{
    bool enable = false;

    if( m_BND_Enable)
    {
        enable = true;
    }
    else
    {
        if( m_LicenseValid)
        {
            if( m_LicenseType == LicType_Fix)
            {
                enable = verifyExp();
            }
            else
            {
                enable = false;
            }
        }
        else
        {
            enable = false;
        }
    }

    return enable;
}

bool COptInfo::verifyExp()
{
    bool valid = false;

    if( m_LicenseTime == LicTime_Forever)
    {
        valid = true;
    }
    else if( m_LicenseTime == LicTime_Limit)
    {
        valid = !(isExpire());
    }
    else
    {
        valid = false;
    }

    return valid;
}

void COptInfo::setRunTime(int run)   //! run Unit: min
{

    //! load m_RunTime, m_LimitTime_InstNum, m_Fail_InstNum
    int optId = m_IdBase + (int)m_Type;
    int timeVal = 0;
    if( servDso::getLen( optId) != sizeof(timeVal))
    {
        m_RunTime = 0;                     //! Default m_RunTime: 0
        m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
        m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
    }
    else
    {
        int loadSize = servDso::load( optId, &timeVal, sizeof(timeVal));
        Q_ASSERT( loadSize == sizeof(timeVal));
        if( loadSize == sizeof(timeVal))
        {
            m_RunTime = timeVal&0xffff;               //! m_RunTime: Lo 2 Byte
            m_LimitTime_InstNum = (timeVal>>16)&0xf;  //! m_LimitTime_InstNum: Hi 2 Byte
            m_Fail_InstNum = timeVal>>24;             //! m_Fail_InstNum: Hi 1 Byte
        }
        else
        {
            m_RunTime = 0;                     //! Default m_RunTime: 0
            m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
            m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
        }
    }

    m_RunTime += run; //! Add run time (Unit:min)

    //! save m_RunTime, m_LimitTime_InstNum, m_Fail_InstNum
    timeVal = m_RunTime + (m_LimitTime_InstNum<<16) + (m_Fail_InstNum<<24);
    int saveSize = servDso::save( optId, &timeVal, sizeof(timeVal));
    Q_ASSERT( saveSize == sizeof(timeVal));
}

int COptInfo::getRemainTime()
{
    int rtime = LICENSE_EXPTIME - m_RunTime;
    return rtime;
}

void COptInfo::resetRunTime()
{
    //! load m_LimitTime_InstNum, m_Fail_InstNum
    int optId = m_IdBase + (int)m_Type;
    int timeVal = 0;
    if( servDso::getLen( optId) != sizeof(timeVal))
    {
        m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
        m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
    }
    else
    {
        int loadSize = servDso::load( optId, &timeVal, sizeof(timeVal));
        Q_ASSERT( loadSize == sizeof(timeVal));
        if( loadSize == sizeof(timeVal))
        {
            m_LimitTime_InstNum = (timeVal>>16)&0xf;  //! m_LimitTime_InstNum: Hi 2 Byte
            m_Fail_InstNum = timeVal>>24;             //! m_Fail_InstNum: Hi 1 Byte
        }
        else
        {
            m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
            m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
        }
    }

    m_RunTime = 0;  //! reset RunTime

    //! save m_RunTime & m_LimitTime_InstNum
    timeVal = m_RunTime + (m_LimitTime_InstNum<<16) + (m_Fail_InstNum<<24);
    int saveSize = servDso::save( optId, &timeVal, sizeof(timeVal));
    Q_ASSERT( saveSize == sizeof(timeVal));
}

bool COptInfo::isExpire()
{
    if( m_RunTime >= LICENSE_EXPTIME)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void COptInfo::setLicInstNum()
{
    //! load m_RunTime, m_LimitTime_InstNum, m_Fail_InstNum
    int optId = m_IdBase + (int)m_Type;
    int timeVal = 0;
    if( servDso::getLen( optId) != sizeof(timeVal))
    {
        m_RunTime = 0;                     //! Default m_RunTime: 0
        m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
        m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
    }
    else
    {
        int loadSize = servDso::load( optId, &timeVal, sizeof(timeVal));
        Q_ASSERT( loadSize == sizeof(timeVal));
        if( loadSize == sizeof(timeVal))
        {
            m_RunTime = timeVal&0xffff;               //! m_RunTime: Lo 2 Byte
            m_LimitTime_InstNum = (timeVal>>16)&0xf;  //! m_LimitTime_InstNum: Hi 2 Byte
            m_Fail_InstNum = timeVal>>24;             //! m_Fail_InstNum: Hi 1 Byte
        }
        else
        {
            m_RunTime = 0;                     //! Default m_RunTime: 0
            m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
            m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
        }
    }

    m_LimitTime_InstNum += 1;  //! Increase LimitTime Lic install number

    //! save m_RunTime & m_LimitTime_InstNum
    timeVal = m_RunTime + (m_LimitTime_InstNum<<16) + (m_Fail_InstNum<<24);
    int saveSize = servDso::save( optId, &timeVal, sizeof(timeVal));
    Q_ASSERT( saveSize == sizeof(timeVal));

}

int COptInfo::getLicInstNum()
{
    //! load m_LimitTime_InstNum
    int optId = m_IdBase + (int)m_Type;
    int timeVal = 0;
    if( servDso::getLen( optId) != sizeof(timeVal))
    {
        m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
    }
    else
    {
        int loadSize = servDso::load( optId, &timeVal, sizeof(timeVal));
        Q_ASSERT( loadSize == sizeof(timeVal));
        if( loadSize == sizeof(timeVal))
        {
            m_LimitTime_InstNum = (timeVal>>16)&0xf;  //! m_LimitTime_InstNum: Hi 2 Byte
        }
        else
        {
            m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
        }
    }

    return m_LimitTime_InstNum;
}

void COptInfo::set_Opt_Fail_InstNum(bool reset)
{
    //! load m_RunTime, m_LimitTime_InstNum, m_Fail_InstNum
    int optId = m_IdBase + (int)m_Type;
    int timeVal = 0;
    if( servDso::getLen( optId) != sizeof(timeVal))
    {
        m_RunTime = 0;                     //! Default m_RunTime: 0
        m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
        m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
    }
    else
    {
        int loadSize = servDso::load( optId, &timeVal, sizeof(timeVal));
        Q_ASSERT( loadSize == sizeof(timeVal));
        if( loadSize == sizeof(timeVal))
        {
            m_RunTime = timeVal&0xffff;               //! m_RunTime: Lo 2 Byte
            m_LimitTime_InstNum = (timeVal>>16)&0xf;  //! m_LimitTime_InstNum: Hi 2 Byte
            m_Fail_InstNum = timeVal>>24;             //! m_Fail_InstNum: Hi 1 Byte
        }
        else
        {
            m_RunTime = 0;                     //! Default m_RunTime: 0
            m_LimitTime_InstNum = 0;           //! Default m_LimitTime_InstNum: 0
            m_Fail_InstNum = 0;                //! Default m_Fail_InstNum: 0
        }
    }

    if(reset)
    {
        m_Fail_InstNum = 0;
        m_Fail_InstTime = 0;
    }
    else
    {
        m_Fail_InstNum += 1;  //! Increase Opt fail install number
    }

    //! save m_RunTime, m_LimitTime_InstNum, m_Fail_InstNum
    timeVal = m_RunTime + (m_LimitTime_InstNum<<16) + (m_Fail_InstNum<<24);

    int saveSize = servDso::save( optId, &timeVal, sizeof(timeVal));
    Q_ASSERT( saveSize == sizeof(timeVal));
}

int  COptInfo::get_Opt_Fail_InstNum()
{
    //! load m_Fail_InstNum
    int optId = m_IdBase + (int)m_Type;
    int timeVal = 0;
    if( servDso::getLen( optId) != sizeof(timeVal))
    {
        m_Fail_InstNum = 0;           //! Default m_Fail_InstNum: 0
    }
    else
    {
        int loadSize = servDso::load( optId, &timeVal, sizeof(timeVal));
        Q_ASSERT( loadSize == sizeof(timeVal));
        if( loadSize == sizeof(timeVal))
        {
            m_Fail_InstNum = timeVal>>24; //! m_Fail_InstNum: Hi 1 Byte
        }
        else
        {
            m_Fail_InstNum = 0;           //! Default m_Fail_InstNum: 0
        }
    }


    return m_Fail_InstNum;
}

void COptInfo::set_Opt_Fail_InstTime()
{
    m_Fail_InstTime += 1;  //! Increase Opt fail install time
}

int  COptInfo::get_Opt_Fail_InstTime()
{
    return m_Fail_InstTime;
}

QString servLicense::_licensePath;
QString servLicense::_keyPath;

servLicense::servLicense( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    // For test
//    createTestLic();

    m_Model = "";
    m_SN    = "";
    m_CmdOptType = OPT_UNKNOWN;

    m_OptList.clear();
    m_OptInfoList.clear();
}

void servLicense::setLicensePath(const QString &licensePath)
{
    _licensePath = licensePath;
}

void servLicense::setKeyPath( const QString &keyPath)
{
    _keyPath = keyPath;
}

void servLicense::rst()
{

}

void servLicense::init()
{
    clearOptLicData();

    getOptList();
}

int servLicense::startup()
{
    static bool bCheck = false;
    if( !bCheck )
    {
        onSysVendor();
        bCheck = true;
    }

    return ERR_NONE;
}

DsoErr servLicense::start()
{
#ifndef _DEBUG
    startTimer(60000,0,timer_repeat);
#else
    startTimer(1000,0,timer_repeat);
#endif
    return ERR_NONE;
}

//static int _testTime = 5;
struct timeOpt
{
    OptType opt;
    int from, to;
};
timeOpt _opts[] =
{ { OPT_MSO, 10, 20},
  { OPT_2RL, 20, 30},
  { OPT_5RL, 30, 40},
};

int servLicense::checkExpTime(int)
{
    bool isExp = false;

    bool needRedraw = false;

    //qDebug() << "time:" << QDateTime::currentDateTime();
    foreach( OptType type, m_OptList.keys())
    {
        COptInfo *opt = m_OptList.value(type);
        if(  (opt->getBndEable() == false)
           &&(opt->isEnable())
           &&(opt->getLicenseTime() == LicTime_Limit) )
        {
            opt->setRunTime(1);
            if( opt->isExpire())
            {
                isExp = true;
            }
            needRedraw = true;
        }

        //! 选件安装失败次数超过10次后，12小时后可重新尝试安装
        if( opt->get_Opt_Fail_InstNum() >= LICENSE_RETRY_TIMES )
        {
            opt->set_Opt_Fail_InstTime();
            if( opt->get_Opt_Fail_InstTime() >= FAIL_EXPTIME)
            {
                opt->set_Opt_Fail_InstNum( true);  //! reset Opt fail install number
            }
        }
    }

    if( isExp)
    {
        async( servLicense::cmd_Opt_Exp, 0);
    }
    if( needRedraw )
    {
        serviceExecutor::post(serv_name_help,
                              servHelp::cmd_opt_redraw, 0);
    }

    return ERR_NONE;
}

void servLicense::registerSpy()
{
    spyOn( E_SERVICE_ID_STORAGE,
           MSG_STORAGE_USB_INSERT,
           servLicense::cmd_USB_Insert);

    spyOn( E_SERVICE_ID_STORAGE,
           MSG_STORAGE_USB_REMOVE,
           servLicense::cmd_USB_Remove);

    spyOn( E_SERVICE_ID_UTILITY,
           servUtility::cmd_vend_config,
           servLicense::cmd_sys_vendor);
}

void servLicense::onOptActive()
{
//    qDebug() << "servLicense::"<<__FUNCTION__<<"Opt active!";
}

void servLicense::onOptExp()
{
//    qDebug() << "servLicense::"<<__FUNCTION__<<"Opt expire!";
}

void servLicense::onOptInvalid()
{
//    qDebug()  <<"servLicense::"<< __FUNCTION__<<"Opt invalid!";
}

int servLicense::onInsertUSB(const QString& dirName)
{
    LOG_DBG()<<"--------------------------------------------";
    QString dir = dirName;

    if( checkOptSetupFile(dir))
    {
        //comment by hxh. 2018.3.25
        //servGui::showInfo(MSG_OPT_FILE_DETECT);
        //! for bug 1804, add by lidongming
        //post( E_SERVICE_ID_HELP, CMD_SERVICE_ACTIVE, MSG_APP_HELP_SETUP_OPT);
        post( E_SERVICE_ID_HELP, servHelp::cmd_opt_file_status, true);
    }

    return ERR_NONE;
}

int servLicense::onRemoveUSB(const QString& path)
{
    Q_UNUSED(path);
    QString dir = "";
    if( !checkOptSetupFile(dir))
    {
        post(E_SERVICE_ID_HELP, servHelp::cmd_opt_file_status, false);
    }

    return ERR_NONE;
}

bool servLicense::checkOptSetupFile(QString& path)
{
    QString optSetupFile = m_SN + ".lic";

    if( path.size() > 0)
    {
        QString filePath = path + "/" + optSetupFile;
        if(QFile::exists(filePath))
        {
            path = filePath;
            return true;
        }
    }
    else
    {
        QList<QString> list;
        servStorage::getRemovableList(list);
        LOG_DBG()<<"---------RemovableList.size: "<<list.size();

        for(int i=0; i<list.size(); i++)
        {
            QString filePath = list.at(i) + "/" + optSetupFile;
            LOG_DBG()<<"-------------------Opt.Lic Path:"<<filePath;
            if(QFile::exists(filePath))
            {
                path = filePath;
                LOG_DBG()<<"------------------Opt.Lic true Path:"<<path;
                return true;
            }
        }

    }

    return false;
}

void servLicense::onOptSetup()
{
    QString optSetupPath = "";
    if( checkOptSetupFile(optSetupPath))
    {
        LOG_DBG() << "------------------opt Setup Path: "<<optSetupPath;
        QFile optionsFile( optSetupPath);
        if ( optionsFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            QTextStream stream( &optionsFile);
            QString str = "";
            QString optStr = "";
            QString optName = "";
            QString optLic = "";
            OptType optType = OPT_UNKNOWN;
            bool optAct = false;
            int msg = MSG_OPT_RETRY;

            for( int i=0; !stream.atEnd(); i++)
            {
                str = stream.readLine();
                if( str.isEmpty())
                {
                    continue;
                }

                LOG_DBG() <<"-------------str: "<<str;

                optStr = str.section("-", 1, 1);
                optName = optStr.section("@", 0, 0);
                optLic = optStr.section("@", 1, 1);

                optType = getOptType( optName);
                LOG_DBG() <<"-------------optType: "<<optType;

                if( optType != OPT_UNKNOWN)
                {
                    msg = activeOpt(optType, optLic);
                    if( (msg == MSG_OPT_SETUP_SUCCESS) &&
                        (!optAct) )
                    {
                        optAct = true;
                    }
                }
                else
                {
                    continue;
                }
            }
            optionsFile.close();

            if( optAct)
            {
                serviceExecutor::post(serv_name_license,
                                      servLicense::cmd_Opt_Active,
                                      0);

                serviceExecutor::post(serv_name_help,
                                      servHelp::cmd_opt_show,
                                      0);
            }
            else
            {
//                if( msg == MSG_OPT_LIC_INVALID)
//                {
//                    int num = m_OptList.value(optType)->get_Opt_Fail_InstNum();
//                    QString info = sysGetString(msg) + QString("%1").arg( num);
//                    servGui::showInfo(QLatin1Literal(info));
//                }
//                else
//                {
//                    servGui::showInfo(msg);
//                }
            }
        }
        else
        {
            servGui::showInfo( QLatin1Literal("Read License error"));
        }
    }
    else
    {
        servGui::showInfo( QLatin1Literal("No License file."));
    }

//    testPrivateSave();
}

void servLicense::onSysVendor()
{
    getSysInfo();
    verifyLicense();
    defer( servLicense::cmd_Opt_Active );
}

void servLicense::testPrivateSave()
{
    LOG_DBG() <<"-------------------------------------------------------------------";
    int idBase;
    idBase = servDso::getIdBase( getId());

    int testId = idBase + 100;
    int size = servDso::getLen( testId);
    LOG_DBG()<<"servDso::getLen: "<<size;

    if( size == 0)
    {
        QString data = "10;89F006B5CED26B6DDA19D058D0C1679F664886CE76413CBE915C6FD6F330A3295317292C52D084BFE004E806921A94F9816425969552803C126D9D029EBA4DEA\n";
        QByteArray uncompData = data.toLatin1();
        QByteArray compData = qCompress( uncompData);
        LOG_DBG() << "uncompData: "<<uncompData;
        LOG_DBG() << "uncompData.size: "<<uncompData.size();
        LOG_DBG() << "compData: "<<compData;
        LOG_DBG() << "compData.size: "<<compData.size();
        uncompData = qUncompress( compData);
        LOG_DBG() << "uncompData: "<<uncompData;
        LOG_DBG() << "uncompData.size: "<<uncompData.size();
        int saveSize = servDso::save( testId, compData.data(), compData.size());
        if( saveSize != compData.size())
        {
            LOG_DBG() << "servDso::save fall!!!!!!!";
            LOG_DBG() << "servDso::save size: "<<saveSize;
            LOG_DBG() << "compData.size: "<<compData.size();
        }
        else
        {
            LOG_DBG() << "servDso::save success---------";
        }
    }
    else
    {
        char dataIn[ size];
        int loadSize = servDso::load( testId, dataIn, size);
        if( loadSize != size)
        {
            LOG_DBG() << "servDso::load fall!!!!!!!!";
            LOG_DBG() << "servDso::load size: "<<loadSize;
            LOG_DBG() << "size: "<<size;
        }
        else
        {
            LOG_DBG() << "servDso::load success-----------";
            LOG_DBG() << "servDso::load size: "<<loadSize;


            QByteArray compDataIn = QByteArray(dataIn, size);
            QByteArray uncompDataIn = qUncompress( compDataIn);

            LOG_DBG() << "compDataIn: "<<compDataIn;
            LOG_DBG() << "compDataIn.size: "<<compDataIn.size();
            LOG_DBG() << "loadSize: "<<loadSize;
            LOG_DBG() << "uncompDataIn: "<<uncompDataIn;
        }

        bool isRemove = true;
        if( isRemove)
        {
            int err = servDso::remove( testId);
            if( err != ERR_NONE)
            {
                LOG_DBG() << "servDso::remove Err!!";
            }
            else
            {
                LOG_DBG() << "servDso::remove Id = "<<testId;
            }
        }
    }

    LOG_DBG() <<"-------------------------------------------------------------------";
}

void servLicense::testClearPrivate()
{
    int idBase;
    idBase = servDso::getIdBase( getId());

    int KeyId = idBase + FRAM_ID_PUB_KEY;     //! Id for Pub Key
    int licId = idBase + FRAM_ID_OPTIONS;  //! Id for All options License
    int AllId = idBase + FRAM_ID_FAILED_MAX;  //! Id for All Option Install fall number

    if( servDso::getLen( KeyId) != 0)
    {
        servDso::remove( KeyId);
    }

    if( servDso::getLen( licId) != 0)
    {
        servDso::remove( licId);
    }

    if( servDso::getLen( AllId) != 0)
    {
        servDso::remove( AllId);
    }
}

OptType servLicense::getOptType(QString &name)
{
    QString localName = name;
    if( localName.compare("AWG", Qt::CaseInsensitive) == 0 )
    {
        localName = "DG";
    }

    for( int i=0; i<array_count(g_OptName); i++)
    {
        if( g_OptName[i] == localName)
        {
            return (OptType)i;
        }
    }

    return OPT_UNKNOWN;
}

void servLicense::getSysInfo()
{
    m_Model = servUtility::getSystemModel();
    m_SN = servUtility::getSystemSerial();
}

bool servLicense::cmd_GetOptEnable(OptType type)
{
    bool optEnable = false;

    if( /*!m_OptList.isEmpty()*/ m_OptList.count() > type)
    {
        optEnable = m_OptList.value(type)->isEnable();

        // if 500 enable then 200 is OK
        if( type == OPT_2RL && !optEnable )
        {
            optEnable = m_OptList.value(OPT_5RL)->isEnable();
        }
    }

    return optEnable;
}

void servLicense::createTestLic()
{
    QFile testLicenseFile( _licensePath+"CTR.lic");
    if( testLicenseFile.open( QIODevice::WriteOnly | QIODevice::Text ))
    {
        QTextStream stream( &testLicenseFile);
        //Format: optName;License
        stream << QString ( "CTR;78882734385D24A4A2C4D456F8D1E78BDADE57432EA1CBEFB4B47D2514831A4943F3A443AE3F626C08FFF40BF68909BB5F04975C3C34D320CC8E19706F007B19") << "\n";
        testLicenseFile.close();
    }

    QFile pubKeyFile( _keyPath+"Key.data");
    if( pubKeyFile.open( QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream( &pubKeyFile);
        //Format: curveID;pubKey str
        stream << QString( "brainpoolP256r1;042CD74A79E48CA97E3414C81730716F21CEE94D7DE602B331C78DBAEB407351475EAD8DBB2629B5DB2506217CF3801712F9A2DD85CD4B0CFFF5D3E5FDF93290F1");
        pubKeyFile.close();
    }

}

void servLicense::getOptList()
{
    int idBase;
    idBase = servDso::getIdBase( getId());
    for(int i=0; i<NumOfOpt; i++)
    {
        COptInfo *opt = new COptInfo(OptTypeList[i], idBase);
        QFile optLicenseFile( _licensePath+opt->getName()+".lic");
        if ( optLicenseFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            QTextStream stream( &optLicenseFile);
            QString str = stream.readLine();
            QString license = str.section(";", 1, 1);
            opt->setLicense(license);
            optLicenseFile.close();
        }
        m_OptList[OptTypeList[i]] = opt;
    }
}

void *servLicense::getOptInfo()
{
    m_OptInfoList.clear();

    if( !m_OptList.isEmpty())
    {
        foreach (OptType type, m_OptList.keys())
        {
            if( type == OPT_BND)
            {
                continue;
            }
            else if( (type >= OPT_BW1T2)&&(type <= OPT_BW3T5) )
            {
                if( check_BandWidthOpt(type))
                {
                    m_OptInfoList[type] = m_OptList.value(type);
                }
                else
                {
                    continue;
                }
            }
            else if(  (type == OPT_MASK)||(type == OPT_CTR)
                    ||(type == OPT_DVM)||(type == OPT_MSO)
                    ||(type == OPT_SENSOR)||(type == OPT_ARINC)
                    ||(type == OPT_JITTER)||(type == OPT_EDK)  )
            {
                continue;
            }
            else if( type == OPT_DG)
            {
                if( sysHasDG())
                {
                    m_OptInfoList[type] = m_OptList.value(type);
                }
                else
                {
                    continue;
                }
            }
            else
            {
                m_OptInfoList[type] = m_OptList.value(type);
            }
        }

        //remove low bandwidth when higher is installed
        for(int high = OPT_BW3T5; high > OPT_BW1T2; high-- )
        {
            if( m_OptInfoList.contains( (OptType)high )  )
            {
                COptInfo *opt = m_OptInfoList[(OptType)high];
                if( opt->isEnable() )
                {
                    for( int low = OPT_BW1T2; low<high; low ++ )
                    {
                        if( m_OptInfoList.contains( (OptType)low ) )
                        {
                            m_OptInfoList.remove( (OptType)low);
                        }
                    }
                }
            }
        }

        return &m_OptInfoList;
    }
    else
    {
        return NULL;
    }
}

void servLicense::verifyLicense()
{
    QString keyStr = "";
    QString ec_idStr = "";

    getPublicKeyStr(keyStr, ec_idStr);

    if( (keyStr != "") && (ec_idStr != "") )
    {
        foreach (OptType type, m_OptList.keys())
        {
            verifyOptLic( m_OptList.value(type), keyStr, ec_idStr);
        }
        set_BndOpt_Enable(OPT_BND, true);
    }

}

bool servLicense::verifyOptLic( COptInfo *opt, QString &key, QString &ec_id)
{
    bool licValid = false;
    int digestLen = 0;
    int verifyVal = -1;

    unsigned char digest[256] = { 0 };

    unsigned char publicKey[1024] = {0};
    EC_KEY *pubkey = NULL;
    int eccCurveID = (int)eBrainpoolP256r1_Curve;
    QByteArray pubKeyByte;
    int pubKeyLen = 0;
    for ( int i = 0; i < eEndOfEccCurve; ++i )
    {
        if ( 0 == ec_id.compare(ECC_CURVENAME[i]) )
        {
            eccCurveID = i;
            break;
        }
    }
    pubKeyByte = key.toLatin1();
    pubKeyLen = key.size();
    memcpy( publicKey, pubKeyByte.data(),pubKeyLen);

    pubkey = cryptoRigolImportPubKey( publicKey, ECC_CurveNID[eccCurveID] );

    opt->setLicenseValid(false);
    licValid = false;

    if( pubkey != NULL)
    {
        rigolInstrumentInfo *instInfo = new rigolInstrumentInfo;

        QByteArray model = m_Model.toLatin1();
        instInfo->m_pModel = model.data();

        QByteArray sn = m_SN.toLatin1();
        instInfo->m_pSN    = sn.data();

        QByteArray optN = opt->getName().toLatin1();
        instInfo->m_pOption = optN.data();

        QByteArray ver = opt->getVer().toLatin1();
        instInfo->m_pVersion = ver.data();

        for(int floatLic = (int)LicType_Fix;
                floatLic < (int)LicTypeNum && !licValid;
                floatLic++)
        {
            for(int expTag =(int)LicTime_Forever;
                    expTag < (int)LicTimeNum && !licValid;
                    expTag++)
            {
                //message
                opt->setLicenseType((LicenseType)floatLic);
                opt->setLicenseTime((LicenseTime)expTag);

                instInfo->m_s8Floating = opt->getLicTypeVal();
                instInfo->m_s8ExpireTag = opt->getLicTimeVal();

                //Digest
                digestLen = cryptoRigolGenDigest( digest, instInfo);


                //Verify
                if( digestLen >= 0 && opt->getLicense() != "")
                {
                    QByteArray license = opt->getLicense().toLatin1();
                    verifyVal = cryptoRigolVerifyDigestRS(digest,
                                                          digestLen,
                                                          license.data(),
                                                          pubkey);
                    if( verifyVal == 0)
                    {
                        opt->setLicenseValid(true);
                        if( (LicenseTime)expTag == LicTime_Limit)
                        {
                            opt->setRunTime(0); //Init Option RunTime
                        }
                        licValid = true;
                    }
                }              
            }
        }
        delete instInfo;
    }
    return licValid;
}

void servLicense::getPublicKeyStr(QString &key, QString &ec_id)
{
    QByteArray keyCode;
    //! load PUB_KEY
    int idBase;
    idBase = servDso::getIdBase( getId());
    int KeyId = idBase + FRAM_ID_PUB_KEY;
    int size = servDso::getLen( KeyId);
    LOG_DBG()<<"servLicense::getPublicKeyStr  servDso::getLen: "<<size;

    if( size > 0)
    {
        //! keyData Format: "curveID;pubKey str"
        char keyData[ size];
        int loadSize = servDso::load( KeyId, keyData, size);
        if( loadSize == size)
        {
            keyCode = QByteArray(keyData, size);
            //LOG_DBG() << "load keyCode = " << keyCode;
            //! Decode key
            if( decodeKeyData( keyCode, NULL))
            {
                //LOG_DBG() << "decode keyCode = " << keyCode;
                ec_id = QString(keyCode).section(";", 0, 0).trimmed();
                key   = QString(keyCode).section(";", 1, 1).trimmed();

                LOG_DBG() << "KEY=" << key;
                LOG_DBG() << "EC=" << ec_id;
                return;   //! PUB_KEY load success
            }
            else          //! PUB_KEY load fail
            {
                key = "";
                ec_id = "";
            }
        }
    }

    //! load backup PUB_KEY
    QFile pubKeyFile( _keyPath+"Key.data");

    if( pubKeyFile.open( QIODevice::ReadOnly ) )
    {
        int size = pubKeyFile.size();
        char keyData[ size];
        pubKeyFile.read( keyData, size);
        keyCode = QByteArray(keyData, size);

        //! Decode key
        if( decodeKeyData( keyCode, NULL))
        {
            ec_id = QString(keyCode).section(";", 0, 0).trimmed();
            key   = QString(keyCode).section(";", 1, 1).trimmed();

            pubKeyFile.close();

            //! load success, save to private data
            if( size != servDso::getLen( KeyId))
            {
                servDso::remove( KeyId);
            }
            if( size == servDso::save( KeyId, keyData, size) )
            {
                post( E_SERVICE_ID_DSO, servDso::CMD_FLUSH_CACHE, (int)1 );
            }

            return; //! backup PUB_KEY load success
        }
        else        //! backup PUB_KEY load fail
        {
            key = "";
            ec_id = "";
        }
    }
    else
    {
        key = "";   //! backup PUB_KEY load fail
        ec_id = "";
    }

}

void servLicense::set_BndOpt_Enable(OptType optType, bool enble)
{
    if( optType == OPT_BND)
    {
        if(enble) //Enable BND Options
        {
            if( m_OptList.value(optType)->isEnable() )
            {
                //Enable all BND app options
                for( int i=0; i<array_count(BndOptList); i++)
                {
                    m_OptList.value(BndOptList[i])->setBndEable(true);
                }
            }
        }
        else   //Disable BND Options Flag
        {
            for( int i=0; i<array_count(BndOptList); i++)
            {
                m_OptList.value(BndOptList[i])->setBndEable(false);
            }
        }
    }
}

bool servLicense::check_BandWidthOpt(OptType optType)
{
    int bandW = (int)BW_100M;      // Default BandWidth

    if( (optType >= OPT_BW1T2)&&(optType <= OPT_BW3T5) )  // is BandWidth Opt
    {
        serviceExecutor::query(serv_name_utility,
                               servUtility::cmd_model_band,
                               bandW);
        if( (Bandwidth)bandW == BW_100M)
        {
            if(  (optType == OPT_BW1T2)
               ||(optType == OPT_BW1T3)
               ||(optType == OPT_BW1T5) )
            {
                return true;
            }
        }
        else if( (Bandwidth)bandW == BW_200M)
        {
            if(  (optType == OPT_BW2T3)
               ||(optType == OPT_BW2T5) )
            {
                return true;
            }
        }
        else if( (Bandwidth)bandW == BW_350M)
        {
            if(  optType == OPT_BW3T5 )
            {
                return true;
            }
        }

        return false;
    } //if( (optType >= OPT_BW1T2)&&(optType <= OPT_BW3T5) )  is BandWidth Opt
    else
    {
        return true;
    }
}

bool servLicense::check_LimitLic_InstNum(OptType optType,
                                         QString optLic,
                                         QString &key,
                                         QString &ec_id)
{
    int idBase;
    idBase = servDso::getIdBase( getId() );

    COptInfo *opt = new COptInfo(optType, idBase);
    opt->setLicense(optLic);
    verifyOptLic(opt, key, ec_id);

    if( (opt->isEnable())&&(opt->getLicenseTime() == LicTime_Limit) )
    {
        int instNum = m_OptList.value(optType)->getLicInstNum();

        if( instNum >= TRIAL_LICENSE_MAX)
        {
            delete opt;
            return false;
        }
        else
        {
            delete opt;
            return true;
        }
    }
    else
    {
        delete opt;

        if(m_OptList.value(optType)->get_Opt_Fail_InstNum() >= LICENSE_RETRY_TIMES )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

void servLicense::set_AllLic_InstNum()
{
    //! load All Option Install fall number
    int idBase = servDso::getIdBase( getId());
    int AllId = idBase + FRAM_ID_FAILED_MAX;
    int instNum = 0;
    if( servDso::getLen( AllId) != sizeof(instNum))
    {
        instNum = 0;
    }
    else
    {
        int loadSize = servDso::load( AllId, &instNum, sizeof(instNum));
        Q_ASSERT( loadSize == sizeof(instNum));
        if( loadSize != sizeof(instNum))
        {
            instNum = 0;
        }
    }

    instNum += 1;  //! All Options install fall number + 1

    //! save All Option Install fall number
    int saveSize = servDso::save( AllId, &instNum, sizeof(instNum));
    Q_ASSERT( saveSize == sizeof(instNum));
}

void servLicense::reset_AllLic_InstNum()
{
    int idBase = servDso::getIdBase( getId());
    int AllId = idBase + FRAM_ID_FAILED_MAX;
    int instNum = 0;  //! reset All Options install fall number

    //! save All Option Install fall number
    int saveSize = servDso::save( AllId, &instNum, sizeof(instNum));
    Q_ASSERT( saveSize == sizeof(instNum));
}

bool servLicense::check_AllLic_InstNum()
{
    //! load All Option Install fall number
    int idBase = servDso::getIdBase( getId());
    int AllId = idBase + FRAM_ID_FAILED_MAX;
    int instNum = 0;

    if( servDso::getLen( AllId) != sizeof(instNum))
    {
        instNum = 0;
    }
    else
    {
        int loadSize = servDso::load( AllId, &instNum, sizeof(instNum));
        Q_ASSERT( loadSize == sizeof(instNum));
        if( loadSize != sizeof(instNum))
        {
            instNum = 0;
        }
    }

    if( instNum >= ALL_OPTLIC_INST_NUM)
    {
        return false;
    }
    else
    {
        return true;
    }
}

DsoErr servLicense::cmd_ActiveOpt(QString opt)
{
    QString optStr = "";
    QString optName = "";
    QString optLic  = "";
    OptType optType;
    int msg = 0;

    optStr = opt.section("-", 1 ,1);
    optName = optStr.section("@", 0, 0);
    optLic  = optStr.section("@", 1, 1);

    optType = getOptType(optName);

    if( (optType != OPT_UNKNOWN )&&
        (!optLic.isEmpty()) )
    {
        msg = activeOpt( optType, optLic);

        if( msg == MSG_OPT_SETUP_SUCCESS)
        {
            serviceExecutor::post(serv_name_license,
                                  servLicense::cmd_Opt_Active,
                                  0);
        }
    }
    else
    {
        if( optType != OPT_UNKNOWN)
        {
            servGui::showInfo(QLatin1Literal("Option is unknown!"));
        }
        else //optLic.isEmpty
        {
            servGui::showInfo(QLatin1Literal("Option license is empty!"));
        }
    }

    return ERR_NONE;
}

DsoErr servLicense::cmd_setKey(CArgument arg)
{
    DsoErr err = ERR_NONE;

    if(arg.size() != 1)
    {
        err = ERR_INVALID_INPUT;
        return err;
    }

    QByteArray keyData( (char*)(arg[0].arbVal.mPtr), arg[0].arbVal.mBlockLen);

    if( keyData.isEmpty())
    {
        servGui::showInfo("Invalid Key!");
    }
    else
    {
        //! Save PUB_KEY
        int idBase;
        idBase = servDso::getIdBase( getId());

        int KeyId = idBase + FRAM_ID_PUB_KEY;

        //! keyData Format: curveID;pubKeycode
        if( keyData.size() != servDso::getLen( KeyId))
        {
            servDso::remove( KeyId);
        }

        if( keyData.size() != servDso::save( KeyId, keyData.data(), keyData.size()))
        {
            servGui::showInfo("Write key fail!");
        }
        else
        {
            post( E_SERVICE_ID_DSO, servDso::CMD_FLUSH_CACHE, (int)1 );
            servGui::showInfo("Set Key successfully!");
        }

        //! Backup PUB_KEY
        QFile pubKeyFile( _keyPath+"Key.data");
        if( pubKeyFile.open( QIODevice::WriteOnly))
        {
            pubKeyFile.write( keyData.data(), keyData.size());
            fsync(pubKeyFile.handle());
            pubKeyFile.close();
        }

        //安装秘钥时，之前的限制次数恢复默认
        //1 试用选件安装次数 not reset to 0
        //2 正式选件失败次数 reset to 0
        //added by hxh
        foreach( OptType type, m_OptList.keys())
        {
            COptInfo *opt = m_OptList.value(type);

            //! 选件安装失败次数超过10次后，12小时后可重新尝试安装
            if( opt->get_Opt_Fail_InstNum() >= LICENSE_RETRY_TIMES )
            {
                //! reset Official license install number
                opt->set_Opt_Fail_InstNum( true);
            }
        }

        //RE Verify for all option
        onSysVendor();

    }

    //startup();

    return err;
}

int servLicense::activeOpt( OptType optType, QString optLic)
{
    QString keyStr = "";
    QString ec_idStr = "";
    bool licenseValid = false;
    int msg = 0;

    COptInfo* optInfo = m_OptList.value(optType);

    if(  (optInfo->isEnable()) &&
         (optInfo->getLicenseTime() == LicTime_Forever) )
    {
        msg = MSG_OPT_ALREADY_ACT;
        servGui::showInfo(msg);
        return msg;
    }

    optInfo->setLicense(optLic);

    getPublicKeyStr(keyStr, ec_idStr);

    if( (keyStr != "") && (ec_idStr != "") )
    {
        if( !check_BandWidthOpt(optType))
        {
            msg = MSG_OPT_BW_OPT_INVALID;
            servGui::showInfo(msg);
            return msg;
        }

        //临时选件只能安装三次
        if( !check_LimitLic_InstNum(optType, optLic, keyStr,ec_idStr))
        {
            msg = MSG_OPT_TRIAL_LIMIT;
            servGui::showInfo(msg);
            return msg;
        }

        //正式选件可以尝试安装10次
        if( optInfo->get_Opt_Fail_InstNum() >= LICENSE_RETRY_TIMES )
        {
            msg = MSG_OPT_FAILED_LIMIT;
            servGui::showInfo(msg);
            return msg;
        }

        licenseValid = verifyOptLic( optInfo, keyStr,ec_idStr);
        if( licenseValid)
        {
            //临时选件成功安装次数加1，并复位使用时间，也就是重新计时36小时
            if( optInfo->getLicenseTime() == LicTime_Limit)
            {
                optInfo->setLicInstNum();
                optInfo->resetRunTime();
            }

            writeLicense(optType, optLic);
            set_BndOpt_Enable(optType, true);

            msg = MSG_OPT_SETUP_SUCCESS;
            servGui::showInfo(msg);
            return msg;
        }
        else
        {
            optInfo->set_Opt_Fail_InstNum( false);

            msg = MSG_OPT_RETRY;
            int num = optInfo->get_Opt_Fail_InstNum();
            QString info = sysGetString(msg) + QString("%1").arg( LICENSE_RETRY_TIMES - num);
            servGui::showInfo( info);

            return msg;
        }
    }
    else
    {
        msg = MSG_OPT_KEY_INVALID;
        servGui::showInfo(msg);
        return msg;
    }
}

void servLicense::writeLicense(OptType optType, QString &optLic)
{
    QString optName = m_OptList.value(optType)->getName();
    QFile LicenseFile( _licensePath+optName+".lic");
    if( LicenseFile.open( QIODevice::WriteOnly | QIODevice::Text ))
    {
        QTextStream stream( &LicenseFile);
        stream << QString ( optName + ";"+ optLic) << "\n";
        fsync(LicenseFile.handle());
        LicenseFile.close();
    }
}

void servLicense::cmd_deleteOpt(CArgument &optInfo)
{
    LOG_DBG() << optInfo[0].iVal;
    QString optN;
    OptType optType = (OptType)optInfo[0].iVal;

    if( m_OptList.value(optType)->getLicenseTime() == LicTime_Forever)
    {
        m_OptList.value(optType)->deleteLicense();
        set_BndOpt_Enable(optType, false);
        optN = m_OptList.value(optType)->getName();

        QFile optLicFile(_licensePath+optN+".lic");
        optLicFile.remove();

        serviceExecutor::post(serv_name_license,
                              servLicense::cmd_Opt_Invalid, 0);

        serviceExecutor::post(serv_name_help,
                              servHelp::cmd_opt_show, 0);

        servFile::sync2Flash();
    }
}

void servLicense::cmd_deleteAllOpt()
{
    QString optN;
    foreach(OptType type, m_OptList.keys())
    {
        if(  m_OptList.value(type)->getLicenseTime() == LicTime_Forever)
        {
            m_OptList.value(type)->deleteLicense();
            optN = m_OptList.value(type)->getName();
            QFile optLicFile(_licensePath+optN+".lic");
            optLicFile.remove();
        }
        m_OptList.value(type)->setBndEable(false);
    }

    serviceExecutor::post(serv_name_license,
                          servLicense::cmd_Opt_Invalid,
                          0);
    serviceExecutor::post(serv_name_help,
                          servHelp::cmd_opt_show,
                          0);

    servFile::sync2Flash();
}

void servLicense::clearOptLicData()
{
    int idBase;
    idBase = servDso::getIdBase( getId());
    int licId = idBase + FRAM_ID_OPTIONS;  //! Id for All options License

    servDso::remove( licId);
}


bool servLicense::decodeKeyData( QByteArray &data, unsigned int *pxxteaKey)
{
    //!4字节对齐后的大小
    int  buffSize = data.size();

    //!需要解密的数据不是4字节对齐，不能解密
    if( 0 != (buffSize%4) )
    {
        return false;   //! Decode fail
    }

    com_algorithm::CXXTEA::setKeys( (XXTEA_TYPE*)pxxteaKey );
    if( 0 == com_algorithm::CXXTEA::decode((XXTEA_TYPE*)(data.data()), (long)(buffSize)))
    {
        return true;   //! Decode success
    }
    else
    {
        return false;  //! Decode fail
    }
}
