/*
 * cryptoRigol.h
 *
 *  Created on: Feb 16, 2017
 *      Author: rigolee
 */

#ifndef CRYPTORIGOL_H_
#define CRYPTORIGOL_H_

# ifdef  __cplusplus
extern "C" {
# endif

#include "../../com/openssl-1.1.0d/include/openssl/bio.h"
#include "../../com/openssl-1.1.0d/include/openssl/ec.h"

//#include <openssl/bio.h>
//#include <openssl/ec.h>

typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef signed short int16_t;
typedef unsigned short uint16_t;
typedef signed int int32_t;
typedef unsigned int uint32_t;
typedef signed long long int64_t;
typedef unsigned long long uint64_t;

/*****************************************/
/*
 * 功能：256bit椭圆曲线参数
 */
/*****************************************/
#define NID_secp256k1 714
#define NID_brainpoolP256r1 927
#define NID_brainpoolP256t1 928
enum EEccCurve
{
    eSecp256k1_Curve,
    eBrainpoolP256r1_Curve,
    eBrainpoolP256t1_Curve,
    eEndOfEccCurve
};

const int ECC_CurveNID[ eEndOfEccCurve ] =
{
    NID_secp256k1,
    NID_brainpoolP256r1,
    NID_brainpoolP256t1
};

/*****************************************/
/*
 * 功能：存储仪器信息
 * 参数介绍：Mode-仪器型号
 * 			SN-仪器序列号
 *          Option-选件名称
 *          Version-选件版本
 *          Floating-浮动授权码 0-固定授权  1-浮动授权
 *          ExpireTag-失效时间标签 0-永久授权 1-demo 授权(12小时)
 */
/*****************************************/

#define INSTRUMENTINFO_MODEL_LENGTH 8
#define INSTRUMENTINFO_SN_LENGTH 16
#define INSTRUMENTINFO_OPTION_LENGTH 5
#define INSTRUMENTINFO_VERSION_LENGTH 4
#define INSTRUMENTINFO_FLOATING_LENGTH 4
#define INSTRUMENTINFO_EXPIRETAG_LENGTH 4

enum ELicenseFloat
{
    eFix_License,
    eFloat_License,
    eEndOfLiceseFloat
};

enum ELicenseExpire
{
    eForever_License,
    e12Hour_License,
    eEndOfLicenseExpire
};

typedef struct rigolInstrumentInfo{ //>note : the last byte is for '\0'
    char *m_pModel;
    char *m_pSN;
    char *m_pOption;
    char *m_pVersion;
    int8_t m_s8Floating;
    int8_t m_s8ExpireTag;
}rigolInstrumentInfo;

int cryptoRigolGenDigest(unsigned char *instrumentInfoDigest, rigolInstrumentInfo* instrumentInfo);
EC_KEY * cryptoRigolGetPubkey(unsigned char *publicKey, int len, int curveNameNid );
EC_KEY * cryptoRigolImportPubKey(unsigned char *publicKey, int curveNameNid );
int cryptoRigolVerifyDigest(const unsigned char *dgst, int dgst_len,
        const unsigned char *sigbuf, int sig_len, EC_KEY *eckey);
int cryptoRigolVerifyDigestRS(const unsigned char *dgst, int dgst_len,
        const char *license, EC_KEY *eckey);
int changeHexTobyte(char source[],char bits[],int sourceLen);
int sign_itecc(void);
#  ifdef  __cplusplus
}
#  endif

#endif /* CRYPTORIGOL_H_ */
