#-------------------------------------------------
#
# Project created by QtCreator 2017-03-24T11:06:51
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets
QT       += network

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/services/servlicense
INCLUDEPATH += .
INCLUDEPATH += "../../com/openssl-1.1.0d/include/"

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


# dbg show
dbg = $$(_DEBUG)
equals(dbg, _DEBUG){

}
#DEFINES += _DEBUG
# Input
SOURCES += servlicense.cpp \
    cryptoRigol.cpp

HEADERS += servlicense.h \
    cryptoRigol.h
