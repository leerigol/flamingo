#undef _DEBUG
#include "servicefactory.h"
//! scpi中引用到了 network module
//! 避免其它service中也需要引入 network
#include "./servscpi/servscpi.h"

QList<service*> serviceFactory::_serviceList;

#define delete_serv( pServ )    if ( pServ != NULL )  { delete pServ; }


//#define _GUI_CASE
/*! \brief createServices
* - 创建服务
* - 将服务添加到工厂列表中
*/
DsoErr serviceFactory::createServices()
{
    service *pServ;

    //! have created?
    if ( _serviceList.size() > 0 )
    {
        Q_ASSERT( false );
        return ERR_SERV_CREATED;
    }
    LOG_FACTORY();

    //! utility
    pServ = new servUtility(serv_name_utility);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //! License
    pServ = new servLicense( serv_name_license, E_SERVICE_ID_LICENSE);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //! menu test hxh
//    pServ = new servMenuTest( serv_name_menutester );
//    Q_ASSERT( NULL != pServ );
//    _serviceList.append( pServ );
//    LOG_FACTORY();

//    pServ = new servMenuTest( serv_name_menutester2 );
//    Q_ASSERT( NULL != pServ );
//    _serviceList.append( pServ );
//    LOG_FACTORY();

    //! gui
    pServ = new servGui( serv_name_gui, E_SERVICE_ID_ERR_LOGER );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! dso
    pServ = new servDso( serv_name_dso, E_SERVICE_ID_DSO );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! auto
    pServ = new servAuto( serv_name_autoset, E_SERVICE_ID_AUTO_SET );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! cal
    pServ = new servCal( serv_name_cal, E_SERVICE_ID_CAL );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! ch
    pServ = new servCH( serv_name_ch4, chan4, CH4_color, E_SERVICE_ID_CH4 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servCH( serv_name_ch3, chan3, CH3_color, E_SERVICE_ID_CH3 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servCH( serv_name_ch2, chan2, CH2_color, E_SERVICE_ID_CH2 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servCH( serv_name_ch1, chan1, CH1_color, E_SERVICE_ID_CH1 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! La
    pServ = new servLa( serv_name_la, E_SERVICE_ID_LA );
    Q_ASSERT( NULL != pServ );
    _serviceList.append(pServ);
    LOG_FACTORY();

    //! hori
    pServ = new servHori( serv_name_hori, E_SERVICE_ID_HORI );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! math
    pServ = new servMath( serv_name_math1, m1, E_SERVICE_ID_MATH1 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servMath( serv_name_math2, m2, E_SERVICE_ID_MATH2 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servMath( serv_name_math3, m3, E_SERVICE_ID_MATH3 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servMath( serv_name_math4, m4, E_SERVICE_ID_MATH4 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servMathSel( serv_name_math_sel );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servFFTSel( serv_name_math_fft );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servVertMgr( serv_name_vert_mgr );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! plot
    pServ = new servPlot( serv_name_plot, E_SERVICE_ID_PLOT );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! trace
    pServ = new servTrace( serv_name_trace, E_SERVICE_ID_TRACE );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! search
    pServ = new servSearch( serv_name_search, E_SERVICE_ID_SEARCH);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! scpi
    pServ = new servScpi( serv_name_scpi, E_SERVICE_ID_SCPI );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! ref
    pServ = new servRef( serv_name_ref, E_SERVICE_ID_REF );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! source1
    pServ = new servDG(serv_name_source1, E_SERVICE_ID_SOURCE1);
    Q_ASSERT( NULL != pServ);
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! source2
    pServ = new servDG(serv_name_source2, E_SERVICE_ID_SOURCE2);
    Q_ASSERT( NULL != pServ);
    _serviceList.append( pServ );

    //! decode
    pServ = new servDec( serv_name_decode1, E_SERVICE_ID_DEC1 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    pServ = new servDec( serv_name_decode2, E_SERVICE_ID_DEC2 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    pServ = new servDec( serv_name_decode3, E_SERVICE_ID_DEC3 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    pServ = new servDec( serv_name_decode4, E_SERVICE_ID_DEC4 );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //! decode sel
    pServ = new servDecodeSel( serv_name_decode_sel );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    pServ = new servDisplay( serv_name_display );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    int ret = servTrig::createTriggers(&_serviceList);
    Q_ASSERT( 0 == ret );
    LOG_FACTORY();


    //!WRec
    pServ = new servRecord(serv_name_wrec);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! Utility_IOset
    pServ = new servInterface(serv_name_utility_IOset);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! Utility_print
    pServ = new servPrinter(serv_name_utility_Print);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! Utility_mail
    pServ = new servMail(serv_name_utility_Email);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //!Utility_wifi
    pServ = new servWifi(serv_name_utility_Wifi);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();


    //! cursor
    pServ = new servCursor( serv_name_cursor);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! mask
    pServ = new servMask( serv_name_mask );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! quick
    pServ = new servQuick( serv_name_quick );
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! storage
    pServ = new servStorage( serv_name_storage);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();


    //! Measure
    pServ = new servMeasure(serv_name_measure);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //! Counter
    pServ = new servCounter(serv_name_counter);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //! DVM
    pServ = new servDvm(serv_name_dvm);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! UPA
    int t = servUPA::createServUPA(&_serviceList);
    Q_ASSERT( 0 == t );
    LOG_FACTORY();

    //! EyeJit
    pServ = new servEyeJit(serv_name_eyejit,E_SERVICE_ID_EYEJIT);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! Histo
    pServ = new servHisto(serv_name_histo,E_SERVICE_ID_HISTO);
    Q_ASSERT( NULL != pServ);
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! Help
    pServ = new servHelp(serv_name_help, E_SERVICE_ID_HELP);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //!公共命令
    pServ = new servCommonCmd( serv_name_common_cmd);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //!WfmData
    pServ = new servWfmData(serv_name_wfm_data);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //!memory
    pServ = new servMemory(serv_name_memory, E_SERVICE_ID_MEMORY);
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );

    //! Net device listen
    pServ = new servListener();
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    //! hotplug
    pServ = new servHotplug();
    Q_ASSERT( NULL != pServ );
    _serviceList.append( pServ );
    LOG_FACTORY();

    return ERR_NONE;
}


/*! \brief 销毁服务
*/
DsoErr serviceFactory::destroyServices()
{
    int i;

    for ( i = 0; i < _serviceList.size(); i++ )
    {
        delete_serv( _serviceList.at(i) );
    }

    return ERR_NONE;
}

/*! \brief attachServices
* 依次启动列表中的服务运行
* \note move the service obj to servie thread
*/
DsoErr serviceFactory::attachServices( QThread *pThread )
{
    int i;
    serviceExecutor *pObj;

    //! start service
    for ( i = 0; i < _serviceList.size(); i++ )
    {

        if ( pThread != NULL )
        {
            pObj = dynamic_cast<serviceExecutor*>( _serviceList.at(i) );
            if ( pObj != NULL )
            {
                pObj->moveToThread( pThread );
            }
        }
    }

    return ERR_NONE;
}

DsoErr serviceFactory::startServices( )
{
    int i;

    for ( i = 0; i < _serviceList.size(); i++ )
    {
        _serviceList.at(i)->start();
    }

    return ERR_NONE;
}

/*!
 * \brief serviceFactory::startup
 * \return
 * -init
 * -rst
 * -加载设置
 * -startup
 */
DsoErr serviceFactory::startup()
{
    int i;
    serviceExecutor *pObj;

    serviceExecutor::post( serv_name_dso,
                           CMD_SERVICE_LOAD_PRIVACY, (int)0, NULL );

    //! rst init
    for ( i = 0; i < _serviceList.size(); i++ )
    {
        pObj = dynamic_cast<serviceExecutor*>( _serviceList.at(i) );
        if ( pObj != NULL )
        {
            serviceExecutor::post( pObj->getId(), CMD_SERVICE_INIT,(int)0 );
            serviceExecutor::post( pObj->getId(), CMD_SERVICE_RST,(int)0 );
        }
    }

#ifndef _GUI_CASE
    //! load setup
    serviceExecutor::post( serv_name_dso,
                           CMD_SERVICE_SETTING_LOAD, (int)0, NULL );
#endif

    //! startup
    for ( i = 0; i < _serviceList.size(); i++ )
    {
        pObj = dynamic_cast<serviceExecutor*>( _serviceList.at(i) );
        if ( pObj != NULL )
        {
            serviceExecutor::post( pObj->getId(), CMD_SERVICE_LOAD,(int)0 );
            serviceExecutor::post( pObj->getId(), CMD_SERVICE_STARTUP,(int)0 );
        }
    }

    return ERR_NONE;
}

DsoErr serviceFactory::serviceup()
{
    serviceExecutor *pObj;
    //! startup
    for ( int i = 0; i < _serviceList.size(); i++ )
    {
        pObj = dynamic_cast<serviceExecutor*>( _serviceList.at(i) );
        if ( pObj != NULL )
        {
            serviceExecutor::post( pObj->getId(), CMD_SERVICE_SERVICE_UP,(int)0 );
        }
    }

    return ERR_NONE;
}

DsoErr serviceFactory::registerSpy()
{
    serviceExecutor *pObj;

    for ( int i = 0; i < _serviceList.size(); i++ )
    {
        pObj = dynamic_cast<serviceExecutor*>( _serviceList.at(i) );
        if ( pObj != NULL )
        {
            pObj->registerSpy();
        }
    }
    return ERR_NONE;
}

service* serviceFactory::findService( const QString &name )
{
    service* pServ;

    foreach( pServ, _serviceList )
    {
        if ( pServ->getName() == name )
        {
            return pServ;
        }
    }

    return NULL;
}

QList<service*>* serviceFactory::getServiceList()
{
    return &_serviceList;
}
