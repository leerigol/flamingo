#include "servauto.h"

#include "../service_msg.h"

#include "../servgui/servgui.h"
#include "../servdso/servdso.h"
#include "../servmeasure/servmeasure.h"

#include "../servtrace/servtrace.h"
#include "../servch/servch.h"
#include "../servhori/servhori.h"

#include "../servicefactory.h"

IMPLEMENT_CMD( serviceExecutor, servAuto )
start_of_entry()

//! menu msg
on_set_void_void( MSG_AUTO_CYCLE, &servAuto::onSinglePeriod ),
on_set_void_void( MSG_AUTO_NCYCLE, &servAuto::onMultiPeriod ),
on_set_void_void( MSG_AUTO_RISE, &servAuto::onRise ),
on_set_void_void( MSG_AUTO_FALL, &servAuto::onFall ),

on_set_void_void( MSG_AUTO_UNDO, &servAuto::onUndo ),
on_set_void_void( MSG_AUTO_S32BACK, &servAuto::onReturn ),

on_set_int_bool( MSG_AUTO_S32AUTOLOCK, &servAuto::setLock ),
on_get_bool( MSG_AUTO_S32AUTOLOCK,     &servAuto::getLock ),

on_set_int_bool( servAuto::cmd_lock_auto,      &servAuto::lockAuto),
on_set_int_bool( servAuto::cmd_scpi_lock_auto, &servAuto::scpiLockAuto ),
on_get_bool(     servAuto::cmd_scpi_lock_auto, &servAuto::scpiGetLock ),
on_set_int_bool( servAuto::cmd_auto_enable,    &servAuto::setAutoEnable ),

on_get_string  ( servAuto::cmd_auto_password,  &servAuto::getPassword),
on_set_int_string(servAuto::cmd_auto_password, &servAuto::setPassword),

on_set_int_bool( MSG_AUTO_S32AUTORANGE, &servAuto::setPeak ),
on_get_bool( MSG_AUTO_S32AUTORANGE, &servAuto::getPeak ),

on_set_int_bool( MSG_AUTO_S32AUTOCHAN, &servAuto::setOpenCh ),
on_get_bool( MSG_AUTO_S32AUTOCHAN, &servAuto::getOpenCh ),

on_set_int_bool( MSG_AUTO_S32OVERLAY, &servAuto::setOverlap ),
on_get_bool( MSG_AUTO_S32OVERLAY, &servAuto::getOverlap ),

on_set_int_bool( MSG_AUTO_S32KEEPCOUP, &servAuto::setKeepCoup ),
on_get_bool( MSG_AUTO_S32KEEPCOUP, &servAuto::getKeepCoup ),

on_set_int_int( MSG_AUTO_TITLE, &servAuto::onAuto ),

on_set_int_int (MSG_AUTO_PASSWORD,              &servAuto::doNull),
on_set_int_int (servAuto::cmd_auto_unlock,      &servAuto::doNull),

//! system msg
on_set_void_void( CMD_SERVICE_RST, &servAuto::rst ),
on_set_int_void( CMD_SERVICE_STARTUP, &servAuto::startup ),
on_set_void_void( CMD_SERVICE_INIT, &servAuto::init ),

on_set_int_void( CMD_SERVICE_LOAD, &servAuto::onLoad ),

on_set_int_int( CMD_SERVICE_DEACTIVE, &servAuto::onDeactive ),

on_set_int_obj  ( CMD_SERVICE_INTENT, &servAuto::autoOption ),

end_of_entry()

#define NVRAM_ID_LOCKER  0
#define NVRAM_ID_PASSWD  1

servAuto::servAuto( QString name, ServiceId id )
         : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();
}
servAuto::~servAuto()
{
}
int servAuto::serialOut( CStream &stream, serialVersion &ver )
{
    ISerial::serialOut(stream, ver );

    ver = 1;

    quint8 pack;

    pack = (mbAutoPeak <<0)
            | (mbAutoOpenCh <<1)
            | (mbAutoKeepCoup <<2)
            | (mbAutoOverlap <<3);

    ar_out( "opt", pack );

    return 0;
}
int servAuto::serialIn( CStream &stream, serialVersion ver )
{
    ISerial::serialIn(stream, ver );

    //! check ver
    if ( ver != 1 )
    { return 0; }

    //! ar
    quint8 pack;
    ar_in( "opt", pack );

    mbAutoPeak = get_bit( pack, 0 );
    mbAutoOpenCh = get_bit( pack, 1 );

    mbAutoKeepCoup = get_bit( pack, 2 );
    mbAutoOverlap = get_bit( pack, 3 );

    return 0;
}
//! \note keep the user setting
//! do not rst
void servAuto::rst()
{}
int servAuto::startup()
{
    //mUiAttr.setEnable( MSG_AUTO_S32AUTOLOCK, !mbAutoLock );

    return 0;
}
void servAuto::init()
{
    mbAutoLock = false;
    mbAutoPeak = false;
    mbAutoOpenCh = false;

    mbAutoKeepCoup = false;
    mbAutoOverlap = false;

    mbAutoEnable = true;

    //! platform
    attachPlatform( cplatform::sysGetDsoPlatform() );

    m_pSession = NULL;
    mActiveServ = E_SERVICE_ID_UTILITY;

    //! hori
    mSessionList.append( serv_name_hori );

    //! for active channel
    //mSessionList.append( serv_name_vert_mgr );

    //! vert
    mSessionList.append( serv_name_ch1 );
    mSessionList.append( serv_name_ch2 );
    mSessionList.append( serv_name_ch3 );
    mSessionList.append( serv_name_ch4 );

    mSessionList.append( serv_name_la );

    //! the trig services
    foreach( struServItem *pItem, service::_servList )
    {
        Q_ASSERT( pItem != NULL );

        if ( pItem->servName.contains( serv_name_trigger) )
        { mSessionList.append( pItem->servName); }
    }

    //! display
    mSessionList.append( serv_name_display );

    //! meas
    mSessionList.append( serv_name_measure );


    loadPassword();
}

//! load lock bit from private data
DsoErr servAuto::onLoad()
{
    int idBase;

    idBase = servDso::getIdBase( getId() ) + NVRAM_ID_LOCKER;

    //! get lock
    bool bPack;
    if ( servDso::getLen( idBase) != sizeof(bPack) )
    { return ERR_NONE; }

    //! read
    if ( sizeof(bPack) != servDso::load( idBase, &bPack, sizeof(bPack) ) )
    {
        //! If read failure to write again
        bPack = true;
        if ( sizeof(bPack) != servDso::save( idBase, &bPack, sizeof(bPack) ) )
        { return ERR_IN_WRITE; }
    }

    mbAutoLock = get_bit( bPack, 0 );

    return ERR_NONE;
}

DsoErr servAuto::onAuto()
{
    //! check enable
    if ( mbAutoLock )
    {
        return ERR_AUTO_DISABLED;
    }

    //!防止auto中多次按下auto，重复执行。
    if (!mbAutoEnable)
    {
        return ERR_NONE;
    }

    //! lock
    mbAutoEnable = false;

    DsoErr err;
    err = doAuto();

    serviceExecutor::flushQueue();

    //! unlock
    async(cmd_auto_enable, true);

    if ( err == ERR_NONE )
    {
        return defer( CMD_SERVICE_ACTIVE );
    }
    else
    {
        return ERR_AUTO_NO_SIGNAL;
    }
}

DsoErr servAuto::onDeactive( int para )
{
    if ( NULL != m_pSession )
    {
        delete m_pSession;
        m_pSession = NULL;
    }

    return serviceExecutor::setdeActive( para );
}

void servAuto::preAuto()
{
    //! save session
    if ( m_pSession == NULL )
    {
        m_pSession = new QByteArray();
    }
    else
    {
        m_pSession->clear();
    }

    saveSession();

    //! stop
    //postEngine( ENGINE_CONTROL_ACTION, Control_Stop );
    //postEngine( ENGINE_DISP_CLEAR );

    //! config chs
    serviceExecutor *pExe;
    for ( int id = E_SERVICE_ID_CH1; id <= E_SERVICE_ID_CH4; id++ )
    {
        pExe = getExecutor( id );
        Q_ASSERT( pExe != NULL );

        /*! [dba, 2018-04-18], bug: 2987,关闭带宽限制。*/
        pExe->postEngine( ENGINE_CH_BANDLIMIT, BW_OFF );
        pExe->postEngine( ENGINE_CH_ON, true );
    }
    pExe = getExecutor( E_SERVICE_ID_LA );
    Q_ASSERT( pExe != NULL );
    pExe->postEngine( ENGINE_LA_EN, false);

    //! hori
    postEngine( ENGINE_ACQUIRE_TIMEMODE, Acquire_YT );      //! yt
    postEngine( ENGINE_ACQUIRE_DEPTH, 0 );                  //! auto depth
    postEngine( ENGINE_MAIN_SCALE, time_ms(1) );            //! 1ms/
    postEngine( ENGINE_MAIN_OFFSET, 0ll );
    postEngine( ENGINE_HORI_VIEW_MODE, Horizontal_Main );

    //! close la
    //id_async( E_SERVICE_ID_LA, MSG_LA_SELECT_CHAN, 0 );

    //! close mask
    id_async( E_SERVICE_ID_MASK, MSG_MASK_ENABLE, false );
    postEngine( ENGINE_MASK_ACTION_ACTION, mask_action_none );

    //! close zone
    id_async( E_SERVICE_ID_TRIG_ZONE, MSG_TRIG_ZONE_A_ENABLE, false );
    id_async( E_SERVICE_ID_TRIG_ZONE, MSG_TRIG_ZONE_B_ENABLE, false );

    //!auto在stop状态下完成， 但是此处必须要run一下，保证ENGINE_CH_ON生效，通道模式正确。
    postEngine( ENGINE_CONTROL_ACTION, Control_Run );
    postEngine( ENGINE_CONTROL_ACTION, Control_Stop );
    postEngine( ENGINE_DISP_CLEAR, true);
    //postEngine( ENGINE_WAIT_CONTROL_STOP);

    arithAuto::preAuto();
}
void servAuto::postAuto()
{  
    arithAuto::postAuto();
}

DsoErr servAuto::doAuto()
{
    DsoErr err;
    //!准备工作
    preAuto();

    //! snap ch
    int autoCnt = 0;
    if ( mbAutoOpenCh )
    {
        autoCnt = snapOpenedCH();

        if ( autoCnt == 0 )
        {
            servGui::showInfo( IDS_AUTO_ALL_CHAN );
        }
    }

    if( autoCnt == 0 )
    {
        autoCnt = snapAllCH();
    }

    //! iterate
    quint32 ret;
    ret = iterAuto( mpAutoCHs, autoCnt );

    //! apply
    if ( ret != 0)
    {
        ret = snapOKCH( ret );
        Q_ASSERT( ret > 0 );

        couplingAuto( mpOKCHs, ret );
        impedanceAuto(mpOKCHs, ret );

        applyAuto( mpOKCHs, ret, autoCnt);
        err = ERR_NONE;
    }
    else
    {
        loadSession( false );

        err = ERR_AUTO_NO_SIGNAL;
    }

    postAuto();

    return err;
}

void servAuto::onSinglePeriod()
{
    //! trig ok
    qlonglong hScale;
    double peri = arithAuto::mCHs[mTrigId].getPeri();

    hScale = iterConst::fixTimeScale( peri, 1.25 );
    qlonglong mhScale = sysGetMinHScale();
    if ( hScale < mhScale )
    { hScale = mhScale; }

    async( MSG_HORI_MAIN_SCALE, hScale, serv_name_hori );

    //! add meas
    CArgument arg;
    arg.append( Meas_Period );
    arg.append( mTrigId + chan1 );
    async( servMeasure::MSG_CMD_SET_MEAS, arg, serv_name_measure );

    arg.setVal( Meas_Freq, 0 );
    async( servMeasure::MSG_CMD_SET_MEAS, arg, serv_name_measure );

    //! active the last service
    //![dba bug 2106]
    async(CMD_SERVICE_DEACTIVE, 1);
    //id_async( mActiveServ, CMD_SERVICE_ACTIVE, 1 );
}

void servAuto::onMultiPeriod()
{
    qlonglong hScale;
    double peri = arithAuto::mCHs[mTrigId].getPeri();

    hScale = iterConst::fixTimeScale( peri, 5 );
    qlonglong mhScale = sysGetMinHScale();
    if ( hScale < mhScale )
    { hScale = mhScale; }

    async( MSG_HORI_MAIN_SCALE, hScale, serv_name_hori );

    //! add meas
    CArgument arg;
    arg.append( Meas_Period );
    arg.append( mTrigId + chan1 );
    async( servMeasure::MSG_CMD_SET_MEAS, arg, serv_name_measure );

    arg.setVal( Meas_Freq, 0 );
    async( servMeasure::MSG_CMD_SET_MEAS, arg, serv_name_measure );

    //![bug 2106]
    async(CMD_SERVICE_DEACTIVE,1);
    //id_async( mActiveServ, CMD_SERVICE_ACTIVE, 1 );
}
void servAuto::onRise()
{
    //! rise trig
    qlonglong hScale;
    hScale = time_ns(10);

    async( MSG_HORI_MAIN_SCALE, hScale, serv_name_hori );

    //! add meas
    CArgument arg;
    arg.append( Meas_RiseTime );
    arg.append( mTrigId + chan1 );
    async( servMeasure::MSG_CMD_SET_MEAS, arg, serv_name_measure );

    //![bug 2106]
    async(CMD_SERVICE_DEACTIVE,1);
    //id_async( mActiveServ, CMD_SERVICE_ACTIVE, 1 );
}
void servAuto::onFall()
{
    //! fall trig
    qlonglong hScale;
    hScale = time_ns(10);

    async( MSG_HORI_MAIN_SCALE, hScale, serv_name_hori );

    async( MSG_TRIGGER_EDGE_A, (int)Trigger_Edge_Falling, serv_name_trigger_edge );

    //! add meas
    CArgument arg;
    arg.append( Meas_FallTime );
    arg.append( mTrigId + chan1 );
    async( servMeasure::MSG_CMD_SET_MEAS, arg, serv_name_measure );

    //![bug 2106]
    async(CMD_SERVICE_DEACTIVE,1);
    //id_async( mActiveServ, CMD_SERVICE_ACTIVE, 1 );
}

void servAuto::onReturn()
{
    id_async( mActiveServ, CMD_SERVICE_ACTIVE, 1 );
}
void servAuto::onUndo()
{
    loadSession();

    id_async( mActiveServ, CMD_SERVICE_ACTIVE, 1 );
}

DsoErr servAuto::setLock( bool bLock )
{
    //! only check the lock
    if ( bLock )
    {
        servGui::showMsgBox( getName(),
                             INFO_LOCK_SURE,
                             servAuto::cmd_lock_auto );

        return ERR_NONE;
    }
    else
    {
        //return lockAuto(true);
        post(this->getName(), servAuto::cmd_auto_unlock, 0);
        return ERR_NONE;
    }
}
bool servAuto::getLock()
{
    return mbAutoLock;
}

DsoErr servAuto::lockAuto(bool en)
{
    mbAutoLock = en;

    //mUiAttr.setEnable( MSG_AUTO_S32AUTOLOCK, !en );

    //! save auto in private area
    int idBase;
    idBase = servDso::getIdBase( getId() ) + NVRAM_ID_LOCKER;

    bool bPack = mbAutoLock;
    if ( sizeof(bPack) != servDso::save( idBase, &bPack, sizeof(bPack) ) )
    {
        return ERR_IN_WRITE;
    }

    setuiChange(MSG_AUTO_S32AUTOLOCK);

    if( en )
    {
        return ERR_AUTO_LOCKED;
    }
    else
    {
        return ERR_AUTO_UNLOCKED;
    }
}

DsoErr servAuto::scpiLockAuto(bool en)
{
    DsoErr err = async(servAuto::cmd_lock_auto, !en);
    return err;
}
bool servAuto::scpiGetLock()
{
    bool b = getLock();
    return !b;
}

DsoErr servAuto::setPeak( bool bPeak )
{
    mbAutoPeak = bPeak;
    return ERR_NONE;
}
bool servAuto::getPeak()
{ return mbAutoPeak; }

DsoErr servAuto::setOpenCh( bool bOpened )
{
    mbAutoOpenCh = bOpened;
    return ERR_NONE;
}
bool servAuto::getOpenCh()
{ return mbAutoOpenCh; }

DsoErr servAuto::setOverlap( bool bOverlap )
{
    mbAutoOverlap = bOverlap;
    return ERR_NONE;
}
bool servAuto::getOverlap()
{ return mbAutoOverlap; }

DsoErr servAuto::setKeepCoup( bool bKeep )
{
    mbAutoKeepCoup = bKeep;
    return ERR_NONE;
}
bool servAuto::getKeepCoup()
{
    return mbAutoKeepCoup;
}

//! the opened CH now
int servAuto::snapOpenedCH()
{
    int id = 0;
    dsoVert *pVert;
    for ( int ch = chan1; ch <= chan4; ch++ )
    {
        pVert = dsoVert::getCH( (Chan)ch );

        Q_ASSERT( NULL != pVert );
        if ( pVert->getOnOff() )
        {
            mpAutoCHs[ id++ ] = &arithAuto::mCHs[ ch - chan1 ];
        }
        else
        {}
    }

    //! auto count
    return id;
}

int servAuto::snapAllCH()
{
    for ( int i = 0; i < array_count(arithAuto::mCHs); i++ )
    { mpAutoCHs[ i ] = &arithAuto::mCHs[ i ]; }

    return array_count(arithAuto::mCHs);
}

int servAuto::snapOKCH( quint32 mask )
{
    int id = 0;
    for ( int i = 0; i < array_count(arithAuto::mCHs); i++ )
    {
        if ( get_bit(mask, i) )
        {
            mpOKCHs[ id++ ] = &arithAuto::mCHs[i];
        }
    }

    return id;
}

void servAuto::couplingAuto( aCH *pChs[], int cnt )
{
    dsoVert *pVert;

    if ( mbAutoKeepCoup )
    {
        for ( int i = 0; i < cnt; i++ )
        {
            pVert = dsoVert::getCH( (Chan)(chan1 + pChs[i]->getId() ) );
            Q_ASSERT( NULL != pVert );

            //! ac coupling
            if ( pVert->getCoupling() == AC )
            {
                pChs[i]->setCoupling( AC );
                pChs[i]->acCoupling();
            }
            //! keep dc
            else
            {
                pChs[i]->setCoupling( DC );
            }
        }
    }
    //! dc coupling
    //! default dc
    else
    {}
}

void servAuto::impedanceAuto(aCH *pChs[], int cnt)
{
    for ( int i = 0; i < cnt; i++ )
    {
        if ( pChs[i]->getImp() == IMP_50 )
        {
            pChs[i]->Impedance_50();
        }
    }
}

void servAuto::applyAuto( aCH *pChs[], int cnt, int autoCount )
{
    //! split
    if ( mbAutoPeak )
    { splitAC( pChs, cnt ); }
    else
    { splitDC( pChs, cnt ); }

    applyVert( pChs, cnt, autoCount);

    applyHori( pChs, cnt );

    applyTrig( pChs, cnt );

    applyRelated( pChs, cnt );

    applyUi( pChs, cnt );

}
void servAuto::splitDC( aCH *pChs[], int cnt )
{
    iterSplitDC( pChs, cnt, mbAutoOverlap );
}
void servAuto::splitAC( aCH *pChs[], int cnt )
{
    iterSplitAC( pChs, cnt, mbAutoOverlap );
}

void servAuto::applyVert( aCH *pChs[], int cnt, int autoCount)
{
    int       id;
    int       servId;
    int       scale;
    int       offset;
    Coupling  coup;
    Impedance imp;

    CCH *pCHCfg = NULL;

    //! desc order
    for( int i = cnt - 1; i >= 0; i-- )
    {
        id     = pChs[i]->getId();
        scale  = pChs[i]->getCfgScale();
        offset = pChs[i]->getCfgOffset();
        coup   = pChs[i]->getCoupling();
        imp    = pChs[i]->getImp();

        servId = E_SERVICE_ID_CH1 + id;

        //! delete in executor
        pCHCfg = new CCH();
        if ( NULL == pCHCfg )
        { continue; }

        //! config
        pCHCfg->setShowOnOff( true   );
        pCHCfg->setOffset(    offset );
        pCHCfg->setScale(     scale  );
        pCHCfg->setImpedance( imp    );
        pCHCfg->setInvert(    false  );
        pCHCfg->setCoupling(  coup   );

        if ( iterConst::bwScale( scale) )
        {
            pCHCfg->setBandwidth(BW_20M);
        }
        else
        {
            pCHCfg->setBandwidth(BW_OFF);
        }

        //! async in batch
        id_async( servId, servCH::cmd_ch_cfg, pCHCfg ); 

        //!active ch
        id_async( servId, CMD_SERVICE_ACTIVE, 1 );
    }

    //! other chans off
    for ( int i = 0; i < array_count(arithAuto::mCHs); i++ )
    {
        servId = E_SERVICE_ID_CH1 + arithAuto::mCHs[i].getId();

        if(mbAutoOpenCh)
        {
            id_async( servId, MSG_CHAN_ON_OFF, false );
        }
        else
        {
            id_async( servId, MSG_CHAN_ON_OFF, arithAuto::mCHs[i].getEnd() );
        }
    }

    //qDebug()<<__FILE__<<__LINE__<<"cnt:"<<autoCount;
    if(mbAutoOpenCh)
    {
        for(int i = 0; i < autoCount; i++)
        {
            servId = E_SERVICE_ID_CH1 + mpAutoCHs[i]->getId();
            id_async( servId, MSG_CHAN_ON_OFF, true ); //!已打开保持
        }
    }  
}
//! YT
//! NORMAL
void servAuto::applyHori( aCH *pChs[], int cnt )
{
    Q_UNUSED(pChs)
    Q_UNUSED(cnt)
    id_async(E_SERVICE_ID_HORI, servHori::cmd_clear_session, servHori::hori_ses_yt );

    id_async(E_SERVICE_ID_HORI, MSG_HOR_TIME_MODE,        (int)Acquire_YT         );
    id_async(E_SERVICE_ID_HORI, MSG_HOR_ACQ_MODE_YT,      (int)Acquire_Normal     );
    id_async(E_SERVICE_ID_HORI, MSG_HOR_ACQ_MEM_DEPTH,    (int)Acquire_Depth_Auto );
    id_async(E_SERVICE_ID_HORI, MSG_HOR_ACQ_ANTI_ALIASING, false                  );
    id_async(E_SERVICE_ID_HORI, MSG_HOR_ZOOM_ON,           false                  );

    qlonglong hScale;
    if ( mTrigId >= 0 )
    {
        double peri  = arithAuto::mCHs[mTrigId].getPeri();

        /*!* 为了防止频率边界两次auto档位不同, 计算档位时取频率的3位有效数字。*/
        bool ok = false;
        double peri3 = QString::number(peri,'g', 3).toDouble(&ok);
        if(!ok)
        {peri3 = peri;}
        //qDebug()<<__FILE__<<__LINE__<<peri<<peri3;

        hScale = iterConst::fixTimeScale( peri3, 1.5);

        qlonglong mhScale = sysGetMinHScale();
        if ( hScale < mhScale )
        { hScale = mhScale; }

        id_async(E_SERVICE_ID_HORI, MSG_HORI_MAIN_SCALE, hScale);
    }
    //! use the def scale
    else
    {
        id_async(E_SERVICE_ID_HORI, MSG_HORI_MAIN_SCALE, time_ms(1));
    }

    /*! [dba, 2018-04-18], bug: 2942, 需要先改档位，后改偏移*/
    id_async(E_SERVICE_ID_HORI, MSG_HORI_MAIN_OFFSET, (qlonglong)0);
    id_async(E_SERVICE_ID_HORI, MSG_HORIZONTAL_RUN, (int)Control_Run);
}
void servAuto::applyTrig( aCH */*pChs*/[], int /*cnt*/ )
{
    if ( mTrigId >= 0 )
    {
        async( MSG_TRIGGER_TYPE, Trigger_Edge,               serv_name_trigger );
        async( MSG_TRIGGER_SWEEP, (int)Trigger_Sweep_Auto,   serv_name_trigger );
        async( MSG_TRIGGER_HOLDOFF, time_ns(16),             serv_name_trigger_edge );
        async( MSG_TRIGGER_SOURCE_LA_EXT_AC, mTrigId+chan1,  serv_name_trigger_edge );
        async( MSG_TRIGGER_EDGE_A, (int)Trigger_Edge_Rising, serv_name_trigger_edge );
        async( MSG_TRIGGER_COUPLING, (int)DC,                serv_name_trigger_edge );//![dba,bug:交流耦合时auto后触发不住]

        //! trigger level
        if ( arithAuto::mCHs[mTrigId].getCoupling() == AC )
        {
            async( MSG_TRIGGER_LEVEL, 0ll, serv_name_trigger );
        }
        else
        {
            async( MSG_TRIGGER_LEVEL,
                   (qlonglong)arithAuto::mCHs[mTrigId].getTrigLevel(),
                   serv_name_trigger );
        }
    }
    else
    {
        async( MSG_TRIGGER_SWEEP, (int)Trigger_Sweep_Auto, serv_name_trigger_edge );
    }

    async( MSG_TRIGGER_APPLY_LEVEL, 1 , serv_name_trigger );
}

void servAuto::applyRelated( aCH */*pChs*/[], int /*cnt*/ )
{
    async( CMD_SERVICE_RST, 0, serv_name_display );

    async( CMD_SERVICE_STARTUP, 0, serv_name_display );
    async( CMD_SERVICE_STARTUP, 0, serv_name_measure );

    async( CMD_SERVICE_STARTUP, 0, serv_name_la );

    id_async(E_SERVICE_ID_COUNTER, CMD_SERVICE_STARTUP, 0);
    id_async(E_SERVICE_ID_DVM,     CMD_SERVICE_STARTUP, 0);
}

void servAuto::applyUi( aCH */*pChs*/[], int /*cnt*/ )
{
    bool bPeriEn = mTrigId >=0 ;
    mUiAttr.setVisible( MSG_AUTO_CYCLE, bPeriEn );
    mUiAttr.setVisible( MSG_AUTO_NCYCLE, bPeriEn );
    mUiAttr.setVisible( MSG_AUTO_RISE, bPeriEn );
    mUiAttr.setVisible( MSG_AUTO_FALL, bPeriEn );

    if(!bPeriEn)
    {
        servGui::showInfo(ERR_AUTO_OVERRANGE_SIGNAL);
    }
}

void servAuto::saveSession()
{
    if ( NULL != m_pSession )
    {
        /*! [dba,2018-03-07, 将auto撤销改为恢复全部设置，解决zoom模式恢复后没波形以及类似问题] */
        servDso::pullSession( *m_pSession/*,mSessionList*/ );
    }

    //! not self
    int id = getActiveService();
    if ( id != getId() )
    { mActiveServ = id; }

    //! save impedance
    for ( int ch = chan1; ch <= chan4; ch++ )
    {
        mCHs[ ch - chan1].setImp( dsoVert::getCH( (Chan)ch)->getImpedance() );
    }
}
void servAuto::loadSession( bool bLoad )
{
    if ( NULL != m_pSession && bLoad )
    {
        //! recover session
        servDso::pushSession( *m_pSession );

        delete m_pSession;
        m_pSession = NULL;
    }

    //![dba bug 1973]
    if ( !bLoad)  //!auto 失败 重新重新配置受影响的项目
    {
        /*! [dba,2018-03-07, 将auto撤销改为恢复全部设置，解决zoom模式恢复后没波形以及类似问题] */
        //! startup again
        foreach( QString str, mSessionList )
        {
            async( CMD_SERVICE_STARTUP, (int)0, str );
        }
    }
    else//!auto 撤销，恢复auto前保存的设置。
    {
        if ( NULL != m_pSession )
        {
            //! recover session
            servDso::pushSession( *m_pSession );
            delete m_pSession;
            m_pSession = NULL;
        }

        QList<service*>* pServList = serviceFactory::getServiceList();
        int count = pServList->size() ;

        for(int i=0; i<count; i++)
        {
            int servId = pServList->at(i)->getId();

            //DG模块独立与示波器，不用恢复设置。
            if( (E_SERVICE_ID_SOURCE1 == servId)
                    || (E_SERVICE_ID_SOURCE2 == servId) )
            {
                continue;
            }

            serviceExecutor::post(servId, CMD_SERVICE_STARTUP, (int)0);
        }
    }
}

DsoErr servAuto::autoOption(CIntent */*pIntent*/)
{
    //qDebug() << "auto option" << pIntent->mMsg;
    return ERR_NONE;
}

DsoErr servAuto::setAutoEnable(bool en)
{
    mbAutoEnable = en;
    return ERR_NONE;
}

DsoErr servAuto::loadPassword()
{
    int idBase = servDso::getIdBase( getId() ) + NVRAM_ID_PASSWD;

    int size = servDso::getLen( idBase );

    mPassword = "";
    if( size > 0)
    {
        //! keyData Format: "curveID;pubKey str"
        char keyData[ 8 ];
        memset(keyData, 0, sizeof(keyData));
        int loadSize = servDso::load( idBase, keyData, size);
        if( loadSize == size )
        {
            mPassword.append(keyData);
        }
    }
    return ERR_NONE;
}

DsoErr servAuto::doNull(int)
{
    return ERR_NONE;
}



DsoErr servAuto::setPassword(QString pwd)
{
    int idBase = servDso::getIdBase( getId() ) + NVRAM_ID_PASSWD;

    if( pwd.compare(AUTO_DEFAULT_PASSWORD) == 0 || pwd.size() == 0 )
    {
        mPassword = "";
        servDso::remove( idBase );
    }
    else
    {
        mPassword = pwd;
        servDso::save( idBase, pwd.toLocal8Bit().data(), pwd.size() );
    }
    return ERR_NONE;
}

QString servAuto::getPassword()
{
    return mPassword;
}
