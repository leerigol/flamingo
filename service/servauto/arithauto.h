#ifndef ARITHAUTO_H
#define ARITHAUTO_H

#include "./impauto.h"
#include "./iterauto.h"

/**
 * @brief The arithAuto class: arithmetic\n
 * 扫通道峰峰值,拆分屏幕，设置合适垂直档位
 *
 */
class arithAuto : public impAuto
{
public:
    arithAuto();

protected:
    void preAuto();
    void postAuto();

    quint32 iterAuto(aCH *pChs[], int &chCnt );
    void    iterPrepare( iterData *pData );
    void    iterImpedance();

    bool iterInit(iterData *pData, aCH *pChs[], int &chCnt );
    void iterLoop( iterData *pData, aCH *pChs[], int chCnt );
    void iterResultApply( aCH *pChs[], int chCnt );

    void iterTrig( aCH *pChs[], int chCnt );
    int  iterFreq( aCH *pChs[], int chCnt );

    void iterAcCoupling( aCH *pChs[], int chCnt );

    void iterSplitDC( aCH *pChs[], int chCnt, bool bOverlap );
    void iterSplitAC( aCH *pChs[], int chCnt, bool bOverlap );

    void iterSplitDC_Seperate( aCH *pChs[], int chCnt );
    void iterSplitDC_Overlap( aCH *pChs[], int chCnt );

    void iterSplitAC_Seperate( aCH *pChs[], int chCnt );
    void iterSplitAC_Overlap( aCH *pChs[], int chCnt );

    void _scanAuto();

protected:
    void wait_ms( int ms );

protected:
    aCH mCHs[4];
    int mTrigId;
};

#endif // ARITHAUTO_H
