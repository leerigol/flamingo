#ifndef SESSIONAUTO_H
#define SESSIONAUTO_H

#include <QtCore>

//! session for 'undo'
class sessionAuto
{
public:
    sessionAuto();
    ~sessionAuto();

protected:
    QStringList mSessionList;
    QByteArray *m_pSession;
    int mActiveServ;
};

#endif // SESSIONAUTO_H
