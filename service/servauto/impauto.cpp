#include "impauto.h"

impAuto::impAuto()
{
    m_pDsoPlatform = NULL;
}
void impAuto::attachPlatform( cplatform *pPlatform )
{
    Q_ASSERT( NULL != pPlatform );

    m_pDsoPlatform = pPlatform;
}
IPhyCH* impAuto::getCH( int i )
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    Q_ASSERT( i >= 0 && i <= 3 );

    return &m_pDsoPlatform->m_pPhy->m_ch[ i ];
}
IPhyScu *impAuto::getScu( int id )
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return m_pDsoPlatform->m_pPhy->mScuGp[ id ];
}
IPhySpu *impAuto::getSpu( int id )
{
    Q_ASSERT( NULL != m_pDsoPlatform );

    return m_pDsoPlatform->m_pPhy->mSpuGp[ id ];
}
IPhyScuGp *impAuto::getScuGp()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mScuGp;
}
IPhySpuGp *impAuto::getSpuGp()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mSpuGp;
}
IPhyTpu *impAuto::getTpu()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mTpu;
}
IPhyFrequ *impAuto::getCounter()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mCounter;
}
IPhyWpu *impAuto::getWpu()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mWpu;
}
IPhyCcu *impAuto::getCcu()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return &m_pDsoPlatform->m_pPhy->mCcu;
}
CDsoRecEngine *impAuto::getEngine()
{
    Q_ASSERT( NULL != m_pDsoPlatform );
    return (CDsoRecEngine *)m_pDsoPlatform->m_pRecEngine;
}
