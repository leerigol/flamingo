#include "sessionauto.h"

sessionAuto::sessionAuto()
{
    m_pSession = NULL;
}

sessionAuto::~sessionAuto()
{
    if ( NULL != m_pSession )
    {
        delete m_pSession;
    }
}
