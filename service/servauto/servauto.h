#ifndef SERVAUTO_H
#define SERVAUTO_H

#include "../service.h"
#include "../../baseclass/iserial.h"

#include "./arithauto.h"
#include "./sessionauto.h"

#define  AUTO_DEFAULT_PASSWORD  QString("EMPTY")

/**
 * @brief The servAuto class
 */
class servAuto : public serviceExecutor,
                 public ISerial,
                 public arithAuto,
                 public sessionAuto
{
    Q_OBJECT

    DECLARE_CMD()

public:
    enum autoCmd
    {
        cmd_none = 0,
        cmd_lock_auto,
        cmd_scpi_lock_auto,
        cmd_async_active,
        cmd_auto_enable,
        cmd_auto_password,
        cmd_auto_unlock,
    };

public:
    servAuto( QString name,
              ServiceId id=E_SERVICE_ID_NONE );
    ~servAuto();

public:
    virtual int serialOut( CStream &stream, serialVersion &ver );
    virtual int serialIn( CStream &stream, serialVersion ver );
    virtual void rst();
    virtual int startup();
    virtual void init();

protected:
    DsoErr onLoad();
    DsoErr onAuto();
    DsoErr onDeactive( int para );

protected:
    void preAuto();
    void postAuto();

    DsoErr doAuto();

    void onSinglePeriod();
    void onMultiPeriod();
    void onRise();
    void onFall();

    void onReturn();
    void onUndo();

    DsoErr setLock( bool bLock );
    bool getLock();

    DsoErr lockAuto(bool);
    DsoErr scpiLockAuto(bool en);
    bool   scpiGetLock();

    DsoErr setPeak( bool bPeak );
    bool getPeak();

    DsoErr setOpenCh( bool bOpened );
    bool getOpenCh();

    DsoErr setOverlap( bool bOverlap );
    bool getOverlap();

    DsoErr setKeepCoup( bool bKeep );
    bool getKeepCoup();

    DsoErr autoOption(CIntent *pIntent);

    DsoErr setAutoEnable(bool en);

    DsoErr doNull(int);
    DsoErr loadPassword();
    DsoErr setPassword(QString pwd);
    QString getPassword();

protected:
    int snapOpenedCH();
    int snapAllCH();
    int snapOKCH( quint32 mask );

    void couplingAuto(  aCH *pChs[], int cnt );
    void impedanceAuto( aCH *pChs[], int cnt );

    void applyAuto(aCH *pChs[], int cnt , int autoCount);
    void splitDC( aCH *pChs[], int cnt );
    void splitAC( aCH *pChs[], int cnt );

    void applyVert(aCH *pChs[], int cnt , int autoCount);
    void applyHori( aCH *pChs[], int cnt );
    void applyTrig( aCH *pChs[], int cnt );

    void applyRelated( aCH *pChs[], int cnt );

    void applyUi( aCH *pChs[], int cnt );

    void saveSession();
    void loadSession( bool bLoad = true );

protected:
    bool mbAutoLock;
    bool mbAutoPeak;
    bool mbAutoOpenCh;
    bool mbAutoOverlap;
    bool mbAutoKeepCoup;
    bool mbAutoEnable;

protected:
    aCH  *mpAutoCHs[4];
    aCH  *mpOKCHs[4];

private:
    QString mPassword;
};

#endif // SERVAUTO_H
