
#include "arithauto.h"
#include "iterauto.h"

#ifdef _DEBUG
#define ch_idle_time    1           //! ms
#else
#define ch_idle_time    1
#endif

#define peak_time       22          //! about 12ms

#define foreach_ch( index, chs, chCnt )    for( int index = 0; index < (chCnt); index++ )

arithAuto::arithAuto()
{
}

//! 4 channel mode
//! 1M impedance
//! dc coupling
//! trig auto, holdoff = min
void arithAuto::preAuto()
{
    foreach_ch( i, mCHs, array_count(mCHs) )
    {
        mCHs[i].rst();
    }

    mTrigId = -1;

    //! wpu off
    IPhyWpu *pWpu = getWpu();
    pWpu->outdisplay_en( 2 );
    pWpu->setanalog_mode_analog_ch_en( 0 );
    pWpu->setLA_en( 0 );
    pWpu->flushWCache();
    pWpu->outdisplay_en( 3 );
}
void arithAuto::postAuto()
{
}

quint32 arithAuto::iterAuto( aCH *pChs[], int &chCnt )
{
    //! prepare
    iterData data;
    data.attachPhy( getSpu(0) );

    iterPrepare( &data );
    iterImpedance();

    //! init
    bool bEnd;
    bEnd = iterInit( &data, pChs, chCnt );

    //! iter
    if ( !bEnd )
    {
        LOG_DBG()<<"ch count:"<<chCnt;
        iterLoop( &data, pChs, chCnt );
    }

    //!apply result [dba+; bug:1700]
    iterResultApply(pChs, chCnt);

    //! trig
    iterTrig( pChs, chCnt );

    //! the mask
    quint32 ret = 0;
    foreach_ch( i, pChs, chCnt )
    {
        bEnd = pChs[i]->getEnd();
        set_x_bit( ret, pChs[i]->getId(), bEnd );
    }

    return ret;
}

void arithAuto::iterPrepare( iterData *pData )
{
    Q_ASSERT( NULL != pData );

    //! all on
    foreach_ch( i, chs, array_count(mCHs) )
    {
        mCHs[i].attachPhy( getCH( i ) );
        mCHs[i].attachData( pData, i );

        mCHs[i].attachEngine( getEngine() );
        mCHs[i].attachCounter( getCounter() );
        getCH(i)->setOnOff( true );
        getCH(i)->flushWCache();
    }

    //! 4 channels
    LOG_DBG();
    getEngine()->calAdcGroup( dso_phy::IPhyADC::ch_abcd );
    getEngine()->flushWCache();
}

void arithAuto::iterImpedance()
{
    int adcInv = 0xf;
    //! all on
    foreach_ch( i, chs, array_count(mCHs) )
    {
        bool bImp = false;
        serviceExecutor::query(serviceExecutor::getServiceName(E_SERVICE_ID_CH1+i),
                               MSG_CHAN_IMPEDANCE,
                               bImp);

        mCHs[i].setImp( (Impedance)bImp);

        //!50欧时硬件是反向的。
        if(IMP_50 == (Impedance)bImp )
        {
            adcInv = adcInv & (~(1<<i));
        }
    }

    getSpuGp()->outCTRL_adc_data_inv(adcInv);
}

bool arithAuto::iterInit( iterData *pData, aCH *pChs[], int &chCnt )
{
    Q_ASSERT( NULL != pData );

    int initCnts = pChs[0]->getInitCnt();
    quint32 ret;
    bool bHasNext;

    for ( int i = 0; i < initCnts; i++ )
    {
        ret = 0;

        foreach_ch( j, chs, chCnt )
        {
            pChs[j]->init( i );
        }

        //! wait idle
        wait_ms( ch_idle_time );

        //! read data
        pData->read();

        foreach_ch( j, chs, chCnt )
        {
            pChs[j]->result();
        }

        //! check ret
        foreach_ch( j, chs, chCnt )
        {
            bHasNext = pChs[j]->hasNext();
            set_x_bit( ret, j, bHasNext );
        }

        //! end
        if ( ret == 0 )
        {return true;}
    }

   /*[dba]******************************************************************************
    * 如果ret = 0，表示后续步骤不需要迭代过程。
    * 如果ret ！= 0；表示迭代过程没有结束，需要后续继续迭代，前面迭代失败的不需要再重新迭代。
   **************************************************************************************/
    aCH *tepCHs[4];
    int  endCount = 0;
    for ( int i = 0; i < chCnt; i++ )
    {
        if( pChs[i]->getEnd() || pChs[i]->hasNext() )
        {
            tepCHs[endCount] = pChs[i];
            endCount++;
            Q_ASSERT(endCount<=4);
        }
    }

    for ( int i = 0; i < endCount; i++ )
    {
        pChs[i] = tepCHs[i];
    }
    chCnt = endCount;
    /****************************************************/

    return false;
}
void arithAuto::iterLoop( iterData *pData, aCH *pChs[], int chCnt )
{
    Q_ASSERT( NULL != pData );
    Q_ASSERT( NULL != pChs );

    quint32 ret;
    bool bHasNext;

    //! now iter
    do
    {
        ret = 0;
        foreach_ch( i, chs, chCnt )
        {
            pChs[i]->step();

            wait_ms( ch_idle_time );

            pData->read();

            pChs[i]->result();

            bHasNext = pChs[i]->hasNext();

            set_x_bit( ret, i, bHasNext );
        }

    }while( ret != 0 );
}

void arithAuto::iterResultApply(aCH *pChs[], int chCnt)
{
    Q_ASSERT( NULL != pChs );
    foreach_ch( i, chs, chCnt )
    {
        if ( pChs[i]->getEnd() )
        {
             pChs[i]->resultApply();
        }
    }
}

void arithAuto::iterTrig( aCH *pChs[], int chCnt )
{
    Q_ASSERT( NULL != pChs );

    foreach_ch( i, chs, chCnt )
    {
        { pChs[i]->dbgShow(); }
    }

    //! for trig
    mTrigId = iterFreq( pChs, chCnt );
    LOG_DBG()<<mTrigId;
}

int arithAuto::iterFreq( aCH *pChs[], int chCnt )
{
    Q_ASSERT( NULL != pChs );

    foreach_ch( i, chs, chCnt )
    {
        //! trig detect
        if ( pChs[i]->getEnd() )
        {
            if ( pChs[i]->scanFreq() )
            { return pChs[i]->getId(); }
        }
    }

    return -1;
}

void arithAuto::iterAcCoupling( aCH *pChs[], int chCnt )
{
    Q_ASSERT( NULL != pChs );

    foreach_ch( i, chs, chCnt )
    {
        pChs[i]->acCoupling();
    }
}

//! amp
void arithAuto::iterSplitDC( aCH *pChs[], int chCnt, bool bOverlap )
{
    Q_ASSERT( NULL != pChs );

    if ( bOverlap )
    {
        iterSplitDC_Overlap( pChs, chCnt );
    }
    else
    {
        iterSplitDC_Seperate( pChs, chCnt );
    }
}

//! peak - peak
void arithAuto::iterSplitAC( aCH *pChs[], int chCnt, bool bOverlap )
{
    Q_ASSERT( NULL != pChs );

    if ( bOverlap )
    {
        iterSplitAC_Overlap( pChs, chCnt );
    }
    else
    {
        iterSplitAC_Seperate( pChs, chCnt );
    }
}

void arithAuto::iterSplitDC_Seperate( aCH *pChs[], int chCnt )
{
    Q_ASSERT( NULL != pChs );

    int fixScale, fixOffset;
    int top, bottom;
    foreach_ch( i, chs, chCnt )
    {
        //! show the gnd
        top = pChs[i]->top() < 0 ? 0 : pChs[i]->top();
        bottom = pChs[i]->bottom() > 0 ? 0 : pChs[i]->bottom();
        Impedance imp = pChs[i]->getImp();
        Q_ASSERT( chCnt > 0 );
        fixScale = iterConst::fixScale( top,
                                        bottom,
                                        &fixOffset,
                                        (float)vert_div/chCnt,
                                        (chCnt-2*i-1)*4/chCnt,
                                        imp
                                        );

        pChs[i]->setCfgInfo( fixScale, fixOffset );
    }
}

void arithAuto::iterSplitDC_Overlap( aCH *pChs[], int chCnt )
{
    int fixScale, fixOffset;
    int top, bottom;
    foreach_ch( i, chs, chCnt )
    {
        //! show the gnd
        top = pChs[i]->top() < 0 ? 0 : pChs[i]->top();
        bottom = pChs[i]->bottom() > 0 ? 0 : pChs[i]->bottom();
        Impedance imp = pChs[i]->getImp();
        fixScale = iterConst::fixScale( top,
                                        bottom,
                                        &fixOffset,
                                        vert_div,
                                        0.0f,
                                        imp);

        pChs[i]->setCfgInfo( fixScale, fixOffset );
    }
}

void arithAuto::iterSplitAC_Seperate( aCH *pChs[], int chCnt )
{
    int fixScale, fixOffset;

    foreach_ch( i, pChs, chCnt )
    {
        Q_ASSERT( chCnt > 0 );
        Impedance imp = pChs[i]->getImp();

        fixScale = iterConst::fixScale( pChs[i]->top(),
                                        pChs[i]->bottom(),
                                        &fixOffset,
                                        (float)vert_div/chCnt,
                                        (chCnt-2*i-1)*4/chCnt,
                                        imp
                                        );

        pChs[i]->setCfgInfo( fixScale, fixOffset );
    }

}
void arithAuto::iterSplitAC_Overlap( aCH *pChs[], int chCnt )
{
    int fixScale, fixOffset;
    foreach_ch( i, pChs, chCnt )
    {
        Impedance imp = pChs[i]->getImp();
        fixScale = iterConst::fixScale( pChs[i]->top(),
                                        pChs[i]->bottom(),
                                        &fixOffset,
                                        vert_div,
                                        0.0f,
                                        imp);

        pChs[i]->setCfgInfo( fixScale, fixOffset );
    }
}

void arithAuto::_scanAuto()
{
    IPhyCH *pCH;
    IPhySpu *pSpu;

    pCH = getCH( 0 );
    pSpu = getSpu( 0 );

    int scales[]={ 2000,5000,10000,
                   20000,50000,100000,
                   200000,500000,1000000,
                   2000000,5000000,10000000,
                 };
    int peaks[8];
    for ( int i = 0; i < array_count(scales); i++ )
    {
        //! config
        pCH->setScale( scales[i] );
        pCH->setOffset( 0 );

        //! wait idle
        QThread::msleep( ch_idle_time );

        //! get peak
        {
            //! get peak
            pSpu->startAdcPeak( peak_time );
            m_pDsoPlatform->m_pPhy->flushWCache();

            while ( !pSpu->getAdcPeakDone() )
            { QThread::msleep( 1 ); }

            pSpu->stopAdcPeak();
            pSpu->flushWCache();

            //! read peak
            pSpu->getAdcPeaks( peaks );
        }

        //! show peaks
        for ( int j = 0; j < array_count(peaks); j+=2 )
        {
            LOG_DBG()<<peaks[j]<<peaks[j+1];
        }
    }
}

void arithAuto::wait_ms( int ms )
{
    QThread::msleep( ms );
}


