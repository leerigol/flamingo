//#define  _DEBUG  1
#include "iterauto.h"
#include "../../include/dsodbg.h"

//! configs
#define iter_max 255
#define iter_min 0

#define noise_pp  (25)
#define trig_pp   (25)

//! 20 -- 3ms/
#define peak_idle_time  23  //! 8 * 3 = 24

#define IMP_INDEX( imp )    ( (imp) - IMP_1M )

//! scales
#define IMP_1M_MAX_SCLAE    10000000
#define IMP_50_MAX_SCLAE    1000000

//! point
iterPoint::iterPoint()
{
    mScale = 0;
    mOffset = 0;
    mAdcMax = 0;
    mAdcMin = 0;
}

void iterPoint::set( int scale, int offset, int mMa, int mMi )
{
    mScale = scale;
    mOffset = offset;
    mAdcMax = mMa;
    mAdcMin = mMi;
}
//! point -- data
iterData::iterData()
{
    m_pSpu = NULL;

    for ( int i = 0; i < array_count(mPeaks); i++ )
    {
        mPeaks[i] = 0;
    }
}

void iterData::attachPhy( IPhySpu *pSpu )
{
    Q_ASSERT( NULL != pSpu );

    m_pSpu = pSpu;
}

void iterData::read()
{
    Q_ASSERT( NULL != m_pSpu );

    //! 读取三次峰峰值
    //! (3.2ns) * (2^23) * 3 = 80.5ms [12Hz].
    int aa_peaks[3][8];
    for(int i=0; i<3; i++ )
    {
        m_pSpu->startAdcPeak( peak_idle_time );
        m_pSpu->flushWCache();

        while ( !m_pSpu->getAdcPeakDone() )
        { QThread::msleep( 1 ); }

        m_pSpu->stopAdcPeak();
        m_pSpu->flushWCache();

        //! read peak
        m_pSpu->getAdcPeaks( aa_peaks[i] );
    }

    //!取三次峰峰值的中值为结果
    for(int i = 0; i < 8; i++)
    {    //!排序
        for(int j = 0; j < (3-1); j++)
        {
            for(int k = j+1; k < 3; k++)
            {
                if(aa_peaks[j][i] > aa_peaks[k][i])
                {
                    int temp         = aa_peaks[j][i];
                    aa_peaks[j][i]   = aa_peaks[k][i];
                    aa_peaks[k][i] = temp;
                }
            }
        }
        mPeaks[i] = aa_peaks[1][i];
    }

    LOG_DBG()<<mPeaks[0]<<mPeaks[1];
    /*LOG_DBG()<<mPeaks[2]<<mPeaks[3];
    LOG_DBG()<<mPeaks[4]<<mPeaks[5];
    LOG_DBG()<<mPeaks[6]<<mPeaks[7];*/
}

void iterData::getData(int id, int *pMax, int *pMin)
{
    Q_ASSERT( NULL != pMax && NULL != pMin );

    Q_ASSERT( id >= 0 && id < 4 );

    *pMax = mPeaks[ 2*id ];
    *pMin = mPeaks[ 2*id+1 ];
}

#define V_N(n)  (n*1000000)
#define _1V_        V_N(1)
#define _30V_       V_N(30)
#define _100V_      V_N(100)

#define _4V_        V_N(4)

static struCHInfo _ch_infos[]=
{
    //! [0] -- 1M, [1] -- 50ohm
    //! \todo noise too large
    //    { 1000,     {{_1V_,   noise_pp, true},{_1V_,   noise_pp, true}} },
    //    { 2000,     {{_1V_,   noise_pp, true},{_1V_,   noise_pp, true}} },
    { 5000,     {{_1V_,   noise_pp, true},{_1V_,   noise_pp, true}} },

    { 10000,    {{_1V_,   noise_pp, false},{_1V_,   noise_pp, false}} },
    { 20000,    {{_1V_,   noise_pp, false},{_1V_,   noise_pp, false}} },
    { 50000,    {{_1V_,   noise_pp, false},{_1V_,   noise_pp, false}} },

    { 100000,   {{_30V_,  noise_pp, false},{_1V_,   noise_pp, false}} },
    { 200000,   {{_30V_,  noise_pp, false},{_4V_,   noise_pp, false}} },
    { 500000,   {{_100V_, noise_pp, false},{_4V_,   noise_pp, false}} },

    { 1000000,  {{_100V_, noise_pp, false},{_4V_,   noise_pp, false}} },
    { 2000000,  {{_100V_, noise_pp, false},{_4V_,   noise_pp, false}} },
    { 5000000,  {{_100V_, noise_pp, false},{_4V_,   noise_pp, false}} },

    { 10000000, {{_100V_, noise_pp, false},{_4V_,   noise_pp, false}} },
};

int iterConst::indexToScale( int index )
{
    Q_ASSERT( index >= 0 && index < array_count(_ch_infos) );

    return _ch_infos[ index ].scale;
}
int iterConst::scaleToIndex( int scale )
{
    for ( int i = 0; i < array_count(_ch_infos); i++ )
    {
        if ( _ch_infos[i].scale == scale )
        { return i; }
    }

    Q_ASSERT( false );
    return 0;
}

int iterConst::maxScale( Impedance imp )
{
    if ( imp == IMP_1M )
    { return IMP_1M_MAX_SCLAE;}
    else
    { return IMP_50_MAX_SCLAE; }
}

int iterConst::maxScaleIndex( Impedance imp )
{
    return scaleToIndex( maxScale(imp) );
}

int iterConst::scaleNoise( int scale, Impedance imp )
{
    int index;

    index = scaleToIndex( scale );

    return _ch_infos[index].infos[ IMP_INDEX(imp)].noise;
}

bool iterConst::scaleLimitOffset( int scale, int &offset, Impedance imp )
{
    int index;
    bool bOver;

    index = scaleToIndex( scale );

    //! limit
    if ( offset > _ch_infos[index].infos[IMP_INDEX(imp)].offset )
    {
        offset = _ch_infos[index].infos[IMP_INDEX(imp)].offset;
        bOver = true;
    }
    else if ( offset < -_ch_infos[index].infos[IMP_INDEX(imp)].offset )
    {
        offset = -_ch_infos[index].infos[IMP_INDEX(imp)].offset;
        bOver = true;
    }
    //! do not limit
    else
    { bOver = false; }

    return bOver;
}

bool iterConst::bwScale( int scale, Impedance imp )
{
    int index;

    index = scaleToIndex( scale );

    return _ch_infos[index].infos[ IMP_INDEX(imp) ].bw;
}

int iterConst::fixScale( int max, int min,
                         int *pOffset,
                         float divs,
                         float shiftDiv,
                         Impedance imp )
{
    int pp = max - min;
    int avg = (max + min )/2;

    int offset;

    //! from low to up
    for ( int i = 0; i <= maxScaleIndex(imp); i++ )
    {
        //! offset
        offset = -avg + shiftDiv * _ch_infos[i].scale;

        //! pp in range
        if ( pp <= _ch_infos[i].scale * divs )
        {
            //! offset in range
            if ( offset >= -_ch_infos[i].infos[IMP_INDEX(imp)].offset
                 && offset <= _ch_infos[i].infos[IMP_INDEX(imp)].offset )
            {
                (*pOffset) = offset;
                LOG_DBG()<<"max:"<<max<<"min:"<<min<<"avg:"<<avg
                        <<"offset:"<<offset<<"scale"<<_ch_infos[i].scale;
                return _ch_infos[i].scale;
            }
        }
    }

    //! limit the range
    if ( avg > _ch_infos[  maxScaleIndex(imp) ].infos[IMP_INDEX(imp)].offset )
    {
        avg = _ch_infos[  maxScaleIndex(imp) ].infos[IMP_INDEX(imp)].offset;
    }
    else if ( avg < _ch_infos[  maxScaleIndex(imp) ].infos[IMP_INDEX(imp)].offset )
    {
        avg = -_ch_infos[  maxScaleIndex(imp) ].infos[IMP_INDEX(imp)].offset;
    }
    else
    {}

    *pOffset = -avg;
    qDebug()<<__FILE__<<__LINE__<<"offset:"<<offset;

    return _ch_infos[  maxScaleIndex(imp) ].scale;
}

int iterConst::voltDot()
{ return adc_vdiv_dots; }

int iterConst::adcToValue( int adc, int scale, int gnd )
{
    return ( adc - gnd )*scale/voltDot();
}

qlonglong iterConst::fixTimeScale( double peri,
                                   float cycleCnt,
                                   int divs )
{
    qlonglong normPeri = peri * time_s(1);
    qlonglong hScale = normPeri * cycleCnt/ (divs);

    qlonglong roofScale;
    roof125( hScale, roofScale );
    LOG_DBG()<<hScale<<roofScale;
    return roofScale;
}

//! iter ret
iterRet::iterRet()
{
    rst();
}

void iterRet::rst()
{
    mbValid = false;
    mValue = 0;
}

void iterRet::setValue( int value )
{
    mbValid = true;
    mValue = value;
}
iterRet &iterRet::operator=(int val )
{
    setValue( val );
    return *this;
}

//! auto machine
iterExe::iterExe()
{
    rst();
}
iterExe::~iterExe()
{
    foreach( iterPoint *p, mIterList )
    {
        Q_ASSERT( NULL != p );

        delete p;
    }
}

void iterExe::rst()
{
    mbEnd = false;

    mbFail = false;
    mFailCause = fail_unk;

    mMax.rst();
    mMin.rst();

    mPeri = 0.0;
    mTrigLevel = 0;
}

int iterExe::getInitCnt()
{ return 2; }

void iterExe::init( int /*id*/ )
{}
bool iterExe::next()
{ return mbEnd; }

void iterExe::result()
{}

void iterExe::setEnd( int ma, int mi )
{
    mMax = ma;
    mMin = mi;

    mbEnd = true;
    mbFail = false;
}

void iterExe::setEnd( bool b )
{ mbEnd = b; }

bool iterExe::getEnd()
{ return mbEnd; }

void iterExe::setFail( bool b, failCause fail )
{
    mbFail = b;
    mFailCause = fail;
}

bool iterExe::getFail( failCause &cause )
{
    cause = mFailCause;
    return mbFail;
}

void iterExe::setMax( int ma )
{
    mMax = ma;
}

void iterExe::setMin( int mi )
{
    mMin = mi;
}

int iterExe::top()
{ return mMax.mValue; }

int iterExe::bottom()
{ return mMin.mValue; }

int iterExe::mid()
{
    return ( mMax.mValue + mMin.mValue )/2;
}

double iterExe::getPeri()
{ return mPeri; }

int iterExe::getTrigLevel()
{
    return mTrigLevel;
}

void iterExe::getResult( iterRet *pMax, iterRet *pMin )
{
    Q_ASSERT( NULL != pMax && NULL != pMin );

    *pMax = mMax;
    *pMin = mMin;
}

void iterExe::dbgShow()
{
    LOG_DBG()<<mMax.mbValid<<mMax.mValue<<mMin.mbValid<<mMin.mValue;
    LOG_DBG()<<(mMax.mValue-mMin.mValue)<<(mMax.mValue+mMin.mValue)/2;
}

//! ch iter auto machine
aCH::aCH()
{
    rst();
}

void aCH::rst()
{
    //! base rst
    iterExe::rst();

    //! sub rst
    m_pCH = NULL;

    m_pData = NULL;
    mId = 0;

    m_pEngine = NULL;
    m_pCounter = NULL;

    mCoupling = DC;
    mImp = IMP_1M;
}

void aCH::attachPhy( IPhyCH *pCH )
{
    Q_ASSERT( NULL != pCH );
    m_pCH = pCH;
}

void aCH::attachData( iterData *pData, int id )
{
    Q_ASSERT( NULL != pData );

    m_pData = pData;
    mId = id;
}

int aCH::getId()
{ return mId; }

void aCH::attachEngine( dsoEngine *pEngine )
{
    Q_ASSERT( NULL != pEngine );

    m_pEngine = pEngine;
}

void aCH::attachCounter( IPhyFrequ *pCounter )
{
    Q_ASSERT( NULL != pCounter );

    m_pCounter = pCounter;
}

int aCH::getCfgScale()
{ return mCfgScale; }

int aCH::getCfgOffset()
{
    return mCfgOffset;
}

void aCH::setCoupling( Coupling coup )
{ mCoupling = coup; }

Coupling aCH::getCoupling()
{ return mCoupling; }

void aCH::setImp( Impedance imp )
{ mImp = imp; }

Impedance aCH::getImp()
{ return mImp; }

void aCH::init( int id )
{
    //! low scale
    if ( id == 0 )
    {
        mNowScale = iterConst::indexToScale(0);
        mNowOffset = 0;

        m_pCH->setCoupling( DC );
        applyNow();

        mNowScaleIndex = 0;
        mLowerIndex = 0;

        //! set the max for iter
        mUpperIndex = iterConst::maxScaleIndex( mImp );
    }
    //! up scale
    else if ( id == 1 )
    {
        mNowScale = iterConst::maxScale( mImp );
        mNowOffset = 0;

        applyNow();

        mNowScaleIndex = iterConst::maxScaleIndex();
        mUpperIndex = mNowScaleIndex;
    }
    else
    {}
}

bool aCH::hasNext()
{
    //! has completed
    if ( mbEnd ) return false;

    //! has fail
    if ( mbFail ) return false;

    return true;
}
void aCH::step()
{
    { applyNow(); }
}

/*!
 * \brief aCH::result
 * @verbatim
 *            5  4  3
 *            |  |  |
 *  ---------    |  |   6   2
 *               |  |   |   |
 *                  |   |   |
 *  ---------       |       |    1
 *                  |       |    |
 * @endverbatim
 * @see result_1(),result_2(),result_3(),result_4(),result_5(),result_6()
 */
void aCH::result()
{
    Q_ASSERT( m_pData != NULL );

    m_pData->getData( mId, &mNowMax, &mNowMin );

    mNowAfeGnd = m_pCH->getAfeGnd();
    mNowAfeScale = m_pCH->getAfeScale();

    // check result
    if ( mNowMax < iter_max )
    {
        if ( mNowMin > iter_min )
        {
            result_6();
        }
        //! min <= iter_min
        else
        {
            //! max ready
            if ( mNowMax > iter_min )
            {
                result_2();
            }
            //! <= min
            else
            {
                result_1();
            }
        }
    }
    //! mNowMax >= iterMax
    else
    {
        //! check min
        if ( mNowMin > iter_min )
        {
            //! min ready
            if ( mNowMin < iter_max )
            {
                result_4();
            }
            //! >= max
            else
            {
                result_5();
            }
        }
        //! mNowMin <= iterMin
        else
        {
            result_3();
        }
    }
}

/*!
 * \brief aCH::resultApply
 * .为解决峰峰优先不再屏幕中间的问题[bug：1700].
 * .问题分析：迭代档位成功后，计算峰峰值时的 AfeScale已经不合适，需要重新获取ADC值计算。
 */
void aCH::resultApply()
{
    if ( !mbEnd ) return;

    //! ch config
    int fixScale, fixOffset;
    fixScale = iterConst::fixScale( mMax.mValue, mMin.mValue, &fixOffset,
                                    10.0f, 0.0f, getImp() );

    chCfg( fixScale, fixOffset );
    LOG_DBG()<<mMax.mValue<<mMin.mValue<<fixScale<<fixOffset;
    //! wait stable
    m_pCH->waitIdle( 1 );

    //! read again
    m_pData->read();

    int aMa, aMi;
    m_pData->getData( mId, &aMa, &aMi );

    /*[dba+]*******************************************************/
    mNowAfeGnd   = m_pCH->getAfeGnd();
    mNowAfeScale = m_pCH->getAfeScale();
    int volt1 = iterConst::adcToValue( aMa, mNowAfeScale, mNowAfeGnd );
    int volt2 = iterConst::adcToValue( aMi, mNowAfeScale, mNowAfeGnd );
    setMax(volt1);
    setMin(volt2);
    LOG_DBG()<<"max:"<<volt1<<"min:"<<volt2<<"sacle:"<<mNowAfeScale<<"gnd"<<mNowAfeGnd;
    /********************************************************/
}

bool aCH::scanFreq()
{
    if ( !mbEnd ) return false;

    //! trig level

    //! ch config
    int fixScale, fixOffset;
    fixScale = iterConst::fixScale( mMax.mValue, mMin.mValue, &fixOffset,
                                    10.0f, 0.0f, getImp() );

    chCfg( fixScale, fixOffset );
    LOG_DBG()<<mMax.mValue<<mMin.mValue<<fixScale<<fixOffset;
    //! wait stable
    m_pCH->waitIdle( 1 );

    //! read again
    m_pData->read();

    int aMa, aMi;
    m_pData->getData( mId, &aMa, &aMi );

    //! pp low
    if ( (aMa - aMi) <= trig_pp )
    { return false; }

    //! trig level
    {
        int lH, lL;
        lH = (aMa + aMi)/2 + (aMa - aMi)/4;
        lL = (aMa + aMi)/2 - (aMa - aMi)/4;
        //! trig level
        Q_ASSERT( m_pEngine != NULL );
        m_pEngine->_setTrigLevelA( (Chan)(chan1 + mId),
                                   lL,
                                   lH );
        LOG_DBG()<<lH<<lL<<aMa<<aMi;

        //! trig edge
        TrigEdgeCfg edgeCfg;
        edgeCfg.setChan( (Chan)(mId + chan1) );
        edgeCfg.setSlope( Trigger_Edge_Rising );

        m_pEngine->setTrigEdgeCfg( &edgeCfg );
        m_pEngine->flushWCache();

        mTrigLevel = iterConst::adcToValue(
                    (aMa + aMi)/2,
                    m_pCH->getAfeScale(),
                    m_pCH->getAfeGnd()
                    );

        LOG_DBG()<<"trig level:"<<mTrigLevel;
    }

    double peri;
    do
    {
        FrequMemProxy frequ;
        frequ.assignfreq_cfg_1_src_sel(  mId + 1 );
        frequ.assignfreq_cfg_1_sel_gate( mId + 1 );
        frequ.assignfreq_cfg_1_gate_time( 2 );          //! 50ms/ for 20Hz

        peri = getPeri_freq( frequ );
        if ( peri > 0 )
        { break; }

        peri = getPeri_peri( frequ );

    }while ( 0 );

    //! success
    LOG_DBG()<<"peri:"<<peri;
    if ( peri > 0 )
    { mPeri = peri; }
    else
    { return false; }
    return true;
}

double aCH::getPeri_freq( FrequMemProxy &frequ )
{
    //! start counter
    frequ.assignfreq_cfg_1_ref_en( 1 );
    frequ.assignfreq_cfg_1_sel_mode( 0 );   //! freq
    m_pCounter->push( &frequ );
    m_pCounter->flushWCache();

    m_pCounter->waitGated();

    //! stop counter
    frequ.assignfreq_cfg_1_ref_en( 0 );
    m_pCounter->push( &frequ );
    m_pCounter->flushWCache();

    return m_pCounter->getPeri();
}

double aCH::getPeri_peri( FrequMemProxy &frequ )
{
    frequ.assignfreq_cfg_1_ref_en( 1 );
    frequ.assignfreq_cfg_1_sel_mode( 1 );   //! peri
    m_pCounter->push( &frequ );
    m_pCounter->flushWCache();

    m_pCounter->waitGated();
    m_pCounter->waitGated();

    frequ.assignfreq_cfg_1_ref_en( 0 );
    m_pCounter->push( &frequ );
    m_pCounter->flushWCache();

    return m_pCounter->getPeri();
}

void aCH::acCoupling()
{
    int avg = (mMax.mValue + mMin.mValue)/2;
    mMax.mValue -= avg;
    mMin.mValue -= avg;
}

void aCH::Impedance_50()
{
    int value = mMax.mValue;
    mMax.mValue = 256- mMin.mValue;
    mMin.mValue = 256- value;
    mTrigLevel  = (-mTrigLevel);
}

void aCH::setCfgInfo( int scale, int offset )
{
    mCfgScale = scale;
    mCfgOffset = offset;
}

void aCH::result_1()
{//LOG_DBG();
    int volt;

    //! guess the offset
    volt = iterConst::adcToValue( mNowMin, mNowAfeScale, mNowAfeGnd );
    mNowOffset = -volt;

    //! + scale
    mLowerIndex = mNowScaleIndex;

    //! upper now
    if ( mNowScaleIndex ==  mUpperIndex )
    {
        if ( iterConst::scaleLimitOffset( mNowScale, mNowOffset ) )
        {
            setFail( true, fail_min_over );
        }
        //! continue the offset
        else
        {}
    }
    //! + scale
    else
    {
        mNowScaleIndex = (mUpperIndex + mNowScaleIndex )/2;
        mNowScale = iterConst::indexToScale( mNowScaleIndex );
    }
}
void aCH::result_2()
{//LOG_DBG();
    int volt;

    //! guess the offset
    volt = iterConst::adcToValue( mNowMin, mNowAfeScale, mNowAfeGnd );
    mNowOffset = -volt;

    //! + scale
    mLowerIndex = mNowScaleIndex;

    //! max ready
    volt = iterConst::adcToValue( mNowMax, mNowAfeScale, mNowAfeGnd );
    setMax( volt );

    //! upper now
    if ( mNowScaleIndex ==  mUpperIndex )
    {
        if ( iterConst::scaleLimitOffset( mNowScale, mNowOffset ) )
        {
            setFail( true, fail_min_over );
        }
        //! continue the offset
        else
        {}
    }
    //! + scale
    else
    {
        mNowScaleIndex = (mUpperIndex + mNowScaleIndex )/2;
        mNowScale = iterConst::indexToScale( mNowScaleIndex );
    }
}
void aCH::result_3()
{//LOG_DBG();
    //! + scale
    if ( mNowScaleIndex == mUpperIndex )
    {
        setFail( true, fail_pp_large );
    }
    else
    {
        mLowerIndex = mNowScaleIndex;
        mNowScaleIndex = (mUpperIndex + mNowScaleIndex )/2;
        mNowScale = iterConst::indexToScale( mNowScaleIndex );
    }
}
void aCH::result_4()
{//LOG_DBG();
    int volt;

    //! guess the offset
    volt = iterConst::adcToValue( mNowMax, mNowAfeScale, mNowAfeGnd );
    mNowOffset = -volt;

    //! + scale
    mLowerIndex = mNowScaleIndex;

    //! min ready
    volt = iterConst::adcToValue( mNowMin, mNowAfeScale, mNowAfeGnd );
    setMin( volt );

    //! upper now
    if ( mNowScaleIndex ==  mUpperIndex )
    {
        if ( iterConst::scaleLimitOffset( mNowScale, mNowOffset ) )
        {
            setFail( true, fail_max_over );
        }
        //! continue the offset
        else
        {}
    }
    //! + scale
    else
    {
        mNowScaleIndex = (mUpperIndex + mNowScaleIndex )/2;
        mNowScale = iterConst::indexToScale( mNowScaleIndex );
    }
}
void aCH::result_5()
{//LOG_DBG();
    int volt;

    //! guess the offset
    volt = iterConst::adcToValue( mNowMax, mNowAfeScale, mNowAfeGnd );
    mNowOffset = -volt;

    //! + scale
    mLowerIndex = mNowScaleIndex;

    //! upper now
    if ( mNowScaleIndex ==  mUpperIndex )
    {
        if ( iterConst::scaleLimitOffset( mNowScale, mNowOffset ) )
        {
            setFail( true, fail_max_over );
        }
        //! continue the offset
        else
        {}
    }
    //! + scale
    else
    {
        mNowScaleIndex = (mUpperIndex + mNowScaleIndex )/2;
        mNowScale = iterConst::indexToScale( mNowScaleIndex );
    }
}
void aCH::result_6()
{//LOG_DBG();

    int volt1, volt2;
    volt1 = iterConst::adcToValue( mNowMax, mNowAfeScale, mNowAfeGnd );
    volt2 = iterConst::adcToValue( mNowMin, mNowAfeScale, mNowAfeGnd );
    /*[dba+]*******************************************************
    LOG_DBG()<<"Id:"<<getId()
             <<"adc max:"      <<mNowMax               <<"adc min:"    <<mNowMin
             <<"volt1:"        <<volt1                 <<"volt2:"      <<volt2
             <<"mNowAfeScale:" <<mNowAfeScale          <<"mNowAfeGnd:" <<mNowAfeGnd
             <<"getScale:"     <<m_pCH->getAfeScale()  <<"getGnd:"     <<m_pCH->getAfeGnd();
    ********************************************************/
    //! ac
    if ( (mNowMax - mNowMin ) >= iterConst::scaleNoise(mNowScale) )
    {
        setEnd( volt1, volt2 );
        return;
    }

    //! dc
    int avg = (mNowMax + mNowMin)/2;
    if ( abs( avg-mNowAfeGnd ) >= iterConst::scaleNoise(mNowScale)/2 )
    {
        setEnd( volt1, volt2 );
        return;
    }

    //! too low
    mUpperIndex = mNowScaleIndex;

    //! - scale
    if ( mNowScaleIndex == mLowerIndex )
    {
        setFail( true, fail_pp_small );
    }
    else
    {
        mNowScaleIndex = (mLowerIndex + mNowScaleIndex )/2;
        mNowScale = iterConst::indexToScale( mNowScaleIndex );
    }
}

void aCH::applyNow()
{
    //! tune offset
    iterConst::scaleLimitOffset( mNowScale, mNowOffset );

    chCfg( mNowScale, mNowOffset );
}

void aCH::chCfg( int scale, int offset )
{
    Q_ASSERT( NULL != m_pCH );

    m_pCH->setScale( scale );
    m_pCH->setOffset(offset );

    //! bw on
    if ( iterConst::bwScale( scale) )
    {
        m_pCH->setBw( BW_20M, mImp );
    }
    else
    {
        m_pCH->setBw( BW_OFF, mImp );
    }

    m_pCH->flushWCache();
}
