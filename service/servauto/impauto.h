#ifndef IMPAUTO_H
#define IMPAUTO_H

//! hw
#include "../../engine/cplatform.h"
#include "../../engine/record/cdsorecengine.h"

#include "../../phy/ch/pathcal.h"
#include "../../phy/scu/iphyscu.h"
#include "../../phy/spu/iphyspu.h"
#include "../../phy/adc/iphyadc.h"
#include "../../phy/wpu/iphywpu.h"

#include "../../phy/trig/iphytpu.h"
#include "../../phy/counter/iphycounter.h"

using namespace  dso_phy;

/**
 * @brief The impAuto class: implement auto   \n
 * 在auto中需要直接操作寄存器，通过这个类获取各个PHY \n
 */
class impAuto
{
public:
    impAuto();

public:
    void attachPlatform( cplatform *pPlatform );

protected:
    IPhyCH* getCH( int i );

    IPhyScu *getScu( int id = 0 );
    IPhySpu *getSpu( int id = 0 );
    IPhyScuGp *getScuGp();
    IPhySpuGp *getSpuGp();

    IPhyTpu *getTpu();
    IPhyFrequ *getCounter();

    IPhyWpu *getWpu();
    IPhyCcu *getCcu();

    CDsoRecEngine *getEngine();

protected:
    cplatform *m_pDsoPlatform;
};

#endif // IMPAUTO_H
