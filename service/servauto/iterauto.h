#ifndef ITERAUTO
#define ITERAUTO

#include "./impauto.h"

class iterPoint
{
public:
    iterPoint();

public:
    void set( int scale, int offset, int mMa, int mMi );

public:
    int mScale;
    int mOffset;
    int mAdcMax, mAdcMin;
};

class iterData
{
public:
    iterData();

public:
    void attachPhy( IPhySpu *pSpu );

public:
    void read();
    void getData(
                    int id,
                    int *pMax,
                    int *pMin );
public:
    IPhySpu *m_pSpu;
    int mPeaks[8];
};

struct struScaleInfo
{
    int offset;
    int noise;
    bool bw;
};

struct struCHInfo
{
    int scale;

    //! [0] -- imp1M
    //! [1] -- imp50
    struScaleInfo infos[2];
};

class iterConst
{
public:
    static int indexToScale( int index );
    static int scaleToIndex( int scale );
    static int maxScale( Impedance imp=IMP_1M );
    static int maxScaleIndex( Impedance imp=IMP_1M );

    //! pp noise in struCHInfo
    static int scaleNoise( int scale, Impedance imp=IMP_1M );

    static bool scaleLimitOffset( int scale, int &offset, Impedance imp=IMP_1M );
    static bool bwScale( int scale, Impedance imp=IMP_1M );

    static int fixScale( int max, int min,
                         int *pOffset,
                         float divs = 10.0f,
                         float shfitDif = 0.0f,
                         Impedance imp = IMP_1M );

    static int voltDot();

    static int adcToValue( int adc, int scale, int gnd );

    static qlonglong fixTimeScale( double peri,
                                   float cycle,
                                   int divs  = 10);
};

class iterRet
{
public:
    iterRet();
    void rst();
    void setValue( int value );
public:
    iterRet &operator=(int val );
public:
    bool mbValid;
    int mValue;
};

enum failCause
{
    fail_unk = 0,
    fail_pp_large,
    fail_pp_small,
    fail_max_over,
    fail_min_over,
};

//! an iter machine
class iterExe
{
public:
    iterExe();
    ~iterExe();
    void rst();
public:
    int getInitCnt();

    void init( int id );
    bool next();

    void result();

public:
    void setEnd( int ma, int mi );
    void setEnd( bool b );
    bool getEnd();

    void setFail( bool b, failCause failCause = fail_unk );
    bool getFail( failCause &cause );

    void setMax( int ma );
    void setMin( int mi );

    int  top();
    int  bottom();
    int  mid();

    double getPeri();
    int    getTrigLevel();

    void getResult( iterRet *pMax, iterRet *pMin );

    void dbgShow();

protected:
    QList< iterPoint *> mIterList;

    bool mbEnd;

    iterRet mMax, mMin;

    bool mbFail;
    failCause mFailCause;

    double mPeri;
    int mTrigLevel;
};

//! ch iter machine
class aCH : public iterExe
{
public:
    aCH();
    void rst();
public:
    void attachPhy( IPhyCH *pCH );
    void attachData( iterData *pData, int id );

    int getId();

    void attachEngine( dsoEngine *pEngine );
    void attachCounter( IPhyFrequ *pCounter );

    int getCfgScale();
    int getCfgOffset();

    void setCoupling( Coupling coup );
    Coupling getCoupling();

    void setImp( Impedance imp );
    Impedance getImp();

public:
    void init( int id );
    bool hasNext();
    void step();
    void result();
    void resultApply();

    bool   scanFreq();
    double getPeri_freq( FrequMemProxy &proxy );
    double getPeri_peri( FrequMemProxy &proxy );

    void   acCoupling();
    void   Impedance_50();

    void   setCfgInfo( int scale, int offset );
protected:
    //! results
    void result_1();
    void result_2();
    void result_3();
    void result_4();
    void result_5();
    void result_6();

    void applyNow();
    void chCfg( int scale, int offset );
public:
    IPhyCH *m_pCH;

    iterData *m_pData;
    int mId;

    dsoEngine *m_pEngine;
    IPhyFrequ *m_pCounter;

protected:
    //! iter vars
    int mNowScale, mNowOffset;
    int mNowMax, mNowMin;
    int mNowScaleIndex, mNowAfeScale, mNowAfeGnd;

    int mUpperIndex, mLowerIndex;

    //! apply vars
    int mCfgScale, mCfgOffset;
    Coupling mCoupling;
    Impedance mImp;
};

#endif // ITERAUTO

