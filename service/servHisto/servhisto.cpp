#include "servhisto.h"
#include "servhisto_thread.h"
#include "../servmeasure/servmeasure.h"

servHisto::servHisto(QString name, ServiceId id):serviceExecutor(name,id)
{
    m_pthread = new servhisto_thread;
    m_pthread->setClient(this);
    m_pHistoProvider = NULL;

    appendCacheList(this);

    m_bZoomHistoEn = false;
}

DsoErr servHisto::setDataProvider( void *ptr )
{
    Q_UNUSED( ptr );
    //m_pHistoProvider = (dataProvider*)ptr;
    if(m_dataSema.available() < 1 )
    {
        m_dataSema.release();
    }
    return ERR_NONE;
}

DsoErr servHisto::onSrcHoriChange()
{
    if( !m_histoEnable )
    {
        return ERR_NONE;
    }

    LOG_DBG();
    //! horizontal change   
    Chan ch = getAttrCh();
    horiAttr hAttr;
    hAttr = getHoriAttr( ch, horizontal_view_main);
#if 1
    DsoErr err;
    m_Range.posLeft = round(m_Range.realLeft/hAttr.inc + hAttr.gnd);
    //bug 2366 by zy
    err = limitRange(m_Range.posLeft, 0, wave_width-2);
    //err = limitRange(m_Range.posLeft, 0, wave_width-1);
    setuiChange(MSG_HISTO_LEFTPOS);
    if(err != 0)
    {
        //! 只有在超出屏幕范围的情况下才会去更新菜单上显示的值
        float temp;
        mUiAttr.setPostStr(MSG_HISTO_LEFTPOS ,horiFormat(m_Range.posLeft, temp));
    }

    m_Range.posRight = round(m_Range.realRight/hAttr.inc + hAttr.gnd);
    //bug 2366 by zy
    err = limitRange(m_Range.posRight, 1, wave_width-1);
    //err = limitRange(m_Range.posRight, 0, wave_width-1);
    setuiChange(MSG_HISTO_RIGHTPOS);
    if(err != 0)
    {
       //! 只有在超出屏幕范围的情况下才会去更新菜单上显示的值
       float temp;
       mUiAttr.setPostStr(MSG_HISTO_RIGHTPOS ,horiFormat(m_Range.posRight, temp));
    }
#else
    int Lpos = 0;
    int Rpos = 0;

    Lpos = round(m_Range.realLeft/hAttr.inc + hAttr.gnd);
    Rpos = round(m_Range.realRight/hAttr.inc + hAttr.gnd);

     //! 在当前模块未打开时修改系统状态时要更改设置
     async( MSG_HISTO_LEFTPOS, Lpos )
     async( MSG_HISTO_RIGHTPOS, Rpos );
#endif
    m_IsAttrChanged = true;
    return ERR_NONE;
}

DsoErr servHisto::onSrcVertChange()
{
    if( !m_histoEnable)
    {
        return ERR_NONE;
    }
    Chan ch = getAttrCh();
  //  qDebug() << "ch = " << ch;
    dsoVert* pVert;
    pVert = dsoVert::getCH(ch);
    float ratio;
    pVert->getProbe().toReal(ratio);
    float base = ratio*ref_volt_base;
    float scale = pVert->getYRefScale()*base;
    float offset = pVert->getYRefOffset()*base;

    DsoErr err;
    m_Range.posHigh = round(-(m_Range.realHigh + offset)/scale * scr_vdiv_dots + 240);
    err = limitRange(m_Range.posHigh, 1, wave_height-1);
    setuiChange(MSG_HISTO_HIGHPOS);
    if(err != 0)
    {
        //! 只有在超出屏幕范围的情况下才会去更新菜单上显示的值
        float temp;
        mUiAttr.setPostStr(MSG_HISTO_HIGHPOS ,vertFormat(m_Range.posHigh, temp));
    }

    m_Range.posLow = round(-(m_Range.realLow + offset)/scale * scr_vdiv_dots + 240);
    err = limitRange(m_Range.posLow, 1, wave_height-1);
    setuiChange(MSG_HISTO_LOWPOS);
    if(err != 0)
    {
        //! 只有在超出屏幕范围的情况下才会去更新菜单上显示的值
        float temp;
        mUiAttr.setPostStr(MSG_HISTO_LOWPOS ,vertFormat(m_Range.posLow, temp));
    }
    m_IsAttrChanged = true;
    return ERR_NONE;
}

DsoErr servHisto::onSrcEnChange()
{
    QString chServName;
    QString mathServName;

    int ch =  getChan();
    for(int i = 0;i < 4;i++)
    {
        bool chEn = false;
        chServName = QString("chan") + QString::number(i+1);
        serviceExecutor::query(chServName, MSG_CHAN_ON_OFF, chEn);

        bool mathEn = false;
        mathServName = QString("math") + QString::number(i+1);
        serviceExecutor::query(mathServName, MSG_MATH_EN, mathEn);

        mUiAttr.setEnable(MSG_HISTO_SOURCE, chan1 + i, chEn);
        mUiAttr.setEnable(MSG_HISTO_SOURCE, m1 + i, mathEn);

        if((ch == (i + 1)) && (!chEn))
        {
            setChan(chan_none);
        }
    }

    bool eyeEn = false;
    serviceExecutor::query(serv_name_eyejit, MSG_EYEJIT_EMEAS_EN, eyeEn);
    mUiAttr.setVisible(MSG_HISTO_SOURCE, color_grade, eyeEn);
    return ERR_NONE;
}

void servHisto::setMeasItemVis()
{
//    void *ptr = NULL;
//    serviceExecutor::query( serv_name_measure, servMeasure::MSG_STAT_MEAS_DATA, ptr);

//    Q_ASSERT( NULL != ptr );
//    QList<measStatData*> *pStatDataList = (QList<measStatData*>*)ptr;
    
//    int itemNum = pStatDataList->size();
    
//    for(int i = 0;i < itemNum;i++)
//    {
//        mUiAttr.setVisible(MSG_HISTO_SOURCEITEM,i,true);
//    }
//    for(int i = itemNum; i < 9;i++)
//    {
//        mUiAttr.setVisible(MSG_HISTO_SOURCEITEM,i,false);
//    }
}

DsoErr servHisto::limitRange(int& pos, int min, int max)
{
    if(pos > max)
    {
        pos = max;
        return ERR_OVER_LOW_RANGE;
    }
    else if(pos < min)
    {
        pos = min;
        return ERR_OVER_UPPER_RANGE;
    }
    return ERR_NONE;
}

Chan servHisto::getAttrCh()
{
    Chan ch;
    if(m_chan == color_grade)
    {
        int n;
        serviceExecutor::query(serv_name_eyejit,MSG_EYEJIT_SRCCH, n);
        ch = static_cast<Chan> (n);
    }
    else
    {
        ch = m_chan;
    }

    if(ch == chan_none)
    {
        ch = chan1;
    }
    return ch;
}

DsoErr servHisto::setRangeHigh(int pos)
{
    m_Range.posHigh = pos;
    /*DsoErr err = */
    limitRange(m_Range.posHigh, 1, m_Range.posLow - 1);
    //! 只有来自菜单改变位置的消息才改变物理值
    setuiOutStr(vertFormat(m_Range.posHigh, m_Range.realHigh));

    //remove frequently hint by hxh
//    if(m_histoEnable)
//    {
//        return err;
//    }
//    else
    {
        return ERR_NONE;
    }
}

int servHisto::getRangeHigh()
{
    setuiStep(-1);
    return m_Range.posHigh;
}

DsoErr servHisto::setRangeLow(int pos)
{
    m_Range.posLow = pos;

//    DsoErr err = ;
    limitRange(m_Range.posLow, m_Range.posHigh + 1, wave_height - 1);
    setuiOutStr(vertFormat(m_Range.posLow, m_Range.realLow));

//remove frequently hint by hxh
    //    if(m_histoEnable)
//    {
//        return err;
//    }
//    else
    {
        return ERR_NONE;
    }
}

int servHisto::getRangeLow()
{
    setuiStep(-1);

   //  qDebug() << "m_Range.posLow = " << m_Range.posLow;
    return m_Range.posLow;
}

DsoErr servHisto::setRangeLeft(int pos)
{
    m_Range.posLeft = pos;
    /*DsoErr err = */
    limitRange(m_Range.posLeft, 0, m_Range.posRight - 1);
    setuiOutStr(horiFormat(m_Range.posLeft, m_Range.realLeft));

    return ERR_NONE;
}

int servHisto::getRangeLeft()
{
    return m_Range.posLeft;
}

DsoErr servHisto::setRangeRight(int pos)
{
    m_Range.posRight = pos;
    /*DsoErr err = */

    limitRange(m_Range.posRight, m_Range.posLeft + 1, wave_width - 1);
    setuiOutStr(horiFormat(m_Range.posRight, m_Range.realRight));

    return ERR_NONE;
}

int servHisto::getRangeRight()
{
    return m_Range.posRight;
}

DsoErr servHisto::setHistoEn(bool en)
{
    LOG_DBG();
    m_histoEnable = en;
    if(m_histoEnable)
    {
        m_pthread->start();
        async(MSG_HISTO_LEFTPOS,  m_Range.posLeft);
        async(MSG_HISTO_RIGHTPOS, m_Range.posRight);

        async(MSG_HISTO_HIGHPOS,  m_Range.posHigh);
        async(MSG_HISTO_LOWPOS, m_Range.posLow);
    }
    else
    {
        defer(cmd_display_clean);
    }
    return ERR_NONE;
}

bool servHisto::getHistoEn()
{
    return m_histoEnable;
}

DsoErr servHisto::setHistoType(int type)
{
    m_histoType = (HistoType)type;
    if(m_histoType == histoHori || m_histoType == histoVert)
    {
        mUiAttr.setVisible( MSG_HISTO_RANGE, true );
    }
    else
    {
        mUiAttr.setVisible( MSG_HISTO_RANGE, false );
    }

    //bug 2083 by zy
    defer(servHisto::cmd_trace_update);
    return ERR_NONE;
}

HistoType servHisto::getHistoType()
{
    if(m_histoType == histoMeas)
    {
        setMeasItemVis();
    }

#ifdef FLAMINGO_TR5
    mUiAttr.setVisible(MSG_HISTO_TYPE, histoJitMeas, false);
#endif
    return m_histoType;
}

DsoErr servHisto::setHistoItem(int item)
{
    m_measItem = item;
    return ERR_NONE;
}

int servHisto::getHistoItem()
{
    return m_measItem;
}

DsoErr servHisto::setHistoJitter()
{
    bool jitHistEn;
    serviceExecutor::query(serv_name_eyejit, MSG_EYEJIT_JMEAS_ENHISTO, jitHistEn);

    if(jitHistEn)
    {
        mUiAttr.setEnable(MSG_HISTO_TYPE, histoJitMeas, true);
        async(MSG_HISTO_TYPE, histoJitMeas);
    }
    else
    {
        mUiAttr.setEnable(MSG_HISTO_TYPE,  histoJitMeas, false);
    }
    return ERR_NONE;
}

DsoErr servHisto::onHistoEnable()
{
    int timemode;
    query(serv_name_hori, MSG_HOR_TIME_MODE, timemode);
    static bool en;
    if(timemode == Acquire_XY  ||timemode == Acquire_ROLL )
    {
        //setChan(chan_none);
        defer(cmd_meas_clean);
        en = getHistoEn();
        if(en)
        {
            setHistoEn(!en);
        }
        mUiAttr.setEnable(MSG_HISTO_EN, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_HISTO_EN, true);
        if(en)
        {
            setHistoEn(en);
        }
    }

    return ERR_NONE;
}

DsoErr servHisto::onZoomEnChange()
{
    bool zoomEn;
    query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);

    bool FftEn;
    bool FftEn1,FftEn2,  FftEn3, FftEn4;
    query(serv_name_math1, MSG_MATH_S32FFTSCR, FftEn1);
    query(serv_name_math2, MSG_MATH_S32FFTSCR, FftEn2);
    query(serv_name_math3, MSG_MATH_S32FFTSCR, FftEn3);
    query(serv_name_math4, MSG_MATH_S32FFTSCR, FftEn4);

    if(FftEn1 == fft_screen_half  ||FftEn2 == fft_screen_half
           || FftEn3 == fft_screen_half||FftEn4 == fft_screen_half)
    {
        FftEn = true;
    }
    else
    {
         FftEn = false;
    }


    if(zoomEn || FftEn)
    {
        m_bZoomHistoEn = getHistoEn();
        if( m_bZoomHistoEn )
        {
            setHistoEn( false );
        }
        mUiAttr.setEnable(MSG_HISTO_EN, false);
    }
    else
    {
        if( m_bZoomHistoEn )
        {
            setHistoEn( true );
        }
        mUiAttr.setEnable(MSG_HISTO_EN, true);
    }

    return ERR_NONE;
}
DsoErr servHisto::setChan(int ch)
{
    m_chan = (Chan) ch;

    defer(servHisto::cmd_trace_update);
    return ERR_NONE;
}

Chan servHisto::getChan()
{ 
    return m_chan;
}

DsoErr servHisto::setHistoHeigth(int height)
{
    m_histoHeight = height;
    return ERR_NONE;
}

int servHisto::getHistoHeight()
{
    setuiMaxVal(4);
    setuiMinVal(1);
    setuiZVal(1);
    setuiUnit(Unit_Div);
    return m_histoHeight;
}

DsoErr servHisto::setStatisEn(bool en)
{
    m_statisEn = en;
    return ERR_NONE;
}

bool servHisto::getStatisEn()
{
    LOG_DBG();
    return m_statisEn;
}

DsoErr servHisto::resetStatis()
{
   // defer(cmd_meas_clean);
    traceWork.resetStatis();
    measWork.resetStatis();
    colorWork.resetStatis();

    defer(cmd_work_clean);
    defer(cmd_histoRes_update);
    //bug 2083 by zy
    defer(servHisto::cmd_trace_update);
    return ERR_NONE;
}

void *servHisto::getMeasWork()
{
    return &measWork;
}

void* servHisto::getHistoPtr()
{
    return &m_result.m_histoPix;
}

void *servHisto::getResPtr()
{
    return &m_result;
}

DsoErr servHisto::setScpiRangeHigh(qlonglong val)
{
    float fVal = (float)val/(1e6);
    float max,min;
    //m_Range.posHigh + 1, wave_height - 1
    vertFormat(1, max);
    vertFormat( m_Range.posLow - 1 , min);
    if(fVal >= max)
    {
        fVal = max;
    }
    else if(fVal <= min)
    {
        fVal = min;
    }
    QString yRealStr;
    yRealStr = vertFormatToPos(fVal, m_Range.posHigh);
    //bug 3187 by zy
#if 0
     async(MSG_HISTO_LOWPOS,  m_Range.posLow);
#else
     ssync(MSG_HISTO_HIGHPOS,  m_Range.posHigh);
     mUiAttr.setOutStr( MSG_HISTO_HIGHPOS, yRealStr);
     m_Range.realHigh = fVal;
#endif

    return ERR_NONE;
}

qlonglong servHisto::getScpiRangeHigh()
{
    return m_Range.realHigh*(1e6);
}

DsoErr servHisto::setScpiRangeLow(qlonglong val)
{
    float fVal = (float)val/(1e6);
    float max,min;
    vertFormat(m_Range.posHigh +  1, max);
    vertFormat(wave_height -1 , min);
    if(fVal >= max)
    {
        fVal = max;
    }
    else if(fVal <= min)
    {
        fVal = min;
    }
    QString yRealStr;
    yRealStr = vertFormatToPos(fVal, m_Range.posLow);

    //bug 3187 by zy
#if 0
     async(MSG_HISTO_LOWPOS,  m_Range.posLow);
#else
     ssync(MSG_HISTO_LOWPOS,  m_Range.posLow);
     mUiAttr.setOutStr( MSG_HISTO_LOWPOS, yRealStr);
     m_Range.realLow = fVal;
#endif
    return ERR_NONE;
}

qlonglong servHisto::getScpiRangeLow()
{
    return m_Range.realLow*(1e6);
}

DsoErr servHisto::setScpiRangeLeft(qlonglong val)
{
    float fVal = (float)val/(1e12);

    horiFormatToPos(fVal, m_Range.posLeft);
    async(MSG_HISTO_LEFTPOS,  m_Range.posLeft);

    return ERR_NONE;
}

qlonglong servHisto::getScpiRangeLeft()
{
    return m_Range.realLeft*(1e12);
}

DsoErr servHisto::setScpiRangeRight(qlonglong val)
{
    float fVal = (float)val/(1e12);
    horiFormatToPos(fVal, m_Range.posRight);
    async(MSG_HISTO_RIGHTPOS,  m_Range.posRight);

    return ERR_NONE;
}

qlonglong servHisto::getScpiRangeRight()
{
    return m_Range.realRight*(1e12);
}

QString servHisto::vertFormat(int pos, float& real)
{
    Chan ch;
    ch = getAttrCh();

    QString yRealStr;
    dsoVert* pVert;
    pVert = dsoVert::getCH(ch);

    float ratio;
    pVert->getProbe().toReal(ratio);
    float base = ratio*ref_volt_base;
    float scale = pVert->getYRefScale()*base;
    float offset = pVert->getYRefOffset()*base;
    real = -(pos - 240)*1.0/scr_vdiv_dots*scale - offset;

    gui_fmt::CGuiFormatter::format( yRealStr, real,fmt_def, pVert->getUnit());

    return yRealStr;
}

QString servHisto::horiFormat(int pos, float& real)
{
    Chan ch;
    ch = getAttrCh();

    QString xRealStr;
    horiAttr hAttr;
    hAttr = getHoriAttr(ch, horizontal_view_main);
    real = ( pos - hAttr.gnd ) * hAttr.inc;

    dsoVert* pVert;
    pVert = dsoVert::getCH(ch);
    chZone zone = pVert->getZone();
    if(zone == time_zone)
    {
        gui_fmt::CGuiFormatter::format( xRealStr, real, fmt_def, Unit_s);
    }
    else if(zone == freq_zone)
    {
        gui_fmt::CGuiFormatter::format( xRealStr, real, fmt_def, Unit_hz);
    }
    return xRealStr;
}

QString servHisto::vertFormatToPos(float real, int &pos )
{
    Chan ch;
    ch = getAttrCh();

    QString yRealStr;
    dsoVert* pVert;
    pVert = dsoVert::getCH(ch);

    float ratio;
    pVert->getProbe().toReal(ratio);
    float base = ratio*ref_volt_base;
    float scale = pVert->getYRefScale()*base;
    float offset = pVert->getYRefOffset()*base;
    pos = 240 - (real + offset)/scale*scr_vdiv_dots;

    gui_fmt::CGuiFormatter::format( yRealStr, real,fmt_def, pVert->getUnit());

    return yRealStr;  
}

QString servHisto::horiFormatToPos(float real, int &pos)
{
    Chan ch;
    ch = getAttrCh();

    QString xRealStr;
    horiAttr hAttr;
    hAttr = getHoriAttr(ch, horizontal_view_main);
    float tPos;
    tPos = real/hAttr.inc + hAttr.gnd;
    if(hAttr.inc)
    {
      //  pos = real/hAttr.inc + hAttr.gnd;
        pos = tPos;
    }
    dsoVert* pVert;
    pVert = dsoVert::getCH(ch);
    chZone zone = pVert->getZone();
    if(zone == time_zone)
    {
        gui_fmt::CGuiFormatter::format( xRealStr, real, fmt_def, Unit_s);
    }
    else if(zone == freq_zone)
    {
        gui_fmt::CGuiFormatter::format( xRealStr, real, fmt_def, Unit_hz);
    }
    return xRealStr;
}
