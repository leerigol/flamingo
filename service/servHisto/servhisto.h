#ifndef SERVHISTO_H
#define SERVHISTO_H

#include "../../include/dsodbg.h"
#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../../include/dsostd.h"
#include "../service_msg.h"
#include "../servtrace/servtrace.h"
#include "../servtrace/dataprovider.h"
#include "histowork.h"

enum histoMeasItem
{
    HISTOITEM_BEGIN = 0,
    SUM_HITS = HISTOITEM_BEGIN,
    PEAK_HITS,
    MAX_VAL,
    MIN_VAL,
    MAXMIN_VAL,
    MEAN_VAL,
    MEDIAN_VAL,
    MODE_VAL,
    BINWIDTH_VAL,
    SIGMA_VAL,
    HISTOITEM_END,

    XSCALE_VAL = HISTOITEM_END,
    YSCALE_VAL,
};

struct HistoRangeMgr
{
    int posLeft;
    int posRight;
    int posHigh;
    int posLow;

    float realLeft;
    float realRight;
    float realHigh;
    float realLow;
};

enum HistoType
{
    histoHori = 0,
    histoVert,
    histoMeas,
    histoJitMeas,
};

class servhisto_thread;
class servHisto : public serviceExecutor,public ISerial
{
    Q_OBJECT
protected:
    DECLARE_CMD()
public:
    servHisto(QString name,ServiceId id = E_SERVICE_ID_HISTO);

    enum enumCmd
    {
        cmdNone = 0,
        cmd_src_hori_change,
        cmd_src_vert_change,        
        cmd_src_en_change,
        cmd_histo_jitter_en,
        //! input
        cmd_trace_update,
        cmd_measWork_ptr,

        //! output
        cmd_histo_updata,
        cmd_histoRes_update,

        //! Measure
        cmd_meas_update,
        cmd_meas_clean,

        cmd_work_clean,
        cmd_display_clean,

        cmd_high_range,
        cmd_low_range,
        cmd_right_range,
        cmd_left_range,

        cmd_meas_item_change,
    };

    DsoErr start();
    void registerSpy();
    int    serialOut(CStream &stream, serialVersion &ver);
    int    serialIn( CStream &stream, serialVersion ver );
    int startup();
    void rst();

    DsoErr setRangeHigh(int pos);
    int getRangeHigh();

    DsoErr setRangeLow(int pos);
    int getRangeLow();

    DsoErr setRangeLeft(int pos);
    int getRangeLeft();

    DsoErr setRangeRight(int pos);
    int getRangeRight();

    DsoErr setHistoEn(bool en);
    bool getHistoEn();

    DsoErr setHistoType(int type);
    HistoType getHistoType();

    DsoErr setHistoItem(int item);
    int getHistoItem();

    DsoErr setHistoJitter();

    DsoErr     onHistoEnable();
    DsoErr     onZoomEnChange();

    DsoErr setChan(int ch);
    Chan getChan();

    DsoErr setHistoHeigth(int height);
    int getHistoHeight();

    DsoErr setStatisEn(bool en);
    bool getStatisEn();

    DsoErr resetStatis();

    void* getMeasWork();
    void* getHistoPtr();
    void* getResPtr();

    //scpi
    DsoErr      setScpiRangeHigh(qlonglong val);
    qlonglong   getScpiRangeHigh();

    DsoErr      setScpiRangeLow(qlonglong val);
    qlonglong   getScpiRangeLow();

    DsoErr      setScpiRangeLeft(qlonglong val);
    qlonglong   getScpiRangeLeft();

    DsoErr      setScpiRangeRight(qlonglong val);
    qlonglong   getScpiRangeRight();


    HistoTraceWork traceWork;
    HistoColorWork colorWork;
    histoMeasWork  measWork;
    bool  m_IsAttrChanged;

private:
    DsoErr  limitRange(int &pos, int min, int max );
    Chan    getAttrCh();
    QString vertFormat(int pos, float &real);
    QString horiFormat(int pos, float &real);

    QString vertFormatToPos(float real, int &pos );
    QString horiFormatToPos(float real, int &pos );
    
    DsoErr setDataProvider(void* ptr);
    DsoErr onSrcHoriChange();
    DsoErr onSrcVertChange();
    DsoErr onSrcEnChange();

    void setMeasItemVis();
    QSemaphore m_dataSema;
    dataProvider* m_pHistoProvider;

    HistoRangeMgr m_Range;
    bool m_histoEnable;
    bool m_bZoomHistoEn;

    HistoType m_histoType;
    Chan m_chan;
    int m_measItem;

    int m_histoHeight;
    bool m_statisEn;

    HistoStatisRes m_result;

    servhisto_thread* m_pthread;

    friend class HistoWork;
    friend class HistoTraceWork;
    friend class HistoColorWork;
    friend class histoMeasWork;
};
#endif // SERVHISTO_H
