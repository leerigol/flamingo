#include "../../service/servhori/servhori.h"
#include "../../service/servmeasure/servmeasure.h"
#include "servhisto.h"

DsoErr servHisto::start()
{
    setMsgAttr( (int)servHisto::cmd_trace_update );
    return ERR_NONE;
}

void servHisto::registerSpy()
{
    QVector<QString> chanServ;
    chanServ.append(serv_name_ch1);
    chanServ.append(serv_name_ch2);
    chanServ.append(serv_name_ch3);
    chanServ.append(serv_name_ch4);

    QVector<QString> mathServ;
    mathServ.append(serv_name_math1);
    mathServ.append(serv_name_math2);
    mathServ.append(serv_name_math3);
    mathServ.append(serv_name_math4);

    for(int i = 0;i < 4;i++)
    {
        spyOn( chanServ[i],  MSG_CHAN_SCALE,  cmd_src_vert_change);
        spyOn( chanServ[i],  MSG_CHAN_OFFSET, cmd_src_vert_change);
        spyOn( chanServ[i],  MSG_CHAN_PROBE,  cmd_src_vert_change);
        spyOn( chanServ[i],  MSG_CHAN_ON_OFF, cmd_src_en_change);

        spyOn( mathServ[i],  MSG_MATH_VIEW_OFFSET,  cmd_src_vert_change);
        spyOn( mathServ[i],  MSG_MATH_S32FUNCSCALE, cmd_src_vert_change);
        spyOn( mathServ[i],  MSG_MATH_EN, cmd_src_en_change);
    }

    spyOn( serv_name_hori, servHori::cmd_main_scale,  cmd_src_hori_change);
    spyOn( serv_name_hori, servHori::cmd_main_offset, cmd_src_hori_change);

    serviceExecutor::spyOn( E_SERVICE_ID_TRACE,
                            servTrace::cmd_set_provider,
                            mpItem->servId,
                            servHisto::cmd_trace_update );

    setMsgAttr( servHisto::cmd_trace_update);
    //! spy on eye
    spyOn( serv_name_eyejit, MSG_EYEJIT_EMEAS_EN, cmd_src_en_change);


    spyOn(   serv_name_measure,MSG_APP_MEAS_CLEAR_ONE,  cmd_meas_clean );
    spyOn(   serv_name_measure,MSG_APP_MEAS_CLEAR_ALL,    cmd_meas_clean );
    spyOn(   serv_name_measure, servMeasure::cmd_set_select_item,    cmd_meas_item_change );

    spyOn(    serv_name_display, MSG_DISPLAY_CLEAR,  cmd_display_clean);

    //!auto
   // spyOn(   serv_name_measure,MSG_APP_MEAS_CLEAR_ALL,    cmd_active_chan );
    spyOn(serv_name_hori, MSG_HOR_TIME_MODE);

    //监听ZOOM
    spyOn(serv_name_hori, MSG_HOR_ZOOM_ON);

    spyOn(serv_name_math1, MSG_MATH_S32FFTSCR);
    spyOn(serv_name_math2, MSG_MATH_S32FFTSCR);
    spyOn(serv_name_math3, MSG_MATH_S32FFTSCR);
    spyOn(serv_name_math4, MSG_MATH_S32FFTSCR);
}

#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

#define SERIAL_OP   stream.write
int servHisto::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;
    stream.write("Enable",      m_histoEnable);
    stream.write("HistoType",   (quint8) m_histoType);
    stream.write("HistoChan",   (quint8) m_chan);
    stream.write("HistoHeight", m_histoHeight);
    stream.write("StatisEn",    m_statisEn);
    stream.write("posHigh",     m_Range.posHigh);
    stream.write("posLow",      m_Range.posLow);
    stream.write("posLeft",     m_Range.posLeft);
    stream.write("posRight",    m_Range.posRight);

    return ERR_NONE;
}

#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

#define SERIAL_OP   stream.read
#define SERIAL_OP_C( a, b, c, type )   stream.read( a, c ); \
                                       memset( &b, 0, sizeof(b));\
                                       memcpy( &b, &c, sizeof(type) );
int servHisto::serialIn(CStream &stream, serialVersion ver)
{
    quint8  qu8Dat;
    if( ver == mVersion )
    {
       stream.read("Enable",      m_histoEnable);
       SERIAL_OP_C("HistoType",   m_histoType, qu8Dat, quint8);
       SERIAL_OP_C("HistoChan",   m_chan, qu8Dat, quint8);
       stream.read("HistoHeight", m_histoHeight);
       stream.read("StatisEn",    m_statisEn);

        stream.read("posHigh",     m_Range.posHigh);
        stream.read("posLow",      m_Range.posLow);
        stream.read("posLeft",     m_Range.posLeft);
        stream.read("posRight",    m_Range.posRight);
        mUiAttr.setEnable(MSG_HISTO_EN, true);
    }
    else
    {
        rst();
    }
    return ERR_NONE;
}

int servHisto::startup()
{
    async(MSG_HISTO_EN,  m_histoEnable);
    async(MSG_HISTO_LEFTPOS,  m_Range.posLeft);
    async(MSG_HISTO_RIGHTPOS, m_Range.posRight);
    async(MSG_HISTO_HIGHPOS,  m_Range.posHigh);
    async(MSG_HISTO_LOWPOS,   m_Range.posLow);
    mUiAttr.setEnable(MSG_HISTO_TYPE,histoJitMeas,false);

#ifdef FLAMINGO_TR5
    mUiAttr.setVisible(MSG_HISTO_TYPE,histoJitMeas,false);
    for(int i = 0;i < 4;i++)
    {
       mUiAttr.setVisible(MSG_HISTO_SOURCE, m1 + i, false);
    }

    mUiAttr.setVisible(MSG_HISTO_SOURCEITEM, false);
#endif
   // defer(MSG_HISTO_EN);
    return 0;
}

void servHisto::rst()
{
    m_histoEnable = false;
    m_bZoomHistoEn= false;

    m_histoType = histoHori;
    m_chan = chan1;
    m_histoHeight = 2;
    m_statisEn = false;

    m_Range.posHigh = 100;
    m_Range.posLow = 300;

    m_Range.posLeft = 300;
    m_Range.posRight = 700;

    mUiAttr.setEnable(MSG_HISTO_EN, true);
}
