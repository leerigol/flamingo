#include "histowork.h"
#include "servhisto.h"
#include "../serveyejit/serveyejit.h"


void HistoStatisRes::setUnit(Unit u)
{
    unit = u;
}

Unit HistoStatisRes::getUnit()
{
    return unit;
}

//! histo work
HistoWork::HistoWork()
{

}

void HistoWork::op(servHisto *)
{
}

void HistoWork::statisHits(servHisto *pHistoService)
{
    LOG_DBG();
    HistoStatisRes& res = pHistoService->m_result;
    res.peakHits = 0;
    res.sumHits = 0;

    for(int i = 0;i < m_histos.size();i++)
    {
        res.sumHits += m_histos.at(i);
        if(res.peakHits < m_histos.at(i))
        {
            res.peakHits = m_histos.at(i);
            m_resIndex.mode = i;
        }
    }
    LOG_DBG()<<res.sumHits<<res.peakHits;
}

void HistoWork::calcResIndex()
{
    //! min, max
    int size = m_histos.size();
    for(int i = 0;i < size;i++)
    {
        if(m_histos.at(i) > 0)
        {
            m_resIndex.min = i;
            break;
        }
    }

    for(int i = size - 1; i >= 0;i--)
    {
        if(m_histos.at(i) > 0)
        {
            m_resIndex.max = i;
            break;
        }
    }

    //! mean,dev
    int num = 0;
    qlonglong sum = 0;
    float deviation = 0.0;
    for (int i = m_resIndex.min;i < m_resIndex.max;i++)
    {
        if(m_histos[i] != 0)
        {
            num += m_histos[i];
            sum += (m_histos[i]*i);
        }
    }
    float average = sum*1.0/num;
    m_resIndex.mean = average;

    for (int i = m_resIndex.min;i < m_resIndex.max;i++)
    {
        if(m_histos[i] != 0)
        {
            deviation += m_histos[i]*pow(i - average,2);
        }
    }
    deviation = deviation / num;
    m_resIndex.sigma = sqrt(deviation);

    //! median
    m_resIndex.median = getMedianIndex(m_histos,num);
}

int HistoWork::getMedianIndex(QVector<int> histos,int sumHits)
{
    int numMedian = (sumHits+1)/2;
    int indexMedian = 0;
    int num = 0;
    while(num < numMedian)
    {
        num += histos[indexMedian];
        indexMedian++;
    }
    indexMedian = indexMedian - 1;
    return indexMedian;
}

//! calc the resItem first, then form the histo pix image
void HistoWork::toHistoPix(servHisto *pHistoService)
{  
    int pixMax;
    LOG_DBG();
    QVector<int>& histoPix = pHistoService->m_result.m_histoPix;
    if(pHistoService->m_histoType == histoVert)
    {
        pixMax = pHistoService->m_histoHeight * adc_hdiv_dots;
        histoPix.resize(wave_height);
    }
    else
    {
        pixMax = pHistoService->m_histoHeight * scr_vdiv_dots;
        histoPix.resize(wave_width);
    }

    if( pHistoService->m_result.peakHits == 0 || !pHistoService->m_result.Valid )
    {
        for(int i = 0;i < m_histos.size();i++)
        {
            histoPix[i] = 0;
        }
        return;
    }

    float inc = pixMax * 1.0/ pHistoService->m_result.peakHits;
    for(int i = 0;i < m_histos.size();i++)
    {
        histoPix[i] = int (m_histos[i]*inc);
    }
    LOG_DBG();
}

void HistoWork::resetStatis()
{

}

HistoColorWork::HistoColorWork()
{

}

void HistoColorWork::op(servHisto *pHistoService)
{
    LOG_DBG()<<__FILE__;
    void* ptr;
    serviceExecutor::query(serv_name_eyejit,servEyeJit::cmd_eye_statisScr,ptr);
    QVector<QVector<int> >* eyeStatis = static_cast<QVector<QVector<int> >* > (ptr);

    HistoStatisRes& res = pHistoService->m_result;
    if(eyeStatis->size() < 480)
    {
        LOG_DBG()<<__FILE__;
        res.Valid = false;
        m_histos.clear();
        return;
    }
    LOG_DBG()<<__FILE__;
    int left = pHistoService->m_Range.posLeft;
    int right = pHistoService->m_Range.posRight;
    int high = pHistoService->m_Range.posHigh;
    int low = pHistoService->m_Range.posLow;

    if(pHistoService->m_histoType == histoHori)
    {
        m_histos.clear();
        m_histos.resize(wave_width);

        for(int i = high;i <= low;i++)
        {
            for(int j = left; j <= right; j++)
            {
                m_histos[j] = eyeStatis->at(i).at(j) + m_histos[j];
            }
        }
    }
    else if(pHistoService->m_histoType == histoVert)
    {
        m_histos.clear();
        m_histos.resize(wave_height);

        for(int i = high;i <= low;i++)
        {
            for(int j = left; j <= right; j++)
            {
                m_histos[480 - i] = eyeStatis->at(i).at(j) + m_histos[480 - i];
            }
        }
    }
    statisHits(pHistoService);

    if(res.sumHits == 0)
    {
        res.Valid = false;
        return;
    }
    else
    {
        res.Valid = true;
    }

    toHistoPix(pHistoService);

    if(pHistoService->getStatisEn())
    {
        calcResIndex();
        ResIndexToVal(pHistoService);
    }
}

void HistoColorWork::resetStatis()
{
    //! to do:直方图中的重置色级统计发一条消息去重置眼图目前的统计结果。
    for(int i = 0; i < m_histos.size();i++)
    {
        m_histos[i] = 0;
    }
}

void HistoColorWork::ResIndexToVal(servHisto *pHistoService)
{
    HistoStatisRes& res = pHistoService->m_result;

    if( pHistoService->getHistoType() == histoHori )
    {
        //!
        float tIncScr;
        serviceExecutor::query(serv_name_eyejit, servEyeJit::cmd_eye_tInc_scr, tIncScr);

        res.minVal = m_resIndex.min * tIncScr;
        res.maxVal = m_resIndex.max * tIncScr;
        res.pKpK = res.maxVal - res.minVal;

        res.mean = m_resIndex.mean * tIncScr;
        res.median = m_resIndex.median * tIncScr;
        res.mode = m_resIndex.mode * tIncScr;
        res.BinWidth = tIncScr;
        res.sigma = m_resIndex.sigma * tIncScr;
    }
    else if( pHistoService->getHistoType() == histoVert )
    {
        int n;
        serviceExecutor::query(serv_name_eyejit, MSG_EYEJIT_SRCCH, n);
        Chan eyeChan = (Chan) n;

        dsoVert* pVert;
        pVert = dsoVert::getCH(eyeChan);
        float ratio;
        pVert->getProbe().toReal(ratio);
        float base = ratio*ref_volt_base;
        float scale = pVert->getYRefScale()*base;
        float offset = pVert->getYRefOffset()*base;

        res.minVal = - ( m_resIndex.min - 240 ) / scr_vdiv_dots * scale + offset;
        res.maxVal = - ( m_resIndex.max - 240 ) / scr_vdiv_dots * scale + offset;
        res.pKpK = res.maxVal - res.minVal;

        res.mean = - ( m_resIndex.mean - 240 ) / scr_vdiv_dots * scale + offset;
        res.median = - ( m_resIndex.median - 240 ) / scr_vdiv_dots * scale + offset;
        res.mode = - ( m_resIndex.mode - 240 ) / scr_vdiv_dots * scale + offset;
        res.BinWidth = scale / scr_vdiv_dots ;
        res.sigma = - ( m_resIndex.sigma - 240 ) / scr_vdiv_dots * scale;
    }
}
