#ifndef SERVHISTO_THREAD_H
#define SERVHISTO_THREAD_H

#include "servhisto.h"
#include "histowork.h"

class servhisto_thread : public QThread
{
    Q_OBJECT
public:
    servhisto_thread();
public:
    void run();
    void setClient(servHisto *pHisto);

private:
    servHisto *m_pHistoServ;
};

#endif // SERVHISTO_THREAD_H
