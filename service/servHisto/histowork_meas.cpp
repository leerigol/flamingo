#include "histowork.h"
#include "servhisto.h"
#include "../servmeasure/servmeasure.h"

histoMeasWork::histoMeasWork()
{
    inited = false;
    firstVal = 9.9e+37;
    lastVal = 9.9e-37;

    maxVal = 9.9e-37;
    minVal = 9.9e+37;
    m_histos.resize(wave_width);
}

void histoMeasWork::op(servHisto *pHistoService)
{
    //! selected item
     void *ptr = NULL;
    HistoStatisRes& res = pHistoService->m_result;

    if(pHistoService->getHistoType() == histoJitMeas)
    {
        supressJitter(pHistoService);
    }
    else if(pHistoService->getHistoType() == histoMeas)
    {
//        void *ptr = NULL;
//        serviceExecutor::query( serv_name_measure,
//                                servMeasure::MSG_STAT_MEAS_DATA, ptr);
//        Q_ASSERT( NULL != ptr );
//        QList<measStatData*> *pStatDataList = (QList<measStatData*>*)ptr;

//        if(item > pStatDataList->size())
//        {
//            qDebug()<<__FILE__<<__FUNCTION__<<"histo meas overflow";
//            res.Valid = false;
//            return;
//        }

      //  void *ptr = NULL;
        serviceExecutor::query( serv_name_measure,
                                servMeasure::cmd_get_histo_meas, ptr);
        if(ptr != NULL)
        {
            QQueue<MeasValue>* pMeasQue = static_cast<QQueue<MeasValue>* > (ptr);
            res.setUnit( pMeasQue->first().mUnit );
            for(int i = 0;i < pMeasQue->size();i++)
            {
                //bug 2492 by zy
                if(!pMeasQue->at(i).mVal)
                {
                    return;
                }
                supressData(pHistoService, pMeasQue->dequeue().mVal);
            }
        }
        else
        {
       //     supressData(pHistoService, 0);
            LOG_DBG()<<"no_data";
        }
    }
    else
    {
        Q_ASSERT(false);
        return;
    }

    statisHits(pHistoService);
    if(res.sumHits == 0)
    {
        res.Valid = false;
        return;
    }
    else
    {
        res.Valid = true;
    }

    toHistoPix( pHistoService );

    if(pHistoService->getStatisEn())
    {
        calcResIndex();
       // if(ptr != NULL)
        {
            ResIndexToVal( res );
        }
        //else
        {
       //     ResClear(res);
        }
    }
}

void histoMeasWork::supressData(servHisto *pHistoService, float measVal)
{
    if(!inited)
    {
        LOG_DBG();
        initXScale(measVal);

        HistoStatisRes& res = pHistoService->m_result;
        res.centVal = measVal;
        inited = true;
    }

    if(measVal > maxVal)
    {
        maxVal = measVal;
    }
    else if(measVal < minVal)
    {
        minVal = measVal;
    }

    int index = round((measVal - firstVal)/valVDiv);
    if(index >= 0 && index < wave_width)
    {
        m_histos[index]++;
    }
    else
    {
        insertVal(measVal, index);
    }
}

void histoMeasWork::supressJitter(servHisto *pHistoService)
{
    if(!jitterBuffer.isEmpty())
    {
        LOG_DBG();
        QVector<float> jitter = jitterBuffer.dequeue();

        if(!inited)
        {
            LOG_DBG();
            if(!jitter.isEmpty())
            {
                initXScale(jitter.at(0));
                inited = true;
            }
            else
            {
                LOG_DBG();
            }
        }

        for(int i = 0;i < jitter.size();i++)
        {
            supressData(pHistoService, jitter.at(i));
        }
    }
}

void histoMeasWork::resetStatis()
{
    m_histos.clear();
    m_histos.resize(1000);
    inited = false;
}

void histoMeasWork::addJitterBuffer(QVector<float> jitter)
{
    //! 队列中已经保存的数据过多时则不再保存数据
    if(jitterBuffer.size() < 3)
    {
        LOG_DBG()<<__FILE__;
        jitterBuffer.enqueue(jitter);
    }
}

void histoMeasWork::ResClear(HistoStatisRes &res)
{
#if 0
    res.maxVal = m_resIndex.max * valVDiv + firstVal;
    res.minVal = m_resIndex.min * valVDiv + firstVal;
    res.pKpK = res.maxVal - res.minVal;

    res.mean = m_resIndex.mean * valVDiv + firstVal;
    res.mode = m_resIndex.mode * valVDiv + firstVal;
    res.median = m_resIndex.median * valVDiv + firstVal;

    //! don't need to minus the firstVal
    res.BinWidth = valVDiv;
    res.sigma = m_resIndex.sigma * valVDiv;
    res.xScale = res.BinWidth * 100;
#else

    res.sumHits = 0;
    res.peakHits = 0;

    res.maxVal = 0;
    res.minVal = 0;
    res.pKpK = 0;

    res.mean = 0;
    res.mode = 0;
    res.median = 0;

    //! don't need to minus the firstVal
    res.BinWidth = 0;
    res.sigma = 0;
    res.xScale = 0;
#endif
}


void histoMeasWork::ResIndexToVal(HistoStatisRes &res)
{
    res.maxVal = m_resIndex.max * valVDiv + firstVal;
    res.minVal = m_resIndex.min * valVDiv + firstVal;
    res.pKpK = res.maxVal - res.minVal;

    res.mean = m_resIndex.mean * valVDiv + firstVal;
    res.mode = m_resIndex.mode * valVDiv + firstVal;
    res.median = m_resIndex.median * valVDiv + firstVal;

    //! don't need to minus the firstVal
    res.BinWidth = valVDiv;
    res.sigma = m_resIndex.sigma * valVDiv;
    res.xScale = res.BinWidth * 100;
}
void histoMeasWork::initXScale(float centVal,MeasType /*type*/,Chan )
{
    //! 水平定标：根据显示的末位分度以及wfm的finc两者中较大的一个来确定刻度；
    //! 垂直定标：根据显示的末位分度以及垂直属性中的yInc中两者较大的一个值来确定刻度.
    if(valVDiv == 0)
    {
        valVDiv = 1e-10;
    }

    valVDiv = centVal / (wave_width*10);
    firstVal = centVal - valVDiv * wave_width/2;
    lastVal = centVal + valVDiv * wave_width/2;

    inited = true;
}

void histoMeasWork::insertVal(float measVal,int oldIndex)
{
    LOG_DBG();

    float newFirstVal = 0;
    float newLastVal = 0;
    float newValVDiv = valVDiv;
    if(oldIndex < 0)
    {
        float centVal = (minVal + maxVal)/2;
        while(newValVDiv < (lastVal - minVal)/1000)
        {
            newValVDiv *= 2;
        }
        newFirstVal = centVal - wave_width*newValVDiv/2;
        newLastVal = centVal + wave_width*newValVDiv/2;
    }
    else if(oldIndex >= wave_width)
    {
        float centVal = (minVal + maxVal)/2;
        while(newValVDiv < (maxVal - firstVal)/1000)
        {
            newValVDiv *= 2;
        }
        newFirstVal = centVal - wave_width*newValVDiv/2;
        newLastVal = centVal + wave_width*newValVDiv/2;
    }

    LOG_DBG()<<"newHisto";
    QVector<int> newHistos(1000);

    //for bug3096 by hxh.   FPE or overflow
    if( newValVDiv != 0 )
    {
        for(int i = 0; i < wave_width;i++)
        {
            int newIndex = ((firstVal + i*valVDiv) - newFirstVal)/newValVDiv;
            //Q_ASSERT(newIndex < 1000 && newIndex >= 0);
            if( newIndex >= 0 && newIndex < 1000 )
            {
                newHistos[newIndex] += m_histos[i];
            }
            else
            {
                qDebug() << "Q_ASSERT(measIndex < 1000 && measIndex >= 0);" << newIndex;
            }
        }
    }

    int measIndex = round((measVal - newFirstVal)/newValVDiv);
    //Q_ASSERT(measIndex < 1000 && measIndex >= 0);
    if( measIndex >= 0 && measIndex < 1000 )
    {
        newHistos[measIndex]++;
    }
    else
    {
        qDebug() << "Q_ASSERT(measIndex < 1000 && measIndex >= 0);" << measIndex;
    }

    firstVal = newFirstVal;
    lastVal = newLastVal;
    valVDiv = newValVDiv;
    m_histos = newHistos;
}
