#include "servhisto_thread.h"
#include "../servgui/servgui.h"
servhisto_thread::servhisto_thread()
{

}

void servhisto_thread::run()
{
    while(1)
    {
        LOG_DBG()<<__FILE__;
        if(!m_pHistoServ->getHistoEn())
        {
            break;
        }
        if(m_pHistoServ->getHistoType() == histoMeas || m_pHistoServ->getHistoType() == histoJitMeas)
        {
            m_pHistoServ->measWork.op(m_pHistoServ);
        }
        else
        {
            if(m_pHistoServ->getChan() == color_grade)
            {
                m_pHistoServ->colorWork.op(m_pHistoServ);
            }
            else if(m_pHistoServ->getChan() == chan_none)
            {
                 msleep(200);
                 continue;
            }
            else
            {
                m_pHistoServ->traceWork.op(m_pHistoServ);
            }
        }

        if(m_pHistoServ->m_IsAttrChanged)
        {
            m_pHistoServ->traceWork.resetStatis();
            m_pHistoServ->m_IsAttrChanged = false;
            continue;
        }

        serviceExecutor::post(serv_name_histo, servHisto::cmd_histo_updata, 0);
        serviceExecutor::post(serv_name_histo, servHisto::cmd_histoRes_update, 0);
        msleep(200);

    }

    serviceExecutor::post( serv_name_gui, servGui::cmd_logo_animate_run, false);

}

void servhisto_thread::setClient(servHisto *pHisto)
{
    Q_ASSERT( pHisto != NULL);
    m_pHistoServ = pHisto;
}

