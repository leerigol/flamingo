#include "servhisto.h"

/*! \var 定义消息映射表
*
* 定义消息映射表
*/

IMPLEMENT_CMD( serviceExecutor, servHisto )
start_of_entry()

on_set_int_int( MSG_HISTO_HIGHPOS, &servHisto::setRangeHigh ),
on_get_int( MSG_HISTO_HIGHPOS, &servHisto::getRangeHigh ),

on_set_int_int( MSG_HISTO_LOWPOS, &servHisto::setRangeLow ),
on_get_int( MSG_HISTO_LOWPOS, &servHisto::getRangeLow ),

on_set_int_int( MSG_HISTO_LEFTPOS, &servHisto::setRangeLeft ),
on_get_int( MSG_HISTO_LEFTPOS, &servHisto::getRangeLeft),

on_set_int_int( MSG_HISTO_RIGHTPOS, &servHisto::setRangeRight ),
on_get_int( MSG_HISTO_RIGHTPOS, &servHisto::getRangeRight),

on_set_int_bool( MSG_HISTO_EN, &servHisto::setHistoEn ),
on_get_bool( MSG_HISTO_EN, &servHisto::getHistoEn ),

on_set_int_int( MSG_HISTO_TYPE, &servHisto::setHistoType ),
on_get_int( MSG_HISTO_TYPE, &servHisto::getHistoType ),

on_set_int_int( MSG_HISTO_SOURCEITEM, &servHisto::setHistoItem),
on_get_int( MSG_HISTO_SOURCEITEM, &servHisto::getHistoItem),

on_set_int_int( MSG_HISTO_SOURCE, &servHisto::setChan ),
on_get_int( MSG_HISTO_SOURCE, &servHisto::getChan ),

on_set_int_int( MSG_HISTO_DISPGRID, &servHisto::setHistoHeigth),
on_get_int( MSG_HISTO_DISPGRID, &servHisto::getHistoHeight),

on_set_int_int( MSG_HISTO_STATISEN, &servHisto::setStatisEn),
on_get_bool( MSG_HISTO_STATISEN, &servHisto::getStatisEn),

on_set_int_void( MSG_HISTO_RESET, &servHisto::resetStatis),
on_set_int_void(servHisto::cmd_display_clean,&servHisto::resetStatis),
on_set_int_void(servHisto::cmd_meas_item_change,&servHisto::resetStatis),

on_get_pointer( servHisto::cmd_histo_updata, &servHisto::getHistoPtr),
on_get_pointer( servHisto::cmd_histoRes_update, &servHisto::getResPtr),

on_set_int_pointer( servHisto::cmd_trace_update, &servHisto::setDataProvider),
on_get_pointer( servHisto::cmd_measWork_ptr, &servHisto::getMeasWork),

on_set_int_void( servHisto::cmd_src_hori_change, &servHisto::onSrcHoriChange),
on_set_int_void( servHisto::cmd_src_vert_change, &servHisto::onSrcVertChange),
on_set_int_void( servHisto::cmd_src_en_change,   &servHisto::onSrcEnChange),
on_set_int_void( servHisto::cmd_histo_jitter_en, &servHisto::setHistoJitter),

//void servCursor::on_zoom_en_change()
//on_set_void_void( servHisto::cmd_meas_clean, &servHisto::setClean),
on_set_int_void( MSG_HOR_TIME_MODE, &servHisto::onHistoEnable),
on_set_int_void( MSG_HOR_ZOOM_ON, &servHisto::onZoomEnChange),
on_set_int_void( MSG_MATH_S32FFTSCR, &servHisto::onZoomEnChange),
//cmd_meas_clean
//!sys msg
on_set_int_int(CMD_SERVICE_ACTIVE,            &servHisto::setActive ),
on_set_void_void(CMD_SERVICE_RST,               &servHisto::rst ),
on_set_int_void(CMD_SERVICE_STARTUP,           &servHisto::startup ),

/****************scpi**************************/
on_set_int_ll( servHisto::cmd_high_range, &servHisto::setScpiRangeHigh ),
on_get_ll(     servHisto::cmd_high_range, &servHisto::getScpiRangeHigh ),

on_set_int_ll( servHisto::cmd_low_range, &servHisto::setScpiRangeLow ),
on_get_ll(     servHisto::cmd_low_range, &servHisto::getScpiRangeLow ),

on_set_int_ll( servHisto::cmd_right_range, &servHisto::setScpiRangeRight ),
on_get_ll(     servHisto::cmd_right_range, &servHisto::getScpiRangeRight ),

on_set_int_ll( servHisto::cmd_left_range, &servHisto::setScpiRangeLeft ),
on_get_ll(     servHisto::cmd_left_range, &servHisto::getScpiRangeLeft ),

end_of_entry()
