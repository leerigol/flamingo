#include "histowork.h"
#include "servhisto.h"
HistoTraceWork::HistoTraceWork()
{
    for(int i = 0;i < adc_max+1;i++)
    {
        for(int j = 0;j < wave_width;j++)
        {
            m_screenStatis[i][j]=0;
        }
    }
}

void HistoTraceWork::op(servHisto *pHistoService)
{
    Chan ch = pHistoService->m_chan;
    DsoWfm wfm;
    pHistoService->m_dataSema.acquire();

//    pHistoService->m_pHistoProvider->acquire();
    dsoVert* pVert;
    pVert = dsoVert::getCH(ch);
    pVert->getTrace(wfm);
//    pHistoService->m_pHistoProvider->release();
    //! pHistoService->m_result.setUnit(pVert->getUnit());
    chZone zone = pVert->getZone();
    HistoStatisRes& res = pHistoService->m_result;

     if(pHistoService->getHistoType() == histoHori)
     {
         if(zone == time_zone)
         {
             res.setUnit(Unit_s);
         }
        else if(zone == freq_zone)
        {
            res.setUnit(Unit_hz);
        }
     }
     //bug 2885 by zy
     else
     {
          res.setUnit(pVert->getUnit());
     }
    LOG_DBG();
    supressData(pHistoService,&wfm);
    statisHits(pHistoService);
    if(res.sumHits == 0)
    {
        res.Valid = false;
        //bug 2123 by zy
        return;
    }
    else
    {
        res.Valid = true;
    }
    if(pHistoService->getHistoType() == histoVert)
    {
        //! 256ADC to waveHeight
        toVertHistoPix(pHistoService,&wfm);
    }
    else
    {
        toHistoPix(pHistoService);
    }
    if(pHistoService->getStatisEn())
    {
        calcResIndex();
        ResIndexToVal(pHistoService);
    }
}

void HistoTraceWork::toVertHistoPix(servHisto *pHistoService,DsoWfm* pWfm)
{
    int pixMax;
    LOG_DBG();

    QVector<int>& histoPix = pHistoService->m_result.m_histoPix;
    pixMax = pHistoService->m_histoHeight * adc_hdiv_dots;
    histoPix.resize(wave_height);

    float temp;
    uchar adcInScrTop = getTraceYADC(0, pWfm, temp);
    uchar adcInScrBase = getTraceYADC(wave_height-1, pWfm, temp);

    //! 重新将ADC值与屏幕进行对应
    float vertInc = (adcInScrTop - adcInScrBase)*1.0/wave_height;
    for(int i = 0;i < wave_height;i++)
    {
        histoPix[i] = m_histos[int(i*vertInc) + adcInScrBase];
    }

    float inc = ((float)pixMax) / pHistoService->m_result.peakHits;
    for(int i = 0;i < histoPix.size();i++)
    {
        histoPix[i] = int (histoPix[i]*inc);
    }
    LOG_DBG();
}

void HistoTraceWork::resetStatis()
{
    for(int i = 0;i < adc_max;i++)
    {
        for(int j = 0;j < wave_width;j++)
        {
            m_screenStatis[i][j] = 0;
        }
    }
}

void HistoTraceWork::supressData(servHisto *pHistoService,DsoWfm* pWfm)
{
    Chan ch = pHistoService->m_chan;
    HistoType type = pHistoService->m_histoType;
    HistoRangeMgr& range = pHistoService->m_Range;
    statisScreen(pWfm);
    float temp;
    int xLeft = getTraceXIndex(ch, range.posLeft, pWfm);
    //! 取右界限对应的trace下标时，与求左坐标不同，左闭右开.因为一个像素点往往对应着多个trace点
    int xRight = getTraceXIndex(ch, range.posRight+1, pWfm)-1;

    uchar yHighADC = getTraceYADC(range.posHigh, pWfm, temp);
    uchar yLowADC = getTraceYADC(range.posLow, pWfm, temp);

    int xBeginIndex = 0;
    int xEndIndex = pWfm->size();
    float incX = wave_width*1.0/(xEndIndex - xBeginIndex);



    int iLeft = (int)((xLeft - xBeginIndex)*incX);
    int iRght = (int)((xRight - xBeginIndex)*incX);

    if( iLeft < 0 || iRght > wave_width )
    {
        return;
    }

    if(type == histoHori)
    {
        m_histos.clear();
        m_histos.resize(wave_width);

        for(int i = iLeft; i < iRght; i++)
        {
            for(uchar j = yLowADC;j <= yHighADC;j++)
            {
                m_histos[i] += m_screenStatis[j][i];
            }
        }
    }
    else if(type == histoVert)
    {
        m_histos.clear();
        m_histos.resize(256);
        for(uchar i = yLowADC;i <= yHighADC;i++)
        {
            for(int j = iLeft;j < iRght; j++)
            {
                m_histos[i] += m_screenStatis[i][j];
            }
        }
    }
    else
    {
        //Q_ASSERT(false);
    }
}

void HistoTraceWork::statisScreen(DsoWfm* pWfm)
{
#if 0
    int xLeft = getTraceXIndex(ch, 0, pWfm);
    //! 取右界限对应的trace下标时，与求左坐标不同，左闭右开.因为一个像素点往往对应着多个trace点
    int xRight = getTraceXIndex(ch, wave_width, pWfm);
    if(xLeft < 0 || xRight > pWfm->size())
    {
       // qDebug()<<"statis fail";
        return;
    }
#endif
    int xLeft = 0;
    //! 取右界限对应的trace下标时，与求左坐标不同，左闭右开.因为一个像素点往往对应着多个trace点
    int xRight = pWfm->size();
    float incX = 1.0*wave_width/(xRight - xLeft);

    int index = (int)(incX * (xRight - xLeft - 1));
    if( index < wave_width )
    {
        for(int i = xLeft;i < xRight;i++)
        {
            index = (int)(incX * (i - xLeft));
            m_screenStatis[pWfm->at(i)][index]++;
        }
    }
}

void HistoTraceWork::ResIndexToVal(servHisto *pHistoService)
{
    HistoStatisRes& res = pHistoService->m_result;
    Chan ch = pHistoService->getChan();
    //! only if the source is trace
    if( pHistoService->getHistoType() == histoHori )
    {
        horiAttr hAttr;
        hAttr = getHoriAttr(ch, horizontal_view_main);
        res.minVal = ( m_resIndex.min - hAttr.gnd ) * hAttr.inc;
        res.maxVal = ( m_resIndex.max - hAttr.gnd ) * hAttr.inc;
        res.pKpK = res.maxVal - res.minVal;

        res.mean = ( m_resIndex.mean - hAttr.gnd ) * hAttr.inc;
        res.median = ( m_resIndex.median - hAttr.gnd ) * hAttr.inc;
        res.mode = ( m_resIndex.mode - hAttr.gnd ) * hAttr.inc;

        res.BinWidth = hAttr.inc;
        res.sigma = m_resIndex.sigma * hAttr.inc;
    }
    else if( pHistoService->getHistoType() == histoVert )
    {
        vertAttr vAttr;
        vAttr = getVertAttr(ch);
        res.minVal = ( m_resIndex.min - vAttr.yGnd ) * vAttr.yInc;
        res.maxVal = ( m_resIndex.max - vAttr.yGnd ) * vAttr.yInc;
        res.pKpK = res.maxVal - res.minVal;

        res.mean = ( m_resIndex.mean - vAttr.yGnd ) * vAttr.yInc;
        res.median = ( m_resIndex.median - vAttr.yGnd ) * vAttr.yInc;
        res.mode = ( m_resIndex.mode - vAttr.yGnd ) * vAttr.yInc;
        res.BinWidth = vAttr.yInc;
        res.sigma = m_resIndex.sigma * vAttr.yInc;
    }
}

int HistoTraceWork::getTraceXIndex(Chan ch, int pos, DsoWfm *pWfm)
{
    horiAttr hAttr;
    hAttr = getHoriAttr(ch, horizontal_view_main);
    float Xreal = ( pos - hAttr.gnd ) * hAttr.inc;
    int Xindex = ( Xreal - pWfm->gett0().toFloat())/pWfm->gettInc().toFloat();
    return Xindex;
}

uchar HistoTraceWork::getTraceYADC(int posY, DsoWfm* pWfm,
                                   float& Yreal)
{
    float base;
    pWfm->getyRefBase().toReal(base);
    float scale = pWfm->getyRefScale()*base;
    float offset = pWfm->getyRefOffset()*base;
    Yreal = -(posY - 240)*1.0/scr_vdiv_dots*scale - offset;
    //! 这边的换算有可能会造成一定的误差
    uchar yAdc = Yreal/pWfm->realyInc() + pWfm->getyGnd();
    return yAdc;
}
