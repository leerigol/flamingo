#ifndef HISTOWORK_H
#define HISTOWORK_H

#include <QVector>
#include "../../baseclass/dsowfm.h"
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"
//! 统计为水平时，统计项的单位通过pvert获取；统计为垂直时，统计项的单位通过zone获取。
//! 的统计源为测量项时，统计项单位通过

struct HistoStatisRes
{
    int sumHits;
    int peakHits;
    //! 统计范围边界值
    float maxVal;
    float minVal;
    float pKpK;
    //! 平均数、中位数、众数
    float mean;
    float median;
    float mode;
    float BinWidth;
    float sigma;
    float xScale;
    //int   yScale;
    float  centVal;
    bool  Valid;

    void setUnit(Unit u);
    Unit getUnit();
    QVector<int> m_histoPix;
private:
    Unit unit;
};

struct ResIndex
{
    int max;
    int min;

    float mean;
    int   median;
    int   mode;//! 众数
    float binWidth;
    float sigma;
};

class servHisto;
class HistoWork
{
public: 
    HistoWork();
    virtual void op(servHisto *);
    virtual void resetStatis();

protected:
    //! 形成最后的结果向量（以像素为单位，并计算统计项目）
    //! input:the vector of supress to 1000
    //! 参数:需要直方图像素高，需要是否统计的使能信息，需要直方图类型及信源单位
    void statisHits(servHisto *pHistoService);
    void calcResIndex();
    int  getMedianIndex(QVector<int> histos, int sumHits);
    int  getMedianIndex(QVector<qint64> histos, int sumHits);
    void toHistoPix(servHisto *pHistoService);

    QVector<int> m_histos;
    ResIndex m_resIndex;
};

class HistoTraceWork:public HistoWork
{
public:
    HistoTraceWork();
    virtual void op(servHisto *pHistoService);
    virtual void resetStatis();

protected:
    void supressData(servHisto *pHistoService, DsoWfm *pWfm);
    //void statisScreen(Chan ch,DsoWfm* pWfm);
    void statisScreen(DsoWfm* pWfm);
    void ResIndexToVal(servHisto *pHistoService);
    virtual void toVertHistoPix(servHisto *pHistoService, DsoWfm *pWfm);

private:
    int   getTraceXIndex(Chan ch, int pos, DsoWfm* pWfm);
    uchar getTraceYADC(int posY, DsoWfm* pWfm,float &Yreal);
    int   m_screenStatis[adc_max+1][wave_width];
};

class histoMeasWork:public HistoWork
{
public:
    histoMeasWork();
    virtual void op(servHisto *pHistoService);
    virtual void resetStatis();
    void addJitterBuffer(QVector<float> jitter);

private:
    void  supressData(servHisto *, float measVal);
    void  supressJitter(servHisto *pHistoService);
    void  ResIndexToVal(HistoStatisRes& res);
    void  ResClear(HistoStatisRes &res);
    void  initXScale(float centVal, MeasType = Meas_HOR_TYPE, Chan = chan_none);
    void  insertVal(float measVal, int oldIndex);
    bool  inited;
    float firstVal;
    float valVDiv;
    float lastVal;

    float maxVal;
    float minVal;

    QQueue<float> measBuffer;
    QQueue<QVector<float> > jitterBuffer;
};

class HistoColorWork:public HistoWork
{
public:
    HistoColorWork();
    virtual void op(servHisto *pHistoService);
    virtual void resetStatis();
private:
    void  ResIndexToVal(servHisto *pHistoService);
};
#endif // HISTOWORK_H
