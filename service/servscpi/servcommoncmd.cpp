#include "servcommoncmd.h"
#include "../servscpi/servscpi.h"
#include "../servutility/servutility.h"
#include "../../com/scpiparse/cscpieventreg.h"

IMPLEMENT_CMD( CExecutor, servCommonCmd )
start_of_entry()
on_get_string( servCommonCmd::cmd_idn, &servCommonCmd::getIdn ),

on_get_int( servCommonCmd::cmd_esr, &servCommonCmd::getEsr ),
on_get_int( servCommonCmd::cmd_stb, &servCommonCmd::getStb ),

on_get_int( servCommonCmd::cmd_ese, &servCommonCmd::getEse ),
on_set_void_int( servCommonCmd::cmd_ese, &servCommonCmd::setEse ),

on_get_int( servCommonCmd::cmd_sre, &servCommonCmd::getSre ),
on_set_void_int( servCommonCmd::cmd_sre, &servCommonCmd::setSre ),

on_get_string( servCommonCmd::cmd_test, &servCommonCmd::getTest ),

end_of_entry()

servCommonCmd::servCommonCmd(QString name, ServiceId eId):serviceExecutor(name, eId)
{

}

QString servCommonCmd::getIdn()
{
    QString version = "00.00.00.SP0";
    query(serv_name_utility, servUtility::cmd_softwar_ver, version );

    QString idn = QString("RIGOL TECHNOLOGIES,DS7000,DS70541234567890,%1").arg(version);
    return idn;
}

quint8 servCommonCmd::getStb()
{
    return 0;
}

void servCommonCmd::setSre(quint8 mask)
{
    RegSTB.setIMask((eventReg)mask);
}

quint8 servCommonCmd::getSre()
{
    return (quint8)RegSTB.getIMask();
}

bool servCommonCmd::getTrg()
{
    return false;
}

quint8 servCommonCmd::getEsr()
{
    return (quint8)RegESR.getReg();
}

void servCommonCmd::setEse(quint8 mask)
{
    RegESR.setIMask((eventReg)mask);
}

quint8 servCommonCmd::getEse()
{
    return RegESR.getIMask();
}



