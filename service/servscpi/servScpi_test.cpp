#include "servscpitest.h"
#include "servscpicommon.h"
#include "../../service/servdso/sysdso.h"

static bool flag;

scpiTestThread testThread;

QString servCommonCmd::getTest()
{
//    testThread.start();
    return "scpi test ok\n";
}


CTestFormater::CTestFormater()
{
    pfile = NULL;
    path = "/user/testData.bin";
}

CTestFormater::~CTestFormater()
{
    if( pfile != NULL)
    {
        pfile->close();
        delete pfile;
    }
    path.clear();
}

scpiError CTestFormater::format(GCVal /*&val*/)
{
    flag = true;
    return scpi_err_none;
}

quint32 CTestFormater::getOutData(QByteArray &buff, quint32 len)
{

#if 0   //!test1 返回固定字符串
    buff = "scpi test ok\n";
    mOutDataLen = 0;
    return buff.count();
#endif

#if 1  //!test2 读取文件返回

    //!打开文件，生成头信息
    if((!path.isEmpty()) && ( NULL == pfile))
    {
        if( !QFile::exists(path) )
        {
            return 0;
        }

        pfile = new QFile(path);
        Q_ASSERT(NULL != pfile);
        if(pfile->open(QIODevice::ReadOnly))
        {
            mOutDataLen = pfile->size();

            mOutStr.append(QString("#9%1").arg(mOutDataLen,9,10,QLatin1Char('0')));

            mOutDataLen += mOutStr.size();
        }
        else
        {
            return 0;
        }
    }

    //! 如果缓存中的数据量小于接口请求的数据量，则从文件中读取数据来补够接口请求的数据量
    //! 若缓存中的数据量大于接口的请求数量，则直接从缓存中读取。
    if((int)len > mOutStr.size())
    {
        Q_ASSERT(NULL != pfile);
        mOutStr.append( pfile->read(len - mOutStr.size()) );
        buff = mOutStr;
        mOutStr.clear();

    }
    else
    {
        buff.append(mOutStr.data(),len);
        mOutStr.remove(0,len);
    }

    mOutDataLen -= buff.size();
    return buff.size();
#endif
}

bool CTestFormater::isWaiting()
{
#if 0  //!test1 延时输出
    return flag;
#endif

#if 1  //!test2 实时输出
    return false;
#endif
}
//#include "../../service/servwfmdata/servwfmdata.h"
//#include "../../service/service_name.h"
void scpiTestThread::run()
{
    qDebug()<<"flag = ture";

     QThread::msleep(500);

//    int i = 500;
//    CArgument executeOutData;
//    serviceExecutor::query(serv_name_wfm_data, servWfmData::CMD_MODE, executeOutData);
//    qDebug()<<"executeOutData = "<<executeOutData[0].iVal;
    qDebug()<<"flag = false";
    flag = false;
    sysSetScpiEvent(scpi_event_output_queue,1);
}
