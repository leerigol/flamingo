#ifndef SERVSCPI_H
#define SERVSCPI_H

#include "../service.h"
#include <QTcpSocket>
#include <QTcpServer>
#include "../../com/lxiInterface/vxi11/vxi11thread.h"
#include "stdio.h"
namespace scpi_parse{
class  CScpiController;
}
using namespace scpi_parse;

class  usbtmc488interface;
class  lxiInterface;
class  CSocketInterface;
class  CUsbGpibInterface;

class  MessageExchange;


class servScpi : public serviceExecutor
{
    Q_OBJECT

    DECLARE_CMD()
public:
    servScpi( QString name, ServiceId eId = E_SERVICE_ID_NONE );
    ~servScpi();

     DsoErr start();
     void   registerSpy();
private:
    CScpiController *pScpiController;
    MessageExchange *pMesExchange;

/*!------------------------socket interface------------------------*/
private:
    const static quint16       _SOCKET_PORT=5555;
    QTcpServer                 mTcpServer;
    QList<CSocketInterface *>  pSocketIntfList;
public :
    CSocketInterface *getSocketInterface(int id);
    void              onSocketNewConnection();
    DsoErr            socketTransferRecycle();
/*! -----------------------------------------------------------------*/

/*!------------------------USBtmc 488 interface---------------------*/
private:
    usbtmc488interface  *pUsbtmc488Intf;
public :
    usbtmc488interface  *getUsbtmcInterface(int id);
    DsoErr               onUsbTmcNewConnection();
/*! -----------------------------------------------------------------*/

/*!------------------------LXI interface-----------------------------*/
private:
    vxi11thread                      vxi11;
    static QList<lxiInterface *>     pLxiIntfList;

private:
    DsoErr onStartVxi(int);

public :
    static lxiInterface *getLxiInterface(  int id);
    DsoErr               onLxiNewConnection(CArgument arg);
    DsoErr               onLxiDisconnected( CArgument arg);

    DsoErr               lXiTransferRecycle();
    QString              getNetStat();
    bool                 checkTcpStat(QString &netStat, QString ip, int port);
/*! --------------------------------------------------------------*/

/*!------------------------USB GPIB interface---------------------*/
private:
    CUsbGpibInterface   *pUsbgpibIntf;
    int                  mUsbgpibAddr;
public :
    CUsbGpibInterface   *getUsbgpibInterface(int id);
    DsoErr               onUsbgpibNewConnection();
    DsoErr               setUsbgpibAddr(int addr);
    int                  getUsbgpibAddr();
    void                 onUsbgpibParseEnd();
    void                 onCheckLink(void);
/*! -----------------------------------------------------------------*/

/*! ----------------------interface Active---------------------------*/
public :
    DsoErr setActiveOutInterface(CArgument argi);
    void   getActiveOutInterface(CArgument argo);

private :
    void    scpiEvent(CArgument arg);

/*! ------------------ 为满足服务要求，过滤一些指令 ----------------------*
 * 过滤服务的优先级大于白名单
 */
private :
    static QMultiMap<QString, int>  s_TempMap;

public :
    static void addTempMap(QString servName, QList<int> &cmdList);
    DsoErr   setExecuteEnab(bool b);
    void     setFilterServive(CArgument arg);
    DsoErr   setExecuteLock();
    DsoErr   setExecuteUnLock();
/*! ------------------------- lock -----------------------------*/
};

#endif // SERVSCPI_H
