#include "messageExchange.h"

#include "../../com/scpiparse/cscpiparser.h"
#include "../../com/scpiparse/cscpieventreg.h"
using namespace scpi_parse;

MessageExchange::MessageExchange()
{
    pCurrentIntf = NULL;
    connect( &RegSTB, SIGNAL(srq(quint8)), this, SLOT(srqSlots(quint8)));
}

bool MessageExchange::brq() const
{
    return m_brq;
}

void MessageExchange::setBrq(bool brq)
{
    m_brq = brq;
}

bool MessageExchange::bav() const
{
    return m_bav;
}

void MessageExchange::setBav(bool bav)
{
    m_bav = bav;
}

bool MessageExchange::dcas() const
{
    return m_dcas;
}

void MessageExchange::setDcas(bool dcas)
{
    m_dcas = dcas;
}

bool MessageExchange::getGET() const
{
    return m_GET;
}

void MessageExchange::setGET(bool GET)
{
    m_GET = GET;
}

bool MessageExchange::getPMT_SENT() const
{
    return m_PMT_SENT;
}

void MessageExchange::setPMT_SENT(bool PMT_SENT)
{
    m_PMT_SENT = PMT_SENT;
}

bool MessageExchange::getEom() const
{
    return m_eom;
}

void MessageExchange::setEom(bool eom)
{
    m_eom = eom;
}

bool MessageExchange::getQuery() const
{
    return m_query;
}

void MessageExchange::setQuery(bool query)
{
    m_query = query;
}

bool MessageExchange::getP_idle() const
{
    return m_p_idle;
}

void MessageExchange::setP_idle(bool p_idle)
{
    m_p_idle = p_idle;
}

bool MessageExchange::getP_blocked() const
{
    return m_p_blocked;
}

void MessageExchange::setP_blocked(bool p_blocked)
{
    m_p_blocked = p_blocked;
}

bool MessageExchange::getEc_idle() const
{
    return m_ec_idle;
}

void MessageExchange::setEc_idle(bool ec_idle)
{
    m_ec_idle = ec_idle;
}

bool MessageExchange::getEc_blocked() const
{
    return m_ec_blocked;
}

void MessageExchange::setEc_blocked(bool ec_blocked)
{
    m_ec_blocked = ec_blocked;
}

bool MessageExchange::getPon() const
{
    return m_pon;
}

void MessageExchange::setPon(bool pon)
{
    if(pon)
    {
         RegESR.setRb(scpi_event_b7);
    }
    else
    {
        RegESR.rstRb(scpi_event_b7);
    }
}

bool MessageExchange::getResponseData() const
{
    return m_responseData;
}

void MessageExchange::setResponseData(bool responseData)
{
    m_responseData = responseData;
}

bool MessageExchange::getRf_blocked() const
{
    return m_rf_blocked;
}

void MessageExchange::setRf_blocked(bool rf_blocked)
{
    m_rf_blocked = rf_blocked;
}

bool MessageExchange::getMAV() const
{
    return m_MAV;
}

void MessageExchange::setMAV(bool mav)
{
    static qint64 emitCount = 0;
    Q_UNUSED(emitCount)

    if(pCurrentIntf == NULL)
    {
        qWarning()<<__FILE__<<__LINE__<<"pCurrentIntf == NULL";
        return;
    }

    if( mav && pCurrentIntf->outisValid() )
    {
        //!设置STB寄存器的: STB bit4 ----- MAV
        RegSTB.setRb(scpi_event_b4);
        SCPI_INTERFACE_DEBUG("emit out: %lld",emitCount++);
        emit intfOutReq();
    }
    else
    {
        RegSTB.rstRb(scpi_event_b4);
    }
}

BuffStatus MessageExchange::getIb_buff() const
{
    return m_ib_buff;
}

void MessageExchange::setIb_buff(const BuffStatus &ib_buff)
{
    m_ib_buff = ib_buff;
}

CScpiInterface *MessageExchange::getPCurrentIntf() const
{
    return pCurrentIntf;
}

void MessageExchange::setPCurrentIntf(CScpiInterface *pIntf)
{
    Q_ASSERT( pIntf != NULL );
        //!解绑ＳＴＢ信号与其他的接口的链接
         disconnect(this,SIGNAL(intfOutReq()), 0, 0);

        //!切换信息交换中心的当前接口
        pCurrentIntf = pIntf;

        //! 重新绑定发送请求信号 与 当前接口 的 链接
        connect( this, SIGNAL(intfOutReq()), pCurrentIntf, SLOT(onRequestOut()) );
}


BuffStatus MessageExchange::getOq_queue() const
{
    return m_oq_queue;
}

void MessageExchange::setOq_queue(const BuffStatus &oq_queue)
{
    m_oq_queue = oq_queue;
}

void MessageExchange::srqSlots(quint8 event)
{
    Q_ASSERT( pCurrentIntf != NULL );
    pCurrentIntf->srqRespond(event);
}

