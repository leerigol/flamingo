#include "servscpicommon.h"
#include "../servutility/servutility.h"
#include "../servstorage/servstorage.h"

#include "../../com/scpiparse/cscpieventreg.h"
using namespace scpi_parse;

/*! stb */
#define STB_TRG      scpi_event_b0
#define STB_USER     scpi_event_b1
#define STB_MSG      scpi_event_b2
#define STB_RESERVE  scpi_event_b3
#define STB_MAV      scpi_event_b4
#define STB_ESB      scpi_event_b5
#define STB_RQS_MSS  scpi_event_b6
#define STB_OPER     scpi_event_b7

/*! ESR */
#define ESR_OPC      scpi_event_b0   //!操作完成
#define ESR_RQL      scpi_event_b1   //!设备请求控制
#define ESR_QYE      scpi_event_b2   //!查询错误
#define ESR_DDE      scpi_event_b3   //!设备相关错误
#define ESR_EXE      scpi_event_b4   //!命令执行错误
#define ESR_QME      scpi_event_b5   //!命令错误（格式错误或者命令表中没有）
#define ESR_URQ      scpi_event_b6   //!前面板按键被按下
#define ESR_PON      scpi_event_b7   //!电源由ＯＦＦ－》ＯＮ



servCommonCmd::servCommonCmd(QString name, ServiceId eId):serviceExecutor(name, eId)
{
    serviceExecutor::baseInit();
}

DsoErr servCommonCmd::setCls()
{
    //!清除 OPER,ESR,STB寄存器的值,不清掩码。
    RegESR.clearReg();
    RegSTB.clearReg();
    RegOPER.clearReg();
    _scpiErr.clear();
    return ERR_NONE;
}

DsoErr servCommonCmd::setOpc()
{
    //!标准事件寄存器 ESR的OPC位值1
    RegESR.setRb(ESR_OPC);
    return ERR_NONE;
}

int servCommonCmd::getOpc()
{
    return ( (1<<ESR_OPC)&(RegESR.getReg()) );
}

DsoErr servCommonCmd::setRcl(int /*i*/)
{
    //!清除 标准事件寄存器、查询事件寄存器、操作状态寄存器、操作状态子寄存器、状态字节寄存器
    RegESR.clearReg();
    RegSTB.clearReg();
    return ERR_NONE;
}

DsoErr servCommonCmd::setSav(int i)
{
    Q_UNUSED(i)
    return ERR_NONE;
}

DsoErr servCommonCmd::setRst()
{
    //!复位电源操作到出厂状态。
    return send(serv_name_storage, servStorage::FUNC_RESTORE_DEFAULT, (int)1);
}

QString servCommonCmd::getIdn()
{
    QString model =  servUtility::getSystemModel();
    QString version= servUtility::getSystemVersion();
    QString serial = servUtility::getSystemSerial();

    QString idn = QString("RIGOL TECHNOLOGIES,%1,%2,%3").arg(model).arg(serial).arg(version);
    return idn;
}

quint8 servCommonCmd::getStb()
{
    return (quint8)RegSTB.getReg()& (0xF7);
}

DsoErr servCommonCmd::setSre(quint8 mask)
{
    RegSTB.clearReg();
    RegSTB.setIMask((eventReg)mask);
    return ERR_NONE;
}

quint8 servCommonCmd::getSre()
{
    //! Not used: bit 3, bit6
    return (quint8)RegSTB.getIMask() & (0xB7);
}

bool servCommonCmd::getTrg()
{
   return false;
}

quint8 servCommonCmd::getEsr()
{
    quint8 i = (quint8)RegESR.getReg();
    RegESR.clearReg();
    return i;
}

DsoErr servCommonCmd::setEse(quint8 mask)
{
    RegESR.clearReg();
    RegESR.setIMask((eventReg)mask);
    return ERR_NONE;
}

quint8 servCommonCmd::getEse()
{
    return RegESR.getIMask();
}

int servCommonCmd::getScpiError()
{
    return _scpiErr.getError();
}

bool servCommonCmd::getTst()
{
    return false;
}

DsoErr servCommonCmd::setWait()
{
    return ERR_NONE;
}



