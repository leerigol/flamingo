######################################################################
# Automatically generated by qmake (3.0) ?? 10? 29 10:27:34 2016
######################################################################

QT       += core
QT       += gui
QT       += widgets
QT       += network

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/services/servscpi
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

# Input
HEADERS += servscpi.h \
    messageExchange.h \
    servscpitest.h \
    servscpicommon.h \
    servscpidso.h
SOURCES += servscpi.cpp \
    messageExchange.cpp \
    servScpi_common.cpp \
    servScpi_test.cpp \
    servscpi_map.cpp

CONFIG += C++11
#DEFINES += Q_COMPILER_INITIALZER_LISTS
