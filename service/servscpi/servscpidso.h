#ifndef SCPISERVCMD
#define SCPISERVCMD


namespace dsoServScpi{

//enum scpiEvent
//{
//    srq_event_mav,        //!输出缓存数据变化
//    srq_event_opc,        //!操作完成
//    srq_event_rql,        //!设备请求控制
//    srq_event_qye,        //!查询指令错误
//    srq_event_dde,        //!设备相关错误
//    srq_event_exe,        //!命令执行错误
//    srq_event_qme,        //!命令错误（格式错误或者命令表中没有）
//    srq_event_pon,        //!电源由ｏｆｆ－》ｏｎ
//};


enum servScpiCmd
{
    cmd_scpi_event       = 1, //! CArgument, [0]: dsoScpiEvent,[1]: bitVal
    cmd_active_out_interface,

    cmd_usbTmc_newconnected ,

    cmd_lxi_newconnected    ,
    cmd_lxi_disconnected    ,

    cmd_usbgpib_newconnected    ,
    cmd_usbgpib_disconnected    ,
    cmd_usbgpib_addr           ,

    cmd_lxi_interface       ,

    cmd_usbgpib_parse_end,  //!为了解决rigol的GPIB，必须有回执的机制而设定。
    cmd_usbgpib_check_link, //!当系统检测到usb口发生中断时，去检测gpib是否连接。
    cmd_start_vxi,          // start vxi later.added by hxh

    cmd_scpi_execute_en,
    cmd_scpi_execute_lock,
    cmd_scpi_execute_unlock,
    cmd_scpi_serv_filter,
};

enum servScpiCommonCmd
{
    cmd_common_cls = 1,

    cmd_common_esr,
    cmd_common_ese,

    cmd_common_idn,
    cmd_common_lrn,
    cmd_common_opc,
    cmd_common_opt,
    cmd_common_rcl,
    cmd_common_rst,
    cmd_common_sav,

    cmd_common_stb,
    cmd_common_sre,

    cmd_common_trg,
    cmd_common_tst,
    cmd_common_wai, /*! 15*/

    cmd_scpi_error,

    cmd_common_test = 200, /*! test*/
};

enum servScpiKeyCmd
{
    cmd_key_press = 1,
    cmd_key_increase,
    cmd_key_decrease,
};

}



#endif // SCPISERVCMD

