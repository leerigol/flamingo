#ifndef SERVCOMMONCMD_H
#define SERVCOMMONCMD_H

#include "../service.h"

class servCommonCmd: public serviceExecutor
{
    Q_OBJECT

    DECLARE_CMD()

public:
    servCommonCmd(QString name, ServiceId eId = E_SERVICE_ID_NONE);

public:

    DsoErr setCls();
    DsoErr setOpc();
    int    getOpc();
    DsoErr setRcl(int);
    DsoErr setSav(int i);
    DsoErr setRst();

    QString getIdn();

    quint8  getStb();
    DsoErr  setSre(quint8 mask);
    quint8  getSre();

    bool    getTrg();

    quint8  getEsr();
    DsoErr  setEse(quint8 mask);
    quint8  getEse();

    int     getScpiError();

    bool    getTst();
    DsoErr  setWait();

public:
  QString    getTest();

};

#endif // SERVCOMMONCMD_H
