#ifndef SCPITEST_H
#define SCPITEST_H
#include <QThread>
#include "../../com/scpiparse/cconverter.h"

class CTestFormater:public CFormater
{
public:
    CTestFormater();
    ~CTestFormater();
public:

    scpiError format(GCVal);
    quint32   getOutData(QByteArray &buff, quint32 len);
    bool      isWaiting();

    QString    path;
    QFile     *pfile;
};



class scpiTestThread : public QThread
{
public:
    scpiTestThread(){}

protected:
    void  virtual run();
};

#endif // SCPITEST_H
