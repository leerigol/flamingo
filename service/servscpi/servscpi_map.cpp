#include "servscpi.h"
#include "servscpicommon.h"

#include "servscpidso.h"

using namespace dsoServScpi;
/*scpi serv*/
IMPLEMENT_CMD( serviceExecutor, servScpi  )

on_set_void_arg( cmd_scpi_event,           &servScpi::scpiEvent             ),
on_set_int_bool( cmd_scpi_execute_en,      &servScpi::setExecuteEnab        ),
on_set_void_arg( cmd_scpi_serv_filter,     &servScpi::setFilterServive      ),
on_set_int_void( cmd_scpi_execute_lock,    &servScpi::setExecuteLock        ),
on_set_int_void( cmd_scpi_execute_unlock,  &servScpi::setExecuteUnLock      ),

on_set_int_void( cmd_usbTmc_newconnected,  &servScpi::onUsbTmcNewConnection ),

on_set_int_arg(  cmd_lxi_newconnected,     &servScpi::onLxiNewConnection    ),
on_set_int_arg(  cmd_lxi_disconnected,     &servScpi::onLxiDisconnected     ),

on_set_int_void( cmd_usbgpib_newconnected, &servScpi::onUsbgpibNewConnection),
on_set_int_int(  cmd_usbgpib_addr,         &servScpi::setUsbgpibAddr        ),
on_get_int(      cmd_usbgpib_addr,         &servScpi::getUsbgpibAddr        ),
on_set_void_void(cmd_usbgpib_parse_end,    &servScpi::onUsbgpibParseEnd     ),
on_set_void_void(cmd_usbgpib_check_link,   &servScpi::onCheckLink           ),

on_set_int_arg(  cmd_active_out_interface, &servScpi::setActiveOutInterface ),
on_get_void_argo(cmd_active_out_interface, &servScpi::getActiveOutInterface ),

on_set_int_int(  cmd_start_vxi,            &servScpi::onStartVxi            ),

end_of_entry()

/*common */
IMPLEMENT_CMD( serviceExecutor, servCommonCmd )
start_of_entry()
on_get_string(   cmd_common_idn,   &servCommonCmd::getIdn ),
on_get_int(      cmd_common_esr,   &servCommonCmd::getEsr ),
on_get_int(      cmd_common_stb,   &servCommonCmd::getStb ),
on_get_int(      cmd_common_ese,   &servCommonCmd::getEse ),
on_set_int_int(  cmd_common_ese,   &servCommonCmd::setEse ),
on_get_int(      cmd_common_sre,   &servCommonCmd::getSre ),
on_set_int_int(  cmd_common_sre,   &servCommonCmd::setSre ),
on_get_int(      cmd_common_opc,   &servCommonCmd::getOpc ),
on_set_int_void(  cmd_common_opc,   &servCommonCmd::setOpc ),
on_set_int_void( cmd_common_cls,   &servCommonCmd::setCls ),
on_set_int_void( cmd_common_rst,   &servCommonCmd::setRst ),
on_set_int_int ( cmd_common_rcl,   &servCommonCmd::setRcl ),
on_set_int_int ( cmd_common_sav,   &servCommonCmd::setSav ),

on_set_int_void( cmd_common_wai,   &servCommonCmd::setWait ),
on_get_bool(     cmd_common_tst,   &servCommonCmd::getTst ),

on_get_int(      cmd_scpi_error,   &servCommonCmd::getScpiError ),

on_get_string(   cmd_common_test,  &servCommonCmd::getTest ),

end_of_entry()
