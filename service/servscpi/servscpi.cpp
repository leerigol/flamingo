#include "servscpi.h"
#include "../servgui/servgui.h"
#include "../service_name.h"
#include "./messageExchange.h"
#include "../../com/socketinterface/csocketinterface.h"
#include "../../com/usbtmc488interface/usbtmc488interface.h"
#include "../../com/lxiInterface/lxiinterface.h"
#include "../../com/scpiparse/cscpieventreg.h"
#include "./servscpidso.h"
#include "../../com/usbgpibinterface/usbgpibinterface.h"

QList<lxiInterface *> servScpi::pLxiIntfList;
servScpi::servScpi( QString name, ServiceId eId) : serviceExecutor(name, eId )
{
    serviceExecutor::baseInit();

    mTcpServer.setMaxPendingConnections(SOCKET_INTERFACE_MAX);
    pScpiController = NULL;
    pMesExchange    = NULL;
    pUsbtmc488Intf  = NULL;
    pUsbgpibIntf    = NULL;
    mUsbgpibAddr    = 1;
}

servScpi::~servScpi()
{
    if(pScpiController != NULL)
    {
        delete pScpiController;
        pScpiController = NULL;
    }
    if(pMesExchange != NULL)
    {
        delete pMesExchange;
        pMesExchange = NULL;
    }
    if(pUsbtmc488Intf != NULL)
    {
        delete pUsbtmc488Intf;
        pUsbtmc488Intf = NULL;
    }

    for(int i = 0; i< pLxiIntfList.count(); i++)
    {
        if(pLxiIntfList.at(i) != NULL)
        {
            delete pLxiIntfList.at(i);
        }
    }
    pLxiIntfList.clear();

    for(int i = 0; i< pSocketIntfList.count(); i++)
    {
        if(pSocketIntfList.at(i) != NULL)
        {
            delete pSocketIntfList.at(i);
        }
    }
    pSocketIntfList.clear();
}

/*!
 * \brief servScpi::start
 * \return
 * \todo socket需要动态得到
 */
DsoErr servScpi::start()
{
    pScpiController = new CScpiController();
    Q_ASSERT( pScpiController != NULL );
    pScpiController->start();
    CScpiInterface::setController( pScpiController );

    pMesExchange    = new MessageExchange();
    Q_ASSERT(pMesExchange != NULL);

/*!--------------------------------------usb tmc 488------------------------------------------- */
    pUsbtmc488Intf  = new usbtmc488interface(scpi_intf_usb, USB_INTERFACE_MAX);
    Q_ASSERT( pUsbtmc488Intf != NULL );
/*!------------------------------------------------------------------------------------------- */


/*!----------------------------------------lxi------------------------------------------------- */
    for(int i = 0; i< LXI_INTERFACE_MAX; i++)
    {
        lxiInterface *p = new lxiInterface(scpi_intf_lxi, INTERFACE_INVALID_ID);
        Q_ASSERT( p != NULL );
        pLxiIntfList.append( p );
    }

    vxi11CallbackRegister();
/*!-------------------------------------------------------------------------------------------- */


/*!--------------------------------------Socket------------------------------------------------- */
    for(int i = 0; i< SOCKET_INTERFACE_MAX; i++)
    {
        CSocketInterface *p = new CSocketInterface(scpi_intf_socket, INTERFACE_INVALID_ID);
        Q_ASSERT( p != NULL );
        pSocketIntfList.append( p );
    }

    CSocketInterface::setServer( &mTcpServer );
    if ( mTcpServer.listen( QHostAddress::Any, _SOCKET_PORT ) )
    {
        //! 这里要用直接调用的方式Qt::DirectConnection, 应为创建服务即调用start的线程后期会被挪走,不能采用异步的方式调用.
        QObject::connect( &mTcpServer, &QTcpServer::newConnection,
                          this,        &servScpi::onSocketNewConnection,
                          Qt::DirectConnection );
    }
    else
    {
        mTcpServer.close();
        qDebug()<<__FILE__<<__LINE__<<mTcpServer.errorString();
    }
/*!------------------------------------------------------------------------------------------- */

/*!--------------------------------------usb gpib------------------------------------------- */
    pUsbgpibIntf  = new CUsbGpibInterface(scpi_intf_gpib, GPIB_INTERFACE_MAX);
    Q_ASSERT( pUsbgpibIntf != NULL );
/*!------------------------------------------------------------------------------------------- */

    return ERR_NONE;
}

void servScpi::registerSpy()
{

}

CSocketInterface *servScpi::getSocketInterface(int id)
{
    CSocketInterface* pIoIntf = NULL;

    for(int i = 0; i < pSocketIntfList.count(); i++)
    {
        pIoIntf = pSocketIntfList.at(i);
        Q_ASSERT( pIoIntf != NULL );

        if(pIoIntf->getId() == id)
        {
           return pIoIntf;
        }
    }

    return NULL;
}

void servScpi::onSocketNewConnection()
{
    static int id = 0;
    QTcpSocket          *pSocket     = mTcpServer.nextPendingConnection();
    CSocketInterface    *pInterface  = getSocketInterface(INTERFACE_INVALID_ID);

    if( pInterface != NULL )
    {
        id++;
        pInterface->onNewConnection(pSocket, id);
    }
    else
    {
        servGui::showInfo(sysGetString( INF_SOCKET_CEILING, "SOCKET connected ceiling") );
        delete pSocket;
        pSocket =   NULL;
    }
}

DsoErr servScpi::socketTransferRecycle()
{
    DsoErr err = INF_LXI_CEILING;
    return err;
}

usbtmc488interface *servScpi::getUsbtmcInterface(int id)
{
    if( (id >= 0) && ( id <= USB_INTERFACE_MAX) )
    {
        return pUsbtmc488Intf;
    }
    else
    {
        return NULL;
    }
}

DsoErr servScpi::onStartVxi(int)
{
    vxi11.start();
    return ERR_NONE;
}

DsoErr servScpi::onUsbTmcNewConnection()
{
    usbtmc488interface  *pInterface = getUsbtmcInterface(USB_INTERFACE_MAX);

    Q_ASSERT(pInterface != NULL);
    if( pInterface == NULL )
    {
        return ERR_INVALID_INPUT;
    }

    pInterface->onNewConnection();

    return ERR_NONE;
}

lxiInterface *servScpi::getLxiInterface(int id)
{
    lxiInterface* pIoIntf = NULL;

    for(int i = 0; i < pLxiIntfList.count(); i++)
    {
        pIoIntf = pLxiIntfList.at(i);
        Q_ASSERT( pIoIntf != NULL );

        if(pIoIntf->getId() == id)
        {
           return pIoIntf;
        }
    }

    return NULL;
}

DsoErr servScpi::onLxiNewConnection(CArgument arg)
{
    DsoErr err = ERR_NONE;

    if(arg.size() != 3)
    {
        return ERR_INVALID_INPUT;
    }

    lxiInterface  *pInterface  = getLxiInterface(INTERFACE_INVALID_ID);

    //! 当前没有空闲接口，尝试释放垃圾资源。
    if( NULL == pInterface  )
    {
        err = lXiTransferRecycle();
        if ( ERR_NONE != err )
        {
            return err;
        }
        else  //! 释放垃圾资源成功，重新获取空闲接口。
        {
            pInterface  = getLxiInterface(INTERFACE_INVALID_ID);
        }
    }

    //!新建新接口。
    if( pInterface != NULL )
    {
        QString ip   = arg[0].strVal;
        int     id   = arg[1].iVal;
        int     port = arg[2].iVal;

        pInterface->onNewConnection(ip, port, id);
        //qDebug()<<__FILE__<<__LINE__<<ip<<port;

        err = ERR_NONE;
    }
    else
    {
        err = INF_LXI_CEILING;
    }

    return err;
}

DsoErr servScpi::onLxiDisconnected(CArgument arg)
{
    if(arg.size() != 2)
    {
        return ERR_INVALID_INPUT;
    }

    QString ip = arg[0].strVal;
    int     id = arg[1].iVal;

    lxiInterface  *pInterface  = getLxiInterface(id);
    if( pInterface == NULL )
    {
        return INF_LXI_CEILING;
    }

    pInterface->onDisConnected();

    return ERR_NONE;
}

DsoErr servScpi::lXiTransferRecycle()
{
    DsoErr err = INF_LXI_CEILING;

    QString netStat = getNetStat();

    lxiInterface* pIoIntf = NULL;
    QString       ip      = "0.0.0.0";
    int           port    = 0;

    for(int i = 0; i < pLxiIntfList.count(); i++)
    {
        pIoIntf = pLxiIntfList.at(i);
        Q_ASSERT( pIoIntf != NULL );

        pIoIntf->getInterfaceInfo(ip, port);

        if( !checkTcpStat(netStat, ip, port) )
        {
            qDebug()<<__FILE__<<__LINE__<<"Transfer recycle:"<<ip<<port;
            pIoIntf->onDisConnected();
            err = ERR_NONE;
        }
    }
    return err;
}

CUsbGpibInterface *servScpi::getUsbgpibInterface(int id)
{
    if( (id >= 0) && ( id <= GPIB_INTERFACE_MAX) )
    {
        return pUsbgpibIntf;
    }
    else
    {
        return NULL;
    }
}

DsoErr servScpi::onUsbgpibNewConnection()
{
    CUsbGpibInterface  *pInterface = getUsbgpibInterface(GPIB_INTERFACE_MAX);

    Q_ASSERT(pInterface != NULL);
    if( pInterface == NULL )
    {
        return ERR_INVALID_INPUT;
    }

    pInterface->onNewConnection();

    return ERR_NONE;

}

DsoErr servScpi::setUsbgpibAddr(int addr)
{
    Q_ASSERT(pUsbgpibIntf != NULL);
    mUsbgpibAddr = addr;

    if( (-1) != pUsbgpibIntf->setGpibAddr(addr) )
    {
        return ERR_NONE;
    }
    else
    {
        if(pUsbgpibIntf->getConnectionState())
        {
            return ERR_ADDR_SET_FAIL;
        }
        else
        {
            return INF_USB_GPIB_NOT_CONNECTED;
        }
    }
}

int servScpi::getUsbgpibAddr()
{
    return mUsbgpibAddr;
}

void servScpi::onUsbgpibParseEnd()
{
    Q_ASSERT(pUsbgpibIntf != NULL);
    pUsbgpibIntf->parseEnd();
}

void servScpi::onCheckLink()
{
    Q_ASSERT(pUsbgpibIntf != NULL);
    pUsbgpibIntf->checkLink();
}

DsoErr servScpi::setActiveOutInterface(CArgument argi)
{
    if( argi.size() != 2)
    {
        return ERR_INVALID_INPUT;
    }

    scpiIntfType type = (scpiIntfType)argi[0].iVal;
    int          id   = argi[1].iVal;

    CScpiInterface* pIoIntf = NULL;
    switch(type)
    {
    case scpi_intf_socket:
        pIoIntf = getSocketInterface(id);
        break;
    case scpi_intf_usb:
        pIoIntf = getUsbtmcInterface(id);
        break;
    case scpi_intf_lxi:
        pIoIntf = getLxiInterface(id);
        break;
    case scpi_intf_gpib:
        pIoIntf = getUsbgpibInterface(id);
        break;

    default:
        pIoIntf = NULL;
        break;
    }

    if(pIoIntf != NULL)
    {
        pMesExchange->setPCurrentIntf(pIoIntf);
        return ERR_NONE;
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
}

void servScpi::getActiveOutInterface(CArgument argo)
{
    CScpiInterface* pIoIntf = NULL;

    pIoIntf = pMesExchange->getPCurrentIntf();
    if(pIoIntf == NULL)
    {
        return;
    }

    int id = pIoIntf->getId();
    scpiIntfType type = pIoIntf->getType();

    argo.setVal(type,0);
    argo.setVal(id,  1);
}

#define SER_REG_BIT(reg, bit, b) do{\
    if(b){reg.setRb(bit);}\
    else {reg.rstRb(bit);}\
    }while(0)

void servScpi::scpiEvent(CArgument arg)
{
    if(arg.size()<2)
    {
        return;
    }

    dsoScpiEvent event  = (dsoScpiEvent)arg[0].iVal;
    bool         bitVal = (bool)        arg[0].iVal;

    switch(event)
    {
    //! over load
    case  scpi_event_ch1_overload:

        break;
    case  scpi_event_ch2_overload:

        break;
    case  scpi_event_ch3_overload:

        break;
    case  scpi_event_ch4_overload:

        break;
    case  scpi_event_ext_overload:

        break;

        //! fault
    case  scpi_event_ch1_fault:

        break;
    case  scpi_event_ch2_fault:

        break;
    case  scpi_event_ch3_fault:

        break;
    case  scpi_event_ch4_fault:

        break;

    case  scpi_event_ext_fault:

        break;

        //! mask
    case  scpi_event_mask_complete:

        break;
    case  scpi_event_mask_fail:

        break;
    case  scpi_event_mask_started:

        break;

        //! io
    case  scpi_event_io_complete:

        break;
    case  scpi_event_io_fail:

        break;

        //! oscr
    case  scpi_event_calibrating:

        break;
    case  scpi_event_setting:

        break;
    case  scpi_event_ranging:
        SER_REG_BIT(RegOPER, scpi_event_b3, bitVal);
        break;
    case  scpi_event_sweeping:
        break;

    case  scpi_event_measuring:

        break;
    case  scpi_event_waitingfortrigger:
        SER_REG_BIT(RegOPER, scpi_event_b5, bitVal);
        break;
    case  scpi_event_waitingforarm:

        break;
    case  scpi_event_correcting:

        break;

    case  scpi_event_inst_summary:

        break;
    case  scpi_event_command_warnning:

        break;

        //! esr
    case  scpi_event_operation_complete:
        SER_REG_BIT(RegESR, scpi_event_b0, bitVal);
        break;
    case  scpi_event_request_control:
        SER_REG_BIT(RegESR, scpi_event_b1, bitVal);
        break;
    case  scpi_event_query_error:
        SER_REG_BIT(RegESR, scpi_event_b2, bitVal);
        break;
    case  scpi_event_device_depend_error:
        SER_REG_BIT(RegESR, scpi_event_b3, bitVal);
        break;
    case  scpi_event_execution_error:
        SER_REG_BIT(RegESR, scpi_event_b4, bitVal);
        break;
    case  scpi_event_command_error:
        SER_REG_BIT(RegESR, scpi_event_b5, bitVal);
        break;
    case  scpi_event_user_request:
        SER_REG_BIT(RegESR, scpi_event_b6, bitVal);
        break;
    case  scpi_event_power_on:
        SER_REG_BIT(RegESR, scpi_event_b7, bitVal);
        break;

        //! qsr
    case  scpi_event_voltage:

        break;
    case  scpi_event_current:

        break;
    case  scpi_event_time:

        break;
    case  scpi_event_power:

        break;

    case  scpi_event_temperature:

        break;
    case  scpi_event_frequency:

        break;
    case  scpi_event_phase:

        break;
    case  scpi_event_modulation:

        break;

    case  scpi_event_calibration:

        break;

        //! stb
    case  scpi_event_error_queue:

        break;
    case  scpi_event_output_queue:
        //qDebug() << "4.." << QTime::currentTime() << "read...";
          pMesExchange->setMAV(bitVal);
        //qDebug() << "4.." << QTime::currentTime() << "read...over";
        break;
    default:
        break;
    }
}


QString servScpi::getNetStat()
{
    QProcess process;
    process.start("netstat -anlpt");
    process.waitForFinished();
    QByteArray netSata = process.readAllStandardOutput();
    //qDebug(netSata);
    return QString(netSata);
}

bool servScpi::checkTcpStat(QString &netStat, QString ip, int port)
{
    QString tcpInfo = QString("%1:%2").arg(ip).arg(port);
    //qDebug()<<__FILE__<<__LINE__<<"tcpInfo:"<<tcpInfo;
    return netStat.contains(tcpInfo);
}


QMultiMap<QString, int>  servScpi::s_TempMap;

void servScpi::addTempMap(QString servName, QList<int> &cmdList)
{
    for(int i = 0; i < cmdList.count(); i++)
    {
        //qDebug()<<__FILE__<<__LINE__<<servName<<cmdList.at(i);
        s_TempMap.insert(servName, cmdList.at(i));
    }
}

DsoErr servScpi::setExecuteEnab(bool b)
{
    Q_ASSERT( pScpiController != NULL );
    if(b)
    {
        pScpiController->setExecuteEnable();
    }
    else
    {
        pScpiController->setExecuteDisable(s_TempMap);
        s_TempMap.clear();
    }
    return ERR_NONE;
}

void servScpi::setFilterServive(CArgument arg)
{
    Q_ASSERT( pScpiController != NULL );

    if(arg.size()<2)
    {
        return;
    }

    QString servName = "";
    bool    b        = false;
    arg.getVal(servName, 0);
    arg.getVal(b,        1);

    //qDebug()<<__FILE__<<__LINE__<<servName<<b;

    if(b)
    {
        pScpiController->addFilterServive(servName);
    }
    else
    {
        pScpiController->rmFilterServive(servName);
    }
}


/*!
 * \brief servScpi::setExecuteLock
 *  ‘必须同步调用，否则可能lock不及时，导致指令返回。
 */
DsoErr servScpi::setExecuteLock()
{
    Q_ASSERT( pScpiController != NULL );
    pScpiController->setExecuteLock();
    return ERR_NONE;
}

DsoErr servScpi::setExecuteUnLock()
{
    Q_ASSERT( pScpiController != NULL );
    pScpiController->setExecuteUnLock();
    return ERR_NONE;
}


