#ifndef SCPIMESSAGEEXCHANGECONTROL_H
#define SCPIMESSAGEEXCHANGECONTROL_H

#include <QObject>

namespace scpi_parse{
class  CScpiInterface;
}
using namespace scpi_parse;


enum BuffStatus
{
    INTERFACE_BUFF_EMPTY,
    INTERFACE_BUFF_FULL,
};


class MessageExchange: public QObject
{
     Q_OBJECT
public:
    MessageExchange();

public:
    bool brq() const;
    bool bav() const;
    bool dcas() const;
    bool getGET() const;
    bool getPMT_SENT() const;
    bool getEom() const;
    bool getQuery() const;
    bool getP_idle() const;
    bool getP_blocked() const;
    bool getEc_idle() const;
    bool getEc_blocked() const;
    bool getPon() const;
    bool getResponseData() const;
    bool getRf_blocked() const;
    bool getMAV() const;
    BuffStatus getOq_queue() const;
    BuffStatus getIb_buff() const;

public Q_SLOTS:
    void setBrq(bool brq);
    void setBav(bool bav);
    void setDcas(bool dcas);
    void setGET(bool GET);
    void setPMT_SENT(bool PMT_SENT);
    void setEom(bool eom);
    void setQuery(bool query);
    void setP_idle(bool p_idle);
    void setP_blocked(bool p_blocked);
    void setEc_idle(bool ec_idle);
    void setEc_blocked(bool ec_blocked);
    void setPon(bool pon);
    void setResponseData(bool responseData);
    void setRf_blocked(bool rf_blocked);
    void setMAV(bool mav);
    void setOq_queue(const BuffStatus &oq_queue);
    void setIb_buff(const BuffStatus &ib_buff);

private:
    /*! interface*/
    CScpiInterface *pCurrentIntf;

    /*! I/O control*/
    bool m_brq;
    bool m_bav;
    bool m_dcas;
    bool m_GET;
    bool m_PMT_SENT;

    /*! Parser */
    bool m_eom;
    bool m_query;
    bool m_p_idle;
    bool m_p_blocked;

    /*! Execution control*/
    bool m_ec_idle;
    bool m_ec_blocked;

    /*! device function*/
    bool m_pon;
    bool m_responseData;

    /*! Response Formatter*/
    bool m_rf_blocked;

    /*! output queue*/
    bool m_MAV;
    BuffStatus m_oq_queue;

    /*! inout Buffer*/
    BuffStatus m_ib_buff;

public:
    void clearOutputQueue();
    void clearInputBuffer();
    void resetResponseFormatter();
    void resetParser();
    void resetExecutionControl();

    CScpiInterface *getPCurrentIntf() const;
    void setPCurrentIntf(CScpiInterface *pIntf);

protected Q_SLOTS:
    void srqSlots(quint8 event);

Q_SIGNALS:
    void intfOutReq();
    void srqSignals(quint8);

};

#endif // SCPIMESSAGEEXCHANGECONTROL_H
