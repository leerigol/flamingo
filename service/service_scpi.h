#ifndef SERVICE_SCPI_H
#define SERVICE_SCPI_H

/**********************************
Only for scpi tool. by hexiaohua
************************************/

#include "servauto/arithauto.h"
#include "servcal/servcal.h"
#include "servch/servch.h"
#include "servcounter/servcounter.h"
#include "servcursor/servcursor.h"
#include "servdecode/decoder.h"
#include "servdecode/servdecode.h"
#include "servDG/servdg.h"
#include "servdisplay/servdisplay.h"
#include "servdso/servdso.h"
#include "servdvm/servdvm.h"
#include "serveyejit/serveyejit.h"
#include "servgui/servgui.h"
#include "servhelp/servhelp.h"
#include "servHisto/servhisto.h"
#include "servHotplug/servhotplug.h"
#include "servhori/servhori.h"
#include "servHotplug/servhotplug.h"
#include "servla/servla.h"
#include "servmeasure/servmeasure.h"
#include "servmask/servmask.h"
#include "servmath/servmath.h"
#include "servlicense/servlicense.h"
#include "servplot/servplot.h"
#include "servquick/servquick.h"
#include "servRecord/servrecord.h"
#include "servref/servref.h"
#include "servscpi/servscpicommon.h"
#include "servscpi/servscpi.h"
#include "servSearch/servsearch.h"
#include "servstorage/servstorage.h"
#include "servtrace/servtrace.h"
#include "servtrig/servtrig.h"
#include "servUPA/servUPA.h"
#include "servutility/servutility.h"
#include "servvertmgr/servvertmgr.h"
#include "servwfmdata/servwfmdata.h"

#include "servscpi/servscpidso.h"
#include "servutility/servinterface/servInterface.h"

#include "servstorage/storage_api.h"

#include "servauto/servauto.h"

#endif // SERVICE_H
