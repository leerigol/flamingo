#ifndef SERVICEFACTORY_H
#define SERVICEFACTORY_H

#include "service.h"

#include "service_name.h"
#include "service_id.h"

#include "./servch/servch.h"
#include "./servhori/servhori.h"

#include "./servscpi/servscpi.h"
#include "./servscpi/servscpicommon.h"

#include "./servmath/servmath.h"
#include "./servplot/servplot.h"
#include "./servtrace/servtrace.h"
#include "./servdisplay/servdisplay.h"
#include "./servtrig/servtrig.h"
#include "./servref/servref.h"
#include "./servla/servla.h"
#include "./servmask/servmask.h"
#include "./servDG/servdg.h"
#include "./servSearch/servsearch.h"
#include "./servstorage/servstorage.h"
#include "./servHotplug/servhotplug.h"
#include "./servvertmgr/servvertmgr.h"

#include "./servmath/servmathsel.h"
#include "./servmath/servmath.h"

#include "./servRecord/servrecord.h"

#include "./servcursor/servcursor.h"

#include "./servmenutest/servmenutest.h"
#include "./servutility/servutility.h"

#include "./servutility/servinterface/servInterface.h"
#include "./servutility/servinterface/servwifi.h"
#include "./servutility/servinterface/servlistener.h"
#include "./servutility/servprinter/servprinter.h"
#include "./servutility/servmail/servmail.h"

#include "./servdecode/servdecode.h"

#include "./servquick/servquick.h"

#include "./servgui/servgui.h"

#include "./servmeasure/servmeasure.h"
#include "./servcounter/servcounter.h"
#include "./servdvm/servdvm.h"
#include "./servUPA/servUPA.h"
#include "./serveyejit/serveyejit.h"
#include "./servdso/servdso.h"
#include "./servhelp/servhelp.h"
#include "./servlicense/servlicense.h"
#include "./servHisto/servhisto.h"

#include "./servcal/servcal.h"

#include "./servwfmdata/servwfmdata.h"
#include "./servauto/servauto.h"

#include "./servmemory/servmemory.h"

/*! \class serviceFactory
* \brief 服务创建工厂
*/
class serviceFactory
{
public:
    static DsoErr createServices();
    static DsoErr destroyServices();

    static DsoErr attachServices( QThread *pThread = NULL );
    static DsoErr startServices( );
    static DsoErr startup();
    static DsoErr serviceup();
    static DsoErr registerSpy();

    static service* findService( const QString &name );

    /*added by hxh for default,serial in and out*/
    static QList<service*>* getServiceList(void);
private:

    static QList<service*> _serviceList;    /*!< 创建的服务列表 \todo 和service中登记的重复？ */  
};

#ifdef _DEBUG
#define LOG_FACTORY()   qDebug()<<__FUNCTION__<<__LINE__
#else
#define LOG_FACTORY()   QT_NO_QDEBUG_MACRO()
#endif

#endif // SERVICEFACTORY_H
