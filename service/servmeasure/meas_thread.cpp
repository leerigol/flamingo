#include "meas_thread.h"
#include "../../service/servicefactory.h"
#include "../service.h"

servMeasureThread::servMeasureThread(servMeasure *pMeasure) : QThread( )
{
    m_pServMeasure = pMeasure;
}

void servMeasureThread::run()
{
    int updateTime = servMeasure::MEAS_NORM_TIM;
    while(1)
    {
        if( m_pServMeasure->haveMeasData() )
        {
            //timeout for 4second
            m_pServMeasure->startTimer(4000,0,timer_repeat);
            servMeasure::m_bTraceTimeout = false;

            servMeasure::m_bSoftCalc = !(m_pServMeasure->isFPGA_Mode());          

            if( servMeasure::m_bSoftCalc )
            {                
                m_pServMeasure->m_MeasSrcSema.acquire();                

                if( m_pServMeasure->isEngineStop() && m_pServMeasure->hasMathSrc())
                {
                    QThread::msleep(200);//wait math
                }
                //qDebug() << "start measure:" << servMeasure::m_nConfigEngineID;
            }
            servMeasure::m_nConfigEngineID = servHori::getAttrId();

            if( m_pServMeasure->get_RangeMode() == (int)FPGA_MODE)
            {
                if( m_pServMeasure->isEngineStop())
                {
                    m_pServMeasure->m_MeasSrcSema.acquire();
                    QThread::msleep(200);
                    if( !m_pServMeasure->traceValid() )
                    {
                        continue;
                    }
                }

                m_pServMeasure->getFPGA_MeasData();
            }

            if( m_pServMeasure->calcMeasResult() &&
                servMeasure::m_nConfigEngineID == servHori::getAttrId())
            {
                m_pServMeasure->setMeasValReady();
                //! meas refresh
                serviceExecutor::post(E_SERVICE_ID_MEASURE,
                                      servMeasure::MSG_UPDATE_MEAS,
                                      true);

                //! Meas Histogram
                m_pServMeasure->updateHistoMeas();
            }

//            qDebug() << "id.src=" << servMeasure::m_nConfigEngineID
//                     << "id.wfm=" << servMeasure::m_nWfmTraceID
//                     << "id.now=" << servHori::getAttrId();

            updateTime = m_pServMeasure->getUpdateTime();
            if( updateTime == servMeasure::MEAS_SCAN_TIM)
            {
                m_pServMeasure->setFPGA_MeasOnce();
            }

            //QThread::msleep(updateTime);
        }
        else
        {
            QThread::msleep(200);
        }
    }
}
