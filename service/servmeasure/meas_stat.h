#ifndef MEASSTATDATA_H
#define MEASSTATDATA_H

#include "../../include/dsostd.h"
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"
#include "../../service/servmeasure/meas_dsrc.h"
#include "../../service/servmeasure/meas_interface.h"
typedef QList<measDataDSrc*>* MeasDataDSrcList;

class measStatData
{
public:
    measStatData(MeasType type, Chan srcA, Chan srcB);

public:
    void  setType(MeasType type);
    void  setSrcA(Chan s);
    void  setSrcB(Chan s);

    MeasType getType(void)  { return m_Type;}
    Chan     getSrcA(void)  { return m_SrcA;}
    Chan     getSrcB(void)  { return m_SrcB;}

public:
    float     getCurr(void)   { return m_Curr;}
    MeasValue getCurrMeas(void)   { return m_RawCurrVal;}

    float getAvg(void)    { return m_Avg;}
    float getMax(void)    { return m_Max;}
    float getMin(void)    { return m_Min;}
    float getDev(void)    { return m_Dev;}
    ulong getCnt(void)    { return m_Cnt;}
    Unit  getUnit(void)   { return m_Unit;}

    MeasIndicator getAx() { return m_Ax;}
    MeasIndicator getBx() { return m_Bx;}
    MeasIndicator getAy() { return m_Ay;}
    MeasIndicator getBy() { return m_By;}

    MeasStatus getValStatus(void)  { return m_Status;}

    void setTraceId( int id)  { m_TraceId = id;}
    int  getTraceId()         { return m_TraceId;}

    void  updateStatVal(float val, MeasStatus stat);
    void  updateStatVal(MeasValue mval, MeasStatus stat);
    void  resetStatVal(void);
    void  setCntLimit(ulong cntLimit);

    void  setCount(int i=0) { m_Cnt = i;}

private:
    void  setCurr(float val) { m_Curr = val;}
    void  setAvg(float val);
    void  setMax(float val);
    void  setMin(float val);
    void  setDev(float val);
    void  setCnt(void);
private:
    MeasType  m_Type;
    Chan      m_SrcA;
    Chan      m_SrcB;
    float     m_Curr;
    float     m_Avg;
    float     m_Max;
    float     m_Min;
    float     m_Dev;
    float     m_AvgSquare;
    ulong     m_Cnt;
    ulong     m_CntLimit;
    Unit      m_Unit;

    MeasValue m_RawCurrVal;

    MeasIndicator m_Ax;
    MeasIndicator m_Bx;
    MeasIndicator m_Ay;
    MeasIndicator m_By;
    MeasStatus m_Status;

    int m_TraceId;
};

#endif // MEASSTATDATA_H

