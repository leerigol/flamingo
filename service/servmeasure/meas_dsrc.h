#ifndef MEASDATADSRC_H
#define MEASDATADSRC_H

#include <QMap>
#include "../../include/dsotype.h"
#include "meas_interface.h"
#include "meas_soft.h"

using namespace DsoType;
//using namespace meas_algorithm;

class measDataDSrc
{
public:
    measDataDSrc(Chan srcA, Chan srcB, MeasType type, MeasFPGA_DlyIndex index);
    virtual ~measDataDSrc();

public:
    void setMeasSrcA(Chan src) { m_SrcA = src;}
    Chan getMeasSrcA(void)     { return m_SrcA;}
    void setMeasSrcB(Chan src) { m_SrcB = src;}
    Chan getMeasSrcB(void)     { return m_SrcB;}
    MeasType getMeasType(void) { return m_Type;}
    MeasFPGA_DlyIndex getFPGADlyIndex()    { return m_FPGADlyIndex;}


    void CalcRealMeasDly_SOFT(measData *pDataA, measData *pDataB);
    void CalcRealMeasDly_FPGA(void *pFPGA_ret, measData *pDataA, measData *pDataB);
    MeasValue  getRealMeasData(MeasType type);
    MeasStatus getMeasStatus(MeasType type);

private:
    void CalcRawMeasDly_SOFT(measData *pDataA, measData *pDataB);
    void CalcRawMeasDly_FPGA(void *pFPGA_ret, measData *pDataA, measData *pDataB);

    bool dly_xloc1_Valid();
    bool dly_xloc2_Valid();
    void calc_closed_to_Ref( float tInc, unsigned int refLoc);
    void calc_xloc1( float tInc);
    void calc_xloc2( float tInc);

    //! Calc Real MeasValue
    void calc_Delay(MeasType type, measData *pDataA, measData *pDataB);
    void calc_Phase(MeasType type, measData *pDataA, measData *pDataB);

    //! Get Pix position
    int getXPos(int index, measData *pData);
    int getYPos(int yAdc, measData *pData);

private:
    Chan m_SrcA;
    Chan m_SrcB;
    MeasType m_Type;
    MeasStatus m_Status;

    Meas_FPGA_RawData            *m_FPGARawData;
    MeasFPGA_DlyIndex             m_FPGADlyIndex;

    meas_algorithm::MEASURE_DLY m_RawMeasDly;
    QMap<MeasType,MeasValue>    m_RealMeasDly;
};

#endif // MEASDATADSRC_H
