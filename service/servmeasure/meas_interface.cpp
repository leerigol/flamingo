#include <QtMath>
#include "meas_interface.h"
#include "servmeasure.h"

#include "../../engine/base/enginecfg.h"
#include "../../service/servhori/servhori.h"

#define M_VAILD_MIN			0
#define M_VAILD_MAX			255
#define M_VAILD_VPP			13

measData::measData(DsoType::Chan src)
{
    m_Src  = src;

    m_RegionType = REGION_MAIN; //! Default Region: All Wav Data
    m_RangeX1 = 0;
    m_RangeX2 = 0;

    m_RealyInc   = 0.0f;
    m_RealyGnd   = 0.0f;
    m_RealScale  = 0.0f;
    m_RealOffset = 0.0f;
    m_Realt0     = 0.0f;
    m_RealtInc   = 0.0f;
    m_DataLen    = 0;

    m_Wfm = new DsoWfm();
    m_TraceId = 0;
    m_HorInfo = NULL;

    m_FPGARawData = new Meas_FPGA_RawData();
    m_CycleIndex_FPGA = CYCLE_NONE;

    m_RawMeasData.Area   = 0;
    m_RawMeasData.Area_S = 0;
    m_RawMeasData.bVaild_Vmax = false;
    m_RawMeasData.bVaild_Vmin = false;
    m_RawMeasData.bVaild_Vpp  = false;
    m_RawMeasData.FallTime   = 0;
    m_RawMeasData.FallTimeX1 = 0;
    m_RawMeasData.FallTimeX2 = 0;
    m_RawMeasData.FallTimeY1 = 0;
    m_RawMeasData.FallTimeY2 = 0;
    m_RawMeasData.FallX1     = 0;
    m_RawMeasData.FallX2     = 0;
    m_RawMeasData.FEdgeCount = 0;
    m_RawMeasData.FEdgeX1    = 0;
    m_RawMeasData.FEdgeX2    = 0;
    m_RawMeasData.NPulseCount = 0;
    m_RawMeasData.NPulseX1    = 0;
    m_RawMeasData.NPulseX2    = 0;
    m_RawMeasData.Period   = 0;
    m_RawMeasData.PeriodX1 = 0;
    m_RawMeasData.PeriodX2 = 0;
    m_RawMeasData.PeriodY  = 0;
    m_RawMeasData.PPulseCount = 0;
    m_RawMeasData.PPulseX1    = 0;
    m_RawMeasData.PPulseX2    = 0;
    m_RawMeasData.REdgeCount = 0;
    m_RawMeasData.REdgeX1    = 0;
    m_RawMeasData.REdgeX2    = 0;
    m_RawMeasData.RiseTime   = 0;
    m_RawMeasData.RiseTimeX1 = 0;
    m_RawMeasData.RiseTimeX2 = 0;
    m_RawMeasData.RiseTimeY1 = 0;
    m_RawMeasData.RiseTimeY2 = 0;
    m_RawMeasData.RiseX1     = 0;
    m_RawMeasData.RiseX2     = 0;

    m_RawMeasData.Vamp = 0;
    m_RawMeasData.Vavg = 0;
    m_RawMeasData.Vavg_S = 0;
    m_RawMeasData.Vbas = 0;
    m_RawMeasData.Vdev = 0;
    m_RawMeasData.Vmax = 0;
    m_RawMeasData.Vmin = 0;
    m_RawMeasData.Vover = 0;
    m_RawMeasData.VoverY1 = 0;
    m_RawMeasData.VoverY2 = 0;
    m_RawMeasData.Vpp = 0;
    m_RawMeasData.Vpre = 0;
    m_RawMeasData.VpreY1 = 0;
    m_RawMeasData.VpreY2 = 0;
    m_RawMeasData.Vrms = 0;
    m_RawMeasData.Vrms_S = 0;
    m_RawMeasData.Vtop = 0;
    m_RawMeasData.WidthNX1 = 0;
    m_RawMeasData.WidthNX2 = 0;
    m_RawMeasData.WidthPX1 = 0;
    m_RawMeasData.WidthPX2 = 0;
    m_RawMeasData.Width_N = 0;
    m_RawMeasData.Width_P = 0;
    m_RawMeasData.XMax = 0;
    m_RawMeasData.XMin = 0;

    m_RawMeasData.Vupper = 0;
    m_RawMeasData.Vmid = 0;
    m_RawMeasData.Vlower = 0;

    //Read Data for SOFT_Algorithm Test
    m_pTest_Data = NULL;
    m_Test_DataNum = 1000000;  //1M points

    m_SOFT_Algorithm_Test = false;
    if( m_SOFT_Algorithm_Test)
    {
        test_ReadTestData();
    }

    m_ChUnit = Unit_V;
    m_ChOnOff = false;

    m_MeasDataStatus = MeasData_Empty;
}

measData::~measData()
{
    Q_ASSERT( NULL != m_Wfm );
    delete m_Wfm;

    Q_ASSERT( NULL != m_FPGARawData );
    delete m_FPGARawData;

    if( NULL != m_pTest_Data )
    {
        delete []m_pTest_Data;
    }
}

void measData::buildLaWfm()
{
    DsoPoint *pPoint;
    pPoint = new DsoPoint[ m_Wfm->size()*8];

    for( int i=0; i<m_Wfm->size(); i++)
    {
        for( int j=0; j<8; j++)
        {
            //! La point 0: 0
            //!          1: 255
            pPoint[i*8+j] = ((m_Wfm->at(i) >> (7-j))&1) * 255;
        }
    }

    int size = m_Wfm->size()*8;
    int start= m_Wfm->start()*8;
    m_Wfm->setPoint( pPoint, size, size, 0 );
    m_Wfm->setRange( start,  size );

    delete[] pPoint;
}

void measData::getSrcWfmData_SOFT()
{
    dsoVert *pVert = NULL;
    if( isLaCH())
    {
        pVert = dsoVert::getCH( la );
        if( pVert != NULL)
        {
            m_Wfm->init(0);
            if( m_RegionType == REGION_MAIN)
            {
                pVert->getDigi( *m_Wfm, m_Src, horizontal_view_main);
            }
            else
            {
                if( m_ZoomOn)
                {
                    pVert->getDigi( *m_Wfm, m_Src, horizontal_view_zoom);
                }
                else
                {
                    pVert->getDigi( *m_Wfm, m_Src, horizontal_view_main);
                }
            }

            if( m_Wfm->size() > 0 )
            {
                buildLaWfm();
                //m_Wfm->expandLa();
                m_RealtInc   = m_Wfm->gettInc().toFloat();
                m_Realt0     = m_Wfm->gett0().toFloat() + m_Wfm->start()*m_RealtInc;
                m_DataLen    = m_Wfm->size();
                m_ChUnit     = pVert->getUnit();
                m_ChOnOff    = pVert->getOnOff();
            }
        }
    }
    else
    {        
        pVert = dsoVert::getCH( m_Src );
        if( pVert != NULL)
        {
            if( m_RegionType == REGION_MAIN)
            {
                pVert->getTrace(*m_Wfm, chan_none, horizontal_view_main);
            }
            else
            {
                if( m_ZoomOn)
                {
                    pVert->getTrace(*m_Wfm, chan_none, horizontal_view_zoom);
                }
                else
                {
                    pVert->getTrace(*m_Wfm, chan_none, horizontal_view_main);
                }
            }

            float base;
            m_Wfm->getyRefBase().toReal(base);
            m_RealyInc   = m_Wfm->realyInc();
            m_RealyGnd   = m_Wfm->getyGnd();
            m_RealScale  = m_Wfm->getyRefScale()*base;
            m_RealOffset = m_Wfm->getyRefOffset()*base;

            m_RealtInc   = m_Wfm->gettInc().toFloat();
            m_Realt0     = m_Wfm->gett0().toFloat() + m_Wfm->start()*m_RealtInc;;
            m_DataLen    = m_Wfm->size();
            m_ChUnit     = pVert->getUnit();
            m_ChOnOff    = pVert->getOnOff();

        }
    }
    LOG_DBG()<<"m_Src"<<m_Src;
    LOG_DBG()<< "m_RealtInc: "<<m_RealtInc;
    LOG_DBG()<<"m_DataLen"<<m_DataLen;
    LOG_DBG()<<"m_RealyInc"<<m_RealyInc;
    LOG_DBG()<<"m_RealyGnd"<<m_RealyGnd;
}

void measData::CalcRawMeasData_SOFT(ThresholdData *th,
                               meas_algorithm::StateMethodMode stateMode,
                               meas_algorithm::StateMethod topMethod,
                               meas_algorithm::StateMethod baseMethod)
{
    unsigned char *pSrcData = NULL;
    int s32Len = 0;
    int s32Base = 0;

    getSrcWfmData_SOFT();

    if( m_RegionType == REGION_MAIN)  //! Meas Region(Main): ALL Trace Data
    {
        pSrcData = m_Wfm->getPoint() + m_Wfm->start();
        s32Len   = m_DataLen;
    }
    else if( m_RegionType == REGION_ZOOM)  //! Meas Region(Zoom): Trace Data in Zoom region
    {
        pSrcData = m_Wfm->getPoint()+ m_Wfm->start();
        s32Len   = m_DataLen;
    }
    else if( m_RegionType == REGION_CURSOR) //! Meas Region(Cursor): Trace Data between CursorA&B
    {
        int tracex1 = 0;
        int tracex2 = 0;

        if( m_ZoomOn)
        {
            tracex1 = getTraceX(m_RangeX1, horizontal_view_zoom);
            tracex2 = getTraceX(m_RangeX2, horizontal_view_zoom);
        }
        else
        {
            tracex1 = getTraceX(m_RangeX1, horizontal_view_main);
            tracex2 = getTraceX(m_RangeX2, horizontal_view_main);
        }

        s32Len = tracex2 - tracex1 + 1;

        pSrcData = (m_Wfm->getPoint()) + tracex1 + (m_Wfm->start());
    }
    else     //! Meas Region: Default All Trace Data
    {
        pSrcData = m_Wfm->getPoint() + m_Wfm->start();
        s32Len   = m_DataLen;
    }
    s32Base  = s32Len/2;


    if( m_SOFT_Algorithm_Test)
    {
        pSrcData = m_pTest_Data;
        s32Len  = m_Test_DataNum;
        s32Base = s32Len/2;
    }

    servMeasure::m_nWfmTraceID = m_Wfm->getAttrId();
    if(  (pSrcData != 0) &&  m_ChOnOff )
    {
        if(s32Len)
        {
            m_MeasDataStatus = MeasData_Valid;
            meas_algorithm::MeasThreshold measTh;
            if( isLaCH())
            {
                measTh.eThreshHoldType = meas_algorithm::TH_TYPE_PER;
                measTh.f32ThreshHoldH = 0.9;  //! La: default ThreshHoldH 90%
                measTh.f32ThreshHoldM = 0.5;  //! La: default ThreshHoldM 50%
                measTh.f32ThreshHoldL = 0.1;  //! La: default ThreshHoldL 10%
            }
            else
            {
                Q_ASSERT( NULL != th );
                measTh.eThreshHoldType = th->mtype;
                measTh.f32ThreshHoldH = th->mfHigh_SOFT;
                measTh.f32ThreshHoldM = th->mfMid_SOFT;
                measTh.f32ThreshHoldL = th->mfLow_SOFT;
            }

            meas_algorithm::SetStateMethod(stateMode, topMethod, baseMethod);
            meas_algorithm::SetMeasAnalyseSaRate(1/m_Wfm->gettInc().toDouble());

            meas_algorithm::MeasAnalyser(pSrcData,
                                         s32Len,
                                         s32Base,
                                         m_RealyGnd,
                                         measTh,
                                         &m_RawMeasData);
        }
        else
        {
            m_MeasDataStatus = MeasData_Empty;
            m_RawMeasData.bVaild_Vpp = false;
        }
    }
    else if( !m_ChOnOff)
    {
        m_MeasDataStatus = MeasData_Empty;
        m_RawMeasData.bVaild_Vpp = false;
    }
    else
    {
        m_MeasDataStatus = MeasData_Empty;
        m_RawMeasData.bVaild_Vpp = false;
    }
}


void measData::CalcRealMeasData_SOFT(ThresholdData *th,
                                meas_algorithm::StateMethodMode sMode,
                                meas_algorithm::StateMethod top,
                                meas_algorithm::StateMethod base )
{

    //! wave token
    CalcRawMeasData_SOFT(th, sMode, top, base);

    //! Horizontal Measure
    calc_Period();
    calc_Freq();
    calc_RiseTime();
    calc_FallTime();
    calc_PWidth();
    calc_NWidth();
    calc_PDuty();
    calc_NDuty();
    calc_PPulses();
    calc_NPulses();
    calc_PEdges();
    calc_NEdges();
    calc_Tvmax();
    calc_Tvmin();
    calc_Pslew_rate();
    calc_Nslew_rate();

    //! Vertical Measure
    calc_Vmax();
    calc_Vmin();
    calc_Vpp();
    calc_Vtop();
    calc_Vbase();
    calc_Vamp();
    calc_Vupper();
    calc_Vmid();
    calc_Vlower();
    calc_Vavg();
    calc_Vrms();
    calc_Vrms_S();
    calc_Overshoot();
    calc_Preshoot();
    calc_Area();
    calc_Area_S();
    calc_Variance();

}

void measData::clearData()
{
    m_MeasDataStatus = MeasData_Empty;
    m_RawMeasData.bVaild_Vpp = false;

    //! Horizontal Measure
    calc_Period();
    calc_Freq();
    calc_RiseTime();
    calc_FallTime();
    calc_PWidth();
    calc_NWidth();
    calc_PDuty();
    calc_NDuty();
    calc_PPulses();
    calc_NPulses();
    calc_PEdges();
    calc_NEdges();
    calc_Tvmax();
    calc_Tvmin();
    calc_Pslew_rate();
    calc_Nslew_rate();

    //! Vertical Measure
    calc_Vmax();
    calc_Vmin();
    calc_Vpp();
    calc_Vtop();
    calc_Vbase();
    calc_Vamp();
    calc_Vupper();
    calc_Vmid();
    calc_Vlower();
    calc_Vavg();
    calc_Vrms();
    calc_Vrms_S();
    calc_Overshoot();
    calc_Preshoot();
    calc_Area();
    calc_Area_S();
    calc_Variance();
}

void measData::PulseMeas_FPGA(float tInc, unsigned int refLoc)
{
    bool cycle_x1_R_Valid = false;
    bool cycle_x1_F_Valid = false;
    if( (m_FPGARawData->cycle_x1_valid==1)&&(m_FPGARawData->cycle_x1_edge_type==1) )
        cycle_x1_R_Valid = true;
    else
        cycle_x1_R_Valid = false;

    if( (m_FPGARawData->cycle_x1_valid==1)&&(m_FPGARawData->cycle_x1_edge_type==0) )
        cycle_x1_F_Valid = true;
    else
        cycle_x1_F_Valid = false;

    bool cycle_x2_R_Valid = false;
    bool cycle_x2_F_Valid = false;
    if( (m_FPGARawData->cycle_x2_valid==1)&&(m_FPGARawData->cycle_x2_edge_type==1) )
        cycle_x2_R_Valid = true;
    else
        cycle_x2_R_Valid = false;

    if( (m_FPGARawData->cycle_x2_valid==1)&&(m_FPGARawData->cycle_x2_edge_type==0) )
        cycle_x2_F_Valid = true;
    else
        cycle_x2_F_Valid = false;

    bool cycle_x3_R_Valid = false;
    bool cycle_x3_F_Valid = false;
    if( (m_FPGARawData->cycle_x3_valid==1)&&(m_FPGARawData->cycle_x3_edge_type==1) )
        cycle_x3_R_Valid = true;
    else
        cycle_x3_R_Valid = false;

    if( (m_FPGARawData->cycle_x3_valid==1)&&(m_FPGARawData->cycle_x3_edge_type==0) )
        cycle_x3_F_Valid = true;
    else
        cycle_x3_F_Valid = false;

    bool cycle_x4_R_Valid = false;
    bool cycle_x4_F_Valid = false;
    if( (m_FPGARawData->cycle_x4_valid==1)&&(m_FPGARawData->cycle_x4_edge_type==1) )
        cycle_x4_R_Valid = true;
    else
        cycle_x4_R_Valid = false;

    if( (m_FPGARawData->cycle_x4_valid==1)&&(m_FPGARawData->cycle_x4_edge_type==0) )
        cycle_x4_F_Valid = true;
    else
        cycle_x4_F_Valid = false;

    LOG_DBG()<<"Period_1_2_3_4(F):    "<<m_FPGARawData->cycle_x1<<"  "<<m_FPGARawData->cycle_x2<<"  "
                                       <<m_FPGARawData->cycle_x3<<"  "<<m_FPGARawData->cycle_x4;
    LOG_DBG()<<"Period_Edge_Type(F):  "<<m_FPGARawData->cycle_x1_edge_type<<"   "<<m_FPGARawData->cycle_x2_edge_type<<"   "
                                       <<m_FPGARawData->cycle_x3_edge_type<<"   "<<m_FPGARawData->cycle_x4_edge_type;
    LOG_DBG()<<"Period_Valid(F):      "<<m_FPGARawData->cycle_x1_valid<<"   "<<m_FPGARawData->cycle_x2_valid<<"   "
                                       <<m_FPGARawData->cycle_x3_valid<<"   "<<m_FPGARawData->cycle_x4_valid;
    /************************************ Period ********************************/
    unsigned int cycle_A = 0;
    unsigned int cycle_B = 0;
    if( (m_FPGARawData->cycle_x1_valid == 1)&&(m_FPGARawData->cycle_x3_valid == 1) )
    {
        cycle_A = m_FPGARawData->cycle_x3 - m_FPGARawData->cycle_x1;
    }
    else
    {
        cycle_A = 0;
    }

    if( (m_FPGARawData->cycle_x2_valid == 1)&&(m_FPGARawData->cycle_x4_valid == 1) )
    {
        cycle_B = m_FPGARawData->cycle_x4 - m_FPGARawData->cycle_x2;
    }
    else
    {
        cycle_B = 0;
    }

    if( (cycle_A != 0)&&(cycle_B != 0) )
    {
        if( ((int)refLoc-(int)(m_FPGARawData->cycle_x2))<=((int)(m_FPGARawData->cycle_x3)-(int)refLoc) )
        {
            m_RawMeasData.PeriodX1 = m_FPGARawData->cycle_x1;
            m_RawMeasData.PeriodX2 = m_FPGARawData->cycle_x3;
            m_RawMeasData.Period = cycle_A*tInc;
            m_CycleIndex_FPGA = CYCLE_1;
        }
        else
        {
            m_RawMeasData.PeriodX1 = m_FPGARawData->cycle_x2;
            m_RawMeasData.PeriodX2 = m_FPGARawData->cycle_x4;
            m_RawMeasData.Period = cycle_B*tInc;
            m_CycleIndex_FPGA = CYCLE_2;
        }
    }
    else if( cycle_A != 0)
    {
        m_RawMeasData.PeriodX1 = m_FPGARawData->cycle_x1;
        m_RawMeasData.PeriodX2 = m_FPGARawData->cycle_x3;
        m_RawMeasData.Period = cycle_A*tInc;
        m_CycleIndex_FPGA = CYCLE_1;
    }
    else if( cycle_B != 0)
    {
        m_RawMeasData.PeriodX1 = m_FPGARawData->cycle_x2;
        m_RawMeasData.PeriodX2 = m_FPGARawData->cycle_x4;
        m_RawMeasData.Period = cycle_B*tInc;
        m_CycleIndex_FPGA = CYCLE_2;
    }
    else
    {
        m_RawMeasData.PeriodX1 = 0;
        m_RawMeasData.PeriodX2 = 0;
        m_RawMeasData.Period = 0;
        m_CycleIndex_FPGA = CYCLE_NONE;
    }
    m_RawMeasData.PeriodY = m_FPGARawData->vmid;

    /************************************ +Width ********************************/
    unsigned int Width_P1 = 0;
    unsigned int Width_P2 = 0;
    if(  (cycle_x1_R_Valid && cycle_x2_F_Valid) &&
         (cycle_x3_R_Valid && cycle_x4_F_Valid) )
    {
        Width_P1 = m_FPGARawData->cycle_x2 - m_FPGARawData->cycle_x1;
        Width_P2 = m_FPGARawData->cycle_x4 - m_FPGARawData->cycle_x3;
        if( ((int)refLoc-(int)(m_FPGARawData->cycle_x2))<=((int)(m_FPGARawData->cycle_x3)-(int)refLoc) )
        {
            m_RawMeasData.WidthPX1 = m_FPGARawData->cycle_x1;
            m_RawMeasData.WidthPX2 = m_FPGARawData->cycle_x2;
            m_RawMeasData.Width_P = Width_P1*tInc;
        }
        else
        {
            m_RawMeasData.WidthPX1 = m_FPGARawData->cycle_x3;
            m_RawMeasData.WidthPX2 = m_FPGARawData->cycle_x4;
            m_RawMeasData.Width_P = Width_P2*tInc;
        }
    }
    else if( cycle_x1_R_Valid && cycle_x2_F_Valid)
    {
        Width_P1 = m_FPGARawData->cycle_x2 - m_FPGARawData->cycle_x1;
        m_RawMeasData.WidthPX1 = m_FPGARawData->cycle_x1;
        m_RawMeasData.WidthPX2 = m_FPGARawData->cycle_x2;
        m_RawMeasData.Width_P = Width_P1*tInc;
    }
    else if( cycle_x2_R_Valid && cycle_x3_F_Valid )
    {
        Width_P1 = m_FPGARawData->cycle_x3 - m_FPGARawData->cycle_x2;
        m_RawMeasData.WidthPX1 = m_FPGARawData->cycle_x2;
        m_RawMeasData.WidthPX2 = m_FPGARawData->cycle_x3;
        m_RawMeasData.Width_P = Width_P1*tInc;
    }
    else if( cycle_x3_R_Valid && cycle_x4_F_Valid )
    {
        Width_P1 = m_FPGARawData->cycle_x4 - m_FPGARawData->cycle_x3;
        m_RawMeasData.WidthPX1 = m_FPGARawData->cycle_x3;
        m_RawMeasData.WidthPX2 = m_FPGARawData->cycle_x4;
        m_RawMeasData.Width_P = Width_P1*tInc;
    }
    else
    {
        m_RawMeasData.WidthPX1 = 0;
        m_RawMeasData.WidthPX2 = 0;
        m_RawMeasData.Width_P = 0;
    }

    /************************************ -Width ********************************/
    unsigned int Width_N1 = 0;
    unsigned int Width_N2 = 0;
    if(  (cycle_x1_F_Valid && cycle_x2_R_Valid) &&
         (cycle_x3_F_Valid && cycle_x4_R_Valid) )
    {
        Width_N1 = m_FPGARawData->cycle_x2 - m_FPGARawData->cycle_x1;
        Width_N2 = m_FPGARawData->cycle_x4 - m_FPGARawData->cycle_x3;
        if( ((int)refLoc-(int)(m_FPGARawData->cycle_x2))<=((int)(m_FPGARawData->cycle_x3)-(int)refLoc) )
        {
            m_RawMeasData.WidthNX1 = m_FPGARawData->cycle_x1;
            m_RawMeasData.WidthNX2 = m_FPGARawData->cycle_x2;
            m_RawMeasData.Width_N = Width_N1*tInc;
        }
        else
        {
            m_RawMeasData.WidthNX1 = m_FPGARawData->cycle_x3;
            m_RawMeasData.WidthNX2 = m_FPGARawData->cycle_x4;
            m_RawMeasData.Width_N = Width_N2*tInc;
        }
    }
    else if( cycle_x1_F_Valid && cycle_x2_R_Valid)
    {
        Width_N1 = m_FPGARawData->cycle_x2 - m_FPGARawData->cycle_x1;
        m_RawMeasData.WidthNX1 = m_FPGARawData->cycle_x1;
        m_RawMeasData.WidthNX2 = m_FPGARawData->cycle_x2;
        m_RawMeasData.Width_N = Width_N1*tInc;
    }
    else if( cycle_x2_F_Valid && cycle_x3_R_Valid )
    {
        Width_N1 = m_FPGARawData->cycle_x3 - m_FPGARawData->cycle_x2;
        m_RawMeasData.WidthNX1 = m_FPGARawData->cycle_x2;
        m_RawMeasData.WidthNX2 = m_FPGARawData->cycle_x3;
        m_RawMeasData.Width_N = Width_N1*tInc;
    }
    else if( cycle_x3_F_Valid && cycle_x4_R_Valid )
    {
        Width_N1 = m_FPGARawData->cycle_x4 - m_FPGARawData->cycle_x3;
        m_RawMeasData.WidthNX1 = m_FPGARawData->cycle_x3;
        m_RawMeasData.WidthNX2 = m_FPGARawData->cycle_x4;
        m_RawMeasData.Width_N = Width_N1*tInc;
    }
    else
    {
        m_RawMeasData.WidthNX1 = 0;
        m_RawMeasData.WidthNX2 = 0;
        m_RawMeasData.Width_N = 0;
    }

}

void measData::EdgeMeas_FPGA(float tInc, unsigned int refLoc, float yGnd)
{
    unsigned int rtimeA = m_FPGARawData->rtimeA;
    unsigned int rtimeA_valid = m_FPGARawData->rtimeA_valid;
    unsigned int rtimeA_x2 = m_FPGARawData->rtimeA_x2;
    unsigned int rtimeA_x1 = rtimeA_x2 - rtimeA;

    unsigned int rtimeB = m_FPGARawData->rtimeB;
    unsigned int rtimeB_valid = m_FPGARawData->rtimeB_valid;
    unsigned int rtimeB_x2 = m_FPGARawData->rtimeB_x2;
    unsigned int rtimeB_x1 = rtimeB_x2 - rtimeB;

    m_RawMeasData.RiseTimeY1 = (int)(m_FPGARawData->vlower) - (int)yGnd;
    m_RawMeasData.RiseTimeY2 = (int)(m_FPGARawData->vupper) - (int)yGnd;

    if( (rtimeA_valid==1)&&(rtimeB_valid==1) )
    {
        if( qAbs((int)refLoc-(int)rtimeA_x2)<=qAbs((int)rtimeB_x1-(int)refLoc) )
        {
            m_RawMeasData.RiseTime = rtimeA*tInc;
            m_RawMeasData.RiseTimeX1 = rtimeA_x1;
            m_RawMeasData.RiseTimeX2 = rtimeA_x2;
        }
        else
        {
            m_RawMeasData.RiseTime = rtimeB*tInc;
            m_RawMeasData.RiseTimeX1 = rtimeB_x1;
            m_RawMeasData.RiseTimeX2 = rtimeB_x2;
        }
    }
    else if( rtimeA_valid==1)
    {
        m_RawMeasData.RiseTime = rtimeA*tInc;
        m_RawMeasData.RiseTimeX1 = rtimeA_x1;
        m_RawMeasData.RiseTimeX2 = rtimeA_x2;
    }
    else if( rtimeB_valid==1)
    {
        m_RawMeasData.RiseTime = rtimeB*tInc;
        m_RawMeasData.RiseTimeX1 = rtimeB_x1;
        m_RawMeasData.RiseTimeX2 = rtimeB_x2;
    }
    else
    {
        m_RawMeasData.RiseTime = 0;
        m_RawMeasData.RiseTimeX1 = 0;
        m_RawMeasData.RiseTimeX2 = 0;
    }

    unsigned int ftimeA = m_FPGARawData->ftimeA;
    unsigned int ftimeA_valid = m_FPGARawData->ftimeA_valid;
    unsigned int ftimeA_x2 = m_FPGARawData->ftimeA_x2;
    unsigned int ftimeA_x1 = ftimeA_x2 - ftimeA;

    unsigned int ftimeB = m_FPGARawData->ftimeB;
    unsigned int ftimeB_valid = m_FPGARawData->ftimeB_valid;
    unsigned int ftimeB_x2 = m_FPGARawData->ftimeB_x2;
    unsigned int ftimeB_x1 = ftimeB_x2 - ftimeB;

    m_RawMeasData.FallTimeY1 = (int)(m_FPGARawData->vupper) - (int)yGnd;
    m_RawMeasData.FallTimeY2 = (int)(m_FPGARawData->vlower) - (int)yGnd;

    if( (ftimeA_valid==1)&&(ftimeB_valid==1) )
    {
        if( qAbs((int)refLoc-(int)ftimeA_x2)<=qAbs((int)ftimeB_x1-(int)refLoc) )
        {
            m_RawMeasData.FallTime = ftimeA*tInc;
            m_RawMeasData.FallTimeX1 = ftimeA_x1;
            m_RawMeasData.FallTimeX2 = ftimeA_x2;
        }
        else
        {
            m_RawMeasData.FallTime = ftimeB*tInc;
            m_RawMeasData.FallTimeX1 = ftimeB_x1;
            m_RawMeasData.FallTimeX2 = ftimeB_x2;
        }
    }
    else if( ftimeA_valid==1)
    {
        m_RawMeasData.FallTime = ftimeA*tInc;
        m_RawMeasData.FallTimeX1 = ftimeA_x1;
        m_RawMeasData.FallTimeX2 = ftimeA_x2;
    }
    else if( ftimeB_valid==1)
    {
        m_RawMeasData.FallTime = ftimeB*tInc;
        m_RawMeasData.FallTimeX1 = ftimeB_x1;
        m_RawMeasData.FallTimeX2 = ftimeB_x2;
    }
    else
    {
        m_RawMeasData.FallTime = 0;
        m_RawMeasData.FallTimeX1 = 0;
        m_RawMeasData.FallTimeX2 = 0;
    }

}

void measData::CountMeas_FPGA()
{
#if 1
    unsigned int rf_num = m_FPGARawData->rf_num;
    unsigned int rf_num_last_slope = m_FPGARawData->rf_num_last_slope;
    bool rf_num_last_rise = false;
    if( rf_num_last_slope == 1)   // 1: Rise edge
        rf_num_last_rise = true;
    else                          // 0: Fall edge
        rf_num_last_rise = false;

    if( m_FPGARawData->rf_num_valid == 1 )
    {
        if( rf_num_last_rise)  //! last rise edge
        {
            m_RawMeasData.REdgeCount = qCeil(rf_num/2.0);
            m_RawMeasData.FEdgeCount = qFloor(rf_num/2.0);
            m_RawMeasData.PPulseCount = m_RawMeasData.REdgeCount - 1;
            m_RawMeasData.NPulseCount = m_RawMeasData.FEdgeCount;

            if( rf_num%2 == 0)  //! even edge num
            {
                //! Rise Edge
                if( m_FPGARawData->edge_cnt_start2_valid == 1)
                    m_RawMeasData.REdgeX1 = m_FPGARawData->edge_cnt_start2_xloc;
                else
                    m_RawMeasData.REdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end2_valid == 1)
                    m_RawMeasData.REdgeX2 = m_FPGARawData->edge_cnt_end2_xloc;
                else
                    m_RawMeasData.REdgeX2 = 0;

                //! Fall Edge
                if( m_FPGARawData->edge_cnt_start_valid == 1)
                    m_RawMeasData.FEdgeX1 = m_FPGARawData->edge_cnt_start_xloc;
                else
                    m_RawMeasData.FEdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end_valid == 1)
                    m_RawMeasData.FEdgeX2 = m_FPGARawData->edge_cnt_end_xloc;
                else
                    m_RawMeasData.FEdgeX2 = 0;
            }
            else                //! odd edge num
            {
                //! Rise Edge
                if( m_FPGARawData->edge_cnt_start_valid == 1)
                    m_RawMeasData.REdgeX1 = m_FPGARawData->edge_cnt_start_xloc;
                else
                    m_RawMeasData.REdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end2_valid == 1)
                    m_RawMeasData.REdgeX2 = m_FPGARawData->edge_cnt_end2_xloc;
                else
                    m_RawMeasData.REdgeX2 = 0;

                //! Fall Edge
                if( m_FPGARawData->edge_cnt_start2_valid == 1)
                    m_RawMeasData.FEdgeX1 = m_FPGARawData->edge_cnt_start2_xloc;
                else
                    m_RawMeasData.FEdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end_valid == 1)
                    m_RawMeasData.FEdgeX2 = m_FPGARawData->edge_cnt_end_xloc;
                else
                    m_RawMeasData.FEdgeX2 = 0;
            }
        }
        else   //! last fall edge
        {
            m_RawMeasData.REdgeCount = qFloor(rf_num/2.0);
            m_RawMeasData.FEdgeCount = qCeil(rf_num/2.0);
            m_RawMeasData.PPulseCount = m_RawMeasData.REdgeCount;
            m_RawMeasData.NPulseCount = m_RawMeasData.FEdgeCount - 1;

            if( rf_num%2 == 0)  //! even edge num
            {
                //! Rise Edge
                if( m_FPGARawData->edge_cnt_start_valid == 1)
                    m_RawMeasData.REdgeX1 = m_FPGARawData->edge_cnt_start_xloc;
                else
                    m_RawMeasData.REdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end_valid == 1)
                    m_RawMeasData.REdgeX2 = m_FPGARawData->edge_cnt_end_xloc;
                else
                    m_RawMeasData.REdgeX2 = 0;

                //! Fall Edge
                if( m_FPGARawData->edge_cnt_start2_valid == 1)
                    m_RawMeasData.FEdgeX1 = m_FPGARawData->edge_cnt_start2_xloc;
                else
                    m_RawMeasData.FEdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end2_valid == 1)
                    m_RawMeasData.FEdgeX2 = m_FPGARawData->edge_cnt_end2_xloc;
                else
                    m_RawMeasData.FEdgeX2 = 0;
            }
            else            //! odd edge num
            {
                //! Rise Edge
                if( m_FPGARawData->edge_cnt_start2_valid == 1)
                    m_RawMeasData.REdgeX1 = m_FPGARawData->edge_cnt_start2_xloc;
                else
                    m_RawMeasData.REdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end_valid == 1)
                    m_RawMeasData.REdgeX2 = m_FPGARawData->edge_cnt_end_xloc;
                else
                    m_RawMeasData.REdgeX2 = 0;

                //! Fall Edge
                if( m_FPGARawData->edge_cnt_start_valid == 1)
                    m_RawMeasData.FEdgeX1 = m_FPGARawData->edge_cnt_start_xloc;
                else
                    m_RawMeasData.FEdgeX1 = 0;

                if( m_FPGARawData->edge_cnt_end2_valid == 1)
                    m_RawMeasData.FEdgeX2 = m_FPGARawData->edge_cnt_end2_xloc;
                else
                    m_RawMeasData.FEdgeX2 = 0;
            }
        }

        //! Pos.Pulse
        if( m_RawMeasData.PPulseCount != 0)
        {
            m_RawMeasData.PPulseX1 = m_RawMeasData.REdgeX1;
            m_RawMeasData.PPulseX2 = m_RawMeasData.FEdgeX2;
        }
        else
        {
            m_RawMeasData.PPulseX1 = 0;
            m_RawMeasData.PPulseX2 = 0;
        }

        //! Neg.Pulse
        if( m_RawMeasData.NPulseCount != 0)
        {
            m_RawMeasData.NPulseX1 = m_RawMeasData.FEdgeX1;
            m_RawMeasData.NPulseX2 = m_RawMeasData.REdgeX2;
        }
        else
        {
            m_RawMeasData.NPulseX1 = 0;
            m_RawMeasData.NPulseX2 = 0;
        }
    }
    else
    {
        m_RawMeasData.REdgeCount = 0;
        m_RawMeasData.REdgeX1 = 0;
        m_RawMeasData.REdgeX2 = 0;

        m_RawMeasData.FEdgeCount = 0;
        m_RawMeasData.FEdgeX1 = 0;
        m_RawMeasData.FEdgeX2 = 0;

        m_RawMeasData.PPulseCount = 0;
        m_RawMeasData.PPulseX1 = 0;
        m_RawMeasData.PPulseX2 = 0;

        m_RawMeasData.NPulseCount = 0;
        m_RawMeasData.NPulseX1 = 0;
        m_RawMeasData.NPulseX2 = 0;
    }

#endif
}

void measData::AmpMeas_FPGA( float yGnd, unsigned int dataLen)
{
    m_RawMeasData.Vmax = (int)(m_FPGARawData->vmax)-(int)yGnd;
//    LOG_DBG() << "FPGARawData->vmax = " << m_FPGARawData->vmax;
//    LOG_DBG() << "yGnd = " << yGnd;
//    LOG_DBG() << "m_RawMeasData.Vmax = " << m_RawMeasData.Vmax;

    m_RawMeasData.XMax = m_FPGARawData->xmax;
    if( m_RawMeasData.Vmax == M_VAILD_MAX)
    {
        m_RawMeasData.bVaild_Vmax = false;
    }
    else
    {
        m_RawMeasData.bVaild_Vmax = true;
    }

    m_RawMeasData.Vmin = (int)(m_FPGARawData->vmin)-(int)yGnd;
    m_RawMeasData.XMin = m_FPGARawData->xmin;
    if( m_RawMeasData.Vmin == M_VAILD_MIN)
    {
        m_RawMeasData.bVaild_Vmin = false;
    }
    else
    {
        m_RawMeasData.bVaild_Vmin = true;
    }

    m_RawMeasData.Vpp  = m_RawMeasData.Vmax - m_RawMeasData.Vmin;
    if( m_RawMeasData.Vpp < M_VAILD_VPP)
    {
        m_RawMeasData.bVaild_Vpp = false;
    }
    else
    {
        m_RawMeasData.bVaild_Vpp = true;
    }

    m_RawMeasData.Vtop = (int)(m_FPGARawData->vtop)-(int)yGnd;
    m_RawMeasData.Vbas = (int)(m_FPGARawData->vbase)-(int)yGnd;
    m_RawMeasData.Vamp = m_RawMeasData.Vtop - m_RawMeasData.Vbas;

    m_RawMeasData.Vupper = (int)(m_FPGARawData->vupper)-(int)yGnd;
    m_RawMeasData.Vmid   = (int)(m_FPGARawData->vmid)-(int)yGnd;
    m_RawMeasData.Vlower = (int)(m_FPGARawData->vlower)-(int)yGnd;

    /************************************ avg Area********************************/
    qlonglong ave_sum_bit31_0  = (qlonglong)(m_FPGARawData->ave_sum_bit31_0);
    qlonglong ave_sum_bit39_32 = ((qlonglong)(m_FPGARawData->ave_sum_bit39_32))<<32;
    float ave_sum = ave_sum_bit39_32 + ave_sum_bit31_0;

    m_RawMeasData.Vavg = ave_sum/dataLen - (int)yGnd;
    m_RawMeasData.Area = ave_sum - (int)yGnd*dataLen;

    /************************************ Single avg Area********************************/
    qlonglong ave_sum_1_bit31_0  = (qlonglong)(m_FPGARawData->ave_sum_1_bit31_0);
    qlonglong ave_sum_1_bit39_32 = ((qlonglong)(m_FPGARawData->ave_sum_1_bit39_32))<<32;
    float ave_sum_1 = ave_sum_1_bit39_32 + ave_sum_1_bit31_0;

    qlonglong ave_sum_2_bit31_0  = (qlonglong)(m_FPGARawData->ave_sum_2_bit31_0);
    qlonglong ave_sum_2_bit39_32 = ((qlonglong)(m_FPGARawData->ave_sum_2_bit39_32))<<32;
    float ave_sum_2 = ave_sum_2_bit39_32 + ave_sum_2_bit31_0;

    unsigned int avg_len = m_RawMeasData.PeriodX2 - m_RawMeasData.PeriodX1;

    if( m_CycleIndex_FPGA == CYCLE_1)
    {
        m_RawMeasData.Vavg_S = ave_sum_1/avg_len - (int)yGnd;
        m_RawMeasData.Area_S = ave_sum_1 - (int)yGnd*avg_len;
    }
    else if( m_CycleIndex_FPGA == CYCLE_2)
    {
        m_RawMeasData.Vavg_S = ave_sum_2/avg_len-(int)yGnd;
        m_RawMeasData.Area_S = ave_sum_2 - (int)yGnd*avg_len;
    }
    else
    {
        m_RawMeasData.Vavg_S = 0;
        m_RawMeasData.Area_S = 0;
    }

    /************************************ Vrms Vdev**************************************/
    qlonglong vrms_sum_bit31_0  = (qlonglong)(m_FPGARawData->vrms_sum_bit31_0);
    qlonglong vrms_sum_bit47_32 = ((qlonglong)(m_FPGARawData->vrms_sum_bit47_32))<<32;
    float vrms_sum = vrms_sum_bit31_0 + vrms_sum_bit47_32;
    m_RawMeasData.Vrms = qSqrt(vrms_sum/dataLen - 2*ave_sum*yGnd/dataLen + yGnd*yGnd);

    m_RawMeasData.Vdev = qSqrt( vrms_sum/dataLen - (ave_sum/dataLen)*(ave_sum/dataLen));
//    m_RawMeasData.Vdev = vrms_sum/dataLen - (ave_sum/dataLen)*(ave_sum/dataLen);

    /************************************ Single Vrms ********************************/
    qlonglong vrms_sum_1_bit31_0 =  (qlonglong)(m_FPGARawData->vrms_sum_1_bit31_0);
    qlonglong vrms_sum_1_bit47_32 = ((qlonglong)(m_FPGARawData->vrms_sum_1_bit47_32))<<32;
    float vrms_sum_1 = vrms_sum_1_bit31_0 + vrms_sum_1_bit47_32;

    qlonglong vrms_sum_2_bit31_0 =  (qlonglong)(m_FPGARawData->vrms_sum_2_bit31_0);
    qlonglong vrms_sum_2_bit47_32 = ((qlonglong)(m_FPGARawData->vrms_sum_2_bit47_32))<<32;
    float vrms_sum_2 = vrms_sum_2_bit31_0 + vrms_sum_2_bit47_32;

    unsigned int vrms_len = m_RawMeasData.PeriodX2-m_RawMeasData.PeriodX1;

    if( m_CycleIndex_FPGA == CYCLE_1)
    {
        m_RawMeasData.Vrms_S = qSqrt(vrms_sum_1/vrms_len - 2*ave_sum_1*yGnd/vrms_len + yGnd*yGnd);
    }
    else if( m_CycleIndex_FPGA == CYCLE_2)
    {
        m_RawMeasData.Vrms_S = qSqrt(vrms_sum_2/vrms_len - 2*ave_sum_2*yGnd/vrms_len + yGnd*yGnd);
    }
    else
    {
        m_RawMeasData.Vrms_S = 0;
    }

    unsigned int overshoot = m_FPGARawData->overshoot;
    unsigned int preshoot  = m_FPGARawData->preshoot;
    m_RawMeasData.Vover = qFabs(overshoot/(m_RawMeasData.Vamp));
    m_RawMeasData.VoverY2 = m_RawMeasData.Vtop;
    m_RawMeasData.VoverY1 = m_RawMeasData.VoverY2 + overshoot;

    m_RawMeasData.Vpre  = qFabs(preshoot/(m_RawMeasData.Vamp));
    m_RawMeasData.VpreY1 = m_RawMeasData.Vbas;
    m_RawMeasData.VpreY2 = m_RawMeasData.VpreY1 - preshoot;

}

void measData::CalcRawMeasData_FPGA(void *pFPGA_ret, void *adcCH, float yGnd)
{
    if( (pFPGA_ret != NULL)&&(adcCH != NULL) )
    {
        if( m_ChOnOff)
        {
            m_MeasDataStatus = MeasData_Valid;
        }
        else
        {
            m_MeasDataStatus = MeasData_Empty;
        }

        EngineAdcCoreCH *pAdcCH = (EngineAdcCoreCH *)adcCH;
        EngineMeasRet *pRet = (EngineMeasRet *)pFPGA_ret;

        if( isLaCH())
        {
            m_Realt0   = pRet->mMeta.mHMetas[1].mTOrigin.toFloat()/time_s(1);  //! ps to s
            m_RealtInc = pRet->mMeta.mHMetas[1].mTInc.toFloat()/time_s(1);     //! ps to s
            m_DataLen  = pRet->mMeta.mHMetas[1].mLength;
        }
        else
        {
            m_Realt0   = pRet->mMeta.mHMetas[0].mTOrigin.toFloat()/time_s(1);  //! ps to s
            m_RealtInc = pRet->mMeta.mHMetas[0].mTInc.toFloat()/time_s(1);     //! ps to s
            m_DataLen  = pRet->mMeta.mHMetas[0].mLength;
        }

        unsigned int refLoc = (unsigned int)m_DataLen/(unsigned int)2;

        m_FPGARawData->meas_getFPGARawData(m_Src, pAdcCH, pRet);
        PulseMeas_FPGA(m_RealtInc, refLoc);

        if( !isLaCH())
        {
            AmpMeas_FPGA(yGnd, m_DataLen);
            EdgeMeas_FPGA(m_RealtInc, refLoc, yGnd);
            CountMeas_FPGA();
        }
    }
}

bool measData::isLaCH()
{
    if( (m_Src >= d0)&&(m_Src <= d15))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void measData::CalcRealMeasData_FPGA(void *pFPGA_ret, void *adcCH, void *horInfo)
{
    int nRollStart = 0;
    m_HorInfo = (EngineHoriInfo *)horInfo;

    dsoVert *pVert;
    if( isLaCH())
    {
        pVert = dsoVert::getCH( la);
        m_ChUnit = pVert->getUnit();
        m_ChOnOff = pVert->getOnOff();
    }
    else
    {
        pVert = dsoVert::getCH( m_Src);

        if( m_RegionType == REGION_MAIN)
        {
            pVert->getPixel(*m_Wfm);
        }
        else
        {
            if( m_ZoomOn)
            {
                pVert->getPixel(*m_Wfm, chan_none, horizontal_view_zoom);
            }
            else
            {
                pVert->getPixel(*m_Wfm);
            }
        }

        nRollStart = m_Wfm->start();

        //qDebug() << "offset left:" << m_Wfm->start();

        m_ChUnit = pVert->getUnit();
        m_ChOnOff    = pVert->getOnOff();
        float ratio;
        pVert->getProbe().toReal( ratio, 1.0f);

        float base;
        pVert->getYRefBase().toReal(base);
        m_RealScale  = pVert->getYRefScale() * base * ratio;
        m_RealOffset = pVert->getYRefOffset() *base * ratio;

        EngineMeasRet *pRet = (EngineMeasRet *)pFPGA_ret;
        float yInc = 0.0;
        qint32 yGnd = 0;
        switch (m_Src)
        {
        case chan1:
            yInc = pRet->mMeta.mVMetas[0].myInc.toFloat()/vv(1);  //! uv to v
            yGnd = pRet->mMeta.mVMetas[0].myGnd;
            break;
        case chan2:
            yInc = pRet->mMeta.mVMetas[1].myInc.toFloat()/vv(1);  //! uv to v
            yGnd = pRet->mMeta.mVMetas[1].myGnd;
            break;
        case chan3:
            yInc = pRet->mMeta.mVMetas[2].myInc.toFloat()/vv(1);  //! uv to v
            yGnd = pRet->mMeta.mVMetas[2].myGnd;
            break;
        case chan4:
            yInc = pRet->mMeta.mVMetas[3].myInc.toFloat()/vv(1);  //! uv to v
            yGnd = pRet->mMeta.mVMetas[3].myGnd;
            break;
        default:
            yInc = pRet->mMeta.mVMetas[0].myInc.toFloat()/vv(1);  //! uv to v
            yGnd = pRet->mMeta.mVMetas[0].myGnd;
            break;
        }
        m_RealyInc   = ratio * yInc;
        //m_RealyGnd   = ratio * yGnd;
        m_RealyGnd   = yGnd;
    }


    if( m_RegionType == REGION_MAIN)  //! Meas Region: ALL Memory Data
    {

    }
    else if( m_RegionType == REGION_CURSOR) //! Meas Region: Memory Data between CursorA&B
    {

    }
    else     //! Meas Region: Default All Memory Data
    {

    }

    CalcRawMeasData_FPGA(pFPGA_ret, adcCH, m_RealyGnd);

    //added by hxh for LA Measure
    if( isLaCH())
    {
        m_RawMeasData.bVaild_Vpp = true;
    }
    //! Horizontal Measure
    calc_Period(nRollStart);
    calc_Freq(nRollStart);
    calc_RiseTime(nRollStart);
    calc_FallTime(nRollStart);
    calc_PWidth(nRollStart);
    calc_NWidth(nRollStart);
    calc_PDuty(nRollStart);
    calc_NDuty(nRollStart);
    calc_PPulses(nRollStart);
    calc_NPulses(nRollStart);
    calc_PEdges(nRollStart);
    calc_NEdges(nRollStart);
    calc_Tvmax(nRollStart);
    calc_Tvmin(nRollStart);
    calc_Pslew_rate(nRollStart);
    calc_Nslew_rate(nRollStart);

    //! Vertical Measure
    calc_Vmax();
    calc_Vmin();
    calc_Vpp();
    calc_Vtop();
    calc_Vbase();
    calc_Vamp();
    calc_Vupper();
    calc_Vmid();
    calc_Vlower();
    calc_Vavg();
    calc_Vrms();
    calc_Vrms_S();
    calc_Overshoot();
    calc_Preshoot();
    calc_Area();
    calc_Area_S();
    calc_Variance();
}

MeasValue measData::getRealMeasData(MeasType type)
{
    MeasValue value;
    value = m_RealMeasData.value(type);
    return value;
}

MeasStatus measData::getMeasStatus(MeasType type)
{
    return getRealMeasData( type).mStatus;
}

bool measData::isValid()
{
    if( m_RawMeasData.bVaild_Vpp)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool measData::isValidData()
{
    if( m_MeasDataStatus == Trace_Id_Invalid)
    {
        return false;
    }
    else
    {
        return true;
    }
}

//! Get Data X index from Cursor position
int measData::getTraceX(int rangeX1, HorizontalView hview)
{
    horiAttr hAttr;
    hAttr = getHoriAttr(m_Src, hview);
    float traceXreal = (float)( rangeX1 - hAttr.gnd ) * hAttr.tInc/time_s(1);  //! ps to s

    int traceX = ( traceXreal - m_Realt0)/m_RealtInc;

    if( isnan(traceXreal) || isinf(traceXreal))
    {
        return 0;
    }
    else if( traceX <= 0)
    {
        return 0;
    }
    else if( traceX >= m_DataLen)
    {
        return m_DataLen;
    }
    else
    {
        return traceX;
    }
}

void measData::setRegion( RegionType rType, bool zoom, int rX1, int rX2)
{
    m_RegionType = rType;
    m_RangeX1 = rX1;
    m_RangeX2 = rX2;
    m_ZoomOn = zoom;
}

void measData::test_ImportSrcData(unsigned char *pData, short Len)
{
    QFile traceFile("/rigol/test/trace.csv");
    if( traceFile.open( QIODevice::WriteOnly | QIODevice::Text ))
    {
        QTextStream stream( &traceFile);

        stream << QString ( "Traca Data:") << "\n";

        for(int i=0; i<Len; i++)
        {
            stream << QString("%1").arg(*pData++)<<"\n";
        }

        traceFile.close();
    }
    else
    {
        LOG_DBG()<< "Error: cannot import trace data!!";
    }
}

// for SOFT_ALgorithm Test
void measData::test_ReadTestData(void)
{

    QFile measDataFile("/rigol/test/meas.data");
    if(measDataFile.open(QIODevice::ReadOnly))
    {
        QDataStream stream( &measDataFile );
        m_pTest_Data = new DsoPoint[m_Test_DataNum];
        Q_ASSERT( NULL != m_pTest_Data );

        m_Test_DataNum = stream.readRawData( (char*)m_pTest_Data, m_Test_DataNum);
        LOG_DBG() << "Read "<<m_Test_DataNum <<" bytes";

        measDataFile.close();
    }
    else
    {
        m_pTest_Data = NULL;
        LOG_DBG() << "Error: cannot read test data!!";
    }
}
