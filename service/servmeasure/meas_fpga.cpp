#include "servmeasure.h"

 void servMeasure::setFPGA_Region()
 {
     qlonglong memAXreal = 0;
     qlonglong memBXreal = 0;
     horiAttr hAttr;

     if( (RegionType)m_Region == REGION_CURSOR)   //! Meas Region(Cursor)
     {
         if( m_ZoomOn)
         {
             hAttr = getHoriAttr( chan1, horizontal_view_zoom);
             postEngine( ENGINE_MEAS_VIEW, horizontal_view_zoom);
         }
         else
         {
             hAttr = getHoriAttr( chan1, horizontal_view_main);
             postEngine( ENGINE_MEAS_VIEW, horizontal_view_main);
         }
         memAXreal = ( m_CursorA - hAttr.gnd ) * hAttr.tInc; //! ps
         memBXreal = ( m_CursorB - hAttr.gnd ) * hAttr.tInc; //! ps
     }
     else if( (RegionType)m_Region == REGION_ZOOM) //! Meas Region(Zoom)
     {
         hAttr = getHoriAttr( chan1, horizontal_view_zoom);
         postEngine( ENGINE_MEAS_VIEW, horizontal_view_zoom);
         memAXreal = ( 0 - hAttr.gnd ) * hAttr.tInc;  //! ps
         //! zx change
         //memBXreal = ( trace_width-1 - hAttr.gnd ) * hAttr.tInc; //! ps
         memBXreal = ( trace_width - hAttr.gnd ) * hAttr.tInc; //! ps
     }
     else      //! Meas Region(Main)
     {
         hAttr = getHoriAttr( chan1, horizontal_view_main);
         postEngine( ENGINE_MEAS_VIEW, horizontal_view_main);
         memAXreal = ( 0 - hAttr.gnd ) * hAttr.tInc;  //! ps
         //! zx change
         //memBXreal = ( trace_width-1 - hAttr.gnd ) * hAttr.tInc; //! ps
         memBXreal = ( trace_width - hAttr.gnd ) * hAttr.tInc; //! ps
     }

     qlonglong tS = memAXreal;
     qlonglong tLength = memBXreal - memAXreal;

     LOG_DBG() << "tS: " <<tS;
     LOG_DBG() << "tLength: " <<tLength;
     postEngine( ENGINE_MEAS_RANGE, tS, tLength);
 }

void servMeasure::setFPGA_MeasOnce()
{
    serviceExecutor::postEngine( ENGINE_MEAS_ONCE_REQ);
}

void servMeasure::setFPGA_MeasCfg()
{
#if 1
    unsigned int topMethod, baseMethod;
    if( getTopMethod() == meas_algorithm::METHOD_MaxMin)
    {
        topMethod = FPGA_TopBase_MaxMin;
    }
    else
    {
        topMethod = FPGA_TopBase_Hist;
    }

    if( getBaseMethod() == meas_algorithm::METHOD_MaxMin)
    {
        baseMethod = FPGA_TopBase_MaxMin;
    }
    else
    {
        baseMethod = FPGA_TopBase_Hist;
    }

    //Get HORI_INFO
//    DsoErr ret_val = ERR_NONE;
//    EngineHoriInfo *horInfo; //= new EngineHoriInfo();
//    EngineHoriInfo horinfo;

//    LOG_DBG();
//    horInfo = &horinfo;
//    ret_val = queryEngine( qENGINE_HORI_INFO, horInfo);
//    LOG_DBG() <<":   "<<ret_val << " = queryEngine( qENGINE_HORI_INFO, horInfo)";
//    LOG_DBG() <<":   horInfo->mChViewLenM = "<< horInfo->mChViewLen.M();
//    LOG_DBG() <<":   horInfo->mChViewLenN = "<< horInfo->mChViewLen.N();
//    qreal viewLenM = (qreal)(horInfo->mChViewLen.M());
//    qreal viewLenN = (qreal)(horInfo->mChViewLen.N());
//    int viewLen = qRound( viewLenM/viewLenN);
//    LOG_DBG() <<":   viewLen = "<< viewLen;

//    m_FPGAMeasCfg->assigntotal_num( (unsigned int)viewLen);   //Set FPGA Points
//    m_FPGAMeasCfg->assignmeas_statis_ctl(1);
    if( getStateMethodMode() == meas_algorithm::MODE_AUTO)
    {
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_cha_top_type_sel(FPGA_TopBase_Auto);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_cha_base_type_sel(FPGA_TopBase_Auto);

        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chb_top_type_sel(FPGA_TopBase_Auto);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chb_base_type_sel(FPGA_TopBase_Auto);

        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chc_top_type_sel(FPGA_TopBase_Auto);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chc_base_type_sel(FPGA_TopBase_Auto);

        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chd_top_type_sel(FPGA_TopBase_Auto);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chd_base_type_sel(FPGA_TopBase_Auto);
    }
    else
    {
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_cha_top_type_sel( topMethod);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_cha_base_type_sel( baseMethod);

        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chb_top_type_sel( topMethod);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chb_base_type_sel( baseMethod);

        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chc_top_type_sel( topMethod);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chc_base_type_sel( baseMethod);

        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chd_top_type_sel( topMethod);
        m_FPGAMeasCfg->assignmeas_vtop_base_result_sel_chd_base_type_sel( baseMethod);
    }

    EngineAdcCoreCH adcCH;
    queryEngine( qENGINE_ADC_CORE_CH, &adcCH);
    ThresholdData *th = NULL;

    th = m_ThresholdList.value( getAdcCH( adcCH.mCHa));
    m_FPGAMeasCfg->assigncha_threshold_h_mid_l_cfg_cha_abs_percent_sel(th->mtype);
    m_FPGAMeasCfg->assigncha_threshold_h_mid_l_cfg_cha_threshold_h_cfg(th->mIntHigh_FPGA);
    m_FPGAMeasCfg->assigncha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg(th->mIntMid_FPGA);
    m_FPGAMeasCfg->assigncha_threshold_h_mid_l_cfg_cha_threshold_l_cfg(th->mIntLow_FPGA);

//    qDebug()<<"adcCH.Threshold.mtype"<<th->mtype;
//    qDebug()<<"adcCH.Threshold.mIntHigh_FPGA"<<th->mIntHigh_FPGA;
//    qDebug()<<"adcCH.Threshold.mIntMid_FPGA"<<th->mIntMid_FPGA;
//    qDebug()<<"adcCH.Threshold.mIntLow_FPGA"<<th->mIntLow_FPGA;

    th = m_ThresholdList.value( getAdcCH( adcCH.mCHb));
    m_FPGAMeasCfg->assignchb_threshold_h_mid_l_cfg_chb_abs_percent_sel(th->mtype);
    m_FPGAMeasCfg->assignchb_threshold_h_mid_l_cfg_chb_threshold_h_cfg(th->mIntHigh_FPGA);
    m_FPGAMeasCfg->assignchb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg(th->mIntMid_FPGA);
    m_FPGAMeasCfg->assignchb_threshold_h_mid_l_cfg_chb_threshold_l_cfg(th->mIntLow_FPGA);

    th = m_ThresholdList.value( getAdcCH( adcCH.mCHc));
    m_FPGAMeasCfg->assignchc_threshold_h_mid_l_cfg_chc_abs_percent_sel(th->mtype);
    m_FPGAMeasCfg->assignchc_threshold_h_mid_l_cfg_chc_threshold_h_cfg(th->mIntHigh_FPGA);
    m_FPGAMeasCfg->assignchc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg(th->mIntMid_FPGA);
    m_FPGAMeasCfg->assignchc_threshold_h_mid_l_cfg_chc_threshold_l_cfg(th->mIntLow_FPGA);

    th = m_ThresholdList.value( getAdcCH( adcCH.mCHd));
    m_FPGAMeasCfg->assignchd_threshold_h_mid_l_cfg_chd_abs_percent_sel(th->mtype);
    m_FPGAMeasCfg->assignchd_threshold_h_mid_l_cfg_chd_threshold_h_cfg(th->mIntHigh_FPGA);
    m_FPGAMeasCfg->assignchd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg(th->mIntMid_FPGA);
    m_FPGAMeasCfg->assignchd_threshold_h_mid_l_cfg_chd_threshold_l_cfg(th->mIntLow_FPGA);

//    m_FPGAMeasCfg->assignmeas_cycnum(1);    //Loop Number
//    m_FPGAMeasCfg->assignanalog_la_sample_rate(4); // Analog Points/LA Points rate
    m_FPGAMeasCfg->assignresult_rdduring_flag(Not_Reading_FPGA);  //Not Reading result
//    m_FPGAMeasCfg->assignrw_test_reg_rw_test_reg(1);

    postEngine( ENGINE_MEAS_CFG, m_FPGAMeasCfg);
    LOG_DBG() <<":   set ENGINE_MEAS_CFG";

    postEngine( ENGINE_MEAS_EN, (quint32)FPGA_CH_LA_MAIN_EN);
    LOG_DBG() <<":   set ENGINE_MEAS_EN";
#endif
}

Chan servMeasure::getAdcCH( Chan adcCH)
{
    if( adcCH == chan1)
    {
        return chan1;
    }
    else if( adcCH == chan2)
    {
        return chan2;
    }
    else if( adcCH == chan3)
    {
        return chan3;
    }
    else if( adcCH == chan4)
    {
        return chan4;
    }
    else
    {
        return chan1;
    }
}

void servMeasure::setFPGA_CntType( CArgument arg)
{
    if( arg.size() != 2)  //! 2 Val: MeasType & src
    {
        return;
    }

    LOG_DBG()<<" setFPGA_CntType arg.size() = "<<arg.size();
    MeasType type = (MeasType)arg[0].iVal;
    Chan src = (Chan)arg[1].iVal;

    Chan chFPGA;
    EngineAdcCoreCH adcCH;
    queryEngine( qENGINE_ADC_CORE_CH, &adcCH);

    if( src == adcCH.mCHa)         chFPGA = chan1;
    else if( src == adcCH.mCHb)    chFPGA = chan2;
    else if( src == adcCH.mCHc)    chFPGA = chan3;
    else if( src == adcCH.mCHd)    chFPGA = chan4;
    else                           chFPGA = chan1;

    if( type == Meas_PPulses)
    {
        postEngine( ENGINE_MEAS_PULSE_TYPE, (int)chFPGA, (int)FPGA_Cnt_Pulse_P);
        LOG_DBG()<<"postEngine( ENGINE_MEAS_PULSE_TYPE) FPGA_Cnt_Pulse_P";
    }

    if( type == Meas_NPulses)
    {
        postEngine( ENGINE_MEAS_PULSE_TYPE, (int)chFPGA, (int)FPGA_Cnt_Pulse_N);
        LOG_DBG()<<"postEngine( ENGINE_MEAS_PULSE_TYPE) FPGA_Cnt_Pulse_N";
    }

    if( type == Meas_PEdges)
    {
        postEngine( ENGINE_MEAS_EDGE_TYPE,  (int)chFPGA, (int)FPGA_Cnt_Edge_Rise);
        LOG_DBG()<<"postEngine( ENGINE_MEAS_EDGE_TYPE) FPGA_Cnt_Edge_Rise";
    }

    if( type == Meas_NEdges)
    {
        postEngine( ENGINE_MEAS_EDGE_TYPE,  (int)chFPGA, (int)FPGA_Cnt_Edge_Fall);
        LOG_DBG()<<"postEngine( ENGINE_MEAS_EDGE_TYPE) FPGA_Cnt_Edge_Fall";
    }
}

void servMeasure::setFPGA_MeasDSrc()
{
    if( m_MeasDataDSrc.count() != 0)
    {
        foreach( measDataDSrc *pData, m_MeasDataDSrc)
        {
            Q_ASSERT( NULL != pData);
            setFPGA_Dly( pData);
        }
    }
}

void servMeasure::setFPGA_Dly( measDataDSrc *pData)
{
    EngineAdcCoreCH adcCH;
    queryEngine( qENGINE_ADC_CORE_CH, &adcCH);
    Chan chA = pData->getMeasSrcA();
    Chan chB = pData->getMeasSrcB();
    MeasType type = pData->getMeasType();
    MeasFPGA_DlyIndex index = pData->getFPGADlyIndex();

    setFPGA_DlySrcA( index, &adcCH, chA);
    setFPGA_DlySrcB( index, &adcCH, chB);
    setFPGA_Dly_edge_from_type( index, type);
    setFPGA_Dly_edge_to_type( index, type);
}

void servMeasure::setFPGA_DlySrcA( MeasFPGA_DlyIndex index, EngineAdcCoreCH *adcCH, Chan srcA)
{
    switch (srcA)
    {
    case d0:    setFPGA_Dly_edge_from_ch( index, FPGA_LA0);  break;
    case d1:    setFPGA_Dly_edge_from_ch( index, FPGA_LA1);  break;
    case d2:    setFPGA_Dly_edge_from_ch( index, FPGA_LA2);  break;
    case d3:    setFPGA_Dly_edge_from_ch( index, FPGA_LA3);  break;
    case d4:    setFPGA_Dly_edge_from_ch( index, FPGA_LA4);  break;
    case d5:    setFPGA_Dly_edge_from_ch( index, FPGA_LA5);  break;
    case d6:    setFPGA_Dly_edge_from_ch( index, FPGA_LA6);  break;
    case d7:    setFPGA_Dly_edge_from_ch( index, FPGA_LA7);  break;

    case d8:    setFPGA_Dly_edge_from_ch( index, FPGA_LA8);  break;
    case d9:    setFPGA_Dly_edge_from_ch( index, FPGA_LA9);  break;
    case d10:   setFPGA_Dly_edge_from_ch( index, FPGA_LA10); break;
    case d11:   setFPGA_Dly_edge_from_ch( index, FPGA_LA11); break;
    case d12:   setFPGA_Dly_edge_from_ch( index, FPGA_LA12); break;
    case d13:   setFPGA_Dly_edge_from_ch( index, FPGA_LA13); break;
    case d14:   setFPGA_Dly_edge_from_ch( index, FPGA_LA14); break;
    case d15:   setFPGA_Dly_edge_from_ch( index, FPGA_LA15); break;

    default:
        if( srcA == adcCH->mCHa)
        {
            setFPGA_Dly_edge_from_ch( index, FPGA_CHa);
        }
        else if( srcA == adcCH->mCHb)
        {
            setFPGA_Dly_edge_from_ch( index, FPGA_CHb);
        }
        else if( srcA == adcCH->mCHc)
        {
            setFPGA_Dly_edge_from_ch( index, FPGA_CHc);
        }
        else if( srcA == adcCH->mCHd)
        {
            setFPGA_Dly_edge_from_ch( index, FPGA_CHd);
        }
        else
        {
            setFPGA_Dly_edge_from_ch( index, FPGA_CHa);
        }
        break;
    }
}

void servMeasure::setFPGA_DlySrcB(MeasFPGA_DlyIndex index, EngineAdcCoreCH *adcCH, Chan srcB)
{
    switch (srcB)
    {
    case d0:    setFPGA_Dly_edge_to_ch( index, FPGA_LA0);  break;
    case d1:    setFPGA_Dly_edge_to_ch( index, FPGA_LA1);  break;
    case d2:    setFPGA_Dly_edge_to_ch( index, FPGA_LA2);  break;
    case d3:    setFPGA_Dly_edge_to_ch( index, FPGA_LA3);  break;
    case d4:    setFPGA_Dly_edge_to_ch( index, FPGA_LA4);  break;
    case d5:    setFPGA_Dly_edge_to_ch( index, FPGA_LA5);  break;
    case d6:    setFPGA_Dly_edge_to_ch( index, FPGA_LA6);  break;
    case d7:    setFPGA_Dly_edge_to_ch( index, FPGA_LA7);  break;

    case d8:    setFPGA_Dly_edge_to_ch( index, FPGA_LA8);  break;
    case d9:    setFPGA_Dly_edge_to_ch( index, FPGA_LA9);  break;
    case d10:   setFPGA_Dly_edge_to_ch( index, FPGA_LA10); break;
    case d11:   setFPGA_Dly_edge_to_ch( index, FPGA_LA11); break;
    case d12:   setFPGA_Dly_edge_to_ch( index, FPGA_LA12); break;
    case d13:   setFPGA_Dly_edge_to_ch( index, FPGA_LA13); break;
    case d14:   setFPGA_Dly_edge_to_ch( index, FPGA_LA14); break;
    case d15:   setFPGA_Dly_edge_to_ch( index, FPGA_LA15); break;

    default:
        if( srcB == adcCH->mCHa)
        {
            setFPGA_Dly_edge_to_ch( index, FPGA_CHa);
        }
        else if( srcB == adcCH->mCHb)
        {
            setFPGA_Dly_edge_to_ch( index, FPGA_CHb);
        }
        else if( srcB == adcCH->mCHc)
        {
            setFPGA_Dly_edge_to_ch( index, FPGA_CHc);
        }
        else if( srcB == adcCH->mCHd)
        {
            setFPGA_Dly_edge_to_ch( index, FPGA_CHd);
        }
        else
        {
            setFPGA_Dly_edge_to_ch( index, FPGA_CHa);
        }
        break;
    }
}

void servMeasure::setFPGA_Dly_edge_from_ch( MeasFPGA_DlyIndex index, FPGADlySrc ch)
{
    if( NULL != m_FPGAMeasCfg)
    {
        switch (index)
        {
        case Dly1:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_from_channel_sel(ch); break;
        case Dly2:
            m_FPGAMeasCfg->assigndly_phase_cfg_2_edge_from_channel_sel(ch); break;
        case Dly3:
            m_FPGAMeasCfg->assigndly_phase_cfg_3_edge_from_channel_sel(ch); break;
        case Dly4:
            m_FPGAMeasCfg->assigndly_phase_cfg_4_edge_from_channel_sel(ch); break;
        case Dly5:
            m_FPGAMeasCfg->assigndly_phase_cfg_5_edge_from_channel_sel(ch); break;
        case Dly6:
            m_FPGAMeasCfg->assigndly_phase_cfg_6_edge_from_channel_sel(ch); break;
        case Dly7:
            m_FPGAMeasCfg->assigndly_phase_cfg_7_edge_from_channel_sel(ch); break;
        case Dly8:
            m_FPGAMeasCfg->assigndly_phase_cfg_8_edge_from_channel_sel(ch); break;
        case Dly9:
            m_FPGAMeasCfg->assigndly_phase_cfg_9_edge_from_channel_sel(ch); break;
        case Dly10:
            m_FPGAMeasCfg->assigndly_phase_cfg_10_edge_from_channel_sel(ch); break;

        default:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_from_channel_sel(ch); break;
        }
    }
}

void servMeasure::setFPGA_Dly_edge_from_type( MeasFPGA_DlyIndex index, MeasType type)
{
    if( NULL != m_FPGAMeasCfg)
    {
        FPGADlyEdgeType edge = FPGA_Dly_Rise;
        if(  (type == Meas_DelayFF)||(type == Meas_PhaseFF)
           ||(type == Meas_DelayFR)||(type == Meas_PhaseFR) )
        {
            edge = FPGA_Dly_fall;
        }
        else
        {
            edge = FPGA_Dly_Rise;
        }

        switch (index)
        {
        case Dly1:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_from_type(edge); break;
        case Dly2:
            m_FPGAMeasCfg->assigndly_phase_cfg_2_edge_from_type(edge); break;
        case Dly3:
            m_FPGAMeasCfg->assigndly_phase_cfg_3_edge_from_type(edge); break;
        case Dly4:
            m_FPGAMeasCfg->assigndly_phase_cfg_4_edge_from_type(edge); break;
        case Dly5:
            m_FPGAMeasCfg->assigndly_phase_cfg_5_edge_from_type(edge); break;
        case Dly6:
            m_FPGAMeasCfg->assigndly_phase_cfg_6_edge_from_type(edge); break;
        case Dly7:
            m_FPGAMeasCfg->assigndly_phase_cfg_7_edge_from_type(edge); break;
        case Dly8:
            m_FPGAMeasCfg->assigndly_phase_cfg_8_edge_from_type(edge); break;
        case Dly9:
            m_FPGAMeasCfg->assigndly_phase_cfg_9_edge_from_type(edge); break;
        case Dly10:
            m_FPGAMeasCfg->assigndly_phase_cfg_10_edge_from_type(edge); break;

        default:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_from_type(edge); break;
        }
    }
}

void servMeasure::setFPGA_Dly_edge_to_ch(MeasFPGA_DlyIndex index, FPGADlySrc ch)
{
    if( NULL != m_FPGAMeasCfg)
    {
        switch (index)
        {
        case Dly1:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_to_channel_sel(ch); break;
        case Dly2:
            m_FPGAMeasCfg->assigndly_phase_cfg_2_edge_to_channel_sel(ch); break;
        case Dly3:
            m_FPGAMeasCfg->assigndly_phase_cfg_3_edge_to_channel_sel(ch); break;
        case Dly4:
            m_FPGAMeasCfg->assigndly_phase_cfg_4_edge_to_channel_sel(ch); break;
        case Dly5:
            m_FPGAMeasCfg->assigndly_phase_cfg_5_edge_to_channel_sel(ch); break;
        case Dly6:
            m_FPGAMeasCfg->assigndly_phase_cfg_6_edge_to_channel_sel(ch); break;
        case Dly7:
            m_FPGAMeasCfg->assigndly_phase_cfg_7_edge_to_channel_sel(ch); break;
        case Dly8:
            m_FPGAMeasCfg->assigndly_phase_cfg_8_edge_to_channel_sel(ch); break;
        case Dly9:
            m_FPGAMeasCfg->assigndly_phase_cfg_9_edge_to_channel_sel(ch); break;
        case Dly10:
            m_FPGAMeasCfg->assigndly_phase_cfg_10_edge_to_channel_sel(ch); break;

        default:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_to_channel_sel(ch); break;
        }
    }
}

void servMeasure::setFPGA_Dly_edge_to_type( MeasFPGA_DlyIndex index, MeasType type)
{
    if( NULL != m_FPGAMeasCfg)
    {
        FPGADlyEdgeType edge = FPGA_Dly_Rise;
        if(  (type == Meas_DelayFF)||(type == Meas_PhaseFF)
           ||(type == Meas_DelayRF)||(type == Meas_PhaseRF) )
        {
            edge = FPGA_Dly_fall;
        }
        else
        {
            edge = FPGA_Dly_Rise;
        }

        switch (index)
        {
        case Dly1:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_to_type(edge); break;
        case Dly2:
            m_FPGAMeasCfg->assigndly_phase_cfg_2_edge_to_type(edge); break;
        case Dly3:
            m_FPGAMeasCfg->assigndly_phase_cfg_3_edge_to_type(edge); break;
        case Dly4:
            m_FPGAMeasCfg->assigndly_phase_cfg_4_edge_to_type(edge); break;
        case Dly5:
            m_FPGAMeasCfg->assigndly_phase_cfg_5_edge_to_type(edge); break;
        case Dly6:
            m_FPGAMeasCfg->assigndly_phase_cfg_6_edge_to_type(edge); break;
        case Dly7:
            m_FPGAMeasCfg->assigndly_phase_cfg_7_edge_to_type(edge); break;
        case Dly8:
            m_FPGAMeasCfg->assigndly_phase_cfg_8_edge_to_type(edge); break;
        case Dly9:
            m_FPGAMeasCfg->assigndly_phase_cfg_9_edge_to_type(edge); break;
        case Dly10:
            m_FPGAMeasCfg->assigndly_phase_cfg_10_edge_to_type(edge); break;

        default:
            m_FPGAMeasCfg->assigndly_phase_cfg_1_edge_to_type(edge); break;
        }
    }
}

MeasFPGA_DlyIndex servMeasure::getFPGA_Dly_index()
{
    MeasFPGA_DlyIndex index = Dly_none;
    QList<MeasFPGA_DlyIndex> FPGA_Dly_Index_List;
    FPGA_Dly_Index_List.clear();
    FPGA_Dly_Index_List.append( Dly1);
    FPGA_Dly_Index_List.append( Dly2);
    FPGA_Dly_Index_List.append( Dly3);
    FPGA_Dly_Index_List.append( Dly4);
    FPGA_Dly_Index_List.append( Dly5);
    FPGA_Dly_Index_List.append( Dly6);
    FPGA_Dly_Index_List.append( Dly7);
    FPGA_Dly_Index_List.append( Dly8);
    FPGA_Dly_Index_List.append( Dly9);
    FPGA_Dly_Index_List.append( Dly10);

    if( m_MeasDataDSrc.count() != 0)
    {
        MeasFPGA_DlyIndex index_inUse = Dly_none;
        foreach( measDataDSrc *pData, m_MeasDataDSrc)
        {
            Q_ASSERT( NULL != pData);
            index_inUse = pData->getFPGADlyIndex();
            FPGA_Dly_Index_List.removeAll( index_inUse);
        }
    }

    if( FPGA_Dly_Index_List.isEmpty())
    {
        index = Dly_none;
    }
    else
    {
        index = FPGA_Dly_Index_List.takeFirst();
    }

    return index;
}

void servMeasure::getFPGA_MeasData()
{
    quint32 ret_Done = FPGA_MEAS_NOT_Ready;

    for(int i=0; i<20; i++)
    {
        queryEngine( qENGINE_MEAS_RESULT_DONE, ret_Done);

        if( ret_Done == FPGA_MEAS_Ready)
        {
            LOG_DBG() <<"   FPGA_MEAS_RESULT_DONE: "<<ret_Done;
            break;
        }
        QThread::msleep(50);
    }

    postEngine( ENGINE_MEAS_RD_DURING_FLAG, true);
    queryEngine( qENGINE_MEAS_VALUE, m_FPGAMeasRet);
    postEngine( ENGINE_MEAS_RD_DURING_FLAG, false);

}

bool servMeasure::isFPGA_Mode()  //! for Meas Thread
{
    Chan srcA = chan_none;
    Chan srcB = chan_none;

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        if( (m_FPGAMeasRet == NULL)||
            (m_FPGAMeasRet->mMeta.mHMeta.mLength <= trace_width) )
        {
            return false;    //! mLength <1000 Points,switch to Soft_algorithm
        }

        m_MeasDataMutex.lock();
        bool empty = m_MeasStatData.isEmpty();
        m_MeasDataMutex.unlock();
        if( empty)
        {
            return true;  //! No Meas Src, m_Range_Mode == FPGA_MODE
        }
        else
        {
            bool b = true;
            m_MeasDataMutex.lock();
            for( int i=0; i<m_MeasStatData.size(); i++)
            {
                srcA = list_at( m_MeasStatData,i)->getSrcA();
                srcB = list_at( m_MeasStatData,i)->getSrcB();
                if(  ((srcA >= m1)&&(srcA <= m4))
                   ||((srcB >= m1)&&(srcB <= m4)) )
                {
                    b =  false;  //! have Soft src(Math src)
                    break;
                }
            }
            m_MeasDataMutex.unlock();
            return b;
        }
    }
    else
    {
        return false;
    }
}

bool servMeasure::isFPGA_Src( Chan src)
{
    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        if( (m_FPGAMeasRet == NULL)||
            (m_FPGAMeasRet->mMeta.mHMeta.mLength <= trace_width) )
        {
            return false;    //! mLength <1000 Points,switch to Soft_algorithm
        }
        else if( ((src >= chan1)&&(src <= chan4))||
                 ((src >= d0)&&(src <= d15)) )
        {
            if( m_MeasDataDSrc.size() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

int servMeasure::getUpdateTime()
{
    int mode = (int)Acquire_YT;
    query( serv_name_hori, MSG_HORI_TIME_VIEW_MODE, mode);

    if( (mode == (int)Acquire_ROLL)||
        (mode == (int)Acquire_SCAN) )
    {
        if( m_Range_Mode == (int)FPGA_MODE)
        {
            return MEAS_SCAN_TIM;
        }
        else
        {
            return MEAS_NORM_TIM;
        }
    }
    else
    {
        if( m_Range_Mode == (int)FPGA_MODE && isEngineStop())
        {
            return 200;
        }
        return MEAS_NORM_TIM;
    }
}


//! ------------------------------------For FPGA Measure Test----------------------------------------
void servMeasure::getFPGA_MeasData_Test()
{
    LOG_DBG() <<"------------------------------------------ Start------------------------------";
    quint32 ret_Done = FPGA_MEAS_NOT_Ready;

    LOG_DBG()<<(((getStateMethodMode())==meas_algorithm::MODE_AUTO)? "Method:Auto":"Method:Manual") ;
    LOG_DBG()<<(((getTopMethod())==meas_algorithm::METHOD_Histogram)? "TopMethod:HIS":"TopMethod:MAX-MIN") ;
    LOG_DBG()<<(((getBaseMethod())==meas_algorithm::METHOD_Histogram)? "BaseMethod:HIS":"BaseMethod:MAX-MIN") ;

    dsoVert *pVert = dsoVert::getCH((Chan)m_SrcA);
    float yInc = pVert->getyInc();
    int yGnd   = pVert->getyGnd();

    EngineAdcCoreCH *adcCH, adcch;

    adcCH = &adcch;

    queryEngine( qENGINE_ADC_CORE_CH, adcCH);

    for(int i=0; i<20; i++)
    {
        queryEngine( qENGINE_MEAS_RESULT_DONE, ret_Done);

        if( ret_Done == FPGA_MEAS_Ready)
        {
            LOG_DBG() <<"FPGA_MEAS_RESULT_DONE: "<<ret_Done;
            break;
        }
        QThread::msleep(50);
    }

    postEngine( ENGINE_MEAS_RD_DURING_FLAG, true);
    queryEngine( qENGINE_MEAS_VALUE, m_FPGAMeasRet);

    float Realt0   = m_FPGAMeasRet->mMeta.mHMeta.mTOrigin.toFloat()/time_s(1);  //! ps to s
    float RealtInc = m_FPGAMeasRet->mMeta.mHMeta.mTInc.toFloat()/time_s(1);     //! ps to s
    int   DataLen  = m_FPGAMeasRet->mMeta.mHMeta.mLength;
    LOG_DBG() <<"Realt0"<<Realt0;
    LOG_DBG() <<"RealtInc"<<RealtInc;
    LOG_DBG() <<"DataLen"<<DataLen;

    qlonglong VmaxF = m_FPGAMeasRet->getcha_max_min_top_base_cha_vmax();
    qlonglong VminF = m_FPGAMeasRet->getcha_max_min_top_base_cha_vmin();
    qlonglong VtopF = m_FPGAMeasRet->getcha_max_min_top_base_cha_vtop();
    qlonglong VbaseF = m_FPGAMeasRet->getcha_max_min_top_base_cha_vbase();

    float Vmax = (int(VmaxF)-yGnd)*yInc;
    float Vmin = (int(VminF)-yGnd)*yInc;
    float Vtop = (int(VtopF)-yGnd)*yInc;
    float Vbase = (int(VbaseF)-yGnd)*yInc;

    LOG_DBG() <<"  yInc:  "<< yInc <<"V";
    LOG_DBG() <<"  yGnd:  "<< yGnd;
    LOG_DBG() <<"  Vmax:   "<< Vmax <<"V   " <<"Vmax(F):   "<< VmaxF;
    LOG_DBG() <<"  Vtop:   "<< Vtop <<"V   " <<"Vtop(F):   "<< VtopF;
    LOG_DBG() <<"  Vbase:  "<< Vbase <<"V   "<<"Vbase(F):  "<< VbaseF;
    LOG_DBG() <<"  Vmin:   "<< Vmin  <<"V   "<<"Vmin(F):   "<< VminF;
    LOG_DBG() <<"-------------------------------------------------------------------------------";

    qlonglong ave_sum_bit31_0 = (qlonglong)(m_FPGAMeasRet->getcha_ave_sum_n_bits31_0_ave_sum_n_bit31_0());
    qlonglong ave_sum_bit39_32 = ((qlonglong)(m_FPGAMeasRet->getcha_ave_sum_x_bit39_32_ave_sum_n_bit39_32()));
    qlonglong ave_sum = ave_sum_bit39_32 + ave_sum_bit31_0;
    float avg = (ave_sum/1000 - yGnd)*yInc;

    LOG_DBG() << "   " <<"ave_sum_bit31_0:  "<< ave_sum_bit31_0;
    LOG_DBG() << "   " <<"ave_sum_bit39_32:  "<< ave_sum_bit39_32;
    LOG_DBG() << "   " <<"ave_sum:  "<< ave_sum;
    LOG_DBG() << "   " <<"Vavg:  "<< avg;
    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"Vupper(F):  "<<m_FPGAMeasRet->getcha_threshold_l_mid_h_cha_threshold_h();
    LOG_DBG() << "   " <<"Vmid(F):    "<<m_FPGAMeasRet->getcha_threshold_l_mid_h_cha_threshold_mid();
    LOG_DBG() << "   " <<"Vlower(F):  "<<m_FPGAMeasRet->getcha_threshold_l_mid_h_cha_threshold_l();
    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"Overshoot(F):  "<<m_FPGAMeasRet->getcha_shoot_over_shoot();
    LOG_DBG() << "   " <<"Preshoot(F):   "<<m_FPGAMeasRet->getcha_shoot_pre_shoot();
    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"Tmax(F):  "<<m_FPGAMeasRet->getcha_vmax_xloc_vmax_xloc();
    LOG_DBG() << "   " <<"Tmin(F):  "<<m_FPGAMeasRet->getcha_vmin_xloc_vmin_xloc();
    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"RiseTime(F):  "<<m_FPGAMeasRet->getcha_rtime_rtime();
    LOG_DBG() << "   " <<"RiseTime_Valid(F):  "<<m_FPGAMeasRet->getcha_rtime_rtime_vld();
    LOG_DBG() << "   " <<"FallTime(F):  "<<m_FPGAMeasRet->getcha_ftime_ftime();
    LOG_DBG() << "   " <<"FallTime_Valid(F):  "<<m_FPGAMeasRet->getcha_ftime_ftime_vld();
    LOG_DBG() <<"-------------------------------------------------------------------------------";

    unsigned int cha_p1 = m_FPGAMeasRet->getcha_cycle_xloc_1_cycle_xloc_1();
    unsigned int cha_p2 = m_FPGAMeasRet->getcha_cycle_xloc_2_cycle_xloc_2();
    unsigned int cha_p3 = m_FPGAMeasRet->getcha_cycle_xloc_3_cycle_xloc_3();
    unsigned int cha_p4 = m_FPGAMeasRet->getcha_cycle_xloc_4_cycle_xloc_4();

    unsigned int cha_p1EdgeType = m_FPGAMeasRet->getcha_cycle_xloc_1_edge_type();
    unsigned int cha_p2EdgeType = m_FPGAMeasRet->getcha_cycle_xloc_2_edge_type();
    unsigned int cha_p3EdgeType = m_FPGAMeasRet->getcha_cycle_xloc_3_edge_type();
    unsigned int cha_p4EdgeType = m_FPGAMeasRet->getcha_cycle_xloc_4_edge_type();

    unsigned int cha_p1Valid = m_FPGAMeasRet->getcha_cycle_xloc_1_cycle_xloc_1_vld();
    unsigned int cha_p2Valid = m_FPGAMeasRet->getcha_cycle_xloc_2_cycle_xloc_2_vld();
    unsigned int cha_p3Valid = m_FPGAMeasRet->getcha_cycle_xloc_3_cycle_xloc_3_vld();
    unsigned int cha_p4Valid = m_FPGAMeasRet->getcha_cycle_xloc_4_cycle_xloc_4_vld();

    LOG_DBG() << "   " <<"cha_Period_1_2_3_4(F):    "<<cha_p1<<"  "<<cha_p2<<"  "<<cha_p3<<"  "<<cha_p4;
    LOG_DBG() << "   " <<"cha_Period_Edge_Type(F):  "<<cha_p1EdgeType<<"   "<<cha_p2EdgeType<<"   "<<cha_p3EdgeType<<"   "<<cha_p4EdgeType;
    LOG_DBG() << "   " <<"cha_Period_Valid(F):      "<<cha_p1Valid<<"   "<<cha_p2Valid<<"   "<<cha_p3Valid<<"   "<<cha_p4Valid;

    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"cha_Total_rf_num:  "<<m_FPGAMeasRet->getcha_total_rf_num_total_rf_num();
    LOG_DBG() << "   " <<"cha_Total_rf_num_last_slope:  "<<m_FPGAMeasRet->getcha_total_rf_num_slope();
    LOG_DBG() <<"-------------------------------------------------------------------------------";

    LOG_DBG() <<"-------------------------------------------------------------------------------";
    unsigned int chb_p1 = m_FPGAMeasRet->getchb_cycle_xloc_1_cycle_xloc_1();
    unsigned int chb_p2 = m_FPGAMeasRet->getchb_cycle_xloc_2_cycle_xloc_2();
    unsigned int chb_p3 = m_FPGAMeasRet->getchb_cycle_xloc_3_cycle_xloc_3();
    unsigned int chb_p4 = m_FPGAMeasRet->getchb_cycle_xloc_4_cycle_xloc_4();

    unsigned int chb_p1EdgeType = m_FPGAMeasRet->getchb_cycle_xloc_1_edge_type();
    unsigned int chb_p2EdgeType = m_FPGAMeasRet->getchb_cycle_xloc_2_edge_type();
    unsigned int chb_p3EdgeType = m_FPGAMeasRet->getchb_cycle_xloc_3_edge_type();
    unsigned int chb_p4EdgeType = m_FPGAMeasRet->getchb_cycle_xloc_4_edge_type();

    unsigned int chb_p1Valid = m_FPGAMeasRet->getchb_cycle_xloc_1_cycle_xloc_1_vld();
    unsigned int chb_p2Valid = m_FPGAMeasRet->getchb_cycle_xloc_2_cycle_xloc_2_vld();
    unsigned int chb_p3Valid = m_FPGAMeasRet->getchb_cycle_xloc_3_cycle_xloc_3_vld();
    unsigned int chb_p4Valid = m_FPGAMeasRet->getchb_cycle_xloc_4_cycle_xloc_4_vld();

    LOG_DBG() << "   " <<"chb_Period_1_2_3_4(F):    "<<chb_p1<<"  "<<chb_p2<<"  "<<chb_p3<<"  "<<chb_p4;
    LOG_DBG() << "   " <<"chb_Period_Edge_Type(F):  "<<chb_p1EdgeType<<"   "<<chb_p2EdgeType<<"   "<<chb_p3EdgeType<<"   "<<chb_p4EdgeType;
    LOG_DBG() << "   " <<"chb_Period_Valid(F):      "<<chb_p1Valid<<"   "<<chb_p2Valid<<"   "<<chb_p3Valid<<"   "<<chb_p4Valid;

    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"chb_Total_rf_num:  "<<m_FPGAMeasRet->getchb_total_rf_num_total_rf_num();
    LOG_DBG() << "   " <<"chb_Total_rf_num_last_slope:  "<<m_FPGAMeasRet->getchb_total_rf_num_slope();
    LOG_DBG() <<"-------------------------------------------------------------------------------";

    LOG_DBG() <<"-------------------------------------------------------------------------------";
    unsigned int chc_p1 = m_FPGAMeasRet->getchc_cycle_xloc_1_cycle_xloc_1();
    unsigned int chc_p2 = m_FPGAMeasRet->getchc_cycle_xloc_2_cycle_xloc_2();
    unsigned int chc_p3 = m_FPGAMeasRet->getchc_cycle_xloc_3_cycle_xloc_3();
    unsigned int chc_p4 = m_FPGAMeasRet->getchc_cycle_xloc_4_cycle_xloc_4();

    unsigned int chc_p1EdgeType = m_FPGAMeasRet->getchc_cycle_xloc_1_edge_type();
    unsigned int chc_p2EdgeType = m_FPGAMeasRet->getchc_cycle_xloc_2_edge_type();
    unsigned int chc_p3EdgeType = m_FPGAMeasRet->getchc_cycle_xloc_3_edge_type();
    unsigned int chc_p4EdgeType = m_FPGAMeasRet->getchc_cycle_xloc_4_edge_type();

    unsigned int chc_p1Valid = m_FPGAMeasRet->getchc_cycle_xloc_1_cycle_xloc_1_vld();
    unsigned int chc_p2Valid = m_FPGAMeasRet->getchc_cycle_xloc_2_cycle_xloc_2_vld();
    unsigned int chc_p3Valid = m_FPGAMeasRet->getchc_cycle_xloc_3_cycle_xloc_3_vld();
    unsigned int chc_p4Valid = m_FPGAMeasRet->getchc_cycle_xloc_4_cycle_xloc_4_vld();

    LOG_DBG() << "   " <<"chc_Period_1_2_3_4(F):    "<<chc_p1<<"  "<<chc_p2<<"  "<<chc_p3<<"  "<<chc_p4;
    LOG_DBG() << "   " <<"chc_Period_Edge_Type(F):  "<<chc_p1EdgeType<<"   "<<chc_p2EdgeType<<"   "<<chc_p3EdgeType<<"   "<<chc_p4EdgeType;
    LOG_DBG() << "   " <<"chc_Period_Valid(F):      "<<chc_p1Valid<<"   "<<chc_p2Valid<<"   "<<chc_p3Valid<<"   "<<chc_p4Valid;

    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"chc_Total_rf_num:  "<<m_FPGAMeasRet->getchc_total_rf_num_total_rf_num();
    LOG_DBG() << "   " <<"chc_Total_rf_num_last_slope:  "<<m_FPGAMeasRet->getchc_total_rf_num_slope();
    LOG_DBG() <<"-------------------------------------------------------------------------------";

    LOG_DBG() <<"-------------------------------------------------------------------------------";
    unsigned int chd_p1 = m_FPGAMeasRet->getchd_cycle_xloc_1_cycle_xloc_1();
    unsigned int chd_p2 = m_FPGAMeasRet->getchd_cycle_xloc_2_cycle_xloc_2();
    unsigned int chd_p3 = m_FPGAMeasRet->getchd_cycle_xloc_3_cycle_xloc_3();
    unsigned int chd_p4 = m_FPGAMeasRet->getchd_cycle_xloc_4_cycle_xloc_4();

    unsigned int chd_p1EdgeType = m_FPGAMeasRet->getchd_cycle_xloc_1_edge_type();
    unsigned int chd_p2EdgeType = m_FPGAMeasRet->getchd_cycle_xloc_2_edge_type();
    unsigned int chd_p3EdgeType = m_FPGAMeasRet->getchd_cycle_xloc_3_edge_type();
    unsigned int chd_p4EdgeType = m_FPGAMeasRet->getchd_cycle_xloc_4_edge_type();

    unsigned int chd_p1Valid = m_FPGAMeasRet->getchd_cycle_xloc_1_cycle_xloc_1_vld();
    unsigned int chd_p2Valid = m_FPGAMeasRet->getchd_cycle_xloc_2_cycle_xloc_2_vld();
    unsigned int chd_p3Valid = m_FPGAMeasRet->getchd_cycle_xloc_3_cycle_xloc_3_vld();
    unsigned int chd_p4Valid = m_FPGAMeasRet->getchd_cycle_xloc_4_cycle_xloc_4_vld();

    LOG_DBG() << "   " <<"chd_Period_1_2_3_4(F):    "<<chd_p1<<"  "<<chd_p2<<"  "<<chd_p3<<"  "<<chd_p4;
    LOG_DBG() << "   " <<"chd_Period_Edge_Type(F):  "<<chd_p1EdgeType<<"   "<<chd_p2EdgeType<<"   "<<chd_p3EdgeType<<"   "<<chd_p4EdgeType;
    LOG_DBG() << "   " <<"chd_Period_Valid(F):      "<<chd_p1Valid<<"   "<<chd_p2Valid<<"   "<<chd_p3Valid<<"   "<<chd_p4Valid;

    LOG_DBG() <<"-------------------------------------------------------------------------------";
    LOG_DBG() << "   " <<"chd_Total_rf_num:  "<<m_FPGAMeasRet->getchd_total_rf_num_total_rf_num();
    LOG_DBG() << "   " <<"chd_Total_rf_num_last_slope:  "<<m_FPGAMeasRet->getchd_total_rf_num_slope();
    LOG_DBG() <<"-------------------------------------------------------------------------------";

    postEngine( ENGINE_MEAS_RD_DURING_FLAG, false);
    LOG_DBG() <<"------------------------------------------ Finish------------------------------";
}

DsoErr servMeasure::set_Test()  //For FPGA Test
{
    LOG_DBG() <<"------------------------------------------ set_Test() Start------------------------------";

    m_HistoSelectItem = 0;

    void *buf = NULL;

    buf = getHistoMeas();

    if( NULL != buf)
    {
        QQueue<float>* measBuf = (QQueue<float>*)buf;
        foreach (float val, (*measBuf) )
        {
            LOG_DBG()<<" getHistoMeas(): "<<val;
        }
    }

//    setFPGA_MeasCfg();
//    setFPGA_Region();
//    getFPGA_MeasData_Test();

//    Q_ASSERT( NULL != m_pMeasureThread );
//    if( NULL != m_pMeasureThread)
//    {
//        m_pMeasureThread->start();
//    }

    LOG_DBG() <<"------------------------------------------ set_Test() Finish------------------------------";
    return ERR_NONE;
}
