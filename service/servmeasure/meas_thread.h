#ifndef SERVMEASURETHREAD_H
#define SERVMEASURETHREAD_H

#include <QtCore>

class servMeasure;

class servMeasureThread : public QThread
{
    Q_OBJECT
public:
    servMeasureThread( servMeasure *pMeasure);

public:
    virtual void run();

private:
    servMeasure *m_pServMeasure;
};

#endif // SERVMEASURETHREAD_H
