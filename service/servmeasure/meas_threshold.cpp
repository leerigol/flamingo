#include "meas_threshold.h"

ThresholdData::ThresholdData(Chan s, meas_algorithm::ThresholdType t,
                             qlonglong h, qlonglong m, qlonglong l, float base)
{
    mSrc = s;
    setType(t, h, m, l, base);
}

void ThresholdData::setType(meas_algorithm::ThresholdType t,
                            qlonglong h, qlonglong m, qlonglong l, float base)
{
    mtype = t;

    setThHigh(h, base);
    setThMid(m, base);
    setThLow(l, base);
}

void ThresholdData::setVal(qlonglong *val_Menu,
                           float *val_SOFT,
                           unsigned int *val_FPGA,
                           int *val_ScrPos,
                           qlonglong val,
                           float base)
{
    *val_Menu = val;

    if( mtype == meas_algorithm::TH_TYPE_PER)
    {
        *val_SOFT = val/100.0;
        *val_FPGA = val;
    }
    else if( mtype == meas_algorithm::TH_TYPE_ABS)
    {
        float realVal = val/base;   //! realVal: V
        dsoVert *pVert = dsoVert::getCH( mSrc);
        float ratio;
        pVert->getProbe().toReal( ratio, 1.0f);
        float realyInc = pVert->getyInc() * ratio;
        float realyGnd = pVert->getyGnd();

//        float adc = ( (realVal / realyInc) + realyGnd );
//        float scr = (adc - 128)*480/200 + 240;

        *val_SOFT = realVal/realyInc + realyGnd;   //! yAdc value: float
        *val_FPGA = qRound( *val_SOFT);            //! yAdc value: unsigned int
        *val_ScrPos  = 240 - ((*val_SOFT)-adc_center)*trace_height/200;
    }
    else
    {
        *val_SOFT = val/100.0;
        *val_FPGA = val;
    }
}

void ThresholdData::setThHigh(qlonglong h, float base)
{
    setVal( &mIntHigh_Menu, &mfHigh_SOFT, &mIntHigh_FPGA, &mHigh_SrcPos, h, base);
}

void ThresholdData::setThMid(qlonglong m, float base)
{
    setVal( &mIntMid_Menu, &mfMid_SOFT, &mIntMid_FPGA, &mMid_SrcPos, m, base);
}

void ThresholdData::setThLow(qlonglong l, float base)
{
    setVal( &mIntLow_Menu, &mfLow_SOFT, &mIntLow_FPGA, &mLow_SrcPos, l, base);
}

void ThresholdData::setDefault()
{
    if( mtype == meas_algorithm::TH_TYPE_PER)
    {
        setThHigh(90ll, 1.0f);     //! Default High(%): 90%
        setThMid(50ll, 1.0f);      //! Default Mid(%):  50%
        setThLow(10ll, 1.0f);      //! Default Low(%):  10%
    }
    else if(mtype == meas_algorithm::TH_TYPE_ABS)
    {
        mHigh_SrcPos = trace_height/vert_div * 1;  //! Default High(SrcPos): 1div(From Top)
        mMid_SrcPos  = trace_height/vert_div * 4;  //! Default Mid(SrcPos):  4div(From Top)
        mLow_SrcPos  = trace_height/vert_div * 7;  //! Default Low(SrcPos):  7div(From Top)

        mfHigh_SOFT = (240 - mHigh_SrcPos)*200*1.0f /trace_height + adc_center;
        mfMid_SOFT  = (240 - mMid_SrcPos)*200*1.0f /trace_height + adc_center;
        mfLow_SOFT  = (240 - mLow_SrcPos)*200*1.0f /trace_height + adc_center;

        mIntHigh_FPGA = qRound( mfHigh_SOFT);
        mIntMid_FPGA  = qRound( mfMid_SOFT);
        mIntLow_FPGA  = qRound( mfLow_SOFT);

        dsoVert *pVert = dsoVert::getCH( mSrc);
        double ratio;
        pVert->getProbe().toReal( ratio, 1.0);
        double realyInc = pVert->getyInc() * ratio;
        double realyGnd = pVert->getyGnd();
        float base = 1e6f;   //! base: 1uV

        mIntHigh_Menu = qRound64((mfHigh_SOFT - realyGnd) * realyInc * base);
        mIntMid_Menu  = qRound64( (mfMid_SOFT - realyGnd) * realyInc * base);
        mIntLow_Menu  = qRound64( (mfLow_SOFT - realyGnd) * realyInc * base);
//        qDebug()<<" ThresholdData::setDefault() ratio: "<<ratio;
//        qDebug()<<" ThresholdData::setDefault() realyGnd: "<<realyGnd;
//        qDebug()<<" ThresholdData::setDefault() mIntHigh_Menu: "<<mIntHigh_Menu;
    }
    else
    {
        setThHigh(90ll, 1.0f);     //! Default High(%): 90%
        setThMid(50ll, 1.0f);      //! Default Mid(%):  50%
        setThLow(10ll, 1.0f);      //! Default Low(%):  10%
    }
}
