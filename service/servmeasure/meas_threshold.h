#ifndef THRESHOLDDATA_H
#define THRESHOLDDATA_H

#include "meas_soft.h"
#include "../../include/dsostd.h"

class ThresholdData
{
public:
    ThresholdData(Chan s, meas_algorithm::ThresholdType t,
                  qlonglong h, qlonglong m, qlonglong l, float base);

    void setType(meas_algorithm::ThresholdType t, qlonglong h, qlonglong m, qlonglong l, float base);
    void setThHigh(qlonglong h, float base);
    void setThMid(qlonglong m, float base);
    void setThLow(qlonglong l, float base);

    void setDefault();

private:
    void setVal(qlonglong *val_Menu, float *val_SOFT, unsigned int *val_FPGA, int *val_ScrPos,
                qlonglong val, float base);

public:
    Chan mSrc;
    meas_algorithm::ThresholdType mtype;
    qlonglong mIntHigh_Menu;
    qlonglong mIntMid_Menu;
    qlonglong mIntLow_Menu;

    float mfHigh_SOFT;            //! For SOFT meas_algorithm
    float mfMid_SOFT;             //! For SOFT meas_algorithm
    float mfLow_SOFT;             //! For SOFT meas_algorithm

    unsigned int mIntHigh_FPGA;   //! For FPGA meas_algorithm
    unsigned int mIntMid_FPGA;    //! For FPGA meas_algorithm
    unsigned int mIntLow_FPGA;    //! For FPGA meas_algorithm

    int mHigh_SrcPos;             //! For Screen pix position
    int mMid_SrcPos;              //! For Screen pix position
    int mLow_SrcPos;              //! For Screen pix position
};

#endif // THRESHOLDDATA_H
