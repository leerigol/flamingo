#ifndef MEASDATA_H
#define MEASDATA_H

#include <QMap>
#include "../../include/dsotype.h"
#include "../../include/dsostd.h"
#include "meas_soft.h"
#include "meas_threshold.h"
#include "meas_phy.h"


using namespace DsoType;

struct MeasIndicator
{
    bool bVisible;
    int  mPos;
};

enum MeasStatus
{
    VALID,
    LOW_SIGNAL,
    GREATER_CLIPPED,
    LESS_CLIPPED,
    CLIPPED,
    NO_PERIOD,
    NO_EDGE,
    NO_SIGNAL,
    INVALID,
};

struct MeasValue
{
    float mVal;
    Unit  mUnit;
    MeasIndicator mAx;
    MeasIndicator mBx;
    MeasIndicator mAy;
    MeasIndicator mBy;
    MeasStatus mStatus;
};

enum RegionType
{
    REGION_MAIN,
    REGION_ZOOM,
    REGION_CURSOR,
};


enum MeasMode
{
    SOFT_MODE,
    FPGA_MODE,
};

class measData
{
public:
    measData(Chan src = chan1);
    virtual ~measData();

    enum CycleIndex_FPGA
    {
        CYCLE_NONE,
        CYCLE_1,
        CYCLE_2,
    };

    enum MeasDataStatus
    {
        Trace_Id_Invalid,
        MeasData_Empty,

        MeasData_Valid,
    };

public:
    void setMeasSrc(Chan src) { m_Src = src;}
    Chan getMeasSrc(void)     { return m_Src;}
    bool isValid();
    bool isValidData();
    bool isZoomOn()           { return m_ZoomOn;}

    void setRegion(RegionType rType, bool zoom, int rX1, int rX2);
    RegionType getRegionType()  { return m_RegionType;}
    int        getRangeX1()     { return m_RangeX1;}
    int        getRangeX2()     { return m_RangeX2;}

    void CalcRealMeasData_SOFT(ThresholdData *th,
                          meas_algorithm::StateMethodMode sMode,
                          meas_algorithm::StateMethod top,
                          meas_algorithm::StateMethod base);

    void CalcRealMeasData_FPGA(void *pFPGA_ret, void *adcCH, void *horInfo);

    MeasValue getRealMeasData(MeasType type);
    MeasStatus getMeasStatus(MeasType type);

    meas_algorithm::MEASURE *getRawMeasData(void)
    { return &m_RawMeasData;}

    DsoWfm *getWfm(void)
    { return m_Wfm;}

    int   getTraceId()     { return m_TraceId;}

    float getRealt0()      { return m_Realt0;}
    float getRealtInc()    { return m_RealtInc;}
    float getRealScale()   { return m_RealScale;}
    float getRealOffset()  { return m_RealOffset;}
    float getRealyInc()    { return m_RealyInc;}
    float getDataLen()     { return m_DataLen;}
    //! Get Trace X index from Cursor position
    int getTraceX(int rangeX1, HorizontalView hview);

    //! Get Pix position
    int getXPos(int index);
    int getYPos(float yAdc);

    void clearData();

private:
    void CalcRawMeasData_SOFT(ThresholdData *th,
                         meas_algorithm::StateMethodMode stateMode,
                         meas_algorithm::StateMethod topMethod,
                         meas_algorithm::StateMethod baseMethod);
    void getSrcWfmData_SOFT();
    void buildLaWfm();

    void CalcRawMeasData_FPGA(void *pFPGA_ret, void *adcCH, float yGnd);
    void PulseMeas_FPGA(float tInc, unsigned int refLoc);
    void EdgeMeas_FPGA(float tInc, unsigned int refLoc, float yGnd);
    void CountMeas_FPGA();


    void AmpMeas_FPGA(float yGnd, unsigned int dataLen);
    bool isLaCH();

    //! Calc Real MeasValue
    void calc_Period(int xPos = 0, int yPos = 0);
    void calc_Freq(int xPos = 0, int yPos = 0);
    void calc_RiseTime(int xPos = 0, int yPos = 0);
    void calc_FallTime(int xPos = 0, int yPos = 0);
    void calc_PWidth(int xPos = 0, int yPos = 0);
    void calc_NWidth(int xPos = 0, int yPos = 0);
    void calc_PDuty(int xPos = 0, int yPos = 0);
    void calc_NDuty(int xPos = 0, int yPos = 0);
    void calc_PPulses(int xPos = 0, int yPos = 0);
    void calc_NPulses(int xPos = 0, int yPos = 0);
    void calc_PEdges(int xPos = 0, int yPos = 0);
    void calc_NEdges(int xPos = 0, int yPos = 0);
    void calc_Tvmax(int xPos = 0, int yPos = 0);
    void calc_Tvmin(int xPos = 0, int yPos = 0);
    void calc_Pslew_rate(int xPos = 0, int yPos = 0);
    void calc_Nslew_rate(int xPos = 0, int yPos = 0);

    void calc_Vmax(int xPos = 0, int yPos = 0);
    void calc_Vmin(int xPos = 0, int yPos = 0);
    void calc_Vpp(int xPos = 0, int yPos = 0);
    void calc_Vtop(int xPos = 0, int yPos = 0);
    void calc_Vbase(int xPos = 0, int yPos = 0);
    void calc_Vamp(int xPos = 0, int yPos = 0);
    void calc_Vupper(int xPos = 0, int yPos = 0);
    void calc_Vmid(int xPos = 0, int yPos = 0);
    void calc_Vlower(int xPos = 0, int yPos = 0);
    void calc_Vavg(int xPos = 0, int yPos = 0);
    void calc_Vrms(int xPos = 0, int yPos = 0);
    void calc_Vrms_S(int xPos = 0, int yPos = 0);
    void calc_Overshoot(int xPos = 0, int yPos = 0);
    void calc_Preshoot(int xPos = 0, int yPos = 0);
    void calc_Area(int xPos = 0, int yPos = 0);
    void calc_Area_S(int xPos = 0, int yPos = 0);
    void calc_Variance(int xPos = 0, int yPos = 0);

    void setIndicatorVisible( MeasValue& val, bool b);
    // for Trace Data Test
    void test_ImportSrcData(unsigned char *pData, short Len);

    // for SOFT_ALgorithm Test
    void test_ReadTestData(void);


private:
    Chan     m_Src;
    MeasDataStatus     m_MeasDataStatus;

    RegionType m_RegionType;
    int m_RangeX1;
    int m_RangeX2;
    bool m_ZoomOn;

    float m_RealyInc;
    float m_RealyGnd;
    float m_RealScale;
    float m_RealOffset;
    float m_Realt0;
    float m_RealtInc;
    int   m_DataLen;
    Unit  m_ChUnit;
    bool  m_ChOnOff;

    DsoWfm *m_Wfm;
    int     m_TraceId;

    EngineHoriInfo *m_HorInfo;

    Meas_FPGA_RawData            *m_FPGARawData;
    CycleIndex_FPGA m_CycleIndex_FPGA;

    meas_algorithm::MEASURE       m_RawMeasData;
    QMap<MeasType,MeasValue>      m_RealMeasData;


    // for SOFT_ALgorithm Test
    bool     m_SOFT_Algorithm_Test;
    DsoPoint *m_pTest_Data;
    int      m_Test_DataNum;
};

#endif // MEASDATA_H
