/*****************************************************************************
                普源精电科技有限公司版权所有(2004-2010)
******************************************************************************
头文件名: DsoDrvUI_Measure.h

功能描述: Measure操作的相关函数

作    者: YULE(原始)

版    本: 1.0.1

完成日期: 07/08/18

修改历史: 

作者          修改时间          版本        修改内容
GONG          09-05-04          1.0.1       增加采样率设置,直接计算水平参数,
                                            增加周期选择
                                            增加面积、方差以及单周期的面积、有效值、平均值
                                            增加最大最小值的水平位置
                                            增加上升时间门限可调
GONG          2012-8-22         1.0.2       修改延迟测量结构体, 增加一个EdgeChan表示EdgeX1位于哪个通道上, 
*****************************************************************************/
#include <math.h>
#include <QtCore>
#include "../../include/dsodbg.h"
#include "meas_soft.h"

namespace meas_algorithm {

#define VOLT_GND_VALUE      0

#define M_VAILD_MIN			0
#define M_VAILD_MAX			255
#define M_VAILD_VPP			13

#define TB_FLAG_TOP			1
#define TB_FLAG_BASE		2
#define TB_FLAG_NONE		3

#define TB_VALID_DOTS       15	//10  //5
#define TB_FLAT_RANGE       5
#define TB_SLOPE_RANGE      0.3

#define TB_AMP_PROB_FLAT	15

#define	MEAS_AMP_RATIO		0.05 //5%: Top and Base levels each have over 5% of the total hits
#define MEAS_PERIOD_RATIO	800
#define MEAS_AMP_AVG_NUM    6
#define	MEAS_AMP_AVG_RANGE	10

// 测量的周期位置
short g_meas_s16Period = 1;

// 采样率
float g_meas_f32SaRate = 1;


// Vtop and Vbase method(Algorithms for determining state levels--IEEE Std181-2003)
StateMethodMode g_meas_eStateMethod = MODE_AUTO;

StateMethod g_meas_eTopMethod   = METHOD_MaxMin;

StateMethod g_meas_eBaseMethod  = METHOD_MaxMin;


/*******************************************************************************
 函 数 名:     SetMeasAnalysePeriod()
 描    述:	   设置测量的周期位置
 输入参数:    			                                
 输出参数:     
 			   s16Period	----- 测量周期
 返 回 值:     无	
 说    明:     最小周期数是1，如果设置超过屏幕的周期数则测量最后周期
*******************************************************************************/
void SetMeasAnalysePeriod(short s16Period)
{
    if (s16Period < 1)
    {
        s16Period = 1;
    }

    g_meas_s16Period = s16Period;
}

/*******************************************************************************
 函 数 名:     SetMeasAnalyseSaRate()
 描    述:	   设置示波器采样率
 输入参数:    			                                
 输出参数:     
 			   f32SaRate	----- 采样率
 返 回 值:     无	
 说    明:     主要是为计算水平参数
*******************************************************************************/
void SetMeasAnalyseSaRate(float f32SaRate)
{
    if( !isinf(f32SaRate) && !isnan(f32SaRate))
    {
        if (f32SaRate < 1e-9)
        {
            f32SaRate = 1e-9;
        }

        g_meas_f32SaRate = f32SaRate;
    }
    else
    {
        g_meas_f32SaRate = 1e6;
    }

//    qDebug()<<f32SaRate;
}

/*******************************************************************************
 函 数 名:     SetStateMethod()
 描    述:     Set Vtop and Vbase method
              (Algorithms for determining state levels--IEEE Std181-2003)
 输入参数:     u8StateM -----State level Method Mode
              u8TopM  -----Vtop level Method
              u8BaseM -----Vbase level Method
 输出参数:

 返 回 值:     无
 说    明:
*******************************************************************************/
void SetStateMethod(StateMethodMode eStateM, StateMethod eTopM, StateMethod eBaseM)
{
    g_meas_eStateMethod = eStateM;
    g_meas_eTopMethod   = eTopM;
    g_meas_eBaseMethod  = eBaseM;
}

float GetAutoAmpRatio( short vpp )
{
    extern short AmpRatioTable[256];
    if( vpp >= 0 && vpp < 256 )
    {
        float f = AmpRatioTable[vpp];
        return f/100;
    }

    return MEAS_AMP_RATIO;
}

/*******************************************************************************
 函 数 名:     MeasAnalyser()
 描    述:	   自动测量算法
 输入参数:    			                                
 输出参数:     
 			   pMeasData	----- 测量数据指针
 			   s16Len		----- 测量数据长度
 			   s16Gnd       ----- 波形偏移
 			   CH_M			----- 测量结构体
 返 回 值:     无	
 说    明:     5
*******************************************************************************/
//#pragma optimize_for_speed
//section("fast2_code")
void MeasAnalyser(unsigned char *pMeasData,
                  int s32Len,
                  int s32Base,
                  short s16Gnd ,
                  MeasThreshold th,
                  MEASURE *pMeasure)
{
    short s16tempmax = 0;
    short s16tempmin = 255;

    long long s64tempavg  = 0;
    long long s64temparea = 0;
    long long s64tempdev  = 0;

    int i = 0;
    int s32Start = 0;
    int s32End = 0;

    short s16temp = 0;
    short s16oldtemp = 0;

    int meas_s32amp[256] = {0};
	
    float f32ThH = 0;
    float f32ThM = 0;
    float f32ThL = 0;        // 上升、下降时间电压范围,脉宽比较位置

    int s32RisingEdgeX1 = 0;         // 第一个上升沿
    int s32RisingEdgeX2 = 0;         // 第二个上升沿
    int s32FallingEdgeX1 = 0;        // 第一个下降沿
    int s32FallingEdgeX2 = 0;        // 第二个下降沿
    int s32Edge = -1;

    float f32temprms = 0;
	
    int s16amp_top = 0;
    int s16amp_base = 0;

    int s16amp_top_num = 0;
    int s16amp_base_num = 0;

    int s16prob_vtop = 0;
    int s16prob_vbase = 0;
  
    unsigned char *pu8Data = pMeasData;

	s16oldtemp = VOLT_GND_VALUE + s16Gnd;
    pMeasure->XMin = 0;
    pMeasure->XMax = 0;
    for (i = 0; i < s32Len; i++)
	{
		s16temp = *pu8Data++;
        s64tempavg += s16temp;
        meas_s32amp[s16temp] += 1;
				
		if (s16tempmin > s16temp)
		{
			s16tempmin = s16temp;
            pMeasure->XMin = i;
		}
		else if (s16tempmax < s16temp)
		{
			s16tempmax = s16temp;
            pMeasure->XMax = i;
		}
		
		s16temp = s16oldtemp - s16temp;
        s64temparea -= s16temp;
		f32temprms += (s16temp * s16temp);
	}
/************************************ Vdev *********************************/ 
	pu8Data = pMeasData;
    s16oldtemp = s64tempavg / s32Len;
    for (i = 0; i < s32Len; i++)
	{
		s16temp = *pu8Data++;
		s16temp -= s16oldtemp;
        s64tempdev += (s16temp * s16temp);
	}    
    pMeasure->Vdev = sqrt((float)s64tempdev / s32Len);
//    pMeasure->Vdev = (float)s64tempdev / s32Len;
/************************************Vmax************************************/
	if (s16tempmin == M_VAILD_MIN)
	{
        pMeasure->bVaild_Vmin = false;
	}
	else
	{
        pMeasure->bVaild_Vmin = true;
	}
    pMeasure->Vmax = s16tempmax - (VOLT_GND_VALUE + s16Gnd);	//实际最大幅值
/************************************Vmin************************************/
	if (s16tempmax == M_VAILD_MAX)
	{
        pMeasure->bVaild_Vmax = false;
	}
	else
	{
        pMeasure->bVaild_Vmax = true;
	}
    pMeasure->Vmin = s16tempmin - (VOLT_GND_VALUE + s16Gnd);	//实际最小幅值
/************************************Vpp*************************************/
    pMeasure->Vpp = s16tempmax - s16tempmin;
    if (pMeasure->Vpp < M_VAILD_VPP)
	{
        pMeasure->bVaild_Vpp = false;
	}
	else
	{
        pMeasure->bVaild_Vpp = true;
	}
/************************************Vavg************************************/
    pMeasure->Vavg = (float)s64tempavg / s32Len - (s16Gnd + VOLT_GND_VALUE);
/************************************Vrms************************************/
    pMeasure->Vrms = sqrt(f32temprms / s32Len);
/************************************Area************************************/
    pMeasure->Area = s64temparea;
/************************************Vtop & Vbase****************************/    

    /////////////////////////////////////////////
    // 使用概率来计算幅度
    /////////////////////////////////////////////
    s16amp_top = 0;
    s16amp_base = 0;
    s16amp_top_num = 0;
    s16amp_base_num = 0;
    short region = 0, base_region = 0,top_region = 0;

    region = round((s16tempmax - s16tempmin)*0.4); //40% of Max-Min range of waveform
    LOG_DBG()<<"measAnalyzer region: "<<region;

    base_region = s16tempmin + region;
    top_region  = s16tempmax - region;

    if( g_meas_eStateMethod == MODE_MANUAL)
    {
        if( g_meas_eBaseMethod == METHOD_MaxMin)
        {
            pMeasure->Vbas = s16tempmin;
        }
        else
        {
            for( i=0; i<=base_region; i++)
            {
                if (meas_s32amp[i] > s16prob_vbase)
                {
                    s16prob_vbase = meas_s32amp[i];
                    s16amp_base = i;
                }
            }
            pMeasure->Vbas = s16amp_base;
        }
    }
    else
    {
        for( i=0; i<=base_region; i++)
        {
            if (meas_s32amp[i] > s16prob_vbase)
            {
                s16prob_vbase = meas_s32amp[i];
                s16amp_base = i;
            }
            s16amp_base_num += meas_s32amp[i];
        }

        //if( s16prob_vbase <= s16amp_base_num * MEAS_AMP_RATIO )

        if( s16prob_vbase <= s16amp_base_num * GetAutoAmpRatio(pMeasure->Vpp) )
        {
            pMeasure->Vbas = s16tempmin;
        }
        else
        {
            pMeasure->Vbas = s16amp_base;
        }
    }

    if( g_meas_eStateMethod == MODE_MANUAL)
    {
        if( g_meas_eTopMethod == METHOD_MaxMin)
        {
            pMeasure->Vtop = s16tempmax;
        }
        else
        {
            for( i= top_region; i<256; i++)
            {
                if (meas_s32amp[i] > s16prob_vtop)
                {
                    s16prob_vtop = meas_s32amp[i];
                    s16amp_top = i;
                }
            }
            pMeasure->Vtop = s16amp_top;
        }
    }
    else
    {
        for( i= top_region; i<256; i++)
        {
            if (meas_s32amp[i] > s16prob_vtop)
            {
                s16prob_vtop = meas_s32amp[i];
                s16amp_top = i;
            }
            s16amp_top_num += meas_s32amp[i];
        }

        //if( s16prob_vtop <= s16amp_top_num * MEAS_AMP_RATIO )
        if( s16prob_vtop <= s16amp_top_num * GetAutoAmpRatio(pMeasure->Vpp) )
        {
            pMeasure->Vtop = s16tempmax;
        }
        else
        {
            pMeasure->Vtop = s16amp_top;
        }
    }

    // Clean meas_s32amp
    for( i=0; i<=255; i++)
    {
        meas_s32amp[i] = 0;
    }

    LOG_DBG()<< " s16amp_base"<<s16amp_base;
    LOG_DBG()<< " s16amp_top"<<s16amp_top;
    LOG_DBG()<< " s16tempmax"<<s16tempmax;
    LOG_DBG()<< " s16tempmin"<<s16tempmin;

    pMeasure->Vamp = pMeasure->Vtop - pMeasure->Vbas;

    if( th.eThreshHoldType == TH_TYPE_PER)
    {
        f32ThH = pMeasure->Vbas + pMeasure->Vamp * th.f32ThreshHoldH;
        f32ThM = pMeasure->Vbas + pMeasure->Vamp * th.f32ThreshHoldM;
        f32ThL = pMeasure->Vbas + pMeasure->Vamp * th.f32ThreshHoldL;
    }
    else
    {
        f32ThH = th.f32ThreshHoldH;
        f32ThM = th.f32ThreshHoldM;
        f32ThL = th.f32ThreshHoldL;
    }
    /*
     * hexiaohua
     * 2018.05.25
     * 特殊处理，防止上限和中值，中值和下限太接近，无法测量出结果
     */
    {
        if( (f32ThH - f32ThM) < 6.0 )
        {
            f32ThH = f32ThM + 6;
            if( f32ThH >255.0 )
            {
                f32ThH = 255;
            }
        }

        if( (f32ThM - f32ThL) < 6.0 )
        {
            f32ThL = f32ThM - 6.0;
            if( f32ThL < 0.0  )
            {
                f32ThL = 0;
            }
        }
    }

    pMeasure->Vupper = f32ThH - (VOLT_GND_VALUE + s16Gnd);
    pMeasure->Vmid   = f32ThM - (VOLT_GND_VALUE + s16Gnd);
    pMeasure->Vlower = f32ThL - (VOLT_GND_VALUE + s16Gnd);

    pMeasure->PeriodY = f32ThM;

    pMeasure->Vtop = pMeasure->Vtop - (VOLT_GND_VALUE + s16Gnd);
    pMeasure->Vbas = pMeasure->Vbas - (VOLT_GND_VALUE + s16Gnd);



/************************************Time************************************/
  pMeasure->RiseX1 = 0;
  pMeasure->RiseX2 = 0;
  pMeasure->FallX1 = 0;
  pMeasure->FallX2 = 0;
  bool bFindCenter = false;
  int lEdge2Base = 0;
  int rEdge2Base = 0;
	    
    if (pMeasure->bVaild_Vpp == true)
	{
	    pu8Data = pMeasData;
	    s16temp = *pu8Data;
	   
        LOG_DBG()<<" s16temp = "<<s16temp;
        LOG_DBG()<<" f32ThM  = "<<f32ThM;
        LOG_DBG()<<" s16temp <= f32ThM?: "<<(s16temp <= f32ThM);
	    // 第一个沿是上升沿
        if (s16temp <= f32ThM)
	    {
	       //              F1(R1=R2)      F1(R1=R2)        F1(R1=R2)  
	       //     R2(F2=F1)        R2(F2=F1)       R2(F2=F1)  
	       //          _______         _______         _______
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       // ________|       |_______|       |_______|       |_______
	        // 先查找上升沿
            short s16last = s16temp;
            while((float)s16temp < f32ThH && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
                if( (s16temp > f32ThM)&&(s16last <= f32ThM) ) //!Cross f32ThM
                {
                    // 找到上升沿
                    s32RisingEdgeX1 = s32Edge;
                    s32RisingEdgeX2 = s32Edge;
                    // 找偏移两侧的沿
                    if (s32Edge <= s32Base)
                    {
                        pMeasure->RiseX1 = s32Edge;
                    }
                    else if (pMeasure->RiseX2 == 0)
                    {
                        pMeasure->RiseX2 = s32Edge;
                    }
                }
                s16last = s16temp;
            }
            // 数据是否结束
            if (s32Edge >= s32Len)
            {
                goto EXIT_MEAS_PERIOD;
            }

            // 查找波形中间位置附近的边沿
            while(s32Edge < s32Len)
	        {  			    	 
    			///////////////////////////////////////////////////////////////////	    			    			
    			// 查找下降沿

                while(s16temp >= f32ThM && s32Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}
    			
    			// 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}
				
    			// 找到下降沿
                // 设置双源测量边沿
                if (s32Edge <= s32Base)
                {
                    pMeasure->FallX1 = s32Edge;
                }
                else if (pMeasure->FallX2 == 0)
                {
                    pMeasure->FallX2 = s32Edge;
                }

                s32FallingEdgeX2 = s32Edge;
                s32RisingEdgeX1  = s32RisingEdgeX2;

                if (s32RisingEdgeX1 <= s32Base)     ////前一个上升沿在中间位置的左侧
                {
                    if(s32Edge <= s32Base)          //当前下降沿在中间位置的左侧
                    {}                              //继续查找下一个上升沿
                    else                            //当前下降沿在中间位置的右侧
                    {
                        bFindCenter = true;         //确定找到中间位置附近的最近两个边沿（一个上升沿、一个下降沿）
                        lEdge2Base = s32Base - s32RisingEdgeX1; //前一个上升沿到中间位置的距离
                        rEdge2Base = s32Edge - s32Base;         //当前沿到中间位置的距离
                        if(lEdge2Base <= rEdge2Base)
                        {
                            if(s32FallingEdgeX1 != 0)
                            { break;}          //////找到中间位置附近的两个下降沿，退出查找
                        }
                        else
                        {}                          //继续查找下一个上升沿
                    }
                }
                else                                ////前一个上升沿在中间位置的右侧
                {
                    if(bFindCenter)                 //已确定找到中间位置附近的最近两个边沿
                    { break;}                   //////找到中间位置附近的两个下降沿，退出查找
                    else
                    {
                        bFindCenter = true;        //确定前一个上升沿与下一个上升沿是中间位置附近的两个上升沿
                    }                             //继续查找下一个上升沿
                }
                // 必须过低值点才可以查找上升沿，防止在下降沿上波形微小噪声造成测量出错
                while((float)s16temp > f32ThL && s32Edge < s32Len)
				{
					s16temp = *pu8Data++;
                    s32Edge ++;
				} 		
				
				///////////////////////////////////////////////////////////////////				
    	        // 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			} 
    			
    			// 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}
				  			
    			// 找到上升沿
                // 设置双源测量边沿
                if (s32Edge <= s32Base)
                {
                    pMeasure->RiseX1 = s32Edge;
                }
                else if (pMeasure->RiseX2 == 0)
                {
                    pMeasure->RiseX2 = s32Edge;
                }

                s32RisingEdgeX2 = s32Edge;
                s32FallingEdgeX1 = s32FallingEdgeX2;

                if (s32FallingEdgeX1 <= s32Base)     ////前一个下降沿在中间位置的左侧
                {
                    if(s32Edge <= s32Base)          //当前上升沿在中间位置的左侧
                    {}                              //继续查找下一个下降沿
                    else                            //当前上升沿在中间位置的右侧
                    {
                        bFindCenter = true;         //确定找到中间位置附近的最近两个边沿（一个上升沿、一个下降沿）
                        lEdge2Base = s32Base - s32FallingEdgeX1; //前一个下降沿到中间位置的距离
                        rEdge2Base = s32Edge - s32Base;         //当前沿到中间位置的距离
                        if(lEdge2Base <= rEdge2Base)
                        {
                            if(s32RisingEdgeX1 != 0)
                            { break;}          //////找到中间位置附近的两个上升沿，退出查找
                        }
                        else
                        {}                          //继续查找下一个下降沿
                    }
                }
                else                                ////前一个下降沿在中间位置的右侧
                {
                    if(bFindCenter)                 //已确定找到中间位置附近的最近两个边沿
                    { break;}                   //////找到中间位置附近的两个上升沿，退出查找
                    else
                    {
                        bFindCenter = true;        //确定前一个下降沿与下一个下降沿是中间位置附近的两个下降沿
                    }                             //继续查找下一个下降沿
                }
    						    
                // 必须过高值点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while((float)s16temp < f32ThH && s32Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}
				

            } // 查找波形中间位置附近的边沿
	    }
	    // 第一个沿是下降沿
        else
        {
	       //          F2(R2=R1)        F2(R2=R1)      F2(R2=R1)        F2(R2=R1)  
	       //                R1(F1=F2)        R1(F1=F2)       R1(F1=F2)  
	       //    ________          _______         _______         _______
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |________|       |_______|       |_______|       |_______  
	        // 先查找下降沿
            short s16last = s16temp;
            while((float)s16temp > f32ThL && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
                if( (s16temp < f32ThM)&&(s16last >= f32ThM) ) //!Cross f32ThM
                {
                    // 找到下降沿
                    s32FallingEdgeX1 = s32Edge;
                    s32FallingEdgeX2 = s32Edge;
                    // 找偏移两侧的沿
                    if (s32Edge <= s32Base)
                    {
                        pMeasure->FallX1 = s32Edge;
                    }
                    else if (pMeasure->FallX2 == 0)
                    {
                        pMeasure->FallX2 = s32Edge;
                    }
                }
                s16last = s16temp;
            }
            // 数据是否结束
            if (s32Edge >= s32Len)
            {
                goto EXIT_MEAS_PERIOD;
            }

            // 查找波形中间位置附近的边沿
            while(s32Edge < s32Len)
            {
                ///////////////////////////////////////////////////////////////////
    			// 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}
    			
    			// 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}

    			// 找到上升沿
                // 设置双源测量边沿
                if (s32Edge <= s32Base)
                {
                    pMeasure->RiseX1 = s32Edge;
                }
                else if (pMeasure->RiseX2 == 0)
                {
                    pMeasure->RiseX2 = s32Edge;
                }

                s32RisingEdgeX2 = s32Edge;
                s32FallingEdgeX1 = s32FallingEdgeX2;
                if (s32FallingEdgeX1 <= s32Base)     ////前一个下降沿在中间位置的左侧
                {
                    if(s32Edge <= s32Base)          //当前上升沿在中间位置的左侧
                    {}                              //继续查找下一个下降沿
                    else                            //当前上升沿在中间位置的右侧
                    {
                        bFindCenter = true;         //确定找到中间位置附近的最近两个边沿（一个上升沿、一个下降沿）
                        lEdge2Base = s32Base - s32FallingEdgeX1; //前一个下降沿到中间位置的距离
                        rEdge2Base = s32Edge - s32Base;         //当前沿到中间位置的距离
                        if(lEdge2Base <= rEdge2Base)
                        {
                            if(s32RisingEdgeX1 != 0)
                            { break;}          //////找到中间位置附近的两个上升沿，退出查找
                        }
                        else
                        {}                          //继续查找下一个下降沿
                    }
                }
                else                                ////前一个下降沿在中间位置的右侧
                {
                    if(bFindCenter)                 //已确定找到中间位置附近的最近两个边沿
                    { break;}                   //////找到中间位置附近的两个上升沿，退出查找
                    else
                    {
                        bFindCenter = true;       //确定前一个下降沿与下一个下降沿是中间位置附近的两个下降沿
                    }                             //继续查找下一个下降沿
                }

                // 必须过高值点才可以查找下降沿，防止在下降沿上波形微小噪声造成测量出错
                while((float)s16temp < f32ThH && s32Edge < s32Len)
				{
					s16temp = *pu8Data++;
                    s32Edge ++;
				} 	
				
				///////////////////////////////////////////////////////////////////				
    	        // 查找下降沿
                while(s16temp >= f32ThM && s32Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			} 
    			
    			// 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}    		
					
    			// 找到下降沿
                // 设置双源测量边沿
                if (s32Edge <= s32Base)
                {
                    pMeasure->FallX1 = s32Edge;
                }
                else if (pMeasure->FallX2 == 0)
                {
                    pMeasure->FallX2 = s32Edge;
                }

                s32FallingEdgeX2 = s32Edge;
                s32RisingEdgeX1 = s32RisingEdgeX2;
                if (s32RisingEdgeX1 <= s32Base)     ////前一个上升沿在中间位置的左侧
                {
                    if(s32Edge <= s32Base)          //当前下降沿在中间位置的左侧
                    {}                              //继续查找下一个上升沿
                    else                            //当前下降沿在中间位置的右侧
                    {
                        bFindCenter = true;         //确定找到中间位置附近的最近两个边沿（一个上升沿、一个下降沿）
                        lEdge2Base = s32Base - s32RisingEdgeX1; //前一个上升沿到中间位置的距离
                        rEdge2Base = s32Edge - s32Base;         //当前沿到中间位置的距离
                        if(lEdge2Base <= rEdge2Base)
                        {
                            if(s32FallingEdgeX1 != 0)
                            { break;}          //////找到中间位置附近的两个下降沿，退出查找
                        }
                        else
                        {}                          //继续查找下一个上升沿
                    }
                }
                else                                ////前一个上升沿在中间位置的右侧
                {
                    if(bFindCenter)                 //已确定找到中间位置附近的最近两个边沿
                    { break;}                   //////找到中间位置附近的两个下降沿，退出查找
                    else
                    {
                        bFindCenter = true;       //确定前一个上升沿与下一个上升沿是中间位置附近的两个上升沿
                    }                             //继续查找下一个上升沿
                }
    			

    			
                // 必须过低值点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while((float)s16temp > f32ThL && s32Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}	 

            } // 查找波形中间位置附近的边沿
	    }
	}

	EXIT_MEAS_PERIOD:	

/************************************+/-Pulse Num, +/-Edge Num*******************************/  	
  pMeasure->PPulseCount = 0;
  pMeasure->NPulseCount = 0;
  pMeasure->REdgeCount = 0;
  pMeasure->FEdgeCount = 0;
  pMeasure->PPulseX1 = 0;
  pMeasure->PPulseX2 = 0;
  pMeasure->NPulseX1 = 0;
  pMeasure->NPulseX2 = 0;
  pMeasure->REdgeX1 = 0;
  pMeasure->REdgeX2 = 0;
  pMeasure->FEdgeX1 = 0;
  pMeasure->FEdgeX2 = 0;
  s32Edge = -1;
	
    if (pMeasure->bVaild_Vpp == true)
	{
	    pu8Data = pMeasData;
	    s16temp = *pu8Data;
        short s16last = s16temp;
        bool b1stLower = false;
        bool b1stMid   = false;
        bool b1stUpper = false;
        int s32tempEdge = 0;

	    // 第一个沿是上升沿
        if (s16temp <= f32ThM)
	    {
	       //              F1(R1=R2)      F1(R1=R2)        F1(R1=R2)  
	       //     R2(F2=F1)        R2(F2=F1)       R2(F2=F1)  
	       //          _______         _______         _______
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       //         |       |       |       |       |       |
	       // ________|       |_______|       |_______|       |_______

            //! 先查找第一个上升沿
            while((float)s16temp < f32ThH && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
                if( (s16temp > f32ThL)&&(s16last <= f32ThL) ) //! 找到 低值点
                {
                    //! 找到第一个低值点
                    b1stLower = true;
                }
                if( (s16temp > f32ThM)&&(s16last <= f32ThM))  //! 找到 中值点
                {
                    //! 找到第一个中值点
                    b1stMid = true;
                    s32tempEdge = s32Edge;
                }
                s16last = s16temp;
            }
            //! 数据是否结束
            if (s32Edge >= s32Len)
            {
                goto EXIT_MEAS_PULSE_NUM;
            }
            else
            {
                //! 找到第一个高值点
                b1stUpper = true;
                if( b1stLower && b1stMid)
                {
                    //! 找到第一个上升沿
                    pMeasure->PPulseX1 = s32tempEdge;
                    pMeasure->REdgeX1  = s32tempEdge;
                    pMeasure->REdgeCount += 1;
                }
            }
            ///////////////////////////////////////////////////////////////////
            while(s32Edge < s32Len)
	        {  			    	 
                //! 查找下降沿
                while(s16temp >= f32ThM && s32Edge < s32Len)  //! 查找 中值点
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}
    			
                //! 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}

                s32tempEdge = s32Edge;

                while((float)s16temp > f32ThL && s32Edge < s32Len)  //! 查找 低值点
				{
					s16temp = *pu8Data++;
                    s32Edge ++;
				} 
				
                if (s32Edge < s32Len)
				{
                    if( pMeasure->PPulseX1 != 0)
                    {
                        pMeasure->PPulseCount += 1;
                        pMeasure->PPulseX2 = s32tempEdge;
                    }

                    if (pMeasure->NPulseX1 == 0)
                    {
                        pMeasure->NPulseX1 = s32tempEdge;
                    }

                    pMeasure->FEdgeCount += 1;
                    if (pMeasure->FEdgeX1 == 0)
                    {
                        pMeasure->FEdgeX1 = s32tempEdge;
                    }
                    else
                    {
                        pMeasure->FEdgeX2 = s32tempEdge;
                    }
				}				
				///////////////////////////////////////////////////////////////////				
                //! 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)  //! 查找 中值点
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			} 
    			
                //! 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}
				
                s32tempEdge = s32Edge;

                while((float)s16temp < f32ThH && s32Edge < s32Len)  //! 查找 高值点
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}
				
                if (s32Edge < s32Len)
				{
                    if( pMeasure->NPulseX1 != 0)
                    {
                        pMeasure->NPulseCount += 1;
                    }
                    pMeasure->NPulseX2 = s32tempEdge;

                    if (pMeasure->PPulseX1 == 0)
                    {
                        pMeasure->PPulseX1 = s32tempEdge;
                    }

                    pMeasure->REdgeCount += 1;
                    if (pMeasure->REdgeX1 == 0)
                    {
                        pMeasure->REdgeX1 = s32tempEdge;
                    }
                    else
                    {
                        pMeasure->REdgeX2 = s32tempEdge;
                    }
				}    	
				
            } //! while(s32Edge < s32Len)
	    }
        //! 第一个沿是下降沿
        else
        {
	       //          F2(R2=R1)        F2(R2=R1)      F2(R2=R1)        F2(R2=R1)  
	       //                R1(F1=F2)        R1(F1=F2)       R1(F1=F2)  
	       //    ________          _______         _______         _______
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |        |       |       |       |       |       |
	       //            |________|       |_______|       |_______|       |_______			

            //! 先查找第一个下降沿
            while((float)s16temp > f32ThL && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
                if( (s16temp < f32ThH)&&(s16last >= f32ThH) ) //! 找到 高值点
                {
                    //! 找到第一个高值点
                    b1stUpper = true;
                }
                if( (s16temp < f32ThM)&&(s16last >= f32ThM))  //! 找到 中值点
                {
                    //! 找到第一个中值点
                    b1stMid = true;
                    s32tempEdge = s32Edge;
                }
                s16last = s16temp;
            }
            //! 数据是否结束
            if (s32Edge >= s32Len)
            {
                goto EXIT_MEAS_PULSE_NUM;
            }
            else
            {
                //! 找到第一个低值点
                b1stLower = true;
//                qDebug()<< "measAnalyzer b1stUpper:"<<b1stUpper;
//                qDebug()<< "measAnalyzer b1stMid:"<<b1stMid;
                if( b1stUpper && b1stMid)
                {
                    //! 找到第一个下降沿
                    pMeasure->NPulseX1 = s32tempEdge;
                    pMeasure->FEdgeX1  = s32tempEdge;
                    pMeasure->FEdgeCount += 1;
                }
            }

            ////////////////////////////////////////////////////////////////////////
            while(s32Edge < s32Len)
	        {    		   			
                //! 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)  //! 查找 中值点
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}
    			
                //! 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}
				
                s32tempEdge = s32Edge;

                while((float)s16temp < f32ThH && s32Edge < s32Len)  //! 查找 高值点
				{
					s16temp = *pu8Data++;
                    s32Edge ++;
				} 	
				
                if (s32Edge < s32Len)
				{
                    if( pMeasure->NPulseX1 != 0)
                    {
                        pMeasure->NPulseCount += 1;
                    }
                    pMeasure->NPulseX2 = s32tempEdge;

                    if (pMeasure->PPulseX1 == 0)
                    {
                        pMeasure->PPulseX1 = s32tempEdge;
                    }

                    pMeasure->REdgeCount += 1;
                    if (pMeasure->REdgeX1 == 0)
                    {
                        pMeasure->REdgeX1 = s32tempEdge;
                    }
                    else
                    {
                        pMeasure->REdgeX2 = s32tempEdge;
                    }
				}
				///////////////////////////////////////////////////////////////////				
                //! 查找下降沿
                while(s16temp >= f32ThM && s32Edge < s32Len)  //! 查找 中值点
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			} 
    			
                //! 数据是否结束
                if (s32Edge >= s32Len)
    			{
    				break;
    			}    		
				
                s32tempEdge = s32Edge;
				
                while((float)s16temp > f32ThL && s32Edge < s32Len)  //! 查找 低值点
    			{
    				s16temp = *pu8Data++;
                    s32Edge ++;
    			}	 
				
                if (s32Edge < s32Len)
				{
                    if( pMeasure->PPulseX1 != 0)
                    {
                        pMeasure->PPulseCount += 1;
                    }
                    pMeasure->PPulseX2 = s32tempEdge;

                    if (pMeasure->NPulseX1 == 0)
                    {
                        pMeasure->NPulseX1 = s32tempEdge;
                    }

                    pMeasure->FEdgeCount += 1;
                    if (pMeasure->FEdgeX1 == 0)
                    {
                        pMeasure->FEdgeX1 = s32tempEdge;
                    }
                    else
                    {
                        pMeasure->FEdgeX2 = s32tempEdge;
                    }
				}		 

            } //! while(s32Edge < s32Len)
	    }
	}
	
	EXIT_MEAS_PULSE_NUM:
	
/************************************Overshoot*******************************/    
    
	s16tempmin = 255;
	s16tempmax = 0;
    
	//先降后升
    if (s32RisingEdgeX1 > s32FallingEdgeX1 && s32FallingEdgeX1 != 0)
	{
        s32Start = s32FallingEdgeX1;
        s32End = s32FallingEdgeX1 + (s32RisingEdgeX1 - s32FallingEdgeX1) / 2;
		
        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmin > s16temp)
			{
				s16tempmin = s16temp;
			}
		}
		
        pMeasure->VoverY1 = pMeasure->Vbas;
        pMeasure->VoverY2 = s16tempmin - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->Vover = fabs((pMeasure->VoverY1 - pMeasure->VoverY2) / pMeasure->Vamp);
	}
	// 先升后降
    else if (s32FallingEdgeX1 > s32RisingEdgeX1 && s32RisingEdgeX1 != 0)
	{
        s32Start = s32RisingEdgeX1;
        s32End = s32RisingEdgeX1 + (s32FallingEdgeX1 - s32RisingEdgeX1) / 2;

        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmax < s16temp)
			{
				s16tempmax = s16temp;
			}
		}
		
        pMeasure->VoverY1 = s16tempmax - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->VoverY2 = pMeasure->Vtop;
        pMeasure->Vover = fabs((pMeasure->VoverY1 - pMeasure->VoverY2) / pMeasure->Vamp);
	}
	//只有上升沿
    else if (s32RisingEdgeX1 != 0 && s32FallingEdgeX1 == 0)
	{
        s32Start = s32RisingEdgeX1;
        s32End = s32RisingEdgeX1 + (s32Len - s32RisingEdgeX1) / 2;

        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmax < s16temp)
			{
				s16tempmax = s16temp;
			}
		}

        pMeasure->VoverY1 = s16tempmax - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->VoverY2 = pMeasure->Vtop;
        pMeasure->Vover = fabs((pMeasure->VoverY1 - pMeasure->VoverY2) / pMeasure->Vamp);
	}
	//只有下降沿
    else if (s32RisingEdgeX1 == 0 && s32FallingEdgeX1 != 0)
	{
        s32Start = s32FallingEdgeX1;
        s32End = s32FallingEdgeX1 + (s32Len - s32FallingEdgeX1) / 2;

        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmin > s16temp)
			{
				s16tempmin = s16temp;
			}
		}

        pMeasure->VoverY1 = pMeasure->Vbas;
        pMeasure->VoverY2 = s16tempmin - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->Vover = fabs((pMeasure->VoverY1 - pMeasure->VoverY2) / pMeasure->Vamp);
	}
	else
	{
        pMeasure->Vover = 0.0;
	}
/************************************Preshoot********************************/
	s16tempmin = 255;
	s16tempmax = 0;

	//先降后升
    if (s32RisingEdgeX1 > s32FallingEdgeX1 && s32FallingEdgeX1 != 0)
	{
        s32Start = s32FallingEdgeX1 + (s32RisingEdgeX1 - s32FallingEdgeX1) / 2;
        s32End = s32RisingEdgeX1;

        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmin > s16temp)
			{
				s16tempmin = s16temp;
			}
		}

        pMeasure->VpreY1 = pMeasure->Vbas;
        pMeasure->VpreY2 = s16tempmin - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->Vpre = fabs((pMeasure->VpreY1 - pMeasure->VpreY2) / pMeasure->Vamp);
	}
	//先升后降
    else if (s32FallingEdgeX1 > s32RisingEdgeX1 && s32RisingEdgeX1 != 0)
	{
        s32Start = s32RisingEdgeX1 + (s32FallingEdgeX1 - s32RisingEdgeX1) / 2;
        s32End = s32FallingEdgeX1;

        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmax < s16temp)
			{
				s16tempmax = s16temp;
			}
		}

        pMeasure->VpreY1 = s16tempmax - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->VpreY2 = pMeasure->Vtop;
        pMeasure->Vpre = fabs((pMeasure->VpreY1 - pMeasure->VpreY2) / pMeasure->Vamp);
	}
	//只有上升沿
    else if (s32RisingEdgeX1 != 0 && s32FallingEdgeX1 == 0)
	{
        s32Start = s32RisingEdgeX2 / 2;
        s32End = s32RisingEdgeX2;

        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmin > s16temp)
			{
				s16tempmin = s16temp;
			}
		}

        pMeasure->VpreY1 = pMeasure->Vbas;
        pMeasure->VpreY2 = s16tempmin - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->Vpre = fabs((pMeasure->VpreY1 - pMeasure->VpreY2) / pMeasure->Vamp);
	}
	//只有下降沿
    else if (s32RisingEdgeX1 == 0 && s32FallingEdgeX1 != 0)
	{
        s32Start = s32FallingEdgeX2 / 2;
        s32End = s32FallingEdgeX2;

        for (i=s32Start;i<s32End;i++)
		{
			s16temp = (short)pMeasData[i];
			if (s16tempmax < s16temp)
			{
				s16tempmax = s16temp;
			}
		}

        pMeasure->VpreY1 = s16tempmax - (s16Gnd + VOLT_GND_VALUE);
        pMeasure->VpreY2 = pMeasure->Vtop;
        pMeasure->Vpre = fabs((pMeasure->VpreY1 - pMeasure->VpreY2) / pMeasure->Vamp);
	}
	else
	{
        pMeasure->Vpre = 0.0;
	}
	
/************************************ Period ********************************/	
	// 计算周期，周期为两个上升沿之差
    if ((s32RisingEdgeX2 > s32RisingEdgeX1) && (s32RisingEdgeX2 > s32FallingEdgeX1) &&(s32RisingEdgeX1 != 0))
	{		
        pMeasure->Period = (s32RisingEdgeX2 - s32RisingEdgeX1) / g_meas_f32SaRate;
        LOG_DBG()<<"s32RisingEdgeX2: "<<s32RisingEdgeX2;
        LOG_DBG()<<"s32RisingEdgeX1: "<<s32RisingEdgeX1;
        LOG_DBG()<<"s32RisingEdgeX2 - s32RisingEdgeX1 = "<<s32RisingEdgeX2 - s32RisingEdgeX1;
        LOG_DBG()<<"g_meas_f32SaRate: "<<g_meas_f32SaRate;

        pMeasure->PeriodX1 = s32RisingEdgeX1;
        pMeasure->PeriodX2 = s32RisingEdgeX2;
	}
	// 计算周期，周期为两个下降沿之差
    else if ((s32FallingEdgeX2 > s32FallingEdgeX1) && (s32FallingEdgeX2 > s32RisingEdgeX1) &&(s32FallingEdgeX1 != 0))
	{
        pMeasure->Period = (s32FallingEdgeX2 - s32FallingEdgeX1) / g_meas_f32SaRate;
        pMeasure->PeriodX1 = s32FallingEdgeX1;
        pMeasure->PeriodX2 = s32FallingEdgeX2;
        LOG_DBG()<<"s32FallingEdgeX2: "<<s32FallingEdgeX2;
        LOG_DBG()<<"s32FallingEdgeX1: "<<s32FallingEdgeX1;
        LOG_DBG()<<"s32FallingEdgeX2 - s32FallingEdgeX1 = "<<s32FallingEdgeX2 - s32FallingEdgeX1;
        LOG_DBG()<<"g_meas_f32SaRate: "<<g_meas_f32SaRate;
	}
	else
	{
        pMeasure->Period = 0;
	}
/************************************ +Width ********************************/	
    // 计算正脉宽,正脉宽为下降沿减去上升沿
//    qDebug()<<"measAnalyzer  Width  s32FallingEdgeX1 = "<<s32FallingEdgeX1;
//    qDebug()<<"measAnalyzer  Width  s32FallingEdgeX2 = "<<s32FallingEdgeX2;
//    qDebug()<<"measAnalyzer  Width  s32RisingEdgeX1 = "<<s32RisingEdgeX1;
//    qDebug()<<"measAnalyzer  Width  s32RisingEdgeX2 = "<<s32RisingEdgeX2;
    if ((s32FallingEdgeX1 > s32RisingEdgeX1) && (s32RisingEdgeX1 != 0))
    {						
        pMeasure->Width_P = (s32FallingEdgeX1 - s32RisingEdgeX1) / g_meas_f32SaRate;
        pMeasure->WidthPX1 = s32RisingEdgeX1;
        pMeasure->WidthPX2 = s32FallingEdgeX1;
    }
    else if((s32FallingEdgeX2 > s32RisingEdgeX1) && (s32RisingEdgeX1 != 0))
    {
        pMeasure->Width_P = (s32FallingEdgeX2 - s32RisingEdgeX1) / g_meas_f32SaRate;
        pMeasure->WidthPX1 = s32RisingEdgeX1;
        pMeasure->WidthPX2 = s32FallingEdgeX2;
    }
    else
    {
        pMeasure->Width_P = 0;
    }

/************************************ -Width ********************************/	
    // 计算负脉宽，负脉宽为上升沿与下降沿之差
    if ((s32RisingEdgeX2 > s32FallingEdgeX1) && (s32FallingEdgeX1 != 0))
    {
        pMeasure->Width_N = (s32RisingEdgeX2 - s32FallingEdgeX1) / g_meas_f32SaRate;
        pMeasure->WidthNX1 = s32FallingEdgeX1;
        pMeasure->WidthNX2 = s32RisingEdgeX2;
    }
    else if((s32RisingEdgeX1 > s32FallingEdgeX1) && (s32FallingEdgeX1 != 0))
    {
        pMeasure->Width_N = (s32RisingEdgeX1 - s32FallingEdgeX1) / g_meas_f32SaRate;
        pMeasure->WidthNX1 = s32FallingEdgeX1;
        pMeasure->WidthNX2 = s32RisingEdgeX1;
    }
    else
    {
        pMeasure->Width_N = 0;
    }
/************************************Rise Time*******************************/

    pMeasure->RiseTime = 0;

    if(  (s32RisingEdgeX1 > 0)&&(s32RisingEdgeX1 < s32Len)
       &&(s32RisingEdgeX2 > 0)&&(s32RisingEdgeX2 < s32Len) )
    {
        if( qAbs(s32Base - s32RisingEdgeX1) <= qAbs(s32RisingEdgeX2 - s32Base) )
        {
            // 以s32RisingEdgeX1为基准查找RiseTimeX1，RiseTimeX2
            MeasRiseTime( pMeasData, s32Len, s16Gnd, s32RisingEdgeX1, pMeasure);
        }
        else
        {
            // 以s32RisingEdgeX2为基准查找RiseTimeX1，RiseTimeX2
            MeasRiseTime( pMeasData, s32Len, s16Gnd, s32RisingEdgeX2, pMeasure);
        }
    }
    else if( (s32RisingEdgeX1 > 0)&&(s32RisingEdgeX1 < s32Len))
    {
        // 以s32RisingEdgeX1为基准查找RiseTimeX1，RiseTimeX2
        MeasRiseTime( pMeasData, s32Len, s16Gnd, s32RisingEdgeX1, pMeasure);
    }
    else if( (s32RisingEdgeX2 > 0)&&(s32RisingEdgeX2 < s32Len))
    {
        // 以s32RisingEdgeX2为基准查找RiseTimeX1，RiseTimeX2
        MeasRiseTime( pMeasData, s32Len, s16Gnd, s32RisingEdgeX2, pMeasure);
    }
    else
    {
        pMeasure->RiseTime = 0;
        pMeasure->RiseTimeY1 = 0;
        pMeasure->RiseTimeY2 = 0;
    }

/************************************Fall TIme*******************************/
	
    pMeasure->FallTime = 0;
    if(  (s32FallingEdgeX1 > 0)&&(s32FallingEdgeX1 < s32Len)
       &&(s32FallingEdgeX2 > 0)&&(s32FallingEdgeX2 < s32Len) )
    {
        if( qAbs(s32Base - s32FallingEdgeX1) <= qAbs(s32FallingEdgeX2 - s32Base) )
        {
            // 以s32FallingEdgeX1为基准查找FallTimeX1，FallTimeX2
            MeasFallTime( pMeasData, s32Len, s16Gnd, s32FallingEdgeX1, pMeasure);
        }
        else
        {
            // 以s32FallingEdgeX2为基准查找FallTimeX1，FallTimeX2
            MeasFallTime( pMeasData, s32Len, s16Gnd, s32FallingEdgeX2, pMeasure);
        }
    }
    else if( (s32FallingEdgeX1 > 0)&&(s32FallingEdgeX1 < s32Len))
    {
        // 以s32FallingEdgeX1为基准查找FallTimeX1，FallTimeX2
        MeasFallTime( pMeasData, s32Len, s16Gnd, s32FallingEdgeX1, pMeasure);
    }
    else if( (s32FallingEdgeX2 > 0)&&(s32FallingEdgeX2 < s32Len))
    {
        // 以s32FallingEdgeX2为基准查找FallTimeX1，FallTimeX2
        MeasFallTime( pMeasData, s32Len, s16Gnd, s32FallingEdgeX2, pMeasure);
    }
    else
    {
        pMeasure->FallTime = 0;
        pMeasure->FallTimeY1 = 0;
        pMeasure->FallTimeY2 = 0;
    }
	
/************************************Single Period****************************/
    pMeasure->Area_S = 0;
    pMeasure->Vrms_S = 0;
    pMeasure->Vavg_S = 0;
    s64tempavg = 0;
    f32temprms = 0;
    s64temparea = 0;
    
    if (pMeasure->Period != 0)
	{
	    s16oldtemp = VOLT_GND_VALUE + s16Gnd;
        pu8Data = pMeasData + pMeasure->PeriodX1;
        for (i = pMeasure->PeriodX1; i < pMeasure->PeriodX2; i++)
        {
            s16temp = *pu8Data++;
            s64tempavg += s16temp;
            
            s16temp = s16oldtemp- s16temp;
            s64temparea -= s16temp;
            f32temprms += (s16temp * s16temp);	       
        }
        
        pMeasure->Area_S = s64temparea;
        pMeasure->Vrms_S = sqrt((float)f32temprms / (pMeasure->PeriodX2-pMeasure->PeriodX1));
        pMeasure->Vavg_S = (float)s64tempavg / (pMeasure->PeriodX2-pMeasure->PeriodX1)
                           - (s16Gnd+VOLT_GND_VALUE);
            
	}
		
}

/*****上升时间测量算法*************************************************************/
void MeasRiseTime(unsigned char *pMeasData, int s32Len, short s16Gnd, int s32RiseEdge, MEASURE *pMeasure)
{
    float f32ThL = 0.0;
    float f32ThH = 0.0;
    short currData = 0;
    // 以s32RiseEdge为基准查找RiseTimeX1，RiseTimeX2
    // 查找上升沿10%点
    f32ThL = pMeasure->Vlower + (VOLT_GND_VALUE + s16Gnd);
    pMeasure->RiseTimeX1 = s32RiseEdge;
    do {
        pMeasure->RiseTimeX1 --;
        currData = (short)pMeasData[pMeasure->RiseTimeX1];
    } while(currData > f32ThL && pMeasure->RiseTimeX1 > 0);
    // 查找上升沿90%点
    f32ThH = pMeasure->Vupper + (VOLT_GND_VALUE + s16Gnd);
    pMeasure->RiseTimeX2 = s32RiseEdge;
    do {
        pMeasure->RiseTimeX2 ++;
        currData = (short)pMeasData[pMeasure->RiseTimeX2];
    } while(currData < f32ThH && pMeasure->RiseTimeX2 < s32Len);

    if(pMeasure->RiseTimeX1 <= 0 || pMeasure->RiseTimeX2 >= s32Len)
    {
        pMeasure->RiseTime = 0;
    }
    else
    {
        pMeasure->RiseTime = (pMeasure->RiseTimeX2 - pMeasure->RiseTimeX1) / g_meas_f32SaRate;
    }

    pMeasure->RiseTimeY1 = pMeasure->Vlower;
    pMeasure->RiseTimeY2 = pMeasure->Vupper;
}

/*****下降时间测量算法*************************************************************/
void MeasFallTime(unsigned char *pMeasData, int s32Len, short s16Gnd, int s32FallEdge, MEASURE *pMeasure)
{
    float f32ThL = 0.0;
    float f32ThH = 0.0;
    short currData = 0;
    // 以s32FallEdge为基准查找FallTimeX1，FalllTimeX2
    // 查找下降沿90%点
    f32ThH = pMeasure->Vupper + (VOLT_GND_VALUE + s16Gnd);
    pMeasure->FallTimeX1 = s32FallEdge;
    do {
        pMeasure->FallTimeX1 --;
        currData = (short)pMeasData[pMeasure->FallTimeX1];
    } while(currData < f32ThH && pMeasure->FallTimeX1 > 0);
    // 查找下降沿10%点
    f32ThL = pMeasure->Vlower + (VOLT_GND_VALUE + s16Gnd);
    pMeasure->FallTimeX2 = s32FallEdge;
    do {
        pMeasure->FallTimeX2 ++;
        currData = (short)pMeasData[pMeasure->FallTimeX2];
    } while(currData >= f32ThL && pMeasure->FallTimeX2 < s32Len);

    if(pMeasure->FallTimeX1 <= 0 || pMeasure->FallTimeX2 >= s32Len)
    {
        pMeasure->FallTime = 0;
    }
    else
    {
        pMeasure->FallTime = (pMeasure->FallTimeX2 - pMeasure->FallTimeX1) / g_meas_f32SaRate;
    }
    pMeasure->FallTimeY1 = pMeasure->Vupper;
    pMeasure->FallTimeY2 = pMeasure->Vlower;
}

/*******************************************************************************
 函 数 名:     MeasUpaPowerQ()
 描    述:	   UPA Power Quality Measure
 输入参数:
 输出参数:
               pSrcData   	----- 测量源数据指针
               s16Gnd       ----- 波形偏移
               FreqRef      ----- Power Quality Frequency Ref. Measure Data
               pMeasure			----- 测量结构体
 返 回 值:     无
 说    明:
*******************************************************************************/
void UpaPowerQAnalyser(unsigned char *pSrcData, int s32Len, short s16Gnd, MEASURE *FreqRef, MeasPowerQ *powerQ)
{
    powerQ->PowerQ_MAX = 0;
    powerQ->PowerQ_RMS = 0;
    powerQ->PowerQ_AVG = 0;
    short s16temp = 0;
    short s16tempmax = 0;
    float f32temprms = 0;
    long long s64tempavg = 0;
    unsigned char *pu8Data = pSrcData;

//    qDebug() << "UpaPowerQAnalyser  FreqRef->Period: "<<FreqRef->Period;
//    qDebug() << "UpaPowerQAnalyser  s32Len: "<<s32Len;
//    qDebug() << "UpaPowerQAnalyser  FreqRef->REdgeX2: "<<FreqRef->REdgeX2;

    if (FreqRef->Period != 0)
    {
        if( s32Len > FreqRef->REdgeX2)
        {
            pu8Data = pSrcData + FreqRef->REdgeX1;
            for (int i = FreqRef->REdgeX1; i < FreqRef->REdgeX2; i++)
            {
                s16temp = *pu8Data++;
                s64tempavg += s16temp;
                if(s16tempmax < s16temp)
                {
                    s16tempmax = s16temp;
                }

                s16temp = s16Gnd- s16temp;
                f32temprms += (s16temp * s16temp);
            }

            LOG_DBG() << "  FreqRef->REdgeX1 = " << FreqRef->REdgeX1;
            LOG_DBG() << "  FreqRef->REdgeX2 = " << FreqRef->REdgeX2;
            LOG_DBG() << "  s64tempavg = " << s64tempavg;
            LOG_DBG() << "  Data = " << pu8Data;
            LOG_DBG() << "  Data[0] = " << *pu8Data;
            powerQ->PowerQ_MAX = s16tempmax - (VOLT_GND_VALUE + s16Gnd);
            powerQ->PowerQ_RMS = sqrt((float)f32temprms / (FreqRef->REdgeX2-FreqRef->REdgeX1));
            powerQ->PowerQ_AVG = (float)s64tempavg / (FreqRef->REdgeX2-FreqRef->REdgeX1 + 1)
                    - (VOLT_GND_VALUE + s16Gnd);
//            qDebug() << "UpaPowerQAnalyser  PowerQ_AVG = " << powerQ->PowerQ_AVG;
        }
    }
}

/*******************************************************************************
 函 数 名:     UpaRippleAnalyser()
 描    述:	  UPA Ripple Analyser
 输入参数:
 输出参数:
               pData 	    ----- 纹波源数据
               pMeasure  	----- 纹波测量结果结构体
 返 回 值:     无
 说    明:
*******************************************************************************/
void UpaRippleAnalyser(unsigned char *pData, int s32Len, short s16Gnd, MeasRipple *pRippleVal)
{
    unsigned char *pu8Data = pData;
    short s16temp = 0;
    short s16tempmax = 0, s16tempmin = 255;

    for (int i = 0; i < s32Len; i++)
    {
        s16temp = *pu8Data++;
        if (s16tempmin > s16temp)
        {
            s16tempmin = s16temp;
        }
        else if (s16tempmax < s16temp)
        {
            s16tempmax = s16temp;
        }
    }

    if (s16tempmin == M_VAILD_MIN)
    {
        pRippleVal->bValid_Vmin = false;
    }
    else
    {
        pRippleVal->bValid_Vmin = true;
    }
    pRippleVal->Vmin = s16tempmin - s16Gnd;

    if (s16tempmax == M_VAILD_MAX)
    {
        pRippleVal->bValid_Vmax = false;
    }
    else
    {
        pRippleVal->bValid_Vmax = true;
    }
    pRippleVal->Vmax = s16tempmax - s16Gnd;

    pRippleVal->Ripple = s16tempmax - s16tempmin;
    if(pRippleVal->Ripple < M_VAILD_VPP)
    {
        pRippleVal->bValid_Ripple = false;
    }
    else
    {
        pRippleVal->bValid_Ripple = true;
    }
}

/*******************************************************************************
 函 数 名:     MeasDelayRR()
 描    述:	   自动测量延迟
 输入参数:    			                                
 输出参数:     
 			   pData	    ----- 测量数据指针
 			   s16Len		----- 测量数据长度
 			   pCH_M		----- 测量结构体
 返 回 值:     无	
 说    明:     
*******************************************************************************/
short MeasDelayRR(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE *pCH_M1,
                 MEASURE *pCH_M2,
                 MEASURE_DLY *pCH_DLY)
{
    int s32Edge1 = 0, s132Edge2 = 0;
    short s16temp;
    int s16Edge = 0;
    short s16Gnd;
    short s16BaseCH1 = 1;
    short s16tempmin,s16tempmax;
    unsigned char *pu8Data;
    MEASURE *pMeasure;
    int s16BaseOrg = s32Base;
    
    // 查找CH1离Base较近的一个边沿
    if (pCH_M1->RiseX1 != 0 && pCH_M1->RiseX2 != 0)
    {
        if (s32Base - pCH_M1->RiseX1 > pCH_M1->RiseX2 - s32Base)
        {
            s32Edge1 = pCH_M1->RiseX2;
        }
        else
        {
            s32Edge1 = pCH_M1->RiseX1;
        }
    }
    else if (pCH_M1->RiseX1 != 0)
    {
        s32Edge1 = pCH_M1->RiseX1;
    }
    else if (pCH_M1->RiseX2 != 0)
    {
        s32Edge1 = pCH_M1->RiseX2;
    }
    
    // 查找CH2离Base较远的一个边沿
    if (pCH_M2->RiseX1 != 0 && pCH_M2->RiseX2 != 0)
    {
        if (s32Base - pCH_M2->RiseX1 > pCH_M2->RiseX2 - s32Base)
        {
            s132Edge2 = pCH_M2->RiseX2;
        }
        else
        {
            s132Edge2 = pCH_M2->RiseX1;
        }
    }
    else if (pCH_M2->RiseX1 != 0)
    {
        s132Edge2 = pCH_M2->RiseX1;
    }
    else if (pCH_M2->RiseX2 != 0)
    {
        s132Edge2 = pCH_M2->RiseX2;
    }  

    // 以离Base较远的沿作为基准
    if ( abs(s32Edge1 - s32Base) > abs(s132Edge2 - s32Base))
    {
        if (s32Edge1 != 0)
        {
            s32Base = s32Edge1;
            pu8Data = pData2;
            pMeasure = pCH_M2;
            s16Gnd = s16Gnd2;
            s16BaseCH1 = 1;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        if (s132Edge2 != 0)
        {
            s32Base = s132Edge2;
            pu8Data = pData1;
            pMeasure = pCH_M1;
            s16Gnd = s16Gnd1;
            s16BaseCH1 = -1;
        }
        else
        {
            return -1;
        }
    }
    
    // 查找Base两边的上升沿
    s32Edge1 = 0;
    s132Edge2 = 0;
//	s16tempmax = pMeasure->Vtop + (VOLT_GND_VALUE + s16Gnd) - pMeasure->Vamp / 5;
//	s16tempmin = pMeasure->Vbas + (VOLT_GND_VALUE + s16Gnd) + pMeasure->Vamp / 5;
    s16tempmax = pMeasure->Vupper + (VOLT_GND_VALUE + s16Gnd);
    s16tempmin = pMeasure->Vlower + (VOLT_GND_VALUE + s16Gnd);

    float f32ThM = pMeasure->PeriodY;

	if (pMeasure->bVaild_Vpp == true)
	{
	    s16temp = *pu8Data;
	    
	    // 第一个沿是上升沿
        if (s16temp <= f32ThM)
	    {
	        // 先查找上升沿
            while(s16temp <= f32ThM && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			} 
			
			// 数据是否结束
            if (s16Edge >= s32Len)
			{
				return 0;
			}
			    						
			// 找偏移两侧的沿
            if (s16Edge <= s32Base)
			{
                s32Edge1 = s16Edge;
			}
            else if (s132Edge2 == 0)
			{
                s132Edge2 = s16Edge;
			    goto DLY_END;
			}
				          			
			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp < s16tempmax && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			}
    			
			// 查找指定周期
            while(s16Edge < s32Len)
	        {  			    	          
    			///////////////////////////////////////////////////////////////////	    			    			
    			// 查找下降沿
                while(s16temp >= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			}
    			 			
				// 必须过底端点才可以查找上升沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s16Edge < s32Len)
				{
					s16temp = *pu8Data++;
					s16Edge ++;
				} 
				
				///////////////////////////////////////////////////////////////////				
    	        // 查找上升沿
                while(s16temp <= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			} 
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			}
    			
    			// 找偏移两侧的沿
                if (s16Edge <= s32Base)
    			{
                    s32Edge1 = s16Edge;
    			}
                else if (s132Edge2 == 0)
    			{
                    s132Edge2 = s16Edge;
    			    goto DLY_END;
    			}    			
			    
    			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}
    			
	        } // 查找指定周期
	    }
	    // 第一个沿是下降沿
        else
        {
	        // 先查找下降沿
            while(s16temp >= f32ThM && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			} 
			
			// 数据是否结束
            if (s16Edge >= s32Len)
			{
				return -1;
			}			
			    				          	
			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp > s16tempmin && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			}	
    			
			// 查找指定周期
            while(s16Edge < s32Len)
	        {    		
    			///////////////////////////////////////////////////////////////////    			
    			// 查找上升沿
                while(s16temp <= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			}

    			// 找偏移两侧的沿
                if (s16Edge <= s32Base)
    			{
                    s32Edge1 = s16Edge;
    			}
                else if (s132Edge2 == 0)
    			{
                    s132Edge2 = s16Edge;
    			    goto DLY_END;
    			}    			
    			
				// 必须过顶端点才可以查找下降沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s16Edge < s32Len)
				{
					s16temp = *pu8Data++;
					s16Edge ++;
				} 	
				
				///////////////////////////////////////////////////////////////////				
    	        // 查找下降沿
                while(s16temp >= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			} 
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			}    		    						
    			
    			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}	    			
    			     		   									
	        } // 查找指定周期            
	    }
	} 
	
DLY_END:	   
	
    pCH_DLY->EdgeX1 = s32Base;
	
    if (abs(s32Edge1 - s32Base) > abs(s132Edge2 - s32Base))
	{	  
          if (s132Edge2 == 0)
		  {
            pCH_DLY->EdgeX2 = s32Edge1;
	    }
	    else
	    {
              pCH_DLY->EdgeX2 = s132Edge2;
	    }
	}
	else
	{
          if (s32Edge1 == 0)
		  {
            pCH_DLY->EdgeX2 = s132Edge2;
	    }
	    else
	    {
              pCH_DLY->EdgeX2 = s32Edge1;
	    }
	}	
	
	
	if ((s16BaseOrg - pCH_DLY->EdgeX2) * (s16BaseOrg - pCH_DLY->EdgeX1) < 0)	// 在base两边
	{
		if (s16BaseCH1 == 1) 		// CH1
		{
			if (pCH_DLY->EdgeX2 - pCH_M1->RiseX1 < pCH_M1->RiseX2 - pCH_DLY->EdgeX2) 
			{
				if ( pCH_M1->RiseX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->RiseX1;
			}	
			else
			{
				if ( pCH_M1->RiseX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->RiseX2;
			}		
		}
		else
		{
			if (pCH_DLY->EdgeX2 - pCH_M2->RiseX1 < pCH_M2->RiseX2 - pCH_DLY->EdgeX2) 
			{
				if ( pCH_M2->RiseX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->RiseX1;
			}			
			else
			{
				if ( pCH_M2->RiseX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->RiseX2;
			}
		}
		
	}
	   	
	pCH_DLY->EdgeChan = s16BaseCH1;
	pCH_DLY->Delay = s16BaseCH1 * (pCH_DLY->EdgeX2 - pCH_DLY->EdgeX1) / g_meas_f32SaRate;

	
	return 0;
}

/*******************************************************************************
 函 数 名:     MeasDelayFF()
 描    述:	   自动测量延迟
 输入参数:    			                                
 输出参数:     
 			   pData	    ----- 测量数据指针
 			   s16Len		----- 测量数据长度
 			   pCH_M		----- 测量结构体
 返 回 值:     无	
 说    明:     
*******************************************************************************/
short MeasDelayFF(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE *pCH_M1,
                 MEASURE *pCH_M2,
                 MEASURE_DLY *pCH_DLY)
{
    int s16Edge1 = 0, s16Edge2 = 0;
    short s16temp;
    int s16Edge = 0;
    short s16Gnd;
    short s16BaseCH1 = 1;
    short s16tempmin,s16tempmax;
    unsigned char *pu8Data;
    MEASURE *pMeasure;
    int s16BaseOrg = s32Base;
    
    // 查找CH1离Base较近的一个边沿
    if (pCH_M1->FallX1 != 0 && pCH_M1->FallX2 != 0)
    {
        if (s32Base - pCH_M1->FallX1 > pCH_M1->FallX2 - s32Base)
        {
            s16Edge1 = pCH_M1->FallX2;
        }
        else
        {
            s16Edge1 = pCH_M1->FallX1;
        }
    }
    else if (pCH_M1->FallX1 != 0)
    {
        s16Edge1 = pCH_M1->FallX1;
    }
    else if (pCH_M1->FallX2 != 0)
    {
        s16Edge1 = pCH_M1->FallX2;
    }
    
    // 查找CH2离Base较远的一个边沿
    if (pCH_M2->FallX1 != 0 && pCH_M2->FallX2 != 0)
    {
        if (s32Base - pCH_M2->FallX1 > pCH_M2->FallX2 - s32Base)
        {
            s16Edge2 = pCH_M2->FallX2;
        }
        else
        {
            s16Edge2 = pCH_M2->FallX1;
        }
    }
    else if (pCH_M2->FallX1 != 0)
    {
        s16Edge2 = pCH_M2->FallX1;
    }
    else if (pCH_M2->FallX2 != 0)
    {
        s16Edge2 = pCH_M2->FallX2;
    }  

    // 以离Base较远的沿作为基准
    if (qAbs(s16Edge1 - s32Base) > qAbs(s16Edge2 - s32Base))
    {
        if (s16Edge1 != 0)
        {
            s32Base = s16Edge1;
            pu8Data = pData2;
            pMeasure = pCH_M2;
            s16Gnd = s16Gnd2;
            s16BaseCH1 = 1;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        if (s16Edge2 != 0)
        {
            s32Base = s16Edge2;
            pu8Data = pData1;
            pMeasure = pCH_M1;
            s16Gnd = s16Gnd1;
            s16BaseCH1 = -1;
        }
        else
        {
            return -1;
        }
    }
    
    // 查找Base两边的上升沿
    s16Edge1 = 0;
    s16Edge2 = 0;
//	s16tempmax = pMeasure->Vtop + (VOLT_GND_VALUE + s16Gnd) - pMeasure->Vamp / 5;
//	s16tempmin = pMeasure->Vbas + (VOLT_GND_VALUE + s16Gnd) + pMeasure->Vamp / 5;
    s16tempmax = pMeasure->Vupper + (VOLT_GND_VALUE + s16Gnd);
    s16tempmin = pMeasure->Vlower + (VOLT_GND_VALUE + s16Gnd);

    float f32ThM = pMeasure->PeriodY;
			   
	if (pMeasure->bVaild_Vpp == true)
	{
	    s16temp = *pu8Data;
	    
	    // 第一个沿是上升沿
        if (s16temp <= f32ThM)
	    {
	        // 先查找上升沿
            while(s16temp <= f32ThM && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			} 
			
			// 数据是否结束
            if (s16Edge >= s32Len)
			{
				return -1;
			}
				          			
			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp < s16tempmax && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			}
    			
			// 查找指定周期
            while(s16Edge < s32Len)
	        {  			    	          
    			///////////////////////////////////////////////////////////////////	    			
    			
    			// 查找下降沿
                while(s16temp >= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			}
    			 			
    			// 找偏移两侧的沿
                if (s16Edge <= s32Base)
    			{
    			    s16Edge1 = s16Edge;
    			}
    			else if (s16Edge2 == 0)
    			{
    			    s16Edge2 = s16Edge;
    			    goto DLY_END;
    			}
    			
				// 必须过底端点才可以查找上升沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s16Edge < s32Len)
				{
					s16temp = *pu8Data++;
					s16Edge ++;
				} 
				
				///////////////////////////////////////////////////////////////////
				
    	        // 查找上升沿
                while(s16temp <= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			} 
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			}  			
			    
    			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}
    			
	        } // 查找指定周期
	    }
	    // 第一个沿是下降沿
        else
        {       
	        // 先查找下降沿
            while(s16temp >= f32ThM && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			} 
			
			// 数据是否结束
            if (s16Edge >= s32Len)
			{
				return 0;
			}
			
			// 找偏移两侧的沿
            if (s16Edge <= s32Base)
			{
			    s16Edge1 = s16Edge;
			}
			else if (s16Edge2 == 0)
			{
			    s16Edge2 = s16Edge;
			    goto DLY_END;
			}  			
			    				          	
			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp > s16tempmin && s16Edge < s32Len)
			{
				s16temp = *pu8Data++;
				s16Edge ++;
			}	
    			
			// 查找指定周期
            while(s16Edge < s32Len)
	        {    		
    			///////////////////////////////////////////////////////////////////
    			
    			// 查找上升沿
                while(s16temp <= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			} 			
    			
				// 必须过顶端点才可以查找下降沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s16Edge < s32Len)
				{
					s16temp = *pu8Data++;
					s16Edge ++;
				} 	
				
				///////////////////////////////////////////////////////////////////
				
    	        // 查找下降沿
                while(s16temp >= f32ThM && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			} 
    			
    			// 数据是否结束
                if (s16Edge >= s32Len)
    			{
    				break;
    			}    		
    			
    			// 找偏移两侧的沿
                if (s16Edge <= s32Base)
    			{
    			    s16Edge1 = s16Edge;
    			}
    			else if (s16Edge2 == 0)
    			{
    			    s16Edge2 = s16Edge;
    			    goto DLY_END;
    			}    			
    			
    			// 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s16Edge < s32Len)
    			{
    				s16temp = *pu8Data++;
    				s16Edge ++;
    			}	    			
    			     		   									
	        } // 查找指定周期            
	    }
	}   

DLY_END:
	
    pCH_DLY->EdgeX1 = s32Base;
	
    if (abs(s16Edge1 - s32Base) > abs(s16Edge2 - s32Base))
	{	  
		  if (s16Edge2 == 0)  
		  {
	        pCH_DLY->EdgeX2 = s16Edge1;	
	    }
	    else
	    {
	    	  pCH_DLY->EdgeX2 = s16Edge2;	
	    }
	}
	else
	{
		  if (s16Edge1 == 0)
		  {
	        pCH_DLY->EdgeX2 = s16Edge2;	    
	    }
	    else
	    {
	    	  pCH_DLY->EdgeX2 = s16Edge1;	    
	    }
	}	
	
	if ((s16BaseOrg - pCH_DLY->EdgeX2) * (s16BaseOrg - pCH_DLY->EdgeX1) < 0)	// 在base两边
	{
		if (s16BaseCH1 == 1) 		// CH1
		{
			if (pCH_DLY->EdgeX2 - pCH_M1->FallX1 < pCH_M1->FallX2 - pCH_DLY->EdgeX2 ) 
			{
				if ( pCH_M1->FallX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->FallX1;
			}	
			else
			{
				if ( pCH_M1->FallX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->FallX2;
			}		
		}
		else
		{
			if (pCH_DLY->EdgeX2 - pCH_M2->FallX1 < pCH_M2->FallX2 - pCH_DLY->EdgeX2) 
			{
				if ( pCH_M2->FallX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->FallX1;
			}			
			else
			{
				if ( pCH_M2->FallX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->FallX2;
			}
		}
		
	}
	    
		
	pCH_DLY->EdgeChan = s16BaseCH1;
	pCH_DLY->Delay = s16BaseCH1 * (pCH_DLY->EdgeX2 - pCH_DLY->EdgeX1) / g_meas_f32SaRate;
	
	return 0; 
}

/*******************************************************************************
 函 数 名:     MeasDelay()
 描    述:	   自动测量延迟
 输入参数:
 输出参数:
               pData	    ----- 测量数据指针
               s32Len		----- 测量数据长度
               pCH_M		----- 测量结构体
 返 回 值:     无
 说    明:
*******************************************************************************/
short MeasDelayRF(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE *pCH_M1,
                 MEASURE *pCH_M2,
                 MEASURE_DLY *pCH_DLY)
{
    int s32Edge1 = 0, s32Edge2 = 0;
    short s16Gnd;
    short s16BaseCH1 = 1;
    short s16tempmin,s16tempmax;
    unsigned char *pu8Data;
    MEASURE *pMeasure;
    int s16BaseOrg = s32Base;

    // 查找CH1离Base较近的一个边沿
    if (pCH_M1->RiseX1 != 0 && pCH_M1->RiseX2 != 0)
    {
        if (s32Base - pCH_M1->RiseX1 > pCH_M1->RiseX2 - s32Base)
        {
            s32Edge1 = pCH_M1->RiseX2;
        }
        else
        {
            s32Edge1 = pCH_M1->RiseX1;
        }
    }
    else if (pCH_M1->RiseX1 != 0)
    {
        s32Edge1 = pCH_M1->RiseX1;
    }
    else if (pCH_M1->RiseX2 != 0)
    {
        s32Edge1 = pCH_M1->RiseX2;
    }
    else
    {
        s32Edge1 = 0;
    }

    // 查找CH2离Base较远的一个边沿
    if (pCH_M2->FallX1 != 0 && pCH_M2->FallX2 != 0)
    {
        if (s32Base - pCH_M2->FallX1 > pCH_M2->FallX2 - s32Base)
        {
            s32Edge2 = pCH_M2->FallX2;
        }
        else
        {
            s32Edge2 = pCH_M2->FallX1;
        }
    }
    else if (pCH_M2->FallX1 != 0)
    {
        s32Edge2 = pCH_M2->FallX1;
    }
    else if (pCH_M2->FallX2 != 0)
    {
        s32Edge2 = pCH_M2->FallX2;
    }
    else
    {
        s32Edge2 = 0;
    }

    // 以离Base较远的沿作为基准
    if (abs(s32Edge1 - s32Base) > abs(s32Edge2 - s32Base))
    {
        if (s32Edge1 != 0)
        {
            s32Base = s32Edge1;
            pu8Data = pData2;
            pMeasure = pCH_M2;
            s16Gnd = s16Gnd2;
            s16BaseCH1 = 1;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        if (s32Edge2 != 0)
        {
            s32Base = s32Edge2;
            pu8Data = pData1;
            pMeasure = pCH_M1;
            s16Gnd = s16Gnd1;
            s16BaseCH1 = -1;
        }
        else
        {
            return -1;
        }
    }

    // 查找Base两边的上升沿
    s32Edge1 = 0;
    s32Edge2 = 0;
//    s16tempmax = pMeasure->Vtop + (VOLT_GND_VALUE + s16Gnd) - pMeasure->Vamp / 5;
//    s16tempmin = pMeasure->Vbas + (VOLT_GND_VALUE + s16Gnd) + pMeasure->Vamp / 5;
    s16tempmax = pMeasure->Vupper + (VOLT_GND_VALUE + s16Gnd);
    s16tempmin = pMeasure->Vlower + (VOLT_GND_VALUE + s16Gnd);

    if (s16BaseCH1 == -1)
    {
        MeasDealyEdgeR(pMeasure, pu8Data, s16tempmax, s16tempmin, s32Len, s32Base, &s32Edge1, &s32Edge2);
    }
    else
    {
        MeasDealyEdgeF(pMeasure, pu8Data, s16tempmax, s16tempmin, s32Len, s32Base, &s32Edge1, &s32Edge2);
    }

    pCH_DLY->EdgeX1 = s32Base;

    if (abs(s32Edge1 - s32Base) > abs(s32Edge2 - s32Base))
    {
          if (s32Edge2 == 0)
          {
            pCH_DLY->EdgeX2 = s32Edge1;
        }
        else
        {
              pCH_DLY->EdgeX2 = s32Edge2;
        }
    }
    else
    {
          if (s32Edge1 == 0)
          {
            pCH_DLY->EdgeX2 = s32Edge2;
        }
        else
        {
              pCH_DLY->EdgeX2 = s32Edge1;
        }
    }


    if ((s16BaseOrg - pCH_DLY->EdgeX2) * (s16BaseOrg - pCH_DLY->EdgeX1) < 0)	// 在base两边
    {
        if (s16BaseCH1 == 1) 		// CH1
        {
            if (pCH_DLY->EdgeX2 - pCH_M1->RiseX1 < pCH_M1->RiseX2 - pCH_DLY->EdgeX2)
            {
                if ( pCH_M1->RiseX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->RiseX1;
            }
            else
            {
                if ( pCH_M1->RiseX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->RiseX2;
            }
        }
        else
        {
            if (pCH_DLY->EdgeX2 - pCH_M2->FallX1 < pCH_M2->FallX2 - pCH_DLY->EdgeX2)
            {
                if ( pCH_M2->FallX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->FallX1;
            }
            else
            {
                if ( pCH_M2->FallX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->FallX2;
            }
        }

    }

    pCH_DLY->EdgeChan = s16BaseCH1;
    pCH_DLY->Delay = s16BaseCH1 * (pCH_DLY->EdgeX2 - pCH_DLY->EdgeX1) / g_meas_f32SaRate;


    return 0;
}

/*******************************************************************************
 函 数 名:     MeasDelayFR()
 描    述:	   自动测量延迟
 输入参数:
 输出参数:
               pData	    ----- 测量数据指针
               s32Len		----- 测量数据长度
               pCH_M		----- 测量结构体
 返 回 值:     无
 说    明:
*******************************************************************************/
short MeasDelayFR(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE *pCH_M1,
                 MEASURE *pCH_M2,
                 MEASURE_DLY *pCH_DLY)
{
    int s16Edge1 = 0, s16Edge2 = 0;
    short s16Gnd;
    short s16BaseCH1 = 1;
    short s16tempmin,s16tempmax;
    unsigned char *pu8Data;
    MEASURE *pMeasure;
    int s16BaseOrg = s32Base;

    // 查找CH1离Base较近的一个边沿
    if (pCH_M1->FallX1 != 0 && pCH_M1->FallX2 != 0)
    {
        if (s32Base - pCH_M1->FallX1 > pCH_M1->FallX2 - s32Base)
        {
            s16Edge1 = pCH_M1->FallX2;
        }
        else
        {
            s16Edge1 = pCH_M1->FallX1;
        }
    }
    else if (pCH_M1->FallX1 != 0)
    {
        s16Edge1 = pCH_M1->FallX1;
    }
    else if (pCH_M1->FallX2 != 0)
    {
        s16Edge1 = pCH_M1->FallX2;
    }

    // 查找CH2离Base较远的一个边沿
    if (pCH_M2->RiseX1 != 0 && pCH_M2->RiseX2 != 0)
    {
        if (s32Base - pCH_M2->RiseX1 > pCH_M2->RiseX2 - s32Base)
        {
            s16Edge2 = pCH_M2->RiseX2;
        }
        else
        {
            s16Edge2 = pCH_M2->RiseX1;
        }
    }
    else if (pCH_M2->RiseX1 != 0)
    {
        s16Edge2 = pCH_M2->RiseX1;
    }
    else if (pCH_M2->RiseX2 != 0)
    {
        s16Edge2 = pCH_M2->RiseX2;
    }

    // 以离Base较远的沿作为基准
    if (abs(s16Edge1 - s32Base) > abs(s16Edge2 - s32Base))
    {
        if (s16Edge1 != 0)
        {
            s32Base = s16Edge1;
            pu8Data = pData2;
            pMeasure = pCH_M2;
            s16Gnd = s16Gnd2;
            s16BaseCH1 = 1;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        if (s16Edge2 != 0)
        {
            s32Base = s16Edge2;
            pu8Data = pData1;
            pMeasure = pCH_M1;
            s16Gnd = s16Gnd1;
            s16BaseCH1 = -1;
        }
        else
        {
            return -1;
        }
    }

    // 查找Base两边的上升沿
    s16Edge1 = 0;
    s16Edge2 = 0;
//    s16tempmax = pMeasure->Vtop + (VOLT_GND_VALUE + s16Gnd) - pMeasure->Vamp / 5;
//    s16tempmin = pMeasure->Vbas + (VOLT_GND_VALUE + s16Gnd) + pMeasure->Vamp / 5;
    s16tempmax = pMeasure->Vupper + (VOLT_GND_VALUE + s16Gnd);
    s16tempmin = pMeasure->Vlower + (VOLT_GND_VALUE + s16Gnd);

    if (s16BaseCH1 == 1)
    {
        MeasDealyEdgeR(pMeasure, pu8Data, s16tempmax, s16tempmin, s32Len, s32Base, &s16Edge1, &s16Edge2);
    }
    else
    {
        MeasDealyEdgeF(pMeasure, pu8Data, s16tempmax, s16tempmin, s32Len, s32Base, &s16Edge1, &s16Edge2);
    }

    pCH_DLY->EdgeX1 = s32Base;

    if (abs(s16Edge1 - s32Base) > abs(s16Edge2 - s32Base))
    {
          if (s16Edge2 == 0)
          {
            pCH_DLY->EdgeX2 = s16Edge1;
        }
        else
        {
              pCH_DLY->EdgeX2 = s16Edge2;
        }
    }
    else
    {
          if (s16Edge1 == 0)
          {
            pCH_DLY->EdgeX2 = s16Edge2;
        }
        else
        {
              pCH_DLY->EdgeX2 = s16Edge1;
        }
    }


    if ((s16BaseOrg - pCH_DLY->EdgeX2) * (s16BaseOrg - pCH_DLY->EdgeX1) < 0)	// 在base两边
    {
        if (s16BaseCH1 == 1) 		// CH1
        {
            if (pCH_DLY->EdgeX2 - pCH_M1->FallX1 < pCH_M1->FallX2 - pCH_DLY->EdgeX2 )
            {
                if ( pCH_M1->FallX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->FallX1;
            }
            else
            {
                if ( pCH_M1->FallX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M1->FallX2;
            }
        }
        else
        {
            if (pCH_DLY->EdgeX2 - pCH_M2->RiseX1 < pCH_M2->RiseX2 - pCH_DLY->EdgeX2)
            {
                if ( pCH_M2->RiseX1 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->RiseX1;
            }
            else
            {
                if ( pCH_M2->RiseX2 != 0 ) pCH_DLY->EdgeX1 = pCH_M2->RiseX2;
            }
        }

    }

    pCH_DLY->EdgeChan = s16BaseCH1;
    pCH_DLY->Delay = s16BaseCH1 * (pCH_DLY->EdgeX2 - pCH_DLY->EdgeX1) / g_meas_f32SaRate;


    return 0;
}

short MeasDealyEdgeR(MEASURE *pMeasure, unsigned char *pu8Data, short s16tempmax, short s16tempmin, int s32Len, int s32Base, int *s32Edge1, int *s32Edge2)
{
    short s16temp;
    int s32Edge = -1;

    float f32ThM = pMeasure->PeriodY;

    if (pMeasure->bVaild_Vpp == true)
    {
        s16temp = *pu8Data;

        // 第一个沿是上升沿
        if (s16temp <= f32ThM)
        {
            // 先查找上升沿
            while(s16temp <= f32ThM && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 数据是否结束
            if (s32Edge >= s32Len)
            {
                return 0;
            }

            // 找偏移两侧的沿
            if (s32Edge <= s32Base)
            {
                *s32Edge1 = s32Edge;
            }
            else if (*s32Edge2 == 0)
            {
                *s32Edge2 = s32Edge;
                goto DLY_END;
            }

            // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp < s16tempmax && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 查找指定周期
            while(s32Edge < s32Len)
            {
                ///////////////////////////////////////////////////////////////////
                // 查找下降沿
                while(s16temp >= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 必须过底端点才可以查找上升沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                ///////////////////////////////////////////////////////////////////
                // 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 找偏移两侧的沿
                if (s32Edge <= s32Base)
                {
                    *s32Edge1 = s32Edge;
                }
                else if (*s32Edge2 == 0)
                {
                    *s32Edge2 = s32Edge;
                    goto DLY_END;
                }

                // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

            } // 查找指定周期
        }
        // 第一个沿是下降沿
        else
        {
            // 先查找下降沿
            while(s16temp >= f32ThM && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 数据是否结束
            if (s32Edge >= s32Len)
            {
                return -1;
            }

            // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp > s16tempmin && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 查找指定周期
            while(s32Edge < s32Len)
            {
                ///////////////////////////////////////////////////////////////////
                // 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 找偏移两侧的沿
                if (s32Edge <= s32Base)
                {
                    *s32Edge1 = s32Edge;
                }
                else if (*s32Edge2 == 0)
                {
                    *s32Edge2 = s32Edge;
                    goto DLY_END;
                }

                // 必须过顶端点才可以查找下降沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                ///////////////////////////////////////////////////////////////////
                // 查找下降沿
                while(s16temp >= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

            } // 查找指定周期
        }
    }

DLY_END:

    return 0;
}

short MeasDealyEdgeF(MEASURE *pMeasure, unsigned char *pu8Data, short s16tempmax, short s16tempmin, int s32Len, int s32Base, int *s32Edge1, int *s32Edge2)
{
    short s16temp;
    int s32Edge = -1;
    float f32ThM = pMeasure->PeriodY;

    if (pMeasure->bVaild_Vpp == true)
    {
        s16temp = *pu8Data;

        // 第一个沿是上升沿
        if (s16temp <= f32ThM)
        {
            // 先查找上升沿
            while(s16temp <= f32ThM && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 数据是否结束
            if (s32Edge >= s32Len)
            {
                return -1;
            }

            // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp < s16tempmax && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 查找指定周期
            while(s32Edge < s32Len)
            {
                ///////////////////////////////////////////////////////////////////

                // 查找下降沿
                while(s16temp >= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 找偏移两侧的沿
                if (s32Edge <= s32Base)
                {
                    *s32Edge1 = s32Edge;
                }
                else if (*s32Edge2 == 0)
                {
                    *s32Edge2 = s32Edge;
                    goto DLY_END;
                }

                // 必须过底端点才可以查找上升沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                ///////////////////////////////////////////////////////////////////

                // 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

            } // 查找指定周期
        }
        // 第一个沿是下降沿
        else
        {
            // 先查找下降沿
            while(s16temp >= f32ThM && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 数据是否结束
            if (s32Edge >= s32Len)
            {
                return 0;
            }

            // 找偏移两侧的沿
            if (s32Edge <= s32Base)
            {
                *s32Edge1 = s32Edge;
            }
            else if (*s32Edge2 == 0)
            {
                *s32Edge2 = s32Edge;
                goto DLY_END;
            }

            // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
            while(s16temp > s16tempmin && s32Edge < s32Len)
            {
                s16temp = *pu8Data++;
                s32Edge ++;
            }

            // 查找指定周期
            while(s32Edge < s32Len)
            {
                ///////////////////////////////////////////////////////////////////

                // 查找上升沿
                while(s16temp <= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 必须过顶端点才可以查找下降沿，防止在下降沿上波形微小噪声造成测量出错
                while(s16temp < s16tempmax && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                ///////////////////////////////////////////////////////////////////

                // 查找下降沿
                while(s16temp >= f32ThM && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

                // 数据是否结束
                if (s32Edge >= s32Len)
                {
                    break;
                }

                // 找偏移两侧的沿
                if (s32Edge <= s32Base)
                {
                    *s32Edge1 = s32Edge;
                }
                else if (*s32Edge2 == 0)
                {
                    *s32Edge2 = s32Edge;
                    goto DLY_END;
                }

                // 必须过顶端点才可以查找下降沿，防止在上升沿上波形微小噪声造成测量出错
                while(s16temp > s16tempmin && s32Edge < s32Len)
                {
                    s16temp = *pu8Data++;
                    s32Edge ++;
                }

            } // 查找指定周期
        }
    }

DLY_END:

    return 0;
}

short AmpRatioTable[256] =
{
    65 ,65 ,65 ,65 ,65 ,65 ,65 ,65 ,65 ,65 ,60 ,53 ,48 ,42 ,37 ,
    36 ,35 ,33 ,30 ,30 ,26 ,24 ,23 ,21 ,20 ,20 ,20 ,19 ,17 ,17 ,
    17 ,16 ,16 ,16 ,16 ,16 ,15 ,14 ,13 ,13 ,13 ,13 ,13 ,13 ,12 ,
    12 ,12 ,11 ,10 ,10 ,10 ,9  ,9  ,9  ,9  ,9  ,9  ,9  ,8  ,8  ,
    8  ,8  ,8  ,8  ,8  ,8  ,8  ,8  ,7  ,7  ,7  ,6  ,6  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,
    5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,5  ,};

}

