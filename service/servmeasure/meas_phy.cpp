#include "meas_phy.h"

Meas_FPGA_RawData::Meas_FPGA_RawData()
{
    vmin = 0;
    vmax = 0;
    vbase = 0;
    vtop = 0;

    vupper = 0;
    vmid = 0;
    vlower= 0;

    xmax = 0;
    xmin = 0;

    rtimeA = 0;
    rtimeA_valid = 0;
    rtimeA_x2 = 0;
    rtimeB = 0;
    rtimeB_valid = 0;
    rtimeB_x2 = 0;

    ftimeA = 0;
    ftimeA_valid = 0;
    ftimeA_x2 = 0;
    ftimeB = 0;
    ftimeB_valid = 0;
    ftimeB_x2 = 0;

    cycle_x1 = 0;
    cycle_x1_edge_type = 0;
    cycle_x1_valid = 0;
    cycle_x2 = 0;
    cycle_x2_edge_type = 0;
    cycle_x2_valid = 0;
    cycle_x3 = 0;
    cycle_x3_edge_type = 0;
    cycle_x3_valid = 0;
    cycle_x4 = 0;
    cycle_x4_edge_type = 0;
    cycle_x4_valid = 0;

    ave_sum_bit31_0 = 0;
    ave_sum_bit39_32 = 0;
    ave_sum_1_bit31_0 = 0;
    ave_sum_1_bit39_32 = 0;
    ave_sum_2_bit31_0 = 0;
    ave_sum_2_bit39_32 = 0;

    vrms_sum_bit31_0 = 0;
    vrms_sum_bit47_32 = 0;
    vrms_sum_1_bit31_0 = 0;
    vrms_sum_1_bit47_32 = 0;
    vrms_sum_2_bit31_0 = 0;
    vrms_sum_2_bit47_32 = 0;

    rf_num = 0;
    rf_num_last_slope = 0;
    rf_num_valid = 0;

    edge_cnt_start_xloc = 0;
    edge_cnt_start_valid = 0;
    edge_cnt_end_xloc = 0;
    edge_cnt_end_valid = 0;

    edge_cnt_start2_xloc = 0;
    edge_cnt_start2_valid = 0;
    edge_cnt_end2_xloc = 0;
    edge_cnt_end2_valid = 0;

    overshoot = 0;
    preshoot = 0;
}

void Meas_FPGA_RawData::meas_getFPGARawData(Chan ch, EngineAdcCoreCH *adcCH, EngineMeasRet *ptr)
{
    if( (ptr != NULL)&&(adcCH != NULL) )
    {
        LOG_DBG() << " ch = " << ch;

//        qDebug()<<"Meas_FPGA_RawData::meas_getFPGARawData ch = "<<ch;
//        qDebug()<<"Meas_FPGA_RawData::meas_getFPGARawData adcCH->mCHa = "<<adcCH->mCHa;
//        qDebug()<<"Meas_FPGA_RawData::meas_getFPGARawData adcCH->mCHb = "<<adcCH->mCHb;
//        qDebug()<<"Meas_FPGA_RawData::meas_getFPGARawData adcCH->mCHc = "<<adcCH->mCHc;
//        qDebug()<<"Meas_FPGA_RawData::meas_getFPGARawData adcCH->mCHd = "<<adcCH->mCHd;

        switch( ch)
        {
        case d0: meas_getFPGARawData_la0(ptr); break;
        case d1: meas_getFPGARawData_la1(ptr); break;
        case d2: meas_getFPGARawData_la2(ptr); break;
        case d3: meas_getFPGARawData_la3(ptr); break;
        case d4: meas_getFPGARawData_la4(ptr); break;
        case d5: meas_getFPGARawData_la5(ptr); break;
        case d6: meas_getFPGARawData_la6(ptr); break;
        case d7: meas_getFPGARawData_la7(ptr); break;

        case d8:  meas_getFPGARawData_la8(ptr);  break;
        case d9:  meas_getFPGARawData_la9(ptr);  break;
        case d10: meas_getFPGARawData_la10(ptr); break;
        case d11: meas_getFPGARawData_la11(ptr); break;
        case d12: meas_getFPGARawData_la12(ptr); break;
        case d13: meas_getFPGARawData_la13(ptr); break;
        case d14: meas_getFPGARawData_la14(ptr); break;
        case d15: meas_getFPGARawData_la15(ptr); break;

        default:
            if( ch == adcCH->mCHa)
            {
                meas_getFPGARawData_cha(ptr);
            }
            else if( ch == adcCH->mCHb)
            {
                meas_getFPGARawData_chb(ptr);
            }
            else if( ch == adcCH->mCHc)
            {
                meas_getFPGARawData_chc(ptr);
            }
            else if( ch == adcCH->mCHd)
            {
                meas_getFPGARawData_chd(ptr);
            }
            else
            {
                meas_getFPGARawData_cha(ptr);
            }
            break;
        }

        LOG_DBG() << "vmin = " << vmin;
        LOG_DBG() << "vmax = " << vmax;
        LOG_DBG() << "vbase = " << vbase;
        LOG_DBG() << "vtop = " << vtop;

        LOG_DBG() << "cycle_x1 = " << cycle_x1;
        LOG_DBG() << "cycle_x2 = " << cycle_x2;
        LOG_DBG() << "cycle_x3 = " << cycle_x3;
        LOG_DBG() << "cycle_x4 = " << cycle_x4;

        LOG_DBG()<<"FPGA.vupper"<<vupper;
        LOG_DBG()<<"FPGA.vmid"<<vmid;
        LOG_DBG()<<"FPGA.vlower"<<vlower;
    }
}

void Meas_FPGA_RawData::meas_getFPGARawData_cha(EngineMeasRet *ptr)
{
    vmin  = ptr->getcha_max_min_top_base_cha_vmin();
    vmax  = ptr->getcha_max_min_top_base_cha_vmax();
    vbase = ptr->getcha_max_min_top_base_cha_vbase();
    vtop  = ptr->getcha_max_min_top_base_cha_vtop();

    vupper = ptr->getcha_threshold_l_mid_h_cha_threshold_h();
    vmid   = ptr->getcha_threshold_l_mid_h_cha_threshold_mid();
    vlower = ptr->getcha_threshold_l_mid_h_cha_threshold_l();

    xmax = ptr->getcha_vmax_xloc_vmax_xloc();
    xmin = ptr->getcha_vmin_xloc_vmin_xloc();

    rtimeA       = ptr->getcha_rtime_pre_rtime_pre();
    rtimeA_valid = ptr->getcha_rtime_pre_rtime_pre_vld();
    rtimeA_x2    = ptr->getcha_rtime_xloc_end_pre_rtime_xloc_end_pre();
    rtimeB       = ptr->getcha_rtime_rtime();
    rtimeB_valid = ptr->getcha_rtime_rtime_vld();
    rtimeB_x2    = ptr->getcha_rtime_xloc_end_rtime_xloc_end();

    ftimeA       = ptr->getcha_ftime_pre_ftime_pre();
    ftimeA_valid = ptr->getcha_ftime_pre_ftime_pre_vld();
    ftimeA_x2    = ptr->getcha_ftime_xloc_end_pre_ftime_xloc_end_pre();
    ftimeB       = ptr->getcha_ftime_ftime();
    ftimeB_valid = ptr->getcha_ftime_ftime_vld();
    ftimeB_x2    = ptr->getcha_ftime_xloc_end_ftime_xloc_end();

    cycle_x1           = ptr->getcha_cycle_xloc_1_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getcha_cycle_xloc_1_edge_type();
    cycle_x1_valid     = ptr->getcha_cycle_xloc_1_cycle_xloc_1_vld();
    cycle_x2           = ptr->getcha_cycle_xloc_2_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getcha_cycle_xloc_2_edge_type();
    cycle_x2_valid     = ptr->getcha_cycle_xloc_2_cycle_xloc_2_vld();
    cycle_x3           = ptr->getcha_cycle_xloc_3_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getcha_cycle_xloc_3_edge_type();
    cycle_x3_valid     = ptr->getcha_cycle_xloc_3_cycle_xloc_3_vld();
    cycle_x4           = ptr->getcha_cycle_xloc_4_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getcha_cycle_xloc_4_edge_type();
    cycle_x4_valid     = ptr->getcha_cycle_xloc_4_cycle_xloc_4_vld();

    ave_sum_bit31_0    = ptr->getcha_ave_sum_n_bits31_0_ave_sum_n_bit31_0();
    ave_sum_bit39_32   = ptr->getcha_ave_sum_x_bit39_32_ave_sum_n_bit39_32();
    ave_sum_1_bit31_0  = ptr->getcha_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0();
    ave_sum_1_bit39_32 = ptr->getcha_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32();
    ave_sum_2_bit31_0  = ptr->getcha_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0();
    ave_sum_2_bit39_32 = ptr->getcha_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32();

    vrms_sum_bit31_0    = ptr->getcha_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0();
    vrms_sum_bit47_32   = ptr->getcha_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32();
    vrms_sum_1_bit31_0  = ptr->getcha_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0();
    vrms_sum_1_bit47_32 = ptr->getcha_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32();
    vrms_sum_2_bit31_0  = ptr->getcha_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0();
    vrms_sum_2_bit47_32 = ptr->getcha_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32();

    rf_num              = ptr->getcha_total_rf_num_total_rf_num();
    rf_num_last_slope   = ptr->getcha_total_rf_num_slope();
    rf_num_valid        = ptr->getcha_total_rf_num_valid();
    edge_cnt_start_xloc  = ptr->getcha_edge_cnt_start_xloc_edge_cnt_start_xloc();
    edge_cnt_start_valid = ptr->getcha_edge_cnt_start_xloc_valid();
    edge_cnt_end_xloc    = ptr->getcha_edge_cnt_end_xloc_edge_cnt_end_xloc();
    edge_cnt_end_valid   = ptr->getcha_edge_cnt_end_xloc_valid();

    edge_cnt_start2_xloc  = ptr->getcha_edge_cnt_start2_xloc_edge_cnt_start2_xloc();
    edge_cnt_start2_valid = ptr->getcha_edge_cnt_start2_xloc_valid();
    edge_cnt_end2_xloc    = ptr->getcha_edge_cnt_end2_xloc_edge_cnt_end2_xloc();
    edge_cnt_end2_valid   = ptr->getcha_edge_cnt_end2_xloc_valid();

    overshoot = ptr->getcha_shoot_over_shoot();
    preshoot  = ptr->getcha_shoot_pre_shoot();
}

void Meas_FPGA_RawData::meas_getFPGARawData_chb(EngineMeasRet *ptr)
{
    vmin  = ptr->getchb_max_min_top_base_cha_vmin();
    vmax  = ptr->getchb_max_min_top_base_cha_vmax();
    vbase = ptr->getchb_max_min_top_base_cha_vbase();
    vtop  = ptr->getchb_max_min_top_base_cha_vtop();

    vupper = ptr->getchb_threshold_l_mid_h_cha_threshold_h();
    vmid   = ptr->getchb_threshold_l_mid_h_cha_threshold_mid();
    vlower = ptr->getchb_threshold_l_mid_h_cha_threshold_l();

    xmax = ptr->getchb_vmax_xloc_vmax_xloc();
    xmin = ptr->getchb_vmin_xloc_vmin_xloc();

    rtimeA       = ptr->getchb_rtime_pre_rtime_pre();
    rtimeA_valid = ptr->getchb_rtime_pre_rtime_pre_vld();
    rtimeA_x2    = ptr->getchb_rtime_xloc_end_pre_rtime_xloc_end_pre();
    rtimeB       = ptr->getchb_rtime_rtime();
    rtimeB_valid = ptr->getchb_rtime_rtime_vld();
    rtimeB_x2    = ptr->getchb_rtime_xloc_end_rtime_xloc_end();

    ftimeA       = ptr->getchb_ftime_pre_ftime_pre();
    ftimeA_valid = ptr->getchb_ftime_pre_ftime_pre_vld();
    ftimeA_x2    = ptr->getchb_ftime_xloc_end_pre_ftime_xloc_end_pre();
    ftimeB       = ptr->getchb_ftime_ftime();
    ftimeB_valid = ptr->getchb_ftime_ftime_vld();
    ftimeB_x2    = ptr->getchb_ftime_xloc_end_ftime_xloc_end();

    cycle_x1           = ptr->getchb_cycle_xloc_1_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getchb_cycle_xloc_1_edge_type();
    cycle_x1_valid     = ptr->getchb_cycle_xloc_1_cycle_xloc_1_vld();
    cycle_x2           = ptr->getchb_cycle_xloc_2_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getchb_cycle_xloc_2_edge_type();
    cycle_x2_valid     = ptr->getchb_cycle_xloc_2_cycle_xloc_2_vld();
    cycle_x3           = ptr->getchb_cycle_xloc_3_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getchb_cycle_xloc_3_edge_type();
    cycle_x3_valid     = ptr->getchb_cycle_xloc_3_cycle_xloc_3_vld();
    cycle_x4           = ptr->getchb_cycle_xloc_4_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getchb_cycle_xloc_4_edge_type();
    cycle_x4_valid     = ptr->getchb_cycle_xloc_4_cycle_xloc_4_vld();

    ave_sum_bit31_0    = ptr->getchb_ave_sum_n_bits31_0_ave_sum_n_bit31_0();
    ave_sum_bit39_32   = ptr->getchb_ave_sum_x_bit39_32_ave_sum_n_bit39_32();
    ave_sum_1_bit31_0  = ptr->getchb_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0();
    ave_sum_1_bit39_32 = ptr->getchb_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32();
    ave_sum_2_bit31_0  = ptr->getchb_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0();
    ave_sum_2_bit39_32 = ptr->getchb_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32();

    vrms_sum_bit31_0    = ptr->getchb_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0();
    vrms_sum_bit47_32   = ptr->getchb_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32();
    vrms_sum_1_bit31_0  = ptr->getchb_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0();
    vrms_sum_1_bit47_32 = ptr->getchb_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32();
    vrms_sum_2_bit31_0  = ptr->getchb_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0();
    vrms_sum_2_bit47_32 = ptr->getchb_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32();

    rf_num            = ptr->getchb_total_rf_num_total_rf_num();
    rf_num_last_slope = ptr->getchb_total_rf_num_slope();
    rf_num_valid      = ptr->getchb_total_rf_num_valid();
    edge_cnt_start_xloc  = ptr->getchb_edge_cnt_start_xloc_edge_cnt_start_xloc();
    edge_cnt_start_valid = ptr->getchb_edge_cnt_start_xloc_valid();
    edge_cnt_end_xloc    = ptr->getchb_edge_cnt_end_xloc_edge_cnt_end_xloc();
    edge_cnt_end_valid   = ptr->getchb_edge_cnt_end_xloc_valid();

    edge_cnt_start2_xloc  = ptr->getchb_edge_cnt_start2_xloc_edge_cnt_start2_xloc();
    edge_cnt_start2_valid = ptr->getchb_edge_cnt_start2_xloc_valid();
    edge_cnt_end2_xloc    = ptr->getchb_edge_cnt_end2_xloc_edge_cnt_end2_xloc();
    edge_cnt_end2_valid   = ptr->getchb_edge_cnt_end2_xloc_valid();

    overshoot = ptr->getchb_shoot_over_shoot();
    preshoot  = ptr->getchb_shoot_pre_shoot();
}

void Meas_FPGA_RawData::meas_getFPGARawData_chc(EngineMeasRet *ptr)
{
    vmin  = ptr->getchc_max_min_top_base_cha_vmin();
    vmax  = ptr->getchc_max_min_top_base_cha_vmax();
    vbase = ptr->getchc_max_min_top_base_cha_vbase();
    vtop  = ptr->getchc_max_min_top_base_cha_vtop();

    vupper = ptr->getchc_threshold_l_mid_h_cha_threshold_h();
    vmid   = ptr->getchc_threshold_l_mid_h_cha_threshold_mid();
    vlower = ptr->getchc_threshold_l_mid_h_cha_threshold_l();

    xmax = ptr->getchc_vmax_xloc_vmax_xloc();
    xmin = ptr->getchc_vmin_xloc_vmin_xloc();

    rtimeA       = ptr->getchc_rtime_pre_rtime_pre();
    rtimeA_valid = ptr->getchc_rtime_pre_rtime_pre_vld();
    rtimeA_x2    = ptr->getchc_rtime_xloc_end_pre_rtime_xloc_end_pre();
    rtimeB       = ptr->getchc_rtime_rtime();
    rtimeB_valid = ptr->getchc_rtime_rtime_vld();
    rtimeB_x2    = ptr->getchc_rtime_xloc_end_rtime_xloc_end();

    ftimeA       = ptr->getchc_ftime_pre_ftime_pre();
    ftimeA_valid = ptr->getchc_ftime_pre_ftime_pre_vld();
    ftimeA_x2    = ptr->getchc_ftime_xloc_end_pre_ftime_xloc_end_pre();
    ftimeB       = ptr->getchc_ftime_ftime();
    ftimeB_valid = ptr->getchc_ftime_ftime_vld();
    ftimeB_x2    = ptr->getchc_ftime_xloc_end_ftime_xloc_end();

    cycle_x1           = ptr->getchc_cycle_xloc_1_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getchc_cycle_xloc_1_edge_type();
    cycle_x1_valid     = ptr->getchc_cycle_xloc_1_cycle_xloc_1_vld();
    cycle_x2           = ptr->getchc_cycle_xloc_2_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getchc_cycle_xloc_2_edge_type();
    cycle_x2_valid     = ptr->getchc_cycle_xloc_2_cycle_xloc_2_vld();
    cycle_x3           = ptr->getchc_cycle_xloc_3_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getchc_cycle_xloc_3_edge_type();
    cycle_x3_valid     = ptr->getchc_cycle_xloc_3_cycle_xloc_3_vld();
    cycle_x4           = ptr->getchc_cycle_xloc_4_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getchc_cycle_xloc_4_edge_type();
    cycle_x4_valid     = ptr->getchc_cycle_xloc_4_cycle_xloc_4_vld();

    ave_sum_bit31_0    = ptr->getchc_ave_sum_n_bits31_0_ave_sum_n_bit31_0();
    ave_sum_bit39_32   = ptr->getchc_ave_sum_x_bit39_32_ave_sum_n_bit39_32();
    ave_sum_1_bit31_0  = ptr->getchc_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0();
    ave_sum_1_bit39_32 = ptr->getchc_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32();
    ave_sum_2_bit31_0  = ptr->getchc_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0();
    ave_sum_2_bit39_32 = ptr->getchc_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32();

    vrms_sum_bit31_0    = ptr->getchc_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0();
    vrms_sum_bit47_32   = ptr->getchc_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32();
    vrms_sum_1_bit31_0  = ptr->getchc_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0();
    vrms_sum_1_bit47_32 = ptr->getchc_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32();
    vrms_sum_2_bit31_0  = ptr->getchc_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0();
    vrms_sum_2_bit47_32 = ptr->getchc_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32();

    rf_num            = ptr->getchc_total_rf_num_total_rf_num();
    rf_num_last_slope = ptr->getchc_total_rf_num_slope();
    rf_num_valid      = ptr->getchc_total_rf_num_valid();
    edge_cnt_start_xloc  = ptr->getchc_edge_cnt_start_xloc_edge_cnt_start_xloc();
    edge_cnt_start_valid = ptr->getchc_edge_cnt_start_xloc_valid();
    edge_cnt_end_xloc    = ptr->getchc_edge_cnt_end_xloc_edge_cnt_end_xloc();
    edge_cnt_end_valid   = ptr->getchc_edge_cnt_end_xloc_valid();

    edge_cnt_start2_xloc  = ptr->getchc_edge_cnt_start2_xloc_edge_cnt_start2_xloc();
    edge_cnt_start2_valid = ptr->getchc_edge_cnt_start2_xloc_valid();
    edge_cnt_end2_xloc    = ptr->getchc_edge_cnt_end2_xloc_edge_cnt_end2_xloc();
    edge_cnt_end2_valid   = ptr->getchc_edge_cnt_end2_xloc_valid();

    overshoot = ptr->getchc_shoot_over_shoot();
    preshoot  = ptr->getchc_shoot_pre_shoot();
}

void Meas_FPGA_RawData::meas_getFPGARawData_chd(EngineMeasRet *ptr)
{
    vmin  = ptr->getchd_max_min_top_base_cha_vmin();
    vmax  = ptr->getchd_max_min_top_base_cha_vmax();
    vbase = ptr->getchd_max_min_top_base_cha_vbase();
    vtop  = ptr->getchd_max_min_top_base_cha_vtop();

    vupper = ptr->getchd_threshold_l_mid_h_cha_threshold_h();
    vmid   = ptr->getchd_threshold_l_mid_h_cha_threshold_mid();
    vlower = ptr->getchd_threshold_l_mid_h_cha_threshold_l();

    xmax = ptr->getchd_vmax_xloc_vmax_xloc();
    xmin = ptr->getchd_vmin_xloc_vmin_xloc();

    rtimeA       = ptr->getchd_rtime_pre_rtime_pre();
    rtimeA_valid = ptr->getchd_rtime_pre_rtime_pre_vld();
    rtimeA_x2    = ptr->getchd_rtime_xloc_end_pre_rtime_xloc_end_pre();
    rtimeB       = ptr->getchd_rtime_rtime();
    rtimeB_valid = ptr->getchd_rtime_rtime_vld();
    rtimeB_x2    = ptr->getchd_rtime_xloc_end_rtime_xloc_end();

    ftimeA       = ptr->getchd_ftime_pre_ftime_pre();
    ftimeA_valid = ptr->getchd_ftime_pre_ftime_pre_vld();
    ftimeA_x2    = ptr->getchd_ftime_xloc_end_pre_ftime_xloc_end_pre();
    ftimeB       = ptr->getchd_ftime_ftime();
    ftimeB_valid = ptr->getchd_ftime_ftime_vld();
    ftimeB_x2    = ptr->getchd_ftime_xloc_end_ftime_xloc_end();

    cycle_x1           = ptr->getchd_cycle_xloc_1_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getchd_cycle_xloc_1_edge_type();
    cycle_x1_valid     = ptr->getchd_cycle_xloc_1_cycle_xloc_1_vld();
    cycle_x2           = ptr->getchd_cycle_xloc_2_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getchd_cycle_xloc_2_edge_type();
    cycle_x2_valid     = ptr->getchd_cycle_xloc_2_cycle_xloc_2_vld();
    cycle_x3           = ptr->getchd_cycle_xloc_3_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getchd_cycle_xloc_3_edge_type();
    cycle_x3_valid     = ptr->getchd_cycle_xloc_3_cycle_xloc_3_vld();
    cycle_x4           = ptr->getchd_cycle_xloc_4_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getchd_cycle_xloc_4_edge_type();
    cycle_x4_valid     = ptr->getchd_cycle_xloc_4_cycle_xloc_4_vld();

    ave_sum_bit31_0    = ptr->getchd_ave_sum_n_bits31_0_ave_sum_n_bit31_0();
    ave_sum_bit39_32   = ptr->getchd_ave_sum_x_bit39_32_ave_sum_n_bit39_32();
    ave_sum_1_bit31_0  = ptr->getchd_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0();
    ave_sum_1_bit39_32 = ptr->getchd_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32();
    ave_sum_2_bit31_0  = ptr->getchd_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0();
    ave_sum_2_bit39_32 = ptr->getchd_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32();

    vrms_sum_bit31_0    = ptr->getchd_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0();
    vrms_sum_bit47_32   = ptr->getchd_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32();
    vrms_sum_1_bit31_0  = ptr->getchd_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0();
    vrms_sum_1_bit47_32 = ptr->getchd_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32();
    vrms_sum_2_bit31_0  = ptr->getchd_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0();
    vrms_sum_2_bit47_32 = ptr->getchd_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32();

    rf_num            = ptr->getchd_total_rf_num_total_rf_num();
    rf_num_last_slope = ptr->getchd_total_rf_num_slope();
    rf_num_valid      = ptr->getchd_total_rf_num_valid();
    edge_cnt_start_xloc  = ptr->getchd_edge_cnt_start_xloc_edge_cnt_start_xloc();
    edge_cnt_start_valid = ptr->getchd_edge_cnt_start_xloc_valid();
    edge_cnt_end_xloc    = ptr->getchd_edge_cnt_end_xloc_edge_cnt_end_xloc();
    edge_cnt_end_valid   = ptr->getchd_edge_cnt_end_xloc_valid();

    edge_cnt_start2_xloc  = ptr->getchd_edge_cnt_start2_xloc_edge_cnt_start2_xloc();
    edge_cnt_start2_valid = ptr->getchd_edge_cnt_start2_xloc_valid();
    edge_cnt_end2_xloc    = ptr->getchd_edge_cnt_end2_xloc_edge_cnt_end2_xloc();
    edge_cnt_end2_valid   = ptr->getchd_edge_cnt_end2_xloc_valid();

    overshoot = ptr->getchd_shoot_over_shoot();
    preshoot  = ptr->getchd_shoot_pre_shoot();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la0(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla0_cycle_xloc_1_la0_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla0_cycle_xloc_1_la0_edge_type();
    cycle_x1_valid     = ptr->getla0_cycle_xloc_1_la0_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla0_cycle_xloc_2_la0_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla0_cycle_xloc_2_la0_edge_type();
    cycle_x2_valid     = ptr->getla0_cycle_xloc_2_la0_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla0_cycle_xloc_3_la0_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla0_cycle_xloc_3_la0_edge_type();
    cycle_x3_valid     = ptr->getla0_cycle_xloc_3_la0_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla0_cycle_xloc_4_la0_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla0_cycle_xloc_4_la0_edge_type();
    cycle_x4_valid     = ptr->getla0_cycle_xloc_4_la0_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la1(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla1_cycle_xloc_1_la1_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla1_cycle_xloc_1_la1_edge_type();
    cycle_x1_valid     = ptr->getla1_cycle_xloc_1_la1_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla1_cycle_xloc_2_la1_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla1_cycle_xloc_2_la1_edge_type();
    cycle_x2_valid     = ptr->getla1_cycle_xloc_2_la1_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla1_cycle_xloc_3_la1_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla1_cycle_xloc_3_la1_edge_type();
    cycle_x3_valid     = ptr->getla1_cycle_xloc_3_la1_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla1_cycle_xloc_4_la1_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla1_cycle_xloc_4_la1_edge_type();
    cycle_x4_valid     = ptr->getla1_cycle_xloc_4_la1_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la2(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla2_cycle_xloc_1_la2_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla2_cycle_xloc_1_la2_edge_type();
    cycle_x1_valid     = ptr->getla2_cycle_xloc_1_la2_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla2_cycle_xloc_2_la2_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla2_cycle_xloc_2_la2_edge_type();
    cycle_x2_valid     = ptr->getla2_cycle_xloc_2_la2_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla2_cycle_xloc_3_la2_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla2_cycle_xloc_3_la2_edge_type();
    cycle_x3_valid     = ptr->getla2_cycle_xloc_3_la2_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla2_cycle_xloc_4_la2_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla2_cycle_xloc_4_la2_edge_type();
    cycle_x4_valid     = ptr->getla2_cycle_xloc_4_la2_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la3(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla3_cycle_xloc_1_la3_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla3_cycle_xloc_1_la3_edge_type();
    cycle_x1_valid     = ptr->getla3_cycle_xloc_1_la3_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla3_cycle_xloc_2_la3_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla3_cycle_xloc_2_la3_edge_type();
    cycle_x2_valid     = ptr->getla3_cycle_xloc_2_la3_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla3_cycle_xloc_3_la3_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla3_cycle_xloc_3_la3_edge_type();
    cycle_x3_valid     = ptr->getla3_cycle_xloc_3_la3_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla3_cycle_xloc_4_la3_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla3_cycle_xloc_4_la3_edge_type();
    cycle_x4_valid     = ptr->getla3_cycle_xloc_4_la3_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la4(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla4_cycle_xloc_1_la4_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla4_cycle_xloc_1_la4_edge_type();
    cycle_x1_valid     = ptr->getla4_cycle_xloc_1_la4_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla4_cycle_xloc_2_la4_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla4_cycle_xloc_2_la4_edge_type();
    cycle_x2_valid     = ptr->getla4_cycle_xloc_2_la4_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla4_cycle_xloc_3_la4_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla4_cycle_xloc_3_la4_edge_type();
    cycle_x3_valid     = ptr->getla4_cycle_xloc_3_la4_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla4_cycle_xloc_4_la4_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla4_cycle_xloc_4_la4_edge_type();
    cycle_x4_valid     = ptr->getla4_cycle_xloc_4_la4_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la5(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla5_cycle_xloc_1_la5_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla5_cycle_xloc_1_la5_edge_type();
    cycle_x1_valid     = ptr->getla5_cycle_xloc_1_la5_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla5_cycle_xloc_2_la5_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla5_cycle_xloc_2_la5_edge_type();
    cycle_x2_valid     = ptr->getla5_cycle_xloc_2_la5_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla5_cycle_xloc_3_la5_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla5_cycle_xloc_3_la5_edge_type();
    cycle_x3_valid     = ptr->getla5_cycle_xloc_3_la5_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla5_cycle_xloc_4_la5_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla5_cycle_xloc_4_la5_edge_type();
    cycle_x4_valid     = ptr->getla5_cycle_xloc_4_la5_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la6(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla6_cycle_xloc_1_la6_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla6_cycle_xloc_1_la6_edge_type();
    cycle_x1_valid     = ptr->getla6_cycle_xloc_1_la6_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla6_cycle_xloc_2_la6_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla6_cycle_xloc_2_la6_edge_type();
    cycle_x2_valid     = ptr->getla6_cycle_xloc_2_la6_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla6_cycle_xloc_3_la6_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla6_cycle_xloc_3_la6_edge_type();
    cycle_x3_valid     = ptr->getla6_cycle_xloc_3_la6_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla6_cycle_xloc_4_la6_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla6_cycle_xloc_4_la6_edge_type();
    cycle_x4_valid     = ptr->getla6_cycle_xloc_4_la6_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la7(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla7_cycle_xloc_1_la7_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla7_cycle_xloc_1_la7_edge_type();
    cycle_x1_valid     = ptr->getla7_cycle_xloc_1_la7_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla7_cycle_xloc_2_la7_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla7_cycle_xloc_2_la7_edge_type();
    cycle_x2_valid     = ptr->getla7_cycle_xloc_2_la7_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla7_cycle_xloc_3_la7_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla7_cycle_xloc_3_la7_edge_type();
    cycle_x3_valid     = ptr->getla7_cycle_xloc_3_la7_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla7_cycle_xloc_4_la7_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla7_cycle_xloc_4_la7_edge_type();
    cycle_x4_valid     = ptr->getla7_cycle_xloc_4_la7_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la8(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla8_cycle_xloc_1_la8_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla8_cycle_xloc_1_la8_edge_type();
    cycle_x1_valid     = ptr->getla8_cycle_xloc_1_la8_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla8_cycle_xloc_2_la8_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla8_cycle_xloc_2_la8_edge_type();
    cycle_x2_valid     = ptr->getla8_cycle_xloc_2_la8_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla8_cycle_xloc_3_la8_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla8_cycle_xloc_3_la8_edge_type();
    cycle_x3_valid     = ptr->getla8_cycle_xloc_3_la8_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla8_cycle_xloc_4_la8_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla8_cycle_xloc_4_la8_edge_type();
    cycle_x4_valid     = ptr->getla8_cycle_xloc_4_la8_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la9(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla9_cycle_xloc_1_la9_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla9_cycle_xloc_1_la9_edge_type();
    cycle_x1_valid     = ptr->getla9_cycle_xloc_1_la9_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla9_cycle_xloc_2_la9_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla9_cycle_xloc_2_la9_edge_type();
    cycle_x2_valid     = ptr->getla9_cycle_xloc_2_la9_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla9_cycle_xloc_3_la9_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla9_cycle_xloc_3_la9_edge_type();
    cycle_x3_valid     = ptr->getla9_cycle_xloc_3_la9_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla9_cycle_xloc_4_la9_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla9_cycle_xloc_4_la9_edge_type();
    cycle_x4_valid     = ptr->getla9_cycle_xloc_4_la9_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la10(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla10_cycle_xloc_1_la10_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla10_cycle_xloc_1_la10_edge_type();
    cycle_x1_valid     = ptr->getla10_cycle_xloc_1_la10_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla10_cycle_xloc_2_la10_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla10_cycle_xloc_2_la10_edge_type();
    cycle_x2_valid     = ptr->getla10_cycle_xloc_2_la10_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla10_cycle_xloc_3_la10_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla10_cycle_xloc_3_la10_edge_type();
    cycle_x3_valid     = ptr->getla10_cycle_xloc_3_la10_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla10_cycle_xloc_4_la10_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla10_cycle_xloc_4_la10_edge_type();
    cycle_x4_valid     = ptr->getla10_cycle_xloc_4_la10_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la11(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla11_cycle_xloc_1_la11_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla11_cycle_xloc_1_la11_edge_type();
    cycle_x1_valid     = ptr->getla11_cycle_xloc_1_la11_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla11_cycle_xloc_2_la11_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla11_cycle_xloc_2_la11_edge_type();
    cycle_x2_valid     = ptr->getla11_cycle_xloc_2_la11_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla11_cycle_xloc_3_la11_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla11_cycle_xloc_3_la11_edge_type();
    cycle_x3_valid     = ptr->getla11_cycle_xloc_3_la11_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla11_cycle_xloc_4_la11_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla11_cycle_xloc_4_la11_edge_type();
    cycle_x4_valid     = ptr->getla11_cycle_xloc_4_la11_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la12(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla12_cycle_xloc_1_la12_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla12_cycle_xloc_1_la12_edge_type();
    cycle_x1_valid     = ptr->getla12_cycle_xloc_1_la12_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla12_cycle_xloc_2_la12_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla12_cycle_xloc_2_la12_edge_type();
    cycle_x2_valid     = ptr->getla12_cycle_xloc_2_la12_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla12_cycle_xloc_3_la12_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla12_cycle_xloc_3_la12_edge_type();
    cycle_x3_valid     = ptr->getla12_cycle_xloc_3_la12_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla12_cycle_xloc_4_la12_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla12_cycle_xloc_4_la12_edge_type();
    cycle_x4_valid     = ptr->getla12_cycle_xloc_4_la12_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la13(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla13_cycle_xloc_1_la13_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla13_cycle_xloc_1_la13_edge_type();
    cycle_x1_valid     = ptr->getla13_cycle_xloc_1_la13_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla13_cycle_xloc_2_la13_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla13_cycle_xloc_2_la13_edge_type();
    cycle_x2_valid     = ptr->getla13_cycle_xloc_2_la13_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla13_cycle_xloc_3_la13_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla13_cycle_xloc_3_la13_edge_type();
    cycle_x3_valid     = ptr->getla13_cycle_xloc_3_la13_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla13_cycle_xloc_4_la13_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla13_cycle_xloc_4_la13_edge_type();
    cycle_x4_valid     = ptr->getla13_cycle_xloc_4_la13_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la14(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla14_cycle_xloc_1_la14_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla14_cycle_xloc_1_la14_edge_type();
    cycle_x1_valid     = ptr->getla14_cycle_xloc_1_la14_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla14_cycle_xloc_2_la14_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla14_cycle_xloc_2_la14_edge_type();
    cycle_x2_valid     = ptr->getla14_cycle_xloc_2_la14_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla14_cycle_xloc_3_la14_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla14_cycle_xloc_3_la14_edge_type();
    cycle_x3_valid     = ptr->getla14_cycle_xloc_3_la14_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla14_cycle_xloc_4_la14_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla14_cycle_xloc_4_la14_edge_type();
    cycle_x4_valid     = ptr->getla14_cycle_xloc_4_la14_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_la15(EngineMeasRet *ptr)
{
    cycle_x1           = ptr->getla15_cycle_xloc_1_la15_cycle_xloc_1();
    cycle_x1_edge_type = ptr->getla15_cycle_xloc_1_la15_edge_type();
    cycle_x1_valid     = ptr->getla15_cycle_xloc_1_la15_cycle_xloc_1_vld();
    cycle_x2           = ptr->getla15_cycle_xloc_2_la15_cycle_xloc_2();
    cycle_x2_edge_type = ptr->getla15_cycle_xloc_2_la15_edge_type();
    cycle_x2_valid     = ptr->getla15_cycle_xloc_2_la15_cycle_xloc_2_vld();
    cycle_x3           = ptr->getla15_cycle_xloc_3_la15_cycle_xloc_3();
    cycle_x3_edge_type = ptr->getla15_cycle_xloc_3_la15_edge_type();
    cycle_x3_valid     = ptr->getla15_cycle_xloc_3_la15_cycle_xloc_3_vld();
    cycle_x4           = ptr->getla15_cycle_xloc_4_la15_cycle_xloc_4();
    cycle_x4_edge_type = ptr->getla15_cycle_xloc_4_la15_edge_type();
    cycle_x4_valid     = ptr->getla15_cycle_xloc_4_la15_cycle_xloc_4_vld();
}

void Meas_FPGA_RawData::meas_getFPGARawData_dly(EngineMeasRet *ptr, MeasFPGA_DlyIndex index)
{
    if( NULL != ptr)
    {
        get_dly_from_xloc2( ptr, index);
        get_dly_from_xloc1( ptr, index);
        get_dly_to_xloc2_pre( ptr, index);
        get_dly_to_xloc2( ptr, index);
        get_dly_to_xloc1_pre( ptr, index);
        get_dly_to_xloc1( ptr, index);
    }
}

void Meas_FPGA_RawData::get_dly_from_xloc2( EngineMeasRet *ptr, MeasFPGA_DlyIndex index)
{
    switch (index)
    {
    case Dly1:
        dly_from_xloc2     = ptr->getdlyresult1_from_xloc2_dlyresult1_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult1_from_xloc2_dlyresult1_from_xloc2_vld();
        break;
    case Dly2:
        dly_from_xloc2     = ptr->getdlyresult2_from_xloc2_dlyresult2_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult2_from_xloc2_dlyresult2_from_xloc2_vld();
        break;
    case Dly3:
        dly_from_xloc2     = ptr->getdlyresult3_from_xloc2_dlyresult3_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult3_from_xloc2_dlyresult3_from_xloc2_vld();
        break;
    case Dly4:
        dly_from_xloc2     = ptr->getdlyresult4_from_xloc2_dlyresult4_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult4_from_xloc2_dlyresult4_from_xloc2_vld();
        break;
    case Dly5:
        dly_from_xloc2     = ptr->getdlyresult5_from_xloc2_dlyresult5_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult5_from_xloc2_dlyresult5_from_xloc2_vld();
        break;
    case Dly6:
        dly_from_xloc2     = ptr->getdlyresult6_from_xloc2_dlyresult6_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult6_from_xloc2_dlyresult6_from_xloc2_vld();
        break;
    case Dly7:
        dly_from_xloc2     = ptr->getdlyresult7_from_xloc2_dlyresult7_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult7_from_xloc2_dlyresult7_from_xloc2_vld();
        break;
    case Dly8:
        dly_from_xloc2     = ptr->getdlyresult8_from_xloc2_dlyresult8_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult8_from_xloc2_dlyresult8_from_xloc2_vld();
        break;
    case Dly9:
        dly_from_xloc2     = ptr->getdlyresult9_from_xloc2_dlyresult9_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult9_from_xloc2_dlyresult9_from_xloc2_vld();
        break;
    case Dly10:
        dly_from_xloc2     = ptr->getdlyresult10_from_xloc2_dlyresult10_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult10_from_xloc2_dlyresult10_from_xloc2_vld();
        break;
    default:
        dly_from_xloc2     = ptr->getdlyresult1_from_xloc2_dlyresult1_from_xloc2();
        dly_from_xloc2_vld = ptr->getdlyresult1_from_xloc2_dlyresult1_from_xloc2_vld();
        break;
    }
}

void Meas_FPGA_RawData::get_dly_from_xloc1( EngineMeasRet *ptr, MeasFPGA_DlyIndex index)
{
    switch (index)
    {
    case Dly1:
        dly_from_xloc1     = ptr->getdlyresult1_from_xloc1_dlyresult1_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult1_from_xloc1_dlyresult1_from_xloc1_vld();
        break;
    case Dly2:
        dly_from_xloc1     = ptr->getdlyresult2_from_xloc1_dlyresult2_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult2_from_xloc1_dlyresult2_from_xloc1_vld();
        break;
    case Dly3:
        dly_from_xloc1     = ptr->getdlyresult3_from_xloc1_dlyresult3_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult3_from_xloc1_dlyresult3_from_xloc1_vld();
        break;
    case Dly4:
        dly_from_xloc1     = ptr->getdlyresult4_from_xloc1_dlyresult4_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult4_from_xloc1_dlyresult4_from_xloc1_vld();
        break;
    case Dly5:
        dly_from_xloc1     = ptr->getdlyresult5_from_xloc1_dlyresult5_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult5_from_xloc1_dlyresult5_from_xloc1_vld();
        break;
    case Dly6:
        dly_from_xloc1     = ptr->getdlyresult6_from_xloc1_dlyresult6_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult6_from_xloc1_dlyresult6_from_xloc1_vld();
        break;
    case Dly7:
        dly_from_xloc1     = ptr->getdlyresult7_from_xloc1_dlyresult7_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult7_from_xloc1_dlyresult7_from_xloc1_vld();
        break;
    case Dly8:
        dly_from_xloc1     = ptr->getdlyresult8_from_xloc1_dlyresult8_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult8_from_xloc1_dlyresult8_from_xloc1_vld();
        break;
    case Dly9:
        dly_from_xloc1     = ptr->getdlyresult9_from_xloc1_dlyresult9_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult9_from_xloc1_dlyresult9_from_xloc1_vld();
        break;
    case Dly10:
        dly_from_xloc1     = ptr->getdlyresult10_from_xloc1_dlyresult10_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult10_from_xloc1_dlyresult10_from_xloc1_vld();
        break;
    default:
        dly_from_xloc1     = ptr->getdlyresult1_from_xloc1_dlyresult1_from_xloc1();
        dly_from_xloc1_vld = ptr->getdlyresult1_from_xloc1_dlyresult1_from_xloc1_vld();
        break;
    }
}

void Meas_FPGA_RawData::get_dly_to_xloc2_pre( EngineMeasRet *ptr, MeasFPGA_DlyIndex index)
{
    switch (index)
    {
    case Dly1:
        dly_to_xloc2_pre     = ptr->getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre_vld();
        break;
    case Dly2:
        dly_to_xloc2_pre     = ptr->getdlyresult2_to_xloc2_pre_dlyresult2_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult2_to_xloc2_pre_dlyresult2_to_xloc2_pre_vld();
        break;
    case Dly3:
        dly_to_xloc2_pre     = ptr->getdlyresult3_to_xloc2_pre_dlyresult3_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult3_to_xloc2_pre_dlyresult3_to_xloc2_pre_vld();
        break;
    case Dly4:
        dly_to_xloc2_pre     = ptr->getdlyresult4_to_xloc2_pre_dlyresult4_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult4_to_xloc2_pre_dlyresult4_to_xloc2_pre_vld();
        break;
    case Dly5:
        dly_to_xloc2_pre     = ptr->getdlyresult5_to_xloc2_pre_dlyresult5_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult5_to_xloc2_pre_dlyresult5_to_xloc2_pre_vld();
        break;
    case Dly6:
        dly_to_xloc2_pre     = ptr->getdlyresult6_to_xloc2_pre_dlyresult6_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult6_to_xloc2_pre_dlyresult6_to_xloc2_pre_vld();
        break;
    case Dly7:
        dly_to_xloc2_pre     = ptr->getdlyresult7_to_xloc2_pre_dlyresult7_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult7_to_xloc2_pre_dlyresult7_to_xloc2_pre_vld();
        break;
    case Dly8:
        dly_to_xloc2_pre     = ptr->getdlyresult8_to_xloc2_pre_dlyresult8_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult8_to_xloc2_pre_dlyresult8_to_xloc2_pre_vld();
        break;
    case Dly9:
        dly_to_xloc2_pre     = ptr->getdlyresult9_to_xloc2_pre_dlyresult9_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult9_to_xloc2_pre_dlyresult9_to_xloc2_pre_vld();
        break;
    case Dly10:
        dly_to_xloc2_pre     = ptr->getdlyresult10_to_xloc2_pre_dlyresult10_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult10_to_xloc2_pre_dlyresult10_to_xloc2_pre_vld();
        break;
    default:
        dly_to_xloc2_pre     = ptr->getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre();
        dly_to_xloc2_pre_vld = ptr->getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre_vld();
        break;
    }
}

void Meas_FPGA_RawData::get_dly_to_xloc2( EngineMeasRet *ptr, MeasFPGA_DlyIndex index)
{
    switch (index)
    {
    case Dly1:
        dly_to_xloc2     = ptr->getdlyresult1_to_xloc2_dlyresult1_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult1_to_xloc2_dlyresult1_to_xloc2_vld();
        break;
    case Dly2:
        dly_to_xloc2     = ptr->getdlyresult2_to_xloc2_dlyresult2_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult2_to_xloc2_dlyresult2_to_xloc2_vld();
        break;
    case Dly3:
        dly_to_xloc2     = ptr->getdlyresult3_to_xloc2_dlyresult3_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult3_to_xloc2_dlyresult3_to_xloc2_vld();
        break;
    case Dly4:
        dly_to_xloc2     = ptr->getdlyresult4_to_xloc2_dlyresult4_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult4_to_xloc2_dlyresult4_to_xloc2_vld();
        break;
    case Dly5:
        dly_to_xloc2     = ptr->getdlyresult5_to_xloc2_dlyresult5_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult5_to_xloc2_dlyresult5_to_xloc2_vld();
        break;
    case Dly6:
        dly_to_xloc2     = ptr->getdlyresult6_to_xloc2_dlyresult6_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult6_to_xloc2_dlyresult6_to_xloc2_vld();
        break;
    case Dly7:
        dly_to_xloc2     = ptr->getdlyresult7_to_xloc2_dlyresult7_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult7_to_xloc2_dlyresult7_to_xloc2_vld();
        break;
    case Dly8:
        dly_to_xloc2     = ptr->getdlyresult8_to_xloc2_dlyresult8_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult8_to_xloc2_dlyresult8_to_xloc2_vld();
        break;
    case Dly9:
        dly_to_xloc2     = ptr->getdlyresult9_to_xloc2_dlyresult9_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult9_to_xloc2_dlyresult9_to_xloc2_vld();
        break;
    case Dly10:
        dly_to_xloc2     = ptr->getdlyresult10_to_xloc2_dlyresult10_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult10_to_xloc2_dlyresult10_to_xloc2_vld();
        break;
    default:
        dly_to_xloc2     = ptr->getdlyresult1_to_xloc2_dlyresult1_to_xloc2();
        dly_to_xloc2_vld = ptr->getdlyresult1_to_xloc2_dlyresult1_to_xloc2_vld();
        break;
    }
}

void Meas_FPGA_RawData::get_dly_to_xloc1_pre( EngineMeasRet *ptr, MeasFPGA_DlyIndex index)
{
    switch (index)
    {
    case Dly1:
        dly_to_xloc1_pre     = ptr->getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre_vld();
        break;
    case Dly2:
        dly_to_xloc1_pre     = ptr->getdlyresult2_to_xloc1_pre_dlyresult2_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult2_to_xloc1_pre_dlyresult2_to_xloc1_pre_vld();
        break;
    case Dly3:
        dly_to_xloc1_pre     = ptr->getdlyresult3_to_xloc1_pre_dlyresult3_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult3_to_xloc1_pre_dlyresult3_to_xloc1_pre_vld();
        break;
    case Dly4:
        dly_to_xloc1_pre     = ptr->getdlyresult4_to_xloc1_pre_dlyresult4_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult4_to_xloc1_pre_dlyresult4_to_xloc1_pre_vld();
        break;
    case Dly5:
        dly_to_xloc1_pre     = ptr->getdlyresult5_to_xloc1_pre_dlyresult5_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult5_to_xloc1_pre_dlyresult5_to_xloc1_pre_vld();
        break;
    case Dly6:
        dly_to_xloc1_pre     = ptr->getdlyresult6_to_xloc1_pre_dlyresult6_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult6_to_xloc1_pre_dlyresult6_to_xloc1_pre_vld();
        break;
    case Dly7:
        dly_to_xloc1_pre     = ptr->getdlyresult7_to_xloc1_pre_dlyresult7_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult7_to_xloc1_pre_dlyresult7_to_xloc1_pre_vld();
        break;
    case Dly8:
        dly_to_xloc1_pre     = ptr->getdlyresult8_to_xloc1_pre_dlyresult8_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult8_to_xloc1_pre_dlyresult8_to_xloc1_pre_vld();
        break;
    case Dly9:
        dly_to_xloc1_pre     = ptr->getdlyresult9_to_xloc1_pre_dlyresult9_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult9_to_xloc1_pre_dlyresult9_to_xloc1_pre_vld();
        break;
    case Dly10:
        dly_to_xloc1_pre     = ptr->getdlyresult10_to_xloc1_pre_dlyresult10_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult10_to_xloc1_pre_dlyresult10_to_xloc1_pre_vld();
        break;
    default:
        dly_to_xloc1_pre     = ptr->getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre();
        dly_to_xloc1_pre_vld = ptr->getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre_vld();
        break;
    }
}

void Meas_FPGA_RawData::get_dly_to_xloc1( EngineMeasRet *ptr, MeasFPGA_DlyIndex index)
{
    switch (index)
    {
    case Dly1:
        dly_to_xloc1     = ptr->getdlyresult1_to_xloc1_dlyresult1_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult1_to_xloc1_dlyresult1_to_xloc1_vld();
        break;
    case Dly2:
        dly_to_xloc1     = ptr->getdlyresult2_to_xloc1_dlyresult2_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult2_to_xloc1_dlyresult2_to_xloc1_vld();
        break;
    case Dly3:
        dly_to_xloc1     = ptr->getdlyresult3_to_xloc1_dlyresult3_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult3_to_xloc1_dlyresult3_to_xloc1_vld();
        break;
    case Dly4:
        dly_to_xloc1     = ptr->getdlyresult4_to_xloc1_dlyresult4_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult4_to_xloc1_dlyresult4_to_xloc1_vld();
        break;
    case Dly5:
        dly_to_xloc1     = ptr->getdlyresult5_to_xloc1_dlyresult5_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult5_to_xloc1_dlyresult5_to_xloc1_vld();
        break;
    case Dly6:
        dly_to_xloc1     = ptr->getdlyresult6_to_xloc1_dlyresult6_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult6_to_xloc1_dlyresult6_to_xloc1_vld();
        break;
    case Dly7:
        dly_to_xloc1     = ptr->getdlyresult7_to_xloc1_dlyresult7_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult7_to_xloc1_dlyresult7_to_xloc1_vld();
        break;
    case Dly8:
        dly_to_xloc1     = ptr->getdlyresult8_to_xloc1_dlyresult8_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult8_to_xloc1_dlyresult8_to_xloc1_vld();
        break;
    case Dly9:
        dly_to_xloc1     = ptr->getdlyresult9_to_xloc1_dlyresult9_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult9_to_xloc1_dlyresult9_to_xloc1_vld();
        break;
    case Dly10:
        dly_to_xloc1     = ptr->getdlyresult10_to_xloc1_dlyresult10_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult10_to_xloc1_dlyresult10_to_xloc1_vld();
        break;
    default:
        dly_to_xloc1     = ptr->getdlyresult1_to_xloc1_dlyresult1_to_xloc1();
        dly_to_xloc1_vld = ptr->getdlyresult1_to_xloc1_dlyresult1_to_xloc1_vld();
        break;
    }
}
