#ifndef MEAS_FPGA_RET_API
#define MEAS_FPGA_RET_API
#include "../../engine/base/enginecfg.h"


enum MeasFPGA_DlyIndex
{
    Dly_none,
    Dly1,
    Dly2,
    Dly3,
    Dly4,
    Dly5,
    Dly6,
    Dly7,
    Dly8,
    Dly9,
    Dly10,
};

struct Meas_FPGA_RawData
{
    unsigned int vmin;
    unsigned int vmax;
    unsigned int vbase;
    unsigned int vtop;

    unsigned int vupper;
    unsigned int vmid;
    unsigned int vlower;

    unsigned int xmax;
    unsigned int xmin;

    unsigned int rtimeA;
    unsigned int rtimeA_valid;
    unsigned int rtimeA_x2;
    unsigned int rtimeB;
    unsigned int rtimeB_valid;
    unsigned int rtimeB_x2;

    unsigned int ftimeA;
    unsigned int ftimeA_valid;
    unsigned int ftimeA_x2;
    unsigned int ftimeB;
    unsigned int ftimeB_valid;
    unsigned int ftimeB_x2;

    unsigned int cycle_x1;
    unsigned int cycle_x1_edge_type;
    unsigned int cycle_x1_valid;
    unsigned int cycle_x2;
    unsigned int cycle_x2_edge_type;
    unsigned int cycle_x2_valid;
    unsigned int cycle_x3;
    unsigned int cycle_x3_edge_type;
    unsigned int cycle_x3_valid;
    unsigned int cycle_x4;
    unsigned int cycle_x4_edge_type;
    unsigned int cycle_x4_valid;

    unsigned int ave_sum_bit31_0;
    unsigned int ave_sum_bit39_32;
    unsigned int ave_sum_1_bit31_0;
    unsigned int ave_sum_1_bit39_32;
    unsigned int ave_sum_2_bit31_0;
    unsigned int ave_sum_2_bit39_32;

    unsigned int vrms_sum_bit31_0;
    unsigned int vrms_sum_bit47_32;
    unsigned int vrms_sum_1_bit31_0;
    unsigned int vrms_sum_1_bit47_32;
    unsigned int vrms_sum_2_bit31_0;
    unsigned int vrms_sum_2_bit47_32;

    unsigned int rf_num;
    unsigned int rf_num_last_slope;
    unsigned int rf_num_valid;
    unsigned int edge_cnt_start_xloc;
    unsigned int edge_cnt_start_valid;
    unsigned int edge_cnt_end_xloc;
    unsigned int edge_cnt_end_valid;

    unsigned int edge_cnt_start2_xloc;
    unsigned int edge_cnt_start2_valid;
    unsigned int edge_cnt_end2_xloc;
    unsigned int edge_cnt_end2_valid;

    unsigned int overshoot;
    unsigned int preshoot;

    unsigned int dly_from_xloc2;
    unsigned int dly_from_xloc2_vld;
    unsigned int dly_from_xloc1;
    unsigned int dly_from_xloc1_vld;
    unsigned int dly_to_xloc2_pre;
    unsigned int dly_to_xloc2_pre_vld;
    unsigned int dly_to_xloc2;
    unsigned int dly_to_xloc2_vld;
    unsigned int dly_to_xloc1_pre;
    unsigned int dly_to_xloc1_pre_vld;
    unsigned int dly_to_xloc1;
    unsigned int dly_to_xloc1_vld;


    Meas_FPGA_RawData();

    void meas_getFPGARawData( Chan ch, EngineAdcCoreCH *adcCH, EngineMeasRet *ptr);

    void meas_getFPGARawData_cha( EngineMeasRet *ptr);
    void meas_getFPGARawData_chb( EngineMeasRet *ptr);
    void meas_getFPGARawData_chc( EngineMeasRet *ptr);
    void meas_getFPGARawData_chd( EngineMeasRet *ptr);

    void meas_getFPGARawData_la0( EngineMeasRet *ptr);
    void meas_getFPGARawData_la1( EngineMeasRet *ptr);
    void meas_getFPGARawData_la2( EngineMeasRet *ptr);
    void meas_getFPGARawData_la3( EngineMeasRet *ptr);
    void meas_getFPGARawData_la4( EngineMeasRet *ptr);
    void meas_getFPGARawData_la5( EngineMeasRet *ptr);
    void meas_getFPGARawData_la6( EngineMeasRet *ptr);
    void meas_getFPGARawData_la7( EngineMeasRet *ptr);

    void meas_getFPGARawData_la8( EngineMeasRet *ptr);
    void meas_getFPGARawData_la9( EngineMeasRet *ptr);
    void meas_getFPGARawData_la10( EngineMeasRet *ptr);
    void meas_getFPGARawData_la11( EngineMeasRet *ptr);
    void meas_getFPGARawData_la12( EngineMeasRet *ptr);
    void meas_getFPGARawData_la13( EngineMeasRet *ptr);
    void meas_getFPGARawData_la14( EngineMeasRet *ptr);
    void meas_getFPGARawData_la15( EngineMeasRet *ptr);

    void meas_getFPGARawData_dly( EngineMeasRet *ptr, MeasFPGA_DlyIndex index);
    void get_dly_from_xloc2( EngineMeasRet *ptr, MeasFPGA_DlyIndex index);
    void get_dly_from_xloc1( EngineMeasRet *ptr, MeasFPGA_DlyIndex index);
    void get_dly_to_xloc2_pre( EngineMeasRet *ptr, MeasFPGA_DlyIndex index);
    void get_dly_to_xloc2( EngineMeasRet *ptr, MeasFPGA_DlyIndex index);
    void get_dly_to_xloc1_pre( EngineMeasRet *ptr, MeasFPGA_DlyIndex index);
    void get_dly_to_xloc1( EngineMeasRet *ptr, MeasFPGA_DlyIndex index);
};



#endif // MEAS_FPGA_RET_API

