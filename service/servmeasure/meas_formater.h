#ifndef CMEASFORMATER_H
#define CMEASFORMATER_H

#include "../../com/scpiparse/cconverter.h"
using namespace scpi_parse;

class CMeasFormater:public CNR3Formater
{

public:
    CMeasFormater();
    ~CMeasFormater();

public:
    void startup();
    bool isWaiting();

    volatile static bool mCmdGetValid;
};

#endif // CMEASFORMATER_H
