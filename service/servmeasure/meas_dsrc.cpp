#include "servmeasure.h"
#include "meas_dsrc.h"

measDataDSrc::measDataDSrc(Chan srcA, Chan srcB, MeasType type, MeasFPGA_DlyIndex index)
{
    m_SrcA = srcA;
    m_SrcB = srcB;
    m_Type = type;


    m_FPGARawData = new Meas_FPGA_RawData();
    m_FPGADlyIndex = index;

    m_RawMeasDly.Delay = 0;
    m_RawMeasDly.EdgeChan = 1;
    m_RawMeasDly.EdgeX1 = 0;
    m_RawMeasDly.EdgeX2 = 0;

    m_Status = INVALID;
}

measDataDSrc::~measDataDSrc()
{
    Q_ASSERT( NULL != m_FPGARawData );
    delete m_FPGARawData;
}

void measDataDSrc::CalcRawMeasDly_SOFT(measData *pDataA, measData *pDataB)
{
    unsigned char *pSrcDataA = 0;
    unsigned char *pSrcDataB = 0;
    int s32Len = 0;
    int s32Base = 0;
    short s16Gnd1 = 0;
    short s16Gnd2 = 0;
    DsoWfm *wfmA, *wfmB;
    meas_algorithm::MEASURE *pCH_MA = 0;
    meas_algorithm::MEASURE *pCH_MB = 0;

    Q_ASSERT(pDataA != 0);
    Q_ASSERT(pDataB != 0);

    wfmA =   pDataA->getWfm();
    wfmB =   pDataB->getWfm();
    pCH_MA = pDataA->getRawMeasData();
    pCH_MB = pDataB->getRawMeasData();
    s16Gnd1 = wfmA->getyGnd();
    s16Gnd2 = wfmB->getyGnd();
    RegionType rType = pDataA->getRegionType();

    if(  (rType == REGION_MAIN)
       ||(rType == REGION_ZOOM) ) //! Meas Region(Main/Zoom): ALL Trace Data
    {
        pSrcDataA = wfmA->getPoint();
        pSrcDataB = wfmB->getPoint();
        s32Len = wfmA->size();
    }
    else if( rType == REGION_CURSOR) //! Meas Region: Trace Data between CursorA&B
    {
        int x1 = pDataA->getRangeX1();
        int x2 = pDataA->getRangeX2();
        int tracex1 = 0;
        int tracex2 = 0;

        if( pDataA->isZoomOn())
        {
            tracex1 = pDataA->getTraceX(x1, horizontal_view_zoom);
            tracex2 = pDataA->getTraceX(x2, horizontal_view_zoom);
        }
        else
        {
            tracex1 = pDataA->getTraceX(x1, horizontal_view_main);
            tracex2 = pDataA->getTraceX(x2, horizontal_view_main);
        }
        pSrcDataA = (wfmA->getPoint()) + tracex1;
        pSrcDataB = (wfmB->getPoint()) + tracex1;
        s32Len = tracex2 - tracex1 + 1;    }
    else //! Meas Region: Default All Trace Data
    {
        pSrcDataA = wfmA->getPoint();
        pSrcDataB = wfmB->getPoint();
        s32Len = wfmA->size();
    }

    s32Base = s32Len/2;

    short ret = 0;
    servMeasure::m_nWfmTraceID = wfmA->getAttrId();

    if((pSrcDataA != 0)&&(pSrcDataB != 0))
    {
        if(m_Type == Meas_DelayFF)
        {
            ret = meas_algorithm::MeasDelayFF( pSrcDataA,
                                               pSrcDataB,
                                               s32Base,
                                               s32Len,
                                               s16Gnd1,
                                               s16Gnd2,
                                               pCH_MA,
                                               pCH_MB,
                                               &m_RawMeasDly);
        }
        else if(m_Type == Meas_DelayRR)
        {
            ret = meas_algorithm::MeasDelayRR(  pSrcDataA,
                                                pSrcDataB,
                                                s32Base,
                                                s32Len,
                                                s16Gnd1,
                                                s16Gnd2,
                                                pCH_MA,
                                                pCH_MB,
                                                &m_RawMeasDly);
        }
        else if(m_Type == Meas_DelayRF)
        {
            ret = meas_algorithm::MeasDelayRF(  pSrcDataA,
                                                pSrcDataB,
                                                s32Base,
                                                s32Len,
                                                s16Gnd1,
                                                s16Gnd2,
                                                pCH_MA,
                                                pCH_MB,
                                                &m_RawMeasDly);
        }
        else if(m_Type == Meas_DelayFR)
        {
            ret = meas_algorithm::MeasDelayFR(  pSrcDataA,
                                                pSrcDataB,
                                                s32Base,
                                                s32Len,
                                                s16Gnd1,
                                                s16Gnd2,
                                                pCH_MA,
                                                pCH_MB,
                                                &m_RawMeasDly);
        }

        if( (ret == -1)||(m_RawMeasDly.Delay == 0) )
        {
            m_Status = NO_EDGE;
        }
        else
        {
            m_Status = VALID;
        }
    }
    else
    {
        if( (pSrcDataA == 0)&&(pSrcDataB == 0) )
        {
            m_Status = NO_SIGNAL;
        }
        else
        {
            m_Status = NO_EDGE;
        }
    }
}

void measDataDSrc::CalcRawMeasDly_FPGA(void *pFPGA_ret, measData *pDataA, measData *pDataB)
{
    if( (NULL != pFPGA_ret)&&(NULL != pDataA)&&(NULL != pDataB) )
    {
        int dataLen = pDataA->getDataLen();
        unsigned int refLoc = dataLen/(unsigned int)2;
        EngineMeasRet *pRet = (EngineMeasRet *)pFPGA_ret;

        m_FPGARawData->meas_getFPGARawData_dly( pRet, m_FPGADlyIndex);

        if( dly_xloc1_Valid() && dly_xloc2_Valid())
        {
            calc_closed_to_Ref( pDataA->getRealtInc(), refLoc);
        }
        else if( dly_xloc1_Valid())
        {
            //! xloc1 group
            calc_xloc1( pDataA->getRealtInc());
        }
        else if( dly_xloc2_Valid())
        {
            //! xloc2 group
            calc_xloc2( pDataA->getRealtInc());
        }
        else
        {
            //! Both xloc1 and xloc2 invalid
        }
    }
}

void measDataDSrc::calc_closed_to_Ref( float tInc, unsigned int refLoc)  //! check xloc1 and xloc2 valid before call this function
{
    unsigned int from_xloc_final = 0;
    unsigned int to_xloc_final = 0;

    if( qAbs(m_FPGARawData->dly_from_xloc2 - refLoc)<=qAbs(refLoc - m_FPGARawData->dly_from_xloc1) )
    {
        //! from_xloc2 closed to refLoc
        if(  qAbs(m_FPGARawData->dly_to_xloc2 - m_FPGARawData->dly_from_xloc2)
           <=qAbs(m_FPGARawData->dly_from_xloc2 - m_FPGARawData->dly_to_xloc2_pre) )
        {
            //! to_xloc2 closed to from_xloc2
            from_xloc_final = m_FPGARawData->dly_from_xloc2;
            to_xloc_final   = m_FPGARawData->dly_to_xloc2;
        }
        else
        {
            //! to_xloc2_pre closed to from_xloc2
            from_xloc_final = m_FPGARawData->dly_from_xloc2;
            to_xloc_final   = m_FPGARawData->dly_to_xloc2_pre;
        }
    }
    else
    {
        //! from_xloc1 closed to refLoc
        if(  qAbs(m_FPGARawData->dly_to_xloc1 - m_FPGARawData->dly_from_xloc1)
           <=qAbs(m_FPGARawData->dly_from_xloc1 - m_FPGARawData->dly_to_xloc1_pre) )
        {
            //! to_xloc1 closed to from_xloc1
            from_xloc_final = m_FPGARawData->dly_from_xloc1;
            to_xloc_final   = m_FPGARawData->dly_to_xloc1;
        }
        else
        {
            //! to_xloc1_pre closed to from_xloc1
            from_xloc_final = m_FPGARawData->dly_from_xloc1;
            to_xloc_final   = m_FPGARawData->dly_to_xloc1_pre;
        }
    }

    m_RawMeasDly.Delay = ((int)to_xloc_final - (int)from_xloc_final)*tInc;
    m_RawMeasDly.EdgeX1 = from_xloc_final;
    m_RawMeasDly.EdgeX2 = to_xloc_final;
}

void measDataDSrc::calc_xloc1( float tInc)
{
    unsigned int from_xloc_final = 0;
    unsigned int to_xloc_final = 0;

    if(  (m_FPGARawData->dly_to_xloc1_pre_vld == 1)     //! Both to_xloc1 and to_xloc1_pre valid
       &&(m_FPGARawData->dly_to_xloc1_vld == 1) )
    {
        if(  qAbs(m_FPGARawData->dly_to_xloc1 - m_FPGARawData->dly_from_xloc1)
           <=qAbs(m_FPGARawData->dly_from_xloc1 - m_FPGARawData->dly_to_xloc1_pre) )
        {
            //! to_xloc1 closed to from_xloc1
            from_xloc_final = m_FPGARawData->dly_from_xloc1;
            to_xloc_final   = m_FPGARawData->dly_to_xloc1;
        }
        else
        {
            //! to_xloc1_pre closed to from_xloc1
            from_xloc_final = m_FPGARawData->dly_from_xloc1;
            to_xloc_final   = m_FPGARawData->dly_to_xloc1_pre;
        }
    }
    else if( m_FPGARawData->dly_to_xloc1_pre_vld == 1)  //! to_xloc1_pre valid
    {
        from_xloc_final = m_FPGARawData->dly_from_xloc1;
        to_xloc_final   = m_FPGARawData->dly_to_xloc1_pre;
    }
    else if( m_FPGARawData->dly_to_xloc1_vld == 1)      //! to_xloc1 valid
    {
        from_xloc_final = m_FPGARawData->dly_from_xloc1;
        to_xloc_final   = m_FPGARawData->dly_to_xloc1;
    }
    else                                                 //! Both to_xloc1 and to_xloc1_pre invalid
    {}

    m_RawMeasDly.Delay = ((int)to_xloc_final - (int)from_xloc_final)*tInc;
    m_RawMeasDly.EdgeX1 = from_xloc_final;
    m_RawMeasDly.EdgeX2 = to_xloc_final;
}

void measDataDSrc::calc_xloc2(float tInc)
{
    unsigned int from_xloc_final = 0;
    unsigned int to_xloc_final = 0;

    if(  (m_FPGARawData->dly_to_xloc2_pre_vld == 1)     //! Both to_xloc2 and to_xloc2_pre valid
       &&(m_FPGARawData->dly_to_xloc2_vld == 1) )
    {
        if(  qAbs(m_FPGARawData->dly_to_xloc2 - m_FPGARawData->dly_from_xloc2)
           <=qAbs(m_FPGARawData->dly_from_xloc2 - m_FPGARawData->dly_to_xloc2_pre) )
        {
            //! to_xloc2 closed to from_xloc2
            from_xloc_final = m_FPGARawData->dly_from_xloc2;
            to_xloc_final   = m_FPGARawData->dly_to_xloc2;
        }
        else
        {
            //! to_xloc2_pre closed to from_xloc2
            from_xloc_final = m_FPGARawData->dly_from_xloc2;
            to_xloc_final   = m_FPGARawData->dly_to_xloc2_pre;
        }
    }
    else if( m_FPGARawData->dly_to_xloc2_pre_vld == 1)  //! to_xloc2_pre valid
    {
        from_xloc_final = m_FPGARawData->dly_from_xloc2;
        to_xloc_final   = m_FPGARawData->dly_to_xloc2_pre;
    }
    else if( m_FPGARawData->dly_to_xloc2_vld == 1)      //! to_xloc2 valid
    {
        from_xloc_final = m_FPGARawData->dly_from_xloc2;
        to_xloc_final   = m_FPGARawData->dly_to_xloc2;
    }
    else                                                 //! Both to_xloc2 and to_xloc2_pre invalid
    {}

    m_RawMeasDly.Delay = ((int)to_xloc_final - (int)from_xloc_final)*tInc;
    m_RawMeasDly.EdgeX1 = from_xloc_final;
    m_RawMeasDly.EdgeX2 = to_xloc_final;
}

bool measDataDSrc::dly_xloc1_Valid()
{
    if( m_FPGARawData->dly_from_xloc1_vld == 1)
    {
        if(  (m_FPGARawData->dly_to_xloc1_pre_vld == 1)
           ||(m_FPGARawData->dly_to_xloc1_vld == 1) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool measDataDSrc::dly_xloc2_Valid()
{
    if( m_FPGARawData->dly_from_xloc2_vld == 1)
    {
        if(  (m_FPGARawData->dly_to_xloc2_pre_vld == 1)
           ||(m_FPGARawData->dly_to_xloc2_vld == 1) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void measDataDSrc::CalcRealMeasDly_SOFT(measData *pDataA, measData *pDataB)
{
    if(pDataA->isValid()&&pDataB->isValid())
    {
        CalcRawMeasDly_SOFT(pDataA, pDataB);
    }
    else
    {
        m_Status = NO_EDGE;
    }

    if(m_Type == Meas_DelayFF)
    {
        calc_Delay(Meas_DelayFF, pDataA, pDataB);
        calc_Phase(Meas_PhaseFF, pDataA, pDataB);
    }
    else if(m_Type == Meas_DelayRR)
    {
        calc_Delay(Meas_DelayRR, pDataA, pDataB);
        calc_Phase(Meas_PhaseRR, pDataA, pDataB);
    }
    else if(m_Type == Meas_DelayRF)
    {
        calc_Delay(Meas_DelayRF, pDataA, pDataB);
        calc_Phase(Meas_PhaseRF, pDataA, pDataB);
    }
    else if(m_Type == Meas_DelayFR)
    {
        calc_Delay(Meas_DelayFR, pDataA, pDataB);
        calc_Phase(Meas_PhaseFR, pDataA, pDataB);
    }
    else
    {
        calc_Delay(Meas_DelayFF, pDataA, pDataB);
        calc_Phase(Meas_PhaseFF, pDataA, pDataB);
    }
}

void measDataDSrc::CalcRealMeasDly_FPGA(void *pFPGA_ret, measData *pDataA, measData *pDataB)
{
    if( (NULL != pFPGA_ret)&&(NULL != pDataA)&&(NULL != pDataB) )
    {
        CalcRawMeasDly_FPGA( pFPGA_ret, pDataA, pDataB);
    }

    if(m_Type == Meas_DelayFF)
    {
        calc_Delay(Meas_DelayFF, pDataA, pDataB);
        calc_Phase(Meas_PhaseFF, pDataA, pDataB);
    }
    else if(m_Type == Meas_DelayRR)
    {
        calc_Delay(Meas_DelayRR, pDataA, pDataB);
        calc_Phase(Meas_PhaseRR, pDataA, pDataB);
    }
    else if(m_Type == Meas_DelayRF)
    {
        calc_Delay(Meas_DelayRF, pDataA, pDataB);
        calc_Phase(Meas_PhaseRF, pDataA, pDataB);
    }
    else if(m_Type == Meas_DelayFR)
    {
        calc_Delay(Meas_DelayFR, pDataA, pDataB);
        calc_Phase(Meas_PhaseFR, pDataA, pDataB);
    }
    else
    {
        calc_Delay(Meas_DelayFF, pDataA, pDataB);
        calc_Phase(Meas_PhaseFF, pDataA, pDataB);
    }
}

void measDataDSrc::calc_Delay(MeasType type, measData *pDataA, measData *pDataB)
{
    MeasValue val;
    val.mUnit = Unit_s;
    if( pDataA->isValid()&&pDataB->isValid()
       &&(m_Status == VALID) )
    {
        val.mVal = m_RawMeasDly.Delay;
        val.mStatus = VALID;

        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasDly.EdgeX1, pDataA);
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(pDataA->getRawMeasData()->Vmid,
                               pDataA);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasDly.EdgeX2, pDataB);
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(pDataB->getRawMeasData()->Vmid,
                               pDataB);

        m_RealMeasDly[type] = val;
//        qDebug()<<"measDataDSrc::calc_Delay m_RawMeasDly.EdgeX1 "<<m_RawMeasDly.EdgeX1;
//        qDebug()<<"measDataDSrc::calc_Delay m_RawMeasDly.EdgeX2 "<<m_RawMeasDly.EdgeX2;

    }
    else
    {
        val.mVal = 9.9E+37;
        val.mStatus = m_Status;

        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = false;
        val.mAy.mPos = 0;

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;

        m_RealMeasDly[type] = val;
    }
}

void measDataDSrc::calc_Phase(MeasType type, measData *pDataA, measData *pDataB)
{
    MeasValue val;
    val.mUnit = Unit_degree;
    if(  pDataA->isValid()&&pDataB->isValid()
       &&(m_Status == VALID) )
    {
        if(pDataA->getRawMeasData()->Period != 0)
        {
            val.mVal = m_RawMeasDly.Delay*360/(pDataA->getRawMeasData()->Period);
            val.mStatus = VALID;

            val.mAx.bVisible = true;
            val.mAx.mPos = getXPos(m_RawMeasDly.EdgeX1, pDataA);
            val.mAy.bVisible = true;
            val.mAy.mPos = getYPos(pDataA->getRawMeasData()->Vmid,
                                   pDataA);

            val.mBx.bVisible = true;
            val.mBx.mPos = getXPos(m_RawMeasDly.EdgeX2, pDataB);
            val.mBy.bVisible = true;
            val.mBy.mPos = getYPos(pDataB->getRawMeasData()->Vmid,
                                   pDataB);

            m_RealMeasDly[type] = val;
        }
        else
        {
            val.mVal = 9.9E+37;
            val.mStatus = NO_PERIOD;

            val.mAx.bVisible = false;
            val.mAx.mPos = 0;
            val.mAy.bVisible = false;
            val.mAy.mPos = 0;

            val.mBx.bVisible = false;
            val.mBx.mPos = 0;
            val.mBy.bVisible = false;
            val.mBy.mPos = 0;

            m_RealMeasDly[type] = val;
        }
    }
    else
    {
        val.mVal = 9.9E+37;
        val.mStatus = m_Status;

        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = false;
        val.mAy.mPos = 0;

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;

        m_RealMeasDly[type] = val;
    }
}

//! Get pix position of wav point X
//! Range: 0--999 (X Pix: 1000)
int measDataDSrc::getXPos(int index, measData *pData)
{
    int XPos = 0;
    RegionType rType = pData->getRegionType();
    int x1 = pData->getRangeX1();

    if( rType == REGION_CURSOR)
    {
        if( pData->isZoomOn())
        {
            index += (pData->getTraceX( x1, horizontal_view_zoom));
        }
        else
        {
            index += (pData->getTraceX( x1, horizontal_view_main));
        }
    }

    float Xreal = index * (pData->getRealtInc()) + (pData->getRealt0());

    horiAttr hAttr;
    if( rType == REGION_MAIN)
    {
        hAttr = getHoriAttr(pData->getMeasSrc(), horizontal_view_main);
    }
    else
    {
        if( pData->isZoomOn())
        {
            hAttr = getHoriAttr(pData->getMeasSrc(), horizontal_view_zoom);
        }
        else
        {
            hAttr = getHoriAttr(pData->getMeasSrc(), horizontal_view_main);
        }
    }

    XPos = qRound(Xreal / hAttr.inc + hAttr.gnd);

    return XPos;
}

//! Get pix position of wav point Y
//! Range:0--479 (Y Pix: 480)
int measDataDSrc::getYPos(int yAdc, measData *pData)
{
    int YPos = 0;

    float scale  = pData->getRealScale();
    float offset = pData->getRealOffset();

    float Yreal = yAdc * pData->getRealyInc();
    YPos = 240 - qRound((Yreal + offset)/scale*scr_vdiv_dots);

    return YPos;
}

MeasValue measDataDSrc::getRealMeasData(MeasType type)
{
    MeasValue value;
    value = m_RealMeasDly.value(type);
    return value;
}

MeasStatus measDataDSrc::getMeasStatus(MeasType type)
{
    return m_RealMeasDly.value(type).mStatus;
}
