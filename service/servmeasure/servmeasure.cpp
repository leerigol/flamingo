#include "servmeasure.h"
#include "../../include/dsoassert.h"
#include "../servscpi/servscpidso.h"
#include "../servvert/servvert.h"
#include "../servtrace/servtrace.h"
#include "../servgui/servgui.h"
#include "../servUPA/powerQuality.h"
#include "../servUPA/ripple.h"
#include "../servhori/servhori.h"
#include "../servvertmgr/servvertmgr.h"
#include "../servutility/servutility.h"
#include "../servHisto/servhisto.h"

#include "meas_formater.h"

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";

#define def_time()
#define start_time()
#define end_time()

#define dbg_out()

IMPLEMENT_CMD( serviceExecutor, servMeasure )
start_of_entry()

on_set_int_bool ( MSG_APP_MEASURE,                 &servMeasure::set_AppShow ),
on_get_bool     ( MSG_APP_MEASURE,                 &servMeasure::get_AppShow ),

on_set_int_int  ( MSG_APP_MEAS_SRCA,               &servMeasure::set_SrcA ),
on_get_int      ( MSG_APP_MEAS_SRCA,               &servMeasure::get_SrcA ),
on_set_int_int  ( MSG_APP_MEAS_SRCB,               &servMeasure::set_SrcB ),
on_get_int      ( MSG_APP_MEAS_SRCB,               &servMeasure::get_SrcB ),
on_set_int_int  ( MSG_APP_MEAS_CAT,                &servMeasure::set_Category ),
on_get_int      ( MSG_APP_MEAS_CAT,                &servMeasure::get_Category ),

on_set_int_int  ( MSG_APP_MEAS_ALL_SRC,            &servMeasure::set_MeasAllSrc ),
on_get_int      ( MSG_APP_MEAS_ALL_SRC,            &servMeasure::get_MeasAllSrc ),

on_set_int_void ( MSG_APP_MEAS_TO_REMOVE,          &servMeasure::set_To_RemoveMenu ),
//on_set_int_void ( MSG_APP_MEAS_TO_ADD,             &servMeasure::set_To_AddMenu ),

on_set_int_int  ( MSG_APP_MEAS_RANGE_MODE,         &servMeasure::set_RangeMode ),
on_get_int      ( MSG_APP_MEAS_RANGE_MODE,         &servMeasure::get_RangeMode ),
on_set_int_int  ( MSG_APP_MEAS_SET_TYPE,           &servMeasure::set_SetType ),
on_get_int      ( MSG_APP_MEAS_SET_TYPE,           &servMeasure::get_SetType ),

on_set_int_int  ( MSG_APP_MEAS_TH_SRC,             &servMeasure::set_ThresholdSrc ),
on_get_int      ( MSG_APP_MEAS_TH_SRC,             &servMeasure::get_ThresholdSrc ),

on_set_int_int  ( MSG_APP_MEAS_TH_HIGH_TYPE,       &servMeasure::set_Th_High_Type ),
on_get_int      ( MSG_APP_MEAS_TH_HIGH_TYPE,       &servMeasure::get_Th_High_Type ),
on_set_int_ll   ( MSG_APP_MEAS_TH_HIGH_TYPE_SUB_1, &servMeasure::set_Th_High ),
on_get_ll       ( MSG_APP_MEAS_TH_HIGH_TYPE_SUB_1, &servMeasure::get_Th_high ),

on_set_int_int  ( MSG_APP_MEAS_TH_MID_TYPE,        &servMeasure::set_Th_Mid_Type ),
on_get_int      ( MSG_APP_MEAS_TH_MID_TYPE,        &servMeasure::get_Th_Mid_Type ),
on_set_int_ll   ( MSG_APP_MEAS_TH_MID_TYPE_SUB_1,  &servMeasure::set_Th_Mid ),
on_get_ll       ( MSG_APP_MEAS_TH_MID_TYPE_SUB_1,  &servMeasure::get_Th_Mid ),

on_set_int_int  ( MSG_APP_MEAS_TH_LOW_TYPE,        &servMeasure::set_Th_Low_Type ),
on_get_int      ( MSG_APP_MEAS_TH_LOW_TYPE,        &servMeasure::get_Th_Low_Type ),
on_set_int_ll   ( MSG_APP_MEAS_TH_LOW_TYPE_SUB_1,  &servMeasure::set_Th_Low ),
on_get_ll       ( MSG_APP_MEAS_TH_LOW_TYPE_SUB_1,  &servMeasure::get_Th_Low ),

on_get_int      ( servMeasure::cmd_get_Th_HighPos, &servMeasure::get_Th_HighPos),
on_get_int      ( servMeasure::cmd_get_Th_MidPos,  &servMeasure::get_Th_MidPos),
on_get_int      ( servMeasure::cmd_get_Th_LowPos,  &servMeasure::get_Th_LowPos),

on_set_int_void(  MSG_APP_MEAS_TH_DEFAULT,         &servMeasure::set_Th_Default ),

on_set_int_bool ( MSG_APP_MEAS_SET_STATE_METHOD,   &servMeasure::set_Method ),
on_get_bool     ( MSG_APP_MEAS_SET_STATE_METHOD,   &servMeasure::get_Method ),
on_set_int_int  ( MSG_APP_MEAS_SET_TOP_METHOD,     &servMeasure::set_Top_Method ),
on_get_int      ( MSG_APP_MEAS_SET_TOP_METHOD,     &servMeasure::get_Top_Method ),
on_set_int_int  ( MSG_APP_MEAS_SET_BASE_METHOD,    &servMeasure::set_Base_Method ),
on_get_int      ( MSG_APP_MEAS_SET_BASE_METHOD,    &servMeasure::get_Base_Method ),

on_set_int_int  ( MSG_APP_MEAS_RANGE_MODE,         &servMeasure::set_RangeMode ),
on_get_int      ( MSG_APP_MEAS_RANGE_MODE,         &servMeasure::get_RangeMode ),
on_set_int_int  ( MSG_APP_MEAS_REGION,             &servMeasure::set_Region ),
on_get_int      ( MSG_APP_MEAS_REGION,             &servMeasure::get_Region ),

on_set_int_int  ( MSG_APP_MEAS_RANGE_CURSOR_AX,    &servMeasure::set_CursorA ),
on_get_int      ( MSG_APP_MEAS_RANGE_CURSOR_AX,    &servMeasure::get_CursorA ),
on_set_int_int  ( MSG_APP_MEAS_RANGE_CURSOR_BX,    &servMeasure::set_CursorB ),
on_get_int      ( MSG_APP_MEAS_RANGE_CURSOR_BX,    &servMeasure::get_CursorB ),
on_set_int_int  ( MSG_APP_MEAS_RANGE_CURSOR_ABX,   &servMeasure::set_CursorAB ),
on_get_int      ( MSG_APP_MEAS_RANGE_CURSOR_ABX,   &servMeasure::get_CursorAB ),

on_set_int_bool ( MSG_APP_MEAS_SET_STATE_METHOD,   &servMeasure::set_Method ),
on_get_bool     ( MSG_APP_MEAS_SET_STATE_METHOD,   &servMeasure::get_Method ),
on_set_int_int  ( MSG_APP_MEAS_SET_TOP_METHOD,     &servMeasure::set_Top_Method ),
on_get_int      ( MSG_APP_MEAS_SET_TOP_METHOD,     &servMeasure::get_Top_Method ),
on_set_int_int  ( MSG_APP_MEAS_SET_BASE_METHOD,    &servMeasure::set_Base_Method ),
on_get_int      ( MSG_APP_MEAS_SET_BASE_METHOD,    &servMeasure::get_Base_Method ),

on_set_int_void ( MSG_APP_MEAS_CLEAR_ONE,          &servMeasure::set_Delete ),
on_set_int_void ( MSG_APP_MEAS_CLEAR_ALL,          &servMeasure::set_DeleteAll ),

on_set_int_bool ( MSG_APP_MEAS_STAT_ENABLE,        &servMeasure::set_StatEnable ),
on_get_bool     ( MSG_APP_MEAS_STAT_ENABLE,        &servMeasure::get_StatEnable ),
on_set_int_bool ( MSG_APP_MEAS_TREND_ENABLE,       &servMeasure::set_TrendEnable ),
on_get_bool     ( MSG_APP_MEAS_TREND_ENABLE,       &servMeasure::get_TrendEnable ),

on_set_int_void ( MSG_APP_MEAS_STAT_RESET,         &servMeasure::set_StatReset ),
on_set_int_int  ( MSG_APP_MEAS_STAT_COUNT,         &servMeasure::set_StatCount ),
on_get_int      ( MSG_APP_MEAS_STAT_COUNT,         &servMeasure::get_StatCount ),

on_set_int_void(  MSG_APP_MEAS_COUNTER_MENU,       &servMeasure::set_Counter ),
on_set_int_void(  MSG_APP_MEAS_DVM_MENU,           &servMeasure::set_DVM ),
on_set_int_void(  MSG_APP_MEAS_UPA_MENU,           &servMeasure::set_UPA ),
on_set_int_void(  MSG_APP_MEAS_EYE_MENU,           &servMeasure::set_Eye_Jit ),
on_set_int_void(  MSG_APP_MEAS_HISTO_MENU,         &servMeasure::set_HISTO),
on_set_int_void(  MSG_APP_MEAS_ZONE_MENU,          &servMeasure::set_Zone_Trig),

on_set_int_bool(  MSG_APP_MEAS_INDICATOR,          &servMeasure::set_Indicator ),
on_get_bool    (  MSG_APP_MEAS_INDICATOR,          &servMeasure::get_Indicator ),
on_set_int_void(  MSG_APP_MEAS_TEST,               &servMeasure::set_Test ),

on_set_int_void ( servMeasure::MSG_TO_ADD_MENU,       &servMeasure::toAddMenu ),

on_set_int_arg  ( servMeasure::MSG_ADD_STAT_ITEM,     &servMeasure::addStatItem),
on_set_int_int  ( servMeasure::MSG_DEL_STAT_ITEM,     &servMeasure::delStatItem),
on_set_int_int  ( servMeasure::MSG_DEL_ALL_STAT_ITEM, &servMeasure::delAllStatItem),

on_set_int_bool ( servMeasure::MSG_UPDATE_MEAS,    &servMeasure::set_UpdateMeas),
on_get_pointer  ( servMeasure::MSG_UPDATE_MEAS,    &servMeasure::get_UpdateMeas),

on_get_bool     ( servMeasure::cmd_meas_val_ready,  &servMeasure::cmd_GetMeasValueReady),

on_get_pointer  ( servMeasure::MSG_ALL_MEAS_DATA,  &servMeasure::get_AllMeasData),
on_get_pointer  ( servMeasure::MSG_STAT_MEAS_DATA, &servMeasure::get_MeasStatData),

on_set_int_pointer( servMeasure::MSG_TRACE_UPDATED, &servMeasure::setMeasSrcReady),

/*******scpi test********/
on_set_int_arg  (      servMeasure::MSG_CMD_SET_MEAS,             &servMeasure::cmd_SetMeas ),
on_get_void_argi_argo( servMeasure::MSG_CMD_GET_MEAS,             &servMeasure::cmd_GetMeas ),
on_set_int_arg  (      servMeasure::MSG_CMD_SET_STAT_MEAS,        &servMeasure::cmd_SetStatMeas ),
on_get_void_argi_argo( servMeasure::MSG_CMD_GET_STAT_MEAS,        &servMeasure::cmd_GetStatMeas ),

on_get_float_arg(      servMeasure::MSG_CMD_GET_EPOS_SA,          &servMeasure::cmd_GetEposSA ),
on_get_float_arg(      servMeasure::MSG_CMD_GET_EPOS_SB,          &servMeasure::cmd_GetEposSB ),

on_get_bool     (      servMeasure::MSG_CMD_GET_VALUE_STATUS,     &servMeasure::cmd_GetValueStatus),
on_get_void_argo(      servMeasure::MSG_CMD_GET_VALUE,            &servMeasure::cmd_GetMeasValue),

on_get_void_argo(      servMeasure::MSG_CMD_GET_VALUE_ITEM,       &servMeasure::cmd_GetValueItem),
on_set_int_arg (       servMeasure::MSG_CMD_ADD_VALUE_ITEM,       &servMeasure::cmd_AddValueItem),
on_set_int_int (       servMeasure::MSG_CMD_CLEAR,                &servMeasure::cmd_Clear ),
on_set_int_int (       servMeasure::MSG_CMD_SEL_ITEM,         &servMeasure::cmd_SetSelItem ),
on_get_int(            servMeasure::MSG_CMD_SEL_ITEM,         &servMeasure::cmd_GetSelItem),

//! spy on
on_set_void_void( servMeasure::cmd_hor_setting_change,       &servMeasure::on_horizontal_changed ),
on_set_void_void( servMeasure::cmd_on_ch_active,             &servMeasure::on_ChActive ),
on_set_void_void( servMeasure::cmd_hor_zoom_on,              &servMeasure::on_HorZoomOn ),

on_set_void_void( servMeasure::cmd_ch1_setting_change,       &servMeasure::on_Ch1Changed),
on_set_void_void( servMeasure::cmd_ch2_setting_change,       &servMeasure::on_Ch2Changed),
on_set_void_void( servMeasure::cmd_ch3_setting_change,       &servMeasure::on_Ch3Changed),
on_set_void_void( servMeasure::cmd_ch4_setting_change,       &servMeasure::on_Ch4Changed),

on_set_void_void( servMeasure::cmd_on_ch1_on_off,            &servMeasure::on_OnOffCh1 ),
on_set_void_void( servMeasure::cmd_on_ch2_on_off,            &servMeasure::on_OnOffCh2 ),
on_set_void_void( servMeasure::cmd_on_ch3_on_off,            &servMeasure::on_OnOffCh3 ),
on_set_void_void( servMeasure::cmd_on_ch4_on_off,            &servMeasure::on_OnOffCh4 ),

on_set_void_void( servMeasure::cmd_on_math1_on_off,          &servMeasure::on_OnOffMath1 ),
on_set_void_void( servMeasure::cmd_on_math2_on_off,          &servMeasure::on_OnOffMath2 ),
on_set_void_void( servMeasure::cmd_on_math3_on_off,          &servMeasure::on_OnOffMath3 ),
on_set_void_void( servMeasure::cmd_on_math4_on_off,          &servMeasure::on_OnOffMath4 ),

on_set_void_void( servMeasure::cmd_on_la_on_off,             &servMeasure::on_LaOnOff ),
on_set_void_void( servMeasure::cmd_on_histo_meas_en,         &servMeasure::on_HistoMeasEn ),

on_set_void_void( servLicense::cmd_Opt_Active,               &servMeasure::on_License),
on_set_void_void( servLicense::cmd_Opt_Exp,                  &servMeasure::on_License),
on_set_void_void( servLicense::cmd_Opt_Invalid,              &servMeasure::on_License),

on_set_void_void( MSG_DISPLAY_CLEAR,                         &servMeasure::on_DisplayClear),
on_set_void_void( MSG_HORIZONTAL_RUN,                        &servMeasure::on_RunStop),
on_set_void_void( MSG_TRIGGER_SWEEP,                         &servMeasure::on_TrigSweep),
//on_set_void_void( MSG_HOR_TIME_MODE,                         &servMeasure::on_Timemode),

//on_set_void_arg( servMeasure::cmd_set_FPGA_CntType,          &servMeasure::setFPGA_CntType ),
//on_set_int_bool ( servMeasure::MSG_SHOW_ITEMBOX,  &servMeasure::onShowItemBox)

//! Histogram
on_get_pointer(  servMeasure::cmd_get_histo_meas,   &servMeasure::getHistoMeas ),
on_set_void_int( servMeasure::cmd_set_select_item,  &servMeasure::setHistoSelectItem ),

on_get_int      ( servMeasure::MSG_GET_SUB_MENU,   &servMeasure::getSubMenu),
on_set_void_void( CMD_SERVICE_INIT,                &servMeasure::init ),
on_set_void_void( CMD_SERVICE_RST,                 &servMeasure::rst ),
on_set_int_void ( CMD_SERVICE_STARTUP,             &servMeasure::startup ),
on_set_int_int  ( CMD_SERVICE_ACTIVE,              &servMeasure::onActive ),
on_set_int_int  ( CMD_SERVICE_SUB_ENTER,           &servMeasure::onEnterSubMenu ),
on_set_int_int  ( CMD_SERVICE_SUB_RETURN,          &servMeasure::onReturnMainMenu ),
on_set_int_void ( CMD_SERVICE_EXIT_ACTIVE ,        &servMeasure::on_enterActive ),
on_set_int_void ( CMD_SERVICE_TIMEOUT ,            &servMeasure::onTraceTimeout ),

//upa enable
on_set_int_bool ( servMeasure::cmd_meas_upa_show,  &servMeasure::set_UPA_Visable ),////////
end_of_entry()

Chan servMeasure::_ThresholdSrcList[] =
{
  chan1,chan2,chan3,chan4,
  m1, m2, m3, m4,
  r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,
};

int servMeasure::m_nConfigEngineID = 0;
int servMeasure::m_nWfmTraceID     = 0;
bool servMeasure::m_bSoftCalc      = true;
int  servMeasure::m_nRemoteList    = 0;
bool servMeasure::m_bTraceTimeout  = false;

servMeasure::servMeasure( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    mVersion = 6;
    serviceExecutor::baseInit();

    initThresholdList();

    //! delete by link
    CMeasFormater fmt;
    CVal val;
    fmt.format( val );

    //! do not change setting
    setMsgAttr( (int)servMeasure::MSG_UPDATE_MEAS );
//    setMsgAttr( (int)servMeasure::MSG_ALL_MEAS_DATA );
//    setMsgAttr( (int)servMeasure::MSG_STAT_MEAS_DATA );

    //FPGA Data
    m_FPGAMeasCfg = new EngineMeasCfg();
    m_FPGAMeasRet = new EngineMeasRet();
}

int servMeasure::serialOut( CStream &stream, serialVersion &ver )
{
    ver = mVersion;
    stream.write(QStringLiteral("m_SrcA"),       m_SrcA);
    stream.write(QStringLiteral("m_SrcB"),       m_SrcB);
    stream.write(QStringLiteral("m_MeasAllSrc"), m_MeasAllSrc);

    stream.write(QStringLiteral("m_Range_Mode"), m_Range_Mode);
    stream.write(QStringLiteral("m_Set_Type"),   m_Set_Type);

    stream.write(QStringLiteral("m_ThresholdSrc"),  m_ThresholdSrc);
    stream.write(QStringLiteral("m_TH_Type"),  m_TH_Type);
    stream.write(QStringLiteral("m_HighPer"),  m_HighPer);
    stream.write(QStringLiteral("m_MidPer"),   m_MidPer);
    stream.write(QStringLiteral("m_LowPer"),   m_LowPer);
    stream.write(QStringLiteral("m_HighAbs"),  m_HighAbs);
    stream.write(QStringLiteral("m_MidAbs"),   m_MidAbs);
    stream.write(QStringLiteral("m_LowAbs"),   m_LowAbs);

    stream.write(QStringLiteral("m_Region"),   m_Region);
    stream.write(QStringLiteral("m_CursorA"),  m_CursorA);
    stream.write(QStringLiteral("m_CursorB"),  m_CursorB);
    stream.write(QStringLiteral("m_CursorAB"), m_CursorAB);

    stream.write(QStringLiteral("m_Method"),      m_Method);
    stream.write(QStringLiteral("m_Top_Method"),  m_Top_Method);
    stream.write(QStringLiteral("m_Base_Method"), m_Base_Method);

    stream.write(QStringLiteral("m_StatEnable"),  m_StatEnable);
    stream.write(QStringLiteral("m_TrendEnable"), m_TrendEnable);
    stream.write(QStringLiteral("m_StatCount"),   m_StatCount);

    stream.write(QStringLiteral("m_Indicator"),  m_Indicator);

    stream.write(QStringLiteral("m_bAppShow"),   m_bAppShow);
    stream.write(QStringLiteral("m_SubMenuMsg"), m_SubMenuMsg);
    stream.write(QStringLiteral("m_ZoomOn"),     m_ZoomOn);

    m_MeasDataMutex.lock();

    m_MeasStatData_Count = m_MeasStatData.size();
    stream.write(QStringLiteral("m_MeasStatData_Count"), m_MeasStatData_Count);

    for( int i=0; i<m_MeasStatData.size(); i++)
    {
        MeasType type = list_at( m_MeasStatData,i)->getType();
        Chan srcA = list_at( m_MeasStatData,i)->getSrcA();
        Chan srcB = list_at( m_MeasStatData,i)->getSrcB();
        QString name = QString("m_MeasStatData%1_type").arg(i);
        stream.write( name, type);

        name = QString("m_MeasStatData%1_SrcA").arg(i);
        stream.write( name, srcA);

        name = QString("m_MeasStatData%1_SrcB").arg(i);
        stream.write( name, srcB);
    }
    m_MeasDataMutex.unlock();

    return ERR_NONE;
}

int servMeasure::serialIn( CStream &stream, serialVersion ver )
{
    if( ver == mVersion)
    {
        stream.read(QStringLiteral("m_SrcA"),       m_SrcA);
        stream.read(QStringLiteral("m_SrcB"),       m_SrcB);

        stream.read(QStringLiteral("m_MeasAllSrc"), m_MeasAllSrc);

        stream.read(QStringLiteral("m_Range_Mode"), m_Range_Mode);
        stream.read(QStringLiteral("m_Set_Type"),   m_Set_Type);

        stream.read(QStringLiteral("m_ThresholdSrc"),  m_ThresholdSrc);
        stream.read(QStringLiteral("m_TH_Type"),  m_TH_Type);
        stream.read(QStringLiteral("m_HighPer"),  m_HighPer);
        stream.read(QStringLiteral("m_MidPer"),   m_MidPer);
        stream.read(QStringLiteral("m_LowPer"),   m_LowPer);
        stream.read(QStringLiteral("m_HighAbs"),  m_HighAbs);
        stream.read(QStringLiteral("m_MidAbs"),   m_MidAbs);
        stream.read(QStringLiteral("m_LowAbs"),   m_LowAbs);

        stream.read(QStringLiteral("m_Region"),   m_Region);
        stream.read(QStringLiteral("m_CursorA"),  m_CursorA);
        stream.read(QStringLiteral("m_CursorB"),  m_CursorB);
        stream.read(QStringLiteral("m_CursorAB"), m_CursorAB);

        stream.read(QStringLiteral("m_Method"),      m_Method);
        stream.read(QStringLiteral("m_Top_Method"),  m_Top_Method);
        stream.read(QStringLiteral("m_Base_Method"), m_Base_Method);

        stream.read(QStringLiteral("m_StatEnable"),  m_StatEnable);
        stream.read(QStringLiteral("m_TrendEnable"), m_TrendEnable);
        stream.read(QStringLiteral("m_StatCount"),   m_StatCount);

        stream.read(QStringLiteral("m_Indicator"),  m_Indicator);

        stream.read(QStringLiteral("m_bAppShow"),   m_bAppShow);
        stream.read(QStringLiteral("m_SubMenuMsg"), m_SubMenuMsg);
        stream.read(QStringLiteral("m_ZoomOn"),     m_ZoomOn);

        stream.read(QStringLiteral("m_MeasStatData_Count"), m_MeasStatData_Count);

        m_CmdMeasValue = 0.0;

        m_CmdMeasType  = Meas_Period;
        m_CmdStatType  = STAT_CURR;
        m_CmdSrcA      = (Chan)m_SrcA;
        m_CmdSrcB      = (Chan)m_SrcB;
        m_CmdSelItem   = (int)ALL_ITEM;

        m_MeasValueReady = false;

        m_CmdEposType  = EPOS_SA;

        resetThresholdList();

        int type;
        int srcA, srcB;
//        qDebug()<<"servMeasure::serialIn  m_MeasStatData_Count: "<<m_MeasStatData_Count;
        for( int i=0; i<m_MeasStatData_Count; i++)
        {
            QString name = QString("m_MeasStatData%1_type").arg(i);
            stream.read( name, type);

            name = QString("m_MeasStatData%1_SrcA").arg(i);
            stream.read( name, srcA);

            name = QString("m_MeasStatData%1_SrcB").arg(i);
            stream.read( name, srcB);

            CArgument arg;
            arg.setVal( type, 0);
            arg.setVal( srcA, 1);
            arg.setVal( srcB, 2);
            m_CurrMeasStatItem.append( arg);
        }

        if( m_MeasStatData_Count == 0) //! Clear Meas Data
        {
           m_MeasDataMutex.lock();
            measStatData *pSData = NULL;

            int size = m_MeasStatData.count();
            for( int i=0; i<size; i++)
            {
                pSData = list_at(m_MeasStatData, i);
                delete pSData;
                pSData = NULL;
            }

            m_MeasStatData.clear();
            m_MeasDataMutex.unlock();

            m_CurrMeasStatItem.clear();
        }
        m_MeasAllData = NULL;
    }
    else
    {
        rst();
    }
    return ERR_NONE;
}

void servMeasure::rst()
{
    m_SrcA        = chan1;
    m_SrcB        = chan2;
    m_Category    = 0;  //Horizontal
    m_MeasAllSrc  = 0;  //OFF

    m_Range_Mode  = 0;  //Range_Mode: Screen
    m_Set_Type    = 1;  //Set_Type: Threshold

    m_ThresholdSrc   = chan1;
    m_TH_Type  = 0;  //Percent%
    m_HighPer        = 90; //90%
    m_MidPer         = 50; //50%
    m_LowPer         = 10; //10%
    m_HighAbs        = 1500;
    m_MidAbs         = 1200;
    m_LowAbs         =  800;

    m_Region      = 0;  //All
    m_CursorA     = 300;  //! 3 Div
    m_CursorB     = 700;  //! 7 Div
    m_CursorAB    = 0;

    m_Method      = false;
    m_Top_Method  = 0;
    m_Base_Method = 0;

    m_StatEnable  = false;    //Stat.OFF
    m_TrendEnable  = false;    //Trend OFF
    m_StatCount   = 1000;

    m_Indicator = false;

    m_bAppShow    = false;
    m_SubMenuMsg  = 0;
    m_ZoomOn      = false;

    m_CmdMeasValue = 0.0;

    m_CmdMeasType  = Meas_Period;
    m_CmdStatType  = STAT_CURR;
    m_CmdSrcA      = (Chan)m_SrcA;
    m_CmdSrcB      = (Chan)m_SrcB;
    m_CmdSelItem   = (int)ALL_ITEM;

    m_MeasValueReady = false;

    m_CmdEposType  = EPOS_SA;

    m_MeasDataMutex.lock();

    measStatData *pSData = NULL;

    int size = m_MeasStatData.count();
    for( int i=0; i<size; i++)
    {
        pSData = list_at(m_MeasStatData, i);
        delete pSData;
        pSData = NULL;
    }

    m_MeasStatData.clear();

    m_MeasDataMutex.unlock();

    m_MeasAllData = NULL;

    resetThresholdList();

    m_HistoMeasEn = false;
    m_HistoSelectItem = 0;

    m_HistoMeasMutex.lock();
    m_HistoMeasBuffer.clear();
    m_HistoMeasMutex.unlock();

    m_HistoMeasOutBuffer.clear();

    m_CurrMeasStatItem.clear();

    //mUiAttr.setEnable(MSG_APP_MEAS_INDICATOR, true);
}

int servMeasure::startup()
{
#ifdef FLAMINGO_TR5
     mUiAttr.setVisible(MSG_APP_MEAS_EYE_MENU, false);
#endif
     set_RangeMode( m_Range_Mode );

     if( m_Method == false)
     {
         mUiAttr.setVisible( MSG_APP_MEAS_SET_TOP_METHOD, false);
         mUiAttr.setVisible( MSG_APP_MEAS_SET_BASE_METHOD, false);
     }
     else
     {
         mUiAttr.setVisible( MSG_APP_MEAS_SET_TOP_METHOD, true);
         mUiAttr.setVisible( MSG_APP_MEAS_SET_BASE_METHOD, true);
     }

     if(sysCheckLicense(OPT_PWR))
     {
         mUiAttr.setVisible( MSG_APP_MEAS_UPA_MENU, true);
     }
     else
     {
         mUiAttr.setVisible( MSG_APP_MEAS_UPA_MENU, false);
     }

     on_OnOffCh1();
     on_OnOffCh2();
     on_OnOffCh3();
     on_OnOffCh4();
     on_LaOnOff();

     on_OnOffMath1();
     on_OnOffMath2();
     on_OnOffMath3();
     on_OnOffMath4();

     if( m_Category == 2) //! Double Src Meas, set srcB
     {
         mUiAttr.setEnable(MSG_APP_MEAS_SRCB, true);
     }
     else
     {
         mUiAttr.setEnable(MSG_APP_MEAS_SRCB, false);
     }


     if( m_CurrMeasStatItem.size() > 0)
     {
         for( int i=0; i<m_CurrMeasStatItem.size(); i++)
         {
             CArgument arg = m_CurrMeasStatItem.at(i);
             m_CmdMeasType = (MeasType)(arg[0].iVal);
             m_CmdSrcA = (Chan)(arg[1].iVal);
             cmd_SetSrcB((Chan)(arg[2].iVal));
             cmd_AddMeas();
         }

         //! Measure App add ValueItem
         async(servMeasure::MSG_CMD_ADD_VALUE_ITEM,0);

         m_CurrMeasStatItem.clear();

         //added by hxh. for bug
         async(MSG_APP_MEAS_STAT_ENABLE, m_StatEnable);
         async(MSG_APP_MEAS_INDICATOR,   m_Indicator);
     }

     return 0;
}

void servMeasure::init()
{
    measInit();
}

DsoErr servMeasure:: start()
{
    m_pMeasureThread = new servMeasureThread(this);
    Q_ASSERT( NULL != m_pMeasureThread );

    m_pMeasureThread->start();
    return ERR_NONE;
}

void servMeasure::initThresholdList()
{
    for(int i = 0; i< array_count(_ThresholdSrcList); i++ )
    {
        m_ThresholdList[_ThresholdSrcList[i]] = new ThresholdData(_ThresholdSrcList[i],
                                                                  meas_algorithm::TH_TYPE_PER,
                                                                  90,50,10, 1.0); //! base: 1.0 %
    }
}

void servMeasure::resetThresholdList()
{
    foreach (Chan src, m_ThresholdList.keys())
    {
        m_ThresholdList.value(src)->setType(meas_algorithm::TH_TYPE_PER, 90,50,10,1.0);
    }
}

DsoErr servMeasure::onActive(int msg)
{
//    qDebug()<< "servMeasure::onActive  msg = "<<msg;
    if( msg == 1)     //! servMeasure active, App Show
    {
        async( MSG_APP_MEASURE, true );
    }
    else              //! Enter SubMenu
    {
        servMeasure::setActive(msg);
    }

    return ERR_NONE;
}

DsoErr servMeasure::onEnterSubMenu(int msg)
{
    switch (msg)
    {
    case MSG_APP_MEAS_ADD_MENU:
    case MSG_APP_MEAS_CAT:  //Menu skip from Remove Menu
        m_SubMenuMsg = MSG_APP_MEAS_ADD_MENU;
        async( MSG_APP_MEAS_CAT, m_Category);
        break;

    case MSG_APP_MEAS_REMOVE_MENU:
    case MSG_APP_MEAS_CLEAR_ALL:  //Menu skip from Add Menu
        m_SubMenuMsg = MSG_APP_MEAS_REMOVE_MENU;
        break;

    case MSG_APP_MEAS_SET_MENU:
        m_SubMenuMsg = MSG_APP_MEAS_SET_MENU;
        break;

    case MSG_APP_MEAS_STAT_MENU:
        m_SubMenuMsg = MSG_APP_MEAS_STAT_MENU;
        break;
    case MSG_APP_MEAS_ANALYZE_MENU:
        m_SubMenuMsg = MSG_APP_MEAS_ANALYZE_MENU;
        break;

    default:
        m_SubMenuMsg = 0;
        break;
    }

    return ERR_NONE;
}

DsoErr servMeasure::onTraceTimeout()
{
    if( m_nRemoteList > 0 &&  //have remote command
        !isEngineStop() &&    // not stop or waiting
        m_MeasSrcSema.available() == 0 ) // no trace
    {
        setMeasValReady();
        servMeasure::m_bTraceTimeout = true;
    }
    return ERR_NONE;
}

DsoErr servMeasure::onReturnMainMenu(int /*msg*/)
{
    return ERR_NONE;
}

int servMeasure::getSubMenu()
{
    return m_SubMenuMsg;
}

void   servMeasure::clickClear()
{
    LOG_DBG() << "click Clear Menu";

//     top_CmdItem[0].pDst = this;

}

DsoErr servMeasure::toAddMenu()
{
//    setActive(MSG_APP_MEAS_SRCA);

    return ERR_NONE;
}

DsoErr servMeasure::set_SrcA(int v)
{
    DsoErr err = ERR_NONE;

    m_SrcA = v;
    return err;
}

int servMeasure::get_SrcA()
{
    setuiEnable( m_ChMask != 0 );

    if(get_bit( m_ChMask, m_SrcA))
    {
        return m_SrcA;
    }
    else
    {
        for( int i=chan1; i <= chan4; i++)
        {
            if(get_bit( m_ChMask, i))
            {
                m_SrcA = i;
                return m_SrcA;
            }
        }
        for( int i=m1; i <= m4; i++)
        {
            if(get_bit( m_ChMask, i))
            {
                m_SrcA = i;
                return m_SrcA;
            }
        }
        for( int i=d0; i <= d15; i++)
        {
            if(get_bit( m_ChMask, i))
            {
                m_SrcA = i;
                return m_SrcA;
            }
        }

        return m_SrcA;
    }
}

DsoErr servMeasure::set_SrcB(int v)
{
    DsoErr err = ERR_NONE;
    m_SrcB = v;
    return err;
}

int servMeasure::get_SrcB()
{
    if( m_Category != 2)
    {
        setuiEnable( false);
    }
    else
    {
        setuiEnable( m_ChMask != 0 );
    }

    if(get_bit( m_ChMask, m_SrcB))
    {
        return m_SrcB;
    }
    else
    {
        for( int i=chan1; i <= chan4; i++)
        {
            if(get_bit( m_ChMask, i))
            {
                m_SrcB = i;
                return m_SrcB;
            }
        }
        for( int i=m1; i <= m4; i++)
        {
            if(get_bit( m_ChMask, i))
            {
                m_SrcB = i;
                return m_SrcB;
            }
        }
        for( int i=d0; i <= d15; i++)
        {
            if(get_bit( m_ChMask, i))
            {
                m_SrcB = i;
                return m_SrcB;
            }
        }

        return m_SrcB;
    }
}

DsoErr servMeasure::set_Category(int v)
{
    m_Category = v;

    LOG_DBG()<<"m_Category = "<<m_Category;
    if(m_Category == 0 || m_Category == 1)
    {
        mUiAttr.setEnable(MSG_APP_MEAS_SRCB, false);
        LOG_DBG()<<"Set SrcB false";
    }
    else if(m_Category == 2)
    {
        mUiAttr.setEnable(MSG_APP_MEAS_SRCB, true);
        for( int i=chan1; i<=chan4; i++)
        {
            mUiAttr.setEnable( MSG_APP_MEAS_SRCB, i, get_bit( m_ChMask, i));
        }
        for( int i=m1; i<=m4; i++)
        {
            mUiAttr.setEnable( MSG_APP_MEAS_SRCB, i, get_bit( m_ChMask, i));
        }
        for( int i=d0; i<=d15; i++)
        {
            mUiAttr.setEnable( MSG_APP_MEAS_SRCB, i, get_bit( m_ChMask, i));
        }
        defer( MSG_APP_MEAS_SRCB);

        LOG_DBG()<<"Set SrcB true";
    }
    else
    {
        mUiAttr.setEnable(MSG_APP_MEAS_SRCB, false);
    }

    return ERR_NONE;
}

int servMeasure::get_Category()
{


    return m_Category;
}

DsoErr servMeasure::set_MeasAllSrc(int v)
{
    m_MeasAllSrc = v;

    return ERR_NONE;
}

int servMeasure::get_MeasAllSrc()
{
    setuiEnable( m_ChMask != 0 );
    return m_MeasAllSrc;
}

DsoErr servMeasure::set_To_RemoveMenu()
{
    serviceExecutor::post( serv_name_measure, CMD_SERVICE_ACTIVE, MSG_APP_MEAS_CLEAR_ALL);
    LOG_DBG()<<"-----------------------------------------"<<__FUNCTION__;
    return ERR_NONE;
}

DsoErr servMeasure::set_To_AddMenu()
{
    serviceExecutor::post( serv_name_measure, CMD_SERVICE_ACTIVE, MSG_APP_MEAS_CAT);
    return ERR_NONE;
}

DsoErr servMeasure::set_Delete(bool b)
{
    m_MeasValueReady = false;
    b =b;

    return ERR_NONE;
}


DsoErr servMeasure::set_DeleteAll(bool b)
{
    m_MeasValueReady = false;
    DsoErr err = ERR_NONE;
    b =b;
    err = delAllStatItem();

    return err;
}

DsoErr servMeasure::set_SetType(int v)
{
    m_Set_Type = v;
    return ERR_NONE;
}

int servMeasure::get_SetType()
{
    return m_Set_Type;
}

DsoErr servMeasure::set_ThresholdSrc(int v)
{
    m_ThresholdSrc = v;

    meas_algorithm::ThresholdType thType = m_ThresholdList.value((Chan)m_ThresholdSrc)->mtype;
    m_TH_Type = (int)thType;

    setuiChange( MSG_APP_MEAS_TH_HIGH_TYPE );
    setuiChange( MSG_APP_MEAS_TH_MID_TYPE );
    setuiChange( MSG_APP_MEAS_TH_LOW_TYPE );

    setuiChange( MSG_APP_MEAS_TH_HIGH_TYPE_SUB_1 );
    setuiChange( MSG_APP_MEAS_TH_MID_TYPE_SUB_1 );
    setuiChange( MSG_APP_MEAS_TH_LOW_TYPE_SUB_1 );

    return ERR_NONE;
}

int servMeasure::get_ThresholdSrc()
{
    return m_ThresholdSrc;
}

DsoErr servMeasure::set_ThresholdType(int v)
{
    m_TH_Type = v;

    if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_PER)
    {
        m_ThresholdList.value((Chan)m_ThresholdSrc)->setType((meas_algorithm::ThresholdType)m_TH_Type,
                                                             m_HighPer, m_MidPer, m_LowPer, 1.0); //! base: 1.0 %
    }
    else if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_ABS)
    {
        m_ThresholdList.value((Chan)m_ThresholdSrc)->setType((meas_algorithm::ThresholdType)m_TH_Type,
                                                             m_HighAbs, m_MidAbs, m_LowAbs, 1e6); //! base: 1e6 uV
    }
    else
    {
        m_ThresholdList.value((Chan)m_ThresholdSrc)->setType((meas_algorithm::ThresholdType)m_TH_Type,
                                                             m_HighPer, m_MidPer, m_LowPer, 1.0); //! base: 1.0 %
    }

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    return ERR_NONE;
}

DsoErr servMeasure::set_Th_High_Type(int v)
{
    m_TH_Type = v;

    setuiChange( MSG_APP_MEAS_TH_MID_TYPE );
    setuiChange( MSG_APP_MEAS_TH_LOW_TYPE );

    set_ThresholdType(v);
    return ERR_NONE;
}

int    servMeasure::get_Th_High_Type()
{
    return m_TH_Type;
}

DsoErr servMeasure::set_Th_High(qint64 v)
{
    if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_PER)
    {
        set_HighPer(v);
    }
    else if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_ABS)
    {
        set_HighAbs(v);
    }
    else
    {
        set_HighPer(v);
    }

    checkEngineStatus();
    return ERR_NONE;
}

qint64 servMeasure::get_Th_high()
{
    if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_PER)
    {
        return get_HighPer();
    }
    else if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_ABS)
    {
        return get_HighAbs();
    }
    else
    {
        return get_HighPer();
    }
}

DsoErr servMeasure::set_Th_Mid_Type(int v)
{
    m_TH_Type = v;

    setuiChange( MSG_APP_MEAS_TH_HIGH_TYPE );
    setuiChange( MSG_APP_MEAS_TH_LOW_TYPE );

    set_ThresholdType(v);
    return ERR_NONE;
}

int    servMeasure::get_Th_Mid_Type()
{
    return m_TH_Type;
}

DsoErr servMeasure::set_Th_Mid(qint64 v)
{
//    qDebug()<<"servMeasure::set_Th_Mid: "<<v;
    if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_PER)
    {
        set_MidPer(v);
    }
    else if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_ABS)
    {
        set_MidAbs(v);
    }
    else
    {
        set_MidPer(v);
    }

    checkEngineStatus();
    return ERR_NONE;
}

qint64 servMeasure::get_Th_Mid()
{
    if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_PER)
    {
        return get_MidPer();
    }
    else if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_ABS)
    {
        return get_MidAbs();
    }
    else
    {
        return get_MidPer();
    }
}

DsoErr servMeasure::set_Th_Low_Type(int v)
{
    m_TH_Type = v;

    setuiChange( MSG_APP_MEAS_TH_MID_TYPE );
    setuiChange( MSG_APP_MEAS_TH_HIGH_TYPE );

    set_ThresholdType(v);
    return ERR_NONE;
}

int    servMeasure::get_Th_Low_Type()
{
    return m_TH_Type;
}

DsoErr servMeasure::set_Th_Low(qint64 v)
{
    if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_PER)
    {
        set_LowPer(v);
    }
    else if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_ABS)
    {
        set_LowAbs(v);
    }
    else
    {
        set_LowPer(v);
    }

    checkEngineStatus();
    return ERR_NONE;
}

qint64 servMeasure::get_Th_Low()
{
    if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_PER)
    {
        return get_LowPer();
    }
    else if( (meas_algorithm::ThresholdType)m_TH_Type == meas_algorithm::TH_TYPE_ABS)
    {
        return get_LowAbs();
    }
    else
    {
        return get_LowPer();
    }
}

DsoErr servMeasure::set_HighPer(qint64 v)
{
    m_HighPer = v;

    m_ThresholdList.value((Chan)m_ThresholdSrc)->setThHigh(m_HighPer, 1.0); //! base: 1.0 %

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    setuiChange( MSG_APP_MEAS_TH_MID_TYPE_SUB_1 );
    return ERR_NONE;
}

qint64 servMeasure::get_HighPer()
{
    m_HighPer = m_ThresholdList.value((Chan)m_ThresholdSrc)->mIntHigh_Menu;

    qint64 minVal =  m_MidPer+1;
    setuiRange( minVal, 100ll, 90ll);  //! m_MidPer - 100%
    setuiStep( 1 );
    setuiPostStr( "%" );

    setuiUnit(Unit_percent);
    setuiBase(1, E_0);

    return m_HighPer;
}

DsoErr servMeasure::set_MidPer(qint64 v)
{
    m_MidPer = v;

    m_ThresholdList.value((Chan)m_ThresholdSrc)->setThMid(m_MidPer, 1.0); //! base: 1.0 %

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    setuiChange( MSG_APP_MEAS_TH_HIGH_TYPE_SUB_1 );
    setuiChange( MSG_APP_MEAS_TH_LOW_TYPE_SUB_1 );
    return ERR_NONE;
}

qint64 servMeasure::get_MidPer()
{
    m_MidPer = m_ThresholdList.value((Chan)m_ThresholdSrc)->mIntMid_Menu;

    qint64 minVal = m_LowPer+1;
    qint64 maxVal = m_HighPer-1;

    setuiRange( minVal, maxVal, 50ll);  //! 5% - 95%
    setuiStep( 1 );
    setuiPostStr( "%" );
    //setuiFmt(fmt_int);
    setuiUnit(Unit_percent);
    setuiBase(1, E_0);

    return m_MidPer;
}

DsoErr servMeasure::set_LowPer(qint64 v)
{
    m_LowPer = v;

    m_ThresholdList.value((Chan)m_ThresholdSrc)->setThLow(m_LowPer, 1.0); //! base: 1.0 %

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    setuiChange( MSG_APP_MEAS_TH_MID_TYPE_SUB_1 );
    return ERR_NONE;
}

qint64 servMeasure::get_LowPer()
{
    m_LowPer = m_ThresholdList.value((Chan)m_ThresholdSrc)->mIntLow_Menu;

    qint64 maxVal =  m_MidPer-1;
    setuiRange( 0ll, maxVal, 10ll);  //! 0% - m_MidPer
    setuiStep( 1 );

    setuiUnit(Unit_percent);

    if( m_LowPer < 10 )
    {
        setuiFmt(dso_fmt_trim(fmt_int, fmt_width_1, fmt_no_trim_0) );
    }
    else
    {
        setuiFmt(dso_fmt_trim(fmt_int, fmt_width_2, fmt_no_trim_0) );
    }

    setuiBase(1, E_0);
    setuiPostStr("%");

    return m_LowPer;
}

DsoErr servMeasure::set_HighAbs(qint64 v)
{    
    m_HighAbs = v;
    m_ThresholdList.value((Chan)m_ThresholdSrc)->setThHigh(m_HighAbs, 1e6); //! base: 1e6 uV

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    return ERR_NONE;
}

qint64 servMeasure::get_HighAbs()
{
    m_HighAbs = m_ThresholdList.value((Chan)m_ThresholdSrc)->mIntHigh_Menu;
    dsoVert *pVert = dsoVert::getCH( (Chan)m_ThresholdSrc );
    float base;
    pVert->getProbe().toReal(base);

    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;

    setuiStep( step );

    qlonglong minVal = m_MidAbs + step;
    qlonglong maxVal = 100000000ll*1000000ll;   //! Max: 100M

//    setuiMinVal(minVal);   //! Min: -100M
//    setuiMaxVal(maxVal);   //! Max: 100M
    setuiRange( minVal, maxVal, 0ll);

    Unit unit = pVert->getUnit();
    setuiUnit( unit);
    if( unit == Unit_W)       setuiPostStr( "W" );
    else if( unit == Unit_A)  setuiPostStr( "A" );
    else if( unit == Unit_V)  setuiPostStr( "V" );
    else if( unit == Unit_U)  setuiPostStr( "U" );
    else                      setuiPostStr( "V" );

    setuiBase(1, E_N6);
    return m_HighAbs;
}

DsoErr servMeasure::set_MidAbs(qint64 v)
{
    m_MidAbs = v;

    m_ThresholdList.value((Chan)m_ThresholdSrc)->setThMid(m_MidAbs, 1e6); //! base: 1e6 uV

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    return ERR_NONE;
}

qint64 servMeasure::get_MidAbs()
{
    m_MidAbs = m_ThresholdList.value((Chan)m_ThresholdSrc)->mIntMid_Menu;
    dsoVert *pVert = dsoVert::getCH( (Chan)m_ThresholdSrc );
    float base;
    pVert->getProbe().toReal(base);

    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;
    setuiStep( step );

    qlonglong minVal = m_LowAbs + step;
    qlonglong maxVal = m_HighAbs - step;

//    setuiMinVal(minVal);   //! Min: -100M
//    setuiMaxVal(maxVal);   //! Max: 100M
    setuiRange( minVal, maxVal, 0ll);

    Unit unit = pVert->getUnit();
    setuiUnit( unit);
    if( unit == Unit_W)       setuiPostStr( "W" );
    else if( unit == Unit_A)  setuiPostStr( "A" );
    else if( unit == Unit_V)  setuiPostStr( "V" );
    else if( unit == Unit_U)  setuiPostStr( "U" );
    else                      setuiPostStr( "V" );

    setuiBase(1, E_N6);

    return m_MidAbs;
}

DsoErr servMeasure::set_LowAbs(qint64 v)
{
    m_LowAbs = v;

    m_ThresholdList.value((Chan)m_ThresholdSrc)->setThLow(m_LowAbs, 1e6); //! base: 1e6 uV

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    return ERR_NONE;
}

qint64 servMeasure::get_LowAbs()
{
    m_LowAbs = m_ThresholdList.value((Chan)m_ThresholdSrc)->mIntLow_Menu;

    dsoVert *pVert = dsoVert::getCH( (Chan)m_ThresholdSrc );
    float base;
    pVert->getProbe().toReal(base);

    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;
    setuiStep( step );

    qlonglong minVal = -100000000ll*1000000ll;  //! Min: -100M
    qlonglong maxVal = m_MidAbs - step;

//    setuiMinVal(minVal);   //! Min: -100M
//    setuiMaxVal(maxVal);   //! Max: 100M
    setuiRange( minVal, maxVal, 0ll);

    Unit unit = pVert->getUnit();
    setuiUnit( unit);
    if( unit == Unit_W)       setuiPostStr( "W" );
    else if( unit == Unit_A)  setuiPostStr( "A" );
    else if( unit == Unit_V)  setuiPostStr( "V" );
    else if( unit == Unit_U)  setuiPostStr( "U" );
    else                      setuiPostStr( "V" );

    setuiBase(1, E_N6);

    setuiFmt(dso_fmt_trim(fmt_float, fmt_width_3, fmt_no_trim_0) );

    return m_LowAbs;
}

DsoErr servMeasure::set_Th_Default()
{
    m_ThresholdList.value((Chan)m_ThresholdSrc)->setDefault();

    setuiChange( MSG_APP_MEAS_TH_HIGH_TYPE_SUB_1 );
    setuiChange( MSG_APP_MEAS_TH_MID_TYPE_SUB_1 );
    setuiChange( MSG_APP_MEAS_TH_LOW_TYPE_SUB_1 );

    //add by hxh. 2018-05-17
    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    checkEngineStatus();
    return ERR_NONE;
}

DsoErr servMeasure::set_Method(bool b)
{
    m_Method = b;

    if( m_Method == false)
    {
        mUiAttr.setVisible( MSG_APP_MEAS_SET_TOP_METHOD, false);
        mUiAttr.setVisible( MSG_APP_MEAS_SET_BASE_METHOD, false);
    }
    else
    {
        mUiAttr.setVisible( MSG_APP_MEAS_SET_TOP_METHOD, true);
        mUiAttr.setVisible( MSG_APP_MEAS_SET_BASE_METHOD, true);
    }

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    checkEngineStatus();
    return ERR_NONE;
}

bool servMeasure::get_Method()
{
    return m_Method;
}

DsoErr servMeasure::set_Top_Method(int v)
{
    m_Top_Method = v;

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    checkEngineStatus();
    return ERR_NONE;
}

int servMeasure::get_Top_Method()
{
    return m_Top_Method;
}

DsoErr servMeasure::set_Base_Method(int v)
{
    m_Base_Method = v;

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_MeasCfg();
    }

    checkEngineStatus();
    return ERR_NONE;
}

int servMeasure::get_Base_Method()
{
    return m_Base_Method;
}

DsoErr servMeasure::set_RangeMode(int v)
{
    m_Range_Mode = v;

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {

        setFPGA_Region();
        LOG_DBG() << "setFPGA_Region()";
        setFPGA_MeasCfg();
        LOG_DBG() << "setFPGA_MeasCfg()";
    }
    else
    {
        postEngine( ENGINE_MEAS_EN, (quint32)FPGA_CH_LA_DIS); //! Disable CH LA
    }

    return ERR_NONE;
}

int servMeasure::get_RangeMode()
{
    return m_Range_Mode;
}

DsoErr servMeasure::set_Region(int v)
{
    m_Region = v;

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_Region();
    }

    checkEngineStatus();
    return ERR_NONE;
}

int servMeasure::get_Th_HighPos()
{
    int high_Pos = m_ThresholdList.value((Chan)m_ThresholdSrc)->mHigh_SrcPos;
    return high_Pos;
}

int servMeasure::get_Th_MidPos()
{
    int mid_Pos  = m_ThresholdList.value((Chan)m_ThresholdSrc)->mMid_SrcPos;
    return mid_Pos;
}

int servMeasure::get_Th_LowPos()
{
    int low_Pos  = m_ThresholdList.value((Chan)m_ThresholdSrc)->mLow_SrcPos;
    return low_Pos;
}

int servMeasure::get_Region()
{
    return m_Region;
}

DsoErr servMeasure::set_CursorA(int v)
{
    DsoErr err = ERR_NONE;
    err = limit_rangeCursor( v, 0, m_CursorB-1);
    m_CursorA = v;

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_Region();
    }

    checkEngineStatus();
    LOG_DBG() << "m_CursorA"<<m_CursorA;
    return err;
}

DsoErr servMeasure::limit_rangeCursor( int& v, int min, int max)
{
    DsoErr err;
    if( v <= min)
    {
        v = min;
        err = ERR_OVER_LOW_RANGE;
    }
    else if( v >= max)
    {
        v = max;
        err = ERR_OVER_UPPER_RANGE;
    }
    else
    {
        err = ERR_NONE;
    }

    return err;
}


int servMeasure::get_CursorA()
{
    QString str;
    horiAttr attr;
    float val;

    if( m_ZoomOn == true)
    {
        attr = getHoriAttr( chan1, horizontal_view_zoom );
    }
    else
    {
        attr = getHoriAttr( chan1, horizontal_view_main );
    }

    val = (m_CursorA - attr.gnd) * attr.inc;

    gui_fmt::CGuiFormatter::format( str, val );
    str = str + "s";

    mUiAttr.setOutStr( MSG_APP_MEAS_RANGE_CURSOR_AX, str );

    LOG_DBG() << "m_CursorA"<<m_CursorA;
    return m_CursorA;
}

DsoErr servMeasure::set_CursorB(int v)
{
    DsoErr err;
    err = limit_rangeCursor( v, m_CursorA+1, wave_width);
    m_CursorB = v;

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_Region();
    }

    checkEngineStatus();
    return err;
}

int servMeasure::get_CursorB()
{
    QString str;
    horiAttr attr;
    float val;

    if( m_ZoomOn == true)
    {
        attr = getHoriAttr( chan1, horizontal_view_zoom );
    }
    else
    {
        attr = getHoriAttr( chan1, horizontal_view_main );
    }

    val = (m_CursorB - attr.gnd) * attr.inc;

    gui_fmt::CGuiFormatter::format( str, val );
    str = str + "s";

    mUiAttr.setOutStr( MSG_APP_MEAS_RANGE_CURSOR_BX, str );

    return m_CursorB;
}

DsoErr servMeasure::set_CursorAB(int v)
{
    DsoErr err = ERR_NONE;
    //err = limit_rangeCursorAB( m_CursorA, m_CursorB, v, wave_width);
    m_CursorAB = v;

    m_CursorA += v;
    m_CursorB += v;


    if( m_CursorA <= 0 )
    {
        m_CursorA = 0;
        err = ERR_OVER_LOW_RANGE;
    }

    if( m_CursorB <= 1 )
    {
        m_CursorB = 1;
        err = ERR_OVER_LOW_RANGE;
    }

    if( m_CursorA > 998 )
    {
        m_CursorA = 998;
        err = ERR_OVER_UPPER_RANGE;
    }

    if( m_CursorB > 999 )
    {
        m_CursorB = 999;
        err = ERR_OVER_UPPER_RANGE;
    }

    //async( MSG_APP_MEAS_RANGE_CURSOR_AX, m_CursorA);
    //async( MSG_APP_MEAS_RANGE_CURSOR_BX, m_CursorB);
    setuiChange(MSG_APP_MEAS_RANGE_CURSOR_AX);
    setuiChange(MSG_APP_MEAS_RANGE_CURSOR_BX);

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_Region();
    }

    checkEngineStatus();
    return err;
}

DsoErr servMeasure::limit_rangeCursorAB( int& cur1, int& cur2, int& v, int highLimit)
{
    int max = qMax(cur2, cur1);
    int min = qMin(cur2, cur1);
    if(min + v < 0)
    {
        cur2 = cur2;
        cur1 = cur1;
        v = 0;
        return ERR_NONE;
    }
    if(max + v > highLimit)
    {
        cur2 = cur2;
        cur1 = cur1;
        v = 0;
        return ERR_NONE;
    }

    cur2 = cur2 + v;
    cur1 = cur1 + v;

    return ERR_NONE;
}

int servMeasure::get_CursorAB()
{
    return m_CursorAB;
}

DsoErr servMeasure::set_Indicator(bool b)
{    
    m_Indicator = b;

    int mode = Acquire_YT;
    serviceExecutor::query( serv_name_hori,
                            MSG_HOR_TIME_MODE, mode);
    if( mode == Acquire_XY && b)
    {
        return INF_INDICATOR_DISABLED;
    }
    return ERR_NONE;
}

bool servMeasure::get_Indicator()
{
    return m_Indicator;
}

DsoErr servMeasure::set_StatReset()
{
    if( isEngineStop() )
    {
        on_DisplayClear();
    }
    else
    {
        m_MeasDataMutex.lock();

        int size = m_MeasStatData.size();
        for(int i=0; i<size; ++i)
        {
            m_MeasStatData.at(i)->resetStatVal();
        }

        if(size > 0 || m_MeasAllSrc != chan_none )
        {
            if( m_MeasAllData != NULL )
            {
                m_MeasAllData->clearData();
            }
            //added by hxh
            serviceExecutor::post(serv_name_measure,
                                  servMeasure::MSG_UPDATE_MEAS,
                                  0);
        }

        m_MeasDataMutex.unlock();
    }
    return ERR_NONE;
}

DsoErr servMeasure::set_StatCount(int v)
{
    m_StatCount = v;

    m_MeasDataMutex.lock();
    if(m_MeasStatData.size() != 0)
    {        
        for(int i=0; i<m_MeasStatData.size(); ++i)
        {
            m_MeasStatData.at(i)->setCntLimit(m_StatCount);
        }
        serviceExecutor::post(serv_name_measure,
                              servMeasure::MSG_UPDATE_MEAS, 0);

    }
    m_MeasDataMutex.unlock();

    return ERR_NONE;
}

int servMeasure::get_StatCount()
{
    setuiRange( 2ll, 5000ll, 1000ll);
    setuiStep( 1 );
    setuiFmt(fmt_int);
    setuiPostStr( "" );
    setuiBase(1, E_0);

    return m_StatCount;
}

DsoErr servMeasure::set_StatEnable(bool b)
{
    DsoErr err = ERR_NONE;

    if( m_StatEnable != b)
    {
        m_StatEnable = b;

        if(m_StatEnable == true)  //Stat On
        {
            err = set_StatReset();
        }
    }

    //    CArgument arg, argOut;
    //    arg.setVal(Meas_Vpp,0);
    //    arg.setVal(chan1,1);
    //    arg.setVal(chan_none);

    //    DsoErr err;

    //    err = serviceExecutor::query(serv_name_measure, servMeasure::MSG_CMD_GET_MEAS, arg, argOut);
    //    LOG_DBG()<<"MSG_CMD_GET_MEAS"<< servMeasure::MSG_CMD_GET_MEAS;
    //    LOG_DBG()<<"query err: "<< err;
    return err;
}

bool servMeasure::get_StatEnable()
{
    return m_StatEnable;
}

DsoErr servMeasure::set_TrendEnable(bool b)
{
    m_TrendEnable = b;
    return ERR_NONE;
}

bool servMeasure::get_TrendEnable()
{
    return m_TrendEnable;
}

DsoErr servMeasure::set_Counter()
{
    serviceExecutor::post( serv_name_counter, CMD_SERVICE_ACTIVE, (int)1 );

    return ERR_NONE;
}

DsoErr servMeasure::set_DVM()
{
    serviceExecutor::post( serv_name_dvm, CMD_SERVICE_ACTIVE, (int)1 );
    return ERR_NONE;
}

DsoErr servMeasure::set_UPA()
{
    serviceExecutor::post( E_SERVICE_ID_UPA, CMD_SERVICE_ACTIVE, (int)1 );

    return ERR_NONE;
}
DsoErr servMeasure::set_UPA_Visable(bool b)
{
    mUiAttr.setVisible( MSG_APP_MEAS_UPA_MENU, b);

    return ERR_NONE;
}
DsoErr servMeasure::set_HISTO()
{
    serviceExecutor::post( serv_name_histo,CMD_SERVICE_ACTIVE,(int) 1);
    return ERR_NONE;
}

DsoErr servMeasure::set_Zone_Trig()
{
    serviceExecutor::post( serv_name_trigger_zone,CMD_SERVICE_ACTIVE,(int)1);
    return ERR_NONE;
}

DsoErr servMeasure::set_Eye_Jit()
{
    serviceExecutor::post( serv_name_eyejit,CMD_SERVICE_ACTIVE,(int) 1);
    return ERR_NONE;
}

DsoErr servMeasure::set_AppShow(bool b)
{
    Q_UNUSED(b);
    //ADD by hxh. Close the window if it is visible
    if( this->isActive())
    {        
        if(  (m_SubMenuMsg == MSG_APP_MEAS_ADD_MENU)
           ||(m_SubMenuMsg == MSG_APP_MEAS_REMOVE_MENU)
           ||(m_SubMenuMsg == MSG_APP_MEAS_SET_MENU)
           ||(m_SubMenuMsg == MSG_APP_MEAS_STAT_MENU)
           ||(m_SubMenuMsg == MSG_APP_MEAS_ANALYZE_MENU) )  //! On SubMenu
        {
            m_bAppShow = true;
            servMeasure::setActive(1);
            m_SubMenuMsg = MSG_APP_MEASURE;
            async(CMD_SERVICE_SUB_RETURN, 1);
        }
        else       //! On TopMenu
        {
            m_bAppShow = false;     //! Close All Meas Window
            servMeasure::setdeActive();
        }
    }
    else
    {
        m_bAppShow = true;      //! Display All Meas Window
        servMeasure::setActive(1);
    }

    return ERR_NONE;
}

bool servMeasure::get_AppShow()
{
//    LOG_DBG()<<__FUNCTION__<<__LINE__;
    return m_bAppShow;
}

DsoErr servMeasure::addStatItem(CArgument &arg)
{
    MeasType type = (MeasType)arg[0].iVal;
    Chan srcA     = (Chan)arg[1].iVal;
    Chan srcB     = (Chan)arg[2].iVal;

    checkEngineStatus();
//    qDebug()<<"servMeasure::addStatItem type"<<type;
//    qDebug()<<"servMeasure::addStatItem srcA"<<srcA;
//    qDebug()<<"servMeasure::addStatItem srcB"<<srcB;
    m_MeasDataMutex.lock();
    if((type >= Meas_DOUBLE_SRC_TYPE)&&(type < Meas_DOUBLE_SRC_TYPE_END))
    {
//        addMeasDSrcSafe( type, srcA, srcB);
        addStatItemSafe( type, srcA, srcB);
    }
    else
    {
//        addMeasSrcSafe( srcA);
        addStatItemSafe(type, srcA, chan_none);
    }
    m_MeasDataMutex.unlock();

    LOG_DBG() << "finish addStatItem";

    return ERR_NONE;
}

DsoErr servMeasure::delStatItem(int row)
{
    m_MeasDataMutex.lock();
    if(row < m_MeasStatData.size())
    {        
        measStatData *pData = list_at(m_MeasStatData, row);
        delete pData;
        pData = NULL;
        m_MeasStatData.removeAt(row);
//        removeUnusedSrc();        
    }
    m_MeasDataMutex.unlock();
    //added by hxh
//    if(m_MeasStatData.size() == 0)
//    {

//        mUiAttr.setEnable(MSG_APP_MEASURE_ACTION, false);
//    }
    return ERR_NONE;
}


DsoErr servMeasure::delAllStatItem(void)
{
    measStatData *pData = NULL;

    m_MeasDataMutex.lock();

    int size = m_MeasStatData.count();
    for(int i=0; i<size; i++)
    {
        pData = list_at(m_MeasStatData, i);
        delete pData;
        pData = NULL;
    }
    m_MeasStatData.clear();

    m_MeasDataMutex.unlock();

    m_HistoMeasMutex.lock();
    m_HistoMeasBuffer.clear();
    m_HistoMeasMutex.unlock();

    m_HistoMeasOutBuffer.clear();

    return ERR_NONE;
}


DsoErr servMeasure::addStatItemSafe(MeasType type, Chan srcA, Chan srcB)
{
    measStatData* pStatData = new measStatData(type, srcA, srcB);
    Q_ASSERT( NULL != pStatData );

    pStatData->setCntLimit(m_StatCount);
    m_MeasStatData.append(pStatData);

    return ERR_NONE;
}

DsoErr servMeasure::addMeasSrc(Chan src)
{
    src = src;
#if 0
    m_MeasDataMutex.lock();
    addMeasSrcSafe(src);
    removeUnusedSrc();
    m_MeasDataMutex.unlock();
#endif

    return ERR_NONE;
}

DsoErr servMeasure::addMeasSrcSafe(Chan src)
{
    bool measExist = false;

    int size = m_MeasData.size();
    for(int i=0; i<size; ++i)
    {
        if(list_at( m_MeasData,i)->getMeasSrc() == src)
        {
            measExist = true;
            break;
        }
        else
        {}
    }

    if(!measExist)
    {
        measData *pData = new measData(src);
        m_MeasData.append(pData);
    }

    return ERR_NONE;
}

DsoErr servMeasure::addMeasDSrcSafe(MeasType type, Chan srcA, Chan srcB)
{
    bool measExist = false;
    MeasType temptype = type;

    if( (type == Meas_DelayFF)||(type == Meas_DelayRR)
      ||(type == Meas_DelayFR)||(type == Meas_DelayRF) )
    {
        temptype = type;
    }
    else
    {
        switch (type)
        {
        case Meas_PhaseFF:
            temptype = Meas_DelayFF;
            break;
        case Meas_PhaseRR:
            temptype = Meas_DelayRR;
            break;
        case Meas_PhaseRF:
            temptype = Meas_DelayRF;
            break;
        case Meas_PhaseFR:
            temptype = Meas_DelayFR;
            break;
        default:
            temptype = type;
            break;
        }
    }

    for(int i=0; i<m_MeasDataDSrc.size(); ++i)
    {
        if(  (list_at( m_MeasDataDSrc,i)->getMeasSrcA() == srcA)
           &&(list_at( m_MeasDataDSrc,i)->getMeasSrcB() == srcB)
           &&(list_at( m_MeasDataDSrc,i)->getMeasType() == temptype)  )
        {
            measExist = true;
            break;
        }
        else
        {}
    }

    if(!measExist)
    {
        addMeasSrcSafe( srcA);
        addMeasSrcSafe( srcB);

        MeasFPGA_DlyIndex index = getFPGA_Dly_index();
        measDataDSrc *pData = new measDataDSrc( srcA, srcB, temptype, index);
        m_MeasDataDSrc.append(pData);
    }

    return ERR_NONE;
}

//DsoErr servMeasure::removeUnusedSrc(void)
//{
//    Chan src = chan_none;
//    Chan srcA = chan_none;
//    Chan srcB = chan_none;
//    MeasType type = MeasType(0);
//    measData* pMeasData = 0;
//    measDataDSrc* pMeasDataDSrc = 0;
//    int i = 0;

//    foreach(pMeasData, m_MeasData)
//    {
//        Q_ASSERT(pMeasData != 0);
//        src = pMeasData->getMeasSrc();
//        if(  isCurrAllSrc(src)
//           ||isStatDataSrc(src))
//        {}
//        else
//        {
//            LOG_DBG() << "remove: src" << src;
//            m_MeasData.removeAll(pMeasData);
//            delete pMeasData;
//            pMeasData = NULL;
//        }
//    }
//    LOG_DBG() << "removeUnusedSrc::m_MeasData.size = " << m_MeasData.size();

//    foreach(pMeasDataDSrc, m_MeasDataDSrc)
//    {
//        Q_ASSERT(pMeasDataDSrc != 0);
//        srcA = pMeasDataDSrc->getMeasSrcA();
//        srcB = pMeasDataDSrc->getMeasSrcB();
//        type = pMeasDataDSrc->getMeasType();
//        LOG_DBG() << "i"<<i<<"srcA"<<srcA << " srcB" << srcB << " type" << type <<"isDelaySrc: " << isDelaySrc(srcA,srcB);
//        LOG_DBG() << "i"<<i<<"srcA"<<srcA << " srcB" << srcB << " type" << type <<"isPhaseSrc: " << isPhaseSrc(srcA,srcB);
//       LOG_DBG() << "isStatDataSrc: " << isStatDataSrc(srcA,srcB,type);

//        if( isStatDataSrc(srcA,srcB,type) )
//        {}
//        else
//        {
//            m_MeasDataDSrc.removeAll(pMeasDataDSrc);
//            delete pMeasDataDSrc;
//            pMeasDataDSrc = NULL;
//        LOG_DBG() << "remove: " << i;
//        }
//        i++;
//    }

//    LOG_DBG() << "removeUnusedSrc::m_MeasDataDSrc.size: " << m_MeasDataDSrc.size();

//    return ERR_NONE;
//}

bool servMeasure::isCurrAllSrc(Chan src)
{
    return src == m_MeasAllSrc;
}

bool servMeasure::isStatDataSrc(Chan src)
{
    bool b = false;
    m_MeasDataMutex.lock();
    if(m_MeasStatData.size() != 0)
    {
        for(int i = 0; i < m_MeasStatData.size(); ++i)
        {
           if(  (src == list_at( m_MeasStatData,i)->getSrcA())
              ||(src == list_at( m_MeasStatData,i)->getSrcB()) )
            {
                b = true;
                break;
            }
        }
    }
    m_MeasDataMutex.unlock();

    return b;
}

bool servMeasure::isStatDataSrc(Chan srcA, Chan srcB, MeasType type)
{
    bool isStat = false;

    MeasType statDataType = MeasType(0);

    m_MeasDataMutex.lock();
    if(m_MeasStatData.size() != 0)
    {
        for(int i = 0; i < m_MeasStatData.size(); ++i)
        {
            if(  (srcA == list_at( m_MeasStatData,i)->getSrcA())
               &&(srcB == list_at( m_MeasStatData,i)->getSrcB()) )
            {
                statDataType = list_at( m_MeasStatData,i)->getType();
                if(  (statDataType == Meas_DelayFF)
                   ||(statDataType == Meas_PhaseFF) )
                {
                    isStat = (type == Meas_DelayFF)?true:false;
                }
                else if(  (statDataType == Meas_DelayRR)
                        ||(statDataType == Meas_PhaseRR) )
                {
                    isStat = (type == Meas_DelayRR)?true:false;
                }
                else if(  (statDataType == Meas_DelayRF)
                        ||(statDataType == Meas_PhaseRF) )
                {
                    isStat = (type == Meas_DelayRF)?true:false;
                }
                else if(  (statDataType == Meas_DelayFR)
                        ||(statDataType == Meas_PhaseFR) )
                {
                    isStat = (type == Meas_DelayFR)?true:false;
                }
                else
                {
                    isStat = false;
                }

                if(isStat)
                {
                    break;
                }
            }
        }
    }
    m_MeasDataMutex.unlock();

    return isStat;
}

void* servMeasure::get_AllMeasData(void)
{
    return m_MeasAllData;
}

void*  servMeasure::get_MeasStatData(void)
{   
    return &m_MeasStatData;
}

measData* servMeasure::getMeasDataCore(Chan srcA)
{
    for(int i=0; i<m_MeasData.size(); ++i)
    {
        if(list_at( m_MeasData,i)->getMeasSrc() == srcA)
        {
            return list_at( m_MeasData,i);
        }
    }
    return 0;
}

measDataDSrc* servMeasure::getMeasDataCore(MeasType type, Chan srcA, Chan srcB)
{
    for(int i=0; i<m_MeasDataDSrc.size(); ++i)
    {
        if(  (list_at( m_MeasDataDSrc,i)->getMeasSrcA() == srcA)
           &&(list_at( m_MeasDataDSrc,i)->getMeasSrcB() == srcB))
        {
            if((type == Meas_DelayFF)||(type == Meas_PhaseFF))
            {
                if(list_at( m_MeasDataDSrc,i)->getMeasType() == Meas_DelayFF)
                {
                    return list_at( m_MeasDataDSrc,i);
                }
                else
                {}
            }
            else if((type == Meas_DelayRR)||(type == Meas_PhaseRR))
            {
                if(list_at( m_MeasDataDSrc,i)->getMeasType() == Meas_DelayRR)
                {
                    return list_at( m_MeasDataDSrc,i);
                }
                else
                {}
            }
            else if((type == Meas_DelayFR)||(type == Meas_PhaseFR))
            {
                if(list_at( m_MeasDataDSrc,i)->getMeasType() == Meas_DelayFR)
                {
                    return list_at( m_MeasDataDSrc,i);
                }
                else
                {}
            }
            else if((type == Meas_DelayRF)||(type == Meas_PhaseRF))
            {
                if(list_at( m_MeasDataDSrc,i)->getMeasType() == Meas_DelayRF)
                {
                    return list_at( m_MeasDataDSrc,i);
                }
                else
                {}
            }
        }
    }
    return 0;
}

bool servMeasure::haveMeasData(void)
{
    m_MeasDataMutex.lock();
    int size = m_MeasStatData.size();
    m_MeasDataMutex.unlock();

    if( (size > 0) ||
        ((Chan)m_MeasAllSrc != chan_none) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool servMeasure::hasMathSrc()
{
    int size =  m_MeasStatData.size();
    for(int i=0; i<size; i++)
    {
        measStatData *pItem = list_at(m_MeasStatData, i);
        if( pItem->getSrcA() >= m1 && pItem->getSrcA() <= m4 )
        {
            return true;
        }

        if( pItem->getSrcB() >= m1 && pItem->getSrcB() <= m4 )
        {
            return true;
        }
    }
    return false;
}

///
/// \brief 硬件测量时，STOP，clear后，判断trace是否有效
/// \return
///
bool servMeasure::traceValid()
{

    Chan srcA ;
    dsoVert *pVert = NULL;

    for( int ch=chan1; ch<=d15; ch=ch+1 )
    {
        if( get_bit( m_ChMask, ch) )
        {
            srcA = (Chan)ch;
            if( ch >= d0 && ch <= d15 )
            {
                srcA = la;
            }
            pVert = dsoVert::getCH( srcA );
            DsoWfm wfm;
            wfm.init(0);
            pVert->getTrace(wfm, chan_none, horizontal_view_main);
            return (wfm.size() > 0);
        }
    }

    return false;
}

void  servMeasure::setMeasValReady()
{
    m_MeasValueReady = true;

    //if( CMeasFormater::mCmdGetValid )
    if( servMeasure::m_nRemoteList > 0 )
    {
        sysSetScpiEvent(scpi_event_output_queue,1);

        //qDebug() << "2.." << QTime::currentTime() << "......measure ready:" ;
//        CArgument arg;

//        //! argument: 0, 1
//        arg.append( (int)scpi_event_output_queue );
//        arg.append( 1 );
//        serviceExecutor::send( serv_name_scpi,
//                               dsoServScpi::cmd_scpi_event,
//                               arg );

    }
    else
    {
    }
}

DsoErr servMeasure::set_UpdateMeas(bool b)
{
    b = b;

    return ERR_NONE;
}

bool servMeasure::cmd_GetMeasValueReady()
{
    return m_MeasValueReady;
}

void *servMeasure::get_UpdateMeas(void)
{
    return &m_MeasStatData;
}


void servMeasure::measInit()
{
    m_HighPer = 90; //ThHigh default value: 90%
    m_MidPer = 50; //ThMid default value:  50%
    m_LowPer = 10; //ThMin default value:  10%

    //! SrcA Hide Ref1-10
    for( int i = (int)r1; i<=(int)r10; i++)  //! Hide Ref1-10
    {
        mUiAttr.setVisible( MSG_APP_MEAS_SRCA, i, false);
    }   

    //! SrcB Hide Ref1-10
    for( int i = (int)r1; i<=(int)r10; i++)  //! Hide Ref1-10
    {
        mUiAttr.setVisible( MSG_APP_MEAS_SRCB, i, false);
    }

     //! La check
     if( sysHasLA())
     {
         for( int i = (int)d0; i<=(int)d15; i++)
         {
             mUiAttr.setVisible( MSG_APP_MEAS_SRCA, i, true);
             mUiAttr.setVisible( MSG_APP_MEAS_SRCB, i, true);
         }
     }
     else
     {
         for( int i = (int)d0; i<=(int)d15; i++)
         {
             mUiAttr.setVisible( MSG_APP_MEAS_SRCA, i, false);
             mUiAttr.setVisible( MSG_APP_MEAS_SRCB, i, false);
         }
     }


    //! All Src Hide Math1-4, Ref1-10
    for( int i = (int)m1; i<=(int)m4; i++)  //! Hide Math1-4
    {
        mUiAttr.setVisible( MSG_APP_MEAS_ALL_SRC, i, false);
    }
    for( int i = (int)r1; i<=(int)r10; i++)  //! Hide Ref1-10
    {
        mUiAttr.setVisible( MSG_APP_MEAS_ALL_SRC, i, false);
    }

    //! Threshold Src Hide Ref1-10
    for( int i = (int)r1; i<=(int)r10; i++)  //! Hide Ref1-10
    {
        mUiAttr.setVisible( MSG_APP_MEAS_TH_SRC, i, false);
    }


#ifndef _DEBUG
    mUiAttr.setVisible( MSG_APP_MEAS_TEST, false);
    mUiAttr.setVisible( MSG_APP_MEAS_TREND_ENABLE, false);
#else
    mUiAttr.setVisible( MSG_APP_MEAS_TEST, true);
    mUiAttr.setVisible( MSG_APP_MEAS_TREND_ENABLE, true);
#endif

    on_HorZoomOn();
}

DsoErr servMeasure::setMeasSrcReady(void *)
{
    if ( m_MeasSrcSema.available() < 1 )
    {
        m_MeasSrcSema.release();
    }

    return ERR_NONE;
}

void servMeasure::checkEngineStatus()
{
    if(  isEngineStop() )
    {
        if ( m_MeasSrcSema.available() < 1 )
        {
            m_MeasSrcSema.release();
        }
    }
}

bool servMeasure::isEngineStop()
{
    bool isStop = false;
    int  iStat;
    if ( ERR_NONE == queryEngine( qENGINE_CONTROL_STATUS, iStat ) )
    {
        if(  ((ControlStatus)iStat == Control_Stoped)
           ||((ControlStatus)iStat == Control_Fpga_Force_Stopped)
           ||((ControlStatus)iStat == Control_waiting) )
        {
            isStop = true;
        }
    }

    return isStop;
}

void servMeasure::registerSpy()
{
    spyOn( E_SERVICE_ID_TRACE,
           servTrace::cmd_set_provider,
           mpItem->servId,
           servMeasure::MSG_TRACE_UPDATED );

    setMsgAttr( servMeasure::MSG_TRACE_UPDATED );

    spyOn( serv_name_hori, servHori::cmd_main_scale, servMeasure::cmd_hor_setting_change );
    spyOn( serv_name_hori, servHori::cmd_main_offset, servMeasure::cmd_hor_setting_change );
    spyOn( serv_name_hori, servHori::cmd_zoom_scale, servMeasure::cmd_hor_setting_change );
    spyOn( serv_name_hori, servHori::cmd_zoom_offset, servMeasure::cmd_hor_setting_change );

    spyOn( serv_name_ch1, MSG_CHAN_SCALE_VALUE, servMeasure::cmd_ch1_setting_change );
    spyOn( serv_name_ch1, MSG_CHAN_SCALE,       servMeasure::cmd_ch1_setting_change );
    spyOn( serv_name_ch1, MSG_CHAN_OFFSET,      servMeasure::cmd_ch1_setting_change );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE_VALUE, servMeasure::cmd_ch2_setting_change );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE,       servMeasure::cmd_ch2_setting_change );
    spyOn( serv_name_ch2, MSG_CHAN_OFFSET,      servMeasure::cmd_ch2_setting_change );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE_VALUE, servMeasure::cmd_ch3_setting_change );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE,       servMeasure::cmd_ch3_setting_change );
    spyOn( serv_name_ch3, MSG_CHAN_OFFSET,      servMeasure::cmd_ch3_setting_change );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE_VALUE, servMeasure::cmd_ch4_setting_change );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE,       servMeasure::cmd_ch4_setting_change );
    spyOn( serv_name_ch4, MSG_CHAN_OFFSET,      servMeasure::cmd_ch4_setting_change );

    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON, servMeasure::cmd_hor_zoom_on );
    spyOn( serv_name_vert_mgr, servVertMgr::cmd_active_chan, servMeasure::cmd_on_ch_active );

    spyOn( serv_name_ch1, MSG_CHAN_ON_OFF, servMeasure::cmd_on_ch1_on_off);
    spyOn( serv_name_ch2, MSG_CHAN_ON_OFF, servMeasure::cmd_on_ch2_on_off);
    spyOn( serv_name_ch3, MSG_CHAN_ON_OFF, servMeasure::cmd_on_ch3_on_off);
    spyOn( serv_name_ch4, MSG_CHAN_ON_OFF, servMeasure::cmd_on_ch4_on_off);

    //for bug1695 by hxh
    spyOn( serv_name_ch1, servVert::user_cmd_on_off, servMeasure::cmd_on_ch1_on_off);
    spyOn( serv_name_ch2, servVert::user_cmd_on_off, servMeasure::cmd_on_ch2_on_off);
    spyOn( serv_name_ch3, servVert::user_cmd_on_off, servMeasure::cmd_on_ch3_on_off);
    spyOn( serv_name_ch4, servVert::user_cmd_on_off, servMeasure::cmd_on_ch4_on_off);

    spyOn( serv_name_math1, MSG_MATH_EN,  servMeasure::cmd_on_math1_on_off);
    spyOn( serv_name_math2, MSG_MATH_EN,  servMeasure::cmd_on_math2_on_off);
    spyOn( serv_name_math3, MSG_MATH_EN,  servMeasure::cmd_on_math3_on_off);
    spyOn( serv_name_math4, MSG_MATH_EN,  servMeasure::cmd_on_math4_on_off);


    spyOn( serv_name_math1, MSG_MATH_S32MATHOPERATOR,  servMeasure::cmd_on_math1_on_off);
    spyOn( serv_name_math2, MSG_MATH_S32MATHOPERATOR,  servMeasure::cmd_on_math2_on_off);
    spyOn( serv_name_math3, MSG_MATH_S32MATHOPERATOR,  servMeasure::cmd_on_math3_on_off);
    spyOn( serv_name_math4, MSG_MATH_S32MATHOPERATOR,  servMeasure::cmd_on_math4_on_off);

    spyOn( serv_name_la, MSG_LA_ENABLE, servMeasure::cmd_on_la_on_off);
    spyOn( serv_name_la, MSG_LA_D0D7_ONOFF, servMeasure::cmd_on_la_on_off);
    spyOn( serv_name_la, MSG_LA_D8D15_ONOFF, servMeasure::cmd_on_la_on_off);
    spyOn( serv_name_la, MSG_LA_SELECT_CHAN, servMeasure::cmd_on_la_on_off);
    spyOn( serv_name_la, MSG_LA_SELECT_GROUP, servMeasure::cmd_on_la_on_off);

    //! Histogram
    spyOn( serv_name_histo, MSG_HISTO_EN,   servMeasure::cmd_on_histo_meas_en);
    spyOn( serv_name_histo, MSG_HISTO_TYPE, servMeasure::cmd_on_histo_meas_en);

    //! Clear
    spyOn(serv_name_display, MSG_DISPLAY_CLEAR );
    spyOn(serv_name_trigger, MSG_TRIGGER_SWEEP );

    //spyOn(serv_name_hori,    MSG_HOR_TIME_MODE);

    // ROLL from stop to run. BY HXH
    spyOn(serv_name_hori, MSG_HORIZONTAL_RUN);


    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Active);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Exp    );
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Invalid);
}

bool servMeasure::haveTraceSrc()
{
    return true;
}

void servMeasure::on_horizontal_changed()
{
    set_StatReset();

    if( (MeasMode)m_Range_Mode == FPGA_MODE)
    {
        setFPGA_Region();
        LOG_DBG() << "-------------setFPGA_Region()";
    }

    if( (this->isActive()) && ((RegionType)m_Region == REGION_CURSOR))
    {
        async( MSG_APP_MEAS_RANGE_CURSOR_AX, m_CursorA);
        async( MSG_APP_MEAS_RANGE_CURSOR_BX, m_CursorB);
    }
}

void servMeasure::on_ChChanged( Chan ch)
{
    bool isReset = false;

    m_MeasDataMutex.lock();
    int size = m_MeasStatData.size();
    for( int i=0; i<size; i++)
    {
        measStatData *pMeasStat = m_MeasStatData.at(i);
        if( (pMeasStat->getSrcA() == ch)||
            (pMeasStat->getSrcB() == ch) )
        {
            pMeasStat->resetStatVal();
            isReset = true;
        }
    }

    if( isReset  || m_MeasAllSrc != chan_none )
    {
        if( m_MeasAllData != NULL )
        {
            m_MeasAllData->clearData();
        }
        serviceExecutor::post(serv_name_measure,
                              servMeasure::MSG_UPDATE_MEAS, 0);        
    }
    m_MeasDataMutex.unlock();
}

void servMeasure::on_Ch1Changed()
{
    on_ChChanged( chan1);
}

void servMeasure::on_Ch2Changed()
{
    on_ChChanged( chan2);
}

void servMeasure::on_Ch3Changed()
{
    on_ChChanged( chan3);
}

void servMeasure::on_Ch4Changed()
{
    on_ChChanged( chan4);
}

void servMeasure::on_HorZoomOn()
{
    set_StatReset();

    bool zoomOn;
    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON, zoomOn);
    LOG_DBG() << "query mode: "<<zoomOn;

    m_ZoomOn = zoomOn;

    if( zoomOn)
    {
        mUiAttr.setEnable( MSG_APP_MEAS_REGION, (int)REGION_ZOOM, true);
    }
    else
    {
        mUiAttr.setEnable( MSG_APP_MEAS_REGION, (int)REGION_ZOOM, false);
        if( (RegionType)m_Region == REGION_ZOOM)
        {
            async( MSG_APP_MEAS_REGION, (int)REGION_MAIN);
        }
    }

    if( (this->isActive()) && ((RegionType)m_Region == REGION_CURSOR))
    {
       LOG_DBG() <<"set Range Cursor";
        async( MSG_APP_MEAS_RANGE_CURSOR_AX, m_CursorA);
        async( MSG_APP_MEAS_RANGE_CURSOR_BX, m_CursorB);
    }
}

void servMeasure::on_ChActive()
{
    int actChan;
    serviceExecutor::query( serv_name_vert_mgr,
                            servVertMgr::cmd_active_chan,
                            actChan );


    if(  ((Chan)actChan >= chan1)&&((Chan)actChan <= chan4) )
    {
        if( get_bit( m_ChMask, actChan))
        {
            async( MSG_APP_MEAS_SRCA, actChan, serv_name_measure );

            if( m_Category == 2)
            {
                async( MSG_APP_MEAS_SRCB, actChan, serv_name_measure );
            }
        }
    }
}

void servMeasure::on_OnOffCh1()
{
    bool chEn = false;
    serviceExecutor::query( serv_name_ch1, MSG_CHAN_ON_OFF, chEn);

    if( chEn)
    {
        m_ChMask |= ((qint64)1 << (int)chan1);
    }
    else
    {
        m_ChMask &= ~((qint64)1 << (int)chan1);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_SRCA, chan1, chEn);

    if( m_Category == 2)
    {
        mUiAttr.setEnable( MSG_APP_MEAS_SRCB, chan1, chEn);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_ALL_SRC, chan1, chEn);

    on_ChActive();
}

void servMeasure::on_OnOffCh2()
{
    bool chEn = false;
    serviceExecutor::query( serv_name_ch2, MSG_CHAN_ON_OFF, chEn);

    if( chEn)
    {
        m_ChMask |= ((qint64)1 << (int)chan2);
    }
    else
    {
        m_ChMask &= ~((qint64)1 << (int)chan2);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_SRCA, chan2, chEn);

    if( m_Category == 2)
    {
        mUiAttr.setEnable( MSG_APP_MEAS_SRCB, chan2, chEn);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_ALL_SRC, chan2, chEn);

    on_ChActive();
}

void servMeasure::on_OnOffCh3()
{
    bool chEn = false;
    serviceExecutor::query( serv_name_ch3, MSG_CHAN_ON_OFF, chEn);

    if( chEn)
    {
        m_ChMask |= ((qint64)1 << (int)chan3);
    }
    else
    {
        m_ChMask &= ~((qint64)1 << (int)chan3);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_SRCA, chan3, chEn);

    if( m_Category == 2)
    {
        mUiAttr.setEnable( MSG_APP_MEAS_SRCB, chan3, chEn);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_ALL_SRC, chan3, chEn);

    on_ChActive();
}

void servMeasure::on_OnOffCh4()
{
    bool chEn = false;
    serviceExecutor::query( serv_name_ch4, MSG_CHAN_ON_OFF, chEn);

    if( chEn)
    {
        m_ChMask |= ((qint64)1 << (int)chan4);
    }
    else
    {
        m_ChMask &= ~((qint64)1 << (int)chan4);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_SRCA, chan4, chEn);

    if( m_Category == 2)
    {
        mUiAttr.setEnable( MSG_APP_MEAS_SRCB, chan4, chEn);
    }

    mUiAttr.setEnable( MSG_APP_MEAS_ALL_SRC, chan4, chEn);

    on_ChActive();
}

void servMeasure::on_OnOffMath(QString servName, Chan mathSrc)
{
    bool en = false;
    int op = 0;
    serviceExecutor::query( servName, MSG_MATH_EN, en);
    serviceExecutor::query( servName, MSG_MATH_S32MATHOPERATOR, op);

    if( en&&((MathOperator)op != operator_fft))
    {
        m_ChMask |= ((qint64)1 << (int)mathSrc);
    }
    else
    {
        m_ChMask &= ~((qint64)1 << (int)mathSrc);
        en = false;
    }

    mUiAttr.setEnable( MSG_APP_MEAS_SRCA, mathSrc, en);

    if( m_Category == 2)
    {
        mUiAttr.setEnable( MSG_APP_MEAS_SRCB, mathSrc, en);
    }

    if( en&&((MathOperator)op != operator_fft))
    {
        async( MSG_APP_MEAS_SRCA, mathSrc, serv_name_measure );
    }
}

void servMeasure::on_OnOffMath1()
{
    on_OnOffMath( serv_name_math1, m1);
}

void servMeasure::on_OnOffMath2()
{
    on_OnOffMath( serv_name_math2, m2);
}

void servMeasure::on_OnOffMath3()
{
    on_OnOffMath( serv_name_math3, m3);
}

void servMeasure::on_OnOffMath4()
{
    on_OnOffMath( serv_name_math4, m4);
}

void servMeasure::on_LaOnOff()
{
    bool laEn = false;
    serviceExecutor::query( serv_name_la, MSG_LA_ENABLE, laEn );

    if( laEn)
    {
        int dxOnOff;
        serviceExecutor::query( serv_name_la, MSG_LA_SELECT_CHAN, dxOnOff );
        LOG_DBG()<<"dxOnOff = "<<dxOnOff;
        for( int i=d0; i<=d15; i++)
        {
            int labit = get_bit(dxOnOff, i);
            if( labit)
            {
                m_ChMask |= (qint64)1 << i;
            }
            else
            {
                m_ChMask &= ~((qint64)1 << i);
            }
            mUiAttr.setEnable( MSG_APP_MEAS_SRCA, i, labit);

            if( m_Category == 2)
            {
                mUiAttr.setEnable( MSG_APP_MEAS_SRCB, i, labit);
            }
        }
    }
    else
    {
        for( int i=d0; i<=d15; i++)
        {
            m_ChMask &= ~((qint64)1 << i);
            mUiAttr.setEnable( MSG_APP_MEAS_SRCA, i, laEn);

            if( m_Category == 2)
            {
                mUiAttr.setEnable( MSG_APP_MEAS_SRCB, i, laEn);
            }
        }
    }
}

void servMeasure::on_HistoMeasEn()
{
    bool histoEn;
    serviceExecutor::query( serv_name_histo, MSG_HISTO_EN, histoEn );
    if( histoEn)
    {
        int histoType = 0;
        serviceExecutor::query( serv_name_histo, MSG_HISTO_TYPE, histoType );
        if( histoType == histoMeas)
        {
            m_HistoMeasEn = true;
        }
        else
        {
            m_HistoMeasEn = false;
        }
    }
    else
    {
        m_HistoMeasEn = false;
    }
    m_HistoMeasMutex.lock();
    m_HistoMeasBuffer.clear();
    m_HistoMeasMutex.unlock();
    m_HistoMeasOutBuffer.clear();
}

/**
 * 滚动或满扫描下，
 * 从STOP切换到RUN时，
 * 测量结果复位，类似clear
 *
 * 2018-4-12.by hxh
 */
void servMeasure::on_RunStop()
{
    int mode = Acquire_YT;

    serviceExecutor::query( serv_name_hori,
                            MSG_HOR_TIME_MODE, mode);

    int run = Control_Stop;
    serviceExecutor::query(serv_name_hori,
                           MSG_HORIZONTAL_RUN, run);

//    if( run == Control_Run )
//    {
//        set_StatReset();
//    }

    if( mode == Acquire_ROLL || mode == Acquire_SCAN )
    {
        if( run == Control_Run )
        {            
            on_DisplayClear();
        }
        else if( run == Control_Stop )
        {
            if( hasMathSrc() )
            {
                /*
                 * math通道在wait，stop，single->stop状态时，
                 * 如果来一次trace，此时需要多访问几次math才能拿到math数据
                 * */
                 m_MeasSrcSema.release(2);//need once more
            }
        }
    }
}

void servMeasure::on_TrigSweep()
{
    int trigSweep;
    serviceExecutor::query( serv_name_trigger,
                            MSG_TRIGGER_SWEEP,
                            trigSweep );

    if ( trigSweep == Trigger_Sweep_Single )
    {
        on_DisplayClear();
        if( hasMathSrc() )
        {
            /*
             * math通道在wait，stop，single->stop状态时，
             * 如果来一次trace，此时需要多访问几次math才能拿到math数据
             * */
             m_MeasSrcSema.release(2);//need once more
        }
    }
}

void servMeasure::on_Timemode()
{
//    int mode;
//    query(serv_name_hori,MSG_HOR_TIME_MODE,mode);

//    if( mode == Acquire_XY )
//    {
//        mUiAttr.setEnable(MSG_APP_MEAS_INDICATOR, false);
//    }
//    else
//    {
//        mUiAttr.setEnable(MSG_APP_MEAS_INDICATOR, true);
//    }
}

void servMeasure::on_License()
{
    if(sysCheckLicense(OPT_PWR))
    {
        mUiAttr.setVisible( MSG_APP_MEAS_UPA_MENU, true);
    }
    else
    {
        mUiAttr.setVisible( MSG_APP_MEAS_UPA_MENU, false);
    }
}

//! add by lidongming bug 1369
void servMeasure::on_DisplayClear()
{

    int size = 0;
    MeasValue val;
    val.mVal = 0.0;
    val.mStatus = NO_SIGNAL;  //! Clear m_MeasStatData

    m_MeasDataMutex.lock();

    size = m_MeasStatData.size();
    for(int i=0; i<size; ++i)
    {
        measStatData *pItem = list_at(  m_MeasStatData,i);
        pItem->updateStatVal(val, val.mStatus);
    }

    if(size > 0 || m_MeasAllSrc != chan_none )
    {
        if( m_MeasAllData != NULL )
        {
            m_MeasAllData->clearData();
        }
        serviceExecutor::post(serv_name_measure,
                              servMeasure::MSG_UPDATE_MEAS,
                              0);
    }

    m_MeasDataMutex.unlock();
}

void servMeasure::postHistoMeas()
{
    if( m_HistoMeasEn)
    {
        m_MeasDataMutex.lock();
        if( !m_MeasStatData.isEmpty())
        {
            CArgument arg;
            for( int i=0; i<m_MeasStatData.size(); i++)
            {
                arg.setVal( m_MeasStatData.at(i)->getCurr(), i);
                LOG_DBG()<<" arg["<<i<<"] = "<<m_MeasStatData.at(i)->getCurr();
            }
            serviceExecutor::post( serv_name_histo,
                                   servHisto::cmd_meas_update,
                                   arg);
        }
        m_MeasDataMutex.unlock();
    }
}

void servMeasure::updateHistoMeas()
{
    if( m_HistoMeasEn)
    {
        m_MeasDataMutex.lock();
        bool valid = false;
        MeasValue currMeasVal;
        if( !m_MeasStatData.isEmpty())
        {
            if( (m_HistoSelectItem >= 0)&&(m_HistoSelectItem < m_MeasStatData.size()) )
            {
                valid = true;
                currMeasVal = m_MeasStatData.at(m_HistoSelectItem)->getCurrMeas();
                LOG_DBG() << "currMeasVal.mUnit = " << currMeasVal.mUnit;
            }
        }
        m_MeasDataMutex.unlock();

        if(valid)
        {
            m_HistoMeasMutex.lock();
            m_HistoMeasBuffer.enqueue( currMeasVal);
            LOG_DBG()<<" m_HistoMeasBuffer: "<<m_HistoMeasBuffer;
            m_HistoMeasMutex.unlock();
        }
        clearHistoMeasBuffer();
    }
}

void servMeasure::clearHistoMeasBuffer()
{
    LOG_DBG()<<" m_HistoMeasBuffer.size: "<<m_HistoMeasBuffer.size();
    LOG_DBG()<<" m_HistoMeasBuffer.size*MEAS_UPDATE_TIM: "<<m_HistoMeasBuffer.size()*MEAS_NORM_TIM;
    LOG_DBG()<<" HISTO_MEAS_TIMEOUT: "<<HISTO_MEAS_TIMEOUT;

    m_HistoMeasMutex.lock();
    if( !m_HistoMeasBuffer.isEmpty())
    {
        if( m_HistoMeasBuffer.size()*MEAS_NORM_TIM >= HISTO_MEAS_TIMEOUT)
        {
            LOG_DBG()<<" m_HistoMeasBuffer.size: "<<m_HistoMeasBuffer.size();
            LOG_DBG()<<" m_HistoMeasBuffer: "<<m_HistoMeasBuffer;
            m_HistoMeasBuffer.clear();
            m_HistoMeasOutBuffer.clear();
        }
    }
    m_HistoMeasMutex.unlock();
}

void servMeasure::setHistoSelectItem( int sel)
{
    m_HistoSelectItem = sel;

    m_HistoMeasMutex.lock();
    if( !m_HistoMeasBuffer.isEmpty())
    {
        m_HistoMeasBuffer.clear();
    }
    m_HistoMeasMutex.unlock();
}

pointer servMeasure::getHistoMeas()
{
    LOG_DBG()<<"---------------getHistoMeas() Start----------";
    m_HistoMeasMutex.lock();
    bool empty = m_HistoMeasBuffer.isEmpty();
    if( !empty)
    {
        m_HistoMeasOutBuffer.clear();
        m_HistoMeasOutBuffer = m_HistoMeasBuffer;
        m_HistoMeasBuffer.clear();
    }
    m_HistoMeasMutex.unlock();

    if( !empty)
    {
        return &m_HistoMeasOutBuffer;
    }
    else
    {
        return NULL;
    }
}

meas_algorithm::StateMethodMode servMeasure::getStateMethodMode()
{
    if( m_Method == false)
    {
        return meas_algorithm::MODE_AUTO;
    }
    else
    {
        return meas_algorithm::MODE_MANUAL;
    }
}

meas_algorithm::StateMethod servMeasure::getTopMethod()
{
    if( m_Top_Method == 0)
    {
        return meas_algorithm::METHOD_Histogram;
    }
    else
    {
        return meas_algorithm::METHOD_MaxMin;
    }
}

meas_algorithm::StateMethod servMeasure::getBaseMethod()
{
    if( m_Base_Method == 0)
    {
        return meas_algorithm::METHOD_Histogram;
    }
    else
    {
        return meas_algorithm::METHOD_MaxMin;
    }
}

void servMeasure::addMeasDSrc(MeasType type, Chan srcA, Chan srcB)
{
    MeasType temptype = type;

    if( (type == Meas_DelayFF)||(type == Meas_DelayRR)
      ||(type == Meas_DelayFR)||(type == Meas_DelayRF) )
    {
        temptype = type;
    }
    else
    {
        switch (type)
        {
        case Meas_PhaseFF:
            temptype = Meas_DelayFF;
            break;
        case Meas_PhaseRR:
            temptype = Meas_DelayRR;
            break;
        case Meas_PhaseRF:
            temptype = Meas_DelayRF;
            break;
        case Meas_PhaseFR:
            temptype = Meas_DelayFR;
            break;
        default:
            temptype = type;
            break;
        }
    }

    MeasFPGA_DlyIndex index = getFPGA_Dly_index();
    measDataDSrc *pData = new measDataDSrc( srcA, srcB, temptype, index);
    m_MeasDataDSrc.append(pData);
}

void servMeasure::clearBaseMeasData()
{
    //! Clear m_MeasData
    measData *pMData = NULL;
    int size = m_MeasData.count();
    for( int i=0; i<size; i++)
    {
        pMData = list_at(m_MeasData, i);
        delete pMData;
        pMData = NULL;
    }
    m_MeasData.clear();

    //! Clear m_MeasDataDSrc
    measDataDSrc *pDSrcData = NULL;

    size = m_MeasDataDSrc.count();
    for( int i=0; i<size; i++)
    {
        pDSrcData = list_at(m_MeasDataDSrc, i);
        delete pDSrcData;
        pDSrcData = NULL;
    }
    m_MeasDataDSrc.clear();
}

void servMeasure::buildBaseMeasData()
{
    //! Add m_MeasData & m_MeasDataDSrc
    m_MeasDataMutex.lock();
    measStatData *pSData = NULL;

    int size = m_MeasStatData.count();
    for( int i=0; i<size; i++)
    {
        pSData = list_at(m_MeasStatData, i);
        Chan srcA = pSData->getSrcA();
        Chan srcB = pSData->getSrcB();
        MeasType type = pSData->getType();
        if( srcB != chan_none)
        {
            addMeasDSrc( type, srcA, srcB);  //! Add m_MeasDataDSrc
            addMeasSrcSafe( srcA);
            addMeasSrcSafe( srcB);
        }
        else
        {
            addMeasSrcSafe( srcA);           //! Add m_MeasData
        }
    }
    m_MeasDataMutex.unlock();
}

//! calcMeasResult
//! return true:  MeasData is valid
//! return false: MeasData is invalid
bool servMeasure::calcMeasResult(void)
{
    def_time();
    MeasType type;
    Chan srcA,srcB;
    measData *pData = 0;
    measData *pDataA = 0;
    measData *pDataB = 0;
    measDataDSrc *pDataDsrc = 0;
    EngineAdcCoreCH adcCH;
    EngineHoriInfo horInfo;

    start_time();

    if( (MeasMode)m_Range_Mode == FPGA_MODE )
    {
        queryEngine( qENGINE_ADC_CORE_CH, &adcCH);
    }

    //! build base MeasData: m_MeasData & m_MeasDataDSrc
    buildBaseMeasData();

    //qDebug() << "measure...." << m_MeasStatData.size();

    bool isValid = true;
    int preMeas = m_MeasData.size();
    for(int i=0; i<m_MeasData.size(); ++i)
    {
        measData *pMeasCore = list_at( m_MeasData,i);
        srcA = pMeasCore->getMeasSrc();
        if( isFPGA_Src( srcA) )
        {
            pMeasCore->setRegion( (RegionType)m_Region,
                                               m_ZoomOn,
                                               get_CursorA(),
                                               get_CursorB());

            pMeasCore->CalcRealMeasData_FPGA(m_FPGAMeasRet,
                                              &adcCH,
                                              &horInfo);
        }
        else
        {
            pMeasCore->setRegion( (RegionType)m_Region,
                                               m_ZoomOn,
                                               get_CursorA(),
                                               get_CursorB());


            pMeasCore->CalcRealMeasData_SOFT(m_ThresholdList.value(srcA),
                                              getStateMethodMode(),
                                              getTopMethod(),
                                              getBaseMethod());
        }
    }

    //! All Meas Data for bug1025
    if( m_MeasAllSrc != chan_none)
    {
        if( m_MeasAllData == NULL )
        {
             m_MeasAllData = new measData();
        }

        if( m_MeasAllData != NULL)
        {
            m_MeasAllData->setMeasSrc( (Chan)m_MeasAllSrc );
            if( isFPGA_Src( (Chan)m_MeasAllSrc ) )
            {
                m_MeasAllData->setRegion( (RegionType)m_Region,
                                          m_ZoomOn,
                                          get_CursorA(),
                                          get_CursorB());

                m_MeasAllData->CalcRealMeasData_FPGA(m_FPGAMeasRet,
                                                     &adcCH,
                                                     &horInfo);
            }
            else
            {
                ThresholdData *th = m_ThresholdList.value((Chan)m_MeasAllSrc);
                m_MeasAllData->setRegion( (RegionType)m_Region,
                                          m_ZoomOn,
                                          get_CursorA(),
                                          get_CursorB());

                m_MeasAllData->CalcRealMeasData_SOFT(th,
                                                     getStateMethodMode(),
                                                     getTopMethod(),
                                                     getBaseMethod());
            }
        }
    }
    else
    {
        if( m_MeasAllData != NULL)
        {
            delete m_MeasAllData;
            m_MeasAllData = NULL;
        }
    }

    for(int i=0; i<m_MeasDataDSrc.size(); ++i)
    {
        measDataDSrc *pMeasCore = list_at( m_MeasDataDSrc,i);
        srcA = pMeasCore->getMeasSrcA();
        srcB = pMeasCore->getMeasSrcB();

        pDataA = getMeasDataCore(srcA);
        pDataB = getMeasDataCore(srcB);
        Q_ASSERT(pDataA != 0);
        Q_ASSERT(pDataB != 0);

        if((pDataA != 0)&&(pDataB != 0))
        {
            pMeasCore->CalcRealMeasDly_SOFT(pDataA, pDataB);
        }
    }

    m_MeasDataMutex.lock();

    int list = m_MeasStatData.size();

    //delete all. but not yet return to PC
    if( preMeas > list &&
         list == 0 &&
        servMeasure::m_nRemoteList > 0)
    {
        servMeasure::m_bTraceTimeout = true;
    }

    //qDebug() << "delete all measuring" << preMeas << list << QTime::currentTime();
    for(int i=0; i<list; ++i)
    {
        measStatData *pMeasCore = list_at( m_MeasStatData,i);
        type = pMeasCore->getType();
        srcA = pMeasCore->getSrcA();
        srcB = pMeasCore->getSrcB();

        if( (type >= Meas_DOUBLE_SRC_TYPE) &&
            (type <= Meas_DOUBLE_SRC_TYPE_END))
        {
            pDataDsrc = getMeasDataCore(type, srcA, srcB);
            pData = getMeasDataCore(srcA);
            if(pDataDsrc != 0)
            {
                pMeasCore->updateStatVal(pDataDsrc->getRealMeasData(type),
                                         pDataDsrc->getMeasStatus(type));

            }
        }
        else
        {
            pData = getMeasDataCore(srcA);
            if(pData != 0)
            {
               pMeasCore->updateStatVal(pData->getRealMeasData(type),
                                        pData->getMeasStatus(type));
            }
        }

        if( isEngineStop() && pMeasCore->getCnt() < 3 )
        {
            pMeasCore->setCount(1);
        }
    }
    m_MeasDataMutex.unlock();

    clearBaseMeasData();


    end_time();

    return isValid;
}

double servMeasure::getMeasItemCurValue()
{
    QTime time = QTime::currentTime();
    qsrand(time.msec()+time.second()+1);
    return (qrand()%100)/100.0;
}
