#include "meas_formater.h"
#include "servmeasure.h"
#include "../servhori/servhori.h"
#include "../servvertmgr/servvertmgr.h"

//For SCPI cmd

// add one measure item
DsoErr servMeasure::cmd_SetMeas(CArgument &arg)
{
    int inArgSize = arg.size();
    if( inArgSize >= 3 )  //:MEAS:ITEM <arg[0]>,<arg[1]>,<arg[2]>
    {
       m_CmdMeasType = (MeasType)arg[0].iVal;
       m_CmdSrcA     = (Chan)arg[1].iVal;
       cmd_SetSrcB((Chan)arg[2].iVal);
    }
    else if(inArgSize == 2)  //:MEAS:ITEM <arg[0]>,<arg[1]>
    {
        m_CmdMeasType = (MeasType)arg[0].iVal;
        m_CmdSrcA     = (Chan)arg[1].iVal;
        cmd_SetSrcB( (Chan) m_SrcB);
    }
    else if(inArgSize == 1)  //:MEAS:ITEM <arg[0]>
    {
        m_CmdMeasType = (MeasType)arg[0].iVal;
        m_CmdSrcA     = (Chan)m_SrcA;
        cmd_SetSrcB((Chan)m_SrcB);       
    }
    else
    {
        return ERR_NONE;
    }

    //open the source if it is off
    int ret = checkSource(m_CmdSrcA, m_CmdSrcB);
    if( ERR_NONE !=  ret)
    {
        return ret;
    }

    m_CmdStatType = STAT_CURR;
    //Measure App add ValueItem

    //! add for bug1757
    if( isEngineStop() &&
        isStatDataSrc(m_CmdSrcA) &&
        isStatDataSrc(m_CmdSrcB) )
        {
            //! Stop: do not change
        }
    else
    {
        m_MeasValueReady = false;        //! Engine Err: Clear
    }


    cmd_AddMeas();
    serviceExecutor::post(serv_name_measure,
                          servMeasure::MSG_CMD_ADD_VALUE_ITEM,
                          1);

    return ERR_NONE;
}

//add one meas stat item
DsoErr servMeasure::cmd_SetStatMeas(CArgument &arg)
{
    int inArgSize = arg.size();
    if(inArgSize >= 3)  //:MEAS<arg[0]>,<arg[1]>,<arg[2]>
    {
        m_CmdMeasType = (MeasType)arg[0].iVal;
        m_CmdSrcA     = (Chan)arg[1].iVal;
        cmd_SetSrcB((Chan)arg[2].iVal);
    }
    else if((inArgSize == 2))  //:MEAS<arg[0]>,<arg[1]>
    {
        m_CmdMeasType = (MeasType)arg[0].iVal;
        m_CmdSrcA     = (Chan)arg[1].iVal;
        cmd_SetSrcB((Chan)m_SrcB);
    }
    else if(inArgSize == 1)  //:MEAS<arg[0]>
    {
        m_CmdMeasType = (MeasType)arg[0].iVal;
        m_CmdSrcA     = (Chan)m_SrcA;
        cmd_SetSrcB((Chan)m_SrcB);
    }
    else
    {
        return ERR_NONE;
    }

    //
    int ret = checkSource(m_CmdSrcA, m_CmdSrcB);
    if( ERR_NONE !=  ret)
    {
        return ret;
    }

    //! add for bug2764
    if(!m_StatEnable)
    {
        async(MSG_APP_MEAS_STAT_ENABLE, true);
    }

    //! add for bug1757
    if( isEngineStop() &&
        isStatDataSrc(m_CmdSrcA) &&
        isStatDataSrc(m_CmdSrcB) )
        {
            //! Stop: do not change
        }
    else
    {
        m_MeasValueReady = false;        //! Engine Err: Clear
    }

    cmd_AddMeas();

    //Measure App add ValueItem
    serviceExecutor::post(serv_name_measure,
                          servMeasure::MSG_CMD_ADD_VALUE_ITEM, 1);

    return ERR_NONE;
}

//! add by lidongming 远程指令直接在服务中添加测量项，避免App添加时延
void servMeasure::cmd_AddMeas(void)
{
    bool itemExist = false;
    measStatData *pStatData = 0;

    m_MeasDataMutex.lock();
    for(int i=0; i<m_MeasStatData.count(); i++)
    {
        pStatData = list_at( m_MeasStatData,i);
        if(  (m_CmdMeasType == pStatData->getType())
           &&(m_CmdSrcA == pStatData->getSrcA())
           &&(m_CmdSrcB == pStatData->getSrcB()) )
        {
            itemExist = true;
            break;
        }
    }

    if( !itemExist)
    {
        if( m_MeasStatData.count() >= MEAS_MAX_COUNT )  //! del index0 Item
        {
            measStatData *pData = list_at(m_MeasStatData, 0);
            delete pData;
            pData = NULL;
            m_MeasStatData.removeAt(0);
        }

        if((m_CmdMeasType >= Meas_DOUBLE_SRC_TYPE)&&(m_CmdMeasType < Meas_DOUBLE_SRC_TYPE_END))
        {
            addStatItemSafe( m_CmdMeasType, m_CmdSrcA, m_CmdSrcB);
        }
        else
        {
            addStatItemSafe(m_CmdMeasType, m_CmdSrcA, chan_none);
        }
    }
    m_MeasDataMutex.unlock();

    if( !itemExist)
    {
        checkEngineStatus();
    }
}

DsoErr servMeasure::checkSource(int srcA, int srcB)
{
    if( (srcA>=d0 && srcA <=d15) ||
        (srcB>=d0 && srcB <=d15) )
    {
        if( sysCheckLicense( OPT_MSO ))
        {
            //valid
        }
        else
        {
            //not a mso
            return ERR_INVALID_INPUT;
        }
    }

    if(m_CmdMeasType >= Meas_DOUBLE_SRC_TYPE &&
       m_CmdMeasType <= Meas_DOUBLE_SRC_TYPE_END  )
    {
        if( !get_bit( m_ChMask, srcA) )
        {
            serviceExecutor::send(serv_name_vert_mgr,
                                  servVertMgr::cmd_open_source,
                                  srcA);

        }

        if( !get_bit( m_ChMask, srcB ) )
        {
            serviceExecutor::send(serv_name_vert_mgr,
                                  servVertMgr::cmd_open_source,
                                  srcB);
        }
    }
    else
    {
        if( !get_bit( m_ChMask, srcA ) )
        {
            serviceExecutor::send(serv_name_vert_mgr,
                                  servVertMgr::cmd_open_source,
                                  srcA);
        }
    }
    return ERR_NONE;
}

void servMeasure::cmd_SetSrcB(Chan src)
{
    if(  (m_CmdMeasType >= Meas_DOUBLE_SRC_TYPE)
       &&(m_CmdMeasType < Meas_DOUBLE_SRC_TYPE_END))
    {
        m_CmdSrcB = src;
    }
    else
    {
        m_CmdSrcB = chan_none;
    }
}

bool  servMeasure::cmd_GetValueStatus(void)
{
    return CMeasFormater::mCmdGetValid;
}

DsoErr servMeasure::cmd_Clear( int index)
{
    DsoErr err = ERR_NONE;

    if( index == (int)ALL_ITEM)
    {

        err = ssync( MSG_APP_MEAS_CLEAR_ALL, 1 );
        return err;
    }

    int size = m_MeasStatData.size();
    if( (index >= 0)&&(index<size) )
    {
        //! Set current selected item to index
        ssync( servMeasure::MSG_CMD_SEL_ITEM, index);

        //! Clear selected item
        err = ssync( MSG_APP_MEAS_CLEAR_ONE, 1);
    }

    return err;
}

DsoErr servMeasure::cmd_SetSelItem( int sel)
{
    m_CmdSelItem = sel;

    return ERR_NONE;
}

int servMeasure::cmd_GetSelItem()
{
    return m_CmdSelItem;
}

//read auto value
void servMeasure::cmd_GetMeas(CArgument &arg, CArgument &argOut )
{
    CMeasFormater::mCmdGetValid = true;

//! 保存TraceId，解决测试系统第一次读值错误
    servMeasure::m_nConfigEngineID = servHori::getAttrId();
    servMeasure::m_nRemoteList++;
    startTimer(4000,0,timer_repeat);

    cmd_SetMeas(arg);
    argOut.setVal( 9.9E+37, 0 );
}


//read stat value
void servMeasure::cmd_GetStatMeas(CArgument &arg, CArgument &argOut )
{
    CMeasFormater::mCmdGetValid = true;
    int statType = arg[0].iVal;
    CArgument argin;
    int inArgSize = arg.size();
    if( inArgSize >= 4 )
    {
        argin.append(arg[1].iVal);
        argin.append(arg[2].iVal);
        argin.append(arg[3].iVal);
    }
    else if( inArgSize == 3)
    {
        argin.append(arg[1].iVal);
        argin.append(arg[2].iVal);
    }
    else if( inArgSize == 2)
    {
        argin.append(arg[1].iVal);
    }
    else
    {}
    m_CmdStatType = (StatType)statType;

//! 保存TraceId，解决测试系统第一次读值错误
    servMeasure::m_nConfigEngineID = servHori::getAttrId();
    servMeasure::m_nRemoteList++;
    startTimer(4000,0,timer_repeat);

    cmd_SetStatMeas(argin);
    argOut.setVal( 9.9E+37, 0 );
}

void servMeasure::cmd_GetValueItem(CArgument &argOut)
{
    argOut.setVal(m_CmdMeasType,0);
    argOut.setVal(m_CmdSrcA,1);
    argOut.setVal(m_CmdSrcB,2);
}


DsoErr servMeasure::cmd_AddValueItem(CArgument)
{
    //for bug1674 by hxh
    if( !isActive() )
    {
        setActive();
    }

    return ERR_NONE;
}


float servMeasure::cmd_GetEposSA(CArgument &arg)
{
    m_CmdEposType = EPOS_SA;
    return cmd_GetEpos(arg);
}

float servMeasure::cmd_GetEposSB(CArgument &arg)
{
    m_CmdEposType = EPOS_SB;
    return cmd_GetEpos(arg);
}

float servMeasure::cmd_GetEpos(CArgument &arg)
{
    Chan src;
    meas_algorithm::MEASURE *rawMeas = 0;
    float epos = 0.0;

    if( arg.size() != 0)
    {
        src = (Chan)arg[0].iVal;
    }
    else
    {
        src = (Chan)m_SrcA;
    }

    if( src != chan_none)
    {
        measData* eposData = new measData( src);

        if( eposData != NULL)
        {
            eposData->setRegion( (RegionType)m_Region, m_ZoomOn, get_CursorA(), get_CursorB());
            eposData->CalcRealMeasData_SOFT(m_ThresholdList.value(src),
                                            getStateMethodMode(),
                                            getTopMethod(),
                                            getBaseMethod());
            //! Get epos
            rawMeas = eposData->getRawMeasData();
            if( m_CmdEposType == EPOS_SA)
            {
                epos = (float)(eposData->getXPos( rawMeas->REdgeX1));
                if( epos > wave_width/2)
                {
                    epos = -1.0;
                }
            }
            else if( m_CmdEposType == EPOS_SB)
            {
                epos = (float)(eposData->getXPos( rawMeas->REdgeX1));
                if( epos <= wave_width/2)
                {
                    epos = -1.0;
                }
            }
            else
            {
                epos = 0.0;
            }

            if( epos <= 0.0)
            {
                epos = -1.0;
            }
        }
        else
        {
            epos = -1.0;
        }

        if( eposData != NULL)
        {
            delete eposData;
            eposData = NULL;
        }
    }
    else
    {
        epos = -1.0;
    }

    return epos;
}

void servMeasure::cmd_GetMeasValue(CArgument &arg)
{
    measStatData *pStatData = 0;
    float measVal = 9.9E+37;
    bool  cmdMeasValue_Exist = false;

    if( servMeasure::m_bTraceTimeout )
    {
        cmdMeasValue_Exist = true;
        servMeasure::m_bTraceTimeout = false;
    }

    if( m_MeasValueReady || cmdMeasValue_Exist )
    {
        m_MeasDataMutex.lock();
        for(int i=0; i<m_MeasStatData.size(); i++)
        {
            pStatData = list_at( m_MeasStatData,i);

            if(  (m_CmdSrcA == pStatData->getSrcA())
               &&(m_CmdSrcB == pStatData->getSrcB())
               &&(m_CmdMeasType == pStatData->getType()) )
            {
                MeasStatus result = pStatData->getValStatus();

                cmdMeasValue_Exist = false;
                if( result  != INVALID )
                {
                    /*  测量结果有效，软件测量，
                     *  通过ID过滤不正常的结果，等待返回
                     *  其它错误，立即返回结果 */
                    cmdMeasValue_Exist = true;
                    /*if( result == VALID &&
                        servMeasure::m_bSoftCalc &&
                        (m_nConfigEngineID != servHori::getAttrId() ||
                         m_nConfigEngineID != m_nWfmTraceID)  )
                    {
                        cmdMeasValue_Exist = false;
                    }*/
                }
                else
                {
                    //qDebug() << "invalid result";
                }

                switch (m_CmdStatType)
                {
                case STAT_AVER:
                    measVal = pStatData->getAvg();
                    break;

                case STAT_CURR:
                    measVal = pStatData->getCurr();
                    break;

                case STAT_DEV:
                    measVal = pStatData->getDev();
                    break;

                case STAT_MAX:
                    measVal = pStatData->getMax();
                    break;

                case STAT_MIN:
                    measVal = pStatData->getMin();
                    break;

                default:
                    measVal = 9.9E+37;
                    break;
                }
                break;
            }
        }
        m_MeasDataMutex.unlock();
    }

    arg.setVal( cmdMeasValue_Exist, 0);
    arg.setVal( measVal, 1);
}

volatile bool CMeasFormater::mCmdGetValid = false;

/*! ------------------------------------------------------------------------------*/

CMeasFormater::CMeasFormater()
{
    mCmdGetValid = false;
}

CMeasFormater::~CMeasFormater()
{
    mCmdGetValid = false;

}

void CMeasFormater::startup()
{
    //qDebug() << "1.." << QTime::currentTime() << "......SCPI get measure" ;
    //mCmdGetValid = true;
}

bool CMeasFormater::isWaiting()
{
    //float measValue = 0.0;
    bool  measExist = false;
    CArgument meas;
    serviceExecutor::query(serv_name_measure,
                           servMeasure::MSG_CMD_GET_VALUE, meas);
    measExist = meas[0].bVal;

    // qDebug() << "3.." << QTime::currentTime() << "......ready? " << measExist;
    if( measExist)
    {
        CVal val;
        val.setVal(meas[1].fVal);

        format(val);
        //mCmdGetValid = false;

        servMeasure::m_nRemoteList--;
        if( servMeasure::m_nRemoteList < 0 )
        {
            servMeasure::m_nRemoteList = 0;
        }
    }

    return (!measExist);
}




