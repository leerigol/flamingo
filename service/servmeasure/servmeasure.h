#ifndef SERVMEASURE_H
#define SERVMEASURE_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "./meas_thread.h"
#include "../../include/dsotype.h"
#include "meas_interface.h"
#include "meas_dsrc.h"
#include "meas_stat.h"
#include "meas_threshold.h"
#include "../servUPA/upapowerqdata.h"
#include "../../engine/base/enginecfg.h"

#define Reading_FPGA 1
#define Not_Reading_FPGA 0
#define FPGA_MEAS_Ready 1
#define FPGA_MEAS_NOT_Ready 0

#define HISTO_MEAS_TIMEOUT 2000  //! ms
#define MEAS_MAX_COUNT 10    //! for measItem max count

class servMeasure : public serviceExecutor,
                    public ISerial
{
    Q_OBJECT

protected:
    DECLARE_CMD()

public:
    servMeasure( QString name, ServiceId eId = E_SERVICE_ID_MEASURE );
    enum
    {
        cmd_base = 0,
        MSG_TO_ADD_MENU,
        MSG_SHOW_ITEMBOX,

        MSG_ADD_STAT_ITEM,
        MSG_DEL_STAT_ITEM,
        MSG_DEL_ALL_STAT_ITEM,

        MSG_UPDATE_MEAS,

        MSG_MEAS_DATA,
        MSG_DSRC_MEAS_DATA,
        MSG_ALL_MEAS_DATA,
        MSG_STAT_MEAS_DATA,

        MSG_TRACE_UPDATED,

        //! scpi
        MSG_CMD_ADD_VALUE_ITEM,
        MSG_CMD_SET_MEAS,
        MSG_CMD_GET_MEAS,
        MSG_CMD_SET_STAT_MEAS,
        MSG_CMD_GET_STAT_MEAS,

        MSG_CMD_GET_EPOS_SA,
        MSG_CMD_GET_EPOS_SB,
        MSG_CMD_GET_VALUE_STATUS,
        MSG_CMD_GET_VALUE,
        MSG_CMD_GET_VALUE_ITEM,

        MSG_CMD_CLEAR,
        MSG_CMD_SEL_ITEM,

        //! ui
        MSG_GET_SUB_MENU,

        //! spy on
        cmd_on_ch_active,
        cmd_hor_setting_change,
        cmd_hor_zoom_on,

        cmd_ch1_setting_change,
        cmd_ch2_setting_change,
        cmd_ch3_setting_change,
        cmd_ch4_setting_change,

        cmd_on_ch1_on_off,
        cmd_on_ch2_on_off,
        cmd_on_ch3_on_off,
        cmd_on_ch4_on_off,
        cmd_on_la_on_off,

        cmd_on_math1_on_off,
        cmd_on_math2_on_off,
        cmd_on_math3_on_off,
        cmd_on_math4_on_off,

        //! FPGA
        cmd_set_FPGA_CntType,

        //! Threshold Level
        cmd_get_Th_HighPos,
        cmd_get_Th_MidPos,
        cmd_get_Th_LowPos,

        //! Histogram
        cmd_get_histo_meas,
        cmd_set_select_item,
        cmd_on_histo_meas_en,

        cmd_meas_val_ready,

        cmd_meas_upa_show,
    };

    enum StatType
    {
        STAT_MAX,
        STAT_MIN,
        STAT_CURR,
        STAT_AVER,
        STAT_DEV,
    };

    enum EposType
    {
        EPOS_SA,
        EPOS_SB,
    };

    enum FPGADlySrc
    {
        FPGA_LA0 = 0,
        FPGA_LA1,
        FPGA_LA2,
        FPGA_LA3,
        FPGA_LA4,
        FPGA_LA5,
        FPGA_LA6,
        FPGA_LA7,

        FPGA_LA8,
        FPGA_LA9,
        FPGA_LA10,
        FPGA_LA11,
        FPGA_LA12,
        FPGA_LA13,
        FPGA_LA14,
        FPGA_LA15,

        FPGA_CHa,
        FPGA_CHb,
        FPGA_CHc,
        FPGA_CHd,
    };

    enum FPGADlyEdgeType
    {
        FPGA_Dly_fall = 0,
        FPGA_Dly_Rise,
    };

    enum FPGACntEdgeType
    {
        FPGA_Cnt_Edge_Fall = 0,
        FPGA_Cnt_Edge_Rise = 1,
    };

    enum FPGACntPulseType
    {
        FPGA_Cnt_Pulse_N = 0,
        FPGA_Cnt_Pulse_P = 1,
    };

    enum FPGAEnType     //! bit2:LA  bit1:CH,  bit0:Main0/Zoom1
    {
        FPGA_CH_MAIN_EN    = 2,
        FPGA_LA_MAIN_EN    = 4,
        FPGA_CH_LA_MAIN_EN = 6,
        FPGA_CH_ZOOM_EN    = 3,
        FPGA_LA_ZOOM_EN    = 5,
        FPGA_CH_LA_ZOOM_EN = 7,

        FPGA_CH_LA_DIS     = 0,  //! Disable CH LA
    };

    enum FPGATopBaseMethod
    {
        FPGA_TopBase_Auto,
        FPGA_TopBase_Hist,
        FPGA_TopBase_MaxMin,
    };

    enum CMDClearItem
    {
        ITEM1,
        ITEM2,
        ITEM3,
        ITEM4,
        ITEM5,
        ITEM6,
        ITEM7,
        ITEM8,
        ITEM9,
        ITEM10,

        ALL_ITEM,
    };

private:
    int  m_SrcA;
    int  m_SrcB;
    int  m_Category;
    int  m_MeasAllSrc;

    int  m_Range_Mode;
    int  m_Set_Type;

    int  m_ThresholdSrc;
    int  m_TH_Type;
    qint64 m_HighPer;
    qint64 m_HighAbs;

    qint64 m_MidPer;
    qint64 m_MidAbs;

    qint64 m_LowPer;
    qint64 m_LowAbs;

    int  m_Region;
    int  m_CursorA;
    int  m_CursorB;
    int  m_CursorAB;

    bool m_Method;
    int  m_Top_Method;
    int  m_Base_Method;

    bool m_StatEnable;
    bool m_TrendEnable;
    int  m_StatCount;

    bool m_Indicator;

    bool m_bAppShow;
    int  m_SubMenuMsg;
    bool m_ZoomOn;  //! Zoom status

    qint64 m_ChMask;

    //For SCPI
    float    m_CmdMeasValue;

    MeasType m_CmdMeasType;
    StatType m_CmdStatType;
    Chan     m_CmdSrcA;
    Chan     m_CmdSrcB;
    int      m_CmdSelItem;

    EposType m_CmdEposType;

    bool     m_MeasValueReady;

    //! Histogram
    bool m_HistoMeasEn;
    int  m_HistoSelectItem;
    QQueue<MeasValue> m_HistoMeasBuffer;
    QQueue<MeasValue> m_HistoMeasOutBuffer;
    QMutex m_HistoMeasMutex;

private:
    QList<measData*>      m_MeasData;
    QList<measDataDSrc*>  m_MeasDataDSrc;
    QList<measStatData*>  m_MeasStatData;
    QList<CArgument>      m_CurrMeasStatItem;  //! for serialIn
    int                   m_MeasStatData_Count;
    measData*             m_MeasAllData;

    static Chan _ThresholdSrcList[];
    QMap<Chan, ThresholdData*> m_ThresholdList;

    //FPGA Data
    EngineMeasCfg *m_FPGAMeasCfg;
    EngineMeasRet *m_FPGAMeasRet;

private:
    QThread *m_pMeasureThread;
    QMutex m_MeasDataMutex;

protected:
    QSemaphore m_MeasSrcSema;
    static const int MEAS_NORM_TIM = 10;   //! 10ms:Normal Meas update time
    static const int MEAS_ROLL_TIM = 1000; //! 1s:  Roll/SCAN(<100M) FPGA Meas update time
    static const int MEAS_SCAN_TIM = 2000; //! 2S:  SCAN(>=100M) FPGA Meas update time

public:
    int  serialOut( CStream &stream, serialVersion &ver );
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();
    void init();

    virtual DsoErr start();
    virtual void registerSpy();

    DsoErr onActive(int msg);
    DsoErr onEnterSubMenu(int msg);
    DsoErr onReturnMainMenu(int msg);
    int    getSubMenu();
    void   onSystemStatus();
    DsoErr onTraceTimeout();

    static int  m_nConfigEngineID;
    static int  m_nWfmTraceID;
    static bool m_bSoftCalc;

    static bool m_bTraceTimeout;
    static int  m_nRemoteList;

protected:
    void measInit();

public:
    DsoErr set_SrcA(int v);
    int    get_SrcA();
    DsoErr set_SrcB(int v);
    int    get_SrcB();
    DsoErr set_Category(int v);
    int    get_Category();

    DsoErr set_MeasAllSrc(int v);
    int    get_MeasAllSrc();

    DsoErr set_To_RemoveMenu();
    DsoErr set_To_AddMenu();

    DsoErr set_RangeMode(int v);
    int    get_RangeMode();
    DsoErr set_SetType(int v);
    int    get_SetType();

    DsoErr set_ThresholdSrc(int v);
    int    get_ThresholdSrc();
    DsoErr set_ThresholdType(int v);

    DsoErr set_Th_High_Type(int v);
    int    get_Th_High_Type();
    DsoErr set_Th_High(qint64 v);
    qint64 get_Th_high();

    DsoErr set_Th_Mid_Type(int v);
    int    get_Th_Mid_Type();
    DsoErr set_Th_Mid(qint64 v);
    qint64 get_Th_Mid();

    DsoErr set_Th_Low_Type(int v);
    int    get_Th_Low_Type();
    DsoErr set_Th_Low(qint64 v);
    qint64 get_Th_Low();

    DsoErr set_HighPer(qint64 v);
    qint64 get_HighPer();
    DsoErr set_MidPer(qint64 v);
    qint64 get_MidPer();
    DsoErr set_LowPer(qint64 v);
    qint64 get_LowPer();

    DsoErr set_HighAbs(qint64 v);
    qint64 get_HighAbs();
    DsoErr set_MidAbs(qint64 v);
    qint64 get_MidAbs();
    DsoErr set_LowAbs(qint64 v);
    qint64 get_LowAbs();

    int get_Th_HighPos();
    int get_Th_MidPos();
    int get_Th_LowPos();

    DsoErr set_Th_Default();

    DsoErr set_Region(int v);
    int    get_Region();

    DsoErr set_CursorA(int v);
    int    get_CursorA();
    DsoErr set_CursorB(int v);
    int    get_CursorB();
    DsoErr set_CursorAB(int v);
    int    get_CursorAB();

    DsoErr set_Method(bool b);
    bool   get_Method();
    DsoErr set_Top_Method(int v);
    int    get_Top_Method();
    DsoErr set_Base_Method(int v);
    int    get_Base_Method();

    void   clickClear();
    DsoErr set_Delete(bool b);
    DsoErr set_DeleteAll(bool b);

    DsoErr set_Indicator(bool b);
    bool   get_Indicator();

    DsoErr set_StatEnable(bool b);
    bool   get_StatEnable();

    DsoErr set_TrendEnable(bool b);
    bool   get_TrendEnable();

    DsoErr set_StatReset();
    DsoErr set_StatCount(int v);
    int    get_StatCount();

    DsoErr set_Counter();
    DsoErr set_DVM();
    DsoErr set_UPA();
    DsoErr set_UPA_Visable(bool b);
    DsoErr set_Eye_Jit();
    DsoErr set_HISTO();
    DsoErr set_Zone_Trig();

    //! For Range Cursor limit
    DsoErr limit_rangeCursor(int &v, int min, int max);
    DsoErr limit_rangeCursorAB(int &cur1, int &cur2, int &v, int highLimit);

    //add by hxh
    DsoErr set_AppShow(bool b);
    bool   get_AppShow(void);

    DsoErr addMeasSrc(Chan src);

    DsoErr toAddMenu();

    DsoErr addStatItem(CArgument &arg);
    DsoErr delStatItem(int row);
    DsoErr delAllStatItem(void);

    void*  get_AllMeasData(void);
    void*  get_MeasStatData(void);

    DsoErr setMeasSrcReady(void *ptr);
    void   checkEngineStatus();
    bool   isEngineStop();

    void   setMeasValReady();
    DsoErr set_UpdateMeas(bool b);
    void*  get_UpdateMeas(void);
    bool   haveMeasData(void);

    bool   traceValid();
    bool   hasMathSrc();

    void   initThresholdList();
    void   resetThresholdList();

    //for similator
    double getMeasItemCurValue(void);

    //For SCPI cmd
    DsoErr cmd_SetMeas(CArgument &arg);
    void   cmd_GetMeas(CArgument &arg, CArgument &argOut );
    DsoErr cmd_SetStatMeas(CArgument &arg);
    void   cmd_GetStatMeas(CArgument &arg, CArgument &argOut );

    float  cmd_GetEposSA(CArgument &arg);
    float  cmd_GetEposSB(CArgument &arg);
    float  cmd_GetEpos(CArgument &arg);

    void   cmd_SetSrcB(Chan src);
    bool   cmd_GetValueStatus(void);
    void   cmd_GetMeasValue(CArgument &arg);
    void   cmd_GetValueItem(CArgument &argOut);
    DsoErr cmd_AddValueItem(CArgument);
    void   cmd_AddMeas(void);

    DsoErr cmd_Clear( int index);
    DsoErr cmd_SetSelItem( int sel);
    int    cmd_GetSelItem();

    bool   cmd_GetMeasValueReady();

    //! -- spy
    void on_horizontal_changed();
    void on_HorZoomOn();
    void on_ChActive();

    void on_ChChanged(Chan ch);
    void on_Ch1Changed();
    void on_Ch2Changed();
    void on_Ch3Changed();
    void on_Ch4Changed();

    void on_OnOffCh1();
    void on_OnOffCh2();
    void on_OnOffCh3();
    void on_OnOffCh4();

    void on_OnOffMath1();
    void on_OnOffMath2();
    void on_OnOffMath3();
    void on_OnOffMath4();
    void on_OnOffMath(QString servName, Chan mathSrc);

    void on_LaOnOff();
    void on_HistoMeasEn();

    void on_DisplayClear();
    void on_RunStop();
    void on_TrigSweep();
    void on_Timemode();
    void on_License();

    //! ----------Histogram------------------------------------------
    //! Meas Thread post all current meas val
    //! servHisto::cmd_meas_update
    void postHistoMeas();

    //! Meas Thread update HistoMeas buffer(servHisto selected meas item)
    void updateHistoMeas();

    //! servHisto set selected meas item
    //! servMeasure::cmd_set_select_item
    void setHistoSelectItem( int sel);

    //! servHisto get HistoMeas buffer
    //! servMeasure::cmd_get_histo_meas
    pointer getHistoMeas();

    //! if Histo Meas timeout, clear HistoMeas buffer
    void clearHistoMeasBuffer();

protected:
    meas_algorithm::StateMethodMode getStateMethodMode();
    meas_algorithm::StateMethod     getTopMethod();
    meas_algorithm::StateMethod     getBaseMethod();

    bool calcMeasResult(void);
    void buildBaseMeasData();
    void clearBaseMeasData();
    bool haveTraceSrc();

    //! FPGA
    void setFPGA_Region();
    void setFPGA_MeasCfg();
    void setFPGA_MeasOnce();
    Chan getAdcCH( Chan adcCH);
    void setFPGA_MeasDSrc();
    void setFPGA_Dly( measDataDSrc *pData);
    void setFPGA_DlySrcA( MeasFPGA_DlyIndex index, EngineAdcCoreCH *adcCH, Chan srcA);
    void setFPGA_DlySrcB( MeasFPGA_DlyIndex index, EngineAdcCoreCH *adcCH, Chan srcB);

    void setFPGA_Dly_edge_from_ch(MeasFPGA_DlyIndex index, FPGADlySrc ch);
    void setFPGA_Dly_edge_from_type(MeasFPGA_DlyIndex index, MeasType type);
    void setFPGA_Dly_edge_to_ch( MeasFPGA_DlyIndex index, FPGADlySrc ch);
    void setFPGA_Dly_edge_to_type( MeasFPGA_DlyIndex index, MeasType type);
    MeasFPGA_DlyIndex getFPGA_Dly_index();
    int  getUpdateTime();

    //! FPGA Cnt Type
    void setFPGA_CntType(CArgument arg);

    void getFPGA_MeasData();
    bool isFPGA_Mode();
    bool isFPGA_Src(Chan src);
    //! For FPGA Measure Test
    DsoErr set_Test();
    void getFPGA_MeasData_Test();

private:
    measData*     getMeasDataCore(Chan srcA);
    measDataDSrc* getMeasDataCore(MeasType type, Chan srcA, Chan srcB);

    DsoErr addMeasSrcSafe(Chan src);
    DsoErr addMeasDSrcSafe(MeasType type, Chan srcA, Chan srcB);
    void   addMeasDSrc(MeasType type, Chan srcA, Chan srcB);
    DsoErr addStatItemSafe(MeasType type, Chan srcA, Chan srcB);

    bool isCurrAllSrc(Chan src);
    bool isStatDataSrc(Chan src);
    bool isStatDataSrc(Chan srcA, Chan srcB, MeasType type);

    DsoErr checkSource(int srcA, int srcB);

friend class servMeasureThread;
};



#endif // SERVMEASURE_H
