#include "meas_stat.h"

measStatData::measStatData(MeasType type, Chan srcA, Chan srcB)
{

    m_Type = type;
    m_SrcA = srcA;
    m_SrcB = srcB;

    m_Curr = 0;
    m_Avg  = 0;
    m_Max  = 0;
    m_Min  = 0;
    m_Dev  = 0;
    m_Cnt  = 0;
    m_AvgSquare = 0;

    m_Ax.bVisible = false;
    m_Ax.mPos = 0;

    m_Bx.bVisible = false;
    m_Bx.mPos = 0;

    m_Ay.bVisible = false;
    m_Ay.mPos = 0;

    m_By.bVisible = false;
    m_By.mPos = 0;

    m_Status = INVALID;
    setCntLimit(100);

    m_Unit = Unit_V;

    m_RawCurrVal.mAx.bVisible = false;
    m_RawCurrVal.mBx.bVisible = false;
    m_RawCurrVal.mAy.bVisible = false;
    m_RawCurrVal.mBy.bVisible = false;
    m_RawCurrVal.mStatus = INVALID;
    m_RawCurrVal.mUnit = Unit_V;
    m_RawCurrVal.mVal = 0;

    m_TraceId = 0;
}

void measStatData::setAvg(float val)
{
    if(m_Cnt == 1)
    {
        m_Avg = val;
    }
    else
    {
        m_Avg = m_Avg + (val-m_Avg)/m_Cnt;
    }

}

void measStatData::setMax(float val)
{
    if(m_Cnt == 1)
    {
        m_Max = val;
    }
    else
    {
        m_Max = (val>m_Max)? val : m_Max;
    }
}

void measStatData::setMin(float val)
{
    if(m_Cnt == 1)
    {
        m_Min = val;
    }
    else
    {
        m_Min = (val<m_Min)? val : m_Min;
    }
}

void measStatData::setDev(float val)
{
    if(m_Cnt == 1)
    {
        m_AvgSquare = val * val;
        m_Dev = 0;
    }
    else
    {
        m_AvgSquare = m_AvgSquare + (val*val - m_AvgSquare)/m_Cnt;
        if((m_AvgSquare-m_Avg*m_Avg) <= 0)
        {
            m_Dev = 0;
        }
        else
        {
            m_Dev = sqrt(m_AvgSquare - m_Avg*m_Avg);
        }
    }
}

void measStatData::setCnt(void)
{
    if(m_Cnt >= m_CntLimit)
    {
        m_Cnt = m_CntLimit;
    }
    else
    {
        m_Cnt  = m_Cnt + 1;
    }
}

void measStatData::setCntLimit(ulong cntLimit)
{
    resetStatVal();
    m_CntLimit = cntLimit;
}

void  measStatData::updateStatVal(float val, MeasStatus stat)
{
    LOG_DBG()<<"-------------val: "<<val;
    LOG_DBG()<<"-------------MeasStat: "<<stat;

    m_Status = stat;

    if(  (stat == VALID)
       ||(stat == GREATER_CLIPPED)
       ||(stat == LESS_CLIPPED) )
    {
        setCnt();
        setCurr(val);
        setMax(val);
        setMin(val);
        setAvg(val);
        setDev(val);
    }
    else
    {
        setCurr(9.9E+37);
        m_Avg  = 9.9E+37;
        m_Max  = 9.9E+37;
        m_Min  = 9.9E+37;
        m_Dev  = 9.9E+37;
        m_Cnt  = 0;
    }
}

void measStatData::updateStatVal(MeasValue mval, MeasStatus stat)
{
    m_Status = stat;
    m_RawCurrVal = mval;

    if( (stat == VALID)
       /*||(stat == GREATER_CLIPPED)
       ||(stat == LESS_CLIPPED)*/ ) //by hxh for bug3127
    {
        setCnt();
        setCurr(mval.mVal);
        setMax(mval.mVal);
        setMin(mval.mVal);
        setAvg(mval.mVal);
        setDev(mval.mVal);

        m_Unit = mval.mUnit;

        m_Ax = mval.mAx;
        m_Bx = mval.mBx;
        m_Ay = mval.mAy;
        m_By = mval.mBy;
    }
    else
    {
        setCurr(9.9E+37);
        m_Avg  = 9.9E+37;
        m_Max  = 9.9E+37;
        m_Min  = 9.9E+37;
        m_Dev  = 9.9E+37;
        m_Cnt  = 0;

        m_Unit = mval.mUnit;

        m_Ax.bVisible = false;
        m_Bx.bVisible = false;
        m_Ay.bVisible = false;
        m_By.bVisible = false;
    }
}

///
/// \brief measStatData::resetStatVal
///
///  bug3263  hexiaohua 2018-05-31
void measStatData::resetStatVal(void)
{
    m_Curr = 9.9E+37;
    m_Avg  = 9.9E+37;
    m_Max  = 9.9E+37;
    m_Min  = 9.9E+37;
    m_Dev  = 0;
    m_Cnt  = 0;
}

