/*****************************************************************************
                普源精电科技有限公司版权所有(2004-2010)
******************************************************************************
头文件名: DsoDrvUI_Measure.h

功能描述: Measure操作的相关函数

作    者: YULE(原始)

版    本: 1.0.1

完成日期: 07/08/18

修改历史: 

作者          修改时间          版本        修改内容
GONG          09-05-04          1.0.1       增加采样率设置,直接计算水平参数,
                                            增加周期选择
                                            增加面积、方差以及单周期的面积、有效值、平均值
                                            增加最大最小值的水平位置
                                            增加上升时间门限可调
*****************************************************************************/
#ifndef MEASANALYZER_H
#define MEASANALYZER_H


//#include "data_type.h"

namespace meas_algorithm {

enum ThresholdType
{
    TH_TYPE_PER,
    TH_TYPE_ABS,
};

// Vtop and Vbase method Mode(Algorithms for determining state levels--IEEE Std181-2003)
enum StateMethodMode
{
    MODE_MANUAL,
    MODE_AUTO,
};

// Vtop and Vbase method (Algorithms for determining state levels--IEEE Std181-2003)
enum StateMethod
{
    METHOD_MaxMin,
    METHOD_Histogram,
};

//测量门限类型、电平
struct MeasThreshold
{
    ThresholdType eThreshHoldType;
    float f32ThreshHoldH;
    float f32ThreshHoldM;
    float f32ThreshHoldL;
};

//通道测量数据总结构体
typedef struct _MEASURE{

	bool bVaild_Vmax;
	bool bVaild_Vmin;
	bool bVaild_Vpp;

    /************************************************************************/
    /* Voltage Measurements                                                 */
    /************************************************************************/
    // Vmax
	short Vmax;  		   // 最大值

    // Vmin
	short Vmin;  		   // 最小值

    // Vpp
	short Vpp;  		   // 峰峰值

    // Vtop
	float Vtop;  		   // 顶端值

    // Vbase
	float Vbas;  		   // 底端值

    // threshold
    float Vupper;          // 高阈值 No Gnd
    float Vmid;            // 中阈值 No Gnd
    float Vlower;          // 低阈值 No Gnd
    // Vamp
	float Vamp;  		   // 幅度

    // Vrms
	float  Vrms;  		   // 全部数据的有效值
	float  Vrms_S;         // 单周期的有效值		   

    // Vavg
	float Vavg;            // 全部数据的平均值
	float Vavg_S;          // 单周期的有效值
	
	// StdDev
	float Vdev;            // 标准方差
    
    // Overshoot        
	float Vover;           // 过冲 
	short VoverY1;         // 过冲垂直位置1
	short VoverY2;         // 过冲垂直位置2
    
    // Preshoot
	float Vpre;            // 预冲
	short VpreY1;          // 预冲垂直位置1
	short VpreY2;          // 预冲垂直位置2

    /************************************************************************/
    /* Time Measrurements                                                   */
    /************************************************************************/
    // XMax
    int XMax;             // 最大值水平位置X
    
    // XMin
    int XMin;             // 最小值水平位置X
    
    // Period
	float Period;           // 周期
    int PeriodX1;  		// 周期水平位置1
    int PeriodX2;  		// 周期水平位置2
    short PeriodY;          // 周期垂直位置 Include Gnd

    // +Width
	float Width_P;          // 正脉宽
    int WidthPX1;  		// 正脉宽水平位置1
    int WidthPX2;         // 正脉宽水平位置2

    // -Width
	float Width_N;          // 负脉宽
    int WidthNX1;  		// 负脉宽水平位置1
    int WidthNX2;         // 负脉宽水平位置2

    // Rise Time            
	float RiseTime;  		// 上升时间   
    int RiseTimeX1;  		// 上升时间水平位置1
    int RiseTimeX2;  		// 上升时间水平位置2
    short RiseTimeY1;       // 上升时间垂直位置1
	short RiseTimeY2;       // 上升时间垂直位置2

    // Fall Time            
	float FallTime;  		// 下降时间   
    int FallTimeX1;  		// 下降时间水平位置1
    int FallTimeX2;  		// 下降时间水平位置2
	short FallTimeY1;       // 下降时间垂直位置1
	short FallTimeY2;       // 下降时间垂直位置2
    
    // Delay & Phase
    int RiseX1;           // 基准左侧延迟上升沿
    int RiseX2;           // 基准右侧延迟上升沿
    int FallX1;           // 基准左侧延迟下降沿
    int FallX2;  	        // 基准右侧延迟下降沿
        
    // 脉宽计数
    int PPulseCount;
    int NPulseCount;
    int PPulseX1;
    int PPulseX2;
    int NPulseX1;
    int NPulseX2;
    
    // 边沿计数
    int REdgeCount;
    int REdgeX1;
    int REdgeX2;
    int FEdgeCount;
    int FEdgeX1;
    int FEdgeX2;

    /************************************************************************/
    /* Area                                                                 */
    /************************************************************************/
    int Area;               // 全部数据面积
    int Area_S;             // 单周期面积
	
}MEASURE;

//通道测量数据总结构体//! Get Trace X index from Cursor position
int getTraceX(int rangeX1);
typedef struct _MEASURE_DLY{
    int EdgeX1;
    int EdgeX2;
    float Delay;
    short EdgeChan;     // 1 --- CH1, -1 --- CH2
}MEASURE_DLY;

struct MeasPowerQ
{
    short PowerQ_MAX;          //PowerQ MAX in all complete period
    float PowerQ_RMS;          //PowerQ RMS in all complete period
    float PowerQ_AVG;          //PowerQ AVG in all complete period
};

struct MeasRipple
{
    bool bValid_Vmax;
    bool bValid_Vmin;
    bool bValid_Ripple;

    short Vmax;  		   // 最大值
    short Vmin;  		   // 最小值
    short Ripple;  		   // 纹波值(峰峰值)
};

/*******************************************************************************
 函 数 名:     MeasAnalyser()
 描    述:	   自动测量算法
 输入参数:    			                                
 输出参数:     
 			   pMeasData	----- 测量数据指针
               s32Len		----- 测量数据长度
               s32Base      ----- 测量数据基准位置
 			   s16Gnd       ----- 波形偏移
               pMeasure     ----- 测量结构体
 返 回 值:     无	
 说    明:     
*******************************************************************************/
void MeasAnalyser(unsigned char *pMeasData, int s32Len, int s32Base, short s16Gnd, MeasThreshold th, MEASURE *pMeasure);

/*****上升时间测量算法*************************************************************/
void MeasRiseTime(unsigned char *pMeasData, int s32Len, short s16Gnd, int s32RiseEdge, MEASURE *pMeasure);

/*****下降时间测量算法*************************************************************/
void MeasFallTime(unsigned char *pMeasData, int s32Len, short s16Gnd, int s32FallEdge, MEASURE *pMeasure);

/*******************************************************************************
 函 数 名:     MeasUpaPowerQ()
 描    述:	   UPA Power Quality Measure
 输入参数:
 输出参数:
               pSrcData 	----- 测量数据指针
               s16Gnd       ----- 波形偏移
               FreqRef      ----- Power Quality Frequency Ref. Measure Data
               pMeasure  	----- 测量结构体
 返 回 值:     无
 说    明:
*******************************************************************************/
void UpaPowerQAnalyser(unsigned char *pSrcData, int s32Len, short s16Gnd, MEASURE *FreqRef, MeasPowerQ *powerQ);

/*******************************************************************************
 函 数 名:     UpaRippleAnalyser()
 描    述:	  UPA Ripple Analyser
 输入参数:
 输出参数:
               pData 	    ----- 纹波源数据
               pMeasure  	----- 纹波测量结果结构体
 返 回 值:     无
 说    明:
*******************************************************************************/
void UpaRippleAnalyser(unsigned char *pData, int s32Len, short s16Gnd, MeasRipple *pRippleVal);


/*******************************************************************************
 函 数 名:     SetMeasAnalysePeriod()
 描    述:	   设置测量的周期位置
 输入参数:    			                                
 输出参数:     
 			   s16Period	----- 测量周期
 返 回 值:     无	
 说    明:     最小周期数是1，如果设置超过屏幕的周期数则测量最后周期
*******************************************************************************/
void SetMeasAnalysePeriod(short s16Period);

/*******************************************************************************
 函 数 名:     SetStateMethod()
 描    述:     Set Vtop and Vbase method
              (Algorithms for determining state levels--IEEE Std181-2003)
 输入参数:     u8StateM -----State level Method Mode
              u8TopM  -----Vtop level Method
              u8BaseM -----Vbase level Method
 输出参数:

 返 回 值:     无
 说    明:
*******************************************************************************/
void SetStateMethod(StateMethodMode eStateM, StateMethod eTopM, StateMethod eBaseM);

/*******************************************************************************
 函 数 名:     SetMeasAnalyseSaRate()
 描    述:	   设置示波器采样率
 输入参数:    			                                
 输出参数:     
 			   f32SaRate	----- 采样率
 返 回 值:     无	
 说    明:     主要是为计算水平参数
*******************************************************************************/
void SetMeasAnalyseSaRate(float f32SaRate);

/*******************************************************************************
 函 数 名:     MeasDelayFF()
 描    述:	   自动测量下降沿延迟
 输入参数:    			                                
 输出参数:     
 			   pData	    ----- 测量数据指针
 			   s16Len		----- 测量数据长度
 			   pCH_M		----- 测量结构体
 			   s16Gnd       ----- 波形偏移
 			   pCH_DLY      ----- 测量结果
 返 回 值:     0 测量有效, -1 测量无效	
 说    明:     
*******************************************************************************/
short MeasDelayFF(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE* pCH_M1,
                 MEASURE* pCH_M2,
                 MEASURE_DLY* pCH_DLY);
                 
/*******************************************************************************
 函 数 名:     MeasDelayRR()
 描    述:	   自动测量上升沿延迟
 输入参数:    			                                
 输出参数:     
 			   pData	    ----- 测量数据指针
               s32Len		----- 测量数据长度
 			   pCH_M		----- 测量结构体
 			   s16Gnd       ----- 波形偏移
 			   pCH_DLY      ----- 测量结果
 返 回 值:     0 测量有效, -1 测量无效	
 说    明:     
*******************************************************************************/
short MeasDelayRR(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE *pCH_M1,
                 MEASURE *pCH_M2,
                 MEASURE_DLY *pCH_DLY);

/*******************************************************************************
 函 数 名:     MeasDelayRF()
 描    述:	   自动测量上升到下降沿延迟
 输入参数:
 输出参数:
               pData	    ----- 测量数据指针
               s32Len		----- 测量数据长度
               pCH_M		----- 测量结构体
               s16Gnd       ----- 波形偏移
               pCH_DLY      ----- 测量结果
 返 回 值:     0 测量有效, -1 测量无效
 说    明:
*******************************************************************************/
short MeasDelayRF(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE *pCH_M1,
                 MEASURE *pCH_M2,
                 MEASURE_DLY *pCH_DLY);

/*******************************************************************************
 函 数 名:     MeasDelayFR()
 描    述:	   自动测量下降到上升沿延迟
 输入参数:
 输出参数:
               pData	    ----- 测量数据指针
               s32Len		----- 测量数据长度
               pCH_M		----- 测量结构体
               s16Gnd       ----- 波形偏移
               pCH_DLY      ----- 测量结果
 返 回 值:     0 测量有效, -1 测量无效
 说    明:
*******************************************************************************/
short MeasDelayFR(unsigned char *pData1,
                 unsigned char *pData2,
                 int s32Base,
                 int s32Len,
                 short s16Gnd1,
                 short s16Gnd2,
                 MEASURE *pCH_M1,
                 MEASURE *pCH_M2,
                 MEASURE_DLY *pCH_DLY);

short MeasDealyEdgeF(MEASURE *pMeasure, unsigned char *pu8Data, short s16tempmax, short s16tempmin, int s32Len, int s32Base, int *s32Edge1, int *s32Edge2);

short MeasDealyEdgeR(MEASURE *pMeasure, unsigned char *pu8Data, short s16tempmax, short s16tempmin, int s32Len, int s32Base, int *s32Edge1, int *s32Edge2);

}
                                  
#endif
