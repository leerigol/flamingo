#include "meas_interface.h"

//! Get pix position of wav point index
//! Range: 0--999 (X Pix: 1000)
int measData::getXPos(int index)
{
    int XPos = 0;

    if( m_RegionType == REGION_CURSOR)
    {
        if( m_ZoomOn)
        {
            index += getTraceX( m_RangeX1, horizontal_view_zoom);
        }
        else
        {
            index += getTraceX( m_RangeX1, horizontal_view_main);
        }
    }

    float Xreal;
    Xreal = index * m_RealtInc + m_Realt0;

    horiAttr hAttr;
    if( m_RegionType == REGION_MAIN)
    {
        hAttr = getHoriAttr(m_Src, horizontal_view_main);
    }
    else
    {
        if( m_ZoomOn)
        {
            hAttr = getHoriAttr(m_Src, horizontal_view_zoom);
        }
        else
        {
            hAttr = getHoriAttr(m_Src, horizontal_view_main);
        }
    }

    XPos = qRound(Xreal / hAttr.inc + hAttr.gnd);

    return XPos;
}

//! Get pix position of wav point yAdc
//! Range:0--479 (Y Pix: 480)
int measData::getYPos(float yAdc)
{
    int YPos = 0;
    float Yreal = yAdc * m_RealyInc;
    YPos = 240 - qRound((Yreal + m_RealOffset)/m_RealScale*scr_vdiv_dots);

    return YPos;
}

void measData::setIndicatorVisible( MeasValue& val, bool b)
{
    val.mAx.bVisible = b;
    val.mBx.bVisible = b;
    val.mAy.bVisible = b;
    val.mBy.bVisible = b;
}

void measData::calc_Period( int xPos, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Period;
    val.mUnit = Unit_s;

    if(  ( m_RawMeasData.bVaild_Vpp ) &&
         (m_RawMeasData.PeriodX1 > 0) &&
         (m_RawMeasData.PeriodX2 > 0) &&
         (m_RawMeasData.Period > 0.0) &&
         (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.PeriodX1) + xPos;
        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.PeriodX2) + xPos;

        if( isLaCH())
        {
            val.mAy.bVisible = false;
            val.mBy.bVisible = false;
        }
        else
        {
            val.mAy.bVisible = true;
            val.mAy.mPos = getYPos(m_RawMeasData.Vmid);
            val.mBy.bVisible = true;
            val.mBy.mPos = val.mAy.mPos;
        }
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if(  (m_RawMeasData.PeriodX1 <= 0) ||
                  (m_RawMeasData.PeriodX2 <= 0) ||
                  (m_RawMeasData.Period <= 0.0) )
        {
            val.mStatus = NO_PERIOD;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Period] = val;
}

void measData::calc_Freq( int xPos, int /*yPos*/ )
{
    MeasValue val;    
    val.mUnit = Unit_hz;

    if( m_RawMeasData.bVaild_Vpp &&
       (m_RawMeasData.PeriodX1 > 0) &&
       (m_RawMeasData.PeriodX2 > 0) &&
       (m_RawMeasData.Period > 0.0) &&
       (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mVal = 1/m_RawMeasData.Period;
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.PeriodX1) + xPos;
        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.PeriodX2) + xPos;

        if( isLaCH())
        {
            val.mAy.bVisible = false;
            val.mBy.bVisible = false;
        }
        else
        {
            val.mAy.bVisible = true;
            val.mAy.mPos = getYPos(m_RawMeasData.Vmid);
            val.mBy.bVisible = true;
            val.mBy.mPos = val.mAy.mPos;
        }
    }
    else
    {
        val.mVal = 0;
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if(  (m_RawMeasData.PeriodX1 <= 0)||(m_RawMeasData.PeriodX2 <= 0)
                ||(m_RawMeasData.Period <= 0.0) )
        {
            val.mStatus = NO_PERIOD;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Freq] = val;
}

void measData::calc_RiseTime( int xPos, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.RiseTime;
    val.mUnit = Unit_s;

    if( m_RawMeasData.bVaild_Vpp &&
        (m_RawMeasData.RiseTime > 0) &&
        (m_MeasDataStatus  == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.RiseTimeX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.RiseTimeY1);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.RiseTimeX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.RiseTimeY2);
    }
    else
    {
        if( m_MeasDataStatus  == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( m_RawMeasData.RiseTime <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_RiseTime] = val;
}

void measData::calc_FallTime( int xPos, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.FallTime;
    val.mUnit = Unit_s;

    if( m_RawMeasData.bVaild_Vpp &&
        (m_RawMeasData.FallTime > 0) &&
        (m_MeasDataStatus  == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.FallTimeX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.FallTimeY1);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.FallTimeX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.FallTimeY2);
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( m_RawMeasData.FallTime <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_FallTime] = val;
}

void measData::calc_PWidth( int xPos, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Width_P;
    val.mUnit = Unit_s;

    if( (val.mVal > 0) &&
        (m_RawMeasData.bVaild_Vpp) &&
       (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.WidthPX1) + xPos;
        val.mAy.bVisible = false;
        val.mAy.mPos = 0;

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.WidthPX2) + xPos;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( val.mVal <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_PWidth] = val;
}

void measData::calc_NWidth( int xPos, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Width_N;
    val.mUnit = Unit_s;

    if( (val.mVal > 0) &&
        (m_RawMeasData.bVaild_Vpp) &&
        (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.WidthNX1) + xPos;
        val.mAy.bVisible = false;
        val.mAy.mPos = 0;

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.WidthNX2) + xPos;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( val.mVal <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_NWidth] = val;
}

void measData::calc_PDuty( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mUnit = Unit_percent;

    if( (m_RawMeasData.Width_P > 0) &&
        (m_RawMeasData.Period > 0) &&
        (m_RawMeasData.bVaild_Vpp) &&
        (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mVal = m_RawMeasData.Width_P/m_RawMeasData.Period;  //! %
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.WidthPX1) + xPos;
        val.mAy.bVisible = false;
        val.mAy.mPos = 0;

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.WidthPX2) + xPos;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        val.mVal = 0;
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( m_RawMeasData.Width_P <= 0.0)
        {
            val.mStatus = NO_EDGE;
        }
        else if( m_RawMeasData.Period <= 0.0)
        {
            val.mStatus = NO_PERIOD;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_PDuty] = val;
}

void measData::calc_NDuty( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mUnit = Unit_percent;

    if( (m_RawMeasData.Width_N > 0) &&
        (m_RawMeasData.Period > 0) &&
        (m_RawMeasData.bVaild_Vpp) &&
        (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mVal = m_RawMeasData.Width_N/m_RawMeasData.Period;  //! %;
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.WidthNX1) + xPos;
        val.mAy.bVisible = false;
        val.mAy.mPos = 0;

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.WidthNX2) + xPos;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        val.mVal = 0;

        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( m_RawMeasData.Width_N <= 0.0)
        {
            val.mStatus = NO_EDGE;
        }
        else if( m_RawMeasData.Period <= 0.0)
        {
            val.mStatus = NO_PERIOD;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_NDuty] = val;
}

void measData::calc_PPulses( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mVal = m_RawMeasData.PPulseCount;
    val.mUnit = Unit_none;

    if( m_RawMeasData.bVaild_Vpp &&
        (val.mVal > 0) &&
        (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.PPulseX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.PPulseX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vlower);
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( val.mVal <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_PPulses] = val;
}

void measData::calc_NPulses( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mVal = m_RawMeasData.NPulseCount;
    val.mUnit = Unit_none;

    if( m_RawMeasData.bVaild_Vpp &&
        (val.mVal > 0) &&
        (m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.NPulseX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.NPulseX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vlower);
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( val.mVal <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_NPulses] = val;
}

void measData::calc_PEdges( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mVal = m_RawMeasData.REdgeCount;
    val.mUnit = Unit_none;

    if( m_RawMeasData.bVaild_Vpp && (val.mVal > 1)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.REdgeX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.REdgeX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vlower);
    }
    else if( (val.mVal > 0)&&(val.mVal <= 1) )
    {
        val.mStatus = VALID;

        if( m_RawMeasData.REdgeX1 == 0)
        {
            val.mAx.bVisible = false;
        }
        else
        {
            val.mAx.bVisible = true;
            val.mAx.mPos = getXPos(m_RawMeasData.REdgeX1);
        }
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        if( m_RawMeasData.REdgeX2 == 0)
        {
            val.mBx.bVisible = false;
        }
        else
        {
            val.mBx.bVisible = true;
            val.mBx.mPos = getXPos(m_RawMeasData.REdgeX2);
        }
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vlower);
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( val.mVal <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_PEdges] = val;
}

void measData::calc_NEdges( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mVal =m_RawMeasData.FEdgeCount;
    val.mUnit = Unit_none;

    if( m_RawMeasData.bVaild_Vpp && (val.mVal > 1)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.FEdgeX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.FEdgeX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vlower);
    }
    else if( (val.mVal > 0)&&(val.mVal <= 1) )
    {
        val.mStatus = VALID;
        if( m_RawMeasData.FEdgeX1 == 0)
        {
            val.mAx.bVisible = false;
        }
        else
        {
            val.mAx.bVisible = true;
            val.mAx.mPos = getXPos(m_RawMeasData.FEdgeX1);
        }
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        if( m_RawMeasData.FEdgeX2 == 0)
        {
            val.mBx.bVisible = false;
        }
        else
        {
            val.mBx.bVisible = true;
            val.mBx.mPos = getXPos(m_RawMeasData.FEdgeX2);
        }
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vlower);
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( val.mVal <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_NEdges] = val;
}

void measData::calc_Tvmax( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mVal =m_RawMeasData.XMax * m_RealtInc + m_Realt0;
    val.mUnit = Unit_s;

    if( m_RawMeasData.bVaild_Vpp && m_RawMeasData.bVaild_Vmax
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.XMax) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vmax);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vmax)
        {
            val.mStatus = CLIPPED;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Tvmax] = val;
}

void measData::calc_Tvmin( int xPos, int /*yPos*/ )
{
    MeasValue val;
    val.mVal =m_RawMeasData.XMin * m_RealtInc + m_Realt0;
    val.mUnit = Unit_s;

    if( m_RawMeasData.bVaild_Vpp && m_RawMeasData.bVaild_Vmin
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.XMin) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vmin);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vmin)
        {
            val.mStatus = CLIPPED;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Tvmin] = val;
}

void measData::calc_Pslew_rate( int xPos, int /*yPos*/ )
{
    MeasValue val;
    if( m_ChUnit == Unit_V)      val.mUnit = Unit_VdivS;
    else if( m_ChUnit == Unit_W) val.mUnit = Unit_WdivS;
    else if( m_ChUnit == Unit_A) val.mUnit = Unit_AdivS;
    else if( m_ChUnit == Unit_U) val.mUnit = Unit_UdivS;
    else                         val.mUnit = Unit_VdivS;

    if( m_RawMeasData.bVaild_Vpp && (m_RawMeasData.RiseTime > 0)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mVal = (m_RawMeasData.Vupper-m_RawMeasData.Vlower)*m_RealyInc/m_RawMeasData.RiseTime;
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.RiseTimeX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vlower);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.RiseTimeX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vupper);
    }
    else
    {
        val.mVal = 0;
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( m_RawMeasData.RiseTime <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Pslew_rate] = val;
}

void measData::calc_Nslew_rate( int xPos, int /*yPos*/ )
{
    MeasValue val;
    if( m_ChUnit == Unit_V)      val.mUnit = Unit_VdivS;
    else if( m_ChUnit == Unit_W) val.mUnit = Unit_WdivS;
    else if( m_ChUnit == Unit_A) val.mUnit = Unit_AdivS;
    else if( m_ChUnit == Unit_U) val.mUnit = Unit_UdivS;
    else                         val.mUnit = Unit_VdivS;

    if( m_RawMeasData.bVaild_Vpp && (m_RawMeasData.FallTime > 0)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mVal = (m_RawMeasData.Vlower-m_RawMeasData.Vupper)*m_RealyInc/m_RawMeasData.FallTime;
        val.mStatus = VALID;
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.FallTimeX1) + xPos;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.FallTimeX2) + xPos;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vlower);
    }
    else
    {
        val.mVal = 0;
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( m_RawMeasData.FallTime <= 0)
        {
            val.mStatus = NO_EDGE;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Nslew_rate] = val;
}

void measData::calc_Vmax( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vmax * m_RealyInc;
    val.mUnit = m_ChUnit;

    if(   m_RawMeasData.bVaild_Vmax
       && ((m_RawMeasData.Vmax - m_RawMeasData.Vmin) > 0)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vmax)
        {
            val.mStatus = GREATER_CLIPPED;
        }
        else if( (m_RawMeasData.Vmax - m_RawMeasData.Vmin) <= 0)
        {
            val.mStatus = LESS_CLIPPED;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vmax);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vmax] = val;
}

void measData::calc_Vmin( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vmin * m_RealyInc;
    val.mUnit = m_ChUnit;

    if(  m_RawMeasData.bVaild_Vmin
       &&((m_RawMeasData.Vmax - m_RawMeasData.Vmin) > 0)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vmin)
        {
            val.mStatus = LESS_CLIPPED;
        }
        else if( (m_RawMeasData.Vmax - m_RawMeasData.Vmin) <= 0)
        {
            val.mStatus = GREATER_CLIPPED;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vmin);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vmin] = val;
}

void measData::calc_Vpp( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vpp * m_RealyInc;
    val.mUnit = m_ChUnit;

//    qDebug()<<"measData::calc_Vpp() m_RawMeasData.Vmax = "<<m_RawMeasData.Vmax;
//    qDebug()<<"measData::calc_Vpp() m_RawMeasData.Vmin = "<<m_RawMeasData.Vmin;

    if( m_RawMeasData.bVaild_Vmax && m_RawMeasData.bVaild_Vmin
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( (m_RawMeasData.Vmax - m_RawMeasData.Vmin) <= 0)
        {
            val.mStatus = CLIPPED;
        }
        else if( (!m_RawMeasData.bVaild_Vmax) || (!m_RawMeasData.bVaild_Vmin))
        {
            val.mStatus = GREATER_CLIPPED;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vmin);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vmax);
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vpp] = val;
}

void measData::calc_Vtop( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vtop * m_RealyInc;
    val.mUnit = m_ChUnit;

    if( m_RawMeasData.bVaild_Vpp
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vtop);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vtop] = val;
}

void measData::calc_Vbase( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vbas * m_RealyInc;
    val.mUnit = m_ChUnit;

    if( m_RawMeasData.bVaild_Vpp
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vbas);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vbase] = val;
}

void measData::calc_Vamp( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vamp * m_RealyInc;
    val.mUnit = m_ChUnit;

    if( m_RawMeasData.bVaild_Vpp && m_RawMeasData.bVaild_Vmax && m_RawMeasData.bVaild_Vmin
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else if( (!m_RawMeasData.bVaild_Vmax) || (!m_RawMeasData.bVaild_Vmin))
        {
            val.mStatus = GREATER_CLIPPED;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vbas);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.Vtop);
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vamp] = val;
}

void measData::calc_Vupper( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vupper * m_RealyInc;
    val.mUnit = m_ChUnit;

    if( m_RawMeasData.bVaild_Vpp
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vupper);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vupper] = val;
}

void measData::calc_Vmid( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vmid * m_RealyInc;
    val.mUnit = m_ChUnit;

    if( m_RawMeasData.bVaild_Vpp
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vmid);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vmid] = val;
}

void measData::calc_Vlower( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;
    val.mVal = m_RawMeasData.Vlower * m_RealyInc;
    val.mUnit = m_ChUnit;

    if( m_RawMeasData.bVaild_Vpp
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vlower);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vlower] = val;
}

void measData::calc_Vavg( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vavg * m_RealyInc;
    val.mUnit = m_ChUnit;

//    qDebug()<<"measData::calc_Vavg() m_RawMeasData.Vavg = "<<m_RawMeasData.Vavg;
//    qDebug()<<"measData::calc_Vavg() m_RealyInc"<<m_RealyInc;
//    qDebug()<<"measData::calc_Vavg() m_MeasDataStatus"<<m_MeasDataStatus<<"\n";

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mStatus = VALID;

        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vavg);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        val.mStatus = NO_SIGNAL;
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vavg] = val;
}

void measData::calc_Vrms( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vrms * m_RealyInc;
    val.mUnit = m_ChUnit;

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mStatus = VALID;

        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vrms);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        val.mStatus = NO_SIGNAL;
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vrms] = val;
}

void measData::calc_Vrms_S( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vrms_S * m_RealyInc;
    val.mUnit = m_ChUnit;

    if(  (m_RawMeasData.PeriodX1 > 0)&&(m_RawMeasData.PeriodX2 > 0)
       &&(m_RawMeasData.Period > 0.0)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.PeriodX1);;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vrms_S);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.PeriodX2);;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( (m_RawMeasData.PeriodX1 <= 0)||(m_RawMeasData.PeriodX2 <= 0)
           ||(m_RawMeasData.Period <= 0.0) )
        {
            val.mStatus = NO_PERIOD;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Vrms_S] = val;
}

void measData::calc_Overshoot( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vover;   //! %
    val.mUnit = Unit_percent;

    if( m_RawMeasData.bVaild_Vpp
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.VoverY1);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = true;
        val.mBy.mPos = getYPos(m_RawMeasData.VoverY2);
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Overshoot] = val;
}

void measData::calc_Preshoot( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Vpre;  //! %
    val.mUnit = Unit_percent;

    if( m_RawMeasData.bVaild_Vpp
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( !m_RawMeasData.bVaild_Vpp)
        {
            val.mStatus = LOW_SIGNAL;
        }
        else
        {
            val.mStatus = INVALID;
        }
    }

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;

        val.mAy.mPos = getYPos(m_RawMeasData.VpreY1);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = true;

        val.mBy.mPos = getYPos(m_RawMeasData.VpreY2);
    }
    else
    {
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Preshoot] = val;
}

void measData::calc_Area( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;
    val.mVal = m_RawMeasData.Area * m_RealyInc * m_RealtInc;
    if( m_ChUnit == Unit_V)      val.mUnit = Unit_VmulS;
    else if( m_ChUnit == Unit_W) val.mUnit = Unit_WmulS;
    else if( m_ChUnit == Unit_A) val.mUnit = Unit_AmulS;
    else if( m_ChUnit == Unit_U) val.mUnit = Unit_UmulS;
    else                         val.mUnit = Unit_VmulS;

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mStatus = VALID;

        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(val.mVal);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        val.mStatus = NO_SIGNAL;
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Area] = val;
}

void measData::calc_Area_S( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;

    val.mVal = m_RawMeasData.Area_S * m_RealyInc * m_RealtInc;
    if( m_ChUnit == Unit_V)      val.mUnit = Unit_VmulS;
    else if( m_ChUnit == Unit_W) val.mUnit = Unit_WmulS;
    else if( m_ChUnit == Unit_A) val.mUnit = Unit_AmulS;
    else if( m_ChUnit == Unit_U) val.mUnit = Unit_UmulS;
    else                         val.mUnit = Unit_VmulS;

    if(  (m_RawMeasData.PeriodX1 > 0)&&(m_RawMeasData.PeriodX2 > 0)
       &&(m_RawMeasData.Period > 0.0)
       &&(m_MeasDataStatus == MeasData_Valid) )
    {
        val.mAx.bVisible = true;
        val.mAx.mPos = getXPos(m_RawMeasData.PeriodX1);
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(val.mVal);

        val.mBx.bVisible = true;
        val.mBx.mPos = getXPos(m_RawMeasData.PeriodX2);
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;

        val.mStatus = VALID;
    }
    else
    {
        if( m_MeasDataStatus == MeasData_Empty)
        {
            val.mStatus = NO_SIGNAL;
        }
        else if( (m_RawMeasData.PeriodX1 <= 0)||(m_RawMeasData.PeriodX2 <= 0)
                ||(m_RawMeasData.Period <= 0.0) )
        {
            val.mStatus = NO_PERIOD;
        }
        else
        {
            val.mStatus = INVALID;
        }
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Area_S] = val;
}

void measData::calc_Variance( int /*xPos*/, int /*yPos*/ )
{
    MeasValue val;
    val.mVal = m_RawMeasData.Vdev * m_RealyInc;
    if( m_ChUnit == Unit_V)      val.mUnit = Unit_V;
    else if( m_ChUnit == Unit_W) val.mUnit = Unit_W;
    else if( m_ChUnit == Unit_A) val.mUnit = Unit_A;
    else if( m_ChUnit == Unit_U) val.mUnit = Unit_U;
    else                         val.mUnit = Unit_V;

    if( m_MeasDataStatus == MeasData_Valid)
    {
        val.mStatus = VALID;

        val.mAx.bVisible = false;
        val.mAx.mPos = 0;
        val.mAy.bVisible = true;
        val.mAy.mPos = getYPos(m_RawMeasData.Vdev);

        val.mBx.bVisible = false;
        val.mBx.mPos = 0;
        val.mBy.bVisible = false;
        val.mBy.mPos = 0;
    }
    else
    {
        val.mStatus = NO_SIGNAL;
        setIndicatorVisible( val, false);
    }

    m_RealMeasData[Meas_Variance] = val;
}

