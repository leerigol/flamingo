#ifndef DSOLA_H
#define DSOLA_H
#include<QColor>
#include "../../include/dsotype.h"

using namespace DsoType;

class DsoLa
{
public:
    DsoLa(Chan id);

public:
    Chan  getChID();

//    DsoErr setOnOff( bool onoff);
//    bool   getOnOff();

//    DsoErr setColor(QColor color);
//    QColor getColor();

    DsoErr  setLabel(QString &label);
    QString getLabel();

    DsoErr setPosY(int posy);
    int    getPosY();

public:
    bool operator<( DsoLa * pItem );
    bool operator<( DsoLa & item );

private:
    Chan m_ChID;

private:
    bool     m_OnOff ;
    QColor   m_Color;

    QString  m_Label;

    int      m_PosY;
};
#endif // DSOLA_H

