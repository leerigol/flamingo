#include "servla.h"
#include "../../include/dsoassert.h"
#include "../service_msg.h"

#include "../../com/keyevent/rdsokey.h"
#include "../../service/servgui/servgui.h"
#include "../../include/dsoarith.h"

IMPLEMENT_CMD( servVert, servLa  )

start_of_entry()

on_set_int_bool(MSG_LA_ENABLE,   &servLa::onUserOnOff ),
on_set_int_bool(MSG_LA_ENABLE,   &servLa::setLaEn ),
on_get_bool(MSG_LA_ENABLE,       &servLa::getLaEn ),

on_set_int_bool(CMD_SERVICE_ENTER_ACTIVE,&servLa::on_enterActive),


on_set_int_bool(servVert::user_cmd_on_off,&servLa::onUserOnOff ),

on_set_int_int( MSG_LA_POS, &servLa::setCurPos ),
on_get_int( MSG_LA_POS,     &servLa::getCurPos ),

on_set_int_int(MSG_LA_CURRENT_CHAN, &servLa::setCurCh),
on_get_int    (MSG_LA_CURRENT_CHAN, &servLa::getCurCh),

on_set_int_int(MSG_LA_WAVE_SIZE,    &servLa::setWaveSize),
on_get_int    (MSG_LA_WAVE_SIZE,    &servLa::getWaveSize),

on_set_int_int(MSG_LA_GROUP1_SET,   &servLa::setGroup1Ch),
on_get_int    (MSG_LA_GROUP1_SET,   &servLa::getGroup1Ch),

on_set_int_int(MSG_LA_GROUP2_SET,   &servLa::setGroup2Ch),
on_get_int    (MSG_LA_GROUP2_SET,   &servLa::getGroup2Ch ),

on_set_int_int(MSG_LA_GROUP3_SET,   &servLa::setGroup3Ch),
on_get_int    (MSG_LA_GROUP3_SET,   &servLa::getGroup3Ch),

on_set_int_int(MSG_LA_GROUP4_SET,   &servLa::setGroup4Ch),
on_get_int    (MSG_LA_GROUP4_SET,   &servLa::getGroup4Ch),

on_set_int_int(MSG_LA_UN_GROUP,    &servLa::setUnChGroup),
on_get_int    (MSG_LA_UN_GROUP,    &servLa::getUnChGroup),

on_set_int_int( MSG_LA_HIGH_COLOR, &servLa::setHighColor ),
on_get_int( MSG_LA_HIGH_COLOR, &servLa::getHighColor ),

on_set_int_int( MSG_LA_EDGE_COLOR, &servLa::setEdgeColor ),
on_get_int( MSG_LA_EDGE_COLOR, &servLa::getEdgeColor ),

on_set_int_int( MSG_LA_LOW_COLOR, &servLa::setLowColor ),
on_get_int( MSG_LA_LOW_COLOR, &servLa::getLowColor ),

on_set_int_bool(MSG_LA_D0D7_ONOFF,  &servLa::setLWOnOff),
on_get_bool    (MSG_LA_D0D7_ONOFF,  &servLa::getLWOnOff),

on_set_int_bool(MSG_LA_D8D15_ONOFF, &servLa::setHWOnOff),
on_get_bool    (MSG_LA_D8D15_ONOFF, &servLa::getHWOnOff),

on_set_int_int(servLa::cmd_la_state,&servLa::setDxOnOff),
on_set_int_int(MSG_LA_SELECT_CHAN,  &servLa::setLaOnOff),
on_get_int    (MSG_LA_SELECT_CHAN,  &servLa::getDxOnOff),

on_set_int_int(MSG_LA_SELECT_GROUP, &servLa::setGroupOnOff),
on_get_int    (MSG_LA_SELECT_GROUP, &servLa::getGroupOnOff),

on_set_int_int(MSG_LA_HIGH_THRE_VAL, &servLa::setHThrevalue),
on_get_int(MSG_LA_HIGH_THRE_VAL, &servLa::getHThrevalue),

on_set_int_int(MSG_LA_LOW_THRE_VAL,  &servLa::setLThrevalue),
on_get_int(MSG_LA_LOW_THRE_VAL,  &servLa::getLThrevalue),

on_set_int_bool(MSG_LA_AUTO_SET,  &servLa::setAutoArrange),
on_get_bool    (MSG_LA_AUTO_SET,  &servLa::getAutoArrange),

on_set_int_bool(MSG_LA_LABEL_VIEW,  &servLa::setLabelView),
on_get_bool    (MSG_LA_LABEL_VIEW,  &servLa::getLabelView),

on_set_int_int(MSG_LA_LABEL_SELECT_CHAN, &servLa::setLabelDx),
on_get_int    (MSG_LA_LABEL_SELECT_CHAN, &servLa::getLabelDx),

on_set_int_int(MSG_LA_PRESET_LABEL,     &servLa::setPreLabel),
on_get_int    (MSG_LA_PRESET_LABEL,     &servLa::getPreLabel),

on_set_int_string(MSG_LA_INPUT_LABEL, &servLa::setLabel ),
on_get_string( MSG_LA_INPUT_LABEL, &servLa::getLabel ),

on_set_int_ll(MSG_LA_DELAY_CAL,     &servLa::setDelayCal ),
on_get_ll_attr(MSG_LA_DELAY_CAL,    &servLa::getDelayCal, &servLa::getDelayAttr ),

//! user cmd
on_set_void_void( servLa::cmd_group_changed, &servLa::on_group_changed ),

on_set_void_void( servLa::cmd_dx_changed, &servLa::on_dx_changed ),
on_set_void_void( servLa::cmd_dx_changed, &servLa::on_group_changed ),
on_set_void_void( servLa::cmd_opt_changed, &servLa::on_opt_changed ),

on_set_int_int2( servLa::cmd_dx_pos, &servLa::setDxPos ),

on_set_int_int( servLa::cmd_has_la, &servLa::setHasLa ),
on_get_int( servLa::cmd_has_la,     &servLa::getHasLa ),

on_set_void_void( EVT_TIME_MODE_CHANGED, &servLa::on_hori_mode ),

//!scpi cmd
on_set_int_arg( servLa::cmd_dx_display,             &servLa::setDxDisplay ),
on_get_void_argi_argo( servLa::cmd_dx_display,      &servLa::getDxDisplay ),
on_set_int_arg( servLa::cmd_digital_display,        &servLa::setDigDisplay ),
on_get_void_argi_argo( servLa::cmd_digital_display, &servLa::getDigDisplay ),
on_set_int_arg( servLa::cmd_digital_position,       &servLa::setDigPosition ),
on_get_void_argi_argo( servLa::cmd_digital_position,&servLa::getDigPosition ),
on_set_int_arg( servLa::cmd_digital_label,          &servLa::setDigLabel ),
on_get_void_argi_argo( servLa::cmd_digital_label,   &servLa::getDiglabel ),
on_set_int_arg( servLa::cmd_group_append,           &servLa::setGroupAppend ),

on_set_int_int(servLa::cmd_la_high_thre_val,        &servLa::setDigHThrevalue),
on_get_int(servLa::cmd_la_high_thre_val,            &servLa::getDigHThrevalue),
on_set_int_int(servLa::cmd_la_low_thre_val,         &servLa::setDigLThrevalue),
on_get_int(servLa::cmd_la_low_thre_val,             &servLa::getDigLThrevalue),

on_set_int_int(servLa::cmd_la_unGroup,              &servLa::setLaUnChGroup),
on_get_int    (servLa::cmd_la_unGroup,              &servLa::getLaUnChGroup),

//! system msg
on_set_void_void( CMD_SERVICE_RST,     &servLa::rst ),
on_set_int_void ( CMD_SERVICE_STARTUP, &servLa::startup ),
on_set_void_void( CMD_SERVICE_INIT,    &servLa::on_init ),
on_set_void_int ( CMD_SERVICE_SOFT_INT,&servLa::on_soft_int ),

end_of_entry()

QString servLa::_presetLabel[] = {"ACK","ADO","ADDR","BIT",
                              "CAS","CLK","CS", "DATA",
                              "HALT","INT","LOAD","NMI",
                              "OUT","RAS","PIN","RDY",
                              "RST","RX","TX","WR",
                              "MISO","MOSI","D0","D1",
                               "D2","D3","D4","D5","D6",
                               "D7","D8","D9","D10","D11",
                                "D12","D13","D14","D15"
 };
static quint32 _colorTable[]=
{
    0x008000,
    0xffffff,
    0x00ff00,
};


servLa::servLa(QString name, ServiceId id):
                 servVert( name, id ),
                 dsoVert( la )
{
    serviceExecutor::baseInit();

    DsoLa *pLa;
    for(int i = 0;i<LA_NUMBER;i++)
    {
        pLa = new DsoLa((Chan)((int)d0+i));
        Q_ASSERT( NULL != pLa );
        mDxs.append(  pLa );
    }

    mVersion = 5;

    dsoVert::setServId( mpItem->servId );
    dsoVert::yColor = Qt::green;

}

servLa::~servLa()
{
    foreach( DsoLa *pLa, mDxs )
    {
        Q_ASSERT( NULL != pLa );

        delete pLa;
    }
}

void servLa::registerSpy()
{
    int groupMsgs[]=
    {   MSG_LA_GROUP1_SET,
        MSG_LA_GROUP2_SET,
        MSG_LA_GROUP3_SET,
        MSG_LA_GROUP4_SET,
        MSG_LA_UN_GROUP, };
    for ( int i = 0; i < array_count(groupMsgs); i++ )
    {
        spyOn( mpItem->servId, groupMsgs[i],
               mpItem->servId, servLa::cmd_group_changed );
    }

    int dxMsgs[] =
    {
      servVert::user_cmd_on_off,
      MSG_LA_SELECT_CHAN,
      MSG_LA_SELECT_GROUP,
      MSG_LA_ENABLE,

      MSG_LA_CURRENT_CHAN,
      MSG_LA_POS,
      MSG_LA_AUTO_SET,
      MSG_LA_WAVE_SIZE,

      MSG_LA_UN_GROUP,

      servLa::cmd_dx_pos,
      servLa::cmd_la_state
    };

    for ( int i = 0; i < array_count(dxMsgs); i++ )
    {
        spyOn( mpItem->servId, dxMsgs[i],
               mpItem->servId, servLa::cmd_dx_changed );
    }

    //! [dba+ 2018-03-23]
    //! option
    spyOn( serv_name_license, servLicense::cmd_Opt_Exp,     servLa::cmd_opt_changed );
    spyOn( serv_name_license, servLicense::cmd_Opt_Invalid, servLa::cmd_opt_changed );
    spyOn( serv_name_license, servLicense::cmd_Opt_Active,  servLa::cmd_opt_changed );

    //! hori mode
    spyOn( serv_name_hori, EVT_TIME_MODE_CHANGED );
}

#define ar ar_out
#define ar_pack_e ar_out
int  servLa::serialOut(CStream &stream,
                       serialVersion &ver)
{
    ISerial::serialOut(stream, ver );

    ar( "on", dsoVert::yOnOff );
    ar_pack_e( "cur", mCurChan );

    ar( "gp1", mGroups[0] );
    ar( "gp2", mGroups[1] );
    ar( "gp3", mGroups[2] );
    ar( "gp4", mGroups[3] );

    ar( "dxon", mDxOnOff );
    ar( "gpon", mGpOnOff );
    ar_pack_e( "size", m_WaveSize );

    ar( "lthre", m_LThrevalue );
    ar( "hthre", m_HThrevalue );

    ar( "arrange", m_AutoArrange );

    ar( "delay", m_DelayCal );

    ar( "label", m_LabelView );
    ar( "label_dx", mLabelDx );

    //! pos && label
    int pos;
    QString str;
    QString val;
    for ( int i = 0; i < LA_NUMBER; i++ )
    {
        pos = mDxs[i]->getPosY();

        str = QString("p%1").arg(i);
        //val = QString("%1").arg(pos);
        stream.write( str, pos );

        str = QString("l%1").arg(i);
        stream.write( str, mDxs[i]->getLabel() );
    }    

    //! colors
    ar_pack_e( "h_c", mHighColor );
    ar_pack_e( "e_c", mEdgeColor );
    ar_pack_e( "l_c", mLowColor );

    return ERR_NONE;
}

#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int  servLa::serialIn(CStream &stream,
                      serialVersion ver)
{
    if( ver == mVersion)
    {
        ar( "on", dsoVert::yOnOff );
        ar_pack_e( "cur", mCurChan );

        ar( "gp1", mGroups[0] );
        ar( "gp2", mGroups[1] );
        ar( "gp3", mGroups[2] );
        ar( "gp4", mGroups[3] );

        ar( "dxon", mDxOnOff );
        ar( "gpon", mGpOnOff );
        ar_pack_e( "size", m_WaveSize );

        ar( "lthre", m_LThrevalue );
        ar( "hthre", m_HThrevalue );

        ar( "arrange", m_AutoArrange );

        ar( "delay", m_DelayCal );

        ar( "label", m_LabelView );
        ar( "label_dx", mLabelDx );

        //! pos && label
        int pos;
        QString str, label,val;
        for ( int i = 0; i < LA_NUMBER; i++ )
        {
            str = QString("p%1").arg(i);
            stream.read( str, pos );
            //pos = val.toInt();

            str = QString("l%1").arg(i);
            stream.read( str, label );

            mDxs[i]->setPosY( pos );
            mDxs[i]->setLabel( label );
        }

        //! colors
        ar_pack_e( "h_c", mHighColor );
        ar_pack_e( "e_c", mEdgeColor );
        ar_pack_e( "l_c", mLowColor );
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

void servLa::rst()
{
    dsoVert::yOnOff = false;

    mCurChan = chan_none;
    for ( int i = 0; i < array_count(mGroups); i++ )
    { mGroups[i] = 0; }

    mDxOnOff = LW_1 | HW_1 | (1<<all_bit);
    mGpOnOff = 0;

    m_WaveSize    = Medium;

    m_HThrevalue  = 1400000;
    m_LThrevalue  = 1400000;

    m_AutoArrange = true;

    m_DelayCal = 0;

    m_LabelView = false;
    mLabelDx = d0;

    for(int i=0;i<LA_NUMBER;i++)
    {
        mDxs[i]->setPosY(i);
    }

    mHighColor = la_color_little_green;
    mEdgeColor = la_color_white;
    mLowColor = la_color_green;
}

int servLa::startup()
{
    defer( servLa::cmd_group_changed );
    defer( servLa::cmd_dx_changed );

    updateAllUi();

    //! config
//    setLaEn( dsoVert::yOnOff );

    bool bAvilable = sysHasLA();

    onUserOnOff( dsoVert::yOnOff & bAvilable );

    setLThrevalue( m_LThrevalue );
    setHThrevalue( m_HThrevalue );

    setWaveSize( m_WaveSize );

    setHighColor( mHighColor );
    setEdgeColor( mEdgeColor);
    setLowColor( mLowColor );

    //notify UI to SHOW STATUS
    async(servLa::cmd_has_la, bAvilable);

    return ERR_NONE;
}

DsoErr servLa::setLaEn( bool b )
{
    if ( b )
    {
        if ( serviceExecutor::_workMode == work_help )//qxl help
        {
            return ERR_NONE;
        }

        async( CMD_SERVICE_ACTIVE, 1 );
        dsoVert::yOnOff = b;
    }
    else
    {
        if ( isActive() && sysGetMenuWindow()->isVisible())
        {
            dsoVert::yOnOff = b;
            async( CMD_SERVICE_DEACTIVE, 0 );
        }
        else
        {
            async( CMD_SERVICE_ACTIVE, 1 );
        }
    }

    return ERR_NONE;
}
bool servLa::getLaEn()
{
    return dsoVert::yOnOff;
}

DsoErr servLa::on_enterActive(int id)
{
    if( id == E_SERVICE_ID_LA )
    {
        async(MSG_LA_ENABLE,true);
    }
    return ERR_NONE;
}

DsoErr servLa::onUserOnOff( bool b )
{
    if ( serviceExecutor::_workMode == work_help )//qxl help
    {
        return ERR_NONE;
    }
    dsoVert::yOnOff = b;
//    if( b && mDxOnOff == 0 )//for bug1810 by hxh
//    {
//        mDxOnOff = LW_1 | HW_1 | (1<<all_bit);
//    }

    return ERR_NONE;
}

//! dx + gp + none
DsoErr  servLa::setCurCh(Chan ch)
{
    mCurChan = ch;

    return ERR_NONE;
}
int  servLa::getCurCh()
{
    return mCurChan;
}

DsoErr servLa::setCurPos( int pos )
{
    DsoErr err = ERR_NONE;

    if ( mCurChan == chan_none )
    {
        return ERR_INVALID_CONFIG;
    }
    else if ( mCurChan >= d0 && mCurChan <= d15 )
    {
        DsoLa *pCurItem;
        int dist;

        int maxPos = getMaxPos( m_WaveSize );
        /*err = */
        checkRange( pos, 0, maxPos );

        //! convert to dist
        pCurItem = mDxs[ mCurChan - d0 ];
        dist = pos - pCurItem->getPosY();

        //! todo refresh the pos
        updatePos( pCurItem, dist );
    }
    else if ( mCurChan >= la_g1 && mCurChan <= la_g4 )
    {
        /*bool bChange;

        bChange = */

        updateGpPos( mCurChan, pos );

//        if ( bChange )
//        { err = ERR_NONE; }
//        else
//        {
//            err = pos > 0 ? ERR_OVER_UPPER_RANGE : ERR_OVER_LOW_RANGE;
//        }
    }
    else
    {
        return ERR_INVALID_CONFIG;
    }

    return err;
}
int servLa::getCurPos()
{
    //! max pos
    if ( mCurChan == chan_none )
    {
        return 0;
    }
    else if ( mCurChan >= d0 && mCurChan <= d15 )
    {
        return mDxs[ mCurChan - d0 ]->getPosY();
    }
    //! group has no pos
    else if ( mCurChan >= la_g1 && mCurChan <= la_g4 )
    {
        return 0;
    }
    else
    {}

    return 0;
}

DsoErr  servLa::setWaveSize(LaScale lascale )
{
    //! check input
    if ( lascale >= Small && lascale <= Large )
    {}
    else
    { return ERR_INVALID_INPUT; }

    //! rearrange each pos
    if ( m_WaveSize != lascale )
    {
        reArrange( getMaxPos( lascale ) );

        m_WaveSize = lascale;

        LOG_DBG()<<m_WaveSize;
    }

    //! set max pos
    dsoVert::setyMaxPos( getMaxPos( m_WaveSize ) );

    return ERR_NONE;
}
LaScale servLa::getWaveSize()
{
    mUiAttr.setZVal( MSG_LA_WAVE_SIZE, Medium );

    return m_WaveSize;
}

DsoErr  servLa::setGroup1Ch(quint32 value)
{
    mGroups[0] = value;

    return ERR_NONE;
}
quint32  servLa::getGroup1Ch()
{
    return mGroups[0];
}

DsoErr  servLa::setGroup2Ch(quint32 value)
{
    mGroups[1] = value;

    return ERR_NONE;
}
quint32  servLa::getGroup2Ch()
{
    return mGroups[1];
}

DsoErr  servLa::setGroup3Ch(quint32 value)
{
    mGroups[2] = value;

    return ERR_NONE;
}
quint32  servLa::getGroup3Ch()
{
    return mGroups[2];
}

DsoErr  servLa::setGroup4Ch(quint32 value)
{
    mGroups[3] = value;

    return ERR_NONE;
}
quint32  servLa::getGroup4Ch()
{
    return mGroups[3];
}

quint32  servLa::getGroupx( Chan gp )
{
    Q_ASSERT( gp >= la_g1 && gp <= la_g4 );

    return mGroups[ gp - la_g1 ];
}

DsoErr  servLa::setUnChGroup(quint32 value)
{
    for ( int i = 0; i < array_count(mGroups); i++ )
    {
        //! keep group
        if ( value & GP_1(i) )
        {}
        //! deGroup
        else
        { mGroups[i] = 0; }
    }

    return ERR_NONE;
}

quint32 servLa::getUnChGroup()
{
    //! check en
    quint32 gpMask = 0;
    for ( int i = 0; i < array_count(mGroups); i++ )
    {
        if ( mGroups[i] != 0 )
        {
            set_bit( gpMask, INDEX_TO_GP( i ) );
        }
        else
        {
        }
    }

    return gpMask;
}

DsoErr servLa::setLaOnOff(quint32 value)
{
    quint32 changedMask = mDxOnOff ^ value;
    if ( get_bit(changedMask, all_bit) )
    {
        if ( get_bit(value, all_bit ) )
        {
            value = 0x1ffff << all_bit;
        }
        else
        {
            value = value & 0xf;
        }
    }

    //async(servLa::cmd_la_state, (int)value);
    setDxOnOff( value );
    return ERR_NONE;
}

DsoErr servLa::setDxOnOff( quint32 value)
{
    quint32 preDx, rawDx;
    quint32 changedMask;

    preDx = mDxOnOff;
    rawDx = trim_raw_dx( preDx );

    changedMask = preDx ^ value;
    if ( get_bit(changedMask, all_bit) )
    {
        if ( get_bit(value, all_bit ) )
        {
            mDxOnOff = 0x1ffff << all_bit;
        }
        else
        {
            mDxOnOff = value;
        }
    }
    else
    {
        if ( (value >> (all_bit+1) ) != 0xffff )
        {
            mDxOnOff = trim_raw_dx(value);
        }
        else
        {
            mDxOnOff = (0x1ffff<<all_bit);
        }
    }

    //! check pos count
    rawDx = trim_raw_dx( mDxOnOff );
    int sumNext = sum1b( rawDx );

    //! to change the size
    if ( (getMaxPos(m_WaveSize )+1) < sumNext )
    {
        LOG_DBG()<<sumNext;

        ssync( MSG_LA_WAVE_SIZE, Medium );

        //! pop info
        servGui::showInfo( IDS_SIZE_CHANGED );
    }
    else
    {}

    //! the new items
    rawDx = trim_raw_dx( ( (mDxOnOff ^ preDx ) & mDxOnOff ) );
    collidePos( trim_raw_dx( preDx), rawDx );

    return ERR_NONE;
}

quint32  servLa::getDxOnOff()
{
    return mDxOnOff;
}

DsoErr servLa::collidePos( quint32 usedDx, quint32 newDx )
{
    QList< int > usedPos;

    int maxPos = getMaxPos( m_WaveSize );

    //! raw pos
    collectUsedPos( usedDx, usedPos );
    qSort( usedPos.begin(), usedPos.end() );

    int pos;
    QList<int> myUsedPos;
    for ( int i = d0; i <= d15; i++ )
    {
        //! enabled
        if ( get_bit(newDx,i) )
        {
            int tryPos = mDxs[i-d0]->getPosY();
            if( !myUsedPos.contains( tryPos ) && !usedPos.contains(tryPos))
            {
                myUsedPos.append( tryPos );
            }
            else
            {
                tryPos = -1;
            }
            pos = searchPos( usedPos, tryPos, maxPos );
            if ( pos >= 0 )
            {}
            else
            {
                LOG_DBG()<<i<<pos<<newDx;
                //Q_ASSERT( false );
                pos = 0;
            }

            mDxs[i-d0]->setPosY( pos );
        }
    }

    return ERR_NONE;
}

int servLa::searchPos( QList< int > &usedPos, int pos, int maxPos )
{
    if ( usedPos.contains(pos) )
    { return pos; }

    for ( int i = 0; i <= maxPos; i++ )
    {
        if ( !usedPos.contains(i) )
        {
            usedPos.append( i );
            qSort( usedPos.begin(), usedPos.end() );
            return i;
        }
    }

    //! fail
    return 0;
}

DsoErr  servLa::setGroupOnOff( quint32 value)
{
    quint32 changeGp;

    //! changed mask
    changeGp = mGpOnOff ^ value;

    mGpOnOff = value;

    //! cascade dx on off
    quint32 dxOnOff;
    dxOnOff = mDxOnOff;
    for ( int i = la_g1; i <= la_g4; i++ )
    {
        //! change bit
        if ( get_bit(changeGp, i) )
        {
            //! gp on
            if ( get_bit(mGpOnOff, i ) )
            {
                set_attr( quint32, dxOnOff, mGroups[ i - la_g1 ] );
            }
            //! gp off
            else
            {
                unset_attr( quint32, dxOnOff, mGroups[ i - la_g1 ] );
            }
        }
    }

    setDxOnOff( dxOnOff );
//    async( MSG_LA_SELECT_CHAN, (int)dxOnOff );

    return ERR_NONE;
}

quint32  servLa::getGroupOnOff()
{
    return mGpOnOff;
}

DsoErr servLa::setLWOnOff(bool value)
{
    quint32 dxOnOff;

    dxOnOff = mDxOnOff;

    //! low word
    if ( value )
    {
        dxOnOff |= LW_1;
    }
    else
    {
        dxOnOff &= LW_0;
    }

    async( MSG_LA_SELECT_CHAN, (int)dxOnOff );

    return ERR_NONE;
}

bool servLa::getLWOnOff()
{
    if( getLaEn() ) //for bug1893 by hxh
    {
        return ( mDxOnOff & LW_1 ) == LW_1;
    }
    return false;
}

DsoErr servLa::setHWOnOff(bool value)
{
    quint32 dxOnOff;

    dxOnOff = mDxOnOff;

    //! high word
    if ( value )
    {
        dxOnOff |= HW_1;
    }
    else
    {
        dxOnOff &= HW_0;
    }

    async( MSG_LA_SELECT_CHAN, (int)dxOnOff );

    return ERR_NONE;
}

bool servLa::getHWOnOff()
{
    if( getLaEn() ) //for bug1893 by hxh
    {
        return (mDxOnOff & HW_1 ) == HW_1;
    }
    return false;
}

#define align_threshold( value )  (value)// + LA_THRESHOLD_STEP - 1 )/LA_THRESHOLD_STEP*LA_THRESHOLD_STEP
DsoErr servLa::setHThrevalue(int value)
{
#if 0
    //! align
    m_HThrevalue = align_threshold( value );
    return postEngine( ENGINE_LA_H_THRESHOLD, m_HThrevalue );
#else
    m_HThrevalue = value;
    return postEngine( ENGINE_LA_H_THRESHOLD, align_threshold( m_HThrevalue ) );
#endif
}
int servLa::getHThrevalue()
{
    setuiRange( MIN_LA_THRESHOLD, MAX_LA_THRESHOLD, DEF_LA_THRESHOLD );
    setuiBase( 1, E_N6 );
    setuiStep( getVoltStep(m_HThrevalue) );

    setuiUnit( Unit_V );
    return m_HThrevalue;
}

DsoErr servLa::setLThrevalue(int value)
{
#if 0
    m_LThrevalue = align_threshold( value );;

    return postEngine( ENGINE_LA_L_THRESHOLD, m_LThrevalue );
#else
    m_LThrevalue = value;
    return postEngine( ENGINE_LA_L_THRESHOLD, align_threshold( m_LThrevalue ) );
#endif
}
int servLa::getLThrevalue()
{
    setuiRange( MIN_LA_THRESHOLD, MAX_LA_THRESHOLD, DEF_LA_THRESHOLD );
    setuiBase( 1, E_N6 );
    setuiStep( getVoltStep(m_LThrevalue) );

    setuiUnit( Unit_V );
    return m_LThrevalue;
}

DsoErr  servLa::setAutoArrange(bool value)
{
    m_AutoArrange = value;

    //! max pos
    int maxPos = getMaxPos( m_WaveSize );

    //! special small
    //if ( m_WaveSize == Small )
    {
        int h,l;
        l = 0;
        h = 0;
        for ( int i = d0; i <= d15; i++ )
        {
            if ( get_bit(mDxOnOff, i ) )
            {
                if ( mDxs[i - d0 ]->getPosY() <= maxPos/2  )
                { l++; }
                else
                { h++; }
            }
        }

        //! align to bottom
        if ( l >= h )
        {
            //! D15-D0
            if( value )
            {
                arrange( d0, d15, 1, 0, 1, 0, maxPos );
            }
            else
            {
                arrange( d15, d0, -1, 0, 1, 0, maxPos );
            }
        }
        else
        {
            //! D15-D0
            if( value )
            {
                arrange( d15, d0, -1, maxPos, -1, 0, maxPos );
            }
            else
            {
                arrange( d0, d15, 1, maxPos, -1, 0, maxPos );
            }
        }

        return ERR_NONE;
    }

//    //! D15-D0
//    if( value )
//    {
//        arrange( d0, d15, 1, 0, 1, 0, maxPos );
//    }
//    //! D0-D15
//    else
//    {
//        arrange( d15, d0, -1, 0, 1, 0, maxPos );
//    }

//    return ERR_NONE;
}
bool servLa::getAutoArrange()
{
    return m_AutoArrange;
}

void servLa::arrange( int dxFrom,
                      int dxTo,
                      int dxStep,
                      int posFrom,
                      int posStep,
                      int minPos,
                      int maxPos )
{
    int pos;

    //! for the opened dx
    int dx = dxFrom;
    pos = posFrom;
    do
    {
        if ( get_bit(mDxOnOff, dx ) )
        {
            mDxs[dx - d0 ]->setPosY( pos );
            pos+=posStep;

            if ( pos < minPos ) { pos = minPos; }
            else if ( pos > maxPos ) { pos = maxPos; }
            else {}
        }
        dx += dxStep;
    }while( dx != (dxTo+dxStep) );

    //! for the closed dx
    dx = dxFrom;
    do
    {
        if ( !get_bit(mDxOnOff, dx ) )
        {
            mDxs[dx - d0 ]->setPosY( pos );
            pos+=posStep;

            if ( pos < minPos ) { pos = minPos; }
            else if ( pos > maxPos ) { pos = maxPos; }
            else {}
        }
        dx += dxStep;
    }while( dx != (dxTo+dxStep) );
}

DsoErr servLa::setLabelView( bool b )
{
    m_LabelView = b;
    return ERR_NONE;
}
bool servLa::getLabelView()
{ return m_LabelView; }

DsoErr servLa::setLabelDx( int dx )
{
    mLabelDx = dx;

    setuiChange( MSG_LA_INPUT_LABEL );

    return ERR_NONE;
}
int servLa::getLabelDx()
{ return mLabelDx;}

DsoErr servLa::setPreLabel(int value)
{
    if ( value < 0 || value >= array_count(_presetLabel) )
    { return ERR_INVALID_INPUT; }

    async( MSG_LA_INPUT_LABEL, _presetLabel[value] );

    return ERR_NONE;
}

int servLa::getPreLabel()
{ return 0; }

DsoErr servLa::setLabel( QString &str )
{
    mDxs[ DX_TO_INDEX(mLabelDx) ]->setLabel( str );

    return ERR_NONE;
}
QString servLa::getLabel()
{
    mUiAttr.setMaxLength( MSG_LA_INPUT_LABEL, max_label_length );

    return mDxs[ DX_TO_INDEX(mLabelDx) ]->getLabel();
}

DsoErr servLa::setDelayCal(qlonglong delay)
{
    m_DelayCal  = delay;
    return ERR_NONE;
}
qlonglong  servLa::getDelayCal()
{
    return m_DelayCal;
}

void servLa::getDelayAttr( enumAttr /*subAttr*/ )
{

#if 1 //for bug1873 by hxh
    setuiBase(1,E_N12);
    setuiMaxVal(time_ns(100));
    setuiMinVal(time_ns(-100));
    setuiZVal(time_ns(0));
    setuiUnit( Unit_s );
    setuiStep( time_ns(10) );
#else
    setuiBase( 1, E_N12 );
    setuiUnit( Unit_s );

    //! -100ns ~ 100ns
    setuiRange( -time_ns(100), time_ns(100), 0ll );

    //! 1/100 ceil(scale)
    qlonglong tScale;
    horiAttr hAttr = sysGetHoriAttr( chan1, horizontal_view_active );
    tScale = hAttr.tInc * adc_hdiv_dots;

    roof125( tScale, tScale );
    setuiStep( tScale/adc_hdiv_dots );
#endif
}

DsoErr servLa::setHighColor( laColor color )
{
    mHighColor = color;

    postEngine( ENGINE_LA_COLOR, 0, (int)_colorTable[ color - la_color_little_green ] );

    return ERR_NONE;
}
servLa::laColor servLa::getHighColor()
{ return mHighColor; }

DsoErr servLa::setEdgeColor( laColor color )
{
    mEdgeColor = color;

    postEngine( ENGINE_LA_COLOR, 1, (int)_colorTable[ color - la_color_little_green ] );

    return ERR_NONE;
}
servLa::laColor servLa::getEdgeColor()
{ return mEdgeColor; }

DsoErr servLa::setLowColor( laColor color )
{
    mLowColor = color;

    postEngine( ENGINE_LA_COLOR, 2, (int)_colorTable[ color - la_color_little_green ] );

    return ERR_NONE;
}
servLa::laColor servLa::getLowColor()
{ return mLowColor; }

int servLa::getMaxPos( LaScale size )
{
    int maxPos;
    if ( size == Small )
    { maxPos = 31; }
    else if ( size == Medium )
    { maxPos = 15; }
    else if ( size == Large )
    { maxPos = 7; }
    else
    { maxPos = 15; }

    return maxPos;
}

DsoLa* servLa::getDx( Chan la )
{
    Q_ASSERT( la >= d0 && la <= d15 );

    return mDxs[ la- d0 ];
}

DsoErr servLa::setDxPos( Chan dx, int pos )
{
    //! check range
    if ( dx >= d0 && dx <= d15 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    //! dx opened
    if ( get_bit(mDxOnOff, dx ) )
    {}
    else
    { return ERR_INVALID_CONFIG; }

    //! change pos
    DsoErr err = ERR_NONE;
    err = checkRange( pos, 0, getyMaxPos() );

    //! convert to dist
    DsoLa *pCurItem;
    pCurItem = mDxs[ dx - d0 ];
    int dist = pos - pCurItem->getPosY();

    //! todo refresh the pos
    updatePos( pCurItem, dist );

    return err;
}

DsoErr servLa::setHasLa()
{
    return ERR_NONE;
}

int servLa::getHasLa()
{
    return sysHasLA();
}

void servLa::on_init()
{
    //! option enable
    mbOptEn = true;
    setUiEnables(mbOptEn);
    setEnable( false, IDS_LA_OPTION_MISSING );

    //! pos
    mServKeyMap.append( encode_inc(R_Pkey_WAVE_POS),
                      MSG_LA_POS,
                      R_Skey_TuneInc );

    mServKeyMap.append( encode_dec(R_Pkey_WAVE_POS),
                      MSG_LA_POS,
                      R_Skey_TuneDec );

    mServKeyMap.append( (R_Pkey_WAVE_POS_Z),
                      MSG_LA_CURRENT_CHAN,
                      R_Skey_Next );
    //! scale
    mServKeyMap.append( encode_inc(R_Pkey_WAVE_VOLT),
                      MSG_LA_WAVE_SIZE,
                      R_Skey_TuneInc );

    mServKeyMap.append( encode_dec(R_Pkey_WAVE_VOLT),
                      MSG_LA_WAVE_SIZE,
                      R_Skey_TuneDec );

    mServKeyMap.append( (R_Pkey_WAVE_VOLT_Z),
                      MSG_LA_WAVE_SIZE,
                      R_Skey_Zval );

    //! link changes
    linkChange( servLa::cmd_dx_changed,servLa::cmd_la_state,
                MSG_LA_D0D7_ONOFF, MSG_LA_D8D15_ONOFF,
                MSG_LA_SELECT_GROUP, MSG_LA_SELECT_CHAN );

    //no Channel delay func temporarily, bug 2270 qxl 1.5.5
    mUiAttr.setVisible(MSG_LA_DELAY_CAL, false);

}


bool servLa::keyTranslate(int key,
               int &msg,
               int &controlAction)
{
    if( !this->isActive() )
    {
        return false;
    }


    serviceKey servKey;
    if ( mServKeyMap.find( key, servKey) )
    {
        msg = servKey.mMsg;
        controlAction = servKey.mAction;
        return true;
    }
    return false;
}
void servLa::on_soft_int( quint32 ival )
{
    //! la in
    if ( get_bit(ival, 0) )
    {
        servGui::showInfo( IDS_LA_PROBE_CONNECT );
    }
    else
    //! la off
    {
        servGui::showInfo( IDS_LA_PROBE_DISCONNECT );
    }
}

void servLa::on_group_changed()
{
    //! group set
    groupAttrSet( MSG_LA_GROUP1_SET, mGroups[0] );
    groupAttrSet( MSG_LA_GROUP2_SET, mGroups[1] );
    groupAttrSet( MSG_LA_GROUP3_SET, mGroups[2] );
    groupAttrSet( MSG_LA_GROUP4_SET, mGroups[3] );

    //! un group set
    unGroupAttrSet();

    //! group on/off
    int gpIndex;
    for( int i = la_g1; i <= la_g4; i++ )
    {
        gpIndex = GP_TO_INDEX( i );
        //! group has la
        mUiAttr.setVisible( MSG_LA_SELECT_GROUP, i , mGroups[ gpIndex ] != 0 );

        //! no group
        if ( mGroups[ gpIndex ] == 0  )
        {
            if ( get_bit(mGpOnOff, (i) ) )
            {
                unset_bit( mGpOnOff, i );

                setuiChange( MSG_LA_SELECT_GROUP );
            }
        }
        else
        {
            //! group chan changed
            if( ( trim_raw_dx( mGroups[ gpIndex] & mDxOnOff) )
                    != trim_raw_dx( mGroups[gpIndex] ) )
            {
                unset_bit( mGpOnOff, i );

                setuiChange( MSG_LA_SELECT_GROUP );
            }
        }
    }

    bool bVisible;
    //! group visible
    for( int i = la_g1; i <= la_g4; i++ )
    {
        bVisible =  ( get_bit( mGpOnOff, i ) );

        mUiAttr.setVisible( MSG_LA_CURRENT_CHAN, (i), bVisible );

    }

    //! gp conflict
    if ( mCurChan >= la_g1 && mCurChan <= la_g4 )
    {
        if ( get_bit( mGpOnOff, (mCurChan) ) )
        {}
        else
        {
            async( MSG_LA_CURRENT_CHAN, findNextDx() );

            servGui::showInfo( IDS_SELECT_CHANGED );
        }
    }
}

void servLa::on_dx_changed()
{
    bool bVisible;
    int openCnt;

    openCnt = 0;
    //! dx visible
    for ( int i = d0; i <= d15; i++ )
    {
        bVisible = ( get_bit( mDxOnOff, i ) );

        mUiAttr.setVisible( MSG_LA_CURRENT_CHAN, i, bVisible );

        openCnt += bVisible;
    }

    //! led proc
    bool bLed;
    bLed = (openCnt > 0) && getOnOff();
    syncEngine( bLed ? ENGINE_LED_ON : ENGINE_LED_OFF,
                DsoEngine::led_digital );

    //! current chan valid
    if ( mCurChan >= d0 && mCurChan <=d15 )
    {
        if ( get_bit(mDxOnOff, mCurChan) )
        {}
        else
        {
            async( MSG_LA_CURRENT_CHAN, findNextDx() );

            servGui::showInfo( IDS_SELECT_CHANGED );
        }
    }

    //! wave size changed
    mUiAttr.setEnable( MSG_LA_WAVE_SIZE, Large, openCnt <= 8 );

    //! change the size
    if ( m_WaveSize == Large && openCnt > 8 )
    {
        ssync( MSG_LA_WAVE_SIZE, Medium );

        servGui::showInfo( IDS_SIZE_CHANGED );
    }

    //! verify the pos
    if ( validatePos( getMaxPos(m_WaveSize) ) )
    {}
    else
    {
        reArrange( getMaxPos(m_WaveSize) );
        servGui::showInfo( IDS_POS_CHANGED );
    }

    //! group changed
    for( int i = la_g1; i <= la_g4; i++ )
    {
        //! gp on
        if ( get_bit(mGpOnOff, i) )
        {
            if ( is_attr( mDxOnOff, mGroups[i - la_g1] ) )
            {}
            else
            {
                unset_bit( mGpOnOff, i );
                setuiChange( MSG_LA_SELECT_GROUP );
            }
        }
    }

    //! engine config
    engineCfg();
}

void servLa::on_opt_changed()
{
#if 0
    bool bNow;

    bNow = sysCheckLicense(OPT_MSO);
    LOG_DBG()<<bNow;
    //! changed
    if ( mbOptEn != bNow )
    {
        //! dis->en
        if ( bNow )
        {
            setUiEnables(true);
            setEnable( true );
            LOG_DBG();
        }
        //! en->dis
        else
        {
            setUiEnables(false);
            setEnable( false, IDS_LA_OPTION_MISSING );
            LOG_DBG();

            //! disable now
            if ( getLaEn() )
            {
                async( MSG_LA_ENABLE, false );

                servGui::showInfo( IDS_LA_OPTION_EXPIRED );
                LOG_DBG();
            }
        }

        mbOptEn = bNow;
    }
    else
    { LOG_DBG(); }
#else
    //! [dba+ 2018-03-23]
    sysScpiFilterServive(getName(), (!sysHasLA()) );
#endif
}

void servLa::on_hori_mode( )
{
    int mode;
    serviceExecutor::query( serv_name_hori, MSG_HOR_TIME_MODE,  mode);
    if ( mode == Acquire_XY )
    {
        setEnable(false, IDS_LA_DISABLE_IN_XY);
    }
    else
    {
        setEnable(true);
    }
}

int  servLa::collectActiveMask()
{
    int activeMask = 0;
    if ( mCurChan >= d0 && mCurChan <= d15 )
    {
        set_bit( activeMask, mCurChan );
        return activeMask;
    }
    else if ( mCurChan >= la_g1 && mCurChan <= la_g4 )
    {
        return getGroupx( mCurChan );
    }
    else
    { return 0; }
}
void servLa::collectDxPos( int pos[16] )
{
    int i = 0;
    foreach( DsoLa *pLa, mDxs )
    {
        Q_ASSERT( NULL != pLa );

        pos[ i++ ] = pLa->getPosY();
    }
}
void servLa::collectUsedPos( quint32 usedDx, QList<int> &usedPos )
{
    for ( int i = d0; i <= d15; i++ )
    {
        if ( get_bit(usedDx, i) )
        {
            usedPos.append( mDxs[i-d0]->getPosY() );
        }
    }
}
void servLa::engineCfg()
{
    int mask;

    postEngine( ENGINE_LA_EN, getOnOff() );

    mask = mDxOnOff>>d0;
    postEngine( ENGINE_LA_ON, mask );

    int pos[16];
    collectDxPos( pos );
    postEngine( ENGINE_LA_POS, pos );

    mask = collectActiveMask();
    mask >>= d0;
    postEngine( ENGINE_LA_ACTIVE, mask );

    //! foreach group
    int gpIndex;
    for ( int i = la_g1; i <= la_g4; i++ )
    {
        gpIndex = GP_TO_INDEX( i );
        //! gp on
        if ( get_bit( mGpOnOff, i) )
        {
            postEngine( ENGINE_LA_GROUP, gpIndex, (int)(mGroups[ gpIndex ]>>d0) );
        }
        else
        {
            postEngine( ENGINE_LA_GROUP, gpIndex, (int)0 );
        }
    }

    postEngine( ENGINE_LA_SIZE, m_WaveSize );
}

void servLa::groupAttrSet( int msg, quint32 gpMask )
{
    quint32 gpSum;

    //! group dx
    gpSum = 0;
    for ( int i = 0; i < array_count(mGroups); i++ )
    {
        gpSum |= mGroups[i];
    }

    //! hide the used item
    for ( int i = d0; i <= d15; i++ )
    {
        mUiAttr.setVisible( msg, i, !get_bit(gpSum,i) );
    }

    //! show the self item
    for ( int i = d0; i <= d15; i++ )
    {
        if ( get_bit(gpMask,i) )
        { mUiAttr.setVisible( msg, i, true ); }
    }
}

void  servLa::unGroupAttrSet()
{
    //! check en
    for ( int i = 0; i < array_count(mGroups); i++ )
    {
        if ( mGroups[i] != 0 )
        {
            mUiAttr.setVisible( MSG_LA_UN_GROUP, INDEX_TO_GP(i), true );
        }
        else
        {
            mUiAttr.setVisible( MSG_LA_UN_GROUP, INDEX_TO_GP(i), false );
        }
    }
}

//! put dx in [pos]
int servLa::scanDxPosArray(
                            DsoLa *posAry[],
                            int posCnt )
{
    //! opened la
    QList< DsoLa *> openLas;
    for ( int i = d0; i <= d15; i++ )
    {
        if ( get_bit( mDxOnOff, i) )
        {
            openLas.append( mDxs[ i - d0 ] );
        }
    }

    //! init
    for ( int i = 0; i < posCnt; i++ )
    {
        posAry[i] = NULL;
    }

    //! fill the open pos
    int pos;
    foreach( DsoLa * laItem, openLas )
    {
        Q_ASSERT( NULL != laItem );

        pos = laItem->getPosY();
        Q_ASSERT( pos >= 0 && pos < posCnt );
        Q_ASSERT( posAry[pos] == NULL );
        posAry[ pos ] = laItem;
    }

    return openLas.count();
}

bool servLa::shiftPos( DsoLa *posAry[],
               DsoLa *pItem,
               int aryCount,
               int dir,
               const QList<DsoLa*> &gpLa )
{
    Q_ASSERT( NULL != pItem );
    Q_ASSERT( aryCount >= 8 );      //! for large size

    int from, to;

    from = pItem->getPosY();

    to = from + dir * 1;

    //! check range
    if ( to < 0 )
    { to = 0; }
    else if ( to >= aryCount )
    { to = aryCount - 1;  }
    else
    {}

    //! same now
    if ( from == to )
    { return false; }

    //! dst is sibling
    if ( gpLa.contains( posAry[to]) )
    { return false; }

    //! swap the item
    if ( posAry[to] != NULL )
    {
        DsoLa *temp = posAry[to];
        posAry[to] = pItem;
        posAry[from] = temp;

        posAry[to]->setPosY( to );
        posAry[from]->setPosY( from );
    }
    else
    {
        posAry[to] = posAry[from];
        posAry[ from ] = 0;

        posAry[to]->setPosY( to );
    }

    return true;
}

bool servLa::updatePos( DsoLa *pItem,
                        int dist,
                        const QList<DsoLa*> &gpLa )
{
    Q_ASSERT( NULL != pItem );

    int posCount;
    posCount = getMaxPos(m_WaveSize) + 1;
    DsoLa *posAry[ posCount ];

    //! scan the array
    scanDxPosArray( posAry, posCount );

    //! move a few steps
    int step = dist > 0 ? 1 : -1;
    bool bShift = false, bShiftHist=false;
    for ( ; dist != 0; dist -= step )
    {
        //! shift
        bShift = shiftPos( posAry, pItem, posCount, step, gpLa );
        if ( !bShift )
        { return bShiftHist; }

        bShiftHist = bShiftHist || bShift;
    }

    return bShiftHist;
}

bool servLa::updateGpPos( Chan gp, int dist )
{
    QList< DsoLa *> gpChanList;

    collectGp( gp, gpChanList );

    //! no chan
    if ( gpChanList.size() < 1 ) return false;

    //! ascend
    qSort( gpChanList );

    bool bUpdate = false;
    if ( dist > 0 )
    {
        //! for each item
        for ( int i = gpChanList.size()-1; i >= 0; i-- )
        {
            bUpdate = updatePos( gpChanList[i], dist, gpChanList ) || bUpdate;
        }
    }
    else if( dist < 0 )
    {
        for ( int i = 0; i < gpChanList.size(); i++ )
        {
            bUpdate = updatePos( gpChanList[i], dist, gpChanList ) || bUpdate;
        }
    }
    else
    {}

    return bUpdate;
}

static bool laLessThan( DsoLa *pA, DsoLa *pB )
{
    Q_ASSERT( NULL != pA && NULL != pB );
    return pA->getPosY() < pB->getPosY();
}
bool servLa::validatePos( int maxPos )
{
    //! scan open dx
    QList< DsoLa *> laList;
    collectOpen( laList );

    foreach( DsoLa *pLa, laList )
    {
       if ( pLa->getPosY() < 0 || pLa->getPosY() > maxPos )
       { return false; }
    }

    return true;
}
void servLa::reArrange( int maxPos )
{
    //! scan open dx
    QList< DsoLa *> laList;
    collectOpen( laList );

    //! sort
    qSort( laList.begin(), laList.end(), laLessThan );

    //! for open dx
    //! keep the last order
    int pos = 0;
    foreach( DsoLa *pLa, laList )
    {
        Q_ASSERT( pos <= maxPos );
        pLa->setPosY( pos );
        pos++;
    }

    //! for closed dx
    //! do not change the pos
//    foreach( DsoLa *pLa, mDxs )
//    {
//        Q_ASSERT( NULL != pLa );

//        //! closed
//        if ( !get_bit(mDxOnOff, pLa->getChID() ) )
//        {
//            pLa->setPosY( pos );
//            pos++;
//        }
//    }
}

void servLa::collectGp( Chan gp,
                        QList<DsoLa*> &laList )
{
    Q_ASSERT( gp >= la_g1 && gp <= la_g4 );

    for ( int i = d0; i <= d15; i++ )
    {
        if ( get_bit( mGroups[ gp-la_g1 ], i ) )
        {
            laList.append( mDxs[ i - d0 ] );
        }
    }
}

void servLa::collectOpen(
                       QList<DsoLa*> &laList )
{
    for ( int i = d0; i <= d15; i++ )
    {
        if ( get_bit( mDxOnOff, i ) )
        {
            laList.append( mDxs[ i-d0] );
        }
    }
}

int servLa::sum1b( int dxs )
{
    int cnt;

    cnt = 0;
    for ( int i = d0; i <= d15; i++ )
    {
        if ( get_bit( dxs, i ) )
        { cnt++; }
    }

    return cnt;
}

Chan servLa::findNextDx( )
{
    for ( int i = d0; i <= d15; i++ )
    {
        if ( get_bit( mDxOnOff, i ) )
        { return (Chan)i; }
    }

    return chan_none;
}


/*! *********************************************************************
 * scpi
 ************************************************************************/

#define   ARG_CHECK(arg, a0, a1)                \
            if(argi.size() < 2)                 \
            {                                   \
                return ERR_INVALID_INPUT;       \
            }                                   \
            arg.getVal(a0,0);                   \
            a0 -= d0;                           \
            argi.getVal(a1,1)                   \

DsoErr servLa::setDxDisplay(CArgument argi)
{
    int  digital;
    bool disp;
    ARG_CHECK(argi, digital, disp);

    if( digital < 0 )
    {
        digital = digital + d0;
        for ( int i = 0; i < array_count(mGroups); i++ )
        {
            if ( i == digital-1 )
            {
                quint32 gpOnOff = 0;
                if(disp)
                {
                    gpOnOff = mGpOnOff|GP_1(i);
                }
                else
                {
                    gpOnOff = mGpOnOff& (~(GP_1(i)));
                }
                async(MSG_LA_SELECT_GROUP, (int)gpOnOff);
            }
        }
    }
    else if( digital > d15-d0 )
    {
        if( digital == d0d7-d0 )
        {
            async(MSG_LA_D0D7_ONOFF, disp);
        }
        else
        {
            async(MSG_LA_D8D15_ONOFF, disp);
        }
    }
    else
    {
        if(disp)
        {
            mDxOnOff = mDxOnOff|(0x01<<(digital+all_bit+1));
        }
        else
        {
            mDxOnOff = mDxOnOff&(~(0x01<<(digital+all_bit+1)));
        }
        async(MSG_LA_SELECT_CHAN, (int)mDxOnOff);
    }

    return ERR_NONE;
}

void servLa::getDxDisplay(CArgument argi, CArgument argo)
{
    if(argi.size() <= 0)
    {
        return;
    }

    int ch = 0;
    argi.getVal(ch, 0);
    ch = ch - (int)d0;

    if( ch < 0)
    {
        ch = ch + d0;
        for ( int i = 0; i < array_count(mGroups); i++ )
        {
            if ( i == ch-1 )
            {
                if(mGpOnOff&GP_1(i))
                {
                    argo.setVal(true, 0);
                }
                else
                {
                    argo.setVal(false, 0);
                }
            }
        }
    }
    else  if( ch > d15-d0 )
    {
        if( d0d7-d0 == ch )
        {
            bool b = getLWOnOff();
            argo.setVal(b, 0);
        }
        else
        {
            bool b = getHWOnOff();
            argo.setVal(b, 0);
        }
    }
    else
    {
        if((mDxOnOff>>(ch+all_bit+1)) & 0x01)
        {
            argo.setVal(true,0);
        }
        else
        {
            argo.setVal(false,0);
        }
    }

}

DsoErr servLa::setDigDisplay(CArgument argi)
{
    int  digital;
    bool disp;
    ARG_CHECK(argi, digital, disp);

    if(disp)
    {
        mDxOnOff = mDxOnOff|(0x01<<(digital+all_bit+1));
    }
    else
    {
        mDxOnOff = mDxOnOff&(~(0x01<<(digital+all_bit+1)));
    }
    async(MSG_LA_SELECT_CHAN, (int)mDxOnOff);

    return ERR_NONE;
}

void servLa::getDigDisplay( CArgument argi, CArgument argo )
{
    if(argi.size() < 0)
    {
        return;
    }

    int ch = 0;
    argi.getVal(ch, 0);
    ch = ch - (int)d0;

    if((mDxOnOff>>(ch+all_bit+1)) & 0x01)
    {
        argo.setVal(true,0);
    }
    else
    {
        argo.setVal(false,0);
    }

}

DsoErr servLa::setDigPosition(CArgument argi)
{
    int digital;
    int pos;
    ARG_CHECK(argi, digital, pos);
    //!设置pos
    setDxPos((Chan)(digital + d0), pos);
    async(MSG_LA_SELECT_CHAN, (int)mDxOnOff);

    return ERR_NONE;
}

void servLa::getDigPosition( CArgument argi, CArgument argo )
{
    if(argi.size() < 0)
    {
        return;
    }
    int dx = 0;
    argi.getVal(dx, 0);

    //! check range
    if ( dx >= d0 && dx <= d15 )
    {}
    else
    { return; }

    //! dx opened
    if ( get_bit(mDxOnOff, dx ) )
    {}
    else
    { return; }

    //! convert to dist
    DsoLa *pCurItem;
    pCurItem = mDxs[ dx - d0 ];
    argo.setVal(pCurItem->getPosY());
}

DsoErr servLa::setDigLabel(CArgument argi)
{
    int digital;
    QString label;
    ARG_CHECK(argi, digital, label);
    //!设置lable
    if(label.size()<=22)
    {
        mDxs[ digital ]->setLabel( label );
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    setuiChange( MSG_LA_INPUT_LABEL );

    return ERR_NONE;
}

void servLa::getDiglabel( CArgument argi, CArgument argo )
{
    if(argi.size() <= 0)
    {
        return;
    }
    int dx = 0;
    argi.getVal(dx, 0);

    QString label = mDxs[ dx-d0]->getLabel();

    argo.setVal(label);
}

DsoErr servLa::setGroupAppend(CArgument argi)
{
    if( argi.size()<2 )
        return ERR_NONE;

    int group = 0;
    quint32 groupVal = 0;
    quint32 groupOldVal = 0;

    argi.getVal(group, 0);
    for(int i=1; i<argi.size(); i++ )
    {
        int dx = 0;
        argi.getVal(dx, i);
        dx = dx-d0;
        groupVal |= 0x01<<(dx+all_bit+1);
    }

    switch(group)
    {
    case la_g1:
        groupOldVal = getGroup1Ch();
        groupOldVal |= groupVal;
        async( MSG_LA_GROUP1_SET, (int)groupOldVal );
        break;
    case la_g2:
        groupOldVal = getGroup2Ch();
        groupOldVal |= groupVal;
        async( MSG_LA_GROUP2_SET, (int)groupOldVal );
        break;
    case la_g3:
        groupOldVal = getGroup3Ch();
        groupOldVal |= groupVal;
        async( MSG_LA_GROUP3_SET, (int)groupOldVal );
        break;
    case la_g4:
        groupOldVal = getGroup4Ch();
        groupOldVal |= groupVal;
        async( MSG_LA_GROUP4_SET, (int)groupOldVal );
        break;
    default:
        break;
    }

    return ERR_NONE;
}

DsoErr servLa::setDigHThrevalue(int value)
{
    async(MSG_LA_HIGH_THRE_VAL , value);

    return ERR_NONE;
}
int servLa::getDigHThrevalue()
{
    return getHThrevalue();
}

DsoErr servLa::setDigLThrevalue(int value)
{
    async(MSG_LA_LOW_THRE_VAL , value);

    return ERR_NONE;
}
int servLa::getDigLThrevalue()
{
    return getLThrevalue();
}

DsoErr  servLa::setLaUnChGroup(quint32 value)
{
    if(value >= unGroup1)
    {
        quint32 unGroupValue = 0;
        for ( int i = 0; i < array_count(mGroups); i++ )
        {
            if(mGroups[i])
            {
                unGroupValue |= GP_1(i);
            }
        }
        unGroupValue = unGroupValue&(~(GP_1(value-unGroup1)));

        async( MSG_LA_UN_GROUP, (int)unGroupValue);
    }
    return ERR_NONE;
}

quint32 servLa::getLaUnChGroup()
{
    quint32 gpMask = 0;
    for ( int i = 0; i < array_count(mGroups); i++ )
    {
        if ( mGroups[i] != 0 )
        {
            set_bit( gpMask, INDEX_TO_GP( i ) );
        }
        else
        {

        }
    }

    return gpMask;
}

