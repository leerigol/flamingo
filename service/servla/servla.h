#ifndef SERVLA_H
#define SERVLA_H
#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../../baseclass/dsovert.h"
#include "../service_msg.h"
#include "../service_name.h"

#include "../servvert/servvert.h"

#include "dsola.h"

#define LA_NUMBER 16

#define MAX_LA_THRESHOLD    (20*1000000)    //! uv
#define MIN_LA_THRESHOLD    -MAX_LA_THRESHOLD
#define DEF_LA_THRESHOLD    0

#define LA_THRESHOLD_STEP   (10*1000)       //! 10mv

#define INDEX_TO_GP( index ) ( Q_ASSERT( (index) >= 0 && (index)<=3 ), (index) + la_g1 )
#define GP_TO_INDEX( gp )    ( ( Q_ASSERT( (gp) >= la_g1 && (gp) <= la_g4) ), (gp) - la_g1 )

#define INDEX_TO_DX( ind )   ( Q_ASSERT( (ind) >= 0 && (ind) <= 15), (ind) + d0 )
#define DX_TO_INDEX( dx )    ( Q_ASSERT( (dx) >= d0 && (dx) <= d15), (dx) - d0 )

#define LW_1    (0xff<<d0)
#define LW_0    (~LW_1)

#define HW_1    (0xff<<d8)
#define HW_0    (~HW_1)

#define GP_1( id )  ( 1 << (id+la_g1))
#define GP_0( id )  (~GP_1(id))

#define all_bit     4
#define trim_raw_dx( dx )   ( dx & (0xffff<<5) )


class servLa : public servVert,
               public ISerial,
               public dsoVert
{
    Q_OBJECT

    DECLARE_CMD()

public:
    enum servCmd
    {
        cmd_none = 16,
        cmd_group_changed,
        cmd_dx_changed,

        cmd_opt_changed,

        cmd_dx_pos,     //! dx, pos

        cmd_has_la,
        cmd_la_state,

         //scpi
         cmd_dx_display,
         cmd_digital_display ,
         cmd_digital_position,
         cmd_digital_label,
         cmd_group_append,
         cmd_la_high_thre_val,
         cmd_la_low_thre_val,

         cmd_la_unGroup,
    };

    enum laColor
    {
        la_color_little_green,
        la_color_white,
        la_color_green
    };

public:
    servLa(QString name = serv_name_la, ServiceId id = E_SERVICE_ID_LA);
    virtual ~servLa();

    virtual void registerSpy();
public:
    virtual int  serialOut(CStream &stream, serialVersion &ver);
    virtual int  serialIn(CStream &stream, serialVersion ver);
    virtual void rst();
    virtual int  startup();

public:
    virtual void getPixel( DsoWfm &wfm, Chan subChan=chan_none, HorizontalView view = horizontal_view_main );
    virtual void getTrace( DsoWfm &wfm, Chan subChan=chan_none, HorizontalView view = horizontal_view_main );   //! 16 bit
    virtual void getMemory( DsoWfm &wfm, Chan subChan=chan_none );
                                            //! foreach chan
    virtual void getDigi( DsoWfm &wfm, Chan subChan = chan_none, HorizontalView view = horizontal_view_main );
    virtual int getyPos( Chan subChan  );
public:
    DsoErr      setLaEn( bool b );
    bool        getLaEn();

    DsoErr      onUserOnOff( bool b );

    DsoErr      setCurCh(Chan ch);
    int         getCurCh();

    DsoErr      setCurPos( int pos );
    int         getCurPos();

    DsoErr      setWaveSize(LaScale high);
    LaScale     getWaveSize();

    DsoErr      setGroup1Ch(quint32 value);
    quint32     getGroup1Ch();

    DsoErr      setGroup2Ch(quint32 value);
    quint32     getGroup2Ch();

    DsoErr      setGroup3Ch(quint32 value);
    quint32     getGroup3Ch();

    DsoErr      setGroup4Ch(quint32 value);
    quint32     getGroup4Ch();

    quint32     getGroupx( Chan gp );

    DsoErr      setUnChGroup(quint32 value);
    quint32     getUnChGroup();

    DsoErr      setLaOnOff( quint32 value); //for menu
    DsoErr      setDxOnOff( quint32 value); //for touch window
    quint32     getDxOnOff();
    DsoErr      collidePos( quint32 usedDx, quint32 newDx );
    int         searchPos( QList<int> &usedPos,
                           int pos,
                           int maxPos );

    DsoErr      setGroupOnOff( quint32 value);
    quint32     getGroupOnOff();

    DsoErr      setLWOnOff(bool value);
    bool        getLWOnOff();

    DsoErr      setHWOnOff(bool value);
    bool        getHWOnOff();

    DsoErr      setHThrevalue(int value);
    int         getHThrevalue();

    DsoErr      setLThrevalue(int value);
    int         getLThrevalue();

    DsoErr      setAutoArrange(bool value);
    bool        getAutoArrange();
    void        arrange( int dxFrom,
                         int dxTo,
                         int dxStep,
                         int posFrom,
                         int posStep,
                         int minPos,
                         int maxPos );

    DsoErr      setLabelView( bool b );
    bool        getLabelView();

    DsoErr      setLabelDx( int dx );
    int         getLabelDx();

    DsoErr      setPreLabel(int value);
    int         getPreLabel();

    DsoErr      setLabel( QString &str );
    QString     getLabel();

    DsoErr      setDelayCal(qlonglong delay);
    qlonglong   getDelayCal();
    void        getDelayAttr( enumAttr  );

    DsoErr      setHighColor( laColor color );
    laColor     getHighColor();

    DsoErr      setEdgeColor( laColor color );
    laColor     getEdgeColor();

    DsoErr      setLowColor( laColor color );
    laColor     getLowColor();

public:
    int         getMaxPos( LaScale size );
    DsoLa*      getDx( Chan la );

    DsoErr      setDxPos( Chan dx, int pos );

    DsoErr      setHasLa();
    int         getHasLa();

protected:
    void        on_init();

    void        on_soft_int( quint32 ival );

    void        on_group_changed();
    void        on_dx_changed();

    void        on_opt_changed();
    void        on_hori_mode();

public:
    int         collectActiveMask();
    void        collectDxPos( int pos[16] );
    void        collectUsedPos( quint32 usedDx, QList<int> &usedPos );
    void        engineCfg();

protected:
    void        groupAttrSet( int msg, quint32 gpMask );
    void        unGroupAttrSet();

    bool        keyTranslate(int key, int &msg, int &controlAction);
protected:
    //! arith
    int         scanDxPosArray( DsoLa *posAry[],
                                int aryCount );
    bool        shiftPos( DsoLa *posAry[],
                          DsoLa *pItem,
                          int aryCount,
                          int step,
                          const QList<DsoLa*> &gpLa );

    bool        updatePos( DsoLa *pItem,
                           int dist,
                           const QList<DsoLa*> &gpLa = QList<DsoLa*>() );
    bool        updateGpPos( Chan gp, int dist );

    bool        validatePos( int maxPos );
    void        reArrange( int maxPos );

    void        collectGp( Chan gp,
                           QList<DsoLa*> &laList );

    void        collectOpen(
                           QList<DsoLa*> &laList );

    int         sum1b( int dxs );

    Chan        findNextDx( );

//!scpi cmd
public:   
    DsoErr  setDxDisplay(CArgument argi);
    void    getDxDisplay(CArgument argi, CArgument argo);

    DsoErr  setDigDisplay(CArgument argi);
    void    getDigDisplay(CArgument argi, CArgument argo);

    DsoErr  setDigPosition(CArgument argi);
    void    getDigPosition( CArgument argi, CArgument argo );

    DsoErr  setDigLabel(CArgument argi);
    void    getDiglabel(CArgument argi, CArgument argo);

    DsoErr  setGroupAppend(CArgument argi);

    DsoErr  setDigHThrevalue(int value);
    int     getDigHThrevalue( );

    DsoErr  setDigLThrevalue(int value);
    int     getDigLThrevalue( );

    DsoErr  setLaUnChGroup(quint32 value);
    quint32 getLaUnChGroup();

protected:
    DsoErr  on_enterActive(int id);

private:
    static QString _presetLabel[];

private:
    QList< DsoLa *> mDxs;

private:
    Chan        mCurChan;
    quint32     mGroups[4];     //! group: la mask

    quint32     mDxOnOff;
    quint32     mGpOnOff;

    LaScale     m_WaveSize;
    int         m_HThrevalue;
    int         m_LThrevalue;

    qlonglong   m_DelayCal;

    bool        m_AutoArrange;

    bool        m_LabelView;
    int         mLabelDx;       //! select for label set

    //! color
    laColor     mHighColor, mEdgeColor, mLowColor;

    //! variables
    bool        mbOptEn;
};

#endif // SERVLA_H
