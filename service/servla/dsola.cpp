#include "dsola.h"

DsoLa::DsoLa(Chan id):m_ChID(id)
{
    Q_ASSERT( id >= d0 && id <= d15 );

//    m_OnOff = false;
//    m_Color = Qt::white;

    m_PosY = 0;
    m_Label = QString("D%1").arg( id - d0 );
}

Chan  DsoLa::getChID()
{
    return m_ChID;
}

//DsoErr DsoLa::setOnOff( bool onoff)
//{
//    m_OnOff = onoff;
//    return ERR_NONE;
//}
//bool DsoLa::getOnOff()
//{
//    return m_OnOff;
//}

//DsoErr DsoLa::setColor(QColor color)
//{
//    m_Color = color;
//    return ERR_NONE;
//}
//QColor DsoLa::getColor()
//{
//    return m_Color;
//}

DsoErr DsoLa::setLabel(QString &label)
{
    m_Label = label;
    return ERR_NONE;
}
QString DsoLa::getLabel()
{
    return m_Label;
}

DsoErr DsoLa::setPosY(int posy)
{
    m_PosY = posy;
    return ERR_NONE;
}
int  DsoLa::getPosY()
{
    return   m_PosY;
}

bool DsoLa::operator<( DsoLa * pItem )
{
    Q_ASSERT( NULL != pItem );

    return m_PosY < pItem->getPosY();
}
bool DsoLa::operator<( DsoLa & item )
{
    return m_PosY < item.getPosY();
}
