#include "./servla.h"
#include "../servtrace/servtrace.h"

//! 16 bit
void servLa::getPixel( DsoWfm &wfm, Chan subChan, HorizontalView view )
{
    Q_UNUSED(wfm);
    Q_UNUSED(subChan);
    Q_UNUSED(view);
//    //! -- digi
//    DsoWfm wfmDigi;
//    getDigi( wfmDigi, subChan );
//    if ( wfmDigi.size() > 0 )
//    {}
//    else
//    { return; }

//    //! plane to 1 dot


//    //! -- to pixel
//    struMem mem;
//    struView view;

//    //! ---- trace to screen
//    //! mem
//    mem.sett( wfmDigi.getllt0(), wfmDigi.getlltInc() );
//    mem.setLength( wfmDigi.size() );

//    //! view
//    EngineViewAttr mainViewAttr;
//    DsoErr err;
//    err = queryEngine( qENGINE_MAIN_VIEW_ATTR, &mainViewAttr );
//    if ( err != ERR_NONE ) return;

//    view.sett( mainViewAttr.mt0, mainViewAttr.mtInc );
//    view.setWidth( mainViewAttr.mWidth );

//    //! -- resample
//    //! cap
//    wfm.init( mainViewAttr.mWidth );

//    //! resample
//    int ret = reSampleExt( &mem,
//                           &view,
//                           wfmTrace.getPoint() + wfmTrace.start(),
//                           wfm.getPoint(),
//                           e_sample_norm|e_sample_head|e_sample_tail );
//    if ( ret != 0 )
//    {
//        wfm.setRange( 0, 0 );
//        return;
//    }
//    else
//    {
//        wfm.setvAttr( &wfmTrace );
//        wfm.settAttr( mainViewAttr.mt0,
//                      mainViewAttr.mtInc,
//                      mainViewAttr.mWidth );

//        wfm.setRange( view.vLeft, view.vLen );
//    }

//#ifdef _DEBUG
//    wfm.dbgShow();
//    wfm.save( "/rigol/log/chx_pixel.dat" );
//#endif
}

void servLa::getTrace( DsoWfm &wfm, Chan /*subChan*/, HorizontalView view )   //! 16 bit
{
    //! -- query
    CArgument argIn,argOut;

    //! dx
    argIn.setVal( (int)(la)  );
    argIn.setVal( (pointer)&wfm, 1 );
    argIn.setVal( view, 2 );

    DsoErr err;
    err = serviceExecutor::query( serv_name_trace,
                                  servTrace::cmd_query_trace_xxx,
                                  argIn,
                                  argOut);
    if ( err != ERR_NONE )
    { return; }
}

void servLa::getMemory( DsoWfm &/*wfm*/, Chan /*subChan*/ )
{}

//! foreach chan
void servLa::getDigi( DsoWfm &wfm, Chan subChan, HorizontalView view )
{
    Q_ASSERT( subChan >= d0 && subChan <= d15 );

    if( get_bit(mDxOnOff, subChan) )
    {
        //! -- query
        CArgument argIn,argOut;

        //! dx
        argIn.setVal( (int)( subChan)  );
        argIn.setVal( (pointer)&wfm, 1 );
        argIn.setVal( view, 2 );

        DsoErr err;
        err = serviceExecutor::query( serv_name_trace,
                                      servTrace::cmd_query_trace_xxx,
                                      argIn,
                                      argOut);
        if ( err != ERR_NONE )
        {
            return;
        }
    }
}

//! -1 -- closed chan
int servLa::getyPos( Chan subChan  )
{
    Q_ASSERT( dsoVert::yChId == la );
    Q_ASSERT( subChan >= d0 && subChan <= d15 );

    //! dx on
    if ( get_bit(mDxOnOff, subChan) )
    {
        return mDxs[ subChan - d0 ]->getPosY();
    }
    else
    { return -1; }
}
