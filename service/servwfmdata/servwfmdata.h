#ifndef SERVWFMDATA_H
#define SERVWFMDATA_H

#include "../service.h"
#include "../../baseclass/dsovert.h"
#include "../../engine/base/dsoengine.h"

enum readMode
{//! 枚举值不可改变，手册要求。
    wfm_normal  = 0,
    wfm_maximum = 1,
    wfm_raw     = 2,
};

enum dataMode
{
    data_screen,
    data_memory,
};

enum returnFormat
{//! 枚举值不可改变，手册要求。
    wfm_byte  = 0,
    wfm_word  = 1,
    wfm_ascii = 2,
};


class servWfmData : public serviceExecutor
{
    Q_OBJECT
    DECLARE_CMD()

public:
    servWfmData( QString name, ServiceId id=E_SERVICE_ID_NONE);
    ~servWfmData();
    virtual DsoErr start();
    virtual void   registerSpy();
public:
    enum servCmd
    {
        CMD_NONE,
        CMD_SOURCE,
        CMD_MODE,
        CMD_FORMAT,
        CMD_START_OFFS,
        CMD_STOP_OFFS,
        CMD_DATA,

        CMD_WAV_POINTS,
        CMD_XINCREMENT,
        CMD_XORIGIN,
        CMD_XREFERENCE,
        CMD_YINCREMENT,
        CMD_YORIGIN,
        CMD_YREFERENCE,

        CMD_WAV_BEGIN,
        CMD_WAV_END,
        CMD_WAV_RESET,
        CMD_PREAMBLE,
        CMD_WAV_STATUS,

        CMD_GET_DATA,
    };

private:
    Chan          m_source;
    readMode      m_mode;
    returnFormat  m_reformat;
    dataMode      m_dataMode;

    int           m_starPoint;
    int           m_stopPoint;
    int           m_offsPoint;
    int           m_points;

    QByteArray    outData;
protected:
    Chan         getSource() ;
    DsoErr       setSource(Chan source);

    readMode     getMode();
    DsoErr       setMode(readMode mode);

    returnFormat getReformat();
    DsoErr       setReformat(returnFormat reformat);

    int          getStarPoint();
    DsoErr       setStarPoint(int star);

    int          getStopPoint();
    DsoErr       setStopPoint(int stop);

    void         onDataModeChange();
    bool         onCheckDataRange(int start, int stop);

    /***********scpi************/
    int         setWavePoints(int points);
    int         getWavePoints();

    qint64      getXIncrement();
    qint64      getXOrigin();
    qint64      getXReference();
    float       getYIncrement();
    int         getYOrigin();
    qint64      getYReference();

    DsoErr      getPreamble(CArgument argO);

    /*----------------*/
    int         setWaveBegin();
    int         setWaveEnd();
    int         setWaveReset();
    DsoErr      getWaveStatus(CArgument argO);

protected:
    CSaFlow     getAcqSaFlow();

public:

    int getScreenData(quint8 *pBuf, int start, int len);
    int getMemoryData(quint8 *pBuf, int start, int len);
    int getwfmData(   quint8 *pBuf, int start, int len);

    QByteArray   toWord(quint8  *pBuf, int len);
    QByteArray   toAscii(quint8 *pBuf, int len);
    QByteArray   toByte(quint8  *pBuf, int len);
    QByteArray   toFormat(quint8  *pBuf, int len);

    /*-------scpi---------*/
    int  scpiGetDataSize();
    void getData(CArgument &iarg, CArgument &oarg);
};

#endif // SERVWFMDATA_H
