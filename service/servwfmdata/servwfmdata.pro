#-------------------------------------------------
#
# Project created by QtCreator 2017-03-28T14:48:13
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets
QT       += network


TARGET = ../../lib$$(PLATFORM)/services/servwfmdata
TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += .

SOURCES += servwfmdata.cpp \
    wfmdataformater.cpp

HEADERS += servwfmdata.h \
    wfmdataformater.h

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

