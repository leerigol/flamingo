#include "servwfmdata.h"
#include "../servmemory/servmemory.h"
#include "../servdso/sysdso.h"
#include "wfmdataformater.h"
#include "../../engine/cplatform.h"
#include "../../engine/base/adt/patchrequest.h"
#include "../servtrace/servtrace.h"

//#undef  LOG_DBG
//#define LOG_DBG()    qDebug()<<__FILE__<<__FUNCTION__<<__LINE__

IMPLEMENT_CMD( serviceExecutor, servWfmData )
start_of_entry()

on_set_int_int(servWfmData::CMD_SOURCE,            &servWfmData::setSource),
on_get_int(    servWfmData::CMD_SOURCE,            &servWfmData::getSource),
                                                   
on_set_int_int(servWfmData::CMD_MODE,              &servWfmData::setMode),
on_get_int(    servWfmData::CMD_MODE,              &servWfmData::getMode),
                                                   
on_set_int_int(servWfmData::CMD_FORMAT,            &servWfmData::setReformat),
on_get_int(    servWfmData::CMD_FORMAT,            &servWfmData::getReformat),
                                                   
on_set_int_int(servWfmData::CMD_START_OFFS,        &servWfmData::setStarPoint),
on_get_int(    servWfmData::CMD_START_OFFS,        &servWfmData::getStarPoint),
                                                   
on_set_int_int(servWfmData::CMD_STOP_OFFS,         &servWfmData::setStopPoint),
on_get_int(    servWfmData::CMD_STOP_OFFS,         &servWfmData::getStopPoint),
                                                   
on_get_int(    servWfmData::CMD_DATA,              &servWfmData::scpiGetDataSize),

on_set_void_void(MSG_HORIZONTAL_RUN,                &servWfmData::onDataModeChange),

/*************************scpi**************************/
on_set_int_int( servWfmData::CMD_WAV_POINTS,       &servWfmData::setWavePoints),
on_get_int(     servWfmData::CMD_WAV_POINTS,       &servWfmData::getWavePoints),
                                                   
on_get_ll(     servWfmData::CMD_XINCREMENT,        &servWfmData::getXIncrement),
on_get_ll(     servWfmData::CMD_XORIGIN,           &servWfmData::getXOrigin),
on_get_ll(     servWfmData::CMD_XREFERENCE,        &servWfmData::getXReference),
on_get_float(  servWfmData::CMD_YINCREMENT,        &servWfmData::getYIncrement),
on_get_int(     servWfmData::CMD_YORIGIN,          &servWfmData::getYOrigin),
on_get_ll(     servWfmData::CMD_YREFERENCE,        &servWfmData::getYReference),
on_get_void_argo(servWfmData::CMD_PREAMBLE,        &servWfmData::getPreamble),
                                                   
                                                   
on_set_int_void( servWfmData::CMD_WAV_BEGIN,       &servWfmData::setWaveBegin),
on_set_int_void( servWfmData::CMD_WAV_END,         &servWfmData::setWaveEnd),
on_set_int_void( servWfmData::CMD_WAV_RESET,       &servWfmData::setWaveReset),
on_get_void_argo(servWfmData::CMD_WAV_STATUS,      &servWfmData::getWaveStatus),

on_get_void_argi_argo(servWfmData::CMD_GET_DATA,   &servWfmData::getData),

end_of_entry()

#define batch_size  (2*patch_size*sub_patch)

servWfmData::servWfmData(QString name, ServiceId id)
    :serviceExecutor(name,id)
{
    serviceExecutor::baseInit();

    m_source   = chan1;
    m_mode     = wfm_normal;
    m_reformat = wfm_byte;
    m_dataMode = data_screen;

    m_starPoint = 0;
    m_stopPoint = 1000;
    m_offsPoint = 0;
    m_points    = 1000;

    //! delete by link
    wfmdataFormater fmt;
    CVal val;
    fmt.format( val );
}

servWfmData::~servWfmData()
{

}

DsoErr servWfmData::start()
{
    return ERR_NONE;
}

void servWfmData::registerSpy()
{
    spyOn(serv_name_hori, MSG_HORIZONTAL_RUN);
}

Chan servWfmData::getSource()
{
    return m_source;
}

DsoErr servWfmData::setSource(Chan source)
{
    if( (!(chan1<= source && source <= d15))  && (!(m1<= source && source <= m4)) )
    {
        return ERR_INVALID_INPUT;
    }

    m_source = source;

    onDataModeChange();
    return ERR_NONE;
}

readMode servWfmData::getMode()
{
    return m_mode;
}

DsoErr servWfmData::setMode(readMode mode)
{
    m_mode = mode;
    onDataModeChange();
    return ERR_NONE;
}

returnFormat servWfmData::getReformat()
{
    return m_reformat;
}

DsoErr servWfmData::setReformat(returnFormat reformat)
{
    m_reformat = reformat;
    return ERR_NONE;
}

int servWfmData::getStarPoint()
{
    return m_starPoint;
}

DsoErr servWfmData::setStarPoint(int star)
{
    if( onCheckDataRange(star, m_stopPoint) )
    {
        m_starPoint = star;
        return ERR_NONE;
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
}

int servWfmData::getStopPoint()
{
    return m_stopPoint;
}

DsoErr servWfmData::setStopPoint(int stop)
{
    if( onCheckDataRange(m_starPoint, stop) )
    {
        m_stopPoint = stop;
        return ERR_NONE;
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
}

void servWfmData::onDataModeChange()
{
    if( (wfm_normal == m_mode) || (m1<= m_source && m_source <= m4))
    {
        m_dataMode = data_screen;
    }
    else if(wfm_raw == m_mode)
    {
        m_dataMode = data_memory;
    }
    else if(wfm_maximum == m_mode)
    {
        int run;
        query( serv_name_hori, MSG_HORIZONTAL_RUN, run );
        if(run == Control_Stop)
        {
            m_dataMode = data_memory;
        }
        else
        {
            m_dataMode = data_screen;
        }
    }
    else
    {
        Q_ASSERT(false);
    }

    onCheckDataRange(m_starPoint, m_stopPoint);
}

bool servWfmData::onCheckDataRange(int start, int stop)
{
    int min = 1;
    int max = 1000;

    if( data_screen == m_dataMode )
    {
        min = 1;
        max = 1000;
    }
    else if(data_memory == m_dataMode)
    {
        min = 1;
        max = getAcqSaFlow().getDepth().toInt();
    }
    else
    {Q_ASSERT(false);}

    if( (qBound(min, start, max) !=  start) ||
        (qBound(min, stop,  max) !=  stop)  )
    {
        m_starPoint = qBound(min, m_starPoint, max);
        m_stopPoint = qBound(min, m_stopPoint,  max);
        return false;
    }

    return true;
}

int servWfmData::getScreenData(quint8 *pBuf, int start, int len)
{
    Q_ASSERT(pBuf != NULL);

    dsoVert *pVert = NULL;
    pVert = dsoVert::getCH( m_source );
    Q_ASSERT( NULL != pVert );

    DsoWfm wfm;
    pVert->getPixel( wfm );

    DsoPoint *pData = wfm.getPoint();
    Q_ASSERT( NULL != pData );

    len = qMin( wfm.size() - start, len);
    LOG_DBG()<<"size:"<<wfm.size()<<"start:"<<start<<"len"<<len;

    if( len > 0 )
    {
        memcpy( pBuf, pData+start, len );
    }
    return len;
}

int servWfmData::getMemoryData(quint8 *pBuf, int start, int len)
{
    LOG_DBG()<<"start:"<<start<<"len:"<<len;

    Q_ASSERT( NULL != pBuf );
    int ret = -1;

    bool bRun = false;
    serviceExecutor::query(serv_name_trace, servTrace::cmd_trace_run, bRun);
    if(bRun)
    {
        return ret;
    }

#if 1
    dsoEngine *pEngine;
    pEngine = cplatform::sysGetDsoPlatform()->getPlayEngine();
    Q_ASSERT(pEngine != NULL);

    //! open
    handleMemory *handle = pEngine->openMemory( m_source );
    if ( NULL == handle || !handle->getValid() )
    { return ret; }
    LOG_DBG()<<handle->getFullSize();

    //! Engine export init
    pEngine->exportInit(handle);

    //! read
    //! readMemory的缓存不能小于 sub_patch_size;
    int    redSize   = qMin(len, batch_size);
    quint8 *pTempBuf = new quint8[ redSize ];
    ret = pEngine->readMemory( handle, pTempBuf, start, redSize);
    Q_ASSERT( ret<= redSize );
    LOG_DBG()<<ret;
    if(ret>0)
    {
         memcpy( pBuf, pTempBuf, ret);
    }
    delete []pTempBuf;

    //! close
    pEngine->closeMemory( handle );
#else
    for(int i = 0; i < len; i++)
    {
        pBuf[i] = 0x55;
    }
    ret = len;

#endif
    return ret;
}

int servWfmData::getwfmData(quint8 *pBuf, int start, int len)
{
    Q_ASSERT(pBuf != NULL);

    if(data_screen == m_dataMode)
    {
        return getScreenData(pBuf, start, len);
    }
    else
    {
        return getMemoryData(pBuf, start, len);
    }
}

QByteArray servWfmData::toWord(quint8 *pBuf, int len)
{
    Q_ASSERT( NULL != pBuf );
    QByteArray out;

    const char hByte = 0x00;
    for(int i =0; i<len; i++)
    {
        out.append( (char*)(pBuf + i) );
        out.append(hByte);
    }
    return out;
}

QByteArray servWfmData::toAscii(quint8 *pBuf, int len)
{
    Q_ASSERT( NULL != pBuf );
    QByteArray out;
    double     fValue = 0.0;
    QString    sValue = "+0.000000E+00";
    for(int i =0; i< len; i++)
    {
        fValue = (double)(*(pBuf+i));
        sValue = QString("%1").arg(fValue, 0, 'E', 6);

        if(fValue>=0)
        {
            sValue = "+" + sValue;
        }

        out.append(sValue);

        if(i != len)
        {
            out.append(',');
        }
    }
    return out;
}

QByteArray servWfmData::toByte(quint8 *pBuf, int len)
{
    Q_ASSERT( NULL != pBuf );
    QByteArray out;
    out.append((char*)pBuf, len);
    return out;
}
QByteArray servWfmData::toFormat(quint8 *pBuf, int len)
{
    switch (m_reformat)
    {
    case wfm_word:
        return toWord(pBuf, len);
    case wfm_byte:
        return toByte(pBuf, len);
    case wfm_ascii:
        return toAscii(pBuf, len);
    default:
        return toByte(pBuf, len);
    }
}


int servWfmData::scpiGetDataSize()
{
    //! 防止用户吧 start和stop设置反。
    m_starPoint = qMin(m_starPoint, m_stopPoint);
    m_stopPoint = qMax(m_starPoint, m_stopPoint);

    m_offsPoint = m_starPoint;
    int len = m_stopPoint - m_starPoint + 1;

    switch (m_reformat)
    {
    case wfm_word:
        return len*2 - 1;
    case wfm_byte:
        return len;
    case wfm_ascii:
        return len*14 - 1;
    default:
        return len;
    }
}

/*!
 * \brief servWfmData::getData
 * \param iarg     int类型，   获取数据的长度
 * \param oarg     ArbBin类型，数据以二进制的方式返回，且需要调用者释放空间。
 */
void servWfmData::getData(CArgument &iarg, CArgument &oarg)
{
    Q_ASSERT(iarg.size()>0);
    oarg.clear();

    //!计算长度
    int outSize = 0;
    iarg.getVal(outSize, 0);
    outSize = qMin(outSize, (m_stopPoint - m_offsPoint) );
    Q_ASSERT( outSize >= 0);

    //!获取数据
    outData.clear();

    if(outSize <= 0)
    {
        LOG_DBG()<<"outSize:"<<outSize;
        return;
    }

    quint8 *pBuf = new quint8[ outSize ];
    int ret = getwfmData(pBuf, m_offsPoint, outSize);
    if(ret > 0)
    {
        //!转化格式
        QByteArray byteData = toFormat(pBuf, ret);

        //!填充数据， arbData 需要手动释放空间
        ArbBin arbData(byteData.size(), byteData.data());
        oarg.setVal(arbData,0);
    }
    else
    {
        oarg.clear();
    }
    delete []pBuf;

    //!偏移下次起点
    m_offsPoint += ret;
    Q_ASSERT(m_offsPoint <= m_stopPoint);
}

/**************scpi*******************/
int servWfmData::setWavePoints(int points)
{
    m_points = points;
    return ERR_NONE;
}

int servWfmData::getWavePoints()
{
    if( data_screen == m_dataMode )
    {
        m_points = 1000;
    }
    else if(data_memory == m_dataMode)
    {
        m_points = getAcqSaFlow().getDepth().toInt();

    }
    else{Q_ASSERT(false);}

    return m_points;
}

qint64 servWfmData::getXIncrement()
{
    if(data_screen == m_dataMode)
    {
        return getHoriAttr(m_source, horizontal_view_main).tInc;
    }
    else
    {
        return getAcqSaFlow().getDotTime().toLonglong();
    }  
}

qint64 servWfmData::getXOrigin()
{
    if(data_screen == m_dataMode)
    {
       horiAttr attr = getHoriAttr(m_source, horizontal_view_main);
       return (-attr.gnd * attr.tInc);
    }
    else
    {
        return getAcqSaFlow().getT0().toLonglong();
    }
}

qint64 servWfmData::getXReference()
{
    return 0;
}

float servWfmData::getYIncrement()
{
    return getVertAttr(m_source).yInc;
}

int servWfmData::getYOrigin()
{
    vertAttr attr = getVertAttr(m_source);
    return attr.yGnd - 128;
}

qint64 servWfmData::getYReference()
{
    return adc_center;
}


DsoErr servWfmData::getPreamble(CArgument argO)
{
    int acqMode = Acquire_Normal;
    int count = 1;
    query(serv_name_hori, MSG_HOR_ACQ_MODE_YT, acqMode);
    if(Acquire_Average == acqMode)
    {
        query(serv_name_hori, MSG_HOR_ACQ_AVG_TIMES, count);
    }

    argO.setVal( (int)getReformat(), 0);
    argO.setVal( (int)getMode(),     1);
    argO.setVal( getWavePoints(),    2);
    argO.setVal( (int)count,         3);//!
    argO.setVal( getXIncrement(),    4);
    argO.setVal( getXOrigin(),       5);
    argO.setVal( getXReference(),    6);
    argO.setVal( getYIncrement(),    7);
    argO.setVal( getYOrigin(),       8);
    argO.setVal( getYReference(),    9);
    return ERR_NONE;
}
/*! ------------4条 ds2000 的指令 现在已经弃用-----------------*/
int servWfmData::setWaveBegin()
{
    return ERR_NONE;
}

int servWfmData::setWaveEnd()
{
    return ERR_NONE;
}

int servWfmData::setWaveReset()
{
    return ERR_NONE;
}

DsoErr servWfmData::getWaveStatus(CArgument argO)
{
    argO.setVal(0, 0);
    argO.setVal(0, 1);
    return ERR_NONE;
}
/*! --------------------------------------------------------*/

CSaFlow servWfmData::getAcqSaFlow()
{
    CSaFlow  saAttr;
    if( (chan1<=m_source) && (m_source<=chan4))
    {
        queryEngine( qENGINE_ACQ_CH_MAIN_SAFLOW, &saAttr);
    }
    else if( (d0<=m_source) && (m_source<=d15) )
    {
        queryEngine( qENGINE_ACQ_LA_MAIN_SAFLOW, &saAttr);
    }
    else
    {Q_ASSERT(false);}

    return saAttr;
}

