#ifndef WFMDATAFORMATER_H
#define WFMDATAFORMATER_H

#include "../../com/scpiparse/cconverter.h"

using namespace scpi_parse;

class wfmdataFormater :public CFormater
{
public:
    wfmdataFormater();

public:
    scpiError format( CVal &val );
    quint32   getOutData(QByteArray &buff, quint32 len);

private:
    bool      bFirstPack;
};

#endif // VWFDATAFORMATER_H
