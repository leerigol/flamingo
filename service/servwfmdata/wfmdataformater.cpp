#include "wfmdataformater.h"
#include "./servwfmdata.h"

wfmdataFormater::wfmdataFormater()
{
    bFirstPack = true;
}

scpiError wfmdataFormater::format(CVal &val)
{
    if ( val.vType != val_int )
    {
        return scpi_err_val_type_mismatch;
    }

    mOutDataLen = val.iVal;

    return scpi_err_none;
}

quint32 wfmdataFormater::getOutData(QByteArray &buff, quint32 len)
{
    DsoErr err = ERR_NONE;
    buff.clear();

    if(bFirstPack)
    {
        //qDebug()<<__FILE__<<__LINE__<<"Out dtat len:"<<mOutDataLen;
        mOutStr.append(QString("#9%1").arg(mOutDataLen,9,10,QLatin1Char('0')));
        mOutDataLen += mOutStr.size();

        //第一次取数据结束.
        bFirstPack = false;
    }

    //! 如果缓存中的数据量小于接口请求的数据量，则请求新数据
    //! 若缓存中的数据量大于接口的请求数量，则直接从缓存中读取。
    if((int)len > mOutStr.size())
    {
        CArgument iarg;
        CArgument oarg;
        iarg.append( (int)(len - mOutStr.size()) );
        serviceExecutor::query(serv_name_wfm_data,
                               servWfmData::CMD_GET_DATA,
                               iarg, oarg);

        if(oarg.size() > 0 )
        {
            ArbBin binData;
            err = oarg.getVal(binData, 0);
            Q_ASSERT(err == ERR_NONE);

            mOutStr.append( (char*)(binData.mPtr),  binData.mBlockLen);

            binData.free();

            quint32 copyLen = qMin( (int)len, mOutStr.size());
            buff.append(mOutStr.data(), copyLen);
            mOutStr.remove(0, copyLen);
        }
        else //!获取数据失败
        {
            buff.append(mOutStr.data(), mOutStr.size());
            mOutStr.clear();
        }
    }
    else
    {
        buff.append(mOutStr.data(),len);
        mOutStr.remove(0,len);
    }
    //qDebug()<<__FILE__<<__LINE__<<mOutDataLen<<len<<buff.size();

    if(buff.size() > 0 )
    {
        mOutDataLen -= buff.size();
        mOutDataLen = qMax((qint64)0, mOutDataLen);
    }
    else
    {
        mOutDataLen = 0;
    }
    Q_ASSERT(mOutDataLen >=0);

    return buff.size();
}

