#include "servdg.h"
#include "../servlicense/servlicense.h"
#include "../service_name.h"
#include "../servstorage/servstorage.h"
#include "../servHotplug/servhotplug.h"
#include "../../module/funcsource/funcSweepConfig.h"


servDG::servDG(QString name, ServiceId eId) : serviceExecutor(name, eId)
{
    serviceExecutor::baseInit();

    if(*(name.end()-1) == QString("1"))
    {
        m_dgChan = CH1_FPGA;
    }
    else
    {
        m_dgChan = CH2_FPGA;
    }
    m_stBasic.bSourceEnable = false;

    mVersion = 0xA;
}

DsoErr servDG::setSourceEn(bool en)
{
    if(sysGetWorkMode() == work_help)
    {
        return ERR_NONE;
    }

    m_stBasic.bSourceEnable = en;


    syncEngine( m_stBasic.bSourceEnable ? ENGINE_LED_ON : ENGINE_LED_OFF,
                DsoEngine::led_source1 + m_dgChan - 1 );

    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);

    configMode();
    if(m_stBurst.bBurstEn)
    {
        cBurst.funBurstParaConfig(m_stBurst, m_dgChan);
    }

    return ERR_NONE;
}

bool servDG::getSourceEn()
{
    return m_stBasic.bSourceEnable;
}

DsoErr servDG::setWaveType(servDG::enumDGwave t)
{
    if(m_nWaveType != t)
    {
        m_bSync = false;
    }
    //!DC切换到其他波形，电压要变化
    if(getWaveType() == DG_WAVE_DC && t != DG_WAVE_DC)
    {
        async(MSG_DG_OFFSET_LEVEL,0);
        async(MSG_DG_OFFSET_LEVEL_SUB_1,m_nDGOffset);
        //qDebug() << "m_nDGOffset = "  << m_nDGOffset;
    }

    m_nWaveType = t;
    if( WAVE_DC == (SourceWaveformEnum) m_nWaveType ||
            WAVE_PULSE == (SourceWaveformEnum) m_nWaveType ||
            WAVE_NOISE == (SourceWaveformEnum) m_nWaveType)
    {
        m_stModu.bModEnable = false;
        if(m_stBurst.bBurstEn)
        {
            m_stBasic.s64Freq = m_stBasic.s64Freq_Menu;
        }
        m_stBurst.bBurstEn = false;
        m_stSweep.bSweepEn = false;

        //bug 2750 by zy
        m_stBasic.bSourceEnable = false;
        defer(CMD_SERVICE_DEACTIVE);
        defer(CMD_SERVICE_ACTIVE);
    }
#if 1
    else
    {
        m_stModu.bModEnable = true;
        if(m_nWorkMode == DG_BURST)
        {
            m_stBurst.bBurstEn = true;
        }
        m_stSweep.bSweepEn = true;

        if(m_nWorkMode == DG_SWEEP)
        {
            async(MSG_DG_SWEEP_STARTFREQ,m_stSweep.startFreq);
            async(MSG_DG_SWEEP_ENDFREQ,m_stSweep.endFreq);
        }
    }
 #endif

    if(m_nWaveType < DG_INTERWAVE_FORM)
    {
        m_stBasic.enSourceWaveform = (SourceWaveformEnum)m_nWaveType;
        mUiAttr.setVisible(MSG_DG_AMP_LEVEL, true);
        mUiAttr.setVisible(MSG_DG_AMP_LEVEL, true);
    }
    if(m_nWaveType >= DG_INTERWAVE_FORM && m_nWaveType < DG_WAVE_ARB)
    {
        m_stBasic.enSourceWaveform = WAVE_ARB_INTERNAL;
        m_stBasic.enInterWaveform = (SourceInternalWaveformEnum) (m_nWaveType - DG_INTERWAVE_FORM);
        mUiAttr.setVisible(MSG_DG_AMP_LEVEL, true);
    }
    if(m_nWaveType == DG_WAVE_ARB)
    {
        //to do : set arb wavetable
        m_stBasic.enSourceWaveform = WAVE_ARB_EXTERNAL;
        //! 在任意波编辑时，后期使用控件幅度和偏移。
#if 0
        mUiAttr.setVisible(MSG_DG_AMP_LEVEL, false);
        mUiAttr.setVisible(MSG_DG_AMP_LEVEL, false);
#else
        mUiAttr.setVisible(MSG_DG_AMP_LEVEL, true);
        mUiAttr.setVisible(MSG_DG_AMP_LEVEL, true);
#endif
       // mUiAttr.setVisible(MSG_DG_OFFSET_LEVEL, false);
    }

    changeBasicMenuEn(m_nWaveType);
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
    configMode();
    async(MSG_DG_FREQ_PERIOD_SUB_1,m_nFreqPeriod ? (s64)(1e18/m_stBasic.s64Freq_Menu):m_stBasic.s64Freq_Menu);
    return ERR_NONE;
}

int servDG::getWaveType()
{
    return m_nWaveType;
}

DsoErr servDG::selectFreqPeriod(int b)
{
    m_nFreqPeriod = b;
    return ERR_NONE;
}

int servDG::getSelectFreqPeriod()
{
    return m_nFreqPeriod;
}

DsoErr servDG::setFreqPeriod(qint64 value)
{
     if(getSelectFreqPeriod())
     {
         setDGPeriod(value);
     }
     else
     {
         setDGFreq(value);
     }
 //   defer(MSG_DG_CHANGEFREQ);
    return ERR_NONE;
}

qint64 servDG::getFreqPeriod()
{
    if(getSelectFreqPeriod())
    {
        return getDGPeriod();
    }
    else
    {
        return getDGFreq();
    }
}

DsoErr servDG::setDGFreq(qint64 f)
{
    m_stBasic.s64Freq_Menu = f;
    if(1e18/f >= m_stBurst.stTrigger.s64Trig_Interval)
    {
        m_stBurst.stTrigger.s64Trig_Interval = 2 * 1e18/f;
        //async(MSG_DG_BURST_PERIOD,m_stBurst.stTrigger.s64Trig_Interval);
    }
    if(m_nWorkMode == DG_MODU)
    {
        m_stBasic.s64Freq = m_stModu.isPolarity ? m_stModu.u64HopFreq_menu : m_stBasic.s64Freq_Menu;
    }
#if 1
    else if(m_nWorkMode == DG_BURST &&
            !(WAVE_DC == (SourceWaveformEnum) m_nWaveType ||
             WAVE_PULSE == (SourceWaveformEnum) m_nWaveType ||
             WAVE_NOISE == (SourceWaveformEnum) m_nWaveType))
    {
        m_stBasic.s64Freq = m_stBasic.s64Freq_Menu * 2;
    }
#endif
    else
    {
        m_stBasic.s64Freq = m_stBasic.s64Freq_Menu;
    }

 //   cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);

#if 0
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
#else
    if((m_nWaveType == DG_WAVE_SQUARE) ||(m_nWaveType == DG_WAVE_PULSE))
    {
        cBasic.funcSetSourceFreq(m_stBasic, m_dgChan);
    }
    else
    {
        cBasic.funcSetSourcePara(m_stBasic, m_dgChan);
    }
#endif
    configMode();
    return ERR_NONE;
}

qint64 servDG::getDGFreq()
{
    qlonglong maxFreq = 0;
    switch (m_nWaveType) {
    case DG_WAVE_SINE:
        maxFreq = 25000;
        break;
    case DG_WAVE_SQUARE:
        maxFreq = 15000;
      //  maxFreq = 15010;
        break;
    case DG_WAVE_RAMP:
        maxFreq = 100;
        break;
    case DG_WAVE_PULSE:
        maxFreq = 1000;
        break;
    case DG_WAVE_ARB:
        maxFreq = 10000;
        break;
    default:
        maxFreq = 1000;
        break;
    }
    //bug 1999 by zy
#if 0
    setuiStep( getDGFreqStep(m_stBasic.s64Freq_Menu, freq_mHz(1)) );
#else
    if(m_stBasic.s64Freq_Menu == freq_KHz(maxFreq))
    {
        setuiStep(freq_mHz(1));
    }
    else
    {
        setuiStep(getDGFreqStep(m_stBasic.s64Freq_Menu, freq_mHz(1)));
    }
#endif
    setuiZVal(freq_KHz(1) );
    setuiMinVal(freq_mHz(100) );
    setuiMaxVal(freq_KHz(maxFreq));
    //setuiMaxVal( freq_KHz(25000));
    setuiUnit(Unit_hz);
    setuiBase(1, E_N6);
    setuiPostStr("Hz");
    return m_stBasic.s64Freq_Menu;
}

DsoErr servDG::setDGPeriod(qint64 p)
{
    m_stBasic.s64Freq_Menu = 1e18/p;
    if(p >= m_stBurst.stTrigger.s64Trig_Interval)
    {
        m_stBurst.stTrigger.s64Trig_Interval = 2 * p;
        //async(MSG_DG_BURST_PERIOD,m_stBurst.stTrigger.s64Trig_Interval);
    }
    if(m_nWorkMode == DG_MODU)
    {
        m_stBasic.s64Freq = m_stModu.isPolarity ? m_stModu.u64HopFreq_menu : m_stBasic.s64Freq_Menu;
    }
#if 1
    else if(m_nWorkMode == DG_BURST &&
               !(WAVE_DC == (SourceWaveformEnum) m_nWaveType ||
                WAVE_PULSE == (SourceWaveformEnum) m_nWaveType ||
                WAVE_NOISE == (SourceWaveformEnum) m_nWaveType))
    {
        m_stBasic.s64Freq = m_stBasic.s64Freq_Menu * 2;
    }
#endif
    else
    {
        m_stBasic.s64Freq = m_stBasic.s64Freq_Menu;
    }

#if 0
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
#else
    if((m_nWaveType == DG_WAVE_SQUARE) ||(m_nWaveType == DG_WAVE_PULSE))
    {
        cBasic.funcSetSourceFreq(m_stBasic, m_dgChan);
    }
    else
    {
        cBasic.funcSetSourcePara(m_stBasic, m_dgChan);
    }
   //cBasic.funcSetSourcePara(m_stBasic, m_dgChan);
#endif
    configMode();
    return ERR_NONE;
}

qint64 servDG::getDGPeriod()
{
    qlonglong minPeriod = 0;
    switch (m_nWaveType) {
    case DG_WAVE_SQUARE:
        minPeriod  = 66600;
        break;
    case DG_WAVE_RAMP:
         minPeriod = 10000000;
        break;
    case DG_WAVE_PULSE:
        minPeriod = 1000000;
        break;
    default:
         minPeriod = 40000;
         break;
    }
  //  setuiMinVal(time_ps(minPeriod));
    qint64 p = 1e18/(m_stBasic.s64Freq_Menu);
    setuiStep( getDGTimeStep(p, time_ns(40)) );
    setuiZVal(time_ms(1));
  //  setuiMinVal(time_ns(40));
    setuiMinVal(time_ps(minPeriod));
    setuiMaxVal(time_s(10));
    setuiUnit(Unit_s);
    setuiBase(1e-12f);
    setuiPostStr("s");
    return p;
}

DsoErr servDG::selectAmpLevel(int a)
{
    m_nAmpLevel = a;
    if(getSelectOffsetLevel() != a)
    {
        async(MSG_DG_OFFSET_LEVEL,a);
    }
    return ERR_NONE;
}

int servDG::getSelectAmpLevel()
{
    return m_nAmpLevel;
}

DsoErr servDG::setAmpLevel(qint64 value)
{
    if(getSelectAmpLevel())
    {
        setDGHVol(value);
#if 1
        setDGAmp(m_nAmplitude);
        setDGOffset(m_nDGOffset);
#endif
    }
    else
    {
        setDGAmp(value);
#if 1
        setDGHVol(m_nHighVol);
        setDGLVol(m_nLowVol);
#endif
    }
    //! 设置完标准波后检查猝发设置
    configMode();
    return ERR_NONE;
}

qint64 servDG::getAmpLevel()
{
    if(getSelectAmpLevel())
    {
        return getDGHVol();
    }
    else
    {
        return getDGAmp();
    }
}

DsoErr servDG::setDGAmp(qint64 a)
{
    DsoErr s32Err = ERR_NONE;
    unHotPlug();

    m_nAmplitude = a;
    m_stBasic.u32Amp = m_bImpedance?(a*2):a;
    LOG_DBG()<<m_stBasic.u32Amp;

    if (m_nAmplitude < DG_MIN_AMP)
    {
        m_nAmplitude = DG_MIN_AMP;
        s32Err = ERR_OVER_LOW_RANGE;
    }
    else if (m_nAmplitude > DG_MAX_AMP)
    {
        m_nAmplitude = DG_MAX_AMP;
        s32Err = ERR_OVER_UPPER_RANGE;
    }

    qint32 s32OffsetMax = (DG_MAX_AMP / 2) - m_nAmplitude / 2 ;

    if(m_nDGOffset > s32OffsetMax)
    {
        m_nDGOffset  = s32OffsetMax;
    }
    else if(m_nDGOffset < -s32OffsetMax)
    {
        m_nDGOffset  = (-s32OffsetMax);
    }
    LOG_DBG()<<m_nDGOffset;

    setuiChange(MSG_DG_OFFSET_LEVEL_SUB_1);
#if 0
    setDGOffset(m_nDGOffset);
#endif

  //  cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);

#if 0
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
#else
   cBasic.funcSetSourcePara(m_stBasic, m_dgChan);
#endif

    m_nHighVol = m_nAmplitude/2 + m_nDGOffset;
    m_nLowVol = -m_nAmplitude/2 + m_nDGOffset;
#if 0
    setDGOffset(m_nDGOffset);
#endif
    return s32Err;
}

qint64 servDG::getDGAmp()
{
    setuiStep(getDGAmpStep(m_nAmplitude, mv(1)));
    setuiZVal(vv(1));
    setuiMinVal(DG_MIN_AMP);
    setuiMaxVal(DG_MAX_AMP);
    setuiUnit(Unit_V);
    setuiBase(1,E_N6);
    setuiPostStr("V");
    return m_nAmplitude;
}

DsoErr servDG::setDGHVol(qint64 value)
{
    unHotPlug();

    m_nHighVol = value;
    m_nHimpHVol = m_bImpedance?(m_nHighVol*2):m_nHighVol;

    m_nAmplitude = m_nHighVol - m_nLowVol;
    m_nDGOffset = (m_nHighVol + m_nLowVol)/2;

    return ERR_NONE;
}

qint64 servDG::getDGHVol()
{
    setuiStep( getDGAmpStep(m_nHighVol, mv(1)) );
    setuiZVal( mv(500));
    setuiMinVal( m_nLowVol + DG_MIN_AMP);
    setuiMaxVal( DG_MAX_AMP/2);
    setuiUnit(Unit_V);
    setuiBase(1,E_N6);
    setuiPostStr("V");
    return m_nHighVol;
}

DsoErr servDG::selectOffsetLevel(int o)
{
    m_nOffsetLevel = o;
    if(getSelectAmpLevel() != o)
    {
        async(MSG_DG_AMP_LEVEL, o);
    }
    return ERR_NONE;
}

int servDG::getSelectOffsetLevel()
{
    return m_nOffsetLevel;
}

DsoErr servDG::setOffsetLevel(qint64 value)
{
    if(getSelectOffsetLevel())
    {
        setDGLVol(value);
#if 1
            setDGAmp(m_nAmplitude);
            setDGOffset(m_nDGOffset);
#endif
    }
    else
    {
        setDGOffset(value);
#if 1
             setDGHVol(m_nHighVol);
             setDGLVol(m_nLowVol);
#endif
    }

    //! 设置完标准波后检查猝发设置
    configMode();
    return ERR_NONE;
}

qint64 servDG::getOffsetLevel()
{
    if(getSelectOffsetLevel())
    {
        return getDGLVol();
    }
    else
    {
        return getDGOffset();
    }
    LOG_DBG()<<m_nDGOffset;
}

DsoErr servDG::setDGOffset(qint64 Offset)
{
    unHotPlug();
    m_nDGOffset = Offset;
    m_stBasic.s32Offset = m_bImpedance ? (Offset*2):Offset;
    LOG_DBG()<<m_stBasic.s32Offset;

    cBasic.funcSetSourcePara(m_stBasic, m_dgChan);

    m_nHighVol = m_nAmplitude/2 + m_nDGOffset;
    m_nLowVol = -m_nAmplitude/2 + m_nDGOffset;
    return ERR_NONE;
}

qint64 servDG::getDGOffset()
{
    int offsetMax = (DG_MAX_AMP / 2) - m_nAmplitude / 2;
    setuiStep(getDGAmpStep(m_nDGOffset, mv(1)) );
    setuiZVal(0);
    if(m_nWaveType == DG_WAVE_DC)
    {
        setuiMaxVal( DG_MAX_AMP/2);
        setuiMinVal( -DG_MAX_AMP/2);
    }
    else
    {
        setuiMaxVal( offsetMax);
        setuiMinVal( -offsetMax);
    }

    setuiUnit(Unit_V);
    setuiBase(1, E_N6);
    setuiPostStr("V");

    return m_nDGOffset;
}

DsoErr servDG::setDGLVol(qint64 value)
{
    unHotPlug();
    //qDebug() << "value = " << value;
    m_nLowVol = value;
    m_nHimpLVol = m_bImpedance?(m_nLowVol*2):m_nLowVol;

    m_nAmplitude = m_nHighVol - m_nLowVol;
    m_nDGOffset = (m_nHighVol + m_nLowVol)/2;

    return ERR_NONE;
}

qint64 servDG::getDGLVol()
{
    setuiStep( getDGAmpStep(m_nLowVol, mv(1)) );
    setuiZVal( mv(500));
    setuiMinVal( -DG_MAX_AMP/2 );
    setuiMaxVal( m_nHighVol - DG_MIN_AMP);
    setuiUnit(Unit_V);
    setuiBase(1,E_N6);
    setuiPostStr("V");
    return m_nLowVol;
}


DsoErr servDG::setSync()
{
        cBasic.funcSetSourcePhaseAlign(m_dgChan, m_stBasic);
        //bug 2028 by zy
#if 1  
        //! 在进行了基本波的配置之后，重新刷新一下burst的设置
        cBurst.funBurstParaConfig(m_stBurst, ALL_FPGA);

        if(m_nWorkMode == DG_MODU)
        {
            cModu.funcSetSourceModPara(ALL_FPGA, m_stBasic, m_stModu);
        }

        //! 如果sweep打开的情况下，配置一下sweep
        if(m_nWorkMode == DG_SWEEP)
        {
            cSweep.funcSweepParaConfig(m_stSweep, ALL_FPGA);
        }
#endif   
    return ERR_NONE;
}

DsoErr servDG::setPhase(int Phase)
{
    m_stBasic.u16Phase = Phase;

#if 0
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
#else
   cBasic.funcSetSourcePhase(m_dgChan,m_stBasic);
#endif
    return ERR_NONE;
}

int servDG::getPhase()
{
    setuiStep(getDGNumStep(m_stBasic.u16Phase));//! 步进为0.1°
    setuiMinVal(0);
    setuiMaxVal(360*10);
    setuiUnit(Unit_degree);
    setuiBase(1,E_N1);

    //for bug455/456
    double val = m_stBasic.u16Phase;
    val = val / 10;
    setuiOutStr(QString("%1°").arg(val,0,'f',1));
    return m_stBasic.u16Phase;
}

DsoErr servDG::setDutyCycle(int d)
{
    m_stBasic.u16Duty = d;
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
    //! 设完标准置波后检查猝发设置
    configMode();
    return ERR_NONE;
}

int servDG::getDutyCycle()
{
    setuiStep(getDGNumStep(m_stBasic.u16Duty));
    setuiMinVal(10*10);
    setuiMaxVal(90*10);
    setuiUnit(Unit_percent);
    setuiBase(1,E_N1);

    //for bug455/456
    double val = m_stBasic.u16Duty;
    val = val / 10;
    //setuiOutStr(QString("%1%").arg(val,0,'f',1));
    setuiPostStr("%");
    return m_stBasic.u16Duty;
}

DsoErr servDG::setSymmetry(int s)
{
    m_stBasic.u16Symmetry = s;

    //qDebug() << m_stBasic.u16Symmetry;
 //   cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
    //! 设置完标准波后检查猝发设置
 //   configMode();
    startTimer(dg_ramp_update_wave_tmo, dg_ramp_update_wave_id, timer_single);
    return ERR_NONE;
}

int servDG::getSymmetry()
{
    setuiStep(getDGNumStep(m_stBasic.u16Symmetry));
    setuiMinVal(0);
    setuiMaxVal(100*10);
    setuiUnit(Unit_percent);
    setuiBase(1,E_N1);

    //for bug455/456
    double val = m_stBasic.u16Symmetry;
    val = val / 10;
    setuiOutStr(QString("%1%").arg(val,0,'f',1));
    return m_stBasic.u16Symmetry;
}

DsoErr servDG::setImpedance(bool b)
{
   m_bImpedance = b;
   if(m_bImpedance)
   {
       m_nAmplitude = m_stBasic.u32Amp/2;
       m_nDGOffset = m_stBasic.s32Offset/2;
       m_nHighVol = m_nHimpHVol/2;
       m_nLowVol = m_nHimpLVol/2;
       DG_MIN_AMP = mv(10);
       DG_MAX_AMP = vv(2.5);
   }
   else
   {
       m_nAmplitude = m_stBasic.u32Amp;
       m_nDGOffset = m_stBasic.s32Offset;
       m_nHighVol = m_nHimpHVol;
       m_nLowVol = m_nHimpLVol;
       DG_MIN_AMP = mv(20);
       DG_MAX_AMP = vv(5);
   }
   setuiChange(MSG_DG_OFFSET);
   setuiChange(MSG_DG_AMP_LEVEL_SUB_1);
   setuiChange(MSG_DG_OFFSET_LEVEL_SUB_1);
   return ERR_NONE;
}

/**
 * @brief servDG::getImpedance
 * @return false=HighZ, true=50ou
 */
bool servDG::getImpedance()
{
    return m_bImpedance;
}

/********************************Modu*****************************************/

DsoErr servDG::setWorkType(enumDGWorkMode mode)
{
    if(m_nWorkMode == mode)
    {
        //! 没有切换
        return ERR_NONE;
    }
    //! 切换前模式
    if(m_nWorkMode == DG_MODU)
    {
        setModEnable(false);
    }
    else if(m_nWorkMode == DG_BURST)
    {
        m_stBurst.bBurstEn = false;
        cBurst.funBurstParaConfig(m_stBurst, m_dgChan);
    }
    else
    {
        m_stSweep.bSweepEn = false;
        cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    }

    //! 切换后模式
    m_nWorkMode = mode;

    //!类型转换后，刷新频率
    async(MSG_DG_FREQ_PERIOD_SUB_1,m_nFreqPeriod ? (s64)(1e18/m_stBasic.s64Freq_Menu):m_stBasic.s64Freq_Menu);
    // async(MSG_DG_FREQ_PERIOD_SUB_1,m_stBasic.s64Freq_Menu);
    //!使能Frequency
    mUiAttr.setEnable(MSG_DG_FREQ_PERIOD,  true);
    mUiAttr.setEnable(MSG_DG_STARTPHASE,  true);

    switch (mode) {
    case DG_MODU:
        setModEnable(true);
        break;
    case DG_BURST:
        m_stBurst.bBurstEn = true;

        //!禁用掉外部触发
        mUiAttr.setEnable(MSG_DG_TRIGSRC, Trig_EXTERNAL,false);
        mUiAttr.setEnableVisible(MSG_DG_TRIGSRC,Trig_EXTERNAL,false,false);
        //!类型转换后，刷新频率
         //async(MSG_DG_FREQ_PERIOD_SUB_1,getFreqPeriod());
        cBurst.funBurstParaConfig(m_stBurst, m_dgChan);
        break;
    case DG_SWEEP:
        m_stSweep.bSweepEn = true;
        cSweep.funcSweepParaConfig( m_stSweep, m_dgChan);

        //!失能Frequency
        mUiAttr.setEnable(MSG_DG_FREQ_PERIOD,  false);
        mUiAttr.setEnable(MSG_DG_STARTPHASE,  false);
        //!禁用掉外部触发
        mUiAttr.setEnable(MSG_DG_TRIGSRC, Trig_EXTERNAL,false);
        mUiAttr.setEnableVisible(MSG_DG_TRIGSRC,Trig_EXTERNAL,false,false);
        break;
    default:
        break;
    }

    changeMenuEn(m_nWorkMode);
    return ERR_NONE;
}

enumDGWorkMode servDG::getWorkType()
{
    return m_nWorkMode;
}

DsoErr servDG::setModEnable(bool b)
{
   m_stModu.bModEnable = b;
   LOG_DBG()<<b;
   if(m_stModu.enModType == MOD_FSK && m_stModu.bModEnable)
   {     
       setModFSKPol(m_stModu.isPolarity);
   }
   else
   {
       cModu.funcSetSourceModEnable(m_dgChan,m_stBasic,m_stModu);
       cBasic.funcSetSourcePara(m_stBasic,m_dgChan);
       configMode();
   }
   return ERR_NONE;
}

bool servDG::getModEnable()
{
    return m_stModu.bModEnable;
}

DsoErr servDG::setModType(int t)
{
    m_stModu.enModType = (SourceModType)t;

    if(m_stModu.enModType == MOD_FSK && m_stModu.bModEnable)
    {
        setModFSKFreq();
    }

    cModu.funcSetSourceModEnable(m_dgChan,m_stBasic,m_stModu);
    cBasic.funcSetSourcePara(m_stBasic,m_dgChan);
    configMode();
    return ERR_NONE;
}

int  servDG::getModType()
{
    return m_stModu.enModType;
}

DsoErr servDG::setModWave(int t)
{
    m_stModu.enModWave = (SourceModWave)t;

    //!zy 调制波形为噪声时，调制频率置灰
    if(m_stModu.enModWave == MOD_WAVE_NOISE)
    {
        mUiAttr.setEnable(MSG_DG_MOD_FREQ, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_DG_MOD_FREQ, true);
    }
    cModu.funcSetSourceModEnable(m_dgChan,m_stBasic,m_stModu);
    return ERR_NONE;
}

int servDG::getModWave()
{
    return (int)m_stModu.enModWave;
}

DsoErr servDG::setModFreq(qint64 f)
{
    m_stModu.u64ModFreq = f;

    cModu.funcSetSourceModEnable(m_dgChan,m_stBasic,m_stModu);
    return ERR_NONE;
}

qint64 servDG::getModFreq()
{
    setuiStep( getDGFreqStep(m_stModu.u64ModFreq, freq_Hz(1)) );
    setuiZVal( freq_KHz(1) );
    setuiMinVal( freq_Hz(1) );
    setuiMaxVal( freq_KHz(50));
    setuiUnit(Unit_hz);
    setuiBase(1, E_N6);
    setuiPostStr("Hz");
    return m_stModu.u64ModFreq;
}

DsoErr servDG::setModDepth(int d)
{
    m_stModu.u8AMDepth = d;
    cModu.funcSetSourceModEnable(m_dgChan,m_stBasic,m_stModu);
    return ERR_NONE;
}

int servDG::getModDepth()
{
    setuiStep( getDGNumStep(m_stModu.u8AMDepth) );
    setuiZVal( 60 );
    setuiMinVal( 0 );
    setuiMaxVal(120);
    setuiUnit(Unit_percent);
    setuiBase(1, E_0);
    setuiFmt( dso_view_format(fmt_int, fmt_width_4 ) );
    setuiPostStr("%");
    return m_stModu.u8AMDepth;
}

DsoErr servDG::setModDeviation(qint64 Dev)
{
    m_stModu.u64FMDev = Dev;
    LOG_DBG()<<m_stModu.u64FMDev<<Dev;
    cModu.funcSetSourceModEnable(m_dgChan,m_stBasic,m_stModu);
    return ERR_NONE;
}

qint64 servDG::getModDeviation()
{

//    qDebug() << "bj " << getDGFreqStep(m_stModu.u64FMDev, freq_Hz(1));
#if 0
    setuiStep( getDGFreqStep(m_stModu.u64FMDev, freq_Hz(1)) );
#else
     setuiStep(freq_Hz(1));
#endif
    setuiZVal( freq_KHz(1) );
#if 1
    setuiMinVal(freq_Hz(1) );
#else
    setuiMinVal(0);
#endif
    setuiMaxVal(m_stBasic.s64Freq_Menu);
    setuiUnit(Unit_hz);
    setuiBase(1, E_N6);
    setuiPostStr("Hz");
    LOG_DBG()<<m_stModu.u64FMDev;
    return m_stModu.u64FMDev;
}

DsoErr servDG::setModHopFreq(qint64 Freq)
{
    m_stModu.u64HopFreq_menu = Freq;
    setModFSKFreq();
    cModu.funcSetSourceModEnable(m_dgChan, m_stBasic, m_stModu);
    return ERR_NONE;
}

qint64 servDG::getModHopFreq()
{
    setuiStep( getDGFreqStep(m_stModu.u64HopFreq_menu, freq_mHz(1)) );
    setuiZVal( freq_KHz(1) );
    setuiMinVal(freq_mHz(100) );
    setuiMaxVal( freq_MHz(25));
    setuiUnit(Unit_hz);
    setuiBase(1, E_N6);
    setuiPostStr("Hz");
    return m_stModu.u64HopFreq_menu;
}

//! 只有在打开DG并打开调制同时调制类型是FSK时才会改变设置
DsoErr servDG::setModFSKPol(bool isNeg)
{
    m_stModu.isPolarity = isNeg;
    setModFSKFreq();
    cModu.funcSetSourceModEnable(m_dgChan,m_stBasic,m_stModu);
    cBasic.funcSetSourcePara(m_stBasic,m_dgChan);
    configMode();
    return ERR_NONE;
}

bool servDG::getModFSKPol()
{
    return m_stModu.isPolarity;
}

DsoErr servDG::setModFSKFreq()
{
    if(m_nWorkMode == DG_MODU && m_stBasic.bSourceEnable && m_stModu.enModType == MOD_FSK)
    {
        m_stBasic.s64Freq   = m_stModu.isPolarity ? m_stModu.u64HopFreq_menu : m_stBasic.s64Freq_Menu;
        m_stModu.u64HopFreq = m_stModu.isPolarity ? m_stBasic.s64Freq_Menu : m_stModu.u64HopFreq_menu;
    }
    else
    {
        m_stBasic.s64Freq = m_stBasic.s64Freq_Menu;
        m_stModu.u64HopFreq = m_stModu.u64HopFreq_menu;
    }
    return ERR_NONE;
}

//! burst
DsoErr servDG::setBurstType(enumBurstType type)
{
    m_stBurst.euBurstType = type;

    cBurst.funBurstParaConfig(m_stBurst, m_dgChan);
    if(type == BURST_INFINITE)
    {
        m_bTrigInit = false;
        mUiAttr.setEnable(MSG_DG_BURST_CYCLE_NUM, false);

        //!For bug 1094 猝发类型为无限，触发源为内部
        //! 2018年1月2日19:57:29 by Yang,Zhou
  #if 1
        setTrigSrc(Trig_MANUAL);
        mUiAttr.setEnable(MSG_DG_TRIGSRC, false);
 #endif
    }
    else
    {
        mUiAttr.setEnable(MSG_DG_BURST_CYCLE_NUM, true);
 #if 1
        setTrigSrc(Trig_INTERNAL);
        mUiAttr.setEnable(MSG_DG_TRIGSRC, true);
#endif
    }
    return ERR_NONE;
}

enumBurstType servDG::getBurstType()
{
    return m_stBurst.euBurstType;
}

DsoErr servDG::setCycleNum(int num)
{
    m_stBurst.s32Cycle = num;  

   qlonglong maxNum = m_stBurst.stTrigger.s64Trig_Interval/(1e18/m_stBasic.s64Freq_Menu);
   int remNum = (int)(m_stBurst.stTrigger.s64Trig_Interval%((qlonglong)(1e18/m_stBasic.s64Freq_Menu)));
  // if(maxNum <= num || maxNum >= 1000000)
   if((maxNum <= num || maxNum > (time_s(500)/time_ns(40))) && (!remNum))
   {
        m_stBurst.bBurstEn = false;
        m_stBasic.s64Freq = m_stBasic.s64Freq_Menu;
   }
   else
   {
        m_stBurst.bBurstEn = true;
        m_stBasic.s64Freq = m_stBasic.s64Freq_Menu * 2;
   }

    cBasic.funcSetSourcePara(m_stBasic, m_dgChan);
    cBurst.funBurstParaConfig(m_stBurst, m_dgChan);
    return ERR_NONE;
}

int servDG::getCycleNum()
{
    setuiZVal(1);
    qlonglong maxNum = m_stBurst.stTrigger.s64Trig_Interval/(1e18/m_stBasic.s64Freq_Menu);
  /*
    if(maxNum <= 1)
    {
        async(MSG_DG_BURST_PERIOD,m_stBurst.stTrigger.s64Trig_Interval*2);
        maxNum = 2;
    }
    */
    if(m_stBurst.stTrigger.euTrig_Source != Trig_MANUAL)
    {
#if 0
        setuiMaxVal(m_stBurst.stTrigger.s64Trig_Interval/m_nPeriod);
        //qDebug() << m_stBurst.stTrigger.s64Trig_Interval << (int(1e18/m_stBasic.s64Freq_Menu)) << m_stBurst.stTrigger.s64Trig_Interval/m_nPeriod;
#else
    //    if(maxNum >= 1000000)
        {
        //    maxNum = 1000000;
        }
        //qlonglong maxNum = m_stBurst.stTrigger.s64Trig_Interval/(1e18/m_stBasic.s64Freq_Menu);
       setuiMaxVal(maxNum);
#endif
       setuiMinVal(1);
    }
    else
    {
        setuiMaxVal(1000000);
        setuiMinVal(1);
    }
    return m_stBurst.s32Cycle;
}

DsoErr servDG::setDelay(qlonglong delay)
{
    //!For bug 1098
    //! 2018年1月2日19:57:29 by Yang,Zhou
    if(delay < time_us(100))
    {
        delay = 0;
    }

    m_stBurst.s64Delay = delay;
    cBurst.funBurstParaConfig(m_stBurst, m_dgChan);

    async(MSG_DG_TRIGSRC,getTrigSrc());
    return ERR_NONE;
}

qlonglong servDG::getDelay()
{
    setuiStep( getDGTimeStep(m_stBurst.s64Delay, time_ms(1)) );
    setuiZVal( time_s(1) );
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr("s");
    setuiMinVal( 0 );

    qlonglong maxPeriod = m_stBurst.stTrigger.s64Trig_Interval - m_stBurst.s32Cycle*(1e18/m_stBasic.s64Freq_Menu);
#if 1
    setuiMaxVal(maxPeriod);
#else
    setuiMaxVal( time_s(100));
#endif
    return m_stBurst.s64Delay;
}

DsoErr servDG::setTrigSrc(enumTrigSRC src)
{
    //!禁用掉外部触发
 //   mUiAttr.setEnable(MSG_DG_TRIGSRC, Trig_EXTERNAL,false);
  //  mUiAttr.setEnableVisible(MSG_DG_TRIGSRC,Trig_EXTERNAL,false);

    if(m_nWorkMode == DG_BURST)
    {
        m_stBurst.stTrigger.euTrig_Source = src;
        cBurst.funBurstParaConfig(m_stBurst, m_dgChan);
        if(src == Trig_MANUAL)
        {
            //!模式发生改变后，重置无限模式手动触发状态，无限模式下下次按手动触发显示波形
            m_bTrigInit = false;
        }
    }
    if(m_nWorkMode == DG_SWEEP)
    {
        m_stSweep.stTrigger.euTrig_Source = src;
#if 1
        async(MSG_DG_SWEEP_STARTFREQ,m_stSweep.startFreq);
        async(MSG_DG_SWEEP_ENDFREQ,m_stSweep.endFreq);
#endif
    }

    if(src == Trig_MANUAL)
    {
        LOG_DBG();
        mUiAttr.setEnable(MSG_DG_MANUAL_TRIG, true);
    }
    else
    {
        LOG_DBG();
        mUiAttr.setEnable(MSG_DG_MANUAL_TRIG, false);
    }

    return ERR_NONE;
}

enumTrigSRC servDG::getTrigSrc()
{
    enumTrigSRC src = Trig_INTERNAL;
    if(m_nWorkMode == DG_BURST)
    {
        src = m_stBurst.stTrigger.euTrig_Source;
    }
    else
    {
        src = m_stSweep.stTrigger.euTrig_Source;
    }
    return src;
}

DsoErr servDG::trigManual()
{
    if(m_nWorkMode == DG_BURST)
    {
        if(m_stBurst.euBurstType == BURST_INFINITE)
        {
            cBurst.funBurstManualTrig(m_dgChan, m_bTrigInit);
            m_bTrigInit = !m_bTrigInit;
        }
        else
        {
            cBurst.funBurstManualTrig(m_dgChan);
        }
    }
    if(m_nWorkMode == DG_SWEEP)
    {
        cSweep.funcSweepTrigManual(m_dgChan);
    }

    return ERR_NONE;
}

DsoErr servDG::setBurstPeriod(qlonglong period)
{
    m_stBurst.stTrigger.s64Trig_Interval = period;
    /*
      if(maxNum <= 1)
      {
          async(MSG_DG_BURST_PERIOD,m_stBurst.stTrigger.s64Trig_Interval*2);
          maxNum = 2;
      }
      */
    cBurst.funBurstParaConfig(m_stBurst, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getBurstPeriod()
{
    setuiStep( getDGTimeStep(m_stBurst.stTrigger.s64Trig_Interval, time_ms(1)) );
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr("s");
#if 1
    setuiMaxVal(time_s(500));
#else
    setuiMaxVal(time_s(1000));
#endif
    setuiZVal( time_ms(1) );
    //! 猝发周期的最小值位载波周期×循环数
   // setuiMinVal( m_nPeriod * m_stBurst.s32Cycle);
    setuiMinVal((qlonglong)(1e18/m_stBasic.s64Freq_Menu)* m_stBurst.s32Cycle);
    setCycleNum(m_stBurst.s32Cycle);

    return m_stBurst.stTrigger.s64Trig_Interval;
}

//! sweep
DsoErr servDG::setSweepType(enumSweepType type)
{
    m_stSweep.sweepType = type;
    cSweep.funcSweepParaConfig(m_stSweep,m_dgChan);
    if(type == SWEEP_STEP)
    {
        mUiAttr.setEnable(MSG_DG_SWEEP_STEP, true);
    }
    else
    {
        mUiAttr.setEnable(MSG_DG_SWEEP_STEP, false);
    }
    return ERR_NONE;
}

enumSweepType servDG::getSweepType()
{
    return m_stSweep.sweepType;
}

DsoErr servDG::setSweepTime(qlonglong time)
{
    m_stSweep.sweepTime = time;
    cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getSweepTime()
{
    setuiStep( getDGTimeStep(m_stSweep.sweepTime, time_us(100)) );
    setuiMaxVal( time_s(500) );
    setuiZVal( time_ms(1) );
#if 0
    setuiMinVal( 0 );
#else
    setuiMinVal( time_ms(1) );
 #endif
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr("s");
    return m_stSweep.sweepTime;
}

DsoErr servDG::setReturnTime(qlonglong time)
{
    m_stSweep.returnTime = time;
    cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getReturnTime()
{
    setuiStep( getDGTimeStep(m_stSweep.returnTime, time_us(100)) );
    setuiMaxVal( time_s(500) );
    setuiZVal( time_ms(1) );
    setuiMinVal( 0 );
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr("s");
    return m_stSweep.returnTime;
}

DsoErr servDG::setStartKeep(qlonglong time)
{
    m_stSweep.startKeepTime = time;
    cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getStartKeep()
{
    setuiStep( getDGTimeStep(m_stSweep.startKeepTime, time_us(100)) );
    setuiMaxVal( time_s(500) );
    setuiZVal( time_ms(1) );
    setuiMinVal( 0 );
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr("s");
    return m_stSweep.startKeepTime;
}

DsoErr servDG::setEndKeep(qlonglong time)
{
    m_stSweep.endKeepTime = time;
    cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getEndKeep()
{
    setuiStep( getDGTimeStep(m_stSweep.endKeepTime, time_us(100)) );
    setuiMaxVal(time_s(500) );
    setuiZVal( time_ms(1) );
    setuiMinVal( 0 );
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr("s");
    return m_stSweep.endKeepTime;
}

DsoErr servDG::setSweepStep(qlonglong step)
{
    m_stSweep.step = step;
    cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getSweepStep()
{
    setuiMaxVal(1000);
    setuiMinVal(2);
    setuiZVal(2);
    return m_stSweep.step;
}

DsoErr servDG::setStartFreq(qlonglong freq)
{
  // qDebug() << "m_stSweep.startFreq = " << freq;
    m_stSweep.startFreq = freq;
    cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getStartFreq()
{
    qlonglong maxFreq = 0;
    switch (m_nWaveType) {
    case DG_WAVE_SINE:
        maxFreq = 25000;
        break;
    case DG_WAVE_SQUARE:
        maxFreq = 15000;
      //  maxFreq = 15010;
        break;
    case DG_WAVE_RAMP:
        maxFreq = 100;
        break;
    case DG_WAVE_PULSE:
        maxFreq = 1000;
        break;
    case DG_WAVE_ARB:
        maxFreq = 10000;
        break;
    default:
        maxFreq = 1000;
        break;
    }

    setuiStep(getDGFreqStep(m_stSweep.startFreq, freq_mHz(1)));
    setuiMaxVal(freq_KHz(maxFreq));
    setuiZVal( freq_KHz(1) );
    setuiMinVal( freq_mHz(100) );
    setuiUnit(Unit_hz);
    setuiBase(1, E_N6);
    setuiPostStr("Hz");
    return m_stSweep.startFreq;
}

DsoErr servDG::setEndFreq(qlonglong freq)
{
    m_stSweep.endFreq = freq;
    cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    return ERR_NONE;
}

qlonglong servDG::getEndFreq()
{
    qlonglong maxFreq = 0;
    switch(m_nWaveType) {
    case DG_WAVE_SINE:
        maxFreq = 25000;
        break;
    case DG_WAVE_SQUARE:
        maxFreq = 15000;
      //  maxFreq = 15010;
        break;
    case DG_WAVE_RAMP:
        maxFreq = 100;
        break;
    case DG_WAVE_PULSE:
        maxFreq = 1000;
        break;
    case DG_WAVE_ARB:
        maxFreq = 10000;
        break;
    default:
        maxFreq = 1000;
        break;
    }
    setuiStep( getDGFreqStep(m_stSweep.endFreq, freq_mHz(1)) );
    setuiMaxVal(freq_KHz(maxFreq));
    setuiZVal( freq_KHz(1) );
    setuiMinVal(freq_mHz(100));
    setuiUnit(Unit_hz);
    setuiBase(1, E_N6);
    setuiPostStr("Hz");
    return m_stSweep.endFreq;
}

int servDG::getMenuMSG()
{
    return m_nMenuMSG;
}

int servDG::getExitMenuMsg()
{
    return m_nExitMenu;
}

DsoErr servDG::configMode()
{   
    //! 在进行了基本波的配置之后，重新刷新一下burst的设置
    cBurst.funBurstParaConfig(m_stBurst, m_dgChan);

    if(m_nWorkMode == DG_MODU)
    {
        cModu.funcSetSourceModPara(m_dgChan, m_stBasic, m_stModu);
    }

    //! 如果sweep打开的情况下，配置一下sweep
    if(m_nWorkMode == DG_SWEEP)
    {
        cSweep.funcSweepParaConfig(m_stSweep, m_dgChan);
    }
    return ERR_NONE;
}

//!scpi
int servDG::cmd_setSin(CArgument &arg)
{
    cmd_setWaveArg(arg);
    async(MSG_DG_WAVE,DG_WAVE_SINE);
    //temp
    m_stBasic.bSourceEnable = true;
    async(MSG_APP_DG,m_stBasic.bSourceEnable);
    return ERR_NONE;
}

int servDG::cmd_setSquare(CArgument &arg)
{
    cmd_setWaveArg(arg);
    async(MSG_DG_WAVE,DG_WAVE_SQUARE);
    //temp
    m_stBasic.bSourceEnable = true;
    async(MSG_APP_DG,m_stBasic.bSourceEnable);
    return ERR_NONE;
}

int servDG::cmd_setPulse(CArgument &arg)
{
    cmd_setWaveArg(arg);
    async(MSG_DG_WAVE,DG_WAVE_PULSE);
    //temp
    m_stBasic.bSourceEnable = false;
    defer(CMD_SERVICE_DEACTIVE);
    defer(CMD_SERVICE_ACTIVE);
    return ERR_NONE;
}

int servDG::cmd_setRamp(CArgument &arg)
{
    cmd_setWaveArg(arg);
    async(MSG_DG_WAVE,DG_WAVE_RAMP);
    //temp
    m_stBasic.bSourceEnable = true;
    async(MSG_APP_DG,m_stBasic.bSourceEnable);
    return ERR_NONE;
}

int servDG::cmd_setNoise(CArgument &arg)
{
    qlonglong val = 0;
    switch (arg.size()) {
    case 2:
        async(MSG_DG_OFFSET_LEVEL,0);
        arg.getVal(val,1);
        async(MSG_DG_OFFSET_LEVEL_SUB_1,val);
    case 1:
        async(MSG_DG_AMP_LEVEL,0);
        arg.getVal(val,0);
        async(MSG_DG_AMP_LEVEL_SUB_1,val);
    default:
        break;
    }
    async(MSG_DG_WAVE,DG_WAVE_NOISE);
    //temp
    //m_stBasic.bSourceEnable = true;
    //async(MSG_APP_DG,m_stBasic.bSourceEnable);

    m_stBasic.bSourceEnable = false;
    defer(CMD_SERVICE_DEACTIVE);
    defer(CMD_SERVICE_ACTIVE);
    return ERR_NONE;
}

int servDG::cmd_setDC(CArgument &arg)
{
    cmd_setWaveArg(arg);
    async(MSG_DG_WAVE,DG_WAVE_DC);
    //temp
    m_stBasic.bSourceEnable = true;
    async(MSG_APP_DG,m_stBasic.bSourceEnable);
    return ERR_NONE;
}

int servDG::cmd_setUser(CArgument &arg)
{
    cmd_setWaveArg(arg);
    async(MSG_DG_WAVE,DG_WAVE_ARB);
    //temp
    m_stBasic.bSourceEnable = true;
    async(MSG_APP_DG,m_stBasic.bSourceEnable);
    return ERR_NONE;
}

int servDG::cmd_setWaveArg(CArgument &arg)
{
    qlonglong val = 0;
    switch (arg.size()) {
    case 4:
        arg.getVal(val,3);
        async(MSG_DG_STARTPHASE,val);
    case 3:
        async(MSG_DG_OFFSET_LEVEL,0);
        arg.getVal(val,2);
        async(MSG_DG_OFFSET_LEVEL_SUB_1,val);
    case 2:
        async(MSG_DG_AMP_LEVEL,0);
        arg.getVal(val,1);
        async(MSG_DG_AMP_LEVEL_SUB_1,val);
    case 1:
    {
        async(MSG_DG_FREQ_PERIOD,0);
        qlonglong freq;
        arg.getVal(freq,0);
        async(MSG_DG_FREQ_PERIOD_SUB_1,freq);
    }
    default:
        break;
    }

    return ERR_NONE;
}


#if 0
void servDG::cmd_getWaveArg(CArgument &argout)
{ 
    argout.setVal(m_nWaveType,0);
    argout.setVal(m_stBasic.s64Freq_Menu,1);
    argout.setVal(m_nAmpLevel,2);
    if(m_nWaveType == DG_WAVE_DC)
    {
        argout.setVal(m_nDCOffset,3);
    }
    else
    {
        argout.setVal(m_nDGOffset,3);
    }
    argout.setVal(m_stBasic.u16Phase,4);
}
#else

static QString dgFormater(long long value,int en)
{
    QString strValue;

    if(value < 0)
    {
        if(value >  -pow(10,en))
        {
            strValue    = QString("%1").arg(value,en,10,QLatin1Char('0') );
            strValue.insert(1, "0.");
        }
        else
        {
            strValue    = QString("%1").arg(value);
            strValue.insert(strValue.count() - en, ".");
        }
    }
   else if(value < pow(10,en))
    {
        strValue    = QString("%1").arg(value,en,10,QLatin1Char('0') );
        strValue.insert(0, "0.");
    }
    else
    {
        strValue = QString("%1").arg(value);
        strValue.insert(strValue.count() - en , '.');
    }

    if(en < 6)
    {
        QByteArray s(6-en,'0');
        strValue.append(s );
    }

    return strValue;
}

void servDG::cmd_getWaveArg( CArgument &argout )
{
    QString strFreq;
    QString strAmpLevel;
    QString strOffset;
    QString strPhase;

    //!freq, Phase
    if( m_nWaveType == DG_WAVE_DC || m_nWaveType == DG_WAVE_NOISE )
    {
        strFreq  = "DEF";
        strPhase = "DEF";
    }
    else if( m_nWaveType == DG_WAVE_ARB )
    {
        strPhase = "DEF";
        strFreq  += dgFormater(m_stBasic.s64Freq_Menu,6);
    }
    else
    {
        strFreq  += dgFormater(m_stBasic.s64Freq_Menu,6);
        strPhase += dgFormater(m_stBasic.u16Phase,1);
    }

    //! AmpLevel
    strAmpLevel = (m_nWaveType == DG_WAVE_DC)? "DEF" : dgFormater(m_nAmplitude,6);
    //! Offset
    strOffset = dgFormater(m_nDGOffset,6);
    //! out
    argout.setVal(m_nWaveType,0);

    argout.setVal(QString("%1,%2,%3,%4")
                  .arg(strFreq).arg(strAmpLevel)
                  .arg(strOffset).arg(strPhase),1);
}

int servDG::cmd_setFreq(qlonglong freq)
{
    async(MSG_DG_FREQ_PERIOD,0);
    async(MSG_DG_FREQ_PERIOD_SUB_1,freq);
    return ERR_NONE;
}

long long servDG::cmd_getFreq()
{
    return m_stBasic.s64Freq_Menu;
}

int servDG::cmd_setAmp(qlonglong amp)
{
    async(MSG_DG_AMP_LEVEL,0);
    async(MSG_DG_AMP_LEVEL_SUB_1,amp);
    return ERR_NONE;
}

qint64 servDG::cmd_getAmp()
{
    return m_nAmplitude;
}

int servDG::cmd_setOffset(qlonglong offset)
{
    if( m_nWaveType != DG_WAVE_DC)
    {
        async(MSG_DG_OFFSET_LEVEL,0);
        async(MSG_DG_OFFSET_LEVEL_SUB_1,offset);
    }
    else
    {
        async(MSG_DG_OFFSET,offset);
    }
    return ERR_NONE;
}

qint64 servDG::cmd_getOffset()
{
    return m_nDGOffset;
}

int servDG::cmd_setStartPhase(qlonglong offset)
{
    if(offset >= 0  && offset <= 3600)
    {
        async(MSG_DG_STARTPHASE, offset);
    }
    return ERR_NONE;
}

qint64 servDG::cmd_getStartPhase()
{
    //bug 1849 by zy
    //return getPhase();
    return m_stBasic.u16Phase;
}

DsoErr servDG::setScpiSymmetry(int Symmetry)
{
    async( MSG_DG_SYMMETRY, Symmetry);
    return ERR_NONE;
}

int servDG::getScpiSymmetry()
{
    return getSymmetry();
}

DsoErr servDG::setScpiDutyCycle(int Duty)
{
    if(Duty >= 100 && Duty <= 900)
    {
        async( MSG_DG_DUTY_CYCLE, Duty);
    }
    else if(Duty < 100)
    {
        return ERR_OVER_LOW_RANGE;
    }
    else
    {
        return ERR_OVER_UPPER_RANGE;
    }
    return ERR_NONE;
}

int servDG::getScpiDutyCycle()
{
    return m_stBasic.u16Duty;
   // return getDutyCycle();
}

DsoErr servDG::setScpiModWave(int wave)
{
    async( MSG_DG_MOD_SHAPE, wave);
    return ERR_NONE;
}

int servDG::getScpiModWave()
{
    return getModWave();
}

DsoErr servDG::setScpiModDeviation(qint64 Dev)
{
    async( MSG_DG_MOD_FM_DEV, Dev);
    return ERR_NONE;
}

qint64 servDG::getScpiModDeviation()
{
    return getModDeviation();
}
#endif

int servDG::onEnterSubMenu(int menu)
{
    m_nMenuMSG = menu;

#if 1
    mUiAttr.setVisible(MSG_DG_IMPEDANCE, true);

    if(m_nWaveType == DG_WAVE_ARB)
    {
        if(MSG_DG_SETUP_NORM == menu )
        {
             mUiAttr.setVisible(MSG_DG_IMPEDANCE, false);
        }
    }
#endif

    if(MSG_DG_ARB_CREATE == menu)
    {
       // mUiAttr.setVisible(MSG_DG_ARB_POINTS, true);
        mUiAttr.setEnableVisible(MSG_DG_ARB_POINTS, true,true);
        //!创建任意波时，初始化点数恢复默认值
        async(MSG_DG_ARB_POINTS, 2);
        async(MSG_DG_ARB_CURRENT, 1);
    }

    if(MSG_DG_ARB_EDIT == menu)
    {
        //mUiAttr.setVisible(MSG_DG_ARB_POINTS, false);
        mUiAttr.setEnableVisible(MSG_DG_ARB_POINTS, false,false);
    }

    return ERR_NONE;
}

int servDG::onExitSubMenu(int menu)
{
    mUiAttr.setVisible(MSG_DG_IMPEDANCE, true);
    m_nExitMenu = menu;
    return ERR_NONE;
}
