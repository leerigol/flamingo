#ifndef SERVDG_H
#define SERVDG_H

#include<QWidget>
#include<QTimerEvent>

#include<QTimer>
#include<QTime>

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../../include/dsostd.h"
#include "../service_msg.h"
#include "../../baseclass/dsovert.h"

#include "../../module/funcsource/cmoduconfig.h"
#include "../../module/funcsource/funcBasicConfig.h"

#include "../../module/funcsource/funcBurstConfig.h"
#include "../../module/funcsource/funcSourceStartSet.h"
#include "../../module/funcsource/funcConstructWaveData.h"
#include "../../module/funcsource/funcCalSource.h"
#include "../../module/funcsource/funcSweepConfig.h"

#include "servdg_caldate.h"

#include "../../include/dsodbg.h"



//modified by hxh
#define DG_DEFAULT_CAL_FILE_NAME    QLatin1Literal("/rigol/default/dgCal.hex")
#define DG_CAL_FILE_NAME            QLatin1Literal("/rigol/data/dgCal.hex")

#define dg_cursora_update_timer_tmo  1000  //! ms
#define dg_cursora_update_timer_id    3

#define dg_cursorb_update_timer_tmo  1000  //! ms
#define dg_cursorb_update_timer_id    4

#define dg_arb_update_point_tmo  1000  //! ms
#define dg_arb_update_point_id    5

#define dg_ramp_update_wave_tmo  1000  //! ms
#define dg_ramp_update_wave_id    6
class servDG : public serviceExecutor, public ISerial
{

    Q_OBJECT

    DECLARE_CMD()
public:

    enum enumDGwave
    {
         DG_WAVE_FORM = 0,
         DG_WAVE_SINE = 0,
         DG_WAVE_SQUARE,
         DG_WAVE_RAMP,
         DG_WAVE_PULSE,
         DG_WAVE_DC,
         DG_WAVE_NOISE,

         DG_INTERWAVE_FORM,
         DG_WAVE_SINC = DG_INTERWAVE_FORM,
         DG_WAVE_EXP_RISE,
         DG_WAVE_EXP_FALL,
         DG_WAVE_ECG,
         DG_WAVE_GAUSS,
         DG_WAVE_LORENTZ,
         DG_WAVE_HAVERSINE,

         DG_WAVE_ARB,
     };

     enum queryCMD
     {
         MSG_SYS_RESERVED = 0,
         MSG_DG_Get_SubMenu,
         MSG_DG_Get_ExitMenu,
         MSG_DG_Get_WavTable,
         MSG_DG_Get_ChanData,
         MSG_DG_CHANGEPOINTS,
         //2017/12/15
//         MSG_DG_CHANGEFREQ,
         MSG_DG_LOADPOINTS,

         MSG_CMD_HAS_DG,
         //*scpi
         MSG_CMD_SET_NOISE,
         MSG_CMD_SET_PULSE,
         MSG_CMD_SET_RAMP,
         MSG_CMD_SET_SIN,
         MSG_CMD_SET_SQUARE,
         MSG_CMD_SET_DC,
         MSG_CMD_SET_USER,
         MSG_CMD_GET_APPLY,
         MSG_DG_CMD_FREQ,
         MSG_DG_CMD_AMP,
         MSG_DG_CMD_OFFSET,
         MSG_DG_CMD_PHASE,
         MSG_DG_CMD_SYMMETRY,
         MSG_DG_CMD_DCYCLE,
         MSG_DG_CMD_MODWAVE,
         MSG_DG_CMD_MODDEVI,

         //*scpi cal
         MSG_DG_CAL_FREQ,
         MSG_DG_CAL_PHASE,
         MSG_DG_CAL_OFFSET,
         MSG_DG_CAL_DC,
         MSG_DG_CAL_AMP,
         MSG_DG_CAL_SAVE,
         MSG_DG_CAL_DEFAULT,

     };

    servDG(QString name, ServiceId eId = E_SERVICE_ID_NONE );
public:
    int serialOut( CStream &stream, serialVersion &ver );
    int serialIn( CStream &stream, serialVersion ver );
    void rst();
    void init();
    int  startup();
    int  onActive(int a);
    int  onTimeOut(int);
    void   registerSpy();
public slots:
    void unHotPlug();
    void enHotPlug();

public:

    DsoErr              setSourceEn(bool en);
    bool                getSourceEn();

    DsoErr              setWaveType(enumDGwave);
    int                 getWaveType();


    DsoErr              selectFreqPeriod(int);
    int                 getSelectFreqPeriod();

    DsoErr              setFreqPeriod(qint64);
    qint64              getFreqPeriod();

    DsoErr              setDGPeriod(qint64);
    qint64              getDGPeriod();

    DsoErr              setDGFreq(qint64);
    qint64              getDGFreq();

    DsoErr              selectAmpLevel(int);
    int                 getSelectAmpLevel();

    DsoErr              setAmpLevel(qint64);
    qint64              getAmpLevel();

    DsoErr              setDGAmp(qint64 AMP);
    qint64              getDGAmp();

    DsoErr              setDGHVol(qint64 value);
    qint64              getDGHVol();

    DsoErr              selectOffsetLevel(int);
    int                 getSelectOffsetLevel();

    DsoErr              setOffsetLevel(qint64);
    qint64              getOffsetLevel();

    DsoErr              setDGOffset(qint64 Offset);
    qint64              getDGOffset();
    //int                 getDCOffset();

    DsoErr              setDGLVol(qint64 value);
    qint64              getDGLVol();

    DsoErr              setPhase(int Phase);
    int                 getPhase();

    DsoErr              setImpedance(bool);
    bool                getImpedance();

    DsoErr              setSync();

    DsoErr              setDutyCycle(int Duty);
    int                 getDutyCycle();

    DsoErr              setSymmetry(int Symmetry);
    int                 getSymmetry();

    DsoErr              setWorkType(enumDGWorkMode mode);
    enumDGWorkMode      getWorkType();

    //! for mode
    DsoErr              setModEnable(bool);
    bool                getModEnable();

    DsoErr              setModType(int);
    int                 getModType();

    DsoErr              setModWave(int);
    int                 getModWave();

    DsoErr              setModFreq(qint64 Freq);
    qint64              getModFreq();

    DsoErr              setModDepth(int Depth);
    int                 getModDepth();

    DsoErr              setModDeviation(qint64 Dev);
    qint64              getModDeviation();

    DsoErr              setModHopFreq(qint64 Freq);
    qint64              getModHopFreq();

    DsoErr              setModFSKPol(bool isNeg);
    bool                getModFSKPol();
    DsoErr              setModFSKFreq();

    //! for burst
    DsoErr              setBurstType(enumBurstType type);
    enumBurstType       getBurstType();

    DsoErr              setCycleNum(int num);
    int                 getCycleNum();

    DsoErr              setDelay(qlonglong delay);
    qlonglong           getDelay();

    DsoErr              setTrigSrc(enumTrigSRC src);
    enumTrigSRC         getTrigSrc();

    DsoErr              trigManual();

    DsoErr              setBurstPeriod(qlonglong period);
    qlonglong           getBurstPeriod();

    //! for sweep
    DsoErr              setSweepType(enumSweepType type);
    enumSweepType       getSweepType();

    DsoErr              setSweepTime(qlonglong time);
    qlonglong           getSweepTime();

    DsoErr              setReturnTime(qlonglong time);
    qlonglong           getReturnTime();

    DsoErr              setStartKeep(qlonglong time);
    qlonglong           getStartKeep();

    DsoErr              setEndKeep(qlonglong time);
    qlonglong           getEndKeep();

    DsoErr              setSweepStep(qlonglong step);
    qlonglong           getSweepStep();

    DsoErr              setStartFreq(qlonglong freq);
    qlonglong           getStartFreq();

    DsoErr              setEndFreq(qlonglong freq);
    qlonglong           getEndFreq();

    /*! FOR ARB API */
    DsoErr              setLoadChannel(int);
    int                 getLoadChannel();

    DsoErr              setRegion(bool);
    bool                getRegion();

    DsoErr              setCursorA(int);
    int                 getCursorA();

    DsoErr              setCursorB(int);
    int                 getCursorB();

    DsoErr              setCursorAB(int b);
    int                 getCursorAB();

    DsoErr              setPoints(int);
    int                 getPoints();

    DsoErr              setCurrent(int);
    int                 getCurrent();

    DsoErr              setLinear(bool);
    bool                getLinear();

    DsoErr              setZoom(bool);
    bool                getZoom();

    DsoErr              loadChan(int);
    DsoErr              insPoint(int);
    DsoErr              delPoint(int);
    DsoErr              apply(int);


    void                  sleep(unsigned int msec);
    DsoErr              loadStored(int);
    DsoErr              save(int);
    static  DsoErr      saveArb(QString& pathName);
    static  DsoErr      loadArb(QString& pathName);

    DsoErr              setVoltage(int);
    int                 getVoltage();

    DsoErr              setTime(qint64);
    qint64              getTime();

    DsoErr              limitRange(int& pos, int min, int max);
    //! sys msg
    int                 m_nMenuMSG;
    int                 getMenuMSG();
    int                 m_nExitMenu;
    int                 getExitMenuMsg();

    int                 onEnterSubMenu(int);
    int                 onExitSubMenu(int);

    void*               getEditTable();
    void                editToWaveTable();

    int                 setLoadPoints(int points);

    DsoErr           setHasDg();
    bool               getHasDg();
/************** scpi msg ***************/
    int                 cmd_setSin(CArgument &arg);
    int                 cmd_setSquare(CArgument &arg);
    int                 cmd_setPulse(CArgument &arg);
    int                 cmd_setRamp(CArgument &arg);
    int                 cmd_setNoise(CArgument &arg);
    int                 cmd_setDC(CArgument &arg);
    int                 cmd_setUser(CArgument &arg);
    int                 cmd_setWaveArg(CArgument &arg);
    void                cmd_getWaveArg(CArgument &argout);

    int                 cmd_setFreq(qlonglong freq);
    long long           cmd_getFreq();

    int                 cmd_setAmp(qlonglong amp);
    qint64              cmd_getAmp();

    int                 cmd_setOffset(qlonglong offset);
    qint64              cmd_getOffset();

    int                 cmd_setStartPhase(qlonglong offset);
    qint64              cmd_getStartPhase();

    DsoErr              setScpiSymmetry(int Symmetry);
    int                 getScpiSymmetry();

    DsoErr              setScpiDutyCycle(int Duty);
    int                 getScpiDutyCycle();

    DsoErr              setScpiModWave(int wave);
    int                 getScpiModWave();

    DsoErr              setScpiModDeviation(qint64 Dev);
    qint64              getScpiModDeviation();


private:
    //！cal cmd
    int getIndex(bool ATT1,bool ATT2);

    int cmd_getCalFreq ();
    int cmd_setCalFreq (int val);

    int cmd_getCalPhase();
    int cmd_setCalPhase(int val);

    //!Amp
    void cmd_getCalAmp(CArgument& argi,CArgument& argout);
    int  cmd_setCalAmp(CArgument& argi);

    //!offset
    void cmd_getCalOffset(CArgument& argi,CArgument& argout);
    int  cmd_setCalOffset(CArgument& argi);

    //!DC
    void cmd_getCalDC(CArgument& argi,CArgument& argout);
    int  cmd_setCalDC(CArgument& argi);

    //!cal
    int  cmd_calSave();
    int  cmd_calDefault();
    int  loadCalData();
    int  initLoadCalData();

    //!选件
    void set_Opt_DG();
private:
    qlonglong  getDGFreqStep(qlonglong currentVal, qlonglong minStep);
    qlonglong  getDGTimeStep(qlonglong currentVal, qlonglong minStep);
    qlonglong  getDGAmpStep(qlonglong currentVal, qlonglong minStep);
    int        getDGNumStep(int currentNum);
    int        SearchDGMaxMinVolt(QList<int> &m_EditPointsTable,
                                                       s32& s32MaxVolt,
                                                       s32& s32MinVolt);

    DsoErr              changeBasicMenuEn(enumDGwave waveType);
    DsoErr              changeMenuEn(enumDGWorkMode mode);

    DsoErr              configMode();

    bool                m_LicenseValid;

    enumDGwave          m_nWaveType;
    int                 m_nFreqPeriod;
    qint64              m_nPeriod;
    qint64              m_nFreq;

    qint64              m_nAmpLevel;
    qint64              m_nAmplitude;
    qint64              m_nHighVol;

    qint64              m_nOffsetLevel;
    qint64              m_nDGOffset;
    qint64              m_nLowVol;

   // bool                  m_bFlag;

    qint64              m_nPhase;
    qint64              m_nDuty;
    qint64              m_nSymmetry;

    bool                m_bImpedance;//false=HighZ, true=50ou;
    //!保存一份高阻状态下的参数值，便于切换以及往下配置
    qint64              m_nHimpAmp;
    qint64              m_nHimpOffset;
    qint64              m_nHimpHVol;
    qint64              m_nHimpLVol;

    enumDGWorkMode      m_nWorkMode; //! mode, burst,sweep

    //! for mode
    qint64              m_nHopFreq;
    int                 m_nAMDepth;

    /* FOR ARB API */
    QList<int>          m_EditPointsTable;//table
    u16*                m_arbWaveTable;

    //!用于save，标记当前正在save的服务
    static devFpgaChanEu  m_saveChan;

    int                 m_nChannel;
    int                 m_nCursorA;
    int                 m_nCursorB;
    int                 m_nCursorAB;

    int                 m_nEditPoints;
    int                 m_nCurrent;
    int                 m_nVoltage;

    bool                m_bRegion;
    bool                m_bLinear;
    bool                m_bZoom;
    bool                m_bSync;
    DsoWfm              m_ChanData;//channel data

    comBasicParaStru    m_stBasic;
    comModuParaStru     m_stModu;
    comBurstParaStru    m_stBurst;
    comSweepParaStru    m_stSweep;

    BasicConfig         cBasic;
    CModuConfig         cModu;
    BurstConfig         cBurst;
    SweepConfig         cSweep;

    bool                m_bTrigInit;//! burst 手动无限触发需要切换开关
    devFpgaChanEu       m_dgChan;

    int  DG_MIN_AMP;
    int  DG_MAX_AMP;
    const static int   DG_MAX_ARB_LEN = 16*1024;

    //! 用于校准，与库中的校准数据同步
    SourceCalParaStru* calParaStru;
    static dgCalData   m_dgCalData;


};
#endif // SERVDG_H
