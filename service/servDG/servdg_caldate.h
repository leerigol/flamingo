#ifndef SERVDG_CALDATE
#define SERVDG_CALDATE

#include "../../baseclass/checkedstream/ccheckedstream.h"
#include "../../module/funcsource/funcCalSource.h"

struct  dgCalData: public CCheckedStream
{
    SourceCalParaStru mPayload;
};

#endif // SERVDG_CALDATE

