#include "servdg.h"
#include "servdg_caldate.h"

//! 在校准的过程中，两个通道不可能同时打开。
dgCalData servDG::m_dgCalData;

int servDG::cmd_getCalFreq()
{

    return (int)calParaStru->u16VCTOXFactor;
}

int servDG::cmd_setCalFreq(int val)
{
    calParaStru->u16VCTOXFactor = (u16) val;
    async(MSG_DG_FREQ_PERIOD,0);
    async(MSG_DG_FREQ_PERIOD_SUB_1,m_stBasic.s64Freq_Menu);
    return ERR_NONE;
}

int servDG::cmd_getCalPhase()
{
    return (int)calParaStru->s16PhaseDev;
}

int servDG::cmd_setCalPhase(int val)
{
    calParaStru->s16PhaseDev = (s16) val;
    return ERR_NONE;
}

int servDG::getIndex(bool ATT1,bool ATT2)
{
    int index = 0;
    if(ATT1 && ATT2)   //on on
    {
        index = AMP_CAL_ATT_ON_AMP_ON;
    }
    if(ATT1 && !ATT2)  //on off
    {
        index = AMP_CAL_ATT_ON_AMP_OFF;
    }
    if(!ATT1 && ATT2)  //off on
    {
        index = AMP_CAL_ATT_OFF_AMP_ON;
    }
    if(!ATT1 && !ATT2)  //off off
    {
        index = AMP_CAL_ATT_OFF_AMP_OFF;
    }
    return index;
}

void servDG::cmd_getCalAmp(CArgument &argi, CArgument &argout)
{
    bool ATT1;
    bool ATT2;
    argi.getVal(ATT1,0);
    argi.getVal(ATT2,1);

    int index = 0;
    index = getIndex(ATT1,ATT2);

    if(m_dgChan == CH1_FPGA)
    {
        calParaStru = getSourCalPara();
        argout.setVal((int)calParaStru->s16CH1AmpCalFactor[index][SOURCE_CAL_FACTOR_A],0);
        argout.setVal((int)calParaStru->s16CH1AmpCalFactor[index][SOURCE_CAL_FACTOR_B],1);
//        qDebug()<<calParaStru->s16CH1AmpCalFactor[index][SOURCE_CAL_FACTOR_A];
//        qDebug()<<calParaStru->s16CH1AmpCalFactor[index][SOURCE_CAL_FACTOR_B];
    }
    if(m_dgChan == CH2_FPGA)
    {
        argout.setVal((int)calParaStru->s16CH2AmpCalFactor[index][SOURCE_CAL_FACTOR_A],0);
        argout.setVal((int)calParaStru->s16CH2AmpCalFactor[index][SOURCE_CAL_FACTOR_B],1);
    }
}

int servDG::cmd_setCalAmp(CArgument &argi)
{
    bool ATT1;
    bool ATT2;
    argi.getVal(ATT1,0);
    argi.getVal(ATT2,1);

    int index = 0;
    index = getIndex(ATT1,ATT2);

    qlonglong factorA = 0;
    qlonglong factorB = 0;
    factorA = argi[2].iVal;
    factorB = argi[3].iVal;
    if(m_dgChan == CH1_FPGA)
    {
        calParaStru->s16CH1AmpCalFactor[index][SOURCE_CAL_FACTOR_A]=(s16)factorA;
        calParaStru->s16CH1AmpCalFactor[index][SOURCE_CAL_FACTOR_B]=(s16)factorB;
        return ERR_NONE;
    }
    if(m_dgChan == CH2_FPGA)
    {
        calParaStru->s16CH2AmpCalFactor[index][SOURCE_CAL_FACTOR_A]=(s16)factorA;
        calParaStru->s16CH2AmpCalFactor[index][SOURCE_CAL_FACTOR_B]=(s16)factorB;
        return ERR_NONE;
    }
    return ERR_INVALID_CONFIG;
}

void servDG::cmd_getCalOffset(CArgument& argi,CArgument& argout)
{
    qDebug()<<__FUNCTION__;
    bool ATT1;
    bool ATT2;
    argi.getVal(ATT1,0);
    argi.getVal(ATT2,1);

    int index = 0;
    index = getIndex(ATT1,ATT2);

    if(m_dgChan == CH1_FPGA)
    {
        argout.setVal((int)calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_A],0);
        argout.setVal((int)calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_B],1);
    }
    if(m_dgChan == CH2_FPGA)
    {
        argout.setVal((int)calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_A],0);
        argout.setVal((int)calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_B],1);
    }
}

int servDG::cmd_setCalOffset(CArgument &argi)
{
    bool ATT1;
    bool ATT2;
    argi.getVal(ATT1,0);
    argi.getVal(ATT2,1);

    int index = 0;
    index = getIndex(ATT1,ATT2);

    qlonglong factorA = 0;
    qlonglong factorB = 0;
    qDebug()<<__FUNCTION__;
    factorA = argi[2].iVal;
    factorB = argi[3].iVal;

    if(m_dgChan == CH1_FPGA)
    {
        calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_A] = (s16)factorA;
        calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_B] = (s16)factorB;
        return ERR_NONE;
    }
    if(m_dgChan == CH2_FPGA)
    {
        calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_A] = (s16)factorA;
        calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_B] = (s16)factorB;
        return ERR_NONE;
    }
    return ERR_INVALID_CONFIG;
}


void servDG::cmd_getCalDC(CArgument& argi,CArgument& argout)
{
    bool ATT1;
    argi.getVal(ATT1,0);

    int index = 0;
    if(ATT1)
    {
        index = DC_CAL_ATT_OFF_AMP_ON;
    }
    else
    {
        index = DC_CAL_ATT_OFF_AMP_OFF;
    }

    if(m_dgChan == CH1_FPGA)
    {
        argout.setVal((int)calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_A],0);
        argout.setVal((int)calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_B],1);
    }
    if(m_dgChan == CH2_FPGA)
    {
        argout.setVal((int)calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_A],0);
        argout.setVal((int)calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_B],1);
    }
}

int servDG::cmd_setCalDC(CArgument &argi)
{
    bool ATT1;
    argi.getVal(ATT1,0);

    qlonglong factorA = 0;
    qlonglong factorB = 0;
    factorA = argi[1].iVal;
    factorB = argi[2].iVal;

    int index = 0;
    if(ATT1)
    {
        index = DC_CAL_ATT_OFF_AMP_ON;
    }
    else
    {
        index = DC_CAL_ATT_OFF_AMP_OFF;
    }

    if(m_dgChan == CH1_FPGA)
    {
        calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_A]=(s16)factorA;
        calParaStru->s16CH1OffsetCalFactor[index][SOURCE_CAL_FACTOR_B]=(s16)factorB;
        return ERR_NONE;
    }
    if(m_dgChan == CH2_FPGA)
    {
        calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_A]=(s16)factorA;
        calParaStru->s16CH2OffsetCalFactor[index][SOURCE_CAL_FACTOR_B]=(s16)factorB;
        return ERR_NONE;
    }
    return ERR_INVALID_CONFIG;
}


//!scpi     :source1:cal:savedata
int servDG::cmd_calSave()
{
    LOG_DBG();
    m_dgCalData.mPayload = *calParaStru;
    int size = sizeof(m_dgCalData.mPayload);
    if ( size != m_dgCalData.save( DG_CAL_FILE_NAME, &m_dgCalData.mPayload, size ))
    {
        return ERR_CAL_WRITE_FAIL;
    }
    return ERR_NONE;
}

int servDG::cmd_calDefault()
{
    funcSetSourceCalParaDefault();
    return ERR_NONE;
}

int servDG::loadCalData()
{
    dgCalData *pCalData;

    pCalData = new dgCalData();
    if ( NULL == pCalData )
    { return ERR_CAL_READ_FAIL; }

    int size = sizeof(pCalData->mPayload);
    if ( pCalData->load( DG_CAL_FILE_NAME, &pCalData->mPayload, size ) != size )
    {
        if( pCalData->load( DG_DEFAULT_CAL_FILE_NAME, &pCalData->mPayload, size ) != size )
        {
            delete pCalData;
            return ERR_CAL_READ_FAIL;
        }
    }

    //! export
    m_dgCalData = *pCalData;
    delete pCalData;

    return ERR_NONE;
}
