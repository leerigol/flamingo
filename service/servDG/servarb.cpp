#include "servdg.h"
#include "../servgui/servgui.h"
#include "../servstorage/servstorage.h"
#include "../servch/servch.h"
#include "../../baseclass/resample/reSample.h"
devFpgaChanEu  servDG::m_saveChan = CH1_FPGA;

DsoErr servDG::setLoadChannel(int b)
{
    m_nChannel = b;

//    qDebug() << b << "channel";
    return ERR_NONE;
}

int servDG::getLoadChannel()
{
    return m_nChannel;
}

DsoErr servDG::setRegion(bool b)
{
   m_bRegion = b;
   if(b == 1)  //m_Region: Cursor
   {
       int measRA = getCursorA();
       servGui::showHLine( "measRA", true, Qt::white, measRA,0, 2,480);
      startTimer(dg_cursora_update_timer_tmo, dg_cursora_update_timer_id, timer_single);

       int measRB = getCursorB();
       servGui::showHLine( "measRB", true, Qt::white, measRB,0, 2,480);
       startTimer(dg_cursorb_update_timer_tmo, dg_cursorb_update_timer_id, timer_single);
   }
   else
   {
       servGui::showHLine( "measRA", false);
       servGui::showHLine( "measRB", false);
   }
   return ERR_NONE;
}

/**
 * @brief servDG::getRegion
 * @return false=Screen, true=cursor
 */
bool servDG::getRegion()
{
    return m_bRegion;
}

DsoErr servDG::setCursorA(int b)
{
    m_nCursorA = b;
    DsoErr err = limitRange(m_nCursorA, 0, m_nCursorB - 1);
    servGui::showHLine( "measRA", true, Qt::white, m_nCursorA,0, 2,480);
    startTimer(dg_cursora_update_timer_tmo, dg_cursora_update_timer_id, timer_single);

    servGui::showHLine( "measRB", true, Qt::white, m_nCursorB, 0, 2,480);
    startTimer(dg_cursorb_update_timer_tmo, dg_cursorb_update_timer_id, timer_single);
    return err;
}

int servDG::getCursorA()
{
    QString str;
    horiAttr attr;
    float val;

    attr = getHoriAttr( chan1, horizontal_view_main );
    val = (m_nCursorA - attr.gnd) * attr.inc;

    gui_fmt::CGuiFormatter::format( str, val );
    str = str + "s";

    mUiAttr.setOutStr( MSG_DG_ARB_CURSORA, str );
    return m_nCursorA;
}

DsoErr servDG::setCursorB(int b)
{
    m_nCursorB = b;

    servGui::showHLine( "measRA", true, Qt::white, m_nCursorA,0, 2,480);
    startTimer(dg_cursora_update_timer_tmo, dg_cursora_update_timer_id, timer_single);

    DsoErr err = limitRange(m_nCursorB, m_nCursorA +1, wave_width - 1);
    servGui::showHLine( "measRB", true, Qt::white, m_nCursorB, 0, 2,480);
    startTimer(dg_cursorb_update_timer_tmo, dg_cursorb_update_timer_id, timer_single);
    return err;
}

int servDG::getCursorB()
{
    QString str;
    horiAttr attr;
    float val;

    attr = getHoriAttr( chan1, horizontal_view_main );
    val = (m_nCursorB - attr.gnd) * attr.inc;

    gui_fmt::CGuiFormatter::format( str, val );
    str = str + "s";

    mUiAttr.setOutStr( MSG_DG_ARB_CURSORB, str );
    return m_nCursorB;
}

DsoErr servDG::setCursorAB(int b)
{
    m_nCursorAB = b;

    int dgRA = getCursorA()+b;  
    int dgRB = getCursorB()+b;

    DsoErr err_left = limitRange(dgRA, 0, getCursorB());
    DsoErr err_right = limitRange(dgRB, getCursorA(), wave_width - 1);
    if((err_left == ERR_NONE) && (err_right == ERR_NONE))
    {
        async(MSG_DG_ARB_CURSORA, dgRA);
        async(MSG_DG_ARB_CURSORB, dgRB);

        startTimer(dg_cursora_update_timer_tmo, dg_cursora_update_timer_id, timer_single);
        startTimer(dg_cursorb_update_timer_tmo, dg_cursorb_update_timer_id, timer_single);
    }
    else
    {
        async(MSG_DG_ARB_CURSORA, getCursorA());
        async(MSG_DG_ARB_CURSORB, getCursorB());
        startTimer(dg_cursora_update_timer_tmo, dg_cursora_update_timer_id, timer_single);
        startTimer(dg_cursorb_update_timer_tmo, dg_cursorb_update_timer_id, timer_single);
        if(err_left != ERR_NONE)
        {
             return err_left;
        }
        else
        {
            return err_right;
        }
    }
    //qDebug() << "CursorAB: " << m_nCursorAB;
    return ERR_NONE;
}

int servDG::getCursorAB()
{
    return m_nCursorAB;
}

DsoErr servDG::setPoints(int b)
{
    m_nEditPoints = b;
    m_EditPointsTable.clear();
    m_EditPointsTable.reserve(m_nEditPoints);
    for(int i = 0;i < m_nEditPoints;i++)
    {
        m_EditPointsTable.append(0);
    }

    defer(MSG_DG_CHANGEPOINTS);

    //defer(MSG_DG_CHANGEFREQ);
    return ERR_NONE;
}

int servDG::getPoints()
{
    setuiStep( 1 );
    setuiZVal( 2 );
    setuiMinVal(2 );
    setuiMaxVal( 16*1024 );

    setuiBase(1, E_0);

    return m_nEditPoints;
}

DsoErr servDG::setLinear(bool b)
{
   m_bLinear = b;
   return ERR_NONE;
}

/**
 * @brief servDG::getRegion
 * @return false=off, true=open
 */
bool servDG::getLinear()
{
    return m_bLinear;
}

DsoErr servDG::setCurrent(int b)
{
    if(b < 1)
    {
        return ERR_OVER_LOW_RANGE;
    }
    else if(b > m_nEditPoints)
    {
        return ERR_OVER_UPPER_RANGE;
    }
    m_nCurrent = b;
    async(MSG_DG_ARB_VOLTAGE, m_EditPointsTable[m_nCurrent-1]);

    return ERR_NONE;
}

int servDG::getCurrent()
{
    setuiStep( 1 );
    setuiZVal( 1 );
    setuiMinVal( 1 );
    setuiMaxVal( m_nEditPoints );

    setuiBase(1, E_0);
    return m_nCurrent;
}

DsoErr servDG::setVoltage(int b)
{
#if 1
    Q_ASSERT(m_nCurrent<=DG_MAX_ARB_LEN && m_nCurrent >= 1);
#else
    Q_ASSERT(m_nCurrent<DG_MAX_ARB_LEN && m_nCurrent >= 1);
#endif
    m_EditPointsTable[m_nCurrent-1] = b;

    return ERR_NONE;
}

int servDG::getVoltage()
{
    Q_ASSERT(m_nCurrent <= m_nEditPoints  && m_nCurrent >= 1);

    setuiStep( getDGAmpStep(m_EditPointsTable[m_nCurrent-1], mv(1)) );
    setuiZVal(vv(0));
    setuiMinVal( vv(-2.5) );
    setuiMaxVal( vv(2.5) );
    setuiUnit(Unit_V);
    setuiBase(1, E_N6);
    setuiPostStr("V");
    return m_EditPointsTable[m_nCurrent-1];
}

DsoErr servDG::setZoom(bool b)
{
   m_bZoom = b;
   return ERR_NONE;
}

/**
 * @brief servDG::getRegion
 * @return false=off, true=open
 */
bool servDG::getZoom()
{
    return m_bZoom;
}

DsoErr servDG::insPoint(int)
{
      if(m_nEditPoints < WAVE_SIZE)
      {
#if 0
    if(m_nCurrent <= m_nEditPoints)
    {
        m_EditPointsTable.insert(m_nCurrent,m_nVoltage);
        m_nCurrent++;
        m_nVoltage = 0;
        defer(MSG_DG_CHANGEPOINTS);
        //重復調用APP中的onCurrent，可以在APP的setCurrent中進行一下過濾
        async(MSG_DG_ARB_CURRENT,m_nCurrent);
        async(MSG_DG_ARB_VOLTAGE,m_nVoltage);
    }
#else
    m_EditPointsTable.insert(m_nCurrent,m_nVoltage);
    m_nCurrent++;
    m_nEditPoints++;
    m_nVoltage = 0;
    defer(MSG_DG_CHANGEPOINTS);
    //重復調用APP中的onCurrent，可以在APP的setCurrent中進行一下過濾
    async(MSG_DG_ARB_CURRENT,m_nCurrent);
    async(MSG_DG_ARB_VOLTAGE,m_nVoltage);
#endif
      }
    return ERR_NONE;
}

DsoErr servDG::delPoint(int)
{
    if(m_nEditPoints > 2)
    {
        m_nEditPoints--;
        m_EditPointsTable.removeAt(m_nCurrent-1);
        defer(MSG_DG_CHANGEPOINTS);
        if(m_nCurrent > 1)
        {
            m_nCurrent--;
        }
#if 0
        async(MSG_DG_ARB_CURRENT,m_nCurrent);
#else
        startTimer(dg_arb_update_point_tmo, dg_arb_update_point_id, timer_single);
#endif
    }
    return ERR_NONE;
}

void servDG:: sleep(unsigned int msec)
{
    QTime dieTime = QTime::currentTime().addMSecs(msec);
    while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void servDG::editToWaveTable()
{
    m_arbWaveTable = funcGetArbWavePointer(m_dgChan);
    float XStep = WAVE_SIZE*1.0/m_EditPointsTable.size();

    float f32StartPoint = 0.0;

    int s32StartPoint = 0;
    int s32EndPoint = 0;

    int currentAmp = 0;
    int nextAmp = 0;
#if  1
    s32 s32MaxVolt ;
    s32 s32MinVolt;
   // servDG::
    SearchDGMaxMinVolt( m_EditPointsTable,
                                            s32MaxVolt,
                                            s32MinVolt);

   // qDebug()<<" s32MaxVolt = "<< s32MaxVolt << "s32MinVolt = " << s32MinVolt;
    s32 s32Amp = s32MaxVolt - s32MinVolt;
    if(s32Amp == 0)
        s32Amp = 1;
    async(MSG_DG_AMP_LEVEL_SUB_1,s32Amp);
#endif
    int point_k = 0;
    for(int i = 0;i < m_EditPointsTable.size()-1;i++)
    {
        s32StartPoint = floor(f32StartPoint);
        s32EndPoint = floor(f32StartPoint + XStep);
#if 1
      //  qDebug()<<"s32MaxVolt" << s32MaxVolt << "s32MinVolt"<<s32MinVolt << "s32Amp" << s32Amp ;
        //!处理电压数值为0或者幅度为0的情况
        if((s32MaxVolt - s32MinVolt) == 0)
        {
             currentAmp  = (int)((float)(m_EditPointsTable.at(i)/5000000.0) * WAVE_AMP + WAVE_AMP/2);
             nextAmp  = (int)((float)(m_EditPointsTable.at(i+1)/5000000.0) * WAVE_AMP + WAVE_AMP/2);
        }
        else
        {
              currentAmp = (qlonglong)(m_EditPointsTable.at(i) - s32MinVolt)*WAVE_AMP/s32Amp;
              nextAmp = (qlonglong)(m_EditPointsTable.at(i+1) - s32MinVolt)*WAVE_AMP/s32Amp;
        }

        //qDebug() << "currentAmp = " << currentAmp << "nextAmp = " << nextAmp;
#else
        currentAmp = (qlonglong)(m_EditPointsTable.at(i) - m_nLowVol)*WAVE_AMP/m_nAmplitude;
        nextAmp = (qlonglong)(m_EditPointsTable.at(i+1) - m_nLowVol)*WAVE_AMP/m_nAmplitude;
#endif
        float hStep = ((float)(nextAmp - currentAmp))/(s32EndPoint - s32StartPoint);
        if(m_bLinear)
        {
            Q_ASSERT(point_k < WAVE_SIZE);

            for(point_k = s32StartPoint;point_k < s32EndPoint;point_k++)
            {
                *(m_arbWaveTable + point_k) = (u16) (currentAmp + (point_k - s32StartPoint) * hStep);
            }
        }
        else
        {
            Q_ASSERT(point_k < WAVE_SIZE);

            for(point_k = s32StartPoint;point_k < s32EndPoint;point_k++)
            {
                *(m_arbWaveTable + point_k) = (u16) currentAmp;
            }
        }
        f32StartPoint = f32StartPoint +XStep;
      //  qDebug()<<"i = " <<  i << currentAmp<<"currentAmp " ;
    }

    //qDebug() << "point_k = " << point_k;
    //! the last section
#if 1
    if(point_k < (WAVE_SIZE))
#else
    if(point_k < (WAVE_SIZE-1))
#endif
    {
        s32StartPoint = floor(f32StartPoint);
        s32EndPoint = WAVE_SIZE;

#if 1
        //!处理电压数值为0或者幅度为0的情况
        if((s32MaxVolt - s32MinVolt) == 0)
        {
            currentAmp  = (int)((float)(m_EditPointsTable.at(m_EditPointsTable.size()-1)/5000000.0) * WAVE_AMP + WAVE_AMP/2);
            nextAmp  = (int)((float)(m_EditPointsTable.at(0)/5000000.0) * WAVE_AMP + WAVE_AMP/2);
        }
        else
        {
              currentAmp = (qlonglong)(m_EditPointsTable.at(m_EditPointsTable.size()-1) - s32MinVolt)*WAVE_AMP/s32Amp;
              nextAmp = (qlonglong)(m_EditPointsTable.at(0) - s32MinVolt)*WAVE_AMP/s32Amp;
        }
       // qDebug() << "currentAmp = " << currentAmp << "nextAmp = " << nextAmp;
#else
        currentAmp = (qlonglong)(m_EditPointsTable.at(m_EditPointsTable.size()-1) - m_nLowVol)
                *WAVE_AMP/m_nAmplitude;
        nextAmp = (qlonglong)(m_EditPointsTable.at(0) - m_nLowVol)*WAVE_AMP/m_nAmplitude;
#endif

        float hStep = ((float)(nextAmp - currentAmp))/(s32EndPoint - s32StartPoint);
        if(m_bLinear)
        {
            Q_ASSERT(point_k < WAVE_SIZE);
            for(point_k = s32StartPoint;point_k < s32EndPoint;point_k++)
            {
                *(m_arbWaveTable + point_k) = (u16) (currentAmp + (point_k - s32StartPoint) * hStep);
//                currentAmp = currentAmp + hStep;
            }
        }
        else
        {
            Q_ASSERT(point_k < WAVE_SIZE);
            for(point_k = s32StartPoint;point_k < s32EndPoint;point_k++)
            {
                *(m_arbWaveTable + point_k) = (u16) currentAmp;
            }
        }
    }

}

int servDG::setLoadPoints(int points)
{
    m_nEditPoints = points;
    //! 加载信号后改变总点数的同时讲current置1
    m_nCurrent = 1;
    return 0;
}

DsoErr servDG::setHasDg()
{
    bool en =  sysCheckLicense(OPT_DG);
    if(!en)
    {
        async(MSG_APP_DG,false);
        defer(CMD_SERVICE_DEACTIVE);
    }

    //! [dba+ 2018-03-23]
    sysScpiFilterServive(getName(), (!en) );

    return ERR_NONE;
}

bool servDG::getHasDg()
{
    return sysHasDG();
}

DsoErr servDG::apply(int)
{
    editToWaveTable();
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
    return ERR_NONE;
}

void* servDG::getEditTable()
{
    return &m_EditPointsTable;
}

DsoErr servDG::loadChan(int)
{
#if 1
    dsoVert::getCH((DsoType::Chan)m_nChannel)->getTrace(m_ChanData);
 #else
    dsoVert::getCH(chan1)->getTrace(m_ChanData);
#endif
#if 1
    float data[WAVE_SIZE];
    float* pData = data;
    reSampleExt( m_ChanData.size(),
                 WAVE_SIZE,
                  m_ChanData.getValue(),
                  pData,
                  e_sample_norm | e_sample_head | e_sample_tail);
    //qDebug()    <<   *pData << " pData";
    m_nEditPoints = WAVE_SIZE;
    m_EditPointsTable.clear();
    m_EditPointsTable.reserve(m_nEditPoints);

    int startIndex = 0;
    int endIndex = WAVE_SIZE;
#else
    float* pData = m_ChanData.getValue();
    m_nEditPoints = m_ChanData.size();

    m_EditPointsTable.clear();
    m_EditPointsTable.reserve(m_nEditPoints);

    int startIndex = 0;
    int endIndex = m_ChanData.size();
#endif

    if(m_bRegion)
    {
        startIndex = m_nCursorA*endIndex/1000;
        endIndex = m_nCursorB*endIndex/1000;
    }

    for(int i = startIndex; i < endIndex; i++)
    {       
        m_EditPointsTable<<1e6 * (*pData); //float to int
        pData++;
    }

    m_nEditPoints = m_EditPointsTable.size();
    m_nCurrent = 1;

    editToWaveTable();
    cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
    return ERR_NONE;
}

DsoErr servDG::save(int)
{
    m_saveChan = m_dgChan;
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_SAVE,
                      servStorage::FUNC_SAVE_ARB,
                      1, this->getId());
    startIntent( serv_name_storage, intent );
    return ERR_NONE;
}

//! 从load中调用这个静态函数
DsoErr servDG::saveArb(QString& pathName)
{
    int f;
   // qDebug() << "save arb";
    serviceExecutor::query( serv_name_storage, MSG_STORAGE_FILETYPE, f );
    QList<int>* editPtr;
    if(f == servFile::FILETYPE_ARB)
    {
        QString serv_name = (m_saveChan == CH1_FPGA)?
                             serv_name_source1:serv_name_source1;

        void* p;
        serviceExecutor::query( serv_name,MSG_DG_Get_WavTable, p);
        editPtr = (QList<int>*) p;

        qlonglong temp;
        serviceExecutor::query( serv_name,MSG_DG_FREQ_PERIOD_SUB_1,temp);
        int isPeriod;
        serviceExecutor::query( serv_name,MSG_DG_FREQ_PERIOD,isPeriod);
        qlonglong period = isPeriod? temp:(1e18/temp);

        if(editPtr == NULL)
        {
            return ERR_FILE_SAVE_FAIL;
        }
        QFile file(pathName);
        if( file.open(QIODevice::WriteOnly) )
        {
            QTextStream out( &file );

            int pointNum = editPtr->size();
            qlonglong step = period/pointNum;

            out<<"x-axis"<<","<<"WaveGen-ARB"<<"\n";
            out<<"second"<<","<<"Volt"<<"\n";

            for(int i = 0;i < pointNum;i++)
            {
                QString time = QString::number((double)step * i/1e12,'E',6);
                QString vol = QString::number((double)editPtr->at(i)/1e6,'E',4);
                out<<time<<","<<vol<<"\n";
            }
            //!保持与加载一致，最后有一个空行
            out<<"  ";
            file.flush();
            file.close();
            return ERR_NONE;
        }
    }
    return ERR_FILE_SAVE_FAIL;
}
DsoErr servDG::loadStored(int)
{
    m_saveChan = m_dgChan;
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_LOAD,
                      servStorage::FUNC_LOAD_ARB,1,
                      getId());

    startIntent( serv_name_storage, intent );
    return ERR_NONE;
}

DsoErr servDG::loadArb(QString& pathName)
{
    int ret = ERR_NONE;
    int f;
    //qDebug() << "load arb";
    serviceExecutor::query( serv_name_storage, MSG_STORAGE_FILETYPE, f );
    QList<int>* editPtr;
    if(f == servFile::FILETYPE_ARB)
    {
        QString serv_name = (m_saveChan == CH1_FPGA)?
                             serv_name_source1:serv_name_source2;

        void* p;
        serviceExecutor::query(serv_name,MSG_DG_Get_WavTable, p);
        editPtr = (QList<int>*) p;

        if(editPtr == NULL)
        {
            Q_ASSERT(editPtr != NULL);
            return ERR_NULL_ADDRESSEE;
        }
        QFile file(pathName);
        if( file.open(QIODevice::ReadOnly) )
        {
            QTextStream in( &file );

            QStringList lineList = in.readAll().split("\n");

            if(lineList.size() < 3)
            {
                return ERR_FILE_SAVE_FAIL;
            }

            int pointNum = lineList.size()-3;

            //! 单独提供一个设置pointsNum的消息是为了防止给app中发送刷新的消息
            serviceExecutor::post(findService(serv_name)->getId(),
                                  servDG::MSG_DG_LOADPOINTS,
                                  pointNum);

            editPtr->clear();
            //!最后有一行空格
            lineList.removeLast();
            for(int i = 2; i < lineList.size();i++)
            {
                QStringList tempLine = lineList.at(i).split(",");
                double voltemp = tempLine.at(1).toDouble();
                int vol = vv(voltemp);
                if(vol > vv(2.5))
                {
                    vol = vv(2.5);
                    ret = ERR_INVALID_INPUT;
                }
                if(vol < vv(-2.5))
                {
                    vol = vv(-2.5);
                    ret = ERR_INVALID_INPUT;
                }

                editPtr->append(vol);
            }
#if 0
            QStringList lineLast = lineList.back().split(",");
            double timeTemp = lineLast.at(0).toDouble();
         //   qlonglong time = timeTemp/(pointNum-1)*pointNum*s_unit;

             qlonglong time = timeTemp*s_unit;

            serviceExecutor::post(findService(serv_name)->getId(),
                                  MSG_DG_FREQ_PERIOD_SUB_1 ,time);
#endif
            file.close();


          serviceExecutor::post(findService(serv_name)->getId(),
                              MSG_DG_ARB_VOLTAGE ,editPtr->at(0));


          serviceExecutor::post(findService(serv_name)->getId(),
                              MSG_DG_ARB_DONE ,0);

    }
        return ERR_NONE;
    }
    return (DsoErr)ret;
}


DsoErr servDG::limitRange(int& pos, int min, int max)
{
    if(pos > max)
    {
        pos = max;
        return ERR_OVER_UPPER_RANGE;
    }
    else if(pos < min)
    {
        pos = min;
        return ERR_OVER_LOW_RANGE;
    }
    return ERR_NONE;
}

