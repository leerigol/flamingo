#include "servdg.h"
#include "../servHotplug/servhotplug.h"


#define dg_hotplug_update_timer_tmo  1000  //! ms
#define dg_hotplug_update_timer_id    2


qlonglong servDG::getDGFreqStep(qlonglong currentVal, qlonglong minStep)
{
    qlonglong step = getFreqStep(currentVal);
    step = step > minStep?step:minStep;
    return step;
}

qlonglong servDG::getDGTimeStep(qlonglong currentVal, qlonglong minStep)
{
    qlonglong step = getTimeStep(currentVal);
    step = step > minStep?step:minStep;
    return step;
}

qlonglong servDG::getDGAmpStep(qlonglong currentVal, qlonglong minStep)
{
    qlonglong step = getVoltStep(currentVal);
    step = step > minStep?step:minStep;
    return step;
}

int servDG::getDGNumStep(int currentNum)
{
    int step = getNumStep(currentNum);
    step = step > 1?step:1;
    return step;
}
int servDG::SearchDGMaxMinVolt(QList<int>& m_EditPointsTable,
                                                   s32 &s32MaxVolt,
                                                   s32 &s32MinVolt)
{
        u16 i;
       // qDebug()<<__FUNCTION__;
        //qDebug()<<"edit size"<<m_EditPointsTable.size();

      #if 0
         for(i = 0; i < m_EditPointsTable.size(); i++)
         {
            if( m_EditPointsTable.at(i) > )
            {

            }

              if( m_EditPointsTable.at(i) < )
              {

              }
         }
       #endif
        s32MaxVolt = m_EditPointsTable.at(0);
        s32MinVolt = m_EditPointsTable.at(0);
        for(i = 0; i < m_EditPointsTable.size(); i++)
        {
            if(m_EditPointsTable.at(i) >  s32MaxVolt)
            {
                s32MaxVolt = m_EditPointsTable.at(i);
            }

            if(m_EditPointsTable.at(i) < s32MinVolt)
            {
                s32MinVolt = m_EditPointsTable.at(i);
            }

        }

//        qDebug()<<" s32MaxVolt = "<< s32MaxVolt << "s32MinVolt = " << s32MinVolt;
        return 0;
}

DsoErr servDG::changeBasicMenuEn(servDG::enumDGwave waveType)
{
    if(waveType == DG_WAVE_RAMP)
    {
        mUiAttr.setVisible(MSG_DG_SYMMETRY, true);
    }
    else
    {
        mUiAttr.setVisible(MSG_DG_SYMMETRY, false);
    }
    if(waveType == DG_WAVE_PULSE)
    {
        mUiAttr.setVisible(MSG_DG_DUTY_CYCLE, true);
    }
    else
    {
        mUiAttr.setVisible(MSG_DG_DUTY_CYCLE, false);
    }
    return ERR_NONE;
}

DsoErr servDG::changeMenuEn(enumDGWorkMode mode)
{
    if(mode == DG_SWEEP)
    {
        LOG_DBG();
        setEnable(MSG_DG_FREQ_PERIOD_SUB_1, false);
    }
    else
    {
        setEnable(MSG_DG_FREQ_PERIOD_SUB_1, true);
    }
    return ERR_NONE;
}

#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

#define SERIAL_OP   stream.write
//!还要将库中存储的数据进行序列化存储
int  servDG::serialOut( CStream &stream, serialVersion &ver )
{
    ver = mVersion;
    stream.write(QStringLiteral("m_nWaveType"), (quint8)m_nWaveType);
    stream.write(QStringLiteral("m_stBasic.s64Freq_Menu"),  m_stBasic.s64Freq_Menu);
    stream.write(QStringLiteral("m_nPeriod"), m_nPeriod);

    stream.write(QStringLiteral("m_nAmpLevel"), m_nAmpLevel);
    stream.write(QStringLiteral("m_nAmplitude"),m_nAmplitude);
    stream.write(QStringLiteral("m_nHighVol"), m_nHighVol);

    stream.write(QStringLiteral("m_nOffsetLevel"),m_nOffsetLevel);
    stream.write(QStringLiteral("m_nDGOffset"),m_nDGOffset);
    stream.write(QStringLiteral("m_nLowVol"),m_nLowVol);
    stream.write(QStringLiteral("m_stBasic.u32Amp"),m_stBasic.u32Amp);
    stream.write(QStringLiteral("m_nHimpHVol"),m_nHimpHVol);
    stream.write(QStringLiteral("m_stBasic.s32Offset"),m_stBasic.s32Offset);
    stream.write(QStringLiteral("m_nHimpLVol"),m_nHimpLVol);

    stream.write(QStringLiteral("m_stModu.bModEnable"),m_stModu.bModEnable);
    stream.write(QStringLiteral("m_stModu.enModType"),(quint8)m_stModu.enModType);
    stream.write(QStringLiteral("m_stModu.enModWave"),(quint8)m_stModu.enModWave);
    stream.write(QStringLiteral("m_stModu.u64ModFreq"),m_stModu.u64ModFreq);
    stream.write(QStringLiteral("m_stModu.u64FMDev"),m_stModu.u64FMDev);
    stream.write(QStringLiteral("m_stModu.u8AMDepth"),m_stModu.u8AMDepth);
    stream.write(QStringLiteral("m_stModu.u64HopFreq"),m_stModu.u64HopFreq);
    stream.write(QStringLiteral("m_stModu.isPolarity"),  m_stModu.isPolarity);
    stream.write(QStringLiteral("m_bImpedance"),m_bImpedance);
    setImpedance(m_bImpedance);

    stream.write(QStringLiteral("m_stBasic.u16Phase"),m_stBasic.u16Phase);
    stream.write(QStringLiteral("m_stBasic.u16Duty"),m_stBasic.u16Duty);
    stream.write(QStringLiteral("m_stBasic.u16Symmetry"),m_stBasic.u16Symmetry);
    return ERR_NONE;
}

#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

#define SERIAL_OP   stream.read
#define SERIAL_OP_C( a, b, c, type )   stream.read( a, c ); \
                                       memset( &b, 0, sizeof(b));\
                                       memcpy( &b, &c, sizeof(type) );
int  servDG::serialIn( CStream &stream, serialVersion ver )
{
    if(ver == mVersion)
    {
        quint8 qu8Dat;
        SERIAL_OP_C(QStringLiteral("m_nWaveType"), m_nWaveType, qu8Dat, quint8);
        stream.read(QStringLiteral("m_stBasic.s64Freq_Menu"), m_stBasic.s64Freq_Menu);
        stream.read(QStringLiteral("m_nPeriod"), m_nPeriod);
        stream.read(QStringLiteral("m_nAmpLevel"), m_nAmpLevel);
        stream.read(QStringLiteral("m_nAmplitude"),m_nAmplitude);
        stream.read(QStringLiteral("m_nHighVol"), m_nHighVol);

        stream.read(QStringLiteral("m_nOffsetLevel"),m_nOffsetLevel);
        stream.read(QStringLiteral("m_nDGOffset"),m_nDGOffset);
        stream.read(QStringLiteral("m_nLowVol"),m_nLowVol);

        stream.read(QStringLiteral("m_stBasic.u32Amp"), m_stBasic.u32Amp);//! m_nHimpAmp
        stream.read(QStringLiteral("m_nHimpHVol"), m_nHimpHVol);
        stream.read(QStringLiteral("m_stBasic.s32Offset"), m_stBasic.s32Offset);//! m_nHimpOffset
        stream.read(QStringLiteral("m_nHimpLVol"),m_nHimpLVol);

        //! modu
        stream.read(QStringLiteral("m_stModu.bModEnable"),m_stModu.bModEnable);
        SERIAL_OP_C(QStringLiteral("m_stModu.enModType"), m_stModu.enModType, qu8Dat, quint8);
        SERIAL_OP_C(QStringLiteral("m_stModu.enModWave"), m_stModu.enModWave, qu8Dat, quint8);
        stream.read(QStringLiteral("m_stModu.u64ModFreq"),m_stModu.u64ModFreq);
        stream.read(QStringLiteral("m_stModu.u64FMDev"),  m_stModu.u64FMDev);
        stream.read(QStringLiteral("m_stModu.u8AMDepth"), m_stModu.u8AMDepth);
        stream.read(QStringLiteral("m_stModu.u64HopFreq"),m_stModu.u64HopFreq);
        SERIAL_OP(QStringLiteral("m_stModu.isPolarity"),  m_stModu.isPolarity);
        //! burst
        stream.read(QStringLiteral("m_bImpedance"),m_bImpedance);
        setImpedance(m_bImpedance);

        stream.read(QStringLiteral("m_stBasic.u16Phase"),m_stBasic.u16Phase);
        stream.read(QStringLiteral("m_stBasic.u16Duty"),m_stBasic.u16Duty);
        stream.read(QStringLiteral("m_stBasic.u16Symmetry"),m_stBasic.u16Symmetry);
        //! sweep

        //! arb
        m_nCursorA    = 250;
        m_nCursorB    = 750;
        m_nVoltage    = 0;

        m_nChannel    = chan1;
        m_bRegion     = false;
        m_bLinear     = false;
        m_bZoom       = false;

        m_nEditPoints = 2;
        m_nCurrent    = 1;

        m_EditPointsTable.clear();
        for(int i = 0;i < m_nEditPoints;i++)
        {
            m_EditPointsTable.append(0);
        }
    }
    else
    {
        rst();
    }
    return ERR_NONE;
}

 void servDG::registerSpy()
 {
     spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Active , MSG_CMD_HAS_DG);
     spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Exp    , MSG_CMD_HAS_DG);
     spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Invalid, MSG_CMD_HAS_DG);
 }

void servDG::rst()
{
   // defer(CMD_SERVICE_EXIT_ACTIVE);
    m_nWaveType  = DG_WAVE_SINE;
    m_stBasic.bSourceEnable = false;
    m_stBasic.enSourceWaveform = WAVE_SINE;
    m_stBasic.enInterWaveform = INTERNAL_WAVE_SINC;

    m_nFreqPeriod = 0;
    m_stBasic.s64Freq_Menu       = freq_KHz(1);
    m_stBasic.s64Freq = freq_KHz(1);
    m_nPeriod     = time_ms(1);

    m_nAmpLevel = 0;
    //m_nAmplitude = vv(1);
    //m_nHighVol = mv(500);
    m_nAmplitude = mv(500);
    m_nHighVol = mv(250);

    m_nOffsetLevel= 0;
    m_nDGOffset   = 0;
    //m_nLowVol   = mv(-500);
    m_nLowVol   = mv(-250);
#if 0
    m_stBasic.u32Amp    = vv(1);
    m_nHimpHVol   = mv(500);
    m_stBasic.s32Offset = 0;
    m_nHimpLVol   = mv(-500);
#else
    m_stBasic.u32Amp    = mv(500);
    m_nHimpHVol   = mv(250);
    m_stBasic.s32Offset = 0;
    m_nHimpLVol   = mv(-250);
#endif
    m_nWorkMode = NONE_MODE;

    //! mode
    m_stModu.bModEnable  = false;
    m_stModu.enModType   = MOD_AM;
    m_stModu.enModWave   = MOD_WAVE_SINE;
    m_stModu.u64ModFreq  = freq_KHz(1);
    m_stModu.u64FMDev    = freq_Hz(1000);
    m_stModu.u8AMDepth   = 100;
    m_stModu.u64HopFreq  = freq_KHz(10);
    m_stModu.u64HopFreq_menu = freq_KHz(10);
    m_stModu.isPolarity  = false;

    m_bImpedance  = false;
    setImpedance(m_bImpedance);

    m_stBasic.u16Phase      = 0;
    m_stBasic.u16Duty  = 20*10;
#if 0
    m_stBasic.u16Symmetry   = 5*10;
#else
     m_stBasic.u16Symmetry   = 50*10;
#endif

    //! burst
    m_stBurst.bBurstEn = false;
    m_stBurst.euBurstType = BURST_NCYCLE;
    m_stBurst.s32Cycle = 1;
    m_stBurst.stTrigger.euTrig_Source = Trig_INTERNAL;
    mUiAttr.setEnable(MSG_DG_MANUAL_TRIG, false);
#if 0
    m_stBurst.stTrigger.s64Trig_Interval = time_ms(1);
#else
     m_stBurst.stTrigger.s64Trig_Interval = time_ms(10);
 #endif
    m_stBurst.s64Delay = time_ns(0);

    //! sweep
    m_stBurst.bBurstEn = false;
    m_stSweep.step = 2;
    m_stSweep.stTrigger.euTrig_Source = Trig_INTERNAL;
    m_stSweep.sweepType = SWEEP_LINEAR;
    m_stSweep.startFreq = freq_Hz(100);
    m_stSweep.endFreq = freq_KHz(1);
    m_stSweep.sweepTime = time_s(1);
    m_stSweep.returnTime = time_s(0);
    m_stSweep.startKeepTime = time_s(0);
    m_stSweep.endKeepTime = time_s(0);

    //! arb
    m_nCursorA    = 250;
    m_nCursorB    = 750;
    m_nVoltage    = 0;

    m_nChannel    = chan1;
    m_bRegion     = false;
    m_bLinear     = false;
    m_bZoom       = false;

    m_nEditPoints = 2;
    m_nCurrent    = 1;

    m_EditPointsTable.clear();
    for(int i = 0;i < m_nEditPoints;i++)
    {
        m_EditPointsTable.append(0);
    }
    //bug 2232 by zy
    mUiAttr.setEnable(MSG_DG_ARB_TIME,  false);

    //bug 2185 by zy
    mUiAttr.setEnable(MSG_DG_FREQ_PERIOD,  true);
    mUiAttr.setEnable(MSG_DG_STARTPHASE,  true);

    //bug 3090 by zy
    mUiAttr.setEnable(MSG_DG_MOD_FREQ, true);
    //bug 3091 by zy
    mUiAttr.setEnable(MSG_DG_BURST_CYCLE_NUM, true);
    mUiAttr.setEnable(MSG_DG_TRIGSRC, true);
}

void servDG::init()
{
    initSourceWaveData();
}

int  servDG::startup()
{
    calParaStru = getSourCalPara();
    if(loadCalData() == 0)
    {
        *calParaStru = m_dgCalData.mPayload;
    }

    async(MSG_DG_WAVE, m_nWaveType); //! 将symmtry菜单隐藏
    defer(MSG_DG_CHANGEPOINTS);
    defer(CMD_SERVICE_SUB_RETURN);
    async(servDG::MSG_CMD_HAS_DG, sysHasDG());

    //Load setup, Restart, Default. All to disable the DG
    async(MSG_APP_DG, false);

    return ERR_NONE;
}

int servDG::onActive(int /*a*/)
{
    bool bNow;
    bNow = sysHasDG();
    if(!bNow)
    {
        return ERR_ACTION_DISABLED;
    }
    else
    {
        bool en =  sysCheckLicense(OPT_DG);
        if(!en)
        {
            serviceExecutor::send( serv_name_help, MSG_OPT_HELP_INFO, (int)OPT_DG);
            return ERR_NONE;
        }
    }

    // set_Opt_DG();
    if(m_stBasic.bSourceEnable && this->isActive())
    {
        m_stBasic.bSourceEnable = false;
        m_bSync = false;
        async(MSG_APP_DG,m_stBasic.bSourceEnable);
    }
    else if(this->isActive())
    {
        m_stBasic.bSourceEnable = true;
        m_bSync = false;
        async(MSG_APP_DG,m_stBasic.bSourceEnable);
        //funcSetSourceParaStartUp(m_dgChan);
    }
    else
    {
        setActive();
        //!优化DG打开关闭方式 by zy
        if(!m_stBasic.bSourceEnable)
        {
             m_stBasic.bSourceEnable = !m_stBasic.bSourceEnable;
             async(MSG_APP_DG,m_stBasic.bSourceEnable);
        }
    }
  // async(MSG_APP_DG,m_stBasic.bSourceEnable);
    return ERR_NONE;
}

int servDG::onTimeOut(int id)
{
    switch(id)
    {
         case dg_cursora_update_timer_id:
             servGui::showHLine( "measRA", false);
             break;
         case dg_cursorb_update_timer_id:
             servGui::showHLine( "measRB", false);
             break;
         case dg_arb_update_point_id:
               async(MSG_DG_ARB_CURRENT,m_nCurrent);
              break;

         case dg_ramp_update_wave_id:
                cBasic.funcSetSourceEnabel(m_stBasic, m_dgChan);
                 //! 设置完标准波后检查猝发设置
                configMode();
               break;
         default:
             break;
    }

    enHotPlug();
    return ERR_NONE;
}

void servDG::unHotPlug()
{
//    serviceExecutor::post(serv_name_hotplug,
//                          servHotplug::cmd_source1_enable + (int) m_dgChan - 1 ,false);
//    LOG_DBG();
//    startTimer(dg_hotplug_update_timer_tmo, dg_hotplug_update_timer_id, timer_single);
}

void servDG::enHotPlug()
{
//    LOG_DBG();
//    serviceExecutor::post(serv_name_hotplug,
//                          servHotplug::cmd_source1_enable + (int) m_dgChan - 1, true);
}

void servDG::set_Opt_DG()
{
    //by hxh
//    CArgument argIn,argOut;

//    argIn.setVal((int)OPT_DG);
//    query(serv_name_license, servLicense::cmd_get_OptEnable, argIn, argOut);

//    if( argOut[0].bVal)
//    {
//        m_LicenseValid  = true;
//    }
//    else
//    {
//        m_LicenseValid  = false;
//    }
//    qDebug()<<"servCounter::"<<__FUNCTION__<<"m_LicenseValid = "<<m_LicenseValid;

//    mUiAttr.setEnable(MSG_APP_DG, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_WAVE, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_FREQ_PERIOD, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_FREQ_PERIOD_SUB_1, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_AMP_LEVEL, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_AMP_LEVEL_SUB_1, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_OFFSET_LEVEL, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_OFFSET_LEVEL_SUB_1, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_STARTPHASE, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_SYNCPAHSE, m_LicenseValid);
//    mUiAttr.setEnable(MSG_DG_SETUP_NORM, m_LicenseValid);
}
