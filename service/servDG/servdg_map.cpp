#include "servdg.h"

IMPLEMENT_CMD( serviceExecutor, servDG )
start_of_entry()

on_get_bool     (MSG_APP_DG,  &servDG::getSourceEn),
on_set_int_bool (MSG_APP_DG,  &servDG::setSourceEn),

on_set_int_int(MSG_DG_WAVE, &servDG::setWaveType),
on_get_int    (MSG_DG_WAVE, &servDG::getWaveType),

on_set_int_int(MSG_DG_FREQ_PERIOD, &servDG::selectFreqPeriod),
on_get_int    (MSG_DG_FREQ_PERIOD, &servDG::getSelectFreqPeriod),

on_set_int_ll (MSG_DG_FREQ_PERIOD_SUB_1, &servDG::setFreqPeriod),
on_get_ll     (MSG_DG_FREQ_PERIOD_SUB_1, &servDG::getFreqPeriod),

on_set_int_int(MSG_DG_AMP_LEVEL, &servDG::selectAmpLevel),
on_get_int    (MSG_DG_AMP_LEVEL, &servDG::getSelectAmpLevel),

on_set_int_ll(MSG_DG_AMP_LEVEL_SUB_1, &servDG::setAmpLevel),
on_get_ll    (MSG_DG_AMP_LEVEL_SUB_1, &servDG::getAmpLevel),

on_set_int_int(MSG_DG_OFFSET_LEVEL, &servDG::selectOffsetLevel),
on_get_int    (MSG_DG_OFFSET_LEVEL, &servDG::getSelectOffsetLevel),

on_set_int_ll(MSG_DG_OFFSET_LEVEL_SUB_1, &servDG::setOffsetLevel),
on_get_ll    (MSG_DG_OFFSET_LEVEL_SUB_1, &servDG::getOffsetLevel),

on_set_int_ll(MSG_DG_OFFSET, &servDG::setDGOffset),
on_get_ll    (MSG_DG_OFFSET, &servDG::getDGOffset),

on_set_int_int(MSG_DG_STARTPHASE, &servDG::setPhase),
on_get_int    (MSG_DG_STARTPHASE, &servDG::getPhase),

on_set_int_void (MSG_DG_SYNCPAHSE, &servDG::setSync),

// ramp
on_set_int_int(MSG_DG_SYMMETRY, &servDG::setSymmetry),
on_get_int    (MSG_DG_SYMMETRY, &servDG::getSymmetry),

// pulse
on_set_int_int(MSG_DG_DUTY_CYCLE, &servDG::setDutyCycle),
on_get_int    (MSG_DG_DUTY_CYCLE, &servDG::getDutyCycle),

// workType
on_set_int_int(MSG_DG_MOD_BUR_SWEEP, &servDG::setWorkType),
on_get_int    (MSG_DG_MOD_BUR_SWEEP, &servDG::getWorkType),

// modu
on_set_int_int(MSG_DG_MOD_TYPE, &servDG::setModType),
on_get_int    (MSG_DG_MOD_TYPE, &servDG::getModType),

on_set_int_int(MSG_DG_MOD_TYPE_RAMP, &servDG::setModType),
on_get_int    (MSG_DG_MOD_TYPE_RAMP, &servDG::getModType),

on_set_int_int(MSG_DG_MOD_SHAPE, &servDG::setModWave),
on_get_int    (MSG_DG_MOD_SHAPE, &servDG::getModWave),

on_set_int_ll(MSG_DG_MOD_FREQ, &servDG::setModFreq),
on_get_ll    (MSG_DG_MOD_FREQ, &servDG::getModFreq),

on_set_int_ll(MSG_DG_MOD_FSK_RATE, &servDG::setModFreq),
on_get_ll    (MSG_DG_MOD_FSK_RATE, &servDG::getModFreq),

on_set_int_int(MSG_DG_MOD_AM_DEPTH, &servDG::setModDepth),
on_get_int    (MSG_DG_MOD_AM_DEPTH, &servDG::getModDepth),

on_set_int_ll(MSG_DG_MOD_FM_DEV, &servDG::setModDeviation),
on_get_ll    (MSG_DG_MOD_FM_DEV, &servDG::getModDeviation),

on_set_int_ll(MSG_DG_MOD_FSK_HOP, &servDG::setModHopFreq),
on_get_ll    (MSG_DG_MOD_FSK_HOP, &servDG::getModHopFreq),

on_set_int_bool(MSG_DG_MOD_FSK_POLARITY, &servDG::setModFSKPol),
on_get_bool    (MSG_DG_MOD_FSK_POLARITY, &servDG::getModFSKPol),

on_set_int_bool(MSG_DG_IMPEDANCE, &servDG::setImpedance),
on_get_bool    (MSG_DG_IMPEDANCE, &servDG::getImpedance),

//on_set_int_bool(MSG_DG_MOD, &servDG::setModEnable),
//on_get_bool    (MSG_DG_MOD, &servDG::getModEnable),

// burst
on_set_int_int(MSG_DG_BURST_TYPE, &servDG::setBurstType),
on_get_int    (MSG_DG_BURST_TYPE, &servDG::getBurstType),

on_set_int_int(MSG_DG_BURST_CYCLE_NUM, &servDG::setCycleNum),
on_get_int    (MSG_DG_BURST_CYCLE_NUM, &servDG::getCycleNum),

on_set_int_ll(MSG_DG_BURST_PERIOD, &servDG::setBurstPeriod),
on_get_ll    (MSG_DG_BURST_PERIOD, &servDG::getBurstPeriod),

on_set_int_ll(MSG_DG_BURST_DELAY, &servDG::setDelay),
on_get_ll    (MSG_DG_BURST_DELAY, &servDG::getDelay),

on_set_int_int(MSG_DG_TRIGSRC, &servDG::setTrigSrc),
on_get_int    (MSG_DG_TRIGSRC, &servDG::getTrigSrc),

on_set_int_void(MSG_DG_MANUAL_TRIG, &servDG::trigManual),

// sweep
on_set_int_int(MSG_DG_SWEEP_TYPE, &servDG::setSweepType),
on_get_int    (MSG_DG_SWEEP_TYPE, &servDG::getSweepType),

on_set_int_ll (MSG_DG_SWEEP_TIME, &servDG::setSweepTime),
on_get_ll     (MSG_DG_SWEEP_TIME, &servDG::getSweepTime),

on_set_int_ll (MSG_DG_SWEEP_BACKTIME, &servDG::setReturnTime),
on_get_ll     (MSG_DG_SWEEP_BACKTIME, &servDG::getReturnTime),

on_set_int_ll (MSG_DG_SWEEP_STARTKEEP, &servDG::setStartKeep),
on_get_ll     (MSG_DG_SWEEP_STARTKEEP, &servDG::getStartKeep),

on_set_int_ll (MSG_DG_SWEEP_ENDKEEP,   &servDG::setEndKeep),
on_get_ll     (MSG_DG_SWEEP_ENDKEEP,   &servDG::getEndKeep),

on_set_int_ll (MSG_DG_SWEEP_STEP, &servDG::setSweepStep),
on_get_ll     (MSG_DG_SWEEP_STEP, &servDG::getSweepStep),

on_set_int_ll (MSG_DG_SWEEP_STARTFREQ, &servDG::setStartFreq),
on_get_ll     (MSG_DG_SWEEP_STARTFREQ, &servDG::getStartFreq),

on_set_int_ll (MSG_DG_SWEEP_ENDFREQ, &servDG::setEndFreq),
on_get_ll     (MSG_DG_SWEEP_ENDFREQ, &servDG::getEndFreq),

//arb begin
on_set_int_int(MSG_DG_ARB_CHAN_SELECT, &servDG::setLoadChannel),
on_get_int    (MSG_DG_ARB_CHAN_SELECT, &servDG::getLoadChannel),

on_set_int_bool(MSG_DG_ARB_REGION, &servDG::setRegion),
on_get_bool    (MSG_DG_ARB_REGION, &servDG::getRegion),

on_set_int_int(MSG_DG_ARB_CURSORA, &servDG::setCursorA),
on_get_int    (MSG_DG_ARB_CURSORA, &servDG::getCursorA),

on_set_int_int(MSG_DG_ARB_CURSORB, &servDG::setCursorB),
on_get_int    (MSG_DG_ARB_CURSORB, &servDG::getCursorB),

on_set_int_int(MSG_DG_ARB_CURSORAB, &servDG::setCursorAB),
on_get_int    (MSG_DG_ARB_CURSORAB, &servDG::getCursorAB),

on_set_int_int(MSG_DG_ARB_POINTS, &servDG::setPoints),
on_get_int    (MSG_DG_ARB_POINTS, &servDG::getPoints),

on_set_int_bool(MSG_DG_ARB_LINEAR, &servDG::setLinear),
on_get_bool    (MSG_DG_ARB_LINEAR, &servDG::getLinear),

on_set_int_int(MSG_DG_ARB_STORED, &servDG::loadStored),
on_set_int_int(MSG_DG_ARB_LOAD,   &servDG::loadChan),
on_set_int_int(MSG_DG_ARB_SAVE,   &servDG::save),

on_set_int_int(MSG_DG_ARB_CURRENT, &servDG::setCurrent),
on_get_int    (MSG_DG_ARB_CURRENT, &servDG::getCurrent),

on_set_int_int(MSG_DG_ARB_VOLTAGE, &servDG::setVoltage),
on_get_int    (MSG_DG_ARB_VOLTAGE, &servDG::getVoltage),

//on_set_int_ll (MSG_DG_ARB_TIME, &servDG::setTime),
//on_get_ll     (MSG_DG_ARB_TIME, &servDG::getTime),

on_set_int_int(MSG_DG_ARB_INSERT, &servDG::insPoint),
on_set_int_int(MSG_DG_ARB_DELETE, &servDG::delPoint),
on_set_int_int(MSG_DG_ARB_DONE, &servDG::apply),

on_set_int_bool(MSG_DG_ARB_ZOOM, &servDG::setZoom),
on_get_bool    (MSG_DG_ARB_ZOOM, &servDG::getZoom),

on_get_int    (servDG::MSG_DG_Get_SubMenu,  &servDG::getMenuMSG),
on_get_int    (servDG::MSG_DG_Get_ExitMenu, &servDG::getExitMenuMsg),
on_get_pointer(servDG::MSG_DG_Get_WavTable, &servDG::getEditTable),

//!
on_set_int_int(servDG::MSG_DG_LOADPOINTS, &servDG::setLoadPoints),

on_set_int_void( servDG::MSG_CMD_HAS_DG, &servDG::setHasDg ),
on_get_bool( servDG::MSG_CMD_HAS_DG,     &servDG::getHasDg ),

//!scpi msg
on_set_int_arg  (servDG::MSG_CMD_SET_SIN,    &servDG::cmd_setSin),
on_set_int_arg  (servDG::MSG_CMD_SET_SQUARE, &servDG::cmd_setSquare),
on_set_int_arg  (servDG::MSG_CMD_SET_PULSE,  &servDG::cmd_setPulse),
on_set_int_arg  (servDG::MSG_CMD_SET_RAMP,   &servDG::cmd_setRamp),
on_set_int_arg  (servDG::MSG_CMD_SET_NOISE,  &servDG::cmd_setNoise),
on_set_int_arg  (servDG::MSG_CMD_SET_DC,        &servDG::cmd_setDC),
on_set_int_arg  (servDG::MSG_CMD_SET_USER,   &servDG::cmd_setUser),
on_get_void_argo(servDG::MSG_CMD_GET_APPLY,  &servDG::cmd_getWaveArg),

on_set_int_ll   (servDG::MSG_DG_CMD_FREQ,   &servDG::cmd_setFreq),
on_get_ll       (servDG::MSG_DG_CMD_FREQ,   &servDG::cmd_getFreq),

on_set_int_ll  (servDG::MSG_DG_CMD_AMP,   &servDG::cmd_setAmp),
on_get_ll      (servDG::MSG_DG_CMD_AMP,   &servDG::cmd_getAmp),

on_set_int_ll  (servDG::MSG_DG_CMD_OFFSET,   &servDG::cmd_setOffset),
on_get_ll      (servDG::MSG_DG_CMD_OFFSET,   &servDG::cmd_getOffset),

on_set_int_ll  (servDG::MSG_DG_CMD_PHASE,   &servDG::cmd_setStartPhase),
on_get_ll      (servDG::MSG_DG_CMD_PHASE,   &servDG::cmd_getStartPhase),

on_set_int_int(servDG::MSG_DG_CMD_SYMMETRY, &servDG::setScpiSymmetry),
on_get_int    (servDG::MSG_DG_CMD_SYMMETRY, &servDG::getScpiSymmetry),

on_set_int_int(servDG::MSG_DG_CMD_DCYCLE, &servDG::setScpiDutyCycle),
on_get_int    (servDG::MSG_DG_CMD_DCYCLE, &servDG::getScpiDutyCycle),

on_set_int_int(servDG::MSG_DG_CMD_MODWAVE, &servDG::setScpiModWave),
on_get_int    (servDG::MSG_DG_CMD_MODWAVE, &servDG::getScpiModWave),

on_set_int_ll(servDG::MSG_DG_CMD_MODDEVI, &servDG::setScpiModDeviation),
on_get_ll    (servDG::MSG_DG_CMD_MODDEVI, &servDG::getScpiModDeviation),

//!cal scpi
on_set_int_int  (servDG::MSG_DG_CAL_FREQ, &servDG::cmd_setCalFreq),
on_get_int      (servDG::MSG_DG_CAL_FREQ, &servDG::cmd_getCalFreq),

on_set_int_int  (servDG::MSG_DG_CAL_PHASE, &servDG::cmd_setCalPhase),
on_get_int      (servDG::MSG_DG_CAL_PHASE, &servDG::cmd_getCalPhase),

on_set_int_arg         (servDG::MSG_DG_CAL_AMP, &servDG::cmd_setCalAmp),
on_get_void_argi_argo  (servDG::MSG_DG_CAL_AMP, &servDG::cmd_getCalAmp),

on_set_int_arg         (servDG::MSG_DG_CAL_DC, &servDG::cmd_setCalDC),
on_get_void_argi_argo  (servDG::MSG_DG_CAL_DC, &servDG::cmd_getCalDC),

on_set_int_arg         (servDG::MSG_DG_CAL_OFFSET, &servDG::cmd_setCalOffset),
on_get_void_argi_argo  (servDG::MSG_DG_CAL_OFFSET, &servDG::cmd_getCalOffset),

on_set_int_void        (servDG::MSG_DG_CAL_SAVE,   &servDG::cmd_calSave),
on_set_int_void        (servDG::MSG_DG_CAL_DEFAULT,&servDG::cmd_calDefault),

//! sys msg
on_set_void_int (CMD_SERVICE_TIMEOUT,           &servDG::onTimeOut ),
on_set_void_void(CMD_SERVICE_RST,               &servDG::rst ),
on_set_void_void(CMD_SERVICE_INIT,              &servDG::init ),
on_set_int_void (CMD_SERVICE_STARTUP,           &servDG::startup ),
on_set_int_int  (CMD_SERVICE_ACTIVE,            &servDG::onActive),
on_set_int_int  (CMD_SERVICE_SUB_ENTER,         &servDG::onEnterSubMenu ),
on_set_int_int  (CMD_SERVICE_SUB_RETURN,        &servDG::onExitSubMenu ),
end_of_entry()

