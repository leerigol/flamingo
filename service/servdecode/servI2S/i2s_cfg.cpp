#include "../../include/dsocfg.h"
#include "../../servdso/sysdso.h"
#include "../../servhori/servhori.h"
#include "../../service_name.h"

#include "i2s_cfg.h"
#include "../servdecode.h"


void CI2SCfg::reset()
{
    sclk = chan1;
    edge = false;
    ws   = chan2;
    sdata= chan3;
    align= I2S_ALIGN_STD;
    wsLow= ws_left;
    wordSize = 4;
    recvSize = 4;
    endian   = msb;
    polarity = Polarity_Positive;
}

void CI2SCfg::serialIn(CStream & stream)
{
    int temp = 0;
    stream.read("sclk",  temp);
    sclk = (Chan)temp;

    stream.read("edge", edge);

    stream.read("ws", temp);
    ws = (Chan)temp;

    stream.read("sdata", temp);
    sdata = (Chan)temp;

    stream.read("align", align);
    stream.read("wsLow",  wsLow);

    stream.read("wordSize", wordSize);
    stream.read("recvSize", recvSize);
}

void CI2SCfg::serialOut(CStream & stream)
{
    stream.write("sclk",  (int)sclk);
    stream.write("edge", edge);

    stream.write("ws", (int)ws);
    stream.write("sdata", (int)sdata);



    stream.write("align", align);

    stream.write("wsLow",  wsLow);
    stream.write("wordSize", wordSize);
    stream.write("recvSize", recvSize);
}

DsoErr servDec::setI2SClk(Chan src)
{
    mServI2S.sclk = src;

    if(src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_I2S_SCLK_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_I2S_SCLK_THRE, true);
        setuiChange( MSG_DECODE_I2S_SCLK_THRE );
    }
    return ERR_NONE;
}

Chan servDec::getI2SClk()
{    
    return mServI2S.sclk;
}


DsoErr servDec::setI2SClkThre(qint64 t)
{
    setChThreshold( mServI2S.sclk, t);
    return ERR_NONE;
}

qint64 servDec::getI2SClkThre()
{
    Chan s = mServI2S.sclk;

    return getChThreshold(s);
}


DsoErr servDec::setI2SEdge(bool edge)
{
    mServI2S.edge = edge;
    return ERR_NONE;
}

bool servDec::getI2SEdge()
{
    return mServI2S.edge;
}


DsoErr servDec::setI2SWS(Chan src)
{
    mServI2S.ws = src;
    if(src == chan_none || src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_I2S_WS_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_I2S_WS_THRE, true);
        setuiChange( MSG_DECODE_I2S_WS_THRE );
    }

    return ERR_NONE;
}

Chan servDec::getI2SWS()
{
    return mServI2S.ws;
}


DsoErr servDec::setI2SWSThre(qint64 t)
{
    setChThreshold( mServI2S.ws, t);
    return ERR_NONE;
}

qint64 servDec::getI2SWSThre()
{
    Chan s = mServI2S.ws;
    return getChThreshold(s);
}


DsoErr servDec::setI2SData(Chan src)
{
    mServI2S.sdata = src;
    if(src == chan_none || src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_I2S_DATA_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_I2S_DATA_THRE, true);
        setuiChange( MSG_DECODE_I2S_DATA_THRE );
    }

    return ERR_NONE;
}

Chan servDec::getI2SData()
{    
    return mServI2S.sdata;
}


DsoErr servDec::setI2SDataThre(qint64 t)
{
    setChThreshold( mServI2S.sdata, t);
    return ERR_NONE;
}

qint64 servDec::getI2SDataThre()
{
    Chan s = mServI2S.sdata;
    return getChThreshold(s);
}


DsoErr servDec::setI2SWord(int time)
{
    mServI2S.wordSize = time;
    return ERR_NONE;
}

int servDec::getI2SWord()
{
    setuiZVal(8);
    setuiMinVal( 4 );
    setuiMaxVal(  32 );
    return mServI2S.wordSize;
}



DsoErr servDec::setI2SWSLow(bool b)
{
    mServI2S.wsLow = b;
    return ERR_NONE;
}
bool servDec::getI2SWSLow()
{
    return mServI2S.wsLow;
}

DsoErr servDec::setI2SRecv(int pn)
{
    mServI2S.recvSize = pn;
    return ERR_NONE;
}

int servDec::getI2SRecv()
{
    setuiZVal(4);
    setuiMinVal( 4 );
    setuiMaxVal(  32 );
    return mServI2S.recvSize;
}


DsoErr servDec::setI2SAlign(int datawid)
{
    mServI2S.align = datawid;
    return ERR_NONE;
}

int servDec::getI2SAlign()
{
    setuiZVal(I2S_ALIGN_STD);
    setuiMinVal( I2S_ALIGN_STD );
    setuiMaxVal(  I2S_ALIGN_RJ );

    return mServI2S.align;
}


DsoErr servDec::setI2SEndian(bool b)
{
    mServI2S.endian = b;
    return ERR_NONE;
}
bool servDec::getI2SEndian()
{
    return mServI2S.endian;
}



DsoErr servDec::setI2SPol(bool b)
{
    mServI2S.polarity = b;
    return ERR_NONE;
}
bool servDec::getI2SPol()
{
    return mServI2S.polarity;
}
