#ifndef CI2SDecoder_H
#define CI2SDecoder_H


#include "i2s_cfg.h"
#include "../decoder.h"
#include "../decTool/DecState.h"



class CI2SDecoder : public CDecoder,public CDecState
{
public:
     CI2SDecoder();
    ~CI2SDecoder();

public:
    void rst();
    bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
    int  setEventTable(CTable* table);

public:
    static CI2SCfg* mpI2SCfg;

private:
    bool decode(Chan ws, Chan clk, Chan dat);
};

class CI2SIdle : public CI2SDecoder
{
public:
    CI2SIdle();
    virtual ~CI2SIdle();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );

private:
    int  mNextChan;
    bool mWSEdge;
};

class CI2SData: public CI2SDecoder
{
public:
    CI2SData();
    virtual ~CI2SData();

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );

protected:
     int  mCurrChan;
     int  mNextChan;
     bool mWSEdge;
     QList<int> mPosArray;
};


#endif // CI2SDecoder_H
