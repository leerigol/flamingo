#include <QDebug>
#include "i2s_fsm.h"


//! CI2SIdle
CI2SIdle::CI2SIdle()
{}
CI2SIdle::~CI2SIdle()
{}

void CI2SIdle::onEnter( int /*bitVal*/ )
{
    mCount  = 0;
    mWSEdge = false;
}

void CI2SIdle::serialIn( int event, int /*bitVal*/ )
{
    if( event == LeftChannel ||
        event == RightChannel )
    {
        mNextChan = event;
        mWSEdge   = true;
        if( mpI2SCfg->align != I2S_ALIGN_STD )
        {
            AddOneFrame();
            toNext( mNextChan );
        }
    }

    if( event == SAMP && mWSEdge)
    {
        mCount++;
        if(mCount == 1)
        {
            AddOneFrame();
            toNext( mNextChan );
        }
    }
}

//! CI2SData
CI2SData::CI2SData()
{}
CI2SData::~CI2SData()
{}

void CI2SData::onEnter( int ch)
{
    mCount = 0;
    mData  = 0;
    mCurrChan = ch;
    mWSEdge = false;

    mPosArray.clear();
}

void CI2SData::serialIn(int event,int bitVal)
{
    if( event == SAMP )
    {
        if( mCount == 0 )
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }
        mPosArray.append(mCounter.now());

        if( mpI2SCfg->endian == msb )
        {
            mData = (mData  << 1) | bitVal;
        }
        else
        {
            mData |= ( bitVal << mCount );
        }
        mCount++;


        if((int)mCount == mpI2SCfg->wordSize )
        {
            if( !mWSEdge && mpI2SCfg->align != I2S_ALIGN_RJ)
            {
                toIdle( event );
                return;
            }
        }

        //LSB WHEN iis std
        if( mWSEdge )
        {
            toSelf( mNextChan );
        }
    }

    if( event == LeftChannel ||
        event == RightChannel)
    {
        mWSEdge   = true;
        mNextChan = event;
        if( mpI2SCfg->align != I2S_ALIGN_STD )
        {
            toSelf( mNextChan );
        }
    }
}

void CI2SData::onExit(int /*bitVal*/)
{
    mSpan = mCounter.getDur();

    CDecPayloadData* pData = new CDecPayloadData();
    Q_ASSERT( NULL != pData );

    int color = IIS_LEFT_COLOR;
    int caption = TEXT_LEFT;


    //qDebug() << "mCount=" << mCount << "wordsize=" << mpI2SCfg->wordSize;

    if( mCurrChan == RightChannel )
    {
        color   = IIS_RIGHT_COLOR;
        caption = TEXT_RIGHT;
    }

    if( mpI2SCfg->align == I2S_ALIGN_RJ )
    {
        int index = mCount - mpI2SCfg->wordSize ;
        if( index >=0 )
        {
            mPosition = mPosArray.at(index);
            mSpan     = mCounter.now() - mPosition;
            mCount    = mpI2SCfg->wordSize;
        }
    }

    if((int)mCount != mpI2SCfg->wordSize)
    {
        color = ERR_DATA_COLOR;
    }

    pData->setPosSpan( mPosition, mSpan );
    pData->setPayload( mData, mCount , color, caption);
    AddBlock(CDecPayloadData,pData);

    mpTable->append(new CCellTime(mPosition));

    if( mCurrChan == RightChannel )
    {
        mpTable->append(new CCellStr(0));//NULL
        mpTable->append(new CCellDW(mData,mCount));
    }
    else
    {
        mpTable->append(new CCellDW(mData,mCount));
        mpTable->append(new CCellStr(0));//NULL
    }
}


