#ifndef I2S_MAP_H
#define I2S_MAP_H

#include "../servdecode/servdecode.h"

on_set_int_int  (MSG_DECODE_I2S_SCLK,       &servDec::setI2SClk),
on_get_int      (MSG_DECODE_I2S_SCLK,       &servDec::getI2SClk),

on_set_int_bool  (MSG_DECODE_I2S_SCLKEDGE,  &servDec::setI2SEdge),
on_get_bool      (MSG_DECODE_I2S_SCLKEDGE,  &servDec::getI2SEdge),

on_set_int_ll   (MSG_DECODE_I2S_SCLK_THRE,  &servDec::setI2SClkThre),
on_get_ll       (MSG_DECODE_I2S_SCLK_THRE,  &servDec::getI2SClkThre),

on_set_int_int  (MSG_DECODE_I2S_WS,         &servDec::setI2SWS),
on_get_int      (MSG_DECODE_I2S_WS,         &servDec::getI2SWS),

on_set_int_ll   (MSG_DECODE_I2S_WS_THRE,    &servDec::setI2SWSThre),
on_get_ll       (MSG_DECODE_I2S_WS_THRE,    &servDec::getI2SWSThre),

on_set_int_int  (MSG_DECODE_I2S_DATA,       &servDec::setI2SData),
on_get_int      (MSG_DECODE_I2S_DATA,       &servDec::getI2SData),


on_set_int_ll   (MSG_DECODE_I2S_DATA_THRE,  &servDec::setI2SDataThre),
on_get_ll       (MSG_DECODE_I2S_DATA_THRE,  &servDec::getI2SDataThre),

on_set_int_int  (MSG_DECODE_I2S_WORD,       &servDec::setI2SWord),
on_get_int      (MSG_DECODE_I2S_WORD,       &servDec::getI2SWord),

on_set_int_int  (MSG_DECODE_I2S_RECEIVE,    &servDec::setI2SRecv),
on_get_int      (MSG_DECODE_I2S_RECEIVE,    &servDec::getI2SRecv),

on_set_int_int  (MSG_DECODE_I2S_ALIGN,      &servDec::setI2SAlign),
on_get_int      (MSG_DECODE_I2S_ALIGN,      &servDec::getI2SAlign),

on_set_int_bool (MSG_DECODE_I2S_WSLOW,      &servDec::setI2SWSLow),
on_get_bool     (MSG_DECODE_I2S_WSLOW,      &servDec::getI2SWSLow),

on_set_int_bool (MSG_DECODE_I2S_ENDIAN,     &servDec::setI2SEndian),
on_get_bool     (MSG_DECODE_I2S_ENDIAN,     &servDec::getI2SEndian),

on_set_int_bool (MSG_DECODE_I2S_POL,        &servDec::setI2SPol),
on_get_bool     (MSG_DECODE_I2S_POL,        &servDec::getI2SPol),

#endif // I2S_MAP_H

