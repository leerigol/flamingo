#include "i2s_fsm.h"
#include "../servdecode.h"

static CI2SIdle   fsm_i2s_idle;
static CI2SData   fsm_i2s_data;

CI2SCfg* CI2SDecoder::mpI2SCfg;

CI2SDecoder::CI2SDecoder()
{
     mpNow = &fsm_i2s_idle;
     fsm_i2s_idle.setNextStat( &fsm_i2s_data );
     fsm_i2s_data.setNextStat( &fsm_i2s_idle );
     setIdle( &fsm_i2s_idle );
}

CI2SDecoder::~CI2SDecoder()
{
}


void CI2SDecoder::rst()
{
   mpNow = &fsm_i2s_idle;
   mCounter.rst();
   mClock.stop();
   mpI2SCfg = &mpCurrServ->mServI2S;
   mpTable = new CTable();
}



//! rst to idle
bool CI2SDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{
    bool ret = false;

    if( decode(mpI2SCfg->ws,mpI2SCfg->sclk,mpI2SCfg->sdata) )
    {
        CDecLine* line = new CDecLine();

        //if( mpCurrServ->getLabelOn())
        {
            QString label = "I2S";
            line->setLabel(label);
        }
        mLineView.makeLine(*line,mBlocklist);
        bus->append( line );
        if(zoomBus)
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        setEventTable( mpTable );
        ret = true;
    }
    return ret;
}

bool CI2SDecoder::decode(Chan ws, Chan clk, Chan dat)
{
    uint  u32Clk,u32WS;

    int next_dat;
    int last_clk;
    int next_clk;
    int last_ws;
    int next_ws;


    uchar* pWS = mpCurrServ->getChanData(ws);
    uchar* pClk= mpCurrServ->getChanData(clk);
    uchar* pDat= mpCurrServ->getChanData(dat);
    if( !pWS || !pClk || !pDat )
    {
        return false;
    }

    last_clk = ( *pClk & 0x80 ) ? 1 : 0;
    last_ws  = ( *pWS  & 0x80 ) ? 1 : 0;


    bool wsLow  = mpI2SCfg->wsLow;
    bool dat_pol = mpI2SCfg->polarity;
    bool clk_edge= mpI2SCfg->edge;

    clearBlock();
    int size = mpCurrServ->getDataSize();
    now()->onEnter( 0 );
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32WS, pWS + i, 4 );
        memcpy( &u32Clk, pClk + i, 4 );

        //! no change
        if ( u32WS == m32IdleMask[last_ws] && u32Clk == m32IdleMask[last_clk])
        {
            i += 4;
            mCounter.count( 32 );
            continue;
        }

        //! no change in u8
        if ( (u32WS  & 0xff) == m8IdleMask[last_ws] &&
             (u32Clk & 0xff) == m8IdleMask[last_clk] )
        {
            i += 1;
            mCounter.count(8);
            continue;
        }

        char data = pDat[i];
        if( dat_pol == Polarity_Negtive)
        {
            data = (~data) & 0xff;
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_ws  = u32WS  & m32Masks[j] ? 1 : 0;
            next_clk = u32Clk & m32Masks[j] ? 1 : 0;
            next_dat = data & m32Masks[j] ? 1 : 0;

            /* select start condition by CS signal */
            if( last_ws < next_ws)//cs rise
            {
                if(wsLow == ws_right)//ss=0
                {
                    now()->serialIn( LeftChannel, next_dat );
                }
                else
                {
                    now()->serialIn( RightChannel, next_dat );
                }
                last_ws = next_ws;
                continue;
            }
            else if(last_ws > next_ws)//cs fall
            {
                if( wsLow == ws_right)
                {
                    now()->serialIn( RightChannel, next_dat );
                 }
                else
                {
                    now()->serialIn( LeftChannel, next_dat );
                }
                last_ws = next_ws;
                continue;
            }

            /* sample by rise or fall clock */
            if(last_clk < next_clk) //clk rise
            {                
                if( clk_edge == EDGE_RISING)
                {
                    now()->serialIn( SAMP, next_dat );
                }
                last_clk = next_clk;
            }
            else if(last_clk > next_clk)//clk fall
            {
                if( clk_edge == EDGE_FALLING)
                {
                    now()->serialIn( SAMP, next_dat );
                }
                last_clk = next_clk;
            }
            mCounter.count();
        }
        i += 1;
    }
    return true;
}



int CI2SDecoder::setEventTable(CTable* table)
{
    //! head
    QList<quint32> lstHead;
    //! layout
    QList<quint32> lstLayout;

    lstHead.append( MSG_DECODE_EVT_TIME );
    lstLayout.append( 50 );


    lstHead.append( MSG_DECODE_EVT_LEFT );
    lstHead.append( MSG_DECODE_EVT_RIGHT );
    lstLayout.append( 100 );
    lstLayout.append( 200 );

    table->setHead( lstHead );

    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_I2S);

    table->setTitle( pTitle );
    return ERR_NONE;
}
