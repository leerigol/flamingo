#ifndef CI2SCFG_H
#define CI2SCFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;

enum
{
    I2S_ALIGN_STD,
    I2S_ALIGN_LJ,
    I2S_ALIGN_RJ
};
struct CI2SCfg
{
    void reset();
    void serialIn(CStream&);
    void serialOut(CStream&);

    Chan sclk;
    bool edge;
    Chan ws;
    Chan sdata;
    int  align;
    bool wsLow;
//for data
    bool endian;
    bool polarity;
    int  wordSize;
    int  recvSize;

};

#endif // CI2SCFG_H
