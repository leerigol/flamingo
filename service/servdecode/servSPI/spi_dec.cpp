#include "spi_fsm.h"
#include "../servdecode.h"

static CSpiIdle   fsm_spi_idle;
static CSpiStart  fsm_spi_start;
static CSpiData   fsm_spi_data;

CSpiCfg* CSpiDecoder::mpSpiCfg;

CSpiDecoder::CSpiDecoder()
{
     mpNow = &fsm_spi_idle;
     fsm_spi_idle.setNextStat( &fsm_spi_start );
     fsm_spi_start.setNextStat( &fsm_spi_data );
     fsm_spi_data.setNextStat( &fsm_spi_idle );
}

CSpiDecoder::~CSpiDecoder()
{
}


void CSpiDecoder::rst()
{
   mpNow = &fsm_spi_idle;
   mCounter.rst();
   mClock.stop();
   mpSpiCfg = &mpCurrServ->mServSPI;
   mpTable = new CTable();
}



//! rst to idle
bool CSpiDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{
    bool ret = false;

    CTable* misoTable;
    CTable* mosiTable;
    if( mpSpiCfg->mMode == SPI_MODE_TMO )
    {
        misoTable = mpTable;
        if( mpSpiCfg->mMISOSrc != chan_none)
        {
            ret = decode_tmo(mpSpiCfg->mClkSrc, mpSpiCfg->mMISOSrc);

            if( ret )
            {
                CDecLine* line = new CDecLine();

                //if( mpCurrServ->getLabelOn())
                {
                    QString label = "SPI-MISO";
                    line->setLabel(label);
                }
                mLineView.makeLine(*line,mBlocklist);
                bus->append( line );
                if( zoomBus )
                {
                    CDecLine* lineZoom = new CDecLine();
                    lineZoom->setLabel( line->getLabel() );

                    mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
                    zoomBus->append(lineZoom);
                }
            }
        }

        rst();
        mosiTable = mpTable;
        if( mpSpiCfg->mMOSISrc != chan_none )
        {
            ret = decode_tmo(mpSpiCfg->mClkSrc, mpSpiCfg->mMOSISrc );

            if( ret )
            {
                CDecLine* line = new CDecLine();
                //if( mpCurrServ->getLabelOn())
                {
                    QString label = "SPI-MOSI";
                    line->setLabel(label);
                }
                mLineView.makeLine(*line,mBlocklist);
                bus->append( line );
                if( zoomBus )
                {
                    CDecLine* lineZoom = new CDecLine();
                    lineZoom->setLabel( line->getLabel() );

                    mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
                    zoomBus->append(lineZoom);
                }
            }
        }
    }
    else
    {
        misoTable = mpTable;
        if( mpSpiCfg->mMISOSrc != chan_none)
        {
            ret = decode_cs(mpSpiCfg->mCsSrc,
                            mpSpiCfg->mClkSrc,
                            mpSpiCfg->mMISOSrc);

            if( ret )
            {
                CDecLine* line = new CDecLine();

                //if( mpCurrServ->getLabelOn())
                {
                    QString label = "SPI-MISO";
                    line->setLabel(label);
                }
                mLineView.makeLine(*line,mBlocklist);
                bus->append( line );
                if( zoomBus )
                {
                    CDecLine* lineZoom = new CDecLine();
                    lineZoom->setLabel( line->getLabel() );

                    mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
                    zoomBus->append(lineZoom);
                }
            }
        }

        rst();
        mosiTable = mpTable;
        if( mpSpiCfg->mMOSISrc != chan_none )
        {
            ret = decode_cs(mpSpiCfg->mCsSrc,
                            mpSpiCfg->mClkSrc,
                            mpSpiCfg->mMOSISrc );

            if( ret )
            {
                CDecLine* line = new CDecLine();

                //if( mpCurrServ->getLabelOn())
                {
                    QString label = "SPI-MOSI";
                    line->setLabel(label);
                }
                mLineView.makeLine(*line,mBlocklist);
                bus->append( line );
                if( zoomBus )
                {
                    CDecLine* lineZoom = new CDecLine();
                    lineZoom->setLabel( line->getLabel() );

                    mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
                    zoomBus->append(lineZoom);
                }
            }
        }
    }

    if( ret )
    {
        mpTable = new CTable();
        setEventTable( mpTable );
        mergeEvt(misoTable, mosiTable, mpTable);
    }

    return ret;
}

bool CSpiDecoder::decode_tmo(Chan clk, Chan dat)
{
    uint  u32Clk;
    uchar u8Dat;

    int next_dat = 0;
    int last_clk = 0;
    int next_clk = 0;

    bool dat_pol = mpSpiCfg->mDatPN;
    bool clk_edge= mpSpiCfg->mClkEdge;

    uchar* pClk = mpCurrServ->getChanData(clk);
    uchar* pDat = mpCurrServ->getChanData(dat);
    if( !pClk || !pDat )
    {
        return false;
    }

    int dist= 0;
    last_clk = (*pClk & 0x80 ) ? 1 : 0;

    int Tmo = mpSpiCfg->getTimeoutSample();

    clearBlock();

    bool bStart = false;
    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Clk, pClk + i, 4 );

        //! no change
        if ( u32Clk == m32IdleMask[last_clk])
        {
            if( (dist+32) < Tmo)
            {
                i += 4;
                dist += 32;
                mCounter.count( 32 );
                continue;
            }
        }

        //! no change in u8
        if ( (u32Clk & 0xff) == m8IdleMask[last_clk] )
        {
            if( (dist+8) < Tmo)
            {
                i += 1;
                dist += 8;
                mCounter.count(8);
                continue;
            }
        }

        u8Dat= *(pDat+i);
        if(dat_pol == Polarity_Negtive)
        {
            u8Dat = (~u8Dat) & 0xff;
        }
        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_clk = u32Clk & m32Masks[j] ? 1 : 0;

            if( next_clk == last_clk )
            {
                //waiting start condition
                dist++;
                if(dist == Tmo )
                {
                    mpNow->serialIn( SPITMO, 0 ); // to start or end
                    bStart = true;
                }
            }
            else if(last_clk < next_clk) //clk rise
            {
                next_dat = u8Dat & m32Masks[j] ? 1 : 0;
                if(clk_edge == EDGE_RISING)
                {
                    mpNow->serialIn( SAMP, next_dat );
                }
                else
                {
                    mpNow->serialIn( SampleOver, next_dat );
                }
                last_clk = next_clk;

                //bug1945 by hxh
                if( !bStart )
                {
                    bStart =  true;
                    mpNow->serialIn( SPITMO, 0 );
                }
                dist = 0;
            }
            else if(last_clk > next_clk)//clk fall
            {
                next_dat = u8Dat & m32Masks[j] ? 1 : 0;

                if(clk_edge == EDGE_FALLING)
                {
                    mpNow->serialIn( SAMP, next_dat );
                }
                else
                {
                    mpNow->serialIn( SampleOver, next_dat );
                }
                last_clk = next_clk;

                //bug1945 by hxh
                if( !bStart )
                {
                    bStart =  true;
                    mpNow->serialIn( SPITMO, 0 );
                }
                dist = 0;
            }
            mCounter.count();
        }
        i += 1;
    }
    return true;
}

bool CSpiDecoder::decode_cs(Chan cs, Chan clk, Chan dat)
{
    uint  u32Clk,u32CS;

    int next_dat;
    int last_clk;
    int next_clk;
    int last_cs;
    int next_cs;


    uchar* pCS = mpCurrServ->getChanData(cs);
    uchar* pClk= mpCurrServ->getChanData(clk);
    uchar* pDat= mpCurrServ->getChanData(dat);
    if( !pCS || !pClk || !pDat )
    {
        return false;
    }

    last_clk = ( *pClk & 0x80 ) ? 1 : 0;
    last_cs  = ( *pCS  & 0x80 ) ? 1 : 0;


    bool cs_pol  = mpSpiCfg->mCsPN;
    bool dat_pol = mpSpiCfg->mDatPN;
    bool clk_edge= mpSpiCfg->mClkEdge;

    clearBlock();
    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32CS, pCS + i, 4 );
        memcpy( &u32Clk, pClk + i, 4 );

        //! no change
        if ( u32CS == m32IdleMask[last_cs] && u32Clk == m32IdleMask[last_clk])
        {
            i += 4;
            mCounter.count( 32 );
            continue;
        }

        //! no change in u8
        if ( (u32CS  & 0xff) == m8IdleMask[last_cs] &&
             (u32Clk & 0xff) == m8IdleMask[last_clk] )
        {
            i += 1;
            mCounter.count(8);
            continue;
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_cs  = u32CS  & m32Masks[j] ? 1 : 0;
            next_clk = u32Clk & m32Masks[j] ? 1 : 0;

            /* select start condition by CS signal */
            if( last_cs < next_cs)//cs rise
            {
                if(cs_pol == Polarity_Negtive)//ss=0
                {
                    now()->serialIn( STOP, next_cs );
                }
                else
                {
                    now()->serialIn( START, next_cs );
                }
                last_cs = next_cs;
                continue;
            }
            else if(last_cs > next_cs)//cs fall
            {
                if( cs_pol == Polarity_Negtive)
                {
                    now()->serialIn( START, next_cs );
                 }
                else
                {
                    mpNow->serialIn( STOP, next_cs );
                }
                last_cs = next_cs;
                continue;
            }

            /* sample by rise or fall clock */
            if(last_clk < next_clk) //clk rise
            {
                next_dat = pDat[i] & m32Masks[j] ? 1 : 0;

                if( dat_pol == Polarity_Negtive)
                {
                    next_dat =!next_dat;
                }
                if( clk_edge == EDGE_RISING)
                {
                    now()->serialIn( SAMP, next_dat );
                }
                else
                {
                    mpNow->serialIn( SampleOver, next_dat );
                }
                last_clk = next_clk;
            }
            else if(last_clk > next_clk)//clk fall
            {
                next_dat = pDat[i] & m32Masks[j] ? 1 : 0;

                if( dat_pol == Polarity_Negtive)
                {
                    next_dat =!next_dat;
                }
                if( clk_edge == EDGE_FALLING)
                {
                    now()->serialIn( SAMP, next_dat );
                }
                else
                {
                    mpNow->serialIn( SampleOver, next_dat );
                }
                last_clk = next_clk;
            }
            mCounter.count();
        }
        i += 1;
    }
    return true;
}



int CSpiDecoder::setEventTable(CTable* table)
{
    //! head
    QList<quint32> lstHead;
    //! layout
    QList<quint32> lstLayout;

    lstHead.append( MSG_DECODE_EVT_TIME );
    lstLayout.append( 100 );
    if(mpSpiCfg->mMISOSrc != chan_none)
    {
        lstHead.append( MSG_DECODE_EVT_MISO );
        lstHead.append( MSG_DECODE_EVT_MISOERR );
        lstLayout.append( 100 );
        lstLayout.append( 100 );
    }
    if(mpSpiCfg->mMOSISrc != chan_none)
    {
        lstHead.append( MSG_DECODE_EVT_MOSI );
        lstHead.append( MSG_DECODE_EVT_MOSIERR );
        lstLayout.append( 100 );
        lstLayout.append( 100 );
    }
    table->setHead( lstHead );

    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_SPI);

    table->setTitle( pTitle );
    return ERR_NONE;
}
