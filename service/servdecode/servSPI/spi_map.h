#ifndef SPI_MAP_H
#define SPI_MAP_H

#include "../servdecode/servdecode.h"

on_set_int_int  (MSG_DECODE_SPI_CS,     &servDec::setSpiCsSrc),
on_get_int      (MSG_DECODE_SPI_CS,     &servDec::getSpiCsSrc),

on_set_int_ll   (MSG_DECODE_CS_THRE,    &servDec::setSpiCsAbsThre),
on_get_ll       (MSG_DECODE_CS_THRE,    &servDec::getSpiCsAbsThre),

on_set_int_int  (MSG_DECODE_SPI_CLK,    &servDec::setSpiClkSrc),
on_get_int      (MSG_DECODE_SPI_CLK,    &servDec::getSpiClkSrc),

on_set_int_int  (MSG_DECODE_SPI_MISO,   &servDec::setSpiMisoSrc),
on_get_int      (MSG_DECODE_SPI_MISO,   &servDec::getSpiMisoSrc),

on_set_int_int  (MSG_DECODE_SPI_MOSI,   &servDec::setSpiMosiSrc),
on_get_int      (MSG_DECODE_SPI_MOSI,   &servDec::getSpiMosiSrc),

on_set_int_ll   (MSG_DECODE_CLK_THRE,   &servDec::setSpiClkAbsThre),
on_get_ll       (MSG_DECODE_CLK_THRE,   &servDec::getSpiClkAbsThre),

on_set_int_ll   (MSG_DECODE_MISO_THRE,  &servDec::setSpiMisoAbsThre),
on_get_ll       (MSG_DECODE_MISO_THRE,  &servDec::getSpiMisoAbsThre),

on_set_int_ll   (MSG_DECODE_MOSI_THRE,  &servDec::setSpiMosiAbsThre),
on_get_ll       (MSG_DECODE_MOSI_THRE,  &servDec::getSpiMosiAbsThre),

on_set_int_bool (MSG_DECODE_SPI_MODE,   &servDec::setSpiMode),
on_get_bool     (MSG_DECODE_SPI_MODE,   &servDec::getSpiMode),

on_set_int_bool (MSG_DECODE_CS_POL,     &servDec::setSpiCsPN),
on_get_bool     (MSG_DECODE_CS_POL,     &servDec::getSpiCsPN),

on_set_int_bool (MSG_DECODE_CLK_EDGE,   &servDec::setSpiClkEdge),
on_get_bool     (MSG_DECODE_CLK_EDGE,   &servDec::getSpiClkEdge),

on_set_int_bool (MSG_DECODE_SPI_ENDIAN, &servDec::setSpiEndian),
on_get_bool     (MSG_DECODE_SPI_ENDIAN, &servDec::getSpiEndian),

on_set_int_bool (MSG_DECODE_DAT_POL,    &servDec::setSpiDataPN),
on_get_bool     (MSG_DECODE_DAT_POL,    &servDec::getSpiDataPN),

on_set_int_ll   (MSG_DECODE_SPI_TMO,    &servDec::setSpiTimeout),
on_get_ll       (MSG_DECODE_SPI_TMO,    &servDec::getSpiTimeout),

on_set_int_int  (MSG_DECODE_SPI_WIDTH,  &servDec::setSpiDataWidth),
on_get_int      (MSG_DECODE_SPI_WIDTH,  &servDec::getSpiDataWidth),

#endif // SPI_MAP_H

