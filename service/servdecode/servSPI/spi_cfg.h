#ifndef CSPICFG_H
#define CSPICFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;

#define SPI_MODE_CS  (0)
#define SPI_MODE_TMO (1)
struct CSpiCfg
{
    void reset();
    void serialIn(CStream&);
    void serialOut(CStream&);
    qint64  getTimeoutSample();
    void setTimeoutSample(qint64 sample);

    Chan mCsSrc;
    Chan mClkSrc;
    Chan mMISOSrc;
    Chan mMOSISrc;
    bool mMode;
    bool mCsPN;    
    bool mClkEdge;
    bool mEndian;
    bool mDatPN;
    int  mDataWidth;

    qint64 mTimeout;
    qint64 mTimeoutPoints;
};

#endif // CSPICFG_H
