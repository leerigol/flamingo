#ifndef CSpiDecoder_H
#define CSpiDecoder_H


#include "spi_cfg.h"
#include "../decoder.h"
#include "../decTool/DecState.h"



class CSpiDecoder : public CDecoder,public CDecState
{
public:
     CSpiDecoder();
    ~CSpiDecoder();

public:
    void rst();
    bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
    int  setEventTable(CTable* table);
public:
    static CSpiCfg* mpSpiCfg;
private:
    bool decode_cs(Chan cs, Chan clk, Chan dat);
    bool decode_tmo(Chan clk, Chan dat);
};

class CSpiIdle : public CSpiDecoder
{
public:
    CSpiIdle();
    virtual ~CSpiIdle();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
};

class CSpiStart : public CSpiDecoder
{
public:
    CSpiStart();
    virtual ~CSpiStart();

public:
    void onEnter( int bitVal );
    void onExit(int bitVal);
    void serialIn( int event, int bitVal );
};

class CSpiData: public CSpiDecoder
{
public:
    CSpiData();
    virtual ~CSpiData();

protected:
    bool mDataErr;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};


class CSpiStop : public CSpiDecoder
{
public:
    CSpiStop();
    virtual ~CSpiStop();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
};


#endif // CSpiDecoder_H
