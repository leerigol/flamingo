#include <QDebug>
#include "spi_fsm.h"

//! CSpiIdle
CSpiIdle::CSpiIdle()
{}
CSpiIdle::~CSpiIdle()
{}

void CSpiIdle::onEnter( int /*bitVal*/ )
{
}

void CSpiIdle::serialIn( int event, int bitVal )
{
    if( event == START ||
        event == SPITMO )
    {
        toNext( bitVal );
    }
}

//! CSpiStart
CSpiStart::CSpiStart()
{}
CSpiStart::~CSpiStart()
{}
void CSpiStart::onEnter(int /*bitVal*/)
{
}

void CSpiStart::onExit(int /*bitVal*/)
{
}
void CSpiStart::serialIn( int event, int bitVal )
{
    if( SAMP == event)
    {
        toNext( bitVal );
    }
}

//! CSpiData
CSpiData::CSpiData()
{}
CSpiData::~CSpiData()
{}

void CSpiData::onEnter(int bitVal)
{
    mCount = 0;
    mData  = 0;
    mDataErr = false;
    mCounter.begin();
    mPosition = mCounter.now();

    mData = bitVal;//LSB
    mCount++;
}

void CSpiData::serialIn(int event,int bitVal)
{    
    if( event == SAMP )
    {
        if( mpSpiCfg->mEndian == msb )
        {
            mData = (mData  << 1) | bitVal;
        }
        else
        {
            mData |= ( bitVal << mCount );
        }
        mCount++;
    }
    else if( event == SampleOver )
    {
        if ((int)mCount == mpSpiCfg->mDataWidth)
        {
            toPrev( bitVal );
        }
    }
    else if( event == SPITMO || event == STOP )
    {
        toNext( bitVal );
    }
}

void CSpiData::onExit(int /*bitVal*/)
{
    mSpan = mCounter.getDur();

    CDecPayloadData* pData = new CDecPayloadData();
    Q_ASSERT( NULL != pData );
    if(mDataErr)
    {
        pData->busContent_SetColor(ERR_DATA_COLOR);
    }
    pData->setPosSpan( mPosition, mSpan );
    pData->setPayload( mData  , mpSpiCfg->mDataWidth);
    AddBlock(CDecPayloadData,pData);

    mpTable->append(new CCellTime(mPosition));
    mpTable->append(new CCellDW(mData,mCount));
    if(mDataErr)
    {
        mpTable->append(new CCellStr(MSG_DECODE_EVT_ERR));//Err
    }
    else
    {
        mpTable->append(new CCellStr(0));//NULL
    }

    AddOneFrame();
}
