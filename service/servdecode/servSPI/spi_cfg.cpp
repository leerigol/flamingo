#include "../../include/dsocfg.h"
#include "../../servdso/sysdso.h"
#include "../../servhori/servhori.h"
#include "../../service_name.h"

#include "spi_cfg.h"
#include "../servdecode.h"


void CSpiCfg::reset()
{
    mMode   =   SPI_MODE_TMO;

    mClkSrc =   chan1;
    mClkEdge=   EDGE_RISING;

    mMISOSrc=   chan2;
    mMOSISrc=   chan_none;
    mDatPN  =   Polarity_Positive;
    mDataWidth= 8;

    mCsSrc  =   chan3;
    mCsPN   =   Polarity_Negtive;
    mTimeout=   time_us(1);
    mEndian =   msb;
}

void CSpiCfg::serialIn(CStream & stream)
{
    int temp = 0;
    stream.read("mClkSrc",  temp);
    mClkSrc = (Chan)temp;
    stream.read("mClkEdge", mClkEdge);

    stream.read("mMISOSrc", temp);
    mMISOSrc = (Chan)temp;

    stream.read("mMOSISrc", temp);
    mMOSISrc = (Chan)temp;

    stream.read("mCsSrc",   temp);
    mCsSrc = (Chan)temp;
    stream.read("mCsPN",    mCsPN);

    stream.read("mDatPN",   mDatPN);
    stream.read("mDataWid", mDataWidth);
    stream.read("mTimeout", mTimeout);
    stream.read("mEndian",  mEndian);
}

void CSpiCfg::serialOut(CStream & stream)
{
    stream.write("mClkSrc",  (int)mClkSrc);
    stream.write("mClkEdge", mClkEdge);

    stream.write("mMISOSrc", (int)mMISOSrc);
    stream.write("mMOSISrc", (int)mMOSISrc);


    stream.write("mCsSrc",   (int)mCsSrc);
    stream.write("mCsPN",    mCsPN);

    stream.write("mDatPN",   mDatPN);
    stream.write("mDataWid", mDataWidth);
    stream.write("mTimeout", mTimeout);
    stream.write("mEndian",  mEndian);
}


/* sample x time . scr sample= 100/scale */
qint64 CSpiCfg::getTimeoutSample()
{
    return mTimeoutPoints;
}

void CSpiCfg::setTimeoutSample(qint64 sample)
{
    mTimeoutPoints = sample * mTimeout *1e-12;
}

//!SPI
DsoErr servDec::setSpiCsSrc(Chan src)
{
    mServSPI.mCsSrc = src;

    if(src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_CS_THRE, false);
    }
    else
    {
        getChThreshold1( src );
        mUiAttr.setEnable(MSG_DECODE_CS_THRE, true);
        setuiChange( MSG_DECODE_CS_THRE );
    }
    return ERR_NONE;
}

Chan servDec::getSpiCsSrc()
{    
    return mServSPI.mCsSrc;
}


DsoErr servDec::setSpiClkSrc(Chan src)
{
    mServSPI.mClkSrc = src;
    if(src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_CLK_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_CLK_THRE, true);
        setuiChange( MSG_DECODE_CLK_THRE );
    }    
    return ERR_NONE;
}

Chan servDec::getSpiClkSrc()
{    
    return mServSPI.mClkSrc;
}


DsoErr servDec::setSpiMisoSrc(Chan src)
{
    mServSPI.mMISOSrc = src;
    if(src == chan_none || src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_MISO_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_MISO_THRE, true);
        setuiChange( MSG_DECODE_MISO_THRE );
    }
    if(src == chan_none)
    {
        mUiAttr.setVisible(MSG_DECODE_SPI_MOSI, chan_none, false);
    }
    else
    {
        mUiAttr.setVisible(MSG_DECODE_SPI_MOSI, chan_none, true);
    }
    return ERR_NONE;
}

Chan servDec::getSpiMisoSrc()
{
    return mServSPI.mMISOSrc;
}

DsoErr servDec::setSpiMosiSrc(Chan src)
{
    mServSPI.mMOSISrc = src;
    if(src == chan_none || src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_MOSI_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_MOSI_THRE, true);
        setuiChange( MSG_DECODE_MOSI_THRE );
    }

    if(src == chan_none)
    {
        mUiAttr.setVisible(MSG_DECODE_SPI_MISO, chan_none, false);
    }
    else
    {
        mUiAttr.setVisible(MSG_DECODE_SPI_MISO, chan_none, true);
    }
    return ERR_NONE;
}

Chan servDec::getSpiMosiSrc()
{    
    return mServSPI.mMOSISrc;
}

DsoErr servDec::setSpiMode(bool mode)
{
    mServSPI.mMode = mode;
    return ERR_NONE;
}

bool servDec::getSpiMode()
{
    return mServSPI.mMode;
}


DsoErr servDec::setSpiCsPN(bool pn)
{
    mServSPI.mCsPN = pn;
    return ERR_NONE;
}

bool servDec::getSpiCsPN()
{
    return mServSPI.mCsPN;
}

DsoErr servDec::setSpiTimeout(qint64 time)
{
    mServSPI.mTimeout = time;
    return ERR_NONE;
}

qint64 servDec::getSpiTimeout()
{
    //! update attr
    setuiStep( getTimeStep(mServSPI.mTimeout) );
    setuiZVal( time_us(1) );
    setuiMinVal( time_ns(8) );
    setuiMaxVal(time_s(10));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);

    setuiPostStr( "s" );
    return mServSPI.mTimeout;
}

DsoErr servDec::setSpiClkEdge(bool edge)
{
    mServSPI.mClkEdge = edge;
    return ERR_NONE;
}

bool servDec::getSpiClkEdge()
{
    return mServSPI.mClkEdge;
}

DsoErr servDec::setSpiEndian(bool endian)
{
    mServSPI.mEndian = endian;
    return ERR_NONE;
}
bool servDec::getSpiEndian()
{
    return mServSPI.mEndian;
}

DsoErr servDec::setSpiDataPN(bool pn)
{
    mServSPI.mDatPN = pn;
    return ERR_NONE;
}

bool servDec::getSpiDataPN()
{
    return mServSPI.mDatPN;
}


DsoErr servDec::setSpiDataWidth(int datawid)
{
    mServSPI.mDataWidth = datawid;
    return ERR_NONE;
}

int servDec::getSpiDataWidth()
{
    setuiZVal(8);
    setuiMinVal( 4 );
    setuiMaxVal(  32 );

    return mServSPI.mDataWidth;
}

DsoErr servDec::setSpiCsAbsThre(qint64 t)
{
    setChThreshold( mServSPI.mCsSrc, t);
    return ERR_NONE;
}

DsoErr servDec::setSpiClkAbsThre(qint64 t)
{    
    setChThreshold( mServSPI.mClkSrc, t);
    return ERR_NONE;
}

DsoErr servDec::setSpiMisoAbsThre(qint64 t)
{
    setChThreshold( mServSPI.mMISOSrc, t);
    return ERR_NONE;
}

DsoErr servDec::setSpiMosiAbsThre(qint64 t)
{
    setChThreshold( mServSPI.mMOSISrc, t);
    return ERR_NONE;
}

qint64 servDec::getSpiCsAbsThre()
{
    Chan s = mServSPI.mCsSrc;

    return getChThreshold(s);
}

qint64 servDec::getSpiClkAbsThre()
{
    Chan s = mServSPI.mClkSrc;
    return getChThreshold(s);
}

qint64 servDec::getSpiMisoAbsThre()
{
    Chan s = mServSPI.mMISOSrc;
    return getChThreshold(s);
}

qint64 servDec::getSpiMosiAbsThre()
{
    Chan s = mServSPI.mMOSISrc;
    return getChThreshold(s);
}
