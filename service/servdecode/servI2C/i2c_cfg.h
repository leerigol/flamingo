#ifndef CI2CCFG_H
#define CI2CCFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;

struct CI2CCfg
{
    void reset();
    void serialIn(CStream&);
    void serialOut(CStream&);

    Chan mClkSrc;
    Chan mDataSrc;
    bool mWithRW;
    bool mExch;
};


#endif // CI2CCFG_H
