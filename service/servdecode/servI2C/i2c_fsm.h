// DecI2C.h: interface for the CDecI2C class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(DEC_I2C)
#define DEC_I2C


#include "../decTool/DecState.h"
#include "../decoder.h"
#include "i2c_cfg.h"


#define TYPE_MASTCODE     0
#define TYPE_ADDR_10      1

#define TYPE_ADDR_7       3
#define TYPE_BYTEERR      4    //Addr ,Data or Master Code Err

#define TYPE_DATA         5
#define TYPE_RESTART      6
#define TYPE_END          7
#define TYPE_DATAERR      8

#define TYPE_WBIT         0
#define TYPE_RBIT         1
#define TYPE_RWERR        2     //Write,or Read Err

#define TYPE_ACK          0
#define TYPE_UACK         1

class CI2CDecoder : public CDecoder,public CDecState
{
public:
     CI2CDecoder();
    ~CI2CDecoder();

public:
    void rst();
    bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
    int  setEventTable(CTable* table);
public:
    static CI2CCfg* mpI2CCfg;
private:
    bool decode(Chan,Chan);
};

class CI2CIdle : public CI2CDecoder
{
public:
    CI2CIdle();
    virtual ~CI2CIdle();

public:
    void onEnter(int bitVal);
    void serialIn( int event, int bitval );
    void onExit( int bitVal );

private:
    int mStatus;
};


class CI2CAddr : public CI2CDecoder
{
public:
    CI2CAddr();
    virtual ~CI2CAddr();

protected:
    unsigned int mType;
    unsigned int mRW;
    unsigned int mDataLen;

    unsigned int mACK;
    unsigned int mAckPos;
    unsigned int mAckSpan;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitval );
};

class CI2CData : public CI2CDecoder
{
public:
    CI2CData();
    virtual ~CI2CData();

protected:
    unsigned int mType;
    unsigned int mACK;
    unsigned int mAckPos;
    unsigned int mAckSpan;

public:
    void onEnter( int bitVal );//added
    void onExit( int bitVal );//added3x
    void serialIn( int event, int bitval );
};



#endif // !defined(AFX_DECUART_H__554D854D_13AA_4B8F_BDFA_670AC9DB619C__INCLUDED_)
