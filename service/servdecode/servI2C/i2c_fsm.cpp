// DecI2C.cpp: implementation of the CDecI2C class.
//
//////////////////////////////////////////////////////////////////////
#include <QDebug>
#include "i2c_fsm.h"

//! CI2CIdle TO 2
CI2CIdle::CI2CIdle()
{
}
CI2CIdle::~CI2CIdle()
{}

void CI2CIdle::onEnter(int /*bitVal*/)
{
    mStatus = -1;
}

void CI2CIdle::serialIn( int event, int bitVal )
{
    if( event == START )
    {
        toNext( bitVal );
    }
}

void CI2CIdle::onExit( int /*bitVal*/ )
{

}

CI2CAddr::CI2CAddr()
{}
CI2CAddr::~CI2CAddr()
{}

void CI2CAddr::onEnter( int /*bitVal*/ )
{
    mType = TYPE_ADDR_7;
    mRW   = TYPE_RWERR;

    mData    = 0;//MSB
    mCount   = 0;
    mDataLen = 0;
}


void CI2CAddr::serialIn( int event, int bitVal )
{
    if(RISE == event )
    {
        if( mCount == 0 )
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }
        if(mCount<8 || (mCount >= 9 && mCount<17 && mType == TYPE_ADDR_10) )
        {
            mData = (mData  << 1) | bitVal;//MSB
            mDataLen++;
        }
        mCount++;
        if (mCount == 8)
        {
            if( (mData & 0xF8 ) == 0xF0 ) //start with 1111 0xxx
            {
                mType = TYPE_ADDR_10;

                mData = mData & 0x7; //remove 1111
                mData = mData >> 1;
                mDataLen--;
                mRW = bitVal;//R/W bit
            }
            //for bug1736
//            else if( (mData & 0xf8) == 0x8 )// Master Code 0000 1xx
//            {
//                mType = TYPE_MASTCODE;
//            }
            else
            {
                mType = TYPE_ADDR_7;//7bit Addr
                if(!mpI2CCfg->mWithRW)
                {
                    mData = mData  >> 1;
                    mDataLen--;
                }
                mRW = bitVal;//R/W bit
            }
        }
    }
    else if( event == FALL )
    {
        //end of addr and start of ack
        {
            if ( (mCount == 8 && mType != TYPE_ADDR_10) || mCount == 17)
            {
                mSpan = mCounter.getDur();
                mCounter.begin();
                mAckPos = mCounter.now();
            }
            else if( mCount == 9 || mCount == 18) //ack span
            {
                mAckSpan = mCounter.getDur();
                mACK = bitVal;
                if(mType == TYPE_ADDR_7 || mCount == 18)
                {
                    toNext( bitVal );
                }
//                else if(mCount ==9 &&
//                        (mType == TYPE_MASTCODE || mType == TYPE_ADDR_10) )
//                {
//                    toSelf( bitVal );
//                }
            }
        }
    }
}

void CI2CAddr::onExit( int /*bitVal*/ )
{
    switch (mType)
    {
        case TYPE_ADDR_7:
        {
            CDecPayloadCmd* pCmd = new  CDecPayloadCmd();
            Q_ASSERT( NULL != pCmd );

            pCmd->setPosSpan( mPosition, mSpan );

            int dir = MSG_DECODE_EVT_READ;
            int txt = TEXT_R;
            if(mRW==TYPE_WBIT)
            {
                txt = TEXT_W;
                dir = MSG_DECODE_EVT_WRITE;
            }
            pCmd->setPayload(mData,mDataLen,CAN_ID_COLOR, txt);
            AddBlock(CDecPayloadCmd,pCmd);

            mpTable->append(new CCellTime(mPosition));
            mpTable->append(new CCellStr(dir));
            mpTable->append(new CCellInfo(mData,mDataLen));
            break;
        }
        case TYPE_MASTCODE:
        {
            CDecPayloadCmd* pCmd = new  CDecPayloadCmd();
            Q_ASSERT( NULL != pCmd );
            pCmd->setPayload(mData,mDataLen,IIC_MAST_COLOR,TEXT_MAST);
            pCmd->setPosSpan( mPosition, mSpan );
            AddBlock(CDecPayloadCmd,pCmd);

            mpTable->append(new CCellTime(mPosition));
            mpTable->append(new CCellStr(MSG_DECODE_EVT_WRITE));
            mpTable->append(new CCellInfo(mData,mDataLen));
            break;
        }

        case TYPE_ADDR_10:
        {
            CDecPayloadCmd* pCmd = new  CDecPayloadCmd();
            Q_ASSERT( NULL != pCmd );
            pCmd->setPosSpan( mPosition, mSpan );

            if(mpI2CCfg->mWithRW)
            {
                int low = mData & 0xff;
                mData = mData >> 8;
                mData = (mData << 1) | mRW;
                mData = (mData << 8) | low;
                mDataLen++;
            }
            if( mDataLen > 4  ) //just for show
            {
                mDataLen -= 4;
            }
            if(mRW==TYPE_WBIT)
            {
                pCmd->setPayload(mData,mDataLen,CAN_ID_COLOR,TEXT_W);
            }
            else
            {
                pCmd->setPayload(mData,mDataLen,CAN_ID_COLOR,TEXT_R);
            }
            AddBlock(CDecPayloadCmd,pCmd);

            int dir = MSG_DECODE_EVT_READ;
            if(mRW==TYPE_WBIT)
            {
                dir = MSG_DECODE_EVT_WRITE;
            }
            mpTable->append(new CCellTime(mPosition));
            mpTable->append(new CCellStr(dir));
            mpTable->append(new CCellInfo(mData,mDataLen));
            break;
        }
    }

    //ack
    CDecPayloadCmd* pCmd = new  CDecPayloadCmd();
    Q_ASSERT( NULL != pCmd );
    pCmd->setPosSpan( mAckPos, mAckSpan );
    if( mACK == TYPE_ACK )
    {
        pCmd->setPayload(mACK,1,COMM_ACK_COLOR,TEXT_ACK);
    }
    else
    {
        pCmd->setPayload(mACK,1,ICON_RED_COLOR,TEXT_ACK);
    }
    AddBlock(CDecPayloadCmd,pCmd);

    mCellData = new CCellDW();
    mpTable->append(mCellData);

    AddOneFrame();
}


//! CI2CData(Data OR Sr)TO(14 or 18)
CI2CData::CI2CData()
{}
CI2CData::~CI2CData()
{}

void CI2CData::onEnter(int /*bitVal*/)
{
    mCounter.begin();
    mPosition = mCounter.now();

    mType = TYPE_DATA;
    mCount = 0;
    mData = 0;
}


void CI2CData::serialIn( int event, int bitVal )//added
{
    switch( event )
    {
        case START:
        {
            mType = TYPE_RESTART;
            toPrev(bitVal);
            mpTable->append(new CCellInfo(mACK,1));
            break;
        }

        case RISE: //SAMPLE
        {
            if(mCount <8)
            {
                mType = TYPE_DATA;
                mData = (mData  << 1) | bitVal;//MSB
            }
            mCount++;
            break;
        }

        case FALL:
        {
            if( mType == TYPE_DATA ) //end of addr and start of ack
            {
                if (mCount == 8)
                {
                    mSpan = mCounter.getDur();
                    mCounter.begin();
                    mAckPos = mCounter.now();
                }
                else if( mCount == 9 ) //ack span
                {
                    mAckSpan = mCounter.getDur();
                    mACK  = bitVal;
                    mCount= 8;
                    toSelf( bitVal );
                }
            }
            break;
        }

        case STOP:
            mType = TYPE_END;
            toNext( bitVal );
            mpTable->append(new CCellInfo(mACK,1));
            break;
    }

}

void CI2CData::onExit(int /*bitVal*/)
{
    if( mType == TYPE_DATA )
    {
        CDecPayloadData* pData = new  CDecPayloadData();
        Q_ASSERT( NULL != pData );
        pData->setPosSpan( mPosition, mSpan );
        pData->setPayload( mData );
        AddBlock(CDecPayloadData,pData);

        if(mCellData->size() > 1)
        {
            mCellData->append(new CCellStr(1));//add space
        }
        mCellData->append(new CCellDW(mData,mCount));


        CDecPayloadCmd* pCmd = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pCmd );
        pCmd->setPosSpan( mAckPos, mAckSpan );
        if( mACK == TYPE_ACK )
        {
            pCmd->setPayload(mACK,1,COMM_ACK_COLOR,TEXT_ACK);
        }
        else
        {
            pCmd->setPayload(mACK,1,ICON_RED_COLOR,TEXT_ACK);
        }
        AddBlock(CDecPayloadCmd,pCmd);
    }
}
