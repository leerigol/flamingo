#include "i2c_cfg.h"
#include "../servdecode.h"

//! I2C

void CI2CCfg::reset()
{
    mClkSrc = chan1;
    mDataSrc= chan2;
    mWithRW = false;
    mExch   = false;
}

void CI2CCfg::serialIn(CStream &stream)
{
    int temp = 0;
    stream.read("mClkSrc",    temp);
    mClkSrc = (Chan)temp;

    stream.read("mDataSrc",   temp);
    mDataSrc = (Chan)temp;

    stream.read("mWithRW",    mWithRW);

}

void CI2CCfg::serialOut(CStream &stream)
{
    stream.write("mClkSrc",    (int)mClkSrc);
    stream.write("mDataSrc",   (int)mDataSrc);
    stream.write("mWithRW",    mWithRW);
}

/*I2C Set/Get*/
DsoErr servDec::setI2cClkSrc(Chan clk)
{
    mServI2C.mClkSrc = clk;
    if(clk > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_SCL_THRE, false);
    }
    else
    {
        getChThreshold1( clk );
        mUiAttr.setEnable(MSG_DECODE_SCL_THRE, true);
        setuiChange( MSG_DECODE_SCL_THRE );
    }

    return ERR_NONE;
}


Chan servDec::getI2cClkSrc()
{
    return mServI2C.mClkSrc;
}


DsoErr servDec::setI2cDatSrc(Chan data)
{
    mServI2C.mDataSrc = data;
    if(data > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_SDA_THRE, false);
    }
    else
    {
        getChThreshold1( data );
        mUiAttr.setEnable(MSG_DECODE_SDA_THRE, true);
        setuiChange( MSG_DECODE_SDA_THRE );
    }

    return ERR_NONE;
}

Chan servDec::getI2cDatSrc()
{
    return mServI2C.mDataSrc;
}

DsoErr servDec::setI2cClkThre(qint64 t)
{
    setChThreshold(mServI2C.mClkSrc, t);
    return ERR_NONE;
}

qint64 servDec::getI2cClkThre()
{
    Chan s = mServI2C.mClkSrc;
    return getChThreshold(s);
}

DsoErr servDec::setI2cDatThre(qint64 t)
{
    setChThreshold(mServI2C.mDataSrc, t);
    return ERR_NONE;
}

qint64 servDec::getI2cDatThre()
{
    Chan s = mServI2C.mDataSrc;
    return getChThreshold(s);
}


DsoErr servDec::setI2cIncRW(bool incrw)
{
    mServI2C.mWithRW = incrw;
    return ERR_NONE;
}

bool servDec::getI2cIncRW()
{
    return mServI2C.mWithRW;
}

DsoErr servDec::setI2cChange(bool b)
{
    mServI2C.mExch = b;
    async(MSG_DECODE_I2C_SCL, mServI2C.mDataSrc);
    async(MSG_DECODE_I2C_SDA, mServI2C.mClkSrc);
    return ERR_NONE;
}

bool servDec::getI2cChange()
{
    return mServI2C.mExch;
}
