#include "i2c_fsm.h"
#include "../servdecode.h"

static CI2CIdle   fsm_i2c_idle;
static CI2CAddr   fsm_i2c_addr;
static CI2CData   fsm_i2c_data;

CI2CCfg* CI2CDecoder::mpI2CCfg;

CI2CDecoder::CI2CDecoder()
{
     mpNow = &fsm_i2c_idle;
     fsm_i2c_idle.setNextStat( &fsm_i2c_addr );
     fsm_i2c_addr.setNextStat( &fsm_i2c_data );
     fsm_i2c_data.setNextStat( &fsm_i2c_idle );
}

CI2CDecoder::~CI2CDecoder()
{
}


void CI2CDecoder::rst()
{
   mpNow = &fsm_i2c_idle;
   mCounter.rst();
   mClock.stop();
   clearBlock();

   mpI2CCfg = &mpCurrServ->mServI2C;
   mpTable = new CTable();
   setEventTable(mpTable);
}



//! rst to idle
bool CI2CDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{
    bool ret = false;
    if( decode(mpI2CCfg->mClkSrc, mpI2CCfg->mDataSrc) )
    {
        CDecLine* line = new CDecLine();

        //if( mpCurrServ->getLabelOn())
        {
            QString label = "I2C";
            line->setLabel(label);
        }

        mLineView.makeLine(*line, mBlocklist);
        bus->append( line );
        if( zoomBus )
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        ret = true;
    }

    return ret;
}

bool CI2CDecoder::decode(Chan scl, Chan sda)
{

    unsigned int u32Dat, u32Clk;

    uint last_sda;
    uint next_sda;
    uint last_scl;
    uint next_scl;

    uchar* pSCL = mpCurrServ->getChanData( scl );
    uchar* pSDA = mpCurrServ->getChanData( sda );
    if( !pSCL || !pSDA )
    {
        return false;
    }

    last_sda = *pSDA;
    last_scl = *pSCL;

    last_sda = ( last_sda & 0x80 ) ? 1 : 0;
    last_scl = ( last_scl & 0x80 ) ? 1 : 0;


    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Dat, pSDA + i, 4 );
        memcpy( &u32Clk, pSCL + i, 4 );

        //! no change
        if ( u32Clk == m32IdleMask[last_scl] &&
             u32Dat == m32IdleMask[last_sda])
        {
            i += 4;
            mCounter.count( 32 );
            continue;
        }

        //! no change in u8
        if (( (u32Clk & 0xff) == m8IdleMask[last_scl] ) &&
            ( (u32Dat & 0xff) == m8IdleMask[last_sda] ))
        {
            i += 1;
            mCounter.count(8);
            continue;
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_sda = u32Dat & m32Masks[j] ? 1 : 0;
            next_scl = u32Clk & m32Masks[j] ? 1 : 0;

            //clock rise samp
            if( last_scl < next_scl ) // 0-1 rise-clock for sampling
            {
                mpNow->serialIn( RISE, next_sda );
            }
            else if(last_scl > next_scl )
            {
                mpNow->serialIn( FALL, next_sda );
            }

            else if(last_scl == 1 && next_scl == 1)
            {
                if(last_sda < next_sda)// 0-1 rise-data for checking stop
                {
                    mpNow->serialIn( STOP, next_sda );
                }
                //data fall
                else if(last_sda > next_sda)// 1-0 fall-data for checking start
                {
                    mpNow->serialIn( START, next_sda );
                }
            }

            last_sda = next_sda;
            last_scl = next_scl;
            mCounter.count();
        }
        i += 1;
    }
    return true;
}

int CI2CDecoder::setEventTable(CTable* table)
{
    QList<quint32> lstHead;
    lstHead.append( MSG_DECODE_EVT_TIME );
    lstHead.append( MSG_DECODE_EVT_WR );
    lstHead.append( MSG_DECODE_EVT_ADDR );
    lstHead.append( MSG_DECODE_EVT_DATA );
    lstHead.append( MSG_DECODE_EVT_ACK );
    table->setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 30 );//id
    lstLayout.append( 100 );//time
    lstLayout.append( 80 );//wr
    lstLayout.append( 80 );//addr
    lstLayout.append( 200 );//data

    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_I2C);
    table->setTitle( pTitle );
    return ERR_NONE;
}
