#ifndef I2C_MAP_H
#define I2C_MAP_H

#include "../servdecode/servdecode.h"

on_set_int_int  (MSG_DECODE_I2C_SCL,        &servDec::setI2cClkSrc),
on_get_int      (MSG_DECODE_I2C_SCL,        &servDec::getI2cClkSrc),

on_set_int_int  (MSG_DECODE_I2C_SDA,        &servDec::setI2cDatSrc),
on_get_int      (MSG_DECODE_I2C_SDA,        &servDec::getI2cDatSrc),

on_set_int_int  (MSG_DECODE_FORMAT,         &servDec::setBusFormat),
on_get_int      (MSG_DECODE_FORMAT,         &servDec::getBusFormat),

on_set_int_ll   (MSG_DECODE_SCL_THRE,       &servDec::setI2cClkThre),
on_get_ll       (MSG_DECODE_SCL_THRE,       &servDec::getI2cClkThre),

on_set_int_ll   (MSG_DECODE_SDA_THRE,       &servDec::setI2cDatThre),
on_get_ll       (MSG_DECODE_SDA_THRE,       &servDec::getI2cDatThre),

on_set_int_bool (MSG_DECODE_I2C_RW,         &servDec::setI2cIncRW),
on_get_bool     (MSG_DECODE_I2C_RW,         &servDec::getI2cIncRW),

on_set_int_bool (MSG_DECODE_I2C_EXC,        &servDec::setI2cChange),
on_get_bool     (MSG_DECODE_I2C_EXC,        &servDec::getI2cChange),


#endif // I2C_MAP_H

