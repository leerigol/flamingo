#include "flex_fsm.h"

#include <QDebug>

//! CFlexIdle
CFlexIdle::CFlexIdle()
{}
CFlexIdle::~CFlexIdle()
{}

void CFlexIdle::onEnter( int /*bitVal*/ )
{

    //! clock start samp
    mClock.start( mpFlexCfg->getSamplePerBaud(),
                  mpFlexCfg->getSamplePerBaud()/2);

    mCount = 0;
    mData  = 0;
    mCounter.begin();
    mPosition = mCounter.now();
}

/*The transmission start sequence  (TSS).
 * a TSS that consists of a continuous LOW for a period*/
void CFlexIdle::serialIn( int event, int bitVal )
{
    if( event == SAMP )
    {
        if( bitVal == 0)
        {
            mCount++;
        }
        else
        {
            mCount = 0;
        }
    }
    else if( event == FALL )
    {
        mCounter.begin();
        mPosition = mCounter.now();
    }
    else if( event == RISE && mCount >= 3 && mCount <=15 )//not good maybe
    {
        toNext( bitVal );
    }
}

void CFlexIdle::onExit(int)
{
    CDecPayloadString* pData = new  CDecPayloadString();
    Q_ASSERT( NULL != pData );

    mSpan = mCounter.getDur();
    pData->setPosSpan( mPosition, mSpan );
    pData->setPayload( FLEX_TSS_DTS_COLOR, TEXT_TSS);
    AddBlock(CDecPayloadString,pData);

    mpTable->append(new CCellTime(mPosition));

    AddOneFrame();
}

//! CFlexHeader
CFlexHeader::CFlexHeader()
{}
CFlexHeader::~CFlexHeader()
{}

void CFlexHeader::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData  = 0;
    bFSS   = false;
    bBSS   = false;
    mCounter.begin();
    mPosition = mCounter.now();

    mHeaderStatus = HeaderIndicator;
    mHeaderCount  = 0;
    mHeaderData   = 0;
    mPayloadSize  = 0;
    mCrcCode.clear();
}

/*The frame start sequence  (FSS) is used to compensate for
 * a possible quantization error in the first byte start
sequence after the TSS. The FSS shall consist of one HIGH

The byte start sequence  (BSS) is used to provide bit stream
timing information to the receiving devices. The
BSS shall consist of one HIGH followed by one LOW
*/
void CFlexHeader::serialIn( int event, int bitVal )
{
    if( event ==SAMP )
    {        
        if( !bFSS )
        {
            bFSS = true;
            if( bitVal == 0 )
            {
                //qDebug() << "fss error";
            }
            mCounter.begin();
            mPosition = mCounter.now();
        }
        else
        {
            mCount ++;
            if( !bBSS )
            {
                mData = (mData << 1) | bitVal;
                if ( mCount == 2 )
                {
                    bBSS = true;
                    if (mData != 2 )
                    {
                        //qDebug() << "bss error";
                    }
                    mData = 0;
                }
            }
            else
            {
                if(mCount == 10)
                {
                    mCount=0;
                    bBSS = false;
                }

                mCrcCode.append(bitVal);
                switch( mHeaderStatus )
                {
                    case HeaderIndicator:
                    {
                        onIndicator( bitVal );
                    }
                    break;

                    case HeaderFrameID:
                    {

                        onFrameID( bitVal );
                    }
                    break;

                    case HeaderPayloadLength:
                    {
                        onPayloadLength( bitVal );
                    }
                    break;

                    case HeaderCRC:
                    {
                        onHeaderCRC( bitVal );
                    }
                    break;

                    case HeaderCycleCount:
                    {
                        onCycleCount( bitVal );

                        //end of header field
                        if( mHeaderStatus == HeaderIndicator )
                        {
                            return;
                        }
                    }
                    break;
                }
            }
        }
    }
}

void CFlexHeader::onIndicator(int bitVal )
{
    if( mHeaderCount == 0)
    {
        mReserved = bitVal;
    }
    else if( mHeaderCount == 1)
    {
        mPayloadPreamble = bitVal;
    }
    else if( mHeaderCount == 2 )
    {
        mNullFrame = bitVal;
    }
    else if( mHeaderCount == 3)
    {
        mSyncFrame = bitVal;
        //mCrcCode.append(bitVal);
    }
    else if( mHeaderCount == 4 )
    {
        mStartupFrame = bitVal;
        //mCrcCode.append(bitVal);
    }

    mHeaderCount ++;
    if(mHeaderCount == 1)
    {

    }
    else if( mHeaderCount == 5 )
    {
        CDecPayloadString* pCaption = new CDecPayloadString();
        Q_ASSERT( NULL != pCaption );

        mSpan = mCounter.getDur();
        pCaption->setPosSpan( mPosition, mSpan );
        if( mSyncFrame )
        {
            pCaption->setPayload(FLEX_INDICATOR_COLOR,TEXT_INDICATOR_SYNC);
        }
        else if( mStartupFrame )
        {
            pCaption->setPayload(FLEX_INDICATOR_COLOR,TEXT_INDICATOR_STARTUP);
        }
        else if( mNullFrame )
        {
            pCaption->setPayload(FLEX_INDICATOR_COLOR,TEXT_INDICATOR_NORMAL);
        }


        AddBlock(CDecPayloadString,pCaption);

        mHeaderData   = 0;
        mHeaderCount  = 0;
        mHeaderStatus = HeaderFrameID;
    }
}

void CFlexHeader::onFrameID(int bitVal )
{
    mHeaderData = (mHeaderData << 1 ) | bitVal;
    mHeaderCount++;

    if(mHeaderCount == 1)
    {
        mCounter.begin();
        mPosition = mCounter.now();
    }
    else if(mHeaderCount == 11)
    {
        CDecPayloadCmd* pData = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pData );

        int color = FLEX_FRAME_ID_COLOR;
        if( mHeaderData >= 1 && mHeaderData <= 2047 )
        {
            //normal ID
        }
        else
        {
            color = ERR_COLOR;
        }

        mSpan = mCounter.getDur();
        pData->setPosSpan( mPosition, mSpan );
        pData->setPayload( mHeaderData ,mHeaderCount,color, TEXT_FRAME_ID);
        AddBlock(CDecPayloadCmd,pData);

        mpTable->append(new CCellInfo(mHeaderData,mHeaderCount));

        mHeaderData  = 0;
        mHeaderCount = 0;
        mHeaderStatus = HeaderPayloadLength;
    }
}

void CFlexHeader::onPayloadLength(int bitVal)
{
    mHeaderData = (mHeaderData << 1 ) | bitVal;
    mHeaderCount++;

    if(mHeaderCount == 1)
    {
        mCounter.begin();
        mPosition = mCounter.now();
    }
    else if(mHeaderCount == 7)
    {
        CDecPayloadCmd* pData = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pData );

        mSpan = mCounter.getDur();
        pData->setPosSpan( mPosition, mSpan );
        pData->setPayload( mHeaderData ,mHeaderCount,FLEX_PL_COLOR, TEXT_PL);
        AddBlock(CDecPayloadCmd,pData);

        mpTable->append(new CCellInfo(mHeaderData,mHeaderCount));

        mPayloadSize = mHeaderData<<1;

        mHeaderData  = 0;
        mHeaderCount = 0;
        mHeaderStatus = HeaderCRC;
    }
}

void CFlexHeader::onHeaderCRC(int bitVal)
{
    mHeaderData = (mHeaderData << 1 ) | bitVal;
    mHeaderCount++;

    if(mHeaderCount == 1)
    {
        mCounter.begin();
        mPosition = mCounter.now();
    }
    else if(mHeaderCount == 11)
    {
        CDecPayloadCmd* pData = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pData );

        mSpan = mCounter.getDur();
        pData->setPosSpan( mPosition, mSpan );

        int color = FLEX_HCRC_COLOR;
        if( mHeaderData != getCRC(HeaderCRC) )
        {
            color = ERR_COLOR;
        }

        pData->setPayload( mHeaderData ,mHeaderCount,color, TEXT_HCRC);
        AddBlock(CDecPayloadCmd,pData);
        mpTable->append(new CCellInfo(mHeaderData,mHeaderCount));

        mHeaderData  = 0;
        mHeaderCount = 0;
        mHeaderStatus = HeaderCycleCount;
    }
}

void CFlexHeader::onCycleCount(int bitVal)
{
    mHeaderData = (mHeaderData << 1 ) | bitVal;
    mHeaderCount++;

    if(mHeaderCount == 1)
    {
        mCounter.begin();
        mPosition = mCounter.now();
    }
    else if(mHeaderCount == 6)
    {
        CDecPayloadCmd* pData = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pData );

        mSpan = mCounter.getDur();
        pData->setPosSpan( mPosition, mSpan );
        pData->setPayload( mHeaderData ,mHeaderCount,FLEX_CYC_COLOR, TEXT_CYCCNT);
        AddBlock(CDecPayloadCmd,pData);
        mpTable->append(new CCellInfo(mHeaderData,mHeaderCount));

        toNext( bitVal );

        mHeaderData  = 0;
        mHeaderCount = 0;
        mHeaderStatus = HeaderIndicator;
    }
}

void CFlexHeader::onExit(int /*bitVal*/)
{
    mCellData = new CCellDW();
    mpTable->append(mCellData);
}

//! CFlexPayload
CFlexPayload::CFlexPayload()
{}
CFlexPayload::~CFlexPayload()
{}

void CFlexPayload::onEnter( int /*bitVal*/ )
{
    mCount  = 0;
    mData   = 0;
}

void CFlexPayload::serialIn( int event, int bitVal )
{
    if( event == SAMP )
    {
        mData = (mData << 1) | bitVal;
        mCount++;

        if( mCount == 1 )
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }
        if ( mCount > 2 )
        {
            mCrcCode.append(bitVal);
        }

        if( mCount == 10 )
        {
            mData = mData & 0xff;

            mPayloadSize--;
            if(mPayloadSize>0)
            {
                toSelf( bitVal );
            }
            else
            {
                toNext( bitVal );
            }
        }
    }
}

void CFlexPayload::onExit(int /*bitVal*/)
{  
    CDecPayloadCmd* pData = new  CDecPayloadCmd();
    Q_ASSERT( NULL != pData );

    mSpan = mCounter.getDur();
    pData->setPosSpan( mPosition, mSpan );
    pData->setPayload( mData ,8 ,FLEX_DATA_COLOR, TEXT_DATA);
    AddBlock(CDecPayloadCmd,pData);

    if(mCellData->size() > 1)
    {
        mCellData->append(new CCellStr(1));//add space
    }
    mCellData->append(new CCellDW(mData,8));
}

//! CFlexTrailer::
CFlexTrailer::CFlexTrailer()
{}
CFlexTrailer::~CFlexTrailer()
{}
void CFlexTrailer::onEnter( int /*bitVal*/ )
{
    mCount  = 0;
    mData   = 0;
}

void CFlexTrailer::serialIn( int event, int bitVal )
{
    if( event == SAMP )
    {
        mData = (mData << 1) | bitVal;
        mCount++;
        if( mCount == 1 )
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }
        if( mCount == 30 )
        {
            uint crc1 = mData & 0xff;
            uint crc2 = ( mData >> 10 ) & 0xff;
            uint crc3 = ( mData >> 20 ) & 0xff;
            mData = (crc3 << 16) | (crc2<<8) | crc1;
            toNext( bitVal );
        }
    }
}

void CFlexTrailer::onExit( int /*bitVal*/ )
{
    CDecPayloadCmd* pData = new  CDecPayloadCmd();
    Q_ASSERT( NULL != pData );

    mSpan = mCounter.getDur();
    pData->setPosSpan( mPosition, mSpan );

    int color = FLEX_HCRC_COLOR;
    if( mData != getCRC(FrameCRC_A + mpFlexCfg->mChannel) )
    {
        color = ERR_COLOR;
    }

    pData->setPayload( mData ,24,color, TEXT_TCRC);
    AddBlock(CDecPayloadCmd,pData);

    mpTable->append(new CCellInfo(mData,24));
}
