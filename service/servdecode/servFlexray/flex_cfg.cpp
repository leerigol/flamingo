#include "flex_cfg.h"
#include "servdecode.h"

void CFlexCfg::reset()
{
    mRate       =   FlexRate_10M;
    mSrc        =   chan1;
    mSignal     =   FLEX_BP;
    mChannel    =   false;// 0-A, 1-B
    mSamplePos  =   50;
}

void CFlexCfg::serialIn(CStream &stream)
{
    int temp = 0;
    stream.read("mSrc",        temp);
    mSrc = (Chan)temp;
    stream.read("mRate",       temp);
    mRate = (RATE)temp;
    stream.read("mSignal",     temp);
    mSignal = (SIGTYPE)temp;
}

void CFlexCfg::serialOut(CStream &stream)
{
    stream.write("mSrc",        (int)mSrc);
    stream.write("mRate",       (int)mRate);
    stream.write("mSignal",     (int)mSignal);
}


void CFlexCfg::setSamplePerBaud(qint64 sample)
{
    if( mRate > 0)
    {
        mSamplePerBaud = sample / mRate;
    }
    else
    {
        mSamplePerBaud = 8;
    }

    if( mSamplePerBaud < 3)
    {
        mSamplePerBaud = 3;
    }
}

int CFlexCfg::getSamplePerBaud()
{
    return mSamplePerBaud;
}

DsoErr servDec::setFlexSrc(Chan src)
{
    mServFlex.mSrc = src;
    if(src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_FLEX_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_FLEX_THRE, true);
        setuiChange( MSG_DECODE_FLEX_THRE );
    }

    return ERR_NONE;
}

Chan servDec::getFlexSrc()
{
    return mServFlex.mSrc;
}

DsoErr servDec::setFlexThreAbs(qint64 t)
{
    setChThreshold(mServFlex.mSrc, t);
    return ERR_NONE;
}

qint64 servDec::getFlexThreAbs()
{
    Chan s = mServFlex.mSrc;
    return getChThreshold(s);
}

DsoErr servDec::setFlexRate(int baud)
{
    if(baud<=FlexRate_2_5M)
    {
        baud = FlexRate_2_5M;
    }
    else if(baud > FlexRate_5M)
    {
        baud = FlexRate_10M;
    }
    else
    {
        baud = FlexRate_5M;
    }
    mServFlex.mRate = (RATE)baud;
    return ERR_NONE;
}

int servDec::getFlexRate()
{
    setuiBase( 1, E_0 );
    setuiPostStr( "bps" );
    return mServFlex.mRate;
}

DsoErr servDec::setFlexSignal(int s)
{
    mServFlex.mSignal = (SIGTYPE)s;
    return ERR_NONE;
}

int servDec::getFlexSignal()
{
    setuiBase( 1, E_0 );
    setuiPostStr( "bps" );
    return mServFlex.mSignal;
}

DsoErr servDec::setFlexChannel(bool c)
{
    mServFlex.mChannel = c;
    return ERR_NONE;
}

bool servDec::getFlexChannel()
{
    return mServFlex.mChannel;
}

DsoErr servDec::setFlexSamplePos(int p)
{
    mServFlex.mSamplePos = p;
    return ERR_NONE;
}

int  servDec::getFlexSamplePos()
{
    setuiBase( 1, E_0 );
    setuiMaxVal(90);
    setuiMinVal(10);
    setuiZVal(50);
    setuiPostStr( "%" );
    return mServFlex.mSamplePos;
}
