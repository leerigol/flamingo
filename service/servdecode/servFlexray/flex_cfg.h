#ifndef CFLEXCFG_H
#define CFLEXCFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;

//#define FlexRate_2_5M    2500000
//#define FlexRate_5M      5000000
//#define FlexRate_10M     10000000

enum SIGTYPE
{
    FLEX_BP,
    FLEX_BM,
    FLEX_TX_RX
};
enum RATE
{
    FlexRate_2_5M = 2500000,
    FlexRate_5M = 5000000,
    FlexRate_10M = 10000000,
};

struct CFlexCfg
{
    void reset();
    void serialIn(CStream&);
    void serialOut(CStream&);
    int  getSamplePerBaud(void);
    void setSamplePerBaud(qint64 sample); //sample / baud

    Chan    mSrc;
    RATE    mRate;
    SIGTYPE mSignal;
    int     mSamplePos;
    bool    mChannel;
    int     mSamplePerBaud;

};
#endif // CFLEXCFG_H
