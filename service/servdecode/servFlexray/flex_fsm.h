#ifndef CDECFLEX_H
#define CDECFLEX_H

#include "flex_cfg.h"

#include "../decoder.h"
#include "../decTool/DecState.h"


class CFlexDecoder: public CDecoder,public CDecState
{
public:
    CFlexDecoder();
    ~CFlexDecoder();

    enum
    {
        HeaderCRC,
        FrameCRC_A,
        FrameCRC_B
    };
public:
     static CFlexCfg* mpFlexCfg;

public:
     bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
     void rst();
     int  setEventTable(CTable* table);

protected:
    static QList<int> mCrcCode;
    static uint       mPayloadSize;

    uint   getCRC(int);

private:
    bool decode(Chan src);
};

class CFlexIdle : public CFlexDecoder
{
public:
    CFlexIdle();
    virtual ~CFlexIdle();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit( int );
protected:
    unsigned int mPosition;
};

class CFlexHeader : public CFlexDecoder
{
public:
    CFlexHeader();
    virtual ~CFlexHeader();

    enum
    {
        HeaderIndicator,
        HeaderFrameID,
        HeaderPayloadLength,
        HeaderCRC,
        HeaderCycleCount
    };
public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit(int bitVal);

private:
    void onIndicator( int );
    void onFrameID( int );
    void onPayloadLength(int);
    void onHeaderCRC( int );
    void onCycleCount( int );

private:
    // 5bit
    bool  mReserved;
    bool  mPayloadPreamble;
    bool  mNullFrame;
    bool  mSyncFrame;
    bool  mStartupFrame;

    //11bit
    uint   mHeaderData;

    bool  bFSS;
    bool  bBSS;

    int   mHeaderCount;
    int   mHeaderStatus;
};

class CFlexPayload: public CFlexDecoder
{
public:
    CFlexPayload();
    virtual ~CFlexPayload();

protected:
    bool bBSS;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CFlexTrailer: public CFlexDecoder
{
public:
    CFlexTrailer();
    virtual ~CFlexTrailer();

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};



#endif // CDECLIN_H
