#include "flex_fsm.h"
#include "../servdecode.h"

static CFlexIdle    fsm_flex_idle;
static CFlexHeader  fsm_flex_header;
static CFlexPayload fsm_flex_payload;
static CFlexTrailer fsm_flex_trailer;

CFlexCfg* CFlexDecoder::mpFlexCfg;
QList<int> CFlexDecoder::mCrcCode;
uint       CFlexDecoder::mPayloadSize;

CFlexDecoder::CFlexDecoder()
{
    fsm_flex_idle.setNextStat( &fsm_flex_header );
    fsm_flex_header.setNextStat( &fsm_flex_payload );

    fsm_flex_payload.setNextStat( &fsm_flex_trailer );
    fsm_flex_trailer.setNextStat( &fsm_flex_idle );


    setIdle( &fsm_flex_idle );
    setErr( &fsm_flex_idle );
}
CFlexDecoder::~CFlexDecoder()
{
}

//! rst to idle
void CFlexDecoder::rst()
{
    mpNow = &fsm_flex_idle;

    mCounter.rst();
    mClock.stop();

    clearBlock();

    mpFlexCfg = &mpCurrServ->mServFlex;

    mpTable = new CTable();
    setEventTable(mpTable);
}

bool CFlexDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{
    bool ret = false;
    if( decode( mpCurrServ->getFlexSrc()) )
    {
        CDecLine* line = new CDecLine();

        //if( mpCurrServ->getLabelOn())
        {
            QString label = "Flexray";
            line->setLabel(label);
        }

        mLineView.makeLine(*line, mBlocklist);
        bus->append( line );
        if( zoomBus )
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        ret = true;
    }

    return ret;
}

bool CFlexDecoder::decode(Chan src)
{
    unsigned int u32Data;
    int last_dat, next_dat;

    uchar* pStream = mpCurrServ->getChanData( src );
    if( !pStream )
    {
        return false;
    }
    last_dat = ( *pStream & 0x80 ) ? 1 : 0;
    next_dat = last_dat;


    now()->onEnter(next_dat);

    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Data, pStream + i, 4 );

        //! no change
        if ( u32Data == m32IdleMask[last_dat] )
        {
            if ( mClock.tryTick(32) > 0)
            {                
            }
            else
            {
                i += 4;
                mCounter.count( 32 );
                mClock.tick(32);
                continue;
            }
        }

        uint dat = u32Data & 0xff;
        if(dat == m8IdleMask[last_dat])
        {
            if ( mClock.tryTick(8) > 0 )
            {
            }
            else
            {
                i += 1;
                mCounter.count(8);
                mClock.tick(8);                
                continue;
            }
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_dat = u32Data & m32Masks[j] ? 1 : 0;

            if( mpFlexCfg->mSignal == FLEX_BM )
            {
                next_dat = !next_dat;
            }
            //! rise
            if (last_dat <  next_dat)//rise
            {
                now()->serialIn( RISE, next_dat );
                last_dat = next_dat;
                mClock.sync();
            }
            else if (last_dat  >  next_dat)//fall
            {

                now()->serialIn( FALL, next_dat );
                last_dat = next_dat;
                mClock.sync();
            }

            //! timeout
            if ( mClock.tick() > 0 )
            {
                now()->serialIn( SAMP, next_dat );
            }
            mCounter.count();
        }
        i += 1;
    }
    return true;
}

int CFlexDecoder::setEventTable(CTable* table)
{
    //! head
    QList<quint32> lstHead;
    lstHead.append( MSG_DECODE_EVT_TIME );
    lstHead.append( MSG_DECODE_EVT_FLEX_FID );
    lstHead.append( MSG_DECODE_EVT_FLEX_LEN );
    lstHead.append( MSG_DECODE_EVT_FLEX_HCRC );
    lstHead.append( MSG_DECODE_EVT_FLEX_CYC );
    lstHead.append( MSG_DECODE_EVT_DATA );
    lstHead.append( MSG_DECODE_EVT_FLEX_FCRC );
    table->setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 50 );
    lstLayout.append( 80 );
    lstLayout.append( 50 );
    lstLayout.append( 30 );
    lstLayout.append( 50 );
    lstLayout.append( 30 );
    lstLayout.append( 200 );
    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_LIN);
    table->setTitle( pTitle );
    return ERR_NONE;
}

uint CFlexDecoder::getCRC(int type)
{
    uint CrcSize = 11;
    uint CrcReg  = 0x1A;
    uint CrcPolynomial = 0x385;

    int  i=0;
    int  size = mCrcCode.size();
    uint CrcNext;
    uint CrcMask = 0xffffffff;


    if( type == FrameCRC_A )
    {
        CrcSize = 24;
        CrcReg  = 0xFEDCBA;
        CrcPolynomial = 0x5D6DCB;
    }
    else if(type == FrameCRC_B )
    {
        CrcSize = 24;
        CrcReg  = 0xABCDEF;
        CrcPolynomial = 0x5D6DCB;
    }
    else
    {
        i=3;
        if( size >= 23 )
        {
            size = 23;
        }
    }

    uint CrcShift = CrcSize - 1;

    CrcMask = ~(CrcMask << CrcSize);

    while ( i < size )
    {
        uint NextBit = mCrcCode[i];

        CrcNext = ( (CrcReg >> CrcShift) ^ NextBit ) & 1;

        CrcReg = CrcReg << 1;
        CrcReg = CrcReg & CrcMask;

        if ( CrcNext == 1 )
        {
            CrcReg = CrcReg ^ CrcPolynomial;
        }
        i++;
    }

    return CrcReg;
}
