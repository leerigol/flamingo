#ifndef FLEX_MAP_H
#define FLEX_MAP_H

#include "../servdecode/servdecode.h"


on_set_int_int  (MSG_DECODE_FLEX_SRC,        &servDec::setFlexSrc),
on_get_int      (MSG_DECODE_FLEX_SRC,        &servDec::getFlexSrc),


on_set_int_ll   (MSG_DECODE_FLEX_THRE,       &servDec::setFlexThreAbs),
on_get_ll       (MSG_DECODE_FLEX_THRE,       &servDec::getFlexThreAbs),

on_set_int_int  (MSG_DECODE_FLEX_BAUD,       &servDec::setFlexRate),
on_get_int      (MSG_DECODE_FLEX_BAUD,       &servDec::getFlexRate),

on_set_int_int  (MSG_DECODE_FLEX_SIGNAL,     &servDec::setFlexSignal),
on_get_int      (MSG_DECODE_FLEX_SIGNAL,     &servDec::getFlexSignal),

on_set_int_bool (MSG_DECODE_FLEX_CHANNEL,    &servDec::setFlexChannel),
on_get_bool     (MSG_DECODE_FLEX_CHANNEL,    &servDec::getFlexChannel),

on_set_int_int  (MSG_DECODE_FLEX_SAMP,       &servDec::setFlexSamplePos),
on_get_int      (MSG_DECODE_FLEX_SAMP,       &servDec::getFlexSamplePos),

#endif // Flex_MAP_H

