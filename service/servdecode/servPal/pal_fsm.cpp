#include "pal_fsm.h"
#include "../servdecode.h"

#include <QDebug>


CPalIdle::CPalIdle()
{}
CPalIdle::~CPalIdle()
{}
void CPalIdle::serialIn(int event, int bitVal)
{
    int edge = mpPalCfg->mClkEdge;

    if(  event == SAMP ||
        (event == RISE && (edge == EDGE_RISING  || edge == EDGE_ALL) ) ||
        (event == FALL && (edge == EDGE_FALLING || edge == EDGE_ALL) ) )
    {
        toNext( bitVal );
    }
}

//! CPalData
CPalData::CPalData()
{}
CPalData::~CPalData()
{}

void CPalData::onEnter( int bitVal )
{
    mCounter.begin();
    mPosition = mCounter.now();
    mSamp = false;
    mData = bitVal;
}

void CPalData::serialIn( int event, int bitVal )
{
    switch( event )
    {
        case RISE:
            if(mpPalCfg->mClkEdge == EDGE_RISING ||
               mpPalCfg->mClkEdge == EDGE_ALL)
            {
                mSamp = true;
            }
            break;

        case FALL:
            if(mpPalCfg->mClkEdge == EDGE_FALLING ||
               mpPalCfg->mClkEdge == EDGE_ALL)
            {
                mSamp = true;
            }
            break;

        case SAMP:
            mSamp = true;
            break;

        default:
            mSamp = false;
            break;
    }
    if(mSamp)
    {
        toNext( bitVal );
    }
}

void CPalData::onExit( int /*bitVal*/ )
{
    //! export data
    mSpan = mCounter.getDur();


    CDecPayloadData* pData = new  CDecPayloadData();
    Q_ASSERT( NULL != pData );
    pData->setPosSpan( mPosition, mSpan );

    if( mpPalCfg->mBitOrder )
    {
        mData = this->bitOrder(mData, mpPalCfg->mDataWid );
    }

    mpTable->append(new CCellTime(mPosition));

    //la bus
    if((mpPalCfg->mNRJ && mSpan > mpPalCfg->mRejectWidth) ||  !mpPalCfg->mNRJ)
    {
        pData->setPayload( mData, mpPalCfg->mDataWid );
        mLatch = mData;//lock
        mpTable->append(new CCellDW(mData,mpPalCfg->mDataWid));
    }
    else
    {
        pData->setPayload( mLatch, mpPalCfg->mDataWid ,ERR_DATA_COLOR);
        mpTable->append(new CCellDW(mLatch,mpPalCfg->mDataWid));
    }

    AddBlock(CDecPayloadData,pData);

    AddOneFrame();
}
