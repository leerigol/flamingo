#ifndef CENGINEPAL_H
#define CENGINEPAL_H

#include "pal_fsm.h"
#include "../servView/decview.h"
#include "../servView/dectableview.h"
#include "../servBlock/DecLine.h"


class CEnginePal
{
public:
    CEnginePal();
    virtual ~CEnginePal();
public:
    static CDecLine doDecode_Clk( QMap<Chan,uchar*>& streamList,
                                  uchar *pClkStream,
                                  int len ,
                                  CPalCfg mPalDec,
                                  declineview lineview,
                                  BLOCK_FILTER *pFilter,
                                  CTable * pTable);

    static CDecLine doDecode_noClk( QMap<Chan,uchar*>& streamList,
                                    int len ,
                                    CPalCfg mPalDec,
                                    declineview lineview,
                                    BLOCK_FILTER *pFilter,
                                    CTable * pTable);
protected:
    static int ParallelIn(bool pn,
                          QMap<Chan,uchar>& u8DatList,
                          int index);

    static int  Edge_Change(QMap<Chan,int>&);
    static void ShadowInit(QMap<Chan,uchar>&);
private:
    static int mReg[20];
    static int mRegByte[20];
    static int mRegWord[20];
};


#endif // CENGINEPAL_H
