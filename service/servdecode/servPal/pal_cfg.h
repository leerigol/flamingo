#ifndef CPALCFG_H
#define CPALCFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;

typedef enum
{
    Pal_DataWid_0,
    Pal_DataWid_1,
    Pal_DataWid_2,
    Pal_DataWid_3,
    Pal_DataWid_4,
    Pal_DataWid_5,
    Pal_DataWid_6,
    Pal_DataWid_7,
    Pal_DataWid_8,
    Pal_DataWid_9,
    Pal_DataWid_10,
    Pal_DataWid_11,
    Pal_DataWid_12,
    Pal_DataWid_13,
    Pal_DataWid_14,
    Pal_DataWid_15,
    Pal_DataWid_16
}PalDataWid;
/*! \enum parallel userBus */
typedef enum
{
    CH1 = chan1,
    CH2 = chan2,
    CH3 = chan3,
    CH4 = chan4,
    D7_D0,
    D15_D8,
    D15_D0,
    D0_D7,
    D8_D15,
    D0_D15,
    USER
}BusUser;

struct CPalCfg
{
    void reset();
    void serialIn(CStream&);
    void serialOut(CStream&);

    Chan mClkSrc;
    int  mClkEdge;
    qint64  mClkDly;

    int  mBusWidth;
    int  mDataWid;
    int  mDataArray[32];

    bool mPolarity;
    bool mNRJ;
    bool mGraph;

    qint64 mNRJTime;
    uint   mRejectWidth;

    int  mCurbit;
    bool mBitOrder;
};
#endif // CPALCFG_H
