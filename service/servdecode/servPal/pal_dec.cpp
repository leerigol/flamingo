#include "pal_fsm.h"
#include "../servdecode.h"

static CPalIdle   fsm_pal_idle;
static CPalData   fsm_apl_data;

CPalCfg* CPalDecoder::mpPalCfg;

CPalDecoder::CPalDecoder()
{
     mpNow = &fsm_pal_idle;
     fsm_pal_idle.setNextStat( &fsm_apl_data );
     fsm_apl_data.setNextStat( &fsm_apl_data );
     setIdle( mpNow );
}

CPalDecoder::~CPalDecoder()
{
}


void CPalDecoder::rst()
{
   mpNow = &fsm_pal_idle;
   mCounter.rst();
   mClock.stop();
   clearBlock();

   mpPalCfg = &mpCurrServ->mServPal;
   mpTable = new CTable();

   setEventTable(mpTable);
}



//! rst to idle
bool CPalDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{
    QMap<Chan,uchar*> streamList;
    for(int i=0; i< mpPalCfg->mDataWid; i++)
    {
        Chan key  =  (Chan)mpPalCfg->mDataArray[i];
        uchar* data = mpCurrServ->getChanData( key );

        if( data == NULL )
        {
            return false;
        }
        streamList.insert(key, data);
    }

    if( mpPalCfg->mClkSrc != chan_none )
    {
        uchar* pClk = mpCurrServ->getChanData( mpPalCfg->mClkSrc );
        if( pClk == NULL )
        {
            return false;
        }
        decode_clk( pClk, streamList);
    }
    else
    {
        decode_dat( streamList );
    }



    CDecLine* line = new CDecLine();

    //if( mpCurrServ->getLabelOn())
    {
        QString label = "Parallel";
        line->setLabel(label);
    }

    mLineView.makeLine(*line,mBlocklist);
    bus->append( line );
    if( zoomBus )
    {
        CDecLine* lineZoom = new CDecLine();
        lineZoom->setLabel( line->getLabel() );

        mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
        zoomBus->append(lineZoom);
    }

    return true;
}

bool CPalDecoder::decode_clk(uchar *pClkStream, QMap<Chan,uchar*>& streamList)
{

    unsigned int u32Clk;

    QMap<Chan,uchar> u8DatList;
    int  next_dat;
    int  last_clk;
    int  next_clk;

    int  size  = mpCurrServ->getDataSize();

    last_clk = ( *pClkStream & 0x80 ) ? 1 : 0;
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Clk, pClkStream + i, 4 );

        //! no change
        if ( u32Clk == m32IdleMask[last_clk])
        {
            i += 4;
            mCounter.count( 32 );
            continue;
        }

        //! no change in u8
        if ( (u32Clk & 0xff) == m8IdleMask[last_clk] )
        {
            i += 1;
            mCounter.count(8);
            continue;
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++)
        {
            next_clk = u32Clk & m32Masks[j] ? 1 : 0;

            u8DatList.clear();
            //read 1byte data for each chan
            QMapIterator<Chan, uchar*> data(streamList);
            while (data.hasNext())
            {
                data.next();
                u8DatList.insert( data.key(), *(data.value() + i) );
            }

            //idle
            //clock rise samp
            if(last_clk < next_clk) //
            {
                next_dat = getPalDat(j,u8DatList);
                now()->serialIn( RISE, next_dat );
            }
            //clock fall samp
            else if(last_clk > next_clk)//
            {
                next_dat = getPalDat(j,u8DatList);
                now()->serialIn( FALL, next_dat );
            }

            last_clk = next_clk;
            mCounter.count();

            /***********************************************
             *!!!!!! Limit the count of the Event Table!!!!!!
            ************************************************/
            if( mpTable->getSheet()->size() >= LIMIT_RESULT_SIZE )
            {
                return true;
            }
        }
        //QThread::msleep(10);
        i += 1;
    }
    return true;
}

bool CPalDecoder::decode_dat(QMap<Chan, uchar *> &streamList)
{
    QMap<Chan,uchar> u8DatList;
    QMap<Chan,int>   u32DatList;
    int  next_dat = 0;
    int  last_dat = 0;
    int  temp = 0;
    int  step   = 0;


    QMapIterator<Chan, uchar*> data(streamList);
    while (data.hasNext())
    {
        data.next();
        u8DatList.insert( data.key(), *(data.value()) );
    }

    last_dat = getPalDat(0, u8DatList);
    now()->serialIn( SAMP, last_dat );

    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        u8DatList.clear();

        //read 1byte data for each chan
        QMapIterator<Chan, uchar*> data(streamList);
        while (data.hasNext())
        {
            data.next();
            memcpy( &temp , data.value() + i, 4);
            u8DatList.insert( data.key(), *(data.value() + i) );
            u32DatList.insert( data.key(), temp);
        }

        if( getStartBit(u8DatList) && last_dat ==  getPalDat(0, u8DatList))
        {
            step = getJumpStep(u32DatList); //jump step
            if( step )
            {
                i   += step;
                step = step * 8;
                mCounter.count( step );
                continue;
            }
        }


        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_dat = getPalDat(j,u8DatList);
            if(last_dat != next_dat)
            {
                now()->serialIn( SAMP, next_dat );
                last_dat = next_dat;
            }
            mCounter.count();

            /***********************************************
             *!!!!!! Limit the count of the Event Table!!!!!!
            ************************************************/
            if( mpTable->getSheet()->size() >= LIMIT_RESULT_SIZE )
            {
                return true;
            }
        }
        i += 1;
    }
    now()->onExit(0);
    return true;
}

/**
 * @brief get parallel data for one sample point
 * @param index,sample point
 * @param original data
 * @return the parallel data
 */
int CPalDecoder::getPalDat(int index,
                           QMap<Chan,uchar>& u8DatList
                           )// index 当前BYTE的位置	;DataArray D0~D15,CH1,2,3,4
{
    int i;
    int temp;
    int bitVal = 0;
    int mask = 1 << (7-index);
    bool pn  = mpPalCfg->mPolarity;

    for( i=0;i<mpPalCfg->mDataWid;i++ )		//
    {
       Chan chanId = (Chan)mpPalCfg->mDataArray[i];
       temp = u8DatList[chanId] &  mask;         // 按顺序组合成为并行数据

       if(temp)
       {
           bitVal |= 1 << i;
       }
    }
    if(pn == Polarity_Negtive)
    {
        bitVal = !bitVal;
    }
    return bitVal;
}

/**
 * @brief Parallel start bit for every channel
 * @param u8DatList
 * @return true if all are save
 */
bool CPalDecoder::getStartBit( QMap<Chan,uchar>& u8DatList )
{
    int mask = 0x80;
    int i;

    int bit1 = 0;
    int bit0 = 0;
    for( i=0;i<mpPalCfg->mDataWid;i++ )
    {
       Chan chanId = (Chan)mpPalCfg->mDataArray[i];
       if( u8DatList[chanId] & mask)
       {
           mRegWord[i] = 0xffffffff;
           mRegByte[i] = 0xff;
           bit1++ ;
       }
       else
       {
           mRegWord[i] = 0;
           mRegByte[i] = 0;
           bit0++;
       }
    }

    if( bit1 == mpPalCfg->mDataWid || bit0 == mpPalCfg->mDataWid)
    {
        return true;
    }
    return false;
}

//only return 0,1,4
int CPalDecoder::getJumpStep(QMap<Chan,int>& wordList)
{
  int i;
  int value = 0;
  int step  = 4;

  for(i=0;i<mpPalCfg->mDataWid;i++)
  {
      Chan chanId = (Chan)mpPalCfg->mDataArray[i];

      value = wordList[chanId];

      if( value != mRegWord[i] )
      {
          if( value != mRegByte[i] )
          {
              return 0;
          }
          else
          {
              step = 1;
          }
      }
  }
  return step;
}

int CPalDecoder::setEventTable(CTable* table)
{
    //! head
    QList<quint32> lstHead;
    lstHead.append( MSG_DECODE_EVT_TIME );
    lstHead.append( MSG_DECODE_EVT_DATA );
    table->setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 80 );
    lstLayout.append( 200 );
    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_PAL);
    table->setTitle( pTitle );
    return ERR_NONE;
}
