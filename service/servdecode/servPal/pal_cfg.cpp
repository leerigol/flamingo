
#include "servdecode.h"
#include "pal_cfg.h"
#include "pal_fsm.h"

void CPalCfg::reset()
{

    mDataWid  = Pal_DataWid_1;
    mClkSrc   = chan_none;

    mClkEdge  = EDGE_RISING;

    mClkDly   = 0;
    mNRJTime  = 0;
    mRejectWidth=0;

    mCurbit   = 0;

    mGraph    = false;
    mBusWidth  = CH1;
    mNRJ      = false;
    mPolarity = true;
    mBitOrder = false;

    for(int i=0; i<array_count(mDataArray); i++)
    {
        mDataArray[i] = chan1;
    }
}

void CPalCfg::serialIn(CStream & stream)
{
    int temp = 0;
    stream.read("mDataWid",     mDataWid);
    stream.read("mClkSrc",      temp);
    mClkSrc = (Chan)temp;

    stream.read("mClkEdge",     mClkEdge);
    stream.read("mClkDly",      mClkDly);

    stream.read("mCurbit",      mCurbit);
    stream.read("mGraph",       mGraph);
    stream.read("mBusWidth",    mBusWidth);
    stream.read("mPolarity",    mPolarity);
    stream.read("mBitOrder",    mBitOrder);
    for(int i=0; i<array_count(mDataArray); i++)
    {
        QString dataArray = QString("PalDataArray_%1").arg(i);
        stream.read(dataArray,  mDataArray[i]);
    }

    if( !servDec::m_bWithMSO )
    {
        if( mBusWidth >= D7_D0 && mBusWidth <= D0_D15 )
        {
            mBusWidth = USER;
        }

        for(int i=0; i<array_count(mDataArray); i++)
        {
            if (mDataArray[i] >= d0 && mDataArray[i]<=d15)
            {
                mDataArray[i] = CH1;
            }
        }
    }
}

void CPalCfg::serialOut(CStream &stream)
{
    stream.write("mDataWid",     mDataWid);
    stream.write("mClkSrc",      (int)mClkSrc);
    stream.write("mClkEdge",     mClkEdge);
    stream.write("mClkDly",      mClkDly);
    stream.write("mCurbit",      mCurbit);
    stream.write("mGraph",       mGraph);
    stream.write("mBusWidth",    mBusWidth);
    stream.write("mPolarity",    mPolarity);
    stream.write("mBitOrder",    mBitOrder);
    for(int i=0; i<array_count(mDataArray); i++)
    {
        QString dataArray = QString("PalDataArray_%1").arg(i);
        stream.write(dataArray,  mDataArray[i]);
    }
}

//! Pal
DsoErr servDec::setPalClkSrc(Chan clksrc)
{
    mServPal.mClkSrc = clksrc;
    if(clksrc > chan4)
    {
    }
    else
    {
        getChThreshold1(clksrc);
        setuiChange( MSG_DECODE_PAL_CLK_THRE );
    }
    return ERR_NONE;
}

Chan servDec::getPalClkSrc()
{
    return mServPal.mClkSrc;
}

DsoErr servDec::setPalClkEdge(int clkedge)
{
    mServPal.mClkEdge = clkedge;
    return ERR_NONE;
}

int servDec::getPalClkEdge()
{
    return mServPal.mClkEdge;
}

DsoErr servDec::setPalNrj(bool nrj)
{
    mServPal.mNRJ = nrj;
    return ERR_NONE;
}


bool servDec::getPalNrj()
{
    return mServPal.mNRJ;
}

DsoErr servDec::setPalNrjTime(qint64 nrjtime)
{
    mServPal.mNRJTime = nrjtime;
    mServPal.mRejectWidth = nrjtime * servDec::m_nLaSampleRate * 1e-9;
    return ERR_NONE;
}

//not more than clock period
qint64 servDec::getPalNrjTime()
{
    setuiZVal( 0 );
    setuiMinVal(0);
    setuiMaxVal(time_s(1));
    setuiStep( getTimeStep( mServPal.mNRJTime) );

    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return mServPal.mNRJTime;
}


DsoErr servDec::setPalClkDly(qint64 clkdly)
{
    mServPal.mClkDly = clkdly;
    return ERR_NONE;
}

qint64 servDec::getPalClkDly()
{
    setuiBase(1,E_N12);
    setuiPostStr("s");
    setuiUnit(Unit_s);

    setuiZVal(0);
    setuiStep( getTimeStep( mServPal.mClkDly) );
    setuiMinVal(time_ms(-100));
    setuiMaxVal( time_ms(100) );
    return mServPal.mClkDly;
}

DsoErr servDec::setPalPolarity(bool p)
{
    mServPal.mPolarity = p;
    return ERR_NONE;
}

bool servDec::getPalPolarity()
{
    return mServPal.mPolarity;
}


DsoErr servDec::setPalGraph(bool graph)
{
    mServPal.mGraph = graph;
    return ERR_NONE;
}


bool servDec::getPalGraph()
{
    setuiVisible(false);
    return mServPal.mGraph;
}

DsoErr servDec::setPalBus(int nBusWidth)
{
    mServPal.mBusWidth = nBusWidth;
    mServPal.mCurbit = 0;

    switch( nBusWidth )
    {
        case CH1:
        case CH2:
        case CH3:
        case CH4:
            mServPal.mDataWid=1;
            mServPal.mDataArray[0] = nBusWidth;
            getChThreshold1( (Chan)nBusWidth );
            setuiChange(MSG_DECODE_PAL_DAT_THRE);
            break;

        case D7_D0:
            mServPal.mDataWid=8;
            for(int i=0;i<mServPal.mDataWid;i++)
            {
                mServPal.mDataArray[i] = d7-i;//
            }
            break;
        case D15_D8:
            mServPal.mDataWid=8;
            for(int i=0;i<mServPal.mDataWid;i++)
            {
               mServPal.mDataArray[i] = d15-i;
            }
            break;
        case D15_D0:
            mServPal.mDataWid=16;
            for(int i=0;i<mServPal.mDataWid;i++)
            {
                mServPal.mDataArray[i] = d15-i;
            }
            break;
        case D0_D7:
            mServPal.mDataWid=8;
            for(int i=0;i<mServPal.mDataWid;i++)
            {
                mServPal.mDataArray[i] = i+d0;
            }
            break;
        case D8_D15:
            mServPal.mDataWid=8;
            for(int i=0;i<mServPal.mDataWid;i++)
            {
                mServPal.mDataArray[i] = i+d8;
            }
            break;
        case D0_D15:
            mServPal.mDataWid=16;
            for(int i=0;i<mServPal.mDataWid;i++)
            {
                mServPal.mDataArray[i] = i+d0;
            }
            break;

        default:
            break;
    }

    if( mServPal.mCurbit >= mServPal.mDataWid )
    {
        mServPal.mCurbit = mServPal.mDataWid - 1;
    }


    bool b = nBusWidth > CH4;
    int  j = chan4;
    if( m_bWithMSO )
    {
        j = d15;
    }
    for(int i=chan1; i<=j; i++)
    {
        mUiAttr.setEnable(MSG_DECODE_BUS_CH, i, b);
    }


    bool en = false;
    if( nBusWidth == USER )
    {
        en = true;
    }

    mUiAttr.setEnable(MSG_DECODE_BUS_WIDTH, en);

    en = mServPal.mDataWid>1;
    mUiAttr.setEnable(MSG_DECODE_BUS_BITX, en);

//    updateAllUi();
    setuiChange(MSG_DECODE_BUS_CH);
    setuiChange(MSG_DECODE_BUS_WIDTH);
    setuiChange(MSG_DECODE_BUS_BITX);

    return ERR_NONE;
}

int servDec::getPalBus()
{
    for(int i=D7_D0; i<=D0_D15; i++)
    {
        setuiVisible(i, m_bWithMSO);
    }
    return mServPal.mBusWidth;
}


DsoErr servDec::setPalBusWidth(int w)
{
    mServPal.mDataWid = w;

    if( w > 1 )
    {
        mUiAttr.setMaxVal(MSG_DECODE_BUS_BITX, mServPal.mDataWid - 1 );
        mUiAttr.setEnable(MSG_DECODE_BUS_BITX, true);
    }
    else
    {
        mServPal.mCurbit = 0;
        mUiAttr.setEnable(MSG_DECODE_BUS_BITX, false);

    }
    //async(MSG_DECODE_BUS_BITX, mServPal.mCurbit);
    setuiChange(MSG_DECODE_BUS_BITX);
    return ERR_NONE;
}

int servDec::getPalBusWidth()
{
    setuiZVal(1);
    setuiStep(1);
    setuiMinVal(1);

//    if mso
//            20
//    else
//            4

    if( m_bWithMSO )
    {
        setuiMaxVal( 20 );
    }
    else
    {
        setuiMaxVal( 4 );
    }
    setuiUnit(Unit_none);
    setuiBase(1, E_0);

    return mServPal.mDataWid;
}


DsoErr servDec::setPalCurbit(int curbit)
{
    if( curbit < mServPal.mDataWid )
    {
        async(MSG_DECODE_BUS_CH, mServPal.mDataArray[curbit] );
        mServPal.mCurbit = curbit;
        return ERR_NONE;
    }
    else
    {
        return ERR_OVER_UPPER_RANGE;
    }
}

int servDec::getPalCurbit()
{
    setuiZVal(0);
    setuiStep(1);
    setuiMinVal(0);
    setuiMaxVal( mServPal.mDataWid-1 );
    setuiUnit(Unit_none);
    setuiBase(1, E_0);

    return mServPal.mCurbit;
}

DsoErr servDec::setPalCurCh(Chan curch)
{
    mServPal.mDataArray[mServPal.mCurbit] = curch;
    if( curch <= chan4 )
    {
        getChThreshold1(curch);
        setuiChange(MSG_DECODE_PAL_DAT_THRE);
    }
    else
    {
        //setuiEnable( true );
    }
    return ERR_NONE;
}

int servDec::getPalCurCh()
{
    return mServPal.mDataArray[mServPal.mCurbit];
}


DsoErr servDec::setPalClkAbsThre(qint64 t)
{
    setChThreshold(mServPal.mClkSrc, t);
    return ERR_NONE;
}

qint64 servDec::getPalClkAbsThre()
{

    Chan s = mServPal.mClkSrc;

    return getChThreshold( s );
}

DsoErr servDec::setPalCurAbsThre(qint64 t)
{
    Chan ch = (Chan)(mServPal.mDataArray[mServPal.mCurbit]);
    setChThreshold( ch, t);
    return ERR_NONE;
}

qint64 servDec::getPalCurAbsThre()
{
    Chan s = (Chan)(mServPal.mDataArray[mServPal.mCurbit]);

    return getChThreshold( s );
}


DsoErr servDec::setPalEndian(bool b)
{
    mServPal.mBitOrder = b;
    return ERR_NONE;
}

bool servDec::getPalEndian()
{
    return mServPal.mBitOrder;
}

