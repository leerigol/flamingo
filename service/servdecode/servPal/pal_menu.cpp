#include "servdecode.h"


//! Pal
//!
//!
DsoErr servDec::setPalClkSrc(Chan clksrc)
{
    mServPal.mClkSrc = clksrc;
    return ERR_NONE;
}

Chan servDec::getPalClkSrc()
{
    return mServPal.mClkSrc;
}

DsoErr servDec::setPalClkEdge(int clkedge)
{
    mServPal.mClkEdge = clkedge;
    return ERR_NONE;
}

int servDec::getPalClkEdge()
{
    returnmServPal.mClkEdge;
}

DsoErr servDec::setPalNrj(bool nrj)
{
    mServPal.mNRJ = njr;
    return ERR_NONE;
}


bool servDec::getPalNrj()
{
    return mServPal.mNRJ;
}

DsoErr servDec::setPalNrjTime(int nrjtime)
{
    mServPal.mNRJTime = nrjtime;
    return ERR_NONE;
}

int servDec::getPalNrjTime()
{
    setuiBase(1,E_N9);
    setuiPostStr("s");
    return mServPal.mNRJTime;
}


DsoErr servDec::setPalClkDly(int clkdly)
{
    mServPal.mClkDly = clkdly;
    return ERR_NONE;
}

int servDec::getPalClkDly()
{
    setuiBase(1,E_N9);
    setuiPostStr("s");

    return mServPal.mClkDly
}

DsoErr servDec::setPalPolarity(bool p)
{
    mServPal.mPolarity = p;
}

bool servDec::getPalPolarity()
{
    return mServPal.mPolarity;
}


DsoErr servDec::setPalGraph(bool graph)
{
    mServPal.mGraph = graph;
    return ERR_NONE;
}


bool servDec::getPalGraph()
{
    return mServPal.mGraph;
}

DsoErr servDec::setPalBus(int t)
{
    switch(t)
    {
    case D7_D0:
        mDataWid=8;
        for(int i=0;i<mDataWid;i++)
        {
            mDataArray[i] = i+5;//
        }
        break;
    case D15_D8:
        mDataWid=8;
        for(int i=0;i<mDataWid;i++)
        {
            mDataArray[i] = i+13;
        }
        break;
    case D15_D0:
        mDataWid=16;
        for(int i=0;i<mDataWid;i++)
        {
            mDataArray[i] = i+5;
        }
        break;
    case D0_D7:
        mDataWid=8;
        for(int i=0;i<mDataWid;i++)
        {
            mDataArray[i] = 12-i;
        }
        break;
    case D8_D15:
        mDataWid=8;
        for(int i=0;i<mDataWid;i++)
        {
            mDataArray[i] = 20-i;
        }
        break;
    case D0_D15:
        mDataWid=16;
        for(int i=0;i<mDataWid;i++)
        {
            mDataArray[i] = 20-i;
        }
        break;

    default:
        break;
    }
    return mServPal.mBusType = t;
}

int servDec::getPalBus()
{
    return mServPal.mBusType;
}


DsoErr servDec::setPalBusWidth(int w)
{
    return mServPal.mDataWid = w;
}

int servDec::getPalBusWidth()
{
    return mServPal.mDataWid
}


DsoErr servDec::setPalCurbit(int curbit)
{
    async(MSG_DECODE_BUS_BITX, mServPal.mDataArray[curbit] );
    return mServPal.mCurbit = curbit;
}

int servDec::getPalCurbit()
{
    setuiZVal(0);
    setuiStep(1);
    setuiMinVal(0);
    setuiMaxVal(mServPal.mDataWid - 1);
    return mServPal.mCurbit;
}

DsoErr servDec::setPalCurCh(Chan curch)
{
    return mServPal.mCurChan = curch;
}

Chan servDec::getPalCurCh()
{
    return mServPal.mCurChan
}


DsoErr servDec::setPalClkAbsThre(int clkabsthre)
{
    //mThreshold;
}

DsoErr servDec::getPalClkAbsThre(int clkabsthre)
{
    //return mServPal.setClkAbsThre(clkabsthre);
}

DsoErr servDec::setPalCurAbsThre(int curabsthre)
{
    //return mServPal.setCurAbsThre(curabsthre);
}

int servDec::getPalCurAbsThre()
{
    //return mServPal.getCurAbsThre();
}
