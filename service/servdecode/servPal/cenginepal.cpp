#include <QDebug>
#include "cenginepal.h"
#include "../decodetype.h"
#include "../servComm/DecState.h"


int CEnginePal::mRegWord[];
int CEnginePal::mRegByte[];
int CEnginePal::mReg[];

CEnginePal::CEnginePal()
{
}

CEnginePal::~CEnginePal()
{
}

CDecLine CEnginePal::doDecode_Clk(  QMap<Chan,uchar*>& streamList,
                                    uchar *pClkStream,
                                    int len,
                                    CPalCfg mPalDec,
                                    declineview lineview,
                                    BLOCK_FILTER *pFilter,
                                    CTable * pTable)
{
//    unsigned int u8Datas[] ={0,0xff};
//    unsigned int u32Datas[]={0,0xffffffff};
//    unsigned int u32Masks[]={0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
//    unsigned int u32Clk;

//    QMap<Chan,uchar> u8DatList;
//    int  next_dat;
//    int  last_clk;
//    int  next_clk;

//    int  i, j;


//    last_clk = ( *pClkStream & 0x80 ) ? 1 : 0;

//    CDecPAL::mPalCfg = (CPalCfg)mPalDec;
//    CDecPAL::mTable = pTable;
//    CDecPAL::rst();

//    bool bPolarity = CDecPAL::mPalCfg.mPolarity;

//    //! overflow 3 bytes
//    for ( i = 0; i < len; )
//    {
//        //! read 32 bits
//        memcpy( &u32Clk, pClkStream + i, 4 );

//        //! no change
//        if ( u32Clk == u32Datas[last_clk])
//        {
//            i += 4;
//            CDecPAL::mpCounter->count( 32 );
//            continue;
//        }

//        //! no change in u8
//        if ( (u32Clk & 0xff) == u8Datas[last_clk] )
//        {
//            i += 1;
//            CDecPAL::mpCounter->count(8);
//            continue;
//        }



//        //! change in 8bits
//        for ( j = 0; j < 8; j++)
//        {
//            next_clk = u32Clk & u32Masks[j] ? 1 : 0;

//            u8DatList.clear();
//            //read 1byte data for each chan
//            QMapIterator<Chan, uchar*> data(streamList);
//            while (data.hasNext())
//            {
//                data.next();
//                u8DatList.insert( data.key(), *(data.value() + i) );
//            }

//            //idle
//            //clock rise samp
//            if(last_clk < next_clk) //
//            {
//                next_dat = ParallelIn(bPolarity,u8DatList,j);
//                if(bPolarity == Polarity_Positive)//added
//                {
//                    CDecStat::mNow->serialIn( RISE, next_dat );
//                }
//                else
//                {
//                    CDecStat::mNow->serialIn( FALL, next_dat );
//                }
//            }
//            //clock fall samp
//            else if(last_clk > next_clk)//
//            {
//                next_dat = ParallelIn(bPolarity,u8DatList,j);
//                if(bPolarity == Polarity_Positive)//added
//                {
//                    CDecStat::mNow->serialIn( FALL, next_dat );
//                }
//                else
//                {
//                    CDecStat::mNow->serialIn( RISE, next_dat );
//                }
//            }

//            last_clk = next_clk;
//            CDecPAL::mpCounter->count();
//        }
//        i += 1;
//    }

//    CDecLine line = lineview.creatLine(CDecPAL::mBlocklist,
//                                       CDecPAL::mpCounter->now(),
//                                       pFilter);//filter block to creat line
    return line;
}

CDecLine CEnginePal::doDecode_noClk( QMap<Chan,uchar*>& streamList,
                                     int len ,
                                     CPalCfg mPalDec,
                                     declineview lineview,
                                     BLOCK_FILTER *pFilter,
                                     CTable * pTable)
{
    QMap<Chan,uchar> u8DatList;
    QMap<Chan,int>   u32DatList;
    int  next_dat = 0;
    int  last_dat = 0;
    int  temp = 0;
    int  i, j;
    int step      = 0;
    CDecPAL::mPalCfg = (CPalCfg)mPalDec;
    CDecPAL::mTable = pTable;
    CDecPAL::rst();

    bool bPolarity = CDecPAL::mPalCfg.mPolarity;

    QMapIterator<Chan, uchar*> data(streamList);
    while (data.hasNext())
    {
        data.next();
        u8DatList.insert( data.key(), *(data.value()) );
    }
    CEnginePal::ShadowInit( u8DatList );//mask init
    last_dat = ParallelIn(bPolarity, u8DatList, 0);
    CDecStat::mNow->serialIn( SAMP, last_dat );

    //! overflow 3 bytes
    for ( i = 0; i < len; )
    {
        u8DatList.clear();
        //read 1byte data for each chan
        QMapIterator<Chan, uchar*> data(streamList);
        while (data.hasNext())
        {
            data.next();
            memcpy( &temp , data.value() + i, 4);
            u8DatList.insert( data.key(), *(data.value() + i) );
            u32DatList.insert( data.key(), temp);
        }

        ShadowInit(u8DatList);

        step = Edge_Change(u32DatList); //jump step
        if( step )
        {
            i   += step;
            step = step * 8;
            CDecPAL::mpCounter->count( step );
            continue;
        }


        //! change in 8bits
        for ( j = 0; j < 8; j++ )
        {
            next_dat = ParallelIn(bPolarity, u8DatList, j);
            if(last_dat != next_dat)
            {
                CDecStat::mNow->serialIn( SAMP, next_dat );
                last_dat = next_dat;
            }
            CDecPAL::mpCounter->count();
        }
        i += 1;
    }
    CDecStat::mNow->onExit(0);

    CDecLine line = lineview.creatLine(CDecPAL::mBlocklist,
                                       CDecPAL::mpCounter->now(),
                                       pFilter);//filter block to creat line
    return line;
}

int CEnginePal::ParallelIn(bool pn,
                           QMap<Chan,uchar>& u8DatList,
                           int index )// index 当前BYTE的位置	;DataArray D0~D15,CH1,2,3,4
{
    int i;
    int temp;
    int bitVal = 0;
    int mask   = 1 << (7-index);

    for( i=0;i<CDecPAL::mPalCfg.mDataWid;i++ )		//
    {
       Chan chanId = (Chan)CDecPAL::mPalCfg.mDataArray[i];
       temp = u8DatList[chanId] &  mask;         // 按顺序组合成为并行数据

       if(pn == Polarity_Negtive)
       {
           if(temp)
           {
               temp = 0;
           }
           else
           {
               temp = mask;
           }
       }
       if(temp)
       {
           bitVal |= 1 << i;
       }
    }
    return bitVal;
}

void CEnginePal::ShadowInit( QMap<Chan,uchar>& u8DatList )
{
    int mask = 0x80;
    int i;
    for( i=0;i<CDecPAL::mPalCfg.mDataWid;i++ )		//
    {
       Chan chanId = (Chan)CDecPAL::mPalCfg.mDataArray[i];
       if( u8DatList[chanId] & mask)
       {
           mRegWord[i] = 0xffffffff;
           mRegByte[i] = 0xff;
       }
       else
       {
           mRegWord[i] = 0;
           mRegByte[i] = 0;
       }
    }
}

//only return 0,1,4
int CEnginePal::Edge_Change(QMap<Chan,int>& wordList)
{
  int i;
  int value = 0;
  int step  = 4;

  for(i=0;i<CDecPAL::mPalCfg.mDataWid;i++)
  {
      Chan chanId = (Chan)CDecPAL::mPalCfg.mDataArray[i];

      value = wordList[chanId];

      if( value != mRegWord[i] )
      {
          if( value != mRegByte[i] )
          {
              return 0;
          }
          else
          {
              step = 1;
          }
      }
  }
  return step;
}
