#include "../servdecode/servdecode.h"

/*Pal*/
on_set_int_int  (MSG_DECODE_PAL_CLK,        &servDec::setPalClkSrc),
on_get_int      (MSG_DECODE_PAL_CLK,        &servDec::getPalClkSrc),

on_set_int_int  (MSG_DECODE_PAL_CLK_EDGE,   &servDec::setPalClkEdge),
on_get_int      (MSG_DECODE_PAL_CLK_EDGE,   &servDec::getPalClkEdge),

on_set_int_ll   (MSG_DECODE_NRJ_TIME,       &servDec::setPalNrjTime),
on_get_ll       (MSG_DECODE_NRJ_TIME,       &servDec::getPalNrjTime),

on_set_int_ll   (MSG_DECODE_PAL_CLK_DLY,    &servDec::setPalClkDly),
on_get_ll       (MSG_DECODE_PAL_CLK_DLY,    &servDec::getPalClkDly),

on_set_int_bool (MSG_DECODE_PAL_NRJ,        &servDec::setPalNrj),
on_get_bool     (MSG_DECODE_PAL_NRJ,        &servDec::getPalNrj),

on_set_int_bool (MSG_DECODE_POLARITY,       &servDec::setPalPolarity),
on_get_bool     (MSG_DECODE_POLARITY,       &servDec::getPalPolarity),

on_set_int_bool (MSG_DECODE_BUS_GRAPH,      &servDec::setPalGraph),
on_get_bool     (MSG_DECODE_BUS_GRAPH,      &servDec::getPalGraph),

on_set_int_bool (MSG_DECODE_PAL_ENDIAN,     &servDec::setPalEndian),
on_get_bool     (MSG_DECODE_PAL_ENDIAN,     &servDec::getPalEndian),

on_set_int_ll   (MSG_DECODE_PAL_CLK_THRE,   &servDec::setPalClkAbsThre),
on_get_ll       (MSG_DECODE_PAL_CLK_THRE,   &servDec::getPalClkAbsThre),

on_set_int_int  (MSG_DECODE_PAL_BUS,        &servDec::setPalBus),
on_get_int      (MSG_DECODE_PAL_BUS,        &servDec::getPalBus),


on_set_int_int  (MSG_DECODE_BUS_WIDTH,      &servDec::setPalBusWidth),
on_get_int      (MSG_DECODE_BUS_WIDTH,      &servDec::getPalBusWidth),

on_set_int_int  (MSG_DECODE_BUS_BITX,       &servDec::setPalCurbit),
on_get_int      (MSG_DECODE_BUS_BITX,       &servDec::getPalCurbit),

on_set_int_int  (MSG_DECODE_BUS_CH,         &servDec::setPalCurCh),
on_get_int      (MSG_DECODE_BUS_CH,         &servDec::getPalCurCh),

on_set_int_ll   (MSG_DECODE_PAL_DAT_THRE,   &servDec::setPalCurAbsThre),
on_get_ll       (MSG_DECODE_PAL_DAT_THRE,   &servDec::getPalCurAbsThre),

//on_set_int_int(MSG_DECODE_S32PALTHRECHREL,&servDec::setPalCurRelThre),
//on_get_int(MSG_DECODE_S32PALTHRECHREL,&servDec::getPalCurRelThre),
