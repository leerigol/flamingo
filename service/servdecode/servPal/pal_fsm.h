#ifndef CDECPAL_H
#define CDECPAL_H

#include "../decTool/DecState.h"

#include "../decoder.h"
#include "pal_cfg.h"


class CPalDecoder : public CDecoder,public CDecState
{
public:
     CPalDecoder();
    ~CPalDecoder();

public:
    void rst();
    bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
    int  setEventTable(CTable* table);
public:
    static CPalCfg* mpPalCfg;

private:
    //sample by clk edge
    bool decode_clk(uchar *pClkStream, QMap<Chan,uchar*>& streamList);

    //sample by data edge
    bool decode_dat(QMap<Chan,uchar*>& streamList);


    int  getPalDat(int index, QMap<Chan,uchar>& u8DatList);

    int  getJumpStep(QMap<Chan,int>&);
    bool getStartBit(QMap<Chan,uchar>&);
private:
    int mReg[20];
    int mRegByte[20];
    int mRegWord[20];
};

class CPalIdle : public CPalDecoder
{
public:
    CPalIdle();
    ~CPalIdle();
    void serialIn( int event, int bitVal );

};

class CPalData : public CPalDecoder
{
public:
    CPalData();
    ~CPalData();

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );

private:
    bool mSamp;
};

#endif // CDECPAL_H
