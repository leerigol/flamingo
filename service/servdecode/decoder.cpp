#include "decoder.h"

#include "./servUart/uart_fsm.h"
#include "./servPal/pal_fsm.h"
#include "./servI2C/i2c_fsm.h"
#include "./servSPI/spi_fsm.h"
#include "./servCAN/can_fsm.h"
#include "./servLin/lin_fsm.h"
#include "./servFlexray/flex_fsm.h"
#include "./servI2S/i2s_fsm.h"
#include "./serv1553B/1553b_fsm.h"

#include "./servdecode.h"
#include "./servBlock/DecLine.h"

QMap<int,CDecoder*> CDecoder::mDecoders;
servDec*            CDecoder::mpCurrServ;
CLineView           CDecoder::mLineView;
CLineView           CDecoder::mLineViewZoom;

uint CDecoder::mData;
uint CDecoder::mCount;
uint CDecoder::mPosition;
uint CDecoder::mSpan;
uint CDecoder::mLatch;
uint CDecoder::mFrameCount;

QList<CDecBlock*> CDecoder::mBlocklist;
QList<CDecBlock*> CDecoder::mZoomBlocklist;

CCounter  CDecoder::mCounter;
CClock    CDecoder::mClock;
CTable*   CDecoder::mpTable;
CCellDW*  CDecoder::mCellData;

uint CDecoder::m8IdleMask[] ={0,0xff};
uint CDecoder::m32IdleMask[]={0,0xffffffff};
uint CDecoder::m32Masks[]   ={0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

CDecoder::CDecoder()
{}

CDecoder::~CDecoder()
{}

CDecoder* CDecoder::buildDecoder(servDec *pServDec)
{
    int type = pServDec->getDecodeType();

    CDecoder* pDecoder = NULL;
    if( mDecoders.contains( type ))
    {
        pDecoder = mDecoders[type];
    }
    else
    {
        switch(type)
        {
            case Bus_Parallel:
                pDecoder = new CPalDecoder();
                break;

            case Bus_RS232:
                pDecoder = new CUartDecoder();
                break;

            case Bus_I2C:
                pDecoder = new CI2CDecoder();
                break;

            case Bus_SPI:
                pDecoder = new CSpiDecoder();
                break;

            case Bus_CAN:
                pDecoder = new CCanDecoder();
                break;

            case Bus_LIN:
                pDecoder = new CLinDecoder();
                break;

            case Bus_FlexRay:
                pDecoder = new CFlexDecoder();
                break;

            case Bus_I2S:
                pDecoder = new CI2SDecoder();
                break;

            case Bus_STD1553B:
                pDecoder = new C1553BDecoder();
                break;

            default:
                qDebug() << "invalid decoder";
                Q_ASSERT(false);
                break;
        }
        mDecoders[type] = pDecoder;
    }

    mpCurrServ = pServDec;

    return pDecoder;
}



void CDecoder::clearBlock()
{
//    CDecBlock *pBlock;

//    foreach( pBlock, mBlocklist )
//    {
//        Q_ASSERT( pBlock != NULL );
//        delete pBlock;
//    }
    mBlocklist.clear();
    mZoomBlocklist.clear();
    mFrameCount  = 0;
}

//void CDecoder::appendBlock(CDecBlock *block)
//{
//    mBlocklist.append(block);
//}

CTable* CDecoder::mergeEvt(CTable *pTa, CTable *pTb, CTable *pEvt)
{
    Q_ASSERT(pTa);
    Q_ASSERT(pTb);
    Q_ASSERT(pEvt);

    CCell *cell  = NULL;

    CCellList *cellListA = pTa->getSheet();
    CCellList *cellListB = pTb->getSheet();

    int nCountA = cellListA->size();
    int nCountB = cellListB->size();
    if(nCountA > 0 && nCountB > 0)
    {
        CCell *cellA = cellListA->head();
        CCell *cellB = cellListB->head();

        while (cellA && cellB)
        {
            //time equal
            if(cellA->data()->getData() == cellB->data()->getData())
            {
                cell = cellA->next(); //must before the following
                pEvt->append( cellA ); // add time
                cellA = cell;
                if( cellA)
                {
                    cell = cellA->next();
                    pEvt->append( cellA );//add tx
                    cellA = cell;
                    if( cellA )
                    {
                        cell = cellA->next();
                        pEvt->append( cellA );//add rx
                        cellA = cell;
                    }
                    else
                    {
                        pEvt->append(new CCellStr(0));
                    }
                }
                else
                {
                    pEvt->append(new CCellStr(0));
                    pEvt->append(new CCellStr(0));
                }

                cellB = cellB->next();//jump time
                if( cellB )
                {
                    cell = cellB->next();
                    pEvt->append( cellB );
                    cellB = cell;
                    if( cellB )
                    {
                        cell = cellB->next();
                        pEvt->append( cellB );
                        cellB = cell;
                    }
                    else
                    {
                        pEvt->append(new CCellStr(0));
                    }
                }
                else
                {
                    pEvt->append(new CCellStr(0));
                    pEvt->append(new CCellStr(0));
                }
            }
            else if(cellA->data()->getData() > cellB->data()->getData())
            {
                cell = cellB->next();
                pEvt->append( cellB ); // time null,null
                pEvt->append(new CCellStr(0));
                pEvt->append(new CCellStr(0));

                cellB = cell;
                if(cellB)
                {
                    cell = cellB->next();
                    pEvt->append( cellB );
                    cellB = cell;
                    if(cellB)
                    {
                        cell = cellB->next();
                        pEvt->append( cellB );
                        cellB = cell;
                    }
                    else
                    {
                        pEvt->append(new CCellStr(0));
                    }
                }
                else
                {
                    pEvt->append(new CCellStr(0));
                    pEvt->append(new CCellStr(0));
                }
            }
            else if(cellA->data()->getData() < cellB->data()->getData())
            {
                cell = cellA->next();
                pEvt->append( cellA );
                cellA = cell;
                if(cellA)
                {
                    cell = cellA->next();
                    pEvt->append( cellA );
                    cellA = cell;
                    if( cellA )
                    {
                        cell = cellA->next();
                        pEvt->append( cellA );
                        cellA = cell;
                    }
                    else
                    {
                        pEvt->append(new CCellStr(0));
                    }
                }
                else
                {
                    pEvt->append(new CCellStr(0));
                    pEvt->append(new CCellStr(0));
                }
                pEvt->append(new CCellStr(0));
                pEvt->append(new CCellStr(0));
            }
        }

        //if any table left ,then add them to the queue
        while(cellA)
        {
            cell = cellA->next();
            pEvt->append( cellA );
            cellA = cell;
        }

        while(cellB)
        {
            cell = cellB->next();
            pEvt->append( cellB );
            cellB = cell;
        }
    }
    else if( nCountA > 0)
    {
         CCell *cellA = cellListA->head();
         while(cellA)
         {
             cell = cellA->next();
             pEvt->append( cellA );
             cellA = cell;
         }
    }
    else if( nCountB > 0)
    {
        CCell *cellB = cellListB->head();
        while(cellB)
        {
            cell = cellB->next();
            pEvt->append( cellB );
            cellB = cell;
        }
    }

    return pEvt;
}

int CDecoder::bitOrder(int data, int width)
{
    Q_ASSERT(width <=32);
    if(width > 1)
    {
        int dat = 0;
        for(int i=0; i<width; i++)
        {
            if( data & (1<<i) )
            {
                dat |= ( 1<<(width-i-1));
            }
        }
        return dat;
    }
    return data;
}
