// Clock.h: interface for the CClock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLOCK_H__4D209DBF_5447_4FC8_A2D4_EA062C833EA4__INCLUDED_)
#define AFX_CLOCK_H__4D209DBF_5447_4FC8_A2D4_EA062C833EA4__INCLUDED_


class CClock
{
public:
    CClock();
    virtual ~CClock();

protected:
    int mDelay;
    int mTick;
    int mPeri;
    int mSync;
    int mEn;

public:
    void start( int peri, int dly = 0 );
    void stop();
    void sync( int syn );
    void sync();
    int tick( int ticks = 1 );
    int tryTick( int ticks = 1 );

};

class CTimer//:public CClock
{
public:
    CTimer();
    virtual ~CTimer();
protected:
    int mDelay;
    int mTick;
    int mPeri;
    int mSync;
    int mEn;
public:
    void start( int peri, int dly = 0 );
    void stop();
    void sync( int syn );
    void sync();
    int  tick( int ticks = 1 );
    int  tryTick( int ticks = 1 );
};

#endif // !defined(AFX_CLOCK_H__4D209DBF_5447_4FC8_A2D4_EA062C833EA4__INCLUDED_)
