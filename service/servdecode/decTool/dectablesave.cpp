#include "dectablesave.h"


CTable *CDecSave::pPayloadTable;
bool   CDecSave::m_bScpiSave = true;
CDecSave::CDecSave()
{}

//! save event table
int CDecSave::saveEventtable(QString &fname)
{
    int f = 0;
    int ret = ERR_NONE;
    if(m_bScpiSave)
    {
        serviceExecutor::query( serv_name_storage, MSG_STORAGE_FILETYPE, f );
    }
    else
    {
        f = servFile::FILETYPE_CSV;
        m_bScpiSave = true;
    }

    qDebug() << "format=" << f;
    qDebug() << "fname=" << fname;
    if(f == servFile::FILETYPE_DAT)
    {
        qDebug() << "dat=" << f;
        ret = saveDAT(fname);
    }
    else if(f == servFile::FILETYPE_CSV)
    {
        qDebug() << "csv=" << f;
        ret = saveCSV(fname);
    }
    return ret;
}

//! save payload table
int CDecSave::savePayloadtable(QString &fname)
{
    int f = 0;
    int ret = ERR_NONE;
    serviceExecutor::query( serv_name_storage, MSG_STORAGE_FILETYPE, f );
    qDebug() << "format=" << f;
    qDebug() << "fname=" << fname;
    if(f == servFile::FILETYPE_DAT)
    {
        qDebug() << "dat=" << f;
        ret = savePayloadDAT(fname);
    }
    else if(f == servFile::FILETYPE_CSV)
    {
        qDebug() << "csv=" << f;
        ret = savePayloadCSV(fname);
    }
    return ret;
}

//! save csv event table
int CDecSave::saveCSV(QString &fname)
{
//    servDec servdec;
    QByteArray arrayFile;
    QString cellstr ;
    QString decName;
    CCellList *pHeadList,*pDataList;
    CCell *pDataCell,*pHeadCell ;
    CTable *pTable = NULL;//servDec::m_pEvtTable;//servDec::getEvtTable();//.getEvtTable();//(CTable*)ptr;
    int ret = ERR_NONE;
    QFile file(fname);
    void *ptr;
    int  evtFormat = 0;
    if(file.open(QIODevice::WriteOnly))
    {
        //! get table
        int servId = 0 ;
        serviceExecutor::query(serv_name_decode1, servDec::cmd_dec_evt_id, servId );//qxl 2018.02.05
        QString sname = service::getServiceName(servId);
        serviceExecutor::query(sname, servDec::cmd_bus_evt, ptr );
        pTable = static_cast<CTable *>(ptr);

        serviceExecutor::query(sname, MSG_DECODE_EVT_FORMAT, evtFormat );

        if ( NULL != pTable )
        {
            int i=0;
            int colum = pTable->column();
//            qDebug()<<"colum"<<colum;
            int cellCnts = pTable->getSheet()->size();
//            qDebug()<<"cellCnts"<<cellCnts;

            arrayFile.reserve(cellCnts*2 + 1);

            decName = QString("Decode").append(sname.right(1))+","+sysGetString(pTable->getTitle()->getData());
//            qDebug()<<"decName"<<sname+","+decName;
            arrayFile.append(decName);


            pHeadList = pTable->getHead();
            pDataList = pTable->getSheet();
            //qDebug()<<"pHeadList"<<pHeadList->size();
            //qDebug()<<"pDataList"<<pDataList->size();
            pDataCell = pDataList->head();//->data();
            pHeadCell = pHeadList->head();
            i = 0;
            while( pHeadCell != NULL)//i < cellCnts)//payload != NULL )
            {
                cellstr = "";
                if(i % colum == 0)
                {
                    arrayFile.append(NEW_LINE);
                }
                pHeadCell->format( pTable->getContext(), cellstr );
                //qDebug()<<"headstr"<<cellstr;
                arrayFile.append(cellstr);
                arrayFile.append(SEPARATOR);
                pHeadCell = pHeadCell->next();
                i++;
            }
            i = 0;
            while( pDataCell != NULL)//i < cellCnts)//payload != NULL )
            {
                cellstr = "";
                if(i % colum == 0)
                {
                    arrayFile.append(NEW_LINE);
                }

                pDataCell->format( pTable->getContext(), cellstr );

                if(pDataCell->data()->getIsData())
                {
                    bool b;
                    QString temp;
                    QStringList dataList = cellstr.simplified().split(" ");

                    for(int i=0; i<dataList.count(); i++)
                    {
                        QString strTran = dataList.at(i);
                        int dataVal = strTran.toInt(&b,16);
                        if(F_ASC == evtFormat)
                        {
                            int index = dataVal & 0x7f;
                            temp.append( CDecodeFormat::_ascTable[index]+" " );
                            cellstr = temp;
                        }
                        else if(F_DEC == evtFormat)
                        {
                            temp.append(QString::number(dataVal)+" ");
                            cellstr = temp;
                        }
                    }
                }
//                qDebug()<<"datastr"<<cellstr;

                arrayFile.append(cellstr);
                arrayFile.append(SEPARATOR);
                pDataCell = pDataCell->next();
                i++;
            }
            if(-1 != file.write(arrayFile))
            {
                file.close();
                return ERR_NONE;
            }
            else
            {
                file.close();
                ret = ERR_FILE_SAVE_FAIL;
            }
        }
    }
    else
    {
        ret = ERR_FILE_SAVE_FAIL;
    }
    file.close();
    return ret;
}

//! save csv payload table
int CDecSave::savePayloadCSV(QString &fname)
{
    getPayloadTable();//get pPayloadTable
    QByteArray arrayFile;
    QString cellstr ;
    QString decName;
    CCellList /**pHeadList,*/*pDataList;
    CCell *pDataCell/*,*pHeadCell*/ ;
    int ret = ERR_NONE;
    QFile file(fname);
    if(file.open(QIODevice::WriteOnly))
    {
        if ( NULL != pPayloadTable )
        {
            int i=0;
            int colum = pPayloadTable->column();
            qDebug()<<"colum"<<colum;
            int cellCnts = pPayloadTable->getSheet()->size();
            qDebug()<<"cellCnts"<<cellCnts;

            arrayFile.reserve(cellCnts*16 + 1);

            decName = sysGetString(pPayloadTable->getTitle()->getData());
            qDebug()<<"decName"<<decName;
            arrayFile.append(decName);

            pDataList = pPayloadTable->getSheet();
            qDebug()<<"pDataList"<<pDataList->size();
            pDataCell = pDataList->head();//->data();
            i = 0;
            while(i < PAYLOAD_COLUMS)//( pHeadCell != NULL)//i < cellCnts)//payload != NULL )
            {
                cellstr = "";
                if(i % PAYLOAD_COLUMS == 0)
                {
                    arrayFile.append(NEW_LINE);
                }
                cellstr = QString::number(i,16);
                qDebug()<<"headstr"<<cellstr;
                arrayFile.append(cellstr);
                arrayFile.append(SEPARATOR);
                i++;
            }
            i = 0;
            QStringList strList;
            while( pDataCell != NULL)//i < cellCnts)//payload != NULL )
            {
                cellstr = "";
                if(i % PAYLOAD_COLUMS == 0)
                {
                    arrayFile.append(NEW_LINE);
                }
                pDataCell->format( pPayloadTable->getContext(), cellstr );
                qDebug()<<"datastr"<<cellstr;
                strList = cellstr.split(SPACE);
                foreach (QString tempStr, strList)
                {
                    arrayFile.append(tempStr);
                    arrayFile.append(SEPARATOR);
                    i++;
                }

                pDataCell = pDataCell->next();
            }
            if(-1 != file.write(arrayFile))
            {
                file.close();
                return ERR_NONE;
            }
            else
            {
                file.close();
                ret = ERR_FILE_SAVE_FAIL;
            }
        }
    }
    else
    {
        ret = ERR_FILE_SAVE_FAIL;
    }
    file.close();
    return ret;

}


//! save bin event table
int CDecSave::saveDAT(QString &/*fname*/)
{
    return ERR_NONE;
}

//! save bin payload table
int CDecSave::savePayloadDAT(QString &fname)
{
    getPayloadTable();//get pPayloadTable
    QByteArray arrayFile;
    QString cellstr ;
    QString decName;
    CCellList *pDataList;
    CCell *pDataCell ;
    int ret = ERR_NONE;
    QFile file(fname);
    if(file.open(QIODevice::WriteOnly))
    {
        if ( NULL != pPayloadTable )
        {
            pDataList = pPayloadTable->getSheet();
            qDebug()<<"pDataList"<<pDataList->size();
            pDataCell = pDataList->head();//->data();

            arrayFile.reserve(pDataList->size()*16 + 1);
            QStringList strList;
            while( pDataCell != NULL)//i < cellCnts)//payload != NULL )
            {
                cellstr = "";
                pDataCell->format( pPayloadTable->getContext(), cellstr );
                qDebug()<<"datastr"<<cellstr;
                strList = cellstr.split(SPACE);
                foreach (QString tempStr, strList)
                {
                    arrayFile.append(tempStr);
//                    arrayFile.append(SEPARATOR);
//                    i++;
                }

                pDataCell = pDataCell->next();
            }
            if(-1 != file.write(arrayFile))
            {
                file.close();
                return ERR_NONE;
            }
            else
            {
                file.close();
                ret = ERR_FILE_SAVE_FAIL;
            }
        }
    }
    else
    {
        ret = ERR_FILE_SAVE_FAIL;
    }
    file.close();
    return ret;
}


int CDecSave::getPayloadTable()
{
#if 0
    CTable *pTable = NULL;//servDec::m_pEvtTable;

    pPayloadTable = new CTable();
    if(pTable->column() > 0)
    {
        int j=0;
        CCellList *cellList = pTable->getSheet();
        qDebug()<<"cellList->size()"<<cellList->size();
        CCell *mhead = cellList->head();
        qDebug()<<"mhead->data()->getData()"<<mhead->data()->getData();
        while (true)
        {
            int ret = mhead->next(j)->data()->getIsData();//getData((quint32*)temp);
            if( ret)
            {
                pPayloadTable->append(mhead->next(j)->data());
            }
            j++;
            if(j >= cellList->size())
            {
                break;
            }

            //! title
            CCellStr *pTitle = new CCellStr(pTable->getTitle()->getData());
            pPayloadTable->setTitle( pTitle );
        }
    }
#endif
    return 0;
}

void CDecSave::setScpiSave(bool b)
{
    m_bScpiSave = b;
}
