// Counter.cpp: implementation of the CCounter class.
//
//////////////////////////////////////////////////////////////////////

#include "Counter.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCounter::CCounter()
{
}

CCounter::~CCounter()
{
}

void CCounter::rst( int cnt )
{
    mTag = 0;
    mCnt = cnt;
}
void CCounter::count( int cnt )
{
    mCnt += cnt;
}
unsigned int CCounter::now()
{
    return mCnt;
}

void CCounter::begin()
{
    mTag = mCnt;
}
int CCounter::getDur()
{
    return mCnt - mTag;
}
