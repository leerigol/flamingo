// DecStat.h: interface for the CDecStat class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DECSTAT_H__BDCF4826_DB84_4E9D_A3EB_AAB85C7BD341__INCLUDED_)
#define AFX_DECSTAT_H__BDCF4826_DB84_4E9D_A3EB_AAB85C7BD341__INCLUDED_

#define cNEXT_COUNT 3
#define DEC_DATACOUNT 1024

enum
{
    FALL,   //! fall
    RISE,   //! edge rise
    SAMP,   //! sampling
    START,
    STOP,
    SPITMO,
    SampleOver,
    LeftChannel,
    RightChannel
};

#if 0
class CDecStat
{
public:
    CDecStat();
    virtual ~CDecStat();

public:
    static CDecStat *mNow;
    static CTable * mTable;
    static CCellDW *mCellData;
    static QList<CDecBlock*> mBlocklist;

protected:
    CDecStat * mpNexts[ cNEXT_COUNT ];

public:
    void setNextStat( CDecStat *pNexts[], int count=1);
    void setNextStat( CDecStat *pNxt );
    void switchTo( CDecStat *pNext, int bitVal );

public:
    virtual void onEnter( int bitVal );
    virtual void onExit( int bitVal );
    virtual void serialIn( int cmd, int bitval );

public:
    uint mData;
    uint mCount;
    uint mPosition;
    uint mSpan;

};
#endif

//Decoder fsm
class CDecState
{
public:
    CDecState();
    virtual ~CDecState();

public:
    virtual void setNextStat( CDecState *pNxt );
    virtual void setIdle(CDecState* idle);
    virtual void setErr(CDecState* err);


    virtual void switchTo( CDecState *pNext, int bitVal);
    virtual void toNext(int bitVal);
    virtual void toPrev(int bitVal);
    virtual void toSelf(int bitVal);
    virtual void toNextNext(int bitVal);

    virtual void toErr(int bitVal);
    virtual void toIdle(int bitVal);

    virtual CDecState* now();

public:
    virtual void onEnter(int bitVal);
    virtual void onExit(int bitVal);
    virtual void serialIn(int cmd, int bitval);


protected:
    static CDecState* mpNow;
private:
    CDecState* mpNext;
    CDecState* mpPrev;
    static CDecState* mpError;
    static CDecState* mpIdle;

};

#endif // !defined(AFX_DECSTAT_H__BDCF4826_DB84_4E9D_A3EB_AAB85C7BD341__INCLUDED_)
