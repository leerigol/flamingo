#include "decview.h"
#include <QDebug>



CBlockFilter::CBlockFilter()
{
    f32Div =1;

    u32MemPos = 0;
    u32MemSta = 0;
    u32MemStp = 0;
    u32MemWid =1;
    u32ScrPos = 0;
    u32ScrSta = 0;
    u32ScrStp = 0;
    u32ScrWid =1;
}


CLineView::CLineView()
{
}


bool CLineView::makeLine(CDecLine& line,QList<CDecBlock*>& blocklist)
{
    CBlockFilter* pFilter = &mFilter;
    foreach(CDecBlock *block , blocklist)
    {
        block->mStp = block->mPos + block->mSpan-1;

        //qDebug() << block->mPos << block->mStp  << pFilter->u32MemSta << pFilter->u32MemStp;

        //result before the memory start
        if( block->mPos < pFilter->u32MemSta )
        {
            if( block->mStp > pFilter->u32MemSta &&
                block->mStp <= pFilter->u32MemStp )
            {
                ProcFilterMsg( pFilter, FIL_MSG_DAT_L,
                           pFilter->u32MemSta,
                           block->mStp,
                           block
                           );
            }
            else if( block->mStp > pFilter->u32MemStp )
            {
                ProcFilterMsg( pFilter, FIL_MSG_DAT_S,
                           pFilter->u32MemSta,
                           pFilter->u32MemStp,
                           block
                           );
            }
        }//result between the memory region
        else if( block->mPos >= pFilter->u32MemSta
                && block->mStp <= pFilter->u32MemStp )
        {
            ProcFilterMsg( pFilter, FIL_MSG_DAT_I,
                           block->mPos,
                           block->mStp,
                           block);
        }
        else if( block->mPos < pFilter->u32MemStp
                && block->mStp > pFilter->u32MemStp ) //part outside of the right memory region
        {
            ProcFilterMsg( pFilter, FIL_MSG_DAT_R,
                           block->mPos,
                           pFilter->u32MemStp,
                           block);
        }
        else
        {
            /*ProcFilterMsg( pFilter, FIL_MSG_POS,
                           pBlk->u32Stp,
                           pBlk->u32Stp,
                           pBlk );*/
        }
        //qDebug() << block->mPos << block->mSpan  << pFilter->u32ScrSta << pFilter->u32ScrStp;
        line.append(block);
    }
    return true;
}

//初始化BLOCK FILTER
void CLineView::setBlockFilter(unsigned int u32ScrSta, //FILTER 屏幕起点
                               unsigned int u32ScrStp, //FILTER 屏幕终点
                               unsigned int u32MemSta, //FILTER 内存起点
                               unsigned int u32MemStp) //FILTER 内存终点
{
    CBlockFilter* pFilter = &mFilter;
    pFilter->u32ScrSta = u32ScrSta;
    pFilter->u32ScrStp = u32ScrStp;
    pFilter->u32ScrPos = u32ScrSta;
    pFilter->u32ScrWid = u32ScrStp - u32ScrSta + 1;

    pFilter->u32MemSta = u32MemSta;
    pFilter->u32MemStp = u32MemStp;
    pFilter->u32MemPos = u32MemSta;
    pFilter->u32MemWid = u32MemStp - u32MemSta + 1;

    pFilter->f32Div    = (float)(pFilter->u32MemWid)/(pFilter->u32ScrWid);

    //qDebug() << pFilter->u32MemWid << pFilter->u32ScrWid;
}


/***************************************************************************
METHODS
***************************************************************************/

//FILTER 设置屏幕起点，终点
void CLineView::setBlockFilterScreen(unsigned int u32ScrSta,
                                     unsigned int u32ScrStp )
{
    CBlockFilter* pFilter = &mFilter;
    pFilter->u32ScrSta = u32ScrSta;
    pFilter->u32ScrStp = u32ScrStp;
    pFilter->u32ScrPos = u32ScrSta;
    pFilter->u32ScrWid = u32ScrStp - u32ScrSta + 1;

    pFilter->f32Div    = (float)(pFilter->u32MemWid)/(pFilter->u32ScrWid);
}

// BLOCK FILTER 设置屏幕显示对应的内存显示起点，终点
void CLineView::setBlockFilterMemory(unsigned int u32MemSta,
                                     unsigned int u32MemStp )
{
    CBlockFilter* pFilter = &mFilter;
    pFilter->u32MemSta = u32MemSta;
    pFilter->u32MemStp = u32MemStp;
    pFilter->u32MemPos = u32MemSta;
    pFilter->u32MemWid = u32MemStp - u32MemSta + 1;

    pFilter->f32Div    = (float)(pFilter->u32MemWid)/(pFilter->u32ScrWid);    
}


//添加数据BLOCK
void CLineView::AddDataBlock( CBlockFilter *pFilter, //FILTER
                   unsigned int u32Sta, //起点
                   unsigned int u32Stp, //终点
                   CDecBlock *block,
                   blockShape u32BlkShp) //BLOCK形状
{
    //添加数据BLK
    //根据不同的BLOCK类型设置位置
    switch( u32BlkShp )
    {
        case FRAME_LEFT:
        u32Sta = pFilter->u32ScrSta;
        u32Stp = FilterMemToScrPos(pFilter,u32Stp);
        break;

        case FRAME_RIGHT:
        u32Sta = FilterMemToScrPos(pFilter,u32Sta);
        u32Stp = pFilter->u32ScrStp;
        break;

        case FRAME_INTER:
        u32Sta = FilterMemToScrPos(pFilter,u32Sta);
        u32Stp = FilterMemToScrPos(pFilter,u32Stp);
        break;

        case FRAME_SPAN:
        u32Sta = pFilter->u32ScrSta;
        u32Stp = pFilter->u32ScrStp;
        break;

        default:
        u32Sta = FilterMemToScrPos(pFilter,u32Sta);
        u32Stp = FilterMemToScrPos(pFilter,u32Stp);
        break;
    }

    block->mPos = u32Sta;//(int)(block->mPos*para);
    block->mSpan = u32Stp-u32Sta +1;//(int)(block->mSpan*para);
    block->setShape(u32BlkShp);
}

                                                        //屏幕位置转换
/***************************************************************************
*函数名  : FilterMemToScrPos
*描  述  : FILTER中的屏幕位置转换
  ScrSta ~~ ScrEnd
  MemSta ~~ MemEnd
  u32DistDiv / 2 用于调整当拧小档位时，波形不够屏幕像素，波形在屏幕上的偏移，左右各一半
***************************************************************************/
unsigned int CLineView::FilterMemToScrPos( CBlockFilter *pFilter,
                             unsigned int u32MemPos )
{
    unsigned int u32Dist;
    unsigned int u32DistDiv;

    if ( u32MemPos < pFilter->u32MemSta )
    {
        return pFilter->u32ScrSta;
    }
    else if ( u32MemPos > pFilter->u32MemStp )
    {
        return pFilter->u32ScrStp;
    }

    u32Dist = ( u32MemPos- pFilter->u32MemSta ) / pFilter->f32Div;
    u32DistDiv = 1 / pFilter->f32Div;

    return( u32Dist + (u32DistDiv / 2) + pFilter->u32ScrSta );
}


/***************************************************************************
*函数名  : ProcFilterMsg
*描  述  : FILTER消息处理
***************************************************************************/
void CLineView::ProcFilterMsg( CBlockFilter *pFilter, //FILTER
                    FILTER_MSG msg, //MSG
                    unsigned int u32Sta, //MEM STA
                    unsigned int u32Stp, //MEM STP
                    CDecBlock *block ) //BLOCK
{
    switch( msg )
    {
        case FIL_MSG_STA: //FILTER起点
            pFilter->u32MemPos = pFilter->u32MemSta;
            break;

        case FIL_MSG_POS: //FILTER位置
            pFilter->u32MemPos = u32Sta;
            break;

        case FIL_MSG_DAT_L: //左边界的数据
            AddDataBlock( pFilter, pFilter->u32MemSta, u32Stp, block, FRAME_LEFT);
            pFilter->u32MemPos = u32Stp+1;

            break;

        case FIL_MSG_DAT_I: //中间的数据
            if( pFilter->u32MemPos<pFilter->u32MemSta )
            {
                AddDataBlock( pFilter, u32Sta, u32Stp, block, FRAME_INTER );
                pFilter->u32MemPos = u32Stp+1;
            }
            else
            {
                AddDataBlock( pFilter,u32Sta, u32Stp, block, FRAME_INTER );
                pFilter->u32MemPos = u32Stp+1;
            }

            break;

        case FIL_MSG_DAT_R: //右边界的数据
            AddDataBlock( pFilter, u32Sta, u32Stp, block, FRAME_RIGHT );
            pFilter->u32MemPos = u32Stp+1;

            break;

        case FIL_MSG_DAT_S:
            AddDataBlock( pFilter, u32Sta, u32Stp, block, FRAME_SPAN );
            pFilter->u32MemPos = u32Stp+1;
            break;

        case FIL_MSG_END: //结束处理
            break;

        default:
            break;
    }
}
