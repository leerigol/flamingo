#ifndef DECVIEW_H
#define DECVIEW_H

#include "../servBlock/DecLine.h"
#include "../servBlock/DecPayload.h"
#include "../servBlock/DecIcon.h"
#include "../servBlock/DecBlock.h"

#define HORIPIXEL 1000

struct CBlockFilter
{
    unsigned int u32ScrSta;     //屏幕起点
    unsigned int u32ScrStp;     //屏幕终点
    unsigned int u32ScrWid;     //屏幕宽度

    unsigned int u32MemSta;     //内存起点
    unsigned int u32MemStp;     //内存终点
    unsigned int u32MemWid;     //内存宽度

    float        f32Div;        //倍率 内存宽度/屏幕宽度

    unsigned int u32ScrPos;     //屏幕当前位置
    unsigned int u32MemPos;     //内存当前位置

//        BUS_BLOCK_ARY *pAry;
    void *pOutFrameLine; //输出LINE
    CBlockFilter();
};

class CLineView
{
public:
    CLineView();


    typedef enum
    {
        FIL_MSG_STA,
        FIL_MSG_STP,
        FIL_MSG_POS,

        FIL_MSG_DAT_L,
        FIL_MSG_DAT_I,
        FIL_MSG_DAT_R,
        FIL_MSG_DAT_S,
        FIL_MSG_END,

    }FILTER_MSG;


    bool makeLine(CDecLine& line,QList<CDecBlock*>& blocklist);

    //初始化BLOCK FILTER
    void setBlockFilter(  unsigned int u32ScrSta, //FILTER 屏幕起点
                          unsigned int u32ScrStp, //FILTER 屏幕终点
                          unsigned int u32MemSta, //FILTER 内存起点
                          unsigned int u32MemStp //FILTER 内存终点
                          ); //BLOCK ARY  BUS_BLOCK_ARY *pAry

    // FILTER 设置屏幕起点，终点
    void setBlockFilterScreen(unsigned int u32ScrSta,
                              unsigned int u32ScrStp );

    //BLOCK FILTER 设置屏幕显示对应的内存显示起点，终点
    void setBlockFilterMemory(unsigned int u32MemSta,
                              unsigned int u32MemStp );


private:
    //添加数据BLOCK
    void AddDataBlock( CBlockFilter *pFilter, //FILTER
                       unsigned int u32Sta, //起点
                       unsigned int u32Stp, //终点
                       CDecBlock *pBlk,
                       blockShape u32BlkShp); //BLOCK形状

    //FILTER中的屏幕位置转换//  ScrSta ~~ ScrEnd//  MemSta ~~ MemEnd
    unsigned int FilterMemToScrPos( CBlockFilter *pFilter,
                                 unsigned int u32MemPos );

    //FILTER消息处理
    void ProcFilterMsg( CBlockFilter *pFilter, //FILTER
                        FILTER_MSG msg, //MSG
                        unsigned int u32Sta, //MEM STA
                        unsigned int u32Stp, //MEM STP
                        CDecBlock *pBlk ); //BLOCK

private:
    CBlockFilter mFilter;
};

#endif // DECVIEW_H
