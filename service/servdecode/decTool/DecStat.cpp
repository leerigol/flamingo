// DecStat.cpp: implementation of the CDecStat class.
//
//////////////////////////////////////////////////////////////////////

#include "DecState.h"


///////////////////////////////////////////////////////////////////////
/// \brief CDecState::setNextStat
/// \param pNxt
///
/// //////////////////////////////////////////////////////////////////

CDecState*  CDecState::mpNow;
CDecState*  CDecState::mpError;
CDecState*  CDecState::mpIdle;

CDecState::CDecState()
{
    mpNext = 0;
}

CDecState::~CDecState()
{
}

void CDecState::setNextStat( CDecState *pNxt )
{
    mpNext = pNxt;
    mpNext->mpPrev = this;
}

void CDecState::switchTo( CDecState *pNext, int bitVal )
{
    //! exit
    onExit( bitVal );
    //! enter
    pNext->onEnter( bitVal );

    mpNow = pNext;

    //mpNow->mpPrev = this;
}

void CDecState::toPrev(int bitVal)
{
    switchTo( mpPrev, bitVal );
}

void CDecState::toNext(int bitVal)
{
    switchTo( mpNext, bitVal);
}

void CDecState::toNextNext(int bitVal)
{
    switchTo(mpNext->mpNext, bitVal);
}

void CDecState::toSelf(int bitVal)
{
    switchTo(this, bitVal);
}

void CDecState::toErr(int bitVal)
{
    switchTo( mpError, bitVal );
}

void CDecState::toIdle(int bitVal)
{
    switchTo( mpIdle, bitVal );
}

void CDecState::setIdle(CDecState *idle)
{
    mpIdle = idle;
}

void CDecState::setErr(CDecState *err)
{
    mpError = err;
}

CDecState* CDecState::now()
{
    return mpNow;
}



void CDecState::onEnter( int /*bitVal*/)
{}
void CDecState::onExit( int /*bitVal*/ )
{}

void CDecState::serialIn( int /*cmd*/, int /*bitval*/ )
{}
