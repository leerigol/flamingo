// Counter.h: interface for the CCounter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COUNTER_H__EB8B651D_08DD_4FA3_8A28_1A8AC2881054__INCLUDED_)
#define AFX_COUNTER_H__EB8B651D_08DD_4FA3_8A28_1A8AC2881054__INCLUDED_


class CCounter  
{
public:
	CCounter();
	virtual ~CCounter();

protected:
    unsigned int mCnt;
    unsigned int mTag;
public:
    void rst( int cnt = 0 );
    void count( int cnt = 1 );
    unsigned int now();

    void begin();
    int  getDur();

};

#endif // !defined(AFX_COUNTER_H__EB8B651D_08DD_4FA3_8A28_1A8AC2881054__INCLUDED_)
