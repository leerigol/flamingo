#ifndef DECTABLESAVE_H
#define DECTABLESAVE_H

#include<QObject>
#include<QFile>

#include "../servdecode.h"
#include "../servstorage/servstorage.h"
#include  "../eventTable/ctable.h"


#define NEW_LINE  ",\n"
#define NEW_LINE_ "\n"
#define SEPARATOR ","
#define SPACE      " "
#define PAYLOAD_COLUMS 16

class CDecSave
{
public:
    CDecSave();

public:
    static int      saveEventtable(QString &fname);
    static int      savePayloadtable(QString &fname);
    static int      saveDAT(QString&);
    static int      saveCSV(QString&);
    static int      savePayloadDAT(QString&);
    static int      savePayloadCSV(QString&);
    static int      getPayloadTable();

    static void     setScpiSave(bool b);

private:

private:
    static CTable *pPayloadTable;
    static  bool   m_bScpiSave;

};

#endif // DECTABLESAVE_H
