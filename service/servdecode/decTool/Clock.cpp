// Clock.cpp: implementation of the CClock class.
//
//////////////////////////////////////////////////////////////////////

#include "Clock.h"
#include <QDebug>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CClock::CClock()
{
    mEn = 0;
}

CClock::~CClock()
{

}

CTimer::CTimer()
{
    mEn = 0;
}
CTimer::~CTimer()
{}

void CClock::start( int peri, int dly )
{
    mPeri = peri;//period = points per baud
    mDelay = dly;//timeout points
    mEn = 1;
    mTick = 0;
}

void CClock::stop()
{
    mEn = 0;
}

void CClock::sync( int syn )
{
    mTick = syn;
}

void CClock::sync()
{
    mTick = mPeri/2;
}

//! > 0 time out in tick step
int CClock::tick( int ticks )
{
    int tmo;

    if ( mEn < 1 )
    {
        return 0;
    }

    if ( mDelay > ticks )
    {
        mDelay -= ticks;
        return 0;
    }

    if ( mDelay == ticks )
    {
        mDelay = 0;
        mTick = 0;
        return ticks;
    }

    if ( mDelay != 0 && mDelay < ticks )
    {
        tmo = mDelay;

        mDelay = 0;
        mTick = 0;

        return tmo;
    }
    // timeout
    if ( mTick + ticks >= mPeri )
    {
        tmo = mPeri - mTick;
        mTick = 0;
        return ( tmo );
    }
    else
    {
        mTick += ticks;
        return 0;
    }
}

//! do not change inner tick
int CClock::tryTick( int ticks )
{

    if ( mEn < 1 )
    {
        return 0;
    }

    if ( mDelay > ticks )
    {
        return 0;
    }

    if ( mDelay != 0 && mDelay <= ticks )
    {
        return mDelay;
    }

    // timeout
    if ( mTick + ticks >= mPeri )
    {
        return ( mPeri - mTick );
    }
    else
    {
        return 0;
    }
}

void CTimer::start( int peri, int dly )
{
    mPeri = peri;
    mDelay = dly;
    mEn = 1;
    mTick = 0;
}

void CTimer::stop()
{
    mEn = 0;
}

void CTimer::sync( int syn )
{
    mTick = syn;
}

void CTimer::sync()
{
    mTick = mPeri/2;
}

//! > 0 time out in tick step
int CTimer::tick( int ticks )
{
    int tmo;

    if ( mEn < 1 )
    {
        return 0;
    }

    if ( mDelay > ticks )
    {
        mDelay -= ticks;
        return 0;
    }

    if ( mDelay == ticks )
    {
        mDelay = 0;
        mTick = 0;
        return ticks;
    }

    if ( mDelay != 0 && mDelay < ticks )
    {
        tmo = mDelay;

        mDelay = 0;
        mTick = 0;

        return tmo;
    }
    // timeout
    if ( mTick + ticks >= mPeri )
    {

        tmo = mPeri - mTick;
        mTick = 0;
        return ( tmo );
    }
    else
    {
        mTick += ticks;
        return 0;
    }
}

//! do not change inner tick
int CTimer::tryTick( int ticks )
{

    if ( mEn < 1 )
    {
        return 0;
    }

    if ( mDelay > ticks )
    {
        return 0;
    }

    if ( mDelay != 0 && mDelay <= ticks )
    {
        return mDelay;
    }

    // timeout
    if ( mTick + ticks >= mPeri )
    {
        return ( mPeri - mTick );
    }
    else
    {
        return 0;
    }
}


