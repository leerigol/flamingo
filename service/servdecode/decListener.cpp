#include "servdecode.h"
#include "decoder.h"

#include "../service_name.h"
#include "../servdso/servdso.h"



DsoErr servDec::onTraceReady(void *)
{
    //only when decoder is open
    if( m_nLed > 0 )
    {
        if( mTraceSem.available() < 1)
        {
            mTraceSem.release();
        }
        else
        {
            //qDebug() << "Why more than 0";
        }
    }

    return ERR_NONE;
}


int servDec::onTriggerSweep()
{
    int trigSweep;
    serviceExecutor::query( serv_name_trigger,
                            MSG_TRIGGER_SWEEP,
                            trigSweep );

    if ( trigSweep == Trigger_Sweep_Single )
    {
        onDispClear();
        servDec::mTraceSem.release();
    }
    else
    {}

    return ERR_NONE;
}

int servDec::onDispClear()
{
    getBus()->clear();
    getZoomBus()->clear();

    finishDecode(true);
    return ERR_NONE;
}

int servDec::onRunStop()
{
    int run = Control_Stop;
    serviceExecutor::query(serv_name_hori,
                           MSG_HORIZONTAL_RUN, run);
    if( run == Control_Run )
    {
        setChThreshold(chan1, getValidThreshold(chan1));
        setChThreshold(chan2, getValidThreshold(chan2));
        setChThreshold(chan3, getValidThreshold(chan3));
        setChThreshold(chan4, getValidThreshold(chan4));
    }

    return ERR_NONE;
}

int servDec::onChan1Change()
{
    if(m_bBusOn)
    {
        int stat = sysGetSystemStatus();
        bool cfg = true;
        if( stat == Control_Stoped ||
            stat == Control_waiting )
        {
            cfg = false;
        }
        setChThreshold(chan1, getValidThreshold(chan1), cfg);
    }
    if( isActive() )
    {
        updateAllUi( false );
    }
    return ERR_NONE;
}

int servDec::onChan2Change()
{
    if(m_bBusOn)
    {
        int stat = sysGetSystemStatus();
        bool cfg = true;
        if( stat == Control_Stoped ||
            stat == Control_waiting )
        {
            cfg = false;
        }
        setChThreshold(chan2, getValidThreshold(chan2), cfg);
    }
    if( isActive() )
    {
        updateAllUi( false );
    }
    return ERR_NONE;
}

int servDec::onChan3Change()
{
    if(m_bBusOn)
    {
        int stat = sysGetSystemStatus();
        bool cfg = true;
        if( stat == Control_Stoped ||
            stat == Control_waiting )
        {
            cfg = false;
        }
        setChThreshold(chan3, getValidThreshold(chan3), cfg);
    }
    if( isActive() )
    {
        updateAllUi( false );
    }
    return ERR_NONE;
}

int servDec::onChan4Change()
{
    if(m_bBusOn)
    {
        int stat = sysGetSystemStatus();
        bool cfg = true;
        if( stat == Control_Stoped ||
            stat == Control_waiting )
        {
            cfg = false;
        }
        setChThreshold(chan4, getValidThreshold(chan4), cfg);
    }
    if( isActive() )
    {
        updateAllUi( false );
    }
    return ERR_NONE;
}


#define CheckType(t,en) { \
                            if( t == m_nBusType && !en ) \
                            { \
                                m_nBusType = Bus_Parallel; \
                            }\
                        }

int servDec::onCheckLicense()
{
    bool en = sysCheckLicense(OPT_COMP);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_RS232, en);
    CheckType(Bus_RS232,en);

    en = sysCheckLicense(OPT_EMBD);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_SPI,en);
    CheckType(Bus_SPI,en);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_I2C,en);
    CheckType(Bus_I2C,en);

    en = sysCheckLicense(OPT_AUTO);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_CAN,en);
    CheckType(Bus_CAN,en);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_LIN,en);
    CheckType(Bus_LIN,en);

    en = sysCheckLicense(OPT_FLEX);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_FlexRay,en);
    CheckType(Bus_FlexRay,en);

    en = sysCheckLicense(OPT_AUDIO);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_I2S,en);
    CheckType(Bus_I2S,en);
    mUiAttr.setVisible(MSG_DECODE_I2S_RECEIVE,en);

    en = sysCheckLicense(OPT_AERO);
    mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_STD1553B,en);
    CheckType(Bus_STD1553B,en);

    //if(!sysCheckLicense(OPT_ARINC))
    {
        en = false;
        mUiAttr.setVisible(MSG_DECODE_TYPE, Bus_ARINC429,en);
        CheckType(Bus_ARINC429,en);
    }

    setLASource();

    return ERR_NONE;
}

int servDec::onViewChanged(void)
{
    query(serv_name_dso, servDso::CMD_SCREEN_MODE, m_nViewMode );

    if( m_nViewMode == screen_yt_main_zoom)
    {
       query(serv_name_hori, MSG_HORI_MAIN_SCALE, m_n64MainScale);
       query(serv_name_hori, MSG_HORI_MAIN_OFFSET, m_n64MainOffset);

       onZoomScale();
       onZoomOffset();
    }

    return ERR_NONE;
}


int servDec::onZoomScale()
{
    query(serv_name_hori, MSG_HORI_ZOOM_SCALE, m_n64ZoomScale);
    //qDebug() << "zoom scale:" << m_n64ZoomScale;
    return ERR_NONE;
}

qint64 servDec::getZoomScale()
{
    return m_n64ZoomScale;
}

int servDec::onZoomOffset()
{
    query(serv_name_hori, MSG_HORI_ZOOM_OFFSET, m_n64ZoomOffset);
//    qDebug() << "zoom scale:" << m_n64ZoomOffset;
    return ERR_NONE;
}

qint64 servDec::getZoomOffset()
{
    return m_n64ZoomOffset;
}
