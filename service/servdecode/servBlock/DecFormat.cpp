#include "DecFormat.h"

QChar CDecodeFormat::_hexAry[] =
{
    '0','1','2','3','4','5','6','7',
    '8','9','A','B','C','D','E','F'
};

//! asc table
QString CDecodeFormat::_ascTable[] = {
    "NUL",	"SOH",	"STX",	"ETX",	"EOT",	"ENQ",	"ACK",	"BEL",
    "BS",	"HT",	"LF",	"VT",	"FF",	"CR",	"SO",	"SI",
    "DLE",	"DC1",	"DC2",	"DC3",	"DC4",	"NAK",	"SYN",	"ETB",
    "CAN",	"EM",	"SUB",	"ESC",	"FS",	"GS",	"RS",	"US",

    "SP",	"!",	"\"",	"#",	"$",	"%",	"&",	"'",
    "(",	")",	"*",	"+",	",",	"-",	".",	"/",
    "0",	"1",	"2",	"3",	"4",	"5",	"6",	"7",
    "8",	"9",	":",	";",	"<",	"=",	">",	"?",

    "@",	"A",	"B",	"C",	"D",	"E",	"F",	"G",
    "H",	"I",	"J",	"K",	"L",	"M",	"N",	"O",
    "P",	"Q",	"R",	"S",	"T",	"U",	"V",	"W",
    "X",	"Y",	"Z",	"[",	"\\",	"]",	"^",	"_",

    "`",	"a",	"b",	"c",	"d",	"e",	"f",	"g",
    "h",	"i",	"j",	"k",	"l",	"m",	"n",	"o",
    "p",	"q",	"r",	"s",	"t",	"u",	"v",	"w",
    "x",	"y",	"z",	"{",	"|",	"}",	"~",	"DEL",
};


quint32 CDecodeFormat::trim( quint32 data,
                        int width )
{
    quint32 mask;
    if(width > 32 )
    {
        qDebug() << "WARNNING::trim:" << width << ":data:" << data;
        //Q_ASSERT( width <= 32 ); //for bug2168
        width = 32;
    }

    mask = 0xffffffff;
    mask = mask >> ( 32 - width );

    return data & mask;
}

int CHexFormat::format( unsigned int data,
                 int width,
                 QChar *pFmtStream )
{
    int bytes;
    data = trim( data, width);
    bytes = ( width + 3 ) / 4;

    while ( bytes > 0 )
    {
        pFmtStream[ bytes - 1 ] = _hexAry [ (data&0x0f) ];
        data >>= 4;
        bytes--;
    }
    return ( ( width + 3 ) / 4 );
}

int CDecFormat::format( unsigned int data,
                 int width,
                 QChar *pFmtStream )
{
    data = trim( data, width );

    QString str = QString("%1").arg(data);
    memcpy( pFmtStream, str.data(), str.size()*2 );
    return str.size();
}

int CBinFormat::format( unsigned int data,
                 int width,
                 QChar *pFmtStream )
{
    int binWidth;

    data = trim( data, width );

    binWidth = width;
    while( binWidth > 0 )
    {
        pFmtStream[ binWidth - 1 ] = _hexAry[  data & 0x01 ];

        data >>= 1;

        binWidth--;
    }

    return width;
}

int CAscFormat::format( unsigned int data,
                       int /*width*/,
                       QChar *pFmtStream )
{
    //! 0~127
    data = trim( data, 7 );

    QString &ascItem = _ascTable[data];

    memcpy( pFmtStream, ascItem.data(), ascItem.size()*2 );
    return ascItem.size();
}

CHexFormat CFormatFactory::_hexFormater;
CDecFormat CFormatFactory::_decFormater;
CBinFormat CFormatFactory::_binFormater;
CAscFormat CFormatFactory::_ascFormater;

CDecodeFormat *CFormatFactory::createFormater( DsoType::DecodeFormat fmt )
{
    switch( fmt )
    {
    case DsoType::F_HEX:
        return &CFormatFactory::_hexFormater;

    case DsoType::F_BIN:
        return &CFormatFactory::_binFormater;

    case DsoType::F_DEC:
        return &CFormatFactory::_decFormater;

    case DsoType::F_ASC:
        return &CFormatFactory::_ascFormater;

    default:
        return &CFormatFactory::_hexFormater;
    }
}
