#ifndef CDecIcon_H
#define CDecIcon_H

#include "DecContent.h"


class CDecIcon:public CDecContent
{
public :
    CDecIcon();
    virtual ~CDecIcon();

protected :
    blockIcon mIcon;

public:
    virtual void setIcon( blockIcon icon,
                  blockColor color = 21);

    void virtual paint( QPainter &painter,DsoType::DecodeFormat busFmt);
};

#endif
