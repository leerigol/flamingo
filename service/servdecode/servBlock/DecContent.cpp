
#include "DecContent.h"


CDecContent::CDecContent()
{
    u8ColorIndex = 0;
    u8TextIndex = 0;
}
CDecContent::~CDecContent()
{}

void CDecContent::busContent_SetColor( blockColor colorIndex)
{
    u8ColorIndex = colorIndex;
}
void CDecContent::busContent_SetText( blockCaption textIndex)
{
    u8TextIndex = textIndex;
}

void CDecContent::paint( QPainter &/*painter*/, DsoType::DecodeFormat /*busFmt*/ )
{
}


