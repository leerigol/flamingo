#include "Decblock_style.h"

decBlock_Style::decBlock_Style()
{
    bInited = false;

    mBrackWidth = 5;
    mBlockHeight = 30;
    mBlockCap = 30;

    mBorderColor = QColor::fromRgb( 0x0,204,0);
    mBorderWidth = 1;

    mFontSize = 12;

    mFgColor = Qt::white;
}

void decBlock_Style::loadResource( const QString & )
{
    if ( bInited != false ) return;



    bInited = true;
}
