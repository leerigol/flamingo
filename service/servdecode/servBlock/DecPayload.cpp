#include "DecPayload.h"
#include "DecBlock.h"

#include "DecFormat.h"


CDecPayload::CDecPayload()
{}

CDecPayload::~CDecPayload()
{}

void CDecPayload::setPayload( blockData data, blockWidth wid,
              blockColor color, blockCaption caption )
{
    if( wid > 32 )
    {
        qDebug() << "error DATA WIDTH:" << __FUNCTION__ << __LINE__;
    }
    u32DataSet = data;
    u8bitWidth = wid;
    u8ColorIndex = color;
    u8TextIndex = caption;
}

blockData CDecPayload::GetPayloadData()
{
    return u32DataSet;
}

blockWidth CDecPayload::getPayloadWidth()
{
    return u8bitWidth;
}

blockColor CDecPayload::getPayloadColor()
{
    return u8ColorIndex;
}

blockCaption CDecPayload::getPayloadCation()
{
    return u8TextIndex;
}

void CDecPayload::paint( QPainter &/*painter*/, DsoType::DecodeFormat /*busFmt*/ )
{
}

CDecPayloadData::CDecPayloadData()
{}
CDecPayloadData::~CDecPayloadData()
{}

void CDecPayloadData::paint( QPainter &painter, DsoType::DecodeFormat busFmt )
{
    //! format
    QString strFull, strAbbri, strNone;
    QString strSel;
    int blockSel;

    //! caption
    strFull = getFullCaption( u8TextIndex );
    strAbbri = getAbbriCaption( u8TextIndex );

    //! formater
    CDecodeFormat *pFmt = CFormatFactory::createFormater( busFmt );
    Q_ASSERT( pFmt != NULL );
    int streamLen;
    QChar streams[ 32 ];

    streamLen = pFmt->format( u32DataSet, u8bitWidth, streams );

    strFull = QString("%1:").arg(strFull);
    strFull.append( streams, streamLen );

    strAbbri = QString("%1:").arg(strAbbri);
    strAbbri.append( streams, streamLen );

    strNone.append( streams, streamLen );

    //! select shape
    blockSel = shapeSelection( painter,
                               strFull, strAbbri, strNone,
                               strSel );

    //! paint frame
    paintFrame( painter, blockSel, u8ColorIndex );

    //! paint text
    if ( strSel.size() > 0 )
    {
        QRect textRect;
        painter.setPen( _style.mFgColor );
        textRect.setRect(0,0, mSpan, _style.mBlockHeight );
        painter.drawText( textRect, Qt::AlignCenter, strSel );
    }
}

CDecPayloadCmd::CDecPayloadCmd()
{}
CDecPayloadCmd::~CDecPayloadCmd()
{}
void CDecPayloadCmd::paint( QPainter &painter, DsoType::DecodeFormat /*busFmt*/ )
{
    //! format
    QString strFull, strAbbri, strNone;
    QString strSel;
    int blockSel;

    //! caption
    strFull = getFullCaption( u8TextIndex );
    strAbbri = getAbbriCaption( u8TextIndex );

    //! formater
    CDecodeFormat *pFmt = CFormatFactory::createFormater( DsoType::F_HEX );
    Q_ASSERT( pFmt != NULL );
    int streamLen;
    QChar streams[ 32 ];

    streamLen = pFmt->format( u32DataSet, u8bitWidth, streams );

    strFull = QString("%1:").arg(strFull); strFull.append( streams, streamLen );
    strAbbri = QString("%1:").arg(strAbbri); strAbbri.append( streams, streamLen );
    strNone.append( streams, streamLen );

    //! select shape
    blockSel = shapeSelection( painter,
                               strFull, strAbbri, strNone,
                               strSel );

    //! paint frame
    paintFrame( painter, blockSel, u8ColorIndex );

    //! paint text
    if ( strSel.size() > 0 )
    {
        QRect textRect;
        painter.setPen( _style.mFgColor );
        textRect.setRect(0,0, mSpan, _style.mBlockHeight );
        painter.drawText( textRect, Qt::AlignCenter, strSel );
    }
}



CDecPayloadString::CDecPayloadString()
{}
CDecPayloadString::~CDecPayloadString()
{}

void CDecPayloadString::setPayload(blockColor color, blockCaption caption)
{
    u8ColorIndex = color;
    u8TextIndex = caption;
}

void CDecPayloadString::paint( QPainter &painter, DsoType::DecodeFormat /*busFmt*/ )
{
    //! format
    QString strFull, strAbbri, strNone;
    QString strSel;
    int blockSel;

    //! caption
    strFull = getFullCaption( u8TextIndex );
    strAbbri = getAbbriCaption( u8TextIndex );

    //! select shape
    blockSel = shapeSelection( painter,
                               strFull,
                               strAbbri,
                               strNone,
                               strSel );

    //! paint frame
    paintFrame( painter, blockSel, u8ColorIndex );

    //! paint text
    if ( strSel.size() > 0 )
    {
        QRect textRect;
        painter.setPen( _style.mFgColor );
        textRect.setRect(0,0, mSpan, _style.mBlockHeight );
        painter.drawText( textRect, Qt::AlignCenter, strSel );
    }
}

CDecPayloadPackage::CDecPayloadPackage()
{}
CDecPayloadPackage::~CDecPayloadPackage()
{}

void CDecPayloadPackage::setPayload( blockData data, blockWidth wid,
              blockColor color, blockCaption caption )
{
    mDataSet.append(data);
    u8bitWidth = wid;
    u8ColorIndex = color;
    u8TextIndex = caption;
}

void CDecPayloadPackage::addPayload(uint data)
{
    mDataSet.append(data);
}

void CDecPayloadPackage::getPayload(QList<uint> &data)
{
    data = mDataSet;
}

void CDecPayloadPackage::paint(QPainter &painter, DecodeFormat busFmt)
{
    //! format
    QString strFull, strAbbri, strNone;
    QString strSel;
    int blockSel;

    //! caption
    strFull = getFullCaption( u8TextIndex );
    strFull = QString("%1:").arg(strFull);

    strAbbri = getAbbriCaption( u8TextIndex );
    strAbbri = QString("%1:").arg(strAbbri);

    //! formater
    CDecodeFormat *pFmt = CFormatFactory::createFormater( busFmt );
    Q_ASSERT( pFmt != NULL );
    int streamLen;
    QChar streams[ 32 ];

    foreach(uint data, mDataSet)
    {
        streamLen = pFmt->format( data, u8bitWidth, streams );
        strFull.append( streams, streamLen );
        strAbbri.append( streams, streamLen );
        strNone.append( streams, streamLen );
    }



    //! select shape
    blockSel = shapeSelection( painter,
                               strFull, strAbbri, strNone,
                               strSel );

    //! paint frame
    paintFrame( painter, blockSel, u8ColorIndex );

    //! paint text
    if ( strSel.size() > 0 )
    {
        QRect textRect;
        painter.setPen( _style.mFgColor );
        textRect.setRect(0,0, mSpan, _style.mBlockHeight );
        painter.drawText( textRect, Qt::AlignCenter, strSel );
    }
}
