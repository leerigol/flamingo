#ifndef CDecContent_H
#define CDecContent_H

#include "DecBlock.h"

class CDecContent : public CDecBlock
{
public :
    CDecContent();
    virtual ~CDecContent();

public:
    struct struString
    {
        unsigned int s32Len;
        QString  pText;
    };

public:
    blockColor u8ColorIndex;            //! fg color
    blockCaption u8TextIndex;           //! caption

public:
    void busContent_SetColor( blockColor u8ColorIndex );
    void busContent_SetText( blockCaption u8TextIndex );

public:
    void virtual paint( QPainter &painter, DsoType::DecodeFormat busFmt );

};

#endif
