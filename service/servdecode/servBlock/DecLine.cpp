#include <QDebug>
#include "DecLine.h"
#include "../../include/dsocfg.h"

CDecLine::CDecLine()
{
    mXPos = 0;
    mXSpan = wave_width;

    mTop = 0;
    bLabel = true;
}

CDecLine::~CDecLine()
{
    //clear();
}

void CDecLine::setXPosSpan( int pos, int span )
{
    mXPos = pos;
    mXSpan = span;
}

void CDecLine::setTop( int pos )
{
    mTop = pos;
}

void CDecLine::setLabelEn(bool b)
{
    bLabel = b;
}

bool CDecLine::getLabelEn()
{
    return bLabel;
}

void CDecLine::setLabel( const QString &label )
{
    mLabel = label;
}

QString &CDecLine::getLabel()
{
    return mLabel;
}

void CDecLine::append( CDecBlock *block )
{
    Q_ASSERT( NULL != block );
    mItems.append( block );
}

void CDecLine::empty()
{
    mItems.clear();
}

int CDecLine::size()
{
    return mItems.size();
}

void CDecLine::clear()
{
    CDecBlock *pBlock;

    foreach( pBlock, mItems )
    {
        //qDebug() << "del:" << pBlock;
        Q_ASSERT( pBlock != NULL );
        delete pBlock;        
    }
    mItems.clear();

    //qDebug() << "-------------------end----------";
}

void CDecLine::paint( QPainter &painter,
                      DsoType::DecodeFormat fmt,
                      bool showLabel)
{
    int lineY;
    int lineX1 = mXPos;
    int lineX2 = mXPos + mXSpan - 1;

    //! draw the line
    QPen pen;
    pen.setColor( CDecBlock::_style.mBorderColor );
    pen.setWidth( CDecBlock::_style.mBorderWidth );

    painter.save();


    //! draw the label
    if ( showLabel )
    {
        if( mLabel.size() > 0 )
        {
            QRect r;
            r.setX(mXPos);
            r.setY(mTop);
            r.setWidth(mLabel.size()*10);
            r.setHeight(CDecBlock::_style.mBlockHeight-6);

            painter.fillRect( r, Qt::black);
            painter.setPen(CDecBlock::_style.mBorderColor);
            painter.drawRect(r);

            painter.setPen( Qt::white );
            painter.drawText( r, Qt::AlignCenter,  mLabel );

            mTop = mTop + CDecBlock::_style.mBlockHeight-6 + 1;
        }
    }

    painter.setPen( pen );
    lineY = mTop + CDecBlock::_style.mBlockHeight / 2 + 1;
    painter.drawLine( lineX1, lineY, lineX2, lineY );
    painter.restore();

    //not good. freeze limit
    if( mItems.size() < 500 )
    {
        //! draw blocks
        CDecBlock *pBlock;

        foreach ( pBlock, mItems )
        {
            Q_ASSERT( NULL != pBlock );

            painter.save();
            painter.translate( pBlock->mPos, lineY - CDecBlock::_style.mBlockHeight / 2  );
            pBlock->paint( painter, fmt );
            painter.restore();

        }
    }
    else
    {

    }
}
