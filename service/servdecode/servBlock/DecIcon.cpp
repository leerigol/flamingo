#include "DecIcon.h"
#include "DecBlock.h"


CDecIcon::CDecIcon()
{
    bIcon = true;
}
CDecIcon::~CDecIcon()
{}

void CDecIcon::setIcon( blockIcon icon, blockColor color)
{
    mIcon = icon;
    u8ColorIndex = color;
}

void CDecIcon::paint( QPainter &painter, DsoType::DecodeFormat /*busFmt*/)
{

    QColor color = getColor( u8ColorIndex ); //! format

    QRect textRect;

    QString icon = getIcon(mIcon);


    textRect.setRect(0,0, icon.size()*10, _style.mBlockHeight-1 );

    painter.setPen(Qt::black);
    painter.fillRect(textRect, Qt::SolidPattern );

    painter.setPen(CDecBlock::_style.mBorderColor);
    painter.drawRect(textRect);

    painter.setPen( color );
    painter.drawText( textRect, Qt::AlignCenter, getIcon(mIcon) );

    //qDebug() << getIcon(mIcon) << mPos << mSpan;
}

