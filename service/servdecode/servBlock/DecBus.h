#ifndef CDECBUS_H
#define CDECBUS_H

#include "../../include/dsotype.h"
#include "DecLine.h"

class CDecBus
{
public:
    CDecBus();
    ~CDecBus();

public:
    void setYPos( int ypos );
    int  getYPos();

    void setFmt( DsoType::DecodeFormat fmt );
    DsoType::DecodeFormat getFmt();

    void append( CDecLine *pLine );
    void clear();//free memory of items
    void empty();//not free memory of items
    int  size();
    void setLabelEn( bool b);

    void paint( QPainter &painter );

private:
    void sortLines();

private:
    int mYPos;          //! bus pos
    DsoType::DecodeFormat mFmt;
    QList< CDecLine *> mLines;
    QSemaphore locker;
    bool bLabel;
};

#endif // CDECBUS_H
