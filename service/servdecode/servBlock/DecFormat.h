#ifndef CDECFORMAT_H
#define CDECFORMAT_H

#include <QtCore>

#include "../../include/dsotype.h"


class CDecodeFormat
{
public:
    static QChar _hexAry[];
    static QString _ascTable[];

public:
    virtual int format( unsigned int data,
                 int width,
                 QChar *pFmtStream ) = 0;

protected:
    quint32 trim( quint32 data,
                   int width );
};

class CHexFormat : public CDecodeFormat
{

public:
    virtual int format( unsigned int data,
                  int width,
                 QChar *pFmtStream );
};

class CDecFormat : public CDecodeFormat
{

public:
    virtual int format( unsigned int data,
                  int width,
                 QChar *pFmtStream );
};

class CBinFormat : public CDecodeFormat
{

public:
    virtual int format( unsigned int data,
                  int width,
                 QChar *pFmtStream );
};

class CAscFormat : public CDecodeFormat
{

public:
    virtual int format( unsigned int data,
                 int width,
                 QChar *pFmtStream );
};

class CFormatFactory
{
private:
    static CHexFormat _hexFormater;
    static CDecFormat _decFormater;
    static CBinFormat _binFormater;
    static CAscFormat _ascFormater;

public:
    static CDecodeFormat *createFormater( DsoType::DecodeFormat fmt );

};


#endif // CDECFORMAT_H
