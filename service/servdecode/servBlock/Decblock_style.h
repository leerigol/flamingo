#ifndef DECBLOCK_STYLE_H
#define DECBLOCK_STYLE_H

#include <QColor>

class decBlock_Style
{
public:
    decBlock_Style();

public:
    void loadResource( const QString &path );

public:
    int mBrackWidth;
    int mBlockHeight;
    int mBlockCap;

    QColor mBorderColor;
//    QColor mBgColor;
    QColor mFgColor;
    int mBorderWidth;

    int mFontSize;

private:
    bool bInited;
};

#endif // DECBLOCK_STYLE_H
