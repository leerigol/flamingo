#include <QDebug>
#include "DecBus.h"
#include "../../../include/dsoassert.h"

CDecBus::CDecBus()
{
    mYPos = 100;
    locker.release();
}

CDecBus::~CDecBus()
{

}

void CDecBus::setYPos( int ypos )
{
    mYPos = ypos;
}
int  CDecBus::getYPos()
{
    return mYPos;
}

void CDecBus::setFmt( DsoType::DecodeFormat fmt )
{
    mFmt = fmt;
}
DsoType::DecodeFormat CDecBus::getFmt()
{
    return mFmt;
}

void CDecBus::append( CDecLine *pLine )
{
    mLines.append( pLine );
}

void CDecBus::empty()
{
    locker.acquire();
    CDecLine *pLine;
    foreach( pLine, mLines )
    {
        pLine->empty();
        delete pLine;
        pLine = NULL;
    }
    locker.release();
}

void CDecBus::clear()
{
    locker.acquire();
    CDecLine *pLine;

    foreach( pLine, mLines )
    {
        pLine->clear();
        delete pLine;
        pLine = NULL;
    }
    mLines.clear();
    locker.release();
}

int CDecBus::size()
{
    return mLines.size();
}

void CDecBus::setLabelEn(bool b)
{
//    CDecLine *pLine;
//    foreach( pLine, mLines )
//    {
//        pLine->setLabelEn(b);
//    }
    bLabel = b;
}

void CDecBus::paint( QPainter &painter )
{
    if( locker.available() > 0 )
    {
        locker.acquire();
        int l = mLines.size();
        if( l == 1 || l == 2)
        {
            sortLines();
    
            CDecLine *pLine;
            foreach( pLine, mLines )
            {                
                 pLine->paint( painter, mFmt , bLabel);
            }
        }
    
        locker.release();
    }
    else
    {
        qDebug() << "locked bus";
    }
}

void CDecBus::sortLines()
{
    int l = mLines.size();
    int top = CDecBlock::_style.mBlockHeight + CDecBlock::_style.mBlockCap;
    if ( l == 2 )
    {
        list_at(mLines,0)->setTop( 0 );
        list_at(mLines,1)->setTop( top );
    }
    else if ( l == 1 )
    {
        list_at(mLines,0)->setTop( 0 );
    }
    else
    {
        qDebug() << "!!!!Error!!!!" << __FUNCTION__ << __LINE__ << l;
//        Q_ASSERT(false);
    }

}
