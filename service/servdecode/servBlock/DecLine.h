#ifndef DECLINE_H
#define DECLINE_H

#include "DecBlock.h"

class CDecBus;
class CDecLine
{
public:
    CDecLine();
    ~CDecLine();

public:
    void setXPosSpan( int pos, int span );
private:
    void setTop( int pos );
public:
    void setLabel( const QString &label );
    QString &getLabel();
    void setLabelEn(bool b = true);
    bool getLabelEn();

    void append( CDecBlock *block );
    void clear();//free
    void empty();//not free
    int  size();
    void setFrameCount(uint c) { mFrameCount = c; }
    uint getFrameCount();

public:
    void paint( QPainter &painter,
                DsoType::DecodeFormat fmt,
                bool showLabel = true);

private:
    int mXPos, mTop;
    int mXSpan;
    bool bLabel;
    uint mFrameCount;

    QString mLabel;
    QList< CDecBlock *> mItems;
friend class CDecBus;
};

#endif // DECLINE_H
