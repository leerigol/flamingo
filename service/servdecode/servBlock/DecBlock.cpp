
#include <QFontMetrics>
#include <QDebug>

#include "DecBlock.h"
#include "../servBlock/DecPayload.h"
#include "../servBlock/DecIcon.h"


//! block style
decBlock_Style CDecBlock::_style;

//! caption table
struBlockCaption CDecBlock::_captions[] =
{
    {"",""},                    // 0
    {"Data","D"},
    {"Write","W"},
    {"Read","R"},

    {"CRC","C"},
    {"DLC",""},
    {"ID",""},
    {"ID","ID"},

    {"PL","PL"},	// 8
    {"HCRC","HCRC"},      // 9
    {"Count","CNT"},			// 10
    {"TCRC","TCRC"},		// 11

    {"TSS","TSS"},              // 12
    {"DTS","DTS"},              // 13
    {"NORM","N"},           // 14
    {"Startup","ST"},          // 15

    {"SYNC","SY"},             // 16
    {"NULL","NUL"},             // 17

    {"C/S","C/S"},              // 18
    {"RTA","RTA"},              // 19
    {"CSW","CSW"},              // 20
    {"RMT","R"},           // 21
    {"ACK","A"},                // 22
    {"Delimiter","DELI"},       // 23
    {"Stuff","STUF"},           // 24

    {"Break","B"},		// 25
    {"Wakeup","W"},     // 26
    {"SYNC","S"},		// 27
    {"Parity","P"},     // 28


    {"Start","S"},		//29 SBUS
    {"Frame","Fr"},     //30
    {"Fail","Fa"},      //31
    {"EOF","E"},   //32

    {"CH1","1"},        //33
    {"CH2","2"},        //34
    {"CH3","3"},        //35
    {"CH4","4"},        //36

    {"CH5","5"},        //37
    {"CH6","6"},        //38
    {"CH7","7"},        //39
    {"CH8","8"},        //40

    {"STOP","S"},        //41
    {"CH10","10"},      //42
    {"CH11","11"},      //43
    {"CH11","12"},      //44

    {"CH12","13"},      //45
    {"CH14","14"},      //46
    {"CH15","15"},      //47
    {"CH16","16"},      //48

    {"CH17","17"},      //49
    {"CH18","18"},      //50

    {"Master","M"},     //51

    {"Left","L"},       //52
    {"Right","R"},      //53

    {"MANCH","M"},      //54

    {"End","E"},        //55
};

//! caption table
#define RGB_BUS( r,g, b)  ( ( r<<16) | (g<<8) | (b) )
QRgb CDecBlock::_colors[]=
{
    RGB_BUS( 0,102,0),          //0
    RGB_BUS( 255,0,0),          //1

    RGB_BUS( 0,128,128 ),       //2
    RGB_BUS( 128,128,0 ),       //3
    RGB_BUS( 102,102,0),		//4

    RGB_BUS(255,128,128),   //5
    RGB_BUS(255,0,128),		//6
    RGB_BUS(255,0,0),		//7
    RGB_BUS(0,64,128),		//8
    RGB_BUS(128,128,0),		//9
    RGB_BUS(64,100,255),	//10
    RGB_BUS( 0,102,0),		//11
    RGB_BUS(128,128,0),		//12
    RGB_BUS(255,100,20),	//13

    RGB_BUS(255,0,0),		//14
    RGB_BUS(0,255,255),		//15

    RGB_BUS(0,0,255),		//16
    RGB_BUS( 128,128,255),	//17

    RGB_BUS(255,80,0),		//18

    RGB_BUS(0,128,128),		//19
    RGB_BUS(0,0,255),		//20
    RGB_BUS(0,0,0)          //21
};

CDecBlock::CDecBlock()
{
    busBlock_Init();

    _style.loadResource("ui/decode/block/");
}
CDecBlock::~CDecBlock()
{
}

bool CDecBlock::isIcon()
{
    return bIcon;
}

void CDecBlock::busBlock_Init()
{
    mPos = 0;
    mSpan = 0;
    mShape = FRAME_INTER;
    bIcon  = false;
}

void CDecBlock::busBlock_SetPos(unsigned int pos)
{
    mPos = pos;
}
void CDecBlock::busBlock_SetSpan( int span)
{
    mSpan= span;
}

void CDecBlock::setPosSpan( unsigned int pos, int span )
{
    mPos = pos;
    mSpan = span;
}
void CDecBlock::setShape( blockShape shape )
{
    mShape = shape;
}

//! ui used
void CDecBlock::paint( QPainter &/*painter*/, DsoType::DecodeFormat /*fmt*/ )
{
    qDebug() << "You won't come here";
}


void CDecBlock::paintFrame( QPainter &painter, int shapeSel, int bgColorIndex )
{
    painter.save();

    QPen pen;
    pen.setColor( _style.mBorderColor );
    pen.setWidth( _style.mBorderWidth );
    painter.setPen( pen );

    QColor color = getColor( bgColorIndex );

    if ( shapeSel == block_shape_simple )
    {
        paintSimpleFrame( painter, color );
    }
    else
    {
        paintFullFrame( painter, color );
    }

    painter.restore();
}


void CDecBlock::paintSimpleFrame( QPainter &painter, QColor &bgColor )
{
    QPainterPath path;

    path.addRect( 0,0, mSpan, _style.mBlockHeight );
    painter.fillPath( path, bgColor );

    //! mid
    painter.drawLine( 0, 0, mSpan-1, 0 );
    painter.drawLine( 0, _style.mBlockHeight-1, mSpan-1, _style.mBlockHeight-1 );


    //! beg
    if ( mShape == FRAME_INTER
         || mShape == FRAME_RIGHT )
    {
        painter.drawLine( 0, 0,
                          0, _style.mBlockHeight-1 );

    }

    //! end
    if ( mShape == FRAME_INTER
         || mShape == FRAME_LEFT
         )
    {
        painter.drawLine( mSpan-1, 0,
                          mSpan-1, _style.mBlockHeight-1 );
    }


}

void CDecBlock::paintFullFrame( QPainter &painter, QColor &bgColor )
{
    int beg, end;

    QPainterPath path;
    QPainterPath subPath;

    //! init path
    path.addRect( 0,0, mSpan, _style.mBlockHeight );

    //! beg
    if ( mShape == FRAME_INTER
         || mShape == FRAME_RIGHT )
    {
        //! lt
        subPath.moveTo( 0, 0 );
        subPath.lineTo( 0, _style.mBlockHeight/2 );
        subPath.lineTo( _style.mBrackWidth-1, 0 );
        subPath.closeSubpath();

        path = path.subtracted( subPath );

        //! ld
        subPath.moveTo( 0, _style.mBlockHeight/2 );
        subPath.lineTo( 0, _style.mBlockHeight );
        subPath.lineTo( _style.mBrackWidth-1, _style.mBlockHeight );
        subPath.closeSubpath();

        path = path.subtracted( subPath );
    }

    //! end
    if ( mShape == FRAME_INTER
         || mShape == FRAME_LEFT
         )
    {
        //! rt
        subPath.moveTo( mSpan , _style.mBlockHeight/2 );
        subPath.lineTo( mSpan - _style.mBrackWidth, 0 );
        subPath.lineTo( mSpan , 0 );
        subPath.closeSubpath();

        path = path.subtracted( subPath );

        //! rd
        subPath.moveTo( mSpan , _style.mBlockHeight/2 );
        subPath.lineTo( mSpan  - _style.mBrackWidth, _style.mBlockHeight );
        subPath.lineTo(  mSpan , _style.mBlockHeight );
        subPath.closeSubpath();

        path = path.subtracted( subPath );

    }

    //! bgd
    painter.fillPath( path, bgColor );

    //! beg
    if ( mShape == FRAME_INTER
         || mShape == FRAME_RIGHT )
    {
        painter.drawLine( 0, _style.mBlockHeight/2,
                          _style.mBrackWidth-1, 0 );

        painter.drawLine( 0, _style.mBlockHeight/2,
                          _style.mBrackWidth-1, _style.mBlockHeight-1 );

        beg = _style.mBrackWidth;
    }
    else
    {
        beg = 0;
    }

    //! end
    if ( mShape == FRAME_INTER
         || mShape == FRAME_LEFT
         )
    {
        painter.drawLine( mSpan-1, _style.mBlockHeight/2,
                          mSpan-1 -_style.mBrackWidth, 0 );

        painter.drawLine( mSpan-1, _style.mBlockHeight/2,
                          mSpan-1 - _style.mBrackWidth, _style.mBlockHeight-1 );

        end = mSpan - 1 - _style.mBrackWidth;
    }
    else
    {
        end = mSpan-1;
    }

    //! mid
    painter.drawLine( beg, 0, end, 0 );
    painter.drawLine( beg, _style.mBlockHeight-1, end, _style.mBlockHeight-1 );
}


int CDecBlock::getFullTextArea()
{
    switch( mShape )
    {
        case FRAME_INTER:
            return  -_style.mBrackWidth*2 + mSpan;

        case FRAME_LEFT:
        case FRAME_RIGHT:
            return -_style.mBrackWidth + mSpan;

        case FRAME_SPAN:
            return  mSpan;

        default:
            return -_style.mBrackWidth*2 + mSpan;
    }
}
int CDecBlock::getSimpleTextArea()
{
    switch( mShape )
    {
        case FRAME_INTER:
            return -1*2 + mSpan;

        case FRAME_LEFT:
        case FRAME_RIGHT:
            return -1 + mSpan;

        case FRAME_SPAN:
            return mSpan;

        default:
            return -1*2 + mSpan;
    }
}


int CDecBlock::shapeSelection( QPainter &painter,
                                    QString &full,
                                    QString &abbri,
                                    QString &strNone,
                                    QString &strSel
                                    )
{
    int width;
    int fullTxtArea, simpleTxtArea;

    QFontMetrics metrics = painter.fontMetrics();

    fullTxtArea = getFullTextArea();
    simpleTxtArea = getSimpleTextArea();

    //! try full
    strSel = full;
    width = metrics.width( full );
    if ( width <= fullTxtArea )
    {
        return block_shape_full;
    }

    if ( width <= simpleTxtArea )
    {
        return block_shape_simple;
    }

    //! try abbri
    strSel = abbri;
    width = metrics.width( abbri );
    if ( width <= fullTxtArea )
    {
        return block_shape_full;
    }

    if ( width <= simpleTxtArea )
    {
        return block_shape_simple;
    }

    //! try none
    strSel = strNone;
    width = metrics.width( strNone );
    if ( width <= fullTxtArea )
    {
        return block_shape_full;
    }

    if ( width <= simpleTxtArea )
    {
        return block_shape_simple;
    }

    //! else
    strSel = "...";
    return block_shape_simple;
}

QString& CDecBlock::getFullCaption( int capId )
{
    int count = array_count( CDecBlock::_captions );
    if ( capId < 0 || capId >= count )
    {
        //qWarning()<<"over range";
        return CDecBlock::_captions[0].mFull;
    }

    //! has full
    if ( CDecBlock::_captions[capId].mFull.size() > 0 )
    {
        return CDecBlock::_captions[capId].mFull;
    }

    return CDecBlock::_captions[capId].mAbbri;
}

QString& CDecBlock::getAbbriCaption( int capId )
{
    int count = array_count( CDecBlock::_captions );
    if ( capId < 0 || capId >= count )
    {
        //qWarning()<<"over range";
        return CDecBlock::_captions[0].mFull;
    }

    //! has abbri
    if ( CDecBlock::_captions[capId].mAbbri.size() > 0 )
    {
        return CDecBlock::_captions[capId].mAbbri;
    }

    return CDecBlock::_captions[capId].mFull;
}


QColor CDecBlock::getColor( int colorId )
{
    int count = array_count( CDecBlock::_colors );
    if ( colorId < 0 || colorId >= count )
    {
        //qWarning()<<"over range";
        return QColor( CDecBlock::_colors[0] );
    }

    return QColor( CDecBlock::_colors[colorId] );
}

QString CDecBlock::getIcon(int icon)
{
    switch(icon)
    {
    case ICON_QUE: return "P";
    case ICON_EXC: return "!";
    case ICON_ERROR: return "S";
    case ICON_END:   return "EOF";
    case ICON_RMT:   return "RMT";
    }

    return "UNK";
}
