#ifndef CDecPayload_H
#define CDecPayload_H

#include "DecContent.h"


class CDecPayload: public CDecContent
{
public :
    CDecPayload();
    virtual ~CDecPayload();

public:
    virtual void setPayload( blockData data, blockWidth wid = 8,
                  blockColor color = 21, blockCaption caption = TEXT_DATA );

    blockData GetPayloadData();
    blockWidth getPayloadWidth();
    blockColor getPayloadColor();
    blockCaption getPayloadCation();
public:
    virtual void paint( QPainter &painter, DsoType::DecodeFormat busFmt );

protected:
    blockData u32DataSet;
    blockWidth u8bitWidth;
    blockColor u8ColorIndex ;
    blockCaption u8TextIndex ;
};

class CDecPayloadData:public CDecPayload
{
public:
    CDecPayloadData();
    virtual ~CDecPayloadData();

public:
    virtual void paint( QPainter &painter, DsoType::DecodeFormat busFmt );
};

class CDecPayloadCmd:public CDecPayloadData
{
public:
    CDecPayloadCmd();
    virtual ~CDecPayloadCmd();

public:
    virtual void paint( QPainter &painter, DsoType::DecodeFormat busFmt );
};

class CDecPayloadString:public CDecPayload
{
public:
    CDecPayloadString();
    virtual ~CDecPayloadString();

    void setPayload(blockColor color = 21, blockCaption caption = TEXT_DATA );

public:
    virtual void paint( QPainter &painter, DsoType::DecodeFormat busFmt );
};

class CDecPayloadPackage:public CDecPayload
{
public:
    CDecPayloadPackage();
    virtual ~CDecPayloadPackage();

    virtual void setPayload( blockData data, blockWidth wid = 8,
                  blockColor color = 21, blockCaption caption = TEXT_DATA );

    void addPayload( uint data);
    void getPayload( QList<uint>&);
public:
    virtual void paint( QPainter &painter, DsoType::DecodeFormat busFmt );

protected:
    QList<uint> mDataSet;
};

#endif
