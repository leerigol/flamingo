
// DecBlock.h: interface for the DecBlock class.
////interface for block ,the only visible interface outside
//////////////////////////////////////////////////////////////////////

#pragma once

#include <QPainter>
#include "Decblock_style.h"
#include "../../include/dsotype.h"
#include "../eventTable/ctable.h"


//frame type
typedef unsigned char blockType;

#define CONTENT_DATA		0
#define CONTENT_SEPRATOR    1
#define CONTENT_LINE		2
#define CONTENT_ICON		3
#define CONTENT_EMPTY		4
#define CONTENT_DESC		5
#define CONTENT_CMD         6
#define CONTENT_CRLF        7


//icon shape
typedef unsigned char blockIcon;

#define ICON_QUE		0			//?
#define ICON_EXC		1			//!
#define ICON_L_BRACKET	2			//(
#define ICON_R_BRACKET	3			//)
#define ICON_L_RECT		4			//[
#define ICON_R_RECT		5			//]
#define ICON_L_PEAK		6			//{
#define ICON_R_PEAK		7			//}
#define ICON_START      8           //<
#define ICON_END        9           //>
#define ICON_ERROR      10          //S
#define ICON_RMT        11

//caption id
typedef     unsigned char blockCaption;

#define TEXT_NONE				0			// ""
#define TEXT_DATA				1			// "DATA"
#define TEXT_W					2			// "W"
#define TEXT_R					3			// "R"

#define TEXT_CRC				4           // "CRC"
#define TEXT_DLC				5           // "DLC"
#define TEXT_ID					6           // "ID"

#define TEXT_FRAME_ID 			7           // "FID"
#define TEXT_PL					8           // "PL"
#define TEXT_HCRC				9           // "HCRC"
#define TEXT_CYCCNT				10			// "CYCCNT"
#define TEXT_TCRC				11          // "TCRC"

#define TEXT_TSS     			12   		// TSS
#define TEXT_DTS     			13   		// DTS
#define TEXT_INDICATOR_NORMAL 	14          // NORMAL  NORM
#define TEXT_INDICATOR_STARTUP 	15          // STARTUP START
#define TEXT_INDICATOR_SYNC 	16          // SYNC
#define TEXT_INDICATOR_NULL 	17          // NULL

// MIL1553
#define TEXT_CS          	18              // C/S
#define TEXT_RT			  	19			  	// RT
#define TEXT_CSW		  	20			  	// CSW

#define TEXT_RMT		  	21				// RMT
#define TEXT_ACK		  	22				// ACK

#define TEXT_DELI			23				// DELI
#define TEXT_STUFF			24				// STUFF

// LIN
#define TEXT_BREAK			25				// BREAK / B
#define TEXT_WAKEUP			26				// WAKEUP / W
#define TEXT_SYNC			27				// SYNC  / S
#define TEXT_PARITY			28				// Parity / P


// TEXT
#define TEXT_START			29              // START
#define TEXT_FRAME_BIT		30              // FRAME BIT
#define TEXT_FAIL_SAFE_ACT 	31              // FAIL SAFE ACTIVATED
#define TEXT_EOF				32			// eof

#define TEXT_CH1				33			// CH1
#define TEXT_CH2				34
#define TEXT_CH3				35
#define TEXT_CH4				36

#define TEXT_CH5				37
#define TEXT_CH6				38
#define TEXT_CH7				39
#define TEXT_CH8				40

#define TEXT_CH9				41
#define TEXT_CH10			42
#define TEXT_CH11			43
#define TEXT_CH12			44

#define TEXT_CH13			45
#define TEXT_CH14			46
#define TEXT_CH15			47
#define TEXT_CH16			48

#define TEXT_CH17			49
#define TEXT_CH18			50

#define TEXT_MAST           51     //I2C Master Code

//Left channel
#define TEXT_LEFT           52
//Right channel
#define TEXT_RIGHT          53

#define TEXT_MANCH			54

#define TEXT_END			55


//! COLOR ID
typedef     unsigned char blockColor;

#define     COMM_DATA_COLOR				    0
#define     COMM_ADDR_COLOR				    1
#define     COMM_START_COLOR                COMM_ADDR_COLOR
#define     COMM_END_COLOR                  COMM_ADDR_COLOR
#define     COMM_ACK_COLOR					4
#define     ERR_COLOR                       14

#define     IIC_MAST_COLOR                  11
#define     DELI_COLOR						4
#define 	STUFF_COLOR					    4
#define     CAN_ID_COLOR					3
#define 	CAN_DATA_COLOR				    COMM_DATA_COLOR
#define     CAN_DLC_COLOR					2
#define     CAN_CRC_COLOR					3
#define     CAN_OVERLOAD_COLOR			    4
#define     CAN_ID_RMT_COLOR				18
#define     FLEX_TSS_DTS_COLOR			    5
#define 	FLEX_INDICATOR_COLOR     		6
#define     FLEX_FRAME_ID_COLOR			    19
#define     FLEX_PL_COLOR					8
#define     FLEX_HCRC_COLOR				    9
#define     FLEX_CYC_COLOR				    10
#define 	FLEX_DATA_COLOR				    11
#define     FLEX_TCRC_COLOR				    12
#define     FLEX_FRAME_COLOR				13
#define     ICON_QUE_RED_COLOR			    14
#define     ICON_QUE_YEL_COLOR			    15
#define     ICON_RED_COLOR				    14
#define     ICON_GREEN_COLOR				19
#define     ICON_BLUE_COLOR				    20
#define     ERR_DATA_COLOR				    ICON_RED_COLOR
#define     FRAME_LINE_COLOR				17
#define     FRAME_BACK_FILL_BLCOMM_ACK_COLOR 21
#define     IIS_LEFT_COLOR				    ICON_GREEN_COLOR
#define     IIS_RIGHT_COLOR				    ICON_BLUE_COLOR

// LIN
#define     LIN_BREAK_COLOR				   CAN_DLC_COLOR
#define		LIN_ID_COLOR				   CAN_ID_COLOR
#define     LIN_DATA_COLOR				   COMM_DATA_COLOR
#define     LIN_CRC_COLOR				   CAN_CRC_COLOR
#define     LIN_WK_COLOR				   CAN_ID_RMT_COLOR
#define     LIN_SYNC_COLOR				   CAN_OVERLOAD_COLOR
#define     LIN_WK_BK_COLOR				   FLEX_TSS_DTS_COLOR
#define 	LIN_PARITY_COLOR			   FLEX_INDICATOR_COLOR

// 1553B
#define     STD1553_CS_COLOR			   2
#define     STD1553_ADDR_COLOR		       1

// SBUS
#define     SBUS_START_COLOR		CAN_ID_COLOR
#define     SBUS_END_COLOR			CAN_CRC_COLOR
#define     SBUS_RES_COLOR			CAN_OVERLOAD_COLOR
#define     SBUS_FRAME_BIT_COLOR	FLEX_TSS_DTS_COLOR
#define     SBUS_FAIL_SAFE_ACT_COLOR FLEX_INDICATOR_COLOR

#define     LIMIT_RESULT_SIZE       (24000)

#define FRAME_INTER		0X0         //<==>
#define FRAME_LEFT		0X1         // ==>
#define FRAME_RIGHT		0X2         //<==
#define FRAME_SPAN		0X3         //==


//! block data
typedef unsigned int   blockData;
typedef unsigned char blockWidth;   //! bit width
//! data frame  shape
typedef unsigned char blockShape;

#define block_shape_simple 0
#define block_shape_full   1

struct struBlockCaption
{
    QString mFull;
    QString mAbbri;
};

class CDecBus;
class CDecLine;
class CDecContent;
class CDecBlock
{
    public:
        CDecBlock();
        virtual ~CDecBlock();

    public:
        static decBlock_Style _style;
                                    //! cache tables

        static struBlockCaption _captions[];
        static QRgb _colors[];

    public:
        void busBlock_Init();
        void busBlock_SetPos(unsigned int pos);
        void busBlock_SetSpan(int span);

        void setPosSpan( unsigned int pos, int span );
        void setShape( blockShape shape );
        bool isIcon();

    public:
        virtual void paint( QPainter &painter, DsoType::DecodeFormat fmt );

    protected:
        void paintFrame( QPainter &painter, int shapSel, int bgColorIndex );
        void paintSimpleFrame( QPainter &painter, QColor &bgColor );
        void paintFullFrame( QPainter &painter, QColor &bgColor );

        int getFullTextArea();
        int getSimpleTextArea();

        int shapeSelection( QPainter &painter,
                                            QString &full,
                                            QString &abbri,
                                            QString &strNone,
                                            QString &strSel
                                            );

        QString& getFullCaption( int capId );
        QString& getAbbriCaption( int capId );

        QColor   getColor( int colorId );
        QString  getIcon(int icon);

public:
    unsigned int mPos;          //! block pos
    unsigned int mSpan;         //! block width
    unsigned int mStp;          //! block stop
    blockShape   mShape;        //! block shape
    bool         bIcon;


    friend class CDecLine;
    friend class CDecBus;
};
