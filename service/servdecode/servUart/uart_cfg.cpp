#include "servdecode.h"
#include "uart_cfg.h"

void CUartCfg::reset()
{
    mTxSrc     =   chan1;
    mRxSrc     =   chan_none;

    mBaud      =   BaudRate_9600;
    mDataWidth =   Uart_DataWid_8;

    mLSB       =   lsb;
    mNegative  =   UART_POSITIVE;

    mPackgeEn  =   PACKAGE_DISEN;
    mPackgeEnd =   PACKAGEEND_NUL;

    mParity    =   PARITY_NONE;
    mStopBit   =   STOP_WIDTH_1;
}

void CUartCfg::serialIn(CStream & stream)
{
    int temp = 0;
    stream.read("mTxSrc",     temp);
    mTxSrc = (Chan)temp;
    stream.read("mRxSrc",     temp);
    mRxSrc = (Chan)temp;
    stream.read("mBaud",      mBaud);
    stream.read("mDataWidth", mDataWidth);
    stream.read("mLSB",       mLSB);
    stream.read("mNegative",  mNegative);
    stream.read("mPackgeEn",  mPackgeEn);
    stream.read("mPackgeEnd", mPackgeEnd);
    stream.read("mParityn",   mParity);
    stream.read("mStopBit",   mStopBit);
}

void CUartCfg::serialOut(CStream & stream)
{
    stream.write("mTxSrc",     (int)mTxSrc);
    stream.write("mRxSrc",     (int)mRxSrc);
    stream.write("mBaud",      mBaud);
    stream.write("mDataWidth", mDataWidth);
    stream.write("mLSB",       mLSB);
    stream.write("mNegative",  mNegative);
    stream.write("mPackgeEn",  mPackgeEn);
    stream.write("mPackgeEnd", mPackgeEnd);
    stream.write("mParityn",   mParity);
    stream.write("mStopBit",   mStopBit);
}

void CUartCfg::setSamplePerBaud(qint64 sample)
{
    if( mBaud > 0)
    {
        mSamplePerBaud = sample / mBaud;
    }
    else
    {
        mSamplePerBaud = 8;
    }

    if( mSamplePerBaud < 3)
    {
        mSamplePerBaud = 3;
    }
}

int CUartCfg::getSamplePerBaud()
{
    return mSamplePerBaud;
}

/*Uart Set/Get*/
DsoErr servDec::setUartTxSrc(Chan tx)
{
    mServUart.mTxSrc = tx;
    if(tx == chan_none || tx > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_TX_THRE, false);
    }
    else
    {
        getChThreshold1( tx );
        mUiAttr.setEnable(MSG_DECODE_TX_THRE, true);
        setuiChange( MSG_DECODE_TX_THRE );
    }
    if(tx == chan_none)
    {
        getChThreshold1( mServUart.mRxSrc );
        setuiChange( MSG_DECODE_RX_THRE );
        mUiAttr.setVisible(MSG_DECODE_RS232_RX, chan_none, false);
    }
    else
    {
        mUiAttr.setVisible(MSG_DECODE_RS232_RX, chan_none, true);
    }
    return ERR_NONE;
}

Chan servDec::getUartTxSrc()
{
    return mServUart.mTxSrc;
}

DsoErr servDec::setUartRxSrc(Chan rx)
{
    mServUart.mRxSrc = rx;
    if(rx == chan_none || rx > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_RX_THRE, false);
    }
    else
    {
        getChThreshold1( rx );
        mUiAttr.setEnable(MSG_DECODE_RX_THRE, true);
        setuiChange( MSG_DECODE_RX_THRE );
    }

    if(rx == chan_none)
    {
        getChThreshold1( mServUart.mTxSrc );
        setuiChange( MSG_DECODE_TX_THRE );
        mUiAttr.setVisible(MSG_DECODE_RS232_TX, chan_none, false);
    }
    else
    {        
        mUiAttr.setVisible(MSG_DECODE_RS232_TX, chan_none, true);        
    }

    return ERR_NONE;
}

Chan servDec::getUartRxSrc()
{
    return mServUart.mRxSrc;
}

DsoErr servDec::setUartTxThre(qint64 t)
{
    setChThreshold(mServUart.mTxSrc, t);
    return ERR_NONE;
}

qint64 servDec::getUartTxThre()
{
    Chan s = mServUart.mTxSrc;
    return getChThreshold(s);
}

DsoErr servDec::setUartRxThre(qint64 t)
{
    setChThreshold(mServUart.mRxSrc, t);
    return ERR_NONE;
}

qint64 servDec::getUartRxThre()
{
    Chan s = mServUart.mRxSrc;
    return getChThreshold(s);
}


DsoErr servDec::setUartBaudVal(int baud)
{    
    mServUart.mBaud = baud;
    return ERR_NONE;
}

int servDec::getUartBaudVal()
{
    setuiBase( 1, E_0 );
    setuiZVal( 9600 );
    setuiMaxVal(20000000);
    setuiMinVal(1);
    setuiPostStr( "bps" );
    return mServUart.mBaud;
}

DsoErr servDec::setUartNegative(bool negative)
{
    mServUart.mNegative = negative;
    return ERR_NONE;
}

bool servDec::getUartNegative()
{
    return mServUart.mNegative;
}


DsoErr servDec::setUartLsb(bool lsb)
{
    mServUart.mLSB = lsb;
    return ERR_NONE;
}

bool servDec::getUartLsb()
{
    return mServUart.mLSB;
}

DsoErr servDec::setUartDataWidth(int datawid)
{
    mServUart.mDataWidth = datawid;
    return ERR_NONE;
}

int servDec::getUartDataWidth()
{
    return mServUart.mDataWidth;
}

DsoErr servDec::setUartParity(uint parity)
{
    mServUart.mParity =  parity;
    return ERR_NONE;
}

uint servDec::getUartParity()
{
    return mServUart.mParity;
}

DsoErr servDec::setUartStopBit(uint b)
{
    mServUart.mStopBit = b;
    return ERR_NONE;
}

uint servDec::getUartStopBit()
{
    return mServUart.mStopBit;
}


DsoErr servDec::setUartPackgeEn(bool packgeen)
{
    mServUart.mPackgeEn = packgeen;
    return ERR_NONE;
}
DsoErr servDec::setUartPackgeEnd(uint packgeend)
{
    mServUart.mPackgeEnd = packgeend;
    return ERR_NONE;
}

uint servDec::getUartPackgeEnd()
{
    return mServUart.mPackgeEnd;
}

bool servDec::getUartPackgeEn()
{
    return mServUart.mPackgeEn;
}

