#pragma once

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;


#define PARITY_NONE     0
#define PARITY_ODD      1
#define PARITY_EVEN     2

#define DATA_WIDTH_5	5
#define DATA_WIDTH_6	6
#define DATA_WIDTH_7	7
#define DATA_WIDTH_8	8
#define DATA_WIDTH_9	9


#define UART_NEGATIVE	0
#define UART_POSITIVE	1

#define PACKAGE_DISEN   0
#define PACKAGE_EN      1

#define PACKAGEEND_NUL  0
#define PACKAGEEND_LF   10
#define PACKAGEEND_CR   13
#define PACKAGEEND_SP   32
#define PACKAGEEND_FFH  4
typedef enum
{
    Uart_DataWid_0,
    Uart_DataWid_1,
    Uart_DataWid_2,
    Uart_DataWid_3,
    Uart_DataWid_4,
    Uart_DataWid_5,
    Uart_DataWid_6,
    Uart_DataWid_7,
    Uart_DataWid_8,
    Uart_DataWid_9,
}UartDataWid;


struct CUartCfg
{
    void reset();
    void setSamplePerBaud(qint64 sample);
    int  getSamplePerBaud();
    void serialIn(CStream&);
    void serialOut(CStream&);

    Chan mTxSrc;
    Chan mRxSrc;

    int  mBaud;
    bool mNegative;		// 0 -- NEGATIVE
    bool mLSB;             // 0 -- LSB
    uint mSamplePerBaud;       // Sample times per bit= sample rate / baud rate
    int  mDataWidth;       // data width 5/6/7/8
    uint mParity;          // 0 -- None, 1 -- ODD, 2 -- EVEN
    uint mStopBit;       // 10/15/20
    bool mPackgeEn;        //1 -- Open
    uint mPackgeEnd;       // 0 -- NUL, 1 -- LF, 2 -- CR, 3 -- SP, 4 -- FFh
    float mBitErr;
                        // Post
    uint mStopPatt;		// stop bit pattern 0x03,0x01
    uint mStopBitWidth;	// stop bit 1/2

};

