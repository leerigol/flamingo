// DecUart.cpp: implementation of the CDecUart class.
//
//////////////////////////////////////////////////////////////////////
#include <QDebug>
#include "uart_fsm.h"

CUartIdle::CUartIdle()
{
}

CUartIdle::~CUartIdle()
{}

//! CUartIdle
void CUartIdle::onEnter( int /*bitVal*/ )
{
}

void CUartIdle::serialIn( int event, int bitVal )
{
    if( event == FALL)
    {
        if ( mCounter.getDur() >= ( mpUartCfg->getSamplePerBaud()/2) )//
        {
            //! enter into start
            toNext( bitVal );

            //! clock start samp
            mClock.start( mpUartCfg->getSamplePerBaud(),
                          mpUartCfg->getSamplePerBaud() * 1 / 2 );
        }
        mCounter.begin();
    }
    else if(event == RISE)
    {
        mCounter.begin();
    }
}

CUartStart::CUartStart()
{

}

CUartStart::~CUartStart()
{

}

//! CUartStart
void CUartStart::onEnter(int /*bitVal*/)
{
    mCounter.begin();
    mPosition = mCounter.now();
}

void CUartStart::serialIn( int event, int bitVal )
{
    if(event == SAMP ) //samp bit must be 0 when in start
    {
        if ( bitVal == 0 )
        {
            toNext( bitVal );
        }
    }
}

void CUartStart::onExit(int /*bitVal*/)
{}

CUartData::CUartData()
{}

CUartData::~CUartData()
{}

//! CUartData
void CUartData::onEnter(int /*bitVal*/)
{
    mCount = 0;
    mData = 0;
    mCounter.begin();
    mPosition = mCounter.now();
}

void CUartData::serialIn(int event,int bitVal)
{
    if( event == SAMP )
    {
        if(mpUartCfg->mLSB == lsb)
        {
            mData |= ( bitVal << mCount );
        }
        else
        {
            mData = (mData  << 1) | bitVal;
        }
        mCount++;
    }

    if ( (int)mCount == (mpUartCfg->mDataWidth))
    {
        toNext( bitVal );;
    }
}

void CUartData::onExit(int /*bitVal*/)
{
    //jump the holding area
    int hold_time = mpUartCfg->getSamplePerBaud()/2;
    mSpan = mCounter.getDur() + hold_time;
    //mCounter.count(hold_time);

    CDecPayloadData  *pData = new CDecPayloadData();
    Q_ASSERT( NULL != pData );
    pData->setPosSpan( mPosition, mSpan );

    pData->setPayload( mData, mCount );
    AddBlock(CDecPayloadData, pData);

    mpTable->append(new CCellTime(mPosition));
    if(mCurrDir == uart_tx )
    {
        mpTable->append(new CCellStr(MSG_DECODE_EVT_TX));
    }
    else
    {
        mpTable->append(new CCellStr(MSG_DECODE_EVT_RX));
    }
    mpTable->append(new CCellDW(mData,mCount));

    AddOneFrame();
}

CUartCheck::CUartCheck()
{
}

CUartCheck::~CUartCheck()
{
}

//! CUartCheck
void CUartCheck::onEnter(int /*bitVal*/)
{    
    mCounter.begin();
    mPosition = mCounter.now();
    mParity = 1;
}


//Parity=0 Success ;Other Fail
void CUartCheck::serialIn( int event, int bitVal )//added
{
    int  data = mData;

    if(mpUartCfg->mParity == PARITY_NONE)
    {
        toNext(bitVal);
        return;
    }
    if( event != SAMP )
    {
        return;
    }

    int bitCount = 0;
    for (int i = 0; i < mpUartCfg->mDataWidth; i++ )
    {
        if ( data & 0x01 )
        {
            bitCount++;
        }
        data >>= 1;
    }

    if ( bitVal == 0x01 )				// Chk BIT
    {
        bitCount++;
    }

    switch (mpUartCfg->mParity)
    {
        case PARITY_ODD:
            if ( bitCount & 0x01 )
            {
                mParity = 0;
            }
            else
            {
                mParity = 1;
            }
            break;

        case PARITY_EVEN:
            if ( bitCount & 0x01 )
            {
                mParity = 1;
            }
            else
            {
                mParity = 0;
            }
            break;

        default:
            break;
    }
    toNext( bitVal );
}

void CUartCheck::onExit(int /*bitVal*/)
{
    //! export parity
    if(mpUartCfg->mParity != PARITY_NONE)//even or odd
    {
        if(mParity!=0)//output parity err
        {
            int hold_time = mpUartCfg->getSamplePerBaud()/2;
            mSpan = mCounter.getDur() + hold_time;

            CDecPayloadString* pCmd = new  CDecPayloadString();
            Q_ASSERT( NULL != pCmd );
            pCmd->setPosSpan( mPosition, mSpan );

            pCmd->setPayload( ERR_COLOR, TEXT_PARITY);
            AddBlock(CDecPayloadString,pCmd);

            //mpTable->append(new CCellStr(MSG_DECODE_EVT_ERR));

            mUartError = true;
        }
    }
}

//! CUartStop

CUartStop::CUartStop()
{}

CUartStop::~CUartStop()
{}

void CUartStop::onEnter(int /*bitVal*/)
{
    mCount = 0;
    bError = false;

    mCounter.begin();
    mPosition = mCounter.now();
}

void CUartStop::serialIn( int event, int bitVal )
{
    if(event == SAMP )
    {
        mCount++;

        if( bitVal == 0 )
        {
            bError = true;
            toNext(bitVal);
            return;
        }

        if( mCount == 2 )
        {
            toNext( bitVal );
            return;
        }

        //1bit. to idle
        if(mpUartCfg->mStopBit == STOP_WIDTH_1)
        {
            toNext( bitVal );
        }
        else if(mpUartCfg->mStopBit == STOP_WIDTH_1_5 )//1.5bit
        {
            int p = mpUartCfg->getSamplePerBaud();
            mClock.start( p/2, p*3/4);
        }

    }
}

void CUartStop::onExit( int /*bit*/ )
{
    mUartError = mUartError | bError;
    if( bError )
    {
        int hold_time = mpUartCfg->getSamplePerBaud()/2;
        mSpan = mCounter.getDur() + hold_time;

        CDecPayloadString* pCmd = new  CDecPayloadString();
        Q_ASSERT( NULL != pCmd );
        pCmd->setPosSpan( mPosition, mSpan );

        pCmd->setPayload( ERR_COLOR, TEXT_CH9);
        AddBlock(CDecPayloadString,pCmd);
    }

    if( mUartError )
    {
        mpTable->append(new CCellStr(MSG_DECODE_EVT_Y));//Err
    }
    else
    {
        mpTable->append(new CCellStr(MSG_DECODE_EVT_N));//Err
    }
}
