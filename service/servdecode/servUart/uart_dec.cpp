#include "uart_fsm.h"
#include "../servdecode.h"

static CUartIdle   fsm_uart_idle;
static CUartStart  fsm_uart_start;
static CUartData   fsm_uart_data;
static CUartCheck  fsm_uart_check;
static CUartStop   fsm_uart_stop;

CUartCfg* CUartDecoder::mpUartCfg;
bool      CUartDecoder::mUartError;
int       CUartDecoder::mCurrDir;

CUartDecoder::CUartDecoder()
{
     mpNow = &fsm_uart_idle;
     fsm_uart_idle.setNextStat( &fsm_uart_start );
     fsm_uart_start.setNextStat( &fsm_uart_data );
     fsm_uart_data.setNextStat( &fsm_uart_check );
     fsm_uart_check.setNextStat( &fsm_uart_stop );
     fsm_uart_stop.setNextStat( &fsm_uart_idle);     
}

CUartDecoder::~CUartDecoder()
{
}


void CUartDecoder::rst()
{
   mpNow = &fsm_uart_idle;
   mCounter.rst();
   mClock.stop();

   mUartError = false;
   mpUartCfg = &mpCurrServ->mServUart;

   //with check?
   if( mpUartCfg->mParity == PARITY_NONE )
   {
        fsm_uart_data.setNextStat( &fsm_uart_stop );
   }
   else
   {
       fsm_uart_data.setNextStat( &fsm_uart_check );
   }
}



//! rst to idle
bool CUartDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{    
    bool ret = false;

    mpTable = new CTable();

    mCurrDir = uart_tx;
    if( decode(mpUartCfg->mTxSrc) )
    {
        if( mpUartCfg->mPackgeEn )
        {
            doPackage();
        }

        CDecLine* line = new CDecLine();
        //if( mpCurrServ->getLabelOn())
        {
            QString label = "RS232-TX";
            line->setLabel(label);
        }

        mLineView.makeLine(*line,mBlocklist);
        bus->append( line );

        if( zoomBus )
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        ret = true;
    }

    rst();
    mCurrDir = uart_rx;
    if( decode(mpUartCfg->mRxSrc) )
    {        
        CDecLine* line = new CDecLine();

        //if( mpCurrServ->getLabelOn())
        {
            QString label = "RS232-RX";
            line->setLabel(label);
        }

        mLineView.makeLine(*line, mBlocklist);
        bus->append( line );

        if( zoomBus )
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        ret = true;
    }

//    merge
    setEventTable( mpTable );
    return ret;
}

bool CUartDecoder::decode(Chan src)
{
    uint u32Data;
    int last_dat, next_dat;

    uchar* pStream = mpCurrServ->getChanData( src );

    if( !pStream )
    {
        return false;
    }

    last_dat = ( *pStream & 0x80 ) ? 1 : 0;


    bool bPolarity = mpUartCfg->mNegative;
    if(bPolarity == UART_NEGATIVE)//added
    {
        last_dat = !last_dat;
    }

    clearBlock();

    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Data, pStream + i, 4 );
        if(bPolarity == UART_NEGATIVE)//added
        {
            u32Data = ~u32Data;
        }

        //! no change, 0xffffffff or 0x00000000
        if ( u32Data == m32IdleMask[last_dat] )
        {
            if ( mClock.tryTick(32) > 0 )
            {
            }
            else
            {
                i += 4;
                mCounter.count( 32 );
                mClock.tick(32);
                continue;
            }
        }

        //! no change in u8, 0xff or 0x00
        if ((u32Data & 0x0ff) == m8IdleMask[last_dat])
        {
            if ( mClock.tryTick(8) > 0 )
            {
            }
            else
            {
                i += 1;
                mCounter.count(8);
                mClock.tick(8);
                continue;
            }
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_dat = u32Data & m32Masks[j] ? 1 : 0;
            //! rise
            if ( last_dat < next_dat)
            {
                now()->serialIn( RISE, next_dat );                
                mClock.sync();
            }
            else if ( last_dat > next_dat )//fall
            {
                now()->serialIn( FALL, next_dat );
                mClock.sync();
            }
            last_dat = next_dat;

            //! timeout
            if ( mClock.tick() > 0 )
            {
                now()->serialIn( SAMP, next_dat );
            }

            mCounter.count();
        }
        i += 1;
    }
    return true;
}


int CUartDecoder::setEventTable(CTable *table)
{
    //! head
    QList<quint32> lstHead;
    //! layout
    QList<quint32> lstLayout;


    lstHead.append( MSG_DECODE_EVT_TIME );       
    lstHead.append( MSG_DECODE_EVT_UART_DIR );
    lstHead.append( MSG_DECODE_EVT_DATA );
    lstHead.append( MSG_DECODE_EVT_ERR );

    lstLayout.append( 50 );//id
    lstLayout.append( 100 );//time
    lstLayout.append( 80 );//dir
    lstLayout.append( 280 );//data


    table->setHead( lstHead );
    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_RS232);
    table->setTitle( pTitle );
    return ERR_NONE;
}

void CUartDecoder::doPackage()
{
    int size = mBlocklist.size();
    if( size > 0 )
    {
        QList<CDecBlock*> dataList;
        CDecPayload* payload;
        CDecPayloadPackage* package;

        bool packaged = true;
        for(int j=0; j<size; j++)
        {
            CDecBlock* block = mBlocklist.at(j);
            if( !block->isIcon() )
            {
                payload = (CDecPayload*)block;

                if(payload->GetPayloadData() !=  mpUartCfg->mPackgeEnd )
                {
                    if( packaged )
                    {
                        package = new CDecPayloadPackage();
                        package->busBlock_SetPos( payload->mPos );
                    }
                    package->setPayload(payload->GetPayloadData(),
                                        payload->getPayloadWidth(),
                                        payload->getPayloadColor(),
                                        payload->getPayloadCation());

                    package->mSpan = payload->mPos + payload->mSpan - package->mPos;
                    //qDebug() << payload->GetPayloadData();
                    packaged = false;
                }
                else
                {
                    if ( !packaged )
                    {
                        dataList.append(package);
                    }
                    package = new CDecPayloadPackage();
                    package->setPayload(payload->GetPayloadData(),
                                        payload->getPayloadWidth(),
                                        payload->getPayloadColor(),
                                        payload->getPayloadCation());
                    package->busBlock_SetPos( payload->mPos );
                    package->busBlock_SetSpan( payload->mSpan);
                    dataList.append(package);

                    packaged = true;
                }
            }
            delete block;
        }
        clearBlock();
        if( !packaged )
        {
            dataList.append(package);
        }

        mBlocklist = dataList;
    }

}
