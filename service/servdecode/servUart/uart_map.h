#ifndef RS232_MAP
#define RS232_MAP

#include "../servdecode/servdecode.h"
on_set_int_int  (MSG_DECODE_RS232_TX,       &servDec::setUartTxSrc),
on_get_int      (MSG_DECODE_RS232_TX,       &servDec::getUartTxSrc),

on_set_int_int  (MSG_DECODE_RS232_RX,       &servDec::setUartRxSrc),
on_get_int      (MSG_DECODE_RS232_RX,       &servDec::getUartRxSrc),

on_set_int_ll   (MSG_DECODE_TX_THRE,        &servDec::setUartTxThre),
on_get_ll       (MSG_DECODE_TX_THRE,        &servDec::getUartTxThre),

on_set_int_ll   (MSG_DECODE_RX_THRE,        &servDec::setUartRxThre),
on_get_ll       (MSG_DECODE_RX_THRE,        &servDec::getUartRxThre),

on_set_int_int  (MSG_DECODE_RS232_BAUD,     &servDec::setUartBaudVal),
on_get_int      (MSG_DECODE_RS232_BAUD,     &servDec::getUartBaudVal),

on_set_int_bool (MSG_DECODE_RS232_POL,      &servDec::setUartNegative),
on_get_bool     (MSG_DECODE_RS232_POL,      &servDec::getUartNegative),

on_set_int_bool (MSG_DECODE_RS232_ENDIAN,   &servDec::setUartLsb),
on_get_bool     (MSG_DECODE_RS232_ENDIAN,   &servDec::getUartLsb),

on_set_int_int  (MSG_DECODE_RS232_WIDTH,    &servDec::setUartDataWidth),
on_get_int      (MSG_DECODE_RS232_WIDTH,    &servDec::getUartDataWidth),

on_set_int_int  (MSG_DECODE_RS232_STOP,     &servDec::setUartStopBit),
on_get_int      (MSG_DECODE_RS232_STOP,     &servDec::getUartStopBit),

on_set_int_int  (MSG_DECODE_RS232_PARITY,   &servDec::setUartParity),
on_get_int      (MSG_DECODE_RS232_PARITY,   &servDec::getUartParity),

on_set_int_bool (MSG_DECODE_RS232_PACKEN,   &servDec::setUartPackgeEn),
on_get_bool     (MSG_DECODE_RS232_PACKEN,   &servDec::getUartPackgeEn),

on_set_int_int  (MSG_DECODE_RS232_PACKEND,  &servDec::setUartPackgeEnd),
on_get_int      (MSG_DECODE_RS232_PACKEND,  &servDec::getUartPackgeEnd),


#endif // RS232_MAP

