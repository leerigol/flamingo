// DecUart.h: interface for the CDecUart class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(DEC_UART)
#define DEC_UART


#include "../decTool/DecState.h"
#include "../decoder.h"
#include "uart_cfg.h"

enum
{
    uart_tx = 0xabcd,
    uart_rx
};
class CUartDecoder : public CDecoder,public CDecState
{
public:
     CUartDecoder();
    ~CUartDecoder();

public:
    void rst();
    bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
    int  setEventTable(CTable* table);

    void doPackage();
public:
    static CUartCfg* mpUartCfg;
    static bool      mUartError;
    static int       mCurrDir;
private:
    bool decode(Chan);
};

class CUartIdle : public CUartDecoder
{
public:
     CUartIdle();
    ~CUartIdle();

public:
    void onEnter( int bitVal);
    void serialIn( int event, int bitVal );
};

class CUartStart : public CUartDecoder
{
public:
     CUartStart();
    ~CUartStart();

public:
    void onEnter( int bitVal );
    void onExit(int bitVal);
    void serialIn( int event, int bitVal );
};

class CUartData: public CUartDecoder
{
public:
     CUartData();
    ~CUartData();

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};


class CUartCheck : public CUartDecoder
{
public:
     CUartCheck();
    ~CUartCheck();

public:
    void onEnter( int bitVal );//added
    void onExit( int bitVal );//added3x
    void serialIn( int event, int bitVal );
private:
    uint mParity;
};

class CUartStop : public CUartDecoder
{
public:
     CUartStop();
    ~CUartStop();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit( int bitVal );
private:
    bool bError;
};

#endif // !defined(AFX_DECUART_H__554D854D_13AA_4B8F_BDFA_670AC9DB619C__INCLUDED_)
