#ifndef CCELLSTR_H
#define CCELLSTR_H
#include "ctable.h"
//！事件表中显示字符串的数据类型
class CCellStr : public CCellDW
{
public:
    CCellStr(quint32 strId);

public:
    virtual int format( CTableContext *pContext,
                          QString &outStr );
    virtual int getData( quint32 &dw );
    virtual bool getIsData();


};

//！事件表中显示时间一列数据类型
class CCellTime : public CCellDW
{
public:
    CCellTime( quint32 time);
public:
    virtual int format( CTableContext *pContext,
                          QString &outStr );
    virtual int getData( quint32 &dw );
    virtual bool getIsData();
    static  double mTimeStart;
    static  double mTimeInc;

};
//！事件表中显示数据之外的十六进制数据，如CRC等等
class CCellInfo : public CCellDW
{
public:
    CCellInfo( quint32 dat, int wid = 32);
public:
    virtual int format( CTableContext *pContext,
                          QString &outStr );
    virtual int getData( quint32 &dw );
    virtual bool getIsData();

};

#endif // CCELLSTR_H
