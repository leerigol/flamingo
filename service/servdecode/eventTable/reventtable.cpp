#include "reventtable.h"
#include "../../include/dsodbg.h"
REventTable_Style REventTable::_style;

REventTable_Style::REventTable_Style()
{
    mHeaderY = 20;
    mHeaderHeight = 15;
    mCellHeight = 15;

    mViewRow = 20;
    mScrollWidth = 10;

    mIdWidth = 20;

    mHeader = 10;
    mFooter = 20;
}

void REventTable_Style::applyLayout( CCellList *pLayout )
{
    mColLayout.clear();

    CCell *pCell;
    CCellPayload *pData;
    quint32 dw, offs;

    pCell = pLayout->head();
    offs = mIdWidth;
    mSheetWidth = mIdWidth;
    while( pCell != NULL )
    {
        pData = pCell->data();
        Q_ASSERT( NULL != pData );

        if ( pData->getData(dw ) != 0 )
        { Q_ASSERT( false ); }

        mColLayout.append( (int)(offs) );
        offs += (int)dw;

        mSheetWidth += (int)dw;

        pCell = pCell->next();
    }
}

REventTable::REventTable(QWidget *parent) :
                   QWidget(parent)
{
    m_pTable = NULL;

    m_pVScrollBar = R_NEW( QScrollBar( this ) );

    setWindowFlags( Qt::FramelessWindowHint );

    connect( m_pVScrollBar, SIGNAL(valueChanged(int)),
             this, SLOT( on_vscroll_value_changed(int)) );
m_pVScrollBar->hide();
    setVisible( true );

}

void REventTable::paintEvent( QPaintEvent */*event*/ )
{
    if ( NULL == m_pTable ) return;

    QPainter painter(this);

    paintBg( painter );

    QString str;

    //! header
    int i;
    CCellList *pCellList;
    CCell *pCell;
    pCellList = m_pTable->getHead();

    pCell = pCellList->head();
    i = 0;
    while( pCell != NULL )
    {
        pCell->format( m_pTable->getContext(),
                       str );

        painter.drawText( _style.mColLayout[i],
                          _style.mHeaderY,
                          str );

        pCell = pCell->next();
        i++;
    }

    //! cells
    pCellList = m_pTable->getSheet();
    pCell = pCellList->head();

    //! jump to next
    pCell = pCell->next( m_pVScrollBar->value() * m_pTable->column() );

    i = 0;
    int y = _style.mHeaderY + _style.mHeaderHeight;
    int seq = m_pVScrollBar->value() + 1;
    int rowCount = 0;
    while ( pCell != NULL && rowCount < _style.mViewRow )
    {
        //! id col
        if ( i == 0 )
        {
            str = QString("%1").arg(seq);
            painter.drawText( 0, y, str );
        }

        //! each cell
        pCell->format( m_pTable->getContext(),
                       str );
        painter.drawText( _style.mColLayout[i],
                          y ,
                          str );

        pCell = pCell->next();
        i++;

        //! new row
        if ( i >= m_pTable->column() )
        {
            i = 0;
            y += _style.mCellHeight;
            seq++;
            rowCount++;
        }
    }
}

void REventTable::resizeEvent( QResizeEvent */*event*/ )
{
    QRect selfRect;

    selfRect = rect();

    m_pVScrollBar->setGeometry( selfRect.width() - _style.mScrollWidth,
                                _style.mHeaderHeight,
                                _style.mScrollWidth,
                                selfRect.height() - _style.mHeaderHeight );
}
QSize REventTable::sizeHint() const
{
    return QSize( 500, 400 );
}
void REventTable::paintBg( QPainter &painter )
{
    QRect selfRect = rect();

    //! header line
    painter.drawLine( 0,
                      _style.mHeaderY,
                      selfRect.width(),
                      _style.mHeaderY);

    //! cell vline
    int i, y;
    y = _style.mHeaderY + _style.mCellHeight;
    for ( i = 0; i < _style.mViewRow -1; i++ )
    {
        painter.drawLine( 0,
                          y,
                          selfRect.width(),
                          y );

        y += _style.mCellHeight;
    }

    //! id line
    painter.drawLine( _style.mIdWidth, 0,
                      _style.mIdWidth, selfRect.height() );

    //! cell hline
    for ( i = 0; i < m_pTable->column() - 1; i++ )
    {
        painter.drawLine( _style.mColLayout[i+1]-1,
                          0,
                          _style.mColLayout[i+1]-1,
                          selfRect.height() );
    }

}

void REventTable::on_vscroll_value_changed( int /*val*/ )
{
    update();
}

void REventTable::attachTable( CTable *pTable )
{
    Q_ASSERT( NULL != pTable );

    m_pTable = pTable;

    _style.applyLayout( pTable->getLayout() );

    int rows, scrollMax;
    Q_ASSERT( m_pTable->column() > 0 );
    rows = m_pTable->getSheet()->size() / m_pTable->column();

    scrollMax = rows - _style.mViewRow;
    if ( scrollMax < 0 )
    {
        m_pVScrollBar->setRange( 0, 0 );
    }
    else
    {
        m_pVScrollBar->setRange( 0, scrollMax );
    }

    rows = qMin( rows, _style.mViewRow );

    m_pVScrollBar->setValue( 0 );

    resize( _style.mSheetWidth,
            _style.mHeader +
            _style.mHeaderHeight +
            _style.mCellHeight * rows +
            _style.mFooter );
}



