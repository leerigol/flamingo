#include "ccellstr.h"

#include "../../service/servdso/sysdso.h"
#include "../../service/servhori/servhori.h"
#include "../../service/service_name.h"

CCellStr::CCellStr(quint32 strId)
{
    mPad = strId;
    mWidth = 1;
    isTime = false;
}

int CCellStr::format( CTableContext */*pContext*/,
                          QString &outStr )
{
    Q_ASSERT( mWidth == 1 );

    outStr = sysGetString( mPad );
//outStr = QString("%1").arg( mPad );
//    qDebug()<<"CCellStr "<<outStr;
    if(mPad == 0)
    {
        outStr ="";
    }
    if(mPad == 1)
    {
        outStr = " ";
    }
    return 0;
}
int CCellStr::getData( quint32 &/*dw*/ )
{
    return 1;
}
bool CCellStr::getIsData()
{
    return false;
}

double CCellTime::mTimeInc;
double CCellTime::mTimeStart;
CCellTime::CCellTime( quint32 time )
{
    mPad = time;
    mWidth = 1;
    isTime = true;
}

int CCellTime::format( CTableContext */*pContext*/,
                          QString &outStr )
{

    double time_pos = mPad;
    time_pos = time_pos * mTimeInc + mTimeStart;
    gui_fmt::CGuiFormatter::format( outStr, time_pos, fmt_def, Unit_s );

    return 0;
}
int CCellTime::getData( quint32 &/*dw*/ )
{
    return 1;
}
bool CCellTime::getIsData()
{
    return false;
}

CCellInfo::CCellInfo( quint32 dat, int wid )
{
    mPad = dat;
    mWidth = wid;
    isTime = false;
}

int CCellInfo::format( CTableContext */*pContext*/,
                          QString &outStr )
{
    //! formater
    CDecodeFormat *pFmt =
            CFormatFactory::createFormater( DsoType::F_HEX );

    Q_ASSERT( pFmt != NULL );
    int streamLen;
    QChar streams[ 32 ];
    streamLen = pFmt->format( mPad, (unsigned char)mWidth, streams );

    outStr.append(streams,streamLen);

    return 0;
}

int CCellInfo::getData( quint32 &/*dw*/ )
{
    return 1;
}
bool CCellInfo::getIsData()
{
    return false;
}
