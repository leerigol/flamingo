#include "ctable.h"
#include "ccellstr.h"

void *CTableHeap::_pMem = NULL;
quint32 CTableHeap::_now = 0;
quint32 CTableHeap::_size = 0;

CHeap::CHeap( void *pMem, size_t size )
{
    Q_ASSERT( NULL != pMem && size != 0 );

    m_pMem = pMem;
    mSize = size;
}

void *CHeap::mem()
{ return m_pMem; }
size_t CHeap::size()
{ return mSize; }

CHeapMgr::CHeapMgr()
{}
CHeapMgr::~CHeapMgr()
{
    foreach(CHeap * pHeap, mHeaps )
    {
        Q_ASSERT( NULL != pHeap );

        delete pHeap;
    }
}
void CHeapMgr::attachHeap( void *pMem, size_t size )
{
    CHeap *pHeap = new CHeap( pMem, size ) ;
    Q_ASSERT( NULL != pHeap );

    mHeaps.append( pHeap );
}

CHeap *CHeapMgr::getHeap( int id )
{
    Q_ASSERT( id >=0 && id < mHeaps.size() );

    return mHeaps[ id ];
}

void CHeapMgr::clearHeap()
{
    mHeaps.clear();
}

int CHeapMgr::getHeapCount()
{ return mHeaps.size(); }

void CTableHeap::setHeap( void *pMem, size_t size )
{
    Q_ASSERT( pMem != NULL && size > 0 );

    CTableHeap::_pMem = pMem;
    CTableHeap::_now = 0;
    CTableHeap::_size = size;
}

void CTableHeap::rstHeap()
{
    CTableHeap::_now = 0;
}

CTableObj::CTableObj()
{}

void * CTableObj::operator new ( size_t n )
{
    int alignSize = ( ( n + 7 ) / 8 ) * 8;

    if ( ( CTableHeap::_now + alignSize ) > CTableHeap::_size )
    {
        qDebug() << "now/all=" << CTableHeap::_now << "/" << CTableHeap::_size << n;
        return NULL;
    }

    Q_ASSERT( CTableHeap::_pMem != NULL );

    quint8 *pBase = (quint8*)CTableHeap::_pMem;

    pBase += CTableHeap::_now;

    CTableHeap::_now += alignSize + 1;

    return pBase;
}

CCellStyle::CCellStyle()
{
    mBgColor = 0;
    mFgColor = 0;
}

void CCellStyle::setColor( quint8 bg, quint8 fg )
{
    mBgColor = bg;
    mFgColor = fg;
}
quint8 CCellStyle::getBgColor() const
{ return mBgColor; }
quint8 CCellStyle::getFgColor() const
{ return mFgColor; }

CCellPayload::CCellPayload()
{
    m_pNext = NULL;
    isTime = false;
    mCount = 1;
}

void CCellPayload::append( CCellPayload *pNxt )
{
    Q_ASSERT( NULL != pNxt );

    CCellPayload *pTail;

    //! jump to end
    pTail = next( mCount - 1);

    pTail->m_pNext = pNxt;
    mCount += 1;
}

int CCellPayload::getData( quint32 &dw )
{
    dw = 0;
    return 0;
}
int CCellPayload::getData()
{
    return 0;
}
bool CCellPayload::getIsTime()
{
    return isTime;
}
bool CCellPayload::getIsData()
{
    return false;
}

int CCellPayload::size()
{
    return mCount;
}
CCellPayload *CCellPayload::next( int n )
{
    CCellPayload *pNxt;

    if ( n < 1 ) return this;

    pNxt = m_pNext;
    while ( n > 1 )
    {
        Q_ASSERT( NULL != pNxt );
        pNxt = pNxt->m_pNext;
        n--;
    }

    return pNxt;
}

CCellDW::CCellDW()
{
    mPad = 0;
    mWidth = 0;
    isTime = false;
    mCount = 1;
}

CCellDW::CCellDW( quint32 dat, int wid )
{
    mPad = dat;
    mWidth = wid;
    mFmt = DsoType::F_HEX;
    isTime = false;
}

int CCellDW::format( CTableContext */*pContext*/,
                       QString &outStr )
{
    //! formater
    CDecodeFormat *pFmt = CFormatFactory::createFormater( mFmt );
    Q_ASSERT( pFmt != NULL );
    int streamLen;
    QChar streams[ 32 ];
    streamLen = pFmt->format( mPad, (unsigned char)mWidth, streams );

    outStr.append(streams,streamLen);
    return 0;
}

void CCellDW::setData( quint32 dat, int wid )
{
    mPad = dat;
    mWidth = wid;
}

int CCellDW::getData( quint32 &dw )
{
    dw = mPad;
    return 0;
}
int CCellDW::getData()
{
    return mPad;
}

bool CCellDW::getIsTime( )
{
    return isTime;
}
bool CCellDW::getIsData()
{
    return true;
}

CCellYN::CCellYN( bool b )
{
    mPad = (int)b;
    mWidth = 1;
    isTime = false;
}

int CCellYN::format( CTableContext */*pContext*/,
                          QString &outStr )
{
    Q_ASSERT( mWidth == 1 );

    if ( mPad == 1 )
    {
        outStr = QLatin1Literal("Y");
    }
    else
    {
        outStr = QLatin1Literal("N");
    }

    return 0;
}
int CCellYN::getData( quint32 &/*dw*/ )
{
    return 1;
}
bool CCellYN::getIsData()
{
    return false;
}

CCell::CCell()
{
    m_pNext = NULL;
    m_pData = NULL;
}

int CCell::format( CTableContext *pContext,
            QString &outStr )
{
    CCellPayload *pData;
    int err;

    pData = m_pData;
    QString strSection;
    outStr.clear();
    while ( pData != NULL )
    {
        strSection.clear();
        err = pData->format( pContext, strSection );
        if ( err != 0 ) return err;

        outStr += strSection;

        pData = pData->next();
    }
    return 0;
}

void CCell::append( CCellPayload *pData )
{
    Q_ASSERT( NULL != pData );

    if ( NULL == m_pData )
    {
        m_pData = pData;
    }
    else
    {
        m_pData->append( pData );
    }
}
CCell *CCell::next( int n )
{
    CCell *pNxt;

    if ( n < 1 ) return this;

    pNxt = m_pNext;
    while ( n > 1 )
    {
        Q_ASSERT( NULL != pNxt );

        pNxt = pNxt->m_pNext;

        n--;
    }

    return pNxt;
}
CCellPayload *CCell::data()
{
    return m_pData;
}

void CCell::setFmt(DsoType::DecodeFormat fmt)
{
    mFmt = fmt;
}

CCellList::CCellList()
{
    m_pHead = NULL;
    m_pTail = NULL;
    mCount = 0;
}

void CCellList::append( CCell *pCell )
{
    Q_ASSERT( NULL != pCell );

    if ( m_pHead == NULL )
    {
        m_pHead = pCell;
        m_pTail = pCell;
        pCell->m_pNext = NULL;

        mCount = 1;
    }
    else
    {
        m_pTail->m_pNext = pCell;
        m_pTail=pCell;
        pCell->m_pNext = NULL;

        mCount++;
    }
}

void CCellList::append( CCellPayload *pData )
{
    Q_ASSERT( NULL != pData );

    CCell *pCell;

    pCell = new CCell();
    Q_ASSERT( NULL != pCell );

    pCell->append( pData );

    append( pCell );
}

int CCellList::size()
{
    return mCount;
}

CCell *CCellList::head()
{
    return m_pHead;
}

CTable::CTable()
{
    mColumns = 0;

    m_pTitle = NULL;
    m_pContext = NULL;
}
int CTable::column()
{
    return mColumns;
}
void CTable::setHead( QList<quint32> &headers )
{
    Q_ASSERT( headers.size() > 0 );
    if ( mColumns != 0 )
    {
        Q_ASSERT(mColumns==headers.size());
    }
    else
    {
    }

    quint32 headId;
    CCellStr *pData;
    foreach( headId, headers )
    {
        pData = new CCellStr(headId);//new CCellDW( );
        Q_ASSERT( NULL != pData );
        mHead.append( pData );
    }
}

CCellList *CTable::getHead()
{
    return &mHead;
}

void CTable::setLayout( QList<quint32> &layouts )
{
    Q_ASSERT( layouts.size() > 0 );

    if ( mColumns != 0 )
    { Q_ASSERT(mColumns==layouts.size()); }
    else
    {
        mColumns = layouts.size();
    }

    quint32 layoutWidth;

    CCellDW *pData;
    foreach( layoutWidth, layouts )
    {
        pData = new CCellDW();
        Q_ASSERT( NULL != pData );

        pData->setData( layoutWidth );

        mLayout.append( pData );
    }
}
CCellList *CTable::getLayout()
{
    return &mLayout;
}
void CTable::setTitle( CCellPayload *pTitle )
{
    Q_ASSERT( NULL != pTitle );
    m_pTitle = pTitle;
}
CCellPayload* CTable::getTitle()
{
    return m_pTitle;
}

void CTable::setContext( CTableContext *pContext )
{
    Q_ASSERT( NULL != pContext );
    m_pContext = pContext;
}
CTableContext *CTable::getContext()
{
    return m_pContext;
}
void CCellPayload::setfmt(DsoType::DecodeFormat fmt)
{
    mFmt = fmt;
}

void CTable::append( CCell *pCell )
{
    Q_ASSERT( NULL != pCell );

    pCell->setFmt(mFmt);
    mSheet.append( pCell );
}

void CTable::append( CCellPayload *pData )
{
    /***********************************************
     *!!!!!! Limit the count of the Event Table!!!!!!
    ************************************************/
    if( mSheet.size() < LIMIT_RESULT_SIZE )
    {
        Q_ASSERT( NULL != pData );

        pData->setfmt(mFmt);
        mSheet.append( pData );
    }
}
CCellList *CTable::getSheet()
{
    return &mSheet;
}

void CTable::setFmt(DsoType::DecodeFormat fmt)
{
    mFmt= fmt;
}

void CTable::show()
{
    QString str;
    m_pTitle->format( m_pContext, str );

    CCell *pCell;

    pCell = mHead.m_pHead;
    while( pCell != NULL )
    {
        Q_ASSERT( NULL != pCell->m_pData );
        pCell->m_pData->format( m_pContext, str );
        pCell = pCell->m_pNext;
    }

    pCell = mLayout.m_pHead;
    while( pCell != NULL )
    {
        Q_ASSERT( NULL != pCell->m_pData );
        pCell->m_pData->format( m_pContext, str );
        pCell = pCell->m_pNext;
    }

    pCell = mSheet.m_pHead;
    while( pCell != NULL )
    {
        Q_ASSERT( NULL != pCell->m_pData );
        pCell->m_pData->format( m_pContext, str );
        pCell = pCell->m_pNext;
    }
}

void CTable::test( void *pMem, int size )
{
    CTableHeap::setHeap( pMem, size );
//    CTable *pTable = new CTable();

    //! head
    QList<quint32> lstHead;
    lstHead.append( 1 );
    lstHead.append( 2 );
    lstHead.append( 3 );

    setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 100 );
    lstLayout.append( 100 );
    lstLayout.append( 100 );

    setLayout( lstLayout );

    //! title
    CCellDW *pTitle = new CCellDW();

    pTitle->setColor( 100, 200 );
    pTitle->setData( 110 );
    setTitle( pTitle );

    //! cells
    for ( int i = 0; i < 24 * 3; i++ )
    {
        append( new CCellDW(1001 + i ) );
    }

    //! show
    show();
}

void CTable::randTable()
{
    //! head
    QList<quint32> lstHead;
    lstHead.append( 14278 );
    lstHead.append( 14279 );
    lstHead.append( 14280 );

    setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 100 );
    lstLayout.append( 100 );
    lstLayout.append( 100 );

    setLayout( lstLayout );

    //! title
    CCellDW *pTitle = new CCellDW();

    pTitle->setColor( 100, 200 );
    pTitle->setData( 110 );
    setTitle( pTitle );

    //! cells
    for ( int i = 0; i < 24 * 3; i++ )
    {
        append( new CCellDW( rand() ) );
    }
}
