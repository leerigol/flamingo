#ifndef REVENTTABLE_H
#define REVENTTABLE_H

#include <QtWidgets>

#include "ctable.h"

class REventTable_Style
{
public:
    REventTable_Style();

    void applyLayout( CCellList *pLayout );

public:
    int mHeaderY;
    int mHeader, mFooter;
    int mHeaderHeight;
    int mCellHeight;

    int mViewRow;
    int mScrollWidth;
    int mIdWidth;
    QList<int> mColLayout;

    //! cache
    int mSheetWidth;
};

class REventTable : public QWidget
{
    Q_OBJECT
private:
    static REventTable_Style _style;
public:
    explicit REventTable(QWidget *parent = 0);

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual void resizeEvent( QResizeEvent *event );
    virtual QSize sizeHint() const;

protected:
    void paintBg( QPainter &painter );

signals:

protected slots:
    void on_vscroll_value_changed( int val );
public:
    void attachTable( CTable *pTable );

private:
    CTable *m_pTable;

    QScrollBar *m_pVScrollBar;
};

#endif // REVENTTABLEWIDGET_H
