#ifndef CTABLE_H
#define CTABLE_H

#include <QtCore>
#include "../../service/servdecode/servBlock/DecBlock.h"
#include "../../service/servdecode/servBlock/DecFormat.h"
#include "../../include/dsotype.h"
using namespace DsoType;

class CTableObj;

class CHeap
{
public:
    CHeap( void *pMem, size_t size );

    void *mem();
    size_t size();

private:
    void *m_pMem;
    size_t mSize;
};

class CHeapMgr
{
public:
    CHeapMgr();
    ~CHeapMgr();
    void attachHeap( void *pMem, size_t size );
    CHeap *getHeap( int id );

    void clearHeap();

    int getHeapCount();
private:
    QList< CHeap *> mHeaps;
};

class CTableHeap
{
private:
    static void *_pMem;
    static quint32 _now;
    static size_t _size;

public:
    static void setHeap( void *pMem, size_t size );
    static void rstHeap();

friend class CTableObj;
};

class CTableObj
{
protected:
    CTableObj();

public:
    void * operator new ( size_t n );
};

class CTableContext : public CTableObj
{
};

class CCellStyle : public CTableObj
{
public:
    CCellStyle();

    void setColor( quint8 bg, quint8 fg );
    quint8 getBgColor() const;
    quint8 getFgColor() const;
private:
    quint8 mBgColor;
    quint8 mFgColor;
};

class CCellPayload : public CCellStyle
{
protected:
    CCellPayload();

public:
    virtual int format( CTableContext *pContext,
                        QString &outStr )=0;

    void append( CCellPayload *pNxt );

    virtual int getData( quint32 &dw );
    virtual int getData();
    virtual bool getIsData();
    int size();

    CCellPayload *next( int n = 1 );

    void setfmt(DsoType::DecodeFormat fmt);
protected:
    CCellPayload *m_pNext;

    int mCount;
    DsoType::DecodeFormat mFmt;
    bool isTime;
    bool mIsData;
    virtual bool getIsTime();

};

class CCellDW : public CCellPayload
{
public:
    CCellDW();
    CCellDW( quint32 dat, int wid = 32 );
public:
    virtual int format( CTableContext *pContext,
                          QString &outStr );

    void setData( quint32 dat, int wid = 32 );
    virtual int getData( quint32 &dw );
    virtual int getData();
    virtual bool getIsTime();
    virtual bool getIsData();

protected:
    quint32 mPad;
    int mWidth;


};

class CCellYN : public CCellDW
{
public:
    CCellYN( bool b );
public:
    virtual int format( CTableContext *pContext,
                          QString &outStr );
    virtual int getData( quint32 &dw );
    virtual bool getIsData();
};

class CCellList;
class CTable;
class CCell : public CTableObj
{
public:
    CCell();
public:
    int format( CTableContext *pContext,
                QString &outStr );

    void append( CCellPayload *pData );

    CCell *next( int n = 1 );
    CCellPayload *data();
    void setFmt(DsoType::DecodeFormat fmt);
    bool getIsData();

private:
    CCell *m_pNext;
    CCellPayload *m_pData;
    DsoType::DecodeFormat mFmt;

friend class CCellList;
friend class CTable;
};

class CCellList : public CTableObj
{
public:
    CCellList();

    void append( CCell *pCell );
    void append( CCellPayload *pData );
    int size();
    CCell* head();

private:
    CCell *m_pHead;
    CCell *m_pTail;

    int mCount;
friend class CTable;
};

class CTable : public CTableObj
{
public:
    CTable();

public:
    int column();

    void setHead( QList<quint32> &headers );
    CCellList *getHead();

    void setLayout( QList<quint32> &layouts );
    CCellList *getLayout();

    void setTitle( CCellPayload *pTitle );
    CCellPayload* getTitle();

    void setContext( CTableContext *pContext );
    CTableContext *getContext();

    void append( CCell *pCell );
    void append( CCellPayload *pData );

    CCellList *getSheet();

    void setFmt(DsoType::DecodeFormat fmt);

public:
    void show();
    void test( void *pMem, int size );
    void randTable();
private:
    int mColumns;       /*!< 列数 */

    CCellList mHead;
    CCellList mLayout;
    CCellList mSheet;

    CCellPayload *m_pTitle;

    CTableContext *m_pContext;

    DsoType::DecodeFormat mFmt;

};

#endif // CTABLE_H
