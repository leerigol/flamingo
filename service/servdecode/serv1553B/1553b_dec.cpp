#include "1553b_fsm.h"
#include "../servdecode.h"

static C1553BIdle   fsm_1553b_idle;
static C1553BSync   fsm_1553b_sync;
static C1553BRT     fsm_1553b_rt;
static C1553BData   fsm_1553b_data;
static C1553BParity fsm_1553b_parity;

C1553BCfg* C1553BDecoder::mp1553BCfg;

int C1553BDecoder::m_nFormat   = FORMAT_ERR;
int C1553BDecoder::m_nHoldTime = 0;
bool C1553BDecoder::m_bHasError = false;

C1553BDecoder::C1553BDecoder()
{
    fsm_1553b_idle.setNextStat( &fsm_1553b_sync );
    fsm_1553b_sync.setNextStat( &fsm_1553b_rt );
    fsm_1553b_rt.setNextStat( &fsm_1553b_data );
    fsm_1553b_data.setNextStat( &fsm_1553b_parity );
    fsm_1553b_parity.setNextStat( &fsm_1553b_sync );

    setIdle(&fsm_1553b_idle);
}
C1553BDecoder::~C1553BDecoder()
{
}

//! rst to idle
void C1553BDecoder::rst()
{
    mpNow = &fsm_1553b_idle;
    mCounter.rst();
    mClock.stop();

    clearBlock();

    mp1553BCfg = &mpCurrServ->mServ1553B;

    mpTable = new CTable();
    setEventTable(mpTable);
}

bool C1553BDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{
    bool ret = false;
    if( decode( mp1553BCfg->mSrc) )
    {
        CDecLine* line = new CDecLine();

        //if( mpCurrServ->getLabelOn())
        {
            QString label = "1553B";
            line->setLabel(label);
        }

        mLineView.makeLine(*line, mBlocklist);
        bus->append( line );
        if(zoomBus)
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        ret = true;
    }

    return ret;
}

bool C1553BDecoder::decode(Chan src)
{
    unsigned int u32Data;
    int last_dat, next_dat;

    uchar* pStream = mpCurrServ->getChanData( src );
    if( !pStream /*|| !pStreamL*/)
    {
        return false;
    }

    last_dat = ( *pStream & 0x80 ) ? 1 : 0;
    next_dat = last_dat;

//    bool bPolarity = mp1553BCfg->mPolarity;

    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Data, pStream + i, 4 );

        //! no change
        if ( u32Data == m32IdleMask[last_dat] )
        {
            if ( mClock.tryTick(32) > 0)
            {                
            }
            else
            {
                i += 4;
                mCounter.count( 32 );
                mClock.tick(32);
                continue;
            }
        }

        uint dat = u32Data & 0xff;
        if(dat == m8IdleMask[last_dat])
        {
            if ( mClock.tryTick(8) > 0 )
            {
            }
            else
            {
                i += 1;
                mCounter.count(8);
                mClock.tick(8);
                continue;
            }
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_dat = u32Data & m32Masks[j] ? 1 : 0;

            //! rise
            if (last_dat <  next_dat)//rise
            {
                now()->serialIn( RISE, next_dat );
                last_dat = next_dat;
                mClock.sync();
            }
            else if (last_dat  >  next_dat)//fall
            {
                now()->serialIn( FALL, next_dat );
                last_dat = next_dat;
                mClock.sync();
            }
            //! timeout
            if ( mClock.tick() > 0 )
            {
                now()->serialIn( SAMP, next_dat );
            }
            mCounter.count();
        }
        i += 1;
    }
    return true;
}

int C1553BDecoder::setEventTable(CTable* table)
{
    //! head
    QList<quint32> lstHead;
    lstHead.append( MSG_DECODE_EVT_TIME );
    lstHead.append( MSG_DECODE_EVT_TYPE );
    lstHead.append( MSG_DECODE_EVT_PAYLOAD );
    lstHead.append( MSG_DECODE_EVT_ERR );
    table->setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 50 );
    lstLayout.append( 100 );
    lstLayout.append( 100 );
    lstLayout.append( 200 );
    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_1553B);
    table->setTitle( pTitle );
    return ERR_NONE;
}
