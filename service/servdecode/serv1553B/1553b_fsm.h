#ifndef C1553B_DECODER_H
#define C1553B_DECODER_H

#include "1553b_cfg.h"

#include "../decoder.h"
#include "../decTool/DecState.h"

enum
{    
    FORMAT_CS,
    FORMAT_DATA,
    FORMAT_ERR,
};

class C1553BDecoder: public CDecoder,public CDecState
{
public:
    C1553BDecoder();
    ~C1553BDecoder();

public:
     static C1553BCfg* mp1553BCfg;

public:
     bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
     void rst();
     int  setEventTable(CTable* table);
protected:
     static int  m_nFormat;//
     static int  m_nHoldTime;
     static bool m_bHasError;

     bool   mErr;
private:
    bool decode(Chan src);
};

class C1553BIdle : public C1553BDecoder
{
public:
    C1553BIdle();
    ~C1553BIdle();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );

};

class C1553BSync : public C1553BDecoder
{
public:
    C1553BSync();
    ~C1553BSync();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit( int bitVal );
};


class C1553BRT : public C1553BDecoder
{
public:
    C1553BRT();
    ~C1553BRT();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit( int bitVal );

private:
    int  nBitCount;
    int  nBitValue;
};

class C1553BData : public C1553BDecoder
{
public:
    C1553BData();
    ~C1553BData();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit( int bitVal );

private:
    int  nBitCount;
    int  nBitValue;
};


class C1553BParity : public C1553BDecoder
{
public:
    C1553BParity();
    ~C1553BParity();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit( int bitVal );

private:
    int  nBitCount;
    int  nBitValue;
};
#endif // C1553B_DECODER_H
