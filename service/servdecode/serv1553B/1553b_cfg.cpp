#include "1553b_cfg.h"
#include "servdecode.h"

void C1553BCfg::reset()
{

    mPolarity   =   Polarity_Positive;

    mSrc        =   chan1;

}

void C1553BCfg::serialIn(CStream &stream)
{
    int temp = 0;
    stream.read("mSrc",        temp);
    mSrc = (Chan)temp;

    stream.read("mPolarity",   mPolarity);
}

void C1553BCfg::serialOut(CStream &stream)
{
    stream.write("mSrc",        (int)mSrc);

    stream.write("mPolarity",   mPolarity);
}

void C1553BCfg::setSamplePerBaud(qint64 sample)
{
    qint32  mBaudPre = 2000000;
    if( mBaudPre > 0)
    {
        mSamplePerBaud = sample / mBaudPre;
    }
    else
    {
        mSamplePerBaud = 8;
    }

    if( mSamplePerBaud < 3)
    {
        mSamplePerBaud = 3;
    }
}

int C1553BCfg::getSamplePerBaud()
{
    return mSamplePerBaud;
}


DsoErr servDec::set1553BSrc(Chan src)
{
    mServ1553B.mSrc = src;
    getChThreshold1(src);
    setuiChange( MSG_DECODE_1553B_SRC );
    return ERR_NONE;
}

Chan servDec::get1553BSrc()
{
    return mServ1553B.mSrc;
}


DsoErr servDec::set1553BThre1(qint64 t)
{
    setChThreshold(mServ1553B.mSrc, t);
    return ERR_NONE;
}

qint64 servDec::get1553BThre1()
{
    Chan s = mServ1553B.mSrc;
    return getChThreshold(s);
}

DsoErr servDec::set1553BThre2(qint64 t)
{
    setChThresholdL(mServ1553B.mSrc, t);
    return ERR_NONE;
}

qint64 servDec::get1553BThre2()
{
    Chan s = mServ1553B.mSrc;
    return getChThresholdL(s);
}

DsoErr servDec::set1553BPol(bool pn)
{
    mServ1553B.mPolarity = pn;
    return ERR_NONE;
}

bool servDec::get1553BPol()
{
    return mServ1553B.mPolarity;
}
