#include "1553b_fsm.h"
#include "servdecode.h"

#include <QDebug>

//! C1553BIdle
C1553BIdle::C1553BIdle()
{}
C1553BIdle::~C1553BIdle()
{}

void C1553BIdle::onEnter( int /*bitVal*/ )
{
    mCounter.begin();
    mClock.stop();
}

void C1553BIdle::serialIn( int event, int bitVal )
{
    if( event == FALL || event == RISE )
    {
        //! enter into break
        toNext( bitVal );

        m_nHoldTime = mp1553BCfg->getSamplePerBaud();
        //! clock start samp
        mClock.start( m_nHoldTime, m_nHoldTime >> 1 );
    }
}


C1553BSync::C1553BSync()
{}
C1553BSync::~C1553BSync()
{}

void C1553BSync::onEnter(int /*bitVal*/)
{
    mData    = 0;
    mCount   = 0;
    mErr     = false;
    m_nFormat= FORMAT_ERR;
}

void C1553BSync::serialIn(int event, int bitVal)
{
    if( event == SAMP && mCount < 6)
    {
        mData = (mData<<1)  | bitVal;
        mCount++;

        if(mCount == 1)
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }
        /*
         * --------
         *        -
         *        ---------
         */
        if( mCount == 6  )
        {
            if( mData == 0x38 )
            {
                m_nFormat = FORMAT_CS;
                toNext( bitVal );
                return;
            }
            else if(mData == 0x7)
            {
                m_nFormat = FORMAT_DATA;
             }
            else if(mData == 0 || mData == 0xff)
            {
                mCount = 0;
                toIdle( bitVal );
                return;
            }
            else
            {
                //qDebug() << "error format:0x" << QString().setNum(mData,16);
                mErr = true;
            }
            toNextNext( bitVal );
        }
    }
}

void C1553BSync::onExit(int /*bitVal*/)
{
    if( mCount == 6 )
    {
        CDecPayloadString* pData = new  CDecPayloadString();
        Q_ASSERT( NULL != pData );

        mSpan = mCounter.getDur() + (m_nHoldTime >> 1);
        pData->setPosSpan( mPosition, mSpan );

        int text = TEXT_CS;
        int type = MSG_DECODE_EVT_CS;
        if( m_nFormat == FORMAT_DATA )
        {
            text = TEXT_DATA;
            type = MSG_DECODE_EVT_DATA;
        }
        else if( m_nFormat == FORMAT_ERR )
        {
            text = TEXT_SYNC;
            type = MSG_TRIGGER_VIDEO_SYNC;
        }

        if(mErr)
        {
            pData->setPayload(ERR_COLOR, text);
        }
        else
        {
            pData->setPayload(STD1553_CS_COLOR,text);
        }
        AddBlock(CDecPayloadString,pData);

        mpTable->append(new CCellTime(mPosition));

        mpTable->append(new CCellStr( type) );
        m_bHasError = mErr;
    }
    mData = 0;
}



C1553BRT::C1553BRT()
{}
C1553BRT::~C1553BRT()
{}

void C1553BRT::onEnter( int /*bitVal*/ )
{
    mData    = 0;
    mCount   = 0;
    nBitCount= 0;
    nBitValue= 0;
}

void C1553BRT::serialIn( int event, int bitVal )
{
    if( event == SAMP )
    {
        if(mCount == 0 && nBitCount == 0)
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }

        if( nBitCount < 2 )
        {
            nBitValue = (nBitValue << nBitCount) | bitVal;
            nBitCount++;
        }

        if( nBitCount == 2 )
        {
            mData = ( mData << 1 ) | (nBitValue == 2);
            mCount++;


            nBitValue=0;
            nBitCount=0;

            if( mCount == 5 )
            {
                toNext( bitVal );
            }
        }
    }
}

void C1553BRT::onExit(int /*bitVal*/)
{
    if(mCount == 5)
    {
        CDecPayloadCmd* pData = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pData );

        mSpan = mCounter.getDur() + (m_nHoldTime >> 1);
        pData->setPosSpan( mPosition, mSpan );
        pData->setPayload( mData ,mCount, CAN_ID_RMT_COLOR, TEXT_RT);
        AddBlock(CDecPayloadCmd,pData);
    }
}


C1553BData::C1553BData()
{}
C1553BData::~C1553BData()
{}

void C1553BData::onEnter( int /*bitVal*/ )
{
    //mData    = 0;
    mCount   = 0;
    nBitCount= 0;
    nBitValue= 0;
    mErr     = false;
}

void C1553BData::serialIn( int event, int bitVal )
{
    if( event == SAMP )
    {
        if(mCount == 0 && nBitCount == 0)
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }

        if( nBitCount < 2 )
        {
            nBitValue = (nBitValue << nBitCount) | bitVal;
            nBitCount++;
        }

        if( nBitCount == 2 )
        {
            if( nBitValue ==1 || nBitValue == 2)
            {
                mData = ( mData << 1 ) | (nBitValue -1) ;
            }
            else
            {
                mErr = true;
            }
            mCount++;

            nBitValue=0;
            nBitCount=0;

            if( (mCount == 16 && m_nFormat >= FORMAT_DATA) ||
                (mCount == 11 && m_nFormat == FORMAT_CS )    )
            {
                toNext( bitVal );
            }
        }
    }
}

void C1553BData::onExit(int /*bitVal*/)
{
    if(mCount == 16 || mCount == 11 )
    {
        if( mErr )
        {
            CDecPayloadString* pData = new  CDecPayloadString();
            Q_ASSERT( NULL != pData );

            mSpan = mCounter.getDur() + (m_nHoldTime >> 1);
            pData->setPosSpan( mPosition, mSpan );

            pData->setPayload(ERR_COLOR, TEXT_MANCH);

            AddBlock(CDecPayloadString,pData);
        }
        else
        {
            CDecPayloadData* pData = new  CDecPayloadData();
            Q_ASSERT( NULL != pData );

            mSpan = mCounter.getDur() + (m_nHoldTime >> 1);
            pData->setPosSpan( mPosition, mSpan );
            if( m_nFormat == FORMAT_CS )
            {
                 pData->setPayload( mData,mCount,COMM_DATA_COLOR,TEXT_CS);
            }
            else
            {
                pData->setPayload( mData,mCount,COMM_DATA_COLOR,TEXT_DATA);
            }
            AddBlock(CDecPayloadData,pData);
        }

        m_bHasError |= mErr;
        mpTable->append(new CCellDW(mData,mCount));
    }
}




C1553BParity::C1553BParity()
{}
C1553BParity::~C1553BParity()
{}

void C1553BParity::onEnter( int /*bitVal*/ )
{
    nBitCount= 0;
    nBitValue= 0;
    mCount   = 0;
    mErr     = false;
}

void C1553BParity::serialIn( int event, int bitVal )
{
    if( event == SAMP )
    {
        if(mCount == 0 && nBitCount == 0)
        {
            mCounter.begin();
            mPosition = mCounter.now();
        }

        if( nBitCount < 2 )
        {
            nBitValue = (nBitValue << nBitCount) | bitVal;
            nBitCount++;
        }

        if( nBitCount == 2 )
        {
            if( nBitValue == 1 || nBitValue == 2)
            {
                int oneCount = 0;
                for(int i=0; i<16; i++)
                {
                    if( mData & ( 1 << i) )
                    {
                        oneCount++;
                    }
                }

                nBitValue = nBitValue - 1;// 1/2 - 1 = 0/1

                if( ((nBitValue + oneCount) & 0x1) == 0)
                {
                    mErr = true;
                }
            }
            else
            {
                mErr = true;
            }
            toNext( bitVal );
        }
    }
}

void C1553BParity::onExit(int /*bitVal*/)
{
    CDecPayloadCmd* pData = new  CDecPayloadCmd();

    int color = COMM_ACK_COLOR;
    if( mErr )
    {
        color = ERR_COLOR;
    }
    //LIN_PARITY_COLOR
    pData->setPayload( nBitValue,1, color, TEXT_PARITY);

    mSpan = mCounter.getDur() + (m_nHoldTime >> 1);
    pData->setPosSpan( mPosition, mSpan );
    AddBlock(CDecPayloadCmd, pData);

    m_bHasError |= mErr;

    if( m_bHasError )
    {
        mpTable->append(new CCellStr(MSG_DECODE_EVT_ERR));//Err
    }
    else
    {
        mpTable->append(new CCellStr(0));
    }
}
