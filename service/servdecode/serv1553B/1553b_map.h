#ifndef B1553B_MAP_H
#define B1553B_MAP_H

#include "../servdecode/servdecode.h"


on_set_int_int  (MSG_DECODE_1553B_SRC,        &servDec::set1553BSrc),
on_get_int      (MSG_DECODE_1553B_SRC,        &servDec::get1553BSrc),


on_set_int_ll   (MSG_DECODE_1553B_THRE1,      &servDec::set1553BThre1),
on_get_ll       (MSG_DECODE_1553B_THRE1,      &servDec::get1553BThre1),

on_set_int_ll   (MSG_DECODE_1553B_THRE2,      &servDec::set1553BThre2),
on_get_ll       (MSG_DECODE_1553B_THRE2,      &servDec::get1553BThre2),

on_set_int_bool (MSG_DECODE_1553B_POL,        &servDec::set1553BPol),
on_get_bool     (MSG_DECODE_1553B_POL,        &servDec::get1553BPol),

#endif // B1553B_MAP_H

