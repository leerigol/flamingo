#ifndef C1553BCFG_H
#define C1553BCFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;

struct C1553BCfg
{
    void reset();
    void serialIn(CStream&);
    void serialOut(CStream&);

    int  getSamplePerBaud(void);
    void setSamplePerBaud(qint64 sample); //sample / baud

    Chan   mSrc;
    bool   mPolarity;
    int    mSamplePerBaud;
};
#endif // C1553BCFG_H
