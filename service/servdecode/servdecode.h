#ifndef SERVDECODE_H
#define SERVDECODE_H

#include "../service.h"
#include "../service_msg.h"

#include "../../include/dsocfg.h"

#include "../../baseclass/iserial.h"
#include "../../include/dsotype.h"

#include "eventTable//ctable.h"
#include "eventTable/ccellstr.h"
#include "../servtrace/servtrace.h"



#include "./servUart/uart_cfg.h"
#include "./servI2C/i2c_cfg.h"
#include "./servSPI/spi_cfg.h"
#include "./servLin/lin_cfg.h"
#include "./servCAN/can_cfg.h"
#include "./servPal/pal_cfg.h"
#include "./servFlexray/flex_cfg.h"
#include "./servI2S/i2s_cfg.h"
#include "./serv1553B/1553b_cfg.h"

#include "./servBlock/DecBus.h"

typedef enum
{
    Bus_Parallel,//0
    Bus_RS232,
    Bus_SPI,
    Bus_I2C,

    Bus_LIN,//4
    Bus_CAN,
    Bus_FlexRay,

    Bus_I2S,//7

    Bus_STD1553B,
    Bus_ARINC429,
    Bus_SENT,

    Bus_QC,
    Bus_PD,

    Bus_OneWire,//8
    Bus_S_BUS,

}BusType;

typedef enum
{
    EVENT_TABLE,
    EVENT_DETAILS,
    EVENT_PAYLOAD
}EventMode;

typedef enum
{
    EDGE_RISING,
    EDGE_FALLING,
    EDGE_ALL
}Edge;

typedef enum
{
    Polarity_Negtive,
    Polarity_Positive
}Polarity;

enum
{
    ws_left,
    ws_right
};

enum
{
    BUS_READY,
    BUS_DIRTY,
    BUS_ALL
};

//!scpi
typedef enum
{
    //! pal
    thre_pal,
    //!rs232
    thre_tx,
    thre_rx,
    //!i2c
    thre_scl,
    thre_sda,
    //!spi
    thre_cs,
    thre_clk,
    thre_miso,
    thre_mosi,
    //!lin
    thre_lin,
    //!can
    thre_can,
    thre_can_sub_1,

}DecThreType;


/*!
 * \brief The servDec class
 * Ref
 */
class servDec : public serviceExecutor, public ISerial
{
    Q_OBJECT

    DECLARE_CMD()

public:
    enum enumCmd
    {
        cmd_none = 0,
        cmd_bus_completed,

        cmd_bus_data,
        cmd_bus_zoom_data,

        cmd_bus_evt,

        cmd_bus_src_thre,
        cmd_bus_src_param,
        cmd_bus_chan1_change,
        cmd_bus_chan2_change,
        cmd_bus_chan3_change,
        cmd_bus_chan4_change,
        cmd_bus_license,
        cmd_trace_ready,

        cmd_dec_evt_id,
        cmd_bus_evt_data,
        cmd_bus_jump_to,
        cmd_bus_evt_save,

        cmd_bus_position,
#if 0
        //!scpi
        //! abs
        cmd_decode_thre_abs,
        //!rs232
        cmd_decode_thre_tx,
        cmd_decode_thre_rx,
        //!i2c
        cmd_decode_thre_scl,
        cmd_decode_thre_sda,
        //!spi
        cmd_decode_thre_cs,
        cmd_decode_thre_clk,
        cmd_decode_thre_miso,
        cmd_decode_thre_mosi,
        //!lin
        cmd_decode_thre_lin,
        //!can
        cmd_decode_thre_can,
        cmd_decode_thre_can_sub_1,
#endif

    };

public:
     servDec( QString name, ServiceId eId = E_SERVICE_ID_NONE );
    ~servDec();

     static void clearCache();
     static void switchHeap();
/*************************************************************
 * Base Interface
 ************************************************************/
public:
    DsoErr start();
    void   rst();
    void   init();
    int    startup();

    void   registerSpy();
    int    serialOut( CStream &stream, serialVersion &ver );
    int    serialIn( CStream &stream, serialVersion ver );

    int    onViewChanged(void);
    int    onReturnMenu(int);

    int    on_enterActive();
    int    on_exitActive(int id);
/*************************************************************
 * Common parameters and API
 ************************************************************/
public:
    DsoErr setBusOn(bool);
    bool   getBusOn();


    DsoErr setBusOffset(int);
    int    getBusOffset();
    DsoErr setBusScpiPos(int);

    DsoErr setBusFormat(int);
    DecodeFormat    getBusFormat();

    DsoErr setDecodeType(int);
    int    getDecodeType();

    DsoErr setApplytoTrig();

    void   setLASource();

    /*event & config*/
    DsoErr setLabelOn(bool);
    bool   getLabelOn();

    DsoErr setEventOn(bool);
    bool   getEventOn();

    DsoErr setEventFormat(int);
    DecodeFormat    getEventFormat();

    DsoErr setEventViewMode(int);
    int    getEventViewMode();

    DsoErr setEventBusX(int busx);
    int    getEventBusX();

    DsoErr exportEventData();
    DsoErr setJumpEvent();

    DsoErr copyTrig();

    DsoErr onTraceReady(void* ptr);
    void   getChanParam(CArgument&);

    void   getEvtData( CArgument &arg );

    int saveEvtData(QString fileName);

    bool   getStream(Chan src, DsoWfm& stream);

    uchar* getChanData(Chan c);
    int    getDataSize();
    bool   getDataAttr(int zoom ,int& vLeft ,int& vLen );

    void   postCmdData(bool on);
    void   openDecodeTrace(Chan chx);
    void   closeDecodeTrace();

    void   setBusCompleted();
    bool   getBusCompleted();

    bool   startDecode();
    void   finishDecode(bool);

    CDecBus *getBus();
    CDecBus *getZoomBus();

    CDecBus *getDirtyBus();
    CDecBus *getDirtyZoomBus();

    void    switchBusType();

    CTable  *getEvtTable();
    void    addWaveKeys();

    int     onZoomScale();
    qint64  getZoomScale();

    int     onZoomOffset();
    qint64  getZoomOffset();

    int     onChan1Change();
    int     onChan2Change();
    int     onChan3Change();
    int     onChan4Change();

    int     onCheckLicense();
    int     onTriggerSweep();

    int     getDecId();//qxl

    int     onDispClear();
    int     onRunStop();

public:
    int  m_nBusType;
    bool m_bBusOn;
    int  m_nPosition;
    bool m_bSuccess;
    int  m_nViewMode;
    qint64 m_n64ZoomScale;
    qint64 m_n64ZoomOffset;
    qint64 m_n64MainScale;
    qint64 m_n64MainOffset;

    static int m_nDecId;//qxl 2018.2.5
    CTable *m_pEvt;

    static QSemaphore mTraceSem;
    static bool m_bShowEvt;
    static bool m_bShowLabel;
    static int  m_nEvtViewMode;
    static int  m_nEvtViewBusX;
    static int  m_nEvtBusXList;
    static int  m_nFormat;
    static int  m_nEventFormt;

    static QMap<Chan,qint64> m_Threshold;
    static QMap<Chan,qint64> m_ThresholdL;
    static QMap<Chan,DsoWfm*> m_Streams;
    static int               m_nStreamSize;
    static int               m_nLed;
    static bool              m_bWithMSO;

    static int               m_nCurrentChan;
    static qint64            m_nCurrentThre;

    static qint64            m_nSampleRate;
    static qint64            m_nLaSampleRate;
    static double            m_timeInc;
    static double            m_timeStart;


private:
    //! results
    CDecBus mBus[2];
    CDecBus mBusZoom[2];
    int     m_nBusIndex;//dirty or ready

protected:
    bool keyTranslate(int key, int &msg, int &controlAction);

private:
    static serviceKeyMap     m_WaveKeyMap;

private:
    static CHeapMgr  EvtHeapMgr;
    const static int EvtHeapSize = 2*1024*1024;


/*************************************************************************
 * Parallel configuration API
*************************************************************************/
public:
    CPalCfg mServPal;
    DsoErr  setPalClkSrc(Chan clksrc);
    Chan    getPalClkSrc();

    DsoErr  setPalClkEdge(int clkedge);
    int     getPalClkEdge();

    DsoErr  setPalPolarity(bool palpn);
    bool    getPalPolarity();

    DsoErr  setPalNrj(bool nrj);
    bool    getPalNrj();

    DsoErr  setPalNrjTime(qint64 nrjtime);
    qint64  getPalNrjTime();

    DsoErr  setPalClkDly(qint64 clkdly);
    qint64  getPalClkDly();

    DsoErr  setPalGraph(bool graph);
    bool    getPalGraph();

    DsoErr  setPalCurbit(int curbit);
    int     getPalCurbit();

    DsoErr  setPalCurCh(Chan curch);
    int     getPalCurCh();

    DsoErr  setPalBusWidth(int);
    int     getPalBusWidth();

    DsoErr  setPalCurAbsThre(qint64);
    qint64  getPalCurAbsThre();

    DsoErr  setPalClkAbsThre(qint64);
    qint64  getPalClkAbsThre();

    DsoErr  setPalBus(int);
    int     getPalBus();

    DsoErr  setPalEndian(bool b);
    bool    getPalEndian();

/*************************************************************************
 * RS232 configuration API
*************************************************************************/
public:
    CUartCfg  mServUart;

    DsoErr  setUartTxSrc(Chan tx);
    Chan    getUartTxSrc();

    DsoErr  setUartRxSrc(Chan rx);
    Chan    getUartRxSrc();

    DsoErr  setUartTxThre(qint64 txthre);
    qint64  getUartTxThre();

    DsoErr  setUartRxThre(qint64 rxthre);
    qint64  getUartRxThre();

    DsoErr  setUartBaudVal(int baud);
    int     getUartBaudVal();

    DsoErr  setUartNegative(bool negative);
    bool    getUartNegative();

    DsoErr  setUartLsb(bool lsb);
    bool    getUartLsb();

    DsoErr  setUartDataWidth(int datawid);
    int     getUartDataWidth();

    DsoErr  setUartParity(uint parity);
    uint    getUartParity();

    DsoErr  setUartStopBit(uint);
    uint    getUartStopBit();

    DsoErr  setUartPackgeEn(bool packgeen);
    bool    getUartPackgeEn();

    DsoErr  setUartPackgeEnd(uint packgeend);
    uint    getUartPackgeEnd();

/*************************************************************************
 * I2C configuration API
*************************************************************************/
public:
    CI2CCfg mServI2C;
    DsoErr  setI2cClkSrc(Chan clk);
    Chan    getI2cClkSrc();

    DsoErr  setI2cDatSrc(Chan data);
    Chan    getI2cDatSrc();

    DsoErr  setI2cClkThre(qint64 clkthre);
    qint64  getI2cClkThre();

    DsoErr  setI2cDatThre(qint64 datthre);
    qint64  getI2cDatThre();

    DsoErr  setI2cIncRW(bool incrw);
    bool    getI2cIncRW();

    DsoErr  setI2cChange(bool);
    bool    getI2cChange();

/*************************************************************************
 * SPI configuration API
*************************************************************************/
public:
    CSpiCfg mServSPI;
    DsoErr  setSpiCsSrc(Chan cssrc);
    Chan    getSpiCsSrc();

    DsoErr  setSpiCsAbsThre(qint64 thre);
    qint64  getSpiCsAbsThre();

    DsoErr  setSpiClkSrc(Chan src);
    Chan    getSpiClkSrc();

    DsoErr  setSpiMisoSrc(Chan src);
    Chan    getSpiMisoSrc();

    DsoErr  setSpiMosiSrc(Chan src);
    Chan    getSpiMosiSrc();

    DsoErr  setSpiClkAbsThre(qint64 thre);
    qint64  getSpiClkAbsThre();

    DsoErr  setSpiMisoAbsThre(qint64 thre);
    qint64  getSpiMisoAbsThre();

    DsoErr  setSpiMosiAbsThre(qint64 thre);
    qint64  getSpiMosiAbsThre();

    DsoErr  setSpiMode(bool mode);
    bool    getSpiMode();

    DsoErr  setSpiCsPN(bool pn);
    bool    getSpiCsPN();

    DsoErr  setSpiTimeout(qint64 time);
    qint64  getSpiTimeout();

    DsoErr  setSpiClkEdge(bool edge);
    bool    getSpiClkEdge();

    DsoErr  setSpiEndian(bool endian);
    bool    getSpiEndian();

    DsoErr  setSpiDataPN(bool pn);
    bool    getSpiDataPN();

    DsoErr  setSpiDataWidth(int datawid);
    int     getSpiDataWidth();

/*************************************************************************
 * LIN configuration API
*************************************************************************/
public:
    CLinCfg mServLin;

    DsoErr  setLinSrc(Chan src);
    Chan    getLinSrc();

    DsoErr  setLinThreAbs(qint64 thre);
    qint64  getLinThreAbs();

    DsoErr  setLinBaudPre(int baud);
    int     getLinBaudPre();

    DsoErr  setLinProtocol(int version);
    int     getLinProtocol();

    DsoErr  setLinPolarity(bool pn);
    bool    getLinPolarity();

    DsoErr  setLinParity(bool parity);
    bool    getLinParity();

/*************************************************************************
 * CAN configuration API
*************************************************************************/
public:
    CCanCfg mServCan;

    DsoErr  setCanSrc(Chan src);
    Chan    getCanSrc();

    DsoErr  setCanThreAbsPre(qint64 thre);
    qint64  getCanThreAbsPre();

    DsoErr  setCanBaud(int baud);
    int     getCanBaud();

    DsoErr  setCanFDBaud(int baud);
    int     getCanFDBaud();

    DsoErr  setCanSignalType(int type);
    int     getCanSignalType();

    DsoErr  setCanSamplePos(int);
    int     getCanSamplePos();

/*************************************************************************
 * Flexray configuration API
*************************************************************************/
    CFlexCfg mServFlex;
    DsoErr  setFlexSrc(Chan src);
    Chan    getFlexSrc();

    DsoErr  setFlexThreAbs(qint64);
    qint64  getFlexThreAbs();

    DsoErr  setFlexRate(int);
    int     getFlexRate();

    DsoErr  setFlexSignal(int);
    int     getFlexSignal();

    DsoErr  setFlexSamplePos(int);
    int     getFlexSamplePos();


    DsoErr  setFlexChannel(bool);
    bool    getFlexChannel();

/*************************************************************************
 * I2S API
 * ***********************************************************************/
    CI2SCfg mServI2S;

    DsoErr  setI2SClk(Chan src);
    Chan    getI2SClk();

    DsoErr  setI2SClkThre(qint64 t);
    qint64  getI2SClkThre();

    DsoErr  setI2SEdge(bool edge);
    bool    getI2SEdge();

    DsoErr  setI2SWS(Chan src);
    Chan    getI2SWS();
    DsoErr  setI2SWSThre(qint64 t);
    qint64  getI2SWSThre();

    DsoErr  setI2SData(Chan src);
    Chan    getI2SData();

    DsoErr  setI2SDataThre(qint64 t);
    qint64  getI2SDataThre();

    DsoErr  setI2SWord(int time);
    int     getI2SWord();

    DsoErr  setI2SWSLow(bool b);
    bool    getI2SWSLow();

    DsoErr  setI2SRecv(int pn);
    int     getI2SRecv();

    DsoErr  setI2SAlign(int datawid);
    int     getI2SAlign();

    DsoErr  setI2SEndian(bool);
    bool    getI2SEndian();

    DsoErr  setI2SPol(bool);
    bool    getI2SPol();

    /*************************************************************************
    * 1553B API
    *************************************************************************/
    C1553BCfg mServ1553B;
    DsoErr  set1553BSrc(Chan src);
    Chan    get1553BSrc();

    DsoErr  set1553BThre1(qint64 t);
    qint64  get1553BThre1();

    DsoErr  set1553BThre2(qint64 t);
    qint64  get1553BThre2();

    DsoErr  set1553BPol(bool pn);
    bool    get1553BPol();
/*************************************************************************
* SCPI API
*************************************************************************/
private:
    void   configThreshold(Chan ch, qint64 a);

    //!scpi
    DsoErr setChThreshold(Chan ch, qint64 value, bool cfg = true);
    qint64 getChThreshold(Chan ch);
    qint64 getChThreshold1(Chan ch);//NOT CHANGE MENU ATTR

    //get valid threshold when chan changed
    qint64 getValidThreshold(Chan ch);

    DsoErr setChThresholdL(Chan ch, qint64 value);
    qint64 getChThresholdL(Chan ch);

    Chan   getThreCh(DecThreType tch);
public:
    DsoErr setDecThre(CArgument argi);
    void   getDecThre(CArgument argi, CArgument argo);

};

class CDecoderThread:public QThread
{
    Q_OBJECT
public:
    CDecoderThread(QObject *parent);
    void run();

public:
    void     addDecoder(servDec*);
    servDec* getDecoder(int i = 0);
    static int  m_nTimemode;

protected:
    QList<servDec*> mDecoders;

};

class servDecodeSel: public serviceExecutor
{
    Q_OBJECT

protected:
    DECLARE_CMD()

public:
    servDecodeSel(QString name, ServiceId id = E_SERVICE_ID_NONE );
    void registerSpy();

    void    onTimeMode();

    int     enterBus1();
    int     enterBus2();
    int     enterBus3();
    int     enterBus4();

    QString getBusStr1();
    QString getBusStr2();
    QString getBusStr3();
    QString getBusStr4();

private:
    int     enterBus(int i = 0);
    QString getBusStr(int i=0);
};
#endif //SERVDECODE_H
