#include "can_fsm.h"

#include <QDebug>


//! CCanIdle
CCanIdle::CCanIdle()
{}
CCanIdle::~CCanIdle()
{}

void CCanIdle::onEnter( int /*bitVal*/ )
{
    mCounter.begin();
    mClock.start( mpCanCfg->getSamplePerBaud(),
                  mpCanCfg->getSamplePerBaud() * mpCanCfg->mSamplePos  / 100);
    mCount = 0;
    mData  = 0;
    bEOF   = false;
}

void CCanIdle::serialIn( int event, int bitVal )
{
    if(event == SAMP )
    {
        mData = ( mData << 1 ) | bitVal;
        mCount++;
        if( mCount >= 7 && (mData & CAN_EOF) == CAN_EOF )
        {
            bEOF = true;
        }
    }
    else if( event == FALL && bEOF )
    {
        toNext( bitVal );
        //! clock start samp        
    }
}

//! CCanStart
CCanStart::CCanStart()
{}
CCanStart::~CCanStart()
{}

void CCanStart::onEnter( int /*bitVal*/ )
{
    mFillBit = 0;
    mCheckStart = 0;
    crc15 = 0;
}

// SOF- 1bit
void CCanStart::serialIn( int event, int bitVal )
{
    if ( event == SAMP )
    {
        if(bitVal == 0)
        {
            if( BitFilled == checkFillBit( bitVal ) ) //if fill bit then get next
            {
                return;
            }
            crc(bitVal);
            toNext( bitVal );
        }
        else
        {
            toPrev( bitVal );
        }
    }
}

void CCanStart::onExit(int /*bitVal*/)
{
}



//! CCanArb
CCanArb::CCanArb()
{}
CCanArb::~CCanArb()
{}

void CCanArb::onEnter( int /*bitVal*/ )
{
    mErr    = false;
    mCount  = 0;
    mData   = 0;
    mRTR    = RECESSIVE;
    mIDE    = CAN_STD;
    mEDL    = CAN_STD;
    mBRS    = ERRFLAG;
    mExtCount= 0;

    mIdentifier = 0;
    mCounter.begin();
    mPosition = mCounter.now();
}

void CCanArb::serialIn( int event, int bitVal )
{
    if( event  == SAMP)
    {
        int bit = checkFillBit( bitVal );
        //check bit fill
        if( bit == BitFilled )
        {
            return;
        }
//        else if( bit == BitError )
//        {
//            toIdle( bitVal );
//            mpTable->append(new CCellStr(1));//dlc
//            mpTable->append(new CCellStr(1));//data
//            mpTable->append(new CCellStr(1));//crc
//            mpTable->append(new CCellStr(1));//ack
//            return;
//        }

        crc(bitVal);

        if( mIDE == CAN_STD )
        {            
            serialInStd( bitVal );
        }
        else if(mIDE == CAN_EXT)
        {            
            serialInExt( bitVal );
        }
    }
}

void CCanArb::serialInStd(int bitVal)
{
    mCount++;
    if(mCount < 12)
    {
        mIdentifier = (mIdentifier  << 1) | bitVal;
        if( mCount == 7 )
        {
            if(( mIdentifier & CAN_EOF ) == CAN_EOF)
            {
                mErr = true;
            }
        }

    }


    /*
     Remote Transmission Request BIT
     In DATA FRAMEs the RTR BIT has to be 0. Within a REMOTE FRAME the
     RTR BIT has to be 1.

    Substitute Remote Request BIT
    The SRR is a 1. It is transmitted in Extended Frames at the position of the
    RTR bit in Standard Frames and so substitutes the RTR-Bit in the Standard Frame.

    Identifier Extension Bit
    The IDE Bit belongs to
    - the ARBITRATION FIELD for the  Extended Format
    - the Control Field for the  Standard Format
    The IDE bit in the Standard Format is transmitted 0, whereas in the Extended
    Format the IDE bit is 1.
    */
    if(mCount == 12)
    {
        mR= bitVal;//RTR or SRR
        mSpan = mCounter.getDur();
    }

    if(mCount == 13) //check wheather standard or ext
    {
        mIDE = bitVal;
        if(mIDE == CAN_EXT)
        {
            mExtCount = 0;
        }
        mRTR = mR;
    }

    if(mCount == 14) //check wheather CAN for FD
    {
        //Extended Data Length
        mEDL = bitVal;//0 CAN_STD;1 CAN_FD
        if(mEDL == CAN_STD)//Can
        {
            toNext( bitVal );
        }
    }
    if(mCount == 16)
    {
        //Bit Rate Switch,0 NO switch, 1 switch
        mBRS = bitVal;

        if( mBRS )
        {
            mClock.start( mpCanCfg->getFDClkPeriod(),
                          mpCanCfg->getFDClkPeriod()/2);
        }
    }
    if(mCount == 17)
    {
        mESI = bitVal;
        toNext( bitVal );
    }
}

void CCanArb::serialInExt(int bitVal)
{
    mExtCount++;
    if(mExtCount < 19)
    {
        mIdentifier = (mIdentifier  << 1) | bitVal;
    }
    else
    {
        if(mExtCount == 19)
        {
            mRTR= bitVal;//RTR
            mSpan = mCounter.getDur();
        }
        else if(mExtCount == 20)
        {
            mEDL = bitVal;//1,FD;0,CAN
        }
        else if( mExtCount == 21 )
        {
            //bit20=r1,bit21 r0
            if( mEDL == CAN_STD )
            {
                toNext( bitVal );
            }
        }
        else if(mExtCount == 22)
        {
            mBRS = bitVal;
        }
        else if(mExtCount == 23)
        {
            mESI = bitVal;
            toNext( bitVal );
        }
    }
}

void CCanArb::onExit(int /*bitVal*/)
{       
        CDecPayloadCmd* pCmd = new CDecPayloadCmd();
        Q_ASSERT( NULL != pCmd );
        pCmd->setPosSpan( mPosition, mSpan );

        int id_color = CAN_ID_COLOR;
        if( mErr )
        {
            id_color = ERR_COLOR;
        }

        int m = 11;
        if( mIDE == CAN_STD)
        {
            pCmd->setPayload(mIdentifier,m ,id_color,TEXT_ID);
        }
        else
        {
            m = 29;
            pCmd->setPayload(mIdentifier,m ,id_color,TEXT_ID);
        }
        AddBlock(CDecPayloadCmd,pCmd);

        mpTable->append(new CCellTime(mPosition));
        mpTable->append(new CCellInfo(mIdentifier,m));

        AddOneFrame();
}

//! CCanCtl
CCanCtl::CCanCtl()
{}
CCanCtl::~CCanCtl()
{}
void CCanCtl::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mErr = false;
    mDLC = 0;
    mDLCTemp = 0;
    mCounter.begin();
    mPosition = mCounter.now();
}


void CCanCtl::serialIn( int event, int bitVal )
{

    if( event == SAMP )
    {
        int bit = checkFillBit( bitVal );
        //check bit fill
        if( bit == BitFilled )
        {
            return;
        }
//        else if( bit == BitError )
//        {
//            toIdle( bitVal );
//            mpTable->append(new CCellStr(1));//data
//            mpTable->append(new CCellStr(1));//crc
//            mpTable->append(new CCellStr(1));//ack
//            return;
//        }

        crc(bitVal);
        mDLC = (mDLC  << 1) | bitVal;
        mCount++;
        if(mCount == 4)
        {
            if(mDLC >= 9 && mDLC <=12)//canFD DCL 12,16,20,24,28,32
            {
                mDLC = (mDLC-8)*4 + 8;
            }
            if(mDLC == 13)
            {
                mDLC = 32;
            }
            if(mDLC == 14)
            {
                mDLC = 48;
            }
            if(mDLC ==15)
            {
                mDLC = 64;
            }

            if(mDLC == 4 )
            {
                //qDebug() << "mdlc=4";
            }
            toNext( bitVal );//Data Frame

            //if RMT or DLC = 0 then jump to CRC field
            if(mRTR == CAN_FRAME_RMT || mDLC == 0)
            {
                now()->toNext( bitVal );
            }
        }
    }
}

void CCanCtl::onExit(int /*bitVal*/)
{
    //! export id
    mDLCTemp = mDLC;

    mSpan = mCounter.getDur();

    CDecPayloadCmd* pCmd = new CDecPayloadCmd();
    Q_ASSERT( NULL != pCmd );
    pCmd->setPosSpan( mPosition, mSpan );
    if( mRTR == CAN_FRAME_RMT )
    {
        pCmd->setPayload(mDLC,4,CAN_ID_RMT_COLOR,TEXT_RMT);
    }
    else
    {
        pCmd->setPayload(mDLC,4,CAN_DLC_COLOR,TEXT_DLC);
    }

    AddBlock(CDecPayloadCmd,pCmd);


    mpTable->append(new CCellInfo(mDLC,8));
    if( mRTR != CAN_FRAME_RMT && mDLC > 0)
    {
        mCellData = new CCellDW();
        mpTable->append(mCellData);
    }
    else
    {
        mpTable->append(new CCellStr(1));//add space
    }
}

//! CCanData
CCanData::CCanData()
{}
CCanData::~CCanData()
{}

void CCanData::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData = 0;
    mErr = false;
    mCounter.begin();
    mPosition = mCounter.now();
}

void CCanData::serialIn( int event, int bitVal )
{

    if( event == SAMP )
    {
        int bit = checkFillBit( bitVal );
        //check bit fill
        if( bit == BitFilled )
        {
            return;
        }
        else if( bit == BitError )
        {
            mCount = 0;
            toErr( bitVal );
            return;
        }

        crc(bitVal);
        mData = (mData  << 1) | bitVal;
        mCount++;

        if(mCount == 8)
        {
            mDLCTemp--;
            if(mDLCTemp==0)
            {
                toNext( bitVal );
            }
            else if(mDLCTemp > 0)
            {
                toSelf( bitVal );
            }
        }
    }
}

void CCanData::onExit(int /*bitVal*/)
{
    if( mCount > 0 )
    {
        mSpan = mCounter.getDur();

        CDecPayloadData* pData = new CDecPayloadData();
        Q_ASSERT( NULL != pData );
        pData->setPosSpan( mPosition, mSpan );
        pData->setPayload(mData,mCount,COMM_DATA_COLOR,TEXT_DATA);
        AddBlock(CDecPayloadData,pData);


        Q_ASSERT( NULL != mCellData );
        if(mCellData->size() > 1)
        {
            mCellData->append(new CCellStr(1));//add space
        }
        mCellData->append(new CCellDW(mData,mCount));
    }
}

//! CCanCrc
CCanCrc::CCanCrc()
{}
CCanCrc::~CCanCrc()
{}
void CCanCrc::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData = 0;
    mErr = false;
    mCounter.begin();
    mPosition = mCounter.now();
}


void CCanCrc::serialIn( int event, int bitVal )
{
    if( event == SAMP )
    {
        int bit = checkFillBit( bitVal );
        //check bit fill
        if( bit == BitFilled )
        {
            return;
        }
        else if( bit == BitError )
        {
            toIdle( bitVal );
            mpTable->append(new CCellStr(1));//ack
            return;
        }

        mData = (mData  << 1) | bitVal;
        mCount++;
    }

    //with delimiter bit. for bug1950 by hxh
    if( mCount==16 && mEDL == CAN_STD)
    {
        mData  = mData >> 1;
        mCount--;
        if(crc15 != mData)//crc Err
        {
            mErr = true;
        }
        toNext( bitVal );
    }
    else if( mCount == 22 )
    {
        mData  = mData >> 1;
        mCount--;
        toNext( bitVal );
    }
}

void CCanCrc::onExit(int /*bitVal*/)
{
    mSpan = mCounter.getDur();

    CDecPayloadCmd* pCmd = new CDecPayloadCmd();
    Q_ASSERT( NULL != pCmd );
    pCmd->setPosSpan( mPosition, mSpan );
    if(mErr)
    {
        pCmd->setPayload(mData,mCount,ERR_COLOR,TEXT_CRC);
    }
    else
    {
        pCmd->setPayload(mData,mCount,CAN_CRC_COLOR,TEXT_CRC);
    }
    AddBlock(CDecPayloadCmd,pCmd);

    mpTable->append(new CCellInfo(mData,mCount));
}

//! CCanAck
CCanAck::CCanAck()
{}
CCanAck::~CCanAck()
{}
void CCanAck::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData = 0;
    mErr = false;
    mCounter.begin();
    mPosition = mCounter.now();
}


void CCanAck::serialIn( int event, int bitVal )
{
    if( event ==SAMP)
    {
        mData = (mData  << 1) | bitVal;
        mCount++;
        if(mCount == 2)
        {
            //mData  = mData >> 1;
            //mCount = 1;
            toNext( bitVal );//to end
        }
    }
}

/*
 ACK SLOT只有一个BIT，而接下去的ACK delimiter始终为隐性（我们可认为是1），
当数据到达ACK SLOT的时候，所有的节点都会发送显性位（我们可以认为是0），
而发送者在ACK这个时间里保持隐性位（即发送者在发送的时候ACK SLOT为1），
这时发送者会检测总线上的ACK时间内的信号，如果是0，则表示正确，如果是1，表示有错误
 */

void CCanAck::onExit(int /*bitVal*/)
{
    //bug 1488
    if( mRTR != CAN_FRAME_RMT)
    {
        //! export data
        mSpan = mCounter.getDur();

        CDecPayloadString* pCmd = new  CDecPayloadString();
        Q_ASSERT( NULL != pCmd );
        pCmd->setPosSpan( mPosition, mSpan );
        //pCmd->setPayload(mData,mCount,COMM_ACK_COLOR,TEXT_ACK);
        if(mData == 1)
        {
             pCmd->setPayload( COMM_ACK_COLOR, TEXT_ACK);
             mpTable->append(new CCellStr(MSG_DECODE_EVT_ACK));
        }
        else
        {
            pCmd->setPayload( ERR_COLOR, TEXT_ACK);
            mpTable->append(new CCellStr(MSG_DECODE_EVT_ERR));
        }
        AddBlock(CDecPayloadString,pCmd);
    }
    else
    {
        mpTable->append(new CCellStr(1));
    }

}

//! CCanEnd
CCanEnd::CCanEnd()
{}
CCanEnd::~CCanEnd()
{}
void CCanEnd::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData = 0;
    mErr = false;
    mCounter.begin();
    mPosition = mCounter.now();
}


void CCanEnd::serialIn( int event, int bitVal )
{

    switch( event )
    {
    case SAMP:
        mData = (mData  << 1) | bitVal;
        mCount++;
        break;


    case FALL:
        if(mCount == 7)
        {
            toNext( bitVal );
        }
        break;
    default:
        break;
    }
    if(mCount == 8)
    {
        toIdle( bitVal );
    }
}

void CCanEnd::onExit(int /*bitVal*/)
{
    //! export end
    mSpan = mCounter.getDur();

    CDecIcon* pIcon = new CDecIcon();
    Q_ASSERT( NULL != pIcon );
    pIcon->setPosSpan(mPosition, mSpan );
    pIcon->setIcon(ICON_END,CAN_CRC_COLOR);
    AddBlock(CDecIcon,pIcon);
}



CCanError::CCanError()
{}
CCanError::~CCanError()
{}
void CCanError::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData = 0;
    mCounter.begin();
    mPosition = mCounter.now();
}

void CCanError::serialIn( int event, int bitVal )
{
    if( event == SAMP)
    {
        mData = (mData  << 1) | bitVal;
        mCount++;
        if( mCount >= 7 && (mData & CAN_EOF) == CAN_EOF )
        {
            toIdle( bitVal );
        }
    }
}

void CCanError::onExit(int /*bitVal*/)
{
    //! export data
    mSpan = mCounter.getDur();

    CDecPayloadString* pCaption = new CDecPayloadString();
    Q_ASSERT( NULL != pCaption );

    pCaption->setPosSpan( mPosition, mSpan );
    pCaption->setPayload(ERR_COLOR,TEXT_STUFF);
    AddBlock(CDecPayloadString,pCaption);

    //mpTable->append(new CCellInfo(mData,mCount));
}


//! CCanOverload
CCanOverload::CCanOverload()
{}
CCanOverload::~CCanOverload()
{}

void CCanOverload::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData = 0;
    mPosition = mCounter.now();
}

void CCanOverload::serialIn( int event, int bitVal )
{
    switch( event )
    {
    case SAMP:
        mData = (mData  << 1) | bitVal;
        mCount++;
        break;
    case FALL:
        if(mCount == 7)//overload delimiter to overload
        {
            toSelf( bitVal );
        }
        break;
    default:
        break;
    }
    if(mCount == 8)//to interspace
    {
        toIdle(bitVal);
    }
}

void CCanOverload::onExit(int /*bitVal*/)
{
}
