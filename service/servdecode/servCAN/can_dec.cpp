#include "../servdecode.h"
#include "can_fsm.h"


static CCanIdle     fsm_can_idle;
static CCanStart    fsm_can_start;
static CCanArb      fsm_can_arb;
static CCanCtl      fsm_can_ctl;
static CCanData     fsm_can_data;
static CCanCrc      fsm_can_crc;
static CCanAck      fsm_can_ack;
static CCanEnd      fsm_can_end;
static CCanError    fsm_can_err;
static CCanOverload fsm_can_overload;


uint CCanDecoder::mRTR;
uint CCanDecoder::mBRS;
uint CCanDecoder::mEDL;
uint CCanDecoder::mDLC;//data length
uint CCanDecoder::mDLCTemp;
uint CCanDecoder::mIDE;
uint CCanDecoder::mIdentifier;

QList<int> CCanDecoder::mCrcData;
int  CCanDecoder::mCheckStart;
int  CCanDecoder::mFillBit;

uint  CCanDecoder::crc15;
uint  CCanDecoder::crc17;
uint  CCanDecoder::crc21;

CCanCfg* CCanDecoder::mpCanCfg;

CCanDecoder::CCanDecoder()
{
    fsm_can_idle.setNextStat( &fsm_can_start );
    fsm_can_start.setNextStat( &fsm_can_arb );
    fsm_can_arb.setNextStat( &fsm_can_ctl );
    fsm_can_ctl.setNextStat( &fsm_can_data );
    fsm_can_data.setNextStat( &fsm_can_crc );
    fsm_can_crc.setNextStat( &fsm_can_ack );
    fsm_can_ack.setNextStat( &fsm_can_idle );

    fsm_can_end.setNextStat( &fsm_can_overload );
    fsm_can_err.setNextStat( &fsm_can_idle );
    setErr( &fsm_can_err);
    setIdle( &fsm_can_idle );
}
CCanDecoder::~CCanDecoder()
{

}

//! rst to idle
void CCanDecoder::rst()
{
    mpNow = &fsm_can_idle;
    mCounter.rst();
    clearBlock();

    mpCanCfg = &mpCurrServ->mServCan;

    mpTable = new CTable();    
    setEventTable(mpTable);
}

int CCanDecoder::checkFillBit(int bit)
{
    int b = BitNormal;

    mFillBit = mFillBit & 0x1f;

    if( mCheckStart >= 5 &&
      ( mFillBit == 0x1f || mFillBit == 0) )
    {
        b = BitFilled;
        if( (mFillBit & 1) == bit )//
        {
            b = BitError;
        }
    }
    mFillBit = ( mFillBit << 1 ) | bit;
    mCheckStart++;


    return b;
}

void CCanDecoder::crc(int bit)
{
    uint crcNext = (crc15 & 0x4000) != 0;
    crcNext = crcNext ^ bit;

    crc15 = crc15<<1;
    crc15 = crc15 & 0x7ffe;


    if(crcNext)
    {
        crc15 = crc15 ^ 0x4599;
    }
}

bool CCanDecoder::doDecode(CDecBus* bus, CDecBus* zoomBus)
{
    bool ret = false;
    if( decode( mpCanCfg->mSrc) )
    {
        CDecLine* line = new CDecLine();

        //if( mpCurrServ->getLabelOn())
        {
            QString label = "CAN";
            line->setLabel(label);
        }

        mLineView.makeLine(*line, mBlocklist);
        bus->append( line );
        if( zoomBus )
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        ret = true;
    }

    return ret;
}

bool CCanDecoder::decode(Chan src)
{
    unsigned int u8Datas[]={0,0xff};
    unsigned int u32Datas[]={0,0xffffffff};
    unsigned int u32Masks[]={ 0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

    unsigned int u32Dat;
    int last_dat, next_dat;
    int i, j;

    uchar* pStream = mpCurrServ->getChanData( src );
    if( !pStream )
    {
        return false;
    }

    last_dat = ( *pStream & 0x80 ) ? 1 : 0;

    now()->onEnter( last_dat );

    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for ( i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Dat, pStream + i, 4 );
        if( mpCanCfg->mSignalType == SIGTYPE_RX ||
            mpCanCfg->mSignalType == SIGTYPE_CAN_H)
        {
            u32Dat = ~u32Dat;
        }
        // clock in step
        //! no change
        if ( u32Dat == u32Datas[last_dat] )
        {
            if ( mClock.tryTick(32) > 0 )
            {
            }
            else
            {
                i += 4;
                mCounter.count( 32 );
                mClock.tick(32);
                continue;
            }
        }

        //! no change in u8
        if ((u32Dat & (unsigned int )0x0ff) == u8Datas[last_dat])
        {
            if( mClock.tryTick(8) > 0 )
            {
            }
            else
            {
                i += 1;
                mCounter.count(8);
                mClock.tick(8);
                continue;
            }
        }

        //! change in 8bits
        for ( j = 0; j < 8; j++ )
        {
            next_dat = u32Dat & u32Masks[j] ? 1 : 0;
            //! rise
            if ( next_dat > last_dat )
            {
                now()->serialIn( RISE, next_dat );
                mClock.sync();
            }
            else if ( next_dat < last_dat )
            {
                now()->serialIn( FALL, next_dat );
                mClock.sync();
            }
            //! timeout
            if ( mClock.tick() > 0 )
            {
                now()->serialIn( SAMP, next_dat );
            }
            last_dat = next_dat;
            mCounter.count();
        }
        i += 1;
    }

    return true;
}

int CCanDecoder::setEventTable(CTable* table)
{
    //! head
    QList<quint32> lstHead;
    lstHead.append( MSG_DECODE_EVT_TIME );
    lstHead.append( MSG_DECODE_EVT_ID );
    lstHead.append( MSG_DECODE_EVT_DLC );
    lstHead.append( MSG_DECODE_EVT_DATA );
    lstHead.append( MSG_DECODE_EVT_CRC );
    lstHead.append( MSG_DECODE_EVT_ACK );

    table->setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 30 );
    lstLayout.append( 80 );
    lstLayout.append( 100 );
    lstLayout.append( 50 );
    lstLayout.append( 200 );
    lstLayout.append( 70 );
    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_CAN);
    table->setTitle( pTitle );
    return ERR_NONE;
}
