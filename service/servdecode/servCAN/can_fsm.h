#ifndef DECCAN
#define DECCAN

#include "can_cfg.h"
#include "../decoder.h"
#include "../decTool/DecState.h"

using namespace DsoType;


#define ERRFLAG             3

#define CAN_EOF             (0x3F)

#define CAN_STD             (0)
#define CAN_EXT             (1)

//CAN with Flexible Data-rate
#define CAN_FD              (1)

#define DOMINANT            (0)
#define RECESSIVE           (1)

#define CAN_FRAME_DATA      (0)
#define CAN_FRAME_RMT       (1)

enum
{
    BitNormal,
    BitFilled,
    BitError
};

class CCanDecoder: public CDecoder,public CDecState
{
public:
    CCanDecoder();
    virtual ~CCanDecoder();

public:
   bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
   void rst();
   int  setEventTable(CTable* table);

public:
    static CCanCfg* mpCanCfg;

    static uint mIdentifier;
    static uint mRTR;
    static uint mBRS;
    static uint mEDL;
    static uint mDLC;//data length
    static uint mDLCTemp;
    static uint mIDE;
    static QList<int> mCrcData;

    static uint  crc15;
    static uint  crc17;
    static uint  crc21;

    static int  mFillBit;
    static int  mCheckStart;

    static int  checkFillBit(int);
    static void crc(int);
private:
    bool decode(Chan);
};

class CCanIdle : public CCanDecoder
{
public:
    CCanIdle();
    virtual ~CCanIdle();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
private:
    bool bEOF;
};

class CCanStart : public CCanDecoder
{
public:
    CCanStart();
    virtual ~CCanStart();


public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit(int bitVal);
};

//Arbitration field. with ext,fd
class CCanArb: public CCanDecoder
{
public:
    CCanArb();
    virtual ~CCanArb();

protected:
    uint mR;
    uint mESI;
    bool mErr;
    uint mExtCount;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );

    void serialInStd( int bitVal );
    void serialInExt( int bitVal );
};

class CCanCtl: public CCanDecoder
{
public:
    CCanCtl();
    virtual ~CCanCtl();

protected:
    bool mErr;
public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CCanData : public CCanDecoder
{
public:
    CCanData();
    virtual ~CCanData();

protected:
    uint mDataLen;
    bool mErr;
public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CCanCrc : public CCanDecoder
{
public:
    CCanCrc();
    virtual ~CCanCrc();
protected:
    bool mErr;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CCanAck : public CCanDecoder
{
public:
    CCanAck();
    virtual ~CCanAck();
protected:
    bool mErr;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CCanEnd : public CCanDecoder
{
public:
    CCanEnd();
    virtual ~CCanEnd();
protected:
    bool mErr;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CCanError : public CCanDecoder
{
public:
    CCanError();
    virtual ~CCanError();

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CCanOverload : public CCanDecoder
{
public:
    CCanOverload();
    virtual ~CCanOverload();

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

#endif // DECCAN

