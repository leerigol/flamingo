#ifndef CANCFG
#define CANCFG

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"


using namespace DsoType;

enum
{
    SIGTYPE_CAN_H,
    SIGTYPE_CAN_L,
    SIGTYPE_RX,
    SIGTYPE_TX,
    SIGTYPE_CSNZ_H_L,
};
struct CCanCfg
{
    Chan mSrc;
    int mBaud;
    int mFDBaud;
    int mSignalType;
    int mSamplePerBaud;
    int mFDClkPeriod;
    int mSamplePos;

    void  reset();
    void  serialIn(CStream&);
    void  serialOut(CStream&);

    int   getSamplePerBaud(void);
    int   getFDClkPeriod(void);
    void  setSamplePerBaud(qint64 sample); //sample / baud

};
#endif // CANCFG

