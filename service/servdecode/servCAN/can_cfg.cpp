#include "can_cfg.h"
#include "../servdecode.h"

void CCanCfg::reset()
{
    mSrc        =   chan1;
    mBaud       =   BaudRate_1Mbps;
    mFDBaud     =   BaudRate_10Mbps;
    mSignalType =   SIGTYPE_CAN_L;
    mSamplePos  =   50;
}


void CCanCfg::serialIn(CStream &stream)
{
    int temp = 0;
    stream.read("mSrc",        temp);
    mSrc = (Chan)temp;
    stream.read("mBaud",        mBaud);
    stream.read("mFDBaud",      mFDBaud);
    stream.read("mSignalType",  mSignalType);
    stream.read("mSamplePos",   mSamplePos);
}

void CCanCfg::serialOut(CStream &stream)
{
    stream.write("mSrc",        (int)mSrc);
    stream.write("mBaud",       mBaud);
    stream.write("mFDBaud",     mFDBaud);
    stream.write("mSignalType", mSignalType);
    stream.write("mSamplePos",  mSamplePos);
}

void CCanCfg::setSamplePerBaud(qint64 sample)
{
    if( mBaud > 0)
    {
        mSamplePerBaud = sample / mBaud;
    }
    else
    {
        mSamplePerBaud = 8;
    }

    if( mSamplePerBaud < 3)
    {
        mSamplePerBaud = 3;
    }

    ////////////////CAN-FD/////////////////////
    if(mFDBaud > 0)
    {
        mFDClkPeriod = sample / mFDBaud;
    }
    else
    {
        mFDClkPeriod = 8;
    }

    if( mFDClkPeriod < 3)
    {
        mFDClkPeriod = 3;
    }

}

int CCanCfg::getSamplePerBaud()
{
    return mSamplePerBaud;
}

int CCanCfg::getFDClkPeriod()
{
    return mFDClkPeriod;
}

DsoErr servDec::setCanSrc(Chan src)
{
    mServCan.mSrc = src;
    if(src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_CAN_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_CAN_THRE, true);
        setuiChange( MSG_DECODE_CAN_THRE );
    }

    return ERR_NONE;
}

Chan servDec::getCanSrc()
{
    return mServCan.mSrc;
}


DsoErr servDec::setCanBaud(int baud)
{
    mServCan.mBaud = baud;
    return ERR_NONE;
}

int servDec::getCanBaud()
{
    setuiBase( 1, E_0 );
    setuiMinVal( 10000 );
    setuiZVal(1000000);
    setuiMaxVal( 5000000 );
    setuiPostStr( "bps" );

    return mServCan.mBaud;
}

DsoErr servDec::setCanFDBaud(int baud)
{
    mServCan.mFDBaud = baud;
    return ERR_NONE;
}

int servDec::getCanFDBaud()
{
    setuiBase( 1, E_0 );
    setuiMaxVal( 1000000 );
    setuiMaxVal( 10000000 );
    setuiZVal(1000000);
    setuiPostStr( "bps" );
    return mServCan.mFDBaud;
}


DsoErr servDec::setCanSignalType(int type)
{
    mServCan.mSignalType = type;
    return ERR_NONE;
}

int servDec::getCanSignalType()
{
    return mServCan.mSignalType;
}


DsoErr servDec::setCanThreAbsPre(qint64 t)
{
    setChThreshold(mServCan.mSrc,t);
    return ERR_NONE;
}

qint64 servDec::getCanThreAbsPre()
{    
    Chan s = mServCan.mSrc;

    return getChThreshold(s);
}

DsoErr servDec::setCanSamplePos(int s)
{
    mServCan.mSamplePos = s;
    return ERR_NONE;
}

int servDec::getCanSamplePos()
{
    setuiBase( 1, E_0 );
    setuiMaxVal(90);
    setuiMinVal(10);
    setuiZVal(50);
    setuiPostStr( "%" );
    return mServCan.mSamplePos;
}



