#ifndef CAN_MAP
#define CAN_MAP

#include "../servdecode/servdecode.h"


on_set_int_int  (MSG_DECODE_CAN_SRC,        &servDec::setCanSrc),
on_get_int      (MSG_DECODE_CAN_SRC,        &servDec::getCanSrc),

on_set_int_int  (MSG_DECODE_CAN_SIGNAL,     &servDec::setCanSignalType),
on_get_int      (MSG_DECODE_CAN_SIGNAL,     &servDec::getCanSignalType),

on_set_int_ll   (MSG_DECODE_CAN_THRE,       &servDec::setCanThreAbsPre),
on_get_ll       (MSG_DECODE_CAN_THRE,       &servDec::getCanThreAbsPre),

on_set_int_int  (MSG_DECODE_CAN_BAUD,       &servDec::setCanBaud),
on_get_int      (MSG_DECODE_CAN_BAUD,       &servDec::getCanBaud),

on_set_int_int  (MSG_DECODE_CANFD_BAUD,     &servDec::setCanFDBaud),
on_get_int      (MSG_DECODE_CANFD_BAUD,     &servDec::getCanFDBaud),

on_set_int_int  (MSG_DECODE_CAN_SAMP,       &servDec::setCanSamplePos),
on_get_int      (MSG_DECODE_CAN_SAMP,       &servDec::getCanSamplePos),




#endif // CAN_MAP

