#ifndef DECODER_H
#define DECODER_H


#include <QMap>
#include "./servBlock/DecBus.h"
#include "./servBlock/DecBlock.h"
#include "eventTable/ctable.h"
#include "eventTable/ccellstr.h"
#include "../../service/service_msg.h"
#include "./decTool/Clock.h"
#include "./decTool/Counter.h"
#include "./servBlock/DecPayload.h"
#include "./servBlock/DecIcon.h"
#include "./decTool/decview.h"
#include "servdecode.h"


/***********************************************
 *!!!!!! Limit the count of the Decoder result!!!!!!
************************************************/
#define AddOneFrame()          {mFrameCount++;}
#define AddBlock(name,obj)  {   if(mBlocklist.size() < LIMIT_RESULT_SIZE ) \
                                {\
                                    mBlocklist.append(obj);\
                                    if(mpCurrServ->m_nViewMode == screen_yt_main_zoom) \
                                    { \
                                       name *newObj = new name;\
                                       *newObj = *obj;\
                                       mZoomBlocklist.append(newObj);\
                                    }\
                                 }\
                             }

class servDec;
class CLineView;
class CBlockFilter;

class CDecoder
{
public:
   CDecoder();
   ~CDecoder();

   static CDecoder* buildDecoder(servDec*);
   static CTable*   mergeEvt(CTable*, CTable*, CTable*);
   static void      clearBlock();

   virtual void     rst() = 0;
   virtual bool     doDecode(CDecBus*, CDecBus* zoomBus = NULL) = 0;
   virtual int      setEventTable(CTable *table) = 0;
           int      bitOrder(int data, int width);

   static servDec*     mpCurrServ;
   static CLineView    mLineView;
   static CLineView    mLineViewZoom;

public:
    static uint mData;
    static uint mLatch;
    static uint mCount;
    static uint mPosition;
    static uint mSpan;
    static QList<CDecBlock*> mBlocklist;
    static QList<CDecBlock*> mZoomBlocklist;
    static uint  mFrameCount;

    static CCounter  mCounter;
    static CClock    mClock;
    static CTable*   mpTable;
    static CCellDW*  mCellData;

    static uint m8IdleMask[];
    static uint m32IdleMask[];
    static uint m32Masks[];

private:
   static QMap<int,CDecoder*> mDecoders;

};

#endif // DECODER

