#include "servdecode.h"
#include "../servch/servch.h"

//for copyTrig
void servDec::configThreshold(Chan ch, qint64 a)
{
    QString name = QString("chan%1").arg( ch );
    int    nScale = 0;
    //int    nOffset = 0;
    serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);    
    //serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);
    //a = a - nOffset;

    qint64 b = a +nScale*3/10;

    //! 高阈值在前，低阈值在后
    postEngine( ENGINE_DECODE_THRESHOLD,ch,b,a);
}

/* by menu */
DsoErr servDec::setChThreshold(Chan ch, qint64 value, bool cfg)
{
    float fRatio = 1.0;

    if( (chan1 <= ch) && ( ch <=  chan4) )
    {
        QString name = QString("chan%1").arg( ch );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);

        m_nCurrentChan = ch;
        m_nCurrentThre = value;
        m_Threshold[ch] = value / fRatio;

        if( cfg )
        {
            configThreshold(ch, m_Threshold[ch] );
        }
    }
    return ERR_NONE;
}

qint64 servDec::getChThreshold(Chan ch)
{
    float  fRatio = 1.0;
    int    nScale = 0;
    int    nOffset= 0;

    qint64 value = 0;
    if( (ch >= chan1) && ( ch <=  chan4) )
    {
        QString name = QString("chan%1").arg( ch );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);

        Q_ASSERT(fRatio != 0);

        serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);
        serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);

        nScale = nScale * fRatio;
        nOffset= nOffset* fRatio;
        setuiStep( (qlonglong)(nScale / adc_vdiv_dots) );

        value  = m_Threshold[ch] * fRatio;
    }

    setuiZVal(0);
    setuiMinVal( -5*nScale - nOffset );
    setuiMaxVal(  5*nScale - nOffset );
    setuiUnit(Unit_V);
    setuiBase( ch_volt_base );
    setuiPostStr("V");

    return value;
}

//for source change
qint64 servDec::getChThreshold1(Chan ch)
{
    m_nCurrentChan = ch;
    m_nCurrentThre = getValidThreshold(ch);
    return m_nCurrentThre;
}

qint64 servDec::getValidThreshold(Chan ch)
{
    float  fRatio = 1.0;
    int    nScale = 0;
    int    nOffset= 0;

    qint64 value = 0;
    if( (ch >= chan1) && ( ch <=  chan4) )
    {
        QString name = QString("chan%1").arg( ch );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);

        Q_ASSERT(fRatio != 0);

        serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);
        serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);

        nScale = nScale * fRatio;
        nOffset= nOffset* fRatio;

        value  = m_Threshold[ch] * fRatio;

        qint64 min = -5*nScale - nOffset;
        qint64 max =  5*nScale - nOffset;
        if( value > max )
        {
            value = max;
        }
        if( value < min )
        {
            value = min;
        }
        return value;
    }

    qDebug()<< "Can't come here for decoder threshold";
    return -1;
}

DsoErr servDec::setChThresholdL(Chan ch, qint64 value)
{
    float fRatio = 1.0;
    int nScale  = 0;
    int nOffset = 0;

    if( (chan1 <= ch) && ( ch <=  chan4) )
    {
        QString name = QString("chan%1").arg( ch );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);


        serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);
        serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);

        nOffset = nOffset * fRatio;
    }
    else
    {
        return ERR_NONE;
    }


    m_nCurrentChan = ch;
    m_nCurrentThre = value;

    int t = value;
    value = value + nOffset;
    Q_ASSERT(fRatio != 0);
    value = (qint64)(value/fRatio);


    m_ThresholdL[ch] = value ;

    t = t / fRatio;

    qint64 a = t;// -nScale*0.30;
    qint64 b = t +nScale*0.30;
    //qDebug() << "a=" << a << "b=" << b << value << nScale*0.25;

    //! 高阈值在前，低阈值在后
    postEngine( ENGINE_DECODE_THRESHOLD_L,
                ch,
                b,
                a);

    return ERR_NONE;
}

qint64 servDec::getChThresholdL(Chan ch)
{
    float  fRatio = 1.0;
    int    nScale = 0;
    int    nOffset= 0;

    if( (chan1 <= ch) && ( ch <=  chan4) )
    {
        QString name = QString("chan%1").arg( ch );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);

        Q_ASSERT(fRatio != 0);

        serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);
        serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);

        nScale = nScale * fRatio;
        nOffset= nOffset* fRatio;
        setuiStep( (qlonglong)(nScale / adc_vdiv_dots) );
    }
    else
    {
        //qDebug() << "la get threashold";
        //Q_ASSERT( false );
        return 0;
    }

    qint64 value  = m_ThresholdL[ch] * fRatio;


    setuiZVal(0);
    setuiMinVal( -5*nScale - nOffset );
    setuiMaxVal(  5*nScale - nOffset );
    setuiUnit(Unit_V);
    setuiBase( ch_volt_base );
    setuiPostStr("V");

    return value - nOffset;
}

Chan servDec::getThreCh(DecThreType tch)
{
    Chan ch = chan_none;

    switch (tch)
    {
    case  thre_pal://! abs
        ch = (Chan)getPalCurCh();
        break;

    case  thre_tx: //!rs232
        ch = getUartTxSrc();
        break;
    case  thre_rx:
        ch = getUartRxSrc();
        break;

    case  thre_scl: //!i2c
        ch = getI2cClkSrc();
        break;
    case  thre_sda:
        ch = getI2cDatSrc();
        break;

    case  thre_cs://!spi
        ch = getSpiCsSrc();
        break;
    case  thre_clk:
        ch = getSpiClkSrc();
        break;
    case  thre_miso:
        ch = getSpiMisoSrc();
        break;
    case  thre_mosi:
        ch = getSpiMosiSrc();
        break;

    case  thre_lin://!lin
        ch = getLinSrc();
        break;

    case  thre_can: //!can
        ch = getCanSrc();
        break;
    case  thre_can_sub_1:
        ch = getCanSrc();
        break;

    default:
        ch = chan_none;
        break;
    }

    return ch;
}

DsoErr servDec::setDecThre(CArgument argi)
{
    if(argi.size() != 2)
    {
        return ERR_INVALID_INPUT;
    }

    qint64      threValue   = argi[0].llVal;
    DecThreType threType    = (DecThreType)argi[1].iVal;

    float  fRatio = 1.0;
    int    nScale = 0;
    int    nOffset= 0;
    Chan ch = getThreCh(threType);
    if( (ch >= chan1) && ( ch <=  chan4) )
    {
        QString name = QString("chan%1").arg( ch );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);

        Q_ASSERT(fRatio != 0);

        serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);
        serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);

        nScale = nScale * fRatio;
        nOffset= nOffset* fRatio;
        setuiStep( (qlonglong)(nScale / adc_vdiv_dots) );

        if(threValue<= -5*nScale - nOffset)
        {
            threValue = -5*nScale - nOffset;
        }
        else if(threValue>=5*nScale - nOffset)
        {
            threValue = 5*nScale - nOffset;
        }
    }

    DsoErr err = setChThreshold(getThreCh(threType), threValue);
    updateAllUi();
    return err;
}

void servDec::getDecThre(CArgument argi, CArgument argo)
{
    if(argi.size() != 1)
    {
        return;
    }

    DecThreType threType    = (DecThreType)argi[0].iVal;
    qint64      threValue   = getChThreshold( getThreCh(threType) );

    argo.setVal(threValue);
}

void servDec::getChanParam(CArgument &arg)
{
    arg.setVal( m_nCurrentChan, 0);
    arg.setVal( m_nCurrentThre, 1);
}

#define NEW_LINE  ",\n"
#define SEPARATOR ","
void servDec::getEvtData( CArgument &arg )  //qxl 2018.2.6
{
    QByteArray arrayFile;
    QString cellstr ;
    QString decName;
    CCellList *pHeadList,*pDataList;
    CCell *pDataCell,*pHeadCell ;

    CTable * pTable = m_pEvt;
    if ( NULL != pTable )
    {
        int i=0;
        int colum = pTable->column();
        int cellCnts = pTable->getSheet()->size();
        arrayFile.reserve(cellCnts*2 + 1);

        decName = sysGetString(pTable->getTitle()->getData());
        arrayFile.append(decName.toUpper());

        pHeadList = pTable->getHead();
        pDataList = pTable->getSheet();
        pDataCell = pDataList->head();
        pHeadCell = pHeadList->head();
        i = 0;
        while( pHeadCell != NULL)
        {
            cellstr = "";
            if(i % colum == 0)
            {
                arrayFile.append(NEW_LINE);
            }
            pHeadCell->format( pTable->getContext(), cellstr );
            arrayFile.append(cellstr);
            arrayFile.append(SEPARATOR);
            pHeadCell = pHeadCell->next();
            i++;
        }
        i = 0;
        while( pDataCell != NULL)
        {
            cellstr = "";
            if(i % colum == 0)
            {
                arrayFile.append(NEW_LINE);
            }
            pDataCell->format( pTable->getContext(), cellstr );
            arrayFile.append(cellstr);
            arrayFile.append(SEPARATOR);
            pDataCell = pDataCell->next();
            i++;
        }
    }

    if(arrayFile.size())
    {
        ArbBin abin( arrayFile.size(),
                     arrayFile.data() );

        arg.setVal( abin );
    }
}
