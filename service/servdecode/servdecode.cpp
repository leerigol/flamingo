#include <QTime>
#include "servdecode.h"
#include "../servhori/servhori.h"
#include "../servstorage/servstorage.h"
#include "../servtrig/servtrig.h"
#include "../servdso/servdso.h"
#include "../../baseclass/resample/memView.h"
#include "./decTool/dectablesave.h"
#include "../service_name.h"
#include "decoder.h"

/*! \var 定义消息映射表
* 定义消息映射表
*/

IMPLEMENT_CMD( serviceExecutor, servDec )
start_of_entry()

//on_set_int_int  (CMD_SERVICE_ACTIVE,    &servDec::setActive),
on_get_bool     (CMD_SERVICE_ACTIVE,    &servDec::isActive),
on_set_void_void(CMD_SERVICE_INIT,      &servDec::init ),
on_set_void_void(CMD_SERVICE_RST,       &servDec::rst ),
on_set_int_void (CMD_SERVICE_STARTUP,   &servDec::startup ),
on_set_int_void (CMD_SERVICE_VIEW_CHANGE,&servDec::onViewChanged),
on_set_int_int  (CMD_SERVICE_SUB_RETURN, &servDec::onReturnMenu),

on_set_int_void (CMD_SERVICE_ENTER_ACTIVE,&servDec::on_enterActive),
on_set_int_int  (CMD_SERVICE_EXIT_ACTIVE ,&servDec::on_exitActive),

on_set_int_bool (MSG_DECODE_ONOFF,      &servDec::setBusOn),
on_get_bool     (MSG_DECODE_ONOFF,      &servDec::getBusOn),

on_set_int_int  (MSG_DECODE_TYPE,       &servDec::setDecodeType),
on_get_int      (MSG_DECODE_TYPE,       &servDec::getDecodeType),

on_set_int_int  (MSG_DECODE_POS,        &servDec::setBusOffset),
on_get_int      (MSG_DECODE_POS,        &servDec::getBusOffset),

on_set_int_int  (MSG_DECODE_FORMAT,     &servDec::setBusFormat),
on_get_int      (MSG_DECODE_FORMAT,     &servDec::getBusFormat),

on_set_int_void (MSG_DECODE_COPY_TRIG,  &servDec::copyTrig),

/*event & config*/
on_set_int_bool (MSG_DECODE_LABEL,      &servDec::setLabelOn),
on_get_bool     (MSG_DECODE_LABEL,      &servDec::getLabelOn),


on_set_int_bool (MSG_DECODE_EVT,        &servDec::setEventOn),
on_get_bool     (MSG_DECODE_EVT,        &servDec::getEventOn),

on_set_int_int  (MSG_DECODE_EVT_FORMAT, &servDec::setEventFormat),
on_get_int      (MSG_DECODE_EVT_FORMAT, &servDec::getEventFormat),

on_set_int_int  (MSG_DECODE_EVT_VIEW,   &servDec::setEventViewMode),
on_get_int      (MSG_DECODE_EVT_VIEW,   &servDec::getEventViewMode),

on_set_int_int  (MSG_DECODE_EVT_BUSX,   &servDec::setEventBusX),
on_get_int      (MSG_DECODE_EVT_BUSX,   &servDec::getEventBusX),

on_set_int_void (MSG_DECODE_EVT_EXPORT, &servDec::exportEventData),
on_set_int_void (MSG_DECODE_EVT_JUMP,   &servDec::setJumpEvent),

//spy on
on_set_int_void (MSG_HORI_ZOOM_SCALE,   &servDec::onZoomScale),
on_set_int_void (MSG_HORI_ZOOM_OFFSET,  &servDec::onZoomOffset),

on_set_int_void (MSG_TRIGGER_SWEEP,     &servDec::onTriggerSweep),
on_set_int_void (MSG_DISPLAY_CLEAR,     &servDec::onDispClear),
on_set_void_void( MSG_HORIZONTAL_RUN,   &servDec::onRunStop),

on_set_int_pointer(servDec::cmd_trace_ready,     &servDec::onTraceReady),
on_set_int_void (servDec::cmd_bus_chan1_change,  &servDec::onChan1Change),
on_set_int_void (servDec::cmd_bus_chan2_change,  &servDec::onChan2Change),
on_set_int_void (servDec::cmd_bus_chan3_change,  &servDec::onChan3Change),
on_set_int_void (servDec::cmd_bus_chan4_change,  &servDec::onChan4Change),

on_set_int_void (servDec::cmd_bus_license,       &servDec::onCheckLicense),

//! service
on_set_void_void(servDec::cmd_bus_completed, &servDec::setBusCompleted ),
on_get_bool     (servDec::cmd_bus_completed, &servDec::getBusCompleted ),

on_get_pointer( servDec::cmd_bus_data,      &servDec::getBus ),
on_get_pointer( servDec::cmd_bus_zoom_data, &servDec::getZoomBus ),

on_get_pointer( servDec::cmd_bus_evt,       &servDec::getEvtTable ),

#include "./servPal/pal_map.h"

/*Uart*/
#include "./servUart/uart_map.h"

/*I2C*/
#include "./servI2C/i2c_map.h"

/*SPI*/
#include "./servSPI/spi_map.h"

/*LIN*/
#include "./servLin/lin_map.h"

/*Can*/
#include "./servCAN/can_map.h"

/*Flexray*/
#include "./servFlexray/flex_map.h"

/* I2S */
#include "./servI2S//i2s_map.h"

/* 1553B */
#include "./serv1553B/1553b_map.h"

on_get_int (    servDec::cmd_dec_evt_id,       &servDec::getDecId),//qxl

/*SCPI*/
on_set_int_arg       (servDec::cmd_bus_src_thre, &servDec::setDecThre),
on_get_void_argi_argo(servDec::cmd_bus_src_thre, &servDec::getDecThre),
on_get_void_argo     (servDec::cmd_bus_src_param,&servDec::getChanParam),

on_get_void_argo(     servDec::cmd_bus_evt_data, &servDec::getEvtData ),

on_set_int_string(    servDec::cmd_bus_evt_save, &servDec::saveEvtData ),

on_set_int_int  (     servDec::cmd_bus_position, &servDec::setBusScpiPos),

end_of_entry()

serviceKeyMap servDec::m_WaveKeyMap;
//! heap Mgr
CHeapMgr servDec::EvtHeapMgr;


QMap<Chan,qint64>   servDec::m_Threshold;
QMap<Chan,qint64>   servDec::m_ThresholdL;
QMap<Chan,DsoWfm*>  servDec::m_Streams;

static CDecoderThread* m_pDecThread = NULL;

bool    servDec::m_bShowEvt;
bool    servDec::m_bShowLabel;
int     servDec::m_nEventFormt;
int     servDec::m_nFormat;
int     servDec::m_nEvtViewMode;
int     servDec::m_nEvtViewBusX;
int     servDec::m_nEvtBusXList;
QSemaphore servDec::mTraceSem;

int      servDec::m_nStreamSize  = 0;
int      servDec::m_nLed         = 0;
bool     servDec::m_bWithMSO     = false;
int      servDec::m_nCurrentChan = 0;
qint64   servDec::m_nCurrentThre = 0;
qint64   servDec::m_nSampleRate  = 0;
qint64   servDec::m_nLaSampleRate= 0;
double   servDec::m_timeInc      = 0.0;
double   servDec::m_timeStart    = 0.0;
int      servDec::m_nDecId       = 0;


servDec::servDec( QString name, ServiceId eId ) : serviceExecutor(name, eId )
{
    mVersion = 6;
    m_pEvt   = NULL;

    if( m_pDecThread == NULL )
    {
        //! worker thread
        m_pDecThread = new CDecoderThread( this );        
        Q_ASSERT( NULL != m_pDecThread );
    }

    m_pDecThread->addDecoder(this);
}

servDec::~servDec()
{
    int i;
    CHeap *pHeap;

    for ( i = 0; i < servDec::EvtHeapMgr.getHeapCount(); i++ )
    {
        pHeap = servDec::EvtHeapMgr.getHeap( i );
        Q_ASSERT( NULL != pHeap );

        Q_ASSERT( NULL != pHeap->mem() );
        delete [] ( (quint32*)pHeap->mem() );
    }
    servDec::EvtHeapMgr.clearHeap();
}

DsoErr servDec::start()
{
    if( getId() == E_SERVICE_ID_DEC4)
    {
        if( m_pDecThread && !m_pDecThread->isRunning())
        {
            m_pDecThread->start();
        }
    }
    return ERR_NONE;
}

void servDec::registerSpy()
{
    spyOn( serv_name_hori, MSG_HORI_ZOOM_SCALE );
    spyOn( serv_name_hori, MSG_HORI_ZOOM_OFFSET );

    spyOn( serv_name_trigger,MSG_TRIGGER_SWEEP );
    spyOn( serv_name_display, MSG_DISPLAY_CLEAR);

    spyOn( serv_name_ch1,  MSG_CHAN_SCALE,cmd_bus_chan1_change);
    spyOn( serv_name_ch2,  MSG_CHAN_SCALE,cmd_bus_chan2_change);
    spyOn( serv_name_ch3,  MSG_CHAN_SCALE,cmd_bus_chan3_change);
    spyOn( serv_name_ch4,  MSG_CHAN_SCALE,cmd_bus_chan4_change);

    spyOn( serv_name_ch1,  MSG_CHAN_OFFSET,cmd_bus_chan1_change);
    spyOn( serv_name_ch2,  MSG_CHAN_OFFSET,cmd_bus_chan2_change);
    spyOn( serv_name_ch3,  MSG_CHAN_OFFSET,cmd_bus_chan3_change);
    spyOn( serv_name_ch4,  MSG_CHAN_OFFSET,cmd_bus_chan4_change);

    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Active , cmd_bus_license);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Exp    , cmd_bus_license);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Invalid, cmd_bus_license);

    if( this->getId() == E_SERVICE_ID_DEC1 )
    {
        serviceExecutor::spyOn( E_SERVICE_ID_TRACE,
                                servTrace::cmd_set_provider,
                                mpItem->servId,
                                cmd_trace_ready );

        //! do not change setting
        setMsgAttr( cmd_trace_ready );
    }

}

int servDec::serialOut( CStream &stream, serialVersion &ver )
{
    ver = mVersion;

    //QString name = getName();

    stream.write("bus",     m_bBusOn);
    stream.write("pos",     m_nPosition);
    stream.write("type",    m_nBusType);


    //share the threshold for all decoders
    if( this->getName().compare( serv_name_decode1) == 0 )
    {
        stream.write("format",          m_nFormat);
        stream.write("event_format",    m_nEventFormt);
        stream.write("label",           m_bShowLabel);
        stream.write("event",           m_bShowEvt);

        stream.write("threshold1",     m_Threshold[chan1]);
        stream.write("threshold2",     m_Threshold[chan2]);
        stream.write("threshold3",     m_Threshold[chan3]);
        stream.write("threshold4",     m_Threshold[chan4]);
    }

    mServPal.serialOut(stream);
    mServUart.serialOut(stream);
    mServI2C.serialOut(stream);
    mServSPI.serialOut(stream);
    mServCan.serialOut(stream);
    mServLin.serialOut(stream);
    mServI2S.serialOut(stream);
    mServ1553B.serialOut(stream);

    return 0;
}
int servDec::serialIn( CStream &stream, serialVersion ver )
{
    if( mVersion == ver)
    {
        stream.read("bus",    m_bBusOn);
        stream.read("pos",    m_nPosition);
        stream.read("type",   m_nBusType);

        if( this->getName().compare( serv_name_decode1) == 0 )
        {            
            stream.read("format",         m_nFormat);
            stream.read("event_format",   m_nEventFormt);
            stream.read("label",          m_bShowLabel);
            stream.read("event",          m_bShowEvt);

            qint64 h=0;
            stream.read("threshold1",     h);
            m_Threshold[chan1] = h;

            stream.read("threshold2",     h);
            m_Threshold[chan2] = h;

            stream.read("threshold3",     h);
            m_Threshold[chan3] = h;

            stream.read("threshold4",     h);
            m_Threshold[chan4] = h;
        }
        mServPal.serialIn(stream);
        mServUart.serialIn(stream);
        mServI2C.serialIn(stream);
        mServSPI.serialIn(stream);
        mServCan.serialIn(stream);
        mServLin.serialIn(stream);
        mServI2S.serialIn(stream);
        mServ1553B.serialIn(stream);
    }
    else
    {
        rst();
    }
    return 0;
}

void servDec::switchHeap()
{
    static int index = 0;
    //! get heap
    CHeap *pHeap = servDec::EvtHeapMgr.getHeap( index );
    Q_ASSERT( NULL != pHeap );

    //! set heap again
    CTableHeap::setHeap( pHeap->mem(),pHeap->size() );

    index = !index;
}

void servDec::init()
{
    //! attach heap
    if ( servDec::EvtHeapMgr.getHeapCount() < 2 )
    {
        void *pBuf = new quint32[ servDec::EvtHeapSize / 4 ];
        Q_ASSERT( NULL != pBuf );

        servDec::EvtHeapMgr.attachHeap( pBuf, servDec::EvtHeapSize );

        pBuf = new quint32[ servDec::EvtHeapSize / 4 ];
        Q_ASSERT( NULL != pBuf );
        servDec::EvtHeapMgr.attachHeap( pBuf, servDec::EvtHeapSize );
    }

    if( m_WaveKeyMap.size() == 0)
    {
        addWaveKeys();
    }
}

void servDec::setLASource()
{
    m_bWithMSO = sysCheckLicense(OPT_MSO);

    for(int src=d0; src<=d15; src++)
    {
        mUiAttr.setEnableVisible(MSG_DECODE_PAL_CLK, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_BUS_CH, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_RS232_TX, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_RS232_RX, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_I2C_SCL, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_I2C_SDA, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_SPI_CS, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_SPI_CLK, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_SPI_MISO, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_SPI_MOSI, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_LIN_SRC, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_CAN_SRC, src, m_bWithMSO,m_bWithMSO);

        mUiAttr.setEnableVisible(MSG_DECODE_I2S_WS, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_I2S_SCLK, src, m_bWithMSO,m_bWithMSO);
        mUiAttr.setEnableVisible(MSG_DECODE_I2S_DATA, src, m_bWithMSO,m_bWithMSO);

        mUiAttr.setEnableVisible(MSG_DECODE_FLEX_SRC, src, m_bWithMSO,m_bWithMSO);
    }


    //special for pal
    if(mServPal.mDataWid == 1)
    {
        if( mServPal.mBusWidth != USER)
        {
            mUiAttr.setEnable(MSG_DECODE_BUS_WIDTH, false);
        }
        mUiAttr.setEnable(MSG_DECODE_BUS_BITX, false);

        bool b = mServPal.mBusWidth > CH4;
        int  j = chan4;
        if( m_bWithMSO )
        {
            j = d15;
        }
        for(int i=chan1; i<=j; i++)
        {
            mUiAttr.setEnable(MSG_DECODE_BUS_CH, i, b);
        }
    }
}

void servDec::rst()
{
    //! common
    //! cfg

    if( this->getName().compare( serv_name_decode1) == 0 )
    {
        m_bShowLabel  = true;
        m_bShowEvt    = false;
        m_nEventFormt = F_HEX;
        m_nFormat     = F_HEX;
        m_nEvtViewMode= 0;
        m_nEvtViewBusX= 0;
        m_nEvtBusXList= 0;


        if( mUiAttr.getItem(MSG_APP_DECODE1)->getEnable() == false)
        {
            mUiAttr.setEnable(MSG_APP_DECODE1, true);
            mUiAttr.setEnable(MSG_APP_DECODE2, true);
            mUiAttr.setEnable(MSG_APP_DECODE3, true);
            mUiAttr.setEnable(MSG_APP_DECODE4, true);
        }
    }

    m_nPosition   = 0;
    m_bBusOn      = false;
    m_bSuccess    = false;
    m_nBusType    = Bus_Parallel;
    m_nBusIndex   = BUS_READY;

    m_nViewMode   = screen_yt_main;

    m_n64MainOffset= 0;
    m_n64MainScale = time_us(1);

    m_Threshold[chan1] = 0;
    m_Threshold[chan2] = 0;
    m_Threshold[chan3] = 0;
    m_Threshold[chan4] = 0;

    m_ThresholdL[chan1] = 100;
    m_ThresholdL[chan2] = 100;
    m_ThresholdL[chan3] = 100;
    m_ThresholdL[chan4] = 100;

    mServPal.reset();
    mServUart.reset();
    mServI2C.reset();
    mServSPI.reset();
    mServCan.reset();
    mServLin.reset();
    mServFlex.reset();
    mServI2S.reset();
    mServ1553B.reset();

    syncEngine(ENGINE_LED_OFF, led_decode);

    //bug3260
    setuiChange( MSG_DECODE_ONOFF );
}

int servDec::startup()
{
    mUiAttr.setVisible(MSG_DECODE_RS232_TX, 0, false);
    mUiAttr.setVisible(MSG_DECODE_RS232_RX, 0, true);
    mUiAttr.setEnable(MSG_DECODE_TX_THRE, true);
    mUiAttr.setEnable(MSG_DECODE_RX_THRE, false);

    mUiAttr.setVisible(MSG_DECODE_SPI_MISO, 0, false);
    mUiAttr.setVisible(MSG_DECODE_SPI_MOSI, 0, true);
    mUiAttr.setEnable(MSG_DECODE_MISO_THRE, true);
    mUiAttr.setEnable(MSG_DECODE_MOSI_THRE, false);

    mUiAttr.setVisible(MSG_DECODE_PAL_SET, false);
    //bug3129
    mUiAttr.setVisible(MSG_DECODE_PAL_CLK_DLY,false);

    mUiAttr.setVisible(MSG_DECODE_1553B_THRE2, false);
    mUiAttr.setVisible(MSG_DECODE_LIN_POL, false);

    mUiAttr.setVisible(MSG_DECODE_CANFD_BAUD, false);


    if( this->getName().compare( serv_name_decode1) == 0 )
    {
        if( mUiAttr.getItem(MSG_APP_DECODE1)->getEnable() == false)
        {
            mUiAttr.setEnable(MSG_APP_DECODE1, true);
            mUiAttr.setEnable(MSG_APP_DECODE2, true);
            mUiAttr.setEnable(MSG_APP_DECODE3, true);
            mUiAttr.setEnable(MSG_APP_DECODE4, true);
        }
    }

    setBusOn(m_bBusOn);
    setBusFormat(m_nFormat);

    setLabelOn(m_bShowLabel);


    onCheckLicense();

    //updateAllUi( false );

    return 0;
}


int servDec::onReturnMenu(int)
{
    if( m_bShowEvt )
    {
        async(MSG_DECODE_EVT, false);
    }

    return ERR_NONE;
}

int servDec::on_enterActive()
{
    for(int i=0; i<4; i++)
    {
        if(m_nEvtBusXList & (0x1<<i))
        {
            mUiAttr.setEnable(MSG_DECODE_EVT_BUSX, i, true);
        }
        else
        {
            mUiAttr.setEnable(MSG_DECODE_EVT_BUSX, i, false);
        }
    }
    async(MSG_DECODE_EVT_BUSX, m_nEvtViewBusX);

    return ERR_NONE;
}

int servDec::on_exitActive(int id)
{
    if( id == this->getId() )
    {
        onReturnMenu(id);
    }
    return ERR_NONE;
}

DsoErr servDec::setDecodeType(int type)
{
    m_nBusType = type;
    return ERR_NONE;
}

int servDec::getDecodeType()
{
    return m_nBusType;
}


DsoErr servDec::setBusOn(bool on)
{
    m_bBusOn = on;

    int index = getId() - E_SERVICE_ID_DEC1;
    if(on)
    {
        if( m_nLed == 0 )
        {
            openDecodeTrace( digi_ch1 );

            //force to get trace if open bus when STOP
            int  iStat = sysGetSystemStatus();
            if( ((ControlStatus)iStat == Control_Stoped) ||
                ((ControlStatus)iStat == Control_Fpga_Force_Stopped))
            {
                post(serv_name_hori, servHori::cmd_apply_engine,0);
            }
            syncEngine(ENGINE_LED_ON, led_decode);
        }

        m_nEvtBusXList |= 1<<index;
        m_nBusIndex = BUS_DIRTY;
        m_nLed |= (1 << index);      
    }
    else
    {
        m_nEvtBusXList &= ~(1<<index);
        m_nLed &= ~(1 << index);

        mBus[0].clear();
        mBus[1].clear();
        mBusZoom[0].clear();
        mBusZoom[1].clear();
    }

    if( m_nLed == 0 )
    {
        syncEngine(ENGINE_LED_OFF, led_decode);
        closeDecodeTrace();
    }

    for(int i=0; i<4; i++)
    {
        if(m_nEvtBusXList & (0x1<<i))
        {
            mUiAttr.setEnable(MSG_DECODE_EVT_BUSX, i, true);
        }
        else
        {
            mUiAttr.setEnable(MSG_DECODE_EVT_BUSX, i, false);
        }
    }

    if( m_bShowEvt && !on )
    {
        async(MSG_DECODE_EVT, on);
    }

    if( on && m_nBusType == Bus_Parallel)
    {
        setChThreshold(chan1, getChThreshold(chan1));
        setChThreshold(chan2, getChThreshold(chan2));
        setChThreshold(chan3, getChThreshold(chan3));
        setChThreshold(chan4, getChThreshold(chan4));
    }
    return ERR_NONE;
}

bool servDec::getBusOn()
{
    return m_bBusOn;
}

DsoErr servDec::setBusOffset(int offset)
{
    int ret = ERR_NONE;
    int LOW = -167;
    int UPP  = 217;

    if( m_nViewMode == screen_yt_main_zoom)
    {
        if(mBusZoom[0].size() == 2 )
        {
            LOW = 0;
        }
    }

    m_nPosition += offset;

    if(  m_nPosition > UPP )
    {
        m_nPosition = UPP;
        ret = ERR_OVER_RANGE;
    }
    else if( m_nPosition < LOW )
    {
        m_nPosition = LOW;
        ret = ERR_OVER_RANGE;
    }
    //qDebug() << "pos=" << m_nPosition;

    return ret;
}

DsoErr servDec::setBusScpiPos(int offset)
{
    int pos = offset-getBusOffset();
    async( MSG_DECODE_POS, pos);
    return ERR_NONE;
}

int servDec::getBusOffset()
{
    setuiZVal(0);
    return m_nPosition;
}

DsoErr servDec::setBusFormat(int format)
{
    m_nFormat = format;
    mBus[0].setFmt( (DecodeFormat)m_nFormat );
    mBus[1].setFmt( (DecodeFormat)m_nFormat );

    mBusZoom[0].setFmt( (DecodeFormat)m_nFormat );
    mBusZoom[1].setFmt( (DecodeFormat)m_nFormat );
    return ERR_NONE;
}
DecodeFormat servDec::getBusFormat()
{
    return (DecodeFormat)m_nFormat;
}

DsoErr servDec::copyTrig()
{
    QList<int> decodeSource;
    switch( m_nBusType )
    {
        case Bus_Parallel:
        {
            decodeSource.append(chan1);
            decodeSource.append(chan2);
            decodeSource.append(chan3);
            decodeSource.append(chan4);
        }
        break;

        case Bus_RS232:
        {
            int baud = 115200;
            bool pol = false;
            int source = chan1;
            int check = 0;
            int width = 8;
            int stopBit = 0;
            query(serv_name_trigger_rs232, MSG_TRIGGER_SOURCE, source);
            query(serv_name_trigger_rs232, MSG_TRIGGER_RS232_BAUDRATE, baud);
            query(serv_name_trigger_rs232, MSG_TRIGGER_RS232_POLARITY, pol);

            query(serv_name_trigger_rs232, MSG_TRIGGER_RS232_DATAWIDTH, width);
            query(serv_name_trigger_rs232, MSG_TRIGGER_RS232_CHECK, check);
            query(serv_name_trigger_rs232, MSG_TRIGGER_RS232_STOPBIT, stopBit);
            setUartTxSrc( (Chan)source );
            setUartBaudVal(baud);
            setUartNegative(!pol);
            setUartDataWidth( width+1 );
            setUartParity( check );
            setUartStopBit( stopBit );

            decodeSource.append(source);

            //update source
            setuiChange(MSG_DECODE_RS232_TX);
        }
        break;
        case Bus_I2C:
        {
            int scl = chan1;
            int sda = chan2;
            query(serv_name_trigger_i2c, MSG_TRIGGER_I2C_SCL, scl);
            query(serv_name_trigger_i2c, MSG_TRIGGER_I2C_SDA, sda);
            setI2cClkSrc( (Chan)scl );
            setI2cDatSrc( (Chan)sda );

            decodeSource.append(scl);
            decodeSource.append(sda);

            //update source
            setuiChange(MSG_DECODE_I2C_SCL);
            setuiChange(MSG_DECODE_I2C_SDA);
        }
        break;

        case Bus_LIN:
        {
            int src = chan1;
            int baud = 9600;
            int ver = 0;
            query(serv_name_trigger_lin, MSG_TRIGGER_SOURCE_LA, src);
            query(serv_name_trigger_lin, MSG_TRIGGER_LIN_BAUD,baud);
            query(serv_name_trigger_lin, MSG_TRIGGER_LIN_VERSION, ver);
            setLinSrc( (Chan)src );
            setLinBaudPre(baud);
            setLinProtocol(ver);
            decodeSource.append(src);
            //update source
            setuiChange(MSG_DECODE_LIN_SRC);
        }
        break;

        case Bus_SPI:
        {
            int clk  = chan1;
            int sda = chan2;
            bool mode= false;
            bool edge= false;
            int width = 8;

            query(serv_name_trigger_spi, MSG_TRIGGER_SPI_SCL, clk);
            query(serv_name_trigger_spi, MSG_TRIGGER_SLOPE_POLARITY, edge);
            query(serv_name_trigger_spi, MSG_TRIGGER_SPI_SDA, sda);

            query(serv_name_trigger_spi, MSG_TRIGGER_SPI_WHEN, mode);
            query(serv_name_trigger_spi, MSG_TRIGGER_SPI_DATABITS, width);

            decodeSource.append(clk);
            decodeSource.append(sda);

            if( mode )//timeout
            {
                qint64 timeout = time_us(3);
                query(serv_name_trigger_spi, MSG_TRIGGER_SPI_TIMEOUT, timeout);
                setSpiTimeout( timeout );

            }
            else
            {
                bool pol = false;
                int cs   = chan4;
                query(serv_name_trigger_spi, MSG_TRIGGER_SPI_CSMODE, pol);
                query(serv_name_trigger_spi, MSG_TRIGGER_SPI_CS,  cs);
                setSpiCsPN( !pol );
                setSpiCsSrc( (Chan)cs );

                decodeSource.append(cs);

                //update source
                setuiChange(MSG_DECODE_SPI_CS);
            }
            setSpiMode(mode);
            setSpiClkSrc( (Chan)clk );
            setSpiMisoSrc( (Chan)sda );
            setSpiClkEdge(edge);
            setSpiDataWidth(width);

            //update source
            setuiChange(MSG_DECODE_SPI_CLK);
            setuiChange(MSG_DECODE_SPI_MISO);
        }
        break;

        case Bus_CAN:
        {
            int src = chan1;
            int bd  = BaudRate_1Mbps;
            int sig = SIGTYPE_CAN_L;
            int sp  = 50;
            query(serv_name_trigger_can, MSG_TRIGGER_SOURCE_LA, src);
            query(serv_name_trigger_can, MSG_TRIGGER_CAN_BAUD, bd);
            query(serv_name_trigger_can, MSG_TRIGGER_CAN_SINGNAL, sig);
            query(serv_name_trigger_can, MSG_TRIGGER_CAN_SAMPLE_POINT, sp);
            setCanBaud(bd);
            setCanSrc((Chan)src);
            if( sig == 3 )
            {
                sig = SIGTYPE_CSNZ_H_L;
            }
            setCanSignalType(sig);
            setCanSamplePos(sp);

            decodeSource.append(src);

            setuiChange(MSG_DECODE_CAN_SRC);
        }
        break;

        case Bus_I2S:
        {
            int  sclk = chan1;
            int  ws   = chan2;
            bool sclk_edge = false;
            int data  = chan3;
            int align = I2S_ALIGN_STD;
            query(serv_name_trigger_i2s, MSG_TRIGGER_IIS_SCLK, sclk);
            query(serv_name_trigger_i2s, MSG_TRIGGER_IIS_WS, ws);
            query(serv_name_trigger_i2s, MSG_TRIGGER_IIS_SDA, data);
            query(serv_name_trigger_i2s, MSG_TRIGGER_IIS_SLOPE, sclk_edge);
            query(serv_name_trigger_i2s, MSG_TRIGGER_IIS_ALIGNMENT, align);

            setI2SClk((Chan)sclk);
            setI2SEdge(sclk_edge);

            setI2SWS((Chan)ws);
            setI2SData((Chan)data);
            setI2SAlign(align);

            decodeSource.append(sclk);
            decodeSource.append(ws);
            decodeSource.append(data);

            setuiChange(MSG_DECODE_I2S_SCLK);
            setuiChange(MSG_DECODE_I2S_WS);
            setuiChange(MSG_DECODE_I2S_DATA);
        }
        break;

        case Bus_STD1553B:
        {
            int src = chan1;
            //bool pol = false;
            query(serv_name_trigger_1553b, MSG_TRIGGER_SOURCE,src);
            //query(serv_name_trigger_1553b, MSG_TRIG)
            set1553BSrc( (Chan)src );
            decodeSource.append(src);

            setuiChange(MSG_DECODE_1553B_SRC);
        }
        break;

        case Bus_FlexRay:
        {
            int src = chan1;
            int baud=FlexRate_10M;
            //int sig = FLEX_BP;
            query(serv_name_trigger_flexray, MSG_TRIGGER_SOURCE, src);
            query(serv_name_trigger_flexray, MSG_TRIGGER_FLEXRAY_BAUD, baud);
            //query(serv_name_trigger_flexray, MSG_TRIGGER_FL)

            setFlexSrc( (Chan)src );
            setFlexRate( baud );
            decodeSource.append(src);
            setuiChange(MSG_DECODE_FLEX_SRC);
        }
    }

    //copy trigger level as threshold
    int size = decodeSource.size();
    for(int i=0; i< size; i++)
    {

        CSource* pSrc = TriggerBase::getChanSource( decodeSource.at(i) );
        if( pSrc )
        {
            Chan c = (Chan)pSrc->getChan();
            if( c>=chan1 && c<=chan4)
            {
                qint64 a = pSrc->getLevel(0);
                configThreshold( c , a);
                m_Threshold[c] = a;

                m_nCurrentThre = a * pSrc->get_fRatio();
            }
            m_nCurrentChan = c;
        }
    }
    return ERR_NONE;
}

//!event & config
DsoErr servDec::setLabelOn(bool b)//
{
    m_bShowLabel = b;

    mBus[0].setLabelEn(b);
    mBus[1].setLabelEn(b);

    mBusZoom[0].setLabelEn(b);
    mBusZoom[1].setLabelEn(b);
    return ERR_NONE;
}

bool servDec::getLabelOn()
{
    return m_bShowLabel;
}


DsoErr servDec::setEventOn(bool eventon)//
{
    m_bShowEvt = eventon;
    return ERR_NONE;
}

bool servDec::getEventOn()
{
    return m_bShowEvt;
}

DsoErr servDec::setEventFormat(int format)//
{
    m_nEventFormt = format;
    return ERR_NONE;
}

DecodeFormat servDec::getEventFormat()
{
    return (DecodeFormat)m_nEventFormt;
}

DsoErr servDec::setEventViewMode(int viewmode)//
{
    m_nEvtViewMode = viewmode;
    if(viewmode == EVENT_TABLE )
    {
        mUiAttr.setEnable(MSG_DECODE_EVT_EXPORT, true);
    }
    else
    {
        mUiAttr.setEnable(MSG_DECODE_EVT_EXPORT, false);
    }
    return ERR_NONE;
}

int servDec::getEventViewMode()
{
    return m_nEvtViewMode;
}

DsoErr servDec::setEventBusX(int busx)
{
    m_nEvtViewBusX = busx;
    return ERR_NONE;
}
DsoErr servDec::getEventBusX()
{
    setuiEnable( m_nEvtBusXList != 0 );
    return m_nEvtViewBusX;
}

DsoErr servDec::exportEventData()
{
    //export event
    CIntent intent;

    m_nDecId = this->getId();

    intent.setIntent( MSG_STORAGE_SUB_SAVE,1,1, this->getId());

    if(getEventViewMode()==EVENT_TABLE)
    {
        intent.mIntent = servStorage::FUNC_SAVE_DEC;
    }
    else if(getEventViewMode() == EVENT_DETAILS)
    {
        intent.mIntent = servStorage::FUNC_SAVE_DEC;
    }
    else if(getEventViewMode() == EVENT_PAYLOAD)
    {
        intent.mIntent = servStorage::FUNC_SAVE_DEC;
    }

    startIntent( serv_name_storage, intent );
    return ERR_NONE;
}

DsoErr servDec::setJumpEvent()
{
    return ERR_NONE;
}

void servDec::setBusCompleted()
{

}

bool servDec::getBusCompleted()
{    
    return m_bSuccess;
}

/**
 * @brief clear pools
 */
void servDec::clearCache()
{
    foreach(DsoWfm*d, m_Streams)
    {
        delete d;
    }
    m_Streams.clear();
}

CDecBus *servDec::getBus()
{
    return &mBus[m_nBusIndex];
}
CDecBus *servDec::getZoomBus()
{
    return &mBusZoom[m_nBusIndex];
}

CDecBus* servDec::getDirtyBus()
{
    return &mBus[ BUS_DIRTY - m_nBusIndex ];
}

CDecBus* servDec::getDirtyZoomBus()
{
    if( m_nViewMode == screen_yt_main_zoom)
    {
        return &mBusZoom[ BUS_DIRTY - m_nBusIndex ];
    }
    return NULL;
}

void servDec::switchBusType()
{
    m_nBusIndex = !m_nBusIndex;
}

CTable *servDec::getEvtTable()
{
    //opened more than one bus
    if( m_nLed > 0 )
    {
        return m_pEvt;
    }
    return NULL;
}

void servDec::openDecodeTrace(Chan )
{
    serviceExecutor::post( E_SERVICE_ID_TRACE,
                           servTrace::cmd_open_digi, digi_ch1);
}

void servDec::closeDecodeTrace()
{
    serviceExecutor::post( E_SERVICE_ID_TRACE,
                           servTrace::cmd_close_digi, digi_ch1);
}

bool servDec::getDataAttr(int zoom, int& vLeft, int& vLen)
{
    horiAttr hAttr;
    struMem  mem;
    struView view;
    bool bFound = false;
    DsoWfm *pStream = NULL;

    //for bug 2601
    int allChan = chan4;
    if( m_bWithMSO )
    {
        allChan = d15;
    }

    int ch = chan1;
    for( ch=chan1; ch<=allChan; ch++)
    {
        getChanData((Chan)ch);
        if( m_Streams.size() == 0)
        {
            continue;
        }

        pStream = m_Streams.first();
        if( !pStream )
        {
            continue;
        }

        bFound = true;
        break;
    }
    if( !bFound )
    {
        return bFound;
    }

    if(zoom == screen_yt_main_zoom)
    {
        hAttr = sysGetHoriAttr(chan1, horizontal_view_zoom);
    }
    else
    {
        hAttr = sysGetHoriAttr(chan1, horizontal_view_main);
    }


    //qDebug() << (-hAttr.gnd * hAttr.inc) << hAttr.tInc ;
    //qDebug() << pStream->getllt0() << pStream->getlltInc();

    mem.sett( pStream->getllt0(), pStream->getlltInc() );
    mem.setLength( pStream->size()*8 );

    qlonglong s = -hAttr.gnd;
    s = s * hAttr.tInc;
    view.sett( s, hAttr.tInc );
    view.setWidth( wave_width );

    view.mapOfMem(&mem);

    vLeft = view.vLeft;
    vLen  = view.vLen;

    //qDebug() << "Screen=" << view.vLeft << view.vLen << view.vRight ;
    return true;
}


bool servDec::getStream(Chan src, DsoWfm& stream)
{
//    QTime timeTick;
//    timeTick.start();
    //postCmdData(m_bBusOn);
//    openDecodeTrace(src);

    Chan sub = chan_none;
    if( src >= digi_ch1_l && src <= digi_ch4_l)
    {
        int dff = src - digi_ch1_l;
        src = (Chan)((int)chan1 + dff);
        sub = src;
    }

    if( src <= chan4 )
    {
        dsoVert::getCH(src)->getDigi(stream, sub);
    }
    else if( src >= d0 && src <= d15 )
    {
        dsoVert::getCH(la)->getDigi(stream, src);
    }
    else
    {
        qDebug() << "Invalid bus source";
        Q_ASSERT(false);
    }

    //qDebug()<<"GetDigi cost:"<<timeTick.elapsed()<<"ms";


    if( stream.size() > 0 )
    {
        return true;
    }

    //qDebug() << "Can't get decoder data";
    return false;
}

int servDec::getDataSize()
{
    return m_nStreamSize;
}
/**
 * @brief data buffer pools
 * @param c
 * @return
 */
uchar* servDec::getChanData(Chan c)
{
    DsoWfm *pStream = NULL;
    if( c == chan_none )
    {
        return NULL;
    }

    if( m_Streams.contains(c) )
    {
        pStream = m_Streams[c];
    }
    else
    {
        DsoWfm *data = new DsoWfm;
        if ( getStream(c, *data) )
        {
            m_nStreamSize = data->size();
            m_Streams[c] = data;
            pStream = data;
        }
        else
        {
            delete data;
            return NULL;
        }
    }

    if( pStream )
    {
        CCellTime::mTimeInc    = pStream->gettInc().toDouble();
        CCellTime::mTimeStart  = pStream->gett0().toDouble();

        qint64 sample  = 1 / CCellTime::mTimeInc; //sample ;
        mServUart.setSamplePerBaud( sample );
        mServCan.setSamplePerBaud( sample );
        mServLin.setSamplePerBaud( sample );
        mServSPI.setTimeoutSample(sample);
        mServFlex.setSamplePerBaud( sample );
        mServ1553B.setSamplePerBaud( sample );

        return pStream->getPoint();
    }
    return NULL;
}


int servDec::saveEvtData(QString fileName)
{
    int ret = ERR_NONE;
    CDecSave::setScpiSave(false);
    m_nDecId = this->getId();
    QString path = servStorage::getRealPath(fileName);
    while(path.contains("//"))
    {
        path.replace("//","/");
    }

    ret = CDecSave::saveEventtable(path);
    if( ERR_NONE == ret )
    {
        servGui::showInfo( sysGetString(ERR_FILE_SAVE_OK, "File save completed" ) );
    }

    return ret;
}

bool servDec::keyTranslate(int key, int &msg, int &controlAction)
{
    //qDebug()<<__FILE__<<__LINE__<<getName();
    if( !this->getBusOn() )
    {
        return false;
    }

    serviceKey servKey;
    if ( m_WaveKeyMap.find( key, servKey) )
    {
        msg = servKey.mMsg;
        controlAction = servKey.mAction;
        return true;
    }
    return false;
}

void servDec::addWaveKeys()
{
    //! offset
    m_WaveKeyMap.append( encode_inc(R_Pkey_WAVE_POS),
                      MSG_DECODE_POS,
                      R_Skey_TuneInc );

    m_WaveKeyMap.append( encode_dec(R_Pkey_WAVE_POS),
                      MSG_DECODE_POS,
                      R_Skey_TuneDec );

    m_WaveKeyMap.append( (R_Pkey_WAVE_POS_Z),
                      MSG_DECODE_POS,
                         R_Skey_Zval );
}


bool servDec::startDecode()
{
    CDecoder* decoder = CDecoder::buildDecoder(this);
    CDecBus*  bus     = getDirtyBus();
    CDecBus*  zoomBus = getDirtyZoomBus();

    m_bSuccess = false;


    int vLeft = 0;
    int vLen  = wave_width;

    if( false == getDataAttr(m_nViewMode , vLeft, vLen) )
    {
        return false;
    }

    int size  = getDataSize() * 8;
    int left  = 0;
    int right = size - 1;
    //qDebug() << "playback" << left << right<< vLeft << vLen ;
    CDecoder::mLineView.setBlockFilter(vLeft, vLeft+vLen-1, left, right);


    if(zoomBus)
    {
        zoomBus->clear();
        qint64 zoomLeft  = m_n64ZoomOffset - hori_div * m_n64ZoomScale/2;
        qint64 zoomRight = m_n64ZoomOffset + hori_div * m_n64ZoomScale/2;
        qint64 mainLeft  = m_n64MainOffset - hori_div * m_n64MainScale/2;

        zoomLeft = zoomLeft - mainLeft;
        if( zoomLeft < 0 )
        {
            zoomLeft = 0;
        }

        zoomRight = zoomLeft + hori_div * m_n64ZoomScale;

        left  = zoomLeft * size / (m_n64MainScale*hori_div);
        right = zoomRight * size / (m_n64MainScale*hori_div);

//        left = abs(left);
//        left = size/2 - left;
//        right = size/2 + right;

        //qDebug() << "left=" << left << ", right=" << right;

        CDecoder::mLineViewZoom.setBlockFilter(0,wave_width-1, left, right-1);

        zoomBus->setLabelEn( getLabelOn());
    }

    bus->clear();
    bus->setLabelEn( getLabelOn());

    decoder->rst();

    if( !decoder->doDecode( bus, zoomBus ) )
    {
        bus->clear();
        if( zoomBus != NULL )
        {
            zoomBus->clear();
        }
    }
    else
    {
        //qDebug() << "more result:" << decoder->mBlocklist.size();
        if( decoder->mBlocklist.size()>=LIMIT_RESULT_SIZE )
        {
            //qDebug() << "more result:" << decoder->mBlocklist.size();
            decoder->mBlocklist.clear();
            bus->clear();
            if( zoomBus != NULL )
            {
                zoomBus->clear();
            }
        }
        else
        {
            m_pEvt = decoder->mpTable;
        }
    }

    if( getDirtyZoomBus() != zoomBus && zoomBus == NULL )
    {
        servDec::mTraceSem.release();
    }
    switchBusType();
    return true;
}


void servDec::finishDecode( bool b)
{
    m_bSuccess = b;
    serviceExecutor::post( getId(), cmd_bus_completed, 0);
}

int servDec::getDecId()//qxl 2018.02.05
{
    return m_nDecId;
}

/*-----------------------------------------------------------------
 * Thread
 * ---------------------------------------------------------------*/
int CDecoderThread::m_nTimemode = Acquire_YT;

#define def_time()          QTime timeTick;
#define start_time()        timeTick.start();
#define end_time()          //qDebug()<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";

CDecoderThread::CDecoderThread( QObject */*parent*/ ) : QThread()
{
}


void CDecoderThread::run()
{
    //waiting for the system started
    QThread::sleep(25);

    while( true )
    {
        //wait trace data ready
        servDec::mTraceSem.acquire();

        servDec::clearCache();
        servDec::switchHeap();
        foreach(servDec* decoder, mDecoders)
        {
            if( decoder->getBusOn() && CDecoderThread::m_nTimemode != Acquire_XY)
            {

                bool b = decoder->startDecode();
                decoder->finishDecode(b);

            }
        }
        QThread::msleep( 10 );
    }
}


void CDecoderThread::addDecoder(servDec *dec)
{
    mDecoders.append(dec);
}

servDec* CDecoderThread::getDecoder(int i)
{
    if( i < mDecoders.size() )
    {
        return mDecoders.at(i);
    }


    return NULL;
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
IMPLEMENT_CMD( serviceExecutor, servDecodeSel  )
start_of_entry()
//on_set_int_int( CMD_SERVICE_ACTIVE, &servDecodeSel::setActive ),
on_set_void_void( MSG_HOR_TIME_MODE, &servDecodeSel::onTimeMode),

on_set_int_void ( MSG_APP_DECODE1, &servDecodeSel::enterBus1),
on_get_string   ( MSG_APP_DECODE1, &servDecodeSel::getBusStr1),

on_set_int_void ( MSG_APP_DECODE2, &servDecodeSel::enterBus2),
on_get_string   ( MSG_APP_DECODE2, &servDecodeSel::getBusStr2),

on_set_int_void ( MSG_APP_DECODE3, &servDecodeSel::enterBus3),
on_get_string   ( MSG_APP_DECODE3, &servDecodeSel::getBusStr3),

on_set_int_void ( MSG_APP_DECODE4, &servDecodeSel::enterBus4),
on_get_string   ( MSG_APP_DECODE4, &servDecodeSel::getBusStr4),
end_of_entry()

servDecodeSel::servDecodeSel(QString name, ServiceId id  )
    : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();
}

void servDecodeSel::registerSpy()
{
    spyOn( serv_name_hori,MSG_HOR_TIME_MODE);
}


void servDecodeSel::onTimeMode()
{
    query(serv_name_hori,
          MSG_HOR_TIME_MODE,
          CDecoderThread::m_nTimemode);
    if( CDecoderThread::m_nTimemode == Acquire_XY ||
        CDecoderThread::m_nTimemode == Acquire_ROLL    )
    {
        mUiAttr.setEnable(MSG_APP_DECODE1, false);
        mUiAttr.setEnable(MSG_APP_DECODE2, false);
        mUiAttr.setEnable(MSG_APP_DECODE3, false);
        mUiAttr.setEnable(MSG_APP_DECODE4, false);

        serviceExecutor::post(E_SERVICE_ID_DEC1,
                              MSG_DECODE_ONOFF,
                              false);
        serviceExecutor::post(E_SERVICE_ID_DEC2,
                              MSG_DECODE_ONOFF,
                              false);
        serviceExecutor::post(E_SERVICE_ID_DEC3,
                              MSG_DECODE_ONOFF,
                              false);
        serviceExecutor::post(E_SERVICE_ID_DEC4,
                              MSG_DECODE_ONOFF,
                              false);
    }
    else
    {
        mUiAttr.setEnable(MSG_APP_DECODE1, true);
        mUiAttr.setEnable(MSG_APP_DECODE2, true);
        mUiAttr.setEnable(MSG_APP_DECODE3, true);
        mUiAttr.setEnable(MSG_APP_DECODE4, true);
    }
}

QString servDecodeSel::getBusStr1()
{
    return getBusStr(0);
}

QString servDecodeSel::getBusStr2()
{
    return getBusStr(1);
}

QString servDecodeSel::getBusStr3()
{
    return getBusStr(2);
}

QString servDecodeSel::getBusStr4()
{
    return getBusStr(3);
}

QString servDecodeSel::getBusStr( int i)
{
    if( m_pDecThread != NULL )
    {
        servDec *pDec = m_pDecThread->getDecoder(i);
        if( pDec != NULL )
        {
            QString str;

            QString onoff ;
            if( pDec->getBusOn() )
            {
                onoff = "ON";
            }
            else
            {
                onoff = "OFF";
            }

            int busType = pDec->getDecodeType();
            str = QString("%1$%2").arg(sysGetOptionString(MSG_DECODE_TYPE,busType,"Bus"))
                                  .arg(onoff);

//                str = QString("%1(%2)").arg(sysGetString(mBusStrID[bt],"Bus"))
//                                       .arg(onoff);
            return str;
        }
    }

    return "OFF";
}


int servDecodeSel::enterBus1()
{
    return enterBus(0);
}

int servDecodeSel::enterBus2()
{
    return enterBus(1);
}

int servDecodeSel::enterBus3()
{
    return enterBus(2);
}

int servDecodeSel::enterBus4()
{
    return enterBus(3);
}

int servDecodeSel::enterBus( int i)
{
    serviceExecutor::post(E_SERVICE_ID_DEC1 + i,
                          CMD_SERVICE_ACTIVE,
                          1);
    return ERR_NONE;
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
