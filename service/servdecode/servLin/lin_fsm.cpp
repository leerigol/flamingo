#include "lin_fsm.h"
#include "servdecode.h"

#include <QDebug>

//! CLInIdle
CLinIdle::CLinIdle()
{}
CLinIdle::~CLinIdle()
{}

void CLinIdle::onEnter( int /*bitVal*/ )
{
    mCounter.begin();
    mClock.stop();
    mLinData.clear();
}

void CLinIdle::serialIn( int event, int bitVal )
{
    m_nErrType = LIN_ERR_NONE;
    if( event == FALL )
    {
        mPosition = mCounter.now();
        //! enter into break
        toNext( bitVal );

        //! clock start samp
        mClock.start( mpLinCfg->getSamplePerBaud(),
                      mpLinCfg->getSamplePerBaud()/2);
    }
}

//! CLinBreak
CLinBreak::CLinBreak()
{}
CLinBreak::~CLinBreak()
{}

void CLinBreak::onEnter( int /*bitVal*/ )
{
    mCount = 0;
    mData = 0;
    mCounter.begin();
    mPosition = mCounter.now();
}

void CLinBreak::serialIn( int event, int bitVal )
{
    if( event ==SAMP )
    {
        if( bitVal )
        {
            if( mData == 0 && mCount >= 11 )
            {
                toNext( bitVal );
            }
            else
            {
                toIdle(bitVal);
            }
            return;
        }
        mData |= bitVal << mCount;
        mCount++;
    }
}

void CLinBreak::onExit(int /*bitVal*/)
{
    if( mCount >= 5 )
    {

        CDecPayloadCmd* pData = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pData );

        mSpan = mCounter.getDur();
        pData->setPosSpan( mPosition, mSpan );

        if( mData == 0 && mCount >= 11 )
        {
            pData->setPayload( mData ,16,LIN_WK_BK_COLOR,TEXT_BREAK);
            AddBlock(CDecPayloadCmd,pData);
        }
        else
        {
            pData->setPayload( mData ,5,LIN_WK_COLOR,TEXT_WAKEUP);
            AddBlock(CDecPayloadCmd,pData);
        }

        AddOneFrame();
    }
}



//! CLinSync 0X55
CLinSync::CLinSync()
{}
CLinSync::~CLinSync()
{}

void CLinSync::onEnter( int /*bitVal*/ )
{
    mErr = false;
    mDataField = false;
    mCount = 0;
    mData = 0;
}

void CLinSync::serialIn( int event, int bitVal )
{
    if( event == SAMP && mDataField)
    {
        mData |=  bitVal << mCount;
        mCount++;

        if( mCount == 10)//SCI with start and stop bit
        {
            mData = ( mData >> 1 ) & 0xff;

            //for bug1717
//            if(mData != 0X55 || bitVal == 0)
//            {
//                mErr = true;
//                toIdle( bitVal );
//            }
//            else
            {
                mCount = 8;
                //sync end;
                toNext( bitVal );
            }
        }
    }
    else if( event ==  FALL && mCount ==0) //sync filed
    {
        mDataField = true;
        mCounter.begin();
        mPosition = mCounter.now();
    }
}
void CLinSync::onExit(int /*bitVal*/)
{
   if( !mErr )
   {
        CDecPayloadCmd* pData = new  CDecPayloadCmd();
        Q_ASSERT( NULL != pData );

        mSpan = mCounter.getDur();
        pData->setPosSpan( mPosition, mSpan );
        if( mData != 0x55 )
        {
            pData->setPayload( mData ,mCount,ERR_COLOR,TEXT_SYNC);
            m_nErrType = LIN_ERR_SYNC;
        }
        else
        {
            pData->setPayload( mData ,mCount,LIN_SYNC_COLOR,TEXT_SYNC);
        }
        AddBlock(CDecPayloadCmd,pData);
   }
   else
   {
       m_nErrType = LIN_ERR_SYNC;
   }
}


//! CLinPid
CLinPid::CLinPid()
{}
CLinPid::~CLinPid()
{}

void CLinPid::onEnter( int /*bitVal*/ )
{
    mDataField = false;
    mCount  = 0;
    mData   = 0;
    mErr    = false;
}

void CLinPid::serialIn( int event, int bitVal )
{
    switch( event )
    {
        case SAMP:
        if(mDataField)
        {
            bitData[mCount] = bitVal;
            mData |=  bitVal << mCount;
            mCount++;
            if( mCount == 10 )
            {
                mData = ( mData >> 1 ) & 0xff;

                //bitData[0] = start bit
                //bitData[9] = stop bit
                // D6= D0+D1+D2+D4
                // D7= D1+D3+D4+D5)
                int p0 =  bitData[1] ^ bitData[2] ^ bitData[3] ^ bitData[5];
                int p1 = !(bitData[2] ^ bitData[4] ^ bitData[5] ^ bitData[6]);
                if( bitVal ==0 || p0 != bitData[7] || p1 != bitData[8] )
                {
                    mErr = true;
                }

                mLinData.append(mData);
                mCount = 8;
                toNext( bitVal );
            }
        }
        break;

    case FALL://the first fall  start bit
        if(mCount ==0)
        {
            mDataField = true;
            memset( bitData, 0, sizeof(bitData) );

            mCounter.begin();
            mPosition = mCounter.now();
        }
        break;
    }
}
void CLinPid::onExit(int /*bitVal*/)
{
    mSpan = mCounter.getDur();

    CDecPayloadCmd* pData = new  CDecPayloadCmd();
    Q_ASSERT( NULL != pData );
    pData->setPosSpan( mPosition, mSpan );

    if( mpLinCfg->mParity == false)
    {
        mData = mData & 0x3f;
    }

    if(mErr)
    {
        pData->setPayload( mData ,mCount,ERR_COLOR,TEXT_ID);
        if( m_nErrType == LIN_ERR_NONE )
        {
            m_nErrType = LIN_ERR_PID;
        }
    }
    else
    {
        pData->setPayload( mData ,mCount,LIN_ID_COLOR,TEXT_ID);
    }
    AddBlock(CDecPayloadCmd,pData);

    mpTable->append(new CCellTime(mPosition));
    mpTable->append(new CCellInfo(mData,8));

    //for payload
    mCellData = new CCellDW();
    mpTable->append(mCellData);
}



//! CLinData
CLinData::CLinData()
{}
CLinData::~CLinData()
{}

void CLinData::onEnter( int /*bitVal*/ )
{
    mDataField = false;
    mErr    = false;
    mCount  = 0;
    mData   = 0;
}

void CLinData::serialIn( int event, int bitVal )
{
    switch( event )
    {
        case SAMP:
            mData |= bitVal << mCount;
            mCount++;
            if(mDataField)
            {
                //SCI with start and stop bit
                if( mCount == 10 )
                {
                    mData = ( mData >> 1 ) & 0xff;

                    //with pid and checksum 10bit
                    mLinData.append(mData);

                    //every data is 8bit
                    mCount = 8;
                    toSelf( bitVal );
                }
            }
            //ref x6000 and tek
            else if(mCount == 16 &&
                    mData == 0xffff && //idle
                    mLinData.size() >= 3 )//ID,Data,Crc
            {
                toIdle(bitVal);
            }
            break;

        //the first fall is start  bit
        case FALL:
            if(!mDataField)
            {
                mCount  = 0;
                mData   = 0;
                mDataField = true;
                mCounter.begin();
                mPosition = mCounter.now();
            }
            break;
    }
}

void CLinData::onExit(int /*bitVal*/)
{  
    if(mDataField)
    {
        mSpan = mCounter.getDur();

        CDecPayloadData* pData = new  CDecPayloadData();
        Q_ASSERT( NULL != pData );
        pData->setPosSpan( mPosition, mSpan );
        pData->setPayload( mData);
        AddBlock(CDecPayloadData,pData);
    }
    //CRC Check
    else
    {
        uint sum     = 0;
        uint sumpid  = 0;
        uint version = 0;

        uint pid = mLinData[0];
        //all datas sum with
        for(int i=1; i<(mLinData.size()-1); i++)
        {
            sum += mLinData[i];
            if(sum >= 256)
            {
                sum -= 255;
            }

            //add data payload for event table
            if(mCellData->size() > 1)
            {
                mCellData->append(new CCellStr(1));//add space
            }
            mCellData->append(new CCellDW(mLinData[i],8));
        }

        version = mpLinCfg->mVersion;

        //Frame idendifier range 60~61 use classic checksum
        if( (pid & 0x3F) == 0x3c ||
            (pid & 0x3F) == 0x3d)            
        {
            version = Lin_1X;
        }

        uint checksum = mLinData.last();
        mErr = false;
        switch(version)
        {
            case Lin_1X:
                if(checksum + sum != 0xFF)//check + data sum != 0xFF
                {
                    mErr = true;
                }
                break;

            case Lin_2X:
                sum += pid;//sum with pid
                if(sum >= 256)
                {sum -= 255;}

                if(checksum + sum != 0xFF)//check + pid + data sum != 0xFF
                {
                    mErr = true;
                }
                break;

            case Lin_both:
                sumpid = sum + pid;//sum with pid
                if(sumpid >= 256)
                {sumpid -= 255;}

                if(checksum + sum != 0xFF && checksum + sumpid != 0xFF)
                {
                    mErr = true;
                }
                break;
            default:
                mErr = true;
                break;
        }

        CDecPayloadData* pData = (CDecPayloadData*)mBlocklist.last();
        pData->setPosSpan( mPosition, mSpan );
        if(!mErr)
        {
            pData->setPayload(pData->GetPayloadData(),
                              pData->getPayloadWidth(),
                              LIN_CRC_COLOR,
                              TEXT_CRC );
        }
        else
        {
            pData->setPayload(pData->GetPayloadData(),
                              pData->getPayloadWidth(),
                              ERR_COLOR,
                              TEXT_CRC );

            if( m_nErrType == LIN_ERR_NONE )
            {
                m_nErrType = LIN_ERR_CRC;
            }
        }

        mpTable->append(new CCellInfo(pData->GetPayloadData(),8));
        if( m_nErrType == LIN_ERR_CRC)
        {
            mpTable->append(new CCellStr(MSG_DECODE_EVT_CRC));
        }
        else if( m_nErrType == LIN_ERR_DATA )
        {
            mpTable->append(new CCellStr(MSG_DECODE_EVT_DATA));
        }
        else if( m_nErrType == LIN_ERR_SYNC )
        {
            mpTable->append(new CCellStr(MSG_TRIGGER_VIDEO_SYNC));
        }
        else
        {
            mpTable->append(new CCellStr(0));
        }
    }
}

//! CLinErr::
CLinErr::CLinErr()
{}
CLinErr::~CLinErr()
{}
void CLinErr::onEnter( int /*bitVal*/ )
{
    if( m_nErrType != LIN_ERR_NONE )
    {
        switch( m_nErrType )
        {
            case LIN_ERR_DATA:
                mpTable->append(new CCellStr(MSG_DECODE_EVT_DATA));
            break;

            case LIN_ERR_CRC:
                mpTable->append(new CCellStr(MSG_DECODE_EVT_CRC));
            break;

            default:
                mpTable->append(new CCellStr(0));
            break;
        }
    }
    else
    {
        mpTable->append(new CCellStr(0));
    }
}

void CLinErr::onExit( int bitVal )
{
    toNext( bitVal );
}
void CLinErr::serialIn( int /*event*/, int /*bitVal*/ )
{}


