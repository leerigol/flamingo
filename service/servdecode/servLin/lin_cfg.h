#ifndef CLINCFG_H
#define CLINCFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/iserial.h"

using namespace DsoType;

struct CLinCfg
{
    void reset();
    void serialIn(CStream&);
    void serialOut(CStream&);

    int  getSamplePerBaud(void);
    void setSamplePerBaud(qint64 sample); //sample / baud

    Chan   mSrc;
    int    mBaudPre;
    int    mVersion;
    bool   mPolarity;
    bool   mParity;
    int    mSamplePerBaud;

};
#endif // CLINCFG_H
