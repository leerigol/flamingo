#include "lin_fsm.h"
#include "../servdecode.h"

static CLinIdle  fsm_lin_idle;
static CLinBreak fsm_lin_break;
static CLinSync  fsm_lin_sync;
static CLinPid   fsm_lin_pid;
static CLinData  fsm_lin_data;
static CLinErr   fsm_lin_err;

CLinCfg* CLinDecoder::mpLinCfg;
QList<uint> CLinDecoder::mLinData;
int CLinDecoder::m_nErrType;

CLinDecoder::CLinDecoder()
{
    fsm_lin_idle.setNextStat( &fsm_lin_break );
    fsm_lin_break.setNextStat( &fsm_lin_sync );

    fsm_lin_sync.setNextStat( &fsm_lin_pid );
    fsm_lin_pid.setNextStat( &fsm_lin_data );

    fsm_lin_data.setNextStat( &fsm_lin_err );
    fsm_lin_err.setNextStat( &fsm_lin_idle );

    setIdle( &fsm_lin_idle );
    setErr( &fsm_lin_err );
}
CLinDecoder::~CLinDecoder()
{
}

//! rst to idle
void CLinDecoder::rst()
{
    mpNow = &fsm_lin_idle;

    mCounter.rst();
    mClock.stop();

    clearBlock();

    mpLinCfg = &mpCurrServ->mServLin;

    mpTable = new CTable();
    setEventTable(mpTable);
}

bool CLinDecoder::doDecode( CDecBus* bus, CDecBus* zoomBus )
{
    bool ret = false;
    if( decode( mpLinCfg->mSrc) )
    {
        CDecLine* line = new CDecLine();

        //if( mpCurrServ->getLabelOn())
        {
            QString label = "LIN";
            line->setLabel(label);
        }

        mLineView.makeLine(*line, mBlocklist);
        bus->append( line );
        if(zoomBus)
        {
            CDecLine* lineZoom = new CDecLine();
            lineZoom->setLabel( line->getLabel() );

            mLineViewZoom.makeLine(*lineZoom,mZoomBlocklist);
            zoomBus->append(lineZoom);
        }
        ret = true;
    }

    return ret;
}

bool CLinDecoder::decode(Chan src)
{
    unsigned int u32Data;
    int last_dat, next_dat;

    uchar* pStream = mpCurrServ->getChanData( src );
    if( !pStream )
    {
        return false;
    }
    last_dat = ( *pStream & 0x80 ) ? 1 : 0;
    next_dat = last_dat;

    //bool bPolarity = mpLinCfg->mPolarity;

    int size = mpCurrServ->getDataSize();
    //! overflow 3 bytes
    for (int i = 0; i < size; )
    {
        //! read 32 bits
        memcpy( &u32Data, pStream + i, 4 );

        //! no change
        if ( u32Data == m32IdleMask[last_dat] )
        {
            if ( mClock.tryTick(32) > 0)
            {                
            }
            else
            {
                i += 4;
                mCounter.count( 32 );
                mClock.tick(32);
                continue;
            }
        }

        uint dat = u32Data & 0xff;
        if(dat == m8IdleMask[last_dat])
        {
            if ( mClock.tryTick(8) > 0 )
            {
            }
            else
            {
                i += 1;
                mCounter.count(8);
                mClock.tick(8);                
                continue;
            }
        }

        //! change in 8bits
        for (int j = 0; j < 8; j++ )
        {
            next_dat = u32Data & m32Masks[j] ? 1 : 0;

            //! rise
            if (last_dat <  next_dat)//rise
            {
                now()->serialIn( RISE, next_dat );
                last_dat = next_dat;
                mClock.sync();
            }
            else if (last_dat  >  next_dat)//fall
            {
                now()->serialIn( FALL, next_dat );
                last_dat = next_dat;
                mClock.sync();
            }

            //! timeout
            if ( mClock.tick() > 0 )
            {
                now()->serialIn( SAMP, next_dat );
                //mClock.sync();
            }
            mCounter.count();
        }
        i += 1;
    }
    return true;
}

int CLinDecoder::setEventTable(CTable* table)
{
    //! head
    QList<quint32> lstHead;
    lstHead.append( MSG_DECODE_EVT_TIME );
    lstHead.append( MSG_DECODE_EVT_ID );
    lstHead.append( MSG_DECODE_EVT_DATA );
    lstHead.append( MSG_DECODE_EVT_CRC );
    lstHead.append( MSG_DECODE_EVT_ERR );
    table->setHead( lstHead );

    //! layout
    QList<quint32> lstLayout;
    lstLayout.append( 50 );
    lstLayout.append( 100 );
    lstLayout.append( 50 );
    lstLayout.append( 200 );
    lstLayout.append( 50 );
    table->setLayout( lstLayout );

    //! title
    CCellStr *pTitle = new CCellStr(MSG_DECODE_EVT_LIN);
    table->setTitle( pTitle );
    return ERR_NONE;
}
