#ifndef CDECLIN_H
#define CDECLIN_H

#include "lin_cfg.h"

#include "../decoder.h"
#include "../decTool/DecState.h"


#define PIDNO 9

enum
{
    LIN_ERR_NONE,
    LIN_ERR_SYNC,
    LIN_ERR_PID,
    LIN_ERR_DATA,
    LIN_ERR_CRC
};
class CLinDecoder: public CDecoder,public CDecState
{
public:
    CLinDecoder();
    ~CLinDecoder();

public:
     static CLinCfg* mpLinCfg;

public:
     bool doDecode( CDecBus*, CDecBus* zoomBus = NULL );
     void rst();
     int  setEventTable(CTable* table);

protected:
    static QList<uint> mLinData;
    static int m_nErrType;

private:
    bool decode(Chan src);
};

class CLinIdle : public CLinDecoder
{
public:
    CLinIdle();
    virtual ~CLinIdle();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
protected:
    unsigned int mPosition;
};

class CLinBreak : public CLinDecoder
{
public:
    CLinBreak();
    virtual ~CLinBreak();

public:
    void onEnter( int bitVal );
    void serialIn( int event, int bitVal );
    void onExit(int bitVal);
};

class CLinSync: public CLinDecoder
{
public:
    CLinSync();
    virtual ~CLinSync();

protected:
    bool mSampOn;
    bool mErr;
    bool mDataField;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CLinPid: public CLinDecoder
{
public:
    CLinPid();
    virtual ~CLinPid();

protected:
    uchar    bitData[10];// for parity
    bool mErr;
    bool mDataField;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};


class CLinData : public CLinDecoder
{
public:
    CLinData();
    virtual ~CLinData();

protected:
    bool mDataField;
    bool mCRCField;
    bool mErr;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CLinCheck : public CLinDecoder
{
public:
    CLinCheck();
    virtual ~CLinCheck();
protected:
    bool mDataField;
    bool mErr;

public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

class CLinErr : public CLinDecoder
{
public:
    CLinErr();
    virtual ~CLinErr();
protected:
public:
    void onEnter( int bitVal );
    void onExit( int bitVal );
    void serialIn( int event, int bitVal );
};

#endif // CDECLIN_H
