#include "lin_cfg.h"
#include "servdecode.h"

void CLinCfg::reset()
{
    mBaudPre    =   BaudRate_19200;

    mParity     =   false;
    mPolarity   =   Polarity_Positive;

    mVersion    =   Lin_1X;

    mSrc        =   chan1;

}

void CLinCfg::serialIn(CStream &stream)
{
    int temp = 0;
    stream.read("mSrc",        temp);
    mSrc = (Chan)temp;
    stream.read("mBaudPre",    mBaudPre);
    stream.read("mParity",     mParity);
    stream.read("mVersion",    mVersion);
    stream.read("mPolarity",   mPolarity);
}

void CLinCfg::serialOut(CStream &stream)
{
    stream.write("mSrc",        (int)mSrc);
    stream.write("mBaudPre",    mBaudPre);
    stream.write("mParity",     mParity);
    stream.write("mVersion",    mVersion);
    stream.write("mPolarity",   mPolarity);
}

void CLinCfg::setSamplePerBaud(qint64 sample)
{
    if( mBaudPre > 0)
    {
        mSamplePerBaud = sample / mBaudPre;
    }
    else
    {
        mSamplePerBaud = 8;
    }

    if( mSamplePerBaud < 3)
    {
        mSamplePerBaud = 3;
    }
}

int CLinCfg::getSamplePerBaud()
{
    return mSamplePerBaud;
}


DsoErr servDec::setLinSrc(Chan src)
{
    mServLin.mSrc = src;
    if(src > chan4)
    {
        mUiAttr.setEnable(MSG_DECODE_LIN_THRE, false);
    }
    else
    {
        getChThreshold1(src);
        mUiAttr.setEnable(MSG_DECODE_LIN_THRE, true);
        setuiChange( MSG_DECODE_LIN_THRE );
    }
    return ERR_NONE;
}

Chan servDec::getLinSrc()
{
    return mServLin.mSrc;
}


DsoErr servDec::setLinThreAbs(qint64 t)
{
    setChThreshold(mServLin.mSrc, t);
    return ERR_NONE;
}

qint64 servDec::getLinThreAbs()
{
    Chan s = mServLin.mSrc;
    return getChThreshold(s);
}


DsoErr servDec::setLinBaudPre(int baud)
{
    mServLin.mBaudPre = baud;
    return ERR_NONE;
}

int servDec::getLinBaudPre()
{
    setuiBase( 1, E_0 );
    setuiMinVal( 2400 );
    setuiMaxVal( 20000000 );
    setuiZVal(19200);
    setuiPostStr( "bps" );
    return mServLin.mBaudPre;
}

DsoErr servDec::setLinProtocol(int version)
{
    mServLin.mVersion = version;
    return ERR_NONE;
}

int servDec::getLinProtocol()
{
    return mServLin.mVersion;
}

DsoErr servDec::setLinPolarity(bool pn)
{
    mServLin.mPolarity = pn;
    return ERR_NONE;
}

bool servDec::getLinPolarity()
{
    return mServLin.mPolarity;
}

DsoErr servDec::setLinParity(bool parity)
{
    mServLin.mParity = parity;
    return ERR_NONE;
}

bool servDec::getLinParity()
{
    return mServLin.mParity;
}




