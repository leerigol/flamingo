#ifndef LIN_MAP_H
#define LIN_MAP_H

#include "../servdecode/servdecode.h"


on_set_int_int  (MSG_DECODE_LIN_SRC,        &servDec::setLinSrc),
on_get_int      (MSG_DECODE_LIN_SRC,        &servDec::getLinSrc),


on_set_int_ll   (MSG_DECODE_LIN_THRE,       &servDec::setLinThreAbs),
on_get_ll       (MSG_DECODE_LIN_THRE,       &servDec::getLinThreAbs),

on_set_int_int  (MSG_DECODE_LIN_BAUD,       &servDec::setLinBaudPre),
on_get_int      (MSG_DECODE_LIN_BAUD,       &servDec::getLinBaudPre),

on_set_int_int  (MSG_DECODE_LIN_VER,        &servDec::setLinProtocol),
on_get_int      (MSG_DECODE_LIN_VER,        &servDec::getLinProtocol),

on_set_int_bool (MSG_DECODE_LIN_POL,        &servDec::setLinPolarity),
on_get_bool     (MSG_DECODE_LIN_POL,        &servDec::getLinPolarity),

on_set_int_bool (MSG_DECODE_LIN_PARITY,     &servDec::setLinParity),
on_get_bool     (MSG_DECODE_LIN_PARITY,     &servDec::getLinParity),

#endif // LIN_MAP_H

