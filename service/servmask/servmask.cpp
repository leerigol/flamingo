//#define _DEBUG 1
#include "servmask.h"
#include "../servstorage/servstorage.h"
#include "../servquick/servquick.h"
#include "../../engine/base/enginecfg.h"
#include "../servvertmgr/servvertmgr.h"
#include "../servvert/servvert.h"

#include "../../arith/norm125.h"
#include "../../arith/masktolerance.h"

#include "../servhori/servhori.h"

#define RANGE_SCREEN 1000

#define X_MASK_DEF 24   //! Old:  10
#define X_MASK_MAX 200
#define X_MASK_MIN 1

#define Y_MASK_DEF 12   //! Old:  3
#define Y_MASK_MAX 50
#define Y_MASK_MIN 1

#define X_MASK_STEP  1
#define Y_MASK_STEP  1

#define XY_MASK_DIVISOR 100

#define MASK_CURSOR_RANGE_Z  (-32768)
#define MASK_TORLEANCE_FMT  ( dso_view_format(fmt_f,fmt_2f) )

#define mask_timer  50

/*! \var 定义消息映射表
* 定义消息映射表
*/
IMPLEMENT_CMD( serviceExecutor, servMask )
start_of_entry()

on_set_int_bool( MSG_MASK_ENABLE,       &servMask::setEnable ),
on_get_bool    ( MSG_MASK_ENABLE,       &servMask::getEnable ),

on_set_int_int( MSG_MASK_SOURCE,        &servMask::setMaskSrc),
on_get_int    ( MSG_MASK_SOURCE,        &servMask::getMaskSrc),

on_set_int_bool( MSG_MASK_OPERATE,      &servMask::setMaskStart),
on_get_bool    ( MSG_MASK_OPERATE,      &servMask::getMaskStart),

on_set_int_int( MSG_MASK_RANGE,        &servMask::setMaskRange ),
on_get_int    ( MSG_MASK_RANGE,        &servMask::getMaskRange ),

on_set_int_int( MSG_MASK_X_MASK,        &servMask::setXMask ),
on_get_int    ( MSG_MASK_X_MASK,        &servMask::getXMask ),

on_set_int_int( MSG_MASK_Y_MASK,        &servMask::setYMask ),
on_get_int    ( MSG_MASK_Y_MASK,        &servMask::getYMask ),

on_set_int_bool(MSG_MASK_SHOW_STAT,     &servMask::setShowStat ),
on_get_bool    (MSG_MASK_SHOW_STAT,     &servMask::getShowStat ),

on_set_int_int( MSG_MASK_RESET_STAT,   &servMask::resetStat),

on_set_void_void( MSG_MASK_SAVE,        &servMask::saveMask),
on_set_void_void( MSG_MASK_LOAD,        &servMask::loadMask),

on_set_int_void(MSG_MASK_CREATE,        &servMask::createMask),

//! options
on_set_int_int( MSG_MASK_OUT_EVENT,  &servMask::setOutputEvent ),
on_get_int(     MSG_MASK_OUT_EVENT,  &servMask::getOutputEvent ),

on_set_int_bool( MSG_MASK_OUT_ONOFF, &servMask::setOutputOnOff ),
on_get_bool(     MSG_MASK_OUT_ONOFF, &servMask::getOutputOnOff),

on_set_int_ll( MSG_MASK_OUT_PULSE,   &servMask::setOutputTime),
on_get_ll(     MSG_MASK_OUT_PULSE,   &servMask::getOutputTime),

on_set_int_bool( MSG_MASK_OUT_HL,    &servMask::setOutputPulse),
on_get_bool(     MSG_MASK_OUT_HL,    &servMask::getOutputPulse),

on_set_int_int( MSG_MASK_ERR_ACTION,    &servMask::setErrAction ),
on_get_int    ( MSG_MASK_ERR_ACTION,    &servMask::getErrAction ),

on_set_int_int( MSG_MASK_RANGE_A, &servMask::setMaskHCursorAPos ),
on_get_int( MSG_MASK_RANGE_A, &servMask::getMaskHCursorAPos ),

on_set_int_int( MSG_MASK_RANGE_B, &servMask::setMaskHCursorBPos ),
on_get_int( MSG_MASK_RANGE_B, &servMask::getMaskHCursorBPos ),

on_set_int_int( MSG_MASK_RANGE_AB, &servMask::setMaskHCursorABPos ),
on_get_int( MSG_MASK_RANGE_AB, &servMask::getMaskHCursorABPos ),

//!user msg
on_set_int_pointer(servMask::cmd_mask_data_acq,    &servMask::setMaskData ),
on_get_pointer( servMask::cmd_mask_data_acq,    &servMask::getMaskData ),

on_get_pointer( servMask::cmd_mask_report, &servMask::getMaskReport ),
on_get_int_int( servMask::cmd_mask_info, &servMask::getMaskReportData ),

on_set_int_int( servMask::cmd_mask_state, &servMask::setState ),
on_get_int( servMask::cmd_mask_state, &servMask::getState ),

on_get_int_ptr( servMask::cmd_mask_imm_rule, &servMask::getMaskImmRule ),
on_get_int_ptr( servMask::cmd_mask_mask, &servMask::getMaskRule ),

//! spy on
on_set_void_void( servMask::cmd_on_hor_time_mode, &servMask::onHorTimeMode ),
on_set_void_void( servMask::cmd_on_ch_on_off,     &servMask::on_OnOffChX ),
on_set_void_void( servMask::cmd_on_ch_active,     &servMask::on_ChActive ),

on_set_void_void( servMask::cmd_on_hor_run,       &servMask::on_HorRun ),

//! sys msg
on_set_void_void( CMD_SERVICE_RST,               &servMask::rst ),
on_set_int_void( CMD_SERVICE_STARTUP,           &servMask::startup ),

on_set_void_int( CMD_SERVICE_TIMEOUT, &servMask::onTimer ),

//! test
on_set_int_bool( servMask::cmd_mask_test, &servMask::testMask ),
on_set_int_void( servMask::cmd_mask_report, &servMask::queryMaskReport ),

//scpi reset stat
on_set_int_void( servMask::cmd_scpi_reset_stat, &servMask::onScpiResetStat ),

on_set_int_ll( servMask::cmd_scpi_x_mask,        &servMask::setScpiXMask ),

on_get_ll    ( servMask::cmd_scpi_x_mask,        &servMask::getScpiXMask ),

on_set_int_ll( servMask::cmd_scpi_y_mask,        &servMask::setScpiYMask ),
on_get_int    ( servMask::cmd_scpi_y_mask,        &servMask::getScpiYMask ),

end_of_entry()

//! report by app used
maskReport::maskReport()
{
    mTotal = 0;
    mEvent = 0;
}

void maskReport::rst()
{
    mTotal = 0;
    mEvent = 0;
}

servMask::servMask(QString name, ServiceId eId):serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    mVersion = 0x10;

    mRuleWfmSize = 0;

    m_bEnable     = false;
    m_bLastEnable = false;

    m_nStart      = false;
    m_bShowStat   = false;

    m_nMaskSrc    = chan1;

    m_nRange      = mask_range_screen;
    mxStart       = 0;
    mxEnd         = trace_width - 1;

    m_nXMask      = X_MASK_DEF;
    m_nYMask      = Y_MASK_DEF;

    //! options
    mbAuxOut = true;
    mbAuxPulse = true;
    mAuxTime = time_us( 1 );
    mActionEvent = mask_action_on_fail;

    mErrAction = 0;

    mState = mask_closed;

    mOldEvent = 0;

    m_ChMask = 0x1e;
}

int servMask::serialOut( CStream &stream, serialVersion &ver )
{
    ver = mVersion;
    stream.write(QStringLiteral("m_bEnable"),     getEnable());
    stream.write(QStringLiteral("m_bShowStat"),   m_bShowStat);
    stream.write(QStringLiteral("m_nMaskSrc"),    m_nMaskSrc);
    stream.write(QStringLiteral("m_nRange"),      m_nRange);
    stream.write(QStringLiteral("mxStart"),       mxStart);
    stream.write(QStringLiteral("mxEnd"),         mxEnd);
    stream.write(QStringLiteral("m_nXMask"),      m_nXMask);
    stream.write(QStringLiteral("m_nYMask"),      m_nYMask);
    stream.write(QStringLiteral("mbAuxOut"),      mbAuxOut);
    stream.write(QStringLiteral("mbAuxPulse"),    mbAuxPulse);
    stream.write(QStringLiteral("mAuxTime"),      mAuxTime);
    stream.write(QStringLiteral("mActionEvent"),  mActionEvent);
    stream.write(QStringLiteral("mErrAction"),    mErrAction);
    stream.write(QStringLiteral("mState"),        mState);

    mRuleWfmSize = mLastRuleWfm.size();
    stream.write(QStringLiteral("mRuleWfmSize"),    mRuleWfmSize);

    return ERR_NONE;
}

int servMask::serialIn( CStream &stream, serialVersion ver )
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("m_bEnable"),     m_bEnable);
        stream.read(QStringLiteral("m_bShowStat"),   m_bShowStat);
        int temp = 0;
        stream.read(QStringLiteral("m_nMaskSrc"),    temp);
        m_nMaskSrc = (Chan)temp;

        stream.read(QStringLiteral("m_nRange"),      temp);
        m_nRange = (MaskRange)temp;

        stream.read(QStringLiteral("mxStart"),       mxStart);
        stream.read(QStringLiteral("mxEnd"),         mxEnd);
        stream.read(QStringLiteral("m_nXMask"),      m_nXMask);
        stream.read(QStringLiteral("m_nYMask"),      m_nYMask);
        stream.read(QStringLiteral("mbAuxOut"),      mbAuxOut);
        stream.read(QStringLiteral("mbAuxPulse"),    mbAuxPulse);
        stream.read(QStringLiteral("mAuxTime"),      mAuxTime);

        stream.read(QStringLiteral("mActionEvent"),  temp);
        mActionEvent = (MaskActionEvent)temp;

        stream.read(QStringLiteral("mErrAction"),    mErrAction);
        m_nStart = false;

        stream.read(QStringLiteral("mState"),        temp);
        mState = (maskState)temp;
        if( mState == mask_running)
        {
            mState = mask_stoped;
        }
        stream.read(QStringLiteral("mRuleWfmSize"),    mRuleWfmSize);
        mOldEvent = 0;
        m_bLastEnable = m_bLastEnable;
    }
    else
    {
        rst();
    }
    return ERR_NONE;
}

void servMask::rst()
{
    m_bEnable     = false;
    m_bLastEnable = false;

    m_nStart      = false;
    m_bShowStat   = false;

    m_bTimeout    = false;

    m_nMaskSrc    = chan1;

    m_nRange      = mask_range_screen;
    mxStart       = 0;
    mxEnd         = trace_width - 1;

    m_nXMask      = X_MASK_DEF;
    m_nYMask      = Y_MASK_DEF;

    //! options
    mbAuxOut = true;
    mbAuxPulse = true;
    mAuxTime = time_us( 1 );
    mActionEvent = mask_action_on_fail;

    mErrAction = 0;
    mOldEvent = 0;

    createMaskDef();

    mState = mask_closed;

    async(MSG_MASK_OPERATE,false);
    defer(MSG_MASK_RESET_STAT);
}

int servMask::startup()
{
    mUiAttr.setVisible( MSG_MASK_ERR_ACTION, 3, false);  //! Disable print(3)
    mUiAttr.setVisible( MSG_MASK_ERR_ACTION, 4, false);  //! Disable email(4)
    mUiAttr.setVisible( MSG_MASK_ERR_ACTION, 5, false);  //! Disable SRQ(5)

    toState( mState );

    //! for bug 1818, 1975
    async( MSG_MASK_ENABLE, m_bEnable);

    createMaskDef();

#if 0
    for ( int i = 0; i < array_count(mSubRuleL.mRule); i++ )
    {
        mSubRuleL.mRule[i] = 128-25;
        mSubRuleH.mRule[i] = 128+25;
    }
    mSubRuleL.mRule[0] = 0;
    mSubRuleH.mRule[0] = 255;

    mSubRuleL.mRule[999] = 0;
    mSubRuleH.mRule[999] = 255;

    mRule.attachRule( &mSubRuleL, 0 );
    mRule.attachRule( &mSubRuleH, 3 );

    mImmRule = mRule;

    LOG_DBG()<<"Finish build mRule for test--------------------------";
#endif

    onHorTimeMode();
    return ERR_NONE;
}

void servMask::registerSpy()
{
    spyOn( serv_name_hori, MSG_HOR_TIME_MODE, servMask::cmd_on_hor_time_mode );

    spyOn( serv_name_ch1, MSG_CHAN_ON_OFF, servMask::cmd_on_ch_on_off);
    spyOn( serv_name_ch2, MSG_CHAN_ON_OFF, servMask::cmd_on_ch_on_off);
    spyOn( serv_name_ch3, MSG_CHAN_ON_OFF, servMask::cmd_on_ch_on_off);
    spyOn( serv_name_ch4, MSG_CHAN_ON_OFF, servMask::cmd_on_ch_on_off);

    spyOn( serv_name_ch1, servVert::user_cmd_on_off, servMask::cmd_on_ch_on_off);
    spyOn( serv_name_ch2, servVert::user_cmd_on_off, servMask::cmd_on_ch_on_off);
    spyOn( serv_name_ch3, servVert::user_cmd_on_off, servMask::cmd_on_ch_on_off);
    spyOn( serv_name_ch4, servVert::user_cmd_on_off, servMask::cmd_on_ch_on_off);

    spyOn( serv_name_hori, MSG_HORIZONTAL_RUN, servMask::cmd_on_hor_run);

    spyOn( serv_name_vert_mgr,
           servVertMgr::cmd_active_chan,
           servMask::cmd_on_ch_active );
}

void servMask::on_ChActive()
{
    /*! [dba, 2018-04-13], bug:2863 */
    bool srcEn = mUiAttr.getOption(MSG_MASK_SOURCE, m_nMaskSrc)->getEnable();
    //qDebug()<<__FILE__<<__LINE__<<"SRC:"<<m_nMaskSrc<<"srcEn:"<<srcEn;

    if(!srcEn)
    {
        int actChan;
        serviceExecutor::query( serv_name_vert_mgr,
                                servVertMgr::cmd_active_chan,
                                actChan );


        if(  ((Chan)actChan >= chan1)&&((Chan)actChan <= chan4) )
        {
            if( get_bit( m_ChMask, actChan))
            {
                id_async( getId(), MSG_MASK_SOURCE, actChan);
            }
        }
    }
}

void servMask::on_OnOffCh( Chan ch)
{
    bool chEn = false;
    QString servId;

    if( ch == chan1)       servId = serv_name_ch1;
    else if( ch == chan2)  servId = serv_name_ch2;
    else if( ch == chan3)  servId = serv_name_ch3;
    else if( ch == chan4)  servId = serv_name_ch4;
    else                   servId = serv_name_ch1;

    serviceExecutor::query( servId, MSG_CHAN_ON_OFF, chEn);
    if( chEn)
    {
        m_ChMask |= ((int)1 << (int)ch);
    }
    else
    {
        m_ChMask &= ~((int)1 << (int)ch);

        if( (m_nMaskSrc == ch) && (mState == mask_running) )
        {
            id_async( getId(), MSG_MASK_OPERATE, false);
        }
    }

    mUiAttr.setEnable( MSG_MASK_SOURCE, ch, chEn);

    async(servMask::cmd_mask_state, getState());

    on_ChActive();
}

void servMask::on_OnOffChX()
{
    for( int ch = chan1; ch <= chan4; ch++)
    {
        on_OnOffCh((Chan)ch);
    }
}

void servMask::onHorTimeMode()
{
    int mode;
    serviceExecutor::query( serv_name_hori, MSG_HOR_TIME_MODE, mode);

    if(  ((AcquireTimemode)mode == Acquire_ROLL)
       ||((AcquireTimemode)mode == Acquire_XY))
    {
        //! close Mask
        if( !m_bLastEnable)
        {
            m_bLastEnable = m_bEnable;
        }
        if( m_bEnable)
        {
            if ( mState == mask_running )
            {
                id_async( getId(), MSG_MASK_OPERATE, false);
            }
            id_async( getId(), MSG_MASK_ENABLE, false);
        }

        mUiAttr.setEnable( MSG_MASK_ENABLE, false);
    }
    else if( (AcquireTimemode)mode == Acquire_YT)
    {
        mUiAttr.setEnable( MSG_MASK_ENABLE, true);
        //! recover Mask
        if( m_bLastEnable)
        {
            id_async( getId(), MSG_MASK_ENABLE, true);
            m_bLastEnable = false;
        }
    }
    else
    {
        return;
    }
}

void servMask::on_HorRun()
{
    int act;
    serviceExecutor::query( serv_name_hori, MSG_HORIZONTAL_RUN, act);

    if( (ControlAction)act == Control_Stop && m_bTimeout )
    {
        checkErrAction_PrtScr();
        m_bTimeout = false;
    }
}

DsoErr servMask::setEnable(bool t)
{
    m_bEnable = t;

    if(!t)
    {
        async( MSG_MASK_OPERATE, false );
    }

    //! [dba+, 2018-04-022, bug:2981 ]
    post(serv_name_hori,  servHori::cmd_apply_engine, 1);

    return ERR_NONE;
}

bool servMask::getEnable()
{
    return m_bEnable;
}

DsoErr servMask::setMaskSrc( Chan c)
{
    LOG_DBG()<<" servMask::setMaskSrc  "<<c;
    m_nMaskSrc = c;

    return  ssync(MSG_MASK_RESET_STAT, 0);
    //return postEngine( ENGINE_MASK_SOURCE, 1<<m_nMaskSrc );
}

Chan servMask::getMaskSrc()
{
    return m_nMaskSrc;
}

DsoErr servMask::setMaskOperator(bool s)
{
    if( s )
    {
        resetStat(0);
    }

    setMaskStart( s );
    setuiChange( MSG_MASK_OPERATE );
    return ERR_NONE;
}

DsoErr servMask::setMaskStart(bool s)
{
    if ( s )
    {
        int act;
        serviceExecutor::query( serv_name_hori, MSG_HORIZONTAL_RUN, act);

        if( (ControlAction)act == Control_Stop )
        {
            serviceExecutor::post( serv_name_hori,
                                   MSG_HORIZONTAL_RUN,
                                   (int)Control_Run);
        }
        startMasking();
        startTimer( mask_timer, 0, timer_repeat );
    }
    else
    {
        stopTimer( 0 );
        stopMasking();
    }

    m_nStart = s;

    return ERR_NONE;
}

bool servMask::getMaskStart()
{
    return m_nStart;
}

DsoErr servMask::setMaskRange( MaskRange r)
{
    m_nRange = r;

    updateRule();

    return ERR_NONE;
}

MaskRange servMask::getMaskRange()
{
    return m_nRange;
}

DsoErr servMask::setMaskHCursorAPos(int pos)
{
    LOG_DBG()<<" servMask::setMaskHCursorAPos  "<<pos;
    if ( pos == ( MASK_CURSOR_RANGE_Z ) )
    {
        setuiFocus( MSG_MASK_RANGE_B );
        return ERR_NONE;
    }

    DsoErr err;

    err = checkRange( pos, 0, mxEnd - 1 );

    mxStart = pos;

    updateRule();

    return err;
}
int servMask::getMaskHCursorAPos()
{
    setuiZVal( MASK_CURSOR_RANGE_Z );

    return mxStart;
}

DsoErr servMask::setMaskHCursorBPos(int pos)
{
    if ( pos == ( MASK_CURSOR_RANGE_Z ) )
    {
        setuiFocus( MSG_MASK_RANGE_AB );
        return ERR_NONE;
    }

    DsoErr err;

    err = checkRange( pos, mxStart+1, trace_width-1 );

    mxEnd = pos;

    updateRule();

    return err;
}
int servMask::getMaskHCursorBPos()
{
    setuiZVal( MASK_CURSOR_RANGE_Z );

    return mxEnd;
}

DsoErr  servMask::setMaskHCursorABPos(int pos)
{
    if ( pos == ( MASK_CURSOR_RANGE_Z ) )
    {
        setuiFocus( MSG_MASK_RANGE_A );
        return ERR_NONE;
    }

    DsoErr err = ERR_NONE;
    int pre;

    //! decrease
    if ( pos < 0 )
    {
        pre = mxStart;
        err = setMaskHCursorAPos( mxStart + pos );
        setMaskHCursorBPos( mxEnd + mxStart - pre );
    }
    else if ( pos > 0 )
    {
        pre = mxEnd;
        err = setMaskHCursorBPos( mxEnd + pos );
        setMaskHCursorAPos( mxStart + mxEnd - pre );
    }
    else
    {}

    return err;
}
int servMask::getMaskHCursorABPos()
{
    setuiZVal( MASK_CURSOR_RANGE_Z );

    return 0;
}

DsoErr servMask::setXMask(int value)
{
   LOG_DBG()<<"servMask::setXMask  "<<value;
    m_nXMask = value;

   return updateRule();
}

int servMask::getXMask()
{
    setuiZVal( X_MASK_DEF );
    setuiMaxVal(X_MASK_MAX);
    setuiMinVal(X_MASK_MIN);
    setuiUnit(Unit_Div);
    setuiBase(1, E_N2);     //! 1/100
    setuiStep( X_MASK_STEP );

    setuiFmt( MASK_TORLEANCE_FMT );

    return m_nXMask;
}

DsoErr servMask::setYMask(int value)
{
    LOG_DBG()<< "servMask::setYMask  "<<value;
    m_nYMask = value;

    return updateRule();
}

int servMask::getYMask()
{
    setuiZVal( Y_MASK_DEF );
    setuiMaxVal(Y_MASK_MAX);
    setuiMinVal(Y_MASK_MIN);
    setuiUnit(Unit_Div);
    setuiBase(4, E_N2 );     //! 1/25
    setuiStep( Y_MASK_STEP );

    setuiFmt( MASK_TORLEANCE_FMT );

    return m_nYMask;
}

DsoErr servMask::setShowStat(bool s)
{
    m_bShowStat = s;

    return ERR_NONE;
}

bool servMask::getShowStat()
{
    return m_bShowStat;
}

int servMask::resetStat(int)
{
    postEngine( ENGINE_MASK_RST );

    mReport.rst();

    mOldEvent = 0;

    return ERR_NONE;
}

void servMask::saveMask()
{
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_SAVE,
                      servStorage::FUNC_SAVE_MSK,
                      1,
                      getId());

    startIntent( serv_name_storage, intent );
}

void servMask::loadMask()
{
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_LOAD,
                      servStorage::FUNC_LOAD_MSK,
                      1,
                      getId());

    startIntent( serv_name_storage, intent );
}

int servMask::createMask()
{
    updateMask();
    return ERR_NONE;
}

void servMask::createMaskDef()
{
    for ( int i = 0; i < array_count(mSubRuleL.mRule); i++ )
    {
        mSubRuleL.mRule[i] = 128-25;
        mSubRuleH.mRule[i] = 128+25;
    }
    mSubRuleL.mRule[0] = 0;
    mSubRuleH.mRule[0] = 255;

    mSubRuleL.mRule[999] = 0;
    mSubRuleH.mRule[999] = 255;

    mRule.attachRule( &mSubRuleL, 0 );
    mRule.attachRule( &mSubRuleH, 3 );

    mImmRule = mRule;

   /*qDebug()<<__FILE__<<__FUNCTION__<<__LINE__
          <<"Finish build mRule for test--------------------------";*/
}

void* servMask::getMaskData()
{
    if( mRuleWfm.size() != 0)
    {
        return &mRuleWfm;
    }
    else
    {
        return NULL;
    }
}

int servMask::setMaskData(void*data)
{
    DsoErr err = ERR_NONE;
    if( NULL != data)
    {
        mRuleWfm = *((DsoWfm*)data);

        err = buildRule( mRuleWfm );
        if ( err != ERR_NONE )
        { return err; }

        //! save lastwfm
        mLastRuleWfm = mRuleWfm;

        mRule.attachRule( &mSubRuleL, 0 );
        mRule.attachRule( &mSubRuleH, 3 );

        mRule.setInfo( m_nMaskSrc,
                          mRuleWfm.getyGnd(),
                          mRuleWfm.getyDiv() );
        defer( servMask::cmd_mask_load );

        LOG_DBG()<<"--------------------------servMask::setMaskData data="<<data;
    }

    return err;
}

DsoErr servMask::setOutputOnOff( bool b )
{
    mbAuxOut = b;

    // for bug 2042, 2289
    id_async( E_SERVICE_ID_UTILITY, MSG_APP_UTILITY_AUXOUT, mbAuxOut);

    return ERR_NONE;
}

bool servMask::getOutputOnOff()
{
    int b = 0;

    // for bug 2042, 2289
    query( serv_name_utility, MSG_APP_UTILITY_AUXOUT, b);
    mbAuxOut = (bool)b;

    return mbAuxOut;
}

DsoErr servMask::setOutputPulse( bool bHL )
{
    mbAuxPulse = bHL;
    return ERR_NONE;
}
bool servMask::getOutputPulse()
{ return mbAuxPulse; }

//! pulse time
DsoErr servMask::setOutputTime( qlonglong ps )
{
    mAuxTime = ps;

    return ERR_NONE;
}
qlonglong servMask::getOutputTime()
{
    setuiRange( time_ns(100), time_ms(10), time_us(1) );
    setuiIStep( ll_roof10(mAuxTime)/100 );
    setuiDStep( ll_roof10L(mAuxTime)/100 );

    setuiUnit( Unit_s );
    setuiBase( 1, E_N12 );

    return mAuxTime;
}

DsoErr servMask::setOutputEvent( MaskActionEvent action )
{
    mActionEvent = action;

    return ERR_NONE;
}
MaskActionEvent servMask::getOutputEvent()
{
    return mActionEvent;
}

//! bitmaps
DsoErr servMask::setErrAction( quint32 act )
{
    //! set mask_prtscr true
    if( (get_bit( act, mask_prtscr)) &&(!get_bit( mErrAction, mask_prtscr)) )
    {
        if( !get_bit( act, mask_stop))
        {
            set_bit( act, mask_stop );
        }
    }

    //! set mask_stop false
    if( (!get_bit( act, mask_stop))&&(get_bit( mErrAction, mask_stop)) )
    {
        if( get_bit( act, mask_prtscr))
        {
            unset_bit( act, mask_prtscr)
        }
    }

    mErrAction = act;
    return ERR_NONE;
}
quint32 servMask::getErrAction()
{
    return mErrAction;
}

DsoErr servMask::setState( servMask::maskState stat )
{
    mState = stat;
    return ERR_NONE;
}
servMask::maskState servMask::getState()
{
    return mState;
}

int servMask::getMaskRule( EngineMaskRule *ptr )
{
    Q_ASSERT( NULL != ptr );

    *ptr = mRule;
    LOG_DBG()<<ptr;
    return 0;
}
int servMask::getMaskImmRule( EngineMaskRule *ptr )
{
    Q_ASSERT( NULL != ptr );

    *ptr = mImmRule;

    LOG_DBG()<<ptr;
    return 0;
}

void servMask::onTimer( int /*id*/ )
{
    //! in running
    if ( m_nStart )
    {
        m_bTimeout = true;
        snapReport();
        checkErrAction();
    }
    //! stop timer
    else
    {
    }
}

maskReport *servMask::getMaskReport()
{
    return &mReport;
}

int servMask::getMaskReportData(int num)
{
    int value = 0;
    switch (num)
    {
    case 0:
        value = mReport.mEvent;
        break;
    case 1:
        value = mReport.mTotal-mReport.mEvent;
        break;
    case 2:
        value = mReport.mTotal;
        break;
    default:
        value = mReport.mEvent;
        break;
    }
    return value;
}

DsoErr servMask::startMasking()
{
    LOG_DBG()<<"--------------mSubRuleH.mRule[0] = "<<mSubRuleH.mRule[0];
    LOG_DBG()<<"--------------mSubRuleH.mRule[100] = "<<mSubRuleH.mRule[100];
    LOG_DBG()<<"--------------mSubRuleH.mRule[300] = "<<mSubRuleH.mRule[300];
    LOG_DBG()<<"--------------mSubRuleH.mRule[500] = "<<mSubRuleH.mRule[500];
    LOG_DBG()<<"--------------mSubRuleH.mRule[700] = "<<mSubRuleH.mRule[700];
    LOG_DBG()<<"--------------mSubRuleH.mRule[900] = "<<mSubRuleH.mRule[900];
    LOG_DBG()<<"--------------mSubRuleH.mRule[999] = "<<mSubRuleH.mRule[999];
    //! source
    postEngine( ENGINE_MASK_SOURCE, 1<<m_nMaskSrc );

    //! mask options
    postEngine( ENGINE_MASK_RULE, &mRule );

//    postEngine( ENGINE_MASK_OUTPUT_ON,  mbAuxOut);

    postEngine( ENGINE_MASK_ACTION_EVENT, mActionEvent );

    if( get_bit( mErrAction, mask_stop))
    {
        postEngine( ENGINE_MASK_ACTION_ACTION, mask_action_stop );
        LOG_DBG()<<" postEngine( ENGINE_MASK_ACTION_ACTION, mask_action_stop )";
    }
    else
    {
        postEngine( ENGINE_MASK_ACTION_ACTION, mask_action_none );
        LOG_DBG()<<" postEngine( ENGINE_MASK_ACTION_ACTION, mask_action_none )";
    }

    postEngine( ENGINE_MASK_OUTPUT_HL, mbAuxPulse );

    postEngine( ENGINE_MASK_OUTPUT_TIME, mAuxTime );

    //! mask on
    // add by zx for bug:1521 fast
    int err = postEngine( ENGINE_MASK_ON, true );
    if(err != ERR_NONE)
    {
        LOG_DBG();
        /*! [dba: 2018-04-25] bug: 3053*/
        post(serv_name_hori,  servHori::cmd_apply_engine, 1);
    }

    return ERR_NONE;
}
DsoErr servMask::stopMasking()
{
    /*! [dba: 2018-04-22] bug: 2993*/
    postEngine( ENGINE_MASK_ACTION_ACTION, mask_action_none );

    //! mask off
    // add by zx for bug:1521 fast
    int err = postEngine( ENGINE_MASK_ON, false );

    if(err != ERR_NONE)
    {
        /*! [dba: 2018-04-22] bug: 2993*/
        if( !( get_bit( mErrAction, mask_stop) && (mReport.mEvent > 0) ) )
        {
            LOG_DBG();
            /*! [dba: 2018-04-25] bug: 3053*/
            post(serv_name_hori,  servHori::cmd_apply_engine, 1);
        }
    }
    return ERR_NONE;
}

DsoErr servMask::snapReport()
{
    EngineMaskReport report;
    EngineAdcCoreCH adcCH;

    queryEngine( qENGINE_MASK_REPORT, &report );
    queryEngine( qENGINE_ADC_CORE_CH, &adcCH);

    if( m_nMaskSrc == adcCH.mCHa)
    {
        mReport.mEvent = report.mA;
    }
    else if( m_nMaskSrc == adcCH.mCHb)
    {
        mReport.mEvent = report.mB;
    }
    else if( m_nMaskSrc == adcCH.mCHc)
    {
        mReport.mEvent = report.mC;
    }
    else
    {
        mReport.mEvent = report.mD;
    }

    mReport.mTotal = report.mTotal;

    defer( servMask::cmd_mask_update_report );

    return ERR_NONE;
}

void servMask::checkErrAction()
{
    if( qint64(mReport.mEvent - mOldEvent) > 0)
    {
        if( get_bit( mErrAction, mask_stop))
        {
            async(MSG_MASK_OPERATE, false);
        }

        if( get_bit( mErrAction, mask_beeper))
        {
            setBeep();
        }
    }

    mOldEvent = mReport.mEvent;
}

void servMask::checkErrAction_PrtScr()
{
    if( get_bit( mErrAction, mask_prtscr))
    {
        if( mReport.mEvent > 0)
        {
            post(serv_name_storage,
                 servStorage::FUNC_QUICK_SAVE_IMG,
                 servQuick::SaveImage);

//            if( !get_bit( mErrAction, mask_stop))
//            {
//                async(servMask::cmd_mask_operator, true);
//            }
        }
    }
}

void servMask::setBeep()
{
    syncEngine( ENGINE_BEEP_SHORT );
}

DsoErr servMask::updateRule()
{
    DsoErr err;

    err = snapPixel( mRuleWfm );
    if ( err != ERR_NONE )
    { return err; }

    err = buildRule( mRuleWfm );
    if ( err != ERR_NONE )
    { return err; }

    mImmRule.attachRule( &mSubRuleL, 0 );
    mImmRule.attachRule( &mSubRuleH, 3 );

    mImmRule.setInfo( m_nMaskSrc,
                      mRuleWfm.getyGnd(),
                      mRuleWfm.getyDiv() );

    return ERR_NONE;
}

DsoErr servMask::updateMask()
{
    DsoErr err;

    err = snapPixel( mRuleWfm );
    if ( err != ERR_NONE )
    { return err; }

    err = buildRule( mRuleWfm );
    if ( err != ERR_NONE )
    { return err; }

    //! save lastwfm
    mLastRuleWfm = mRuleWfm;

    mRule.attachRule( &mSubRuleL, 0 );
    mRule.attachRule( &mSubRuleH, 3 );

    mRule.setInfo( m_nMaskSrc,
                      mRuleWfm.getyGnd(),
                      mRuleWfm.getyDiv() );
    LOG_DBG()<<"--------------mSubRuleH.mRule[0] = "<<mSubRuleH.mRule[0];
    LOG_DBG()<<"--------------mSubRuleH.mRule[100] = "<<mSubRuleH.mRule[100];
    LOG_DBG()<<"--------------mSubRuleH.mRule[300] = "<<mSubRuleH.mRule[300];
    LOG_DBG()<<"--------------mSubRuleH.mRule[500] = "<<mSubRuleH.mRule[500];
    LOG_DBG()<<"--------------mSubRuleH.mRule[700] = "<<mSubRuleH.mRule[700];
    LOG_DBG()<<"--------------mSubRuleH.mRule[900] = "<<mSubRuleH.mRule[900];
    LOG_DBG()<<"--------------mSubRuleH.mRule[999] = "<<mSubRuleH.mRule[999];
    return err;
}

DsoErr servMask::snapPixel( DsoWfm &pixel )
{
    //! 1. chan
    dsoVert *pVert;
    pVert = dsoVert::getCH( m_nMaskSrc );
    if ( NULL == pVert )
    { return ERR_NULL_PTR; }

    //! 2. pixel
    pVert->getPixel( pixel );
    if ( pixel.size() < 1 )
    { return ERR_MASK_NO_TRACE; }

    return ERR_NONE;
}

DsoErr servMask::buildRule( DsoWfm &pixel )
{
    //! y change
    int y;
    dsoFract yDiv;

    yDiv = pixel.getyDiv();
    LOG_DBG()<<y<<m_nYMask<<yDiv.M()<<yDiv.N();

    //! delta y value
    Q_ASSERT(0 != yDiv.M());
    y = m_nYMask * adc_vdiv_dots * yDiv.N() / yDiv.M();

    LOG_DBG()<<"  buildRule  y= "<<y;

    //! 1. tolerance
    maskTolerance( pixel.getPoint(),
                   pixel.start(),
                   pixel.size(),

                   m_nXMask,
                   y,

                   mSubRuleH.mRule,
                   mSubRuleL.mRule,
                   adc_max + 1,
                   -1,
                   trace_width
                   );

    //! 2. trim range
    if ( m_nRange == mask_range_screen )  //! screen
    {}
    else                                  //! cursor(user)
    {
        //! head
        for ( int i = 0; i < mxStart; i++ )
        {
            mSubRuleH.mRule[i] = adc_max + 1;
            mSubRuleL.mRule[i] = -1;
        }

        //! tail
        for ( int i = trace_width -1; i >= mxEnd; i-- )
        {
            mSubRuleH.mRule[i] = adc_max + 1;
            mSubRuleL.mRule[i] = -1;
        }
    }

    return ERR_NONE;
}

void servMask::toState( maskState stat )
{
    async( servMask::cmd_mask_state, stat );
}

DsoErr servMask::testMask( bool bon )
{
    //! mask off
    if ( !bon )
    {
        // add by zx for bug:1521 fast
        int err = postEngine( ENGINE_MASK_ON, false );
        if(err != ERR_NONE)
        {
            LOG_DBG();
            /*! [dba: 2018-04-25] bug: 3053*/
            post(serv_name_hori,  servHori::cmd_apply_engine, 1);
        }
        return ERR_NONE;
    }

    //! create the mask
    EngineMaskRule mask;

    MaskRule rule0, rule3;

    for( int i = 0; i < 1000; i++ )
    {
        rule0.mRule[i] = 100;
        rule3.mRule[i] = 150;
    }

    mask.mChan = chan1;
    mask.attachRule( &rule0, 0 );
    mask.attachRule( &rule3, 3 );

    postEngine( ENGINE_MASK_RULE, &mask );

    //! source
    postEngine( ENGINE_MASK_SOURCE, (0xf<<chan1) );

    //! cross
//    postEngine( ENGINE_MASK_ACTION_EVENT, mask_action_on_fail );
    postEngine( ENGINE_MASK_ACTION_EVENT, mask_action_on_pass );

    //! output
    postEngine( ENGINE_MASK_OUTPUT_ON, true );

    //! H/L
//    postEngine( ENGINE_MASK_OUTPUT_HL, true );
    postEngine( ENGINE_MASK_OUTPUT_HL, false );

    //! pulse time
//    postEngine( ENGINE_MASK_OUTPUT_TIME, time_us(1) );
    postEngine( ENGINE_MASK_OUTPUT_TIME, time_us(2) );

    //! delay time
    postEngine( ENGINE_MASK_OUTPUT_DLY, time_us(2) );

    //! mask on
    // add by zx for bug:1521 fast
    int err = postEngine( ENGINE_MASK_ON, true );
    if(err != ERR_NONE)
    {
        LOG_DBG();
        /*! [dba: 2018-04-25] bug: 3053*/
        post(serv_name_hori,  servHori::cmd_apply_engine, 1);
    }
LOG_DBG();
    return ERR_NONE;
}

DsoErr  servMask::queryMaskReport()
{
    EngineMaskReport report;

    queryEngine( qENGINE_MASK_REPORT, &report );

    return ERR_NONE;
}

DsoErr  servMask::onScpiResetStat()
{
    async( MSG_MASK_RESET_STAT, 0);
    return ERR_NONE;
}

DsoErr servMask::setScpiXMask(qlonglong value)
{
    int temp;
    if(value >= X_MASK_MAX)
    {
        temp = X_MASK_MAX;
    }
    else
    {
        temp =  (int)value;
    }
    async( MSG_MASK_X_MASK, temp);
    return ERR_NONE;
}

qlonglong servMask::getScpiXMask()
{
    return getXMask();
}

DsoErr servMask::setScpiYMask(qint64 value)
{
    value = value/4;
    async( MSG_MASK_Y_MASK, (int)value );
    return ERR_NONE;
}

int servMask::getScpiYMask()
{
    return getYMask()*4;
}
