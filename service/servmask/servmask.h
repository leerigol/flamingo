#ifndef SERVMASK_H
#define SERVMASK_H

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../../include/dsostd.h"
#include "../service_msg.h"
#include "../servplot/servplot.h"

#include "../../engine/base/enginecfg.h"

class maskReport
{
public:
    maskReport();
    void rst();

public:
    quint64 mEvent, mTotal;
};

class servMask: public serviceExecutor,
                public ISerial
{
    Q_OBJECT

    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    enum maskState
    {
        mask_disabled,
        mask_closed,
        mask_stoped,
        mask_running,
    };

    enum enumRangeType
    {
        RANGE_TYPE_SCREEN,
        RANGE_TYPE_CURSOR,
    };

    enum eMaskErrAction
    {
        mask_stop,
        mask_beeper,
        mask_prtscr,
        mask_print,
        mask_mail,
        mask_srq,
    };

    enum enumCMD
    {
        cmd_base = 0,

        cmd_mask_test,
        cmd_mask_report,        

        cmd_mask_update_report,
        cmd_mask_state,
        cmd_mask_imm_rule,
        cmd_mask_mask,

        cmd_mask_data_acq,
        cmd_mask_load,

        cmd_on_hor_time_mode,

        cmd_on_ch_on_off,
        cmd_on_hor_run,

        //scpi
        cmd_scpi_reset_stat,
        cmd_scpi_x_mask,
        cmd_scpi_y_mask,

        cmd_on_ch_active,
        cmd_mask_operator,

        cmd_mask_info,

    };

public:
    servMask(QString name, ServiceId eId = E_SERVICE_ID_MASK );

public:
    int  serialOut( CStream &stream, serialVersion &ver );
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();
    void registerSpy();

    void onHorTimeMode();
    void on_HorRun();
    void on_OnOffCh(Chan ch);

    void on_OnOffChX();
    void on_ChActive();

public:
    DsoErr  setEnable(bool);
    bool    getEnable();

    DsoErr  setMaskSrc( Chan );
    Chan    getMaskSrc();

    DsoErr  setMaskOperator(bool);
    DsoErr  setMaskStart(bool);
    bool    getMaskStart();

    //! range
    DsoErr  setMaskRange( MaskRange rng );
    MaskRange getMaskRange();

    DsoErr  setMaskHCursorAPos(int pos);
    int     getMaskHCursorAPos();

    DsoErr  setMaskHCursorBPos(int pos);
    int     getMaskHCursorBPos();

    DsoErr  setMaskHCursorABPos(int pos);
    int     getMaskHCursorABPos();

    //! tolerance
    DsoErr  setXMask(int value);
    int     getXMask();

    DsoErr  setYMask(int value);
    int     getYMask();

    DsoErr  setShowStat(bool bMaskSta);
    bool    getShowStat();

    int     resetStat(int);

    void    saveMask();
    void    loadMask();

    int     setMaskData(void*data);
    void*   getMaskData();
    int     createMask();
    void    createMaskDef();

    //! options
    DsoErr setOutputOnOff( bool b );
    bool   getOutputOnOff();

    DsoErr setOutputPulse( bool bHL );
    bool   getOutputPulse();

    DsoErr    setOutputTime( qlonglong ps );
    qlonglong getOutputTime();

    DsoErr setOutputEvent( MaskActionEvent action );
    MaskActionEvent getOutputEvent();

    DsoErr setErrAction( quint32 act );
    quint32 getErrAction();

    DsoErr setState( maskState stat );
    maskState getState();

    int getMaskRule( EngineMaskRule * ptr );
    int getMaskImmRule( EngineMaskRule * ptr );

protected:
    void on_post_mask_enable();
    void on_post_mask_operate();
    void on_post_mask_state();
protected:
    void onTimer( int id );

    maskReport* getMaskReport();
    int getMaskReportData(int num);

protected:
    DsoErr startMasking();
    DsoErr stopMasking();

    DsoErr snapReport();

    void checkErrAction();
    void checkErrAction_PrtScr();

    DsoErr updateRule();
    DsoErr updateMask();
    DsoErr snapPixel( DsoWfm &pixel );
    DsoErr buildRule( DsoWfm &pixel );

    void toState( maskState stat );

    DsoErr  testMask( bool bON );
    DsoErr  queryMaskReport();

    //scpi
    DsoErr  onScpiResetStat();


    DsoErr  setScpiXMask(qlonglong value);
    qlonglong     getScpiXMask();

   // DsoErr  setScpiXMask(qint64 value);
  //  int     getScpiXMask();


    DsoErr  setScpiYMask(qint64 value);
    int     getScpiYMask();


    void setBeep();

private:
    bool    m_bEnable;
    bool    m_bLastEnable;

    bool    m_nStart;
    bool    m_bShowStat;

    Chan    m_nMaskSrc;
    int     m_ChMask; //! for MaskSrc On/Off

    MaskRange m_nRange;
    int mxStart, mxEnd;

    quint64 mOldEvent;

    //! tolerance
    int m_nXMask;
    int m_nYMask;

    //! mask
    MaskRule mSubRuleL,mSubRuleH;
    EngineMaskRule mRule,mImmRule;
    maskReport mReport;
    DsoWfm mRuleWfm, mLastRuleWfm;

    //! options
    bool mbAuxOut;
    bool mbAuxPulse;
    qlonglong mAuxTime;
    MaskActionEvent mActionEvent;

    quint32 mErrAction;

    //! status
    maskState mState;

    //! for serialIn
    int mRuleWfmSize;

    bool m_bTimeout;

};
#endif // SERVMASK_H

