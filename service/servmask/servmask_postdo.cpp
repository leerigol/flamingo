#include "servmask.h"

IMPLEMENT_POST_DO( serviceExecutor, servMask )
start_of_postdo()
on_postdo( MSG_MASK_ENABLE,          &servMask::on_post_mask_enable ),
on_postdo( MSG_MASK_OPERATE,         &servMask::on_post_mask_operate ),
on_postdo( servMask::cmd_mask_state, &servMask::on_post_mask_state ),

on_postdo( MSG_MASK_CREATE,          &servMask::on_post_mask_state ),
on_postdo( MSG_MASK_LOAD,            &servMask::on_post_mask_state ),
end_of_postdo()

void servMask::on_post_mask_enable()
{
    if ( m_bEnable )
    {
        async( servMask::cmd_mask_state, servMask::mask_stoped );
    }
    else
    {
        //async( servMask::cmd_mask_state, servMask::mask_disabled );
    }
}
void servMask::on_post_mask_operate()
{
    if ( m_nStart )
    {
        async( servMask::cmd_mask_state, servMask::mask_running );
    }
    else
    {
        if( m_bEnable )
        {
            async( servMask::cmd_mask_state, servMask::mask_stoped );
        }
        else
        {
            async( servMask::cmd_mask_state, servMask::mask_disabled );
        }
    }
}

void servMask::on_post_mask_state()
{
//    qDebug()<<"servMask::on_post_mask_state  mState == "<<mState;
    //! disabled
    if ( mState == mask_disabled )
    {
        mUiAttr.setEnable( MSG_MASK_SOURCE, false);
        mUiAttr.setEnable( MSG_MASK_OPERATE, false);
        mUiAttr.setEnable( MSG_MASK_RANGE, false);
        mUiAttr.setEnable( MSG_MASK_RANGE_A, false);
        mUiAttr.setEnable( MSG_MASK_RANGE_B, false);
        mUiAttr.setEnable( MSG_MASK_RANGE_AB, false);
        mUiAttr.setEnable( MSG_MASK_X_MASK, false);
        mUiAttr.setEnable( MSG_MASK_Y_MASK, false);
        mUiAttr.setEnable( MSG_MASK_SHOW_STAT, false);
        mUiAttr.setEnable( MSG_MASK_RESET_STAT, false);
        mUiAttr.setEnable( MSG_MASK_SAVE, false);
        mUiAttr.setEnable( MSG_MASK_LOAD, false);
        mUiAttr.setEnable( MSG_MASK_CREATE, false);
        mUiAttr.setEnable( MSG_MASK_OUT_EVENT, false);
        mUiAttr.setEnable( MSG_MASK_OUT_ONOFF, false);
        mUiAttr.setEnable( MSG_MASK_OUT_PULSE, false);
        mUiAttr.setEnable( MSG_MASK_OUT_HL, false);
        mUiAttr.setEnable( MSG_MASK_ERR_ACTION, false);
    }
    else if ( mask_closed == mState )
    {
        mUiAttr.setEnable( MSG_MASK_SOURCE, false);
        mUiAttr.setEnable( MSG_MASK_OPERATE, false);
        mUiAttr.setEnable( MSG_MASK_RANGE, false);
        mUiAttr.setEnable( MSG_MASK_RANGE_A, false);
        mUiAttr.setEnable( MSG_MASK_RANGE_B, false);
        mUiAttr.setEnable( MSG_MASK_RANGE_AB, false);
        mUiAttr.setEnable( MSG_MASK_X_MASK, false);
        mUiAttr.setEnable( MSG_MASK_Y_MASK, false);
        mUiAttr.setEnable( MSG_MASK_SHOW_STAT, false);
        mUiAttr.setEnable( MSG_MASK_RESET_STAT, false);
        mUiAttr.setEnable( MSG_MASK_SAVE, false);
        mUiAttr.setEnable( MSG_MASK_LOAD, false);
        mUiAttr.setEnable( MSG_MASK_CREATE, false);
        mUiAttr.setEnable( MSG_MASK_OUT_EVENT, false);
        mUiAttr.setEnable( MSG_MASK_OUT_ONOFF, false);
        mUiAttr.setEnable( MSG_MASK_OUT_PULSE, false);
        mUiAttr.setEnable( MSG_MASK_OUT_HL, false);
        mUiAttr.setEnable( MSG_MASK_ERR_ACTION, false);
    }
    else if ( mask_stoped == mState )
    {
        mUiAttr.setEnable( MSG_MASK_RANGE, true);
        mUiAttr.setEnable( MSG_MASK_RANGE_A, true);
        mUiAttr.setEnable( MSG_MASK_RANGE_B, true);
        mUiAttr.setEnable( MSG_MASK_RANGE_AB, true);
        mUiAttr.setEnable( MSG_MASK_X_MASK, true);
        mUiAttr.setEnable( MSG_MASK_Y_MASK, true);
        mUiAttr.setEnable( MSG_MASK_SHOW_STAT, true);
        mUiAttr.setEnable( MSG_MASK_RESET_STAT, true);
        mUiAttr.setEnable( MSG_MASK_LOAD, true);
        mUiAttr.setEnable( MSG_MASK_CREATE, true);
        mUiAttr.setEnable( MSG_MASK_OUT_EVENT, true);
        mUiAttr.setEnable( MSG_MASK_OUT_ONOFF, true);
        mUiAttr.setEnable( MSG_MASK_OUT_PULSE, true);
        mUiAttr.setEnable( MSG_MASK_OUT_HL, true);
        mUiAttr.setEnable( MSG_MASK_ERR_ACTION, true);
        mUiAttr.setEnable( MSG_MASK_CREATE_MENU, true);

        mUiAttr.setEnable( MSG_MASK_SAVE, 0 != mRuleWfm.size());

        if( 0 != m_ChMask)
        {
            mUiAttr.setEnable( MSG_MASK_OPERATE, true);
            mUiAttr.setEnable( MSG_MASK_SOURCE,  true);
        }
        else
        {
            mUiAttr.setEnable( MSG_MASK_OPERATE, false);
            mUiAttr.setEnable( MSG_MASK_SOURCE,  false);
        }
    }
    else if ( mask_running == mState )
    {
         mUiAttr.setEnable( MSG_MASK_SOURCE, false);
         mUiAttr.setEnable( MSG_MASK_RANGE, false);
         mUiAttr.setEnable( MSG_MASK_RANGE_A, false);
         mUiAttr.setEnable( MSG_MASK_RANGE_B, false);
         mUiAttr.setEnable( MSG_MASK_RANGE_AB, false);
         mUiAttr.setEnable( MSG_MASK_X_MASK, false);
         mUiAttr.setEnable( MSG_MASK_Y_MASK, false);
         mUiAttr.setEnable( MSG_MASK_LOAD, false);
         mUiAttr.setEnable( MSG_MASK_CREATE, false);
         mUiAttr.setEnable( MSG_MASK_OUT_EVENT, false);
         mUiAttr.setEnable( MSG_MASK_OUT_ONOFF, false);
         mUiAttr.setEnable( MSG_MASK_OUT_PULSE, false);
         mUiAttr.setEnable( MSG_MASK_OUT_HL, false);
         mUiAttr.setEnable( MSG_MASK_ERR_ACTION, false);
         mUiAttr.setEnable( MSG_MASK_CREATE_MENU, false);

         mUiAttr.setEnable( MSG_MASK_SAVE, 0 != mRuleWfm.size());
    }
    else
    {}
}
