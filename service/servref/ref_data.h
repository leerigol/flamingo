#ifndef _REF_DATA_H
#define _REF_DATA_H

#include "../../service/servtrace/dataprovider.h"
#include "../../baseclass/dsovert.h"
#include "../../include/dsocfg.h"
#include<QColor>

#define DEBUG_TEST()   //qDebug()<< __FILE__ << __FUNCTION__<<__LINE__

class CRefData : public dsoVert
{

public:
    CRefData(int ch = 0);

public:
     void       getPixel(DsoWfm &wfm, Chan subChan = chan_none, HorizontalView view = horizontal_view_main);
     void       getTrace(DsoWfm &wfm, Chan subChan = chan_none, HorizontalView = horizontal_view_main);

public:
    DsoErr      setSrc(Chan);
    Chan        getSrc();

    DsoErr      setVScale(qlonglong );
    qlonglong   getVScale();

    DsoErr      setVOffset(qlonglong );
    qlonglong   getVOffset();

    DsoErr      setHScale(qlonglong );
    qlonglong   getHScale();

    DsoErr      setHOffset(qlonglong );
    qlonglong   getHOffset();

    DsoErr      setColorIndex(int c);
    int         getColorIndex();

    DsoErr      setLabelIndex(int );
    int         getLabelIndex();

    DsoErr      setRefLabel(QString );
    QString     getRefLabel();

    void        setValid(bool b) { m_bValid = b;}
    bool        isValid()       { return m_bValid;}

    void        saveRefTime();
    void        setRefTime(QString s) { m_RefTime = s;}
    QString     getRefTime() const;

    void        setUnit(QString u);
    QString     getUnit();

    DsoErr      setOnOff(bool);
    bool        getOnOff();

    void        setOrigData(DsoWfm*);
    DsoWfm*     getOrigData();

    void        setViewData(DsoWfm*);
    DsoWfm*     getViewData();

    void        setDrawData(DsoWfm*);
    DsoWfm*     getDrawData();

    Chan        getRefID() const {  return yChId; }

    qint64      getOrigHScale();
    qint64      getOrigHOffset();
    qint64      getOrigVScale();
    qint64      getOrigVOffset();

    void        setOrigVScale(qint64);
    void        setOrigVOffset(qint64);
    void        setOrigHScale(qint64);
    void        setOrigHOffset(qint64);

public:
    void        setyGnd( int gnd );
    void        setyInc( dsoFract fract, DsoReal real );

public:
    Chan m_nSaveSrc;

private:
    DsoWfm      mOrigWfm; //original data
    DsoWfm      mViewWfm;

    DsoWfm      mDrawWfm;

    Chan        m_nSrc;
    int         m_nColorIndex;

    QString     m_Label;
    int         m_nLabelIndex;

    qint64      m_s64VScale;
    qint64      m_s64VOffset;

    qint64      m_s64HScale;
    qint64      m_s64HOffset;

    qint64      m_s64OrigVScale;
    qint64      m_s64OrigVOffset;

    qint64      m_s64OrigHScale;
    qint64      m_s64OrigHOffset;

    QString     m_RefTime;
    QString     m_Unit;

    //ref channel is open
    bool        bOpen;
    bool        m_bValid; //data is ready?

};

#endif // DSOREF_H
