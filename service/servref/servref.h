#ifndef SERVREF_H
#define SERVREF_H

#include "../../service/servtrace/dataprovider.h"
#include "../../baseclass/iserial.h"
#include "../servplot/servplot.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "../service.h"
#include "../servvert/servvert.h"
#include "ref_data.h"

#define REF_COUNT (10)

#define ref_update_timer_tmo  1000  //! ms
#define ref_update_timer_id    6

class servRef : public servVert,
                public ISerial,
                public dsoVert
{
    Q_OBJECT

    DECLARE_CMD()

public:
        typedef enum
        {
            cmd_none = 0,//CMD_APP_BASE,
            MSG_REF_SHOW,
            MSG_REF_CLEAR_ALL,
            MSG_REF_HOR_SCALE,
            MSG_REF_HOR_OFFSET,
            MSG_REF_ZOOM_MODE,
            MSG_REF_HOR_MODE,

            MSG_REF_VOR_SCALE,
            MSG_REF_HALF_FFT,

            MSG_REF_SRC_CHAN1,
            MSG_REF_SRC_CHAN2,
            MSG_REF_SRC_CHAN3,
            MSG_REF_SRC_CHAN4,

            MSG_REF_SRC_MATH1,
            MSG_REF_SRC_MATH2,
            MSG_REF_SRC_MATH3,
            MSG_REF_SRC_MATH4,

            MSG_REF_SRC_LA,
            MSG_REF_UPDATED_TRACE,

            //bug 1837 by zy  添加active指令
            SCPI_REF_ACTIVE,

            SCPI_REF_ENABLE,
            SCPI_REF_CURRENT,
            SCPI_REF_SOURCE,
            SCPI_REF_VSCALE,
            SCPI_REF_VOFFSET,
            SCPI_REF_COLOR,
            SCPI_REF_LABEL,
            SCPI_REF_RESET,
            SCPI_REF_SAVE,
        }enumCmd;

        enum
        {
            REF_COLOR_GRAY,
            REF_COLOR_GREEN,
            REF_COLOR_BLUE,
            REF_COLOR_RED,
            REF_COLOR_ORANGE,
            REF_COLOR_ALL
        };

public:
    servRef( QString name, ServiceId eId = E_SERVICE_ID_REF );
    static CRefData* getRefData(int ch = -1);//-1 get the current channel
    static QColor    getColor(int);

protected:
    int  serialIn(  CStream &, serialVersion   );
    int  serialOut( CStream &, serialVersion & );
    void init();
    void rst();
    int  startup();
    void registerSpy();
    virtual bool keyTranslate(int key, int &msg, int &controlAction);

private:
    DsoErr      setChanID(int);
    int         getChanID();

    DsoErr      setSrc(int);
    int         getSrc();

    DsoErr      setVScale(qlonglong);
    qlonglong   getVScale();

    DsoErr      setVOffset(qlonglong);
    qlonglong   getVOffset();

    DsoErr      setHScale(qlonglong);
    qlonglong   getHScale();

    DsoErr      setHOffset(qlonglong);
    qlonglong   getHOffset();

    DsoErr      setColorIndex(int);
    int         getColorIndex();

    DsoErr      setShowLabel(bool b);
    bool        getShowLabel();

    DsoErr      setDispType(bool b);
    bool        getDispType();

    DsoWfm*     getOrigData();
    DsoWfm*     getViewData();

    QString     getRefTime() const;

    DsoErr      setRefDetails(bool);
    bool        getRefDetails();

    DsoErr      setLabelIndex(int labelIndex);
    int         getLabelIndex();

    int         setLabel(QString&);
    QString     getLabel();

    DsoErr      clear();
    DsoErr      save();
    DsoErr      load();
    DsoErr      reset();

    bool        saveToMem(bool isNew = true);//false:reset
    bool        saveToFile( int id = -1);
    void        sendToShow(int id, bool b = true); //default is show
    void        onViewChanged();

    void        doModify(int id = -1, bool need = false);

    void        setRefOperEn(bool b);

    int         showLabel();

    DsoErr      exportRef();
    DsoErr      importRef();

    int         onShow(int);
    bool        getShow();

    int         onDeActive(int);
    int         onActive(int);
    int         onTimeOut(int);

    bool        readData( int id = -1 );
    void        addWaveKeys();

//couple with other APP
    int         onClear();
    int         onHorScale(qint64 scale);
    int         onHorMode(int);
    int         getHorMode();

    int        onVorScale();

    int        onHalfFFT();

    int         onChan1Enable(bool);
    int         onChan2Enable(bool);
    int         onChan3Enable(bool);
    int         onChan4Enable(bool);

    int         onMath1Enable(bool);
    int         onMath2Enable(bool);
    int         onMath3Enable(bool);
    int         onMath4Enable(bool);

    int         onLaEnable(int);

    void      onTraceEnable(void);

    void      findChan(int except, bool en);
    void      changeFftRange(qlonglong &, qlonglong&);

//SCPI api
    int        setRefActive(int msg);

    int         setRefEnable(CArgument&);
    bool        getRefEnable(CArgument&);

    int         setRefSource(CArgument&);
    int         getRefSource(CArgument&);

    int         setRefVScale(CArgument&);
    qint64      getRefVScale(CArgument&);

    int         setRefVOffset(CArgument&);
    qint64      getRefVOffset(CArgument&);

    int         setRefColor(CArgument&);
    int         getRefColor(CArgument&);

    int         setRefLabel(CArgument&);
    QString     getRefLabel(CArgument&);

    int         setRefReset(int);
    int         setRefSave(int);
    int         setRefCurrent(int);

private:
    static QList<CRefData*> m_RefData;
    static QColor m_ColorIndex[];
    static QList<QString> m_RefLabel;
    static serviceKeyMap  m_WaveKeyMap;

    QSemaphore  m_Sem;
    plotContext m_plotContext;

    static int  m_nChanID;
    CRefData*   m_pChanData;

    bool        m_bDispType;
    bool        m_bDetail;
    //label is shown
    bool        m_bShowLabel;

    bool        m_bOpen;
    bool        m_bScaleFlag;
    bool        m_bUpdata;
    bool        m_bInvert;
    int           m_nHorMode;//couple module active
    qint64     m_nChanMask;
    qlonglong     m_nStart;
    qlonglong     m_nSpan;

    int             m_nGnd;
    qlonglong m_nInc;
    //qlonglong m_nLastInc;

};
#endif //SERVCH_H
