#include "rigol_bin.h"
#include <QDebug>
#include <QtGlobal>
#include "../../include/dsoassert.h"

const char PB_COOKIE[2] = {'R', 'L'};
const char PB_VERSION[2] = {'1', '0'};
char *ScopeSerials = "MSO-X 7054A:MY5124573211";


rigolbin::rigolbin()
{
    pBinFileHeader         = new BinFileHeader;
    pBinWaveformHeader     = new BinWaveformHeader;
    pBinWaveformDataHeader = new BinWaveformDataHeader;

}

void rigolbin::loadQFile(QFile &binFile, QList<DsoWfm>&chList, QStringList label)
{
    int labelIterator = 0;

    if(binFile.open(QIODevice::WriteOnly))
    {
        //! build header
        buildpBinFileHeader(binFile, chList);

        //! store data in Segment
        foreach(DsoWfm wfm, chList)
        {
            float *data = new float[wfm.size()];
            m_label = list_at( label,labelIterator++);

            for(int i=0; i<wfm.size(); i++)
            {
                data[i] = wfm.getValue()[i];

            }

            buildpBinWaveformHeader(wfm);
            buildpBinWaveformDataHeader(wfm);

            //! link data with header
            LinkWaveFormData((const char *)data, binFile);

            delete []data;
            data = NULL;


        }

        labelIterator = 0;
    }

    binFile.close();

}

void rigolbin::buildpBinFileHeader(QFile &binFile, QList<DsoWfm > &chList)
{
    int       chNUM    = 0;
    qlonglong datasize = 0;

    Q_ASSERT(0 !=chList.count());

    foreach(DsoWfm wfm, chList)
    {
        if(0 != wfm.size())
        {
            chNUM ++;
            datasize += sizeof( *wfm.getValue()) * wfm.size(); // calc data size
        }

    }

    qlonglong filesize = sizeof(BinFileHeader) + (sizeof(BinWaveformDataHeader) +
                         sizeof(BinWaveformHeader)) * chNUM + datasize;

    memcpy(pBinFileHeader->Cookie,  PB_COOKIE,  COOKIE_VERSION_LENGTH);
    memcpy(pBinFileHeader->Version, PB_VERSION, COOKIE_VERSION_LENGTH);
    pBinFileHeader->FileSize = filesize;
    pBinFileHeader->NumberOfWaveforms = chNUM;

    binFile.write((const char *)pBinFileHeader, sizeof(BinFileHeader));
}

void rigolbin::buildpBinWaveformHeader(DsoWfm &wfm)
{
    QString WaveFormLabel   = m_label;
    char   *pWaveFormLabel  = NULL;
    QByteArray ba  = WaveFormLabel.toLatin1();
    pWaveFormLabel = ba.data();

    pBinWaveformHeader->HeaderSize       = sizeof(BinWaveformHeader);
    pBinWaveformHeader->WaveformType     = PB_NORMAL;
    pBinWaveformHeader->NWaveformBuffers = 1;
    pBinWaveformHeader->Points           = wfm.size();
    pBinWaveformHeader->Count            = 0;

    pBinWaveformHeader->XDisplayRange    = wfm.size() * wfm.gettInc();
    pBinWaveformHeader->XDisplayOrigin   = wfm.gett0();
    pBinWaveformHeader->XIncrement       = wfm.gettInc();
    pBinWaveformHeader->XOrigin          = wfm.gett0();

    pBinWaveformHeader->XUnits = 2;
    pBinWaveformHeader->YUnits = 1;

    memset(pBinWaveformHeader->Date, 0,DATE_TIME_STRING_LENGTH);
    memset(pBinWaveformHeader->Time, 0,DATE_TIME_STRING_LENGTH);

    memcpy(pBinWaveformHeader->Frame, ScopeSerials,FRAME_STRING_LENGTH);
    memcpy(pBinWaveformHeader->WaveformLabel, pWaveFormLabel,SIGNAL_STRING_LENGTH);

    pBinWaveformHeader->TimeTag      = 0.0;
    pBinWaveformHeader->SegmentIndex = 1;

}

void rigolbin::buildpBinWaveformDataHeader(DsoWfm &wfm)
{

    pBinWaveformDataHeader->HeaderSize    = sizeof(BinWaveformDataHeader);
    pBinWaveformDataHeader->BufferType    = PB_DATA_NORMAL;
    pBinWaveformDataHeader->BytesPerPoint = sizeof(*wfm.getValue());
    pBinWaveformDataHeader->BufferSize    =  pBinWaveformDataHeader->BytesPerPoint * wfm.size();

}

void rigolbin::LinkWaveFormData(const char *data, QFile &BinFile)
{

   BinFile.write((const char *)pBinWaveformHeader, sizeof(BinWaveformHeader));
   BinFile.write((const char *)pBinWaveformDataHeader, sizeof(BinWaveformDataHeader));
   BinFile.write(data, pBinWaveformHeader->Points * pBinWaveformDataHeader->BytesPerPoint);

}
