#include "../../service/servhori/servhori.h"
#include "ref_data.h"
#include "../../baseclass/dsoreal.h"
CRefData::CRefData(int ch) : dsoVert((Chan)(r1+ch))
{
    m_nSrc           = chan1;
    m_nColorIndex    = 0;
    m_nLabelIndex    = 0;
    m_s64VScale      = 1000000;
    m_s64VOffset     = 0;
    m_s64HScale      = 500;
    m_s64HOffset     = 0;
    m_RefTime        = "";
    m_bValid         = false;

    m_Unit           = "V";
    dsoVert::yChlabel = QString("REF%1").arg(ch+1);
    dsoVert::yOnOff   = false;
    dsoVert::yChId    = (Chan)(r1+ch);
    dsoVert::setyZone( time_zone );
}

void CRefData::getPixel(DsoWfm &wfm, Chan /*subChan*/, HorizontalView /*view*/ )
{
    wfm = mDrawWfm;
}

void CRefData::getTrace(DsoWfm &wfm, Chan /*subChan*/ , HorizontalView /*view*/)
{
     if(getOnOff())
     {
         DsoWfm *pWfm = getViewData();
         pWfm->m_wfmMutex.lock();
         wfm  = *pWfm;
         wfm.setAttrId( servHori::getAttrId() );
         pWfm->m_wfmMutex.unlock();
     }
     else
     {
         wfm.setRange(0, 0);
     }
}

DsoErr CRefData::setSrc(Chan ch)
{
    m_nSrc = ch;
    return ERR_NONE;
}

Chan CRefData::getSrc()
{
    return m_nSrc;
}

DsoErr CRefData::setColorIndex(int c)
{
    m_nColorIndex = c;
    return ERR_NONE;
}

int CRefData::getColorIndex()
{
    return m_nColorIndex;
}

DsoErr CRefData::setLabelIndex(int labelIndex)
{
   m_nLabelIndex = labelIndex;
   return ERR_NONE;
}

int  CRefData::getLabelIndex()
{
   return m_nLabelIndex;
}

DsoErr CRefData::setRefLabel(QString label)
{
    dsoVert::yChlabel = label;
    return ERR_NONE;
}

QString CRefData::getRefLabel()
{
    return dsoVert::yChlabel;
}

DsoErr CRefData::setVScale(qlonglong newScale)
{
    DEBUG_TEST();
    if(newScale != m_s64VScale)
    {
        m_s64VScale = newScale;
    }
    return ERR_NONE;
}

qlonglong CRefData::getVScale()
{
    return m_s64VScale;
}

DsoErr CRefData::setVOffset(qlonglong newOffset)
{
    DEBUG_TEST();
    if(newOffset != m_s64VOffset)
    {
        m_s64VOffset = newOffset;
    }
    return ERR_NONE;
}

qlonglong CRefData::getVOffset()
{
    return m_s64VOffset;
}

DsoErr CRefData::setHScale(qlonglong newScale)
{
    DEBUG_TEST();
    if(newScale != m_s64HScale)
    {
        m_s64HScale = newScale;
        mViewWfm.setlltInc( newScale /adc_hdiv_dots );
    }
    return ERR_NONE;
}

qlonglong CRefData::getHScale()
{
    return m_s64HScale;
}

DsoErr CRefData::setHOffset(qlonglong newOffset)
{
    DEBUG_TEST();
    if(newOffset != m_s64HOffset)
    {
        m_s64HOffset = newOffset;
        mViewWfm.setllt0( m_s64HOffset * adc_hdiv_dots / m_s64HScale );
    }
    return ERR_NONE;
}

qlonglong CRefData::getHOffset()
{
    return m_s64HOffset;
}

void CRefData::saveRefTime()
{
    m_RefTime = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
}

QString CRefData::getRefTime() const
{
    return m_RefTime;
}

DsoErr CRefData::setOnOff(bool b)
{
    dsoVert::yOnOff = b;
    return ERR_NONE;
}

bool CRefData::getOnOff()
{
    return dsoVert::yOnOff;
}

void CRefData::setUnit(QString u)
{
    m_Unit = u;
}

QString CRefData::getUnit()
{
    return m_Unit;
}

void CRefData::setOrigData(DsoWfm *p)
{
    mOrigWfm = *p;
}

DsoWfm* CRefData::getOrigData()
{
    return &mOrigWfm;
}

void CRefData::setViewData(DsoWfm *p)
{
    mViewWfm.m_wfmMutex.lock();
    mViewWfm = *p;
    mViewWfm.m_wfmMutex.unlock();
}

DsoWfm* CRefData::getViewData()
{
    return &mViewWfm;
}

void CRefData::setDrawData(DsoWfm *p)
{
    mDrawWfm = *p;
}

DsoWfm* CRefData::getDrawData()
{
    return &mDrawWfm;
}

qint64 CRefData::getOrigHScale()
{
    return m_s64OrigHScale;
}

qint64 CRefData::getOrigHOffset()
{   
    return m_s64OrigHOffset;
}

void CRefData::setOrigVScale(qint64 v)
{
    m_s64OrigVScale = v;
}

qint64 CRefData::getOrigVScale()
{
   return m_s64OrigVScale;
}

void CRefData::setOrigVOffset(qint64 o)
{
    m_s64OrigVOffset = o;
}

qint64 CRefData::getOrigVOffset()
{
    return m_s64OrigVOffset;
}

void CRefData::setyGnd(int gnd)
{
//    dsoVert::setyGnd(gnd);
    mViewWfm.setyGnd(gnd);
}

void CRefData::setyInc( dsoFract fract, DsoReal realBase )
{
    dsoVert::setyInc( realBase.toDouble() * fract.toDouble() );
    mViewWfm.setyInc( fract );
    mViewWfm.setyRefBase( realBase );
}
