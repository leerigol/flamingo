#include "servref.h"

int servRef::setRefActive(int msg)
{
       if ( msg )
       {
           if(this->isActive())
           {
               return ERR_NONE;
           }
           else
           {
               if( m_nHorMode ==  Acquire_XY)
               {
                   return ERR_ACTION_DISABLED;
               }
               else
               {
                   setActive(msg);
                   if( !m_bOpen )
                   {
                       syncEngine(ENGINE_LED_ON, led_ref);
                       m_bOpen = true;
                       dsoVert::setyOnOff( m_bOpen );

                       //load();
                       for(int i=0; i<REF_COUNT; i++)
                       {
                           if( getRefData(i)->getOnOff() )
                           {
                                sendToShow(i, true);
                                if(i == m_nChanID )
                                {
                                    setRefOperEn( true );
                                }
                           }
                       }
                   }
                   mUiAttr.setEnable( MSG_REF_EXPORT, m_pChanData->isValid());
                   mUiAttr.setEnable( MSG_REF_RESET, m_pChanData->isValid());
               }
           }
           m_bDetail = false;
       }
       else
       {
           if( m_bOpen )
           {
              async(CMD_SERVICE_DEACTIVE,0);
              onDeActive(0);
           }
       }
        return ERR_NONE;
}

int servRef::setRefEnable(CArgument& arg)
{
    if( arg.size() == 2)
    {
        int ref = arg[0].iVal - 1;
        bool en = arg[1].bVal;
        CRefData* pRef = getRefData(ref);
        if( pRef )
        {
            if( en )
            {
                if( pRef->getOnOff() == false)
                {
                    setRefSave(ref);
                }
            }
            else
            {
                pRef->setOnOff(false);

                if( ref == m_nChanID )
                {
                    setRefOperEn(false);
                }
                sendToShow(ref, false);
                saveToFile(ref);
            }
        }
    }

    return ERR_NONE;
}

bool servRef::getRefEnable(CArgument& arg)
{
    int ref = 0;
    if( arg.size() )
    {
        ref = arg[0].iVal - 1;
    }
    CRefData* pRef = getRefData(ref);
    if( pRef )
    {
        return pRef->getOnOff();
    }
    return false;
}

int servRef::setRefSource(CArgument& arg)
{
    if(arg.size() == 2)
    {
        int ref = arg[0].iVal - 1;
        int src = arg[1].iVal;
        CRefData* pRef = getRefData(ref);
        if( pRef )
        {
            if( ref == m_nChanID )
            {
                async( MSG_REF_SOURCE, src);
            }
            else
            {
                pRef->setSrc( (Chan)src);
            }
            return ERR_NONE;
        }
    }
    return ERR_INVALID_INPUT;
}

int servRef::getRefSource(CArgument& arg)
{
    int ref = 0;
    if( arg.size() )
    {
        ref = arg[0].iVal - 1;
    }

    CRefData* pRef = getRefData(ref);
    if( pRef )
    {
        return pRef->getSrc();
    }

   // qDebug() << "Invalid Ref App";
    Q_ASSERT(false);

    return chan1;
}

int servRef::setRefVScale(CArgument& arg)
{
    if(arg.size() == 2)
    {
        int ref = arg[0].iVal - 1;
        qint64 scale = arg[1].llVal;
        CRefData* pRef = getRefData(ref);
        if( pRef )
        {
            if( ref == m_nChanID )
            {
                async( MSG_REF_VOLT, scale);
            }
            else
            {
                pRef->setVScale( scale );
                doModify(ref);
                sendToShow(ref);
                saveToFile(ref);
            }
            return ERR_NONE;
        }
    }
    return ERR_INVALID_INPUT;
}

qint64 servRef::getRefVScale(CArgument& arg)
{
    int ref = 0;
    if( arg.size() )
    {
        ref = arg[0].iVal - 1;
    }

    CRefData* pRef = getRefData(ref);
    if( pRef )
    {
        return pRef->getVScale();
    }

    //qDebug() << "Invalid Ref App";
    Q_ASSERT(false);

    return 100000;//100mV
}

int servRef::setRefVOffset(CArgument& arg)
{
    if(arg.size() == 2)
    {
        int ref = arg[0].iVal - 1;
        qint64 off = arg[1].llVal;
        CRefData* pRef = getRefData(ref);
        if( pRef )
        {
            if( ref == m_nChanID )
            {
                async( MSG_REF_POS, off);
            }
            else
            {
                pRef->setVOffset( off );
                doModify(ref);
                sendToShow(ref);
                saveToFile(ref);
            }
            return ERR_NONE;
        }
    }
    return ERR_INVALID_INPUT;
}

qint64 servRef::getRefVOffset(CArgument& arg)
{
    int ref = 0;
    if( arg.size() )
    {
        ref = arg[0].iVal - 1;
    }

    CRefData* pRef = getRefData(ref);
    if( pRef )
    {
        return pRef->getVOffset();
    }

    //qDebug() << "Invalid Ref App";
    Q_ASSERT(false);

    return 0;//0mV
}

int servRef::setRefColor(CArgument& arg)
{
    if(arg.size() == 2)
    {
        int ref = arg[0].iVal - 1;
        int col = arg[1].iVal;

        if(col >=REF_COLOR_GRAY && col <REF_COLOR_ALL )
        {
            CRefData* pRef = getRefData(ref);
            if( pRef )
            {
                if( ref == m_nChanID )
                {
                    async( MSG_REF_COLOR, col);
                }
                else
                {
                    pRef->setColorIndex(col);
                    //doModify(ref);
                    sendToShow(ref);
                    saveToFile(ref);
                }
                return ERR_NONE;
            }
        }
    }
    return ERR_INVALID_INPUT;
}

int servRef::getRefColor(CArgument& arg)
{
    int ref = 0;
    if( arg.size() )
    {
        ref = arg[0].iVal - 1;
    }

    CRefData* pRef = getRefData(ref);
    if( pRef )
    {
        return pRef->getColorIndex();
    }
    return REF_COLOR_GRAY;
}

int servRef::setRefLabel(CArgument& arg)
{

    //qDebug() << "m_nChanID  = " << m_nChanID;
    if(arg.size() == 2)
    {
        int ref = arg[0].iVal - 1;
        QString lab = arg[1].strVal;

        CRefData* pRef = getRefData(ref);

        if(pRef && lab.size() <= 22 )
        {
            pRef->setRefLabel(lab);
            async(MSG_REF_LABEL_ONOFF, true);

           if( ref == m_nChanID )
            {
                setuiChange( MSG_REF_LABEL_EDIT );
            }
            return ERR_NONE;
        }
    }
    return ERR_INVALID_INPUT;
}

QString servRef::getRefLabel(CArgument& arg)
{
    int ref = 1;
    if( arg.size() )
    {
        ref = arg[0].iVal - 1;
    }

    CRefData* pRef = getRefData(ref);
    if( pRef )
    {
        return pRef->getRefLabel();
    }

    //qDebug() << "Invalid Ref App";
    Q_ASSERT(false);
    return "REF";
}

int servRef::setRefReset(int ref)
{
    ref--;
    if( ref == m_nChanID )
    {
        async(MSG_REF_RESET, 0);
    }
    else
    {
        CRefData* pRef = getRefData(ref);
        if( pRef )
        {
            pRef->setViewData(pRef->getOrigData());
            pRef->setDrawData(pRef->getOrigData());


            pRef->setVScale( pRef->getOrigVScale() );
            pRef->setVOffset( pRef->getOrigVOffset());

            doModify(ref);
            sendToShow(ref);
            saveToFile(ref);
        }
    }

    return ERR_NONE;
}

int servRef::setRefSave(int ref)
{
    ref--;
    if( ref == m_nChanID )
    {
        async(MSG_REF_SAVE, 0);
    }
    else
    {
        CRefData* pRef = getRefData(ref);
        if( pRef )
        {
            readData( ref );

            pRef->setViewData(pRef->getOrigData());
            pRef->setDrawData(pRef->getOrigData());


            pRef->setVScale( pRef->getOrigVScale() );
            pRef->setVOffset( pRef->getOrigVOffset());

            doModify(ref);
            sendToShow(ref);
            saveToFile(ref);
        }
    }
    return ERR_NONE;
}

int servRef::setRefCurrent(int ref)
{
    ref--;
    async(MSG_REF_CHANNEL,ref);
    return ERR_NONE;
}
