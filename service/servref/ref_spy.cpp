#include "../servhori/servhori.h"
#include "servref.h"

void servRef::registerSpy()
{
    spyOn( serv_name_display, MSG_DISPLAY_CLEAR,  MSG_REF_CLEAR_ALL);

    spyOn( serv_name_hori,    MSG_HORI_MAIN_SCALE,  MSG_REF_HOR_SCALE);
    spyOn( serv_name_hori,    MSG_HORI_ZOOM_SCALE,  MSG_REF_HOR_SCALE);
    spyOn( serv_name_hori,    MSG_HOR_ZOOM_ON,  MSG_REF_ZOOM_MODE);

    spyOn( serv_name_hori,   MSG_HORI_MAIN_OFFSET, MSG_REF_HOR_OFFSET);
    spyOn( serv_name_hori,    MSG_HORI_ZOOM_OFFSET, MSG_REF_HOR_OFFSET);
    spyOn( serv_name_hori,    MSG_HOR_TIME_MODE,MSG_REF_HOR_MODE );

    spyOn( serv_name_ch1,     MSG_CHAN_ON_OFF, MSG_REF_SRC_CHAN1);
    spyOn( serv_name_ch2,     MSG_CHAN_ON_OFF, MSG_REF_SRC_CHAN2);
    spyOn( serv_name_ch3,     MSG_CHAN_ON_OFF, MSG_REF_SRC_CHAN3);
    spyOn( serv_name_ch4,     MSG_CHAN_ON_OFF, MSG_REF_SRC_CHAN4);

    //for bug1695 by hxh
    spyOn( serv_name_ch1,     servVert::user_cmd_on_off, MSG_REF_SRC_CHAN1);
    spyOn( serv_name_ch2,     servVert::user_cmd_on_off, MSG_REF_SRC_CHAN2);
    spyOn( serv_name_ch3,     servVert::user_cmd_on_off, MSG_REF_SRC_CHAN3);
    spyOn( serv_name_ch4,     servVert::user_cmd_on_off, MSG_REF_SRC_CHAN4);

    spyOn( serv_name_math1,   MSG_MATH_EN, MSG_REF_SRC_MATH1);
    spyOn( serv_name_math2,   MSG_MATH_EN, MSG_REF_SRC_MATH2);
    spyOn( serv_name_math3,   MSG_MATH_EN, MSG_REF_SRC_MATH3);
    spyOn( serv_name_math4,   MSG_MATH_EN, MSG_REF_SRC_MATH4);

    spyOn( serv_name_math1,   MSG_MATH_S32FFTSCR, MSG_REF_HALF_FFT);
    spyOn( serv_name_math2,   MSG_MATH_S32FFTSCR, MSG_REF_HALF_FFT);
    spyOn( serv_name_math3,   MSG_MATH_S32FFTSCR, MSG_REF_HALF_FFT);
    spyOn( serv_name_math4,   MSG_MATH_S32FFTSCR, MSG_REF_HALF_FFT);

    spyOn( serv_name_la, MSG_LA_ENABLE,              MSG_REF_SRC_LA);
    spyOn( serv_name_la, MSG_LA_D0D7_ONOFF,    MSG_REF_SRC_LA);
    spyOn( serv_name_la, MSG_LA_D8D15_ONOFF,  MSG_REF_SRC_LA);
    spyOn( serv_name_la, MSG_LA_SELECT_CHAN,   MSG_REF_SRC_LA);
    spyOn( serv_name_la, MSG_LA_SELECT_GROUP, MSG_REF_SRC_LA);

    spyOn( E_SERVICE_ID_TRACE, servTrace::cmd_set_provider,
                           mpItem->servId, MSG_REF_UPDATED_TRACE);

    //! do not change setting
    setMsgAttr( MSG_REF_UPDATED_TRACE );
    //! trig type
    spyOn( serv_name_trigger ,MSG_TRIGGER_SWEEP,MSG_REF_UPDATED_TRACE );

}

int servRef::onHorScale(qint64 )
{
    for(int i=0; i<REF_COUNT; i++)
    {
        if( getRefData(i)->getOnOff() )
        {
             onShow(i);
        }
    }
    return ERR_NONE;
}

int servRef::onVorScale()
{
    m_bScaleFlag = !m_bScaleFlag;
    return ERR_NONE;
}
int servRef::onHalfFFT()
{
    return ERR_NONE;
}

int servRef::onHorMode(int mode)
{
    if( m_bOpen )
    {
        if( mode == Acquire_XY )
        {
            async(servRef::MSG_REF_CLEAR_ALL,0);
            for(int i = 0; i < REF_COUNT; i++)
            {
                if( getRefData(i)->getOnOff())
                {
                     sendToShow(i, false);
                }
            }
            async(CMD_SERVICE_ACTIVE,0);
        }
        else if( m_nHorMode ==  Acquire_XY)
        {
            for(int i=0; i<REF_COUNT; i++)
            {
                if( getRefData(i)->getOnOff())
                {
                     async(servRef::MSG_REF_SHOW, i);
                }
            }
        }
    }
    m_nHorMode = mode;
    return ERR_NONE;
}

int servRef::getHorMode()
{
    return m_nHorMode;
}

int servRef::onChan1Enable(bool b)
{
    query( serv_name_ch1,   MSG_CHAN_ON_OFF, b);
    findChan(chan1,b);
    return ERR_NONE;
}
int servRef::onChan2Enable(bool b)
{
    query( serv_name_ch2,   MSG_CHAN_ON_OFF, b);
    findChan(chan2,b);
    return ERR_NONE;
}
int servRef::onChan3Enable(bool b)
{
    query( serv_name_ch3,   MSG_CHAN_ON_OFF, b);
    findChan(chan3,b);
    return ERR_NONE;
}

int servRef::onChan4Enable(bool b)
{
    query( serv_name_ch4, MSG_CHAN_ON_OFF, b);
    findChan(chan4,b);
    return ERR_NONE;
}

int servRef::onMath1Enable(bool b)
{
    query( serv_name_math1,   MSG_MATH_EN, b);
    findChan(m1,b);
    return ERR_NONE;
}

int servRef::onMath2Enable(bool b)
{
    query( serv_name_math2,   MSG_MATH_EN, b);
    findChan(m2,b);
    return ERR_NONE;
}

int servRef::onMath3Enable(bool b)
{
    query( serv_name_math3,   MSG_MATH_EN, b);
    findChan(m3, b);
    return ERR_NONE;
}

int servRef::onMath4Enable(bool b)
{
    query( serv_name_math4,   MSG_MATH_EN, b);
    findChan(m4,b);
    return ERR_NONE;
}


int servRef::onLaEnable(int b)   //(bool b)
{
    b = b;
#if 1
    bool laEn = false;
    serviceExecutor::query( serv_name_la, MSG_LA_ENABLE, laEn );
    if( laEn)
    {
        int dxOnOff;
        serviceExecutor::query( serv_name_la, MSG_LA_SELECT_CHAN, dxOnOff );
        LOG_DBG()<<"dxOnOff = "<<dxOnOff;
        for( int i=d0; i<=d15; i++)
        {
            int labit = get_bit(dxOnOff, i);
            if( labit)
            {
                m_nChanMask |= (qint64)1 << i;
            }
            else
            {
                m_nChanMask &= ~((qint64)1 << i);
            }
            mUiAttr.setEnable( MSG_REF_SOURCE, i, labit);
            if( (m_pChanData->getSrc() == i && labit == false) || m_nChanMask == 0 )
            {
                bool bChanEnable = false;
                for(int c = chan1; c <= chan4; c++)
                {
                    if( i == c && !labit)
                    {
                        continue;
                    }
                    if( m_nChanMask & (1 << c) )
                    {
                        m_pChanData->setSrc( (Chan)c );
                        bChanEnable = true;
                        break;
                    }
                }

                if( !bChanEnable )
                {
                    for(int c=m1; c<=m4; c++)
                    {
                        if( i == c && !labit)
                        {
                            continue;
                        }
                        if( m_nChanMask & ((qint64)1 << c) )
                        {
                            m_pChanData->setSrc( (Chan)c );
                            bChanEnable = true;
                            break;
                        }
                    }
                }
            }
        }
    }
    else
    {
        for( int i=d0; i<=d15; i++)
        {
            m_nChanMask &= ~((qint64)1 << i);
            mUiAttr.setEnable( MSG_REF_SOURCE, i, laEn);
            {
                bool bChanEnable = false;
                for(int c = chan1; c <= chan4; c++)
                {
                    if( m_nChanMask & (1 << c) )
                    {
                        m_pChanData->setSrc( (Chan)c );
                        bChanEnable = true;
                        break;
                    }
                }

                if( !bChanEnable )
                {
                    for(int c=m1; c<=m4; c++)
                    {
                        if( m_nChanMask & ((qint64)1 << c) )
                        {
                            m_pChanData->setSrc( (Chan)c );
                            bChanEnable = true;
                            break;
                        }
                    }
                }
            }
        }
    }
 #if 0

    if( (m_pChanData->getSrc() == except && en == false) || mask == 0 )
    {
        bool bChanEnable = false;
        for(int c = chan1; c <= chan4; c++)
        {
            if( except == c && !en)
            {
                continue;
            }
            if( m_nChanMask & (1 << c) )
            {
                m_pChanData->setSrc( (Chan)c );
                bChanEnable = true;
                break;
            }
        }

        if( !bChanEnable )
        {
            for(int c=m1; c<=m4; c++)
            {
                if( except == c && !en)
                {
                    continue;
                }
                if( m_nChanMask & ((qint64)1 << c) )
                {
                    m_pChanData->setSrc( (Chan)c );
                    bChanEnable = true;
                    break;
                }
            }
        }
    }

  #endif
    async(MSG_REF_SOURCE, m_pChanData->getSrc());
    mUiAttr.setEnable(MSG_REF_SAVE, m_nChanMask>0);
    mUiAttr.setEnable(MSG_REF_OPTION, m_nChanMask>0);
#endif
    return ERR_NONE;
}

void servRef::onTraceEnable(void)
{
    int iStat = sysGetSystemStatus();

    if(((ControlStatus)iStat == Control_waiting)
            || ((ControlStatus)iStat == Control_td ))
    {
        m_bUpdata = true;
    }
    else
    {
        m_bUpdata = false;
    }
}

void servRef::findChan(int except, bool en)
{
    qint64 mask = m_nChanMask;
    if( en )
    {
        m_nChanMask |=  (qint64)1 << except;
    }
    else
    {
        m_nChanMask &= ~((qint64)1 << except);
    }

    mUiAttr.setEnable( MSG_REF_SOURCE, except, en);
    if( (m_pChanData->getSrc() == except && en == false) || mask == 0 )
    {
        bool bChanEnable = false;
        for(int c = chan1; c <= chan4; c++)
        {
            if( except == c && !en)
            {
                continue;
            }
            if( m_nChanMask & (1 << c) )
            {
                m_pChanData->setSrc( (Chan)c );
                bChanEnable = true;
                break;
            }
        }

        if( !bChanEnable )
        {
            for(int c=m1; c<=m4; c++)
            {
                if( except == c && !en)
                {
                    continue;
                }
                if( m_nChanMask & ((qint64)1 << c) )
                {
                    m_pChanData->setSrc( (Chan)c );
                    bChanEnable = true;
                    break;
                }
            }
        }
    }

    async(MSG_REF_SOURCE, m_pChanData->getSrc());
    mUiAttr.setEnable(MSG_REF_SAVE, m_nChanMask>0);
    mUiAttr.setEnable(MSG_REF_OPTION, m_nChanMask>0);

}
void  servRef::changeFftRange(qlonglong &Start, qlonglong& Span)
{

    horiAttr hMain;
    hMain = getHoriAttr(chan1, horizontal_view_main );
    horiAttr hZoom;
    hZoom = getHoriAttr(chan1, horizontal_view_zoom );

    qlonglong hMainSpan = hMain.tInc * wave_width;
    qlonglong hZoomOffset = hMain.tInc * hMain.gnd - hZoom.tInc * hMain.gnd ;

    Start = m_nStart + hZoomOffset *  (double) m_nSpan / hMainSpan;
    Span = hZoom.tInc * (double) m_nSpan / hMain.tInc ;
}
//by physical key
int servRef::onClear()
{
    for(int i=0; i<REF_COUNT; i++)
    {
        if( getRefData(i)->getOnOff() )
        {
            getRefData(i)->setOnOff(false);
            sendToShow(i, false);
            saveToFile(i);
        }
    }
    setRefOperEn(false);
    return ERR_NONE;
}

int servRef::onTimeOut(int id)
{
    switch(id)
    {
         case ref_update_timer_id:
             defer(MSG_REF_HOR_SCALE);
             break;
         default:
             break;

    }
    return ERR_NONE;
}
