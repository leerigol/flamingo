#include "../../include/dsocfg.h"

#include "../../service/servhori/servhori.h"
#include "../servch/servch.h"
#include "../servplot/servplot.h"
#include "../servmath/servmath.h"
#include "../servtrace/servtrace.h"

#include "../servstorage/servstorage.h"
#include "../servstorage/storage_wav.h"

#include "../../baseclass/resample/memView.h"
#include "../../baseclass/resample/reSample.h"
#include "servref.h"

#include "../../service/servdso/servdso.h"

#ifndef _SIMULATE
#define REF_FILE_PATH   QString("/user/ref")
#else
#define REF_FILE_PATH  QDir::currentPath()
#endif
#define REF_FILE_NAME   QString("RigolDsoRef")
//! DsoType
using namespace DsoType;

/*! \var 定义消息映射表
* 定义消息映射表
*/
IMPLEMENT_CMD( serviceExecutor, servRef )
start_of_entry()
//setuiMaxLength();
 // mUiAttr.setEnable(MSG_REF_TIMESCALE , false);
on_set_int_int  (MSG_REF_SOURCE,          &servRef::setSrc),
on_get_int      (MSG_REF_SOURCE,          &servRef::getSrc),

on_set_int_int  (MSG_REF_CHANNEL,         &servRef::setChanID),
on_get_int      (MSG_REF_CHANNEL,         &servRef::getChanID),

on_set_int_int  (MSG_REF_COLOR,           &servRef::setColorIndex),
on_get_int      (MSG_REF_COLOR,           &servRef::getColorIndex),

on_set_int_void (MSG_REF_ONOFF,           &servRef::clear),

on_set_int_ll   (MSG_REF_POS,             &servRef::setVOffset),
on_get_ll       (MSG_REF_POS,             &servRef::getVOffset),

on_set_int_ll   (MSG_REF_VOLT,            &servRef::setVScale),
on_get_ll       (MSG_REF_VOLT,            &servRef::getVScale),

on_set_int_void (MSG_REF_SAVE,            &servRef::save),
on_set_int_void (MSG_REF_RESET,           &servRef::reset),

on_set_int_bool (MSG_REF_DETAILS,         &servRef::setRefDetails),
on_get_bool     (MSG_REF_DETAILS,         &servRef::getRefDetails),

on_set_int_bool (MSG_REF_DISPTYPE,        &servRef::setDispType),
on_get_bool     (MSG_REF_DISPTYPE,        &servRef::getDispType),

on_set_int_int  (MSG_REF_LABEL_NAME,      &servRef::setLabelIndex),
on_get_int      (MSG_REF_LABEL_NAME,      &servRef::getLabelIndex),

on_set_int_bool (MSG_REF_LABEL_ONOFF,     &servRef::setShowLabel),
on_get_bool     (MSG_REF_LABEL_ONOFF,     &servRef::getShowLabel),

on_set_int_string(MSG_REF_LABEL_EDIT,     &servRef::setLabel),
on_get_string   (MSG_REF_LABEL_EDIT,      &servRef::getLabel),

on_set_int_int  ( MSG_REF_EXPORT,         &servRef::exportRef),
on_set_int_int  ( MSG_REF_IMPORT,         &servRef::importRef),

on_set_int_int  (servRef::MSG_REF_SHOW,   &servRef::onShow),
on_get_bool     (servRef::MSG_REF_SHOW,   &servRef::getShow),

on_set_int_ll   (servRef::MSG_REF_HOR_SCALE, &servRef::onHorScale),
on_set_int_ll   (servRef::MSG_REF_HOR_OFFSET,&servRef::onHorScale),

on_set_int_void   (servRef::MSG_REF_VOR_SCALE,&servRef::onVorScale),

//on_set_int_void(servRef::MSG_REF_ZOOM_MODE,&servRef::onZoomMode),
on_set_int_void(servRef::MSG_REF_HALF_FFT,&servRef::onHalfFFT),

on_set_int_int  (servRef::MSG_REF_HOR_MODE, &servRef::onHorMode),
on_get_int      (servRef::MSG_REF_HOR_MODE, &servRef::getHorMode),

on_set_int_bool (servRef::MSG_REF_SRC_CHAN1,&servRef::onChan1Enable),
on_set_int_bool (servRef::MSG_REF_SRC_CHAN2,&servRef::onChan2Enable),
on_set_int_bool (servRef::MSG_REF_SRC_CHAN3,&servRef::onChan3Enable),
on_set_int_bool (servRef::MSG_REF_SRC_CHAN4,&servRef::onChan4Enable),

on_set_int_bool (servRef::MSG_REF_SRC_MATH1,&servRef::onMath1Enable),
on_set_int_bool (servRef::MSG_REF_SRC_MATH2,&servRef::onMath2Enable),
on_set_int_bool (servRef::MSG_REF_SRC_MATH3,&servRef::onMath3Enable),
on_set_int_bool (servRef::MSG_REF_SRC_MATH4,&servRef::onMath4Enable),

on_set_int_bool (servRef::MSG_REF_SRC_LA,&servRef::onLaEnable),
on_set_void_void(servRef::MSG_REF_UPDATED_TRACE,&servRef::onTraceEnable),
//MSG_REF_UPDATED_TRACE

on_set_int_void (servRef::MSG_REF_CLEAR_ALL,&servRef::onClear),

//scpi
#include "ref_scpi.h"

//! system msg
on_set_void_void( CMD_SERVICE_INIT,     &servRef::init),
on_set_void_void( CMD_SERVICE_RST,      &servRef::rst ),
on_set_int_void(  CMD_SERVICE_STARTUP,  &servRef::startup ),
on_set_void_int(CMD_SERVICE_TIMEOUT,  &servRef::onTimeOut ),
on_set_int_int(   CMD_SERVICE_ACTIVE,   &servRef::onActive ),
on_get_int   (   CMD_SERVICE_ACTIVE,   &servRef::isActive),

#if 0
on_set_int_int(   CMD_SERVICE_DEACTIVE, &servRef::onDeActive ),
#endif

on_set_void_void( CMD_SERVICE_VIEW_CHANGE,   &servRef::onViewChanged ),

end_of_entry()


//! ref color tabel
QColor servRef::m_ColorIndex[] = { Qt::darkGray,
                                   Qt::darkGreen,//green
                                   QColor(173, 216, 230, 255),//lightblue
                                   Qt::darkRed,//darkRed
                                   QColor(200, 64, 0, 255)   //Orange
                                   };

serviceKeyMap  servRef::m_WaveKeyMap;
QList<QString> servRef::m_RefLabel;

//!init static CRefData List
QList<CRefData* >servRef::m_RefData;
int servRef::m_nChanID = chan1;

servRef::servRef(QString name, ServiceId eId )
        : servVert( name, eId ), dsoVert(ref)

{
    serviceExecutor::baseInit();
    mVersion = 0xB;
    m_pChanData = NULL;

    for(int i=0; i<REF_COUNT; i++)
    {
        m_RefData.append(new CRefData(i));
        m_RefData[ i ]->setServId( mpItem->servId );
    }

    //! servid
    dsoVert::setServId( mpItem->servId );
}

void servRef::init()
{
    addWaveKeys();

    /* !!! The sequence MUST be same with the ref.xls !!!*/
    m_RefLabel.append( QStringLiteral("REF") );
    m_RefLabel.append( QStringLiteral("ACK") );
    m_RefLabel.append( QStringLiteral("ADDR") );
    m_RefLabel.append( QStringLiteral("BIT") );
    m_RefLabel.append( QStringLiteral("CLK") );

    m_RefLabel.append( QStringLiteral("CS") );
    m_RefLabel.append( QStringLiteral("DATA") );
    m_RefLabel.append( QStringLiteral("IN") );
    m_RefLabel.append( QStringLiteral("OUT") );

    m_RefLabel.append( QStringLiteral("MISO") );
    m_RefLabel.append( QStringLiteral("MOSI") );
    m_RefLabel.append( QStringLiteral("TX") );
    m_RefLabel.append( QStringLiteral("RX") );

   // mUiAttr.setEnable(MSG_REF_TIMESCALE, false);
    mUiAttr.setVisible(MSG_REF_TIMESCALE, false);
    mUiAttr.setVisible(MSG_REF_OFFSET, false);

    //depend on FPGA and not ready for now
    mUiAttr.setVisible(MSG_REF_DISPTYPE, false);

    //!For bug 509 ref多次用F键点击标签库切换标签，示波器挂死
    //! 2017年12月27日16:29:29 by Yang,Zhou
    #if 1
        mUiAttr.setEnable(MSG_REF_TIMESCALE, false);
        mUiAttr.setEnable(MSG_REF_OFFSET,  false);
    #endif

    bool en = sysCheckLicense(OPT_MSO);
    if(!en)
    {
        for(int i = d0; i <= d15; i++)
        {
            mUiAttr.setVisible(MSG_REF_SOURCE, i, en);
        }
    }
}


int servRef::serialOut( CStream &stream, unsigned char &ver )
{
    ver = mVersion;
    stream.write(QStringLiteral("ChanID"),     m_nChanID);
    stream.write(QStringLiteral("DispType"),   m_bDispType);
    stream.write(QStringLiteral("ShowLabel"),  m_bShowLabel);
    stream.write(QStringLiteral("Open"),       m_bOpen);
    //qDebug() << " Out m_bOpen = " << m_bOpen;

    for(int i=0; i<REF_COUNT; i++)
    {
        QString name = QString("Ref%1_Index").arg(i+1);
        stream.write(name,       m_RefData[ i ]->getLabelIndex());

        name = QString("Ref%1_Color").arg(i+1);
        stream.write(name,       m_RefData[ i ]->getColorIndex());

        name = QString("Ref%1_Label").arg(i+1);
        stream.write(name,       m_RefData[ i ]->getRefLabel());
    }
    return ERR_NONE;
}

int servRef::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("ChanID"),     m_nChanID);
        setChanID(m_nChanID);

        stream.read(QStringLiteral("DispType"),   m_bDispType);
        stream.read(QStringLiteral("ShowLabel"),  m_bShowLabel);
        stream.read(QStringLiteral("Open"),       m_bOpen);
        QString svalue;
        int     ivalue;
        for(int i=0; i<REF_COUNT; i++)
        {
            QString name = QString("Ref%1_Index").arg(i+1);
            stream.read(name,       ivalue);
            m_RefData[i]->setLabelIndex(ivalue);

            name = QString("Ref%1_Color").arg(i+1);
            stream.read(name,       ivalue);
            m_RefData[i]->setColorIndex(ivalue);

            name = QString("Ref%1_Label").arg(i+1);
            stream.read(name,       svalue);
            m_RefData[i]->setRefLabel(svalue);
        }
    }
    else
    {
        rst();
    }
    return ERR_NONE;
}


void servRef::rst()
{
    m_bDetail       = false;
    m_nChanID       = 0;
    m_pChanData     = m_RefData.at(0);
    m_bDispType     = false;
    m_bShowLabel    = false;
    m_bOpen         = false;
    m_nChanMask     = 0x1e;

    m_nHorMode      =  Acquire_YT;

    for(int i=0; i<REF_COUNT; i++)
    {
        m_RefData.at(i)->setOnOff(false);
        m_RefData.at(i)->setValid(false);
        m_RefData.at(i)->setColorIndex(4  - i % 5);
        m_RefData.at(i)->setVOffset(0);
        m_RefData.at(i)->setVScale(vv(1));
    }

#if 0
    for(int i=d0; i<=d15; i++)
    {
        mUiAttr.setVisible(MSG_REF_SOURCE, i, false);
    }
#endif
    for(int i=d0; i<=d15; i++)
    {
        mUiAttr.setEnable(MSG_REF_SOURCE, i, false);
    }

    for(int i=m1; i<=m4; i++)
    {
        mUiAttr.setEnable(MSG_REF_SOURCE, i, false);
    }

    setRefOperEn(false);
    mUiAttr.setVisible(MSG_REF_VOLT, true);
}

int servRef::startup()
{
    if(m_bOpen)
    {
        //load();
        syncEngine(ENGINE_LED_ON, led_ref);
    }
    else
    {
        //bug 2659 by zy
        onDeActive(m_bOpen);
        syncEngine(ENGINE_LED_OFF, led_ref);
    }

    onChan1Enable(false);
    onChan2Enable(false);
    onChan3Enable(false);
    onChan4Enable(false);

    onMath1Enable(false);
    onMath2Enable(false);
    onMath3Enable(false);
    onMath4Enable(false);

    onLaEnable(0);
    updateAllUi();
    return 0;
}

int servRef::onDeActive(int para )
{
    for(int i=0; i<REF_COUNT; i++)
    {
        if( getRefData(i)->getOnOff() )
        {
             sendToShow(i, false);
        }
    }
    syncEngine(ENGINE_LED_OFF, led_ref);

    m_bOpen = false;
    dsoVert::setyOnOff( m_bOpen );

    return servVert::onDeActive( para );
}

int servRef::onActive(int msg)
{
    if( work_help == sysGetWorkMode() )//qxl help
    {
        return ERR_NONE;
    }

    if ( msg )
    {
        if(this->isActive())
        {
  //!bug 1837 by zy
#if 0
            return ERR_NONE;
#else
           async(CMD_SERVICE_DEACTIVE,0);
           onDeActive(0);
#endif
        }
        else
        {
            if( m_nHorMode ==  Acquire_XY)
            {
               onHorMode(Acquire_XY);
               return ERR_ACTION_DISABLED;
            }
            else
            {
                setActive(msg);
                if( !m_bOpen )
                {
                    syncEngine(ENGINE_LED_ON, led_ref);
                    m_bOpen = true;
                    dsoVert::setyOnOff( m_bOpen );
                    //load();
                    for(int i=0; i<REF_COUNT; i++)
                    {
                        if( getRefData(i)->getOnOff() )
                        {
                             sendToShow(i, true);
                             if(i == m_nChanID )
                             {
                                 setRefOperEn(true);
                             }
                        }
                    }
                    //bug 2422 by zy
                    defer(MSG_REF_HOR_SCALE);
                }
                mUiAttr.setEnable( MSG_REF_EXPORT, m_pChanData->isValid());
                mUiAttr.setEnable( MSG_REF_RESET, m_pChanData->isValid());
            }
        }
        m_bDetail = false;
    }
    else
    {
        if( m_bOpen )
        {
           async(CMD_SERVICE_DEACTIVE,0);
           onDeActive(0);
        }
    }
    return ERR_NONE;
}

///
/// \brief for other module to get Ref data
/// \param ch:0-10/r1-r10
/// \return
///
CRefData* servRef::getRefData(int ch)
{
    int ref_ch = 0;
    if(ch >=0 && ch < REF_COUNT)
    {
        ref_ch = ch;
    }
    else if((Chan)ch >= r1 && (Chan)ch <= r10)
    {
        ref_ch = (Chan)ch - r1;
    }
    else //if ch < 0 then get default ref channel
    {
        ref_ch = m_nChanID;
    }

    if(ref_ch < m_RefData.size())
    {
        return m_RefData.at(ref_ch);
    }

    Q_ASSERT(false);
    return NULL;
}

DsoErr servRef::setChanID(int id)
{
    if(id >=0 && id < REF_COUNT)
    {
        m_nChanID = id;
        m_pChanData = list_at( m_RefData,m_nChanID);
        setRefOperEn(m_pChanData->getOnOff());

        if(mUiAttr.getOption(MSG_REF_SOURCE, m_pChanData->getSrc())->getEnable())
        {
            async( MSG_REF_SOURCE, m_pChanData->getSrc());
        }
        else
        {
            bool en = false;
            for(int c = chan1; c <= chan4; c++)
            {
                if(mUiAttr.getOption(MSG_REF_SOURCE, c)->getEnable())
                {
                     en = true;
                     async( MSG_REF_SOURCE, c);
                     break;
                }
            }
            if(!en)
            {
                for(int c = m1; c <= m4; c++)
                {
                    if(mUiAttr.getOption(MSG_REF_SOURCE, c)->getEnable())
                    {
                         async( MSG_REF_SOURCE, c);
                         break;
                    }
                }
            }
        }
        if(m_pChanData->getOnOff())
        {
            setuiChange( MSG_REF_VOLT );
            setuiChange( MSG_REF_POS );
        }
        return ERR_NONE;
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

}

int  servRef::getChanID()
{
    return m_nChanID;
}


DsoErr servRef::setSrc( int ch)
{
    m_pChanData->setSrc((Chan)ch);


    if( ch >= d0 && ch <= d15 )
    {
        mUiAttr.setVisible(MSG_REF_VOLT, false);
    }
    else
    {
        mUiAttr.setVisible(MSG_REF_VOLT, true);
    }
    return ERR_NONE;
}

int servRef::getSrc()
{
   setuiEnable( m_nChanMask != 0 );
   return m_pChanData->getSrc();
}

//by menu button
DsoErr servRef::clear()
{
    m_pChanData->setOnOff(false);
    setRefOperEn(false);
    sendToShow(m_nChanID, false);
    saveToFile();

    return ERR_NONE;
}

DsoErr servRef::setVScale( qlonglong scale )
{
    DEBUG_TEST();
    DsoErr err = ERR_NONE;
    float fRatio = 1.0;

    if( (chan1 <= m_pChanData->getSrc())
            && ( m_pChanData->getSrc() <=  chan4) )
    {
        QString name = QString("chan%1").arg( m_pChanData->getSrc() );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);
    }

    if( (m1 <= m_pChanData->getSrc())
            && ( m_pChanData->getSrc() <=  m4))
    {
        QString name = QString("math%1").arg( m_pChanData->getSrc()  - m1 + 1);
        serviceExecutor::query( name, servMath::cmd_q_real_scale,  fRatio);
    }
    qint64 min = 1000 * fRatio;
    qint64 max = 10000000 * fRatio;
    if ( scale < min )
    {
        scale = min;
        err   = ERR_OVER_RANGE;
    }
    else if ( scale > max )
    {
        scale = max;
        err   = ERR_OVER_RANGE;
    }


    qint64 lastScale = m_pChanData->getVScale();

    //by hxh for bug3069
    if( lastScale != 0 && scale != 0 )
    {
        m_pChanData->setVScale(scale);

         qint64 offsetDots = m_pChanData->getVOffset() * adc_vdiv_dots/ lastScale ;
         qint64 offsetNow  = offsetDots * m_pChanData->getVScale() / adc_vdiv_dots ;

         if(offsetNow != getVOffset())
         {
              setVOffset(offsetNow);
              async(MSG_REF_POS,offsetNow);
         }
         else
        {
             doModify();
             sendToShow(m_nChanID);
             saveToFile();
         }
    }
    else
    {
        err = ERR_OVER_RANGE;
    }
    return err;
}

qlonglong servRef::getVScale()
{
    DEBUG_TEST();
    //! update attr
    setuiBase( ch_volt_base );
    setuiPostStr( m_pChanData->getUnit());// other for math or A W
    QString unit = m_pChanData->getUnit();
    if( "dB" == unit)
    {
        setuiUnit(Unit_db);
    }
    else if("v*s" == unit)
    {
        setuiUnit(Unit_VmulS);
    }
    else if ("v/s" == unit)
    {
        setuiUnit(Unit_VdivS);
    }
    else if ("U" == unit)
    {
        setuiUnit(Unit_U);
    }
    else if ("W" == unit)
    {
        setuiUnit(Unit_W);
    }
    else if ("A" == unit)
    {
        setuiUnit(Unit_A);
    }
    else
    {
        setuiUnit(Unit_V);
    }
    if(m_bScaleFlag == false)
    {
        setuiStep( getVoltStep(m_pChanData->getVScale()) * 10);
    }
    else
    {
        setuiStep( getVoltStep(m_pChanData->getVScale()) );
    }

    setuiZVal(vv(1));

    float fRatio = 1.0;

    if( (chan1 <= m_pChanData->getSrc())
            && ( m_pChanData->getSrc() <=  chan4) )
    {
        QString name = QString("chan%1").arg( m_pChanData->getSrc() );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);
    }

    if( (m1 <= m_pChanData->getSrc())
            && ( m_pChanData->getSrc() <=  m4) )
    {
        QString name = QString("math%1").arg( m_pChanData->getSrc()  - m1 + 1);
        serviceExecutor::query( name, servMath::cmd_q_real_scale,  fRatio);
    }


    qint64 val = 1000 * fRatio;
    setuiMinVal( val );

    val = 10000000 * fRatio;
    setuiMaxVal( val );
    return m_pChanData->getVScale();
}

DsoErr servRef::setVOffset(qlonglong offset )
{
    DEBUG_TEST();
    if(offset != m_pChanData->getVOffset())
    {
        m_pChanData->setVOffset(offset);
        doModify(-1, true);
        sendToShow(m_nChanID);
        saveToFile();
    }
    return ERR_NONE;        
}

qlonglong servRef::getVOffset()
{
    DEBUG_TEST();
    //! update attr
    setuiBase( ch_volt_base );

    //by hxh for bug3069
    qlonglong step = m_pChanData->getVScale() / adc_vdiv_dots;
    if( step == 0 )
    {
        step = 1;
    }
    setuiStep( step );

    setuiZVal( m_pChanData->getOrigVOffset() );

    //10*div
    setuiMinVal( -m_pChanData->getVScale()*10 );
    setuiMaxVal(  m_pChanData->getVScale()*10 );
    setuiPostStr( m_pChanData->getUnit() );

    QString unit = m_pChanData->getUnit();
    if( "dB" == unit)
    {
        setuiUnit(Unit_db);
    }
    else if("v*s" == unit)
    {
        setuiUnit(Unit_VmulS);
    }
    else if ("v/s" == unit)
    {
        setuiUnit(Unit_VdivS);
    }
    else if ("U" == unit)
    {
        setuiUnit(Unit_U);
    }
    else if ("W" == unit)
    {
        setuiUnit(Unit_W);
    }
    else if ("A" == unit)
    {
        setuiUnit(Unit_A);
    }
    else
    {
        setuiUnit(Unit_V);
    }
    return m_pChanData->getVOffset();
}


DsoErr servRef::setColorIndex(int colorIndex )
{
    DEBUG_TEST();
    if(colorIndex >=REF_COLOR_GRAY && colorIndex <REF_COLOR_ALL )
    {
        m_pChanData->setColorIndex(colorIndex);
        if(m_pChanData->getOnOff() )
        {
            sendToShow(m_nChanID);
            saveToFile();
        }
        return ERR_NONE;
    }
    return ERR_INVALID_INPUT;
}

int servRef::getColorIndex()
{
    return m_pChanData->getColorIndex();
}

QColor servRef::getColor(int c)
{
    return m_ColorIndex[c];
}

DsoErr servRef::setLabelIndex(int labelIndex)
{
   DsoErr err = ERR_NONE;
   if(labelIndex < 0 || labelIndex >= m_RefLabel.size())
   {
       return ERR_INVALID_INPUT;
   }

   m_pChanData->setLabelIndex(labelIndex);
   if( labelIndex == 0 )
   {
       QString label = QString("%1%2").arg(m_RefLabel.at(labelIndex)).arg(m_nChanID+1);
       m_pChanData->setRefLabel(label);

   }
   else
   {
        m_pChanData->setRefLabel( m_RefLabel.at(labelIndex));
   }

   setuiChange(MSG_REF_LABEL_EDIT);
   return err;
}

int  servRef::getLabelIndex()
{
   return m_pChanData->getLabelIndex();
}

DsoErr servRef::setShowLabel(bool b)
{
    m_bShowLabel = b ;
    return ERR_NONE;
}

bool servRef::getShowLabel()
{
    return m_bShowLabel;
}

int servRef::setLabel(QString &l)
{ 
    m_pChanData->setRefLabel(l);
    return ERR_NONE;
}

QString servRef::getLabel()
{
    setuiMaxLength(22);
    return m_pChanData->getRefLabel();
}

DsoErr servRef::setDispType(bool b)
{
    m_bDispType = b;
    return ERR_NONE;
}

bool servRef::getDispType()
{
      return m_bDispType;
}

DsoWfm *servRef::getOrigData()
{
    return m_pChanData->getOrigData();
}

QString servRef::getRefTime() const
{
    return m_pChanData->getRefTime();
}

DsoWfm *servRef::getViewData()
{
    return m_pChanData->getViewData();
}

DsoErr servRef::setRefDetails(bool)
{
    m_bDetail = !m_bDetail;
    return ERR_NONE;
}

bool servRef::getRefDetails()
{
    return m_bDetail;
}

DsoErr servRef::importRef()
{
    CIntent intent;
    intent.setIntent( MSG_STORAGE_SUB_LOAD, //where
                      servStorage::FUNC_LOAD_REF,//do what
                      MSG_REF_OPTION,//return msg
                      getId());
    startIntent( serv_name_storage, intent );
    return ERR_NONE;
}

DsoErr servRef::exportRef()
{
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_SAVE,
                      servStorage::FUNC_SAVE_REF,
                      MSG_REF_OPTION,
                      getId());

    startIntent( serv_name_storage, intent );
    return ERR_NONE;
}

bool servRef::readData( int id )
{
    CRefData *pRef = m_pChanData;
    if(id > -1)
    {
        pRef = getRefData(id);
    }
    DsoWfm* wfm = pRef->getOrigData();
    Chan src = pRef->getSrc();

    int count = 0;
    wfm->setAttrId(0);
    while(wfm->getAttrId() != servHori::getAttrId())
    {

        if( (src>= chan1 && src <= chan4)  || (src>= m1 && src <= m4))
        {
              dsoVert::getCH(src)->getTrace(*wfm);
        }
        else
        {
              dsoVert::getCH( la )->getDigi( *wfm , src, horizontal_view_main);
              wfm->expandLa();

              //! to value
              wfm->setyGnd(0);
              wfm->setyInc( 1e6 );
              wfm->toValue();
         }

        if(count >= 10)
        {            
            count = 0;
            break;
        }

        if(wfm->getAttrId() != servHori::getAttrId())
        {
           count++;
           QThread::msleep(10);
        }
        else
        {
            break;
        }
     }
    pRef->saveRefTime();
    pRef->setValid( wfm->size() > 0);

    if( pRef->isValid() )
    {
        if( src >= chan1 && src <= chan4)
        {
            void* data;
            QString name = QString("chan%1").arg(src);
            query(name,servCH::cmd_ch_attr,data);
            vertAttr* attr = (vertAttr*)data;
            if(attr)
            {
                qint64 s = adc_vdiv_dots/ch_volt_base;
                s = s * attr->yInc;
                pRef->setOrigVScale( s );

                qint64 offset = attr->yGnd;
                offset = (offset - adc_center) * s / adc_vdiv_dots;
                pRef->setOrigVOffset( offset );
            }
            {
                QString u;
                QString name = servCH::getServiceName((ServiceId)(E_SERVICE_ID_CH1 + src - 1));

                serviceExecutor::query(name, servCH::qcmd_string_unit,u);
                pRef->setUnit(u);
            }         
            //!记录波形是否反向zynq/include/
            query(name,MSG_CHAN_INVERT,m_bInvert);
        }
        else if( src >= m1 && src <= m4 )
        {
            QString name = QString("math%1").arg( src - m1 + 1);
            int oper;
            query(name, MSG_MATH_S32MATHOPERATOR, oper);

            if(oper == operator_fft)
            {
                 qint64 val = 1;
                 query(name, MSG_MATH_FFT_SCALE, val);
                 pRef->setOrigVScale( val/1000 );
                 query(name, MSG_MATH_VIEW_OFFSET, val);
                 pRef->setOrigVOffset( val/1000 );
            }
            else if(oper >= operator_and  && oper <= operator_not )
            {
                qint64 val = 1;
                int scale;
                query(name, MSG_MATH_LOGIC_SCALE, scale);
                switch(scale)
                {
                    case Small:
                         val = vv(2);
                        break;
                    case Medium:
                         val = vv(1);
                        break;
                    case Large:
                         val = mv(500);
                        break;
                    default:
                       break;
                }
                pRef->setOrigVScale(val);
                qint64 offset;

               query(name,MSG_MATH_LOGIC_OFFSET, offset);
               offset = (offset) * val / adc_vdiv_dots;
               pRef->setOrigVOffset(offset);

            }
            else
            {
                qint64 val = 1;
                query(name, MSG_MATH_S32FUNCSCALE, val);
                pRef->setOrigVScale( val/1000 );
                query(name, MSG_MATH_VIEW_OFFSET, val);
                pRef->setOrigVOffset( val/1000 );
            }

            QString u;
            serviceExecutor::query( name, servMath::cmd_q_unit_string,u);
            pRef->setUnit( u );
        }
        else if(src >= d0 && src <= d15)
        {        
            pRef->setUnit( " " );

            int scale;
            query(serv_name_la, MSG_LA_WAVE_SIZE, scale);
            int pos =  dsoVert::getCH( la )->getyPos(src);
            switch(scale)
            {
                case Small:
                     pRef->setOrigVScale( vv(4));
                     pRef->setOrigVOffset(vv( (pos)  - 16));
                    break;
                case Medium:
                     //! to value
                     pRef->setOrigVScale( vv(2) );
                     pRef->setOrigVOffset(vv((pos) - 8));
                    break;
                 case Large:
                      //! to value
                      pRef->setOrigVScale( vv(1) );
                      pRef->setOrigVOffset(vv((pos) - 4));
                     break;
                 default:
                    break;
            }
        }
        else
        {
            pRef->setValid(false);
        }

    }
  //  qDebug() <<"GND/INC=" << wfm->getyGnd() << wfm->realyInc() << wfm->size();
    return pRef->isValid();
}


void servRef::setRefOperEn(bool b)
{
#if 1
    Chan src = m_pChanData->getSrc();
    if( src >= d0 && src <= d15 )
    {
        //mUiAttr.setEnable(MSG_REF_VOLT, b);
    }
    else
    {
         mUiAttr.setEnable(MSG_REF_VOLT, b);
    }
#else
    mUiAttr.setEnable(MSG_REF_VOLT, b);
#endif
    mUiAttr.setEnable(MSG_REF_POS, b);
    mUiAttr.setEnable(MSG_REF_ONOFF, b);
    mUiAttr.setEnable(MSG_REF_EXPORT, b);
}

DsoErr servRef::load()
{
    DsoErr err = ERR_NONE;
#if 0 // not use for now
    bool   menuEN = false;

    for(int i=0; i<REF_COUNT; i++)
    {
        QString path = REF_FILE_PATH + QString("/%1%2.ref").arg(REF_FILE_NAME).arg(i);
        if(QFile::exists(path))
        {
            err = (DsoErr)CWaveFile::loadREF(path, i);
            if(err == ERR_NONE)
            {
                CRefData* pData = getRefData(i);
                if( pData->getOnOff() )
                {
                    async(servRef::MSG_REF_SHOW, i);
                    //Only the current channel, menu can be enable;
                    if ( i == m_nChanID)
                    {
                        menuEN = true;
                    }
                }
                //qDebug() << "loading " << path << menuEN;
            }
        }
    }
    setRefOperEn( menuEN );
#endif
    return err;
}


bool servRef::saveToFile(int id)
{
    DEBUG_TEST();
    if( id == -1 )
    {
        id = m_nChanID;
    }

#if 0 //not use for now
    QString path = REF_FILE_PATH + QString("/%1%2.ref").arg(REF_FILE_NAME).arg(id);

    if(CWaveFile::saveREF(path) != ERR_NONE)
    {
        return false;
    }
#endif
    return true;
}

DsoErr servRef::save()
{        
    if(m_bOpen)
    {
        int iStat = sysGetSystemStatus();
        if(((ControlStatus)iStat != Control_waiting) || m_bUpdata)
        {
            bool b = saveToMem();
            if(b)
            {
                async(servRef::MSG_REF_HALF_FFT, 1);
            }
        }
    }
    return ERR_NONE;
}

DsoErr servRef::reset()
{
    if(m_bOpen)
    {
        saveToMem(false);
    }
    return ERR_NONE;
}

bool servRef::saveToMem(bool isNew)
{

    if( isNew )
    {
        readData();
    }
    mUiAttr.setEnable( MSG_REF_EXPORT, m_pChanData->isValid());
    mUiAttr.setEnable( MSG_REF_RESET, m_pChanData->isValid());
    if(m_pChanData->isValid())
    {
        m_pChanData->setViewData(m_pChanData->getOrigData());
        m_pChanData->setDrawData(m_pChanData->getOrigData());
        m_pChanData->setVScale( m_pChanData->getOrigVScale() );
        m_pChanData->setVOffset( m_pChanData->getOrigVOffset());
       // m_pChanData->setColorIndex(4  - m_nChanID % 5);
        //bug 3355 by zy
        m_pChanData->m_nSaveSrc = m_pChanData->getSrc();
        setuiChange( MSG_REF_VOLT );
        setuiChange( MSG_REF_POS );
        m_pChanData->setOnOff(true);
        doModify();
        sendToShow(m_nChanID);
        saveToFile();
        setRefOperEn(true);
        return true;
    }
 //  qDebug() << "mDrawWfm = " << CRefData::mDrawWfm.size();
    return false;
}

void servRef::sendToShow(int id, bool b)
{
    int mode = 0;
    enum PlotId plotMainID;
    enum PlotId plotZoomID;
    enum PlotId plotclearID;

    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, mode );
    DsoScreenMode scrMode = (DsoScreenMode)mode;
    plotclearID = (enum PlotId)(PLOT_ID_REF1 + id);
    if(scrMode == screen_yt_main_zoom || scrMode == screen_yt_main_zfft)
    {
        plotMainID  = (enum PlotId)(PLOT_ID_MAIN_REF1 + id);
        plotZoomID  = (enum PlotId)(PLOT_ID_ZOOM_REF1 + id);
    }
    else
    {
        plotMainID = (enum PlotId)(PLOT_ID_REF1 + id);
        plotZoomID = (enum PlotId)(PLOT_ID_ZOOM_REF1 + id);
    }

DEBUG_TEST();
    if(b && m_bOpen)
    {
        plotContext pContext;
        CRefData*   pRefData = getRefData(id);
        QColor c = getColor(pRefData->getColorIndex());
        pContext.set(c,plotMainID, pRefData->getDrawData(), &m_Sem);

        Chan src = pRefData->getSrc();
        bool halfOnOff = true;
        if(src >= m1 && src <= m4)
        {
            int oper;
            QString name = QString("math%1").arg( src - m1 + 1);
            query(name, MSG_MATH_S32MATHOPERATOR, oper);
            serviceExecutor::query(name, MSG_MATH_S32FFTSCR, halfOnOff);
        }
        bool zoomEn;
        serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);
        if(zoomEn || halfOnOff)
        {
            serviceExecutor::send( servPlot::singleName(),
                                   servPlot::cmd_plot_sync,
                                   (pointer)&pContext, NULL );

            for(int i = 1; i < 5; i++)
            {
                bool OnOff = true;
                QString name = QString("math%1").arg(i);
                serviceExecutor::query(name, MSG_MATH_S32FFTSCR, OnOff);
                if(!OnOff)
                {
                    serviceExecutor::post( E_SERVICE_ID_PLOT,
                                           servPlot::cmd_close_polt,
                                             plotZoomID, NULL );
                    return;
                }
            }
        }

        //bug 2820 by zy
        if((!zoomEn) && (!halfOnOff))
        {
             pContext.set(c,plotZoomID, pRefData->getDrawData(), &m_Sem);
             serviceExecutor::send(servPlot::singleName(),
                                    servPlot::cmd_plot_sync,
                                    (pointer)&pContext, NULL );

             serviceExecutor::post( E_SERVICE_ID_PLOT,
                                    servPlot::cmd_close_polt,
                                      plotMainID, NULL );
             return;
        }
        if(scrMode == screen_yt_main_zoom  || scrMode == screen_yt_main_zfft)
        {    
            plotContext pZoomContext;
            QSemaphore zoomSema;
            DsoWfm      *viewdata = NULL;
            DsoWfm      zoomwfm;

            int yGnd;
            yGnd = adc_center + pRefData->getVOffset() * adc_vdiv_dots / pRefData->getVScale();
            Chan src = pRefData->getSrc();

            if( src >= d0 && src <= d15 )
            {
                viewdata = pRefData->getOrigData();
                viewdata->expandLa();

                //! to value
                viewdata->setyGnd(0);
                viewdata->setyInc( 1e6 );
                viewdata->toValue();
            }
            else
            {
                 viewdata = pRefData->getOrigData();
            }

            struMem  mem;
            struView view;
            mem.sett( viewdata->getllt0() + viewdata->start()*viewdata->getlltInc(), viewdata->getlltInc() );
            mem.setLength( viewdata->size());            
            if(src >= m1 && src <= m4)
            {
                   int oper;
                   QString name = QString("math%1").arg( src - m1 + 1);
                   query(name, MSG_MATH_S32MATHOPERATOR, oper);
                   if(oper == operator_fft)
                   {
                       qlonglong Start,Span;
                       changeFftRange(Start,Span);
                       view.sett( Start,Span/1000);
                    }
                    else
                    {
                        horiAttr attr = sysGetHoriAttr(chan1, horizontal_view_zoom);
                        view.sett( - attr.gnd * attr.tInc, attr.tInc );
                    }
             }
            else
            {
                horiAttr attr = sysGetHoriAttr(chan1, horizontal_view_zoom);
                view.sett( - attr.gnd * attr.tInc, attr.tInc );
            }
            view.setWidth( wave_width );
            zoomwfm.init( wave_width );

            reSampleExt(&mem,
                           &view,
                           viewdata->getValue()+viewdata->start(),
                           zoomwfm.getValue(),
                           e_sample_peak);
            zoomwfm.setRange( view.vLeft, view.vLen );
            zoomwfm.setyGnd(yGnd);
            zoomwfm.setyInc( dsoFract(pRefData->getVScale(),adc_vdiv_dots), DsoReal(1,E_N6) );

            if(src >= chan1 && src <= chan4)
            {
                bool Invert;
                QString name = QString("chan%1").arg(src);
                //!记录波形是否反向
                query(name,MSG_CHAN_INVERT,Invert);
                if(Invert != m_bInvert)
                {
                    zoomwfm.setInvert(true);
                }
            }

            //bug 2916 by zy
            if(mem.mPtLen <= 1)
            {
                 float *value = zoomwfm.getValue();
                 for(int i = 0; i < zoomwfm.size(); i++)
                 {
                     *(value + i) = 0.0; //int to float
                 }
                 zoomwfm.setValue(value,zoomwfm.size());
            }

            zoomwfm.toPoint( view.vLeft, view.vLen );
            pZoomContext.set(c,plotZoomID, &zoomwfm,&zoomSema);
            serviceExecutor::send(servPlot::singleName(),
                                    servPlot::cmd_plot_sync,
                                    (pointer)&pZoomContext, NULL );

        }
        else
        {
            serviceExecutor::post( E_SERVICE_ID_PLOT,
                                   servPlot::cmd_close_polt,
                                   plotZoomID, NULL );
        }
    }
    else
    {

        serviceExecutor::post( E_SERVICE_ID_PLOT,
                               servPlot::cmd_close_polt,
                                 plotclearID, NULL );

        serviceExecutor::post( E_SERVICE_ID_PLOT,
                               servPlot::cmd_close_polt,
                                 plotMainID, NULL);

        serviceExecutor::post( E_SERVICE_ID_PLOT,
                                   servPlot::cmd_close_polt,
                                   plotZoomID, NULL );

    }

}

void servRef::onViewChanged()
{
    for(int i=0; i<REF_COUNT; i++)
    {
        if( getRefData(i)->getOnOff() )
        {
            sendToShow(i, true);
        }
    }
}

void servRef::doModify(int id, bool need)
{
    CRefData *pRefData = m_pChanData;
    if(id > -1 && id < REF_COUNT)
    {
        pRefData = getRefData(id);
    }

    if(need)
    {
        m_pChanData->setViewData(m_pChanData->getOrigData());
    }

    pRefData->setyInc( dsoFract(pRefData->getVScale(),adc_vdiv_dots),
                       DsoReal(1,E_N6) );

    int yGnd = adc_center +
            pRefData->getVOffset() * adc_vdiv_dots / pRefData->getVScale();
    pRefData->setyGnd(yGnd);

    DsoWfm*  memData  = pRefData->getViewData();
    DsoWfm*  viewData = pRefData->getDrawData();

    struMem  mem;
    struView view;
    mem.sett( memData->getllt0() + memData->start()*memData->getlltInc(), memData->getlltInc() );
    mem.setLength( memData->size());

    Chan src = pRefData->getSrc();
    if(src >= m1 && src <= m4)
    {
          int oper;
          QString name = QString("math%1").arg( src - m1 + 1);
          query(name, MSG_MATH_S32MATHOPERATOR, oper);
          if(oper == operator_fft)
          {
              query(name, MSG_MATH_FFT_H_START, m_nStart);
              query(name,  MSG_MATH_FFT_H_SPAN, m_nSpan);
              view.sett( m_nStart,m_nSpan/1000);
          }
          else
          {
              horiAttr attr = sysGetHoriAttr(chan1, horizontal_view_main);
              view.sett( -attr.gnd * attr.tInc, attr.tInc);
          }
    }
    else
    {
           horiAttr attr = sysGetHoriAttr(chan1, horizontal_view_main);
           view.sett( - attr.gnd * attr.tInc, attr.tInc );
    }

    view.setWidth(wave_width);
    viewData->init(wave_width);
    reSampleExt(&mem,
                &view,
                memData->getValue()+memData->start(),
                viewData->getValue(),
                e_sample_peak);

 #if 1
    if(view.vLen >= 2)
    {
        horiAttr attr = sysGetHoriAttr(chan1, horizontal_view_main);
        m_nGnd = attr.gnd;
        m_nInc = attr.tInc;
    }
    else
    {

        view.sett( - m_nGnd * m_nInc, m_nInc );
        reSampleExt(&mem,
                    &view,
                    memData->getValue()+memData->start(),
                    viewData->getValue(),
                    e_sample_peak);
        //qDebug() << "view.vLen = " << view.vLen;
    }
#endif
    viewData->setRange( view.vLeft, view.vLen );
    viewData->setyGnd( memData->getyGnd());
    viewData->sett(view.mvt0,view.mvtInc,DsoReal(1,E_N6) );
    viewData->setyInc( memData->getyInc(), memData->getyRefBase());
    //bug 2916 by zy
    if(mem.mPtLen <= 1)
    {
         float *value = viewData->getValue();
         for(int i = 0; i < viewData->size(); i++)
         {
             *(value + i) = 0.0; //int to float
         }
         viewData->setValue(value,viewData->size());
    }
    viewData->toPoint( view.vLeft, view.vLen );
#if 0
    //test
    memData->saveValue("/mnt/mem");
    memData->save("/mnt/membin");
    viewData->saveValue("/mnt/view");
    viewData->save("/mnt/viewbin");
#endif
}

int servRef::onShow(int id)
{
    if(!m_bOpen)
    {
        return ERR_NONE;
    }

    if(id < 0)
    {
        m_pChanData->setOnOff(true);
        setRefOperEn(true);
        id = m_nChanID;
    }
    doModify(id);
    sendToShow(id);
    return ERR_NONE;
}

bool servRef::getShow()
{
    return m_bOpen;
}

bool servRef::keyTranslate(int key,
               int &msg,
               int &controlAction)
{
    if( !this->isActive() )
    {
        return false;
    }

    serviceKey servKey;
    if ( m_WaveKeyMap.find( key, servKey) )
    {
        msg = servKey.mMsg;
        controlAction = servKey.mAction;
        return true;
    }
    return false;
}
void servRef::addWaveKeys()
{
    //! offset
    m_WaveKeyMap.append( encode_inc(R_Pkey_WAVE_POS),
                      MSG_REF_POS,
                      R_Skey_TuneInc );

    m_WaveKeyMap.append( encode_dec(R_Pkey_WAVE_POS),
                      MSG_REF_POS,
                      R_Skey_TuneDec );

    m_WaveKeyMap.append( (R_Pkey_WAVE_POS_Z),
                      MSG_REF_POS,
                      R_Skey_Zval );

    //! scale
    m_WaveKeyMap.append( encode_inc(R_Pkey_WAVE_VOLT),
                      MSG_REF_VOLT,
                       R_Skey_TuneDec);

    m_WaveKeyMap.append( encode_dec(R_Pkey_WAVE_VOLT),
                      MSG_REF_VOLT,
                       R_Skey_TuneInc);

    m_WaveKeyMap.append( (R_Pkey_WAVE_VOLT_Z),
                      MSG_REF_VOR_SCALE,
                      R_Skey_Zval );
}
