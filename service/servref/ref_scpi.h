#ifndef REF_SCPI
#define REF_SCPI

#include "servref.h"

on_set_int_int (servRef::SCPI_REF_ACTIVE,   &servRef::setRefActive),
//on_set_int_int(   CMD_SERVICE_ACTIVE,   &servRef::onActive ),

on_set_int_arg (servRef::SCPI_REF_ENABLE,    &servRef::setRefEnable),
on_get_bool_arg(servRef::SCPI_REF_ENABLE,    &servRef::getRefEnable),

on_set_int_int (servRef::SCPI_REF_CURRENT,   &servRef::setRefCurrent),

on_set_int_arg (servRef::SCPI_REF_SOURCE,    &servRef::setRefSource),
on_get_int_arg (servRef::SCPI_REF_SOURCE,    &servRef::getRefSource),

on_set_int_arg (servRef::SCPI_REF_VSCALE,    &servRef::setRefVScale),
on_get_ll_arg  (servRef::SCPI_REF_VSCALE,    &servRef::getRefVScale),

on_set_int_arg (servRef::SCPI_REF_VOFFSET,   &servRef::setRefVOffset),
on_get_ll_arg  (servRef::SCPI_REF_VOFFSET,   &servRef::getRefVOffset),

on_set_int_arg (servRef::SCPI_REF_COLOR,     &servRef::setRefColor),
on_get_int_arg(servRef::SCPI_REF_COLOR,      &servRef::getRefColor),

on_set_int_arg (servRef::SCPI_REF_LABEL,     &servRef::setRefLabel),
on_get_string_arg(servRef::SCPI_REF_LABEL,   &servRef::getRefLabel),

on_set_int_int (servRef::SCPI_REF_RESET,     &servRef::setRefReset),
on_set_int_int (servRef::SCPI_REF_SAVE,      &servRef::setRefSave),

#endif // REF_SCPI

