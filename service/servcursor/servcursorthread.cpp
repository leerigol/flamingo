#include "servcursor.h"
#include "servcursorthread.h"
#include "../../include/dsodbg.h"
#include "../service_name.h"

servCursorThread::servCursorThread(servCursor *pCursor , QObject * /*parent*/): QThread(  )
{
    m_pServ = pCursor;
}

void servCursorThread::run()
{
    for(; ; )
    {   
        if ( m_pServ->getMode() == cursor_mode_manual &&
             m_pServ->getManualSrc() == la)
        {
            m_pServ->manualTrackLa();

            //! for bug 2948:仍然是与主线线程之间的同步问题
            if(m_pServ->getManualSrc() != la)
            {
                m_pServ->manualUpdate();
            }
        }
        else if ( m_pServ->getMode() == cursor_mode_track )
        {
            if(m_pServ->mTrack.mbPosChanging)
            {
                m_pServ->mTrack.mbPosChanging = false;
                m_pServ->mTrack.mxPosMenuChanged = true;
                m_pServ->mTrack.myPosMenuChanged = true;
                if( m_pServ->getTrackMode() == cursor_track_y)
                {
                    m_pServ->on_Ytrack_pos_changed();
                }
                else
                {
                    m_pServ->on_Xtrack_pos_changed();
                }
                serviceExecutor::post( serv_name_cursor,
                                       servCursor::cmd_track_menu_update,
                                       0);
                if(m_pServ->getMode() == cursor_mode_manual)
                {
                    serviceExecutor::post( serv_name_cursor,
                                          servCursor::cmd_manual_pos_changed,
                                           0);
                }
                continue;
            }
            else
            {
                m_pServ->trackUpdate();
                //! for bug 2844:与主线线程之间的同步问题
                if(m_pServ->getMode() == cursor_mode_manual)
                {
                    serviceExecutor::post( serv_name_cursor,
                                          servCursor::cmd_manual_pos_changed,
                                           0);
                }
            }
        }
        else if ( m_pServ->getMode() == cursor_mode_xy
                  && m_pServ->getXyMode() == cursor_xy_track )
        {
            m_pServ->xyTrackUpdate();
            break;
        }

        QThread::msleep( 30 );
    }
}
