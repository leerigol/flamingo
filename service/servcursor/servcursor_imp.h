#ifndef SERVCURSOR_IMP
#define SERVCURSOR_IMP

#include "../../include/dsostd.h"
#include "../../include/dsocfg.h"

#include "../../meta/crmeta.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_name.h"
#include "../../service/service_msg.h"

#include "servcursor.h"


#endif // SERVCURSOR_IMP

