
#include "servcursor_imp.h"

IMPLEMENT_CMD( serviceExecutor, servCursor )
start_of_entry()

//! user msg
on_set_int_pointer( servCursor::cmd_set_cursor_providor, &servCursor::setCursorDataProvidor),

on_get_int( servCursor::cmd_ref_ax, &servCursor::getManualRefX1 ),
on_get_int( servCursor::cmd_ref_bx, &servCursor::getManualRefX2 ),
on_get_int( servCursor::cmd_ref_ay, &servCursor::getManualRefY1 ),
on_get_int( servCursor::cmd_ref_by, &servCursor::getManualRefY2 ),

on_set_void_void( servCursor::cmd_manual_pos_changed, &servCursor::on_manual_pos_changed ),
on_set_void_void( servCursor::cmd_track_pos_changed, &servCursor::on_track_pos_changed),

on_get_pointer( servCursor::cmd_ax_real, &servCursor::getAxReal ),
on_get_pointer( servCursor::cmd_bx_real, &servCursor::getBxReal ),
on_get_pointer( servCursor::cmd_ay_real, &servCursor::getAyReal ),
on_get_pointer( servCursor::cmd_by_real, &servCursor::getByReal ),

on_get_pointer( servCursor::cmd_dx_real, &servCursor::getDxReal ),
on_get_pointer( servCursor::cmd_dy_real, &servCursor::getDyReal ),
on_get_pointer( servCursor::cmd_inv_dx_real, &servCursor::getInvDxReal ),

//! track
on_get_pointer( servCursor::cmd_track_ax_real, &servCursor::getTrackAx ),
on_get_pointer( servCursor::cmd_track_bx_real, &servCursor::getTrackBx ),
on_get_pointer( servCursor::cmd_track_ay_real, &servCursor::getTrackAy ),
on_get_pointer( servCursor::cmd_track_by_real, &servCursor::getTrackBy ),

on_get_pointer( servCursor::cmd_track_dx_real, &servCursor::getTrackDx ),
on_get_pointer( servCursor::cmd_track_dy_real, &servCursor::getTrackDy ),
on_get_pointer( servCursor::cmd_track_inv_dx_real, &servCursor::getTrackInvDx ),

on_get_pointer( servCursor::cmd_track_ax_track, &servCursor::getTrackAxTrack ),
on_get_pointer( servCursor::cmd_track_ay_track, &servCursor::getTrackAyTrack ),
on_get_pointer( servCursor::cmd_track_bx_track, &servCursor::getTrackBxTrack ),
on_get_pointer( servCursor::cmd_track_by_track, &servCursor::getTrackByTrack ),

on_get_int( servCursor::cmd_track_srca_view, &servCursor::getTrackAView),
on_get_int( servCursor::cmd_track_srcb_view, &servCursor::getTrackBView),

on_set_void_void( servCursor::cmd_src_setting_change,  &servCursor::on_src_attr_change ),
on_set_void_void( servCursor::cmd_src_notrack_setting_change,  &servCursor::on_probe_change ),
on_set_void_void( servCursor::cmd_src_en_change, &servCursor::on_src_en_change ),

on_set_void_void( servCursor::cmd_mode_en_change, &servCursor::on_mode_en_change),
on_set_void_void( servCursor::cmd_zoom_en_change, &servCursor::on_zoom_en_change),
on_set_void_void( servCursor::cmd_screen_mode_change, &servCursor::on_screen_mode_change),

on_set_void_void( servCursor::cmd_track_delta_attr_changed, &servCursor::setTrackDeltaUseable ),
on_get_pointer( servCursor::cmd_track_delta_attr_changed, &servCursor::getTrackDeltaUseable),

on_set_void_void( servCursor::cmd_manual_src_attr_changed, &servCursor::on_manual_src_attr_changed),
on_set_void_void( servCursor::cmd_xy_src_attr_changed, &servCursor::on_xy_src_attr_changed),
on_set_void_void( servCursor::cmd_track_menu_update, &servCursor::on_track_menu_update),

////! auto
//on_set_void_arg(servCursor::cmd_auto_x_changed, &servCursor::setAutoXLine),
//on_set_void_arg(servCursor::cmd_auto_y_changed, &servCursor::setAutoYLine),

//on_get_pointer( servCursor::cmd_auto_x_changed, &servCursor::getAutoXLine ),
//on_get_pointer( servCursor::cmd_auto_y_changed, &servCursor::getAutoYLine ),

//! xy
on_set_void_void( servCursor::cmd_xy_pos_changed, &servCursor::on_xy_pos_changed ),
on_set_void_void( servCursor::cmd_xy_hpos_changed, &servCursor::on_xy_hpos_changed),
on_set_int_bool( servCursor::cmd_xy_lisajous_view, &servCursor::uisetLisajous ),

on_get_pointer( servCursor::cmd_xy_ax_real, &servCursor::getXyAx ),
on_get_pointer( servCursor::cmd_xy_bx_real, &servCursor::getXyBx ),
on_get_pointer( servCursor::cmd_xy_ay_real, &servCursor::getXyAy ),
on_get_pointer( servCursor::cmd_xy_by_real, &servCursor::getXyBy ),

on_get_pointer( servCursor::cmd_xy_dx_real, &servCursor::getXyDx ),
on_get_pointer( servCursor::cmd_xy_dy_real, &servCursor::getXyDy ),

on_get_pointer( servCursor::cmd_xy_t1_real, &servCursor::getXyTrackT1 ),
on_get_pointer( servCursor::cmd_xy_t2_real, &servCursor::getXyTrackT2 ),

on_get_pointer( servCursor::cmd_xy_t1x_real, &servCursor::getXyTrackX1 ),
on_get_pointer( servCursor::cmd_xy_t1y_real, &servCursor::getXyTrackY1 ),

on_get_pointer( servCursor::cmd_xy_t2x_real, &servCursor::getXyTrackX2 ),
on_get_pointer( servCursor::cmd_xy_t2y_real, &servCursor::getXyTrackY2 ),

on_get_pointer( servCursor::cmd_xy_dt_real, &servCursor::getXyTrackDt ),
on_get_pointer( servCursor::cmd_xy_inv_dt_real, &servCursor::getXyTrackInvDt ),
on_get_pointer( servCursor::cmd_xy_dy1_real, &servCursor::getXyTrackDy1 ),
on_get_pointer( servCursor::cmd_xy_dy2_real, &servCursor::getXyTrackDy2 ),

on_get_pointer( servCursor::cmd_xy_t1x_track, &servCursor::getXyTrackX1Track ),
on_get_pointer( servCursor::cmd_xy_t1y_track, &servCursor::getXyTrackY1Track ),

on_get_pointer( servCursor::cmd_xy_t2x_track, &servCursor::getXyTrackX2Track ),
on_get_pointer( servCursor::cmd_xy_t2y_track, &servCursor::getXyTrackY2Track ),

//! menu msg
on_set_int_int( MSG_CURSOR_S32CURSORMODE, &servCursor::setMode ),
on_get_int( MSG_CURSOR_S32CURSORMODE, &servCursor::getMode ),

on_set_int_bool( MSG_CURSOR_S32MTYPE, &servCursor::setManualView ),
on_get_bool( MSG_CURSOR_S32MTYPE, &servCursor::getManualView ),

on_set_int_int( MSG_CURSOR_S32MANUALSRC, &servCursor::setManualSrc ),
on_get_int( MSG_CURSOR_S32MANUALSRC, &servCursor::getManualSrc ),

on_set_int_int( MSG_CURSOR_S32MHAPOS, &servCursor::setManualAHPos ),
on_get_int( MSG_CURSOR_S32MHAPOS, &servCursor::getManualAHPos ),

on_set_int_int( MSG_CURSOR_S32MHBPOS, &servCursor::setManualBHPos ),
on_get_int( MSG_CURSOR_S32MHBPOS, &servCursor::getManualBHPos ),

on_set_int_int( MSG_CURSOR_S32MHABPOS, &servCursor::setManualABHPos ),
on_get_int( MSG_CURSOR_S32MHABPOS, &servCursor::getManualABHPos ),

on_set_int_bool( MSG_CURSOR_S23MAREA, &servCursor::setManualArea ),
on_get_bool( MSG_CURSOR_S23MAREA, &servCursor::getManualArea ),

on_set_int_void( MSG_CURSOR_PMHRANGE, &servCursor::setManualHSnap ),

on_set_int_int( MSG_CURSOR_S32MTIMEUNIT, &servCursor::setHUnit ),
on_get_int( MSG_CURSOR_S32MTIMEUNIT, &servCursor::getHUnit ),

on_set_int_int( MSG_CURSOR_S32MVUNIT, &servCursor::setVUnit ),
on_get_int( MSG_CURSOR_S32MVUNIT, &servCursor::getVUnit ),

on_set_int_void( MSG_CURSOR_PMVRANGE, &servCursor::setManualVSnap ),
on_get_int( MSG_CURSOR_PMVRANGE, &servCursor::getManualVSnap ),

on_set_int_int( MSG_CURSOR_S32MVAPOS, &servCursor::setManualAVPos ),
on_get_int( MSG_CURSOR_S32MVAPOS, &servCursor::getManualAVPos ),

on_set_int_int( MSG_CURSOR_S32MVBPOS, &servCursor::setManualBVPos ),
on_get_int( MSG_CURSOR_S32MVBPOS, &servCursor::getManualBVPos ),

on_set_int_int( MSG_CURSOR_S32MVABPOS, &servCursor::setManualABVPos ),
on_get_int( MSG_CURSOR_S32MVABPOS, &servCursor::getManualABVPos ),

on_set_int_bool( MSG_CURSOR_LATYPE, &servCursor::setLaResType ),
on_get_bool( MSG_CURSOR_LATYPE, &servCursor::getLaResType ),

on_set_int_bool( MSG_CURSOR_TRACK_MODE, &servCursor::setTrackMode ),
on_get_bool( MSG_CURSOR_TRACK_MODE, &servCursor::getTrackMode ),

on_set_int_int( MSG_CURSOR_S32TASRC, &servCursor::setTrackASrc ),
on_get_int( MSG_CURSOR_S32TASRC, &servCursor::getTrackASrc ),

on_set_int_int( MSG_CURSOR_S32TBSRC, &servCursor::setTrackBSrc ),
on_get_int( MSG_CURSOR_S32TBSRC, &servCursor::getTrackBSrc ),

on_set_int_int( MSG_CURSOR_S32TAPOS, &servCursor::setTrackAHPos ),
on_get_int( MSG_CURSOR_S32TAPOS, &servCursor::getTrackAHPos ),

on_set_int_int( MSG_CURSOR_S32TBPOS, &servCursor::setTrackBHPos ),
on_get_int( MSG_CURSOR_S32TBPOS, &servCursor::getTrackBHPos ),

on_set_int_int( MSG_CURSOR_S32TABPOS, &servCursor::setTrackABHPos ),
on_get_int( MSG_CURSOR_S32TABPOS, &servCursor::getTrackABHPos ),

on_set_int_int( MSG_CURSOR_S32TAPOS_V, &servCursor::setTrackAVPos ),
on_get_int( MSG_CURSOR_S32TAPOS_V, &servCursor::getTrackAVPos ),

on_set_int_int( MSG_CURSOR_S32TBPOS_V, &servCursor::setTrackBVPos ),
on_get_int( MSG_CURSOR_S32TBPOS_V, &servCursor::getTrackBVPos ),

on_set_int_int( MSG_CURSOR_S32TABPOS_V, &servCursor::setTrackABVPos ),
on_get_int( MSG_CURSOR_S32TABPOS_V, &servCursor::getTrackABVPos ),

on_set_int_bool( MSG_CURSOR_XY_MODE, &servCursor::setXyMode ),
on_get_bool( MSG_CURSOR_XY_MODE, &servCursor::getXyMode ),

on_set_int_int( MSG_CURSOR_S32XY_XA, &servCursor::setAx ),
on_get_int( MSG_CURSOR_S32XY_XA, &servCursor::getAx ),

on_set_int_int( MSG_CURSOR_S32XY_XB, &servCursor::setBx ),
on_get_int( MSG_CURSOR_S32XY_XB, &servCursor::getBx ),

on_set_int_int( MSG_CURSOR_S32XY_YA, &servCursor::setAy ),
on_get_int( MSG_CURSOR_S32XY_YA, &servCursor::getAy ),

on_set_int_int( MSG_CURSOR_S32XY_YB, &servCursor::setBy ),
on_get_int( MSG_CURSOR_S32XY_YB, &servCursor::getBy ),

on_set_int_int( MSG_CURSOR_S32XY_XAB, &servCursor::setABx ),
on_get_int( MSG_CURSOR_S32XY_XAB, &servCursor::getABx ),

on_set_int_int( MSG_CURSOR_S32XY_YAB, &servCursor::setABy ),
on_get_int( MSG_CURSOR_S32XY_YAB, &servCursor::getABy ),

//on_set_int_void( MSG_CURSOR_LISAJOUS, &servCursor::setLisajous ),
on_set_int_int( MSG_CURSOR_S32XY_TRACK_AX, &servCursor::setAHPos ),
on_get_int( MSG_CURSOR_S32XY_TRACK_AX, &servCursor::getAHPos ),

on_set_int_int( MSG_CURSOR_S32XY_TRACK_BX, &servCursor::setBHPos ),
on_get_int( MSG_CURSOR_S32XY_TRACK_BX, &servCursor::getBHPos ),

on_set_int_int( MSG_CURSOR_S32XY_TRACK_ABX, &servCursor::setABHPos ),

on_set_int_bool( MSG_CURSOR_INDICATOR_ONOFF, &servCursor::setMeasIndiOnOff ),
on_get_bool( MSG_CURSOR_INDICATOR_ONOFF, &servCursor::getMeasIndiOnOff ),

//! system msg
on_set_void_void( CMD_SERVICE_RST,       &servCursor::rst ),
on_set_int_void( CMD_SERVICE_STARTUP,    &servCursor::startup ),
on_set_void_void( CMD_SERVICE_ACTIVE,    &servCursor::onCursorButton),
on_set_int_int( CMD_SERVICE_ITEM_FOCUS,  &servCursor::setFocus ),
on_get_int    ( CMD_SERVICE_ITEM_FOCUS,      &servCursor::getFocus ),

on_get_int( servCursor::cmd_scpi_track_cay, &servCursor::getScpiTrackCay ),
on_get_int( servCursor::cmd_scpi_track_cby, &servCursor::getScpiTrackCby ),


end_of_entry()
