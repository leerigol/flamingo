#ifndef SERVCURSOR_H
#define SERVCURSOR_H

#include "../service.h"
#include "../../baseclass/iserial.h"

#include "./cursorlib/ccursormgr.h"

#include "./servcursorthread.h"

#include "../../com/scpiparse/cconverter.h"
using namespace scpi_parse;

using namespace cursor_lib;

//! config
#define cursor_tune_z_val   (-10000)

class servCursor : public serviceExecutor,
                   public ISerial,
                   public CCursorMgr
{
    Q_OBJECT

protected:
    DECLARE_CMD()

public:   
    enum UserCmd
    {
        cmd_none,

        cmd_ax_real,        //! query
        cmd_ay_real,

        cmd_bx_real,
        cmd_by_real,

        cmd_dx_real,
        cmd_dy_real,
        cmd_inv_dx_real,
                            //! query
        cmd_ref_ax,
        cmd_ref_ay,

        cmd_ref_bx,
        cmd_ref_by,

                            //! track
        cmd_track_ax_real,
        cmd_track_ay_real,

        cmd_track_bx_real,
        cmd_track_by_real,

        cmd_track_dx_real,
        cmd_track_dy_real,
        cmd_track_inv_dx_real,

        cmd_track_ax_track,
        cmd_track_ay_track,
        cmd_track_bx_track,
        cmd_track_by_track,

        cmd_track_srca_view,
        cmd_track_srcb_view,
                            //! auto
        cmd_auto_x_changed,
        cmd_auto_y_changed,

        cmd_auto_x_track,
        cmd_auto_y_track,
                            //! xy
        cmd_xy_ax_real,
        cmd_xy_ay_real,
        cmd_xy_bx_real,
        cmd_xy_by_real,

        cmd_xy_dx_real,
        cmd_xy_dy_real,
                            //! xy track
        cmd_xy_track_begin,
        cmd_xy_t1_real = cmd_xy_track_begin,
        cmd_xy_t1x_real,
        cmd_xy_t1y_real,

        cmd_xy_t2_real,
        cmd_xy_t2x_real,
        cmd_xy_t2y_real,

        cmd_xy_dt_real,
        cmd_xy_dy1_real,
        cmd_xy_dy2_real,
        cmd_xy_inv_dt_real,

        cmd_xy_t1x_track,
        cmd_xy_t1y_track,
        cmd_xy_t2x_track,
        cmd_xy_t2y_track,

        cmd_xy_lisajous_view,

                            //! set
        cmd_src_setting_change,
        cmd_src_en_change,
        cmd_src_notrack_setting_change,
        cmd_mode_en_change,
        cmd_zoom_en_change,
        cmd_hori_setting_change,
        cmd_screen_mode_change,    //! zoom and math half

        cmd_set_item_update,
                            //! value update
        cmd_manual_pos_update,
        cmd_manual_la_update,

        cmd_track_ya_value_update,
        cmd_track_yb_value_update,
        cmd_track_delta_value_update,

        cmd_track_xa_value_update,
        cmd_track_xb_value_update,

        cmd_xy_value_update,
        cmd_xy_track_value_update,

                            //! ui
        cmd_manual_src_attr_changed,
        cmd_track_delta_attr_changed,
        cmd_track_menu_update,
        cmd_xy_src_attr_changed,

        cmd_manual_pos_changed,
        cmd_track_pos_changed,

        cmd_xy_pos_changed,
        cmd_xy_hpos_changed,

        cmd_set_cursor_providor,

        cmd_cursor_button,

        //scpi
        cmd_scpi_man_ax,
        cmd_scpi_man_bx,
        cmd_scpi_man_ay,
        cmd_scpi_man_by,
        cmd_scpi_man_dx,
        cmd_scpi_man_dy,
        cmd_scpi_man_idx,

        cmd_scpi_track_ax,
        cmd_scpi_track_bx,
        cmd_scpi_track_ay,
        cmd_scpi_track_by,
        cmd_scpi_track_dx,
        cmd_scpi_track_dy,
        cmd_scpi_track_idx,

        cmd_scpi_track_cay,
        cmd_scpi_track_cby,

        cmd_cursor_raise_ax,
        cmd_cursor_raise_bx,
        cmd_cursor_raise_ay,
        cmd_cursor_raise_by,
    };

public:
    servCursor( QString name, ServiceId eId = E_SERVICE_ID_CURSOR );

public:
    virtual DsoErr start();
    virtual void registerSpy();
public:
    virtual int serialOut( CStream &stream, serialVersion &ver );
    virtual int serialIn( CStream &stream, serialVersion ver );
    virtual void rst();
    virtual int startup();

private:
    DsoErr setFocus( int msg );
    int getFocus();
    DsoErr exitActive();

public:
    //! user manual
    DsoErr onCursorButton();
    DsoErr setCursorDataProvidor(void *ptr);

    void *getAxReal();
    void *getBxReal();
    void *getAyReal();
    void *getByReal();

    void *getDxReal();
    void *getDyReal();
    void *getInvDxReal();

    //! track
    void *getTrackAx();
    void *getTrackBx();
    void *getTrackAy();
    void *getTrackBy();

    void *getTrackDx();
    void *getTrackDy();
    void *getTrackInvDx();

    void *getTrackAxTrack();
    void *getTrackBxTrack();
    void *getTrackAyTrack();
    void *getTrackByTrack();

    int   getTrackAView();
    int   getTrackBView();

    //! auto
    void* getAutoXLine();
    void* getAutoYLine();

    //! xy
    void *getXyAx();
    void *getXyBx();
    void *getXyAy();
    void *getXyBy();

    void *getXyDx();
    void *getXyDy();

    //! track xy
    void *getXyTrackT1();
    void *getXyTrackT2();

    void *getXyTrackX1();
    void *getXyTrackY1();

    void *getXyTrackX2();
    void *getXyTrackY2();

    void *getXyTrackDt();
    void *getXyTrackInvDt();

    void *getXyTrackDy1();
    void *getXyTrackDy2();

    void *getXyTrackX1Track();
    void *getXyTrackY1Track();

    void *getXyTrackX2Track();
    void *getXyTrackY2Track();
    //! mode
    DsoErr setMode( CursorMode mode );
    CursorMode getMode();

    //! manual
    DsoErr setManualView( CursorView view );
    CursorView getManualView();

    DsoErr setManualArea( CursorArea area );
    CursorArea getManualArea();

    DsoErr setManualSrc( Chan src );
    Chan getManualSrc();

    DsoErr setManualAHPos( int pos );
    int getManualAHPos();

    DsoErr setManualBHPos( int pos );
    int getManualBHPos();

    DsoErr setManualABHPos( int add );
    int getManualABHPos();

    DsoErr setManualHSnap( void );
    int getManualHSnap();

    DsoErr setManualAVPos( int pos );
    int getManualAVPos();

    DsoErr setManualBVPos( int pos );
    int getManualBVPos();

    DsoErr setManualABVPos( int add );
    int getManualABVPos();

    DsoErr setManualVSnap( void );
    int getManualVSnap();

    DsoErr setLaResType(LaNumType type);
    LaNumType    getLaResType();

    //! unit
    DsoErr setHUnit( CursorUnit unit );
    CursorUnit getHUnit();

    DsoErr setVUnit( CursorUnit unit );
    int    getVUnit();

    //! ref
    int getManualRefX1();
    int getManualRefX2();
    int getManualRefY1();
    int getManualRefY2();

    void on_manual_src_attr_changed();
    void on_xy_src_attr_changed();
    void on_track_menu_update();

    void on_manual_pos_changed();
    void on_track_pos_changed();
    void on_Ytrack_pos_changed();
    void on_Xtrack_pos_changed();

    void on_src_en_change();
    void on_mode_en_change();
    void on_zoom_en_change();
    void on_screen_mode_change();

    //! track
    DsoErr setTrackMode( CursorTrackMode mode );
    CursorTrackMode getTrackMode();

    DsoErr setTrackASrc( Chan chan );
    Chan getTrackASrc();

    DsoErr setTrackBSrc( Chan chan );
    Chan getTrackBSrc();

    DsoErr setTrackAHPos( int pos );
    int getTrackAHPos();

    DsoErr setTrackBHPos( int pos );
    int getTrackBHPos();

    DsoErr setTrackABHPos( int add );
    int getTrackABHPos();

    DsoErr setTrackAVPos( int pos );
    int getTrackAVPos();

    DsoErr setTrackBVPos( int pos );
    int getTrackBVPos();

    DsoErr setTrackABVPos( int add );
    int getTrackABVPos();

    void setTrackDeltaUseable();
    void *getTrackDeltaUseable();

    //! auto
//    DsoErr setAutoItem( CursorAutoItem item );
//    CursorAutoItem getAutoItem();
    DsoErr setAutoXLine(CArgument &arg);
    DsoErr setAutoYLine(CArgument &arg);

    DsoErr testAutoAxLine();

    //! measure
    DsoErr setMeasIndiOnOff(bool onOff);
    bool   getMeasIndiOnOff();

    //! xy
    DsoErr setXyMode( CursorXYMode mode );
    CursorXYMode getXyMode();

    DsoErr setAx( int pos );
    int getAx();

    DsoErr setBx( int pos );
    int getBx();

    DsoErr setABx(int add );
    int getABx();

    DsoErr setAy( int pos );
    int getAy();

    DsoErr setBy( int pos );
    int getBy();

    DsoErr setABy( int add );
    int getABy();

    DsoErr setAHPos( int pos );
    int getAHPos();

    DsoErr setBHPos( int pos );
    int getBHPos();

    DsoErr setABHPos(int add );
    int getABHpos();

    DsoErr setLisajous(void);
    bool getLisajous();

    DsoErr uisetLisajous( bool b );

    void on_xy_pos_changed();
    void on_xy_hpos_changed();

    int getScpiTrackCay();
    int getScpiTrackCby();

protected:
    void on_cmd_hori_setting_change();
    void on_src_attr_change();
    void on_probe_change();

protected:
    void manualUiRefresh();
    void trackMenuRefreshY();
    void trackMenuRefreshX();

    void trackAppRefreshY();
    void trackAppRefreshX();
public:
    void trackAppRefresh();
    void trackMenuRefresh();
    DsoErr trackUpdate();
    DsoErr manualTrackLa();
    DsoErr manualUpdate();

    void xyUpdate();
    void xyUiRefresh();

    void xyTrackUpdate();
    void xyTrackUiRefresh();

public:
    static void format( cursor_lib::CCursorVal *pVal, QString &str );
    static void format(cursor_lib::CCursorVal *pVal, QString &str16, QString &str2h, QString &str2l);

private:
    servCursorThread *m_pThread;
    QMutex mUpdateMutex;
    bool mViewLisajous;
    int mFocusItem;
};

//scpi formater
class CursorFormater:public CFormater
{
public:
    CursorFormater(){}
public:
    virtual scpiError format( GCVal &val );
};

#endif // SERVCURSOR_H
