
#include "servcursor_imp.h"

//! manual
DsoErr servCursor::setManualView( CursorView view )
{
    mManual.setView( view );
    //testAutoAxLine();
    return ERR_NONE;
}

CursorView servCursor::getManualView()
{
    return mManual.getView();
}

DsoErr servCursor::setManualArea( CursorArea area )
{
     //! BUG 640  zy
    if(area != horizontal_view_active  || mManual.getArea() == horizontal_view_zoom)
    {
        if(area == horizontal_view_active)
        {
            area = horizontal_view_main;
        }

        horiAttr attr;
        if(area == horizontal_view_main)
        {
            attr = sysGetHoriAttr(chan1, horizontal_view_zoom);
        }
        else
        {
            attr = sysGetHoriAttr(chan1, horizontal_view_main);
        }
        float realA = (getManualAHPos() - attr.gnd) * attr.inc;
        float realB = (getManualBHPos() - attr.gnd) * attr.inc;

        attr = sysGetHoriAttr(chan1, area);
        int posA = round(realA/attr.inc + attr.gnd);
        int posB = round(realB/attr.inc + attr.gnd);

        if(getManualSrc() != chan_none)
        {
            async(MSG_CURSOR_S32MHAPOS,posA);
            async(MSG_CURSOR_S32MHBPOS,posB);
        }
    }


     //! BUG 640  zy
    if(area == horizontal_view_active)
    {
        mManual.setArea( horizontal_view_main );
    }
    else
    {
        mManual.setArea( area );
    }

    return ERR_NONE;
}

CursorArea servCursor::getManualArea()
{
    return mManual.getArea();
}

DsoErr servCursor::setManualSrc( Chan src )
{
    mManual.setSrc( src );

    //!
    if(src == chan_none)
    {
        mUiAttr.setEnable(MSG_CURSOR_S32MHAPOS,  false);
        mUiAttr.setEnable(MSG_CURSOR_S32MHBPOS,  false);
        mUiAttr.setEnable(MSG_CURSOR_S32MHABPOS, false);
        mUiAttr.setEnable(MSG_CURSOR_S32MVAPOS,  false);
        mUiAttr.setEnable(MSG_CURSOR_S32MVBPOS,  false);
        mUiAttr.setEnable(MSG_CURSOR_S32MVABPOS, false);

        mUiAttr.setEnable(MSG_CURSOR_PMVRANGE,   false);
        mUiAttr.setEnable(MSG_CURSOR_PMHRANGE,   false);
    }
    else
    {
        mUiAttr.setEnable(MSG_CURSOR_S32MHAPOS,  true);
        mUiAttr.setEnable(MSG_CURSOR_S32MHBPOS,  true);
        mUiAttr.setEnable(MSG_CURSOR_S32MHABPOS, true);
        mUiAttr.setEnable(MSG_CURSOR_S32MVAPOS,  true);
        mUiAttr.setEnable(MSG_CURSOR_S32MVBPOS,  true);
        mUiAttr.setEnable(MSG_CURSOR_S32MVABPOS, true);

        if( Unit_percent == mManual.getUnitY() )
        {
            mUiAttr.setEnable(MSG_CURSOR_PMVRANGE,   true);
        }
        if( Unit_degree == mManual.getUnitX()
                || Unit_percent == mManual.getUnitX() )
        {
            mUiAttr.setEnable(MSG_CURSOR_PMHRANGE,   true);
        }
    }

    if(src <= m4 && src >= m1)
    {
        mUiAttr.setEnable(MSG_CURSOR_S32MVUNIT, false);
        mUiAttr.setEnable(MSG_CURSOR_S32MTIMEUNIT, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_CURSOR_S32MVUNIT, true);
        mUiAttr.setEnable(MSG_CURSOR_S32MTIMEUNIT, true);
    }

    //! cursor信源为la时
    if(src == la)
    {
        id_async(getId(), MSG_CURSOR_S32MTYPE, cursor_view_hori);

        if(mManual.getSrc() == la)
        {
            if ( !m_pThread->isRunning() )
            {
                m_pThread->start();
            }
        }
        mUiAttr.setVisible(MSG_CURSOR_LATYPE,  true);
        mUiAttr.setEnable(MSG_CURSOR_S32MTYPE, false);
    }
    else
    {
        mUiAttr.setVisible(MSG_CURSOR_LATYPE,  false);
        mUiAttr.setEnable(MSG_CURSOR_S32MTYPE, true);
    }
    defer(cmd_manual_pos_changed);

    return ERR_NONE;
}

Chan servCursor::getManualSrc()
{
    return mManual.getSrc();
}

DsoErr servCursor::setManualAHPos( int pos )
{
    DsoErr err;

    if ( pos == cursor_tune_z_val )
    {
        setuiFocus( MSG_CURSOR_S32MHBPOS );
        return ERR_NONE;
    }

    err = mManual.setX1(pos);
    defer(cmd_manual_pos_changed);
    return err;
}

int servCursor::getManualAHPos()
{
    setuiZVal( cursor_tune_z_val );
    return mManual.getX1();
}

DsoErr servCursor::setManualBHPos( int pos )
{
    DsoErr err;

    if ( pos == cursor_tune_z_val )
    {
        setuiFocus( MSG_CURSOR_S32MHABPOS );
        return ERR_NONE;
    }

    err = mManual.setX2(pos);
    defer(cmd_manual_pos_changed);
    return err;
}
int servCursor::getManualBHPos()
{
    setuiZVal( cursor_tune_z_val );

    return mManual.getX2();
}

DsoErr servCursor::setManualABHPos(int add )
{
    DsoErr err;
    if ( add == cursor_tune_z_val )
    {
        setuiFocus( MSG_CURSOR_S32MHAPOS );
        return ERR_NONE;
    }

    err = mManual.setX1X2(add);
    defer(cmd_manual_pos_changed);
    return err;
}

int servCursor::getManualABHPos()
{
    setuiZVal( cursor_tune_z_val );
    return 0;
}

DsoErr servCursor::setManualHSnap( void )
{
    mManual.snapXRange();

    defer(cmd_manual_pos_changed);

    return ERR_NONE;
}

int servCursor::getManualHSnap()
{
    return 0;
}

DsoErr servCursor::setManualAVPos( int pos )
{
    DsoErr err;

    if ( pos == cursor_tune_z_val )
    {
        setuiFocus( MSG_CURSOR_S32MVBPOS );
        return ERR_NONE;
    }

    err = mManual.setY1( pos );

    defer(cmd_manual_pos_changed);

    return err;
}
int servCursor::getManualAVPos()
{
    setuiZVal( cursor_tune_z_val );
    setuiStep( -1 );

    return mManual.getY1();
}

DsoErr servCursor::setManualBVPos( int pos )
{
    DsoErr err;

    if ( pos == cursor_tune_z_val )
    {
        setuiFocus( MSG_CURSOR_S32MVABPOS );
        return ERR_NONE;
    }

    err = mManual.setY2( pos );

    defer(cmd_manual_pos_changed);

    return err;
}
int servCursor::getManualBVPos()
{
    setuiZVal( cursor_tune_z_val );
    setuiStep( -1 );

    return mManual.getY2();
}

DsoErr servCursor::setManualABVPos(int add )
{
    DsoErr err;
    if ( add == cursor_tune_z_val )
    {
        setuiFocus( MSG_CURSOR_S32MVAPOS );
        return ERR_NONE;
    }

    err = mManual.setY1Y2(-add);
    defer(cmd_manual_pos_changed);
    return err;
}

int servCursor::getManualABVPos()
{
    setuiZVal( cursor_tune_z_val );
    return 0;
}

DsoErr servCursor::setManualVSnap( void )
{
    mManual.snapYRange();

    defer(cmd_manual_pos_changed);

    return ERR_NONE;
}

int servCursor::getManualVSnap()
{
    return 0;
}

DsoErr servCursor::setLaResType(LaNumType type)
{
    mManual.mLaType = type;
    return ERR_NONE;
}

LaNumType servCursor::getLaResType()
{
    return mManual.mLaType;
}

DsoErr servCursor::manualTrackLa()
{
    if(mManual.mArea == horizontal_view_main || mManual.mArea == horizontal_view_active)
    {
        mManual.manualTrackLa(horizontal_view_main);
    }
    else
    {
        mManual.manualTrackLa(horizontal_view_zoom);
    }
    defer(cmd_manual_la_update);
    return ERR_NONE;
}

//! unit
DsoErr servCursor::setHUnit( CursorUnit unit )
{
    DsoErr err;
    err = mManual.setUnitX( unit );

    if( Unit_s == unit || Unit_hz == unit
            || mManual.getSrc() == chan_none )
    {
        mUiAttr.setEnable(MSG_CURSOR_PMHRANGE, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_CURSOR_PMHRANGE, true);
    }

    defer(cmd_manual_pos_changed);
    return err;
}

CursorUnit servCursor::getHUnit()
{
    CursorUnit unit;

    unit = mManual.getUnitX();
    return unit;
}

DsoErr servCursor::setVUnit( CursorUnit unit )
{
    DsoErr err;

    err =  mManual.setUnitY( unit );

    if ( unit == Unit_percent && mManual.getSrc() != chan_none)
    {
        mUiAttr.setEnable( MSG_CURSOR_PMVRANGE, true );
    }
    else
    {
        mUiAttr.setEnable( MSG_CURSOR_PMVRANGE, false );
    }
    defer(cmd_manual_pos_changed);

    return err;
}
int servCursor::getVUnit()
{
    Unit unit;
    unit = mManual.getUnitY();  

    return unit;
}

int servCursor::getManualRefX1()
{
    return mManual.getRefX1();
}
int servCursor::getManualRefX2()
{
    return mManual.getRefX2();
}
int servCursor::getManualRefY1()
{
    return mManual.getRefY1();
}
int servCursor::getManualRefY2()
{
    return mManual.getRefY2();
}

void servCursor::on_manual_src_attr_changed()
{
    on_manual_pos_changed();
}

void servCursor::on_manual_pos_changed()
{
    manualUpdate();
    setuiChange( MSG_CURSOR_S32MHAPOS);
    setuiChange( MSG_CURSOR_S32MHBPOS );
    setuiChange( MSG_CURSOR_S32MVAPOS );
    setuiChange( MSG_CURSOR_S32MVBPOS );
}

void servCursor::on_cmd_hori_setting_change()
{
    defer( cmd_manual_src_attr_changed );
}

DsoErr servCursor::manualUpdate()
{
    mManual.updateProc();

    manualUiRefresh();

    return ERR_NONE;
}

void servCursor::manualUiRefresh()
{
    QString str;

    format( &mManual.mAx, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32MHAPOS, str );

    format( &mManual.mBx, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32MHBPOS, str );

    format( &mManual.mAy, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32MVAPOS, str );

    format( &mManual.mBy, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32MVBPOS, str );

    //! update completed
    setuiChange( cmd_manual_pos_update );
}
