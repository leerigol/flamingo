#include "servcursor_imp.h"

DsoErr servCursor::setTrackMode( CursorTrackMode mode )
{
    defer( cmd_track_pos_changed );

    return mTrack.setTrackMode( mode );
}

CursorTrackMode servCursor::getTrackMode()
{
    return mTrack.getTrackMode();
}

DsoErr servCursor::setTrackASrc( Chan chan )
{
    if(chan == chan_none)
    {
        mUiAttr.setEnable(MSG_CURSOR_S32TAPOS, false);
        mUiAttr.setEnable(MSG_CURSOR_S32TABPOS, false);
        mUiAttr.setEnable(MSG_CURSOR_S32TAPOS_V, false);
        mUiAttr.setEnable(MSG_CURSOR_S32TABPOS_V, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_CURSOR_S32TAPOS, true);
        mUiAttr.setEnable(MSG_CURSOR_S32TAPOS_V, true);

        if(mTrack.getSrcB() != chan_none)
        {
            mUiAttr.setEnable(MSG_CURSOR_S32TABPOS, true);
            mUiAttr.setEnable(MSG_CURSOR_S32TABPOS_V, true);
        }
    }

    defer( cmd_track_delta_attr_changed );
    defer( cmd_track_pos_changed );

    return mTrack.setSrcA(chan);
}

Chan servCursor::getTrackASrc()
{
    return mTrack.getSrcA();
}

DsoErr servCursor::setTrackBSrc( Chan chan )
{
    if(chan == chan_none)
    {
        mUiAttr.setEnable(MSG_CURSOR_S32TBPOS, false);
        mUiAttr.setEnable(MSG_CURSOR_S32TABPOS, false);
        mUiAttr.setEnable(MSG_CURSOR_S32TBPOS_V, false);
        mUiAttr.setEnable(MSG_CURSOR_S32TABPOS_V, false);

        //bug 1905 by zy
        // mUiAttr.setVisible(MSG_CURSOR_S32TBPOS, false);
        // mUiAttr.setVisible(MSG_CURSOR_S32TABPOS, false);
        // mUiAttr.setVisible(MSG_CURSOR_S32TBPOS_V, false);
        // mUiAttr.setVisible(MSG_CURSOR_S32TABPOS_V, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_CURSOR_S32TBPOS, true);
        mUiAttr.setEnable(MSG_CURSOR_S32TBPOS_V, true);

        //bug 1905 by zy
        // mUiAttr.setVisible(MSG_CURSOR_S32TBPOS, true);
        //mUiAttr.setVisible(MSG_CURSOR_S32TABPOS, true);
        if(mTrack.getSrcA() != chan_none)
        {
            mUiAttr.setEnable(MSG_CURSOR_S32TABPOS, true);
            mUiAttr.setEnable(MSG_CURSOR_S32TABPOS_V, true);

            //bug 1905 by zy
            //  mUiAttr.setVisible(MSG_CURSOR_S32TBPOS_V, true);
            //  mUiAttr.setVisible(MSG_CURSOR_S32TABPOS_V, true);
        }
    }

    defer( cmd_track_delta_attr_changed );
    defer( cmd_track_pos_changed );

    return mTrack.setSrcB( chan );
}

Chan servCursor::getTrackBSrc()
{
    return mTrack.getSrcB();
}

DsoErr servCursor::setTrackAHPos( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32TBPOS);
        return ERR_NONE;
    }

    mTrack.mxPosMenuChanged = true;
    DsoErr err = mTrack.setAx(pos);
    defer( cmd_track_pos_changed );
    return err;
}

int servCursor::getTrackAHPos()
{
    setuiZVal( cursor_tune_z_val );
    return mTrack.getAx();
}

DsoErr servCursor::setTrackBHPos( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32TABPOS);
        return ERR_NONE;
    }
    mTrack.mxPosMenuChanged = true;
    DsoErr err = mTrack.setBx(pos);
    defer( cmd_track_pos_changed );
    return err;
}

int servCursor::getTrackBHPos()
{
    setuiZVal( cursor_tune_z_val );
    return mTrack.getBx();
}

DsoErr servCursor::setTrackABHPos(int add )
{
    DsoErr err = ERR_NONE;
    if ( add == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32TAPOS);
        return ERR_NONE;
    }

    err = mTrack.setABx(add);
    defer( cmd_track_pos_changed );
    return err;
}

int servCursor::getTrackABHPos()
{
    setuiZVal( cursor_tune_z_val );

    return 0;
}

DsoErr servCursor::setTrackAVPos( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32TBPOS_V);
        return ERR_NONE;
    }

    mTrack.myPosMenuChanged = true;
    defer( cmd_track_pos_changed );
    return mTrack.setAy(pos);
}
int servCursor::getTrackAVPos()
{
    setuiZVal( cursor_tune_z_val );
    setuiStep( -1 );
    return mTrack.getAy();
}

DsoErr servCursor::setTrackBVPos( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32TABPOS_V);
        return ERR_NONE;
    }

    mTrack.myPosMenuChanged = true;
    defer( cmd_track_pos_changed );
    return mTrack.setBy( pos );
}

int servCursor::getTrackBVPos()
{
    setuiZVal( cursor_tune_z_val );
    setuiStep( -1 );
    return mTrack.getBy();
}

DsoErr servCursor::setTrackABVPos(int add )
{
    if ( add == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32TAPOS_V);
        return ERR_NONE;
    }

    defer(cmd_track_pos_changed);
    return mTrack.setABy(-add);
}

int servCursor::getTrackABVPos()
{
    setuiZVal( cursor_tune_z_val );
    return 0;
}

void servCursor::setTrackDeltaUseable()
{
    mTrack.updateTrackDeltaUseable();
}

void *servCursor::getTrackDeltaUseable()
{
    return mTrack.getTrackDeltaUseable();
}


void servCursor::on_track_pos_changed()
{
    mTrack.mbPosChanging = true;
}

void servCursor::on_Ytrack_pos_changed()
{
    bool zoomEn;
    serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);
    horiAttr hAttr;
    if(zoomEn)
    {
        //! ax
        hAttr = getHoriAttr( mTrack.mTSrcAmgr.mCh,  horizontal_view_zoom);
        //! 将xVal的更新放到服务当中
        float val;
        val = ( mTrack.mAx - hAttr.gnd ) * hAttr.inc;
        mTrack.mAxV.setValue( val, hAttr.unit );

        //! bx
        hAttr = getHoriAttr( mTrack.mTSrcBmgr.mCh, horizontal_view_zoom);
        val = ( mTrack.mBx - hAttr.gnd ) * hAttr.inc;
        mTrack.mBxV.setValue( val, hAttr.unit );
    }
    else
    {
        //! ax
        hAttr = getHoriAttr( mTrack.mTSrcAmgr.mCh,  horizontal_view_main);
        //! 将xVal的更新放到服务当中
        float val;
        val = ( mTrack.mAx - hAttr.gnd ) * hAttr.inc;
        mTrack.mAxV.setValue( val, hAttr.unit );

        //! bx
        hAttr = getHoriAttr( mTrack.mTSrcBmgr.mCh, horizontal_view_main);
        val = ( mTrack.mBx - hAttr.gnd ) * hAttr.inc;
        mTrack.mBxV.setValue( val, hAttr.unit );
    }
}

void servCursor::on_Xtrack_pos_changed()
{
    dsoVert *pVert = dsoVert::getCH( mTrack.mTSrcAmgr.mCh );
    if ( NULL == pVert )
    {
        mTrack.mAyV.setValid( false );
    }
    else
    {
        float val;
        float base = pVert->getYRefBase().toFloat();
        base = base * pVert->getProbe().toFloat();
        float scale = pVert->getYRefScale()*base;
        float offset = pVert->getYRefOffset()*base;
        val = (vertical_center - mTrack.mAy) / 60.0f * scale - offset;
        mTrack.mAyV.setValue( val, pVert->getUnit() );
    }

    pVert = dsoVert::getCH( mTrack.mTSrcBmgr.mCh );
    if ( NULL == pVert )
    {
        mTrack.mByV.setValid( false );
    }
    else
    {
        float val;
        float base = pVert->getYRefBase().toFloat();
        base = base * pVert->getProbe().toFloat();
        float scale = pVert->getYRefScale()*base;
        float offset = pVert->getYRefOffset()*base;
        val = (vertical_center - mTrack.mBy) / 60.0f * scale - offset;
        mTrack.mByV.setValue( val, pVert->getUnit() );
    }
}

DsoErr servCursor::trackUpdate()
{
    mUpdateMutex.lock();
    mTrack.updateProc();
    mUpdateMutex.unlock();

    trackAppRefresh();
    return ERR_NONE;
}

void servCursor::on_track_menu_update()
{
    trackMenuRefresh();
    if ( mTrack.mTrackMode == cursor_track_y )
    {
        setuiChange( MSG_CURSOR_S32TAPOS );
        setuiChange( MSG_CURSOR_S32TBPOS );
    }
    else
    {
        setuiChange( MSG_CURSOR_S32TAPOS_V );
        setuiChange( MSG_CURSOR_S32TBPOS_V );
    }
}

void servCursor::trackMenuRefresh()
{
    if ( mTrack.mTrackMode == cursor_track_y )
    {
        trackMenuRefreshY();
    }
    else
    {
        trackMenuRefreshX();
    }
}



void servCursor::trackMenuRefreshY()
{
    QString str;

    if ( mTrack.mTSrcAmgr.mCh != chan_none )
    {
        format( &mTrack.mAxV, str );
    }
    else
    {
        str = "*****";
    }
    mUiAttr.setOutStr( MSG_CURSOR_S32TAPOS, str );



    if ( mTrack.mTSrcBmgr.mCh != chan_none )
    {
        format( &mTrack.mBxV, str );
    }
    else
    {
        str = "*****";
    }
    mUiAttr.setOutStr( MSG_CURSOR_S32TBPOS, str );
}

void servCursor::trackMenuRefreshX()
{
    QString str;

    format( &mTrack.mAyV, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32TAPOS_V, str );

    format( &mTrack.mByV, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32TBPOS_V, str );
}


void servCursor::trackAppRefresh()
{
    if ( mTrack.mTrackMode == cursor_track_y )
    {
        trackAppRefreshY();
    }
    else
    {
        trackAppRefreshX();
    }
}

void servCursor::trackAppRefreshY()
{
    //! update completed
    if ( mTrack.mTSrcAmgr.mCh != chan_none )
    {
        defer( cmd_track_ya_value_update );
    }

    if ( mTrack.mTSrcBmgr.mCh != chan_none )
    {
        defer( cmd_track_yb_value_update );
    }

    //! delta refresh
    defer( cmd_track_delta_value_update );
}

void servCursor::trackAppRefreshX()
{
    //! update completed
    if ( mTrack.mTSrcAmgr.mCh != chan_none )
    {
        defer( cmd_track_xa_value_update );
    }

    if ( mTrack.mTSrcBmgr.mCh != chan_none )
    {
        defer( cmd_track_xb_value_update );
    }

    //! delta refresh
    defer( cmd_track_delta_value_update );
}

