#include "cursorlib.h"

namespace cursor_lib {
DsoErr addPos(int& pos1, int& pos2, int add,
                     int highLimit)
{
    DsoErr err = ERR_NONE;
    int max = pos2 > pos1 ? pos2:pos1;
    int min = pos2 < pos1 ? pos2:pos1;
    if(min + add >= highLimit)
    {
        add = highLimit - min;
        err = err_over_upper_range;
    }
    if(max + add <= 0)
    {
        add = 0 - max;
        err = err_over_lower_range;
    }

    pos2 = pos2 + add;
    pos1 = pos1 + add;

    return err;
}

DsoErr addVertPos(int& pos1, int& pos2, int add,
                  int highLimit)
{
    DsoErr err = ERR_NONE;
    int max = pos2 > pos1 ? pos2:pos1;
    int min = pos2 < pos1 ? pos2:pos1;
    if(min + add >= highLimit)
    {
        add = highLimit - min;
        err = err_over_lower_range;
    }
    if(max + add <= 0)
    {
        add = 0 - max;
        err = err_over_upper_range;
    }

    pos2 = pos2 + add;
    pos1 = pos1 + add;

    return err;
}
}
