#ifndef CMANUAL_H
#define CMANUAL_H

#include "cursorlib.h"
#include "cval.h"

namespace cursor_lib {

class CManual
{
public:
    CManual();
public:
    CursorSrc       mSrc;

    CursorArea      mArea;
    CursorView      mView;
    LaNumType       mLaType;

    CursorUnit mUnitX;
    CursorUnit mUnitY;

    int mX1;
    int mX2;

    int mY1;
    int mY2;

    int mRefX1;
    int mRefX2;

    int mRefY1;
    int mRefY2;

    CCursorVal mAx, mBx;
    CCursorVal mAy, mBy;

    //! dx dy
    CCursorVal mDx;
    CCursorVal mDy;

    CCursorVal mInvDx;
public:
    CursorErr setSrc( CursorSrc src );
    CursorSrc getSrc();

    CursorErr setArea( CursorArea area );
    CursorArea getArea();

    CursorErr setView( CursorView view );
    CursorView getView();

    DsoErr    setLaType( LaNumType type );
    LaNumType getLaType( );

    CursorErr setUnitX( CursorUnit unit );
    CursorUnit getUnitX();

    CursorErr setUnitY( CursorUnit unit );
    CursorUnit getUnitY();

    CursorErr setX1( int pos );
    int getX1();

    CursorErr setX2( int pos );
    int getX2();

    CursorErr setX1X2(int add);

    CursorErr setY1( int pos );
    int getY1();

    CursorErr setY2( int pos );
    int getY2();

    CursorErr setY1Y2(int add);

    CursorErr setY1Y2(int pos1, int pos2);

    CursorErr snapXRange();
    CursorErr snapYRange();

    int getRefX1();
    int getRefX2();

    int getRefY1();
    int getRefY2();

    void manualTrackLa(HorizontalView hView);
public:
    CursorErr updateProc();

protected:
    CursorErr xUpdateProc();

    CursorErr xTimeUpdateProc();
    CursorErr xFreqUpdateProc();

    CursorErr xUpdateProcS();
    CursorErr xUpdateProcHz();
    CursorErr xUpdateProcPercent();
    CursorErr xUpdateProcDegree();

    CursorErr xUpdateProcFreq();

    CursorErr yUpdateProc();

    CursorErr yUpdateProcUnit();
    CursorErr yUpdateProcPercent();
};

}
#endif // CMANUAL_H
