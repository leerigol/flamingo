#ifndef CCURSORVAL_H
#define CCURSORVAL_H

#include "cursorlib.h"

namespace cursor_lib
{

/*!
 * \brief The CCURSORVal class
 * 光标数值项
 */
class CCursorVal
{
public:
    CCursorVal();
    CCursorVal( float v, CursorUnit u=Unit_V );
    CCursorVal( const CCursorVal &v );
    CCursorVal( CCursorVal *pV );
    CCursorVal& operator=( const CCursorVal&v);

public:
    void setValue( float val, CursorUnit u=Unit_V );
    void setValue( int hexVal, CursorValueType vType );

    void getValue( float &val );
    void getValue( int &hexVal );

    CursorValueType getType();

    void setUnit( CursorUnit u );
    CursorUnit getUnit();

    void setValid( bool v);
    bool getValid();

    void setLaValid(int valid);
    int  getLaValid();
public:
    union
    {
        float value;
        int hexValue;    //! for la value
    };
    CursorValueType valueType;
    CursorUnit unit;
    int  mlaValid;   //! for la valid
    bool valid;

public:
    static bool checkValid( const CCursorVal &a, const CCursorVal &b );

    CCursorVal operator+(CCursorVal b ) ;
    CCursorVal operator-(CCursorVal b ) ;
    CCursorVal operator*(CCursorVal b ) ;
    CCursorVal operator/(CCursorVal b ) ;

    bool operator==( const CCursorVal &b ) const;

    CCursorVal abs();
    CCursorVal invert( int inv=1);
};

CCursorVal operator/(int de, CCursorVal nu );

//Q_DELCARE_METATYPE( cursor_lib::CCursorVal )

}



#endif // CCURSORVAL_H
