#ifndef CAUTO_H
#define CAUTO_H

#include "cursorlib.h"
#include "../../baseclass/cargument.h"
#include "cval.h"

namespace cursor_lib {

/*!
 * \brief The CAuto class
 * 自动光标
 * - 自动项目
 * - 项目数值
 */
struct AutoLineAttr
{
    int    mColorType;
    int    mPos;
    bool   bVisible;
};

class CAuto
{
public:
    CAuto();

//    void setAutoItem( CursorAutoItem item );
//    CursorAutoItem getAutoItem();
    void updateXProc(CArgument &arg);
    void updateYProc(CArgument &arg);
public:
    QVector<AutoLineAttr*> xAutoLine;
    QVector<AutoLineAttr*> yAutoLine;
    AutoLineAttr mAxLine, mBxLine, mAyLine, mByLine;
};
}

#endif // CAUTO_H
