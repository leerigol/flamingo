#include "cval.h"
#include <QtDebug>

using namespace cursor_lib;

CCursorVal::CCursorVal()
{
}

CCursorVal::CCursorVal( float v, CursorUnit u ) :
    value(v),valueType(cursor_value_float), unit(u),valid(true)
{

}

CCursorVal::CCursorVal( const CCursorVal &v )
{
    *this = v;
}
CCursorVal::CCursorVal( CCursorVal *pV )
{
    *this = *pV;
}
CCursorVal& CCursorVal::operator=( const CCursorVal&v)
{

    value = v.value;
    hexValue = v.hexValue;

    valueType = v.valueType;

    valid = v.valid;
    mlaValid = v.mlaValid;
    unit = v.unit;

    return *this;
}

void CCursorVal::setValue( float val, CursorUnit u )
{
    value = val;
    unit = u;

    valueType = cursor_value_float;

    valid = true;
}

void CCursorVal::setValue( int val, CursorValueType vType )
{
    hexValue = val;
    valueType = vType;

    valid = true;
}

void CCursorVal::getValue( float &val )
{
    Q_ASSERT( valueType == cursor_value_float );

    val = value;
}

void CCursorVal::getValue( int &hexVal )
{
    Q_ASSERT( valueType != cursor_value_float );

    hexVal = hexValue;
}

CursorValueType CCursorVal::getType()
{
    return valueType;
}

void CCursorVal::setUnit( CursorUnit u )
{
    unit = u;
}

CursorUnit CCursorVal::getUnit()
{
    return unit;
}

void CCursorVal::setValid( bool v)
{
    valid = v;
}
bool CCursorVal::getValid()
{
    return valid;
}

void CCursorVal::setLaValid(int valid)
{
    mlaValid = valid;
}

int CCursorVal::getLaValid()
{
    return mlaValid;
}

bool CCursorVal::checkValid( const CCursorVal &a, const CCursorVal &b )
{
    if ( a.valid && b.valid
         && a.valueType == cursor_value_float
         && b.valueType == cursor_value_float )
    {}
    else
    {
        return false;
    }

    if ( a.unit != b.unit )
    {
        return false;
    }

    return true;
}
#define operator_xx( name, op )    \
CCursorVal CCursorVal::name(CCursorVal a ) \
{\
    CCursorVal val;\
\
    val = a;\
\
    val.valid = checkValid( *this, a );\
\
    if ( val.valid )\
    {\
        val.value = value op a.value;\
        val.unit = unit;\
    }\
\
    return val;\
}

//CCursorVal CCursorVal::operator+(CCursorVal a )
//{
//    CCursorVal val;

//    val.valid = checkValid( *this, a );

//    if ( val.valid )
//    {
//        val.value = value + a.value;
//    }

//    return val;
//}

operator_xx( operator+, +)
operator_xx( operator-,-)
operator_xx( operator*,*)
operator_xx( operator/,/)

bool CCursorVal::operator==( const CCursorVal &b ) const
{
    CCursorVal val;

    val.valid = checkValid( *this, b );

    Q_ASSERT(val.unit == b.unit);
    Q_ASSERT(val.valueType == b.valueType);
    if ( val.valid )
    {
        return value == b.value;
    }
    else
    {
        return false;
    }
}

CCursorVal CCursorVal::abs()
{
    CCursorVal val(this);

    if ( val.value < 0
         && val.valueType == cursor_value_float )
    {
        val.value = -val.value;
    }

    return val;
}

CCursorVal CCursorVal::invert(int de )
{
    CCursorVal valOut;

    if ( !this->valid
         || this->valueType != cursor_value_float )
    {
        valOut.setValid( false );
        return valOut;
    }

    if ( this->value == 0 )
    {
        valOut.setValid( false );
        return valOut;
    }

    //! unit
    valOut = *this;
    valOut.value = de / this->value;
    switch( this->unit )
    {
        case Unit_s:
            valOut.unit = Unit_hz;
            break;

        case Unit_hz:
            valOut.unit = Unit_s;
            break;

        default:
            valOut.unit = Unit_U;
        break;
    }

    return valOut;
}
