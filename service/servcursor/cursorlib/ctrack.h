#ifndef CTRACK_H
#define CTRACK_H

#include "cursorlib.h"
#include "cval.h"
#include "../../servtrace/dataprovider.h"
#include "../../baseclass/dsowfm.h"

namespace cursor_lib {


class CTrack
{
public:
    CTrack();

public:
    CursorErr setSrcA( CursorSrc aSrc );
    CursorSrc getSrcA();

    CursorErr setSrcB( CursorSrc bSrc );
    CursorSrc getSrcB();

    CursorErr setTrackMode( CursorTrackMode mode );
    CursorTrackMode getTrackMode();

    CursorErr setAx(int x);
    int getAx();

    CursorErr setBx(int x);
    int getBx();

    CursorErr setABx(int add);

    CursorErr setAy(int y);
    int getAy();

    CursorErr setBy(int y);
    int getBy();

    CursorErr setABy(int add);
public:
    void updateTrackDeltaUseable();
    deltaUseable *getTrackDeltaUseable();

    void updateProc();

protected:
    void trackYProc();
    void trackXProc();

    void trackX(      Chan src,
                      int pos,
                      CCursorVal &xVal,
                      CCursorVal &yVal,
                      CCursorVal &xScr,
                      HorizontalView hView
                      );

    int findX( int y,
               DsoWfm &wfm );
protected:
    CursorSrc mSrcA;
    CursorSrc mSrcB;

public:
    CCursorSrcMgr mTSrcAmgr;
    CCursorSrcMgr mTSrcBmgr;

    CursorTrackMode mTrackMode;

    //! pos
    int mAx,mAy;
    int mBx,mBy;
    bool mxPosMenuChanged;
    bool myPosMenuChanged;

    HorizontalView mSrcAView;
    HorizontalView mSrcBView;

    bool mbPosChanging;
    //! value
    CCursorVal mAxV, mBxV;
    CCursorVal mAyV, mByV;

    CCursorVal mDxV, mDyV, mDxInv;

    //! adc
    CCursorVal mAyAdc, mByAdc;
    CCursorVal mAxScr, mBxScr;

    CCursorVal mAyScr, mByScr;
//    //! user
//    CCursorVal mAxUser, mBxUser;
//    CCursorVal mAyUser, mByUser;

    deltaUseable mDeltaUseable;

    dataProvider* m_pActiveProvider;
};

}

#endif // CTRACK_H
