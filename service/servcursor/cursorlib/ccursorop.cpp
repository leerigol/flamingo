#include "ccursorop.h"

#include "../../../include/dsostd.h"

namespace cursor_lib {
void CCursorOp::trackY( Chan src,
                  int x,
                  CCursorVal &xVal,
                  CCursorVal &yVal,
                  CCursorVal &adcVal
                  )
{
    dsoVert *pVert;
    DsoWfm wfm;
    int adcY;
    int index;

    horiAttr hAttr;
    float val;

    //! ax
    hAttr = getHoriAttr( src, horizontal_view_main );
    val = ( x - hAttr.gnd ) * hAttr.inc;
    xVal.setValue( val, hAttr.unit );

    //! track ay
    pVert = dsoVert::getCH( src );
    if ( NULL == pVert )
    {
        xVal.setValid( false );
        yVal.setValid( false );
        adcVal.setValid( false );
        return;
    }

    //! screen data
    pVert->getPixel( wfm );
    if( x < wfm.m_start || x >= wfm.m_start + wfm.getLength() )
    {
        adcVal.setValid( false );

        yVal.setValid( false );
    }
    else
    {
        index = x - wfm.m_vLeft;

        adcY = wfm.at( index );
        adcVal.setValue( adcY, cursor_value_adc );
        yVal.setValue( wfm.valueAt(index) );
        yVal.setUnit( pVert->getUnit() );
    }
}

void CCursorOp::trackYScr(Chan src, int x, CCursorVal &xVal, CCursorVal &yVal, CCursorVal &scrVal,
                          HorizontalView hView)
{
    dsoVert *pVert;
    DsoWfm wfm;
    int posY;
    int index;

    horiAttr hAttr;
    //! ax
    hAttr = getHoriAttr( src, hView );   

    //! 将xVal的更新放到服务当中
    //! track ay
    pVert = dsoVert::getCH( src );
    if ( NULL == pVert )
    {
        LOG_DBG();
        xVal.setValid( false );
        yVal.setValid( false );
        scrVal.setValid( false );
        return;
    }

    //! screen data
    pVert->getPixel( wfm, chan_none,hView);

    if( x < wfm.start() || x >= wfm.start() + wfm.size() )
    {
        LOG_DBG()<<x<<wfm.size();
        scrVal.setValid( false );
        yVal.setValid( false );
    }
    else
    {
        index = x - wfm.m_start;
        yVal.setValue( wfm.valueAt(index) );

        //! 计算的时候都按照480个像素计算，在app中统一处理
        float refBase = wfm.getyRefBase().toFloat();
        posY = -(yVal.value + pVert->getYRefOffset() * refBase) * scr_vdiv_dots/
                (pVert->getYRefScale() * refBase) + wave_height/2;

        scrVal.setValue( posY, cursor_value_scr);
        yVal.setUnit( pVert->getUnit() );
    }
}

void CCursorOp::trackLa(Chan src, int x,
                        CCursorVal &xVal,
                        CCursorVal &yVal,
                        HorizontalView hView)
{
    Q_UNUSED(xVal);
    Q_UNUSED(hView);
    dsoVert* pVert = dsoVert::getCH(src);
    DsoWfm wfm;
    pVert->getTrace(wfm, chan_none, hView);

    //bug3060 by hxh
    if( wfm.size() > 0 )
    {
        int dxOnOff;
        serviceExecutor::query( serv_name_la, MSG_LA_SELECT_CHAN, dxOnOff );
        yVal.setLaValid( dxOnOff >> (int)d0);

    //    Q_ASSERT(wfm.size() % 2000 == 0);

        //! x:0-999
        int traceIndex = 2 * x * (wfm.size() / 2000);

        //! 存储La的16位数据
        int laData = 0;
        int d15d8Data = wfm.at(traceIndex);
        int d7d0Data = wfm.at(traceIndex + 1);
        laData += d7d0Data;
        laData += d15d8Data*256;

        yVal.setValid(true);
        yVal.setValue(laData, cursor_value_la_lh);
    }
    else
    {
        yVal.setValid(false);
    }
}

}
