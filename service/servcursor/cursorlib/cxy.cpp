#include "../../../include/dsostd.h"

#include "ccursorop.h"
#include "cxy.h"

namespace cursor_lib{

CXy::CXy()
{

}

CursorErr CXy::setXyMode( CursorXYMode xyMode )
{
    mMode = xyMode;
    return err_cursor_none;
}

CursorXYMode CXy::getXyMode()
{
    //! only reserve the manual mode
    return cursor_xy_manual;
}

CursorErr CXy::setSrcX( CursorSrc src )
{
    mSrcX = src;
    return err_cursor_none;
}
CursorSrc CXy::getSrcX()
{
    return mSrcX;
}

CursorErr CXy::setSrcY( CursorSrc src )
{
    mSrcY = src;
    return err_cursor_none;
}
CursorSrc CXy::getSrcY()
{
    return mSrcY;
}

CursorErr CXy::setAx( int pos )
{
    limit_range(pos,0,479);
    mAx = pos;
    return err;
}
int CXy::getAx()
{
    return mAx;
}

CursorErr CXy::setBx( int pos )
{
    limit_range(pos,0,479);
    mBx = pos;
    return err;
}
int CXy::getBx()
{
    return mBx;
}

DsoErr CXy::setAxBx(int add)
{
    DsoErr err = addPos(mAx,mBx,add,wave_height-1);
    return err;
}

CursorErr CXy::setAy( int pos )
{
    limit_vert_range(pos,0,wave_height-1);
    mAy = pos;
    return err;
}
int CXy::getAy()
{
    return mAy;
}

CursorErr CXy::setBy( int pos )
{
    limit_vert_range(pos,0,wave_height-1);
    mBy = pos;
    return err;
}
int CXy::getBy()
{
    return mBy;
}

DsoErr CXy::setAyBy(int add)
{
    DsoErr err = addVertPos(mAy,mBy,add,wave_height-1);
    return err;
}

CursorErr CXy::setT1( int pos )
{
    mT1 = pos;
    return err_cursor_none;
}
int CXy::getT1()
{
    return mT1;
}

CursorErr CXy::setT2( int pos )
{
    mT2 = pos;
    return err_cursor_none;
}
int CXy::getT2()
{
    return mT2;
}

void CXy::updateProc()
{
    float val;
    int yGnd;
    float yInc;

    dsoVert *pVert;

    //! x
    pVert = dsoVert::getCH( mSrcX );
    yGnd = pVert->getyGnd();
    yInc = pVert->getyInc() * pVert->getProbe().toFloat();

    //! ax (范围为0-479)
    val = ( mAx -  adcToTrace( yGnd )) * adcIncToTrace( yInc );
    mAxV.setValue( val, pVert->getUnit() );

    //! bx
    val = ( mBx - adcToTrace( yGnd ) ) * adcIncToTrace( yInc );
    mBxV.setValue( val, pVert->getUnit() );

    //! dx
    mDxV = mBxV - mAxV;

    //! y
    pVert = dsoVert::getCH( mSrcY );
    yGnd = pVert->getyGnd();
    yInc = pVert->getyInc() * pVert->getProbe().toFloat();

    //! ay
    val = ( adcToPixel( yGnd ) - mAy ) * adcIncToTrace( yInc );
    mAyV.setValue( val, pVert->getUnit() );

    //! by
    val = ( adcToPixel( yGnd ) - mBy ) * adcIncToTrace( yInc );
    mByV.setValue( val, pVert->getUnit() );

    //! dx
    mDyV = mByV - mAyV;
}

void CXy::trackProc()
{
    CCursorOp::trackY( mSrcX, mT1, mtT1, mxT1, mAdcX1 );
    CCursorOp::trackY( mSrcY, mT1, mtT1, myT1, mAdcY1 );

    CCursorOp::trackY( mSrcX, mT1, mtT2, mxT2, mAdcX2 );
    CCursorOp::trackY( mSrcY, mT2, mtT2, myT2, mAdcY2 );

    mDt = mtT2 - mtT1;
    mInvDt = mDt.invert().abs();

    //!通道一和通道二的电压差
    mDy1 = myT1 - mxT1;
    mDy2 = myT2 - mxT2;
}

}
