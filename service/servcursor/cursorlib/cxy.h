#ifndef CXY_H
#define CXY_H

#include "cursorlib.h"
#include "cval.h"

namespace cursor_lib {

class CXy
{
public:
    CXy();

public:
    CursorErr setXyMode( CursorXYMode xyMode );
    CursorXYMode getXyMode();

    CursorErr setSrcX( CursorSrc src );
    CursorSrc getSrcX();

    CursorErr setSrcY( CursorSrc src );
    CursorSrc getSrcY();

    CursorErr setAx( int pos );
    int getAx();

    CursorErr setBx( int pos );
    int getBx();

    CursorErr setAxBx( int add );

    CursorErr setAy( int pos );
    int getAy();

    CursorErr setBy( int pos );
    int getBy();

    CursorErr setAyBy( int add );

    CursorErr setT1( int pos );
    int getT1();

    CursorErr setT2( int pos );
    int getT2();

public:
    void updateProc();
    void trackProc();

public:
    CursorXYMode mMode;

    CursorSrc mSrcX, mSrcY;

    int mAx, mBx;
    int mAy, mBy;

    int mT1, mT2;       //! position

public:
    CCursorVal mtT1, mtT2;
    CCursorVal mxT1, myT1;    //! values
    CCursorVal mxT2, myT2;

    CCursorVal mDt, mInvDt;
    CCursorVal mDy1, mDy2;

    CCursorVal mAdcX1, mAdcY1;
    CCursorVal mAdcX2, mAdcY2;

    //! value
    CCursorVal mAxV, mBxV;
    CCursorVal mAyV, mByV;

    CCursorVal mDxV, mDyV;

};

}


#endif // CXY_H
