#ifndef CURSORLIB_H
#define CURSORLIB_H

#include "../../include/dsotype.h"
using namespace DsoType;

namespace cursor_lib {

#define assert_ptr(ptr) if ( NULL == ptr ) return err_cursor_null_ptr;
#define limit_range( a, mi, ma )    CursorErr err;\
                    if ( a < mi ){ a = mi; err = err_over_lower_range;}\
    else if ( a > ma ) { a = ma; err = err_over_upper_range; }\
    else{ err = err_cursor_none;}

//! 垂直调节，0表示上线，479表示下限
#define limit_vert_range( a, mi, ma )    CursorErr err;\
                    if ( a < mi ){ a = mi; err = err_over_upper_range;}\
    else if ( a > ma ) { a = ma; err = err_over_lower_range; }\
    else{ err = err_cursor_none;}

//! enum with system
#define CursorSrc       Chan

#define cursor_src_1    chan1
#define cursor_src_2    chan2
#define cursor_src_3    chan3
#define cursor_src_4    chan4

#define cursor_src_la   d0d15

#define CursorUnit      Unit

#define CursorArea       HorizontalView
#define cursor_area_main horizontal_view_main
#define cursor_area_zoom horizontal_view_zoom

//! error code
#define CursorErr       DsoErr
#define err_over_lower_range    ERR_OVER_LOW_RANGE
#define err_over_upper_range    ERR_OVER_UPPER_RANGE
#define err_cursor_null_ptr     ERR_NULL_PTR
#define err_cursor_none         ERR_NONE
#define err_cursor_no_data      ERR_NO_TRACE
#define err_cursor_div_0        ERR_CURSOR_NO_REF_RANGE

enum CursorMode
{
    cursor_mode_off,
    cursor_mode_manual,
    cursor_mode_track,
    cursor_mode_auto,
    cursor_mode_xy,
    cursor_mode_meas,
};

//! 追踪Y方向的数值时，显示光标X的位置。
enum CursorTrackMode
{
    cursor_track_y,
    cursor_track_x,
};

enum CursorTrackXMode
{
    cursor_trackx_auto, //! near the center point
    cursor_trackx_first,
    cursor_trackx_last,
};

enum CursorXYMode
{
    cursor_xy_manual,
    cursor_xy_track,
};

enum CursorValueType
{
    cursor_value_la_binary,
    cursor_value_la_lh,
    cursor_value_float,
    cursor_value_la_l8,
    cursor_value_la_h8,

    cursor_value_adc,
    cursor_value_scr,
};

enum CursorView
{
    cursor_view_hori,
    cursor_view_vert,
};

enum CursorStyle
{
    cursor_style_1,
    cursor_style_2,
    cursor_style_3,
    cursor_style_4,
};

enum CursorColor
{
    cursor_color_1,
    cursor_color_2,
    cursor_color_3,
    cursor_color_4,
};

enum CursorAlignment
{
    cursor_align_vert,
    cursor_align_hori,
};

enum CursorAutoItem
{
    CursorAuto_none,

    CursorAuto_1,
    CursorAuto_2,
    CursorAuto_3,
    CursorAuto_4,

    CursorAuto_5,
    CursorAuto_6,
    CursorAuto_7,
    CursorAuto_8,
};

struct deltaUseable
{
    bool xUsable;
    bool yUsable;
    bool invXUsable;
};

struct CCursorSrcMgr
{
    Chan mCh;
    HorizontalView mDataView;
    HorizontalView mDispView;
};

//! 同时移动ABpos时，只有两个光标都超出屏幕才算越界
DsoErr addPos(int& pos1, int& pos2, int add,
                     int highLimit);

DsoErr addVertPos(int& pos1, int& pos2, int add,
                  int highLimit);
}

#endif // CURSORLIB_H
