#include "cauto.h"
#include "../../include/dsodbg.h"

namespace cursor_lib {

CAuto::CAuto()
{
}

//void CAuto::setAutoItem(CursorAutoItem item)
//{
//    //
//}

//CursorAutoItem CAuto::getAutoItem()
//{
//    return CursorAuto_none;
//}

//!argument:
//! ID
//! visable
//! pos
//! color type

void CAuto::updateXProc(CArgument &arg)
{
    LOG_DBG();
    int id = arg[0].iVal;
    while(xAutoLine.size() < id + 1)
    {
        xAutoLine.push_back( new AutoLineAttr );
    }
    xAutoLine[id]->bVisible = arg[1].bVal;
    xAutoLine[id]->mPos = arg[2].iVal;
    xAutoLine[id]->mColorType = arg[3].iVal;
}

void CAuto::updateYProc(CArgument &arg)
{
    LOG_DBG();
    int id = arg[0].iVal;
    while(yAutoLine.size() < id + 1)
    {
        yAutoLine.push_back( new AutoLineAttr );
    }
    yAutoLine[id]->bVisible = arg[1].bVal;
    yAutoLine[id]->mPos = arg[2].iVal;
    yAutoLine[id]->mColorType = arg[3].iVal;
}

}
