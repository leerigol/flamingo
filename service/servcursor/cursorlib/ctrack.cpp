
#include "../../../include/dsostd.h"

#include "ccursorop.h"
#include "ctrack.h"

namespace cursor_lib {

CTrack::CTrack()
{
}

CursorErr CTrack::setSrcA( CursorSrc aSrc )
{
    mTSrcAmgr.mCh = aSrc;

    return err_cursor_none;
}

CursorSrc CTrack::getSrcA()
{
    return mTSrcAmgr.mCh;
}

CursorErr CTrack::setSrcB( CursorSrc bSrc )
{
    mTSrcBmgr.mCh = bSrc;
    return err_cursor_none;
}

CursorSrc CTrack::getSrcB()
{
    return mTSrcBmgr.mCh;
}

CursorErr CTrack::setTrackMode( CursorTrackMode mode )
{
    mTrackMode = mode;
    return err_cursor_none;
}
CursorTrackMode CTrack::getTrackMode()
{
    return mTrackMode;
}

CursorErr CTrack::setAx(int x)
{
    limit_range(x,0,999);
    mAx = x;
    return err;
}
int CTrack::getAx()
{
    return mAx;
}

CursorErr CTrack::setBx(int x)
{
    limit_range(x,0,999);
    mBx = x;
    return err;
}
int CTrack::getBx()
{
    return mBx;
}

DsoErr CTrack::setABx(int add)
{
    DsoErr err;
    err = addPos(mAx, mBx, add, 999);
    return err;
}

CursorErr CTrack::setAy(int y)
{
    limit_vert_range(y,0,479);
    mAy = y;
    return err;
}
int CTrack::getAy()
{
    return mAy;
}

CursorErr CTrack::setBy(int y)
{
    limit_vert_range(y,0,479);
    mBy = y;
    return err;
}
int CTrack::getBy()
{
    return mBy;
}

DsoErr CTrack::setABy(int add)
{
    DsoErr err;
    err = addVertPos(mAy, mBy, add, 479);
    return err;
}

void CTrack::updateTrackDeltaUseable( )
{
    dsoVert *pVert;
    horiAttr hAttrA, hAttrB;
    Unit unitA, unitB;

    pVert = dsoVert::getCH( mTSrcAmgr.mCh );
    if ( pVert == NULL )
    {
        mDeltaUseable.xUsable = false;
        mDeltaUseable.yUsable = false;
        mDeltaUseable.invXUsable = false;
        return;
    }
    unitA = pVert->getUnit();
    hAttrA = getHoriAttr( mTSrcAmgr.mCh, horizontal_view_main );

    pVert = dsoVert::getCH( mTSrcBmgr.mCh );
    if ( pVert == NULL )
    {
        mDeltaUseable.xUsable = false;
        mDeltaUseable.yUsable = false;
        mDeltaUseable.invXUsable = false;
        return;
    }
    unitB = pVert->getUnit();
    hAttrB = getHoriAttr( mTSrcBmgr.mCh, horizontal_view_main );

    mDeltaUseable.yUsable = ( unitA == unitB );

    mDeltaUseable.yUsable = ( unitA == unitB );

    mDeltaUseable.xUsable = ( hAttrA.zone == hAttrB.zone );

    mDeltaUseable.invXUsable = (hAttrA.unit == Unit_s) && mDeltaUseable.xUsable;
}

deltaUseable *CTrack::getTrackDeltaUseable()
{
    return &mDeltaUseable;
}

void CTrack::updateProc()
{
    switch( mTrackMode )
    {
    case cursor_track_y:
        return trackYProc();

    case cursor_track_x:
        return trackXProc();

    default:
        return trackYProc();
    }
}

void CTrack::trackYProc()
{
    bool zoomEn;
    serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);

    if(zoomEn)
    {
        CCursorOp::trackYScr( mTSrcAmgr.mCh, mAx, mAxV, mAyV, mAyScr, horizontal_view_zoom);
        CCursorOp::trackYScr( mTSrcBmgr.mCh, mBx, mBxV, mByV, mByScr, horizontal_view_zoom );
    }
    else
    {
        CCursorOp::trackYScr( mTSrcAmgr.mCh, mAx, mAxV, mAyV, mAyScr,horizontal_view_main );
        CCursorOp::trackYScr( mTSrcBmgr.mCh, mBx, mBxV, mByV, mByScr,horizontal_view_main );
    }

    //! dx
    mDxV = mBxV - mAxV;
    //! dx inv
    if ( mBxV.unit == mAxV.unit
         && mAxV.unit == Unit_s )
    {
        mDxInv = (mBxV - mAxV).invert().abs();
    }
    else
    {
        mDxInv.setValid( false );
    }

    //! dy v
    mDyV = mByV - mAyV;
}

void CTrack::trackXProc()
{
    bool zoomEn;
    serviceExecutor::query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);
    if(zoomEn)
    {
        trackX( mTSrcAmgr.mCh, mAy, mAxV, mAyV, mAxScr, horizontal_view_zoom);
        trackX( mTSrcBmgr.mCh, mBy, mBxV, mByV, mBxScr, horizontal_view_zoom);
    }
    else
    {
        trackX( mTSrcAmgr.mCh, mAy, mAxV, mAyV, mAxScr, horizontal_view_main);
        trackX( mTSrcBmgr.mCh, mBy, mBxV, mByV, mBxScr, horizontal_view_main);
    }

    //! dx
    mDxV = mBxV - mAxV;

    //! dx inv
    if ( mBxV.unit == mAxV.unit
         && mAxV.unit == Unit_s )
    {
        mDxInv = (mBxV - mAxV).invert().abs();
    }
    else
    {
        mDxInv.setValid( false );
    }

    //! dy v
    mDyV = mByV - mAyV;
}

void CTrack::trackX(Chan src,
                  int pos,
                  CCursorVal &xVal,
                  CCursorVal &yVal,
                  CCursorVal &xScr,
                  HorizontalView hView)
{
    DsoWfm wfm;

    horiAttr hAttr;
    float val;

    //! y val
    dsoVert *pVert = dsoVert::getCH( src );
    if ( NULL == pVert )
    {
        xVal.setValid( false );
        yVal.setValid( false );
        xScr.setValid( false );
        return;
    }
    pVert->getPixel( wfm, chan_none, hView);

//    yVal.setValue( wfm.toReal(pixelToAdc(pos)) );

    //! pos 换算为电压值，再换算到之前wfm中的adc值 for bug 3304
    float dsoVertBase = pVert->getYRefBase().toFloat() * pVert->getProbe().toFloat();
    float vertScale = pVert->getYRefScale()*dsoVertBase;
    float vertOffset = pVert->getYRefOffset()*dsoVertBase;
    float Yreal = -(pos - 240)*1.0/scr_vdiv_dots*vertScale - vertOffset;

    int yAdc = Yreal/wfm.realyInc() + wfm.getyGnd();
    //! find x
    int xPos = findX( yAdc, wfm );
    if ( xPos < 0 )
    {
        xVal.setValid( false );

        xScr.setValid( false );
    }
    else
    {
        hAttr = getHoriAttr( src, hView );

        val = ( xPos - hAttr.gnd ) * hAttr.inc;
        xVal.setValue( val, hAttr.unit );

        xScr.setValue( xPos + wfm.m_vLeft, cursor_value_scr );
    }
}

int CTrack::findX( int y,
           DsoWfm &wfm )
{
    int i;
    DsoPoint *pPt;
    pPt = wfm.getPoint();
    for ( i = wfm.start(); i < wfm.size()-1; i++ )
    {
        if ( pPt[i] == y )
        {
            return i;
        }
        //! rise
        else if (  y > pPt[i] && y < pPt[i+1] )
        {
            if ( ( y - pPt[i] ) <= (pPt[i+1] - y ) )
            {
                return i;
            }
            else
            {
                return i+1;
            }
        }
        //! fall
        else if ( y < pPt[i] && y > pPt[i+1] )
        {
            if ( (pPt[i] - y) <= (pPt[i+1] - y) )
            {
                return i;
            }
            else
            {
                return i+1;
            }
        }
        //! continue
        else
        {}
    }

    return -1;
}

}
