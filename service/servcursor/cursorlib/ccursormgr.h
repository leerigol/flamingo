#ifndef CCURSORMGR_H
#define CCURSORMGR_H

#include "cmanual.h"
#include "ctrack.h"
#include "cauto.h"
#include "cxy.h"

namespace cursor_lib {

struct CCursorMgr
{
    CursorMode mMode;

    CManual mManual;

    CTrack mTrack;

    CAuto   mAuto;

    CXy mXy;

    //! value cache
    cursor_lib::CCursorVal mCacheVal;
};

}

#endif // CCURSORMGR_H
