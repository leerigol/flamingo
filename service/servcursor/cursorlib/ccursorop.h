#ifndef CCURSOROP_H
#define CCURSOROP_H

#include "cursorlib.h"
#include "cval.h"

#include "../../baseclass/dsowfm.h"
namespace cursor_lib {

class CCursorOp
{
public:
    static void trackY( Chan src,
                   int x,
                   CCursorVal &xVal,
                   CCursorVal &yVal,
                   CCursorVal &adcVal
                   );
    static void trackYScr( Chan src,
                   int x,
                   CCursorVal &xVal,
                   CCursorVal &yVal,
                   CCursorVal &pixVal,
                   HorizontalView hView );
    static void trackLa( Chan src,
                         int x,
                         CCursorVal &xVal,
                         CCursorVal &yVal,
                         HorizontalView hView );
};

}

#endif // CCURSOROP_H
