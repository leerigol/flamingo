
#include "../../../include/dsostd.h"
#include "ccursorop.h"

#include "cmanual.h"

using namespace cursor_lib;

CManual::CManual()
{

}

CursorErr CManual::setSrc( CursorSrc src )
{
    mSrc = src;
    return err_cursor_none;
}
CursorSrc CManual::getSrc()
{
    return mSrc;
}

CursorErr CManual::setArea( CursorArea area )
{
    mArea = area;
    return err_cursor_none;
}
CursorArea CManual::getArea()
{
    return mArea;
}

CursorErr CManual::setView( CursorView view )
{
    mView = view;
    return err_cursor_none;
}
CursorView CManual::getView()
{
    return mView;
}

CursorErr CManual::setLaType(LaNumType type)
{
    mLaType = type;
    return ERR_NONE;
}

LaNumType CManual::getLaType()
{
    return mLaType;
}

CursorErr CManual::setUnitX( CursorUnit unit )
{
    mUnitX = unit;
    return err_cursor_none;
}
CursorUnit CManual::getUnitX()
{
    return mUnitX;
}

CursorErr CManual::setUnitY( CursorUnit unit )
{
    mUnitY = unit;
    return err_cursor_none;
}

CursorUnit CManual::getUnitY()
{
    return mUnitY;
}

CursorErr CManual::setX1( int pos )
{
    limit_range(pos,0,999);
    mX1 = pos;
    return err;
}

int CManual::getX1()
{
    return mX1;
}

CursorErr CManual::setX2( int pos )
{
    limit_range(pos,0,999);
    mX2 = pos;
    return err;
}
int CManual::getX2()
{
    return mX2;
}

//! 只有两个范围都超限才算越界
DsoErr CManual::setX1X2(int add)
{
    DsoErr err = ERR_NONE;
//    int max = mX2 > mX1 ? mX2:mX1;
//    int min = mX2 < mX1 ? mX2:mX1;
//    if(max + add >= 1000)
//    {
//        add = 999 - max;
//        err = err_over_upper_range;
//    }
//    if(min + add < -1000)
//    {
//        add = -1000 - min;
//        err = err_over_lower_range;
//    }

//    mX2 = mX2 + add;
//    mX1 = mX1 + add;
    err = addPos(mX1, mX2, add, 1000);
    return err;
}

CursorErr CManual::setY1( int pos )
{
    limit_vert_range(pos,0,479);
    mY1 = pos;
    return err;
}

int CManual::getY1()
{
    return mY1;
}

CursorErr CManual::setY2( int pos )
{
    limit_vert_range(pos,0,479);
    mY2 = pos;
    return err;
}

int CManual::getY2()
{
    return mY2;
}

DsoErr CManual::setY1Y2(int add)
{
    DsoErr err;
//    int max = mY2 > mY1 ? mY2:mY1;
//    int min = mY2 < mY1 ? mY2:mY1;
//    if(max + add >= 1000)
//    {
//        add = 999 - max;
//        err = err_over_upper_range;
//    }
//    if(min + add < -1000)
//    {
//        add = -1000 - min;
//        err = err_over_lower_range;
//    }

//    mX2 = mX2 + add;
//    mX1 = mX1 + add;
    err = addVertPos(mY1, mY2, add, 479);
    return err;
}

CursorErr CManual::snapXRange()
{
    mRefX1 = mX1;
    mRefX2 = mX2;
    return err_cursor_none;
}
CursorErr CManual::snapYRange()
{
    mRefY1 = mY1;
    mRefY2 = mY2;
    return err_cursor_none;
}

int CManual::getRefX1()
{
    return mRefX1;
}

int CManual::getRefX2()
{
    return mRefX2;
}

int CManual::getRefY1()
{
    return mRefY1;
}

int CManual::getRefY2()
{
    return mRefY2;
}

void CManual::manualTrackLa(HorizontalView hView)
{
    CCursorOp::trackLa( mSrc, mX1, mAx, mAy, hView);
    CCursorOp::trackLa( mSrc, mX2, mBx, mBy, hView);
}

CursorErr CManual::updateProc()
{
    xUpdateProc();
    if( mSrc != la)
    {
        yUpdateProc();
    }

    return err_cursor_none;
}

CursorErr CManual::xUpdateProc()
{
    horiAttr attr;

    attr = getHoriAttr( mSrc, mArea );

    if ( attr.zone == horizontal_time_zone )
    {
        return xTimeUpdateProc();
    }
    else
    {
        return xFreqUpdateProc();
    }
}

CursorErr CManual::xTimeUpdateProc()
{
//    qDebug()<<__FUNCTION__;
    switch( mUnitX )
    {
    case Unit_s:
        return xUpdateProcS();

    case Unit_hz:
        return xUpdateProcHz();

    case Unit_degree:
        return xUpdateProcDegree();

    case Unit_percent:
        return xUpdateProcPercent();

    default:
        return xUpdateProcS();
    }
}

CursorErr CManual::xFreqUpdateProc()
{
    return xUpdateProcFreq();
}

CursorErr CManual::xUpdateProcS()
{
//    qDebug()<<__FUNCTION__;
    horiAttr attr;
    float val;

    attr = getHoriAttr( mSrc, mArea );

    if ( mSrc == chan_none )
    {
        mAx.setValid( false );
        mBx.setValid( false );
        mDx.setValid( false );
        mInvDx.setValid( false );

        return err_cursor_no_data;
    }
    //! ax
    val = (mX1 - attr.gnd) * attr.inc;
    mAx.setValue( val, Unit_s );

    //! bx
    val = (mX2 - attr.gnd) * attr.inc;
    mBx.setValue( val, Unit_s );

    mDx = mBx - mAx;

    if ( mX1 == mX2 )
    {
        mInvDx.setValid( false );
    }
    else
    {
        mInvDx = mDx.invert();
        mInvDx = mInvDx.abs();
    }

    return err_cursor_none;
}

//! 1 / Dx的含义不是频率差，是时间差的倒数；
CursorErr CManual::xUpdateProcHz()
{
    CCursorVal deltaTime;
    CursorErr err;
    err = xUpdateProcS();

    if(err != ERR_NONE)
    {
        return err;
    }

    deltaTime = mDx.abs();
    //! abs
    mAx = mAx.abs();
    mBx = mBx.abs();
    mAx = mAx.invert();
    mBx = mBx.invert();

    mInvDx = deltaTime;
    if ( mX1 == mX2 )
    {
        mDx.setValid( false );
    }
    else
    {
        mDx = deltaTime.invert();
    }

    return err_cursor_none;
}
CursorErr CManual::xUpdateProcPercent()
{
    float ratio;
    int dist;

    dist = mRefX2 - mRefX1;

    if ( dist == 0 )
    {
        mAx.setValid( false );
        mBx.setValid( false );
        mDx.setValid( false );
        mInvDx.setValid( false );

        return err_cursor_none;
    }

    //! mAy
    ratio = ( mX1 - mRefX1 ) * 100.0f / dist;
    mAx.setValue( ratio, Unit_percent );

    //! mBy
    ratio = ( mX2 - mRefX1 ) * 100.0f / dist;
    mBx.setValue( ratio, Unit_percent );

    //! dy
    mDx = mBx - mAx;

    //! inv dx
    mInvDx.setValid( false );

    return err_cursor_none;
}
CursorErr CManual::xUpdateProcDegree()
{
    float ratio;
    int dist;
    int delta;
    dist = mRefX2 - mRefX1;

    if ( dist == 0 )
    {
        mAx.setValid( false );
        mBx.setValid( false );
        mDx.setValid( false );
        mInvDx.setValid( false );

        return err_cursor_none;
    }

    //! mAy
    delta = mX1 - mRefX1;
//! 把数值范围限制在0-360之间
//    delta %= dist;
//    if ( delta < 0 ) delta += dist;
    ratio = ( delta ) * 360.0f / dist;
    mAx.setValue( ratio, Unit_degree );

    //! mBy
    delta = mX2 - mRefX1;
//    if ( delta < 0 ) delta += dist;
    ratio = ( delta ) * 360.0f / dist;
    mBx.setValue( ratio, Unit_degree );

    //! dy
    mDx = mBx - mAx;

    mInvDx.setValid( false );

    return err_cursor_none;
}

CursorErr CManual::xUpdateProcFreq()
{
    xUpdateProcS();

    mAx.setUnit( Unit_hz );
    mBx.setUnit( Unit_hz );

    mDx.setUnit( Unit_hz );
    mInvDx.setValid( false );

    return err_cursor_none;
}

CursorErr CManual::yUpdateProc()
{
    switch( mUnitY )
    {
        case Unit_V:
        case Unit_A:
        case Unit_W:
        case Unit_U:
            return yUpdateProcUnit();

        case Unit_percent:
            return yUpdateProcPercent();

        default:
            return yUpdateProcUnit();
    }

    return err_cursor_none;
}

CursorErr CManual::yUpdateProcUnit()
{
    float val;
    int yGnd;
    float yInc;

    dsoVert *pVert;

    pVert = dsoVert::getCH( mSrc );

    if ( NULL == pVert )
    {
        mAy.setValid( false );
        mBy.setValid( false );
        mDy.setValid( false );
        return err_cursor_null_ptr;
    }

    yGnd = pVert->getyGnd();
    yInc = pVert->getyInc() * pVert->getProbe().toFloat();

    //! ay
    val = ( adcToTrace( yGnd ) - mY1 ) * adcIncToTrace( yInc );
    mAy.setValue( val, pVert->getUnit() );

    //! by
    val = ( adcToTrace( yGnd ) - mY2 ) * adcIncToTrace( yInc );
    mBy.setValue( val, pVert->getUnit() );

    //! dy
    mDy = mBy - mAy;

    return err_cursor_none;
}
CursorErr CManual::yUpdateProcPercent()
{
    float ratio;
    int dist;

    dist = mRefY2 - mRefY1;

    if ( mSrc == chan_none )
    {
        mAy.setValid( false );
        mBy.setValid( false );
        mDy.setValid( false );
        return err_cursor_no_data;
    }

    //! mAy
    ratio = ( mY1 - mRefY1 ) * 100.0f / dist;
    mAy.setValue( ratio, Unit_percent );

    //! mBy
    ratio = ( mY2 - mRefY1 ) * 100.0f / dist;
    mBy.setValue( ratio, Unit_percent );

    //! dy
    mDy = mBy - mAy;

    return err_cursor_none;
}

