
#include "servcursor_imp.h"

void servCursor::format(cursor_lib::CCursorVal *pVal, QString &str)
{
    //! no input
    if ( NULL == pVal )
    {
        str = QString::fromLatin1("*****");
        return;
    }

    //! invalid
    if ( !pVal->getValid() )
    {
        switch( pVal->getType() )
        {

        case cursor_lib::cursor_value_float:
            str = QString::fromLatin1("*****");
            return;

        case cursor_lib::cursor_value_la_l8:
        case cursor_lib::cursor_value_la_h8:
            str = QString::fromLatin1("XX");
            return;

        case cursor_lib::cursor_value_la_lh:
            str = QString::fromLatin1("XXXX");
            return;

        default:
            str = QString::fromLatin1("*****");
            return;
        }
    }

    //! valid
    switch( pVal->getType() )
    {
    case cursor_lib::cursor_value_float:
        //! for bug542:99.9999847格式化为了99.99
        //! add by zx:因为cursor只有6位有效数字，
        //! 所以在第七位上补了一个数不影响正常结果
        //! 导致cursor死机的根本原因：误差的逐步累计
//        pVal->value += pVal->value / 1e6;
//        gui_fmt::CGuiFormatter::format( str, pVal->value );

        {
            float temp = pVal->value / 1e6 + pVal->value;
            gui_fmt::CGuiFormatter::format( str, temp );
        }
        break;

    case cursor_lib::cursor_value_la_l8:
        str = QString("%1").arg( pVal->hexValue&0xff, 0, 16 );
        break;

    case cursor_lib::cursor_value_la_h8:
        str = QString("%1").arg( (pVal->hexValue>>8)&0xff, 0, 16 );
        return;

    case cursor_lib::cursor_value_la_lh:
        str = QString("0x%1").arg( pVal->hexValue, 4, 16, QLatin1Char('0'));
        return;

    default:
        str = QString::fromLatin1("*****");
        return;
    }

    //!unit
    QString strUnit;

    //for bug1805 by zy
#if 1
    if(pVal->getUnit() >= Unit_End  || pVal->getUnit() < Unit_none)
    {
        strUnit = gui_fmt::CGuiFormatter::toString( Unit_none );
    }
    else
    {
        strUnit = gui_fmt::CGuiFormatter::toString( pVal->getUnit() );
    }
#else
    strUnit = gui_fmt::CGuiFormatter::toString( pVal->getUnit() );
#endif
    str = str + strUnit;
}

void servCursor::format(CCursorVal *pVal, QString &str16, QString &str2h, QString &str2l)
{
    //! no input
    if ( NULL == pVal || !pVal->getValid() )
    {
        str16 = QString::fromLatin1("0xXXXX");
        str2h = QString::fromLatin1("XXXXXXXX");
        str2l = QString::fromLatin1("XXXXXXXX");
        return;
    }

    str16 = QString("0x%1").arg( pVal->hexValue, 4, 16, QLatin1Char('0'));

    str2h = QString("%1 %2").arg( (pVal->hexValue >> 12) & 0xf, 4, 2, QLatin1Char('0')).
            arg( (pVal->hexValue >> 8) & 0xf, 4, 2, QLatin1Char('0'));

    str2l = QString("%1 %2").arg( (pVal->hexValue >> 4) & 0xf, 4, 2, QLatin1Char('0')).
            arg( pVal->hexValue & 0xf, 4, 2, QLatin1Char('0'));


    int dxValid = pVal->getLaValid();
    for(int i = 0;i < 8;i++)
    {
        int labit = get_bit(dxValid, i);
        if( labit == 0)
        {
            //! d0 : str2l[8]   d4:str2l[3]
            str2l[8 - i - (i/4)] = QLatin1Char('X');
        }
    }
    for(int i = 8;i < 16;i++)
    {
        int labit = get_bit(dxValid, i);
        if( labit == 0)
        {
            str2h[16 - i - ((i - 8)/4)] = QLatin1Char('X');
        }
    }

    return;
}


