
#include "servcursor_imp.h"

//CursorAutoItem servCursor::getAutoItem()
//{
//    return mAuto.getAutoItem();
//}

void* servCursor::getAutoXLine()
{
    return &mAuto.xAutoLine;
}

void* servCursor::getAutoYLine()
{
    return &mAuto.yAutoLine;
}

DsoErr servCursor::setAutoXLine(CArgument &arg)
{
    mAuto.updateXProc(arg);
    defer(cmd_auto_x_track);

    return ERR_NONE;
}

DsoErr servCursor::setAutoYLine(CArgument &arg)
{
    mAuto.updateYProc(arg);
    defer(cmd_auto_y_track);

    return ERR_NONE;
}

DsoErr servCursor::testAutoAxLine()
{
    qDebug()<<__FUNCTION__<<__LINE__;
    CArgument autoArg;
    autoArg.setVal(true, 0);
    autoArg.setVal(100,1);
    autoArg.setVal(Qt::red,2);
    serviceExecutor::post( serv_name_cursor, servCursor::cmd_auto_x_changed, autoArg);
    qDebug()<<__FUNCTION__<<__LINE__;

    return ERR_NONE;
}

DsoErr servCursor::setMeasIndiOnOff(bool onOff)
{
    serviceExecutor::post(E_SERVICE_ID_MEASURE, MSG_APP_MEAS_INDICATOR, onOff);
    return ERR_NONE;
}

bool servCursor::getMeasIndiOnOff()
{
    bool onOff;
    serviceExecutor::query(serv_name_measure, MSG_APP_MEAS_INDICATOR, onOff);
    return onOff;
}
