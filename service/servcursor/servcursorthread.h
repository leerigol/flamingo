#ifndef SERVCURSORTHREAD_H
#define SERVCURSORTHREAD_H

#include <QtCore>

//! 避免出现循环依赖
class servCursor;

class servCursorThread : public QThread
{
public:
    servCursorThread(servCursor *pCursor, QObject *parent = NULL);

public:
    virtual void run();

private:
    servCursor *m_pServ;
};

#endif // SERVCURSORTHREAD_H
