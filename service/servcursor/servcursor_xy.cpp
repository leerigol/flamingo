#include "servcursor_imp.h"

DsoErr servCursor::setXyMode( CursorXYMode mode )
{
    mXy.setXyMode( mode );
    return ERR_NONE;
}
CursorXYMode servCursor::getXyMode()
{
    return mXy.getXyMode();
}

DsoErr servCursor::setAx( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32XY_XB);
        return ERR_NONE;
    }

    defer( cmd_xy_pos_changed );

    return mXy.setAx(pos);
}
int servCursor::getAx()
{
    setuiZVal( cursor_tune_z_val );
    return mXy.getAx();
}

DsoErr servCursor::setBx( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32XY_YA);
        return ERR_NONE;
    }

    defer( cmd_xy_pos_changed );

    return mXy.setBx(pos);
}
int servCursor::getBx()
{
    setuiZVal( cursor_tune_z_val );
    return mXy.getBx();
}

DsoErr servCursor::setABx( int add )
{
    if ( add == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32XY_YAB);
        return ERR_NONE;
    }

    DsoErr err = mXy.setAxBx( -add );
    defer( cmd_xy_pos_changed );
    return err;
}

int servCursor::getABx()
{
    setuiZVal( cursor_tune_z_val );
    return 0;
}

DsoErr servCursor::setAy( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32XY_YB);
        return ERR_NONE;
    }
    defer( cmd_xy_pos_changed );
    return mXy.setAy(pos);
}
int servCursor::getAy()
{
    setuiZVal( cursor_tune_z_val );
    setuiStep( -1 );
    return mXy.getAy();
}

DsoErr servCursor::setBy( int pos )
{
    if ( pos == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32XY_XA);
        return ERR_NONE;
    }

    defer( cmd_xy_pos_changed );

    return mXy.setBy(pos);
}

int servCursor::getBy()
{
    setuiZVal( cursor_tune_z_val );
    setuiStep( -1 );
    return mXy.getBy();
}

DsoErr servCursor::setABy(int add )
{
    if ( add == cursor_tune_z_val )
    {
        setuiFocus(MSG_CURSOR_S32XY_XAB);
        return ERR_NONE;
    }

    DsoErr err = mXy.setAyBy( -add );
    defer( cmd_xy_pos_changed );
    return err;
}

int servCursor::getABy()
{
    setuiZVal( cursor_tune_z_val );
    setuiStep( -1 );
    return 0;
}

DsoErr servCursor::setAHPos( int pos )
{
    defer( cmd_xy_hpos_changed );
    return mXy.setT1( pos );
}

int servCursor::getAHPos()
{
    setuiZVal( cursor_tune_z_val );
    return mXy.getT1();
}

DsoErr servCursor::setBHPos( int pos )
{
    defer( cmd_xy_hpos_changed );
    return mXy.setT2( pos );
}

int servCursor::getBHPos()
{
    setuiZVal( cursor_tune_z_val );
    return mXy.getT2();
}

DsoErr servCursor::setABHPos( int add )
{
    DsoErr err = mXy.setAxBx( -add );
    return err;
}

int servCursor::getABHpos()
{
    setuiZVal( cursor_tune_z_val );
    return 0;
}

DsoErr servCursor::setLisajous(void)
{
    mViewLisajous = !mViewLisajous;
    return ERR_NONE;
}

bool servCursor::getLisajous()
{
    return mViewLisajous;
}

DsoErr servCursor::uisetLisajous( bool /*b*/ )
{
    defer( MSG_CURSOR_LISAJOUS );
    return ERR_NONE;
}

//! xy
void *servCursor::getXyAx()
{
    mCacheVal = mXy.mAxV;
    return &mCacheVal;
}

void *servCursor::getXyBx()
{
    mCacheVal = mXy.mBxV;
    return &mCacheVal;
}
void *servCursor::getXyAy()
{
    mCacheVal = mXy.mAyV;
    return &mCacheVal;
}
void *servCursor::getXyBy()
{
    mCacheVal = mXy.mByV;
    return &mCacheVal;
}

void *servCursor::getXyDx()
{
    mCacheVal = mXy.mDxV;
    return &mCacheVal;
}
void *servCursor::getXyDy()
{
    mCacheVal = mXy.mDyV;
    return &mCacheVal;
}

//! track xy
void *servCursor::getXyTrackT1()
{
    mCacheVal = mXy.mtT1;
    return &mCacheVal;
}
void *servCursor::getXyTrackT2()
{
    mCacheVal = mXy.mtT2;
    return &mCacheVal;
}

void *servCursor::getXyTrackX1()
{
    mCacheVal = mXy.mxT1;
    return &mCacheVal;
}
void *servCursor::getXyTrackY1()
{
    mCacheVal = mXy.myT1;
    return &mCacheVal;
}

void *servCursor::getXyTrackX2()
{
    mCacheVal = mXy.mxT2;
    return &mCacheVal;
}
void *servCursor::getXyTrackY2()
{
    mCacheVal = mXy.myT2;
    return &mCacheVal;
}

void *servCursor::getXyTrackDt()
{
    mCacheVal = mXy.mDt;
    return &mCacheVal;
}
void *servCursor::getXyTrackInvDt()
{
    mCacheVal = mXy.mInvDt;
    return &mCacheVal;
}

void *servCursor::getXyTrackDy1()
{
    mCacheVal = mXy.mDy1;
    return &mCacheVal;
}

void *servCursor::getXyTrackDy2()
{
    mCacheVal = mXy.mDy2;
    return &mCacheVal;
}

void *servCursor::getXyTrackX1Track()
{
    mCacheVal = mXy.mAdcX1;
    return &mCacheVal;
}

void *servCursor::getXyTrackY1Track()
{
    mCacheVal = mXy.mAdcY1;
    return &mCacheVal;
}

void *servCursor::getXyTrackX2Track()
{
    mCacheVal = mXy.mAdcX2;
    return &mCacheVal;
}

void *servCursor::getXyTrackY2Track()
{
    mCacheVal = mXy.mAdcY2;
    return &mCacheVal;
}


void servCursor::on_xy_src_attr_changed()
{
    on_xy_pos_changed();
}

void servCursor::on_xy_pos_changed()
{
    xyUpdate();

    setuiChange( MSG_CURSOR_S32XY_XA );
    setuiChange( MSG_CURSOR_S32XY_XB );
    setuiChange( MSG_CURSOR_S32XY_YA );
    setuiChange( MSG_CURSOR_S32XY_YB );

//    setuiChange( cmd_xy_pos_changed );
}

void servCursor::on_xy_hpos_changed()
{
    xyTrackUpdate();

    setuiChange( MSG_CURSOR_S32XY_TRACK_AX );
    setuiChange( MSG_CURSOR_S32XY_TRACK_AX );

    //    setuiChange( cmd_xy_hpos_changed );
}

void servCursor::xyUpdate()
{
    mXy.updateProc();
    xyUiRefresh();
}

void servCursor::xyUiRefresh()
{
    QString str;

    format( &mXy.mAxV, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32XY_XA, str );

    format( &mXy.mBxV, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32XY_XB, str );

    format( &mXy.mAyV, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32XY_YA, str );

    format( &mXy.mByV, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32XY_YB, str );

    //! update completed
    setuiChange( cmd_xy_value_update );
}

void servCursor::xyTrackUpdate()
{
    mUpdateMutex.lock();
    mXy.trackProc();

    xyTrackUiRefresh();
    mUpdateMutex.unlock();
}
void servCursor::xyTrackUiRefresh()
{
    QString str;

    format( &mXy.mtT1, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32XY_XA, str );

    format( &mXy.mtT2, str );
    mUiAttr.setOutStr( MSG_CURSOR_S32XY_XB, str );

    //! track update
    setuiChange( cmd_xy_track_value_update );
}
