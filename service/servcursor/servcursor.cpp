
#include "../../service/servhori/servhori.h"
#include "servcursor_imp.h"
#include "../servtrace/servtrace.h"
#include "../servdso/servdso.h"

using namespace cursor_lib;

servCursor::servCursor( QString name, ServiceId id )
           : serviceExecutor(name,id)
{
    serviceExecutor::baseInit();

    //! do not change setting
    setMsgAttr( (int)servCursor::cmd_auto_x_changed );
    setMsgAttr( (int)servCursor::cmd_auto_y_changed );
    setMsgAttr( (int)servCursor::cmd_auto_x_track );
    setMsgAttr( (int)servCursor::cmd_auto_y_track );
    setMsgAttr( (int)servCursor::cmd_set_cursor_providor );

    appendCacheList(this);
}

DsoErr servCursor::start()
{
    m_pThread = new servCursorThread( this );
    return ERR_NONE;
}

void servCursor::registerSpy()
{
    QList< nameCmdPair *> list;

    nameCmdPair ch1( serv_name_ch1, cmd_src_setting_change );
    nameCmdPair ch2( serv_name_ch2, cmd_src_setting_change );
    nameCmdPair ch3( serv_name_ch3, cmd_src_setting_change );
    nameCmdPair ch4( serv_name_ch4, cmd_src_setting_change );
    nameCmdPair math1( serv_name_math1, cmd_src_setting_change );
    nameCmdPair math2( serv_name_math2, cmd_src_setting_change );
    nameCmdPair math3( serv_name_math3, cmd_src_setting_change );
    nameCmdPair math4( serv_name_math4, cmd_src_setting_change );

    list.append(&ch1);
    list.append(&ch2);
    list.append(&ch3);
    list.append(&ch4);
    list.append(&math1);
    list.append(&math2);
    list.append(&math3);
    list.append(&math4);

    nameCmdPair *pItem;
    for(int i = 0;i < 4;i++)
    {
        pItem = list.at(i);
        spyOn( pItem->first, MSG_CHAN_SCALE,  pItem->second );
        spyOn( pItem->first, MSG_CHAN_OFFSET, pItem->second );
        spyOn( pItem->first, MSG_CHAN_UNIT,   pItem->second );
        spyOn( pItem->first, MSG_CHAN_PROBE,  cmd_src_notrack_setting_change );
        spyOn( pItem->first, MSG_CHAN_ON_OFF, cmd_src_en_change);
    }

    for(int i = 4; i < 8;i++)
    {
        pItem = list.at(i);
        spyOn(pItem->first, MSG_MATH_VIEW_OFFSET,     cmd_src_setting_change);
        spyOn(pItem->first, MSG_MATH_S32FUNCSCALE,    cmd_src_setting_change);
        spyOn(pItem->first, MSG_MATH_S32FFTSCR,       cmd_screen_mode_change);
        spyOn(pItem->first, MSG_MATH_EN,              cmd_src_en_change);
        spyOn(pItem->first, MSG_MATH_S32MATHOPERATOR, cmd_src_notrack_setting_change);
    }

    spyOn( serv_name_la,   MSG_LA_ENABLE,      cmd_src_en_change);
    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON ,   cmd_zoom_en_change);
    spyOn( serv_name_hori, MSG_HOR_TIME_MODE , cmd_mode_en_change);

    spyOn( serv_name_hori, servHori::cmd_main_scale,  cmd_src_setting_change );
    spyOn( serv_name_hori, servHori::cmd_main_offset, cmd_src_setting_change);
    spyOn( serv_name_hori, servHori::cmd_zoom_scale,  cmd_src_setting_change );
    spyOn( serv_name_hori, servHori::cmd_zoom_offset, cmd_src_setting_change);

    serviceExecutor::spyOn( E_SERVICE_ID_TRACE,
                            servTrace::cmd_set_provider,
                            mpItem->servId,
                            servCursor::cmd_set_cursor_providor );

    spyOn( serv_name_utility , MSG_APP_UTILITY_LANGUAGE, cmd_set_item_update);
}

//! serial
int servCursor::serialOut( CStream &stream,
                           serialVersion &ver )
{
    ver = mVersion;
    //! mode
    stream.write(QStringLiteral("mMode"),     (int) mMode);

    stream.write(QStringLiteral("mManualView"),(int) mManual.mView);
    stream.write(QStringLiteral("mManualSrc"), (int) mManual.mSrc);
    stream.write(QStringLiteral("mManualUnitX"),(int) mManual.mUnitX);
    stream.write(QStringLiteral("mManualUnitY"),(int) mManual.mUnitY);

    //! pos
    stream.write(QStringLiteral("mManualmX1"), mManual.mX1);
    stream.write(QStringLiteral("mManualmX2"), mManual.mX2);
    stream.write(QStringLiteral("mManualmY1"), mManual.mY1);
    stream.write(QStringLiteral("mManualmY2"), mManual.mY2);

    stream.write(QStringLiteral("mManualmRefX1"), mManual.mRefX1);
    stream.write(QStringLiteral("mManualmRefX2"), mManual.mRefX2);
    stream.write(QStringLiteral("mManualmRefY1"), mManual.mRefY1);
    stream.write(QStringLiteral("mManualmRefY2"), mManual.mRefY2);

    //! track
    stream.write(QStringLiteral("mTrackmAx"), mTrack.mAx);
    stream.write(QStringLiteral("mTrackmBx"), mTrack.mBx);
    stream.write(QStringLiteral("mTrackmAy"), mTrack.mAy);
    stream.write(QStringLiteral("mTrackmBy"), mTrack.mBy);

    stream.write(QStringLiteral("mTrackMode"),(int)mTrack.mTrackMode);
    stream.write(QStringLiteral("mTrackmSrcA"),(int)mTrack.mTSrcAmgr.mCh);
    stream.write(QStringLiteral("mTrackmSrcB"),(int)mTrack.mTSrcBmgr.mCh);

    //! auto
    //stream.write(QStringLiteral("mAutoItem"), (int)mAuto.mItem);

    //! xy
    stream.write(QStringLiteral("mXySrcX"), (int)mXy.mSrcX);
    stream.write(QStringLiteral("mXySrcY"), (int)mXy.mSrcY);

    stream.write(QStringLiteral("mXymMode"),(int)mXy.mMode);

    stream.write(QStringLiteral("mXymAx"), mXy.mAx);
    stream.write(QStringLiteral("mXymBx"), mXy.mBx);

    stream.write(QStringLiteral("mXymAy"), mXy.mAy);
    stream.write(QStringLiteral("mXymBy"), mXy.mBy);

    stream.write(QStringLiteral("mXymT1"), mXy.mT1);
    stream.write(QStringLiteral("mXymT2"), mXy.mT2);
    return 0;
}

int servCursor::serialIn( CStream &stream,
                          serialVersion ver )
{
    if(ver == mVersion)
    {
        int param;
        stream.read(QStringLiteral("mMode"),         param);
        mMode = (CursorMode)param;

        stream.read(QStringLiteral("mManualView"),   param);
        mManual.setView((CursorView)param);

        stream.read(QStringLiteral("mManualSrc"),    param);
        mManual.setSrc((Chan) param);

        stream.read(QStringLiteral("mManualUnitX"),  param);
        mManual.setUnitX((Unit) param);

        stream.read(QStringLiteral("mManualUnitY"),  param);
        mManual.setUnitY((Unit) param);

        //! pos
        stream.read(QStringLiteral("mManualmX1"),  param);
        mManual.mX1 = param;
        stream.read(QStringLiteral("mManualmX2"),  param);
        mManual.mX2 = param;

        stream.read(QStringLiteral("mManualmY1"),  param);
        mManual.mY1 = param;
        stream.read(QStringLiteral("mManualmY2"),  param);
        mManual.mY2 = param;

        stream.read(QStringLiteral("mManualmRefX1"),  param);
        mManual.mRefX1 = param;
        stream.read(QStringLiteral("mManualmRefX2"),  param);
        mManual.mRefX2 = param;

        stream.read(QStringLiteral("mManualmRefY1"),  param);
        mManual.mRefY1 = param;
        stream.read(QStringLiteral("mManualmRefY2"),  param);
        mManual.mRefY2 = param;

        //! track
        stream.read(QStringLiteral("mTrackmAx"),  param);
        mTrack.mAx = param;
        stream.read(QStringLiteral("mTrackmBx"),  param);
        mTrack.mBx = param;

        stream.read(QStringLiteral("mTrackmAy"),  param);
        mTrack.mAy = param;
        stream.read(QStringLiteral("mTrackmBy"),  param);
        mTrack.mBy = param;

        stream.read(QStringLiteral("mTrackMode"),  param);
        mTrack.mTrackMode = (CursorTrackMode)param;

        stream.read(QStringLiteral("mTrackmSrcA"),  param);
        mTrack.mTSrcAmgr.mCh = (Chan) param;

        stream.read(QStringLiteral("mTrackmSrcB"),  param);
        mTrack.mTSrcBmgr.mCh = (Chan) param;

        //! auto
        //stream.read(QStringLiteral("mAutoItem"),  param);
        //mAuto.mItem = (CursorAutoItem)param;

        //! xy
        stream.read(QStringLiteral("mXySrcX"),  param);
        mXy.mSrcX = (Chan) param;
        stream.read(QStringLiteral("mXySrcY"),  param);
        mXy.mSrcY = (Chan) param;

        stream.read(QStringLiteral("mXymMode"),  param);
        mXy.mMode = (CursorXYMode) param;

        stream.read(QStringLiteral("mXymAx"),  param);
        mXy.mAx = param;
        stream.read(QStringLiteral("mXymBx"),  param);
        mXy.mBx = param;

        stream.read(QStringLiteral("mXymAy"),  param);
        mXy.mAy = param;
        stream.read(QStringLiteral("mXymBy"),  param);
        mXy.mBy = param;

        stream.read(QStringLiteral("mXymT1"),  param);
        mXy.mT1 = param;
        stream.read(QStringLiteral("mXymT2"),  param);
        mXy.mT2 = param;

        //! self
        mViewLisajous = false;

        mFocusItem = 0;
    }
    else
    {
        rst();
    }
    return 0;
}

void servCursor::rst()
{
    mMode = cursor_mode_off;

    //! manual
    mManual.mArea = cursor_area_main;
    mManual.mView = cursor_view_vert;

    mManual.mSrc = chan1;

    mManual.mUnitX = Unit_s;
    mManual.mUnitY = Unit_A;

    //! pos
    mManual.mX1 = 400;
    mManual.mX2 = 600;

    mManual.mY1 = 180;
    mManual.mY2 = 300;

    mManual.mRefX1 = mManual.mX1;
    mManual.mRefX2 = mManual.mX2;

    mManual.mRefY1 = mManual.mY1;
    mManual.mRefY2 = mManual.mY2;

    //! track
    mTrack.mAx = 400;
    mTrack.mBx = 600;

    mTrack.mTrackMode = cursor_track_y;

    mTrack.mAy = 180;
    mTrack.mBy = 300;

    mTrack.mTSrcAmgr.mCh = chan1;
    mTrack.mTSrcBmgr.mCh = chan1;

    //! auto
    //mAuto.mItem = CursorAuto_none;

    //! xy
    mXy.mSrcX = chan1;
    mXy.mSrcY = chan2;

    mXy.mMode = cursor_xy_track;

    mXy.mAx = 100;
    mXy.mBx = 300;

    mXy.mAy = 100;
    mXy.mBy = 300;

    mXy.mT1 = 100;
    mXy.mT2 = 400;

    //! self
    mViewLisajous = false;

    mFocusItem = 0;
    mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_xy, false);
    mUiAttr.setEnable( MSG_CURSOR_PMVRANGE, false );
    mUiAttr.setEnable( MSG_CURSOR_PMHRANGE, false );
    mUiAttr.setEnable( MSG_CURSOR_S23MAREA, false);

    mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_manual, true);
    mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_track, true);
    mUiAttr.setEnable( MSG_CURSOR_S32MTYPE,  true );
    mUiAttr.setEnable( MSG_CURSOR_S32MHAPOS, true);
    mUiAttr.setEnable( MSG_CURSOR_S32MHBPOS, true);
    mUiAttr.setEnable( MSG_CURSOR_S32MHABPOS, true);
    mUiAttr.setEnable( MSG_CURSOR_S32MVAPOS, true);
    mUiAttr.setEnable( MSG_CURSOR_S32MVBPOS, true);
    mUiAttr.setEnable( MSG_CURSOR_S32MVABPOS, true);
    mUiAttr.setEnable( MSG_CURSOR_S32TAPOS,  true);
    mUiAttr.setEnable( MSG_CURSOR_S32TBPOS,  true);
    mUiAttr.setEnable( MSG_CURSOR_S32TABPOS, true);
    mUiAttr.setEnable( MSG_CURSOR_S32TAPOS_V, true);
    mUiAttr.setEnable( MSG_CURSOR_S32TBPOS_V, true);
    mUiAttr.setEnable( MSG_CURSOR_S32TABPOS_V, true);

    mUiAttr.setVisible(MSG_CURSOR_LATYPE,    false );

}

int servCursor::startup()
{
    //! async
    async( MSG_CURSOR_S32CURSORMODE, (int)mMode );
    //! for bug2317：切换模式之后菜单禁用错误
    if(mMode == cursor_mode_manual)
    {
        async( MSG_CURSOR_S32MANUALSRC , mManual.getSrc() );
    }
    else if(mMode == cursor_mode_track)
    {
        async( MSG_CURSOR_S32TASRC, mTrack.getSrcA() );
        async( MSG_CURSOR_S32TBSRC, mTrack.getSrcB() );
    }
    else
    {}

    mUiAttr.setZVal( MSG_CURSOR_S32MVABPOS, cursor_tune_z_val );

    //bug 1911 by zy
#if 1
    mUiAttr.setZVal( MSG_CURSOR_S32XY_XAB, cursor_tune_z_val );
    mUiAttr.setZVal( MSG_CURSOR_S32XY_YAB, cursor_tune_z_val );
#endif
    updateAllUi();

#if 1
//! disable ref src
    for (int i = 0; i < 10; i++ )
    {
        mUiAttr.setVisible( MSG_CURSOR_S32MANUALSRC, r1 + i, false);
        mUiAttr.setVisible( MSG_CURSOR_S32TASRC, r1 + i, false );
        mUiAttr.setVisible( MSG_CURSOR_S32TBSRC, r1 + i, false);
    }

    //bug 1901 by zy
    bool en = sysCheckLicense(OPT_MSO);
    if(!en)
    {
        mUiAttr.setVisible( MSG_CURSOR_S32MANUALSRC, la, false );
    }

//    mUiAttr.setVisible( MSG_CURSOR_S32MANUALSRC, la, false );
//!   math disable
//    for (int i = 0; i < 4; i++)
//    {
//        mUiAttr.setVisible( MSG_CURSOR_S32MANUALSRC, m1 + i, false);
//        mUiAttr.setVisible( MSG_CURSOR_S32TASRC, m1 + i, false );
//        mUiAttr.setVisible( MSG_CURSOR_S32TBSRC, m1 + i, false);
//    }
#endif
    return 0;
}

DsoErr servCursor::setFocus( int msg )
{
    mFocusItem = msg;
    setuiFocus(msg);
    return ERR_NONE;
}

int servCursor::getFocus()
{ return mFocusItem; }

DsoErr servCursor::exitActive()
{
    return serviceExecutor::deActive();
}

DsoErr servCursor::onCursorButton()
{
    if(this->isActive())
    {
        setActive();
        static CursorMode modeBefore = cursor_mode_manual;
        if( mMode == cursor_mode_off )
        {
            if(mUiAttr.getOption(MSG_CURSOR_S32CURSORMODE, modeBefore)->getEnable())
            {
                async(MSG_CURSOR_S32CURSORMODE, modeBefore);
            }
            else
            {
                for(int i = 1;i < 5;i++)
                {
                    if(mUiAttr.getOption(MSG_CURSOR_S32CURSORMODE, i)->getEnable() && i!=3)
                    {
                        async(MSG_CURSOR_S32CURSORMODE, (CursorMode)  i);
                        break;
                    }
                }
            }
        }
        else
        {
            modeBefore = mMode;
            async(MSG_CURSOR_S32CURSORMODE, cursor_mode_off);
        }
    }
    else
    {
        setActive();
    }
    return ERR_NONE;
}

DsoErr servCursor::setCursorDataProvidor( void *ptr )
{
    Q_UNUSED(ptr);
//    mTrack.m_pActiveProvider = (dataProvider*)ptr;
//    Q_ASSERT( NULL != mTrack.m_pActiveProvider );
    return ERR_NONE;
}

//! user
void *servCursor::getAxReal()
{
    mCacheVal = mManual.mAx;
    return &mCacheVal;
}
void *servCursor::getBxReal()
{
    mCacheVal = mManual.mBx;
    return &mCacheVal;
}

void *servCursor::getAyReal()
{
    mCacheVal = mManual.mAy;
    return &mCacheVal;
}
void *servCursor::getByReal()
{
    mCacheVal = mManual.mBy;
    return &mCacheVal;
}

void *servCursor::getDxReal()
{
    mCacheVal = mManual.mDx;

    return &mCacheVal;
}
void *servCursor::getDyReal()
{
    if(getManualSrc() == la)
    {
        mCacheVal.setValue(0, cursor_value_float);
        return &mCacheVal;
    }

    mCacheVal = mManual.mDy;
    return &mCacheVal;
}

void *servCursor::getInvDxReal()
{
    mCacheVal = mManual.mInvDx;
    return &mCacheVal;
}

void *servCursor::getTrackAx()
{
    mCacheVal = mTrack.mAxV;
    return &mCacheVal;
}
void *servCursor::getTrackBx()
{
    mCacheVal = mTrack.mBxV;
    return &mCacheVal;
}
void *servCursor::getTrackAy()
{
    mCacheVal = mTrack.mAyV;
    return &mCacheVal;
}
void *servCursor::getTrackBy()
{
    mCacheVal = mTrack.mByV;
    return &mCacheVal;
}

void *servCursor::getTrackDx()
{
    mCacheVal = mTrack.mDxV;
    return &mCacheVal;
}
void *servCursor::getTrackDy()
{
    mCacheVal = mTrack.mDyV;
    return &mCacheVal;
}
void *servCursor::getTrackInvDx()
{
    mCacheVal = mTrack.mDxInv;
    return &mCacheVal;
}

void *servCursor::getTrackAxTrack()
{
    mCacheVal = mTrack.mAxScr;
    return &mCacheVal;
}
void *servCursor::getTrackBxTrack()
{
    mCacheVal = mTrack.mBxScr;
    return &mCacheVal;
}
void *servCursor::getTrackAyTrack()
{
    mCacheVal = mTrack.mAyScr;
    return &mCacheVal;
}
void *servCursor::getTrackByTrack()
{
    mCacheVal = mTrack.mByScr;
    return &mCacheVal;
}

int servCursor::getTrackAView()
{
    return (int)mTrack.mTSrcAmgr.mDataView;
}

int servCursor::getTrackBView()
{
    return (int)mTrack.mTSrcBmgr.mDataView;
}

//! mode
DsoErr servCursor::setMode( CursorMode mode )
{
    mMode = mode;

    defer(cmd_src_en_change);//! 切换模式时有可能需要禁用菜单
    //! start thread
    if ( mMode == cursor_mode_track
         || mMode == cursor_mode_auto
         || (mMode == cursor_mode_xy && mXy.getXyMode() == cursor_xy_track))
    {
        if ( !m_pThread->isRunning() )
        {
            m_pThread->start();
        }
    }

    if ( mMode == cursor_mode_manual )
    {
        defer( cmd_manual_pos_changed );
        if(mManual.getSrc() == la)
        {
            if ( !m_pThread->isRunning() )
            {
                m_pThread->start();
            }
        }
    }
    else if ( mMode == cursor_mode_track )
    {
        defer( cmd_track_pos_changed );
        defer( cmd_track_delta_attr_changed );
    }
    else if ( mMode == cursor_mode_xy )
    {
        defer( cmd_xy_pos_changed );
    }

    return ERR_NONE;
}
CursorMode servCursor::getMode()
{
    return mMode;
}

void servCursor::on_src_attr_change()
{
    if(mMode == cursor_mode_manual)
    {
        defer( cmd_manual_src_attr_changed );
    }
    else if(mMode == cursor_mode_track)
    {
        //! track模式下通道scale和offset变化时，水平线也跟着变化
        if(mTrack.getTrackMode() == cursor_track_y)
        {
            int AxPos = 0;
            int BxPos = 0;

            static float trackAxReal = mTrack.mAxV.value;
            static float trackBxReal = mTrack.mBxV.value;
            if(mTrack.mxPosMenuChanged)
            {
                trackAxReal = mTrack.mAxV.value;
                trackBxReal = mTrack.mBxV.value;
                mTrack.mxPosMenuChanged = false;
            }

            horiAttr hAttr;
            hAttr = getHoriAttr( mTrack.getSrcA(), mTrack.mTSrcAmgr.mDataView );
            AxPos = round(trackAxReal/hAttr.inc + hAttr.gnd);
            mTrack.setAx( AxPos );

            hAttr = getHoriAttr( mTrack.getSrcB(), mTrack.mTSrcBmgr.mDataView ),
            BxPos = round(trackBxReal/hAttr.inc + hAttr.gnd);
            mTrack.setBx( BxPos );

            //defer( cmd_track_pos_changed);
            setuiChange( MSG_CURSOR_S32TAPOS );
            setuiChange( MSG_CURSOR_S32TBPOS );
        }
        else
        //! track 模式下垂直属性变化
        {
            int AyPos = 0;
            int ByPos = 0;

            static float trackAyReal = mTrack.mAyV.value;
            static float trackByReal = mTrack.mByV.value;
            if(mTrack.myPosMenuChanged)
            {
                trackAyReal = mTrack.mAyV.value;
                trackByReal = mTrack.mByV.value;
                mTrack.myPosMenuChanged = false;
            }

            dsoVert* pVert;
            pVert = dsoVert::getCH(mTrack.getSrcA());    
            float vertBase = ref_volt_base;
            if(mTrack.getSrcA() >= m1 && mTrack.getSrcA() <= m4)
            {
                vertBase = math_v_base;
            }
            else
            {
                vertBase = ref_volt_base;
            }

            if(pVert != NULL)
            {
                float ratio;
                pVert->getProbe().toReal(ratio);
                float base = ratio * vertBase;
                float scale = pVert->getYRefScale()*base;
                float offset = pVert->getYRefOffset()*base;
                AyPos = round(-( trackAyReal + offset)/scale * scr_vdiv_dots + 240);
                mTrack.setAy( AyPos );
            }

            pVert = dsoVert::getCH(mTrack.getSrcB());
            if(mTrack.getSrcB() >= m1 && mTrack.getSrcB() <= m4)
            {
                vertBase = math_v_base;
            }
            else
            {
                vertBase = ref_volt_base;
            }
            if(pVert != NULL)
            {
                float ratio;
                pVert->getProbe().toReal(ratio);
                float base = ratio * vertBase;
                float scale = pVert->getYRefScale()*base;
                float offset = pVert->getYRefOffset()*base;
                ByPos = round(-(trackByReal + offset)/scale * scr_vdiv_dots + 240);
                mTrack.setBy( ByPos );
            }

            //! 因为源属性改变发生的变化不需要更新位置
            setuiChange( MSG_CURSOR_S32TAPOS_V );
            setuiChange( MSG_CURSOR_S32TBPOS_V );
        }
        defer( cmd_track_delta_attr_changed );
    }
    else if(mMode == cursor_mode_xy)
    {
        defer( cmd_xy_src_attr_changed );
    }
}

void servCursor::on_probe_change()
{
    if(mMode == cursor_mode_track)
    {
        defer( cmd_track_pos_changed);
    }
    else
    {
        on_src_attr_change();
    }
}

void servCursor::on_src_en_change()
{
    QString chServName;
    QString mathServName;
    for(int i = 0;i < 4;i++)
    {
        bool chEn = false;
        chServName = QString("chan")+QString::number(i+1);
        serviceExecutor::query(chServName, MSG_CHAN_ON_OFF, chEn);

        bool mathEn = false;
        mathServName = QString("math")+QString::number(i+1);
        serviceExecutor::query(mathServName, MSG_MATH_EN, mathEn);

        if(mMode == cursor_mode_manual)
        {
            if(mManual.getSrc() == (chan1 + i) && !chEn)
            {
                async( MSG_CURSOR_S32MANUALSRC, chan_none );
            }
            if(mManual.getSrc() == (m1 + i) && !mathEn)
            {
                async( MSG_CURSOR_S32MANUALSRC, chan_none );
            }
            mUiAttr.setEnable( MSG_CURSOR_S32MANUALSRC, chan1 + i, chEn);
            mUiAttr.setEnable( MSG_CURSOR_S32MANUALSRC, m1 + i, mathEn);
        }
        else if(mMode == cursor_mode_track)
        {
            if(mTrack.getSrcA() == (chan1 + i) && !chEn)
            {
                async( MSG_CURSOR_S32TASRC, chan_none);
            }
            if(mTrack.getSrcA() == (m1 + i) && !mathEn)
            {
                async( MSG_CURSOR_S32TASRC, chan_none);
            }
            mUiAttr.setEnable( MSG_CURSOR_S32TASRC, chan1+i, chEn);
            mUiAttr.setEnable( MSG_CURSOR_S32TASRC, m1+i, mathEn);

            if(mTrack.getSrcB() == (chan1 + i) && !chEn)
            {
                async( MSG_CURSOR_S32TBSRC ,chan_none);
            }
            if(mTrack.getSrcB() == (m1 + i) && !mathEn)
            {
                async( MSG_CURSOR_S32TBSRC ,chan_none);
            }
            mUiAttr.setEnable( MSG_CURSOR_S32TBSRC, chan1+i, chEn);
            mUiAttr.setEnable( MSG_CURSOR_S32TBSRC, m1+i, mathEn);
        }
    }

    bool laEn = false;
    serviceExecutor::query(serv_name_la, MSG_LA_ENABLE, laEn);
    if(mMode == cursor_mode_manual)
    {
        if(mManual.getSrc() == la && !laEn)
        {
            ssync( MSG_CURSOR_S32MANUALSRC, chan_none );
        }
    }
    mUiAttr.setEnable(MSG_CURSOR_S32MANUALSRC, la, laEn);
}

void servCursor::on_mode_en_change()
{
    int timeMode;
    query(serv_name_hori, MSG_HOR_TIME_MODE, timeMode);
    if(timeMode == Acquire_XY)
    {
        if( mMode == cursor_mode_manual || mMode == cursor_mode_track)
        {
            async( MSG_CURSOR_S32CURSORMODE, cursor_mode_off);
        }
        mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_manual, false);
        mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_track, false);
        mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_xy, true);
    }
    else
    {
        if( mMode == cursor_mode_xy)
        {
            async( MSG_CURSOR_S32CURSORMODE, cursor_mode_off);
        }
        mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_manual, true);
        mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_track, true);
        mUiAttr.setEnable( MSG_CURSOR_S32CURSORMODE, cursor_mode_xy, false);
    }
}

void servCursor::on_zoom_en_change()
{
    bool zoomEn;
    query(serv_name_hori, MSG_HOR_ZOOM_ON, zoomEn);
    if(zoomEn)
    {
        mTrack.mTSrcAmgr.mDataView = horizontal_view_zoom;
        mTrack.mTSrcBmgr.mDataView = horizontal_view_zoom;
        mUiAttr.setEnable( MSG_CURSOR_S23MAREA,true);
    }
    else
    {
        mTrack.mTSrcAmgr.mDataView = horizontal_view_main;
        mTrack.mTSrcBmgr.mDataView = horizontal_view_main;
        async( MSG_CURSOR_S23MAREA, horizontal_view_active);
        mUiAttr.setEnable( MSG_CURSOR_S23MAREA, false);
    }

    //bug 1908 by zy
    horiAttr attr;
    int posA;
    int posB;
    if(zoomEn)
    {
        attr = sysGetHoriAttr(chan1, horizontal_view_main);
//        mTrack.mAxV;
//        float realA = (mTrack.getAx() - attr.gnd) * attr.inc;
//        float realB = (mTrack.getBx() - attr.gnd) * attr.inc;

        attr = sysGetHoriAttr(chan1, horizontal_view_zoom);
        posA = round(mTrack.mAxV.value/attr.inc + attr.gnd);
        posB = round(mTrack.mBxV.value/attr.inc + attr.gnd);
    }
    else
    {
        attr = sysGetHoriAttr(chan1, horizontal_view_zoom);
//        float realA = (mTrack.getAx() - attr.gnd) * attr.inc;
//        float realB = (mTrack.getBx() - attr.gnd) * attr.inc;

        attr = sysGetHoriAttr(chan1, horizontal_view_main);
        posA = round(mTrack.mAxV.value/attr.inc + attr.gnd);
        posB = round(mTrack.mBxV.value/attr.inc + attr.gnd);
    }

    if(mMode == cursor_mode_track) //![dba bug:2073]
    {
        async(MSG_CURSOR_S32TAPOS,posA);
        async(MSG_CURSOR_S32TBPOS,posB);
    }
}

void servCursor::on_screen_mode_change()
{
    int mode;
    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, mode);
    if(mode == (int) screen_yt_main_zfft)
    {
        if(getManualArea() == horizontal_view_zoom)
        {
            ssync(MSG_CURSOR_S23MAREA, horizontal_view_main);
            mUiAttr.setEnable(MSG_CURSOR_S23MAREA, false);
        }
    }
    else if(mode == (int) screen_yt_main)
    {
        mUiAttr.setEnable(MSG_CURSOR_S23MAREA, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_CURSOR_S23MAREA, true);
    }
}

scpiError CursorFormater::format(GCVal &val)
{
    if ( val.vType != val_ptr )
    {
        return scpi_err_val_type_mismatch;
    }

    cursor_lib::CCursorVal *pval = static_cast<cursor_lib::CCursorVal*>(val.pVal);

    if( pval->getType() == cursor_value_float )
    {
        QString str;
        servCursor::format( pval, str );
        if(str.contains("***"))
        {
            mOutStr = QString("%1").arg(str).toLatin1();
        }
        else
        {
            pval->value += pval->value / 1e6;
            mOutStr = QString("%1").arg(pval->value, 0, 'E').toLatin1();
        }
    }
    else if( pval->getType() == cursor_value_la_lh )
    {
        QString str16;
        QString str2h;
        QString str2l;
        servCursor::format( pval, str16, str2h, str2l);
        mOutStr = QString("%1").arg(str16).toLatin1();
    }

    return scpi_err_none;
}

int servCursor::getScpiTrackCay()
{
    return mTrack.mAy;
}

int servCursor::getScpiTrackCby()
{
    return mTrack.mBy;
}
