#include <cmath>
#include <QTime>
#include <fstream>
#include "serveyejit.h"
#include "ceyecr.h"
#include "../../baseclass/dsovert.h"
namespace eyeLib {

CEyeCR::CEyeCR()
{
    m_AcqLength = 0;

    m_crPara.m_CRType = PLL_CR;
    m_crPara.m_DataFreq = pow(10,6);
    m_crPara.m_PllOrder = ONE_ORDER;
    m_crPara.m_PllLoopWidth = m_crPara.m_DataFreq/1000;
    m_crPara.m_DampFactor = 0.707;

    m_crPara.highThreshold = 70;
    m_crPara.midThreshold = 50;
    m_crPara.lowThreshold = 30;

    m_crResult.eyeNumDisp = 1;
}

void CEyeCR::init(DsoWfm* pWfmA, CRPara crPara)
{
    //! \brief init the clock recovery para
    //! 这边的数据相当于添加了一个缓冲，菜单上设置的变化不会立即导致
    //! 运算过程当中使用的参数的变化，以防止导致运算出错。
    m_crPara = crPara;

    m_AcqLength = pWfmA->size();
    m_AcqRate = 1e12/pWfmA->getlltInc();

    DsoPoint* data = pWfmA->getPoint();
    //! 这边的max和min用于设定测量的阈值用,to do:从measure的top和base获取
    uchar max = 0;
    uchar min = 255;

    int i = 0;
    while(i < m_AcqLength)
    {
        if (data[i]>max)
        {
            max = data[i];
        }
        if (data[i]<min)
        {
            min = data[i];
        }
        i++;
    }

    m_EyeVolMax = max;
    m_EyeVolMin = min;

    uchar highThres = (m_crPara.highThreshold*max + (100 - m_crPara.highThreshold)*min)/100;
    uchar midThres = (m_crPara.midThreshold*max + (100 - m_crPara.midThreshold)*min)/100;
    uchar lowThres = (m_crPara.lowThreshold*max + (100 - m_crPara.lowThreshold)*min)/100;

    m_EdgeRes.setThres(highThres, midThres, lowThres);
    data = NULL;
}

void CEyeCR::setAcqDep(servEyeJit *serv)
{
    qlonglong bitRate = serv->getDataFreq();
    int maxSaRate = 2.0e9;
    int sampsPerSym = maxSaRate / bitRate;
    AcquireDepth acqDep;

    if(sampsPerSym > 500)
    {
        acqDep = Acquire_Depth_1M;
    }
    else if(sampsPerSym > 50)
    {
        acqDep = Acquire_Depth_100K;
    }
    else if(sampsPerSym > 8)
    {
        acqDep = Acquire_Depth_10K;
    }
    else
    {
        Q_ASSERT(false);
    }

    //! 将存储深度设置为上述求出来的存储深度,ensure the config take effects
    serviceExecutor::post(serv_name_hori, MSG_HOR_ACQ_MEM_DEPTH, acqDep);
    int qAcqDepth = 0;
    while(qAcqDepth != acqDep)
    {
        QThread::msleep(20);
        serviceExecutor::query(serv_name_hori, MSG_HOR_ACQ_MEM_DEPTH, qAcqDepth);
    }
}

void CEyeCR::CR(DsoWfm *pWfmA,DsoWfm *pWfmB)
{
    m_EdgeRes.findEdge(pWfmA->getPoint(),m_AcqLength);

    m_crResult.index.clear();
    m_crResult.offset.clear();
    m_PhaseErrorRecord.clear();
    m_PhaseErrIndex.clear();

    m_crResult.multiple = 1;
    m_crResult.index.reserve(getSymbolNum()*1.1);
    m_crResult.offset.reserve(getSymbolNum()*1.1);
    //! offset
    m_PhaseErrorRecord.reserve(getSymbolNum()*1.1);
    m_PhaseErrIndex.reserve(getSymbolNum());

    if(m_crPara.m_CRType == CONST_CR)
    {
        //! find rate automatically
        if(m_crPara.freqType == AUTO_FREQ)
        {
            int sampsSym = findLeast(m_EdgeRes.edge, pWfmA->size());
            m_crPara.m_DataFreq = m_AcqRate/sampsSym;
        }
        LOG_DBG();
        constCR(pWfmA);
    }
    else if(m_crPara.m_CRType == PLL_CR)
    {
        LOG_DBG();
        pllCR(pWfmA);
    }
    else
    {
        extCR(pWfmA, pWfmB);
    }
}

//! to do
void CEyeCR::pllCR(DsoWfm* pWfmA)
{
    m_crResult.multiple = round(wave_width/(sampsPerSym()*(m_crResult.eyeNumDisp+0.5)));
    if(m_crResult.multiple < 1)
    {
        m_crResult.multiple = 1;
    }

    DsoPoint* data = pWfmA->getPoint();
    QVector<int>& dataEdge = m_EdgeRes.edge;

    QTime pllTime;
    pllTime.start();

    int Kpd = 1;
    int Kvco = 1;
    float clockFreq = m_crPara.m_DataFreq*2*M_PI;
    float loop_filter = clockFreq/Kvco;
    float tau1 = Kpd*Kvco/(std::pow(2*M_PI*m_crPara.m_PllLoopWidth,2));
    float tau2 = m_crPara.m_DampFactor*2*pow(tau1/(Kpd*Kvco),0.5);

    //! PLL loop
    float clockPhase = 0;
    float phaseError = 0;
    float edgePhase = 0;
    EyeVolType left = 0;
    EyeVolType right = 0;
    int clockNum = 0;

    float T = pWfmA->getlltInc()/1e12f;

    int k_Edge = 0;
    //!防止在dataEdge数组中最后一个数访问越界(原来的dataEdge中的最后一个数小于acqLenth)
    dataEdge.push_back(m_AcqLength+2);
    uchar midThres = m_EdgeRes.getMidThres();
    for(int i = 0;i < m_AcqLength;i++)
    {
        clockPhase = clockPhase+T*clockFreq;

        if(clockPhase >= 2*M_PI)
        {
            clockPhase = clockPhase - 2*M_PI;
            m_crResult.index.push_back(i);
            m_crResult.offset.push_back(0);
            if(m_crResult.multiple > 5)
            {
                //! offset positive:backwards
                m_crResult.offset[clockNum] = m_crResult.multiple*(clockPhase)/(T*clockFreq);
            }

            if( m_PhaseErrorRecord.size() <= clockNum)
            {
                m_PhaseErrorRecord.push_back(0);
            }
            clockNum = clockNum+1;
        }

        if( i == dataEdge[k_Edge] ) //label the Edge
        {
            k_Edge++;
            left = midThres - data[i-1];
            right = data[i] - midThres;
            if(left > 0.00000001 )
            {
                edgePhase = clockPhase + T * clockFreq * (left / (left+right));
            }
            else
            {
                edgePhase = clockPhase;
            }
            phaseError = Kpd * (edgePhase-M_PI);
            LOG_DBG()<<phaseError;
            if( m_PhaseErrorRecord.size() < clockNum || clockNum == 0)
            {
                m_PhaseErrorRecord.push_back(phaseError/clockFreq);
            }
            else
            {
                m_PhaseErrorRecord[clockNum - 1] = phaseError/clockFreq;
            }
            if(clockNum > 0)
            {
                m_PhaseErrIndex.push_back(clockNum - 1);
            }
        }

        if(m_crPara.m_PllOrder == TWO_ORDER)
        {
            loop_filter = loop_filter - phaseError*T/tau1;
            clockFreq = Kvco*(loop_filter - phaseError*tau2/tau1);
        }
        else if(m_crPara.m_PllOrder == ONE_ORDER)
        {
            clockFreq = Kvco*(loop_filter - phaseError*m_crPara.m_PllLoopWidth*2*M_PI);
        }
    }

    LOG_DBG()<<"pllTime"<<pllTime.elapsed()/1000.0<<"s";
//    if(m_PhaseErrIndex.size() < m_PhaseErrIndex.capacity())
//    {
//        QVector<int> temp(m_PhaseErrIndex);
//        temp.swap(m_PhaseErrIndex);
//    }
}

void CEyeCR::constCR(DsoWfm* /*pWfmA*/)
{
    QVector<int>& edge = m_EdgeRes.edge;

    float sampsPerSymbol = sampsPerSym();
    QVector<int> symbolAtEdge = samptoSymbolAtEdge(edge,sampsPerSymbol);
    QVector<double> ab = leastSquare(symbolAtEdge,edge);
    sampsPerSymbol = ab[0];
    m_crPara.m_DataFreq = (qlonglong) m_AcqRate/sampsPerSymbol;

    QVector<int> clock_temp;
    clock_temp.reserve(ceil(m_AcqLength/sampsPerSymbol)+5);

    m_PhaseErrIndex.resize(ceil(m_AcqLength/sampsPerSymbol)+5);
    m_PhaseErrorRecord.resize(ceil(m_AcqLength/sampsPerSymbol)+5);
    int Edge_k = 0;
    int symbolSeq = symbolAtEdge[Edge_k];

    float clockInSamp = edge[Edge_k];

    while(clockInSamp < m_AcqLength)
    {
        if( Edge_k < symbolAtEdge.size() && symbolSeq == symbolAtEdge[Edge_k])
        {
            m_PhaseErrorRecord[symbolSeq] = clockInSamp - edge[Edge_k];
            m_PhaseErrIndex[Edge_k] = symbolSeq;
            Edge_k++;
        }
        symbolSeq++;
        clock_temp.push_back(round(clockInSamp) + sampsPerSymbol/2);
        clockInSamp += sampsPerSymbol;
    }

    //! prevent the over
    m_crResult.index = clock_temp;
    m_crResult.multiple = 1;
    m_crResult.offset.resize(clock_temp.size());
    LOG_DBG();
}

void CEyeCR::extCR(DsoWfm *pWfmA, DsoWfm *pWfmB)
{
    m_EdgeRes.findEdge(pWfmA->getPoint(),pWfmA->size());

    EdgeAttr clockEdgeB;
    clockEdgeB.findEdge(pWfmB->getPoint(), m_AcqLength, m_crPara.highThreshold,
                        m_crPara.midThreshold, m_crPara.lowThreshold);

    int sampsSym = findLeast(m_EdgeRes.edge, pWfmB->size());
    m_crPara.m_DataFreq = m_AcqRate/sampsSym;

    for(int i = 0;i < clockEdgeB.edge.size();i++)
    {
        clockEdgeB.edge[i] = clockEdgeB.edge[i] + sampsSym/2;
    }

    m_crResult.index = clockEdgeB.edge;
    m_crResult.multiple = 1;
    m_crResult.offset.resize( m_crResult.index.size() );
    m_crResult.eyeNumDisp = 1;
    //! 判断是否需要计算抖动
    bool isJitter = true;
    if(isJitter)
    {
        int sizeA = m_EdgeRes.edge.size();
        int sizeB = clockEdgeB.edge.size();
        int length = sizeA < sizeB ? sizeA : sizeB;

        for(int i = 0;i < length;i++)
        {
        }
    }
}

int CEyeCR::findLeast(QVector<int> edge, int sampSize)
{
    int sampSym = 99999;
    QVector<int> edgeToEdge(edge.size() - 1);
    for(int i = 1; i < edge.size();i++)
    {
        edgeToEdge[i-1] = edge[i] - edge[i-1];
    }

    QVector<int> histo(sampSize/500);
    for(int i = 0; i < edgeToEdge.size();i++)
    {
        histo[edgeToEdge[i]/500]++;
    }

    int max = 0;
    int maxIndex = 0;
    for(int i = 0; i < histo.size();i++)
    {
        if(max < histo[i])
        {
            max = histo[i];
            maxIndex = i;
        }
    }

    int step = maxIndex * 500/25;
    step = step > 2 ? step:2;
    histo.clear();;
    histo.resize(500 / step + 1);

    int begin = maxIndex * 500;
    for(int i = 0;i < edgeToEdge.size();i++)
    {
        if(edgeToEdge[i] > begin && edgeToEdge[i] < begin + 500)
        {
            int tempIndex = (edgeToEdge[i] - begin)/step;
            histo[tempIndex] ++;
        }
    }

    max = 0;
    for(int i = 0; i < histo.size();i++)
    {
        if(max < histo[i])
        {
            max = histo[i];
            maxIndex = i;
        }
    }
    sampSym = (maxIndex+maxIndex+1)*step/2 + begin;
    return sampSym;
}

int CEyeCR::findLeast(QVector<int> edge)
{
    int sampsSym = 999999;
    for(int i = 1; i < edge.size() - 1;i++)
    {
        if((edge[i] - edge[i-1]) < sampsSym)
        {
            sampsSym = edge[i] - edge[i-1];
        }
    }
    return sampsSym;
}

QVector<int> CEyeCR::samptoSymbolAtEdge(const QVector<int> &sampsAtEdge,double sampsPeriod)
{
    QVector<int> symbolsAtEdge;
    symbolsAtEdge.reserve(sampsAtEdge.size());
    int Edge = 0;
    double temp = sampsAtEdge[0]/sampsPeriod;
    if(temp < 1)
    {
        Edge = Edge+1;
        symbolsAtEdge.push_back(Edge);
    }
    else
    {
        Edge = Edge + round(temp);
        symbolsAtEdge.push_back(Edge);
    }

    for(int i = 1;i<sampsAtEdge.size();i++)
    {
        temp = (sampsAtEdge[i] - sampsAtEdge[i-1])/sampsPeriod;
        if(temp < 1)
        {
            Edge = Edge+1;
            symbolsAtEdge.push_back(Edge);
        }
        else
        {
            Edge = Edge + round(temp);
            symbolsAtEdge.push_back(Edge);
        }
    }
    return symbolsAtEdge;
}

int CEyeCR::getSymbolNum()
{
    return ceil(m_AcqLength*m_crPara.m_DataFreq/m_AcqRate);
}

double CEyeCR::sampsPerSym()
{
    return (m_AcqRate/m_crPara.m_DataFreq);
}

EyeVolType CEyeCR::getVolMin()
{
    return m_EyeVolMax;
}

EyeVolType CEyeCR::getVolMax()
{
    return m_EyeVolMin;
}

const QVector<int> &CEyeCR::getEdge()
{
    return m_EdgeRes.edge;
}

bool CEyeCR::getFirstEdgeUp()
{
    return m_EdgeRes.isFirstUp;
}

QVector<float> &CEyeCR::getPhaseError()
{
    return m_PhaseErrorRecord;
}

const QVector<int> &CEyeCR::getPhaseErrIndex()
{
    return m_PhaseErrIndex;
}

}
