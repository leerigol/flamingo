/*! to do : acqRate是手动设置的，后续需要从其他服务中获取
 */
#include "ceyemeasure.h"
#include "ceyemap.h"


CEyeMeasure::CEyeMeasure()
{
    m_EyeMeasRes.resize(EYE_END - EYE_BEGIN);
    m_EyeMeasRes[0] = 0.000318;//!test
    m_measDataMap = NULL;
    m_acqRateTemp = 5*pow(10,9);
    m_StatisPercent = 0.2;
}

void CEyeMeasure::measEyePara(CEyeMap *eyeMap)
{
    m_measDataMap = eyeMap->getEyeMap();

    //! 获取相关参数
    int yGnd = eyeMap->m_yGnd;
    float yInc = eyeMap->m_yInc;
    float tInc = eyeMap->m_tInc;

    int col = eyeMap->m_ColMap;
    int leftStart = (0.5 - (m_StatisPercent/2)) * col;
    int hMeasLength =  m_StatisPercent * col;

    //! 进行计算
    QVector<int> VerHisto = histoVGram(leftStart,hMeasLength);//作垂直那一条线上直方图
    int oneInMap = findMaxIndex(VerHisto,VerHisto.size()/2,VerHisto.size());
    int zeroInMap = findMaxIndex(VerHisto,0,VerHisto.size()/2);

    double oneDeviationMap = stDeviation_weight(VerHisto,
                                               VerHisto.size()/2,floor(VerHisto.size()/2));
    double zeroDeviationMap = stDeviation_weight(VerHisto,0,floor(VerHisto.size()/2));

    /*! 根据出现频率最高求出交叉点坐标
    int belowStart = 0.3*oneInMap + 0.7*zeroInMap;
    int heightLength = 0.4*(oneInMap - zeroInMap);
    crossPoint<int> crossLeftMap = {0,0};
    crossPoint<int> crossRightMap = {0,0};
    findCross(belowStart,heightLength,0,col/2,crossLeftMap);
    findCross(belowStart,heightLength,col/2,col,crossRightMap);
    */

    //! 根据交叉点的粗细判断交叉点位置
    enum {Y_row = 0,X_col = 1};
    QVector<int> crossLeftMap = findCross(oneInMap,zeroInMap,0,col/2,m_measDataMap);
    QVector<int> crossRightMap = findCross(oneInMap,zeroInMap,col/2,col-1,m_measDataMap);

    //! 算出交叉点处那水平一条线上的点的标准差
    QVector<int> horiLeft;
    QVector<int> horiRight;
    horiLeft.reserve(col/2+1);
    horiRight.reserve(col/2+1);
    for (int j=0;j<col/2;j++)
    {
        horiLeft.push_back(m_measDataMap->at(crossLeftMap[Y_row]).at(j));
        horiRight.push_back(m_measDataMap->at(crossLeftMap[Y_row]).at(j+col/2));
    }

    double leftHorDiviationMap = stDeviation_weight(horiLeft,0,horiLeft.size());
    double rightHorDeviationMap = stDeviation_weight(horiRight,0,horiRight.size());

    double eyeHeightMap = oneInMap - 3*oneDeviationMap - zeroInMap - 3*zeroDeviationMap;
    double eyeWidthMap = crossRightMap[X_col] - 3*rightHorDeviationMap
                         - crossLeftMap[X_col] - 3*leftHorDiviationMap;

    double eyeAmpMap = oneInMap - zeroInMap;

    //!一电平和零电平是绝对值，眼高和眼宽是相对值，所以Gnd处理不一样
    m_EyeMeasRes[EYE_ONE] = (oneInMap - yGnd)*yInc;
    m_EyeMeasRes[EYE_ZERO] = (zeroInMap - yGnd)*yInc;
    m_EyeMeasRes[EYE_HEIGHT] = eyeHeightMap*yInc;
    m_EyeMeasRes[EYE_AMP] = eyeAmpMap*yInc;

    m_EyeMeasRes[EYE_WIDTH] = eyeWidthMap*tInc;
    m_EyeMeasRes[EYE_QFACTOR] = (oneInMap - zeroInMap)/(oneDeviationMap+zeroDeviationMap);
    m_EyeMeasRes[EYE_CROSSPER] = 100*(crossLeftMap[Y_row] - zeroInMap)/(oneInMap - zeroInMap);
    //m_EyeMeasRes[EYE_BER] = std::exp(-pow(QFactor,2)/2)/(QFactor*pow(2*M_PI,0.5));
}

QVector<int> CEyeMeasure::histoVGram(int start,int length)
{
    QVector<int> Vstatis;
    int row = m_measDataMap->size();
    Vstatis.resize(row);
    for(int i = 0;i < row;i++)
    {
        for (int j = start;j < start+length;j++)
        {
            Vstatis[i] += m_measDataMap->at(i).at(j);
        }
    }
    return Vstatis;
}

QVector<int> CEyeMeasure::findCross(int oneInMap,int zeroInMap,int left,int right,QVector<QVector<int> >* map)
{
    //!记下每一行的宽度，宽度最窄的一行即为交叉点所在行号
    int crossWidth = (right - left);
    int crossRow = 0;
    int start = left;
    int end = right;
    //!找到垂直坐标
    for(int i = zeroInMap;i <= oneInMap;i++)
    {
        for(int j = left;j < right;j++)
        {
            if(map->at(i).at(j)>0)
            {
                start = j;
                break;
            }
        }
        for(int j = right;j > left;j--)
        {
            if(map->at(i).at(j)>0)
            {
                end = j;
                break;
            }
        }

        if((end - start) < crossWidth)
        {
            crossWidth = end - start;
            crossRow = i;
        }

        start = left;
        end = right;
    }
    //!找到水平坐标
    long long sum = 0;
    int num = 0;
    for(int j = left;j <= right;j++)
    {
        if(map->at(crossRow).at(j)>0)
        {
            sum += (map->at(crossRow).at(j)*j);
            num += map->at(crossRow).at(j);
        }
    }
    int crossCol = round(sum/num);
    QVector<int> crossCoord;
    crossCoord<<crossRow<<crossCol;
    return crossCoord;
}

QVector<double> *CEyeMeasure::getMeasRes()
{
    return &m_EyeMeasRes;
}

int CEyeMeasure::findMaxIndex(QVector<int> statis,int start,int size)
{
    int maxIndex = 0;
    int maxValue = 0;
    for(int i = start;i < size;i++)
    {
        if(maxValue < statis[i])
        {
            maxValue = statis[i];
            maxIndex = i;
        }
    }
    return maxIndex;
}

double CEyeMeasure::stDeviation_weight(QVector<int> vertical,int start ,int size)
{   //calculate the standard_deviation with the subscripts of an array
    //and the weight is the array value
    int num = 0;
    int sum = 0;
    double deviation = 0.0;
    for (int i = 0;i < size;i++)
    {
        if(vertical[start+i] != 0)
        {
            num += vertical[start+i];
            sum += (vertical[start+i]*i);
        }
    }
    double average = sum*1.0/num;

    for (int i = 0;i < size;i++)
    {
        if(vertical[start+i] != 0)
        {
            deviation += vertical[start+i]*pow(i-average,2);
        }
    }
    deviation = deviation / num;
    return(sqrt(deviation));
}
