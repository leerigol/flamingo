#include <QTime>
#include <cmath>
#include <QDebug>
#include "ceyemap.h"

CEyeMap::CEyeMap()
{
    m_pEyeCR = new CEyeCR;
    m_RowMap = ADC_DOTs;
    m_ColMap = wave_width;
    m_eyeMap.resize(m_RowMap);
    for(int i = 0;i < m_RowMap;i++)
    {
        m_eyeMap[i].resize(m_ColMap);
    }

    m_eyeStatisScr.resize(wave_height);
    for(int i = 0;i < wave_height;i++)
    {
        m_eyeStatisScr[i].resize(m_ColMap);
    }
    m_curveNum = 7;
}

void CEyeMap::clockRecovery(DsoWfm& wfmA, CRPara crPara, dsoVert* pVertA)
{
    m_pEyeCR->init(&wfmA,crPara);
    {
        //! to do::m_pServEyeJit->m_pEyeCR->setAcqDep();
        pVertA->getEye(wfmA);
    }
    static dsoVert* pVertB = NULL;
    DsoWfm wfmB;
    if(crPara.m_CRType == PARA_CR)
    {
        pVertB  = dsoVert::getCH(crPara.clockChan);
        pVertB->getEye(wfmB);
        m_pEyeCR->CR(&wfmA, &wfmB);
    }
    else
    {
        m_pEyeCR->CR(&wfmA, &wfmB);
    }

}

void CEyeMap::splitter1(CRResult* pCRRes,double sampsPerSym,DsoWfm* pwfmA)
{
    LOG_DBG();
    QTime toMapTime;
    toMapTime.start();
    m_ColMap = ceil(pCRRes->multiple*sampsPerSym*(pCRRes->eyeNumDisp+0.5));

    m_eyeMap.resize(m_RowMap);
    for(int i = 0;i < m_RowMap;i++)
    {
        m_eyeMap[i].resize(m_ColMap);
    }

    int sampsDelay = pwfmA->size()/5;
    const QVector<int>& clock = pCRRes->index;
    DsoPoint* data = pwfmA->getPoint();

    int clockSeq = 0;
    while(clock[clockSeq] < sampsDelay)
    {
        clockSeq++;
    }

    int clock_edge = *(clock.end()-2);

    const int col = m_ColMap;
    const int row = m_RowMap;
    int **eye = NULL;
    eye = new int*[row];
    for(int i = 0;i < row;i++)
    {
        eye[i] = new int[col];
        memset(eye[i],0,col*sizeof(int));
    }

    while(clock[clockSeq+1] < clock_edge)
    {
        int hstep = pCRRes->multiple;
        int offset = pCRRes->offset[clockSeq];

        int beginIndex = clock[clockSeq] - floor((offset + m_ColMap/2-1)/hstep);
        int endIndex = beginIndex + floor((m_ColMap - offset)/hstep)-1;

        if(pCRRes->multiple == 1 )
        {
            insert(beginIndex,endIndex,data,offset,hstep,eye);
        }
        else
        {
            insert(beginIndex,endIndex,data,offset,hstep,eye);
        }

        clockSeq++;
    }

    for(int i = 0;i < m_RowMap;i++)
    {
        for(int j = 0;j < m_ColMap;j++)
        {
            m_eyeMap[i][j] += eye[i][j];
        }
    }
    for(int i = 0;i < m_RowMap;i++)
    {
        delete [] eye[i];
    }
    delete []eye;

//    //! 算出eyeMap的increase
//    if(pCRRes->multiple > 0)
//    {
//        m_tInc = pwfmA->gettInc().toDouble()/pCRRes->multiple;
//    }
//    else
//    {
//        m_tInc = pwfmA->gettInc().toDouble()/(-pCRRes->multiple);
//    }
    m_yInc = pwfmA->realyInc();
    m_yGnd = pwfmA->getyGnd();

    LOG_DBG()<<"toMapTime"<<toMapTime.elapsed()/1000.0<<"s";
    scaletoScr(pwfmA);
    setCurveScreen();

    m_tIncScr = pwfmA->gettInc().toDouble() * ceil(sampsPerSym*(pCRRes->eyeNumDisp+0.5)) / 1000;
}

void CEyeMap::insert(int beginIndex,int endIndex,DsoPoint* data,int offset,int hstep,int** eye)
{
    for(int i = 0;i < offset;i++)
    {
        eye[data[beginIndex]][i]++;
    }

    int k = offset;
    for(int i = beginIndex;i < endIndex;i++)
    {
        int vleft = data[i];
        int vright = data[i+1];
        float f32vcurrent = vleft;
        int   s32vcurrent = vleft;
        int vstep = vright - vleft;
        if(abs(vstep) > abs(hstep))
        {
            float f32hcurrent = k;
            int   s32hcurrent = k;
            //!竖直方向为基准进行插值
            float inc = abs(hstep*1.0/vstep);
            if(vleft <= vright)
            {
                for(int j = vleft;j < vright;j++)
                {
                    Q_ASSERT(s32hcurrent < m_ColMap);
                    //!int k强制转化为 float hcurrent，再强制转化为int ，会不会发生舍入错误
                    eye[j][s32hcurrent]++;
                    f32hcurrent = f32hcurrent + inc;
                    s32hcurrent = (int) f32hcurrent;
                }
            }
            else
            {
                for(int j = vleft;j > vright;j--)
                {
                    Q_ASSERT(s32hcurrent < m_ColMap);
                    //!int k强制转化为 float hcurrent，再强制转化为int ，会不会发生舍入错误
                    eye[j][s32hcurrent]++;
                    f32hcurrent = f32hcurrent + inc;
                    s32hcurrent = (int) f32hcurrent;
                }
            }

            k = k + hstep;
        }
        else
        {
            //!水平方向为基准进行插值
            float inc = vstep*1.0/hstep;
            for(int j = k;j < k+hstep;j++)
            {
                Q_ASSERT(j < m_ColMap);
                eye[s32vcurrent][j]++;
                f32vcurrent = f32vcurrent + inc;
                s32vcurrent = (int) f32vcurrent;
            }
            k = k + hstep;
        }
    }
}

void CEyeMap::noInsert(int beginIndex, int endIndex, unsigned char *data, int **eye)
{
    for(int i = beginIndex; i < endIndex;i++)
    {
        eye[data[i]][endIndex - beginIndex]++;
    }
}

void CEyeMap::setCurveScreen()
{
    QTime setMapTime;
    setMapTime.start();
    m_curveScreen.clear();

    LOG_DBG();
    int maxCount = 0;
    int secondMax = 0;
    for(int i = 0;i < wave_height;i++)
    {
        for(int j = 0;j < wave_width;j++)
        {
            if( maxCount < m_eyeStatisScr.at(i).at(j) )
            {
                secondMax = maxCount;
                maxCount = m_eyeStatisScr.at(i).at(j);
            }
        }
    }

    for (int i = 0; i < m_curveNum;i++)
    {
        m_curveScreen[i].reserve(wave_height*wave_width/10);
    }

    LOG_DBG();
    QVector<int> Edge;
    Edge.resize(m_curveNum+1);  // need "+1"
    Edge[m_curveNum] = secondMax;
    for (int i = m_curveNum - 1; i > 0; i--)  //Edge value from big to small
    {
        Edge[i] = round(Edge[i+1]/2);
    }
    Edge[0] = 1;

    LOG_DBG();
    for(int i = 0;i < wave_height;i++)
    {
        for(int j = 0;j < wave_width;j++)
        {
            for(int k = 0;k < m_curveNum;k++)
            {
                if(m_eyeStatisScr.at(i).at(j) > Edge[k]&&m_eyeStatisScr.at(i).at(j) <= Edge[k+1])
                {
                    //!注意QPointF中i和j的顺序，行值i对应的是纵坐标
                    m_curveScreen[k].push_back(QPoint(j,i));
                    continue;
                }
            }
        }
    }
    qDebug()<<"setMapTime"<<setMapTime.elapsed()/1000.0<<"s";
}

void CEyeMap::scaletoScr(DsoWfm* pWfm)
{
    LOG_DBG();
    float base;
    pWfm->getyRefBase().toReal(base);
    float scale = pWfm->getyRefScale()*base;
    float offset = pWfm->getyRefOffset()*base;

    int scrY[ADC_DOTs];
    for(int i = 0; i < ADC_DOTs;i++)
    {
        float yReal = (i - pWfm->getyGnd()) * pWfm->realyInc();
        scrY[i] = -(yReal + offset)/scale * scr_vdiv_dots + 240;
        if(scrY[i] >= wave_height)
        {
            scrY[i] = wave_height - 1;
//            qDebug()<<__FILE__<<__FUNCTION__<<__LINE__<<"overflow";
        }
        if(scrY[i] < 0)
        {
            scrY[i] = 0;
//            qDebug()<<__FILE__<<__FUNCTION__<<__LINE__<<"overflow";
        }
    }

    LOG_DBG();
    for(int i = 255; i >= 1;i--)
    {
        Q_ASSERT(scrY[i] < 480 && scrY[i] >= 0);
        insert(m_eyeMap[i], m_eyeStatisScr[scrY[i]]);
        for(int j = scrY[i]+1; j < scrY[i-1];j++)
        {
            QVector<int> temp(m_ColMap);
            for(int index = 0; index < m_ColMap;index++)
            {
                temp[index] = (m_eyeMap[i][index] + m_eyeMap[i-1][index])/2;
            }

            insert(temp, m_eyeStatisScr[j]);
        }
    }
    LOG_DBG();
}

QVector<QVector<int> >* CEyeMap::getEyeMap()
{
    return &m_eyeMap;
}

QVector<QVector<int> > *CEyeMap::getEyeStatisScr()
{
    return &m_eyeStatisScr;
}

QMap<int,QVector<QPoint> >* CEyeMap::getEyeCurveScreen()
{
    return &m_curveScreen;
}

void CEyeMap::insert(QVector<int> &befor, QVector<int> &after)
{
    float inc = m_ColMap*1.0/wave_width;
    for(int i = 0; i < wave_width;i++)
    {
        after[i] = befor[int (i * inc)];
    }
}
