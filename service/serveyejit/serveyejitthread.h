#ifndef SERVEYEJITTHREAD_H
#define SERVEYEJITTHREAD_H

#include <QtCore>

class servEyeJit;

class servEyeJitThread : public QThread
{
public:
    servEyeJitThread(servEyeJit *pEyeJit);

public:
    void run();

private:
    int m_loopCR;
    int m_loopEyeMap;
    int m_loopMask;

    servEyeJit *m_pServEyeJit;
};


class servJitterThread : public QThread
{
public:
    servJitterThread(servEyeJit *pJitter);
    void run();
private:
    servEyeJit *m_pServJitter;
};
#endif // SERVEYEJITTHREAD_H
