#ifndef CEYEMEASURE_H
#define CEYEMEASURE_H

#include "../../include/dsotype.h"

using namespace DsoType;

class CEyeMap;

class CEyeMeasure
{
private:
    QVector<double> m_EyeMeasRes;
    QVector<QVector<int> >* m_measDataMap;

    long long m_acqRateTemp;

    float m_StatisPercent;
public:
    CEyeMeasure();
    void measEyePara(CEyeMap* eyeMap);

    QVector<int> histoVGram(int start,int length);
    int findMaxIndex(QVector<int> statis,int start,int size);
    QVector<int> findCross(int belowStart,int VLength,
                int leftStart,int HLength,QVector<QVector<int> > * data);
    double stDeviation_weight(QVector<int> vertical,int start ,int size);

    QVector<double>* getMeasRes();
};

#endif // CEYEMEASURE_H
