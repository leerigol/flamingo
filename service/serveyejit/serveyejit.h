#ifndef SERVEYEJIT_H
#define SERVEYEJIT_H

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../../include/dsostd.h"
#include "../service_msg.h"
#include "../servtrace/servtrace.h"
#include "../servtrace/dataprovider.h"

#include "ceyecr.h"
#include "ceyemap.h"
#include "cmasktest.h"
#include "ceyemeasure.h"
#include "cjitmeasure.h"
#include "serveyejitthread.h"

typedef enum
{
    FLEXRAY,
    SIGNAL2,
    SIGNAL3,
    USB2_0,
}SIG_STANDARD;

typedef enum
{
    ONE,
    TWO,
    FOUR,
}EYE_NUM;

typedef enum
{
    TP1,
    TP2,
}MASK_TYPE;

using namespace eyeLib;
class servEyeJit : public serviceExecutor,public ISerial
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:
    servEyeJit( QString name, ServiceId eId = E_SERVICE_ID_EYEJIT );

    void calculate();
    void rst();
    int startup();
    void registerSpy();
    int serialIn(CStream &stream, serialVersion ver);
    int serialOut(CStream &stream, serialVersion &ver);

    enum
    {
        cmd_base = 0,
        MSG_EYEJIT_EYEMAP,
        MSG_EYEJIT_EYESCREEN,
        MSG_EYEJIT_MASKMAP,

        MSG_EYEJIT_UPDATELOOP,
        MSG_EYEJIT_EMEASRES,
        cmd_trace_updated,

        cmd_eye_viewOffset,
        cmd_eye_statisScr,
        cmd_eye_tInc_scr,

        cmd_jitter_jitterwfm,
        cmd_jitter_provider,
        cmd_jitter_autoScale,
        cmd_jitter_MaxMin,
    };

public:
    DsoErr setSrcCH(Chan Src);
    Chan   getSrcCH();

    DsoErr setSigStandard(SIG_STANDARD SigStandard);
    SIG_STANDARD getSigStandard();

    DsoErr setSigType(bool SigType);
    bool   getSigType();

    DsoErr setEyeOffset(int offset);
    int    getEyeOffset();

    DsoErr setEyeScale(int vScale);

    DsoErr setEyeViewOffset(int vOffset);
    int    getEyeViewOffset();

    DsoErr setThresType(ThresType type);
    ThresType getThresType();

    DsoErr setHighThresPer(int ThresH);
    int    getHighThresPer();

    DsoErr setMidThresPer(int ThresM);
    int    getMidThresPer();

    DsoErr setLowThresPer(int ThresL);
    int    getLowThresPer();

    DsoErr    setHighThresVal(qlonglong ThresH);
    qlonglong getHighThresVal();

    DsoErr    setMidThresVal(qlonglong ThresM);
    qlonglong getMidThresVal();

    DsoErr       setLowThresVal(qlonglong ThresL);
    qlonglong    getLowThresVal();

    DsoErr      setFreqType(FreqType type);
    FreqType    getFreqType();

    DsoErr      setClockChan(Chan chan);
    Chan        getClockChan();

    DsoErr setCRType(CRType type);
    int    getCRType();

    DsoErr setDataFreq(qlonglong dataRate);
    qlonglong getDataFreq();

    DsoErr setPLLorder(PLLorder Order);
    int getPLLorder();

    DsoErr setPLLWidth(int LoopWidth);
    int getPLLWidth();

    DsoErr setDampfact(int dampFactor);
    int    getDampfact();

    DsoErr setDispEyeOnOff(bool dispEyeOnOff);
    bool getDispEyeOnOff();

    DsoErr setEyeNum(EYE_NUM eyeNum);
    EYE_NUM getEyeNum();

    DsoErr setEyeMeasOnOff(bool OnOff);
    bool getEyeMeasOnOff();

    DsoErr setJMeasType(JMEAS_TYPE JitType);
    JMEAS_TYPE getJMeasType();

//    DsoErr setJMeasItem();

    DsoErr setJitTrackOnOff(bool jitTrackOnOff);
    bool getJitTrackOnOff();

    DsoErr setJitHistoOnOff(bool jitHistoOnOff);
    bool getJitHistoOnOff();

    DsoErr setJitSpecOnOff(bool jitSpecOnOff);
    bool getJitSpecOnOff();

    DsoErr setMaskTestOnOff(bool maskTestOnOff);
    bool getMaskTestOnOff();

    DsoErr setMaskType(MASK_TYPE maskType);
    MASK_TYPE getMaskType();

    DsoErr setColorCntOnOff(bool colorCntOnOff);
    bool getColorCntOnOff();

    DsoErr setRstColorCnt();

    //custom msg::calculated results
    DsoErr setEyeMap();
    void *getEyeMap();
    void *getEyeStatisScr();
    DsoErr setEyeScreen();
    void *getEyeScreen();

    DsoErr setMaskMap();
    void *getMaskMap();

    void *getJitterWfm();

    DsoErr setEyeMeasRes();
    void *getEyeMeasRes();

    void clearEyeMap();

    CRPara getCRPara();

    int setDataProvider(void *ptr);

    void setJitterScale();

    void getJitMaxMin(CArgument& argo);

    float gettIncScr();

    CEyeCR      *m_pEyeCR;
    CEyeMap     *m_pEyeMap;
    CMaskTest   *m_pMaskTest;
    CEyeMeasure *m_pEyeMeasRes;
    CJitMeasure *m_pJitMeasure;
    servEyeJitThread *m_peyeTread;
    servJitterThread *m_pJitterThread;
    int loop;

    dataProvider *m_pProvider;
    QSemaphore    m_DataSema;

    QSemaphore    m_eyeSema;//! 控制在计算的过程中菜单上的设置的变化不会影响当次计算过程

private:
    DsoErr limitRange(qlonglong& val, qlonglong min, qlonglong max);
    DsoErr limitRange(int& val, int min, int max);
    JMEAS_TYPE JMeas_type;

    MASK_TYPE mask_type;
    SIG_STANDARD signal_standard;
    EYE_NUM eye_num;

    Chan m_nSource;
    int  m_nyOffset;
    int  m_nScale;
    int  m_nviewOffset;

    CRPara m_crPara;

    bool sigType_Clo0_Dat1;
    bool dispEye_OnOff;
    bool eyeMeas_OnOff;
    bool dispJit_OnOff;
    bool maskTest_OnOff;

    bool colorCnt_OnOff;

    friend class servEyeJitThread;
};

#endif // SERVEYEJIT_H
