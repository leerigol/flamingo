#ifndef CJITMEASURE_H
#define CJITMEASURE_H

#include <QVector>
#include <QPointF>
#include "ceyecr.h"
#include "eyelib/eyelib.h"
#include "../../baseclass/dsowfm.h"
#include "../../baseclass/cargument.h"

typedef enum
{
    DATA_TIE,
    CLOCK_TIE,
    CYCLE_CYCLE,
    POS_POS,
    NEG_NEG,
}JMEAS_TYPE;

using namespace eyeLib;
class CJitMeasure
{
public:
    CJitMeasure();
    //!根据传过来的时钟序列计算抖动
    void calculateJitter(DsoWfm* pWfmA, servEyeJit* pServJitter);
    void setJitterType(JMEAS_TYPE type);

    void calCycToCyc(const QVector<int>& edge, bool firstEdgeUp, int N = 1);

    void insertTie(QVector<float> &phaseErr, const QVector<int> &phaseErrIndex, QVector<int> & clock);
    void* getJitterWfm();

    void  autoScaleOffset();

    void  setMathCh(int i);
    int   getMathCh();
    void  getMaxMin(CArgument& arg);

    bool JitTrack_OnOff;
    bool JitHisto_OnOff;
    bool JitSpec_OnOff;
private:
    CEyeCR* m_pEyeCR;
    QVector<int>  m_period;
    JMEAS_TYPE m_jitterType;
    QVector<float> m_jitter;
    int mathCh;
    float   samptRealInc;
    float   m_jitterMax;
    float   m_jitterMin;
    DsoWfm* m_pJitWfm;
};

#endif // CJITMEASURE_H
