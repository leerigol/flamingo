#include "cjitmeasure.h"
#include "serveyejit.h"
#include "../servHisto/servhisto.h"

CJitMeasure::CJitMeasure()
{
    m_pEyeCR = new CEyeCR;
    m_jitterType = DATA_TIE;
    m_pJitWfm = new DsoWfm;

    JitTrack_OnOff = false;
    JitSpec_OnOff = false;
    JitHisto_OnOff = false;
}

void CJitMeasure::calculateJitter(DsoWfm *pWfmA, servEyeJit *pServJitter)
{
    CEyeCR* pEyeCR = pServJitter->m_pEyeCR;
    pEyeCR->init(pWfmA,pServJitter->getCRPara());
    samptRealInc = pWfmA->gettInc().toFloat();
    LOG_DBG()<<"samptRealInc"<<samptRealInc;
    if(m_pJitWfm->size() != pWfmA->size())
    {
        m_pJitWfm->init(pWfmA->size());
    }

    m_pJitWfm->settAttr(pWfmA->getAttr());
    m_pJitWfm->setRange(0, pWfmA->size());

    DsoWfm* pWfmB = NULL;
    switch (m_jitterType) {
    //! 利用时钟恢复计算出来的抖动曲线需要截去前面一段的无效数据，无效数据的长度取决于锁相环特性
    case DATA_TIE:
        pEyeCR->CR(pWfmA, pWfmB);
        insertTie(pEyeCR->getPhaseError(),pEyeCR->getPhaseErrIndex(),pEyeCR->m_crResult.index);
        break;

    case CLOCK_TIE:
        pEyeCR->CR(pWfmA, pWfmB);
        insertTie(pEyeCR->getPhaseError(),pEyeCR->getPhaseErrIndex(),pEyeCR->m_crResult.index);
        break;

    default:
        pEyeCR->m_EdgeRes.findEdge(pWfmA->getPoint(),pWfmA->size());
        const QVector<int>& edge = pEyeCR->getEdge();
        bool firstEdgeUp = pEyeCR->getFirstEdgeUp();
        calCycToCyc(edge,firstEdgeUp);
        break;
    }
    //! 当打开直方图测量时
    if(JitHisto_OnOff)
    {
        void* ptr = NULL;
        serviceExecutor::query(serv_name_histo, servHisto::cmd_measWork_ptr, ptr);
        Q_ASSERT(ptr != NULL);
        histoMeasWork* pHistoWork = static_cast<histoMeasWork*> (ptr);
        pHistoWork->addJitterBuffer(m_jitter);
    }
}

void CJitMeasure::setJitterType(JMEAS_TYPE type)
{
    m_jitterType = type;
}

void CJitMeasure::calCycToCyc(const QVector<int>& edge, bool firstEdgeUp,int N)
{
    LOG_DBG();
    int first;
    int step = 2;
    int inter = 1;
    if(m_jitterType == CYCLE_CYCLE)
    {
        first = 0;
        inter = 2 * N;
    }
    else
    {
        step = 2;
        inter = 1;
        if(m_jitterType == POS_POS)
        {
            if(firstEdgeUp)
            {
                first = 0;
            }
            else
            {
                first = 1;
            }
        }
        else
        {
            if(firstEdgeUp)
            {
                first = 1;
            }
            else
            {
                first = 0;
            }
        }
    }


    LOG_DBG();
    float* jitter = m_pJitWfm->getValue();

    m_jitter.clear();
    m_jitter.reserve(edge.size() - inter - step);
    for(int i = first;i < edge.size() - inter;i = i + step)
    {
        int S32Jitter = edge[i + inter] - edge[i];
        float f32Jitter = S32Jitter * samptRealInc;
        for(int k = edge[i];k < edge[i + inter];k++)
        {
            jitter[k] = f32Jitter;
        }
        m_jitter.push_back( f32Jitter );
    }
    LOG_DBG();
}

void CJitMeasure::insertTie(QVector<float>& phaseErr,const QVector<int>& phaseErrIndex, QVector<int>& clockIndex)
{
    LOG_DBG();
    float* jitter = m_pJitWfm->getValue();

    Q_ASSERT(phaseErrIndex[phaseErrIndex.size() - 1] < phaseErr.size());
    LOG_DBG()<<phaseErrIndex.size();
    for(int i = 0;i < phaseErrIndex.size()-1;i++)
    {
        double incTemp = (phaseErr[phaseErrIndex[i+1]] - phaseErr[phaseErrIndex[i]])/
                         (phaseErrIndex[i+1] - phaseErrIndex[i]);
        for(int j = phaseErrIndex[i] + 1;j < phaseErrIndex[i+1];j++)
        {
            int size = phaseErr.size();
            Q_ASSERT(j < size - 1);
            phaseErr[j] = phaseErr[j-1] + incTemp;
            for(int k = clockIndex[j];k < clockIndex[j+1];k++)
            {
                jitter[k] = phaseErr[j];
            }
        }
    }
    m_jitter = phaseErr;
    LOG_DBG();
}

void* CJitMeasure::getJitterWfm()
{
    return m_pJitWfm;
}

//! 将这个函数使用一个标记放在抖动线程中运行，而不能放在主线程中运行，防止出现数据的访问冲突。
void CJitMeasure::autoScaleOffset()
{
    m_jitterMax = -1e13;
    m_jitterMin = 1e13;

    for(int i = 0; i < m_jitter.size();i++)
    {
        if(m_jitter[i] > m_jitterMax)
        {
            m_jitterMax = m_jitter[i];
        }
        if(m_jitter[i] < m_jitterMin)
        {
            m_jitterMin = m_jitter[i];
        }
    }
}

void CJitMeasure::setMathCh(int i)
{
    mathCh = i;
}

int CJitMeasure::getMathCh()
{
    return mathCh;
}

void CJitMeasure::getMaxMin(CArgument& arg)
{
    arg.setVal(m_jitterMax, 0);
    arg.setVal(m_jitterMin, 1);
}
