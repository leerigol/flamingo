#include "serveyejit.h"

/*! \var 定义消息映射表
* 定义消息映射表
*/

IMPLEMENT_CMD( serviceExecutor, servEyeJit )
start_of_entry()

//! custom message
on_set_int_void   ( servEyeJit::MSG_EYEJIT_EYEMAP, &servEyeJit::setEyeMap),
on_get_pointer    ( servEyeJit::MSG_EYEJIT_EYEMAP, &servEyeJit::getEyeMap),

on_get_pointer    ( servEyeJit::cmd_eye_statisScr, &servEyeJit::getEyeStatisScr),

on_set_int_void   ( servEyeJit::MSG_EYEJIT_EYESCREEN, &servEyeJit::setEyeScreen),
on_get_pointer    ( servEyeJit::MSG_EYEJIT_EYESCREEN, &servEyeJit::getEyeScreen),

on_set_int_void   ( servEyeJit::MSG_EYEJIT_MASKMAP, &servEyeJit::setMaskMap),
on_get_pointer    ( servEyeJit::MSG_EYEJIT_MASKMAP, &servEyeJit::getMaskMap),

on_set_int_void  (servEyeJit::MSG_EYEJIT_EMEASRES, &servEyeJit::setEyeMeasRes),
on_get_pointer   (servEyeJit::MSG_EYEJIT_EMEASRES, &servEyeJit::getEyeMeasRes),

on_set_int_int(servEyeJit::cmd_eye_viewOffset, &servEyeJit::setEyeViewOffset),
on_get_int    (servEyeJit::cmd_eye_viewOffset, &servEyeJit::getEyeViewOffset),

//! \brief on_get_int
on_set_int_int( MSG_EYEJIT_SRCCH, &servEyeJit::setSrcCH ),
on_get_int    ( MSG_EYEJIT_SRCCH, &servEyeJit::getSrcCH ),

on_set_int_int( MSG_EYEJIT_YOFFSET, &servEyeJit::setEyeOffset),
on_get_int    ( MSG_EYEJIT_YOFFSET, &servEyeJit::getEyeOffset),

on_set_int_int( MSG_EYEJIT_SRC_SELSTD, &servEyeJit::setSigStandard),
on_get_int    ( MSG_EYEJIT_SRC_SELSTD, &servEyeJit::getSigStandard),

on_set_int_int( MSG_EYEJIT_SRC_SELTYPE, &servEyeJit::setSigType ),
on_get_int    ( MSG_EYEJIT_SRC_SELTYPE, &servEyeJit::getSigType ),

on_set_int_int( MSG_EYEJIT_THRE_TYPE,    &servEyeJit::setThresType),
on_get_int    ( MSG_EYEJIT_THRE_TYPE,    &servEyeJit::getThresType),

on_set_int_int( MSG_EYEJIT_HIGHTHRE_PER, &servEyeJit::setHighThresPer),
on_get_int    ( MSG_EYEJIT_HIGHTHRE_PER, &servEyeJit::getHighThresPer),

on_set_int_ll( MSG_EYEJIT_HIGHTHRE_VAL, &servEyeJit::setHighThresVal),
on_get_ll    ( MSG_EYEJIT_HIGHTHRE_VAL, &servEyeJit::getHighThresVal),

on_set_int_int( MSG_EYEJIT_MIDTHRE_PER, &servEyeJit::setMidThresPer ),
on_get_int    ( MSG_EYEJIT_MIDTHRE_PER, &servEyeJit::getMidThresPer ),

on_set_int_ll( MSG_EYEJIT_MIDTHRE_VAL, &servEyeJit::setMidThresVal),
on_get_ll    ( MSG_EYEJIT_MIDTHRE_VAL, &servEyeJit::getMidThresVal),

on_set_int_int( MSG_EYEJIT_LOWTHRE_PER, &servEyeJit::setLowThresPer ),
on_get_int    ( MSG_EYEJIT_LOWTHRE_PER, &servEyeJit::getLowThresPer ),

on_set_int_ll( MSG_EYEJIT_LOWTHRE_VAL, &servEyeJit::setLowThresVal ),
on_get_ll    ( MSG_EYEJIT_LOWTHRE_VAL, &servEyeJit::getLowThresVal ),

on_set_int_int( MSG_EYEJIT_CR_SELMETHOD, &servEyeJit::setCRType ),
on_get_int    ( MSG_EYEJIT_CR_SELMETHOD, &servEyeJit::getCRType ),

on_set_int_int( MSG_EYEJIT_CR_FREQTYPE, &servEyeJit::setFreqType ),
on_get_int    ( MSG_EYEJIT_CR_FREQTYPE, &servEyeJit::getFreqType ),

on_set_int_int( MSG_EYEJIT_CR_SELCLOCKSRC, &servEyeJit::setClockChan),
on_get_int    ( MSG_EYEJIT_CR_SELCLOCKSRC, &servEyeJit::getClockChan),

on_set_int_ll( MSG_EYEJIT_CR_DATEFREQ, &servEyeJit::setDataFreq ),
on_get_ll    ( MSG_EYEJIT_CR_DATEFREQ, &servEyeJit::getDataFreq ),

on_set_int_int( MSG_EYEJIT_CR_PLLORDER, &servEyeJit::setPLLorder ),
on_get_int    ( MSG_EYEJIT_CR_PLLORDER, &servEyeJit::getPLLorder ),

on_set_int_int( MSG_EYEJIT_CR_PLLWIDTH, &servEyeJit::setPLLWidth ),
on_get_int    ( MSG_EYEJIT_CR_PLLWIDTH, &servEyeJit::getPLLWidth ),

on_set_int_int( MSG_EYEJIT_CR_DAMPFACTOR, &servEyeJit::setDampfact ),
on_get_int    ( MSG_EYEJIT_CR_DAMPFACTOR, &servEyeJit::getDampfact ),

on_set_int_bool( MSG_EYEJIT_EMEAS_EN, &servEyeJit::setDispEyeOnOff ),
on_get_bool    ( MSG_EYEJIT_EMEAS_EN, &servEyeJit::getDispEyeOnOff ),

on_set_int_int( MSG_EYEJIT_EMEAS_NUM, &servEyeJit::setEyeNum ),
on_get_int    ( MSG_EYEJIT_EMEAS_NUM, &servEyeJit::getEyeNum ),

on_set_int_bool( MSG_EYEJIT_EMEAS_SELITEM, &servEyeJit::setEyeMeasOnOff ),
on_get_bool    ( MSG_EYEJIT_EMEAS_SELITEM, &servEyeJit::getEyeMeasOnOff),

on_set_int_int( MSG_EYEJIT_JMEAS_SELSRC, &servEyeJit::setJMeasType ),
on_get_int    ( MSG_EYEJIT_JMEAS_SELSRC, &servEyeJit::getJMeasType ),

on_set_int_bool( MSG_EYEJIT_JMEAS_ENTRACK, &servEyeJit::setJitTrackOnOff ),
on_get_bool    ( MSG_EYEJIT_JMEAS_ENTRACK, &servEyeJit::getJitTrackOnOff ),

on_set_int_bool( MSG_EYEJIT_JMEAS_ENHISTO, &servEyeJit::setJitHistoOnOff ),
on_get_bool    ( MSG_EYEJIT_JMEAS_ENHISTO, &servEyeJit::getJitHistoOnOff ),

on_set_int_bool( MSG_EYEJIT_JMEAS_ENSPEC, &servEyeJit::setJitSpecOnOff ),
on_get_bool    ( MSG_EYEJIT_JMEAS_ENSPEC, &servEyeJit::getJitSpecOnOff ),

on_set_int_bool( MSG_EYEJIT_MASK_EN, &servEyeJit::setMaskTestOnOff ),
on_get_bool    ( MSG_EYEJIT_MASK_EN, &servEyeJit::getMaskTestOnOff ),

on_set_int_int( MSG_EYEJIT_MASK_SELTYPE, &servEyeJit::setMaskType ),
on_get_int    ( MSG_EYEJIT_MASK_SELTYPE, &servEyeJit::getMaskType ),

on_set_int_bool( MSG_EYEJIT_COLOR_ENCOUNT, &servEyeJit::setColorCntOnOff ),
on_get_bool    ( MSG_EYEJIT_COLOR_ENCOUNT, &servEyeJit::getColorCntOnOff ),

on_set_int_void( MSG_EYEJIT_COLOR_RESET, &servEyeJit::setRstColorCnt ),

on_get_pointer    ( servEyeJit::cmd_jitter_jitterwfm, &servEyeJit::getJitterWfm),
on_get_void_argo  ( servEyeJit::cmd_jitter_MaxMin, &servEyeJit::getJitMaxMin),
on_get_float      ( servEyeJit::cmd_eye_tInc_scr, &servEyeJit::gettIncScr),


on_set_int_pointer( servEyeJit::cmd_trace_updated, &servEyeJit::setDataProvider),    
on_set_int_pointer( servEyeJit::cmd_jitter_autoScale, &servEyeJit::setJitterScale),

//!sys msg
//on_set_int_int(         CMD_SERVICE_ACTIVE,            &servEyeJit::setActive ),
on_set_void_void(       CMD_SERVICE_RST,               &servEyeJit::rst ),
on_set_int_void(        CMD_SERVICE_STARTUP,           &servEyeJit::startup ),

end_of_entry()
