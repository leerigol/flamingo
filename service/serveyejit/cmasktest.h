#ifndef CMASKTEST_H
#define CMASKTEST_H

#include <QVector>
#define risePercent 0.8

struct MaskNRZReal
{
    //!作NRZ码的模板标准，Y[5]表示从下到上的五个边界,不含最上和最下
    double Jitter_max;//!抖动容限，单位为UI
    double Mask_risetime;//!上升时间（20%-80%),eye_mask中输入为以UI为单位
    double Mask_falltime;//!下降时间（20%-80%),eye_mask中输入为以UI为单位
    double Y[5];//!从下倒上五个点的电压值
};

struct MaskNRZMap
{
    //!作NRZ码模板对应到map中的位置，Y[5]表示从下到上的五个边界,不含最上和最下
    //!X[6]中的前四个表示中间一块从左到右,最后两个数表示模板范围在Map中的左起点和右起点
    QVector<int>  X;
    QVector<int>  Y;
};

class CEyeMap;

class CMaskTest
{
private:
    MaskNRZMap m_maskMap;
    int m_testNum;
    int m_failNum;
    int UI_start;
    int UI_end;

public:
    CMaskTest();
    MaskNRZMap* getMaskMap();
    void maskRealtoMap(MaskNRZReal mask_real, CEyeMap *eyeMap);
    void calcuTestResult(CEyeMap *eyeMap);
};

#endif // CMASKTEST_H
