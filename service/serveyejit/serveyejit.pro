#-------------------------------------------------
#
# Project created by QtCreator 2017-02-07T11:43:16
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets

TEMPLATE = lib
TARGET = ../../lib$$(PLATFORM)/services/serveyejit
CONFIG += staticlib
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

#DEFINES += _DEBUG

#Input
HEADERS  +=  ceyecr.h \
    eyelib/eyelib.h
HEADERS  +=  ceyemap.h
#HEADERS  +=  cmasktest.h
HEADERS  +=  cjitmeasure.h
HEADERS  +=  ceyemeasure.h
HEADERS  +=  serveyejit.h
HEADERS  +=  serveyejitthread.h


SOURCES  +=  ceyecr.cpp \
    serveyejit_map.cpp \
    eyelib/eyelib.cpp
SOURCES  +=  ceyemap.cpp
#SOURCES  +=  cmasktest.cpp
SOURCES  +=  cjitmeasure.cpp
SOURCES  +=  ceyemeasure.cpp
SOURCES  +=  serveyejit.cpp
SOURCES  +=  serveyejitthread.cpp
