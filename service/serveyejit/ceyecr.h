#ifndef CEYECR_H
#define CEYECR_H
#include <QVector>
#include "eyelib/eyelib.h"
#include "../../baseclass/dsowfm.h"
#include "../../include/dsodbg.h"
#include "../../include/dsocfg.h"

class CEyeMap;
class servEyeJit;
typedef double EyeVolType;

namespace eyeLib {
class CEyeCR
{
public:
    CEyeCR();

    void init(DsoWfm *pWfmA, CRPara crPara);

    void setAcqDep(servEyeJit* serv);
    void CR(DsoWfm *pWfmA, DsoWfm *pWfmB);

    int getSymbolNum();
    double sampsPerSym();

    const QVector<int>& getEdge();
    bool getFirstEdgeUp();
    QVector<float>& getPhaseError();
    const QVector<int>& getPhaseErrIndex();

    //!clock recovery results
    CRResult       m_crResult;
    CRPara         m_crPara;
    EdgeAttr       m_EdgeRes;

private:
    QVector<int> samptoSymbolAtEdge(const QVector<int> &sampsAtEdge, double sampsPeriod);
    void pllCR(DsoWfm *pWfmA);
    void constCR(DsoWfm *);
    void extCR(DsoWfm *pWfmA,DsoWfm *pWfmB );

    int  findLeast(QVector<int> edge,int sampSize);
    int  findLeast(QVector<int> edge);
    EyeVolType getVolMin();
    EyeVolType getVolMax();

    int          m_AcqLength;
    long long    m_AcqRate;

    EyeVolType   m_EyeVolMax;
    EyeVolType   m_EyeVolMin;
    bool         m_isThresInited;

    QVector<float> m_PhaseErrorRecord;
    QVector<int>   m_PhaseErrIndex;

    friend class CEyeMap;
    friend class servEyeJit;
};
}
#endif // CEYECR_H
