#ifndef EYELIB_H
#define EYELIB_H
#include <QVector>
#include "../../../include/dsotype.h"

#include "../../include/dsodbg.h"

using namespace DsoType;
namespace eyeLib {

typedef enum
{
    CONST_CR = 0,
    PLL_CR,
    PARA_CR,
}CRType;

typedef enum
{
    ONE_ORDER = 0,
    TWO_ORDER,
}PLLorder;

enum FreqType
{
    AUTO_FREQ,
    SEMIAUTO_FREQ,
    MANUAL_FREQ,
};

typedef struct
{
    CRType     m_CRType;
    FreqType   freqType;
    long long  m_DataFreq;
    int        m_PllLoopWidth;
    int        m_DampFactor;
    PLLorder   m_PllOrder;

    DsoType::Chan clockChan;

    ThresType  thresType;
    int        lowThreshold;
    int        lowThresVal;

    int        midThreshold;
    int        midThresVal;

    int        highThreshold;
    int        highThresVal;
}CRPara;

struct CRResult
{
    QVector<int> index;
    QVector<int> offset;
    int     eyeNumDisp;
    int     multiple;
};

struct EdgeAttr
{
    bool     isFirstUp;
    QVector<int>  edge;
    void setThres(uchar high, uchar mid, uchar low);

    void findEdge(unsigned char *data, int length, int highPer, int midPer, int lowPer);
    void findEdge(unsigned char *data,int length);
    uchar getMidThres();
private:
    uchar highThres;
    uchar midThres;
    uchar lowThres;
};

QVector<double> leastSquare(const QVector<int>& x, const QVector<int> &y);
double leastSquare(const QVector<int> &x, const QVector<int> &y,double a);

void EyeDbgLog( const QString &fileName, uchar *pBuf, int size );
void EyeDbgLog( const QString &fileName, float *pBuf, int size);
}
#endif // EYELIB_H
