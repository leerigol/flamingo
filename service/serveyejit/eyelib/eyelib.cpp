#include "eyelib.h"

namespace eyeLib {

void EdgeAttr::setThres(uchar high, uchar mid, uchar low)
{
    highThres = high;
    midThres = mid;
    lowThres = low;
}

void EdgeAttr::findEdge(unsigned char *data, int length, int highPer, int midPer, int lowPer)
{
    uchar max = 0;
    uchar min = 255;

    int i = 0;
    while(i < length)
    {
        if (data[i] > max)
        {
            max = data[i];
        }
        if (data[i] < min)
        {
            min = data[i];
        }
        i++;
    }

    highThres = (highPer*max + (100 - highPer)*min)/100;
    midThres = (midPer*max + (100 - midPer)*min)/100;
    lowThres = (lowPer*max + (100 - lowPer)*min)/100;

    findEdge( data, length);
}

void EdgeAttr::findEdge(unsigned char *data, int length)
{
    LOG_DBG();

    //EyeDbgLog(QString("eyetrace"),data,length);
    edge.clear();
    bool up1_down0;
    bool crossedMid = false;
    int currentEdge = 0;
    int i = 0;
    for(i = 0; i < length; i++)
    {
        if(data[i] > highThres)
        {
            up1_down0 = false;
            isFirstUp = false;
            break;
        }
        if(data[i] < lowThres)
        {
            up1_down0 = true;
            isFirstUp = true;
            break;
        }
    }

    for( ; i < length; i++)
    {
        //! up1_down0 == false寻找下降沿
        if( up1_down0 == false )
        {
            if(!crossedMid)
            {
                if(data[i] < midThres)
                {
                    currentEdge = i;
                    crossedMid = true;
                }
            }
            //! 这边没有用else的原因是考虑到mid到low中间无点的情形
            if(crossedMid)
            {
                if(data[i] < lowThres)
                {
                    edge.push_back(currentEdge);
                    up1_down0 = true;
                    crossedMid = false;
                }
                if(data[i] > midThres)
                {
                    crossedMid = false;
                }
            }
        }
        //!up1_down0 == true寻找上升沿
        else
        {
            if(!crossedMid)
            {
                if(data[i] > midThres)
                {
                    currentEdge = i;
                    crossedMid = true;
                }
            }

            if(crossedMid)
            {
                if(data[i] > highThres)
                {
                    edge.push_back(currentEdge);
                    up1_down0 = false;
                    crossedMid = false;
                }
                if(data[i] < midThres)
                {
                    crossedMid = false;
                }
            }
        }
    }

    //!在m_edge当中压入一个超过m_acqLength的值，以防止在clock recover循环当中访问m_edge时出现越界
    edge.push_back(length+5);
    LOG_DBG();
}

uchar EdgeAttr::getMidThres()
{
    return midThres;
}

/*!
 * y = ax + b;
 * 输入x为symbolAtEdge，y为samperAtEdge，则输出a为sampsPerSymbol，b为
 * */
QVector<double> leastSquare(const QVector<int>& x,const QVector<int>& y)
{
    double t1 = 0,t2 = 0,t3 = 0,t4 = 0;
    double length = y.length();
    for(int i = 0; i < length; i++)
    {
        qlonglong xi = x[i];
        qlonglong yi = y[i];
        t1 += xi*xi;
        t2 += xi;
        t3 += xi*yi;
        t4 += yi;
    }
    double a = (t3*length - t2*t4)/(t1*length-t2*t2);
    double b = (1.0*(t1*t4 - t2*t3))/(1.0*(t1*length - t2*t2));
    QVector<double> p;
    p.push_back(a);
    p.push_back(b);
    return p;
}

double leastSquare(const QVector<int>& x,const QVector<int>& y,double a)
{
    double t2 = 0,t4 = 0;
    double length = y.length();
    for(int i = 0;i < length;i++)
    {
        t2 += x[i];
        t4 += y[i];
    }
    double b = (t4 - a * t2) / x.size();
    return b;
}

void EyeDbgLog( const QString &fileName, uchar *pBuf, int size )
{
    Q_ASSERT( NULL != pBuf && size > 0 );

    QFile file( "/rigol/log/"+fileName );
    if( !file.open(QIODevice::WriteOnly) )
    {
        LOG_DBG()<<"!!!"<<fileName<<"log fail";
        return;
    }

    QTextStream out(&file);
    for(int i = 0 ;i < size;i++)
    {
        int temp = pBuf[i];
        out<<QString::number(temp, 10)<<"\n";
    }

    file.close();
}

void EyeDbgLog( const QString &fileName, float *pBuf, int size )
{
    Q_ASSERT( NULL != pBuf && size > 0 );

    QFile file( "/rigol/log/"+fileName );
    if( !file.open(QIODevice::WriteOnly) )
    {
        LOG_DBG()<<"!!!"<<fileName<<"log fail";
        return;
    }

    QTextStream out(&file);
    for(int i = 0 ;i < size;i++)
    {
        double temp = pBuf[i];
        out<<QString::number(temp)<<"\n";
    }

    file.close();
}

}
