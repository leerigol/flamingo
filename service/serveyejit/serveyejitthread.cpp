#include <fstream>
#include "serveyejitthread.h"
#include "serveyejit.h"
#include "../servgui/servgui.h"
#include "../servmath/servmath.h"
#include "../service.h"

servEyeJitThread::servEyeJitThread(servEyeJit *pEyeJit)
{
    m_pServEyeJit = pEyeJit;
    m_loopCR = -1;
    m_loopEyeMap = -1;
    m_loopMask = -1;
}

//! 每按下一次按键，线程被调用一次，要检查数据帧loop是否被更新，计算对应的被更新了的数据模块
void servEyeJitThread::run()
{
    LOG_DBG();
    while(1)
    {
        dsoVert* pVertA;
        DsoWfm wfmA;
        bool CRonoff = false;
        if(m_pServEyeJit->getDispEyeOnOff())  {CRonoff = true;}
//        if(m_pServEyeJit->getMaskTestOnOff()) {CRonoff = true;}

        if(CRonoff)
        {
            m_pServEyeJit->m_DataSema.acquire();
            m_pServEyeJit->m_pProvider->acquire();
//            m_pServEyeJit->m_eyeSema.acquire();
            LOG_DBG();
            pVertA  = dsoVert::getCH(m_pServEyeJit->m_nSource);
            pVertA->getEye(wfmA);

            //! 计算对应到屏幕上的offset
//            int scale = wfmA.getyRefScale();
//            m_pServEyeJit->setEyeScale(scale);
            if(wfmA.size() < 200)
            {
                LOG_DBG()<<"get trace fail";
                continue;
            }

            //! apply the crPara from menu to the crPara in the service
            m_pServEyeJit->m_pEyeMap->clockRecovery(wfmA, m_pServEyeJit->getCRPara(),pVertA);
            LOG_DBG();
    
            m_pServEyeJit->m_pProvider->release();
            
            if(m_pServEyeJit->getDispEyeOnOff())
            {
                LOG_DBG();
                m_pServEyeJit->m_pEyeMap->splitter1(&m_pServEyeJit->m_pEyeCR->m_crResult,
                                                    m_pServEyeJit->m_pEyeCR->sampsPerSym(),&wfmA);
                serviceExecutor::post( serv_name_eyejit,servEyeJit::MSG_EYEJIT_EYEMAP,0 );
                LOG_DBG();

                if(m_pServEyeJit->getEyeMeasOnOff())
                {
                    LOG_DBG();
                    m_pServEyeJit->m_pEyeMeasRes->measEyePara(m_pServEyeJit->m_pEyeMap);
                    serviceExecutor::post( serv_name_eyejit,servEyeJit::MSG_EYEJIT_EMEASRES,0);
                }
            }
        }
        else
        {
            //! exit the thread
            serviceExecutor::post(serv_name_eyejit,servEyeJit::MSG_EYEJIT_UPDATELOOP,0);
            m_pServEyeJit->clearEyeMap();
            break;
        }

        //m_pServEyeJit->m_eyeSema.release();
        LOG_DBG();
        QThread::msleep(200);
    }
}

servJitterThread::servJitterThread(servEyeJit *pJitter)
{
    m_pServJitter = pJitter;
}

void servJitterThread::run()
{
    LOG_DBG();

    if(m_pServJitter->getJitTrackOnOff())
    {
        for(int i = 0;i < 4;i++)
        {
            bool mathEn;
            serviceExecutor::query( QString("math%1").arg(QString::number(i)),
                                   MSG_MATH_EN, mathEn );
            if(!mathEn)
            {
                LOG_DBG()<<"turn on the math channel: "<<QString::number(i);
                serviceExecutor::post(E_SERVICE_ID_MATH1 + i, MSG_MATH_EN, true);
                serviceExecutor::post(E_SERVICE_ID_MATH1 + i,
                                      MSG_MATH_S32MATHOPERATOR, operator_trend);
                m_pServJitter->m_pJitMeasure->setMathCh(i);
                break;
            }
        }
    }

    LOG_DBG()<<__FILE__;
    while(1)
    {
        dsoVert* pVertA;
        DsoWfm wfmA;
        if(m_pServJitter->getJitTrackOnOff())
        {
            LOG_DBG();
            m_pServJitter->m_DataSema.acquire();
            m_pServJitter->m_pProvider->acquire();

            LOG_DBG()<<__FILE__;
            pVertA = dsoVert::getCH(m_pServJitter->getSrcCH());
            pVertA->getTrace(wfmA);

            m_pServJitter->m_pProvider->release();
            if(wfmA.size() < 2)
            {
                LOG_DBG()<<"get trace fail";
                continue;
            }
            LOG_DBG();
            m_pServJitter->m_pJitMeasure->calculateJitter(&wfmA,m_pServJitter);
            serviceExecutor::post(serv_name_eyejit,servEyeJit::cmd_jitter_provider,
                                  pointer (m_pServJitter));
        }
        else
        {
            break;
        }
    }
    serviceExecutor::post( serv_name_gui, servGui::cmd_logo_animate_run, false);
}
