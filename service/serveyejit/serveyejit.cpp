#include "serveyejit.h"
#include "ceyecr.h"
#include "../servmath/servmath.h"
#include "../servHisto/servhisto.h"

servEyeJit::servEyeJit(QString name, ServiceId eId)
    : serviceExecutor(name, eId)
{
    serviceExecutor::baseInit();

    signal_standard = FLEXRAY;
    sigType_Clo0_Dat1 = true;
    eye_num = ONE;
    JMeas_type = DATA_TIE;
    mask_type = TP1;

    m_nSource = chan1;

    dispEye_OnOff = false;
    maskTest_OnOff = false;
    eyeMeas_OnOff = false;
    colorCnt_OnOff = false;

    m_crPara.m_CRType = PLL_CR;
    m_crPara.m_DataFreq = 10000000;
    m_crPara.m_PllLoopWidth = 10000;
    m_crPara.highThreshold = 70;
    m_crPara.midThreshold = 50;
    m_crPara.lowThreshold = 30;

    m_nyOffset = 0;
    m_nviewOffset = 0;
    m_nScale = mv(500);

    m_pEyeCR = new CEyeCR;
    m_pEyeMap = new CEyeMap;
    //m_pMaskTest = new CMaskTest;
    m_pEyeMeasRes = new CEyeMeasure;
    m_pJitMeasure = new CJitMeasure;
    m_peyeTread = new servEyeJitThread(this);
    m_pJitterThread = new servJitterThread(this);
}

void servEyeJit::rst()
{
    signal_standard = FLEXRAY;
    sigType_Clo0_Dat1 = true;
    eye_num = ONE;
    JMeas_type = DATA_TIE;
    mask_type = TP1;

    m_nSource = chan1;

    dispEye_OnOff = false;
    maskTest_OnOff = false;

    m_pJitMeasure->JitTrack_OnOff = false;
    m_pJitMeasure->JitHisto_OnOff = false;
    m_pJitMeasure->JitSpec_OnOff = false;

    colorCnt_OnOff = false;
}

int servEyeJit::startup()
{
    return 0;
}

void servEyeJit::registerSpy()
{
    serviceExecutor::spyOn( E_SERVICE_ID_TRACE,
                            servTrace::cmd_set_provider,
                            mpItem->servId,
                            servEyeJit::cmd_trace_updated );

    setMsgAttr( servEyeJit::cmd_trace_updated );
}

#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

#define SERIAL_OP   stream.read
#define SERIAL_OP_C( a, b, c, type )   stream.read( a, c ); \
                                       memset( &b, 0, sizeof(b));\
                                       memcpy( &b, &c, sizeof(type) );
int servEyeJit::serialIn(CStream &stream, serialVersion /*ver*/)
{
    quint8 qu8Dat;
    SERIAL_OP_C(QStringLiteral("m_nSource"), m_nSource ,qu8Dat, quint8);
    SERIAL_OP_C(QStringLiteral("m_crPara.m_CRType"), m_crPara.m_CRType,qu8Dat,quint8);
    SERIAL_OP(QStringLiteral("m_crPara.m_DataFreq"), m_crPara.m_DataFreq);
    SERIAL_OP_C(QStringLiteral("m_crPara.m_PllOrder"), m_crPara.m_PllOrder, qu8Dat, quint8);
    SERIAL_OP(QStringLiteral("m_crPara.m_PllLoopWidth"), m_crPara.m_PllLoopWidth);
    SERIAL_OP(QStringLiteral("m_crPara.m_DampFactor"), m_crPara.m_DampFactor);

    SERIAL_OP_C(QStringLiteral("signal_standard"), signal_standard, qu8Dat,quint8);
    return ERR_NONE;
}


#ifdef SERIAL_OP
#undef SERIAL_OP
#endif

#define SERIAL_OP   stream.write
int servEyeJit::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;
    SERIAL_OP(QStringLiteral("m_nSource"), (quint8) m_nSource);
    SERIAL_OP(QStringLiteral("m_crPara.m_CRType"), (quint8) m_crPara.m_CRType );
    SERIAL_OP(QStringLiteral("m_crPara.m_DataFreq"), m_crPara.m_DataFreq);
    SERIAL_OP(QStringLiteral("m_crPara.m_PllOrder"), (quint8) m_crPara.m_PllOrder );
    SERIAL_OP(QStringLiteral("m_crPara.m_PllLoopWidth"), m_crPara.m_PllLoopWidth);
    SERIAL_OP(QStringLiteral("m_crPara.m_DampFactor"), m_crPara.m_DampFactor);

    stream.write(QStringLiteral("signal_standard"), (quint8) signal_standard);
    return ERR_NONE;
}

DsoErr servEyeJit::setSrcCH(Chan Src)
{
    m_nSource = Src;
    return ERR_NONE;
}

Chan servEyeJit::getSrcCH()
{
    return m_nSource;
}

DsoErr servEyeJit::setSigStandard(SIG_STANDARD SigStandard)
{
    signal_standard = SigStandard;
    return ERR_NONE;
}

SIG_STANDARD servEyeJit::getSigStandard()
{
    return signal_standard;
}

DsoErr servEyeJit::setSigType(bool SigType)
{
    sigType_Clo0_Dat1 = SigType;
    return ERR_NONE;
}

bool servEyeJit::getSigType()
{
    return sigType_Clo0_Dat1;
}

DsoErr servEyeJit::setEyeOffset(int offset)
{
    m_nyOffset = offset;
    return ERR_NONE;
}

int servEyeJit::getEyeOffset()
{
    setuiBase(1,E_N6);
    setuiStep(m_nScale/25);
    setuiUnit(Unit_V);
    return m_nyOffset;
}

DsoErr servEyeJit::setEyeScale(int vScale)
{
    m_nScale = vScale;
    return ERR_NONE;
}

DsoErr servEyeJit::setEyeViewOffset(int vOffset)
{
    m_nviewOffset = vOffset;
    return ERR_NONE;
}

int servEyeJit::getEyeViewOffset()
{
    m_nviewOffset = m_nyOffset * 60/m_nScale;
    return m_nviewOffset;
}

DsoErr servEyeJit::setThresType(ThresType type)
{
    m_crPara.thresType = type;
    return ERR_NONE;
}

ThresType servEyeJit::getThresType()
{
    return m_crPara.thresType;
}

DsoErr servEyeJit::setHighThresPer(int ThresH)
{
    m_crPara.highThreshold = ThresH;
    return ERR_NONE;
}

int servEyeJit::getHighThresPer()
{
    setuiBase(1,E_0);
    setuiUnit(Unit_percent);
    return m_crPara.highThreshold;
}

DsoErr servEyeJit::setMidThresPer(int ThresM)
{
    m_crPara.midThreshold = ThresM;
    return ERR_NONE;
}

int servEyeJit::getMidThresPer()
{
    setuiBase(1,E_0);
    setuiUnit(Unit_percent);
    return m_crPara.midThreshold;
}

DsoErr servEyeJit::setLowThresPer(int ThresL)
{
    m_crPara.lowThreshold = ThresL;
    return ERR_NONE;
}

int servEyeJit::getLowThresPer()
{
    setuiBase(1,E_0);
    setuiUnit(Unit_percent);
    return m_crPara.lowThreshold;
}

DsoErr servEyeJit::setHighThresVal(qlonglong ThresH)
{
    m_crPara.highThresVal = ThresH;
    return ERR_NONE;
}

qlonglong servEyeJit::getHighThresVal()
{
    setuiBase(1,E_N6);
    return m_crPara.highThresVal;
}

DsoErr servEyeJit::setMidThresVal(qlonglong ThresM)
{
    m_crPara.midThresVal = ThresM;
    return ERR_NONE;
}

qlonglong servEyeJit::getMidThresVal()
{
    return m_crPara.midThresVal;
}

DsoErr servEyeJit::setLowThresVal(qlonglong ThresL)
{
    m_crPara.lowThresVal = ThresL;

    return ERR_NONE;
}

qlonglong servEyeJit::getLowThresVal()
{
    return m_crPara.lowThresVal;
}

DsoErr servEyeJit::setFreqType(FreqType type)
{
    m_crPara.freqType = type;
    if(type == AUTO_FREQ)
    {
        mUiAttr.setEnable(MSG_EYEJIT_CR_DATEFREQ, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_EYEJIT_CR_DATEFREQ, true);
    }
    return ERR_NONE;
}

FreqType servEyeJit::getFreqType()
{
    return m_crPara.freqType;
}

DsoErr servEyeJit::setClockChan(Chan chan)
{
    m_crPara.clockChan = chan;
    return ERR_NONE;
}

Chan servEyeJit::getClockChan()
{
    return m_crPara.clockChan;
}

DsoErr servEyeJit::setCRType(CRType type)
{
    m_crPara.m_CRType = type;
    return ERR_NONE;
}

int servEyeJit::getCRType()
{
    return m_crPara.m_CRType;
}

DsoErr servEyeJit::setDataFreq(qlonglong dataRate)
{
    m_crPara.m_DataFreq = dataRate;
    return ERR_NONE;
}

qlonglong servEyeJit::getDataFreq()
{
    setuiMinVal( (qlonglong) 100e3);
    setuiMaxVal( (qlonglong) 1e9);
    setuiBase(1,E_0);
    setuiUnit(Unit_hz);
    setuiStep(getFreqStep(m_crPara.m_DataFreq));
    return m_crPara.m_DataFreq;
}

DsoErr servEyeJit::setPLLorder(PLLorder Order)
{
    m_crPara.m_PllOrder = Order;
    if(Order == ONE_ORDER)
    {
        mUiAttr.setVisible(MSG_EYEJIT_CR_DAMPFACTOR,false);
    }
    else
    {
        mUiAttr.setVisible(MSG_EYEJIT_CR_DAMPFACTOR,true);
    }
    return ERR_NONE;
}

int servEyeJit::getPLLorder()
{
    return m_crPara.m_PllOrder;
}

DsoErr servEyeJit::setPLLWidth(int LoopWidth)
{
    m_crPara.m_PllLoopWidth = LoopWidth;
    return ERR_NONE;
}

int servEyeJit::getPLLWidth()
{
    setuiMaxVal(m_crPara.m_DataFreq/10);
    setuiMinVal(m_crPara.m_DataFreq/10000);
    setuiBase(1,E_0);
    setuiUnit(Unit_hz);
    setuiStep(getFreqStep(m_crPara.m_DataFreq));
    return m_crPara.m_PllLoopWidth;
}

DsoErr servEyeJit::setDampfact(int dampFactor)
{
    m_crPara.m_DampFactor = dampFactor;
    return ERR_NONE;
}

int servEyeJit::getDampfact()
{
    setuiMaxVal(1);
    setuiMinVal(0);
    setuiBase(1,E_N3);
    return m_crPara.m_DampFactor;
}

DsoErr servEyeJit::setDispEyeOnOff(bool dispEyeOnOff)
{
    dispEye_OnOff = dispEyeOnOff;
    calculate();
    return ERR_NONE;
}

bool servEyeJit::getDispEyeOnOff()
{
    return dispEye_OnOff;
}

DsoErr servEyeJit::setEyeNum(EYE_NUM eyeNum)
{
    eye_num = eyeNum;
    return ERR_NONE;
}

EYE_NUM servEyeJit::getEyeNum()
{
    return eye_num;
}

DsoErr servEyeJit::setEyeMeasOnOff(bool OnOff)
{
    eyeMeas_OnOff = OnOff;
    return ERR_NONE;
}

bool servEyeJit::getEyeMeasOnOff()
{
    //calculate();
    return eyeMeas_OnOff;
}

DsoErr servEyeJit::setJMeasType(JMEAS_TYPE JitType)
{
    JMeas_type = JitType;
    m_pJitMeasure->setJitterType(JMeas_type);
    return ERR_NONE;
}

JMEAS_TYPE servEyeJit::getJMeasType()
{
    return JMeas_type;
}

DsoErr servEyeJit::setJitTrackOnOff(bool jitTrackOnOff)
{
    m_pJitMeasure->JitTrack_OnOff = jitTrackOnOff;
    calculate();
    return ERR_NONE;
}

bool servEyeJit::getJitTrackOnOff()
{
    return m_pJitMeasure->JitTrack_OnOff;
}

DsoErr servEyeJit::setJitHistoOnOff(bool jitHistoOnOff)
{
    LOG_DBG();
    m_pJitMeasure->JitHisto_OnOff = jitHistoOnOff;
    calculate();
    serviceExecutor::post(serv_name_histo, servHisto::cmd_histo_jitter_en, false);
    return ERR_NONE;
}

bool servEyeJit::getJitHistoOnOff()
{
    return m_pJitMeasure->JitHisto_OnOff;
}

DsoErr servEyeJit::setJitSpecOnOff(bool jitSpecOnOff)
{
    m_pJitMeasure->JitSpec_OnOff = jitSpecOnOff;
    return ERR_NONE;
}

bool servEyeJit::getJitSpecOnOff()
{
    return m_pJitMeasure->JitSpec_OnOff;
}

DsoErr servEyeJit::setMaskTestOnOff(bool maskTestOnOff)
{
    maskTest_OnOff = maskTestOnOff;
    calculate();
    return ERR_NONE;
}

bool servEyeJit::getMaskTestOnOff()
{
    return maskTest_OnOff;
}

DsoErr servEyeJit::setMaskType(MASK_TYPE maskType)
{
    mask_type = maskType;
    return ERR_NONE;
}

MASK_TYPE servEyeJit::getMaskType()
{
    return mask_type;
}

DsoErr servEyeJit::setColorCntOnOff(bool colorCntOnOff)
{
    colorCnt_OnOff = colorCntOnOff;
    return ERR_NONE;
}

bool servEyeJit::getColorCntOnOff()
{
    return colorCnt_OnOff;
}

DsoErr servEyeJit::setRstColorCnt()
{
    return ERR_NONE;
}

DsoErr servEyeJit::setEyeMap()
{
    return ERR_NONE;
}

void *servEyeJit::getEyeMap()
{
    return m_pEyeMap->getEyeMap();
}

void *servEyeJit::getEyeStatisScr()
{
    return m_pEyeMap->getEyeStatisScr();
}

DsoErr servEyeJit::setEyeScreen()
{
    return ERR_NONE;
}

void *servEyeJit::getEyeScreen()
{
    return m_pEyeMap->getEyeCurveScreen();
}

DsoErr servEyeJit::setMaskMap()
{
    return ERR_NONE;
}

void *servEyeJit::getMaskMap()
{
    //return m_pMaskTest->getMaskMap();
    return NULL;
}

void *servEyeJit::getJitterWfm()
{
    return m_pJitMeasure->getJitterWfm();
}

DsoErr servEyeJit::setEyeMeasRes()
{
    return ERR_NONE;
}

void *servEyeJit::getEyeMeasRes()
{
    return m_pEyeMeasRes->getMeasRes();
}

void servEyeJit::clearEyeMap()
{
    //! to do : 设置一个清空的标记，在循环结束处进行清空
    m_pEyeMap->getEyeMap()->clear();
}

CRPara servEyeJit::getCRPara()
{
    return m_crPara;
}

int servEyeJit::setDataProvider(void *ptr)
{
    Q_UNUSED(ptr);
//    m_pProvider = (dataProvider*)ptr;

    if ( m_DataSema.available() < 1 )
    {
        m_DataSema.release();
    }

//    Q_ASSERT( NULL != m_pProvider );

    return ERR_NONE;
}

void servEyeJit::setJitterScale()
{
    m_pJitMeasure->autoScaleOffset();
    serviceExecutor::post(E_SERVICE_ID_MATH1 + m_pJitMeasure->getMathCh(),
                          servMath::cmd_trend_scale,0);
}

void servEyeJit::getJitMaxMin(CArgument &argo)
{
    m_pJitMeasure->getMaxMin(argo);
}

float servEyeJit::gettIncScr()
{
    return m_pEyeMap->m_tIncScr;
}

//! \brief servEyeJit::calculateEyeMap
void servEyeJit::calculate()
{
    loop = 1;
    if(dispEye_OnOff)
    {
        LOG_DBG();
        m_peyeTread->start();
    }

    if(m_pJitMeasure->JitTrack_OnOff || m_pJitMeasure->JitHisto_OnOff)
    {
        LOG_DBG();
        m_pJitterThread->start();
    }
}

DsoErr servEyeJit::limitRange(qlonglong& val, qlonglong min, qlonglong max)
{
    if(val > max)
    {
        val = max;
        return ERR_OVER_UPPER_RANGE;
    }
    else if(val < min)
    {
        val = min;
        return ERR_OVER_LOW_RANGE;
    }
    return ERR_NONE;
}

DsoErr servEyeJit::limitRange(int &val, int min, int max)
{
    if(val > max)
    {
        val = max;
        return ERR_OVER_UPPER_RANGE;
    }
    else if(val < min)
    {
        val = min;
        return ERR_OVER_LOW_RANGE;
    }
    return ERR_NONE;
}
