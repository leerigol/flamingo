#ifndef CEYEDISP_H
#define CEYEDISP_H

#include <QVector>
#include <QPoint>
#include "ceyecr.h"
#include "eyelib/eyelib.h"
#include "../../baseclass/dsovert.h"

using namespace eyeLib;
class CEyeMap
{  
public:
    CEyeMap();

    void clockRecovery(DsoWfm &wfmA, CRPara crPara, dsoVert* pVertA);
    void splitter1(CRResult* pCRRes,double sampsPerSym, DsoWfm* pwfmA);

    QVector<QVector<int> >* getEyeMap();
    QVector<QVector<int> >* getEyeStatisScr();
    QMap<int,QVector<QPoint> >* getEyeCurveScreen();

    void insert(QVector<int>& befor,QVector<int>& after);


    void setCurveScreen();

    int m_RowMap;
    int m_ColMap;

    //! map中每两个点之间的时间间隔
    float      m_tInc;
    float      m_yInc;
    int        m_yGnd;

    //! scr中每两个点之间的时间间隔
    float      m_tIncScr;
private:

    void scaletoScr(DsoWfm *pWfm);
    void insert(int beginIndex, int endIndex, DsoPoint* data, int offset, int hstep, int **eye);
    void noInsert(int beginIndex, int endIndex, DsoPoint* data,int **eye);

    CEyeCR* m_pEyeCR;
    int m_sampsScreen;
    int m_curveNum;
    QMap<int,QVector<QPoint> > m_curveScreen;
    QVector<QVector<int> > m_eyeMap;
    QVector<QVector<int> > m_eyeStatisScr;
};

#endif// CEYEDISP_H
