#include <cmath>
#include "cmasktest.h"
#include "ceyemap.h"

MaskNRZReal flexrayMask = {0.24,0.15,0.15,{-0.08,0.00,0.16,0.32,0.4}};
MaskNRZReal signal1 = {0.24,0.15,0.15,{-1.5,-0.4,0.00,0.4,1.5}};

CMaskTest::CMaskTest()
{
    m_testNum = 0;
    m_failNum = 0;
    UI_start = 0;
    UI_end = 0;
}

MaskNRZMap* CMaskTest::getMaskMap()
{
    return &m_maskMap;
}

void CMaskTest::maskRealtoMap(MaskNRZReal mask_real, CEyeMap* eyeMap)
{
    //!
    m_maskMap.X.resize(6);
    m_maskMap.Y.resize(5);

    //! 当打开眼图模板时，眼图的只数被限定为1.5
    float eyeNumScreen = 1.5;

    int   eyeColMap = eyeMap->m_ColMap;

    //!eye_mast中输入为以UI为单位，将以UI为单位转化为以map格为单位
    double pixelsMapPerUI = eyeColMap/eyeNumScreen;
    m_maskMap.X[0] = floor((eyeNumScreen/2 - (1-mask_real.Jitter_max)/2)*pixelsMapPerUI);
    m_maskMap.X[3] = eyeColMap - m_maskMap.X[0];
    m_maskMap.X[1] = m_maskMap.X[0] + 0.5/risePercent*mask_real.Mask_risetime*pixelsMapPerUI;
    m_maskMap.X[2] = m_maskMap.X[3] - 0.5/risePercent*mask_real.Mask_falltime*pixelsMapPerUI;

    UI_start = eyeColMap*(1.0*eyeNumScreen/2-0.5)/eyeNumScreen;
    UI_end = UI_start + pixelsMapPerUI;
    m_maskMap.X[4] = UI_start;
    m_maskMap.X[5] = UI_end;

    for(int i = 0;i < 5;i++)
    {
        m_maskMap.Y[i] = (mask_real.Y[i] - mask_real.Y[0])*v_unit/eyeMap->m_yInc + eyeMap->m_yGnd;
    }
}

void CMaskTest::calcuTestResult(CEyeMap *eyeMap)
{
    maskRealtoMap(signal1,eyeMap);
    int fail = 0;
    int testNum = 0;
    QVector<QVector<int> > *map = eyeMap->getEyeMap();

    int rowMap = eyeMap->m_RowMap;

    for(int i = 0;i < m_maskMap.Y[0];i++) //从1个UI的xia dao shang
    {
        for(int j = UI_start;j < UI_end;j++)
        {
            testNum += map->at(i).at(j);
            fail += map->at(i).at(j);
        }
    }

    for(int i = m_maskMap.Y[4];i < rowMap;i++)
    {
        for(int j = UI_start;j < UI_end;j++)
        {
            testNum += map->at(i).at(j);
            fail += map->at(i).at(j);
        }
    }

    for(int i = m_maskMap.Y[0];i <m_maskMap.Y[4];i++)
    {
        for(int j = UI_start;j < UI_end;j++)
        {
            testNum += map->at(i).at(j);
        }
    }

    double temp_slope = 1.0*(m_maskMap.X[1] - m_maskMap.X[0])/(double)(m_maskMap.Y[2] - m_maskMap.Y[1]);
    int leftStartTemp = 0;
    int rightEndTemp = 0;
    for(int i = m_maskMap.Y[2];i < m_maskMap.Y[3];i++)
    {
        leftStartTemp = round(m_maskMap.X[0] + (i-m_maskMap.Y[2])*temp_slope);
        rightEndTemp = round(m_maskMap.X[3] - (i-m_maskMap.Y[2])*temp_slope);
        for(int j = leftStartTemp;j < rightEndTemp;j++)
        {
            fail += map->at(i).at(j);
        }
    }

    for(int i = m_maskMap.Y[1];i < m_maskMap.Y[2];i++)
    {
        leftStartTemp = round(m_maskMap.X[0] + (m_maskMap.Y[2] - i)*temp_slope);
        rightEndTemp = round(m_maskMap.X[3] - (m_maskMap.Y[2] - i)*temp_slope);
        for(int j = leftStartTemp;j < rightEndTemp;j++)
        {
            fail += map->at(i).at(j);
        }
    }
}
