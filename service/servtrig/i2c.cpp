#include "servtrig.h"
#include "trigcode.h"

IMPLEMENT_POST_DO( TriggerBase, servI2CTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servI2CTrigger::applyLevel),
on_postdo( MSG_TRIGGER_I2C_SCL,           &servI2CTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servI2CTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_I2C_SCL,            &servI2CTrigger::setSCL),
on_get_int      (       MSG_TRIGGER_I2C_SCL,            &servI2CTrigger::getSCL),

on_set_int_int  (       MSG_TRIGGER_I2C_SDA,            &servI2CTrigger::setSDA),
on_get_int      (       MSG_TRIGGER_I2C_SDA,            &servI2CTrigger::getSDA),

on_set_int_int  (       MSG_TRIGGER_I2C_WHEN,           &servI2CTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_I2C_WHEN,           &servI2CTrigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_I2C_ADDRTYPE,       &servI2CTrigger::setAddrBits),
on_get_int      (       MSG_TRIGGER_I2C_ADDRTYPE,       &servI2CTrigger::getAddrBits),

on_set_int_int  (       MSG_TRIGGER_I2C_ADDRDATA_BITS,  &servI2CTrigger::setAddrBits),
on_get_int      (       MSG_TRIGGER_I2C_ADDRDATA_BITS,  &servI2CTrigger::getAddrBits),

on_set_int_int  (       MSG_TRIGGER_I2C_ADDRESS,        &servI2CTrigger::setAddress),
on_get_int      (       MSG_TRIGGER_I2C_ADDRESS,        &servI2CTrigger::getAddress),

on_set_int_int  (       MSG_TRIGGER_I2C_DIRECTION,      &servI2CTrigger::setDir),
on_get_int      (       MSG_TRIGGER_I2C_DIRECTION,      &servI2CTrigger::getDir),

on_set_int_int  (       MSG_TRIGGER_I2C_BYTELENGTH,     &servI2CTrigger::setByteCount),
on_get_int      (       MSG_TRIGGER_I2C_BYTELENGTH,     &servI2CTrigger::getByteCount),

on_set_int_ll  (        MSG_TRIGGER_I2C_DATA,           &servI2CTrigger::setData),
on_get_ll      (        MSG_TRIGGER_I2C_DATA,           &servI2CTrigger::getData),

on_set_int_int  (       MSG_TRIGGER_CODE,               &servI2CTrigger::setValue),
on_get_int      (       MSG_TRIGGER_CODE,               &servI2CTrigger::getValue),

on_set_int_int (       MSG_TRIGGER_CODE_ALL,            &servI2CTrigger::setAll),

on_set_int_int  (       MSG_TRIGGER_I2C_CURRBIT,        &servI2CTrigger::setCurrBit),
on_get_int      (       MSG_TRIGGER_I2C_CURRBIT,        &servI2CTrigger::getCurrBit),

on_get_void_argo(       MSG_TRIGGER_SET_CODE,           &servI2CTrigger::getCode),

on_set_int_pointer(     CMD_TRIGGER_I2S_CFG,            &servI2CTrigger::setCfg),
on_get_pointer    (     CMD_TRIGGER_I2S_CFG,            &servI2CTrigger::getCfg),

//!scpi
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_CLK,      &servI2CTrigger::setLevelClk ),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_CLK,      &servI2CTrigger::getLevelClk ),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_DATA,     &servI2CTrigger::setLevelData),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_DATA,     &servI2CTrigger::getLevelData),

on_set_void_void(      CMD_SERVICE_RST,                 &servI2CTrigger::rst ),
on_set_int_void  (     CMD_SERVICE_STARTUP,             &servI2CTrigger::startup ),
on_set_int_int  (      CMD_SERVICE_ACTIVE,              &servI2CTrigger::setActive ),
end_of_entry()

static const int I2C_ADDR_7BIT=0;
static const int I2C_ADDR_8BIT=1;
static const int I2C_ADDR_10BIT=2;

static const int I2C_ADDR_7BIT_MAX=127;
static const int I2C_ADDR_8BIT_MAX=255;
static const int I2C_ADDR_10BIT_MAX=1023;

#define IIC_BYT_COUNT_MAX 5
#define IIC_BYT_COUNT_MIN 1

servI2CTrigger::servI2CTrigger(QString name)
    : TriggerBase(name, Trigger_I2C, E_SERVICE_ID_TRIG_I2C)
{
    serviceExecutor::baseInit();
}

DsoErr servI2CTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servI2CTrigger::getChSlope(Chan ch)
{
    if( (ch == getSDA()) && (trig_i2c_end == getWhen()) )
    {
        return Trigger_Edge_Rising;
    }
    else
    {
        return Trigger_Edge_Falling;
    }
}

DsoErr servI2CTrigger::applyLevel()
{
    DsoErr err = ERR_NONE;
    err = applyLevelEngine((Chan)getSCL());
    if(err != ERR_NONE) return err;

    err = applyLevelEngine((Chan)getSDA());
    return err;
}


int servI2CTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);

    stream.write(QStringLiteral("m_currBit"),   m_currBit);

    cfg_i2c_serialOut(stream, ver);

    return 0;
}
int servI2CTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("m_currBit"),   m_currBit);

        cfg_i2c_serialIn(stream, ver);
    }
    else
    {
        rst();
    }
    return 0;
}

void servI2CTrigger::rst()
{
    cfg_i2c_init();

    m_currBit  = 0;
    m_bytCount = 1;
    m_Data     = 0;

    TriggerBase::rst();

    async(MSG_TRIGGER_I2C_WHEN, getWhen() );
}

int servI2CTrigger::startup()
{
    async(MSG_TRIGGER_I2C_WHEN, getWhen() );
    return  TriggerBase::startup();
}

void servI2CTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

/**
 * @brief servI2CTrigger::getWhen
 * @return
 */
int servI2CTrigger::getWhen(void)
{
    return cfg_get_i2c_When();
}

/**
 * @brief servI2CTrigger::setWhen
 * @param s
 */
int servI2CTrigger::setWhen(int s )
{
    if(s == trig_i2c_data)
    {
        mUiAttr.setVisible(MSG_TRIGGER_I2C_DIRECTION, false);
        mUiAttr.setVisible(MSG_TRIGGER_I2C_ADDRESS,   false);
    }
    else
    {
        mUiAttr.setVisible(MSG_TRIGGER_I2C_DIRECTION, true);
        mUiAttr.setVisible(MSG_TRIGGER_I2C_ADDRESS,   true);
    }

    return cfg_set_i2c_When(s);
}

int servI2CTrigger::getDir()
{
    return cfg_get_i2c_Dir();
}

int servI2CTrigger::setDir(int b)
{
    return cfg_set_i2c_Dir(b);
}

/**
 * @brief servI2CTrigger::getAddrBits
 * @return
 */
int servI2CTrigger::getAddrBits( void )
{
    return cfg_get_i2c_AddrBits();
}

/**
 * @brief servI2CTrigger::setAddrBits
 * @param a
 * @return
 */
int servI2CTrigger::setAddrBits(int a)
{
    int max = 0;
    if (trig_i2c_addr7 == a)
    {
      max = 127;
    }
    else if (trig_i2c_addr8 == a)
    {
      max = 255;
    }
    else if (trig_i2c_addr10 == a)
    {
      max = 1023;
    }
    else
    {
      max = 127;
    }

    mUiAttr.setMaxVal(MSG_TRIGGER_I2C_ADDRESS, max);
    async(MSG_TRIGGER_I2C_ADDRESS, qMin(cfg_get_i2c_Address(), max));

    return cfg_set_i2c_AddrBits(a);
}

/**
 * @brief servI2CTrigger::getAddress
 * @return
 */
int servI2CTrigger::getAddress( void )
{
    int addr = cfg_get_i2c_Address();

    setuiMinVal(0);
    setuiZVal( 0 );

    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    setuiOutStr(QString("%1(0x%2)").arg(addr).arg(addr,0,16) );

    return addr;
}

/**
 * @brief servI2CTrigger::setAddress
 * @param a
 * @return
 */
int servI2CTrigger::setAddress(int a)
{
    return cfg_set_i2c_Address(a);
}

/**
 * @brief servI2CTrigger::getValue
 * @return
 */
int servI2CTrigger::getValue(void)
{
    int value = trigCode::getBitValue(cfg_get_i2c_Data_Min(),
                                      cfg_get_i2c_Data_Mask(),
                                      m_currBit,
                                      m_bytCount);
    return value;
}

/**
 * @brief servI2CTrigger::setValue
 * @param s
 * @return
 */
int servI2CTrigger::setValue(int s )
{
    trigCode::setBitValue(cfg_get_i2c_Data_Min(),
                          cfg_get_i2c_Data_Mask(),
                          m_currBit,
                          s,
                          m_bytCount);

    trigCode::setBitValue(cfg_get_i2c_Data_Max(),
                          cfg_get_i2c_Data_Mask(),
                          m_currBit,
                          s,
                          m_bytCount);
    return apply();
}

/**
 * @brief servI2CTrigger::setAll
 * @return
 */
int servI2CTrigger::setAll(int value )
{
    trigCode::setAllValue(   cfg_get_i2c_Data_Min(),
                             cfg_get_i2c_Data_Mask(),
                             m_currBit,
                             value,
                             m_bytCount);

    trigCode::setAllValue(   cfg_get_i2c_Data_Max(),
                             cfg_get_i2c_Data_Mask(),
                             m_currBit,
                             value,
                             m_bytCount);
    return apply();
}

/**
 * @brief servI2CTrigger::getByteCount
 * @return
 */
int servI2CTrigger::getByteCount()
{
    setuiZVal(  IIC_BYT_COUNT_MIN );
    setuiMaxVal(IIC_BYT_COUNT_MAX);
    setuiMinVal(IIC_BYT_COUNT_MIN);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return m_bytCount;
}

/**
 * @brief servI2CTrigger::setByteCount
 * @param a
 * @return
 */
int servI2CTrigger::setByteCount(int byt)
{
    m_bytCount = byt;
    return cfg_set_i2c_data_width(byt);
}

int servI2CTrigger::getSCL()
{
    return cfg_get_i2c_SCL();
}

int servI2CTrigger::setSCL(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        TriggerBase::setSourceEN(src);
        TriggerBase::setSourceEN(cfg_get_i2c_SDA());

        return cfg_set_i2c_SCL(src);
    }
}

int servI2CTrigger::getSDA()
{
    return cfg_get_i2c_SDA();
}

int servI2CTrigger::setSDA(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        TriggerBase::setSourceEN(src);
        TriggerBase::setSourceEN(cfg_get_i2c_SCL());

        return cfg_set_i2c_SDA(src);
    }
}


/**
 * @brief servI2CTrigger::getCurrBit
 * @return
 */
int servI2CTrigger::getCurrBit()
{
    mUiAttr.setZVal(MSG_TRIGGER_I2C_CURRBIT  , TRIG_CURRBIT_Z_VALUE);
    return m_currBit;
}


/**
 * @brief servI2CTrigger::setCurrBit
 * @param a
 * @return
 */
int servI2CTrigger::setCurrBit(int bit)
{
    //!按下 切换当前位的值
    if(TRIG_CURRBIT_Z_VALUE == bit)
    {
        int    curValue = getValue();
        bool   binHex = true;

        if( m_currBit >= getBitMax() )
        {
            binHex = false;
        }
        return setValue(trigCode::get_z_value(curValue,binHex));
    }
    else //!旋转 切换当前位置
    {
        int b = bit  + m_currBit;
        b = qMax(b, 0);
        b = qMin(b, getBitByteMax()-1 );

        m_currBit = b;
        return ERR_NONE;
    }
}

/**
 * @brief servI2CTrigger::setData
 * @param data
 * @return
 */
int servI2CTrigger::setData(qint64 data)
{
    qint64 maxData = 0;//qxl 2018.2.1
    qint64 tranData = 1;
    for(int i=0; i<m_bytCount*8; i++)
    {
        maxData = maxData|(tranData<<i);
    }

    if(maxData <= data)
    {
        trigCode::setAllValue(   cfg_get_i2c_Data_Min(),
                                 cfg_get_i2c_Data_Mask(),
                                 m_currBit,
                                 1,
                                 m_bytCount);
    }
    else
    {
        for(int i=m_bytCount*8-1; i>=0; i--)
        {
            int codeBit = 0;
            codeBit = data&0x01;
            trigCode::setBitValue(cfg_get_i2c_Data_Min(),
                                  cfg_get_i2c_Data_Mask(),                                i,
                                  codeBit,
                                  m_bytCount);
            data = data>>1;
        }
    }

    setuiChange( MSG_TRIGGER_CODE);

    return ERR_NONE;
}

/**
 * @brief servI2CTrigger::getData
 * @return
 */
qint64 servI2CTrigger::getData()
{
    qint64 data = 0;
    CArgument arg;
    getCode(arg);
    for(int i=0; i<arg.size(); i++)
    {
        int num=0;
        arg[i].getVal(num);
        if(Trigger_Code_X == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_L == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_H == num)
        {
            data = (data<<1)+1;
        }
    }

    return data;
}

/**
 * @brief servI2CTrigger::getCode
 * @param arg
 * @return
 */
void servI2CTrigger::getCode(CArgument &arg)
{
    arg.clear();
    for(int i = 0; i < getBitMax(); i++)
    {
        int value = Trigger_Code_X;
        value = trigCode::getBitValue(cfg_get_i2c_Data_Min(),
                                      cfg_get_i2c_Data_Mask(),
                                      i,
                                      m_bytCount);
        arg.append(value);
    }
}

int servI2CTrigger::getBitMax()
{
    return m_bytCount*8;
}

int servI2CTrigger::getBitByteMax()
{
    return (m_bytCount)*(8+2);
}

DsoErr servI2CTrigger::setCfg(pointer p)
{
    DsoErr err = ERR_NONE;
    err = cfg_i2c_setCfg(p);
    updateAllUi();
    return err;
}

pointer servI2CTrigger::getCfg()
{
    return cfg_i2c_getCfg();
}

int servI2CTrigger::setLevelClk(qint64 l)
{
    Chan ch        = (Chan)cfg_get_i2c_SCL();
    m_pCurrSource  = m_pSources[ch];
    return serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL,
                                  (qint64)(l/getProbeRatio(ch)) );
}

qint64 servI2CTrigger::getLevelClk()
{
    Chan   ch        = (Chan)cfg_get_i2c_SCL();
    qint64 l         = getSourceObj(ch)->getLevel(0);

    return l*getProbeRatio(ch);
}

int servI2CTrigger::setLevelData(qint64 l)
{
    Chan ch        = (Chan)cfg_get_i2c_SDA();
    m_pCurrSource  = m_pSources[ch];
    return serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL,
                                  (qint64)(l/getProbeRatio(ch)) );
}

qint64 servI2CTrigger::getLevelData()
{
    Chan   ch        = (Chan)cfg_get_i2c_SDA();
    qint64 l         = getSourceObj(ch)->getLevel(0);

    return l*getProbeRatio(ch);
}


/*! cfg---------------------------------------------------------*/
int servI2cCfg::cfg_i2c_serialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    stream.write(QStringLiteral("when"),       (int)i2cCfg.when());
    stream.write(QStringLiteral("clk"),        (int)i2cCfg.clk());
    stream.write(QStringLiteral("data"),       (int)i2cCfg.data());
    stream.write(QStringLiteral("spec"),       (int)i2cCfg.spec());
    stream.write(QStringLiteral("wr"),         (int)i2cCfg.wr());
    stream.write(QStringLiteral("addrcmp"),    (int)i2cCfg.addrCmp());
    stream.write(QStringLiteral("datacmp"),    (int)i2cCfg.datCmp());
    stream.write(QStringLiteral("width"),      (int)i2cCfg.width());
    stream.write(QStringLiteral("addrmin"),    (qint64)i2cCfg.addrMin());
    stream.write(QStringLiteral("addrmax"),    (qint64)i2cCfg.addrMax());
    stream.write(QStringLiteral("addrmask"),   (qint64)i2cCfg.addrMask());
    return ERR_NONE;
}

int servI2cCfg::cfg_i2c_serialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    int value;
    stream.read(QStringLiteral("when"),     value );
    i2cCfg.setWhen (     (Trigger_I2C_When)value );

    stream.read(QStringLiteral("clk"),      value );
    i2cCfg.setClk (      (Chan)value );

    stream.read(QStringLiteral("data"),     value );
    i2cCfg.setData (     (Chan)value );

    stream.read(QStringLiteral("spec"),     value );
    i2cCfg.setSpec (     (Trigger_I2C_Spec)value );

    stream.read(QStringLiteral("wr"),       value );
    i2cCfg.setWr (       (Trigger_I2C_WR)value );

    stream.read(QStringLiteral("addrcmp"),  value );
    i2cCfg.setAddrCmp (  (Trigger_value_cmp)value );

    stream.read(QStringLiteral("datacmp"),  value );
    i2cCfg.setDatCmp (   (Trigger_value_cmp)value );

    stream.read(QStringLiteral("width"),    value );
    i2cCfg.setWidth (    (Trigger_I2C_Width)value );

    qint64 addrValue;
    stream.read(QStringLiteral("addrmin"),  addrValue );
    i2cCfg.setAddrMin (  addrValue );
    stream.read(QStringLiteral("addrmax"),  addrValue );
    i2cCfg.setAddrMax (  addrValue );
    stream.read(QStringLiteral("addrmask"), addrValue );
    i2cCfg.setAddrMask ( addrValue );

    i2cCfg.mDataMin  = QByteArray(IIC_BYT_COUNT_MAX, 0);
    i2cCfg.mDataMax  = QByteArray(IIC_BYT_COUNT_MAX, 0);
    i2cCfg.mDataMask = QByteArray(IIC_BYT_COUNT_MAX, 0);
    return ERR_NONE;
}

void servI2cCfg::cfg_i2c_init()
{
    i2cCfg.setClk(chan1);
    i2cCfg.setData(chan2);
    i2cCfg.setSpec(trig_i2c_addr7);
    i2cCfg.setWhen(trig_i2c_start);
    i2cCfg.setWr(trig_i2c_write);

    i2cCfg.setAddrMin(0);
    i2cCfg.setAddrMax(0);
    i2cCfg.setAddrMask(0xffffffff);
    i2cCfg.setAddrCmp(cmp_eq);
    i2cCfg.setDatCmp(cmp_eq);

    i2cCfg.setWidth(trig_i2c_Width_1);

    i2cCfg.mDataMin  = QByteArray(IIC_BYT_COUNT_MAX, 0);
    i2cCfg.mDataMax  = QByteArray(IIC_BYT_COUNT_MAX, 0);
    i2cCfg.mDataMask = QByteArray(IIC_BYT_COUNT_MAX, 0);
}

int servI2cCfg::cfg_get_i2c_When()
{
    return i2cCfg.when();
}

int servI2cCfg::cfg_set_i2c_When(int s )
{
    i2cCfg.setWhen((Trigger_I2C_When)s);
    return apply();
}

int servI2cCfg::cfg_get_i2c_Dir()
{
    return i2cCfg.wr();
}

int servI2cCfg::cfg_set_i2c_Dir(int b)
{
    i2cCfg.setWr((Trigger_I2C_WR)b);
    return apply();
}

int servI2cCfg::cfg_get_i2c_AddrBits()
{
    return i2cCfg.spec();
}

int servI2cCfg::cfg_set_i2c_AddrBits(int a)
{
    if(a == trig_i2c_addr7 )
    {
        if( (i2cCfg.addrMin() > (quint32)I2C_ADDR_7BIT_MAX) )
        {
            i2cCfg.setAddrMin( I2C_ADDR_7BIT_MAX );
        }

        i2cCfg.setAddrMask(0x007F);
    }
    else if(a == trig_i2c_addr10)
    {
        if(i2cCfg.addrMin() > (quint32)I2C_ADDR_10BIT_MAX)
        {
            i2cCfg.setAddrMin( I2C_ADDR_10BIT_MAX );
        }

        i2cCfg.setAddrMask(0x03FF);
    }
    else if(a == trig_i2c_addr8)
    {
        if(i2cCfg.addrMin() > (quint32)I2C_ADDR_8BIT_MAX)
        {
            i2cCfg.setAddrMin( I2C_ADDR_8BIT_MAX );
        }

        i2cCfg.setAddrMask(0x00FF);
    }
    else
    {
        Q_ASSERT(false);
    }

    i2cCfg.setSpec((Trigger_I2C_Spec)a );

    return apply();
}

int servI2cCfg::cfg_get_i2c_Address()
{
    return i2cCfg.addrMin();
}

int servI2cCfg::cfg_set_i2c_Address(int a)
{
    i2cCfg.setAddrMin( a );
    i2cCfg.setAddrMax( a );
    return apply();
}

int servI2cCfg::cfg_get_i2c_data_width()
{
    return i2cCfg.width() + 1;
}

int servI2cCfg::cfg_set_i2c_data_width(int byt)
{
    i2cCfg.setDataMin( i2cCfg.dataMin().left(byt).leftJustified( IIC_BYT_COUNT_MAX,0) );
    i2cCfg.setDataMax( i2cCfg.dataMax().left(byt).leftJustified( IIC_BYT_COUNT_MAX,0) );
    i2cCfg.setDataMask(i2cCfg.dataMask().left(byt).leftJustified(IIC_BYT_COUNT_MAX,0));

    //!底层逻辑里面 0代表1字节
    i2cCfg.setWidth((Trigger_I2C_Width)(byt-1) );
    return apply();
}

QByteArray &servI2cCfg::cfg_get_i2c_Data_Min()
{
    return i2cCfg.mDataMin;
}

QByteArray &servI2cCfg::cfg_get_i2c_Data_Max()
{
    return i2cCfg.mDataMax;
}

QByteArray &servI2cCfg::cfg_get_i2c_Data_Mask()
{
    return i2cCfg.mDataMask;
}


int servI2cCfg::cfg_get_i2c_SCL()
{
    return i2cCfg.clk();
}

int servI2cCfg::cfg_set_i2c_SCL(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        i2cCfg.setClk(Chan(src));
        return apply();
    }
}

int servI2cCfg::cfg_get_i2c_SDA()
{
    return i2cCfg.data();
}

int servI2cCfg::cfg_set_i2c_SDA(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        i2cCfg.setData( Chan(src));
        return apply();
    }
}

int servI2cCfg::cfg_i2c_setCfg(void *p)
{
    TrigIICcfg  *pCfg = static_cast<TrigIICcfg *>(p);
    Q_ASSERT(pCfg != NULL);
    i2cCfg = *pCfg;
    return apply();
}

void *servI2cCfg::cfg_i2c_getCfg()
{
    return &i2cCfg;
}

/*! -----------------------------------------------------------------------------*/
