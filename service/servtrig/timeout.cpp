#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servTimeoutTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servTimeoutTrigger::applyLevel),
on_postdo( MSG_TRIGGER_EDGE_A,            &servTimeoutTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,         &servTimeoutTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servTimeoutTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,          &servTimeoutTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_EDGE_A,             &servTimeoutTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_EDGE_A,             &servTimeoutTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_TIMEOUT_TIME,       &servTimeoutTrigger::setTimeout),
on_get_ll       (       MSG_TRIGGER_TIMEOUT_TIME,       &servTimeoutTrigger::getTimeout),

on_set_void_void(       CMD_SERVICE_RST,                &servTimeoutTrigger::rst ),
on_set_int_void  (       CMD_SERVICE_STARTUP,           &servTimeoutTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servTimeoutTrigger::setActive ),
end_of_entry()


servTimeoutTrigger::servTimeoutTrigger(QString name)
    : TriggerBase(name,Trigger_Timeout,E_SERVICE_ID_TRIG_Timeout)
{
    serviceExecutor::baseInit();
}

DsoErr servTimeoutTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servTimeoutTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource() );
    return getWhen();
}

int servTimeoutTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    stream.write(QStringLiteral("mCh"),       (int)cfg.mCh);
    stream.write(QStringLiteral("mPolarity"), (int)cfg.mPolarity);
    stream.write(QStringLiteral("mWidth"),    cfg.mWidth);

    return 0;
}
int servTimeoutTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value;
        stream.read(QStringLiteral("mCh"),       value);
        cfg.mCh = (Chan)value;
        stream.read(QStringLiteral("mPolarity"), value);
        cfg.mPolarity = (TriggerPulsePolarity)value;

        stream.read(QStringLiteral("mWidth"),    cfg.mWidth);
    }
    else
    {
        rst();
    }
    return 0;
}
void servTimeoutTrigger::rst()
{
    TriggerBase::rst();
    cfg.setCh((Chan)getSource());
    cfg.setPolarity(Trigger_pulse_positive);
    cfg.setWidth(G_s64LowerDefault);
}
int servTimeoutTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servTimeoutTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

/**
 * @brief servTimeoutTrigger::getWhen
 * @return
 */
int servTimeoutTrigger::getWhen(void)
{
    return cfg.polarity();
}

/**
 * @brief servTimeoutTrigger::setWhen
 * @param s
 */
int servTimeoutTrigger::setWhen(int s )
{
    cfg.setPolarity( (TriggerPulsePolarity)s);
    return apply();
}

int servTimeoutTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        cfg.setCh(Chan(src));
        TriggerBase::setSource(src);
        return apply();
    }
}

int servTimeoutTrigger::setLevel(qint64 level)
{
    DsoErr err = ERR_NONE;
    err = m_pCurrSource->setLevel(Trigger_Level_High,level);
    if(ERR_NONE != err) return err;

    err = m_pCurrSource->setLevel(Trigger_Level_Low, level);
    if(ERR_NONE != err) return err;

    return applyLevel();
}

qint64 servTimeoutTrigger::getTimeout( void )
{
    //! update attr
    setuiStep( getTimeStep(cfg.width() ) );

    setuiRange(time_ns(16),time_s(10), time_ns(16));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return cfg.width();
}

int servTimeoutTrigger::setTimeout(qint64 t)
{
    cfg.setWidth(t);

    return apply();
}



