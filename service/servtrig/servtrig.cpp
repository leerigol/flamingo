
#include "../servch/servch.h"

#include "servtrig.h"

#include "../../com/scpiparse/cscpiparser.h"
#include "../servhori/servhori.h"

/**************************Trigger API For other modules****************************/

IMPLEMENT_CMD( serviceExecutor, servTrig  )
start_of_entry()

on_get_int       (  MSG_TRIGGER_TYPE,              &servTrig::getMode),
on_set_int_int   (  MSG_TRIGGER_TYPE,              &servTrig::setMode),

on_get_int       (   MSG_TRIGGER_SOURCE,            &servTrig::getSource),
on_get_pointer   (   MSG_TRIGGER_SOURCE_PTR,        &servTrig::getSourcePtr),

on_set_int_void  (   CMD_TRIGGER_SET_MSG_ENAB,      &servTrig::setMsgEnable),

//!不带探头比
on_set_int_ll  (   MSG_TRIGGER_LEVEL,              &servTrig::setLevel),
on_get_ll      (   MSG_TRIGGER_LEVEL,              &servTrig::getLevel),
on_set_int_int (   MSG_TRIGGER_LEVEL_Z,            &servTrig::setLevelZ),
//!带探头比
on_set_int_ll  (   CMD_SCPI_TRIGGER_LEVEL,         &servTrig::setScpiLevel),
on_get_ll      (   CMD_SCPI_TRIGGER_LEVEL,         &servTrig::getScpiLevel),

on_set_int_ll   (  MSG_TRIGGER_HOLDOFF,            &servTrig::setTrigHoldoff),
on_get_ll       (  MSG_TRIGGER_HOLDOFF,            &servTrig::getTrigHoldoff),

on_set_int_int  (   CMD_TRIGGER_MODE_KEY,          &servTrig::setTrigModeKey),
on_get_int      (   CMD_TRIGGER_MODE_KEY,          &servTrig::getTrigModeKey),

on_set_int_int  (   MSG_TRIGGER_SWEEP,             &servTrig::setTrigSweep),
on_get_int      (   MSG_TRIGGER_SWEEP,             &servTrig::getTrigSweep),

on_set_int_void  (   MSG_TRIGGER_FORCE,            &servTrig::setForce),

on_set_void_void (  CMD_TRIGGER_SYST_STOP,         &servTrig::spySystemStoped),

on_set_int_void (   MSG_TRIGGER_SINGLE,            &servTrig::setTrigSingle),
on_set_int_void (   MSG_TRIGGER_APPLY_LEVEL,       &servTrig::asyncLevel),
on_set_int_int  (   MSG_CHAN_INVERT,               &servTrig::spyChInvert),

on_set_void_void(   CMD_SERVICE_INIT,              &servTrig::onInit ),
on_set_int_int  (   CMD_SERVICE_ACTIVE,            &servTrig::onActive ),
on_set_int_void(    CMD_SERVICE_STARTUP,           &servTrig::startup ),
on_set_void_void(       CMD_SERVICE_RST,           &servTrig::rst ),

end_of_entry()

QPoint   servTrig::G_sLevelPoint;
/**************************Implementation****************************/
servTrig::servTrig( QString name )
    : serviceExecutor(name, E_SERVICE_ID_TRIG)
{
    serviceExecutor::baseInit();
}

int servTrig::createTriggers(QList<service*> *pService)
{
    service *pServ;

    pServ = new servPulseTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servSlopeTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servVideoTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servPatternTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servDurationTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servTimeoutTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servRuntTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servOverTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servWindowTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servDelayTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servSetupTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servNthTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servRS232Trigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servI2CTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servSPITrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servABTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servEdgeTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servCanTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servLinTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servI2STrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servFlexRayTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new serv1553BTrigger();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    //! create at last
    pServ = new servTrig();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servZoneTrig();
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    return 0;
}

int servTrig::startup()
{
    //!xml
    r_meta::CServMeta meta;
    meta.getMetaVal("trig/levelPoint",G_sLevelPoint);
    //! trig
    setMode((TriggerMode)getMode());
    setTrigSweep(   getTrigSweep() );
    if( getTrigSweep() == Trigger_Sweep_Normal )
    {
        setTrigModeKey( getTrigSweep() );
    }
    else
    {
        mModeKey = getTrigSweep();
    }

    //! update the current trig ui
    updateAllUi( false );

    async(CMD_TRIGGER_SET_MSG_ENAB, 1);
    return 0;
}

void servTrig::onInit()
{
    mUiAttr.setNoCache( MSG_TRIGGER_LEVEL );
}

void servTrig::rst()
{
    CSource::rst();
    //![dba,这里有个bug，暂时屏蔽，否则rst后会切到触发菜单。]
    ssync(MSG_TRIGGER_TYPE, (int)Trigger_Edge);
    async(CMD_TRIGGER_SET_MSG_ENAB, 1);
}

int servTrig::onActive(int id)
{
    id = id;
    serviceExecutor::post(TriggerBase::currTrigger(), CMD_SERVICE_ACTIVE, (int)1 );
    return 0;
}

int servTrig::getSource(void)
{
    CSource* pSource = static_cast<CSource*>(getSourcePtr());
    Q_ASSERT(pSource != NULL);
    return pSource->getChan();
}

void* servTrig::getSourcePtr(void)
{
    void* val = NULL;
    serviceExecutor::query( TriggerBase::currTrigger(), MSG_TRIGGER_SOURCE_PTR, val );
    return val;
}

int servTrig::getMode(void)
{
    return TriggerBase::currTriggerMode();
}

DsoErr servTrig::setMode(TriggerMode mode)
{
    QString servName = TriggerBase::getTriggerName(mode);
    send(servName, CMD_SERVICE_ACTIVE, 1);
    return ERR_NONE;
}


qint64 servTrig::getLevel(void)
{
    TriggerBase* trigger = getCurTrigServPtr();
    qint64 level64 = trigger->getLevel();

    qlonglong step   = trigger->getSourcePtr()->get_nLevelStep();
    qlonglong max    = trigger->getSourcePtr()->get_nLevelMax();
    qlonglong min    = trigger->getSourcePtr()->get_nLevelMin();

    mUiAttr.setBase( MSG_TRIGGER_LEVEL, (float)1.0 );
    mUiAttr.setStep( MSG_TRIGGER_LEVEL,  step );
    mUiAttr.setRange(MSG_TRIGGER_LEVEL,  min, max, (qint64)mv(0));
    return level64;
}

int servTrig::setLevel(qint64 l)
{
    DsoErr err = ERR_NONE;

    TriggerBase* trigger = getCurTrigServPtr();
    err = trigger->setLevel(l);
    if(ERR_NONE != err) return err;

    trigger->dispLevel();
    return ERR_NONE;
}


static bool compHalf(const int &a, const int &b)
{
    return (a<b)?true:false;
}

int servTrig::setLevelZ(int /*z*/)
{
    TriggerBase* pTrigger = getCurTrigServPtr();

    if( pTrigger->getDoubleLevel() )
    {
        int value = pTrigger->getLevelSelect();
        if( (++value) >  Trigger_Level_Double)
        {
            value = Trigger_Level_High;
        }
        return post(pTrigger->getId(), MSG_TRIGGER_LEVELSELECT, value);
    }

    if( pTrigger->getSelfMode() == Trigger_Edge )
    {
        Coupling coup = pTrigger->getCoupling();
        if( (coup == AC) || (coup == LF))
        {
            return async(MSG_TRIGGER_LEVEL, (qlonglong)vv(0));
        }
    }

    //!50% 读取2n+1次峰峰值， 从小到大排序，取中间的值作为50%值, (n=20), 耗时[65us, 1s];
    {
        Chan ch = (Chan)getSource();
        QList<int> halfList;
        halfList.clear();

        for(int i = 0; i <=20; i++)
        {
            halfList << getLevelHalf(ch);
        }
        qSort(halfList.begin(), halfList.end(), compHalf);

        //for(int i = 0; i <halfList.count(); i++){qDebug()<<__FILE__<<__LINE__<<i<<halfList.at(i);}

        qlonglong level = halfList.at(halfList.count()/2);
        return async(MSG_TRIGGER_LEVEL, level);
    }
}

int servTrig::getLevelHalf(Chan ch)
{
    //! only analog has Vpp
    if ( ch >= chan1 && ch <= chan4 )
    {}
    else
    { return 0; }

    qint32  Value = 0;
    queryEngine(qENGINE_TRACE_PEAK, ch-chan1, Value);

    qint16  lw = Value&0xFFFF;
    qint16  hw = Value>>16;
    //qDebug()<<__FILE__<<__LINE__<<lw<<hw;

    dsoVert* pVert;
    pVert = dsoVert::getCH(ch);
    Q_ASSERT( NULL != pVert );

   int levelHalf =  ( vv( (hw+lw)/2 - pVert->getyGnd()) ) * pVert->getyInc();
   return levelHalf;
}

DsoErr servTrig::asyncLevel()
{
    DsoErr err = ERR_NONE;
    TriggerBase* trigger = getCurTrigServPtr();

    err = trigger->initLevelEngine();
    if(ERR_NONE != err)
    {
        return err;
    }

    err = trigger->applyLevel();

    return err;
}

DsoErr servTrig::setTrigModeKey(int key)
{
    mModeKey = key;

    if( Trigger_Sweep_Auto == mModeKey )
    {
        servGui::showInfo(STRING_TRIGGER_AUTO);
    }
    else if( Trigger_Sweep_Normal == mModeKey )
    {
        servGui::showInfo(STRING_TRIGGER_NORMAL);
    }
    else if(Trigger_Sweep_Single == mModeKey)
    {
        servGui::showInfo(STRING_TRIGGER_SINGLE);
    }

    return async(MSG_TRIGGER_SWEEP, mModeKey);
}

int servTrig::getTrigModeKey()
{
    return mModeKey;
}

/**
 * @brief For Key
 * @param sweep
 * @return
 */
DsoErr servTrig::setTrigSweep( int sweep )
{
    TriggerBase::setSweep((TriggerSweep)sweep);
    return postEngine( ENGINE_TRIGGER_SWEEP, (int)sweep );
}

int servTrig::getTrigSweep()
{
    return (int)TriggerBase::getSweep();
}
DsoErr servTrig::setTrigSingle()
{
    return ssync(MSG_TRIGGER_SWEEP, (int)Trigger_Sweep_Single);
}

int servTrig::setForce()
{
    TriggerBase* trigger = getCurTrigServPtr();
    return trigger->setForce();
}

void servTrig::spySystemStoped()
{
    int st = Control_Stop;
    query( serv_name_hori, MSG_HORIZONTAL_RUN, st );
    //qDebug()<<__FILE__<<__FUNCTION__<<__LINE__<<"**********************************"<<st;

    /*! [dba: 2018-04-26] bug:2965 规避，未解决*/
    if(Control_Stop != st)
    {
        return;
    }

    if(TriggerBase::getSweep() == Trigger_Sweep_Single)
    {
        ssync(MSG_TRIGGER_SWEEP, TriggerBase::getSingleLast());
    }
}

struct _ch_invert_trig
{
    QString servName;
    int     chCmd;
    int     slopeCmd;
    int     positive;
    int     negative;
};

static _ch_invert_trig ch_invert_trig[] = {
{serv_name_trigger_edge     , MSG_TRIGGER_SOURCE_LA_EXT_AC, MSG_TRIGGER_EDGE_A,   Trigger_Edge_Rising,      Trigger_Edge_Falling    },
{ serv_name_trigger_pulse   , MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_POLARITY,       true,                     false                   },
{ serv_name_trigger_slope   , MSG_TRIGGER_SOURCE,     MSG_TRIGGER_SLOPE_POLARITY, true,                     false                   },
{ serv_name_trigger_video   , MSG_TRIGGER_SOURCE,     MSG_TRIGGER_POLARITY,       true,                     false                   },
{ serv_name_trigger_pattern , MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_CODE,           Trigger_pat_h,            Trigger_pat_l           },
{ serv_name_trigger_pattern , MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_CODE,           Trigger_pat_rise,         Trigger_pat_rise        },
{ serv_name_trigger_duration, MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_CODE,           Trigger_pat_h,            Trigger_pat_l           },
{ serv_name_trigger_duration, MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_CODE,           Trigger_pat_rise,         Trigger_pat_rise        },
{ serv_name_trigger_timeout , MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_EDGE_A,         Trigger_pulse_positive,   Trigger_pulse_negative  },
{ serv_name_trigger_runt    , MSG_TRIGGER_SOURCE,     MSG_TRIGGER_POLARITY,       true,                     false                   },
{ serv_name_trigger_over    , MSG_TRIGGER_SOURCE,     MSG_TRIGGER_OVER_SLOPE,     Trigger_Edge_Rising,      Trigger_Edge_Falling    },
{ serv_name_trigger_delay   , MSG_TRIGGER_DELAY_SRCA, MSG_TRIGGER_EDGEA,          Trigger_Edge_Rising,      Trigger_Edge_Falling    },
{ serv_name_trigger_delay   , MSG_TRIGGER_DELAY_SRCB, MSG_TRIGGER_EDGEB,          Trigger_Edge_Rising,      Trigger_Edge_Falling    },
{ serv_name_trigger_setup   , MSG_TRIGGER_SETUP_SCL,  MSG_TRIGGER_SLOPE_POLARITY, true,                     false                   },
{ serv_name_trigger_nth     , MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_SLOPE_POLARITY, true,                     false                   },
{ serv_name_trigger_rs232   , MSG_TRIGGER_SOURCE_LA,  MSG_TRIGGER_RS232_POLARITY, true,                     false                   },
{ serv_name_trigger_i2s     , MSG_TRIGGER_IIS_SCLK,   MSG_TRIGGER_IIS_SLOPE,      Trigger_Edge_Rising,      Trigger_Edge_Falling    },
{ serv_name_trigger_spi     , MSG_TRIGGER_SPI_SCL,    MSG_TRIGGER_SLOPE_POLARITY, true,                     false                   },
{ serv_name_trigger_spi     , MSG_TRIGGER_SPI_CS,     MSG_TRIGGER_SPI_CSMODE,     true,                     false                   },
{ serv_name_trigger_ab      , MSG_TRIGGER_SOURCE,     MSG_TRIGGER_EDGE_A,         Trigger_Edge_Rising,      Trigger_Edge_Falling    },
{ serv_name_trigger_ab      , MSG_TRIGGER_SOURCE,     MSG_TRIGGER_EDGE_B,         Trigger_Edge_Rising,      Trigger_Edge_Falling    },
{ serv_name_trigger_ab      , MSG_TRIGGER_SOURCE,     MSG_TRIGGER_POLARITY,       true,                     false                   },
};

DsoErr servTrig::spyChInvert(int chId)
{    
    int ch  = (chId-E_SERVICE_ID_CH1)+chan1;
    int trigCh;
    int slope;

    QString servName;
    int     chCmd;
    int     slopeCmd;
    int     positive;
    int     negative;

    bool b_currTrigChange = false;

    //!改变所有触发与此通道相关的边沿类型。
    for(int i = 0; i < array_count(ch_invert_trig); i++)
    {
        servName = ch_invert_trig[i].servName;
        chCmd    = ch_invert_trig[i].chCmd;
        slopeCmd = ch_invert_trig[i].slopeCmd;
        positive = ch_invert_trig[i].positive;
        negative = ch_invert_trig[i].negative;
        query(servName, chCmd, trigCh);
        if(trigCh == ch)
        {
            query(servName, slopeCmd, slope);
            if(slope == positive)
            {
                post(servName, slopeCmd, negative);
                b_currTrigChange = true;
            }
            else if(slope == negative)
            {
                post(servName, slopeCmd, positive);
                b_currTrigChange = true;
            }
            else
            {/*不做处理*/}

            if(b_currTrigChange && (servName == TriggerBase::currTrigger()) )
            {    //!仅当当前触发的配置发生变化，才给用户提示，其他触发的后台修改不做提示。
                servGui::showInfo(sysGetString(STRING_TRIGGER_INVERT_HINT, "Trigger setting has been changed"));
            }
        }
    }
    return post(TriggerBase::currTrigger(), MSG_CHAN_INVERT, ch);;
}

//!*SCPI*/
qint64 servTrig::getScpiLevel()
{
    TriggerBase* trigger = getCurTrigServPtr();
    Chan ch              = (Chan)trigger->getSource();
    float fRatio         = TriggerBase::getProbeRatio(ch);

    return (trigger->getLevel()*fRatio);
}

int servTrig::setScpiLevel(qint64 l)
{
    TriggerBase* trigger = getCurTrigServPtr();
    Chan ch              = (Chan)trigger->getSource();
    float fRatio         = TriggerBase::getProbeRatio(ch);

    return async(MSG_TRIGGER_LEVEL, (qint64)(l/fRatio));
}

DsoErr servTrig::setTrigHoldoff(qint64 t)
{
    TriggerBase* trigger = getCurTrigServPtr();

    return send(trigger->getName(), MSG_TRIGGER_HOLDOFF, t);
}

qint64 servTrig::getTrigHoldoff()
{
    qlonglong t = 0;
    TriggerBase* trigger = getCurTrigServPtr();
    query(trigger->getName(), MSG_TRIGGER_HOLDOFF, t);

    return t;
}

int servTrig::setMsgEnable()
{
    int  edgeSrc  = chan_none;
    int  AcqMode  = Acquire_YT;

    query(serv_name_trigger_edge, MSG_TRIGGER_SOURCE_LA_EXT_AC, edgeSrc);
    query(serv_name_hori, MSG_HOR_TIME_MODE, AcqMode);

    //! xy:set trig mode = edge, disEnable edge service all msg
    QStringList servList;
    servList << serv_name_trigger_edge;

    mUiAttr.setEnable(MSG_TRIGGER_LEVEL,    true);
    mUiAttr.setEnable(MSG_TRIGGER_SWEEP,    true);
    mUiAttr.setEnable(MSG_TRIGGER_FORCE,    true);
    mUiAttr.setEnable(MSG_TRIGGER_SINGLE,   true);
    mUiAttr.setEnable(CMD_TRIGGER_MODE_KEY, true);
    mUiAttr.setEnable(CMD_TRIGGER_MODE_KEY, true);
    mUiAttr.setEnable(CMD_SERVICE_ACTIVE,   true);
    //setUiEnables( servList, true );

    if (edgeSrc == acline)
    {
        mUiAttr.setEnable(MSG_TRIGGER_LEVEL, false);
    }

    if((AcqMode == Acquire_XY) || (AcqMode == Acquire_ROLL))
    { 
        mUiAttr.setEnable(MSG_TRIGGER_LEVEL,    false);
        mUiAttr.setEnable(MSG_TRIGGER_SWEEP,    false);
        mUiAttr.setEnable(MSG_TRIGGER_FORCE,    false);
        mUiAttr.setEnable(MSG_TRIGGER_SINGLE,   false);
        mUiAttr.setEnable(CMD_TRIGGER_MODE_KEY, false);
        mUiAttr.setEnable(CMD_SERVICE_ACTIVE,   false);

        //setUiEnables( servList, false );
    }

    return ERR_NONE;
}


TriggerBase *servTrig::getCurTrigServPtr()
{
    TriggerBase* pTrig = (TriggerBase*)findService(TriggerBase::currTrigger());
    Q_ASSERT(pTrig != NULL);
    return pTrig;
}

void servTrig::registerSpy()
{
    //spyOn( serv_name_hori, MSG_HORIZONTAL_RUN);
    spyOn( serv_name_hori, servHori::cmd_system_stoped, CMD_TRIGGER_SYST_STOP);

    spyOn( serv_name_ch1, MSG_CHAN_SCALE,  MSG_TRIGGER_APPLY_LEVEL );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE,  MSG_TRIGGER_APPLY_LEVEL );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE,  MSG_TRIGGER_APPLY_LEVEL );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE,  MSG_TRIGGER_APPLY_LEVEL );

    spyOn( serv_name_ch1, MSG_CHAN_OFFSET, MSG_TRIGGER_APPLY_LEVEL );
    spyOn( serv_name_ch2, MSG_CHAN_OFFSET, MSG_TRIGGER_APPLY_LEVEL );
    spyOn( serv_name_ch3, MSG_CHAN_OFFSET, MSG_TRIGGER_APPLY_LEVEL );
    spyOn( serv_name_ch4, MSG_CHAN_OFFSET, MSG_TRIGGER_APPLY_LEVEL );

    spyOn( serv_name_ch1, MSG_CHAN_INVERT, e_spy_post,spy_serv_id  );
    spyOn( serv_name_ch2, MSG_CHAN_INVERT, e_spy_post,spy_serv_id  );
    spyOn( serv_name_ch3, MSG_CHAN_INVERT, e_spy_post,spy_serv_id  );
    spyOn( serv_name_ch4, MSG_CHAN_INVERT, e_spy_post,spy_serv_id  );

    spyOn( serv_name_hori,         MSG_HOR_TIME_MODE,            CMD_TRIGGER_SET_MSG_ENAB);
    spyOn( serv_name_trigger_edge, MSG_TRIGGER_SOURCE_LA_EXT_AC, CMD_TRIGGER_SET_MSG_ENAB);
}
