#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servSetupTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servSetupTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SLOPE_POLARITY,    &servSetupTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SETUP_SCL,         &servSetupTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SETUP_SDA,         &servSetupTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servSetupTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SETUP_SCL,          &servSetupTrigger::setSCL),
on_get_int      (       MSG_TRIGGER_SETUP_SCL,          &servSetupTrigger::getSCL),

on_set_int_bool (       MSG_TRIGGER_SLOPE_POLARITY,     &servSetupTrigger::setSCLEdge),
on_get_bool     (       MSG_TRIGGER_SLOPE_POLARITY,     &servSetupTrigger::getSCLEdge),

on_set_int_int  (       MSG_TRIGGER_SETUP_SDA,          &servSetupTrigger::setSDA),
on_get_int      (       MSG_TRIGGER_SETUP_SDA,          &servSetupTrigger::getSDA),

on_set_int_bool (       MSG_TRIGGER_SETUP_DATA,         &servSetupTrigger::setSDAEdge),
on_get_bool     (       MSG_TRIGGER_SETUP_DATA,         &servSetupTrigger::getSDAEdge),

on_set_int_int  (       MSG_TRIGGER_SETUP_WHEN,         &servSetupTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_SETUP_WHEN,         &servSetupTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_SETUP_TIME,         &servSetupTrigger::setSetupTime),
on_get_ll       (       MSG_TRIGGER_SETUP_TIME,         &servSetupTrigger::getSetupTime),

on_set_int_ll   (       MSG_TRIGGER_HOLD_TIME,          &servSetupTrigger::setHoldTime),
on_get_ll       (       MSG_TRIGGER_HOLD_TIME,          &servSetupTrigger::getHoldTime),


//!scpi
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_L,        &servSetupTrigger::setLevelSCL),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_L,        &servSetupTrigger::getLevelSCL),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_H,        &servSetupTrigger::setLevelSDA),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_H,        &servSetupTrigger::getLevelSDA),


on_set_void_void(       CMD_SERVICE_RST,                &servSetupTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servSetupTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servSetupTrigger::setActive ),
end_of_entry()


servSetupTrigger::servSetupTrigger(QString name)
    : TriggerBase(name,Trigger_Setup, E_SERVICE_ID_TRIG_SetupHold)
{
    serviceExecutor::baseInit();
}

DsoErr servSetupTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servSetupTrigger::getChSlope(Chan ch)
{
    if(ch == getSCL())
    {
        return getSCLEdge();
    }

    if(ch == getSDA())
    {
        if(Trigger_SH_setup == getWhen())
        {
            return Trigger_Edge_Rising;
        }
        else
        {
            return Trigger_Edge_Falling;
        }
    }

    Q_ASSERT( false );
}

DsoErr servSetupTrigger::applyLevel()
{
    DsoErr err = ERR_NONE;
    err = applyLevelEngine((Chan)getSCL());
    if(err != ERR_NONE) return err;

    err = applyLevelEngine((Chan)getSDA());
    return err;
}


int servSetupTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    stream.write(QStringLiteral("m_nWhen"),         (int)cfg.event() );
    stream.write(QStringLiteral("m_nSCL"),          (int)cfg.clk()   );
    stream.write(QStringLiteral("m_nSDA"),          (int)cfg.dat()   );
    stream.write(QStringLiteral("m_Slope"),         (int)cfg.slope() );
    stream.write(QStringLiteral("m_s64HoldTime"),   cfg.holdTime()   );
    stream.write(QStringLiteral("m_s64SetupTime"),  cfg.setupTime()  );

    return 0;
}
int servSetupTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value;
        stream.read(QStringLiteral("m_nWhen"),         value);
        cfg.setEvent((SHEvent)value);

        stream.read(QStringLiteral("m_nSCL"),          value);
        cfg.setClk((Chan)value);

        stream.read(QStringLiteral("m_nSDA"),          value);
        cfg.setDat((Chan)value);

        stream.read(QStringLiteral("m_Slope"),          value);
        cfg.setSlope((EdgeSlope)value);

        stream.read(QStringLiteral("m_s64HoldTime"),   cfg.mHoldTime);
        stream.read(QStringLiteral("m_s64SetupTime"),  cfg.mSetupTime);
    }
    else
    {
        rst();
    }
    return 0;
}
void servSetupTrigger::rst()
{
    TriggerBase::rst();
    cfg.setClk((Chan)getSource());
    cfg.setDat(chan2);
    cfg.setEvent(Trigger_SH_setup);
    cfg.setHoldTime(G_s64LowerDefault);
    cfg.setSetupTime(G_s64LowerDefault);
    cfg.setSlope(Trigger_Edge_Rising);
    cfg.setDatPolarity(Trigger_pulse_positive);
}
int servSetupTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servSetupTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}


/**
 * @brief servSetupTrigger::getWhen
 * @return int
 */
int servSetupTrigger::getWhen(void)
{
    return cfg.event();
}

/**
 * @brief servSetupTrigger::setWhen
 * @param s
 */
int servSetupTrigger::setWhen(int s )
{
    cfg.setEvent( (SHEvent)s );
    return apply();
}


/**
 * @brief servSetupTrigger::getSetupTime
 * @return
 */
qint64 servSetupTrigger::getSetupTime(void)
{
    //! update attr
    setuiStep( getTimeStep(cfg.setupTime()) );
    setuiRange(time_ns(8),time_s(1), time_ns(8));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );
    return cfg.setupTime();
}

/**
 * @brief servSetupTrigger::setSetupTime
 * @param t
 * @return
 */
int servSetupTrigger::setSetupTime(qint64 t)
{
    cfg.setSetupTime(t);
    return apply();
}

/**
 * @brief servSetupTrigger::getHoldTime
 * @return
 */
qint64 servSetupTrigger::getHoldTime(void)
{
    //! update attr
    setuiStep( getTimeStep(cfg.holdTime()) );
    setuiRange(time_ns(8),time_s(1),time_ns(8));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return cfg.holdTime();
}

/**
 * @brief servSetupTrigger::setHoldTime
 * @param t
 * @return
 */
int servSetupTrigger::setHoldTime(qint64 t)
{
    cfg.setHoldTime(t);
    return apply();
}

int servSetupTrigger::getSCL()
{
    return cfg.clk();
}

int servSetupTrigger::setSCL(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        cfg.setClk((Chan)src);
        TriggerBase::setSource(src);
        TriggerBase::setSourceEN(cfg.clk());
        TriggerBase::setSourceEN(cfg.dat());
        return apply();
    }
}

int servSetupTrigger::getSCLEdge()
{
    return cfg.slope();
}

int servSetupTrigger::setSCLEdge(int e)
{
    cfg.setSlope( (EdgeSlope)e);
    return apply();
}

int servSetupTrigger::getSDA()
{
    return cfg.dat();
}

int servSetupTrigger::setSDA(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        cfg.setDat((Chan)src);
        TriggerBase::setSource(src);
        TriggerBase::setSourceEN(cfg.clk());
        TriggerBase::setSourceEN(cfg.dat());
        return apply();
    }
}

int servSetupTrigger::getSDAEdge()
{
    return cfg.datPolarity();
}

int servSetupTrigger::setSDAEdge(int pola)
{
    cfg.setDatPolarity((TriggerPulsePolarity)pola);
    return apply();
}


//!----------------scpi-----------------------------------//
int servSetupTrigger::setLevelSCL(qint64 l)
{
    ssync(MSG_TRIGGER_SETUP_SCL, getSCL() );
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servSetupTrigger::getLevelSCL()
{
    qint64 l = 0;
    ssync(MSG_TRIGGER_SETUP_SCL, getSCL() );
    serviceExecutor::query( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return l;
}

int servSetupTrigger::setLevelSDA(qint64 l)
{
    ssync(MSG_TRIGGER_SETUP_SDA, getSDA() );
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servSetupTrigger::getLevelSDA()
{
    qint64 l = 0;
    ssync(MSG_TRIGGER_SETUP_SDA, getSDA() );
    serviceExecutor::query( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return l;
}


