#include "servtrig.h"
#include "../servdisplay/servdisplay.h"
#include "../servvertmgr/servvertmgr.h"

IMPLEMENT_POST_DO( TriggerBase, servVideoTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servVideoTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE,            &servVideoTrigger::applyLevel),
on_postdo( MSG_TRIGGER_POLARITY,          &servVideoTrigger::applyLevel),
on_postdo( CMD_SERVICE_ACTIVE,            &servVideoTrigger::spyScale140),
on_postdo( MSG_TRIGGER_TYPE,              &servVideoTrigger::spyScale140),

end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servVideoTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE,             &servVideoTrigger::setSource),

on_set_int_bool (       MSG_TRIGGER_POLARITY,           &servVideoTrigger::setPolarity),
on_get_bool     (       MSG_TRIGGER_POLARITY,           &servVideoTrigger::getPolarity),

on_set_int_int  (       MSG_TRIGGER_VIDEO_STANDARD,     &servVideoTrigger::setStardard),
on_get_int      (       MSG_TRIGGER_VIDEO_STANDARD,     &servVideoTrigger::getStardard),

on_set_int_int  (       MSG_TRIGGER_VIDEO_SYNC,         &servVideoTrigger::setSync),
on_get_int      (       MSG_TRIGGER_VIDEO_SYNC,         &servVideoTrigger::getSync),

on_set_int_int  (       MSG_TRIGGER_VIDEO_LINENUM,      &servVideoTrigger::setLineNum),
on_get_int      (       MSG_TRIGGER_VIDEO_LINENUM,      &servVideoTrigger::getLineNum),

on_set_int_void (       MSG_CHAN_SCALE,                 &servVideoTrigger::spyScale140),

on_set_void_void(       CMD_SERVICE_RST,                &servVideoTrigger::rst ),
on_set_int_void  (       CMD_SERVICE_STARTUP,           &servVideoTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servVideoTrigger::setActive ),
end_of_entry()


servVideoTrigger::servVideoTrigger(QString name)
    : TriggerBase(name,Trigger_Video, E_SERVICE_ID_TRIG_Video)
{
    serviceExecutor::baseInit();
}

DsoErr servVideoTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servVideoTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    stream.write(QStringLiteral("mCh"),               (int)cfg.ch());
    stream.write(QStringLiteral("m_bPolarity"),      (int)cfg.polarity());
    stream.write(QStringLiteral("m_nStardard"),      (int)cfg.fromat());
    stream.write(QStringLiteral("m_nSync"),          (int)cfg.sync());
    stream.write(QStringLiteral("m_nLineNum"),       cfg.lineN());

    return 0;
}
int servVideoTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value;
        stream.read(QStringLiteral("mCh"),              value);
        cfg.setCh((Chan)value);

        stream.read(QStringLiteral("m_bPolarity"),      value);
        cfg.setPolarity((TriggerPulsePolarity)value);

        stream.read(QStringLiteral("m_nStardard"),      value);
        cfg.setFromat((Trigger_Video_Format)value);

        stream.read(QStringLiteral("m_nSync"),          value);
        cfg.setSync((Trigger_Video_Sync)value);

        stream.read(QStringLiteral("m_nLineNum"),       value);
        cfg.setLineN(value);
    }
    else
    {
        rst();
    }
    return 0;
}
void servVideoTrigger::rst()
{
    TriggerBase::rst();
    cfg.setCh((Chan)getSource());
    cfg.setFromat(Video_Stardard_NTSC);
    cfg.setLineN(1);
    cfg.setPolarity(Trigger_pulse_positive);
    cfg.setSync(trig_video_any_line);

    m_dispGrids = 0;

    async(MSG_TRIGGER_VIDEO_STANDARD, getStardard());
}

int servVideoTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    for(int i=d0; i<all_trig_src; i++)
    {
        mUiAttr.setVisible( MSG_TRIGGER_SOURCE, i, false );
    }
    async(MSG_TRIGGER_VIDEO_STANDARD, getStardard());
    return  TriggerBase::startup();
}


/**
 * @brief servVideoTrigger::getPolarity
 * @return
 */
bool servVideoTrigger::getPolarity(void)
{
    return cfg.polarity();
}

/**
 * @brief servVideoTrigger::setPolarity
 * @param b
 */
int servVideoTrigger::setPolarity(bool b)
{
    cfg.setPolarity((TriggerPulsePolarity)b);
    return apply();
}

/**
 * @brief servVideoTrigger::getStardard
 * @return
 */
int servVideoTrigger::getStardard( void )
{
    return (int)cfg.fromat();
}

/**
 * @brief servVideoTrigger::setStardard
 * @param s
 * @return
 */
int servVideoTrigger::setStardard(int s)
{
    if(s >= Video_Stardard_480P )
    {
        mUiAttr.setVisible( MSG_TRIGGER_VIDEO_SYNC, trig_video_odd, false );
        mUiAttr.setVisible( MSG_TRIGGER_VIDEO_SYNC, trig_video_even,false );

        if(cfg.sync() >= trig_video_odd)
        {
            cfg.setSync(trig_video_any_line);
            setuiChange(MSG_TRIGGER_VIDEO_SYNC);
        }
    }
    else
    {
        mUiAttr.setVisible( MSG_TRIGGER_VIDEO_SYNC, trig_video_odd, true );
        mUiAttr.setVisible( MSG_TRIGGER_VIDEO_SYNC, trig_video_even,true );
    }

    cfg.setFromat((Trigger_Video_Format)s);

    async(MSG_TRIGGER_VIDEO_LINENUM, cfg.lineN());

    return apply();
}


struct sync_odd_even
{
    Trigger_Video_Format format;
    Trigger_Video_Sync   sync;
    int                  row;
};
sync_odd_even _sync_odd_even[] =
{
    {Video_Stardard_NTSC, trig_video_odd , 4   },
    {Video_Stardard_NTSC, trig_video_even, 267 },
    {Video_Stardard_PAL,  trig_video_odd , 1   },
    {Video_Stardard_PAL,  trig_video_even, 313 },
};
/**
 * @brief servVideoTrigger::getSync
 * @return int
 */
int servVideoTrigger::getSync(void)
{
    return cfg.sync();
}
/**
 * @brief servVideoTrigger::setSync
 * @param s
 */
int servVideoTrigger::setSync(int s )
{
    for(int  i = 0; i < array_count(_sync_odd_even); i++)
    {
        if(  (cfg.fromat() == _sync_odd_even[i].format)
             &&(cfg.sync() == _sync_odd_even[i].sync) )
        {
            cfg.setLineN(_sync_odd_even[i].row);
        }
    }

    cfg.setSync( (Trigger_Video_Sync)s );
    return apply();
}

int servVideoTrigger::getLineNum( void )
{
    if( cfg.fromat() != Video_Stardard_PAL )
    {
        setuiRange(1,525,1);
    }
    else
    {
        setuiRange(1,625,1);
    }

    return cfg.lineN();
}

int servVideoTrigger::setLineNum(int l)
{
    l = qBound(1, l, 625);

    if( cfg.fromat() != Video_Stardard_PAL )
    {
        l = qBound(1, l, 525);
    }

    cfg.setLineN(l);
    return apply();
}

int servVideoTrigger::setSource(int src)
{
    if( !(src >= chan1  && src <= chan4) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        cfg.setCh(Chan(src));
        TriggerBase::setSource(src);
        return apply();
    }
}

int  servVideoTrigger::spyScale140()
{
#if 0
    qDebug() << "video";
    //by hxh for bug 298
    if( Trigger_Video != getMode())
    {
        return ERR_NONE;
        //return  async( MSG_DISPLAY_GRID, m_dispGrids, serv_name_display );
    }

    int nScale    = 0;
    int dispGrids = 0;
    int nVideoStandard = 0;
    int chan      = chan1;

    serviceExecutor::query( serv_name_vert_mgr, servVertMgr::cmd_active_chan,  chan);

    qDebug() << "chan() = " << chan;
    QString name = QString("chan%1").arg(chan);

    serviceExecutor::query( name,             MSG_CHAN_SCALE_VALUE,  nScale);
    serviceExecutor::query(serv_name_display, MSG_DISPLAY_GRID,      dispGrids);

    if(dispGrids != servDisplay::GRID_IS_IRE)
    {
        m_dispGrids = dispGrids;
    }
   qDebug() << "qDebug() = " << nScale;
   qDebug() << "dispGrids = " << dispGrids;
   qDebug() << "m_dispGrids = " << m_dispGrids;
#if 1
   serviceExecutor::query( serv_name_trigger_video,
                           MSG_TRIGGER_VIDEO_STANDARD,
                           nVideoStandard);
#endif
    if( (nScale == mv(140)) && (nVideoStandard == Video_Stardard_NTSC) && (dispGrids != servDisplay::GRID_IS_IRE) )
    {
        async( MSG_DISPLAY_GRID, servDisplay::GRID_IS_IRE, serv_name_display );
    }
    else if( (nScale != mv(140)) && (dispGrids == servDisplay::GRID_IS_IRE))
    {
        async( MSG_DISPLAY_GRID, m_dispGrids, serv_name_display );
    }
#endif
    return ERR_NONE;
}

void servVideoTrigger::registerSpy()
{
    TriggerBase::registerSpy();
    spyOn( serv_name_ch1, MSG_CHAN_SCALE );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE );

    spyOn( serv_name_trigger, MSG_TRIGGER_TYPE, MSG_CHAN_SCALE);
    spyOn( serv_name_vert_mgr, servVertMgr::cmd_active_chan,  MSG_CHAN_SCALE);
}

int servVideoTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource() );
    return (int)getPolarity();
}
