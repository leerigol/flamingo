#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servWindowTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servWindowTrigger::applyLevel),
on_postdo( MSG_TRIGGER_EDGE_A,            &servWindowTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE,            &servWindowTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servWindowTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE,             &servWindowTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_LEVEL,              &servWindowTrigger::setLevel),
on_get_int      (       MSG_TRIGGER_LEVEL,              &servWindowTrigger::getLevel),

on_set_int_int  (       MSG_TRIGGER_EDGE_A,             &servWindowTrigger::setEdgeType),
on_get_int      (       MSG_TRIGGER_EDGE_A,             &servWindowTrigger::getEdgeType),

on_set_int_int  (       MSG_TRIGGER_WINDOW_POS,         &servWindowTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_WINDOW_POS,         &servWindowTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_WINDOW_TIME,        &servWindowTrigger::setTime),
on_get_ll       (       MSG_TRIGGER_WINDOW_TIME,        &servWindowTrigger::getTime),

on_set_int_int  (       MSG_TRIGGER_LEVELSELECT,        &servWindowTrigger::setLevelSelect),
on_get_int      (       MSG_TRIGGER_LEVELSELECT,        &servWindowTrigger::getLevelSelect),

on_set_void_void(       CMD_SERVICE_RST,                &servWindowTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servWindowTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servWindowTrigger::setActive ),
end_of_entry()


servWindowTrigger::servWindowTrigger(QString name)
    : TriggerBase(name,Trigger_Window, E_SERVICE_ID_TRIG_Window,true)
{
    serviceExecutor::baseInit();
}

DsoErr servWindowTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servWindowTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource() );
    return getEdgeType();
}

int servWindowTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    stream.write(QStringLiteral("m_nWhen"),    m_nWhen);
    stream.write(QStringLiteral("m_s64Time"),  m_s64Time);

    stream.write(QStringLiteral("mCh"),        (int)cfg.mCh);
    stream.write(QStringLiteral("mEvent"),     (int)cfg.mEvent);
    stream.write(QStringLiteral("mCmp"),       (int)cfg.mCmp);
    stream.write(QStringLiteral("mWidthH"),    cfg.mWidthH);
    stream.write(QStringLiteral("mWidthL"),    cfg.mWidthL);

    return 0;
}
int servWindowTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value;
        stream.read(QStringLiteral("m_nWhen"),   m_nWhen);
        stream.read(QStringLiteral("m_s64Time"), m_s64Time);

        stream.read(QStringLiteral("mCh"),       value);
        cfg.mCh = (Chan)value;
        stream.read(QStringLiteral("mEvent"),    value);
        cfg.mEvent = (WindowEvent)value;
        stream.read(QStringLiteral("mCmp"),      value);
        cfg.mCmp = (EMoreThan)value;

        stream.read(QStringLiteral("mWidthH"),    cfg.mWidthH);
        stream.read(QStringLiteral("mWidthL"),    cfg.mWidthL);
    }
    else
    {
        rst();
    }
    return 0;
}
void servWindowTrigger::rst()
{
    TriggerBase::rst();
    m_nWhen         =   Trigger_When_None;
    m_s64Time       =   G_s64LowerDefault;
    m_nLevelSelect  =   Trigger_Level_High;
}
int servWindowTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    for(int i=d0; i<ext; i++)
    {
        mUiAttr.setVisible( MSG_TRIGGER_SOURCE, i, false );
    }
    return  TriggerBase::startup();
}

void servWindowTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}


int servWindowTrigger::getEdgeType(void)
{
    return m_nEdgeType;
}


int servWindowTrigger::setEdgeType(int b)
{
    m_nEdgeType = b;

    return ERR_NONE;
}

/**
 * @brief servWindowTrigger::getWhen
 * @return int
 */
int servWindowTrigger::getWhen(void)
{
    return m_nWhen;
}

/**
 * @brief servWindowTrigger::setWhen
 * @param s
 */
int servWindowTrigger::setWhen(int s )
{
    m_nWhen = s ;
    return ERR_NONE;
}

/**
 * @brief servWindowTrigger::getUpper
 * @return
 */
qint64 servWindowTrigger::getTime(void)
{
    //! update attr
    setuiBase( 1.0/time_s(1) );
    setuiStep( getTimeStep(m_s64Time) );
    setuiZVal( G_s64LowerDefault );

    setuiPostStr( "s" );

    return m_s64Time;
}

/**
 * @brief servWindowTrigger::setTime
 * @param t
 * @return
 */
int servWindowTrigger::setTime(qint64 t)
{
    m_s64Time = t;
    return ERR_NONE;
}

