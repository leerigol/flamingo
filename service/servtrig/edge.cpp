#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servEdgeTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,           &servEdgeTrigger::applyLevel),
on_postdo( MSG_TRIGGER_EDGE_A,           &servEdgeTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA_EXT_AC, &servEdgeTrigger::applyLevel),
on_postdo( MSG_TRIGGER_COUPLING,         &servEdgeTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servEdgeTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA_EXT_AC,   &servEdgeTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_COUPLING,           &servEdgeTrigger::setCoupling),
on_get_int      (       MSG_TRIGGER_COUPLING,           &servEdgeTrigger::getCoupling),

on_set_int_int  (       MSG_TRIGGER_EDGE_A,             &servEdgeTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_EDGE_A,             &servEdgeTrigger::getWhen),

on_set_void_void(       MSG_APP_UTILITY_PROJECT,        &servEdgeTrigger::onProjectMode),

on_set_int_pointer(     CMD_TRIGGER_EDGE_CFG,           &servEdgeTrigger::setCfg),
on_get_pointer    (     CMD_TRIGGER_EDGE_CFG,           &servEdgeTrigger::getCfg),

on_set_void_void(       CMD_SERVICE_RST,                &servEdgeTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servEdgeTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servEdgeTrigger::setActive ),
end_of_entry()

/*!----------------------------------------------------------------------------------------------------------*/
#define ar ar_out
#define ar_pack_e ar_out
int servEdgeCfg::cfg_edge_serialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    //!edge
    ar_pack_e("mCh",    edgeCfg.mCh  );
    ar_pack_e("mSlope", edgeCfg.mSlope );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servEdgeCfg::cfg_edge_serialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    //!edge
    ar_pack_e("mCh",    edgeCfg.mCh  );
    ar_pack_e("mSlope", edgeCfg.mSlope );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servEdgeCfg::cfg_edge_init()
{
    edgeCfg.setSlope(Trigger_Edge_Rising);
    edgeCfg.setChan((Chan)chan1);
}

int servEdgeCfg::cfg_edge_getWhen()
{
    return edgeCfg.getSlope();
}

int servEdgeCfg::cfg_edge_setWhen(int w)
{
    edgeCfg.setSlope((EdgeSlope)w);
    return apply();
}

int servEdgeCfg::cfg_edge_setCh(int ch)
{
    edgeCfg.mCh = Chan(ch);
    return apply();
}

int servEdgeCfg::cfg_edge_getCh()
{
    return edgeCfg.getChan();
}

int servEdgeCfg::cfg_edge_setCfg(void  *p)
{
    TrigEdgeCfg  *pCfg = static_cast<TrigEdgeCfg *>(p);
    Q_ASSERT(pCfg != NULL);
    edgeCfg = *pCfg;
    return apply();
}

void *servEdgeCfg::cfg_edge_getCfg()
{
    return &edgeCfg;
}
/*!----------------------------------------------------------------------------------------------------------*/

servEdgeTrigger::servEdgeTrigger(QString name)
    :TriggerBase(name, Trigger_Edge, E_SERVICE_ID_TRIG_Edge, false)
{
    serviceExecutor::baseInit();
    linkChange(MSG_TRIGGER_SOURCE_LA_EXT_AC, MSG_TRIGGER_NOISE);
    linkChange(MSG_TRIGGER_SOURCE_LA_EXT_AC, MSG_TRIGGER_COUPLING);
    linkChange(CMD_SERVICE_RST,     MSG_TRIGGER_EDGE_A);
    linkChange(CMD_SERVICE_STARTUP, MSG_TRIGGER_EDGE_A);
}

int servEdgeTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    TriggerBase::commomSerialOut(stream, ver);
    servEdgeCfg::cfg_edge_serialOut(stream, ver);
    return 0;
}
int servEdgeTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        TriggerBase::commomSerialIn(stream, ver);
        servEdgeCfg::cfg_edge_serialIn(stream, ver);
    }
    else
    {
        rst();
    }
    return 0;
}

void servEdgeTrigger::rst()
{
    TriggerBase::rst();
    servEdgeCfg::cfg_edge_init();
    async(CMD_TRIGGER_ALL_SPY_LICENSE,1);
    async(MSG_TRIGGER_SOURCE_LA_EXT_AC, getSource());
}

/*Select right trigger to startup */
int servEdgeTrigger::startup()
{
    //! apply Common paramaters
    DsoErr err = ERR_NONE;
    TriggerBase::startup();
    err = async(MSG_TRIGGER_SWEEP   ,(int)getSweep()          );
    err = async(MSG_TRIGGER_COUPLING,(int)getCoupling()       );
    err = async(MSG_TRIGGER_HOLDOFF ,(qint64)getTrigHoldoff() );
    err = async(MSG_TRIGGER_NOISE   ,(bool)getNoiseReject()   );
    err = async(MSG_TRIGGER_TYPE    ,(int)getMode()           );

    async(CMD_TRIGGER_ALL_SPY_LICENSE,1);

    async(CMD_TRIGGER_ALL_SPY_LICENSE,1);
    async(MSG_TRIGGER_SOURCE_LA_EXT_AC, getSource());
    return  err;
}

/**
 * @brief servEdgeTrigger::getWhen
 * @return
 */
int servEdgeTrigger::getWhen(void)
{
    return cfg_edge_getWhen();
}

/**
 * @brief servEdgeTrigger::setWhen
 * @param s
 */
int servEdgeTrigger::setWhen(int w )
{
    return cfg_edge_setWhen(w);
}

int servEdgeTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }

    if((src >= chan1) && (src <= chan4))
    {
        mUiAttr.setEnable(MSG_TRIGGER_COUPLING, true );
    }
    else
    {
        mUiAttr.setEnable(MSG_TRIGGER_COUPLING, false);
    }

    TriggerBase::setSource(src);
    TriggerBase::setCoupling( m_nCouple) ;
    return cfg_edge_setCh(src);
}

int servEdgeTrigger::setLevel(qint64 level)
{
    DsoErr err = ERR_NONE;
    m_pCurrSource->setLevel(Trigger_Level_High,level);
    if(ERR_NONE != err) return err;

    m_pCurrSource->setLevel(Trigger_Level_Low, level);
    if(ERR_NONE != err) return err;
    return applyLevel();
}

void servEdgeTrigger::onProjectMode()
{
    post(serv_name_utility,  MSG_APP_UTILITY_PROJECT,0);
}

DsoErr servEdgeTrigger::setCfg(pointer p)
{
    DsoErr err = cfg_edge_setCfg(p);
    ssync( MSG_TRIGGER_SOURCE_LA_EXT_AC, cfg_edge_getCh() );
    updateAllUi();
    return err;
}

pointer servEdgeTrigger::getCfg()
{
    return (pointer)cfg_edge_getCfg();
}

void servEdgeTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

DsoErr servEdgeTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servEdgeTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource());

    int slope = getWhen();

    if(slope == Trigger_Edge_Any)
    {
        slope = Trig_View_Any_L_H;
    }

    if(slope == Trigger_Edge_Alternating)
    {
        slope = Trig_View_Alternating_L_H;
    }
    return slope;
}

DsoErr servEdgeTrigger::applyLevel()
{
    if( (m_nCouple == AC) || (m_nCouple == LF) )
    {
        Chan    ch      = (Chan)m_pCurrSource->getChan();
        QString name    = QString("chan%1").arg(ch);
        int     nOffset = 0;
        serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);
        return applyLevelEngine( ch, (-nOffset));
    }
    else
    {
        return TriggerBase::applyLevel();
    }
}
