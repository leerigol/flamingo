#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servNthTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servNthTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,         &servNthTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SLOPE_POLARITY,    &servNthTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servNthTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,          &servNthTrigger::setSource),

on_set_int_bool (       MSG_TRIGGER_SLOPE_POLARITY,     &servNthTrigger::setWhen),
on_get_bool     (       MSG_TRIGGER_SLOPE_POLARITY,     &servNthTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_NTH_IDLETIME,       &servNthTrigger::setIdleTime),
on_get_ll       (       MSG_TRIGGER_NTH_IDLETIME,       &servNthTrigger::getIdleTime),

on_set_int_int  (       MSG_TRIGGER_NTH_EDGE,           &servNthTrigger::setEdgeNum),
on_get_int      (       MSG_TRIGGER_NTH_EDGE,           &servNthTrigger::getEdgeNum),

on_set_void_void(       CMD_SERVICE_RST,                &servNthTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servNthTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servNthTrigger::setActive ),
end_of_entry()


servNthTrigger::servNthTrigger(QString name)
    : TriggerBase(name,Trigger_NEdge,E_SERVICE_ID_TRIG_Nth)
{
    serviceExecutor::baseInit();
}

DsoErr servNthTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servNthTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource());
    return getWhen();
}

int servNthTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);

    stream.write(QStringLiteral("mNum"),       cfg.mNum);
    stream.write(QStringLiteral("mIdleTime"),  cfg.mIdleTime);
    stream.write(QStringLiteral("mCh"),        (int)cfg.mCh);
    stream.write(QStringLiteral("mSlope"),     (int)cfg.mSlope);

    return 0;
}
int servNthTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {   int value;
        stream.read(QStringLiteral("mNum"),       cfg.mNum);
        stream.read(QStringLiteral("mIdleTime"),  cfg.mIdleTime);
        stream.read(QStringLiteral("mCh"),        value);
        cfg.setChan((Chan)value);
        stream.read(QStringLiteral("mSlope"),     value);
        cfg.setSlope((EdgeSlope)value);
    }
    else
    {
        rst();
    }
    return 0;
}
void servNthTrigger::rst()
{
    TriggerBase::rst();
    cfg.setNum(1);
    cfg.setIdleTime(G_s64LowerDefault);
    cfg.setSlope(Trigger_Edge_Rising);
    cfg.setChan((Chan)getSource());
}

int servNthTrigger::startup()
{
    for(int i=d0; i<ext; i++)
    {
        mUiAttr.setVisible( MSG_TRIGGER_SOURCE, i, false );
    }
    return  TriggerBase::startup();
}

void servNthTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

int servNthTrigger::getWhen()
{
    return cfg.getSlope();
}

int servNthTrigger::setWhen(int s)
{
    cfg.setSlope((EdgeSlope)s);
    return apply();
}

int servNthTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        cfg.mCh = Chan(src);
        TriggerBase::setSource(src);
        return apply();
    }
}


qint64 servNthTrigger::getIdleTime( void )
{
    setuiStep( getTimeStep(cfg.idleTime()) );
    setuiRange(time_ns(16),time_s(10),time_ns(16));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return cfg.idleTime();
}

int servNthTrigger::setIdleTime(qint64 t)
{
    cfg.setIdleTime(t);
    return apply();
}

int servNthTrigger::getEdgeNum()
{
    setuiRange(1,65535,1);
    return cfg.num();
}

int servNthTrigger::setEdgeNum(int e)
{
    if(e < 1)
    {
        cfg.setNum(1);
        apply();
        return ERR_OVER_LOW_RANGE;
    }
    else
    {
        cfg.setNum(e);
        return apply();
    }
}


