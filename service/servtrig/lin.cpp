#include "servtrig.h"
#include "trigcode.h"

IMPLEMENT_POST_DO( TriggerBase, servLinTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servLinTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,         &servLinTrigger::applyLevel),
end_of_postdo()


IMPLEMENT_CMD( TriggerBase, servLinTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,           &servLinTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_LIN_WHEN,            &servLinTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_LIN_WHEN,            &servLinTrigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_LIN_ERR_TYPE,        &servLinTrigger::setErrType),
on_get_int      (       MSG_TRIGGER_LIN_ERR_TYPE,        &servLinTrigger::getErrType),

on_set_int_int  (       MSG_TRIGGER_LIN_ID,              &servLinTrigger::setId),
on_get_int      (       MSG_TRIGGER_LIN_ID,              &servLinTrigger::getId),

on_set_int_int  (       MSG_TRIGGER_LIN_DATA_BIT,        &servLinTrigger::setCurrBit),
on_get_int      (       MSG_TRIGGER_LIN_DATA_BIT,        &servLinTrigger::getCurrBit),

on_set_int_int  (       MSG_TRIGGER_LIN_DATA_BYTE,       &servLinTrigger::setByteCount),
on_get_int      (       MSG_TRIGGER_LIN_DATA_BYTE,       &servLinTrigger::getByteCount),

on_set_int_int  (       MSG_TRIGGER_LIN_VERSION,         &servLinTrigger::setVersion),
on_get_int      (       MSG_TRIGGER_LIN_VERSION,         &servLinTrigger::getVersion),

on_set_int_int  (       MSG_TRIGGER_LIN_BAUD,            &servLinTrigger::setBaudRate),
on_get_int      (       MSG_TRIGGER_LIN_BAUD,            &servLinTrigger::getBaudRate),

on_set_int_int  (       MSG_TRIGGER_LIN_SAMPLE_POINT,    &servLinTrigger::setSamplePoint),
on_get_int      (       MSG_TRIGGER_LIN_SAMPLE_POINT,    &servLinTrigger::getSamplePoint),

on_set_int_int  (       MSG_TRIGGER_CODE,                &servLinTrigger::setCodeValue),
on_get_int      (       MSG_TRIGGER_CODE,                &servLinTrigger::getCodeValue),
on_set_int_arg  (       MSG_TRIGGER_SET_CODE,            &servLinTrigger::setCode),
on_get_void_argo(       MSG_TRIGGER_SET_CODE,            &servLinTrigger::getCode),
on_set_int_int  (       MSG_TRIGGER_CODE_ALL,            &servLinTrigger::setCodeAll),

on_set_void_void(       CMD_SERVICE_RST,                 &servLinTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,             &servLinTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,              &servLinTrigger::setActive ),
end_of_entry()

servLinTrigger::servLinTrigger(QString name)
    : TriggerBase(name, Trigger_LIN, E_SERVICE_ID_TRIG_LIN)
{
    serviceExecutor::baseInit();
}

DsoErr servLinTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

#define ar ar_out
#define ar_pack_e ar_out
int servLinTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    ar("m_currBit",          m_currBit         );

    ar_pack_e("mCh",     cfg.mCh        );
    ar_pack_e("mClk",    cfg.mClk       );
    ar_pack_e("mVer",    cfg.mVer       );
    ar_pack_e("mWhen",   cfg.mWhen      );
    ar_pack_e("mIdCmp",  cfg.mIdCmp     );
    ar_pack_e("mDatCmp", cfg.mDatCmp    );
    ar_pack_e("mErr",    cfg.mErr       );
    ar("mIdMin",         cfg.mIdMin     );
    ar("mIdMax",         cfg.mIdMax     );
    ar("mSaPos",         cfg.mSaPos     );
    ar("mByteCount",     cfg.mByteCount );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servLinTrigger::serialIn(CStream &stream, unsigned char ver)
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        ar("m_currBit",          m_currBit         );

        ar_pack_e("mCh",     cfg.mCh        );
        ar_pack_e("mClk",    cfg.mClk       );
        ar_pack_e("mVer",    cfg.mVer       );
        ar_pack_e("mWhen",   cfg.mWhen      );
        ar_pack_e("mIdCmp",  cfg.mIdCmp     );
        ar_pack_e("mDatCmp", cfg.mDatCmp    );
        ar_pack_e("mErr",    cfg.mErr       );
        ar("mIdMin",         cfg.mIdMin     );
        ar("mIdMax",         cfg.mIdMax     );
        ar("mSaPos",         cfg.mSaPos     );

        cfg.setMinBits(  QByteArray(8,0));
        cfg.setMaxBits(  QByteArray(8,0));
        cfg.setMaskBits( QByteArray(8,0));
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servLinTrigger::rst()
{
    cfg.setCh((Chan)chan1);
    cfg.setClk(9600);
    cfg.setVer((Trigger_Lin_Ver)trig_lin_ver_1x);
    cfg.setWhen((Trigger_Lin_When)trig_lin_sync);
    cfg.setIdCmp((Trigger_value_cmp) cmp_eq);
    cfg.setDatCmp((Trigger_value_cmp) cmp_eq);

    cfg.setErr((Trigger_Lin_Err)trig_lin_sync_err);
    cfg.setIdMin(0);
    cfg.setIdMax(0);
    cfg.setSaPos(50);

    cfg.setByteCount(1);
    cfg.setMinBits(  QByteArray(8,0));
    cfg.setMaxBits(  QByteArray(8,0));
    cfg.setMaskBits( QByteArray(8,0));
    m_currBit = 0;
    TriggerBase::rst();
}

int servLinTrigger::startup()
{
    return  TriggerBase::startup();
}

void servLinTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

DsoErr servLinTrigger::setSource(int ch)
{
    cfg.setCh((Chan)ch);
    TriggerBase::setSource(ch);
    return apply();
}

DsoErr servLinTrigger::setWhen(int w)
{
    cfg.setWhen((Trigger_Lin_When)w);
    return apply();
}

int servLinTrigger::getWhen()
{
    return cfg.when();
}

DsoErr servLinTrigger::setErrType(int type)
{
    cfg.setErr((Trigger_Lin_Err) type);
    return apply();
}

int servLinTrigger::getErrType()
{
    return cfg.err();
}

DsoErr servLinTrigger::setId(int id)
{
    cfg.setIdMin(id);
    return apply();
}

int servLinTrigger::getId()
{
    int data = cfg.idMin();

    setuiRange( 0, 63, 0);
    setuiOutStr(QString("%1(0x%2)").arg(data).arg(data,0,16) );
    return data;
}

DsoErr servLinTrigger::setCurrBit(int bit)
{
    DsoErr err = ERR_NONE;
    //!按下 切换当前位的值
    if(TRIG_CURRBIT_Z_VALUE == bit)
    {
        int    curValue = getCodeValue();
        bool   binHex = true;

        if( m_currBit >= getBitMax() )
        {
            binHex = false;
        }

        int stop = (getBitMax()%4)?((1<<getBitMax()%4)-1):(0xf); //! 0x00-0xf
        stop = (m_currBit == getBitMax())?stop:(0xf);

        err = setCodeValue( trigCode::get_z_value(curValue,binHex) );
    }
    else //!旋转 切换当前位置
    {
        int b = bit  + m_currBit;

        m_currBit = qMax(0,b);
        m_currBit = qMin(m_currBit, getBitByteMax()-1 );
    }

    return err;
}

int servLinTrigger::getCurrBit()
{
    setuiZVal( TRIG_CURRBIT_Z_VALUE );
    return m_currBit;
}

DsoErr servLinTrigger::setByteCount(int byt)
{
    cfg.setMinBits(  cfg.mMinBits.left(byt).leftJustified(8,0) );
    cfg.setMaxBits(  cfg.mMaxBits.left(byt).leftJustified(8,0) );
    cfg.setMaskBits( cfg.mMaskBits.left(byt).leftJustified(8,0));

    cfg.setByteCount(byt);
    m_currBit = qMin(m_currBit, getBitByteMax()-1);
    return apply();
}

int servLinTrigger::getByteCount()
{
    setuiRange( 1, 8, 1 );
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return cfg.byteCount();
}

DsoErr servLinTrigger::setVersion(int ver)
{
    cfg.setVer((Trigger_Lin_Ver)ver);
    return apply();
}

int servLinTrigger::getVersion()
{
    return cfg.ver();
}

DsoErr servLinTrigger::setBaudRate(int baud)
{
    cfg.setClk(baud);
    return apply();
}

int servLinTrigger::getBaudRate()
{
    setuiRange( (int)1E3, (int)2E7, 9600 );
    setuiPostStr("bps");
    return cfg.clk();
}

DsoErr servLinTrigger::setSamplePoint(int point)
{
    cfg.setSaPos(point);
    return apply();
}

int servLinTrigger::getSamplePoint()
{
    setuiRange( 10, 90, 50);
    setuiPostStr("%");
    return cfg.saPos();
}

int servLinTrigger::getBitMax()
{
    return (cfg.byteCount()*8);
}

int servLinTrigger::getBitByteMax()
{
    return (cfg.byteCount()*(8+2));
}

DsoErr servLinTrigger::setCodeValue(int value)
{
    //set data
    trigCode::setBitValue(cfg.mMinBits,
                          cfg.mMaskBits,
                          m_currBit,
                          value,
                          cfg.byteCount());
    return apply();
}

int servLinTrigger::getCodeValue()
{
    int value = Trigger_Code_X;
    value = trigCode::getBitValue(   cfg.mMinBits,
                                     cfg.mMaskBits,
                                     m_currBit,
                                     cfg.byteCount());
    return value;
}

DsoErr servLinTrigger::setCode(CArgument &/*arg*/)
{
    return ERR_NONE;
}

void servLinTrigger::getCode(CArgument &arg)
{
    for(int i = 0; i < getBitMax(); i++)
    {
        int value = Trigger_Code_X;
        value = trigCode::getBitValue(   cfg.mMinBits,
                                         cfg.mMaskBits,
                                         i,
                                         cfg.byteCount());
        arg.append(value);
    }
}

DsoErr servLinTrigger::setCodeAll(int value )
{
    trigCode::setAllValue(   cfg.mMinBits,
                             cfg.mMaskBits,
                             m_currBit,
                             value,
                             cfg.byteCount());
    return apply();
}
