#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servDelayTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,          &servDelayTrigger::applyLevel),
on_postdo( MSG_TRIGGER_DELAY_SRCA,      &servDelayTrigger::applyLevel),
on_postdo( MSG_TRIGGER_EDGE_A,          &servDelayTrigger::applyLevel),
on_postdo( MSG_TRIGGER_DELAY_SRCB,      &servDelayTrigger::applyLevel),
on_postdo( MSG_TRIGGER_EDGEB,           &servDelayTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servDelayTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_DELAY_SRCA,         &servDelayTrigger::setSourceA),
on_get_int      (       MSG_TRIGGER_DELAY_SRCA,         &servDelayTrigger::getSourceA),

on_set_int_bool (       MSG_TRIGGER_EDGEA,              &servDelayTrigger::setEdgeA),
on_get_bool     (       MSG_TRIGGER_EDGEA,              &servDelayTrigger::getEdgeA),

on_set_int_int  (       MSG_TRIGGER_DELAY_SRCB,         &servDelayTrigger::setSourceB),
on_get_int      (       MSG_TRIGGER_DELAY_SRCB,         &servDelayTrigger::getSourceB),

on_set_int_int  (       MSG_TRIGGER_EDGEB,              &servDelayTrigger::setEdgeB),
on_get_int      (       MSG_TRIGGER_EDGEB,              &servDelayTrigger::getEdgeB),

on_set_int_int  (       MSG_TRIGGER_DELAY_WHEN,         &servDelayTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_DELAY_WHEN,         &servDelayTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_UPPER,        &servDelayTrigger::setUpper),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_UPPER,        &servDelayTrigger::getUpper, &servDelayTrigger::getUpperAttr),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_LOWER,        &servDelayTrigger::setLower),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_LOWER,        &servDelayTrigger::getLower, &servDelayTrigger::getLowerAttr),

//!scpi
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_H,        &servDelayTrigger::setLevelSrcA),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_H,        &servDelayTrigger::getLevelSrcA),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_L,        &servDelayTrigger::setLevelSrcB),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_L,        &servDelayTrigger::getLevelSrcB),


on_set_void_void(       CMD_SERVICE_RST,                &servDelayTrigger::rst ),
on_set_int_void  (       CMD_SERVICE_STARTUP,           &servDelayTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servDelayTrigger::setActive ),

end_of_entry()

servDelayTrigger::servDelayTrigger(QString name)
    : TriggerBase(name,Trigger_Delay, E_SERVICE_ID_TRIG_Delay)
{
    serviceExecutor::baseInit();
    linkChange( MSG_TRIGGER_DELAY_WHEN,     MSG_TRIGGER_LIMIT_LOWER, MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_LOWER,    MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_UPPER,    MSG_TRIGGER_LIMIT_LOWER);
}

DsoErr servDelayTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servDelayTrigger::getChSlope(Chan ch)
{
    if(ch == getSourceA())
    {
        return getEdgeA();
    }

    if(ch == getSourceB())
    {
        return getEdgeB();
    }
    Q_ASSERT(false);
}

DsoErr servDelayTrigger::applyLevel()
{
    DsoErr err = ERR_NONE;
    err = applyLevelEngine((Chan)getSourceA());
    if(err != ERR_NONE) return err;

    err = applyLevelEngine((Chan)getSourceB());
    return err;
}

void servDelayTrigger::getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    tH = cfg.widthH();
    tL = cfg.widthL();
    w  = cfg.cmp();
}

DsoErr servDelayTrigger::setTimeRangeValue(qint64 &tL, qint64 &tH)
{
    cfg.setWidthH(tH);
    cfg.setWidthL(tL);
    return apply();
}

void servDelayTrigger::getUpperAttr(enumAttr)
{
    qint64 t = cfg.widthH();
    SET_TRIG_TIME_ATTR(t, time_ns(8), time_s(10), G_s64UpperDefault);
}

void servDelayTrigger::getLowerAttr(enumAttr)
{
    qint64 t = cfg.widthL();
    SET_TRIG_TIME_ATTR(t, time_ns(8), time_s(10), G_s64UpperDefault);
}

int servDelayTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    stream.write(QStringLiteral("m_nWhen"),         (int)cfg.cmp()   );

    stream.write(QStringLiteral("m_SrcA"),          (int)cfg.chA());
    stream.write(QStringLiteral("m_bEdgeA"),        (int)cfg.slopeA());

    stream.write(QStringLiteral("m_SrcB"),          (int)cfg.chB());
    stream.write(QStringLiteral("m_bEdgeB"),        (int)cfg.slopeB());

    stream.write(QStringLiteral("m_s64Lower"),      cfg.widthL());
    stream.write(QStringLiteral("m_s64Upper"),      cfg.widthH());

    CSource *p = getSourceObj(cfg.chA());
    qint64   l = p->getLevel(0);
    stream.write(QStringLiteral("srcAlevel"),      l);

    p = getSourceObj(cfg.chB());
    l = p->getLevel(0);
    stream.write(QStringLiteral("srcBlevel"),      l);

    return 0;
}
int servDelayTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        int value;
        stream.read(QStringLiteral("m_nWhen"),       value);
        cfg.setCmp((EMoreThan)value);

        stream.read(QStringLiteral("m_SrcA"),        value);
        cfg.setChA((Chan)value);
        stream.read(QStringLiteral("m_bEdgeA"),      value);
        cfg.setSlopeA((EdgeSlope)value);
        stream.read(QStringLiteral("m_SrcB"),        value);
        cfg.setChB((Chan)value);
        stream.read(QStringLiteral("m_bEdgeB"),      value);
        cfg.setSlopeB((EdgeSlope)value);

        stream.read(QStringLiteral("m_s64Lower"),    cfg.mWidthL);
        stream.read(QStringLiteral("m_s64Upper"),    cfg.mWidthH);

        qint64 l = 0;
        stream.read(QStringLiteral("srcAlevel"),      l);
        getSourceObj(cfg.chA())->setLevel(0,l);

        stream.read(QStringLiteral("srcBlevel"),      l);
        getSourceObj(cfg.chB())->setLevel(0,l);
    }
    else
    {
        rst();
    }
    return 0;
}
void servDelayTrigger::rst()
{
    cfg.setChA(chan1);
    cfg.setChB(chan2);
    cfg.setCmp(Trigger_When_Morethan);
    cfg.setSlopeA(Trigger_Edge_Rising);
    cfg.setSlopeB(Trigger_Edge_Rising);
    cfg.setWidthL(G_s64LowerDefault);
    cfg.setWidthH(G_s64UpperDefault);
    TriggerBase::rst();
}

int servDelayTrigger::startup()
{
    return  TriggerBase::startup();
}

void servDelayTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

/**
 * @brief servDelayTrigger::getWhen
 * @return int
 */
int servDelayTrigger::getWhen(void)
{
    return cfg.cmp();
}

/**
 * @brief servDelayTrigger::setWhen
 * @param s
 */
int servDelayTrigger::setWhen(int s )
{
    EMoreThan w0 = (EMoreThan)getWhen();
    cfg.setCmp((EMoreThan)s);

    return checkTimeRange(T_Rule_L, w0, time_ns(8), time_s(10));
}


/**
 * @brief servDelayTrigger::getLower
 * @return
 */
qint64 servDelayTrigger::getLower(void)
{
    return cfg.widthL();
}

/**
 * @brief servDelayTrigger::setLower
 * @param t
 * @return
 */
int servDelayTrigger::setLower(qint64 t)
{
    cfg.setWidthL(t);
    return checkTimeRange(T_Rule_L, Trigger_When_None,
                          time_ns(8), time_s(10));
}

/**
 * @brief servDelayTrigger::getUpper
 * @return
 */
qint64 servDelayTrigger::getUpper(void)
{
    return cfg.widthH();
}

/**
 * @brief servDelayTrigger::setUpper
 * @param t
 * @return
 */
int servDelayTrigger::setUpper(qint64 t)
{
    cfg.setWidthH(t);
    return checkTimeRange(T_Rule_H, Trigger_When_None,
                          time_ns(8), time_s(10));
}

int servDelayTrigger::getSourceA()
{
    return cfg.chA();
}

int servDelayTrigger::setSourceA(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    cfg.setChA((Chan)src);

    TriggerBase::setSource(src);
    TriggerBase::setSourceEN(cfg.chA());
    TriggerBase::setSourceEN(cfg.chB());
    return apply();
}

bool servDelayTrigger::getEdgeA()
{
    return cfg.slopeA();
}

int servDelayTrigger::setEdgeA(bool e)
{
    EdgeSlope slope = (EdgeSlope)e;
    cfg.setSlopeA(slope);
    return apply();
}

int servDelayTrigger::getSourceB()
{
    return cfg.chB();
}

int servDelayTrigger::setSourceB(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    cfg.setChB((Chan)src);

    TriggerBase::setSource(src);
    TriggerBase::setSourceEN(cfg.chA());
    TriggerBase::setSourceEN(cfg.chB());
    return apply();
}

bool servDelayTrigger::getEdgeB()
{
    return cfg.slopeB();
}

int servDelayTrigger::setEdgeB(bool e)
{
    EdgeSlope slope = (EdgeSlope)e;

    cfg.setSlopeB(slope);

    return apply();
}

int servDelayTrigger::setLevelSrcA(qint64 l)
{
    ssync(MSG_TRIGGER_DELAY_SRCA, getSourceA() );
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );

}

qint64 servDelayTrigger::getLevelSrcA()
{
    qint64 l = 0;
    ssync(MSG_TRIGGER_DELAY_SRCA, getSourceA() );
    serviceExecutor::query( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return l;
}

int servDelayTrigger::setLevelSrcB(qint64 l)
{
    ssync(MSG_TRIGGER_DELAY_SRCB, getSourceB() );
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servDelayTrigger::getLevelSrcB()
{
    qint64 l = 0;
    ssync(MSG_TRIGGER_DELAY_SRCB, getSourceB() );
    serviceExecutor::query( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return l;
}


