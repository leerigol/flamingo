#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servSlopeTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servSlopeTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SLOPE_POLARITY,    &servSlopeTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE,            &servSlopeTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servSlopeTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE,             &servSlopeTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_SLOPE_WHEN,         &servSlopeTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_SLOPE_WHEN,         &servSlopeTrigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_SLOPE_POLARITY,     &servSlopeTrigger::setSlope),
on_get_int      (       MSG_TRIGGER_SLOPE_POLARITY,     &servSlopeTrigger::getSlope),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_UPPER,        &servSlopeTrigger::setUpper),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_UPPER,        &servSlopeTrigger::getUpper,&servSlopeTrigger::getUpperAttr),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_LOWER,        &servSlopeTrigger::setLower),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_LOWER,        &servSlopeTrigger::getLower,&servSlopeTrigger::getLowerAttr),

on_set_int_int  (       MSG_TRIGGER_LEVELSELECT,        &servSlopeTrigger::setLevelSelect),
on_get_int      (       MSG_TRIGGER_LEVELSELECT,        &servSlopeTrigger::getLevelSelect),

on_set_int_pointer(     CMD_TRIGGER_SLOPE_CFG,          &servSlopeTrigger::setCfg),
on_get_pointer    (     CMD_TRIGGER_SLOPE_CFG,          &servSlopeTrigger::getCfg),

//!scpi
on_set_int_ll   (       CMD_SCPI_TRIGGER_SLOPE_TIME,    &servSlopeTrigger::setTime),
on_get_ll       (       CMD_SCPI_TRIGGER_SLOPE_TIME,    &servSlopeTrigger::getTime),

on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_L,        &servSlopeTrigger::setLevelL),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_L,        &servSlopeTrigger::getLevelL),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_H,        &servSlopeTrigger::setLevelH),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_H,        &servSlopeTrigger::getLevelH),

on_set_void_void(       CMD_SERVICE_RST,                &servSlopeTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servSlopeTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servSlopeTrigger::setActive ),
end_of_entry()

#define ar ar_out
#define ar_pack_e ar_out
int servSlopeCfg::cfg_Slope_serialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("cfg_slope_mCh",      slopeCfg.mCh      );
    ar_pack_e("cfg_slope_mSlope",   slopeCfg.mSlope   );
    ar_pack_e("cfg_slope_mCmp",     slopeCfg.mCmp     );
    ar("cfg_slope_mWidthH",         slopeCfg.mWidthH  );
    ar("cfg_slope_mWidthL",         slopeCfg.mWidthL  );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servSlopeCfg::cfg_Slope_serialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("cfg_slope_mCh",      slopeCfg.mCh      );
    ar_pack_e("cfg_slope_mSlope",   slopeCfg.mSlope   );
    ar_pack_e("cfg_slope_mCmp",     slopeCfg.mCmp     );
    ar("cfg_slope_mWidthH",         slopeCfg.mWidthH  );
    ar("cfg_slope_mWidthL",         slopeCfg.mWidthL  );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servSlopeCfg::cfg_slope_init()
{
    slopeCfg.setChan(chan1);
    slopeCfg.setSlope(Trigger_Edge_Rising);
    slopeCfg.setCmp(Trigger_When_Morethan);
    slopeCfg.setWidthL(time_us(1));
    slopeCfg.setWidthH(time_us(1));
}

qint64 servSlopeCfg::cfg_slope_getLower()
{
    return slopeCfg.getWidthL();
}

int servSlopeCfg::cfg_slope_setLower(qint64 t)
{
    slopeCfg.setWidthL(t);
    return apply();
}

qint64 servSlopeCfg::cfg_slope_getUpper()
{
    return slopeCfg.getWidthH();
}

int servSlopeCfg::cfg_slope_setUpper(qint64 t)
{
    slopeCfg.setWidthH(t);
    return apply();
}

int servSlopeCfg::cfg_slope_setSource(int src)
{
    slopeCfg.setChan(Chan(src));
    return apply();
}

int servSlopeCfg::cfg_slope_getSource()
{
    return slopeCfg.getChan();
}

int servSlopeCfg::cfg_slope_setCmp(int cmp)
{
    slopeCfg.setCmp((EMoreThan)cmp);
    return apply();
}

int servSlopeCfg::cfg_slope_getCmp()
{
    return slopeCfg.getCmp();
}

int servSlopeCfg::cfg_slope_setSlope(int slope)
{
    slopeCfg.setSlope((EdgeSlope)slope);
    return apply();
}

int servSlopeCfg::cfg_slope_getSlope()
{
    return slopeCfg.getSlope();
}

void servSlopeCfg::cfg_slope_getTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    tL = cfg_slope_getLower();
    tH = cfg_slope_getUpper();
    w  = (EMoreThan)cfg_slope_getCmp();
}

DsoErr servSlopeCfg::cfg_slope_setTimeRange(qint64 &tL, qint64 &tH)
{
    cfg_slope_setLower(tL);
    return cfg_slope_setUpper(tH);
}

int servSlopeCfg::cfg_slope_setCfg(void *p)
{
    TrigSlopeCfg *pCfg = static_cast<TrigSlopeCfg*>(p);
    Q_ASSERT(pCfg != NULL);
    slopeCfg = *pCfg;
    return apply();
}

void *servSlopeCfg::cfg_slope_getCfg()
{
    return &slopeCfg;
}

/*!----------------------------------------------------------------------------------------------------------*/

servSlopeTrigger::servSlopeTrigger(QString name)
    : TriggerBase(name,Trigger_Slope,E_SERVICE_ID_TRIG_Slope,true)
{
    serviceExecutor::baseInit();
    linkChange( MSG_TRIGGER_SLOPE_WHEN,  MSG_TRIGGER_LIMIT_LOWER, MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_LOWER, MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_UPPER, MSG_TRIGGER_LIMIT_LOWER);
}

DsoErr servSlopeTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servSlopeTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource());
    return (int)getSlope();
}

void servSlopeTrigger::getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    cfg_slope_getTimeRange(tL, tH, w);

}

DsoErr servSlopeTrigger::setTimeRangeValue(qint64 &tL, qint64 &tH)
{
    return cfg_slope_setTimeRange(tL, tH);
}

int servSlopeTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);

    stream.write(QStringLiteral("m_nLevelSelect"), m_nLevelSelect  );

    servSlopeCfg::cfg_Slope_serialOut(stream, ver);

    return 0;
}
int servSlopeTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value;
        stream.read(QStringLiteral("m_nLevelSelect"), value);
        m_nLevelSelect = (Trigger_Level_ID)value;

        servSlopeCfg::cfg_Slope_serialIn(stream, ver);
    }
    else
    {
        rst();
    }
    return 0;
}
void servSlopeTrigger::rst()
{
    TriggerBase::rst();
    servSlopeCfg::cfg_slope_init();
    m_nLevelSelect  =   Trigger_Level_High;
}
int servSlopeTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    for(int i=d0; i<ext; i++)
    {
        mUiAttr.setVisible( MSG_TRIGGER_SOURCE, i, false );
    }
    return  TriggerBase::startup();
}

void servSlopeTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

bool servSlopeTrigger::getSlope()
{
    return (bool)cfg_slope_getSlope();
}

int servSlopeTrigger::setSlope(bool b)
{
    return cfg_slope_setSlope((int)b);
}

/**
 * @brief servSlopeTrigger::getWhen
 * @return int
 */
int servSlopeTrigger::getWhen(void)
{
    return cfg_slope_getCmp();
}

/**
 * @brief servSlopeTrigger::setWhen
 * @param s
 */
int servSlopeTrigger::setWhen(int w )
{
    EMoreThan w0 = (EMoreThan)getWhen();

    DsoErr err = cfg_slope_setCmp(w);

    checkTimeRange(T_Rule_None,w0);
    return err;
}

/**
 * @brief servSlopeTrigger::getLower
 * @return
 */
qint64 servSlopeTrigger::getLower(void)
{
    qint64 value = cfg_slope_getLower();
    return value;
}

/**
 * @brief servSlopeTrigger::setLower
 * @param t
 * @return
 */
int servSlopeTrigger::setLower(qint64 t)
{
    DsoErr err = cfg_slope_setLower(t);
    checkTimeRange(T_Rule_L);
    return err;
}

/**
 * @brief servSlopeTrigger::getUpper
 * @return
 */
qint64 servSlopeTrigger::getUpper(void)
{
    qint64 value = cfg_slope_getUpper();
    return value;
}

/**
 * @brief servSlopeTrigger::setUpper
 * @param t
 * @return
 */
int servSlopeTrigger::setUpper(qint64 t)
{
    DsoErr err = cfg_slope_setUpper(t);
    checkTimeRange(T_Rule_H);
    return err;
}

int servSlopeTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }

    TriggerBase::setSource(src);
    return cfg_slope_setSource(src);
}

DsoErr servSlopeTrigger::setCfg(pointer p)
{
    DsoErr err = cfg_slope_setCfg(p);
    ssync( MSG_TRIGGER_SOURCE, cfg_slope_getSource() );

    updateAllUi();
    return err;
}

pointer servSlopeTrigger::getCfg()
{
    return cfg_slope_getCfg();
}

DsoErr servSlopeTrigger::setTime(qint64 time)
{
    DsoErr err  = ERR_NONE;
    qint64 diff = 0;

    switch (cfg_slope_getCmp())
    {
    case Trigger_When_Morethan:
        err = async(MSG_TRIGGER_LIMIT_LOWER, time);
        break;

    case Trigger_When_Lessthan:
        err = async(MSG_TRIGGER_LIMIT_UPPER, time);
        break;

    case Trigger_When_MoreLess:

        diff =   cfg_slope_getUpper() - cfg_slope_getLower();
        err = async(MSG_TRIGGER_LIMIT_LOWER, time);
        err = async(MSG_TRIGGER_LIMIT_UPPER, time + diff );
        break;

    default:
        err = ERR_INVALID_INPUT;
        break;
    }
    return err;
}

qint64 servSlopeTrigger::getTime()
{
    qint64 time = 0;

    switch (cfg_slope_getCmp())
    {
    case Trigger_When_Morethan:
        time = cfg_slope_getLower();
        break;

    case Trigger_When_Lessthan:
        time = cfg_slope_getUpper();
        break;

    case Trigger_When_MoreLess:
        time = cfg_slope_getLower();
        break;

    default:
        time = 0;
        break;
    }
    return time;
}

int servSlopeTrigger::setLevelL(qint64 l)
{
    setLevelSelect(Trigger_Level_Low);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servSlopeTrigger::getLevelL()
{
    qint64 l = 0;
    setLevelSelect(Trigger_Level_Low);
    serviceExecutor::query(serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return  l;
}

int servSlopeTrigger::setLevelH(qint64 l)
{
    setLevelSelect(Trigger_Level_High);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servSlopeTrigger::getLevelH()
{
    qint64 l = 0;
    setLevelSelect(Trigger_Level_High);
    serviceExecutor::query(serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return l;
}
