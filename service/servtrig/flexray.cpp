#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servFlexRayTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servFlexRayTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,         &servFlexRayTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servFlexRayTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,               &servFlexRayTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_WHEN,            &servFlexRayTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_FLEXRAY_WHEN,            &servFlexRayTrigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_POST_TYPE,       &servFlexRayTrigger::setPosType),
on_get_int      (       MSG_TRIGGER_FLEXRAY_POST_TYPE,       &servFlexRayTrigger::getPosType),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_FRAME_TYPE,      &servFlexRayTrigger::setFrameType),
on_get_int      (       MSG_TRIGGER_FLEXRAY_FRAME_TYPE,      &servFlexRayTrigger::getFrameType),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_SYMBOL_TYPE,     &servFlexRayTrigger::setSymbolType),
on_get_int      (       MSG_TRIGGER_FLEXRAY_SYMBOL_TYPE,     &servFlexRayTrigger::getSymbolType),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_ERROR_TYPE,      &servFlexRayTrigger::setErrType),
on_get_int      (       MSG_TRIGGER_FLEXRAY_ERROR_TYPE,      &servFlexRayTrigger::getErrType),

on_set_int_bool (       MSG_TRIGGER_FLEXRAY_DEFINE,          &servFlexRayTrigger::setDefine),
on_get_bool     (       MSG_TRIGGER_FLEXRAY_DEFINE,          &servFlexRayTrigger::getDefine),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_ID_COMP,         &servFlexRayTrigger::setIdComp),
on_get_int      (       MSG_TRIGGER_FLEXRAY_ID_COMP,         &servFlexRayTrigger::getIdComp),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_ID_MIN,          &servFlexRayTrigger::setIdMin),
on_get_int      (       MSG_TRIGGER_FLEXRAY_ID_MIN,          &servFlexRayTrigger::getIdMin),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_ID_MAX,          &servFlexRayTrigger::setIdMax),
on_get_int      (       MSG_TRIGGER_FLEXRAY_ID_MAX,          &servFlexRayTrigger::getIdMax),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_CYC_COMP,        &servFlexRayTrigger::setCycComp),
on_get_int      (       MSG_TRIGGER_FLEXRAY_CYC_COMP,        &servFlexRayTrigger::getCycComp),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_CYC_MIN,         &servFlexRayTrigger::setCycMin),
on_get_int      (       MSG_TRIGGER_FLEXRAY_CYC_MIN,         &servFlexRayTrigger::getCycMin),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_CYC_MAX,         &servFlexRayTrigger::setCycMax),
on_get_int      (       MSG_TRIGGER_FLEXRAY_CYC_MAX,         &servFlexRayTrigger::getCycMax),

on_set_int_int  (       MSG_TRIGGER_FLEXRAY_BAUD,            &servFlexRayTrigger::setBaudRate),
on_get_int      (       MSG_TRIGGER_FLEXRAY_BAUD,            &servFlexRayTrigger::getBaudRate),

on_set_int_bool (       MSG_TRIGGER_FLEXRAY_CH_A_B,          &servFlexRayTrigger::setChType),
on_get_bool     (       MSG_TRIGGER_FLEXRAY_CH_A_B,          &servFlexRayTrigger::getChType),


on_set_void_void(       CMD_SERVICE_RST,                 &servFlexRayTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,             &servFlexRayTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,              &servFlexRayTrigger::setActive ),
end_of_entry()

servFlexRayTrigger::servFlexRayTrigger(QString name)
    : TriggerBase(name, Trigger_FlexRay, E_SERVICE_ID_TRIG_FLEXRAY)
{
    serviceExecutor::baseInit();

    linkChange( MSG_TRIGGER_FLEXRAY_CYC_COMP,  MSG_TRIGGER_FLEXRAY_CYC_MIN, MSG_TRIGGER_FLEXRAY_CYC_MAX);
    linkChange( MSG_TRIGGER_FLEXRAY_CYC_MAX,   MSG_TRIGGER_FLEXRAY_CYC_MIN);
    linkChange( MSG_TRIGGER_FLEXRAY_CYC_MIN,   MSG_TRIGGER_FLEXRAY_CYC_MAX);

    linkChange( MSG_TRIGGER_FLEXRAY_ID_COMP,  MSG_TRIGGER_FLEXRAY_ID_MIN, MSG_TRIGGER_FLEXRAY_ID_MAX);
    linkChange( MSG_TRIGGER_FLEXRAY_ID_MIN,   MSG_TRIGGER_FLEXRAY_ID_MAX);
    linkChange( MSG_TRIGGER_FLEXRAY_ID_MAX,   MSG_TRIGGER_FLEXRAY_ID_MIN);

    mUiAttr.setEnable(MSG_TRIGGER_FLEXRAY_CH_A_B, false);
}

DsoErr servFlexRayTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

#define ar ar_out
#define ar_pack_e ar_out
int servFlexRayTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    ar("mDefine",          mDefine               );

    ar_pack_e("mCh",        cfg.mCh              );
    ar_pack_e("mBaud",      cfg.mBaud            );
    ar_pack_e("mPhy",       cfg.mPhy             );
    ar_pack_e("mWhen",      cfg.mWhen            );
    ar_pack_e("mPos",       cfg.mPos             );
    ar_pack_e("mFrame",     cfg.mFrame           );
    ar_pack_e("mSymbol",    cfg.mSymbol          );
    ar_pack_e("mErr",       cfg.mErr             );
    ar_pack_e("mIdCmp",     cfg.mIdCmp           );
    ar("mIdCmpMask",        cfg.mIdCmpMask       );
    ar_pack_e("mCycleCmp",  cfg.mCycleCmp        );
    ar("mCycleCmpMask",     cfg.mCycleCmpMask    );
    ar("mTssTransmitter",   cfg.mTssTransmitter  );
    ar("mCasLowMax",        cfg.mCasLowMax       );
    ar("mIdMin",            cfg.mIdMin           );
    ar("mIdMax",            cfg.mIdMax           );
    ar("mIdMask",           cfg.mIdMask          );
    ar("mCycleMin",         cfg.mCycleMin        );
    ar("mCycleMax",         cfg.mCycleMax        );
    ar("mCycleMask",        cfg.mCycleMask       );

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servFlexRayTrigger::serialIn(CStream &stream, unsigned char ver)
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        ar("mDefine",          mDefine               );

        ar_pack_e("mCh",        cfg.mCh              );
        ar_pack_e("mBaud",      cfg.mBaud            );
        ar_pack_e("mPhy",       cfg.mPhy             );
        ar_pack_e("mWhen",      cfg.mWhen            );
        ar_pack_e("mPos",       cfg.mPos             );
        ar_pack_e("mFrame",     cfg.mFrame           );
        ar_pack_e("mSymbol",    cfg.mSymbol          );
        ar_pack_e("mErr",       cfg.mErr             );
        ar_pack_e("mIdCmp",     cfg.mIdCmp           );
        ar("mIdCmpMask",        cfg.mIdCmpMask       );
        ar_pack_e("mCycleCmp",  cfg.mCycleCmp        );
        ar("mCycleCmpMask",     cfg.mCycleCmpMask    );
        ar("mTssTransmitter",   cfg.mTssTransmitter  );
        ar("mCasLowMax",        cfg.mCasLowMax       );
        ar("mIdMin",            cfg.mIdMin           );
        ar("mIdMax",            cfg.mIdMax           );
        ar("mIdMask",           cfg.mIdMask          );
        ar("mCycleMin",         cfg.mCycleMin        );
        ar("mCycleMax",         cfg.mCycleMax        );
        ar("mCycleMask",        cfg.mCycleMask       );
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e


void servFlexRayTrigger::rst()
{
    cfg.setCh(    (Chan) chan1);

    cfg.setBaud(  (Trigger_Flex_Baud)trig_flex_10M);

    cfg.setPhy(   (Trigger_Flex_Phy)trig_flex_cha);

    cfg.setWhen(  (Trigger_Flex_When)trig_flex_frame);

    cfg.setPos(   (Trigger_Flex_Pos)trig_flex_pos_tss_end);

    cfg.setFrame( (Trigger_Flex_Frame)trig_flex_frame_all);

    cfg.setSymbol((Trigger_Flex_Symbol)trig_flex_symbol_cas_mts);

    cfg.setErr(   (Trigger_Flex_Err)trig_flex_symbol_cas_mts);

    cfg.setIdCmp( (Trigger_value_cmp)cmp_eq);
    cfg.setIdCmpMask(true);

    cfg.setCycleCmp((Trigger_value_cmp)cmp_eq);
    cfg.setCycleCmpMask(true);

    cfg.setTssTransmitter(mTssTransmitter);
    cfg.setCasLowMax(mCasLowMax);

    cfg.setIdMin(0);
    cfg.setIdMax(0);
    cfg.setIdMask(0xFFFFFFFF);

    cfg.setCycleMin(0);
    cfg.setCycleMax(0);
    cfg.setCycleMask(0xFFFFFFFF);

    mDefine = false;
    TriggerBase::rst();

    async(MSG_TRIGGER_FLEXRAY_WHEN, getWhen() );
}

int servFlexRayTrigger::startup()
{
    async(MSG_TRIGGER_FLEXRAY_WHEN, getWhen() );
    return  TriggerBase::startup();
}

void servFlexRayTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

DsoErr servFlexRayTrigger::setSource(int ch)
{
    cfg.setCh((Chan)ch);
    TriggerBase::setSource(ch);
    return apply();
}

DsoErr servFlexRayTrigger::setWhen(int w)
{
    if(trig_flex_symbol == w)
    {
        id_async(E_SERVICE_ID_TRIG_FLEXRAY,MSG_TRIGGER_FLEXRAY_DEFINE,false);
        mUiAttr.setEnable(MSG_TRIGGER_FLEXRAY_DEFINE,false);
    }
    else
    {
        mUiAttr.setEnable(MSG_TRIGGER_FLEXRAY_DEFINE,true);
    }

    cfg.setWhen(  (Trigger_Flex_When)w);
    return apply();
}

int servFlexRayTrigger::getWhen()
{
    return cfg.when();
}

DsoErr servFlexRayTrigger::setFrameType(int type)
{
    cfg.setFrame( (Trigger_Flex_Frame)type);
    return apply();
}

int servFlexRayTrigger::getFrameType()
{
    return cfg.frame();
}

DsoErr servFlexRayTrigger::setSymbolType(int type)
{
    cfg.setSymbol((Trigger_Flex_Symbol)type);
    return apply();
}

int servFlexRayTrigger::getSymbolType()
{
    return cfg.symbol();
}

DsoErr servFlexRayTrigger::setErrType(int type)
{
    cfg.setErr( (Trigger_Flex_Err)type);
    return apply();
}

int servFlexRayTrigger::getErrType()
{
    return cfg.err();
}

DsoErr servFlexRayTrigger::setPosType(int type)
{
    cfg.setPos( (Trigger_Flex_Pos)type);
    return apply();
}

int servFlexRayTrigger::getPosType()
{
    return cfg.pos();
}

DsoErr servFlexRayTrigger::setDefine(bool define)
{
    mDefine = define;
    return ERR_NONE;
}

bool servFlexRayTrigger::getDefine()
{
    return mDefine;
}


DsoErr servFlexRayTrigger::setIdComp(int value)
{
    Trigger_value_cmp c0 = (Trigger_value_cmp)getIdComp();

    cfg.setIdCmp( (Trigger_value_cmp)value);

    return checkIdRange(T_Rule_None, c0);;
}

int servFlexRayTrigger::getIdComp()
{
    int value = cfg.idCmp();
    return value;
}

DsoErr servFlexRayTrigger::setIdMin(int value)
{
    cfg.setIdMin(value);
    return checkIdRange(T_Rule_L);
}

int servFlexRayTrigger::getIdMin()
{
    setuiZVal(  0);
    setuiMaxVal(1023);
    setuiMinVal(0);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return cfg.idMin();
}

DsoErr servFlexRayTrigger::setIdMax(int value)
{
    cfg.setIdMax(value);
    return checkIdRange(T_Rule_H);
}

int servFlexRayTrigger::getIdMax()
{
    setuiZVal(  0);
    setuiMaxVal(1023);
    setuiMinVal(0);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return cfg.idMax();
}

DsoErr servFlexRayTrigger::setCycComp(int value)
{
    Trigger_value_cmp c0 = (Trigger_value_cmp)getCycComp();

    cfg.setCycleCmp( (Trigger_value_cmp)value);;

    return checkCyCRange(T_Rule_None, c0);
}

int servFlexRayTrigger::getCycComp()
{
    int value = cfg.cycleCmp();
    return value;
}

DsoErr servFlexRayTrigger::setCycMin(int value)
{
    cfg.setCycleMin(value);
    return checkCyCRange(T_Rule_L);
}

int servFlexRayTrigger::getCycMin()
{
    setuiZVal(  0);
    setuiMaxVal(63);
    setuiMinVal(0);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);

    return cfg.cycleMin();
}

DsoErr servFlexRayTrigger::setCycMax(int value)
{
    cfg.setCycleMax(value);
    return checkCyCRange(T_Rule_H);
}

int servFlexRayTrigger::getCycMax()
{
    setuiZVal(  0);
    setuiMaxVal(63);
    setuiMinVal(0);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return cfg.cycleMax();
}

DsoErr servFlexRayTrigger::setBaudRate(int baud)
{
    if(baud<=trig_flex_2_5M)
    {
        baud = trig_flex_2_5M;
    }
    else if(baud > trig_flex_5M)
    {
        baud = trig_flex_10M;
    }
    else
    {
        baud = trig_flex_5M;
    }

    cfg.setBaud(  (Trigger_Flex_Baud)baud);
    return apply();
}

int servFlexRayTrigger::getBaudRate()
{
    return cfg.baud();
}

DsoErr servFlexRayTrigger::setChType(bool b)
{
    cfg.setPhy(   (Trigger_Flex_Phy)b);
    return apply();
}

int servFlexRayTrigger::getChType()
{
    return cfg.phy();
}

DsoErr servFlexRayTrigger::checkIdRange(TrigTimeRule rule, Trigger_value_cmp c0)
{
    qint64 L = cfg.idMin();
    qint64 H = cfg.idMax();
    Trigger_value_cmp c = cfg.idCmp();

    trigrules::cmpRangeRule(rule, L, H, c, c0, 0, 1023);

    cfg.setIdMin(L);
    cfg.setIdMax(H);
    return apply();
}

DsoErr servFlexRayTrigger::checkCyCRange(TrigTimeRule rule, Trigger_value_cmp c0)
{
    qint64 L = cfg.cycleMin();
    qint64 H = cfg.cycleMax();
    Trigger_value_cmp c = cfg.cycleCmp();

    trigrules::cmpRangeRule(rule, L, H, c, c0, 0, 63);

    cfg.setCycleMin(L);
    cfg.setCycleMax(H);
    return apply();
}

