#include "trigcode.h"
#include <QDebug>
#include "../../include/dsotype.h"

const int       trigCode::_z_value_bin[] =  {Trigger_Code_X,Trigger_Code_L,Trigger_Code_H};
const int       trigCode::_z_value_hex[]  = {Trigger_Code_X,0,1,2,3,4,5,6,7,8,9,0XA,0XB,0XC,0XD,0XE,0XF};

trigCode::trigCode()
{

}

/*!
 * \brief trigCode::setBitValue  设置QByteArray中的bit位
 * \param data                      数据源  (data中的数据,低在左 ,高位右,按 字节 序列存储.)
 * \param mask                      因数据包含X,所以存在与数据源对应的掩码
 * \param bit                       要设置的bit位
 * \param code                      该bit位的三种状态 1,0,X
 * \param bytMax                    data换算成二进制的有效位数,bytMax默认为100,
 *                                  有效位数 在data.count()*8 与 bytMax 中取最小值
 */
void trigCode::setBitValue(QByteArray &data,
                           QByteArray &mask,
                           int bit,
                           int code, int bytMax)
{
    int byteCount = qMin(data.count(), mask.count()) ;
    byteCount = qMin(byteCount,bytMax);

    if(bit < byteCount*8)         //!set bin
    {
        bit = (byteCount*8-1)-bit;//!倒序
        for(int i = 0; i < byteCount; i++)
        {
            if( (bit/8) == i )
            {
                char data_i = data.at(i);
                char mask_i = mask.at(i);
                qDebug()<<__FUNCTION__<<__LINE__<<data_i<<"+++++"<<mask_i;

                if(Trigger_Code_H == code)
                {
                    data_i |= (1<<(bit%8));
                    mask_i |= (1<<(bit%8));
                }
                else if(Trigger_Code_L == code)
                {
                    data_i &= (~(1<<(bit%8)));
                    mask_i |= (1<<(bit%8));
                }
                else if(Trigger_Code_X == code)
                {
                    mask_i &= (~(1<<(bit%8)));
                }
                else
                {Q_ASSERT(false);}

                data[i] = data_i;
                mask[i] = mask_i;

                qDebug()<<__FUNCTION__<<__LINE__<<data_i<<"+++++"<<mask_i;
                return;
            }
        }
    }
    else if(bit < byteCount*10) //!set hex
    {
        int  curByte  = (bit - (byteCount*8))/2; //!对应 HEX 位
        bool curByteL = (bit - (byteCount*8))%2; //!HEX 位 的 低4位;
        curByte       = byteCount-curByte - 1;       //!倒序

        char data_i = data.at(curByte);
        char mask_i = mask.at(curByte);

        if(Trigger_Code_X == code)
        {
            mask_i &= (curByteL? 0xF0:0X0F);
        }
        else if(code <= 0xF)
        {
            data_i &= (curByteL? 0xF0:0X0F);
            data_i |= ((code&0X0F)<<(curByteL?0:4));
            mask_i |= (curByteL? 0x0F:0XF0);
        }
        else
        {Q_ASSERT(false);}

        data[curByte] = data_i;
        mask[curByte] = mask_i;
    }
    else
    {Q_ASSERT(false);}
}

/*!
 * \brief trigCode::getBitValue 获取QByteArray中的bit位的值
 * \param data                     数据源  (data中的数据,低在左 ,高位右,按 字节 序列存储.)
 * \param mask                     数据源对应的掩码,该位的掩码为0时,返回 X,否则返回data中,该位的真实值
 * \param bit                      要获取的bit位
 * \param bytMax                   data换算成二进制的有效位数,bytMax默认为100,
 *                                 有效位数 在data.count()*8 与 bytMax 中取最小值
 * \return
 */
int trigCode::getBitValue( QByteArray &data,
                           QByteArray &mask,
                           int bit, int bytMax)
{
    int byteCount = qMin(data.count(), mask.count()) ;
    byteCount = qMin(byteCount,bytMax);

    if(bit < byteCount*8)         //!get bin
    {
        bit = (byteCount*8-1)-bit;//!倒序
        int data_i = data.at( bit/8) & (1<< (bit%8));
        int mask_i = mask.at( bit/8) & (1<< (bit%8));

        if(0 == mask_i)
        {
            return Trigger_Code_X;
        }
        else
        {
            return (data_i != 0)?Trigger_Code_H:Trigger_Code_L;
        }
    }
    else if(bit < byteCount*10) //!get hex
    {
        int  curByte  = (bit - (byteCount*8))/2; //!对应 HEX 位
        bool curByteL = (bit - (byteCount*8))%2; //!HEX 位 的 低4位;
        curByte       = byteCount -curByte -1;       //!倒序

        char data_i_H = data.at(curByte) & 0XF0;
        char data_i_L = data.at(curByte) & 0X0F;

        char mask_i_H = mask.at(curByte) & 0XF0;
        char mask_i_L = mask.at(curByte) & 0X0F;

        if(curByteL) //!低4位
        {
            return (int)(0x0F != mask_i_L)?Trigger_Code_X:data_i_L;
        }
        else         //!高4位
        {
            return (int)(0xF0 != mask_i_H)?Trigger_Code_X:(data_i_H>>4);
        }
    }
    else
    {Q_ASSERT(false);}
}
/*!
 * \brief trigCode::setBitValue 设置QBitArray中的bit位的值
 * \param data                     数据源  (data中的数据,低在左 ,高位右,按bit序列存储.)
 * \param mask                     因数据包含X,所以存在与数据源对应的掩码
 * \param bit                      要设置的bit位
 * \param code                     该bit位的三种状态 1,0,X
 * \param bitMax                   data换算成二进制的有效位数,bytMax默认为100,
 *                                 有效位数 在data.count() 与 bytMax 中取最小值
 */
void trigCode::setBitValue(QBitArray &data,
                           QBitArray &mask,
                           int bit, int code, int bitMax)
{
    int bitCount  =  qMin(data.count(), mask.count()) ;
    bitCount  =  qMin(bitCount,bitMax);

    int bytCount  =  ((bitCount+3)/4); //!HEX码总位数
    int curByte   =  bit - bitCount;   //!当前在HEXHEX码位置

    if(bit < bitCount)  //!set bin
    {
        bit = (bitCount-1) - bit ;  //!倒序
        if(Trigger_Code_H == code)
        {
            data.setBit(bit);
            mask.setBit(bit);
        }
        else if(Trigger_Code_L == code)
        {
            data.clearBit(bit);
            mask.setBit(bit);
        }
        else if(Trigger_Code_X == code)
        {
            mask.clearBit(bit);
        }
        else
        {Q_ASSERT(false);}
    }
    else if( curByte < bytCount ) //!set hex
    {
        curByte        = bytCount - curByte -1; //!倒序
        int currBit0   = curByte*4;

        for(int i = 0; i< qMin(bitCount-currBit0, 4); i++)
        {
            if(Trigger_Code_X == code)
            {
                mask.clearBit(currBit0+i);
            }
            else
            {
                data.setBit(currBit0+i,((1<<i)&code)?true:false);
                mask.setBit(currBit0+i);
            }
        }
    }
    else
    {Q_ASSERT(false);}
}
/*!
 * \brief trigCode::getBitValue 获取QBitArray中的bit位的值
 * \param data                     数据源  (data中的数据,低在左 ,高位右,按bit序列存储.)
 * \param mask                     数据源对应的掩码,该位的掩码为0时,返回 X,否则返回data中,该位的真实值
 * \param bit                      要获取的bit位
 * \param bytMax                   data换算成二进制的有效位数,bytMax默认为100,
 *                                 有效位数 在data.count() 与 bytMax 中取最小值
 * \return
 */
int trigCode::getBitValue(QBitArray &data,
                          QBitArray &mask,
                          int bit, int bitMax)
{
    int bitCount  =  qMin(data.count(), mask.count()) ;
    bitCount  =  qMin(bitCount,bitMax);

    int bytCount  =  ((bitCount+3)/4); //!HEX码总位数
    int curByte   =  bit - bitCount;   //!当前在HEXHEX码位置

    if(bit < bitCount)         //!get bin
    {
        bit = (bitCount-1) - bit;  //!倒序
        if(mask.at(bit))
        {
            return  data.at(bit);
        }
        else
        {
            return Trigger_Code_X;
        }
    }
    else if(curByte < bytCount) //!get hex
    {
        curByte        = bytCount - curByte -1; //!倒序
        int currBit0 = curByte*4;
        int retBytL  = 0;
        for(int i = 0; i< qMin(bitCount-currBit0, 4); i++)
        {
            if(0 == mask.at(currBit0+i) )
            {
                return Trigger_Code_X;
            }
            else if(Trigger_Code_H == data.at(currBit0+i))
            {
                retBytL |= (1<<i);
            }
            else if(Trigger_Code_L == data.at(currBit0+i))
            {
                retBytL &= (~(1<<i));
            }
            else
            {Q_ASSERT(false);}
        }
        return retBytL;
    }
    else
    {Q_ASSERT(false);}
}

/*!
 * \brief trigCode::get_z_value  旋钮按下时,获取下一个值
 * \param value                     当前值
 * \param binHex                    true获取二进制值 false获取HEX值
 * \param stop                      当前表中循环的终止条件
 * \return                          返回int值
 */
int trigCode::get_z_value(int value, bool binHex, int stop)
{
    int count = binHex? array_count(_z_value_bin) : array_count(_z_value_hex) ;
    QVector<int> tmp(count,0);
    //!拷贝bin表或hex表到零时数组tmp
    for(int i = 0; i < count; i++)
    {
        tmp[i] = binHex? _z_value_bin[i] : _z_value_hex[i];
    }
    //!获取当前数值的下一个数值
    for(int i = 0; (i<tmp.count())&&(value != stop); i++)
    {
        if( value == tmp[i] )
        {
            int j = ( (i+1) < tmp.count() )? (i+1) : 0;

            return tmp[j];
        }
    }
    return tmp[0];
}

void trigCode::setAllValue(QByteArray &data,
                           QByteArray &mask,
                           int bit,
                           int code,
                           int bytMax)
{
    int byteCount = qMin(data.count(), mask.count()) ;
    byteCount = qMin(byteCount,bytMax);
    for(int i = 0; i < byteCount; i++)
    {
        if(bit<byteCount*8) //!bin
        {
            if(Trigger_Code_H == code)
            {
                data[i]  = 0xFF;
                mask[i]  = 0xFF;
            }
            else if(Trigger_Code_L == code)
            {
                data[i]  = 0x00;
                mask[i]  = 0xFF;
            }
            else if(Trigger_Code_X == code)
            {
                data[i]  = 0x00;
                mask[i]  = 0x00;
            }
            else
            {
                Q_ASSERT(false);
            }
        }
        else//! hex
        {
            if(Trigger_Code_X == code)
            {
                data[i]  = 0x00;
                mask[i]  = 0x00;
            }
            else
            {
                data[i]  = code + (code<<4) ;
                mask[i]  = 0xFF;
            }
        }
    }
}

void trigCode::setAllValue(QBitArray &data,
                           QBitArray &mask,
                           int bit,
                           int code,
                           int bitMax)
{
    int bitCount  =  qMin(data.count(), mask.count()) ;
    bitCount  =  qMin(bitCount,bitMax);

    for(int i = 0; i < bitCount; i++)
    {
        if(bit<bitCount) //!bin
        {
            if(Trigger_Code_H == code)
            {
                data[i]  = true;
                mask[i]  = true;
            }
            else if(Trigger_Code_L == code)
            {
                data[i]  = false;
                mask[i]  = true;
            }
            else if(Trigger_Code_X == code)
            {
                data[i]  = false;
                mask[i]  = false;
            }
            else
            {
                Q_ASSERT(false);
            }
        }
        else//!hex
        {
            if(Trigger_Code_X == code)
            {
                data[i]  = false;
                mask[i]  = false;
            }
            else
            {
                data[i]  = code&(1<<(i%4));
                mask[i]  = true;
            }
        }
    }
}
