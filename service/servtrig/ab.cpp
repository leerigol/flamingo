#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servABTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,           &servABTrigger::applyLevel),
on_postdo( MSG_TRIGGER_EDGE_A,           &servABTrigger::applyLevel),
on_postdo( MSG_TRIGGER_EDGE_B,           &servABTrigger::applyLevel),
on_postdo( MSG_TRIGGER_POLARITY,         &servABTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE,           &servABTrigger::applyLevel),
end_of_postdo()


IMPLEMENT_CMD( TriggerBase, servABTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE,             &servABTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_EDGE_A,             &servABTrigger::setEdgeA),
on_get_int      (       MSG_TRIGGER_EDGE_A,             &servABTrigger::getEdgeA),

on_set_int_bool (       MSG_TRIGGER_POLARITY,           &servABTrigger::setPolarity),
on_get_bool     (       MSG_TRIGGER_POLARITY,           &servABTrigger::getPolarity),

on_set_int_int  (       MSG_TRIGGER_AB_WHEN,            &servABTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_AB_WHEN,            &servABTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_UPPER,        &servABTrigger::setUpper),
on_get_ll       (       MSG_TRIGGER_LIMIT_UPPER,        &servABTrigger::getUpper),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_LOWER,        &servABTrigger::setLower),
on_get_ll       (       MSG_TRIGGER_LIMIT_LOWER,        &servABTrigger::getLower),

on_set_int_int  (       MSG_TRIGGER_EDGE_B,             &servABTrigger::setEdgeB),
on_get_int      (       MSG_TRIGGER_EDGE_B,             &servABTrigger::getEdgeB),

on_set_int_ll   (       MSG_TRIGGER_EVENT_DELAY,        &servABTrigger::setDelay),
on_get_ll       (       MSG_TRIGGER_EVENT_DELAY,        &servABTrigger::getDelay),

on_set_int_int  (       MSG_TRIGGER_EVENT_COUNT,        &servABTrigger::setCount),
on_get_int      (       MSG_TRIGGER_EVENT_COUNT,        &servABTrigger::getCount),

on_set_int_int  (       MSG_TRIGGER_AB_A_TYPE,          &servABTrigger::setAEventMode),
on_get_int      (       MSG_TRIGGER_AB_A_TYPE,          &servABTrigger::getAEventMode),

on_set_void_void(       CMD_SERVICE_RST,                &servABTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servABTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servABTrigger::setActive ),
//on_set_int_int  (       CMD_SERVICE_ENTER_ACTIVE,       &TriggerBase::on_enterActive ),
//on_set_int_int  (       CMD_SERVICE_EXIT_ACTIVE,        &TriggerBase::on_exitActive ),
end_of_entry()


servABTrigger::servABTrigger(QString name)
    :TriggerBase(name, Trigger_AB, E_SERVICE_ID_TRIG_AB)
{
    serviceExecutor::baseInit();
}

DsoErr servABTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servABTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource());
    if( getEdgeA() ==  getEdgeB())
    {
        return getEdgeA();
    }
    else
    {
        return  Trigger_Edge_Any;
    }
}

#define ar ar_out
#define ar_pack_e ar_out
int servABTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    ar_pack_e("maEdgeCfg.mCh",         cfg.maEdgeCfg.mCh       );
    ar_pack_e("maEdgeCfg.mSlope",      cfg.maEdgeCfg.mSlope    );
    ar_pack_e("maPulseCfg.mCh",        cfg.maPulseCfg.mCh      );
    ar_pack_e("maPulseCfg.mPolariy",   cfg.maPulseCfg.mPolariy );
    ar_pack_e("maPulseCfg.mCmp",       cfg.maPulseCfg.mCmp     );
    ar("maPulseCfg.mWidthH",           cfg.maPulseCfg.mWidthH  );
    ar("maPulseCfg.mWidthL",           cfg.maPulseCfg.mWidthL  );
    ar_pack_e("mbEdgeCfg.mCh",         cfg.mbEdgeCfg.mCh       );
    ar_pack_e("mbEdgeCfg.mSlope",      cfg.mbEdgeCfg.mSlope    );
    ar_pack_e("maType",                cfg.maType              );
    ar_pack_e("mbType",                cfg.mbType              );
    ar("mbCount",                      cfg.mbCount             );
    ar("mDelay",                       cfg.mDelay              );

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servABTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        ar_pack_e("maEdgeCfg.mCh",         cfg.maEdgeCfg.mCh       );
        ar_pack_e("maEdgeCfg.mSlope",      cfg.maEdgeCfg.mSlope    );
        ar_pack_e("maPulseCfg.mCh",        cfg.maPulseCfg.mCh      );
        ar_pack_e("maPulseCfg.mPolariy",   cfg.maPulseCfg.mPolariy );
        ar_pack_e("maPulseCfg.mCmp",       cfg.maPulseCfg.mCmp     );
        ar("maPulseCfg.mWidthH",           cfg.maPulseCfg.mWidthH  );
        ar("maPulseCfg.mWidthL",           cfg.maPulseCfg.mWidthL  );
        ar_pack_e("mbEdgeCfg.mCh",         cfg.mbEdgeCfg.mCh       );
        ar_pack_e("mbEdgeCfg.mSlope",      cfg.mbEdgeCfg.mSlope    );
        ar_pack_e("maType",                cfg.maType              );
        ar_pack_e("mbType",                cfg.mbType              );
        ar("mbCount",                      cfg.mbCount             );
        ar("mDelay",                       cfg.mDelay              );
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servABTrigger::rst()
{
    resetSource();
    //!Aevent
    cfg.maEdgeCfg.setChan(  (Chan)getSource() );
    cfg.maEdgeCfg.setSlope(Trigger_Edge_Rising);

    cfg.maPulseCfg.setChan( (Chan)getSource() );
    cfg.maPulseCfg.setPolarity(Trigger_pulse_positive);
    cfg.maPulseCfg.setCmp(Trigger_When_Morethan);
    cfg.maPulseCfg.setWidthL(G_s64LowerDefault);
    cfg.maPulseCfg.setWidthH(G_s64UpperDefault);


    //!B event
    cfg.mbEdgeCfg.setChan(  (Chan)getSource() );
    cfg.mbEdgeCfg.setSlope(Trigger_Edge_Rising);


    //!common
    cfg.setbCount(1);
    cfg.setDelay((qint64)G_s64UpperDefault);
    cfg.setaType(Trigger_Edge);
    cfg.setbType(Trigger_Edge);
    TriggerBase::rst();
}

int servABTrigger::startup()
{
    for(int i=d0; i<all_trig_src; i++)
    {
        mUiAttr.setVisible( MSG_TRIGGER_SOURCE, i, false );
    }
    return  TriggerBase::startup();
}

void servABTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

int servABTrigger::setSource(int src)
{
    cfg.maEdgeCfg.setChan(  (Chan)src );
    cfg.maPulseCfg.setChan( (Chan)src );
    cfg.mbEdgeCfg.setChan(  (Chan)src );
    TriggerBase::setSource(src);
    return apply();
}

/**
 * @brief servABTrigger::getEdgeA
 * @return
 */
int servABTrigger::getEdgeA(void)
{
    return cfg.maEdgeCfg.getSlope();
}

/**
 * @brief servABTrigger::setEdgeA
 * @param s
 */
int servABTrigger::setEdgeA(int s )
{
    cfg.maEdgeCfg.setSlope((EdgeSlope)s);
    return apply();
}


/**
 * @brief servABTrigger::getWhen
 * @return int
 */
int servABTrigger::getWhen(void)
{
    return cfg.maPulseCfg.getCmp();
}

/**
 * @brief servABTrigger::setWhen
 * @param s
 */
int servABTrigger::setWhen(int s )
{
    cfg.maPulseCfg.setCmp((EMoreThan)s );
    return apply();
}

/**
 * @brief servABTrigger::getPolarity
 * @return
 */
bool servABTrigger::getPolarity(void)
{
    return cfg.maPulseCfg.getPolarity();
}

/**
 * @brief servABTrigger::setPolarity
 * @param b
 */
int servABTrigger::setPolarity(bool b)
{
    cfg.maPulseCfg.setPolarity( (TriggerPulsePolarity)b );
    return apply();
}

/**
 * @brief servABTrigger::getLower
 * @return
 */
qint64 servABTrigger::getLower(void)
{
    //! update attr
    setuiStep( getTimeStep(cfg.maPulseCfg.getWidthL()) );
    setuiZVal( G_s64LowerDefault );
    setuiMinVal(time_ns(8));
    setuiMaxVal(time_s(10));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);

    setuiPostStr( "s" );

    return cfg.maPulseCfg.getWidthL();
}

/**
 * @brief servABTrigger::setLower
 * @param t
 * @return
 */
int servABTrigger::setLower(qint64 t)
{
    if( (Trigger_When_MoreLess == cfg.maPulseCfg.getCmp())
            && (t >= cfg.maPulseCfg.getWidthH() ) )
    {
        cfg.maPulseCfg.setWidthL(cfg.maPulseCfg.getWidthH());
        apply();
        return ERR_OVER_LOW_RANGE;
    }
    else
    {
        cfg.maPulseCfg.setWidthL(t);
        return apply();
    }
}

/**
 * @brief servABTrigger::getUpper
 * @return
 */
qint64 servABTrigger::getUpper(void)
{
    //! update attr
    setuiStep( getTimeStep(cfg.maPulseCfg.getWidthH()) );
    setuiZVal( G_s64UpperDefault );
    setuiMinVal(time_ns(16));
    setuiMaxVal(time_s(10));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return cfg.maPulseCfg.getWidthH();
}

/**
 * @brief servABTrigger::setUpper
 * @param t
 * @return
 */
int servABTrigger::setUpper(qint64 t)
{
    if( (Trigger_When_MoreLess == cfg.maPulseCfg.getCmp())
            && ( t <= cfg.maPulseCfg.getWidthL() ) )
    {
        cfg.maPulseCfg.setWidthH(cfg.maPulseCfg.getWidthL());
        apply();
        return ERR_OVER_LOW_RANGE;
    }
    else
    {
        cfg.maPulseCfg.setWidthH(t);
        return apply();
    }
}

/////////////////////////////////EVENT B//////////////////////
/**
 * @brief servABTrigger::getEdgeB
 * @return
 */
int servABTrigger::getEdgeB(void)
{
    return cfg.mbEdgeCfg.getSlope();
}

/**
 * @brief servABTrigger::setEdgeB
 * @param s
 */
int servABTrigger::setEdgeB(int s )
{
    cfg.mbEdgeCfg.setSlope((EdgeSlope)s);
    return apply();
}

qint64 servABTrigger::getDelay()
{
    setuiStep( getTimeStep(cfg.getDelay()) );
    setuiMinVal(G_s64LowerDefault);
    setuiMaxVal(G_s64UpperDefault);
    setuiZVal( G_s64LowerDefault );
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return cfg.getDelay();
}

int servABTrigger::setDelay(qint64 t)
{
    cfg.setDelay(t);
    return apply();
}

int servABTrigger::getCount()
{
    setuiRange(1,65536, 1);
    return cfg.getbCount();
}

int servABTrigger::setCount(int c)
{
    if(c>=0)
    {
        cfg.setbCount(c);
        return apply();
    }
    else
    {
        cfg.setbCount(0);
        apply();
        return ERR_OVER_LOW_RANGE;
    }
}

int servABTrigger::setAEventMode(TriggerMode mode)
{
    cfg.setaType(mode);
    return apply();
}

TriggerMode servABTrigger::getAEventMode()
{
    return cfg.getaType();
}
