#ifndef SOURCE
#define SOURCE
#include "../../include/dsotype.h"
using namespace  DsoType;

namespace DsoTrigSource {

struct  sourceLevel
{
    qint64               exitLevel;        //!外触发通道
    qint64               laLevel[2];       //!LA通道阈值:D0-D7-->0,D8-D15-->1
    qint64               chLevel[4][2];    //!4个模拟通道触发电平
    bool                 bTwoLevel[2];     //!模拟通道显示几条触发电平线
};


class CSource
{
public:
    enum sType
    {
        trig_source,
        search_source,
    };

public:
    static void rst();

public:
    CSource(int ch = chan1, sType t = trig_source);

    int     getChan(void)       { return source;}
    void    setChan(int c)      { source = c;}

    bool    hasTwoLevel(int ab = 1);
    void    setTwoLevel(bool b, int ab = 1);

    qint64  getLevel(int ab/* = 0*/);
    int     setLevel(int ab/* = 1*/, qint64 l/* = 0*/);
    int     setInvert();

    void    levelCopy(sType des, sType src);
public:
    int     get_nScale();
    int     get_nOffset();
    float   get_fRatio();

    qint64  get_nLevelMax();
    qint64  get_nLevelMin();
    qint64  get_nLevelDef();
    qint64  get_nLevelStep();


    float   get_fLevelMax();
    float   get_fLevelMin();
    float   get_fLevelDef();

    float   get_fLevel(int ab = 0);
    int     get_nLevelPost(int ab = 0);

    qint64  get_nViewLevel(int ab = 0);
    float   get_fViewLevel(int ab = 0);

    Unit    get_nUnit();
    QString get_sUnit();
private:
    int       source;
    sType     m_type;

    static sourceLevel  m_trigLevel;
    static sourceLevel  m_searchLevel;

private:
    sourceLevel *getLeverPtr(void);
    sourceLevel *getLeverPtr(sType src);

};
}
#endif // SOURCE

