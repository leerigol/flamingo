#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servRuntTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servRuntTrigger::applyLevel),
on_postdo( MSG_TRIGGER_POLARITY,          &servRuntTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE,            &servRuntTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servRuntTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE,             &servRuntTrigger::setSource),

on_set_int_bool (       MSG_TRIGGER_POLARITY,           &servRuntTrigger::setPolarity),
on_get_bool     (       MSG_TRIGGER_POLARITY,           &servRuntTrigger::getPolarity),

on_set_int_int  (       MSG_TRIGGER_RUNT_WHEN,          &servRuntTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_RUNT_WHEN,          &servRuntTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_UPPER,        &servRuntTrigger::setUpper),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_UPPER,        &servRuntTrigger::getUpper, &servRuntTrigger::getUpperAttr),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_LOWER,        &servRuntTrigger::setLower),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_LOWER,        &servRuntTrigger::getLower, &servRuntTrigger::getLowerAttr),

on_set_int_int  (       MSG_TRIGGER_LEVELSELECT,        &servRuntTrigger::setLevelSelect),
on_get_int      (       MSG_TRIGGER_LEVELSELECT,        &servRuntTrigger::getLevelSelect),

on_set_int_pointer(     CMD_TRIGGER_RUNT_CFG,           &servRuntTrigger::setCfg),
on_get_pointer    (     CMD_TRIGGER_RUNT_CFG,           &servRuntTrigger::getCfg),

//!scpi
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_L,        &servRuntTrigger::setLevelL),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_L,        &servRuntTrigger::getLevelL),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_H,        &servRuntTrigger::setLevelH),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_H,        &servRuntTrigger::getLevelH),

on_set_void_void(       CMD_SERVICE_RST,                &servRuntTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servRuntTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servRuntTrigger::setActive ),
end_of_entry()

#define ar ar_out
#define ar_pack_e ar_out
int servRuntCfg::cfg_runt_serialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("m_Ch",        runtCfg.mCh      );
    ar_pack_e("m_bPolarity", runtCfg.mPolariy );
    ar_pack_e("m_nCmp",      runtCfg.mCmp     );
    ar("m_s64Upper",         runtCfg.mWidthH  );
    ar("m_s64Lower",         runtCfg.mWidthL  );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servRuntCfg::cfg_runt_serialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("m_Ch",        runtCfg.mCh      );
    ar_pack_e("m_bPolarity", runtCfg.mPolariy );
    ar_pack_e("m_nCmp",      runtCfg.mCmp     );
    ar("m_s64Upper",         runtCfg.mWidthH  );
    ar("m_s64Lower",         runtCfg.mWidthL  );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servRuntCfg::cfg_runt_init()
{
    runtCfg.setCmp(Trigger_When_None);
    runtCfg.setWidthL(time_ns(8));
    runtCfg.setChan(chan1);
    runtCfg.setPolarity(Trigger_pulse_positive);
    runtCfg.setWidthH(time_us(2));
}

bool servRuntCfg::cfg_runt_getPolarity()
{
    return (bool)runtCfg.getPolarity();
}

int servRuntCfg::cfg_runt_setPolarity(bool b)
{
    if(!b)
    {
        runtCfg.setPolarity(Trigger_pulse_positive);
    }
    else
    {
        runtCfg.setPolarity(Trigger_pulse_negative);
    }
    return apply();
}

int servRuntCfg::cfg_runt_getWhen()
{
    return runtCfg.getCmp();
}

int servRuntCfg::cfg_runt_setWhen(int w)
{
    runtCfg.setCmp((EMoreThan)w);
    return apply();
}

qint64 servRuntCfg::cfg_runt_getLower()
{
    return runtCfg.getWidthL();
}

int servRuntCfg::cfg_runt_setLower(qint64 t)
{
    runtCfg.setWidthL(t);
    return apply();
}

qint64 servRuntCfg::cfg_runt_getUpper()
{
    return runtCfg.getWidthH();
}

int servRuntCfg::cfg_runt_setUpper(qint64 t)
{
    runtCfg.setWidthH(t);
    return apply();
}

int servRuntCfg::cfg_runt_setSource(int src)
{
    runtCfg.setChan(Chan(src));
    return apply();
}

int servRuntCfg::cfg_runt_getSource()
{
    return runtCfg.getChan();
}

void servRuntCfg::cfg_runt_getTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    tH = cfg_runt_getUpper();
    tL = cfg_runt_getLower();
    w  = (EMoreThan)cfg_runt_getWhen();
}

DsoErr servRuntCfg::cfg_runt_setTimeRange(qint64 &tL, qint64 &tH)
{
    cfg_runt_setLower(tL);
    cfg_runt_setUpper(tH);
    return apply();
}

int servRuntCfg::cfg_runt_setCfg(void *p)
{
    TrigRuntCfg *pCfg = static_cast<TrigRuntCfg*>(p);
    Q_ASSERT(pCfg != NULL);
    runtCfg = *pCfg;
    return apply();
}

void *servRuntCfg::cfg_runt_getCfg()
{
    return &runtCfg;
}

/*!-----------------------------------------------------------------------------------------------------------------------*/

servRuntTrigger::servRuntTrigger(QString name)
    : TriggerBase(name,Trigger_Runt, E_SERVICE_ID_TRIG_Runt,true)
{
    serviceExecutor::baseInit();
    linkChange( MSG_TRIGGER_RUNT_WHEN,     MSG_TRIGGER_LIMIT_LOWER, MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_LOWER,    MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_UPPER,    MSG_TRIGGER_LIMIT_LOWER);
}

DsoErr servRuntTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servRuntTrigger::getChSlope(Chan ch)
{
    Q_ASSERT( ch == getSource() );
    return Trig_View_Alternating_H_L;
}

void servRuntTrigger::getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    cfg_runt_getTimeRange(tL, tH, w);
}

DsoErr servRuntTrigger::setTimeRangeValue(qint64 &tL, qint64 &tH)
{
    return cfg_runt_setTimeRange(tL, tH);
}

void servRuntTrigger::getUpperAttr(enumAttr)
{
    qint64 t = cfg_runt_getUpper();
    SET_TRIG_TIME_ATTR(t, time_ns(8), time_s(10), G_s64UpperDefault);
}

void servRuntTrigger::getLowerAttr(enumAttr)
{
    qint64 t = cfg_runt_getLower();
    SET_TRIG_TIME_ATTR(t, time_ns(8), time_s(10), G_s64UpperDefault);
}

int servRuntTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    stream.write(QStringLiteral("m_nLevelSelect"), m_nLevelSelect         );
    servRuntCfg::cfg_runt_serialOut(stream, ver);
    return 0;
}
int servRuntTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value;

        stream.read(QStringLiteral("m_nLevelSelect"), value );
        m_nLevelSelect = (Trigger_Level_ID)value;
        servRuntCfg::cfg_runt_serialIn(stream, ver);
    }
    else
    {
        rst();
    }
    return 0;
}
void servRuntTrigger::rst()
{
    TriggerBase::rst();
    servRuntCfg::cfg_runt_init();
    m_nLevelSelect  = Trigger_Level_High;
}

int servRuntTrigger::startup()
{
    for(int i=d0; i<ext; i++)
    {
        mUiAttr.setVisible( MSG_TRIGGER_SOURCE, i, false );
    }
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servRuntTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

/**
 * @brief servRuntTrigger::getPolarity
 * @return
 */
bool servRuntTrigger::getPolarity(void)
{
    return cfg_runt_getPolarity();
}

/**
 * @brief servRuntTrigger::setPolarity
 * @param b
 */
int servRuntTrigger::setPolarity(bool b)
{
    return cfg_runt_setPolarity(b);
}

/**
 * @brief servRuntTrigger::getWhen
 * @return int
 */
int servRuntTrigger::getWhen(void)
{     
    return cfg_runt_getWhen();
}

/**
 * @brief servRuntTrigger::setWhen
 * @param s
 */
int servRuntTrigger::setWhen(int w )
{
    EMoreThan w0 = (EMoreThan)getWhen();
    DsoErr    err = cfg_runt_setWhen(w);

    checkTimeRange(T_Rule_None,w0);
    return err;
}

/**
 * @brief servRuntTrigger::getLower
 * @return
 */
qint64 servRuntTrigger::getLower(void)
{
    qint64 value = cfg_runt_getLower();
    return value;
}

/**
 * @brief servRuntTrigger::setLower
 * @param t
 * @return
 */
int servRuntTrigger::setLower(qint64 t)
{
    cfg_runt_setLower(t);
    return checkTimeRange(T_Rule_L, Trigger_When_None,
                          time_ns(8), time_s(10));
}

/**
 * @brief servRuntTrigger::getUpper
 * @return
 */
qint64 servRuntTrigger::getUpper(void)
{
    qint64 value = cfg_runt_getUpper();
    return value;
}

/**
 * @brief servRuntTrigger::setUpper
 * @param t
 * @return
 */
int servRuntTrigger::setUpper(qint64 t)
{
    cfg_runt_setUpper(t);
    return checkTimeRange(T_Rule_H, Trigger_When_None,
                          time_ns(8), time_s(10));
}

int servRuntTrigger::setSource(int src)
{
    if( !(src >= chan1 && src <= all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }

    TriggerBase::setSource(src);
    return cfg_runt_setSource(src);
}

DsoErr servRuntTrigger::setCfg(pointer p)
{
    DsoErr err = cfg_runt_setCfg(p);
    ssync( MSG_TRIGGER_SOURCE, cfg_runt_getSource() );

    updateAllUi();
    return err;
}

pointer servRuntTrigger::getCfg()
{
    return cfg_runt_getCfg();
}

int servRuntTrigger::setLevelL(qint64 l)
{
    setLevelSelect(Trigger_Level_Low);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servRuntTrigger::getLevelL()
{
    Chan   ch        = (Chan)getSource();
    qint64 l         = getSourceObj(ch)->getLevel(Trigger_Level_Low);
    return l*getProbeRatio(ch);
}

int servRuntTrigger::setLevelH(qint64 l)
{
    setLevelSelect(Trigger_Level_High);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servRuntTrigger::getLevelH()
{
    Chan   ch        = (Chan)getSource();
    qint64 l         = getSourceObj(ch)->getLevel(Trigger_Level_High);
    return l*getProbeRatio(ch);

}

