#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servRS232Trigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servRS232Trigger::applyLevel),
on_postdo( MSG_TRIGGER_RS232_POLARITY,    &servRS232Trigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,         &servRS232Trigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servRS232Trigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,          &servRS232Trigger::setSource),

on_set_int_bool (       MSG_TRIGGER_RS232_POLARITY,     &servRS232Trigger::setPolarity),
on_get_bool     (       MSG_TRIGGER_RS232_POLARITY,     &servRS232Trigger::getPolarity),

on_set_int_int  (       MSG_TRIGGER_RS232_BAUDRATE,     &servRS232Trigger::setBaudRate),
on_get_int      (       MSG_TRIGGER_RS232_BAUDRATE,     &servRS232Trigger::getBaudRate),

on_set_int_int  (       MSG_TRIGGER_RS232_WHEN,         &servRS232Trigger::setWhen),
on_get_int      (       MSG_TRIGGER_RS232_WHEN,         &servRS232Trigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_RS232_CHECK,        &servRS232Trigger::setParity),
on_get_int      (       MSG_TRIGGER_RS232_CHECK,        &servRS232Trigger::getParity),

on_set_int_int (       MSG_TRIGGER_RS232_STOPBIT,       &servRS232Trigger::setStopBits),
on_get_int     (       MSG_TRIGGER_RS232_STOPBIT,       &servRS232Trigger::getStopBits),

on_set_int_int  (       MSG_TRIGGER_RS232_DATAWIDTH,    &servRS232Trigger::setDataBits),
on_get_int      (       MSG_TRIGGER_RS232_DATAWIDTH,    &servRS232Trigger::getDataBits),

on_set_int_int  (       MSG_TRIGGER_RS232_DATA,         &servRS232Trigger::setData),
on_get_int      (       MSG_TRIGGER_RS232_DATA,         &servRS232Trigger::getData),

on_set_int_pointer(     CMD_TRIGGER_RS232_CFG,          &servRS232Trigger::setCfg),
on_get_pointer    (     CMD_TRIGGER_RS232_CFG,          &servRS232Trigger::getCfg),

on_set_void_void(       CMD_SERVICE_RST,                &servRS232Trigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servRS232Trigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servRS232Trigger::setActive ),
end_of_entry()

#define ar ar_out
#define ar_pack_e ar_out
int servRs232Cfg::cfg_rs232_serialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar(       "cfg_rs232_m_when",   m_when);

    ar_pack_e("cfg_rs232_mCh",      rs232Cfg.mCh);
    ar(       "cfg_rs232_mBaudRate",rs232Cfg.mBaudRate);
    ar(       "cfg_rs232_mbInvert", rs232Cfg.mbInvert);
    ar_pack_e("cfg_rs232_mParity",  rs232Cfg.mParity);
    ar_pack_e("cfg_rs232_mStop",    rs232Cfg.mStop);
    ar_pack_e("cfg_rs232_mWidth",   rs232Cfg.mWidth);
    ar_pack_e("cfg_rs232_mEvent",   rs232Cfg.mEvent);
    ar_pack_e("cfg_rs232_mDataCmp", rs232Cfg.mDataCmp);
    ar_pack_e("cfg_rs232_mErr",     rs232Cfg.mErr);
    QByteArray datas = rs232Cfg.getDatas();
    int *pData  = (int*)datas.data();
    Q_ASSERT(pData != NULL);
    ar_pack_e("cfg_rs232_mDatas",   (*pData) );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servRs232Cfg::cfg_rs232_serialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar(       "cfg_rs232_m_when",   m_when);

    ar_pack_e("cfg_rs232_mCh",      rs232Cfg.mCh);
    ar(       "cfg_rs232_mBaudRate",rs232Cfg.mBaudRate);
    ar(       "cfg_rs232_mbInvert", rs232Cfg.mbInvert);
    ar_pack_e("cfg_rs232_mParity",  rs232Cfg.mParity);
    ar_pack_e("cfg_rs232_mStop",    rs232Cfg.mStop);
    ar_pack_e("cfg_rs232_mWidth",   rs232Cfg.mWidth);
    ar_pack_e("cfg_rs232_mEvent",   rs232Cfg.mEvent);
    ar_pack_e("cfg_rs232_mDataCmp", rs232Cfg.mDataCmp);
    ar_pack_e("cfg_rs232_mErr",     rs232Cfg.mErr);

    int value;
    ar("cfg_rs232_mDatas",   value);
    QByteArray datas((char*)&value, 4);
    rs232Cfg.setDatas(datas);
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servRs232Cfg::cfg_rs232_init()
{
    m_when = rs232_when_start;
    rs232Cfg.setCh(chan1);
    rs232Cfg.setDataCmp(cmp_eq);

    rs232Cfg.setEvent(RS232_When_Start);
    rs232Cfg.setDatas(0);
    rs232Cfg.setErr(RS232_Err_Check);
    rs232Cfg.setMbInvert(false);
    rs232Cfg.setParity(RS232_Parity_None);
    rs232Cfg.setStop(STOP_WIDTH_1);
    rs232Cfg.setWidth(RS232_Width_8);
    rs232Cfg.setBaudRate(9600);

    int value = 0;
    QByteArray datas((char*)&value, 4);
    rs232Cfg.setDatas(datas);
}

bool servRs232Cfg::cfg_rs232_getPolarity()
{
    return rs232Cfg.getMbInvert();
}

DsoErr servRs232Cfg::cfg_rs232_setPolarity(bool b)
{
    rs232Cfg.setMbInvert(b);
    return apply();
}

int servRs232Cfg::cfg_rs232_getWhen()
{
    return m_when;
}

DsoErr servRs232Cfg::cfg_rs232_setWhen(int w)
{
    m_when = w;
    switch (m_when)
    {
    case rs232_when_start:
        rs232Cfg.setEvent(RS232_When_Start);
        break;
    case rs232_when_error:
        rs232Cfg.setEvent(RS232_When_Error);
        rs232Cfg.setErr(RS232_Err_All);
        break;
    case rs232_when_check_error:
        rs232Cfg.setEvent(RS232_When_Error);
        rs232Cfg.setErr(RS232_Err_Check);
        break;
    case rs232_when_data:
        rs232Cfg.setEvent(RS232_When_Data);
        break;
    default:
        rs232Cfg.setEvent(RS232_When_Start);
        break;
    }
    return apply();
}

int servRs232Cfg::cfg_rs232_getBaudRate()
{
    return rs232Cfg.baudRate();
}

DsoErr servRs232Cfg::cfg_rs232_setBaudRate(int baud)
{
    rs232Cfg.setBaudRate( baud );
    return apply();
}

int servRs232Cfg::cfg_rs232_getParity()
{
    return rs232Cfg.getParity();
}

DsoErr servRs232Cfg::cfg_rs232_setParity(int par)
{
    rs232Cfg.setParity((Trigger_RS232_Parity)par);
    return  apply();
}

int servRs232Cfg::cfg_rs232_getStopBits()
{
    return rs232Cfg.getStop();
}

DsoErr servRs232Cfg::cfg_rs232_setStopBits(int bit)
{
    rs232Cfg.setStop( (Trigger_RS232_Stop)bit );
    return apply();
}

int servRs232Cfg::cfg_rs232_getDataBits()
{
    return rs232Cfg.getWidth();
}

DsoErr servRs232Cfg::cfg_rs232_setDataBits(int bit)
{
    rs232Cfg.setWidth((Trigger_RS232_Width)bit);
    return apply();
}

int servRs232Cfg::cfg_rs232_getData()
{
    QByteArray datas = rs232Cfg.getDatas();
    int *pdata = (int*)datas.data();
    Q_ASSERT(pdata != NULL);
    return (*pdata);
}

DsoErr servRs232Cfg::cfg_rs232_setData(int data)
{
    QByteArray datas((char*)&data, 4);
    rs232Cfg.setDatas(datas);
    return apply();
}

int servRs232Cfg::cfg_rs232_getSource()
{
    return rs232Cfg.ch();
}

DsoErr servRs232Cfg::cfg_rs232_setSource(int src)
{
    rs232Cfg.setCh( Chan(src));
    return apply();
}

int servRs232Cfg::cfg_rs232_setCfg(void *p)
{
    TrigRS232Cfg *pCfg = static_cast<TrigRS232Cfg*>(p);
    Q_ASSERT(pCfg != NULL);
    rs232Cfg = *pCfg;
    return apply();
}

void *servRs232Cfg::cfg_rs232_getCfg()
{
    return &rs232Cfg;
}

/*!--------------------------------------------------------------------------------------------------------------------------------*/

servRS232Trigger::servRS232Trigger(QString name)
    : TriggerBase(name, Trigger_RS232, E_SERVICE_ID_TRIG_RS232)
{
    serviceExecutor::baseInit();
}

DsoErr servRS232Trigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servRS232Trigger::getChSlope(Chan /*ch*/)
{
    return (int)(!getPolarity());
}

int servRS232Trigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    servRs232Cfg::cfg_rs232_serialOut(stream, ver);

    return 0;
}
int servRS232Trigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        servRs232Cfg::cfg_rs232_serialIn(stream, ver);
    }
    else
    {
        rst();
    }
    return 0;
}
void servRS232Trigger::rst()
{
    TriggerBase::rst();
    servRs232Cfg::cfg_rs232_init();
}
int servRS232Trigger::startup()
{
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servRS232Trigger::registerSpy()
{
    TriggerBase::registerSpy();
}


/**
 * @brief servRS232Trigger::getPolarity
 * @return
 */
bool servRS232Trigger::getPolarity(void)
{
    return cfg_rs232_getPolarity();
}

/**
 * @brief servRS232Trigger::setPolarity
 * @param b
 */
int servRS232Trigger::setPolarity(bool b)
{
    return cfg_rs232_setPolarity(b);
}


/**
 * @brief servRS232Trigger::getBaudRate
 * @return
 */
int servRS232Trigger::getBaudRate(void)
{
    setuiRange( 1, (int)20e6, 9600 );
    setuiPostStr("bps");
    return cfg_rs232_getBaudRate();
}

/**
 * @brief servRS232Trigger::setBaudRate
 * @param s
 */
int servRS232Trigger::setBaudRate(int s )
{
    return cfg_rs232_setBaudRate(s);
}

/**
 * @brief servRS232Trigger::getWhen
 * @return
 */
int servRS232Trigger::getWhen(void)
{
    return cfg_rs232_getWhen();
}

/**
 * @brief servRS232Trigger::setWhen
 * @param s
 */
int servRS232Trigger::setWhen(int s )
{
    return cfg_rs232_setWhen(s);
}

int servRS232Trigger::getParity(void)
{
    return cfg_rs232_getParity();
}

int servRS232Trigger::setParity(int p)
{
    return  cfg_rs232_setParity(p);
}


int servRS232Trigger::getStopBits(void)
{
    return cfg_rs232_getStopBits();
}

int servRS232Trigger::setStopBits(int p)
{
    return cfg_rs232_setStopBits(p);
}

int servRS232Trigger::getDataBits(void)
{
    return cfg_rs232_getDataBits();
}

int servRS232Trigger::setDataBits(int p)
{
    return cfg_rs232_setDataBits(p);
}

int servRS232Trigger::getData(void)
{
    int data = cfg_rs232_getData();

    setuiRange( 0, 255, 0);
    setuiOutStr(QString("%1(0x%2)").arg(data).arg(data,0,16) );
    return data;
}

int servRS232Trigger::setData(int data)
{
    return cfg_rs232_setData(data);
}

int servRS232Trigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }

    TriggerBase::setSource(src);
    return cfg_rs232_setSource(src);
}

DsoErr servRS232Trigger::setCfg(pointer p)
{
    DsoErr err = ERR_NONE;
    err = cfg_rs232_setCfg(p);
    updateAllUi();
    return err;
}

pointer servRS232Trigger::getCfg()
{
    return cfg_rs232_getCfg();
}
