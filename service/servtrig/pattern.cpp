#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servPatternTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servPatternTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,         &servPatternTrigger::applyLevel),
on_postdo( MSG_TRIGGER_CODE_ALL,          &servPatternTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SET_CODE,          &servPatternTrigger::applyLevel),
on_postdo( MSG_TRIGGER_CODE,              &servPatternTrigger::applyLevel),

end_of_postdo()


IMPLEMENT_CMD( TriggerBase, servPatternTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,          &servPatternTrigger::setSource),

on_set_int_void (       MSG_TRIGGER_CODE_ALL,           &servPatternTrigger::setAll),

on_set_int_arg  (       MSG_TRIGGER_SET_CODE,           &servPatternTrigger::setCode),
on_get_void_argo(       MSG_TRIGGER_SET_CODE,           &servPatternTrigger::getCode),

on_set_int_int  (       MSG_TRIGGER_CODE,               &servPatternTrigger::setValue),
on_get_int      (       MSG_TRIGGER_CODE,               &servPatternTrigger::getValue),

//!scpi
on_set_int_arg  (      CMD_SCPI_TRIGGER_LEVEL,          &servPatternTrigger::setSourceLevel),
on_get_void_argi_argo( CMD_SCPI_TRIGGER_LEVEL,          &servPatternTrigger::getSourceLevel),

on_get_string(       CMD_SCPI_PATTERN_GET_CODE,           &servPatternTrigger::getScpiCode),

on_set_void_void(       CMD_SERVICE_RST,                &servPatternTrigger::rst ),
on_set_int_void (       CMD_SERVICE_STARTUP,            &servPatternTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servPatternTrigger::setActive ),
end_of_entry()


servPatternTrigger::servPatternTrigger(QString name)
    : TriggerBase(name,Trigger_Pattern, E_SERVICE_ID_TRIG_Pattern)
{
    serviceExecutor::baseInit();
}

DsoErr servPatternTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servPatternTrigger::getChSlope(Chan ch)
{
    int i = ch - chan1;
    Q_ASSERT( (i >= 0) && (i < array_count(cfg.mPatterns)));
    switch (cfg.mPatterns[i])
    {
    case Trigger_pat_h:
    case Trigger_pat_rise:
        return Trigger_Edge_Rising;

    case Trigger_pat_l:
    case Trigger_pat_fall:
        return Trigger_Edge_Falling;

    default:
        return TriggerBase::getChSlope(ch);
    }
}

DsoErr servPatternTrigger::applyLevel()
{
    DsoErr err = ERR_NONE;
    for(int i = chan_none; i < all_pattern_src; i++)
    {
        if( cfg.mPatterns[i] != Trigger_pat_x)
        {
            err = applyLevelEngine( (Chan)(i+chan1) );
            if(err != ERR_NONE ) return err;
        }
    }
    return err;
}

int servPatternTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    for(int i=0; i< cfg.mCodeCount; i++)
    {
        stream.write(QString("mPatterns_%1").arg(i), (int)cfg.mPatterns[i]);
    }
    stream.write(QStringLiteral("mOperator"), cfg.mOperator);

    return 0;
}

int servPatternTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value;
        for(int i=0; i< cfg.mCodeCount; i++)
        {
            stream.read(QString("mPatterns_%1").arg(i), value);
            cfg.mPatterns[i] = (TriggerPattern)value;
        }

        stream.read(QStringLiteral("mOperator"), value);
        cfg.mOperator = (TriggerLogicOperator)value;
    }
    else
    {
        rst();
    }
    return 0;
}

void servPatternTrigger::rst()
{
    TriggerBase::rst();
    for(int i=0; i< cfg.mCodeCount; i++)
    {
        cfg.mPatterns[i] =  Trigger_pat_x ;
    }

    cfg.mOperator = Trigger_logic_and;
}
int servPatternTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servPatternTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

/**
 * @brief servPatternTrigger::setCode
 * @param arg
 * @return
 */
int servPatternTrigger::setCode(CArgument &arg)
{
    int size = ( arg.size() <=  cfg.mCodeCount)? arg.size() : cfg.mCodeCount ;

    TrigPatternCfg tempCfg = cfg;

    int val = 0;
    for(int i=0; i<size; i++)
    {
        arg.getVal(val, i);

        tempCfg.mPatterns[i] = (TriggerPattern)val;
    }

    if(tempCfg.checkCode())
    {
        for(int i=0; i<size; i++)
        {
            arg.getVal(val, i);

            cfg.mPatterns[i] = (TriggerPattern)val;
        }
        return apply();
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
}

/**
 * @brief servPatternTrigger::getCode
 * @param arg
 * @return
 */
void servPatternTrigger::getCode(CArgument &arg)
{
    for(int i=0; i<cfg.mCodeCount; i++)
    {
        arg.append( (int)cfg.mPatterns[i] );
    }
}

/**
 * @brief servPatternTrigger::getScpiCode
 * @return
 */
QString servPatternTrigger::getScpiCode()
{
    QString pattString = "";
    int     tempCount  = 4;
    bool  isHasLa = sysCheckLicense(OPT_MSO);
    if(isHasLa)
    {
        tempCount = cfg.mCodeCount-1;
    }
    for(int i=0; i<tempCount; i++)
    {
        switch ( (int)cfg.mPatterns[i] )
        {
        case Trigger_pat_h:
            pattString.append("H,");
            break;

        case Trigger_pat_l:
            pattString.append("L,");
            break;

        case Trigger_pat_x:
            pattString.append("X,");
            break;

        case Trigger_pat_rise:
            pattString.append("R,");
            break;

        case Trigger_pat_fall:
            pattString.append("F,");
            break;

        default:
            break;
        }
    }
    pattString.chop(1);
    return pattString;
}

/**
 * @brief servPatternTrigger::getValue
 * @return
 */
int servPatternTrigger::getValue(void)
{
    int i = m_pCurrSource->getChan() - chan1;
    qDebug()<<__FILE__<<__LINE__<<i<<array_count(cfg.mPatterns);
    Q_ASSERT(i < array_count(cfg.mPatterns) );

    return cfg.mPatterns[i];
}

/**
 * @brief servPatternTrigger::setValue
 * @param s
 * @return
 */
int servPatternTrigger::setValue(int s )
{
    int i = m_pCurrSource->getChan() - chan1;
    Q_ASSERT(i < array_count(cfg.mPatterns));

    cfg.mPatterns[i] = (TriggerPattern)s;

    if(cfg.checkCode())
    {
        return apply();
    }
    else
    {
        cfg.mPatterns[i] = Trigger_pat_x;
        return ERR_INVALID_INPUT;
    }
}

/**
 * @brief servPatternTrigger::setAll
 * @return
 */
int servPatternTrigger::setAll( void )
{
    int code = getValue();

    if(Trigger_pat_x < code)
    {
        return ERR_INVALID_INPUT;
    }

    for(int i=0; i<cfg.mCodeCount; i++)
    {
        cfg.mPatterns[i] = (TriggerPattern)code;
    }
    return apply();
}

int servPatternTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        async(MSG_TRIGGER_CODE, getValue());
    }
    return ERR_NONE;
}

DsoErr servPatternTrigger::setSourceLevel(CArgument &arg)
{
    Q_ASSERT( arg.size() >= 1 );

    if(arg.size() >= 2 )
    {
        setSource(arg[0].iVal);
        serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, arg[1].llVal);
    }
    return ERR_NONE;
}

void servPatternTrigger::getSourceLevel(CArgument &argi, CArgument &argo)
{
    int ch;
    qint64 level = 0;

    if(argi.size() == 0)
    {
        level = getLevel();
        ch    = m_pCurrSource->getChan();
    }
    else if( ERR_NONE == argi.getVal(ch) )
    {
        level = getSourceObj(ch)->getLevel(0);
    }
    else
    {
        qDebug()<<__FILE__<<__LINE__<<"input source  invalid";
    }
    argo.setVal((qint64)(level*getProbeRatio((Chan)ch)));
}

scpiError CodeFormater::format(GCVal &val)
{
    if ( val.vType != val_ptr )
    {
        return scpi_err_val_type_mismatch;
    }

    QList<int> *pCode = static_cast<QList<int> *>(val.pVal);

    if(pCode && pCode->size())
    {
        int size = pCode->size();
        QString tab[] = {"H,","L,","X,","R,","F,"};

        mOutStr.clear();
        for(int i=0; i<size; i++)
        {
            int code = pCode->at(i);
            if(code >= Trigger_pat_h)
            {
                mOutStr += tab[code];
            }
        }
        mOutStr.remove(mOutStr.size()-1,1);
        return scpi_err_none;
    }

    return scpi_err_0_length;
}
