#include "servtrig.h"
#include "../servch/servch.h"
/*-----------------------------------------------------------------------------------------*/
sourceLevel  CSource::m_trigLevel   = {0,{vv(1.4), vv(1.4)},{{0,0},{0,0},{0,0},{0,0}}, {false, false} }  ;
sourceLevel  CSource::m_searchLevel = m_trigLevel;

void CSource::rst()
{
    m_trigLevel.exitLevel = 0;

    for(int  i= 0; i < 4; i++)
    {
         m_trigLevel.chLevel[i][0]   = vv(0);
         m_trigLevel.chLevel[i][1]   = vv(0);
    }

    m_trigLevel.laLevel[0]   = vv(1.4);
    m_trigLevel.laLevel[1]   = vv(1.4);

    m_searchLevel = m_trigLevel;
}

CSource::CSource(int ch, CSource::sType t):source(ch),m_type(t)
{
    m_trigLevel.bTwoLevel[0] = true;
    m_trigLevel.bTwoLevel[1] = false;

    m_searchLevel.bTwoLevel[0] = true;
    m_searchLevel.bTwoLevel[1] = false;
}

bool CSource::hasTwoLevel(int ab)
{
    sourceLevel *ptr = getLeverPtr();
    Q_ASSERT(ptr != NULL);

    if((ab == 0) || (ab == 1))
    {
        return ptr->bTwoLevel[ab];
    }
    else
    {
        qDebug() << "invalid input";
        return false;
    }
}

void CSource::setTwoLevel(bool b, int ab)
{
    sourceLevel *ptr = getLeverPtr();
    Q_ASSERT(ptr != NULL);

    if((ab == 0) || (ab == 1))
    {
        ptr->bTwoLevel[ab] = b;
    }
    else
    {
        qDebug() << "invalid input";
    }
}

qint64 CSource::getLevel(int ab)
{
    Q_ASSERT( (0<= ab) && (ab <= 1));

    sourceLevel *ptr = getLeverPtr();
    Q_ASSERT(ptr != NULL);

    if( (chan1 <= source) && (source <= chan4) )
    {
        qint64 l    = ptr->chLevel[source-chan1][ab];

        return l;
    }
    else if( (d0 <= source) && (source <= d7)  )
    {
        return ptr->laLevel[0];
    }
    else if( (d8 <= source) && (source <= d15) )
    {
        return ptr->laLevel[1];
    }
    else if( source == ext )
    {
        return ptr->exitLevel;
    }

    return 0;
}

int CSource::setLevel(int ab , qint64 l)
{
    sourceLevel *ptr = getLeverPtr();
    Q_ASSERT(ptr != NULL);

    if( (chan1 <= source) && (source <= chan4) )
    {
        qint64 nMax =  get_nLevelMax();
        qint64 nMin =  get_nLevelMin();
        l = qBound(nMin, l, nMax);

        Q_ASSERT( (0<= ab) && (ab <= 1));
        ptr->chLevel[source-chan1][ab] = l;
    }
    else if( (d0 <= source) && (source <= d7)  )
    {
        ptr->laLevel[0] = l;
    }
    else if( (d8 <= source) && (source <= d15) )
    {
        ptr->laLevel[1] = l;
    }
    else if(source == ext )
    {
        ptr->exitLevel = l;
    }
    else
    {
//        qWarning()<<__FUNCTION__<<__LINE__<<"source = "<<source;
        return ERR_INVALID_INPUT;
    }

    return ERR_NONE;
}

int CSource::setInvert()
{
    qint64 h = 0;
    qint64 l = 0;

    DsoErr err;
    if( hasTwoLevel(0) && hasTwoLevel(1) )
    {
        h = -getLevel(1);
        l = -getLevel(0);
    }
    else
    {
        h = -getLevel(0);
        l = -getLevel(1);
    }

    err = setLevel(0, h);
    if(err != ERR_NONE) return err;
    err = setLevel(1, l);
    return err;
}

void CSource::levelCopy(CSource::sType des, CSource::sType src)
{
    sType  type_bak = m_type;

    m_type = src;
    qint64 levelA = getLevel(0);
    qint64 levelB = getLevel(1);

    m_type = des;
    setLevel(0, levelA);
    setLevel(1, levelB);

    m_type = type_bak;
}

int CSource::get_nScale()
{
    int nScale  = 1e3;
    if( (chan1 <= source) && (source <= chan4) )
    {
        QString name = QString("chan%1").arg(getChan());
        serviceExecutor::query( name, MSG_CHAN_SCALE_VALUE,  nScale);
    }
    return nScale;
}

int CSource::get_nOffset()
{
    int nOffset  = 0;

    int coup;
    int type;
    serviceExecutor::query( serv_name_trigger,      MSG_TRIGGER_TYPE,     type);
    serviceExecutor::query( serv_name_trigger_edge, MSG_TRIGGER_COUPLING, coup);

    if( (type== Trigger_Edge) && ( (coup== AC) || (coup== LF) ) )
    {
        nOffset = 0;
    }
    else if( (chan1 <= source) && (source <= chan4) )
    {
        QString name = QString("chan%1").arg(getChan());
        serviceExecutor::query( name, MSG_CHAN_OFFSET,  nOffset);
    }

    return nOffset;
}

float CSource::get_fRatio()
{
    float fRatio  = 1.0;
    if( (chan1 <= source) && (source <= chan4) )
    {
        QString name = QString("chan%1").arg(getChan());
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);
    }
    return fRatio;
}

qint64 CSource::get_nLevelMax()
{
    qint64 nMax  = 0;
    if( (chan1 <= source) && (source <= chan4) )
    {
        int nScale  = get_nScale();
        int nOffset = get_nOffset();
        nMax =  qAbs(Trigger_DIV_MAX*nScale  - nOffset);
    }
    else if( (d0<=source) && (source<=d15) )
    {
        nMax = (qint64)vv(20);
    }
    else if(ext == source)
    {
        nMax = (qint64)vv(8);
    }

    return nMax;
}

qint64 CSource::get_nLevelMin()
{
    qint64 nMin  = 0;
    if( (chan1 <= source) && (source <= chan4) )
    {
        int nScale  = get_nScale();
        int nOffset = get_nOffset();
        nMin = -qAbs(-Trigger_DIV_MAX*nScale - nOffset);
    }
    else if( (d0<=source) && (source<=d15) )
    {
        nMin = (qint64)vv(-20);
    }
    else if(ext == source)
    {
        nMin = (qint64)vv(-8);
    }
    return nMin;
}

qint64 CSource::get_nLevelDef()
{
    return (qint64)vv(0);
}

qint64 CSource::get_nLevelStep()
{
    qint64 step  = 0;
    if( (chan1 <= source) && (source <= chan4) )
    {
        int nScale  = get_nScale();
        step = (qint64)(nScale / adc_vdiv_dots);
    }
    else
    {
        step = (qint64)mv(10);
    }
    return step;
}

float CSource::get_fLevelMax()
{
    return get_nLevelMax()* ch_volt_base * get_fRatio();
}

float CSource::get_fLevelMin()
{
    return get_nLevelMin()* ch_volt_base * get_fRatio();
}

float CSource::get_fLevelDef()
{
    return get_nLevelDef()* ch_volt_base * get_fRatio();
}

float CSource::get_fLevel(int ab)
{
    float  fLevel  = 0.0;
    qint64 nLevel  = getLevel(ab);

    if( (chan1<=source) && (source <= chan4))
    {
        float fRatio =  get_fRatio();
        fLevel   = nLevel*ch_volt_base*fRatio;
    }
    else
    {
        fLevel   = nLevel*ch_volt_base;
    }

    return fLevel;
}

int CSource::get_nLevelPost(int ab)
{
    int post = 0;
    if( (chan1<=source) && (source <= chan4))
    {
        int    nScale    = get_nScale();
        int    nOffset   = get_nOffset();
        qint64 nLevel    = getLevel(ab);
        post  = (nLevel + nOffset) * scr_vdiv_dots /nScale ;  //将电平值转换为屏幕位置
    }
    return post;
}

qint64 CSource::get_nViewLevel(int ab)
{
    qint64 nLevel  = getLevel(ab);

    //! 模拟通道，显示时触发电平值是（电平线与通道GND的距离）;实际电平值是电压值。
    //! 电平线范围为屏幕范围内，通道GND不是屏幕范围内。

    if( (chan1<=source) && (source <= chan4))
    {
        int    nScale    =  get_nScale();
        int    nOffset   =  get_nOffset();
        float post  = (float((nLevel + nOffset) * scr_vdiv_dots)) /nScale ; //将电平值转换为屏幕位置(浮点数)

        post = qBound( (float)(-screen_height/2), post, (float)(screen_height/2) );

        nLevel = post*nScale/scr_vdiv_dots - nOffset;
    }

    return nLevel;
}

float CSource::get_fViewLevel(int ab)
{
    float  fLevel  = 0.0;
    qint64 nLevel  = get_nViewLevel(ab);

    if( (chan1<=source) && (source <= chan4))
    {
        float fRatio =  get_fRatio();
        fLevel   = nLevel*ch_volt_base*fRatio;
    }
    else
    {
        fLevel   = nLevel*ch_volt_base;
    }
    return fLevel;
}

Unit CSource::get_nUnit()
{
    int unit  = Unit_V;
    if( (chan1 <= source) && (source <= chan4) )
    {
        QString name = QString("chan%1").arg(getChan());
        serviceExecutor::query( name, MSG_CHAN_UNIT,  unit);
    }
    return (Unit)unit;
}

QString CSource::get_sUnit()
{
    QString unit  = "V";
    if( (chan1 <= source) && (source <= chan4) )
    {
        QString name = QString("chan%1").arg(getChan());
        serviceExecutor::query( name, servCH::qcmd_string_unit,  unit);
    }
    return unit;
}

sourceLevel *CSource::getLeverPtr(void)
{
    return getLeverPtr(m_type);
}

sourceLevel *CSource::getLeverPtr(CSource::sType src)
{
    switch(src)
    {
    case trig_source:
        return &m_trigLevel;
    case search_source:
        return &m_searchLevel;
    default:
        return NULL;
    }
}

