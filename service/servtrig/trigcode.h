#ifndef TRIGCODE_H
#define TRIGCODE_H

#include <QByteArray>
#include <QBitArray>

#define Trigger_Code_X  (0xff)
#define Trigger_Code_H  (1)
#define Trigger_Code_L  (0)

class trigCode
{
public:
    trigCode();

public:
    static void          setBitValue(QByteArray &data,
                                     QByteArray &mask,
                                     int bit,
                                     int code,
                                     int bytMax = 100);

    static int           getBitValue(QByteArray &data,
                                     QByteArray &mask,
                                     int bit,
                                     int bytMax = 100);

    static void          setBitValue(QBitArray &data,
                                     QBitArray &mask,
                                     int bit,
                                     int code,
                                     int bitMax = 100);

    static int           getBitValue(QBitArray &data,
                                     QBitArray &mask,
                                     int bit,
                                     int bitMax = 100);

    static int           get_z_value(int value,
                                     bool binHex,
                                     int  stop = -1);

    static void          setAllValue(QByteArray &data,
                                     QByteArray &mask,
                                     int bit,
                                     int code,
                                     int bytMax = 100);

    static void          setAllValue(QBitArray &data,
                                     QBitArray &mask,
                                     int bit,
                                     int code,
                                     int bitMax = 100);
public:
    static const  int    _z_value_bin[];
    static const  int    _z_value_hex[];
};

#endif // TRIGCODE_H
