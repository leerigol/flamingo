#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servPulseTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,       &servPulseTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,    &servPulseTrigger::applyLevel),
on_postdo( MSG_TRIGGER_POLARITY,     &servPulseTrigger::applyLevel),
end_of_postdo()


IMPLEMENT_CMD( TriggerBase, servPulseTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,          &servPulseTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_POLARITY,           &servPulseTrigger::setPolarity),
on_get_int      (       MSG_TRIGGER_POLARITY,           &servPulseTrigger::getPolarity),

on_set_int_int  (       MSG_TRIGGER_PULSE_WHEN,         &servPulseTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_PULSE_WHEN,         &servPulseTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_UPPER,        &servPulseTrigger::setUpper),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_UPPER,        &servPulseTrigger::getUpper, &servPulseTrigger::getUpperAttr),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_LOWER,        &servPulseTrigger::setLower),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_LOWER,        &servPulseTrigger::getLower, &servPulseTrigger::getLowerAttr),

on_set_int_pointer(     CMD_TRIGGER_PULSE_CFG,          &servPulseTrigger::setCfg),
on_get_pointer    (     CMD_TRIGGER_PULSE_CFG,          &servPulseTrigger::getCfg),

//!scpi
on_set_int_ll (         CMD_SCPI_TRIGGER_PULSE_WIDTH,   &servPulseTrigger::setWidth),
on_get_ll      (        CMD_SCPI_TRIGGER_PULSE_WIDTH,   &servPulseTrigger::getWidth),

on_set_void_void(       CMD_SERVICE_RST,                &servPulseTrigger::rst ),
on_set_int_void (       CMD_SERVICE_STARTUP,            &servPulseTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servPulseTrigger::setActive ),
end_of_entry()

#define ar ar_out
#define ar_pack_e ar_out
int servPulseCfg::cfg_pulse_serialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("m_Ch",        pulseCfg.mCh      );
    ar_pack_e("m_bPolarity", pulseCfg.mPolariy );
    ar_pack_e("m_nCmp",      pulseCfg.mCmp     );
    ar("m_s64Upper",         pulseCfg.mWidthH  );
    ar("m_s64Lower",         pulseCfg.mWidthL  );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servPulseCfg::cfg_pulse_serialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("m_Ch",        pulseCfg.mCh      );
    ar_pack_e("m_bPolarity", pulseCfg.mPolariy );
    ar_pack_e("m_nCmp",      pulseCfg.mCmp     );
    ar("m_s64Upper",         pulseCfg.mWidthH  );
    ar("m_s64Lower",         pulseCfg.mWidthL  );
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servPulseCfg::cfg_pulse_init()
{
    pulseCfg.setChan(chan1);
    pulseCfg.setPolarity(Trigger_pulse_positive);
    pulseCfg.setCmp(Trigger_When_Morethan);
    pulseCfg.setWidthL(time_us(1));
    pulseCfg.setWidthH(time_us(2));
}

bool servPulseCfg::cfg_pulse_getPolarity()
{
    return  (bool)pulseCfg.getPolarity();
}

int servPulseCfg::cfg_pulse_setPolarity(bool b)
{
    pulseCfg.setPolarity((TriggerPulsePolarity)b);
    return apply();
}

int servPulseCfg::cfg_pulse_getCmp()
{
    return pulseCfg.getCmp();
}

int servPulseCfg::cfg_pulse_setCmp(int c)
{
    pulseCfg.setCmp((EMoreThan)c);
    return apply();
}

qint64 servPulseCfg::cfg_pulse_getLower()
{
    return pulseCfg.getWidthL();
}

int servPulseCfg::cfg_pulse_setLower(qint64 value)
{
    pulseCfg.setWidthL(value);
    return apply();
}

qint64 servPulseCfg::cfg_pulse_getUpper()
{
    return pulseCfg.getWidthH();
}

int servPulseCfg::cfg_pulse_setUpper(qint64 value)
{
    pulseCfg.setWidthH(value);
    return apply();
}

int servPulseCfg::cfg_pulse_setSource(int src)
{
    pulseCfg.setChan((Chan)src);
    return apply();
}

int servPulseCfg::cfg_pulse_getSource()
{
    return pulseCfg.getChan();
}

void servPulseCfg::cfg_pulse_getTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    tL = cfg_pulse_getLower();
    tH = cfg_pulse_getUpper();
    w  = (EMoreThan)cfg_pulse_getCmp();
}

DsoErr servPulseCfg::cfg_pulse_setTimeRange(qint64 &tL, qint64 &tH)
{
    cfg_pulse_setLower(tL);
    return cfg_pulse_setUpper(tH);
}

int servPulseCfg::cfg_pulse_setCfg(void *p)
{
    TrigPulseCfg *pCfg = static_cast<TrigPulseCfg*>(p);
    Q_ASSERT(pCfg != NULL);
    pulseCfg = *pCfg;
    return apply();
}

void *servPulseCfg::cfg_pulse_getCfg()
{
    return &pulseCfg;
}

/*!--------------------------------------------------------------------------------*/
servPulseTrigger::servPulseTrigger(QString name)
    : TriggerBase(name,Trigger_Pulse, E_SERVICE_ID_TRIG_Pulse)
{
    serviceExecutor::baseInit();
    linkChange( MSG_TRIGGER_PULSE_WHEN,  MSG_TRIGGER_LIMIT_LOWER, MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_LOWER, MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_UPPER, MSG_TRIGGER_LIMIT_LOWER);
}

DsoErr servPulseTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servPulseTrigger::getChSlope(Chan ch)
{
    Q_ASSERT(ch == getSource());
    return (int)(!getPolarity());
}

void servPulseTrigger::getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    cfg_pulse_getTimeRange(tL, tH, w);
}

DsoErr servPulseTrigger::setTimeRangeValue(qint64 &tL, qint64 &tH)
{
    return cfg_pulse_setTimeRange(tL, tH);
}

int servPulseTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    servPulseCfg::cfg_pulse_serialOut(stream, ver);
    return 0;
}
int servPulseTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        servPulseCfg::cfg_pulse_serialIn(stream, ver);
    }
    else
    {
        rst();
    }
    return 0;
}
void servPulseTrigger::rst()
{
    TriggerBase::rst();
    servPulseCfg::cfg_pulse_init();
}

int servPulseTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servPulseTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

/**
 * @brief servPulseTrigger::getWhen
 * @return int
 */
int servPulseTrigger::getWhen(void)
{
    //    return cfg_pulse_getWhen();
    return cfg_pulse_getCmp();
}

/**
 * @brief servPulseTrigger::setWhen
 * @param s
 */
int servPulseTrigger::setWhen(int s )
{
    EMoreThan w0 = (EMoreThan)getWhen();

    DsoErr err = cfg_pulse_setCmp(s);

    checkTimeRange(T_Rule_None,w0);
    return err;
}

/**
 * @brief servPulseTrigger::getPolarity
 * @return
 */
bool servPulseTrigger::getPolarity(void)
{
    return cfg_pulse_getPolarity();
}

/**
 * @brief servPulseTrigger::setPolarity
 * @param b
 */
int servPulseTrigger::setPolarity(bool b)
{
    return cfg_pulse_setPolarity(b);
}

/**
 * @brief servPulseTrigger::getLower
 * @return
 */
qint64 servPulseTrigger::getLower(void)
{
    return cfg_pulse_getLower();
}

/**
 * @brief servPulseTrigger::setLower
 * @param t
 * @return
 */
int servPulseTrigger::setLower(qint64 t)
{
    DsoErr err = cfg_pulse_setLower(t);
    checkTimeRange(T_Rule_L);
    return err;
}

/**
 * @brief servPulseTrigger::getUpper
 * @return
 */
qint64 servPulseTrigger::getUpper(void)
{
    return cfg_pulse_getUpper();
}

/**
 * @brief servPulseTrigger::setUpper
 * @param t
 * @return
 */
int servPulseTrigger::setUpper(qint64 t)
{
    DsoErr err = cfg_pulse_setUpper(t);
    checkTimeRange(T_Rule_H);
    return err;
}

int servPulseTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        return cfg_pulse_setSource(src);
    }
}

DsoErr servPulseTrigger::setCfg(pointer p)
{
    DsoErr err = cfg_pulse_setCfg(p);
    ssync( MSG_TRIGGER_SOURCE_LA, cfg_pulse_getSource() );

    updateAllUi();
    return err;
}

pointer servPulseTrigger::getCfg()
{
    return cfg_pulse_getCfg();
}

int servPulseTrigger::setWidth(qint64 time)
{
    DsoErr err  = ERR_NONE;
    qint64 diff = 0;

    if( Trigger_When_Morethan == pulseCfg.getCmp() )
    {
        err = async(MSG_TRIGGER_LIMIT_LOWER, time);
    }
    else if( Trigger_When_Lessthan == pulseCfg.getCmp() )
    {
        err = async(MSG_TRIGGER_LIMIT_UPPER, time);
    }
    else
    {
        diff =   pulseCfg.getWidthH() - pulseCfg.getWidthL();
        err = async(MSG_TRIGGER_LIMIT_LOWER, time);
        err = async(MSG_TRIGGER_LIMIT_UPPER, time + diff);
    }

    return err;
}

qint64 servPulseTrigger::getWidth()
{
    qint64 time = 0;

    switch (pulseCfg.getCmp())
    {
    case Trigger_When_Morethan:
        time = pulseCfg.getWidthL();
        break;

    case Trigger_When_Lessthan:
        time = pulseCfg.getWidthH();
        break;

    case Trigger_When_MoreLess:
        time = pulseCfg.getWidthL();
        break;

    default:
        time = 0;
        break;
    }
    return time;
}
