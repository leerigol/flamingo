#include "servtrig.h"

IMPLEMENT_POST_DO( TriggerBase, servOverTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servOverTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE,            &servOverTrigger::applyLevel),
on_postdo( MSG_TRIGGER_OVER_SLOPE,        &servOverTrigger::applyLevel),
on_postdo( MSG_TRIGGER_OVER_POS,          &servOverTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servOverTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_LEVELSELECT,        &TriggerBase::setLevelSelect),
on_get_int      (       MSG_TRIGGER_LEVELSELECT,        &TriggerBase::getLevelSelect),

on_set_int_int  (       MSG_TRIGGER_SOURCE,             &servOverTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_OVER_SLOPE,         &servOverTrigger::setSlope),
on_get_int      (       MSG_TRIGGER_OVER_SLOPE,         &servOverTrigger::getSlope),

on_set_int_int  (       MSG_TRIGGER_OVER_POS,           &servOverTrigger::setEvent),
on_get_int      (       MSG_TRIGGER_OVER_POS,           &servOverTrigger::getEvent),

on_set_int_ll   (       MSG_TRIGGER_OVER_TIME,          &servOverTrigger::setTime),
on_get_ll       (       MSG_TRIGGER_OVER_TIME,          &servOverTrigger::getTime),

//!scpi
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_L,        &servOverTrigger::setLevelL),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_L,        &servOverTrigger::getLevelL),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_H,        &servOverTrigger::setLevelH),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_H,        &servOverTrigger::getLevelH),

on_set_void_void(       CMD_SERVICE_RST,                &servOverTrigger::rst ),
on_set_int_void (       CMD_SERVICE_STARTUP,            &servOverTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servOverTrigger::setActive ),
end_of_entry()


servOverTrigger::servOverTrigger(QString name)
    : TriggerBase(name,Trigger_Over, E_SERVICE_ID_TRIG_Over, true)
{
    serviceExecutor::baseInit();
}

DsoErr servOverTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servOverTrigger::getChSlope(Chan ch)
{
    int slope = getSlope();

    if( Trigger_over_exit == getEvent() )
    {
        switch (slope) {
        case Trigger_Edge_Rising:
            slope = Trig_View_Falling_L;
            break;
        case Trigger_Edge_Falling:
            slope = Trig_View_Rising_H;
            break;
        case Trigger_Edge_Alternating:
            slope = Trig_View_Any_L_H;
            break;
        case Trigger_Edge_Any:
            slope = Trig_View_Any_L_H;
            break;
        default:
            break;
        }
    }
    Q_ASSERT( ch == getSource() );
    return slope;
}

int servOverTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);

    stream.write(QStringLiteral("source"),      (int)cfg.mCh);
    stream.write(QStringLiteral("slope"),       (int)cfg.mSlope);
    stream.write(QStringLiteral("event"),       (int)cfg.mEvent);
    stream.write(QStringLiteral("width"),       cfg.mWidth);

    return 0;
}

int servOverTrigger::serialIn(CStream &stream, unsigned char ver)
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        int value = 0;
        stream.read(QStringLiteral("source"),     value);
        cfg.setChan((Chan)value);

        stream.read(QStringLiteral("slope"),      value);
        cfg.setSlope((EdgeSlope)value);

        stream.read(QStringLiteral("event"),      value);
        cfg.setEvent((OverEvent)value);

        stream.read(QStringLiteral("width"),      cfg.mWidth);
    }
    else
    {
        rst();
    }
    return 0;
}

void servOverTrigger::rst()
{
    TriggerBase::rst();
    cfg.setChan((Chan)(getSource()));
    cfg.setSlope(Trigger_Edge_Rising);
    cfg.setEvent(Trigger_over_enter);
    cfg.setWidth(time_us(1));
}

int servOverTrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servOverTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

EdgeSlope servOverTrigger::getSlope()
{
    EdgeSlope s = cfg.getSlope();
    return s;
}

DsoErr servOverTrigger::setSlope(EdgeSlope s)
{
    cfg.setSlope(s);
    return apply();
}

qint64 servOverTrigger::getTime()
{
    //! update attr
    setuiStep( getTimeStep(cfg.getWidth() ) );
    setuiRange(time_ns(8),time_s(10),time_ns(8));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return cfg.getWidth();

}

DsoErr servOverTrigger::setTime(qint64 t)
{
    cfg.setWidth( t );
    return apply();
}

DsoErr servOverTrigger::setEvent(OverEvent evt)
{
    cfg.setEvent(evt);
    return apply();
}

OverEvent servOverTrigger::getEvent()
{
    return cfg.getEvent();
}

int servOverTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        cfg.setChan( Chan(src) );
        TriggerBase::setSource(src);
        return apply();
    }
}

DsoErr servOverTrigger::setSourcesTwoLevel(bool b)
{
    for(int key = chan1; key <= chan4; key++)
    {
        if( b )
        {
            TriggerBase::m_pSources[key]->setTwoLevel(true, 0);
            TriggerBase::m_pSources[key]->setTwoLevel(true, 1);
        }
        else
        {
            if(getLevelSelect() == Trigger_Level_Low)
            {
                TriggerBase::m_pSources[key]->setTwoLevel(false,  0);
                TriggerBase::m_pSources[key]->setTwoLevel(true,   1);
            }
            else
            {
                TriggerBase::m_pSources[key]->setTwoLevel(true,  0);
                TriggerBase::m_pSources[key]->setTwoLevel(false, 1);

            }
        }
    }

    return ERR_NONE;
}

int servOverTrigger::setLevelL(qint64 l)
{
    setSlope(Trigger_Edge_Falling);
    ssync(MSG_TRIGGER_LEVELSELECT, Trigger_Level_Low);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servOverTrigger::getLevelL()
{
    ssync(MSG_TRIGGER_LEVELSELECT, Trigger_Level_Low);
    Chan   ch        = (Chan)getSource();
    qint64 l         = getSourceObj(ch)->getLevel(Trigger_Level_Low);
    return l*getProbeRatio(ch);
}

int servOverTrigger::setLevelH(qint64 l)
{
    ssync(MSG_TRIGGER_LEVELSELECT, Trigger_Level_High);
    setSlope(Trigger_Edge_Rising);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 servOverTrigger::getLevelH()
{
    ssync(MSG_TRIGGER_LEVELSELECT, Trigger_Level_High);
    Chan   ch        = (Chan)getSource();
    qint64 l         = getSourceObj(ch)->getLevel(Trigger_Level_High);
    return l*getProbeRatio(ch);
}
