#ifndef SERVTRIG_H
#define SERVTRIG_H

#include "../service.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "../servgui/servgui.h"
#include "../../baseclass/iserial.h"
#include "../../engine/module/ctrigger.h"
#include "../../include/dsotype.h"
#include "../../include/dsostd.h"
#include "../../engine/base/enginecfg.h"
#include "../../com/scpiparse/cconverter.h"
#include "source.h"
#include "trigrules.h"

using namespace  scpi_parse;
using namespace  DsoTrigSource;

#define SET_TRIG_TIME_ATTR(t, tMin, tMax, tDef)  \
    do{                                          \
    setuiUnit(Unit_s);                           \
    setuiBase(1, E_N12);                         \
    setuiPostStr( "s" );                         \
    setuiRange(tMin, tMax, tDef);                \
    setuiStep( (t<time_ns(100)) ? time_ps(400): getTimeStep(t) );}while(0)

#define CHECK_WORKING_TRIGGER() {if( getMode() != m_nMyID ) return ERR_NONE;}
#define Trigger_DIV_MAX (5)

typedef enum
{
    Trig_View_Rising_H = 0,
    Trig_View_Falling_L,
    Trig_View_Alternating_H_L,
    Trig_View_Any_H_L,

    Trig_View_Rising_L,
    Trig_View_Falling_H,
    Trig_View_Alternating_L_H,
    Trig_View_Any_L_H,

    Trig_View_Alternating_H_H,
    Trig_View_Alternating_L_L,
    Trig_View_Any_H_H,
    Trig_View_Any_L_L,
    Trig_View_Any_None,

}TriggerSlopeLevelView;

struct _levelSlopeInc
{
    TriggerSlopeLevelView    slope;
    int                      aMaxInc;
    int                      aMinInc;
    int                      bMaxInc;
    int                      bMinInc;
};

class TriggerBase : public serviceExecutor, public ISerial
{ 
    Q_OBJECT
    DECLARE_CMD()
public:
    TriggerBase( QString name,
                 TriggerMode m = Trigger_Edge,
                 ServiceId eId = E_SERVICE_ID_TRIG,
                 bool hasTwoLevel = false );

    virtual  int           serialOut(CStream &stream, unsigned char &ver);
    virtual  int           serialIn( CStream &stream, unsigned char ver );
    virtual  void          rst();
    virtual  int           startup();
    virtual  void          registerSpy();
    DsoErr                 setActive( int active=1 );

public:
    DsoErr                 setItemFocus( int msg );
    int                    getItemFocus();

public:
    static int             commomSerialOut(CStream &stream, unsigned char &ver);
    static int             commomSerialIn( CStream &stream, unsigned char ver );
    static QString         currTrigger(void);
    static TriggerMode     currTriggerMode(void);
    static QString         getTriggerName(TriggerMode mode);
    static float           getProbeRatio(Chan ch);
    static DsoErr          setSweep( TriggerSweep sweep );
    static TriggerSweep    getSweep(void);
    static TriggerSweep    getSingleLast(void);

public:  
    int                    setMode( TriggerMode mode );
    TriggerMode            getMode(void);
    TriggerMode            getSelfMode(void) const;
    virtual DsoErr         apply()   = 0;
                       
    Coupling               getCoupling(void);
    int                    setCoupling(Coupling coup);
                       
    bool                   getNoiseReject(void);
    int                    setNoiseReject(bool);
    DsoErr                 setTrigHoldoff(qint64);
    qint64                 getTrigHoldoff(void);
                       
public:                
    int                    setTrigHoldObj( TriggerHoldObj hold );
    int                    setTrigHoldMode( TriggerHoldMode mode );
    int                    setTrigHoldCount( int count );
    int                    setSourceEN(int src);
    int                    setSource(int src);
    int                    getSource(void);
    CSource*               getSourcePtr(void);

    static CSource*        getChanSource(int ch);

    virtual qint64         getLevel(void);
    virtual int            setLevel(qint64 level = 0);  //  input is voltage of uV
    virtual int            getChSlope(Chan ch);
    virtual DsoErr         applyLevel();
    void                   dispLevel(void);

    DsoErr                 initLevelEngine(); //! 将所有通道的阈值重新配置一遍。
    DsoErr                 applyLevelEngine(Chan   ch,
                                            qint64 gnd   = 0,
                                            int    slope = Trig_View_Any_None);

    int                    sourceInvert(Chan ch);

    int                    setForce();

    Trigger_Level_ID       getLevelSelect(void);
    int                    setLevelSelect(Trigger_Level_ID id);
                         
    bool                   getDoubleLevel();
    virtual DsoErr         setSourcesTwoLevel(bool b); //!默认情况 设置lecelA显示, levelB = b;
                         
    virtual void           spyLicenseOpt();

public:
    virtual DsoErr          checkTimeRange(TrigTimeRule rule,
                                          const EMoreThan w0   = Trigger_When_None,
                                          const qint64    tMin = time_ps(800),
                                          const qint64    tMax = time_s(10));

    virtual  void          getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w);
    virtual  DsoErr        setTimeRangeValue(qint64 &tL, qint64 &tH);


    virtual  void          getUpperAttr( enumAttr );
    virtual  void          getLowerAttr( enumAttr );


protected:               
    CSource*               getSourceObj(int key = chan1);
    void                   resetSource(bool b = false);

protected:
    CSource*               m_pCurrSource;
    TriggerMode            m_nMyID;         // save the current trigger type ID
    Trigger_Level_ID       m_nLevelSelect;  //!触发电平选择
    bool                   m_bDoubleLevel;  //!true 两个触发电平相互关系

    /** all source shared with all trigger type. */
    static CSource*        m_pSources[all_trig_src];
                       
    static QString         m_TriggerName[Trigger_All];
    static TriggerSweep    m_nSweep;
    static TriggerSweep    m_nSingleLast;
    static TriggerMode     m_nMode;
    static Coupling        m_nCouple;
    static qint64          m_s64Holdoff;
    static bool            m_bNoiseReject;
    //external status      
    static bool            m_bRoll;
    static bool            m_bInvert[chan4+1];

protected:
    static int             m_itemFocus;

public:
    const  static qint64   G_s64LowerDefault = time_us(1);
    const  static qint64   G_s64UpperDefault = time_us(2);
    static const  int      TRIG_CURRBIT_Z_VALUE = 0xABCDEF00;

    static const _levelSlopeInc s_a_levelSlopeInc[Trig_View_Any_None+1];
};

/**
 * @brief The servEdgeTrigger class
 */
class servEdgeCfg
{
public:
    servEdgeCfg(){}
    int       cfg_edge_serialOut(CStream &stream, unsigned char &ver);
    int       cfg_edge_serialIn( CStream &stream, unsigned char ver );
    void      cfg_edge_init();
    virtual DsoErr  apply() = 0;
public:
    int       cfg_edge_getWhen(void);
    int       cfg_edge_setWhen(int w) ;
    int       cfg_edge_setCh(int ch);
    int       cfg_edge_getCh();

    int        cfg_edge_setCfg(void *p);
    void      *cfg_edge_getCfg();
protected:
    TrigEdgeCfg edgeCfg;
};
class servEdgeTrigger       : public TriggerBase, servEdgeCfg
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:  
    servEdgeTrigger(QString name = serv_name_trigger_edge);
    int                serialOut(CStream &stream, unsigned char &ver);
    int                serialIn( CStream &stream, unsigned char ver );
    void               rst();
    int                startup();
    virtual  void      registerSpy();

    DsoErr             setActive( int active=1 );
    virtual DsoErr     apply(){ return postEngine( ENGINE_TRIGGER_EDGE_CFG, &edgeCfg);}
    virtual int        getChSlope(Chan ch);

    virtual DsoErr     applyLevel();

public:
    int                getWhen(void);
    int                setWhen(int w);
                     
    int                setSource(int src);
                     
    int                setLevel(qint64 level = 0);
                     
    void               onProjectMode(void);
                     
//    void               spyHoriTimeMode(AcquireTimemode mode);
                     
    DsoErr             setCfg(pointer p);
    pointer            getCfg();
};

class servABTrigger       : public TriggerBase
{
    Q_OBJECT

    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servABTrigger(QString name = serv_name_trigger_ab);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply(){ return postEngine( ENGINE_TRIGGER_AB_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);
public:
    int             setSource(int src);
                    
    int             getEdgeA(void);
    int             setEdgeA(int);
                    
    bool            getPolarity(void);
    int             setPolarity(bool);
                    
    int             getWhen(void);
    int             setWhen(int);
                    
    qint64          getLower(void);
    int             setLower(qint64);
                    
    qint64          getUpper(void);
    int             setUpper(qint64);
                    
    int             getEdgeB(void);
    int             setEdgeB(int);
                    
    qint64          getDelay(void);
    int             setDelay(qint64);
                    
    int             getCount(void);
    int             setCount(int);
    
    int             setAEventMode(TriggerMode mode);
    TriggerMode     getAEventMode(void);

private:
    TrigABCfg cfg;
};

/**
 * @brief The servPulseTrigger class
 */
class servPulseCfg
{
public:
    enum
    {
        pulse_p_greater = 1,
        pulse_p_less,
        pulse_p_greater_less,

        pulse_n_greater,
        pulse_n_less,
        pulse_n_greater_less,
    };

public:
    servPulseCfg(){}
    int       cfg_pulse_serialOut(CStream &stream, unsigned char &ver);
    int       cfg_pulse_serialIn( CStream &stream, unsigned char ver );
    void      cfg_pulse_init();
    virtual DsoErr  apply() = 0;
public:
    bool      cfg_pulse_getPolarity(void);
    int       cfg_pulse_setPolarity(bool b);

    int       cfg_pulse_getCmp(void);
    int       cfg_pulse_setCmp(int c);

    qint64    cfg_pulse_getLower(void);
    int       cfg_pulse_setLower(qint64 value);

    qint64    cfg_pulse_getUpper(void);
    int       cfg_pulse_setUpper(qint64 value);

    int       cfg_pulse_setSource(int src);
    int       cfg_pulse_getSource();

    void      cfg_pulse_getTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w);
    DsoErr    cfg_pulse_setTimeRange(qint64 &tL, qint64 &tH);

    int       cfg_pulse_setCfg(void *p);
    void      *cfg_pulse_getCfg();
protected:
    TrigPulseCfg pulseCfg;
};
class servPulseTrigger      : public TriggerBase, servPulseCfg
{
    Q_OBJECT

    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servPulseTrigger(QString name = serv_name_trigger_pulse);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply(){return postEngine( ENGINE_TRIGGER_PULSE_CFG, &pulseCfg);}
    virtual int     getChSlope(Chan ch);
    virtual void    getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w);
    virtual DsoErr  setTimeRangeValue(qint64 &tL, qint64 &tH);

public:
    bool            getPolarity(void);
    int             setPolarity(bool b);
                  
    int             getWhen(void);
    int             setWhen(int);
                  
    qint64          getLower(void);
    int             setLower(qint64);

    qint64          getUpper(void);
    int             setUpper(qint64);

    int             setSource(int src);
                  
    DsoErr          setCfg(pointer p);
    pointer         getCfg();

//!SCPI           
public:           
    int             setWidth(qint64 time);
    qint64          getWidth(void);
};

/*!
 * \brief The servSlopeCfg class
 */
class servSlopeCfg
{
public:
    enum
    {
        slope_p_greater = 1,
        slope_p_less,
        slope_p_greater_less,

        slope_n_greater,
        slope_n_less,
        slope_n_greater_less,
    };

public:
    servSlopeCfg(){}
    int       cfg_Slope_serialOut(CStream &stream, unsigned char &ver);
    int       cfg_Slope_serialIn( CStream &stream, unsigned char ver );
    void      cfg_slope_init();
    virtual DsoErr  apply() = 0;
public:
    qint64    cfg_slope_getLower(void);
    int       cfg_slope_setLower(qint64 t);

    qint64    cfg_slope_getUpper(void);
    int       cfg_slope_setUpper(qint64 t);

    int       cfg_slope_setSource(int src);
    int       cfg_slope_getSource();

    int       cfg_slope_setCmp(int cmp);
    int       cfg_slope_getCmp();

    int       cfg_slope_setSlope(int slope);
    int       cfg_slope_getSlope();

    void      cfg_slope_getTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w);
    DsoErr    cfg_slope_setTimeRange(qint64 &tL, qint64 &tH);

    int       cfg_slope_setCfg(void *p);
    void      *cfg_slope_getCfg();
protected:
    TrigSlopeCfg slopeCfg;
};
class servSlopeTrigger      : public TriggerBase,servSlopeCfg
{
    Q_OBJECT

    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servSlopeTrigger(QString name = serv_name_trigger_slope);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply(){ return postEngine( ENGINE_TRIGGER_SLOPE_CFG, &slopeCfg);}
    virtual int     getChSlope(Chan ch);
    virtual void    getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w);
    virtual DsoErr  setTimeRangeValue(qint64 &tL, qint64 &tH);

public:
    bool            getSlope(void);
    int             setSlope(bool b);
                  
    int             getWhen(void);
    int             setWhen(int w);
                  
    qint64          getLower(void);
    int             setLower(qint64);
                  
    qint64          getUpper(void);
    int             setUpper(qint64);

    int             setSource(int src);
                  
    DsoErr          setCfg(pointer p);
    pointer         getCfg();
//!scpi           
   DsoErr           setTime(qint64 time);
   qint64           getTime();
                  
   int              setLevelL(qint64 l = 0);
   qint64           getLevelL(void);
   int              setLevelH(qint64 l = 0);
   qint64           getLevelH(void);

};

class servVideoTrigger      : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servVideoTrigger(QString name = serv_name_trigger_video);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply()  { return postEngine( ENGINE_TRIGGER_VIDEO_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);

public:
    bool            getPolarity(void);
    int             setPolarity(bool);
                
    int             getStardard(void);
    int             setStardard(int);
                
    int             getSync(void);
    int             setSync(int);
                
    int             getLineNum(void);
    int             setLineNum(int);
                
    int             setSource(int src);
                
    int             spyScale140();

private:
    int             m_dispGrids;
    TrigVideoCfg cfg;
};

class CodeFormater:public CFormater
{
public:
    CodeFormater(){}
public:
    virtual scpiError format( GCVal &val );
};
class servPatternTrigger    : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servPatternTrigger(QString name = serv_name_trigger_pattern);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() { return postEngine( ENGINE_TRIGGER_LOGIC_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);
    virtual DsoErr  applyLevel();
public:
    void          getCode(CArgument &arg);
    int             setCode(CArgument &arg);
    QString     getScpiCode();
                
    int             getValue(void);
    int             setValue(int);
                
    int             setAll(void);
                
    int             setSource(int src);

//!scpi
    DsoErr          setSourceLevel(CArgument &arg);
    void            getSourceLevel(CArgument &argi, CArgument &argo);

private:
    TrigPatternCfg cfg;
};

class servDurationTrigger   : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servDurationTrigger(QString name = serv_name_trigger_duration);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply()  {return postEngine( ENGINE_TRIGGER_DURATION_CFG, &cfg);}

    virtual int     getChSlope(Chan ch);
    virtual DsoErr  applyLevel();
    virtual void    getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w);
    virtual DsoErr  setTimeRangeValue(qint64 &tL, qint64 &tH);

public:
    int             setSource(int src);
                 
    int             getValue(void);
    int             setValue(int);
                 
    void          getCode(CArgument &arg);
    int             setCode(CArgument &arg);
    QString     getScpiCode();
                 
    int             getWhen(void);
    int             setWhen(int w);
                 
    qint64          getLower(void);
    int             setLower(qint64 t);
                 
    qint64          getUpper(void);
    int             setUpper(qint64 t);

    int             setAll(void);

//!scpi
    DsoErr          setSourceLevel(CArgument &arg);
    void            getSourceLevel(CArgument &argi, CArgument &argo);

private:
    TrigDurationCfg cfg;
};

class servTimeoutTrigger    : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servTimeoutTrigger(QString name = serv_name_trigger_timeout);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() { return postEngine( ENGINE_TRIGGER_TIMEOUT_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);

public:
    qint64          getTimeout(void);
    int             setTimeout(qint64);
                 
    int             getWhen(void);
    int             setWhen(int);
                 
    int             setSource(int src);
                 
    int             setLevel(qint64 level = 0);
                 
private:         
    qint64          m_s64Timeout;
    int             m_nWhen;

private:
    TrigTimeoutCfg cfg;
};

/*!
 * \brief The servRuntCfg class
 */
class servRuntCfg
{
public:
    servRuntCfg(){}
    int       cfg_runt_serialOut(CStream &stream, unsigned char &ver);
    int       cfg_runt_serialIn( CStream &stream, unsigned char ver );
    void      cfg_runt_init();
    virtual DsoErr  apply() = 0;
public:
    bool      cfg_runt_getPolarity(void);
    int       cfg_runt_setPolarity(bool b);

    int       cfg_runt_getWhen(void);
    int       cfg_runt_setWhen(int w);

    qint64    cfg_runt_getLower(void);
    int       cfg_runt_setLower(qint64 t);

    qint64    cfg_runt_getUpper(void);
    int       cfg_runt_setUpper(qint64 t);

    int       cfg_runt_setSource(int src);
    int       cfg_runt_getSource();

    void      cfg_runt_getTimeRange(qint64 &tL, qint64 &tH, EMoreThan &w);
    DsoErr    cfg_runt_setTimeRange(qint64 &tL, qint64 &tH);

    int       cfg_runt_setCfg(void *p);
    void      *cfg_runt_getCfg();
protected:
    TrigRuntCfg runtCfg;
};
class servRuntTrigger       : public TriggerBase, servRuntCfg
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servRuntTrigger(QString name = serv_name_trigger_runt);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_RUNT_CFG, &runtCfg);}
    virtual int     getChSlope(Chan ch);

    virtual void    getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w);
    virtual DsoErr  setTimeRangeValue(qint64 &tL, qint64 &tH);
    virtual  void   getUpperAttr( enumAttr );
    virtual  void   getLowerAttr( enumAttr );
public:
    bool            getPolarity(void);
    int             setPolarity(bool);
                   
    int             getWhen(void);
    int             setWhen(int w);
                   
    qint64          getLower(void);
    int             setLower(qint64);
                   
    qint64          getUpper(void);
    int             setUpper(qint64);

    int             setSource(int src);
                   
    DsoErr          setCfg(pointer p);
    pointer         getCfg();
//!scpi          
    int             setLevelL(qint64 l = 0);
    qint64          getLevelL(void);
    int             setLevelH(qint64 l = 0);
    qint64          getLevelH(void);
};

class servOverTrigger     : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servOverTrigger(QString name = serv_name_trigger_over);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() { return postEngine( ENGINE_TRIGGER_OVER_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);

public:
    EdgeSlope       getSlope(void);
    DsoErr          setSlope(EdgeSlope s);
                  
    qint64          getTime(void);
    DsoErr          setTime(qint64 t);
                  
    DsoErr          setEvent( OverEvent evt);
    OverEvent       getEvent();
                  
    int             setSource(int src);
                  
    DsoErr          setSourcesTwoLevel(bool b);

    //!scpi
    int             setLevelL(qint64 l = 0);
    qint64          getLevelL(void);
    int             setLevelH(qint64 l = 0);
    qint64          getLevelH(void);

private:
    TrigOverCfg cfg;
};

class servWindowTrigger     : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    enum
    {
        WINDOW_ENTER,
        WINDOW_EXIT,
        WINDOW_INSIDE,
        WINDOW_OUTSITE
    };
    servWindowTrigger(QString name = serv_name_trigger_window);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply(){ return ERR_NONE; }
    virtual int     getChSlope(Chan ch);

public:
    int             getWhen(void);
    int             setWhen(int);
                 
    int             getEdgeType(void);
    int             setEdgeType(int);
                 
    qint64          getTime(void);
    int             setTime(qint64);

private:
    int             m_nWhen;
    int             m_nEdgeType;
    qint64          m_s64Time;

private:
    TrigWindowCfg cfg;

};

class servDelayTrigger      : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servDelayTrigger(QString name = serv_name_trigger_delay);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() { return postEngine( ENGINE_TRIGGER_DELAY_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);
    virtual DsoErr  applyLevel();
    virtual void    getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w);
    virtual DsoErr  setTimeRangeValue(qint64 &tL, qint64 &tH);
    virtual  void   getUpperAttr( enumAttr );
    virtual  void   getLowerAttr( enumAttr );

public:
    int             getWhen(void);
    int             setWhen(int);
                  
    qint64          getLower(void);
    int             setLower(qint64);
                  
    qint64          getUpper(void);
    int             setUpper(qint64);
                  
    int             getSourceA(void);
    int             setSourceA(int src = chan1);
                  
    bool            getEdgeA(void);
    int             setEdgeA(bool e);
                  
    int             getSourceB(void);
    int             setSourceB(int src = chan2);
                  
    bool            getEdgeB(void);
    int             setEdgeB(bool e);

public:
    int              setLevelSrcA(qint64 l = 0);
    qint64           getLevelSrcA(void);
    int              setLevelSrcB(qint64 l = 0);
    qint64           getLevelSrcB(void);

private:          
    bool            m_bEdgeA;
    bool            m_bEdgeB;
    int             m_nSrcA;
    int             m_nSrcB;
    int             m_nWhen;
    qint64          m_s64Lower;
    qint64          m_s64Upper;
private:
    TrigDelayCfg cfg;
};

class servSetupTrigger      : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servSetupTrigger(QString name = serv_name_trigger_setup);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() { return postEngine( ENGINE_TRIGGER_SETUPHOLD_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);
    virtual DsoErr  applyLevel();
public:
    int             getWhen(void);
    int             setWhen(int);
                    
    qint64          getSetupTime(void);
    int             setSetupTime(qint64);
                    
    qint64          getHoldTime(void);
    int             setHoldTime(qint64);
                    
    int             getSCL(void);
    int             setSCL(int src = chan1);
                    
    int             getSCLEdge(void);
    int             setSCLEdge(int e);
                    
                    
    int             getSDA(void);
    int             setSDA(int src = chan2);
                    
    int             getSDAEdge(void);
    int             setSDAEdge(int pola);

public:
    int              setLevelSCL(qint64 l = 0);
    qint64           getLevelSCL(void);
    int              setLevelSDA(qint64 l = 0);
    qint64           getLevelSDA(void);

private:
    TrigSHCfg cfg;
};

class servNthTrigger    : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servNthTrigger(QString name = serv_name_trigger_nth);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() { return postEngine( ENGINE_TRIGGER_NEDGE_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);

public:
    int             getWhen(void);
    int             setWhen(int s);
    int             setSource(int src);
                 
    qint64          getIdleTime(void);
    int             setIdleTime(qint64);
    int             getEdgeNum(void);
    int             setEdgeNum(int e);

private:
    TrigNEdgeCfg cfg;
};



/*!
 * \brief The servRs232Cfg class
 */
class servRs232Cfg
{
public:
    servRs232Cfg(){}
    int       cfg_rs232_serialOut(CStream &stream, unsigned char &ver);
    int       cfg_rs232_serialIn( CStream &stream, unsigned char ver );
    void      cfg_rs232_init();
    virtual DsoErr  apply() = 0;
public:
    bool      cfg_rs232_getPolarity(void);
    DsoErr    cfg_rs232_setPolarity(bool b);

    int       cfg_rs232_getWhen(void);
    DsoErr    cfg_rs232_setWhen(int w);

    int       cfg_rs232_getBaudRate(void);
    DsoErr    cfg_rs232_setBaudRate(int baud);

    int       cfg_rs232_getParity(void);
    DsoErr    cfg_rs232_setParity(int par);

    int       cfg_rs232_getStopBits(void);
    DsoErr    cfg_rs232_setStopBits(int bit);

    int       cfg_rs232_getDataBits(void);
    DsoErr    cfg_rs232_setDataBits(int bit);

    int       cfg_rs232_getData(void);
    DsoErr    cfg_rs232_setData(int data);

    int       cfg_rs232_getSource();
    DsoErr    cfg_rs232_setSource(int src);

    int       cfg_rs232_setCfg(void *p);
    void      *cfg_rs232_getCfg();
protected:
    int       m_when;
    TrigRS232Cfg rs232Cfg;
};
class servRS232Trigger      : public TriggerBase,servRs232Cfg
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servRS232Trigger(QString name = serv_name_trigger_rs232);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    virtual  int    startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_UART_CFG, &rs232Cfg);}
    virtual int     getChSlope(Chan ch);

public:
    bool            getPolarity(void);
    int             setPolarity(bool);
                  
    int             getWhen(void);
    int             setWhen(int);
                  
    int             getBaudRate(void);    
    int             setBaudRate(int);
                  
    int             getParity(void);
    int             setParity(int);
                  
    int             getStopBits(void);
    int             setStopBits(int);
                  
    int             getDataBits(void);
    int             setDataBits(int);
                  
    int             getData(void);
    int             setData(int data);
                  
    int             setSource(int src);
                  
    DsoErr          setCfg(pointer p);
    pointer         getCfg();
};

class servI2cCfg
{

public:
    servI2cCfg(){}
    int       cfg_i2c_serialOut(CStream &stream, unsigned char &ver);
    int       cfg_i2c_serialIn( CStream &stream, unsigned char ver );
    void      cfg_i2c_init();
    virtual DsoErr  apply() = 0;

public:
    int             cfg_get_i2c_When(void);
    int             cfg_set_i2c_When(int s);

    int             cfg_get_i2c_Dir(void);
    int             cfg_set_i2c_Dir(int b);

    int             cfg_get_i2c_AddrBits(void);
    int             cfg_set_i2c_AddrBits(int a);

    int             cfg_get_i2c_Address(void);
    int             cfg_set_i2c_Address(int a);

    int             cfg_get_i2c_data_width(void);
    int             cfg_set_i2c_data_width(int byt);

    QByteArray&     cfg_get_i2c_Data_Min();
    QByteArray&     cfg_get_i2c_Data_Max();
    QByteArray&     cfg_get_i2c_Data_Mask();

    int             cfg_get_i2c_SCL(void);
    int             cfg_set_i2c_SCL(int src = chan1);

    int             cfg_get_i2c_SDA(void);
    int             cfg_set_i2c_SDA(int src = chan2);

    int             cfg_i2c_setCfg(void *p);
    void            *cfg_i2c_getCfg();
protected:
    TrigIICcfg i2cCfg;

};

class servI2CTrigger        : public TriggerBase, servI2cCfg
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servI2CTrigger(QString name = serv_name_trigger_i2c);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_I2C_CFG, &i2cCfg);}
    virtual int     getChSlope(Chan ch);
    virtual DsoErr  applyLevel();
public:
    int             getWhen(void);
    int             setWhen(int s);
                  
    int             getDir(void);
    int             setDir(int b);
                  
    int             getAddrBits(void);
    int             setAddrBits(int);
                  
    int             getAddress(void);
    int             setAddress(int);
                  
    qint64          getData();
    int             setData(qint64 data);
                  
    void            getCode(CArgument&);
                  
    int             getValue(void);
    int             setValue(int);
                  
    int             setAll(int value);
                  
    int             getCurrBit(void);
    int             setCurrBit(int bit);
                  
    int             getByteCount(void);
    int             setByteCount(int byt);
                  
    int             getSCL(void);
    int             setSCL(int src = chan1);
                  
    int             getSDA(void);
    int             setSDA(int src = chan2);
                  
    int             getBitMax();
    int             getBitByteMax();

    DsoErr          setCfg(pointer p);
    pointer         getCfg();

    //!scpi
    int            setLevelClk(qint64 l = 0);
    qint64         getLevelClk(void);
    int            setLevelData(qint64 l = 0);
    qint64         getLevelData(void);

private:
    int       m_currBit;
    int       m_bytCount;

    /********************scpi**************/
    long long   m_Data;

};


class servSpiCfg
{

public:
    servSpiCfg(){}
    int       cfg_spi_serialOut(CStream &stream, unsigned char &ver);
    int       cfg_spi_serialIn( CStream &stream, unsigned char ver );
    void      cfg_spi_init();
    virtual DsoErr  apply() = 0;

public:
    bool            cfg_get_spi_when(void);
    int             cfg_set_spi_when(bool b);

    bool            cfg_get_spi_cs_mode(void);
    int             cfg_set_spi_cs_mode(bool b);

    int             cfg_get_spi_scl(void);
    int             cfg_set_spi_scl(int src = chan1);

    bool            cfg_get_spi_edge(void);
    int             cfg_set_spi_edge(bool b);

    int             cfg_get_spi_sda(void);
    int             cfg_set_spi_sda(int src = chan2);

    int             cfg_get_spi_cs(void);
    int             cfg_set_spi_cs(int src = chan3);

    int             cfg_get_spi_bitCount(void);
    int             cfg_set_spi_bitCount(int count);

    qint64          cfg_get_spi_timeout(void);
    int             cfg_set_spi_timeout(qint64 t);

    QBitArray       &cfg_get_spi_data_Min();
    QBitArray       &cfg_get_spi_data_Max();
    QBitArray       &cfg_get_spi_data_Mask();

    qint64          cfg_get_spi_data(void);
    int             cfg_set_spi_data(qint64 data);

    int             cfg_spi_setCfg(void *p);
    void            *cfg_spi_getCfg();
protected:
    TrigSpiCfg      spiCfg;

};
class servSPITrigger        : public TriggerBase, servSpiCfg
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servSPITrigger(QString name = serv_name_trigger_spi);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_SPI_CFG, &spiCfg);}
    virtual int     getChSlope(Chan ch);
    virtual DsoErr  applyLevel();
public:
    bool            getWhen(void);
    int             setWhen(bool);
                
    bool            getCSMode(void);
    int             setCSMode(bool b);
                
    int             getSCL(void);
    int             setSCL(int src = chan1);
                
    bool            getEdge(void);
    int             setEdge(bool b);
                
    int             getSDA(void);
    int             setSDA(int src = chan2);
                
    int             getCS(void);
    int             setCS(int src = chan3);
                
    qint64          getData(void);
    int             setData(qint64 data);
                
    int             getBitCount(void);
    int             setBitCount(int count);
                
    void            getCode(CArgument&);
                
    int             getValue(void);
    int             setValue(int);
                
    int             setAll(int value);
                
    int             getCurrBit(void);
    int             setCurrBit(int bit);
                
    qint64          getTimeout(void);
    int             setTimeout(qint64);
                
    int             getBitMax();
    int             getBitByteMax();

    DsoErr          setCfg(pointer p);
    pointer         getCfg();
    //!scpi
    int             setLevelClk(qint64  l = 0);
    qint64          getLevelClk(void);
    int             setLevelData(qint64 l = 0);
    qint64          getLevelData(void);
    int             setLevelCs(qint64 l = 0);
    qint64          getLevelCs(void);

private:
    int       m_bitCount;
    int       m_currBit;
};

enum serv_can_when
{
    can_when_sof          ,
    can_when_eof          ,
    can_when_remote_id    ,
    can_when_overload     ,
    can_when_frame_id     ,
    can_when_frame_data   ,
    can_when_frame_data_id,
    can_when_frame_error  ,

    can_when_bit_error ,
    can_when_answer_error ,
    can_when_check_error  ,
    can_when_format_error ,
    can_when_random_error ,
    can_when_none ,
};
class servCanTrigger        : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servCanTrigger(QString name = serv_name_trigger_can);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_CAN_CFG, &cfg);}

public:
    DsoErr          setSource(int ch);
                   
    DsoErr          setWhen(int w);
    int             getWhen(void);
                   
    DsoErr          setExtendedId(bool b);
    bool            getExtendedId();
                   
    DsoErr          setId(int id);
    int             getId(void);
                   
    DsoErr          setCurrBit(int bit);
    int             getCurrBit(void);
                   
    DsoErr          setByteCount(int byt);
    int             getByteCount(void);
                   
                   
    DsoErr          setDefine(bool d);
    bool            getDefine();
                   
    DsoErr          setFilterId(bool b);
    bool            getFilterId(void);
                   
    DsoErr          setSignal(int d);
    int             getSignal(void);
                   
    DsoErr          setBaudRate(int baud);
    int             getBaudRate(void);
                   
    DsoErr          setSamplePoint(int point);
    int             getSamplePoint(void);
                   
    DsoErr          setStandard(bool b);
    int             getStandard(void);
                   
                   
    bool            getSelectIdData();
    int             getBitMax();
    int             getBitByteMax();
    DsoErr          setCodeValue(int value);
    int             getCodeValue();
    DsoErr          setCode(CArgument &arg);
    void            getCode(CArgument &arg);
    DsoErr          setCodeAll(int value);

private:
    int       m_when;
    int       m_currBit;
    int       m_dataByteCount;
    int       m_define;
    bool      m_standard;

    static const int   m_idNormCount = 11;
    static const int   m_idExtendCount = 29;
    QBitArray   m_bitIdMin;
    QBitArray   m_bitIdMax;
    QBitArray   m_bitIdMask;

    TrigCanCfg cfg;
};

class servLinTrigger        : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servLinTrigger(QString name = serv_name_trigger_lin);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_LIN_CFG, &cfg);}

public:
    DsoErr          setSource(int ch);
                    
    DsoErr          setWhen(int w);
    int             getWhen(void);
                    
    DsoErr          setErrType(int type);
    int             getErrType(void);
                    
    DsoErr          setId(int id);
    int             getId(void);

    DsoErr          setCurrBit(int bit);
    int             getCurrBit(void);
                    
    DsoErr          setByteCount(int byt);
    int             getByteCount(void);
                    
    DsoErr          setVersion(int ver);
    int             getVersion(void);
                    
    DsoErr          setBaudRate(int baud);
    int             getBaudRate(void);
                    
    DsoErr          setSamplePoint(int point);
    int             getSamplePoint(void);
                    
    int             getBitMax();
    int             getBitByteMax();
    DsoErr          setCodeValue(int value);
    int             getCodeValue();
    DsoErr          setCode(CArgument &arg);
    void            getCode(CArgument &arg);
    DsoErr          setCodeAll(int value);

private:
    int       m_currBit;

    TrigLinCfg cfg;
};

class servI2STrigger        : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servI2STrigger(QString name = serv_name_trigger_i2s);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );

    DsoErr          setItemFocus( int id);

    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_I2S_CFG, &cfg);}
    virtual int     getChSlope(Chan ch);
    virtual DsoErr  applyLevel();
public:
    DsoErr          setSclk(int ch);
    int             getSclk(void);
                    
    DsoErr          setWs(int ch);
    int             getWs(void);
                    
    DsoErr          setSda(int ch);
    int             getSda(void);
                    
    DsoErr          setSclkSlope(int slope);
    int             getSclkSlope(void);
                    
    DsoErr          setWsLow(int wl);
    int             getWsLow(void);
                    
    DsoErr          setWhen(int w);
    int             getWhen(void);
                    
    DsoErr          setCurrBit(int bit);
    int             getCurrBit(void);

    DsoErr          setData(int bit);
    int             getData();
    DsoErr          setIISData(qint64 data);//qxl
    int             getIISData();

    DsoErr          setDataMin(int bit);
    int             getDataMin();
    DsoErr          setDataMax(int bit);
    int             getDataMax();

    DsoErr          setWidth(int width);
    int             getWidth(void);
    DsoErr          setUserWidth(int width);
    int             getUserWidth(void);
    DsoErr          updateByteConut();
                    
    DsoErr          setAlignment(int n);
    int             getAlignment(void);
                    
                    
    int             getBitMax();
    int             getBitByteMax();
    DsoErr          setCodeValue(int value);
    int             getCodeValue();
    DsoErr          setCode(CArgument &arg);
    void            getCode(CArgument &arg);
    DsoErr          setCodeAll(int value);

    DsoErr          checkWidthRange(TrigTimeRule rule);

private:
    int         m_dataCurrBit;
    QBitArray   m_bitDataMin;
    QBitArray   m_bitDataMax;
    QBitArray   m_bitDataMask;
    QBitArray  *m_pBitCurrData;

private:
    TrigI2SCfg cfg;

};

class servFlexRayTrigger        : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    servFlexRayTrigger(QString name = serv_name_trigger_flexray);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_FLEXRAY_CFG, &cfg);}

public:
    DsoErr          setSource(int ch);
                    
    DsoErr          setWhen(int w);
    int             getWhen(void);
                    
    DsoErr          setFrameType(int type);
    int             getFrameType();
                    
    DsoErr          setSymbolType(int type);
    int             getSymbolType();
                    
    DsoErr          setErrType(int type);
    int             getErrType();
                    
    DsoErr          setPosType(int type);
    int             getPosType();
                    
    DsoErr          setDefine(bool define);
    bool            getDefine(void);
                    
    DsoErr          setIdComp(int value);
    int             getIdComp(void);
                    
    DsoErr          setIdMin(int value);
    int             getIdMin(void);
                    
    DsoErr          setIdMax(int value);
    int             getIdMax(void);
                    
    DsoErr          setCycComp(int value);
    int             getCycComp(void);
                    
    DsoErr          setCycMin(int value);
    int             getCycMin(void);
                    
    DsoErr          setCycMax(int value);
    int             getCycMax(void);
                    
    DsoErr          setBaudRate(int baud);
    int             getBaudRate(void);
                    
    DsoErr          setChType(bool b);
    int             getChType(void);

    DsoErr  checkIdRange(TrigTimeRule rule, Trigger_value_cmp c0 = cmp_out);
    DsoErr  checkCyCRange(TrigTimeRule rule, Trigger_value_cmp c0 = cmp_out);

private:
    bool             mDefine;
    static const int mTssTransmitter = 15;
    static const int mCasLowMax      = 99;

    TrigFlexrayCfg cfg;
};

class serv1553BTrigger        : public TriggerBase
{
    Q_OBJECT
    DECLARE_CMD()
    DECLARE_POST_DO()
public:
    serv1553BTrigger(QString name = serv_name_trigger_1553b);
    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn( CStream &stream, unsigned char ver );
    void            rst();
    int             startup();
    virtual  void   registerSpy();

    DsoErr          setActive( int active=1 );
    DsoErr          setItemFocus( int id);
    virtual DsoErr  apply() {return postEngine( ENGINE_TRIGGER_1553_CFG, &cfg);}

public:
    DsoErr          setSource(int ch);
                    
    DsoErr          setWhen(int w);
    int             getWhen(void);
                    
    DsoErr          setSyncType(int type);
    int             getSyncType();
                    
    DsoErr          setDataComp(int comp);
    int             getDataComp();
                    
    DsoErr          setDataMin(int bit);
    int             getDataMin();
                    
    DsoErr          setDataMax(int bit);
    int             getDataMax();
                    
    qint64          getData();//qxl 2018.2.1
    int             setData(qint64 data);

    DsoErr          setRTA(int bit);
    int             getRTA();

    DsoErr          setRTA11(int bit);
    int             getRTA11();

    DsoErr          setTime(int time);
    int             getTime();
                    
    DsoErr          setErrType(int type);
    int             getErrType();

    bool            getPolarity();
    DsoErr          setPolarity(bool polarity);

    DsoErr          setCurrBit(int bit);
    int             getCurrBit(void);

    int             getBitMax();
    int             getBitByteMax();
    DsoErr          setCodeValue(int value);
    int             getCodeValue();
    DsoErr          setCode(CArgument &arg);
    void            getCode(CArgument &arg);
    DsoErr          setCodeAll(int value);

    int              setLevelSrcA(qint64 l = 0);
    qint64           getLevelSrcA(void);
    int              setLevelSrcB(qint64 l = 0);
    qint64           getLevelSrcB(void);

private:
    int         m_dataCurrBit;
    QBitArray   m_bitDataMin;
    QBitArray   m_bitDataMax;
    QBitArray   m_bitDataMask;

    QBitArray   m_bitRTA;
    QBitArray   m_bitRTA11;
    QBitArray   m_bitRTAMask;
    QBitArray   m_bitRTA11Mask;

    QBitArray  *m_pBitCurrData;
    QBitArray  *m_pBitCurrMask;

private:
    TrigStd1553bCfg cfg;
};


class servTrig : public serviceExecutor
{
    Q_OBJECT
    DECLARE_CMD()

public:
    servTrig( QString name = serv_name_trigger);
    static int      createTriggers(QList<service*> *pService);
    virtual void    registerSpy();
    virtual int     startup();
    void            onInit();
    void            rst();
private:
    int             onActive(int id);
    int             getMode(void);
    DsoErr          setMode(TriggerMode mode);

    int             getSource(void);
    void*           getSourcePtr(void);

    qint64          getLevel(void);
    int             setLevel(qint64);
    int             setLevelZ(int z);
    int             getLevelHalf(Chan ch);
    DsoErr          asyncLevel();

    DsoErr          setTrigModeKey(  int key );
    int             getTrigModeKey();

    DsoErr          setTrigSweep(  int sweep );
    int             getTrigSweep();
    DsoErr          setTrigSingle();

    int             setForce();

    void            spySystemStoped();
    DsoErr          spyChInvert(int chId);

    //!scpi
    qint64          getScpiLevel(void);
    int             setScpiLevel(qint64 l);

    DsoErr          setTrigHoldoff(qint64);
    qint64          getTrigHoldoff();

    int             setMsgEnable();

    TriggerBase*    getCurTrigServPtr(void);
private:
    int             mModeKey;
    bool            mSingleFilter;  //!防止连续多次重复按下signle。

public:
    static  QPoint   G_sLevelPoint;
};


class servZoneTrig : public serviceExecutor, public ISerial
{
    Q_OBJECT
    DECLARE_CMD()

public:
    enum cmd
    {
        cmd_zone_get_cfg_ptr = 1,
        cmd_zone_apply,
    };

public:
    servZoneTrig( QString name = serv_name_trigger_zone);
    virtual void    registerSpy();

    int   serialOut( CStream &stream, serialVersion &ver );
    int   serialIn( CStream &stream, serialVersion ver );
    void  rst();
    int   startup();
    void  init();

public:
    virtual DsoErr  apply();
    TrigZoneCfg     *getCfgPtr();

public:
    DsoErr applyZoneEnable();

    DsoErr setEnableA(bool en);
    bool   getEnableA();

    DsoErr setSourceA(Chan ch);
    int    getSourceA();

    DsoErr setIntersectA(int value);
    int    getIntersectA();

    DsoErr setEnableB(bool en);
    bool   getEnableB();

    DsoErr setSourceB(Chan ch);
    int    getSourceB();

    DsoErr setIntersectB(int value);
    int    getIntersectB();

private:
    TrigZoneCfg     cfg;

    bool            m_touchEnable;

    bool            m_aEnable;
    bool            m_bEnable;

    int             m_aIntersect;
    int             m_bIntersect;
};

#endif // SERVTRIG_H
