#include "servtrig.h"
#include "../servhori/servhori.h"

/**************************Trigger API For other modules****************************/
IMPLEMENT_CMD( serviceExecutor, servZoneTrig  )
start_of_entry()

on_get_pointer(     servZoneTrig::cmd_zone_get_cfg_ptr,       &servZoneTrig::getCfgPtr ),
on_set_int_void(    servZoneTrig::cmd_zone_apply,             &servZoneTrig::apply ),
                   
on_set_int_bool(    MSG_TRIG_ZONE_A_ENABLE,                   &servZoneTrig::setEnableA ),
on_get_bool(        MSG_TRIG_ZONE_A_ENABLE,                   &servZoneTrig::getEnableA ),
                                                             
on_set_int_int(     MSG_TRIG_ZONE_A_SOURCE,                   &servZoneTrig::setSourceA ),
on_get_int(         MSG_TRIG_ZONE_A_SOURCE,                   &servZoneTrig::getSourceA ),

on_set_int_int(     MSG_TRIG_ZONE_A_INTERSECT,                &servZoneTrig::setIntersectA ),
on_get_int(         MSG_TRIG_ZONE_A_INTERSECT,                &servZoneTrig::getIntersectA ),
                                                                                                                        
on_set_int_bool(    MSG_TRIG_ZONE_B_ENABLE,                   &servZoneTrig::setEnableB ),
on_get_bool(        MSG_TRIG_ZONE_B_ENABLE,                   &servZoneTrig::getEnableB ),

on_set_int_int(     MSG_TRIG_ZONE_B_SOURCE,                   &servZoneTrig::setSourceB ),
on_get_int(         MSG_TRIG_ZONE_B_SOURCE,                   &servZoneTrig::getSourceB ),

on_set_int_int(     MSG_TRIG_ZONE_B_INTERSECT,                &servZoneTrig::setIntersectB ),
on_get_int(         MSG_TRIG_ZONE_B_INTERSECT,                &servZoneTrig::getIntersectB ),

on_set_void_void(   CMD_SERVICE_RST,                          &servZoneTrig::rst ),
on_set_int_void  (  CMD_SERVICE_STARTUP,                      &servZoneTrig::startup ),
on_set_int_int  (   CMD_SERVICE_ACTIVE,                       &servZoneTrig::setActive ),

end_of_entry()

servZoneTrig::servZoneTrig( QString name )
    : serviceExecutor(name, E_SERVICE_ID_TRIG_ZONE)
{
    mVersion = 0x02;

    serviceExecutor::baseInit();
    for(int i = 0; i < 1; i++)
    {
        cfg.a_zone.append({false,chan1,true,0,0,0,0});
        cfg.a_zone.append({false,chan1,true,0,0,0,0});
    }
}

void servZoneTrig::registerSpy()
{
    spyOn( serv_name_ch1, MSG_CHAN_SCALE,       CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn( serv_name_ch2, MSG_CHAN_SCALE,       CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn( serv_name_ch3, MSG_CHAN_SCALE,       CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn( serv_name_ch4, MSG_CHAN_SCALE,       CMD_TRIG_ZONE_UPDATE_SHOW);

    spyOn( serv_name_ch1, MSG_CHAN_OFFSET,      CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn( serv_name_ch2, MSG_CHAN_OFFSET,      CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn( serv_name_ch3, MSG_CHAN_OFFSET,      CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn( serv_name_ch4, MSG_CHAN_OFFSET,      CMD_TRIG_ZONE_UPDATE_SHOW);

    spyOn(serv_name_hori, MSG_HORI_MAIN_SCALE , CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn(serv_name_hori, MSG_HORI_MAIN_OFFSET, CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn(serv_name_hori, MSG_HORI_ZOOM_SCALE , CMD_TRIG_ZONE_UPDATE_SHOW);
    spyOn(serv_name_hori, MSG_HORI_ZOOM_OFFSET, CMD_TRIG_ZONE_UPDATE_SHOW);
}

#define ar ar_out
#define ar_pack_e ar_out
#define ar_pack_s(name, val) stream.write(name, val)
int servZoneTrig::serialOut(CStream &stream, serialVersion &ver)
{
    ISerial::serialOut(stream, ver );

    ar("m_touchEnable",  m_touchEnable);
    ar("m_aEnable",      m_aEnable);
    ar("m_bEnable",      m_bEnable);
    ar("m_aIntersect",   m_aIntersect);
    ar("m_bIntersect",   m_bIntersect);

    for(int i = 0; i< cfg.a_zone.count(); i++)
    {
        ar_pack_s(QString("en%1").arg(i),         cfg.a_zone[i].en         );
        ar_pack_s(QString("cross%1").arg(i),      cfg.a_zone[i].cross      );
        ar_pack_s(QString("tMin%1").arg(i),       cfg.a_zone[i].tMin       );
        ar_pack_s(QString("tMax%1").arg(i),       cfg.a_zone[i].tMax       );
        ar_pack_s(QString("thresholdH%1").arg(i), cfg.a_zone[i].thresholdH );
        ar_pack_s(QString("thresholdL%1").arg(i), cfg.a_zone[i].thresholdL );
        ar_pack_s(QString("ch%1").arg(i),         (int)cfg.a_zone[i].ch    );
    }

    return 0;
}
#undef ar
#undef ar_pack_e
#undef ar_pack_s

#define ar ar_in
#define ar_pack_e ar_e_in
#define ar_pack_s(name, val) stream.read(name, val)
int servZoneTrig::serialIn(CStream &stream, serialVersion ver)
{
    ISerial::serialIn(stream, ver );
    if(ver == mVersion)
    {
        ar("m_touchEnable",  m_touchEnable);
        ar("m_aEnable",      m_aEnable);
        ar("m_bEnable",      m_bEnable);
        ar("m_aIntersect",   m_aIntersect);
        ar("m_bIntersect",   m_bIntersect);

        for(int i = 0; i< cfg.a_zone.count(); i++)
        {
            ar_pack_s(QString("en%1").arg(i),         cfg.a_zone[i].en         );
            ar_pack_s(QString("cross%1").arg(i),      cfg.a_zone[i].cross      );
            ar_pack_s(QString("tMin%1").arg(i),       cfg.a_zone[i].tMin       );
            ar_pack_s(QString("tMax%1").arg(i),       cfg.a_zone[i].tMax       );
            ar_pack_s(QString("thresholdH%1").arg(i), cfg.a_zone[i].thresholdH );
            ar_pack_s(QString("thresholdL%1").arg(i), cfg.a_zone[i].thresholdL );

            int value = 0;
            ar_pack_s(QString("ch%1").arg(i),         value    );
            cfg.a_zone[i].ch = (Chan)value;
        }
    }
    else
    {
        rst();
    }
    return 0;
}
#undef ar
#undef ar_pack_e
#undef ar_pack_s

int servZoneTrig::startup()
{
    async(MSG_TRIG_ZONE_A_ENABLE, m_aEnable);
    async(MSG_TRIG_ZONE_B_ENABLE, m_bEnable);
    updateAllUi();
    return ERR_NONE;
}

void servZoneTrig::init()
{
    m_aEnable     = false;
    m_bEnable     = false;

    m_aIntersect  = 1;
    m_bIntersect  = 1;
}

void servZoneTrig::rst()
{
    m_aEnable     = false;
    m_bEnable     = false;

    m_aIntersect  = 1;
    m_bIntersect  = 1;
}

DsoErr servZoneTrig::apply()
{

    if(m_aEnable)
    {
        TrigZoneCfg::TZoneDataStru &zone = cfg.a_zone[0];
        if(zone.thresholdL == zone.thresholdH || zone.tMin == zone.tMax)
        {

        }

    }







    if(  m_aEnable || m_bEnable)
    {
       return postEngine( ENGINE_TRIGGER_ZONE_CFG, &cfg);
    }
    else
    {
        return ERR_NONE;
    }
}

TrigZoneCfg *servZoneTrig::getCfgPtr()
{
    return &cfg;
}

DsoErr servZoneTrig::applyZoneEnable()
{
    bool b = false;
    b = m_aEnable || m_bEnable;

    //! [dba+, 2018-04-02, bug:1862,2376 ]
    post(serv_name_hori,  servHori::cmd_apply_engine, 1);

    return postEngine( ENGINE_TRIGGER_ZONE_EN, b);
}

DsoErr servZoneTrig::setEnableA(bool en)
{
    DsoErr err = ERR_NONE;

    m_aEnable = en;

    Q_ASSERT(cfg.a_zone.count() > 0);
    cfg.a_zone[0].en = m_aEnable;

    err = apply();
    if(ERR_NONE == err)
    {
        err = applyZoneEnable();
    }

    return err;
}

bool servZoneTrig::getEnableA()
{
    return m_aEnable;
}

DsoErr servZoneTrig::setSourceA(Chan ch)
{
    Q_ASSERT(cfg.a_zone.count() > 0);
    cfg.a_zone[0].ch = ch;
    return apply();
}

int servZoneTrig::getSourceA()
{
    Q_ASSERT(cfg.a_zone.count() > 0);
    return cfg.a_zone[0].ch;
}

DsoErr servZoneTrig::setIntersectA(int value)
{
    m_aIntersect = value;

    Q_ASSERT(cfg.a_zone.count() > 0);
    cfg.a_zone[0].cross = (bool)m_aIntersect;

    return apply();
}

int servZoneTrig::getIntersectA()
{
    return m_aIntersect;
}

DsoErr servZoneTrig::setEnableB(bool en)
{
    DsoErr err = ERR_NONE;

    m_bEnable = en;

    Q_ASSERT(cfg.a_zone.count() > 1);
    cfg.a_zone[1].en = m_bEnable;

    err = apply();
    if(ERR_NONE == err)
    {
        err = applyZoneEnable();
    }
    return err;
}

bool servZoneTrig::getEnableB()
{
    return m_bEnable;
}

DsoErr servZoneTrig::setSourceB(Chan ch)
{
    Q_ASSERT(cfg.a_zone.count() > 1);
    cfg.a_zone[1].ch = ch;
    return apply();
}

int servZoneTrig::getSourceB()
{
    Q_ASSERT(cfg.a_zone.count() > 1);
    return cfg.a_zone[1].ch;
}

DsoErr servZoneTrig::setIntersectB(int value)
{
    m_bIntersect = value;

    Q_ASSERT(cfg.a_zone.count() > 1);
    cfg.a_zone[1].cross = (bool)m_bIntersect;

    return apply();
}

int servZoneTrig::getIntersectB()
{
    return m_bIntersect;
}
