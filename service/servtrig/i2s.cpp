#include "servtrig.h"
#include "trigcode.h"

IMPLEMENT_POST_DO( TriggerBase, servI2STrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servI2STrigger::applyLevel),
on_postdo( MSG_TRIGGER_IIS_SCLK,          &servI2STrigger::applyLevel),
on_postdo( MSG_TRIGGER_IIS_SLOPE,         &servI2STrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servI2STrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_IIS_SCLK,           &servI2STrigger::setSclk),
on_get_int      (       MSG_TRIGGER_IIS_SCLK,           &servI2STrigger::getSclk),

on_set_int_int  (       MSG_TRIGGER_IIS_SLOPE,          &servI2STrigger::setSclkSlope),
on_get_int      (       MSG_TRIGGER_IIS_SLOPE,          &servI2STrigger::getSclkSlope),

on_set_int_int  (       MSG_TRIGGER_IIS_WS,             &servI2STrigger::setWs),
on_get_int      (       MSG_TRIGGER_IIS_WS,             &servI2STrigger::getWs),

on_set_int_int  (       MSG_TRIGGER_IIS_WS_LOW,         &servI2STrigger::setWsLow),
on_get_int      (       MSG_TRIGGER_IIS_WS_LOW,         &servI2STrigger::getWsLow),

on_set_int_int  (       MSG_TRIGGER_IIS_SDA,            &servI2STrigger::setSda),
on_get_int      (       MSG_TRIGGER_IIS_SDA,            &servI2STrigger::getSda),

on_set_int_int  (       MSG_TRIGGER_IIS_WHEN,           &servI2STrigger::setWhen),
on_get_int      (       MSG_TRIGGER_IIS_WHEN,           &servI2STrigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_IIS_CURR_BIT,       &servI2STrigger::setCurrBit),
on_get_int      (       MSG_TRIGGER_IIS_CURR_BIT,       &servI2STrigger::getCurrBit),

on_set_int_int  (       MSG_TRIGGER_IIS_DATA,           &servI2STrigger::setData),
on_get_int      (       MSG_TRIGGER_IIS_DATA,           &servI2STrigger::getData),

on_set_int_ll  (        CMD_SCPI_TRIGGER_DATA_IIS,      &servI2STrigger::setIISData),//qxl
on_get_ll      (        CMD_SCPI_TRIGGER_DATA_IIS,      &servI2STrigger::getIISData),

on_set_int_int  (       MSG_TRIGGER_IIS_DATA_MIN,       &servI2STrigger::setDataMin),
on_get_int      (       MSG_TRIGGER_IIS_DATA_MIN,       &servI2STrigger::getDataMin),
on_set_int_int  (       MSG_TRIGGER_IIS_DATA_MAX,       &servI2STrigger::setDataMax),
on_get_int      (       MSG_TRIGGER_IIS_DATA_MAX,       &servI2STrigger::getDataMax),

on_set_int_int  (       MSG_TRIGGER_IIS_WIDTH,          &servI2STrigger::setWidth),
on_get_int      (       MSG_TRIGGER_IIS_WIDTH,          &servI2STrigger::getWidth),

on_set_int_int  (       MSG_TRIGGER_IIS_USER_WIDTH,     &servI2STrigger::setUserWidth),
on_get_int      (       MSG_TRIGGER_IIS_USER_WIDTH,     &servI2STrigger::getUserWidth),

on_set_int_int  (       MSG_TRIGGER_IIS_ALIGNMENT,      &servI2STrigger::setAlignment),
on_get_int      (       MSG_TRIGGER_IIS_ALIGNMENT,      &servI2STrigger::getAlignment),

on_set_int_int  (       MSG_TRIGGER_CODE,               &servI2STrigger::setCodeValue),
on_get_int      (       MSG_TRIGGER_CODE,               &servI2STrigger::getCodeValue),
on_set_int_arg  (       MSG_TRIGGER_SET_CODE,           &servI2STrigger::setCode),
on_get_void_argo(       MSG_TRIGGER_SET_CODE,           &servI2STrigger::getCode),
on_set_int_int  (       MSG_TRIGGER_CODE_ALL,           &servI2STrigger::setCodeAll),

on_set_void_void(       CMD_SERVICE_RST,                &servI2STrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &servI2STrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servI2STrigger::setActive ),
on_set_int_int  (       CMD_SERVICE_ITEM_FOCUS,         &servI2STrigger::setItemFocus ),

end_of_entry()


#define IIS_BIT_COUNT_MAX 32
#define IIS_BIT_COUNT_MIN 4

#define  IIS_SET_CURR_SOURCES(src) do{ \
    if( !((src >= chan1 && src <= d15)||(src == ext)) ) {return ERR_INVALID_INPUT;}\
    else{TriggerBase::setSource(src);}\
    }while(0)

#define ar ar_out
#define ar_pack_e ar_out
int servI2STrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    ar("m_dataCurrBit",          m_dataCurrBit         );

    ar_pack_e("mSclk",    cfg.mSclk        );
    ar_pack_e("mWs",      cfg.mWs          );
    ar_pack_e("mSda",     cfg.mSda         );
    ar_pack_e("mLine",    cfg.mLine        );
    ar_pack_e("mSpec",    cfg.mSpec        );
    ar_pack_e("mDataCmp", cfg.mDataCmp     );
    ar("mUsedWidth",      cfg.mUsedWidth   );
    ar("mWidth",          cfg.mWidth       );
    ar("mMinData",        cfg.mMinData     );
    ar("mMaxData",        cfg.mMaxData     );
    ar("mMask",           cfg.mMask        );

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servI2STrigger::serialIn(CStream &stream, unsigned char ver)
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        ar_pack_e("mSclk",    cfg.mSclk        );
        ar_pack_e("mWs",      cfg.mWs          );
        ar_pack_e("mSda",     cfg.mSda         );
        ar_pack_e("mLine",    cfg.mLine        );
        ar_pack_e("mSpec",    cfg.mSpec        );
        ar_pack_e("mDataCmp", cfg.mDataCmp     );
        ar("mUsedWidth",      cfg.mUsedWidth   );
        ar("mWidth",          cfg.mWidth       );
        ar("mMinData",        cfg.mMinData     );
        ar("mMaxData",        cfg.mMaxData     );
        ar("mMask",           cfg.mMask        );

        m_bitDataMin    = QBitArray(IIS_BIT_COUNT_MAX);
        m_bitDataMax    = QBitArray(IIS_BIT_COUNT_MAX);
        m_bitDataMask   = QBitArray(IIS_BIT_COUNT_MAX);
        m_pBitCurrData  = &m_bitDataMin;
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

servI2STrigger::servI2STrigger(QString name)
    : TriggerBase(name, Trigger_I2S, E_SERVICE_ID_TRIG_IIS)
{
    serviceExecutor::baseInit();
    m_pBitCurrData = NULL;

    linkChange(MSG_TRIGGER_IIS_WIDTH, MSG_TRIGGER_IIS_USER_WIDTH);
    linkChange(MSG_TRIGGER_IIS_USER_WIDTH,MSG_TRIGGER_IIS_WIDTH );

    /*! [dba, 2018-04-14], bug: 苑婷让去掉ext*/
    mUiAttr.setVisible(MSG_TRIGGER_IIS_SCLK,  ext, false);
    mUiAttr.setVisible(MSG_TRIGGER_IIS_WS,    ext, false);
    mUiAttr.setVisible(MSG_TRIGGER_IIS_SDA,   ext, false);
}

void servI2STrigger::rst()
{
    cfg.setSclk(    (Chan)chan1 );
    cfg.setWs(      (Chan)chan1 );
    cfg.setSda(     (Chan)chan1 );
    cfg.setSlope(   (EdgeSlope)Trigger_Edge_Rising         );
    cfg.setSpec(    (Trigger_IIS_Spec)trig_iis_standard    );
    cfg.setLine(    (Trigger_IIS_Ch)trig_iis_lch           );
    cfg.setDataCmp( (Trigger_IIS_data_cmp)trig_iis_data_eq );
    cfg.setUsedWidth(4 );
    cfg.setWidth(    4 );
    cfg.setMinData(  0 );
    cfg.setMaxData(  0 );
    cfg.setMask(     0xffff );

    m_dataCurrBit = 0;
    m_bitDataMin  = QBitArray(IIS_BIT_COUNT_MAX);
    m_bitDataMax  = QBitArray(IIS_BIT_COUNT_MAX);
    m_bitDataMask = QBitArray(IIS_BIT_COUNT_MAX);
    m_pBitCurrData  = &m_bitDataMin;
    TriggerBase::rst();
}

int servI2STrigger::startup()
{
    return  TriggerBase::startup();
}

void servI2STrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

DsoErr servI2STrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

DsoErr servI2STrigger::setItemFocus(int id)
{
    TriggerBase::setItemFocus(id);

    if(MSG_TRIGGER_IIS_DATA == id)
    {
        async(MSG_TRIGGER_IIS_DATA, getData() );
    }
    else if(MSG_TRIGGER_IIS_DATA_MIN == id)
    {
        async(MSG_TRIGGER_IIS_DATA_MIN, getDataMin() );
    }
    else if(MSG_TRIGGER_IIS_DATA_MAX == id)
    {
        async(MSG_TRIGGER_IIS_DATA_MAX, getDataMax() );
    }
    else
    {}
    return ERR_NONE;
}

int servI2STrigger::getChSlope(Chan ch)
{
    if(ch == getSclk() )
    {
        return getSclkSlope();
    }
    else
    {
        return TriggerBase::getChSlope(ch);
    }
}

DsoErr servI2STrigger::applyLevel()
{
    DsoErr err = ERR_NONE;
    err = applyLevelEngine((Chan)getSclk());
    if(err != ERR_NONE) return err;

    err = applyLevelEngine((Chan)getSda());
    if(err != ERR_NONE) return err;

    err = applyLevelEngine((Chan)getWs());
    return err;
}

DsoErr servI2STrigger::setSclk(int ch)
{
    IIS_SET_CURR_SOURCES(ch);
    cfg.setSclk((Chan)ch);
    TriggerBase::setSourceEN(cfg.sda());
    TriggerBase::setSourceEN(cfg.sclk());
    TriggerBase::setSourceEN(cfg.ws());
    return apply();
}

int servI2STrigger::getSclk()
{
    return cfg.sclk();
}

DsoErr servI2STrigger::setWs(int ch)
{
    IIS_SET_CURR_SOURCES(ch);
    cfg.setWs((Chan)ch);
    TriggerBase::setSourceEN(cfg.sda());
    TriggerBase::setSourceEN(cfg.sclk());
    TriggerBase::setSourceEN(cfg.ws());
    return apply();
}

int servI2STrigger::getWs()
{
    return cfg.ws();
}

DsoErr servI2STrigger::setSda(int ch)
{
    IIS_SET_CURR_SOURCES(ch);
    cfg.setSda((Chan)ch);
    TriggerBase::setSourceEN(cfg.sda());
    TriggerBase::setSourceEN(cfg.sclk());
    TriggerBase::setSourceEN(cfg.ws());
    return apply();
}

int servI2STrigger::getSda()
{
    return cfg.sda();
}

DsoErr servI2STrigger::setSclkSlope(int slope)
{
    cfg.setSlope((EdgeSlope)slope);
    return apply();
}

int servI2STrigger::getSclkSlope()
{
    return cfg.slope();
}

DsoErr servI2STrigger::setWsLow(int wl)
{

    cfg.setLine((Trigger_IIS_Ch)wl);
    return apply();
}

int servI2STrigger::getWsLow()
{
    return cfg.line();
}


DsoErr servI2STrigger::setWhen(int w)
{
    cfg.setDataCmp((Trigger_IIS_data_cmp)w);

    switch (w) {
    case trig_iis_data_eq:
    case trig_iis_data_neq:
    case trig_iis_data_mt:
        setItemFocus(MSG_TRIGGER_IIS_DATA);
        break;
    case trig_iis_data_lt:
        setItemFocus(MSG_TRIGGER_IIS_DATA_MAX);
        break;
    case trig_iis_data_in:
    case trig_iis_data_out:
        setItemFocus(MSG_TRIGGER_IIS_DATA_MIN);
        break;
    default:
        break;
    }

    return apply();
}

int servI2STrigger::getWhen()
{
    return cfg.dataCmp();
}

DsoErr servI2STrigger::setCurrBit(int bit)
{
    //!按下 切换当前位的值
    if(TRIG_CURRBIT_Z_VALUE == bit)
    {
        int    curValue = getCodeValue();
        bool   binHex = true;

        if( m_dataCurrBit >= getBitMax() )
        {
            binHex = false;
        }
        int stop = (getBitMax()%4)?((1<<getBitMax()%4)-1):(0xf); //! 0x00-0xf
        stop = (m_dataCurrBit == getBitMax())?stop:(0xf);
        return setCodeValue( trigCode::get_z_value(curValue,binHex, stop) );
    }
    else //!旋转 切换当前位置
    {
        int b = bit  + m_dataCurrBit;

        b = qMax(b, 0);
        b = qMin(b, getBitByteMax()-1 );

        m_dataCurrBit = b;
        return ERR_NONE;
    }

    return ERR_NONE;
}

int servI2STrigger::getCurrBit()
{
    setuiZVal( TRIG_CURRBIT_Z_VALUE );
    return m_dataCurrBit;
}


DsoErr servI2STrigger::setData(int bit)
{
    m_pBitCurrData = &m_bitDataMin;
    return setCurrBit(bit);
}

int servI2STrigger::getData()
{
    return getCurrBit();
}

DsoErr servI2STrigger::setIISData(qint64 data)//qxl 2018.2.1
{
    qint64 maxData = 0;
    qint64 tranData = 1;
    int width  = cfg.usedWidth();
    for(int i=0; i<width; i++)
    {
        maxData = maxData|(tranData<<i);
    }

    if(maxData <= data)
    {
        trigCode::setAllValue(  *m_pBitCurrData,
                                m_bitDataMask,
                                m_dataCurrBit,
                                1,
                                getBitMax() );
    }
    else
    {
        for(int i=width-1; i>=0; i--)
        {
            int codeBit = 0;
            codeBit = data&0x01;
            trigCode::setBitValue(*m_pBitCurrData,
                                  m_bitDataMask,
                                  i,
                                  codeBit,
                                  getBitMax() );
            data = data>>1;
        }
    }

    setuiChange( MSG_TRIGGER_CODE);

    return ERR_NONE;
}

int servI2STrigger::getIISData()
{
    qint64 data = 0;
    CArgument arg;
    getCode(arg);
    for(int i=0; i<arg.size(); i++)
    {
        int num=0;
        arg[i].getVal(num);
        if(Trigger_Code_X == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_L == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_H == num)
        {
            data = (data<<1)+1;
        }
    }

    return data;
}

DsoErr servI2STrigger::setDataMin(int bit)
{
    m_pBitCurrData = &m_bitDataMin;
    return async(MSG_TRIGGER_IIS_CURR_BIT, bit);
}

int servI2STrigger::getDataMin()
{
    return getCurrBit();
}

DsoErr servI2STrigger::setDataMax(int bit)
{
    m_pBitCurrData = &m_bitDataMax;
    return async(MSG_TRIGGER_IIS_CURR_BIT, bit);
}

int servI2STrigger::getDataMax()
{
    return getCurrBit();
}

DsoErr servI2STrigger::setWidth(int width)
{
    cfg.setWidth(width);
    return checkWidthRange(T_Rule_H);
}

int servI2STrigger::getWidth()
{
    setuiZVal(  IIS_BIT_COUNT_MIN );
    setuiMaxVal(IIS_BIT_COUNT_MAX );
    setuiMinVal(IIS_BIT_COUNT_MIN );
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return cfg.width();
}
DsoErr servI2STrigger::setUserWidth(int width)
{
    cfg.setUsedWidth(width);
    return checkWidthRange(T_Rule_L);
}
int servI2STrigger::getUserWidth()
{
    setuiZVal(   IIS_BIT_COUNT_MIN );
    setuiMaxVal( IIS_BIT_COUNT_MAX );
    setuiMinVal( IIS_BIT_COUNT_MIN );
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return cfg.usedWidth();
}

DsoErr servI2STrigger::updateByteConut()
{ 
    int bitMax = getBitMax();
    m_bitDataMin.fill(false,  bitMax,  IIS_BIT_COUNT_MAX);
    m_bitDataMax.fill( false, bitMax, IIS_BIT_COUNT_MAX);
    m_bitDataMask.fill(false, bitMax, IIS_BIT_COUNT_MAX);

    m_dataCurrBit = qMin(m_dataCurrBit,getBitByteMax()-1);
    return apply();
}

DsoErr servI2STrigger::setAlignment(int n)
{
    cfg.setSpec((Trigger_IIS_Spec)n);
    return apply();
}

int servI2STrigger::getAlignment()
{
    return cfg.spec();
}

int servI2STrigger::getBitMax()
{
    return  qMin( cfg.width(), cfg.usedWidth() );
}

int servI2STrigger::getBitByteMax()
{
    return getBitMax() + (getBitMax()+3)/4;
}

DsoErr servI2STrigger::setCodeValue(int value)
{
    Q_ASSERT(m_pBitCurrData != NULL);
    trigCode::setBitValue(*m_pBitCurrData,
                          m_bitDataMask,
                          m_dataCurrBit,
                          value,
                          getBitMax() );

    quint32 min = 0, max = 0, mask = 0;

    for(int i = 0; i<m_bitDataMin.count(); i++)
    {
        min  |= (m_bitDataMin.at(i) ? (1<<i) : 0 );
        max  |= (m_bitDataMax.at(i) ? (1<<i) : 0 );
        mask |= (m_bitDataMask.at(i)? (1<<i) : 0 );
    }

    cfg.setMinData( min  );
    cfg.setMaxData( max  );
    cfg.setMask(    mask );

    return apply();
}

int servI2STrigger::getCodeValue()
{
    Q_ASSERT(m_pBitCurrData != NULL);
    int value = trigCode::getBitValue(*m_pBitCurrData,
                                      m_bitDataMask,
                                      m_dataCurrBit,
                                      getBitMax() );
    return value;
}

DsoErr servI2STrigger::setCode(CArgument &)
{
    return ERR_NONE;
}

void servI2STrigger::getCode(CArgument &arg)
{
    Q_ASSERT(m_pBitCurrData != NULL);
    arg.clear();
    for(int i = getBitMax()-1; i >= 0 ; i--)
    {
        int value = Trigger_Code_X;
        if(m_bitDataMask.at(i))
        {
            value = m_pBitCurrData->at(i );
        }
        else
        {
            value = Trigger_Code_X;
        }
        arg.append(value);
    }
}

DsoErr servI2STrigger::setCodeAll(int value )
{
    trigCode::setAllValue(  *m_pBitCurrData,
                            m_bitDataMask,
                            m_dataCurrBit,
                            value,
                            getBitMax() );
    return apply();
}

DsoErr servI2STrigger::checkWidthRange(TrigTimeRule rule)
{
    qint64 H = cfg.width();
    qint64 L = cfg.usedWidth();

    trigrules::cmpRangeRule(rule, L, H, cmp_in, cmp_in, IIS_BIT_COUNT_MIN, IIS_BIT_COUNT_MAX);

    cfg.setWidth(H);
    cfg.setUsedWidth(L);
    return updateByteConut();
}

