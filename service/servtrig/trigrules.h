#ifndef TRIGRULES_H
#define TRIGRULES_H
#include "../../include/dsotype.h"
#include "../../include/dsocfg.h"
#include "../servdso/sysdso.h"

using namespace  DsoType;
typedef enum
{
    T_Rule_None, //！无规则
    T_Rule_H,    //！仅仅检查 tMin <= tH <= tMax
    T_Rule_L,    //！仅仅检查 tMin <= tL <= tMax
    T_Rule_H_L,  //！检查 1、tMin + Step <= tH <= tMax, 2、tL <= tH - Step
    T_Rule_L_H,  //！检查 1、tMin <= tL <= tMax-Step,   2、tH >= tL + Step
}TrigTimeRule;


class trigrules
{
public:
    trigrules();
public:
    static  void timeRangeRule( TrigTimeRule rule, qint64 &tL, qint64 &tH,
                                const EMoreThan    w,
                                const EMoreThan    w0   = Trigger_When_None,
                                const qint64       tMin = time_ps(800),
                                const qint64       tMax = time_s(10));

    static  void cmpRangeRule( TrigTimeRule rule, qint64 &l, qint64 &h,
                                const Trigger_value_cmp    c,
                                const Trigger_value_cmp    c0   = cmp_out,
                                const qint64       Min = 0,
                                const qint64       Max = 65536);
};

#endif // TRIGRULES_H
