#include "trigrules.h"

trigrules::trigrules()
{

}

/*!
 * \brief trigrules::timeRangeRule \n
 * -#  仅一个时间参数生效时， tMin <= (tL或tH) <= tMax.
 * -#  当两个时间参数生效时， tMin <= tL < tH <= tMax, (tH-tL) >= step; step是当前步进。
 * -#  当从一个时间参数有效变成两个时间参数有效时，需要满足条件2的同时需要满足如下条件：
 * - - 从 Trigger_When_Morethan 到 Trigger_When_MoreLess； tH跟随tl变化，
 * - - 从 Trigger_When_Lessthan 到 Trigger_When_MoreLess;  tL跟随tH变化，
 * \param [in]      rule  規則：TrigTimeRule
 * \param [in/out]  tL    时间低值
 * \param [in/out]  tH    时间高值
 * \param [in]      w     当前的条件选项，
 * \param [in]      w0    w0：when的上一次状态
 * \param [in]      tMin  范围最小值，默认 800ps
 * \param [in]      tMax  范围最大值，默认 10s
 */
void trigrules::timeRangeRule(TrigTimeRule rule, qint64 &tL, qint64 &tH,
                              const EMoreThan w, const EMoreThan w0,
                              const qint64 tMin, const qint64 tMax)
{
    //!when变化 tH或tL可能需要重新检查范围
    if( (w0 != w))
    {
        if( w0 == Trigger_When_Morethan)
        {
            rule = T_Rule_L;
        }
        else if(w0 == Trigger_When_Lessthan)
        {
            rule = T_Rule_H;
        }
        else{}
    }

    //!when等于 Trigger_When_MoreLess 时，tH与tL相互之间有关联
    if( (Trigger_When_MoreLess == w)|| (Trigger_When_UnMoreLess == w) )
    {
        switch (rule) {
        case T_Rule_H:
            rule = T_Rule_H_L;
            break;
        case T_Rule_L:
            rule = T_Rule_L_H;
            break;
        default:
            break;
        }
    }

    //！按照rule规定，进行规则检查
    switch (rule) {
    case T_Rule_H://!set --> tH
        tH = qMin(tH, tMax);
        tH = qMax(tH, tMin);
        break;
    case T_Rule_L://!set --> tL
        tL = qMin(tL, tMax);
        tL = qMax(tL, tMin);
        break;
    case T_Rule_L_H: //!tH 随 tL 变化
        tL = qMax(tL, tMin);
        tL = qMin(tL, tMax - getTimeStep(tMax));
        tH = qMax(tH, tL   + getTimeStep(tL)  );
        break;
    case T_Rule_H_L://!tL 随 tH 变化
        tH = qMin(tH, tMax);
        tH = qMax(tH, tMin + getTimeStep(tMin));
        tL = qMin(tL, tH   - getTimeStep(tH)  );
        break;
    default:
        break;
    }
}

void trigrules::cmpRangeRule(TrigTimeRule rule, qint64 &l, qint64 &h,
                             const Trigger_value_cmp c, const Trigger_value_cmp c0,
                             const qint64 Min, const qint64 Max)
{
    //!when变化 tH或tL可能需要重新检查范围
    if( (c == cmp_in) || (c == cmp_out) )
    {
        if( ( c0 == cmp_eq) || ( c0 == cmp_neq) || ( c0 == cmp_gt) )
        {
            rule = T_Rule_L;
        }
        else if(( c0 == cmp_lt))
        {
            rule = T_Rule_H;
        }
        else{}

        switch (rule) {
        case T_Rule_H:
            rule = T_Rule_H_L;
            break;
        case T_Rule_L:
            rule = T_Rule_L_H;
            break;
        default:
            break;
        }
    }

    //！按照rule规定，进行规则检查
    switch (rule) {
    case T_Rule_H://!set --> h
        h = qMin(h, Max);
        h = qMax(h, Min);
        break;
    case T_Rule_L://!set --> l
        l = qMin(l, Max);
        l = qMax(l, Min);
        break;
    case T_Rule_L_H: //!h 随 l 变化
        l = qMax(l, Min);
        l = qMin(l, Max/* - getNumStep(Max)*/);
        h = qMax(h, l/*   + getNumStep(l)*/  );
        break;
    case T_Rule_H_L://!l 随 h 变化
        h = qMin(h, Max);
        h = qMax(h, Min/* + getNumStep(Min)*/);
        l = qMin(l, h /*  - getNumStep(h)*/  );
        break;
    default:
        break;
    }
}

