#include "servtrig.h"
#include "trigcode.h"

IMPLEMENT_POST_DO( TriggerBase, servSPITrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,            &servSPITrigger::applyLevel),
on_postdo( MSG_TRIGGER_SLOPE_POLARITY,    &servSPITrigger::applyLevel),
on_postdo( MSG_TRIGGER_SPI_SCL,           &servSPITrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servSPITrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SPI_SCL,            &servSPITrigger::setSCL),
on_get_int      (       MSG_TRIGGER_SPI_SCL,            &servSPITrigger::getSCL),

on_get_bool     (       MSG_TRIGGER_SLOPE_POLARITY,     &servSPITrigger::getEdge),
on_set_int_bool (       MSG_TRIGGER_SLOPE_POLARITY,     &servSPITrigger::setEdge),

on_set_int_int  (       MSG_TRIGGER_SPI_SDA,            &servSPITrigger::setSDA),
on_get_int      (       MSG_TRIGGER_SPI_SDA,            &servSPITrigger::getSDA),

on_set_int_bool (       MSG_TRIGGER_SPI_WHEN,           &servSPITrigger::setWhen),
on_get_bool     (       MSG_TRIGGER_SPI_WHEN,           &servSPITrigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_SPI_CS,             &servSPITrigger::setCS),
on_get_int      (       MSG_TRIGGER_SPI_CS,             &servSPITrigger::getCS),

on_set_int_bool (       MSG_TRIGGER_SPI_CSMODE,         &servSPITrigger::setCSMode),
on_get_bool     (       MSG_TRIGGER_SPI_CSMODE,         &servSPITrigger::getCSMode),

on_set_int_ll   (       MSG_TRIGGER_SPI_TIMEOUT,        &servSPITrigger::setTimeout),
on_get_ll       (       MSG_TRIGGER_SPI_TIMEOUT,        &servSPITrigger::getTimeout),

on_set_int_ll   (       MSG_TRIGGER_SPI_DATA,           &servSPITrigger::setData),
on_get_ll       (       MSG_TRIGGER_SPI_DATA,           &servSPITrigger::getData),

on_set_int_int  (       MSG_TRIGGER_SPI_DATABITS,       &servSPITrigger::setBitCount),
on_get_int      (       MSG_TRIGGER_SPI_DATABITS,       &servSPITrigger::getBitCount),

on_set_int_int  (       MSG_TRIGGER_CODE,               &servSPITrigger::setValue),
on_get_int      (       MSG_TRIGGER_CODE,               &servSPITrigger::getValue),

on_set_int_int (        MSG_TRIGGER_CODE_ALL,            &servSPITrigger::setAll),

on_set_int_int  (       MSG_TRIGGER_SPI_CURRBIT,        &servSPITrigger::setCurrBit),
on_get_int      (       MSG_TRIGGER_SPI_CURRBIT,        &servSPITrigger::getCurrBit),

on_get_void_argo(       MSG_TRIGGER_SET_CODE,           &servSPITrigger::getCode),

on_set_int_pointer(     CMD_TRIGGER_SPI_CFG,            &servSPITrigger::setCfg),
on_get_pointer    (     CMD_TRIGGER_SPI_CFG,            &servSPITrigger::getCfg),

//!scpi
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_CLK,      &servSPITrigger::setLevelClk ),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_CLK,      &servSPITrigger::getLevelClk ),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_DATA,     &servSPITrigger::setLevelData),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_DATA,     &servSPITrigger::getLevelData),
on_set_int_ll  (       CMD_SCPI_TRIGGER_LEVEL_CS,       &servSPITrigger::setLevelCs),
on_get_ll      (       CMD_SCPI_TRIGGER_LEVEL_CS,       &servSPITrigger::getLevelCs),

on_set_void_void(       CMD_SERVICE_RST,                &servSPITrigger::rst ),
on_set_int_void (       CMD_SERVICE_STARTUP,            &servSPITrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servSPITrigger::setActive ),
end_of_entry()

static const int SPI_MAX_BITS = 32;
static const int SPI_MIN_BITS = 4;
static const int SPI_DEF_BITS = 8;

servSPITrigger::servSPITrigger(QString name)
    : TriggerBase(name, Trigger_SPI, E_SERVICE_ID_TRIG_SPI)
{
    serviceExecutor::baseInit();
}

DsoErr servSPITrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servSPITrigger::getChSlope(Chan ch)
{
    if(ch == getSCL())
    {
        return getEdge();
    }
    else if(ch == getCS())
    {
        return (int)getCSMode();
    }
    else
    {
        return TriggerBase::getChSlope(ch);
    }
}

DsoErr servSPITrigger::applyLevel()
{
    DsoErr err = ERR_NONE;
    err = applyLevelEngine((Chan)getCS());
    if(err != ERR_NONE) return err;

    err = applyLevelEngine((Chan)getSCL());
    if(err != ERR_NONE) return err;

    err = applyLevelEngine((Chan)getSDA());
    return err;
}

#define ar ar_out
#define ar_pack_e ar_out
int servSPITrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    cfg_spi_serialOut(stream, ver);

    ar_pack_e("m_bitCount", m_bitCount    );
    ar_pack_e("m_currBit" , m_currBit     );

    return 0;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servSPITrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);
    if(ver == mVersion)
    {
        cfg_spi_serialIn(stream, ver);

        ar_pack_e("m_bitCount", m_bitCount    );
        ar_pack_e("m_currBit" , m_currBit     );
    }
    else
    {
        rst();
    }
    return 0;
}
#undef ar
#undef ar_pack_e

void servSPITrigger::rst()
{
    TriggerBase::rst();

    cfg_spi_init();

    m_bitCount    = spiCfg.bitCountDef;
    m_currBit     =  0;
}
int servSPITrigger::startup()
{
    CHECK_WORKING_TRIGGER();
    return  TriggerBase::startup();
}

void servSPITrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

/**
 * @brief servSPITrigger::getWhen
 * @return bool
 */
bool servSPITrigger::getWhen(void)
{
    return cfg_get_spi_when();
}

/**
 * @brief servSPITrigger::setWhen
 * @param s
 */
int servSPITrigger::setWhen(bool s )
{
    return cfg_set_spi_when( s );
}

bool servSPITrigger::getCSMode()
{
    return cfg_get_spi_cs_mode();
}

int servSPITrigger::setCSMode(bool b)
{
    return cfg_set_spi_cs_mode(b);
}

int servSPITrigger::getSCL()
{
    return cfg_get_spi_scl();
}

int servSPITrigger::setSCL(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        TriggerBase::setSourceEN( src) ;
        TriggerBase::setSourceEN( cfg_get_spi_sda() );
        TriggerBase::setSourceEN( cfg_get_spi_cs() );

        return cfg_set_spi_scl(src);
    }
}

bool servSPITrigger::getEdge()
{
    return cfg_get_spi_edge();
}

int servSPITrigger::setEdge(bool b)
{
    return cfg_set_spi_edge(b);
}

int servSPITrigger::getSDA()
{
    return cfg_get_spi_sda();
}

int servSPITrigger::setSDA(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        TriggerBase::setSourceEN(src);
        TriggerBase::setSourceEN( cfg_get_spi_scl() );
        TriggerBase::setSourceEN( cfg_get_spi_cs() );
        return cfg_set_spi_sda(src);
    }
}

int servSPITrigger::getCS()
{
    return cfg_get_spi_cs();
}

int servSPITrigger::setCS(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        TriggerBase::setSourceEN(src);
        TriggerBase::setSourceEN( cfg_get_spi_scl() );
        TriggerBase::setSourceEN( cfg_get_spi_sda() );
        return cfg_set_spi_cs(src);
    }
}

/**
 * @brief servSPITrigger::getData
 * @return
 */
qint64 servSPITrigger::getData( void )
{
    qint64 data = 0;
    CArgument arg;
    getCode(arg);
    for(int i=0; i<arg.size(); i++)
    {
        int num=0;
        arg[i].getVal(num);
        if(Trigger_Code_X == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_L == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_H == num)
        {
            data = (data<<1)+1;
        }
    }
    return data;
}

/**
 * @brief servSPITrigger::setData
 * @param a
 * @return
 */
int servSPITrigger::setData(qint64 data)
{
   QBitArray &min  = cfg_get_spi_data_Min();
   QBitArray &mask = cfg_get_spi_data_Mask();

    qint64 maxData = 0;//qxl 2018.2.1
    qint64 tranData = 1;
    for(int i=0; i<m_bitCount; i++)
    {
        maxData = maxData|(tranData<<i);
    }

    if(maxData <= data)
    {
        trigCode::setAllValue(   min,
                                 mask,
                                 m_currBit,
                                 1,
                                 getBitMax());
    }
    else
    {
        for(int i=m_bitCount-1; i>=0; i--)
        {
            int codeBit = 0;
            codeBit = data&0x01;
            trigCode::setBitValue(min,
                                  mask,
                                  i,
                                  codeBit,
                                  getBitMax() );
            data = data>>1;
        }
    }

    setuiChange( MSG_TRIGGER_CODE);

    return ERR_NONE;
}

/**
 * @brief servSPITrigger::getTimeout
 * @return
 */
qint64 servSPITrigger::getTimeout( void )
{
    qint64 time = cfg_get_spi_timeout();
    //! update attr
    setuiStep( (time<time_ns(100)) ? time_ps(400): getTimeStep(time) );
    setuiRange(time_ns(8),time_s(10),time_ns(16));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );

    return time;
}

/**
 * @brief servSPITrigger::setTimeout
 * @param t
 * @return
 */
int servSPITrigger::setTimeout(qint64 t)
{
    return cfg_set_spi_timeout(t);
}

/**
 * @brief servSPITrigger::getCode
 * @param arg
 * @return
 */
void servSPITrigger::getCode(CArgument &arg)
{
    QBitArray &min  = cfg_get_spi_data_Min();
    QBitArray &mask = cfg_get_spi_data_Mask();

    arg.clear();
    for(int i = m_bitCount-1; i >= 0 ; i--)
    {
        int value = Trigger_Code_X;
        if(mask.at(i))
        {
            value = min.at(i );
        }
        else
        {
            value = Trigger_Code_X;
        }
        arg.append(value);
    }
}


/**
 * @brief servSPITrigger::getBitCount
 * @return
 */
int servSPITrigger::getBitCount()
{
    setuiZVal(  spiCfg.bitCountMin );
    setuiMaxVal(spiCfg.bitCountMax);
    setuiMinVal(spiCfg.bitCountMin);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return m_bitCount;
}

/**
 * @brief servSPITrigger::setBitCount
 * @param a
 * @return
 */
int servSPITrigger::setBitCount(int count)
{
    m_bitCount = count;
    m_currBit = qMin(m_currBit,getBitByteMax()-1);

    return cfg_set_spi_bitCount(count);
}

/**
 * @brief servSPITrigger::getCurrBit
 * @return
 */
int servSPITrigger::getCurrBit()
{
    mUiAttr.setZVal(MSG_TRIGGER_SPI_CURRBIT  , TRIG_CURRBIT_Z_VALUE);
    return m_currBit;
}

/**
 * @brief servSPITrigger::setCurrBit
 * @param a
 * @return
 */
int servSPITrigger::setCurrBit(int bit)
{
    //!按下 切换当前位的值
    if(TRIG_CURRBIT_Z_VALUE == bit)
    {
        int    curValue = getValue();
        bool   binHex = true;

        if( m_currBit >= getBitMax() )
        {
            binHex = false;
        }

        int stop = (getBitMax()%4)?((1<<getBitMax()%4)-1):(0xf); //! 0x00-0xf
        stop = (m_currBit == getBitMax())?stop:(0xf);
        return setValue( trigCode::get_z_value(curValue,binHex, stop) );
    }
    else //!旋转 切换当前位置
    {
        int b = bit  + m_currBit;

        b = qMax(b, 0);
        b = qMin(b, getBitByteMax()-1 );

        m_currBit = b;
    }
    return ERR_NONE;
}

/**
 * @brief servSPITrigger::setAll
 * @return
 */
int servSPITrigger::setAll(int value )
{
    QBitArray &min  = cfg_get_spi_data_Min();
    QBitArray &max  = cfg_get_spi_data_Max();
    QBitArray &mask = cfg_get_spi_data_Mask();

    trigCode::setAllValue(   min,
                             mask,
                             m_currBit,
                             value,
                             getBitMax() );

    trigCode::setAllValue(   max,
                             mask,
                             m_currBit,
                             value,
                             getBitMax() );
    return apply();
}
/**
 * @brief servSPITrigger::getValue
 * @return
 */
int servSPITrigger::getValue(void)
{
    QBitArray &min  = cfg_get_spi_data_Min();
    QBitArray &mask = cfg_get_spi_data_Mask();

    int value = trigCode::getBitValue(min,
                                      mask,
                                      m_currBit,
                                      getBitMax() );
    return value;
}
/**
 * @brief servSPITrigger::setValue
 * @param s
 * @return
 */
int servSPITrigger::setValue(int s )
{
    QBitArray &min  = cfg_get_spi_data_Min();
    QBitArray &max  = cfg_get_spi_data_Max();
    QBitArray &mask = cfg_get_spi_data_Mask();

    trigCode::setBitValue(min,
                          mask,
                          m_currBit,
                          s,
                          getBitMax() );

    trigCode::setBitValue(max,
                          mask,
                          m_currBit,
                          s,
                          getBitMax() );
    return apply();
}

int servSPITrigger::getBitMax()
{
    return m_bitCount;
}

int servSPITrigger::getBitByteMax()
{
    return getBitMax() + (getBitMax()+3)/4;
}

DsoErr servSPITrigger::setCfg(pointer p)
{
    DsoErr err = ERR_NONE;
    err = cfg_spi_setCfg(p);
    updateAllUi();
    return err;
}

pointer servSPITrigger::getCfg()
{
    return cfg_spi_getCfg();
}

int servSPITrigger::setLevelClk(qint64 l)
{
    Chan ch        = (Chan)cfg_get_spi_scl();
    m_pCurrSource  = m_pSources[ch];
    return serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL,
                                  (qint64)(l/getProbeRatio(ch)) );
}

qint64 servSPITrigger::getLevelClk()
{
    Chan ch          = (Chan)cfg_get_spi_scl();
    qint64 l         = getSourceObj(ch)->getLevel(0);
    return l*getProbeRatio(ch);
}

int servSPITrigger::setLevelData(qint64 l)
{
    Chan ch        = (Chan)cfg_get_spi_sda();
    m_pCurrSource  = m_pSources[ch];
    return serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL,
                                  (qint64)(l/getProbeRatio(ch)) );
}

qint64 servSPITrigger::getLevelData()
{
    Chan ch          = (Chan)cfg_get_spi_sda();
    qint64 l         = getSourceObj(ch)->getLevel(0);
    return l*getProbeRatio(ch);
}

int servSPITrigger::setLevelCs(qint64 l)
{
    Chan ch        = (Chan)cfg_get_spi_cs();
    m_pCurrSource  = m_pSources[ch];
    return serviceExecutor::post( serv_name_trigger, MSG_TRIGGER_LEVEL,
                                  (qint64)(l/getProbeRatio(ch)) );

}

qint64 servSPITrigger::getLevelCs()
{
    Chan ch          = (Chan)cfg_get_spi_cs();
    qint64 l         = getSourceObj(ch)->getLevel(0);
    return l*getProbeRatio(ch);
}



/*! cfg---------------------------------------------------------*/
#define ar ar_out
#define ar_pack_e ar_out
int servSpiCfg::cfg_spi_serialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("mClk"      , spiCfg.mClk      );
    ar_pack_e("mData"     , spiCfg.mData     );
    ar_pack_e("mCs"       , spiCfg.mCs       );
    ar_pack_e("mClkSlope" , spiCfg.mClkSlope );
    ar_pack_e("mCsSlope"  , spiCfg.mCsSlope  );
    ar_pack_e("mType"     , spiCfg.mType     );

    ar_pack_e("mDatCmp"   , spiCfg.mDatCmp   );
    ar_pack_e("mDataLen"  , spiCfg.mDataLen  );

    ar("mTmo", spiCfg.mTmo) ;
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servSpiCfg::cfg_spi_serialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)
    ar_pack_e("mClk"      , spiCfg.mClk      );
    ar_pack_e("mData"     , spiCfg.mData     );
    ar_pack_e("mCs"       , spiCfg.mCs       );
    ar_pack_e("mClkSlope" , spiCfg.mClkSlope );
    ar_pack_e("mCsSlope"  , spiCfg.mCsSlope  );
    ar_pack_e("mType"     , spiCfg.mType     );

    ar_pack_e("mDatCmp"   , spiCfg.mDatCmp   );
    ar_pack_e("mDataLen"  , spiCfg.mDataLen  );

    ar("mTmo", spiCfg.mTmo ) ;

    spiCfg.mDataMin  = QBitArray(spiCfg.bitCountMax);
    spiCfg.mDataMax  = QBitArray(spiCfg.bitCountMax);
    spiCfg.mDataMask = QBitArray(spiCfg.bitCountMax);
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

void servSpiCfg::cfg_spi_init()
{
    spiCfg.setClk(  chan1 );
    spiCfg.setData( chan2 );
    spiCfg.setCs(   chan3 );
    spiCfg.setClkSlope( Trigger_Edge_Rising );
    spiCfg.setCsSlope(  Trigger_Edge_Rising );
    spiCfg.setType(     trig_spi_cs         );
    spiCfg.setTmo(      time_us(1)          );
    spiCfg.setDatCmp(   cmp_eq              );

    spiCfg.setDataLen(spiCfg.bitCountDef - 1);
    spiCfg.mDataMin  = QBitArray(spiCfg.bitCountMax);
    spiCfg.mDataMax  = QBitArray(spiCfg.bitCountMax);
    spiCfg.mDataMask = QBitArray(spiCfg.bitCountMax);
}

bool servSpiCfg::cfg_get_spi_when()
{
    return spiCfg.getType();
}

int servSpiCfg::cfg_set_spi_when(bool b)
{
    spiCfg.setType( b? trig_spi_idle : trig_spi_cs );

    return apply();
}

bool servSpiCfg::cfg_get_spi_cs_mode()
{
    bool cs = (bool)spiCfg.getCsSlope();
    return (!cs);
}

int servSpiCfg::cfg_set_spi_cs_mode(bool b)
{
    EdgeSlope cs = b? Trigger_Edge_Rising : Trigger_Edge_Falling;
    spiCfg.setCsSlope( cs );

    return apply();
}

int servSpiCfg::cfg_get_spi_scl()
{
    return spiCfg.clk();
}

int servSpiCfg::cfg_set_spi_scl(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        spiCfg.setClk(Chan(src));
        return apply();
    }
}

bool servSpiCfg::cfg_get_spi_edge()
{
    return spiCfg.getClkSlope();
}

int servSpiCfg::cfg_set_spi_edge(bool b)
{
    spiCfg.setClkSlope(b? Trigger_Edge_Falling : Trigger_Edge_Rising );

    return apply();
}

int servSpiCfg::cfg_get_spi_sda()
{
    return spiCfg.data();
}

int servSpiCfg::cfg_set_spi_sda(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        spiCfg.setData(Chan(src));
        return apply();
    }
}

int servSpiCfg::cfg_get_spi_cs()
{
    return spiCfg.cs();
}

int servSpiCfg::cfg_set_spi_cs(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        spiCfg.setCs(Chan(src));
        return apply();
    }
}


int servSpiCfg::cfg_get_spi_bitCount()
{
    return spiCfg.mDataLen + 1 ;
}

int servSpiCfg::cfg_set_spi_bitCount(int count)
{
    spiCfg.mDataMin.fill( false, count, spiCfg.bitCountMax);
    spiCfg.mDataMax.fill( false, count, spiCfg.bitCountMax);
    spiCfg.mDataMask.fill(false, count, spiCfg.bitCountMax);

    spiCfg.setDataLen(count-1);
    return apply();
}

qint64 servSpiCfg::cfg_get_spi_timeout()
{
    return spiCfg.getTmo();
}

int servSpiCfg::cfg_set_spi_timeout(qint64 t)
{
    spiCfg.setTmo(t);
    return apply();
}

QBitArray &servSpiCfg::cfg_get_spi_data_Min()
{
    return spiCfg.mDataMin;
}

QBitArray &servSpiCfg::cfg_get_spi_data_Max()
{
    return spiCfg.mDataMax;
}

QBitArray &servSpiCfg::cfg_get_spi_data_Mask()
{
    return spiCfg.mDataMask;
}

qint64 servSpiCfg::cfg_get_spi_data()
{
    return 0;
}

int servSpiCfg::cfg_set_spi_data(qint64 /*data*/)
{
    return ERR_NONE;
}

int servSpiCfg::cfg_spi_setCfg(void *p)
{
    TrigSpiCfg  *pCfg = static_cast<TrigSpiCfg *>(p);
    Q_ASSERT(pCfg != NULL);
    spiCfg = *pCfg;
    return apply();
}

void *servSpiCfg::cfg_spi_getCfg()
{
    return &spiCfg;
}




