#include "../service_msg.h"
#include "../servch/servch.h"
#include "../../app/appdisplay/cgrid.h"
#include "servtrig.h"

IMPLEMENT_CMD( serviceExecutor, TriggerBase )
start_of_entry()

on_set_int_int  ( MSG_TRIGGER_TYPE,                &TriggerBase::setMode),
on_get_int      ( MSG_TRIGGER_TYPE,                &TriggerBase::getSelfMode),
on_get_int      ( MSG_TRIGGER_SOURCE,              &TriggerBase::getSource),
on_get_int      ( MSG_TRIGGER_SOURCE_LA,           &TriggerBase::getSource),
on_get_int      ( MSG_TRIGGER_SOURCE_LA_EXT,       &TriggerBase::getSource),
on_get_int      ( MSG_TRIGGER_SOURCE_LA_EXT_AC,    &TriggerBase::getSource),
on_get_pointer  ( MSG_TRIGGER_SOURCE_PTR,          &TriggerBase::getSourcePtr),

on_set_int_bool ( MSG_TRIGGER_NOISE,               &TriggerBase::setNoiseReject),
on_get_bool     ( MSG_TRIGGER_NOISE,               &TriggerBase::getNoiseReject),
on_set_int_ll   ( MSG_TRIGGER_HOLDOFF,             &TriggerBase::setTrigHoldoff),
on_get_ll       ( MSG_TRIGGER_HOLDOFF,             &TriggerBase::getTrigHoldoff),

on_set_void_void( CMD_TRIGGER_ALL_SPY_LICENSE,     &TriggerBase::spyLicenseOpt),

on_set_int_int  ( MSG_CHAN_INVERT,                 &TriggerBase::sourceInvert),

on_set_void_void( CMD_SERVICE_RST,                 &TriggerBase::rst ),
on_set_int_int  ( CMD_SERVICE_ITEM_FOCUS,          &TriggerBase::setItemFocus ),
on_get_int      ( CMD_SERVICE_ITEM_FOCUS,          &TriggerBase::getItemFocus ),
end_of_entry()

/**************************static member****************************/
QString         TriggerBase::m_TriggerName[Trigger_All];
CSource*        TriggerBase::m_pSources[all_trig_src] = {0};
TriggerSweep    TriggerBase::m_nSweep       = Trigger_Sweep_Auto;
TriggerSweep    TriggerBase::m_nSingleLast  = Trigger_Sweep_Auto;
TriggerMode     TriggerBase::m_nMode        = Trigger_Edge;
Coupling        TriggerBase::m_nCouple      = DC;
qint64          TriggerBase::m_s64Holdoff   = time_ns(8);
bool            TriggerBase::m_bNoiseReject = false;
bool            TriggerBase::m_bRoll        = false;
bool            TriggerBase::m_bInvert[]    = {false,false,false,false,false};
int             TriggerBase::m_itemFocus     = 0;
///////////////////////Trigger Base API Implementation////////////////////


TriggerBase::TriggerBase( QString name,TriggerMode m, ServiceId eId, bool hasTwoLevel )
    : serviceExecutor( name, eId )
{
    mVersion = 0x03;

    Q_ASSERT(m >= Trigger_Edge && m < Trigger_All);
    if(m_pSources[0] == NULL)
    {
        for(int key=chan1; key<all_trig_src; key++)
        {
            m_pSources[key] = new CSource(key);
            Q_ASSERT(m_pSources[key]);
        }
        m_pSources[0] = m_pSources[chan1];
    }
    m_pCurrSource = getSourceObj(chan1);
    
    m_bDoubleLevel = hasTwoLevel;
    m_nLevelSelect = Trigger_Level_High;
    
    TriggerBase::m_TriggerName[m] = name;
    
    m_nMyID    = m;
}

#define ar ar_out
#define ar_pack_e ar_out
#define ar_pack_s(name, val) stream.write(name, val)
int TriggerBase::serialOut(CStream &stream, unsigned char &ver)
{
    ISerial::serialOut(stream, ver );

    int value = m_pCurrSource->getChan();
    ar_pack_e("m_currSource",        value);
    ar_pack_e("m_nMyID",             m_nMyID);
    ar_pack_e("m_nLevelSelect",      m_nLevelSelect);
    ar(       "m_bDoubleLevel",      m_bDoubleLevel);

    return 0;
}

int TriggerBase::commomSerialOut(CStream &stream, unsigned char &ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)

    ar_pack_e("m_nSweep",      m_nSweep);
    ar_pack_e("m_nSingleLast", m_nSingleLast);
    ar_pack_e("m_nMode",       m_nMode);
    ar_pack_e("m_nCouple",     m_nCouple);
    ar("m_s64Holdoff",         m_s64Holdoff);
    ar("m_bNoiseReject",       m_bNoiseReject);
    ar("m_bRoll",              m_bRoll);

    qint64 i64_value = 0;
    for(int i = 0; i < array_count(m_pSources); i++)
    {
        i64_value = m_pSources[i]->getLevel(0);
        ar_pack_s(QString("level_%1_a").arg(i), i64_value);

        i64_value = m_pSources[i]->getLevel(1);
        ar_pack_s(QString("level_%1_b").arg(i), i64_value);
    }
    return 0;
}
#undef ar
#undef ar_pack_e
#undef ar_pack_s

#define ar ar_in
#define ar_pack_e ar_e_in
#define ar_pack_s(name, val) stream.read(name, val)
int TriggerBase::serialIn(CStream &stream, unsigned char ver)
{
    ISerial::serialIn(stream, ver );

    if(ver == mVersion)
    {
        int value=0;
        ar_pack_e("m_currSource",        value);
        ar_pack_e("m_nMyID",             m_nMyID);
        ar_pack_e("m_nLevelSelect",      m_nLevelSelect);
        ar(       "m_bDoubleLevel",      m_bDoubleLevel);
        m_pCurrSource = m_pSources[value];
    }
    else
    {
        rst();
    }
    return 0;
}

int TriggerBase::commomSerialIn(CStream &stream, unsigned char ver)
{
    Q_UNUSED(stream)
    Q_UNUSED(ver)

    ar_pack_e("m_nSweep",      m_nSweep);
    ar_pack_e("m_nSingleLast", m_nSingleLast);
    ar_pack_e("m_nMode",       m_nMode);
    ar_pack_e("m_nCouple",     m_nCouple);
    ar("m_s64Holdoff",         m_s64Holdoff);
    ar("m_bNoiseReject",       m_bNoiseReject);
    ar("m_bRoll",              m_bRoll);

    qint64 i64_value = 0;
    for(int i = 0; i < array_count(m_pSources); i++)
    {
        ar_pack_s(QString("level_%1_a").arg(i), i64_value);
        m_pSources[i]->setLevel(0,i64_value);

        ar_pack_s(QString("level_%1_b").arg(i), i64_value);
        m_pSources[i]->setLevel(1,i64_value);
    }
    return 0;
}
#undef ar
#undef ar_pack_e
#undef ar_pack_s

void TriggerBase::rst()
{
    m_pCurrSource = m_pSources[chan1];
    resetSource();
    setSweep(Trigger_Sweep_Auto);
    setCoupling(DC);
    setTrigHoldoff(time_ns(16));
    m_bNoiseReject = false;
}

int TriggerBase::startup()
{
    setCoupling(    getCoupling()    );
    setNoiseReject( getNoiseReject() );
    setTrigHoldoff( getTrigHoldoff() );
    return 0;
}


void TriggerBase::registerSpy()
{
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Active , CMD_TRIGGER_ALL_SPY_LICENSE);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Exp    , CMD_TRIGGER_ALL_SPY_LICENSE);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Invalid, CMD_TRIGGER_ALL_SPY_LICENSE);
}

DsoErr TriggerBase::setActive(int active)
{
    DsoErr err = ERR_NONE;
    err = postEngine( ENGINE_TRIGGER_CHs_EN_RST);
    if(ERR_NONE != err)return err;

    err = postEngine( ENGINE_TRIGGER_CH_EN, m_pCurrSource->getChan(), true);
    if(ERR_NONE != err)return err;

    err = setSourcesTwoLevel( m_bDoubleLevel );
    if(ERR_NONE != err)return err;

    err = setCoupling(m_nCouple);
    if(ERR_NONE != err)return err;

    err = applyLevel();
    if(ERR_NONE != err)return err;
    
    err = apply();
    if(ERR_NONE != err)return err;
    
    m_nMode = m_nMyID;
    return serviceExecutor::setActive(active);
}

DsoErr TriggerBase::setItemFocus(int msg)
{
    //qDebug()<<__FILE__<<__LINE__<<getName()<<msg;
    m_itemFocus = msg;
    return ERR_NONE;
}

int TriggerBase::getItemFocus()
{
    return m_itemFocus;
}


/// \brief TriggerBase::currTrigger
/// \return
///
QString TriggerBase::currTrigger( void )
{
    return TriggerBase::m_TriggerName[TriggerBase::m_nMode];
}

///
/// \brief TriggerBase::currTriggerMode
/// \return
///
TriggerMode TriggerBase::currTriggerMode( void )
{
    return TriggerBase::m_nMode;
}

QString TriggerBase::getTriggerName(TriggerMode mode)
{
    Q_ASSERT( (mode>=Trigger_Edge)&&(mode<Trigger_All) );
    return TriggerBase::m_TriggerName[mode];
}

TriggerMode TriggerBase::getMode()
{
    return currTriggerMode();
}

TriggerMode TriggerBase::getSelfMode() const
{
    return m_nMyID;
}

int TriggerBase::setMode( TriggerMode mode )
{
    setuiNodeCompress( true );
    
    m_nMode = mode;
    QString  trigName  = m_TriggerName[mode];

    return post(getServiceId(trigName), CMD_SERVICE_ACTIVE, (int)1 );
}


int TriggerBase::setSourceEN(int src)
{
    return postEngine( ENGINE_TRIGGER_CH_EN, src, true);
}
int TriggerBase::setSource(int src)
{
    m_pCurrSource =  m_pSources[src];
    Q_ASSERT( m_pCurrSource != NULL );


    if((src >= chan1) && (src <= chan4))
    {
        mUiAttr.setEnable(MSG_TRIGGER_NOISE, true );
    }
    else
    {
        mUiAttr.setEnable(MSG_TRIGGER_NOISE, false);
    }

    postEngine( ENGINE_TRIGGER_CHs_EN_RST);
    postEngine( ENGINE_TRIGGER_CH_EN, m_pCurrSource->getChan(), true);
    return ERR_NONE;
}
int TriggerBase::getSource( void )
{
    return m_pCurrSource->getChan();
}

CSource* TriggerBase::getSourcePtr(void)
{
    return m_pCurrSource;
}

CSource* TriggerBase::getChanSource(int ch)
{
    if( ch < all_trig_src )
    {
        return m_pSources[ch];
    }
    return NULL;
}

void TriggerBase::resetSource( bool b )
{
    for(int key = chan1; key < all_trig_src; key++)
    {
        m_pSources[key]->setChan(key);

        /*! [dba, 2018-04-18], bug:结构体已经初始化*/
        //m_pSources[key]->setLevel(0,0);
        //m_pSources[key]->setLevel(1,60);

        m_pSources[key]->setTwoLevel(b);
    }
    m_pCurrSource = m_pSources[chan1];
}

float TriggerBase::getProbeRatio(Chan ch)
{
    float fRatio = 1.0;
    
    if( (chan1 <= ch) && ( ch <=  chan4) )
    {
        QString name = QString("chan%1").arg( ch );
        serviceExecutor::query( name, servCH::qcmd_probe_real,  fRatio);
    }
    
    Q_ASSERT(fRatio != 0);
    
    return fRatio;
}


bool TriggerBase::getDoubleLevel()
{
    return m_bDoubleLevel;
}

DsoErr TriggerBase::setSourcesTwoLevel(bool b)
{
    for(int key = (int)chan1; key <= (int)chan4; key++)
    {
        TriggerBase::m_pSources[key]->setTwoLevel(true, 0);
        TriggerBase::m_pSources[key]->setTwoLevel(b,    1);
    }
    return ERR_NONE;
}

static int opt_trig_Type[][2] = {
    {OPT_COMP,  Trigger_RS232   },
    {OPT_EMBD,  Trigger_I2C     },
    {OPT_EMBD,  Trigger_SPI     },
    {OPT_AUTO,  Trigger_CAN     },
    {OPT_AUTO,  Trigger_LIN     },
    {OPT_FLEX,  Trigger_FlexRay },
    {OPT_AUDIO, Trigger_I2S     },
    {OPT_AERO,  Trigger_1553    },
    //{OPT_SENSOR,  Trigger_AB    },
};
static int opt_trig_la[][2] = {
    {E_SERVICE_ID_TRIG_Edge,       MSG_TRIGGER_SOURCE_LA_EXT_AC},
    {E_SERVICE_ID_TRIG_CAN,        MSG_TRIGGER_SOURCE_LA},
    {E_SERVICE_ID_TRIG_Duration,   MSG_TRIGGER_SOURCE_LA},
    {E_SERVICE_ID_TRIG_FLEXRAY,    MSG_TRIGGER_SOURCE_LA},
    {E_SERVICE_ID_TRIG_LIN,        MSG_TRIGGER_SOURCE_LA},
    {E_SERVICE_ID_TRIG_Nth,        MSG_TRIGGER_SOURCE_LA},
    {E_SERVICE_ID_TRIG_Pattern,    MSG_TRIGGER_SOURCE_LA},
    {E_SERVICE_ID_TRIG_Pulse,      MSG_TRIGGER_SOURCE_LA},
    {E_SERVICE_ID_TRIG_RS232,      MSG_TRIGGER_SOURCE_LA},
                                   
    {E_SERVICE_ID_TRIG_I2C,        MSG_TRIGGER_I2C_SCL},
    {E_SERVICE_ID_TRIG_I2C,        MSG_TRIGGER_I2C_SDA},
                                   
    {E_SERVICE_ID_TRIG_SPI,        MSG_TRIGGER_SPI_SCL},
    {E_SERVICE_ID_TRIG_SPI,        MSG_TRIGGER_SPI_SDA},
    {E_SERVICE_ID_TRIG_SPI,        MSG_TRIGGER_SPI_CS},
                                   
    {E_SERVICE_ID_TRIG_Timeout,    MSG_TRIGGER_SOURCE_LA},

    {E_SERVICE_ID_TRIG_Delay,      MSG_TRIGGER_DELAY_SRCA},
    {E_SERVICE_ID_TRIG_Delay,      MSG_TRIGGER_DELAY_SRCB},
    {E_SERVICE_ID_TRIG_SetupHold,  MSG_TRIGGER_SETUP_SCL},
    {E_SERVICE_ID_TRIG_SetupHold,  MSG_TRIGGER_SETUP_SDA},

    {E_SERVICE_ID_TRIG_IIS,        MSG_TRIGGER_IIS_SCLK},
    {E_SERVICE_ID_TRIG_IIS,        MSG_TRIGGER_IIS_WS},
    {E_SERVICE_ID_TRIG_IIS,        MSG_TRIGGER_IIS_SDA},
};

///
/// \brief TriggerBase::spyLicenseOpt
///
/// modified by hxh
void TriggerBase::spyLicenseOpt()
{
    bool en;
    for(int i = 0; i < array_count(opt_trig_Type); i++)
    {
        en = sysCheckLicense((OptType)opt_trig_Type[i][0]);

        mUiAttr.setVisible(MSG_TRIGGER_TYPE, opt_trig_Type[i][1], en);

        if( (!en) && (getMode() == opt_trig_Type[i][1]) )
        {
            setMode(Trigger_Edge);
        }
    }

    en = sysHasLA();
    for(int i = 0; i < array_count(opt_trig_la); i++)
    {
        if( service::getId() == opt_trig_la[i][0])
        {
            int chCmd = opt_trig_la[i][1];

            for(int i = d0; i <= d15; i++)
            {
                mUiAttr.setVisible(chCmd, i, en);
            }
        }
    }
}

/*!
 * \brief TriggerBase::checkTimeRange \n
 * 对涉及时间比较的参数制定的参数检查规则。
 */
DsoErr TriggerBase::checkTimeRange(TrigTimeRule rule, const EMoreThan w0,
                                   const qint64 tMin, const qint64 tMax
                                 )
{
    EMoreThan w  = Trigger_When_None;
    qint64 tL = 0, tH = 0;
    //！获取规则检查需要的参数
    getTimeRangeValue(tL,tH,w);

    //!进行规则检查
    trigrules::timeRangeRule(rule, tL, tH,
                             w, w0, tMin, tMax);
    //！检查后的参数重新配置
    return setTimeRangeValue(tL,tH);
}

void TriggerBase::getTimeRangeValue(qint64 &, qint64 &, EMoreThan &)
{
    qWarning()<<__FILE__<<__FUNCTION__<<"Warning:Does not implement the function";
}

DsoErr TriggerBase::setTimeRangeValue(qint64 &, qint64 &)
{
    qWarning()<<__FILE__<<__FUNCTION__<<"Warning:Does not implement the function";
    return ERR_NONE;
}

void TriggerBase::getUpperAttr(enumAttr)
{
    qint64 tL = 0, tH = 0;
    EMoreThan w = Trigger_When_None;
    getTimeRangeValue(tL, tH, w);
    SET_TRIG_TIME_ATTR(tH, time_ps(800), time_s(10), G_s64UpperDefault);
}

void TriggerBase::getLowerAttr(enumAttr)
{
    qint64 tL = 0, tH = 0;
    EMoreThan w = Trigger_When_None;
    getTimeRangeValue(tL, tH, w);
    SET_TRIG_TIME_ATTR(tL, time_ps(800), time_s(10), G_s64UpperDefault);
}


CSource* TriggerBase::getSourceObj(int key)
{
    if(!(key > chan_none && key < all_trig_src))
    {
        qDebug()<<__FILE__<<__LINE__<<m_nMyID<<key;
        Q_ASSERT(false);
    }

    return m_pSources[key];
}

DsoErr TriggerBase::setSweep( TriggerSweep sweep )
{    
    if( (Trigger_Sweep_Single == sweep)
            && (Trigger_Sweep_Single != TriggerBase::m_nSweep) )
    {
        m_nSingleLast = m_nSweep;
    }

    TriggerBase::m_nSweep = sweep;
    return ERR_NONE;
}
TriggerSweep TriggerBase::getSweep(void)
{
    return   TriggerBase::m_nSweep;
}

TriggerSweep TriggerBase::getSingleLast()
{
    return TriggerBase::m_nSingleLast;
}


/*!
 * \brief TriggerBase::getCoupling
 * \return
 */
Coupling TriggerBase::getCoupling(void)
{
    int src = getSource();
    if( src >= chan1 && src <= chan4)
    {
        return TriggerBase::m_nCouple;
    }
    else
    {
        return DC;
    }
}

/*!
 * \brief TriggerBase::setCoupling
 * \param coup
 * \return
 */
int TriggerBase::setCoupling( Coupling coup )
{
    TriggerBase::m_nCouple = coup;

    for(int ch = chan1; ch <= chan4; ch++)
    {
         postEngine( ENGINE_TRIGGER_COUPLING, ch,(int)DC );
    }

    int src = getSource();
    if( (m_nMyID == Trigger_Edge) && (src >= chan1) && (src <= chan4) )
    {
        postEngine( ENGINE_TRIGGER_COUPLING, getSource(),(int)m_nCouple );
    }

    return ERR_NONE;
}

/*!
 * \brief TriggerBase::getNoiseReject
 * \return
 */
bool TriggerBase::getNoiseReject( void )
{
    int ch = getSource();
    if( !(chan1 <= ch && ch <= chan4) )
    {
        return false;
    }

    if( m_pCurrSource->get_nScale() < mv(5) )
    {
        return true;
    }

    return TriggerBase::m_bNoiseReject;
}

/*!
 * \brief TriggerBase::setNoiseReject
 * \param b
 * \return
 */
int TriggerBase::setNoiseReject(bool b)
{
    TriggerBase::m_bNoiseReject = b;
    
    //需要重新配置触发电平的阈值
    return setLevel(getLevel());
}

/*!
 * \brief TriggerBase::setForce
 * \return
 */
int TriggerBase::setForce()
{
    return postEngine( ENGINE_TRIGGER_FORCE );
}

Trigger_Level_ID TriggerBase::getLevelSelect()
{
    return m_nLevelSelect;
}

int TriggerBase::setLevelSelect(Trigger_Level_ID id)
{
    m_nLevelSelect = id;
    
    return ERR_NONE;
}

///
/// \brief TriggerBase::setTrigHoldObj
/// \param hold
/// \return
///
int TriggerBase::setTrigHoldObj( TriggerHoldObj hold )
{
    hold = hold;
    //    CTrigger::setTrigHoldObj( hold );
    
    //    return postEngine( ENGINE_TRIGGER_HOLDER, (int)hold );
    
    return 0;
}

///
/// \brief TriggerBase::setTrigHoldMode
/// \param mode
/// \return
///
int TriggerBase::setTrigHoldMode( TriggerHoldMode mode )
{
    mode = mode;
    //    getTrigHold().setMode( mode );
    
    //    return postEngine( ENGINE_TRIGGER_HOLDER_MODE, (int)mode );
    return 0;
}

///
/// \brief TriggerBase::setTrigHoldCount
/// \param count
/// \return
///
int TriggerBase::setTrigHoldCount( int count )
{
    count = count;
    //    getTrigHold().setCount( count );
    
    //    return postEngine( ENGINE_TRIGGER_HOLD_COUNT, count );
    
    return 0;
}

///
/// \brief TriggerBase::getTrigHoldoff
/// \return
///
qint64 TriggerBase::getTrigHoldoff( void )
{
    //! update attr
    setuiStep( getTimeStep(TriggerBase::m_s64Holdoff) );
    setuiRange(time_ns(8),time_s(10),time_ns(8));
    setuiUnit(Unit_s);
    setuiBase(1, E_N12);
    setuiPostStr( "s" );
    return TriggerBase::m_s64Holdoff;
}

///
/// \brief TriggerBase::setTrigHoldoff
/// \param h
/// \return
///
DsoErr TriggerBase::setTrigHoldoff( qint64 h )
{
    m_s64Holdoff = h;
    return postEngine( ENGINE_TRIGGER_HOLD_TIME, (quint64)h );
}


/**
 * @brief TriggerBase::getLevel
 * @param key
 * @return
 */
qint64 TriggerBase::getLevel( void )
{
    if(m_nLevelSelect == Trigger_Level_Double)
    {
        return m_pCurrSource->getLevel(Trigger_Level_High);
    }
    else
    {
        return m_pCurrSource->getLevel(m_nLevelSelect);
    }
}

/**
 * @brief TriggerBase::setLevel
 * @param key
 * @param level
 * @return
 */
int TriggerBase::setLevel(qint64 level)
{
    DsoErr err = ERR_NONE;

    if(m_nLevelSelect == Trigger_Level_Double)
    {
        qint64 dL = m_pCurrSource->getLevel(Trigger_Level_High) - m_pCurrSource->getLevel(Trigger_Level_Low);

        qint64 H =  level;
        qint64 L =  H - dL;

        if( H > m_pCurrSource->get_nLevelMax() )
        {
            H = m_pCurrSource->get_nLevelMax();
            L = H - dL;
        }

        if(L < m_pCurrSource->get_nLevelMin())
        {
            L = m_pCurrSource->get_nLevelMin();
            H = L + dL;
        }

        m_pCurrSource->setLevel(Trigger_Level_High,H);
        if(ERR_NONE != err) return err;

        m_pCurrSource->setLevel(Trigger_Level_Low,L);
        if(ERR_NONE != err) return err;
    }
    else if(m_nLevelSelect == Trigger_Level_Low)
    {
        if(m_bDoubleLevel) //!双触发电平模式
        {
            level = qMin(m_pCurrSource->getLevel(Trigger_Level_High), level);
        }
        
        m_pCurrSource->setLevel(m_nLevelSelect,level);
        if(ERR_NONE != err) return err;
    }
    else if(m_nLevelSelect == Trigger_Level_High)
    {
        if(m_bDoubleLevel)
        {
            level = qMax(m_pCurrSource->getLevel(Trigger_Level_Low), level);
        }
        
        m_pCurrSource->setLevel(m_nLevelSelect,level);
        if(ERR_NONE != err) return err;
    }
    else
    {
        Q_ASSERT(false);
    }
    return applyLevel();
}

int TriggerBase::getChSlope(Chan /*ch*/)
{
    return Trigger_Edge_Rising;
}


DsoErr TriggerBase::applyLevel()
{
    return applyLevelEngine( (Chan)m_pCurrSource->getChan() );
}

#define fmt_level   dso_fmt_trim(fmt_float,fmt_width_3,fmt_no_trim_0)
void TriggerBase::dispLevel()
{
    QStringList levelList;
    QList<QColor>  levelCorlorList;
    levelCorlorList<<Trig_Color<<Trig_Color<<Trig_Color;

    float fLevelH = m_pCurrSource->get_fLevel(Trigger_Level_High);
    float fLevelL = m_pCurrSource->get_fLevel(Trigger_Level_Low);;

    //! for bug:99.9999847格式化为了99.99
    //! 所以在第七位上补了一个数不影响正常结果
    fLevelH += fLevelH / 1e6;
    fLevelL += fLevelL / 1e6;

    //!格式化
    QString str;
    if( m_pCurrSource->hasTwoLevel(Trigger_Level_High)
            && m_pCurrSource->hasTwoLevel(Trigger_Level_Low) )
    {
        gui_fmt::CGuiFormatter::format( str, fLevelH,           fmt_level );
        levelList<<QString("H: %1V").arg(str);
        gui_fmt::CGuiFormatter::format( str, fLevelL,           fmt_level );
        levelList<<QString("L: %1V").arg(str);
        gui_fmt::CGuiFormatter::format( str, fLevelH - fLevelL, fmt_level );
        levelList<<QString("Δ: %1V").arg(str);
        
        servGui::showInfo( levelList, levelCorlorList,
                           NULL,
                           servTrig::G_sLevelPoint.x(),
                           servTrig::G_sLevelPoint.y());
    }
    else if(m_pCurrSource->hasTwoLevel(Trigger_Level_High))
    {
        //! do not show level
    }
    else
    {
        //! do not show level
    }
}

DsoErr TriggerBase::initLevelEngine()
{
    DsoErr err = ERR_NONE;
    for ( int ch = chan1; ch <= chan4; ch++ )
    {
        err = applyLevelEngine((Chan)ch, 0, Trig_View_Rising_H);

        if(ERR_NONE != err)
        {
            return err;
        }
    }

    err =  applyLevelEngine(ext, 0, Trig_View_Rising_H);

    return err;
}

const _levelSlopeInc TriggerBase::s_a_levelSlopeInc[]=
{
    {Trig_View_Rising_H        ,  0,  -1, 0, -1 },
    {Trig_View_Falling_L       ,  1,   0, 1,  0 },
    {Trig_View_Alternating_H_L ,  0,  -1, 1,  0 },
    {Trig_View_Any_H_L         ,  0,  -1, 1,  0 },
                                       
    {Trig_View_Rising_L        ,  1,   0, 1,  0 },
    {Trig_View_Falling_H       ,  0,  -1, 0, -1 },
    {Trig_View_Alternating_L_H ,  1,   0, 0, -1 },
    {Trig_View_Any_L_H         ,  1,   0, 0, -1 },
                                       
    {Trig_View_Alternating_H_H ,  0,  -1, 0, -1 },
    {Trig_View_Alternating_L_L ,  1,   0, 1,  0 },
    {Trig_View_Any_H_H         ,  0,  -1, 0, -1 },
    {Trig_View_Any_L_L         ,  1,   0, 1,  0 },
    {Trig_View_Any_None        ,  0,   0, 0,  0 },
};


DsoErr TriggerBase::applyLevelEngine(Chan ch, qint64 gnd, int slope)
{
    DsoErr err = ERR_NONE;
    if(m_nMode != m_nMyID)
    {
        return err;
    }

    CSource  *pSource =  getSourceObj(ch);
    Q_ASSERT(pSource != NULL);

    qint64    nLeVelA  = pSource->getLevel(0) + gnd;
    qint64    nLeVelB  = pSource->getLevel(1) + gnd;
    //qDebug()<<__FILE__<<__LINE__<<"chan:"<<ch<<"------LEVEL A:"<<nLeVelA<<"--------LEVEL B:"<<nLeVelB;
    qint64    nMax = pSource->get_nLevelMax();
    qint64    nMin = pSource->get_nLevelMin();

    nLeVelA = qBound(nMin, nLeVelA, nMax);
    nLeVelB = qBound(nMin, nLeVelB, nMax);

    if(Trig_View_Any_None == slope)
    {
        slope = getChSlope(ch);
    }

    if( (chan1 <= ch) && (ch <=  chan4) )
    {
        //!档位
        int nScale = pSource->get_nScale();
        //!噪声容限
        float  noiseCoef = getNoiseReject()? 1.0f : 0.5f;

        int   aMin = 0, aMax = 0;
        int   bMin = 0, bMax = 0;

        mUiAttr.setEnable(MSG_TRIGGER_NOISE, (nScale >= mv(5)) );
        setuiChange(MSG_TRIGGER_NOISE);
        updateUiAttr();

        for(int i = 0; i < array_count(s_a_levelSlopeInc); i++)
        {
            if(slope == s_a_levelSlopeInc[i].slope)
            {
#if 0
                /**********************************************************************
                 * nScale < mv(5)            nNoise =  5mv
                 * mv(5) <= nScale < mv(10)  nNoise =  1div/2div
                 * nScale >= mv(10)          nNoise =  0.5div/1div
                ***********************************************************************/
                int nNoise = nScale*noiseCoef;
                if(  (mv(5) <= nScale) && (nScale < mv(10)) )
                {
                    nNoise = nNoise*2;
                }
                else if( nScale < mv(5) )
                {
                    nNoise = mv(5);
                }
                else
                {Q_ASSERT(nNoise>=mv(5));}
#else
                /**********************************************************************
                 * nScale <= mv(2)            nNoise =  4mv
                 * mv(2) < nScale <= mv(5)    nNoise =  5mv
                 * mv(5) < nScale < mv(10)    nNoise =  1div/2div
                 * nScale >= mv(10)           nNoise =  0.5div/1div
                ***********************************************************************/
                int nNoise = nScale*noiseCoef;
                if(nScale <= mv(2))
                {
                    nNoise = mv(4);
                }
                else if ((mv(2) < nScale) && (nScale <= mv(5)))
                {
                    nNoise = mv(5);
                }
                else if ((mv(5) < nScale) && (nScale < mv(10)))
                {
                    nNoise = nNoise * 2;
                }
#endif
                /* -- 史hui测试用 -- */
                //nNoise = 1.5*nScale;
                /* --------------- */

                /*****************************************************/
                aMax = (int)(nLeVelA + s_a_levelSlopeInc[i].aMaxInc * nNoise);
                aMin = (int)(nLeVelA + s_a_levelSlopeInc[i].aMinInc * nNoise);
                bMax = (int)(nLeVelB + s_a_levelSlopeInc[i].bMaxInc * nNoise);
                bMin = (int)(nLeVelB + s_a_levelSlopeInc[i].bMinInc * nNoise);
                break;
            }
        }

       //qDebug()<<__FILE__<<__LINE__<<"aMin:"<<aMin<<"aMax:"<<aMax;
        //!配置到引擎
        err = postEngine( ENGINE_TRIGGER_LEVEL_A,
                          ch, aMin, aMax);
        if(ERR_NONE != err) return err;

        err = postEngine( ENGINE_TRIGGER_LEVEL_B,
                          ch, bMin, bMax);
    }
    else if( (d0 <= ch) && (ch <=  d7) )
    {
        err =  post(E_SERVICE_ID_LA,MSG_LA_LOW_THRE_VAL,(int)nLeVelA);
    }
    else if( (d8 <= ch) && (ch <=  d15) )
    {
        err =  post(E_SERVICE_ID_LA,MSG_LA_HIGH_THRE_VAL,(int)nLeVelA);
    }
    else if(ext == ch)
    {
        int sens = m_bNoiseReject ?  7 : 3;
        err      =  postEngine( ENGINE_TRIGGER_LEVEL_EXT, (int)nLeVelA, sens);
    }
    else if(ext5 == ch)
    {
        int sens = m_bNoiseReject ?  7 : 3;
        err      =  postEngine( ENGINE_TRIGGER_LEVEL_EXT5, (int)nLeVelA, sens);
    }
    else
    {/*err = ERR_INVALID_INPUT*/}

    return err;
}

int TriggerBase::sourceInvert(Chan ch)
{
    if(!(ch > chan_none && ch < all_trig_src))
    {
        Q_ASSERT(false);
    }

    CSource  *pSource =  m_pSources[ch];
    Q_ASSERT(pSource != NULL);
    pSource->setInvert();
    apply();
    return applyLevel();
}

