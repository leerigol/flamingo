#include "servtrig.h"
#include "trigcode.h"

IMPLEMENT_POST_DO( TriggerBase, serv1553BTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,           &serv1553BTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,        &serv1553BTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, serv1553BTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE,             &serv1553BTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_1553B_WHEN,         &serv1553BTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_1553B_WHEN,         &serv1553BTrigger::getWhen),

on_set_int_int  (       MSG_TRIGGER_1553B_SYNC_TYPE,    &serv1553BTrigger::setSyncType),
on_get_int      (       MSG_TRIGGER_1553B_SYNC_TYPE,    &serv1553BTrigger::getSyncType),

on_set_int_int  (       MSG_TRIGGER_1553B_DATA_COMP,    &serv1553BTrigger::setDataComp),
on_get_int      (       MSG_TRIGGER_1553B_DATA_COMP,    &serv1553BTrigger::getDataComp),

on_set_int_int  (       MSG_TRIGGER_1553B_DATA_MIN,     &serv1553BTrigger::setDataMin),
on_get_int      (       MSG_TRIGGER_1553B_DATA_MIN,     &serv1553BTrigger::getDataMin),

on_set_int_int  (       MSG_TRIGGER_1553B_DATA_MAX,     &serv1553BTrigger::setDataMax),
on_get_int      (       MSG_TRIGGER_1553B_DATA_MAX,     &serv1553BTrigger::getDataMax),

on_set_int_ll   (       CMD_SCPI_TRIGGER_DATA_1553,     &serv1553BTrigger::setData),
on_get_ll       (       CMD_SCPI_TRIGGER_DATA_1553,     &serv1553BTrigger::getData),

on_set_int_int  (       MSG_TRIGGER_1553B_RTA,          &serv1553BTrigger::setRTA),
on_get_int      (       MSG_TRIGGER_1553B_RTA,          &serv1553BTrigger::getRTA),

on_set_int_int  (       MSG_TRIGGER_1553B_RTA_11,       &serv1553BTrigger::setRTA11),
on_get_int      (       MSG_TRIGGER_1553B_RTA_11,       &serv1553BTrigger::getRTA11),

on_set_int_int  (       MSG_TRIGGER_1553B_TIME,         &serv1553BTrigger::setTime),
on_get_int      (       MSG_TRIGGER_1553B_TIME,         &serv1553BTrigger::getTime),

on_set_int_int  (       MSG_TRIGGER_1553B_ERR_TYPE,     &serv1553BTrigger::setErrType),
on_get_int      (       MSG_TRIGGER_1553B_ERR_TYPE,     &serv1553BTrigger::getErrType),

on_set_int_int  (       MSG_TRIGGER_LEVELSELECT,        &serv1553BTrigger::setLevelSelect),
on_get_int      (       MSG_TRIGGER_LEVELSELECT,        &serv1553BTrigger::getLevelSelect),

on_set_int_bool (       MSG_TRIGGER_POLARITY,           &serv1553BTrigger::setPolarity),
on_get_bool     (       MSG_TRIGGER_POLARITY,           &serv1553BTrigger::getPolarity),

on_set_int_int  (       MSG_TRIGGER_CURR_BIT,           &serv1553BTrigger::setCurrBit),
on_get_int      (       MSG_TRIGGER_CURR_BIT,           &serv1553BTrigger::getCurrBit),
on_set_int_int  (       MSG_TRIGGER_CODE,               &serv1553BTrigger::setCodeValue),
on_get_int      (       MSG_TRIGGER_CODE,               &serv1553BTrigger::getCodeValue),
on_set_int_arg  (       MSG_TRIGGER_SET_CODE,           &serv1553BTrigger::setCode),
on_get_void_argo(       MSG_TRIGGER_SET_CODE,           &serv1553BTrigger::getCode),
on_set_int_int  (       MSG_TRIGGER_CODE_ALL,           &serv1553BTrigger::setCodeAll),


on_set_void_void(       CMD_SERVICE_RST,                &serv1553BTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,            &serv1553BTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &serv1553BTrigger::setActive ),
on_set_int_int  (       CMD_SERVICE_ITEM_FOCUS,         &serv1553BTrigger::setItemFocus),

//!scpi
on_set_int_ll  (        CMD_SCPI_TRIGGER_LEVEL_H,       &serv1553BTrigger::setLevelSrcA),
on_get_ll      (        CMD_SCPI_TRIGGER_LEVEL_H,       &serv1553BTrigger::getLevelSrcA),
on_set_int_ll  (        CMD_SCPI_TRIGGER_LEVEL_L,       &serv1553BTrigger::setLevelSrcB),
on_get_ll      (        CMD_SCPI_TRIGGER_LEVEL_L,       &serv1553BTrigger::getLevelSrcB),

end_of_entry()

#define M1553B_BIT_COUNT_MAX 16
#define M1553B_BIT_COUNT_MIN 4

serv1553BTrigger::serv1553BTrigger(QString name)
    : TriggerBase(name, Trigger_1553, E_SERVICE_ID_TRIG_1553B,true)
{
    serviceExecutor::baseInit();
}

DsoErr serv1553BTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

DsoErr serv1553BTrigger::setItemFocus(int id)
{
    TriggerBase::setItemFocus(id);

    if(MSG_TRIGGER_1553B_DATA_MIN == id)
    {
        async(MSG_TRIGGER_1553B_DATA_MIN, getDataMin() );
    }
    else if(MSG_TRIGGER_1553B_DATA_MAX == id)
    {
        async(MSG_TRIGGER_1553B_DATA_MAX, getDataMax() );
    }
    else if(MSG_TRIGGER_1553B_RTA == id)
    {
        async(MSG_TRIGGER_1553B_RTA, getDataMax() );
    }
    else if(MSG_TRIGGER_1553B_RTA_11 == id)
    {
        async(MSG_TRIGGER_1553B_RTA_11, getDataMax() );
    }
    else
    {}
    return ERR_NONE;
}
#define ar ar_out
#define ar_pack_e ar_out
int serv1553BTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    ar_pack_e("mCh",         cfg.mCh        );
    ar_pack_e("mPolarity",   cfg.mPolarity  );
    ar_pack_e("mWhen",       cfg.mWhen      );
    ar_pack_e("mSync",       cfg.mSync      );
    ar_pack_e("mErr",        cfg.mErr       );
    ar_pack_e("mDatCmp",     cfg.mDatCmp    );
    ar_pack_e("mRmtAddrCmp", cfg.mRmtAddrCmp);
    ar_pack_e("mSubAddrCmp", cfg.mSubAddrCmp);
    ar_pack_e("mCntCodeCmp", cfg.mCntCodeCmp);
    ar("mCmpMin",     cfg.mCmpMin    );
    ar("mCmpMax",     cfg.mCmpMax    );
    ar("mCmpMask",    cfg.mCmpMask   );

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int serv1553BTrigger::serialIn(CStream &stream, unsigned char ver)
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        ar_pack_e("mCh",         cfg.mCh        );
        ar_pack_e("mPolarity",   cfg.mPolarity  );
        ar_pack_e("mWhen",       cfg.mWhen      );
        ar_pack_e("mSync",       cfg.mSync      );
        ar_pack_e("mErr",        cfg.mErr       );
        ar_pack_e("mDatCmp",     cfg.mDatCmp    );
        ar_pack_e("mRmtAddrCmp", cfg.mRmtAddrCmp);
        ar_pack_e("mSubAddrCmp", cfg.mSubAddrCmp);
        ar_pack_e("mCntCodeCmp", cfg.mCntCodeCmp);
        ar("mCmpMin",     cfg.mCmpMin    );
        ar("mCmpMax",     cfg.mCmpMax    );
        ar("mCmpMask",    cfg.mCmpMask   );

        m_bitDataMin    = QBitArray(M1553B_BIT_COUNT_MAX);
        m_bitDataMax    = QBitArray(M1553B_BIT_COUNT_MAX);
        m_bitDataMask   = QBitArray(M1553B_BIT_COUNT_MAX);

        m_bitRTA        = QBitArray(5);
        m_bitRTA11      = QBitArray(11);
        m_bitRTAMask    = QBitArray(5);
        m_bitRTA11Mask  = QBitArray(11);

        m_pBitCurrData  = &m_bitDataMin;
        m_pBitCurrMask  = &m_bitDataMask;
    }
    else
    {
        rst();
    }
    return ERR_NONE;
}
#undef ar
#undef ar_pack_e


void serv1553BTrigger::rst()
{
    cfg.setCh( (Chan)chan1);
    cfg.setWhen( (Trigger_1553_When)trig_1553_sync);
    cfg.setSync( (Trigger_1553_Sync)trig_1553_sync_data);
    cfg.setErr( (Trigger_1553_Err)trig_1553_err_sync);
    cfg.setDatCmp(     (Trigger_value_cmp)cmp_eq);
    cfg.setRmtAddrCmp( (Trigger_value_cmp)cmp_eq);
    cfg.setSubAddrCmp( (Trigger_value_cmp)cmp_eq);
    cfg.setCntCodeCmp( (Trigger_value_cmp)cmp_eq);
    cfg.setCmpMin(  0 );
    cfg.setCmpMax(  0 );
    cfg.setCmpMask( 0xFFFFFFFF );
    cfg.setPolarity(Trigger_pulse_positive);

    m_dataCurrBit = 0;
    m_bitDataMin  = QBitArray(M1553B_BIT_COUNT_MAX);
    m_bitDataMax  = QBitArray(M1553B_BIT_COUNT_MAX);
    m_bitDataMask = QBitArray(M1553B_BIT_COUNT_MAX);
    m_bitRTA        = QBitArray(5);
    m_bitRTA11      = QBitArray(11);
    m_bitRTAMask    = QBitArray(5);
    m_bitRTA11Mask  = QBitArray(11);

    m_pBitCurrData  = &m_bitDataMin;
    m_pBitCurrMask  = &m_bitDataMask;
    TriggerBase::rst();
}

int serv1553BTrigger::startup()
{
    return  TriggerBase::startup();;
}

void serv1553BTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

DsoErr serv1553BTrigger::setSource(int ch)
{
    cfg.setCh((Chan)ch);
    TriggerBase::setSource(ch);
    return apply();
}

DsoErr serv1553BTrigger::setWhen(int w)
{
    cfg.setWhen( (Trigger_1553_When)w);
    return apply();
}

int serv1553BTrigger::getWhen()
{
    return cfg.when();
}

DsoErr serv1553BTrigger::setSyncType(int type)
{
    cfg.setSync( (Trigger_1553_Sync)type);
    return apply();
}

int serv1553BTrigger::getSyncType()
{
    return cfg.sync();
}

DsoErr serv1553BTrigger::setDataComp(int comp)
{
    cfg.setDatCmp((Trigger_value_cmp)comp);
#if 0
    int when = cfg.when();
    switch(when)
    {
    case trig_1553_status:
        cfg.setRmtAddrCmp((Trigger_value_cmp)comp);
        break;
    case trig_1553_cmd:
        cfg.setSubAddrCmp((Trigger_value_cmp)comp);
        break;
    default:
        cfg.setDatCmp((Trigger_value_cmp)comp);
        break;
    }
#endif
    return apply();
}

int serv1553BTrigger::getDataComp()
{
#if 0
    int when = cfg.when();
    switch(when)
    {
    case trig_1553_status:
        return cfg.rmtAddrCmp();
    case trig_1553_cmd:
        return cfg.subAddrCmp();
    default:
        return cfg.datCmp();
    }
#endif
    return cfg.datCmp();
}

DsoErr serv1553BTrigger::setDataMin(int bit)
{
    m_pBitCurrData  = &m_bitDataMin;
    m_pBitCurrMask  = &m_bitDataMask;
    return async(MSG_TRIGGER_CURR_BIT, bit);
}

int serv1553BTrigger::getDataMin()
{
    return getCurrBit();
}

DsoErr serv1553BTrigger::setDataMax(int bit)
{
    m_pBitCurrData = &m_bitDataMax;
    m_pBitCurrMask = &m_bitDataMask;
    return async(MSG_TRIGGER_CURR_BIT, bit);
}

int serv1553BTrigger::getDataMax()
{
    return getCurrBit();
}

qint64 serv1553BTrigger::getData()
{
    qint64 data = 0;
    CArgument arg;
    getCode(arg);
    for(int i=0; i<arg.size(); i++)
    {
        int num=0;
        arg[i].getVal(num);
        if(Trigger_Code_X == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_L == num)
        {
            data = data<<1;
        }
        else if(Trigger_Code_H == num)
        {
            data = (data<<1)+1;
        }
    }

    return data;
}

int serv1553BTrigger::setData(qint64 data)
{
    Q_ASSERT(m_pBitCurrData != NULL);
    Q_ASSERT(m_pBitCurrMask != NULL);

    qint64 maxData = 0;//qxl 2018.2.1
    qint64 tranData = 1;
    for(int i=0; i<getBitMax(); i++)
    {
        maxData = maxData|(tranData<<i);
    }

    if(maxData <= data)
    {
        trigCode::setAllValue(  *m_pBitCurrData,
                                *m_pBitCurrMask,
                                m_dataCurrBit,
                                1,
                                getBitMax() );
    }
    else
    {
        for(int i=getBitMax()-1; i>=0; i--)
        {
            int codeBit = 0;
            codeBit = data&0x01;
            trigCode::setBitValue(*m_pBitCurrData,
                                  *m_pBitCurrMask,
                                  i,
                                  codeBit,
                                  getBitMax() );
            data = data>>1;
        }
    }

    setuiChange( MSG_TRIGGER_CODE);
    return ERR_NONE;
}

DsoErr serv1553BTrigger::setRTA(int bit)
{
    m_pBitCurrData  = &m_bitRTA;
    m_pBitCurrMask  = &m_bitRTAMask;
    return async(MSG_TRIGGER_CURR_BIT, bit);
}

int serv1553BTrigger::getRTA()
{
    return getCurrBit();
}

DsoErr serv1553BTrigger::setRTA11(int bit)
{
    m_pBitCurrData = &m_bitRTA11;
    m_pBitCurrMask  = &m_bitRTA11Mask;
    return async(MSG_TRIGGER_CURR_BIT, bit);
}

int serv1553BTrigger::getRTA11()
{
    return getCurrBit();
}

DsoErr serv1553BTrigger::setTime(int time)
{
    cfg.setCmpMin(  time );
    return apply();
}

int serv1553BTrigger::getTime()
{
    return cfg.cmpMin();
}

DsoErr serv1553BTrigger::setErrType(int type)
{
    cfg.setErr((Trigger_1553_Err)type);
    return apply();
}

int serv1553BTrigger::getErrType()
{
    return cfg.err();
}

bool serv1553BTrigger::getPolarity()
{
    return (bool)cfg.polarity();
}

DsoErr serv1553BTrigger::setPolarity(bool polarity)
{
    cfg.setPolarity((TriggerPulsePolarity)polarity);
    return apply();
}

DsoErr serv1553BTrigger::setCurrBit(int bit)
{
    //!按下 切换当前位的值
    if(TRIG_CURRBIT_Z_VALUE == bit)
    {
        int    curValue = getCodeValue();
        bool   binHex = true;

        if( m_dataCurrBit >= getBitMax() )
        {
            binHex = false;
        }
        int stop = (getBitMax()%4)?((1<<getBitMax()%4)-1):(0xf); //! 0x00-0xf
        stop = (m_dataCurrBit == getBitMax())?stop:(0xf);
        return setCodeValue( trigCode::get_z_value(curValue,binHex, stop) );
    }
    else //!旋转 切换当前位置
    {
        int b = bit  + m_dataCurrBit;

        b = qMax(b, 0);
        b = qMin(b, getBitByteMax()-1 );

        m_dataCurrBit = b;
        return ERR_NONE;
    }

    return ERR_NONE;
}

int serv1553BTrigger::getCurrBit()
{
    setuiZVal( TRIG_CURRBIT_Z_VALUE );
    return m_dataCurrBit;
}

int serv1553BTrigger::getBitMax()
{
    return m_pBitCurrData->count();
}

int serv1553BTrigger::getBitByteMax()
{
    return getBitMax() + (getBitMax()+3)/4;
}

DsoErr serv1553BTrigger::setCodeValue(int value)
{
    Q_ASSERT(m_pBitCurrData != NULL);
    Q_ASSERT(m_pBitCurrMask != NULL);
    trigCode::setBitValue(*m_pBitCurrData,
                          *m_pBitCurrMask,
                          m_dataCurrBit,
                          value,
                          getBitMax() );

    quint32 min = 0, max = 0, mask = 0;

    for(int i = 0; i<m_bitDataMin.count(); i++)
    {
        min  |= (m_bitDataMin.at(i) ? (1<<i) : 0 );
        max  |= (m_bitDataMax.at(i) ? (1<<i) : 0 );
        mask |= (m_bitDataMask.at(i)? (1<<i) : 0 );
    }
    qDebug()<<__FILE__<<__LINE__<<min<<max<<mask;

    cfg.setCmpMin( min  );
    cfg.setCmpMax( max  );
    cfg.setCmpMask(mask );

    quint32 rta = 0, rta11 = 0, rtaMask = 0, rtaMask11 = 0;
    for(int i = 0; i<m_bitRTA.count(); i++)
    {
        rta      |= (m_bitRTA.at(i) ? (1<<i) : 0 );
        rtaMask  |= (m_bitRTAMask.at(i)? (1<<i) : 0 );
    }

    for(int i = 0; i<m_bitRTA11.count(); i++)
    {
        rta11     |= (m_bitRTA11.at(i) ? (1<<i) : 0 );
        rtaMask11 |= (m_bitRTA11Mask.at(i)? (1<<i) : 0 );
    }

    quint32 csData = ((rta&0x1F)<<11)+(rta11&0X7FF);
    quint32 csMask = ((rtaMask&0x1F)<<11)+(rtaMask11&0X7FF);
    cfg.setCsData(csData);
    cfg.setCsMask(csMask);

    return apply();
}

int serv1553BTrigger::getCodeValue()
{
    Q_ASSERT(m_pBitCurrData != NULL);
    Q_ASSERT(m_pBitCurrMask != NULL);

    int value = trigCode::getBitValue(*m_pBitCurrData,
                                      *m_pBitCurrMask,
                                      m_dataCurrBit,
                                      getBitMax() );
    return value;
}

DsoErr serv1553BTrigger::setCode(CArgument &/*arg*/)
{
    return ERR_NONE;
}

void serv1553BTrigger::getCode(CArgument &arg)
{
    Q_ASSERT(m_pBitCurrData != NULL);
    Q_ASSERT(m_pBitCurrMask != NULL);

    arg.clear();
    for(int i = getBitMax()-1; i >= 0 ; i--)
    {
        int value = Trigger_Code_X;
        if(m_pBitCurrMask->at(i))
        {
            value = m_pBitCurrData->at(i );
        }
        else
        {
            value = Trigger_Code_X;
        }
        arg.append(value);
    }
}

DsoErr serv1553BTrigger::setCodeAll(int value)
{
    Q_ASSERT(m_pBitCurrData != NULL);
    Q_ASSERT(m_pBitCurrMask != NULL);

    trigCode::setAllValue(  *m_pBitCurrData,
                            *m_pBitCurrMask,
                            m_dataCurrBit,
                            value,
                            getBitMax() );
    return apply();
}

int serv1553BTrigger::setLevelSrcA(qint64 l)
{
    setLevelSelect(Trigger_Level_High);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );

}

qint64 serv1553BTrigger::getLevelSrcA()
{
    qint64 l = 0;
    setLevelSelect(Trigger_Level_High);
    serviceExecutor::query( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return l;
}

int serv1553BTrigger::setLevelSrcB(qint64 l)
{
    setLevelSelect(Trigger_Level_Low);
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
}

qint64 serv1553BTrigger::getLevelSrcB()
{
    qint64 l = 0;
    setLevelSelect(Trigger_Level_Low);
    serviceExecutor::query( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, l );
    return l;
}
