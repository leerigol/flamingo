#include "servtrig.h"
#include "trigcode.h"

IMPLEMENT_POST_DO( TriggerBase, servCanTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,           &servCanTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,        &servCanTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servCanTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,           &servCanTrigger::setSource),

on_set_int_int  (       MSG_TRIGGER_CAN_WHEN,            &servCanTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_CAN_WHEN,            &servCanTrigger::getWhen),

on_set_int_bool (       MSG_TRIGGER_CAN_ID_EXTENDED,     &servCanTrigger::setExtendedId),
on_get_bool     (       MSG_TRIGGER_CAN_ID_EXTENDED,     &servCanTrigger::getExtendedId),

on_set_int_int  (       MSG_TRIGGER_CURR_BIT,            &servCanTrigger::setCurrBit),
on_get_int      (       MSG_TRIGGER_CURR_BIT,            &servCanTrigger::getCurrBit),

on_set_int_int  (       MSG_TRIGGER_CAN_DATA_BYTE,       &servCanTrigger::setByteCount),
on_get_int      (       MSG_TRIGGER_CAN_DATA_BYTE,       &servCanTrigger::getByteCount),

on_set_int_bool (       MSG_TRIGGER_CAN_DEFINE,          &servCanTrigger::setDefine),
on_get_bool     (       MSG_TRIGGER_CAN_DEFINE,          &servCanTrigger::getDefine),

on_set_int_bool (       MSG_TRIGGER_CAN_ID_FILTER,       &servCanTrigger::setFilterId),
on_get_bool     (       MSG_TRIGGER_CAN_ID_FILTER,        &servCanTrigger::getFilterId),

on_set_int_int  (       MSG_TRIGGER_CAN_SINGNAL,         &servCanTrigger::setSignal),
on_get_int      (       MSG_TRIGGER_CAN_SINGNAL,         &servCanTrigger::getSignal),

on_set_int_int  (       MSG_TRIGGER_CAN_BAUD,            &servCanTrigger::setBaudRate),
on_get_int      (       MSG_TRIGGER_CAN_BAUD,            &servCanTrigger::getBaudRate),

on_set_int_int  (       MSG_TRIGGER_CAN_SAMPLE_POINT,    &servCanTrigger::setSamplePoint),
on_get_int      (       MSG_TRIGGER_CAN_SAMPLE_POINT,    &servCanTrigger::getSamplePoint),

on_set_int_bool (       MSG_TRIGGER_CAN_STANDARD,        &servCanTrigger::setStandard),
on_get_bool     (       MSG_TRIGGER_CAN_STANDARD,        &servCanTrigger::getStandard),


on_set_int_int  (       MSG_TRIGGER_CODE,                &servCanTrigger::setCodeValue),
on_get_int      (       MSG_TRIGGER_CODE,                &servCanTrigger::getCodeValue),
on_set_int_arg  (       MSG_TRIGGER_SET_CODE,            &servCanTrigger::setCode),
on_get_void_argo(       MSG_TRIGGER_SET_CODE,            &servCanTrigger::getCode),
on_set_int_int  (       MSG_TRIGGER_CODE_ALL,            &servCanTrigger::setCodeAll),

on_set_void_void(       CMD_SERVICE_RST,                 &servCanTrigger::rst ),
on_set_int_void  (      CMD_SERVICE_STARTUP,             &servCanTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,              &servCanTrigger::setActive ),

end_of_entry()

servCanTrigger::servCanTrigger(QString name)
    : TriggerBase(name, Trigger_CAN, E_SERVICE_ID_TRIG_CAN)
{
    serviceExecutor::baseInit();
}

DsoErr servCanTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

#define ar ar_out
#define ar_pack_e ar_out
int servCanTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);
    ar("m_when",          m_when         );
    ar("m_currBit",       m_currBit      );
    ar("m_dataByteCount", m_dataByteCount);
    ar("m_define",        m_define       );
    ar("m_standard",      m_standard     );

    ar_pack_e("mCh",     cfg.mCh           );
    ar("mBaud",          cfg.mBaud         );
    ar_pack_e("mPhy",    cfg.mPhy          );
    ar_pack_e("mSpec",   cfg.mSpec         );
    ar_pack_e("mWhen",   cfg.mWhen         );
    ar_pack_e("mField",  cfg.mField        );
    ar_pack_e("mFrame",  cfg.mFrame        );
    ar_pack_e("mErr",    cfg.mErr          );
    ar_pack_e("mIdCmp",  cfg.mIdCmp        );
    ar("mIdCmpMask",     cfg.mIdCmpMask    );
    ar_pack_e("mDatCmp", cfg.mDatCmp       );
    ar("mDatCmpMask",    cfg.mDatCmpMask   );
    ar("mSaPos",         cfg.mSaPos        );
    ar("mIdMin",         cfg.mIdMin        );
    ar("mIdMax",         cfg.mIdMax        );
    ar("mIdMask",        cfg.mIdMask       );

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servCanTrigger::serialIn(CStream &stream, unsigned char ver)
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        ar("m_when",          m_when         );
        ar("m_currBit",       m_currBit      );
        ar("m_dataByteCount", m_dataByteCount);
        ar("m_define",        m_define       );
        ar("m_standard",      m_standard     );

        ar_pack_e("mCh",     cfg.mCh           );
        ar("mBaud",          cfg.mBaud         );
        ar_pack_e("mPhy",    cfg.mPhy          );
        ar_pack_e("mSpec",   cfg.mSpec         );
        ar_pack_e("mWhen",   cfg.mWhen         );
        ar_pack_e("mField",  cfg.mField        );
        ar_pack_e("mFrame",  cfg.mFrame        );
        ar_pack_e("mErr",    cfg.mErr          );
        ar_pack_e("mIdCmp",  cfg.mIdCmp        );
        ar("mIdCmpMask",     cfg.mIdCmpMask    );
        ar_pack_e("mDatCmp", cfg.mDatCmp       );
        ar("mDatCmpMask",    cfg.mDatCmpMask   );
        ar("mSaPos",         cfg.mSaPos        );
        ar("mIdMin",         cfg.mIdMin        );
        ar("mIdMax",         cfg.mIdMax        );
        ar("mIdMask",        cfg.mIdMask       );

        cfg.setDatMin(  QByteArray(8,0));
        cfg.setDatMax(  QByteArray(8,0));
        cfg.setDatMask( QByteArray(8,0));

        m_bitIdMin  = QBitArray(32);
        m_bitIdMax  = QBitArray(32);
        m_bitIdMask = QBitArray(32);

        cfg.setByteCount(m_dataByteCount);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}
#undef ar
#undef ar_pack_e


void servCanTrigger::rst()
{  
    m_when          = 0;
    m_currBit   = 0;
    m_dataByteCount = 1;
    m_define        = 0;
    m_standard      = 0;

    cfg.setCh(chan1);
    cfg.setBaud(1000000);
    cfg.setPhy(trig_can_h);
    cfg.setSpec(trig_can_norm);
    cfg.setWhen(trig_can_field);
    cfg.setField(trig_can_filed_start);
    cfg.setFrame(trig_can_frame_data);
    cfg.setErr(trig_can_err_bitfilled);
    cfg.setIdCmp(cmp_eq);
    cfg.setIdCmpMask(false);
    cfg.setDatCmp(cmp_eq);
    cfg.setDatCmpMask(false);
    cfg.setSaPos(50);
    cfg.setIdMin(0);
    cfg.setIdMax(0);
    cfg.setIdMask(0);

    cfg.setByteCount(m_dataByteCount);
    cfg.setDatMin(  QByteArray(8,0));
    cfg.setDatMax(  QByteArray(8,0));
    cfg.setDatMask( QByteArray(8,0));

    m_bitIdMin  = QBitArray(32);
    m_bitIdMax  = QBitArray(32);
    m_bitIdMask = QBitArray(32);
    TriggerBase::rst();
}

int servCanTrigger::startup()
{
    return  TriggerBase::startup();
}

void servCanTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

DsoErr servCanTrigger::setSource(int ch)
{
    cfg.setCh((Chan)ch);
    TriggerBase::setSource(ch);
    return apply();
}

struct when_can_type
{
    serv_can_when    serv_when;
    Trigger_Can_When can_when;
    int              type;
    bool             idCmpMask;
    bool             dataCmpMask;
};
static when_can_type _when_can_type[]=
{
    { can_when_sof,          trig_can_field,   trig_can_filed_start   ,false, false},
    { can_when_eof,          trig_can_field,   trig_can_field_end     ,true,  false},

    { can_when_remote_id,    trig_can_frame,   trig_can_frame_rmt     ,true,  false},
    { can_when_frame_id,     trig_can_frame,   trig_can_frame_data    ,true,  false},
    { can_when_frame_data,   trig_can_frame,   trig_can_frame_data    ,false, true},
    { can_when_frame_data_id,trig_can_frame,   trig_can_frame_data    ,true,  true},
    { can_when_frame_error,  trig_can_frame,   trig_can_frame_err     ,false, false},
    { can_when_overload,     trig_can_frame,   trig_can_frame_overload,false, false},

    { can_when_bit_error,    trig_can_err,    trig_can_err_bitfilled  ,false, false},
    { can_when_answer_error, trig_can_err,    trig_can_err_ack        ,false, false},
    { can_when_check_error,  trig_can_err,    trig_can_err_crc        ,false, false},
    { can_when_format_error, trig_can_err,    trig_can_err_form       ,false, false},
    { can_when_random_error, trig_can_err,    trig_can_err_any        ,false, false},
};
DsoErr servCanTrigger::setWhen(int w)
{
    for(int i = 0; i < array_count(_when_can_type); i++)
    {
        if(w == _when_can_type[i].serv_when)
        {
            Trigger_Can_When cw = _when_can_type[i].can_when;

            switch (cw) {
            case trig_can_field:
                cfg.setField((Trigger_Can_Field)_when_can_type[i].type);
                break;
            case trig_can_frame:
                cfg.setFrame((Trigger_Can_Frame)_when_can_type[i].type);
                break;
            case trig_can_err:
                cfg.setErr((Trigger_Can_Err)_when_can_type[i].type);
                break;
            default:
                return ERR_INVALID_INPUT;
            }
            m_when = w;
            cfg.setWhen(cw);
            cfg.setDatCmpMask(_when_can_type[i].dataCmpMask);
            cfg.setIdCmpMask(_when_can_type[i].idCmpMask);

            m_currBit = qMin(m_currBit, getBitByteMax()-1);
            return apply();
        }
    }
    return ERR_INVALID_INPUT;
}

int servCanTrigger::getWhen()
{
    return m_when;
}

DsoErr servCanTrigger::setExtendedId(bool b)
{
    if(b)
    {
        m_bitIdMin.fill( false, m_idExtendCount,32);
        m_bitIdMax.fill( false, m_idExtendCount,32);
        m_bitIdMask.fill(false, m_idExtendCount,32);
    }
    else
    {
        m_bitIdMin.fill( false, m_idNormCount,32);
        m_bitIdMax.fill( false, m_idNormCount,32);
        m_bitIdMask.fill(false, m_idNormCount,32);

    }

    cfg.setSpec((Trigger_Can_Spec)b);
    m_currBit = qMin(m_currBit, getBitByteMax()-1);
    return apply();
}

bool servCanTrigger::getExtendedId()
{
    return (bool)cfg.spec();
}

DsoErr servCanTrigger::setId(int id)
{
    cfg.setIdMin(id);
    return apply();
}

int servCanTrigger::getId(void)
{
    return cfg.idMin();
}


DsoErr servCanTrigger::setCurrBit(int bit)
{
    DsoErr err = ERR_NONE;
    //!按下 切换当前位的值
    if(TRIG_CURRBIT_Z_VALUE == bit)
    {
        int    curValue = getCodeValue();
        bool   binHex = true;

        if( m_currBit >= getBitMax() )
        {
            binHex = false;
        }

        int stop = (getBitMax()%4)?((1<<getBitMax()%4)-1):(0xf); //! 0x00-0xf
        stop = (m_currBit == getBitMax())?stop:(0xf);

        err = setCodeValue(trigCode::get_z_value(curValue, binHex, stop));
    }
    else //!旋转 切换当前位置
    {
        int b = bit  + m_currBit;

        m_currBit = qMax(0,b);
        m_currBit = qMin(m_currBit, getBitByteMax()-1 );
    }
    return err;
}

int servCanTrigger::getCurrBit()
{
    setuiZVal( TRIG_CURRBIT_Z_VALUE );
    return m_currBit;
}

/*!
 * \brief servCanTrigger::setByteCount 位序( 0-7_8-15_16-23_24-32_....)
 * \param byt
 * \return
 */
DsoErr servCanTrigger::setByteCount(int byt)
{
    cfg.setDatMin(  cfg.datMin().left(byt).leftJustified(8,0) );
    cfg.setDatMax(  cfg.datMax().left(byt).leftJustified(8,0) );
    cfg.setDatMask( cfg.datMask().left(byt).leftJustified(8,0));
    m_dataByteCount = byt;

    m_currBit = qMin(m_currBit, getBitByteMax()-1);

    cfg.setByteCount(m_dataByteCount);

    return apply();
}

int servCanTrigger::getByteCount()
{
    setuiRange( 1, (int)8, 1 );
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return m_dataByteCount;
}


DsoErr servCanTrigger::setDefine(bool d)
{
    m_define = d;
    m_currBit = qMin(m_currBit, getBitByteMax()-1);
    return apply();
}

bool servCanTrigger::getDefine()
{
    return m_define;
}

DsoErr servCanTrigger::setFilterId(bool b)
{
    cfg.setIdCmpMask(b);
    return apply();
}

bool servCanTrigger::getFilterId()
{
    return cfg.idCmpMask();
}

DsoErr servCanTrigger::setSignal(int d)
{
    cfg.setPhy((Trigger_Can_Phy)d);
    return apply();
}

int servCanTrigger::getSignal()
{
    return cfg.phy();
}

DsoErr servCanTrigger::setBaudRate(int baud)
{
    cfg.setBaud(baud);
    return apply();
}

int servCanTrigger::getBaudRate()
{
    setuiRange( (int)10E3, (int)5E6, 1000000 );
    setuiStep( getNumStep(cfg.baud()) );
    setuiPostStr("bps");
    return cfg.baud();
}

DsoErr servCanTrigger::setSamplePoint(int point)
{
    cfg.setSaPos(point);
    return apply();
}

int servCanTrigger::getSamplePoint()
{
    setuiRange( 10, 90, 50);
    setuiPostStr("%");
    return cfg.saPos();
}

DsoErr servCanTrigger::setStandard(bool b)
{
    m_standard = b;
    return apply();
}

int servCanTrigger::getStandard()
{
    return m_standard;
}

bool servCanTrigger::getSelectIdData()
{
    if( cfg.datCmpMask() && cfg.idCmpMask() )
    {
        return m_define;
    }
    else if(cfg.idCmpMask())
    {
        return true;
    }
    else
    {
        return false;
    }
}

int servCanTrigger::getBitMax()
{
    if(getSelectIdData())
    {
        return (cfg.spec() == trig_can_norm)? m_idNormCount:m_idExtendCount;
    }
    else
    {
        return m_dataByteCount*8;
    }
}

int servCanTrigger::getBitByteMax()
{
    if(getSelectIdData())
    {
        int count = (cfg.spec() == trig_can_norm)? m_idNormCount : m_idExtendCount;
        return (count+(count+3)/4);
    }
    else
    {
        return (m_dataByteCount)*(8+2);
    }
}

DsoErr servCanTrigger::setCodeValue(int value)
{
    int curBit = m_currBit;
    if(getSelectIdData())
    {
        //!set id
        trigCode::setBitValue(m_bitIdMin,
                              m_bitIdMask,
                              curBit,
                              value,
                              getBitMax());

        quint32 min = 0, max = 0, mask = 0;
        for(int i = 0; i<m_bitIdMin.count(); i++)
        {
            min  |= (m_bitIdMin.at(i) ? (1<<i) : 0 );
            max  |= (m_bitIdMax.at(i) ? (1<<i) : 0 );
            mask |= (m_bitIdMask.at(i)? (1<<i) : 0 );
        }
        cfg.setIdMin( min );
        cfg.setIdMax( max );
        cfg.setIdMask(mask);
    }
    else
    {
        //set data
        trigCode::setBitValue(cfg.mDatMin,
                              cfg.mDatMask,
                              curBit,
                              value,
                              m_dataByteCount);
    }

    return apply();
}

int servCanTrigger::getCodeValue()
{
    int curBit = m_currBit;
    int value = Trigger_Code_X;
    if(getSelectIdData())
    {
        value = trigCode::getBitValue(m_bitIdMin,
                                      m_bitIdMask,
                                      curBit,
                                      getBitMax());
    }
    else
    {
        value = trigCode::getBitValue(cfg.mDatMin,
                                      cfg.mDatMask,
                                      curBit,
                                      m_dataByteCount);
    }
    qDebug()<<__FUNCTION__<<__LINE__<<value;
    return value;
}

DsoErr servCanTrigger::setCode(CArgument &arg)
{
    qDebug()<<__FUNCTION__<<__LINE__<<cfg.datMin()<<arg.size();
    return ERR_NONE;
}

void servCanTrigger::getCode(CArgument &arg)
{
    arg.clear();
    for(int i = 0; i < getBitMax(); i++)
    {
        int value = Trigger_Code_X;
        if(getSelectIdData())
        {
            value = trigCode::getBitValue(m_bitIdMin,
                                          m_bitIdMask,
                                          i,
                                          getBitMax());
        }
        else
        {
            value = trigCode::getBitValue(cfg.mDatMin,
                                          cfg.mDatMask,
                                          i,
                                          m_dataByteCount);
        }
        arg.append(value);
    }
}


DsoErr servCanTrigger::setCodeAll(int value)
{
    //! 1:id 0:data
    if(getSelectIdData())
    {
        //!set id
        trigCode::setAllValue(m_bitIdMin,
                              m_bitIdMask,
                              m_currBit,
                              value,
                              getBitMax());

        quint32 min = 0, max = 0, mask = 0;
        for(int i = 0; i<m_bitIdMin.count(); i++)
        {
            min  |= (m_bitIdMin.at(i) ? (1<<i) : 0 );
            max  |= (m_bitIdMax.at(i) ? (1<<i) : 0 );
            mask |= (m_bitIdMask.at(i)? (1<<i) : 0 );
        }
        cfg.setIdMin( min );
        cfg.setIdMax( max );
        cfg.setIdMask(mask);
    }
    else
    {
        //set data
        trigCode::setAllValue(cfg.mDatMin,
                              cfg.mDatMask,
                              m_currBit,
                              value,
                              m_dataByteCount);
    }

    return apply();
}
