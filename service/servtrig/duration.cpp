
#include "servtrig.h"
IMPLEMENT_POST_DO( TriggerBase, servDurationTrigger)
start_of_postdo()
on_postdo( CMD_SERVICE_ACTIVE,          &servDurationTrigger::applyLevel),
on_postdo( MSG_TRIGGER_SOURCE_LA,       &servDurationTrigger::applyLevel),
end_of_postdo()

IMPLEMENT_CMD( TriggerBase, servDurationTrigger )
start_of_entry()

on_set_int_int  (       MSG_TRIGGER_SOURCE_LA,          &servDurationTrigger::setSource),

on_set_int_arg  (       MSG_TRIGGER_SET_CODE,           &servDurationTrigger::setCode),
on_get_void_argo(       MSG_TRIGGER_SET_CODE,           &servDurationTrigger::getCode),

on_set_int_void (       MSG_TRIGGER_CODE_ALL,           &servDurationTrigger::setAll),

on_set_int_int  (       MSG_TRIGGER_CODE,               &servDurationTrigger::setValue),
on_get_int      (       MSG_TRIGGER_CODE,               &servDurationTrigger::getValue),

on_set_int_int  (       MSG_TRIGGER_DURATION_WHEN,      &servDurationTrigger::setWhen),
on_get_int      (       MSG_TRIGGER_DURATION_WHEN,      &servDurationTrigger::getWhen),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_UPPER,        &servDurationTrigger::setUpper),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_UPPER,        &servDurationTrigger::getUpper, &servDurationTrigger::getUpperAttr),

on_set_int_ll   (       MSG_TRIGGER_LIMIT_LOWER,        &servDurationTrigger::setLower),
on_get_ll_attr  (       MSG_TRIGGER_LIMIT_LOWER,        &servDurationTrigger::getLower, &servDurationTrigger::getLowerAttr),

//!scpi
on_set_int_arg  (      CMD_SCPI_TRIGGER_LEVEL,          &servDurationTrigger::setSourceLevel),
on_get_void_argi_argo( CMD_SCPI_TRIGGER_LEVEL,          &servDurationTrigger::getSourceLevel),

on_get_string(       CMD_SCPI_DURATION_GET_CODE,           &servDurationTrigger::getScpiCode),

on_set_void_void(       CMD_SERVICE_RST,                &servDurationTrigger::rst ),
on_set_int_void (       CMD_SERVICE_STARTUP,            &servDurationTrigger::startup ),
on_set_int_int  (       CMD_SERVICE_ACTIVE,             &servDurationTrigger::setActive ),
end_of_entry()


servDurationTrigger::servDurationTrigger(QString name)
    : TriggerBase(name,Trigger_Duration, E_SERVICE_ID_TRIG_Duration)
{
    serviceExecutor::baseInit();
    linkChange( MSG_TRIGGER_DURATION_WHEN,  MSG_TRIGGER_LIMIT_LOWER, MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_LOWER,    MSG_TRIGGER_LIMIT_UPPER);
    linkChange( MSG_TRIGGER_LIMIT_UPPER,    MSG_TRIGGER_LIMIT_LOWER);

    mUiAttr.setVisible(MSG_TRIGGER_CODE, Trigger_pat_fall, false);
    mUiAttr.setVisible(MSG_TRIGGER_CODE, Trigger_pat_rise, false);
}

DsoErr servDurationTrigger::setActive(int active)
{
    return TriggerBase::setActive(active);
}

int servDurationTrigger::getChSlope(Chan ch)
{
    int i = ch - chan1;
    Q_ASSERT( (i >= 0) && (i < array_count(cfg.mPatterns)));
    switch (cfg.mPatterns[i])
    {
    case Trigger_pat_h:
    case Trigger_pat_rise:
        return Trigger_Edge_Rising;

    case Trigger_pat_l:
    case Trigger_pat_fall:
        return Trigger_Edge_Falling;

    default:
        return TriggerBase::getChSlope(ch);
    }
}

DsoErr servDurationTrigger::applyLevel()
{
    DsoErr err = ERR_NONE;
    for(int ch = chan_none; ch < all_pattern_src; ch++)
    {
        if( cfg.mPatterns[ch] != Trigger_pat_x)
        {
            err = applyLevelEngine( (Chan)(ch+chan1) );
            if(err != ERR_NONE ) return err;
        }
    }
    return err;
}

void servDurationTrigger::getTimeRangeValue(qint64 &tL, qint64 &tH, EMoreThan &w)
{
    tH = cfg.widthH();
    tL = cfg.widthL();
    w  = cfg.cmp();
}

DsoErr servDurationTrigger::setTimeRangeValue(qint64 &tL, qint64 &tH)
{
    cfg.setWidthH(tH);
    cfg.setWidthL(tL);
    return apply();
}

int servDurationTrigger::serialOut(CStream &stream, unsigned char &ver)
{
    TriggerBase::serialOut(stream, ver);

    for(int i=0; i< cfg.mCodeCount; i++)
    {
        stream.write(QString("mPatterns_%1").arg(i), (int)cfg.mPatterns[i]);
    }
    stream.write(QStringLiteral("mOperator"), cfg.mOperator );
    stream.write(QStringLiteral("mCh"      ), cfg.ch()      );
    stream.write(QStringLiteral("mPolariy" ), cfg.polariy() );
    stream.write(QStringLiteral("mCmp"     ), cfg.cmp()     );
    stream.write(QStringLiteral("mWidthH"  ), cfg.widthH()  );
    stream.write(QStringLiteral("mWidthL"  ), cfg.widthL()  );

    return 0;
}

int servDurationTrigger::serialIn( CStream &stream, unsigned char ver )
{
    TriggerBase::serialIn(stream, ver);

    if(ver == mVersion)
    {
        int value;
        for(int i=0; i< cfg.mCodeCount; i++)
        {
            stream.read(QString("mPatterns_%1").arg(i), value);
            cfg.mPatterns[i] = (TriggerPattern)value;
        }

        stream.read(QStringLiteral("mOperator"), value);
        cfg.mOperator = (TriggerLogicOperator)value;

        stream.read(QStringLiteral("mCh"      ), value       );
        cfg.setCh((Chan)value);

        stream.read(QStringLiteral("mPolariy" ), value       );
        cfg.setPolariy((TriggerPulsePolarity)value);

        stream.read(QStringLiteral("mCmp"     ), value       );
        cfg.setCmp((EMoreThan)value);

        stream.read(QStringLiteral("mWidthH"  ), cfg.mWidthH  );
        stream.read(QStringLiteral("mWidthL"  ), cfg.mWidthL  );
    }
    else
    {
        rst();
    }
    return 0;
}
void servDurationTrigger::rst()
{
    for(int i=0; i< cfg.mCodeCount; i++)
    {
        cfg.mPatterns[i] =  Trigger_pat_x ;
    }

    cfg.mOperator = Trigger_logic_and ;
    cfg.setCh(chan1                  );
    cfg.setPolariy(Trigger_pulse_any );
    cfg.setCmp(Trigger_When_Morethan );
    cfg.setWidthH( (qint64)G_s64UpperDefault  );
    cfg.setWidthL( (qint64)G_s64LowerDefault  );
    TriggerBase::rst();
}

int servDurationTrigger::startup()
{
    mUiAttr.setVisible(MSG_TRIGGER_SET_CODE,3, false);
    mUiAttr.setVisible(MSG_TRIGGER_SET_CODE,4, false);
    return  TriggerBase::startup();
}

void servDurationTrigger::registerSpy()
{
    TriggerBase::registerSpy();
}

int servDurationTrigger::setSource(int src)
{
    if( !(src > chan_none && src < all_trig_src) )
    {
        return ERR_INVALID_INPUT;
    }
    else
    {
        TriggerBase::setSource(src);
        async(MSG_TRIGGER_CODE, getValue());
    }
    return ERR_NONE;
}

/**
 * @brief servDurationTrigger::setCode
 * @param arg
 * @return
 */
int servDurationTrigger::setCode(CArgument &arg)
{
    int size = ( arg.size() <=  cfg.mCodeCount)? arg.size() : cfg.mCodeCount ;

    TrigPatternCfg tempCfg = cfg;

    int val = 0;
    for(int i=0; i<size; i++)
    {
        arg.getVal(val, i);

        tempCfg.mPatterns[i] = (TriggerPattern)val;
    }

    if(tempCfg.checkCode())
    {
        for(int i=0; i<size; i++)
        {
            arg.getVal(val, i);

            cfg.mPatterns[i] = (TriggerPattern)val;
        }
        return apply();
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
}

/**
 * @brief servDurationTrigger::getCode
 * @param arg
 * @return
 */
void servDurationTrigger::getCode(CArgument &arg)
{
    for(int i=0; i<cfg.mCodeCount; i++)
    {
        arg.append( (int)cfg.mPatterns[i] );
    }
}

/**
 * @brief servPatternTrigger::getScpiCode
 * @return
 */
QString servDurationTrigger::getScpiCode()
{
    QString pattString = "";
    int     tempCount  = 4;
    bool  isHasLa = sysCheckLicense(OPT_MSO);
    if(isHasLa)
    {
        tempCount = cfg.mCodeCount-1;
    }
    for(int i=0; i<tempCount; i++)
    {
        switch ( (int)cfg.mPatterns[i] )
        {
        case Trigger_pat_h:
            pattString.append("H,");
            break;

        case Trigger_pat_l:
            pattString.append("L,");
            break;

        case Trigger_pat_x:
            pattString.append("X,");
            break;

        case Trigger_pat_rise:
            pattString.append("R,");
            break;

        case Trigger_pat_fall:
            pattString.append("F,");
            break;

        default:
            break;
        }
    }
    pattString.chop(1);
    return pattString;
}

/**
 * @brief servDurationTrigger::getValue
 * @return
 */
int servDurationTrigger::getValue(void)
{
    int src = m_pCurrSource->getChan() - 1;
    Q_ASSERT(src < array_count(cfg.mPatterns));

    return cfg.mPatterns[src];
}

int servDurationTrigger::setAll( void )
{
    int code = getValue();

    if(Trigger_pat_x < code)
    {
        return ERR_INVALID_INPUT;
    }

    for(int i=0; i<cfg.mCodeCount; i++)
    {
        cfg.mPatterns[i] = (TriggerPattern)code;
    }
    return apply();
}

DsoErr servDurationTrigger::setSourceLevel(CArgument &arg)
{
    Q_ASSERT( arg.size() >= 1 );

    if(arg.size() >= 2 )
    {
        setSource(arg[0].iVal);
    }
    return serviceExecutor::post( serv_name_trigger, CMD_SCPI_TRIGGER_LEVEL, arg[1].llVal);

}

void servDurationTrigger::getSourceLevel(CArgument &argi, CArgument &argo)
{
    int ch;
    qint64 level = 0;

    if(argi.size() == 0)
    {
        level = getLevel();
        ch    = m_pCurrSource->getChan();
    }
    else if( ERR_NONE == argi.getVal(ch) )
    {
        level = getSourceObj(ch)->getLevel(0);
    }
    else
    {
        qDebug()<<__FILE__<<__LINE__<<"input source  invalid";
    }
    argo.setVal((qint64)(level*getProbeRatio((Chan)ch)));
}

/**
 * @brief servDurationTrigger::setValue
 * @param s
 * @return
 */
int servDurationTrigger::setValue(int s )
{
    int src = m_pCurrSource->getChan() - 1;
    Q_ASSERT(src < array_count(cfg.mPatterns));

    cfg.mPatterns[src] = (TriggerPattern)s;

    if(cfg.checkCode())
    {
        return apply();
    }
    else
    {
        cfg.mPatterns[src] = Trigger_pat_x;
        return ERR_INVALID_INPUT;
    }
}

/**
 * @brief servDurationTrigger::getWhen
 * @return int
 */
int servDurationTrigger::getWhen(void)
{
    return cfg.cmp();
}

/**
 * @brief servDurationTrigger::setWhen
 * @param s
 */
int servDurationTrigger::setWhen(int w )
{
    EMoreThan w0 = (EMoreThan)getWhen();

    cfg.setCmp( (EMoreThan)w );

    return checkTimeRange(T_Rule_None,w0);
}

/**
 * @brief servDurationTrigger::getLower
 * @return
 */
qint64 servDurationTrigger::getLower(void)
{
    return cfg.widthL();
}

/**
 * @brief servDurationTrigger::setLower
 * @param t
 * @return
 */
int servDurationTrigger::setLower(qint64 t)
{
    cfg.setWidthL(t);
    return checkTimeRange(T_Rule_L);;
}

/**
 * @brief servDurationTrigger::getUpper
 * @return
 */
qint64 servDurationTrigger::getUpper(void)
{
    return cfg.widthH();
}

/**
 * @brief servDurationTrigger::setUpper
 * @param t
 * @return
 */
int servDurationTrigger::setUpper(qint64 t)
{
    cfg.setWidthH(t);
    return checkTimeRange(T_Rule_H);;
}


