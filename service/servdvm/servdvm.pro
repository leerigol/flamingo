QT       += core
QT       += gui
QT       += widgets
QT       += network

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/services/servdvm
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

#DEFINES += _DEBUG

# Input
HEADERS += \
    servdvm.h \
    dvmdata.h

SOURCES += \
    servdvm.cpp \
    dvmdata.cpp
