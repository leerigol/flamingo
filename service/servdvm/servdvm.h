#ifndef SERVDVM_H
#define SERVDVM_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "../../include/dsotype.h"
#include "dvmdata.h"

//! DVM Gate Time
#define GATE107000  107000   //! 107ms
#define GATE3430000 3430000  //! 3.43s
#define GATE850000  850000   //! 0.85s
#define GATE13400   13400    //! 13.4ms

enum DvmLimitType
{
    IN_LIMIT,
    OUT_LIMIT,
};

class servDvm : public serviceExecutor,
                public ISerial
{
    Q_OBJECT

protected:
    DECLARE_CMD()

public:
    servDvm( QString name, ServiceId eId = E_SERVICE_ID_DVM );

    enum
    {
        cmd_base = 0,
        cmd_Get_DvmVal,
        cmd_Set_Reset,

        cmd_ch1_setting_change,
        cmd_ch2_setting_change,
        cmd_ch3_setting_change,
        cmd_ch4_setting_change,

        //scpi
        cmd_scpi_dvm_val,
    };

    int  serialOut(CStream &stream, serialVersion &ver);
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();
    virtual void registerSpy();

    DsoErr set_DvmEnable(bool b);
    bool   get_DvmEnable();

    DsoErr set_Source(int v);
    int    get_Source();

    DsoErr set_Mode(int v);
    int    get_Mode();

    DsoErr set_AutoRange(bool b);
    bool   get_AutoRange();

    DsoErr set_BeepEnable(bool b);
    bool   get_BeepEnable();

    DsoErr set_LimitType(int v);
    int    get_LimitType();

    DsoErr set_LowerLimit(qint64 v);
    qint64 get_LowerLimit();

    DsoErr set_UpperLimit(qint64 v);
    qint64 get_UpperLimit();

    void *getDvmVal();
    void setReset();
    void setBeep();
    void checkLimit( float val);

    //!  spy
    void on_ChChanged();

    //scpi
    qlonglong getScpiDvmVal();

private:
    bool m_DvmEnable;
    int  m_Source;
    int  m_Mode;
    bool m_AutoRange;
    bool m_BeepEnable;
    int  m_LimitType;
    qint64  m_LowerLimit;
    qint64  m_UpperLimit;

    DvmData *m_DvmData;
    bool m_Reset;
};

#endif // SERVDVM_H
