#include "servdvm.h"
#include "../servlicense/servlicense.h"
#include "../../engine/base/enginecfg.h"

#define dbg_out()

IMPLEMENT_CMD( serviceExecutor, servDvm )
start_of_entry()
on_set_int_bool( MSG_DVM_ENABLE,         &servDvm::set_DvmEnable ),
on_get_bool    ( MSG_DVM_ENABLE,         &servDvm::get_DvmEnable ),

on_set_int_int ( MSG_DVM_SRC,            &servDvm::set_Source ),
on_get_int     ( MSG_DVM_SRC,            &servDvm::get_Source ),

on_set_int_int ( MSG_DVM_MODE,           &servDvm::set_Mode ),
on_get_int     ( MSG_DVM_MODE,           &servDvm::get_Mode ),

//on_set_int_bool( MSG_DVM_AUTO_RANGE,     &servDvm::set_AutoRange ),
//on_get_bool    ( MSG_DVM_AUTO_RANGE,     &servDvm::get_AutoRange ),

on_set_int_bool( MSG_DVM_BEEP_ENABLE,    &servDvm::set_BeepEnable ),
on_get_bool    ( MSG_DVM_BEEP_ENABLE,    &servDvm::get_BeepEnable ),

on_set_int_int ( MSG_DVM_LIMIT_TYPE,     &servDvm::set_LimitType ),
on_get_int     ( MSG_DVM_LIMIT_TYPE,     &servDvm::get_LimitType ),

on_set_int_ll  ( MSG_DVM_LIMIT_LOWER,    &servDvm::set_LowerLimit ),
on_get_ll      ( MSG_DVM_LIMIT_LOWER,    &servDvm::get_LowerLimit ),

on_set_int_ll  ( MSG_DVM_LIMIT_UPPER,    &servDvm::set_UpperLimit ),
on_get_ll      ( MSG_DVM_LIMIT_UPPER,    &servDvm::get_UpperLimit ),

on_set_void_void(servDvm::cmd_Set_Reset,  &servDvm::setReset ),
on_get_pointer ( servDvm::cmd_Get_DvmVal, &servDvm::getDvmVal  ),

//scpi
on_get_ll ( servDvm::cmd_scpi_dvm_val, &servDvm::getScpiDvmVal  ),

//! spy on
on_set_void_void( servDvm::cmd_ch1_setting_change,       &servDvm::on_ChChanged),
on_set_void_void( servDvm::cmd_ch2_setting_change,       &servDvm::on_ChChanged),
on_set_void_void( servDvm::cmd_ch3_setting_change,       &servDvm::on_ChChanged),
on_set_void_void( servDvm::cmd_ch4_setting_change,       &servDvm::on_ChChanged),

on_set_void_void( CMD_SERVICE_RST,                 &servDvm::rst ),
on_set_int_void ( CMD_SERVICE_STARTUP,             &servDvm::startup ),
//on_set_int_void ( CMD_SERVICE_ACTIVE,              &servDvm::setActive ),

end_of_entry()

servDvm::servDvm( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    mVersion = 2;
    serviceExecutor::baseInit();

    m_DvmEnable = false;
    m_Source    = 1; // CH1
    m_Mode      = 0;  //AC RMS
    m_AutoRange = false;
    m_BeepEnable = false;
    m_LimitType  = (int)IN_LIMIT;
    m_LowerLimit = 0;     //! Default Lower Limit: 0V
    m_UpperLimit = 1000000;  //! Default Upper Limit: 1V

    m_DvmData = NULL;
    m_Reset   = false;  //! Reset Dvm Statistic val
}

void servDvm::rst()
{
    //Reset DVM if one source opened. by hxh. 2018-05-17
    if( m_DvmEnable )
    {
        set_DvmEnable( false );
    }

    m_DvmEnable = false;
    m_Source    = chan1; // CH1
    m_Mode      = 0;  //AC RMS
    m_AutoRange = false;
    m_BeepEnable = false;
    m_LimitType  = (int)IN_LIMIT;
    m_LowerLimit = 0;     //! Default Lower Limit: 0V
    m_UpperLimit = 1000000;  //! Default Upper Limit: 1V

    if( NULL != m_DvmData)
    {
        delete m_DvmData;
    }
    m_DvmData = NULL;
    m_Reset   = false;  //! Reset Dvm Statistic val
}

int  servDvm::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;
    stream.write(QStringLiteral("m_DvmEnable"),    m_DvmEnable);
    stream.write(QStringLiteral("m_Source"),       m_Source);
    stream.write(QStringLiteral("m_Mode"),         m_Mode);
    stream.write(QStringLiteral("m_AutoRange"),    m_AutoRange);
    stream.write(QStringLiteral("m_BeepEnable"),   m_BeepEnable);
    stream.write(QStringLiteral("m_LimitType"),    m_LimitType);
    stream.write(QStringLiteral("m_LowerLimit"),   m_LowerLimit);
    stream.write(QStringLiteral("m_UpperLimit"),   m_UpperLimit);

    return ERR_NONE;
}

int  servDvm::serialIn( CStream &stream, serialVersion ver )
{
    if( ver == mVersion)
    {
        stream.read(QStringLiteral("m_DvmEnable"),    m_DvmEnable);
        stream.read(QStringLiteral("m_Source"),       m_Source);
        stream.read(QStringLiteral("m_Mode"),         m_Mode);
        stream.read(QStringLiteral("m_AutoRange"),    m_AutoRange);
        stream.read(QStringLiteral("m_BeepEnable"),   m_BeepEnable);
        stream.read(QStringLiteral("m_LimitType"),    m_LimitType);
        stream.read(QStringLiteral("m_LowerLimit"),   m_LowerLimit);
        stream.read(QStringLiteral("m_UpperLimit"),   m_UpperLimit);
        async( MSG_DVM_ENABLE, m_DvmEnable);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

int  servDvm::startup()
{
//    defer(MSG_DVM_LIMIT_LOWER);
//    defer(MSG_DVM_LIMIT_UPPER);
    return ERR_NONE;
}

void servDvm::registerSpy()
{
    spyOn( serv_name_ch1, MSG_CHAN_SCALE_VALUE, servDvm::cmd_ch1_setting_change );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE_VALUE, servDvm::cmd_ch2_setting_change );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE_VALUE, servDvm::cmd_ch3_setting_change );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE_VALUE, servDvm::cmd_ch4_setting_change );

    spyOn( serv_name_ch1, MSG_CHAN_SCALE, servDvm::cmd_ch1_setting_change );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE, servDvm::cmd_ch2_setting_change );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE, servDvm::cmd_ch3_setting_change );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE, servDvm::cmd_ch4_setting_change );
}

void servDvm::on_ChChanged()
{
    async( MSG_DVM_LIMIT_LOWER, m_LowerLimit);
    async( MSG_DVM_LIMIT_UPPER, m_UpperLimit);
}

qlonglong servDvm::getScpiDvmVal()
{
    DvmData *data = static_cast<DvmData*>(getDvmVal());
    if(data != NULL)
    {
        qlonglong val = 0;
        switch(m_Mode)
        {
        case DVM_AC_RMS:
            val = data->m_AcRms*(1e6);
            break;
        case DVM_DC:
            val = data->m_Dc*(1e6);
            break;
        case DVM_DC_RMS:
            val = data->m_DcRms*(1e6);
            break;
        default:
            val = data->m_AcRms*(1e6);
            break;
        }
        return val;
    }
    else
    {
        return ERR_NONE;
    }
}

DsoErr servDvm::set_DvmEnable(bool b)
{
    DsoErr err = ERR_NONE;
    m_DvmEnable = b;

    if( m_DvmEnable)
    {
        err = postEngine(ENGINE_DVM_COUNTOR_CH_EN, m_Source, true);
        if( err != ERR_NONE)
        {
            return err;
        }

        err = postEngine( ENGINE_DVM_SOURCE, m_Source);
        err = postEngine( ENGINE_DVM_GATE_TIME, GATE850000); //!DVM_GATE_TIME 0.85s

        if( NULL == m_DvmData)
        {
            m_DvmData = new DvmData( (Chan)m_Source, (DvmMode)m_Mode);
            LOG_DBG()<<"new DvmData";
            Q_ASSERT( NULL != m_DvmData);
        }
    }
    else
    {
        if( m_DvmData != NULL)
        {
            delete m_DvmData;
            m_DvmData = NULL;
        }

        err = postEngine(ENGINE_DVM_COUNTOR_CH_EN, m_Source, false);
        if( err != ERR_NONE)
        {
            return err;
        }
    }

    return err;
}

bool servDvm::get_DvmEnable()
{
    return m_DvmEnable;
}

DsoErr servDvm::set_Source(int v)
{
    DsoErr err = ERR_NONE;
    m_Source = v;

    if( m_DvmEnable)
    {
        err = postEngine(ENGINE_DVM_COUNTOR_CH_EN, m_Source, true);
        if( err != ERR_NONE)
        {
            return err;
        }
    }

    err = postEngine( ENGINE_DVM_SOURCE, m_Source);
    err = postEngine( ENGINE_DVM_GATE_TIME, GATE850000); //!DVM_GATE_TIME 0.85s
    if( NULL != m_DvmData)
    {
        m_DvmData->setSrc( (Chan)m_Source);
    }

    return err;
}

int servDvm::get_Source()
{
    return m_Source;
}

DsoErr servDvm::set_Mode(int v)
{
    DsoErr err = ERR_NONE;
    m_Mode = v;

    err = postEngine( ENGINE_DVM_GATE_TIME, GATE850000); //!DVM_GATE_TIME 0.85s
    if( NULL != m_DvmData)
    {
        m_DvmData->setMode( (DvmMode)m_Mode);
    }

    return err;
}

int servDvm::get_Mode()
{
    return m_Mode;
}

DsoErr servDvm::set_AutoRange(bool b)
{
    m_AutoRange = b;
    return ERR_NONE;
}

bool servDvm::get_AutoRange()
{
    return m_AutoRange;
}

DsoErr servDvm::set_BeepEnable(bool b)
{
    m_BeepEnable = b;

    return ERR_NONE;
}

bool servDvm::get_BeepEnable()
{
    return m_BeepEnable;
}

DsoErr servDvm::set_LimitType(int v)
{
    m_LimitType = v;
    return ERR_NONE;
}

int servDvm::get_LimitType()
{
    return m_LimitType;
}

DsoErr servDvm::set_LowerLimit(qint64 v)
{
    m_LowerLimit = v;
    return ERR_NONE;
}

qint64 servDvm::get_LowerLimit()
{
    dsoVert *pVert = dsoVert::getCH( (Chan)m_Source );

    qlonglong step = pVert->getYRefScale() / adc_vdiv_dots;
    setuiStep( step );

    qlonglong minVal = -500 * 1000000ll;      //! Min: -500V
    qlonglong maxVal = m_UpperLimit - step;
    setuiRange( minVal, maxVal, 0ll);

    Unit unit = pVert->getUnit();
//    qDebug()<<"pVert->getUnit(): "<<unit;
    setuiUnit( unit);
    if( unit == Unit_W)       setuiPostStr( "W" );
    else if( unit == Unit_A)  setuiPostStr( "A" );
    else if( unit == Unit_V)  setuiPostStr( "V" );
    else if( unit == Unit_U)  setuiPostStr( "U" );
    else                      setuiPostStr( "V" );

    setuiBase(1, E_N6);

    return m_LowerLimit;
}

DsoErr servDvm::set_UpperLimit(qint64 v)
{
    m_UpperLimit = v;
    return ERR_NONE;
}

qint64 servDvm::get_UpperLimit()
{
    dsoVert *pVert = dsoVert::getCH( (Chan)m_Source );

    qlonglong step = pVert->getYRefScale() / adc_vdiv_dots;
    setuiStep( step );

    qlonglong minVal = m_LowerLimit + step;
    qlonglong maxVal = 500 * 1000000ll;   //! Max: 500V
    setuiRange( minVal, maxVal, 0ll);

    Unit unit = pVert->getUnit();
    setuiUnit( unit);
    if( unit == Unit_W)       setuiPostStr( "W" );
    else if( unit == Unit_A)  setuiPostStr( "A" );
    else if( unit == Unit_V)  setuiPostStr( "V" );
    else if( unit == Unit_U)  setuiPostStr( "U" );
    else                      setuiPostStr( "V" );

    setuiBase(1, E_N6);

    return m_UpperLimit;
}

void *servDvm::getDvmVal()
{
    DsoErr err = ERR_NONE;
    EngineDvmValue val;

    err = queryEngine( qENGINE_DVM_VOLT, &val);
    if( err == ERR_NONE)
    {
        if( NULL != m_DvmData)
        {
            m_DvmData->calcData(&val, m_Reset);
            if( m_BeepEnable == true)
            {
                setBeep();
            }
            if( m_Reset)
            {
                m_Reset = false;
            }
            return m_DvmData;
        }
        else
        {
            return NULL;
        }
    }
    else
    {
        return NULL;
    }
}

void servDvm::setReset()
{
    m_Reset   = true;  //! Reset Dvm Statistic val
}

void servDvm::setBeep()
{
    if( NULL != m_DvmData)
    {
        if( m_DvmData->m_Mode == DVM_AC_RMS)
        {
            checkLimit( m_DvmData->m_AcRms);
        }
        else if( m_DvmData->m_Mode == DVM_DC)
        {
            checkLimit( m_DvmData->m_Dc);
        }
        else if( m_DvmData->m_Mode == DVM_DC_RMS)
        {
            checkLimit( m_DvmData->m_DcRms);
        }
        else
        {
            checkLimit( m_DvmData->m_AcRms);
        }
    }
}

void servDvm::checkLimit( float val)
{
    LOG_DBG()<<"servDvm::checkLimit InputVal: "<<val;
    LOG_DBG()<<"servDvm::checkLimit m_LowerLimit: "<<(m_LowerLimit/1e6f);
    LOG_DBG()<<"servDvm::checkLimit m_UpperLimit: "<<(m_UpperLimit/1e6f);

    if( (DvmLimitType)m_LimitType == IN_LIMIT)  //! Within Limits
    {
        if( (val > (m_LowerLimit/1e6f))&&(val < (m_UpperLimit/1e6f)) )  //! Limits: us
        {
            LOG_DBG()<<"servDvm::checkLimit within Limits";
            //! Set BEEP
            syncEngine( ENGINE_BEEP_SHORT );
        }
    }

    if( (DvmLimitType)m_LimitType == OUT_LIMIT) //! Outside Limits
    {
        if( (val < (m_LowerLimit/1e6f))||(val > (m_UpperLimit/1e6f)) ) //! Limits: us
        {
            LOG_DBG()<<"servDvm::checkLimit outside Limits";
            //! Set BEEP
            syncEngine( ENGINE_BEEP_SHORT );
        }
    }
}
