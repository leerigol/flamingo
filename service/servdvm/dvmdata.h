#ifndef DVMDATA_H
#define DVMDATA_H
#include "../../engine/base/enginecfg.h"
#include "../../include/dsostd.h"

#define DVM_BUFFER_SIZE 6

enum DvmStatus
{
    DVM_Valid,
    DVM_Invalid,
};

enum DvmMode
{
    DVM_AC_RMS,
    DVM_DC,
    DVM_DC_RMS,
};

class DvmData
{
public:
    DvmData(Chan s, DvmMode m);

    void setSrc( Chan s)      { m_Src = s;}
    void setMode( DvmMode m)  { m_Mode = m;}
    void calcData( EngineDvmValue *val, bool resetStat);
    void checkStatus();

    void getDcMaxMin();
    void getAcRmsMaxMin();
    void getDcRmsMaxMin();
    void writeBuffer(QList<float> &buffer, float val);
public:
    Chan m_Src;
    DvmMode m_Mode;
    Unit  m_Unit;

    float m_AcRms;
    float m_AcRmsMax;
    float m_AcRmsMin;
    float m_AcRms_ScaleMin;
    float m_AcRms_ScaleMax;
    QList<float> m_AcRmsBuffer;

    float m_DcRms;
    float m_DcRmsMax;
    float m_DcRmsMin;
    float m_DcRms_ScaleMin;
    float m_DcRms_ScaleMax;
    QList<float> m_DcRmsBuffer;

    float m_Dc;
    float m_DcMax;
    float m_DcMin;
    float m_Dc_ScaleMin;
    float m_Dc_ScaleMax;
    QList<float> m_DcBuffer;

    float m_VppMax;
    float m_VppMin;

    DvmStatus m_Status;
};

#endif // DVMDATA_H
