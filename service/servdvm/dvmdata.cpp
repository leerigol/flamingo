#include "dvmdata.h"

DvmData::DvmData(Chan s, DvmMode m)
{
    m_Src = s;
    m_Mode = m;
    m_Unit = Unit_V;

    m_AcRms = 0.0f;
    m_AcRmsMax = 0.0f;
    m_AcRmsMin = 0.0f;
    m_AcRms_ScaleMin = 0.0f;
    m_AcRms_ScaleMax = 0.0f;

    m_DcRms = 0.0f;
    m_DcRmsMax = 0.0f;
    m_DcRmsMin = 0.0f;
    m_DcRms_ScaleMin = 0.0f;
    m_DcRms_ScaleMax = 0.0f;

    m_Dc    = 0.0f;
    m_DcMax = 0.0f;
    m_DcMin = 0.0f;
    m_Dc_ScaleMin = 0.0f;
    m_Dc_ScaleMax = 0.0f;

    m_VppMax   = 0.0f;
    m_VppMin   = 0.0f;
    m_Status = DVM_Invalid;

    LOG_DBG()<<"buffer clear";
    m_AcRmsBuffer.clear();
    m_DcRmsBuffer.clear();
    m_DcBuffer.clear();
}

void DvmData::calcData( EngineDvmValue *val, bool resetStat)
{
    if( NULL != val)
    {
        dsoVert *pVert = dsoVert::getCH( m_Src);
        float ratio;
        pVert->getProbe().toReal( ratio, 1.0f);
        float yInc = (val->mScale)/1e6f/(val->mVDiv)*ratio; //! Scale unit: uv
        m_Unit = pVert->getUnit();

        float avg = val->mDvmAver/256.0;
        m_Dc = (avg*1.0f - val->mGnd)*yInc;
        writeBuffer( m_DcBuffer, m_Dc);
//        m_AcRms = qSqrt(val->mDvmRms*1.0f - (val->mDvmAver)*(val->mDvmAver))*yInc ;
//        float Rms = qSqrt(val->mDvmRms*1.0f - (val->mGnd)*(val->mGnd))*yInc;
//        m_DcRms = Rms - m_AcRms;

        m_DcRms = qSqrt( val->mDvmRms*1.0f
                         - 2*(val->mGnd)*(avg)*1.0f
                         + (val->mGnd)*(val->mGnd))*yInc;
        writeBuffer( m_DcRmsBuffer, m_DcRms);

        m_AcRms = qSqrt( val->mDvmRms*1.0f
                         - (avg)*(avg))*yInc;
        writeBuffer( m_AcRmsBuffer, m_AcRms);

        checkStatus();

        float base;
        pVert->getYRefBase().toReal(base);
        float scale  = pVert->getYRefScale()*base*ratio;
        float offset = pVert->getYRefOffset()*base*ratio;;
        m_AcRms_ScaleMin = 0.0f;
        m_AcRms_ScaleMax = scale*vert_div/2;
        m_DcRms_ScaleMin = 0;
        m_DcRms_ScaleMax = scale*vert_div/2 + qAbs(offset);
        m_Dc_ScaleMin = -scale*vert_div/2 - offset;
        m_Dc_ScaleMax = scale*vert_div/2 - offset;

        if(resetStat)
        {
            m_DcMax = m_Dc;
            m_DcMin = m_Dc;
            m_DcBuffer.clear();

            m_AcRmsMax = m_AcRms;
            m_AcRmsMin = m_AcRms;
            m_AcRmsBuffer.clear();

            m_DcRmsMax = m_DcRms;
            m_DcRmsMin = m_DcRms;
            m_DcRmsBuffer.clear();
        }
        else
        {
            getDcMaxMin();
            getAcRmsMaxMin();
            getDcRmsMaxMin();
        }

        LOG_DBG() << "-------------------calcData mDvmAver= "<<val->mDvmAver;
        LOG_DBG() << "-------------------calcData mDvmRms = "<<val->mDvmRms;
        LOG_DBG() << "-------------------calcData mGnd   = "<<val->mGnd;
        LOG_DBG() << "-------------------calcData mScale = "<<val->mScale;
        LOG_DBG() << "-------------------calcData mVDiv  = "<<val->mVDiv;
        LOG_DBG() << "-------------------calcData ProbeRatio  = "<<ratio;
        LOG_DBG() << "-------------------calcData yInc  = "<<yInc;

        LOG_DBG() << "-------------------calcData m_Dc             = "<<m_Dc;
        LOG_DBG() << "-------------------calcData m_DcMax          = "<<m_DcMax;
        LOG_DBG() << "-------------------calcData m_DcMin          = "<<m_DcMin;

        LOG_DBG() << "-------------------calcData m_DcRms             = "<<m_DcRms;
        LOG_DBG() << "-------------------calcData m_AcRms             = "<<m_AcRms;

//        m_DcMax = val->mDvmRange>>8;   //! [31:16]: Max(in 3s)
//        m_DcMin = val->mDvmRange&0xff; //! [15:0]:  Min(in 3s)
    }
}

void DvmData::writeBuffer( QList<float> &buffer, float val)
{
    if( buffer.size() >= DVM_BUFFER_SIZE)  //! buffer full, remove head
    {
        buffer.removeFirst();
        buffer.append( val);
    }
    else                                  //! buffer not full
    {
        buffer.append( val);
    }
}

void DvmData::getDcMaxMin()
{
    LOG_DBG()<<"DvmData::getDcMaxMin()  m_DcBuffer.size(): "<<m_DcBuffer.size();
    if( m_DcBuffer.size() <= 1)
    {
        m_DcMax = m_Dc;
        m_DcMin = m_Dc;
    }
    else
    {
        m_DcMax = m_DcBuffer.at(0);
        m_DcMin = m_DcBuffer.at(0);
        foreach( float val, m_DcBuffer)
        {
            LOG_DBG()<<"m_DcBuffer.val: "<<val;
            m_DcMax = (val > m_DcMax)? val : m_DcMax;
            m_DcMin = (val < m_DcMin)? val : m_DcMin;
        }
    }
}

void DvmData::getDcRmsMaxMin()
{
    if( m_DcRmsBuffer.size() <= 1)
    {
        m_DcRmsMax = m_DcRms;
        m_DcRmsMin = m_DcRms;
    }
    else
    {
        m_DcRmsMax = m_DcRmsBuffer.at(0);
        m_DcRmsMin = m_DcRmsBuffer.at(0);
        foreach( float val, m_DcRmsBuffer)
        {
            m_DcRmsMax = (val > m_DcRmsMax)? val : m_DcRmsMax;
            m_DcRmsMin = (val < m_DcRmsMin)? val : m_DcRmsMin;
        }
    }
}

void DvmData::getAcRmsMaxMin()
{
    if( m_AcRmsBuffer.size() <= 1)
    {
        m_AcRmsMax = m_AcRms;
        m_AcRmsMin = m_AcRms;
    }
    else
    {
        m_AcRmsMax =  m_AcRmsBuffer.at(0);
        m_AcRmsMin =  m_AcRmsBuffer.at(0);
        foreach( float val, m_AcRmsBuffer)
        {
            m_AcRmsMax = (val > m_AcRmsMax)? val : m_AcRmsMax;
            m_AcRmsMin = (val < m_AcRmsMin)? val : m_AcRmsMin;
        }
    }
}

void DvmData::checkStatus()
{
    if( m_Mode == DVM_AC_RMS)
    {
        if( !isinf(m_AcRms) && !isnan(m_AcRms))
        {
            m_Status = DVM_Valid;
        }
        else
        {
            m_Status = DVM_Invalid;
        }
    }
    else if( m_Mode == DVM_DC)
    {
        if( !isinf(m_Dc) && !isnan(m_Dc))
        {
            m_Status = DVM_Valid;
        }
        else
        {
            m_Status = DVM_Invalid;
        }
    }
    else if( m_Mode == DVM_DC_RMS)
    {
        if( !isinf(m_DcRms) && !isnan(m_DcRms))
        {
            m_Status = DVM_Valid;
        }
        else
        {
            m_Status = DVM_Invalid;
        }
    }
    else
    {
        if( !isinf(m_AcRms) && !isnan(m_AcRms))
        {
            m_Status = DVM_Valid;
        }
        else
        {
            m_Status = DVM_Invalid;
        }
    }
}
