#include <sys/vfs.h>

#include "../servhelp/servhelp.h"
#include "servstorage.h"
#include "storage_formater.h"

///
/// \brief servStorage::getFreeSpace
/// \param path
/// \return  size of free space.
///

qint64 servStorage::getFreeSpace(QString &path)
{
    struct statfs diskInfo;
    QStringList pathList = path.split("/");
    QString     devPath = "/";

    if( path.size() > 3 )
    {
        devPath = devPath + pathList.at(1) + "/" + pathList.at(2);
    }
    if( statfs(devPath.toUtf8().data(), &diskInfo) >= 0 )
    {
        //qDebug("%s 总大小:%.0lfMB 可用大小:%.0lfMB",path.toStdString().c_str(),(diskInfo.f_blocks * diskInfo.f_bsize)/1024.0/1024.0,
        //(diskInfo.f_bavail * diskInfo.f_bsize)/1024.0/1024.0);
        qint64 size = ((qint64)diskInfo.f_bavail * (qint64)diskInfo.f_bsize)/(qint64)1024;
        //qDebug() << devPath <<  "FreeSapce" << size;

        return (size) * 1000;
    }
    return 0;
}

int servStorage::getRemovableList(QList<QString > &list)
{
    list.clear();

    int size = m_hFat.size();
    if( size > 1 )
    {
        for(int i=1; i<m_hFat.size(); i++)
        {
            list.append( m_hFat.at(i).second );
        }
    }
    return (size-1);
}

bool servStorage::isPathExist(QString &p)
{
    int size = m_hFat.size();
    if( size == 1)
    {
        return false;
    }

    QFileInfo fi(p);
    if( fi.exists() )
    {
        return true;
    }
    qDebug() << p << "not exist";
    return false;
}

///
/// \brief static
/// \param f
/// \return
///
QString& servStorage::getFileExt(int f)
{
    if( f == -1)
    {
        f = m_nFileFormat;
    }
    if(m_NameInfo.contains(f))
    {
        return m_NameInfo.value(f)->ext;
    }

    return m_NameInfo.value(servFile::FILETYPE_ANY)->ext;
}

QString servStorage::getRealDisk(QString disk)
{
   //qDebug()<<__FILE__<<__LINE__<<disk;
    for(int i=0; i<m_hFat.size(); i++)
    {
        if( m_hFat.at(i).first.compare(disk,Qt::CaseInsensitive) == 0)
        {
            return m_hFat.at(i).second;
        }
    }
    return  "";
}

QString servStorage::getRealPath(QString &path)
{
    QString realPath = STORAGE_USER_PATH;
    if(path.size())
    {
        path = path.simplified().replace(QChar('\\'), QChar('/'));
        realPath = getRealDisk(path.mid(0,1));
        if(realPath.size())
        {
            if( path.mid(1,1).compare(":") != 0 )
            {
                realPath = realPath + "/" + path;
            }
            else
            {
              realPath = realPath + "/" +  path.mid(2);
            }
            return realPath;
        }
        else
        {
            return "";
        }
    }

    Q_ASSERT(0);
    return realPath;
}

QString servStorage::getFrendlyDisk(QString p)
{
    //qDebug() << "getDispDisk" << p;
    for(int i=0; i<m_hFat.size(); i++)
    {
        //qDebug() << m_hFat.at(i).second;
        if( m_hFat.at(i).second.compare(p) == 0)
        {
            if(p.startsWith(STORAGE_USER_PATH))
            {
                return "Local Disk (C)";
            }
            return QString("Removable USB Disk (%1)").arg(m_hFat.at(i).first);
        }
    }
    return "";
}

QString servStorage::getFrendlyPath(QString &p)
{

    if( p.startsWith("/tmp/"))
    {
        return p.right(p.size() - QString("/tmp/").size());
    }
    QString output = getDispPath(p);

    QStringList list = output.split("/");
    QString str;
    //qDebug() << list;

    int size = output.size();

    output = list_at( list,1) + ":\\";

    if(size < 40)
    {
        for(int i=2; i<list.size()-1; i++)
        {
            str = list_at( list,i);
            output = output + str + "\\";
        }

        if(list.size() > 2 )
        {
            output = output + list_at( list,list.size() - 1);
        }
    }
    else
    {
        for(int i=2; i<list.size()-1; i++)
        {
            output = output + ".." + "\\";
        }
        output = output + list_at( list,list.size()-1);
    }
    //qDebug() << p << output;
    return output;
}

QString servStorage::getDispDisk(QString p)
{
    //qDebug() << "getDispDisk" << p;
    for(int i=0; i<m_hFat.size(); i++)
    {
        //qDebug() << m_hFat.at(i).second;
        if( m_hFat.at(i).second.compare(p) == 0)
        {
            return m_hFat.at(i).first;
        }
    }
    return "";
}

QString servStorage::getDispPath(QString &path)
{
    QString dispPath = "/";

    if(path.startsWith(STORAGE_USER_PATH))
    {
        dispPath =  dispPath + STORAGE_USER_DISK + path.mid(STORAGE_USER_PATH.size());
    }
    else
    {
        int len = STORAGE_USB_PATH.size();
        dispPath = dispPath + getDispDisk(path.left(len)) + path.mid(len);
    }

    //qDebug() << path << "->" << dispPath;
    return dispPath;
}

bool servStorage::isRootPath()
{
    return m_nPathSeries == 0;
}

void servStorage::updateList(int f)
{
    m_stFileExt.clear();
    m_stFileExt << getFileExt(f);
    if(f == servFile::FILETYPE_JPG)
    {
        m_stFileExt << QString("jpeg");
    }

    readFiles();

    if( m_nDoProc & FUNC_SAVE )
    {
        buildFileName(f);
        async(MSG_STORAGE_PREFIX, STORAGE_REVERVED);
    }
}

void servStorage::initUsbMedia()
{
    m_hFat.clear();
    //checking if USB Disk is inserted
    QPair<QString,QString> p;
    p.first  = STORAGE_USER_DISK;;
    p.second = STORAGE_USER_PATH;
    m_hFat.append( p );

    QDir usblist(STORAGE_USB_MOUNT);
    m_stFiles = usblist.entryInfoList(QStringList("*"),
                                      QDir::Dirs | QDir::NoDotAndDotDot,
                                      QDir::Name);
    int count = m_stFiles.size();
    if( count > 0 )
    {
        for(int i=0; i<count; i++)
        {
            QString disk = QString('D'+i);
            p.first = disk;
            p.second= m_stFiles.at(i).filePath();

            m_hFat.append(p);
        }
    }
}

/**
 * @brief this will cost more CPU clocks
 */
void servStorage::readFiles()
{
    m_stFiles.clear();
    if(isRootPath()) //top
    {
        initUsbMedia();
        m_stFiles.insert(0,m_UserPath);
    }
    else
    {
        //Read all entries first
        //filter the specific ext files
        QDir::Filters nFilter = QDir::Files |
                                QDir::Dirs |
                                QDir::NoSymLinks |
                                QDir::NoDotAndDotDot;

        m_stFiles = m_Dir.entryInfoList(QStringList("*"),
                                        nFilter,
                                        QDir::Time|QDir::DirsFirst);

        if(m_stFiles.size() && !m_stFileExt.contains("*") )
        {
            foreach(QFileInfo f, m_stFiles)
            {
               QString suffix = f.suffix();
               if(f.isFile() && !m_stFileExt.contains(suffix,Qt::CaseInsensitive))
               {
                   m_stFiles.removeOne(f);
               }
            }
        }
        m_stFiles.insert(0, m_DotDot);
    }
}

int servStorage::setFileIndex(int index)
{
    const QFileInfo* f = NULL;

    if(!m_stFiles.size())
    {
        return ERR_NONE;
    }
    else
    {
        if(index < m_stFiles.size())
        {
            f = &( list_at( m_stFiles,index));
        }
        else //self-adjust
        {
            f = &( list_at( m_stFiles,0));
            index = 0;
        }
    }

    m_nFileIndex = index;
    //qDebug() << f->filePath();
    if(m_nDoProc == FUNC_STORAGE_DISK)
    {
        /* normal file or directory */
        if( f->isFile()  || (f->isDir() && f->fileName().compare("..") != 0) )
        {
            setCopyEnable(true);
            setRenameEnable(true);
            if(f->filePath().compare(STORAGE_USER_PATH) == 0)
            {
                setFormatEnable(isRootPath());
            }
            else
            {
                setFormatEnable(false);
            }
        }
        else
        {
            setCopyEnable(false);
            setRenameEnable(false);
        }
    }
    else // save or load
    {
        if(m_nDoProc & FUNC_LOAD )
        {
            if( f->isFile() )
            {
                QString fn = f->fileName();
                setFileName(fn);
            }
            //Enable the load button only if it is a filtered file
            setLoadEnable(f->isFile());
            //qDebug() << "file select:" << f->fileName();
        }
        else
        {
            //buildFileName(m_nFileFormat);
        }
    }

    //enable prefix modifiy
    if( (m_nDoProc & FUNC_SAVE) && getThreadProc() != servFile::ProcSaving)
    {
        mUiAttr.setEnable(MSG_STORAGE_PREFIX, true);
    }
    return ERR_NONE;
}

void* servStorage::getFileList()
{
    return (void*)&m_stFiles;
}


/// \param ret
/// \return
///
int servStorage::onResult(int ret)
{
    int proc = getThreadProc();
    setProcStatus(proc_done);

    if(proc == servFile::ProcSaving ||
       proc == servFile::ProcCopying)
    {
        updateList(m_nFileFormat);
    }
    else if(proc == servFile::ProcSnaping)
    {
        if(!CImageFormater::mCmdValueReady)
        {
           CImageFormater::mCmdValueReady = true;
           sysSetScpiEvent(scpi_event_output_queue, 1);
        }
        return ret;
    }

    else if(proc == servFile::ProcUpgrading)
    {
        m_nUpgradeResult = ret;
        if(ret == ERR_NONE)
        {
            syncEngine(ENGINE_POWER_RESTART);
        }
        else
        {
//            if( !m_bUpgradeMedia )
//            {
//                post(serv_name_help, servHelp::cmd_usb_status, true);
//            }
            defer(servStorage::FUNC_UPGRADE_RESULT);
        }        
        return ERR_NONE;
    }
    else if(proc == servFile::ProcFormating)
    {
        setFormatEnable(true);

        servGui::showInfo("Security cleared successfully");
        return ERR_NONE;
    }
    else if(proc == servFile::ProcRestoring)
    {
        //qDebug() << proc << "update result";
        m_bDefaulting = false;
        return ERR_NONE;
    }

    if(m_nDoProc == FUNC_STORAGE_DISK)
    {
        setPasteEnable(true);
    }
    else if(m_nDoProc & servStorage::FUNC_SAVE)
    {
        setSaveEnable(true);
        async(MSG_STORAGE_PREFIX, STORAGE_REVERVED);
    }
    else if(m_nDoProc & servStorage::FUNC_LOAD)
    {
        setLoadEnable(true);

        //return to ref when loaded
        if( m_nDoProc == FUNC_LOAD_REF )
        {
            post(E_SERVICE_ID_REF, CMD_SERVICE_ACTIVE, MSG_REF_OPTION);
        }
    }
    return ret;
}

int servStorage::onWebPull(int)
{
    //else if(proc == servFile::ProcPulling)
    {
        if( !CPathFormater::mCmdValueReady )
        {
            CPathFormater::mCmdValueReady = true;
            sysSetScpiEvent(scpi_event_output_queue, 1);
        }
    }
    return ERR_NONE;
}

////////////////////Function Tune push///////////////////////////////

void servStorage::onTunePush(int tune)
{
    const QFileInfo* f = &( list_at( m_stFiles,m_nFileIndex));
    QString fname = f->fileName();

    if(tune || fname.compare("..") == 0)
    {
        dirReturn();
    }
    else if(f->isDir())
    {
        dirEnter(f->filePath());
    }
    else
    {
        //sync with the path content
        if( m_nDoProc & FUNC_SAVE)
        {
            bool b = mUiAttr.getItem(MSG_STORAGE_PREFIX)->getEnable();

            //select another as the current file .continue disable
            if( b || m_FileName.compare(fname))
            {
                m_FileName = fname;
                if(b)
                {
                    mUiAttr.setEnable(MSG_STORAGE_PREFIX, false);
                }
            }
            else
            {
                //select the same file twice,enable
                buildFileName( m_nFileFormat );
                async(MSG_STORAGE_PREFIX, STORAGE_REVERVED );
                mUiAttr.setEnable(MSG_STORAGE_PREFIX, true);
            }
        }
    }
    setFormatEnable(isRootPath());
}

void servStorage::dirEnter(const QString &d)
{
    m_nPathSeries++;

    setPathName(d);
    m_Dir.setPath(d);
    readFiles();
    setNewEnable(true);
    setPasteEnable( m_bCopiedFile );
}

void servStorage::dirReturn()
{
    if(m_nPathSeries > 0)
    {
        m_nPathSeries--;
    }
    if(isRootPath())
    {
       setPathName(STORAGE_USER_PATH);
       setPasteEnable( false );
    }
    else  // only can return in sub dir
    {
        m_Dir.cdUp();
        setPathName(m_Dir.path());
        setPasteEnable( m_bCopiedFile );
    }

    setNewEnable(!isRootPath());

    readFiles();

    defer(servStorage::FUNC_STORAGE_REDRAW);
}


