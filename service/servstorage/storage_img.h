#ifndef IMAGEFILE
#define IMAGEFILE
#include <QImage>

#ifdef __cplusplus
extern "C" {
#endif

extern int userTIFDecoder(uchar* inStream,
                          uchar* outStream,
                          int imgWidth,
                          int imgHeight,
                          int* pBufferSize);
#ifdef __cplusplus
}
#endif

class CImageFile: public QObject
{
    public:
        static int      saveIMG(QString&);
        static int      saveIMG(QImage&, QString&);
        static int      saveTIF(QImage&, QString&);
        static int      getRawData(QImage& out, bool header = false);
        static int      toGray(QImage& src, QImage& dst);
    private:
        static uchar*   m_pImageBuf;

        static const int IMAGE_HEADER_HEIGHT = 30;
};

#endif // IMAGEFILE

