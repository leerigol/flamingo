#include "storage_wav.h"
#include "../servmath/servmath.h"

CCSVFile::CCSVFile()
{
    m_pFile = NULL;
    mTimeEn = false;
}

CCSVFile::~CCSVFile()
{
    close();
}


bool CCSVFile::close()
{
    if(m_pFile)
    {
        if(m_pFile->isOpen())
        {
            m_pFile->flush();
            m_pFile->close();
        }
        delete m_pFile;
        m_pFile = NULL;
        return true;
    }
    return false;
}

QString CCSVFile::getFileName()
{
    return mFileName;
}

bool CCSVFile::create(QString &csvFileName)
{
    mFileName = csvFileName;

    m_pFile = new QFile(csvFileName);
    if(m_pFile)
    {
       return m_pFile->open(QIODevice::WriteOnly | QIODevice::Text);
    }
    return false;
}

bool CCSVFile::writeCSVHeader(QString& ref)
{
    QTextStream stream(m_pFile);
    stream << "Time(s)," << ref << "\n";
    return true;
}

bool CCSVFile::writeCSVHeader(QList<int>& chanList, bool laHigh, bool laLow)
{
    int size = chanList.size();
    if(size)
    {
        QTextStream stream(m_pFile);

//        stream << "Vendor," << "RIGOL\n";
//        stream << "Model,"  << servUtility::getSystemModel() << "\n";
//        stream << "Firmware," << servUtility::getSystemVersion() << "\n";
//        stream << "Serial," << servUtility::getSystemSerial() << "\n";

//        stream << "\n";
//        stream << "Sample Rate,"   << "10GSa/s\n";
//        stream << "Memory Length,"  << "100Mpts\n";
//        stream << "Timebase,"      << "0.02us\n";
//        stream << "Time Offset,"   << "0s\n";
//        stream << "\n";

//        Unit_W,
//        Unit_A,
//        Unit_V,
//        Unit_U,

        QString chanUnits[] = {"W","A","V","U"};
        QString chans[] = {serv_name_ch1,serv_name_ch2,serv_name_ch3,serv_name_ch4};
        QString maths[] = {serv_name_math1,serv_name_math2,serv_name_math3,serv_name_math4};
        stream << "Time(s),";

        QString chanName;
        QString chanUnit;
        for(int i=0; i<size; i++)
        {
            if( chanList.at(i) >= chan1 && chanList.at(i) <= chan4 )
            {
                 int unit = 0;
                 serviceExecutor::query(chans[chanList.at(i)-chan1], MSG_CHAN_UNIT, unit);
                 if(unit <= Unit_U)
                 {
                    chanUnit = chanUnits[ unit - Unit_W ];
                 }
                 else
                 {
                     chanUnit = "U";
                 }

                 chanName = QString("CH%1(%2)").arg( chanList.at(i))
                                               .arg( chanUnit );
            }
            else//m1 m2 m3m 4
            {
                serviceExecutor::query(maths[chanList.at(i)-m1],
                                      servMath::cmd_q_unit_string, chanUnit);

                chanName = QString("MATH%1(%2)").arg( chanList.at(i)-m1 + 1)
                                              .arg( chanUnit );

            }

            stream << chanName;
            if( i != (size-1))
            {
                stream << ",";
            }
        }

        if( laHigh )
        {
            stream << ",d15d8";
        }
        if( laLow )
        {
            stream << ",d7d0";
        }

        stream << "\n";
        return true;
    }

    return false;
}

bool CCSVFile::writeWaveData(QList<DsoWfm*>& chanList)
{
    int chan_count = chanList.size();
    if(chan_count)
    {
        QString csvData = "";
        int step = 1000;
        int size = chanList.at(0)->size();
        if( size > 100 )
        {
            step = size / 100;
        }

        for(int i=0; i<size; i++) // how many point?
        {
            DsoWfm* wfm = chanList.at(0);
            csvData = QString("%1").arg(wfm->gett0().toDouble() + i*wfm->gettInc().toDouble());
            for(int j=0; j<chan_count; j++) // how many channel?
            {
                DsoWfm* wfm = chanList.at(j);
#if 0
                csvData = csvData + QString(",%1").arg(wfm->valueAt(i) +  (wfm->getyRefOffset() / 1e6) ) ;
#else
                if( i < wfm->size() )
                {
                    csvData = csvData + QString(",%1").arg(wfm->valueAt(i)) ;
                }
                else
                {
                    csvData = csvData + QString(",");
                }
#endif
            }

            csvData = csvData + "\n";

            if( m_pFile->write(csvData.toLocal8Bit().data(), csvData.size()) < 0 )
            {
                return false;
            }

            if( i % step == 0 )
            {
                servFile::setProgress( i * 100 / size );
            }

        }
        return true;
    }

    return false;
}


bool CCSVFile::writeWaveData(QList<DsoWfm*>& chanList,
                             DsoWfm *plaHigh,
                             DsoWfm *plaLow)
{
    int chan_count = chanList.size();
    if(chan_count)
    {
        QString csvData = "";
        int step = 1000;
        int size = chanList.at(0)->size();
        if( size > 100 )
        {
            step = size / 100;
        }

        for(int i=0; i<size; i++) // how many point?
        {
            DsoWfm* wfm = chanList.at(0);
            csvData = QString("%1").arg(wfm->gett0().toDouble() + i*wfm->gettInc().toDouble());
            for(int j=0; j<chan_count; j++) // how many channel?
            {
                DsoWfm* wfm = chanList.at(j);
#if 0
                csvData = csvData + QString(",%1").arg(wfm->valueAt(i) +  (wfm->getyRefOffset() / 1e6) ) ;
#else
                csvData = csvData + QString(",%1").arg(wfm->valueAt(i)) ;
#endif
            }

            if( plaHigh != NULL)
            {
                csvData = csvData + QString(",0x%1").arg( plaHigh->at(2*i),0,16);
            }
            if( plaLow != NULL )
            {
                csvData = csvData + QString(",0x%1").arg( plaLow->at(2*i+1),0,16);
            }
            csvData = csvData + "\n";

            if( m_pFile->write(csvData.toLocal8Bit().data(), csvData.size()) < 0 )
            {
                return false;
            }

            if( i % step == 0 )
            {
                servFile::setProgress( i * 100 / size );
            }

        }
        return true;
    }

    return false;
}

bool CCSVFile::writeCSVHeader(Chan ch, vertAttr vAttr, horiAttr hAttr)
{
    mPastCount     = 0;

    //! Voltage Map
    qDebug()<<__FILE__<<__LINE__<<"yInc:"<<vAttr.yInc<<"yGnd:"<<vAttr.yGnd;
    QString sValue = "";
    for(int adc = 0; adc <= 255; adc++)
    {
        float  voltage    = (-vAttr.yGnd * vAttr.yInc) +   adc * vAttr.yInc;

        sValue = QString("%1").arg(voltage, 0, 'E', 6);

        if(voltage>=0)
        {
            sValue = "+" + sValue;
        }

        mVoltageMap[adc]  = sValue.toLatin1();
    }
    mHAttr = hAttr;

    //! CH Title
    QString chanUnits[] = {"W","A","V","U"};
    QString chans[]     = {serv_name_ch1,serv_name_ch2,serv_name_ch3,serv_name_ch4};
    QString maths[]     = {serv_name_math1,serv_name_math2,serv_name_math3,serv_name_math4};
    QString chanName    = "";
    QString chanUnit    = "";
    QString timeTitle   = "Time(s),";

    if( chan1 <= ch  && ch <= chan4 )
    {
        int unit = 0;
        serviceExecutor::query(chans[ch-chan1], MSG_CHAN_UNIT, unit);
        if(unit <= Unit_U)
        {
            chanUnit = chanUnits[ unit - Unit_W ];
        }
        else
        {
            chanUnit = "U";
        }

        chanName = QString("CH%1(%2),").arg( ch ).arg( chanUnit );
    }
    else if ( d0 <= ch  && ch <= d15 )
    {
        chanName = QString("D%1,").arg(ch-d0);
    }
    else if ( ch == d0d7 )
    {
        chanName = QString("D7-D0,");
    }
    else if ( ch == d8d15 )
    {
        chanName = QString("D15-D8,");
    }
    else if ( (ch == d0d15) || (ch == la) )
    {
        chanName = QString("D15-D0,");
    }
    else if ( m1 <= ch  && ch <= m4 )
    {
        serviceExecutor::query( maths[ch - m1],
                                servMath::cmd_q_unit_string,
                                chanUnit);

        chanName = QString("MATH%1(%2),").arg( ch-m1 + 1).arg( chanUnit );

    }
    else
    {
        return false;
    }

    serviceExecutor::query(serv_name_storage,
                           MSG_STORAGE_MEM_CSV_TIME, mTimeEn);

    //! write Title
    QTextStream stream(m_pFile);
    if(mTimeEn)
    {
        stream << timeTitle;
    }

    stream<< chanName;

    stream<<QString("t0 = %1s, tInc = %2,")
            .arg(-mHAttr.gnd * mHAttr.inc)
            .arg(mHAttr.inc);

    stream<< "\n";

    return true;
}

bool CCSVFile::writeWaveData(quint8 *pBuf, quint64 size, quint64 totalCount)
{
    if(!checkSplitFile())
    {
        return false;
    }

    //qDebug()<<__FILE__<<__LINE__<<"size:"<<size;
    int progStep  = qMax( totalCount/100, (quint64)1);
    double  inc   = mHAttr.inc;
    double  t     = (-mHAttr.gnd * inc) + mPastCount * inc;

    QTime timeTick;
    timeTick.start();

    QByteArray  horTime   = "";
    QByteArray  outBuf    = "";
    outBuf.reserve(size);

    for(quint64 i =  0; i < size; i ++)
    {
        if(mTimeEn)
        {
            t += inc;
            horTime.setNum(t, 'E', 6);
            outBuf += horTime ;
            outBuf += ",";
        }

        outBuf += mVoltageMap.value(pBuf[i]);
        outBuf += ",\n";

        //!进度条
        if( (mPastCount + i)%progStep ==  0)
        {
            servFile::setProgress(mPastCount/progStep);
        }
    }

    mPastCount += size;
    //qDebug()<<__FILE__<<__LINE__
           //<<"size:"<<outBuf.size()<<"time:"<<timeTick.elapsed()<<"ms";

    timeTick.start();
    if( (outBuf.size()) != m_pFile->write(outBuf) )
    {
        qDebug()<<__FILE__<<__LINE__<<"write fail.";
        return false;
    }
    //qDebug()<<__FILE__<<__LINE__<<"time:"<<timeTick.elapsed()<<"ms";

    return true;
#if 0
    qDebug()<<__FILE__<<__LINE__<<"size:"<<size;

    QTime timeTick;
    timeTick.start();

    quint8      *pChunkBuf  = new quint8[ qMax((quint64)1E6, size*2) ]();
    qint64       usedSize   = 0;
    QByteArray  horTime     = "";

    int progStep  = totalCount/100;

    double  inc   = mHAttr.inc;
    double  t     = (-mHAttr.gnd * inc) + mPastCount * inc;

    for(quint64 i =  0; i < size; i ++)
    {
        t += inc;

        //horTime.setNum(t);
        memcpy(pChunkBuf + usedSize, horTime.data(), horTime.size() );
        usedSize += horTime.size();

        memcpy(pChunkBuf + usedSize, ",", sizeof(",") );
        usedSize += sizeof(",");

        const QByteArray &voltage  = mVoltageMap.value(pBuf[i]);
        memcpy(pChunkBuf + usedSize, voltage.data(), voltage.size() );
        usedSize += voltage.size();

        memcpy(pChunkBuf + usedSize, ",\n", sizeof(",\n") );
        usedSize += sizeof(",\n");

        if( (quint64)usedSize >= size )
        {
//            QByteArray byarr((char*)pChunkBuf,usedSize);
//            qDebug()<<__FILE__<<__LINE__<< byarr;
            if( usedSize != m_pFile->write((char*)pChunkBuf, usedSize) )
            {
                qDebug()<<__FILE__<<__LINE__<<"write fail.";
                delete [] pChunkBuf;
                pChunkBuf = NULL;
                return false;
            }

            usedSize = 0;
        }
        //!进度条
        if( (mPastCount + i)%progStep ==  0)
        {
            servFile::setProgress(mPastCount/progStep);
        }
    }

    mPastCount += size;
    qDebug()<<__FILE__<<__LINE__<<"time:"<<timeTick.elapsed()<<"ms";

    delete [] pChunkBuf;
    pChunkBuf = NULL;

    return true;
#endif
}

/*!
 * \brief CCSVFile::checkSplitFile
 * .检查是否需要拆分文件，fat32单个文件只支持4G，
 * .cvs格式最大放大数据30倍，所以100M原始点拆分一个文件。
 * .拆分原则：先保存上一个文件，然后修改文件名创建新文件，csv文件指针指向新文件。
 * \return bool
 */
bool CCSVFile::checkSplitFile()
{
    const  quint64 subSize    = 100e6;
    static quint64 splitCount = 1;

    if(mPastCount == 0)
    {
        splitCount = 1;
    }

    if( mPastCount >= (subSize * splitCount) )
    {
        splitCount++;

        //!save last file
        if(NULL != m_pFile)
        {
            if(m_pFile->isOpen())
            {
                m_pFile->flush();
                m_pFile->close();
            }
            delete m_pFile;
            m_pFile = NULL;
        }

        //!Create new file
        QRegExp reSuffix("(?:_\\S*)*(?:\\.csv)$",Qt::CaseInsensitive);
        QString suffix = QString("_%1.csv").arg(mPastCount);
        mFileName = mFileName.replace(reSuffix, suffix);

        m_pFile = new QFile(mFileName);
        if(NULL != m_pFile)
        {
            return m_pFile->open(QIODevice::WriteOnly | QIODevice::Text);
        }
        else
        {
            return false;
        }
    }

    return true;
}


