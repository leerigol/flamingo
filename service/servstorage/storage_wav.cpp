#include "storage_wav.h"
#include "servstorage.h"
#include "../servref/servref.h"
#include "storage_api.h"

#include"../servmemory/servmemory.h"

#define BEFORE_RETURN do{ \
                        newFile.close(); \
                        QFile::remove(fname); \
                        return ERR_FILE_SAVE_FAIL;\
                       }while(false)


#define FREE_LIST  do{ \
                       foreach(DsoWfm* wfm, dataList) \
                       { \
                            delete wfm; \
                       } \
                       dataList.clear(); \
                     }while(false)

int CWaveFile::saveWAV(QString &fname, bool b, int ext)
{
    int f = ext;
    int ret = ERR_NONE;

    if( f == -1 )
    {
        serviceExecutor::query( serv_name_storage, MSG_STORAGE_FILETYPE, f );
    }
    //qDebug() << "format=" << f;

    if(f == servFile::FILETYPE_BIN)
    {
        ret = saveBIN(fname, b);
    }
    else if(f == servFile::FILETYPE_CSV)
    {
        ret = saveCSV(fname, b);
    }
    else if(f == servFile::FILETYPE_WFM)
    {
        ret = saveWFM(fname);
    }
    else if(f == servFile::FILETYPE_REF)
    {
        servFile::setProgress(50);
        ret = saveREF(fname);
    }
    else
    {
        ret = ERR_FILE_SAVE_FAIL;
    }

    return ret;
}

///
/// \brief CWaveFile::saveBIN
/// \param fname
/// \param b true for ref, false for wave
/// \return
///
int CWaveFile::saveBIN(QString &fname, bool b)
{
    ///Saving ref as bin format
    if(b)
    {
        servFile::setProgress(50);
        return saveRefAsBin(fname);
    }
    else
    {
        bool waveFrom = servStorage::DATA_SRC_SCREEN;
        serviceExecutor::query(serv_name_storage, MSG_STORAGE_WAVE_DEPTH, waveFrom);
        if(waveFrom == servStorage::DATA_SRC_SCREEN)
        {
            return saveScrAsBin(fname);
        }
        else
        {
            return saveMemAsBin(fname);
        }
    }

    return ERR_NONE;
}

int CWaveFile::saveCSV(QString &fname, bool b)
{
    ///Saving ref as csv format
    if(b)
    {
        servFile::setProgress(50);
        return saveRefAsCSV(fname);
    }
    else
    {
        bool waveFrom = servStorage::DATA_SRC_SCREEN;
        serviceExecutor::query(serv_name_storage, MSG_STORAGE_WAVE_DEPTH, waveFrom);
        if(waveFrom == servStorage::DATA_SRC_SCREEN)
        {
            return saveScrAsCSV(fname);
        }
        else
        {
            return saveMemAsCSV(fname);
        }
    }

    return ERR_NONE;
}

///
/// \brief  Saving ref as bin format
/// \param fname
/// \return
///
int CWaveFile::saveRefAsBin(QString& fname)
{
    CBinFile newFile;

    //1. create bin file
    if( !newFile.create(fname) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    //2. add bin header
    CRefData* pRefData  = servRef::getRefData();
    pRefData->getViewData()->toValue();

    int bpp = sizeof( *(pRefData->getViewData()->getValue()) );
    newFile.setBpp(bpp);
    newFile.setPointCount(pRefData->getViewData()->size());

    if(!newFile.writeBinHeader(1) )
    {
        BEFORE_RETURN;
    }
    //3. add ref header
    if(!newFile.writeWaveHeader(pRefData->getRefID(), BIN_UNIT_V,
                                pRefData->getRefLabel(), *(pRefData->getViewData())))
    {
        BEFORE_RETURN;
    }
    //4. add ref data
    if(!newFile.writeWaveData( *(pRefData->getViewData())))
    {
        BEFORE_RETURN;
    }
    newFile.close();
    return ERR_NONE;

}

int CWaveFile::saveRefAsCSV(QString& fname)
{
    CCSVFile newFile;

    //1. create bin file
    if( !newFile.create(fname) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    //2. add csv header
    CRefData* pRefData  = servRef::getRefData();
    pRefData->getViewData()->setyRefOffset( pRefData->getVOffset());
#if 0
     pRefData->getViewData()->toValue();
#endif
    QList<DsoWfm*> dataList;
    QList<int> chanList;

    chanList.append(pRefData->getRefID());
    dataList.append(pRefData->getViewData());

    QString ref = QString("Ref%1").arg(pRefData->getRefID() - r1 + 1);

    if(!newFile.writeCSVHeader(ref) )
    {
        BEFORE_RETURN;
    }

    //3. add csv header
    if(!newFile.writeWaveData(dataList) )
    {
        BEFORE_RETURN;
    }

    newFile.close();
    return ERR_NONE;
}


int CWaveFile::saveScrAsBin(QString &fname)
{
    CBinFile newFile;

    //count the all active channels
    bool bChan[4] = {false, false, false, false};
    int  nChanCount = 0;
    for(int i=0; i<4; i++)
    {
        QString name = QString("chan%1").arg(i+1);
        serviceExecutor::query(name, MSG_CHAN_ON_OFF, bChan[i]);
        if(bChan[i])
        {
            nChanCount++;
        }
    }

    if(nChanCount == 0)
    {
        return ERR_INVALID_INPUT;
    }

    //find the first active channel
    int nStartChan = 0;
    while( !bChan[nStartChan])
    {
        nStartChan++;
    }



    dsoVert* pData  = dsoVert::getCH( (Chan)(chan1+nStartChan) );
    DsoWfm   wfm;

             pData->getTrace(wfm);
             //wfm.toValue();

     int bpp = sizeof( *(wfm.getValue()) );

     {
         int aboutSize = wfm.size() * nChanCount * bpp;
//         if( laHigh )
//         {
//             aboutSize += plaHigh->size();
//         }

//         if( laLow )
//         {
//             aboutSize += plaLow->size();
//         }
         if( aboutSize >= servStorage::getFreeSpace(fname) )
         {
             return ERR_FILE_SAVE_FAIL;
         }
     }

    //1. create bin file
    if( !newFile.create(fname) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    //2. add bin header

    newFile.setBpp(bpp);
    newFile.setPointCount(wfm.size());

    if(!newFile.writeBinHeader(nChanCount) )
    {
        BEFORE_RETURN;
    }

    int step = 100 / nChanCount;
    int start= step;

    do
    {
        if(bChan[nStartChan])
        {
            servFile::setProgress(start-1);
            start += step;

            int unit = BIN_UNIT_UNKNOW;
            if(pData->getUnit() == Unit_A)
            {
                unit = BIN_UNIT_A;
            }
            else if(pData->getUnit() == Unit_V)
            {
                unit = BIN_UNIT_V;
            }

            //3. add ref header
            if(!newFile.writeWaveHeader((Chan)(chan1+nStartChan),unit, pData->getCHLabel(), wfm))
            {
                BEFORE_RETURN;
            }
            //4. add ref data
            if(!newFile.writeWaveData( wfm ))
            {
                BEFORE_RETURN;
            }
        }
        nStartChan++;
        if(nStartChan < 4)
        {
            if(bChan[nStartChan])
            {
                dsoVert::getCH( (Chan)(chan1+nStartChan) )->getTrace(wfm);
                //wfm.toValue();
            }
        }
        else
        {
            break;
        }

    }while(true);

    newFile.close();
    return ERR_NONE;
}

int CWaveFile::checkMath(int id)
{
    bool b = false;
    QString chans[] = {serv_name_math1,serv_name_math2,serv_name_math3,serv_name_math4};

    if( id < 4 )
    {
        serviceExecutor::query(chans[id], MSG_MATH_EN, b);
        if( b )
        {
            int mathType = 0;
            serviceExecutor::query(chans[id], MSG_MATH_S32MATHOPERATOR, mathType);
            if( mathType == operator_fft )
            {
                b = false;
            }
        }
    }
    return b;
}

int CWaveFile::saveScrAsCSV(QString &fname)
{

    CCSVFile newFile;

    //count the all active channels
    bool bChan[4] = {false, false, false, false};
    int  nChanCount = 0;
    for(int i=0; i<4; i++)
    {
        QString name = QString("chan%1").arg(i+1);
        serviceExecutor::query(name, MSG_CHAN_ON_OFF, bChan[i]);
        if(bChan[i])
        {
            nChanCount++;
        }
    }

    //2. add csv header
    QList<DsoWfm*> dataList;
    QList<int> chanList;

    if(nChanCount > 0)
    {
        //find the first active channel
        int nStartChan = 0;
        while( !bChan[nStartChan])
        {
            nStartChan++;
        }

        for(int i=0; i<nChanCount; i++)
        {
            DsoWfm *wfm = new DsoWfm();
            dsoVert::getCH( (Chan)(chan1+nStartChan) )->getTrace(*wfm);

            dataList.append(wfm);
            chanList.append(nStartChan + chan1);

            nStartChan++;
        }
    }

    for(int i=0; i<4; i++)
    {
        if(checkMath(i))
        {
            DsoWfm *wfm = new DsoWfm();
            dsoVert::getCH( (Chan)(m1+i) )->getTrace(*wfm);

            dataList.append(wfm);
            chanList.append(m1+i);
        }
    }

    //with LA
    bool laHigh = false;
    bool laLow  = false;
    DsoWfm laData;
    DsoWfm *plaHigh = NULL;
    DsoWfm *plaLow  = NULL;
    if( sysHasLA() )
    {
        bool b=false;
        serviceExecutor::query(serv_name_la, MSG_LA_ENABLE, b);
        if( b )
        {
            dsoVert::getCH( la )->getTrace(laData);

            serviceExecutor::query(serv_name_la, MSG_LA_D8D15_ONOFF, laHigh);
            if( laHigh )
            {
                plaHigh = &laData;
            }

            serviceExecutor::query(serv_name_la, MSG_LA_D8D15_ONOFF, laLow);
            if( laLow )
            {
                plaLow = &laData;
            }

        }
    }

    if(  chanList.size() > 0 )
    {
        int aboutSize = (1 + chanList.size()) * dataList.at(0)->size() * 10;
        if( laHigh )
        {
            aboutSize += plaHigh->size();
        }

        if( laLow )
        {
            aboutSize += plaLow->size();
        }
        if( aboutSize >= servStorage::getFreeSpace(fname) )
        {
            return ERR_FILE_SAVE_FAIL;
        }
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    //1. create csv file
    if( !newFile.create(fname) )
    {
        return ERR_FILE_SAVE_FAIL;
    }

    if(!newFile.writeCSVHeader(chanList, laHigh, laLow) )
    {        
        FREE_LIST;
        BEFORE_RETURN;
    }

    if(!newFile.writeWaveData(dataList, plaHigh, plaLow) )
    {
        FREE_LIST;
        BEFORE_RETURN;
    }

    FREE_LIST;
    newFile.close();
    return ERR_NONE;
}

int CWaveFile::saveMemAsBin(QString &FileName)
{
    DsoErr err = servMemory::s_exportChMemory(FileName);
    if( err != ERR_NONE )
    {
        QFile::remove(FileName);
    }

    return err;
}

int CWaveFile::saveMemAsCSV(QString &FileName)
{
    DsoErr err = servMemory::s_exportChMemory(FileName);
    if( err != ERR_NONE )
    {
        QFile::remove(FileName);
    }

    return err;
}

//////////////////////////////////////////////////////
/// \brief CWaveFile::saveREF
/// \param fname
/// \return
///Format:
/// p   type    len  content
/// 1.  u32     4    magic
/// 2.  u32     4    type
/// 3.  u32     4    ver
/// 4.  char    32   model
/// 5.  void    80   reserved
/// 6.  bool    1    on/off
/// 7.  s64      8    vscale     modified
/// 8.  s64      8    voffset    modified
/// 9.   s64     8    hscale     modified
/// 10. s64     8    hoffset    modified
/// 11. float   4    h-t0       original
/// 12. float   4    h-inc      original
/// 13. s64     8    htimebase  modified
/// 14. int      8    ygnd       original
/// 15. float   4    yinc       original
/// 16. bool   1    invert
/// 17. int      4    size
/// 18. char   32   savetime
/// 19. int      4    color index modified
/// 20. char   16   label
/// 21. void    60   reserved
/// 22. char           size data
int CWaveFile::saveREF(QString &fname)
{
    CRefData* pRefData  = servRef::getRefData(-1);

    if(pRefData && pRefData->isValid())
    {

        QFile file(fname);
        if( file.open(QIODevice::WriteOnly) )
        {
            QDataStream out( &file );
            out << servFile::FILE_MAGIC;
            out << servFile::FILE_REF_TYPE;
            out << REF_VER;

            char outBuf[CWaveFile::BUF_SIZE];
            memset(outBuf, 0, BUF_SIZE);
            strcpy(outBuf, servUtility::getSystemModel().toLocal8Bit().data());

            out.writeRawData(outBuf, BUF_SIZE);

            //for reserved
            for(int i=0; i<20; i++)
            {
                out << (quint32)0;
            }
            //on off
            //qDebug() << "Ref saving:on/off=" << pRefData->getOnOff();
            out << pRefData->getOnOff();
            //view setup
            out << pRefData->getVScale();
            out << pRefData->getVOffset();

            out << pRefData->getHScale();
            out << pRefData->getHOffset();

            //original data setup
            out << pRefData->getOrigData()->getllt0();
            out << pRefData->getOrigData()->getlltInc();
            out << pRefData->getOrigData()->gettBase().toDouble();

            out << pRefData->getOrigData()->getyGnd();
            out << pRefData->getOrigData()->realyInc();

            out << pRefData->getOrigData()->getInvert();
            out << pRefData->getOrigData()->size();

            //write model
            memset(outBuf, 0, BUF_SIZE);
            strcpy(outBuf, pRefData->getRefTime().toLocal8Bit().data());
            out.writeRawData(outBuf, BUF_SIZE);

            //write color index
            out << pRefData->getColorIndex();

            //label
            memset(outBuf, 0, BUF_SIZE);
            strcpy(outBuf, pRefData->getRefLabel().toLocal8Bit().data());
            out.writeRawData(outBuf, LAB_SIZE);

            //for reserved
            for(int i=0; i<15; i++)
            {
                out << (quint32)0;
            }

            //ref data
            out.writeRawData((const char*)(pRefData->getOrigData()->getPoint()),
                             sizeof(DsoPoint) *pRefData->getOrigData()->size());

            fsync(file.handle());
            file.close();
            return ERR_NONE;
        }
    }
    return ERR_FILE_SAVE_FAIL;
}

int CWaveFile::loadREF(QString &fname, int id)
{
    CRefData* pRefData  = servRef::getRefData(id);

    QFile file(fname);
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream in( &file );
        char inBuf[BUF_SIZE];

        //checking magic
        quint32 u32Value = 0;

        in >> u32Value;
        if( u32Value != servFile::FILE_MAGIC)
        {
            file.close();
            return ERR_FILE_MAGIC;
        }
        //checking ref file id
        in >> u32Value;
        if( u32Value != servFile::FILE_REF_TYPE)
        {
            file.close();
            return ERR_FILE_TYPE;
        }

        //checking version
        in >> u32Value;
        if( u32Value != REF_VER)
        {
            file.close();
            if(u32Value > REF_VER)
            {
                return ERR_FILE_NEW_VER;
            }
            return ERR_FILE_OLD_VER;
        }

        //checking model
        in.readRawData((char*)inBuf, BUF_SIZE);
        QString qsMode(inBuf);
        if(qsMode.compare(servUtility::getSystemModel()) != 0)
        {
            file.close();
            return ERR_FILE_MODEL;
        }
        ////////////////////////////////////////////////////////
        //jump the reserved
        for(int i=0; i<20; i++)
        {
            in >> u32Value;
        }

        bool bValue;
        in >> bValue;
        //qDebug() << "reading" << b;
        pRefData->setOnOff(bValue);

        //read setup
        qint64 s64Value = 0;

        in >> s64Value;
        if( s64Value == 0 )
        {
             file.close();
             return ERR_FILE_NOT_EXIST;
        }

        pRefData->setVScale(s64Value);

        in >> s64Value;
        pRefData->setVOffset(s64Value);

        in >> s64Value;
        pRefData->setHScale(s64Value);

        in >> s64Value;
        pRefData->setHOffset(s64Value);

        //original data setup
        float f32Value;

        DsoWfm* pData = pRefData->getOrigData();

        qlonglong qllv;
        in >> qllv;
        pData->setllt0( qllv );

        in >> qllv;
        pData->setlltInc( qllv );

        in >> f32Value;
        pData->settBase( f32Value );

        quint64 u64Value = 0;
        //yGnd
       // in >> u32Value;
        in >> u64Value;
        pData->setyGnd(u64Value);

        //qint64 offset = u32Value;
        qint64 offset = u64Value;
       // offset = (offset - adc_center) * u32Value / adc_vdiv_dots;
        offset = (offset - adc_center) * u64Value / adc_vdiv_dots;
        pRefData->setOrigVOffset( offset );


        //yInc
        in >> f32Value;
        qint64 scale = f32Value * 1e6 * 25;
        pData->setyInc( dsoFract(scale,adc_vdiv_dots),
                        DsoReal(1,E_N6));

        pRefData->setOrigVScale(scale);

        in >> bValue;
        pData->setInvert(bValue);

        in >> u32Value;
        pData->init(u32Value);

        int size = u32Value;

        //Reading saved time
        in.readRawData((char*)inBuf, BUF_SIZE);
        if(inBuf[0] != 0)
        {
            pRefData->setRefTime(QString(inBuf));
        }

        //read color index
        int color = 0;
        in >> color;
        if(color>=0 && color<servRef::REF_COLOR_ALL)
        {
            pRefData->setColorIndex(color);
        }
        else
        {
            pRefData->setColorIndex(servRef::REF_COLOR_BLUE);
        }

        //label
        memset(inBuf, 0, BUF_SIZE);
        in.readRawData((char*)inBuf, LAB_SIZE);
        pRefData->setRefLabel(QString(inBuf));

        ////////////////////////////////////////////////////
        //jump the reserved
        for(int i = 0; i < 15; i++)
        {
            in >> u32Value;
        }

        //reading data
        in.readRawData((char*)(pData->getPoint()),
                       sizeof(DsoPoint)* size);

        pData->setRange(0,size);
        //bug 2018 by zy
        pData->toValue();
        pRefData->setViewData(pData);

        pRefData->setValid(true);


        file.close();
        return ERR_NONE;
    }
    return ERR_FILE_NOT_EXIST;
}

int CWaveFile::saveWFM(QString &fname)
{
   return servMemory::s_exportAllMemory(fname, 0, 0);
}

int CWaveFile::loadWFM(QString &fname)
{
    qDebug() << "Implement your loading for" << fname;

    return servMemory::s_importAllMemory(fname);
}


