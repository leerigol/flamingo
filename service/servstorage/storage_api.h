#ifndef _FILE_PROC_H
#define _FILE_PROC_H

#include <QThread>
#include "../../include/dsotype.h"
#include "../servgui/servgui.h"     //! gui ctrls

class CFileThread;

class servFile : public QObject
{
    Q_OBJECT

public:
    //!!!!!Attention.
    //! You MUST Modify the filetype of storage.xls when the following modified
    enum enFileType
    {
//image
        FILETYPE_PNG = 0,
        FILETYPE_BMP,
        FILETYPE_JPG,
        FILETYPE_TIF,
        FILETYPE_GIF,
//wave
        FILETYPE_BIN = 5,
        FILETYPE_CSV,
        FILETYPE_WFM,
        FILETYPE_REF,
        FILETYPE_STP,

        FILETYPE_PF  = 10,
        FILETYPE_HTM,
        FILETYPE_ARB,
        FILETYPE_REC,
        FILETYPE_TXT,


        FILETYPE_GEL = 15, /* Update package */
        FILETYPE_DAT,
        FILETYPE_ANY,      // *.*
        All_FILETYPE,
    };
    enum Operation
    {
        ProcIdle   = 0xabcd,
        ProcSaving,
        ProcLoading,
        ProcCopying,
        ProcUpgrading,
        ProcFormating,
        ProcSnaping,
        ProcPulling,
        ProcRestoring,
        ProcSaveSTP,
        ProcSync,
    };

    const static quint32 FILE_MAGIC        =   0xAABBCCDD;
    const static quint32 FILE_REF_TYPE     =   0xFFFF0000;
    const static quint32 FILE_PF_TYPE      =   0xFFFFF000;
    const static quint32 FILE_STP_TYPE     =   0xFFFFFE00;

public:
        servFile();
        void rst();

        bool  getRun();
        CFileThread* getThread();

public:
    //open for progress bar
    static void initProgress(const QString &,
                             const QString& title = QString(""),
                             int count = 10);

    static void setProgress(int value = -1,
                            const QString& info = QString(""));

    static void hideProgress();
    static void sync2Flash(int nRet = ERR_NONE);

    static int  getStep()              { return m_nStep;}
public:
    int         save(int fmt, int ext = -1);
    int         save(QString&,int fmt, int ext=-1);

    int         load(int fmt);
    int         load(QString&,int);

    int         snap(QString&,int);
    int         mail(QString&);
    int         sync(int);

    int         copy(const QString&, const QString&,
                     const QStringList& ext = QStringList("*"));
    int         upgrade(const QString&);
    int         diskFormat();
    int         restoreDefault();
    int         webPull(QString);
public:

    DsoErr      setCaller(int id)      { m_nCallerID = id; return ERR_NONE;}
    int         getCaller(void)        { return m_nCallerID;}
    Operation   getMethod(void)        { return m_nMethod;}


    int         saveFile(void);
    int         snapFile(void);
    int         saveSTP(void);
    int         copyFile(const QString&, const QString&);
    int         copyFolder(const QString&, const QString&, const QStringList&);
    //load
    int         loadFile(void);

    QString     m_srcPath;
    QString     m_dstPath;
    QStringList m_fileExt;

private:
    int         saveARB(void);
    int         saveREC(void);
    int         saveTXT(QString &);

    int         saveMSK(void);
    int         saveMSK_PF(void);
    int         saveMSK_CSV(void);
    int         saveHTM(void);

    int         saveSEARCH(QString &fname);
    int         saveFFTSEARCH(QString &fname);
    int         saveRECORD(QString &fname);

    int         loadPF(void);
    int         loadARB(void);

private:
    int         m_nFormat;
    int         m_nFormatExt;//csv,bin,wfm
    int         m_nCallerID;

    QString     m_FilePathName;
    Operation   m_nMethod;
    CFileThread*    m_pThread;
    QThread*    m_pWebPull;
    static int  m_nStep;

};


class CFileThread : public QThread
{
    Q_OBJECT

public:
    CFileThread( QObject *parent );
    bool    getRun();
protected:
    void run();

private:
    int         doUpgradeGEL(const QString&);
    int         doDiskFormat();
    int         doRestoreDefault();
    bool        bRunning;
    servFile*   m_pFileProc;
};

class CWebControlThread : public QThread
{
    Q_OBJECT

public:
    CWebControlThread( QObject *parent );

protected:
    void run();

private:
    servFile*   m_pFileProc;
};
#endif // FILESAVER_H

