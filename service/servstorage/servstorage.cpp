#include <unistd.h>
#include "../../service/servvertmgr/servvertmgr.h"
#include "../service_msg.h"
#include "../servquick/servquick.h"
#include "../servhelp/servhelp.h"
#include "../servutility/servinterface/servInterface.h"
#include "servstorage.h"

IMPLEMENT_CMD( serviceExecutor, servStorage  )

start_of_entry()

//!sys msg

on_set_void_void(  CMD_SERVICE_RST,                    &servStorage::rst),
on_set_void_void(  CMD_SERVICE_INIT,                   &servStorage::init),
on_set_int_void (  CMD_SERVICE_STARTUP,                &servStorage::startup),

//on_set_int_int  (  CMD_SERVICE_ACTIVE,                 &servStorage::setActive),
on_set_int_int  (  CMD_SERVICE_SUB_ENTER,              &servStorage::onEnterSubMenu),
on_set_int_int  (  CMD_SERVICE_SUB_RETURN,             &servStorage::onExitSubMenu),
on_set_int_obj  (  CMD_SERVICE_INTENT,                 &servStorage::onIntent),
on_set_int_int  (  CMD_SERVICE_DEACTIVE,               &servStorage::onDeActive ),

on_set_int_void (  MSG_STORAGE_SAVE,                   &servStorage::onSave),
on_set_int_void (  servStorage::FUNC_STORAGE_SAVE_PRE, &servStorage::doNULL),
on_set_int_void (  servStorage::FUNC_STORAGE_SAVE_ING, &servStorage::doSave),

on_set_int_void (  MSG_STORAGE_LOAD,                   &servStorage::doLoad),
on_get_string_arg( MSG_STORAGE_PRNTSCR,                &servStorage::getImage),
on_get_string_arg( MSG_STORAGE_PULLSCR,                &servStorage::getImagePath),

on_set_int_arg   ( servStorage::FUNC_SAVE_CSV,         &servStorage::scpiSaveCSV),
on_set_int_arg   ( servStorage::FUNC_SAVE_IMG,         &servStorage::scpiSaveIMG),
on_set_int_arg   ( servStorage::FUNC_SAVE_STP,         &servStorage::scpiSaveSetup),
on_set_int_string( servStorage::FUNC_LOAD_STP,         &servStorage::loadSetup),

on_set_int_arg   ( servStorage::FUNC_SAVE_WAV,         &servStorage::scpiSaveWave),

on_set_int_string( servStorage::FUNC_PUSH_SETUP,       &servStorage::pushSetup),
on_get_string    ( servStorage::FUNC_PULL_SETUP,       &servStorage::pullSetup),

on_set_int_string( MSG_STORAGE_PREFIX,                 &servStorage::setPrefixName),
on_get_string    ( MSG_STORAGE_PREFIX,                 &servStorage::getPrefixName),

on_set_int_int(    MSG_STORAGE_IMAGE_FORMAT,           &servStorage::setImageFormat),
on_get_int    (    MSG_STORAGE_IMAGE_FORMAT,           &servStorage::getImageFormat),

on_set_int_bool(   MSG_STORAGE_IMAGE_INVERT,           &servStorage::setInvert),
on_get_bool    (   MSG_STORAGE_IMAGE_INVERT,           &servStorage::getInvert),

on_set_int_bool(   MSG_STORAGE_IMAGE_COLOR,            &servStorage::setColorType),
on_get_bool    (   MSG_STORAGE_IMAGE_COLOR,            &servStorage::getColorType),

on_set_int_bool(   MSG_STORAGE_IMAGE_HEADER,           &servStorage::setImageHeader),
on_get_bool    (   MSG_STORAGE_IMAGE_HEADER,           &servStorage::getImageHeader),

on_set_int_bool(   MSG_STORAGE_IMAGE_FOOTER,           &servStorage::setImageFooter),
on_get_bool    (   MSG_STORAGE_IMAGE_FOOTER,           &servStorage::getImageFooter),

on_set_int_bool(   MSG_STORAGE_DIALOG_VISIBLE,         &servStorage::setDialogHidden),
on_get_bool    (   MSG_STORAGE_DIALOG_VISIBLE,         &servStorage::getDialogVisible),


///////////////////////////////////ABOUT WAVE//////////////////////////////////////////
on_set_int_int(    MSG_STORAGE_CHANNEL,                &servStorage::setChannelSelected),
on_get_int    (    MSG_STORAGE_CHANNEL,                &servStorage::getChannelSelected),

on_set_int_bool(   MSG_STORAGE_WAVE_DEPTH,             &servStorage::setWaveDepth),
on_get_bool    (   MSG_STORAGE_WAVE_DEPTH,             &servStorage::getWaveDepth),

on_set_int_bool(   MSG_STORAGE_OPTION_PARASAVE,        &servStorage::setWaveParaSave),
on_get_bool    (   MSG_STORAGE_OPTION_PARASAVE,        &servStorage::getWaveParaSave),

on_set_int_int(    MSG_STORAGE_WAVE_FORMAT,            &servStorage::setWaveFormat),
on_get_int    (    MSG_STORAGE_WAVE_FORMAT,            &servStorage::getWaveFormat),

on_set_int_int(    MSG_STORAGE_SCR_FORMAT,             &servStorage::setWaveFormat),
on_get_int    (    MSG_STORAGE_SCR_FORMAT,             &servStorage::getWaveFormat),

on_set_int_bool(   MSG_STORAGE_WFM_CHANNEL,            &servStorage::setMemChannel),
on_get_bool    (   MSG_STORAGE_WFM_CHANNEL,            &servStorage::getMemChannel),

on_set_int_bool(   MSG_STORAGE_MEM_CSV_TIME,           &servStorage::setCsvTimeEnab),
on_get_bool    (   MSG_STORAGE_MEM_CSV_TIME,           &servStorage::getCsvTimeEnab),

///////////////////////////////////ABOUT DISK//////////////////////////////////////////
on_set_int_int(    MSG_STORAGE_FILETYPE,               &servStorage::setFileFormat),
on_get_int    (    MSG_STORAGE_FILETYPE,               &servStorage::getFileFormat),

on_set_int_void(   MSG_STORAGE_COPY,                   &servStorage::doCopy),

on_set_int_void(   MSG_STORAGE_RENAME,                 &servStorage::onRename),
on_set_int_string( servStorage::FUNC_STORAGE_RENAME,   &servStorage::doRename),

on_set_int_void(   MSG_STORAGE_NEWFOLDER,              &servStorage::onNewFolder),
on_get_string  (   MSG_STORAGE_NEWFOLDER,              &servStorage::buildFolderName),
on_set_int_string( servStorage::FUNC_STORAGE_NEWFOLDER,&servStorage::doNewFolder),

on_set_int_void(   MSG_STORAGE_SECURITYCLEAR,          &servStorage::onFormatDisk),
on_set_int_void(   servStorage::FUNC_STORAGE_FORMAT,   &servStorage::doFormatDisk),

on_set_int_void(   MSG_RESTORE_DEFAULT,                &servStorage::onRestoreDefault),
on_set_int_void(   servStorage::FUNC_RESTORE_DEFAULT,  &servStorage::doRestoreDefault),

on_set_int_void(   MSG_STORAGE_PASTE,                  &servStorage::onPaste),//CHECK
on_set_int_void(   servStorage::FUNC_STORAGE_PASTE_PRE,&servStorage::doNULL), //CONFIRM
on_set_int_void(   servStorage::FUNC_STORAGE_PASTE,    &servStorage::doPaste),

on_set_int_void(   MSG_STORAGE_DELETE,                 &servStorage::onDelete),
on_set_int_void(   servStorage::FUNC_STORAGE_DELETE,   &servStorage::doDelete),

///////////////////////////////////DiskManager////////////////////////////////////////
on_set_int_string( MSG_STORAGE_PATHNAME,               &servStorage::setPathName),
on_get_string  (   MSG_STORAGE_PATHNAME,               &servStorage::getPathName),

on_set_int_string( MSG_STORAGE_FILENAME,               &servStorage::setFileName),
on_get_string  (   MSG_STORAGE_FILENAME,               &servStorage::getFileName),

on_set_int_bool(   MSG_STORAGE_AUTONAME,               &servStorage::setAutoName),
on_get_bool(       MSG_STORAGE_AUTONAME,               &servStorage::getAutoName),

on_get_string  (   MSG_STORAGE_FILEPATH,               &servStorage::getFilePathName),

on_set_int_int (   servStorage::FUNC_STORAGE_INDEX,    &servStorage::setFileIndex),
on_get_pointer (   servStorage::FUNC_STORAGE_PTR,      &servStorage::getFileList),
on_set_int_void(   servStorage::FUNC_STORAGE_REDRAW,   &servStorage::doNULL),
on_set_void_int(   servStorage::FUNC_STORAGE_PUSH,     &servStorage::onTunePush),

on_set_int_string( MSG_STORAGE_USB_INSERT,             &servStorage::onInsertUSB),
on_set_int_string( MSG_STORAGE_USB_REMOVE,             &servStorage::onRemoveUSB),

///////////////////////////////////Sub save//////////////////////////////////////////

on_set_int_bool(   MSG_CHECKING_FIRMWARE,              &servStorage::setCheckFirmware),//check
on_get_bool(       MSG_CHECKING_FIRMWARE,              &servStorage::getCheckFirmware),

on_set_int_int (   servStorage::FUNC_UPGRADE_USB,      &servStorage::updateFromUSB),//updateFromUSB
on_set_int_string( servStorage::FUNC_UPGRADE_NET,      &servStorage::updateFromNET),

on_set_int_void(   servStorage::FUNC_UPGRADE_RESULT,   &servStorage::doNULL),
on_get_int     (   servStorage::FUNC_UPGRADE_RESULT,   &servStorage::getUpgradeResult),

on_get_int     (   servStorage::FUNC_SUB_MENU,         &servStorage::getSubMenu),
on_get_int     (   servStorage::FUNC_RETURN_MENU,      &servStorage::getReturnMenu),

on_get_int     (   servStorage::FUNC_THREAD_PROC,      &servStorage::getThreadProc),
on_set_int_int (   servStorage::FUNC_THREAD_PROC,      &servStorage::setThreadProc),

on_set_int_int (   servStorage::FUNC_PROC_STATUS,      &servStorage::setProcStatus),
on_get_int     (   servStorage::FUNC_PROC_STATUS,      &servStorage::getProcStatus),

on_set_int_int(    servStorage::FUNC_QUICK_SAVE_IMG,   &servStorage::quickSaving),
on_set_int_int(    servStorage::FUNC_QUICK_SAVE_WAV,   &servStorage::quickSaving),
on_set_int_int(    servStorage::FUNC_QUICK_SAVE_STP,   &servStorage::quickSaving),

on_set_int_int (   MSG_STORAGE_RESULT,                 &servStorage::onResult),
on_set_int_int (   servStorage::FUNC_WEB_PULL,         &servStorage::onWebPull),

///////////////////////////////////scpi//////////////////////////////////////////
on_get_int     (   servStorage::FUNC_STORAGE_FILETYPE,      &servStorage::getFileType),
on_set_int_int (   servStorage::FUNC_STORAGE_FILETYPE,      &servStorage::setFileType),

on_set_void_void (  servStorage::FUNC_SPY_LICENSE,           &servStorage::spyLicenseOpt),

end_of_entry()


QHash<int,tNameInfo*>  servStorage::m_NameInfo;

//how many disk fat
QList< QPair<QString, QString> > servStorage::m_hFat;

//current file format
int  servStorage::m_nFileFormat = servFile::FILETYPE_PNG;

/* 10 9*/
const static quint64 MAX_FILE_COUNT = 9999999999;

#define DebugPrint() //qDebug() << __FUNCTION__ << __LINE__

servStorage::servStorage(QString name, ServiceId eId):serviceExecutor( name, eId)
{
    serviceExecutor::baseInit();

    ISerial::mVersion = 0xd;
}

int servStorage::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("m_nImageFormat"),     m_nImageFormat);
        stream.read(QStringLiteral("m_nWaveFormat"),      m_nWaveFormat);

        stream.read(QStringLiteral("m_bColorType"),       m_bColorType);
        stream.read(QStringLiteral("m_bInvert"),          m_bInvert);

        stream.read(QStringLiteral("header"),             m_bImageHeader);
        stream.read(QStringLiteral("footer"),             m_bImageFooter);

        stream.read(QStringLiteral("m_bAutoName"),        m_bAutoName);

        stream.read(QStringLiteral("m_nWaveDepth"),       m_nWaveDepth);
    }
    else
    {
        rst();
    }

    return 0;
}

int servStorage::serialOut(CStream &stream, unsigned char &ver)
{
    ver = mVersion;
    //Common paramaters
    stream.write(QStringLiteral("m_nImageFormat"),     getImageFormat());
    stream.write(QStringLiteral("m_nWaveFormat"),      getWaveFormat());

    stream.write(QStringLiteral("m_bColorType"),       getColorType());
    stream.write(QStringLiteral("m_bInvert"),          getInvert());

    stream.write(QStringLiteral("header"),             m_bImageHeader);
    stream.write(QStringLiteral("footer"),             m_bImageFooter);

    stream.write(QStringLiteral("m_bAutoName"),        m_bAutoName);
    stream.write(QStringLiteral("m_nWaveDepth"),       m_nWaveDepth);

    return 0;
}

void servStorage::init()
{
    //create file ext
    {
        m_NameInfo[servFile::FILETYPE_PNG] = new tNameInfo(QString("png"));
        m_NameInfo[servFile::FILETYPE_JPG] = new tNameInfo(QString("jpg"));
        m_NameInfo[servFile::FILETYPE_BMP] = new tNameInfo(QString("bmp"));
        m_NameInfo[servFile::FILETYPE_TIF] = new tNameInfo(QString("tif"));
        m_NameInfo[servFile::FILETYPE_GIF] = new tNameInfo(QString("gif"));

        m_NameInfo[servFile::FILETYPE_BIN] = new tNameInfo(QString("bin"));
        m_NameInfo[servFile::FILETYPE_WFM] = new tNameInfo(QString("wfm"));
        m_NameInfo[servFile::FILETYPE_CSV] = new tNameInfo(QString("csv"));
        m_NameInfo[servFile::FILETYPE_REF] = new tNameInfo(QString("ref"));
        m_NameInfo[servFile::FILETYPE_STP] = new tNameInfo(QString("stp"));

        m_NameInfo[servFile::FILETYPE_TXT] = new tNameInfo(QString("txt"));
        m_NameInfo[servFile::FILETYPE_ARB] = new tNameInfo(QString("arb"));
        m_NameInfo[servFile::FILETYPE_PF]  = new tNameInfo(QString("pf"));
        m_NameInfo[servFile::FILETYPE_REC] = new tNameInfo(QString("rec"));
        m_NameInfo[servFile::FILETYPE_HTM] = new tNameInfo(QString("htm"));

        m_NameInfo[servFile::FILETYPE_DAT] = new tNameInfo(QString("dat"));
        m_NameInfo[servFile::FILETYPE_GEL] = new tNameInfo(QString("GEL"));
        m_NameInfo[servFile::FILETYPE_ANY] = new tNameInfo(QString("*"));


        m_pFileProc = new servFile();
        Q_ASSERT(m_pFileProc != NULL);
        m_pFileProc->setCaller(this->mpItem->servId);
    }

    initUsbMedia();

    if(m_hFat.size()>1)
    {
        //usb
        QString path = m_hFat.at(1).second;
        post(E_SERVICE_ID_STORAGE, MSG_STORAGE_USB_INSERT, path);
    }

    QString s = "";
    if ( checkUpgradeGEL(s) )
    {
        post(E_SERVICE_ID_HELP, servHelp::cmd_usb_status, true);
    }

    setThreadProc( 0 );

    mUiAttr.setVisible(MSG_STORAGE_IMAGE_FOOTER,false);

    ms_bIsUKeyReady = checkUKey();
}

void servStorage::rst()
{
     m_nImageFormat  = servFile::FILETYPE_PNG;
     m_nFileFormat   = servFile::FILETYPE_PNG;
     m_nWaveFormat   = 0;
     m_nWaveDepth    = DATA_SRC_SCREEN;
     m_nWaveParaSave = false;

     m_nChannel      = chan1;

     m_bCsvTime      = false;
     m_bMemWfmCh     = false;

     m_bColorType    = false;
     m_bInvert       = false;
     m_bImageHeader  = true;
     m_bImageFooter  = false;

     m_bCopiedFile   = false;

     m_bAutoName     = true;

     m_bDialogClose  = false;
     m_nPathSeries   = 0;
     m_nNextNumber   = 0;
     m_nDigitCount   = 0;

     m_Prefix        = "RigolDS";

     m_PreFixList.clear();
     for(int i=servFile::FILETYPE_PNG; i< servFile::FILETYPE_ANY; i++)
     {
         m_PreFixList.insert(i,"RigolDS");
     }
     ///default file info
     m_DotDot.setFile("..");
     m_UserPath.setFile(STORAGE_USER_PATH);
     m_PathName = STORAGE_USER_PATH;

     //file index start from 0
     m_nFileIndex    = 0;

     m_bAutoChecking = false;
     m_nUpgradeResult= 0;
     m_bUpgradeMedia = false;

     m_nRetuenMenu   = 0;
     m_fileType = 0;
}

int servStorage::startup()
{
    bool bMso = sysCheckLicense(OPT_MSO);

    for(int src=d0; src<=d15; src++)
    {
        mUiAttr.setEnableVisible(MSG_STORAGE_CHANNEL, src, bMso,bMso);
    }

    mUiAttr.setEnableVisible(MSG_STORAGE_WFM_CHANNEL, false, false);

    return ERR_NONE;
}

void servStorage::registerSpy()
{
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Active , FUNC_SPY_LICENSE);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Exp    , FUNC_SPY_LICENSE);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Invalid, FUNC_SPY_LICENSE);
}

int servStorage::onDeActive(int)
{
    setdeActive();
    return ERR_NONE;
}

DsoErr servStorage::onEnterSubMenu(int msg)
{
    QList<int> nExt;

    mUiAttr.setEnable(MSG_STORAGE_FILETYPE,true);

    int f = m_nFileFormat;
    switch(msg)
    {
        case MSG_STORAGE_SAVE_IMAGE:
        {
            f = m_nImageFormat;
            m_nDoProc = FUNC_SAVE_IMG;
        }
        break;

        case MSG_STORAGE_SAVE_WAVE:
        {
            f = m_nWaveFormat + servFile::FILETYPE_BIN;
            m_nDoProc = FUNC_SAVE_WAV;
        }
        break;

        case MSG_STORAGE_LOAD_WAVE:
        {
            m_nDoProc = FUNC_LOAD_WAV;
            f = servFile::FILETYPE_WFM;
            mUiAttr.setEnable(MSG_STORAGE_FILETYPE,false);
        }
        break;

        case MSG_STORAGE_SAVE_SETUP:
        case MSG_STORAGE_LOAD_SETUP:
        {
            if(msg == MSG_STORAGE_SAVE_SETUP)
            {
                m_nDoProc = FUNC_SAVE_STP;
            }
            else
            {
                m_nDoProc = FUNC_LOAD_STP;
            }
            f = servFile::FILETYPE_STP;
        }
        break;

        case MSG_STORAGE_SUB_SAVE:
        {
            if(m_nDoProc == FUNC_SAVE_REF)
            {
                f = servFile::FILETYPE_BIN;
                nExt.append(f);
                nExt.append(servFile::FILETYPE_CSV);
                nExt.append(servFile::FILETYPE_REF);
            }
            else if(m_nDoProc == FUNC_SAVE_GIF)
            {
                f = servFile::FILETYPE_GIF;
            }
            else if(m_nDoProc == FUNC_SAVE_MSK)
            {
                f = servFile::FILETYPE_PF;
                //nExt.append(f);
            }
            else if(m_nDoProc == FUNC_EXPT_MSK)
            {
                f = servFile::FILETYPE_CSV;
                nExt.append(f);
                nExt.append(servFile::FILETYPE_TXT);
                nExt.append(servFile::FILETYPE_HTM);
            }
            else if(m_nDoProc == FUNC_SAVE_DEC)//
            {
                f = servFile::FILETYPE_CSV;
                //nExt.append(f);
            }
            else if(m_nDoProc == FUNC_SAVE_DECDAT)//
            {
                f = servFile::FILETYPE_CSV;
                //nExt.append(f);
                //nExt.append(servFile::FILETYPE_DAT);
            }
            else if(m_nDoProc == FUNC_SAVE_ARB)
            {
                f = servFile::FILETYPE_ARB;
                //nExt.append(f);
            }
            else if(m_nDoProc == FUNC_SAVE_SEARCH)
            {                
                 f = servFile::FILETYPE_CSV;
                 //nExt.append(f);
            }
            else if(m_nDoProc == FUNC_SAVE_FFT)
            {
                 f = servFile::FILETYPE_CSV;
                 //nExt.append(f);
            }
            else if(m_nDoProc == FUNC_SAVE_REC)
            {
                 f = servFile::FILETYPE_WFM;
                 //nExt.append(f);
            }
        }
        break;

        case MSG_STORAGE_SUB_LOAD:
        {            
            if(m_nDoProc == FUNC_LOAD_REF)
            {
                f = servFile::FILETYPE_REF;
            }
            else if(m_nDoProc == FUNC_LOAD_ARB)
            {
                f = servFile::FILETYPE_ARB;
            }
            else if(m_nDoProc == FUNC_LOAD_MSK)
            {
                f = servFile::FILETYPE_PF;
            }//load any file
            else if(m_nDoProc == FUNC_LOAD)
            {
                f = servFile::FILETYPE_ANY;
            }
            else if(m_nDoProc == FUNC_LOAD_ATT)
            {
                f = servFile::FILETYPE_ANY;
            }
            else if(m_nDoProc == FUNC_LOAD_SCR )
            {
                f = servFile::FILETYPE_PNG;
                nExt.append(f);
                nExt.append(servFile::FILETYPE_BMP);
                nExt.append(servFile::FILETYPE_JPG);
                nExt.append(servFile::FILETYPE_TIF);
            }
        }
        break;

        case MSG_STORAGE_DISK_MANAGE:
        {
            f = servFile::FILETYPE_ANY;
            m_nDoProc = FUNC_STORAGE_DISK;

            setPasteEnable(m_bCopiedFile);
            setRenameEnable(false);
            setCopyEnable(false);
            setFormatEnable( true );
        }
        break;

        case MSG_STORAGE_OPTION:
        {
            m_nDoProc = FUNC_STORAGE_OPTION;
            return ERR_NONE;
        }
        break;

        default:
            return ERR_NONE;
    }

    //reset the pathname
    m_PathName = STORAGE_USER_PATH;

    //can not create new dir
    setNewEnable(false);

    //return to root dir
    m_nPathSeries = 0;
    m_nFileFormat = f;

    if(m_nDoProc & FUNC_LOAD )
    {
        m_FileName = getFileExt(f);
        setLoadEnable( false );
    }
    else
    {
        if( m_PreFixList.contains(f))
        {
            m_Prefix = m_PreFixList[f];
        }
    }
    updateList(f);
    m_nFileIndex=0;

    if( nExt.size() > 0)
    {
        for(int f=servFile::FILETYPE_PNG; f<=servFile::FILETYPE_ANY; f++)
        {
            if( nExt.contains(f))
            {
                mUiAttr.setVisible(MSG_STORAGE_FILETYPE, f, true);
            }
            else
            {
                mUiAttr.setVisible(MSG_STORAGE_FILETYPE, f, false);
            }
        }
        nExt.clear();
    }
    else
    {
        bool en = (msg == MSG_STORAGE_DISK_MANAGE);
        mUiAttr.setEnable(MSG_STORAGE_FILETYPE, en);
        if( en )
        {
            for(int f=servFile::FILETYPE_PNG; f<=servFile::FILETYPE_ANY; f++)
            {
                mUiAttr.setVisible(MSG_STORAGE_FILETYPE, f, true);
            }
        }
    }
    setuiChange(MSG_STORAGE_FILETYPE);
    return ERR_NONE;
}

DsoErr servStorage::onExitSubMenu(int msg)
{
    m_nRetuenMenu = msg;
    if( msg != MSG_STORAGE_MEM_CSV_MORE)
    {
        m_stFiles.clear();
    }

    return ERR_NONE;
}

DsoErr servStorage::onIntent(CIntent *pIntent)
{
    if(pIntent->mMsg == MSG_STORAGE_SUB_SAVE ||
       pIntent->mMsg == MSG_STORAGE_SUB_LOAD)
    {
        m_nDoProc = pIntent->mIntent;
        if(pIntent->mMsg == MSG_STORAGE_SUB_SAVE)
        {
           if(m_nDoProc == FUNC_SAVE_REF)
           {
               m_nFileFormat = servFile::FILETYPE_BIN;
           }
           else if(m_nDoProc == FUNC_SAVE_GIF)
           {
               m_nFileFormat = servFile::FILETYPE_GIF;
           }
           else if(m_nDoProc == FUNC_SAVE_MSK)
           {
               m_nFileFormat = servFile::FILETYPE_PF;
           }
           else if(m_nDoProc == FUNC_EXPT_MSK)
           {
               m_nFileFormat = servFile::FILETYPE_CSV;
           }
           else if(m_nDoProc == FUNC_SAVE_DEC)//
           {
               m_nFileFormat = servFile::FILETYPE_CSV;
           }
           else if(m_nDoProc == FUNC_SAVE_DECDAT)//
           {
               m_nFileFormat = servFile::FILETYPE_CSV;
           }
           else if(m_nDoProc == FUNC_SAVE_DG)//
           {
               m_nFileFormat = servFile::FILETYPE_ARB;
           }
        }
        else
        {
           if(m_nDoProc == FUNC_LOAD_REF)
           {
               m_nFileFormat = servFile::FILETYPE_REF;
           }
           else if(m_nDoProc == FUNC_LOAD_ARB)
           {
               m_nFileFormat = servFile::FILETYPE_ARB;
           }
           else if(m_nDoProc == FUNC_LOAD_MSK)
           {
               m_nFileFormat = servFile::FILETYPE_PF;
           }//load any file
           else if(m_nDoProc == FUNC_LOAD)
           {
               m_nFileFormat = servFile::FILETYPE_ANY;
           }
           else if(m_nDoProc == FUNC_LOAD_ATT)
           {
               m_nFileFormat = servFile::FILETYPE_ANY;
           }
           else if(m_nDoProc == FUNC_LOAD_SCR )
           {
               m_nFileFormat = servFile::FILETYPE_ANY;
           }
        }
    }
    else
    {
        return ERR_MENU_JUMP_INVALID;
    }
    return ERR_NONE;
}

int servStorage::getSubMenu()
{
    return m_nDoProc;
}

int servStorage::getReturnMenu()
{
    return m_nRetuenMenu;
}

///
/// \brief servStorage::setImageFormat
/// \param fmt
/// \return
///
DsoErr servStorage::setImageFormat(int fmt)
{
    m_nImageFormat = fmt;
    setFileFormat(fmt);
    return ERR_NONE;
}

///
/// \brief servStorage::servStorage::getImageFormat
/// \return
///
int servStorage::getImageFormat()
{
    return m_nImageFormat;
}

///
/// \brief servStorage::setFileFormat
/// \param f
/// \return
///
DsoErr servStorage::setFileFormat(int f)
{
    m_nFileFormat = f;
    updateList(f);
    return ERR_NONE;
}

///
/// \brief servStorage::getFileFormat
/// \return
///
int servStorage::getFileFormat()
{
    return m_nFileFormat;
}


/**
 * @brief .Customed user file name
 * We can get the start number from the name,for example
 * if 'rigolds100' is inputed. then the next file name is 'rigolds101'
 *
 * Max size =8
 * @param f
 * @return
 */
DsoErr servStorage::setPrefixName(QString& f)
{    
    int size = f.size();
    if( size <= 0 )
    {
        return ERR_NONE;
    }

    /*RESERVED for redraw */
    if(f.compare(STORAGE_REVERVED) == 0)
    {
        return ERR_NONE;
    }

    //QByteArray ba = f.toLocal8Bit();
    //qDebug() << "size=" << getStrSpace(f);
    chkPrefixName(f);

    m_FileName.clear();

    buildFileName(m_nFileFormat);

    return ERR_NONE;
}

void servStorage::chkPrefixName(QString s)
{
    QString start   = "";
    QString input   = s;
    int endIndex    = s.size() - 1;
    bool    next    = true;


    m_Prefix.clear();
    //check prefix and start number
    for(; endIndex>=0; endIndex--)
    {
        if(next && list_at(input,endIndex).isDigit())
        {
            start.insert(0, list_at(input,endIndex));
        }
        else
        {
            next = false;
            m_Prefix.insert(0, list_at(input,endIndex));
        }
    }

    m_nNextNumber = 0;
    //rigol001-> numcount  = 3
    //rigol12 -> numcount  = 2
    m_nDigitCount  = start.size();
    if(m_nDigitCount > 0)
    {
        m_nNextNumber = start.toLongLong();
    }


    foreach(tNameInfo* name, m_NameInfo)
    {
        name->counter = m_nNextNumber;
    }


    if( m_PreFixList.contains(m_nFileFormat))
    {
        m_PreFixList[m_nFileFormat] = m_Prefix;
    }
}

QString servStorage::getPrefixName()
{
    setuiMaxLength( MAX_FILENAME_LEN );
    if( m_FileName.size() == 0 )
    {
        QString fn = QString("%1%2").arg(m_Prefix).arg(m_nNextNumber);
        return  fn;
    }
    else
    {
        QStringList prefix = m_FileName.split(".");
        return list_at(prefix,0);
    }
}

/**
 * @brief servStorage::setFileName
 *
 * @param f
 * @return
 */
DsoErr servStorage::setFileName(QString& f)
{
    m_FileName = f;
    return ERR_NONE;
}

///
/// \brief servStorage::getFileName
/// \return
///
QString servStorage::getFileName()
{
    return m_FileName;
}

DsoErr servStorage::setAutoName(bool b)
{
    m_bAutoName = b;
    return ERR_NONE;
}

bool servStorage::getAutoName()
{
    return m_bAutoName;
}

///
/// \brief servStorage::setPathName
/// \param f
/// \return
///
DsoErr servStorage::setPathName(const QString& f)
{
    if(m_PathName.compare(f) != 0)
    {
        m_PathName = f;
        DebugPrint();
        m_NameInfo[m_nFileFormat]->counter = m_nNextNumber;
        buildFileName(m_nFileFormat);
    }
    return ERR_NONE;
}

///
/// \brief servStorage::getPathName
/// \return
///
QString servStorage::getPathName()
{
    return m_PathName;
}

///
/// \brief servStorage::getFilePathName
/// \return
///
QString servStorage::getFilePathName()
{
    return m_PathName + "/" + m_FileName;
}


///
/// \brief servStorage::setWaveDepth
/// \param f
/// \return
///
DsoErr servStorage::setWaveDepth( bool f )
{
   m_nWaveDepth = f;

   if(DATA_SRC_SCREEN == m_nWaveDepth)
   {
       if( !(m_nWaveFormat == 0 || m_nWaveFormat == 1) )
       {
           m_nFileFormat = servFile::FILETYPE_BIN;
           m_nWaveFormat = 0;
       }
   }
   return ERR_NONE;
}

///
/// \brief servStorage::getWaveDepth
/// \return
///
bool servStorage::getWaveDepth()
{
   return  m_nWaveDepth;
}

///
/// \brief servStorage::setWaveParaSave
/// \param f
/// \return
///
DsoErr servStorage::setWaveParaSave( bool f )
{
   m_nWaveParaSave = f;
   return ERR_NONE;
}

///
/// \brief servStorage::getWaveParaSave
/// \return
///
bool servStorage::getWaveParaSave()
{
   return  m_nWaveParaSave;
}
///
/// \brief servStorage::setWaveFormat
/// \param f
/// \return
///
DsoErr servStorage::setWaveFormat( int f )
{
   m_nWaveFormat = f;

   DebugPrint();
   setFileFormat(f + servFile::FILETYPE_BIN);

   return ERR_NONE;
}

/// \brief servStorage::getWaveFormat
/// \return
///
int servStorage::getWaveFormat()
{
   return  m_nWaveFormat;
}


///
/// \brief servStorage::setParamSave
/// \param b
/// \return
///
DsoErr servStorage::setParamSave(bool b)
{
    m_bParamSave = b;
    return ERR_NONE;
}

///
/// \brief servStorage::getParamSave
/// \return
///
bool servStorage::getParamSave()
{
    return m_bParamSave;
}


///
/// \brief servStorage::setInvert
/// \param b
/// \return
///For Image
DsoErr servStorage::setInvert(bool b)
{
    m_bInvert = b;
    return ERR_NONE;
}

///
/// \brief servStorage::getInvert
/// \return
///
bool servStorage::getInvert()
{
    return m_bInvert;
}

///
/// \brief servStorage::setColorType
/// \param b
/// \return
///For Image
DsoErr servStorage::setColorType(bool b)
{
    m_bColorType = b;
    return ERR_NONE;
}

///
/// \brief servStorage::getColorType
/// \return
///
bool servStorage::getColorType()
{
    return m_bColorType;
}

DsoErr servStorage::setImageHeader(bool b)
{
    m_bImageHeader = b;
    return ERR_NONE;
}

bool servStorage::getImageHeader()
{
    return m_bImageHeader;
}

DsoErr servStorage::setImageFooter(bool b)
{
    m_bImageFooter = b;
    return ERR_NONE;
}

bool servStorage::getImageFooter()
{
    return m_bImageFooter;
}

///
/// \brief servStorage::setDialogHidden
/// \param b
/// \return
///
DsoErr servStorage::setDialogHidden(bool b)
{
    m_bDialogClose = b;
    return ERR_NONE;
}

///
/// \brief servStorage::getDialogVisible
/// \return
///
bool servStorage::getDialogVisible()
{
    return m_bDialogClose;
}

///
/// \brief servStorage::setChannelSelected
/// \param opt
/// \return
///
DsoErr servStorage::setChannelSelected( int opt)
{
    m_nChannel = opt;
    return ERR_NONE;
}

///
/// \brief servStorage::getChannelSelected
/// \return
///
int servStorage::getChannelSelected()
{
    for(int dx = d0; dx <= d15; dx++)
    {
        mUiAttr.setVisible(MSG_STORAGE_CHANNEL, dx, false);
    }

    return m_nChannel;
}

DsoErr servStorage::setMemChannel(bool ch)
{
    m_bMemWfmCh = ch;
    return ERR_NONE;
}

bool servStorage::getMemChannel()
{
    return m_bMemWfmCh;
}

DsoErr servStorage::setCsvTimeEnab(bool b)
{
    m_bCsvTime = b;
    return ERR_NONE;
}

bool servStorage::getCsvTimeEnab()
{
    return m_bCsvTime;
}


bool servStorage::setThreadProc(int proc)
{
    if( m_nThreadProc == servFile::ProcUpgrading &&
            proc == servFile::ProcPulling &&
            proc != servFile::ProcIdle)
    {
        return false;
    }    
    m_nThreadProc = proc;
    return true;
}

int servStorage::getThreadProc()
{
    return m_nThreadProc;
}

int servStorage::setProcStatus(int s)
{
    m_nProcStatus = s;
    return ERR_NONE;
}

int servStorage::getProcStatus()
{
    return m_nProcStatus;
}

///
/// \brief servStorage::buildFileName
/// \param fmt
///
void servStorage::buildFileName(int fmt)
{
    if(!m_NameInfo.contains(fmt))
    {
        fmt = servFile::FILETYPE_PNG;
    }
    //QString tmp;
    QString fn = "";
    tNameInfo* pNameInfo =  m_NameInfo[fmt];
    Q_ASSERT(pNameInfo != NULL);

    /* reference as x6000 */
    if( pNameInfo->counter >= MAX_FILE_COUNT )
    {
        pNameInfo->counter = 0;
    }

    if ( m_bAutoName )
    {
        QString counter  = "";
        QString filePath = "";


        for(; pNameInfo->counter < MAX_FILE_COUNT; pNameInfo->counter++)
        {
            counter = QString("%1").arg(pNameInfo->counter);
            if(counter.size() < m_nDigitCount)
            {
                fn = QString("%1%2").arg(m_Prefix).arg(pNameInfo->counter,
                                                       m_nDigitCount,
                                                       10,
                                                       QLatin1Char('0'));
            }
            else
            {
                fn = QString("%1%2").arg(m_Prefix).arg(counter);
            }
            m_FileName = fn + "." + QString(pNameInfo->ext);

            filePath = m_PathName + "/" + m_FileName;

            if( !QFile::exists( filePath) )
            {
                break;
            }
        }
    }
    else //build the same filename
    {
        if( m_nDigitCount > 0 )
        {
            fn = QString("%1%2").arg(m_Prefix).arg(pNameInfo->counter);
        }
        else
        {
            fn = m_Prefix;
            if( m_Prefix.size() == 0 )
            {
                fn = QString("%1").arg(pNameInfo->counter);
            }
        }
        m_FileName = fn + "." + QString(pNameInfo->ext);
    }
}




QString servStorage::buildFolderName()
{
    static int startFrom = 0;

    QString newFolder = "";
    QString path;
    if ( m_bAutoName )
    {
        for(int i=startFrom; i < 999999; i++)
        {
            newFolder = QString("Folder%1").arg(i);
                 path = m_PathName + "/" + newFolder;

            QFileInfo fi(path);
            if( !fi.isDir() )
            {
                return newFolder;
            }
        }
    }
    else//bug1667
    {
        newFolder = QString("Folder%1").arg(startFrom);
             //path = m_PathName + "/" + newFolder;
    }

    return newFolder;
}

////////////////////Quick Action///////////////////////////////
/// \brief servStorage::quickSaving
/// \param type
///
int servStorage::quickSaving(int type)
{
    int cmd = FUNC_SAVE_IMG;
    int fmt = servFile::FILETYPE_PNG;

    if(m_hFat.size()>1)
    {
        if( m_PathName.compare(STORAGE_USER_PATH) == 0)
        {
            //get the first Mass Storage path
            setPathName(m_hFat.at(1).second);
        }
    }

    if(type == servQuick::SaveWave)
    {
        cmd = FUNC_SAVE_WAV;
        fmt = m_nWaveFormat + servFile::FILETYPE_BIN;
    }
    else if(type == servQuick::SaveSetup)
    {
        cmd = FUNC_SAVE_STP;
        fmt = servFile::FILETYPE_STP;
    }
    else
    {
        fmt = m_nImageFormat;
    }

    buildFileName(fmt);

    setThreadProc(servFile::ProcIdle);
    setThreadProc(servFile::ProcSaving);
    //qDebug() << m_nFileFormat << m_PathName + "/" + m_FileName;
    m_pFileProc->save(cmd);
    return ERR_NONE;
}

void servStorage::quickLoadSetup()
{

}

int servStorage::setFileType(int type)
{
    m_fileType = type;
    return ERR_NONE;
}

int servStorage::getFileType()
{
    return m_fileType;
}

///
/// \brief Disabled the MenuBack if progress is ongoing
/// \param key
/// \return
///
bool servStorage::keyEat(int key, int /*count*/, bool /*bRelease*/ )
{
    if ( !isActive() )
    {
        return false;
    }
    if ( m_pFileProc->getRun() && key != R_Pkey_PageOff)
    {
        servGui::showErr( ERR_ACTION_DISABLED );
        return true;
    }

    return false;
}

void servStorage::spyLicenseOpt()
{
     bool la = sysHasLA();

     mUiAttr.setVisible(MSG_STORAGE_CHANNEL, d0d7, la);
     mUiAttr.setVisible(MSG_STORAGE_CHANNEL, d8d15, la);
}
