#include "storage_api.h"
#include "servstorage.h"
#include "storage_img.h"
#include "../servplot/servplot.h"
#include "../servscpi/servscpidso.h"
#include "../servutility/servutility.h"

extern QWidget *sysGetMainWindow();

int CImageFile::saveIMG(QImage &rawData, QString &f)
{
    bool    bInvert;
    bool    bGray;

    int     nRet = 0;

    serviceExecutor::query(serv_name_storage, MSG_STORAGE_IMAGE_INVERT, bInvert);
    serviceExecutor::query(serv_name_storage, MSG_STORAGE_IMAGE_COLOR,  bGray);

    //if invert
    if(bInvert)
    {
        int depth = rawData.depth();
        //it need to be 32
        if( depth == 32 )
        {
            rawData.invertPixels();
        }
        else if(depth == 16)
        {
            int height = rawData.height();
            int width  = rawData.width();
            QImage dst = QImage(width, height, QImage::Format_RGB16);
            for(int i = 0; i < height; i ++)
            {
                ushort *pSrc  = (ushort*)rawData.scanLine(i);
                ushort *pDst  = (ushort*)dst.scanLine(i);
                uchar R,G,B;
                ushort C;
                for( int j = 0; j < width; j ++)
                {
                     R = 255 - ((*pSrc & 0xf800 ) >> 8);
                     G = 255 - ((*pSrc & 0x7e0 ) >> 3);
                     B = 255 - ((*pSrc & 0x1f ) << 3);

                     C =  (R >> 3) << 11;
                     C |= (G >> 2) << 5;
                     C |= (B >> 3) ;

                     pDst[j] = C;
                     pSrc++;
                     //pDst++;
                }
            }
            rawData = dst;
        }
    }

    //if !color
    if(bGray)
    {
        QImage dst;
        toGray(rawData, dst);
        rawData = dst;
    }



    if(f.endsWith(".tif", Qt::CaseInsensitive))
    {
        nRet = saveTIF(rawData, f);
    }
    else
    {
        if( rawData.save( f,0,62) )
        {
            nRet = ERR_NONE;
        }
        else
        {
            //qDebug() <<  strerror(errno);
            nRet = ERR_FILE_SAVE_FAIL;
        }
    }
    return nRet;
}

int CImageFile::saveIMG(QString &f)
{

    QImage  rawData ;

    int     nRet    =  getRawData(rawData);

    if(nRet != ERR_NONE)
    {
        return nRet;
    }

    return saveIMG(rawData, f);
}

int CImageFile::saveTIF(QImage &image,QString &f)
{
    int nRet    = ERR_FILE_SAVE_FAIL;

    int nWidth  = image.width();
    int nHeight = image.height();

    uchar*  pOutBuf = new uchar[nWidth*nHeight*2];
    ushort* pInBuf  = new ushort[nWidth*nHeight];

    Q_ASSERT(pOutBuf != NULL);
    Q_ASSERT(pInBuf != NULL);

    ushort* pRGB16 = pInBuf;
    uchar*  pRaw   = image.bits();
    uchar*  pSrc   = (uchar*)pInBuf;

    int nByteLeft = image.byteCount();

    if(image.depth() == 16)//arm
    {
        pSrc = pRaw;
    }
    else if(image.depth() == 8)
    {
        //convet 8bit to 16 bit
        uchar R,G,B;
        ushort COL;
        while(nByteLeft > 0)
        {
            B = *pRaw++;
            G = B;
            R = B;

            COL =  (R >> 3) << 11;
            COL |= (G >> 2) << 5;
            COL |= (B >> 3) ;

            *pRGB16++ = COL;
            nByteLeft -= 1;
        }
    }
    else if(image.depth() == 32)//pc
    {
        //convet 32bit to 16 bit
        uchar R,G,B;
        ushort COL;
        while(nByteLeft > 0)
        {
            B = *pRaw++;
            G = *pRaw++;
            R = *pRaw++;
                 pRaw++;

            COL =  (R >> 3) << 11;
            COL |= (G >> 2) << 5;
            COL |= (B >> 3) ;

            *pRGB16++ = COL;
            nByteLeft -= 4;
        }
    }

    int nFileSize = 0;
    int _r = userTIFDecoder(pSrc,pOutBuf,nWidth,nHeight, &nFileSize);
    if(nFileSize > 0 && _r == 0 )
    {
        QFile file(f);
        if( file.open(QIODevice::WriteOnly) )
        {
            file.write((const char*)pOutBuf, nFileSize);
            fsync(file.handle());
            file.close();
            nRet = ERR_NONE;
        }
    }
    delete []pOutBuf;
    delete []pInBuf;

    return nRet;
}

int CImageFile::toGray(QImage &src, QImage& dst)
{
    int height = src.height();
    int width  = src.width();

    dst = QImage(width, height, QImage::Format_Indexed8);
    dst.setColorCount(256);
    for(int i = 0; i < 256; i++)
    {
        dst.setColor(i, qRgb(i, i, i));
    }

    if(src.depth() == 16)
    {
        for(int i = 0; i < height; i ++)
        {
            ushort *pSrc  = (ushort*)src.scanLine(i);
            uchar  *pDest = dst.scanLine(i);
            int R,G,B;
            for( int j = 0; j < width; j ++)
            {
                 R = (*pSrc & 0xf800 ) >> 8;
                 G = (*pSrc & 0x7e0 ) >> 3;
                 B = (*pSrc & 0x1f ) << 3;
                 pDest[j] = (uchar)qGray( R, G, B);

                 pSrc++;
            }
        }
    }
    else if(src.depth() == 32)
    {
        for(int i = 0; i < height; i ++)
        {
            uint *pSrc   =  (uint*)src.scanLine(i);
            uchar *pDest =  dst.scanLine(i);
            int R,G,B;
            for(int j = 0; j < width; j ++)
            {
                 R = (*pSrc >> 16) & 0xff;
                 G = (*pSrc >> 8) & 0xff;
                 B = *pSrc & 0xff;
                 pDest[j] = (uchar)qGray( R, G, B);

                 pSrc++;
            }
        }
    }
    else //
    {
        for(int i = 0; i < height; i ++)
        {
            uchar *pSrc   =  src.scanLine(i);
            uchar *pDest  =  dst.scanLine(i);
            for( int j = 0; j < width; j ++)
            {
                 pDest[j] = (uchar)qGray( *pSrc, *pSrc, *pSrc);
                 pSrc++;
            }
        }
    }

    return ERR_NONE;
}


///
/// \brief:If we can't get framebuffer from the physical device.
/// then use the Qt windows buffer.
/// \return
///
uchar *CImageFile::m_pImageBuf = NULL;

#include "../servplot/servplot_dpu.h"
int CImageFile::getRawData(QImage &out, bool header)
{
#ifndef _SIMULATE
    if(m_pImageBuf == NULL)
    {
        m_pImageBuf = new uchar[PLOT_WIDTH*(PLOT_HEIGHT+IMAGE_HEADER_HEIGHT)*2]; //with footer
    }

    if(m_pImageBuf != NULL )
    {
        uchar *pImgData = (uchar*)(CDPU::getFrameBuffer());
        int    nImgHeight = PLOT_HEIGHT;

        if(pImgData == NULL )
        {
            return ERR_INVALID_INPUT;
        }

        if( header )
        {
            memset(m_pImageBuf, 0, PLOT_WIDTH * IMAGE_HEADER_HEIGHT * 2);
            memcpy(m_pImageBuf + PLOT_WIDTH*IMAGE_HEADER_HEIGHT*2, pImgData, PLOT_SIZE);
            nImgHeight += IMAGE_HEADER_HEIGHT;

            pImgData = m_pImageBuf;
        }
        else
        {
            ;//without image header
        }

        out = QImage((uchar*)pImgData,
                     PLOT_WIDTH,
                     nImgHeight,
                     PLOT_BPL,
                     QImage::Format_RGB16);

        if( header )
        {
            QPainter painter;
            painter.begin(&out);
            QPen pen(Qt::white);
            painter.setPen(pen);
            QRect r;
            r.setLeft(0);
            r.setTop(0);
            r.setWidth(PLOT_WIDTH);
            r.setHeight(IMAGE_HEADER_HEIGHT);

            QString str = QString("%1     %2     %3    Build : %4")
                                  .arg(servUtility::getSystemModel()) \
                                  .arg(servUtility::getSystemVersion()) \
                                  .arg(servUtility::getSystemSerial()) \
                                  .arg(QDateTime::currentDateTime().toString("ddd MMMM dd hh:mm:ss yyyy"));

            painter.drawText(r,Qt::AlignLeft | Qt::AlignVCenter,str);
            painter.end();
        }
        return ERR_NONE;
    }
#endif
    //RTextBox QEditText will make the fllowing code not work
//    if(1)
//    {
//        QWidget *pWin   = sysGetMainWindow();
//        if(pWin)
//        {
//            out = pWin->grab().toImage();
//            return ERR_NONE;
//        }
//    }
    qDebug() << "Can't make framebuffer addres" << __FUNCTION__;
    return ERR_INVALID_INPUT;
}
