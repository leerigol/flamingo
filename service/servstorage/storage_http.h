#ifndef IHTTPDOWNLOADS_H
#define IHTTPDOWNLOADS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QFile>
#include <QNetworkReply>
#include <QTimer>
#include <QThread>

class CHttp : public QObject
{
    Q_OBJECT
public:
    explicit CHttp(QObject *parent = 0, bool showWin = false);
    ~CHttp();

    bool    download(const QUrl &url, const QString &filePath);

    /* get file from url which we need to download, and restore to filePath */
    QString& getFilePath(void) { return m_FirmwarePath; }

signals:
    void sigFinish(QNetworkReply::NetworkError);

public slots:
    void replyFinished(); /* download finished */
    void replyDownloadProgress(qint64, qint64); /* downloading... */
    void slotError(QNetworkReply::NetworkError); /* handle error */
    void slotReadyRead();/* ready read */
    void handleTimeOut(); /* handle time out */


    void run();

private:
    QNetworkAccessManager    *m_pRequest;
    QNetworkReply            *m_pResponse;
    QTimer                   *m_pTimer;

    QUrl                     m_ServerURL;
    QFile                    m_File;
    QString                  m_FirmwarePath;
    bool                     m_bShowWin;
    bool                     m_bStarted;
};

#endif // IHTTPDOWNLOADS_H
