#include <unistd.h>
#include "storage_wav.h"


CBinFile::CBinFile()
{
    m_pFile = NULL;
    m_nChanCount = 0;
    m_nBpp = 4;
    m_nSize = 1000;
}

CBinFile::~CBinFile()
{
    close();
}

bool CBinFile::create(QString &binFileName)
{
    //qDebug() << "size of header" << sizeof(tBinHeader) << sizeof(tWaveHeader) << sizeof(tWaveData);
    m_pFile = new QFile(binFileName);
    if(m_pFile)
    {
         return m_pFile->open(QIODevice::ReadWrite);
    }
    return false;
}

bool CBinFile::writeBinHeader(int chan_count)
{
    tBinHeader bh;
    mPastCount = 0;

    int size = sizeof(bh);

    bh.Cookie[0] = 'R';
    bh.Cookie[1] = 'G';

    bh.Version[0] = '0';
    bh.Version[1] = '1';

    m_nChanCount = chan_count;

    bh.NumberOfWaveforms = chan_count;

    bh.FileSize = (chan_count * m_nSize * m_nBpp) +
            size + sizeof(tWaveHeader) + sizeof(tWaveData);

    if( size == m_pFile->write((const char*)&bh, sizeof(bh)) )
    {
        return true;
    }

    return false;
}

bool CBinFile::writeWaveHeader(Chan ch,int unit, QString label, DsoWfm &wave)
{
    tWaveHeader wave_hdr;
    tWaveData   wave_data;
    memset(&wave_hdr, 0, sizeof(tWaveHeader));
    memset(&wave_data, 0, sizeof(tWaveData));

    wave_hdr.HeaderSize     =   sizeof(tWaveHeader);
    wave_data.HeaderSize    =   sizeof(tWaveData);
    if(ch < d0 || (ch >=r1 && ch <=r10 ))
    {
        wave_hdr.WaveformType = PB_NORMAL;
        wave_data.BufferType  = PB_DATA_NORMAL;
    }
    else if(ch < acline || ch == d0d7 || ch == d8d15 || ch == d0d15)
    {
        wave_hdr.WaveformType = PB_LOGIC;
        wave_data.BufferType  = PB_DATA_LOGIC;
    }
    //qDebug() << "wave.buffer" << wave_data.BufferType << __LINE__;

    wave_hdr.NWaveformBuffers = 1;
    wave_hdr.Points = m_nSize ;
    wave_hdr.Count  = 0;

    wave_hdr.XDisplayRange   = wave.gettInc().toFloat() * m_nSize;
    wave_hdr.XDisplayOrigin  = wave.gett0().toDouble();
    wave_hdr.XIncrement      = wave.gettInc().toDouble();
    wave_hdr.XOrigin         = wave.gett0().toDouble();

    wave_hdr.XUnits          = BIN_UNIT_S;
    wave_hdr.YUnits          = unit;

    QString str;

    str = QDateTime::currentDateTime().toString("yyyy-MM-dd");
    strcpy(wave_hdr.Date, str.toLocal8Bit().data());

    str = QDateTime::currentDateTime().toString("HH:mm:ss");
    strcpy(wave_hdr.Time, str.toLocal8Bit().data());

    str = servUtility::getSystemModel() + ":" + servUtility::getSystemSerial();
    strcpy(wave_hdr.Frame, str.toLocal8Bit().data());

    //qDebug() << "wave.buffer" << wave_data.BufferType << __LINE__;

    if(label.size() >= SIZEOF_LABEL)
    {
        memcpy(wave_hdr.WaveformLabel, label.toLocal8Bit().data(), SIZEOF_LABEL);
    }
    else
    {
        strcpy(wave_hdr.WaveformLabel, label.toLocal8Bit().data());
    }
    //qDebug() << "wave.buffer" << wave_data.BufferType << __LINE__;

    wave_hdr.TimeTag = 0.0;
    wave_hdr.SegmentIndex = 1;

    wave_data.BytesPerPoint = m_nBpp;
    wave_data.BufferSize    = m_nSize * m_nBpp;

    int size = wave_hdr.HeaderSize;

    if( size == m_pFile->write((const char*)&wave_hdr, size ) )
    {
        //qDebug() << "wave.buffer" << wave_data.BufferType << __LINE__;
        size = wave_data.HeaderSize;
        if( size == m_pFile->write((const char*)&wave_data, size ) )
        {
            return true;
        }
    }
    return false;
}

bool CBinFile::writeWaveData(DsoWfm &wave)
{
    int size =  m_nBpp * wave.size();
    if( size == m_pFile->write( (const char*)wave.getValue(), size))
    {
        return true;
    }
    return false;
}

bool CBinFile::close()
{
    if(m_pFile)
    {
        if(m_pFile->isOpen())
        {
            fsync(m_pFile->handle());
            m_pFile->close();
        }
        delete m_pFile;
        m_pFile = NULL;
        return true;
    }
    return false;
}

QString CBinFile::getFileName()
{
    Q_ASSERT(NULL != m_pFile);
    return m_pFile->fileName();
}

bool CBinFile::writeData(quint8 *pBuf, quint64 size, quint64 totalCount)
{
    quint64 progStep  = totalCount/100;
    quint64 progStart = 99;
    int     count     = 0;
    if(progStep > 0)
    {
        progStart = mPastCount/progStep;
        count    = (size/progStep);
        //qDebug()<<__FILE__<<__LINE__<<progStart<<progStep;
    }

    Q_ASSERT(size >= count*progStep);
    quint64  remain = size - count*progStep;

    if(size < progStep)
    {
        count = 0;
        remain = size;
    }

    for(int i = 0; i< count; i++)
    {
        Q_ASSERT((i*progStep) < size);

        if( (-1) == m_pFile->write( (char*)(pBuf+i*progStep), progStep) )
        {
            return false;
        }

        progStart++;
        progStart = qMin(progStart, (quint64)100);
        servFile::setProgress(progStart);
        //qDebug()<<__FILE__<<__LINE__<<"-----------------------------------------prog:"<<progStart;
    }

    if(remain > 0)
    {
        Q_ASSERT(count*progStep < size);

        if( (-1) == m_pFile->write( (char*)(pBuf + count*progStep), remain) )
        {
            return false;
        }

        servFile::setProgress(progStart);
    }

    mPastCount += size;
    return true;
}

