#ifndef WAVEFILE
#define WAVEFILE

#include<QObject>
#include<QFile>
#include "../../baseclass/dsovert.h"
#include "../servutility/servutility.h"
#include "storage_api.h"

///////////////////////////////////About Bin file format define/////////////////////
#define SIZEOF_DATETIME 16
#define SIZEOF_FRAME    24
#define SIZEOF_LABEL    16
#define SIZEOF_ID       2


//!File Header
typedef struct
{
   char         Cookie[SIZEOF_ID];   //! identify of bin
   char         Version[SIZEOF_ID];  //! version the bin
   int          FileSize;           //! size of the whole of the bin
   int          NumberOfWaveforms;  //! the numbers of the waveform in all

} tBinHeader;

//!Tile of WaveForm
typedef struct
{
   int          HeaderSize;
   int          WaveformType;
   int          NWaveformBuffers;          //! default to be 1
   int          Points;                    //! the points of wfm
   int          Count;                     //! default zero
   float        XDisplayRange;             //! the sign of time for x axis
   double       XDisplayOrigin;            //! the first point appearing at the leftest of the screen
   double       XIncrement;
   double       XOrigin;                   //! the first data`s x value of wave data
   int          XUnits;
   int          YUnits;
   char         Date[SIZEOF_DATETIME];  //! default to be blank
   char         Time[SIZEOF_DATETIME];  //! default to be blank
   char         Frame[SIZEOF_FRAME];     //! contain digital scope`s model and serial number
   char         WaveformLabel[SIZEOF_LABEL]; //! the lable assigned to current wave
   double       TimeTag;                  //! used to mark time when triggered at the first (only active on saving multiple segments)
   unsigned int SegmentIndex;

} tWaveHeader;

//!Tile of WaveForm Data
typedef struct
{
   int          HeaderSize;
   short        BufferType;     //! buffer`s data type
   short        BytesPerPoint;  //! sign of per Point
   int          BufferSize;     //! buffer`s size

} tWaveData;

typedef enum
{
   PB_UNKNOWN,
   PB_NORMAL,
   PB_PEAK_DETECT,
   PB_AVERAGE,
   PB_WAVE_FORM_RESERV1,
   PB_WAVE_FORM_RESERV2,
   PB_LOGIC
} eWavetype;

typedef enum
{
   PB_DATA_UNKNOWN,
   PB_DATA_NORMAL,
   PB_DATA_MAX,
   PB_DATA_MIN,
   PB_DATA_RESERV2,
   PB_DATA_LOGIC
} eDataType;

typedef enum
{
    BIN_UNIT_UNKNOW = 0,
    BIN_UNIT_V,
    BIN_UNIT_S,
    BIN_UNIT_C,//3 = 常数。
    BIN_UNIT_A,//4 = 安培。A
    BIN_UNIT_DB,//5 = 分贝。db
    BIN_UNIT_HZ,//6 = 赫兹。HZ
}eWaveUnit;

class CBinFile : public QObject
{
public:
    CBinFile();
    ~CBinFile();
    bool create(QString& binFileName);
    bool writeBinHeader(int chan_count);
    bool writeWaveHeader(Chan, int unit, QString, DsoWfm&);
    bool writeWaveData(DsoWfm&);
    bool writeData(quint8 *pBuf, quint64 size, quint64 totalCount);
    bool close();

    void setBpp(int bpp)   { m_nBpp = bpp;}
    void setPointCount(int size) { m_nSize = size;}
    QString getFileName();
protected:
    QFile* m_pFile;
    int    m_nChanCount;
    int    m_nBpp;
    int    m_nSize;

protected:
    quint64 mPastCount;
};

///////////////////////////////////About CSV file format define/////////////////////
class CCSVFile : public QObject
{
public:
    CCSVFile();
    ~CCSVFile();
    bool create(QString& csvFileName);
    bool writeCSVHeader(QList<int>& chanList, bool laHigh=false, bool lsLow=false);
    bool writeCSVHeader(QString& ref);
    bool writeWaveData(QList<DsoWfm*>& chanList);
    bool writeWaveData(QList<DsoWfm*>& chanList, DsoWfm *plaHigh , DsoWfm *plaLow );

    bool writeCSVHeader(Chan ch, vertAttr vAttr, horiAttr hAttr);
    bool writeWaveData(quint8 *pBuf, quint64 size, quint64 totalCount);
    bool checkSplitFile();

    bool close();
    QString getFileName();
protected:
    QFile* m_pFile;

protected:
    quint64               mPastCount;
    QMap<int, QByteArray> mVoltageMap;
    horiAttr              mHAttr;
    bool                  mTimeEn;
private:
    QString  mFileName;
};

///////////////////////////////////About  wfm file format define/////////////////////

class setupInfo :public QObject
{
public:
    quint32  version;
    quint32  crc;
    quint64  size;   //!byte

#ifndef QT_NO_DATASTREAM
    friend QDataStream& operator>>(QDataStream &in, setupInfo& data)
    {
        in>>data.version>>data.crc>>data.size;
        return in;
    }
    friend QDataStream& operator<<(QDataStream &out,setupInfo& data)
    {
        out<<data.version<<data.crc<<data.size;
        return out;
    }
#endif
};

class  stFrmInfo :public QObject
{
public:
    quint64  frameStart;
    quint64  frameEnd;
    quint64  frame;
    quint64  size;
    quint64  timeTag;
    quint32  trigPos;
    quint32  ch;

    quint32  saChLen;
    quint32  saLaLen;
    bool     saValid;

public:
    stFrmInfo& operator=(const stFrmInfo &other)
    {
        this->frameStart  = other.frameStart;
        this->frameEnd    = other.frameEnd;
        this->frame       = other.frame;
        this->size        = other.size;
        this->timeTag     = other.timeTag;
        this->trigPos     = other.trigPos;
        this->ch          = other.ch;

        this->saChLen     = other.saChLen;
        this->saLaLen     = other.saLaLen;
        this->saValid     = other.saValid;
        return *this;
    }

#ifndef QT_NO_DATASTREAM
    friend QDataStream& operator>>(QDataStream &in, stFrmInfo& data)
    {
          in>>data.frameStart
            >>data.frameEnd
            >>data.frame
            >>data.size
            >>data.timeTag
            >>data.trigPos
            >>data.ch

            >>data.saChLen
            >>data.saLaLen
            >>data.saValid;
        return in;

    }
    friend QDataStream& operator<<(QDataStream &out,stFrmInfo& data)
    {
       out<<data.frameStart
          <<data.frameEnd
          <<data.frame
          <<data.size
          <<data.timeTag
          <<data.trigPos
          <<data.ch

          <<data.saChLen
          <<data.saLaLen
          <<data.saValid;
        return out;
    }
#endif
};

class CWFMFile : public CBinFile
{  
public:
    bool writeHeader(QByteArray session);
    bool readHeader( QByteArray &session);

    bool writeFrameInfo(stFrmInfo &frameInfo);
    bool writeData(quint8    *pBuf,
                   quint64    size,
                   quint64    totalCount);


    bool   readFrameInfo(stFrmInfo &frameInfo);
    qint64 readData(quint8    *pBuf,
                    quint64   size);

protected:
    const static int  m_version = QDataStream::Qt_5_5;
    quint64           m_frameSize;
};


class CWaveFile : public QObject
{
public:
    const static quint32 BUF_SIZE     =   32;
    const static quint32 LAB_SIZE     =   16;
    const static quint32 REF_VER      =   0x20161230;

    public:
                                          //false=wave,true=ref
        static int      saveWAV(QString&, bool b = false, int ext = -1);
        static int      saveBIN(QString&, bool b = false);
        static int      saveCSV(QString&, bool b = false);
        static int      saveWFM(QString&fname);
        static int      saveREF(QString&);

        static int      loadREF(QString&, int id = -1);
        static int      loadWFM(QString&);

    private:
        static int      saveRefAsBin(QString&);
        static int      saveScrAsBin(QString&);
        static int      saveMemAsBin(QString &FileName);

        static int      saveRefAsCSV(QString&);
        static int      saveScrAsCSV(QString&);
        static int      saveMemAsCSV(QString &FileName);

        static int      checkMath(int id);

};
#endif // WAVEFILE

