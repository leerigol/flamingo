#include <QFile>

#include "../../com/crc32/ccrc32.h"
#include "../servicefactory.h"
#include "../service_name.h"
#include "../servmask/servmask.h"
#include "../servutility/servutility.h"
#include "../../engine/base/dsoengine.h"
#include "../../service/service.h"
#include "storage_api.h"
#include "storage_dat.h"


#define SERIAL_OUT(name, value) { if(bPrivate) \
                                  { \
                                    return 1;\
                                  }\
                                  mTextStream << "<" \
                                              << name \
                                              << ">" \
                                              <<  value \
                                              << "</" \
                                              << name \
                                              << ">" \
                                              << "\n";}

#define SERIAL_IN(name, value)  do  \
                                {   \
                                    if(bPrivate) \
                                    { \
                                      return 1;\
                                    }\
                                    QString nodePath = mAppName + "/" + name;\
                                    int temp;\
                                    if( mpSetup->getMetaVal(nodePath,temp) ) \
                                    { \
                                        value = temp;\
                                        return 0; \
                                    } \
                                    return 1; \
                                } \
                                while(0);


CSetupStream::CSetupStream(QFile& file) : CStream()
{
    mpSetup = NULL;
    mTextStream.setDevice(&file);
}

CSetupStream::CSetupStream(QString& stp, QString& app) : CStream()
{
    mpSetup = new r_meta::CXmlMeta(stp);
    mAppName = app;
}

void CSetupStream::setAppName(QString &s)
{
    mAppName = s;
}

CSetupStream::~CSetupStream()
{
    if( mpSetup )
    {
        delete mpSetup;
        mpSetup = NULL;
    }
}

int CSetupStream::write(const QString &node)
{
    mTextStream << node << "\n";
    return 0;
}

/*!
 * \brief CSetupStream::write
 * \param name
 * \param dat
 * \return
 */
int CSetupStream::write( const QString &name,
                         const bool &dat,
                         bool bPrivate,
                         cryptKey /*key*/ )
{
    SERIAL_OUT(name,dat)
    return 0;
}
int CSetupStream::write( const QString &name,
                         const char &dat,
                         bool bPrivate,
                         cryptKey /*key*/)
{
    SERIAL_OUT(name,dat)
    return 0;
}
int CSetupStream::write( const QString &name,
                         const short &dat,
                         bool bPrivate,
                         cryptKey /*key*/)
{
    SERIAL_OUT(name,dat)
    return 0;
}
int CSetupStream::write( const QString &name,
                         const int &dat,
                         bool bPrivate,
                         cryptKey /*key*/ )
{
    SERIAL_OUT(name,dat)
    return 0;
}

int CSetupStream::write( const QString &name,
                         const unsigned char &dat,
                         bool bPrivate,
                         cryptKey /*key*/)
{
    SERIAL_OUT(name,dat)
    return 0;
}

int CSetupStream::write( const QString &name,
                         const unsigned short &dat,
                         bool bPrivate,
                         cryptKey /*key*/)
{
    SERIAL_OUT(name,dat)
    return 0;
}

int CSetupStream::write( const QString &name,
                         const unsigned int &dat,
                         bool bPrivate,
                         cryptKey /*key*/ )
{
    SERIAL_OUT(name,dat)
    return 0;
}

int CSetupStream::write( const QString &name,
                         const qlonglong &dat,
                         bool bPrivate,
                         cryptKey /*key*/ )
{
    QString data = QString("%1%2").arg(dat).arg("ll");
    SERIAL_OUT(name,data)
    return 0;
}

int CSetupStream::write( const QString &name,
                         const qulonglong &dat,
                         bool bPrivate,
                         cryptKey /*key*/)
{

    QString data = QString("%1%2").arg(dat).arg("ll");
    SERIAL_OUT(name,data)
    return 0;
}

int CSetupStream::write( const QString &name,
                         const QString &dat,
                         bool bPrivate,
                         cryptKey /*key*/ )
{
    SERIAL_OUT(name,dat)
    return 0;
}
/*!
 * \brief CSetupStream::write
 * \param name
 * \param pBuf
 * \param length
 * \param bPrivate
 * \param key
 * \return
 * 二进制数据串存储格式：
 * -#[length]
 * -length:4 bytes
 */
int CSetupStream::write( const QString &/*name*/,
                         void */*pBuf*/,
                         int /*length*/,
                         bool /*bPrivate*/,
                         cryptKey /*key*/ )
{
    qDebug() << "not support now";
    return 0;
}

/*!
 * \brief CSetupStream::read
 * \param name
 * \param dat
 * \return
 */
int CSetupStream::read( const QString &name,
                        bool &dat,
                        bool bPrivate,
                        cryptKey /*key*/ )
{
    if( bPrivate ) return 1;
    QString nodePath = mAppName + "/" + name;
    int temp;
    if( mpSetup->getMetaVal(nodePath,temp) )
    {
        dat = temp;
        return 0;
    }
    return 1;
}
int CSetupStream::read( const QString &name,
                        char &dat,
                        bool bPrivate,
                        cryptKey /*key*/)
{
    SERIAL_IN(name,dat)
}
int CSetupStream::read( const QString &name,
                        short &dat,
                        bool bPrivate,
                        cryptKey /*key*/)
{
    SERIAL_IN(name,dat)
}
int CSetupStream::read( const QString &name,
                        int &dat,
                        bool bPrivate,
                        cryptKey /*key*/ )
{
    SERIAL_IN(name,dat)
}

int CSetupStream::read( const QString &name,
                        qlonglong &dat,
                        bool bPrivate,
                        cryptKey /*key*/ )
{
    if( bPrivate ) return 1;
    QString nodePath = mAppName + "/" + name;
    if( mpSetup->getMetaVal(nodePath,dat) )
    {
        return 0;
    }
    return 1;
}

int CSetupStream::read( const QString &name,
                        unsigned char &dat,
                        bool bPrivate,
                        cryptKey /*key*/)
{
    SERIAL_IN(name,dat)
}
int CSetupStream::read( const QString &name,
                        unsigned short &dat,
                        bool bPrivate,
                        cryptKey /*key*/)
{
    SERIAL_IN(name,dat)
}
int CSetupStream::read( const QString &name,
                        unsigned int &dat,
                        bool bPrivate,
                        cryptKey /*key*/ )
{
    SERIAL_IN(name,dat)
}

int CSetupStream::read( const QString &name,
                        qulonglong &dat,
                        bool bPrivate,
                        cryptKey /*key*/)
{
    if( bPrivate ) return 1;
    QString nodePath = mAppName + "/" + name;
    qint64 temp;
    if( mpSetup->getMetaVal(nodePath,temp) )
    {
        dat = temp;
        return 0;
    }
    return 1;
}

int CSetupStream::read( const QString &name,
                        QString &dat,
                        bool bPrivate,
                        cryptKey /*key*/ )
{
    if( bPrivate ) return 1;
    QString nodePath = mAppName + "/" + name;
    if( mpSetup->getMetaVal(nodePath,dat) )
    {
        return 0;
    }
    return 1;
}

int CSetupStream::read( const QString &/*name*/,
                        void */*pBuf*/,
                        int /*cap*/,
                        bool /*bPrivate*/,
                        cryptKey /*key*/ )
{
    qDebug() << "not support now";
    return 0;
}

int CDataFile::preSTP(CSetupStream& stream)
{
    QString str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

    stream.write( str );

    str = "<meta>";
    stream.write( str );

    str = QString("<version>%1</version>").arg(STP_VER,0,16);
    stream.write( str );

    str = "<vendor>RIGOL</vendor>";
    stream.write( str );

    str = servUtility::getSystemModel();
    QString node = "<model>" + str + "</model>";
    stream.write ( node );

    str = servUtility::getSystemVersion();
    node = "<firmware>" + str + "</firmware>";
    stream.write ( node );

    str = servUtility::getSystemSerial();

    node = "<serial>" + str + "</serial>\n";
    stream.write ( node );


    return 0;
}

int CDataFile::postSTP(CSetupStream& stream)
{
    QString str = "</meta>";
    stream.write( str );
    return 0;
}

///
/// \brief CDataFile::saveSTP
/// \param fname
/// \return
int CDataFile::saveSTP(QString &fname)
{
    QFile file(fname);
    if( file.open(QIODevice::WriteOnly | QIODevice::Truncate) )
    {
        unsigned char ver = 0;
        QString str;
        CSetupStream stream(file);


        QList<service*>* pServList = serviceFactory::getServiceList();
        int size = pServList->size() ;


        preSTP(stream);

        for(int i=0; i<size; i++)
        {
            ISerial* pServ =  dynamic_cast<ISerial*>(pServList->at(i));
            if(pServ)
            {

                str = "<" + pServList->at(i)->getName() + ">";
                stream.write( str );

                pServ->serialOut(stream, ver);
                str = "<version>"+QString().setNum(ver) + "</version>";
                stream.write( str );

                str = "</" + pServList->at(i)->getName() + ">\n";
                stream.write( str );
            }
            else
            {
                //qDebug() << "from serviceexecutor to iserial" << pServList->at(i)->getName();
            }
        }

        postSTP(stream);

        file.flush();
        file.close();
        return ERR_NONE;
    }
    return ERR_FILE_SAVE_FAIL;
}

int CDataFile::loadSTP(QString & fname)
{
    QFile file(fname);
    if(file.exists())
    {
        QList<service*>* pServList = serviceFactory::getServiceList();
        int count = pServList->size() ;
        QString appName = "meta";
        uchar ver=0;

        CSetupStream stream(fname, appName);

        //force ccu stop
        if( sysGetSystemStatus() == 0 )
        {
            serviceExecutor::send(serv_name_hori,
                                  servHori::cmd_control_status,
                                  Control_Run);
            QThread::msleep( 100 );
        }
        serviceExecutor::send(serv_name_hori,
                              servHori::cmd_control_status,
                              Control_Stop);

        bool bmso = sysHasLA();

        servMath::resetData();

        for(int i=0; i<count; i++)
        {
            ISerial* pServ =  dynamic_cast<ISerial*>(pServList->at(i));
            if(pServ)
            {               
                appName = pServList->at(i)->getName();

                //mso setting can not apply to ds
                if( !bmso && appName.compare(serv_name_la) == 0 )
                {
                    continue;
                }

                //qDebug() << appName;
                stream.setAppName( appName );

                stream.read("version", ver);

                pServ->serialIn(stream,ver);

                serviceExecutor::post(pServList->at(i)->getId(),
                                      CMD_SERVICE_STARTUP,
                                      (int)0);
            }
            else
            {
                //qDebug() << pServList->at(i)->getName();
            }
        }
    }
    return ERR_NONE;
}

///
/// \brief CDataFile::savePF
/// \return
//////Format:
/// p   type    len  content
/// 1.  u32     4    magic
/// 2.  u32     4    type
/// 3.  u32     4    ver
/// 4.  char    32   model
/// 5.  void    80   reserved
/// 6.  int     4    source
/// 7.  int     4    mask range
/// 8.  int     4    mask-x
/// 9.  int     4    mask-y
/// 10.  int     4    left
/// 11. int     4    right
/// 12. void    80   reserved
/// 13. int     4    RuleWfm_yGnd
/// 14. int     4    RuleWfm_start
/// 15. int     4    RuleWfm_yDiv_M
/// 16. int     4    RuleWfm_yDiv_N
/// 17. char    size RuleWfm_data
int CDataFile::savePF(QString &fname )
{
    QFile file(fname);
    if( file.open(QIODevice::WriteOnly) )
    {
        QDataStream out( &file );
        out << servFile::FILE_MAGIC;
        out << servFile::FILE_PF_TYPE;
        out << PF_VER;

        //! model
        char model[BUF_SIZE];
        memset(model, 0, BUF_SIZE);
        QString modelStr = servUtility::getSystemModel();
        strcpy(model, modelStr.toLocal8Bit().data());
        out.writeRawData(model, BUF_SIZE);

        //! for reserved, len = 4*20
        for(int i=0; i<20; i++)
        {
            out << (quint32)0;
        }

        //! Mask Source
        int source = chan1;
        serviceExecutor::query(serv_name_mask,
                               MSG_MASK_SOURCE,  source);
        out << source;
qDebug()<<__FILE__<<__LINE__<<"-------source-------"<<source;

        //! Mask Range
        int range = (int)mask_range_screen;
        serviceExecutor::query( serv_name_mask,
                                MSG_MASK_RANGE, range);
        out << range;
qDebug()<<__FILE__<<__LINE__<<"-------range-------"<<range;
        //! Mask-x
        int val = 0;
        serviceExecutor::query(serv_name_mask,
                               MSG_MASK_X_MASK, val);
        out << val;
qDebug()<<__FILE__<<__LINE__<<"-------val-------"<<val;
        //! Maxk-y
        val = 0;
        serviceExecutor::query(serv_name_mask,
                               MSG_MASK_Y_MASK, val);
        out << val;
qDebug()<<__FILE__<<__LINE__<<"-------val-------"<<val;
        //! Mask left: RangeA
        val = 0;
        serviceExecutor::query(serv_name_mask,
                               MSG_MASK_RANGE_A, val);
        out << val;
        qDebug()<<__FILE__<<__LINE__<<"-------val-------"<<val;

        //! Mask right: RangeB
        val = 0;
        serviceExecutor::query(serv_name_mask,
                               MSG_MASK_RANGE_B, val);
        out << val;
        qDebug()<<__FILE__<<__LINE__<<"-------val-------"<<val;

        //! for reserved, len = 4*20
        for(int i=0; i<20; i++)
        {
            out << (quint32)0;
        }

        //! Mask RuleWfm data
        void *ptr = NULL;
        serviceExecutor::query(serv_name_mask,
                               servMask::cmd_mask_data_acq, ptr);
        if(ptr)
        {
            DsoWfm *data = (DsoWfm*)ptr;
            //! Mask RuleWfm attr
            out << (qlonglong)(data->getyGnd());
            out << (int)(data->start());
            out << (qint64)(data->getyDiv().mM);
            out << (qint64)(data->getyDiv().mN);
            qDebug()<<"CDataFile::savePF  yGnd:  "<<data->getyGnd();
            qDebug()<<"CDataFile::savePF  start:  "<<data->start();
            qDebug()<<"CDataFile::savePF  yDiv_M:  "<<data->getyDiv().mM;
            qDebug()<<"CDataFile::savePF  yDiv_N:  "<<data->getyDiv().mN;

            //! Mask RuleWfm data
            out.writeRawData((const char*)(data->getPoint()),
                             sizeof(DsoPoint) * data->size());

            file.flush();
            file.close();
            return ERR_NONE;
        }
        else  //! No RuleWfm data
        {
            file.close();
            file.remove();
            return ERR_NONE;
        }
    }
    return ERR_FILE_SAVE_FAIL;
}

DsoWfm CDataFile::_wfm;
int CDataFile::loadPF(QString &fname)
{
    qDebug()<<"CDataFile::loadPF Start...";
    QFile file(fname);
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream in( &file );
        char model[BUF_SIZE];

        //! checking magic
        quint32 v;
        in >> v;
qDebug()<<__FILE__<<__LINE__<<"v"<<v;
        if( v != servFile::FILE_MAGIC)
        {
            file.close();
            return ERR_FILE_MAGIC;
        }
//        qDebug()<<"Finish checking magic";

        //!checking PF file id
        in >> v;
qDebug()<<__FILE__<<__LINE__<<"v"<<v;
        if( v != servFile::FILE_PF_TYPE)
        {
            file.close();
            return ERR_FILE_TYPE;
        }
//        qDebug()<<"Finish checking PF file id";

        //! checking version
        in >> v;
qDebug()<<__FILE__<<__LINE__<<"v"<<v;
        if( v != PF_VER)
        {
            file.close();
            if(v > PF_VER)
            {
                return ERR_FILE_NEW_VER;
            }
            return ERR_FILE_OLD_VER;
        }
//        qDebug()<<"Finish checking version";

        //! checking model
        in.readRawData((char*)model, BUF_SIZE);
        QString modelStr(model);

        QString currModel = servUtility::getSystemModel();
        if(modelStr.compare(currModel) != 0)
        {
            file.close();
            return ERR_FILE_MODEL;
        }
//        qDebug()<<"Finish checking model";

        //! jump the reserved, len = 4*20
        for(int i=0; i<20; i++)
        {
            in >> v;
        }

        //! Mask Source
        int source = chan1;
        in >> source;
qDebug()<<__FILE__<<__LINE__<<"-------source-------"<<source;
        serviceExecutor::post( E_SERVICE_ID_MASK,
                               MSG_MASK_SOURCE,  source);

        //! Mask Range
        int range = (int)mask_range_screen;
        in >> range;
qDebug()<<__FILE__<<__LINE__<<"-------range-------"<<range;
        serviceExecutor::post( E_SERVICE_ID_MASK,
                               MSG_MASK_RANGE, range);

        //! Mask-x
        int val = 0;
        in >> val;
qDebug()<<__FILE__<<__LINE__<<"-------val-----"<<val;
        serviceExecutor::post( E_SERVICE_ID_MASK,
                               MSG_MASK_X_MASK, val);

        //! Mask-y
        in >> val;
qDebug()<<__FILE__<<__LINE__<<"-------val-----"<<val;
        serviceExecutor::post( E_SERVICE_ID_MASK,
                               MSG_MASK_Y_MASK, val);

        //! Mask left: RangeA
        in >> val;
qDebug()<<__FILE__<<__LINE__<<"-------val-----"<<val;
        serviceExecutor::post( E_SERVICE_ID_MASK,
                               MSG_MASK_RANGE_A, val);

        //! Mask right: RangeB
        in >> val;
qDebug()<<__FILE__<<__LINE__<<"-------val-----"<<val;
        serviceExecutor::post( E_SERVICE_ID_MASK,
                               MSG_MASK_RANGE_B, val);

        //! jump the reserved, len = 4*20
        for(int i=0; i<20; i++)
        {
            in >> v;
            qDebug()<<__FILE__<<__LINE__<<"v_"<<i<<":"<<v;
        }

        //! Mask RuleWfm attr
        qlonglong yGnd = 0;
        in >> yGnd;

        int start = 0;
        in >> start;

        qint64 yDiv_M = 0, yDiv_N = 0;
        in >> yDiv_M;
        in >> yDiv_N;
        qDebug()<<"CDataFile::loadPF  yGnd:  "<<yGnd;
        qDebug()<<"CDataFile::loadPF  start:  "<<start;
        qDebug()<<"CDataFile::loadPF  yDiv_M:  "<<yDiv_M;
        qDebug()<<"CDataFile::loadPF  yDiv_N:  "<<yDiv_N;

        //! reading Mask RuleWfm data
        DsoPoint *pData = new DsoPoint[trace_width];
        in.readRawData((char*)pData, sizeof(DsoPoint)* trace_width);

        _wfm.init(trace_width);
        _wfm.setPoint( pData, trace_width, trace_width, start);
        _wfm.setyGnd( yGnd);
        _wfm.setyDiv( dsoFract( yDiv_M, yDiv_N));
        delete[] pData;
        void*ptr = (void*)&_wfm;
        serviceExecutor::post( E_SERVICE_ID_MASK,
                               servMask::cmd_mask_data_acq, ptr);
        qDebug()<<"Finish post(serv_name_mask,cmd_mask_data_acq)";

        file.close();
        ptr = NULL;
        qDebug()<<"CDataFile::loadPF Finish";
    }
    return ERR_NONE;
}
