#include "storage_wav.h"

bool CWFMFile::writeHeader(QByteArray session)
{
    mPastCount = 0;

    Q_ASSERT(m_pFile !=NULL );
    setupInfo info;
    info.version = 0x00000001;
    info.crc     = 55;
    info.size    = session.size();

    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::ReadWrite);  //使用QDataStream将Qpoint序列化进QByteArray内
    out.setVersion(m_version);                         //设置Qt串行化版本
    out<<info;

    qint32 headerSize = datagram.size();

    LOG_DBG()<<"Header size :" <<headerSize
            <<"Info size :"   <<info.size
            <<"session size :"<<session.size();

    //! header size
    if( sizeof(headerSize) != m_pFile->write( (char*)(&headerSize), sizeof(headerSize) ) )
    {
        LOG_DBG()<<"file write failure";
        return false;
    }

    //! header data
    if( datagram.size() != m_pFile->write(datagram) )
    {
        LOG_DBG()<<"file write failure";
        return false;
    }

    //! setup data
    if( session.size() != m_pFile->write(session) )
    {
        LOG_DBG()<<"file write failure";
        return false;
    }

    return true;
}

bool CWFMFile::readHeader(QByteArray &session)
{
    m_frameSize = 0;
    mPastCount  = 0;

    Q_ASSERT(m_pFile !=NULL );

    qint32 headerSize = 0;
    if(  sizeof(headerSize) != m_pFile->read( (char*)(&headerSize), sizeof(headerSize) ) )
    {
        LOG_DBG()<<"file read failure";
        return false;
    }

    setupInfo info;
    QByteArray datagram = m_pFile->read( headerSize );

    QDataStream dsIn(&datagram, QIODevice::ReadWrite); //使用QDataStream将QByteArray反序列化
    dsIn.setVersion(m_version);                        //设置Qt串行化版本
    dsIn>>info;

    session = m_pFile->read( info.size );
    if(session.size() != (int)info.size)
    {
        qDebug()<<__FILE__<<__LINE__<<"file read failure";
        return false;
    }

    mPastCount += info.size;

    LOG_DBG()<<"header size:"<<headerSize
            <<"Info size :"<<info.size
            <<"session size:"<<session.size();

    return true;
}

bool CWFMFile::writeFrameInfo(stFrmInfo &frameInfo)
{
    LOG_DBG()<<"frame:"<<frameInfo.frame
            <<"size:"<<frameInfo.size;

    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::ReadWrite);  //使用QDataStream将Qpoint序列化进QByteArray内
    out.setVersion(m_version);                         //设置Qt串行化版本
    out<<frameInfo;


    if( datagram.size() != m_pFile->write(datagram) )
    {
        LOG_DBG()<<"file write failure";
        return false;
    }

    return true;
}

bool CWFMFile::writeData(quint8    *pBuf,
                         quint64   size,
                         quint64   totalCount)
{
    Q_ASSERT(m_pFile !=NULL );
    return CBinFile::writeData(pBuf, size, totalCount);
}

bool CWFMFile::readFrameInfo(stFrmInfo &frameInfo)
{
    if(m_frameSize > 0)
    {
        LOG_DBG()<<"error: frameSize != 0";
        return false;
    }

    //! --------------------------------------------------------------------------------------
    //!为了避免结构提对齐问题，使用这种方式计算结构提大小。
    QByteArray data;
    QDataStream out(&data, QIODevice::ReadWrite);  //使用QDataStream将Qpoint序列化进QByteArray内
    out.setVersion(m_version);                         //设置Qt串行化版本
    out<<frameInfo;

    int size = data.size();   //! 不可使用 sizeof(frameInfo)
    LOG_DBG()<<"size:"<<size <<"sizeof size"<<sizeof(frameInfo);
    //! --------------------------------------------------------------------------------------

    QByteArray datagram = m_pFile->read( size );
    QDataStream dsIn(&datagram, QIODevice::ReadWrite); //使用QDataStream将QByteArray反序列化
    dsIn.setVersion(m_version);                        //设置Qt串行化版本
    dsIn>>frameInfo;
    LOG_DBG()<<frameInfo.frame<<frameInfo.size;

    m_frameSize = frameInfo.size;
    mPastCount  += size;
    return true;
}

qint64 CWFMFile::readData(quint8     *pBuf,
                          quint64     size)
{
    Q_ASSERT(pBuf != NULL);

    /*-------------------------------进度条-----------------------------*/
    quint64 progValue = mPastCount * 100 / m_pFile->size();
    servFile::setProgress(progValue);
    /*-------------------------------------------------------------------*/

    int readSize = qMin(size, m_frameSize) ;
    int retSize  = m_pFile->read((char*)pBuf, readSize);
    if(retSize > 0)
    {
        m_frameSize -= retSize;
        mPastCount  += retSize;
        return retSize;
    }
    else
    {
        return -1;
    }  
}
