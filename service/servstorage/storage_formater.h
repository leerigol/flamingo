#ifndef SCPIFORMATER_H
#define SCPIFORMATER_H

#include "../../com/scpiparse/cconverter.h"
using namespace scpi_parse;


class CImageFormater:public CFormater
{
public:
    CImageFormater();
    ~CImageFormater();
public:

    QString path;
    QFile *pfile;
    scpiError format( GCVal &val );
    quint32   getOutData(QByteArray &buff, quint32 len);
    bool      isWaiting();

    static bool mCmdValueReady;
};


class CPathFormater:public CStringFormater
{
public:
    CPathFormater();
    ~CPathFormater();
public:

    bool      isWaiting();

    static bool mCmdValueReady;
};
#endif // SCPIFORMATER_H

