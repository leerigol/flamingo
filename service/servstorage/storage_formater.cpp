#include "storage_formater.h"
#include "servstorage.h"
#include "../servscpi/servscpidso.h"

//#define  INTERFACE_TEST

/* For screen capture by SCPI */
QString servStorage::getImage(CArgument& type)
{
    QString path = "/tmp/snap.bmp";
# ifndef INTERFACE_TEST
    int fmt = servFile::FILETYPE_BMP;
    if( type.size() )
    {
        type.getVal(fmt);
    }

    switch(fmt)
    {
        case servFile::FILETYPE_PNG:
             path = "/tmp/snap.png" ;
             break;

        case servFile::FILETYPE_BMP:
            path = "/tmp/snap.bmp";
            break;

        case servFile::FILETYPE_JPG:
            path = "/tmp/snap.jpg";
            break;

        case servFile::FILETYPE_TIF:
            path = "/tmp/snap.tif";
            break;
    }

    m_pFileProc->snap(path, FUNC_SAVE_IMG);
    setThreadProc(servFile::ProcSnaping);

#else
    qDebug()<<"get image";
#endif

    return path;
}

/* For webcontrol */
#define FILE_START 10
QString servStorage::getImagePath(CArgument& type)
{
    QString pathName = "/tmp/";

    static int fileCount = FILE_START;
    QString fileName = "1.png";
    QString oldName = "0.png";
    ;
# ifndef INTERFACE_TEST
    int fmt = servFile::FILETYPE_PNG;
    if( type.size() )
    {
        type.getVal(fmt);
    }
    switch(fmt)
    {
        case servFile::FILETYPE_PNG:
             fileName = QString("%1.png").arg(fileCount);
             oldName  = QString("%1.png").arg(fileCount-FILE_START);
             break;

        case servFile::FILETYPE_JPG:
            fileName = QString("%1.jpg").arg(fileCount);
            oldName  = QString("%1.jpg").arg(fileCount-FILE_START);
            break;

        case servFile::FILETYPE_TIF:
            fileName = QString("%1.tif").arg(fileCount);
            oldName  = QString("%1.tif").arg(fileCount-FILE_START);
            break;
    }
    //remove the last file
    {
        QString filePath = pathName + oldName;
        if( QFile::exists( filePath ) )
        {
            QFile::remove(filePath);
        }
    }

    m_pFileProc->webPull(pathName + fileName);

    fileCount++;

#else
    qDebug()<<"get image";
#endif
    QString url = "/img/" + fileName;

    return url;
}

bool CImageFormater::mCmdValueReady = true;

CImageFormater::CImageFormater()
{
    pfile = NULL;
    mCmdValueReady = false;
}

CImageFormater::~CImageFormater()
{
    if( pfile != NULL)
    {
        pfile->close();
        delete pfile;
    }
    QFile::remove(path);
    path.clear();
}

scpiError CImageFormater::format(GCVal &val)
{
    path.clear();
    pfile = NULL;

    if(val.vType == val_string)
    {
        path = val.strVal;
    }

    return scpi_err_none;
}

bool CImageFormater::isWaiting()
{
# ifndef INTERFACE_TEST
//    int proc = 100;
//    serviceExecutor::query(serv_name_storage, servStorage::FUNC_THREAD_PROC, proc);
//    return (proc != 0)? true:false;

    return (!mCmdValueReady);
#else
    return false;
#endif

}


quint32 CImageFormater::getOutData(QByteArray &buff, quint32 len)
{
    //!打开文件，生成头信息
    if((!path.isEmpty()) && ( NULL == pfile))
    {
        if( !QFile::exists(path) )
        {
            return 0;
        }

        pfile = new QFile(path);
        Q_ASSERT(NULL != pfile);

        if( pfile->open(QIODevice::ReadOnly))
        {
            mOutDataLen = pfile->size();

            mOutStr.append(QString("#9%1").arg(mOutDataLen,9,10,QLatin1Char('0')));

            mOutDataLen += mOutStr.size();
        }
        else
        {
            return 0;
        }
    }

    //! 如果缓存中的数据量小于接口请求的数据量，则从文件中读取数据来补够接口请求的数据量
    //! 若缓存中的数据量大于接口的请求数量，则直接从缓存中读取。
    if((int)len > mOutStr.size())
    {
        Q_ASSERT(NULL != pfile);
        mOutStr.append( pfile->read(len - mOutStr.size()) );
        buff = mOutStr;
        mOutStr.clear();
    }
    else
    {
        buff.append(mOutStr.data(),len);
        mOutStr.remove(0,len);
    }

    mOutDataLen -= buff.size();
    return buff.size();
}

//////////////////////////////////////////////////////////////////////////
bool CPathFormater::mCmdValueReady;

CPathFormater::CPathFormater()
{
    mCmdValueReady = false;
}

CPathFormater::~CPathFormater()
{

}

bool CPathFormater::isWaiting()
{
    return !mCmdValueReady;
}
