#include <QWidget>
#include "storage_api.h"
#include "../service_name.h"
#include "../service_msg.h"
#include "../servref/servref.h"
#include "../servstorage/servstorage.h"
#include "../servutility/servutility.h"
#include "../servdecode/decTool/dectablesave.h"
#include "../servDG/servdg.h"
#include "../servSearch/servsearch.h"
#include "../servmemory/servmemory.h"
#include "../servmath/servmath.h"
#include "../servtrace/servtrace.h"
#include "../servhori/servhori.h"

#include "storage_img.h"
#include "storage_wav.h"
#include "storage_dat.h"


int servFile::m_nStep = 0;
servFile::servFile()
{
    m_pThread   = NULL;
    m_pWebPull  = NULL;
    m_nCallerID = 0;
}

void servFile::rst()
{
    m_nFormat   = servStorage::FUNC_SAVE_IMG;
}

bool servFile::getRun()
{
    if( m_pThread && m_pThread->getRun() )
    {
        return true;
    }
    return false;
}

CFileThread* servFile::getThread()
{
    //Run Once
    if(m_pThread == NULL)
    {
        m_pThread = new CFileThread(this);
        Q_ASSERT(m_pThread != NULL);
    }

    return m_pThread;
}

int servFile::save(int fmt , int ext)
{   
    QString name;
    serviceExecutor::query(serv_name_storage,
                           MSG_STORAGE_FILEPATH,  name);

    m_nFormatExt = ext;
    save(name,fmt);
    return ERR_NONE;
}

int servFile::save(QString& fn, int fmt, int ext )
{
    m_FilePathName = fn;
    m_nFormat = fmt;
    m_nMethod = ProcSaving;
    m_nFormatExt = ext;
    getThread()->start();
    return ERR_NONE;
}


int servFile::load(int fmt)
{
    QString name;
    serviceExecutor::query(serv_name_storage,
                           MSG_STORAGE_FILEPATH, name);
    return load(name, fmt);
}

int servFile::load(QString &fn, int fmt)
{
    m_FilePathName = fn;
    m_nFormat = fmt;
    m_nMethod = ProcLoading;
    getThread()->start();

    return ERR_NONE;
}

int servFile::snap(QString& fn, int fmt)
{
    m_nFormat = fmt;
    m_nMethod = ProcSnaping;
    m_dstPath = fn;
    getThread()->start();
    return ERR_NONE;
}

int servFile::sync(int)
{
    m_nMethod = ProcSync;
    getThread()->start();
    return ERR_NONE;
}

int servFile::mail(QString &fn)
{
    m_nFormat = servFile::FILETYPE_STP;
    m_nMethod = ProcSaveSTP;
    m_dstPath = fn;
    getThread()->start();
    return ERR_NONE;
}


int servFile::copy(const QString &src,
                   const QString &dst,
                   const QStringList &ext)
{
    m_srcPath = src;
    m_dstPath = dst;
    m_fileExt = ext;
    m_nMethod = ProcCopying;
    getThread()->start();
    return ERR_NONE;
}

int servFile::upgrade(const QString &f)
{
    m_srcPath = f;
    m_nMethod = ProcUpgrading;
    //qDebug() << getThread()->isRunning() ;
    getThread()->start();
    return ERR_NONE;
}

int servFile::diskFormat()
{
    m_nMethod = ProcFormating;

    getThread()->start();
    return ERR_NONE;
}

int servFile::restoreDefault()
{
    m_nMethod = ProcRestoring;
    getThread()->start();
    return ERR_NONE;
}

int servFile::webPull(QString fn)
{
    m_dstPath = fn;

    //Run Once
    if(m_pWebPull == NULL)
    {
        m_pWebPull = new CWebControlThread(this);
        Q_ASSERT(m_pWebPull != NULL);
    }
    m_pWebPull->start();

    return ERR_NONE;
}

/////////////////////////////////////////////////////////////////////////////
int servFile::copyFolder(const QString &srcPath,
                         const QString &dstPath,
                         const QStringList& ext)
{
    QDir sourceDir(srcPath);
    QDir targetDir(dstPath);

    if(!targetDir.exists())
    {    /**< 如果目标目录不存在，则进行创建 */
        if(!targetDir.mkdir(targetDir.absolutePath()))
        {
            return ERR_FILE_OP_FAIL;
        }
    }

    QFileInfoList fileInfoList = sourceDir.entryInfoList();
    foreach(QFileInfo fileInfo, fileInfoList)
    {
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
        {
            continue;
        }
//        qDebug() << fileInfo.fileName();
        if(fileInfo.isDir())
        {
            /**< 当为目录时，递归的进行copy */
            if(ERR_NONE != copyFolder(fileInfo.filePath(),
                                      targetDir.filePath(fileInfo.fileName()),ext))
            {
                return ERR_FILE_COPY_FAIL;
            }
        }
        else
        {
            /**< 当允许覆盖操作时，将旧文件进行删除操作
            if(targetDir.exists(fileInfo.fileName()))
            {
                if(bCoverExist)
                {
                    targetDir.remove(fileInfo.fileName());
                }
                else
                {
                    return true;//not overwrite
                }
            }*/

            /// 进行文件copy
            if( ERR_NONE != copyFile(fileInfo.filePath(),
                                     targetDir.filePath(fileInfo.fileName())))
            {
                return ERR_FILE_COPY_FAIL;
            }
        }
    }
    return ERR_NONE;
}

int servFile::copyFile(const QString &srcPath, const QString &dstPath)
{
    const int BUF_SIZE_LARGE = 1024 << 10;
    const int BUF_SIZE_SMALL = 4096;

    int size_div = BUF_SIZE_LARGE;
    int buf_size = BUF_SIZE_LARGE;

    QFile srcFile;
          srcFile.setFileName(srcPath);

    if(!srcFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "open srcFile failed！！！";
        return ERR_FILE_OP_FAIL;
    }
    QDataStream in(&srcFile);
                in.setVersion(QDataStream::Qt_5_5);

    int fileSize  = srcFile.size();
    if(fileSize < BUF_SIZE_LARGE)
    {
        buf_size = BUF_SIZE_SMALL;
        size_div = BUF_SIZE_SMALL;
    }
    //qDebug() << "copy size" << fileSize;
    initProgress(m_srcPath, sysGetString(MSG_STORAGE_COPY,"Copy"),fileSize/size_div+1);

    QFile dstFile;
          dstFile.setFileName(dstPath);

     //overwrite
    if(!dstFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        qDebug() << "open dstFile failed!!!";
        hideProgress();
        srcFile.close();
        return ERR_FILE_OP_FAIL;
    }

    QDataStream out(&dstFile);
                out.setVersion(QDataStream::Qt_5_5);

    int readSize   = 0;
    int totalSize  = 0;

    char *ps8ByteTemp = new char[buf_size];//字节数组
    if( ps8ByteTemp != NULL )
    {
        memset(ps8ByteTemp, 0, buf_size);

        while(!in.atEnd())
        {
            //读入字节数组,返回读取的字节数量，如果小于1MiB，则到了文件尾
            readSize = in.readRawData(ps8ByteTemp, buf_size);
            out.writeRawData(ps8ByteTemp, readSize);
            totalSize += readSize;
            setProgress();
        }
        delete []ps8ByteTemp;
        ps8ByteTemp = NULL;
    }

    srcFile.close();
    dstFile.close();
    hideProgress();

    if(totalSize == fileSize)
    {
        return ERR_NONE;
    }
    else
    {
        return ERR_FILE_OP_FAIL;
    }
}

/* processbar will filter the service name */
int servFile::loadFile()
{
    int nRet = ERR_NONE;

    //qDebug() << m_FilePathName << "---------";
    initProgress(servStorage::getFrendlyPath(m_FilePathName),
                 sysGetString(MSG_STORAGE_LOAD,"Load"),
                 100);

    if(servStorage::FUNC_LOAD_WAV == m_nFormat)
    {
        setProgress(1);
    }
    else
    {
        setProgress(50);
    }
    QThread::msleep(100);//wait to shwo progressbar

    switch(m_nFormat)
    {
        case servStorage::FUNC_LOAD_REF:
        {
            nRet = CWaveFile::loadREF(m_FilePathName);
            if(nRet == ERR_NONE)
            {
                serviceExecutor::post(E_SERVICE_ID_REF,
                                      servRef::MSG_REF_SHOW, -1);//(int)-1
            }
        }
        break;

        case servStorage::FUNC_LOAD_WAV:
        {
            nRet = CWaveFile::loadWFM(m_FilePathName);
        }
        break;

        case servStorage::FUNC_LOAD_STP:
        {
            engine::m_bFreeze = true;
            nRet = CDataFile::loadSTP(m_FilePathName);
            serviceExecutor::post(E_SERVICE_ID_HORI,
                                  servHori::cmd_unfreeze_engine, 0);
        }
        break;

        case servStorage::FUNC_LOAD_ARB:
        {
            nRet = loadARB();
        }
        break;

        case servStorage::FUNC_LOAD_MSK:
        {
            nRet = loadPF();
        }
        break;
    }


    setProgress(99);
    QThread::msleep(300);
    hideProgress();

    return nRet;
}

int servFile::snapFile()
{
    CImageFile::saveIMG(m_dstPath);
    return ERR_NONE;
}

int servFile::saveSTP()
{
    CDataFile::saveSTP(m_dstPath);
    sync2Flash(ERR_NONE);
    return ERR_NONE;
}

int servFile::saveFile()
{
    int nRet = ERR_NONE;

    QImage rawData;
    if(m_nFormat == servStorage::FUNC_SAVE_IMG )
    {
        //for bug1669
        QThread::sleep(1);//wait dialog close

        bool header = false;
        serviceExecutor::query(serv_name_storage, MSG_STORAGE_IMAGE_HEADER, header);
        nRet = CImageFile::getRawData(rawData, header);
        if(nRet != ERR_NONE )
        {
            return nRet;
        }
    }


    initProgress(servStorage::getFrendlyPath(m_FilePathName),
                 sysGetString(MSG_STORAGE_SAVE,"Save"),
                 100);
    if(m_nFormat != servStorage::FUNC_SAVE_WAV &&
       m_nFormat != servStorage::FUNC_SAVE_CSV)
    {
        QThread::msleep(10);
        setProgress(50);
    }


    switch(m_nFormat)
    {
        case servStorage::FUNC_SAVE_IMG:
             nRet = CImageFile::saveIMG(rawData,m_FilePathName);
             break;

        case servStorage::FUNC_SAVE_GIF:
             break;

        case servStorage::FUNC_SAVE_CSV:
             nRet = CWaveFile::saveCSV(m_FilePathName, false);
             break;

        case servStorage::FUNC_SAVE_WAV:
        case servStorage::FUNC_SAVE_REF:
             nRet = CWaveFile::saveWAV(m_FilePathName,
                                       m_nFormat == servStorage::FUNC_SAVE_REF,
                                       m_nFormatExt);
             break;

        case servStorage::FUNC_SAVE_STP:
             nRet = CDataFile::saveSTP(m_FilePathName);
             break;

        case servStorage::FUNC_SAVE_MSK:
             nRet = saveMSK();
             break;

        case servStorage::FUNC_SAVE_DEC:
             nRet = CDecSave::saveEventtable(m_FilePathName);
             break;

        case servStorage::FUNC_SAVE_DECDAT:
             nRet = CDecSave::savePayloadtable(m_FilePathName);
            break;

        case servStorage::FUNC_SAVE_ARB:
            nRet = saveARB();
            break;

        case servStorage::FUNC_SAVE_SEARCH:
            nRet = saveSEARCH(m_FilePathName);
            break;

        case servStorage::FUNC_SAVE_REC:
            nRet = saveRECORD(m_FilePathName);
            break;

        case servStorage::FUNC_SAVE_FFT:
            nRet = saveFFTSEARCH(m_FilePathName);
            break;

    }

    setProgress(99);

    sync2Flash(nRet);
    QThread::msleep(100);
    hideProgress();

    //qDebug() << __FUNCTION__ << __LINE__ << "SAVING IMAGE";

    return nRet;
}


int servFile::saveHTM()
{
    QFile file(m_FilePathName);
    if( file.open(QIODevice::WriteOnly) )
    {
        file.write(m_FilePathName.toLocal8Bit());
        file.close();
        return ERR_NONE;
    }
    return ERR_FILE_SAVE_FAIL;
}

int servFile::saveSEARCH(QString &fname)
{
    int ret = servSearch::saveTable(fname);
    return ret;
}

int servFile::saveFFTSEARCH(QString &fname)
{
    int ret = servMath::saveTable(fname);
    return ret;
}

int servFile::saveRECORD(QString &fname)
{
    int start = 0;
    int end   = 0;
    serviceExecutor::query(serv_name_wrec,
                           MSG_RECORD_FROMFRAME, start);
    serviceExecutor::query(serv_name_wrec,
                           MSG_RECORD_TOFRAME,   end);
    start = qMax(start - 1, 0    );
    end   = qMax(end - 1  , start);

    int ret = servMemory::s_exportAllMemory(fname, start, end);
    return ret;
}

int servFile::saveARB(void)
{
    int ret = servDG::saveArb(m_FilePathName);
    return ret;
}

int servFile::saveTXT(QString&)
{
    return saveHTM();
}

int servFile::saveMSK_CSV(void)
{
    return saveHTM();
}

int servFile::saveMSK_PF(void)
{
    return saveHTM();
}

int servFile::saveMSK()
{
    int f;
    int ret = ERR_NONE;
    qDebug() << "save mask";
    serviceExecutor::query( serv_name_storage, MSG_STORAGE_FILETYPE, f );
    if(f == FILETYPE_TXT)
    {
        QString txt = QString("hello world");
        ret = saveTXT(txt);
    }
    else if(f == FILETYPE_CSV)
    {
        ret = saveMSK_CSV();
    }
    else if(f == FILETYPE_HTM)
    {
        ret = saveHTM();
    }
    else if(f == FILETYPE_PF)
    {
        ret = CDataFile::savePF(m_FilePathName);
    }
    else
    {
        ret = ERR_NONE;
    }
    return ret;
}


int servFile::loadARB()
{
    servDG::loadArb(m_FilePathName);
    qDebug() << "Implement your loading for" << m_FilePathName;
    return ERR_NONE;
}

int servFile::loadPF()
{
    return CDataFile::loadPF(m_FilePathName);
}

void servFile::initProgress(const QString& info, const QString& title, int count)
{

    //this pointer will be deleted on appGui
    CtrlProgress* pProgress = new CtrlProgress();
    Q_ASSERT( NULL != pProgress );

    struServItem *item = service::findItem( serv_name_storage );

    pProgress->mInfo         = info;
    pProgress->mTitle        = title;
    pProgress->mbShow        = true;

    pProgress->mClientId     = item->servId;
    pProgress->mClientMsg    = -1;//not show the cancel
    pProgress->mClientPara   = -1;
    pProgress->mValA         = 0;
    pProgress->mValB         = count;
    pProgress->mVal          = 0;
    pProgress->mStep         = 1;
    m_nStep                  = 0;

    CArgument arg;
    arg.setVal( pProgress );

    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_progress_cfg,
                           arg );
}


void servFile::setProgress(int value, const QString& info)
{
    if(value == -1)
    {
        value = (++m_nStep);
    }
    else
    {
        m_nStep = value;
    }

    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_progress_val,
                           value,
                           NULL );

    if( info.size() > 0 )
    {
        QString msg = info;
        serviceExecutor::post( E_SERVICE_ID_UI,
                               servGui::cmd_progress_info,
                               msg,
                               NULL );
    }
}

void servFile::hideProgress()
{
    bool visible = false;
    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_progress_visible,
                           visible,
                           NULL );
}

void servFile::sync2Flash(int nRet)
{
    if(nRet == ERR_NONE)
    {
        //flush to Nand
        QProcess::execute("sync");
    }
}

///////////////////////////////////////////////////////
///             FILE IO ROUTINES END                ///
///////////////////////////////////////////////////////
CFileThread::CFileThread( QObject *parent ): QThread()
{
    m_pFileProc = dynamic_cast<servFile*>( parent );
    bRunning = false;
}

bool CFileThread::getRun()
{
    return bRunning;
}

int CFileThread::doUpgradeGEL(const QString& gel)
{
    QStringList arg;
    QString output;

    QString msg = sysGetString(INF_UPGRADE_H1,"Do not poweroff") + "\n" +
                  sysGetString(INF_UPGRADE_H2,"Do not remove the USB storage device");

    QProcess* pProcess = new QProcess;
    Q_ASSERT(pProcess != NULL);
    pProcess->moveToThread(this);

    int timeout = 200;
    //300 x 200ms.
    m_pFileProc->initProgress(msg,
                              sysGetString(INF_UPGRADE_MSG,
                              "Upgrading, please wait..."),
                              timeout);
    QThread::sleep(1);
    arg << gel;
    pProcess->start("/rigol/shell/update.sh", arg);

    while( pProcess->state() )
    {
        pProcess->waitForFinished(200);
        output = pProcess->readAllStandardOutput();
        if( output.size() > 0 )
        {
            output.remove("\n");
            if(pProcess->state() == QProcess::NotRunning &&
                    pProcess->exitCode() != 0)
            {
                break;
            }            
        }

        if( m_pFileProc->getStep() >= timeout)
        {
            m_pFileProc->setProgress(timeout, msg);
        }
        else
        {
            m_pFileProc->setProgress(-1, msg);
        }
    }
    if(pProcess->exitCode() != 0)
    {
        //qDebug() << "upgrade exit :" << pProcess->exitCode();
        m_pFileProc->hideProgress();
        return ERR_FILE_OLD_VER;
    }
    else
    {
        m_pFileProc->setProgress(timeout,
                                 sysGetString(INF_UPGRADE_H3,
                                 "Upgrading completed! Rebooting..."));
        QThread::sleep(2);
    }
    return ERR_NONE;
}

int CFileThread::doDiskFormat()
{
    QProcess::execute("/rigol/shell/format_disk.sh");
    return ERR_NONE;
}

#include "../servicefactory.h"
int CFileThread::doRestoreDefault()
{
    //! create context
    struActionContextRmt defContext( false, false );
    QSemaphore sema;

    defContext.m_pSema = &sema;
    defContext.isUser = false;

    //! stop trace
    if( menu_res::RMessageBox::isUsed() )
    {
        menu_res::RMessageBox::Hide();
    }

    //force ccu stop
    if( sysGetSystemStatus() == 0 )
    {
        serviceExecutor::send(serv_name_hori,
                              servHori::cmd_control_status,
                              Control_Run);
        QThread::msleep( 100 );
    }
    serviceExecutor::send(serv_name_hori,
                          servHori::cmd_control_status,
                          Control_Stop);

    engine::m_bFreeze = true;

    servGui::showInfo( INF_MODULE_DEFAULTING,Qt::white,-1,-1,2000);
    QThread::msleep( 300 );

    QList<service*>* pServList = serviceFactory::getServiceList();
    int size = pServList->size();
    //! rst
    for(int i=0; i<size; i++)
    {
        service* pServ =  pServList->at(i);
        serviceExecutor::post( pServ->getId(),
                               CMD_SERVICE_RST,
                               (int)0,
                               &defContext );

    }



    //! startup
    for(int i=0; i<size; i++)
    {
        service* pServ =  pServList->at(i);
        serviceExecutor::post( pServ->getId(),
                               CMD_SERVICE_STARTUP,
                               (int)0,
                               &defContext );
    }

    //! check progress
    while( sema.available() != size * 2 )
    {
        QThread::msleep( 10 );
    }


    serviceExecutor::post(E_SERVICE_ID_HORI,
                          servHori::cmd_unfreeze_engine,
                          0);

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //!!!!!!!!!!!!!not a good solution. you can change it.
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    QThread::msleep( 100 );

    serviceExecutor::post( E_SERVICE_ID_HORI, MSG_HORIZONTAL_RUN, Control_Run);
    serviceExecutor::post( E_SERVICE_ID_CH1,  CMD_SERVICE_ACTIVE, 1 );

    //! 复位操作完成，解锁scpi。
    sysScpiExeUnLock();

    //qDebug() << "after:" << QTime::currentTime();
    return ERR_NONE;
}

/*The real procedure of file operation*/
void CFileThread::run()
{
    bRunning = true;
    serviceExecutor::send(serv_name_utility,
                          servUtility::cmd_lock_input,
                          bRunning);

    //disabled scpi
    sysScpiExeLock();

    //1-fail, 0-success
    int nResult = ERR_NONE;
    servFile::Operation method = m_pFileProc->getMethod();

    if(method == servFile::ProcSaving)
    {
        nResult  = m_pFileProc->saveFile();
    }
    else if(method == servFile::ProcSnaping)
    {
        nResult = m_pFileProc->snapFile();
    }
    else if(method == servFile::ProcLoading)
    {
        nResult = m_pFileProc->loadFile();
    }
    else if(method == servFile::ProcSaveSTP)
    {
        nResult = m_pFileProc->saveSTP();        
    }
    else if(method == servFile::ProcCopying)
    {
        QFileInfo fi(m_pFileProc->m_srcPath);
        if(fi.isDir())
        {
            //qDebug() << "starting copy folder" << fi.fileName();
            nResult = m_pFileProc->copyFolder(m_pFileProc->m_srcPath,
                                              m_pFileProc->m_dstPath,
                                              m_pFileProc->m_fileExt);
        }
        else
        {
            nResult = m_pFileProc->copyFile(m_pFileProc->m_srcPath,
                                            m_pFileProc->m_dstPath);
        }
        m_pFileProc->sync2Flash(nResult);
    }
    else if(method == servFile::ProcUpgrading)
    {
        nResult = doUpgradeGEL(m_pFileProc->m_srcPath);
    }
    else if(method == servFile::ProcFormating)
    {
        nResult = doDiskFormat();
    }
    else if(method == servFile::ProcRestoring)
    {
        nResult = doRestoreDefault();
    }
    else if( method == servFile::ProcSync )
    {
        m_pFileProc->sync2Flash( ERR_NONE );
    }

    //qDebug() << nResult << m_pFileProc->getCaller();

    if(m_pFileProc->getCaller())
    {
        serviceExecutor::post( m_pFileProc->getCaller(),
                               MSG_STORAGE_RESULT, nResult);
    }
    bRunning = false;

    serviceExecutor::post(serv_name_utility,
                          servUtility::cmd_lock_input,
                          bRunning);

    sysScpiExeUnLock();
}

/*****************************************************************************/
CWebControlThread::CWebControlThread( QObject *parent ): QThread()
{
    m_pFileProc = dynamic_cast<servFile*>( parent );
}
void CWebControlThread::run()
{
    int nRet = CImageFile::saveIMG(m_pFileProc->m_dstPath);
    if(m_pFileProc->getCaller())
    {
        serviceExecutor::post( m_pFileProc->getCaller(),
                               servStorage::FUNC_WEB_PULL, nRet);
    }
}
