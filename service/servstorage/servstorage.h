#ifndef SERVSTORAGE_H
#define SERVSTORAGE_H

#include <unistd.h>
#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../../include/dsotype.h"
#include "../../include/dsostd.h"
#include "./storage_api.h"


#ifndef _SIMULATE
#define STORAGE_USER_PATH   QString("/user/data")
#define STORAGE_USB_PATH    QString("/media/sda1")
#define STORAGE_USB_MOUNT   QString("/media")
#define STORAGE_DOWN_PATH   QString("/user/download/")
#else
#define STORAGE_USER_PATH   QDir::currentPath()
#define STORAGE_USB_PATH    QString("/media/")
#define STORAGE_USB_MOUNT   QString("/media")
#define STORAGE_DOWN_PATH   QString("./")
#endif

#define STORAGE_USER_DISK   QString("C")
#define STORAGE_REVERVED    QString("#*@RIGOL*#")
#define STORAGE_UPGRADE_GEL QString("FlamingoUpdate.GEL")
/*
 * Remember the counter of every format file.
 * Probe once the counter for opening file
*/
struct tNameInfo
{
    tNameInfo(const QString& f = QString("png") )
    {
        ext = f;
        counter  = 0;
    }

    quint64 counter;
    QString      ext;
};

class servStorage : public serviceExecutor,
                    public ISerial
{
    Q_OBJECT

    DECLARE_CMD()

    public:
        enum storageFUNC
        {
            FUNC_SUB_MENU = 1,

            FUNC_STORAGE_PUSH,
            FUNC_STORAGE_INDEX,
            FUNC_STORAGE_PTR,
            FUNC_STORAGE_REDRAW,

            /* For quick operation*/
            FUNC_QUICK_SAVE_IMG,
            FUNC_QUICK_SAVE_WAV,
            FUNC_QUICK_SAVE_STP,
            FUNC_QUICK_LOAD_WAV,
            FUNC_QUICK_LOAD_STP,           
            FUNC_STORAGE_OPTION,

            FUNC_STORAGE_DISK,
            FUNC_STORAGE_DELETE,

            FUNC_STORAGE_PASTE_PRE, //FOR MESSAGE BOX
            FUNC_STORAGE_PASTE,

            FUNC_STORAGE_SAVE_PRE,
            FUNC_STORAGE_SAVE_ING,
            FUNC_STORAGE_SAVE,

            FUNC_STORAGE_RENAME,
            FUNC_STORAGE_NEWFOLDER,

            FUNC_RESTORE_DEFAULT,
            FUNC_STORAGE_FORMAT,
            FUNC_STORAGE_FILETYPE,

            FUNC_STORAGE_AUTOCHECK,
            FUNC_UPGRADE_NET,
            FUNC_UPGRADE_USB,
            FUNC_UPGRADE_RESULT,
            FUNC_THREAD_PROC,
            FUNC_WEB_PULL,
            FUNC_PULL_SETUP,
            FUNC_PUSH_SETUP,
            FUNC_PROC_STATUS,
            FUNC_RETURN_MENU,
            FUNC_SPY_LICENSE,


            FUNC_SAVE = 0x40,
            FUNC_LOAD = 0x80,

            FUNC_SAVE_IMG      = FUNC_SAVE,
            FUNC_SAVE_WAV      = FUNC_SAVE | 1,
            FUNC_SAVE_STP      = FUNC_SAVE | 2,
            FUNC_SAVE_REF      = FUNC_SAVE | 3,
            FUNC_SAVE_MSK      = FUNC_SAVE | 4,
            FUNC_EXPT_MSK      = FUNC_SAVE | 5,
            FUNC_SAVE_ARB      = FUNC_SAVE | 6,
            FUNC_SAVE_REC      = FUNC_SAVE | 7,
            FUNC_SAVE_DEC      = FUNC_SAVE | 8,
            FUNC_SAVE_DG       = FUNC_SAVE | 9,
            FUNC_SAVE_DECDAT   = FUNC_SAVE | 10,
            FUNC_SAVE_GIF      = FUNC_SAVE | 11,
            FUNC_SAVE_SEARCH   = FUNC_SAVE | 12,
            FUNC_SAVE_CSV      = FUNC_SAVE | 13,
            FUNC_SAVE_FFT      = FUNC_SAVE | 14,

            FUNC_LOAD_WAV  = FUNC_LOAD,
            FUNC_LOAD_STP  = FUNC_LOAD | 1,
            FUNC_LOAD_REF  = FUNC_LOAD | 2,
            FUNC_LOAD_MSK  = FUNC_LOAD | 3,
            FUNC_LOAD_ARB  = FUNC_LOAD | 4,
            FUNC_LOAD_GEL  = FUNC_LOAD | 5,  
            FUNC_LOAD_ATT  = FUNC_LOAD | 6,
            FUNC_LOAD_SCR  = FUNC_LOAD | 7,

        };
        enum enDataSrc
        {
            DATA_SRC_SCREEN,
            DATA_SRC_MEM,
        };
        enum
        {
            proc_doing,
            proc_done
        };


public:
    servStorage(QString name, ServiceId eId = E_SERVICE_ID_STORAGE);
    static QString& getFileExt(int f = -1);

    static QString  getRealDisk(QString);
    static QString  getRealPath(QString&);

    static QString  getFrendlyDisk(QString);
    static QString  getFrendlyPath(QString&);
    static QString  getDispDisk(QString);
    static QString  getDispPath(QString&);
    static qint64   getFreeSpace( QString& );
    static    bool  isDefaulting();
    static int      getRemovableList( QList<QString>&);
    static bool     isPathExist(QString&);
    static bool     isUKeyReady();
    const static int MAX_FILENAME_LEN = 10;

public:
    //from service
    //DsoErr      start();
    //from serial
    int         serialOut( CStream &stream, serialVersion &ver );
    int         serialIn( CStream &stream, serialVersion ver );
    void        rst();
    void        init();
    int         startup();
    void        registerSpy();

    int         onDeActive(int);

public:

    DsoErr      setPrefixName(QString&);
    QString     getPrefixName(void);
    void        chkPrefixName(QString);

    DsoErr      setFileName(QString&);
    QString     getFileName(void);

    DsoErr      setAutoName(bool);
    bool        getAutoName();

    QString     getFilePathName(void);

    DsoErr      setPathName(const QString&);
    QString     getPathName(void);

    DsoErr      setImageFormat(int );
    int         getImageFormat();

    DsoErr      setWaveFormat( int );
    int         getWaveFormat(void);

    DsoErr      setParamSave(bool );
    bool        getParamSave();

    DsoErr      setInvert(bool );
    bool        getInvert();

    DsoErr      setColorType(bool );
    bool        getColorType();

    DsoErr      setImageHeader(bool );
    bool        getImageHeader();

    DsoErr      setImageFooter(bool );
    bool        getImageFooter();

    DsoErr      setDialogHidden( bool );
    bool        getDialogVisible();

    DsoErr      setFileFormat(int );
    int         getFileFormat();

    DsoErr      setWaveDepth( bool );
    bool        getWaveDepth();

    DsoErr      setWaveParaSave( bool );
    bool        getWaveParaSave();

    DsoErr      setChannelSelected( int );
    int         getChannelSelected();

    DsoErr      setMemChannel(bool ch);
    bool        getMemChannel();

    DsoErr      setCsvTimeEnab(bool b);
    bool        getCsvTimeEnab();


    DsoErr      onEnterSubMenu(int);
    DsoErr      onExitSubMenu(int);

    DsoErr      onSave();
    DsoErr      doSave();
    DsoErr      doLoad();

    DsoErr      updateFromUSB(int);
    DsoErr      updateFromNET(const QString&);
    int         getUpgradeResult() { return m_nUpgradeResult; }

    DsoErr      setCheckFirmware(bool b=false);
    bool        getCheckFirmware();


    bool        setThreadProc(int);
    int         getThreadProc();

    int         setProcStatus(int);
    int         getProcStatus();

    DsoErr      onIntent(CIntent *pIntent);
    int         getSubMenu(void);
    int         getReturnMenu(void);

    QString     getImage(CArgument& type);
    QString     getImagePath(CArgument& type);


    //by scpi
    int         scpiSaveSetup(CArgument &path);
    int         loadSetup(QString &path);

    int         scpiSaveIMG(CArgument &path);
    int         scpiSaveCSV(CArgument &path);

    int         scpiSaveWave(CArgument& path);
    int         loadWave(QString& path);

    int         pushSetup(QString&);
    QString     pullSetup();

    int         setFileType(int);
    int         getFileType();

protected:
    virtual bool keyEat(int key, int count, bool bRelease );
    void         spyLicenseOpt();
private:
    int         doCopy(void);
    int         onPaste(void);
    int         doPaste(void);

    int         onRename(void);
    int         doRename(QString);

    int         onDelete(void);
    int         doDelete(void);

    int         onNewFolder(void);
    int         doNewFolder(QString);

    int         onFormatDisk(void);
    int         doFormatDisk(void);

    int         onRestoreDefault(void);
    int         doRestoreDefault(void);

    bool        isRootPath();
    void        initUsbMedia(void);


    //call back after saved.
    int         onResult(int);
    int         onWebPull(int);

    void        dirEnter(const QString &);
    void        dirReturn();

    int         onInsertUSB(const QString &);
    int         onRemoveUSB(const QString &);
    bool        checkUpgradeGEL(QString &path);
    bool        checkUKey( void );
    static bool ms_bIsUKeyReady;

    //for Tune
    void        onTunePush(int tune = 0); //0:enter 1:back
    int         doNULL() { return ERR_NONE;}


    //for Quick
    int         quickSaving(int);
    void        quickLoadWave(void);
    void        quickLoadSetup(void);

    //for diskmanager
    int         setNewEnable(bool);
    int         setCopyEnable(bool);
    int         setPasteEnable(bool);
    int         setRenameEnable(bool);
    int         setDeleteEnable(bool);
    int         setSaveEnable(bool);
    int         setLoadEnable(bool);
    int         setFormatEnable(bool);

    void*       getFileList(void);
    int         setFileIndex(int);

    void        updateList(int f = servFile::FILETYPE_PNG);
    void        readFiles(void);

    void        buildFileName(int fmg = servFile::FILETYPE_PNG);
    QString     buildFolderName();

private:
    int         m_nDoProc;
    int         m_nRetuenMenu;

    /*Disk file operations*/
    int         m_nThreadProc;

    int         m_nProcStatus;

    int         m_nChannel;
    bool        m_bMemWfmCh;
    bool        m_bCsvTime;

    int         m_nWaveDepth;
    bool        m_nWaveParaSave;
    int         m_nImageFormat;
    int         m_nWaveFormat;
    int         m_nFileIndex;

    // 0 is root, other is sub dir
    int         m_nPathSeries;

    QMap<int,QString> m_PreFixList;
    QString     m_Prefix;
    QString     m_FileName;
    bool        m_bAutoName;

    QString     m_PathName;
    int         m_nDigitCount; /* the count of the next suffix number*/
    quint64     m_nNextNumber;/*the next file suffix number*/

    QDir        m_Dir;
    QFileInfoList m_stFiles;
    QStringList   m_stFileExt;

    QFileInfo   m_DotDot;
    QFileInfo   m_UserPath;

    QString     m_CopiedName;
    QString     m_CopyFromPath;

    bool        m_bColorType;
    bool        m_bParamSave;
    bool        m_bInvert;
    bool        m_bImageHeader;
    bool        m_bImageFooter;
    bool        m_bCopiedFile;
    bool        m_bDialogClose;

    static      int m_nFileFormat;
    static      QHash<int,tNameInfo*>  m_NameInfo;
    /*All usb partitions mounted*/
    static      QList<QPair<QString, QString> > m_hFat;
    static volatile bool m_bDefaulting;
    ////////////////////////////////////////////////
    servFile*   m_pFileProc;
    //LocalSocketServer localServer;
    int         m_nUpgradeResult;
    bool        m_bAutoChecking;
    bool        m_bUpgradeMedia;//false usb,true net


    /*!*****************scpi***************/
    int m_fileType;

};

#endif // SERVSTORAGE_H

