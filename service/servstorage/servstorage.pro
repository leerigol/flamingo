######################################################################
# Automatically generated by qmake (3.0) ?? 10? 29 10:28:41 2016
######################################################################

QT       += core
QT       += gui
QT       += widgets
QT       += network
TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/services/servstorage
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc
DEFINES += _DSO_KEYBOARD

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


# Input
HEADERS += servstorage.h \
    storage_dat.h \
    storage_wav.h \
    storage_api.h \
    storage_img.h \
    storage_formater.h

SOURCES += servstorage.cpp \
    storage_bin.cpp \
    storage_wav.cpp \
    storage_api.cpp \
    storage_img.cpp \
    storage_formater.cpp \
    storage_dlg.cpp \
    storage_disk.cpp \
    storage_dat.cpp \
    storage_csv.cpp \
    storage_wfm.cpp
