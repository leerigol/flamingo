#include <fcntl.h> //O_RDONLY

#include "../../com/crypt/xxtea.h"
#include "../servhelp/servhelp.h"
#include "../servutility/servutility.h"
#include "../servutility/servmail/servmail.h"
#include "./storage_dat.h"

#include "servstorage.h"


volatile bool servStorage::m_bDefaulting = false;

bool servStorage::ms_bIsUKeyReady = false;
///
/// \brief restoring default
/// \return
///
int servStorage::onRestoreDefault()
{
    servGui::showMsgBox(this->getName(),
                        sysGetString(INF_DEFAULT,"Restore default?"),
                        servStorage::FUNC_RESTORE_DEFAULT);

    return ERR_NONE;
}

///
/// \brief servStorage::doRestoreDefault
/// \return
///
int servStorage::doRestoreDefault()
{
    //!复位操作执行时，锁定scpi，后续指令将在解锁后继续执行
    sysScpiExeLock();

    m_bDefaulting = true;
    setThreadProc(servFile::ProcRestoring);

    m_pFileProc->restoreDefault();

    return ERR_NONE;
}

///
/// \brief servStorage::isDefaulting
/// \return
///
bool servStorage::isDefaulting()
{
    return m_bDefaulting;
}

///
/// \brief servStorage::onSave
/// \return
///
DsoErr servStorage::onSave()
{
    QString fname = m_PathName + "/" + m_FileName;
    if( QFile::exists(fname) )
    {
        defer(servStorage::FUNC_STORAGE_SAVE_PRE);
    }
    else
    {
        defer(servStorage::FUNC_STORAGE_SAVE_ING);
    }
    //return doSave();
    return ERR_NONE;
}

///
/// \brief servStorage::doSave
/// \return
///
DsoErr servStorage::doSave()
{
    setProcStatus(proc_doing);
    setThreadProc(servFile::ProcSaving);
    m_pFileProc->save( m_nDoProc );

    /* disable button */
    setSaveEnable(false);
    return ERR_NONE;
}


int servStorage::scpiSaveWave(CArgument &p)
{
    setThreadProc(servFile::ProcSaving);
    setProcStatus(proc_doing);


    if( p.size() == 1 )
    {
        QString fname;
        p.getVal( fname );

        fname = getRealPath(fname);
        fname = fname.replace("//", QChar('/'));
        fname = fname.replace(QChar('\\'), QChar('/'));

        if(fname.size() == 0)
        {
            return ERR_INVALID_INPUT;
        }

        int ext = servFile::FILETYPE_BIN;
        if( !fname.contains(".") )
        {
            fname += ".bin";
        }
        else if( fname.contains(".csv", Qt::CaseInsensitive))
        {
            ext = servFile::FILETYPE_CSV;
        }
        else if(fname.contains(".wfm" ,Qt::CaseInsensitive))
        {
            ext = servFile::FILETYPE_WFM;
        }
        else
        {
            fname += "bin";
        }

        m_pFileProc->save(fname, FUNC_SAVE_WAV , ext);
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    return ERR_NONE;
}

int servStorage::scpiSaveIMG(CArgument &p)
{
    setProcStatus(proc_doing);
    setThreadProc(servFile::ProcSaving);
    if( p.size() == 1 )
    {
        QString fname;
        p.getVal( fname );
        if(fname.size() == 0)
        {
            return ERR_INVALID_INPUT;
        }

        fname = getRealPath(fname);
        fname = fname.replace("//", QChar('/'));
        fname = fname.replace(QChar('\\'), QChar('/'));

        if( fname.contains(".png", Qt::CaseInsensitive) ||
            fname.contains(".jpg", Qt::CaseInsensitive) ||
            fname.contains(".jpeg", Qt::CaseInsensitive) ||
            fname.contains(".bmp", Qt::CaseInsensitive) ||
            fname.contains(".tif", Qt::CaseInsensitive) )
           {}
        else
        {
            switch(getImageFormat())
            {
            case servFile::FILETYPE_PNG:
                fname += ".png";
                break;
            case servFile::FILETYPE_BMP:
                fname += ".bmp";
                break;
            case servFile::FILETYPE_JPG:
                fname += ".jpg";
                break;
            case servFile::FILETYPE_TIF:
                fname += ".tif";
                break;
            default:
                fname += ".png";
                break;
            }
        }
        m_pFileProc->save(fname, FUNC_SAVE_IMG );
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    return ERR_NONE;
}

int servStorage::scpiSaveCSV(CArgument &p)
{
    setProcStatus(proc_doing);
    setThreadProc(servFile::ProcSaving);
    if( p.size() == 1 )
    {
        QString fname;
        p.getVal( fname );

        fname = getRealPath(fname);
        fname = fname.replace("//", QChar('/'));
        fname = fname.replace(QChar('\\'), QChar('/'));

        if(fname.size() == 0)
        {
            return ERR_INVALID_INPUT;
        }

        if( !fname.contains(".csv", Qt::CaseInsensitive) )
        {
            fname += ".csv";
        }
        m_pFileProc->save(fname, FUNC_SAVE_CSV );
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    return ERR_NONE;
}
int servStorage::scpiSaveSetup(CArgument &p)
{
    setProcStatus(proc_doing);
    setThreadProc(servFile::ProcSaving);
    if( p.size() == 1 )
    {
        QString fname;
        p.getVal( fname );

        if( !fname.startsWith("/tmp/"))
        {
            fname = getRealPath(fname);
            fname = fname.replace("//", QChar('/'));
            fname = fname.replace(QChar('\\'), QChar('/'));
        }
        if(fname.size() == 0)
        {
            return ERR_INVALID_INPUT;
        }

        if( !fname.contains(".stp", Qt::CaseInsensitive) )
        {
            fname =fname.simplified() + ".stp";
        }
        m_pFileProc->save(fname, FUNC_SAVE_STP );
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    return ERR_NONE;
}

int servStorage::loadSetup(QString &path)
{
    //qxl  01.01.04.02 bug 1832
    if(path.contains('\\'))
    {
        path.replace('\\','/');
    }
    QString realPath = getRealPath(path);

    if(realPath.size() == 0)
    {
        return ERR_INVALID_INPUT;
    }

    setThreadProc( servFile::ProcLoading );
    if( m_pFileProc->load(realPath, FUNC_LOAD_STP) == ERR_NONE )
    {
        qDebug() << "Load setup ok";
        servGui::showInfo("Setup Load OK");
    }
    return ERR_NONE;
}

int servStorage::pushSetup(QString &setup)
{
    QString fname = "/tmp/pushSetup.dat";
    QFile file(fname);
    if( file.open(QIODevice::WriteOnly | QIODevice::Truncate) )
    {
        file.write(setup.toLocal8Bit().data(), setup.size());
        file.flush();
        file.close();

        qDebug() << " send msg to load setup" << setup;
    }
    return ERR_NONE;
}

QString servStorage::pullSetup()
{
    QString fname = "/tmp/pullSetup.dat";
    QString setup = "";
    CDataFile::saveSTP(fname);
    QFile file(fname);
    if(file.open(QIODevice::ReadOnly))
    {
        QByteArray ba = file.readAll();
        setup.append(ba);
        file.close();
    }

    qDebug() << setup;
    return setup;
}

///
/// \brief servStorage::doLoad
/// \return
///
DsoErr servStorage::doLoad()
{
    const QFileInfo* f = &( list_at( m_stFiles,m_nFileIndex));

    QString fname = f->filePath();

    if( !QFile::exists(fname) )
    {
        return ERR_FILE_OP_FAIL;
    }

    if(m_nDoProc == FUNC_LOAD_SCR)
    {
        post(E_SERVICE_ID_UTILITY,
             servUtility::cmd_dso_saver, fname);

        post(E_SERVICE_ID_UTILITY,
             CMD_SERVICE_ACTIVE, MSG_APP_UTILITY_SCREEN_SAVER);
    }
    else if(m_nDoProc == FUNC_LOAD_ATT)
    {

        serviceExecutor::post(E_SERVICE_ID_UTILITY_EMAIL,
                              servMail::cmd_mail_select, fname);
        {
             CIntent intent;
             intent.setIntent( MSG_APP_EMAIL,1,1,1);
             startIntent( serv_name_utility_Email, intent );
        }
    }
    else
    {
        m_pFileProc->load( m_nDoProc);
        setThreadProc(servFile::ProcLoading);
        /* disable button */
        setLoadEnable(false);
    }

    return ERR_NONE;
}


int servStorage::doCopy()
{
    setPasteEnable(true);
    setThreadProc( 0 );

    const QFileInfo* pSelected = &( list_at( m_stFiles,m_nFileIndex));
    if(pSelected)
    {
        m_CopyFromPath = pSelected->absoluteFilePath();
        m_CopiedName   = pSelected->fileName();
        m_bCopiedFile  = true;//pSelected->isFile();

        //qDebug() << "copy" << m_CopyFromPath << m_CopiedName;
    }

    return ERR_NONE;
}

int servStorage::onPaste()
{
    QString PasteTo = m_PathName + "/" +m_CopiedName;

    int nRet = ERR_NONE;
    //QString msg  = "";
    if(m_bCopiedFile)
    {
        //from /path/file to /path/file
        if(m_CopyFromPath.compare(PasteTo) == 0)
        {
            //qDebug() << "Copy self";
            //msg = "Can not copy self";
            nRet = ERR_FILE_EXIST;
        }
        //from /path/sub/sub1 to /path/sub
        else if(m_CopyFromPath.contains(PasteTo) )
        {
            //qDebug() << "Copy to same path";
            //msg = "Can not copy to the same dir";
            nRet= ERR_FILE_EXIST;
        }                
        else //from /path/sub to /path/sub/sub1
        {
            QStringList src = m_CopyFromPath.split("/");
            QStringList dst = PasteTo.split("/");
            bool valid = false;
            if( src.size() <= dst.size() )
            {
                for(int i=0; i<src.size(); i++)
                {
                    QString a = src.at(i);
                    QString b = dst.at(i);
                    if( a.compare(b) != 0 )
                    {
                        valid = true;
                    }
                }
            }
            else
            {
                valid = true;
            }

            //qDebug() << "Copy to sub dir";
            //msg = "Can not copy to sub dir";
            if( !valid )
            {
                nRet= ERR_FILE_EXIST;
            }
            else if(QFile::exists(PasteTo))
            {
                //qDebug() << "File exist";
                defer(servStorage::FUNC_STORAGE_PASTE_PRE);
                return ERR_NONE;
            }
        }
    }
    else
    {
        return ERR_NONE;
    }

    if(nRet == ERR_NONE)
    {
        return doPaste();
    }
    else//error message
    {
        servGui::showInfo( sysGetString(nRet,"File operation error"));
    }
    return ERR_NONE;
}

int servStorage::doPaste()
{

    QString PasteTo = m_PathName + "/" +m_CopiedName;

    setThreadProc( servFile::ProcCopying );
    m_pFileProc->copy(m_CopyFromPath, PasteTo );

    //qDebug() << "paste" << "to:" << PasteTo;
    setPasteEnable(false);

    return ERR_NONE;
}

int servStorage::onRename()
{
    return ERR_NONE;
}

int servStorage::doRename(QString name)
{
    bool    isValid = false;
    QString errMsg  = "";

    int err = ERR_FILE_RENAME_FAIL;

    const QFileInfo* pSelected = &( list_at( m_stFiles,m_nFileIndex));

    if(name.size() == 0)
    {
        errMsg = "New name is empty";
    }
    else if(name.compare(pSelected->baseName()) == 0)
    {
        errMsg = "New name is exist";
    }
    else
    {
        QString pattern("[\\\\/:*?\"<>]");
        QRegExp rx(pattern);
        int match = name.indexOf(rx);
        if(match >= 0)
        {
            errMsg = "Invalid input:" + pattern;
            err = ERR_FILE_INVALID_NAME;
        }
        else
        {
            isValid = true;
        }
    }

    if(isValid)
    {
        QString oldName = pSelected->absoluteFilePath();
        QString newName = pSelected->absolutePath() + "/"  + name;
        if(pSelected->isFile())
        {
            newName = newName  + "."+pSelected->suffix();
        }
        //qDebug() << oldName << newName;

        if( QFile::rename(oldName, newName) )
        {
            m_pFileProc->sync(0);
            updateList( m_nFileFormat );
            return ERR_NONE;
        }
        return ERR_FILE_RENAME_FAIL;
    }
    else//error message
    {
        servGui::showInfo( sysGetString(err, errMsg) );
    }
    setThreadProc(0);
    return ERR_NONE;
}

int servStorage::onNewFolder()
{
    return ERR_NONE;
}

int servStorage::doNewFolder(QString folder)
{   
    QString folderName = m_PathName + "/" + folder;
    QFileInfo fi(folderName);
    QString errMsg  = "";
    bool isValid = false;

    int ret = ERR_NONE;
    if(folder.size() == 0)
    {
        errMsg = "New folder is empty";
        ret = ERR_FILE_INVALID_NAME;
    }
    else if( fi.exists() )
    {
        errMsg = "New folder is exist";
        ret = ERR_FILE_EXIST;
    }
    else
    {
        QString pattern("[\\\\/:*?\"<>]");
        QRegExp rx(pattern);
        int match = folder.indexOf(rx);
          if(match >= 0)
        {
            errMsg = "Invalid input:" + pattern;
            ret = ERR_FILE_INVALID_NAME;
        }
        else
        {
            isValid = true;
        }
    }

    if( isValid )
    {
        if(!m_Dir.mkdir(folderName))
        {
            errMsg = "Create folder failure";
            //servGui::showInfo( sysGetString(ERR_FILE_CREATE_DIR, errMsg));
            return ERR_FILE_CREATE_DIR;
        }
        else
        {
            m_pFileProc->sync(0);
            if(m_nDoProc != FUNC_STORAGE_DISK)
            {
                dirEnter(folderName);
            }
            else
            {
                readFiles();
            }
        }
    }
    return ret;
}

int servStorage::onDelete()
{
//    QString msg = "Delete?";
//    servGui::showMsgBox( serv_name_storage,
//                         msg,
//                         servStorage::FUNC_STORAGE_DELETE);
//    doDelete();
    setThreadProc(0);
    return ERR_NONE;
}

int servStorage::doDelete()
{
    const QFileInfo* pSelected = &( list_at( m_stFiles,m_nFileIndex));
    if(pSelected)
    {
        bool b = false;
        if(pSelected->isFile())
        {
            b = m_Dir.remove(pSelected->absoluteFilePath());
        }
        else if(pSelected->filePath().compare(".."))
        {
            b = m_Dir.rmdir(pSelected->absoluteFilePath());
        }

        if(b)
        {
           // qDebug() << "del" << pSelected->absoluteFilePath();
            m_stFiles.removeAt(m_nFileIndex);

            m_pFileProc->sync(0);
            return ERR_NONE;
        }
        else
        {
            //qDebug() << "del failure";
            return ERR_FILE_DELETE_FAIL;
        }
    }
    return ERR_NONE;
}


int servStorage::onFormatDisk()
{
    servGui::showMsgBox(this->getName(),
                        sysGetString(INF_SECURITY_CLEAR,"Security Clear?"),
                        servStorage::FUNC_STORAGE_FORMAT);

    return ERR_NONE;
}


///
/// \brief servStorage::onResult
int servStorage::doFormatDisk()
{
    setThreadProc(servFile::ProcFormating);
    setFormatEnable(false);
    m_pFileProc->diskFormat();

    return ERR_NONE;
}

////////////////////Disk Manager///////////////////////////////
/// \brief servStorage::setNewEnable
/// \param b
/// \return
///
int servStorage::setNewEnable(bool b)
{
    b = b & (!isRootPath());
    mUiAttr.setEnable(MSG_STORAGE_NEWFOLDER, b );
    return ERR_NONE;
}

int servStorage::setCopyEnable(bool b)
{
    b = b & (!isRootPath());
    mUiAttr.setEnable(MSG_STORAGE_COPY, b );
    return ERR_NONE;
}
int servStorage::setPasteEnable(bool b)
{
    b = b & (!isRootPath());
    mUiAttr.setEnable(MSG_STORAGE_PASTE, b );
    return ERR_NONE;
}
int servStorage::setRenameEnable(bool b)
{
    b = b & (!isRootPath());
    mUiAttr.setEnable(MSG_STORAGE_RENAME, b );
    setDeleteEnable(b);
    return ERR_NONE;
}

int servStorage::setDeleteEnable(bool b)
{
    b = b & (!isRootPath());
    mUiAttr.setEnable(MSG_STORAGE_DELETE, b );
    return ERR_NONE;
}

int servStorage::setFormatEnable(bool b)
{
    mUiAttr.setEnable(MSG_STORAGE_SECURITYCLEAR, b );
    return ERR_NONE;
}

int servStorage::setLoadEnable(bool b)
{
    mUiAttr.setEnable(MSG_STORAGE_LOAD, b);
    return ERR_NONE;
}


int servStorage::setSaveEnable(bool b)
{
    mUiAttr.setEnable(MSG_STORAGE_SAVE, b );

    if( m_nDoProc == FUNC_SAVE_MSK ||
        m_nDoProc == FUNC_SAVE_DEC ||
        m_nDoProc == FUNC_SAVE_FFT ||
        m_nDoProc == FUNC_SAVE_DECDAT ||
        m_nDoProc == FUNC_SAVE_ARB ||
        m_nDoProc == FUNC_SAVE_SEARCH ||
        m_nDoProc == FUNC_SAVE_FFT ||
        m_nDoProc == FUNC_SAVE_REC ||
        m_nDoProc == FUNC_SAVE_STP)
    {

    }
    else
    {
        mUiAttr.setEnable(MSG_STORAGE_FILETYPE, b);
    }
    mUiAttr.setEnable(MSG_STORAGE_PREFIX, b);

    mUiAttr.setEnable(MSG_STORAGE_IMAGE_COLOR, b);
    mUiAttr.setEnable(MSG_STORAGE_IMAGE_INVERT, b);
    mUiAttr.setEnable(MSG_STORAGE_WAVE_FORMAT, b);
    mUiAttr.setEnable(MSG_STORAGE_WAVE_DEPTH, b);
    mUiAttr.setEnable(MSG_STORAGE_CHANNEL, b);


    mUiAttr.setEnable(MSG_STORAGE_DIALOG_VISIBLE, b);


    setNewEnable( b );


    return ERR_NONE;
}



////////////////////USB Hotplug///////////////////////////////

/// \brief servStorage::onInsertUSB
/// \param path
/// \return
///
int servStorage::onInsertUSB(const QString& dirName)
{
    if(isRootPath())
    {
        readFiles();
    }
    else
    {
        QPair<QString,QString> p;

        p.first  = QString('C' + m_hFat.count());
        p.second = dirName;
        m_hFat.append(p);
    }

    QString s = dirName;
    if( checkUpgradeGEL( s ) )
    {
        post(E_SERVICE_ID_HELP, servHelp::cmd_usb_status, true);
    }

    if( !ms_bIsUKeyReady )
    {
        ms_bIsUKeyReady = checkUKey();
        if( ms_bIsUKeyReady )
        {
            serviceExecutor::post(serv_name_license,
                                  servLicense::cmd_Opt_Active, 0);
        }
    }


    servGui::showInfo(sysGetString(INF_USB_STORAGE_CONNECT,
                                    "USB device connected"));

    return ERR_NONE;
}

///
/// \brief servStorage::onRemoveUSB
/// \param path
/// \return
///
int servStorage::onRemoveUSB(const QString& path)
{
    //if the current path in the USB, then return to the root
    if(m_PathName.contains(path))
    {
        m_nPathSeries=0;
        setPathName(STORAGE_USER_PATH);
        async(MSG_STORAGE_PREFIX, (STORAGE_REVERVED) );

        m_Dir.setPath(m_PathName);        
    }

    if(isRootPath())
    {
        readFiles();
    }
    servGui::showInfo( sysGetString(INF_USB_STORAGE_DISCONNECT,
                                    "USB device disconnected"));

    QString s = "";
    if( !checkUpgradeGEL(s))
    {
        post(E_SERVICE_ID_HELP, servHelp::cmd_usb_status, false);
    }

    if( ms_bIsUKeyReady )
    {
        ms_bIsUKeyReady = checkUKey();
        if(!ms_bIsUKeyReady )
        {
            serviceExecutor::post(serv_name_license,
                                  servLicense::cmd_Opt_Invalid, 0);
        }
    }
    return ERR_NONE;
}

//////////////////////////////////////Updating///////////////////////////////
/// \brief servStorage::checkUpgradeGEL
/// \param path,return the absolute usb path
/// \return
///
bool servStorage::checkUpgradeGEL( QString& path)
{
    QString model =  servUtility::getSystemModel();

    QString gelFile = "DS7000Update.GEL";
    if(model.startsWith("DS8") || model.startsWith("MSO8"))
    {
        gelFile = "DS8000Update.GEL";
    }
    else if(model.startsWith("DS5") || model.startsWith("MSO5"))
    {
        gelFile = "DS5000Update.GEL";
    }
    else if(model.startsWith("DS9") || model.startsWith("MSO9"))
    {
        gelFile = "DS9000Update.GEL";
    }

    if( path.size() > 0)
    {
        QString filePath = path + "/" + gelFile;
        if(QFile::exists(filePath))
        {
            path = filePath;
            return true;
        }
    }
    else
    {
        for(int i=1; i<m_hFat.size(); i++)
        {
            QString filePath = m_hFat.at(i).second + "/" + gelFile;
            if(QFile::exists(filePath))
            {
                path = filePath;
                return true;
            }
        }
    }

    return false;
}

bool servStorage::isUKeyReady()
{
    return ms_bIsUKeyReady;
}

bool servStorage::checkUKey( void )
{
    for(int i=1; i<m_hFat.size(); i++)
    {
        QString pathName = m_hFat.at(i).second;
        QString devName  = pathName.replace("media", "dev");

        int fd = open(devName.toLocal8Bit().data(), O_RDONLY);
        if( fd != -1)
        {
            int SECTOR_SIZE = 512;
            char buffer[1024];

            memset(buffer, 0 , sizeof(buffer));
            lseek(fd, 0, SEEK_SET);
            if( SECTOR_SIZE == read(fd, buffer, SECTOR_SIZE) )
            {
                int hide_sector = buffer[0xf] ;
                hide_sector = (( hide_sector << 8 ) | buffer[0xe] ) - 1;

                lseek(fd, hide_sector*SECTOR_SIZE, SEEK_SET);
                memset(buffer, 0 , sizeof(buffer));
                if( SECTOR_SIZE == read(fd, buffer, SECTOR_SIZE) )
                {
                    char orignal[] = "RIGOL TECHNOLOGIES,DS1000Z,SPARROW,201212";
                    com_algorithm::CXXTEA::decode( (XXTEA_TYPE*)buffer, (long)SECTOR_SIZE);
                    if(memcmp( (const void*)orignal,
                               (const void*)buffer,
                               strlen(orignal) ) == 0)
                    {
                        close(fd);
                        return true;
                    }
                }
            }
            close(fd);
        }
    }
    return false;
}

/// \brief servStorage::updateFromUSB
/// \return
///
DsoErr servStorage::updateFromUSB(int)
{
    QString filePath = "";
    m_nUpgradeResult = ERR_NONE;
    if(checkUpgradeGEL( filePath ) )
    {
        setThreadProc(servFile::ProcUpgrading);
        m_pFileProc->upgrade(filePath);
        m_bUpgradeMedia = false;
        return ERR_NONE;
    }

    //qDebug() << "!!!!!!!!Cant fint usb device!!!!!!!";
    servGui::showInfo("No package found");
    return ERR_NONE;
}

DsoErr servStorage::updateFromNET(const QString & path)
{
    //qDebug() << "firmware:" << path;
    if(QFile::exists(path))
    {
        setThreadProc(servFile::ProcUpgrading);
        m_pFileProc->upgrade(path);
        m_bUpgradeMedia = true;
        return ERR_NONE;
    }
    //qDebug() << "Firmware is not exist";
    return ERR_NONE;
}


/**
 * @brief servStorage::checkFirmware
 * @param s=1 auto s=0 manual
 * @return
 */
DsoErr servStorage::setCheckFirmware(bool s)
{
    m_bAutoChecking = s;
    return ERR_NONE;
}

bool servStorage::getCheckFirmware()
{
    return m_bAutoChecking;
}

