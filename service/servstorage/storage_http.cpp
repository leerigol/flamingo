#include <QDebug>
#include "storage_api.h"
#include "storage_http.h"

CHttp::CHttp(QObject *, bool showWin)
{
    m_bShowWin = showWin;
    m_pRequest = new QNetworkAccessManager(this);
    m_pResponse= NULL;
    m_pTimer   = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(handleTimeOut()));
}

CHttp::~CHttp()
{
    m_pTimer->deleteLater();
    m_pRequest->deleteLater();

    if(m_pResponse)
    {
        m_pResponse->deleteLater();
    }
}

/* get file from url which we need to download, and restore to filePath */
bool CHttp::download(const QUrl &url, const QString &filePath)
{
    /* confirm the url is valid or not */
    if (!url.isValid())
    {
        qDebug() << (QString("Error:URL has specify a invalid name."));
        return false;
    }

    if (url.scheme() != "http")
    {
        qDebug() << (QString("Error:URL must start with 'http:'"));
        return false;
    }

    if (url.path().isEmpty())
    {
        qDebug() << (QString("Error:URL's path is empty."));
        return false;
    }

    if (filePath.isEmpty())
    {
        qDebug() << (QString("Error:invalid filePath."));
        return false;
    }

    m_File.setFileName(filePath);
    if (!m_File.open(QIODevice::WriteOnly))
    {
        qDebug() << (QString("Error:Cannot open file."));
        return false;
    }

    m_ServerURL    = url;
    m_FirmwarePath = filePath;
    m_bStarted= false;


    m_pResponse = m_pRequest->get(QNetworkRequest(m_ServerURL));
    connect(m_pResponse, SIGNAL(readyRead()),
              this, SLOT(slotReadyRead()));

    connect(m_pResponse, SIGNAL(downloadProgress(qint64, qint64)),
              this, SLOT(replyDownloadProgress(qint64, qint64)));

    connect(m_pResponse, SIGNAL(error(QNetworkReply::NetworkError)),
              this, SLOT(slotError(QNetworkReply::NetworkError)));

    connect(m_pResponse, SIGNAL(finished()),
              this, SLOT(replyFinished()) );
    return true;
}


/* slots */
void CHttp::handleTimeOut()
{
    slotError(QNetworkReply::TimeoutError );
}

/* download finished */
void CHttp::replyFinished()
{
    if(m_pResponse->error() == QNetworkReply::NoError)
    {
        slotError( m_pResponse->error() );
    }
}

/* downloading... */
void CHttp::replyDownloadProgress(qint64 done, qint64 total)
{
    qDebug()<<QString("%1%").arg(done / (double)total * 100);
    if (m_bShowWin && (0 != total))
    {
        if(!m_bStarted)
        {
            servFile::initProgress("Downloading...", total);
            m_bStarted = true;
        }
        servFile::setProgress(done);
    }
}

/* if this is not been fired for 30s,
 * we trade this timeout,
 *  */
void CHttp::slotReadyRead()
{
    //qDebug() << "downloading" << m_pResponse->bytesToWrite();
    m_File.write(m_pResponse->readAll());
    if (m_pTimer->isActive())
    {
        m_pTimer->stop();
    }
    m_pTimer->start(30000);/* wait 30 seconds */
}

/* handle error */
void CHttp::slotError(QNetworkReply::NetworkError e)
{
    //qDebug()<<"error:"<<e;
    if(m_bShowWin)
    {
        servFile::hideProgress();
    }

    if(e != QNetworkReply::NoError)
    {
       // m_File.remove();
    }

    m_File.close();
    m_pTimer->stop();

    emit sigFinish(e);
}
