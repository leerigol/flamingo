#ifndef DATAFILE
#define DATAFILE

#include<QObject>

#include "../../baseclass/dsovert.h"
#include "../../baseclass/iserial.h"
#include "../../meta/crmeta.h"

class CSetupStream : public CStream
{
public:
    //for serial out. xml
    CSetupStream( QFile& );
    //for serial in.
    CSetupStream(QString& stp, QString& app);
    ~CSetupStream();

public:
    //! aligned
    virtual int write( const QString &name, const bool &dat, bool bPrivate = false, cryptKey key=0);
    virtual int write( const QString &name, const char &dat, bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, const short &dat, bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, const int &dat, bool bPrivate = false,  cryptKey key=0);
    virtual int write( const QString &name, const qlonglong &dat, bool bPrivate = false,  cryptKey key=0 );

    virtual int write( const QString &name, const unsigned char &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const unsigned short &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const unsigned int &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const qulonglong &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, const QString &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int write( const QString &name, void *pBuf, int length, bool bPrivate = false,  cryptKey key=0 );

    virtual int read( const QString &name, bool &val, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, char &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, short &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, int &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, qlonglong &dat, bool bPrivate = false,  cryptKey key=0 );

    virtual int read( const QString &name, unsigned char &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, unsigned short &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, unsigned int &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, qulonglong &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, QString &dat, bool bPrivate = false,  cryptKey key=0 );
    virtual int read( const QString &name, void *pBuf, int cap, bool bPrivate = false,  cryptKey key=0 );

    int write(const QString& node);
    void setAppName(QString& s);

private:
    QTextStream         mTextStream;
    r_meta::CXmlMeta*   mpSetup;
    QString mAppName;
};

class CDataFile: public QObject
{
public:
    const static quint32 PF_VER   = 0x20170217;
    const static quint32 STP_VER  = 0x20180307;
    const static quint32 BUF_SIZE = 32;
    const static quint32 RAM_SIZE = 1024;
public:
    static int preSTP(CSetupStream&);
    static int postSTP(CSetupStream&);

    static int saveSTP(QString&);
    static int loadSTP(QString&);

    static int savePF(QString&);
    static int loadPF(QString&);
private:
    static DsoWfm _wfm;
};

typedef struct
{
    uchar  u8AppId;
    uchar  u8AppVer;
    ushort u16SetupLen;
}stAppSetup;

#endif // DATAFILE

