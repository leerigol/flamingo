#include <QEvent>

#include "servutility.h"
#include "../servgui/servgui.h"

#include "../../engine/cplatform.h"

quint16 servUtility::m_nHardVer = 0;
quint16 servUtility::m_nCPLDVer = 0;

QString servUtility::getSystemModel()
{
    QString s = DEFAULT_MODEL_DSO;
    serviceExecutor::query(serv_name_utility,servUtility::cmd_dso_model, s);
    return s;
}

QString servUtility::getSystemSerial()
{
    QString s = DEFAULT_SERIAL_DSO;
    serviceExecutor::query(serv_name_utility,servUtility::cmd_dso_serial, s);
    //s = "MSO7014xxxxxxxxxx";
    return s;
}

QString servUtility::getSystemVersion()
{
    QString s = DEFAULT_VERSION;
    serviceExecutor::query(serv_name_utility,servUtility::cmd_dso_firmware, s);
    return s;
}

bool servUtility::getSystemKeyLocker()
{
    bool b;
    serviceExecutor::query(serv_name_utility, MSG_APP_UTILITY_LOCK_KB, b);
    return b;
}

bool servUtility::isMSO()
{
    //serial start with DS7A ,Then it'S not a MSO
    if(getSystemSerial().startsWith("DS7A",Qt::CaseInsensitive))
    {
        return false;
    }

    //model start with DS7, Then it'S not a MSO
    if(getSystemModel().startsWith("DS7",Qt::CaseInsensitive))
    {
        return false;
    }

    if( m_nHardVer & LA_BIT )
    {
        return true;
    }
    return false;
}

bool servUtility::isHasDG()
{
    if(getSystemModel().startsWith("DS7",Qt::CaseInsensitive))
    {
        return false;
    }

    if( m_nHardVer & DG_BIT )
    {
        return true;
    }

    return false;
}

void servUtility::readInformation()
{
    #ifndef _SIMULATE
    if(true)
    {
        QSettings configIniRead("/tmp/sysinfo.txt", QSettings::IniFormat);
         //将读取到的ini文件保存在QString中，先取值，然后通过toString()函数转换成QString类型
//        QString str;
//        str = configIniRead.value("model").toString();
//        setModel( str );

        strSoftVer  = configIniRead.value("softver").toString();
        if(strSoftVer.compare(DEFAULT_VERSION) != 0 )
        {
            qDebug() << "<---System Version different with Boot--->";
            //Q_ASSERT(false);
        }
        strBootVer  = configIniRead.value("bootver").toString();


        strBuildDate= configIniRead.value("builddate").toString().remove(0,1);
        strBuildDate= strBuildDate.remove(strBuildDate.size()-1,1);

        m_nHardVer = 0;

        queryEngine(qENGINE_MB_VER, m_nHardVer);
        strHardVer = QString("0x%1").arg(m_nHardVer, 4, 16, QLatin1Char('0'));


        m_nCPLDVer = 0;
        queryEngine(qENGINE_CPLD_VER, m_nCPLDVer);
        strSP6Ver = QString("0x%1").arg(m_nCPLDVer, 4 ,16);

        strSerial   = DEFAULT_SERIAL_DSO;

        //qDebug() << "afe.ver=" << getAfeVer0() << getAfeVer1() << getAfeVer2() << getAfeVer3();
     }
     #endif

    queryEngine( qENGINE_POWER_SWITCH, m_bPowerStatus );
    createKeyMapTable();
}

/*!
 * \brief servUtility::setShowInfo
 * \return
 */
DsoErr servUtility::setShowInfo()
{        
//    m_bShowInfo = !m_bShowInfo;
    return ERR_NONE;
}
/*!
 * \brief servUtility::getShowInfo
 * \return
 */
bool servUtility::getShowInfo()
{
    return m_bShowInfo;
}

/*!
 * \brief servUtility::setPowerOn
 * \param b
 * \return
 */
DsoErr servUtility::setPowerOn(bool b)
{
    sysSetSuspend( b );
    return ERR_NONE;
}

/*!
 * \brief servUtility::getPowerOn
 * \return
 */
bool servUtility::getPowerOn()
{
    return sysGetSuspend();
}

DsoErr servUtility::setPowerStatus(bool b)
{
    m_bPowerStatus = b;
    syncEngine(ENGINE_POWER_SWITCH, b);
    return ERR_NONE;
}

/*!
 * \brief servUtility::getPowerStatus
 * \return
 */
bool servUtility::getPowerStatus()
{
    return m_bPowerStatus;
}

DsoErr servUtility::shutdown()
{
    syncEngine(ENGINE_POWER_SHUTDOWN);
    return ERR_NONE;
}

DsoErr servUtility::resetSystem()
{
    syncEngine(ENGINE_POWER_RESTART);
    return ERR_NONE;
}

DsoErr servUtility::setModel( const QString &model)
{
    Bandwidth bw;
    if ( parseBw( model, &bw ) )
    {
        //bw = BW_500M;
        strModel = model;//"MSO7054";//
        mRawBand = bw;
        mSysBand = bw;

        postEngine(ENGINE_ACQUIRE_BAND, bw);
        return ERR_NONE;
    }
    else
    {
        qWarning()<<"invalid model str"<<model;
        return ERR_INVALID_INPUT;
    }
}

QString servUtility::getModel()
{
     return strModel;
}


QString servUtility::getModule()
{
    QString module;
    if(sysHasLA())
    {
        module.append("1");
    }
    else
    {
        module.append("0");
    }

    if(sysHasDG())
    {
        module.append(",1");
    }
    else
    {
        module.append(",0");
    }

    module.append(",0,0,0");

    return module;
}

DsoErr servUtility::setSerial(QString sn)
{
    strSerial = sn;
    //qDebug()<<__FILE__<<__LINE__<<"SN:"<<strSerial;
    return ERR_NONE;
}

QString servUtility::getSerial()
{
    return strSerial;
}

QString servUtility::getSoftVer()
{
    return strSoftVer;
}

QString servUtility::getHardVer()
{
    return strHardVer;
}

QString servUtility::getBootVer()
{
    return strBootVer;
}

QString servUtility::getSPUVer()
{
    if(strSPUVer.size() == 0 )
    {
        quint32 version = 0;
        queryEngine(qENGINE_SPU_VER, version);
        strSPUVer = QString("0x%1").arg(version, 8 ,16, QLatin1Char('0'));
    }
    return strSPUVer;
}


QString servUtility::getWPUVer()
{
    if(strWPUVer.size() == 0)
    {
        quint32 version = 0;
        queryEngine(qENGINE_WPU_VER, version);
        strWPUVer = QString("0x%1").arg(version, 8 ,16);
    }
    return strWPUVer;
}

QString servUtility::getCCUVer()
{
    if( strSCUVer.size() == 0 )
    {
        quint32 version = 0;
        queryEngine(qENGINE_FCU_VER, version);
        strSCUVer = QString("0x%1").arg(version, 8 ,16);
    }
    return strSCUVer;
}

QString servUtility::getBuildDate()
{
    return strBuildDate;
}

int servUtility::getAdcId0()
{
    quint32 adcId = 0;
    queryEngine( qENGINE_ADC_ID, 0, adcId );

    return adcId;
}

int servUtility::getAdcId1()
{
    quint32 adcId = 0;
    queryEngine( qENGINE_ADC_ID, 1, adcId );

    return adcId;
}

int servUtility::getAfeVer0()
{
    quint32 afeVer = 0;
    queryEngine( qENGINE_AFE_VER, 0, afeVer );

    return afeVer;
}

int servUtility::getAfeVer1()
{
    quint32 afeVer = 0;
    queryEngine( qENGINE_AFE_VER, 1, afeVer );

    return afeVer;
}

int servUtility::getAfeVer2()
{
    quint32 afeVer = 0;
    queryEngine( qENGINE_AFE_VER, 2, afeVer );
    return afeVer;
}

int servUtility::getAfeVer3()
{
    quint32 afeVer = 0;
    queryEngine( qENGINE_AFE_VER, 3, afeVer );
    return afeVer;
}

QString servUtility::getMCUVer()
{
    return strSP6Ver;
}

DsoErr servUtility::setStartTimes(int /*times*/)
{
    sysSetLiveCnt(0);
    servGui::showInfo("Reset BootTime OK!");

    return ERR_NONE;
}

DsoErr servUtility::setLiveTimes(int /*times*/)
{
    sysSetCycleCnt(0);
    servGui::showInfo("Reset LiveTime OK!");
    return ERR_NONE;
}

DsoErr servUtility::resetPrivateData(int /*times*/)
{

    servGui::showInfo("Reset PrivateData OK!");

    return ERR_NONE;
}

DsoErr servUtility::recoverBoard(int /*times*/)
{

    servGui::showInfo("Recover BareBoard OK!");

    return ERR_NONE;
}



DsoErr servUtility::setTimeShow(bool b)
{
    m_bShowTime = b;
    return ERR_NONE;
}

bool servUtility::getTimeShow()
{
    return m_bShowTime;
}

DsoErr servUtility::setYear(int y)
{
    iYear = y;

    if( iMonth == 2)
    {
        if( (iYear % 4 == 0 && iYear % 100 != 0) ||
            (iYear % 400 == 0)
            )
        {
            mUiAttr.setMaxLength(MSG_APP_UTILITY_DAY, 29);
        }
        else
        {
            if( iDay > 28 )
            {
                iDay=28;
            }
            mUiAttr.setMaxLength(MSG_APP_UTILITY_DAY, 28);
            setuiChange(MSG_APP_UTILITY_DAY);
        }
    }

    //saveTime();
    return ERR_NONE;
}

DsoErr servUtility::setMonth(int m)
{
    iMonth = m;

    int max = 30;
    if( iMonth == 2)
    {
        if( (iYear % 4 == 0 && iYear % 100 != 0) ||
            (iYear % 400 == 0)
            )
        {
            if(iDay>29)
            {
                iDay = 29;
            }
            max = 29;
        }
        else
        {
            if( iDay>28)
            {
                iDay = 28;
            }
            max = 28;
        }
    }
    else if( iMonth == 1 ||
             iMonth == 3 ||
             iMonth == 5 ||
             iMonth == 7 ||
             iMonth == 8 ||
             iMonth == 10 ||
             iMonth == 12)
    {
        max = 31;
    }
    else
    {
        if(iDay>30)
        {
            iDay=30;
        }
        max = 30;
    }

    mUiAttr.setMaxVal(MSG_APP_UTILITY_DAY, max);

    return ERR_NONE;
}
DsoErr servUtility::setDay(int d)
{
    iDay = d;
    //saveTime();
    return ERR_NONE;
}
DsoErr servUtility::setHour(int h)
{
    iHour = h;
    //saveTime();
    return ERR_NONE;
}
DsoErr servUtility::setMinute(int m)
{
    iMinute = m;
    //saveTime();
    return ERR_NONE;
}
DsoErr servUtility::setSecond(int s)
{
    iSecond = s;
    //saveTime();
    return ERR_NONE;
}
void servUtility::saveTime()
{    
    QString cmd = QString("date -s %1%2%3%4%5")
                   .arg(iYear, 4, 10, QLatin1Char('0'))
                   .arg(iMonth, 2, 10, QLatin1Char('0'))
                   .arg(iDay, 2, 10, QLatin1Char('0'))
                   .arg(iHour, 2, 10, QLatin1Char('0'))
                   .arg(iMinute, 2, 10, QLatin1Char('0'));
               //.arg(iSecond, 2, 10, QLatin1Char('0')));


    QProcess::execute(cmd);
    QProcess::execute(QString("hwclock -w"));

    async(MSG_APP_UTILITY_TIME,0);
}

int servUtility::getYear()
{
//    setuiZVal( def );
    setuiMaxVal(2099);
    setuiMinVal(2017);
    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);

    return iYear;
}
int servUtility::getMonth()
{
    setuiMaxVal(12);
    setuiMinVal(1);
    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return iMonth;
}
int servUtility::getDay()
{
    if( iMonth == 2)
    {
        if( (iYear % 4 == 0 && iYear % 100 != 0) ||
            (iYear % 400 == 0)
            )
        {
            setuiMaxVal(29);
        }
        else
        {
            setuiMaxVal(28);
        }
    }
    else if( iMonth == 1 ||
             iMonth == 3 ||
             iMonth == 5 ||
             iMonth == 7 ||
             iMonth == 8 ||
             iMonth == 10 ||
             iMonth == 12)
    {
        setuiMaxVal(31);
    }
    else
    {
        setuiMaxVal(30);
    }

    setuiMinVal(1);
    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);    
    return iDay;
}
int servUtility::getHour()
{
    setuiMaxVal(23);
    setuiMinVal(0);
    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return iHour;
}
int servUtility::getMinute()
{
    setuiMaxVal(59);
    setuiMinVal(0);
    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    return iMinute;
}
int servUtility::getSecond()
{
    setuiMaxVal(59);
    setuiMinVal(0);
    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);  
    return iSecond;
}

#include <linux/input.h>
#include <fcntl.h>
#include <unistd.h>

int servUtility::toTouch( unsigned short type,
                             unsigned short code,
                             int value)
{
    static int fd = -1;

    if( fd == -1 )
    {
        fd = open("/dev/input/event0", O_RDWR);
    }

    if( fd >= 0 )
    {
        struct input_event event;
        event.type = type;
        event.code = code;
        event.value = value;

        gettimeofday(&event.time, 0);

        if (write(fd, &event, sizeof(struct input_event)) < 0)
        {
            printf("report key error!\n");
            return -1;
        }
    }

    return 0;
}

DsoErr servUtility::onWebEvent(CArgument &arg)
{
    if(arg.size() == 2)
    {
        int x = arg[0].iVal;
        int y = arg[1].iVal;

        if( x > 0 && x < 1024 && y > 0 && y < 600)
        {
            toTouch(EV_KEY,BTN_TOUCH,1);
            //toTouch(EV_ABS,ABS_MT_TRACKING_ID,0);
            //toTouch(EV_ABS,ABS_MT_TOUCH_MAJOR,1);

            toTouch(EV_ABS,ABS_MT_POSITION_X,x);
            toTouch(EV_ABS,ABS_MT_POSITION_Y,y);

            //toTouch(EV_ABS,ABS_MT_WIDTH_MAJOR,1);

            toTouch(EV_SYN,SYN_MT_REPORT,0);
            toTouch(EV_SYN,SYN_REPORT,0);

            QThread::usleep(50000);

            toTouch(EV_KEY,BTN_TOUCH,0);
            toTouch(EV_ABS,ABS_MT_TRACKING_ID,0);
            toTouch(EV_ABS,ABS_MT_TOUCH_MAJOR,0);

            toTouch(EV_ABS,ABS_MT_POSITION_X,x);
            toTouch(EV_ABS,ABS_MT_POSITION_Y,y);

            toTouch(EV_ABS,ABS_MT_WIDTH_MAJOR,0);

            toTouch(EV_SYN,SYN_MT_REPORT,0);
            toTouch(EV_SYN,SYN_REPORT,0);

        }
    }
    return ERR_NONE;
}


DsoErr servUtility::onKeyPress(int key)
{
    if(keyDsoMap.contains(key))
    {
        quint32 keyCode = keyDsoMap[key];
        postKeyEvent(keyCode, 0);
        return ERR_NONE;
    }
    return ERR_INVALID_INPUT;
}

//Encode_ScanCode
DsoErr servUtility::onKeyInc(CArgument& arg)
{
    if(arg.size() == 2)
    {
        int key = arg[0].iVal;
        int cnt = arg[1].iVal;
        if(keyTuneMap.contains(key))
        {
            quint32 keyCode = keyTuneMap[key];
            postKeyEvent(Encode_ScanCode( encode_inc(keyCode) ), cnt);
            return ERR_NONE;
        }
    }

    return ERR_INVALID_INPUT;
}

DsoErr servUtility::onKeyDec(CArgument& arg)
{
    if(arg.size() == 2)
    {
        int key = arg[0].iVal;
        int cnt = arg[1].iVal;
        if(keyTuneMap.contains(key))
        {
            quint32 keyCode = keyTuneMap[key];
            postKeyEvent(Encode_ScanCode(encode_dec(keyCode)), cnt);
            return ERR_NONE;
        }
    }
    return ERR_INVALID_INPUT;
}

DsoErr servUtility::postKeyEvent(quint32 keyCode, int cnt)
{
    QKeyEvent *pEvt = new QKeyEvent(QEvent::KeyRelease,
                                    keyCode,
                                    Qt::NoModifier,
                                    QString(),
                                    false,
                                    (ushort)cnt );

     qApp->postEvent( qApp, (QEvent*)pEvt );
     return ERR_NONE;
}

int servUtility::onShowLabel(QString label)
{
//    static bool bShowIdentify = false;
//    bool bShow = label.startsWith("hide");

//    if(label.compare("Connected") == 0 )
//    {
//        bShow = bShowIdentify;
//        bShowIdentify = !bShowIdentify;
//    }

//    label = label.replace("-", " ");
//    servGui::showLabel( "Demo",
//                      !bShow,
//                      Qt::white,
//                      label,450,430);

    servGui::showInfo(label);
    syncEngine( ENGINE_BEEP_SHORT );

    return ERR_NONE;
}

void servUtility::createKeyMapTable()
{

    keyDsoMap.insert(r_key_ch1,          R_Pkey_CH1);
    keyDsoMap.insert(r_key_ch2 ,         R_Pkey_CH2);
    keyDsoMap.insert(r_key_ch3,          R_Pkey_CH3);
    keyDsoMap.insert(r_key_ch4,          R_Pkey_CH4);
    keyDsoMap.insert(r_key_math,         R_Pkey_MATH);
    keyDsoMap.insert(r_key_ref,          R_Pkey_REF);
    keyDsoMap.insert(r_key_la,           R_Pkey_LA);
    keyDsoMap.insert(r_key_decode1,      R_Pkey_DECODE);
    keyDsoMap.insert(r_key_moff,         R_Pkey_PageOff);
    keyDsoMap.insert(r_key_f1,           R_Pkey_F1);
    keyDsoMap.insert(r_key_f2,           R_Pkey_F2);
    keyDsoMap.insert(r_key_f3,           R_Pkey_F3);
    keyDsoMap.insert(r_key_f4,           R_Pkey_F4);
    keyDsoMap.insert(r_key_f5,           R_Pkey_F5);
    keyDsoMap.insert(r_key_f6,           R_Pkey_F6);
    keyDsoMap.insert(r_key_f7,           R_Pkey_F7);
    keyDsoMap.insert(r_key_qprevious,    R_Pkey_PLAY_PRE);
    keyDsoMap.insert(r_key_qnext,        R_Pkey_PLAY_NEXT);
    keyDsoMap.insert(r_key_qstop,        R_Pkey_PLAY_STOP);
    keyDsoMap.insert(r_key_vposition1,   R_Pkey_CH1_POS_Z);
    keyDsoMap.insert(r_key_vposition2,   R_Pkey_CH2_POS_Z);
    keyDsoMap.insert(r_key_vposition3,   R_Pkey_CH3_POS_Z);
    keyDsoMap.insert(r_key_vposition4,   R_Pkey_CH4_POS_Z);
    keyDsoMap.insert(r_key_vscale1,      R_Pkey_CH1_VOLT_Z);
    keyDsoMap.insert(r_key_vscale2,      R_Pkey_CH2_VOLT_Z);
    keyDsoMap.insert(r_key_vscale3,      R_Pkey_CH3_VOLT_Z);
    keyDsoMap.insert(r_key_vscale4,      R_Pkey_CH4_VOLT_Z);
    keyDsoMap.insert(r_key_hscale,       R_Pkey_TIME_SCALE_Z);
    keyDsoMap.insert(r_key_hposition,    R_Pkey_TIME_OFFSET_Z);
    keyDsoMap.insert(r_key_kfunction,    R_Pkey_FZ);
    keyDsoMap.insert(r_key_tlevel,       R_Pkey_TRIG_LEVEL_Z);
    keyDsoMap.insert(r_key_tmenu,        R_Pkey_TRIG_MENU);
    keyDsoMap.insert(r_key_tmode,        R_Pkey_TRIG_MODE);

    keyDsoMap.insert(r_key_clear,        R_Pkey_CLEAR);
    keyDsoMap.insert(r_key_auto,         R_Pkey_AUTO);
    keyDsoMap.insert(r_key_rstop,        R_Pkey_RunStop);
    keyDsoMap.insert(r_key_single,       R_Pkey_SINGLE);
    keyDsoMap.insert(r_key_quick,        R_Pkey_QUICK);

    keyDsoMap.insert(r_key_measure,      R_Pkey_MEASURE);
    keyDsoMap.insert(r_key_acquire,      R_Pkey_ACQUIRE);
    keyDsoMap.insert(r_key_storage,      R_Pkey_STORAGE);
    keyDsoMap.insert(r_key_cursor,       R_Pkey_CURSOR);
    keyDsoMap.insert(r_key_display,      R_Pkey_DISPLAY);
    keyDsoMap.insert(r_key_utility,      R_Pkey_UTILITY);
    keyDsoMap.insert(r_key_tforce,       R_Pkey_TRIG_FORCE);
    keyDsoMap.insert(r_key_source1,      R_Pkey_SOURCE1);
    keyDsoMap.insert(r_key_source2,      R_Pkey_SOURCE2);
    keyDsoMap.insert(r_key_touch,        R_Pkey_TOUCH);
    keyDsoMap.insert(r_key_zoom,         R_Pkey_HORI_ZOOM);
    keyDsoMap.insert(r_key_navigate,     R_Pkey_HORI_NAGAVITE);
    keyDsoMap.insert(r_key_back,         R_Pkey_PageReturn);
    keyDsoMap.insert(r_key_wave_scale,   R_Pkey_WAVE_VOLT_Z);
    keyDsoMap.insert(r_key_wave_offset,  R_Pkey_WAVE_POS_Z);
    keyDsoMap.insert(r_key_default,      R_Pkey_DEFAULT);

    keyTuneMap.insert(r_key_vposition1,   R_Pkey_CH1_POS);
    keyTuneMap.insert(r_key_vposition2,   R_Pkey_CH2_POS);
    keyTuneMap.insert(r_key_vposition3,   R_Pkey_CH3_POS);
    keyTuneMap.insert(r_key_vposition4,   R_Pkey_CH4_POS);
    keyTuneMap.insert(r_key_vscale1,      R_Pkey_CH1_VOLT);
    keyTuneMap.insert(r_key_vscale2,      R_Pkey_CH2_VOLT);
    keyTuneMap.insert(r_key_vscale3,      R_Pkey_CH3_VOLT);
    keyTuneMap.insert(r_key_vscale4,      R_Pkey_CH4_VOLT);
    keyTuneMap.insert(r_key_hscale,       R_Pkey_TIME_SCALE);
    keyTuneMap.insert(r_key_hposition,    R_Pkey_TIME_OFFSET);
    keyTuneMap.insert(r_key_kfunction,    R_Pkey_FUNC);
    keyTuneMap.insert(r_key_tlevel,       R_Pkey_TRIG_LEVEL);
    keyTuneMap.insert(r_key_wave_scale,   R_Pkey_WAVE_VOLT);
    keyTuneMap.insert(r_key_wave_offset,  R_Pkey_WAVE_POS);
}

