#include "servutility.h"

#include "../../com/scpiparse/cscpiparser.h"

#include "../service_msg.h"
#include "../servgui/servgui.h"

#include "../servlicense/servlicense.h"
#include "../../fw/service/service.h"
#include "../../service/servdso/sysdso.h"
#include "../../service/servdso/servdso.h"
#define dbg_out()

IMPLEMENT_CMD( serviceExecutor, servUtility )

start_of_entry()
//!Timer_function
on_set_int_int(     CMD_SERVICE_ACTIVE ,                &servUtility::onActive),
on_set_void_void(   CMD_SERVICE_RST,                    &servUtility::rst),
on_set_void_void(   CMD_SERVICE_INIT,                   &servUtility::init),
on_set_int_void  (  CMD_SERVICE_STARTUP,                &servUtility::startup),
on_set_int_int(     CMD_SERVICE_TIMEOUT,                &servUtility::onTimeout),

on_set_int_void(    servUtility::cmd_quick_project,     &servUtility::setProjectMode),
on_get_bool    (    MSG_APP_UTILITY_PROJECT,            &servUtility::getProjectMode),
//!Beeper
on_set_int_bool(    MSG_APP_UTILITY_BEEPER,             &servUtility::setBeeper),
on_get_bool(        MSG_APP_UTILITY_BEEPER,             &servUtility::getBeeper),

on_set_int_int (    servUtility::cmd_dso_beeper,        &servUtility::shortBeeper),
on_set_int_arg (    CMD_SERVICE_PHY_KEY,                &servUtility::onPhyKey ),

////!Language
on_set_int_int(     MSG_APP_UTILITY_LANGUAGE,           &servUtility::setLanguage),
on_get_int(         MSG_APP_UTILITY_LANGUAGE,           &servUtility::getLanguage),

////!Aux output
on_set_int_int(     MSG_APP_UTILITY_AUXOUT,             &servUtility::setAuxout),
on_get_int(         MSG_APP_UTILITY_AUXOUT,             &servUtility::getAuxout),

//!m_bPowerOn
on_set_int_bool(    MSG_APP_UTILITY_POWER_ON_SET,       &servUtility::setPowerOn),
on_get_bool(        MSG_APP_UTILITY_POWER_ON_SET,       &servUtility::getPowerOn),

on_set_int_bool(    MSG_APP_UTILITY_POWER_STATUS,       &servUtility::setPowerStatus),
on_get_bool(        MSG_APP_UTILITY_POWER_STATUS,       &servUtility::getPowerStatus),

//!sys time scpi info
on_set_int_arg(     servUtility::cmd_system_date,       &servUtility::scpiSetDate),
on_set_int_arg(     servUtility::cmd_system_time,       &servUtility::ScpiSetTime),
on_get_void_argo(   servUtility::cmd_system_date,       &servUtility::scpiGetDate),
on_get_void_argo(   servUtility::cmd_system_time,       &servUtility::scpiGetTime),

//!time setting
on_set_int_bool(    MSG_APP_UTILITY_SHOW_TIME,          &servUtility::setTimeShow),
on_get_bool(        MSG_APP_UTILITY_SHOW_TIME,          &servUtility::getTimeShow),

on_set_int_int(     MSG_APP_UTILITY_YEAR,               &servUtility::setYear ),
on_set_int_int(     MSG_APP_UTILITY_MONTH,              &servUtility::setMonth ),
on_set_int_int(     MSG_APP_UTILITY_DAY,                &servUtility::setDay ),
on_set_int_int(     MSG_APP_UTILITY_HOUR,               &servUtility::setHour ),
on_set_int_int(     MSG_APP_UTILITY_MINUTE,             &servUtility::setMinute ),
on_set_void_void(   MSG_APP_UTILITY_APPLY_TIME,         &servUtility::saveTime ),

on_get_int(         MSG_APP_UTILITY_YEAR,               &servUtility::getYear ),
on_get_int(         MSG_APP_UTILITY_MONTH,              &servUtility::getMonth ),
on_get_int(         MSG_APP_UTILITY_DAY,                &servUtility::getDay ),
on_get_int(         MSG_APP_UTILITY_HOUR,               &servUtility::getHour ),
on_get_int(         MSG_APP_UTILITY_MINUTE,             &servUtility::getMinute ),
on_get_int(         MSG_APP_UTILITY_SECOND,             &servUtility::getSecond ),


//!--------------------------systeminfo,from help--------------------------------
//on_set_int_void(    MSG_APP_UTILITY_SYSTEMINFO,         &servUtility::setShowInfo),
//on_get_bool(        MSG_APP_UTILITY_SYSTEMINFO,         &servUtility::getShowInfo),

//!change the system infomation model
on_set_int_string(  servUtility::cmd_dso_model,         &servUtility::setModel),
on_get_string(      servUtility::cmd_dso_model,         &servUtility::getModel),
//!change the system infomation serial number
on_set_int_string(  servUtility::cmd_dso_serial,        &servUtility::setSerial),
on_get_string(      servUtility::cmd_dso_serial,        &servUtility::getSerial),

on_get_string(      servUtility::cmd_dso_module,        &servUtility::getModule),


on_get_string(      servUtility::cmd_dso_firmware,      &servUtility::getSoftVer),
on_get_string(      servUtility::cmd_dso_hardware,      &servUtility::getHardVer),
on_get_string(      servUtility::cmd_dso_bootware,      &servUtility::getBootVer),
on_get_string(      servUtility::cmd_dso_spu,           &servUtility::getSPUVer),
on_get_string(      servUtility::cmd_dso_wpu,           &servUtility::getWPUVer),
on_get_string(      servUtility::cmd_dso_scu,           &servUtility::getCCUVer),
on_get_string(      servUtility::cmd_dso_mcu,           &servUtility::getMCUVer),
on_get_string(      servUtility::cmd_dso_builddate,     &servUtility::getBuildDate),

on_get_int(         servUtility::cmd_dso_adc_id0,       &servUtility::getAdcId0   ),
on_get_int(         servUtility::cmd_dso_adc_id1,       &servUtility::getAdcId1   ),

on_get_int(         servUtility::cmd_dso_afe_ver0,      &servUtility::getAfeVer0   ),
on_get_int(         servUtility::cmd_dso_afe_ver1,      &servUtility::getAfeVer1   ),
on_get_int(         servUtility::cmd_dso_afe_ver2,      &servUtility::getAfeVer2  ),
on_get_int(         servUtility::cmd_dso_afe_ver3,      &servUtility::getAfeVer3   ),

on_set_int_int(     MSG_APP_UTILITY_RST_STARTTIMES,     &servUtility::setStartTimes),
on_set_int_int(     MSG_APP_UTILITY_RESET_LIVE_TIME,    &servUtility::setLiveTimes),
on_set_int_int(     MSG_APP_UTILITY_RESET_PREVATE_DATA, &servUtility::resetPrivateData),

//!Scpi command
on_set_int_void(    servUtility::cmd_dso_restart,       &servUtility::resetSystem),
on_set_int_void(    servUtility::cmd_dso_shutdown,      &servUtility::shutdown),
on_set_int_void(    servUtility::cmd_dso_poweroff,      &servUtility::shutDown),

//!Sys Pres
on_set_int_int(     servUtility::cmd_system_band,       &servUtility::setSysBand),
on_get_int(         servUtility::cmd_system_band,       &servUtility::getSysBand),
on_get_int(         servUtility::cmd_model_band,        &servUtility::getRawBand),

//!Sys vend config
on_set_int_arg(       servUtility::cmd_vend_config,     &servUtility::setSysVendorItem ),
on_get_void_argi_argo(servUtility::cmd_vend_config,     &servUtility::getSysVendorItem ),

//!监听选件失效或删除
on_set_void_void(     servUtility::cmd_spyon_opt,       &servUtility::setSpyonOpt),

//screen saver
//!screensaver-openclose
on_set_int_int(     MSG_APP_UTILITY_SCREEN_SELECT,      &servUtility::setScrSaver),
on_get_int(         MSG_APP_UTILITY_SCREEN_SELECT,      &servUtility::getScrSaver),

//!screensaver-preview
on_set_int_void(    MSG_APP_UTILITY_SCREEN_PREVIEW,     &servUtility::setScrPreview),
on_get_bool(        MSG_APP_UTILITY_SCREEN_PREVIEW,     &servUtility::getScrPreview),

on_set_int_void(    servUtility::cmd_dso_saver_tmo,     &servUtility::setSaverTmo),
on_set_int_void(    servUtility::cmd_dso_saver_start,   &servUtility::setScrStart),
on_get_bool(        servUtility::cmd_dso_saver_start,   &servUtility::getScrStart),

on_set_int_void(    servUtility::cmd_dso_saver_stop,    &servUtility::onScrStop),

//!screensaver-time
on_set_int_int(     MSG_APP_UTILITY_SCREEN_TIME,        &servUtility::setWaitTime),
on_get_int(         MSG_APP_UTILITY_SCREEN_TIME,        &servUtility::getWaitTime),

//!wordview
on_set_int_string(  MSG_APP_UTILITY_SCREEN_WORD,        &servUtility::setTextContent),
on_get_string(      MSG_APP_UTILITY_SCREEN_WORD,        &servUtility::getTextContent),

//!screensaver-picture
on_set_int_void(    MSG_APP_UTILITY_SCREEN_PICTURE,     &servUtility::selectPicture),

on_set_int_string(  servUtility::cmd_dso_saver,         &servUtility::setPicturePath),
on_get_string(      MSG_APP_UTILITY_SCREEN_PICTURE,     &servUtility::getPicturePath),

on_set_int_int(     MSG_APP_UTILITY_SCREEN_DEFAULT,     &servUtility::onDefaultScr),
//self-check
on_get_void_argi_argo( servUtility::cmd_self_check_item, &servUtility::getTestItemValue),

//!key test
on_set_int_void(    MSG_APP_UTILITY_KEY_TEST,           &servUtility::setKeyTest),
on_get_bool(        MSG_APP_UTILITY_KEY_TEST,           &servUtility::getKeyTest),
//!led test
on_set_int_int(     servUtility::cmd_dso_ledon,         &servUtility::setLedOn),
on_set_int_int(     servUtility::cmd_dso_ledoff,        &servUtility::setLedOff),
on_set_int_bool(    servUtility::cmd_dso_keytest,       &servUtility::setKeyTestOver),

//!screen test
on_set_int_void(    MSG_APP_UTILITY_SCREEN_TEST,        &servUtility::setLcdTest),
on_get_bool(        MSG_APP_UTILITY_SCREEN_TEST,        &servUtility::getLcdTest),
on_set_int_int(     servUtility::cmd_dso_lcdback,       &servUtility::setBkLight),


//!touch screen test
on_set_int_void(    MSG_APP_UTILITY_TOUCH_TEST,         &servUtility::setTouchTest),
on_get_bool(        MSG_APP_UTILITY_TOUCH_TEST,         &servUtility::getTouchTest),

on_set_int_void(    MSG_APP_UTILITY_FAN_TEST,           &servUtility::setFanCheck),
on_get_bool(        MSG_APP_UTILITY_FAN_TEST,           &servUtility::getFanCheck),

on_set_int_bool(    MSG_TOUCH_ENABLE,                   &servUtility::setTouchEnable),
on_get_bool    (    MSG_TOUCH_ENABLE,                   &servUtility::getTouchEnable),

//!fan test
on_set_int_int(     servUtility::cmd_dso_fanspeed,      &servUtility::setFanSpeed),
on_get_int(         servUtility::cmd_dso_fanspeed,      &servUtility::getFanSpeed),
//!internal equipment test

on_set_int_void(    MSG_APP_UTILITY_SELF_TEST,          &servUtility::setBoardTest),
on_get_bool(        MSG_APP_UTILITY_SELF_TEST,          &servUtility::getBoardTest),
on_set_int_void(    servUtility::cmd_dso_checkboard,    &servUtility::checkResult),
on_get_pointer(     servUtility::cmd_dso_checkboard,    &servUtility::getTestItem),


on_set_int_bool(    servUtility::cmd_lock_input,        &servUtility::setInputLock),
on_set_int_bool(    MSG_APP_UTILITY_LOCK_KB,            &servUtility::setKbLocker),
on_get_bool(        MSG_APP_UTILITY_LOCK_KB,            &servUtility::getKbLocker),

on_set_int_int  (   CMD_SERVICE_SUB_ENTER,              &servUtility::onEnterSubMenu),
on_set_int_int  (   CMD_SERVICE_SUB_RETURN,             &servUtility::onExitSubMenu),

on_set_int_void(    MSG_APP_UTILITY_AUTO_OPT,           &servUtility::autoOption),

on_set_int_int  (   MSG_MASK_OPERATE,                   &servUtility::onMaskAux),
on_set_int_int  (   MSG_MASK_OUT_ONOFF,                 &servUtility::onMaskAux),
//!key
on_set_int_int(     servUtility::cmd_key_press,         &servUtility::onKeyPress),
on_set_int_arg(     servUtility::cmd_key_increase,      &servUtility::onKeyInc),
on_set_int_arg(     servUtility::cmd_key_decrease,      &servUtility::onKeyDec),

on_set_int_arg(     servUtility::cmd_web_touch,         &servUtility::onWebEvent),
on_set_int_string(  servUtility::cmd_dso_label,         &servUtility::onShowLabel),

//!key record
on_set_int_arg (    CMD_SERVICE_PHY_KEY,                  &servUtility::on_phy_key),
on_set_void_void(   servUtility::cmd_key_record_run,      &servUtility::keyRecordRun),
on_set_void_void(   servUtility::cmd_key_record_suspend,  &servUtility::keyRecordSuspend),
on_set_void_void(   servUtility::cmd_key_record_stop,     &servUtility::keyRecordStop),
on_set_void_void(   servUtility::cmd_key_record_save,     &servUtility::keyRecordSave),
on_set_int_string(  servUtility::cmd_key_record_name ,    &servUtility::setKeyRecordName),

on_set_void_void(   CMD_SERVICE_DO_ERR,                   &servUtility::onCMD_SERVICE_DO_ERR),
on_set_void_void(   servUtility::cmd_dso_hori_mode,       &servUtility::onTimeMode),

on_set_int_void(    CMD_ZONE_TOUCH_ENABLE,                &servUtility::onZoneTouchOnOff),
on_get_bool(        CMD_ZONE_TOUCH_ENABLE,                &servUtility::getZoneTouchOnOff),

//scpi
on_get_int(     servUtility::cmd_scpi_gamount ,    &servUtility::getGamount),
on_get_int(     servUtility::cmd_scpi_ramount ,    &servUtility::getRamount),
on_get_string(  servUtility::cmd_scpi_version ,    &servUtility::getScpiVersion),
on_get_bool(    servUtility::cmd_dg_status ,       &servUtility::getDgStatus),

on_set_int_string(     servUtility::cmd_scpi_screen_time,        &servUtility::setScpiWaitTime),
on_get_string(         servUtility::cmd_scpi_screen_time,        &servUtility::getScpiWaitTime),


end_of_entry()

/*!
 * \brief servUtility::servUtility
 * \param name
 * \param eId
 */
servUtility::servUtility(QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    ISerial::mVersion = 0xe;

    m_pTestThread   = NULL;

    strWPUVer       = "";
    strSPUVer       = "";
    strSCUVer       = "";

    //! user band
    mRawBand = BW_100M;
    mSysBand = BW_100M;

    iYear   = QDate::currentDate().year();
    if ( iYear < 2017 )
    {
        iYear = 2017;
    }
    iMonth  = QDate::currentDate().month();
    iDay    = QDate::currentDate().day();
    iHour   = QTime::currentTime().hour();
    iMinute = QTime::currentTime().minute();
    iSecond = QTime::currentTime().second();

    //Keys for project
    QList<int> key;
    key << R_Pkey_TRIG_MENU << R_Pkey_TRIG_MENU << R_Pkey_TRIG_FORCE << R_Pkey_TRIG_MENU;
    appendQuickKey(key, servUtility::cmd_quick_project );

    m_recordRun  =  false;
    m_recordName = "test";
    m_bZoneTouch = false;

    m_nSubMenu   = 0;
}

int servUtility::serialOut( CStream &stream ,unsigned char &ver)
{
    ver = mVersion;

    stream.write(QStringLiteral("beeper"),    m_bBeeper);
    stream.write(QStringLiteral("auxout"),    m_nAuxout);
    //stream.write(QStringLiteral("locker"),    m_bKbLocker);
    stream.write(QStringLiteral("touch"),     m_bTouchEnable);
    stream.write(QStringLiteral("zone"),      m_bZoneTouch);

    stream.write(QStringLiteral("saver"),     m_nScrSaver);
    stream.write(QStringLiteral("time"),      m_nWaitTime);
    stream.write(QStringLiteral("text"),      m_strText);
    stream.write(QStringLiteral("path"),      m_picPath);

//    stream.write(QStringLiteral("m_nStartTimes"),     m_nStartTimes);
    return 0;
}

int servUtility::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("beeper"),   m_bBeeper);
        stream.read(QStringLiteral("auxout"),   m_nAuxout);
        //stream.read(QStringLiteral("locker"),   m_bKbLocker);
        stream.read(QStringLiteral("touch"),    m_bTouchEnable);
        stream.read(QStringLiteral("zone"),     m_bZoneTouch);


        stream.read(QStringLiteral("saver"),    m_nScrSaver);
        stream.read(QStringLiteral("time"),     m_nWaitTime);

        stream.read(QStringLiteral("text"),     m_strText);
        stream.read(QStringLiteral("path"),     m_picPath);

//        stream.read(QStringLiteral("m_nStartTimes"),     m_nStartTimes);
//        m_nStartTimes++;
    }
    return 0;
}

void servUtility::rst()
{
    m_bProjectMode  = false;

    m_bBeeper       = false;
    m_nAuxout       = 0;

    m_bShowInfo     = false;

    ////////////key locker
    m_bKbLocker     = false;
    m_nSubMenu      = 0;

    ////////self-check
    m_bShowKbTest   = false;
    m_bShowTouchTest= false;
    m_bShowLcdTest  = false;
    m_bShowBoardTest= false;

    m_bShowTime     = true;
    m_bTouchEnable  = true; //default touch is enable


    ////////////screen saver
    m_nScrSaver     = 0;
    m_picPath       = DEFAULT_SCR_PIC;
    m_strText       = DEFAULT_SCR_TXT;
    m_bPreview      = false;
    m_bScrShow      = false;
    m_nWaitTime     = 30;

    //! \todo full speed
    m_nFanSpeed     = 100;

    //! keep the sysband

    //! zone touch
    m_bZoneTouch = false;

    mUiAttr.setEnable(MSG_APP_UTILITY_AUXOUT, true);
}

void servUtility::init()
{
    //! uuid from adc
    //! different for each chip
    queryEngine(qENGINE_ADC_UUID, (char*)fileKeys);

    //! load model info.
    readInformation();

    //![dba:位置搬移到startup中，否则当从fram中恢复数据时，servdso还没加载，获取的长度为0]
    //! [dba, 2018-2-8] 搬走后影响选件，暂时改回来，TR5之后重新思考方案。
    initSysVendorConfig();

    mUiAttr.setVisible(MSG_APP_UTILITY_SCREEN_PICTURE, true);

    mUiAttr.setVisible(MSG_APP_UTILITY_RECORD_KEY, false);


    setMsgAttr( cmd_dso_saver_tmo );
}

DsoErr servUtility::start()
{    
    createTestTable();

    /* QThread's parent can't be this */
    m_pTestThread = new BoardTestThread( this );
    Q_ASSERT( NULL != m_pTestThread );
    m_pTestThread->start();
    return ERR_NONE;
}

int servUtility::startup()
{
    setBeeper(m_bBeeper);
    setFanSpeed(m_nFanSpeed);
    setAuxout(m_nAuxout);
    setTouchLocker(m_bTouchEnable);
    setProjectMenu( m_bProjectMode);

    setScrSaver(m_nScrSaver);

    //! 根据带宽选件状态设置系统带宽
    setSpyonOpt();

    onTimeMode();

    updateAllUi();

    if( m_bKbLocker )
    {
        setInputLock( true );
    }
    return 0;
}

void servUtility::registerSpy()
{
    spyOn( serv_name_gui,
           CMD_SERVICE_DO_ERR );

    spyOn( serv_name_hori,MSG_HOR_TIME_MODE,cmd_dso_hori_mode);
    spyOn( serv_name_hori,MSG_HOR_ZOOM_ON,  cmd_dso_hori_mode );

    spyOn( serv_name_license, servLicense::cmd_Opt_Active,  cmd_spyon_opt);
    spyOn( serv_name_license, servLicense::cmd_Opt_Exp,     cmd_spyon_opt);
    spyOn( serv_name_license, servLicense::cmd_Opt_Invalid, cmd_spyon_opt);
    spyOn( serv_name_dso,     CMD_SERVICE_MODIFIED,         cmd_dso_saver_tmo);


    spyOn( serv_name_mask, MSG_MASK_OPERATE);
    spyOn( serv_name_mask, MSG_MASK_OUT_ONOFF);
}

/*!
 * \brief servUtility::setBeeper
 * \param btn
 * \return
 */
DsoErr servUtility::setBeeper(bool btn)
{
    m_bBeeper = btn;

    if ( m_bBeeper )
    {
        shortBeeper();
    }
    else
    {}

    return ERR_NONE;
}

void servUtility::tickBeeper()
{
    syncEngine( ENGINE_BEEP_TICK );
}

DsoErr servUtility::shortBeeper( int force )
{
    if ( m_bBeeper || force == 1 )
    {
        syncEngine( ENGINE_BEEP_SHORT );
    }

    return ERR_NONE;
}

//! int, count, bRelease
DsoErr servUtility::onPhyKey( CArgument &arg )
{
    //! only release
    if (m_bBeeper && arg.size() == 3 && arg[2].bVal )
    {
        tickBeeper();
    }

    return ERR_NONE;
}

/*!
 * \brief servUtility::getBeeper
 * \return
 */
bool servUtility::getBeeper()
{
    return m_bBeeper;
}

/*!
 * \brief servUtility::setLanguage
 * \param cmb
 * \return
 */
DsoErr servUtility::setLanguage(SystemLanguage cmb)
{
    sysSetLanguage( cmb );
    return ERR_NONE;
}
/*!
 * \brief servUtility::getLanguage
 * \return
 */
SystemLanguage servUtility::getLanguage()
{
    return sysGetLanguage();
}

/*!
 * \brief servUtility::setAux
 * \param btn
 * \return
 */
DsoErr servUtility::setAuxout(bool btn)
{
    if(btn == false)
    {
        syncEngine( ENGINE_TRIG_OUT_MUX, 0x0 ); //! sys trig
    }
    else
    {
        syncEngine( ENGINE_TRIG_OUT_MUX, 0x3 ); //! mask
    }
    m_nAuxout = btn;
    return ERR_NONE;
}
/*!
 * \brief servUtility::getAux
 * \return
 */
bool servUtility::getAuxout()
{
    return (bool)m_nAuxout;
}

DsoErr servUtility::onMaskAux(int)
{
    bool b = false;
    bool a = false;
    query( serv_name_mask, MSG_MASK_OUT_ONOFF, b);
    query( serv_name_mask, MSG_MASK_OPERATE, a);
    //qDebug() << "mask aux:" << b;
    //if( b )
    {
        mUiAttr.setEnable(MSG_APP_UTILITY_AUXOUT, !(b & a));
    }

    return ERR_NONE;
}

DsoErr servUtility::autoOption(int)
{
    CIntent intent;

    intent.setIntent( MSG_AUTO_CFG_TITLE,
                      1,
                      1,
                      1);

    startIntent( serv_name_autoset, intent );
    return ERR_NONE;
}

/*!
 * \brief servUtility::keyEat
 *
 * \param m_bKbLocker
 * \return
 */
//! 被锁定的键不会执行功能
bool servUtility::keyEat(int key, int /*count*/, bool /*bRelease*/ )
{
    if ( !isActive() )
    {
        return false;
    }
    if ( (m_nSubMenu == MSG_APP_UTILITY_SYSTEM_MENU && key == R_Pkey_F5) ||
         key == R_Pkey_PageOff)
    {
        return false;
    }
    //! 全键盘被锁定
    if ( m_bKbLocker  )
    {
        servGui::showErr( ERR_KEYBOARD_LOCKED );
        return true;
    }

    return false;
}

//only lock touch when it is enable
DsoErr servUtility::setInputLock(bool b)
{
    if( m_bTouchEnable )
    {
        b = !b;
        setTouchLocker(b);
    }
    return ERR_NONE;
}

DsoErr servUtility::setKbLocker(bool b)
{
    if ( m_bKbLocker != b )
    {
        if(b)
        {
            servGui::showInfo(sysGetString( ERR_THE_KEY_LOCKED, "Key locked!" ));
        }
        else
        {
            servGui::showInfo(sysGetString( ERR_THE_KEY_UNLOCKED, "Key unlocked!" ));
        }
    }

    m_bKbLocker = b;
    if( m_bTouchEnable )
    {
        setTouchLocker(!b);
    }
    return ERR_NONE;
}

bool servUtility::getKbLocker()
{
    return m_bKbLocker;
}


bool servUtility::getTouchEnable()
{
    return m_bTouchEnable;
}

void servUtility::setTouchLocker(bool b)
{
#if 0
    QString devTouch = "/sys/bus/i2c/devices/0-0048/state";
    QFile file(devTouch);
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        out << (int)b;
        file.close();
    }
#endif
    sysSetTouchEnable(b);
    if( b )
    {
        syncEngine(ENGINE_LED_OFF, led_touch);
    }
    else
    {
        syncEngine(ENGINE_LED_ON, led_touch);
    }
}

DsoErr servUtility::onEnterSubMenu(int menu)
{
    m_nSubMenu = menu;
    return ERR_NONE;
}

DsoErr servUtility::onExitSubMenu(int menu)
{
    m_nSubMenu = menu;
    return ERR_NONE;
}

int servUtility::setTouchEnable(bool b)
{
    setTouchLocker(b);    
    m_bTouchEnable = b;
    if( m_bTouchEnable )
    {
        servGui::showInfo( INF_TOUCH_ENABLE);
    }
    else
    {
        servGui::showInfo( (INF_TOUCH_DISABLE));
    }
    return ERR_NONE;
}

void servUtility::onCMD_SERVICE_DO_ERR()
{
    shortBeeper();
}

void servUtility::setProjectMenu(bool b)
{
    if( b )
    {
        mUiAttr.setVisible( MSG_APP_UTILITY_FAN_TEST, true);
        mUiAttr.setVisible( MSG_APP_UTILITY_SELF_TEST, true);

        mUiAttr.setEnableVisible( MSG_APP_UTILITY_CHECK_MORE_MENU, true,true);
        servGui::showInfo("Enter Project mode");
        mUiAttr.setEnable(MSG_APP_UTILITY_FAN_TEST, true);
        mUiAttr.setEnable(MSG_APP_UTILITY_SELF_TEST, true);


        mUiAttr.setVisible(MSG_APP_UTILITY_RECORD_KEY, true);

        //! switch to help menu
        post(serv_name_help, CMD_SERVICE_ACTIVE,1);
    }
    else
    {
        mUiAttr.setVisible( MSG_APP_UTILITY_FAN_TEST, false);
        mUiAttr.setVisible( MSG_APP_UTILITY_SELF_TEST, false);
        mUiAttr.setEnableVisible( MSG_APP_UTILITY_CHECK_MORE_MENU,false, false);
        //servGui::showInfo("Exit Project mode");
    }
}

DsoErr servUtility::setProjectMode()
{
    m_bProjectMode = !m_bProjectMode;
    async( MSG_APP_UTILITY_PROJECT, m_bProjectMode);

    setProjectMenu(m_bProjectMode);

    return ERR_NONE;
}

bool servUtility::getProjectMode( )
{
    return m_bProjectMode;
}


bool servUtility::isProjectMode()
{
    bool b = false;
    serviceExecutor::query(serv_name_utility,MSG_APP_UTILITY_PROJECT, b);
    return b;
}

int servUtility::onActive(int msg)
{
    if ( msg == MSG_APP_UTILITY_SHUTDOWN )
    {
        async( MSG_APP_UTILITY_SHUTDOWN, 0 );
        return 0;
    }
    if ( msg == CMD_ZONE_TOUCH_ENABLE )
    {
        async( CMD_ZONE_TOUCH_ENABLE, 0 );
        return 0;
    }
    else
    {
        m_bShowFanTest  = false;
        m_bShowBoardTest= false;

        setActive(msg);

        return 0;
    }
}

int servUtility::getGamount()
{
    int mode;
    serviceExecutor::query(serv_name_dso, servDso::CMD_SCREEN_MODE, mode );
    DsoScreenMode scrMode = (DsoScreenMode)mode;
    if( scrMode == screen_xy_full ||
        scrMode == screen_xy_normal ||
        scrMode == screen_xy_fft )
    {
        return 8;
    }
    return 10;
}
int servUtility::getRamount()
{
    return 4;
}

QString servUtility::getScpiVersion()
{
    return "0.0.1";
}

bool servUtility::getDgStatus()
{
    return  sysCheckLicense(OPT_DG);
}

void servUtility::onTimeMode()
{
    int mode = 0;
    query(serv_name_hori,
          MSG_HOR_TIME_MODE,
          mode);

    bool en;
    query(serv_name_hori, MSG_HOR_ZOOM_ON, en);

    if( mode == Acquire_YT)
    {
        mUiAttr.setEnable(MSG_APP_UTILITY_RECODER_MENU, true);
        if( en == false )
        {
            mUiAttr.setEnable(MSG_MASK_MENU, true);
        }
        else
        {
            mUiAttr.setEnable(MSG_MASK_MENU, false);
        }
    }
    else
    {
        mUiAttr.setEnable(MSG_APP_UTILITY_RECODER_MENU, false);
        mUiAttr.setEnable(MSG_MASK_MENU, false);
    }

    if( m_bZoneTouch && ((mode == Acquire_XY) || (mode == Acquire_ROLL)) )
    {
        defer(CMD_ZONE_TOUCH_ENABLE);
    }
}

DsoErr servUtility::onZoneTouchOnOff()
{
    //! xy, roll 禁用区域绘制功能。
    int mode;
    serviceExecutor::query( serv_name_hori,
                            MSG_HOR_TIME_MODE, mode );

    if( !((mode == Acquire_XY) || (mode == Acquire_ROLL)) )
    {
        m_bZoneTouch = !m_bZoneTouch;
    }
    else if(m_bZoneTouch)
    {
        m_bZoneTouch =  false;
    }
    else
    {
        //return ERR_ACTION_DISABLED;
    }

    return ERR_NONE;
}

bool servUtility::getZoneTouchOnOff()
{
    return m_bZoneTouch;
}
