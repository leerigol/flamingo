#include "./servutility.h"
#include "../../com/keyevent/rdsokey.h"
#include <unistd.h>
void servUtility::keyRecordRun()
{
    m_recordRun = true;
}

void servUtility::keyRecordSuspend()
{
    m_recordRun = false;
}

void servUtility::keyRecordStop()
{
    m_recordRun = false;
    keyRecordSave();
    m_keyRecordData.clear();
}

void servUtility::keyRecordSave()
{
    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__;
    QString filePath = QString("%1%2%3")
            .arg("/user/data/")
            .arg(m_recordName)
            .arg(".txt");
    QFile file(filePath);
    if(file.open( QIODevice::WriteOnly))
    {
        file.write(m_keyRecordData.toLatin1());
    }
    file.flush();
    fsync(file.handle());
    file.close();
}

DsoErr servUtility::setKeyRecordName(QString name)
{
    m_recordName = name;
    return ERR_NONE;
}

struct keyMap
{
    int     key;
    QString scpiStr;
};

keyMap keyPresMap[] = {
    {r_key_ch1        , "CH1       "},
    {r_key_ch2        , "CH2       "},
    {r_key_ch3        , "CH3       "},
    {r_key_ch4        , "CH4       "},
    {r_key_math       , "MATH      "},
    {r_key_ref        , "REF       "},
    {r_key_la         , "LA        "},
    {r_key_decode1    , "DECode1   "},
    {r_key_moff       , "MOFF      "},
    {r_key_f1         , "F1        "},
    {r_key_f2         , "F2        "},
    {r_key_f3         , "F3        "},
    {r_key_f4         , "F4        "},
    {r_key_f5         , "F5        "},
    {r_key_f6         , "F6        "},
    {r_key_f7         , "F7        "},
    {r_key_qprevious  , "NPRevious "},
    {r_key_qnext      , "NNEXt     "},
    {r_key_qstop      , "NSTop     "},
    {r_key_vposition1 , "VPOSition1"},
    {r_key_vposition2 , "VPOSition2"},
    {r_key_vposition3 , "VPOSition3"},
    {r_key_vposition4 , "VPOSition4"},
    {r_key_vscale1    , "VSCale1   "},
    {r_key_vscale2    , "VSCale2   "},
    {r_key_vscale3    , "VSCale3   "},
    {r_key_vscale4    , "VSCale4   "},
    {r_key_hscale     , "HSCale    "},
    {r_key_hposition  , "HPOSition "},
    {r_key_kfunction  , "KFUNction "},
    {r_key_tlevel     , "TLEVel    "},
    {r_key_tmenu      , "TMENu     "},
    {r_key_tmode      , "TMODe     "},
    {r_key_clear      , "CLEar     "},
    {r_key_auto       , "AUTO      "},
    {r_key_rstop      , "RSTop     "},
    {r_key_single     , "SINGle    "},
    {r_key_quick      , "QPRInt    "},
    {r_key_measure    , "MEASure   "},
    {r_key_acquire    , "ACQuire   "},
    {r_key_storage    , "STORage   "},
    {r_key_cursor     , "CURSor    "},
    {r_key_display    , "DISPlay   "},
    {r_key_utility    , "UTILity   "},
    {r_key_tforce     , "TFORce    "},
    {r_key_source1    , "SOURce1   "},
    {r_key_source2    , "SOURce2   "},
    {r_key_back       , "BACK      "},
    {r_key_touch      , "TOUch     "},
    {r_key_zoom       , "ZOOM      "},
    {r_key_navigate   , "NAVIgate  "},
    {r_key_wave_scale , "WVOLT     "},
    {r_key_wave_offset, "WPOS      "},
    {r_key_default    , "DEFault   "},
};

static bool keyScpiFormat(int key, QString &str)
{
    for(int i = 0; i < array_count(keyPresMap); i++)
    {
        if(key == keyPresMap[i].key)
        {
            str = keyPresMap[i].scpiStr;
            return true;
        }
    }
    return false;
}

DsoErr servUtility::on_phy_key(CArgument &arg)
{
    if( (!m_recordRun)||(!arg[2].bVal) )
    {
        return ERR_NONE;
    }
    QString strKey;
    QMap<int, quint32>::const_iterator i = keyDsoMap.constBegin();
    int iKey = 0;

    while (i != keyDsoMap.constEnd())
    {
        iKey = i.value();
        if(iKey == arg[0].iVal)
        {
            if( keyScpiFormat(i.key(), strKey) )
            {
                m_keyRecordData.append(QString(":SYSTem:KEY:PRESs %1\n").arg(strKey));
            }
            return ERR_NONE;;
        }
        ++i;
    }

    i = keyTuneMap.constBegin();
    while (i != keyTuneMap.constEnd())
    {

        iKey = Encode_ScanCode( encode_inc(i.value()) );
        if(iKey == arg[0].iVal)
        {
            if( keyScpiFormat(i.key(), strKey) )
            {
                m_keyRecordData.append(QString(":SYSTem:KEY:INCRease %1,%2\n").arg(strKey).arg(arg[1].iVal));
            }
            return ERR_NONE;;
        }

        iKey = Encode_ScanCode( encode_dec(i.value()) );
        if(iKey == arg[0].iVal)
        {
            if( keyScpiFormat(i.key(), strKey) )
            {
                m_keyRecordData.append(QString(":SYSTem:KEY:DECRease %1,%2\n").arg(strKey).arg(arg[1].iVal));
            }
            return ERR_NONE;;
        }
        ++i;
    }

    //QTime::currentTime().toString("hh:mm:ss.zzz");
    return ERR_NONE;
}
