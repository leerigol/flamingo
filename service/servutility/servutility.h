#ifndef UTILITY_H
#define UTILITY_H

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
//#include "../../include/dsostd.h"
#include "../../include/dsotype.h"
#include "boardItem.h"

#define DEFAULT_MODEL_DSO  "DS7014"
#define DEFAULT_MODEL_MSO  "MSO7014"
#define DEFAULT_VERSION    "00.01.01.07.01"
#define DEFAULT_SERIAL_DSO "DS7A7014789012345"
#define DEFAULT_SERIAL_MSO "DS7F7014789012345"

#define DEFAULT_SCR_PIC ":/pictures/utility/scr.jpg"
#define DEFAULT_SCR_TXT "RIGOL Scope"

#define LA_BIT (1<<5)
#define DG_BIT (1<<6)

#define  SYS_INFO_ID_BASE  (100)


class servUtility : public serviceExecutor,
                    public ISerial
{
    Q_OBJECT

    DECLARE_CMD()

 public:
    enum
    {
        cmd_none,
        cmd_dso_restart,
        cmd_dso_shutdown,

        cmd_dso_model,
        cmd_dso_serial,
        cmd_dso_firmware,
        cmd_dso_hardware,
        cmd_dso_bootware,
        cmd_dso_spu,
        cmd_dso_wpu,
        cmd_dso_scu,
        cmd_dso_mcu,

        cmd_dso_builddate,

        cmd_dso_adc_id0,
        cmd_dso_adc_id1,
        cmd_dso_afe_ver0,
        cmd_dso_afe_ver1,
        cmd_dso_afe_ver2,
        cmd_dso_afe_ver3,

        cmd_dso_saver,
        cmd_dso_saver_tmo,
        cmd_dso_saver_start,
        cmd_dso_saver_stop,

        cmd_dso_ledon,
        cmd_dso_ledoff,
        cmd_dso_keytest,

        //for scpi key
        cmd_key_press,
        cmd_key_increase,
        cmd_key_decrease,
        cmd_web_touch,
        cmd_lock_input,

        cmd_dso_lcdback,//back light
        cmd_dso_fanspeed,

        cmd_dso_checkboard,
        cmd_dso_poweroff,
        cmd_dso_beeper,//for other service
        cmd_dso_hori_mode,

        cmd_system_pres,
        cmd_system_band = cmd_system_pres,
        cmd_model_band, //model bandwidth

        cmd_vend_config,

        cmd_spyon_opt,     //!选件到期  监听

        cmd_system_date,       //!设置系统日期
        cmd_system_time,       //!设置系统时间

        cmd_self_check_item,   //!查询硬件信息
        cmd_dso_label,          //show label or not on fixed position
        cmd_hdmi_event,
        cmd_hdmi_edid,

        cmd_quick_project,

        cmd_key_record_run,
        cmd_key_record_suspend,
        cmd_key_record_stop,
        cmd_key_record_name,
        cmd_key_record_save,

        //scpi
        cmd_scpi_gamount,
        cmd_scpi_ramount,
        cmd_scpi_version,
        cmd_dso_module,
        cmd_dg_status,
        cmd_scpi_screen_time,
    };

    enum scr_content
    {
        scr_saver_off,
        scr_saver_pic,
        scr_saver_txt
    };

    enum
    {
        FRAM_ID_SYS_INFO,
        FRAM_ID_SYS_MAC
    };

    servUtility( QString name, ServiceId eId = E_SERVICE_ID_UTILITY );


    int             serialOut(CStream &stream, unsigned char &ver);
    int             serialIn(CStream &stream, unsigned char ver);
    void            rst();
    void            init();
    int             startup();
    DsoErr          start();
    void            registerSpy();

    static QString  getSystemModel();
    static QString  getSystemSerial();
    static QString  getSystemVersion();
    static bool     getSystemKeyLocker();
    static bool     isMSO();
    static bool     isHasDG();
    static bool     isProjectMode();

public:

    DsoErr          setProjectMode();
    bool            getProjectMode();
    void            setProjectMenu(bool b=false);

    //!Beeper
    DsoErr          setBeeper( bool btn );
    bool            getBeeper();

    DsoErr          onPhyKey( CArgument &arg );
    void            tickBeeper( );
    DsoErr          shortBeeper( int forceBeep = 0 );

    //!Language
    DsoErr          setLanguage( SystemLanguage cmb );
    SystemLanguage  getLanguage();

    //!AUX output
    DsoErr          setAuxout(bool btn);
    bool            getAuxout();

    int             onActive(int msg);

//system
    //!systeminfo
    DsoErr          setShowInfo(void);
    bool            getShowInfo();

    DsoErr          setPowerStatus(bool b);
    bool            getPowerStatus();

    //!m_bPowerOn
    DsoErr          setPowerOn(bool b);
    bool            getPowerOn();

    //!show time on lable
    DsoErr          setDateTime();
    void            getDateTime();

    //!system time scpi info
    DsoErr          scpiSetDate(CArgument argi);
    void            scpiGetDate(CArgument &argo);
    DsoErr          ScpiSetTime(CArgument argi);
    void            scpiGetTime(CArgument &argo);

    //time setting
    DsoErr          setTimeShow(bool b);
    bool            getTimeShow();

    DsoErr          setYear(int y);
    DsoErr          setMonth(int m);
    DsoErr          setDay(int d);
    DsoErr          setHour(int h);
    DsoErr          setMinute(int m);
    DsoErr          setSecond(int s);
    void            saveTime();

    int             getYear();
    int             getMonth();
    int             getDay();
    int             getHour();
    int             getMinute();
    int             getSecond();

    //!change the system infomation model
    DsoErr          setModel(const QString &model );
    QString         getModel();

    //!change the system infomation serial number
    DsoErr          setSerial(QString sn);
    QString         getSerial();

    QString         getModule();

    QString         getSoftVer();
    QString         getHardVer();
    QString         getBootVer();
    QString         getSPUVer();
    QString         getWPUVer();
    QString         getCCUVer();
    QString         getMCUVer();
    QString         getBuildDate();
    int             getAdcId0();
    int             getAdcId1();
    int             getAfeVer0();
    int             getAfeVer1();
    int             getAfeVer2();
    int             getAfeVer3();

    //!save the system mac
    DsoErr          saveMac(QString mac);

    //!change the start times
    DsoErr          setStartTimes(int times);
    DsoErr          setLiveTimes(int times);
    DsoErr          resetPrivateData(int times);
    DsoErr          recoverBoard(int times);

    //scpi
    int             getGamount();
    int             getRamount();
    QString         getScpiVersion();
    bool            getDgStatus();

protected:
    bool            parseBw( const QString &model, Bandwidth *pBw );
    //!系统带宽
    DsoErr          setSysBand(Bandwidth value);
    Bandwidth       getSysBand();
    Bandwidth       getRawBand();

    void            setSpyonOpt();  //!监听选件

private:
    Bandwidth       queryOptBand( bool *pbOk );

public:
    //!scpi command handle function
    DsoErr          resetSystem();
    DsoErr          shutdown();

    ////////////////lock key

    //!lock keyboard
    DsoErr          setKbLocker(bool keyboard);
    bool            getKbLocker(void);
    DsoErr          setInputLock(bool);//for other module

    DsoErr          onEnterSubMenu(int);
    DsoErr          onExitSubMenu(int);
    //!lock key
    DsoErr          autoOption(int);

    DsoErr          onMaskAux(int);

    DsoErr          onKeyPress(int key);
    DsoErr          onKeyInc(CArgument& arg);
    DsoErr          onKeyDec(CArgument& arg);
    DsoErr          postKeyEvent(quint32 key, int cnt=0);

    //For web control
    DsoErr          onWebEvent(CArgument& arg);
    int             toTouch(unsigned short type,
                       unsigned short code,
                       int value);

    int             onShowLabel(QString);

    void            onTimeMode();

    DsoErr          onZoneTouchOnOff();
    bool            getZoneTouchOnOff();

public:
//screen saver
    //!screensaver-openclose
    DsoErr          setScrSaver(int);
    int             getScrSaver();

    //!screensaver-preview
    DsoErr          setScrPreview(void);
    bool            getScrPreview();

    //!screen save time
    DsoErr          setWaitTime(int time);
    int                 getWaitTime();

    DsoErr          setScpiWaitTime(QString time);
    QString         getScpiWaitTime();

    //!screensaver wordView
    DsoErr          setTextContent(QString);
    QString         getTextContent();

    //!screensaver picture
    DsoErr          selectPicture(void);

    DsoErr          setPicturePath(QString);
    QString         getPicturePath();

    int             setScrStart();
    bool            getScrStart();

    int             onTimeout(int);
    int             onScrStop();
    DsoErr          setSaverTmo();

    int             onDefaultScr(int);

    //self check
    void            getTestItemValue(CArgument argi, CArgument &argo);

    //!keyboard test
    DsoErr          setKeyTest();
    bool            getKeyTest();

    DsoErr          setLedOn(int);
    DsoErr          setLedOff(int);

    DsoErr          setKeyTestOver(bool);
    QList<bool>     m_bLedStatus;

    //!screen test
    DsoErr          setLcdTest();
    bool            getLcdTest();
    DsoErr          setBkLight( int light );

    //!touch screen check
    DsoErr          setTouchTest();
    bool            getTouchTest();

    int             setTouchEnable(bool);
    bool            getTouchEnable();
    void            setTouchLocker(bool);

    //!fan check
    DsoErr          setFanSpeed(int);
    int             getFanSpeed();

    DsoErr          setFanCheck();
    bool            getFanCheck();
    DsoErr          shutDown();

    //!internal equipment test
    DsoErr          checkResult();
    DsoErr          setBoardTest();
    bool            getBoardTest();
    void*           getTestItem();

//!工厂用
//!×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××/
public:
    enum enumCfgItem
    {
        E_CFG_MANU = 1,		///< 配置厂商

        E_CFG_MODEL,        //！型号
        E_CFG_BAND,          //带宽
        E_CFG_SN,           //！序列号
        E_CFG_MODUELS,      //！模块(DG,LA,....)

        E_CFG_MODEL_RAW,     //！出厂型号
        E_CFG_BAND_RAW,      //！出厂带宽
        E_CFG_SN_RAW,        //！出厂序列号
        E_CFG_MODUELS_RAW,   //！出厂模块

        E_CFG_MAC,           //！MAC地址

        E_CFG_BUILD_TIME,    //！生产时间
        E_CFG_PROG_VER,
        E_CFG_FIRM_VER,
        E_CFG_CPLD_VER,
        E_CFG_MCU_VER,
        E_CFG_BOOT_VER,
        E_CFG_BOARD_VER,
        E_CFG_README,        //！附加信息
    };
    Q_ENUM(enumCfgItem) //记录枚举类型的QString名称

    /*!数据以ID为索引 存放*/
    typedef   QMap<enumCfgItem, QByteArray>  VendorData;
    DsoErr  initSysVendorConfig();

    //!从SCPI收到的 直接保存 密文格式
    VendorData  sysVendorMap;
    DsoErr  setSysVendorItem(CArgument arg);
    void    getSysVendorItem(CArgument id, CArgument &outData); //！SCPI使用     返回的密文格式
    DsoErr  getVendorItem(enumCfgItem id,  QByteArray &outdata);   //！程序内部使用  返回的明文格式

#if 0//!解析后的 明文格式
    VendorData sysVendorData;
    DsoErr  setSysVendorConfig(CArgument arg);
    void    getSysVendorConfig(CArgument id, CArgument outData);
#endif
    DsoErr  saveVendorData(VendorData &data);
    DsoErr  loadVendorData(VendorData &outData);
    DsoErr  encodeVendorData(QByteArray &data, unsigned int *newKey = NULL);
    DsoErr  decodeVendorData(QByteArray &data, unsigned int *newKey = NULL);

//!×××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××/

protected:
    void    onCMD_SERVICE_DO_ERR();
    virtual bool keyEat(int key, int count, bool bRelease );

private:
    void    readInformation();
    void    createKeyMapTable();
    void    postKeyEvent(int , int);
    void    createTestTable();

private:
    bool    m_bBeeper;
    int     m_nAuxout;

//system
private:
    bool        m_bShowInfo;//if show the information or not

     //what to do when the AC line is connected
    bool        m_bPowerStatus;

    bool        m_bKbLocker;
    int         m_nSubMenu;

    //!system_date
    int         system_time;
    int         system_date;

    //time
    int         iYear;
    int         iMonth;
    int         iDay;
    int         iHour;
    int         iMinute;
    int         iSecond;

    static quint16      m_nHardVer;
    static quint16      m_nCPLDVer;
    //!system info window display
    QString     strModel;
    Bandwidth   mRawBand;
    Bandwidth   mSysBand;

    QString     strSerial;
    QString     strSoftVer;
    QString     strHardVer;
    QString     strBootVer;
    QString     strSPUVer;
    QString     strWPUVer;
    QString     strSCUVer;
    QString     strSP6Ver;
    QString     strBuildDate;
    QString     strMac;

    //! Zone Touch
    bool        m_bZoneTouch;

//    int         m_nStartTimes;

//scr saver
private:
    //!screensave
    int         m_nScrSaver;//0-off 1-pic 2-text
    QString     m_strText;
    QString     m_picPath;
    bool        m_bPreview;
    bool        m_bScrShow;
    int         m_nWaitTime;//unit is minute

    //self-check
private:
    bool        m_bShowKbTest;
    int         m_LedStatus;

    bool        m_bShowTouchTest;
    bool        m_bShowLcdTest;
    bool        m_bShowFanTest;
    bool        m_bShowBoardTest;

    bool        m_bShowTime;
    bool        m_bTouchEnable;

    int         m_nFanSpeed; //default is max:50

    bool        m_bProjectMode;

    QThread     *m_pTestThread;

private:
    QMap <int, quint32>   keyDsoMap;
    QMap <int, quint32>   keyTuneMap;
    QList<stBoardTestItem*> m_testItems;

public:
    void            keyRecordRun();
    void            keyRecordSuspend();
    void            keyRecordStop();
    void            keyRecordSave();
    DsoErr          setKeyRecordName(QString name);
    DsoErr          on_phy_key(CArgument &arg);
private:
    bool         m_recordRun;
    QString      m_recordName;
    QString      m_keyRecordData;
};

//! 128bit
extern unsigned int fileKeys[4];


class BoardTestThread : public QThread
{
    Q_OBJECT
public:
    explicit BoardTestThread(QObject *parent = 0);
protected:
    virtual void run();

    DsoErr checking();
public:
    servUtility *m_pBoardTest;
};

enum KeyDso
{
    r_key_ch1 = 0,
    r_key_ch2 ,
    r_key_ch3,
    r_key_ch4,
    r_key_math,
    r_key_ref,
    r_key_la,
    r_key_decode1,
    r_key_moff,
    r_key_f1,
    r_key_f2,
    r_key_f3,
    r_key_f4,
    r_key_f5,
    r_key_f6,
    r_key_f7,
    r_key_qprevious,
    r_key_qnext,
    r_key_qstop,
    r_key_vposition1,
    r_key_vposition2,
    r_key_vposition3,
    r_key_vposition4,
    r_key_vscale1,
    r_key_vscale2,
    r_key_vscale3,
    r_key_vscale4,
    r_key_hscale,
    r_key_hposition,
    r_key_kfunction,
    r_key_tlevel,
    r_key_tmenu,
    r_key_tmode,
    r_key_tforce,
    r_key_clear,
    r_key_auto,
    r_key_rstop,
    r_key_single,
    r_key_quick,
    r_key_measure,
    r_key_acquire,
    r_key_storage,
    r_key_cursor,
    r_key_display,
    r_key_utility,

    r_key_source1,
    r_key_source2,
    r_key_back,
    r_key_touch,
    r_key_zoom,
    r_key_navigate,
    r_key_wave_scale,
    r_key_wave_offset,
    r_key_default,
};

#endif // UTILITY_H
