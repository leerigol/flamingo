#include "servwifi.h"
#include "servInterface.h"
#include "../service_msg.h"

IMPLEMENT_CMD( serviceExecutor, servWifi )
start_of_entry()

//on_set_int_int  (    CMD_SERVICE_ACTIVE ,                  &servWifi::setActive),
on_get_bool     (    CMD_SERVICE_ACTIVE,                   &servWifi::getActive),

on_set_int_void (    CMD_SERVICE_ENTER_ACTIVE ,            &servWifi::on_enterActive),
on_set_int_void (    CMD_SERVICE_EXIT_ACTIVE ,             &servWifi::on_exitActive),


on_set_void_void(    CMD_SERVICE_RST,                      &servWifi::rst),
on_set_int_void (    CMD_SERVICE_STARTUP,                  &servWifi::startup),

//!wifi state
on_set_int_bool (    MSG_APP_UTILITY_WIFI_OPENCLOSE,       &servWifi::setEnable),
on_get_bool     (    MSG_APP_UTILITY_WIFI_OPENCLOSE,       &servWifi::getEnable),

on_set_int_bool (    MSG_APP_UTILITY_WIFI_KEY,             &servWifi::setKey),
on_get_bool     (    MSG_APP_UTILITY_WIFI_KEY,             &servWifi::getKey),

//!wifi scan
on_set_int_void (    MSG_APP_UTILITY_WIFI_SCAN,            &servWifi::doScanning),
on_get_bool     (    MSG_APP_UTILITY_WIFI_SCAN,            &servWifi::getScanning),

//!wifi passwd
on_set_int_string(   MSG_APP_UTILITY_WIFI_NAME,            &servWifi::setName),
on_get_string   (    MSG_APP_UTILITY_WIFI_NAME,            &servWifi::getName),

//!wifi passwd
on_set_int_string(   MSG_APP_UTILITY_WIFI_PWD,             &servWifi::setPassword),
on_get_string   (    MSG_APP_UTILITY_WIFI_PWD,             &servWifi::getPassword),

//!wifi connect
on_set_int_void (    MSG_APP_UTILITY_WIFI_CONNECT,         &servWifi::doConnect),

on_set_int_int(     servInterface::cmd_net_status,               &servWifi::setNetStatus),
on_get_int(         servInterface::cmd_net_status,               &servWifi::getNetStatus),


end_of_entry()

servWifi::servWifi(QString name,ServiceId id ) : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();
}

servWifi::~servWifi()
{

}

int servWifi::serialOut( CStream &stream ,unsigned char &ver)
{
    ver = mVersion;

    stream.write(QStringLiteral("pwd"),   m_Password);

    return 0;
}

int servWifi::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("pwd"),  m_Password);
    }
    else
    {
        rst();
    }
    return 0;
}

void servWifi::rst()
{
    m_bEnable    = false;
    m_bKey       = false;
    m_Name       = "RIGOL";
    m_Password   = "";


    m_nNetStatus  = servInterface::NET_STATUS_CONNECTED;
    m_pWifiThread = NULL;

    setVisableMenu(false);
    setMenuEnable(false);
}

int servWifi::startup()
{
    return 0;
}

void servWifi::setVisableMenu(bool b)
{
    mUiAttr.setVisible(MSG_APP_UTILITY_WIFI_NAME, b);
    mUiAttr.setVisible(MSG_APP_UTILITY_WIFI_PWD,m_bKey & b);
    mUiAttr.setVisible(MSG_APP_UTILITY_WIFI_CONNECT,b);
    mUiAttr.setVisible(MSG_APP_UTILITY_WIFI_KEY,b);
}

void servWifi::setMenuEnable(bool b)
{
    //mUiAttr.setEnable( MSG_APP_UTILITY_WIFI_OPENCLOSE, b);
    if( !b )
    {
        setEnable( b );
    }
}

DsoErr servWifi::setEnable(bool b)
{
    m_bEnable = b;
    setVisableMenu(b);
    setConnect(b);
    return ERR_NONE;
}

bool servWifi::getEnable()
{
    return m_bEnable;
}

DsoErr servWifi::setKey(bool b)
{
    m_bKey = b;
    mUiAttr.setVisible(MSG_APP_UTILITY_WIFI_PWD, b);

    if( b )
    {
        setPassword(m_Password);
    }
    return ERR_NONE;
}

bool servWifi::getKey()
{
    return m_bKey;
}

DsoErr servWifi::doScanning()
{
    m_bScanning = !m_bScanning;
    return ERR_NONE;
}

bool servWifi::getScanning()
{
    return m_bScanning;
}

DsoErr servWifi::setName(QString& name)
{
    m_Name = name;
    if( name.size() == 0)
    {
        setConnect(false);
    }
    else
    {
        setConnect(true);
    }

    return ERR_NONE;
}

QString servWifi::getName()
{
    return m_Name;
}

DsoErr servWifi::setPassword(QString& pwd)
{
    m_Password = pwd;
    if(pwd.length() == 0)
    {
        setConnect(false);
    }
    else
    {
        setConnect(true);
    }
    return ERR_NONE;
}

QString servWifi::getPassword()
{
    return m_Password;
}


void servWifi::setConnect(bool b)
{
    mUiAttr.setEnable(MSG_APP_UTILITY_WIFI_CONNECT, b);
}

DsoErr servWifi::doConnect()
{
    if( m_pWifiThread == NULL )
    {
        m_pWifiThread = new WifiConfigThread( this );
    }

    if(m_pWifiThread && !m_pWifiThread->isRunning())
    {
        m_pWifiThread->start();

        qDebug() << "wifi is connecting";
    }

    setConnect(false);
    return ERR_NONE;
}


DsoErr servWifi::setNetStatus(int state)
{
    if(m_nNetStatus != state)
    {
        if(state == servInterface::NET_STATUS_CONNECTED)
        {
            setMenuEnable(true);
            defer( MSG_APP_UTILITY_WIFI_CONNECT );
            qDebug() << "wifi is ready";
        }
        else if(state == servInterface::NET_STATUS_UNLINK ||
                state == servInterface::NET_STATUS_UNBOUND)
        {
            setMenuEnable(false);
            //qDebug() << "wifi is not ready";
        }
        else if( state == servInterface::NET_STATUS_INIT)
        {
            setConnect(true);
        }

        m_nNetStatus = state;
    }
    return ERR_NONE;
}

int servWifi::getNetStatus()
{
    return m_nNetStatus;
}


bool servWifi::getActive()
{
    return isActive();
}

