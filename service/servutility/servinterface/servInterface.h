#ifndef SERV_INTERFACE_H
#define SERV_INTERFACE_H


#include <QDebug>
#include "../../service.h"

#include "../../service_msg.h"
#include "../../../baseclass/iserial.h"
#include "edid.h"

#define  NET_DEV   "eth0"
#define  WLAN_DEV  "wlan0"

#define  USE_CBB_LXI


class servInterface : public serviceExecutor,
                      public ISerial
{
    Q_OBJECT

    DECLARE_CMD()
public:

    servInterface(QString name, ServiceId id = E_SERVICE_ID_UTILITY_IOSET );
    ~servInterface();

    int  serialOut(CStream &stream,unsigned char &ver);
    int  serialIn(CStream &stream, unsigned char ver);
    void rst();
    void init();
    int  startup();

    static QString processCMD(const QString&, QStringList&);
public:

    enum
    {
        NET_IF_ETH,
        NET_IF_WLAN,
    };

    enum LXITYPE
    {
          IP_ACQUIRE_DHCP  = 0x1,
          IP_ACQUIRE_AUTO  = 0x2,
          IP_ACQUIRE_STATIC= 0x4,
    };

    /* 当前网络的状态 */
    enum tagNET_STATUS_EN
    {
        NET_STATUS_UNLINK = 0,    //网络无连接
        NET_STATUS_CONNECTED,     //网络已连接
        NET_STATUS_INIT,          //网络初始化
        NET_STATUS_UNBOUND,       //网络未绑定
        NET_STATUS_IPCONFLICT,    //网络IP冲突
        NET_STATUS_BUSY,          //网络繁忙
        NET_STATUS_CONFIGURED,    //网络已配置
        NET_STATUS_DHCP_FAILED,   //DHCP分配失败
        NET_STATUS_INVALID_IP,    //无效的IP
        NET_STATUS_IP_LOSE,        //当前IP地址失效,
        NET_STATUS_SET_MAC,
    };

    enum enumCmd
    {
        cmd_none,

        //!about scpi
        cmd_net_dhcp,
        cmd_net_auto,
        cmd_net_static,

        cmd_net_mode,//current IP acquired mode
        cmd_net_config,
        cmd_net_status,

        cmd_net_ip,
        cmd_net_mask,
        cmd_net_gateway,
        cmd_net_dns,
        cmd_net_mac,
        cmd_close_form,

        cmd_visa_addr,
        cmd_net_if,
        cmd_net_dhcp_server,
        cmd_net_init,
        cmd_net_rst,
        cmd_net_domain,
        cmd_net_description
    };

    enum
    {
        visa_type_usb,
        visa_type_lxi,
        visa_type_socket,
    };


 public:

    //!LAN config mode
    DsoErr setIPMode(int cmd);
    int    getIPMode();

    int    getNetMode();
    int    setNetMode(int);

    DsoErr applyConfig();
    DsoErr initConfig();

    int    doDefault();
    int    initLxi();
    int    netConfig( int reset=0);

    //!lan state changed,send the state to app
    DsoErr  setNetStatus(int state);
    int     getNetStatus();

    //!lan config mdns
    DsoErr  setmDNS(bool b);
    void    syncmDNS(bool);
    bool    getmDNS();

    DsoErr  setHostSettingName(QString name);
    QString getHostSettingName();

    DsoErr  setHostDeclareName(QString name);
    QString getHostDeclareName();

    DsoErr  setServiceSettingName(QString name);
    QString getServiceSettingName();

    DsoErr  setServiceDeclareName(QString name);
    QString getServiceDeclareName();

    //!lan net parmaters focus
    DsoErr  inputIP();
    DsoErr  inputGatway();
    DsoErr  inputNetmask();
    DsoErr  inputDNS();

    //!GPIB interface
    DsoErr  setGPIB(int b);
    int     getGPIB();

    DsoErr  showForm(int sig);
    bool    isShowForm();
    DsoErr  onFormClose();

    DsoErr  setEnHDMI(bool);
    bool    getEnHDMI();

    DsoErr  setDefHDMI(int);
    int     getDefHDMI();

    DsoErr  showEDID();
    bool    getShowEDID();
    DsoErr  onHDMI(bool);

    void    setHMDISigType(int);

    void    setHDMIEnable(bool);
    void*   getEDID();
    stEDID  m_stEDID;

public: //!SCPI handle

    DsoErr  setNetInterface(int);
    int     getNetInterface();
    char*   getInterfaceName();

    DsoErr  setDHCP( bool dhcp );
    bool    getDHCP();

    DsoErr  setAUTOip( bool Auto );
    bool    getAUTOip();

    DsoErr  setStatic( bool stc );
    bool    getStatic();

    DsoErr  setIP( QString ip );
    DsoErr  setIPAddr( int ip);
    QString getIP();
    int     getIPAddr();

    DsoErr  setMask( QString mask );
    DsoErr  setMaskAddr( int ip );
    QString getMask();
    int     getMaskAddr();

    DsoErr  setDHCPServer( QString );
    DsoErr  setDHCPAddr( int );
    QString getDHCPServer();
    int     getDHCPAddr();

    DsoErr  setGateway( QString gateway );
    DsoErr  setGatewayAddr( int ip );
    QString getGateway();
    int     getGatewayAddr();
    void    setGateWayEn(bool);

    DsoErr  setDNS( QString dns );
    DsoErr  setDNSAddr( int ip );
    QString getDNS();
    int     getDNSAddr();
    void    setDNSEn(bool);

    DsoErr  setMac( QString );
    QString getMac();

    void    getVisaAddr(CArgument type, CArgument argo);
    void    generateXML();

private:
    int     m_nSock;

    //!lan config parameter
    QString m_strIP;
    QString m_strMask;
    QString m_strGateway;
    QString m_strDNS;
    QString m_strMAC;
    QString m_strDHCP;

    //!lan link state
    int    m_nNetStatus;

    int    m_nIPMode;
    int    m_nNetMode;
    int    m_nNetInterface;//eth0 , wlan0;

    //!_INTERFACE pthread
    QThread *m_pLanConfig;

private:
    bool    m_bmDNS;
    QString HostSettingName;//config page   host name
    QString HostDeclareName;//welcome page  host name
    QString ServiceSettingName;//config page description
    QString ServiceDeclareName;//config page domain name
    bool    m_bShowForm;



    bool        m_bHDMI; // enable or not
    int         m_nHDMI; // definition
    bool        m_bEDID;
    int         m_nSigType;//1 hdmi 0 vga/dvi
    QThread     *m_pEDIDThead;

};

class LanConfigThread : public QThread
{
    Q_OBJECT
public:
    explicit LanConfigThread(QObject *parent = 0);

    static QString   readDefaultMAC();
protected:
    virtual void run();

private:

    bool   runDHCP();
    bool   runAUTO(QString& );

    DsoErr configMAC(QString mac);
    DsoErr configIP(QString&, QString&);
    DsoErr configGateway(QString& );
    DsoErr configDNS(QString&);

    void   lanConfig(int acqType = servInterface::IP_ACQUIRE_DHCP);
    void   wlanConfig();

    bool   checkConflict(QString&);
    void   readIPMask();
    void   readGateway();
    void   readDNS();
    void   readMAC();
private:
    servInterface  *m_pInterface;
};



class EDIDThread : public QThread
{
    Q_OBJECT
public:
    EDIDThread(QObject *parent = 0);
    ~EDIDThread();
public:
    servInterface *m_pUtility;

protected:
    virtual void run();

private:
    int i2c_file;
    char EDID[256];

    int i2cRead(unsigned short addr,
                 unsigned short reg,
                 unsigned char *val,
                 unsigned short len);

    int i2cWrite(unsigned short addr,
                 unsigned short reg,
                 unsigned char* val,
                 unsigned short len);

    int setSegmentPointer(int s32Addr,
                          unsigned char u8SegmentReg,
                          unsigned char u8Reg,
                          unsigned char *pu8Data,
                          int s32Length);

    void checkVPID();
    void checkVersion();
    void checkBasicParam();
    void checkColor();
    void checkTimes();
    void checkDTD();
    void checkExtBasic();
    void checkExtDTD();

    bool checkResolution(int x , int y, int hz);
    void genStandardTimes(int);
};


#endif // SERV_INTERFACE_H
