#include <unistd.h>
#include <sys/ioctl.h>

#include <fcntl.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include "servInterface.h"
#include "../servplot/servplot.h"

#define I2C_FILE_NAME "/dev/i2c-0"

#define HDMI_I2C_ADDR 0x50

#define LOG_ERR()   {qDebug() << "Read I2C error";return;}

struct SEdidExtInfo
{
    unsigned char u8Header;
    unsigned char u8Version;
    unsigned char u8DetailTimingOffset;
    unsigned char u8ExtInfo;
    unsigned char u8DataBlock[123];
    unsigned char u8Checksum;
};
/******************************************************************************
 * pthread selfcheck                                                              *
 ******************************************************************************/
EDIDThread::EDIDThread(QObject *parent) : QThread()
{
    m_pUtility = dynamic_cast<servInterface*>( parent );
    Q_ASSERT( m_pUtility != NULL );
}

EDIDThread::~EDIDThread()
{
    foreach(stEDID_TIMES *p, m_pUtility->m_stEDID.establish_times)
    {
        delete p;
    }
}

void EDIDThread::run()
{

    char buf[256];
    int i2c_addr = HDMI_I2C_ADDR;
    int ext_count = 0;

    SEdidExtInfo* pExt = (SEdidExtInfo*)&EDID[0x80];


    memset(EDID, 0, sizeof(EDID));

    i2c_file = open(I2C_FILE_NAME, O_RDWR);
    if (i2c_file >= 0)
    {
        i2c_addr = HDMI_I2C_ADDR;
    }
    else
    {
        qDebug() << "Can't open the i2c device";
        return;
    }

    if( i2cRead(i2c_addr,0x0, (unsigned char*)buf, 4) != 0  )
    {
        LOG_ERR();
    }

    if( i2cRead(i2c_addr,0x0,  (unsigned char*)&EDID[0],128) != 0 )
    {
        LOG_ERR();
    }

    ext_count = EDID[0x7E];

    if(ext_count)
    {
        if( ext_count == 3)
        {
            if( setSegmentPointer(0x30,0x01,0x00, (unsigned char*)buf,128) != 0 )
            {
                LOG_ERR();
            }

            if( setSegmentPointer(0x30,0x01,0x80, (unsigned char*)buf,128) != 0 )
            {
                LOG_ERR();
            }

            if( setSegmentPointer(0x30,0x0,0x00, (unsigned char*)&EDID[0],128) != 0 )
            {
                LOG_ERR();
            }
            if( setSegmentPointer(0x30,0x0,0x80, (unsigned char*)&EDID[0x80],128) != 0 )
            {
                LOG_ERR();
            }
        }
        else
        {
            if( i2cRead(i2c_addr,0x80, (unsigned char*)&EDID[0x80],128) != 0 )
            {
                LOG_ERR();
            }
        }
    }
    else
    {
        if( i2cRead(i2c_addr,0x0, (unsigned char*)&EDID[0],128) != 0 )
        {
            LOG_ERR();
        }
    }

    m_pUtility->m_stEDID.ExtensionFlag = EDID[0x7E];
    m_pUtility->m_stEDID.CheckSum      = EDID[0X7F];
    //ext_count = m_pUtility->m_stEDID.ExtensionFlag;

     qDebug() << "Input define:" << QString().setNum(EDID[0x14],16) << QString().setNum(i2c_addr,16);
    //PARSE EDID
    //
    checkVPID();
    checkVersion();
    checkBasicParam();
    checkColor();
    checkTimes();

    if( ext_count > 0)
    {
        checkExtBasic();
    }

    int sig_type = 1;

    if(ext_count == 0 ||
        pExt->u8DetailTimingOffset == 0x04 ||
        pExt->u8DetailTimingOffset == 0x00)
    {
       sig_type = 0;
       ext_count = 0;
    }
    else
    {
        int m_s32HasDviVSDB = 0;
        int m_s32HasHdmiVSDB = 0;
        for(int s32Num = 0;s32Num < pExt->u8DetailTimingOffset - 4;s32Num++)
        {
            if((pExt->u8DataBlock[s32Num] & 0xE0) == 0x60)
            {
                int len = pExt->u8DataBlock[s32Num] & 0x1F;
                if(len == 0x0)
                {
                    m_s32HasDviVSDB++;
                }
                else
                {
                    if(pExt->u8DataBlock[s32Num+1] == 0x03 &&
                       pExt->u8DataBlock[s32Num+2] == 0x0C &&
                       pExt->u8DataBlock[s32Num+3] == 0x00)
                    {
                        m_s32HasHdmiVSDB++;

                    }
                    else
                    {
                        m_s32HasDviVSDB++;
                    }
                }
                s32Num += len;
            }
            else if((pExt->u8DataBlock[s32Num] & 0xE0) != 0x00)
            {
                s32Num += pExt->u8DataBlock[s32Num] & 0x1F;
            }
        }

        if(m_s32HasHdmiVSDB != 0)
        {
            sig_type = 1;
        }
        else if(m_s32HasDviVSDB != 0)
        {
            sig_type = 0;
        }
    }

    qDebug() << "Sig=" << sig_type;
    //1-hdmi, 0-dvi
    m_pUtility->setHMDISigType( sig_type );
    //m_pUtility->setHDMIEnable(true);

    if( checkResolution(1280, 720, 60) )
    {
        m_pUtility->setEnHDMI(true);
        serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                              MSG_APP_UTILITY_HDMI_RES,
                              HDMI_1280_720_60);
    }
    else if(checkResolution(720,480,60))
    {
        m_pUtility->setEnHDMI(true);
        serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                              MSG_APP_UTILITY_HDMI_RES,
                              HDMI_720_480_60);
    }
    else if(checkResolution(640,480,60))
    {
        m_pUtility->setEnHDMI(true);
        serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                              MSG_APP_UTILITY_HDMI_RES,
                              HDMI_640_480_60);
    }
    else
    {
        //m_pUtility->setHDMIEnable(false);
        //m_pUtility->setEnHDMI(false);
    }

    close(i2c_file);
}

bool EDIDThread::checkResolution(int x, int y, int hz)
{
    foreach(stEDID_TIMES *res, m_pUtility->m_stEDID.establish_times)
    {
        if( res->x == x && res->y == y && res->hz == hz)
        {
            return true;
        }
    }
    return false;
}

int EDIDThread::i2cWrite(unsigned short addr,
                         unsigned short reg,
                         unsigned char *val,
                         unsigned short len)
{
    unsigned char outbuf[ 2 + len ];
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[1];

    messages[0].addr  = addr;
    messages[0].flags = 0;
    messages[0].len   = len + 1;
    messages[0].buf   = outbuf;

    outbuf[0] = reg & 0xff;

    if( len > 0 )
    {
        memcpy( outbuf + 1, val, len );
    }

    packets.msgs  = messages;
    packets.nmsgs = 1;
    if(ioctl( i2c_file, I2C_RDWR, &packets) < 0)
    {
        return -1;
    }

    return len;
}
int EDIDThread::i2cRead(unsigned short addr,
                     unsigned short reg,
                     unsigned char *val,
                        unsigned short len)
{

    unsigned char outbuf[2];
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[2];

    /*
     * In order to read a register, we first do a "dummy write" by writing
     * 0 bytes to the register we want to read from.  This is similar to
     * the packet in set_i2c_register, except it's 1 byte rather than 2.
     */
    outbuf[0] = reg & 0xff;

    messages[0].addr  = addr;
    messages[0].flags = 0;
    messages[0].len   = 1;
    messages[0].buf   = outbuf;

    /* The data will get returned in this structure */
    messages[1].addr  = addr;
    messages[1].flags = I2C_M_RD/* | I2C_M_NOSTART*/;
    messages[1].len   = len;
    messages[1].buf   = val;

    /* Send the request to the kernel and get the result back */
    packets.msgs      = messages;
    packets.nmsgs     = 2;

    if(ioctl(i2c_file, I2C_RDWR, &packets) < 0)
    {
        perror("Unable to send data");
        return 1;
    }
    return 0;
}

int EDIDThread::setSegmentPointer(int s32Addr,
                                  unsigned char u8SegmentReg,
                                  unsigned char u8Reg,
                                  unsigned char *pu8Data,
                                  int s32Length)
{
    unsigned char u8Outbuf;
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[3];


    u8Outbuf = u8SegmentReg;
    messages[0].addr  = s32Addr;
    messages[0].flags = 0;
    messages[0].len   = sizeof(u8Outbuf);
    messages[0].buf   = &u8Outbuf;//reg

    messages[1].addr  = HDMI_I2C_ADDR;
    messages[1].flags = 0;
    messages[1].len   = sizeof(u8Reg);
    messages[1].buf   = &u8Reg;//data

    messages[2].addr  = HDMI_I2C_ADDR;
    messages[2].flags = I2C_M_RD;
    messages[2].len   = s32Length;
    messages[2].buf   = pu8Data;//data
    /* Send the request to the kernel and get the result back */
    packets.msgs      = messages;
    packets.nmsgs     = 3;
    if(ioctl(i2c_file, I2C_RDWR, &packets) < 0)
    {
        qDebug("[%s] i2c ioctl error\n",__func__);
        return -1;
    }
    return 0;
}
//offset:0x08-0x11
void EDIDThread::checkVPID()
{
     char a,b,c;
     a = (EDID[8] & 0x3C ) >> 2;
     b = ((EDID[8] & 0x3) << 3) | ((EDID[9]) >> 5) ;
     c = EDID[9] & 0x1F;

     a = 0x40 + a;
     b = 0x40 + b;
     c = 0x40 + c;

     m_pUtility->m_stEDID.mid = QString("%1%2%3").arg(a).arg(b).arg(c);


     unsigned int code = *(unsigned short*)&EDID[0xA];

     m_pUtility->m_stEDID.pid = QString("0x%1").arg(code,4,16);

     code = *(unsigned int*)&EDID[0xc];
     m_pUtility->m_stEDID.sid = QString("0x%1").arg(code, 8, 16);

     m_pUtility->m_stEDID.week = EDID[0x10];
     m_pUtility->m_stEDID.year = EDID[0x11] + 1990;

}

//Offset:0x12-0x13
void EDIDThread::checkVersion()
{
    int v1 = EDID[0x12];
    int v2 = EDID[0x13];
    m_pUtility->m_stEDID.version = QString("%1.%2").arg(v1).arg(v2);
}

//offset:0x14-0x18
void EDIDThread::checkBasicParam()
{
    m_pUtility->m_stEDID.video_input_def = EDID[0X14];
    m_pUtility->m_stEDID.max_hori        = EDID[0X15];
    m_pUtility->m_stEDID.max_vert        = EDID[0X16];
    m_pUtility->m_stEDID.gamma           = EDID[0X17];
    m_pUtility->m_stEDID.power_feature   = EDID[0X18];
}

//offset:0x19-0x22
void EDIDThread::checkColor()
{
    m_pUtility->m_stEDID.red_green_bits = EDID[0x19];
    m_pUtility->m_stEDID.blue_white_bits = EDID[0x1a];
    m_pUtility->m_stEDID.red_x = EDID[0x1b];
    m_pUtility->m_stEDID.red_y = EDID[0x1c];
    m_pUtility->m_stEDID.green_x = EDID[0x1d];
    m_pUtility->m_stEDID.green_y = EDID[0x1e];
    m_pUtility->m_stEDID.blue_x = EDID[0x1f];
    m_pUtility->m_stEDID.blue_y = EDID[0x20];
    m_pUtility->m_stEDID.white_x = EDID[0x21];
    m_pUtility->m_stEDID.white_y = EDID[0x22];
}

//OFFSET:0X23-0X25
void EDIDThread::checkTimes()
{
    foreach(stEDID_TIMES *p, m_pUtility->m_stEDID.establish_times)
    {
        delete p;
    }
    m_pUtility->m_stEDID.establish_times.clear();

//    7  720 x 400 @ 70Hz  IBM,  VGA
//    6  720 x 400 @ 88Hz  IBM,  XGA2
//    5  640 x 480 @ 60Hz  IBM,  VGA
//    4  640 x 480 @ 67Hz  Apple, Mac II
//    3  640 x 480 @ 72Hz  VESA
//    2  640 x 480 @ 75Hz  VESA
//    1  800 x 600 @ 56Hz  VESA
//    0  800 x 600 @ 60Hz  VESA
    int times = EDID[0x23];
//    if(times & 0x80)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(720,400,70));
//    }
//    if(times & 0x40)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(720,400,88));
//    }
    if(times & 0x20)
    {
        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(640,480,60));
    }
//    if(times & 0x10)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(640,480,67));
//    }
//    if(times & 0x8)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(640,480,72));
//    }
//    if(times & 0x4)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(640,480,75));
//    }
//    if(times & 0x2)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(800,600,56));
//    }
//    if(times & 0x1)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(800,600,60));
//    }

//    7  800 x 600 @ 72Hz  VESA
//    6  800 x 600 @ 75Hz  VESA
//    5  832 x 624 @ 75H z  Apple, Mac II
//    4  1024  x 768 @ 87Hz    (I)     IBM
//    3  1024 x 768 @ 60Hz  VESA
//    2  1024 x 768 @ 70Hz  VESA
//    1  1024 x 768 @ 75Hz  VESA
//    0  1280 x 1024 @ 75Hz  VES
//    times = EDID[0x24];
//    if(times & 0x80)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(800,600,72));
//    }
//    if(times & 0x40)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(800,600,75));
//    }
//    if(times & 0x20)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(832,624,75));
//    }
//    if(times & 0x10)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(1024,768,87));
//    }
//    if(times & 0x8)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(1024,768,60));
//    }
//    if(times & 0x4)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(1024,768,70));
//    }
//    if(times & 0x2)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(1024,768,75));
//    }
//    if(times & 0x1)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(1280,1024,75));
//    }

//    times = EDID[0x25];
//    if(times & 0x80)
//    {
//        m_pUtility->m_stEDID.establish_times.append( new stEDID_TIMES(1152,870,75));
//    }


    genStandardTimes(0x26);
    genStandardTimes(0x28);
    genStandardTimes(0x2a);
    genStandardTimes(0x2c);
    genStandardTimes(0x2e);
    genStandardTimes(0x30);
    genStandardTimes(0x32);
    genStandardTimes(0x34);
}

void EDIDThread::genStandardTimes(int offset)
{

    int a = EDID[offset];
    if ( a == 1 )
    {
        return ;
    }
    int x = (a + 31) * 8;
    int b = EDID[offset+1];
    int ratio = (b & 0xc0) >> 6;
    int hz = (b & 0x1f) + 60;
    int y = x;
    if( ratio == 1)
    {
        y = x * 3 / 4;
    }
    else if(ratio == 2)
    {
        y = x * 4 / 5;
    }
    else if(ratio == 3)
    {
        y = x * 9 / 16;
    }

    //qDebug() << x << y << hz;

    if(  (x == 640 && y== 480 && hz == 60) ||
         (x == 720 && y== 480 && hz == 60) ||
         (x == 1280 && y== 720 && hz == 60) )
    {
        m_pUtility->m_stEDID.establish_times.append(new stEDID_TIMES(x,y,hz));
    }
}

void EDIDThread::checkDTD()
{

}

void EDIDThread::checkExtBasic()
{
    int data = EDID[0x80  + 4];

    if( (( data >> 5 ) & 0x7) != 2 )
    {
        data = data & 0x1f;
        data = EDID[0X80 + 4 + data + 2];
    }
    else
    {
        int l = (data & 0x1f);
        int o = (0x80+4+1);
        for(int i=o; i<l+o; i++)
        {
            int r = (int)(EDID[i] & 0x7f);
            switch( r )
            {
                case 1: m_pUtility->m_stEDID.establish_times.append(new stEDID_TIMES(640,480,60)); break;

                case 3:
                case 2: m_pUtility->m_stEDID.establish_times.append(new stEDID_TIMES(720,480,60)); break;

                case 4: m_pUtility->m_stEDID.establish_times.append(new stEDID_TIMES(1280,720,60)); break;

            }
        }
    }

}

void EDIDThread::checkExtDTD()
{

}
