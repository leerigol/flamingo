#include "servInterface.h"
#include "servutility.h"

#include "../servgui/servgui.h"
#include "../servhelp/servhelp.h"
#include "../../servscpi/servscpidso.h"


extern servInterface  *g_pInterface;

/*!
 * \brief servInterface::setNetStatus
 * 此函数是响应，网络监控服务中发送的消息cmd_query_netStatus
 * \param state
 *  参数值为：‘LINK’或‘UNLINK’
 * \return
 */

DsoErr servInterface::setNetStatus(int state)
{
    if(m_nNetStatus != state)
    {
        if(state == servInterface::NET_STATUS_CONNECTED
#ifndef USE_CBB_LXI
           || state == servInterface::NET_STATUS_INIT
#endif
                )
        {
            if( m_nNetInterface == NET_IF_ETH)
            {
                servGui::showInfo( sysGetString(INF_LAN_CONNECTED,"LAN connected"));
            }
            else
            {
                servGui::showInfo( sysGetString(INF_WLAN_CONNECTED,"WLAN connected"));
            }

#ifndef USE_CBB_LXI
            async(servInterface::cmd_net_config, 0 );//get ip address
#endif
        }
        else if(state == servInterface::NET_STATUS_UNLINK)
        {
            if( m_nNetInterface == NET_IF_ETH)
            {
                servGui::showInfo( sysGetString(INF_LAN_DISCONNECTED,"LAN disconnected"));
            }
            else
            {
                servGui::showInfo( sysGetString(INF_WLAN_DISCONNECTED,"WLAN disconnected"));
            }
        }
        else if( state == servInterface::NET_STATUS_INIT)
        {
            servGui::showInfo( "Network init");
        }
        else if( state == servInterface::NET_STATUS_DHCP_FAILED)
        {
            servGui::showInfo( "DHCP Config Failed");
        }
        else if( state == servInterface::NET_STATUS_INVALID_IP)
        {
            servGui::showInfo( "Invalid IP");
        }
        else if( state == servInterface::NET_STATUS_IPCONFLICT)
        {
            servGui::showInfo( "IP Conflict");
        }
        else if( state == servInterface::NET_STATUS_IP_LOSE)
        {
            servGui::showInfo( "IP lost");
        }
        m_nNetStatus = state;

        if( mUiAttr.getItem(MSG_APP_UTILITY_LXI_APPLY)->getEnable() == false )
        {
            mUiAttr.setEnable(MSG_APP_UTILITY_LXI_APPLY,true);
            //mUiAttr.setEnable(MSG_APP_UTILITY_LXI_INIT, true);
        }

        if( state == servInterface::NET_STATUS_CONFIGURED )
        {
            //upgrade online only if ip configed
            post(E_SERVICE_ID_HELP,
                 servHelp::cmd_lan_status,true);

            #ifdef USE_CBB_LXI
                generateXML();
            #endif
        }
    }
    return ERR_NONE;
}

int servInterface::getNetStatus()
{
    return m_nNetStatus;
}


/*!
 * \brief servInterface::applyConfig
 * 函数响应菜单‘应用’消息，App收到此消息后会将UI中网络参数发送回server
 * \return
 */
DsoErr servInterface::applyConfig()
{
    if(getNetStatus() > servInterface::NET_STATUS_UNLINK)
    {
        setuiEnable(false);
        //mUiAttr.setEnable(MSG_APP_UTILITY_LXI_INIT, false);

        m_nNetStatus = NET_STATUS_INIT;
        async(servInterface::cmd_net_config, 0 );//get ip address
    }
    else
    {
        return ERR_LAN_NOLINK;
    }
    return ERR_NONE;
}


/*!
 * \brief servInterface::initConfig
 * DEFAULT,DHCP
 * \param init
 * \return
 */
DsoErr servInterface::initConfig()
{
#if 0
    servGui::showMsgBox( getName(),
                         "Init the LAN?",
                         servInterface::cmd_net_rst );
#endif
    return ERR_NONE;
}

int servInterface::doDefault()
{
#if 0
    m_nIPMode    = IP_ACQUIRE_DHCP | IP_ACQUIRE_AUTO;
    m_nNetMode   = IP_ACQUIRE_DHCP;
    setIPMode(m_nIPMode);

    //setmDNS(false);

    //same as apply
    mUiAttr.setEnable(MSG_APP_UTILITY_LXI_APPLY, false);
    //mUiAttr.setEnable(MSG_APP_UTILITY_LXI_INIT, false);
    m_nNetStatus = NET_STATUS_INIT;
    async(servInterface::cmd_net_config, 1);//get ip address

    updateAllUi();
#endif
    return ERR_NONE;
}

int servInterface::initLxi()
{
#ifdef USE_CBB_LXI

    //start lan driver
    {
        //ifconfig eth0 hw ether $MAC_ADDR
        //ifconfig lo   127.0.0.1
        //ifconfig eth0 up
        //qDebug() << "mac:" << m_strMAC;
        char strBuf[64];
        memset(strBuf, 0, sizeof(strBuf));
        sprintf(strBuf, "/sbin/ifconfig eth0 hw ether %s >/dev/null 2>&1", m_strMAC.toLatin1().data());
        system(strBuf);

        memset(strBuf, 0, sizeof(strBuf));
        strcpy(strBuf, "/sbin/ifconfig lo 127.0.0.1");
        system(strBuf);

        memset(strBuf, 0, sizeof(strBuf));
        strcpy(strBuf, "/sbin/ifconfig eth0 up");
        system(strBuf);
    }

    extern int startLxi();
    startLxi();

    post(E_SERVICE_ID_SCPI, dsoServScpi::cmd_start_vxi,0);
#endif
    return ERR_NONE;
}

int servInterface::netConfig(int reset)
{
#ifndef _SIMULATE
    #ifdef USE_CBB_LXI
        extern int configLxi(int reset);
        configLxi(reset);
    #else
    if(m_pLanConfig == NULL)
    {
        m_pLanConfig = new LanConfigThread( this );
        Q_ASSERT( NULL != m_pLanConfig );
    }

    if(m_pLanConfig && !m_pLanConfig->isRunning())
    {
        m_pLanConfig->start();
    }

    reset=reset;
    #endif
#endif
    return ERR_NONE;
}


/*!
 * \brief servInterface::setIPMode
 *  函数响应网络模式菜单消息
 * \param cmd
 *  参数：0x1 - DHCP，0x2 - Auto， 0x4 - Static
 * \return
 */
DsoErr servInterface::setIPMode(int cmd)
{
    //qDebug()<<"serv setIPMode()"<<cmd;
    if(cmd == 0)
    {
        return ERR_INVALID_CONFIG;
    }
    if(cmd & IP_ACQUIRE_DHCP || cmd & IP_ACQUIRE_AUTO)
    {
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_IPADDRESS, false);
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_SUBNET,    false);
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_GATEWAY,   false);
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_DNS,       false);
        #ifndef USE_CBB_LXI
        mUiAttr.setEnable(MSG_APP_UTILITY_LXI_APPLY, true);
        //mUiAttr.setEnable(MSG_APP_UTILITY_LXI_INIT, true);
        #endif
    }
//    else if(cmd & IP_ACQUIRE_AUTO)
//    {
//        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_IPADDRESS, false);
//        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_SUBNET,    false);
//        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_GATEWAY,   true);
//        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_DNS,       true);
//    }
    else
    {
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_IPADDRESS, true);
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_SUBNET,    true);
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_GATEWAY,   true);
        mUiAttr.setVisible(MSG_APP_UTILITY_LXI_DNS,       true);
    }

    m_nIPMode = cmd;
    return ERR_NONE;
}

int servInterface::getIPMode()
{
    return m_nIPMode;
}

int servInterface::setNetMode(int a)
{
    mUiAttr.setEnable(MSG_APP_UTILITY_LXI_APPLY, true);
    //mUiAttr.setEnable(MSG_APP_UTILITY_LXI_INIT, true);
    m_nNetMode = a;
    return ERR_NONE;
}

int servInterface::getNetMode()
{
    return m_nNetMode;
}

/*!
 * \brief servInterface::setmDNS
 * 函数响应‘mDns’菜单，开启还是关闭
 * \param b
 * \return
 */
DsoErr servInterface::setmDNS(bool b)
{
    m_bmDNS = b;
//    mdnsThread.setEnable(m_bmDNS);

//    if( m_bmDNS && (!mdnsThread.isRunning()))
//    {
//        mdnsThread.start();
//    }
    async(servInterface::cmd_net_config, 0 );
    return ERR_NONE;
}

void servInterface::syncmDNS(bool b)
{
    m_bmDNS = b;
    //setuiChange( MSG_APP_UTILITY_LXI_MDNS );
}

bool servInterface::getmDNS()
{
    return m_bmDNS;
}

DsoErr servInterface::setHostSettingName(QString name)
{
    HostSettingName = name;
    return ERR_NONE;
}

QString servInterface::getHostSettingName()
{
    return HostSettingName;
}

DsoErr servInterface::setHostDeclareName(QString name)
{
    HostDeclareName = name;
    return ERR_NONE;
}

QString servInterface::getHostDeclareName()
{
    return HostDeclareName;
}

DsoErr servInterface::setServiceSettingName(QString name)
{
    ServiceSettingName = name;
    return ERR_NONE;
}

QString servInterface::getServiceSettingName()
{
    return  ServiceSettingName;
}

DsoErr servInterface::setServiceDeclareName(QString name)
{
    ServiceDeclareName = name;
    return ERR_NONE;
}

QString servInterface::getServiceDeclareName()
{
    return  ServiceDeclareName;
}
/*!
 * \brief servInterface::inputIP
 * 函数是为了app中设置IP地址时对准光标焦点
 * \return
 */
DsoErr servInterface::inputIP()
{
    return ERR_NONE;
}


/*!
 * \brief servInterface::inputNetmask
 * 函数是为了app中设置子网掩码时对准光标焦点
 * \return
 */
DsoErr servInterface::inputNetmask()
{
    return ERR_NONE;
}


/*!
 * \brief servInterface::inputGatway
 * 函数是为了app中设置网关时对准光标焦点
 * \return
 */
DsoErr servInterface::inputGatway()
{
    return ERR_NONE;
}


/*!
 * \brief servInterface::inputDNS
 * 函数是为了app中设置DNS时对准光标焦点
 * \return
 */
DsoErr servInterface::inputDNS()
{
    return ERR_NONE;
}


/*!
 * \brief servInterface::showForm
 * 函数响应进入子菜单消息CMD_SERVICE_SUB_ENTER
 * \param sig
 *
 * \return
 */
DsoErr servInterface::showForm(int sig)
{
    if(MSG_APP_UTILITY_LXI_MENU == sig)
    {
        m_bShowForm = true;
    }

    return ERR_NONE;
}

DsoErr servInterface::onFormClose()
{
    m_bShowForm = false;
    //async(CMD_SERVICE_ACTIVE , 1);
    return ERR_NONE;
}

bool servInterface::isShowForm()
{
    return m_bShowForm;
}

//!SCPI get net information
DsoErr servInterface::setDHCP(bool dhcp)
{
    if(dhcp)
    {
        m_nIPMode |= IP_ACQUIRE_DHCP;
    }
    else
    {
        if(m_nIPMode == IP_ACQUIRE_DHCP)
        {
           return ERR_INVALID_CONFIG;//MUST be one choice
        }
        m_nIPMode &= ~IP_ACQUIRE_DHCP;
    }
    //async(MSG_APP_UTILITY_LXI_CFG, m_nIPMode);
    return ERR_NONE;
}

bool servInterface::getDHCP()
{
    return (m_nIPMode & IP_ACQUIRE_DHCP);
}

DsoErr servInterface::setAUTOip(bool Auto)
{
    if(Auto)
    {
        m_nIPMode |= IP_ACQUIRE_AUTO;
    }
    else
    {
        if(m_nIPMode == IP_ACQUIRE_AUTO)
        {
           return ERR_INVALID_CONFIG;//MUST be one choice
        }
        m_nIPMode &= ~IP_ACQUIRE_AUTO;
    }
    //async(MSG_APP_UTILITY_LXI_CFG, m_nIPMode);
    return ERR_NONE;
}
bool servInterface::getAUTOip()
{
    return (m_nIPMode & IP_ACQUIRE_AUTO);
}

DsoErr servInterface::setStatic(bool stc)
{
    if(stc)
    {
        m_nIPMode |= IP_ACQUIRE_STATIC;
    }
    else
    {
        if(m_nIPMode == IP_ACQUIRE_STATIC)
        {
           return ERR_INVALID_CONFIG;//MUST be one choice
        }
        m_nIPMode &= ~IP_ACQUIRE_STATIC;
    }
    //async(MSG_APP_UTILITY_LXI_CFG, m_nIPMode);
    return ERR_NONE;
}

bool servInterface::getStatic()
{
    return (m_nIPMode & IP_ACQUIRE_STATIC);
}

DsoErr servInterface::setIP(QString ip)
{
    m_strIP = ip;
    return ERR_NONE;
}

DsoErr servInterface::setIPAddr(int ip)
{
    m_strIP = toStringIP(ip);
    return ERR_NONE;
}
int servInterface::getIPAddr()
{
    return toDigitIP(m_strIP);
}
QString servInterface::getIP()
{
    return m_strIP;
}


DsoErr servInterface::setDHCPServer(QString DHCP)
{
    m_strDHCP = DHCP;
    return ERR_NONE;
}

DsoErr servInterface::setDHCPAddr(int ip)
{
    m_strDHCP = toStringIP(ip);
    return ERR_NONE;
}

int servInterface::getDHCPAddr()
{
    return toDigitIP(m_strDHCP);
}

QString servInterface::getDHCPServer()
{
    return m_strDHCP;
}

DsoErr servInterface::setMask(QString mask)
{
    m_strMask = mask;
    return ERR_NONE;
}

DsoErr servInterface::setMaskAddr(int ip)
{
    m_strMask = toStringIP(ip);
    return ERR_NONE;
}

int servInterface::getMaskAddr()
{
    return toDigitIP(m_strMask);
}

QString servInterface::getMask()
{
    return m_strMask;
}

DsoErr servInterface::setGateway(QString gateway)
{
    m_strGateway = gateway;
    return ERR_NONE;
}

DsoErr servInterface::setGatewayAddr(int ip)
{
    m_strGateway = toStringIP(ip);
    return ERR_NONE;
}

int servInterface::getGatewayAddr()
{
    return toDigitIP(m_strGateway);
}

QString servInterface::getGateway()
{
    return m_strGateway;
}

void servInterface::setGateWayEn(bool b)
{
    mUiAttr.setVisible(MSG_APP_UTILITY_LXI_GATEWAY, b);
}

DsoErr servInterface::setDNS(QString dns)
{
    m_strDNS = dns;

    return ERR_NONE;
}
DsoErr servInterface::setDNSAddr(int ip)
{
    m_strDNS = toStringIP(ip);
    return ERR_NONE;
}

int servInterface::getDNSAddr()
{
    return toDigitIP(m_strDNS);
}

QString servInterface::getDNS()
{
    return m_strDNS;
}

void servInterface::setDNSEn(bool b)
{
    mUiAttr.setVisible(MSG_APP_UTILITY_LXI_DNS, b);
}

DsoErr servInterface::setMac( QString m)
{
    if( m.compare("00-00-00-00-00") == 0 )
    {
        qDebug() << "Warnning:Invalid mac.";
        return 0;
    }
    m_strMAC = m;
    return ERR_NONE;
}

QString servInterface::getMac()
{
    return m_strMAC;
}

void servInterface::getVisaAddr(CArgument type, CArgument argo)
{
    QString outStr;

    if(type.size() <= 0)
    {
        outStr = QString("TCPIP::%1::INSTR").arg(m_strIP);
        argo.setVal(outStr);
        return;
    }
    else
    {
        QString number  = "";

        switch(type[0].iVal)
        {
        case visa_type_usb:
        {
            query(serv_name_utility, servUtility::cmd_dso_serial, number );
            QString usbaddr = QString("%1").arg(SCPI_USB_VID,4,16,QLatin1Char('0'));

            outStr = QString("USB0::0x%1::0x%2::%3::INSTR").arg(usbaddr.toUpper())
                                                        .arg(SCPI_USB_PID,4,16,QLatin1Char('0'))
                                                        .arg(number.toUpper());
        }
            break;
        case visa_type_lxi:
            outStr = QString("TCPIP::%1::INSTR").arg(m_strIP);
            break;

        case visa_type_socket:
            outStr = QString("TCPIP0::%1::5555::INSTR").arg(m_strIP);
            break;

        default:
            outStr = QString("TCPIP::%1::INSTR").arg(m_strIP);
            break;
        }
        argo.setVal(outStr);
    }
}


QString servInterface::processCMD(const QString &cmd, QStringList &args)
{
    QString result = "";

    QProcess process;

    Q_ASSERT(process.state() == QProcess::NotRunning);

    process.start(cmd, args);

    process.waitForStarted();

    process.waitForFinished();//5s;

    result = process.readAllStandardOutput();


    //qDebug() << result;
    process.close();
    process.kill();

    return result;
}


void servInterface::generateXML()
{
    /*
     * %1 IP
       %2 Model
       %3 Serial
       %4 Version
       %5 Subnet
       %6 Mac
       %7 Gateway
    */


    QString xml_name = "/tmp/identification";
    QString xml_init = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
<LXIDevice xmlns=\"http://www.lxistandard.org/InstrumentIdentification/1.0\"\n\
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\
xsi:schemaLocation=\"http://www.lxistandard.org/InstrumentIdentification/1.0 \
http://%1/lxi/identification/LXIIdentification.xsd\">\n\
<Manufacturer>Rigol Technologies</Manufacturer>\n\
<Model>%2</Model>\n\
<SerialNumber>%3</SerialNumber>\n\
<FirmwareRevision>%4</FirmwareRevision>\n\
<ManufacturerDescription>Rigol</ManufacturerDescription>\n\
<HomepageURL>http://www.rigol.com</HomepageURL>\n\
<DriverURL>http://www.rigol.com</DriverURL>\n\
<UserDescription>RGLAN Identification Schema</UserDescription>\n\
<IdentificationURL>http://%1/lxi/identification</IdentificationURL>\n\
<Interface xsi:type=\"NetworkInformation\" InterfaceType=\"LXI\" IPType=\"IPv4\" InterfaceName=\"eth0\">\n\
<InstrumentAddressString>TCPIP0::%1::INSTR</InstrumentAddressString>\n\
<Hostname>%8</Hostname>\n\
<IPAddress>%1</IPAddress>\n\
<SubnetMask>%5</SubnetMask>\n\
<MACAddress>%6</MACAddress>\n\
<Gateway>%7</Gateway>\n\
<DHCPEnabled>%9</DHCPEnabled>\n\
<AutoIPEnabled>%10</AutoIPEnabled>\n\
</Interface>\n\
<Interface InterfaceType=\"Rigol Custom Network Interface\" InterfaceName=\"LAN\">\n\
<InstrumentAddressString>%1</InstrumentAddressString>\n\
</Interface>\n\
<IVISoftwareModuleName>RGLAN</IVISoftwareModuleName><Domain>1</Domain><LXIVersion>1.4</LXIVersion></LXIDevice>";

    QString ip        = g_pInterface->getIP();
    QString hostName  = g_pInterface->getHostSettingName();
    QString subnet    = g_pInterface->getMask();
    QString mac       = g_pInterface->getMac();
    QString gateway   = g_pInterface->getGateway();

    QString model =  servUtility::getSystemModel();
    QString version= servUtility::getSystemVersion();
    QString serial = servUtility::getSystemSerial();
    QString dhcp = g_pInterface->getDHCP() ? "true":"false";
    QString autoip = g_pInterface->getAUTOip() ? "true" : "false";

    QString xml_cont = xml_init.arg( ip )
                               .arg(model)
                               .arg(serial)
                               .arg(version)
                               .arg(subnet)
                               .arg(mac)
                               .arg(gateway)
                               .arg(m_bmDNS? hostName : ip)
                               .arg(dhcp)
                               .arg(autoip);


   QFile file(xml_name);
   if( file.open(QIODevice::WriteOnly | QIODevice::Text) )
   {
       file.write(xml_cont.toLatin1().data(), xml_cont.toLatin1().size());
       file.close();;
   }
}


/**
在Linux下编写获取本机网卡地址的程序，
比较简单的方法是利用套接口（socket）和IO接口（ioctl）函数来获取网卡信息
socket函数的原型是：
int socket（int domain,int type, int protocol）;
本函数有以下3个输入参数：
  domain参数：表示所使用的协议族；
    type参数：表示套接口的类型；
protocol参数：表示所使用的协议族中某个特定的协议。如果函数调用成功，套接口的描述符
             （非负整数）就作为函数的返回值，假如返回值为-1，就表明有错误发生。

    利用socket函数来获取网卡MAC信息时，domain参数取值AF_INET，表示采用internet协议族；
type参数指定为SOCK_DGRAM，表示采用数据报类型套接口，protocol参数在这种组合下只有唯一选择，
故用0填充。I/O控制函数ioctl用于对文件进行底层控制，这里的文件包含网卡、终端、磁带机、套接口
等软硬件设施，实际的操作来自各个设备自己提供的ioctl接口。ioctl函数的原型如下：
int ioctl（int d,int request,…）
这里，参数d取值套接口的描述符，第一个request参数指定通过socket传输的I/O类型。
本实验可以取值 SIONGIFHWADDR（0x8927），表示取硬件地址。
其他取值及其含义详见/usr/includr/linux/sockios.h。其后的
request参数用于为实现I/O控制所必须传入或传出的参数。本实验需要用ifr结构传入网卡设备名，
并传出6B的MAC地址。关键的程序段如下：

    #include <netinet/if_ether.h>
    #include <net/if.h>
    char *device=”eth0”; //teh0是网卡设备名
    unsigned char macaddr[ETH_ALEN]; //ETH_ALEN（6）是MAC地址长度
    int s=socket（AF_INET,SOCK_DGRAM,0）; //建立套接口
    struct ifreq req;
    int err;
    strcpy（req.ifr_name,device）; //将设备名作为输入参数传入
    err=ioctl（s,SIOCGIFHWADDR,&req）; //执行取MAC地址操作
    close（s）;
    if（err!= -1）
    {
        memcpy（addr,req.ifr_hwaddr.sa_data,ETH_ALEN）; //取输出的MAC地址
        for（i=0;i<ETH_ALEN;i++）
            printf（”%3d：”,macaddr[i]）;
    }
 */
//#include <sys/types.h>
//#include <string.h>
//#include <stdlib.h>
//#include <sys/ioctl.h>
//#include <stdio.h>
//#include <error.h>
//#include <net/if.h>
//#include <unistd.h>
//#include <netinet/in.h>
//#include <arpa/inet.h>

//bool servInterface::ioctlNetParam(int type, QString &arg)
//{
//    struct ifreq ifr;

//    if(m_nSock < 0)
//    {
//        m_nSock = socket(AF_INET, SOCK_DGRAM, 0);
//        if(m_nSock < 0)
//        {
//            return false;
//        }
//    }

//    memset(&ifr, 0, sizeof(ifr));
//    strcpy(ifr.ifr_name,  NET_DEV);


//    if(type == net_r_mac)
//    {
//        unsigned char mac[6];
//        if( (ioctl( m_nSock, SIOCGIFHWADDR, &ifr)) < 0)
//        {
//            return false;
//        }
//        else
//        {
//            memcpy(mac, ifr.ifr_hwaddr.sa_data, sizeof(mac));
//            arg = QString("%1.%2.%3.%4.%5.%6").arg(mac[0],2,16)
//                                              .arg(mac[1],2,16)
//                                              .arg(mac[2],2,16)
//                                              .arg(mac[3],2,16)
//                                              .arg(mac[4],2,16)
//                                              .arg(mac[5],2,16);
//            return true;
//        }
//    }
//    else if(type == net_w_mac)
//    {

//    }
//    else if(type == net_r_ip)
//    {
//        struct sockaddr_in *sin;

//        if( (ioctl( m_nSock, SIOCGIFADDR, &ifr)) < 0)
//        {
//            return false;
//        }
//        else
//        {
//            char addr[64];
//            memset(addr, 0, sizeof(addr));
//            sin = (struct sockaddr_in *)&ifr.ifr_addr;
//            strcpy(addr,inet_ntoa(sin->sin_addr));
//            arg = QString( addr );
//        }
//    }
//    else if(type == net_r_mask)
//    {
//        struct sockaddr_in *sin;

//        if( (ioctl( m_nSock, SIOCGIFNETMASK, &ifr)) < 0)
//        {
//            return false;
//        }
//        else
//        {
//            char addr[64];
//            memset(addr, 0, sizeof(addr));
//            sin = (struct sockaddr_in *)&ifr.ifr_netmask;
//            strcpy(addr,inet_ntoa(sin->sin_addr));
//            arg = QString( addr );
//        }
//    }
//}
