#include <QDebug>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <error.h>

#include <unistd.h>

#include "../servstorage/servstorage.h"
#include "servlistener.h"


IMPLEMENT_CMD( serviceExecutor, servListener  )
start_of_entry()
on_set_int_void  (  CMD_SERVICE_STARTUP,    &servListener::startup),
on_set_void_void(   CMD_SERVICE_RST,        &servListener::rst),
end_of_entry()

servListener::servListener():serviceExecutor( QString("netlisten"),E_SERVICE_ID_NONE )
{
    serviceExecutor::baseInit();
}

servListener::~servListener()
{
}

DsoErr servListener::start()
{
#ifndef _SIMULATE
    #ifndef  USE_CBB_LXI
        m_pListener = new ListenerThread(this);
        Q_ASSERT(m_pListener);
        m_pListener->start();
    #endif
#endif
    return ERR_NONE;
}

void servListener::rst()
{
    m_bEth0State = false;
    m_bWifiState = false;
}

int servListener::startup()
{
    return ERR_NONE;
}


ListenerThread::ListenerThread( QObject *parent ): QThread()
{
    m_pNetListen = dynamic_cast<servListener*>( parent );
}


/*!
 * \brief ListenerThread::run
 *  网络监控线程函数，此线程函数会一直运行，一直监控当前系统的LAN和WiFi的状态。
 */
void ListenerThread::run()
{
    QThread::sleep(10);

    QString disWLAN = "wpa_cli -iwlan0 -p/var/run/wpa.conf disc >/dev/null 2>&1";

    strcpy(m_stInterface.ifr_name,  NET_DEV);
    m_nSock = socket(AF_INET, SOCK_DGRAM, 0);
    if(m_nSock < 0)
    {
        qDebug() << "Ethernet listener error";
        return;
    }

    bool bEth0Status;
    bool bWifiStatus;

    while( true )
    {
        if( servStorage::isDefaulting() )
        {
            QThread::sleep(3);//sleep more for other service is ready
            continue;
        }

        bEth0Status = listenEth0();
        bWifiStatus = listenWifi();

        if(bEth0Status && bWifiStatus)
        {
            /*!
             *如果有线网络和无线网络同时可用，则优先使用有线网络
             */
            if(m_pNetListen->getEth0State() != bEth0Status)
            {
                serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                      servInterface::cmd_net_if,
                                      servInterface::NET_IF_ETH);

                //!更新有线网卡链接状态
                serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_CONNECTED);
                m_pNetListen->setEth0State( bEth0Status );


                //!关闭wifi，关闭wifi自动链接
                serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_UNBOUND);
            }
        }
        else if(bEth0Status)
        {
            /*!
             *只有有线网卡可用
             */
            if(m_pNetListen->getEth0State() != bEth0Status)
            {
                serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                      servInterface::cmd_net_if,
                                      servInterface::NET_IF_ETH );

                //!更新有线网卡链接状态
                serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_CONNECTED );
                m_pNetListen->setEth0State( bEth0Status );

                //!关闭wifi，关闭wifi自动链接
                serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_UNBOUND);
            }
        }
        else if(bWifiStatus)
        {
            /*!
             *只有无线网卡可用
             */
            if(m_pNetListen->getEth0State() != bEth0Status)
            {
                //!更新有线网卡链接状态
                serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_UNLINK);
                m_pNetListen->setEth0State( bEth0Status );
            }

            if(m_pNetListen->getWifiState() != bWifiStatus)
            {
                serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                      servInterface::cmd_net_if,
                                      servInterface::NET_IF_WLAN
                                       );

                //!打开wifi，同时开启无线网卡自动链接
                //!更新有线网卡链接状态
                serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_CONNECTED);
                m_pNetListen->setWifiState( bWifiStatus );
            }
        }
        else
        {
            serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                  servInterface::cmd_net_if,
                                  servInterface::NET_IF_ETH);
            /*!
             *有线网卡和无线网卡都不能使用，更新有线网卡链接状态
             */
            if(m_pNetListen->getEth0State() != bEth0Status)
            {
                serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_UNLINK);

                serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_UNLINK);

                m_pNetListen->setEth0State( bEth0Status );
            }

            if(m_pNetListen->getWifiState() != bWifiStatus)
            {
                serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_UNLINK);

                serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                      servInterface::cmd_net_status,
                                      servInterface::NET_STATUS_UNLINK);

                m_pNetListen->setWifiState( bWifiStatus );
            }
        }
        QThread::msleep(3000);
    }

}

/*!
 * \brief ListenerThread::listenEth0
 *  监控网口链接状态，如果网线已接入局域网，则bEth0Status为true,如果网线拔出，则bEth0Status为false。
 * \param skfd
 * \param ifr
 * \return
 */
bool ListenerThread::listenEth0()
{
    if(ioctl(m_nSock, SIOCGIFFLAGS, &m_stInterface) < 0 )
    {
         qDebug()<<__FILE__<<__LINE__<<"IO listener CLOSE";
         return false;
    }

    if(m_stInterface.ifr_flags & IFF_RUNNING)
    {
         return  true;
    }
    else
    {
         return false;
    }
}
/*!
 * \brief ListenerThread::listenWifi
 * 使用Linux系统命令检查系统当前网卡，
 * 如果检测到wlan0则表示wifi模块接入到系统，
 * 则bWifiStatus为true，否则为false；
 * \return
 */
bool ListenerThread::listenWifi()
{
      QString wlan0 = "/proc/net/wireless";
      QFile file(wlan0);
      if( file.exists() && file.open(QIODevice::ReadOnly))
      {
          QByteArray content = file.readAll();
          file.close();


          QString str(content);
          if( str.contains(WLAN_DEV))
          {
              //qDebug() << "WLAN module is exist";
              return true;
          }
      }
      return false;
}

