#ifndef EDID_H
#define EDID_H
#include <QString>

struct stEDID_TIMES
{
    stEDID_TIMES(int a, int b, int c)
    {
        x = a;
        y = b;
        hz = c;
    }

    int x;
    int y;
    int hz;
};

struct stEDID
{
    //Vendor / Product Identification
    QString mid;//Manufacture
    QString pid;//product code
    QString sid;//serial
    int     week;//week of manufacture
    int     year;//year of manufacture

    //EDID Structure Version / Revision
    QString version;

    //Basic Display Parameters / Features
    char    video_input_def;
    char    max_hori;//cm
    char    max_vert;
    char    gamma;
    int     power_feature;

    //Color Characteristics
    char    red_green_bits;
    char    blue_white_bits;
    char    red_x;
    char    red_y;
    char    green_x;
    char    green_y;
    char    blue_x;
    char    blue_y;
    char    white_x;
    char    white_y;

    //Established Timings
    QList<stEDID_TIMES*> establish_times;

    //Standard Timing Identification

    char ExtensionFlag;
    char CheckSum;

    //extension HDMI
    char hdmi_tag;
    char hdmi_rev;
    char ExtCheckSum;
};

#endif // EDID_H

