#include <unistd.h>
#include <QNetworkInterface>
#include "servcmd.h"

#include "servInterface.h"
#include "servutility.h"



#define DEBUG_OUT   (qDebug() << __FUNCTION__ << __LINE__;)
/******************************************************************************
 * Thread ioset                                                              *
 ******************************************************************************/

/*!
 * \brief LanConfigThread::checkConflict
 * 函数是对IP地是否冲突进行检查
 * \param ipstr
 * 参数：IP地址
 * \return true if conflict
 */
bool LanConfigThread::checkConflict(QString& ipstr)
{

    //!arping命令是对设定的IP地址是否在局域网内冲突进行检测，例如：arping 127.0.0.1 -w 2
    //! -w :设定一个超时时间，单位是秒。如果到了超时时间arping还没有收到响应则退出。
    QString cmd     = "/usr/bin/arping";
    QStringList arg;
    arg  << "-w" << "2" << ipstr;

    QString result = servInterface::processCMD(cmd, arg);
    QString ref = "Unicast reply";
    return result.contains(ref);
}

/*!
 * \brief LanConfigThread::runAUTO
 * 函数：如果基于服务器(DHCP)的方法失败，将通过autoip协议分配默认ip地址
 * ip地址的分配范围从169.254.1.0 到169.254.254.255（通常称为169.254/16），
 * 在该地址范围内的地址都是有效的
 * \param IP
 * \return
 */
bool LanConfigThread::runAUTO(QString& ip)
{

    bool    autoValidIP = false;
    QString autoIP;

    int ip3 = 112;
    int ip4 = 67;

    for(;ip3 <= 255;ip3++)
    {
        for(;ip4 <= 255;ip4++)
        {
            autoIP = QString("169.254.%1.%2").arg(ip3).arg(ip4);

            if( !checkConflict(autoIP) )
            {
                ip = autoIP;
                autoValidIP = true;
                break;
            }
        }

        if(autoValidIP)
        {
            break;
        }
    }
    return autoValidIP;
}


/*!
 * \brief LanConfigThread::runDHCP
 * 函数：进行DHCP操作，进行动态网络参数配置
 * \return
 */
bool LanConfigThread::runDHCP()
{
    QString result    = "";
    QString successStr="Sending select for";

    //!udhcpc  -t 3 -T 1 -n -q
    //! 是Linux中自动获取IP地址的V命令
    //!-t：发送请求次数
    //!-T：每个请求之间的间隔时间 s
    //!-n：如果动态获取IP地址失败则退出
    QString     cmd    = "/sbin/udhcpc";
    QStringList arg;
    arg << "-i" << m_pInterface->getInterfaceName();
    arg << "-t" << "3" << "-T" << "1" << "-n" << "-q";


    result = servInterface::processCMD(cmd, arg);

    if( result.size() > 0 && result.contains(successStr) )
    {
        readIPMask();
        readGateway();
        readDNS();
        return true;
    }
    else
    {
        return false;
    }
}

/*!
 * \brief LanConfigThread::readIPMask
 * 函数：通过调用QT函数获得当前系统的IP地址和子网掩码
 * \param ip
 * 参数：获取的IP地址
 * \param mask
 * 参数：获取的子网掩码
 */
void LanConfigThread::readIPMask()
{
    QString ifname(m_pInterface->getInterfaceName());
    QList<QNetworkInterface> interface = QNetworkInterface::allInterfaces();
    foreach(QNetworkInterface item, interface)
    {
        if(item.name().compare(ifname) == 0)
        {
            QList<QNetworkAddressEntry> entries = item.addressEntries();
            if(entries.size())
            {
                QString ip = entries.at(0).ip().toString();
                serviceExecutor::post(m_pInterface->getId(),
                                      servInterface::cmd_net_ip, ip);
                m_pInterface->setIP(ip);
                m_pInterface->setMask(entries.at(0).netmask().toString());
            }
        }
    }
}


/*!
 * \brief LanConfigThread::configIP
 * 函数：配置系统IP地址和子网掩码
 * \param ipaddr
 *  参数：IP地址
 * \param netmask
 *  参数：子网掩码
 * \return
 */
DsoErr LanConfigThread::configIP(QString& ipaddr,QString& netmask)
{

    if(!checkConflict(ipaddr))
    {
        //!设置系统IP地址和子网掩码，例如：ifconfig 127.0.0.1 netmask 255.255.255.0
        QString cmd = QString("ifconfig %1 %2 netmask %3").
                                        arg(NET_DEV).
                                        arg(ipaddr).
                                        arg(netmask);
        QProcess::execute(cmd);
    }
    else
    {
        return ERR_IP_CONFLICT;
    }

    return ERR_NONE;
}

/*!
 * \brief LanConfigThread::readGateway
 *
    !route -n ：查看当前系统路由状态
    !grep：文本搜索正则表达式，将包含搜索内容的行打印出来
    !awk：以空格为分隔符，对每行进行切片操作，
    ’{print $2}‘作为awk的参数，输出切片后的第一个切片内容
 * /sbin/route -n | grep eth0 | grep UG | awk '{print $2}'
 * 函数：获得当前系统网关地址
 * \param gateway
 * 参数：获取的网关地址
 */
void LanConfigThread::readGateway()
{
    char   gateway[32];
    char   line_buf[1024];
    char   iface[16];
    unsigned char tmp[100]={'\0'};
    unsigned int dest_addr=0;
    unsigned int gate_addr=0;
    unsigned int flag=0;
    char* ifname = m_pInterface->getInterfaceName();

    QFile file("/proc/net/route");

    if(file.open(QIODevice::ReadOnly))
    {
        file.readLine(line_buf, 1024);

        while( file.readLine(line_buf, 1024) )
        {
            int cnt = sscanf(line_buf,
                             "%s\t%X\t%X\t%X",
                             iface,
                             &dest_addr,
                             &gate_addr,
                             &flag);
            if( (cnt == 4) &&
                gate_addr != 0 &&
                flag == 0x3 &&
                (memcmp(ifname, iface, strlen(ifname)) == 0)  )
            {
                    memcpy(tmp, (unsigned char *)&gate_addr, 4);
                    sprintf(gateway, "%d.%d.%d.%d",
                            (unsigned char)*tmp,
                            (unsigned char)*(tmp+1),
                            (unsigned char)*(tmp+2),
                            (unsigned char)*(tmp+3));
                    m_pInterface->setGateway( QString(gateway) );
                    break;
            }
        }
        file.close();
    }
}

/*!
 * \brief LanConfigThread::configGateway
 * 函数：配置系统网关地址
 * \param gateway
 * 参数：网关
 * \return
 */
DsoErr LanConfigThread::configGateway(QString& gateway)
{
    //!设置系统网关地址
    QString cmd = "route add default gw " + gateway;
    QStringList arg;

    QProcess::execute(cmd);

    return ERR_NONE;
}

/*!
 * \brief LanConfigThread::readDNS
 * cat /etc/resolv.conf | awk '{print $2}' | awk 'NR == 2'
 * 函数：获取当前系统的DNS地址
 * \param dns
 * 参数：获取的DNS地址
 */
void LanConfigThread::readDNS()
{
    QFile   file("/etc/resolv.conf");
    QString prex = "nameserver";

    //read on DNS
    char  line_buf[1024];
    if(file.open(QIODevice::ReadOnly))
    {
        while( file.readLine(line_buf, 1024) > 0)
        {
            QString abc = QString(line_buf);
            if(abc.startsWith(prex))
            {
                if(abc.size() > prex.size())
                {
                    QString dns = abc.right(abc.size() - prex.size());
                    dns.remove("\n").remove(" ");
                    m_pInterface->setDNS(dns);
                }
            }
        }
        file.close();
    }

}



/*!
 * \brief LanConfigThread::configDNS
 * 函数：配置系统DNS地址
 * \param dns
 * 参数：DNS地址
 * \return
 */
DsoErr LanConfigThread::configDNS(QString& dns)
{
    QFile file("/etc/resolv.conf");
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out( &file );
        out << "nameserver " << dns;
        fsync(file.handle());
        file.close();
    }
    return ERR_NONE;
}

/*!
 * \brief LanConfigThread::lanConfig
 * 系统起来以后启动线程调用此函数对网络进行配置
 * \param modecfg
 * 参数：网络配置模式
 */
void LanConfigThread::lanConfig(int acqType)
{
    QString ip      = m_pInterface->getIP();
    QString mask    = m_pInterface->getMask();
    QString gateway = m_pInterface->getGateway();
    QString dns     = m_pInterface->getDNS();

    bool bAcquired = false;
    bool bDHCP     = false;
    int  nAcqMode  = servInterface::IP_ACQUIRE_DHCP;
    int  nNetStatus= servInterface::NET_STATUS_CONNECTED;

    if(acqType & servInterface::IP_ACQUIRE_DHCP)
    {
        bAcquired = runDHCP();
        bDHCP     = true;
        if(bAcquired)
        {
            nNetStatus=  servInterface::NET_STATUS_CONFIGURED;
        }
        else
        {
            nNetStatus=  servInterface::NET_STATUS_DHCP_FAILED;
        }
    }

    /* dhcp first.   and autoIP including dhcp */
    if( !bAcquired && (acqType & servInterface::IP_ACQUIRE_AUTO) )
    {
        if( !bDHCP )
        {
            bAcquired = runDHCP();
            if(bAcquired)
            {
                nNetStatus=  servInterface::NET_STATUS_CONFIGURED;
            }
            else
            {
                nNetStatus=  servInterface::NET_STATUS_DHCP_FAILED;
            }
        }

        if ( !bAcquired ) //if dhcp failed ,then try auto
        {
            nAcqMode   = servInterface::IP_ACQUIRE_AUTO;
            if( runAUTO(ip) )
            {
                mask    = "255.255.0.0";
                gateway = "0.0.0.0";

                //m_pInterface->setIP(ip);
                serviceExecutor::post(m_pInterface->getId(),
                                      servInterface::cmd_net_ip, ip);

                m_pInterface->setMask(mask);
                m_pInterface->setGateway( gateway );
                m_pInterface->setDNS(dns);

                configIP(ip,mask);
                configGateway(gateway);
                configDNS(dns);
                nNetStatus = servInterface::NET_STATUS_CONFIGURED;
            }
            else
            {
                nNetStatus = servInterface::NET_STATUS_INVALID_IP;
            }
        }
    }

    if( !bAcquired && (acqType & servInterface::IP_ACQUIRE_STATIC) )
    {
        nAcqMode   = servInterface::IP_ACQUIRE_STATIC;
        if( !checkConflict(ip) )
        {
            configIP(ip,mask);
            configGateway(gateway);
            configDNS(dns);

            nNetStatus = servInterface::NET_STATUS_CONFIGURED;            
        }
        else
        {
            nNetStatus = servInterface::NET_STATUS_IPCONFLICT;
        }
    }

    readMAC();

    serviceExecutor::post(m_pInterface->getId(),
                          servInterface::cmd_net_status, nNetStatus);

    serviceExecutor::post(m_pInterface->getId(),
                          servInterface::cmd_net_mode, nAcqMode);

    if(nAcqMode != servInterface::IP_ACQUIRE_DHCP)
    {
        m_pInterface->setDNSEn(true);
        m_pInterface->setGateWayEn(true);
    }
    else
    {
        m_pInterface->setDNSEn(false);
        m_pInterface->setGateWayEn(false);
    }
//    qDebug() << m_pInterface->getIP() <<
//                m_pInterface->getGateway() <<
//                m_pInterface->getDNS() <<
//                m_pInterface->getMask();
}

void LanConfigThread::wlanConfig()
{
    int  nAcqMode  = servInterface::IP_ACQUIRE_DHCP;
    int  nNetStatus= servInterface::NET_STATUS_CONNECTED;

    if( runDHCP() )
    {
        nNetStatus=  servInterface::NET_STATUS_CONFIGURED;
        readMAC();
    }
    else
    {
        nNetStatus=  servInterface::NET_STATUS_DHCP_FAILED;
    }

    serviceExecutor::post(m_pInterface->getId(),
                          servInterface::cmd_net_status, nNetStatus);

    serviceExecutor::post(m_pInterface->getId(),
                          servInterface::cmd_net_mode, nAcqMode);
}

LanConfigThread::LanConfigThread(QObject *parent) : QThread(parent)
{
    m_pInterface = dynamic_cast<servInterface*>( parent );
    Q_ASSERT( m_pInterface != NULL );
}


void LanConfigThread::run()
{    
    int netStatus = m_pInterface->getNetStatus();

//    qDebug() <<  netStatus << m_pInterface->getNetInterface();
    if( netStatus == servInterface::NET_STATUS_CONNECTED ||
        netStatus == servInterface::NET_STATUS_INIT)
    {
        if( m_pInterface->getNetInterface() == servInterface::NET_IF_WLAN )
        {
            wlanConfig();
        }
        else
        {
            lanConfig( m_pInterface->getIPMode() );
        }
#ifndef USE_CBB_LXI
        m_pInterface->generateXML();
#endif
    }
}


#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <arpa/inet.h>

QString LanConfigThread::readDefaultMAC()
{
    unsigned char mac[6];
    struct ifreq ifr;

    int nSock = socket(AF_INET, SOCK_DGRAM, 0);
    if(nSock < 0)
    {
        return "";
    }

    memset(&ifr, 0, sizeof(ifr));
    strcpy(ifr.ifr_name,  NET_DEV);
    if( (ioctl( nSock, SIOCGIFHWADDR, &ifr)) < 0)
    {
        qDebug() << "Read mac error" << __FUNCTION__ << __LINE__;
        close(nSock);
        return "";
    }
    else
    {
        memcpy(mac, ifr.ifr_hwaddr.sa_data, sizeof(mac));
        QString arg = QString("%1-%2-%3-%4-%5-%6").arg( (int)mac[0], 2, 16, QLatin1Char('0'))
                                                  .arg( (int)mac[1], 2, 16, QLatin1Char('0'))
                                                  .arg( (int)mac[2], 2, 16, QLatin1Char('0'))
                                                  .arg( (int)mac[3], 2, 16, QLatin1Char('0'))
                                                  .arg( (int)mac[4], 2, 16, QLatin1Char('0'))
                                                  .arg( (int)mac[5], 2, 16, QLatin1Char('0'));
        arg = arg.toUpper();
        return arg;
    }
    close(nSock);
}

void LanConfigThread::readMAC()
{
    QString mac = readDefaultMAC();
    if( mac.size() > 0)
    {
        //m_pInterface->setMac(mac);
    }
}
