#ifndef _SERVCMD_H
#define _SERVCMD_H
#include <QObject>

class servCMD : public QObject
{

public:
    /* this will block the current thread */

    static     int systemCMD(const QString& cmd, QString &result);
};

#endif // SERVCMD_H

