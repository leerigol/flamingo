#ifndef SERV_LISTENER_H
#define SERV_LISTENER_H

#include <net/if.h>

#include <QThread>

#include "../../service_name.h"
#include "../../service_msg.h"
#include "../../service.h"
#include "../../servutility/servinterface/servInterface.h"
#include "../../servutility/servinterface/servwifi.h"

class servListener : public serviceExecutor,ISerial
{
    Q_OBJECT
protected:
    DECLARE_CMD()
public:
    servListener();
    ~servListener();

    DsoErr   start();
    void     rst();
    int      startup();

    bool     getEth0State()       { return m_bEth0State; }
    void     setEth0State(bool b) { m_bEth0State = b;    }

    bool     getWifiState()       { return m_bWifiState; }
    void     setWifiState(bool b) { m_bWifiState = b;    }

private:
    QThread* m_pListener;

    bool     m_bEth0State;
    bool     m_bWifiState;
};

class ListenerThread:public QThread
{
    Q_OBJECT
public:


public:
    ListenerThread(QObject *parent);
    servListener* m_pNetListen;

protected:
    void run();
private:
    bool listenEth0( void );
    bool listenWifi( void );

    int          m_nSock;
    struct ifreq m_stInterface;
};
#endif // servListener_H

