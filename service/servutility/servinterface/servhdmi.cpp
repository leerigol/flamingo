#include "../servplot/servplot.h"

#include "servInterface.h"

DsoErr servInterface::onHDMI(bool b)
{
#if 0
    if( b && m_pEDIDThead == NULL)
    {
        m_pEDIDThead = new EDIDThread(this);
        m_pEDIDThead->start();
        //new process
    }
    else if(!b && m_pEDIDThead != NULL)
    {
        m_pEDIDThead->deleteLater();
        m_pEDIDThead = NULL;
        //stop process
        m_bEDID = false;
        //async(MSG_APP_UTILITY_HDMI_EDID, 0);
        setHDMIEnable(false);
        //setEnHDMI(false);
    }
#endif
    if( b )
    {
        setEnHDMI(b);
    }
    setHDMIEnable(b);

    //setuiChange(MSG_APP_UTILITY_HDMI);
    //setuiChange(MSG_APP_UTILITY_HDMI_RES);

    return ERR_NONE;
}

DsoErr servInterface::setEnHDMI(bool b)
{
    // xxxx
    if(b != m_bHDMI)
    {
        int val = b;
        val = (val << 16)  | m_nHDMI;
        post(E_SERVICE_ID_PLOT,
             servPlot::cmd_config_hdmi, val);
    }
    m_bHDMI = b;

    //mUiAttr.setEnable( MSG_APP_UTILITY_HDMI_RES, b);
    return ERR_NONE;
}


bool servInterface::getEnHDMI()
{
    return m_bHDMI;
}


DsoErr servInterface::setDefHDMI(int h)
{
    if( h != m_nHDMI )
    {
        int val = m_bHDMI;
        val = (val << 16)  | h;
        post(E_SERVICE_ID_PLOT,
             servPlot::cmd_config_hdmi, val);
    }
    m_nHDMI = h;
    return ERR_NONE;
}

int servInterface::getDefHDMI()
{
    return m_nHDMI;
}


DsoErr servInterface::showEDID()
{
    //m_bEDID = !m_bEDID;
    return ERR_NONE;
}

bool servInterface::getShowEDID()
{
    return m_bEDID;
}

void servInterface::setHDMIEnable(bool /*b*/)
{
    //mUiAttr.setEnable( MSG_APP_UTILITY_HDMI_EDID, b);
    //mUiAttr.setEnable( MSG_APP_UTILITY_HDMI_RES, b);
    //mUiAttr.setEnable( MSG_APP_UTILITY_HDMI, b);
}

void* servInterface::getEDID()
{
    return &m_stEDID;
}

void servInterface::setHMDISigType(int s)
{
    m_nSigType = s;
    serviceExecutor::post(E_SERVICE_ID_PLOT,
                          servPlot::cmd_select_signal, s);
}
