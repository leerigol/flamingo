
#include <QFile>
#include <QKeyEvent>

#include "../../servdso/sysdso.h"
#include "../../include/dsocfg.h"

#include "../service_name.h"

#include "servInterface.h"
#include "servutility.h"

#include "../../com/mdns/mdnsuser.h"
#include "../../servscpi/servscpidso.h"


IMPLEMENT_CMD( serviceExecutor, servInterface )
start_of_entry()
on_set_void_void(   CMD_SERVICE_RST,                    &servInterface::rst),
on_set_int_void (   CMD_SERVICE_STARTUP,                &servInterface::startup),
on_set_int_int  (   CMD_SERVICE_SUB_ENTER,              &servInterface::showForm),
on_get_bool     (   CMD_SERVICE_SUB_ENTER,              &servInterface::isShowForm),


//!Lan config mode
on_set_int_int (    MSG_APP_UTILITY_LXI_CFG,            &servInterface::setIPMode),
on_get_int     (    MSG_APP_UTILITY_LXI_CFG,            &servInterface::getIPMode),
//!lan config apply
on_set_int_void(    MSG_APP_UTILITY_LXI_APPLY,          &servInterface::applyConfig),

//!lan init
on_set_int_void(    MSG_APP_UTILITY_LXI_INIT,           &servInterface::initConfig),

on_set_int_void(    servInterface::cmd_net_rst,               &servInterface::doDefault),
on_set_int_void(    servInterface::cmd_net_init,              &servInterface::initLxi),

//!lan config mdns
on_set_int_bool(    MSG_APP_UTILITY_LXI_MDNS,           &servInterface::setmDNS),
on_get_bool    (    MSG_APP_UTILITY_LXI_MDNS,           &servInterface::getmDNS),

on_set_int_string(  MSG_APP_UTILITY_LXI_HOST_NAME,      &servInterface::setHostSettingName ),
on_get_string(      MSG_APP_UTILITY_LXI_HOST_NAME,      &servInterface::getHostSettingName ),

on_set_int_string(  servInterface::cmd_net_domain,            &servInterface::setServiceDeclareName ),
on_get_string(      servInterface::cmd_net_domain,            &servInterface::getServiceDeclareName ),

on_set_int_string(  servInterface::cmd_net_description,       &servInterface::setServiceSettingName ),
on_get_string(      servInterface::cmd_net_description,       &servInterface::getServiceSettingName ),

on_set_int_void(    MSG_APP_UTILITY_LXI_IPADDRESS,      &servInterface::inputIP),
on_set_int_void(    MSG_APP_UTILITY_LXI_SUBNET,         &servInterface::inputNetmask),
on_set_int_void(    MSG_APP_UTILITY_LXI_GATEWAY,        &servInterface::inputGatway),
on_set_int_void(    MSG_APP_UTILITY_LXI_DNS,            &servInterface::inputDNS),


//!GPIB interface state
//!
on_set_int_int(     MSG_APP_UTILITY_GPIB_MENU,          &servInterface::setGPIB),
on_get_int    (     MSG_APP_UTILITY_GPIB_MENU,          &servInterface::getGPIB),

on_set_int_bool(    MSG_APP_UTILITY_HDMI,               &servInterface::setEnHDMI),
on_get_bool(        MSG_APP_UTILITY_HDMI,               &servInterface::getEnHDMI),

on_set_int_int(     MSG_APP_UTILITY_HDMI_RES,           &servInterface::setDefHDMI),
on_get_int(         MSG_APP_UTILITY_HDMI_RES,           &servInterface::getDefHDMI),

on_set_int_void(    MSG_APP_UTILITY_HDMI_EDID,          &servInterface::showEDID),
on_get_bool(        MSG_APP_UTILITY_HDMI_EDID,          &servInterface::getShowEDID),

on_set_int_int(     servUtility::cmd_hdmi_event,        &servInterface::onHDMI),
on_get_pointer(     servUtility::cmd_hdmi_edid,         &servInterface::getEDID),


//!SCPI get net information
on_set_int_bool(    servInterface::cmd_net_dhcp,        &servInterface::setDHCP),
on_get_bool    (    servInterface::cmd_net_dhcp,        &servInterface::getDHCP),

on_set_int_bool(    servInterface::cmd_net_auto,        &servInterface::setAUTOip),
on_get_bool    (    servInterface::cmd_net_auto,        &servInterface::getAUTOip),

on_set_int_bool(    servInterface::cmd_net_static,      &servInterface::setStatic),
on_get_bool    (    servInterface::cmd_net_static,      &servInterface::getStatic),

on_set_int_int (    servInterface::cmd_net_if,          &servInterface::setNetInterface),
on_get_int     (    servInterface::cmd_net_if,          &servInterface::getNetInterface),

on_set_int_string(  servInterface::cmd_net_ip,          &servInterface::setIP),
on_get_string(      servInterface::cmd_net_ip,          &servInterface::getIP),

on_set_int_string(  servInterface::cmd_net_mask,        &servInterface::setMask),
on_get_string(      servInterface::cmd_net_mask,        &servInterface::getMask),

on_set_int_string(  servInterface::cmd_net_gateway,     &servInterface::setGateway),
on_get_string(      servInterface::cmd_net_gateway,     &servInterface::getGateway),

on_set_int_string(  servInterface::cmd_net_dns,         &servInterface::setDNS),
on_get_string(      servInterface::cmd_net_dns,         &servInterface::getDNS),

on_set_int_string(  servInterface::cmd_net_mac,         &servInterface::setMac),
on_get_string(      servInterface::cmd_net_mac,         &servInterface::getMac),

on_get_string(      servInterface::cmd_net_dhcp_server, &servInterface::getDHCPServer),


on_set_int_int(     servInterface::cmd_net_mode,        &servInterface::setNetMode),
on_get_int     (    servInterface::cmd_net_mode,        &servInterface::getNetMode),

on_set_int_int(     servInterface::cmd_net_status,      &servInterface::setNetStatus),
on_get_int(         servInterface::cmd_net_status,      &servInterface::getNetStatus),

on_set_int_int(     servInterface::cmd_net_config,      &servInterface::netConfig),

on_set_int_void(    servInterface::cmd_close_form,      &servInterface::onFormClose),

on_get_void_argi_argo(servInterface::cmd_visa_addr,     &servInterface::getVisaAddr),

on_set_void_void( CMD_SERVICE_RST,                &servInterface::rst ),
on_set_int_void( CMD_SERVICE_STARTUP,             &servInterface::startup ),
on_set_void_void( CMD_SERVICE_INIT,               &servInterface::init ),

end_of_entry()


servInterface  *g_pInterface = NULL;

servInterface::servInterface(QString name,ServiceId id ) : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();

    m_pLanConfig = NULL;

    m_nSock      = -1;
    mVersion     = 0xb;

    m_strIP      = "192.168.1.10";
    m_strMask    = "255.255.255.0";
    m_strGateway = "192.168.1.1";
    m_strDNS     = "192.168.1.1";

    m_strMAC     = "00:19:AF:00:11:22";

    ServiceSettingName= "RIGOL Oscilloscope";

    m_nNetInterface= NET_IF_ETH;

    //setHDMIEnable(false);
    //disable when restart
    m_bHDMI         = false;
    m_nHDMI         = HDMI_1280_720_60;

    g_pInterface = this;
}

servInterface::~servInterface()
{
}

int servInterface::serialOut( CStream &stream ,unsigned char &ver)
{
    ver = mVersion;

    stream.write(QStringLiteral("ip"),       getIP()     );
    stream.write(QStringLiteral("mask"),     getMask()   );
    stream.write(QStringLiteral("gateway"),  getGateway());
    stream.write(QStringLiteral("dns"),      getDNS()    );

    stream.write(QStringLiteral("mode"),     getIPMode());
    return 0;
}

int servInterface::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        QString param;

        stream.read(QStringLiteral("ip"),       param);
        setIP(param);

        stream.read(QStringLiteral("mask"),     param);
        setMask(param);

        stream.read(QStringLiteral("gateway"),  param);
        setGateway(param);

        stream.read(QStringLiteral("dns"),      param);
        setDNS(param);

        stream.read(QStringLiteral("mode"),       m_nIPMode);
    }
    else
    {
        rst();
    }

    return 0;
}

void servInterface::rst()
{    
    m_bmDNS      = false;
    m_bShowForm  = false;

    m_nIPMode    = IP_ACQUIRE_DHCP | IP_ACQUIRE_AUTO;

    m_bEDID         = false;
    m_nSigType      = 1;

}

void servInterface::init()
{
    mUiAttr.setVisible(MSG_APP_UTILITY_HDMI_EDID, false);
    m_pEDIDThead    = NULL;


    m_nNetStatus = NET_STATUS_UNLINK;
    m_nNetMode   = IP_ACQUIRE_DHCP;
    rst();
}

int servInterface::startup()
{     

    setIPMode(m_nIPMode);

    //disable wifi
#ifdef FLAMINGO_TR5
    mUiAttr.setEnableVisible(MSG_APP_UTILITY_WIFI_MENU, false, false);
    mUiAttr.setEnableVisible(MSG_APP_UTILITY_LXI_HOST_NAME, false, false);
    //mUiAttr.setVisible(MSG_APP_UTILITY_GPIB_MENU, false);
    //mUiAttr.setVisible(MSG_APP_UTILITY_LXI_MDNS, false);

    mUiAttr.setEnableVisible(MSG_APP_UTILITY_LXI_INIT, false, false);
#endif

    updateAllUi();

    return 0;
}

DsoErr servInterface::setNetInterface(int n)
{
    m_nNetInterface = n;
    //mUiAttr.setEnable(MSG_APP_UTILITY_WIFI_MENU, n == NET_IF_WLAN);
    return ERR_NONE;
}

int servInterface::getNetInterface()
{
    return m_nNetInterface;
}

char* servInterface::getInterfaceName()
{
    if( m_nNetInterface == NET_IF_ETH )
    {
        return (char*)NET_DEV;
    }
    else
    {
        return (char*)WLAN_DEV;
    }
}


DsoErr servInterface::setGPIB(int b)
{
    return serviceExecutor::post(E_SERVICE_ID_SCPI,
                dsoServScpi::cmd_usbgpib_addr,
                b);
}

int servInterface::getGPIB()
{
     int addr = 0;
     serviceExecutor::query(serv_name_scpi,
                dsoServScpi::cmd_usbgpib_addr,
                addr);

     setuiRange(1, 30, 1);
     if( sysGetLanguage() == language_thailand )
     {
         setuiOutStr(QString("%1").arg(addr));
     }
     else
     {
        setuiOutStr(QString("GPIB::%1::instr").arg(addr));
     }
     return addr;
}

