#ifndef SERVWIFI_H
#define SERVWIFI_H

#include "../../servgui/servgui.h"

#include "../../service.h"
#include "../../service_name.h"
#include "../../service_msg.h"
#include "../../../baseclass/iserial.h"

#define WIFISIGNAL  QString("/tmp/wifi.signals")
#define WIFINAME    QString("/tmp/wifi.name")
#define WIFILIST    QString("/tmp/wifi.list")

class servWifi : public serviceExecutor,
                 public ISerial
{
    Q_OBJECT

    DECLARE_CMD()
public:

    servWifi(QString name, ServiceId id = E_SERVICE_ID_UTILITY_WIFI );
    ~servWifi();

    int serialOut( CStream &stream ,unsigned char &ver );
    int serialIn( CStream &stream, unsigned char ver );
    void rst();
    int startup();


public:
    //!wifi open/close
    DsoErr setEnable(bool b);
    bool   getEnable();
    void   setMenuEnable(bool);

    //!wifi scan
    DsoErr  doScanning();
    bool    getScanning(void);

    //! wifi passwd
    DsoErr  setName(QString&);
    QString getName();

    DsoErr setKey(bool b);
    bool   getKey();

    //! wifi passwd
    DsoErr  setPassword(QString& pwd);
    QString getPassword();

    //!wifi connect
    DsoErr  doConnect(void);
    void    setConnect(bool);

    DsoErr  setNetStatus(int state);
    int     getNetStatus();


    bool getActive();

    //!enable menu
    void setVisableMenu(bool b);
public:


    bool    m_bEnable;
    bool    m_bKey;

    bool    m_bScanning;
    bool    m_bConnected;

    QString m_Password;
    QString m_Name;

    QThread *m_pWifiThread;
    int      m_nNetStatus;

};

class WifiConfigThread : public QThread
{
    Q_OBJECT
public:
    explicit WifiConfigThread(QObject *parent = 0);

protected:
    void run();
private:

public:
    servWifi *m_pWifi;
};
#endif // SERVWIFI_H

