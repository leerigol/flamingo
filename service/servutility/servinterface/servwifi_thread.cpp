#include "servInterface.h"
#include "servwifi.h"

/***********************************************************
 * Wifi pthread function
 ***********************************************************/
WifiConfigThread::WifiConfigThread(QObject *parent) : QThread(parent)
{
    m_pWifi = dynamic_cast<servWifi*>( parent );
    Q_ASSERT( m_pWifi != NULL );
}


void WifiConfigThread::run()
{
    //if(m_pWifi->m_nNetStatus == servInterface::NET_STATUS_CONNECTED)
    {
        QStringList arg;
        QString output;

        QProcess* pProcess = new QProcess;
        Q_ASSERT(pProcess != NULL);
        pProcess->moveToThread(this);

        qDebug() << "wifi is config...";

        arg << m_pWifi->getName();
        if( m_pWifi->getKey() )
        {
             arg << m_pWifi->getPassword();
        }

        pProcess->start("/rigol/shell/wifi.sh", arg);

        while( pProcess->state() )
        {
            pProcess->waitForFinished(200);
            output = pProcess->readAllStandardOutput();
            if( output.size() > 0 )
            {
                output.remove("\n");
                qDebug() << output;
            }
            if(pProcess->exitCode() != 0)
            {
                break;
            }
        }
        if(output.contains("OK"))
        {
            serviceExecutor::post(E_SERVICE_ID_UTILITY_WIFI,
                                  servInterface::cmd_net_status,
                                  servInterface::NET_STATUS_INIT);
            //get wifi ip
            serviceExecutor::post(E_SERVICE_ID_UTILITY_IOSET,
                                  servInterface::cmd_net_status,
                                  servInterface::NET_STATUS_INIT);
        }
        else
        {
            servGui::showInfo(output);
        }

        m_pWifi->setConnect(true);
        pProcess->deleteLater();
    }

}
