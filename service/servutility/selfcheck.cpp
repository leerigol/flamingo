#include "../../service/servgui/servgui.h"
#include "servutility.h"


DsoErr servUtility::setKeyTest()
{   
    m_bShowKbTest = true;

    //! save led
    queryEngine( qENGINE_LED_STATUS, m_LedStatus );

    //! all led off
    for(int i = led_ch1; i<=led_top; i++)
    {
        syncEngine( ENGINE_LED_OP, i, false );
    }
    return ERR_NONE;
}

bool servUtility::getKeyTest()
{
    return m_bShowKbTest;
}

DsoErr servUtility::setKeyTestOver(bool b)
{
    if(b == false)
    {
        syncEngine( ENGINE_LED_STATUS, m_LedStatus );
    }
    return ERR_NONE;
}

DsoErr servUtility::setLedOn(int led)
{
    syncEngine(ENGINE_LED_ON, led);
    return ERR_NONE;
}

DsoErr servUtility::setLedOff(int led)
{
    syncEngine(ENGINE_LED_OFF, led);
    return ERR_NONE;
}

DsoErr servUtility::setLcdTest()
{
    m_bShowLcdTest = true;
    return ERR_NONE;
}

bool servUtility::getLcdTest()
{
    return m_bShowLcdTest;
}

DsoErr servUtility::setBkLight( int light )
{
    if ( light < 0 || light > 100 )
    { return ERR_INVALID_INPUT;  }

    syncEngine( ENGINE_LCD_LIGHT, light );

    return ERR_NONE;
}

DsoErr servUtility::setTouchTest()
{
    m_bShowTouchTest = !m_bShowTouchTest;
    return ERR_NONE;
}

bool servUtility::getTouchTest()
{
    return m_bShowTouchTest;
}


DsoErr servUtility::setFanCheck()
{
    m_bShowFanTest = !m_bShowFanTest;
    return ERR_NONE;
}

bool servUtility::getFanCheck()
{
    return m_bShowFanTest;
}

DsoErr servUtility::setFanSpeed(int speed)
{
    if(speed>=0 && speed <100)
    {
        m_nFanSpeed = speed;
        syncEngine(ENGINE_FAN_SPEED,speed);
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
    return ERR_NONE;
}

int servUtility::getFanSpeed()
{
    return m_nFanSpeed;
}

DsoErr servUtility::shutDown()
{
    syncEngine(ENGINE_POWER_SHUTDOWN);
    return ERR_NONE;
}

DsoErr servUtility::checkResult()
{
    mUiAttr.setEnable(MSG_APP_UTILITY_SELF_TEST, true);
    return ERR_NONE;
}

DsoErr servUtility::setBoardTest()
{
    m_bShowBoardTest = !m_bShowBoardTest;
    if(m_bShowBoardTest)
    {
        setuiEnable(false);
    }
    else
    {
    }

    return ERR_NONE;
}

bool servUtility::getBoardTest()
{
    return m_bShowBoardTest;
}

void* servUtility::getTestItem()
{
    return &m_testItems;
}

void servUtility::createTestTable()
{
    if(m_testItems.size() > 0)
    {
        return;
    }

//    m_testItems.append(new stBoardTestItem("PROBE_OFFSET4",0X37,7,1,0,0));
//    m_testItems.append(new stBoardTestItem("PROBE_OFFSET3",0X37,6,1,0,0));
//    m_testItems.append(new stBoardTestItem("PROBE_OFFSET2",0X37,5,1,0,0));
//    m_testItems.append(new stBoardTestItem("PROBE_OFFSET1",0X37,4,1,0,0));

    m_testItems.append(new stBoardTestItem("RTC Date",0X32,0,"1970-01-01", "2099-12-30"));
    m_testItems.append(new stBoardTestItem("RTC Time",0X32,1,"00-00-00", "59:59:59"));
    m_testItems.append(new stBoardTestItem("Touch",   0X48,0,"0x0000", "0xffff"));

    m_testItems.append(new stBoardTestItem("TempFan", 0X1c,1,0.001,0,80,Unit_degree));
    m_testItems.append(new stBoardTestItem("TempMADC",0X1d,1,0.001,0,112,Unit_degree));
    m_testItems.append(new stBoardTestItem("TempSADC",0X1e,1,0.001,0,12,Unit_degree));

    m_testItems.append(new stBoardTestItem("ID_RES_CH1",0X37,3,1.5,0.02423,3.6));
    m_testItems.append(new stBoardTestItem("ID_RES_CH2",0X37,2,1.5,0.02423,3.6));
    m_testItems.append(new stBoardTestItem("ID_RES_CH3",0X37,1,1.5,0.02423,3.6));
    m_testItems.append(new stBoardTestItem("ID_RES_CH4",0X37,0,1.5,0.02423,3.6));

    m_testItems.append(new stBoardTestItem("3.3V_SADC",  0x1f,7,1.5,3.2,3.4));
    m_testItems.append(new stBoardTestItem("3.3V_ADC",   0x1f,6,1.5,3.2,3.4));
    m_testItems.append(new stBoardTestItem("MGT_1V2",    0x1f,5,1.1,1.1,1.3));
    m_testItems.append(new stBoardTestItem("MGT_1V0",    0x1f,4,1.1,0.9,1.1));
    m_testItems.append(new stBoardTestItem("-5V_Probe_B",0x1f,3,-2.5,-5.1,-4.9));
    m_testItems.append(new stBoardTestItem("+5V_Probe",  0x1f,2,2.5,4.9,5.1));
    m_testItems.append(new stBoardTestItem("+5V_USB",    0x1f,1,2.5,4.9,5.1));

    m_testItems.append(new stBoardTestItem("MON_FAN",      0x1f,0,5,2,12));
    m_testItems.append(new stBoardTestItem("3.3V_AFE",     0x35,7,1.5,3.2,3.4));
    m_testItems.append(new stBoardTestItem("+5V_AFE",      0x35,6,2.5,4.9,5.3));
    m_testItems.append(new stBoardTestItem("3.3V_Analog",  0x35,5,1.5,3.2,3.4));
    m_testItems.append(new stBoardTestItem("+5V_Analog",   0x35,4,2.5,4.9,5.1));
    m_testItems.append(new stBoardTestItem("-5V6_Analog_B",0x35,3,-2.5,-5.7,-5.5));
    m_testItems.append(new stBoardTestItem("VCC_DDR_1V5",  0x35,2,1.1,1.45,1.51));
    m_testItems.append(new stBoardTestItem("VCC_1V8",      0x35,1,1.1,1.75,1.81));
    m_testItems.append(new stBoardTestItem("VCC_3V3",      0x35,0,1.5,3.2,3.4));
}



/******************************************************************************
 * pthread selfcheck                                                              *
 ******************************************************************************/
BoardTestThread::BoardTestThread(QObject *parent) : QThread()
{
    m_pBoardTest = dynamic_cast<servUtility*>( parent );
    Q_ASSERT( m_pBoardTest != NULL );
}

void BoardTestThread::run()
{
    QThread::sleep(30);
    while( true )
    {
        checking();
        //sync
        if( m_pBoardTest->getBoardTest() )
        {
            serviceExecutor::post(m_pBoardTest->getId(),
                                  servUtility::cmd_dso_checkboard,
                                  0);

        }

        /*{
            QList<stBoardTestItem*> *pTestItems =
                    (QList<stBoardTestItem*>*)m_pBoardTest->getTestItem();
            stBoardTestItem* pTouch = pTestItems->at(2);;
            if( pTouch->_curr.compare("0x2543") == 0 )
            {
                serviceExecutor::post(m_pBoardTest->getId(),
                                      servUtility::cmd_dso_restart,
                                      0);
            }
        }*/
#if 1
        {
            static uint nOverTimes = 0;

            QList<stBoardTestItem*> *pTestItems =
                    (QList<stBoardTestItem*>*)m_pBoardTest->getTestItem();
            stBoardTestItem* fan = pTestItems->at(3);
            stBoardTestItem* adc1 = pTestItems->at(4);
            stBoardTestItem* adc2 = pTestItems->at(5);
            //qDebug() << fan->value << adc1->value << adc2->value;

            if(fan->value > 80 || adc1->value > 112 || adc2->value > 112 )
            {
                nOverTimes++;
                if( nOverTimes > 10 )
                {
                    serviceExecutor::post(m_pBoardTest->getId(),
                                          servUtility::cmd_dso_poweroff,
                                          0);
                }
                else
                {
                    QString msg = QString("Temperature warnning.Poweroff in 30s.");

                    qDebug() << "High temp: Fan=" << fan->value
                             << ", ADC1=" << adc1->value;
                    servGui::showInfo(msg, Qt::red);
                    serviceExecutor::post(E_SERVICE_ID_UTILITY,
                                          servUtility::cmd_dso_beeper,
                                          true);
                }
            }
            else
            {
                nOverTimes = 0;
            }
        }
#endif

        QThread::sleep(3);
    }
}

DsoErr BoardTestThread::checking()
{
    QString devPrex = "/sys/bus/i2c/devices";
    QString devFile = "";
    QList<stBoardTestItem*> *pTestItems =
            (QList<stBoardTestItem*>*)m_pBoardTest->getTestItem();


    int size = pTestItems->size();
    for(int i=0; i<size; i++)
    {
        stBoardTestItem* item = pTestItems->at(i);
        devFile = QString("%1/0-00%2/in%3_input").arg(devPrex)
                                                .arg(item->address,2,16,QChar('0'))
                                                .arg(item->channel);
        if( QFile::exists(devFile) )
        {
            QFile file(devFile);
            if( file.open(QIODevice::ReadOnly) )
            {
                if(item->unit == Unit_none)
                {
                    QTextStream ts(&file);
                    QString dt  = ts.readLine();
                    //qDebug() << dt;
                    item->setTestValue( dt );
                }
                else
                {
                    QByteArray ba = file.readAll();
                    double f = ba.toInt();
                    f = f * item->ratio;
                    //qDebug() << item->testName << ":"<< ba << ba.size() << f ;
                    if(item->unit == Unit_V)
                    {
                        item->setTestValue(f/1000);
                    }
                    else
                    {
                        item->setTestValue(f);
                    }
                }
                file.close();
            }
        }
    }

    return ERR_NONE;
}

//DsoErr BoardTestThread::Ethernettest()
//{
//    EthernetStr = "Ethernet State:";
//    QString path = "/sys/bus/platform/devices/e000b000.ps7-ethernet/net/eth0/operstate";

//    QFile EthOperstate(path);
//    QString state;
//    if(!EthOperstate.open(QIODevice::ReadOnly | QIODevice::Text))
//    {
//        state = "No Device";
//        EthernetStr = EthernetStr.append(state);
//        return ERR_FILE_OP_FAIL;
//    }
//    else
//    {
//        QTextStream txtInput(&EthOperstate);
//        EthernetStr = EthernetStr.append(txtInput.readLine());
//    }

//    return ERR_NONE;
//}

//DsoErr BoardTestThread::USBdevtest()
//{
//    USBStr = "USB Host/Device:";
//    QString cmd = "lsusb";
//    QProcess *pro = new QProcess();
//    pro->start(cmd);

//    if(!pro->waitForFinished(1))
//        return ERR_LINUX_CMD_EXECUTE;

//    QString result = pro->readAllStandardOutput();
//    int host = result.indexOf("Device 001");
//    QString UsbHostStr;
//    if(host >= 0)
//    {
//        UsbHostStr = "USB Host Ok";
//    }
//    else
//    {
//        UsbHostStr = "USB Host Null";
//    }

//    int device = result.indexOf("Device 002");
//    QString UsbDeviceStr;
//    if(device >= 0)
//    {
//        UsbDeviceStr = "USB Device Ok";
//    }
//    else
//    {
//        UsbDeviceStr = "USB Device Null";
//    }

//    USBStr = USBStr.append(UsbHostStr.append(",").append(UsbDeviceStr));

//    pro->close();
//    delete pro;
//    pro = NULL;

//    return ERR_NONE;
//}


void  servUtility::getTestItemValue(CArgument argi, CArgument &argo)
{
    if(argi.size() != 1)
    {
        return;
    }

//    for(int i=0; i<m_testItems.count(); i++)
//    {
//        qDebug()<<"name("<<i<<"):"<<m_testItems.at(i)->testName<<m_testItems.at(i)->value;
//    }

    QString name = argi[0].strVal;
    for(int i = 0; i < m_testItems.count(); i++)
    {
        if(m_testItems.at(i)->testName.compare( name,Qt::CaseInsensitive) == 0)
        {
//            if( m_testItems.at(i)->testName == "RTC Date"
//                    || m_testItems.at(i)->testName == "RTC Time"
//                    || m_testItems.at(i)->testName == "Touch" )
//            {
//                argo.setVal(m_testItems.at(i)->_curr, 0);
//            }
//            else
            {
                argo.setVal(m_testItems.at(i)->value, 0);
            }
            return;
        }
    }
}
