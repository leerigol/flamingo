#include <unistd.h>

#include "servutility.h"
#include "../servlicense/servlicense.h"
#include "../../com/crc32/ccrc32.h"
#include "../../com/crypt/xxtea.h"
#include "../servdso/sysdso.h"
#include "../servutility/servinterface/servInterface.h"
#include "../servgui/servgui.h"
#include "../servch/servch.h"
#include "../servdso/servdso.h"


DsoErr servUtility::setDateTime()
{
    QString year = QString::number(system_date >> 16 & 0xffff);
    QString month = QString::number(system_date >> 8 & 0xff,10);
    QString day = QString::number(system_date & 0xff,10);

    QString hour = QString::number(system_time >> 16 & 0xff);
    QString minute = QString::number(system_time >> 8 & 0xff);
    QString second = QString::number(system_time & 0xff);

    QString datetime = "date";
    QString cmd = datetime+' '+'-'+'s';
    QString set = '"'+ year + '-' + month + '-' + day +
                  ' '+ hour + ':'+ minute + ':' + second + '"';
    cmd.append(set);

    QProcess::execute(cmd);

    QProcess::execute(QString("hwclock -w"));

    return ERR_NONE;
}

/*!
 * \brief servUtility::getDateTime
 */
void servUtility::getDateTime()
{
    bool ok;
    QDateTime time = QDateTime::currentDateTime();

    qint16 year = time.currentDateTime().toString("yyyy").toInt(&ok,10);
    qint8 month = time.currentDateTime().toString("MM").toInt(&ok,10);
    qint8 day = time.currentDateTime().toString("dd").toInt(&ok,10);
    qint8 hour = time.currentDateTime().toString("hh").toInt(&ok,10);
    qint8 minute = time.currentDateTime().toString("mm").toInt(&ok,10);
    qint8 second = time.currentDateTime().toString("ss").toInt(&ok,10);

    system_date =  (year << 16) | (month << 8) | day;

    system_time =  (hour << 16) | (minute << 8) | second;
}

DsoErr servUtility::scpiSetDate(CArgument argi)
{
    if(argi.size() != 3)
    {
        return ERR_INVALID_INPUT;
    }

    int year  = argi[0].iVal;
    int month = argi[1].iVal;
    int day   = argi[2].iVal;

    if( (2011 > year) || (2099 < year)
       ||(1   > month)|| (12   < month)
       ||(1   > day)  || (31   < day)  )
    {
        return ERR_INVALID_INPUT;
    }

    system_date = (0x00000000 | (year<<16) | (month<<8) | day);

    async(MSG_APP_UTILITY_YEAR , year);
    async(MSG_APP_UTILITY_MONTH , month);
    async(MSG_APP_UTILITY_DAY , day);

    async(MSG_APP_UTILITY_APPLY_TIME , 0);

    return ERR_NONE;
}

void servUtility::scpiGetDate(CArgument &argo)
{
    getDateTime();

    int year  = system_date>>16;
    int month = ((system_date>>8)&0x0000ff);
    int day   = system_date&0x000000ff;

    argo.setVal(year, 0);
    argo.setVal(month,1);
    argo.setVal(day,  2);
}

DsoErr servUtility::ScpiSetTime(CArgument argi)
{
    if(argi.size() != 3)
    {
        return ERR_INVALID_INPUT;
    }

    int hours   = argi[0].iVal;
    int minutes = argi[1].iVal;
    int seconds = argi[2].iVal;

    if(  (0 > hours)      || (23    < hours)
       ||(0   > minutes)  || (60    < minutes)
       ||(0   > seconds)  || (60    < seconds)  )
    {
        return ERR_INVALID_INPUT;
    }

    system_time = (0x00000000 | (hours<<16) | (minutes<<8) | seconds);

    async(MSG_APP_UTILITY_HOUR , hours);
    async(MSG_APP_UTILITY_MINUTE , minutes);
    async(MSG_APP_UTILITY_SECOND , seconds);

    async(MSG_APP_UTILITY_APPLY_TIME , 0);

    return ERR_NONE;
}

void servUtility::scpiGetTime(CArgument &argo)
{
    getDateTime();

    int hours   = system_time>>16;
    int minutes = ((system_time>>8)&0x0000ff);
    int seconds = system_time&0x000000ff;

    argo.setVal(hours,   0);
    argo.setVal(minutes, 1);
    argo.setVal(seconds, 2);
}

DsoErr servUtility::saveMac(QString mac)
{
    //! 参数检查
    QRegExp re("^(?:[0-9A-F]{2}(?::[0-9A-F]{2}){5})$",Qt::CaseInsensitive);
    int pos = re.indexIn(mac,0);

    if(pos == -1)
    {
        qWarning()<<__FILE__<<__LINE__<<mac;
        return  ERR_INVALID_INPUT;
    }

#if 1
    post(E_SERVICE_ID_UTILITY_IOSET, servInterface::cmd_net_mac, mac);
#else
    //！存储
    QFile file("/rigol/data/mac.txt");
    if(file.open( QIODevice::WriteOnly))
    {
        file.write(mac.toLatin1());
        fsync(file.handle());
        file.close();
        LOG_DBG()<<"save mac successful:"<<mac;
    }
    else
    {
        return ERR_FILE_SAVE_FAIL;
    }
#endif
    return ERR_NONE;
}

struct bwval_bw
{
    int bandVal;
    Bandwidth bw;
};
static bwval_bw _bwval_bws[]=
{
    {14,BW_100M},
    {24,BW_200M},
    {34,BW_350M},
    {54,BW_500M},

    {104,BW_1G},
};
bool servUtility::parseBw( const QString &model, Bandwidth *pBw )
{
    Q_ASSERT( NULL != pBw );

    //! get band str
    QRegExp reModel("((?:MSO)|(?:DS))(?:[0-9])([01][01235]4)",
                    Qt::CaseInsensitive);

    if((-1) == reModel.indexIn(model,0) )
    { return false; }

    QString bwStr = reModel.cap(2);
    if( bwStr.isEmpty() )
    { return false; }

    bool bOK;
    int bwVal;
    bwVal = bwStr.toInt( &bOK );
    if ( !bOK )
    { return false; }

    for ( int i = 0; i < array_count(_bwval_bws); i++ )
    {
        if ( bwVal == _bwval_bws[i].bandVal )
        {
            *pBw = _bwval_bws[i].bw;
            return true;
        }
    }

    return false;
}

DsoErr servUtility::setSysBand(Bandwidth value)
{
    mSysBand = value;

    id_async(E_SERVICE_ID_CH1, servCH::cmd_bw_apply);
    id_async(E_SERVICE_ID_CH2, servCH::cmd_bw_apply);
    id_async(E_SERVICE_ID_CH3, servCH::cmd_bw_apply);
    id_async(E_SERVICE_ID_CH4, servCH::cmd_bw_apply);

    return ERR_NONE;
}

Bandwidth servUtility::getSysBand()
{
    return mSysBand;
}

Bandwidth servUtility::getRawBand()
{
    return mRawBand;
}

void servUtility::setSpyonOpt()
{
    Bandwidth bw;
    bool bOk;
    bw = queryOptBand( &bOk );
    if ( !bOk )
    {
        return;
    }

    //! opt high
    if ( bw > mRawBand )
    {
        mSysBand = bw;
    }
    else
    {
        mSysBand = mRawBand;
    }
}

struct OPT_BW
{
    OptType opt;
    Bandwidth bw;
};
static OPT_BW _opt_bws[] =
{
    {OPT_BW3T5, BW_500M},
    {OPT_BW2T5, BW_500M},
    {OPT_BW1T5, BW_500M},
    {OPT_BW2T3, BW_350M},
    {OPT_BW1T3, BW_350M},
    {OPT_BW1T2, BW_200M},
};
//! query the highest band
Bandwidth servUtility::queryOptBand( bool *pbOk )
{
    Q_ASSERT( NULL != pbOk );
    for (int i = 0; i < array_count(_opt_bws); i++ )
    {
        if ( sysCheckLicense(_opt_bws[i].opt) )
        {
            *pbOk = true;
            return _opt_bws[i].bw;
        }
    }
    *pbOk = false;
    return _opt_bws[0].bw;
}


/********************************************************************
 * 工厂写配置信息 mac地址，SN等等
 * *****************************************************************/



//文件存储密钥
//!queryEngine(qENGINE_ADC_UUID,(char*)fileKeys);
unsigned int fileKeys[4] = {0};


/*!
 * \brief servUtility::initSysVendorConfig
 * 每次开机后初始化，先从配置文件中读取，如果读取失败，从备份区域读取，
 * 如果还是失败，直接给定默认值。
 * \return
 */
DsoErr servUtility::initSysVendorConfig()
{
    static bool bOnce = true;
    if(bOnce)
    {
        bOnce = false;

        queryEngine(qENGINE_ADC_UUID, (char*)fileKeys);
        /*qDebug()<<__FUNCTION__<<__LINE__
                <<QString::number( fileKeys[0], 16 )
                <<QString::number( fileKeys[1], 16 )
                <<QString::number( fileKeys[2], 16 )
                <<QString::number( fileKeys[3], 16 ); */

        QByteArray itemData;

        if(ERR_NONE == getVendorItem(E_CFG_MODEL_RAW, itemData))
        {
            setModel(itemData);
        }
        else
        {
            if( m_nHardVer & LA_BIT )
            {
                setModel( DEFAULT_MODEL_MSO );
            }
            else
            {
                setModel( DEFAULT_MODEL_DSO );
            }
        }

        if(ERR_NONE == getVendorItem(E_CFG_SN_RAW, itemData))
        {
            setSerial(itemData);
        }
        else
        {
            if( m_nHardVer & LA_BIT )
            {
                setSerial(DEFAULT_SERIAL_MSO);
            }
            else
            {
                setSerial(DEFAULT_SERIAL_DSO);
            }
        }

        //    if(ERR_NONE == getVendorItem(E_CFG_MODUELS_RAW, itemData))
        //    {
        //    }
        //    else
        //    {
        //    }

        if(ERR_NONE == getVendorItem(E_CFG_MAC, itemData))
        {
            saveMac(itemData);
        }
    }
    return ERR_NONE;
}

/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1*/
/*!
 * \brief  servUtility::setSysVendorItem  从SCPI收到的密文数据保存到文件中，并将系统信息跟新
 * \param arg
 *  arg[0]:           写入的项目     enumCfgItem类型
 *  arg[1]:           此项目对应的数据 二进制形式ArbBin类型
 *  arg[1][0-3  ]     CRC           CRC计算（数据+有效数据长度）
 *  arg[1][4-7  ]     数据总长度     (byte)
 *  arg[1][8-11 ]     有效数据的长度 （byte）
 *  arg[1][12...]     数据           加密
 * \return
 */
DsoErr servUtility::setSysVendorItem(CArgument arg)
{
    //qDebug()<<__FILE__<<__LINE__<<"sys cfg srg size"<<arg.size();

    DsoErr err;
    if(arg.size() != 2)
    {
        err = ERR_INVALID_INPUT;
        return err;
    }
    enumCfgItem id   = (enumCfgItem)arg[0].iVal;
    QByteArray inData( (char*)(arg[1].arbVal.mPtr), arg[1].arbVal.mBlockLen);
    //qDebug()<<__FILE__<<__LINE__<<"\n\n\nscpi in data: \n"<< inData;

    /*!CRC*/
    quint32 inCrc    =  *((int*)inData.data());         /*!提取CRC*/
    qint32 dataLen  =  *((int*)(inData.data() + 4));   /*!提取数据长度*/
    quint32 crc32    = 0;
    if( (inData.size() - 8) < (dataLen + 4))
    {
        err = ERR_SYS_VEND_CONF_CHECK_FAIL;
        return err;
    }
    crc32 =  com_algorithm::CCrc32::crc32(inData.data() + 8, dataLen + 4); //！CRC计算（数据+有效数据长度）
    if(crc32 != inCrc)
    {
        err = ERR_SYS_VEND_CONF_CHECK_FAIL;
        return err;
    }

    //!如果已经有了 将其删掉
    if( sysVendorMap.contains( id ) )
    {
        sysVendorMap.remove( id );
    }

    //！保存数据
    sysVendorMap.insert(id, inData);
    saveVendorData(sysVendorMap);

//！更新系统信息
/*************************************/
    QByteArray itemData;
    err = getVendorItem(id, itemData) ;
    if(ERR_NONE != err)
    {
        return err;
    }

    switch (id)
    {
        case E_CFG_MODEL_RAW:      //！型号
            setModel(itemData);
            setSpyonOpt();
            break;
        case E_CFG_BAND_RAW:       //带宽

            break;
        case E_CFG_SN_RAW:         //！序列号
            setSerial(itemData);
            break;
        case E_CFG_MODUELS_RAW:    //！模块(DG,LA,....)

            break;
        case E_CFG_MAC:           //！MAC地址
            saveMac(itemData);
            break;

        default:
            break;
    }
/*************************************/
    return ERR_NONE;
}

/*!
 * \brief  servUtility::getSysVendorItem  从加密文件中获取密文数据
 * \param id       获取的的项目id  enumCfgItem类型
 * \param outData  此项目对应的数据 二进制形式ArbBin类型 (与set中的数据结构相同) 密文格式
 * \return
 */

void servUtility::getSysVendorItem(CArgument id, CArgument &outData)
{
    if(id.size() != 1)
    {
        return ;
    }
    enumCfgItem e_id = (enumCfgItem)id[0].iVal;

    if(ERR_NONE == loadVendorData(sysVendorMap) )
    {
        VendorData::iterator i = sysVendorMap.find(e_id);
        if( (i != sysVendorMap.end()) && (e_id == i.key()) )
        {
              //！回复数据
              ArbBin  arb_data(i.value().size(), (quint8*)i.value().data());

              outData.append(arb_data);
        }
    }
}
/*!
 * \brief servUtility::getVendorItem  从加密文件中解析出对应ID的数据
 * \param id                          项目
 * \param outdata                     解析后的明文数据
 * \return
 */
DsoErr servUtility::getVendorItem(servUtility::enumCfgItem id, QByteArray &outdata)
{
    DsoErr err;

    if(ERR_NONE != loadVendorData(sysVendorMap) )
    {
        err = ERR_SYS_VEND_CONF_LOAD_FAIL;
        return err;
    }

    /*! data此时结构为： CRC(4byte) + 4字节对齐后的数据长度(4byte) + 有效数据长度(4byte) + （数据 + 对齐字节)(加密) */
    QByteArray data;

    VendorData::iterator i = sysVendorMap.find(id);
    if( (i != sysVendorMap.end()) && (id == i.key()) )
    {
       data = i.value();
    }
    else
    {
        err = ERR_SYS_VEND_CONF_LOAD_FAIL;
        return err;
    }

    /*!CRC*/
    quint32 inCrc    =  *((int*)data.data());         /*!提取CRC*/
    qint32 dataLen  =  *((int*)(data.data() + 4));   /*!提取数据长度*/
    quint32 validLen  = *((int*)(data.data() + 8));    /*!提取有效数据长度*/
    quint32 crc32    = 0;
    if( (data.size() - 8) < (dataLen + 4))
    {
        err = ERR_SYS_VEND_CONF_CHECK_FAIL;
        return err;
    }
    crc32 =  com_algorithm::CCrc32::crc32(data.data() + 8, dataLen + 4); //！CRC计算（数据+有效数据长度）
    if(crc32 != inCrc)
    {
        err = ERR_SYS_VEND_CONF_CHECK_FAIL;
        return err;
    }

    //!前面12字节已经没用了
    data.remove(0,12);

    /*!解密*/
    //qDebug()<<__FILE__<<__LINE__<<"scpiKeys decode  -----1----: \n"<< data;
    err = decodeVendorData(data);
    if(err != ERR_NONE )
    {
        err = ERR_SYS_VEND_CONF_DECODE_FAIL;
        return err;
    }
    //qDebug()<<__FILE__<<__LINE__<<"scpiKeys decode: ------2----:\n"<< data;

    //!删除解密后的无效字节
    data.remove(validLen, data.size() - validLen);

    outdata = data;

    return ERR_NONE;
}


#define SYS_VENDOR_PATH     "/rigol/data/sysvendor.bin"
#define SYS_VENDOR_ID_LEN   4
#define SYS_VENDOR_DATA_LEN 12
#define VALID_LEN           12    //！有效数据长度指  所有项目 ( (IL长度)+(ID长度）+(DL长度)+(数据长度）） 的和。
#define CRC_LEN             12
/*!
 * \brief servUtility::saveVendorData
 *
 * 存储结构     CRC  +  总数据长度(不包括CRC)   +    ID长度(IL)     +     ID     +      数据长度(DL)     +      数据
 *             .             .                      .                 .                .                   .
 * 长度(BYTE)   4            12               SYS_VENDOR_ID_LEN       IL        SYS_VENDOR_DATA_LEN         DL
 *
 * \param data    QMap<enumCfgItem, QByteArray>
 * \return
 */
DsoErr servUtility::saveVendorData(VendorData &data)
{
    DsoErr err = ERR_NONE;

    //!读取数据结构中的数据
    QByteArray fileData;
    fileData.clear();

    QMetaEnum metaEnum = QMetaEnum::fromType<servUtility::enumCfgItem>();
    QByteArray lbay_data;
    lbay_data.clear();

    VendorData::iterator i = data.begin();
    while(i != data.end() )
    {
        lbay_data = metaEnum.valueToKey(i.key());
        fileData.append(QString("%1")
                   .arg(lbay_data.size(),SYS_VENDOR_ID_LEN,10,QLatin1Char('0')).toLatin1()); //!保存id长度
        fileData.append(lbay_data); //!保存id的名称

        lbay_data = i.value();
        fileData.append(QString("%1")
                   .arg(lbay_data.size(),SYS_VENDOR_DATA_LEN,10,QLatin1Char('0')).toLatin1()); //!保存数据长度
        fileData.append(lbay_data); //!保存数据内容

        i++;
    }
    //！CRC校验
    quint32 crc32 = 0;
    fileData.insert(0, QString("%1").arg(fileData.size(), VALID_LEN, 10, QLatin1Char('0')) ); //!插入有效数据长度
    crc32 =  com_algorithm::CCrc32::crc32(fileData.data() + VALID_LEN, fileData.size() - VALID_LEN );             //！CRC只计算有效数据
    fileData.insert(0, QString("%1").arg(crc32, CRC_LEN, 10, QLatin1Char('0')) );             //!插入CRC值

    //！加密
    err = encodeVendorData(fileData, fileKeys);
    if(err != ERR_NONE )
    {
        return err;
    }

    //！存储
    QFile file(SYS_VENDOR_PATH);
    if(file.open( QIODevice::WriteOnly))
    {
        file.write(fileData);
        fsync(file.handle());
        file.close();
    }
    else
    {
        err = ERR_FILE_SAVE_FAIL;

    }

    //!备份到FRam
    if(ERR_NONE == err)
    {
        int idBase;
        idBase = servDso::getIdBase( getId());
        int testId = idBase + FRAM_ID_SYS_INFO;
        int size = servDso::getLen( testId);
        LOG_DBG()<<"servDso::getLen: "<<size;

#ifndef  _CLEAR_EEPROM_DATA
        if(size != 0)  //!Farm中此 ID已经存在， 需要先删除，防止重复占用空间
        {
            int err = servDso::remove( testId);
            if( err != ERR_NONE)
            {
                LOG_DBG() << "servDso::remove Err!!";
            }
            else
            {
                LOG_DBG() << "servDso::remove Id = "<<testId;
            }
        }
#else
        //add by dba for bug 2443
        int err = servDso::format();  //Clear private data
        if(err != ERR_NONE )
        {
            return err;
        }

        post( E_SERVICE_ID_DSO, servDso::CMD_FLUSH_CACHE, (int)1 );

#endif

        int saveSize = servDso::save( testId, fileData.data(), fileData.size());
        if( saveSize != fileData.size())
        {
            LOG_DBG() << "!!!!!!!servDso::save fall.";
            LOG_DBG() << "ret size: "<<saveSize<<"data size:"<<fileData.size();
        }
        else
        {
            post( E_SERVICE_ID_DSO, servDso::CMD_FLUSH_CACHE, (int)1 );
            LOG_DBG() << "servDso::save succes :"<<"size:"<<saveSize;
        }
    }
    return err;
}

static QByteArray catData(int len, QByteArray &data)
{
    QByteArray ret_data;

    if( !data.isEmpty() )
    {
        int cat_size = (len <= data.size()) ? len : data.size();

        ret_data = data.left(cat_size);

        data.remove(0, cat_size);
    }
    else
    {
        ret_data.clear();
    }

    return ret_data;
}

DsoErr servUtility::loadVendorData(VendorData &outData)
{
    DsoErr err = ERR_NONE;
    QByteArray  fileData;

    /*!********************************************************************************
     * .从加密文件加载
     * *******************************************************************************/
    QFile file(SYS_VENDOR_PATH);
    if(file.open( QIODevice::ReadOnly))
    {
        fileData = file.readAll();
    }
    else
    {
        err = ERR_SYS_VEND_CONF_OPEN_FAIL;
    }
    file.close();

    /*!********************************************************************************
     * .从文件加载失败,从备份区域加载数据
     * .备份区域加载数据成功后，恢复文件。
     * *******************************************************************************/
    if(ERR_NONE != err)
    {
        int idBase = servDso::getIdBase( getId());
        int testId = idBase + FRAM_ID_SYS_INFO;
        int size = servDso::getLen(testId);
        LOG_DBG()<<"servDso::getLen: "<<size<<"id:"<<testId;

        if(size>0) //! load data
        {
            quint8 *pData   = new quint8[size]();
            int    loadSize = servDso::load( testId, pData, size);
            if(loadSize == size)
            {
                fileData.clear();
                fileData = QByteArray((char*)pData, loadSize);
                err = ERR_NONE;
                LOG_DBG()<<"servDso::load succes!";

                //！恢复数据
                QFile file(SYS_VENDOR_PATH);
                if(file.open( QIODevice::WriteOnly))
                {
                    file.write(fileData);
                    fsync(file.handle());
                    file.close();
                    LOG_DBG()<<"Restore data succes!";
                }
            }
            else
            {
                err = ERR_SYS_VEND_CONF_OPEN_FAIL;
            }
            delete []pData;
            pData = NULL;
        }
    }


    /*!********************************************************************************
     * .备份区域也加载失败，退出使用默认设置。
     * *******************************************************************************/
    if(ERR_NONE != err)
    {return err;}


    /*!.解密*******************************************************************************/
    err = decodeVendorData(fileData, fileKeys);
    if(err != ERR_NONE )
    {
        return err;
    }

    /*!.CRC校验 （CRC只计算有效数据）**********************************************************/
    quint32 crc      = catData(CRC_LEN,   fileData).toUInt();
    qint32 validLen = catData(VALID_LEN, fileData).toInt();

    if( fileData.size() < validLen)
    {
        err = ERR_SYS_VEND_CONF_CHECK_FAIL;
        return err;
    }
    quint32 crc32    = com_algorithm::CCrc32::crc32(fileData.data(), validLen);
    if(crc32 != crc)
    {
        err = ERR_SYS_VEND_CONF_CHECK_FAIL;
        return err;
    }

    /*!.更新数据**********************************************************/
    outData.clear();
    QMetaEnum   metaEnum = QMetaEnum::fromType<servUtility::enumCfgItem>();
    QByteArray  lbyte_data;
    int         lbyte_size;
    while(4 < fileData.size()) //!如果剩余的字节数小于4 则一定是加密时添加的对齐字节 直接忽略掉
    {
        //!读取ID长度
        lbyte_size       = catData(SYS_VENDOR_ID_LEN, fileData).toInt();
        //!读取ID
        lbyte_data       = catData(lbyte_size, fileData);
        enumCfgItem id   = (enumCfgItem)metaEnum.keysToValue(lbyte_data);

        //!读取数据长度
        lbyte_size       = catData(SYS_VENDOR_DATA_LEN, fileData).toInt();
        //!读取数据
        lbyte_data       = catData(lbyte_size, fileData);

        outData.insert(id,lbyte_data);
    }

    /*!.******************************************************************/
    return ERR_NONE;
}



/*!
 * \brief servUtility::encodeVendorData
 * 对数据流进行加密,输入必须按DWORD对齐,即只针对DWORD数据进行加密
 * \param data
 * \param newKey
 * \return
 */
DsoErr servUtility::encodeVendorData(QByteArray &data, unsigned int *newKey)
{
    //!4字节对齐后的大小
    int  buffSize = (data.size() + 3)&(~(3)) ;

    //！给原始数据补0对齐
    const char *s = "00000";
    data.append(s, (buffSize - data.size()) );


    com_algorithm::CXXTEA::setKeys( (XXTEA_TYPE*)newKey );
    if( 0 == com_algorithm::CXXTEA::encode((XXTEA_TYPE*)data.data(), (long)buffSize) )
    {
        return ERR_NONE;
    }
    else
    {
        return ERR_SYS_VEND_CONF_ENCODE_FAIL;
    }
}
/*!
 * \brief servUtility::decodeVendorData
 *	@brief	对数据流进行解密,输入必须按DWORD对齐,只针对DWORD数据进行解密
 * \param data
 * \param newKey
 * \return
 */
DsoErr servUtility::decodeVendorData(QByteArray &data,unsigned int *newKey)
{
    //!4字节对齐后的大小
    int  buffSize = data.size();

    //!需要解密的数据不是4字节对齐，不能解密
    if( 0 != (buffSize%4) )
    {
        return ERR_SYS_VEND_CONF_DECODE_FAIL;
    }

    com_algorithm::CXXTEA::setKeys( (XXTEA_TYPE*)newKey );
    if( 0 == com_algorithm::CXXTEA::decode((XXTEA_TYPE*)data.data(), (long)buffSize))
    {
        return ERR_NONE;
    }
    else
    {
        return ERR_SYS_VEND_CONF_DECODE_FAIL;
    }
}
