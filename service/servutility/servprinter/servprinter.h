#ifndef servPrinter_H
#define servPrinter_H
#include <QProcess>
#include <QTcpSocket>
#include <QHostAddress>
#include <QAbstractSocket>
#include "../../meta/crmeta.h"
#include "../../menu/rmenus.h"
#include "../../service.h"
#include "../../service_name.h"
#include "../../../baseclass/iserial.h"
#include "../../../include/dsotype.h"
#include "../../../include/dsostd.h"


#define PRINTER_DRV_PATH    QString("/rigol/cups/share/cups/model/")

class servPrinter : public serviceExecutor,
                    public ISerial
{
    Q_OBJECT

    DECLARE_CMD()

public:

    //print driver
    enum
    {
        hp_laserjet,
        hp_deskjet,
        epson_9,
        epson_24
    };

    //print status
    enum
    {
        printer_is_idle,
        printer_link_test,
        printer_print_test,
        printer_print_page,
        printer_thread_result
    };

    enum
    {
        cmd_printer_none,
        cmd_printer_menu,
        cmd_printer_net
    };


public:
    servPrinter(QString name, ServiceId id = E_SERVICE_ID_UTILITY_PRINTER );
    ~servPrinter();

    int serialOut(CStream &stream,unsigned char &ver);
    int serialIn(CStream &stream, unsigned char ver);
    void rst();
    void init();
    int startup();
    void registerSpy();

public:
    //!Printering
    DsoErr convertTops();

    DsoErr startPrinting();


    //!Printer range
    DsoErr setRange(bool );
    bool   getRange();

    //!Printer palette
    DsoErr setPrintColor(bool );
    bool   getPrintColor();

    //!Printer invert
    DsoErr setInvert(bool );
    bool   getInvert();


    //!paper size
    DsoErr setPageSize(int );
    int    getPageSize();

    //!Printer copies
    DsoErr setCopies(int );
    int    getCopies();

    DsoErr setVendor(int);
    int    getVendor();

    //!Printer set ip
    DsoErr setServerIP(int );
    int    getServerIP();

    //!Printer set port
    DsoErr setServerPort(int );
    int    getServerPort();

    void   setItemEnable(int,bool);

    //!Printer link test
    DsoErr linkTest();
    DsoErr printTestPage();
    DsoErr setPrinterStatus(int);
    int    getPrinterStatus();

private:
    QThread* getThread();
    DsoErr   setMenuItem(CArgument&);
    DsoErr   setNetStatus(int);
public:
    //!printer paper size
    int m_nPaperSize;

    //!printer copies
    int m_nCopies;

    //!Abort tcp socket
    int    m_nServerPort;
    int    m_nServerIP;

    //!m_bPrinting
    int m_nPrinterCMD;

    //!printer test
    bool m_bTesting;

    //print area. wave or full
    bool m_bRange;

    bool m_bInvert;
    bool m_bColor;

    int  m_nVendor;

    QMap<int,QString> m_driverNames;
private:
    QThread *m_pPrinterThread;
};

class PrinterThread : public QThread
{
    Q_OBJECT
public:
    explicit PrinterThread(QObject *parent = 0);

protected:
    void run();
private:


    //!Abort printer link test
    QString toStringIP(int);

    DsoErr  printFile(int type);
    DsoErr  linkTest();
    DsoErr  printTestPage();
public:
    servPrinter *m_pPrinter;
};
#endif // servPrinter_H

