#include "servprinter.h"
#include "../servgui/servgui.h"
#include "../servstorage/storage_api.h"
#include "../servinterface/servInterface.h"
#include "../service_msg.h"

IMPLEMENT_CMD( serviceExecutor, servPrinter )
start_of_entry()
//!printer-m_bPrintingOrAbort
on_set_int_void(    MSG_PRINTER_START,        &servPrinter::startPrinting),

//!printer-Print range
on_set_int_bool(    MSG_PRINTER_RANGE,        &servPrinter::setRange),
on_get_bool(        MSG_PRINTER_RANGE,        &servPrinter::getRange),
//!printer-Print palette
on_set_int_bool(    MSG_PRINTER_PALETTE,      &servPrinter::setPrintColor),
on_get_bool(        MSG_PRINTER_PALETTE,      &servPrinter::getPrintColor),
//!printer-Print paper size
on_set_int_int(     MSG_PRINTER_PAPER_SIZE,   &servPrinter::setPageSize),
on_get_int(         MSG_PRINTER_PAPER_SIZE,   &servPrinter::getPageSize),
//!printer-Print copies
on_set_int_int(     MSG_PRINTER_COPIES,       &servPrinter::setCopies),
on_get_int(         MSG_PRINTER_COPIES,       &servPrinter::getCopies),
//!printer-Print invert
on_set_int_bool(    MSG_PRINTER_INVERT,       &servPrinter::setInvert),
on_get_bool(        MSG_PRINTER_INVERT,       &servPrinter::getInvert),

on_set_int_int(     MSG_PRINTER_VENDOR,       &servPrinter::setVendor),
on_get_int(         MSG_PRINTER_VENDOR,       &servPrinter::getVendor),

//!printer-Ip
on_set_int_int(     MSG_PRINTER_IP,           &servPrinter::setServerIP),
on_get_int(         MSG_PRINTER_IP,           &servPrinter::getServerIP),
//!printer-Port
on_set_int_int(     MSG_PRINTER_PORT,         &servPrinter::setServerPort),
on_get_int(         MSG_PRINTER_PORT,         &servPrinter::getServerPort),
//!printer-print link test
on_set_int_void(    MSG_PRINTER_LINK_TEST,    &servPrinter::linkTest),
on_set_int_void(    MSG_PRINTER_TEST_PAGE,    &servPrinter::printTestPage),

on_set_int_arg(     servPrinter::cmd_printer_menu,      &servPrinter::setMenuItem),

on_set_int_int (    servPrinter::cmd_printer_net,       &servPrinter::setNetStatus),

on_set_int_int(     CMD_SERVICE_ACTIVE ,                &servPrinter::setActive),
//on_set_void_void(   CMD_SERVICE_RST ,                   &servPrinter::rst),
on_set_void_void(   CMD_SERVICE_INIT,                   &servPrinter::init),
on_set_int_void  (  CMD_SERVICE_STARTUP,                &servPrinter::startup),

//!Scpi command

end_of_entry()



servPrinter::servPrinter(QString name,ServiceId id ) : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();

    m_pPrinterThread = NULL;

    mVersion = 0xA;
}

servPrinter::~servPrinter()
{

}

int servPrinter::serialOut( CStream &stream ,unsigned char &ver)
{
    ver = mVersion;

    stream.write(QStringLiteral("vendor"),      m_nVendor);
    stream.write(QStringLiteral("ip"),    m_nServerIP);
    stream.write(QStringLiteral("port"),  m_nServerPort);
    stream.write(QStringLiteral("copies"),      m_nCopies);
    stream.write(QStringLiteral("size"),   m_nPaperSize);

    return 0;
}

int servPrinter::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("vendor"),      m_nVendor);
        stream.read(QStringLiteral("ip"),    m_nServerIP);
        stream.read(QStringLiteral("port"),  m_nServerPort);
        stream.read(QStringLiteral("copies"),      m_nCopies);
        stream.read(QStringLiteral("size"),   m_nCopies);
    }
    else
    {
        rst();
    }
    return 0;
}

void servPrinter::init()
{
    m_driverNames[hp_laserjet] = "laserjet.ppd";
    m_driverNames[hp_deskjet]  = "deskjet.ppd";
    m_driverNames[epson_9]     = "epson9.ppd";
    m_driverNames[epson_24]    = "epson24.ppd";

    m_nServerIP   = 0xac1003f1;//172.16.3.241
    m_nServerPort = 9100;

    m_nVendor     = hp_laserjet;

    rst();

    setItemEnable(MSG_PRINTER_START, false);
    setItemEnable(MSG_PRINTER_LINK_TEST,false);
    setItemEnable(MSG_PRINTER_TEST_PAGE,false);
}

void servPrinter::rst()
{
    m_nCopies     = 1;
    m_bRange      = false;
    m_nPaperSize  = 3;
    m_bInvert     = true;
    m_bColor      = false;
}

int servPrinter::startup()
{
    m_nPrinterCMD = printer_is_idle;
    updateAllUi( false );

    if(getInvert())
    {
        mUiAttr.setVisible( MSG_PRINTER_PALETTE, false);
    }


    mUiAttr.setVisible(MSG_PRINTER_RANGE, false);
    mUiAttr.setVisible(MSG_PRINTER_PAPER_SIZE, 0, false);
    mUiAttr.setVisible(MSG_PRINTER_PAPER_SIZE, 1, false);
    mUiAttr.setVisible(MSG_PRINTER_PAPER_SIZE, 2, false);
    mUiAttr.setVisible(MSG_PRINTER_PAPER_SIZE, 4, false);

    mUiAttr.setEnable(MSG_PRINTER_PAPER_SIZE, 0, false);
    mUiAttr.setEnable(MSG_PRINTER_PAPER_SIZE, 1, false);
    mUiAttr.setEnable(MSG_PRINTER_PAPER_SIZE, 3, false);
    mUiAttr.setEnable(MSG_PRINTER_PAPER_SIZE, 2, false);
    mUiAttr.setEnable(MSG_PRINTER_PAPER_SIZE, 4, false);


    mUiAttr.setEnable(MSG_PRINTER_PAPER_SIZE, false);
    return 0;
}

void servPrinter::registerSpy()
{
    spyOn(serv_name_utility_IOset,
          servInterface::cmd_net_status,
          servPrinter::cmd_printer_net);
}

DsoErr servPrinter::setRange(bool p)
{
    m_bRange = p;
    return ERR_NONE;
}

bool servPrinter::getRange()
{
    return m_bRange;
}


DsoErr servPrinter::setPageSize(int /*size*/)
{
    m_nPaperSize = 3;

    return ERR_NONE;
}

int servPrinter::getPageSize()
{
    return m_nPaperSize;
}

DsoErr servPrinter::setCopies(int c)
{
    m_nCopies = c;
    return ERR_NONE;
}

int servPrinter::getCopies()
{
    setuiStep( 1 );
    setuiZVal( 1 );
    setuiMinVal(1);
    setuiMaxVal(99);
    setuiUnit(Unit_none);
    setuiBase(1);

    return m_nCopies;
}

DsoErr servPrinter::setInvert(bool b)
{
    m_bInvert = b;
    mUiAttr.setVisible(MSG_PRINTER_PALETTE, !b);
    return ERR_NONE;
}

bool servPrinter::getInvert()
{  
    return m_bInvert;
}


DsoErr servPrinter::setPrintColor(bool b)
{
    m_bColor = b;
    return ERR_NONE;
}

bool servPrinter::getPrintColor()
{
    return m_bColor;
}

DsoErr servPrinter::setVendor(int v)
{
    m_nVendor = v;
    return ERR_NONE;
}

int  servPrinter::getVendor()
{
    return m_nVendor;
}

/*!
 * \brief servPrinter::setServerIP
 * 函数：设置打印机IP地址
 * \param printerIP
 * 参数：菜单控件中的IP地址
 * \return
 */
DsoErr servPrinter::setServerIP(int printerIP)
{
    m_nServerIP = printerIP;

    return ERR_NONE;
}

int servPrinter::getServerIP()
{
    return m_nServerIP;
}

/*!
 * \brief servPrinter::setServerPort
 * 函数：设置打印机端口号
 * \param port
 * 参数：菜单控件中的端口号
 * \return
 */
DsoErr servPrinter::setServerPort(int port)
{
    m_nServerPort = port;
    return ERR_NONE;
}

int servPrinter::getServerPort()
{
    setuiZVal( 9100 );
    setuiMaxVal(65535);
    setuiMinVal(1024);
    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);

    return m_nServerPort;
}

DsoErr servPrinter::setNetStatus(int status)
{
    if( status == servInterface::NET_STATUS_UNLINK)
    {
        setItemEnable(MSG_PRINTER_START, false);
        setItemEnable(MSG_PRINTER_LINK_TEST,false);
        setItemEnable(MSG_PRINTER_TEST_PAGE,false);
    }
    else
    {
        if( m_nPrinterCMD == servPrinter::printer_is_idle)
        {
            setItemEnable(MSG_PRINTER_START, true);
        }
        setItemEnable(MSG_PRINTER_LINK_TEST,true);
        setItemEnable(MSG_PRINTER_TEST_PAGE,true);
    }

    //qDebug()<< "Network Printer:" << status;
    return ERR_NONE;
}

DsoErr servPrinter::setMenuItem(CArgument &arg)
{
    if(arg.size() == 2)
    {
        int msg;
        bool en;
        arg.getVal(msg,0);
        arg.getVal(en,1);
        setItemEnable(msg, en);
    }
    return ERR_NONE;
}

DsoErr servPrinter::setPrinterStatus(int s)
{
    m_nPrinterCMD = s;
    return ERR_NONE;
}

int servPrinter::getPrinterStatus()
{
    return m_nPrinterCMD;
}

QThread* servPrinter::getThread()
{
    //Run Once
    if(m_pPrinterThread == NULL)
    {
        m_pPrinterThread = new PrinterThread(this);
        Q_ASSERT(m_pPrinterThread != NULL);
    }

    return m_pPrinterThread;
}


void servPrinter::setItemEnable(int msg, bool en)
{
    mUiAttr.setEnable(msg, en);
    setuiChange(msg);
}


DsoErr servPrinter::startPrinting()
{

    QThread* pThread = getThread();

    if(!pThread->isRunning() && getPrinterStatus() == printer_is_idle)
    {
        setPrinterStatus(printer_print_page);
        pThread->start();

        setItemEnable(MSG_PRINTER_START, false);
    }
    return ERR_NONE;
}

/*!
 * \brief servPrinter::linkTest
 * 函数：对打印机进行链路测试
 * \return
 */
DsoErr servPrinter::linkTest()
{
    QThread* pThread = getThread();

    if(!pThread->isRunning() && getPrinterStatus() == printer_is_idle)
    {
        setPrinterStatus(printer_link_test);
        pThread->start();

        setItemEnable(MSG_PRINTER_LINK_TEST, false);
    }

    return ERR_NONE;
}


DsoErr servPrinter::printTestPage()
{
    QThread* pThread = getThread();

    if(!pThread->isRunning() && getPrinterStatus() == printer_is_idle)
    {
        setPrinterStatus(printer_print_test);
        pThread->start();

        setItemEnable(MSG_PRINTER_TEST_PAGE, false);
    }
    return ERR_NONE;
}

/***********************************************************
 * Printer pthread function
 ***********************************************************/

PrinterThread::PrinterThread(QObject *parent) : QThread(parent)
{
    m_pPrinter = dynamic_cast<servPrinter*>( parent );
    Q_ASSERT( m_pPrinter != NULL );
}


QString PrinterThread::toStringIP(int ipInt)
{
    QString ip1 = QString::number(((ipInt >> 24)&0xff));
    QString ip2 = QString::number(((ipInt >> 16)&0xff));
    QString ip3 = QString::number(((ipInt >> 8)&0xff));
    QString ip4 = QString::number(ipInt & 0x000000ff);

    QString ipstr;
    ipstr.append(ip1).append(".").append(ip2).append(".").append(ip3).append(".").append(ip4);

    return ipstr;
}
/*!
 * \brief PrinterThread::linkTest
 * 函数：打印机链接测试，使用系统命令ping打印机，
 * 如果能够ping通则说明打印机链路可用，如果不能ping通则打印机链路不可用
 * \return
 */
DsoErr PrinterThread::linkTest()
{
    QString ipstr = toStringIP(m_pPrinter->m_nServerIP);

	//!-w：表示请求回送数据包之间的间隔时间
	//!-c：表示需要发送的回送数据包的数量
    QString cmd = "ping "+ipstr+" -w 2 -c 2";
    QProcess *pro = new QProcess;
    pro->start(cmd);

    pro->waitForFinished();
    QString stout = pro->readAllStandardOutput();
    pro->close();
    delete pro;
    pro = NULL;

    QString ref = "100% packet loss";
    qint32 cnt = stout.indexOf(ref,1);

    if(cnt < 0)
    {
        servGui::showInfo("Network connected");
    }
    else
    {
        servGui::showInfo("Network disconnected");
    }



    CArgument arg;
    arg.setVal((int)MSG_PRINTER_LINK_TEST, 0);
    arg.setVal(true, 1);

    serviceExecutor::post(m_pPrinter->getName(), servPrinter::cmd_printer_menu,arg);

    m_pPrinter->setPrinterStatus(servPrinter::printer_is_idle);
    return ERR_NONE;
}


/*!
 * \brief PrinterThread::Printfile
 * 函数：打印文件，先对文件进行转换，然后将转换后的文件发送给打印机
 */
DsoErr PrinterThread::printFile(int type)
{
    QString printCMD = "/rigol/shell/print_page.sh";
    QStringList args;

    if( type == servPrinter::printer_print_test)
    {
        args << "0";
    }
    else
    {
        args << "1";
    }

    args << QString().setNum(m_pPrinter->m_nCopies);
    if( m_pPrinter->getPageSize() == 4)
    {
        args << "a5";
    }
    else
    {
        args << "a4";
    }
    args << toStringIP(m_pPrinter->m_nServerIP);
    args << QString().setNum(m_pPrinter->m_nServerPort);
    args << m_pPrinter->m_driverNames[m_pPrinter->m_nVendor];

    qDebug() << "PrintTestPage:" << args;

    QString output = "";
    QString msg    = "Now printing...";
    int retCode    = 0;

    QProcess *pProcess = new QProcess;

    Q_ASSERT(pProcess->state() == QProcess::NotRunning);

    int timeout = 20;

    if( type == servPrinter::printer_print_page)
    {
        CArgument fmt;
        fmt.setVal(0);

        //save storage setting
        bool bInvert;
        bool bColor;

        serviceExecutor::query(serv_name_storage, MSG_STORAGE_IMAGE_INVERT, bInvert);
        serviceExecutor::post(serv_name_storage, MSG_STORAGE_IMAGE_INVERT, m_pPrinter->getInvert());

        serviceExecutor::query(serv_name_storage, MSG_STORAGE_IMAGE_COLOR, bColor);
        if( m_pPrinter->getInvert() )
        {
            serviceExecutor::post(serv_name_storage, MSG_STORAGE_IMAGE_COLOR, true);
        }
        else
        {
            serviceExecutor::post(serv_name_storage, MSG_STORAGE_IMAGE_COLOR, m_pPrinter->getPrintColor());
        }
        serviceExecutor::post(serv_name_storage, MSG_STORAGE_PRNTSCR, fmt);
        QThread::sleep(1);

        if(!QFile::exists("/tmp/snap.bmp"))
        {
            qDebug() << "print error";
        }

        serviceExecutor::post(serv_name_storage, MSG_STORAGE_IMAGE_INVERT, bInvert);
        serviceExecutor::post(serv_name_storage, MSG_STORAGE_IMAGE_COLOR, bColor);
    }

    servFile::initProgress(msg,QString("Printer"),20);
    QThread::sleep(1);

    pProcess->start(printCMD, args);


    while( pProcess->state() )
    {
        pProcess->waitForFinished(500);
        output = pProcess->readAllStandardOutput();
        if( output.size() > 0 )
        {
            output.remove("\n");
            //qDebug() << output;

            if(pProcess->state() == QProcess::NotRunning &&
               pProcess->exitCode() != 0)
            {
                break;
            }
        }

        if( servFile::getStep() >= timeout)
        {
            servFile::setProgress(timeout, msg);
        }
        else
        {
            servFile::setProgress(-1, msg);
        }
    }

    retCode = pProcess->exitCode();
    switch(retCode)
    {
        case 1:
            servGui::showInfo("Network disconnected");
            break;

        case 2:
            servGui::showInfo("Printer is busy");
            break;
    }

    servFile::hideProgress();


    m_pPrinter->setPrinterStatus(servPrinter::printer_is_idle);

    //qDebug() << result;
    pProcess->close();
    pProcess->kill();
    delete pProcess;


    return ERR_NONE;
}

void PrinterThread::run()
{
    switch( m_pPrinter->m_nPrinterCMD)
    {
        case servPrinter::printer_link_test:
             linkTest();
             break;

        case servPrinter::printer_print_test:
        {
             printFile(servPrinter::printer_print_test);
             CArgument arg;
             arg.setVal((int)MSG_PRINTER_TEST_PAGE, 0);
             arg.setVal(true, 1);

             serviceExecutor::post(m_pPrinter->getName(), servPrinter::cmd_printer_menu,arg);

        }
        break;

        case servPrinter::printer_print_page:
        {
             printFile(servPrinter::printer_print_page);

             CArgument arg;
             arg.setVal((int)MSG_PRINTER_START, 0);
             arg.setVal(true, 1);

             serviceExecutor::post(m_pPrinter->getName(), servPrinter::cmd_printer_menu,arg);
        }
        break;
    }
}
