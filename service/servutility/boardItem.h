#ifndef BOARD_ITEM_TEST_H
#define BOARD_ITEM_TEST_H



struct stBoardTestItem
{
    stBoardTestItem(QString name,
                    int addr,
                    int chan,
                    double r,
                    double lower,
                    double upper,
                    int u = Unit_V)
    {
        testName = name;
        address  = addr;
        channel  = chan;
        ratio    = r;
        min      = lower;
        max      = upper;
        value    = 0;
        is_ok    = false;
        unit     = u;
    }

    stBoardTestItem(QString name,
                    int addr,
                    int chan,
                    QString lower,
                    QString upper)
    {
            testName = name;
            address  = addr;
            channel  = chan;
            _lower   = lower;
            _upper   = upper;
            is_ok    = false;
            unit     = Unit_none;
    }

    void setTestValue(QString v)
    {
        _curr = v;
        is_ok = true;
    }

    void setTestValue(double v)
    {
        value = v;
        is_ok = true;
    }

    QString  testName;
    int      address;
    int      channel;

    QString  _curr;
    QString  _lower;
    QString  _upper;

    double   ratio;
    double   min;
    double   max;
    double   value;
    bool     is_ok;
    int      unit;
};


#endif // DEV_TEST_H

