#ifndef SERV_EMAIL_H
#define SERV_EMAIL_H

#include "../../service.h"
#include "../../service_name.h"
#include "../../../baseclass/iserial.h"
#include <QProcess>

#define MAIL_MSMTP   QString("/rigol/mail/bin/msmtp")
#define MAIL_MUTT_RC QString("/rigol/mail/etc/Muttrc")
#define MAIL_SMTP_RC QString("/rigol/mail/etc/msmtprc")

class servMail  : public serviceExecutor,
                  public ISerial
{
    Q_OBJECT

    DECLARE_CMD()
public:

    enum
    {
        mail_content_screen,
        mail_content_wave,
        mail_content_setup,
        mail_content_select
    };

    enum
    {
        cmd_mail_none,
        cmd_mail_stmp,
        cmd_mail_port,
        cmd_mail_user,
        cmd_mail_pass,
        cmd_mail_net,
        cmd_mail_menu,
        cmd_mail_select,
    };

    enum
    {
        mail_is_idle,
        mail_test_smtp,
        mail_send
    };

    servMail(QString name, ServiceId id = E_SERVICE_ID_UTILITY_EMAIL );
    ~servMail();

    int  serialOut(CStream &stream,unsigned char &ver);
    int  serialIn(CStream &stream, unsigned char ver);
    void rst();
    void init();
    int  startup();
    void registerSpy();

public:

    //!Email pthread option class
    QThread *m_pMailThread;
    int      m_nMailProc;

public:
    int     setSMTP(QString&);
    QString getSMTP();

    int     setSMTPPort(int);
    int     getSMTPPort();

    int     setUserName(QString&);
    QString getUserName();

    int     setPassword(QString&);
    QString getPassword();

    int     setReceiver(QString&);
    QString getReceiver();


    int     setAttachment(int);
    int     getAttachment();

    int     setAttachmentUser(QString&);
    QString getAttachmentUser();

    int     selectAttachment();

public:
    bool    getActive();

    int     onDeActive(int);

    int     onSubMenu(int);
    int     getSubMenu();
    int     onSubReturn(int);

    int     doApply();
    int     doDefault();
    int     doTest();
    int     doSend();

    int     doNull(int);

    void   setItemEnable(int,bool);
private:
    QThread* getThread();
    DsoErr   setMenuItem(CArgument&);
    DsoErr   setNetStatus(int);
private:
    QString m_SMTP;
    int     m_SMTPPort;
    QString m_UserName;
    QString m_Password;
    QString m_Receiver;
    int     m_nAttachment;
    QString m_AttachmentUser;

    int     m_nSubMenu;//0-Main,1-Sub
};


class MailThread : public QThread
{
    Q_OBJECT
public:
    explicit MailThread(QObject *parent = 0);

public:
    servMail *m_pEmail;

protected:
    void run();

private:
    void TestSMTP();
    void SendMail();

};

#endif // EMAIL_H

