#include "../service_msg.h"
#include "../../service/servstorage/servstorage.h"
#include "../servinterface/servInterface.h"

#include "servmail.h"

IMPLEMENT_CMD( serviceExecutor, servMail )
start_of_entry()



on_set_int_void  (  CMD_SERVICE_STARTUP,         &servMail::startup),
on_set_void_void (  CMD_SERVICE_INIT,            &servMail::init),
//on_set_void_void (  CMD_SERVICE_RST ,            &servMail::rst),

on_set_int_int   (  CMD_SERVICE_SUB_ENTER,       &servMail::onSubMenu),
on_get_int       (  CMD_SERVICE_SUB_ENTER,       &servMail::getSubMenu),

on_set_int_int   (  CMD_SERVICE_SUB_RETURN,      &servMail::onSubReturn),

on_set_int_int   (  CMD_SERVICE_DEACTIVE,        &servMail::onDeActive),



on_set_int_string(  MSG_EMAIL_RECEIVER,          &servMail::setReceiver),
on_get_string    (  MSG_EMAIL_RECEIVER,          &servMail::getReceiver),

on_set_int_int   (  MSG_EMAIL_ATTACHMENT,        &servMail::setAttachment),
on_get_int       (  MSG_EMAIL_ATTACHMENT,        &servMail::getAttachment),

on_set_int_void  (  MSG_EMAIL_ATTACHMENT_USER,   &servMail::selectAttachment),
on_set_int_void  (  MSG_EMAIL_SEND,              &servMail::doSend),
on_set_int_void  (  MSG_EMAIL_TEST,              &servMail::doTest),
on_set_int_void  (  MSG_EMAIL_APPLY,             &servMail::doApply),
on_set_int_void  (  MSG_EMAIL_DEFAULT,           &servMail::doDefault),

on_set_int_int   (  MSG_EMAIL_SMTP_IP,           &servMail::doNull),
on_set_int_int   (  MSG_EMAIL_SMTP_PORT,         &servMail::doNull),
on_set_int_int   (  MSG_EMAIL_USERNAME,          &servMail::doNull),
on_set_int_int   (  MSG_EMAIL_PASSWORD,          &servMail::doNull),

on_set_int_string(  servMail::cmd_mail_stmp,     &servMail::setSMTP),
on_get_string    (  servMail::cmd_mail_stmp,     &servMail::getSMTP),

on_set_int_int   (  servMail::cmd_mail_port,     &servMail::setSMTPPort),
on_get_int       (  servMail::cmd_mail_port,     &servMail::getSMTPPort),

on_set_int_string(  servMail::cmd_mail_user,     &servMail::setUserName),
on_get_string    (  servMail::cmd_mail_user,     &servMail::getUserName),

on_set_int_string(  servMail::cmd_mail_pass,     &servMail::setPassword),
on_get_string    (  servMail::cmd_mail_pass,     &servMail::getPassword),

on_set_int_arg   (  servMail::cmd_mail_menu,     &servMail::setMenuItem),

//select attachment
on_set_int_string(  servMail::cmd_mail_select,   &servMail::setAttachmentUser),
on_get_string    (  servMail::cmd_mail_select,   &servMail::getAttachmentUser),

end_of_entry()

servMail::servMail(QString name,ServiceId id ) : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();
    mVersion = 0xa;

    m_pMailThread = NULL;
}

servMail::~servMail()
{

}


int servMail::serialOut(CStream &stream,unsigned char &ver)
{
    ver = mVersion;

    stream.write(QStringLiteral("smtp"),      m_SMTP);
    stream.write(QStringLiteral("port"),    m_SMTPPort);
    stream.write(QStringLiteral("name"),    m_UserName);
    stream.write(QStringLiteral("pwd"),    m_Password);
    stream.write(QStringLiteral("recv"),    m_Receiver);
    stream.write(QStringLiteral("attachment"), m_nAttachment);
    stream.write(QStringLiteral("attachment_user"), m_AttachmentUser);
    return ERR_NONE;
}

int servMail::serialIn(CStream &stream, unsigned char ver)
{
    if(ver == mVersion)
    {
        stream.read(QStringLiteral("smtp"),      m_SMTP);
        stream.read(QStringLiteral("port"),    m_SMTPPort);
        stream.read(QStringLiteral("name"),    m_UserName);
        stream.read(QStringLiteral("pwd"),    m_Password);
        stream.read(QStringLiteral("recv"),    m_Receiver);
        stream.read(QStringLiteral("attachment"), m_nAttachment);
        stream.read(QStringLiteral("attachment_user"), m_AttachmentUser);
    }
    else
    {
        rst();
    }
    return ERR_NONE;
}

void servMail::rst()
{
     m_SMTP     = "mail.rigol.com";
     m_SMTPPort = 25;
     m_UserName = "rigol_ds@rigol.com";
     m_Password = "Rigol0614";
     m_nAttachment=mail_content_screen;
     m_Receiver = "rigolmail@sina.com";

//     mUiAttr.setEnable(MSG_EMAIL_SEND, false);
}

void servMail::init()
{
    mUiAttr.setVisible(MSG_EMAIL_ATTACHMENT,mail_content_wave, false);
    //mUiAttr.setVisible(MSG_EMAIL_ATTACHMENT,2, false);
    m_nSubMenu = 0;

    rst();
}

int servMail::startup()
{
    m_nMailProc = mail_is_idle;
    updateAllUi( false );

    mUiAttr.setEnable(MSG_EMAIL_SEND, m_Receiver.size()>0);
    return 0;
}

void servMail::registerSpy()
{

}

int servMail::onSubMenu(int msg)
{
    if(msg == MSG_EMAIL_SETTING )
    {
        m_nSubMenu = 1;
    }
    else
    {
        m_nSubMenu = 0;
    }
    return ERR_NONE;
}

int servMail::getSubMenu()
{
    return m_nSubMenu;
}

int servMail::onSubReturn(int)
{
    m_nSubMenu = 0;
    return ERR_NONE;
}

int servMail::onDeActive(int)
{
    setdeActive();
   return ERR_NONE;
}

int servMail::setReceiver(QString& r)
{
    //DO check
    m_Receiver = r;
    mUiAttr.setEnable(MSG_EMAIL_SEND, r.size() > 0);
    return ERR_NONE;
}

QString servMail::getReceiver()
{
    setuiMaxLength(32);
    return m_Receiver;
}

int servMail::setAttachment(int a)
{
    m_nAttachment = a;
    return ERR_NONE;
}

int servMail::getAttachment()
{
    return m_nAttachment;
}

int servMail::setAttachmentUser(QString& a)
{
    m_AttachmentUser = a;
    return ERR_NONE;
}

QString servMail::getAttachmentUser()
{
    return m_AttachmentUser;
}


int servMail::setSMTP(QString& ip)
{
    m_SMTP = ip;
    return ERR_NONE;
}

QString servMail::getSMTP()
{
    return m_SMTP;
}

int servMail::setSMTPPort(int p)
{
    m_SMTPPort = p;
    return ERR_NONE;
}

int servMail::getSMTPPort()
{
    return m_SMTPPort;
}

int servMail::setUserName(QString& u)
{
    m_UserName = u;
    return ERR_NONE;
}

QString servMail::getUserName()
{
    return m_UserName;
}

int servMail::setPassword(QString& p)
{
    m_Password = p;
    return ERR_NONE;
}

QString servMail::getPassword()
{
    return m_Password;
}



void servMail::setItemEnable(int msg, bool en)
{
    mUiAttr.setEnable(msg, en);
    setuiChange(msg);
}

DsoErr servMail::setNetStatus(int status)
{
    if( status == servInterface::NET_STATUS_UNLINK)
    {
        setItemEnable(MSG_EMAIL_SEND, false);
    }
    else
    {
        if( m_nMailProc == servMail::mail_is_idle && m_Receiver.size() > 0)
        {
            setItemEnable(MSG_EMAIL_SEND, true);
        }
    }

    //qDebug()<< "Network Printer:" << status;
    return ERR_NONE;
}

DsoErr servMail::setMenuItem(CArgument &arg)
{
    if(arg.size() == 2)
    {
        int msg;
        bool en;
        arg.getVal(msg,0);
        arg.getVal(en,1);
        setItemEnable(msg, en);
    }
    return ERR_NONE;
}

QThread* servMail::getThread()
{
    //Run Once
    if(m_pMailThread == NULL)
    {
        m_pMailThread = new MailThread(this);
        Q_ASSERT(m_pMailThread != NULL);
    }

    return m_pMailThread;
}
int servMail::doTest()
{
    QThread* pThread = getThread();

    if(!pThread->isRunning() && m_nMailProc == mail_is_idle)
    {
        m_nMailProc = mail_test_smtp;
        pThread->start();

        setItemEnable(MSG_EMAIL_TEST, false);
    }
    return ERR_NONE;
}

int servMail::doSend()
{
    QThread* pThread = getThread();

    if(!pThread->isRunning() && m_nMailProc == mail_is_idle)
    {
        m_nMailProc = mail_send;
        pThread->start();

        setItemEnable(MSG_EMAIL_SEND, false);
    }
    return ERR_NONE;
}

int servMail::doApply()
{
    /**********************************************************
     *   save Mutt rc
     * *******************************************************/
    QFile f(MAIL_MUTT_RC);
    if(!f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        return ERR_FILE_OP_FAIL;
    }
    QTextStream txtOutput(&f);

    QString s2 = "set from=\"" + m_UserName +"\"";
    QString s3 = "set realname=\"RIGOL\"";
    QString s4 = "set sendmail=\""+ MAIL_MSMTP+ "\"";
    QString s5 = "set use_from=yes";


    txtOutput << s2 << "\n";
    txtOutput << s3 << "\n";
    txtOutput << s4 << "\n";
    txtOutput << s5 << "\n";

    f.close();

    /**********************************************************
     *   save smtp rc
     * *******************************************************/

    QFile f2(MAIL_SMTP_RC);
    if(!f2.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        return ERR_FILE_OP_FAIL;
    }
    QTextStream smtprc(&f2);


    smtprc << "host " + m_SMTP << "\n";
    smtprc << "port " + QString("%1").arg(m_SMTPPort) << "\n";

    smtprc << "from " + m_UserName << "\n";
    smtprc << "user " + m_UserName << "\n";
    smtprc << "password " + m_Password << "\n";
    smtprc << "auth login" << "\n";
    smtprc << "tls off" << "\n";


    f2.close();

    return ERR_NONE;
}

int servMail::doDefault()
{
    rst();
    setItemEnable(MSG_EMAIL_SEND, true);
    return ERR_NONE;
}

int servMail::doNull(int)
{
    return ERR_NONE;
}

/*!
 * \brief servMail::set_Attachment
 * 函数：添加邮件附件，此处调用storage中的加载功能，通过该功能浏览文件，选择需要加载的附件，
 * 通过响应storage中发出的cmd_mail_attachment消息获得附件路径
 * \return
 */
int servMail::selectAttachment(void)
{
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_LOAD,
                      servStorage::FUNC_LOAD_ATT,
                      1,
                      getId());

    startIntent( serv_name_storage, intent );

    return ERR_NONE;
}

//int servMail::getAttachment()
//{
//    return ERR_NONE;
//}

/***********************************************************
 * Printer pthread function
 ***********************************************************/

MailThread::MailThread(QObject *parent) : QThread(parent)
{
    m_pEmail = dynamic_cast<servMail*>( parent );
    Q_ASSERT( m_pEmail != NULL );
}


void MailThread::TestSMTP()
{
    QProcess process;
    QString cmd = MAIL_MSMTP;
    QStringList argList;
    argList << QString("--host=%1").arg(m_pEmail->getSMTP());
    argList << QString("--port=%1").arg(m_pEmail->getSMTPPort());
    argList << "--serverinfo";

    process.start(cmd,argList);
    QThread::sleep(1);
    process.waitForFinished(10000);
    QString output = process.readAllStandardOutput();

    //qDebug() << argList << output;
    if( output.contains("Capabilities") )
    {
        servGui::showInfo(sysGetString(INF_MAIL_TEST_OK,
                                       "STMP and port test OK"));
    }
    else
    {
        servGui::showInfo(sysGetString(INF_MAIL_TEST_FAIL,
                                       "Cannot connect to the SMTP"));
    }


    CArgument arg;
    arg.setVal((int)MSG_EMAIL_TEST, 0);
    arg.setVal(true, 1);

    serviceExecutor::post(m_pEmail->getName(), servMail::cmd_mail_menu,arg);

    m_pEmail->m_nMailProc = servMail::mail_is_idle;
}


void MailThread::SendMail()
{
    QString     sendMail = "/rigol/shell/send_mail.sh";
    QStringList args;
    int         att = m_pEmail->getAttachment();

    CArgument argMenu;
    argMenu.setVal((int)MSG_EMAIL_SEND, 0);
    argMenu.setVal(true, 1);

    args << m_pEmail->getReceiver();
    if( att == servMail::mail_content_select)
    {
        args << m_pEmail->getAttachmentUser();
    }
    else if(att == servMail::mail_content_setup)
    {
        CArgument inputPara;
        QString setupFile = "/tmp/setup.stp";
        inputPara.setVal(setupFile);
        serviceExecutor::post(serv_name_storage,
                              servStorage::FUNC_SAVE_STP,
                              inputPara);
        QThread::sleep(1);
        if(!QFile::exists(setupFile))
        {
            serviceExecutor::post(m_pEmail->getName(),
                                  servMail::cmd_mail_menu,
                                  argMenu);
            //qDebug() << "send error";
            return;
        }
        //qDebug() << "Mail attachment:" << setupFile;
        args << setupFile;
    }
    else if(att == servMail::mail_content_screen)
    {
        int fmt = 0;
        QString imgPath = "/tmp/snap.bmp";
        serviceExecutor::query(serv_name_storage,
                               MSG_STORAGE_IMAGE_FORMAT, fmt);

        CArgument inputPara,outPara;
        inputPara.setVal(0);
        if( fmt >=0 && fmt < 4)
        {
            inputPara.setVal( fmt );
        }
        serviceExecutor::query(serv_name_storage,
                               MSG_STORAGE_PRNTSCR,
                               inputPara, outPara);
        QThread::sleep(1);

        outPara.getVal(imgPath);

        if(!QFile::exists(imgPath))
        {
            serviceExecutor::post(m_pEmail->getName(),
                                  servMail::cmd_mail_menu,
                                  argMenu);
            //qDebug() << "send error";
            return;
        }
        //qDebug() << "Mail attachment:" << imgPath;

        args << imgPath;
    }


    QString output = "";
    QString msg    = "Sending...";
    int retCode    = 0;
    int timeout = 50;

    servFile::initProgress(msg,QString("Mail"),timeout);
    QThread::msleep(10);


    QProcess *pProcess = new QProcess;
    Q_ASSERT(pProcess->state() == QProcess::NotRunning);



    //qDebug() << args;
    pProcess->start(sendMail, args);


    while( pProcess->state() )
    {
        pProcess->waitForFinished(500);
        output = pProcess->readAllStandardOutput();
        if( output.size() > 0 )
        {
            output.remove("\n");
            //qDebug() << output;

            if(pProcess->state() == QProcess::NotRunning &&
               pProcess->exitCode() != 0)
            {
                break;
            }
        }

        if( servFile::getStep() >= timeout)
        {
            servFile::setProgress(timeout, msg);
            break;
        }
        else
        {
            servFile::setProgress(-1, msg);
        }
    }

    retCode = pProcess->exitCode();
    switch(retCode)
    {
        case 1:
            servGui::showInfo("Sending failed");
            break;

        case 2:
            servGui::showInfo("Network is busy");
            break;

        case 0:
            servGui::showInfo("Sending OK");
            break;
    }

    servFile::hideProgress();
    m_pEmail->m_nMailProc = servMail::mail_is_idle;



    serviceExecutor::post(m_pEmail->getName(),
                          servMail::cmd_mail_menu,
                          argMenu);

    pProcess->close();
    pProcess->kill();
    delete pProcess;
}

void MailThread::run()
{
    switch( m_pEmail->m_nMailProc )
    {
        case servMail::mail_test_smtp:
             TestSMTP();
             break;

        case servMail::mail_send:
             SendMail();
             break;
    }

}
