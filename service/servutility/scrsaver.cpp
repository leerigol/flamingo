#include "servutility.h"
#include "../servstorage/servstorage.h"


/*!
 * \brief servUtility::setScrSaver
 * \param m_bScrSaver
 * \return
 */
DsoErr servUtility::setScrSaver(int b)
{
    if( b == servUtility::scr_saver_off)
    {
        stopTimer();
    }
    else
    {
        setSaverTmo();
    }
    m_nScrSaver = b;
    return ERR_NONE;
}
/*!
 * \brief servUtility::getScrSaver
 * \return
 */
int servUtility::getScrSaver()
{
    return m_nScrSaver;
}

/*!
 * \brief servUtility::set_ScreenPreview
 * \return
 */
DsoErr servUtility::setScrPreview()
{
    m_bPreview = !m_bPreview;
    return ERR_NONE;
}

bool servUtility::getScrPreview()
{
    return m_bPreview;
}
///
/// \brief Any settings operation can call this for reset the timer
/// \return
///
DsoErr servUtility::setSaverTmo()
{
    if( m_nScrSaver != scr_saver_off)
    {
        startTimer(m_nWaitTime * 1000*60,0,timer_repeat);
    }
    return ERR_NONE;
}

/*!
 * \brief servUtility::set_scrsavpicture
 * \return
 */
DsoErr servUtility::selectPicture()
{
    CIntent intent;

    intent.setIntent( MSG_STORAGE_SUB_LOAD,
                      servStorage::FUNC_LOAD_SCR,
                      1,
                      getId());

    startIntent( serv_name_storage, intent );

    return ERR_NONE;
}

DsoErr servUtility::setPicturePath(QString p)
{
    m_picPath = p;
    //qDebug() << m_picPath << __FUNCTION__;
    return ERR_NONE;
}

QString servUtility::getPicturePath()
{
    return m_picPath;
}

DsoErr servUtility::setWaitTime(int time)
{
    m_nWaitTime = time;
    return ERR_NONE;
}

int servUtility::getWaitTime()
{    
    setuiStep( 1 );
    setuiZVal( 1 );
    setuiMinVal(1);
    setuiMaxVal(999);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);

    setuiPostStr("min");
    //qDebug() << __FUNCTION__ << m_nWaitTime;
    return m_nWaitTime;
}

DsoErr servUtility::setScpiWaitTime(QString time)
{
    if(time.compare("off",Qt::CaseInsensitive) == 0)
    {
        async(MSG_APP_UTILITY_SCREEN_SELECT, 0);      // OFF
    }
    else
    {
        if( m_nScrSaver == scr_saver_off)
        {
            async(MSG_APP_UTILITY_SCREEN_SELECT, 1);
        }

        int tmo = time.toInt();
        if( tmo > 999 )
        {
             tmo = 999;
        }
        if( tmo < 1 )
        {
            tmo = 1;
        }

        async(MSG_APP_UTILITY_SCREEN_TIME, tmo );
    }

    return ERR_NONE;
}

QString  servUtility::getScpiWaitTime()
{
    if( m_nScrSaver == scr_saver_off)
    {
        return "OFF";
    }
    else
    {
        return QString::number(m_nWaitTime);
    }
}

DsoErr servUtility::setTextContent(QString b)
{
    m_strText = b;
    return ERR_NONE;
}

QString servUtility::getTextContent()
{
    setuiMaxLength(20);
    return m_strText;
}

int servUtility::setScrStart()
{
    m_bScrShow = !m_bScrShow;
    return ERR_NONE;
}

bool servUtility::getScrStart()
{
    return m_bScrShow;
}

int servUtility::onScrStop()
{
    m_bPreview = false;
    m_bScrShow = false;
    return ERR_NONE;
}

int servUtility::onTimeout(int)
{
    if( m_nScrSaver != servUtility::scr_saver_off)
    {
        stopTimer();
        defer(servUtility::cmd_dso_saver_start);
    }
    return ERR_NONE;
}

int servUtility::onDefaultScr(int)
{
    if( m_nScrSaver == servUtility::scr_saver_pic)
    {
        m_picPath   = DEFAULT_SCR_PIC;
    }
    else if(m_nScrSaver == servUtility::scr_saver_txt)
    {
        m_strText   = DEFAULT_SCR_TXT;
        setuiChange(MSG_APP_UTILITY_SCREEN_WORD);
    }
    m_nWaitTime     = 30;
    setuiChange(MSG_APP_UTILITY_SCREEN_TIME);

    return ERR_NONE;
}
