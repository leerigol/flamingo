
#include "../../service/service_name.h"
#include "../../baseclass/dsovert.h"
#include "servvertmgr.h"
#include "../servvert/servvert.h"
#include "../../include/dsodbg.h"

IMPLEMENT_CMD( serviceExecutor, servVertMgr )
start_of_entry()

on_set_int_int( servVertMgr::cmd_active_chan, &servVertMgr::setActiveChan ),
on_get_int    ( servVertMgr::cmd_active_chan, &servVertMgr::getActiveChan ),

on_set_int_int( servVertMgr::cmd_open_source, &servVertMgr::openSource ),

on_set_int_int( servVertMgr::cmd_chan1_button, &servVertMgr::onCH1ButtonClicked ),
on_set_int_int( servVertMgr::cmd_chan2_button, &servVertMgr::onCH2ButtonClicked ),
on_set_int_int( servVertMgr::cmd_chan3_button, &servVertMgr::onCH3ButtonClicked ),
on_set_int_int( servVertMgr::cmd_chan4_button, &servVertMgr::onCH4ButtonClicked ),

on_set_int_int( servVertMgr::cmd_la_button, &servVertMgr::onLaButtonClicked ),

on_set_int_int( servVertMgr::cmd_chx_enter_active, &servVertMgr::on_enterActive),

//! system msg
on_set_void_void( CMD_SERVICE_RST,        &servVertMgr::rst ),
on_set_int_void ( CMD_SERVICE_SERVICE_UP, &servVertMgr::serviceup ),
on_set_void_void( CMD_SERVICE_INIT,       &servVertMgr::init ),

end_of_entry()

servVertMgr::servVertMgr( QString name, ServiceId eId )
                   : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();
}

void servVertMgr::registerSpy()
{
    //! active
    spyOn( serv_name_ch1, CMD_SERVICE_ENTER_ACTIVE,
           servVertMgr::cmd_chx_enter_active );
    spyOn( serv_name_ch2, CMD_SERVICE_ENTER_ACTIVE,
           servVertMgr::cmd_chx_enter_active );
    spyOn( serv_name_ch3, CMD_SERVICE_ENTER_ACTIVE,
           servVertMgr::cmd_chx_enter_active );
    spyOn( serv_name_ch4, CMD_SERVICE_ENTER_ACTIVE,
           servVertMgr::cmd_chx_enter_active );

    spyOn( serv_name_la, CMD_SERVICE_ENTER_ACTIVE,
           servVertMgr::cmd_chx_enter_active );

//    //! math 和 ref 目前不需要配置图层顺序
//    //! math
//    spyOn( serv_name_math1, CMD_SERVICE_ENTER_ACTIVE,
//           servVertMgr::cmd_chx_enter_active );
//    spyOn( serv_name_math2, CMD_SERVICE_ENTER_ACTIVE,
//           servVertMgr::cmd_chx_enter_active );
//    spyOn( serv_name_math3, CMD_SERVICE_ENTER_ACTIVE,
//           servVertMgr::cmd_chx_enter_active );
//    spyOn( serv_name_math4, CMD_SERVICE_ENTER_ACTIVE,
//           servVertMgr::cmd_chx_enter_active );

//    //! ref
//    spyOn( serv_name_ref, CMD_SERVICE_ENTER_ACTIVE,
//           servVertMgr::cmd_chx_enter_active );
}

#define ar ar_out
#define ar_pack_e ar_out
int servVertMgr::serialOut( CStream &stream, serialVersion &ver )
{
    ISerial::serialOut( stream, ver );

    ar_pack_e( "active", mActiveChan );
    return 0;
}
#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servVertMgr::serialIn( CStream &stream, serialVersion ver )
{
    ISerial::serialIn( stream, ver );

    ar_pack_e( "active", mActiveChan );

    return 0;
}
void servVertMgr::rst()
{
    mActiveChan = chan1;
}
int servVertMgr::startup()
{ return 0; }
int servVertMgr::serviceup()
{
    //qDebug()<<__FUNCTION__<<__LINE__<<mActiveChan;
    async( cmd_active_chan, mActiveChan );

    return 0;
}

DsoErr servVertMgr::setActiveChan( Chan ch )
{
    if( ((ch >= m1)&&(ch <= m4)) || ch == ref)
    {
        //! math 通道和 ref通道不需要修改图层顺序
        return ERR_NONE;
    }

    dsoVert::setTop( ch );

    //! scan
    int chCnt = dsoVert::getVertSize();
    Chan chPos[ chCnt ];
    dsoVert::scanCHOrder( chPos, chCnt );

    LOG_DBG()<<ch;

    //! post

    static Chan chBefore = chan_none;
    if(ch == chBefore)
    {
        return ERR_NONE;
    }
    else
    {
        chBefore = ch;
    }
    postEngine( ENGINE_DISP_CH_ORDERS, chCnt, chPos );

    return ERR_NONE;
}

Chan servVertMgr::getActiveChan()
{
    Chan act = dsoVert::getActiveChan();

//    LOG_DBG()<<act;

    return act;
}

DsoErr servVertMgr::openSource(Chan ch)
{
    if( ch >= chan1 && ch <= chan4 )
    {
        serviceExecutor::post( E_SERVICE_ID_CH1 + ch-chan1,
                               MSG_CHAN_ON_OFF,
                               true);
    }
    else if( ch >= m1 && ch <= m4 )
    {
        serviceExecutor::post( E_SERVICE_ID_MATH1 + ch-m1,
                               MSG_MATH_EN,
                               true);
    }
    else if( ch >= d0 && ch <= d15 )
    {
        serviceExecutor::post( E_SERVICE_ID_LA,
                               MSG_LA_ENABLE,
                               true);
    }

    return ERR_NONE;
}

DsoErr servVertMgr::on_enterActive( ServiceId id )
{
    LOG_DBG()<<id;
    switch( id )
    {
    case E_SERVICE_ID_CH1:
        ssync( cmd_active_chan, chan1 );
        break;

    case E_SERVICE_ID_CH2:
        ssync( cmd_active_chan, chan2 );
        break;

    case E_SERVICE_ID_CH3:
        ssync( cmd_active_chan, chan3 );
        break;

    case E_SERVICE_ID_CH4:
        ssync( cmd_active_chan, chan4 );
        break;

    case E_SERVICE_ID_LA:
        ssync( cmd_active_chan, la );
        break;

//    //! math
//    case E_SERVICE_ID_MATH1:
//        async( cmd_active_chan, m1 );
//        break;

//    case E_SERVICE_ID_MATH2:
//        async( cmd_active_chan, m2 );
//        break;

//    case E_SERVICE_ID_MATH3:
//        async( cmd_active_chan, m3 );
//        break;

//    case E_SERVICE_ID_MATH4:
//        async( cmd_active_chan, m4 );
//        break;

//    //! ref
//    case E_SERVICE_ID_REF:
//        async( cmd_active_chan, ref );
//        break;

    default:
        break;
    }

    return ERR_NONE;
}

DsoErr servVertMgr::onCH1ButtonClicked()
{
    return onCHxButtonClicked(serv_name_ch1, chan1);
}

DsoErr servVertMgr::onCH2ButtonClicked()
{
    return onCHxButtonClicked(serv_name_ch2, chan2);
}

DsoErr servVertMgr::onCH3ButtonClicked()
{
    return onCHxButtonClicked(serv_name_ch3, chan3);
}

DsoErr servVertMgr::onCH4ButtonClicked()
{
    return onCHxButtonClicked(serv_name_ch4, chan4);
}

DsoErr servVertMgr::onLaButtonClicked()
{
    bool bNow;
    bNow = sysCheckLicense(OPT_MSO);
    if(!bNow)
    {
        return ERR_ACTION_DISABLED;
    }
    //! msg filter
    if ( msgFilter( serv_name_la, MSG_LA_ENABLE) )
    {
        serviceExecutor::post( serv_name_la, MSG_LA_ENABLE, 1 );
        return ERR_NONE;
    }

    bool bEn;
    DsoErr err;

    err = query( serv_name_la, MSG_LA_ENABLE, bEn );
    if ( err != ERR_NONE ) return err;

    //! on now
    if ( bEn )
    {
        //! to off
        if ( serviceExecutor::queryActive(serv_name_la) )
        {
            serviceExecutor::post( serv_name_la, MSG_LA_ENABLE, false );
        }
        //! to active
        else
        {
            serviceExecutor::post( serv_name_la, CMD_SERVICE_ACTIVE, 1 );
        }
    }
    //! not on
    else
    {
        serviceExecutor::post( serv_name_la, MSG_LA_ENABLE, true );
    }

    return ERR_NONE;
}

DsoErr servVertMgr::onCHxButtonClicked( const QString &name,
                           Chan /*chId*/ )
{
    //! msg filter
    if ( msgFilter( name, MSG_CHAN_ON_OFF) )
    {
        serviceExecutor::send( name, CMD_SERVICE_ACTIVE, 1 );
        return ERR_NONE;
    }

    bool bEn;
    DsoErr err;

    err = query( name, MSG_CHAN_ON_OFF, bEn );
    if ( err != ERR_NONE ) return err;

    //! on now
    if ( bEn )
    {
        //! to off
        if ( serviceExecutor::queryActive(name) )
        {
            serviceExecutor::post( name, MSG_CHAN_ON_OFF, false );
        }
        //! to active
        else
        {
            serviceExecutor::post( name, CMD_SERVICE_ACTIVE, 1 );
        }
    }
    else
    {   
        //add for bug2589 by hxh
        serviceExecutor::post( name, MSG_CHAN_ON_OFF, true );

        //set scale and offset when from OFF to ON
        //for kestrel by hxh
        int scale=0;
        serviceExecutor::query(name, MSG_CHAN_SCALE_VALUE, scale );
        serviceExecutor::post(name, MSG_CHAN_SCALE_VALUE, scale);


        serviceExecutor::post( name, CMD_SERVICE_ACTIVE, 1 );
    }

    return ERR_NONE;
}

