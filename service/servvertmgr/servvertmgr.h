#ifndef SERVVERT_MGR_H
#define SERVVERT_MGR_H

#include "../../include/dsotype.h"
#include "../../service/servdso/sysdso.h"
#include "../service.h"
#include "../service_msg.h"
#include "../../baseclass/iserial.h"
/*!
 * \brief The servVertMgr class
 * 垂直通道管理器
 * - 管理垂直通道的活动状态
 * - 系统中最多只有一个通道作为当前的活动通道
 */
class servVertMgr: public serviceExecutor, public ISerial
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:
    enum servCmd
    {
        cmd_none,
        cmd_active_chan,

        cmd_chan_active,
        cmd_la_active,
        cmd_ref_active,
        cmd_math_active,

        cmd_open_source,

        cmd_chan1_button,
        cmd_chan2_button,
        cmd_chan3_button,
        cmd_chan4_button,

        cmd_la_button,

        cmd_chx_enter_active,
    };

public:
    servVertMgr( QString name, ServiceId eId = E_SERVICE_ID_NONE );
    virtual void registerSpy();

public:
    virtual int serialOut( CStream &stream, serialVersion &ver );
    virtual int serialIn( CStream &stream, serialVersion ver );
    virtual void rst();
    virtual int startup();
    int serviceup();

public:
    DsoErr setActiveChan( Chan ch );
    Chan   getActiveChan();

    DsoErr openSource( Chan ch );

    //! system
    DsoErr on_enterActive( ServiceId id );

protected:
    DsoErr onCH1ButtonClicked();
    DsoErr onCH2ButtonClicked();
    DsoErr onCH3ButtonClicked();
    DsoErr onCH4ButtonClicked();

    DsoErr onLaButtonClicked();

    DsoErr onCHxButtonClicked( const QString &name,
                               Chan chId );

protected:
    Chan mActiveChan;
};

#endif // SERVVERT_H
