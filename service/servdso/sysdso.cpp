
#include "../../include/dsostd.h"

#include "../../baseclass/rlangchangeevent.h"
#include "../../arith/hscale.h"
#include "../service.h"
#include "../service_msg.h"

#include "../service_name.h"
#include "../servch/servch.h"
#include "../servhori/servhori.h"
#include "../servmath/servmath.h"

#include "../servutility/servutility.h"
#include "../servstorage/servstorage.h"

#include "../../menu/arch/rmenubuilder.h"

#include "servdso.h"
#include "sysdso.h"

static sysDso _sysDso = {false,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         language_english,
                         QLatin1Literal(""),
                         0,
                         0,
                         false,
                         0ll,
                         0,
                         true
                        };

vertAttr sysGetVertAttr( Chan ch )
{
    //! fill vert attr
    void *pSmartVal;

    vertAttr *pAttr;
    QString servName;
    int attrMsg;

    switch( ch )
    {
    case chan1:
        servName = serv_name_ch1;
        attrMsg = servCH::cmd_ch_attr;
        break;
    case chan2:
        servName = serv_name_ch2;
        attrMsg = servCH::cmd_ch_attr;
        break;
    case chan3:
        servName = serv_name_ch3;
        attrMsg = servCH::cmd_ch_attr;
        break;
    case chan4:
        servName = serv_name_ch4;
        attrMsg = servCH::cmd_ch_attr;
        break;

    default:
        servName = serv_name_ch1;
        attrMsg = servCH::cmd_ch_attr;
        break;
    }

    //! query
    serviceExecutor::query( servName, attrMsg, pSmartVal );
    pAttr = (vertAttr *)pSmartVal;

    return *pAttr;
}

horiAttr sysGetHoriAttr( Chan ch, HorizontalView view )
{
    horiAttr attr;

    float tscalef;
    if(ch != chan_none)
    {
        QString servName;
        int mathOperator = -1;

        //! -- operator
        if( ch <= m4 && ch >=m1 )
        {
            servName = QString("math");
            servName = servName + QString::number((int)(ch - m1) + 1);
            serviceExecutor::query(servName, MSG_MATH_S32MATHOPERATOR, mathOperator);
        }
        else
        {}

        qlonglong tScale;
        qlonglong tOffset;

        //! fft
        if( mathOperator == operator_fft)
        {
            //! real
            serviceExecutor::query(servName, servMath::cmd_q_real_hscale, tscalef);

            serviceExecutor::query(servName, MSG_MATH_FFT_H_SPAN, tScale);
            tScale = tScale / hori_div;

            qlonglong tCenter;
            serviceExecutor::query(servName, MSG_MATH_FFT_H_CENTER, tCenter);

            //! fill attr
            attr.zone = horizontal_freq_zone;
            attr.unit = Unit_hz;

            attr.inc = tscalef / adc_hdiv_dots;

            attr.gnd = 500 - tCenter * adc_hdiv_dots/tScale;
            attr.tInc = tScale / adc_hdiv_dots;
        }
        else
        {
            //! deduce the view
            if ( view == horizontal_view_active )
            {
                bool bZoom;
                serviceExecutor::query(serv_name_hori,
                                       MSG_HOR_ZOOM_ON,
                                       bZoom );

                view = bZoom ? horizontal_view_zoom : horizontal_view_main;
            }

            //! main view
            if(view == horizontal_view_main)
            {
                //! real
                serviceExecutor::query(serv_name_hori,
                                   servHori::cmd_main_scale_real,tscalef);

                //! integer
                serviceExecutor::query(serv_name_hori,
                                   servHori::cmd_main_scale,tScale );

                serviceExecutor::query(serv_name_hori,
                                   servHori::cmd_main_offset,tOffset );

            }
            else
            {
                //! real
                serviceExecutor::query(serv_name_hori,
                                   servHori::cmd_zoom_scale_real,tscalef);

                //! integer
                serviceExecutor::query(serv_name_hori,
                                   servHori::cmd_zoom_scale,tScale );

                serviceExecutor::query(serv_name_hori,
                                   servHori::cmd_zoom_offset,tOffset );
            }

            //! fill attr
            attr.zone = horizontal_time_zone;
            attr.unit = Unit_s;

            attr.inc = tscalef/adc_hdiv_dots;

            attr.tInc = tScale / adc_hdiv_dots;
            attr.gnd = wave_width/2 - tOffset*adc_hdiv_dots/tScale;
        }

    }
    return attr;
}

void sysSetMainWindow( QWidget *wnd )
{
    Q_ASSERT( wnd != NULL );
    _sysDso.m_pMainWindow = wnd;
}

void sysSetMidWindow( QWidget *wnd )
{
    Q_ASSERT( wnd != NULL );
    _sysDso.m_pCentralWindow = wnd;
}

void sysSetBgWindow( QWidget *wnd )
{
    Q_ASSERT( wnd != NULL );
    _sysDso.m_pBgWindow = wnd;
}

QWidget *sysGetMainWindow()
{
    return _sysDso.m_pMainWindow;
}

QWidget *sysGetMidWindow()
{
    return _sysDso.m_pCentralWindow;
}

QWidget *sysGetBgWindow()
{
    return _sysDso.m_pBgWindow;
}

void sysSetMenuWindow( QWidget *wnd )
{
    Q_ASSERT( NULL != wnd );

    _sysDso.m_pMenuWindow = wnd;
}

QWidget *sysGetMenuWindow()
{
    return _sysDso.m_pMenuWindow;
}

QString sysGetString( int msg, const QString &defStr )
{
    return  menu_res::RMenuBuilder::getString( msg, defStr );
}

QString sysGetOptionString(int msg, int opt, const QString &defStr)
{
    return menu_res::RMenuBuilder::getOptionString(msg, opt,defStr);
}

QString sysTrimString( const QString &str, int maxSize )
{
    Q_ASSERT( maxSize > 0 );

    QString abbStr;
    //! xxxx
    if ( str.size() <= maxSize )
    { abbStr = str; }
    //! xx.x
    else
    {
        abbStr = str.left( maxSize - 1);
        abbStr.append( '.' );
        abbStr.append( str.right(1) );
    }
    return abbStr;
}

int sysGetImage( int msg, QImage &img )
{
    return  menu_res::RMenuBuilder::getImage( msg, img );
}

int sysGetResource( int msg, QString &str, QImage &img )
{
    return menu_res::RMenuBuilder::getResource( msg, str, img );
}

QWidget *sysGetWidget( const QString &str, int msg )
{
    return menu_res::RMenuBuilder::findView( str, msg );
}

QRect sysGetDesktopRect()
{
    return QRect( 0, 0, screen_width, screen_height );
}

void sysSetLanguage( SystemLanguage lang )
{
    bool bChanged;

    //! check input
    Q_ASSERT( lang >= language_english );
    Q_ASSERT( lang <= language_indonesia );

    //! changed
    bChanged = _sysDso.mLanguage != lang;

    _sysDso.mLanguage = lang;

    //! language changed
    if ( bChanged )
    {
        QEvent *pEvent;

        pEvent = new RLangchangeEvent( lang );
        Q_ASSERT( NULL != pEvent );
        qApp->postEvent( _sysDso.m_pMainWindow, pEvent );
    }
}

SystemLanguage sysGetLanguage()
{
    return _sysDso.mLanguage;
}

void sysSetTouchEnable(bool b)
{
    _sysDso.bTouchEnable = b;
}

bool sysGetTouchEnable()
{
    return _sysDso.bTouchEnable;
}

///
/// \brief sysGetSystemStatus
/// \return  0 stop, 1 running, 2 auto ,3 waiting ,4 td , 5 force stop
///
/// ref   ControlStatus
///
int sysGetSystemStatus()
{
    int sys_status = 0;
    serviceExecutor::query(serv_name_hori,
                           servHori::cmd_control_status,
                           sys_status);

    return sys_status;
}

/*!
 * \brief sysSetResourcePath
 * \param path
 * 设置系统资源路径
 */
void sysSetResourcePath( const QString &path )
{
    _sysDso.mResourcePath = path;
}
QString &sysGetResourcePath()
{
    return _sysDso.mResourcePath;
}

void sysSetMenuList( menu_res::serviceMenuList *pList )
{
    _sysDso.m_pMenuList = pList;
}

menu_res::serviceMenuList *sysGetMenuList()
{
    Q_ASSERT( _sysDso.m_pMenuList != NULL );
    return _sysDso.m_pMenuList;
}

QWidget * sysGetView( const QString &servName, int msg )
{
    return menu_res::RMenuBuilder::findView( servName, msg );
}

void sysSetKeyMapper( menu_res::RKeyMapper *pMapper )
{
    Q_ASSERT( NULL != pMapper );

    _sysDso.m_pMapper = pMapper;
}

menu_res::RKeyMapper *sysGetKeyMapper()
{
    Q_ASSERT( NULL != _sysDso.m_pMapper );
    return _sysDso.m_pMapper;
}

void sysMimicKey( int pkey )
{
    Q_ASSERT( NULL != _sysDso.m_pMapper );

    _sysDso.m_pMapper->mimicKey( pkey );
}

void sysMimicInc( int key )
{
    Q_ASSERT( NULL != _sysDso.m_pMapper );

    _sysDso.m_pMapper->mimicInc( key );
}

void sysMimicDec( int key )
{
    Q_ASSERT( NULL != _sysDso.m_pMapper );

    _sysDso.m_pMapper->mimicDec( key );
}

void sysMimcKey( const QString &name, int msg, int skey )
{
    QWidget *pWidget;

    pWidget = sysGetView( name, msg );
    if ( NULL != pWidget )
    {
        QKeyEvent *pEvent = new QKeyEvent( QEvent::KeyRelease,
                                           skey,
                                           Qt::NoModifier );
        Q_ASSERT( NULL != pEvent );
        qApp->postEvent( pWidget, pEvent );
    }
}

void sysSetArg( int argc, char **argv )
{
    _sysDso.mArgc = argc;
    _sysDso.mArgv = argv;
}

int sysGetArgc()
{
    return _sysDso.mArgc;
}

char** sysGetArgv()
{
    return _sysDso.mArgv;
}

char* sysGetArg( int i, bool *pbValid )
{
    Q_ASSERT( NULL != pbValid );

    if ( i < 0 || i >= _sysDso.mArgc )
    {
        *pbValid = false;
        return NULL;
    }

    *pbValid = true;
    return _sysDso.mArgv[ i ];
}

int sysGetArgInt( int i, bool *pbValid )
{
    Q_ASSERT( NULL != pbValid );

    bool valid;
    char *arg;

    arg = sysGetArg( i, &valid );
    if ( valid && arg != NULL )
    {}
    else
    {
        *pbValid = false;
        return 0;
    }

    int val;
    QString str( arg );
    val = str.toInt( pbValid );

    return val;
}

int sysCmpArg( int i, const QString &str, bool caseSen )
{
    Q_ASSERT( NULL != str );

    char *arg;
    bool valid;
    arg = sysGetArg( i, &valid );

    if ( arg != NULL && valid )
    { }
    else
    { return -1; }

    QString cmp2( arg );

    Qt::CaseSensitivity cs = caseSen ? Qt::CaseSensitive : Qt::CaseInsensitive;

    return QString::compare( cmp2, str, cs );
}

bool sysHasArg( const QString &str, bool caseSen )
{
     Qt::CaseSensitivity cs = caseSen ? Qt::CaseSensitive : Qt::CaseInsensitive;

    for ( int i = 1; i < _sysDso.mArgc; i++ )
    {
        if ( QString::compare( str, _sysDso.mArgv[i], cs) == 0 )
        { return true; }
    }

    return false;
}

void sysSetSuspend( bool b )
{
    _sysDso.mbSuspend = b;
}

bool sysGetSuspend()
{
    return _sysDso.mbSuspend;
}

void sysSetLiveCnt( quint32 cnt )
{
    _sysDso.mLiveCnt = cnt;
}

quint32 sysGetLiveCnt()
{
    return _sysDso.mLiveCnt;
}

//! in second
void sysSetCycleCnt( quint64 cycle )
{
    _sysDso.mCycleCnt = cycle;
}

quint64 sysGetCycleCnt()
{
    return _sysDso.mCycleCnt;
}


void sysAccCycleCnt( quint64 acc )
{
    _sysDso.mCycleCnt += acc;
}

void sysSetVertExpand( VertExpand expand )
{
    serviceExecutor::post( serv_name_dso,
                           servDso::CMD_VERT_EXPAND,
                           expand );
}

VertExpand sysGetVertExpand()
{
    int expand;

    serviceExecutor::query( serv_name_dso,
                            servDso::CMD_VERT_EXPAND,
                            expand );

    return (VertExpand)expand;
}

Bandwidth sysGetBw()
{
    int bw;
    serviceExecutor::query( serv_name_utility,
                            servUtility::cmd_system_band,
                            bw );

    return (Bandwidth)bw;
}

qlonglong sysGetMinHScale()
{
    return minHScale( sysGetBw() );
}

//! 区分系统工作模式
void sysSetWorkMode( DsoWorkMode mode )
{
    serviceExecutor::post( serv_name_dso,
                           servDso::CMD_WORK_MODE,
                           mode );
}

DsoWorkMode sysGetWorkMode()
{
    int val;

    serviceExecutor::query( serv_name_dso,
                            servDso::CMD_WORK_MODE,
                            val );

    return (DsoWorkMode)val;
}

//! 显示系统错误提示码
void sysShowErr( DsoErr err )
{
    serviceExecutor::post( serv_name_gui,
                           CMD_SERVICE_DO_ERR,
                           err );
}

void sysSetStarted()
{
    //! load completed
    _sysDso.mLoadCompleted = true;
}

bool sysGetStarted()
{
    return _sysDso.mLoadCompleted;
}

bool sysHasLA()
{
    return servUtility::isMSO();
}

bool sysHasDG()
{
    return servUtility::isHasDG();
}

//! valid opt
bool sysCheckLicense( OptType opt )
{
    if( opt == OPT_MSO )
    {
        return sysHasLA();
    }

    if ( sysHasArg("-fullopt")  )
    {
        return true;
    }
    else if( opt>=OPT_MSO && servStorage::isUKeyReady())
    {
        return true;
    }
    else
    {}

    CArgument argIn,argOut;

    argIn.setVal((int)opt);
    DsoErr err;
    err = serviceExecutor::query( serv_name_license,
                                  servLicense::cmd_get_OptEnable,
                                  argIn,
                                  argOut );

    if ( err != ERR_NONE )
    {
        return false;
    }

    return argOut[0].bVal;
}


///
/// \brief getStrSpace
/// \param str
/// 解读UTF-8编码非常简单。如果一个字节的第一位是0，
/// 则这个字节单独就是一个字符；如果第一位是1，则连续有多少个1，
/// 就表示当前字符占用多少个字节
/// \return
///
int sysGetNormalSpace(QString &str)
{
    QByteArray ba = str.toLocal8Bit();
    int size = ba.size();
    int count = 0;
    int i=0;
    while ( i < size )
    {
        int val = ba.at(i) & 0x80;

        if( val > 0 )//utf 3 byte
        {
            val = ba.at(i) & 0xf0;
            if( val == 0xf0 )
            {
                i += 4;
            }
            else if( val == 0xe0)
            {
                i+= 3;
            }
            else if( val == 0xc0)
            {
                i += 2;
            }
            count++;
        }
        else
        {
            i++;
        }
        count++;
    }

    return count;
}
#include <sys/time.h>
float operator-( timeval &tm1, timeval &tm2 )
{
    return tm1.tv_sec*1e6f + tm1.tv_usec - tm2.tv_sec*1e6f-tm2.tv_usec;
}


QColor sysGetChColor(Chan src)
{
    QColor color = Qt::white;
    switch (src)
    {
    case chan1:
        color = CH1_color;
        break;
    case chan2:
        color = CH2_color;
        break;
    case chan3:
        color = CH3_color;
        break;
    case chan4:
        color = CH4_color;
        break;
    case m1: case m2: case m3: case m4:
        color = Math_color;
        break;
    case d0: case d1: case d2: case d3:
    case d4: case d5: case d6: case d7:
    case d8: case d9: case d10: case d11:
    case d12: case d13: case d14: case d15:
        color = QColor::fromRgb(0x00,0xff,0x00);
        break;

    default:
        color = Qt::white;
        break;
    }
    return color;
}


#include "../../service/servscpi/servscpi.h"
#include "../servscpi/servscpidso.h" //! scpi_event

//! 服务中处理SCPI事件标记
void sysSetScpiEvent( dsoScpiEvent event,
                      int bitVal )
{
    CArgument arg;

    //! argument: 0, 1
    arg.append( (int)event );
    arg.append( bitVal );
    serviceExecutor::post( E_SERVICE_ID_SCPI,
                           dsoServScpi::cmd_scpi_event,
                           arg );
}


void sysScpiFilterServive(QString servName, bool b)
{
    CArgument arg;
    //! argument: 0, 1
    arg.append( servName );
    arg.append( b );
    serviceExecutor::post( E_SERVICE_ID_SCPI,
                           dsoServScpi::cmd_scpi_serv_filter,
                           arg );
}

void sysScpiAddMap(QString servName, QList<int> &cmdList)
{
    servScpi::addTempMap(servName, cmdList);
}
void sysScpiExecuteEn(bool b)
{
    serviceExecutor::post( E_SERVICE_ID_SCPI,
                           dsoServScpi::cmd_scpi_execute_en,
                           b );
}
void sysScpiExeLock()
{
    serviceExecutor::send( serv_name_scpi,
                           dsoServScpi::cmd_scpi_execute_lock,
                           1 );
}

void sysScpiExeUnLock()
{
    serviceExecutor::send( serv_name_scpi,
                           dsoServScpi::cmd_scpi_execute_unlock,
                           1 );
}

///
/// \brief sysKeyPress
/// \param key  enum KeyDso
///
void sysKeyPress( int key )
{
     serviceExecutor::post( E_SERVICE_ID_UTILITY,
                            servUtility::cmd_key_press,
                            key);  //! Set to TopMenu
}

void sysKeyIncrease( int key, int cnt )
{
    CArgument arg;
    arg.setVal(key, 0);
    arg.setVal(cnt, 1);
     serviceExecutor::post( E_SERVICE_ID_UTILITY,
                            servUtility::cmd_key_increase,
                            arg);
}

void sysKeyDecrease( int key, int cnt )
{
    CArgument arg;
    arg.setVal(key, 0);
    arg.setVal(cnt, 1);

     serviceExecutor::post( E_SERVICE_ID_UTILITY,
                            servUtility::cmd_key_decrease,
                            arg);
}
