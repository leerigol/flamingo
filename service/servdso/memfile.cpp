#include "../../include/dsodbg.h"
#include "memfile.h"
#include "../../com/crc32/ccrc32.h"

#ifndef _NO_CRC
#define calc_crc( a, b )    com_algorithm::CCrc32::crc32( (a), (b) )
#else
#define calc_crc( a, b )    0
#endif

SegFile::SegFile( int id )
{
    mId = id;
    mInvId = -id;

    mLen = 0;
    mInvLen = 0;

    mCrc = 0;

    //! mem
    m_pMem = NULL;
}
SegFile::~SegFile()
{
    if ( NULL != m_pMem )
    {
        delete []m_pMem;
    }
}

SegFile &SegFile::operator=( SegAttr & attr )
{
    mId = attr.mId;
    mInvId = attr.mInvId;

    mLen = attr.mLen;
    mInvLen = attr.mInvLen;

    mCrc = attr.mCrc;

    return *this;
}
void SegFile::getAttr( SegAttr &attr )
{
    attr.mId = mId;
    attr.mInvId = mInvId ;

    attr.mLen = mLen;
    attr.mInvLen = mInvLen;

    attr.mCrc = mCrc;
}


//! attr
//! data
int SegFile::serialOut( void *pData )
{
    Q_ASSERT( NULL != pData );

    if ( mLen > 0 )
    {}
    else
    { return 0; }

    SegAttr *pAttr = (SegAttr*)this;

    memcpy( pData, pAttr, sizeof(SegAttr) );

    memcpy( (quint8*)pData + sizeof(SegAttr), m_pMem, mLen );

    return sizeof(SegAttr) + mLen;
}
//! > 0 success
//! attr
//! data
int SegFile::serialIn( void *pData )
{
    Q_ASSERT( NULL != pData );

    SegAttr attr;

    //! deload the stream
    memcpy( &attr, pData, sizeof(attr) );

    //! invalid attr
    if ( !checkAttr( &attr ) )
    { return -1; }

    //! save attr
    *this = attr;

    //! read data
    if ( NULL != m_pMem )
    { delete []m_pMem; }

    m_pMem = new quint8[ mLen ];
    if ( NULL == m_pMem )
    { return mLen + sizeof(SegAttr); }

    memcpy( m_pMem, (quint8*)pData + sizeof(SegAttr), mLen );

    //! check crc
    if ( mCrc != calc_crc( m_pMem, mLen ) )
    { return -1; }

    //! data len
    return attr.mLen + sizeof(SegAttr);
}

int SegFile::write( void *pData, int size )
{
    Q_ASSERT( NULL != pData );

    if ( NULL != m_pMem )
    { delete []m_pMem; }

    m_pMem = new quint8[size];
    if ( NULL == m_pMem )
    { return 0; }

    memcpy( m_pMem, pData, size );

    mLen = size;
    mInvLen = -size;
    mCrc = calc_crc( m_pMem, mLen );

    return size;
}
int SegFile::read( void *pData, int size )
{
    Q_ASSERT( NULL != pData );

    if ( m_pMem == NULL )
    { return 0; }

    memcpy( pData, m_pMem, size> mLen ? mLen : size );

    return size> mLen ? mLen : size;
}

int SegFile::getSize()
{
    return mLen + sizeof(SegAttr);
}

int SegFile::getLen()
{
    return mLen;
}

bool SegFile::checkAttr( SegAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );

    if (   (pAttr->mId + pAttr->mInvId) != 0
         ||( pAttr->mLen + pAttr->mInvLen) != 0  )
    { return false; }
    else
    { return true; }
}

MemFile::MemFile()
{
    mbModified = false;
}

int MemFile::save( int id, void *pData, int size )
{
    SegFile *pSeg;

    pSeg = find( id );
    if ( pSeg != NULL )
    {
        mbModified = true;
        return pSeg->write( pData, size );
    }
    else
    {
        pSeg = new SegFile( id );
        if ( NULL == pSeg )
        { return 0; }

        mSegs.append( pSeg );

        mbModified = true;
        return pSeg->write( pData, size );
    }
}
int MemFile::load( int id, void *pData, int cap )
{
    SegFile *pSeg;

    pSeg = find( id );
    if ( pSeg == NULL )
    { return 0; }

    return pSeg->read( pData, cap );
}
int MemFile::getLen( int id )
{
    SegFile *pSeg;

    pSeg = find( id );
    if ( pSeg == NULL )
    { return 0; }

    return pSeg->getLen();
}
int MemFile::remove( int id )
{
    SegFile *pSeg;

    pSeg = find( id );
    if ( pSeg == NULL )
    { return -1; }

    mSegs.removeAll( pSeg );
    mbModified = true;

    delete pSeg;

    return 0;
}
int MemFile::format()
{
    clear( mSegs );
    mbModified = true;

    return 0;
}

SegFile *MemFile::find( int id )
{
    foreach( SegFile * pItem, mSegs )
    {
        Q_ASSERT( NULL !=pItem );

        if ( pItem->mId == id )
        { return pItem; }
    }

    return NULL;
}

void MemFile::clear( QList<SegFile*> &segs )
{
    foreach( SegFile *pSeg, segs )
    {
        Q_ASSERT( NULL != pSeg );

        delete pSeg;
    }

    segs.clear();
}

//! 4 + 4
//! segs
int MemFile::serialIn( void *pStream )
{
    Q_ASSERT( NULL != pStream );

    //! check size
    int size, invSize;
    int sumCnt = 0;

    memcpy( &size, (quint8*)pStream, sizeof(size) );
    memcpy( &invSize, (quint8*)pStream + sizeof(size), sizeof(size) );

    //! move the stream
    sumCnt += 2 * sizeof(size);

    //! raw size check
    if ( (size + invSize) != 0 )
    { return -1; }

    //! deload the data
    QList< SegFile *> subSegs;

    int ret;
    ret = 0;

    SegFile *pSeg;
    while( sumCnt < size )
    {
        //! new segment
        pSeg = new SegFile( 0 );
        if ( NULL == pSeg )
        {
            clear( subSegs );
            return -1;
        }

        //! serial seg
        ret = pSeg->serialIn( (quint8*)pStream + sumCnt );

        //! invlaid len in sub seg
        if ( ret < 0 )
        {
            delete pSeg;
            return -1;
        }
        else
        {
            sumCnt += ret;
            subSegs.append( pSeg );
        }
    }

    //! clear the history
    clear( mSegs );

    //! recover
    mSegs = subSegs;

    LOG_DBG()<<"!!!recover "<<mSegs.size();

    return sumCnt;
}
//! 4 + 4
//! segs
int MemFile::serialOut( void *pStream, int cap )
{
    Q_ASSERT( cap >= 8 );

    int sumSize = 0;

    //! total size
    int totalSize = getSize();

    //! tag
    memcpy( (quint8*)pStream + sumSize, &totalSize, sizeof(totalSize ) );
    sumSize += sizeof( totalSize );
    totalSize = -totalSize;
    memcpy( (quint8*)pStream + sumSize, &totalSize, sizeof(totalSize ) );
    sumSize += sizeof( totalSize );

    foreach( SegFile *pItem, mSegs )
    {
        Q_ASSERT( NULL != pItem );

        if ( (cap - sumSize) < pItem->getSize() )
        {
            qWarning()<<"!!!Lack of cap";
            return sumSize;
        }

        sumSize += pItem->serialOut( (quint8*)pStream + sumSize );
    }

    return sumSize;
}

int MemFile::getSize()
{
    int size = 0;

    foreach( SegFile *pItem, mSegs )
    {
        Q_ASSERT( NULL != pItem );

        size += pItem->getSize();
    }

    return size + 8;
}

bool MemFile::isModified()
{
    return mbModified;
}
void MemFile::setModified( bool b )
{
    mbModified = b;
}

void MemFile::save( const QString &fileName )
{
    int size;

    size = getSize();

    quint8 *pBuf = new quint8[ size ];
    if ( NULL == pBuf )
    { return; }
    int count;

    count = serialOut( pBuf, size );

    QFile file( fileName );
    if ( file.open( QIODevice::WriteOnly ) )
    {
        file.write( (const char*)pBuf, count );

        file.close();
    }
    else
    {}

    delete []pBuf;
}
void MemFile::load( const QString &fileName )
{
    QFile file( fileName );
    if ( file.open( QIODevice::ReadOnly ) )
    {
        int size = file.size();

        quint8 *pBuf = new quint8[size];

        file.read( (char*)pBuf, size );

        serialIn( pBuf );

        delete []pBuf;

        file.close();
    }
    else
    {}
}
