#include "../../include/dsostd.h"
#include "sysdso.h"

/**
 * @brief getTimeStep
 * @param t
 * @return
 */
qint64 getTimeStep(qint64 t)
{
    qint64 base = time_s(1000);
    if ( t == 0 )
    {
        return time_ns(10);
    }
    while(base != 0)
    {
        if( t / base )
        {
            if(base > 100)
            {
                return (base/100);
            }
            else
            {
                return 1;
            }
        }
        base = base / 10;
    }

    /* here should never be come */
    Q_ASSERT(false);

    return time_us(1);
}

qint64 getFreqStep(qint64 freq)
{
    qint64 base = 100000000000000ll;//100M
    while(base != 0)
    {
        if( freq / base )
        {
            if(base > 100)
            {
                return (base/100);
            }
            else
            {
                return 1;
            }
        }
        base = base / 10;
    }
    /* here should never be come */
    Q_ASSERT(false);

    return freq_Hz(1);//1Hz
}

int getVoltStep(int volt)
{
    qint64 base = 10000000000;//100V
    if(volt == 0)
    {
        return uv(1);
    }
    while(base != 0)
    {
        if( volt / base )
        {
            if(base >= 100)
            {
                return (base/100);
            }
            else
            {
                return 1;
            }
        }
        base = base / 10;
    }
    /* here should never be come */
    Q_ASSERT(false);

    return mv(10);//10mv
}

qint64 getNumStep(long long currentValue, StepType type)
{
    qint64 num = 0;
    if(type == INC_STEP)
    {
        num = currentValue;
    }
    else
    {
        //! 左旋时，步进档位切换点上按照上一档的步进值
        //! 例： 1000按照999的步进值
        num = currentValue - 1;
    }

    qint64 base = 1e12;
    if ( num == 0 )
    {
        return 1;
    }
    while(base != 0)
    {
        if( num / base )
        {
            if(base > 100)
            {
                return (base/100);
            }
            else
            {
                return 1;
            }
        }
        base = base / 10;
    }

    /* here should never be come */
    Q_ASSERT(false);

    return 1;
}

qint64 getNumStep(int currentValue)
{
    qint64 num = currentValue;

    qint64 base = 1e12;
    if ( num == 0 )
    {
        return 1;
    }
    while(base != 0)
    {
        if( num / base )
        {
            if(base > 100)
            {
                return (base/100);
            }
            else
            {
                return 1;
            }
        }
        base = base / 10;
    }

    /* here should never be come */
    Q_ASSERT(false);

    return 1;
}

QString toStringIP(int ip)
{
    QString addr = QString("%1.%2.%3.%4").arg( ip & 0xff).arg( (ip & 0xff00) >> 8).arg( (ip & 0xff0000) >> 16).arg( (ip & 0xff000000) >> 24);
    return addr;
}

int toDigitIP(QString &addr)
{
    QStringList list = addr.split(".");
    if( list.size() != 4 )
    {
        qDebug() << "addr ERROR on" << __FUNCTION__ << __LINE__;
        return 0;
    }

    int ip = 0;
    int part = 0;
    for(int i=0; i<list.size(); i++)
    {
        QString d = list[i];
        part = d.toUInt();
        ip = ip | part << (i*8);
    }
    return ip;
}
