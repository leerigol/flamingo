#include "../../include/dsostd.h"
#include "servdso.h"

#include "../../com/crc32/ccrc32.h"
#include "fram_map.h"

#define COMP_SIZE   32
#define COMP_PAGE   16

#define head_type_comp      0
#define head_type_uncomp    1

/***************************************************/
/* 2018-04-28  00.01.01.06.00    0xa      hxh      */
/* 2018-06-06  00.01.01.06.04    0xb      hxh      */
/***************************************************/
const static quint8 G_NVFRAM_DATA_VERSION = 0xb;


struct setHead
{
    quint8 mServId;
    quint8 mType;       //! comp -- 1 / ucomp -- 0
    quint8 mVer;
    quint8 _pad;

    qint16 mRawLen;     //! rawLen
    qint16 mLen;        //! packed len

    qint16 mCapSize;
    qint16 _pad1;
};

//! all services
DsoErr servDso::pullSession( QByteArray &ary )
{
    return servDso::pullSession( ary, service::_servList );
}

//! some services
DsoErr servDso::pullSession( QByteArray &bytesStream,
                       QStringList &servs )
{
    servItemList itemList;

    struServItem *pItem;
    foreach( QString str, servs )
    {
        pItem = service::findItem( str );
        Q_ASSERT( NULL != pItem );

        itemList.append( pItem );
    }

    serviceExecutor *pExe = serviceExecutor::getExecutor( E_SERVICE_ID_DSO );
    Q_ASSERT( NULL != pExe );

    servDso *pDso = dynamic_cast<servDso*>( pExe );
    Q_ASSERT( pDso != NULL );

    return pDso->pullBinSetup( bytesStream, itemList );
}

DsoErr servDso::pullSession( QByteArray &ary,
                           QList<ServiceId> &servList )
{
    servItemList itemList;

    struServItem *pItem;
    foreach( ServiceId id, servList )
    {
        pItem = service::findItem( id );
        Q_ASSERT( NULL != pItem );

        itemList.append( pItem );
    }

    return servDso::pullSession( ary, itemList );
}

DsoErr servDso::pullSession( QByteArray &ary,
                         servItemList &servList )
{
    serviceExecutor *pExe = serviceExecutor::getExecutor( E_SERVICE_ID_DSO );
    Q_ASSERT( NULL != pExe );

    servDso *pDso = dynamic_cast<servDso*>( pExe );
    Q_ASSERT( pDso != NULL );

    return pDso->pullBinSetup( ary, servList );
}

DsoErr servDso::pushSession( QByteArray &ary )
{
    QList<int> lists;

    return pushSession( ary, lists );
}

DsoErr servDso::pushSession( QByteArray &ary, QList<int> &servLists )
{
    serviceExecutor *pExe = serviceExecutor::getExecutor( E_SERVICE_ID_DSO );
    Q_ASSERT( NULL != pExe );

    servDso *pDso = dynamic_cast<servDso*>( pExe );
    Q_ASSERT( pDso != NULL );

    return pDso->pushBinSetup( ary, servLists );
}

DsoErr servDso::startupSession( QList<int> &servList )
{
    serviceExecutor *pExe = serviceExecutor::getExecutor( E_SERVICE_ID_DSO );
    Q_ASSERT( NULL != pExe );

    servDso *pDso = dynamic_cast<servDso*>( pExe );
    Q_ASSERT( pDso != NULL );

    return pDso->startupBinSetup( servList );
}

DsoErr servDso::saveSession()
{
    DsoErr err;
    int ret;

    //! setup
    QByteArray stream;
    err = pullBinSetup( stream );
    if ( err != ERR_NONE )
    {
        return err;
    }

//    Q_ASSERT( setup_size > stream.length() );
    if( setup_size <= stream.length() )
    {
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    ret = mFram.write( stream.data(), setup_start, stream.length() );
    if ( ret != stream.length() )
    {
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    //! nv
    QByteArray nvStream;
    err = pullNvInfo( nvStream );
    if ( err != ERR_NONE )
    {
        return err;
    }

//    Q_ASSERT( nv_size > nvStream.length() );
    if( nv_size <= nvStream.length())
    {
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    ret = mFram.write( nvStream.data(), nv_start, nvStream.length() );
    if ( ret != nvStream.length() )
    {
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    //! private data
    QByteArray privateStream;
    err = pullPrivate( privateStream );
    if ( err != ERR_NONE )
    {
        return err;
    }

    LOG_DBG()<<"privateStream.length() = "<<privateStream.length();

    //add by lidongming for bug 2443
    if( private_size <= privateStream.length())
    {
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    if ( privateStream.length() > 0 )
    {
        ret = mFram.write( privateStream.data(), private_start, privateStream.length() );
        if ( ret != privateStream.length() )
        {
            return ERR_BIN_SETUP_SAVE_FAIL;
        }

        servDso::_memFile.setModified( false );
    }

    ret = mFram.flushCache();
    if ( ret != 0 )
    {
        LOG_DBG();
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    return ERR_NONE;
}

DsoErr servDso::loadSession()
{
    DsoErr err;

    //! nv
    if ( sysHasArg("-nonv") )
    {}
    else
    {
        QByteArray nvStream;
        nvStream.append( (const char*)mFram.getDiskBuf() + nv_start, nv_size );
        err = pushNvInfo( nvStream );
        if ( err != ERR_NONE )
        {
            return err;
        }
    }

    //! load last.
    if ( !sysGetSuspend() )
    {
        qWarning()<<"default setting by user set";
        return ERR_NONE;
    }

    if ( sysHasArg("-default") )
    {
        return ERR_NONE;
    }

    //! setup
    QByteArray stream;
    stream.append( (const char*)mFram.getDiskBuf() + setup_start, setup_size );
    err = pushBinSetup( stream );
    if ( err != ERR_NONE )
    {
        return err;
    }

    return ERR_NONE;
}

DsoErr servDso::saveStream( const QString &name,
                   QByteArray &stream )
{
    if ( stream.size() < 1 )
    { return ERR_BIN_SETUP_SAVE_FAIL; }

    //! write data
    QFile file( name );
    if ( !file.open( QIODevice::WriteOnly ) )
    {
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    if ( file.write( stream ) != stream.length() )
    {
        file.close();
        return ERR_BIN_SETUP_SAVE_FAIL;
    }

    file.close();
    return ERR_NONE;
}

DsoErr servDso::loadStream( const QString &name,
                            QByteArray &ary )
{
    QFile file(name);
    if ( !file.open(QIODevice::ReadOnly) )
    {
        return ERR_BIN_SETUP_READ_FAIL;
    }

    ary = file.readAll();

    return ERR_NONE;
}

//! binary setup
DsoErr servDso::pullBinSetup( QByteArray &bytesStream )
{
    return pullBinSetup( bytesStream, service::_servList );
}

DsoErr servDso::pullBinSetup( QByteArray &bytesStream,
                              servItemList &itemList )
{
    quint8 pBuf[1024];

    CBinStream binStream( pBuf, 1024 );
    serialVersion ver;

    ISerial *pSerial;

    struServItem *pServItem;
    int len = 0;
    int streamLen;

    QByteArray compAry;
    setHead head;

    QByteArray padAry;
    padAry.fill( 0, COMP_PAGE );

    //! services
    foreach( pServItem, itemList )
    {
        Q_ASSERT( pServItem != NULL );
        Q_ASSERT( NULL != pServItem->pService );

        pSerial = dynamic_cast<ISerial*> ( pServItem->pService );
        if ( pSerial != NULL )
        {
            //! out
            binStream.setSeek(0);
            binStream.setLength( 0 );
            pSerial->serialOut( binStream, ver );

            //! raw len
            streamLen = binStream.getLength();

            //! check size
            if ( streamLen > COMP_SIZE )
            {
                //! do compress
                compAry = qCompress( (const uchar*)binStream.getBase(), streamLen );

                //! fill head
                head.mServId = (quint8)pServItem->servId;
                head.mType = head_type_comp;
                head.mVer = ver;
                head._pad = 0;

                head.mRawLen = streamLen;
                head.mLen = compAry.size();

                head.mCapSize = ((head.mLen + COMP_PAGE-1)/COMP_PAGE)*COMP_PAGE;
                head._pad1 = 0;

                //! export
                bytesStream.append( (char*)&head, sizeof(head) );
                bytesStream.append( compAry.constData(), head.mLen );

                //! add tail
                bytesStream.append( padAry.data(), head.mCapSize - head.mLen );
            }
            else if ( streamLen > 0 )
            {
                //! fill head
                head.mServId = (quint8)pServItem->servId;
                head.mType = head_type_uncomp;
                head.mVer = ver;
                head._pad = 0;

                head.mRawLen = streamLen;
                head.mLen = streamLen;

                //! do not align
                head.mCapSize = streamLen;
                head._pad1 = 0;

                //! export
                bytesStream.append( (char*)&head, sizeof(head) );
                bytesStream.append( (char*)binStream.getBase(), head.mLen );

                //! add tail
                bytesStream.append( padAry.data(), head.mCapSize - head.mLen );
            }
            else
            {
            }

//            LOG_DBG()<<pServItem->pService->getName()<<head.mRawLen<<head.mLen<<bytesStream.size();
        }
        else
        {
//            qWarning()<<pServItem->pService->getName()<<"no iserial";
        }

    }

    //! add crc
    quint32 crc;
    crc = com_algorithm::crc32( (unsigned char*)bytesStream.constData(),
                                bytesStream.size() );

    len = bytesStream.size();
    bytesStream.prepend( (char*)&len, 4 );
    bytesStream.prepend( (char*)&crc, 4 );
//    LOG_DBG()<<len;

    return ERR_NONE;
}

DsoErr servDso::pullNvInfo( QByteArray &ary )
{
    DsoErr err;

    struNvInfo info;
    memset( &info, 0, sizeof(info) );
    err = pullNvInfo( info );

    if ( ERR_NONE != err )
    {
        return err;
    }

    ary.append( (char*)&info, sizeof(info) );

    return ERR_NONE;
}


#define load_item( src )  memcpy( info.info + offset, &src, sizeof(src) );\
                          offset += sizeof(src);
DsoErr servDso::pullNvInfo( struNvInfo &info )
{
    int offset = 0 ;
    //! bytes
    info.ver = G_NVFRAM_DATA_VERSION;
    info.len = sizeof( info ) - 8;


    memset( info.info, 0, array_count(info.info) );
    info.info[0] = sysGetLanguage();
    info.info[1] = sysGetSuspend();
    offset = 2;

    quint64 cycles;
    cycles = sysGetCycleCnt();
    load_item( cycles );

    quint32 liveCnt;
    liveCnt = sysGetLiveCnt();
    load_item( liveCnt );

    //! crc
    info.crc = com_algorithm::crc32( &info.ver, sizeof(info) - 8 );

    return ERR_NONE;
}

DsoErr servDso::pullPrivate( QByteArray &ary )
{
    int size;

    if ( _memFile.isModified() )
    {}
    else
    { return ERR_NONE; }

    size = servDso::_memFile.getSize();
    if ( size > 0 )
    {
        //! buf
        quint8 *pBuf = new quint8[size];
        if ( NULL == pBuf )
        { return ERR_FILE_SAVE_FAIL; }

        servDso::_memFile.serialOut( pBuf, size );

        //! attach data
        ary.append( (char*)pBuf, size );

        delete []pBuf;

        return ERR_NONE;
    }
    else
    { return ERR_NONE; }
}

DsoErr servDso::checkStream( QByteArray &ary, quint32 *pLen )
{
    Q_ASSERT( NULL != pLen );

    if ( ary.size() < 8 ) return ERR_BIN_SETUP_CONTENT;

    quint32 crc;
    int len;

    char *data = ary.data();
    memcpy( &crc, data, 4 );
    memcpy( &len, data + 4, 4 );

    //! invalid size
    if ( (len + 8) > ary.size() )
    {
        return ERR_BIN_SETUP_CONTENT;
    }

    //! check crc
    quint32 crcCalc;
    crcCalc = com_algorithm::crc32( data + 8, len );
    if ( crcCalc != crc )
    {
        return ERR_BIN_SETUP_CONTENT;
    }

    *pLen = len;

    return ERR_NONE;
}

DsoErr servDso::pushBinSetup( QByteArray &ary )
{
    QList<int> lists;
    return pushBinSetup( ary, lists );
}

DsoErr servDso::pushBinSetup( QByteArray &ary, QList<int> &servLists )
{
    DsoErr err;
    quint32 fullLen;

    err = checkStream( ary, &fullLen );
    if ( err != ERR_NONE )
    { return err; }

    //! now for each item
    char *pData;

    CBinStream binStream(NULL,0);

    //! skip the check head
    pData = ary.data() + 8;

    setHead head;
    QByteArray unCompAry;
    while( fullLen > 0 )
    {
        //! get the head
        memcpy( &head, pData, sizeof(head) );
        pData += sizeof( head );

        Q_ASSERT( head.mCapSize > 0 );
        Q_ASSERT( head.mLen > 0 && head.mRawLen > 0 );

        //! parse the head
        if ( head.mType == head_type_comp )
        {
            unCompAry = qUncompress( (uchar*)pData, head.mLen );

            Q_ASSERT( unCompAry.size() == head.mRawLen );

            binStream.attach( unCompAry.data(), head.mRawLen );
        }
        else
        {
            binStream.attach( pData, head.mLen );
        }

        //! dispatch only a few
        if ( head.mServId >= E_SERVICE_ID_CH1 && head.mServId <= E_SERVICE_ID_ALL_FRAM )
        {
            err = dispatchStream( head.mServId, head.mVer, binStream );
            if ( err == ERR_NONE )
            {
                servLists.append( head.mServId);
            }
            else
            {
                //!!!! if there is anything error,
                //!     then use default setting. !!!/
                qDebug() << "Setting incompatible";
                return ERR_NONE;
            }
        }

        pData += head.mCapSize;

        fullLen -= ( head.mCapSize + sizeof(head) );
    }

    return ERR_NONE;
}

DsoErr servDso::dispatchStream( int servId,
                       serialVersion ver,
                       CStream &stream )
{
    service *pServ;
    pServ = findService( servId );
    if ( pServ == NULL )
    {
        return ERR_BIN_SETUP_SERV_ID;
    }

    ISerial *pSerial;
    pSerial = dynamic_cast<ISerial*>( pServ );
    if ( pSerial == NULL )
    {
        return ERR_BIN_SETUP_SERV_ID;
    }

    int ret = 0;
    if ( ver != pSerial->getVersion()  )
    {
        if ( pSerial->getBinCompatible() )
        {
            ret = pSerial->serialIn( stream, ver );
        }
        else
        {
            pSerial->rst();//added by hxh
        }
    }
    else
    {
        ret = pSerial->serialIn( stream, ver );
    }

    return ret;
}

DsoErr servDso::startupBinSetup( QList<int> &servLists )
{
    foreach( int servId, servLists )
    {
        id_async( servId, CMD_SERVICE_STARTUP );
        LOG_DBG()<<servId;
    }

    return ERR_NONE;
}

DsoErr servDso::pushNvInfo( QByteArray &ary )
{
    DsoErr err;
    quint32 fullLen;

    err = checkStream( ary, &fullLen );
    if ( err != ERR_NONE )
    {
        return err;
    }

    struNvInfo info;
    memcpy( &info, ary.data(), sizeof(info) );

    pushNvInfo( info );

    return ERR_NONE;
}

#define deload_item( item ) memcpy( &item, info.info + offset, sizeof(item) );\
                            offset += sizeof(item);
DsoErr servDso::pushNvInfo( struNvInfo &info )
{

    int offset = 0;

    sysSetLanguage( (SystemLanguage)info.info[0] );
    sysSetSuspend( info.info[1] );
    offset = 2;

    /**
     * 每次启动时，如果系统设置信息的版本有变化，则需要恢复默认设置
     * 一般用于重大版本更新后使用该机制，平时升级版本不会导致恢复默认设置
    */
    if( info.ver != G_NVFRAM_DATA_VERSION )
    {
        sysSetSuspend( false );
    }

    //! cycles
    quint64 cycles;
    deload_item( cycles );
    sysSetCycleCnt( cycles );

    //! live cnt + 1
    quint32 liveCnt;
    deload_item( liveCnt );
    sysSetLiveCnt( liveCnt + 1 );

    return ERR_NONE;
}

// The first service API executed
// Before init rst,startup
DsoErr servDso::loadPrivacy()
{
    int ret;

    mFram.open();

    //! read fail
    ret = mFram.loadCache();

    if ( ret != 0 )
    {
        qWarning()<<"load setting fail";
        return ERR_BIN_SETUP_READ_FAIL;
    }


    //! private data
    if ( sysHasArg("-noprivacy") )
    {
        servDso::_memFile.format();
    }
    else
    {
        servDso::_memFile.serialIn( mFram.getDiskBuf() + private_start );
    }

    return ERR_NONE;
}

DsoErr servDso::clearPrivacy()
{
    servDso::_memFile.format();
    return ERR_NONE;
}
