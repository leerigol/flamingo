#ifndef SERVDSO
#define SERVDSO

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../../com/fram/cfram.h"
#include "../../meta/crmeta.h"

#include "memfile.h"

#include "servdso_timer.h"

//! byte aligned
struct struNvInfo
{
    quint32 crc;
    quint32 len;

    quint8 ver;

    quint8 info[128];
};

enum screenContent
{
    screen_pic,
    screen_wave
};

//! phiscal wnd
class dsoScreen : public QRect
{
public:
    dsoScreen();
public:
    void setGeometry( int x, int y, int w, int h );
    void setGeometry( const QRect &rect );
    QRect &getGeomerty();

    void setVisible( bool b );
    bool getVisible();

    void setContent( screenContent cont );
    screenContent getContent();

    void set( bool b, screenContent cont = screen_wave );

protected:
    bool mbVisible;
    screenContent mContent;
};

//! physical panels
class dsoPanel
{
public:
    static dsoPanel* instance();
private:
    static dsoPanel *_instance;
private:
    dsoPanel();
public:
    ~dsoPanel();

public:
    void setGeometry( int index, const QRect &rect );
    QRect &getGeometry( int index );

    int screenCount();
    dsoScreen *operator[]( int index );

protected:
    void alloc( int index );

protected:
    QList< dsoScreen* > mScreens;    //! a few screen
};

class servDso : public serviceExecutor
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:
    enum servCmd
    {
        CMD_NONE,

        CMD_SCREEN_MODE,        //! 屏幕分割方式改变
        CMD_VERT_EXPAND,        //! 垂直扩展方式
        CMD_WORK_MODE,          //! 工作模式             

        CMD_FLUSH_CACHE,        //! flaush save cache

        CMD_CLEAR_PRIVACY,      //! clear privacy
        CMD_VIEW_CHANGE,
        CMD_START_TIMER
    };

public:
    static DsoErr checkRange( const QString &path,
                              int &val,
                              const r_meta::CMeta &meta=r_meta::CBMeta() );
    static DsoErr checkRange( const QString &path,
                              qlonglong &val,
                              const r_meta::CMeta &meta=r_meta::CBMeta() );
public:
    static int getIdBase( int servId );

    static int save( int id,void *data,int size );
    static int load( int id,void *data,int size );

    static int    getLen( int id );
    static DsoErr remove( int id );
    static DsoErr format();
public:
    //! pull
    static DsoErr pullSession( QByteArray &ary );
    static DsoErr pullSession( QByteArray &ary,
                           QStringList &servs );
    static DsoErr pullSession( QByteArray &ary,
                             QList<ServiceId> &servList );
    static DsoErr pullSession( QByteArray &ary,
                             servItemList &servList );

    //! push
    static DsoErr pushSession( QByteArray &ary );
    static DsoErr pushSession( QByteArray &ary, QList<int> &servLists );

    //! startup
    static DsoErr startupSession( QList<int> &servList );

public:
    static dsoPanel * getPanel();

private:
    static MemFile _memFile;


public:
    servDso( QString name, ServiceId id );

public:
    virtual void registerSpy();
public:
    virtual DsoErr start();

protected:
    void init();
    void rst();
    int startup();

    void on_view_changed();
    DsoScreenMode getScreenMode();
    DsoErr screenConfig( DsoScreenMode scrMode );
    DsoErr screenRedraw( DsoScreenMode scrMode );

    DsoErr screenSplit( DsoScreenMode scrMode );
    DsoErr screenApply();

    bool getFftHalf(const QString &name);

    DsoErr setVertExpand( VertExpand vExpand );
    VertExpand getVertExpand();

    DsoErr onSettingModified( int id );
    DsoErr onSettingLoad( int para );

    DsoErr onTimeOut( int id );

    DsoErr setWorkMode( DsoWorkMode mode );
    DsoWorkMode getWorkMode();

    DsoErr setLedReq( CArgument arg );

    DsoErr onServiceTick( int tickms );
    DsoErr onEngineTick( int tickms );

    void   checkSoftIrq();
    DsoErr startSysTimer();

public slots:
    void   onSysTimeout();

protected:
    DsoErr saveSession();
    DsoErr loadSession();

    DsoErr saveStream( const QString &name, QByteArray &ar );
    DsoErr loadStream( const QString &name, QByteArray &ar );

    DsoErr pullBinSetup( QByteArray &ary );
    DsoErr pullBinSetup( QByteArray &ary, servItemList &itemList );
    DsoErr pullNvInfo( QByteArray &ary );
    DsoErr pullNvInfo( struNvInfo &info );

    DsoErr pullPrivate( QByteArray &ary );

    DsoErr checkStream( QByteArray &ary, quint32 *pLen );
    DsoErr pushBinSetup( QByteArray &ary );
    DsoErr pushBinSetup( QByteArray &ary, QList<int> &servLists );
    DsoErr dispatchStream( int servId,
                           serialVersion ver,
                           CStream &stream );
    DsoErr startupBinSetup( QList<int> &servLists );

    DsoErr pushNvInfo( QByteArray &ary );
    DsoErr pushNvInfo( struNvInfo &info );

    DsoErr clearPrivacy();
    DsoErr loadPrivacy();

private:
    DsoScreenMode mScrMode;
    VertExpand    mVertExpand;

    fram_disk::CFram mFram;

    union softInt
    {
        struct
        {
            bool mbLaProbe : 1;
        };
        quint32 ival;
    };

    softInt mIntNow;
    softInt mIntMask;
    softInt mIntLast;

    CSystemTimer *m_pSysTimer;
};



#endif // SERVDSO

