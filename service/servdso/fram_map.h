#ifndef FRAM_MAP
#define FRAM_MAP

#define nv_start    0
#define nv_size     256

#define private_start   256
#define private_size    (2048-256)

#define setup_start 2*1024
#define setup_size  5*1024

#define res_start   7*1024
#define res_size    1*1024

#endif // FRAM_MAP

