#ifndef SERVDSO_TIMER_H
#define SERVDSO_TIMER_H

#include <QObject>
#include <QTimer>
#include <QThread>

class CSystemTimer : public QObject
{
    Q_OBJECT
public:
    explicit CSystemTimer(QObject *parent = 0);
    ~CSystemTimer();

private:
    QTimer      *m_pTimer;       //定时器对象
    QThread     *m_pTimerThread; //定时器依赖线程

    int          m_nTimeout;
signals:
    void startSignal( int nMsc );//开启定时器信号
    void stopSignal();           //停止定时器信号
    void sigTimeOut();              //定时器触发，外部需连接此信号
    void deletelater();          //延时删除定时器信号
public slots:
    void onTimer();              //对象内部定时触发槽函数，向外部发射定时器触发信号
public:
    void StartTimer( int nMsc ); //开启定时器
    void StopTimer();            //关闭定时器
    void DeleteLater();          //延时删除定时器对象

};

#endif // SERVDSO_TIMER_H

