
#include "../../include/dsostd.h"

#include "../service.h"
#include "../service_msg.h"
#include "../servmath/servmath.h"
#include "../../service/service_name.h"
#include "../../engine/base/display/enginecfgscr.h"
#include "../../app/appdisplay/cgrid.h"

#include "servdso.h"


IMPLEMENT_CMD( serviceExecutor, servDso  )
start_of_entry()

#ifndef _GUI_CASE
on_set_void_void( MSG_HOR_TIME_MODE,        &servDso::on_view_changed ),
on_set_void_void( MSG_HOR_ZOOM_ON,          &servDso::on_view_changed),
on_set_void_void( MSG_HOR_XY_MODE_YT_DISP,  &servDso::on_view_changed ),
on_set_void_void( MSG_MATH_S32FFTSCR,       &servDso::on_view_changed ),
#endif
on_get_int( servDso::CMD_SCREEN_MODE, &servDso::getScreenMode ),

on_set_int_int( servDso::CMD_VERT_EXPAND, &servDso::setVertExpand ),
on_get_int( servDso::CMD_VERT_EXPAND, &servDso::getVertExpand ),

on_set_int_void( servDso::CMD_FLUSH_CACHE, &servDso::saveSession ),

#ifndef _DEBUG
on_set_int_int( CMD_SERVICE_MODIFIED, &servDso::onSettingModified ),
#endif
on_set_int_int( CMD_SERVICE_SETTING_LOAD, &servDso::onSettingLoad ),

on_set_void_void( CMD_SERVICE_INIT, &servDso::init ),
on_set_void_void( CMD_SERVICE_RST, &servDso::rst ),
on_set_int_void ( CMD_SERVICE_STARTUP, &servDso::startup ),

on_set_int_void ( CMD_SERVICE_LOAD_PRIVACY, &servDso::loadPrivacy),



on_set_int_int( servDso::CMD_WORK_MODE, &servDso::setWorkMode ),
on_get_int    ( servDso::CMD_WORK_MODE, &servDso::getWorkMode ),

on_set_int_arg( CMD_SERVICE_LED_REQUESET, &servDso::setLedReq ),

on_set_int_int( CMD_SERVICE_TICK,    &servDso::onServiceTick ),
on_set_int_int( CMD_ENGINE_TICK,     &servDso::onEngineTick ),

on_set_int_int( CMD_SERVICE_TIMEOUT, &servDso::onTimeOut ),

on_set_int_void(servDso::CMD_START_TIMER,&servDso::startSysTimer),

on_set_int_void( servDso::CMD_CLEAR_PRIVACY, &servDso::clearPrivacy ),

end_of_entry()

#define SAVE_TIME_OUT       (1000/2 ) //! 1s
#define TICK_TIME_OUT       (2000)    //! 2s
#define TICK_PROBE_OUT      (1000)    //! 1s

#define SAVE_TIMER_ID       1
#define TICK_TIMER_ID       2
#define TICK_PROBE_ID       3


MemFile servDso::_memFile;

/*!
 * \brief servCH::checkRange
 * \param path
 * \param val
 * \return
 * -从元数据中提取范围进行判定
 * -限定输入的范围
 * -返回判定的结果错误码
 * -boardmeta
 */
DsoErr servDso::checkRange( const QString &path,
                            int &val,
                            const r_meta::CMeta &rMeta )
{
    DsoErr err;

    rMeta.limitRange( path, val, val, err );

    return err;
}

DsoErr servDso::checkRange( const QString &path,
                            qlonglong &val,
                            const r_meta::CMeta &rMeta )
{
    DsoErr err;

    rMeta.limitRange( path, val, val, err );

    return err;
}

int servDso::getIdBase( int servId )
{
    Q_ASSERT( servId > 0 );
    return servId * 64;//modified by hxh
}

int servDso::save( int id, void *data, int size )
{
    LOG_DBG()<<id<<size;
    return servDso::_memFile.save( id, data, size );
}

int servDso::load( int id, void *data, int cap )
{
    return servDso::_memFile.load( id, data, cap );
}

int servDso::getLen( int id )
{
    return servDso::_memFile.getLen( id );
}

DsoErr servDso::remove( int id )
{
    if ( 0 != servDso::_memFile.remove( id ) )
    {
        return ERR_FILE_DELETE_FAIL;
    }
    else
    {
        return ERR_NONE;
    }
}

DsoErr servDso::format()
{
    if ( 0 != servDso::_memFile.format() )
    {
        return ERR_FILE_DELETE_FAIL;
    }
    else
    {
        return ERR_NONE;
    }
}

servDso::servDso( QString name, ServiceId id )
        : serviceExecutor(name, id )
{
    serviceExecutor::baseInit();

    mScrMode = screen_unk;

    m_pSysTimer = NULL;
}

void servDso::registerSpy()
{
#ifndef _GUI_CASE
    spyOn( serv_name_hori, MSG_HOR_TIME_MODE );
    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON );
    spyOn( serv_name_hori, MSG_HOR_XY_MODE_YT_DISP );

    //! math
    spyOn( serv_name_math1, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math2, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math3, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math4, MSG_MATH_S32FFTSCR );

    spyOn( serv_name_math1, MSG_MATH_EN, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math2, MSG_MATH_EN, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math3, MSG_MATH_EN, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math4, MSG_MATH_EN, MSG_MATH_S32FFTSCR );

    spyOn( serv_name_math1, MSG_MATH_S32MATHOPERATOR, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math2, MSG_MATH_S32MATHOPERATOR, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math3, MSG_MATH_S32MATHOPERATOR, MSG_MATH_S32FFTSCR );
    spyOn( serv_name_math4, MSG_MATH_S32MATHOPERATOR, MSG_MATH_S32FFTSCR );
#endif
}

DsoErr servDso::start()
{
    startTimer( TICK_TIME_OUT, TICK_TIMER_ID, timer_repeat );
    startTimer( TICK_PROBE_OUT, TICK_PROBE_ID, timer_repeat );

    m_pSysTimer = new CSystemTimer();
    Q_ASSERT( m_pSysTimer != NULL );

    m_pSysTimer->StartTimer(10);
    return ERR_NONE;
}

DsoErr servDso::startSysTimer()
{
    return ERR_NONE;
}

void servDso::onSysTimeout()
{
    qDebug() << "Timeout";
}

void servDso::init()
{
    //! interval
    mIntNow.ival = 0;
    mIntLast.ival = 0;
    mIntMask.ival = 0;
}

void servDso::rst()
{
    mScrMode    = screen_unk;
    mVertExpand = vert_expand_gnd;
}

int servDso::startup()
{
    on_view_changed();

    return 0;
}

void servDso::on_view_changed()
{
    //! get the time mode
    int timeMode;
    DsoScreenMode scrMode;

#ifndef _GUI_CASE
    query( serv_name_hori, MSG_HOR_TIME_MODE, timeMode );
    if ( timeMode == Acquire_XY )
    {
        bool bFullXy;
        query( serv_name_hori, MSG_HOR_XY_MODE_YT_DISP, bFullXy );

        if ( bFullXy )
        {
           scrMode = screen_xy_full;
        }
        else
        {
           scrMode = screen_xy_normal;
        }
    }
    else if ( timeMode == Acquire_YT )
    {
        bool bZoom;
        query( serv_name_hori, MSG_HOR_ZOOM_ON, bZoom );

        if ( bZoom )
        {
            scrMode = screen_yt_main_zoom;
        }
        else
        {
            scrMode = screen_yt_main;
        }

        if(getFftHalf(serv_name_math1))
        {
            scrMode = screen_yt_main_zfft;
        }
    }
    else
    {
        scrMode = screen_roll_main;
    }

    //! change view
    if ( mScrMode != scrMode )
    {
        mScrMode = scrMode;
        defer( CMD_VIEW_CHANGE );

        screenConfig( mScrMode );
        screenRedraw( mScrMode );
    }
#endif
}

DsoScreenMode servDso::getScreenMode()
{
    if( screen_unk == mScrMode )
    {
        return screen_yt_main;
    }
    return mScrMode;
}

//! \todo by servmeta
//! -1 ---- wpu 0~479

DsoErr servDso::screenConfig( DsoScreenMode scrMode )
{
    DsoErr err;

    err = screenSplit( scrMode );
    if ( err != ERR_NONE )
    { return err; }

    err = screenApply();
    if ( err != ERR_NONE )
    { return err; }

    postEngine( ENGINE_DISP_CLEAR, false);

    return ERR_NONE;
}

//! screen mode -> screens
DsoErr servDso::screenSplit( DsoScreenMode scrMode )
{
    dsoPanel *pPanel = dsoPanel::instance();

    Q_ASSERT( NULL != pPanel );

    if ( scrMode == screen_yt_main || scrMode == screen_roll_main )
    {
        (*pPanel)[0]->setGeometry(0,0,wave_width,wave_height);
        (*pPanel)[0]->set( true );

        (*pPanel)[1]->setGeometry(0,0,wave_width,wave_height);
        (*pPanel)[1]->set( false );
    }
    else if ( scrMode == screen_yt_main_zoom )
    {
        (*pPanel)[0]->setGeometry(0,0,wave_width,144);
        (*pPanel)[0]->set( true );


        (*pPanel)[1]->setGeometry(0,144+16,wave_width,320);
        (*pPanel)[1]->set( true );
    }
    else if ( scrMode == screen_yt_main_zfft )
    {
        (*pPanel)[0]->setGeometry(0,0,wave_width,144);
        (*pPanel)[0]->set( true );

        (*pPanel)[1]->setGeometry(0,144+16,wave_width,320);
        (*pPanel)[1]->set( true, screen_pic );
    }
    else if ( scrMode == screen_xy_full )
    {
        (*pPanel)[0]->setGeometry(0,0,wave_width,wave_height);
        (*pPanel)[0]->set( true );

        (*pPanel)[1]->setGeometry(0,144+16,wave_width,320);
        (*pPanel)[1]->set( false );
    }
    else if ( scrMode == screen_xy_normal)
    {
        (*pPanel)[0]->setGeometry(0,0,wave_width,wave_height);
        (*pPanel)[0]->set( true );

        (*pPanel)[1]->setGeometry(0,144+16,wave_width,320);
        (*pPanel)[1]->set( false );
    }
    else
    {
        (*pPanel)[0]->setGeometry(0,0,wave_width,wave_height);
        (*pPanel)[0]->set( true );

        (*pPanel)[1]->setGeometry(0,144+16,wave_width,320);
        (*pPanel)[1]->set( false );
    }

    return ERR_NONE;
}

//! screens -> wpu
DsoErr servDso::screenApply()
{
    dsoPanel *pPanel = dsoPanel::instance();
    Q_ASSERT( NULL != pPanel );

    EngineCfgScr scrCfg;
    dsoScreen *pScreen;

    for ( int i = 0; i < pPanel->screenCount(); i++ )
    {
        pScreen = (*pPanel)[i];
        Q_ASSERT( NULL != pScreen );

        //! visible
        if ( pScreen->getVisible() && pScreen->getContent() == screen_wave )
        {
            scrCfg.cfg( i, true, pScreen->height(), pScreen->height()/2 + pScreen->top() - 1 );
        }
        //! invisible
        else
        {
            scrCfg.cfg( i, false, wave_height, 0 );
        }

        postEngine( ENGINE_DISP_SCREEN, &scrCfg );
    }

    return ERR_NONE;
}

bool servDso::getFftHalf(const QString &name)
{
    bool bVal;
    serviceExecutor::query( name, MSG_MATH_EN,  bVal);

    if(bVal)
    {
        int iVal;
        serviceExecutor::query( name, MSG_MATH_S32MATHOPERATOR,  iVal);
        if(iVal == operator_fft)
        {
            serviceExecutor::query( name, MSG_MATH_S32FFTSCR,  bVal);
            //! full screen
            if ( bVal )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

DsoErr servDso::setVertExpand( VertExpand vExpand )
{
    mVertExpand = vExpand;

    return ERR_NONE;
}
VertExpand servDso::getVertExpand()
{
    return mVertExpand;
}

DsoErr servDso::onSettingModified( int /*id*/  )
{
    //! no change
    if ( !sysGetStarted() )
    { return ERR_NONE; }

    //! timeout to save settting
    startTimer( SAVE_TIME_OUT, SAVE_TIMER_ID );

    return ERR_NONE;
}

DsoErr servDso::onSettingLoad( int /*para*/ )
{
    DsoErr err;

    err = loadSession();

    return err;
}

DsoErr servDso::onTimeOut(int id)
{
    if ( id == SAVE_TIMER_ID )
    {
        return saveSession();
    }
    else if ( id == TICK_TIMER_ID )
    {
        sysAccCycleCnt( TICK_TIME_OUT/1000 );
    }
    else if ( id == TICK_PROBE_ID )
    {
        checkSoftIrq();
    }
    else
    {
    }

    return ERR_NONE;
}

DsoErr servDso::setWorkMode( DsoWorkMode mode )
{
    serviceExecutor::_workMode = mode;
    return ERR_NONE;
}
DsoWorkMode servDso::getWorkMode()
{ return serviceExecutor::_workMode; }

DsoErr servDso::setLedReq( CArgument arg )
{
    if ( arg.size() != 2 )
    { return ERR_INVALID_INPUT; }

    if ( arg[0].vType != val_int ||
         arg[1].vType != val_bool )
    { return ERR_INVALID_INPUT; }

    if ( arg[1].bVal )
    {
        syncEngine( ENGINE_LED_ON, arg[0].iVal );
    }
    else
    {
        syncEngine( ENGINE_LED_OFF, arg[0].iVal );
    }

    return ERR_NONE;
}

DsoErr servDso::onServiceTick( int tmoms )
{
    //! timer tick
    serviceExecutor::tickIt( tmoms );

    return ERR_NONE;
}

DsoErr servDso::onEngineTick(int tickms)
{
    dsoEngine::timerTick( tickms );
    return ERR_NONE;
}

void servDso::checkSoftIrq()
{
    //! get bit
    bool bLaProbe;
    queryEngine( qENGINE_LA_PROBE, bLaProbe );

    mIntNow.mbLaProbe = bLaProbe;

    //! check interrupt
    quint32 changed;
    changed = mIntLast.ival ^ mIntNow.ival;
    //! unmask
    unset_attr( quint32, changed, mIntMask.ival );
    //! change input
    if ( changed != 0 )
    {
        if ( get_bit(changed, 0) )
        {
            serviceExecutor::post( serv_name_la,
                                   CMD_SERVICE_SOFT_INT,
                                   (int)mIntNow.ival );
        }
    }

    //! save
    mIntLast.ival = mIntNow.ival;
}


DsoErr servDso::screenRedraw(DsoScreenMode scrMode)
{
    CGrid *m_pGrid = CGrid::getCGridInstance();
    if( m_pGrid != NULL )
    {
        if ( scrMode == screen_yt_main
             || scrMode == screen_roll_main
             )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(false);
            m_pGrid->setXYFull(false);
        }
        else if ( scrMode == screen_yt_main_zoom )
        {
            float scale = 500000.0f;
            query(serv_name_hori, servHori::cmd_zoom_scale_real, scale);
            m_pGrid->getZoomScale()->setValue( scale );

            m_pGrid->setZoom(true);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(false);
            m_pGrid->setXYFull(false);
        }
        else if ( scrMode == screen_yt_main_zfft )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(true);
            m_pGrid->setFFTFull(false);

            m_pGrid->setXYMode(false);
            m_pGrid->setXYFull(false);
        }
        else if ( scrMode == screen_xy_full )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(true);
            m_pGrid->setXYFull(true);
        }
        else if ( scrMode == screen_xy_normal )
        {
            m_pGrid->setZoom(false);

            m_pGrid->setFFT(false);

            m_pGrid->setXYMode(true);
            m_pGrid->setXYFull(false);
        }
        else
        {}

        if( sysGetStarted() )
        {
            QCoreApplication::processEvents();
        }
    }


    return ERR_NONE;
}
