#ifndef MEMFILE_H
#define MEMFILE_H

#include <QtCore>

#include "../../include/dsotype.h"

using namespace DsoType;

struct SegAttr
{
    int mId, mInvId;
    int mLen, mInvLen;

    quint32 mCrc;
};

class SegFile : public SegAttr
{
public:
    SegFile( int id );
    ~SegFile();

public:
    SegFile &operator=( SegAttr &attr );
    void getAttr( SegAttr &attr );
public:
    int serialOut( void *pData );
    int serialIn( void *pData );

    int write( void *pData, int size );
    int read( void *pData, int size );

    int getSize();
    int getLen();

protected:
    void setAttr( SegAttr *pAttr );
    bool checkAttr( SegAttr *pAttr );

public:
    quint8 *m_pMem;
};

class MemFile
{
public:
    MemFile();

public:
    int save( int id, void *pData, int size );
    int load( int id, void *pData, int cap );
    int getLen( int id );
    int remove( int id );
    int format();

protected:
    SegFile *find( int id );
    void clear( QList<SegFile*> &segs );

public:
    int serialIn( void *pStream );
    int serialOut( void *pStream, int cap );
    int getSize();

    bool isModified();
    void setModified( bool b );

    void save( const QString &file );
    void load( const QString &file );

private:
    bool mbModified;
    QList< SegFile *> mSegs;
};

#endif // MEMFILE_H
