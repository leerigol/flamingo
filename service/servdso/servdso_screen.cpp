#include "servdso.h"

dsoScreen::dsoScreen()
{
    mbVisible = false;
    mContent = screen_pic;
}

void dsoScreen::setGeometry( int x, int y, int w, int h )
{
    setRect( x, y, w, h );
}
void dsoScreen::setGeometry( const QRect &rect )
{
    setRect( rect.left(), rect.top(),
             rect.width(), rect.height() );
}
QRect &dsoScreen::getGeomerty()
{
    return *this;
}

void dsoScreen::setVisible( bool b )
{
    mbVisible = b;
}
bool dsoScreen::getVisible()
{ return mbVisible; }

void dsoScreen::setContent( screenContent cont )
{ mContent = cont; }
screenContent dsoScreen::getContent()
{ return mContent; }

void dsoScreen::set( bool b, screenContent cont )
{
    setVisible( b );
    setContent( cont );
}

//! dsoPanel
dsoPanel *dsoPanel::_instance = NULL;
dsoPanel* dsoPanel::instance()
{
    //! has created
    if ( NULL != dsoPanel::_instance )
    { return dsoPanel::_instance; }

    //! create it
    dsoPanel::_instance = new dsoPanel();
    Q_ASSERT( NULL != dsoPanel::_instance);
    return dsoPanel::_instance;
}

//! at least one screen
dsoPanel::dsoPanel()
{
    //! create only once
    Q_ASSERT( mScreens.size() == 0 );

    dsoScreen *pScreen = new dsoScreen();
    Q_ASSERT( NULL != pScreen );

    mScreens.append( pScreen );
}

dsoPanel::~dsoPanel()
{
    foreach( dsoScreen *pScreen, mScreens )
    {
        Q_ASSERT( NULL != pScreen );
        delete pScreen;
    }
}

void dsoPanel::setGeometry( int index, const QRect &rect )
{
    (*this)[ index ]->setGeometry( rect );
}
QRect &dsoPanel::getGeometry( int index )
{ return (*this)[index]->getGeomerty(); }

int dsoPanel::screenCount()
{ return mScreens.size(); }

dsoScreen *dsoPanel::operator[]( int index )
{
    Q_ASSERT( index >=0 );

    alloc( index );

    return mScreens[index];
}

void dsoPanel::alloc( int index )
{
    Q_ASSERT( index >=0 );

    //! allocate the item
    dsoScreen *pScreen;
    for ( int i = mScreens.size(); i < index + 1; i++ )
    {
        pScreen = new dsoScreen();
        Q_ASSERT( NULL != pScreen );

        mScreens.append( pScreen );
    }
}

dsoPanel * servDso::getPanel()
{
    return dsoPanel::instance();
}
