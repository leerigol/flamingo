#ifndef _SYSDSO_
#define _SYSDSO_

#include <QtWidgets>
#include <QEvent>

#include "../../include/dsotype.h"
#include "../../include/dsoscpievent.h"
#include "../../baseclass/cargument.h"
#include "../../menu/arch/rservmenu.h"

#include "../../menu/event/rkeymapper.h"

#include "../../service/servlicense/servlicense.h"

using namespace DsoType;


struct sysDso
{
    bool mLoadCompleted;        //! load completed

    QWidget *m_pMainWindow;     /*!< 主窗口*/
    QWidget *m_pMenuWindow;     /*!< 菜单窗口*/
    QWidget *m_pCentralWindow;  /*!< central窗口*/
    QWidget *m_pBgWindow;
    menu_res::serviceMenuList *m_pMenuList;

    menu_res::RKeyMapper *m_pMapper;

    SystemLanguage mLanguage;   /*!< 系统语言*/
    QString mResourcePath;      /*!< 资源路径*/

    int mArgc;                  //! readonly
    char **mArgv;

    bool mbSuspend;             //! recover setting
    quint64 mCycleCnt;          //! cycle cnt
    quint32 mLiveCnt;
    bool    bTouchEnable;
};

//! system api
vertAttr sysGetVertAttr( Chan ch );
horiAttr sysGetHoriAttr( Chan ch, HorizontalView view );

#define getVertAttr( ch )   sysGetVertAttr( ch )
#define getHoriAttr( ch, view )   sysGetHoriAttr( ch, view )

/*! - api用于简化应用程序的访问
 *  - 后台可能对应了多个服务
*/
void sysSetMainWindow( QWidget *wnd );
void sysSetMidWindow( QWidget *wnd );
void sysSetBgWindow( QWidget *wnd );

QWidget *sysGetMainWindow();
QWidget *sysGetMidWindow();
QWidget *sysGetBgWindow();


void sysSetMenuWindow( QWidget *wnd );
QWidget *sysGetMenuWindow();

//! 从系统资源里查找到资源ID所对应的字符
QString sysGetString( int msg, const QString &defStr="" );
QString sysGetOptionString(int msg, int opt, const QString &defStr);

QString sysTrimString( const QString &str, int maxSize );

//! 查找ID所对应的图片
int sysGetImage( int msg, QImage &img );

int sysGetResource( int msg, QString &str, QImage &img );

QWidget *sysGetWidget( const QString &str, int msg );

QRect sysGetDesktopRect();

void sysSetLanguage( SystemLanguage lang );
SystemLanguage sysGetLanguage();

void sysSetResourcePath( const QString &path );
QString &sysGetResourcePath();

void sysSetMenuList( menu_res::serviceMenuList *pList );
menu_res::serviceMenuList *sysGetMenuList();

QWidget * sysGetView( const QString &servName,
                    int msg );

void sysSetKeyMapper( menu_res::RKeyMapper *pMapper );
menu_res::RKeyMapper *sysGetKeyMapper();

void sysMimicKey( int pkey );
void sysMimicInc( int key );
void sysMimicDec( int key );

void sysMimcKey( const QString &name, int msg, int skey );

void sysSetArg( int argc, char **argv );
int  sysGetArgc();
char** sysGetArgv();

char* sysGetArg( int i, bool *pbValid );
int   sysGetArgInt( int i, bool *pbValid );
int   sysCmpArg( int i, const QString &str, bool caseSen = false );
bool  sysHasArg( const QString &str, bool caseSen = false );

void sysSetSuspend( bool b );
bool sysGetSuspend();

void    sysSetLiveCnt( quint32 cnt );
quint32 sysGetLiveCnt();

void    sysSetCycleCnt( quint64 cycle );
quint64 sysGetCycleCnt();

void    sysAccCycleCnt( quint64 acc );

void sysSetVertExpand( VertExpand expand );
VertExpand sysGetVertExpand();

Bandwidth sysGetBw();
qlonglong sysGetMinHScale();

void sysSetWorkMode( DsoWorkMode mode );
DsoWorkMode sysGetWorkMode();

void sysShowErr( DsoErr err );

void sysSetStarted();
bool sysGetStarted();

bool sysCheckLicense( OptType opt );
bool sysHasLA();
bool sysHasDG();

bool sysGetTouchEnable();
void sysSetTouchEnable(bool b);

int sysGetSystemStatus();

enum StepType
{
    INC_STEP,
    DEC_STEP,
};

/* calculating the step according to the real value */
qint64 getTimeStep(qint64 t);
qint64 getFreqStep(qint64 freq);
qint64 getNumStep(long long currentValue, StepType type = INC_STEP);
qint64 getNumStep(int currentValue);

int    getVoltStep(int volt);

QString toStringIP(int);
int     toDigitIP(QString&);
int     sysGetNormalSpace(QString &str);

/*!* \brief getChColor */
QColor  sysGetChColor(Chan src);

/*!-------sys scpi--------*/
void sysSetScpiEvent( dsoScpiEvent event, int bitVal );

void sysScpiFilterServive(QString servName, bool b);

void sysScpiAddMap(QString servName, QList<int> &cmdList);
void sysScpiExecuteEn(bool b);

void sysScpiExeLock();
void sysScpiExeUnLock();

void sysKeyPress( int key );
void sysKeyDecrease( int key, int cnt );
void sysKeyIncrease( int key, int cnt );
#endif // SYSDSO

