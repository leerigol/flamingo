#ifndef SERVICE_NAME
#define SERVICE_NAME

//! service names
//! chan
#define serv_name_ch1         "chan1"
#define serv_name_ch2         "chan2"
#define serv_name_ch3         "chan3"
#define serv_name_ch4         "chan4"

#define serv_name_vert_mgr    "vertMgr"

//! hori
#define serv_name_hori        "hori"

//! scpi
#define serv_name_scpi        "scpi"
#define serv_name_common_cmd  "commoncmd"

//! math
#define serv_name_math1       "math1"
#define serv_name_math2       "math2"
#define serv_name_math3       "math3"
#define serv_name_math4       "math4"

#define serv_name_math_sel    "mathsel"
#define serv_name_math_fft    "fftsel"

//! cursor
#define serv_name_cursor      "cursor"

//! plot
#define serv_name_plot        "PLOT"

//! ref
#define serv_name_ref         "ref"

//! search

#define serv_name_search      "search"

//! la
#define serv_name_la          "la"

//! Mask
#define serv_name_mask        "mask"

//! display
#define serv_name_display     "display"

//!Wrec
#define serv_name_wrec        "record"

//!utility
#define serv_name_utility       "utility"
#define serv_name_utility_IOset "ioset"
#define serv_name_utility_Print "printer"
#define serv_name_utility_Email "email"

#define serv_name_utility_Option "option"
#define serv_name_utility_Wifi   "wifi"

//!storage
#define serv_name_storage        "storage"

//!source
#define serv_name_source1        "source1"
#define serv_name_source2        "source2"

//! trigger
#define serv_name_trigger           "trigger"
#define serv_name_trigger_zone      "trigger.zone"
#define serv_name_trigger_edge      "trigger.edge"
#define serv_name_trigger_pulse     "trigger.pulse"
#define serv_name_trigger_slope     "trigger.slope"
#define serv_name_trigger_video     "trigger.video"
#define serv_name_trigger_pattern   "trigger.pattern"
#define serv_name_trigger_duration  "trigger.duration"
#define serv_name_trigger_timeout   "trigger.timeout"
#define serv_name_trigger_runt      "trigger.runt"
#define serv_name_trigger_over      "trigger.over"
#define serv_name_trigger_window    "trigger.window"
#define serv_name_trigger_delay     "trigger.delay"
#define serv_name_trigger_setup     "trigger.setup"
#define serv_name_trigger_nth       "trigger.nth"
#define serv_name_trigger_rs232     "trigger.rs232"
#define serv_name_trigger_i2c       "trigger.i2c"
#define serv_name_trigger_i2s       "trigger.i2s"
#define serv_name_trigger_spi       "trigger.spi"
#define serv_name_trigger_lin       "trigger.lin"
#define serv_name_trigger_1553b     "trigger.1553b"
#define serv_name_trigger_can       "trigger.can"
#define serv_name_trigger_flexray   "trigger.flexray"
#define serv_name_trigger_ab        "trigger.ab"
#define serv_name_trigger_sbus      "trigger.sbus"
#define serv_name_trigger_429       "trigger.429"
#define serv_name_trigger_1wire     "trigger.1wire"

#define serv_name_quick             "quick"
#define serv_name_misc              "misc"
#define serv_name_hotplug           "hotplug"
//! tre
#define serv_name_trace             "TRACE"

//! test
#define serv_name_menutester        "menutest"
#define serv_name_menutester2       "test2"

//! decode
#define serv_name_decode1           "decode1"
#define serv_name_decode2           "decode2"
#define serv_name_decode3           "decode3"
#define serv_name_decode4           "decode4"

#define serv_name_decode_sel        "vdecodesel"

//! measure
#define serv_name_measure           "measure"

//! Counter
#define serv_name_counter           "counter"

//! DVM
#define serv_name_dvm               "DVM"

//! UPA
#define serv_name_upa               "UPA"
#define serv_name_upa_powerq        "UPA.powerQuality"
#define serv_name_upa_ripple        "UPA.ripple"

//! EyeJit
#define serv_name_eyejit            "EyeJit"

//! gui
#define serv_name_gui               "gui"
#define serv_name_dso               "servdso"
#define serv_name_help              "help"

//! license
#define serv_name_license           "license"

//! cal
#define serv_name_cal               "selfcal"

//!wfm
#define serv_name_wfm_data          "wfmdata"

//!wfm
#define serv_name_memory            "memory"

//! auto
#define serv_name_autoset           "autoset"

//! histo
#define serv_name_histo              "histo"
#endif // SERVICE_NAME

