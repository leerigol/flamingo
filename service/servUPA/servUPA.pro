######################################################################
# Automatically generated by qmake (3.0) ?? 12? 7 21:19:13 2016
######################################################################

QT       += core
QT       += gui
QT       += widgets

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/services/servUPA
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

#DEFINES += _DEBUG

# Input
HEADERS += powerQuality.h ripple.h servUPA.h servUPABase.h upapowerqdata.h \
    uparippledata.h
SOURCES += powerQuality.cpp \
           ripple.cpp \
           servUPA.cpp \
           servUPABase.cpp \
           upapowerqdata.cpp \
    uparippledata.cpp
