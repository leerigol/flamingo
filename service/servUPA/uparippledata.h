#ifndef UPARIPPLEDATA_H
#define UPARIPPLEDATA_H

#include "../servmeasure/meas_interface.h"

class CUpaRippleData
{
public:
    CUpaRippleData(Chan src);

public:
    void setSrc(Chan src);
    void calcRippleVal( bool zoomOn);

    MeasValue getRippleVal();
    MeasStatus getDataStatus();

private:
    Chan m_Src;

    meas_algorithm::MeasRipple m_RawRippleVal;
    MeasValue m_RippleVal;
};

#endif // CUPARIPPLEDATA_H
