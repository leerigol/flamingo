#include "ripple.h"

#include "../../service/servtrace/servtrace.h"
#include "servUPA.h"

#define dbg_out()

IMPLEMENT_CMD( servUPABase, servRipple )
start_of_entry()
on_set_int_int  ( MSG_UPA_TYPE,                    &servUPABase::cfg_AnalysisType ),
on_get_int      ( MSG_UPA_TYPE,                    &servUPABase::get_SelfAnalysisType ),

on_set_int_int  ( MSG_UPA_STAT_COUNT,              &servUPABase::set_StatCount ),
on_get_int      ( MSG_UPA_STAT_COUNT,              &servUPABase::get_StatCount ),
on_set_int_void ( MSG_UPA_STAT_RESET,              &servUPABase::set_StatReset ),

on_set_int_int  ( MSG_UPA_RIPPLE_SOURCE,           &servRipple::set_Source ),
on_get_int      ( MSG_UPA_RIPPLE_SOURCE,           &servRipple::get_Source ),

on_set_int_void ( MSG_UPA_RIPPLE_AUTOSET,          &servRipple::set_AutoSet ),

on_set_int_bool ( MSG_UPA_RIPPLE_DISP,             &servRipple::set_Display ),
on_get_bool     ( MSG_UPA_RIPPLE_DISP,             &servRipple::get_Display ),

on_set_int_void ( MSG_UPA_RIPPLE_TIPS,             &servRipple::set_Tips ),

on_set_void_void( CMD_SERVICE_RST,                 &servRipple::rst ),
on_set_int_void ( CMD_SERVICE_STARTUP,             &servRipple::startup ),
on_set_int_int  ( CMD_SERVICE_ACTIVE,              &servRipple::setActive ),
on_set_void_void( CMD_SERVICE_INIT,                &servRipple::init ),
end_of_entry()

servRipple::servRipple(QString name) : servUPABase( name, UPA_Ripple, E_SERVICE_ID_UPA_RIPPLE )
{
    serviceExecutor::baseInit();
    m_Source = chan1;
    m_Display = false;
}

int servRipple::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;
    stream.write(QStringLiteral("m_Source"),       m_Source);
    stream.write(QStringLiteral("m_Display"),      m_Display);

    servUPABase::serialOut(stream, ver);

    return ERR_NONE;
}

int servRipple::serialIn( CStream &stream, serialVersion ver )
{
    if( ver == mVersion)
    {
        stream.read(QStringLiteral("m_Source"),  m_Source);
        stream.read(QStringLiteral("m_Display"), m_Display);

        //servUPABase::serialIn(stream, ver);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

void servRipple::rst()
{
    m_Source = chan1;
    m_Display = false;
}


int servRipple::startup()
{
    //updateAllUi();
    if( get_AnalysisType() != m_MyAnalysisType)
    {
        return ERR_NONE;
    }
    return servUPABase::startup();
}

void servRipple::init()
{
    for( int i = (int)r1; i<=(int)r10; i++)  //! Hide Ref1-10
    {
        mUiAttr.setVisible( MSG_UPA_RIPPLE_SOURCE, i, false);
    }

    mUiAttr.setVisible( MSG_UPA_RIPPLE_AUTOSET, false);
}

DsoErr servRipple::setActive( int active)
{
    set_AnalysisType( UPA_Ripple );
    return serviceExecutor::setActive( active);
}

DsoErr servRipple::set_Source(int v)
{
    m_Source = v;

    post( E_SERVICE_ID_UPA, servUPA::cmd_set_ripple_src, m_Source);
    return ERR_NONE;
}

int servRipple::get_Source()
{
    return m_Source;
}

DsoErr servRipple::set_AutoSet()
{

    return ERR_NONE;
}

DsoErr servRipple::set_OffsetReset()
{
    return ERR_NONE;
}

DsoErr servRipple::set_Display(bool b)
{
    m_Display = b;

    if(m_Display)
    {
        async( servUPA::cmd_run_upa, (int)1, serv_name_upa );
    }
    else
    {
        async( servUPA::cmd_stop_upa, (int)1, serv_name_upa );
    }
    return ERR_NONE;
}

bool servRipple::get_Display()
{
    return m_Display;
}

DsoErr servRipple::set_Tips()
{
    return ERR_NONE;
}
