#ifndef UPAPOWERQDATA_H
#define UPAPOWERQDATA_H

#include "../servmeasure/meas_stat.h"

class CUpaPowerQData
{
public:
    struct PowerQVal
    {
        float max;
        float rms;
        float avg;
    };

    CUpaPowerQData(Chan v, Chan i, Chan freqRef, Chan p);
    virtual ~CUpaPowerQData();

    void calcStatData(ThresholdData *th,
                      meas_algorithm::StateMethodMode stateMode,
                      meas_algorithm::StateMethod topMethod,
                      meas_algorithm::StateMethod baseMethod,
                      bool zoomOn);
    QMap<MeasType, measStatData *> *getStatData(void);
    void calcPowerQData(meas_algorithm::MEASURE *freqRef,
                        PowerQVal *val, Chan src , bool zoomOn);


    void setSrcV(Chan v);
    void setSrcI(Chan i);
    void setFreqRef(Chan f);
    void setStatCount(ulong c);
    void setCycles(int c);

    Chan getSrcV(void) { return m_SrcV;}
    Chan getSrcI(void) { return m_SrcI;}
    Chan getFreqRef(void) { return m_FreqRef;}
    Chan getSrcP(void)    { return m_SrcP;}

    MeasStatus getValStatus(float val);

    void resetStatVal(void);

private:
    void calcRefFreq();
    void calcVrms();
    void calcIrms();
    void calcRealP();
    void calcApparentP();
    void calcReactiveP();
    void calcPFactor();
    void calcPhaseAngle();
    void calcImp();
    void calcVCrestFactor();
    void calcICrestFactor();

private:
    QMap<MeasType,measStatData*> m_PowerQStatData;
    measData *m_pFreqRefData;

    PowerQVal m_PowerQ_V;
    PowerQVal m_PowerQ_I;
    PowerQVal m_PowerQ_P;

    Chan      m_SrcV;
    Chan      m_SrcI;
    Chan      m_FreqRef;
    Chan      m_SrcP;
//    int       m_Cycles;
//    ulong     m_StatCount;
};

#endif // UPAPOWERQDATA_H
