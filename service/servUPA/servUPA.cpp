#include "servUPA.h"
#include "powerQuality.h"
#include "ripple.h"

#include "../../service/servtrace/servtrace.h"

#define dbg_out()


IMPLEMENT_CMD( serviceExecutor, servUPA )
start_of_entry()

on_set_int_int    ( servUPA::cmd_set_AnalysisType,       &servUPA::set_AnalysisType),
on_get_int        ( servUPA::cmd_get_AnalysisType,       &servUPA::get_AnalysisType),

on_set_int_pointer( servUPA::cmd_trace_update,           &servUPA::setUpaSrcReady),
on_set_int_int    ( servUPA::cmd_upa_update,             &servUPA::onUpaUpdate),

on_get_pointer    ( servUPA::cmd_get_ripple_val,         &servUPA::getRippleVal),
on_set_void_int   ( servUPA::cmd_set_ripple_src,         &servUPA::setRippleSrc),
on_set_void_int   ( servUPA::cmd_set_ripple_stat_cnt,    &servUPA::setRippleStatCnt),
on_set_void_int   ( servUPA::cmd_set_ripple_stat_reset,  &servUPA::setRippleStatReset),

on_get_pointer    ( servUPA::cmd_get_powerq_val,         &servUPA::getPowerQVal),
on_set_void_int   ( servUPA::cmd_set_powerq_volt,        &servUPA::setPowerQVoltSrc),
on_set_void_int   ( servUPA::cmd_set_powerq_curr,        &servUPA::setPowerQCurrSrc),
on_set_void_int   ( servUPA::cmd_set_powerq_fref,        &servUPA::setPowerQFreqRef),
on_get_int        ( servUPA::cmd_get_powerq_fref,        &servUPA::getPowerQFreqRef),

on_set_void_int   ( servUPA::cmd_set_powerq_stat_cnt,    &servUPA::setPowerQStatCnt),
on_set_void_int   ( servUPA::cmd_set_powerq_stat_reset,  &servUPA::setPowerQStatReset),

on_set_void_void  ( servUPA::cmd_run_upa,                &servUPA::runUpaThread),
on_set_void_void  ( servUPA::cmd_stop_upa,               &servUPA::stopUpaThread),
on_set_int_void  (  servUPA::cmd_close_upa_app,          &servUPA::closeUpaApp),

//!spy on
on_set_void_void( servUPA::cmd_hor_zoom_on,        &servUPA::onHorZoomOn),

on_set_void_void( CMD_SERVICE_RST,                 &servUPA::rst ),
on_set_int_void ( CMD_SERVICE_STARTUP,             &servUPA::startup ),
on_set_int_int  ( CMD_SERVICE_ACTIVE,              &servUPA::onActive ),
end_of_entry()

servUPA::servUPA( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    m_AnalysisType = UPA_PowerQ;
    runUPA = false;
    m_RippleData = NULL;
    m_RippleStatData = NULL;
    m_PowerQData = NULL;
    m_ZoomOn = false;
}

int servUPA::createServUPA(QList<service*> *pService)
{
    service *pServ;

    pServ = new servUPA(serv_name_upa);
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servPowerQ(serv_name_upa_powerq);
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    pServ = new servRipple(serv_name_upa_ripple);
    Q_ASSERT( NULL != pServ );
    pService->append( pServ );

    return 0;
}

int servUPA::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;

    stream.write(QStringLiteral("m_AnalysisType"), m_AnalysisType);
    //stream.write(QStringLiteral("runUPA"),         runUPA);

    return ERR_NONE;
}

int servUPA::serialIn(CStream &stream, serialVersion ver )
{
    if( ver == mVersion)
    {
        stream.read(QStringLiteral("m_AnalysisType"), m_AnalysisType);
        //stream.read(QStringLiteral("runUPA"),         runUPA);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

void servUPA::rst()
{
    m_AnalysisType = UPA_PowerQ;
    runUPA = false;
    m_ZoomOn = false;
    closeUpaApp();

    //serviceExecutor::post(serv_name_upa_powerq, CMD_SERVICE_ACTIVE, (int)1 );
}


int servUPA::startup()
{
//    updateAllUi();
    return 0;
}

DsoErr servUPA::start()
{
    m_pUpaThread = new upaThread(this);
    Q_ASSERT( NULL != m_pUpaThread);

    return ERR_NONE;
}

int servUPA::onActive(int id)
{
    id = id;
    QString servName = "";
    servName = servUPABase::currAnalysisType();
    serviceExecutor::post(servName, CMD_SERVICE_ACTIVE, (int)MSG_UPA_TYPE );
    return 0;
}

void servUPA::registerSpy()
{
    spyOn( E_SERVICE_ID_TRACE, servTrace::cmd_set_provider,
                            mpItem->servId, servUPA::cmd_trace_update );

    setMsgAttr( servUPA::cmd_trace_update );

    spyOn( serv_name_hori, MSG_HOR_ZOOM_ON, servUPA::cmd_hor_zoom_on );

}

void servUPA::onHorZoomOn()
{
    bool zoomOn;
    serviceExecutor::query( serv_name_hori, MSG_HOR_ZOOM_ON, zoomOn);

    m_ZoomOn = zoomOn;
}

DsoErr servUPA::set_AnalysisType(int v)
{
    m_AnalysisType = v;
    return ERR_NONE;
}


int servUPA::get_AnalysisType()
{
    return m_AnalysisType;
}

QString servUPA::getCurrServ()
{
    QString servName = "";

    switch (m_AnalysisType)
    {
    case UPA_PowerQ:
        servName = serv_name_upa_powerq;
        break;
    case UPA_Ripple:
        servName = serv_name_upa_ripple;
        break;
    default:
        servName = serv_name_upa_powerq;
        break;
    }

    return servName;
}

DsoErr servUPA::setUpaSrcReady(pointer)
{
    if( m_UpaSrcSema.available() < 1)
    {
        m_UpaSrcSema.release();
    }

    return ERR_NONE;
}

DsoErr servUPA::onUpaUpdate(int)
{
    return ERR_NONE;
}

void servUPA::calc_PowerQ()
{
    if( NULL != m_PowerQData)
    {
        bool m = false;
        int top_m  = 0;
        int base_m = 0;
        meas_algorithm::StateMethodMode method = meas_algorithm::MODE_AUTO; //! Default State Method
        meas_algorithm::StateMethod top_method = meas_algorithm::METHOD_Histogram; //! Default Top State Method
        meas_algorithm::StateMethod base_method = meas_algorithm::METHOD_Histogram; //! Default Base State Method

        serviceExecutor::query(serv_name_measure, MSG_APP_MEAS_SET_STATE_METHOD, m);
        serviceExecutor::query(serv_name_measure, MSG_APP_MEAS_SET_TOP_METHOD, top_m);
        serviceExecutor::query(serv_name_measure, MSG_APP_MEAS_SET_BASE_METHOD, base_m);

        if( m)
            method = meas_algorithm::MODE_MANUAL;
        else
            method = meas_algorithm::MODE_AUTO;

        if( top_m == 0)
            top_method = meas_algorithm::METHOD_Histogram;
        else
            top_method = meas_algorithm::METHOD_MaxMin;

        if( base_m == 0)
            base_method = meas_algorithm::METHOD_Histogram;
        else
            base_method = meas_algorithm::METHOD_MaxMin;

        int refLevelType = 0;
        qint64 refLPctHigh = 90;
        qint64 refLPctMid  = 50;
        qint64 refLPctLow  = 10;
        qint64 refLAbsHigh = 0;
        qint64 refLAbsMid  = 0;
        qint64 refLAbsLow  = 0;
        meas_algorithm::ThresholdType thType;
        ThresholdData *th;
        serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_REFL_TYPE, refLevelType);
        thType = (meas_algorithm::ThresholdType)refLevelType;
        if( thType == meas_algorithm::TH_TYPE_PER )
        {
            serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_REFL_PCT_HIGH, refLPctHigh);
            serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_REFL_PCT_MID,  refLPctMid);
            serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_REFL_PCT_LOW,  refLPctLow);

            th = new ThresholdData(m_PowerQData->getFreqRef(),
                                   thType, refLPctHigh, refLPctMid, refLPctLow, 1.0); //! base:1.0 %
//            qDebug()<<"servUPA::calc_PowerQ() new ThresholdData finish--------------";
        }
        else
        {
            serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_REFL_ABS_HIGH, refLAbsHigh);
            serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_REFL_ABS_MID,  refLAbsMid);
            serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_REFL_ABS_LOW,  refLAbsLow);
            th = new ThresholdData(m_PowerQData->getFreqRef(),
                                   thType, refLAbsHigh, refLAbsMid, refLAbsLow, 1.0); //! base:1.0 %
        }

//        qDebug()<<"servUPA::calc_PowerQ() start calcStatData";
        m_PowerQData->calcStatData(th, method, top_method, base_method, m_ZoomOn);

        Q_ASSERT( NULL != th);
        delete th;
    }
}

pointer servUPA::getPowerQVal()
{
    return m_PowerQData;
}

void servUPA::createPowerQData()
{
    if( NULL == m_PowerQData)
    {
        int v = (int)chan1;       //! Default Voltage CH
        int i = (int)chan2;       //! Default Current CH
        int freqRef = (int)chan1; //! Default FreqRef CH
        int cnt = 1000;           //! Default stat count value

        serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_POWER_VOLT, v);
        serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_POWER_CURR, i);
        serviceExecutor::query(serv_name_upa_powerq, servPowerQ::MSG_FreqRef_SOURCE, freqRef);
        //set Math1 to v*i
        MathOperator mathOp = operator_mul;
        serviceExecutor::send(serv_name_math1, MSG_MATH_S32ARITHA, v);
        serviceExecutor::send(serv_name_math1, MSG_MATH_S32ARITHB, i);
        serviceExecutor::send(serv_name_math1, MSG_MATH_S32MATHOPERATOR, mathOp);
        serviceExecutor::send(serv_name_math1, MSG_MATH_EN, true);
        serviceExecutor::post(serv_name_math1, MSG_MATH_S32RSTVSCALE, 0);


        if(  ((Chan)v != chan_none)
           ||((Chan)i != chan_none)
           ||((Chan)freqRef != chan_none) )
        {
            m_PowerQData = new CUpaPowerQData(Chan(v), Chan(i), Chan(freqRef), m1);
            Q_ASSERT( NULL != m_PowerQData);

            if( NULL != m_PowerQData)
            {
                query( serv_name_upa_powerq, MSG_UPA_STAT_COUNT, cnt);
                m_PowerQData->setStatCount(cnt);
            }
        }
    }
}

void servUPA::setPowerQVoltSrc(int v)
{
    if( NULL != m_PowerQData)
    {
        m_PowerQData->setSrcV( (Chan)v);
        bool ref;
        serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_POWER_FREF, ref);
        if( !ref)
        {
            m_PowerQData->setFreqRef( (Chan)v);
        }
        serviceExecutor::send(serv_name_math1, MSG_MATH_S32ARITHA, v);
    }
    setPowerQStatReset();
}

void servUPA::setPowerQCurrSrc(int c)
{
    if( NULL != m_PowerQData)
    {
        m_PowerQData->setSrcI( (Chan)c);
        bool ref;
        serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_POWER_FREF, ref);
        if( ref)
        {
            m_PowerQData->setFreqRef( (Chan)c);
        }
        serviceExecutor::send(serv_name_math1, MSG_MATH_S32ARITHB, c);
    }
    setPowerQStatReset();
}

void servUPA::setPowerQFreqRef(int freqRef)
{
    if( NULL != m_PowerQData)
    {
        m_PowerQData->setFreqRef( (Chan)freqRef);
    }
    setPowerQStatReset();
}

int servUPA::getPowerQFreqRef()
{
    bool fRef;
    int ch;
    serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_POWER_FREF, fRef);
    if( fRef)
    {
        serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_POWER_CURR, ch);
        return ch;
    }
    else
    {
        serviceExecutor::query(serv_name_upa_powerq, MSG_UPA_POWER_VOLT, ch);
        return ch;
    }
}

void servUPA::setPowerQStatCnt(int cnt)
{
    if( cnt > 0)
    {
        if( NULL != m_PowerQData)
        {
            m_PowerQData->setStatCount(cnt);
            post( serv_name_upa, servUPA::cmd_upa_update, m_AnalysisType);
        }
    }
}

void servUPA::setPowerQStatReset()
{
    if( NULL != m_PowerQData)
    {
        m_PowerQData->resetStatVal();
        post( serv_name_upa, servUPA::cmd_upa_update, m_AnalysisType);
    }
}

void servUPA::calc_Ripple()
{
    if( NULL != m_RippleData)
    {
        m_RippleData->calcRippleVal( m_ZoomOn);
    }

    if( NULL != m_RippleStatData)
    {
        m_RippleStatData->updateStatVal(m_RippleData->getRippleVal(),
                                        m_RippleData->getDataStatus());
    }
}

pointer servUPA::getRippleVal()
{
    return m_RippleStatData;
}

void servUPA::createUpaData( int type)
{
    LOG_DBG();
    switch ( type )
    {
    case UPA_PowerQ:
        createPowerQData();
        break;
    case UPA_Ripple:
        createRippleData();
        break;
    default:
        break;
    }
}

void servUPA::createRippleData()
{
    if( NULL == m_RippleData)
    {
        int src = (int)chan1;
        int cnt = 1000;  //! Default stat count value
        query( serv_name_upa_ripple, MSG_UPA_RIPPLE_SOURCE, src);

        LOG_DBG()<<"Ripple Source: "<<src;
        if( (Chan)src != chan_none)
        {
            LOG_DBG();
            m_RippleData = new CUpaRippleData((Chan)src);
            Q_ASSERT( NULL != m_RippleData);

            if( NULL == m_RippleStatData)
            {
                m_RippleStatData = new measStatData(Meas_Vpp, (Chan)src, chan_none);
                Q_ASSERT( NULL != m_RippleStatData);
                query( serv_name_upa_ripple, MSG_UPA_STAT_COUNT, cnt);
                m_RippleStatData->setCntLimit(cnt);
            }
        }
    }
}

void servUPA::deleteUpaData( int type )
{
    LOG_DBG();
    switch ( type )
    {
    case UPA_PowerQ:
        deletePowerQData();
        break;
    case UPA_Ripple:
        deleteRippleData();
        break;
    default:
        break;
    }
}

void servUPA::deletePowerQData()
{
    if( NULL != m_PowerQData)
    {
        delete m_PowerQData;
        m_PowerQData = NULL;

        serviceExecutor::post(serv_name_math1, MSG_MATH_EN, false);
    }
}
void servUPA::deleteRippleData()
{
    if( NULL != m_RippleData)
    {
        delete m_RippleData;
        m_RippleData = NULL;
    }

    if( NULL != m_RippleStatData)
    {
        delete m_RippleStatData;
        m_RippleStatData = NULL;
    }
}

void servUPA::setRippleSrc(int src)
{
    if( (Chan)src != chan_none)
    {
        if( NULL != m_RippleData)
        {
            m_RippleData->setSrc( (Chan)src);
        }
        if( NULL != m_RippleStatData)
        {
            m_RippleStatData->resetStatVal();
        }

        async( servUPA::cmd_upa_update, m_AnalysisType);
    }
}

void servUPA::setRippleStatCnt(int cnt)
{
    if( cnt > 0)
    {
        if( NULL != m_RippleStatData)
        {
            m_RippleStatData->setCntLimit(cnt);
            post( E_SERVICE_ID_UPA, servUPA::cmd_upa_update, m_AnalysisType);
        }
    }
}

void servUPA::setRippleStatReset()
{
    if( NULL != m_RippleStatData)
    {
        m_RippleStatData->resetStatVal();
        post( E_SERVICE_ID_UPA, servUPA::cmd_upa_update, m_AnalysisType);
    }
}

void servUPA::runUpaThread()
{
    runUPA = true;

    LOG_DBG();

    if( !(m_pUpaThread->isRunning()) )
    {
        m_pUpaThread->start();
    }
}

void servUPA::stopUpaThread()
{
    runUPA = false;
}

bool servUPA::beRunUpaThread()
{
    return runUPA;
}

int servUPA::closeUpaApp()
{
    async( MSG_UPA_RIPPLE_DISP, false, serv_name_upa_ripple );
    async( MSG_UPA_POWER_DISP, false, serv_name_upa_powerq );

    return ERR_NONE;
}

upaThread::upaThread( servUPA *pUpa) : QThread()
{
    m_pServUPA = pUpa;
}

void upaThread::run()
{
    int type = m_pServUPA->get_AnalysisType();
    m_pServUPA->createUpaData( type );
    while(m_pServUPA->beRunUpaThread())
    {
        m_pServUPA->m_UpaSrcSema.acquire();
        switch ( type )
        {
        case UPA_PowerQ:
            m_pServUPA->calc_PowerQ();
            break;
        case UPA_Ripple:
            m_pServUPA->calc_Ripple();
            break;
        default:
            break;
        }

        // UPA refresh
        serviceExecutor::post(E_SERVICE_ID_UPA, servUPA::cmd_upa_update, m_pServUPA->get_AnalysisType());

        QThread::msleep(100);
    }
    m_pServUPA->deleteUpaData( type );
}
