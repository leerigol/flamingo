#include "uparippledata.h"

CUpaRippleData::CUpaRippleData(Chan src)
{
    setSrc(src);
    m_RawRippleVal.bValid_Ripple = false;
    m_RawRippleVal.bValid_Vmax   = false;
    m_RawRippleVal.bValid_Vmin   = false;
    m_RawRippleVal.Ripple = 0;
    m_RawRippleVal.Vmax   = 0;
    m_RawRippleVal.Vmin   = 0;

    m_RippleVal.mAx.bVisible = false;
    m_RippleVal.mBx.bVisible = false;
    m_RippleVal.mAy.bVisible = false;
    m_RippleVal.mBy.bVisible = false;
    m_RippleVal.mStatus = INVALID;
    m_RippleVal.mUnit = Unit_V;
    m_RippleVal.mVal = 0;
}

void CUpaRippleData::setSrc(Chan src)
{
    m_Src = src;
}

void CUpaRippleData::calcRippleVal(bool zoomOn)
{
    int s32Len = 0;
    short s16Gnd = 0;
    float yInc = 0;
    unsigned char *pData = NULL;
    dsoVert *pVert = dsoVert::getCH( m_Src);
    m_RippleVal.mUnit = pVert->getUnit();
    DsoWfm wfm;

    if( NULL != pVert)
    {
        if( zoomOn)
        {
            pVert->getTrace(wfm, chan_none, horizontal_view_zoom);
        }
        else
        {
            pVert->getTrace(wfm, chan_none, horizontal_view_main);
        }

        s32Len = wfm.size();
        s16Gnd = wfm.getyGnd();
        pData = wfm.getPoint();
        yInc = wfm.realyInc();

        LOG_DBG() << "---------------wfm.size: "<< s32Len;
        LOG_DBG() << "---------------pData: "<< pData;

        if( NULL != pData)
        {
            if(s32Len)
            {
                meas_algorithm::UpaRippleAnalyser(pData, s32Len, s16Gnd, &m_RawRippleVal);
            }
            else
            {
                m_RawRippleVal.bValid_Ripple = false;
            }
        }
        else
        {
            m_RawRippleVal.bValid_Ripple = false;
        }
    }
    else
    {
        m_RawRippleVal.bValid_Ripple = false;
    }
    m_RippleVal.mVal = m_RawRippleVal.Ripple * yInc;

    LOG_DBG() << "-----------------m_RawRipple: "<<m_RawRippleVal.Ripple;
}

MeasValue CUpaRippleData::getRippleVal()
{
    return m_RippleVal;
}

MeasStatus CUpaRippleData::getDataStatus()
{
    if( !isinf(m_RippleVal.mVal) && !isnan(m_RippleVal.mVal))
    {
        if( (m_RawRippleVal.bValid_Vmax)&&(m_RawRippleVal.bValid_Vmin) )
        {
            return VALID;
        }
        else
        {
            return GREATER_CLIPPED;
        }
    }
    else
    {
        return INVALID;
    }
}
