#include "upapowerqdata.h"

CUpaPowerQData::CUpaPowerQData(Chan v, Chan i, Chan freqRef, Chan p)
{
    m_SrcV    = v;
    m_SrcI    = i;
    m_FreqRef = freqRef;
    m_SrcP    = p;

    m_pFreqRefData = new measData(freqRef);
    Q_ASSERT( NULL != m_pFreqRefData );

    //! local variable
    measStatData *pData;

    pData = new measStatData(UPA_Ref_Freq, freqRef, chan_none);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Ref_Freq]      = pData;

    pData = new measStatData(UPA_Vrms, v, chan_none);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Vrms]          = pData;

    pData = new measStatData(UPA_Irms, i, chan_none);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Irms]          = pData;

    pData = new measStatData(UPA_Real_P, p, chan_none);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Real_P]        = pData;

    pData = new measStatData(UPA_Apparent_P, v, i);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Apparent_P]    = pData;

    pData = new measStatData(UPA_Reactive_P, v, i);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Reactive_P]    = pData;

    pData = new measStatData(UPA_P_Factor, v, i);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_P_Factor]      = pData;

    pData = new measStatData(UPA_Phase_Angle, v, i);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Phase_Angle]   = pData;

    pData = new measStatData(UPA_Imp, v, i);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_Imp]           = pData;

    pData = new measStatData(UPA_V_CrestFactor, v, chan_none);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_V_CrestFactor] = pData;

    pData = new measStatData(UPA_I_CrestFactor, i, chan_none);
    Q_ASSERT( NULL != pData );
    m_PowerQStatData[UPA_I_CrestFactor] = pData;
}

CUpaPowerQData::~CUpaPowerQData()
{
    //! collect memory
    Q_ASSERT( NULL != m_pFreqRefData );
    delete m_pFreqRefData;

    //! release the reource
    QMapIterator<MeasType, measStatData*> iter(m_PowerQStatData);
    while (iter.hasNext())
    {
        iter.next();

        Q_ASSERT( iter.value() != NULL  );
        delete iter.value();
    }
}

void CUpaPowerQData::setSrcV(Chan v)
{
    m_SrcV    = v;
}

void CUpaPowerQData::setSrcI(Chan i)
{
    m_SrcI    = i;
}

void CUpaPowerQData::setFreqRef(Chan f)
{
    m_FreqRef = f;
    m_pFreqRefData->setMeasSrc(m_FreqRef);
}

void CUpaPowerQData::calcPowerQData(meas_algorithm::MEASURE *freqRef,
                                    PowerQVal *val, Chan src, bool zoomOn)
{
    meas_algorithm::MeasPowerQ powerQ;
    powerQ.PowerQ_MAX = 0;
    powerQ.PowerQ_RMS = 0.0;
    powerQ.PowerQ_AVG = 0.0;
    unsigned char *pSrcData = NULL;
    short s16Gnd = 0;
    float yInc = 0;
    DsoWfm mWfm;
    dsoVert *pVert = dsoVert::getCH( src );    

    if( NULL != pVert)
    {
        if( zoomOn)
        {
            pVert->getTrace(mWfm, chan_none, horizontal_view_zoom);
        }
        else
        {
            pVert->getTrace(mWfm, chan_none, horizontal_view_main);
        }

        //! fix bug 1577
        yInc = mWfm.realyInc();
//        qDebug()<<"CUpaPowerQData::calcPowerQData yInc = "<<yInc;

        s16Gnd   = mWfm.getyGnd();
        pSrcData = mWfm.getPoint();
        int len = mWfm.size();

        if( NULL != pSrcData)
        {
//            qDebug()<<"calcPowerQData src: "<<src;
            meas_algorithm::UpaPowerQAnalyser(pSrcData, len, s16Gnd, freqRef,
                                          &powerQ);
            val->max = powerQ.PowerQ_MAX * yInc;
            val->rms = powerQ.PowerQ_RMS * yInc;
            val->avg = powerQ.PowerQ_AVG * yInc;
        }
    }
}

void CUpaPowerQData::calcRefFreq()
{
     MeasValue val;
     val.mAx.bVisible = false;
     val.mAy.bVisible = false;
     val.mBx.bVisible = false;
     val.mBy.bVisible = false;
     val.mUnit = Unit_hz;

     if( m_pFreqRefData->getRawMeasData()->Period > 0)
     {
         val.mVal = 1/(m_pFreqRefData->getRawMeasData()->Period);
         val.mStatus = VALID;
     }
     else
     {
         val.mVal = 0;
         val.mStatus = INVALID;
     }

     m_PowerQStatData.value(UPA_Ref_Freq)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcVrms()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_V;

    val.mVal = m_PowerQ_V.rms;
    val.mStatus = VALID;

    m_PowerQStatData.value(UPA_Vrms)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcIrms()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_A;

    val.mVal = m_PowerQ_I.rms;
    val.mStatus = VALID;

    m_PowerQStatData.value(UPA_Irms)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcRealP()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_W;

    val.mVal = m_PowerQ_P.avg;
//    qDebug()<<"CUpaPowerQData::calcRealP() m_PowerQ_P.avg = "<<m_PowerQ_P.avg;

    val.mStatus = VALID;

    m_PowerQStatData.value(UPA_Real_P)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcApparentP()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_VA;

    float ApparentP = m_PowerQ_V.rms * m_PowerQ_I.rms;
    val.mVal = ApparentP;
    val.mStatus = VALID;

    m_PowerQStatData.value(UPA_Apparent_P)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcReactiveP()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_VAR;

    float ApparentP = m_PowerQ_V.rms * m_PowerQ_I.rms;
    float RealP = m_PowerQ_P.avg;
    float ReactiveP = 0;

    if( ApparentP*ApparentP > RealP*RealP)
    {
        ReactiveP = sqrt(ApparentP*ApparentP - RealP*RealP);
        val.mVal = ReactiveP;
        val.mStatus = VALID;
    }
    else
    {
        ReactiveP = 0;
        val.mVal = ReactiveP;
        val.mStatus = INVALID;
    }

    m_PowerQStatData.value(UPA_Reactive_P)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcPFactor()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_none;

    float ApparentP = m_PowerQ_V.rms * m_PowerQ_I.rms;
    float RealP = m_PowerQ_P.avg;
    float PFactor = RealP/ApparentP;

    if( ApparentP != 0)
    {
        PFactor = RealP/ApparentP;
        val.mVal = PFactor;
        val.mStatus = VALID;
    }
    else
    {
        PFactor = 0;
        val.mVal = PFactor;
        val.mStatus = INVALID;
    }

    m_PowerQStatData.value(UPA_P_Factor)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcPhaseAngle()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_degree;

    float ApparentP = m_PowerQ_V.rms * m_PowerQ_I.rms;
    float RealP = m_PowerQ_P.avg;
    float PFactor = 0;
    float phaseAngle = 0;

    if( ApparentP != 0)
    {
        PFactor = RealP/ApparentP;
        phaseAngle = acos(PFactor);
        if( !isinf(phaseAngle) && !isnan(phaseAngle))
        {
            val.mVal = phaseAngle / (2*M_PI) * 360;  //弧度转角度
            val.mStatus = VALID;
        }
        else
        {
            val.mVal = 0;
            val.mStatus = INVALID;
        }
    }
    else
    {
        val.mVal = 0;
        val.mStatus = INVALID;
    }

    m_PowerQStatData.value(UPA_Phase_Angle)->updateStatVal(val, val.mStatus);
}


void CUpaPowerQData::calcImp()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_oum;

    if( m_PowerQ_I.rms != 0)
    {
        val.mVal = m_PowerQ_V.rms/m_PowerQ_I.rms;
        val.mStatus = VALID;
    }
    else
    {
        val.mVal = 0;
        val.mStatus = INVALID;
    }

    m_PowerQStatData.value(UPA_Imp)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcVCrestFactor()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_none;

    if( m_PowerQ_V.rms != 0)
    {
        val.mVal = m_PowerQ_V.max/m_PowerQ_V.rms;
        val.mStatus = VALID;
    }
    else
    {
        val.mVal = 0;
        val.mStatus = INVALID;
    }

    m_PowerQStatData.value(UPA_V_CrestFactor)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcICrestFactor()
{
    MeasValue val;
    val.mAx.bVisible = false;
    val.mAy.bVisible = false;
    val.mBx.bVisible = false;
    val.mBy.bVisible = false;
    val.mUnit = Unit_none;

    if( m_PowerQ_I.rms != 0)
    {
        val.mVal = m_PowerQ_I.max/m_PowerQ_I.rms;
        val.mStatus = VALID;
    }
    else
    {
        val.mVal = 0;
        val.mStatus = INVALID;
    }

    m_PowerQStatData.value(UPA_I_CrestFactor)->updateStatVal(val, val.mStatus);
}

void CUpaPowerQData::calcStatData(ThresholdData *th,
                                  meas_algorithm::StateMethodMode stateMode,
                                  meas_algorithm::StateMethod topMethod,
                                  meas_algorithm::StateMethod baseMethod,
                                  bool zoomOn)
 {
     m_pFreqRefData->CalcRealMeasData_SOFT(th, stateMode, topMethod, baseMethod);
     meas_algorithm::MEASURE *FreqRef_Meas = m_pFreqRefData->getRawMeasData();

     //! calc UPA Power Quality base Meas
     calcPowerQData( FreqRef_Meas, &m_PowerQ_V, m_SrcV, zoomOn);
     calcPowerQData( FreqRef_Meas, &m_PowerQ_I, m_SrcI, zoomOn);
     calcPowerQData( FreqRef_Meas, &m_PowerQ_P, m_SrcP, zoomOn);

//     qDebug()<<"CUpaPowerQData::calcStatData() calc UPA Power Quality base Meas";

     //! calc UPA Power Quality ext. Meas
     calcRefFreq();
     calcVrms();
     calcIrms();
     calcRealP();
     calcApparentP();
     calcReactiveP();
     calcPFactor();
     calcPhaseAngle();
     calcImp();
     calcVCrestFactor();
     calcICrestFactor();
 }

 QMap<MeasType,measStatData*>* CUpaPowerQData::getStatData(void)
 {
     return &m_PowerQStatData;
 }

 void CUpaPowerQData::setStatCount(ulong c)
 {
     foreach(MeasType type, m_PowerQStatData.keys())
     {
         m_PowerQStatData.value(type)->setCntLimit(c);
     }
 }

 void CUpaPowerQData::resetStatVal(void)
 {
     foreach(MeasType type, m_PowerQStatData.keys())
     {
         m_PowerQStatData.value(type)->resetStatVal();
     }
 }

 MeasStatus CUpaPowerQData::getValStatus(float val)
 {
     if( !isinf(val) && !isnan(val))
     {
         return VALID;
     }
     else
     {
         return INVALID;
     }
 }
