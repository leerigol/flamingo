#include "powerQuality.h"

#include "../../service/servtrace/servtrace.h"
#include "../../service/servmeasure/servmeasure.h"
#include "servUPA.h"

#define dbg_out()

IMPLEMENT_CMD( servUPABase, servPowerQ )
start_of_entry()
on_set_int_int  ( MSG_UPA_TYPE,                    &servUPABase::cfg_AnalysisType ),
on_get_int      ( MSG_UPA_TYPE,                    &servUPABase::get_SelfAnalysisType ),
on_get_int      ( servPowerQ::msg_scpi_get_type, &servUPABase::get_AnalysisType ),

on_set_int_int  ( MSG_UPA_STAT_COUNT,              &servUPABase::set_StatCount ),
on_get_int      ( MSG_UPA_STAT_COUNT,              &servUPABase::get_StatCount ),
on_set_int_void ( MSG_UPA_STAT_RESET,              &servUPABase::set_StatReset ),

on_set_int_void ( MSG_UPA_AUTODESKEW,              &servUPABase::set_AutoDeskew ),
on_set_int_int  ( MSG_UPA_REFL_TYPE,               &servUPABase::set_RefLevelType ),
on_get_int      ( MSG_UPA_REFL_TYPE,               &servUPABase::get_RefLevelType ),

on_set_int_int ( MSG_UPA_REFL_PCT_HIGH,           &servUPABase::set_RefLPctHigh ),
on_get_int     ( MSG_UPA_REFL_PCT_HIGH,           &servUPABase::get_RefLPctHigh ),
on_set_int_int ( MSG_UPA_REFL_PCT_MID,            &servUPABase::set_RefLPctMid ),
on_get_int     ( MSG_UPA_REFL_PCT_MID,            &servUPABase::get_RefLPctMid ),
on_set_int_int ( MSG_UPA_REFL_PCT_LOW,            &servUPABase::set_RefLPctLow ),
on_get_int     ( MSG_UPA_REFL_PCT_LOW,            &servUPABase::get_RefLPctLow ),
on_set_int_ll  ( MSG_UPA_REFL_ABS_HIGH,           &servUPABase::set_RefLAbsHigh ),
on_get_ll      ( MSG_UPA_REFL_ABS_HIGH,           &servUPABase::get_RefLAbsHigh ),
on_set_int_ll  ( MSG_UPA_REFL_ABS_MID,            &servUPABase::set_RefLAbsMid ),
on_get_ll      ( MSG_UPA_REFL_ABS_MID,            &servUPABase::get_RefLAbsMid ),
on_set_int_ll  ( MSG_UPA_REFL_ABS_LOW,            &servUPABase::set_RefLAbsLow ),
on_get_ll      ( MSG_UPA_REFL_ABS_LOW,            &servUPABase::get_RefLAbsLow ),

on_set_int_void( MSG_UPA_REFL_DEFAULT,            &servUPABase::set_RefLDefault ),

on_set_int_int  ( MSG_UPA_POWER_VOLT,              &servPowerQ::set_Voltage ),
on_get_int      ( MSG_UPA_POWER_VOLT,              &servPowerQ::get_Voltage ),

on_set_int_int  ( MSG_UPA_POWER_CURR,              &servPowerQ::set_Current ),
on_get_int      ( MSG_UPA_POWER_CURR,              &servPowerQ::get_Current ),

on_set_int_bool ( MSG_UPA_POWER_FREF,              &servPowerQ::set_FreqRef ),
on_get_bool     ( MSG_UPA_POWER_FREF,              &servPowerQ::get_FreqRef ),
on_get_int      ( servPowerQ::MSG_FreqRef_SOURCE,  &servPowerQ::getFreqRefSource ),

on_set_int_int  ( MSG_UPA_POWER_CYC,               &servPowerQ::set_Cycles ),
on_get_int      ( MSG_UPA_POWER_CYC,               &servPowerQ::get_Cycles ),

on_set_int_void ( MSG_UPA_POWER_AUTOSET,           &servPowerQ::set_AutoSet ),

on_set_int_bool ( MSG_UPA_POWER_DISP,              &servPowerQ::set_Display ),
on_get_bool     ( MSG_UPA_POWER_DISP,              &servPowerQ::get_Display ),

on_set_int_void ( MSG_UPA_POWER_TIPS,              &servPowerQ::set_Tips ),

on_set_void_void( CMD_SERVICE_RST,                 &servPowerQ::rst ),
on_set_int_void ( CMD_SERVICE_STARTUP,             &servPowerQ::startup ),
on_set_int_void ( CMD_SERVICE_ACTIVE,              &servPowerQ::setActive ),
on_set_void_void( CMD_SERVICE_INIT,                &servPowerQ::init ),

on_set_int_ll  ( servPowerQ::msg_scpi_abs_high,           &servUPABase::set_ScpiRefLAbsHigh ),
on_set_int_ll  ( servPowerQ::msg_scpi_abs_mid,           &servUPABase::set_ScpiRefLAbsMid ),
on_set_int_ll  ( servPowerQ::msg_scpi_abs_low,           &servUPABase::set_ScpiRefLAbsLow ),

end_of_entry()

servPowerQ::servPowerQ(QString name) : servUPABase( name, UPA_PowerQ, E_SERVICE_ID_UPA_POWERQ )
{
    serviceExecutor::baseInit();
    m_Voltage = chan1;
    m_Current = chan2;
    m_FreqRef = false;
    m_Cycles  = 5;
    m_Display = false;
}

int servPowerQ::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;
    stream.write(QStringLiteral("m_Voltage"),       m_Voltage);
    stream.write(QStringLiteral("m_Current"),       m_Current);
    stream.write(QStringLiteral("m_FreqRef"),       m_FreqRef);
    stream.write(QStringLiteral("m_Cycles"),        m_Cycles);
    stream.write(QStringLiteral("m_Display"),       m_Display);

    servUPABase::serialOut(stream, ver);

    return ERR_NONE;
}

int servPowerQ::serialIn( CStream &stream, serialVersion ver )
{
    if( ver == mVersion)
    {
        stream.read(QStringLiteral("m_Voltage"),       m_Voltage);
        stream.read(QStringLiteral("m_Current"),       m_Current);
        stream.read(QStringLiteral("m_FreqRef"),       m_FreqRef);
        stream.read(QStringLiteral("m_Cycles"),        m_Cycles);
        stream.read(QStringLiteral("m_Display"),       m_Display);

        servUPABase::serialIn(stream, ver);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

void servPowerQ::rst()
{
    m_Voltage = chan1;
    m_Current = chan2;
    m_FreqRef = false;
    m_Cycles  = 5;
    m_Display = false;

    reset();
}


int servPowerQ::startup()
{
    return servUPABase::startup();
}

void servPowerQ::init()
{
    for( int i = (int)r1; i<=(int)r10; i++)  //! Hide Ref1-10
    {
        mUiAttr.setVisible( MSG_UPA_POWER_VOLT, i, false);
    }

    for( int i = (int)r1; i<=(int)r10; i++)  //! Hide Ref1-10
    {
        mUiAttr.setVisible( MSG_UPA_POWER_CURR, i, false);
    }

    mUiAttr.setVisible( MSG_UPA_POWER_CYC, false);
    mUiAttr.setVisible( MSG_UPA_POWER_AUTOSET, false);
    mUiAttr.setVisible( MSG_UPA_AUTODESKEW, false);
}

DsoErr servPowerQ::setActive( int active)
{
    set_AnalysisType( UPA_PowerQ );

    return serviceExecutor::setActive( active);
}

DsoErr servPowerQ::set_Voltage(int v)
{
    m_Voltage = v; 
    serviceExecutor::post(serv_name_upa, servUPA::cmd_set_powerq_volt, m_Voltage);

    return ERR_NONE;
}

int servPowerQ::get_Voltage()
{
    return m_Voltage;
}

DsoErr servPowerQ::set_Current(int c)
{
    m_Current = c;
    serviceExecutor::post(serv_name_upa, servUPA::cmd_set_powerq_curr, m_Current);

    return ERR_NONE;
}

int servPowerQ::get_Current()
{
    return m_Current;
}

DsoErr servPowerQ::set_FreqRef(bool b)
{
    m_FreqRef = b;

    if(m_FreqRef == true)
    {
        serviceExecutor::post(serv_name_upa, servUPA::cmd_set_powerq_fref, m_Current);
    }
    else
    {
        serviceExecutor::post(serv_name_upa, servUPA::cmd_set_powerq_fref, m_Voltage);
    }

    return ERR_NONE;
}

bool servPowerQ::get_FreqRef()
{
    return m_FreqRef;
}

int servPowerQ::getFreqRefSource(void)
{
    if(m_FreqRef == false)
    {
        return m_Voltage;
    }
    else
    {
        return m_Current;
    }
}

DsoErr servPowerQ::set_Cycles(int v)
{
    m_Cycles = v;
    return ERR_NONE;
}

int servPowerQ::get_Cycles()
{
    return m_Cycles;
}

DsoErr servPowerQ::set_AutoSet()
{

    return ERR_NONE;
}

DsoErr servPowerQ::set_Display(bool b)
{
    m_Display = b;

    LOG_DBG() << "m_Display:  "<<b;
    if(m_Display)
    {
        async( servUPA::cmd_run_upa, (int)1, serv_name_upa );
    }
    else
    {
        async( servUPA::cmd_stop_upa, (int)1, serv_name_upa);
    }

    return ERR_NONE;
}

bool servPowerQ::get_Display()
{
    return m_Display;
}

DsoErr servPowerQ::set_Tips()
{

    return ERR_NONE;
}
