#ifndef SERVUPABASE_H
#define SERVUPABASE_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "../../include/dsotype.h"


typedef enum
{
    UPA_PowerQ = 0,
    UPA_Ripple,


}UPA_Type;

class servUPABase : public serviceExecutor,
                    public ISerial
{
    Q_OBJECT

public:
    servUPABase( QString name, UPA_Type type, ServiceId eId = E_SERVICE_ID_UPA );

    virtual int serialOut(CStream &stream, serialVersion &ver);
    virtual int serialIn( CStream &stream, serialVersion ver );

    DsoErr setActive(int active = 1);
    void   reset();

public:
    DsoErr  set_AnalysisType(int v);
    DsoErr  cfg_AnalysisType(int v);
    int     get_AnalysisType();
    int     get_SelfAnalysisType();

    static  QString currAnalysisType();

    DsoErr  set_StatCount(int v);
    int     get_StatCount();
    DsoErr  set_StatReset();

    DsoErr  set_AutoDeskew();

    DsoErr  set_RefLevelType(int v);
    int     get_RefLevelType();

    DsoErr  set_RefLPctHigh(int v);
    int     get_RefLPctHigh();
    DsoErr  set_RefLPctMid(int v);
    int     get_RefLPctMid();
    DsoErr  set_RefLPctLow(int v);
    int     get_RefLPctLow();

    DsoErr  set_RefLAbsHigh(qint64 v);
    qint64  get_RefLAbsHigh();
    DsoErr  set_RefLAbsMid(qint64 v);
    qint64  get_RefLAbsMid();
    DsoErr  set_RefLAbsLow(qint64 v);
    qint64  get_RefLAbsLow();

    DsoErr  set_RefLDefault();

    //scpi
    DsoErr  set_ScpiRefLAbsHigh(qlonglong v);
    DsoErr  set_ScpiRefLAbsMid(qlonglong v);
    DsoErr  set_ScpiRefLAbsLow(qlonglong v);

private:
    static int m_AnalysisType;
    static int m_StatCount;
    static int m_RefLevelType;

    static int m_RefLPctHigh;
    static int m_RefLPctMid;
    static int m_RefLPctLow;

    static qint64 m_RefLAbsHigh;
    static qint64 m_RefLAbsMid;
    static qint64 m_RefLAbsLow;

protected:
    int m_MyAnalysisType;
};



#endif // SERVUPABASE_H
