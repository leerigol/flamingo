#ifndef RIPPLE_H
#define RIPPLE_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "servUPABase.h"
#include "../../include/dsotype.h"

class servRipple : public servUPABase
{
    Q_OBJECT
    DECLARE_CMD()

public:
    servRipple( QString name = serv_name_upa_powerq );
    enum
    {
        cmd_base = 0,

        MSG_UPDATE_MEAS,
    };

public:
    int  serialOut(CStream &stream, serialVersion &ver);
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();
    void init();
    DsoErr setActive( int active = 1);

    //virtual DsoErr start();
    //virtual void registerSpy();
    DsoErr set_Source(int v);
    int    get_Source();
    DsoErr set_AutoSet();
    DsoErr set_OffsetReset();
    DsoErr set_Display(bool b);
    bool   get_Display();
    DsoErr set_Tips();

private:
    int  m_Source;
    bool m_Display;

};



#endif // RIPPLE_H
