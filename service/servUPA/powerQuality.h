#ifndef POWERQUALITY_H
#define POWERQUALITY_H

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "servUPABase.h"
#include "../../include/dsotype.h"
#include "../../com/scpiparse/cconverter.h"

#define dbg_out()

class servPowerQ : public servUPABase
{
    Q_OBJECT
    DECLARE_CMD()

public:
    servPowerQ( QString name = serv_name_upa_powerq );
    enum
    {
        cmd_base = 0,

        MSG_FreqRef_SOURCE,
        msg_scpi_get_type,
        msg_scpi_abs_high,
        msg_scpi_abs_low,
        msg_scpi_abs_mid,
    };

public:
    int  serialOut(CStream &stream, serialVersion &ver);
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();
    void init();
    DsoErr setActive( int active = 1);

    DsoErr set_Voltage(int v);
    int    get_Voltage();

    DsoErr set_Current(int c);
    int    get_Current();

    DsoErr set_FreqRef(bool b);
    bool   get_FreqRef();

    DsoErr set_Cycles(int v);
    int    get_Cycles();

    DsoErr set_AutoSet();

    DsoErr set_Display(bool b);
    bool   get_Display();

    DsoErr set_Tips();

    int    getFreqRefSource(void);

private:
    int  m_Voltage;
    int  m_Current;
    bool m_FreqRef;
    int  m_Cycles;
    bool m_Display;

};



#endif // POWERQUALITY_H
