#ifndef SERVUPA_H
#define SERVUPA_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "../../include/dsotype.h"
#include "servUPABase.h"
#include "powerQuality.h"
#include "ripple.h"
#include "../servmeasure/meas_interface.h"
#include "../servmeasure/meas_stat.h"
#include "uparippledata.h"
#include "upapowerqdata.h"

class upaThread;
class servUPA : public serviceExecutor,
                public ISerial
{
    Q_OBJECT

protected:
    DECLARE_CMD()

public:
    servUPA( QString name, ServiceId eId = E_SERVICE_ID_UPA );
    static int createServUPA(QList<service*> *pService);

    enum
    {
        cmd_base = 0,

        cmd_set_AnalysisType,
        cmd_get_AnalysisType,
        cmd_trace_update,
        cmd_upa_update,

        cmd_get_ripple_val,
        cmd_set_ripple_src,
        cmd_set_ripple_stat_cnt,
        cmd_set_ripple_stat_reset,

        cmd_get_powerq_val,
        cmd_set_powerq_volt,
        cmd_set_powerq_curr,
        cmd_set_powerq_fref,
        cmd_get_powerq_fref,

        cmd_set_powerq_stat_cnt,
        cmd_set_powerq_stat_reset,

        cmd_run_upa,
        cmd_stop_upa,

        cmd_close_upa_app,

        //! spy on
        cmd_hor_zoom_on,        
    };

public:
    int    serialOut(CStream &stream, serialVersion &ver);
    int    serialIn( CStream &stream, serialVersion ver );
    void   rst();
    int    startup();
    DsoErr start();
    int    onActive(int id);
    void   registerSpy();

    //! spy on
    void  onHorZoomOn();    

    DsoErr  set_AnalysisType(int v);
    int     get_AnalysisType();
    QString getCurrServ();

    //PowerQ
    void calc_PowerQ();
    pointer getPowerQVal();
    void createPowerQData();
    void deletePowerQData();
    void setPowerQVoltSrc(int v);
    void setPowerQCurrSrc(int c);
    void setPowerQFreqRef(int freqRef);
    int  getPowerQFreqRef();
    void setPowerQStatCnt(int cnt);
    void setPowerQStatReset();

    //Ripple
    void calc_Ripple();
    pointer getRippleVal();
    void createRippleData();
    void deleteRippleData();
    void setRippleSrc(int src);
    void setRippleStatCnt(int cnt);
    void setRippleStatReset();

    //Common
    DsoErr setUpaSrcReady(pointer);
    DsoErr onUpaUpdate(int);

    void runUpaThread();
    void stopUpaThread();
    bool beRunUpaThread();

    void createUpaData(int type );
    void deleteUpaData(int type );

    int closeUpaApp();
protected:
    QSemaphore m_UpaSrcSema;
    upaThread *m_pUpaThread;

private:
    int m_AnalysisType;

    bool runUPA;
    CUpaRippleData *m_RippleData;
    measStatData *m_RippleStatData;
    CUpaPowerQData *m_PowerQData;
    bool m_ZoomOn;

friend class upaThread;
};

class upaThread : public QThread
{
    Q_OBJECT
public:
    upaThread( servUPA *pUpa);

public:
    void run();

private:
    servUPA *m_pServUPA;
};



#endif // SERVUPA_H
