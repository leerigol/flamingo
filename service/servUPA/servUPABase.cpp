#include "servUPABase.h"
#include "servUPA.h"
#include "../../service/servtrace/servtrace.h"

int servUPABase::m_AnalysisType  = UPA_PowerQ;
int servUPABase::m_StatCount     = 1000;
int servUPABase::m_RefLevelType  = 0;
int    servUPABase::m_RefLPctHigh = 90;
int    servUPABase::m_RefLPctMid  = 50;
int    servUPABase::m_RefLPctLow  = 10;
qint64 servUPABase::m_RefLAbsHigh = 9200;
qint64 servUPABase::m_RefLAbsMid  = 5200;
qint64 servUPABase::m_RefLAbsLow  =1200;

servUPABase::servUPABase(QString name, UPA_Type type, ServiceId eId ) : serviceExecutor( name, eId )
{
    m_MyAnalysisType = type;
}

int servUPABase::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;
    stream.write(QStringLiteral("m_AnalysisType"),    m_AnalysisType);
    stream.write(QStringLiteral("m_StatCount"),       m_StatCount);

    stream.write(QStringLiteral("m_RefLevelType"),    m_RefLevelType);
    stream.write(QStringLiteral("m_RefLPctHigh"),     m_RefLPctHigh);
    stream.write(QStringLiteral("m_RefLPctMid"),      m_RefLPctMid);
    stream.write(QStringLiteral("m_RefLPctLow"),      m_RefLPctLow);
    stream.write(QStringLiteral("m_RefLAbsHigh"),     m_RefLAbsHigh);
    stream.write(QStringLiteral("m_RefLAbsMid"),      m_RefLAbsMid);
    stream.write(QStringLiteral("m_RefLAbsLow"),      m_RefLAbsLow);

    return ERR_NONE;
}

int servUPABase::serialIn(CStream &stream, serialVersion ver )
{
    if( ver == mVersion)
    {
        stream.read(QStringLiteral("m_AnalysisType"), m_AnalysisType);
        stream.read(QStringLiteral("m_StatCount"),    m_StatCount);

        stream.read(QStringLiteral("m_RefLevelType"), m_RefLevelType);
        stream.read(QStringLiteral("m_RefLPctHigh"),  m_RefLPctHigh);
        stream.read(QStringLiteral("m_RefLPctMid"),   m_RefLPctMid);
        stream.read(QStringLiteral("m_RefLPctLow"),   m_RefLPctLow);
        stream.read(QStringLiteral("m_RefLAbsHigh"),  m_RefLAbsHigh);
        stream.read(QStringLiteral("m_RefLAbsMid"),   m_RefLAbsMid);
        stream.read(QStringLiteral("m_RefLAbsLow"),   m_RefLAbsLow);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

DsoErr servUPABase::setActive(int /*active*/)
{
    qDebug() << "test upa";
    //return serviceExecutor::setActive( active);
    return ERR_NONE;
}

void servUPABase::reset()
{
    m_AnalysisType = UPA_PowerQ;
}

DsoErr servUPABase::cfg_AnalysisType(int v)
{
    QString servName = "";

    m_AnalysisType = v;
    switch (v)
    {
    case UPA_PowerQ:
        servName = serv_name_upa_powerq;
        break;
    case UPA_Ripple:
        servName = serv_name_upa_ripple;
        break;
    default:
        servName = serv_name_upa_powerq;
        break;
    }

    async( servUPA::cmd_set_AnalysisType, m_AnalysisType, serv_name_upa );
    async( servUPA::cmd_stop_upa, (int)1, serv_name_upa );
    async( servUPA::cmd_close_upa_app, (int)1, serv_name_upa );
    serviceExecutor::post(servName, CMD_SERVICE_ACTIVE, (int)MSG_UPA_TYPE );

    //send(servName, CMD_SERVICE_ACTIVE, 1);

    return ERR_NONE;
}

DsoErr servUPABase::set_AnalysisType(int v)
{
    m_AnalysisType = v;
    return ERR_NONE;
}

int servUPABase::get_AnalysisType()
{
    return m_AnalysisType;
}

int servUPABase::get_SelfAnalysisType()
{
    return  m_MyAnalysisType;
}

QString servUPABase::currAnalysisType()
{
    QString servName = "";

    switch (m_AnalysisType)
    {
    case UPA_PowerQ:
        servName = serv_name_upa_powerq;
        break;
    case UPA_Ripple:
        servName = serv_name_upa_ripple;
        break;
    default:
        servName = serv_name_upa_powerq;
        break;
    }
    return servName;
}

DsoErr servUPABase::set_StatCount(int v)
{
    m_StatCount = v;

    switch (m_AnalysisType)
    {
    case UPA_PowerQ:
        post( E_SERVICE_ID_UPA, servUPA::cmd_set_powerq_stat_cnt, m_StatCount);
        break;
    case UPA_Ripple:
        post( E_SERVICE_ID_UPA, servUPA::cmd_set_ripple_stat_cnt, m_StatCount);
        break;
    default:

        break;
    }
    return ERR_NONE;
}

int servUPABase::get_StatCount()
{
    setuiRange( 2ll, 5000ll, 1000ll);
    setuiStep( 1 );
    setuiFmt(fmt_int);
    setuiPostStr( "" );
    setuiBase(1, E_0);
    return m_StatCount;
}

DsoErr servUPABase::set_StatReset()
{
    switch (m_AnalysisType)
    {
    case UPA_PowerQ:
        post( E_SERVICE_ID_UPA, servUPA::cmd_set_powerq_stat_reset, 1);
        break;
    case UPA_Ripple:
        post( E_SERVICE_ID_UPA, servUPA::cmd_set_ripple_stat_reset, 1);
        break;
    default:

        break;
    }

    return ERR_NONE;
}

DsoErr servUPABase::set_AutoDeskew()
{

    return ERR_NONE;
}

DsoErr servUPABase::set_RefLevelType(int v)
{
    m_RefLevelType = v;
    return ERR_NONE;
}

int servUPABase::get_RefLevelType()
{
    return m_RefLevelType;
}

DsoErr servUPABase::set_RefLPctHigh(int v)
{
    m_RefLPctHigh = v;
    return ERR_NONE;
}

int servUPABase::get_RefLPctHigh()
{
    int minVal =  m_RefLPctMid+1;
    mUiAttr.setRange( MSG_UPA_REFL_PCT_HIGH, minVal, 100, 0);  //! m_RefLPctMid - 100%
    mUiAttr.setStep( MSG_UPA_REFL_PCT_HIGH, 1 );
    mUiAttr.setPostStr( MSG_UPA_REFL_PCT_HIGH, "%" );
//    mUiAttr.setBase( MSG_UPA_REFL_PCT_HIGH, 1, E_0);

    return m_RefLPctHigh;
}

DsoErr servUPABase::set_RefLPctMid(int v)
{
    m_RefLPctMid = v;
    return ERR_NONE;
}

int servUPABase::get_RefLPctMid()
{
    int minVal = m_RefLPctLow+1;
    int maxVal = m_RefLPctHigh-1;

    mUiAttr.setRange( MSG_UPA_REFL_PCT_MID, minVal, maxVal, 0);  //! 5% - 95%
    mUiAttr.setStep( MSG_UPA_REFL_PCT_MID, 1 );
    mUiAttr.setPostStr( MSG_UPA_REFL_PCT_MID, "%" );
//    mUiAttr.setBase( MSG_UPA_REFL_PCT_MID, 1, E_0);
    return m_RefLPctMid;
}

DsoErr servUPABase::set_RefLPctLow(int v)
{
    m_RefLPctLow = v;
    return ERR_NONE;
}

int servUPABase::get_RefLPctLow()
{
    int maxVal =  m_RefLPctMid-1;
    mUiAttr.setRange( MSG_UPA_REFL_PCT_LOW, 0, maxVal, 0);  //! 0% - m_MidPer
    mUiAttr.setStep( MSG_UPA_REFL_PCT_LOW, 1 );
    mUiAttr.setPostStr( MSG_UPA_REFL_PCT_LOW, "%" );
    return m_RefLPctLow;
}

DsoErr servUPABase::set_RefLAbsHigh(qint64 v)
{
    //qDebug()<<__FILE__<<__LINE__<<m_RefLPctHigh<<m_RefLPctMid<<m_RefLPctLow;
    m_RefLAbsHigh = v;
    qDebug()<<"servUPABase::set_RefLAbsHigh m_RefLAbsHigh"<<m_RefLAbsHigh;
    return ERR_NONE;
}

qint64 servUPABase::get_RefLAbsHigh()
{
    int fRef;
    serviceExecutor::query(serv_name_upa, servUPA::cmd_get_powerq_fref, fRef);

    dsoVert *pVert = dsoVert::getCH( (Chan)fRef);
    float base;
    pVert->getProbe().toReal(base);
    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;
    mUiAttr.setStep( MSG_UPA_REFL_ABS_HIGH, step );
    qlonglong minVal = m_RefLAbsMid + step;
    qlonglong maxVal = 100000000ll*1000000ll;   //! Max: 100M
//    mUiAttr.setRange(MSG_UPA_REFL_ABS_HIGH,  minVal, maxVal, 0ll);
    mUiAttr.setMaxVal(MSG_UPA_REFL_ABS_HIGH, maxVal);
    mUiAttr.setMinVal(MSG_UPA_REFL_ABS_HIGH, minVal);
    mUiAttr.setBase( MSG_UPA_REFL_ABS_HIGH, 1,E_N6);

    Unit unit = pVert->getUnit();
    mUiAttr.setUnit( MSG_UPA_REFL_ABS_HIGH, unit);
    if( unit == Unit_W)       mUiAttr.setPostStr( MSG_UPA_REFL_ABS_HIGH, "W" );
    else if( unit == Unit_A)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_HIGH, "A" );
    else if( unit == Unit_V)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_HIGH, "V" );
    else if( unit == Unit_U)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_HIGH, "U" );
    else                      mUiAttr.setPostStr( MSG_UPA_REFL_ABS_HIGH, "V" );

    return m_RefLAbsHigh;
}

DsoErr servUPABase::set_RefLAbsMid(qint64 v)
{
    m_RefLAbsMid = v;
    return ERR_NONE;
}

qint64 servUPABase::get_RefLAbsMid()
{
    int fRef;
    serviceExecutor::query(serv_name_upa, servUPA::cmd_get_powerq_fref, fRef);

    dsoVert *pVert = dsoVert::getCH( (Chan)fRef);
    float base;
    pVert->getProbe().toReal(base);
    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;
    mUiAttr.setStep( MSG_UPA_REFL_ABS_MID, step );

    qlonglong minVal = m_RefLAbsLow + step;
    qlonglong maxVal = m_RefLAbsHigh - step;
//     mUiAttr.setRange(MSG_UPA_REFL_ABS_MID,  minVal, maxVal, 0ll);
     mUiAttr.setMaxVal(MSG_UPA_REFL_ABS_MID, maxVal);
     mUiAttr.setMinVal(MSG_UPA_REFL_ABS_MID, minVal);

    Unit unit = pVert->getUnit();
    mUiAttr.setUnit( MSG_UPA_REFL_ABS_MID, unit);
    if( unit == Unit_W)       mUiAttr.setPostStr( MSG_UPA_REFL_ABS_MID, "W" );
    else if( unit == Unit_A)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_MID, "A" );
    else if( unit == Unit_V)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_MID, "V" );
    else if( unit == Unit_U)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_MID, "U" );
    else                      mUiAttr.setPostStr( MSG_UPA_REFL_ABS_MID, "V" );

    mUiAttr.setBase( MSG_UPA_REFL_ABS_MID, 1,E_N6);
    return m_RefLAbsMid;
}

DsoErr servUPABase::set_RefLAbsLow(qint64 v)
{
    m_RefLAbsLow = v;
    return ERR_NONE;
}

qint64 servUPABase::get_RefLAbsLow()
{
    int fRef;
    serviceExecutor::query(serv_name_upa, servUPA::cmd_get_powerq_fref, fRef);

    dsoVert *pVert = dsoVert::getCH( (Chan)fRef);
    float base;
    pVert->getProbe().toReal(base);
    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;
    mUiAttr.setStep( MSG_UPA_REFL_ABS_LOW, step );

    qlonglong minVal = -100000000ll*1000000ll;  //! Min: -100M
    qlonglong maxVal = m_RefLAbsMid - step;
//    mUiAttr.setRange(MSG_UPA_REFL_ABS_LOW,  minVal, maxVal, 0ll);
    mUiAttr.setMaxVal(MSG_UPA_REFL_ABS_LOW, maxVal);
    mUiAttr.setMinVal(MSG_UPA_REFL_ABS_LOW, minVal);

    Unit unit = pVert->getUnit();
    mUiAttr.setUnit( MSG_UPA_REFL_ABS_LOW, unit);
    if( unit == Unit_W)       mUiAttr.setPostStr( MSG_UPA_REFL_ABS_LOW, "W" );
    else if( unit == Unit_A)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_LOW, "A" );
    else if( unit == Unit_V)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_LOW, "V" );
    else if( unit == Unit_U)  mUiAttr.setPostStr( MSG_UPA_REFL_ABS_LOW, "U" );
    else                      mUiAttr.setPostStr( MSG_UPA_REFL_ABS_LOW, "V" );

    mUiAttr.setBase( MSG_UPA_REFL_ABS_LOW, 1,E_N6);
    return m_RefLAbsLow;
}

DsoErr servUPABase::set_RefLDefault()
{
    if( (meas_algorithm::ThresholdType)m_RefLevelType == meas_algorithm::TH_TYPE_PER)
    {
        id_async( getId(), MSG_UPA_REFL_PCT_HIGH, 90ll);
        id_async( getId(), MSG_UPA_REFL_PCT_MID, 50ll);
        id_async( getId(), MSG_UPA_REFL_PCT_LOW, 10ll);
    }
    else if( (meas_algorithm::ThresholdType)m_RefLevelType == meas_algorithm::TH_TYPE_ABS)
    {
        int high_SrcPos = trace_height/vert_div * 1;  //! Default High(SrcPos): 1div(From Top)
        int mid_SrcPos  = trace_height/vert_div * 4;  //! Default Mid(SrcPos):  4div(From Top)
        int low_SrcPos  = trace_height/vert_div * 7;  //! Default Low(SrcPos):  7div(From Top)

        float high_SOFT = (240 - high_SrcPos)*200*1.0f /trace_height + adc_center;
        float mid_SOFT  = (240 - mid_SrcPos)*200*1.0f /trace_height + adc_center;
        float low_SOFT  = (240 - low_SrcPos)*200*1.0f /trace_height + adc_center;

        int fRef;
        serviceExecutor::query(serv_name_upa, servUPA::cmd_get_powerq_fref, fRef);
        dsoVert *pVert = dsoVert::getCH( (Chan)fRef);
        double ratio;
        pVert->getProbe().toReal( ratio, 1.0);
        double realyInc = pVert->getyInc() * ratio;
        double realyGnd = pVert->getyGnd();
        float base = 1e6f;   //! base: 1uV

        qint64 high_Menu = qRound64( (high_SOFT - realyGnd) * realyInc * base);
        qint64 mid_Menu  = qRound64( (mid_SOFT - realyGnd) * realyInc * base);
        qint64 low_Menu  = qRound64( (low_SOFT - realyGnd) * realyInc * base);

        qDebug()<<"servUPABase::set_RefLDefault()  high_Menu"<<high_Menu;
        qDebug()<<"servUPABase::set_RefLDefault()  mid_Menu"<<mid_Menu;
        qDebug()<<"servUPABase::set_RefLDefault()  low_Menu"<<low_Menu;
        id_async( getId(), MSG_UPA_REFL_ABS_HIGH, high_Menu);
        id_async( getId(), MSG_UPA_REFL_ABS_MID,  mid_Menu);
        id_async( getId(), MSG_UPA_REFL_ABS_LOW,  low_Menu);
    }
    else
    {
        id_async( getId(), MSG_UPA_REFL_PCT_HIGH, 90ll);
        id_async( getId(), MSG_UPA_REFL_PCT_MID, 50ll);
        id_async( getId(), MSG_UPA_REFL_PCT_LOW, 10ll);
    }

    return ERR_NONE;
}

DsoErr servUPABase::set_ScpiRefLAbsHigh(qlonglong v)
{
    int fRef;
    serviceExecutor::query(serv_name_upa, servUPA::cmd_get_powerq_fref, fRef);

    dsoVert *pVert = dsoVert::getCH( (Chan)fRef);
    float base;
    pVert->getProbe().toReal(base);
    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;

    async(MSG_UPA_REFL_ABS_HIGH, v/step);
    return ERR_NONE;
}

DsoErr servUPABase::set_ScpiRefLAbsMid(qlonglong v)
{
    int fRef;
    serviceExecutor::query(serv_name_upa, servUPA::cmd_get_powerq_fref, fRef);

    dsoVert *pVert = dsoVert::getCH( (Chan)fRef);
    float base;
    pVert->getProbe().toReal(base);
    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;

    async(MSG_UPA_REFL_ABS_MID, v/step);
    return ERR_NONE;
}

DsoErr servUPABase::set_ScpiRefLAbsLow(qlonglong v)
{
    int fRef;
    serviceExecutor::query(serv_name_upa, servUPA::cmd_get_powerq_fref, fRef);

    dsoVert *pVert = dsoVert::getCH( (Chan)fRef);
    float base;
    pVert->getProbe().toReal(base);
    qlonglong step = pVert->getYRefScale()*base / adc_vdiv_dots;

    async(MSG_UPA_REFL_ABS_LOW, v/step);
    return ERR_NONE;
}
