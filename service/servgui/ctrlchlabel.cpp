#include "ctrlchlabel.h"

CtrlCHLabel::CtrlCHLabel()
{
    mColor = Qt::white;

    mTypeId = Ctrl::Ctrl_Label;
}

void CtrlCHLabel::setLabel( const QString &label )
{
    mLabel = label;
}
QString &CtrlCHLabel::getLabel()
{
    return mLabel;
}
void CtrlCHLabel::setPos( int x, int y )
{
    mPt.setX( x );
    mPt.setY( y );
}
QPoint CtrlCHLabel::getPos()
{
    return mPt;
}

