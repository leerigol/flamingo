#include "ctrlparagraph.h"


Paragraph::Paragraph()
{}
Paragraph::Paragraph( const Paragraph &graph )
{
    mColor = graph.mColor;
    mContent = graph.mContent;
}
Paragraph& Paragraph::operator=( const Paragraph &graph )
{
    mColor = graph.mColor;
    mContent = graph.mContent;

    return *this;
}

void Paragraph::set( const QColor &color, const QString &str )
{
    mColor = color;
    mContent = str;
}

CtrlParagraph::CtrlParagraph()
{
    mX = -1;
    mY = -1;

    nHideTimeout=-1;
    m_pImage = NULL;
}

CtrlParagraph::~CtrlParagraph()
{
    if ( NULL != m_pImage )
    { delete m_pImage; }
}

void CtrlParagraph::clear()
{
    mParagraphs.clear();
}

void CtrlParagraph::append(const QColor &color, const QString &content)
{
    Paragraph para;

    para.set( color, content );

    mParagraphs.append( para );
}

void CtrlParagraph::append( QImage *pImage )
{
    Q_ASSERT( NULL != pImage );

    //! delete pre
    if ( NULL != m_pImage )
    { delete m_pImage; }

    //! alloca
    m_pImage = new QImage( *pImage );
    if ( NULL == m_pImage )
    { return; }
}

void CtrlParagraph::setXY( int x, int y )
{
    mX = x;
    mY = y;
}

void CtrlParagraph::setTimeout(int tmo )
{
    nHideTimeout = tmo;
}

CtrlParagraph & CtrlParagraph::operator=( const CtrlParagraph &para )
{
    mParagraphs = para.mParagraphs;
    mX = para.mX;
    mY = para.mY;

    nHideTimeout = para.nHideTimeout;
    //! image copy
    if ( para.m_pImage != NULL )
    {
        if ( NULL != m_pImage )
        { delete m_pImage; }

        m_pImage = new QImage( *para.m_pImage );
    }

    return *this;
}
