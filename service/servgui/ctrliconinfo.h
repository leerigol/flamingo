#ifndef CTRLKEYINFO_H
#define CTRLKEYINFO_H

#include "ctrl.h"

class CtrlIconInfo : public Ctrl
{
public:
    CtrlIconInfo();

    void setInfo( int key, int info );

public:
    QList<int> mIconIds;
    QList<int> mInfoIds;
};

#endif // CTRLKEYINFO_H
