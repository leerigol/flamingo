#include "ctrlline.h"

CtrlLine::CtrlLine()
{
}

void CtrlLine::setRect( const QRect &rect )
{
    mRect = rect;
}
void CtrlLine::setRect( int x, int y, int width, int height )
{
    mRect.setRect( x, y, width, height );
}
QRect & CtrlLine::getRect()
{
    return mRect;
}

CtrlHLine::CtrlHLine()
{
    mTypeId = Ctrl::Ctrl_HLine;
}

void CtrlHLine::setLine( int x, int y, int width, int height )
{
    CtrlLine::setRect( x, y, width, height );
}

CtrlVLine::CtrlVLine()
{
    mTypeId = Ctrl::Ctrl_VLine;
}
void CtrlVLine::setLine( int x, int y, int height, int width )
{
    CtrlLine::setRect( x, y, width, height );
}
