#include "cctrlmsgbox.h"

CCtrlMsgBox::CCtrlMsgBox()
{
}

CCtrlMsgBox::CCtrlMsgBox( const CCtrlMsgBox &msgbox )
{
    *this = msgbox;
}

CCtrlMsgBox& CCtrlMsgBox::operator=( const CCtrlMsgBox &msgbox )
{
    mOkId = msgbox.mOkId;
    mServName = msgbox.mServName;
    mInfo = msgbox.mInfo;

    mButtons = msgbox.mButtons;
    mIcons = msgbox.mIcons;

    return *this;
}

void CCtrlMsgBox::setMsgBox( const QString &servName,
                int okId,
                const QString &info,
                menu_res::RMessageBox::MsgBox_Button btn,
                menu_res::RMessageBox::MsgBox_Icon icons )
{
    mServName = servName;
    mOkId = okId;
    mInfo = info;

    mButtons = btn;
    mIcons = icons;
}

