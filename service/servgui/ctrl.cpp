#include "ctrl.h"

Ctrl::Ctrl()
{
    mColor = Qt::white;
    mbVisible = false;

    mTypeId = Ctrl_none;
}

void Ctrl::setColor( QColor color )
{
    mColor = color;
}
QColor &Ctrl::getColor()
{
    return mColor;
}

void Ctrl::setName( const QString &name )
{
    mName = name;

    mObjectName = toString( mTypeId ) + mName;
}
QString &Ctrl::getName()
{
    return mName;
}

QString &Ctrl::objectName()
{
    return mObjectName;
}

void Ctrl::setVisible( bool bVisible )
{
    mbVisible = bVisible;
}
bool Ctrl::getVisible()
{
    return mbVisible;
}

Ctrl::CtrlType Ctrl::getType()
{
    return mTypeId;
}

bool Ctrl::operator==( Ctrl &ctrl )
{
    return ( ctrl.mTypeId == mTypeId
          && ctrl.mName == mName );
}

static struct CtrlType_Str
{
    Ctrl::CtrlType type;
    const char *str;
}_ctrltype_string[]=
{
{Ctrl::Ctrl_HLine,"hline"},
{Ctrl::Ctrl_VLine,"vline"},
{Ctrl::Ctrl_Label,"label"},
};
QString Ctrl::toString( CtrlType type )
{
    int i;

    for ( i = 0; i < array_count(_ctrltype_string); i++ )
    {
        if ( _ctrltype_string[i].type == type )
        {
            return QString( _ctrltype_string[i].str );
        }
    }

    Q_ASSERT( false );
    return "";
}
