#ifndef CTRLLINE_H
#define CTRLLINE_H


#include "ctrl.h"

class CtrlLine : public Ctrl
{
protected:
    CtrlLine();

public:
    void setRect( const QRect &rect );
    void setRect( int x, int y, int width, int height );
    QRect & getRect();

private:
    QRect mRect;
};

class CtrlHLine : public CtrlLine
{
public:
    CtrlHLine();
public:
    void setLine( int x, int y, int width, int height = 1 );
};

class CtrlVLine : public CtrlLine
{
public:
    CtrlVLine();
public:
    void setLine( int x, int y, int height, int width = 1 );
};


#endif // CTRLLINE_H
