#ifndef SERVGUI_H
#define SERVGUI_H

#include <QtGlobal>
#include <QColor>

#include "../service.h"

#include "cctrlprogress.h"
#include "ctrlparagraph.h"
#include "ctrlnotifyline.h"
#include "ctrlchlabel.h"

#include "ctrlline.h"
#include "cctrlmsgbox.h"
#include "ctrliconinfo.h"
typedef QList< Ctrl *> CtrlList;

class servGui : public serviceExecutor
{
    Q_OBJECT
protected:
    DECLARE_CMD()

public:
    enum logType
    {
        e_log_info = 0,
        e_log_error,
        e_log_warning,
        e_log_critical,
    };

public:
    enum cmdGui
    {
        cmd_none,

        //! info
        cmd_info_cfg,               //! CtrlParagraph

        //! popup
        cmd_popup,                  //! popup_id

        //! msgbox
        cmd_msg_box,                //! CCtrlMsgBox

        //! progress
        cmd_progress_cfg,           //! full config

        cmd_progress_val,           //! int
        cmd_progress_step,          //! int
        cmd_progress_visible,       //! bool
        cmd_progress_info,          //! qstring

        cmd_progress_cancel,        //! cancel from wnd

        //! spy notify
        cmd_notify_install,
        cmd_notify_uninstall,
        cmd_notify_spy,

        qcmd_ctrl_list,

        //! label
        cmd_ch_label_set,

        //! line
        cmd_v_line_set,
        cmd_h_line_set,

        //! animate
        cmd_logo_animate_run,        //! bool
        cmd_logo_animate_rst,

        //! log
        cmd_log_str,
        qcmd_log_str,
        qcmd_log_str_size,
    };

    enum actionFilterCause
    {
        action_filter_msgbox = 0,
        action_filter_progress,
    };

public:
    static void registerNotify( const QString &name,
                               int msg,
                               QColor color,
                               const QString &caption,
                               int valMsg,
                               const QString &unit,
                               int unitMsg = -1  );

    static void showLabel( const QString &name,
                           bool bVisible,
                           QColor color=Qt::white,
                           const QString &txt="",
                           int x=0, int y=0
                           );

    static void showMsgBox( const QString &name,
                            const QString &info,
                            int mOkId );

    static void showMsgBox( const QString &name,
                            int infoId,
                            int mOkId );

    static void showErr( DsoErr errCode );

    static void showImage( const QString& imgName );

    static void showInfo( const QStringList &strings,
                          const QList<QColor> &colorList,
                          QImage *pImg = NULL,
                          int x = -1,
                          int y = -1,
                          int tmo = -1);

    static void showInfo( QImage *pImg,
                          int x = -1,
                          int y = -1,
                          int tmo = -1);

    static void showInfo( const QList<int> &resIdList,
                          const QList<QColor> &colorList,
                          QImage *pImg = NULL,
                          int x = -1,
                          int y = -1,
                          int tmo = -1);

    static void showInfo( const QString &string,
                          const QColor &color = Qt::white,
                          QImage *pImg = NULL,
                          int x = -1,
                          int y = -1,
                          int tmo = -1);

    static void showInfo( const int infoResId,
                          const QColor &color = Qt::white,
                          int x = -1,
                          int y = -1,
                          int tmo = -1);

    //! logStr
    static void logInfo( const QString &str, logType type=e_log_info );
    static void logInfo( const QString &str, int code, logType type=e_log_info );

    static void showVLine( const QString &name,
                           bool bVisible,
                           QColor color=Qt::white,
                           int x=0, int y=0, int height = 100, int width=1 );

    static void showHLine( const QString &name,
                           bool bVisible,
                           QColor color=Qt::white,
                           int x=10, int y=10, int width = 100, int height=1 );

    static Ctrl *findCtrl( const QString &name,
                    CtrlList *pCtrlList );
public:
    servGui( QString name, ServiceId eId = E_SERVICE_ID_ERR_LOGER );
    ~servGui();

protected:
    virtual bool actionFilter( const struServAction &action );

protected:
    void on_init();
    DsoErr on_exitActive( int code );

protected:
    //! error code
    DsoErr setErrCode( DsoErr errCode );
    DsoErr getErrCode();

    DsoErr setMenuShow( bool b );
    bool getMenuShow();

    //! notify
    DsoErr setInfoCfg( CtrlParagraph *pCtrl );
    CtrlParagraph *getInfoCfg();

    DsoErr setPopup( int id );
    int getPopup();

    //! key_icon + info
    DsoErr setIconInfo( CArgument &arg );
    CtrlIconInfo *getIconInfo();

    //! msg box
    DsoErr setMsgBox( CCtrlMsgBox *pMsgBox );
    CCtrlMsgBox *getMsgBox();

    DsoErr setMsgOk();
    DsoErr setMsgCancel();

    //! progress
    DsoErr setProgressCfg( CtrlProgress *pCtrl );
    CtrlProgress* getProgressCfg();

    DsoErr setProgressCancel();

    DsoErr setProgress( int val );
    int getProgress();

    DsoErr stepProgress( int n );

    DsoErr setProgressVisible( bool bShow );
    bool getProgressVisible();

    DsoErr setProgressInfo( QString strInfo );
    QString getProgressInfo();

    //! install notify
    DsoErr installNotify( CtrlNotifyLine *pCtrl );
    DsoErr uninstallNotify( CtrlNotifyLine *pCtrl );

    DsoErr onNotifySpy( int handle );

    CtrlList *getCtrlList();

    //! label
    DsoErr setCHLabel( CtrlCHLabel *pCtrl );

    //! vline
    DsoErr setHLine( CtrlHLine *pCtrl );
    DsoErr setVLine( CtrlVLine *pCtrl );

    //! animate req.
    DsoErr setLogoAnimateReq( bool bRun );
    bool getLogoAnimateReq();

    DsoErr setLogoAnimateReqRst();

    //! log info
    DsoErr setLogInfo( const QString &str );
    pointer getLogInfo();
    int getLogInfoSize();

private:
    void rstProgress();
    CtrlNotifyLine *findNotifyLine( CtrlNotifyLine *pCtrl );
    CtrlNotifyLine *findNotifyLine( int handle );

    Ctrl *findCtrl( Ctrl *pCtrl );
    bool trimCtrl( Ctrl *pCtrl );

private:
    DsoErr mErrCode;
    int mPopId;
    bool mbMenuShow;

    CtrlParagraph mInfoParagraph;

    CCtrlMsgBox mCtrlMsgBox;

    CtrlProgress mProgress;

    CtrlIconInfo mIconInfo;

    QList< CtrlNotifyLine * > mNotifyList;

    CtrlList mCtrlList;

    int mAnimateReq;

    QStringList mLogInfos, mViewLogInfos;

    actionFilterCause mActionFilterCause;
};

#endif // SERVGUI_H
