#ifndef CTRLPARAGRAPH_H
#define CTRLPARAGRAPH_H

#include <QtCore>
#include <QImage>
#include "../../baseclass/cargument.h"

/*!
 * \brief The Paragraph class
 * 字符段落
 * - 颜色
 * - 内容
 */
class Paragraph
{
public:
    Paragraph();
    Paragraph( const Paragraph &graph );
    Paragraph& operator=( const Paragraph &graph );

public:
    void set( const QColor &color, const QString &str );

public:
    QColor mColor;
    QString mContent;
};

typedef QList<Paragraph> ParagraphList;

/*!
 * \brief The CtrlParagraph class
 * 段落控制体
 */
class CtrlParagraph : public CObj
{
public:
    CtrlParagraph();
    ~CtrlParagraph();
public:
    void clear();

    void append( const QColor &color,
                 const QString &mContent );
    void append( QImage *pImage );

    void setXY( int x, int y );
    void setTimeout(int);

public:
    CtrlParagraph & operator=( const CtrlParagraph &para );

public:
    ParagraphList mParagraphs;
    int mX, mY;
    QImage *m_pImage;
    int nHideTimeout;//-1=default 2s
};


#endif // CTRLPARAGRAPH_H
