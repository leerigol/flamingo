#include "cctrlprogress.h"

CtrlProgress::CtrlProgress()
{
    mValA = 0;
    mValB = 100;
    mVal = 0;
    mStep = 1;

    mbShow = false;

    mClientId = -1;
    mClientMsg = -1;
    mClientPara = 0;

    mbModal = true;
}

bool CtrlProgress::isCancelable()
{
    return ( mClientId != -1 && mClientMsg != -1 );
}

bool CtrlProgress::isModal()
{
    return mbModal;
}
