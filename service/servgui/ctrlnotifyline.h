#ifndef CTRLNOTIFYLINE_H
#define CTRLNOTIFYLINE_H

#include "../../baseclass/cargument.h"

/*!
 * \brief The CtrlNotifyLine class
 * notify控制体，被创建到servgui中，监视其它服务的notify动作
 * - 颜色
 * - 内容
 * - 句柄，通过句柄识别是哪一个控制体被触发
 */
class CtrlNotifyLine : public CObj
{
public:
    CtrlNotifyLine();

public:
    void setNameMsg( const QString &name, int msg );

    void setColor( QColor color );
    void setCaption( const QString &cap );
    void setPostString( const QString &pstString );

    void setMsgs( int valMsg, int postStringMsg = -1 );

    void setSpyHandle( int handle );

    bool operator==( CtrlNotifyLine &ctrl );
public:
    QString mServName;      /*!< 目标服务名称 */
    int mNotifyMsg;         /*!< 目标服务消息*/

    QColor mColor;          /*!< 显示颜色 */
    QString mCaption;       /*!< 显示名称 */
    QString mPostString;    /*!< 显示后缀 */

    int    mRealValMsg;     /*!< 数值消息 float */
    int    mPostStringMsg;  /*!< 显示后缀消息 string */

    int    mSpyHandle;      /*!< 监视句柄 */
};

#endif // CTRLNOTIFYLINE_H
