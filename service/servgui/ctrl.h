#ifndef CTRL_H
#define CTRL_H

#include <QtCore>

#include "../../baseclass/cargument.h"

class Ctrl : public CObj
{
public:
    enum CtrlType
    {
        Ctrl_none,
        Ctrl_Label,
        Ctrl_HLine,
        Ctrl_VLine,
    };

protected:
    Ctrl();

public:
    void setColor( QColor color );
    QColor &getColor();

    void setName( const QString &name );
    QString &getName();

    QString &objectName();

    void setVisible( bool bVisible );
    bool getVisible();

    CtrlType getType();

public:
    bool operator==( Ctrl &ctrl );

private:
    QString toString( CtrlType type );

protected:
    QColor mColor;
    QString mName;
    bool mbVisible;

    CtrlType mTypeId;
    QString mObjectName;
};

#endif // CTRL_H
