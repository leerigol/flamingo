#ifndef CTRLCHLABEL_H
#define CTRLCHLABEL_H

#include <QtCore>

#include "ctrl.h"

class CtrlCHLabel : public Ctrl
{
public:
    CtrlCHLabel();

public:
    void setLabel( const QString &label );
    QString &getLabel();

    void setPos( int x, int y );
    QPoint getPos();
private:
    QString mLabel;
    QPoint mPt;

};

#endif // CTRLCHLABEL_H
