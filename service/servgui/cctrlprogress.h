#ifndef CCTRLPROGRESS_H
#define CCTRLPROGRESS_H

#include <QtCore>

#include "../../baseclass/cargument.h"

class CtrlProgress : public CObj
{
public:
    CtrlProgress();

public:
    bool isCancelable();
    bool isModal();
public:
    int mValA, mValB;       //! 进度条范围指示
    int mVal;               //! 进度条数值
    int mStep;              //! 进度条的步进值

    bool mbShow;            //! 进度条显示状态
    QString mInfo;          //! 进度条显示信息,Content of progress
    QString mTitle;       //! Title of progress

    //! in sevice
    int mClientId;         //! 进度条的客户id
    int mClientMsg;        //! 进度条的客户消息
    int mClientPara;       //! 进度条的客户参数
    bool mbModal;          //! can op other action in progress
};

#endif // CCTRLPROGRESS_H
