#include "ctrlnotifyline.h"

CtrlNotifyLine::CtrlNotifyLine()
{
    mNotifyMsg = -1;

    mColor = Qt::white;

    mRealValMsg = -1;
    mPostStringMsg = -1;
    mSpyHandle = -1;
}

void CtrlNotifyLine::setNameMsg( const QString &name, int msg )
{
    mServName = name;
    mNotifyMsg = msg;
}

void CtrlNotifyLine::setColor( QColor color )
{
    mColor = color;
}
void CtrlNotifyLine::setCaption( const QString &cap )
{
    mCaption = cap;
}

void CtrlNotifyLine::setPostString( const QString &pstString )
{
    mPostString = pstString;
}

void CtrlNotifyLine::setMsgs( int valMsg, int postStringMsg )
{
    mRealValMsg = valMsg;
    mPostStringMsg = postStringMsg;
}

void CtrlNotifyLine::setSpyHandle( int handle )
{
    mSpyHandle = handle;
}

bool CtrlNotifyLine::operator==( CtrlNotifyLine &ctrl )
{
    if ( mServName == ctrl.mServName
         && mNotifyMsg == ctrl.mNotifyMsg )
    {
        return true;
    }
    else
    {
        return false;
    }
}
