#ifndef CCTRLMSGBOX_H
#define CCTRLMSGBOX_H

#include <QtCore>

#include "ctrl.h"

#include "../../menu/wnd/rmessagebox.h"

class CCtrlMsgBox : public Ctrl
{
public:
    CCtrlMsgBox();

    CCtrlMsgBox( const CCtrlMsgBox &msgbox );
    CCtrlMsgBox& operator=( const CCtrlMsgBox &msgbox );

public:
    void setMsgBox( const QString &servName,
                    int okId,
                    const QString &info,
                    menu_res::RMessageBox::MsgBox_Button btn
                    = menu_res::RMessageBox::button_okcancel,
                    menu_res::RMessageBox::MsgBox_Icon icons
                    = menu_res::RMessageBox::icon_question );

public:
    int mOkId;
    QString mServName;
    QString mInfo;

    menu_res::RMessageBox::MsgBox_Button mButtons;
    menu_res::RMessageBox::MsgBox_Icon mIcons;

};

#endif // CCTRLMSGBOX_H
