#include "servgui.h"

#include "../../meta/crmeta.h"
#include "../../include/dsostd.h"
#include "../../include/dsocfg.h"

#include "../../gui/cguiformatter.h"

#include "../../service/service_msg.h"
#include "../../service/service_name.h"

IMPLEMENT_CMD( serviceExecutor, servGui )
start_of_entry()
#if 1
//! menu msg
on_set_int_void( MSG_GUI_SERV_ASK_OK, &servGui::setMsgOk ),
on_set_int_void( MSG_GUI_SERV_ASK_CANCEL, &servGui::setMsgCancel ),

on_set_int_void( MSG_GUI_SERV_CANCEL, &servGui::setProgressCancel ),

on_set_int_bool( CMD_SERVICE_PAGE_SHOW, &servGui::setMenuShow ),
on_get_bool( CMD_SERVICE_PAGE_SHOW, &servGui::getMenuShow ),

on_set_int_arg( MSG_GUI_NOTIFY_KEY_INFO, &servGui::setIconInfo ),
on_get_pointer( MSG_GUI_NOTIFY_KEY_INFO, &servGui::getIconInfo ),

//! system msg

on_set_int_int( CMD_SERVICE_EXIT_ACTIVE, &servGui::on_exitActive ),

on_set_void_void( CMD_SERVICE_INIT, &servGui::on_init ),
on_set_int_int( CMD_SERVICE_DO_ERR, &servGui::setErrCode ),
on_get_int( CMD_SERVICE_DO_ERR, &servGui::getErrCode ),

//! info
on_set_int_obj( servGui::cmd_info_cfg, &servGui::setInfoCfg  ),
on_get_pointer( servGui::cmd_info_cfg, &servGui::getInfoCfg ),

on_set_int_int( servGui::cmd_popup, &servGui::setPopup ),
on_get_int( servGui::cmd_popup, &servGui::getPopup ),

on_set_int_obj( servGui::cmd_msg_box, &servGui::setMsgBox ),
on_get_pointer( servGui::cmd_msg_box, &servGui::getMsgBox ),

//! progress
on_set_int_obj( servGui::cmd_progress_cfg, &servGui::setProgressCfg ),
on_get_pointer( servGui::cmd_progress_cfg, &servGui::getProgressCfg ),

on_set_int_int( servGui::cmd_progress_val, &servGui::setProgress ),
on_get_int( servGui::cmd_progress_val, &servGui::getProgress ),

on_set_int_int( servGui::cmd_progress_step, &servGui::stepProgress ),

on_set_int_bool( servGui::cmd_progress_visible, &servGui::setProgressVisible ),
on_get_bool( servGui::cmd_progress_visible, &servGui::getProgressVisible ),

on_set_int_string( servGui::cmd_progress_info, &servGui::setProgressInfo ),
on_get_string( servGui::cmd_progress_info, &servGui::getProgressInfo ),

//! notify install
on_set_int_obj( servGui::cmd_notify_install, &servGui::installNotify ),
on_set_int_obj( servGui::cmd_notify_uninstall, &servGui::uninstallNotify ),

on_set_int_int( servGui::cmd_notify_spy, &servGui::onNotifySpy ),

//! ctrl
on_get_pointer( servGui::qcmd_ctrl_list, &servGui::getCtrlList ),
//! ch label
on_set_int_obj( servGui::cmd_ch_label_set, &servGui::setCHLabel ),
//! line
on_set_int_obj( servGui::cmd_h_line_set, &servGui::setHLine ),
on_set_int_obj( servGui::cmd_v_line_set, &servGui::setVLine ),

//! animate
//on_set_int_bool( servGui::cmd_logo_animate_run, &servGui::setLogoAnimateReq ),
//on_get_bool( servGui::cmd_logo_animate_run, &servGui::getLogoAnimateReq ),

on_set_int_void( servGui::cmd_logo_animate_rst, &servGui::setLogoAnimateReqRst),

//! log info
on_set_int_string( servGui::cmd_log_str, &servGui::setLogInfo ),
on_get_pointer( servGui::qcmd_log_str, &servGui::getLogInfo ),
on_get_int( servGui::qcmd_log_str_size, &servGui::getLogInfoSize ),
#endif
end_of_entry()

/*!
 * \brief servGui::registerNotify
 * \param name  被监视的服务名称
 * \param msg   被监视的服务消息
 * \param color 显示颜色
 * \param caption 显示的名称，例如 Offset:
 * \param valMsg 显示的数值消息，通过查询返回float
 * \param unitStr 显示的单位字符串
 * \param unitMsg 单位字符串读取消息，返回string
 * 注册一个提示信息
 */
void servGui::registerNotify( const QString &name,
                           int msg,
                           QColor color,
                           const QString &caption,
                           int valMsg,
                           const QString &unitStr,
                           int unitMsg )
{
    CtrlNotifyLine *pNotifyLine;

    //! offset notify
    pNotifyLine = new CtrlNotifyLine;
    Q_ASSERT( NULL != pNotifyLine );

    //! config notify
    pNotifyLine->setNameMsg( name,
                             msg );
    pNotifyLine->setColor( color );
    pNotifyLine->setCaption( caption );
    pNotifyLine->setMsgs( valMsg, unitMsg );
    pNotifyLine->setPostString( unitStr );

    //! request gui
    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_notify_install,
                           (CObj*)pNotifyLine );
}

void servGui::showLabel( const QString &name,
                       bool bVisible,
                       QColor color,
                       const QString &txt,
                       int x, int y
                       )
{
    CtrlCHLabel *pCtrl;

    pCtrl = new CtrlCHLabel();
    Q_ASSERT( NULL != pCtrl );

    pCtrl->setName( name );
    pCtrl->setVisible( bVisible );
    pCtrl->setColor( color );
    pCtrl->setLabel( txt );
    pCtrl->setPos( x, y );

    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_ch_label_set,
                           (CObj*)pCtrl );
}

void servGui::showMsgBox( const QString &name,
                        const QString &info,
                        int okId )
{
    CCtrlMsgBox *pMsgBox;

    pMsgBox = new CCtrlMsgBox();
    Q_ASSERT( NULL != pMsgBox );

    pMsgBox->setMsgBox( name, okId, info );

    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_msg_box,
                           (CObj*)pMsgBox );
}

void servGui::showMsgBox( const QString &name,
                        int infoId,
                        int okId )
{
    QString strInfo;

    strInfo = sysGetString( infoId );

    servGui::showMsgBox( name, strInfo, okId );
}

void servGui::showErr( DsoErr errCode )
{
    if ( !sysGetStarted() ) return;

    serviceExecutor::post( E_SERVICE_ID_UI,
                           CMD_SERVICE_DO_ERR,
                           errCode );
}

void servGui::showImage( const QString& imgName )
{
    QImage img;

    if ( !img.load( imgName ) )
    { return; }

    servGui::showInfo( &img );
}

void servGui::showInfo( const QStringList &strings,
                        const QList<QColor> &colorList,
                        QImage *pImg,
                        int x,
                        int y,
                        int tmo)
{
    Q_ASSERT( strings.size() > 0 );

    if ( !sysGetStarted() ) return;

    CtrlParagraph *pParagraph;

    pParagraph = new CtrlParagraph();
    if( NULL == pParagraph )
    {
        return;
    }

    QColor color;
    for ( int i = 0; i < strings.size(); i++ )
    {
        //! default color
        if ( i >= colorList.size() )
        {
            color = Qt::white;
        }
        else
        {
            color = colorList[i];
        }

        pParagraph->append( color, strings[i] );
    }

    //! image
    if ( NULL != pImg )
    {
        pParagraph->append( pImg );
    }

    //! set x, y
    pParagraph->setXY( x, y );
    pParagraph->setTimeout(tmo);

    //! post to gui
    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_info_cfg,
                           (CObj*)pParagraph );
}

void servGui::showInfo( QImage *pImg,
                        int x,
                        int y,
                        int tmo)
{
    if ( !sysGetStarted() ) return;

    Q_ASSERT( NULL != pImg );

    CtrlParagraph *pParagraph;
    pParagraph = new CtrlParagraph();
    if( NULL == pParagraph )
    { return; }

    //! no string

    //! image
    pParagraph->append( pImg );

    //! set x, y
    pParagraph->setXY( x, y );

    pParagraph->setTimeout(tmo);

    //! post to gui
    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_info_cfg,
                           (CObj*)pParagraph );
}

void servGui::showInfo( const QList<int> &resIdList,
                        const QList<QColor> &colorList,
                        QImage *pImg,
                        int x,
                        int y,
                        int tmo )
{
    QStringList strList;

    foreach( int resId, resIdList )
    {
        strList.append( sysGetString( resId) );
    }

    servGui::showInfo( strList, colorList, pImg, x, y ,tmo);
}

void servGui::showInfo( const QString &string,
                        const QColor &color,
                        QImage *pImg,
                        int x,
                        int y,
                        int tmo)
{
    if ( !sysGetStarted() ) return;

    QStringList strings;
    QList<QColor> colors;

    strings.append( string );
    colors.append( color );

    servGui::showInfo( strings, colors, pImg, x, y, tmo );
}

void servGui::showInfo( const int infoResId,
                        const QColor &color,
                        int x,
                        int y,
                        int tmo )
{
    if ( !sysGetStarted() ) return;

    QString str;
    QImage img;
    int ret = sysGetResource( infoResId, str, img );
    if ( ret < 0 )
    { return; }

    //! is picture
    if ( !img.isNull() )
    {
        servGui::showInfo( &img, x, y );
        return;
    }

    //! is string
    if ( str.length() < 1 )
    {
        str = QString("Info:%1").arg( infoResId );
    }
    else
    {
    }

    //! show info
    showInfo(str, color, NULL, x, y ,tmo);
}

void servGui::logInfo( const QString &str, servGui::logType /*type*/ )
{
    QString dt = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    serviceExecutor::post(  serv_name_gui,
                            servGui::cmd_log_str,
                            str+"\n"+dt+"\n",
                            NULL );
}
void servGui::logInfo( const QString &str, int code, servGui::logType /*type*/ )
{
    QString fmtStr;
    fmtStr = QString("%1:%2").arg(str).arg(code);

    servGui::logInfo( fmtStr );
}

void servGui::showVLine( const QString &name,
                       bool bVisible,
                       QColor color,
                       int x, int y, int height, int width )
{
    CtrlVLine *pLine;

    pLine = new CtrlVLine;
    Q_ASSERT( NULL != pLine );

    pLine->setColor( color );
    pLine->setVisible( bVisible );
    pLine->setName( name );
    pLine->setRect( x, y, width, height );

    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_v_line_set,
                           (CObj*)pLine );
}

void servGui::showHLine( const QString &name,
                       bool bVisible,
                       QColor color,
                       int x, int y, int width, int height )
{
    CtrlHLine *pLine;

    pLine = new CtrlHLine;
    Q_ASSERT( NULL != pLine );

    pLine->setColor( color );
    pLine->setVisible( bVisible );
    pLine->setName( name );
    pLine->setRect( x, y, width, height );

    serviceExecutor::post( E_SERVICE_ID_UI,
                           servGui::cmd_h_line_set,
                           (CObj*)pLine );
}

Ctrl *servGui::findCtrl( const QString &name,
                CtrlList *pCtrlList )
{
    Q_ASSERT( NULL != pCtrlList );

    Ctrl *pItem;

    foreach ( pItem, *pCtrlList )
    {
        Q_ASSERT( pItem != NULL );

        if ( pItem->objectName() == name )
        {
            return pItem;
        }
    }

    return NULL;
}

servGui::servGui( QString name, ServiceId eId  ) :
         serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    mErrCode = ERR_NONE;
    mbMenuShow = true;
    mPopId = 0;

    mAnimateReq = 0;
    mActionFilterCause = action_filter_msgbox;

    //! progress
    rstProgress();
}

servGui::~servGui()
{
    CtrlNotifyLine *pItem;
    foreach( pItem, mNotifyList )
    {
        Q_ASSERT( pItem != NULL );
        delete pItem;
    }

    Ctrl *pCtrl;
    foreach( pCtrl, mCtrlList )
    {
        Q_ASSERT( pCtrl != NULL );
        delete pCtrl;
    }
}


bool servGui::actionFilter( const struServAction &action )
{
    //! do not filter mask msg
    if( action.servId == E_SERVICE_ID_MASK)
    {
        return false;
    }

    //! do not filter
    if ( !serviceExecutor::actionFilter(action) )
    {
        return false;
    }

    //! not quick
    if ( mActionFilterCause == action_filter_msgbox )
    {
        if ( action.servId == E_SERVICE_ID_QUICK )
        {
            return false;
        }
    }
    else
    {

    }

    return true;
}

DsoErr servGui::on_exitActive( int id )
{
    if ( mActionFilterCause == action_filter_msgbox )
    {
        setuiChange( MSG_GUI_SERV_ASK_CANCEL );
        uninstallActionFilter();
    }

    return serviceExecutor::on_exitActive( id );
}

void servGui::on_init()
{
}

DsoErr servGui::setErrCode( DsoErr errCode )
{
    mErrCode = errCode;

    return ERR_NONE;
}
DsoErr servGui::getErrCode()
{
    return mErrCode;
}

DsoErr servGui::setMenuShow( bool b )
{
    mbMenuShow = b;
    return ERR_NONE;
}
bool servGui::getMenuShow()
{ return mbMenuShow; }

/*!
 * \brief servGui::setInfoCfg
 * \param pCtrl
 * \return
 * 配置弹出提示
 * - CtrlParagraph 从 CObj 继承
 * - 在框架里统一释放资源
 */
DsoErr servGui::setInfoCfg( CtrlParagraph *pCtrl )
{
    Q_ASSERT( NULL != pCtrl );

    //! copy
    mInfoParagraph = *pCtrl;

    return ERR_NONE;
}

CtrlParagraph *servGui::getInfoCfg()
{
    return &mInfoParagraph;
}

//! 弹出多语言提示
DsoErr servGui::setPopup( int id )
{
    mPopId = id;
    return ERR_NONE;
}
int servGui::getPopup()
{
    return mPopId;
}

DsoErr servGui::setIconInfo( CArgument &arg )
{
    if ( arg.size() != 2 )
    { return ERR_INVALID_INPUT; }

    //! debug used
//    async( cmd_popup, arg[1].iVal );

    //! check type
    if ( arg[0].vType != arg[1].vType
         || arg[0].vType != val_int )
    { return ERR_INVALID_INPUT; }

    mIconInfo.setInfo( arg[0].iVal,
                      arg[1].iVal );

    return ERR_NONE;
}

CtrlIconInfo *servGui::getIconInfo()
{
    return &mIconInfo;
}

DsoErr servGui::setMsgBox( CCtrlMsgBox *pMsgBox )
{
    Q_ASSERT( NULL != pMsgBox );
    mCtrlMsgBox = *pMsgBox;

    //! set active
    async( CMD_SERVICE_ACTIVE, 1 );

    mActionFilterCause = action_filter_msgbox;
    installActionFilter();

    return ERR_NONE;
}
CCtrlMsgBox *servGui::getMsgBox()
{
    return &mCtrlMsgBox;
}

DsoErr servGui::setMsgOk()
{
    //! return
    serviceExecutor::post( mCtrlMsgBox.mServName,
                           mCtrlMsgBox.mOkId,
                           (int)1 );
    //! deactive now
    setdeActive();

    //! uninstall filter
    uninstallActionFilter();

    return ERR_NONE;
}
DsoErr servGui::setMsgCancel()
{
    setdeActive();

    //! uninstall filter
    uninstallActionFilter();
    return ERR_NONE;
}

/*!
 * \brief servGui::setProgressCfg
 * \param pCtrl
 * \return
 * 进度条配置
 * - CtrlProgress 从 CObj 继承
 */
DsoErr servGui::setProgressCfg( CtrlProgress *pCtrl )
{
    Q_ASSERT( NULL != pCtrl );

    //! cfg progress
    mProgress = *pCtrl;

    //! show
    if ( mProgress.mbShow )
    {
        //! cance able
        if ( mProgress.isCancelable() )
        {
            setActive( MSG_GUI_SERV_CANCEL );
        }

        if ( mProgress.isModal() )
        {
            mActionFilterCause = action_filter_progress;
            installActionFilter();
        }
    }
    //! hide
    else
    {
    }

    return ERR_NONE;
}

CtrlProgress* servGui::getProgressCfg()
{
    return &mProgress;
}

/*!
 * \brief servGui::setProgressCancel
 * \return
 * 来自app的进度“取消”请求
 * - 如果clientId,clientMsg配置有效，则通知客户
 */
DsoErr servGui::setProgressCancel()
{
    //! canceable
    if ( mProgress.isCancelable() )
    {
        setuiEnable( false );
        return post( mProgress.mClientId,
                     mProgress.mClientMsg,
                     mProgress.mClientPara );
    }
    else
    {
        return ERR_INVALID_CONFIG;
    }
}

/*!
 * \brief servGui::setProgress
 * \param val
 * \return
 * 设置进度条的进度数值
 * - 进度数值需要在进度范围内
 * - [mValA,mValB]
 */
DsoErr servGui::setProgress( int val )
{
    if ( val < mProgress.mValA )
    {
        mProgress.mVal = mProgress.mValA;
        return ERR_OVER_LOW_RANGE;
    }
    else if ( val > mProgress.mValB )
    {
        mProgress.mVal = mProgress.mValB;
        return ERR_OVER_UPPER_RANGE;
    }
    else
    {
        mProgress.mVal = val;
        return ERR_NONE;
    }
}
int servGui::getProgress()
{
    return mProgress.mVal;
}

/*!
 * \brief servGui::stepProgress
 * \param n
 * \return
 * 在当前进度值基础上步进n步
 * - 步进值=mStep
 * - 步进结果= mVal + n * mStep
 */
DsoErr servGui::stepProgress( int n )
{
    return setProgress( n * mProgress.mStep + mProgress.mVal );
}

DsoErr servGui::setProgressVisible( bool bShow )
{
    //! show
    if ( bShow )
    {
    }
    //! hide
    else
    {
        //! jump the menu
        if ( mProgress.mbShow && mProgress.isCancelable() )
        {
            mUiAttr.setEnable( MSG_GUI_SERV_CANCEL, true );

            setdeActive();
        }

        if ( mProgress.mbShow && mProgress.isModal() )
        {
            uninstallActionFilter();
            LOG_DBG();
        }

        //! rst progress
        rstProgress();
    }

    mProgress.mbShow = bShow;

    //! rst again
    if ( !bShow )
    {
        rstProgress();
    }

    return ERR_NONE;
}
bool servGui::getProgressVisible()
{
    return mProgress.mbShow;
}

DsoErr servGui::setProgressInfo( QString strInfo )
{
    mProgress.mInfo = strInfo;
    return ERR_NONE;
}
QString servGui::getProgressInfo()
{
    return mProgress.mInfo;
}

/*!
 * \brief servGui::installNotify
 * \param pCtrl
 * \return
 * 安装notify监视
 */
DsoErr servGui::installNotify(CtrlNotifyLine *pCtrl)
{
    Q_ASSERT( pCtrl != NULL );

    //! find ctrl
    CtrlNotifyLine *pItem;
    pItem = findNotifyLine( pCtrl );
    if ( NULL != pItem )
    {
        qDebug()<<__FUNCTION__<<__LINE__;
        return ERR_NONE;
    }

    pItem = new CtrlNotifyLine;
    Q_ASSERT( pItem != NULL );

    *pItem = *pCtrl;

    //! spy on
    //! 接收的参数是spy id, 这样就可以在一个消息里统一处理
    int spyId;
    spyId = serviceExecutor::spyOn( pItem->mServName,
                                    pItem->mNotifyMsg,
                                    getName(),
                                    cmd_notify_spy,
                                    (eSpyType)(e_spy_post | e_spy_user),
                                    spy_id
                                    );
    if ( spyId >= 0 )
    {
        pItem->setSpyHandle( spyId );
        mNotifyList.append( pItem );
    }
    else
    {
        delete pItem;
        return ERR_SERV_GUI_INSTALL_NOTIFY;
    }

    return ERR_NONE;
}

DsoErr servGui::uninstallNotify(CtrlNotifyLine *pCtrl)
{
    Q_ASSERT( pCtrl != NULL );

    //! find ctrl
    CtrlNotifyLine *pItem;
    pItem = findNotifyLine( pCtrl );
    if ( NULL == pItem )
    { return ERR_SERV_GUI_UNINSTALL_NOTIFY; }

    serviceExecutor::spyOff( pCtrl->mServName,
                             pCtrl->mNotifyMsg,
                             getName() );
    delete pItem;
    mNotifyList.removeOne( pItem );

    return ERR_NONE;
}

/*!
 * \brief servGui::onNotifySpy
 * \param handle
 * \return
 * 接收到注册的notify消息
 * - 所有注册的notify依据spy时的id进行区分
 * - 注册时指定了侦听的参数类型
 */
DsoErr servGui::onNotifySpy( int handle )
{
    if ( handle < 0 )
    {   return ERR_INVALID_INPUT; }

    CtrlNotifyLine *pCtrl;
    pCtrl = findNotifyLine( handle );

    if ( NULL == pCtrl )
    {   return ERR_SERV_GUI_NOTIFY_SPY; }

    float fVal;
    QString strVal;

    serviceExecutor::query( pCtrl->mServName,
                            pCtrl->mRealValMsg,
                            fVal );

    if ( pCtrl->mPostStringMsg != -1 )
    {
        serviceExecutor::query( pCtrl->mServName,
                                pCtrl->mPostStringMsg,
                                strVal );
    }
    else
    {
        strVal = pCtrl->mPostString;
    }

    QString str;
    QString fmtVal;

    str = pCtrl->mCaption;

    gui_fmt::CGuiFormatter::format( fmtVal, fVal );
    str.append( fmtVal );

    str.append( strVal );

    CtrlParagraph *pParagraph;

    pParagraph = new CtrlParagraph();
    Q_ASSERT( NULL != pParagraph );

    pParagraph->append( pCtrl->mColor, str );

    async( cmd_info_cfg, pParagraph );

    return ERR_NONE;
}

CtrlList *servGui::getCtrlList()
{
    return &mCtrlList;
}

DsoErr servGui::setCHLabel( CtrlCHLabel *pCtrl )
{
    Q_ASSERT( NULL != pCtrl );

    CtrlCHLabel *pLabel;
    Ctrl *pItem;

    if ( trimCtrl( pCtrl ) )
    {
        return ERR_NONE;
    }

    if ( !pCtrl->getVisible() )
    {
        return ERR_NONE;
    }

    pItem = findCtrl( pCtrl );
    if ( NULL == pItem )
    {
        pCtrl->accept();
        mCtrlList.append( pCtrl );
    }
    else
    {
        pLabel = (CtrlCHLabel*)pItem;
        *pLabel = *pCtrl;
    }

    return ERR_NONE;
}

DsoErr servGui::setHLine( CtrlHLine *pCtrl )
{
    Q_ASSERT( NULL != pCtrl );

    Ctrl *pItem;

    if ( trimCtrl( pCtrl ) )
    {
        return ERR_NONE;
    }

    if ( !pCtrl->getVisible() )
    {
        return ERR_NONE;
    }

    pItem = findCtrl( pCtrl );
    //! not exist
    if ( NULL == pItem )
    {
        pCtrl->accept();
        mCtrlList.append( pCtrl );
    }
    //! exist now
    else
    {
        CtrlHLine *pHLine;

        pHLine = (CtrlHLine*)pItem;
        *pHLine = *pCtrl;
    }

    return ERR_NONE;
}
DsoErr servGui::setVLine( CtrlVLine *pCtrl )
{
    Q_ASSERT( NULL != pCtrl );

    Ctrl *pItem;

    if ( trimCtrl( pCtrl ) )
    {
        return ERR_NONE;
    }

    if ( !pCtrl->getVisible() )
    {
        return ERR_NONE;
    }

    pItem = findCtrl( pCtrl );
    //! not exist
    if ( NULL == pItem )
    {
        pCtrl->accept();
        mCtrlList.append( pCtrl );
    }
    //! exist now
    else
    {
        CtrlVLine *pVLine;

        pVLine = (CtrlVLine*)pItem;
        *pVLine = *pCtrl;
    }

    return ERR_NONE;
}

DsoErr servGui::setLogoAnimateReq( bool bRun )
{
    if ( bRun )
    {
        mAnimateReq++;

        if ( mAnimateReq >= INT_MAX-1 )
        { mAnimateReq = INT_MAX-1; }
    }
    else
    {
        mAnimateReq--;
        if ( mAnimateReq < 0 )
        { mAnimateReq = 0; }
    }

    return ERR_NONE;
}
bool servGui::getLogoAnimateReq()
{
    return mAnimateReq == 0;
}

DsoErr servGui::setLogoAnimateReqRst()
{
    mAnimateReq = 0;
    return ERR_NONE;
}

DsoErr servGui::setLogInfo( const QString &str )
{
    mLogInfos.append( str );

    return ERR_NONE;
}
pointer servGui::getLogInfo()
{
    //! export info
    mViewLogInfos = mLogInfos;
    mLogInfos.clear();

    return &mViewLogInfos;
}
int servGui::getLogInfoSize()
{ return mLogInfos.size(); }

void servGui::rstProgress()
{
    mProgress.mbShow = false;
    mProgress.mVal = 0;
    mProgress.mValA = 0;
    mProgress.mValB = 0;

    mProgress.mClientId = -1;
    mProgress.mClientMsg = -1;
}

/*!
 * \brief servGui::findNotifyLine
 * \param pCtrl
 * \return
 * 在已经注册的notify列表中查找
 * - 被监视名称
 * - 被监视消息
 * 都相同时，判定为相同
 */
CtrlNotifyLine *servGui::findNotifyLine( CtrlNotifyLine *pCtrl )
{
    CtrlNotifyLine *pItem;

    foreach( pItem, mNotifyList )
    {
        Q_ASSERT( NULL != pItem );

        if ( *pItem == *pCtrl )
        {
            return pItem;
        }
    }

    return NULL;
}
/*!
 * \brief servGui::findNotifyLine
 * \param handle
 * \return
 * 通过句柄查找notify注册项
 */
CtrlNotifyLine *servGui::findNotifyLine( int handle )
{
    CtrlNotifyLine *pItem;

    foreach( pItem, mNotifyList )
    {
        Q_ASSERT( NULL != pItem );

        if ( pItem->mSpyHandle == handle )
        {
            return pItem;
        }
    }

    return NULL;
}

Ctrl *servGui::findCtrl( Ctrl *pCtrl )
{
    Q_ASSERT( NULL != pCtrl );

    Ctrl *pItem;
    foreach( pItem, mCtrlList )
    {
        if ( *pItem == *pCtrl )
        {
            return pItem;
        }
    }

    return NULL;
}

/*!
 * \brief servGui::trimCtrl
 * \param pCtrl
 * \return true--项目被清除
 * 清除被隐藏的项目
 * - 隐藏的项目被从列表中删除
 */
bool servGui::trimCtrl( Ctrl *pCtrl )
{
    Ctrl *pCtrlIn;

    if ( !pCtrl->getVisible() )
    {
        pCtrlIn = findCtrl( pCtrl );

        if ( NULL != pCtrlIn )
        {
            mCtrlList.removeOne( pCtrlIn );
            delete pCtrlIn;

            return true;
        }
        else
        {
            return false;
        }
    }

    return false;
}

