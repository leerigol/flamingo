#ifndef WAVNAVIGATOR_H
#define WAVNAVIGATOR_H

#include <QtCore>

class wavNavigator
{
public:
    wavNavigator();

public:
    void setMsg( int msg );
    int getMsg();

    void setSpeed( int speed );
    void shiftSpeed();
    int getSpeed();
    int getXSpeed();

    void setStep( qlonglong step );
    qlonglong getStep();

    void setNow( qlonglong tNow );
    void setRange(  qlonglong tMax, qlonglong tMin );
    qlonglong getMax();
    qlonglong getMin();

    void setRun( bool bRun );
    bool getRun();

    bool step( qlonglong &val );

public:
    int mMsg;

    int mSpeed;         //! x1,x10,x100
    qlonglong mStep;
    qlonglong mNow, mMax, mMin;
    bool mbRun;
    int mDir;

};
#endif // WAVNAVIGATOR_H
