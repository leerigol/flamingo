
#include "servhori.h"
#include "../service_msg.h"

void servHori::on_cmd_zoom_status_update()
{
    AcquireTimemode timeMode;

    timeMode = mAcquire.getTimemode();

    if ( timeMode == Acquire_ROLL )
    {
        mUiAttr.setEnable( MSG_HOR_ZOOM_ON, false );
    }
    else if ( timeMode == Acquire_XY)
    {
        mUiAttr.setEnable( MSG_HOR_ZOOM_ON, false );
    }
    else
    {
        mUiAttr.setEnable( MSG_HOR_ZOOM_ON, true );
    }
}

void servHori::updateXxx( EngineHoriInfo &info )
{
    if ( Acquire_YT == mAcquire.getTimemode() )
    {
        updateMemDep_YT( info );
    }
    else if ( Acquire_XY == mAcquire.getTimemode() )
    {
        updateMemDep_XY( info );
    }
    else if ( Acquire_ROLL == mAcquire.getTimemode() )
    {
        updateMemDep_Roll( info );
    }
    else
    {}
}

void servHori::updateMemDep( EngineHoriInfo & /*info*/ )
{
    //bug 1386 by hxh
    for ( int i = Acquire_Depth_Auto; i <= Acquire_Depth_500M; i++ )
    {
        mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, i , false);
    }
    //! only 1M
    mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, false );
}

/*
4通道模式，标配1k,10k,100k,1M,10M,25M，选配50M,100M,125M
2通道模式，标配1k,10k,100k,1M,10M,25M，50M，选配100M,125M，250M
1通道模式，标配1k,10k,100k,1M,10M,25M，50M,100M，选配125,250M，500M

以上中，打开平均后，每个存储深度不能超过25M

2018-03-24 HXH BUG2402
*/
void servHori::updateMemDep_YT( EngineHoriInfo &info )
{
    bool bEnable, bAvg;

    //! running
    if ( mControl.getStatus() == Control_Runing )
    {
        bEnable = true;
    }
    else
    {
        bEnable = false;
    }

    //! avgEn
    bAvg = ( mAcquire.getAcquireMode() == Acquire_Average );

    //! norm
    for ( int i = Acquire_Depth_Auto; i <= Acquire_Depth_25M; i++ )
    {
        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH, i , bEnable, true);
    }

    mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_50M,  bEnable);
    mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_100M, bEnable);
    mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_125M, bEnable);
    mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_250M, bEnable);
    mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_500M, bEnable);


    //for bug1401 by hxh
    if( getTimeViewMode() == Acquire_SCAN )
    {
        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_1K , false, false);
        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_10K , false, false);
        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_100K , false, false);
    }

    //for bug1570 by hxh
    if(bAvg &&
       mAcquire.getChAcqDepIndex() > Acquire_Depth_25M &&
       mControl.m_eAction != Control_Stop )
    {
        //mAcquire.setChAcqDepIndex(Acquire_Depth_25M);
        //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_25M) );
        async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_25M);
    }

    //! chan cnt
    if ( info.mChCnt == 4 && mControl.m_eAction != Control_Stop)
    {
        //for bug1686 by hxh.
        if( mOpt500En)
        {
            if( mAcquire.getChAcqDepIndex() > Acquire_Depth_125M )
            {
                //mAcquire.setChAcqDepIndex(Acquire_Depth_125M);
                //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_125M) );
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_125M);
            }
        }
        else if( mOpt250En)
        {
            if( mAcquire.getChAcqDepIndex() > Acquire_Depth_50M )
            {
                //mAcquire.setChAcqDepIndex(Acquire_Depth_50M);
                //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_50M) );
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_50M);
            }
        }
        else if( mAcquire.getChAcqDepIndex() > Acquire_Depth_25M )
        {
            //mAcquire.setChAcqDepIndex(Acquire_Depth_25M);
            //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_25M) );
            async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_25M);
        }


        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_500M,
                                  false,
                                  false );

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_250M,
                                  false,
                                  false );
        //! avg
        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_125M,
                                  !bAvg && bEnable && mOpt500En,
                                  !bAvg && bEnable && mOpt500En );

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_100M,
                                  !bAvg && bEnable && mOpt500En,
                                  !bAvg && bEnable && mOpt500En );

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_50M,
                                  !bAvg && bEnable && mOpt250En,
                                  !bAvg && bEnable && mOpt250En);
    }
    else if  ( info.mChCnt == 2 && mControl.m_eAction != Control_Stop)
    {
        //for bug1686 by hxh.
        if ( mOpt500En )
        {
            if( mAcquire.getChAcqDepIndex() > Acquire_Depth_250M )
            {
                //mAcquire.setChAcqDepIndex(Acquire_Depth_250M);
                //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_250M) );
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_250M);
            }            
        }
        else if(  mOpt250En )
        {
            if( mAcquire.getChAcqDepIndex() > Acquire_Depth_125M )
            {
                //mAcquire.setChAcqDepIndex(Acquire_Depth_125M);
                //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_125M) );
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_125M);
            }
        }
        else if( mAcquire.getChAcqDepIndex() > Acquire_Depth_50M )
        {
            //mAcquire.setChAcqDepIndex(Acquire_Depth_50M);
            //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_50M) );
            async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_50M);
        }

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_500M,
                                  false,
                                  false );

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_250M,
                                  !bAvg && bEnable && mOpt500En,
                                  !bAvg && bEnable && mOpt500En);
        //! avg
        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_125M,
                                  !bAvg && bEnable && mOpt250En,
                                  !bAvg && bEnable && mOpt250En );

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_100M,
                                  !bAvg && bEnable && mOpt250En,
                                  !bAvg && bEnable && mOpt250En);

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_50M,
                                  !bAvg && bEnable,
                                  !bAvg && bEnable );
    }
    else if ( info.mChCnt == 1 && mControl.m_eAction != Control_Stop)
    {
        //for bug1686 by hxh.
        if ( mOpt500En )
        {
        }
        else if( mOpt250En )
        {
            if( mAcquire.getChAcqDepIndex() > Acquire_Depth_250M )
            {
                //mAcquire.setChAcqDepIndex(Acquire_Depth_250M);
                //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_250M) );
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_250M);
            }
        }
        else
        {
            if( mAcquire.getChAcqDepIndex() > Acquire_Depth_100M )
            {
                //mAcquire.setChAcqDepIndex(Acquire_Depth_100M);
                //postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_100M) );
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_100M);
            }
        }

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_500M,
                                  !bAvg && bEnable && mOpt500En,
                                  !bAvg && bEnable && mOpt500En);

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_250M,
                                  !bAvg && bEnable && mOpt250En,
                                  !bAvg && bEnable && mOpt250En );

        //! avg
        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_125M,
                                  !bAvg && bEnable && mOpt250En,
                                  !bAvg && bEnable && mOpt250En );

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_100M,
                                  !bAvg && bEnable,
                                  !bAvg && bEnable );

        mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH,
                                  Acquire_Depth_50M,
                                  !bAvg && bEnable,
                                  !bAvg && bEnable );
    }
    else
    {
    }
}

void servHori::updateMemDep_XY( EngineHoriInfo &info )
{
    updateMemDep_YT( info );
}
void servHori::updateMemDep_Roll( EngineHoriInfo &info )
{
    updateMemDep( info );
}

void servHori::onChangeTimeOffset_Roll( qlonglong offset )
{
    CHorizontal::getMainView().setRollOffset( offset);
    //LOG_DBG()<<offset;
    setuiChange(MSG_HORIZONTAL_TIMEOFFSET);
}

void servHori::ledRunStopSingle( bool bRun, bool bStop, bool bSingle )
{
    syncEngine( ENGINE_LED_OP,
                DsoEngine::led_run,
                bRun );

    syncEngine( ENGINE_LED_OP,
                DsoEngine::led_stop,
                bStop );

    syncEngine( ENGINE_LED_OP,
                DsoEngine::led_single,
                bSingle );
}

void servHori::on_system_running()
{
}

void servHori::on_system_stoped()
{
}

void servHori::freezeEngine()
{
    postEngine(ENGINE_FREEZE);
}

void servHori::unfreezeEngine()
{
    serviceExecutor::flushQueue();
    postEngine(ENGINE_UNFREEZE);
    setMainScale(getMainScale());
}

void servHori::applyEngine()
{
    qint64 scale = getMainScale();
    setMainScale(scale);
}
