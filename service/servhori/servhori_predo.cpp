#include <QTime>

#include "servhori.h"
#include "../servdso/servdso.h"

IMPLEMENT_PRE_DO( serviceExecutor, servHori )
start_of_predo()
on_predo( MSG_HOR_TIME_MODE, &servHori::on_pre_time_mode ),
end_of_predo()

/***************************************************************************
函数名称： on_pre_time_mode
功能描述： 设置水平模式之前的一些设置
输入参数： 新旧模式参数
输出参数：
返 回 值：

修改记录1：// 修改历史记录，包括修改日期、修改者及修改内容
修改日期： 2018-3-10
版 本 号：
修 改 人： HXH
修改内容： 从YT-XY，不需要设置XY参数，只需要关闭一些特殊设置
         从ROLL-XY，需要恢复XY的设置
         //for bug1735
****************************************************************************/
void servHori::on_pre_time_mode( CArgument &arg )
{
    LOG_DBG()<<getDebugTime();
    if ( !arg.checkType( val_int ) )
    {
        return;
    }

    int timeMode;
    arg.getVal( timeMode );

    if ( timeMode == mAcquire.getTimemode())
    {
        return;
    }

    //added by hxh for 1295 1556
    {
        m_bNeedEat = true;
        serviceExecutor::send(serv_name_trace,
                              servTrace::cmd_trace_run,
                              false);

        //for bug1735
        nTempAction = mControl.m_eAction;
    }
    //! old
    int oldMode = mAcquire.getTimemode();
    switch ( oldMode )
    {
        case Acquire_YT:
            mYTSession.clear();

            //not allow this situation. by hxh. for bug3145
            if( getRunStop() == Control_Run &&
                m_bAutoRoll &&
                oldMode == Acquire_YT &&
                timeMode== Acquire_ROLL &&
                m_mainView.m_s64Scale >= time_ms(200) )
            {
                m_mainView.m_s64Scale = time_ms(100);
            }
            servDso::pullSession( mYTSession, serviceExecutor::cacheList() );
            break;

        case Acquire_XY:
            mXYSession.clear();
            servDso::pullSession( mXYSession, serviceExecutor::cacheList() );
            break;

        case Acquire_ROLL:
            //qDebug()<<"pull roll";
            mRollSession.clear();
            servDso::pullSession( mRollSession, serviceExecutor::cacheList() );
            break;
        default:
            break;
    }

    //! enter
    QList<int> servList;
    switch( timeMode )
    {
        case Acquire_YT:
            {
                mUiAttr.setVisible( MSG_HOR_ZOOM_ON, true );
                mUiAttr.setVisible( MSG_HOR_FINE_ON, true );

                mUiAttr.setEnable( MSG_HOR_ACQ_MODE_YT, true );
                mUiAttr.setEnable( MSG_HOR_ACQ_ANTI_ALIASING, true );

                mUiAttr.setEnable( MSG_HORIZONTAL_TIMEOFFSET, true );

                mUiAttr.setVisible( MSG_HORIZONTAL_NORM_EXPAND, true );
                mUiAttr.setVisible( MSG_HORIZONTAL_ACQNORM_EXPAND, true );

                mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH );

                mUiAttr.setVisible( MSG_HOR_LA_SA_RATE, true );
                mUiAttr.setVisible( MSG_HOR_LA_MEM_DEPTH, true );

                mUiAttr.setEnable( servHori::cmd_main_offset, true );
                mUiAttr.setEnable( servHori::cmd_zoom_offset, true );
            }
            if ( mYTSession.size() > 0 )
            {
                servDso::pushSession( mYTSession, servList );
            }
            else
            {
                return;
            }

            break;

        case Acquire_XY:
            {
                mUiAttr.setVisible( MSG_HOR_ZOOM_ON, false );
                mUiAttr.setVisible( MSG_HOR_FINE_ON, false );

                mUiAttr.setEnable( MSG_HOR_ACQ_MODE_YT, false );
                mUiAttr.setEnable( MSG_HOR_ACQ_ANTI_ALIASING, false );

                mUiAttr.setEnable( MSG_HORIZONTAL_TIMEOFFSET, false );

                mUiAttr.setVisible( MSG_HORIZONTAL_NORM_EXPAND, false );
                mUiAttr.setVisible( MSG_HORIZONTAL_ACQNORM_EXPAND, false );

                mUiAttr.setVisible( MSG_HOR_XY_TYPE, false );
                mUiAttr.setVisible( MSG_HOR_XY_MODE_YT_DISP, false );

                mUiAttr.setEnableVisible( MSG_HOR_ACQ_MEM_DEPTH );

                mUiAttr.setVisible( MSG_HOR_LA_SA_RATE, false );
                mUiAttr.setVisible( MSG_HOR_LA_MEM_DEPTH, false );

                mUiAttr.setEnable( servHori::cmd_main_offset, false );
                mUiAttr.setEnable( servHori::cmd_zoom_offset, false );

                if ( mControl.getStatus() == Control_Stoped )
                {
                    mUiAttr.setEnable( MSG_HORI_MAIN_SCALE, false );
                }
                else
                {
                    mUiAttr.setEnable( MSG_HORI_MAIN_SCALE, true );
                }
            }

            // YT-XY   NO pushSession
            // ROLL-XY pushSession
            if ( mXYSession.size() > 0 && mAcquire.getTimemode() == Acquire_ROLL)
            {
                servDso::pushSession( mXYSession, servList );
            }
            else
            {
                // From YT to XY
                mAcquire.setTimeMode(Acquire_XY);
            }
            break;

        case Acquire_ROLL:
            {
                mUiAttr.setVisible( MSG_HOR_FINE_ON, true );

                mUiAttr.setEnable( MSG_HOR_ACQ_ANTI_ALIASING, false );

                mUiAttr.setEnable( MSG_HORIZONTAL_TIMEOFFSET, true );

                mUiAttr.setVisible( MSG_HORIZONTAL_NORM_EXPAND, false );
                mUiAttr.setVisible( MSG_HORIZONTAL_ACQNORM_EXPAND, false );

                EngineHoriInfo noused;
                updateMemDep_Roll(noused);

                mUiAttr.setVisible( MSG_HOR_LA_SA_RATE, true );
                mUiAttr.setVisible( MSG_HOR_LA_MEM_DEPTH, true );

                mUiAttr.setEnable( servHori::cmd_main_offset, true );
                mUiAttr.setEnable( servHori::cmd_zoom_offset, true );
            }

            if ( mRollSession.size() > 0 )
            {
                bool bAutoRoll = m_bAutoRoll;
                servDso::pushSession( mRollSession, servList );
                m_bAutoRoll = bAutoRoll;
            }
            else
            {
                mAcquire.setTimeMode(Acquire_ROLL);//return;
            }
            break;
        default:
            break;
    }

    if( nTempAction == Control_Stop )
    {
        EngineHoriInfo noused;
        updateMemDep(noused);
    }

    //! startup
    servDso::startupSession( servList );
    mControl.setAction(  nTempAction );

//    servList.removeOne(E_SERVICE_ID_HORI);
//    configEngine();
    serviceExecutor::flushQueue();
}
