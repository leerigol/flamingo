#include "../../include/dsostd.h"
#include "../service_msg.h"

#include "../../arith/norm125.h"
#include "../../arith/ln2.h"

#include "../servdso/servdso.h" //! checkRange
#include "../servgui/servgui.h" //! show infos
#include "../servvert/servvert.h"
#include "servhori.h"

#include "../servmath/servmath.h"

#include "../servdisplay/servdisplay.h"
#include "../../include/dsodbg.h"
struct acqIndex_Depth{
    AcquireDepth acqIndex;
    int depth;
};

static acqIndex_Depth _acqIndex_Depth[]
{
    { Acquire_Depth_Auto, 0 },

    { Acquire_Depth_1K, 1000 },
    { Acquire_Depth_10K, 10000 },
    { Acquire_Depth_100K, 100000 },
    { Acquire_Depth_1M, 1000000 },

    { Acquire_Depth_10M, 10000000 },
    { Acquire_Depth_25M, 25000000 },
    { Acquire_Depth_50M, 50000000 },
    { Acquire_Depth_100M, 100000000 },
    { Acquire_Depth_125M, 125000000 },
    { Acquire_Depth_250M, 250000000 },

    { Acquire_Depth_500M, 500000000 },
    { Acquire_Depth_1G,   1000000000 },
};

servHori::servHori( QString name, ServiceId eId )
            : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    rst();

    appendCacheList(this);

    ISerial::mVersion = 0xB;
}

DsoErr servHori::setControlAction( ControlAction action )
{
    ControlAction lastAction = getRunStop();
    mControl.setAction( action );


    if ( Control_Run == action )
    {
        //qDebug()<<__FUNCTION__<<__LINE__<<"run"<<action;
        //BUG 810 BY HXH
        if ( mAcquire.getTimemode() != Acquire_ROLL )
        {
            mUiAttr.setEnable(MSG_HOR_ACQ_MEM_DEPTH, true);
        }
        else
        {
            //by hxh. add autoroll
            if( !m_bAutoRoll && getMainScale() < time_ms(200) )
            {
                setMainScale( time_ms(200) );
            }
        }
        //! 由于run和stop状态下的偏移范围不一样
        if(getMainOffset() < -5 *getMainScale())
        {
            setMainOffset( -5 *getMainScale() );
        }
        ledRunStopSingle( true, false, false );
        setControlStatus( Control_Runing );

        /*******************Tek mode*****************/
        //当开关打开后，取消慢扫描模式，自动进入滚动模式，类似Tek
        if(m_bAutoRoll && lastAction == Control_Stop)
        {
            qint64 scale = getMainScale();
            if( getTimeMode() == Acquire_YT &&
                scale >= time_ms(200)  )
            {
                QWidget *w = sysGetView(this->getName(), MSG_HOR_AUTO_ROLL);
                if( w != NULL && w->isVisible() )
                {
                    async(CMD_SERVICE_ACTIVE,0);
                }
                //set to roll
                async(MSG_HOR_TIME_MODE, Acquire_ROLL);
            }
            else if( getTimeMode() == Acquire_ROLL &&
                     scale < time_ms(200))
            {
                async(MSG_HOR_TIME_MODE, Acquire_YT);
                async(MSG_HORI_MAIN_SCALE, scale);
            }
        }
    }
    else if ( Control_Stop == action )
    {
        //bug 1386 by hxh
        EngineHoriInfo dummy;
        updateMemDep(dummy);

        ledRunStopSingle( false, true, false );
        stopTimer( sys_status_update_timer_id );
        setControlStatus( Control_Stoped );
    }
    else if ( Control_Single == action )
    {
        mUiAttr.setEnable(MSG_HOR_ACQ_MEM_DEPTH, false);
        ledRunStopSingle( false, false, true );

        //! timer
        startTimer( sys_status_update_timer_tmo,
                    sys_status_update_timer_id,
                    timer_repeat );

//        serviceExecutor::send( E_SERVICE_ID_DISPLAY, MSG_DISPLAY_CLEAR );
        //! clear wave
        postEngine( ENGINE_DISP_CLEAR, true );

        setControlStatus( Control_Runing );

        LOG_DBG();
    }
    else
    {
        //Q_ASSERT( false );
        qDebug() << "Invalid system status";
    }

    if ( action == Control_Stop )
    {
        if ( mAcquire.getTimemode() == Acquire_XY )
        {
            mUiAttr.setEnable( MSG_HORI_MAIN_SCALE, false );
        }
        else
        {
            mUiAttr.setEnable( MSG_HORI_MAIN_SCALE, true );
        }
    }
    else if( action == Control_Run)
    {
        mUiAttr.setEnable( MSG_HORI_MAIN_SCALE, true );
    }

    return postEngine( ENGINE_CONTROL_ACTION, (int)action );
}

//! run or stop
ControlAction servHori::getRunStop()
{
    if ( mControl.m_eAction != Control_Stop )
    { return Control_Run; }
    else
    { return Control_Stop; }
}

void servHori::cfgControlAction(int act)
{
    if( act == Control_Stop )
    {
        postEngine( ENGINE_CONTROL_ACTION, act );
    }
}

//! status change
void servHori::setControlStatus( ControlStatus stat )
{
    if ( stat != Control_Stoped )
    {
        defer( cmd_system_running );
    }
    else
    {
        //qDebug()<<__FILE__<<__LINE__<<"-- sys stat:"<<getRunStop();
        defer( cmd_system_stoped );
    }
}

ControlStatus servHori::getControlStatus()
{
    AcquireTimemode timeMode;

    timeMode = mAcquire.getTimemode();
    if ( timeMode == Acquire_YT )
    {
        return getControlYtStatus();
    }
    else if ( timeMode == Acquire_XY )
    {
        return getControlXyStatus();
    }
    else if ( timeMode == Acquire_ROLL )
    {
        return getControlRollStatus();
    }
    else
    {
        return getControlYtStatus();
    }
}

DsoErr servHori::setExpandMode( HorizontalExpandMode mode )
{
    setuiChange( MSG_HORIZONTAL_ACQNORM_EXPAND );

    m_eExpandMode = mode;
    return ERR_NONE;
}

HorizontalExpandMode servHori::getExpandMode()
{
    return m_eExpandMode;
}

//! [-500,500]
DsoErr servHori::setExpandUser( int user )
{
    DsoErr err;

    err = servDso::checkRange( "hori/user_expand_range",
                               user );

    m_userExpand = user;

    return err;
}

int servHori::getExpandUser()
{
    setuiZVal( 0 );

    return m_userExpand;
}

qlonglong servHori::getExpandGnd()
{
    switch( m_eExpandMode )
    {
        case Horizontal_Expand_Center:
            return 0;

        case Horizontal_Expand_LB:
            return -trace_width/2;

        case Horizontal_Expand_RB:
            return trace_width/2;

        case Horizontal_Expand_Trig:
            return - CHorizontal::getMainView().getOffset() * adc_hdiv_dots / CHorizontal::getMainView().getScale();

        case Horizontal_Expand_User:
            return m_userExpand;

        default:
            return 0;
    }
}

qlonglong servHori::getZoomExpandGnd()
{
    qlonglong expandTime;

    expandTime = getExpandGnd() * CHorizontal::getMainView().getScale() / adc_hdiv_dots
                 + CHorizontal::getMainView().getOffset();

    qlonglong expandGnd;
    expandGnd = trace_width/2 - (expandTime - getZoomOffset())*adc_hdiv_dots / getZoomScale();

    return expandGnd;
}

DsoErr servHori::setFine( bool bFine )
{
     m_bFine = bFine;
     return ERR_NONE;
}
bool servHori::getFine()
{
    return m_bFine;
}

DsoErr servHori::setAutoRoll( bool bAutoRoll )
{
     m_bAutoRoll = bAutoRoll;
     return ERR_NONE;
}
bool servHori::getAutoRoll()
{
    return m_bAutoRoll;
}

DsoErr servHori::setViewMode( HorizontalViewmode mode )
{
    if( mode == Horizontal_Main )
    {
        mUiAttr.setEnable( MSG_HORIZONTAL_ACQNORM_EXPAND, true );
        mUiAttr.setEnable( MSG_HORIZONTAL_ACQNORM_EXPAND, true );
    }
    else
    {
        mUiAttr.setEnable( MSG_HORIZONTAL_ACQNORM_EXPAND, false );
        mUiAttr.setEnable( MSG_HORIZONTAL_ACQNORM_EXPAND, false );
    }

    if( getTimeMode() != Acquire_ROLL )
    {
        //! enter zoom
        if ( mode == Horizontal_Zoom )
        {
            verifyZoom();
        }
        else
        {

        }

        CHorizontal::setViewMode( mode );
        //added by hxh for bug1705
        //! zoom模式下fft只能full，非zoom模式下可以是half，也可以是full
        if( Horizontal_Zoom == mode )
        {
            servMath::mScreenFFT = fft_screen_full;
        }
        else
        {
           // servMath::mScreenFFT = fft_screen_half;
        }
        return postEngine( ENGINE_HORI_VIEW_MODE, (int)mode );
    }
    else
    {
        if( mode == Horizontal_Zoom)
        {
            //DISABLE ZOOM WHEN ROLL by hxh
            return ERR_ACTION_DISABLED;
        }
        else
        {
            //! 允许在切换到滚动模式时发送消息关闭zoom
            CHorizontal::setViewMode( mode );
            //added by hxh for bug1705
            if( Horizontal_Zoom == mode )
            {
                servMath::mScreenFFT = fft_screen_half;
            }
            else
            {
                servMath::mScreenFFT = fft_screen_full;
            }
            return postEngine( ENGINE_HORI_VIEW_MODE, (int)mode );
        }
    }
}

HorizontalViewmode servHori::getViewMode()
{
    return CHorizontal::getViewMode();
}

//! acquire
DsoErr servHori::setAcquireMode( AcquireMode mode )
{
    mAcquire.setAcquireMode( mode );

    if(getTimeViewMode() == Acquire_YT || getTimeViewMode() == Acquire_SCAN )
    {
        id_async(getId(), MSG_HOR_TIME_MODE, Acquire_YT);
    }

    return postEngine( ENGINE_ACQUIRE_MODE, mode );
}

AcquireMode servHori::getAcquireMode()
{
    return mAcquire.getAcquireMode();
}

DsoErr servHori::setTimeMode( AcquireTimemode mode )
{
    bool bAvgEn;
    bool bZoomEn;
    //! avg enable
    if ( mode == Acquire_ROLL )
    {
        bAvgEn = false;
        bZoomEn = false;
        mAcquire.setChAcqDepIndex(Acquire_Depth_10M);
        postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_10M) );
    }
    else if ( mode == Acquire_XY )
    {
        bAvgEn = true;
        bZoomEn = false;
    }
    else
    {
        bAvgEn = true;
        bZoomEn = true;

        if ( getTimeViewMode() == Acquire_SCAN )
        {
            AcquireDepth depth = mAcquire.getChAcqDepIndex();
            if( depth == Acquire_Depth_1K ||
                depth == Acquire_Depth_10K ||
                depth == Acquire_Depth_100K  )
            {
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_1M);
            }
        }
    }

     mUiAttr.setEnable( MSG_HOR_ZOOM_ON, bZoomEn );

     //! conflict mode
    if ( mAcquire.getAcquireMode() == Acquire_Average &&
         !bAvgEn )
    {
        ssync( MSG_HOR_ACQ_MODE_YT, Acquire_Normal );
        servGui::showInfo( IDS_AVERAGE_OFF );
    }

    //! confilict zoom
    if ( !bZoomEn && getViewMode() == Horizontal_Zoom )
    {
        ssync( MSG_HOR_ZOOM_ON, false );

        servGui::showInfo( IDS_ZOOM_OFF );
    }

    mUiAttr.setVisible( MSG_HOR_ACQ_MODE_YT, Acquire_Average, bAvgEn );

    mAcquire.setTimeMode(mode);

    //! enter xy
    if ( mAcquire.m_eTimeMode == Acquire_XY )
    {
        //! vert config
        id_async( E_SERVICE_ID_CH2, servVert::user_cmd_on_off, true );
        id_async( E_SERVICE_ID_CH1, servVert::user_cmd_on_off, true );

        id_async( E_SERVICE_ID_CH3, servVert::user_cmd_on_off, false );
        id_async( E_SERVICE_ID_CH4, servVert::user_cmd_on_off, false );

        id_async( E_SERVICE_ID_LA,  servVert::user_cmd_on_off, false );

        //! acq
        id_async( getId(), MSG_HOR_ACQ_MODE_YT, 0 );
        id_async( getId(), MSG_HOR_ACQ_ANTI_ALIASING, false );

        id_async( getId(), MSG_HOR_FINE_ON, false );
        id_async( getId(), MSG_HOR_ZOOM_ON, false );

        //! pop info
        servGui::showInfo( IDS_X_Y );
    }
    postEngine( ENGINE_ACQUIRE_TIMEMODE, getTimeViewMode() );

    return ERR_NONE;
}

AcquireTimemode servHori::getTimeMode()
{
    return mAcquire.getTimemode();
}

AcquireTimemode servHori::getTimeViewMode()
{
    AcquireTimemode timeMode;

    timeMode = mAcquire.getTimemode();

    //! [dba+, 2018-04-02, bug:1862,2376 ]
    bool en        = false;
    bool maskEn    = false;
    bool zoneaEn   = false;
    bool zonebEn   = false;
    bool recordEn  = false;
    bool acquireEn = false;

    query(serv_name_mask,         MSG_MASK_ENABLE,        maskEn);
    query(serv_name_trigger_zone, MSG_TRIG_ZONE_A_ENABLE, zoneaEn);
    query(serv_name_trigger_zone, MSG_TRIG_ZONE_B_ENABLE, zonebEn);
    query(serv_name_wrec,         MSG_RECORD_ONOFF,       recordEn);

    if(getAcquireMode() == Acquire_Average)
    {
        acquireEn = true;
    }

    en = maskEn || zoneaEn || zonebEn || recordEn || acquireEn;

    if ( timeMode == Acquire_YT )
    {
        if ( CHorizontal::getMainView().getScale() >= scan_time_scale && (!en) )
        { return Acquire_SCAN; }
        else
        { return Acquire_YT; }
    }
    else
    { return timeMode; }
}

//! ref to the current
DsoErr servHori::setAverageCount( int count )
{
    DsoErr err;

    err = servDso::checkRange( "hori/average_range", count );

    //! align
    if ( err == ERR_NONE )
    {
        int alignCnt, normCount;
        alignCnt = ln2( count );
        normCount = 1<<alignCnt;

        if ( normCount != count )
        {
            count = normCount;
            err = ERR_AVERAGE_TRIM;
        }
        else
        {}
    }

    mAcquire.setAverageCount( count );

    postEngine( ENGINE_ACQUIRE_AVG_CNT, count );

    return err;
}
int servHori::getAverageCount()
{
    setuiAcc( key_acc::e_key_power_2 );
    setuiFmt( dso_view_format(fmt_int, fmt_width_1) );

    setuiRange( 2, 65536, 2 );

    return mAcquire.getAverageCount();
}

DsoErr servHori::setInterplate( AcquireInterplate interplate )
{
    return mAcquire.setInterplate( interplate );
}
AcquireInterplate servHori::getInterplate()
{
    return mAcquire.getInterplate();
}

DsoErr servHori::setAntiAliasing( bool bAnti )
{
    postEngine(ENGINE_ACQUIRE_ANTI_ALIASING, bAnti);
    return mAcquire.setAntiAliasing( bAnti );
}
bool servHori::getAntiAliasing()
{
    return mAcquire.getAntiAliasing();
}

DsoErr servHori::setMemDepth( AcquireDepth depth )
{
    mAcquire.setChAcqDepIndex(depth);

    if(mAcquire.getTimemode() != Acquire_ROLL )
    {
        return postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(depth) );
    }
    return ERR_NONE;
}
AcquireDepth servHori::getMemDepth()
{
//    bool en = false;
//    for ( int i = Acquire_Depth_Auto; i <= Acquire_Depth_500M; i++ )
//    {
//        bool e = mUiAttr.getOption(MSG_HOR_ACQ_MEM_DEPTH, i)->getEnable();
//        qDebug() << "Option:" << e;
//        if( e )
//        {
//            en = true;
//            break;
//        }
//        //mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, i , false);
//    }
//    mUiAttr.setEnable(MSG_HOR_ACQ_MEM_DEPTH,en);
//    qDebug() << "en1:" << getEnable() << mUiAttr.getItem(MSG_HOR_ACQ_MEM_DEPTH)->getEnable();
    return mAcquire.getChAcqDepIndex();
}

/**** scpi:get mem depth to int, by qxl 2.27 ******/
int servHori::getMemDepthVal()
{
    return mAcquire.getChLength().ceilInt();
}

qlonglong servHori::getChLength()
{
    return mAcquire.getChLength().ceilLonglong();
}
qlonglong servHori::getLaLength()
{
    return mAcquire.getLaLength().ceilLonglong();
}

qlonglong servHori::getChSaRate()
{
    return mAcquire.getChSaRate();
}

qlonglong servHori::getLaSaRate()
{
    return mAcquire.getLaSaRate();
}

DsoErr servHori::setXyFull( bool b )
{
    m_bFullXY = b;
    return ERR_NONE;
}
bool servHori::getXyFull()
{
    return m_bFullXY;
}

DsoErr servHori::setXyType( HorizontalXYType xyType )
{
    m_xyType = xyType;
    return ERR_NONE;
}
HorizontalXYType servHori::getXyType()
{
    return m_xyType;
}

void servHori::verifyZoom()
{
    //! align main scale
    qlonglong  alignScale = ll_roof125( CHorizontal::getMainView().getScale() );

    int seq = norm125ToSeq( alignScale );

    seq -= 1;
    if ( seq < 0 ){ seq = 0;}

    qlonglong aimScale = seqToNorm125( seq );

    //! verify scale
    aimScale = truncateScale( aimScale );

    //CHorizontal::getZoomView().setOffset( 0 );
    //CHorizontal::getZoomView().setScale( aimScale );

    //! config
    CHorizontal::getZoomView().setScale( aimScale );
    setZoomScale( aimScale );
}

void servHori::on_snap_state()
{
    postEngine( ENGINE_ACQUIRE_TEST );
}

void servHori::on_get_hori_attr( EngineHoriAttr *pAttr )
{
    queryEngine( qENGINE_HORI_ATTR, pAttr );
}

void servHori::on_cmd_hori_changed()
{
    setuiChange( MSG_HOR_ACQ_MEM_DEPTH );
    setuiChange( MSG_HOR_ACQ_SARATE );

    setuiChange( MSG_HOR_LA_SA_RATE );
    setuiChange( MSG_HOR_LA_MEM_DEPTH );
}

Chan servHori::getXyXSrc()
{
    return chan1;
}
Chan servHori::getXyYSrc()
{
    return chan2;
}

ControlStatus servHori::getControlXyStatus()
{
    //! running
    if ( mControl.getStatus() != Control_Stoped )
    {
       return Control_Runing;
    }
    //! stoped
    else
    {
        return Control_Stoped;
    }
}
ControlStatus servHori::getControlRollStatus()
{
    return getControlXyStatus();
}
ControlStatus servHori::getControlYtStatus()
{
    int iStat;
    if ( ERR_NONE != queryEngine( qENGINE_CONTROL_STATUS, iStat ) )
    {
        return Control_autoing;
    }

    //! user running
    if ( mControl.m_eAction == Control_Run )
    {
        //! temp stoped
        if ( iStat == Control_Stoped )
        {
            return Control_Runing;
        }
        if ( iStat == Control_Fpga_Force_Stopped )
        {
            //! 同步action和status
            ssync(MSG_HORIZONTAL_RUN, (int)Control_Stop);
            return Control_Stoped;
        }
        else
        { return (ControlStatus)iStat; }
    }
    else if ( mControl.m_eAction == Control_Stop )
    {
        return Control_Stoped;
    }
    else
    {
        return (ControlStatus)iStat;
    }
}

int servHori::toAcquireDepth( AcquireDepth acqIndex )
{
    for ( int i = 0; i < array_count(_acqIndex_Depth); i++ )
    {
        if ( acqIndex == _acqIndex_Depth[i].acqIndex )
        { return _acqIndex_Depth[i].depth; }
    }

    qWarning()<<"!!! mismatch acquire index";
    return _acqIndex_Depth[0].depth;
}

//! +system bw
//! +time mode
void servHori::getMinMaxScale(  qlonglong *pMinScale,
                                qlonglong *pMaxScale )
{
    Q_ASSERT( NULL != pMinScale );
    Q_ASSERT( NULL != pMaxScale );

    AcquireTimemode timeMode = mAcquire.getTimemode();

    if ( timeMode == Acquire_YT )
    {
        *pMinScale = sysGetMinHScale();
        *pMaxScale = time_s( 1000 );
    }
    else if ( timeMode == Acquire_ROLL )
    {
        if(getRunStop() == Control_Run)
        {
            if(m_bAutoRoll)
            {
                *pMinScale = sysGetMinHScale();
            }
            else
            {
                *pMinScale = time_ms(200);
            }
            *pMaxScale = time_s( 1000 );
        }
        else
        {
            *pMinScale = sysGetMinHScale();
            *pMaxScale = time_s( 1000 );
        }
    }
    else if ( timeMode == Acquire_XY )
    {
        *pMinScale = time_ns( 50 );
        *pMaxScale = time_ms( 100 );
    }
    else if ( timeMode == Acquire_SCAN )
    {
        *pMinScale = time_ms(200);
        *pMaxScale = time_s( 1000 );
    }
    else
    {
        Q_ASSERT( false );

        *pMinScale = time_ms(200);
        *pMaxScale = time_s( 1000 );
    }
}

qlonglong servHori::truncateScale( qlonglong scale )
{
    qlonglong minScale, maxScale;

    getMinMaxScale( &minScale, &maxScale );

    if ( scale < minScale )
    { return minScale; }
    else if ( scale > maxScale )
    { return maxScale; }
    else
    { return scale; }
}

void servHori::zoomOffsetRange( qlonglong &offsetmin,
                      qlonglong &offsetmax )
{
    qlonglong left, right;

    //! screen left && rigth time
    left = -hori_div*getMainScale()/2 + getMainOffset();
    right = hori_div*getMainScale()/2 + getMainOffset();

    qlonglong span = hori_div * getZoomScale();

    offsetmin = left + span/2;
    offsetmax = right - span/2;

    return;
}
