#include "../../include/dsostd.h"
#include "../../com/keyacc/ckeyacc.h"
#include "../../include/dsoarith.h"
#include "../service_msg.h"

#include "servhori.h"

#include "../servdso/servdso.h"
#include "../../engine/base/enginecfg.h"

#define HORI_KEY_ACC    ( key_acc::e_key_raw )

qlonglong timeScaleKnobProc( int knob, void *pContext )
{
    servHori *pHori;

    Q_ASSERT( NULL != pContext );
    pHori = (servHori*)pContext;

    int acc;
    qlonglong scaleNow;
    HorizontalViewmode viewMode;

    //! main or zoom
    viewMode = pHori->getViewMode();
    if ( viewMode == Horizontal_Main )
    {
        scaleNow = (pHori->getMainView()).getScale();
    }
    else
    {
        scaleNow = (pHori->getZoomView()).getScale();
    }

    //! knob proc
    if ( knob != 0 )
    {
        acc = key_acc::CKeyAcc::acc( HORI_KEY_ACC, knob );

        if ( pHori->getFine() )
        {
            scaleNow = servHori::fineTuneScale( scaleNow, acc );
            pHori->m_nLatestScale = scaleNow;

#if 0 //by hxh on 2018.1.26
            //! align scale 10ns
            if ( scaleNow < 10000 )
            {
                if ( knob > 0 )
                { scaleNow = ll_roof125( scaleNow ); }
                //! < 0
                //! floor
                else
                { scaleNow = ll_floor125(scaleNow); }
            }
#endif
        }
        else
        {
            /* slow down the adjust rate when more than 200ms. by hxh */
            if( scaleNow >= time_ms(200) )
            {
                if( acc > 0 )
                {
                    acc = 1;
                }
                else
                {
                    acc = -1;
                }
                scaleNow = servHori::coarseTuneScale( scaleNow, acc );
            }
            else
            {
                scaleNow = servHori::coarseTuneScale( scaleNow, acc );
            }
        }
        return scaleNow;
    }
    //! fine
    else
    {
        return 0;
    }
}

DsoErr servHori::setTimeScaleKnob( int /*knob*/ )
{
    return ERR_NONE;
}
float servHori::getTimeScaleReal()
{
    CKeyRequest keyReq;

    if ( m_eViewMode == Horizontal_Main )
    {
        keyReq.set( servHori::cmd_main_scale, this, timeScaleKnobProc );
        setuiKeyRequest( keyReq );

        return getMainScaleReal();
    }
    else
    {
        keyReq.set( servHori::cmd_zoom_scale, this, timeScaleKnobProc );
        setuiKeyRequest( keyReq );

        return getZoomScaleReal();
    }
}

DsoErr servHori::setMainScale( qint64 scale )
{   
    qlonglong expandGnd;
    expandGnd = getExpandGnd();

    return changeScale( expandGnd, scale );
}


DsoErr servHori::setPinchMainScale( CArgument arg )
{
    if(arg.size() != 2)
    {
        return ERR_INVALID_INPUT;
    }

    qint64 scale;
    qlonglong expandGnd;
    arg.getVal(scale, 0);
    arg.getVal(expandGnd, 1);

    return changeScale( expandGnd, scale );
}

DsoErr servHori::setScpiMainScale(qint64 scale)
{
    if( getViewMode() == Horizontal_Zoom)
    {
        return ERR_INVALID_CONFIG;
    }
    else
    {
        setMainScale(scale);
    }

    return ERR_NONE;
}

qint64 servHori::getMainScale()
{
    return CHorizontal::getMainView().getScale();
}

float servHori::getMainScaleReal()
{
    return realizeTime( CHorizontal::getMainView().getScale() );
}

qlonglong servHori::mainScaleExpand( qint64 preScale,
                                qint64 postScale,
                                qint64 gndExpand )
{
    qlonglong rawOffset, postOffset;

    rawOffset = CHorizontal::getMainView().getOffset()
            + gndExpand * preScale / adc_hdiv_dots;

    postOffset = rawOffset - gndExpand * postScale/adc_hdiv_dots;

    return postOffset;
}

DsoErr servHori::setZoomScale( qlonglong scale )
{
    DsoErr err1, err2;

    //! max-min
    qlonglong minScale, maxScale;
    getMinMaxScale( &minScale, &maxScale );

    err1 = checkRange( scale, minScale, getMainView().getScale() );

    //! align scale
    scale = servHori::alignScale( scale, Horizontal_Zoom);

    //! 调节zoomScale时如果zoomOffset超限会导致压缩倍数计算错误
    qlonglong zoomScaleBefore = CHorizontal::getZoomView().getScale();
    qlonglong zoomOffsetBefore = CHorizontal::getZoomView().getOffset();

    CHorizontal::getZoomView().setScale( scale );

    qlonglong left,right;
    zoomOffsetRange( left, right );
    qlonglong zoomOffset = CHorizontal::getZoomView().getOffset();
    checkRange(zoomOffset, left, right);
    CHorizontal::getZoomView().setOffset( zoomOffset );

    err2 = postEngine( ENGINE_ZOOM_SCALE, scale );

    if ( err2 != ERR_NONE )
    {
        CHorizontal::getZoomView().setScale(zoomScaleBefore);
        CHorizontal::getZoomView().setOffset(zoomOffsetBefore);
        return err2;
    }

    err2 = postEngine( ENGINE_ZOOM_OFFSET, zoomOffset);
    if ( err2 != ERR_NONE )
    {
        CHorizontal::getZoomView().setScale(zoomScaleBefore);
        CHorizontal::getZoomView().setOffset(zoomOffsetBefore);
        return err2;
    }

    setuiChange(MSG_HORI_ZOOM_OFFSET);
    return err1;
}

qlonglong servHori::getZoomScale()
{
    return CHorizontal::getZoomView().getScale();
}

float servHori::getZoomScaleReal()
{
    return realizeTime( CHorizontal::getZoomView().getScale() );
}

void servHori::getMainScaleRange( DsoRealGp *pGp )
{
    Q_ASSERT( NULL != pGp );

    qlonglong maxScale, minScale;

    getMinMaxScale( &minScale, &maxScale );

    pGp->setValue( m_mainView.getScale(),
                   maxScale,
                   minScale,
                   time_us(1),
                   E_N12 );
}
void servHori::getZoomScaleRange( DsoRealGp *pGp )
{
    Q_ASSERT( NULL != pGp );

    //! \todo by record scale
    qlonglong maxScale, minScale;

    getMinMaxScale( &minScale, &maxScale );

    EngineHoriInfo horiInfo;
    DsoErr err;

    err = queryEngine( qENGINE_HORI_INFO, &horiInfo );
    if ( err != ERR_NONE ) return;

    pGp->setValue( m_zoomView.getScale(),
                   m_mainView.getScale(),
                   horiInfo.mPlayScaleMin,
                   m_zoomView.getScale(),
                   E_N12 );
}

DsoErr servHori::changeScale( qlonglong expandGnd, qlonglong scale  )
{
    DsoErr err1, err2;
    qlonglong preScale;

    //! 在这边禁用会导致configEngine函数有问题。
    //! 所以使用按键和按钮禁用，scpi指令使用另外一条消息。
//    if( getViewMode() == Horizontal_Zoom)
//    {
//        return ERR_INVALID_CONFIG;
//    }

    //当开关打开后，取消慢扫描模式，自动进入滚动模式，类似Tek
    if( getTimeMode() == Acquire_YT )
    {
        if( scale >= time_ms(200)  )
        {
            if(m_bAutoRoll && getRunStop() == Control_Run)
            {                
                QWidget *w = sysGetView(this->getName(), MSG_HOR_AUTO_ROLL);
                if( w != NULL && w->isVisible() )
                {
                    async(CMD_SERVICE_ACTIVE,0);
                }

                //set to roll
                async(MSG_HOR_TIME_MODE, Acquire_ROLL);
                return ERR_NONE;
            }
            mUiAttr.setEnable(MSG_HOR_AUTO_ROLL, false);
            setuiChange(MSG_HOR_AUTO_ROLL);
        }
        else
        {
            mUiAttr.setEnable(MSG_HOR_AUTO_ROLL, true);
        }
    }
    else if( getTimeMode()  == Acquire_ROLL )
    {
        if( m_bAutoRoll &&  scale < time_ms(200) && getRunStop() == Control_Run )
        {
            async(MSG_HOR_TIME_MODE, Acquire_YT);
            async(MSG_HORI_MAIN_SCALE, scale);
            return ERR_NONE;
        }
    }

    //! -- pre info
    preScale = CHorizontal::getMainView().getScale();

    /* config when different */
    {
        //! -- scale config
        //! bw limit
        qlonglong minScale, maxScale;
        getMinMaxScale( &minScale, &maxScale );
        err1 = checkRange( scale, minScale, maxScale );

        //! align scale
        scale = servHori::alignScale( scale );

        //! 调整档位时有可能导致offset越界
        if(getMainOffset() < -5 * scale && (mControl.m_eAction == Control_Run))
        {
            setMainOffset( -5 * scale );
        }

        //! engine scale limit
        err2 = postEngine( ENGINE_MAIN_SCALE, scale );

        if ( err2 != ERR_NONE )
        {
            return err2;
        }


        CHorizontal::getMainView().setScale( scale );

        //! yt->scan, scan->YT
        postEngine( ENGINE_ACQUIRE_TIMEMODE, getTimeViewMode() );

        //! -- offset config
        qlonglong expandOffset;
        expandOffset = mainScaleExpand( preScale,
                                        scale,
                                        expandGnd );

        //! get info
        EngineHoriReq info;
        info.setInfo( scale, 0, scale, 0, 4 );
        err2 = queryEngine( qENGINE_HORI_INFO, &info );
        if ( err2 != ERR_NONE )
        {
            return err2;
        }

        //! limit the range
        if ( expandOffset < info.mOffMin )
        {
            m_mainView.setOffset( info.mOffMin );
        }
        else if ( expandOffset > info.mOffMax )
        {
            m_mainView.setOffset( info.mOffMax );
        }
        else
        {   m_mainView.setOffset( expandOffset); }

        m_bNeedAlignOffset = false;
        //! -- config
        if( getTimeMode()  == Acquire_ROLL )
        {
            ssync( MSG_HORI_MAIN_OFFSET, m_mainView.getRollOffset() );
        }
        else
        {
            ssync( MSG_HORI_MAIN_OFFSET, m_mainView.getOffset() );
        }
        m_bNeedAlignOffset = true;

        //for bug1401 by hxh
        if ( getTimeViewMode() == Acquire_SCAN )
        {
            AcquireDepth depth = mAcquire.getChAcqDepIndex();
            if( depth == Acquire_Depth_1K ||
                depth == Acquire_Depth_10K ||
                depth == Acquire_Depth_100K  )
            {
                async(MSG_HOR_ACQ_MEM_DEPTH, Acquire_Depth_1M);
            }
        }
    }

    //by hxh. remove limit hint
    err1 = ERR_NONE;
    return err1;
}
