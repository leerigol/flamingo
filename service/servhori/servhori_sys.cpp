
#include "../../include/dsostd.h"

#include "servhori.h"

#include "../servgui/servgui.h"
#include "../../engine/cplatform.h"

#include "../service_name.h"

#include "../servtrig/servtrig.h"

#include "../../baseclass/iserial.h"

DsoErr servHori::start()
{
    cplatform *plat = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != plat );

    //! hori info
    connect( plat->getRecEngine(),
             SIGNAL(sigHoriInfo(EngineHoriInfo)),
             this, SLOT(onSigHoriInfo(EngineHoriInfo)) );

    connect( plat->getAutoStopEngine(),
             SIGNAL(sigHoriInfo(EngineHoriInfo)),
             this, SLOT(onSigHoriInfo(EngineHoriInfo)) );

    connect( plat->getPlayEngine(),
             SIGNAL(sigHoriInfo(EngineHoriInfo)),
             this, SLOT(onSigHoriInfo(EngineHoriInfo)) );

    //! hori changed
    connect( plat->getRecEngine(),
             SIGNAL(sigHoriChanged(EngineHoriInfo)),
             this, SLOT(onHoriChanged(EngineHoriInfo)) );

    connect( plat->getAutoStopEngine(),
             SIGNAL(sigHoriChanged(EngineHoriInfo)),
             this, SLOT(onHoriChanged(EngineHoriInfo)) );

    connect( plat->getPlayEngine(),
             SIGNAL(sigHoriChanged(EngineHoriInfo)),
             this, SLOT(onHoriChanged(EngineHoriInfo)) );

    connect( plat->getRecEngine(),
             SIGNAL(sigHoriOffsetRoll(qlonglong)),
             this, SLOT(onChangeTimeOffset_Roll(qlonglong)) );

    //! clear trace
    connect( plat->getPlayEngine(),
             SIGNAL(sigClearTrace()),
             this, SLOT(onClearTrace()) );

    //! run stop
    connect( plat->getRecEngine(),
             SIGNAL(sigConRun(bool)),
             this, SLOT(onConRun(bool)) );

    connect( plat->getPlayEngine(),
             SIGNAL(sigConRun(bool)),
             this, SLOT(onConRun(bool)) );


    connect( plat->getPlayEngine(),
             SIGNAL(sigConfigFinished()),
             this,
             SLOT(onConfigFinish()));

    connect( plat->getAutoStopEngine(),
             SIGNAL(sigConfigFinished()),
             this,
             SLOT(onConfigFinish()));

    connect( plat->getRecEngine(),
             SIGNAL(sigConfigFinished()),
             this,
             SLOT(onConfigFinish()));

    return ERR_NONE;
}

bool servHori::keyTranslate(int key, int &msg, int &controlAction)
{
    //! running -- do not filter
    if ( getRunStop() == Control_Run )
    {
        return false;
    }

    if ( getTimeMode() == Acquire_XY )
    {
        return false;
    }

    bool bRet;

    //! stop
    bRet = serviceExecutor::keyTranslate( key, msg, controlAction );

    return bRet;
}

#undef ar
#undef ar_pack_e

#define ar ar_out
#define ar_pack_e ar_out
int servHori::serialOut( CStream &stream, unsigned char &ver )
{
    ISerial::serialOut(stream, ver );

    //! hori
    ar_pack_e( "action", mControl.m_eAction );
    ar_pack_e( "expand", m_eExpandMode );
    ar( "user_expand", m_userExpand );
    ar( "fine", m_bFine );

    ar_pack_e( "view", m_eViewMode );

    //for bug1735 by hxh
    qlonglong off = getMainOffset();
    ar( "main_offset", off );
    ar( "main_scale", m_mainView.m_s64Scale );

    ar( "zoom_offset", m_zoomView.m_s64Offset );
    ar( "zoom_scale", m_zoomView.m_s64Scale );

    //! acquire
    ar_pack_e( "acq_mode", mAcquire.m_eAcquireMode );
    ar_pack_e( "acq_time", mAcquire.m_eTimeMode );
    ar( "acq_avg", mAcquire.m_s32AverageCount );
    ar_pack_e( "acq_intx", mAcquire.m_eInterplate );

    ar( "acq_anti", mAcquire.m_bAntiAliasing );
    ar( "acq_dep", mAcquire.m_chAcqDepIndex );

    //! xy
    ar_pack_e( "xy", m_xyType );
    ar( "full_xy", m_bFullXY );

    ar( "autoRoll", m_bAutoRoll );

    return 0;
}

#undef ar
#undef ar_pack_e

#define ar ar_in
#define ar_pack_e ar_e_in
int servHori::serialIn( CStream &stream,
                        serialVersion ver )
{
    if( ver == ISerial::mVersion )
    {
        //! hori
        ar_pack_e( "action", mControl.m_eAction );
        ar_pack_e( "expand", m_eExpandMode );
        ar( "user_expand", m_userExpand );
        ar( "fine", m_bFine );

        ar_pack_e( "view", m_eViewMode );
        ar( "main_offset", m_mainView.m_s64Offset );
        ar( "main_scale", m_mainView.m_s64Scale );

        ar( "zoom_offset", m_zoomView.m_s64Offset );
        ar( "zoom_scale", m_zoomView.m_s64Scale );

        //! acquire
        ar_pack_e( "acq_mode", mAcquire.m_eAcquireMode );
        ar_pack_e( "acq_time", mAcquire.m_eTimeMode );
        ar( "acq_avg", mAcquire.m_s32AverageCount );
        ar_pack_e( "acq_intx", mAcquire.m_eInterplate );

        ar( "acq_anti", mAcquire.m_bAntiAliasing );
        ar_pack_e( "acq_dep", mAcquire.m_chAcqDepIndex );

        //! xy
        ar_pack_e( "xy", m_xyType );
        ar( "full_xy", m_bFullXY );

        ar( "autoRoll", m_bAutoRoll );
    }

    return 0;
}



void servHori::rst()
{
    //added by hxh. for restoring right status after Default
    resetMenuEnable();

    //! control
    mControl.setAction( Control_Run );

    //! horizontal
    m_eExpandMode = Horizontal_Expand_Center;
    m_userExpand = 0;
    m_bFine = false;
    m_bAutoRoll = false;

    m_eViewMode = Horizontal_Main;

    if ( sysHasArg("-ds8000") )
    {
        m_mainView.m_s64Offset = time_ps(500);
        m_mainView.m_s64Scale = time_ps(200);

        m_zoomView.m_s64Offset = 0;
        m_zoomView.m_s64Scale = time_ps(200);
    }
    else
    {
        m_mainView.m_s64Offset = 0;
        m_mainView.m_s64Scale = time_us(1);

        m_zoomView.m_s64Offset = 0;
        m_zoomView.m_s64Scale = time_us(1);
    }

    //! acquire
    mAcquire.setAcquireMode( Acquire_Normal );
    mAcquire.setTimeMode( Acquire_YT );
    mAcquire.setAverageCount( 2 );
    mAcquire.setInterplate( Acquire_Auto );
    mAcquire.setChAcqDepIndex(Acquire_Depth_Auto);

    mAcquire.setAntiAliasing( false );

    //! xy
    m_xyType = XY_CH12;
    m_bFullXY = true;

    //! session
    mYTSession.clear();
    mXYSession.clear();
    mRollSession.clear();
}

///
/// \for bug1295 and 1556 by hxh
///
void servHori::configEngine()
{
    setMemDepth( mAcquire.m_chAcqDepIndex );

    setMainScale( m_mainView.m_s64Scale );
    setMainOffset( m_mainView.m_s64Offset );

    qlonglong zoomScale = m_zoomView.m_s64Scale;
    ssync( MSG_HOR_ZOOM_ON, getViewMode() );

    ssync( MSG_HORI_ZOOM_SCALE, zoomScale );
    setZoomOffset( m_zoomView.m_s64Offset );    

    setTimeMode( mAcquire.m_eTimeMode );

    setAcquireMode( mAcquire.m_eAcquireMode );
    setAverageCount( mAcquire.m_s32AverageCount );

    updateAllUi();
}

int servHori::startup()
{   
    static bool inited = true;
    ControlAction tempAction = mControl.m_eAction;
    if(inited)
    {
        setControlAction( Control_Stop );
    }

    //qDebug()<<__FUNCTION__<<__LINE__<<getDebugTime();
    configEngine();
    //qDebug()<<__FUNCTION__<<__LINE__<<getDebugTime();
    //for bug3086 for setup loading.
    async(MSG_HOR_TIME_MODE, getTimeMode());
    //qDebug()<<__FUNCTION__<<__LINE__<<getDebugTime();
    //! unfreeze engine
    if(inited)
    {
        postEngine( ENGINE_UNFREEZE );
        inited = false;
    }
    setControlAction( tempAction );

    return 0;
}

void servHori::registerSpy()
{
    //! trig type
    spyOn( serv_name_trigger
           ,MSG_TRIGGER_SWEEP
           ,servHori::cmd_trig_sweep );

    //! license
    spyOn( serv_name_license,
           servLicense::cmd_Opt_Active,
           servHori::cmd_lic_changed );

    spyOn( serv_name_license,
           servLicense::cmd_Opt_Exp,
           servHori::cmd_lic_changed );

    spyOn( serv_name_license,
           servLicense::cmd_Opt_Invalid,
           servHori::cmd_lic_changed );

    spyOn( serv_name_mask,MSG_MASK_ENABLE);
    spyOn( serv_name_histo,MSG_HISTO_EN);
}

void servHori::onInit()
{
    //! option
    mOpt250En = false;
    mOpt500En = false;

    mUiAttr.setEnableVisible(MSG_HOR_ACQ_INTERLEAVE_ON, false,false);
    mUiAttr.setVisible(MSG_HOR_ACQ_MODE_YT, Acquire_HighResolution, false);
    mUiAttr.setVisible(MSG_HOR_ACQ_MODE_ROLL, Acquire_HighResolution, false);


    QList<qint64> stick;
//            stick.append(-4*scale );
//            stick.append(-3*scale );
//            stick.append(-2*scale );
//            stick.append(-scale );
    stick.append( 0 );
//            stick.append( scale );
//            stick.append( 2*scale );
//            stick.append( 3*scale );
//            stick.append( 4*scale );
    mUiAttr.setStick(MSG_HORI_MAIN_OFFSET, stick);
}

void servHori::onTimeOut( int id )
{
    if ( id == sys_status_update_timer_id )
    { onSysupdateTimeOut( id ); }
    else if ( id == navigator_timer_id )
    { onNavigatorTimeOut( id ); }
    else
    { stopTimer( id); }
}

void servHori::onSysupdateTimeOut( int tid )
{
    ControlStatus stat = getControlStatus();

    if ( stat == Control_Stoped )
    {
        stopTimer( tid );

        onStoped();

        setuiChange( MSG_HORIZONTAL_RUN );
    }
    else
    {}
}
void servHori::onNavigatorTimeOut( int /*tid*/ )
{
    qlonglong val = 0;

    if ( getViewMode() == Horizontal_Main )
    {
        mNavigator.setNow(getMainOffset());
        if ( mNavigator.step( val ) )
        {
            async( mNavigator.getMsg(), val );
            return ;
        }
    }
    else
    {
        qlonglong zoomLeft = 0, zoomRight = 0;
        mNavigator.setNow(getZoomOffset());
        zoomOffsetRange( zoomLeft, zoomRight );

        if ( mNavigator.step( val ) )
        {
            if( (zoomLeft<= val) && (val <= zoomRight) )
            {
                async( mNavigator.getMsg(), val );
                return ;
            }
        }
    }

    stopNavigator();
    return ;
}

void servHori::onStoped()
{
    //! led op
    ledRunStopSingle( false, true, false );

    //! action
    setControlAction( Control_Stop );

    //! sweep to auto
    //qDebug()<<__FILE__<<__LINE__<<"-- sys stat:"<<getRunStop();
    ssync( servHori::cmd_system_stoped, 0 );
}

void servHori::onClearSession( horiSession ses )
{
    switch( ses )
    {
        case hori_ses_all:
            mYTSession.clear();
            mXYSession.clear();
            mRollSession.clear();
            break;

        case hori_ses_yt:
            mYTSession.clear();
            break;

        case hori_ses_roll:
            mRollSession.clear();
            break;

        case hori_ses_xy:
            mXYSession.clear();
            break;

        default:
            break;
    }
}

DsoErr servHori::on_scpi_autoset()
{
    return serviceExecutor::post(serv_name_autoset, MSG_AUTO_TITLE, 1 );
}

DsoErr servHori::setScpiAverageCount(int count)
{
    //qDebug()<<__FILE__<<__LINE__<<count;
    async( MSG_HOR_ACQ_AVG_TIMES, count );
    return ERR_NONE;
}

int servHori::getScpiAverageCount()
{
    return getAverageCount();
}

void servHori::resetMenuEnable()
{
    mUiAttr.setVisible( MSG_HOR_ZOOM_ON, true );
    mUiAttr.setVisible( MSG_HOR_FINE_ON, true );

    mUiAttr.setEnable( MSG_HOR_ACQ_MODE_YT, true );
    mUiAttr.setEnable( MSG_HOR_ACQ_ANTI_ALIASING, true );

    mUiAttr.setEnable( MSG_HORIZONTAL_TIMEOFFSET, true );

    mUiAttr.setVisible( MSG_HORIZONTAL_NORM_EXPAND, true );
    mUiAttr.setVisible( MSG_HORIZONTAL_ACQNORM_EXPAND, true );

    mUiAttr.setVisible( MSG_HOR_XY_TYPE, true );
    mUiAttr.setVisible( MSG_HOR_XY_MODE_YT_DISP, true );

    mUiAttr.setVisible( MSG_HOR_LA_SA_RATE, true );
    mUiAttr.setVisible( MSG_HOR_LA_MEM_DEPTH, true );

    mUiAttr.setEnable( servHori::cmd_main_offset, true );
    mUiAttr.setEnable( servHori::cmd_zoom_offset, true );

    mUiAttr.setEnable( MSG_HORI_MAIN_SCALE, true );

    mUiAttr.setEnable(MSG_HOR_AUTO_ROLL, true);
}
