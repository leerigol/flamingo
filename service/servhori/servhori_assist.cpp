#include "../../include/dsostd.h"
#include "../../arith/norm125.h"

#include "servhori.h"
#include "../../engine/base/dsoengine.h"

//! 1ps ~ 1000s
#define MAX_SCALE_INDEX 45

#define MAX_AVERAGE_E   (16)    //! 2^16

#define LIMIT_STEP_SCALE time_ns(10)

/*!
 * \brief servHori::fineStep
 * \param scale
 * \param dir >0 up, < 0 down
 * \return
 */
qlonglong servHori::fineStep( qlonglong scale, int dir )
{
    //! roof, floor
    qlonglong floorScale, roofScale;
    floorScale = ll_floor125( scale );
    roofScale = ll_roof125( scale );

    //! int scale
    if ( floorScale == roofScale )
    {
        //! up
        if ( dir > 0 )
        {
            roofScale = ll_roof125( scale + 1 );
        }
        //! down
        else if ( dir < 0 )
        {
            //! roof not change
        }
        else
        {
            Q_ASSERT( false );
        }
    }
    else
    {
        //! roof, floor are correct
    }

    //!
    if ( roofScale < 100 )
    {
        //should not be here
        return 1;
    }
    else
    {
        if( roofScale <= LIMIT_STEP_SCALE )
        {
            return 100;
        }
        else
        {
            return roofScale / 100;
        }
    }
}

qlonglong servHori::fineTuneScale( qlonglong scale, int knob )
{
    qlonglong step;

    if ( knob > 0 )
    {
        while( knob > 0 )
        {
            step = fineStep( scale, 1 );
            scale += step;

            knob--;
        }

        return scale;
    }
    else if ( knob < 0 )
    {
        while( knob < 0 )
        {
            step = fineStep( scale, -1 );
            scale -= step;
            knob++;
        }

        return scale;
    }
    else
    {
        return scale;
    }
}

qlonglong servHori::coarseTuneScale( qlonglong scale, int knob )
{
    int scaleIndex;

    //! up
    if ( knob > 0 )
    {
        scaleIndex = norm125ToSeq( ll_floor125(scale) );

        scaleIndex += knob;
    }
    //! down
    else if ( knob < 0 )
    {
        scaleIndex = norm125ToSeq( ll_roof125(scale) );

        scaleIndex += knob;
    }
    else
    {
        return scale;
    }

    //! find scale
    if ( scaleIndex < 0 )
    {
        scaleIndex = 0;
    }
    else if ( scaleIndex > MAX_SCALE_INDEX  )
    {
        scaleIndex = MAX_SCALE_INDEX;
    }
    else
    {}

    return seqToNorm125( scaleIndex );
}

qlonglong servHori::alignScale( qlonglong scale, HorizontalViewmode /*mode*/ )
{
    qlonglong step;
    qlonglong roofScale, floorScale, alignScale;

    roof125( scale, roofScale );
    floor125( scale, floorScale );

    //! 100ps
    step = roofScale / adc_hdiv_dots;
    if ( step <  time_ps(100))  //! add by zx : for bug 1373
    {
        step = time_ps(100);
    }

    alignScale = floorScale + ((scale - floorScale)/step)*step;

    return alignScale;
}

qlonglong servHori::alignOffset( qlonglong offset,
                                 qlonglong refScale )
{
    qlonglong step = refScale / adc_hdiv_dots;
    qlonglong alignOffset;

    if ( step < 1 ) step = 1;
    alignOffset = ( offset / step ) * step;

    return alignOffset;
}

float servHori::realizeTime( qlonglong time )
{
    return ( (time)*(hori_time_base) );
}

int servHori::getAttrId()
{
    return dsoEngine::getSysConfigId();
}
