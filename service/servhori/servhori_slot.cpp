
#include "servhori.h"
#include "../servmath/servmath.h"

void servHori::onSigHoriInfo( EngineHoriInfo info )
{
    //! apply info
    mAcquire.setChLength( info.mChLength );
    mAcquire.setLaLength( info.mLaLength );

    mAcquire.setChSaRate( info.mChSa );
    mAcquire.setLaSaRate( info.mLaSa );

    setuiChange( cmd_main_scale );

    //! change
    defer( servHori::cmd_hori_changed );
}

void servHori::onHoriChanged( EngineHoriInfo info )
{
    //! update
    updateXxx( info );

    //! verify
    verifyXxx( info );
}

void servHori::onConRun( bool b )
{
    if ( b )
    {
        ledRunStopSingle( true, false, false );

        /*! [dba: 2018-04-24] bug: 2878*/
        mControl.m_eAction = Control_Run;
        setuiChange(MSG_HORIZONTAL_RUN);
    }
    else
    {
        async( MSG_HORIZONTAL_RUN, (int)Control_Stop );
    }
}

void servHori::onConfigFinish()
{
    m_bNeedEat = false;
    post(E_SERVICE_ID_TRACE,
         servTrace::cmd_trace_run,
         true);
}

void servHori::onClearTrace()
{
    //! ref有可能不需要清屏，所以不能直接发送clear
    for(int i = 0;i < 4;i++)
    {
        serviceExecutor::post(E_SERVICE_ID_MATH1 + i, servMath::cmd_on_clear,0);
    }
    serviceExecutor::post(E_SERVICE_ID_TRACE, servTrace::cmd_clear_trace, 0);
}
