#include "servhori.h"
#include "../servgui/servgui.h"

#define left_icon_pattern       ":/pictures/hori/left%1.bmp"
#define right_icon_pattern      ":/pictures/hori/right%1.bmp"
#define stop_icon_pattern       ":/pictures/hori/stop.bmp"
void servHori::onNavigatorLeft()
{
    int activeId = getActiveService();
    if( E_SERVICE_ID_SEARCH == activeId)
    {
        onSearchLeft();
    }
    else if(E_SERVICE_ID_RECORD == activeId)
    {
        onRecordLeft();
    }
    else
    { onHoriLeft(); }
}
void servHori::onNavigatorRight()
{
    int activeId = getActiveService();
    if( E_SERVICE_ID_SEARCH == activeId)
    {
        onSearchRight();
    }
    else if(E_SERVICE_ID_RECORD == activeId)
    {
        onRecordRight();
    }
    else
    { onHoriRight(); }
}
void servHori::onNavigatorStop()
{
    int activeId = getActiveService();
    if( E_SERVICE_ID_SEARCH == activeId)
    {
        onSearchStop();
    }
    else if(E_SERVICE_ID_RECORD == activeId)
    {
        onRecordStop();
    }
    else
    { onHoriStop(); }
}

void servHori::onHoriLeft()
{
    if (getTimeMode() == Acquire_XY)
    {
        servGui::showErr(ERR_ACTION_DISABLED);
        return;
    }

    if ( getRunStop() == Control_Run )
    {
        servGui::showErr(INF_NAVIGATION_HOR_STOP);
        return;
    }

    if ( mNavigator.getRun() )
    {
        if ( mNavigator.getStep() > 0 )
        { mNavigator.shiftSpeed(); }
        else
        { navigateView( 1 ); }
    }
    else
    {
        navigateView( 1 );

        startNavigator();
    }

    QString strImg = QString(left_icon_pattern).arg( mNavigator.getSpeed() );
    servGui::showImage( strImg );
}

void servHori::onHoriRight()
{
    if (getTimeMode() == Acquire_XY)
    {
        servGui::showErr(ERR_ACTION_DISABLED);
        return;
    }

    if ( getRunStop() == Control_Run )
    {
        servGui::showErr(INF_NAVIGATION_HOR_STOP);
        return;
    }

    if ( mNavigator.getRun() )
    {
        if ( mNavigator.getStep() < 0 )
        { mNavigator.shiftSpeed(); }
        else
        { navigateView( -1 ); }
    }
    else
    {
        navigateView( -1 );
        startNavigator();
    }

    QString strImg = QString(right_icon_pattern).arg( mNavigator.getSpeed() );
    servGui::showImage( strImg );
}

void servHori::onHoriStop()
{
    if (getTimeMode() == Acquire_XY)
    {
        servGui::showErr(ERR_ACTION_DISABLED);
        return;
    }

    if ( getRunStop() == Control_Run )
    {
        servGui::showErr(INF_NAVIGATION_HOR_STOP);
        return;
    }

    if ( mNavigator.getRun() )
    { servGui::showImage( stop_icon_pattern); }

    stopNavigator();
}

void servHori::onSearchLeft()
{
    post(E_SERVICE_ID_SEARCH, MSG_SEARCH_PLAY_PRE, 1);
}

void servHori::onSearchRight()
{
    post(E_SERVICE_ID_SEARCH, MSG_SEARCH_PLAY_NEXT, 1);
}

void servHori::onSearchStop()
{
    servGui::showErr(ERR_ACTION_DISABLED);
}

/*! ---------------- 录制导航需要禁用，这里需要用servName ---------------- */
void servHori::onRecordLeft()
{
    post(serv_name_wrec, MSG_REC_PLAY_BACK, 1);
}

void servHori::onRecordRight()
{
    post(serv_name_wrec, MSG_REC_PLAY_NEXT, 1);
}

void servHori::onRecordStop()
{
    post(serv_name_wrec, MSG_RECORD_PLAY, false);
}
/*! ---------------- -------------------------------- ---------------- */

void servHori::startNavigator()
{
    mNavigator.setRun( true );
    startTimer( navigator_timer_tmo,
                navigator_timer_id,
                timer_repeat );
}
void servHori::stopNavigator()
{
    mNavigator.setRun( false );
    stopTimer( navigator_timer_id );
}

//! dir : 1,-1
void servHori::navigateView( int dir )
{
    Q_ASSERT( dir == 1 || dir == -1 );

    //! main view
    if ( getViewMode() == Horizontal_Main )
    {
        mNavigator.setMsg( servHori::cmd_main_offset );
        mNavigator.setStep( dir * getMainView().getScale() / adc_hdiv_dots );
    }
    //! zoom view
    else
    {
        mNavigator.setMsg( servHori::cmd_zoom_offset );
        mNavigator.setStep( dir * getZoomView().getScale() / adc_hdiv_dots );
    }

    CSaFlow Attr;
    queryEngine( qENGINE_ACQ_CH_MAIN_SAFLOW, &Attr);

    qDebug()<<__FILE__<<__LINE__<<"t0   :"<<Attr.getT0().toLonglong()
                                <<"tEnd :"<<Attr.getTEnd().toLonglong();

    mNavigator.setRange( Attr.getTEnd().toLonglong() ,
                         Attr.getT0().toLonglong()   );
}
