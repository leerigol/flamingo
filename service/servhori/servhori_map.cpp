
#include "../../include/dsostd.h"

#include "../service_msg.h"

#include "servhori.h"

IMPLEMENT_CMD( serviceExecutor, servHori )
start_of_entry()

//! menu msg
on_set_int_int( MSG_HOR_TIME_MODE, &servHori::setTimeMode ),
on_get_int( MSG_HOR_TIME_MODE, &servHori::getTimeMode ),

on_get_int( MSG_HORI_TIME_VIEW_MODE, &servHori::getTimeViewMode ),

on_set_int_int( MSG_HOR_ACQ_MODE_YT, &servHori::setAcquireMode ),
on_get_int( MSG_HOR_ACQ_MODE_YT, &servHori::getAcquireMode ),

on_set_int_int( MSG_HOR_ACQ_MODE_ROLL, &servHori::setAcquireMode ),
on_get_int( MSG_HOR_ACQ_MODE_ROLL, &servHori::getAcquireMode ),

on_set_int_int( MSG_HORIZONTAL_RUN, &servHori::setControlAction ),
on_get_int( MSG_HORIZONTAL_RUN, &servHori::getRunStop ),

on_set_int_bool( MSG_HOR_ACQ_INTERLEAVE_ON, &servHori::setInterplate ),
on_get_bool( MSG_HOR_ACQ_INTERLEAVE_ON, &servHori::getInterplate ),

on_get_ll( MSG_HOR_ACQ_SARATE, &servHori::getChSaRate ),
on_get_ll( MSG_HOR_LA_SA_RATE, &servHori::getLaSaRate ),

on_set_int_int( MSG_HOR_ACQ_MEM_DEPTH, &servHori::setMemDepth ),
on_get_int( MSG_HOR_ACQ_MEM_DEPTH, &servHori::getMemDepth ),

on_get_ll( MSG_HOR_LA_MEM_DEPTH, &servHori::getLaLength ),

on_set_int_bool( MSG_HOR_ACQ_ANTI_ALIASING, &servHori::setAntiAliasing ),
on_get_bool( MSG_HOR_ACQ_ANTI_ALIASING, &servHori::getAntiAliasing ),

on_set_int_bool( MSG_HOR_ZOOM_ON, &servHori::setViewMode ),
on_get_bool( MSG_HOR_ZOOM_ON, &servHori::getViewMode ),

on_set_int_bool( MSG_HOR_FINE_ON, &servHori::setFine ),
on_get_bool( MSG_HOR_FINE_ON, &servHori::getFine ),

on_set_int_bool( MSG_HOR_AUTO_ROLL, &servHori::setAutoRoll ),
on_get_bool( MSG_HOR_AUTO_ROLL, &servHori::getAutoRoll ),

on_set_int_int( MSG_HORIZONTAL_TIMESCALE, &servHori::setTimeScaleKnob ),
on_get_float( MSG_HORIZONTAL_TIMESCALE, &servHori::getTimeScaleReal ),

on_set_int_int( MSG_HORIZONTAL_TIMEOFFSET, &servHori::setTimeOffsetKnob ),
on_get_float( MSG_HORIZONTAL_TIMEOFFSET, &servHori::getTimeOffsetReal ),

on_set_int_int( MSG_HOR_ACQ_AVG_TIMES, &servHori::setAverageCount ),
on_get_int( MSG_HOR_ACQ_AVG_TIMES, &servHori::getAverageCount ),

on_set_int_int( MSG_HOR_XY_TYPE, &servHori::setXyType ),
on_get_int( MSG_HOR_XY_TYPE, &servHori::getXyType ),

on_set_int_bool( MSG_HOR_XY_MODE_YT_DISP, &servHori::setXyFull ),
on_get_bool( MSG_HOR_XY_MODE_YT_DISP, &servHori::getXyFull ),

on_set_int_int( MSG_HORIZONTAL_ACQNORM_EXPAND, &servHori::setExpandMode),
on_get_int( MSG_HORIZONTAL_ACQNORM_EXPAND, &servHori::getExpandMode),

on_set_int_int( MSG_HORIZONTAL_ACQNORM_EXPAND_USER, &servHori::setExpandUser),
on_get_int( MSG_HORIZONTAL_ACQNORM_EXPAND_USER, &servHori::getExpandUser),

//! system msg
on_set_void_void( CMD_SERVICE_RST, &servHori::rst ),
on_set_int_void( CMD_SERVICE_STARTUP, &servHori::startup ),

on_set_void_int( CMD_SERVICE_TIMEOUT, &servHori::onTimeOut ),
on_set_void_void( CMD_SERVICE_INIT, &servHori::onInit ),

//! spy
on_set_void_void( servHori::cmd_trig_sweep,  &servHori::on_trig_sweep ),
on_set_void_void( servHori::cmd_lic_changed, &servHori::on_lic_changed ),
on_set_void_void( MSG_MASK_ENABLE,           &servHori::onMaskEnable),
on_set_void_void( MSG_HISTO_EN,              &servHori::onHistoEnable),

//! navigate
on_set_void_void( MSG_CHAN_PLAY_PRE, &servHori::onNavigatorLeft ),
on_set_void_void( MSG_CHAN_PLAY_NEXT, &servHori::onNavigatorRight ),
on_set_void_void( MSG_CHAN_PLAY_STOP, &servHori::onNavigatorStop ),

//! internal msg
on_set_int_ll( MSG_HORI_MAIN_SCALE, &servHori::setMainScale ),
on_get_ll( MSG_HORI_MAIN_SCALE, &servHori::getMainScale),
on_set_int_ll( servHori::cmd_scpi_main_scale, &servHori::setScpiMainScale ),
on_set_int_arg( servHori::cmd_pinch_main_scale, &servHori::setPinchMainScale ),

on_set_int_ll( MSG_HORI_MAIN_OFFSET, &servHori::setMainOffset ),
on_get_ll( MSG_HORI_MAIN_OFFSET, &servHori::getMainOffset),
on_set_int_ll( servHori::cmd_scpi_main_offset, &servHori::setScpiMainOffset ),

on_set_int_ll( MSG_HORI_ZOOM_SCALE, &servHori::setZoomScale ),
on_get_ll( MSG_HORI_ZOOM_SCALE, &servHori::getZoomScale),

on_set_int_ll( MSG_HORI_ZOOM_OFFSET, &servHori::setZoomOffset ),
on_get_ll( MSG_HORI_ZOOM_OFFSET, &servHori::getZoomOffset),

on_get_float( servHori::cmd_main_scale_real, &servHori::getMainScaleReal ),
on_get_float( servHori::cmd_zoom_scale_real, &servHori::getZoomScaleReal ),

on_get_float( servHori::cmd_main_offset_real, &servHori::getMainOffsetReal ),
on_get_float( servHori::cmd_zoom_offset_real, &servHori::getZoomOffsetReal ),

on_get_ll( servHori::cmd_ch_length, &servHori::getChLength ),
on_get_ll( servHori::cmd_la_length, &servHori::getLaLength ),

on_get_ll( servHori::cmd_ch_sa, &servHori::getChSaRate ),
on_get_ll( servHori::cmd_la_sa, &servHori::getLaSaRate ),

on_get_int     (servHori::cmd_control_status, &servHori::getControlStatus ),
on_set_void_int(servHori::cmd_control_status, &servHori::cfgControlAction),

on_get_int( servHori::cmd_xy_src_x, &servHori::getXyXSrc ),
on_get_int( servHori::cmd_xy_src_y, &servHori::getXyYSrc ),

on_get_void_ptr( servHori::cmd_hori_attr, &servHori::on_get_hori_attr ),
on_set_void_void( servHori::cmd_hori_changed, &servHori::on_cmd_hori_changed ),

on_set_void_void( servHori::cmd_system_running, &servHori::on_system_running ),
on_set_void_void( servHori::cmd_system_stoped, &servHori::on_system_stoped ),

on_set_void_int( servHori::cmd_clear_session, &servHori::onClearSession ),

//! test
on_set_void_void( servHori::cmd_snap_state, &servHori::on_snap_state ),
on_set_int_void( servHori::cmd_scpi_autoset, &servHori::on_scpi_autoset ),

on_set_void_void( servHori::cmd_freeze_engine, &servHori::freezeEngine ),
on_set_void_void( servHori::cmd_unfreeze_engine, &servHori::unfreezeEngine ),

on_set_void_void( servHori::cmd_apply_engine, &servHori::applyEngine ),
//scpi
on_set_int_int( servHori::cmd_scpi_ave_time, &servHori::setScpiAverageCount ),
on_get_int( servHori::cmd_scpi_ave_time, &servHori::getScpiAverageCount ),

on_get_int( servHori::cmd_scpi_mem_depth, &servHori::getMemDepthVal ),

end_of_entry()
