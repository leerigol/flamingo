
#include "servhori.h"
#include "../service_msg.h"
#include "../servgui/servgui.h"

void servHori::verifyXxx( EngineHoriInfo &info )
{
    verifyMemDep( info );
}

void servHori::verifyMemDep( EngineHoriInfo &info )
{
    AcquireDepth acqMemIndex, acqMemMax, acqMemMin;

    //! now
    acqMemIndex = mAcquire.getChAcqDepIndex();

    //! roll
    if ( Acquire_ROLL == mAcquire.getTimemode() )
    {
        acqMemMax = Acquire_Depth_10M;
        acqMemMin = Acquire_Depth_10M;
    }
    //! yt + xy
    else
    {
        //! averge
        if ( Acquire_Average == mAcquire.getAcquireMode() )
        {
            acqMemMax = max_averge_depth;
            acqMemMin = Acquire_Depth_Auto;
        }
        else
        {
            acqMemMin = Acquire_Depth_Auto;

            if ( info.mChCnt == 4 )
            {
                acqMemMax = Acquire_Depth_125M;
            }
            else if ( info.mChCnt == 2 )
            {
                if ( mOpt250En || mOpt500En )
                {
                    acqMemMax = Acquire_Depth_250M;
                }
                else
                {
                    acqMemMax = Acquire_Depth_125M;
                }
            }
            //! 500M contains 250M
            else if ( info.mChCnt == 1 )
            {
                if ( mOpt500En )
                {
                    acqMemMax = Acquire_Depth_500M;
                }
                else
                {
                    acqMemMax = Acquire_Depth_125M;
                }
            }
            else
            {
                LOG_DBG()<<info.mChCnt;
                Q_ASSERT(false);
            }
        }
    }

    //! verify
    if ( acqMemIndex > acqMemMax || acqMemIndex < acqMemMin )
    {
        if ( acqMemMin == Acquire_Depth_Auto )
        {
            //! temp change
            //servGui::showInfo( IDS_MEM_TO_AUTO );
        }
        else if ( acqMemMin == Acquire_Depth_1M )
        {
            servGui::showInfo( IDS_MEM_TO_1M );
        }
        else if ( acqMemMin == Acquire_Depth_25M )
        {
            servGui::showInfo( IDS_MEM_TO_25M );
        }
        else
        {}
    }
    else
    {}
}

