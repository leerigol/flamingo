#include "wavnavigator.h"
#include "../../service/service.h"
#include "../../service/service_name.h"

#define MIN_SPEED   1
#define MAX_SPEED   3

wavNavigator::wavNavigator()
{
    mMsg = 0;
    mSpeed = 1;
    mStep = 1;
    mNow = 0;
    mMax = 10;
    mMin = 0;

    mbRun = false;
}

void wavNavigator::setMsg( int msg )
{ mMsg = msg; }
int wavNavigator::getMsg()
{ return mMsg; }

void wavNavigator::setSpeed( int speed )
{
    if ( speed >= MIN_SPEED && speed <= MAX_SPEED )
    { mSpeed = speed; }
    else
    {}
}
void wavNavigator::shiftSpeed()
{
    if ( mSpeed >= MAX_SPEED )
    { setSpeed( MIN_SPEED ); }
    else
    { setSpeed( mSpeed + 1 ); }
}
int wavNavigator::getSpeed()
{ return mSpeed; }
int wavNavigator::getXSpeed()
{
    switch( mSpeed )
    {
    case 1: return 1;
    case 2: return 10;
    case 3: return 100;
    default: return 1;
    }
}

void wavNavigator::setStep( qlonglong step )
{
    Q_ASSERT( step != 0 );

    mStep = step;
}
qlonglong wavNavigator::getStep()
{ return mStep; }

void wavNavigator::setNow(qlonglong tNow)
{
    mNow = tNow;
}

void wavNavigator::setRange(qlonglong tMax, qlonglong tMin )
{
    Q_ASSERT( tMin <= tMax );

    mMax = tMax;
    mMin = tMin;
}
qlonglong wavNavigator::getMax()
{ return mMax; }
qlonglong wavNavigator::getMin()
{ return mMin; }

void wavNavigator::setRun( bool bRun )
{
    mbRun = bRun;

    if ( mbRun )
    {}
    //! stop
    else
    {
        mbRun = false;
        mSpeed = MIN_SPEED;
    }
}
bool wavNavigator::getRun()
{ return mbRun; }

bool wavNavigator::step( qlonglong &val )
{
    if ( !mbRun  )
    { return false; }

    //! step
    mNow = mNow + mStep * getXSpeed();

    //! check range
    if ( mNow >= mMax )
    {
        setRun( false );
        mNow = mMax;
    }
    else if ( mNow <= mMin )
    {
        setRun( false );
        mNow = mMin;
    }
    else
    { }

    val = mNow;
    return true;
}
