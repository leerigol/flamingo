#include "../../include/dsostd.h"
#include "../../com/keyacc/ckeyacc.h"

#include "../service_msg.h"

#include "servhori.h"


qlonglong timeOffsetKnobProc( int knob, void *pContext )
{
    Q_ASSERT( NULL != pContext );

    servHori *pHori;
    pHori = (servHori*)pContext;

    int inv;


    qlonglong scaleNow, offsetNow, step;
    HorizontalViewmode viewMode;

    //! main or zoom
    viewMode = pHori->getViewMode();
    if ( viewMode == Horizontal_Main )
    {
        scaleNow = pHori->getMainView().getScale();

        if(pHori->getTimeMode() == Acquire_ROLL)
        {
            offsetNow = pHori->getMainView().getRollOffset();;
        }
        else
        {
            offsetNow = pHori->getMainView().getOffset();
        }

        pHori->scanLau( servHori::cmd_main_offset,
                        offsetNow,
                        pHori->getId() );
        inv = 1;
    }
    else if ( viewMode == Horizontal_Zoom )
    {
        scaleNow = pHori->getZoomView().getScale();
        offsetNow = pHori->getZoomView().getOffset();

        pHori->scanLau( servHori::cmd_zoom_offset,
                        offsetNow,
                        pHori->getId() );
        inv = -1;
    }
    else
    {
        qWarning()<<"invalid input";
        return 0;
    }

    //! knob proc
    if ( knob != 0 )
    {
        int acc;
        acc = key_acc::CKeyAcc::acc( key_acc::e_key_square, knob );

        step = scaleNow / adc_hdiv_dots;

        offsetNow += step * acc * inv;
    }
    else
    {
        offsetNow = 0;
    }

    return offsetNow;
}

DsoErr servHori::setTimeOffsetKnob( int /*knob*/ )
{
    return ERR_NONE;
}
float servHori::getTimeOffsetReal()
{
    setuiZVal( 0ll );

    CKeyRequest keyReq;

    if ( m_eViewMode == Horizontal_Main )
    {
        keyReq.set( servHori::cmd_main_offset,
                    this,
                    timeOffsetKnobProc );
        setuiKeyRequest( keyReq );

        setuiZMsg( servHori::cmd_main_offset );
        return getMainOffsetReal();
    }
    else
    {
        keyReq.set( servHori::cmd_zoom_offset,
                    this,
                    timeOffsetKnobProc );
        setuiKeyRequest( keyReq );

        setuiZMsg( servHori::cmd_zoom_offset );
        return getZoomOffsetReal();
    }
}

DsoErr servHori::setMainOffset( qlonglong offset )
{
    EngineHoriInfo horiInfo;
    DsoErr err = ERR_NONE;

    if(mAcquire.getTimemode() == Acquire_ROLL)
    {
        if( mControl.m_eAction == Control_Stop )
        {
            if(m_bNeedAlignOffset)
            {
                //! align offset
                offset = servHori::alignOffset( offset, m_mainView.getScale() );
            }
            //! 回放下的范围限制放在引擎当中
            //err = checkRange( offset, - m_mainView.getScale() * 15, 0);
            CHorizontal::getMainView().setRollOffset( offset );

            err = postEngine( ENGINE_ROLL_OFFSET, offset );
        }
        else
        {
            CHorizontal::getMainView().setRollOffset( - m_mainView.getScale() * 5 );
            err = ERR_ACTION_DISABLED;
        }
    }
    else
    {
        if( mControl.m_eAction == Control_Run )
        {
            err = queryEngine( qENGINE_HORI_INFO, &horiInfo );
            if ( err != ERR_NONE ) return err;
            //! 根据存储深度限定偏移范围
            err = checkRange( offset, horiInfo.mOffMin, horiInfo.mOffMax );
        }

        if(m_bNeedAlignOffset)
        {
            //! align offset
            offset = servHori::alignOffset( offset, m_mainView.getScale() );
        }

        CHorizontal::getMainView().setOffset( offset );

        err = postEngine( ENGINE_MAIN_OFFSET, offset );
    }
    return err;
}

DsoErr servHori::setScpiMainOffset(qlonglong offset)
{
    if( getViewMode() == Horizontal_Zoom)
    {
        return ERR_INVALID_CONFIG;
    }
    else
    {
        async(MSG_HORI_MAIN_OFFSET,offset);
    }

    return ERR_NONE;
}

qlonglong servHori::getMainOffset()
{
    if(mAcquire.getTimemode() == Acquire_ROLL)
    {
        return CHorizontal::getMainView().getRollOffset();
    }
    else
    {
        return CHorizontal::getMainView().getOffset();
    }
}

float servHori::getMainOffsetReal()
{
    if(mAcquire.getTimemode() == Acquire_ROLL)
    {
        return realizeTime( CHorizontal::getMainView().getRollOffset() );
    }
    else
    {
        return realizeTime( CHorizontal::getMainView().getOffset() );
    }
}

DsoErr servHori::setZoomOffset( qlonglong offset )
{
    offset = servHori::alignOffset( offset, m_zoomView.getScale() );

    CHorizontal::getZoomView().setOffset( offset );

    return postEngine( ENGINE_ZOOM_OFFSET, offset );
}
qlonglong servHori::getZoomOffset()
{
    qlonglong left, right;

    zoomOffsetRange( left, right );

    mUiAttr.setRange( MSG_HORI_ZOOM_OFFSET, left, right, 0ll );

    return CHorizontal::getZoomView().getOffset();
}
float servHori::getZoomOffsetReal()
{
    return realizeTime( CHorizontal::getZoomView().getOffset() );
}

void servHori::getMainOffsetRange( DsoRealGp *pGp )
{
    Q_ASSERT( NULL != pGp );

    EngineHoriInfo horiInfo;
    DsoErr err;

    err = queryEngine( qENGINE_HORI_INFO, &horiInfo );
    if ( err != ERR_NONE ) return;

    pGp->setValue( m_mainView.getOffset(),
                   horiInfo.mOffMax,
                   horiInfo.mOffMin,
                   0,
                   E_N12 );
}
//! - zoom scale
//! \todo by zoom scale && record offset
void servHori::getZoomOffsetRange( DsoRealGp *pGp )
{
    Q_ASSERT( NULL != pGp );

    qlonglong tLeft, tRight;

    tLeft = m_mainView.getOffset() - hori_div / 2 * m_mainView.getScale();
    tRight = m_mainView.getOffset() + hori_div / 2 * m_mainView.getScale();

    qlonglong tZoomHalf;
    tZoomHalf = m_zoomView.getScale() * hori_div / 2;
    tLeft += tZoomHalf;
    tRight -= tZoomHalf;

    pGp->setValue( m_zoomView.getOffset(),
                   tRight,
                   tLeft,
                   time_ns(500),
                   E_N12 );
}
//! - roll offset
//! \todo by roll offset
void servHori::getRollOffsetRange(DsoRealGp *pGp)
{
    Q_ASSERT( NULL != pGp );

    EngineHoriInfo horiInfo;
    DsoErr err;

    err = queryEngine( qENGINE_HORI_INFO, &horiInfo );
    if ( err != ERR_NONE ) return;

    pGp->setValue( m_mainView.getRollOffset(),
                   horiInfo.mOffMax,
                   horiInfo.mOffMin,
                   0,
                   E_N12 );
}
