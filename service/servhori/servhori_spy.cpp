
#include "servhori.h"
#include "../service_name.h"

#include "../service_msg.h"

#include "../servdso/sysdso.h"
#include "../servgui/servgui.h"
void servHori::on_trig_sweep()
{
    int trigSweep;
    serviceExecutor::query( serv_name_trigger,
                            MSG_TRIGGER_SWEEP,
                            trigSweep );

    if ( trigSweep == Trigger_Sweep_Single )
    {
        ssync( MSG_HORIZONTAL_RUN, Control_Single );
    }
    else
    {}
}

void servHori::on_lic_changed()
{
    bool opt250En, opt500En;

    opt250En = sysCheckLicense( OPT_2RL );
    opt500En = sysCheckLicense( OPT_5RL );


    mOpt250En = opt250En;
    mOpt500En = opt500En;

    /* 250 enable if 500 is enable. by hxh */
    if( opt500En )
    {
        mOpt250En = true;
    }

    //! check depth
    async( MSG_HOR_ACQ_MEM_DEPTH, mAcquire.getChAcqDepIndex() );

    //add by hxh
    EngineHoriReq info;
    queryEngine( qENGINE_HORI_INFO, &info );
    updateXxx(info);
}

void servHori::onMaskEnable()
{
    bool en;
    query(serv_name_mask, MSG_MASK_ENABLE, en);

#if 0//! [dba+, 2018-04-02, bug:2633 ]
    //! 在慢扫描时基下无论是打开还是关闭mask，如果在满扫描时基下，都需要重新配置时基模式
    if(getMainScale() >= time_ms(200))
    {
        async( MSG_HORI_MAIN_SCALE, getMainScale() );
    }
#endif

    mUiAttr.setEnable(MSG_HOR_ZOOM_ON, !en);
}

void servHori::onHistoEnable()
{
#if 0
    bool en;
    query(serv_name_histo, MSG_HISTO_EN, en);
    //qDebug() << "en = " << en;
    mUiAttr.setEnable(MSG_HOR_TIME_MODE,Acquire_XY, !en);
    mUiAttr.setEnable(MSG_HOR_TIME_MODE,Acquire_ROLL, !en);
#endif
}
