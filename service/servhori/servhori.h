#ifndef SERVHORI_H
#define SERVHORI_H

#include "../service.h"

#include "../../baseclass/iserial.h"

#include "../../engine/module/ccontrol.h"
#include "../../engine/module/chorizontal.h"
#include "../../engine/module/cacquire.h"
#include "../../engine/base/enginecfg.h"
#include "../servtrace/servtrace.h"


#include "wavnavigator.h"

#define sys_status_update_timer_tmo   50    //! ms
#define sys_status_update_timer_id    1

#define navigator_timer_tmo           200    //! ms
#define navigator_timer_id            2

#define max_averge_depth    Acquire_Depth_25M

//! post do proc
class servHori;

class servHori : public serviceExecutor,
                 public ISerial,
                 public CHorizontal
{
    Q_OBJECT

    DECLARE_CMD()

    DECLARE_PRE_DO()
    DECLARE_POST_DO()

public:
    enum horiSession
    {
        hori_ses_all,
        hori_ses_yt,
        hori_ses_xy,
        hori_ses_roll,
    };

    enum horiMsg
    {
        cmd_main_scale = MSG_HORI_MAIN_SCALE,     /*!< qlonglong */
        cmd_zoom_scale = MSG_HORI_ZOOM_SCALE,     /*!< qlonglong */

        cmd_main_offset = MSG_HORI_MAIN_OFFSET,   /*!< qlonglong */
        cmd_zoom_offset = MSG_HORI_ZOOM_OFFSET,   /*!< qlonglong */

        cmd_none = 0,

        cmd_navigate_left,
        cmd_navigate_right,
        cmd_navigate_stop,
                            //! query
        cmd_main_scale_real,
        cmd_zoom_scale_real,
        cmd_main_offset_real,
        cmd_zoom_offset_real,

        cmd_ch_length,
        cmd_la_length,

        cmd_ch_sa,
        cmd_la_sa,

        cmd_control_status,
                            //! xy source
        cmd_xy_src_x,
        cmd_xy_src_y,

        cmd_snap_state,
                            //! update
        cmd_trig_sweep,

        cmd_hori_attr,

        cmd_hori_changed,   //! hori changed

        cmd_lic_changed,

        cmd_system_running,
        cmd_system_stoped,

        cmd_clear_session,  //! horiSession

        cmd_apply_engine,
        //!
        cmd_freeze_engine,
        cmd_unfreeze_engine,

        //scpi auto
        cmd_scpi_autoset,
        cmd_scpi_ave_time,
        cmd_scpi_mem_depth,

        cmd_scpi_main_scale,
        cmd_scpi_main_offset,

        cmd_pinch_main_scale,
    };

public:
    static qlonglong fineStep( qlonglong scale, int dir );

    static qlonglong fineTuneScale( qlonglong scale, int knob );
    static qlonglong coarseTuneScale( qlonglong scale, int knob );
    static qlonglong alignScale( qlonglong scale , HorizontalViewmode mode = Horizontal_Main);
    static qlonglong alignOffset( qlonglong offset,
                                  qlonglong refScale );

    static float realizeTime( qlonglong time );
    static int   getAttrId();

protected Q_SLOTS:
    void onSigHoriInfo( EngineHoriInfo );
    void onHoriChanged( EngineHoriInfo );
    void onChangeTimeOffset_Roll(qlonglong offset );
    void onConRun( bool );
    void onConfigFinish();
    void onClearTrace();

public:
    servHori( QString name, ServiceId eId = E_SERVICE_ID_HORI );

    virtual DsoErr start();
    virtual bool keyTranslate(int key, int &msg, int &controlAction);

public:
    virtual int serialOut( CStream &stream, serialVersion &ver );
    virtual int serialIn( CStream &stream, serialVersion ver );
    virtual void rst();
    virtual int startup();

protected:
    void onInit();

    void onTimeOut( int id );
    void onSysupdateTimeOut( int tid );
    void onNavigatorTimeOut( int tid );

    void configEngine();
    void resetMenuEnable();
    bool keyEat(int key, int count, bool bRelease);


    bool m_bNeedEat;
    ControlAction nTempAction;

public:
    void onStoped();

public:
    virtual void registerSpy();

public:
    //! control
    DsoErr setControlAction( ControlAction action );
    ControlAction getRunStop();

    void cfgControlAction( int act );
    void setControlStatus( ControlStatus stat );
    ControlStatus getControlStatus();

    //! horizontal
    DsoErr setExpandMode( HorizontalExpandMode mode );
    HorizontalExpandMode getExpandMode();

    DsoErr setExpandUser( int user );
    int getExpandUser();
    qlonglong getExpandGnd();
    qlonglong getZoomExpandGnd();

    DsoErr setFine( bool bFine );
    bool getFine();

    DsoErr setAutoRoll( bool bAutoRoll );
    bool   getAutoRoll();

    DsoErr setViewMode( HorizontalViewmode mode );
    HorizontalViewmode getViewMode();

    DsoErr setTimeScaleKnob( int knob );
    float getTimeScaleReal();

    DsoErr setTimeOffsetKnob( int knob );
    float getTimeOffsetReal();

    DsoErr setMainScale( qint64 scale );
    DsoErr setScpiMainScale( qint64 scale );
    DsoErr setPinchMainScale( CArgument arg );
    qint64 getMainScale();
    float getMainScaleReal();

    qlonglong mainScaleExpand( qint64 preScale,
                          qint64 postScale,
                          qint64 expand );

    DsoErr setMainOffset( qlonglong offset );
    DsoErr setScpiMainOffset( qlonglong offset );
    qlonglong getMainOffset();
    float getMainOffsetReal();

    DsoErr setZoomScale( qlonglong scale );
    qlonglong getZoomScale();
    float getZoomScaleReal();

    DsoErr setZoomOffset( qlonglong offset );
    qlonglong getZoomOffset();
    float getZoomOffsetReal();

    //! group value
    void getMainScaleRange( DsoRealGp *pGp );
    void getZoomScaleRange( DsoRealGp *pGp );

    void getMainOffsetRange( DsoRealGp *pGp );
    void getZoomOffsetRange( DsoRealGp *pGp );

    void getRollOffsetRange( DsoRealGp *pGp );

    //! acq
    DsoErr setAcquireMode( AcquireMode mode );
    AcquireMode getAcquireMode();

    DsoErr setTimeMode( AcquireTimemode mode );
    AcquireTimemode getTimeMode();

    AcquireTimemode getTimeViewMode();

    DsoErr setAverageCount( int count );
    int getAverageCount();

    DsoErr setInterplate( AcquireInterplate interplate );
    AcquireInterplate getInterplate();

    DsoErr setAntiAliasing( bool bAnti );
    bool getAntiAliasing();

    DsoErr setMemDepth( AcquireDepth depth );
    AcquireDepth getMemDepth();
    int     getMemDepthVal();

    qlonglong getChLength();
    qlonglong getLaLength();

    qlonglong getChSaRate();
    qlonglong getLaSaRate();

    //! xy
    DsoErr setXyFull( bool b );
    bool   getXyFull();

    DsoErr setXyType(HorizontalXYType xyType );
    HorizontalXYType getXyType();

    //! navigator
    void onNavigatorLeft();
    void onNavigatorRight();
    void onNavigatorStop();

    void onHoriLeft();
    void onHoriRight();
    void onHoriStop();

    void onSearchLeft();
    void onSearchRight();
    void onSearchStop();

    void onRecordLeft();
    void onRecordRight();
    void onRecordStop();

    void startNavigator();
    void stopNavigator();

    void navigateView( int dir );

    //! session
    void onClearSession( horiSession ses );

    //scpi auto
    DsoErr on_scpi_autoset();

    DsoErr setScpiAverageCount( int count );
    int    getScpiAverageCount();

protected:
    void verifyZoom();

protected:
    void on_trig_sweep();
    void on_lic_changed();
    void onMaskEnable();
    void onHistoEnable();

protected:
    void on_cmd_zoom_status_update();
    void on_snap_state();

    void on_get_hori_attr( EngineHoriAttr *pAttr );
    void on_cmd_hori_changed();

    void on_system_running();
    void on_system_stoped();

    void freezeEngine();
    void unfreezeEngine();
    void applyEngine();
public:
    void on_pre_time_mode( CArgument &arg );

public:
    void on_post_time_mode();
    void on_post_run_stop();

    void on_post_acq_mode();

    void on_post_startup();

    qint64 m_nLatestScale;

protected:
    Chan getXyXSrc();
    Chan getXyYSrc();

    ControlStatus getControlXyStatus();
    ControlStatus getControlRollStatus();
    ControlStatus getControlYtStatus();

protected:
    void updateXxx( EngineHoriInfo &info );
    void updateMemDep( EngineHoriInfo &info );

    void updateMemDep_YT( EngineHoriInfo &info );
    void updateMemDep_XY( EngineHoriInfo &info );
    void updateMemDep_Roll( EngineHoriInfo &info );

    void ledRunStopSingle( bool bRun, bool bStop, bool bSingle );
protected:
    void verifyXxx( EngineHoriInfo &info );
    void verifyMemDep( EngineHoriInfo &info );

protected:
    int toAcquireDepth( AcquireDepth acqIndex );

    void getMinMaxScale( qlonglong *pMinScale,
                         qlonglong *pMaxScale );

    qlonglong truncateScale( qlonglong scale );
    void zoomOffsetRange( qlonglong &offsetmin,
                          qlonglong &offsetmax );
private:
    DsoErr changeScale(qlonglong expandGnd, qlonglong scale);

protected:
    //! control
    CControl mControl;

    HorizontalExpandMode m_eExpandMode;
    int m_userExpand;
    bool m_bFine;
    bool m_bNeedAlignOffset;

    CAcquire mAcquire;

    //! xy
    bool m_bFullXY;
    HorizontalXYType m_xyType;

    //! license
    bool mOpt250En, mOpt500En;

    //! navigator
    wavNavigator mNavigator;

    //! sessions
    QByteArray mYTSession, mXYSession, mRollSession;

private:
    //set to ROLL when 200ms
    bool m_bAutoRoll;    
};

#endif // SERVHORI_H

