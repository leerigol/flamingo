
#include "../service_msg.h"

#include "servhori.h"

IMPLEMENT_POST_DO( serviceExecutor, servHori)
start_of_postdo()
on_postdo( MSG_HOR_TIME_MODE, &servHori::on_post_time_mode ),

//on_postdo( MSG_HOR_ZOOM_ON, &servHori::on_post_time_mode ),

on_postdo( MSG_HORIZONTAL_RUN, &servHori::on_post_run_stop ),

on_postdo( MSG_HOR_ACQ_MODE_YT, &servHori::on_post_acq_mode ),
on_postdo( MSG_HOR_ACQ_MODE_ROLL, &servHori::on_post_acq_mode ),

on_postdo( CMD_SERVICE_STARTUP, &servHori::on_post_startup ),
end_of_postdo()

void servHori::on_post_time_mode()
{
    AcquireTimemode timeMode;

    timeMode = mAcquire.getTimemode();

    //! scale offset changed
    if ( timeMode == Acquire_YT )
    {}
    else if ( timeMode == Acquire_XY )
    {
    }
    else if ( timeMode == Acquire_ROLL )
    {
        id_async( getId(), MSG_HORI_MAIN_SCALE, time_ms(200) );
    }
    else
    {}

    defer( EVT_TIME_MODE_CHANGED );

    stopNavigator();
}

void servHori::on_post_run_stop()
{
    stopNavigator();
    if( mControl.m_eAction == Control_Stop )
    {
        EngineHoriInfo noused;
        updateMemDep_Roll(noused);
    }
    else
    {
        if(  mAcquire.getTimemode() != Acquire_ROLL )
        {
            mUiAttr.setEnable( MSG_HOR_ACQ_MEM_DEPTH, true);
        }
    }
}

void servHori::on_post_acq_mode()
{
    AcquireMode acqMode;

    acqMode = mAcquire.getAcquireMode();

    //! average mode
    if ( acqMode == Acquire_Average )
    {
        //! now
        AcquireDepth acqMemIndex = mAcquire.getChAcqDepIndex();
        if( acqMemIndex > Acquire_Depth_25M )
        {
            mAcquire.setChAcqDepIndex( Acquire_Depth_25M );
            postEngine( ENGINE_ACQUIRE_DEPTH, toAcquireDepth(Acquire_Depth_25M) );
            setuiChange( MSG_HOR_ACQ_MEM_DEPTH );
        }
    }
    else
    {

    }
}

void servHori::on_post_startup()
{
    //! hori changed
    defer( servHori::cmd_hori_changed );
    defer( EVT_TIME_MODE_CHANGED );
}

//
/// \brief Disabled the key when configing mode
/// \param key
/// \return
///
bool servHori::keyEat(int /*key*/, int /*count*/, bool /*bRelease*/ )
{
    if ( !isActive() )
    {
        return false;
    }
    if(m_bNeedEat)
    {
        //qDebug() << "Config ing";
        //servGui::showErr( ERR_ACTION_DISABLED );
        return true;
    }

    return false;
}
