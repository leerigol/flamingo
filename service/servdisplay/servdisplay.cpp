#include "servdisplay.h"
#include "../service_name.h"

#include "../servch/servch.h"
#include "../service_msg.h"


IMPLEMENT_CMD( serviceExecutor, servDisplay  )

start_of_entry()

on_set_int_bool(        MSG_DISPLAY_TYPE,              &servDisplay::setWaveType ),
on_get_bool    (        MSG_DISPLAY_TYPE,              &servDisplay::getWaveType ),

on_set_int_int (        MSG_DISPLAY_PERSISTIME,        &servDisplay::setWavePersis ),
on_get_int     (        MSG_DISPLAY_PERSISTIME,        &servDisplay::getWavePersis ),

on_set_int_int (        MSG_DISPLAY_GRID,              &servDisplay::setWaveGrid ),
on_get_int     (        MSG_DISPLAY_GRID,              &servDisplay::getWaveGrid ),

on_set_int_int (        MSG_DISPLAY_WAVE_INTENSITY,    &servDisplay::setWaveIntensity ),
on_get_int     (        MSG_DISPLAY_WAVE_INTENSITY,    &servDisplay::getWaveIntensity ),

on_set_int_int (        MSG_DISPLAY_GRID_INTENSITY,    &servDisplay::setGridBrightness ),
on_get_int     (        MSG_DISPLAY_GRID_INTENSITY,    &servDisplay::getGridBrightness ),

on_set_int_bool(        MSG_DISPLAY_RULERS,            &servDisplay::setShowAxis),
on_get_bool    (        MSG_DISPLAY_RULERS,            &servDisplay::getShowAxis),

on_set_int_bool(        MSG_DISPLAY_10X8_10X10,        &servDisplay::set10x8_10x10),
on_get_bool    (        MSG_DISPLAY_10X8_10X10,        &servDisplay::get10x8_10x10),

on_set_int_bool(        MSG_DISPLAY_PALETTE,           &servDisplay::setPalette),
on_get_bool    (        MSG_DISPLAY_PALETTE,           &servDisplay::getPalette),

on_set_int_bool(        MSG_DISPLAY_FREEZE,            &servDisplay::setFreeze),
on_get_bool    (        MSG_DISPLAY_FREEZE,            &servDisplay::getFreeze),

on_set_int_void(        MSG_DISPLAY_CLEAR,             &servDisplay::clear),

on_set_void_void(       MSG_HOR_TIME_MODE,             &servDisplay::onTimeMode),
on_set_void_void(       On_Trigger_Changed,            &servDisplay::onTriggerChanged ),
on_set_void_void(       On_Chan1_Changed,              &servDisplay::onChan1),
on_set_void_void(       On_Chan2_Changed,              &servDisplay::onChan2),
on_set_void_void(       On_Chan3_Changed,              &servDisplay::onChan3),
on_set_void_void(       On_Chan4_Changed,              &servDisplay::onChan4),

on_set_void_void(       CMD_SERVICE_RST,               &servDisplay::rst ),
on_set_int_void (       CMD_SERVICE_STARTUP,           &servDisplay::startup ),

//on_set_int_int  (       CMD_SERVICE_ACTIVE,            &servDisplay::setActive ),

end_of_entry()

servDisplay::servDisplay( QString name, ServiceId id ) : serviceExecutor( name, id )
{
    serviceExecutor::baseInit();

    ISerial::mVersion = 0xe;
}

int servDisplay::serialOut( CStream &stream ,unsigned char &ver)
{
    ver = mVersion;

    //stream.write(QStringLiteral("m_nbakFormat"), m_nForceFormat);
    stream.write(QStringLiteral("format"),       m_nFormat);

    stream.write(QStringLiteral("grid"),         m_nGrid);
    stream.write(QStringLiteral("persis"),       m_nPersis);
    stream.write(QStringLiteral("intensity"),    m_nIntensity);
    stream.write(QStringLiteral("brightness"),   m_nBrightness);
    stream.write(QStringLiteral("axis"),     m_bShowAxis);
    stream.write(QStringLiteral("b10x8_10x10"),   m_b10x8_10x10);
    stream.write(QStringLiteral("palette"),      m_bPalette);
    stream.write(QStringLiteral("freeze"),       m_bFreeze);
    return 0;
}
int servDisplay::serialIn( CStream &stream, unsigned char ver )
{
    if(ver == mVersion)
    {
        int param;

        //stream.read(QStringLiteral("m_nbakFormat"),       param);
        //m_nForceFormat = (EWaveFormat)param;
        stream.read(QStringLiteral("format"),       param);
        m_nFormat = (EWaveFormat)param;

        stream.read(QStringLiteral("grid"),         param);
        m_nGrid   = (EWaveGrids)param;
        stream.read(QStringLiteral("persis"),       param);
        m_nPersis = (EWavePersis)param;
        stream.read(QStringLiteral("intensity"),    m_nIntensity);
        stream.read(QStringLiteral("brightness"),   m_nBrightness);
        stream.read(QStringLiteral("axis"),     m_bShowAxis);
        stream.read(QStringLiteral("b10x8_10x10"),   m_b10x8_10x10);
        stream.read(QStringLiteral("palette"),      m_bPalette);
        stream.read(QStringLiteral("freeze"),       m_bFreeze);
    }
    else
    {
        rst();
    }
    return 0;
}
void servDisplay::rst()
{
    m_nFormat       =   FORMAT_IS_VECTOR;
    m_nForceFormat  =   m_nFormat;

    m_nGrid         =   GRID_IS_FULL;
    m_nIntensity    =   40;
    m_nBrightness   =   20;
    m_nPersis       =   PERSIS_IS_NONE;
    m_bShowAxis     =   false;

    m_b10x8_10x10   =   true;
    m_bPalette      =   false;
    m_bFreeze       =   true;

    mUiAttr.setEnable(MSG_DISPLAY_GRID, GRID_IS_IRE, false);


}
int servDisplay::startup()
{   
    onVideoListener();

    setWaveFormat( (EWaveFormat)m_nFormat);

    setWavePersis(m_nPersis);

    setWaveIntensity(m_nIntensity);
    setGridBrightness(m_nBrightness);
    setShowAxis(m_bShowAxis);
    set10x8_10x10(m_b10x8_10x10);
    setPalette(m_bPalette);

    onTimeMode();
    setuiChange(MSG_DISPLAY_GRID);    
    updateAllUi();
    return 0;
}

void servDisplay::registerSpy()
{
    spyOn( serv_name_ch1, MSG_CHAN_SCALE,  On_Chan1_Changed );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE,  On_Chan2_Changed );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE,  On_Chan3_Changed );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE,  On_Chan4_Changed );

    spyOn( serv_name_ch1, MSG_CHAN_SCALE_VALUE,  On_Chan1_Changed );
    spyOn( serv_name_ch2, MSG_CHAN_SCALE_VALUE,  On_Chan2_Changed );
    spyOn( serv_name_ch3, MSG_CHAN_SCALE_VALUE,  On_Chan3_Changed );
    spyOn( serv_name_ch4, MSG_CHAN_SCALE_VALUE,  On_Chan4_Changed );

    spyOn( serv_name_trigger_edge,      MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_pulse,     MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_slope,     MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_video,     MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_video,     MSG_TRIGGER_VIDEO_STANDARD,      On_Trigger_Changed );

    spyOn( serv_name_trigger_pattern,   MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_duration,  MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_runt,      MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_window,    MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_timeout,   MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_nth,       MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_delay,     MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_setup,     MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_rs232,     MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_i2c,       MSG_TRIGGER_TYPE,       On_Trigger_Changed );
    spyOn( serv_name_trigger_spi,       MSG_TRIGGER_TYPE,       On_Trigger_Changed );

    spyOn( serv_name_hori,MSG_HOR_TIME_MODE);
}

void servDisplay::onTriggerChanged(void)
{
    onVideoListener();
}

void servDisplay::onChan1(void)
{
    onChanChanged(serv_name_ch1);
}

void servDisplay::onChan2(void)
{
    onChanChanged(serv_name_ch2);
}

void servDisplay::onChan3(void)
{
    onChanChanged(serv_name_ch3);
}

void servDisplay::onChan4(void)
{
    onChanChanged(serv_name_ch4);
}

/*
void servDisplay::onPersistimeenable(void)
{
    qDebug() << "helloworld  ..";
}
*/
void servDisplay::onChanChanged(QString name)
{
    onVideoListener(name);
}

void servDisplay::onTimeMode()
{
    query(serv_name_hori,MSG_HOR_TIME_MODE, m_nTimemode);

    if( m_nTimemode == Acquire_XY )
    {
        configWaveFormat( FORMAT_IS_DOTS );

        mUiAttr.setEnable(MSG_DISPLAY_TYPE, false);
        mUiAttr.setEnable(MSG_DISPLAY_RULERS, false);
        setuiChange( MSG_DISPLAY_TYPE );
    }
    else
    {
        mUiAttr.setEnable(MSG_DISPLAY_TYPE, true);
        mUiAttr.setEnable(MSG_DISPLAY_RULERS, true);
        //if( m_nForceFormat != m_nFormat )
        {
            setWaveFormat((EWaveFormat)m_nFormat);
            setuiChange( MSG_DISPLAY_TYPE );
        }
    }

    if(m_nTimemode  == Acquire_ROLL)
    {
        async(MSG_DISPLAY_PERSISTIME,PERSIS_IS_NONE);
        mUiAttr.setEnable( MSG_DISPLAY_PERSISTIME,false);
    }
    else
    {
        mUiAttr.setEnable( MSG_DISPLAY_PERSISTIME,true);
    }
}

void servDisplay::onVideoListener(QString name)
{
    bool bVideoScale = false;
    int  nTriggerType= 0;
    int  nVideoStandard = 0;
    int  IREScale = 140000;

    int   nScale;
    float scale;
    if(name.size() == 0)
    {
        //! only analog channel
        for(int val = chan1 ; val <= chan4; val++ )
        {           
            name = servCH::getServiceName((ServiceId)(E_SERVICE_ID_CH1 + val - 1));

            serviceExecutor::query( name, servCH::cmd_scale_real,  scale);
            nScale =  scale/ch_volt_base;
            if( nScale == IREScale )
            {
                bVideoScale = true;
                break;
            }
        }
    }
    else
    {
        serviceExecutor::query( name, servCH::cmd_scale_real,  scale);
        nScale =  scale/ch_volt_base;
        bVideoScale = (nScale == IREScale);
    }

    serviceExecutor::query( serv_name_trigger,
                            MSG_TRIGGER_TYPE,
                            nTriggerType);

    serviceExecutor::query( serv_name_trigger_video,
                            MSG_TRIGGER_VIDEO_STANDARD,
                            nVideoStandard);

    if(bVideoScale && nTriggerType == Trigger_Video )//&& nVideoStandard ==  Video_Stardard_NTSC)
    {
        mUiAttr.setEnable(MSG_DISPLAY_GRID, GRID_IS_IRE, true);
        async( MSG_DISPLAY_GRID, servDisplay::GRID_IS_IRE, serv_name_display );
    }
    else
    {
        mUiAttr.setEnable(MSG_DISPLAY_GRID, GRID_IS_IRE, false);
        if( GRID_IS_IRE == m_nGrid)
        {
            async( MSG_DISPLAY_GRID, servDisplay::GRID_IS_FULL, serv_name_display );
        }
        else
        {
            async( MSG_DISPLAY_GRID, m_nGrid, serv_name_display );
        }
    }
}

/**
 * @brief servDisplay::getWaveType
 * @return
 */
bool servDisplay::getWaveType(void)
{
    if( m_nTimemode == Acquire_XY )
    {
        return true;
    }
    return (getWaveFormat() == FORMAT_IS_DOTS);
}

/**
 * @brief servDisplay::setWaveType
 * @param type
 * @return
 */
DsoErr servDisplay::setWaveType(bool type)
{

    if (type)
    {
        setWaveFormat(FORMAT_IS_DOTS);
    }
    else
    {
        setWaveFormat(FORMAT_IS_VECTOR);
    }
    return ERR_NONE;
}

/**
 * @brief servDisplay::getWaveFormat
 * @return
 */
servDisplay::EWaveFormat servDisplay::getWaveFormat(void)
{
    return m_nFormat;
}


void servDisplay::configWaveFormat(EWaveFormat format)
{
    if ( format == FORMAT_IS_VECTOR )
    {
        postEngine( ENGINE_DISP_TYPE, Wfm_Line );
    }
    else
    {
        postEngine( ENGINE_DISP_TYPE, Wfm_Dot );
    }
}

/**
 * @brief servDisplay::setWaveFormat
 * @param format
 * @return
 */
DsoErr servDisplay::setWaveFormat(EWaveFormat format)
{
    m_nFormat = format;    

    configWaveFormat( format );

    m_nForceFormat = m_nFormat;
    return ERR_NONE;
}

/**
 * @brief servDisplay::getWavePersis
 * @return
 */
servDisplay::EWavePersis servDisplay::getWavePersis(void)
{
    return m_nPersis;
}

/**
 * @brief servDisplay::setWavePersis
 * @param persis
 * @return
 */
DsoErr servDisplay::setWavePersis(EWavePersis persis)
{
    int times[] = { 100, 200, 500,
                    1000, 2000, 5000,
                    10000 };

    if ( persis == PERSIS_IS_NONE )
    {
        postEngine( ENGINE_DISP_PERSIST_TIME, 0 );
    }
    else if ( persis == PERSIS_IS_INFINITE )
    {
        postEngine( ENGINE_DISP_PERSIST_TIME, -1 );
    }
    //! time config
    else
    {
        int id ;

        id = persis - PERSIS_IS_100MS;
        if ( id < 0 || id >= array_count(times) )
        {
            return ERR_INVALID_INPUT;
        }

        postEngine( ENGINE_DISP_PERSIST_TIME, times[id] );
    }

    //CLEAR display after open persis.   for bug1732
    if( m_nPersis == PERSIS_IS_NONE &&  persis != PERSIS_IS_NONE)
    {
        postEngine(ENGINE_DISP_CLEAR, true);
    }
    m_nPersis = persis;

    return ERR_NONE;
}

/**
 * @brief servDisplay::getWaveGrid
 * @return
 */
servDisplay::EWaveGrids servDisplay::getWaveGrid(void)
{
    return m_nGrid;
}

/**
 * @brief servDisplay::setWaveGrid
 * @param grid
 * @return
 */
DsoErr servDisplay::setWaveGrid(EWaveGrids grid)
{
    m_nGrid = grid;
    return ERR_NONE;
}

/**
 * @brief servDisplay::getWaveIntensity
 * @return
 */
int servDisplay::getWaveIntensity(void)
{
    setuiRange( 1, 100, 40 );

    setuiFmt(fmt_int);
    setuiUnit(Unit_none);
    setuiBase(1, E_0);
    setuiUnit( DsoType::Unit_percent );

    return m_nIntensity;
}

/**
 * @brief servDisplay::setWaveIntensity
 * @param intensity
 * @return
 */
DsoErr servDisplay::setWaveIntensity(int intensity)
{
    m_nIntensity = intensity;
    syncEngine( ENGINE_DISP_WFM_LIGHT, intensity );

    return ERR_NONE;
}

/**
 * @brief servDisplay::getGridBrightness
 * @return
 */
int servDisplay::getGridBrightness(void)
{
    setuiZVal( 20 );
    setuiMaxVal(100);
    setuiMinVal(1);
    setuiFmt(fmt_int);
    setuiUnit(Unit_percent);
    setuiBase(1, E_0);
    setuiPostStr("%");

    return m_nBrightness;
}

/**
 * @brief servDisplay::setGridBrightness
 * @param brightness
 * @return
 * grid light not back light
 */
DsoErr servDisplay::setGridBrightness(int brightness)
{
    DsoErr err = ERR_NONE;
    if(brightness < 0)
    {
        brightness = 0;
        //err = ERR_OVER_RANGE;
    }
    else if(brightness > 100)
    {
        brightness = 100;
        //err = ERR_OVER_RANGE;
    }
    m_nBrightness = brightness;

    return err;
}

/**
 * @brief servDisplay::getShowAxis
 * @return
 */
bool servDisplay::getShowAxis(void)
{
    return m_bShowAxis;
}

/**
 * @brief servDisplay::setShowAxis
 * @param b
 * @return
 */
DsoErr servDisplay::setShowAxis(bool b)
{
    m_bShowAxis = b;
    return ERR_NONE;
}

bool servDisplay::get10x8_10x10(void)
{
    return m_b10x8_10x10;
}

DsoErr servDisplay::set10x8_10x10(bool b)
{
    m_b10x8_10x10 = b;
    return ERR_NONE;
}


bool servDisplay::getPalette(void)
{
    return m_bPalette;
}

DsoErr servDisplay::setPalette(bool b)
{
    m_bPalette = b;
    if( b )
    {
        postEngine( ENGINE_DISP_PALETTE, Wfm_grade);
    }
    else
    {
        postEngine( ENGINE_DISP_PALETTE, Wfm_color);
    }
    return ERR_NONE;
}


bool servDisplay::getFreeze(void)
{
    return m_bFreeze;
}

DsoErr servDisplay::setFreeze(bool b)
{
    m_bFreeze = b;
    postEngine( ENGINE_DISP_SCREEN_PERSIST, b);
    return ERR_NONE;
}

DsoErr servDisplay::clear()
{
    postEngine(ENGINE_DISP_CLEAR, true);
    return ERR_NONE;
}
