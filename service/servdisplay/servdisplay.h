#ifndef servDisplay_H
#define servDisplay_H

#include "../service.h"
#include "../service_name.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "../servhori/servhori.h"

class servDisplay : public serviceExecutor,public ISerial
{
     Q_OBJECT
public:
    DECLARE_CMD()
public:
    servDisplay( QString name = serv_name_display, ServiceId eId = E_SERVICE_ID_DISPLAY );

    enum ListenerMsg
    {
        cmd_base = 0,//CMD_APP_BASE,
        On_Chan1_Changed,
        On_Chan2_Changed,
        On_Chan3_Changed,
        On_Chan4_Changed,
        On_Trigger_Changed,

        On_TimeMode_changed,
        //On_Timemode_Changed,

    };

    enum EWaveFormat
    {
        FORMAT_IS_VECTOR,
        FORMAT_IS_DOTS
    };
    enum EWavePersis
    {
        PERSIS_IS_NONE,
        PERSIS_IS_100MS,
        PERSIS_IS_200MS,
        PERSIS_IS_500MS,
        PERSIS_IS_1S,
        PERSIS_IS_2S,
        PERSIS_IS_5S,
        PERSIS_IS_10S,
        PERSIS_IS_INFINITE,
    };
    enum EWaveGrids
    {
        GRID_IS_FULL,
        GRID_IS_HALF,
        GRID_IS_NONE,
        GRID_IS_IRE,
    };
    Q_ENUM(EWaveGrids)



public:
    int  serialOut( CStream &stream ,unsigned char &ver);
    int  serialIn ( CStream &stream, unsigned char ver );
    void rst();
    int  startup();
    void registerSpy();

public:
    void onTriggerChanged(void);
    void onChanChanged(QString name);
    void onChan1(void);
    void onChan2(void);
    void onChan3(void);
    void onChan4(void);

    void onVideoListener(QString name="");
    void onTimeMode();

public://set
    DsoErr setWaveFormat( EWaveFormat format );
    void   configWaveFormat( EWaveFormat format );
    DsoErr setWavePersis( EWavePersis persis );
    DsoErr setWaveGrid( EWaveGrids grid);
    DsoErr setWaveIntensity( int intensity);
    DsoErr setGridBrightness( int brightness);
    DsoErr setWaveType(bool type);
    DsoErr setShowAxis(bool b);
    DsoErr set10x8_10x10(bool b);
    DsoErr setPalette(bool b);
    DsoErr setFreeze(bool b);
    DsoErr clear(void);

public://get
    EWaveFormat getWaveFormat(void);
    EWavePersis getWavePersis(void);
    EWaveGrids  getWaveGrid(void);
    int         getWaveIntensity(void);
    int         getGridBrightness(void);
    bool        getWaveType(void);
    bool        getShowAxis(void);
    bool        get10x8_10x10(void);
    bool        getPalette();
    bool        getFreeze();

private:
    EWaveFormat m_nFormat;
    EWavePersis m_nPersis;
    EWaveGrids  m_nGrid;
    int         m_nIntensity;
    int         m_nBrightness;
    bool        m_bShowAxis;

    bool        m_b10x8_10x10;
    bool        m_bPalette;
    bool        m_bFreeze;
    int         m_nTimemode;

    EWaveFormat m_nForceFormat;
};

#endif // servDisplay_H
