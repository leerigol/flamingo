#include "counterdata.h"

counterData::counterData(Chan src)
{
    m_Src = src;
    m_Type = FREQ;
    m_Mode = counter_freq;
    m_GateTime = 1e6; //1s

    m_Val = 0.0;
    m_Max = 0.0;
    m_Min = 0.0;
    m_Status = CTR_No_Signal;

    m_Old_signalWidth = 0;
    m_checkSignal_count = 0;
}

void counterData::setType(CounterType type)
{
    m_Type = type;

    m_Val = 0.0;
    m_Max = 0.0;
    m_Min = 0.0;
    m_Status = CTR_No_Signal;
    m_Old_signalWidth = 0;
    m_checkSignal_count = 0;
}

void counterData::setSource(Chan src)
{
    m_Src  = src;

    m_Val = 0.0;
    m_Max = 0.0;
    m_Min = 0.0;
    m_Status = CTR_No_Signal;
    m_Old_signalWidth = 0;
    m_checkSignal_count = 0;
}

void counterData::setGataTme(quint32 g)
{
     m_GateTime = g;
}

void counterData::calcData(EngineCounterValue *val, bool isTrigEventSrc)
{
    Q_ASSERT( NULL != val );

    if( NULL != val)
    {
//        qDebug()<<"counterData::calcData m_Type = "<<m_Type;
//        qDebug()<<"counterData::calcData isTrigEventSrc = "<<isTrigEventSrc;
        //! for bug 1819 "||" to "&&"
        if( (m_Type != TOTALIZE)&&(!isTrigEventSrc)) //! MeasType: FREQ/PERIOD
        {
            if( (val->mEdgeCnt <= 1)&&(val->signalWidth <= 0) )
            {
                LOG_DBG() <<"------------------------No signal";
                LOG_DBG() <<"------------------------mEdgeCnt: "<<val->mEdgeCnt;
                LOG_DBG() <<"------------------------signalWidth: " <<val->signalWidth;
                m_Val = 0.0;
                m_Status = CTR_No_Signal;

                return;
            }

//            qDebug() << "-------------CounterData: mode == "<<m_Mode;
            //! counter_event(Meas Period) Mode
            if( m_Mode == counter_event)
            {
                if( checkSignal( val)) //! Signal is valid
                {
                    calcPeriod( val);
                }
                else  //! Signal is invalid
                {
                    m_Val = 0.0;
                    m_Status = CTR_No_Signal;
                }
            }
            else  //! counter_freq(Meas Freq) Mode
            {
                calcFreq( val);
            }

        }
        else  //! MeasType: TOTALIZE or isTrigEventSrc
        {
            calcTotalize( val);
        }

    } //if( NULL != val)
}

//! check Signal(for counter_event Mode)
//! true: Signal is valid
//! false:Signal is invalid
bool counterData::checkSignal(EngineCounterValue *val)
{
    if( val->signalWidth == m_Old_signalWidth)
    {
        if( m_checkSignal_count <= 3)
        {
            m_checkSignal_count += 1;
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        m_Old_signalWidth = val->signalWidth;
        m_checkSignal_count = 0;
        return true;
    }
}

void counterData::calcPeriod(EngineCounterValue *val)
{
    double period = (val->signalWidth)*(val->mTimeUnit); //! Unit:ps
//    qDebug() << "------------------------signalWidth: "<<val->signalWidth;
    LOG_DBG() << "------------------------period: "<<period<<" ps";

#if 1 //![dba, bug:1762]
    if( (period == 0) && (m_Type != PERIOD) )
    {
//        qDebug()<<__FILE__<<__LINE__<<"[dba]--------------------period = 0, Invalid-------------------";
//        qDebug()<<"signalWidth:"<<val->signalWidth<<"mTimeUnit:"<<val->mTimeUnit;
//        qDebug()<<__FILE__<<__LINE__<<"----------------------------------------------------------------";
        m_Val = 0.0;
        m_Status = CTR_No_Signal;
        return;
    }
#endif

    if( m_Type == FREQ)
    {
        m_Val = 1e12/period;  //! Unit: Hz
        m_Status = CTR_Valid;
    }
    else if( m_Type == PERIOD)
    {
        m_Val = period/1e12; //! Unit: s
        m_Status = CTR_Valid;
    }
    else
    {
        m_Val = 1e12/period;  //! Unit: Hz
        m_Status = CTR_Valid;
    }

    setMax();
    setMin();
}

void counterData::calcFreq(EngineCounterValue *val)
{
    double period = 0.0;
    double headTime = val->mThead;
    double tailTime = val->mTtail;

    //! Period unit:ps
    //! GateTime unit: us
#if 0
    period = (m_GateTime*1e6-(headTime+tailTime)*(val->mTimeUnit))/(val->mEdgeCnt-1);
#else //![dba+, bug:1762]
    period = (m_GateTime*1e6-(headTime+tailTime)*(val->mTimeUnit))/(qMax((quint32)1,val->mEdgeCnt-1));
    if( (period <= 0) && (m_Type != PERIOD) )
    {
//        qDebug()<<__FILE__<<__LINE__<<"[dba]--------------------period = 0, Invalid-------------------";
//        qDebug()<<"m_GateTime:"<<m_GateTime <<"headTime:"<<headTime <<"tailTime:"<<tailTime<<"mTimeUnit:"<<val->mTimeUnit;
//        qDebug()<<__FILE__<<__LINE__<<"----------------------------------------------------------------";
        m_Val = 0.0;
        m_Status = CTR_No_Signal;
        return;
    }
#endif

    LOG_DBG() << "------------------------mThead: "<<val->mThead;
    LOG_DBG() << "------------------------mTtail: "<<val->mTtail;
//    qDebug() << "------------------------mEdgeCnt: "<<val->mEdgeCnt;
    LOG_DBG() << "------------------------period: "<<period<<" ps";

    if( m_Type == FREQ)
    {
        Q_ASSERT(period!=0); //![dba+, bug:1762]

        m_Val = 1e12/period;  //! Unit: Hz
        m_Status = CTR_Valid;
    }
    else if( m_Type == PERIOD)
    {
        m_Val = period/1e12; //! Unit: s
        m_Status = CTR_Valid;
    }
    else
    {
        Q_ASSERT(period!=0); //![dba+, bug:1762]

        m_Val = 1e12/period;  //! Unit: Hz
        m_Status = CTR_Valid;
    }

    setMax();
    setMin();
}

void counterData::calcTotalize(EngineCounterValue *val)
{
    m_Val = val->signalEvent;

    /*! [dba: 2018-04-27] bug:累加器不计数*/
    m_Status = CTR_Valid;
    //qDebug() << "------------------------signalEvent: "<< val->signalEvent;
}

void counterData::resetMaxMin()
{
    m_Max = 0.0;
    m_Min = 0.0;
}

void counterData::setstatus(CounterStatus s)
{
    m_Status = s;
}

void counterData::setMax()
{
    if( m_Max <= 0.0)
    {
        m_Max = m_Val;
    }
    else
    {
        m_Max = (m_Val>m_Max)? m_Val : m_Max;
    }

    // add by lidomging for bug 2425
    if( m_Max <= 0.0)
    {
        m_Max = 0.0;
    }
}

void counterData::setMin()
{
    if( m_Min <= 0.0)
    {
        m_Min = m_Val;
    }
    else
    {
        m_Min = (m_Val<m_Min)? m_Val : m_Min;
    }

    // add by lidomging for bug 2425
    if( m_Min <= 0.0)
    {
        m_Min = 0.0;
    }
}
