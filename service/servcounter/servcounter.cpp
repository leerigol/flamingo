#include "servcounter.h"
#include "../servmeasure/servmeasure.h"
#include "../servlicense/servlicense.h"
#include "../../engine/base/enginecfg.h"

#define dbg_out()
#define COUNTER_CFG_CHANGE()  do{m_cfgChangeTag = 1;}while(0)
#define CHECK_CFG_CHANGE(b)   do{b = false; \
                                 if(m_cfgChangeTag > 0){b=true; m_cfgChangeTag--;} \
                                 }while(0)

IMPLEMENT_CMD( serviceExecutor, servCounter )
start_of_entry()

on_set_int_bool( MSG_COUNTER_1_ENABLE,          &servCounter::set_Ctr1_Enable ),
on_get_bool(     MSG_COUNTER_1_ENABLE,          &servCounter::get_Ctr1_Enable ),

//on_set_int_bool( MSG_COUNTER_2_ENABLE,          &servCounter::set_Ctr2_Enable ),
//on_get_bool(     MSG_COUNTER_2_ENABLE,          &servCounter::get_Ctr2_Enable ),

on_set_int_int(  MSG_COUNTER_1_SRC,             &servCounter::set_Ctr1_Source ),
on_get_int(      MSG_COUNTER_1_SRC,             &servCounter::get_Ctr1_Source ),

on_set_int_int(  MSG_COUNTER_1_MEAS_TYPE,       &servCounter::set_Ctr1_MeasType ),
on_get_int(      MSG_COUNTER_1_MEAS_TYPE,       &servCounter::get_Ctr1_MeasType ),

on_set_int_int(  MSG_COUNTER_1_RESOLUTION,      &servCounter::set_Ctr1_Resolution ),
on_get_int(      MSG_COUNTER_1_RESOLUTION,      &servCounter::get_Ctr1_Resolution ),

on_set_int_bool( MSG_COUNTER_1_STAT,            &servCounter::set_Ctr1_Stat ),
on_get_bool(     MSG_COUNTER_1_STAT,            &servCounter::get_Ctr1_Stat ),

//on_set_int_int(  MSG_COUNTER_1_TH,              &servCounter::set_Ctr1_Threshold ),
//on_get_int(      MSG_COUNTER_1_TH,              &servCounter::get_Ctr1_Threshold ),

//on_set_int_void( MSG_COUNTER_1_TH_AUTO,         &servCounter::set_Ctr1_AutoSetup ),

on_set_int_void( MSG_COUNTER_1_TOT_CLEAR,       &servCounter::set_Ctr1_Tot_Clear ),

//on_set_int_void( MSG_COUNTER_1_TOT_TH_AUTO,     &servCounter::set_Ctr1_Tot_AutoSetup ),

//on_set_int_bool( MSG_COUNTER_1_TOT_SLOP,        &servCounter::set_Ctr1_Slop ),
//on_get_bool(     MSG_COUNTER_1_TOT_SLOP,        &servCounter::get_Ctr1_Slop ),

//on_set_int_bool( MSG_COUNTER_1_TOT_GATE_ENABLE, &servCounter::set_Ctr1_Gate ),
//on_get_bool(     MSG_COUNTER_1_TOT_GATE_ENABLE, &servCounter::get_Ctr1_Gate ),

//on_set_int_int(  MSG_COUNTER_1_TOT_GATE_SRC,    &servCounter::set_Ctr1_Gate_Source ),
//on_get_int(      MSG_COUNTER_1_TOT_GATE_SRC,    &servCounter::get_Ctr1_Gate_Source ),

//on_set_int_bool( MSG_COUNTER_1_TOT_GATE_POLA,   &servCounter::set_Ctr1_Gate_Polarity ),
//on_get_bool(     MSG_COUNTER_1_TOT_GATE_POLA,   &servCounter::get_Ctr1_Gate_Polarity ),

//on_set_int_int(  MSG_COUNTER_1_TOT_GATE_TH,     &servCounter::set_Ctr1_Gate_Threshold ),
//on_get_int(      MSG_COUNTER_1_TOT_GATE_TH,     &servCounter::get_Ctr1_Gate_Threshold ),

//on_set_int_int(  MSG_COUNTER_2_SRC,             &servCounter::set_Ctr2_Source ),
//on_get_int(      MSG_COUNTER_2_SRC,             &servCounter::get_Ctr2_Source ),

//on_set_int_int(  MSG_COUNTER_2_MEAS_TYPE,       &servCounter::set_Ctr2_MeasType ),
//on_get_int(      MSG_COUNTER_2_MEAS_TYPE,       &servCounter::get_Ctr2_MeasType ),

//on_set_int_int(  MSG_COUNTER_2_RESOLUTION,      &servCounter::set_Ctr2_Resolution ),
//on_get_int(      MSG_COUNTER_2_RESOLUTION,      &servCounter::get_Ctr2_Resolution ),

//on_set_int_bool( MSG_COUNTER_2_STAT,            &servCounter::set_Ctr2_Stat ),
//on_get_bool(     MSG_COUNTER_2_STAT,            &servCounter::get_Ctr2_Stat ),

//on_set_int_int(  MSG_COUNTER_2_TH,              &servCounter::set_Ctr2_Threshold ),
//on_get_int(      MSG_COUNTER_2_TH,              &servCounter::get_Ctr2_Threshold ),

//on_set_int_void( MSG_COUNTER_2_TH_AUTO,         &servCounter::set_Ctr2_AutoSetup ),

//on_set_int_void( MSG_COUNTER_2_TOT_CLEAR,       &servCounter::set_Ctr2_Tot_Clear ),

//on_set_int_void( MSG_COUNTER_2_TOT_TH_AUTO,     &servCounter::set_Ctr2_Tot_AutoSetup ),

//on_set_int_bool( MSG_COUNTER_2_TOT_SLOP,        &servCounter::set_Ctr2_Slop ),
//on_get_bool(     MSG_COUNTER_2_TOT_SLOP,        &servCounter::get_Ctr2_Slop ),

//on_set_int_bool( MSG_COUNTER_2_TOT_GATE_ENABLE, &servCounter::set_Ctr2_Gate ),
//on_get_bool(     MSG_COUNTER_2_TOT_GATE_ENABLE, &servCounter::get_Ctr2_Gate ),

//on_set_int_int(  MSG_COUNTER_2_TOT_GATE_SRC,    &servCounter::set_Ctr2_Gate_Source ),
//on_get_int(      MSG_COUNTER_2_TOT_GATE_SRC,    &servCounter::get_Ctr2_Gate_Source ),

//on_set_int_bool( MSG_COUNTER_2_TOT_GATE_POLA,   &servCounter::set_Ctr2_Gate_Polarity ),
//on_get_bool(     MSG_COUNTER_2_TOT_GATE_POLA,   &servCounter::get_Ctr2_Gate_Polarity ),

//on_set_int_int(  MSG_COUNTER_2_TOT_GATE_TH,     &servCounter::set_Ctr2_Gate_Threshold ),
//on_get_int(      MSG_COUNTER_2_TOT_GATE_TH,     &servCounter::get_Ctr2_Gate_Threshold ),

on_get_pointer(  servCounter::cmd_Get_CounterData, &servCounter::getCounterData),
on_set_void_void(servCounter::cmd_Set_Opt_CTR,     &servCounter::set_Opt_CTR),

/*******scpi*******/
on_get_string( servCounter::cmd_scpi_counter_value, &servCounter::getCounterValue),


on_set_void_void( CMD_SERVICE_RST,                 &servCounter::rst ),
on_set_void_void( CMD_SERVICE_INIT,                &servCounter::init ),
on_set_int_void ( CMD_SERVICE_STARTUP,             &servCounter::startup ),
on_set_int_void ( CMD_SERVICE_ACTIVE,              &servCounter::onActive ),
end_of_entry()

servCounter::servCounter( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    m_Ctr1_Enable          = false;      //! OFF
    m_Ctr1_Source          = (int)chan1;
    m_Ctr1_MeasType        = FREQ;       //! Frequency
    m_Ctr1_Resolution      = 5;          //! 5 Resolution
    m_Ctr1_Stat            = false;      //! Statistic Off
    m_Ctr1_Threshold       = 0;
    m_Ctr1_Slop            = false;      //! Rising
    m_Ctr1_Gate            = false;      //! Gate OFF
    m_Ctr1_Gate_Source     = (int)chan2;
    m_Ctr1_Gate_Polarity   = false;      //! Gate Polarity: Positive
    m_Ctr1_Gate_Threshold  = 0;

    m_Ctr2_Enable          = false;      //! OFF
    m_Ctr2_Source          = (int)chan1;
    m_Ctr2_MeasType        = FREQ;       //! Frequency
    m_Ctr2_Resolution      = 5;          //! 5 Resolution
    m_Ctr2_Stat            = false;      //! Statistic Off
    m_Ctr2_Threshold       = 0;
    m_Ctr2_Slop            = false;      //! Rising
    m_Ctr2_Gate            = false;      //! Gate OFF
    m_Ctr2_Gate_Source     = (int)chan2;
    m_Ctr2_Gate_Polarity   = false;      //! Gate Polarity: Positive
    m_Ctr2_Gate_Threshold  = 0;

    mCtr1FreqSrc  = (int)chan1;
    mCtr1GateTime = Gate200ms;

    mCtr2FreqSrc  = (int)chan1;
    mCtr2GateTime = Gate200ms;

    m_CounterData   = NULL;

    m_LicenseValid  = false;

    m_cfgChangeTag  = 0;
}

int servCounter::serialOut(CStream &stream, serialVersion &ver)
{
    ver = mVersion;
    stream.write(QStringLiteral("m_Ctr1_Enable"),         m_Ctr1_Enable);
    stream.write(QStringLiteral("m_Ctr1_Source"),         m_Ctr1_Source);
    stream.write(QStringLiteral("m_Ctr1_MeasType"),       m_Ctr1_MeasType);
    stream.write(QStringLiteral("m_Ctr1_Resolution"),     m_Ctr1_Resolution);
    stream.write(QStringLiteral("m_Ctr1_Stat"),           m_Ctr1_Stat);
    stream.write(QStringLiteral("m_Ctr1_Threshold"),      m_Ctr1_Threshold);
    stream.write(QStringLiteral("m_Ctr1_Slop"),           m_Ctr1_Slop);
    stream.write(QStringLiteral("m_Ctr1_Gate"),           m_Ctr1_Gate);
    stream.write(QStringLiteral("m_Ctr1_Gate_Source"),    m_Ctr1_Gate_Source);
    stream.write(QStringLiteral("m_Ctr1_Gate_Polarity"),  m_Ctr1_Gate_Polarity);
    stream.write(QStringLiteral("m_Ctr1_Gate_Threshold"), m_Ctr1_Gate_Threshold);

    stream.write(QStringLiteral("m_Ctr2_Enable"),         m_Ctr2_Enable);
    stream.write(QStringLiteral("m_Ctr2_Source"),         m_Ctr2_Source);
    stream.write(QStringLiteral("m_Ctr2_MeasType"),       m_Ctr2_MeasType);
    stream.write(QStringLiteral("m_Ctr2_Resolution"),     m_Ctr2_Resolution);
    stream.write(QStringLiteral("m_Ctr2_Stat"),           m_Ctr2_Stat);
    stream.write(QStringLiteral("m_Ctr2_Threshold"),      m_Ctr2_Threshold);
    stream.write(QStringLiteral("m_Ctr2_Slop"),           m_Ctr2_Slop);
    stream.write(QStringLiteral("m_Ctr2_Gate"),           m_Ctr2_Gate);
    stream.write(QStringLiteral("m_Ctr2_Gate_Source"),    m_Ctr2_Gate_Source);
    stream.write(QStringLiteral("m_Ctr2_Gate_Polarity"),  m_Ctr2_Gate_Polarity);
    stream.write(QStringLiteral("m_Ctr2_Gate_Threshold"), m_Ctr2_Gate_Threshold);

    stream.write(QStringLiteral("mCtr1FreqSrc"),   mCtr1FreqSrc);
    stream.write(QStringLiteral("mCtr1GateTime"),  mCtr1GateTime);

    stream.write(QStringLiteral("mCtr2FreqSrc"),   mCtr2FreqSrc);
    stream.write(QStringLiteral("mCtr2GateTime"),  mCtr2GateTime);

    return ERR_NONE;
}

int servCounter::serialIn( CStream &stream, serialVersion ver )
{
    if( ver == mVersion)
    {   
        stream.read(QStringLiteral("m_Ctr1_Enable"),         m_Ctr1_Enable);
        stream.read(QStringLiteral("m_Ctr1_Source"),         m_Ctr1_Source);
        stream.read(QStringLiteral("m_Ctr1_MeasType"),       m_Ctr1_MeasType);
        stream.read(QStringLiteral("m_Ctr1_Resolution"),     m_Ctr1_Resolution);
        stream.read(QStringLiteral("m_Ctr1_Stat"),           m_Ctr1_Stat);
        stream.read(QStringLiteral("m_Ctr1_Threshold"),      m_Ctr1_Threshold);
        stream.read(QStringLiteral("m_Ctr1_Slop"),           m_Ctr1_Slop);
        stream.read(QStringLiteral("m_Ctr1_Gate"),           m_Ctr1_Gate);
        stream.read(QStringLiteral("m_Ctr1_Gate_Source"),    m_Ctr1_Gate_Source);
        stream.read(QStringLiteral("m_Ctr1_Gate_Polarity"),  m_Ctr1_Gate_Polarity);
        stream.read(QStringLiteral("m_Ctr1_Gate_Threshold"), m_Ctr1_Gate_Threshold);

        stream.read(QStringLiteral("m_Ctr2_Enable"),         m_Ctr2_Enable);
        stream.read(QStringLiteral("m_Ctr2_Source"),         m_Ctr2_Source);
        stream.read(QStringLiteral("m_Ctr2_MeasType"),       m_Ctr2_MeasType);
        stream.read(QStringLiteral("m_Ctr2_Resolution"),     m_Ctr2_Resolution);
        stream.read(QStringLiteral("m_Ctr2_Stat"),           m_Ctr2_Stat);
        stream.read(QStringLiteral("m_Ctr2_Threshold"),      m_Ctr2_Threshold);
        stream.read(QStringLiteral("m_Ctr2_Slop"),           m_Ctr2_Slop);
        stream.read(QStringLiteral("m_Ctr2_Gate"),           m_Ctr2_Gate);
        stream.read(QStringLiteral("m_Ctr2_Gate_Source"),    m_Ctr2_Gate_Source);
        stream.read(QStringLiteral("m_Ctr2_Gate_Polarity"),  m_Ctr2_Gate_Polarity);
        stream.read(QStringLiteral("m_Ctr2_Gate_Threshold"), m_Ctr2_Gate_Threshold);

        stream.read(QStringLiteral("mCtr1FreqSrc"),   mCtr1FreqSrc);
        stream.read(QStringLiteral("mCtr1GateTime"),  mCtr1GateTime);

        stream.read(QStringLiteral("mCtr2FreqSrc"),   mCtr2FreqSrc);
        stream.read(QStringLiteral("mCtr2GateTime"),  mCtr2GateTime);
    }
    else
    {
        rst();
    }

    return ERR_NONE;
}

void servCounter::rst()
{
    //Reset counter if one source opened. by hxh. 2018-05-17
    if( m_Ctr1_Enable )
    {
        set_Ctr1_Enable( false );
    }

    m_Ctr1_Enable          = false;      //! OFF
    m_Ctr1_Source          = (int)chan1;
    m_Ctr1_MeasType        = FREQ;       //! Frequency
    m_Ctr1_Resolution      = 5;          //! 5 Resolution
    m_Ctr1_Stat            = false;      //! Statistic Off
    m_Ctr1_Threshold       = 0;
    m_Ctr1_Slop            = false;      //! Rising
    m_Ctr1_Gate            = false;      //! Gate OFF
    m_Ctr1_Gate_Source     = (int)chan2;
    m_Ctr1_Gate_Polarity   = false;      //! Gate Polarity: Positive
    m_Ctr1_Gate_Threshold  = 0;

    m_Ctr2_Enable          = false;      //! OFF
    m_Ctr2_Source          = (int)chan1;
    m_Ctr2_MeasType        = FREQ;       //! Frequency
    m_Ctr2_Resolution      = 5;          //! 5 Resolution
    m_Ctr2_Stat            = false;      //! Statistic Off
    m_Ctr2_Threshold       = 0;
    m_Ctr2_Slop            = false;      //! Rising
    m_Ctr2_Gate            = false;      //! Gate OFF
    m_Ctr2_Gate_Source     = (int)chan2;
    m_Ctr2_Gate_Polarity   = false;      //! Gate Polarity: Positive
    m_Ctr2_Gate_Threshold  = 0;

    mCtr1FreqSrc  = (int)chan1;
    mCtr1GateTime = Gate200ms;

    mCtr2FreqSrc  = (int)chan1;
    mCtr2GateTime = Gate200ms;

    m_CounterData   = NULL;

    m_LicenseValid  = false;

}

DsoErr servCounter::onActive()
{
    setActive();
    return ERR_NONE;
}

void servCounter::set_Opt_CTR()
{
    //by hxh
//    CArgument argIn,argOut;

//    argIn.setVal((int)OPT_CTR);
//    query(serv_name_license, servLicense::cmd_get_OptEnable, argIn, argOut);

//    if( argOut[0].bVal)
//    {
////        qDebug() << "servCounter::"<<__FUNCTION__<<"counter eable";
//        m_LicenseValid  = true;
//    }
//    else
//    {
////        qDebug() << "servCounter::"<<__FUNCTION__<<"counter disable";
//        m_LicenseValid  = true;
//    }
////    qDebug()<<"servCounter::"<<__FUNCTION__<<"m_LicenseValid = "<<m_LicenseValid;

//    mUiAttr.setEnable(MSG_COUNTER_1_MEAS_TYPE, 3, m_LicenseValid); //! Enable/disable Type: Totalize
//    mUiAttr.setEnable(MSG_COUNTER_1_RESOLUTION, m_LicenseValid);   //! Enable/disable Resolution set
//    mUiAttr.setEnable(MSG_COUNTER_1_STAT, m_LicenseValid);           //! Enable/disable Stat set
}

QString servCounter::getCounterValue()
{
    counterData *pValue = NULL;
    QString valStr = "9.9E+37";

    pValue = (counterData*)getCounterData();

    if(pValue != NULL)
    {
        if( pValue->status() == CTR_Valid)
        {
            double val = pValue->getVal();
            valStr = QString("%1").arg(val, 0, 'G', m_Ctr1_Resolution, '0' );
            //qDebug()<<"servCounter::getCounterValue() valStr = "<<valStr;
        }
        else
        {
            valStr = "9.9E+37";
        }

        return valStr;
    }
    else
    {
        valStr = "9.9E+37";
        return valStr;
    }
}

void servCounter::init()
{
    //! La check
    if( sysHasLA())
    {
        for( int i = (int)d0; i<=(int)d15; i++)
        {
            mUiAttr.setVisible( MSG_COUNTER_1_SRC, i, true);
        }
    }
    else
    {
        for( int i = (int)d0; i<=(int)d15; i++)
        {
            mUiAttr.setVisible( MSG_COUNTER_1_SRC, i, false);
        }
    }
}

int  servCounter::startup()
{
    mUiAttr.setVisible(MSG_COUNTER_1_SRC, SRC_TRIG_EVENT, false);

    async( MSG_COUNTER_1_ENABLE, m_Ctr1_Enable);

    if( (CounterType)m_Ctr1_MeasType == TOTALIZE)
    {
        QThread::msleep( 200);  //! wait for counter reset
        id_async( getId(),MSG_COUNTER_1_TOT_CLEAR, 1 );
    }

    return ERR_NONE;
}

void servCounter::registerSpy()
{
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Active,  servCounter::cmd_Set_Opt_CTR);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Exp,     servCounter::cmd_Set_Opt_CTR);
    spyOn(E_SERVICE_ID_LICENSE, servLicense::cmd_Opt_Invalid, servCounter::cmd_Set_Opt_CTR);
}

DsoErr servCounter::set_Ctr1_Enable(bool b)
{

    m_Ctr1_Enable = b;
    DsoErr err = ERR_NONE;

    if( m_Ctr1_Enable)
    {
        err = postEngine(ENGINE_DVM_COUNTOR_CH_EN, m_Ctr1_Source, true);
        if( err != ERR_NONE)
        {
            return err;
        }
        cfgCounter();

        err = postEngine(ENGINE_COUNTER_EN, true);
        LOG_DBG()<<"--------------------"<<err <<" = postEngine(ENGINE_COUNTER_EN, true)";

        //! -- resource
        if( m_CounterData == NULL)
        {
            m_CounterData = new counterData((Chan)mCtr1FreqSrc);
            Q_ASSERT( NULL != m_CounterData);

            m_CounterData->setGataTme(mCtr1GateTime);
            m_CounterData->setType((CounterType)m_Ctr1_MeasType);
            m_CounterData->setMode(counter_freq);
        }
    }
    else
    {
        //! -- collect resource
        err = postEngine(ENGINE_COUNTER_EN, false);
        LOG_DBG()<<"--------------------"<<err <<" = postEngine(ENGINE_COUNTER_EN, false)";

        if( m_CounterData != NULL)
        {
            delete m_CounterData;
            m_CounterData = NULL;
        }

        err = postEngine(ENGINE_DVM_COUNTOR_CH_EN, m_Ctr1_Source, false);
        if( err != ERR_NONE)
        {
            return err;
        }
    }

    return ERR_NONE;
}

bool servCounter::get_Ctr1_Enable()
{
    return m_Ctr1_Enable;
}

DsoErr servCounter::set_Ctr2_Enable(bool b)
{
    m_Ctr2_Enable = b;
    return ERR_NONE;
}

bool servCounter::get_Ctr2_Enable()
{
    return m_Ctr2_Enable;
}

DsoErr servCounter::set_Ctr1_Source(int v)
{
    DsoErr err = ERR_NONE;
    if( v == m_Ctr1_Gate_Source)
    {
        //! Gate Source disable m_Ctr1_Source item
//        mUiAttr.setEnable( MSG_COUNTER_1_TOT_GATE_SRC, v, false);
        //! Set Gate Source to old m_Ctr1_Source
        m_Ctr1_Gate_Source = m_Ctr1_Source;
    }

    if( m_Ctr1_Enable)
    {
        err = postEngine(ENGINE_DVM_COUNTOR_CH_EN, m_Ctr1_Source, false);
        if( err != ERR_NONE)
        {
            return err;
        }

        m_Ctr1_Source = v;

        err = postEngine(ENGINE_DVM_COUNTOR_CH_EN, m_Ctr1_Source, true);
        if( err != ERR_NONE)
        {
            return err;
        }
    }
    else
    {
        m_Ctr1_Source = v;
    }

    if( m_Ctr1_Source != SRC_TRIG_EVENT)
    {
        mCtr1FreqSrc = m_Ctr1_Source;
    }
    cfgCounter();
    postEngine( ENGINE_COUNTER_RST);

    if( NULL != m_CounterData )
    {
        m_CounterData->setSource( (Chan)mCtr1FreqSrc);
    }

    return ERR_NONE;
}

int servCounter::get_Ctr1_Source()
{
    return m_Ctr1_Source;
}

DsoErr servCounter::set_Ctr1_MeasType(int v)
{
    m_Ctr1_MeasType = v;
    cfgCounter();

    if( m_CounterData != NULL)
    {
        m_CounterData->setType((CounterType)m_Ctr1_MeasType);
    }
    return ERR_NONE;
}

int servCounter::get_Ctr1_MeasType()
{
    return m_Ctr1_MeasType;
}

DsoErr servCounter::set_Ctr1_Resolution(int v)
{
    COUNTER_CFG_CHANGE();

    DsoErr err = ERR_NONE;
    m_Ctr1_Resolution = v;

    if( m_Ctr1_Resolution == 3)       mCtr1GateTime = Gate50ms;
    else if( m_Ctr1_Resolution == 4)  mCtr1GateTime = Gate50ms;
    else if( m_Ctr1_Resolution == 5)  mCtr1GateTime = Gate200ms;
    else if( m_Ctr1_Resolution == 6)  mCtr1GateTime = Gate1000ms;
    else                              mCtr1GateTime = Gate50ms;

    cfgCounter();
    if( NULL != m_CounterData)
    {
        m_CounterData->setGataTme( mCtr1GateTime);
    }
    return err;
}

int servCounter::get_Ctr1_Resolution()
{
    setuiStep(1);

    setuiRange(3, 6, 6);

    return m_Ctr1_Resolution;
}

DsoErr servCounter::set_Ctr1_Stat(bool b)
{
    m_Ctr1_Stat = b;

    if(b)
    {
        if( NULL != m_CounterData)
        {
            m_CounterData->resetMaxMin();
        }
    }
    return ERR_NONE;
}

bool servCounter::get_Ctr1_Stat()
{
    return m_Ctr1_Stat;
}

DsoErr servCounter::set_Ctr1_Threshold(int v)
{
    m_Ctr1_Threshold = v;

    //SpyOn Trigger
    return ERR_NONE;
}

int servCounter::get_Ctr1_Threshold()
{
    return m_Ctr1_Threshold;
}

DsoErr servCounter::set_Ctr1_AutoSetup()
{

    return ERR_NONE;
}

DsoErr servCounter::set_Ctr1_Tot_Clear()
{
    DsoErr err = ERR_NONE;
    err = postEngine( ENGINE_COUNTER_RST);
    return err;
}

DsoErr servCounter::set_Ctr1_Tot_AutoSetup()
{

    return ERR_NONE;
}

DsoErr servCounter::set_Ctr1_Slop(bool b)
{
    m_Ctr1_Slop = b;

    cfgCounter();

    return ERR_NONE;
}

bool servCounter::get_Ctr1_Slop()
{
    return m_Ctr1_Slop;
}

DsoErr servCounter::set_Ctr1_Gate(bool b)
{
    m_Ctr1_Gate = b;

    cfgCounter();

    return ERR_NONE;
}

bool servCounter::get_Ctr1_Gate()
{
    return m_Ctr1_Gate;
}

DsoErr servCounter::set_Ctr1_Gate_Source(int v)
{
    m_Ctr1_Gate_Source = v;
    cfgCounter();

    return ERR_NONE;
}

int servCounter::get_Ctr1_Gate_Source()
{
    return m_Ctr1_Gate_Source;
}

DsoErr servCounter::set_Ctr1_Gate_Polarity(bool b)
{
    m_Ctr1_Gate_Polarity = b;

    cfgCounter();
    return ERR_NONE;
}

bool servCounter::get_Ctr1_Gate_Polarity()
{
    return m_Ctr1_Gate_Polarity;
}

DsoErr servCounter::set_Ctr1_Gate_Threshold(int v)
{
    m_Ctr1_Gate_Threshold = v;

    return ERR_NONE;
}

int servCounter::get_Ctr1_Gate_Threshold()
{
    return m_Ctr1_Gate_Threshold;
}

DsoErr servCounter::set_Ctr2_Source(int v)
{
    m_Ctr2_Source = v;
    return ERR_NONE;
}

int servCounter::get_Ctr2_Source()
{
    return m_Ctr2_Source;
}

DsoErr servCounter::set_Ctr2_MeasType(int v)
{
    m_Ctr2_MeasType = v;
    return ERR_NONE;
}

int servCounter::get_Ctr2_MeasType()
{
    return m_Ctr2_MeasType;
}

DsoErr servCounter::set_Ctr2_Resolution(int v)
{
    m_Ctr2_Resolution = v;
    return ERR_NONE;
}

int servCounter::get_Ctr2_Resolution()
{
    return m_Ctr2_Resolution;
}

DsoErr servCounter::set_Ctr2_Stat(bool b)
{
    m_Ctr2_Stat = b;
    return ERR_NONE;
}

bool servCounter::get_Ctr2_Stat()
{
    return m_Ctr2_Stat;
}

DsoErr servCounter::set_Ctr2_Threshold(int v)
{
    m_Ctr2_Threshold = v;
    return ERR_NONE;
}

int servCounter::get_Ctr2_Threshold()
{
    return m_Ctr2_Threshold;
}

DsoErr servCounter::set_Ctr2_AutoSetup()
{

    return ERR_NONE;
}

DsoErr servCounter::set_Ctr2_Tot_Clear()
{

    return ERR_NONE;
}

DsoErr servCounter::set_Ctr2_Tot_AutoSetup()
{

    return ERR_NONE;
}

DsoErr servCounter::set_Ctr2_Slop(bool b)
{
    m_Ctr2_Slop = b;
    return ERR_NONE;
}

bool servCounter::get_Ctr2_Slop()
{
    return m_Ctr2_Slop;
}

DsoErr servCounter::set_Ctr2_Gate(bool b)
{
    m_Ctr2_Gate = b;
    return ERR_NONE;
}

bool servCounter::get_Ctr2_Gate()
{
    return m_Ctr2_Gate;
}

DsoErr servCounter::set_Ctr2_Gate_Source(int v)
{
    m_Ctr2_Gate_Source = v;
    return ERR_NONE;
}

int servCounter::get_Ctr2_Gate_Source()
{
    return m_Ctr2_Gate_Source;
}

DsoErr servCounter::set_Ctr2_Gate_Polarity(bool b)
{
    m_Ctr2_Gate_Polarity = b;
    return ERR_NONE;
}

bool servCounter::get_Ctr2_Gate_Polarity()
{
    return m_Ctr2_Gate_Polarity;
}

DsoErr servCounter::set_Ctr2_Gate_Threshold(int v)
{
    m_Ctr2_Gate_Threshold = v;
    return ERR_NONE;
}

int servCounter::get_Ctr2_Gate_Threshold()
{
    return m_Ctr2_Gate_Threshold;
}

DsoErr servCounter::cfgCounter()
{
    EngineCounter counterCfg;
    DsoErr err = ERR_NONE;

    counterCfg.mSrc      = (Chan)mCtr1FreqSrc;
    counterCfg.mGateTime = mCtr1GateTime;

    if( m_Ctr1_Gate) //! TOT Gate On
    {
        counterCfg.mGateSrc = (Chan)m_Ctr1_Gate_Source;
    }
    else             //! TOT Gate Off
    {
        counterCfg.mGateSrc = counterCfg.mSrc;
    }

    if( (m_Ctr1_MeasType == TOTALIZE)||(m_Ctr1_Source == SRC_TRIG_EVENT))
    {
        counterCfg.mMode = counter_event;
    }
    else
    {
        if( NULL != m_CounterData)
        {
            if( m_CounterData->getMode() ==  counter_event) //! counter_event(Meas Period) Mode
            {
                counterCfg.mMode  = counter_event;
            }
            else  //! counter_freq(Meas Freq) Mode
            {
                counterCfg.mMode = counter_freq;
            }
        }
        else
        {
            counterCfg.mMode = counter_freq;
        }
    }

    if( m_Ctr1_Source == SRC_TRIG_EVENT)  //! Source: Trigger Event
    {
        counterCfg.mEvent = counter_event_trig;
    }
    else
    {
        counterCfg.mEvent = counter_event_edge;
    }

    if( m_Ctr1_Gate_Polarity)
    {
        counterCfg.mGate = counter_gate_low;
    }
    else
    {
        counterCfg.mGate = counter_gate_high;
    }

    counterCfg.mClear = false;

    err = postEngine(ENGINE_COUNTER_CFG, &counterCfg);
    LOG_DBG()<<"--------------------"<<err <<" = postEngine(ENGINE_COUNTER_CFG, &counterCfg)";

    return err;
}

void servCounter::cfgCounterMode(EngineCounterValue *val)
{
    Q_ASSERT( NULL != val );
    if( NULL != val)
    {
        if( m_Ctr1_MeasType != TOTALIZE) //! MeasType: FREQ/PERIOD
        {
            if( (val->mEdgeCnt <= 1)&&(val->signalWidth <= 0) ) //! Invalid
            {
                return;
            }

            if( NULL != m_CounterData)
            {
                CounterType type = m_CounterData->getType();
                CounterMode mode = m_CounterData->getMode();
                double dataVal = m_CounterData->getVal();

                //! Set to counter_event(Meas Period) Mode
                //! Freq <= 20Hz
                //! Period >= 50ms
                if(  ((type == FREQ)  &&(mode == counter_freq)&&(dataVal <= 20))
                   ||((type == PERIOD)&&(mode == counter_freq)&&(dataVal >= 50e-3)) )
                {
                    m_CounterData->setMode(counter_event);
                    cfgCounter();
                }

                //! Set to counter_freq(Meas Freq) Mode
                //! Freq > 20Hz
                //! Period < 50ms
                if(  ((type == FREQ)  &&(mode == counter_event)&&(dataVal > 20))
                   ||((type == PERIOD)&&(mode == counter_event)&&(dataVal < 50e-3)) )
                {
                    m_CounterData->setMode(counter_freq);
                    cfgCounter();
                }
            }
        }
    }
}

void *servCounter::getCounterData()
{
    /*! [dba: 2018-04-24] bug: 3092*/
    bool bCfgChange = false;
    CHECK_CFG_CHANGE(bCfgChange);
    if(bCfgChange)
    {
        qDebug()<<__FILE__<<__LINE__<<"counter cfg change. ....."<<m_cfgChangeTag;
        return NULL;
    }
    /***************************************/

    DsoErr engineErr;
    EngineCounterValue val;

    //if( (NULL != m_CounterData)&&(m_CounterData->getMode() == counter_freq) )

    //! Stop Counter
    postEngine(ENGINE_COUNTER_EN, false);

    //! Read Counter
    engineErr = queryEngine(qENGINE_COUNTER_VALUE, &val );

    //if( (NULL != m_CounterData)&&(m_CounterData->getMode() == counter_freq) )

    //! Run Counter
    postEngine(ENGINE_COUNTER_EN, true);

    if( engineErr == ERR_NONE)
    {
        //! calc counterData
        if( NULL != m_CounterData)
        {
            m_CounterData->calcData( &val, (m_Ctr1_Source == SRC_TRIG_EVENT) );
            cfgCounterMode(&val);

            return m_CounterData;
        }
        else
        {
            LOG_DBG()<<"***********m_CounterData == NULL";
            return NULL;
        }
    }
    else
    {
        LOG_DBG()<<"*************engineErr: "<< engineErr;
        return NULL;
    }
}
