#ifndef COUNTERDATA_H
#define COUNTERDATA_H
#include "../../engine/base/enginecfg.h"

#define Gate10ms 10e3      //! GateTime: 10,000us
#define Gate50ms 50e3      //! GateTime: 50,000us
#define Gate200ms 200e3    //! GateTime: 200,000us
#define Gate1000ms 1000e3  //! GateTime: 1000,000us

enum CounterType
{
    FREQ = 1,
    PERIOD,
    TOTALIZE,
};


enum CounterStatus
{
    CTR_Valid,
    CTR_Limit,
    CTR_No_Signal,
    CTR_Invalid,
};

class counterData
{
public:
    counterData(Chan src);

    void setType(CounterType type);
    void setSource(Chan src);
    void setGataTme(quint32 g);
    void setMode(CounterMode mode)   { m_Mode = mode;}
    void setMax();
    void setMin();
    void resetMaxMin();
    void setstatus(CounterStatus s);

    CounterType getType()       { return m_Type;}
    Chan getSource()            { return m_Src;}
    CounterMode getMode()       { return m_Mode;}

    void calcData(EngineCounterValue *val, bool isTrigEventSrc);
    void calcPeriod(EngineCounterValue *val);
    void calcFreq(EngineCounterValue *val);
    void calcTotalize(EngineCounterValue *val);

    //! check Signal(for counter_event Mode)
    //! true: Signal is valid
    //! false:Signal is invalid
    bool checkSignal( EngineCounterValue *val);

    double getVal()        { return m_Val;}
    double getMax()        { return m_Max;}
    double getMin()        { return m_Min;}
    CounterStatus status()  { return m_Status;}

private:
    Chan m_Src;
    CounterType m_Type;
    CounterMode m_Mode;
    quint32 m_GateTime;

    double m_Val;
    double m_Max;
    double m_Min;
    CounterStatus   m_Status;

    quint64 m_Old_signalWidth;   //! for checkSignal( EngineCounterValue *val)
    int     m_checkSignal_count; //! for checkSignal( EngineCounterValue *val)
};

#endif // COUNTERDATA_H
