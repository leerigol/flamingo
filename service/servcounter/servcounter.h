#ifndef SERVCOUNTER_H
#define SERVCOUNTER_H

#include "../service.h"
#include "../../baseclass/iserial.h"
#include "../service_msg.h"
#include "../service_name.h"
#include "../../include/dsotype.h"
#include "counterdata.h"

class servCounter : public serviceExecutor,
                    public ISerial
{
    Q_OBJECT

protected:
    DECLARE_CMD()

#define SRC_TRIG_EVENT 100


public:
    servCounter( QString name, ServiceId eId = E_SERVICE_ID_COUNTER );

    enum
    {
        cmd_base = 0,
        cmd_Get_CounterData,
        cmd_Set_Opt_CTR,

        cmd_scpi_counter_value,

    };

    int  serialOut(CStream &stream, serialVersion &ver);
    int  serialIn( CStream &stream, serialVersion ver );
    void rst();
    int  startup();
    void init();
    virtual void registerSpy();

    DsoErr set_Ctr1_Enable(bool b);
    bool   get_Ctr1_Enable();
    DsoErr set_Ctr1_Source(int v);
    int    get_Ctr1_Source();
    DsoErr set_Ctr1_MeasType(int v);
    int    get_Ctr1_MeasType();
    DsoErr set_Ctr1_Resolution(int v);
    int    get_Ctr1_Resolution();
    DsoErr set_Ctr1_Stat(bool b);
    bool   get_Ctr1_Stat();
    DsoErr set_Ctr1_Threshold(int v);
    int    get_Ctr1_Threshold();
    DsoErr set_Ctr1_AutoSetup();
    DsoErr set_Ctr1_Tot_Clear();
    DsoErr set_Ctr1_Tot_AutoSetup();
    DsoErr set_Ctr1_Slop(bool b);
    bool   get_Ctr1_Slop();
    DsoErr set_Ctr1_Gate(bool b);
    bool   get_Ctr1_Gate();
    DsoErr set_Ctr1_Gate_Source(int v);
    int    get_Ctr1_Gate_Source();
    DsoErr set_Ctr1_Gate_Polarity(bool b);
    bool   get_Ctr1_Gate_Polarity();
    DsoErr set_Ctr1_Gate_Threshold(int v);
    int    get_Ctr1_Gate_Threshold();

    DsoErr set_Ctr2_Enable(bool b);
    bool   get_Ctr2_Enable();
    DsoErr set_Ctr2_Source(int v);
    int    get_Ctr2_Source();
    DsoErr set_Ctr2_MeasType(int v);
    int    get_Ctr2_MeasType();
    DsoErr set_Ctr2_Resolution(int v);
    int    get_Ctr2_Resolution();
    DsoErr set_Ctr2_Stat(bool b);
    bool   get_Ctr2_Stat();
    DsoErr set_Ctr2_Threshold(int v);
    int    get_Ctr2_Threshold();
    DsoErr set_Ctr2_AutoSetup();
    DsoErr set_Ctr2_Tot_Clear();
    DsoErr set_Ctr2_Tot_AutoSetup();
    DsoErr set_Ctr2_Slop(bool b);
    bool   get_Ctr2_Slop();
    DsoErr set_Ctr2_Gate(bool b);
    bool   get_Ctr2_Gate();
    DsoErr set_Ctr2_Gate_Source(int v);
    int    get_Ctr2_Gate_Source();
    DsoErr set_Ctr2_Gate_Polarity(bool b);
    bool   get_Ctr2_Gate_Polarity();
    DsoErr set_Ctr2_Gate_Threshold(int v);
    int    get_Ctr2_Gate_Threshold();

    //! Config Counter Mode:
    //! counter_event(Meas Period) Mode or counter_freq(Meas Freq) Mode
    //! if MeasType == TOTALIZE, do nothing
    void   cfgCounterMode(EngineCounterValue *val);
    DsoErr cfgCounter();
    void *getCounterData();

    DsoErr onActive();
    void set_Opt_CTR();

    //!scpi
    QString getCounterValue();

private:
    bool m_Ctr1_Enable;
    int  m_Ctr1_Source;
    int  m_Ctr1_MeasType;
    int  m_Ctr1_Resolution;
    bool m_Ctr1_Stat;
    int  m_Ctr1_Threshold;
    bool m_Ctr1_Slop;
    bool m_Ctr1_Gate;
    int  m_Ctr1_Gate_Source;
    bool m_Ctr1_Gate_Polarity;
    int  m_Ctr1_Gate_Threshold;

    bool m_Ctr2_Enable;
    int  m_Ctr2_Source;
    int  m_Ctr2_MeasType;
    int  m_Ctr2_Resolution;
    bool m_Ctr2_Stat;
    int  m_Ctr2_Threshold;
    bool m_Ctr2_Slop;
    bool m_Ctr2_Gate;
    int  m_Ctr2_Gate_Source;
    bool m_Ctr2_Gate_Polarity;
    int  m_Ctr2_Gate_Threshold;

    int     mCtr1FreqSrc;
    quint32 mCtr1GateTime;

    int     mCtr2FreqSrc;
    quint32 mCtr2GateTime;

    counterData *m_CounterData;

    bool m_LicenseValid;

    int  m_cfgChangeTag;
};

#endif // SERVCOUNTER_H
