#-------------------------------------------------
#
# Project created by QtCreator 2017-03-08T17:37:06
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += widgets
QT       += network

TEMPLATE = lib
CONFIG += staticlib
TARGET = ../../lib$$(PLATFORM)/services/servcounter
INCLUDEPATH += .

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

#DEFINES += _DEBUG

# Input
SOURCES += servcounter.cpp \
    counterdata.cpp

HEADERS += servcounter.h \
    counterdata.h
