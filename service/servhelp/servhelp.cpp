
#include "../service_name.h"
#include "../../meta/crmeta.h"
#include "../servstorage/servstorage.h"
#include "../servcal/servcal.h"
#include "../servutility/servutility.h"
#include "servhelp.h"

#define dbg_out()


IMPLEMENT_CMD( serviceExecutor, servHelp )
start_of_entry()


on_set_void_void(   MSG_APP_HELP_ABOUT,              &servHelp::setAbout),

on_set_int_int(     MSG_APP_HELP_LANGUAGE,           &servHelp::setLanguage),
on_get_int(         MSG_APP_HELP_LANGUAGE,           &servHelp::getLanguage),

on_set_int_void(    MSG_APP_HELP_OPTION,             &servHelp::showOption),

on_set_int_int(     MSG_OPT_HELP_INFO,               &servHelp::showOptionHelp),
on_get_int(         MSG_OPT_HELP_INFO,               &servHelp::getOptionHelp),

on_set_int_void(    MSG_APP_HELP_SETUP_OPT,          &servHelp::setSetupOpt ),

on_set_int_void(    MSG_APP_HELP_HELP,               &servHelp::setHelp),
on_get_bool(        MSG_APP_HELP_HELP,               &servHelp::getHelp),

on_set_int_void(    MSG_APP_HELP_CHECK_NEW,          &servHelp::onLineUpgrade),
on_get_bool(        MSG_APP_HELP_CHECK_NEW,          &servHelp::getOnLineUpgrade),

on_set_int_int(     servHelp::cmd_online_upgrade,    &servHelp::checkNewFirmware),
on_set_int_int(     servHelp::cmd_online_upgradeWnd, &servHelp::onLineUpgradeWnd),

on_set_int_void(    MSG_APP_HELP_UPGRADE,            &servHelp::onUpgrade),
on_set_int_void(    servHelp::cmd_usb_upgrade,       &servHelp::doUpgrade),

on_set_int_bool(    servHelp::cmd_lan_status,        &servHelp::onNetStatus),
on_set_int_bool(    servHelp::cmd_usb_status,        &servHelp::onUsbStatus),
on_set_int_bool(    servHelp::cmd_opt_file_status,   &servHelp::onOptFileStatus),
on_set_int_void(    servHelp::cmd_opt_show,          &servHelp::onOptShow),
/*Used for webcontrol only*/
on_get_string(      servHelp::cmd_opt_show,          &servHelp::getOption),

on_set_int_void(    CMD_SERVICE_ACTIVE,              &servHelp::setActive),
on_get_bool(        CMD_SERVICE_ACTIVE,              &servHelp::getAppShow),

on_set_int_obj(     CMD_SERVICE_HELP_REQUESST,       &servHelp::testHelpRequest),

on_set_int_pointer( servHelp::cmd_help_context,      &servHelp::setHelpContext),
on_get_pointer(     servHelp::cmd_help_context,      &servHelp::getHelpContext),

on_get_int(         servHelp::cmd_help_history,      &servHelp::searchHistoryMenu),

end_of_entry()

servHelp::servHelp( QString name, ServiceId eId ) : serviceExecutor( name, eId )
{
    serviceExecutor::baseInit();

    isAppShow    = false;
    mInited      = false;
    mPreActiveId = -1;
    mOptNum = 0;

    mCurrServContext.servMsg = MSG_CHAN_ACTIVE;
    mCurrServContext.servName = serv_name_ch1;

    mUiAttr.setEnable(MSG_APP_HELP_UPGRADE, false);
    mUiAttr.setEnable(MSG_APP_HELP_CHECK_NEW, false);
    mUiAttr.setEnable(MSG_APP_HELP_SETUP_OPT, false);
}

DsoErr servHelp::start()
{

    return ERR_NONE;
}

void servHelp::registerSpy()
{
}

void servHelp::setAbout()
{
    serviceExecutor::post( serv_name_utility,
                           MSG_APP_UTILITY_SYSTEMINFO, 0);
}

DsoErr servHelp::setLanguage(SystemLanguage lang)
{
    serviceExecutor::post( serv_name_utility,
                           MSG_APP_UTILITY_LANGUAGE,
                           lang );

    return ERR_NONE;
}

SystemLanguage servHelp::getLanguage()
{
    return sysGetLanguage();
}

DsoErr servHelp::showOption()
{
    return ERR_NONE;
}

DsoErr servHelp::showOptionHelp(int opt)
{
    mOptNum = opt;
    return ERR_NONE;
}

int servHelp::getOptionHelp()
{
    return mOptNum;
}

QString servHelp::getOption()
{
    QString str = "";
    void* p = NULL;

    serviceExecutor::query( serv_name_license, servLicense::cmd_get_OptInfo, p);
//    qDebug() << "servHelp::getOption() query(servLicense::cmd_get_OptInfo)";

    QMap<OptType, COptInfo*> *optionList = (QMap<OptType, COptInfo*> *)p;
    if(optionList)
    {
        QString expire;
        foreach(COptInfo* info, *optionList)
        {
            if( info->isEnable())
            {
                if( LicTime_Forever == info->getLicenseTime())
                {
                    expire =  "Forever";
                }
                else if(LicTime_Limit == info->getLicenseTime())
                {
                    expire =  "Limit";
                }
                else
                {
                    expire = "3600 Minutes";
                }
            }
            else
            {
                expire = "----";
            }

            if( str.length() > 0 )
            {
                str = str + "#";
            }

            //QString abc = QString("可以显示中文了");
            str = str +
                    info->getName() + "$" +
                    expire + "$" +
                    sysGetString( info->getInfoID() );
//            qDebug() << str;
        }
    }
    return str;
}

DsoErr servHelp::setSetupOpt()
{
    post( serv_name_license,
          servLicense::cmd_USB_OptSetup, 0);
    return ERR_NONE;
}

DsoErr servHelp::setHelp()
{
    if( isAppShow )
    {
        isAppShow = false;
        sysSetWorkMode( work_normal );
    }
    else
    {
        isAppShow = true;
        sysSetWorkMode( work_help );

        //! jump to pre
        if ( mPreActiveId > E_SERVICE_ID_NONE )
        {
            serviceExecutor::post( mPreActiveId,
                                   CMD_SERVICE_ACTIVE, 1 );
        }
    }

    return ERR_NONE;
}

bool servHelp::getHelp()
{
    return isAppShow;
}

DsoErr servHelp::onLineUpgrade()
{
    if(getAppShow()) return ERR_NONE;

    async(servHelp::cmd_online_upgradeWnd,0);
    setuiEnable(false);

    return ERR_NONE;
}

bool servHelp::getOnLineUpgrade()
{
    return mUiAttr.getItem(MSG_APP_HELP_CHECK_NEW)->getEnable();
}

bool servHelp::getAppShow()
{
    return isAppShow;
}

DsoErr servHelp::setActive( )
{
    int preId;

    preId = serviceExecutor::getActiveService();
    //! not self
    if ( preId > E_SERVICE_ID_NONE &&
         preId != getId() )
    {
        mPreActiveId = preId;
    }

    return serviceExecutor::setActive( isAppShow );
}

DsoErr servHelp::testHelpRequest( CHelpRequest *pReq )
{
    Q_ASSERT( NULL != pReq );
    mCurrServContext.servMsg    = pReq->getMsg();
    mCurrServContext.servName  = pReq->getName();
    mCurrServContext.mValid = pReq->getOption(mCurrServContext.mOpt);

    //qDebug()<<mCurrServContext.servName<<mCurrServContext.servMsg;
    return ERR_NONE;
}

void * servHelp::getHelpContext()
{
    return &mCurrServContext;
}

DsoErr servHelp::setHelpContext( pointer *context )
{
    Q_ASSERT( NULL != context );
    mCurrServContext = *((HelpContext*)context);
    return ERR_NONE;
}

int servHelp::searchHistoryMenu()
{
    if(history().count()){

        for(int i=history().count()-1; i>=0; i--){
            QString servName = getServiceName(history().at(i).servId);
            if(servName.compare(serv_name_help,Qt::CaseInsensitive)){
                int b;
                serviceExecutor::query(servName ,history().at(i).servMsg,  b);

                mCurrServContext.servName  = servName;
                mCurrServContext.servMsg    = history().at(i).servMsg;
                mCurrServContext.mValid = true;
                mCurrServContext.mOpt   = b;

                history().clear();
                break;
            }
        }
    }
    return ERR_NONE;
}


DsoErr servHelp::checkNewFirmware(int val)
{
    if(getAppShow()) return ERR_NONE;

    if(val)
    {
        mUiAttr.setEnable(MSG_APP_HELP_CHECK_NEW, false);
        post(serv_name_storage, MSG_CHECKING_FIRMWARE, false);
    }
    else
    {
        mUiAttr.setEnable(MSG_APP_HELP_CHECK_NEW, true);
    }

    return ERR_NONE;
}

DsoErr servHelp::onLineUpgradeWnd()
{
    return ERR_NONE;
}

DsoErr servHelp::onUpgrade()
{
    if(getAppShow()) return ERR_NONE;

    servGui::showMsgBox(this->getName(),
                        sysGetString(INF_BEFORE_UPDATE,"Upgrade attention"),
                        servHelp::cmd_usb_upgrade);

    return ERR_NONE;

}

DsoErr servHelp::doUpgrade()
{
    if(getAppShow()) return ERR_NONE;

    post(serv_name_storage, servStorage::FUNC_UPGRADE_USB, 0);

    onUsbStatus(false);
    mUiAttr.setEnable(MSG_APP_UTILITY_LANGUAGE, false);
    return ERR_NONE;
}

int servHelp::onNetStatus(bool ready)
{
    mUiAttr.setEnable( MSG_APP_HELP_CHECK_NEW, ready);
    return ERR_NONE;
}

int servHelp::onUsbStatus(bool ready)
{
    if(ready && isActive() == false)
    {
        /* DoNot open help menu when calibration*/
        int stat;
        query( serv_name_cal, servCal::qCMD_CAL_STATUS, stat );
        if ( stat != servCal::cal_running &&
             !servUtility::getSystemKeyLocker())
        {
            serviceExecutor::setActive(1);
        }
    }

    //qDebug() << "usb remove or insert:" << ready;
    mUiAttr.setEnable(MSG_APP_HELP_UPGRADE, ready);

    mUiAttr.setEnable(MSG_APP_UTILITY_LANGUAGE, true);

    return ERR_NONE;
}

int servHelp::onOptFileStatus(bool ready)
{
    //for bug2393
    if(ready && isActive() == false)
    {
        /* DoNot open help menu when calibration*/
        int stat;
        query( serv_name_cal, servCal::qCMD_CAL_STATUS, stat );
        if ( stat != servCal::cal_running &&
             !servUtility::getSystemKeyLocker() )
        {
            serviceExecutor::setActive(1);
        }
    }
    mUiAttr.setEnable(MSG_APP_HELP_SETUP_OPT, ready);
    return ERR_NONE;
}

int servHelp::onOptShow()
{
    return ERR_NONE;
}
