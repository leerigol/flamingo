#ifndef SERVHELP_H
#define SERVHELP_H

#include "../service.h"
#include "../service_msg.h"
#include "../servdso/sysdso.h"
#include "../../baseclass/chelprequest.h"
class HelpContext
{
public:
    QString servName;
    int     servMsg;
    int     mOpt;
    bool    mValid;

    QString mhlpPath;
    QString mPicPath;

    HelpContext &operator=(const HelpContext &other){
        this->servName     = other.servName;
        this->servMsg       = other.servMsg;
        this->mValid    = other.mValid;
        this->mOpt      = other.mOpt;
        this->mhlpPath  = other.mhlpPath;
        this->mPicPath  = other.mPicPath;
        return *this;
    }
};


class servHelp : public serviceExecutor
{
    Q_OBJECT
public:
    enum helpCmd
    {
        cmd_none = 0,
        cmd_help_context,
        cmd_help_history,
        cmd_lan_status,
        cmd_usb_status,
        cmd_usb_upgrade,

        cmd_opt_file_status,
        cmd_opt_show,
        cmd_opt_redraw,

        cmd_opt_help_show,

        cmd_online_upgrade,
        cmd_online_upgradeWnd,
    };

protected:
    DECLARE_CMD()

public:
    servHelp( QString name, ServiceId eId = E_SERVICE_ID_NONE );

    DsoErr start();
    void   registerSpy();

    void    setAbout();

    DsoErr  setLanguage(SystemLanguage lang);
    SystemLanguage getLanguage();

    DsoErr  showOption();
    QString getOption();

    DsoErr  showOptionHelp(int opt);
    int         getOptionHelp();

    DsoErr setSetupOpt();

    DsoErr  setHelp();
    bool    getHelp();

    DsoErr  onLineUpgrade();
    bool    getOnLineUpgrade();

    DsoErr  checkNewFirmware(int val);
    DsoErr  onLineUpgradeWnd();
    DsoErr  onUpgrade();
    DsoErr  doUpgrade();


    int     onNetStatus(bool);
    int     onUsbStatus(bool);
    int     onOptFileStatus(bool);
    int     onOptShow();

private:
    SystemLanguage  mCurrLang;
    bool            isAppShow;
    int mPreActiveId;
    int mOptNum;

private:
    DsoErr  testHelpRequest( CHelpRequest *request );

    void    *getHelpContext();
    DsoErr  setHelpContext(pointer *context );

    int     searchHistoryMenu();

    bool    getAppShow();

    DsoErr  setActive();

    void    getHelpPathOfChinese(QString servName);
    void    getHelpPathOfEnglish(QString servName);

private:
    HelpContext     mCurrServContext;

    bool            mInited;
    QString         mChinese;
    QString         mEnglish;

    QMap<QString, QString>  mMapFile;
    QMap<QString, QString>  mMapPic;

};

#endif // SERVHELP_H
