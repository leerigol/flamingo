#include "cdsoplayengine.h"

CDsoPlayEngine::CDsoPlayEngine()
{
    mName="CDsoPlayEngine";
}

DsoErr CDsoPlayEngine::exec( const struEngineMsgMap *pMap,
                             const struEngineMsgEntry *pEntry,
                             EngineMsg msg,
                             EnginePara& para,
                             void *outPtr,
                             ServiceId sId  )
{
    DsoErr err;

    //! in playback now
    if ( pMap == getMsgMap() )
    {    
        preDoProc( msg, sId );

        int rawMsg = unpackSMsg( msg );
        //! write
        if ( wr(msg) == Engine_Write && rawMsg!= ENGINE_TMO)
        {
            //! 防止按下stop按钮时autoengine产生的超时定时器
            m_ConfigId.addSysConfigId();
        }

        err = doProc( pEntry,
                       pEntry->proc,
                       pEntry->paraType,
                       para,
                       sId
                        );

        postDoProc( msg, sId );
    }
    else
    {
        err = dsoEngine::exec( pMap,
                               pEntry,
                               msg,
                               para,
                               outPtr,
                               sId );
    }

    dsoEngine::m_pPhy->flushWCache();

    return err;
}

DsoErr CDsoPlayEngine::onExit()
{
    return ERR_NONE;
}

//! save history
DsoErr CDsoPlayEngine::onEnter()
{
    //! no noise && persist
    m_pPhy->mWpu.setpersist_mode( 0 );

    //! screen persist
    if ( m_pViewSession->m_display.getScreenPersist() &&
         m_pViewSession->m_acquire.m_eTimeMode != Acquire_SCAN )
    {
        dsoEngine::m_playTraceTimer->start( PLAY_TRACE_TIME);
    }
    else
    {
        dsoEngine::m_postPlayTimer->start( POST_PLAY );
    }
    return ERR_NONE;
}

void CDsoPlayEngine::postDoProc( EngineMsg msg, ServiceId id )
{
    int rawMsg = unpackSMsg( msg );

    //! filter write
    if ( wr(msg) == Engine_Write )
    {}
    else
    { return; }

    //! start playback
    if ( rawMsg != ENGINE_TMO
         && rawMsg != ENGINE_REQUEST_TRACE )
    {
        dsoEngine::m_postPlayTimer->start( POST_PLAY );
    }

    //! hori config in play
    if ( id == E_SERVICE_ID_HORI )
    {
        EngineHoriReq hReq;

        queryHoriInfo( &hReq );

        //! emit info
        emit sigHoriInfo( hReq );
    }
}

//! play back a frame
DsoErr CDsoPlayEngine::playback(bool playWpu)
{
    DsoErr err;

    err = dsoEngine::playback(
                         m_pHistSession,
                         m_pViewSession,
                         playWpu);

    if ( err != ERR_NONE ) return err;

    return ERR_NONE;
}

void CDsoPlayEngine::onTimeout( int id )
{
    if ( id == POST_PLAY_ID )
    {
        //! add_by_zx for bug 2356
        m_ConfigId.addSysConfigId();
        m_pPhy->mScuGp.outCONFIG_ID(m_ConfigId.getConfigId(),-1);
        dsoEngine::m_postPlayTimer->stop();
        playback();
    }
    else if ( id == PLAY_TRACE_ID)
    {
        //! add_by_zx for bug 2356
        m_ConfigId.addSysConfigId();
        m_pPhy->mScuGp.outCONFIG_ID(m_ConfigId.getConfigId(),-1);
        dsoEngine::m_playTraceTimer->stop();
        dsoEngine::playback( m_pHistSession,
                             m_pViewSession,
                             false);
    }

    emit(sigConfigFinished());
}
