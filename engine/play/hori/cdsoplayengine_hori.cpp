
#include "../cdsoplayengine.h"

DsoErr CDsoPlayEngine::setMainScale( qlonglong scale )
{
    //! check scale
    Q_ASSERT( NULL != m_pHistSession );
    Q_ASSERT( NULL != m_pViewSession );

    if(needCheckParam(m_pHistSession, m_pViewSession))
    {
        //! ch view
        if ( validatePlayScale( scale, m_pHistSession ) )
        {}
        else
        {
            return ERR_HORI_SCALE;
        }
    }
    dsoEngine::setMainScale( scale );

    return ERR_NONE;
}
DsoErr CDsoPlayEngine::setZoomScale( qlonglong scale )
{
    //! check scale
    Q_ASSERT( NULL != m_pHistSession );
    Q_ASSERT( NULL != m_pViewSession );

    if( needPlay( m_pHistSession, m_pViewSession ) )
    {
        //! ch view
        if ( validatePlayScale( scale, m_pHistSession ) )
        {}
        else
        { return ERR_HORI_SCALE; }
    }

    dsoEngine::setZoomScale( scale );

    return ERR_NONE;
}

DsoErr CDsoPlayEngine::queryHoriInfo( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    //! valid history
    if ( m_pHistSession->getValid() )
    {}
    //! invalid by meta
    else
    {
        //! input
        int chCnt;
        chCnt = toChCnt( m_pViewSession->getSaCHBm() );

        pInfo->setInfo( m_pViewSession->m_horizontal.getMainView().getScale(),
                        m_pViewSession->m_horizontal.getMainView().getOffset(),
                        m_pViewSession->m_horizontal.getMainView().getScale(),
                        m_pViewSession->m_acquire.getChDepth(),
                        chCnt );
        return dsoEngine::queryHoriInfo( pInfo );
    }

    //! history system
    pInfo->mChSa = m_pHistSession->m_acquire.mCHFlow.getSa();
    pInfo->mChDotTime = m_pHistSession->m_acquire.mCHFlow.getDotTime();

    pInfo->mLaSa = m_pHistSession->m_acquire.mLaFlow.getSa();
    pInfo->mLaDotTime = m_pHistSession->m_acquire.mLaFlow.getDotTime();

    pInfo->mChLength = m_pHistSession->m_acquire.mCHFlow.getDepth();
    pInfo->mLaLength = m_pHistSession->m_acquire.mLaFlow.getDepth();

    pInfo->mScale = m_pHistSession->m_acquire.mCHFlow.mSaScale;

    //! 50*div ,1s
    pInfo->mOffMax = qMax( time_s(1), 50*pInfo->mScale );
    //! -50*div, -1s
    pInfo->mOffMin = qMin( -time_s(1), -50*pInfo->mScale );

    return ERR_NONE;
}

