#ifndef CDSOPLAYENGINE_H
#define CDSOPLAYENGINE_H

#include "../base/dsoengine.h"

/*!
 * \brief The CDsoPlayEngine class
 * 播放引擎
 */
class CDsoPlayEngine : public dsoEngine
{
    DECLARE_MSG_MAP()

public:
    CDsoPlayEngine();

public:
    virtual DsoErr exec( const struEngineMsgMap *pMap,
                         const struEngineMsgEntry *pEntry,
                         EngineMsg msg,
                         EnginePara& para,
                         void *outPtr,
                         ServiceId sId = E_SERVICE_ID_NONE );

    virtual DsoErr onExit();
    virtual DsoErr onEnter();

    virtual void postDoProc( EngineMsg /*msg*/, ServiceId /*id*/ );

protected:
    void onTimeout( int id );

public:
    //! hori
    DsoErr setMainScale( qlonglong scale );
    DsoErr setZoomScale( qlonglong scale );

    //! trace
    virtual DsoErr requestTrace( traceRequest &req,
                                 int tmo=2000 );
public:
    DsoErr queryHoriInfo( EngineHoriInfo *pInfo );

protected:
    DsoErr playback(bool playWpu = true);

};

#endif // CDSOPLAYENGINE_H
