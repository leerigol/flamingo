#include "cplatform.h"

#include "./record/cdsorecengine.h"
#include "./play/cdsoplayengine.h"
#include "./autostop/cdsoautostopengine.h"

cplatform *cplatform::_dsoPlatform=NULL;
cplatform *cplatform::sysGetDsoPlatform()
{
    Q_ASSERT( NULL != cplatform::_dsoPlatform );
    return cplatform::_dsoPlatform;
}

cplatform *cplatform::createInstance()
{
    if ( cplatform::_dsoPlatform != NULL )
    { return cplatform::_dsoPlatform; }
    else
    {
        cplatform::_dsoPlatform = new cplatform();
        Q_ASSERT( NULL != cplatform::_dsoPlatform );
        return cplatform::_dsoPlatform;
    }
}

cplatform::cplatform()
{
    m_pRecEngine = NULL;
    m_pPlayEngine = NULL;
    m_pAutoStopEngine = NULL;

    m_pPhy = NULL;
    m_pBus = NULL;
}

cplatform::~cplatform()
{
    Q_ASSERT( m_pBus != NULL );
    m_pBus->close();

    delete( m_pRecEngine );
    delete( m_pPlayEngine );
    delete( m_pAutoStopEngine );

    delete( m_pPhy );
    delete( m_pBus );
}

/*!
 * \brief cplatform::buildPlatform
 * \return
 * create the objects within the platform
 */
DsoErr cplatform::buildPlatform()
{
    dsoEngine::init();

    //! engines
    m_pRecEngine = new CDsoRecEngine();
    Q_ASSERT( NULL != m_pRecEngine );
    m_pPlayEngine = new CDsoPlayEngine();
    Q_ASSERT( NULL != m_pPlayEngine );
    m_pAutoStopEngine = new CDsoAutoStopEngine();
    Q_ASSERT( NULL != m_pAutoStopEngine );

    //! bus
    m_pBus = new dso_phy::IDsoBus();
    Q_ASSERT( NULL != m_pBus );

    //! phy mgr
    m_pPhy = new dso_phy::CDsoPhy();
    Q_ASSERT( NULL != m_pPhy );

    //! append bus
    m_pPhy->appendBus( &m_pBus->mCpldBus );
    m_pPhy->appendBus( &m_pBus->mCpldBus );
    m_pPhy->appendBus( &m_pBus->mAxiBus );
    m_pPhy->appendBus( &m_pBus->mMemBus );

    //! attach phy and bus
    m_pPhy->mBeeper.attachBus( &m_pBus->mCpldBus );
    m_pPhy->mLcd.attachBus( &m_pBus->mCpldBus );
    m_pPhy->mFan.attachBus( &m_pBus->mCpldBus );
    m_pPhy->mBoard.attachBus( &m_pBus->mCpldBus );

    m_pPhy->mAdc[0].attachBus( &m_pBus->mCpldBus );
    m_pPhy->mAdc[1].attachBus( &m_pBus->mCpldBus );
    m_pPhy->mPll.attachBus( &m_pBus->mCpldBus );

    m_pPhy->mVGA[0].attachBus( &m_pBus->mCpldBus );
    m_pPhy->mVGA[1].attachBus( &m_pBus->mCpldBus );
    m_pPhy->mVGA[2].attachBus( &m_pBus->mCpldBus );
    m_pPhy->mVGA[3].attachBus( &m_pBus->mCpldBus );

    //! dg
    m_pPhy->mRelay.attachBus( &m_pBus->mCpldBus );
    m_pPhy->mDgu.attachBus( &m_pBus->mAxiBus );

    //! axi bus
    m_pPhy->mWpu.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mTpu.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mSearch.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mSearch.attachBus( &m_pBus->mMemBus );

    m_pPhy->mSpu.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mScu.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mSpu2.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mScu2.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mCcu.attachBus( &m_pBus->mAxiBus );

    m_pPhy->mGtu.attachBus( &m_pBus->mAxiBus );

    m_pPhy->mDac.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mMisc.attachBus( &m_pBus->mAxiBus );

    m_pPhy->mMeasCfg.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mMeasRet.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mCounter.attachBus( &m_pBus->mAxiBus );

    //! trace
    m_pPhy->m_trace.attachBus( &m_pBus->mAxiBus );
    m_pPhy->m_trace.attachBus( &m_pBus->mMemBus );

    //! horizontal
    m_pPhy->m_hori.attachBus( &m_pBus->mAxiBus );

    //! mask
    m_pPhy->mMask.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mMask.attachSpuGp( &m_pPhy->mSpuGp );

    //! record
    m_pPhy->mRecPlay.attachBus( &m_pBus->mAxiBus );

    //! 1wire
    m_pPhy->m1WireProbe[0].attachBus( &m_pBus->mAxiBus );
    m_pPhy->m1WireProbe[1].attachBus( &m_pBus->mAxiBus );
    m_pPhy->m1WireProbe[2].attachBus( &m_pBus->mAxiBus );
    m_pPhy->m1WireProbe[3].attachBus( &m_pBus->mAxiBus );

    //! zynq fcu
    m_pPhy->mZynqFcuWr.attachBus( &m_pBus->mAxiBus );
    m_pPhy->mZynqFcuRd.attachBus( &m_pBus->mAxiBus );

    //! function phy
    m_pPhy->m_ch[0].attachAfe( &(m_pPhy->mVGA[0]) );
    m_pPhy->m_ch[1].attachAfe( &(m_pPhy->mVGA[1]) );
    m_pPhy->m_ch[2].attachAfe( &(m_pPhy->mVGA[2]) );
    m_pPhy->m_ch[3].attachAfe( &(m_pPhy->mVGA[3]) );

    m_pPhy->m_ch[0].attachDac( &(m_pPhy->mDac), 10 );
    m_pPhy->m_ch[1].attachDac( &(m_pPhy->mDac), 11 );
    m_pPhy->m_ch[2].attachDac( &(m_pPhy->mDac), 12 );
    m_pPhy->m_ch[3].attachDac( &(m_pPhy->mDac), 13 );

    m_pPhy->m_ch[0].attachWpu( &(m_pPhy->mWpu) );
    m_pPhy->m_ch[1].attachWpu( &(m_pPhy->mWpu) );
    m_pPhy->m_ch[2].attachWpu( &(m_pPhy->mWpu) );
    m_pPhy->m_ch[3].attachWpu( &(m_pPhy->mWpu) );

    m_pPhy->m_ch[0].attachRelay( &(m_pPhy->mRelay) );
    m_pPhy->m_ch[1].attachRelay( &(m_pPhy->mRelay) );
    m_pPhy->m_ch[2].attachRelay( &(m_pPhy->mRelay) );
    m_pPhy->m_ch[3].attachRelay( &(m_pPhy->mRelay) );

    //! hori
    m_pPhy->m_hori.attachSpuGp( &m_pPhy->mSpuGp );
    m_pPhy->m_hori.attachCcu( &m_pPhy->mCcu );
    m_pPhy->m_hori.attachFcuW( &(m_pPhy->mZynqFcuWr) );
    m_pPhy->m_hori.attachWpu( &(m_pPhy->mWpu) );

    //! recPlay
    m_pPhy->mRecPlay.attachSpuGp( &m_pPhy->mSpuGp );
    m_pPhy->mRecPlay.attachScuGp( &m_pPhy->mScuGp );
    m_pPhy->mRecPlay.attachCcu( &m_pPhy->mCcu );

    //! ext
    m_pPhy->mExt.attachDac( &(m_pPhy->mDac), 14 );

    //! la
    m_pPhy->mLa[0].attachDac( &(m_pPhy->mDac), 0 );
    m_pPhy->mLa[1].attachDac( &(m_pPhy->mDac), 1 );

    //! probe1-4
    m_pPhy->mProbe[0].attachDac( &(m_pPhy->mDac), 6 );
    m_pPhy->mProbe[1].attachDac( &(m_pPhy->mDac), 7 );
    m_pPhy->mProbe[2].attachDac( &(m_pPhy->mDac), 8 );
    m_pPhy->mProbe[3].attachDac( &(m_pPhy->mDac), 9 );

    //! dg offset && ref
    m_pPhy->mDgOffset[0].attachDac( &(m_pPhy->mDac), 2 );
    m_pPhy->mDgOffset[1].attachDac( &(m_pPhy->mDac), 4 );

    m_pPhy->mDgRef[0].attachDac( &(m_pPhy->mDac), 3 );
    m_pPhy->mDgRef[1].attachDac( &(m_pPhy->mDac), 5 );

    //! pll
    m_pPhy->mPll.attachDac( &(m_pPhy->mDac), 15 );

    //! dg
    m_pPhy->mDgu.attatchRelay( &(m_pPhy->mRelay) );
    m_pPhy->mDgu.attachDac( &(m_pPhy->mDac) );

    //! trace
    m_pPhy->m_trace.attachZynqFcu( &m_pPhy->mZynqFcuWr,
                                   &m_pPhy->mZynqFcuRd );
    m_pPhy->m_trace.attachSpu( &m_pPhy->mSpuGp );
    m_pPhy->m_trace.attachScu( &m_pPhy->mScuGp );
    m_pPhy->m_trace.attachWpu( &m_pPhy->mWpu );
    m_pPhy->m_trace.attachCcu( &m_pPhy->mCcu );

    return ERR_NONE;
}

/*!
 * \brief cplatform::initPlatform
 * \return
 * -connect the objects in platform
 * \todo dynamic cast
 */
DsoErr cplatform::initPlatform()
{
    dsoEngine::setPhy( m_pPhy );

    CDsoRecEngine *pRecEng;
    CDsoPlayEngine *pPlayEng;

    //! use dynamic cast
    pRecEng = (CDsoRecEngine*)m_pRecEngine;
    pPlayEng = (CDsoPlayEngine*)m_pPlayEngine;

    Q_ASSERT( NULL != pRecEng );
    Q_ASSERT( NULL != pPlayEng );

    dsoEngine::setPlayEngine( m_pPlayEngine );
    dsoEngine::setRecEngine( m_pRecEngine );
    dsoEngine::setAutoStopEngine( m_pAutoStopEngine );

    //! active engine
    engine::setActiveEngine( m_pPlayEngine );

    //! open bus
    Q_ASSERT( m_pBus != NULL );
    m_pBus->open();

    //! check bus
    if ( m_pBus->checkReady() )
    {
    }
    else
    {
        qDebug() << "GTP Reset failed";
        Q_ASSERT( false );
    }


    //! rst the phy
    if ( sysHasArg("-noinit"))
    {
    }
    else
    {
        Q_ASSERT( NULL != m_pPhy );
        m_pPhy->init();
    }

    //!!!!!!Test
//    m_pPhy->mBoard.setRePower();
    return ERR_NONE;
}

DsoErr cplatform::deInitPlatform()
{ return ERR_NONE; }

void cplatform::movetoThread( QThread *pThread )
{
    m_pRecEngine->moveToThread( pThread );
    m_pPlayEngine->moveToThread( pThread );
}

dsoEngine *cplatform::getRecEngine()
{
    return m_pRecEngine;
}
dsoEngine *cplatform::getPlayEngine()
{
    return m_pPlayEngine;
}
dsoEngine *cplatform::getAutoStopEngine()
{
    return m_pAutoStopEngine;
}
dsoEngine *cplatform::getActiveEngine()
{
    return (dsoEngine*)engine::getActiveEngine();
}
