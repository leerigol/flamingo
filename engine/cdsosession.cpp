#include "cdsosession.h"

CDsoSession::CDsoSession()
{
    m_RecDone = false;
}

CDsoSession::CDsoSession( const CDsoSession &ses )
{
    *this = ses;
}

bool CDsoSession::m_bPlayValid = false;

CDsoSession & CDsoSession::operator=( const CDsoSession &ses )
{
    Q_ASSERT( &ses != this );

    for ( int i = 0; i < array_count(m_ch); i++ )
    { m_ch[i] = ses.m_ch[i]; }

    mLa = ses.mLa;

    m_horizontal = ses.m_horizontal;
    m_acquire = ses.m_acquire;
    m_control = ses.m_control;
    m_sequence = ses.m_sequence;

    m_trigger = ses.m_trigger;
    m_display = ses.m_display;
    m_decode = ses.m_decode;
    m_mask = ses.m_mask;

    m_measure = ses.m_measure;
    m_search = ses.m_search;
    m_RecDone = ses.m_RecDone;

    m_bNeedPlayClear = ses.m_bNeedPlayClear;
    m_bFastTimeBase = ses.m_bFastTimeBase;
    return *this;
}

void CDsoSession::setValid( bool b )
{
    m_acquire.setSaValid( b );
}
bool CDsoSession::getValid()
{
    return m_acquire.getSaValid();
}

bool CDsoSession::getSaCHValid( Chan ch )
{
    //! analog
    if ( ch >= chan1 && ch <= chan4 )
    { return m_ch[ ch - chan1 ].getOnOff(); }
    //! eye
    else if ( ch >= eye_ch1 && ch <= eye_ch4 )
    { return m_ch[ ch - eye_ch1 ].getOnOff(); }
    //! digi H
    else if ( ch >= digi_ch1 && ch <= digi_ch4 )
    { return m_ch[ ch - digi_ch1 ].getOnOff(); }
    //! digi L
    else if ( ch >= digi_ch1_l && ch <= digi_ch4_l )
    { return m_ch[ ch - digi_ch1_l ].getOnOff(); }
    //! la
    else if ( ch >= d0 && ch <= d15 )
    {   return mLa.getOnOff(); }
    else if ( ch == la )
    {   return mLa.getOnOff(); }
    else
    { return true; }
}

quint32 CDsoSession::getTrigCHBm()
{
    quint32 bmMask;

    //! analog
    bmMask = 0;
    for ( int i = 0; i < 4; i++ )
    {
        if ( m_ch[i].getTrigOnOff() )
        { set_bit( bmMask, i ); }
    }

    //! la
    if ( mLa.getTrigOnOff() )
    { set_bit( bmMask, la_bm_bit ); }

    return bmMask;
}

quint32 CDsoSession::getShowCHBm()
{
    quint32 bmMask;

    //! analog
    bmMask = 0;
    for ( int i = 0; i < 4; i++ )
    {
        if ( m_ch[i].getShowOnOff() )
        { set_bit( bmMask, i ); }
    }

    //! la
    if ( mLa.getShowOnOff() )
    { set_bit( bmMask, la_bm_bit ); }

    return bmMask;
}

quint32 CDsoSession::getSaCHBm()
{
    quint32 bmMask;

    //! analog
    bmMask = 0;
    for ( int i = 0; i < 4; i++ )
    {
        if ( m_ch[i].getOnOff() )
        { set_bit( bmMask, i ); }
    }

    //! la
    if ( mLa.getOnOff() )
    { set_bit( bmMask, la_bm_bit ); }

    return bmMask;
}

void CDsoSession::snapHoriTag( EngineHoriTag *pTag )
{
    Q_ASSERT( NULL != pTag );

    pTag->mMainScale = m_horizontal.getMainView().getScale();
    pTag->mMainOffset = m_horizontal.getMainView().getOffset();

    pTag->mZoomScale = m_horizontal.getZoomView().getScale();
    pTag->mZoomOffset = m_horizontal.getZoomView().getOffset();

    pTag->mCHLength = m_acquire.getChLength();
    pTag->mLaLength = m_acquire.getLaLength();

    pTag->mCHDotTime = m_acquire.getChDotTime();
    pTag->mLaDotTime = m_acquire.getLaDotTime();
}

