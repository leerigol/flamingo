#include "./cdsoautostopengine.h"

//! local macros
#define on_cmd( msg, id, para, proc, pAttr )   ON_COMMAND( CDsoAutoStopEngine, msg, id, para, proc, pAttr )
#define on_range_id( msg, id1, id2, para, proc, pAttr )    ON_RANGE_ID(CDsoAutoStopEngine, msg, id1, id2, para, proc, pAttr)

#define on_noid_cmd( msg, para, proc, pAttr ) ON_COMMAND( CDsoAutoStopEngine, msg, E_SERVICE_ID_NONE, para, proc, pAttr )
#define on_noid_cmd_context( msg, para, proc, pAttr, context ) \
                                       ON_COMMAND_CONTEXT( CDsoAutoStopEngine, msg, E_SERVICE_ID_NONE, para, proc, pAttr, context )

#define on_noid_stack( msg, proc, pAttr, stackSize, type )   \
                                        ON_COMMAND_STACK( CDsoAutoStopEngine, \
                                        msg, \
                                        E_SERVICE_ID_NONE, \
                                        type, \
                                        proc, \
                                        pAttr,\
                                        stackSize )

#include "../engineentry.h"

IMPLEMENT_MSG_MAP( dsoEngine, CDsoAutoStopEngine )

on_noid_cmd_s32( ENGINE_TMO, onTimeout, NULL ),

//! ch op.
on_range_ch( ENGINE_CH_ON, E_PARA_RANGE_BOOL, chOnOff, NULL ),
//on_range_ch( ENGINE_CH_SCALE, E_PARA_RANGE_S32, chScale ),
on_range_ch( ENGINE_CH_OFFSET, E_PARA_RANGE_S32, chOffset, NULL ),
on_range_ch( ENGINE_CH_COUPLING, E_PARA_RANGE_S32, chCoupling, NULL ),

//! la
on_noid_cmd_bool( ENGINE_LA_EN, setLaEn, NULL ),
on_noid_cmd_s32( ENGINE_LA_ON, setLaOnOff, NULL ),
on_noid_cmd_s32( ENGINE_LA_ACTIVE, setLaActive, NULL ),
on_noid_cmd_s32s32( ENGINE_LA_GROUP, setLaGroup, NULL ),
on_noid_cmd_s32( ENGINE_LA_SIZE, setLaSize, NULL ),
on_noid_cmd_ptr( ENGINE_LA_POS, setLaPos, NULL ),
on_noid_cmd_s32s32( ENGINE_LA_COLOR, setLaColor, NULL ),

//! hori
on_noid_cmd_s32( ENGINE_HORI_VIEW_MODE, setViewMode, NULL ),
//! hori
on_noid_cmd_s64( ENGINE_MAIN_SCALE, setMainScale, NULL ),
on_noid_cmd_s64( ENGINE_MAIN_OFFSET, setMainOffset, NULL ),

on_noid_cmd_s64( ENGINE_ZOOM_SCALE, setZoomScale, NULL ),
on_noid_cmd_s64( ENGINE_ZOOM_OFFSET, setZoomOffset, NULL ),

on_noid_get_ptr( qENGINE_HORI_INFO, queryHoriInfo ),

//! control
on_noid_get_s32void( qENGINE_CONTROL_STATUS, getControlStatus ),

on_noid_cmd_s32ptr( ENGINE_DISP_CH_ORDERS, setChDispOrder, NULL ),

//! display
//on_noid_cmd_s32( ENGINE_DISP_WFM_LIGHT, setIntensitity, NULL ),

END_OF_ITEM()
