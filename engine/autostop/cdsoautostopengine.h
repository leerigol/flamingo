#ifndef CDSOAUTOSTOPENGINE_H
#define CDSOAUTOSTOPENGINE_H

#include "../base/dsoengine.h"

class CDsoAutoStopEngine : public dsoEngine
{
    DECLARE_MSG_MAP()

public:
    CDsoAutoStopEngine();

public:
    virtual DsoErr exec( const struEngineMsgMap *pMap,
                         const struEngineMsgEntry *pEntry,
                         EngineMsg msg,
                         EnginePara& para,
                         void *outPtr,
                         ServiceId sId = E_SERVICE_ID_NONE );

    virtual DsoErr onExit();
    virtual DsoErr onEnter();

public:
    virtual DsoErr requestTrace(traceRequest& req,
                                 int tmo=2000);

protected:
    DsoErr onTimeout( int id );
    DsoErr timeoutToRecord();
    DsoErr timeoutToPlay();
    DsoErr playback();

    DsoErr setZoomScale(qlonglong scale);

    DsoErr queryHoriInfo( EngineHoriInfo *pInfo );
    ControlStatus getControlStatus();
};

#endif // CDSOAUTOSTOPENGINE_H
