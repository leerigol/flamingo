#include "cdsoautostopengine.h"

CDsoAutoStopEngine::CDsoAutoStopEngine()
{
    mName="CDsoAutoStopEngine";
}

DsoErr CDsoAutoStopEngine::exec( const struEngineMsgMap *pMap,
                     const struEngineMsgEntry *pEntry,
                     EngineMsg msg,
                     EnginePara& para,
                     void *outPtr,
                     ServiceId sId )
{
    //! base exec
    DsoErr err = dsoEngine::exec( pMap, pEntry, msg, para, outPtr, sId );

    //! in playback now
    if ( pMap == getMsgMap() )
    {
        //! auto stop engine
        if ( wr(msg) == Engine_Write )
        {
            int rawMsg = unpackSMsg( msg );
            if ( rawMsg != ENGINE_REQUEST_TRACE
                 && rawMsg != ENGINE_TMO )
            {  
                m_ConfigId.addSysConfigId();
                dsoEngine::m_playoutTimer->start( PLAY_TIMEOUT );
                if( rawMsg != ENGINE_DISP_WFM_LIGHT)
                {
                    dsoEngine::m_postPlayTimer->start( POST_PLAY );
                }
                dsoEngine::m_playTraceTimer->start( PLAY_TRACE_TIME );
            }
        }
    }

    dsoEngine::m_pPhy->flushWCache();

    return err;
}

DsoErr CDsoAutoStopEngine::onExit()
{
    return ERR_NONE;
}

DsoErr CDsoAutoStopEngine::onEnter()
{
    dsoEngine::m_playoutTimer->start( PLAY_TIMEOUT );
    //! no persist
    m_pPhy->mWpu.setpersist_mode( 0 );

    return ERR_NONE;
}

DsoErr CDsoAutoStopEngine::onTimeout( int id )
{
    if ( id == POST_PLAY_ID )
    {
        //! add_by_zx for bug 2356
        m_ConfigId.addSysConfigId();
        m_pPhy->mScuGp.setCONFIG_ID(m_ConfigId.getConfigId());
        timeoutToPlay();
        return ERR_NONE;
    }
    else if( id == PLAY_TIMEOUT_ID)
    {
        LOG_DBG()<<getDebugTime();
        return timeoutToRecord();
    }
    else
    { return ERR_NONE; }
}

DsoErr CDsoAutoStopEngine::timeoutToRecord()
{
    //! stoped
    if ( m_pViewSession->m_control.getStatus() == Control_Stoped )
    {
    }
    //! not stoped
    else
    {
        DsoErr err = toEngine( m_pRecEngine );
        LOG_DBG()<<err;
    }

    return ERR_NONE;
}

DsoErr CDsoAutoStopEngine::timeoutToPlay()
{
    playback();
    emit(sigConfigFinished());
    return ERR_NONE;
}

//! play back a frame
DsoErr CDsoAutoStopEngine::playback( )
{
    if( m_pViewSession->m_acquire.m_eTimeMode == Acquire_ROLL )
    {
        return ERR_NONE;
    }

    //! add_by_zx : for bug 2281
    int tempScale[4];
    for(int i = 0; i < 4;i++)
    {
        tempScale[i] = m_pViewSession->m_ch[i].getScale();
        if( m_pViewSession->m_ch[i].getScale() < m_pHistSession->m_ch[i].getScale() / 20)
        {
            m_pViewSession->m_ch[i].setScale( m_pHistSession->m_ch[i].getScale() / 20 );
        }
    }

    qlonglong tempMainHoriScale = 0;
    tempMainHoriScale = m_pViewSession->m_horizontal.getMainView().getScale();
    if(tempMainHoriScale > m_pHistSession->m_acquire.mCHFlow.mSaScale * 1000
            &&  m_pHistSession->m_acquire.mCHFlow.mSaScale != 0)
    {
        m_pViewSession->m_horizontal.getMainView().setScale(m_pHistSession->m_acquire.mCHFlow.mSaScale * 1000);
    }

    if ( sysHasArg("-no_autoplay") )
    {}
    else
    {
        dsoEngine::playback( m_pHistSession,
                             m_pViewSession,
                             true,
                             true);
    }

    for(int i = 0; i < 4;i++)
    {
        m_pViewSession->m_ch[i].setScale( tempScale[i] );
    }

    m_pViewSession->m_horizontal.getMainView().m_s64Scale = tempMainHoriScale;

    return ERR_NONE;
}

DsoErr CDsoAutoStopEngine::setZoomScale(qlonglong scale)
{
    //! check scale
    Q_ASSERT( NULL != m_pHistSession );
    Q_ASSERT( NULL != m_pViewSession );

    if( needPlay( m_pHistSession,  m_pViewSession ) )
    {
        //! ch view
        if ( validatePlayScale( scale, m_pHistSession ) )
        {}
        else
        { return ERR_HORI_SCALE; }
    }

    dsoEngine::setZoomScale( scale );

    return ERR_NONE;
}
