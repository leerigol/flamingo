
#include "../cdsoautostopengine.h"


//! add_by_zx:在autoEngine情况下不需要满足回放的要求，只需要满足下一次run的要求
DsoErr CDsoAutoStopEngine::queryHoriInfo( EngineHoriInfo *pInfo )
{
//    Q_ASSERT( NULL != pInfo );

//    pInfo->mChSa = m_pHistSession->m_acquire.mCHFlow.getSa();
//    pInfo->mChDotTime = m_pHistSession->m_acquire.mCHFlow.getDotTime();

//    pInfo->mLaSa = m_pHistSession->m_acquire.mLaFlow.getSa();
//    pInfo->mLaDotTime = m_pHistSession->m_acquire.mLaFlow.getDotTime();

//    pInfo->mChLength = m_pHistSession->m_acquire.mCHFlow.getDepth();
//    pInfo->mLaLength = m_pHistSession->m_acquire.mLaFlow.getDepth();

//    //! running now
//    //! left time
//    //! 1s, 100div
//    pInfo->mOffMax = decideHoriMaxOffset( m_pViewSession->m_horizontal.getMainView().getScale() * 100 );
//    //! -1/2 memtime
//    pInfo->mOffMin = (-pInfo->mChLength*pInfo->mChDotTime/2).ceilLonglong();

//    if( pInfo->mOffMin < - m_pViewSession->m_horizontal.getMainView().getScale() * 5 )
//    {
//        pInfo->mOffMin = - m_pViewSession->m_horizontal.getMainView().getScale() * 5;
//    }

    //! input
    int chCnt;
    chCnt = toChCnt( m_pViewSession->getSaCHBm() );

    pInfo->setInfo( m_pViewSession->m_horizontal.getMainView().getScale(),
                    m_pViewSession->m_horizontal.getMainView().getOffset(),
                    m_pViewSession->m_horizontal.getMainView().getScale(),
                    m_pViewSession->m_acquire.getChDepth(),
                    chCnt );

    //! output
    dsoEngine::queryHoriInfo( pInfo );

    return ERR_NONE;
}

