#ifndef CPLATFORM_H
#define CPLATFORM_H

#include "./base/dsoengine.h"
#include "cdsosession.h"

#include "../phy/cdsophy.h"
#include "../phy/bus/idsobus.h"

/*!
 * \brief The cplatform class
 * engine platform
 * - engine
 * - session
 * - phy
 * -
 */
class cplatform
{
public:
    static cplatform *sysGetDsoPlatform();
    static cplatform *createInstance();
private:
    static cplatform *_dsoPlatform;

public:
    cplatform();
    virtual ~cplatform();

public:
    dsoEngine *m_pRecEngine;        //! record engine
    dsoEngine *m_pPlayEngine;       //! play engine
    dsoEngine *m_pAutoStopEngine;   //! auto stop

    dso_phy::CDsoPhy *m_pPhy;
    dso_phy::IDsoBus *m_pBus;

public:
    virtual DsoErr buildPlatform();
    virtual DsoErr initPlatform();

    virtual DsoErr deInitPlatform();

    void movetoThread( QThread *pThread );

    dsoEngine *getRecEngine();
    dsoEngine *getPlayEngine();
    dsoEngine *getAutoStopEngine();
    dsoEngine *getActiveEngine();

};

#endif // CPLATFORM_H
