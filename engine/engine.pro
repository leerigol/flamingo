

TEMPLATE = lib
TARGET = ../lib$$(PLATFORM)/dsoengine
INCLUDEPATH += .

QT += widgets

CONFIG += static

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

# dbg show
dbg = $$(_DEBUG)
equals(dbg, _DEBUG){
DEFINES += _DEBUG
}

#DEFINES += _DEBUG
#DEFINES += _LOG_FPGA

model = $$(_MODEL)
equals( model, _DS8000){
DEFINES += _DS8000
}

equals( model, _DS7000){
DEFINES += _DS7000
}

HEADERS += \
    base/cengineobj.h \
    base/dsoengine.h \
    base/engine.h \
    base/enginecfg.h \
    base/trigger/dsoengine_trig.h \
    base/trigger/enginecfg_trig.h \
    base/adt/dsoengine_adt.h \
    base/search/enginecfg_search.h \
    base/display/enginecfgscr.h \
    base/1wireProbe/1wireprobe.h \
    base/adt/handlememory.h \
    base/adt/patchrequest.h \
\
    module/cacquire.h \
    module/cch.h \
    module/ccontrol.h \
    module/cdisplay.h \
    module/cfilter.h \
    module/chorizontal.h \
    module/chorizontalview.h \
    module/cprobe.h \
    module/csequence.h \
    module/ctrigger.h \
    module/cvertical.h \
    module/cdecode.h \
    module/cla.h \
    module/cmask.h \
    module/chwproc.h \
\
    play/cdsoplayengine.h \
    record/cdsorecengine.h \
    autostop/cdsoautostopengine.h \
\
    cdsosession.h \
    cplatform.h \
    enginemsg.h \   
    engineentry.h \
    module/csearch.h

SOURCES += \
    base/dsoengine.cpp \
    base/engine.cpp \
    base/enginecfg.cpp \
    base/enginepara.cpp \
    base/ienginecontext.cpp \
    base/cengineobj.cpp \
    base/ch/dsoengine_ch.cpp \
    base/hori/dsoengine_hori.cpp \
    base/con/dsoengine_con.cpp \
    base/acq/dsoengine_acq.cpp \
    base/trace/dsoengine_trace.cpp \
    base/la/dsoengine_la.cpp \
    base/display/dsoengine_display.cpp \
    base/trigger/dsoengine_trig.cpp \
    base/trigger/delay/dsoengine_trig_delay.cpp \
    base/trigger/duration/dsoengine_trig_duration.cpp \
    base/trigger/edge/dsoengine_trig_edge.cpp \
    base/trigger/nedge/dsoengine_trig_nedge.cpp \
    base/trigger/over/dsoengine_trig_over.cpp \
    base/trigger/pattern/dsoengine_trig_pattern.cpp \
    base/trigger/pulse/dsoengine_trig_pulse.cpp \
    base/trigger/runt/dsoengine_trig_runt.cpp \
    base/trigger/sh/dsoengine_trig_sh.cpp \
    base/trigger/slope/dsoengine_trig_slope.cpp \
    base/trigger/timeout/dsoengine_trig_timeout.cpp \
    base/trigger/video/dsoengine_trig_video.cpp \
    base/trigger/window/dsoengine_trig_window.cpp \
    base/trigger/enginecfg_trig.cpp \
    base/assist/dsoengine_assist.cpp \
    base/misc/dsoengine_misc.cpp \
    base/cal/dsoengine_cal.cpp \
    base/counter/dsoengine_counter.cpp \
    base/meas/dsoengine_meas.cpp \
    base/dsoengine_map.cpp \
    data/dsoengine_data.cpp \
    base/adt/dsoengine_adt.cpp \
    base/xpu/dsoengine_wpu.cpp \
    base/trigger/uart/dsoengine_trig_uart.cpp \
    base/trigger/iic/dsoengine_trig_iic.cpp \
    base/trigger/spi/dsoengine_trig_spi.cpp \
    base/view/dsoengine_view.cpp \
    base/dvm/dsoengine_dvm.cpp \
    base/trigger/can/dsoengine_trig_can.cpp \
    base/trigger/flexray/dsoengine_trig_flexray.cpp \
    base/trigger/std1553b/dsoengine_trig_std1553b.cpp \
    base/trigger/lin/dsoengine_trig_lin.cpp \
    base/trigger/iis/dsoengine_trig_iis.cpp \
    base/mask/dsoengine_mask.cpp \
    base/decode/dsoengine_decode.cpp \
    base/trigger/ab/dsoengine_trig_ab.cpp\
    base/recplay/dsoengine_recplay.cpp \
    base/search/dsoengine_search.cpp \
    base/search/enginecfg_search.cpp \
    base/display/enginecfgscr.cpp \
    base/memory/dsoengine_memory.cpp \
    base/1wireProbe/1wireprobe.cpp \
    base/adt/handlememory.cpp \
    base/playback/dsoengine_playback.cpp \
    base/adt/patchrequest.cpp \
\
    module/cacquire.cpp \
    module/cch.cpp \
    module/ccontrol.cpp \
    module/cdisplay.cpp \
    module/cfilter.cpp \
    module/chorizontal.cpp \
    module/chorizontalview.cpp \
    module/cprobe.cpp \
    module/csequence.cpp \
    module/ctrigger.cpp \
    module/cvertical.cpp \
    module/cdecode.cpp \
    module/cla.cpp \
    module/cmask.cpp \
    module/chwproc.cpp \
\
    record/cdsorecengine.cpp \
    record/cdsorecengine_ch.cpp \
    record/cdsorecengine_hori.cpp \
    record/cdsorecengine_con.cpp \
    record/dsorecengine_la.cpp \
    record/cdsorecengine_adc.cpp \
    record/dsorecengine_map.cpp \
    record/cdsorecengine_fcu.cpp \
    record/cdsorecengine_trig.cpp \
    record/cdsorecengine_meas.cpp \
    record/cdsorecengine_acq.cpp \
    record/cdsorecengine_search.cpp \
    record/cdsorecengine_trace.cpp \
\
    play/con/cdsoplayengine_con.cpp \
    play/dsoplayengine_map.cpp \
    play/cdsoplayengine.cpp \
    play/hori/cdsoplayengine_hori.cpp\
    play/display/cdsoplayengine_display.cpp \
    play/trace/cdsoplayengine_trace.cpp \
\
    autostop/cdsoautostopengine.cpp \
    autostop/cdsoautostopengine_map.cpp \
    autostop/hori/cdsoautostopengine_hori.cpp \
    autostop/hori/cdsoautostopengine_con.cpp \
    autostop/trace/cdsoautostopengine_trace.cpp \
\
    cdsosession.cpp \
    cplatform.cpp \
    record/cdsorecengine_roll.cpp \
    module/csearch.cpp \
    base/trigger/zone/dsoengine_trig_zone.cpp




