#ifndef _ENGINE_MSG
#define _ENGINE_MSG

#include <QtCore>   //! qlonglong
#include "../baseclass/dsoreal.h"

// for 1/2/3/4
namespace DsoEngine {
/*! \enum EngineMsg */
        enum EngineMsg {
        ENGINE_MSG_NONE,

        //! system msg
        ENGINE_TMO = 1,     //! int id
        ENGINE_FREEZE,        /*!< freeze */
        ENGINE_UNFREEZE,      /*!< un freeze */

                            //! EngineSession*
        qENGINE_SESSION,

        //! ch msg
        ENGINE_CH_ON = 256, //! bool
        ENGINE_CH_ONOFF_NOPLAY,
        ENGINE_CH_SCALE,    //! s32 uv
        qENGINE_CH_AFE_SCALE,
        ENGINE_CH_OFFSET,   //! s32 uv
        ENGINE_CH_NULL,     //! s32 uv
        ENGINE_CH_COUPLING, //! e

        ENGINE_CH_BANDLIMIT,//! e
        ENGINE_CH_IMPEDANCE,//! e
        ENGINE_CH_DELAY,    //! s32 ps
        ENGINE_CH_INVERT,   //! bool
        ENGINE_CH_PROBE_REAL,   //! dsoReal

        ENGINE_CH_CAL,      //! chan, chCal ptr
        ENGINE_CH_HIZ_TRIM, //! chan, lfTrim

        ENGINE_ADC_CORE_GO, //! core gain offset
        ENGINE_ADC_CORE_PHASE,   //! core phase

        ENGINE_EXT_CAL,     //! 0/1, pathCal ptr
        ENGINE_LA_CAL,      //! 0/1, pathCal
        ENGINE_PROBE_CAL,   //! 0/1/2/3, pathCal
        ENGINE_DG_OFFSET_CAL,   //! 0/1, pathCal
        ENGINE_DG_REF_CAL,  //! 0/1, pathCal
        ENGINE_ENTER_CAL,

        ENGINE_CH_LAYER,    //! EngineChLayer

        //! .probe
        ENGINE_PROBE_IMPEDANCE, //! e
        ENGINE_PROBE_DELAY, //! qlonglong ps

        qENGINE_PROBE_IMPEDANCE,
        qENGINE_PROBE_DELAY,
        qENGINE_PROBE_RATIO, //! s32: u

        //! la msg
        qENGINE_LA_PROBE,   //! bool
        ENGINE_LA_EN,       //! bool
        ENGINE_LA_ON,       //! mask
        ENGINE_LA_POS,      //! pos[]
        ENGINE_LA_ACTIVE,   //! mask
        ENGINE_LA_GROUP,    //! int, mask
        ENGINE_LA_SIZE,     //! e
        ENGINE_LA_DELAY,    //! la, delay, ps qlonglong

        ENGINE_LA_L_THRESHOLD,  //! threshold: uv
        ENGINE_LA_H_THRESHOLD,

        ENGINE_LA_COLOR,    //! int:0/high,1/edge,2/low, quint32

        //! control msg
        ENGINE_CONTROL_ACTION,      //! e:run/stop
        qENGINE_CONTROL_STATUS,     //! query control status
        ENGINE_WAIT_CONTROL_STOP,   //!wait stop

        //! hori msg
        ENGINE_HORI_VIEW_MODE, //! e
        ENGINE_MAIN_CFG,    //! main cfg

        ENGINE_MAIN_SCALE,  //! qlonglong ps
        ENGINE_MAIN_OFFSET, //! qlonglong ps
        ENGINE_ROLL_OFFSET,

        ENGINE_ZOOM_SCALE,  //! qlonglong ps
        ENGINE_ZOOM_OFFSET, //! qlonglong ps

        ENGINE_CLOCK_TUNE,  //!engine match fail int clock

        qENGINE_HORI_INFO,  //! HoriInfo*
        qENGINE_HORI_LIVE_CH_CNT,
        qENGINE_HORI_ATTR,  //! HoriAttr*

                            //! set chcnt
        ENGINE_HORI_LIVE_CH_CNT,

        //! acquire msg
        ENGINE_ACQUIRE_MODE,    //! e:normal/peak/avg/hres/envelope
        ENGINE_ACQUIRE_AVG_CNT, //! s32
        ENGINE_ACQUIRE_INTERPLATE_MODE, //! e
        ENGINE_ACQUIRE_TIMEMODE,//! e: xy/roll/yt

        ENGINE_ACQUIRE_SAMPLE_MODE, //! e:real/equ
        ENGINE_ACQUIRE_ANTI_ALIASING,   //! bool

        ENGINE_ACQUIRE_DEPTH,   //! s32 0--auto
        qENGINE_ACQUIRE_DEPTH,  //! s32 real depth
        ENGINE_ACQUIRE_SARATE,  //! qlonglong pHz
        qENGINE_ACQUIRE_SARATE, //! pHz

        ENGINE_ACQUIRE_LA_DEPTH,//! s32 0
        qENGINE_ACQUIRE_LA_DEPTH,   //! s32
        ENGINE_ACQUIRE_LA_SARATE,   //! qlonglong
        qENGINE_ACQUIRE_LA_SARATE,

        qENGINE_ACQUIRE_CH_LENGTH,  //! qlonglong
        qENGINE_ACQUIRE_LA_LENGTH,  //! qlonglong

        ENGINE_ACQUIRE_BAND,        //! SYSTEM RAW BAND
        ENGINE_ACQUIRE_TEST,        //! test

        qENGINE_ACQ_CH_MAIN_SAFLOW,
        qENGINE_ACQ_CH_ZOOM_SAFLOW,
        qENGINE_ACQ_LA_MAIN_SAFLOW,
        qENGINE_ACQ_LA_ZOOM_SAFLOW,

        //! -- view
        qENGINE_MAIN_VIEW_ATTR,     //! EngineViewAttr
        qENGINE_ZOOM_VIEW_ATTR,

        //! trigger msg
        ENGINE_TRIGGER_MODE,        //! e
        ENGINE_TRIGGER_HOLDER,      //! e: time/event
        ENGINE_TRIGGER_HOLDER_MODE, //! e: auto/fixed/rand
        ENGINE_TRIGGER_HOLD_COUNT,  //! u32
        ENGINE_TRIGGER_HOLD_TIME,   //! qulonglong: ps

        ENGINE_TRIGGER_SWEEP,       //! e:auto/normal/single
        ENGINE_TRIGGER_AUTO_TMO,    //! ps: 1s->20ms
        ENGINE_TRIGGER_COUPLING,    //! src, coup
        ENGINE_TRIGGER_FORCE,

        ENGINE_TRIGGER_OUTPUT,  //! bool
        ENGINE_TRIGGER_OUT_POLARITY, //! bool
        ENGINE_TRIGGER_OUT_DELAY, //! s32
        ENGINE_TRIGGER_OUT_PULSE, //! s32
        ENGINE_TRIGGER_CH_EN,       //! chan, bool
        ENGINE_DVM_COUNTOR_CH_EN,
        ENGINE_TRIGGER_CHs_EN_RST,  //! void

        //! .level
        ENGINE_TRIGGER_LEVEL_MOD,   //! abs/ref
        ENGINE_TRIGGER_LEVEL_A,     //! level a, a is valid when there is 1 level
                                    //! uv s32
                                    //! ch, l, h

        ENGINE_TRIGGER_LEVEL_B,     //! level b

        ENGINE_TRIGGER_LEVEL_A_REF, //! ~100% step 1
        ENGINE_TRIGGER_LEVEL_B_REF, //! used for slope trigger

        ENGINE_TRIGGER_LEVEL_A_ADC, //! adc unit
        ENGINE_TRIGGER_LEVEL_B_ADC, //! adc unit

        //! ext trig level
        ENGINE_TRIGGER_LEVEL_EXT,   //! uv unit, sens 0.1div
        ENGINE_TRIGGER_LEVEL_EXT5,  //! uv unit, sens 0.1div

        //! .edge
        ENGINE_TRIGGER_EDGE_CFG,

        //! .pulse
        ENGINE_TRIGGER_PULSE_CFG,

        //! .runt
        ENGINE_TRIGGER_RUNT_CFG,

        //! .over
        ENGINE_TRIGGER_OVER_CFG,

        //! .window
        ENGINE_TRIGGER_WINDOW_CFG,

        //! .nEdge
        ENGINE_TRIGGER_NEDGE_CFG,

        //! .slope
        ENGINE_TRIGGER_SLOPE_CFG,

        //! .timeout
        ENGINE_TRIGGER_TIMEOUT_CFG,

        //! .logic
        ENGINE_TRIGGER_LOGIC_CFG,

        //! .setuphold
        ENGINE_TRIGGER_SETUPHOLD_CFG,

        //! .delay
        ENGINE_TRIGGER_DELAY_CFG,

        //! .video
        ENGINE_TRIGGER_VIDEO_CFG,

        //! .ab
        ENGINE_TRIGGER_AB_CFG,

        //! .area
        ENGINE_TRIGGER_ZONE_EN,
        ENGINE_TRIGGER_ZONE_CFG,

        //! .duration
        ENGINE_TRIGGER_DURATION_CFG,

        //! .rs232
        ENGINE_TRIGGER_UART_CFG,

        //! .spi
        ENGINE_TRIGGER_SPI_CFG,

        //! .i2s
        ENGINE_TRIGGER_I2S_CFG,

        //! .i2c
        ENGINE_TRIGGER_I2C_CFG,


        //! .lin
        ENGINE_TRIGGER_LIN_CFG,

        //! .can
        ENGINE_TRIGGER_CAN_CFG,

        //! .flexray
        ENGINE_TRIGGER_FLEXRAY_CFG,

        //! .1553
        ENGINE_TRIGGER_1553_CFG,

        //! .429
        ENGINE_TRIGGER_429_CFG,

        //! .sent
        ENGINE_TRIGGER_SENT_CFG,

        //! .dmx512
        ENGINE_TRIGGER_DMX512_CFG,

        //! .sbus
        ENGINE_TRIGGER_SBUS_CFG,

        //! .1wire
        ENGINE_TRIGGER_1WIRE_CFG,

        //! search
        qENGINE_SEARCH_REQ_DATA,      //! req data
        qENGINE_SEARCH_GET_DATA,      //! get data
        ENGINE_SEARCH_CLEAR_DATA,     //! CLEAR BUFFER

        ENGINE_SEARCH_EN,       //! 3bit
                                //! 0--zoom,1--ch,2--la
                                //!
        ENGINE_SEARCH_LEVEL_A,
        ENGINE_SEARCH_LEVEL_B,
        ENGINE_SEARCH_EDGE_CFG,
        ENGINE_SEARCH_PULSE_CFG,
        ENGINE_SEARCH_RUNT_CFG,
        ENGINE_SEARCH_OVER_CFG,

        ENGINE_SEARCH_WINDOW_CFG,
        ENGINE_SEARCH_NEDGE_CFG,
        ENGINE_SEARCH_SLOPE_CFG,
        ENGINE_SEARCH_TIMEOUT_CFG,

        ENGINE_SEARCH_LOGIC_CFG,
        ENGINE_SEARCH_SETUPHOLD_CFG,
        ENGINE_SEARCH_DELAY_CFG,
        ENGINE_SEARCH_VIDEO_CFG,

        ENGINE_SEARCH_AB_CFG,
        ENGINE_SEARCH_AREA_CFG,
        ENGINE_SEARCH_DURATION_CFG,
        ENGINE_SEARCH_UART_CFG,

        ENGINE_SEARCH_SPI_CFG,
        ENGINE_SEARCH_I2S_CFG,
        ENGINE_SEARCH_I2C_CFG,
        ENGINE_SEARCH_LIN_CFG,

        ENGINE_SEARCH_CAN_CFG,
        ENGINE_SEARCH_FLEXRAY_CFG,
        ENGINE_SEARCH_1553_CFG,
        ENGINE_SEARCH_429_CFG,

        ENGINE_SEARCH_SENT_CFG,
        ENGINE_SEARCH_DMX512_CFG,
        ENGINE_SEARCH_SBUS_CFG,
        ENGINE_SEARCH_1WIRE_CFG,

        //! display
        ENGINE_DISP_BG_LIGHT,        //! s32: background light 0~100%
        ENGINE_DISP_WFM_LIGHT,       //! s32: waveform light 0~100%
        ENGINE_DISP_CLEAR,           //! clear all
        ENGINE_DISP_CLEAR_PERSIST,   //! clear persist only

        ENGINE_DISP_PERSIST_TIME,    //! s32: 0-no, -1 -- inf, 1ms unit
        ENGINE_DISP_PALETTE,         //! e: light/color
        ENGINE_DISP_TYPE,            //! e: dot/vector
        ENGINE_DISP_CH_COLOR,        //! dest, color \todo two paras

        ENGINE_DISP_SCREEN_PERSIST,  //! bool
        ENGINE_DISP_CH_EN,           //! s32: ch, bool: en
        ENGINE_DISP_CH_TOP,          //! s32: ch top

        ENGINE_DISP_CH_ORDERS,       //! s32, ptr

        ENGINE_DISP_SCREEN,          //! EngineCfgScr

        //! mask
        ENGINE_MASK_ON,         //! bool
        ENGINE_MASK_SOURCE,     //! (1<<chan)
        ENGINE_MASK_RULE,       //! EngineMaskRule
        ENGINE_MASK_RST,        //! void
        qENGINE_MASK_REPORT,    //! EngineMaskReport

        ENGINE_MASK_ACTION_EVENT,   //! e: pass/fail
        ENGINE_MASK_ACTION_ACTION,  //! e: only stop can be used

        ENGINE_MASK_OUTPUT_ON,     //! bool
        ENGINE_MASK_OUTPUT_DLY,    //! qlonglong ps
        ENGINE_MASK_OUTPUT_TIME,   //! qlonglong ps
        ENGINE_MASK_OUTPUT_HL,     //! output high or low
                                //! --   ------    -----
                                //!  |___|      ___|   |____

        //! record(sequence)
        ENGINE_RECORD_DONE,       //! bool
        ENGINE_RECORD_ACTION,   //! bool run/stop
        ENGINE_RECORD_INTERVAL, //! qlonglong ps
        ENGINE_RECORD_FRAMES,   //! s32

        ENGINE_RECORD_PLAY_ACTION,      //! e run/stop
        ENGINE_RECORD_PLAY_RANGE,       //! s32 \todo 2 paras,
        ENGINE_RECORD_PLAY_FRAME_ID,    //! s32: current play id
        ENGINE_RECORD_PLAY_INTERVAL,    //! qlonglong ps

        ENGINE_RECORD_PLAY_DIRECTION,   //! e: up/down
        ENGINE_RECORD_PLAY_LOOP,        //! bool
        ENGINE_RECORD_PLAY_STEP,        //! s32

        qENGINE_RECORD_MAX_FRAMES,      //! max now
        qENGINE_RECORD_PLAY_FRAME_TAG,  //! \todo 2 paras: frame, qlonglong ps
        qENGINE_RECORD_PLAY_FRAME_ID,   //! s32
        qENGINE_RECORD_IDLE,            //! bool

        //! get trace
        ENGINE_REQUEST_TRACE,   //! no para
        qENGINE_TRACE,          //! get trace from engine chid, dsowfm

        ENGINE_MEMORY,          //! ch, F_id, memory
        qENGINE_MEMORY,         //! ch, F_id, memory
        qENGINE_TRACE_PEAK,     //! ch

        //! decode
        //! \todo

        //! search
        //! \todo

        //! math
        //! \todo

        //! counter
        ENGINE_COUNTER_RST,         //! rst and start
        ENGINE_COUNTER_EN,          //! bool
        ENGINE_COUNTER_SOURCE,      //! chan
        ENGINE_COUNTER_GATE_TIME,   //! s32:us
        ENGINE_MEAS_ONCE_REQ,            //!

        ENGINE_COUNTER_CFG,         //! EngineCounter
        qENGINE_COUNTER_VALUE,      //! EngineCounterReq

        ENGINE_COUNTER_EVENT,   //! e: rise/fall
        ENGINE_COUNTER_GATE_SOURCE, //! e
        ENGINE_COUNTER_GATE_HL, //! e: h/l

        //! meas
        ENGINE_MEAS_EN,            //! 3bit
                                   //! 0--zoom, 1--ch, 2--la

        ENGINE_MEAS_PULSE_TYPE,    //! Chan, int type
        ENGINE_MEAS_EDGE_TYPE,     //! Chan, int type

        ENGINE_MEAS_CFG,            //! meas cfg ptr
        ENGINE_MEAS_RD_DURING_FLAG, //! bool
        qENGINE_MEAS_VALUE,         //! meas result
        qENGINE_MEAS_RESULT_DONE,   //! meas result done

        qENGINE_ADC_CORE_CH,        //! EngineAdcCoreCH *
        ENGINE_MEAS_CFG_TOTAL_NUM,  //! meas total num

        ENGINE_MEAS_RANGE,          //! qlonglong t0, qlonglong tLength
        ENGINE_MEAS_VIEW,           //! view

        //! dvm
        //!
        ENGINE_DVM_SOURCE,
        ENGINE_DVM_GATE_TIME,       //! int us
        qENGINE_DVM_VOLT,           //! structure

        //! decode
        ENGINE_DECODE_THRESHOLD,    //! chan, h, l
                                    //! h,l : uv
        ENGINE_DECODE_THRESHOLD_L,  //! threshold low

        //! utility
        ENGINE_BEEP_TICK,
        ENGINE_BEEP_SHORT,
        ENGINE_BEEP_START,
        ENGINE_BEEP_STOP,
        ENGINE_BEEP_INTERVAL,

        ENGINE_LCD_LIGHT,   //! s32 0~100
        ENGINE_FAN_SPEED,   //! s32 0~100

        ENGINE_PROBE_1K_EN, //! bool
        ENGINE_TRIG_OUT_MUX,//! s32 see  K7M_FCU.xlsm

        ENGINE_1_1G_OUT,    //! bool

        //! hardware
        ENGINE_LED_ON,      //! s32 led id
        ENGINE_LED_OFF,     //! s32 led id
        ENGINE_LED_OP,      //! EngineLed, on/off

        ENGINE_LED_STATUS,  //! s32 led status
        qENGINE_LED_STATUS, //! query led status

        ENGINE_POWER_SWITCH,    //! bool
        qENGINE_POWER_SWITCH,   //!

        ENGINE_POWER_SHUTDOWN,  //! no para
        ENGINE_POWER_RESTART,   //! no para

        //! cpld
        ENGINE_BOARD_RST_CTP,         //! no para

        ENGINE_BOARD_CLR_INT,         //! no para
        qENGINE_BOARD_INT_STAT,       //! ?quint16
        qENGINE_IBOARD_INT_EN,         //! ?quint16
        qENGINE_BOARD_INT_MASK,       //! ?quint16

        ENGINE_BOARD_TEST_WRITE,    //! quint16
        qENGINE_BOARD_TEST_READ,    //! ?quint16

        qENGINE_CPLD_VER,   //! ?u16
        qENGINE_MB_VER,     //! ?u16

        qENGINE_SPU_VER,    //! ?u32
        qENGINE_SCU_VER,    //! ?u32
        qENGINE_WPU_VER,    //! ?u32
        qENGINE_TPU_VER,    //! ?u32
        qENGINE_FCU_VER,    //! ?u32

        qENGINE_SEARCH_VER, //! ?u32

        //! adc info
        qENGINE_ADC_ID,     //! u32
        qENGINE_ADC_UUID,   //! 128 bit ptr

        //! afe
        qENGINE_AFE_VER,    //! ver quint32(int)

        //! hw
        ENGINE_REG,         //! quint32 bus, quint32 addr, quint32 val
        qENGINE_REG,        //! quint32 ( quint32 bus, quint32 addr )

        //! test
        ENGINE_ADC1_CFG,
        ENGINE_ADC2_CFG,

//        ENGINE_RELAY_CFG,
//        ENGINE_VGA1_CFG,
//        ENGINE_VGA2_CFG,
//        ENGINE_VGA3_CFG,

//        ENGINE_VGA4_CFG,

        qENGINE_1WIRE_PROBE_INFO,
        ENGINE_1WIRE_PROBE_INFO,
        ENGINE_1WIRE_PROBE_DAC,
    };

    /*! \enum EngineParaType */
    enum EngineParaType
    {
        //! ---- setting
        E_PARA_WRITE = 0,    //! w
        E_PARA_NONE = 0,    //! dsoErr set(void)

        E_PARA_BOOL,        //! DsoErr set( bool )
        E_PARA_S32,         //! DsoErr set( s32 )
        E_PARA_S64,         //! DsoErr set( s64 )

        E_PARA_U8,          //! DsoErr set( u8 )
        E_PARA_U16,         //! DsoErr set( u16 )
        E_PARA_U32,         //! DsoErr set( u32 )
        E_PARA_U64,         //! DsoErr set( u64 )

        E_PARA_REAL,        //! DsoErr set( DsoReal )

        E_PARA_PTR,         //! DsoErr do( void * )
        E_PARA_INT_PTR,     //! DsoErr do( int, void *)

        E_PARA_INT_BOOL,    //! DsoErr set( int, bool )
        E_PARA_INT_INT,     //! DsoErr set( int, s32 )
        E_PARA_INT_S64,     //! DsoErr set( int, s64 )

        E_PARA_STACK,       //! DsoErr set( EnginePara &para )
                            //! check stack size

        E_PARA_RANGE_BOOL,  //! DsoErr set( serviceId, bool )
        E_PARA_RANGE_S32,   //! DsoErr set( serviceId, s32 )
        E_PARA_RANGE_S64,   //! DsoErr set( serviceId, s64 )
        E_PARA_RANGE_PTR,   //! DsoErr set( serviceId, void* )
        E_PARA_RANGE_REAL,  //! DsoErr set( serviceId, dsoReal )

        //will not play back MSG
        E_CFG_PARA_NONE,
        E_CFG_PARA_U16,
        E_CFG_PARA_S32,
        E_CFG_PARA_U32,
        E_CFG_PARA_S64,
        E_CFG_PARA_U64,
        E_CFG_PARA_BOOL,
        E_CFG_PARA_INT_BOOL,    //! DsoErr set( int, bool )
        E_CFG_PARA_INT_INT,     //! DsoErr set( int, s32 )
        E_CFG_PARA_INT_S64,     //! DsoErr set( int, s64 )


        //! query -- no setting
        E_PARA_READ  = 32,
        E_PARA_qBOOL = 32, //! bool get()
        E_PARA_qS32,        //! s32 get()
        E_PARA_qS64,        //! s64 get()

        E_PARA_qU8,         //! u8 get()
        E_PARA_qU16,        //! u16 get()
        E_PARA_qU32,        //! u32 get()
        E_PARA_qU64,        //! u64 get()
        E_PARA_qREAL,        //! DsoReal get()

        E_PARA_qPTR,        //! DsoErr get( void *ptr )

        E_PARA_qBOOL_INT,   //! bool get(int)
        E_PARA_qS32_INT,    //! s32 get(int)
        E_PARA_qS64_INT,    //! s64 get(int)

        E_PARA_qU8_INT,     //! u8 get(int)
        E_PARA_qU16_INT,    //! u16 get(int)
        E_PARA_qU32_INT,    //! u32 get(int)
        E_PARA_qU64_INT,    //! u64 get(int)

        E_PARA_qINT_PTR,    //! DsoErr get( int, void *ptr )

        E_PARA_qU32_STACK,  //! u32 get( EnginePara &para )

        E_PARA_MAX = 64,
    };

    enum EngineWR
    {
        Engine_Write,
        Engine_Read,
        Engine_Config,
    };

    /*! \union EnginePara
    */
    struct EnginePara
    {
        int mSize;
        EngineParaType mType;

        //! data
        union
        {
            bool bVal;
            qint32 s32Val;
            qint64 s64Val;

            quint8 u8Val;
            quint16 u16Val;
            quint32 u32Val;
            quint64 u64Val;
            void *ptr;
        };
        DsoReal realVal;

        //! to next
        EnginePara *pNext;

        EnginePara( bool val );
        EnginePara( int val );
        EnginePara( qint64 val );
        EnginePara( void *ptr );

        EnginePara( quint8 val );
        EnginePara( quint16 val );
        EnginePara( quint32 val );
        EnginePara( quint64 val );

        EnginePara( DsoReal val );

        void _ctor();

        void setType( EngineParaType t );
        void pushPara( EnginePara *para );

        EnginePara *operator[]( int index );

        int size();
    };

    /*!
     * \brief The EngineLed enum
     * 系统led定义
     */
    enum EngineLed
    {
        led_base = 0,
        led_ch1 = 0,
        led_ch2,
        led_ch3,
        led_ch4,

        led_source1,
        led_source2,

        led_digital,
        led_math,
        led_ref,
        led_decode,

        led_run,
        led_stop,
        led_single,

        led_func,
        led_touch,
        led_top = led_touch,
    };

    enum EngineBus
    {
        engine_bus_spi_cpld_0,  //! spi0
        engine_bus_spi_cpld_1,  //! spi1
        engine_bus_axi,
        engine_bus_mem,
        engine_bus_top = engine_bus_mem,
    };
}

/*! \def packServiceMsg
  */
#define packServiceMsg( msg, sID, paraType )    (EngineMsg)( ( ((unsigned int)sID&0xff) << 24 ) |\
( ( (unsigned int)msg & 0xffff)<<8 ) |\
( ( (unsigned int)paraType & 0x3f ) << 2 ) )

/*! \def packMsg */
#define packMsg( msg, paraType )    (EngineMsg)( ( ((unsigned int)mpItem->servId&0xff) << 24 ) |\
( ( (unsigned int)msg & 0xffff)<<8 ) |\
( ( (unsigned int)paraType & 0x3f ) << 2 ) )

/*! \def unpackSId */
#define unpackSId( msg )     (ServiceId)( ((unsigned int)(msg)>>24)&0xff )
/*! \def unpackSMsg */
#define unpackSMsg( msg )    (EngineMsg)( ((unsigned int)(msg)>>8)&0xffff )
/*! \def unpackSType */
#define unpackSType( msg )   (EngineParaType)( ((unsigned int)(msg)>>2)&0x3f )

#endif // DSOMSG

