
#include "../base/dsoengine.h"

//! 2.5G
#include "./filter/2_5G_200M.h"
#include "./filter/2_5G_350M.h"

#include "./filter/5G_200M.h"
#include "./filter/5G_350M.h"

#include "./filter/10G_200M.h"
#include "./filter/10G_350M.h"

BandwidthFilter _bw2_5G_Filters[]=
{
    { BW_20M, _2_5G_200M_},
    { BW_100M, _2_5G_200M_},

    { BW_200M, _2_5G_200M_},
    { BW_350M, _2_5G_350M_},

//    { BW_500M, _2G5_500M_},

    //! end
    { BW_FULL, NULL },
};

//! 5G
BandwidthFilter _bw5G_Filters[]=
{
    { BW_20M, _5G_200M_},
    { BW_100M, _5G_200M_},

    { BW_200M, _5G_200M_},
    { BW_350M, _5G_350M_},

//    { BW_500M, _5G500M_},

    //! end
    { BW_FULL, NULL },
};

//! 10G
BandwidthFilter _bw10G_Filters[]=
{
    { BW_20M, _10G_200M_},
    { BW_100M, _10G_200M_},

    { BW_200M, _10G_200M_},
    { BW_350M, _10G_350M_},

//    { BW_500M, _10G500M_},

    //! end
    { BW_FULL, NULL },
};

int dsoEngine::selectFilter( int chCnt,
                  Bandwidth bw,
                  EngineFilter *pFilter )
{
    Q_ASSERT( pFilter != NULL );

    BandwidthFilter *pTable;

    //! filter table
    if ( chCnt == 4 )
    {
        pTable = _bw2_5G_Filters;
    }
    else if ( chCnt == 2 )
    {
        pTable = _bw5G_Filters;
    }
    else
    {
        pTable = _bw10G_Filters;
    }

    int i = 0;
    while ( pTable[i].m_pFilter != NULL )
    {
        if ( pTable[i].bw == bw )
        {
            pFilter->attachWindow( bw,
                                   pTable[i].m_pFilter,
                                   array_count(_2_5G_200M_) );

            return 0;
        }

        i++;
    }

    return -1;
}

void dsoEngine::engineErrMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
//    static QVector<QString> beforeMsg;
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    default :
        break;
    }
}
