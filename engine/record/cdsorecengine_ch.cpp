#include "cdsorecengine.h"

#define assert_ch_id( id )  Q_ASSERT( id >= E_SERVICE_ID_CH1 && id <= E_SERVICE_ID_CH4 );
#define ch_index( id )      (id - E_SERVICE_ID_CH1)

#define chObj(id)   m_pViewSession->m_ch[ ch_index(id)]

#define selfCh    chObj(id)

DsoErr CDsoRecEngine::chOnOff(ServiceId id, bool b)
{
    DsoErr err = dsoEngine::chOnOff( id, b );

    //! hori changed
    EngineHoriInfo horiInfo;
    horiInfo.mChBm = m_pViewSession->getSaCHBm();
    horiInfo.mChCnt = toChCnt( horiInfo.mChBm );
    emit sigHoriChanged( horiInfo );

    return err;
}

void CDsoRecEngine::chInvertApply( ServiceId id )
{
    //! pre reg
    Spu_reg reg = m_pPhy->mSpu.getCTRL_adc_data_inv();

    bool bInv = selfCh.getInvert();
    Impedance imp = selfCh.getImpedance();

    int chIndex;
    assert_ch_id( id );
    chIndex = ch_index( id );

    //! imp
    if ( imp == IMP_50 )
    {
        if ( bInv )
        {
            unset_bit(reg, chIndex );
        }
        else
        {
            set_bit(reg, chIndex );
        }
    }
    else
    {
#ifdef DAMREY_VER_1_5_x
        if ( bInv )
        {
            unset_bit(reg, chIndex );
        }
        else
        {
            set_bit(reg, chIndex );
        }
#else
        if ( bInv )
        {
            set_bit(reg, chIndex );
        }
        else
        {
            unset_bit(reg, chIndex );
        }
#endif
    }

    //! invert
    m_pPhy->mSpu.setCTRL_adc_data_inv( reg );

    m_pPhy->m_ch[ chIndex ].applyOffset(
                ( chObj(id).getOffset() ) * chObj(id).getOffsetSign() + chObj(id).getNull()
                );

}

DsoErr CDsoRecEngine::chInterleave()
{
    //! ch mask
    quint32 chBm;
    chBm = m_pViewSession->getSaCHBm();

    chInterleave_Invert( chBm );

    chInterleave_Filter( chBm );

    return ERR_NONE;
}

void CDsoRecEngine::chInterleave_Invert( quint32 chBm )
{
    //! groups
    AdcGroups adcGroups;
    toAdcGroups( chBm, &adcGroups );

    //! align invert
    Spu_reg reg = m_pPhy->mSpu.getCTRL_adc_data_inv();
    Spu_reg bit;
    //! invert apply
    for ( int i = 0; i < adcGroups.mGroupCnt; i++ )
    {
        //! master bit
        bit = get_bit( reg, adcGroups.mGroups[i].mMaster );

        //! get invert
        for ( int j = 0; j < adcGroups.mGroups[i].mSlaveCnt; j++ )
        {
            set_x_bit( reg, adcGroups.mGroups[i].mSlaves[j], bit );
        }
    }

    //! config
    m_pPhy->mSpu.setCTRL_adc_data_inv( reg );

}

void CDsoRecEngine::chInterleave_Filter( quint32 chBm )
{
    //! groups
    AdcGroups adcGroups;
    toAdcGroups( chBm, &adcGroups );

    //! bw detect
    Bandwidth bw;
    Impedance imp;
    int iMaster;

    CCH *pCH;

    //! filter apply
    for ( int i = 0; i < adcGroups.mGroupCnt; i++ )
    {
        iMaster = adcGroups.mGroups[i].mMaster;

        pCH = m_pViewSession->m_ch + iMaster;

        bw = pCH->getBandwidth();
        imp = pCH->getImpedance();

        //! hw filter
        m_pPhy->m_ch[ iMaster ].setBw( bw, imp );

/********************FPGA Filter NOT USED********************/
        //! sw filter
        EngineFilter filter;
        int ret;
        ret = selectFilter( toChCnt( chBm ),
                            bw,
                            &filter
                            );
//        only for fpga debug
        chFilterCfg( iMaster, filter, ret == 0 );

        //! for slave
        for ( int j = 0; j < adcGroups.mGroups[i].mSlaveCnt; j++ )
        {
            chFilterCfg( adcGroups.mGroups[i].mSlaves[j],
                         filter,
                         ret == 0 );
        }

        //! has filter
        m_pPhy->mSpu.setFilterEn(
                    ret == 0,
                    adcGroups.mGroups[i].mBm
                    );
/*********************************************************/
    }
}

void CDsoRecEngine::chFilterCfg( int chId,
                                  EngineFilter &filter,
                                  bool bOnOff )
{
    m_pPhy->mSpu.cfgFilter(
                            chId,
                            bOnOff,
                            IPhySpu::filter_fir,
                            filter.getWindow() );
}

//! on/off
//! filter
//! invert
DsoErr CDsoRecEngine::chsApply()
{
    int chId;

    for(  int i = E_SERVICE_ID_CH1; i <= E_SERVICE_ID_CH4; i++ )
    {
        chId = ch_index( i );

        //! invert
        chInvertApply( (ServiceId)i );

//        qDebug()<<mLastSession.m_ch[ chId ].getScale()<<m_pViewSession->m_ch[chId].getScale()<<
//                  mRecSession.m_ch[ chId ].getScale();

        //! add_by_zx:连配三次参数，第一次与第二次垂直参数不同，第二次与第三次垂直参数相同，
        //! 同时第二次参数下没有被触发住就会导致垂直的wpu偏移和增益出错，因为第三次run的配置保留了
        //! 第一次参数与第三次参数之间的回放的配置
        //! nothing changed:为了测试系统少配参数 for Calibration.
        if ( mLastSession.m_ch[ chId] == m_pViewSession->m_ch[chId] &&
             m_pViewSession->m_bCalibrating)
        {
            continue;
        }

        //! vertical on/off
        m_pPhy->m_ch[ chId ].setOnOff(  m_pViewSession->m_ch[ chId ].getOnOff() );

        //! wpu on/off
        m_pPhy->mWpu.setChannelEn( chId,
                                   m_pViewSession->m_ch[ chId ].getShowOnOff() );

        //! scale --> wpu gain
        m_pPhy->m_ch[ chId ].setScale(
                                    m_pViewSession->m_ch[ chId ].getScale() );

        //! afe info
        mRecSession.m_ch[ chId ].setAfeScale( m_pPhy->m_ch[ chId ].getAfeScale() );
        mRecSession.m_ch[ chId ].setAfeGnd( m_pPhy->m_ch[ chId ].getAfeGnd() );
        /*qDebug()<<__FILE__<<__LINE__
                <<"CHAN:"<<chId
                <<"afe scale:"<<m_pPhy->m_ch[ chId ].getAfeScale()
                <<"afe gnd:"  <<m_pPhy->m_ch[ chId ].getAfeGnd();*/
    }

    //! interleave
    chInterleave();

    return ERR_NONE;
}

//! .probe
DsoErr CDsoRecEngine::chProbeImpedance( ServiceId /*id*/, Impedance /*imp*/ )
{
    return ERR_NONE;
}
DsoErr CDsoRecEngine::chProbeDelay( ServiceId id, int dly )
{
    //!rigol探头不支持延迟，暂时设置和通道延迟相同。
    assert_ch_id( id );

    selfCh.setDelay( dly );

    return ERR_NONE;
}

Impedance CDsoRecEngine::chProbeGetImpedance( ServiceId /*id*/ )
{
    return IMP_1M;
}
int CDsoRecEngine::chProbeGetDelay( ServiceId /*id*/ )
{
    return 0;
}

int CDsoRecEngine::chProbeGetRatio( ServiceId /*id*/ )
{
    return 1000000;
}

