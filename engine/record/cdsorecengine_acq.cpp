#include "cdsorecengine.h"

#define INTERNAL_MEM_SIZE      8000

DsoErr CDsoRecEngine::acqApply(  )
{
    if( m_pViewSession->m_acquire.getAcquireMode() == Acquire_Peak)
    {
        //! >400ps有降采样
        if( mRecSession.m_acquire.mCHFlow.mDotTime > 400 )
        {
            //! 存在降采样时才能配置峰值检测
            dsoEngine::applyAcquireMode( Acquire_Peak );
        }
        else
        {
            dsoEngine::applyAcquireMode( Acquire_Normal );
        }
    }
    else
    {
        //! acq mode
        dsoEngine::applyAcquireMode( m_pViewSession->m_acquire.getAcquireMode() );
    }

    //! average memory
    quint32 chCnt = toChCnt( m_pViewSession->getSaCHBm() );
    if ( chCnt * m_pPhy->mSpu.getRECORD_LEN() > INTERNAL_MEM_SIZE )
    {
        m_pPhy->mSpu.setCTRL_trace_avg_mem(1);
    }
    else
    {
        //! 临时修改：平均模式为0的情况有bug
        // m_pPhy->mSpu.setCTRL_trace_avg_mem(0);
    }

    if( mRecSession.m_acquire.getAntiAliasing() )
    {
        if(mRecSession.m_acquire.m_chCnt * mRecSession.m_acquire.getChSaRate() <= 5e15)
        {
            //! 抗混叠打开时需要打开峰值压缩
            m_pPhy->mSpu.setCOMPRESS_MAIN_peak(1);
            m_pPhy->mSpu.setCOMPRESS_ZOOM_peak(1);
            m_pPhy->mSpu.setCTRL_anti_alias(1);
        }
        else
        {
            m_pPhy->mSpu.setCTRL_anti_alias(0);
        }
    }
    else
    {
        m_pPhy->mSpu.setCTRL_anti_alias(0);
    }

    return ERR_NONE;
}
