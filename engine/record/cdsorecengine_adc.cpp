
#include "cdsorecengine.h"
#include "../../phy/hori/chorialign.h"

bool CDsoRecEngine::isAdcModeChange( dso_phy::IPhyADC::eAdcInterleaveMode eIntMode )
{
    IPhyADC *pAdc;

    //! --- adc
    pAdc = &m_pPhy->mAdc[0];

    return ( pAdc->getMode() != (quint32)eIntMode );
}

//! chCnt: 1,2,4
DsoErr CDsoRecEngine::calAdcGroup( dso_phy::IPhyADC::eAdcInterleaveMode eIntMode,
                                   DsoWorkAim aim )
{
//    qDebug()<<"calAdcGroup Start"<<getDebugTime();
    Spu_reg abcd;
    int chCnt;

    chCnt = dso_phy::IPhyADC::toChCnt( eIntMode );

    int aligns[4];
    m_pPhy->mSpuGp.outADC_CHN_ALIGN( 0 );
    //! snap
    snapAdcGroup( eIntMode, &abcd, aim, chCnt);

    m_pPhy->flushWCache();
    //! register
    dso_phy::Spu_ADC_CHN_ALIGN_CHK alABCD;
    alABCD.payload = abcd;

    aligns[0] = alABCD.adc_a;
    aligns[1] = alABCD.adc_b;
    aligns[2] = alABCD.adc_c;
    aligns[3] = alABCD.adc_d;
    if( chCnt == 1)
    {
        //qDebug()<<"adc align"<<aligns[0]<<aligns[1]<<aligns[2]<<aligns[3];
        if(aligns[0] == aligns[1] && aligns[0] == aligns[2]
                && aligns[0] == aligns[3] )
        {
            return ERR_NONE;
        }
        //qDebug()<<__LINE__<<"/****************adc sync again****************/";
    }

    if( chCnt == 2)
    {
        //qDebug()<<"adc align"<<aligns[0]<<aligns[1]<<aligns[2]<<aligns[3];

        if(aligns[0] == aligns[1] && aligns[2] == aligns[3] )
        {
//            qDebug()<<"calAdcGroup End"<<getDebugTime();
            return ERR_NONE;
        }
        //qDebug()<<__LINE__<<"/****************adc sync again****************/";
    }

    if( chCnt == 4)
    {
        return ERR_NONE;
    }

    //        qDebug()<<"adc align"<<aligns[0]<<aligns[1]<<aligns[2]<<aligns[3];
    //        qWarning()<<"adc sync fail";
    //! 逻辑移除了check(3)的回读，不能读取带校准的图样

    //        Q_ASSERT(false);


    //    //! align
    //    int norms[4];
    //    err = alignAdcGroup( chCnt, abcd, norms );
    //    if ( err != ERR_NONE )
    //    { return err; }

    //    //! config
    //    configAdcGroup( chCnt, norms );
//    qDebug()<<"calAdcGroup End"<<getDebugTime();

    return ERR_NONE;
}

DsoErr CDsoRecEngine::snapAdcGroup(dso_phy::IPhyADC::eAdcInterleaveMode eIntMode,
                                   Spu_reg *pABCD,
                                   DsoWorkAim aim,
                                   int chCnt)
{
    Q_ASSERT( NULL != pABCD );

    IPhyADC *pAdc;
    int adcCnt;

    //! --- adc
    pAdc = &m_pPhy->mAdc[0];
    adcCnt = m_pPhy->getAdcCnt();

    //! dispen
    Wpu_reg enBm = m_pPhy->mWpu.getanalog_mode_analog_ch_en();
    Spu_reg invAdc = m_pPhy->mSpu.getCTRL_adc_data_inv();

    //! mode change
    if ( pAdc->getMode() != (quint32)eIntMode )
    {
        //! \todo disable wpu
        m_pPhy->mWpu.outanalog_mode_analog_ch_en( 0 );
        m_pPhy->mSpu.setCTRL_adc_data_inv( 0 );     //! not invert

        //! foreach adc
        for( int i = 0; i < adcCnt; i++ )
        {
            pAdc[i].setMode( (quint32)eIntMode, aim );
        }
    }
    else
    { return ERR_NONE; }

    //! set adc mode
    for ( int i = 0; i < adcCnt; i++ )
    {
        pAdc[i].setAdcInteMode( eIntMode );
    }

    for( int i = 0; i < adcCnt; i++ )
    {
        pAdc[i].setRMOD( 1 );
        pAdc[i].setTest( 1 );
        pAdc[i].flushWCache();
    }

    //! ---- spu
    IPhySpu *pSpu;

    pSpu = &m_pPhy->mSpu;
    pSpu->pulseCTRL_spu_clr( 1, 0, 1 );
    pSpu->setCtrl_adcsync();
    pSpu->setDEBUG_gray_bypass( 1 );
    pSpu->flushWCache();

    //! start check
    pSpu->setCTRL_adc_align_check( 2 );
    pSpu->flushWCache();

    QThread::usleep( 1 );

    //! ---- get data
    *pABCD = pSpu->inADC_CHN_ALIGN_CHK();

    //! align
    int norms[4];
    DsoErr err = alignAdcGroup( chCnt, *pABCD, norms);
    if ( err != ERR_NONE )
    {
        qDebug()<<*pABCD;
        Q_ASSERT(false);
    }

    //! config
    configAdcGroup( chCnt, norms );
    m_pPhy->flushWCache();
    pSpu->pulseCTRL_spu_clr( 1, 0, 1 );
    //! check 3
    pSpu->setCTRL_adc_align_check( 3 );
    pSpu->flushWCache();

    //! ---- get data
    *pABCD = pSpu->inADC_CHN_ALIGN_CHK();

    //! end check
    //    pSpu->setCTRL_adc_align_check( 0 );
    pSpu->setDEBUG_gray_bypass( 0 );
    pSpu->flushWCache( );

    //    pSpu->pulseCTRL_spu_clr( 1, 0, 1 );
    //! adc exit test
    for ( int i = 0; i < adcCnt; i++ )
    {
        pAdc[i].setTest( 0 );
    }

    //    m_pPhy->mSpu.setSampClr();
    m_pPhy->mWpu.setanalog_mode_analog_ch_en( enBm );
    m_pPhy->mSpu.setCTRL_adc_data_inv( invAdc );

    //! \errant scu stoped by samp_clr
    //m_pPhy->mCcu.setCcuRun( 1 );
    //m_pPhy->mCcu.flushWCache();

    return ERR_NONE;
}

DsoErr CDsoRecEngine::alignAdcGroup(
        int chCnt,
        Spu_reg abcd,
        int norms[4] )
{
    //! register
    dso_phy::Spu_ADC_CHN_ALIGN_CHK alABCD;

    //! decode
    alABCD.payload = abcd;

    CHoriAlign horiAlign;

    int aligns[4];

    //! split data
    if ( chCnt == 1 )
    {
        aligns[0] = alABCD.adc_a;
        aligns[1] = alABCD.adc_b;
        aligns[2] = alABCD.adc_c;
        aligns[3] = alABCD.adc_d;

        horiAlign.alignQuad( aligns, norms );
    }
    else if ( chCnt == 2 )
    {
        //! ab
        aligns[0] = alABCD.adc_a;
        aligns[1] = alABCD.adc_b;

        horiAlign.alignDouble( aligns + 0, norms + 0 );

        //! cd
        aligns[2] = alABCD.adc_c;
        aligns[3] = alABCD.adc_d;

        horiAlign.alignDouble( aligns + 2, norms + 2 );
    }
    else if ( chCnt == 4 )
    {
        norms[0] = 0;
        norms[1] = 0;
        norms[2] = 0;
        norms[3] = 0;
    }
    else
    {
        return ERR_INVALID_CONFIG;
    }

    LOG_ADC_CAL()<<"align"<<aligns[0]<<aligns[1]<<aligns[2]<<aligns[3];
    LOG_ADC_CAL()<<"norm"<<norms[0]<<norms[1]<<norms[2]<<norms[3];

    return ERR_NONE;
}

DsoErr CDsoRecEngine::configAdcGroup( int chCnt,
                                      int norms[4] )
{
    IPhySpu *pSpu = &m_pPhy->mSpu;

    //! now to config
    if ( chCnt == 1 )
    {
        pSpu->setADC_CHN_ALIGN_adc_a( norms[0] );
        pSpu->setADC_CHN_ALIGN_adc_b( norms[1] );
        pSpu->setADC_CHN_ALIGN_adc_c( norms[2] );
        pSpu->setADC_CHN_ALIGN_adc_d( norms[3] );
    }
    else if ( chCnt == 2 )
    {
        pSpu->setADC_CHN_ALIGN_adc_a( norms[0] );
        pSpu->setADC_CHN_ALIGN_adc_b( norms[1] );
        pSpu->setADC_CHN_ALIGN_adc_c( norms[2] );
        pSpu->setADC_CHN_ALIGN_adc_d( norms[3] );
    }
    else if ( chCnt == 4 )
    {
        pSpu->setADC_CHN_ALIGN_adc_a( norms[0] );
        pSpu->setADC_CHN_ALIGN_adc_b( norms[1] );
        pSpu->setADC_CHN_ALIGN_adc_c( norms[2] );
        pSpu->setADC_CHN_ALIGN_adc_d( norms[3] );
    }
    else
    { return ERR_INVALID_CONFIG; }

    return ERR_NONE;
}
