#include "cdsorecengine.h"
#include "../base/search/enginecfg_search.h"

#if 0
#define LOG_DBG()    qDebug()<<__FILE__<<__FUNCTION__<<__LINE__
DsoErr CDsoRecEngine::searchApply()
{
    if ( m_pViewSession->m_search.getEnable() > 0 )
    {}
    else
    {
        LOG_DBG()<<"search off";
        m_pPhy->mCcu.setSearchClose();
        return ERR_NONE;
    }

    //! get t
    qlonglong mt0, mtLength;
    m_pViewSession->m_search.getRange( mt0, mtLength );

    //! view
    HorizontalView view;
    if ( m_pPhy->mCcu.getMODE_search_en_zoom() )
    {
        view = horizontal_view_zoom;
    }
    else
    {
        view = horizontal_view_main;
    }
    view = view;


    //! map dot
    qint32 offset, length;

    //! chan
    {
       CSaFlow saFlow;
       saFlow = mRecSession.m_acquire.mMainCHView.getFlow();
        ret = timeRangeMap(
                            &saFlow,
                            &saFlow,
                            mt0, mtLength, &offset, &length );

        LOG_DBG()<<offset << length;
        //! set length
        m_pPhy->mSpu.setSEARCH_OFFSET_CH( offset );
        m_pPhy->mSpu.setSEARCH_LEN_CH( length );
    }


    //! la
    {
        saFlow = mRecSession.m_acquire.mMainLAView.getFlow();
        ret = timeRangeMap( &saFlow,
                            &saFlow,
                            mt0, mtLength, &offset, &length );

        //! set length
        m_pPhy->mSpu.setSEARCH_OFFSET_LA( offset );
        m_pPhy->mSpu.setSEARCH_LEN_LA( length );

    }
    return ERR_NONE;
}
#endif
