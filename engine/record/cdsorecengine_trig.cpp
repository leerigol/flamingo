#include "cdsorecengine.h"

DsoErr CDsoRecEngine::setTrigSweep( TriggerSweep sweep )
{
    dsoEngine::setTrigSweep( sweep );

    //! wave clear
    //m_pPhy->mWpu.syncClear();//by hxh

    //! led proc
    //! in running now
    if ( sweep == Trigger_Sweep_Normal )
    {
        m_pPhy->mLed.seLedOp( led_run, true );
        m_pPhy->mLed.seLedOp( led_single, false );
    }
    else if ( sweep == Trigger_Sweep_Auto)
    {
        m_pPhy->mLed.seLedOp( led_run, true );
        m_pPhy->mLed.seLedOp( led_single, false );
    }
    else
    {}

    return ERR_NONE;
}
