#include "cdsorecengine.h"

#define selfCon     m_pViewSession->m_control

//! control
DsoErr CDsoRecEngine::conApply()
{
    TriggerSweep sweep;

    sweep = m_pViewSession->m_trigger.getSweep();

    //! trigger sweep
    applyTrigSweep( sweep );

    //! fsm delay
    m_pPhy->mScu.inREC_PLAY();
    if ( m_pPhy->mScu.getREC_PLAY_mode_rec() )
    {}
    else
    { m_pPhy->mCcu.setFSM_DELAY( 0 ); }

    //! to record mode
    m_pPhy->mScuGp.setREC_PLAY_mode_play( 0 );

    return ERR_NONE;
}

DsoErr CDsoRecEngine::fastApply()
{
    CAcquire* pAcquire = &mRecSession.m_acquire;
    //! fast 无zoom无single无mask无la无区域触发
    //! 条件为无内插压缩、抽取
    bool mLaOnOff = mRecSession.mLa.getOnOff();

    /*! [dba: 2018-04-22] bug: 2993*/
    mRecSession.m_bFastTimeBase = false;

    if( mRecSession.m_acquire.mCHFlow.mDepth <=  1250 &&
            (mRecSession.m_horizontal.getViewMode() != Horizontal_Zoom)
            && (!mLaOnOff)
            && (mRecSession.m_acquire.m_eAcquireMode != Acquire_Average)
            && (m_RecAction != Rec_start) )
    {
        if( pAcquire->mMainCHView.mIntx == 1 &&
            (pAcquire->getChSaRate() * pAcquire->m_chCnt) == 1e16 &&
            (m_pViewSession->m_trigger.getSweep() != Trigger_Sweep_Single) )
        {
            if(mRecSession.m_mask.mMaskEn || mRecSession.m_mask.mZoneEn)
            {
                m_pPhy->mCcu.setMODE_mode_fast_ref(0);
            }
            else
            {
                m_pPhy->mCcu.setMODE_mode_fast_ref(1);

                m_pPhy->mWpu.outtime_base_mode(8);
                //! fast模式的段大小不再由软件配置
                //m_pPhy->mSpuGp.setSEGMENT_SIZE( pAcquire->m_chCnt * 2048);
            }

            //! 无论是否已经打开fast，都需要做一个fast时基标记，供mask开关的时候进行检测
            mRecSession.m_bFastTimeBase = true;
        }
        else
        {
            m_pPhy->mCcu.setMODE_mode_fast_ref(0);
        }
    }
    else
    {
        m_pPhy->mCcu.setMODE_mode_fast_ref(0);
    }
    return ERR_NONE;
}

DsoErr CDsoRecEngine::maskApply()
{
    if(mRecSession.m_mask.mMaskEn)
    {
        //!更新参数
        applyMask(&mRecSession);

        //! 退出run(回放时)先关闭mask使能，解决卡顿问题,重新打开
        //m_pPhy->mSpuGp.outMASK_COMPRESS_en( true );
        m_pPhy->mCcu.setMODE_mask_pf_en( true );
    }
    m_pPhy->mCcu.setMODE_zone_trig_en( mRecSession.m_mask.mZoneEn );

    return ERR_NONE;
}
