
#include "cdsorecengine.h"

//! local macros
#define on_cmd( msg, id, para, proc, pAttr )   ON_COMMAND( CDsoRecEngine, msg, id, para, proc, pAttr )
#define on_range_id( msg, id1, id2, para, proc, pAttr )    ON_RANGE_ID(CDsoRecEngine, msg, id1, id2, para, proc, pAttr)

#define on_noid_cmd( msg, para, proc, pAttr ) ON_COMMAND( CDsoRecEngine, msg, E_SERVICE_ID_NONE, para, proc, pAttr )
#define on_noid_cmd_context( msg, para, proc, pAttr, context ) \
                                       ON_COMMAND_CONTEXT( CDsoRecEngine, msg, E_SERVICE_ID_NONE, para, proc, pAttr, context )

#define on_noid_stack( msg, proc, pAttr, stackSize, type )   \
                                        ON_COMMAND_STACK( CDsoRecEngine, \
                                        msg, \
                                        E_SERVICE_ID_NONE, \
                                        type, \
                                        proc, \
                                        pAttr,\
                                        stackSize )


#include "../engineentry.h"

////! context
//static stopConfigContext _stopCfgContext;

//! message table
IMPLEMENT_MSG_MAP( dsoEngine, CDsoRecEngine )
//ON_COMMAND( CDsoRecEngine, ENGINE_TMO, E_SERVICE_ID_SYSTEM, E_PARA_S32, onTimeout ),

//! ch oper.
on_range_ch( ENGINE_CH_ON, E_PARA_RANGE_BOOL, chOnOff, NULL ),
on_range_ch( ENGINE_CH_INVERT, E_PARA_RANGE_BOOL, chInvert, NULL ),
on_range_ch( ENGINE_CH_IMPEDANCE, E_PARA_RANGE_S32, chImpedance, NULL ),

//! \notice no need to playback by scale as the OFFSET is following
//on_range_ch( ENGINE_CH_SCALE, E_PARA_RANGE_S32, chScale ),
on_range_ch( ENGINE_CH_OFFSET, E_PARA_RANGE_S32, chOffset, NULL ),
on_range_ch( ENGINE_CH_BANDLIMIT, E_PARA_RANGE_S32, chBandwidth, NULL ),
on_range_ch( ENGINE_CH_COUPLING, E_PARA_RANGE_S32, chCoupling, NULL ),

//! no play
on_range_ch( ENGINE_CH_DELAY, E_PARA_RANGE_S32, chDelay, &dsoEngine::_noPlay ),

on_range_ch( ENGINE_PROBE_IMPEDANCE, E_PARA_RANGE_S32, chProbeImpedance, NULL ),
on_range_ch( ENGINE_PROBE_DELAY, E_PARA_RANGE_S32, chProbeDelay, NULL ),

//on_noid_cmd_ptr( ENGINE_CH_LAYER, chLayer ),

//! get probe

//! la op.
on_noid_cmd_bool( ENGINE_LA_EN, setLaEn, NULL ),
on_noid_cmd_s32( ENGINE_LA_ON, setLaOnOff, NULL ),
on_noid_cmd_s32( ENGINE_LA_ACTIVE, setLaActive, NULL ),
on_noid_cmd_s32s32( ENGINE_LA_GROUP, setLaGroup, NULL ),

on_noid_cmd_s32( ENGINE_LA_SIZE, setLaSize, NULL ),
on_noid_cmd_ptr( ENGINE_LA_POS, setLaPos, NULL ),

on_noid_cmd_s32s32( ENGINE_LA_COLOR, setLaColor, NULL ),

//! control
//on_noid_cmd_s32( ENGINE_CONTROL_ACTION, setControlAction, NULL ),

//! hori
on_noid_cmd_s32( ENGINE_HORI_VIEW_MODE, setViewMode, NULL ),

//on_noid_cmd_ptr_context( ENGINE_MAIN_CFG, setMainCfg, &_stopCfgContext ),
//on_noid_cmd_s64_context( ENGINE_MAIN_OFFSET, setMainOffset, &_stopCfgContext ),

on_noid_cmd_s64( ENGINE_MAIN_SCALE, setMainScale, NULL ),
on_noid_cmd_s64( ENGINE_MAIN_OFFSET, setMainOffset, NULL ),

on_noid_cmd_s64( ENGINE_ZOOM_SCALE, setZoomScale, NULL ),
on_noid_cmd_s64( ENGINE_ZOOM_OFFSET, setZoomOffset, NULL ),

on_noid_get_ptr( qENGINE_HORI_INFO, queryHoriInfo ),
on_noid_get_s32void( qENGINE_HORI_LIVE_CH_CNT, getLiveCHCnt ),

////! acquire
on_noid_cmd_s32( ENGINE_ACQUIRE_MODE, setAcquireMode, NULL ),
on_noid_cmd_s32( ENGINE_ACQUIRE_AVG_CNT, setAverageCount, NULL ),
on_noid_cmd_s32( ENGINE_ACQUIRE_TIMEMODE, setTimeMode, NULL ),
//on_noid_cmd_s32( ENGINE_ACQUIRE_INTERPLATE_MODE, setInterplateMode ),
//on_noid_cmd_s32( ENGINE_ACQUIRE_TIMEMODE, setTimeMode ),

//on_noid_cmd_s32( ENGINE_ACQUIRE_SAMPLE_MODE, setSampleMode ),
on_noid_cmd_bool( ENGINE_ACQUIRE_ANTI_ALIASING, setAntiAliasing, NULL),

on_noid_cmd_s32( ENGINE_ACQUIRE_DEPTH, setMemDepth, NULL ),
//on_noid_cmd_s32( ENGINE_ACQUIRE_LA_DEPTH, setLaMemDepth ),

//on_noid_cmd_s64( ENGINE_ACQUIRE_SARATE, setSamplRate ),
//on_noid_cmd_s64( ENGINE_ACQUIRE_LA_SARATE, setLaSamplRate ),


//on_noid_get_s64void( qENGINE_ACQUIRE_CH_LENGTH, getChLength ),
//on_noid_get_s64void( qENGINE_ACQUIRE_LA_LENGTH, getLaLength ),

//on_noid_get_s64void( qENGINE_ACQUIRE_SARATE, getSampleRate ),
//on_noid_get_s64void( qENGINE_ACQUIRE_LA_SARATE, getLaSampleRate ),

//on_noid_cmd_void( ENGINE_ACQUIRE_TEST, snapAcquireState ),
on_noid_cmd_s32( ENGINE_ACQUIRE_BAND, setSysRawBand, NULL ),



////! trigger
on_noid_cmd_s32s32( ENGINE_TRIGGER_COUPLING,  setTrigCoupling, NULL ),
on_noid_cmd_s32(    ENGINE_TRIGGER_SWEEP,     setTrigSweep,    NULL ),
on_noid_cmd_u64(    ENGINE_TRIGGER_HOLD_TIME, setTrigHoldTime, NULL ),

//on_noid_cmd_stack( ENGINE_TRIGGER_LEVEL_A, setTrigLevelA, NULL, 3 ),
//on_noid_cmd_stack( ENGINE_TRIGGER_LEVEL_B, setTrigLevelB, NULL, 3 ),

on_noid_cmd_s32bool( ENGINE_TRIGGER_CH_EN, setTrigCHEn, NULL ),

on_noid_cmd_bool( ENGINE_TRIGGER_ZONE_EN,  setTrigZoneEn,  NULL   ),
on_noid_cmd_ptr(  ENGINE_TRIGGER_ZONE_CFG, setTrigZoneCfg, NULL   ),

//! dvm counter
on_noid_cmd_s32bool( ENGINE_DVM_COUNTOR_CH_EN, setDvmCountCHEn, NULL),

//! measure
on_noid_cmd_u32( ENGINE_MEAS_EN, setMeasEn, NULL ),
on_noid_cmd_stack( ENGINE_MEAS_RANGE, setMeasRange, NULL, 2 ),  // t0, tlength
on_noid_cmd_s32( ENGINE_MEAS_VIEW, setMeasView, NULL ),
////! get trace
//on_noid_cmd_s32( ENGINE_REQUEST_TRACE, requestTrace ),
//on_noid_cmd_ptr( qENGINE_TRACE, getTrace ),

//! display
on_noid_cmd_s32ptr( ENGINE_DISP_CH_ORDERS, setChDispOrder, NULL ),
on_noid_cmd_ptr( ENGINE_DISP_SCREEN, setScrAttr, NULL ),
//on_noid_cmd_s32( ENGINE_DISP_WFM_LIGHT, setIntensitity, NULL ),

//! record&play
on_noid_cmd_s32( ENGINE_RECORD_ACTION, setRecordAction, NULL ),
on_noid_cmd_s64( ENGINE_RECORD_INTERVAL, setRecordInterval, NULL ),
on_noid_cmd_s32( ENGINE_RECORD_FRAMES, setRecordFrames, NULL ),

on_noid_cmd_u32(   ENGINE_SEARCH_EN,    setSearchEn, NULL ),

//by hxh. 通道关闭，修改偏移或档位时，引起了搜索阈值的配置，
//认为搜索阈值配置不需要回放
//on_noid_cmd_stack( ENGINE_SEARCH_LEVEL_A, setSearchLevelA, NULL, 3 ),
//on_noid_cmd_stack( ENGINE_SEARCH_LEVEL_B, setSearchLevelB, NULL, 3 ),

END_OF_ITEM()

