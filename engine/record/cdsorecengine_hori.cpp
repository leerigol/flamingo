#include "cdsorecengine.h"
#include "../../baseclass/resample/reSample.h"

#include "../../phy/hori/iphyhori.h"
#include "../../meta/crmeta.h"

#define selfHori        m_pViewSession->m_horizontal
#define selfHoriMain    m_pViewSession->m_horizontal.getMainView()
#define selfHoriZoom    m_pViewSession->m_horizontal.getZoomView()


#define eye_size    1000000
#define mask_size   1000

#define batch_max_plot_num  4000

//! hori
DsoErr CDsoRecEngine::setViewMode( HorizontalViewmode view )
{
    selfHori.setViewMode( view );

    return ERR_NONE;
}

DsoErr CDsoRecEngine::setMainCfg( EngineHoriReq *pHori )
{
    //! select sa
    CHoriCfg *pCfg;
    pCfg = m_pPhy->m_hori.mHoriCfgMgr.selectSa(
                                         toHoriMode( pHori->mChCnt ),
                                         pHori->mDepth,
                                         pHori->mSaScale,
                                         pHori->mSaOffset,
                                         mRecSession.m_acquire.mCHFlow,
                                         mRecSession.m_acquire.mLaFlow );

    if ( pCfg == NULL )
    { return ERR_INVALID_CONFIG; }

    Q_ASSERT( mRecSession.m_acquire.mCHFlow.mDepth > 0 );
    Q_ASSERT( mRecSession.m_acquire.mLaFlow.mDepth > 0 );

    if( mRecSession.m_horizontal.getViewMode() ==  Horizontal_Main)
    {
        mRecSession.m_acquire.mMainCHCoarseView.cfgTransfer( 1000000 );
        mRecSession.m_acquire.mMainCHView.cfgTransfer( 1000000 );

        mRecSession.m_acquire.mZoomCHCoarseView.cfgTransfer( 1000000 );
        mRecSession.m_acquire.mZoomCHView.cfgTransfer( 1000000 );
    }
    else
    {
        mRecSession.m_acquire.mMainCHCoarseView.cfgTransfer( 500000 );
        mRecSession.m_acquire.mMainCHView.cfgTransfer( 500000 );

        mRecSession.m_acquire.mZoomCHCoarseView.cfgTransfer( 500000 );
        mRecSession.m_acquire.mZoomCHView.cfgTransfer( 500000 );
    }

    //! build views
    qlonglong normScale = ll_roof125( pHori->mScale );
    mRecSession.m_acquire.mMainCHCoarseView.viewOn(   mRecSession.m_acquire.mCHFlow,
                                                normScale,
                                                pHori->mOffset);
    if(normScale == pHori->mScale)
    {
        mRecSession.m_acquire.mMainCHView.viewOn(   mRecSession.m_acquire.mCHFlow,
                                                    pHori->mScale,
                                                    pHori->mOffset );
    }
    else
    {
        mRecSession.m_acquire.mMainCHView.viewOnFine( &mRecSession.m_acquire.mMainCHCoarseView,
                                                      mRecSession.m_acquire.mCHFlow,
                                                      normScale,
                                                      pHori->mScale,
                                                      pHori->mOffset );
    }

    //! 计算LA的内插，压缩系数
    mRecSession.m_acquire.mMainLAView = mRecSession.m_acquire.mMainCHView;
    CFlowView* pLaView = &mRecSession.m_acquire.mMainLAView;
    pLaView->viewOnLa(&mRecSession.m_acquire.mMainCHView);

    //! eye flow
    mRecSession.m_acquire.mEyeView.viewOn( mRecSession.m_acquire.mCHFlow, eye_size );

    //! mask flow: screen memory -> 1000, not mMainCHView -> 1000
    mRecSession.m_acquire.mMaskView.viewOn( mRecSession.m_acquire.mMainCHView.getFlow(),
                                              mask_size );
    mRecSession.m_acquire.mMaskView.mComp *= mRecSession.m_acquire.mMainCHView.mCompL1;

    //! ---- set acquire sa info length
    mRecSession.m_acquire.setChLength( mRecSession.m_acquire.mCHFlow.mDepth );
    mRecSession.m_acquire.setLaLength( mRecSession.m_acquire.mLaFlow.mDepth );

    mRecSession.m_acquire.setChSaRate( mRecSession.m_acquire.mCHFlow.getSa(),
                             mRecSession.m_acquire.mCHFlow.mDotTime );
    mRecSession.m_acquire.setLaSaRate( mRecSession.m_acquire.mLaFlow.getSa(),
                             mRecSession.m_acquire.mLaFlow.mDotTime );

    mRecSession.m_acquire.setCHCnt( pHori->mChCnt, pHori->mLaCnt );
    mRecSession.setValid( true );

    //! scale
    mRecSession.m_horizontal.getMainView().setScale( pHori->mScale );

    //! ---- config phy
    LOG_HORI()<<"record len"<<mRecSession.m_acquire.mCHFlow.mDepth.M()<<mRecSession.m_acquire.mCHFlow.mDepth.N();
    LOG_HORI()<<"T0"<<mRecSession.m_acquire.mCHFlow.getT0().M()<<mRecSession.m_acquire.mCHFlow.getT0().N();
    LOG_HORI()<<"down sample("<<pCfg->mEh<<","<<pCfg->mEl<<")";

    //! fill hcfg
    HoriPara hCfg;
    int chCnt;
    chCnt = toChCnt( m_pViewSession->getSaCHBm() );

    hCfg.setPath( data_path_main );
    hCfg.setSa( pCfg->mCHFlow.mDepth.ceilLonglong(), pCfg->mEl, pCfg->mEh );
    hCfg.setAcquire( m_pViewSession->m_acquire.getAcquireMode(), chCnt );

    //! scan
    hCfg.mAcqTimeMode = mRecSession.m_acquire.m_eTimeMode;
    if( hCfg.mAcqTimeMode == Acquire_SCAN)
    {
        hCfg.setTrigCol(  mRecSession.m_acquire.mMainCHView.mTrigPos.ceilInt() );
    }

    CFlowView *pView;
    pView = &mRecSession.m_acquire.mMainCHView;

    hCfg.setCH( pView->mIntx,
                pView->mCompL1,
                pView->mFixCompL2,
                pView->mTransferLen,
                pView->mMStart, pView->mMLen,
                pView->mLinearIntxOn,
                pView->mLinearIntx.mM, pView->mLinearIntx.mN
                );

    hCfg.setLa( pLaView->mIntx, pLaView->mCompL1 );

    pView = &mRecSession.m_acquire.mEyeView;
    hCfg.setEye( pView->mIntx,
                 pView->mComp,
                 pView->getFlow().mDepth.ceilLonglong() );

    pView = &mRecSession.m_acquire.mMaskView;
    hCfg.setMask( pView->mIntx,
                  pView->mComp );

    m_pPhy->m_hori.setMainCfg( hCfg );

    //! eye pipe
    if ( mRecSession.m_acquire.mEyeView.mIntx > m_pPhy->m_hori.getMaxInterp() )
    { m_pPhy->m_trace.setPipeOpen( IPhyTrace::trace_eye, false ); }
    else
    { m_pPhy->m_trace.setPipeOpen( IPhyTrace::trace_eye, true ); }

    //! log
    pView = &mRecSession.m_acquire.mMainCHView;
    LOG_HORI()<<"CH: chCnt/intx/L1/L2/transLen/mStart/mLen"<<chCnt<<pView->mIntx
             <<pView->mCompL1<<pView->mFixCompL2<<pView->mTransferLen<<pView->mMStart<<pView->mMLen;
    LOG_HORI()<<"CH Linear intx: mM"<<pView->mLinearIntx.mM
             <<"CH Linear intx: mN"<<pView->mLinearIntx.mN;
    pView = &mRecSession.m_acquire.mMainLAView;
    LOG_HORI()<<"LA: chCnt/intx/L1/L2/transLen"<<chCnt<<pLaView->mIntx
             <<pLaView->mCompL1<<pLaView->mFixCompL2<<pLaView->mTransferLen;
    pView = &mRecSession.m_acquire.mEyeView;
    LOG_HORI()<<"eye: chCnt/intx/comp/length"<<chCnt<<pView->mIntx
             <<pView->mComp<<pView->getFlow().mDepth.mM<<pView->getFlow().mDepth.mN;
    pView = &mRecSession.m_acquire.mMaskView;
    LOG_HORI()<<"mask:"<<pView->mComp;

    //! 在慢扫描模式下， posSa、segmentSize和RecordLen需要增加两列
    int posSaSuffix = 0;
    if(mRecSession.m_acquire.m_eTimeMode == Acquire_SCAN)
    {
        posSaSuffix = 2 * ( mRecSession.m_horizontal.getMainView().getScale() / adc_hdiv_dots
                   /mRecSession.m_acquire.mCHFlow.mDotTime.toLonglong() );
        cfgScanPos();
        quint32 recLen = 0;
        recLen = m_pPhy->mSpu.getRECORD_LEN();
        m_pPhy->mSpuGp.setRecordLen( recLen + posSaSuffix);
    }
    else
    {
        posSaSuffix = 0;
    }

    //! offset
    DsoErr err;
    pView = &mRecSession.m_acquire.mMainCHView;
    err = cfgMainOffset( pHori->mOffset,
                         posSaSuffix);

    //! 配置精细延迟
    quint32 finePos;
    if( fineTrigPosition( finePos, mRecSession.m_acquire.mMainCHCoarseView.getFlow().getDotTime().ceilLonglong(),
                          mRecSession.m_horizontal.getMainView().getScale(),
                          mRecSession.m_horizontal.getMainView().getOffset(),
                          mRecSession.m_acquire.mMainCHCoarseView.mIntx ) )
    {
        m_pPhy->mSpuGp.setFINE_DELAY_MAIN_chn_delay( finePos );
    }

    //! 配置ADC通道延迟
    err = cfgAdcChDelay();

    cfgSegmentSize(posSaSuffix);

    //! roll:关掉通道延迟
    if(mRecSession.m_acquire.m_eTimeMode == Acquire_ROLL)
    {
        //逻辑代码当中在滚动模式下自己关闭
//        m_pPhy->mSpuGp.outCOARSE_DELAY_MAIN_en( 0 );
//        m_pPhy->mSpuGp.outFINE_DELAY_MAIN_en( 0 );
//        m_pPhy->mSpuGp.outCOARSE_DELAY_ZOOM_en( 0 );
//        m_pPhy->mSpuGp.outFINE_DELAY_ZOOM_en( 0 );
    }
    else
    {
        m_pPhy->mSpuGp.setINTERP_MULT_MAIN_FINE_en( 1 );
        m_pPhy->mSpuGp.setCOARSE_DELAY_MAIN_delay_en( 1 );
        m_pPhy->mSpuGp.setFINE_DELAY_MAIN_delay_en( 1 );
        m_pPhy->mSpuGp.setCOARSE_DELAY_ZOOM_delay_en( 1 );
        m_pPhy->mSpuGp.setFINE_DELAY_ZOOM_delay_en( 1 );

        //! add_by_zx: tr6晚 for 1k wave bug
        if((mRecSession.m_horizontal.getMainView().getScale() == time_us(200)
            || mRecSession.m_horizontal.getMainView().getScale() == time_us(100))
                && mRecSession.m_acquire.mMainCHView.mCompL1 >= 10)
        {
            m_pPhy->mSpuGp.setCOARSE_DELAY_MAIN_delay_en(0);
        }
    }

    return err;
}

DsoErr CDsoRecEngine::setZoomCfg( EngineHoriReq *pHori )
{
    Q_ASSERT( NULL != pHori );

    //! build views
    qlonglong normScale = ll_roof125( pHori->mScale );
    mRecSession.m_acquire.mZoomCHCoarseView.viewOn(   mRecSession.m_acquire.mCHFlow,
                                                normScale,
                                                pHori->mOffset);
    if(normScale == pHori->mScale)
    {
        mRecSession.m_acquire.mZoomCHView.viewOn(   mRecSession.m_acquire.mCHFlow,
                                                    pHori->mScale,
                                                    pHori->mOffset );
    }
    else
    {
        mRecSession.m_acquire.mZoomCHView.viewOnFine( &mRecSession.m_acquire.mZoomCHCoarseView,
                                                      mRecSession.m_acquire.mCHFlow,
                                                      normScale,
                                                      pHori->mScale,
                                                      pHori->mOffset);
    }

    //! 计算LA的内插，压缩系数
    mRecSession.m_acquire.mZoomLAView = mRecSession.m_acquire.mZoomCHView;
    CFlowView* pLaView = &mRecSession.m_acquire.mZoomLAView;
    pLaView->viewOnLa(&mRecSession.m_acquire.mZoomCHView);

    //! fill hcfg
    HoriPara hCfg;

    int chCnt;
    chCnt = toChCnt( m_pViewSession->getSaCHBm() );

    hCfg.setPath( data_path_zoom );

    hCfg.setSa( mRecSession.m_acquire.mCHFlow.mDepth.ceilLonglong(), 1, 1 );
    hCfg.setAcquire( m_pViewSession->m_acquire.getAcquireMode(),
                     chCnt );

    CFlowView *pView;
    pView = &mRecSession.m_acquire.mZoomCHView;
    hCfg.setCH( pView->mIntx,
                pView->mCompL1,
                pView->mFixCompL2,
                pView->mTransferLen,
                pView->mMStart, pView->mMLen,
                pView->mLinearIntxOn,
                pView->mLinearIntx.mM, pView->mLinearIntx.mN
                );

    hCfg.setLa( pLaView->mIntx, pLaView->mCompL1 );
    m_pPhy->m_hori.setZoomCfg( hCfg );

    //! zoom view
    pView = &mRecSession.m_acquire.mZoomCHView;
    LOG_HORI()<<"Zoom/SaScale/zoomScale"<<pHori->mSaScale<<pHori->mScale;
    LOG_HORI()<<"CH: chCnt/intx/L1/L2/transLen/mStart/mLen"<<chCnt<<pView->mIntx<<pView->mCompL1<<
                pView->mFixCompL2<<pView->mTransferLen<<pView->mMStart<<pView->mMLen;
    pView = &mRecSession.m_acquire.mZoomCHView;
    LOG_HORI()<<"LA: chCnt/intx/L1/L2/transLen"<<chCnt<<pView->mIntx<<pView->mCompL1<<pView->mFixCompL2<<pView->mTransferLen;

    //! zoom offset
    pView = &mRecSession.m_acquire.mZoomCHView;
    cfgZoomOffset( pHori->mOffset, pView->mIntx );

    quint32 finePos;
    if( fineTrigPosition( finePos, mRecSession.m_acquire.mZoomCHCoarseView.getFlow().getDotTime().ceilLonglong(),
                          mRecSession.m_horizontal.getZoomView().getScale(),
                          mRecSession.m_horizontal.getZoomView().getOffset(),
                          mRecSession.m_acquire.mZoomCHCoarseView.mIntx ) )
    {
        m_pPhy->mSpuGp.setFINE_DELAY_ZOOM_chn_delay( finePos );
        LOG_HORI()<<"zoom fine pos"<<finePos;
    }

    return ERR_NONE;
}

DsoErr CDsoRecEngine::cfgMainOffset( qlonglong offset,
                                     int posSaSuffix)
{
    //! dot time, depth
    dsoFract dotTime = mRecSession.m_acquire.mCHFlow.getDotTime().ceilLonglong();
    dsoFract depth = mRecSession.m_acquire.mCHFlow.getDepth();

    qlonglong memt0 = (offset - depth / 2 * dotTime ).ceilLonglong();

    if( memt0 % dotTime.ceilLonglong() != 0)
    {
        memt0 = (- memt0 + dotTime.ceilLonglong() -1)/dotTime.ceilLonglong() *
                dotTime.ceilLonglong();
        memt0 = - memt0;
        depth = depth + 1;
        mRecSession.m_acquire.mCHFlow.setLength(depth);
        m_pPhy->mSpuGp.setRecordLen( depth.ceilLonglong() + 1);
    }
    mRecSession.m_acquire.mCHFlow.setT0(memt0);

    //! offset
    //! ps * uHz
    //! e   12 * e6 = e18
    qlonglong memTime = ( depth * dotTime ).ceilLonglong();
    if ( memt0 < -memTime )
    { return ERR_OVER_LOW_RANGE; }

    struMem mem;
    mem.sett( memt0, dotTime.ceilLonglong() );
    mem.setLength( depth.ceilLonglong() );

    struView view;
    qlonglong viewt0 = offset - selfHoriMain.getScale()*( hori_div/2);
    view.sett( viewt0, selfHoriMain.getScale()/adc_hdiv_dots );
    view.setWidth( wave_width );

    if ( view.mapOfMem( &mem ) < 0 )
    { return ERR_INVALID_CONFIG; }

    //! now pre/post sa
    qint64 preSa, postSa;
    qint64 viewOff, viewLen;
    if ( memt0 < 0 )
    {
        //! ceil (preSa)
        preSa = (- memt0+ dotTime.ceilLonglong()-1)/( dotTime.ceilLonglong());
        postSa = ( depth - preSa).ceilLonglong();

        viewOff = mem.mPtL;
        viewLen = ( mem.mPtLen );
    }
    //! >= 0
    else
    {
        preSa = 0;
        postSa = ( depth + memt0/dotTime ).toLonglong();

        viewOff = mem.mPtL;
        viewLen = ( mem.mPtLen );
    }

    //! ---- config phy
    //! \errant fine trig delay +1
    m_pPhy->mScuGp.setPrePostSa( preSa + 1, postSa + posSaSuffix );

    LOG_HORI()<<"preSa"<<(preSa + 1)<<"posSa"<<(postSa + posSaSuffix);

    m_pPhy->mSpuGp.setWAVE_OFFSET_MAIN( viewOff );
    //! zx add:防止由于对齐而导致的点数不足
    m_pPhy->mSpuGp.setWAVE_LEN_MAIN( viewLen + mRecSession.m_acquire.mMainCHView.mCompL1 * 8 );

    return ERR_NONE;
}

DsoErr CDsoRecEngine::cfgZoomOffset( qlonglong offset,
                                     quint32 /*intxp*/ )
{
    //! offset
    //! ps * uHz
    //! e   12 * e6 = e18
    qlonglong memT0 = mRecSession.m_acquire.mCHFlow.getT0().toLonglong();
    qlonglong viewt0 = offset - selfHoriZoom.getScale()*(hori_div/2);

    if(viewt0 > memT0)
    {
        int offset = ((viewt0 - memT0) / mRecSession.m_acquire.mCHFlow.getDotTime()).toLonglong();
        m_pPhy->mSpuGp.setWAVE_OFFSET_ZOOM( offset );
    }
    else
    {
        m_pPhy->mSpuGp.setWAVE_OFFSET_ZOOM( 0 );
    }
    return ERR_NONE;
}


//! \todo la
DsoErr CDsoRecEngine::cfgMainChDelay( quint32 intxp )
{
    ChTimeDelay timeDelay[4];

    //! set delay
    for ( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].setDelay( m_pViewSession->m_ch[i].getDelay() % 400 );
    }

    //! slove
    Q_ASSERT( intxp > 0 );
    for ( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].slove( mRecSession.m_acquire.mCHFlow.mDotTime.ceilLonglong(),
                            mRecSession.m_acquire.mCHFlow.mDotTime.ceilLonglong() / intxp );
    }

    //! align
    qint32 maxPre, maxPost, minDly;

    maxPre = timeDelay[0].mPreSa;
    maxPost = timeDelay[0].mPostSa;
    minDly = timeDelay[0].mCoarseDelay;
    for ( int i = 1; i < array_count(timeDelay); i++ )
    {
        maxPre = qMax( timeDelay[i].mPreSa, maxPre );
        maxPost = qMax( timeDelay[i].mPostSa, maxPost );
        minDly = qMin( timeDelay[i].mCoarseDelay, minDly );
    }

    //! realign
    for( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].align( minDly );
    }

    //! tune view length
    quint32 pre,post,viewLen;
    pre = m_pPhy->mScuGp.getPRE_SA()  + maxPre;
    post = m_pPhy->mScuGp.getPOS_SA() + maxPost;
    viewLen = m_pPhy->mSpu.getWAVE_LEN_MAIN() + maxPre + maxPost;

    m_pPhy->mScuGp.setPrePostSa( pre , post );

    m_pPhy->mSpuGp.setWAVE_LEN_MAIN( viewLen );

    LOG_HORI()<<"pre/post/viewoff/viewlen"<<pre<<post<<viewLen;

    //! config chx delay
    for( int i = 0; i < array_count(timeDelay); i++ )
    {
        m_pPhy->mSpu.setMainDelay( i,
                                   timeDelay[i].mCoarseDelay,
                                   timeDelay[i].mFineDelay );
    }

    return ERR_NONE;
}

//! \todo zoom delay

DsoErr CDsoRecEngine::cfgZoomChDelay(quint32 /*dispDelay*/, quint32 /*intxp*/ )
{
    return ERR_NONE;
}


DsoErr CDsoRecEngine::cfgAdcChDelay()
{
    uint adcDelay = 0;
    if(mRecSession.m_acquire.m_chCnt == 4)
    {
        for(int i = 0; i < 4; i++)
        {
            if(mRecSession.m_ch[i].getOnOff())
            {
                switch (i) {
                case 0:
                    adcDelay = (mRecSession.m_ch[0].getDelay() + time_ns(100))/time_ps(400);
                    m_pPhy->mSpuGp.setCHN_DELAY_AB_A( adcDelay );
                    break;
                case 1:
                    adcDelay = (mRecSession.m_ch[1].getDelay() + time_ns(100))/time_ps(400);
                    m_pPhy->mSpuGp.setCHN_DELAY_AB_B( adcDelay );
                    break;
                case 2:
                    adcDelay = (mRecSession.m_ch[2].getDelay() + time_ns(100))/time_ps(400);
                    m_pPhy->mSpuGp.setCHN_DELAY_CD_C( adcDelay );
                    break;
                case 3:
                    adcDelay = (mRecSession.m_ch[3].getDelay() + time_ns(100))/time_ps(400);
                    m_pPhy->mSpuGp.setCHN_DELAY_CD_D( adcDelay );
                    break;
                default:
                    break;
                }
            }
        }
    }
    else if(mRecSession.m_acquire.m_chCnt == 2)
    {
        uint adcDelay1 = 0;
        uint adcDelay2 = 0;
        for(int i = 0; i < 4; i++)
        {
            if(mRecSession.m_ch[i].getOnOff())
            {
                switch (i) {
                case 0:
                case 1:
                    adcDelay = (mRecSession.m_ch[i].getDelay() + time_ns(100))/200;
                    adcDelay2 = adcDelay / 2;
                    adcDelay1 = adcDelay - adcDelay2;
                    m_pPhy->mSpuGp.setCHN_DELAY_AB_A( adcDelay1 );
                    break;
                case 2:
                case 3:
                    adcDelay = (mRecSession.m_ch[i].getDelay() + time_ns(100))/200;
                    adcDelay2 = adcDelay / 2;
                    adcDelay1 = adcDelay - adcDelay2;
                    m_pPhy->mSpuGp.setCHN_DELAY_AB_B( adcDelay1 );
                    break;
                default:
                    break;
                }
            }
        }
    }
    else
    {
        for(int i = 0; i < 4; i++)
        {
            if(mRecSession.m_ch[i].getOnOff())
            {
                adcDelay = (mRecSession.m_ch[i].getDelay() + time_ns(100))/100;
                int mode = adcDelay % 4;
                qlonglong adcCoreDelay[4];
                for(int i = 0;i < mode;i++)
                {
                    adcCoreDelay[i] = adcDelay/4 +1;
                }
                for(int i = mode;i < 4;i++)
                {
                    adcCoreDelay[i] = adcDelay/4;
                }
                m_pPhy->mSpuGp.setCHN_DELAY_AB_A( adcCoreDelay[0] );
            }
        }
    }
    return ERR_NONE;
}

void CDsoRecEngine::cfgSegmentSize(int addSuffix)
{
    Q_UNUSED(addSuffix);
    if(m_RecAction != Rec_start)
    {
        if( m_pViewSession->m_acquire.getAcquireMode() == Acquire_Average )
        {
            //m_pPhy->mSpuGp.setSEGMENT_SIZE( mRecSession.m_acquire.m_chCnt * 25000000 + addSuffix);
            m_pPhy->mSpuGp.setSEGMENT_SIZE( mRecSession.m_acquire.m_chCnt * ((25000000 + 1024 + 2047) / 2048 * 2048 ) );
        }
        else
        {
            m_pPhy->mSpuGp.setSEGMENT_SIZE( 0x20000000 ); //! 2^29
        }
    }
    else
    {   //![dba+, bug:1765]
        qlonglong depth = mRecSession.m_acquire.mCHFlow.mDepth.ceilLonglong();
        int       chCnt = toChCnt( mRecSession.getSaCHBm() );
        Q_ASSERT( chCnt > 0 );

        qlonglong slice = (depth * chCnt + 1024 );

        //slice = (slice + 2047) / 2048 * 2048; //!逻辑要求2048Byte对齐
        slice   = (((slice>>11) + 1) <<11 );    //!逻辑要求向上2048Byte对齐

        qDebug()<<__FUNCTION__<<__LINE__
               <<"depth:"<<depth<<"slice:"<<slice<<"ch cnt"<<chCnt;

        m_pPhy->mSpuGp.setSEGMENT_SIZE((Spu_reg)slice);
    }
}

void CDsoRecEngine::cfgScanPos()
{
    CFlowView *pView = &mRecSession.m_acquire.mMainCHView;
    qlonglong scale = mRecSession.m_horizontal.getMainView().getScale();
    qlonglong offset = mRecSession.m_horizontal.getMainView().getOffset();

    if( pView->mTrigPos > 999 )
    {
        //! 触发点在屏幕右侧
//        m_pPhy->mScuGp.setPOS_SA_VIEW_trig_left_side( 0 );
//        m_pPhy->mScuGp.setPOS_SA_VIEW_len( 0 );
        m_pPhy->mScuGp.setSCAN_TRIG_POSITION_trig_left_side( 0 );
        m_pPhy->mScuGp.setSCAN_TRIG_POSITION_position( 0 );
        m_pPhy->mWpu.setscan_trig_cnt_scan_trig_col( 1000 );
        m_pPhy->mWpu.setscan_trig_cnt_scan_trig_addr( 1000 * pView->mFixCompL2
                                                      * toChCnt( mRecSession.getSaCHBm() ) / 8);
    }
    else if( pView->mTrigPos > 0 )
    {
        //! 触发点在屏幕内
//        m_pPhy->mScuGp.setPOS_SA_VIEW_trig_left_side( 0 );
//        m_pPhy->mScuGp.setPOS_SA_VIEW_len( 0 );
        m_pPhy->mScuGp.setSCAN_TRIG_POSITION_trig_left_side( 0 );
        m_pPhy->mScuGp.setSCAN_TRIG_POSITION_position( 0 );
        m_pPhy->mWpu.setscan_trig_cnt_scan_trig_col( pView->mTrigPos.toInt() );
        m_pPhy->mWpu.setscan_trig_cnt_scan_trig_addr( (pView->mTrigPos.toInt() ) *
                                                      pView->mFixCompL2 * toChCnt( mRecSession.getSaCHBm() ) / 8);
    }
    else
    {
        //! 触发点在屏幕左侧
        dsoFract memDot = (offset - 5 * scale)/mRecSession.m_acquire.mCHFlow.mDotTime;
//        m_pPhy->mScuGp.setPOS_SA_VIEW_trig_left_side( 1 );
//        m_pPhy->mScuGp.setPOS_SA_VIEW_len( memDot.ceilInt() );
        m_pPhy->mScuGp.setSCAN_TRIG_POSITION_trig_left_side( 1 );
        m_pPhy->mScuGp.setSCAN_TRIG_POSITION_position( memDot.ceilInt() );
        m_pPhy->mWpu.setscan_trig_cnt_scan_trig_col( 1 );
        m_pPhy->mWpu.setscan_trig_cnt_scan_trig_addr(
                    pView->mFixCompL2 * toChCnt( mRecSession.getSaCHBm() ) / 8 );
    }

    if(mRecSession.m_acquire.m_eTimeMode == Acquire_SCAN)
    {
        dsoFract posSaLastView = (offset + 5 * scale + scale/100)/mRecSession.m_acquire.mCHFlow.mDotTime;
        if( posSaLastView < scale/100/mRecSession.m_acquire.mCHFlow.mDotTime )
        {
            posSaLastView = scale/100/mRecSession.m_acquire.mCHFlow.mDotTime;
        }
        m_pPhy->mCcu.setPOS_SA_LAST_VIEW( posSaLastView.ceilInt() );
    }
}

bool CDsoRecEngine::finePosistion(
                              qlonglong /*saScale*/,
                              qlonglong saOffset,
                              quint32 intxp,
                              dsoFract dotTime,
                              quint32 &finePos )
{
    if ( intxp > 1 )
    {}
    else
    {
        finePos = 0;
        return false;
    }

    //! fine delay
    qlonglong shiftOffset, alignOffset;

    Q_ASSERT( dotTime.N() == 1 );
    qlonglong dotT = dotTime.M();
    Q_ASSERT( dotT > 0 );

    //! shift ps
    shiftOffset = adc_hdiv_dots*hori_div/2%intxp;
    shiftOffset = shiftOffset * dotT / intxp;

    //! sum offset
    alignOffset = shiftOffset + saOffset;
    alignOffset = (alignOffset) % dotT;
    if ( alignOffset < 0 )
    {
        alignOffset += dotT;
    }
    else
    {}
    //! fine pos
    //! x/intx = fineOffset / dotTime
    finePos = alignOffset * intxp/dotT;

    LOG_HORI()<<alignOffset<<dotT<<intxp<<saOffset;

    return true;
}

bool CDsoRecEngine::getPlayValid()
{
    if(mRecSession.m_acquire.getAcquireMode() == Acquire_Average &&
            mHistSession.m_acquire.getAcquireMode() != Acquire_Average)
    {
        return false;
    }

    if(mRecSession.m_acquire.getAcquireMode() != Acquire_Average &&
            mHistSession.m_acquire.getAcquireMode() == Acquire_Average)
    {
        return false;
    }

    return true;
}

DsoErr CDsoRecEngine::setAutoTrigTimeout( EngineHoriReq *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    dsoFract depth, dotTime;

    depth = mRecSession.m_acquire.mCHFlow.getDepth();
    dotTime = mRecSession.m_acquire.mCHFlow.getDotTime();

    qlonglong tdTmo;

    //! get from meta
    m_pViewSession->m_trigger.setTdAutoTmo(
                m_pViewSession->m_horizontal.getMainView().getScale() * 10 * 2); //! tdTmo = scale * 10 * 2;
    tdTmo = m_pViewSession->m_trigger.getTdAutoTmo();

    tdTmo = qMax(time_ms(100), tdTmo);

    m_pPhy->mCcu.setAUTO_TRIG_TIMEOUT( tdTmo );

    //! 加大hold_off时间可以有利于测到异常信号,但是在auto模式下刷新率会变慢
    qlonglong memTime = qMax(time_ms(10), m_pViewSession->m_horizontal.getMainView().getScale());
    m_pPhy->mCcu.setAUTO_TRIG_HOLD_OFF( memTime );
    return ERR_NONE;
}

DsoErr CDsoRecEngine::setZoomScale( qlonglong scale )
{
    if ( validatePlayScale(scale, &mRecSession) )
    {}
    else
    { return ERR_OVER_LOW_RANGE; }

    selfHoriZoom.setScale( scale );
    return ERR_NONE;
}

DsoErr CDsoRecEngine::queryHoriInfo( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    //! input
    int chCnt;
    chCnt = toChCnt( m_pViewSession->getSaCHBm() );

    pInfo->setInfo( m_pViewSession->m_horizontal.getMainView().getScale(),
                    m_pViewSession->m_horizontal.getMainView().getOffset(),
                    m_pViewSession->m_horizontal.getMainView().getScale(),
                    m_pViewSession->m_acquire.getChDepth(),
                    chCnt );

    //! output
    dsoEngine::queryHoriInfo( pInfo );

    return ERR_NONE;
}

DsoErr CDsoRecEngine::queryHoriInfoMain( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    //! input
    int chCnt;
    chCnt = toChCnt( m_pViewSession->getSaCHBm() );

    pInfo->setInfo(
                    ll_roof125( m_pViewSession->m_horizontal.getMainView().getScale() ),
                    m_pViewSession->m_horizontal.getMainView().getOffset(),
                    m_pViewSession->m_horizontal.getMainView().getScale(),
                    m_pViewSession->m_acquire.getChDepth(),
                    chCnt );

    //! output:查表(add_by_zx 注：在setCfg中又进行了一次查表)
    dsoEngine::queryHoriInfo( pInfo );

    if(m_pViewSession->m_horizontal.getMainView().getOffset() <
            pInfo->mOffMin)
    {
        m_pViewSession->m_horizontal.getMainView().setOffset(pInfo->mOffMin);
    }

    return ERR_NONE;
}
DsoErr CDsoRecEngine::queryHoriInfoZoom( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    //! input
    int chCnt;
    chCnt = toChCnt( m_pViewSession->getSaCHBm() );

    pInfo->setInfo(
                    ll_roof125( m_pViewSession->m_horizontal.getMainView().getScale() ),
                    m_pViewSession->m_horizontal.getMainView().getOffset(),
                    m_pViewSession->m_horizontal.getZoomView().getScale(),
                    m_pViewSession->m_acquire.getChDepth(),
                    chCnt );

    //! output
    dsoEngine::queryHoriInfo( pInfo );
    if(m_pViewSession->m_horizontal.getZoomView().getOffset() <
            pInfo->mOffMin)
    {
        m_pViewSession->m_horizontal.getZoomView().setOffset(pInfo->mOffMin);
    }


    return ERR_NONE;
}

DsoErr CDsoRecEngine::horiApply()
{
    EngineHoriReq hReq;

    quint32 bm = m_pViewSession->getSaCHBm();

    //! ---- view tune
    horiViewTune( &mRecSession );

    //! ---- main config
    {
        if( m_pViewSession->m_acquire.m_eTimeMode == Acquire_ROLL )
        {
            mRecSession.m_acquire.mCHFlow.mRollSaSum = 50000000;
            mRecSession.m_acquire.mLaFlow.mRollSaSum = 50000000;//? 待验证
            hReq.setOffset( selfHoriMain.getRollOffset() );
        }
        else
        {
            hReq.setOffset( selfHoriMain.getOffset() );
        }

        queryHoriInfoMain( &hReq );
        //! emit info
        emit sigHoriInfo( hReq );

        emit sigHoriChanged( hReq );

        //! set main info
        setMainCfg( &hReq );

        //! auto trig hold off/auto_trig_delay
        setAutoTrigTimeout( &hReq );
    }

    //! ---- zoom cfg
    if ( selfHori.getViewMode() == Horizontal_Zoom )
    {
        queryHoriInfoZoom( &hReq );

        hReq.setOffset( selfHoriZoom.getOffset() );

        setZoomCfg( &hReq );

        m_pPhy->mWpu.setzoom_en_en( 1 );
        m_pPhy->mCcu.setMODE_zoom_en( 1 );
    }
    else
    {
        m_pPhy->mWpu.setzoom_en_en( 0 );
        m_pPhy->mCcu.setMODE_zoom_en( 0 );
    }

    //! ---- ccu
    ccuApply( bm );

    //! wpu
    applyDispOrder( m_pViewSession->getShowCHBm() );
    wpuApply( bm, m_pViewSession->getShowCHBm() );

    return ERR_NONE;
}

void CDsoRecEngine::wpuApply( quint32 saBm, quint32 showBm )
{
    int chCnt;

    chCnt = chBmCnt( saBm );

    IPhyWpu *pWpu;
    pWpu = &m_pPhy->mWpu;

    //! wpu
    pWpu->setanalog_mode_analog_ch_mode( chCnt );

    pWpu->setanalog_mode_analog_ch_en( showBm );

    //! persist time
    applyPersistTime();

    //! clear persist
    if ( pWpu->getpersist_mode() != 0 )
    { pWpu->setwpu_clear( 3 ); }

    //! fit screen
    pWpu->fitScreen();

    //! interleave
    wpuInterleave( saBm );

    //! view length
    pWpu->setMainView( 0, trace_width );
    pWpu->setZoomView( 0, trace_width );

    //! -- noise
    //! average , close noise
    if ( m_pViewSession->m_acquire.getAcquireMode() == Acquire_Average
         || m_pViewSession->m_trigger.mSweep == Trigger_Sweep_Single
         || m_pViewSession->m_horizontal.getViewMode() == Horizontal_Zoom )
    {
        pWpu->setNoise( 0 );
    }
    else
    {
        pWpu->setNoise( 2 );
    }

    //! line type
    if ( m_pViewSession->m_display.getLineType() == Wfm_Dot )
    {
        if ( m_pViewSession->m_acquire.m_eTimeMode == Acquire_XY )
        {}
        else
        {
            pWpu->setNoise( 0 );
        }
    }
}

void CDsoRecEngine::ccuApply( quint32 saBm )
{
    int chCnt = toChCnt( saBm );

    //! num div
    int plotNumDiv = 1;
    if ( m_pPhy->mWpu.getzoom_en_en() )
    { plotNumDiv = 2; }
    else
    { plotNumDiv = 1; }

    quint32 maxComp;
    maxComp = 1;

    //! main
    if ( m_pPhy->mWpu.getscale_times_scale_times_en() )
    { maxComp = m_pPhy->mWpu.getscale_times_scale_times_m(); }

    //! zoom
    if ( m_pPhy->mWpu.getzoom_en_en()
         && m_pPhy->mWpu.getzoom_scale_times_zoom_scale_times_en() )
    {
        maxComp = qMax( maxComp, m_pPhy->mWpu.getzoom_scale_times_zoom_scale_times_m() );
    }

    //! plot num div
    quint32 plotNum;
    Q_ASSERT( plotNumDiv > 0 );
    Q_ASSERT( chCnt > 0 );
    Q_ASSERT( maxComp > 0 );
    plotNum = batch_max_plot_num / plotNumDiv / chCnt / maxComp;

    //! 当绘制根数小于8时，则配置为1。
    if ( plotNum < 8 )
    { plotNum = 1; }
    m_pPhy->mCcu.setFRAME_PLOT_NUM( plotNum );

    //! interleave
    m_pPhy->mScuGp.setMODE_10G_chn_10G( saBm );
    m_pPhy->mCcu.setMODE_interleave_20G( chCnt );
}
