#include "cdsorecengine.h"
#include "../../phy/zynqfcu/iphyfcu_addr.h"

DsoErr CDsoRecEngine::rollApply()
{
    quint32 searchEn = mRecSession.m_search.getEnable();
    m_pPhy->mCcu.setMODE_search_en_zoom( get_bit(searchEn,0) );
    m_pPhy->mCcu.setMODE_search_en_ch( get_bit(  searchEn,1) );
    m_pPhy->mCcu.setMODE_search_en_la( get_bit(  searchEn,2) );

    if ( mRecSession.m_acquire.m_eTimeMode == Acquire_ROLL )
    {
        LOG_DBG()<<"hori mode is roll";
        m_pPhy->mCcu.setMODE_mode_roll_en( 1 );
        m_pPhy->mCcu.setMODE_mode_scan_en( 0 );
        m_pPhy->mWpu.settime_base_mode( 2 );

        //! 滚动模式下搜索常开
        m_pPhy->mCcu.setMODE_search_en_zoom( 0 );
        m_pPhy->mCcu.setMODE_search_en_ch( 1 );
        if(m_pViewSession->mLa.getShowOnOff())
        {
            m_pPhy->mCcu.setMODE_search_en_la( 1 );
        }
        else
        {
            m_pPhy->mCcu.setMODE_search_en_la( 0 );
        }
    }
    else if( mRecSession.m_acquire.m_eTimeMode == Acquire_SCAN)
             //&& mRecSession.m_trigger.getSweep() != Trigger_Sweep_Single)
    {

#if 0 /*! [dba: 2018-04-22] bug: 2981， mask下退出慢扫描放在服务中处理*/
        if( m_pViewSession->m_mask.mMaskEn )
        {
            LOG_DBG()<<"hori mode is scan";
            m_pPhy->mCcu.setMODE_mode_roll_en( 0 );
            m_pPhy->mCcu.setMODE_mode_scan_en( 0 );
            m_pPhy->mWpu.settime_base_mode( 0 );

        }
        else
        {
            LOG_DBG()<<"hori mode is scan";
            m_pPhy->mCcu.setMODE_mode_roll_en( 0 );
            m_pPhy->mCcu.setMODE_mode_scan_en( 1 );
            m_pPhy->mWpu.settime_base_mode( 1 );

        }
#else
            LOG_DBG()<<"hori mode is scan";
            m_pPhy->mCcu.setMODE_mode_roll_en( 0 );
            m_pPhy->mCcu.setMODE_mode_scan_en( 1 );
            m_pPhy->mWpu.settime_base_mode( 1 );
#endif

    }
    else
    {
        m_pPhy->mCcu.setMODE_mode_roll_en( 0 );
        m_pPhy->mCcu.setMODE_mode_scan_en( 0 );
        if( mRecSession.m_acquire.m_eTimeMode == Acquire_XY)
        {
            m_pPhy->mWpu.settime_base_mode( 4 );
        }
        else
        {
            m_pPhy->mWpu.settime_base_mode( 0 );
        }
    }

    if(dsoEngine::mViewSession.m_acquire.getTimemode() == Acquire_ROLL)
    {
        m_pPhy->mScuGp.setPrePostSa( 10e6 , 0);
        m_pPhy->mSpuGp.setWAVE_LEN_MAIN( 10e3 );

        //! 滚动模式下存储深度与屏幕上显示不同，为了微调时基下的回放
        m_pPhy->mSpuGp.setRecordLen( mRecSession.m_acquire.mMainCHView.mMLen );

        CFlowView* pView = &mRecSession.m_acquire.mMainCHView;
        int tx_len_1000 = pView->mTransferLen;
        m_pPhy->mWpu.setscale_times_scale_times_en( true );
        m_pPhy->mWpu.setscale_times_scale_times_m( tx_len_1000/1000 );
        m_pPhy->mWpu.setscale_times_scale_times_n( 1 );
        m_pPhy->mWpu.setscale_length( tx_len_1000/1000 );
    }

    if( mRecSession.m_acquire.m_eTimeMode == Acquire_ROLL)
    {
        //! 根据通道模式设置trace数据的末地址
        quint32 chCnt = toChCnt( m_pViewSession->getSaCHBm() );
        m_pPhy->mZynqFcuWr.outroll_ana_trace_waddr_end(
                    roll_trace_ana_begin + chCnt * roll_trace_ana_slice * 2 - 1);
        m_pPhy->mZynqFcuWr.outroll_la_trace_waddr_end(
                    roll_trace_la_begin + 16 * roll_trace_la_slice * 2 - 1 );

        LOG_DBG()<<"recegine chcnt"<<chCnt;
        //! 设置解交织读地址的起始地址和拐点地址
        m_pPhy->mZynqFcuWr.outdeinterweave_chx_raddr_base( roll_trace_ana_begin );
        m_pPhy->mZynqFcuWr.outdeinterweave_lax_raddr_base( roll_trace_la_begin );

        m_pPhy->mZynqFcuWr.outdeinterweave_chx_raddr_end(
                    roll_trace_ana_begin + chCnt * roll_trace_ana_slice * 2 - 1);
        m_pPhy->mZynqFcuWr.outdeinterweave_lax_raddr_end(
                    roll_trace_la_begin + 16 * roll_trace_la_slice * 2 - 1 );

        //! 复位滚动下的trace
        m_pPhy->mZynqFcuWr.outroll_trace_waddr_rst( 1 );
        m_pPhy->mZynqFcuWr.outroll_trace_waddr_rst( 0 );

        //! 复位滚动下的serach
        m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(1);
        m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(0);
        m_pPhy->mSearch.outsearch_result_send_rst(1);
        m_pPhy->mSearch.outsearch_result_send_rst(0);
    }
    else
    {
        m_pPhy->mZynqFcuWr.outdeinterweave_chx_raddr_end( 0xffffffff );
        m_pPhy->mZynqFcuWr.outdeinterweave_lax_raddr_end( 0xffffffff );
    }

    if( mRecSession.m_acquire.m_eTimeMode == Acquire_ROLL)
    {
        m_pPhy->mCcu.setPLOT_DELAY(time_ms(15) / TD_TICK);
    }
    else
    {
        m_pPhy->mCcu.setPLOT_DELAY(time_ms(40) / TD_TICK);
    }
    return ERR_NONE;
}
