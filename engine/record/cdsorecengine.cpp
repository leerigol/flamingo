#include "cdsorecengine.h"

CDsoRecEngine::CDsoRecEngine()
{
    mName="CDsoRecEngine";
}

/*!
 * \brief CDsoRecEngine::exec
 * \param msg
 * \param para
 * \param outPtr
 * \param proc
 * \return
 * \todo if the msg is not in map, let the base process it
 */
DsoErr CDsoRecEngine::exec( const struEngineMsgMap *pMap,
                            const struEngineMsgEntry *pEntry,
                            EngineMsg msg,
                            EnginePara& para,
                            void *outPtr,
                            ServiceId sId  )
{
    DsoErr err;

    //! in record map
    if ( pMap == getMsgMap() )
    {
        //! write
        if ( wr(msg) == Engine_Write )
        {
            m_ConfigId.addSysConfigId();

            //remember the last status for auto stop. by hxh
            m_pAutoStopEngine->m_nLastStatus = this->getControlStatus();

            //! to play mode
            err = toEngine( m_pAutoStopEngine );

            if(err == ERR_NONE)
            {
                //! record config
                err = dsoEngine::exec( pMap, pEntry, msg, para, outPtr, sId );
            }

            do
            {
                //! play?
                if ( pEntry->pAttr!= NULL )
                {
                    if ( !pEntry->pAttr->bReplay )
                    {
                        toEngine( m_pRecEngine );
                        break;
                    }
                }

                //! self now
                if ( engine::m_pActEngine == m_pRecEngine )
                { break; }

                //! play proc
                engine::m_pActEngine->doDispatch( msg,
                                                  para,
                                                  outPtr );
            }while( 0 );
        }
        //! read
        else
        {
            //! do msg only
            err = dsoEngine::exec( pMap, pEntry, msg, para, outPtr, sId );
        }
    }
    else
    {
        //! do the base msg
        err = dsoEngine::exec( pMap, pEntry, msg, para, outPtr, sId );
    }

    //! post set
    postExec( msg );

    //! phy config
    Q_ASSERT( NULL != m_pPhy );
    m_pPhy->flushWCache();
    return err;
}

DsoErr CDsoRecEngine::onExit()
{
    //! force stop
    LOG_FPGA_STATUS()<<"before rec exit"<<getDebugTime();
    m_bTraceRequest = false;
    m_pPhy->mCcu.setCcuRun( false );
    m_pPhy->mWpu.waitWpuStop();
    m_ConfigId.changeSysConfigId_stop();
    m_pPhy->mScuGp.outCONFIG_ID(m_ConfigId.getConfigId(),-1);

    //! wave validation
    m_pPhy->mScuGp[0]->inWAVE_INFO();

    m_pHistSession->m_bPlayValid = getPlayValid();

    if(m_pPhy->mScuGp[0]->getWAVE_INFO_wave_play_vld())
    {
        CFlowView* pView = &mRecSession.m_acquire.mMainCHView;
        m_pPhy->mWpu.inroll_lenth();
        int chSaLength = m_pPhy->mWpu.getroll_lenth() * pView->mCompL1 * pView->mCompL2 / pView->mIntx;
        int laSaLength = m_pPhy->mWpu.getroll_lenth() * pView->mCompL1 * pView->mCompL2 / pView->mIntx;
        mRecSession.m_acquire.setSaLength( m_pPhy->mScuGp[0]->getWAVE_INFO_wave_play_vld(),
                                           chSaLength,laSaLength);

        Q_ASSERT( NULL != m_pHistSession );
        *dsoEngine::m_pHistSession = mRecSession;

        //! set fine trig pos
        m_pHistSession->m_acquire.setFinePos( m_pPhy->mSpuGp.getFINE_TRIG_POSITION() );

        m_pPhy->mWpu.inroll_lenth();
        if( m_pHistSession->m_acquire.m_eTimeMode == Acquire_ROLL )
        {
            dsoFract recordSum = 0;
            LOG_FPGA_STATUS()<<"mid rec exit"<<m_pPhy->mWpu.getroll_lenth_roll_full();
            if(m_pPhy->mWpu.getroll_lenth_roll_full() == 1)
            {
                recordSum = mRecSession.m_acquire.mMainCHView.mMLen;
            }
            else
            {
                qlonglong wpuCol = m_pPhy->mWpu.getroll_lenth_roll_lenth_cnt();
                recordSum = wpuCol * mRecSession.m_acquire.mMainCHView.mMLen / 1000;
            }
            m_pHistSession->m_acquire.mCHFlow.mRollSaSum = recordSum;
            m_pHistSession->m_acquire.mCHFlow.setT0( - mRecSession.m_horizontal.getMainView().getScale() * 10 );
            m_pHistSession->m_acquire.mCHFlow.setSaInfo( mRecSession.m_horizontal.getMainView().getScale(),
                                                         mRecSession.m_horizontal.getMainView().getScale() * 5); ;
            m_pHistSession->m_acquire.mCHFlow.setLength(mRecSession.m_acquire.mMainCHView.mMLen);

            if( m_pHistSession->m_acquire.mCHFlow.mRollSaSum == 0 )
            {
                m_pHistSession->m_bPlayValid = false;
            }
        }

        if( m_pHistSession->m_acquire.m_eTimeMode == Acquire_SCAN )
        {
            //! 慢扫描第一次从运行状态切到回放状态需要清屏
            m_pHistSession->m_bNeedPlayClear = true;

            dsoFract recordSum = 0;
            m_pPhy->mWpu.inroll_lenth();
            LOG_DBG()<<"wpu full"<<m_pPhy->mWpu.getroll_lenth_roll_full()
                    <<"wpu count"<<m_pPhy->mWpu.getroll_lenth_roll_lenth_cnt();
            if( m_pPhy->mWpu.getroll_lenth_roll_full() )
            {
                m_pPhy->mSpuGp.inRECORD_SUM();
                LOG_DBG()<<"spu full"<<m_pPhy->mSpuGp.getRECORD_SUM_wav_view_full()
                        <<"spu count"<<m_pPhy->mSpuGp.getRECORD_SUM_wav_view_len();
                if( m_pPhy->mSpuGp.getRECORD_SUM_wav_view_full() )
                {
                    recordSum = mRecSession.m_acquire.mCHFlow.getDepth();
                }
                else
                {
                    recordSum = m_pPhy->mSpuGp.getRECORD_SUM_wav_view_len() /
                            m_pHistSession->m_acquire.m_chCnt;
                }
            }
            else
            {
                int wpuCol = m_pPhy->mWpu.getroll_lenth_roll_lenth_cnt();
                if(wpuCol < ( mRecSession.m_horizontal.getMainView().getOffset() * -100 /
                             mRecSession.m_horizontal.getMainView().getScale() + 500) || wpuCol == 0)
                {
                    LOG_DBG()<<"wpuCpl not trig"<<wpuCol;
                    //! wpu没到触发列的情况下，回放上一次的内存波形
                    recordSum = mRecSession.m_acquire.mCHFlow.getDepth();
                }
                else
                {
                    //! wpu没满屏的情况下，将屏幕上对应的点和屏幕左端外面的点要进行相加
                    //! 列数--->时间 --> 采样长度
                    qlonglong scale = m_pHistSession->m_horizontal.getMainView().getScale();
                    qlonglong offset = m_pHistSession->m_horizontal.getMainView().getOffset();
                    qlonglong currentT = (wpuCol - wave_width/2) * scale/adc_hdiv_dots + offset;
                    recordSum = (currentT - m_pHistSession->m_acquire.mCHFlow.getT0())/
                            m_pHistSession->m_acquire.mCHFlow.mDotTime;
                }
            }

            m_pHistSession->m_acquire.setChLength(recordSum);
            LOG_DBG()<<"scan record sa sum"<<recordSum.toLonglong();
            m_pHistSession->m_acquire.mCHFlow.setLength(recordSum);
            m_pHistSession->m_acquire.mLaFlow.setLength(recordSum);
        }
    }
    else
    {
        //! 即使当前运行次没有触发住，拧动设置，应该回放更前一次的波形
        // mRecSession.m_acquire.setSaValid( m_pPhy->mScuGp[0]->getWAVE_INFO_wave_play_vld());
    }

    if(mRecSession.m_mask.mMaskEn)
    {
        //! 退出run(回放时)先关闭mask使能，解决卡顿问题
        m_pPhy->mCcu.setMODE_mask_pf_en( false );
    }

    if(mRecSession.m_mask.mZoneEn)
    {
        m_pPhy->mCcu.setMODE_zone_trig_en(false);
    }

    m_pPhy->mCcu.flushWCache();
    m_pPhy->mCcu.setFRAME_PLOT_NUM( 1 );

    //! hori req
    EngineHoriReq hReq;
    quint32 bm = mRecSession.getSaCHBm();
    hReq.mChCnt = toChCnt( bm );
    emit sigHoriChanged( hReq );

    LOG_FPGA_STATUS()<<"after rec exit"<<getDebugTime();
    return ERR_NONE;
}

DsoErr CDsoRecEngine::onEnter()
{
    LOG_FPGA_STATUS()<<"before rec enter"<<getDebugTime();
    bool bAdcModeChange = false;
    static bool firstRun = true; //added by hxh

    /*! [dba: 2018-04-21] run:之前退出导入回放模式.*/
    m_pPhy->mCcu.inMODE();
    m_pPhy->mScuGp.inREC_PLAY();

    if( 1 == m_pPhy->mCcu.getMODE_mode_import_play())
    {
        m_pPhy->mCcu.outMODE_mode_import_play(0);
    }

    if( 1 == m_pPhy->mScuGp.getREC_PLAY_mode_play())
    {
         m_pPhy->mScuGp.outREC_PLAY_mode_play(0);
    }

    /*! [dba: 2018-04-19] run:之前退出录制回放模式 */
    m_pHistSession->m_RecDone = false;

    //  added by hxh for 1566
    if( m_pViewSession->m_acquire.m_eTimeMode == Acquire_ROLL ||
         m_pViewSession->m_acquire.m_eTimeMode == Acquire_XY )
    {
        if( !firstRun )
        {
            m_pPhy->mWpu.syncClear();
        }
    }

    if( m_pHistSession->m_acquire.m_eTimeMode != Acquire_SCAN &&
         m_pViewSession->m_acquire.m_eTimeMode == Acquire_SCAN )
    {
        //! 慢扫描模式的清屏控制：慢扫描回放的第一次清屏，回放后运行前不清
        //! 从非慢扫描模式到慢扫描模式运行前需要清屏
        if( !firstRun )
        {
            m_pPhy->mWpu.clearMemory();
        }
    }

    //! open write channel
    m_pPhy->mWpu.outdisplay_en( 2 );

    if( m_pViewSession->m_acquire.m_eTimeMode == Acquire_ROLL )
    {
        CHorizontalView* pHView = &m_pViewSession->m_horizontal.getMainView();
        pHView->setRollOffset( pHView->getScale() * - 5);
        pHView->setOffset( pHView->getScale() * - 5 );
        emit sigHoriOffsetRoll( pHView->getScale() * - 5 );
    }

    //! init recsession
    mRecSession = dsoEngine::mViewSession;

    //! apply
    chsApply();

    laApply();

    //! adc config
    quint32 chBm = m_pViewSession->getSaCHBm();
    bAdcModeChange = isAdcModeChange( toAdcMod( chBm ) );

    //! vert config
    if ( bAdcModeChange )
    {
        //! 切换ADC模式前将延迟配为0进行校准
        m_pPhy->mSpuGp.setCHN_DELAY_AB_A( 0 );
        m_pPhy->mSpuGp.setCHN_DELAY_AB_B( 0 );
        m_pPhy->mSpuGp.setCHN_DELAY_CD_C( 0 );
        m_pPhy->mSpuGp.setCHN_DELAY_CD_D( 0 );
        m_pPhy->flushWCache();
        QThread::msleep(1);
        //! k7 clear
        m_pPhy->mSpuGp.pulseCTRL_spu_clr( 1, 0, 1 );
        calAdcGroup( toAdcMod( chBm ) );
        m_pPhy->flushWCache( );
        QThread::msleep( 1 );
    }

    //! hori config
    horiApply();

    //! k7 clear
    m_pPhy->mSpuGp.pulseCTRL_spu_clr( 1, 0, 1 );

    //! fcu
    fcuApply();

    //! acq
    acqApply();

    //! meas
    measApply(&mRecSession, &mRecSession);

    //! search
    dsoEngine::searchApply(&mRecSession);

    //! zone
    if(mRecSession.m_mask.mZoneEn)
    {
        applyTrigZone(&mRecSession);
    }

    //! con
    conApply();

    //! roll
    rollApply();

    //! fast
    fastApply();

    //! mask
    maskApply();

    if ( m_pPhy->mWpu.getpersist_mode() != 0 )
    {
        m_pPhy->mWpu.outwpu_clear( 3 );
        m_pPhy->mWpu.waitWpuStop();
    }
    //! wav invalid
    m_pPhy->mCcu.setCTRL_mode_play_last( 0 );

    //! config previous
    m_pPhy->flushWCache();

    //! k7 clear
    m_pPhy->mSpuGp.pulseCTRL_spu_clr( 1, 0, 1 );

    //! gtu clear
    m_pPhy->mGtu.pulsegt_buf_clr_pdu_rx_buf_clr( 1, 0, 1 );

    //! ---- run atlast
    Wpu_reg enBm = m_pPhy->mWpu.getanalog_mode_analog_ch_en();
    if ( bAdcModeChange )
    {
        m_pPhy->mWpu.outanalog_mode_analog_ch_en( 0 );
    }

    //! adc mode change
    if ( bAdcModeChange )
    {
        QThread::msleep( 10 );
        m_pPhy->mWpu.outanalog_mode_analog_ch_en( enBm );
    }

    m_pPhy->flushWCache();

    //! close wpu channel
    m_pPhy->mWpu.outdisplay_en( 3 );
    //! false->true
    m_ConfigId.changeSysConfigId_run();
    m_pPhy->mScuGp.outCONFIG_ID(m_ConfigId.getConfigId(),-1);
    m_pPhy->mCcu.setCcuRun( true );

    //! save last/
    mLastSession = mRecSession;

    //! valid
    dsoEngine::mViewSession = mRecSession;
    dsoEngine::m_bTraceRequest = true;

    if( !firstRun )
    {
        //added by hxh
        emit sigConfigFinished();
    }
    else
    {
        firstRun = false;
    }
    LOG_FPGA_STATUS()<<"after rec enter"<<getDebugTime();
    return ERR_NONE;
}

DsoErr CDsoRecEngine::onTimeout( int /*id*/ )
{
    return ERR_NONE;
}

void CDsoRecEngine::postExec( EngineMsg /*packedMsg*/ )
{
//    if ( wr(packedMsg) == Engine_Write )
//    {
//        int rawMsg = unpackSMsg( packedMsg );

//        if ( rawMsg != ENGINE_REQUEST_TRACE )
//        {
//            if ( m_pPhy->mWpu.getpersist_mode() != 0 )
//            { m_pPhy->mWpu.setwpu_clear( 3 ); }
//        }
//    }
}

