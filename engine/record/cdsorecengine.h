#ifndef CDSORECENGINE_H
#define CDSORECENGINE_H

#include "../../baseclass/dsowfm.h"

#include "../base/dsoengine.h"

/*!
 * \brief The CDsoRecEngine class
 * 录制引擎
 */
class CDsoRecEngine : public dsoEngine
{
    DECLARE_MSG_MAP()

public:
    CDsoRecEngine();

public:
    virtual DsoErr exec( const struEngineMsgMap *pMap,
                         const struEngineMsgEntry *pEntry,
                         EngineMsg msg,
                         EnginePara& para,
                         void *outPtr,
                         ServiceId sId = E_SERVICE_ID_NONE );
    virtual DsoErr onExit();
    virtual DsoErr onEnter();

protected:
    CDsoSession mLastSession;
    CDsoSession mRecSession;        //! recording session

public:
    DsoErr setPlayEngine( dsoEngine *pEngine );
    dsoEngine *getPlayEngine();

public:
    DsoErr onTimeout( int id );
    void postExec( EngineMsg msg );

public:
    //! ch operation
    DsoErr chOnOff(ServiceId id, bool b);

    void chInvertApply( ServiceId id );

    DsoErr chInterleave();
    void chInterleave_Invert( quint32 chBm );
    void chInterleave_Filter( quint32 chBm );

    void chFilterCfg( int chId,
                      EngineFilter &flt,
                      bool bOnOff );

    //! chs apply
    DsoErr chsApply();

    //! .probe
    DsoErr chProbeImpedance( ServiceId id, Impedance imp );
    DsoErr chProbeDelay( ServiceId id, int dly );

    Impedance chProbeGetImpedance( ServiceId id );
    int chProbeGetDelay( ServiceId id );
    int chProbeGetRatio( ServiceId id );

    //! la
    void laApply();

    //! hori
    DsoErr setViewMode( HorizontalViewmode view );

    DsoErr setMainCfg( EngineHoriReq *pCfg );
    DsoErr setZoomCfg( EngineHoriReq *pCfg );

    DsoErr cfgMainOffset(qlonglong offset, int posSaSuffix);
    DsoErr cfgZoomOffset( qlonglong offset, quint32 intxp );

    //! 真实的通道延迟
    DsoErr cfgAdcChDelay();
    //! ch delay -> pre sa
    DsoErr cfgMainChDelay(quint32 intxp );
    DsoErr cfgZoomChDelay(quint32, quint32 intxp);

    void cfgSegmentSize(int addSuffix);

    void cfgScanPos();

    bool finePosistion(qlonglong,
                  qlonglong saOffset,
                  quint32 intxp,
                  dsoFract dotTime,
                  quint32 &finePos);

    DsoErr setAutoTrigTimeout( EngineHoriReq *pCfg );

    DsoErr setZoomScale( qlonglong scale );

    DsoErr queryHoriInfo( EngineHoriInfo *pInfo );

    //! main zoom
    DsoErr queryHoriInfoMain( EngineHoriInfo *pInfo );
    DsoErr queryHoriInfoZoom( EngineHoriInfo *pInfo );

    DsoErr horiApply();
    void wpuApply( quint32 saBm, quint32 showBm );
    void ccuApply( quint32 saBm );

    //! acq
    DsoErr acqApply();

    //! con
    DsoErr conApply();

    //! fcu
    DsoErr fcuApply();

//    //! meas
//    DsoErr measApply();

    //! roll
    DsoErr rollApply();

    //! fast
    DsoErr fastApply();

    DsoErr maskApply();

    //! group cal
    bool isAdcModeChange( dso_phy::IPhyADC::eAdcInterleaveMode eIntMode );
    DsoErr calAdcGroup( dso_phy::IPhyADC::eAdcInterleaveMode eIntMode,
                        DsoWorkAim aim = aim_normal );

    DsoErr snapAdcGroup(dso_phy::IPhyADC::eAdcInterleaveMode eIntMode,
                         Spu_reg *pABCD,
                         DsoWorkAim aim,
                         int chCnt);

    DsoErr alignAdcGroup(
                        int chCnt,
                        Spu_reg abCD,
                        int norms[4] );
    DsoErr configAdcGroup(
                           int chCnt,
                           int norms[4] );

    //! trigger
    DsoErr setTrigSweep( TriggerSweep sweep );

    //! trace
    virtual DsoErr requestTrace( traceRequest &req,
                                 int tmo=2000 );
protected:

    bool getPlayValid();
//    //! 0 -- no error
//    int timeRangeMap( CSaFlow *pSaFlow,
//                      CSaFlow *pRefFlow,
//                      qlonglong t0,
//                      qlonglong tSpan,
//                      qint32 *pOffset,
//                      qint32 *pLength );

//    int timeRangeCHMapOnView(
//                      HorizontalView view,
//                      qlonglong t0,
//                      qlonglong tSpan,
//                      qint32 *pOffset,
//                      qint32 *pLength );
//    int timeRangeLAMapOnView(
//                      HorizontalView view,
//                      qlonglong t0,
//                      qlonglong tSpan,
//                      qint32 *pOffset,
//                      qint32 *pLength );
};

#endif // CDSORECENGINE_H
