#include "cdsorecengine.h"

DsoErr CDsoRecEngine::requestTrace( traceRequest &req,
                                    int tmo )
{
    return requestData( req, tmo, &mRecSession );
}
