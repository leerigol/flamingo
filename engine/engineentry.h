#ifndef ENGINEENTRY
#define ENGINEENTRY

//! common
#define on_range_ch( msg, para, proc, pAttr )  on_range_id( msg, \
                                            E_SERVICE_ID_CH1, \
                                            E_SERVICE_ID_CH4, \
                                            para,\
                                            proc,\
                                            pAttr\
                                            )

#define on_noid_cmd_bool( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_BOOL, proc, pAttr )
#define on_noid_cmd_s32( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_S32, proc, pAttr )
#define on_noid_cmd_s64( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_S64, proc, pAttr )

#define on_noid_cmd_u8( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_U8, proc, pAttr )
#define on_noid_cmd_u16( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_U16, proc, pAttr )
#define on_noid_cmd_u32( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_U32, proc, pAttr )
#define on_noid_cmd_u64( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_U64, proc, pAttr )
#define on_noid_cmd_real( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_REAL, proc, pAttr )

#define on_noid_cmd_ptr( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_PTR, proc, pAttr )

#define on_noid_cmd_s32_context( msg, proc, pAttr, context ) \
                                    on_noid_cmd_context( msg, E_PARA_S32, proc, pAttr, context )
#define on_noid_cmd_s64_context( msg, proc, pAttr, context ) \
                                    on_noid_cmd_context( msg, E_PARA_S64, proc, pAttr, context )
#define on_noid_cmd_ptr_context( msg, proc, pAttr, context ) \
                                    on_noid_cmd_context( msg, E_PARA_PTR, proc, pAttr, context )

#define on_noid_cmd_void( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_NONE, proc, pAttr )

#define on_noid_cmd_s32bool( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_INT_BOOL, proc, pAttr )
#define on_noid_cmd_s32s32( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_INT_INT, proc, pAttr )
#define on_noid_cmd_s32s64( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_INT_S64, proc, pAttr )
#define on_noid_cmd_s32ptr( msg, proc, pAttr ) on_noid_cmd( msg, E_PARA_INT_PTR, proc, pAttr )

//////////////////////////////////////////////////////////////////////////////////////////////
//
#define on_noid_cfg_void( msg, proc, pAttr ) on_noid_cmd( msg, E_CFG_PARA_NONE, proc, pAttr )
#define on_noid_cfg_bool( msg, proc, pAttr ) on_noid_cmd( msg, E_CFG_PARA_BOOL, proc, pAttr )
#define on_noid_cfg_s16( msg, proc, pAttr )  on_noid_cmd( msg, E_CFG_PARA_S16, proc, pAttr )
#define on_noid_cfg_s32( msg, proc, pAttr )  on_noid_cmd( msg, E_CFG_PARA_S32, proc, pAttr )
#define on_noid_cfg_s64( msg, proc, pAttr )  on_noid_cmd( msg, E_CFG_PARA_S64, proc, pAttr )

#define on_noid_cfg_u16( msg, proc, pAttr )  on_noid_cmd( msg, E_CFG_PARA_U16, proc, pAttr )
#define on_noid_cfg_u32( msg, proc, pAttr )  on_noid_cmd( msg, E_CFG_PARA_U32, proc, pAttr )
#define on_noid_cfg_u64( msg, proc, pAttr )  on_noid_cmd( msg, E_CFG_PARA_U64, proc, pAttr )

#define on_noid_cfg_s32bool( msg, proc, pAttr) on_noid_cmd( msg, E_CFG_PARA_INT_BOOL,proc, pAttr )
#define on_noid_cfg_s32s32( msg, proc, pAttr ) on_noid_cmd( msg, E_CFG_PARA_INT_INT, proc, pAttr )
#define on_noid_cfg_s32s64( msg, proc, pAttr ) on_noid_cmd( msg, E_CFG_PARA_INT_S64, proc, pAttr )
//////////////////////////////////////////////////////////////////////////////////////////////

//! stack para
#define on_noid_cmd_stack( msg, proc, pAttr, stackSize )    on_noid_stack(msg,proc, pAttr, stackSize, E_PARA_STACK)

//! get
#define on_noid_get_s32void( msg, proc ) on_noid_cmd( msg, E_PARA_qS32, proc, NULL )
#define on_noid_get_s64void( msg, proc ) on_noid_cmd( msg, E_PARA_qS64, proc, NULL )

#define on_noid_get_boolvoid( msg, proc ) on_noid_cmd( msg, E_PARA_qBOOL, proc, NULL )
#define on_noid_get_u8void( msg, proc ) on_noid_cmd( msg, E_PARA_qU8, proc, NULL )
#define on_noid_get_u16void( msg, proc ) on_noid_cmd( msg, E_PARA_qU16, proc, NULL )
#define on_noid_get_u32void( msg, proc ) on_noid_cmd( msg, E_PARA_qU32, proc, NULL )
#define on_noid_get_u64void( msg, proc ) on_noid_cmd( msg, E_PARA_qU64, proc, NULL )
#define on_noid_get_realvoid( msg, proc ) on_noid_cmd( msg, E_PARA_qREAL, proc, NULL )

#define on_noid_get_ptr( msg, proc ) on_noid_cmd( msg, E_PARA_qPTR, proc, NULL )

#define on_noid_get_boolint( msg, proc ) on_noid_cmd( msg, E_PARA_qBOOL_INT, proc, NULL )
#define on_noid_get_s32int( msg, proc ) on_noid_cmd( msg, E_PARA_qS32_INT, proc, NULL )
#define on_noid_get_s64int( msg, proc ) on_noid_cmd( msg, E_PARA_qS64_INT, proc, NULL )

#define on_noid_get_u8int( msg, proc ) on_noid_cmd( msg, E_PARA_qU8_INT, proc, NULL )
#define on_noid_get_u16int( msg, proc ) on_noid_cmd( msg, E_PARA_qU16_INT, proc, NULL )
#define on_noid_get_u32int( msg, proc ) on_noid_cmd( msg, E_PARA_qU32_INT, proc, NULL )
#define on_noid_get_u64int( msg, proc ) on_noid_cmd( msg, E_PARA_qU64_INT, proc, NULL )
#define on_noid_get_ptrint( msg, proc ) on_noid_cmd( msg, E_PARA_qINT_PTR, proc, NULL )

#define on_noid_get_u32stack( msg, proc, pAttr, stackSize )    on_noid_stack(msg,proc, pAttr, stackSize, E_PARA_qU32_STACK)

#endif // ENGINEENTRY

