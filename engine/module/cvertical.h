#ifndef CVERTICAL_H
#define CVERTICAL_H

#include "../base/cengineobj.h"

/*!
 * \brief The CVertical class
 * base vertical
 */
class CVertical : public CEngineObj
{
private:
    static bool _bInterleaveEn;
public:
    static void setInterleaveEn( bool en );
public:
    CVertical();
protected:
    bool m_bShowOnOff;
    bool m_bTrigOnOff;
    bool m_bDvmCountOnOff;
public:
    bool getOnOff();

    DsoErr setShowOnOff( bool b );
    bool getShowOnOff();

    DsoErr setTrigOnOff( bool b );
    bool getTrigOnOff();

    DsoErr setDvmCountOnOff( bool b);
    bool getDvmCountOnOff();
};

#endif // CVERTICAL_H
