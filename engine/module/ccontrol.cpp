#include "ccontrol.h"

CControl::CControl()
{
    m_eAction = Control_Run;
    m_eStatus = Control_Runing;
}

/*!
 * \brief CControl::setAction
 * \param eAction
 * \return
 * run/stop/single action
 * \todo the action not equal status
 */
DsoErr CControl::setAction( ControlAction eAction )
{
    m_eAction = eAction;

    if ( m_eAction == Control_Run || m_eAction == Control_Single )
    {
        m_eStatus = Control_Runing;
    }
    else
    {
        m_eStatus = Control_Stoped;
    }

    return ERR_NONE;
}

//ControlAction CControl::getAction() const
//{
//    return m_eAction;
//}

/*!
 * \brief CControl::getStatus
 * \return
 * \todo the status is filled by event from phy
 */
ControlStatus CControl::getStatus()
{
    return m_eStatus;
}
