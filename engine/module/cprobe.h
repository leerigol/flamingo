#ifndef CPROBE_H
#define CPROBE_H

#include <QString>
#include "../base/cengineobj.h"

/*!
 * \brief The CProbe class
 * base probe
 */
class CProbe : public CEngineObj
{
public:
    CProbe();

public:
    Impedance m_Imp;
    ProbeHead m_Head;
    ProbeType m_Type;
    ProbeModel m_Model;

    bool m_Pwr;     //! power or no power
    bool m_Online;

    int m_Ratio;    //! ratio index
    QString m_SN;

public:
    DsoErr setImpedance( Impedance imp );
    DsoErr setHead( ProbeHead head );
    DsoErr setType( ProbeType type );
    DsoErr setModel( ProbeModel model );
    DsoErr setPwr( bool pwr );
    DsoErr setOnline( bool line );

    DsoErr setRatio( int ratio );
    DsoErr setSN( QString sn );

    Impedance getImpedance() const;
    ProbeHead getHead() const;
    ProbeType getType() const;
    ProbeModel getModel();
    bool getPwr() const;
    bool getOnline() const;

    int getRatio() const;
    QString getSN() const;


};
/*!
 * \brief The CCHProbe class
 * ch probe
 */
class CCHProbe : public CProbe
{
public:
    CCHProbe();
    ~CCHProbe();

public:
    int m_Bias;     //! uv
    int m_Delay;    //! ps

public:
    DsoErr setBias( int bias );
    int getBias();

    DsoErr setDelay( int delay );
    int getDelay();
};

class CLaProbe : public CProbe
{
public:
    CLaProbe();

protected:
    int m_Threshold;    //! uv
    int m_Sens;         //! 1 = 0.1

public:
    DsoErr setThreshold( int thre );
    DsoErr setSens( int sens );

    int getThreshold();
    int getSens();
};


#endif // CPROBE_H
