#include "cprobe.h"

CProbe::CProbe()
{
    m_Imp = IMP_1M;
    m_Head = Probe_Single;
    m_Type = Probe_V;
    m_Model = PROBE_BNC;

    m_Pwr = false;
    m_Online = false;

    m_Ratio = 0;
}

DsoErr CProbe::setImpedance( Impedance imp )
{
    m_Imp = imp;
    return ERR_NONE;
}
DsoErr CProbe::setHead( ProbeHead head )
{
    m_Head = head;
    return ERR_NONE;
}
DsoErr CProbe::setType( ProbeType type )
{
    m_Type = type;
    return ERR_NONE;
}
DsoErr CProbe::setModel( ProbeModel model )
{
    m_Model = model;
    return ERR_NONE;
}
DsoErr CProbe::setPwr( bool pwr )
{
    m_Pwr = pwr;
    return ERR_NONE;
}
DsoErr CProbe::setOnline( bool line )
{
    m_Online = line;
    return ERR_NONE;
}

DsoErr CProbe::setRatio( int ratio )
{
    m_Ratio = ratio;
    return ERR_NONE;
}
DsoErr CProbe::setSN( QString sn )
{
    m_SN = sn;
    return ERR_NONE;
}

Impedance CProbe::getImpedance() const
{
    return m_Imp;
}
ProbeHead CProbe::getHead() const
{
    return m_Head;
}
ProbeType CProbe::getType() const
{
    return m_Type;
}
ProbeModel CProbe::getModel()
{
    return m_Model;
}
bool CProbe::getPwr() const
{
    return m_Pwr;
}
bool CProbe::getOnline() const
{
    return m_Online;
}

int CProbe::getRatio() const
{
    return m_Ratio;
}
QString CProbe::getSN() const
{
    return m_SN;
}

CCHProbe::CCHProbe()
{
    //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__;
}

CCHProbe::~CCHProbe()
{
   //qDebug()<<__FILE__<<__LINE__<<__FUNCTION__;
}

DsoErr CCHProbe::setBias( int bias )
{
    m_Bias = bias;
    return ERR_NONE;
}
int CCHProbe::getBias()
{
    return m_Bias;
}

DsoErr CCHProbe::setDelay( int delay )
{
    m_Delay = delay;
    return ERR_NONE;
}
int CCHProbe::getDelay()
{
    return m_Delay;
}

CLaProbe::CLaProbe()
{
    m_Sens = 3;
}

DsoErr CLaProbe::setThreshold( int /*thre*/ )
{
    return ERR_NONE;
}
DsoErr CLaProbe::setSens( int /*sens*/ )
{
    return ERR_NONE;
}

int CLaProbe::getThreshold()
{
    return m_Threshold;
}
int CLaProbe::getSens()
{
    return m_Sens;
}
