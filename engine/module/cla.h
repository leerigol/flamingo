#ifndef CLA_H
#define CLA_H

#include "cvertical.h"

class CLa : public CVertical
{
public:
    CLa();

public:
    void setColor( int hel, quint32 color );
    quint32 getColor( int hel );

public:
    LaScale mSize;  //! 0,1,2
    int mEns;
    int mPoses[16];
    int mActives;

    int mGroupDx[4];

    quint32 mColors[3]; //! h:0, e:1, l:2
};

#endif // CLA_H
