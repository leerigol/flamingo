#include "chwproc.h"

CHwProc::CHwProc()
{
    mBmProcEn = 0;
    mT0 = 0;
    mTLength = 0;

    mView = horizontal_view_main;
}

void CHwProc::setEnable( quint32 bmEn )
{
    mBmProcEn = bmEn;
}
quint32 CHwProc::getEnable()
{ return mBmProcEn; }

void CHwProc::setRange( qlonglong t0, qlonglong tLength )
{
    mT0 = t0;
    mTLength = tLength;
}

void CHwProc::getRange( qlonglong &t0, qlonglong &tlength )
{
    t0 = mT0;
    tlength = mTLength;
}

void CHwProc::setView( HorizontalView view )
{ mView = view; }
HorizontalView CHwProc::getView()
{ return mView; }
