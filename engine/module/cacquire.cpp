#include "cacquire.h"
#include "../../include/dsodbg.h"
#include "../../include/dsocfg.h"

CAcquire::CAcquire()
{
    m_eAcquireMode =Acquire_Normal;
    m_eTimeMode = Acquire_YT;
    m_s32AverageCount = 2;
    m_eInterplate = Acquire_Sinc;

    m_bAntiAliasing = false;
    m_acqSaMode = Acquire_Real;

    m_chAcqDepIndex = Acquire_Depth_Auto;
    m_laAcqDepIndex = Acquire_Depth_Auto;

    m_chDepth = 0;
    m_laDepth = 0;

    m_chSaRate = 1;
    m_laSaRate = 1;

    m_chDotTime = 1;
    m_laDotTime = 1;

    m_chLength = 0;
    m_laLength = 0;

    m_chCnt = 4;
    m_laCnt = 2;

    m_bSaValid = false;
    m_chSaLength = 0;
    m_laSaLength = 0;

//    m_chPhySlice = 1024+64;
//    m_laPhySlice = 1024+64;
    m_chPhySlice = 4096;
    m_laPhySlice = 4096;

    m_nSysRawBand = BW_100M;
}

DsoErr CAcquire::setAcquireMode( AcquireMode mode )
{
    m_eAcquireMode = mode;
    return ERR_NONE;
}

DsoErr CAcquire::setTimeMode( AcquireTimemode mode )
{
    m_eTimeMode = mode;
    return ERR_NONE;
}

DsoErr CAcquire::setAverageCount( int count )
{
    m_s32AverageCount = count;
    return ERR_NONE;
}

DsoErr CAcquire::setInterplate( AcquireInterplate interplate )
{
    m_eInterplate = interplate;
    return ERR_NONE;
}

DsoErr CAcquire::setAntiAliasing( bool bAnti )
{
    m_bAntiAliasing = bAnti;
    return ERR_NONE;
}

DsoErr CAcquire::setSampleMode( AcquireSamplemode saMode )
{
    m_acqSaMode = saMode;
    return ERR_NONE;
}

DsoErr CAcquire::setChDepth( int depth )
{
    m_chDepth = depth;
    return ERR_NONE;
}

DsoErr CAcquire::setLaDepth( int depth )
{
    m_laDepth = depth;
    return ERR_NONE;
}

DsoErr CAcquire::setChAcqDepIndex( AcquireDepth ind )
{
    m_chAcqDepIndex = ind;
    return ERR_NONE;
}
DsoErr CAcquire::setLaAcqDepIndex( AcquireDepth ind )
{
    m_laAcqDepIndex = ind;
    return ERR_NONE;
}

DsoErr CAcquire::setChSaRate( qlonglong saRate,
                              dsoFract dotTime )
{
    m_chSaRate = saRate;
    m_chDotTime = dotTime;

    return ERR_NONE;
}
DsoErr CAcquire::setLaSaRate( qlonglong saRate,
                              dsoFract dotTime )
{
    m_laSaRate = saRate;
    m_laDotTime = dotTime;

    return ERR_NONE;
}

DsoErr CAcquire::setChSaRate( qlonglong saRate )
{
    m_chSaRate = saRate;
    m_chDotTime = time_s(1)*freq_Hz(1)/saRate;

    return ERR_NONE;
}
DsoErr CAcquire::setLaSaRate( qlonglong saRate )
{
    m_laSaRate = saRate;
    m_laDotTime = time_s(1)*freq_Hz(1)/saRate;
    return ERR_NONE;
}

DsoErr CAcquire::setChLength( dsoFract depth )
{
    m_chLength = depth;

    m_chPhySlice = alignPhySlice( depth.ceilLonglong() );

    //qDebug()<<__FUNCTION__<<__LINE__<<depth.ceilLonglong()<<m_chPhySlice;
    return ERR_NONE;
}
DsoErr CAcquire::setLaLength( dsoFract depth )
{
    m_laLength = depth;

    m_laPhySlice = alignPhySlice( depth.ceilLonglong() );

    return ERR_NONE;
}

void CAcquire::setCHCnt( int chCnt, int laCnt )
{
    m_chCnt = chCnt;
    m_laCnt = laCnt;
}

void CAcquire::setSysRawBand(Bandwidth bw)
{
    m_nSysRawBand = bw;
}

Bandwidth CAcquire::getSysRawBand( void )
{
    return m_nSysRawBand;
}

//void CAcquire::setChTransAttr( qlonglong t0,
//                     qlonglong inc,
//                     qlonglong len )
//{
//    m_chAttr.sett( t0, inc );
//    m_chAttr.setLength( len );
//}
//void CAcquire::setLaTransAttr( qlonglong t0,
//                      qlonglong inc,
//                      qlonglong len )
//{
//    m_laAttr.sett( t0, inc );
//    m_laAttr.setLength( len );
//}

void CAcquire::setSaLength( bool b,
                            quint32 chSaLen,
                            quint32 laSaLen )
{
    m_bSaValid = b;
    m_chSaLength = chSaLen;
    m_laSaLength = laSaLen;
}
void CAcquire::setSaValid( bool b )
{
    m_bSaValid = b;
}
bool CAcquire::getSaValid()
{ return m_bSaValid; }

void CAcquire::setFinePos( quint32 finePos )
{
    mFinePos = finePos;
}

AcquireMode CAcquire::getAcquireMode()
{
    return m_eAcquireMode;
}

AcquireTimemode CAcquire::getTimemode()
{
    return m_eTimeMode;
}

int CAcquire::getAverageCount()
{
    return m_s32AverageCount;
}

AcquireInterplate CAcquire::getInterplate()
{
    return m_eInterplate;
}

bool CAcquire::getAntiAliasing()
{
    return m_bAntiAliasing;
}

AcquireSamplemode CAcquire::getSampleMode()
{
    return m_acqSaMode;
}

int CAcquire::getChDepth()
{
    return m_chDepth;
}

int CAcquire::getLaDepth()
{
    return m_laDepth;
}

AcquireDepth CAcquire::getChAcqDepIndex()
{
    return m_chAcqDepIndex;
}
AcquireDepth CAcquire::getLaAcqDepIndex()
{
    return m_laAcqDepIndex;
}

qlonglong CAcquire::getChSaRate()
{
    return m_chSaRate;
}
qlonglong CAcquire::getLaSaRate()
{
    return m_laSaRate;
}
dsoFract CAcquire::getChDotTime()
{ return m_chDotTime; }
dsoFract CAcquire::getLaDotTime()
{ return m_laDotTime; }
dsoFract CAcquire::getChLength()
{
    return m_chLength;
}
dsoFract CAcquire::getLaLength()
{
    return m_laLength;
}
void CAcquire::getSaLength( bool *pb,
                            quint32 *psaChLength,
                            quint32 *psaLaLength )
{
    Q_ASSERT( NULL != pb );
    Q_ASSERT( NULL != psaChLength );
    Q_ASSERT( NULL != psaLaLength );

    *pb = m_bSaValid;
    *psaChLength = m_chSaLength;
    *psaLaLength = m_laSaLength;
}

qlonglong CAcquire::getChPhySlice()
{ return m_chPhySlice; }
qlonglong CAcquire::getLaPhySlice()
{ return m_laPhySlice; }

qlonglong CAcquire::alignPhySlice( qlonglong length )
{
    //! align slice
    if ( length < 1001 )
    { length = 1001; }
    else
    {}

    //! align length
    length = (length + 7 )/ 8 + 20 * 8;
    return length;
}
