#ifndef CSEQUENCE_H
#define CSEQUENCE_H

#include "../base/cengineobj.h"

/*!
 * \brief The CSequence class
 * 帧号系统
 */
class CSequence : public CEngineObj
{
public:
    CSequence();

protected:
    int m_s32Start;
    int m_s32Count;
    int m_s32Current;

public:
    int getStart();
    int getCount();
    int getCurrent();
};

#endif // CSEQUENCE_H
