#include "cvertical.h"

bool CVertical::_bInterleaveEn = true;

void CVertical::setInterleaveEn( bool en )
{
    CVertical::_bInterleaveEn = en;
}

CVertical::CVertical()
{
    m_bShowOnOff = false;       //! view on/off
    m_bTrigOnOff = false;       //! used by trig
    m_bDvmCountOnOff = false;
}

bool CVertical::getOnOff()
{
    if ( CVertical::_bInterleaveEn )
    { return m_bShowOnOff | m_bTrigOnOff | m_bDvmCountOnOff; }
    else
    { return true; }
}

DsoErr CVertical::setShowOnOff( bool b )
{
    m_bShowOnOff = b;
    return ERR_NONE;
}
bool CVertical::getShowOnOff()
{
    return m_bShowOnOff;
}

DsoErr CVertical::setTrigOnOff( bool b )
{
    m_bTrigOnOff = b;
    return ERR_NONE;
}
bool CVertical::getTrigOnOff()
{
    return m_bTrigOnOff;
}

DsoErr CVertical::setDvmCountOnOff( bool b )
{
    m_bDvmCountOnOff = b;
    return ERR_NONE;
}

bool CVertical::getDvmCountOnOff()
{
    return m_bDvmCountOnOff;
}
