#ifndef CCONTROL_H
#define CCONTROL_H

#include "../base/cengineobj.h"

/*!
 * \brief The CControl class
 * run/stop/single action
 */
class CControl : public CEngineObj
{
public:
    CControl();

public:
    ControlAction m_eAction;
    ControlStatus m_eStatus;

public:
    virtual DsoErr setAction( ControlAction eAction );
//    ControlAction getAction() const;

    ControlStatus getStatus();

};

#endif // CCONTROL_H
