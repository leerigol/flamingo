#ifndef CMASK_H
#define CMASK_H

#include <QtCore>
#include "../base/cengineobj.h"

class CMask : public CEngineObj
{
public:
    CMask();

public:
    void saveAux( quint32 aux );
    quint32 loadAux();

public:
    int     mMaskSrc;
    bool    mMaskEn;
    bool    mZoneEn;
    quint32 mPreAux;
    bool mAuxValid;
};

#endif // CMASK_H
