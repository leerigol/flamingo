#ifndef CACQUIRE_H
#define CACQUIRE_H

#include "../base/cengineobj.h"

#include "../../baseclass/dsoattr.h"    //! dso attr
#include "../../baseclass/saflow/saflow.h"

#include "../../phy/hori/flowview.h"
/*!
 * \brief The CAcquire class
 * - acquire mode
 * - time mode
 * - average count
 */
class CAcquire : public CEngineObj
{
public:
    CAcquire();

public:
    AcquireMode m_eAcquireMode;
    AcquireTimemode m_eTimeMode;
    int m_s32AverageCount;
    AcquireInterplate m_eInterplate;

    bool m_bAntiAliasing;
    AcquireSamplemode m_acqSaMode;

    int m_chDepth;              //! attempt length
    int m_laDepth;

    AcquireDepth m_chAcqDepIndex;
    AcquireDepth m_laAcqDepIndex;

    qlonglong m_chSaRate;
    qlonglong m_laSaRate;

    dsoFract m_chDotTime;       //! dottime
    dsoFract m_laDotTime;

    dsoFract m_chLength;        //! theory depth
    dsoFract m_laLength;

    qlonglong m_chPhySlice;     //! physical slice for each frame
    qlonglong m_laPhySlice;

    int       m_chCnt;
    int       m_laCnt;
                              //! sa length
    quint32   m_chSaLength;
    quint32   m_laSaLength;

    Bandwidth m_nSysRawBand;    // added by hxh. for system raw band width

                              //! hattr
//    DsoAttr m_chAttr;
//    DsoAttr m_laAttr;

//    DsoAttr mAcqCH, mAcqLa, mAcqEye;
    quint32 mFinePos;
                              //! main sa flow -- raw info
    CSaFlow mCHFlow, mLaFlow;

                              //! view
    dso_phy::CFlowView mMainCHCoarseView, mZoomCHCoarseView;
    dso_phy::CFlowView mMainCHView, mZoomCHView;
    dso_phy::CFlowView mMainLACoarseView, mZoomLACoarseView;
    dso_phy::CFlowView mMainLAView, mZoomLAView;
    dso_phy::CFlowView mEyeView, mMaskView;

public:
    DsoErr setAcquireMode( AcquireMode mode );
    DsoErr setTimeMode( AcquireTimemode mode );
    DsoErr setAverageCount( int count );
    DsoErr setInterplate( AcquireInterplate interplate );

    DsoErr setAntiAliasing( bool bAnti );
    DsoErr setSampleMode( AcquireSamplemode saMode );

    DsoErr setChDepth( int depth );
    DsoErr setLaDepth( int depth );

    DsoErr setChAcqDepIndex( AcquireDepth ind );
    DsoErr setLaAcqDepIndex( AcquireDepth ind );

    DsoErr setChSaRate( qlonglong saRate, dsoFract dotTime );
    DsoErr setLaSaRate( qlonglong saRate, dsoFract dotTime );

    DsoErr setChSaRate( qlonglong saRate );
    DsoErr setLaSaRate( qlonglong saRate );

    DsoErr setChLength( dsoFract depth );
    DsoErr setLaLength( dsoFract depth );

//    void setChTransAttr( qlonglong t0,
//                         qlonglong inc,
//                         qlonglong len );
//    void setLaTransAttr( qlonglong t0,
//                          qlonglong inc,
//                          qlonglong len );

    void setCHCnt( int chCnt, int laCnt=2 );
    void setSysRawBand( Bandwidth bw = BW_100M );
    Bandwidth getSysRawBand( void );

    void setSaLength( bool b, quint32 chSaLen, quint32 laSaLen = 0 );
    void setSaValid( bool b );
    bool getSaValid();

    void setFinePos( quint32 finePos );

    AcquireMode getAcquireMode();
    AcquireTimemode getTimemode();
    int getAverageCount();
    AcquireInterplate getInterplate();

    bool getAntiAliasing();
    AcquireSamplemode getSampleMode();

    int getChDepth();
    int getLaDepth();

    AcquireDepth getChAcqDepIndex();
    AcquireDepth getLaAcqDepIndex();

    qlonglong getChSaRate();
    qlonglong getLaSaRate();

    dsoFract getChDotTime();
    dsoFract getLaDotTime();

    dsoFract getChLength();
    dsoFract getLaLength();

    void getSaLength( bool *pb,
                      quint32 *pChSaLength,
                      quint32 *pLaSaLength );

    qlonglong getChPhySlice();
    qlonglong getLaPhySlice();

private:
    qlonglong alignPhySlice( qlonglong length );
    bool      m_bSaValid;
};

#endif // CACQUIRE_H
