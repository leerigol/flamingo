#include <QtCore>
#include "./csearch.h"


CSearch::CSearch()
{
    mBmProcEn         = 0; //! ch,la,zoom
    mMemT0            = 0;
    mMemTInc          = 1; //! ps
    mViewT0           = 0;
    mViewTInc         = 1; //! ps
    mColumnResolution = 1;
    mViewStart        = 0;
    mViewLength       = 0;
    m_control         = Control_Stoped;
}

void CSearch::setEnable(quint32 bmEn)
{
    mBmProcEn = bmEn;
}

quint32 CSearch::getEnable()
{
    return mBmProcEn;
}

int CSearch::setMemAttr(qlonglong t0, qlonglong tInc)
{
    Q_ASSERT( mMemTInc != 0);
    mMemT0   = t0;
    mMemTInc = tInc;
    mColumnResolution = mViewTInc/mMemTInc;
    mColumnResolution = qMax(mColumnResolution, (qlonglong)1); //! [bug, 开搜索后，auto崩溃]
    return 0;
}

int CSearch::setViewAttr(qlonglong t0, qlonglong tInc)
{
    Q_ASSERT( mMemTInc != 0);
    mViewT0   = t0;
    mViewTInc = tInc;
    mColumnResolution = mViewTInc/mMemTInc;
    mColumnResolution = qMax(mColumnResolution, (qlonglong)1); //! [bug, 开搜索后，auto崩溃]
    return 0;
}

int CSearch::setViewRange(int start, int len)
{
    //Q_ASSERT(len>=0);
    if(len<0)
    {
        len = 0;
        qWarning()<<__FILE__<<__FUNCTION__<<__LINE__<<"view len:"<<len;
    }

    mViewStart  = start;
    mViewLength = len;
    return 0;
}

void CSearch::setControl(ControlStatus control)
{
    m_control = control;
}

quint32 CSearch::getBmProcEn() const
{
    return mBmProcEn;
}

qlonglong CSearch::getMemT0() const
{
    return mMemT0;
}

qlonglong CSearch::getColumnResolution() const
{
    return mColumnResolution;
}

int CSearch::getViewStart()
{
    return mViewStart;
}

int CSearch::getViewLength()
{
    return mViewLength;
}
qlonglong CSearch::getViewTInc() const
{
    return mViewTInc;
}

qlonglong CSearch::getViewT0() const
{
    return mViewT0;
}

qlonglong CSearch::getMemTInc() const
{
    return mMemTInc;
}
ControlStatus CSearch::getControl() const
{
    return m_control;
}
