#include "chorizontal.h"

/*!
 * \brief CHorizontal::CHorizontal
 */
CHorizontal::CHorizontal()
{

}

/*!
 * \brief CHorizontal::setViewMode
 * \param mode
 * \return
 * main
 * main + zoom
 */
DsoErr CHorizontal::setViewMode( HorizontalViewmode mode )
{
    m_eViewMode = mode;
    return ERR_NONE;
}
/*!
 * \brief CHorizontal::getViewMode
 * \return
 * get view mode
 */
HorizontalViewmode CHorizontal::getViewMode()
{
    return m_eViewMode;
}
/*!
 * \brief CHorizontal::getMainView
 * \return
 * main view
 */
CHorizontalView &CHorizontal::getMainView()
{
    return m_mainView;
}
/*!
 * \brief CHorizontal::getZoomView
 * \return
 * zoom view
 */
CHorizontalView &CHorizontal::getZoomView()
{
    return m_zoomView;
}
