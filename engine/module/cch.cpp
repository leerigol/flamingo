#include "cch.h"

CCH::CCH()
{
    m_bInvert = false;

    m_s32Scale = 1000;
    m_s32Offset = -1000;

    m_eImpedance = IMP_50;
    m_eBandwidth = BW_FULL;

    m_s32Delay = 0;
    m_s32Null = 0;

    m_s32AfeScale = 1000;
    m_s32AfeGnd = 128;
    m_probeRatio.setVal( 1ll );
}

bool CCH::operator==( const CCH &a )
{   
    if ( m_bShowOnOff != a.m_bShowOnOff ) return false;
    if ( m_bTrigOnOff != a.m_bTrigOnOff ) return false;

    if ( m_s32Scale != a.m_s32Scale ) return false;
    if ( m_s32Offset != a.m_s32Offset ) return false;
    if ( m_bInvert != a.m_bInvert ) return false;
    if ( m_eImpedance != a.m_eImpedance ) return false;

    if ( m_eBandwidth != a.m_eBandwidth ) return false;
    if ( m_eCoupling != a.m_eCoupling ) return false;
    if ( m_eUnit != a.m_eUnit ) return false;

    if ( m_s32Delay != a.m_s32Delay ) return false;
    if ( m_s32Null != a.m_s32Null ) return false;

    if ( m_s32AfeScale != a.m_s32AfeScale ) return false;

    return true;
}
bool CCH::operator!=( const CCH &a )
{
    return !(*this == a);
}

bool CCH::operator==( const CCH *pa )
{
    Q_ASSERT( NULL != pa );
    return *this == *pa;
}
bool CCH::operator!=( const CCH *pa )
{
    Q_ASSERT( NULL != pa );

    return !( *this == *pa );
}

DsoErr CCH::setScale( int scale )
{
    m_s32Scale = scale;
    return ERR_NONE;
}

DsoErr CCH::setOffset( int offset )
{
    m_s32Offset = offset;
    return ERR_NONE;
}

DsoErr CCH::setInvert( bool bInv )
{
    m_bInvert = bInv;
    return ERR_NONE;
}

DsoErr CCH::setImpedance( Impedance imp )
{
    m_eImpedance = imp;
    return ERR_NONE;
}

/*!
 * \brief CCH::setBandwidth
 * \param band
 * \return
 * bandlimit = bandwidth, when bandlimit == off, the channel bandwidth = model Bandwidth
 * So, bandwidth can not =  off in Engine
 */
DsoErr CCH::setBandwidth( Bandwidth band )
{
    m_eBandwidth = band;
    return ERR_NONE;
}

DsoErr CCH::setCoupling( Coupling coup )
{
    m_eCoupling = coup;
    return ERR_NONE;
}

DsoErr CCH::setUnit(Unit unit)
{
    m_eUnit = unit;
    return ERR_NONE;
}

DsoErr CCH::setDelay( int dly )
{
    m_s32Delay = dly;
    return ERR_NONE;
}

DsoErr CCH::setNull( int bias )
{
    m_s32Null = bias;
    return ERR_NONE;
}

DsoErr CCH::setAfeScale( int scale )
{
    m_s32AfeScale = scale;
    return ERR_NONE;
}
DsoErr CCH::setAfeGnd( int gnd )
{
    m_s32AfeGnd = gnd;
    return ERR_NONE;
}
DsoErr CCH::setProbeRatio( DsoReal ratio )
{
    m_probeRatio = ratio;
    return ERR_NONE;
}

int CCH::getScale()
{
    return m_s32Scale;
}

int CCH::getOffset()
{
    return m_s32Offset;
}

bool CCH::getInvert()
{
    return m_bInvert;
}

Impedance CCH::getImpedance()
{
    return m_eImpedance;
}

Bandwidth CCH::getBandwidth()
{
    return m_eBandwidth;
}

Coupling CCH::getCoupling()
{
    return m_eCoupling;
}

Unit CCH::getUnit()
{
   return m_eUnit;
}

int CCH::getDelay()
{
    return m_s32Delay;
}

int CCH::getNull()
{
    return m_s32Null;
}

int CCH::getAfeScale()
{
    return m_s32AfeScale;
}

int CCH::getAfeGnd()
{
    return m_s32AfeGnd;
}

DsoReal CCH::getProbeRatio()
{
    return m_probeRatio;
}

//! 1 -- +
//! -1 -- -
int CCH::getOffsetSign()
{
    if ( m_eImpedance == IMP_50 )
    {
        if ( m_bInvert ) return 1;
        else return -1;
    }
    else
    {
        if ( m_bInvert ) return -1;
        else return 1;
    }
}
