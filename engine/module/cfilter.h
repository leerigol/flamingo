#ifndef CFILTER_H
#define CFILTER_H

#include <QtCore>

#include "../../include/dsotype.h"

using namespace DsoType;

/*!
 * \brief The CSideFf class
 * only one side pass filter
 */
class CSidePf
{
public:
    CSidePf();

public:
    DsoErr setW( qlonglong w );
    qlonglong getW();
public:
    qlonglong mW;
};

class CRangePf
{
public:
    CRangePf();

public:
    DsoErr setlW( qlonglong w );
    DsoErr sethW( qlonglong w );

    qlonglong getlW();
    qlonglong gethW();

public:
    qlonglong mlW;
    qlonglong mhW;
};


class CLpf : public CSidePf
{
public:
    CLpf();
};

class CHpf : public CSidePf
{
public:
    CHpf();
};

class CBpf : public CRangePf
{
public:
    CBpf();
};

class CBtf : public CRangePf
{
public:
    CBtf();
};

class CFilter
{
public:
    CFilter();

    DsoErr setEnable( bool b );
    bool getEnable();

    DsoErr setFilter( Filter e );
    Filter getFilter();

    DsoErr setLpfW( qlonglong w );
    qlonglong getLpfW();

    DsoErr setHpfW( qlonglong w );
    qlonglong getHpfW();

    DsoErr setBpfWl(qlonglong w );
    qlonglong getBpfWl();

    DsoErr setBpfWh(qlonglong w );
    qlonglong getBpfWh();

    DsoErr setBtfWl(qlonglong w );
    qlonglong getBtfWl();

    DsoErr setBtfWh(qlonglong w);
    qlonglong getBtfWh();

public:
    bool mEnable;

    CLpf mLpf;
    CHpf mHpf;
    CBpf mBpf;
    CBtf mBtf;

public:
    Filter mFilter;

};

#endif // CFILTER_H
