#ifndef CHORIZONTALVIEW_H
#define CHORIZONTALVIEW_H

#include "../base/cengineobj.h"

/*!
 * \brief The CHorizontalView class
 * 水平视图
 * - scale
 * - offset
 */
class CHorizontalView : public CEngineObj
{
public:
    CHorizontalView();

public:
    qlonglong m_s64Scale;   //! ps unit
    qlonglong m_s64Offset;  //! ps unit

    CHorizontalView *m_pParentView;

public:
    virtual DsoErr setScale( qlonglong scale );
    virtual DsoErr setOffset( qlonglong offset );
    virtual DsoErr setRollOffset( qlonglong rollOffset);
    DsoErr setParent( CHorizontalView * parent);

    qlonglong getScale();
    qlonglong getOffset();
    qlonglong getRollOffset();
    CHorizontalView *getParent();

protected:
    //! 只有在stop状态下，roll的offset才可调，否则不可调。
    qlonglong m_s64rollOffset;
};

#endif // CHORIZONTALVIEW_H
