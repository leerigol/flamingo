#include "ctrigger.h"

/*!
 * \brief CTriggerOut::CTriggerOut
 */
CTriggerOut::CTriggerOut()
{}

DsoErr CTriggerOut::setOnOff( bool b )
{
    mOnOff = b;
    return ERR_NONE;
}
bool CTriggerOut::getOnOff() const
{
    return mOnOff;
}

DsoErr CTriggerOut::setPolarity( bool b )
{
    mPolarity = b;
    return ERR_NONE;
}
bool CTriggerOut::getPolarity() const
{
    return mPolarity;
}

DsoErr CTriggerOut::setDelay( int delay )
{
    mDelay = delay;
    return ERR_NONE;
}
int CTriggerOut::getDelay() const
{
    return mDelay;
}

DsoErr CTriggerOut::setPulse( int pulse )
{
    mPulse = pulse;
    return ERR_NONE;
}
int CTriggerOut::getPulse() const
{
    return mPulse;
}

/*!
 * \brief CTriggerHold::CTriggerHold
 */
CTriggerHold::CTriggerHold()
{}

DsoErr CTriggerHold::setMode( TriggerHoldMode mode )
{
    mMode = mode;
    return ERR_NONE;
}
TriggerHoldMode CTriggerHold::getMode() const
{
    return mMode;
}

DsoErr CTriggerHold::setCount( int count )
{
    mHoldCount = count;
    return ERR_NONE;
}
int CTriggerHold::getCount()
{
    return mHoldCount;
}

/*!
 * \brief CTrigger::CTrigger
 */
CTrigger::CTrigger()
{
    mSweep = Trigger_Sweep_Auto;

    mTdAutoTmo = time_ms(100);   //! for 50hz
}

DsoErr CTrigger::setMode( TriggerMode mode )
{
    mMode = mode;
    return ERR_NONE;
}
TriggerMode CTrigger::getMode() const
{
    return mMode;
}

DsoErr CTrigger::setSweep( TriggerSweep sweep )
{
    mSweep = sweep;
    return ERR_NONE;
}
TriggerSweep CTrigger::getSweep() const
{
    return mSweep;
}

DsoErr CTrigger::setTdAutoTmo( qlonglong tmo )
{
    if ( tmo < time_ms(100) )
    { return ERR_OVER_RANGE; }
    if ( tmo > time_s(13))
    { return ERR_OVER_RANGE;}

    mTdAutoTmo = tmo;

    return ERR_NONE;
}
qlonglong CTrigger::getTdAutoTmo()
{ return mTdAutoTmo; }

CTriggerOut & CTrigger::getTrigOut()
{
    return mTrigOut;
}

DsoErr CTrigger::setTrigHoldObj( TriggerHoldObj hold )
{
    mHoldObj = hold;
    return ERR_NONE;
}

TriggerHoldObj CTrigger::getTrigHoldObj()
{
    return mHoldObj;
}

CTriggerHold & CTrigger::getTrigHold()
{
    if ( mHoldObj == Trigger_Hold_Time )
    {
        return mTimeHold;
    }
    else
    {
        return mEventHold;
    }
}

DsoErr CTrigger::setSens( int sens )
{
    mSens = sens;
    return ERR_NONE;
}
int CTrigger::getSens() const
{
    return mSens;
}

DsoErr CTrigger::setCoupling( Coupling coup )
{
    mCoupling = coup;
    return ERR_NONE;
}
Coupling CTrigger::getCoupling()
{
    return mCoupling;
}
