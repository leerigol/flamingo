#ifndef CSEARCH
#define CSEARCH
#include <QtCore>
#include "../base/cengineobj.h"
#include "ctrigger.h"

class CSearch : public CTrigger
{
public:
    CSearch();

public:
    void          setEnable(  quint32 bmEn);
    quint32       getEnable();
    int           setMemAttr( qlonglong t0, qlonglong tInc);
    int           setViewAttr(qlonglong t0, qlonglong tInc);
    int           setViewRange(int start, int len);
    void          setControl(ControlStatus control);

public:
    quint32   getBmProcEn() const;
    qlonglong getMemT0() const;
    qlonglong getMemTInc() const;
    qlonglong getViewT0() const;
    qlonglong getViewTInc() const;
    qlonglong getColumnResolution() const;
    int       getViewStart();
    int       getViewLength();
    ControlStatus getControl() const;

public:
    quint32       mBmProcEn;               //! ch,la,zoom
    qlonglong     mMemT0,  mMemTInc;       //! ps
    qlonglong     mViewT0, mViewTInc;      //! ps
    qlonglong     mColumnResolution;
    int           mViewStart,mViewLength;
    ControlStatus m_control;
};
#endif // CSEARCH

