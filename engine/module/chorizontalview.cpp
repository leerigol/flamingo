#include "chorizontalview.h"
#include "../../include/dsocfg.h"
/*!
 * \brief CHorizontalView::CHorizontalView
 * constructor
 */
CHorizontalView::CHorizontalView()
{
    m_s64Offset = 0;
    m_s64Scale = 0;
}

/*!
 * \brief CHorizontalView::setScale
 * \param scale
 * \return
 * scale setting
 */
DsoErr CHorizontalView::setScale( qlonglong scale )
{
    Q_ASSERT(scale >= time_ps(200) && scale <= time_s(1000) );
    m_s64Scale = scale;
    return ERR_NONE;
}
/*!
 * \brief CHorizontalView::setOffset
 * \param offset
 * \return
 * offset set
 */
DsoErr CHorizontalView::setOffset( qlonglong offset )
{
    m_s64Offset = offset;
    return ERR_NONE;
}

DsoErr CHorizontalView::setRollOffset(qlonglong rollOffset)
{
    m_s64rollOffset = rollOffset;
    return ERR_NONE;
}

/*!
 * \brief CHorizontalView::setParent
 * \param parent
 * \return
 * zoom view must not more than parent view.
 * Eg: main scale: 10us, zoom scale canbe 10us, 5us...
 *
 */
DsoErr CHorizontalView::setParent( CHorizontalView * parent )
{
    m_pParentView = parent;
    return ERR_NONE;
}

/*!
 * \brief CHorizontalView::getScale
 * \return
 * scale get
 */
qlonglong CHorizontalView::getScale()
{
    return m_s64Scale;
}
/*!
 * \brief CHorizontalView::getOffset
 * \return
 * offset get
 */
qlonglong CHorizontalView::getOffset()
{
    return m_s64Offset;
}

qlonglong CHorizontalView::getRollOffset()
{
    return m_s64rollOffset;
}

/*!
 * \brief CHorizontalView::getParent
 * \return
 * parent view
 */
CHorizontalView *CHorizontalView::getParent()
{
    return m_pParentView;
}
