#ifndef CCH_H
#define CCH_H

#include "cvertical.h"

/*!
 * \brief The CCH class
 * analog channel
 */
class CCH : public CVertical
{
public:
    CCH();

protected:
    int m_s32Scale; //! uv unit
    int m_s32Offset;//! uv unit
    bool m_bInvert;
    Impedance m_eImpedance;

    Bandwidth m_eBandwidth;
    Coupling m_eCoupling;
    Unit     m_eUnit;

    int m_s32Delay; //! ps unit
    int m_s32Null;  //! vertical zero

                    //! uv unit
                    //! afe scale
    int m_s32AfeScale;
    int m_s32AfeGnd;//! adc

    DsoReal m_probeRatio;

public:
    bool operator==( const CCH &a );
    bool operator!=( const CCH &a );

    bool operator==( const CCH *pa );
    bool operator!=( const CCH *pa );

public:
    DsoErr setScale( int scale );
    DsoErr setOffset( int offset );
    DsoErr setInvert( bool bInv );
    DsoErr setImpedance( Impedance imp );

    DsoErr setBandwidth( Bandwidth band );
    DsoErr setCoupling( Coupling coup );
    DsoErr setUnit( Unit unit );

    DsoErr setDelay( int dly );
    DsoErr setNull( int bias );

    DsoErr setAfeScale( int scale );
    DsoErr setAfeGnd( int gnd );
    DsoErr setProbeRatio( DsoReal ratio );

    int getScale();
    int getOffset();
    bool getInvert();
    Impedance getImpedance();

    Bandwidth getBandwidth();
    Coupling getCoupling();
    Unit getUnit();

    int getDelay();
    int getNull();
    int getAfeScale();
    int getAfeGnd();
    DsoReal getProbeRatio();

    //! by invert
    int getOffsetSign();

};

#endif // CCH_H
