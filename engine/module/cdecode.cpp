#include "cdecode.h"
#include "../include/dsodbg.h"

CDecode::CDecode()
{
    for( int i = 0; i < array_count(mThres); i+=2 )
    {
        mThres[i] = 128;
        mThres[i+1] = 100;

        mThresL[i] = 100;
        mThresL[i+1] = 75;
    }
}

DsoErr CDecode::setThreshold( Chan ch,
                            int h, int l )
{

    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    //! default sens.
    if ( l == -1 )
    {
        l = h - 10;
    }

    //! limit the range
    if ( l < 0 ) l = 0;

    mThres[ (ch - chan1)*2 ] = h;
    mThres[ (ch - chan1)*2 + 1 ] = l;

    return ERR_NONE;
}

void CDecode::getThreshold( Chan ch, int *pH, int *pL )
{
    Q_ASSERT( ch >= chan1 && ch <= chan3 );
    Q_ASSERT( pH != NULL && pL != NULL );

    *pH = mThres[ (ch - chan1)*2 ];
    *pL = mThres[ (ch - chan1)*2 + 1 ];
}

int *CDecode::getThresholds()
{
    return mThres;
}

DsoErr CDecode::setThresholdL( Chan ch, int h, int l )
{
    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    //! default sens.
    if ( l == -1 )
    {
        l = h - 10;
    }

    //! limit the range
    if ( l < 0 ) l = 0;

    mThresL[ (ch - chan1)*2 ] = h;
    mThresL[ (ch - chan1)*2 + 1 ] = l;

    return ERR_NONE;
}
void CDecode::getThresholdL( Chan ch, int *pH, int *pL )
{
    Q_ASSERT( ch >= chan1 && ch <= chan3 );
    Q_ASSERT( pH != NULL && pL != NULL );

    *pH = mThresL[ (ch - chan1)*2 ];
    *pL = mThresL[ (ch - chan1)*2 + 1 ];
}
int *CDecode::getThresholdLs()
{
    return mThresL;
}
