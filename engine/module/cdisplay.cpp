#include "cdisplay.h"
#include "../include/dsodbg.h"
chanDisp::chanDisp( Chan ch,
                    unsigned int gpId,
                    unsigned int chId )
{
    mCh = ch;
    mbEn = false;

    mGpId = gpId;
    mChId = chId;
}

void chanDisp::setEn( bool en )
{
    mbEn= en;
}
bool chanDisp::getEn()
{
    return mbEn;
}

unsigned int chanDisp::getGpId()
{ return mGpId; }

unsigned int chanDisp::getChId()
{ return mChId; }

CDisplay::CDisplay()
{
    chanDisp *pChan;

    mPersistTime = 0;
    mPalette = Wfm_color;
    mIntensity = 50;
    mbScreenPersist = true;
    mLineType = Wfm_Line;

    //! fill the chan table
    pChan = new chanDisp( chan1, 0, 0 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    pChan = new chanDisp( chan2, 0, 1 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    pChan = new chanDisp( chan3, 0, 2 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    pChan = new chanDisp( chan4, 0, 3 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    //! math
    pChan = new chanDisp( m1, 1, 0 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    pChan = new chanDisp( m2, 1, 1 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    pChan = new chanDisp( m3, 1, 2 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    pChan = new chanDisp( m4, 1, 3 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );

    //! logic
    pChan = new chanDisp( d0d15, 2, 0 );
    Q_ASSERT( NULL != pChan );
    mChanDisp.append( pChan );
}

CDisplay::~CDisplay()
{
    chanDisp *pCh;

    foreach( pCh, mChanDisp )
    {
        Q_ASSERT( NULL != pCh );
        delete pCh;

        LOG_DBG();
    }
}

CDisplay::CDisplay( const CDisplay &disp )
{
    *this = disp;
}
CDisplay &CDisplay::operator=( const CDisplay &disp )
{
    mViewOrder = disp.mViewOrder;

    mPersistTime = disp.mPersistTime;
    mIntensity = disp.mIntensity;
    mPalette = disp.mPalette;
    mbScreenPersist = disp.mbScreenPersist;

    mLineType = disp.mLineType;

    //! now for the resource
    Q_ASSERT( disp.mChanDisp.size() == mChanDisp.size() );
    for ( int i = 0; i < disp.mChanDisp.size(); i++ )
    {
        *mChanDisp[i] = *disp.mChanDisp[i];
    }

    return *this;
}

void CDisplay::setChanEn( Chan ch, bool b )
{
    //! find
    chanDisp *pItem = findChan( ch );
    if ( NULL == pItem )
    {
        Q_ASSERT( false );
        return;
    }

    pItem->setEn( b );

    //! disable
    //! to the list last
    if ( !b )
    {
        mChanDisp.removeAll( pItem );
        mChanDisp.append( pItem );
    }
}
bool CDisplay::setChanTop( Chan ch )
{
    //! find
    chanDisp *pItem = findChan( ch );
    //! no ch item
    if ( NULL == pItem )
    {
        return false;
    }

    if ( !pItem->getEn() )
    { return false; }

    //! set to first
    mChanDisp.removeAll( pItem );
    mChanDisp.prepend( pItem );

    return true;
}
void CDisplay::setViewOrder( int chs, Chan chans[] )
{
    mViewOrder.clear();
    for ( int i = 0; i < chs; i++ )
    { mViewOrder.append( chans[i]); }
}
void CDisplay::setViewOrder( QList<Chan> &order )
{ mViewOrder = order; }
QList<Chan> &CDisplay::getViewOrder()
{ return mViewOrder; }

void CDisplay::setPersistTime( int timems )
{
    mPersistTime = timems;
}
int CDisplay::getPersistTime()
{
    return mPersistTime;
}

void CDisplay::setPalette( WfmPalette pal )
{
    mPalette = pal;
}
WfmPalette CDisplay::getPalette()
{
    return mPalette;
}

void CDisplay::setIntensity( int intens )
{ mIntensity = intens; }
int CDisplay::getIntensity()
{ return mIntensity; }

void CDisplay::setScreenPersist( bool b )
{ mbScreenPersist = b; }
bool CDisplay::getScreenPersist()
{ return mbScreenPersist; }

void CDisplay::setLineType( WfmLineType lineType )
{ mLineType = lineType; }
WfmLineType CDisplay::getLineType()
{ return mLineType; }

//! ch:0
//! math:1
//! la:2
//! bit[0:5]
unsigned int CDisplay::getGpOrder()
{
    chanDisp *pItem;

    //! group order
    QList< unsigned int > gpStack;
    //! scan the group
    foreach( pItem, mChanDisp )
    {
        Q_ASSERT( pItem != NULL );

        //! contains
        if ( gpStack.contains(pItem->getGpId()) )
        {}
        else
        {
            gpStack.append( pItem->getGpId() );
        }
    }

    unsigned int gpOrder;
    gpOrder = 0;

    unsigned int val;
    int shift = 0;

    foreach( val, gpStack )
    {
        gpOrder |= val<<shift;
        shift += 2;
    }

    return gpOrder;
}

//! ch1:0,ch2:1,ch3:2,ch4:3
//! [0:7]
//! bit[0:1] at top
unsigned int CDisplay::getChOrder()
{
    //! ch order
    //! extract the chs
    QList< chanDisp * > chList;

    chanDisp *pItem;

    foreach( pItem, mChanDisp )
    {
        if ( pItem->getGpId() == 0 )
        {
            chList.append( pItem );
        }
    }

    unsigned int chOrder;
    int shift = 0;
    chOrder = 0;
    foreach( pItem, chList )
    {
        chOrder |= pItem->getChId()<<shift;
        shift +=2;
    }

    return chOrder;
}
//! m1:0,m2:1,m3:2,m4:3
//! [0:7]
//! bit[0:1] at top
unsigned int CDisplay::getMathOrder()
{
    //! ch order
    //! extract the chs
    QList< chanDisp * > chList;

    chanDisp *pItem;

    foreach( pItem, mChanDisp )
    {
        if ( pItem->getGpId() == 1 )
        {
            chList.append( pItem );
        }
    }

    unsigned int mOrder;
    int shift = 0;
    mOrder = 0;
    foreach( pItem, chList )
    {
        mOrder |= pItem->getChId()<<shift;
        shift +=2;
    }

    return mOrder;
}
unsigned int CDisplay::getLaOrder()
{
    return 0;
}

chanDisp *CDisplay::findChan( Chan ch )
{
    chanDisp *pItem;

    foreach( pItem, mChanDisp )
    {
        Q_ASSERT( NULL != pItem );

        if ( pItem->mCh == ch )
        { return pItem; }
    }

    return NULL;
}

