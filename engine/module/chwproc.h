#ifndef CMEASURE_H
#define CMEASURE_H

#include <QtCore>
#include "../base/cengineobj.h"

class CHwProc : public CEngineObj
{
public:
    CHwProc();

public:
    void setEnable( quint32 bmEn);
    quint32 getEnable();

    void setRange( qlonglong t0, qlonglong tlength );
    void getRange( qlonglong &t0, qlonglong &tlength );

    void setView( HorizontalView view );
    HorizontalView getView();
public:
    quint32 mBmProcEn;    //! ch,la,zoom

    qlonglong mT0,mTLength;

    HorizontalView mView;
};

#endif // CMEASURE_H
