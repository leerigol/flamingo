#ifndef CTRIGGER_H
#define CTRIGGER_H

#include "../base/cengineobj.h"
#include "../base/enginecfg.h"
#include "../base/trigger/enginecfg_trig.h"

class CTriggerOut
{
public:
    CTriggerOut();

protected:
    bool mOnOff;
    bool mPolarity;
    int mDelay;     //! ns
    int mPulse;     //! ns

public:
    DsoErr setOnOff( bool b );
    bool getOnOff() const;

    DsoErr setPolarity( bool b );
    bool getPolarity() const;

    DsoErr setDelay( int delay );
    int getDelay() const;

    DsoErr setPulse( int pulse );
    int getPulse() const;
};

class CTriggerHold
{
public:
    CTriggerHold();

protected:
    TriggerHoldMode mMode;
    int mHoldCount;

public:
    DsoErr setMode( TriggerHoldMode mode );
    TriggerHoldMode getMode() const;

    DsoErr setCount( int count );
    int getCount();
};

class CLevel
{
protected:
    int mLevelsA[ 4 + 1 + 2 ];  // ch + ext + la
    int mLevelsB[ 4 + 1 + 2 ];

};

class CTrigger : public CEngineObj
{
public:
    CTrigger();

public:
    CTriggerOut mTrigOut;

    CTriggerHold mTimeHold;
    CTriggerHold mEventHold;

    TriggerMode mMode;
    TriggerSweep mSweep;
    TriggerHoldObj mHoldObj;

    int mSens;      //! 0.01
    Coupling mCoupling;

    qlonglong mTdAutoTmo;

    CLevel mTrigLevel;

    //! trig cases
    TrigEdgeCfg mEdgeCfg;
    TrigPulseCfg mPulseCfg;
    TrigRuntCfg mRuntCfg;
    TrigOverCfg mOverCfg;

    TrigWindowCfg mWindowCfg;
    TrigSlopeCfg mSlopeCfg;
    TrigTimeoutCfg mTimeoutCfg;
    TrigSHCfg mSHCfg;

    TrigDelayCfg mDelayCfg;
    TrigNEdgeCfg mNEdgeCfg;
    TrigPatternCfg mPatternCfg;
    TrigVideoCfg mVideoCfg;

    TrigDurationCfg mDurationCfg;
    TrigRS232Cfg    mRS232Cfg;
    TrigIICcfg      mIIcCfg;
    TrigSpiCfg      mSpiCfg;
    TrigABCfg       mABCfg;
    TrigCanCfg      mCanCfg;
    TrigFlexrayCfg  mFlexrayCfg;
    TrigStd1553bCfg mStd1553bCfg;
    TrigLinCfg      mLinCfg;
    TrigI2SCfg      mIIsCfg;
    TrigZoneCfg     mZoneCfg;
public:
    DsoErr setMode( TriggerMode mode );
    TriggerMode getMode() const;

    DsoErr setSweep( TriggerSweep sweep );
    TriggerSweep getSweep() const;

    DsoErr setTdAutoTmo( qlonglong tmo );
    qlonglong getTdAutoTmo();

    CTriggerOut &getTrigOut();

    DsoErr setTrigHoldObj( TriggerHoldObj hold );
    TriggerHoldObj getTrigHoldObj();

    CTriggerHold &getTrigHold();

    DsoErr setSens( int sens );
    int getSens() const;

    DsoErr setCoupling( Coupling coup );
    Coupling getCoupling();
};

#endif // CTRIGGER_H
