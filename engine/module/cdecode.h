#ifndef CDECODE_H
#define CDECODE_H

#include <QtCore>

#include "../../include/dsotype.h"
using namespace DsoType;

class CDecode
{
public:
    CDecode();

public:
    //! Single Threshold or H Threshold
    DsoErr setThreshold( Chan ch, int h, int l = -1 );
    void getThreshold( Chan ch, int *pH, int *pL );
    int *getThresholds();

    //! low threshold
    DsoErr setThresholdL( Chan ch, int h, int l = -1 );
    void getThresholdL( Chan ch, int *pH, int *pL );
    int *getThresholdLs();
protected:
    int mThres[2*4];    //! h,l,h,l,h,l,h,l
    int mThresL[2*4];    //! h,l,h,l,h,l,h,l
};

#endif // CDECODE_H
