#include "cmask.h"

CMask::CMask()
{
    mPreAux = 0;
    mMaskEn = false;
    mZoneEn = false;
}

void CMask::saveAux( quint32 aux )
{
    mPreAux = aux;
}
quint32 CMask::loadAux()
{
    return mPreAux;
}
