#include "cfilter.h"

CSidePf::CSidePf()
{}

DsoErr CSidePf::setW(qlonglong w)
{
    mW = w;
    return ERR_NONE;
}

qlonglong CSidePf::getW()
{
    return mW;
}

CRangePf::CRangePf()
{}

DsoErr CRangePf::setlW( qlonglong w )
{
    mlW = w;

    return ERR_NONE;
}
DsoErr CRangePf::sethW( qlonglong w )
{
    mhW = w;

    return ERR_NONE;
}

qlonglong CRangePf::getlW()
{
    return mlW;
}
qlonglong CRangePf::gethW()
{
    return mhW;
}

CLpf::CLpf()
{}

CHpf::CHpf()
{}

CBpf::CBpf()
{}

CBtf::CBtf()
{}

CFilter::CFilter()
{
}

DsoErr CFilter::setEnable( bool b )
{
    mEnable = b;
    return ERR_NONE;
}
bool CFilter::getEnable()
{
    return mEnable;
}

DsoErr CFilter::setFilter( Filter e )
{
    mFilter = e;
    return ERR_NONE;
}
Filter CFilter::getFilter()
{
    return mFilter;
}

DsoErr CFilter::setLpfW( qlonglong w )
{
    return mLpf.setW(w);
}
qlonglong CFilter::getLpfW()
{
    return mLpf.getW();
}
DsoErr CFilter::setHpfW( qlonglong w )
{
    return mHpf.setW(w);
}
qlonglong CFilter::getHpfW()
{
    return mHpf.getW();
}

DsoErr CFilter::setBpfWl(qlonglong w )
{
    return mBpf.setlW(w);
}
qlonglong CFilter::getBpfWl()
{
    return mBpf.getlW();
}

DsoErr CFilter::setBpfWh(qlonglong w )
{
    return mBpf.sethW(w);
}
qlonglong CFilter::getBpfWh()
{
    return mBpf.gethW();
}

DsoErr CFilter::setBtfWl(qlonglong w )
{
    return mBtf.setlW(w);
}
qlonglong CFilter::getBtfWl()
{
    return mBtf.getlW();
}

DsoErr CFilter::setBtfWh(qlonglong w)
{
    return mBtf.sethW(w);
}
qlonglong CFilter::getBtfWh()
{
    return mBtf.gethW();
}

