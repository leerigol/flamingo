#ifndef CDISPLAY_H
#define CDISPLAY_H

#include "../base/cengineobj.h"

class chanDisp
{

public:
    chanDisp( Chan ch,
              unsigned int gpId,
              unsigned int chId );

public:
    void setEn( bool b );
    bool getEn();

    unsigned int getGpId();
    unsigned int getChId();
public:
    Chan mCh;
    bool mbEn;

    unsigned int mGpId;
    unsigned int mChId;
};

class CDisplay : public CEngineObj
{
public:
    CDisplay();
    ~CDisplay();

    CDisplay( const CDisplay &disp );
    CDisplay &operator=( const CDisplay &disp );

public:
    void setChanEn( Chan ch, bool b );
    bool setChanTop( Chan ch );

    void setViewOrder( int chs, Chan chans[] );
    void setViewOrder( QList<Chan> &order );
    QList<Chan> &getViewOrder();

    void setPersistTime( int timems );
    int getPersistTime();

    void setPalette( WfmPalette pal );
    WfmPalette getPalette();

    void setIntensity( int intens );
    int getIntensity();

    void setScreenPersist( bool b );
    bool getScreenPersist();

    void setLineType( WfmLineType lineType );
    WfmLineType getLineType();

    unsigned int getGpOrder();
    unsigned int getChOrder();
    unsigned int getMathOrder();
    unsigned int getLaOrder();
protected:
    chanDisp *findChan( Chan ch );

protected:
    QList< chanDisp* > mChanDisp;

    QList< Chan > mViewOrder;

    int mPersistTime;
    int mIntensity;
    WfmPalette mPalette;
    bool mbScreenPersist;

    WfmLineType mLineType;
};

#endif // CDISPLAY_H
