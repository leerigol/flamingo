#include "cla.h"

CLa::CLa()
{
    mSize = Medium;

    mEns = 0;
    mActives = 0;
    for ( int i = 0; i < array_count(mPoses);i++ )
    {
        mPoses[i] = 0;
    }

    for( int i = 0; i < array_count(mGroupDx); i++ )
    {
        mGroupDx[i] = 0;
    }

    mColors[0] = 0x008000;
    mColors[1] = 0xffffff;
    mColors[2] = 0x00ff00;
}

void CLa::setColor( int hel, quint32 color )
{
    Q_ASSERT( hel >= 0 && hel < array_count(mColors) );

    mColors[hel] = color;
}
quint32 CLa::getColor( int hel )
{
    Q_ASSERT( hel >= 0 && hel < array_count(mColors) );

    return mColors[hel];
}

