#include "csequence.h"
/*!
 * \brief CSequence::CSequence
 * constructor
 */
CSequence::CSequence()
{
    m_s32Start = 0;
    m_s32Count = 0;
    m_s32Current = 0;
}

/*!
 * \brief CSequence::getStart
 * \return
 * start frame
 */
int CSequence::getStart()
{
    return m_s32Start;
}
/*!
 * \brief CSequence::getCount
 * \return
 * total frame count in memory
 * if 0, no valid frame in memory
 */
int CSequence::getCount()
{
    return m_s32Count;
}
/*!
 * \brief CSequence::getCurrent
 * \return
 * current frame view
 */
int CSequence::getCurrent()
{
    return m_s32Current;
}
