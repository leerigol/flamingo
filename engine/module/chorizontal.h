#ifndef CHORIZONTAL_H
#define CHORIZONTAL_H

#include "chorizontalview.h"

/*!
 * \brief The CHorizontal class
 * composed of 2 views:
 * -main view
 * -zoom view
 */
class CHorizontal
{
public:
    CHorizontal();

protected:
    HorizontalViewmode m_eViewMode;

protected:
    CHorizontalView m_mainView;
    CHorizontalView m_zoomView;

public:
    virtual DsoErr setViewMode( HorizontalViewmode mode );
    HorizontalViewmode getViewMode();

    CHorizontalView &getMainView();
    CHorizontalView &getZoomView();
};

#endif // CHORIZONTAL_H
