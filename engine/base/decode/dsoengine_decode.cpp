#include "../dsoengine.h"

DsoErr dsoEngine::setDecodeThre( EnginePara &para )
{
    LOG_DBG();
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int h = para[1]->s32Val;
    int l = para[2]->s32Val;

    l = toAdc( ch, l );
    h = toAdc( ch, h );

    return m_pViewSession->m_decode.setThreshold( ch, h, l );
}

DsoErr dsoEngine::setDecodeThreL( EnginePara &para )
{
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int h = para[1]->s32Val;
    int l = para[2]->s32Val;

    l = toAdc( ch, l );
    h = toAdc( ch, h );

    return m_pViewSession->m_decode.setThresholdL( ch, h, l );
}
