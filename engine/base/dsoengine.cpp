
#include "dsoengine.h"

//! \todo the engine version string
const char *dsoEngine::m_version = "0.1.0";
RTimer * dsoEngine::m_playoutTimer=NULL;
RTimer * dsoEngine::m_postPlayTimer=NULL;
RTimer * dsoEngine::m_playTraceTimer=NULL;
bool   dsoEngine::m_btraceRunning = false;

CDsoSession *dsoEngine::m_pViewSession;
CDsoSession *dsoEngine::m_pHistSession;

CDsoSession dsoEngine::mViewSession;
CDsoSession dsoEngine::mHistSession;

dso_phy::CDsoPhy *dsoEngine::m_pPhy = NULL;

//! engines
dsoEngine *dsoEngine::m_pRecEngine = NULL;
dsoEngine *dsoEngine::m_pPlayEngine = NULL;
dsoEngine *dsoEngine::m_pAutoStopEngine = NULL;

EngineMsgAttr dsoEngine::_noPlay ={ false };

RecAction dsoEngine::m_RecAction = Rec_stop;

bool dsoEngine::m_bTraceRequestOnce = false;
bool dsoEngine::m_bTraceRequest = true;

CSysConfigId dsoEngine::m_ConfigId;
ControlStatus dsoEngine::m_nLastStatus = Control_Stoped;

RTimer::RTimer()
{
    mbRunning = false;
    mTimeout = 0;
}
void RTimer::start( int tmo )
{
    mTimeout = tmo;
    mbRunning = tmo > 0;
}
void RTimer::stop()
{
    mbRunning = false;
}
bool RTimer::isActive()
{
    return mbRunning;
}
//! true -- timeout
bool RTimer::tick( int nms )
{
    if ( mbRunning )
    {
        if ( mTimeout > nms )
        {
            mTimeout -= nms;
            return false;
        }
        else
        {
            mTimeout = 0;
            mbRunning = false;
//            qDebug()<<__FUNCTION__<<__LINE__<<"************"<<getDebugTime();
            return true;
        }
    }
    else
    {
        return false;
    }
}

void dsoEngine::onTimeout( int /*id */ )
{
//    EnginePara para( (int)0 );

//    engine::m_pActEngine->receiveServiceMsg(
//                packServiceMsg( ENGINE_TMO,
//                                E_SERVICE_ID_SYSTEM,
//                                E_PARA_S32 ),
//                para);
//    LOG_ENGINE();
}


dsoEngine::dsoEngine()
{
    //! create and connect once
    if ( dsoEngine::m_playoutTimer == NULL )
    { dsoEngine::m_playoutTimer = new RTimer(); }
    if ( dsoEngine::m_postPlayTimer == NULL )
    { dsoEngine::m_postPlayTimer = new RTimer(); }
    if ( dsoEngine::m_playTraceTimer == NULL)
    { dsoEngine::m_playTraceTimer = new RTimer(); }
    Q_ASSERT( NULL != dsoEngine::m_playoutTimer );
    Q_ASSERT( NULL != dsoEngine::m_postPlayTimer );
    Q_ASSERT( NULL != dsoEngine::m_playTraceTimer);

    mName="dsoEngine";

    //! register the meta type
    qRegisterMetaType<EngineHoriInfo>("EngineHoriInfo");
    qRegisterMetaType<EngineHoriReq>("EngineHoriReq");

    qRegisterMetaType<EngineHoriInfo>("EngineHoriInfo&");
    qRegisterMetaType<EngineHoriReq>("EngineHoriReq&");

    qRegisterMetaType<EngineHoriInfo>("EngineHoriInfo*");
    qRegisterMetaType<EngineHoriReq>("EngineHoriReq*");
}

void dsoEngine::timerTick( int nms )
{
#if 0
    static int temp = 0;
    temp ++;
    if( dsoEngine::m_postPlayTimer->isActive() )
    {
        qDebug()<<nms<<"*************************"<<getDebugTime();
    }

#endif

    if ( dsoEngine::m_playoutTimer->tick( nms ) )
    {
        EnginePara para( (int)PLAY_TIMEOUT_ID );
        EngineMsg msg;

        dsoEngine::m_playoutTimer->stop();

        msg = packServiceMsg( ENGINE_TMO,
                              E_SERVICE_ID_SYSTEM,
                              E_PARA_S32 );

        engine::receiveMsg( msg, para );
    }

    if ( dsoEngine::m_postPlayTimer->tick( nms ) )
    {
        EnginePara para( (int)POST_PLAY_ID );
        EngineMsg msg;

        dsoEngine::m_postPlayTimer->stop();

        msg = packServiceMsg( ENGINE_TMO,
                              E_SERVICE_ID_SYSTEM,
                              E_PARA_S32 );

        engine::receiveMsg( msg, para );
    }

    if ( dsoEngine::m_playTraceTimer->tick( nms ))
    {
        EnginePara para( (int)PLAY_TRACE_ID );
        EngineMsg msg;

        dsoEngine::m_playTraceTimer->stop();

        msg = packServiceMsg( ENGINE_TMO,
                              E_SERVICE_ID_SYSTEM,
                              E_PARA_S32 );
        engine::receiveMsg( msg, para );
    }
}

const char * dsoEngine::getVersion()
{
    return dsoEngine::m_version;
}

void dsoEngine::init()
{
    dsoEngine::m_pViewSession = &dsoEngine::mViewSession;
    dsoEngine::m_pHistSession = &dsoEngine::mHistSession;
}

CDsoSession *dsoEngine::getViewSesion()
{
    return dsoEngine::m_pViewSession;
}

CDsoSession *dsoEngine::getHistSession()
{
    return dsoEngine::m_pHistSession;
}

DsoErr dsoEngine::setPhy( dso_phy::CDsoPhy *pPhy )
{
    Q_ASSERT( NULL != pPhy );

    dsoEngine::m_pPhy = pPhy;

    return ERR_NONE;
}

dso_phy::CDsoPhy *dsoEngine::getPhy()
{
    return dsoEngine::m_pPhy;
}

DsoErr dsoEngine::setRecEngine( dsoEngine *pEngine )
{
    Q_ASSERT( NULL != pEngine );
    m_pRecEngine = pEngine;

    return ERR_NONE;
}
dsoEngine *dsoEngine::getRecEngine()
{ return m_pRecEngine; }

DsoErr dsoEngine::setPlayEngine( dsoEngine *pEngine )
{
    Q_ASSERT( NULL != pEngine );

    m_pPlayEngine = pEngine;
    return ERR_NONE;
}
dsoEngine *dsoEngine::getPlayEngine()
{
    return m_pPlayEngine;
}

DsoErr dsoEngine::setAutoStopEngine( dsoEngine *pEngine )
{
    Q_ASSERT( NULL != pEngine );

    m_pAutoStopEngine = pEngine;
    return ERR_NONE;
}
dsoEngine *dsoEngine::getAutoStopEngine()
{

    return m_pAutoStopEngine;
}

void dsoEngine::preDoProc( int /*msg*/, ServiceId /*id*/ )
{
}
void dsoEngine::postDoProc( int /*msg*/, ServiceId /*id*/ )
{

}

void dsoEngine::flushWCache()
{
    Q_ASSERT( NULL != m_pPhy );

    m_pPhy->flushWCache();
}

DsoErr dsoEngine::querySession( EngineSession *pSes )
{
    Q_ASSERT( pSes != NULL );

    pSes->m_pRecSession = m_pHistSession;
    pSes->m_pViewSession = m_pViewSession;

    return ERR_NONE;
}

DsoErr dsoEngine::enterCal(bool b)
{
    if( m_pViewSession != NULL )
    {
        m_pViewSession->m_bCalibrating = b;
    }

    return ERR_NONE;
}

void CSysConfigId::addSysConfigId()
{
    m_SysConfigId = (m_SysConfigId + 2)%4096;
}

void CSysConfigId::changeSysConfigId_run()
{
    Q_ASSERT(m_SysConfigId % 2 == 0);
    m_SysConfigId = (m_SysConfigId + 1)%4096;
}

void CSysConfigId::changeSysConfigId_stop()
{
    Q_ASSERT(m_SysConfigId % 2 == 1);
    m_SysConfigId = (m_SysConfigId + 1)%4096;
}

int CSysConfigId::getConfigId()
{
    return m_SysConfigId;
}
