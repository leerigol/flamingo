//#define _DEBUG 1
#include "../dsoengine.h"
#include "./enginecfg_search.h"

#define searchObj         m_pViewSession->m_search

DsoErr dsoEngine::searchApply(CDsoSession *pSesView)
{
    Q_ASSERT(m_pViewSession != NULL);
    if ( pSesView->m_search.getEnable() <= 0 )
    {
        return ERR_NONE;
    }

    Q_ASSERT(pSesView != NULL);
    dso_phy::CFlowView  &flowView = pSesView->m_acquire.mMainCHView;
    CSearch             &search   = pSesView->m_search;

    qint32 offset, length;
    {//! chan
        //!搜索范围，暂定屏幕范围，所以0-TransferLen;
        offset = 0;
        length = flowView.mTransferLen;

        LOG_DBG()<<"CH:"<<offset << length;
        LOG_DBG()<<"Transfer:"
                <<flowView.mTransferLen
               <<flowView.mTransferDotTime
              <<flowView.mTransferT0;

        //! set length
        m_pPhy->mSpu.setSEARCH_OFFSET_CH( offset );
        m_pPhy->mSpu.setSEARCH_LEN_CH( length );
    }


    {//! la
        //!搜索范围，暂定屏幕范围，所以0-TransferLen;
        offset = 0;
        length = pSesView->m_acquire.mMainLAView.mTransferLen;
        LOG_DBG()<<"LA:"<<offset << length;
        //! set length
        m_pPhy->mSpu.setSEARCH_OFFSET_LA( offset );
        m_pPhy->mSpu.setSEARCH_LEN_LA( length );
    }

    /*! [dba: 2018-05-02] bug: 2016*/
    //!复位搜索列计数。
    m_pPhy->mSearch.outsearch_result_send_rst(1);
    m_pPhy->mSearch.outsearch_result_send_rst(0);

    //! 水平参数
    qlonglong t0   = 0;
    qlonglong tInc   = 1;

    tInc = flowView.mTransferDotTime;
    t0   = flowView.mTransferT0;
    search.setMemAttr(t0, tInc);
    LOG_DBG()<<"mem:"<<"t0:"<<t0<<"tInc:"<<tInc;

    tInc   = getHoriAttr(chan1, horizontal_view_main).tInc;
    t0     = (-getHoriAttr(chan1, horizontal_view_main).gnd*tInc);
    search.setViewAttr(t0, tInc);
    LOG_DBG()<<"view:"<<"t0:"<<t0<<"tInc:"<<tInc;

    //!ColumnResolution
    m_pPhy->mSearch.setsearch_dat_compress_num_search_dat_compress_num( search.getColumnResolution() );
    LOG_DBG()<<"columnResolution:"<< search.getColumnResolution();

    //!view yt: start --- end
    qlonglong viewLength = flowView.mTransferLen / search.getColumnResolution();
    qlonglong viewStart  = (flowView.mTransferT0 - search.getViewT0()) / search.getViewTInc();
    search.setViewRange((int)viewStart, (int)viewLength);
    LOG_DBG()<<"viewStart:"<<viewStart<<"viewLength:"<<viewLength;

    //!Control status
    search.setControl(pSesView->m_control.getStatus());

    //!tick
    qlonglong dotTime = flowView.mTransferDotTime;
    int       chCount = pSesView->m_acquire.m_chCnt;
    m_pPhy->mSearch.set_c_tick( dotTime/chCount );
    m_pPhy->mSearch.set_td_tick( dotTime );

    //!引擎参数发生变化，一些搜索参数与之相关需要重新计算配置。
    applySearchCfg();

    return ERR_NONE;
}

IPhySearch *dsoEngine::getSearch()
{
    return &m_pPhy->mSearch;
}

bool dsoEngine::inSearchData(EngineSearchData *pCfg)
{
    //pCfg->outSearchData(&(m_pPhy->mSearch));
    pCfg->inSearchData(&(m_pPhy->mSearch));
    return true;
}

DsoErr dsoEngine::reqSearchData(EngineSearchData *pCfg)
{
    Q_ASSERT( NULL != pCfg );
    //! tInfo -- trace info
    qlonglong t0   = 0;
    qlonglong tInc = 1;
    t0   = m_pViewSession->m_search.getMemT0();
    tInc = m_pViewSession->m_search.getMemTInc();
    pCfg->setMemAttr(t0, tInc);
    //qDebug()<<__FILE__<<__LINE__<<"--mem--"<<"t0:"<<t0<<"tInc:"<<tInc;

    t0   = m_pViewSession->m_search.getViewT0();
    tInc = m_pViewSession->m_search.getViewTInc();
    pCfg->setViewAttr(t0, tInc);
    //qDebug()<<__FILE__<<__LINE__<<"--view--"<<"t0:"<<t0<<"tInc:"<<tInc;

    //!stop后按照YT模式处理
    pCfg->setStop( m_pViewSession->m_search.getControl() == Control_Stoped );

    AcquireTimemode timeMode = pCfg->getTimeMode();

    if( Acquire_YT == timeMode || (Acquire_SCAN == timeMode) || pCfg->isStop() )
    {
        m_pPhy->mZynqFcuRd.insearch_result_wdone();
        bool done = m_pPhy->mZynqFcuRd.getsearch_result_wdone_search_result_wdone();

        if(done || pCfg->getDone())
        {
            m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(1);
            m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(0);
            m_pPhy->mCcu.outTRACE_search_once_req(1);
        }

        pCfg->setDone(done);

        int start = m_pViewSession->m_search.getViewStart();
        int end   = start + m_pViewSession->m_search.getViewLength();
        pCfg->setDataRange(start, end);
    }
    else if( Acquire_ROLL == timeMode )
    {
        m_pPhy->mWpu.inroll_lenth();
        m_pPhy->mZynqFcuRd.insearch_result_wdone();

        qint32 lenth = m_pPhy->mWpu.getroll_lenth_roll_lenth_cnt();
        bool   full  = m_pPhy->mWpu.getroll_lenth_roll_full();
        qint32 end   = m_pPhy->mZynqFcuRd.getsearch_result_wdone_search_result_current_col();

        lenth = qMin(1000, lenth);

        pCfg->setRollFull(full);
#if 0
        pCfg->setDataRange(end-lenth, end);
#else
        pCfg->setDataRange(lenth, lenth);
#endif
        //!理论上wpu与zynq时同步的，qAbs(end - lenth)<10 作为done条件。
        int nLenth = qMin( qAbs(end - lenth), 1000-qAbs(end- lenth));
        bool done = (nLenth < 20)?true:false;
        pCfg->setDone(done);

        if(!done)
        {
            //qWarning()<<__FILE__<<__LINE__<<"wpu:"<<lenth<<"zynq:"<<end<<"diff:"<<nLenth;
        }
    }
    else
    {
        qWarning()<<__FILE__<<__FUNCTION__<<__LINE__<<"timeMode:"<<timeMode;
        pCfg->setDone(false);
        //Q_ASSERT(false);
    }

    return ERR_NONE;
}

DsoErr dsoEngine::getSearchData(EngineSearchData *pCfg)
{
    Q_ASSERT( NULL != pCfg );
#if 0
    //!read mem data
    //!产生个随机数
    int const count = 1000;
    pCfg->clear();
    qsrand( QTime(0,0,0).msecsTo(QTime::currentTime()) );

    for(int i = 0; i<count; i++)
    {
        pCfg->appendData( qrand()%25000 );
    }
#endif
    inSearchData(pCfg);
    return ERR_NONE;
}

DsoErr dsoEngine::clearSearchData(EngineSearchData *pCfg)
{
    m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(1);
    m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(0);
    pCfg->setDone(false);
    //pCfg->clearSearchData(&(m_pPhy->mSearch));
    return ERR_NONE;
}

DsoErr dsoEngine::setSearchEn( quint32 bm )
{
    m_pPhy->mCcu.setMODE_search_en_zoom( get_bit(bm,0) );
    m_pPhy->mCcu.setMODE_search_en_ch( get_bit(bm,1) );
    m_pPhy->mCcu.setMODE_search_en_la( get_bit(bm,2) );

    m_pViewSession->m_search.setEnable( bm );

    //!清除搜索的done
    m_pPhy->mZynqFcuWr.setsearch_result_wdone_clr(1);
    m_pPhy->mZynqFcuWr.setsearch_result_wdone_clr(0);

    return ERR_NONE;
}

DsoErr dsoEngine::applySearchCfg()
{
    DsoErr err = ERR_NONE;
    int searchType = 0;
    serviceExecutor::query( serv_name_search,
                            MSG_SEARCH_TYPE,
                            searchType);

    switch (searchType) {
    case SEARCH_TYPE_PULSE:
        err = setSearchPulseCfg( &(searchObj.mPulseCfg) );
        break;
    case SEARCH_TYPE_SLOPE:
        err = setSearchSlopeCfg(&(searchObj.mSlopeCfg));
        break;
    case SEARCH_TYPE_RS232:
        err = setSearchUartCfg(&(searchObj.mRS232Cfg));
        break;
    case SEARCH_TYPE_SPI:
        err = setSearchSpiCfg(&(searchObj.mSpiCfg));
        break;
    default:
        break;
    }

    return err;
}

DsoErr dsoEngine::setSearchLevelA(EnginePara &para)
{
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int l = para[1]->s32Val;
    int h = para[2]->s32Val;

    l = toAdc( ch, l );
    h = toAdc( ch, h );

    IPhyTpu *pTpu = getSearch();
    Q_ASSERT( ch >= chan1 && ch <= chan4 );
    pTpu->setLevelA( (IPhyTpu::eChSeq)(ch - chan1 + IPhyTpu::c_a1), l, h );
    return ERR_NONE;
}

DsoErr dsoEngine::setSearchLevelB(EnginePara &para)
{
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int l = para[1]->s32Val;
    int h = para[2]->s32Val;

    l = toAdc( ch, l );
    h = toAdc( ch, h );

    IPhyTpu *pTpu = getSearch();
    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    pTpu->setLevelB( (IPhyTpu::eChSeq)(ch - chan1 + IPhyTpu::c_a1), l, h);

    return ERR_NONE;;
}

/*! -------------------------------set cfg-----------------------------------*/
DsoErr dsoEngine::setSearchEdgeCfg( TrigEdgeCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mEdgeCfg = *pCfg;

    applyTrigEdge( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchPulseCfg( TrigPulseCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mPulseCfg = *pCfg;

    applyTrigPulse( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchSlopeCfg( TrigSlopeCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mSlopeCfg = *pCfg;

    applyTrigSlopeCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchRuntCfg( TrigRuntCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mRuntCfg = *pCfg;

    applyTrigRunt( *pCfg, getSearch() );

    return ERR_NONE;
}

DsoErr dsoEngine::setSearchOverCfg( TrigOverCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mOverCfg = *pCfg;

    applyTrigOverCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchWindowCfg( TrigWindowCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mWindowCfg = *pCfg;

    applyTrigWindowCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchTimeoutCfg( TrigTimeoutCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mTimeoutCfg = *pCfg;

    applyTrigTimeoutCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchSHCfg( TrigSHCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mSHCfg = *pCfg;

    applyTrigSH( *pCfg, getSearch() );

    return ERR_NONE;
}

DsoErr dsoEngine::setSearchDelayCfg( TrigDelayCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mDelayCfg = *pCfg;

    applyTrigDelay( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchNEdgeCfg( TrigNEdgeCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mNEdgeCfg = *pCfg;

    applyTrigNEdgeCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchPatternCfg( TrigPatternCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mPatternCfg = *pCfg;

    applyTrigPatternCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchVideoCfg( TrigVideoCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mVideoCfg = *pCfg;

    applyTrigVideoCfg( *pCfg, getSearch() );

    return ERR_NONE;
}

DsoErr dsoEngine::setSearchDurationCfg( TrigDurationCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mDurationCfg = *pCfg;

    applyTrigDurationCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchUartCfg( TrigRS232Cfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mRS232Cfg = *pCfg;

    applyTrigUartCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchIicCfg(TrigIICcfg *pCfg)
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mIIcCfg = *pCfg;

    applyTrigIicCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchSpiCfg(TrigSpiCfg *pCfg)
{
    Q_ASSERT( NULL != pCfg );


    searchObj.mSpiCfg = *pCfg;

    applyTrigSpiCfg( *pCfg, getSearch() );

    return ERR_NONE;
}

DsoErr dsoEngine::setSearchABCfg(  TrigABCfg *pCfg   )
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mABCfg = *pCfg;

    applyTrigABCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchCanCfg(TrigCanCfg *pCfg)
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mCanCfg = *pCfg;

    applyTrigCanCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchFlexrayCfg(TrigFlexrayCfg *pCfg)
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mFlexrayCfg = *pCfg;

    applyTrigFlexrayCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchStd1553bCfg(TrigStd1553bCfg *pCfg)
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mStd1553bCfg = *pCfg;

    applyTrigStd1553bCfg( *pCfg, getSearch() );

    return ERR_NONE;
}

DsoErr dsoEngine::setSearchLinCfg(TrigLinCfg *pCfg)
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mLinCfg = *pCfg;

    applyTrigLinCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
DsoErr dsoEngine::setSearchI2SCfg(TrigI2SCfg *pCfg)
{
    Q_ASSERT( NULL != pCfg );

    searchObj.mIIsCfg = *pCfg;

    applyTrigI2SCfg( *pCfg, getSearch() );

    return ERR_NONE;
}
