#include "../dsoengine.h"
#include "enginecfg_search.h"
#define _DEBUG 1
EngineSearchData::EngineSearchData()
{
    mOriginCount       = 0;

    mViewCount         = 0;
    mZoomViewCount     = 0;
    mMemT0             = 0;
    mMemTInc           = 1;
    mViewT0            = 0;
    mViewTInc          = 1;
    mZoomViewT0        = 0;
    mZoomViewTInc      = 1;
    mColumnStart       = 0;
    mColumnEnd         = 1000;
    mColumnResolution  = 1   ;
    mZoom              = false;
    mDone              = false;
    mRollFull          = false;
    mStop              = false;
    mTotalCount        = 0;


    pInDataBufA  = NULL;
    pInDataBufB  = NULL;

    a_originData  = NULL;

    a_view_mark  = NULL;
    a_view_data  = NULL;
    a_view_count = NULL;
    a_view_time  = NULL;

    pInDataBufA  =  new qint32[SEARCH_MEM_COUNT_MAX]();
    Q_ASSERT(pInDataBufA != NULL);

    pInDataBufB  =  new qint32[SEARCH_MEM_COUNT_MAX]();
    Q_ASSERT(pInDataBufB != NULL);

    a_originData  = new originDataItem[SEARCH_ORIGIN_COUNT_MAX]();;
    Q_ASSERT(a_originData != NULL);


    a_view_mark  = new int[SEARCH_MAIN_WIEW_MAX]();
    Q_ASSERT(a_view_mark != NULL);

    a_view_data  = new qlonglong[SEARCH_MAIN_WIEW_MAX]();
    Q_ASSERT(a_view_data != NULL);

    a_view_count = new int[SEARCH_MAIN_WIEW_MAX]();
    Q_ASSERT(a_view_count != NULL);

    a_view_time  = new QString[SEARCH_MAIN_WIEW_MAX]();
    Q_ASSERT(a_view_time != NULL);

    a_zoomMark  = new int[SEARCH_ZOOM_WIEW_MAX]();
    Q_ASSERT(a_zoomMark != NULL);

#if _SEARCH_DEBUG_ON
    mDebugCount  = 0;
    a_debugData  = NULL;
    a_debugData  = new SearchDataItem[SEARCH_DEBUG_COUNT_MAX]();;
    Q_ASSERT(a_debugData != NULL);
#endif
}

EngineSearchData::~EngineSearchData()
{
    delete []pInDataBufA;
    pInDataBufA = NULL;

    delete []pInDataBufB;
    pInDataBufB = NULL;

    delete []a_originData;
    a_originData = NULL;


    delete []a_view_mark;
    a_view_mark = NULL;

    delete []a_view_data;
    a_view_data = NULL;

    delete []a_view_count;
    a_view_count = NULL;

    delete []a_view_time;
    a_view_time = NULL;

    delete []a_zoomMark;
    a_zoomMark = NULL;

#if _SEARCH_DEBUG_ON
    delete []a_debugData;
    a_debugData = NULL;
#endif

}

void EngineSearchData::setLock()
{
    mMutex.lock();
}

void EngineSearchData::setUnLock()
{
    mMutex.unlock();
}
bool EngineSearchData::inSearchData(const qint32 *pBuf,
                                    int columnStart, int columnEnd,
                                    int &columnCount, int columnMax, int /*rowMax*/)
{
    SearchDataItem *pDataItem = NULL;

    if(columnStart<columnEnd)
    {
        for(int i = columnStart; i < columnEnd; i++ )
        {
            pDataItem = (SearchDataItem*)( pBuf + (sizeof(SearchDataItem)/sizeof(qint32)*i) );
            Q_ASSERT(pDataItem != NULL);

#if _SEARCH_DEBUG_ON
            appendDebugData(i, pDataItem);
#endif
            //!逻辑有bug，第一列第一个数据有可能错，暂时规避， [dba; bug:613]
            if( (i == 0) && (pDataItem->count == 1) )
            {
                columnCount++;
                continue;
            }

            if(pDataItem->count != 0)
            {
                //qDebug()<<__FILE__<<__LINE__<<columnCount<<i<<pDataItem->data[0];
            }

            appendData(columnCount, pDataItem);
            columnCount++;
        }
    }
    else
    {
        for(int i = columnStart; i < columnMax; i++)
        {
            pDataItem = (SearchDataItem*)( pBuf + (sizeof(SearchDataItem)/sizeof(qint32)*i) );
            Q_ASSERT(pDataItem != NULL);
#if _SEARCH_DEBUG_ON
            appendDebugData(i, pDataItem);
#endif
            //qDebug()<<__FILE__<<__LINE__<<columnCount<<columnStart<<columnEnd;
            //!逻辑有bug，第一列第一个数据有可能错，暂时规避， [dba; bug:613]
            if( (i == 0) && (pDataItem->count == 1) )
            {
                columnCount++;
                continue;
            }

            appendData(columnCount, pDataItem);
            columnCount++;
        }

        for(int i = 0; i < columnEnd; i++)
        {
            pDataItem = (SearchDataItem*)( pBuf + (sizeof(SearchDataItem)/sizeof(qint32)*i) );
            Q_ASSERT(pDataItem != NULL);
#if _SEARCH_DEBUG_ON
            appendDebugData(i, pDataItem);
#endif
            //qDebug()<<__FILE__<<__LINE__<<columnCount<<columnStart<<columnEnd;

            //!逻辑有bug，第一列第一个数据有可能错，暂时规避， [dba; bug:613]
            if( (i == 0) && (pDataItem->count == 1) )
            {
                columnCount++;
                continue;
            }

            appendData(columnCount, pDataItem);
            columnCount++;
        }
    }

    return 0;
}

int EngineSearchData::appendData(int n, EngineSearchData::SearchDataItem *p)
{
    Q_ASSERT(n < SEARCH_MAIN_WIEW_MAX);
    Q_ASSERT(p != NULL);

    //! 计算屏幕像素位置，时间值
    if(p->count > 0)
    {
        //!view
        mTotalCount += p->count;
        a_view_count[mViewCount] = p->count;
        a_view_mark[mViewCount]  = n;
        a_view_data[mViewCount]  = (mColumnResolution * mOriginCount + p->data[0]) * mMemTInc + mMemT0;
        mViewCount++;
        Q_ASSERT(mViewCount<=SEARCH_MAIN_WIEW_MAX);

        //!zoom
        if(mZoom && (mZoomViewCount < SEARCH_ZOOM_WIEW_MAX))
        {
            int zoomStart = (mZoomViewT0-mViewT0)/mViewTInc;
            int zoomEnd   = (mZoomViewT0 + (mZoomViewTInc * 1000) - mViewT0)/mViewTInc;
            if((zoomStart<=n) && (n<=zoomEnd))
            {
                qlonglong time = 0;
                int       mark = 0;
                for(int i = 0;
                    (i < p->count) && (mZoomViewCount < SEARCH_ZOOM_WIEW_MAX);
                    i++, mZoomViewCount++)
                {
                    time = (mColumnResolution * mOriginCount + p->data[i]) * mMemTInc + mMemT0;
                    mark = (time - mZoomViewT0)/mZoomViewTInc;
                    a_zoomMark[mZoomViewCount] = mark;
                }
            }
        }
        Q_ASSERT(mZoomViewCount<=SEARCH_ZOOM_WIEW_MAX);
    }

    //!缓存保存原始数据， 导出时用。
    (a_originData+mOriginCount)->column     = n;
    memcpy( &((a_originData+mOriginCount)->data), p, sizeof(SearchDataItem) );
    mOriginCount++;
    Q_ASSERT(mOriginCount<=SEARCH_ORIGIN_COUNT_MAX);

    return 0;
}

#if _SEARCH_DEBUG_ON
int EngineSearchData::appendDebugData(int n, EngineSearchData::SearchDataItem *p)
{
    Q_ASSERT(p != NULL);
    memcpy( (a_debugData+mDebugCount), p, sizeof(SearchDataItem) );

    a_debugData[mDebugCount].invalid[0] = n;
    mDebugCount++;
    Q_ASSERT(mDebugCount <= SEARCH_DEBUG_COUNT_MAX);
    return 0;
}
#endif

int EngineSearchData::setMemAttr(qlonglong t0, qlonglong tInc)
{
    Q_ASSERT( mMemTInc != 0);
    mMemT0 = t0;
    mMemTInc = tInc;
    mColumnResolution = mViewTInc/mMemTInc;
    //qDebug()<<__FILE__<<__FUNCTION__<<__LINE__<<mMemT0<<mMemTInc<<mColumnResolution;
    return 0;
}

int EngineSearchData::setViewAttr(qlonglong t0, qlonglong tInc)
{
    Q_ASSERT( mMemTInc != 0);
    mViewT0 = t0;
    mViewTInc = tInc;
    mColumnResolution = mViewTInc/mMemTInc;
    //qDebug()<<__FILE__<<__FUNCTION__<<__LINE__<<mViewT0<<mViewTInc<<mColumnResolution;
    return 0;
}

int EngineSearchData::setZoomViewAttr(qlonglong t0, qlonglong tInc)
{
    mZoomViewT0 = t0;
    mZoomViewTInc = tInc;
    //qDebug()<<__FILE__<<__FUNCTION__<<__LINE__<<mZoomViewT0<<mZoomViewTInc;
    Q_ASSERT( mZoomViewTInc != 0);
    return 0;
}

int EngineSearchData::setDataRange(int start, int stop)
{
    mColumnStart = (start>=0)? start : (1000+start);
    mColumnEnd   = qMin(stop, 1000);
    LOG_DBG()<<__LINE__<<mColumnStart<<mColumnEnd;
    return 0;
}

int EngineSearchData::setTimeMode(AcquireTimemode mode)
{
    mTimeMode = mode;
    return 0;
}

AcquireTimemode EngineSearchData::getTimeMode()
{
    return mTimeMode;
}

qlonglong EngineSearchData::getColumnResolution()
{
    return mColumnResolution;
}

int EngineSearchData::setZoomEnable(bool en)
{
    mZoom = en;
    return 0;
}

int EngineSearchData::setDone(bool done)
{
    mDone = done;
    return 0;
}

bool EngineSearchData::getDone()
{
    return mDone;
}

int EngineSearchData::setRollFull(bool full)
{
    mRollFull = full;
    return 0;
}

int EngineSearchData::setStop(bool b)
{
    mStop = b;
    return 0;
}

bool EngineSearchData::isStop()
{
    return mStop;
}

void EngineSearchData::clear()
{
    mTotalCount    = 0;
    mOriginCount   = 0;
    mViewCount     = 0;
    mZoomViewCount = 0;
    memset(a_view_count, 0, SEARCH_MAIN_WIEW_MAX*sizeof(int));
#if _SEARCH_DEBUG_ON
    mDebugCount    = 0;
#endif
}

bool EngineSearchData::clearSearchData(void *pPhy)
{
    setLock();

    Q_ASSERT(pPhy != NULL );

    IPhySearch *pSearchPhy = static_cast<IPhySearch*>(pPhy);
    Q_ASSERT(pSearchPhy != NULL );

    const qint32  assignSize = SEARCH_MEM_COUNT_MAX*sizeof(qint32);

    memset(pInDataBufA, 0, SEARCH_MEM_COUNT_MAX);
    memset(pInDataBufB, 0, SEARCH_MEM_COUNT_MAX);

    pSearchPhy->assign(mRealAddr, assignSize, assignSize, pInDataBufA);

    qDebug()<<__FILE__<<__LINE__<<"CLEAR BUF";
    setUnLock();
    return true;
}

bool EngineSearchData::outSearchData(void *pPhy)
{   
    setLock();
    Q_ASSERT(pPhy != NULL );

    IPhySearch *pSearchPhy = static_cast<IPhySearch*>(pPhy);
    Q_ASSERT(pSearchPhy != NULL );

    const qint32  assignSize = SEARCH_MEM_COUNT_MAX*sizeof(qint32);

    //!write  test  data
    //! ------------------------------------------------------------------------------------------
    SearchDataItem *pDataItem = NULL;
    memset(pInDataBufA, 0, SEARCH_MEM_COUNT_MAX);

    for(int i = 0; i < 1000; i++)
    {
        pDataItem = (SearchDataItem*)( pInDataBufA + (sizeof(SearchDataItem)/sizeof(qint32)*i) );

#if 1
        if( (i%50) != 0)
        {
            pDataItem->count = 0;
        }
        else
        {
            pDataItem->count = 1;
        }
#else
        pDataItem->count = 1000;
#endif

        qsrand( QTime(0,0,0).msecsTo(QTime::currentTime()) );
        for(int j = 0; j < pDataItem->count; j++)
        {
            pDataItem->data[j] = (mColumnResolution/2)/*qrand()% mColumnResolution*/;
        }
    }
    pSearchPhy->assign(mRealAddr, assignSize, assignSize, pInDataBufA);
    memset(pInDataBufA, 0, SEARCH_MEM_COUNT_MAX);
    //! ------------------------------------------------------------------------------------------
    setUnLock();
    return 0;
}

bool EngineSearchData::inSearchData(void* pPhy)
{
    setLock();

    clear();
    Q_ASSERT(pPhy != NULL );

    IPhySearch *pSearchPhy = static_cast<IPhySearch*>(pPhy);
    Q_ASSERT(pSearchPhy != NULL );

    //mTotalCount = pSearchPhy->insearch_event_total_num();

    const qint32  assignSize = SEARCH_MEM_COUNT_MAX*sizeof(qint32);
    const qint32  assignLen  = SEARCH_MEM_COUNT_MAX*sizeof(qint32);

    int count = 0;
    if( (mTimeMode == Acquire_YT) || (mTimeMode == Acquire_SCAN) || mStop)
    {
        count = mColumnStart;
        memset(pInDataBufA, 0, SEARCH_MEM_COUNT_MAX);
        pSearchPhy->assign(pInDataBufA, assignSize, assignLen, mRealAddr);

        //!当长度 end = start，即长度为0时，不保存数据。
        if( mColumnEnd > mColumnStart )
        {
            inSearchData(pInDataBufA, 0, mColumnEnd-mColumnStart, count);
        }
    }
    else if(mTimeMode == Acquire_ROLL)
    {
        static bool bufSwop = false;
        bufSwop = !bufSwop;

        qint32 *pBufA = (bufSwop)? pInDataBufA : pInDataBufB;
        qint32 *pBufB = (bufSwop)? pInDataBufB : pInDataBufA;

        memset(pBufA, 0, SEARCH_MEM_COUNT_MAX);
        pSearchPhy->assign(pBufA, assignSize, assignLen, mRealAddr);

#if 0   //!方案1 end = zynq, start = zynq - wpu
        if(mRollFull)
        {
            //!满一屏时 先显示旧数据，之后接着显示更新的数据
            //!read bufB   end ---> start
            count = 0;
            inSearchData(pBufB, mColumnEnd, mColumnStart, count);
        }
        else
        {
            //!不满一屏时 需要计算波形当前从第几列开始显示。
            count = mColumnEnd - mColumnStart;
            count = (count>=0)? count : (1000 + count);
            count = 1000-count;
            Q_ASSERT(count>=0);
        }

        //!read bufA   start ---> end
        if(mColumnStart != mColumnEnd)
        {
            inSearchData(pBufA, mColumnStart, mColumnEnd, count);
        }
#else //!方案2,end = wpu, start = 0或end
        if(!mRollFull) //!mRollFull判断是否是不满一屏
        {
            //!read bufB   end ---> start
            count = (SEARCH_MAIN_WIEW_MAX - 1) - mColumnEnd;
            if(mColumnEnd > 0)
            {
                inSearchData(pBufB, 0, mColumnEnd, count);
            }
        }
        else
        {   count = 0;
            inSearchData(pBufB, mColumnEnd, mColumnEnd, count);
        }
#endif
    }
    else
    {}

    setUnLock();
    return 0;
}

int EngineSearchData::updateViewData()
{
    setLock();
    Q_ASSERT(mViewCount <= SEARCH_MAIN_WIEW_MAX);

    qlonglong nTime = 0;
    QString   sTime = "";

    DsoType::DsoViewFmt view_fmt = dso_fmt_trim(fmt_float,fmt_width_6,fmt_no_trim_0);

    for(int i = 0; i < mViewCount; i++)
    {
        sTime.clear();
        nTime = a_view_data[i];

        gui_fmt::CGuiFormatter::format( sTime,
                                        (double)(nTime*hori_time_base),
                                        view_fmt, Unit_s );
        a_view_time[i] = sTime;
    }

    setUnLock();
    return 0;
}

int EngineSearchData::getOriginTimeValue(QList<qlonglong> &outlist)
{
    setLock();
    Q_ASSERT(mViewCount <= SEARCH_MAIN_WIEW_MAX);
    outlist.clear();

    qlonglong time  = 0;
    qlonglong t0    = 0;
    qint32    count = 0;
    for(int i = 0; i< mOriginCount; i++)
    {
        t0 = (mColumnResolution * a_originData[i].column)*mMemTInc + mMemT0;
        count = a_originData[i].data.count;
        for(int j = 0; j<qMin(count,1000); j++)
        {
            time =  a_originData[i].data.data[j] * mMemTInc + t0;
            outlist.append( time );
        }
    }
    setUnLock();
    return 0;
}

int EngineSearchData::getViewTimeValue(QList<qlonglong> &outlist)
{
    setLock();
    Q_ASSERT(mViewCount <= SEARCH_MAIN_WIEW_MAX);
    outlist.clear();
    for(int i = 0; i< mViewCount; i++)
    {
        outlist.append( a_view_data[i] );
    }
    setUnLock();
    return 0;
}

int EngineSearchData::getViewMarkValue(QList<int> &outlist)
{
    setLock();
    Q_ASSERT(mViewCount <= SEARCH_MAIN_WIEW_MAX);
    outlist.clear();
    for(int i = 0; i< mViewCount; i++)
    {
        outlist.append( a_view_mark[i] );
    }
    setUnLock();
    return 0;
}

int EngineSearchData::getViewTimeValue(QList<QString> &outlist)
{
    setLock();
    Q_ASSERT(mViewCount <= SEARCH_MAIN_WIEW_MAX);
    outlist.clear();
    for(int i = 0; i< mViewCount; i++)
    {
        outlist.append( a_view_time[i] );
    }
    setUnLock();
    return 0;
}

int EngineSearchData::getViewTimeCount(QList<QString> &outlist)
{
    setLock();
    Q_ASSERT(mViewCount <= SEARCH_MAIN_WIEW_MAX);
    outlist.clear();
    for(int i = 0; i< mViewCount; i++)
    {
        outlist.append( QString("(%1)").arg(a_view_count[i]) );
    }
    setUnLock();
    return 0;
}

int EngineSearchData::getZoomViewMarkValue(QList<int> &outlist)
{
    setLock();
    Q_ASSERT(mZoomViewCount <= SEARCH_ZOOM_WIEW_MAX);
    outlist.clear();
    for(int i = 0; i< mZoomViewCount; i++)
    {
        outlist.append( a_zoomMark[i] );
    }
    setUnLock();
    return 0;
}

int EngineSearchData::getTotalCount()
{
    setLock();
    int ret = mTotalCount;
    setUnLock();

    return ret;
}

#if _SEARCH_DEBUG_ON
int EngineSearchData::getDebugData(QList<QString> &outlist)
{
    setLock();
    Q_ASSERT(mDebugCount <= SEARCH_DEBUG_COUNT_MAX);
    outlist.clear();
    qint32 count  = 0;
    QString sItem = "";
    for(int i = 0; i< mDebugCount; i++)
    {
        count = a_debugData[i].count;

        sItem  = QString("%1,%2")
                .arg(a_debugData[i].invalid[0])
                .arg(count);

        for(int j = 0; j<qMin(count, 1000); j++)
        {
            sItem += QString(",%1").arg(a_debugData[i].data[j]);
        }

        outlist.append(sItem);
    }
    setUnLock();
    return 0;
}
#endif
