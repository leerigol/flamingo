#ifndef ENGINECFG_SEARCH
#define ENGINECFG_SEARCH

#include "../enginecfg.h"

#define  _SEARCH_DEBUG_ON            0
#define  SEARCH_ORIGIN_COUNT_MAX     (1024)
#define  SEARCH_MAIN_WIEW_MAX        (1000)
#define  SEARCH_ZOOM_WIEW_MAX        (1000)
#define  SEARCH_MEM_COUNT_MAX        (1024*1024)

#if _SEARCH_DEBUG_ON
#define  SEARCH_DEBUG_COUNT_MAX      (1024)
#endif

class EngineSearchData : public EngineCfg
{
public:
     EngineSearchData();
    ~EngineSearchData();

private: //!men data stuct
     union SearchDataItem
     {
         qint32 payload[1024];
         struct{
             qint32 count;
             qint32 data[1000];
             qint32 invalid[23];
         };
     };

     qint32*          pInDataBufA;
     qint32*          pInDataBufB;

     static const qint32     mRealAddr = 0x1F000000;

private:
     struct originDataItem
     {
         int            column;
         SearchDataItem data;
     };

     //!origin
     volatile  int     mOriginCount;
     originDataItem*   a_originData; //!原始数据的备份，导出功能的数据源。

private:
     QMutex    mMutex;

     //!main view
     volatile  int   mViewCount;
     int*       a_view_mark;     //! 小三角标记的位置 （main区域屏幕像素点）
     qlonglong* a_view_data;     //! 时间表显示的时间值（整形）
     int*       a_view_count;    //! 时间表中，事件个数
     QString*   a_view_time;     //! 时间表中，显示的时间值，从a_view_data中格式化出来的。

     //!zoom view
     volatile  int   mZoomViewCount;
     int*            a_zoomMark;  //!zoom标记的小三角位置 （zoom区域，屏幕像素点）
#if _SEARCH_DEBUG_ON
     //!debug
     volatile  int     mDebugCount;
     SearchDataItem*   a_debugData;
#endif

private:
     qlonglong mMemT0,       mMemTInc;       //! ps
     qlonglong mViewT0,      mViewTInc;      //! ps
     qlonglong mZoomViewT0,  mZoomViewTInc;  //! ps

     qlonglong mColumnResolution;

     int             mColumnStart;
     int             mColumnEnd;
     AcquireTimemode mTimeMode;
     bool            mZoom;
     bool            mDone;
     bool            mRollFull;
     bool            mStop;
     int             mTotalCount;
private:
     void         setLock();
     void         setUnLock();
private:
     bool         inSearchData(const qint32* pBuf, int columnStart, int columnEnd,
                               int &columnCount,
                               int columnMax = 1000,
                               int = 1000);
private:
     int          appendData(qlonglong point_0, SearchDataItem *p);
     int          appendData(int       n,       SearchDataItem *p);

#if _SEARCH_DEBUG_ON
     int          appendDebugData(int n, SearchDataItem *p);
#endif

public: //!set cfg api
     int                 setMemAttr(qlonglong t0, qlonglong tInc);
     int                 setViewAttr(qlonglong t0, qlonglong tInc);
     int                 setZoomViewAttr(qlonglong t0, qlonglong tInc);

     int                 setDataRange(int start, int stop);
     int                 setTimeMode(AcquireTimemode mode);
     AcquireTimemode     getTimeMode();
     qlonglong           getColumnResolution();
     int                 setZoomEnable(bool en);
     int                 setDone(bool done);
     bool                getDone();
     int                 setRollFull(bool full);
     int                 setStop(bool b);
     bool                isStop();

private:
     void                clear();

public:  //!engeine api
     bool                 clearSearchData(void* pPhy);
     bool                 outSearchData(void* pPhy ); //!test
     bool                 inSearchData(void* pPhy );
                      
public:  //!service api
     int                  updateViewData();

public:  //!app api
     int           getOriginTimeValue(QList<qlonglong> &outlist );

     int           getViewTimeValue(QList<qlonglong> &outlist );
     int           getViewMarkValue(QList<int>       &outlist );

     int           getViewTimeValue(QList<QString>   &outlist );
     int           getViewTimeCount(QList<QString>   &outlist );
     int           getZoomViewMarkValue(QList<int>   &outlist );
     int           getTotalCount();

#if _SEARCH_DEBUG_ON
public:  //!debug
     int           getDebugData(QList<QString> &outlist );
#endif
};


#endif // ENGINECFG_SEARCH

