
#include "../dsoengine.h"
//#include "../../baseclass/resample/reSample.h"

//#include "../../phy/hori/iphyhori.h"
//#include "../../phy/hori/chorialign.h"

#define selfHori    m_pViewSession->m_horizontal
#define selfHoriMain    m_pViewSession->m_horizontal.getMainView()
#define selfHoriZoom    m_pViewSession->m_horizontal.getZoomView()
#define selfAcquire     m_pViewSession->m_acquire

//! hori
DsoErr dsoEngine::setViewMode( HorizontalViewmode view )
{
    selfHori.setViewMode( view );

    return ERR_NONE;
}

DsoErr dsoEngine::setMainCfg( EngineHoriReq */*pHori*/ )
{
    return ERR_NONE;
}

DsoErr dsoEngine::setMainScale( qlonglong scale )
{
    return selfHoriMain.setScale( scale );
}

DsoErr dsoEngine::setMainOffset( qlonglong offset )
{
    selfHoriMain.setOffset( offset );

    return ERR_NONE;
}

DsoErr dsoEngine::setRollOffset(qlonglong rollOffset)
{
    selfHoriMain.setRollOffset( rollOffset );
    return ERR_NONE;
}

DsoErr dsoEngine::setZoomScale( qlonglong scale )
{
    selfHoriZoom.setScale( scale );
    return ERR_NONE;
}
DsoErr dsoEngine::setZoomOffset( qlonglong offset )
{
    selfHoriZoom.setOffset( offset );
    return ERR_NONE;
}

bool dsoEngine::validatePlayScale( qlonglong scale,
                                        CDsoSession *pHistSes )
{
    Q_ASSERT( NULL != pHistSes );

    //! hist invalid
    if ( pHistSes->getValid() )
    {}
    else
    { return true; }

    //! ch view
    CFlowView viewCH;
    CFlowView coarseViewCH;

    qlonglong offset = 0;

    viewCH.setAcq( pHistSes->m_acquire.getAcquireMode(),
                   pHistSes->m_acquire.m_chCnt,
                   pHistSes->m_acquire.mFinePos
                   );
    qlonglong normScale = ll_roof125( scale );

    if(normScale == scale)
    {
        viewCH.viewOn(   pHistSes->m_acquire.mCHFlow,
                         scale,
                         offset );
    }
    else
    {
        coarseViewCH.viewOn(   pHistSes->m_acquire.mCHFlow,
                               normScale,
                               offset );

        viewCH.viewOnPlayFine( &coarseViewCH,
                               pHistSes->m_acquire.mCHFlow,
                               normScale,
                               scale,
                               offset );
    }

    //! max intx
    if ( viewCH.mIntx > m_pPhy->m_hori.getMaxInterp() )
    { return false; }
    else
    { return true; }
}

//! by record
DsoErr dsoEngine::queryHoriInfo( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    //! view by config
    CFlowView chFlow, laFlow;
    CHoriCfg *pCfg;

    CHoriPage::HoriMode horiMode = toHoriMode( pInfo->mChCnt );


    pCfg = m_pPhy->m_hori.mHoriCfgMgr.view(
                                     horiMode,
                                     pInfo->mDepth,
                                     pInfo->mSaScale,
                                     0,
                                     pInfo->mScale,
                                     0,
                                     chFlow,
                                     laFlow
                                     );
    if ( pCfg == NULL )
    { return ERR_INVALID_CONFIG; }

    //! convert
    pInfo->mChIntxp = chFlow.mIntx;
    pInfo->mChSa = pCfg->mCHFlow.getSa();
    pInfo->mChDotTime = pCfg->mCHFlow.mDotTime;

    pInfo->mLaIntxp = laFlow.mIntx;
    pInfo->mLaSa = pCfg->mLaFlow.getSa();
    pInfo->mLaDotTime = pCfg->mLaFlow.mDotTime;

    pInfo->mChLength = pCfg->mCHFlow.mDepth;
    pInfo->mLaLength = pCfg->mLaFlow.mDepth;

    //! left time
    //! 1s, 100div
    pInfo->mOffMax = decideHoriMaxOffset( pInfo->mScale * 100 );
    //! -1/2 memtime
    pInfo->mOffMin = (-pCfg->mCHFlow.mDepth*pCfg->mCHFlow.mDotTime/2).ceilLonglong();

    roof125( pCfg->mCHFlow.mDotTime.ceilLonglong() / (200 / adc_hdiv_dots), pInfo->mPlayScaleMin);

    pInfo->mChViewLen = chFlow.mViewMemLen;
    pInfo->mLaViewLen = laFlow.mViewMemLen;

    return ERR_NONE;
}

//! meta info
DsoErr dsoEngine::queryMetaHoriInfo( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    //! convert
    pInfo->mChIntxp = 1;
    pInfo->mChSa = m_pHistSession->m_acquire.mMainCHView.getFlow().getSa();
    pInfo->mChDotTime = m_pHistSession->m_acquire.mMainCHView.getFlow().getDotTime();

    pInfo->mLaIntxp = 1;
    pInfo->mLaSa = m_pHistSession->m_acquire.mMainLAView.getFlow().getSa();
    pInfo->mLaDotTime = m_pHistSession->m_acquire.mMainLAView.getFlow().getDotTime();

    pInfo->mChLength = m_pHistSession->m_acquire.mMainCHView.getFlow().getDepth();
    pInfo->mLaLength = m_pHistSession->m_acquire.mMainLAView.getFlow().getDepth();

    //! left time
    //! 1s, 100div
    pInfo->mOffMax = decideHoriMaxOffset( pInfo->mScale * 100 );
    //! -5 div
    pInfo->mOffMin = (-pInfo->mScale * hori_div/2);

    pInfo->mChViewLen = pInfo->mChLength;
    pInfo->mLaViewLen = pInfo->mLaLength;

    return ERR_NONE;
}

//! main info
DsoErr dsoEngine::queryHoriInfoMain( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    return ERR_NONE;
}

//! zoom info
DsoErr dsoEngine::queryHoriInfoZoom( EngineHoriInfo *pInfo )
{
    Q_ASSERT( NULL != pInfo );

    return ERR_NONE;
}

CHoriPage::HoriMode dsoEngine::toHoriMode( int chCnt )
{
    //! 20g
    if ( sysHasArg("-20g") )
    {
        if ( chCnt <= 1 )
        {
            return CHoriPage::clk_20G;
        }
        else if ( chCnt <= 2 )
        {
//            return CHoriPage::clk_10G;
            return CHoriPage::clk_2_5G;
        }
        else
        {
//            return CHoriPage::clk_5G;
            return CHoriPage::clk_2_5G;
        }
    }
    else
    {
        if ( chCnt <= 1 )
        {
//            if( m_pViewSession->m_acquire.getSysRawBand() == BW_100M)
//            {
//                return CHoriPage::clk_5G_100M;
//            }
//            else
            {
                return CHoriPage::clk_10G;
            }
        }
        else if ( chCnt <= 2 )
        {
            return CHoriPage::clk_5G;
        }
        else
        {
            return CHoriPage::clk_2_5G;
        }
    }
}

//! memory t0 && sa scale
qlonglong dsoEngine::decideHoriMaxOffset(
                               qlonglong t0,
                               qlonglong defOff )
{
    Q_ASSERT( t0 > 0 );

    if ( t0 < defOff )
    { return defOff; }
    else
    { return t0; }
}

DsoErr dsoEngine::queryHoriAttr( EngineHoriAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );

    engine *pActEngine;

    pActEngine = dsoEngine::getActiveEngine();
    if ( pActEngine != dsoEngine::m_pRecEngine )
    {
        pAttr->mbPlay = true;
    }
    else
    {
        pAttr->mbPlay = false;
    }

    //! snap attr info
    m_pViewSession->snapHoriTag( &pAttr->mViewTag );
    m_pHistSession->snapHoriTag( &pAttr->mRecTag );

    return ERR_NONE;
}

int dsoEngine::getLiveCHCnt()
{
    quint32 bmMask;

    bmMask = 0;
    for ( int i = 0; i < 4; i++ )
    {
        if ( m_pViewSession->m_ch[i].getOnOff() )
        {
            bmMask |= (1<<i);
        }
    }

    switch( bmMask )
    {
        case 0: case 1: case 2: case 4: case 8:
            return 1;

        case 0x5: case 9: case 6: case 0x0a:
            return 2;

        case 3: case 0x0c:
        case 7: case 0x0e: case 0x0d: case 0x0b:
            return 4;
        default:
            return 4;
    }
}
