#ifndef CENGINEOBJ_H
#define CENGINEOBJ_H

#include <QDebug>
#include "../../include/dsotype.h"
#include "../../baseclass/cargument.h"
using namespace DsoType;

/*!
 * \brief The CEngineObj class
 * base engine control object
 */
class CEngineObj : public CObj
{
public:
    CEngineObj();
};

#endif // CENGINEOBJ_H
