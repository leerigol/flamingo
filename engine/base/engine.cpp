
#include <QDebug>
#include "engine.h"
#include "../../include/dsodbg.h"
const struEngineMsgMap* engine::getMsgMap()
{ return &engine::m_msgMap; }
const struEngineMsgEntry engine::m_msgEntry[]={ ENGINE_MSG_NONE,
                                                E_SERVICE_ID_NONE,
                                                E_PARA_NONE,
                                                NULL,
                                                NULL,
                                                E_SERVICE_ID_NONE,
                                                0,
                                                NULL };

const struEngineMsgMap engine::m_msgMap={ 0, engine::m_msgEntry };

engine *engine::m_pActEngine = NULL;
QMutex *engine::m_pEngineLock = NULL;
bool engine::m_bFreeze = true;          //! default freezed

void engine::lockEngine()
{
    Q_ASSERT( engine::m_pEngineLock != NULL );

    engine::m_pEngineLock->lock();
}
void engine::unlockEngine()
{
    Q_ASSERT( engine::m_pEngineLock != NULL );

    engine::m_pEngineLock->unlock();
}

void engine::freezeEngine()
{
    engine::m_bFreeze = true;
}
void engine::unFreezeEngine()
{
    engine::m_bFreeze = false;
}

DsoErr engine::setActiveEngine( engine *pEngine )
{
    Q_ASSERT( NULL != pEngine );

    engine::m_pActEngine = pEngine;
//    LOG_ON_FILE()<<pEngine->getName();

    return ERR_NONE;
}
engine* engine::getActiveEngine()
{
    return engine::m_pActEngine;
}

DsoErr engine::receiveMsg( EngineMsg msg,
                           EnginePara& para,
                           void *outPtr )
{
    Q_ASSERT( NULL != engine::m_pActEngine );
//LOG_DBG()<<unpackSMsg(msg);
    DsoErr err;
    lockEngine();
    err = engine::m_pActEngine->receiveServiceMsg( msg,
                                                   para,
                                                   outPtr );
    unlockEngine();

    return err;
}

/*!
 * \brief engine::engine
 * \param parent
 */
engine::engine(QObject *parent) : QObject(parent)
{
    if ( NULL == engine::m_pEngineLock )
    {
        engine::m_pEngineLock = new QMutex( QMutex::Recursive );
        Q_ASSERT( engine::m_pEngineLock!=NULL );
    }
}
engine::~engine()
{
    if ( NULL != engine::m_pEngineLock )
    {
        delete engine::m_pEngineLock;
        engine::m_pEngineLock = NULL;
    }
}

/*!
 * \brief engine::receiveServiceMsg
 * \param msg
 * \param para
 * \param outPtr
 * \return
 * receive msg from service
 */
DsoErr engine::receiveServiceMsg( EngineMsg msg,
                                  EnginePara& para,
                                  void *outPtr )
{
    return doDispatch( msg, para, outPtr );
}

/*!
 * \brief engine::doDispatch
 * \param msg
 * \param para
 * \param outPtr
 * \return
 * dispatch and execute command
 * use the root table
 * -msg
 * -serviceid
 * -para type
 */
DsoErr engine::doDispatch( EngineMsg packedMsg,
                           EnginePara& para,
                           void *outPtr )
{
    const struEngineMsgMap *pMap;

    pMap = getMsgMap();

    DsoErr err;
    while( pMap != NULL )
    {
        err = doMap( pMap, packedMsg, para, outPtr );
        if ( err != ERR_MIS_MATCH )
        {
            return err;
        }
        //! base map
        else
        {}

        //! for the base map
        pMap = pMap->baseMap;
    }

    return ERR_MIS_MATCH;
}

DsoErr engine::doMap( const struEngineMsgMap *pMap,
                      EngineMsg packedMsg,
                      EnginePara& para,
                      void *outPtr )
{
    Q_ASSERT( NULL != pMap );

    //! entry item
    const struEngineMsgEntry *pEntry;

    pEntry = pMap->selfEntry;

    ServiceId sId;
    EngineParaType sType;
    EngineMsg msg;

    msg = unpackSMsg( packedMsg );

    for ( int i = 0;
          pEntry != NULL && pEntry[i].msg != ENGINE_MSG_NONE;
          i++ )
    {
        //! msg match
        if ( pEntry[i].msg == msg )
        {
            sId = unpackSId( packedMsg );
            sType = unpackSType( packedMsg );

            //! single entry
            if ( pEntry[i].paraType == sType
                 && pEntry[i].id == sId )
            {
                return exec( pMap, pEntry+i, packedMsg, para, outPtr, sId );
            }
            //! don't care id
            else if ( pEntry[i].paraType == sType
                      && pEntry[i].id == E_SERVICE_ID_NONE )
            {
                return exec( pMap, pEntry+i, packedMsg, para, outPtr, sId );
            }
            //! range
            else if ( rangeTypeMatch(pEntry[i].paraType, sType)
                      && rangeContains( pEntry+i, sId) )
            {
                return exec( pMap, pEntry+i, packedMsg, para, outPtr, sId );
            }
            else
            {
                //qWarning()<<"engine match fail!"<<__FUNCTION__<<__LINE__;
                //qWarning()<<msg<<pEntry[i].paraType<<sType<<pEntry[i].id;
                return ERR_INVALID_INPUT;
            }
        }
    }

    return ERR_MIS_MATCH;
}

DsoErr engine::exec( const struEngineMsgMap */*pMap*/,
                     const struEngineMsgEntry *pEntry,
                     EngineMsg /*msg*/,
                     EnginePara& para,
                     void */*outPtr*/,
                     ServiceId sId)
{
    Q_ASSERT( NULL != pEntry );
    Q_ASSERT( NULL != pEntry->proc );

    DsoErr err;
    err = doProc( pEntry,
                   pEntry->proc,
                   pEntry->paraType,
                   para,
                   sId
                    );

    return err;
}

void engine::preDoProc( EngineMsg /*msg*/, ServiceId /*sId*/ )
{}
void engine::postDoProc( EngineMsg /*msg*/, ServiceId /*sId*/ )
{}

/*!
 * \brief engine::doProc
 * \param proc
 * \param paraType
 * \param para
 * \return
 * execute command according to the comamd para
 */
DsoErr engine::doProc( const struEngineMsgEntry *pEntry,
                       EngineProc proc,
                       EngineParaType paraType,
                       EnginePara& para,
                       ServiceId sId)
{
    Q_ASSERT( NULL != pEntry );
    m_proc.proc = proc;

    switch( paraType )
    {
        case E_PARA_BOOL:
        case E_CFG_PARA_BOOL:
            return (this->*(m_proc.onBool))( para.bVal );

        case E_PARA_S32:
        case E_CFG_PARA_S32:
            return (this->*(m_proc.onInt))( para.s32Val );

        case E_PARA_S64:
        case E_CFG_PARA_S64:
            return (this->*(m_proc.onLonglong))( para.s64Val );

        case E_PARA_U8:
            return (this->*(m_proc.onU8))(para.u8Val);

        case E_PARA_U16:
        case E_CFG_PARA_U16:
            return (this->*(m_proc.onU16))(para.u16Val);

        case E_PARA_U32:
        case E_CFG_PARA_U32:
            return (this->*(m_proc.onU32))(para.u32Val);

        case E_PARA_U64:
        case E_CFG_PARA_U64:
            return (this->*(m_proc.onU64))(para.u64Val);

        case E_PARA_REAL:
            return (this->*(m_proc.onReal))(para.realVal);

        case E_PARA_PTR:
        case E_PARA_qPTR:
            return (this->*(m_proc.onPtr))( para.ptr );

        case E_PARA_STACK:
            if ( para.size() != pEntry->paraCnt )
            {
                qWarning()<<"para cnt mismatch";
                return ERR_TARGET_INVALID_CFG;
            }
            return (this->*(m_proc.onStack))( para );

        case E_PARA_INT_PTR:
        case E_PARA_qINT_PTR:
            Q_ASSERT( para.size() > 1 );
            return (this->*(m_proc.onIntPtr))( para.s32Val, para.pNext->ptr );

        case E_PARA_INT_BOOL:
        case E_CFG_PARA_INT_BOOL:
            return (this->*(m_proc.onRangeBool))( para.s32Val, para.pNext->bVal );

        case E_PARA_INT_INT:
        case E_CFG_PARA_INT_INT:
            return (this->*(m_proc.onRangeInt))( para.s32Val, para.pNext->s32Val );            

        case E_PARA_INT_S64:
        case E_CFG_PARA_INT_S64:
            return (this->*(m_proc.onRangeLonglong))( para.s32Val, para.pNext->s64Val );

        //! range value
        case E_PARA_RANGE_BOOL:
            return (this->*(m_proc.onRangeBool))( (int)sId, para.bVal );

        case E_PARA_RANGE_S32:
            return (this->*(m_proc.onRangeInt))( (int)sId, para.s32Val );

        case E_PARA_RANGE_S64:
            return (this->*(m_proc.onRangeLonglong))( (int)sId, para.s64Val );

        case E_PARA_RANGE_PTR:
            return (this->*(m_proc.onIntPtr))( (int)sId, para.ptr );

        case E_PARA_RANGE_REAL:
            return (this->*(m_proc.onRangeReal))( (int)sId, para.realVal );

        //! no para
        case E_PARA_NONE:
        case E_CFG_PARA_NONE:
            return (this->*(m_proc.onVoid))();

        //! get
        case E_PARA_qBOOL:
            para.bVal = (this->*(m_proc.qBool))();
            return ERR_NONE;

        case E_PARA_qS32:
            para.s32Val = (this->*(m_proc.qInt))();
            return ERR_NONE;

        case E_PARA_qS64:
            para.s64Val = (this->*(m_proc.qLonglong))();
            return ERR_NONE;

        case E_PARA_qU8:
            para.u8Val = (this->*(m_proc.qU8))();
            return ERR_NONE;
        case E_PARA_qU16:
            para.u16Val = (this->*(m_proc.qU16))();
            return ERR_NONE;
        case E_PARA_qU32:
            para.u32Val = (this->*(m_proc.qU32))();
            return ERR_NONE;
        case E_PARA_qU64:
            para.u64Val = (this->*(m_proc.qU64))();
            return ERR_NONE;
        case E_PARA_qREAL:
            para.realVal = (this->*(m_proc.qReal))();
            return ERR_NONE;

        case E_PARA_qBOOL_INT:
            Q_ASSERT( para.size() >= 1 );
            para.bVal = (this->*(m_proc.qBoolInt))( para.s32Val );
            return ERR_NONE;

        case E_PARA_qS32_INT:
            Q_ASSERT( para.size() >= 1 );
            para.s32Val = (this->*(m_proc.qIntInt))( para.s32Val );
            return ERR_NONE;

        case E_PARA_qS64_INT:
            Q_ASSERT( para.size() >= 1 );
            para.s64Val = (this->*(m_proc.qLonglongInt))( para.s32Val );
            return ERR_NONE;

        case E_PARA_qU8_INT:
            Q_ASSERT( para.size() >= 1 );
            para.u8Val = (this->*(m_proc.qU8Int))( para.s32Val );
            return ERR_NONE;

        case E_PARA_qU16_INT:
            Q_ASSERT( para.size() >= 1 );
            para.u16Val = (this->*(m_proc.qU16Int))( para.s32Val );
            return ERR_NONE;

        case E_PARA_qU32_INT:
            Q_ASSERT( para.size() >= 1 );
            para.u32Val = (this->*(m_proc.qU32Int))( para.s32Val );
            return ERR_NONE;

        case E_PARA_qU64_INT:
            Q_ASSERT( para.size() > 1 );
            para.u64Val = (this->*(m_proc.qU64Int))( para.s32Val );
            return ERR_NONE;

        case E_PARA_qU32_STACK:
            if ( para.size() != pEntry->paraCnt )
            {
                qWarning()<<"para cnt mismatch";
                return ERR_TARGET_INVALID_CFG;
            }
            para.u32Val = (this->*(m_proc.qU32Stack))( para );
            return ERR_NONE;

        default:
            return ERR_INVALID_INPUT;
    }
}

DsoErr engine::onExit()
{
    return ERR_NONE;
}
DsoErr engine::onEnter()
{
    return ERR_NONE;
}
DsoErr engine::toEngine( engine *pEngine )
{
    DsoErr err;
    engine *pPre;

    //! test:trace once request
    //! freezed
//    qDebug()<<__FILE__<<__LINE__<< engine::m_bFreeze ;
    if ( engine::m_bFreeze )
    {
        return ERR_NONE;//ERR_ACTION_DISABLED;
    }
    else
    {}

    pPre = engine::m_pActEngine;

    if ( pEngine == pPre )
    {
//        qWarning()<<"same engine";
        return ERR_INVALID_INPUT;
    }

    err = setActiveEngine( pEngine );
    if ( err != ERR_NONE )
    {
        return err;
    }

    err = pPre->onExit();
    if ( err != ERR_NONE )
    {
        setActiveEngine( pPre );
        return err;
    }

    err = pEngine->onEnter();
    if ( err != ERR_NONE )
    {
        return err;
    }

    return ERR_NONE;
}

QString &engine::getName()
{ return mName; }

/*!
 * \brief engine::rangeTypeMatch
 * \param tableEntryType
 * \param praType
 * \return
 * range match?
 */
bool engine::rangeTypeMatch( EngineParaType tableEntryType,
                             EngineParaType paraType )
{
    if ( tableEntryType == E_PARA_RANGE_BOOL
         && paraType == E_PARA_BOOL )
    {
        return true;
    }
    else if ( tableEntryType == E_PARA_RANGE_S32
              && paraType == E_PARA_S32 )
    {
        return true;
    }
    else if ( tableEntryType == E_PARA_RANGE_S64
              && paraType == E_PARA_S64 )
    {
        return true;
    }
    else if ( tableEntryType == E_PARA_RANGE_PTR
              && paraType == E_PARA_PTR )
    {   return true; }
    else if ( tableEntryType == E_PARA_RANGE_REAL
              && paraType == E_PARA_REAL )
    {   return true; }
    else
    {
        return false;
    }
}

bool engine::rangeContains( const struEngineMsgEntry *pEntry,
                            ServiceId sId )
{
    Q_ASSERT( NULL != pEntry );

    if ( pEntry->id <= sId
         && pEntry->idEnd >= sId )
    {
        return true;
    }
    else
    {
        return false;
    }
}

EngineWR engine::wr( EngineMsg msg )
{
    int para = (int)( unpackSType( msg ) );

    if ( para >= E_PARA_READ )
    {
        return Engine_Read;
    }
    else if( para >= E_CFG_PARA_NONE && para <= E_CFG_PARA_BOOL)
    {
        return Engine_Config;
    }
    else
    {
        return Engine_Write;
    }
}
