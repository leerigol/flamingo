//#define _DEBUG 1

#include "../dsoengine.h"
#include "../adt/patchrequest.h"
handleMemory* dsoEngine::openMemory( Chan chan )
{
    //! new handle factory
    handleMemory *pHandle = NULL;
    qlonglong     length = 0;

    switch( chan )
    {
        case chan1:
        case chan2:
        case chan3:
        case chan4:
            pHandle = new handleMemoryCHx( chan );
            length  = m_pHistSession->m_acquire.getChLength().ceilLonglong();
            break;

        case d0: case d1: case d2: case d3:
        case d4: case d5: case d6: case d7:
        case d8: case d9: case d10: case d11:
        case d12: case d13: case d14: case d15:
            pHandle = new handleMemoryDx( chan );
            length  = m_pHistSession->m_acquire.getLaLength().ceilLonglong();
            break;

        case la:
        case d0d7:
        case d8d15:
        case d0d15:
            pHandle = new handleMemoryLa( chan );
            length  = m_pHistSession->m_acquire.getLaLength().ceilLonglong();
            break;

        case ana:
            pHandle = new handleMemory( chan );
            length  = m_pHistSession->m_acquire.getChLength().ceilLonglong();
            break;

        case ana_la:
            pHandle = new handleMemory( chan );
            length  = m_pHistSession->m_acquire.getChLength().ceilLonglong();
            break;

        default:
            //Q_ASSERT(false);
            break;
    }

    if ( NULL == pHandle )
    {
        return NULL;
    }

    //! handle config
    pHandle->setOffset( 0 );

    pHandle->setChCnt( toChCnt(m_pHistSession->getSaCHBm()) );

    //! full size
    //! \todo real size
    pHandle->setSize( length );

    pHandle->setEngine( this );
    pHandle->setValid();

    pHandle->postSet();

    LOG_DBG()<<pHandle->getChCnt()<<pHandle->getFullSize();

    return pHandle;
}

void dsoEngine::exportInit(handleMemory* pHandle)
{
    Q_ASSERT(NULL != pHandle);

    //! ccu
    m_pPhy->mCcu.waitStop();
    //qDebug()<<__FILE__<<__FUNCTION__<<__LINE__;

    m_pPhy->mCcu.setMODE_measure_en_zoom( 0 );
    m_pPhy->mCcu.setMODE_measure_en_ch( 0 );
    m_pPhy->mCcu.setMODE_measure_en_la( 0 );
    m_pPhy->mCcu.setMODE_mask_pf_en( 0 );

    m_pPhy->mCcu.setMODE_mode_fast_ref(0);
    m_pPhy->mCcu.setMODE_zoom_en(0);

    //! spu
    m_pPhy->mSpuGp.setINTERP_MULT_MAIN_en( 0 );
    m_pPhy->mSpuGp.setINTERP_MULT_ZOOM_en( 0 );

    m_pPhy->mSpuGp.setCOMPRESS_MAIN_en( 0 );
    m_pPhy->mSpuGp.setCOMPRESS_ZOOM_en( 0 );

    m_pPhy->mSpuGp.setZYNQ_COMPRESS_MAIN( 0 );
    m_pPhy->mSpuGp.setZYNQ_COMPRESS_ZOOM( 0 );


    m_pPhy->mSpuGp.setLA_COMP_MAIN_en(0);
    m_pPhy->mSpuGp.setLA_COMP_ZOOM_en(0);

    m_pPhy->mSpuGp.setCOARSE_DELAY_MAIN_delay_en(0);
    m_pPhy->mSpuGp.setCOARSE_DELAY_ZOOM_delay_en(0);

    m_pPhy->mSpuGp.setLINEAR_INTX_CTRL_mian_bypass(1);
    m_pPhy->mSpuGp.setLINEAR_INTX_CTRL_zoom_bypass(1);

    m_pPhy->mSpuGp.setLA_LINEAR_INTX_CTRL_mian_bypass(1);
    m_pPhy->mSpuGp.setLA_LINEAR_INTX_CTRL_zoom_bypass(1);

    m_pPhy->flushWCache();

    m_pPhy->mSpuGp.setTRACE_COMPRESS_ZOOM_en(0);
    m_pPhy->mSpuGp.flushWCache();


    //! ccu mod
    if (dso_phy::traceRequest::trace_ch_main == pHandle->mReqSrc)
    {
        m_pPhy->mCcu.setMODE_wave_ch_en(1);
        m_pPhy->mCcu.setMODE_wave_la_en(0);
    }
    else if (dso_phy::traceRequest::trace_la_main == pHandle->mReqSrc)
    {
        m_pPhy->mCcu.setMODE_wave_ch_en(0);
        m_pPhy->mCcu.setMODE_wave_la_en(1);
    }
    else if (dso_phy::traceRequest::trace_cmb_main == pHandle->mReqSrc)
    {
        m_pPhy->mCcu.setMODE_wave_ch_en(1);
        m_pPhy->mCcu.setMODE_wave_la_en(1);
    }
    else
    { Q_ASSERT( false ); }

    m_pPhy->flushWCache();
}

void dsoEngine::exportInit(handleMemory *pHandle, frameAttr &attr)
{
    //! init
    exportInit(pHandle);

    //! load frame
    m_pPhy->mRecPlay.setNow(attr.mFrameId);
    m_pPhy->mRecPlay.loadNow();

    //!引擎参数
    attr.saChLen = m_pHistSession->m_acquire.mCHFlow.getDepth().ceilInt();
    attr.saLaLen = m_pHistSession->m_acquire.mLaFlow.getDepth().ceilInt();
    attr.saValid = m_pHistSession->m_acquire.getSaValid();
    /*qDebug()<<__FILE__<<__LINE__
           <<"saValid"<<attr.saValid
           <<"saChLen"<<attr.saChLen
           <<"saLaLen"<<attr.saLaLen;*/
}

void dsoEngine::importInit(frameAttr &attr)
{
    //qDebug()<<"------------------------"<<__FUNCTION__<<"-------------------------------";
    CDsoPhy *pPhy = m_pPhy;
    Q_ASSERT( NULL != pPhy );
    //！ 0 stop
    pPhy->mCcu.waitStop();

    //! 1 配置录制模式
    //! 1.1. to load mode
    pPhy->mScuGp.setREC_PLAY_mode_rec( 0 );
    pPhy->mScuGp.setREC_PLAY_mode_play( 0 );
    pPhy->mScuGp.setREC_PLAY_index_inc( 0 );
    pPhy->mScuGp.setREC_PLAY_index_load( 1 );
    pPhy->mScuGp.setREC_PLAY_loop_playback(0);
    pPhy->mScuGp.setREC_PLAY_index_full(0);
    pPhy->mScuGp.flushWCache();
    //qDebug("----scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());
    pPhy->mScuGp.inREC_PLAY();
    //qDebug("----in scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());

    //! 1.2. 帧号
    pPhy->mScuGp.setINDEX_CUR_index(   attr.mFrameId    );
    pPhy->mScuGp.setINDEX_BASE_index(  attr.mFrameStart );
    pPhy->mScuGp.setINDEX_UPPER_index( attr.mFrameEnd   );
    pPhy->mScuGp.flushWCache();
//    qDebug("----in INDEX_CUR    :%X", pPhy->mScuGp.inINDEX_CUR());
//    qDebug("----in INDEX_BASE   :%X", pPhy->mScuGp.inINDEX_BASE());
//    qDebug("----in INDEX_UPPER  :%X", pPhy->mScuGp.inINDEX_UPPER());


    //! 1.3. to rec mode
    pPhy->mScuGp.setREC_PLAY_mode_rec( 1 );
    pPhy->mScuGp.setREC_PLAY_index_inc( 1 );
    pPhy->mScuGp.setREC_PLAY_index_load( 0 );
    pPhy->mScuGp.flushWCache();
    //qDebug("----scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());
    pPhy->mScuGp.inREC_PLAY();
    //qDebug("----in scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());

    //! 2 配置 段 大小 （略大于一帧数据的总大小）
    //pPhy->mSpuGp.outSEGMENT_SIZE( attr.mFrameLen );

    //！3 配置波形导入记录模式， 导入数据类型。
    pPhy->mCcu.setMODE_mode_import_rec(1);
    pPhy->mCcu.setMODE_mode_import_play(0);

    if(attr.mCh == ana)
    {
        pPhy->mCcu.setMODE_mode_import_type(0);
        //qDebug()<<"------ana-";
    }
    else if(attr.mCh == la)
    {
        pPhy->mCcu.setMODE_mode_import_type(1);
        //qDebug()<<"------la--";
    }
    else
    {Q_ASSERT(false);}

    pPhy->mCcu.flushWCache();
    //qDebug("----ccu mode:%X", pPhy->mCcu.getMODE());
    pPhy->mCcu.inMODE();
    //qDebug("----in ccu mode:%X", pPhy->mCcu.getMODE());
    //qDebug()<<"---------------------------------------------------------------------";
}

int dsoEngine::readMemory( handleMemory* handle,
                           void *pOut,
                           int from,
                           int len )
{

    Q_ASSERT( NULL != handle );
    Q_ASSERT( NULL != pOut );
    Q_ASSERT( len > 0 );

    int ret;
    //! split package
    CReadSplit readPatch;
    ret = readPatch.split(len);
    if ( ret != 0 )
    { return -1; }

    //! offset
    handle->setOffset(from);

    //! for big patchs
    for ( int i = 0; i < readPatch.mPatchCount; i++ )
    {
        ret = readMisson(  handle,
                           pOut,
                           readPatch.mPatchSize );
        if ( ret != readPatch.mPatchSize )
        { return -2; }

        pOut = (DsoPoint*)pOut + readPatch.mPatchSize * sizeof(DsoPoint);
    }


    //! for the post patch
    if ( readPatch.mPostSize > 0 )
    {
        ret = readMisson(  handle,
                           readPatch.m_pPostPatch,
                           readPatch.mPostPatchSize);

        if ( ret != readPatch.mPostPatchSize )
        { return -2; }

        memcpy( pOut,
                readPatch.m_pPostPatch,
                readPatch.mPostSize * sizeof(DsoPoint) );
    }

    return len;
}



quint8 tempBuf[patch_size*sub_patch] = {0};

int dsoEngine::writeMemory(handleMemory* handle,
                            void *pIn,
                            int from,
                            int len ,
                            bool eof)
{
    Q_ASSERT( NULL != handle );
    Q_ASSERT( NULL != pIn );
    Q_ASSERT( len  > 0 );

    int ret = 0;

    //! split package
    CWriteSplit writePatch;
    ret = writePatch.split(pIn, tempBuf, from,len);
    if ( ret != 0 )
    { return -1; }

    //!offset
    handle->setOffset(writePatch.mOffset);

    //! 第一包，包含上次遗留的数据 + 本次前部分数据 组成 mPrePadSize 大小的包
    if ( writePatch.mPrePadSize > 0 )
    {
       // qDebug()<<__FILE__<<__LINE__<<"PrePadSize: "<<writePatch.mPrePadSize;
        ret = writeMisson( handle,
                           writePatch.m_pPrePadPatch,
                           writePatch.mPrePadSize );

        if ( ret != writePatch.mPrePadSize )
        { return -2; }

        pIn = (DsoPoint*)pIn + writePatch.mPadSize;
    }

    //! for big patchs
    for ( int i = 0; i < writePatch.mPatchCount; i++ )
    {
        //qDebug()<<__FILE__<<__LINE__<<"PatchSize: "<<i<<writePatch.mPatchSize;
        ret = writeMisson( handle,
                           pIn,
                           writePatch.mPatchSize );

        if ( ret != writePatch.mPatchSize )
        { return -2; }

        pIn = (DsoPoint*)pIn + writePatch.mPatchSize;
    }


    //! 最后不足一包的 填充无效数据写一包。
    if ( eof && (writePatch.mPostSize != 0) )
    {
        //qDebug()<<__FILE__<<__LINE__<<"PostSize: "<<writePatch.mPostSize;

        ret = writeMisson( handle,
                           tempBuf,
                           writePatch.mPostSize );

        if ( ret != writePatch.mPostSize )
        { return -2; }

        //qDebug()<<QByteArray((char*)tempBuf, writePatch.mPostSize);

        pIn = (DsoPoint*)pIn + writePatch.mPostSize;
    }

    return len;
}

int dsoEngine::readMisson( handleMemory * handle,
                           void *pOut,
                           int len )
{
    Q_ASSERT( NULL != handle && NULL != pOut );

    return handle->doRead( pOut, len );
}

int dsoEngine::writeMisson(handleMemory * handle,
                           void *pIn,
                           int   len )
{
    Q_ASSERT( NULL != handle && NULL != pIn );

    int writeLen = 0;
    //! read
    writeLen = handle->doWrite( pIn, len );
    if ( writeLen != len )
    {
        qWarning()<<__FILE__<<__FUNCTION__<<__LINE__
                  <<"error len = "<< len;
    }

    return writeLen;
}

void dsoEngine::exportEnd()
{

}

void dsoEngine::importEnd()
{
    //qDebug()<<"------------------------"<<__FUNCTION__<<"-------------------------------";
    CDsoPhy *pPhy = m_pPhy;
    Q_ASSERT( NULL != pPhy );

    //！2 配置波形导入结束。
    pPhy->mCcu.setMODE_mode_import_rec(0);
    pPhy->mCcu.flushWCache();
    pPhy->mCcu.inMODE();
    //qDebug("----in ccu mode:%X", pPhy->mCcu.getMODE());
    //qDebug()<<"------------------------------------------------------------------------------";
}

void dsoEngine::importPlay(frameAttr &attr)
{
//    qDebug()<<"------------------------"<<__FUNCTION__<<"-------------------------------";
    CDsoPhy *pPhy = m_pPhy;
    Q_ASSERT( NULL != pPhy );

    //!debug
    //QThread::sleep(2);
//    qDebug("----in IMPORT_INFO      :%X", pPhy->mSpuGp.inIMPORT_INFO());
//    qDebug("----in IMPORT_CNT_CH    :%X", pPhy->mSpuGp.inIMPORT_CNT_CH());
//    qDebug("----in IMPORT_CNT_LA    :%X", pPhy->mSpuGp.inIMPORT_CNT_LA());
//    qDebug("----in WAVE_LEN_MAIN    :%X", pPhy->mSpuGp.inWAVE_LEN_MAIN());
//    qDebug("----in WAVE_OFFSET_MAIN :%X", pPhy->mSpuGp.inWAVE_OFFSET_MAIN());

    //! 0 stop
    pPhy->mCcu.waitStop();

    //！2 配置波形导入回放模式。
    pPhy->mCcu.setMODE_mode_import_rec(0);
    pPhy->mCcu.setMODE_mode_import_play(1);
    pPhy->mCcu.setMODE_mode_import_type(1);
    pPhy->mCcu.flushWCache();

//    qDebug("----ccu mode:%X", pPhy->mCcu.getMODE());
    pPhy->mCcu.inMODE();
//    qDebug("----in ccu mode:%X", pPhy->mCcu.getMODE());

    //! 3 load帧号
    //! 3.1
    pPhy->mScuGp.setREC_PLAY_mode_rec( 0 );
    pPhy->mScuGp.setREC_PLAY_mode_play( 0 );
    pPhy->mScuGp.setREC_PLAY_index_inc( 0 );
    pPhy->mScuGp.setREC_PLAY_loop_playback(0);
    pPhy->mScuGp.setREC_PLAY_index_load( 1 );
    pPhy->mScuGp.flushWCache();

//    qDebug("----scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());
    pPhy->mScuGp.inREC_PLAY();
//    qDebug("----in scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());

    //! 3.2
    pPhy->mScuGp.setINDEX_CUR_index(0);
    pPhy->mScuGp.setINDEX_BASE_index(0);
    pPhy->mScuGp.setINDEX_UPPER_index(0);
    pPhy->mScuGp.flushWCache();

//    qDebug("----in INDEX_CUR    :%X", pPhy->mScuGp.inINDEX_CUR());
//    qDebug("----in INDEX_BASE   :%X", pPhy->mScuGp.inINDEX_BASE());
//    qDebug("----in INDEX_UPPER  :%X", pPhy->mScuGp.inINDEX_UPPER());

    //! 3.3. 播放状态
    pPhy->mScuGp.setREC_PLAY_index_load( 0 );
    pPhy->mScuGp.setREC_PLAY_mode_play( 1 );
    pPhy->mScuGp.setREC_PLAY_index_inc( 1 );
    pPhy->mScuGp.flushWCache();

//    qDebug("----scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());
    pPhy->mScuGp.inREC_PLAY();
//    qDebug("----in scu PLAY:%X", pPhy->mScuGp.getREC_PLAY());

#if 0
    //! 4 one req trace,search
    pPhy->mCcu.outTRACE_trace_once_req(1);
    pPhy->mCcu.outTRACE_search_once_req(1);

    //! 5 run
    pPhy->mCcu.setCcuRun( 1 );
    pPhy->mCcu.flushWCache();
    pPhy->mCcu.waitStop();
#else
    //! 4 playback
    m_pHistSession->m_acquire.setSaLength(attr.saValid,
                                          attr.saChLen,
                                          attr.saLaLen);
    m_pHistSession->m_acquire.mCHFlow.setLength( attr.saChLen );
    m_pHistSession->m_acquire.mLaFlow.setLength( attr.saLaLen );
//    qDebug()<<__FILE__<<__LINE__
//           <<"saValid"<<attr.saValid
//           <<"saChLen"<<attr.saChLen
//           <<"saLaLen"<<attr.saLaLen;
    dsoEngine::playback(m_pHistSession, m_pViewSession);

    //! 5 等待回放完成。
    pPhy->mCcu.waitStop();
#endif

//    qDebug()<<"------------------------------------------------------------------------------";
}

int dsoEngine::closeMemory( handleMemory* handle )
{
    Q_ASSERT( NULL != handle );

    delete handle;
    handle = NULL;

    return 0;
}
