#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QMutex>

#include "../../include/dsotype.h"
#include "../../service/service_id.h"
#include "../enginemsg.h"

using namespace DsoType;
using namespace DsoEngine;

class engine;
typedef void (engine::*EngineProc)(void);

union unProc
{
    EngineProc proc;

    //! set
    DsoErr (engine::*onBool)( bool );
    DsoErr (engine::*onInt)( int );
    DsoErr (engine::*onLonglong)( qlonglong );
    DsoErr (engine::*onPtr)( void *ptr );

    DsoErr (engine::*onU8)( quint8 );
    DsoErr (engine::*onU16)( quint16 );
    DsoErr (engine::*onU32)( quint32 );
    DsoErr (engine::*onU64)( qulonglong );

    DsoErr (engine::*onReal)( DsoReal );

    DsoErr (engine::*onIntPtr)( int, void *ptr );
    DsoErr (engine::*onStack)( EnginePara &para );

    DsoErr (engine::*onRangeBool)( int, bool );
    DsoErr (engine::*onRangeInt)( int, int );
    DsoErr (engine::*onRangeLonglong)(int, qlonglong );
    DsoErr (engine::*onRangeReal)(int, DsoReal );

    DsoErr (engine::*onVoid)();

    //! get
    bool (engine::*qBool)();
    int (engine::*qInt)();
    qlonglong (engine::*qLonglong)();

    quint8 (engine::*qU8)();
    quint16 (engine::*qU16)();
    quint32 (engine::*qU32)();
    quint64 (engine::*qU64)();
    DsoReal (engine::*qReal)();

    bool (engine::*qBoolInt)(int);
    int (engine::*qIntInt)(int);
    qlonglong (engine::*qLonglongInt)(int);

    quint8 (engine::*qU8Int)( int );
    quint16 (engine::*qU16Int)( int );
    quint32 (engine::*qU32Int)( int );
    quint64 (engine::*qU64Int)( int );
    quint32 (engine::*qU32Stack)( EnginePara &para );

};

struct IEngineContext
{
    virtual void enter(){}
    virtual void exit(){}
};

struct stopConfigContext : public IEngineContext
{
    virtual void enter();
    virtual void exit();
};

struct EngineMsgAttr
{
    bool bReplay;
};

struct struEngineMsgEntry
{
    EngineMsg msg;
    ServiceId id;
    EngineParaType paraType;

    EngineProc proc;
    EngineMsgAttr *pAttr;

    ServiceId idEnd;

    int paraCnt;

    IEngineContext *pEngineContext;
};

struct struEngineMsgMap
{
    const struEngineMsgMap *baseMap;
    const struEngineMsgEntry *selfEntry;
};

#define DECLARE_MSG_MAP() \
private:\
    static const struEngineMsgEntry m_msgEntry[];\
protected:\
    static const struEngineMsgMap m_msgMap;\
    virtual const  struEngineMsgMap* getMsgMap();

#define IMPLEMENT_MSG_MAP( base, name )\
const  struEngineMsgMap* name::getMsgMap()\
{ return &name::m_msgMap; }\
const struEngineMsgMap name::m_msgMap={ &base::m_msgMap, &name::m_msgEntry[0] };\
const struEngineMsgEntry name::m_msgEntry[]={

#define ON_COMMAND( classname, msg, id, type, proc, pAttr )  { msg, id, type,\
                                                       (EngineProc)( void (classname::*)(void))&classname::proc,\
                                                       pAttr,\
                                                        E_SERVICE_ID_NONE, 0, NULL }

#define ON_COMMAND_CONTEXT( classname, msg, id, type, proc, pAttr, cont )  { msg, id, type,\
                                                       (EngineProc)( void (classname::*)(void))&classname::proc,\
                                                        pAttr,\
                                                        E_SERVICE_ID_NONE, 0, cont }


#define ON_COMMAND_STACK( classname, msg, id, type, proc, pAttr, stackSize )  { msg, id, type,\
                                                       (EngineProc)( void (classname::*)(void))&classname::proc,\
                                                        pAttr,\
                                                        E_SERVICE_ID_NONE, stackSize, NULL }


#define ON_RANGE_ID( classname, msg, id1, id2, type, proc, pAttr ) { msg, id1, type,\
    (EngineProc)( void (classname::*)(void))&classname::proc,\
    pAttr,\
     id2, 0, NULL }

#define ON_IdNone_COMMAND( classname, msg, type, proc,pAttr ) \
        ON_COMMAND( classname, msg, E_SERVICE_ID_NONE, type, proc, pAttr )

#define END_OF_ITEM()   {ENGINE_MSG_NONE,E_SERVICE_ID_NONE,E_PARA_NONE, NULL, NULL, E_SERVICE_ID_NONE, 0, NULL } };

/*!
 * \brief The engine class
 * base class of engine
 * it can emit signals
 */
class engine : public QObject
{
    Q_OBJECT

    DECLARE_MSG_MAP()

public:
    explicit engine(QObject *parent = 0);    
    ~engine();
signals:

public slots:

private:
    unProc m_proc;

protected:
    static engine *m_pActEngine;
    static QMutex *m_pEngineLock;

public:
    static void lockEngine();
    static void unlockEngine();

    static bool    m_bFreeze;
    void freezeEngine();
    void unFreezeEngine();

public:
    static DsoErr setActiveEngine( engine *pEngine );
    static engine* getActiveEngine();
    static DsoErr receiveMsg( EngineMsg msg,
                              EnginePara& para,
                              void *outPtr=NULL );
public:
    virtual DsoErr receiveServiceMsg( EngineMsg msg,
                                      EnginePara& para,
                                      void *outPtr=NULL );
    virtual DsoErr doDispatch( EngineMsg msg,
                               EnginePara& para,
                               void *outPtr = NULL );

    DsoErr doMap( const struEngineMsgMap *pMap,
                  EngineMsg msg,
                  EnginePara& para,
                  void *outPtr );

    virtual DsoErr exec(const struEngineMsgMap *pMap,
                         const struEngineMsgEntry *pEntry, EngineMsg,
                         EnginePara& para,
                         void *outPtr,
                         ServiceId sId = E_SERVICE_ID_NONE );

    virtual void preDoProc( EngineMsg msg, ServiceId sId );
    virtual void postDoProc( EngineMsg msg, ServiceId sId );

    virtual DsoErr doProc( const struEngineMsgEntry *pEntry,
                           EngineProc proc,
                           EngineParaType paraType,
                           EnginePara& para,
                           ServiceId sId );

    virtual DsoErr toEngine( engine *pEngine );

    QString &getName();

protected:
    virtual DsoErr onExit();
    virtual DsoErr onEnter();

    bool rangeTypeMatch( EngineParaType tableEntryType,
                         EngineParaType praType );
    bool rangeContains( const struEngineMsgEntry *pEntry,
                        ServiceId sId );

    EngineWR wr( EngineMsg msg );

protected:
    QString mName;
};

#endif // ENGINE_H
