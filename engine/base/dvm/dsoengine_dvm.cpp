
#include "../dsoengine.h"

#define thisDvm m_pPhy->mCounter

DsoErr dsoEngine::setDvmSource( Chan src )
{
    Q_ASSERT( src >= chan1 && src <= chan4 );

    thisDvm.setfreq_cfg_1_dvm_src(  src - chan1 + IPhyFrequ::dvm_ch1 );

    return ERR_NONE;
}
//! us
DsoErr dsoEngine::setDvmGateTime( int time )
{
    thisDvm.setfreq_cfg_1_dvm_gate(  thisDvm.dvmTimeIndex( time ) );

    return ERR_NONE;
}

DsoErr dsoEngine::queryDvmValue( EngineDvmValue *pEngineDvmVal )
{
    Q_ASSERT( NULL != pEngineDvmVal );

    //! value
    pEngineDvmVal->mDvmAver = thisDvm.indvm_aver();
    pEngineDvmVal->mDvmRms = thisDvm.indvm_rms();
    pEngineDvmVal->mDvmRange = thisDvm.indvm_range();

    //! ref
    int id;
    id = thisDvm.freq_cfg_1.dvm_src;
    Q_ASSERT( id >= 0 && id <= 3 );
    pEngineDvmVal->mGnd = m_pPhy->m_ch[ id ].getAfeGnd();
    pEngineDvmVal->mScale = m_pPhy->m_ch[ id ].getAfeScale();

    pEngineDvmVal->mVDiv = 25;

    return ERR_NONE;
}
