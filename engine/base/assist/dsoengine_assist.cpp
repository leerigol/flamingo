
#include "../dsoengine.h"


//! map
struct bmAdcInteMode
{
    quint32 bm;
    int chCnt;

    dso_phy::IPhyADC::eAdcInterleaveMode inteMode;


    Chan coreA;
    Chan coreB;
    Chan coreC;
    Chan coreD;
};

static bmAdcInteMode _transMap[]=
{
    {0, 0, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4, },   //! no ch
    {1, 1, dso_phy::IPhyADC::ch_a, chan1, chan1, chan1, chan1 },       //! ch1
    {2, 1, dso_phy::IPhyADC::ch_b, chan2, chan2, chan2, chan2 },       //! ch2
    {3, 4, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4 },    //! ch1,ch2
    {4, 1, dso_phy::IPhyADC::ch_c, chan3, chan3, chan3, chan3 },       //! ch3
    {5, 2, dso_phy::IPhyADC::ch_ac,chan1, chan3, chan1, chan3 },       //! ch1,ch3
    {6, 2, dso_phy::IPhyADC::ch_bc,chan2, chan3, chan2, chan3 },       //! ch2,ch3
    {7, 4, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4 },    //! ch1,ch2,ch3

    {8, 1, dso_phy::IPhyADC::ch_d,  chan4, chan4, chan4, chan4, },     //! ch4,
    {9, 2, dso_phy::IPhyADC::ch_ad, chan1, chan4, chan1, chan4 },      //! ch1,ch4
    {10,2, dso_phy::IPhyADC::ch_bd, chan2, chan4, chan2, chan4 },      //! ch2,ch4
    {11,4, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4 },    //! ch1,ch2,ch4
    {12,4, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4 },    //! ch3,ch4
    {13,4, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4 },    //! ch1,ch3,ch4
    {14,4, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4 },    //! ch2,ch3,ch4
    {15,4, dso_phy::IPhyADC::ch_abcd, chan1, chan2, chan3, chan4 },    //! ch1,ch2,ch3,ch4
};

int dsoEngine::chBmCnt( quint32 bm )
{
    bm = trim_analog_bm( bm );
    for ( int i = 0; i < array_count(_transMap); i++ )
    {
        if ( _transMap[i].bm == bm )
        { return _transMap[i].chCnt; }
    }

    return 0;
}

int dsoEngine::toChCnt( quint32 bm )
{
    int cnt;

    cnt = chBmCnt( bm );

    if ( cnt == 0 ) return 4;
    else return cnt;
}

//! 4 bit
dso_phy::IPhyADC::eAdcInterleaveMode dsoEngine::toAdcMod( quint32 bm )
{
    //! analog
    bm = trim_analog_bm( bm );

    //! bm, ch mode
    for ( int i = 0; i < array_count(_transMap); i++ )
    {
        if ( _transMap[i].bm == bm )
        { return _transMap[i].inteMode; }
    }

    qWarning()<<"!!!invalid inte mode";
    return dso_phy::IPhyADC::ch_abcd;
}

void dsoEngine::toAdcCoreCH( quint32 bm,
                  EngineAdcCoreCH *pChCore )
{
    Q_ASSERT( NULL != pChCore );

    //! only analog
    bm = trim_analog_bm( bm );
    for ( int i = 0; i < array_count(_transMap); i++ )
    {
        if ( _transMap[i].bm == bm )
        {
            pChCore->assignCoreCH( _transMap[i].coreA,
                                   _transMap[i].coreB,
                                   _transMap[i].coreC,
                                   _transMap[i].coreD );
            return;
        }
    }

    qWarning()<<"invalid ch";
    pChCore->assignCoreCH();
}

int dsoEngine::toAdc( Chan ch, int lev )  //! in uv unit
{
    if( ch >= chan1 && ch <= chan4 )
    {

        int scale, gnd;
        scale = m_pPhy->m_ch[ ch - chan1 ].getAfeScale();
        gnd   = m_pPhy->m_ch[ ch - chan1 ].getAfeGnd();

        int adc;
        if( scale != 0 )
        {
            //! to real adc
            adc = (float)lev*adc_vdiv_dots / scale + gnd;

            adc = qBound(0, adc, adc_max);

            return adc;
        }
    }
    return adc_center;
}

int dsoEngine::toAdcGroups( quint32 bm,
                 AdcGroups *groups )
{
    Q_ASSERT( NULL != groups );

    groups->rst();

    //! gain && offset
    switch( bm & 0x0f )
    {
        case 1:     //! ch1
            groups->appendGroup( 0, 1, 2, 3 );
        break;

        case 2:     //! ch2
            groups->appendGroup( 1, 0, 2, 3 );
        break;

        case 4:     //! ch3
            groups->appendGroup( 2, 0, 1, 3 );
        break;

        case 8:     //! ch4
            groups->appendGroup( 3, 0, 1, 2 );
        break;

        case 5:     //! ch1,ch3
            groups->appendGroup( 0, 1 );
            groups->appendGroup( 2, 3 );
        break;
        case 9:     //! ch1, ch4
            groups->appendGroup( 0, 1 );
            groups->appendGroup( 3, 2 );
        break;
        case 6:     //! ch2,ch3
            groups->appendGroup( 1, 0 );
            groups->appendGroup( 2, 3 );
        break;
        case 10:    //! ch2,ch4
            groups->appendGroup( 1, 0 );
            groups->appendGroup( 3, 2 );
        break;

        default:
            groups->appendGroup( 0 );
            groups->appendGroup( 1 );
            groups->appendGroup( 2 );
            groups->appendGroup( 3 );
            break;
    }

    return 0;
}

