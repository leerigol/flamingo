
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mSlopeCfg

void dsoEngine::enterTrigSlope()
{
    m_pPhy->mTpu.setTrigSrc( IPhyTpu::a );

    m_pPhy->mTpu.setiir_set_iir_type( IPhyTpu::iir_dc );
}

DsoErr dsoEngine::setTrigSlopeCfg( TrigSlopeCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigSlopeCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigSlopeCfg( TrigSlopeCfg &cfg, IPhyTpu *pTpu )
{
    IPhyTpu::eCmpSrc src;

    src = toaCmpSrc( cfg.getChan() );
    Q_ASSERT( src != IPhyTpu::invalid );

    pTpu->rstIir();

    //! source
    if ( Trigger_Edge_Rising == cfg.mSlope )
    {
        //! a -- ch_a
        //! b -- ch_b
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mCh) );
        pTpu->aEventCmpb( toaCmpSrc_b( cfg.mCh) );

        pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                            IPhyTpu::cmp_c_opti, false );

        pTpu->aEventWidth( toWidthCmp(cfg.mCmp),
                           cfg.mWidthL, cfg.mWidthH );

        pTpu->setTrigSrc();
    }
    else if ( Trigger_Edge_Falling == cfg.mSlope )
    {
        //! b -- ch_a
        //! a -- ch_b
        pTpu->aEventCmpa( toaCmpSrc_b( cfg.mCh), IPhyTpu::fall );
        pTpu->aEventCmpb( toaCmpSrc_a( cfg.mCh), IPhyTpu::fall );

        pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                            IPhyTpu::cmp_c_opti, false );

        pTpu->aEventWidth( toWidthCmp(cfg.mCmp),
                           cfg.mWidthL, cfg.mWidthH );

        pTpu->setTrigSrc();
    }
    else
    { Q_ASSERT(false); }

}
