#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mPulseCfg

//void dsoEngine::enterTrigPulse()
//{
//    applyTrigPulse( selfTrigCfg );
//}

DsoErr dsoEngine::setTrigPulseCfg( TrigPulseCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigPulse( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigPulse( TrigPulseCfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.getChan() );
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();

    pTpu->aEventCmpa( src );
    pTpu->aEventCmpb( src );

    pTpu->aEventWidth( toWidthCmp(cfg.getCmp()),
                       cfg.getWidthL(),
                       cfg.getWidthH()
                       );

    if(Trigger_pulse_positive == cfg.getPolarity())
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                            IPhyTpu::cmp_a, false);
    }
    else
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_b, true,
                            IPhyTpu::cmp_b, true);
    }

    pTpu->setTrigSrc( IPhyTpu::a );
}
