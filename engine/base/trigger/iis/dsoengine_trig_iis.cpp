#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mIIsCfg

DsoErr dsoEngine::setTrigI2SCfg( TrigI2SCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigI2SCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigI2SCfg(TrigI2SCfg &cfg, IPhyTpu *pTpu)
{
    //!reset
    pTpu->rstIir();

    //! src
    pTpu->aEventCmpa(toaCmpSrc(cfg.mSclk),
                     (cfg.mSlope == Trigger_Edge_Rising)? IPhyTpu::rise : IPhyTpu::fall);
    pTpu->aEventCmpb(toaCmpSrc(cfg.mSclk),
                     (cfg.mSlope == Trigger_Edge_Rising)? IPhyTpu::rise : IPhyTpu::fall);
    pTpu->aEventClkDat(IPhyTpu::logic, false,
                       IPhyTpu::cmp_a, false);

    pTpu->bEventCmpa(toaCmpSrc(cfg.mWs), IPhyTpu::rise);
    pTpu->bEventCmpb(toaCmpSrc(cfg.mWs), IPhyTpu::rise);
    pTpu->bEventClkDat(IPhyTpu::logic, false,
                       IPhyTpu::cmp_a, false);

    pTpu->rEventCmpa(toaCmpSrc(cfg.mSda), IPhyTpu::rise);
    pTpu->rEventCmpb(toaCmpSrc(cfg.mSda), IPhyTpu::rise);
    pTpu->rEventClkDat(IPhyTpu::logic, false,
                       IPhyTpu::cmp_a, false);

    pTpu->setTrigSrc(IPhyTpu::iis);

    pTpu->setiis_cfg_1_iis_trig_type(cfg.mDataCmp);
    pTpu->setiis_cfg_1_iis_trig_channel_sel(cfg.mLine);
    pTpu->setiis_cfg_1_iis_trig_bus_type(cfg.mSpec);
    pTpu->setiis_cfg_1_iis_trig_bitnum_vld(cfg.mUsedWidth);
    pTpu->setiis_cfg_1_iis_trig_bitnum_total(cfg.mWidth);
    pTpu->setiis_cfg_2_iis_trig_word_min(cfg.mMinData);
    pTpu->setiis_cfg_3_iis_trig_word_max(cfg.mMaxData);
    pTpu->setiis_cfg_4_iis_trig_word_mask(cfg.mMask);
}
