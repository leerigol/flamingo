
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mOverCfg

void dsoEngine::enterTrigOver()
{
    m_pPhy->mTpu.setTrigSrc( IPhyTpu::a );
}

DsoErr dsoEngine::setTrigOverCfg( TrigOverCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigOverCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigOverCfg( TrigOverCfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.getChan() );
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();

    if(cfg.getSlope() == Trigger_Edge_Rising)
    {
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mCh) );

        switch (cfg.getEvent())
        {
        case Trigger_over_enter:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                                IPhyTpu::cmp_a, true );
            pTpu->aEventWidth( IPhyTpu::gt);

            break;
        case Trigger_over_exit:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                                IPhyTpu::cmp_a, false );
            pTpu->aEventWidth( IPhyTpu::gt);

            break;
        case Trigger_over_time:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                                IPhyTpu::cmp_a, false );
            pTpu->aEventWidth( IPhyTpu::tmo, cfg.mWidth);

            break;

        default:
            Q_ASSERT( false );
            break;
        }

        pTpu->setTrigSrc( IPhyTpu::a );
    }
    else if(cfg.getSlope() == Trigger_Edge_Falling)
    {

        pTpu->aEventCmpa( toaCmpSrc_b(cfg.mCh) );


        switch (cfg.getEvent())
        {
        case Trigger_over_enter:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                                IPhyTpu::cmp_a, false );
            pTpu->aEventWidth( IPhyTpu::gt);

            break;
        case Trigger_over_exit:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                                IPhyTpu::cmp_a, true );
            pTpu->aEventWidth( IPhyTpu::gt);

            break;
        case Trigger_over_time:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                                IPhyTpu::cmp_a, true );
            pTpu->aEventWidth( IPhyTpu::tmo, cfg.mWidth);

            break;

        default:
            Q_ASSERT( false );
            break;
        }

        pTpu->setTrigSrc( IPhyTpu::a );
    }
    else if(cfg.getSlope() == Trigger_Edge_Alternating)
    {
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mCh) );


        pTpu->bEventCmpa( toaCmpSrc_b(cfg.mCh) );


        switch (cfg.getEvent())
        {
        case Trigger_over_enter:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                                IPhyTpu::cmp_a, true );

            pTpu->bEventClkDat( IPhyTpu::cmp_a, false,
                                IPhyTpu::cmp_a, false );

            pTpu->aEventWidth( IPhyTpu::gt);

            break;
        case Trigger_over_exit:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                                IPhyTpu::cmp_a, false );

            pTpu->bEventClkDat( IPhyTpu::cmp_a, true,
                                IPhyTpu::cmp_a, true );

            pTpu->aEventWidth( IPhyTpu::gt);

            break;
        case Trigger_over_time:
            pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                                IPhyTpu::cmp_a, false );

            pTpu->bEventClkDat( IPhyTpu::cmp_a, true,
                                IPhyTpu::cmp_a, true );

            pTpu->aEventWidth( IPhyTpu::tmo, cfg.mWidth);
            pTpu->bEventWidth( IPhyTpu::tmo, cfg.mWidth);

            break;

        default:
            Q_ASSERT( false );
            break;
        }

        pTpu->setTrigSrc( IPhyTpu::a_or_b );
    }
    else
    {
        Q_ASSERT( false );
    }

}
