#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mSpiCfg

DsoErr dsoEngine::setTrigSpiCfg( TrigSpiCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigSpiCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

//!数据长度不得小于 spi当前数据位宽 32bit对齐后 的长度
#define CHECK_DATA_SIZE(data)  do{\
                                    if( (data.size()%4 != 0) || ((quint32)data.size()*8 < cfg.mDataLen) ){\
                                    Q_ASSERT(false);}\
                                  }while(0)

#define GET_DATA_L(s)          data = (Tpu_reg*)s.data();               Q_ASSERT(data != NULL)
#define GET_DATA_H(s)          data = (Tpu_reg*)(s.data() + 4);         Q_ASSERT(data != NULL)

void dsoEngine::applyTrigSpiCfg( TrigSpiCfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.mClk);
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();

    //!source
    pTpu->rEventCmpa(toaCmpSrc(cfg.mData));
    pTpu->rEventClkDat(IPhyTpu::cmp_a, false,
                       IPhyTpu::cmp_a, false
                       );

    pTpu->aEventCmpa( toaCmpSrc(cfg.mClk ) );
    pTpu->aEventClkDat(IPhyTpu::cmp_a, ((cfg.mClkSlope ==  Trigger_Edge_Rising)? false : true ),
                       IPhyTpu::cmp_a, ((cfg.mClkSlope ==  Trigger_Edge_Rising)? false : true )
                       );

    pTpu->bEventCmpa(toaCmpSrc(cfg.mCs)
                     );
    pTpu->bEventClkDat(IPhyTpu::cmp_a, (cfg.mCsSlope ==  Trigger_Edge_Rising)? false : true ,
                       IPhyTpu::cmp_a, (cfg.mCsSlope ==  Trigger_Edge_Rising)? false : true
                       );

    pTpu->setTrigSrc( IPhyTpu::spi );

    //!Spi
    pTpu->setspi_cfg_1_spi_trig_type(cfg.mType);

    if(trig_spi_idle == cfg.mType)
    {
         pTpu->setspi_cfg_2_spi_trig_timeout( cfg.mTmo * ( ((double)312.5e6) / time_s(1)) );

    }

    pTpu->setspi_cfg_1_spi_trig_dat_type(cfg.mDatCmp);
    pTpu->setspi_cfg_1_spi_trig_dat_bit_num(cfg.mDataLen);

#if 0
    CHECK_DATA_SIZE(cfg.mDataMin);
    CHECK_DATA_SIZE(cfg.mDataMax);
    CHECK_DATA_SIZE(cfg.mDataMask);

    Tpu_reg  *data = NULL;

    GET_DATA_L(cfg.mDataMin);
    pTpu->setspi_cfg_3_spi_trig_dat_min(*data);
    GET_DATA_L(cfg.mDataMax);
    pTpu->setspi_cfg_4_spi_trig_dat_max(*data);
    GET_DATA_L(cfg.mDataMask);
    pTpu->setspi_cfg_5_spi_trig_dat_mask(*data);
#endif
    Q_ASSERT(cfg.mDataMin.count() >= cfg.bitCountMax);
    Q_ASSERT(cfg.mDataMax.count() >= cfg.bitCountMax);
    Q_ASSERT(cfg.mDataMask.count()>= cfg.bitCountMax);

    quint32 min = 0, max = 0, mask = 0;

    for(int i = 0; i<cfg.mDataMin.count(); i++)
    {
        min  |= (cfg.mDataMin.at(i) ? (1<<i) : 0 );
        max  |= (cfg.mDataMax.at(i) ? (1<<i) : 0 );
        mask |= (cfg.mDataMask.at(i)? (1<<i) : 0 );
    }

    pTpu->setspi_cfg_3_spi_trig_dat_min( min);
    pTpu->setspi_cfg_4_spi_trig_dat_max( max);
    pTpu->setspi_cfg_5_spi_trig_dat_mask(mask);
}
