#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mLinCfg

DsoErr dsoEngine::setTrigLinCfg( TrigLinCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigLinCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigLinCfg(TrigLinCfg &cfg, IPhyTpu *pTpu)
{
    //!reset
    pTpu->rstIir();

    //! src
    pTpu->rEventCmpa(toaCmpSrc(cfg.mCh), IPhyTpu::rise);
    pTpu->rEventCmpb(toaCmpSrc(cfg.mCh), IPhyTpu::rise);
    pTpu->rEventClkDat(IPhyTpu::cmp_a, false,
                       IPhyTpu::cmp_a, false
                       );
    pTpu->setTrigSrc(IPhyTpu::lin);

    pTpu->setlin_cfg_1_lin_trig_type(cfg.mWhen);
    pTpu->setlin_cfg_1_lin_trig_type_id(cfg.mIdCmp);
    pTpu->setlin_cfg_1_lin_trig_type_dat(cfg.mDatCmp);
    pTpu->setlin_cfg_1_lin_trig_type_err(cfg.mErr);
    pTpu->setlin_cfg_1_lin_trig_spec(cfg.mVer);

    quint32 id   = cfg.mIdMin;
    Tpu_reg idP0 = 0;
    Tpu_reg idP1 = 0;

    //!计算ID校验位
    idP0  =  (id&(1<<0))^(id&(1<<1))^(id&(1<<2))^(id&(1<<4));
    idP1  =  ((id&(1<<1))^(id&(1<<3))^(id&(1<<4))^(id&(1<<5)));
    id    &= (~(3<<6));
    id    |=  ((idP0 != 0)?(1<<6):0);
    id    |=  ((idP1 == 0)?(1<<7):0);

    pTpu->setlin_cfg_1_lin_trig_id_min(id);
    pTpu->setlin_cfg_1_lin_trig_id_max(id);
    pTpu->setlin_cfg_1_lin_trig_dat_cmpnum(cfg.byteCount());

    int div = (time_s(1)/TD_TICK)/(cfg.mClk);
    pTpu->setlin_cfg_2_lin_trig_clk_div( div);
    pTpu->setlin_cfg_3_lin_trig_sa_pos( (int)div*cfg.mSaPos*0.01);

    Q_ASSERT(cfg.mMinBits.count() >= 8);
    Q_ASSERT(cfg.mMaxBits.count() >= 8);
    Q_ASSERT(cfg.mMaskBits.count()>= 8);

    Tpu_reg   datMinL  = 0;
    Tpu_reg   datMinH  = 0;

    Tpu_reg   datMaxL  = 0;
    Tpu_reg   datMaxH  = 0;

    Tpu_reg   datMaskL  = 0;
    Tpu_reg   datMaskH  = 0;

    for(int i = 0; i<4; i++)
    {
        datMinL  |= (cfg.mMinBits[i]<<(i*8));
        datMinH  |= (cfg.mMinBits[i+4]<<((i*8)));

        datMaxL  |= (cfg.mMaxBits[i]<<(i*8));
        datMaxH  |= (cfg.mMaxBits[i+4]<<(i*8));

        datMaskL |= (cfg.mMaskBits[i]<<(i*8));
        datMaskH |= (cfg.mMaskBits[i+4]<<(i*8));
    }
    qDebug()<<__FILE__<<__LINE__<<datMinL<<datMinH<<datMaxL<<datMaxH<<datMaskL<<datMaskH;

    pTpu->setlin_cfg_4_lin_trig_dat_min_bits31_0(  datMinL );
    pTpu->setlin_cfg_5_lin_trig_dat_min_bits63_32( datMinH );

    pTpu->setlin_cfg_6_lin_trig_dat_max_bits31_0(  datMaxL );
    pTpu->setlin_cfg_7_lin_trig_dat_max_bits63_32( datMaxH );

    pTpu->setlin_cfg_8_lin_trig_dat_mask_bits31_0( datMaskL );
    pTpu->setlin_cfg_9_lin_trig_dat_maak_bits63_32(datMaskH );


    //!休眠帧特殊处理（id=60, data = 0x 00FFFFFF FFFFFFFF）
    if(trig_lin_sleep == cfg.mWhen)
    {
        pTpu->setlin_cfg_1_lin_trig_type(trig_lin_id_data);
        pTpu->setlin_cfg_1_lin_trig_type_id(cmp_eq);
        pTpu->setlin_cfg_1_lin_trig_type_dat(cmp_eq);
        pTpu->setlin_cfg_1_lin_trig_id_min(60);
        pTpu->setlin_cfg_1_lin_trig_dat_cmpnum(8);
        pTpu->setlin_cfg_4_lin_trig_dat_min_bits31_0(  0xFFFFFFFF );
        pTpu->setlin_cfg_5_lin_trig_dat_min_bits63_32( 0X00FFFFFF );
        pTpu->setlin_cfg_8_lin_trig_dat_mask_bits31_0( 0xFFFFFFFF );
        pTpu->setlin_cfg_9_lin_trig_dat_maak_bits63_32(0xFFFFFFFF );
    }
}

