
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mRuntCfg

void dsoEngine::enterTrigRunt()
{
    m_pPhy->mTpu.setTrigSrc( IPhyTpu::a );
}

DsoErr dsoEngine::setTrigRuntCfg( TrigRuntCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigRunt( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigRunt( TrigRuntCfg &cfg, IPhyTpu *pTpu )
{
    IPhyTpu::eCmpSrc src;

    //! source
    src = toaCmpSrc( cfg.getChan() );
    Q_ASSERT( src != IPhyTpu::invalid );

    pTpu->rstIir();

    TriggerPulsePolarity pol = cfg.getPolarity();

    EMoreThan  cmp    = cfg.mCmp;
    qint64     widthL = cfg.mWidthL;
    qint64     widthH = cfg.mWidthH;

    if(Trigger_When_None ==  cmp)
    {
        cmp    = Trigger_When_Morethan;
        widthL = 0;
        widthH = 0;
    }

    if ( Trigger_pulse_positive == pol )
    {
        //! a -- ch_a
        //! b -- ch_b
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mCh ) );
        pTpu->aEventCmpb( toaCmpSrc_b( cfg.mCh) );

        pTpu->aEventClkDat( IPhyTpu::cmp_b, false,
                            IPhyTpu::cmp_c_opti, false );

        pTpu->aEventWidth( toWidthCmp(cmp),
                           widthL,
                           widthH );

        pTpu->setTrigSrc();
    }
    else if ( Trigger_pulse_negative == pol )
    {
        //! b -- ch_a
        //! a -- ch_b
        pTpu->aEventCmpb( toaCmpSrc_a( cfg.mCh ), IPhyTpu::fall);
        pTpu->aEventCmpa( toaCmpSrc_b( cfg.mCh), IPhyTpu::fall );

        pTpu->aEventClkDat( IPhyTpu::cmp_b, true,
                            IPhyTpu::cmp_c_opti, false );

        pTpu->aEventWidth( toWidthCmp(cmp),
                           widthL,
                           widthH );

        pTpu->setTrigSrc();
    }
    else if ( Trigger_pulse_any == pol )
    {
        //! a
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mCh ) );
        pTpu->aEventCmpb( toaCmpSrc_b( cfg.mCh) );

        pTpu->aEventClkDat( IPhyTpu::cmp_b, false,
                            IPhyTpu::cmp_c_opti, false );

        pTpu->aEventWidth( toWidthCmp(cmp),
                           widthL,
                           widthH );

        //! b
        pTpu->bEventCmpb( toaCmpSrc_a( cfg.mCh ), IPhyTpu::fall);
        pTpu->bEventCmpa( toaCmpSrc_b( cfg.mCh), IPhyTpu::fall );

        pTpu->bEventClkDat( IPhyTpu::cmp_b, true,
                            IPhyTpu::cmp_c_opti, false );

        pTpu->bEventWidth( toWidthCmp(cmp),
                           widthL,
                           widthH );

        pTpu->setTrigSrc( IPhyTpu::a_or_b );
    }
    else
    { Q_ASSERT(false); }

}

