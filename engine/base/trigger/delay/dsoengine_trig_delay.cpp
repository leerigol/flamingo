
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mDelayCfg

void dsoEngine::enterTrigDelay()
{}
void dsoEngine::exitTrigDelay()
{}

DsoErr dsoEngine::setTrigDelayCfg( TrigDelayCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigDelay( selfTrigCfg, getTpu() );

    return ERR_NONE;
}
void dsoEngine::applyTrigDelay( TrigDelayCfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.mChA );
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();

    if(Trigger_Edge_Rising == cfg.mSlopeA)
    {
        pTpu->aEventCmpb( toaCmpSrc_a( cfg.mChA), IPhyTpu::rise);
    }
    else
    {
        pTpu->aEventCmpb( toaCmpSrc_a( cfg.mChA), IPhyTpu::fall  );
    }

    if(Trigger_Edge_Rising == cfg.mSlopeB)
    {
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mChB), IPhyTpu::rise);
        pTpu->aEventClkDat( IPhyTpu::cmp_a, true ,
                            IPhyTpu::cmp_c_opti, false );
    }
    else
    {
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mChB), IPhyTpu::fall  );
        pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                            IPhyTpu::cmp_c_opti, false );
    }

    pTpu->aEventWidth( toWidthCmp(cfg.mCmp),
                       cfg.mWidthL,
                       cfg.mWidthH );

    pTpu->setTrigSrc( IPhyTpu::a );
}
