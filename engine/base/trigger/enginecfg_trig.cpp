
#include "enginecfg_trig.h"

TrigEdgeCfg::TrigEdgeCfg()
{
    mCh = chan1;
    mSlope = Trigger_Edge_Rising;
}

TrigEdgeCfg::TrigEdgeCfg( const TrigEdgeCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );
    *this = *pCfg;
}
TrigEdgeCfg::TrigEdgeCfg( const TrigEdgeCfg &cfg )
{
    *this = cfg;
}
TrigEdgeCfg &TrigEdgeCfg::operator= ( const TrigEdgeCfg &cfg )
{
    mCh = cfg.mCh;
    mSlope = cfg.mSlope;

    return *this;
}

void TrigEdgeCfg::setChan( Chan ch )
{ mCh = ch; }
Chan TrigEdgeCfg::getChan()
{ return mCh; }

void TrigEdgeCfg::setSlope( EdgeSlope slp )
{ mSlope = slp; }
EdgeSlope TrigEdgeCfg::getSlope()
{ return mSlope; }

TrigPulseCfg::TrigPulseCfg()
{
    mCh = chan1;
    mPolariy = Trigger_pulse_positive;
    mCmp = Trigger_When_Morethan;

    mWidthH = 0;
    mWidthL = 0;
}

void TrigPulseCfg::setChan( Chan ch )
{ mCh = ch; }
Chan TrigPulseCfg::getChan()
{ return mCh; }

void TrigPulseCfg::setPolarity( TriggerPulsePolarity pol )
{ mPolariy = pol; }
TriggerPulsePolarity TrigPulseCfg::getPolarity()
{ return mPolariy; }

void TrigPulseCfg::setCmp( EMoreThan cmp )
{ mCmp = cmp; }
EMoreThan TrigPulseCfg::getCmp()
{ return mCmp; }

void TrigPulseCfg::setWidthH( qint64 w )
{ mWidthH = w; }
qint64 TrigPulseCfg::getWidthH()
{ return mWidthH; }

void TrigPulseCfg::setWidthL( qint64 l )
{ mWidthL = l; }
qint64 TrigPulseCfg::getWidthL()
{ return mWidthL; }


TrigSlopeCfg::TrigSlopeCfg()
{
    mCh = chan1;
    mSlope = Trigger_Edge_Rising;

    mCmp = Trigger_When_Morethan;
    mWidthH = 0;
    mWidthL = 0;
}

void TrigSlopeCfg::setChan( Chan ch )
{ mCh = ch; }
Chan TrigSlopeCfg::getChan()
{ return mCh; }

void TrigSlopeCfg::setSlope( EdgeSlope slp )
{ mSlope = slp; }
EdgeSlope TrigSlopeCfg::getSlope()
{ return mSlope; }

void TrigSlopeCfg::setCmp( EMoreThan cmp )
{ mCmp = cmp; }
EMoreThan TrigSlopeCfg::getCmp()
{ return mCmp; }

void TrigSlopeCfg::setWidthH( qint64 w )
{ mWidthH = w; }
qint64 TrigSlopeCfg::getWidthH()
{ return mWidthH; }

void TrigSlopeCfg::setWidthL( qint64 l )
{ mWidthL = l; }
qint64 TrigSlopeCfg::getWidthL()
{ return mWidthL; }

TrigOverCfg::TrigOverCfg()
{
    mCh = chan1;
    mSlope = Trigger_Edge_Rising;

    mEvent = Trigger_over_enter;
    mWidth = 0;
}

void TrigOverCfg::setChan( Chan ch )
{ mCh = ch; }
Chan TrigOverCfg::getChan()
{ return mCh; }

void TrigOverCfg::setSlope( EdgeSlope slp )
{ mSlope = slp; }
EdgeSlope TrigOverCfg::getSlope()
{ return mSlope; }

void TrigOverCfg::setEvent( OverEvent evt )
{ mEvent = evt; }
OverEvent TrigOverCfg::getEvent()
{ return mEvent; }

void TrigOverCfg::setWidth( qint64 w )
{ mWidth = w; }
qint64 TrigOverCfg::getWidth()
{ return mWidth; }

Chan TrigSHCfg::dat() const
{
    return mDat;
}

void TrigSHCfg::setDat(const Chan &dat)
{
    mDat = dat;
}

EdgeSlope TrigSHCfg::slope() const
{
    return mSlope;
}

void TrigSHCfg::setSlope(const EdgeSlope &slope)
{
    mSlope = slope;
}

SHEvent TrigSHCfg::event() const
{
    return mEvent;
}

void TrigSHCfg::setEvent(const SHEvent &event)
{
    mEvent = event;
}

qint64 TrigSHCfg::setupTime() const
{
    return mSetupTime;
}

void TrigSHCfg::setSetupTime(qint64 setupTime)
{
    mSetupTime = setupTime;
}

qint64 TrigSHCfg::holdTime() const
{
    return mHoldTime;
}

void TrigSHCfg::setHoldTime(qint64 holdTime)
{
    mHoldTime = holdTime;
}

TriggerPulsePolarity TrigSHCfg::datPolarity() const
{
    return mDatPolarity;
}

void TrigSHCfg::setDatPolarity(const TriggerPulsePolarity &datPolarity)
{
    mDatPolarity = datPolarity;
}

Chan TrigSHCfg::clk() const
{
    return mClk;
}

void TrigSHCfg::setClk(const Chan &clk)
{
    mClk = clk;
}

qint64 TrigNEdgeCfg::idleTime() const
{
    return mIdleTime;
}

void TrigNEdgeCfg::setIdleTime(qint64 idleTime)
{
    mIdleTime = idleTime;
}

int TrigNEdgeCfg::num() const
{
    return mNum;
}

void TrigNEdgeCfg::setNum(int num)
{
    mNum = num;
}

WindowEvent TrigWindowCfg::event() const
{
    return mEvent;
}

void TrigWindowCfg::setEvent(const WindowEvent &event)
{
    mEvent = event;
}

EMoreThan TrigWindowCfg::cmp() const
{
    return mCmp;
}

void TrigWindowCfg::setCmp(const EMoreThan &cmp)
{
    mCmp = cmp;
}

qint64 TrigWindowCfg::widthH() const
{
    return mWidthH;
}

void TrigWindowCfg::setWidthH(const qint64 &widthH)
{
    mWidthH = widthH;
}

qint64 TrigWindowCfg::widthL() const
{
    return mWidthL;
}

void TrigWindowCfg::setWidthL(const qint64 &widthL)
{
    mWidthL = widthL;
}

Chan TrigWindowCfg::ch() const
{
    return mCh;
}

void TrigWindowCfg::setCh(const Chan &ch)
{
    mCh = ch;
}

Chan TrigDelayCfg::chB() const
{
    return mChB;
}

void TrigDelayCfg::setChB(const Chan &chB)
{
    mChB = chB;
}

EdgeSlope TrigDelayCfg::slopeA() const
{
    return mSlopeA;
}

void TrigDelayCfg::setSlopeA(const EdgeSlope &slopeA)
{
    mSlopeA = slopeA;
}

EdgeSlope TrigDelayCfg::slopeB() const
{
    return mSlopeB;
}

void TrigDelayCfg::setSlopeB(const EdgeSlope &slopeB)
{
    mSlopeB = slopeB;
}

EMoreThan TrigDelayCfg::cmp() const
{
    return mCmp;
}

void TrigDelayCfg::setCmp(const EMoreThan &cmp)
{
    mCmp = cmp;
}

qint64 TrigDelayCfg::widthL() const
{
    return mWidthL;
}

void TrigDelayCfg::setWidthL(qint64 widthL)
{
    mWidthL = widthL;
}

qint64 TrigDelayCfg::widthH() const
{
    return mWidthH;
}

void TrigDelayCfg::setWidthH(qint64 widthH)
{
    mWidthH = widthH;
}

Chan TrigDelayCfg::chA() const
{
    return mChA;
}

void TrigDelayCfg::setChA(const Chan &chA)
{
    mChA = chA;
}

int TrigRS232Cfg::baudRate() const
{
    return mBaudRate;
}

void TrigRS232Cfg::setBaudRate(int baudRate)
{
    mBaudRate = baudRate;
}

bool TrigRS232Cfg::getMbInvert() const
{
    return mbInvert;
}

void TrigRS232Cfg::setMbInvert(bool value)
{
    mbInvert = value;
}

Trigger_RS232_Parity TrigRS232Cfg::getParity() const
{
    return mParity;
}

void TrigRS232Cfg::setParity(const Trigger_RS232_Parity &parity)
{
    mParity = parity;
}

Trigger_RS232_Stop TrigRS232Cfg::getStop() const
{
    return mStop;
}

void TrigRS232Cfg::setStop(const Trigger_RS232_Stop &stop)
{
    mStop = stop;
}

Trigger_RS232_Width TrigRS232Cfg::getWidth() const
{
    return mWidth;
}

void TrigRS232Cfg::setWidth(const Trigger_RS232_Width &width)
{
    mWidth = width;
}

Trigger_RS232_Event TrigRS232Cfg::getEvent() const
{
    return mEvent;
}

void TrigRS232Cfg::setEvent(const Trigger_RS232_Event &event)
{
    mEvent = event;
}

Trigger_value_cmp TrigRS232Cfg::getDataCmp() const
{
    return mDataCmp;
}

void TrigRS232Cfg::setDataCmp(const Trigger_value_cmp &dataCmp)
{
    mDataCmp = dataCmp;
}

Triger_RS232_Error TrigRS232Cfg::getErr() const
{
    return mErr;
}

void TrigRS232Cfg::setErr(const Triger_RS232_Error &err)
{
    mErr = err;
}

QByteArray TrigRS232Cfg::getDatas() const
{
    return mDatas;
}

void TrigRS232Cfg::setDatas(QByteArray datas)
{
    mDatas = datas;
}

Chan TrigRS232Cfg::ch() const
{
    return mCh;
}

void TrigRS232Cfg::setCh(const Chan &ch)
{
    mCh = ch;
}

TriggerPulsePolarity TrigTimeoutCfg::polarity() const
{
    return mPolarity;
}

void TrigTimeoutCfg::setPolarity(const TriggerPulsePolarity &polarity)
{
    mPolarity = polarity;
}

qint64 TrigTimeoutCfg::width() const
{
    return mWidth;
}

void TrigTimeoutCfg::setWidth(qint64 width)
{
    mWidth = width;
}

Chan TrigTimeoutCfg::ch() const
{
    return mCh;
}

void TrigTimeoutCfg::setCh(const Chan &ch)
{
    mCh = ch;
}

TriggerPulsePolarity TrigVideoCfg::polarity() const
{
    return mPolarity;
}

void TrigVideoCfg::setPolarity(const TriggerPulsePolarity &polarity)
{
    mPolarity = polarity;
}

Trigger_Video_Format TrigVideoCfg::fromat() const
{
    return mFromat;
}

void TrigVideoCfg::setFromat(const Trigger_Video_Format &fromat)
{
    mFromat = fromat;
}

Trigger_Video_Sync TrigVideoCfg::sync() const
{
    return mSync;
}

void TrigVideoCfg::setSync(const Trigger_Video_Sync &sync)
{
    mSync = sync;
}

int TrigVideoCfg::lineN() const
{
    return mLineN;
}

void TrigVideoCfg::setLineN(int lineN)
{
    mLineN = lineN;
}

Chan TrigVideoCfg::ch() const
{
    return mCh;
}

void TrigVideoCfg::setCh(const Chan &ch)
{
    mCh = ch;
}

Chan TrigIICcfg::data() const
{
    return mData;
}

void TrigIICcfg::setData(const Chan &data)
{
    mData = data;
}

Trigger_I2C_Spec TrigIICcfg::spec() const
{
    return mSpec;
}

void TrigIICcfg::setSpec(const Trigger_I2C_Spec &spec)
{
    mSpec = spec;
}

Trigger_I2C_When TrigIICcfg::when() const
{
    return mWhen;
}

void TrigIICcfg::setWhen(const Trigger_I2C_When &when)
{
    mWhen = when;
}

Trigger_I2C_WR TrigIICcfg::wr() const
{
    return mWr;
}

void TrigIICcfg::setWr(const Trigger_I2C_WR &wr)
{
    mWr = wr;
}

Trigger_value_cmp TrigIICcfg::addrCmp() const
{
    return mAddrCmp;
}

void TrigIICcfg::setAddrCmp(const Trigger_value_cmp &addrCmp)
{
    mAddrCmp = addrCmp;
}

Trigger_value_cmp TrigIICcfg::datCmp() const
{
    return mDatCmp;
}

void TrigIICcfg::setDatCmp(const Trigger_value_cmp &datCmp)
{
    mDatCmp = datCmp;
}

Trigger_I2C_Width TrigIICcfg::width() const
{
    return mWidth;
}

void TrigIICcfg::setWidth(const Trigger_I2C_Width &width)
{
    mWidth = width;
}

quint32 TrigIICcfg::addrMin() const
{
    return mAddrMin;
}

void TrigIICcfg::setAddrMin(const quint32 &addrMin)
{
    mAddrMin = addrMin;
}

quint32 TrigIICcfg::addrMax() const
{
    return mAddrMax;
}

void TrigIICcfg::setAddrMax(const quint32 &addrMax)
{
    mAddrMax = addrMax;
}

quint32 TrigIICcfg::addrMask() const
{
    return mAddrMask;
}

void TrigIICcfg::setAddrMask(const quint32 &addrMask)
{
    mAddrMask = addrMask;
}

QByteArray TrigIICcfg::dataMin() const
{
    return mDataMin;
}

void TrigIICcfg::setDataMin(const QByteArray &dataMin)
{
    mDataMin = dataMin;
}

QByteArray TrigIICcfg::dataMax() const
{
    return mDataMax;
}

void TrigIICcfg::setDataMax(const QByteArray &dataMax)
{
    mDataMax = dataMax;
}

QByteArray TrigIICcfg::dataMask() const
{
    return mDataMask;
}

void TrigIICcfg::setDataMask(const QByteArray &dataMask)
{
    mDataMask = dataMask;
}

Chan TrigIICcfg::clk() const
{
    return mClk;
}

void TrigIICcfg::setClk(const Chan &clk)
{
    mClk = clk;
}

Chan TrigSpiCfg::data() const
{
    return mData;
}

void TrigSpiCfg::setData(const Chan &data)
{
    mData = data;
}

Chan TrigSpiCfg::cs() const
{
    return mCs;
}

void TrigSpiCfg::setCs(const Chan &cs)
{
    mCs = cs;
}


Trigger_Spi_CS TrigSpiCfg::getType() const
{
    return mType;
}

void TrigSpiCfg::setType(const Trigger_Spi_CS &type)
{
    mType = type;
}

quint64 TrigSpiCfg::getTmo() const
{
    return mTmo;
}

void TrigSpiCfg::setTmo(const quint64 &tmo)
{
    mTmo = tmo;
}

Trigger_value_cmp TrigSpiCfg::getDatCmp() const
{
    return mDatCmp;
}

void TrigSpiCfg::setDatCmp(const Trigger_value_cmp &datCmp)
{
    mDatCmp = datCmp;
}

quint32 TrigSpiCfg::getDataLen() const
{
    return mDataLen;
}

void TrigSpiCfg::setDataLen(const quint32 &dataLen)
{
    mDataLen = dataLen;
}

/*
QByteArray TrigSpiCfg::getDataMin() const
{
    return mDataMin;
}

void TrigSpiCfg::setDataMin(const QByteArray &dataMin)
{
    mDataMin = dataMin;
}

QByteArray TrigSpiCfg::getDataMax() const
{
    return mDataMax;
}

void TrigSpiCfg::setDataMax(const QByteArray &dataMax)
{
    mDataMax = dataMax;
}

QByteArray TrigSpiCfg::getDataMask() const
{
    return mDataMask;
}

void TrigSpiCfg::setDataMask(const QByteArray &dataMask)
{
    mDataMask = dataMask;
}
*/

EdgeSlope TrigSpiCfg::getClkSlope() const
{
    return mClkSlope;
}

void TrigSpiCfg::setClkSlope(const EdgeSlope &clkSlope)
{
    mClkSlope = clkSlope;
}

EdgeSlope TrigSpiCfg::getCsSlope() const
{
    return mCsSlope;
}

void TrigSpiCfg::setCsSlope(const EdgeSlope &csSlope)
{
    mCsSlope = csSlope;
}

QBitArray TrigSpiCfg::getDataMin() const
{
    return mDataMin;
}

void TrigSpiCfg::setDataMin(const QBitArray &dataMin)
{
    mDataMin = dataMin;
}

QBitArray TrigSpiCfg::getDataMax() const
{
    return mDataMax;
}

void TrigSpiCfg::setDataMax(const QBitArray &dataMax)
{
    mDataMax = dataMax;
}

QBitArray TrigSpiCfg::getDataMask() const
{
    return mDataMask;
}

void TrigSpiCfg::setDataMask(const QBitArray &dataMask)
{
    mDataMask = dataMask;
}

Chan TrigSpiCfg::clk() const
{
    return mClk;
}

void TrigSpiCfg::setClk(const Chan &clk)
{
    mClk = clk;
}

TriggerPulsePolarity TrigDurationCfg::polariy() const
{
    return mPolariy;
}

void TrigDurationCfg::setPolariy(const TriggerPulsePolarity &polariy)
{
    mPolariy = polariy;
}

EMoreThan TrigDurationCfg::cmp() const
{
    return mCmp;
}

void TrigDurationCfg::setCmp(const EMoreThan &cmp)
{
    mCmp = cmp;
}

qint64 TrigDurationCfg::widthH() const
{
    return mWidthH;
}

void TrigDurationCfg::setWidthH(const qint64 &widthH)
{
    mWidthH = widthH;
}

qint64 TrigDurationCfg::widthL() const
{
    return mWidthL;
}

void TrigDurationCfg::setWidthL(const qint64 &widthL)
{
    mWidthL = widthL;
}

Chan TrigDurationCfg::ch() const
{
    return mCh;
}

void TrigDurationCfg::setCh(const Chan &ch)
{
    mCh = ch;
}

void TrigABCfg::checkCfg()
{

}

TriggerMode TrigABCfg::getaType() const
{
    return maType;
}

void TrigABCfg::setaType(const TriggerMode &value)
{
    maType = value;
}

TriggerMode TrigABCfg::getbType() const
{
    return mbType;
}

void TrigABCfg::setbType(const TriggerMode &value)
{
    mbType = value;
}

int TrigABCfg::getbCount() const
{
    return mbCount;
}

void TrigABCfg::setbCount(int value)
{
    mbCount = value;
}


qint64 TrigABCfg::getDelay() const
{
    return mDelay;
}

void TrigABCfg::setDelay(const qint64 &delay)
{
    mDelay = delay;
}



int TrigCanCfg::getByteCount() const
{
    return mByteCount;
}

void TrigCanCfg::setByteCount(int value)
{
    mByteCount = value;
}

Chan TrigCanCfg::ch() const
{
    return mCh;
}

void TrigCanCfg::setCh(const Chan &ch)
{
    mCh = ch;
}

int TrigCanCfg::baud() const
{
    return mBaud;
}

void TrigCanCfg::setBaud(int baud)
{
    mBaud = baud;
}

Trigger_Can_Phy TrigCanCfg::phy() const
{
    return mPhy;
}

void TrigCanCfg::setPhy(const Trigger_Can_Phy &phy)
{
    mPhy = phy;
}

Trigger_Can_Spec TrigCanCfg::spec() const
{
    return mSpec;
}

void TrigCanCfg::setSpec(const Trigger_Can_Spec &spec)
{
    mSpec = spec;
}

Trigger_Can_When TrigCanCfg::when() const
{
    return mWhen;
}

void TrigCanCfg::setWhen(const Trigger_Can_When &when)
{
    mWhen = when;
}

Trigger_Can_Field TrigCanCfg::field() const
{
    return mField;
}

void TrigCanCfg::setField(const Trigger_Can_Field &field)
{
    mField = field;
}

Trigger_Can_Frame TrigCanCfg::frame() const
{
    return mFrame;
}

void TrigCanCfg::setFrame(const Trigger_Can_Frame &frame)
{
    mFrame = frame;
}

Trigger_Can_Err TrigCanCfg::err() const
{
    return mErr;
}

void TrigCanCfg::setErr(const Trigger_Can_Err &err)
{
    mErr = err;
}

Trigger_value_cmp TrigCanCfg::idCmp() const
{
    return mIdCmp;
}

void TrigCanCfg::setIdCmp(const Trigger_value_cmp &idCmp)
{
    mIdCmp = idCmp;
}

bool TrigCanCfg::idCmpMask() const
{
    return mIdCmpMask;
}

void TrigCanCfg::setIdCmpMask(bool idCmpMask)
{
    mIdCmpMask = idCmpMask;
}


Trigger_value_cmp TrigCanCfg::datCmp() const
{
    return mDatCmp;
}

void TrigCanCfg::setDatCmp(const Trigger_value_cmp &datCmp)
{
    mDatCmp = datCmp;
}

bool TrigCanCfg::datCmpMask() const
{
    return mDatCmpMask;
}

void TrigCanCfg::setDatCmpMask(bool datCmpMask)
{
    mDatCmpMask = datCmpMask;
}

int TrigCanCfg::saPos() const
{
    return mSaPos;
}

void TrigCanCfg::setSaPos(int saPos)
{
    mSaPos = saPos;
}

quint32 TrigCanCfg::idMin() const
{
    return mIdMin;
}

void TrigCanCfg::setIdMin(const quint32 &idMin)
{
    mIdMin = idMin;
}

quint32 TrigCanCfg::idMax() const
{
    return mIdMax;
}

void TrigCanCfg::setIdMax(const quint32 &idMax)
{
    mIdMax = idMax;
}

quint32 TrigCanCfg::idMask() const
{
    return mIdMask;
}

void TrigCanCfg::setIdMask(const quint32 &idMask)
{
    mIdMask = idMask;
}

QByteArray TrigCanCfg::datMin() const
{
    return mDatMin;
}

void TrigCanCfg::setDatMin(const QByteArray &datMin)
{
    mDatMin = datMin;
}

QByteArray TrigCanCfg::datMax() const
{
    return mDatMax;
}

void TrigCanCfg::setDatMax(const QByteArray &datMax)
{
    mDatMax = datMax;
}

QByteArray TrigCanCfg::datMask() const
{
    return mDatMask;
}

void TrigCanCfg::setDatMask(const QByteArray &datMask)
{
    mDatMask = datMask;
}

quint32 TrigLinCfg::clk() const
{
    return mClk;
}

void TrigLinCfg::setClk(const quint32 &clk)
{
    mClk = clk;
}

Trigger_Lin_Ver TrigLinCfg::ver() const
{
    return mVer;
}

void TrigLinCfg::setVer(const Trigger_Lin_Ver &ver)
{
    mVer = ver;
}

Trigger_Lin_When TrigLinCfg::when() const
{
    return mWhen;
}

void TrigLinCfg::setWhen(const Trigger_Lin_When &when)
{
    mWhen = when;
}

Trigger_value_cmp TrigLinCfg::idCmp() const
{
    return mIdCmp;
}

void TrigLinCfg::setIdCmp(const Trigger_value_cmp &idCmp)
{
    mIdCmp = idCmp;
}

Trigger_value_cmp TrigLinCfg::datCmp() const
{
    return mDatCmp;
}

void TrigLinCfg::setDatCmp(const Trigger_value_cmp &datCmp)
{
    mDatCmp = datCmp;
}

Trigger_Lin_Err TrigLinCfg::err() const
{
    return mErr;
}

void TrigLinCfg::setErr(const Trigger_Lin_Err &err)
{
    mErr = err;
}

quint32 TrigLinCfg::idMin() const
{
    return mIdMin;
}

void TrigLinCfg::setIdMin(const quint32 &idMin)
{
    mIdMin = idMin;
}

quint32 TrigLinCfg::idMax() const
{
    return mIdMax;
}

void TrigLinCfg::setIdMax(const quint32 &idMax)
{
    mIdMax = idMax;
}

int TrigLinCfg::saPos() const
{
    return mSaPos;
}

void TrigLinCfg::setSaPos(int saPos)
{
    mSaPos = saPos;
}

QByteArray TrigLinCfg::minBits() const
{
    return mMinBits;
}

void TrigLinCfg::setMinBits(const QByteArray &minBits)
{
    mMinBits = minBits;
}

QByteArray TrigLinCfg::maxBits() const
{
    return mMaxBits;
}

void TrigLinCfg::setMaxBits(const QByteArray &maxBits)
{
    mMaxBits = maxBits;
}

QByteArray TrigLinCfg::maskBits() const
{
    return mMaskBits;
}

void TrigLinCfg::setMaskBits(const QByteArray &maskBits)
{
    mMaskBits = maskBits;
}

int TrigLinCfg::byteCount() const
{
    return mByteCount;
}

void TrigLinCfg::setByteCount(int byteCount)
{
    mByteCount = byteCount;
}

Chan TrigLinCfg::ch() const
{
    return mCh;
}

void TrigLinCfg::setCh(const Chan &ch)
{
    mCh = ch;
}


Chan TrigI2SCfg::ws() const
{
    return mWs;
}

void TrigI2SCfg::setWs(const Chan &ws)
{
    mWs = ws;
}

Chan TrigI2SCfg::sda() const
{
    return mSda;
}

void TrigI2SCfg::setSda(const Chan &sda)
{
    mSda = sda;
}

EdgeSlope TrigI2SCfg::slope() const
{
    return mSlope;
}

void TrigI2SCfg::setSlope(const EdgeSlope &slope)
{
    mSlope = slope;
}

Trigger_IIS_Ch TrigI2SCfg::line() const
{
    return mLine;
}

void TrigI2SCfg::setLine(const Trigger_IIS_Ch &line)
{
    mLine = line;
}

Trigger_IIS_Spec TrigI2SCfg::spec() const
{
    return mSpec;
}

void TrigI2SCfg::setSpec(const Trigger_IIS_Spec &spec)
{
    mSpec = spec;
}

Trigger_IIS_data_cmp TrigI2SCfg::dataCmp() const
{
    return mDataCmp;
}

void TrigI2SCfg::setDataCmp(const Trigger_IIS_data_cmp &dataCmp)
{
    mDataCmp = dataCmp;
}

quint32 TrigI2SCfg::usedWidth() const
{
    return mUsedWidth;
}

void TrigI2SCfg::setUsedWidth(const quint32 &usedWidth)
{
    mUsedWidth = usedWidth;
}

quint32 TrigI2SCfg::width() const
{
    return mWidth;
}

void TrigI2SCfg::setWidth(const quint32 &width)
{
    mWidth = width;
}

quint32 TrigI2SCfg::minData() const
{
    return mMinData;
}

void TrigI2SCfg::setMinData(const quint32 &minData)
{
    mMinData = minData;
}

quint32 TrigI2SCfg::maxData() const
{
    return mMaxData;
}

void TrigI2SCfg::setMaxData(const quint32 &maxData)
{
    mMaxData = maxData;
}

quint32 TrigI2SCfg::mask() const
{
    return mMask;
}

void TrigI2SCfg::setMask(const quint32 &mask)
{
    mMask = mask;
}

Chan TrigI2SCfg::sclk() const
{
    return mSclk;
}

void TrigI2SCfg::setSclk(const Chan &sclk)
{
    mSclk = sclk;
}

Trigger_Flex_Baud TrigFlexrayCfg::baud() const
{
    return mBaud;
}

void TrigFlexrayCfg::setBaud(const Trigger_Flex_Baud &baud)
{
    mBaud = baud;
}

Trigger_Flex_Phy TrigFlexrayCfg::phy() const
{
    return mPhy;
}

void TrigFlexrayCfg::setPhy(const Trigger_Flex_Phy &phy)
{
    mPhy = phy;
}

Trigger_Flex_When TrigFlexrayCfg::when() const
{
    return mWhen;
}

void TrigFlexrayCfg::setWhen(const Trigger_Flex_When &when)
{
    mWhen = when;
}

Trigger_Flex_Pos TrigFlexrayCfg::pos() const
{
    return mPos;
}

void TrigFlexrayCfg::setPos(const Trigger_Flex_Pos &pos)
{
    mPos = pos;
}

Trigger_Flex_Frame TrigFlexrayCfg::frame() const
{
    return mFrame;
}

void TrigFlexrayCfg::setFrame(const Trigger_Flex_Frame &frame)
{
    mFrame = frame;
}

Trigger_Flex_Symbol TrigFlexrayCfg::symbol() const
{
    return mSymbol;
}

void TrigFlexrayCfg::setSymbol(const Trigger_Flex_Symbol &symbol)
{
    mSymbol = symbol;
}

Trigger_Flex_Err TrigFlexrayCfg::err() const
{
    return mErr;
}

void TrigFlexrayCfg::setErr(const Trigger_Flex_Err &err)
{
    mErr = err;
}

Trigger_value_cmp TrigFlexrayCfg::idCmp() const
{
    return mIdCmp;
}

void TrigFlexrayCfg::setIdCmp(const Trigger_value_cmp &idCmp)
{
    mIdCmp = idCmp;
}

bool TrigFlexrayCfg::idCmpMask() const
{
    return mIdCmpMask;
}

void TrigFlexrayCfg::setIdCmpMask(bool idCmpMask)
{
    mIdCmpMask = idCmpMask;
}

Trigger_value_cmp TrigFlexrayCfg::cycleCmp() const
{
    return mCycleCmp;
}

void TrigFlexrayCfg::setCycleCmp(const Trigger_value_cmp &cycleCmp)
{
    mCycleCmp = cycleCmp;
}

bool TrigFlexrayCfg::cycleCmpMask() const
{
    return mCycleCmpMask;
}

void TrigFlexrayCfg::setCycleCmpMask(bool cycleCmpMask)
{
    mCycleCmpMask = cycleCmpMask;
}

int TrigFlexrayCfg::tssTransmitter() const
{
    return mTssTransmitter;
}

void TrigFlexrayCfg::setTssTransmitter(int tssTransmitter)
{
    mTssTransmitter = tssTransmitter;
}

int TrigFlexrayCfg::casLowMax() const
{
    return mCasLowMax;
}

void TrigFlexrayCfg::setCasLowMax(int casLowMax)
{
    mCasLowMax = casLowMax;
}

quint32 TrigFlexrayCfg::idMin() const
{
    return mIdMin;
}

void TrigFlexrayCfg::setIdMin(const quint32 &idMin)
{
    mIdMin = idMin;
}

quint32 TrigFlexrayCfg::idMax() const
{
    return mIdMax;
}

void TrigFlexrayCfg::setIdMax(const quint32 &idMax)
{
    mIdMax = idMax;
}

quint32 TrigFlexrayCfg::idMask() const
{
    return mIdMask;
}

void TrigFlexrayCfg::setIdMask(const quint32 &idMask)
{
    mIdMask = idMask;
}

quint32 TrigFlexrayCfg::cycleMin() const
{
    return mCycleMin;
}

void TrigFlexrayCfg::setCycleMin(const quint32 &cycleMin)
{
    mCycleMin = cycleMin;
}

quint32 TrigFlexrayCfg::cycleMax() const
{
    return mCycleMax;
}

void TrigFlexrayCfg::setCycleMax(const quint32 &cycleMax)
{
    mCycleMax = cycleMax;
}

quint32 TrigFlexrayCfg::cycleMask() const
{
    return mCycleMask;
}

void TrigFlexrayCfg::setCycleMask(const quint32 &cycleMask)
{
    mCycleMask = cycleMask;
}

Chan TrigFlexrayCfg::ch() const
{
    return mCh;
}

void TrigFlexrayCfg::setCh(const Chan &ch)
{
    mCh = ch;
}

Trigger_1553_When TrigStd1553bCfg::when() const
{
    return mWhen;
}

void TrigStd1553bCfg::setWhen(const Trigger_1553_When &when)
{
    mWhen = when;
}

Trigger_1553_Sync TrigStd1553bCfg::sync() const
{
    return mSync;
}

void TrigStd1553bCfg::setSync(const Trigger_1553_Sync &sync)
{
    mSync = sync;
}

Trigger_1553_Err TrigStd1553bCfg::err() const
{
    return mErr;
}

void TrigStd1553bCfg::setErr(const Trigger_1553_Err &err)
{
    mErr = err;
}

Trigger_value_cmp TrigStd1553bCfg::datCmp() const
{
    return mDatCmp;
}

void TrigStd1553bCfg::setDatCmp(const Trigger_value_cmp &datCmp)
{
    mDatCmp = datCmp;
}

Trigger_value_cmp TrigStd1553bCfg::rmtAddrCmp() const
{
    return mRmtAddrCmp;
}

void TrigStd1553bCfg::setRmtAddrCmp(const Trigger_value_cmp &rmtAddrCmp)
{
    mRmtAddrCmp = rmtAddrCmp;
}

Trigger_value_cmp TrigStd1553bCfg::subAddrCmp() const
{
    return mSubAddrCmp;
}

void TrigStd1553bCfg::setSubAddrCmp(const Trigger_value_cmp &subAddrCmp)
{
    mSubAddrCmp = subAddrCmp;
}

Trigger_value_cmp TrigStd1553bCfg::cntCodeCmp() const
{
    return mCntCodeCmp;
}

void TrigStd1553bCfg::setCntCodeCmp(const Trigger_value_cmp &cntCodeCmp)
{
    mCntCodeCmp = cntCodeCmp;
}

quint32 TrigStd1553bCfg::cmpMin() const
{
    return mCmpMin;
}

void TrigStd1553bCfg::setCmpMin(const quint32 &cmpMin)
{
    mCmpMin = cmpMin;
}

quint32 TrigStd1553bCfg::cmpMax() const
{
    return mCmpMax;
}

void TrigStd1553bCfg::setCmpMax(const quint32 &cmpMax)
{
    mCmpMax = cmpMax;
}

quint32 TrigStd1553bCfg::cmpMask() const
{
    return mCmpMask;
}

void TrigStd1553bCfg::setCmpMask(const quint32 &cmpMask)
{
    mCmpMask = cmpMask;
}

TriggerPulsePolarity TrigStd1553bCfg::polarity() const
{
    return mPolarity;
}

void TrigStd1553bCfg::setPolarity(const TriggerPulsePolarity &polarity)
{
    mPolarity = polarity;
}


quint32 TrigStd1553bCfg::csData() const
{
    return mCsData;
}

void TrigStd1553bCfg::setCsData(const quint32 &csData)
{
    mCsData = csData;
}

quint32 TrigStd1553bCfg::csMask() const
{
    return mCsMask;
}

void TrigStd1553bCfg::setCsMask(const quint32 &csMask)
{
    mCsMask = csMask;
}

Chan TrigStd1553bCfg::ch() const
{
    return mCh;
}

void TrigStd1553bCfg::setCh(const Chan &ch)
{
    mCh = ch;
}
