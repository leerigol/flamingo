
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mPatternCfg

void dsoEngine::enterTrigPattern()
{}
void dsoEngine::exitTrigPattern()
{}

DsoErr dsoEngine::setTrigPatternCfg( TrigPatternCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigPatternCfg( selfTrigCfg, getTpu() );
    return ERR_NONE;
}

void dsoEngine::applyTrigPatternCfg( TrigPatternCfg &cfg, IPhyTpu *pTpu )
{
    quint64 bm64 = 0;
    int shift    = 0;
    IPhyTpu::eLogicSel logicSel;

    //! 0---------------
    //! ch1~ch4 la0~la15 ext
    for ( int i = 0; i < array_count(cfg.mPatterns); i++ )
    {
        logicSel =  toLogicSel( cfg.mOperator, cfg.mPatterns[i] ) ;

        bm64    +=  ((quint64)logicSel & 0x03)<<shift;

        shift += 2;
        //qDebug("(%d: %X, %x)", i, cfg.mPatterns[i], logicSel);
    }

    //! set tpu
    pTpu->rstIir();

    //! dat
    pTpu->setA_event_reg1_A_event_logic_la_sel( bm64 & 0xffffffff );
    //qDebug()<<__FILE__<<__LINE__<<"bm64:"<<bm64<<"CH1-D11:"<<( bm64 & 0xffffffff );

    pTpu->setA_event_reg2_A_event_logic_analog_sel( (bm64 >> 32) & 0x3ff );
    //qDebug()<<__FILE__<<__LINE__<<"bm64:"<<bm64<<"D12-D15:"<<( (bm64 >> 32) & 0x3ff );

    pTpu->aEventLogicFunc( toLogicOp(cfg.mOperator) );
    pTpu->setEventDat( IPhyTpu::event_a, IPhyTpu::logic );

    //! for clock
    int clkSeq;
    int clockCnt = selectPatternEdge( cfg, &clkSeq );
    if ( clockCnt >= 1 )
    {
        //! only one edge
        Q_ASSERT(clockCnt == 1);

        IPhyTpu::eCmpSrc src = toaCmpSrc( (IPhyTpu::eChSeq)clkSeq );

        pTpu->aEventCmpa( src, cfg.mPatterns[clkSeq] == Trigger_pat_rise ? IPhyTpu::rise : IPhyTpu::fall);

        pTpu->setEventClk( IPhyTpu::event_a,
                           IPhyTpu::cmp_a,
                           cfg.mPatterns[clkSeq] == Trigger_pat_rise ? 1 : 0
                           );

        pTpu->aEventWidth();

    }
    else
    {
        //! clk = dat
        pTpu->setEventClk( IPhyTpu::event_a, IPhyTpu::logic );

        pTpu->aEventWidth();
    }

    pTpu->setTrigSrc( IPhyTpu::a );
}

int dsoEngine::selectPatternEdge( TrigPatternCfg &cfg,
                                  int *pFirstEdge )
{
    Q_ASSERT( NULL != pFirstEdge );
    *pFirstEdge = -1;

    int edgeCnt = 0;
    for ( int i = 0; i < array_count(cfg.mPatterns); i++ )
    {
        if ( cfg.mPatterns[i] == Trigger_pat_fall
             || cfg.mPatterns[i] == Trigger_pat_rise )
        {
            //! save the first edge
            if ( (*pFirstEdge) == -1 )
            {
                (*pFirstEdge) = i;
            }

            edgeCnt++;
        }
    }

    return edgeCnt;
}
