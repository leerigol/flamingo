#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mFlexrayCfg

DsoErr dsoEngine::setTrigFlexrayCfg( TrigFlexrayCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigFlexrayCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigFlexrayCfg(TrigFlexrayCfg &cfg, IPhyTpu *pTpu)
{
    //!reset
    pTpu->rstIir();

    //! src
    pTpu->rEventCmpa(toaCmpSrc(cfg.mCh), IPhyTpu::rise);
    pTpu->rEventCmpb(toaCmpSrc(cfg.mCh), IPhyTpu::rise);
    pTpu->rEventClkDat(IPhyTpu::cmp_a, false,
                       IPhyTpu::cmp_a, false
                       );
    pTpu->setTrigSrc(IPhyTpu::flexray);

    int baud =  0;
    switch (cfg.mBaud) {
    case trig_flex_2_5M:
        baud = 0;
        break;
    case trig_flex_5M:
        baud = 1;
        break;
    case trig_flex_10M:
        baud = 2;
        break;
    default:
        baud = 0;
        break;
    }

    pTpu->setflexray_cfg_1_flexray_trig_type(              cfg.mWhen       );
    pTpu->setflexray_cfg_1_flexray_trig_type_location(     cfg.mPos        );
    pTpu->setflexray_cfg_1_flexray_trig_type_frame(        cfg.mFrame      );
    pTpu->setflexray_cfg_1_flexray_trig_type_symbol(       cfg.mSymbol     );
    pTpu->setflexray_cfg_1_flexray_trig_type_err(          cfg.mErr        );
    pTpu->setflexray_cfg_1_flexray_trig_type_id(           cfg.mIdCmp      );
    pTpu->setflexray_cfg_1_flexray_trig_type_id_mask(      cfg.mIdCmpMask  );
    pTpu->setflexray_cfg_1_flexray_trig_type_cyccount(     cfg.mCycleCmp   );
    pTpu->setflexray_cfg_1_flexray_trig_type_cyc_mask(     cfg.mCycleCmpMask);
    pTpu->setflexray_cfg_1_flexray_trig_rate_sel(          baud            );
    pTpu->setflexray_cfg_1_flexray_trig_channel_sel(       cfg.mPhy        );
    pTpu->setflexray_cfg_1_flexray_trig_gdTSSTransmitter(  cfg.mTssTransmitter);
    pTpu->setflexray_cfg_1_flexray_trig_gdCASxLowMax(      cfg.mCasLowMax  );
    pTpu->setflexray_cfg_2_flexray_trig_frame_id_min(      cfg.mIdMin      );
    pTpu->setflexray_cfg_2_flexray_trig_frame_id_max(      cfg.mIdMax      );
    pTpu->setflexray_cfg_3_flexray_trig_frame_id_mask(     cfg.mIdMask     );
    pTpu->setflexray_cfg_4_flexray_trig_frame_cycount_min( cfg.mCycleMin   );
    pTpu->setflexray_cfg_4_flexray_trig_frame_cycount_max( cfg.mCycleMax   );
    pTpu->setflexray_cfg_4_flexray_trig_frame_cycount_mask(cfg.mCycleMask  );
}
