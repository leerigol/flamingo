
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mWindowCfg

void dsoEngine::enterTrigWindow()
{}
void dsoEngine::exitTrigWindow()
{}

DsoErr dsoEngine::setTrigWindowCfg( TrigWindowCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigWindowCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}
void dsoEngine::applyTrigWindowCfg( TrigWindowCfg &cfg, IPhyTpu *pTpu )
{
    pTpu->rstIir();

//    Trigger_window_enter,
//    Trigger_window_exit,
//    Trigger_window_in_width,
//    Trigger_window_out_width,
    if ( cfg.mEvent == Trigger_window_enter
         || cfg.mEvent == Trigger_window_exit )
    {
        pTpu->aEventCmpa( toaCmpSrc_a(cfg.mCh));
        pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                            IPhyTpu::cmp_a, false );

        pTpu->bEventCmpa( toaCmpSrc_b(cfg.mCh) );
        pTpu->bEventClkDat( IPhyTpu::cmp_a, true,
                            IPhyTpu::cmp_a, true );
    }
    else if ( cfg.mEvent == Trigger_window_exit
         || cfg.mEvent == Trigger_window_in_width )
    {
        pTpu->aEventCmpa( toaCmpSrc_a(cfg.mCh));
        pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                            IPhyTpu::cmp_a, true );

        pTpu->bEventCmpa( toaCmpSrc_b(cfg.mCh) );
        pTpu->bEventClkDat( IPhyTpu::cmp_a, false,
                            IPhyTpu::cmp_a, false );
    }
    else
    { Q_ASSERT(false); }

    if ( cfg.mEvent == Trigger_window_enter
         || cfg.mEvent == Trigger_window_exit )
    {
        pTpu->aEventWidth();
        pTpu->bEventWidth();
    }
    else if ( cfg.mEvent == Trigger_window_in_width
              || cfg.mEvent == Trigger_window_out_width )
    {
        pTpu->aEventWidth( toWidthCmp(cfg.mCmp),
                           cfg.mWidthL, cfg.mWidthH );
        pTpu->bEventWidth( toWidthCmp(cfg.mCmp),
                           cfg.mWidthL, cfg.mWidthH );
    }
    else
    { Q_ASSERT(false); }

    pTpu->setTrigSrc( IPhyTpu::a_or_b );

}
