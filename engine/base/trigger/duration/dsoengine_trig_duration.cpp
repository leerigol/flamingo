
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mDurationCfg

void dsoEngine::enterTrigDuration()
{}
void dsoEngine::exitTrigDuration()
{}
DsoErr dsoEngine::setTrigDurationCfg( TrigDurationCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigDurationCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}
void dsoEngine::applyTrigDurationCfg( TrigDurationCfg &cfg, IPhyTpu *pTpu )
{
    applyTrigPatternCfg( cfg, pTpu );

    pTpu->aEventWidth( toWidthCmp(cfg.mCmp),
                       cfg.mWidthL,
                       cfg.mWidthH
                       );
}


