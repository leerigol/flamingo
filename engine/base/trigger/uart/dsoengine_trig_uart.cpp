#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mRS232Cfg

DsoErr dsoEngine::setTrigUartCfg( TrigRS232Cfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigUartCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigUartCfg( TrigRS232Cfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.mCh);
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();   

    pTpu->rEventCmpa(toaCmpSrc(cfg.mCh), IPhyTpu::rise);
    pTpu->rEventCmpb(toaCmpSrc(cfg.mCh), IPhyTpu::rise);

    pTpu->rEventClkDat(IPhyTpu::logic, false,
                       IPhyTpu::cmp_a, cfg.mbInvert
                       );

    pTpu->setTrigSrc( IPhyTpu::uart );

    //!uart
    pTpu->setuart_cfg_1_uart_clk_div( time_s(1)/( cfg.mBaudRate * pTpu->get_td_tick() ) );

    /*qDebug()<<__FILE__<<__LINE__
            <<"baud:"<<cfg.mBaudRate
            <<"tick:"<<pTpu->get_td_tick()
            <<"reg x:"<<time_s(1)/( cfg.mBaudRate * pTpu->get_td_tick() );*/

    pTpu->setuart_cfg_1_uart_trig_type(cfg.mEvent);

    pTpu->setuart_cfg_2_uart_len(cfg.mWidth);
    pTpu->setuart_cfg_1_uart_parity(cfg.mParity);
    pTpu->setuart_cfg_1_uart_trig_err(cfg.mErr);
    pTpu->setuart_cfg_1_uart_eof(cfg.mStop);

    pTpu->setuart_cfg_1_uart_trig_dat(cmp_eq);

    Tpu_reg  *data = NULL;
    data = (Tpu_reg*)cfg.mDatas.data();
    Q_ASSERT(data != NULL);
    pTpu->setuart_cfg_2_uart_dat_min(*data);
    pTpu->setuart_cfg_2_uart_dat_max(*data);

}
