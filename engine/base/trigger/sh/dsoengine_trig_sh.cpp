
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mSHCfg

void dsoEngine::enterTrigSH()
{}

void dsoEngine::exitTrigSH()
{}

DsoErr dsoEngine::setTrigSHCfg( TrigSHCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigSH( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigSH( TrigSHCfg &cfg, IPhyTpu *pTpu )
{
    EdgeSlope             clkSlope     = cfg.mSlope;
    TriggerPulsePolarity  dataPolarity = cfg.datPolarity();

    //! src
    pTpu->rstIir();

    if ( cfg.mEvent == Trigger_SH_setup )
    {
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mClk ),
                           (clkSlope == Trigger_Edge_Rising)? IPhyTpu::rise : IPhyTpu::fall
                          );

        pTpu->aEventCmpb( toaCmpSrc_a( cfg.mDat ),
                          (dataPolarity == Trigger_pulse_positive)? IPhyTpu::rise : IPhyTpu::fall );


        pTpu->aEventClkDat( IPhyTpu::cmp_a, (clkSlope == Trigger_Edge_Rising)? true : false,
                            IPhyTpu::cmp_b, (dataPolarity == Trigger_pulse_positive)? false : true );

        pTpu->aEventWidth( IPhyTpu::lt, 0, cfg.mSetupTime );

        pTpu->setTrigSrc( IPhyTpu::a );
    }
    else if ( cfg.mEvent == Trigger_SH_hold )
    {
        pTpu->aEventCmpb( toaCmpSrc_a( cfg.mClk ),
                          (clkSlope == Trigger_Edge_Rising)? IPhyTpu::rise : IPhyTpu::fall );

        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mDat ),
                          IPhyTpu::any/*(dataPolarity == Trigger_pulse_positive)? IPhyTpu::fall: IPhyTpu::rise */);

        pTpu->aEventClkDat( IPhyTpu::cmp_a, (dataPolarity == Trigger_pulse_negative)?true:false,
                            IPhyTpu::cmp_c_opti, false );

        pTpu->aEventWidth( IPhyTpu::lt, 0, cfg.mHoldTime );

        pTpu->setTrigSrc( IPhyTpu::a );
    }
    else if ( cfg.mEvent == Trigger_SH_setup_hold )
    {
        pTpu->aEventCmpa( toaCmpSrc_a( cfg.mClk ),
                           (clkSlope == Trigger_Edge_Rising)? IPhyTpu::rise : IPhyTpu::fall );

        pTpu->aEventCmpb( toaCmpSrc_a( cfg.mDat ),
                          (dataPolarity == Trigger_pulse_positive)? IPhyTpu::rise : IPhyTpu::fall);

        pTpu->aEventClkDat( IPhyTpu::cmp_a, (clkSlope == Trigger_Edge_Rising)? true : false,
                            IPhyTpu::cmp_b, (dataPolarity == Trigger_pulse_positive)? false : true );
        pTpu->aEventWidth( IPhyTpu::lt, 0, cfg.mSetupTime );


        pTpu->bEventCmpb( toaCmpSrc_a( cfg.mClk ),
                          (clkSlope == Trigger_Edge_Rising)? IPhyTpu::rise : IPhyTpu::fall);

        pTpu->bEventCmpa( toaCmpSrc_a( cfg.mDat ),
                          IPhyTpu::any/*(dataPolarity == Trigger_pulse_positive)? IPhyTpu::fall: IPhyTpu::rise*/);

        pTpu->bEventClkDat( IPhyTpu::cmp_a, (dataPolarity == Trigger_pulse_negative)?true:false,
                            IPhyTpu::cmp_c_opti, false );
        pTpu->bEventWidth( IPhyTpu::lt, 0, cfg.mHoldTime );

        pTpu->setTrigSrc( IPhyTpu::a_or_b );
    }
    else
    {
        Q_ASSERT(false);
    }
}
