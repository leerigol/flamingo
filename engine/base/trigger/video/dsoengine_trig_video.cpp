
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mVideoCfg

void dsoEngine::enterTrigVideo()
{}
void dsoEngine::exitTrigVideo()
{}

DsoErr dsoEngine::setTrigVideoCfg( TrigVideoCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigVideoCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

#define VIDEO_CONFIG(lt, fw, hw, sl, tl)             \
    pTpu->setvideo_cfg_3_level_type(   lt         ); \
    pTpu->setvideo_cfg_2_fpulse_width( fw/TD_TICK ); \
    pTpu->setvideo_cfg_2_hsync_width(  hw/TD_TICK ); \
    pTpu->setvideo_cfg_3_start_line(   sl         ); \
    pTpu->setvideo_cfg_3_total_line(   tl         );

#define SET_VIDEO_TYPE(sync, b) do{\
                                    if(sync != trig_video_any_line){sync = trig_video_x_line;}      \
                                    if(b){pTpu->setvideo_cfg_1_video_trig_type( (sync|(1<<1))    );}\
                                    else{pTpu->setvideo_cfg_1_video_trig_type(  (sync&(~(1<<1))) );}\
                                   }while(0)

void dsoEngine::applyTrigVideoCfg( TrigVideoCfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.ch() );
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();

    pTpu->rEventCmpa(toaCmpSrc_a(cfg.mCh));
    pTpu->rEventCmpb(toaCmpSrc_b(cfg.mCh));

    pTpu->rEventWidth();

    if(Trigger_pulse_positive == cfg.mPolarity )
    {
        pTpu->rEventClkDat(IPhyTpu::cmp_a, false ,
                           IPhyTpu::cmp_a, false
                           );
    }
    else
    {
        pTpu->rEventClkDat(IPhyTpu::cmp_b,  true,
                           IPhyTpu::cmp_b,  true
                           );
    }
    pTpu->setTrigSrc( IPhyTpu::video );

    //!video
    pTpu->setvideo_cfg_1_video_trig_line(cfg.mLineN);

    switch(cfg.mFromat)
    {
    case Video_Stardard_NTSC:
        VIDEO_CONFIG(trig_video_bi, time_us(4.7),  time_us(58.79),  5, 525);
        SET_VIDEO_TYPE(cfg.mSync, false);
        break;
    case Video_Stardard_PAL:
        VIDEO_CONFIG(trig_video_bi, time_us(4.7),  time_us(59.3),   2, 625);
        SET_VIDEO_TYPE(cfg.mSync, false);
        break;
    case Video_Stardard_480P:
        VIDEO_CONFIG(trig_video_bi, time_us(4.7),  time_us(27.077), 8, 525);
        SET_VIDEO_TYPE(cfg.mSync, true);
        break;
    case Video_Stardard_576P:
        VIDEO_CONFIG(trig_video_bi, time_us(2.35), time_us(29.65),  8, 625);
        SET_VIDEO_TYPE(cfg.mSync, true);
        break;
    default:
        Q_ASSERT(false);
        break;
    }

}
