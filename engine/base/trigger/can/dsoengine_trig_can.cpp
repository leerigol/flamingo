#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mCanCfg

DsoErr dsoEngine::setTrigCanCfg( TrigCanCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigCanCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigCanCfg(TrigCanCfg &cfg, IPhyTpu *pTpu)
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.mCh);
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();

    pTpu->rEventCmpa(src, IPhyTpu::rise);
    pTpu->rEventCmpb(src, IPhyTpu::rise);

    pTpu->rEventClkDat(IPhyTpu::logic, false,
                       IPhyTpu::cmp_a, false);

    pTpu->setTrigSrc( IPhyTpu::can );

    //!can
    pTpu->setcan_cfg_1_can_trig_type(          cfg.when()       );
    pTpu->setcan_cfg_1_can_trig_type_field(    cfg.field()      );
    pTpu->setcan_cfg_1_can_trig_type_frame(    cfg.frame()      );
    pTpu->setcan_cfg_1_can_trig_type_err(      cfg.err()        );
    pTpu->setcan_cfg_1_can_trig_type_id(       cfg.idCmp()      );
    pTpu->setcan_cfg_1_can_trig_type_id_mask(  cfg.idCmpMask()     );
    pTpu->setcan_cfg_1_can_trig_type_dat(      cfg.datCmp()     );
    pTpu->setcan_cfg_1_can_trig_type_dat_mask( cfg.datCmpMask() );
    pTpu->setcan_cfg_1_can_trig_bus_type(      cfg.phy()        );
    pTpu->setcan_cfg_1_can_trig_std_extend_sel(cfg.spec()       );

    int div = (time_s(1)/TD_TICK)/cfg.baud();
    pTpu->setcan_cfg_2_can_trig_clk_div( div);
    pTpu->setcan_cfg_2_can_trig_sa_pos(  (int)div*cfg.saPos()*0.01 );

    pTpu->setcan_cfg_3_can_trig_id_min(  cfg.idMin()  );
    pTpu->setcan_cfg_4_can_trig_id_max(  cfg.idMax()  );
    pTpu->setcan_cfg_5_can_trig_id_mask( cfg.idMask() );

    Q_ASSERT(cfg.mDatMin.count() >= 8);
    Q_ASSERT(cfg.mDatMax.count() >= 8);
    Q_ASSERT(cfg.mDatMask.count()>= 8);

    Tpu_reg   datMinL  = 0;
    Tpu_reg   datMinH  = 0;

    Tpu_reg   datMaxL  = 0;
    Tpu_reg   datMaxH  = 0;

    Tpu_reg   datMaskL  = 0;
    Tpu_reg   datMaskH  = 0;

    for(int i = 0; i<4; i++)
    {
        datMinL  |= (cfg.mDatMin[i]<<(i*8));
        datMinH  |= (cfg.mDatMin[i+4]<<((i*8)));

        datMaxL  |= (cfg.mDatMax[i]<<(i*8));
        datMaxH  |= (cfg.mDatMax[i+4]<<(i*8));

        datMaskL |= (cfg.mDatMask[i]<<(i*8));
        datMaskH |= (cfg.mDatMask[i+4]<<(i*8));
    }
    //qDebug()<<__FILE__<<__LINE__<<datMinL<<datMinH<<datMaxL<<datMaxH<<datMaskL<<datMaskH<<cfg.mDatMask.count();

    pTpu->setcan_cfg_1_can_trig_dat_cmpnum( cfg.mByteCount );

    pTpu->setcan_cfg_6_can_trig_dat_min_bits31_0(    datMinL  );
    pTpu->setcan_cfg_7_can_trig_dat_min_bits63_32(   datMinH  );

    pTpu->setcan_cfg_8_can_trig_dat_max_bits31_0(    datMaxL  );
    pTpu->setcan_cfg_9_can_trig_dat_max_bits63_32(   datMaxH  );

    pTpu->setcan_cfg_10_can_trig_dat_mask_bits31_0(  datMaskL );
    pTpu->setcan_cfg_11_can_trig_dat_mask_bits63_32( datMaskH );
}
