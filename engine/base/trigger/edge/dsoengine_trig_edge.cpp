
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mEdgeCfg

//void dsoEngine::enterTrigEdge()
//{
//    preSetTrigEdge();

//    //! apply trig
//    applyTrigEdge( selfTrigCfg );
//}

void dsoEngine::preSetTrigEdge()
{
    //! a width time
    m_pPhy->mTpu.setEventWidthL( IPhyTpu::event_a, 0 );
    m_pPhy->mTpu.setEventWidthH( IPhyTpu::event_a, 0 );

    m_pPhy->mTpu.setEventWidthCmp( IPhyTpu::event_a, IPhyTpu::gt );

    //! b width time
    m_pPhy->mTpu.setEventWidthL( IPhyTpu::event_b, 0 );
    m_pPhy->mTpu.setEventWidthH( IPhyTpu::event_b, 0 );

    m_pPhy->mTpu.setEventWidthCmp( IPhyTpu::event_b, IPhyTpu::gt );
}

DsoErr dsoEngine::setTrigEdgeCfg( TrigEdgeCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    preSetTrigEdge();

    applyTrigEdge( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

//! coupling by outter
void dsoEngine::applyTrigEdge( TrigEdgeCfg &cfg, IPhyTpu *pTpu )
{
    IPhyTpu::eCmpSrc src;
    Chan ch;

    ch  = cfg.getChan();
    src = toaCmpSrc_a( ch );
    Q_ASSERT( src != IPhyTpu::invalid );

    //! a   b
    pTpu->aEventWidth();
    pTpu->bEventWidth();

    pTpu->aEventCmpa( toaCmpSrc_a(ch), IPhyTpu::rise);
    pTpu->bEventCmpa( toaCmpSrc_a(ch), IPhyTpu::rise);

    pTpu->aEventCmpb( toaCmpSrc_a(ch), IPhyTpu::rise );
    pTpu->bEventCmpb( toaCmpSrc_a(ch), IPhyTpu::rise );


    //! slope
    EdgeSlope slp = cfg.getSlope();

    if ( slp == Trigger_Edge_Rising )
    {
        pTpu->aEventClkDat(  IPhyTpu::cmp_a, true,
                             IPhyTpu::cmp_a, true );
        pTpu->setTrigSrc( IPhyTpu::a  );

    }
    else if ( slp == Trigger_Edge_Falling )
    {
        pTpu->aEventClkDat(  IPhyTpu::cmp_a, false,
                             IPhyTpu::cmp_a, false );
        pTpu->setTrigSrc( IPhyTpu::a  );
    }
    else
    {

        pTpu->bEventCmpa( toaCmpSrc_b(ch), IPhyTpu::rise );

        pTpu->aEventClkDat(  IPhyTpu::cmp_a, false,
                             IPhyTpu::cmp_a, false );

        pTpu->bEventClkDat(  IPhyTpu::cmp_a, true,
                             IPhyTpu::cmp_a, true );

        pTpu->setTrigSrc( IPhyTpu::a_or_b );
    }
}
