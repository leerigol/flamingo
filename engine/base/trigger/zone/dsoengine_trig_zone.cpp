#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

/*!****************************debug***********************************/
#define DEBUG_INIT()  //QTime starTime = QTime::currentTime()
#define DEBUG_PRINT() //do{qDebug()<<__FILE__<<__LINE__<<starTime.msecsTo(QTime::currentTime())<<"ms";}while(0)

/*!***************************define**********************************/
#define selfTrigCfg    m_pViewSession->m_trigger.mZoneCfg

typedef void (SpuGp::*void_reg_int)(Spu_reg, int);
struct TZoneTabReg
{
    void_reg_int  tab_index    ;
    void_reg_int  column_addr  ;
    void_reg_int  threshold_en ;
    void_reg_int  threshold_h  ;
    void_reg_int  threshold_l  ;
};

struct TAttr
{
    horiAttr mHAttr;
    int      mChCnt;
    CCH      mCCh[4];

    bool operator==(const TAttr &s )
    {
        if(mChCnt != s.mChCnt)
        {return false;}

        if(mHAttr.gnd != s.mHAttr.gnd)
        {return false;}

        if(mHAttr.inc != s.mHAttr.inc)
        {return false;}

        if(mHAttr.unit != s.mHAttr.unit)
        {return false;}

        if(mHAttr.tInc != s.mHAttr.tInc)
        {return false;}

        if(mHAttr.zone != s.mHAttr.zone)
        {return false;}

        for(int i = 0; i <4; i++)
        {
            if(mCCh[i] != s.mCCh[i])
            {return false;}
        }

        return true;
    }

    TAttr operator = (const TAttr &s )
    {
        mChCnt = s.mChCnt;
        mHAttr  = s.mHAttr;
        for(int i = 0; i <4; i++)
        {
            mCCh[i] = s.mCCh[i];
        }
        return *this;
    }
};

static TAttr g_attr;

static bool setAttr( CDsoSession *pSesView )
{
    Q_ASSERT(NULL != pSesView);
    TAttr attr;
    //!垂直参数
    attr.mChCnt = pSesView->m_acquire.m_chCnt;
    for(int i = 0; i < 4; i++)
    {
        attr.mCCh[i] = pSesView->m_ch[i];
    }

    //!水平参数
    attr.mHAttr  = getHoriAttr(chan1, horizontal_view_main);

    if(g_attr == attr)
    {
        return false;
    }
    else
    {
        g_attr = attr;
        return true;
    }
}

static QVector<int> tabA(1000);
static QVector<int> tabB(1000);
static QVector<int> tabC(1000);
static QVector<int> tabD(1000);
static void createZone(  int  start,   int  end,
                         int  chCount, int  ch,
                         QVector<int> &tabA,
                         QVector<int> &tabB,
                         QVector<int> &tabC,
                         QVector<int> &tabD
                        );
/*!******************************************************************/
DsoErr dsoEngine::setTrigZoneEn(bool en)
{
    m_pPhy->mCcu.outMODE_zone_trig_en(en);
    m_pViewSession->m_mask.mZoneEn = en;
    return ERR_NONE;
}

DsoErr dsoEngine::setTrigZoneCfg( TrigZoneCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );
    //!参数不变，不重新配置逻辑
    if( !(selfTrigCfg == *pCfg))
    {
        selfTrigCfg = *pCfg;
        setAttr(m_pViewSession);
    }
    else
    {
        return ERR_NONE;
    }

    applyTrigZoneCfg(selfTrigCfg);
    return ERR_NONE;
}

void dsoEngine::applyTrigZone(CDsoSession *pSesView)
{
    Q_ASSERT(NULL != pSesView);

    if(!setAttr(pSesView))
    {
        return ;
    }

    applyTrigZoneCfg(selfTrigCfg);
}

void dsoEngine::applyTrigZoneCfg(TrigZoneCfg &cfg)
{
DEBUG_INIT() ;

    int   zoneCount = 0;
    TZoneTabReg  zoneTabA;
    TZoneTabReg  zoneTabB;

    zoneTabA.tab_index    = &SpuGp::outZONE_TRIG_TAB1_tab_index;
    zoneTabA.column_addr  = &SpuGp::outZONE_TRIG_TAB1_column_addr;
    zoneTabA.threshold_en = &SpuGp::outZONE_TRIG_TAB1_threshold_en;
    zoneTabA.threshold_h  = &SpuGp::outZONE_TRIG_TAB1_threshold_h;
    zoneTabA.threshold_l  = &SpuGp::outZONE_TRIG_TAB1_threshold_l;

    zoneTabB.tab_index    = &SpuGp::outZONE_TRIG_TAB2_tab_index;
    zoneTabB.column_addr  = &SpuGp::outZONE_TRIG_TAB2_column_addr;
    zoneTabB.threshold_en = &SpuGp::outZONE_TRIG_TAB2_threshold_en;
    zoneTabB.threshold_h  = &SpuGp::outZONE_TRIG_TAB2_threshold_h;
    zoneTabB.threshold_l  = &SpuGp::outZONE_TRIG_TAB2_threshold_l;

    foreach (TrigZoneCfg::TZoneDataStru item, cfg.a_zone)
    {
        /*qDebug()<<__FILE__<<__LINE__
               <<"tMin:"<<item.tMin<<"tMax:"<<item.tMax
              <<"H:"<<item.thresholdH<<"L:"<<item.thresholdL;*/

        //qDebug()<<__FILE__<<__LINE__<<"ID:"<<zoneCount;
        if(zoneCount == 0)
        {
            bool en = setTrigZoneX(&zoneTabA, item);
            m_pPhy->mSpuGp.setMASK_CTRL_zone1_trig_cross(item.cross);
            m_pPhy->mSpuGp.setMASK_CTRL_zone1_trig_tab_en(en);
            m_pPhy->mSpuGp.flushWCache();

        }
        else if(zoneCount == 1)
        {
            bool en = setTrigZoneX(&zoneTabB, item);
            m_pPhy->mSpuGp.setMASK_CTRL_zone2_trig_cross(item.cross);
            m_pPhy->mSpuGp.setMASK_CTRL_zone2_trig_tab_en(en);
            m_pPhy->mSpuGp.flushWCache();
        }
        else
        {Q_ASSERT(false);}

        zoneCount++;
    }
DEBUG_PRINT();
}

bool dsoEngine::setTrigZoneX(void *pZoneTabReg,
                             TrigZoneCfg::TZoneDataStru item)
{
    //!区域没打开时，不配模版参数
    if(!item.en)
    {
        return false;
    }

    Q_ASSERT(pZoneTabReg != NULL );
    TZoneTabReg *pTabReg = static_cast<TZoneTabReg*>(pZoneTabReg);
    Q_ASSERT(pTabReg != NULL );
    TZoneTabReg  zoneTabReg = *pTabReg;

    const int spuId  = -1;
    horiAttr  hAttr  = g_attr.mHAttr;
    Chan ch          = item.ch;
    int  columnStart = item.tMin / hAttr.tInc + hAttr.gnd;
    int  columnEnd   = item.tMax / hAttr.tInc + hAttr.gnd;
    int  thresholdH  = toAdc( ch, item.thresholdH );
    int  thresholdL  = toAdc( ch, item.thresholdL );

    /*qDebug()<<__FILE__<<__LINE__<<"chCount:"<<g_zoneChCount
           <<"sart:"<<columnStart<<"end:"<<columnEnd
           <<"thre_adc_H:"<<thresholdH<<"thre_adc_L:"<<thresholdL;*/

    columnStart = qBound(0, columnStart, 1000);
    columnEnd   = qBound(0, columnEnd,   1000);

    //!区域范围变成线或点时，直接关闭区域触发。
    if( (thresholdH == thresholdL) || (columnStart == columnEnd) )
    {
        //qDebug()<<__FILE__<<__LINE__<<"err:zone range is too small , auto off zone trig.";
        return false;
    }

    createZone(columnStart, columnEnd, g_attr.mChCnt, ch, tabA, tabB, tabC, tabD);

    /*qDebug()<<__FILE__<<__LINE__<<"\n----------------A--------------\n"<<tabA<<"\n";
    qDebug()<<__FILE__<<__LINE__<<"\n----------------B--------------\n"<<tabB<<"\n";
    qDebug()<<__FILE__<<__LINE__<<"\n----------------C--------------\n"<<tabC<<"\n";
    qDebug()<<__FILE__<<__LINE__<<"\n----------------D--------------\n"<<tabD<<"\n";*/

    for(int i = 0; i<4; i++)
    {
        int index = 1<<i;
        QVector<int> *pTab = NULL;
        switch (i) {
        case 0:
            pTab = &tabA;
            break;
        case 1:
            pTab = &tabB;
            break;
        case 2:
            pTab = &tabC;
            break;
        case 3:
            pTab = &tabD;
            break;
        default:
            pTab = NULL;
            break;
        }

        //qDebug()<<__FILE__<<__LINE__<<"\n----------------"<<"index"<< index <<"--------------\n";
        Q_ASSERT( NULL != pTab);

        //QString debugStr = "";
        int addrEnd = 250 * g_attr.mChCnt;
        for(int addr = 0; addr< addrEnd; ++addr)
        {
            int threshold_en = pTab->at(addr);

            m_pPhy->mSpuGp.outMASK_CTRL_mask_we(1);

            (m_pPhy->mSpuGp.*zoneTabReg.tab_index)( index, spuId);
            (m_pPhy->mSpuGp.*zoneTabReg.column_addr)(addr, spuId);

            (m_pPhy->mSpuGp.*zoneTabReg.threshold_h)(thresholdH, spuId);
            (m_pPhy->mSpuGp.*zoneTabReg.threshold_l)(thresholdL, spuId);
            (m_pPhy->mSpuGp.*zoneTabReg.threshold_en)(threshold_en, spuId);
             m_pPhy->mSpuGp.flushWCache();

             m_pPhy->mSpuGp.outMASK_CTRL_mask_we(0);

            //unsigned int reg = m_pPhy->mSpu.ZONE_TRIG_TAB1.payload;
            //debugStr += QString("0X%1, ").arg(reg, 0, 16);
        }
        //qDebug()<<__FILE__<<__LINE__<<debugStr;
    }

    return true;
}

void createZone4(int  start,   int  end,
                 QVector<int> &tab_4_ch
                 )
{
    int step = 1;
    start = start/step;
    end   = end/step;

    for(int i = start; i < end; i++)
    {
        tab_4_ch[i] = 1;
    }
}

void createZone2(int  start,   int  end,
                 QVector<int> &tab_2_ch_1,
                 QVector<int> &tab_2_ch_2
                 )
{
    int step = 2;
    int step_s = start / step;
    int step_e = end / step;
    for(int i = step_s+1; i < step_e; i++ )
    {
        tab_2_ch_1[i] = 1;
        tab_2_ch_2[i] = 1;
    }

    do
    {
        int i = step - start % step;

        if( (--i) < 0 )
        {break;}
        tab_2_ch_2[step_s] = 1;
        //qDebug()<<__FILE__<<__LINE__<<"---4---"<<i;

        if( (--i) < 0 )
        {break;}
        tab_2_ch_1[step_s] = 1;
        //qDebug()<<__FILE__<<__LINE__<<"--3----"<<i;
    }while(0);


    do
    {
        int i = end % step;
        //qDebug()<<__FILE__<<__LINE__<<"****1***"<<i;
        tab_2_ch_1[step_e] = 1;
        if( (--i) < 0 )
        {break;}

        //qDebug()<<__FILE__<<__LINE__<<"****2***"<<i;
        tab_2_ch_2[step_e] = 1;
        if( (--i) < 0 )
        {break;}

    }while(0);

}


void createZone1(int  start,   int  end,
                 QVector<int> &tab_1_ch_1,
                 QVector<int> &tab_1_ch_2,
                 QVector<int> &tab_1_ch_3,
                 QVector<int> &tab_1_ch_4
                 )
{
    //qDebug()<<__FILE__<<__LINE__<<"\n\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<start<<end;
    int step = 4;

    int step_s = start / step;
    int step_e = end / step;

    //qDebug()<<__FILE__<<__LINE__<<step_s<<step_e;
    for(int i = step_s+1; i < step_e;  i += step)
    {
        tab_1_ch_1[i] = 1;
        tab_1_ch_2[i] = 1;
        tab_1_ch_3[i] = 1;
        tab_1_ch_4[i] = 1;
    }

    do
    {
        int i = step - start % step;

        if( (--i) < 0 )
        {break;}
        tab_1_ch_4[step_s] = 1;
        //qDebug()<<__FILE__<<__LINE__<<"---4---"<<i;

        if( (--i) < 0 )
        {break;}
        tab_1_ch_3[step_s] = 1;
        //qDebug()<<__FILE__<<__LINE__<<"--3----"<<i;

        if( (--i) < 0 )
        {break;}
        tab_1_ch_2[step_s] = 1;
        //qDebug()<<__FILE__<<__LINE__<<"---2---"<<i;

        if( (--i) < 0 )
        {break;}
        tab_1_ch_1[step_s] = 1;
        //qDebug()<<__FILE__<<__LINE__<<"---1---"<<i;

    }while(0);


    do
    {
        int i = end % step;
        //qDebug()<<__FILE__<<__LINE__<<"****1***"<<i;
        tab_1_ch_1[step_e] = 1;
        if( (--i) < 0 )
        {break;}

        //qDebug()<<__FILE__<<__LINE__<<"****2***"<<i;
        tab_1_ch_2[step_e] = 1;
        if( (--i) < 0 )
        {break;}

        //qDebug()<<__FILE__<<__LINE__<<"****3***"<<i;
        tab_1_ch_3[step_e] = 1;
        if( (--i) < 0 )
        {break;}

        //qDebug()<<__FILE__<<__LINE__<<"****4***"<<i;
        tab_1_ch_4[step_e] = 1;
        if( (--i) < 0 )
        {break;}

    }while(0);
}

void createZone( int  start,   int  end,
                 int  chCount, int  ch,
                 QVector<int> &tabA,
                 QVector<int> &tabB,
                 QVector<int> &tabC,
                 QVector<int> &tabD
               )
{
    tabA.clear();
    tabB.clear();
    tabC.clear();
    tabD.clear();

    tabA.resize(1000);
    tabB.resize(1000);
    tabC.resize(1000);
    tabD.resize(1000);

    Q_ASSERT( (0 <= start) && (start <= 1000) );
    Q_ASSERT( (0 <= end)   && (end <= 1000) );

    if(1 == chCount)
    {
        createZone1(start, end, tabA, tabB, tabC, tabD);
    }

    if(2 == chCount)
    {

        if( (chan1 == ch) || (chan2 == ch) )
        {
            createZone2(start, end, tabA, tabC);
        }
        else if( (chan3 == ch) || (chan4 == ch) )
        {
            createZone2(start, end, tabB, tabD);
        }
        else
        {Q_ASSERT(false);}
    }

    if(4 == chCount)
    {
        if (chan1 == ch)
        {
            createZone4(start, end, tabA);
        }
        else if (chan2 == ch)
        {
            createZone4(start, end, tabB);
        }
        else if (chan3 == ch)
        {
            createZone4(start, end, tabC);
        }
        else if (chan4 == ch)
        {
            createZone4(start, end, tabD);
        }
        else
        {Q_ASSERT(false);}
    }
}
