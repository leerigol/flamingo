#ifndef ENGINECFG_TRIG
#define ENGINECFG_TRIG

#include "../enginecfg.h"

class TrigEdgeCfg : public EngineCfg
{
public:
    TrigEdgeCfg();
    TrigEdgeCfg( const TrigEdgeCfg *pCfg );
    TrigEdgeCfg( const TrigEdgeCfg &pCfg );
    TrigEdgeCfg &operator= ( const TrigEdgeCfg &cfg );

    void setChan( Chan ch );
    Chan getChan();

    void setSlope( EdgeSlope slp );
    EdgeSlope getSlope();

public:
    Chan mCh;
    EdgeSlope mSlope;
};

class TrigPulseCfg : public EngineCfg
{
public:
    TrigPulseCfg();

    void setChan( Chan ch );
    Chan getChan();

    void setPolarity( TriggerPulsePolarity pol );
    TriggerPulsePolarity getPolarity();

    void setCmp( EMoreThan cmp );
    EMoreThan getCmp();

    void setWidthH( qint64 w );
    qint64 getWidthH();

    void setWidthL( qint64 l );
    qint64 getWidthL();

public:
    Chan mCh;
    TriggerPulsePolarity mPolariy;
    EMoreThan mCmp;
    qint64 mWidthH, mWidthL;
};

class TrigRuntCfg : public TrigPulseCfg
{};

class TrigOverCfg : public EngineCfg
{
public:
    TrigOverCfg();

    void setChan( Chan ch );
    Chan getChan();

    void setSlope( EdgeSlope slp );
    EdgeSlope getSlope();

    void setEvent( OverEvent evt );
    OverEvent getEvent();

    void setWidth( qint64 w );
    qint64 getWidth();

public:
    Chan mCh;
    EdgeSlope mSlope;
    OverEvent mEvent;
    qint64 mWidth;
};

class TrigWindowCfg : public EngineCfg
{
public:
     Chan mCh;
     WindowEvent mEvent;
     EMoreThan mCmp;
     qint64 mWidthH, mWidthL;
public:
     Chan ch() const;
     void setCh(const Chan &ch);
     WindowEvent event() const;
     void setEvent(const WindowEvent &event);
     EMoreThan cmp() const;
     void setCmp(const EMoreThan &cmp);
     qint64 widthH() const;
     void setWidthH(const qint64 &widthH);
     qint64 widthL() const;
     void setWidthL(const qint64 &widthL);
};

class TrigSlopeCfg : public EngineCfg
{
public:
    TrigSlopeCfg();

    void setChan( Chan ch );
    Chan getChan();

    void setSlope( EdgeSlope slp );
    EdgeSlope getSlope();

    void setCmp( EMoreThan cmp );
    EMoreThan getCmp();

    void setWidthH( qint64 w );
    qint64 getWidthH();

    void setWidthL( qint64 l );
    qint64 getWidthL();

public:
    Chan mCh;
    EdgeSlope mSlope;
    EMoreThan mCmp;
    qint64 mWidthH, mWidthL;
};

class TrigTimeoutCfg : public EngineCfg
{
public:
    Chan mCh;
    TriggerPulsePolarity mPolarity;
    qint64 mWidth;

public:
    Chan ch() const;
    void setCh(const Chan &ch);
    TriggerPulsePolarity polarity() const;
    void setPolarity(const TriggerPulsePolarity &polarity);
    qint64 width() const;
    void setWidth( qint64 width);
};

class TrigSHCfg : public EngineCfg
{
public:
    Chan mClk, mDat;
    EdgeSlope mSlope;
    TriggerPulsePolarity mDatPolarity;
    SHEvent mEvent;

    qint64 mSetupTime, mHoldTime;

public:
    Chan clk() const;
    void setClk(const Chan &clk);
    Chan dat() const;
    void setDat(const Chan &dat);
    EdgeSlope slope() const;
    void setSlope(const EdgeSlope &slope);
    SHEvent event() const;
    void setEvent(const SHEvent &event);
    qint64 setupTime() const;
    void setSetupTime(qint64 setupTime);
    qint64 holdTime() const;
    void setHoldTime(qint64 holdTime);
    TriggerPulsePolarity datPolarity() const;
    void setDatPolarity(const TriggerPulsePolarity &datPolarity);
};

//! a->b
class TrigDelayCfg : public EngineCfg
{
public:
    Chan mChA, mChB;
    EdgeSlope mSlopeA, mSlopeB;
    EMoreThan mCmp;
    qint64 mWidthL, mWidthH;
public:
    Chan chA() const;
    void setChA(const Chan &chA);
    Chan chB() const;
    void setChB(const Chan &chB);
    EdgeSlope slopeA() const;
    void setSlopeA(const EdgeSlope &slopeA);
    EdgeSlope slopeB() const;
    void setSlopeB(const EdgeSlope &slopeB);
    EMoreThan cmp() const;
    void setCmp(const EMoreThan &cmp);
    qint64 widthL() const;
    void setWidthL(qint64 widthL);
    qint64 widthH() const;
    void setWidthH(qint64 widthH);
};

class TrigNEdgeCfg : public TrigEdgeCfg
{
public:
    int mNum;
    qint64 mIdleTime;

public:
    int num() const;
    void setNum(int num);
    qint64 idleTime() const;
    void setIdleTime(qint64 idleTime);
};

class TrigPatternCfg : public EngineCfg
{
public:
    static const int  mCodeCount = 4 + 16 + 1;
    TriggerPattern    mPatterns[ mCodeCount ];
    TriggerLogicOperator mOperator;
public:
    bool checkCode()
    {
        int edgeCount = 0;
        for(int i = 0; i< mCodeCount; i++)
        {
            if( mPatterns[i] > Trigger_pat_x)
            {
                edgeCount++;
            }
        }
        return (edgeCount<=1)?true:false;
    }
    void clearCode()
    {
        for(int i = 0; i< mCodeCount; i++)
        {
            mPatterns[i] = Trigger_pat_x;
        }
    }
};

class TrigDurationCfg : public TrigPatternCfg
{
public:
    Chan mCh;
    TriggerPulsePolarity mPolariy;
    EMoreThan mCmp;
    qint64 mWidthH, mWidthL;
public:
    Chan ch() const;
    void setCh(const Chan &ch);
    TriggerPulsePolarity polariy() const;
    void setPolariy(const TriggerPulsePolarity &polariy);
    EMoreThan cmp() const;
    void setCmp(const EMoreThan &cmp);
    qint64 widthH() const;
    void setWidthH(const qint64 &widthH);
    qint64 widthL() const;
    void setWidthL(const qint64 &widthL);
};

class TrigVideoCfg : public EngineCfg
{
public:
    Chan mCh;
    TriggerPulsePolarity mPolarity;
    Trigger_Video_Format mFromat;
    Trigger_Video_Sync mSync;
    int mLineN;

public:
    Chan ch() const;
    void setCh(const Chan &ch);
    TriggerPulsePolarity polarity() const;
    void setPolarity(const TriggerPulsePolarity &polarity);
    Trigger_Video_Format fromat() const;
    void setFromat(const Trigger_Video_Format &fromat);
    Trigger_Video_Sync sync() const;
    void setSync(const Trigger_Video_Sync &sync);
    int lineN() const;
    void setLineN(int lineN);
};

class TrigRS232Cfg : public EngineCfg
{

public:
    Chan mCh;
    int mBaudRate;                 //! b/s

    bool mbInvert;                 //! true-->uart; false-->RS232
    Trigger_RS232_Parity mParity;  //! 奇偶校验
    Trigger_RS232_Stop mStop;
    Trigger_RS232_Width mWidth;

    Trigger_RS232_Event mEvent;
    Trigger_value_cmp mDataCmp;
    Triger_RS232_Error mErr;

    QByteArray mDatas;

public:
    Chan ch() const;
    void setCh(const Chan &ch);
    int baudRate() const;
    void setBaudRate(int baudRate);
    bool getMbInvert() const;
    void setMbInvert(bool value);
    Trigger_RS232_Parity getParity() const;
    void setParity(const Trigger_RS232_Parity &parity);
    Trigger_RS232_Stop getStop() const;
    void setStop(const Trigger_RS232_Stop &stop);
    Trigger_RS232_Width getWidth() const;
    void setWidth(const Trigger_RS232_Width &width);
    Trigger_RS232_Event getEvent() const;
    void setEvent(const Trigger_RS232_Event &event);
    Trigger_value_cmp getDataCmp() const;
    void setDataCmp(const Trigger_value_cmp &dataCmp);
    Triger_RS232_Error getErr() const;
    void setErr(const Triger_RS232_Error &err);
    QByteArray getDatas() const;
    void setDatas(QByteArray datas);
};

class TrigI2SCfg : public EngineCfg
{
public:
    Chan mSclk;
    Chan mWs;
    Chan mSda;

    EdgeSlope        mSlope;
    Trigger_IIS_Ch   mLine;
    Trigger_IIS_Spec mSpec;

    Trigger_IIS_data_cmp mDataCmp;

    quint32 mUsedWidth;
    quint32 mWidth;

    quint32 mMinData;
    quint32 mMaxData;
    quint32 mMask;
public:

    Chan sclk() const;
    void setSclk(const Chan &sclk);
    Chan ws() const;
    void setWs(const Chan &ws);
    Chan sda() const;
    void setSda(const Chan &sda);
    EdgeSlope slope() const;
    void setSlope(const EdgeSlope &slope);
    Trigger_IIS_Ch line() const;
    void setLine(const Trigger_IIS_Ch &line);
    Trigger_IIS_Spec spec() const;
    void setSpec(const Trigger_IIS_Spec &spec);
    Trigger_IIS_data_cmp dataCmp() const;
    void setDataCmp(const Trigger_IIS_data_cmp &dataCmp);
    quint32 usedWidth() const;
    void setUsedWidth(const quint32 &usedWidth);
    quint32 width() const;
    void setWidth(const quint32 &width);
    quint32 minData() const;
    void setMinData(const quint32 &minData);
    quint32 maxData() const;
    void setMaxData(const quint32 &maxData);
    quint32 mask() const;
    void setMask(const quint32 &mask);
};

class TrigLinCfg : public EngineCfg
{
public:
    Chan mCh;
    quint32 mClk;       //! hz
    Trigger_Lin_Ver mVer;

    Trigger_Lin_When mWhen;
    Trigger_value_cmp mIdCmp;
    Trigger_value_cmp mDatCmp;
    Trigger_Lin_Err mErr;

    quint32 mIdMin, mIdMax;

    int mSaPos;         //! 10% ~ 90%
    QByteArray mMinBits;
    QByteArray mMaxBits;
    QByteArray mMaskBits;
    int        mByteCount;
public:
    Chan ch() const;
    void setCh(const Chan &ch);
    quint32 clk() const;
    void setClk(const quint32 &clk);
    Trigger_Lin_Ver ver() const;
    void setVer(const Trigger_Lin_Ver &ver);
    Trigger_Lin_When when() const;
    void setWhen(const Trigger_Lin_When &when);
    Trigger_value_cmp idCmp() const;
    void setIdCmp(const Trigger_value_cmp &idCmp);
    Trigger_value_cmp datCmp() const;
    void setDatCmp(const Trigger_value_cmp &datCmp);
    Trigger_Lin_Err err() const;
    void setErr(const Trigger_Lin_Err &err);
    quint32 idMin() const;
    void setIdMin(const quint32 &idMin);
    quint32 idMax() const;
    void setIdMax(const quint32 &idMax);
    int saPos() const;
    void setSaPos(int saPos);
    QByteArray minBits() const;
    void setMinBits(const QByteArray &minBits);
    QByteArray maxBits() const;
    void setMaxBits(const QByteArray &maxBits);
    QByteArray maskBits() const;
    void setMaskBits(const QByteArray &maskBits);
    int byteCount() const;
    void setByteCount(int byteCount);
};

class TrigCanCfg : public EngineCfg
{
public:
    Chan mCh;

    int mBaud;      //! hz
    Trigger_Can_Phy mPhy;
    Trigger_Can_Spec mSpec;

    Trigger_Can_When mWhen;
    Trigger_Can_Field mField;
    Trigger_Can_Frame mFrame;
    Trigger_Can_Err mErr;

    Trigger_value_cmp mIdCmp;
    bool mIdCmpMask;

    Trigger_value_cmp mDatCmp;
    bool mDatCmpMask;
    int mSaPos;     //! %

    quint32 mIdMin, mIdMax, mIdMask;

    int        mByteCount;
    QByteArray mDatMin, mDatMax, mDatMask;

public:
    Chan ch() const;
    void setCh(const Chan &ch);
    int baud() const;
    void setBaud(int baud);
    Trigger_Can_Phy phy() const;
    void setPhy(const Trigger_Can_Phy &phy);
    Trigger_Can_Spec spec() const;
    void setSpec(const Trigger_Can_Spec &spec);
    Trigger_Can_When when() const;
    void setWhen(const Trigger_Can_When &when);
    Trigger_Can_Field field() const;
    void setField(const Trigger_Can_Field &field);
    Trigger_Can_Frame frame() const;
    void setFrame(const Trigger_Can_Frame &frame);
    Trigger_Can_Err err() const;
    void setErr(const Trigger_Can_Err &err);
    Trigger_value_cmp idCmp() const;
    void setIdCmp(const Trigger_value_cmp &idCmp);
    bool idCmpMask() const;
    void setIdCmpMask(bool idCmpMask);
    Trigger_value_cmp datCmp() const;
    void setDatCmp(const Trigger_value_cmp &datCmp);
    bool datCmpMask() const;
    void setDatCmpMask(bool datCmpMask);
    int saPos() const;
    void setSaPos(int saPos);
    quint32 idMin() const;
    void setIdMin(const quint32 &idMin);
    quint32 idMax() const;
    void setIdMax(const quint32 &idMax);
    quint32 idMask() const;
    void setIdMask(const quint32 &idMask);
    QByteArray datMin() const;
    void setDatMin(const QByteArray &datMin);
    QByteArray datMax() const;
    void setDatMax(const QByteArray &datMax);
    QByteArray datMask() const;
    void setDatMask(const QByteArray &datMask);
    int getByteCount() const;
    void setByteCount(int value);
};

class TrigFlexrayCfg : public EngineCfg
{
public:
    Chan mCh;

    Trigger_Flex_Baud mBaud;
    Trigger_Flex_Phy mPhy;

    Trigger_Flex_When mWhen;
    Trigger_Flex_Pos mPos;
    Trigger_Flex_Frame mFrame;
    Trigger_Flex_Symbol mSymbol;

    Trigger_Flex_Err mErr;

    Trigger_value_cmp mIdCmp;
    bool mIdCmpMask;

    Trigger_value_cmp mCycleCmp;
    bool mCycleCmpMask;

    int mTssTransmitter;    //! 3~15
    int mCasLowMax;         //! 67~99

    quint32 mIdMin, mIdMax, mIdMask;    //! 11bit
    quint32 mCycleMin, mCycleMax, mCycleMask;   //! 6bit
public:

    Chan ch() const;
    void setCh(const Chan &ch);
    Trigger_Flex_Baud baud() const;
    void setBaud(const Trigger_Flex_Baud &baud);
    Trigger_Flex_Phy phy() const;
    void setPhy(const Trigger_Flex_Phy &phy);
    Trigger_Flex_When when() const;
    void setWhen(const Trigger_Flex_When &when);
    Trigger_Flex_Pos pos() const;
    void setPos(const Trigger_Flex_Pos &pos);
    Trigger_Flex_Frame frame() const;
    void setFrame(const Trigger_Flex_Frame &frame);
    Trigger_Flex_Symbol symbol() const;
    void setSymbol(const Trigger_Flex_Symbol &symbol);
    Trigger_Flex_Err err() const;
    void setErr(const Trigger_Flex_Err &err);
    Trigger_value_cmp idCmp() const;
    void setIdCmp(const Trigger_value_cmp &idCmp);

    bool idCmpMask() const;
    void setIdCmpMask(bool idCmpMask);

    Trigger_value_cmp cycleCmp() const;
    void setCycleCmp(const Trigger_value_cmp &cycleCmp);

    bool cycleCmpMask() const;
    void setCycleCmpMask(bool cycleCmpMask);

    int tssTransmitter() const;
    void setTssTransmitter(int tssTransmitter);
    int casLowMax() const;
    void setCasLowMax(int casLowMax);
    quint32 idMin() const;
    void setIdMin(const quint32 &idMin);
    quint32 idMax() const;
    void setIdMax(const quint32 &idMax);

    quint32 idMask() const;
    void setIdMask(const quint32 &idMask);

    quint32 cycleMin() const;
    void setCycleMin(const quint32 &cycleMin);
    quint32 cycleMax() const;
    void setCycleMax(const quint32 &cycleMax);

    quint32 cycleMask() const;
    void setCycleMask(const quint32 &cycleMask);
};

class TrigSpiCfg : public EngineCfg
{
public:
    Chan mClk, mData, mCs;

    EdgeSlope mClkSlope;
    EdgeSlope mCsSlope;

    Trigger_Spi_CS mType;
    quint64 mTmo;   //! ns

    Trigger_value_cmp mDatCmp;

    quint32 mDataLen;   //! 0 = 1 bit

//    QByteArray mDataMin, mDataMax, mDataMask;
    QBitArray mDataMin, mDataMax, mDataMask;

    static const int bitCountMax = 32;
    static const int bitCountMin = 4;
    static const int bitCountDef = 8;
public:
    Chan clk() const;
    void setClk(const Chan &clk);
    Chan data() const;
    void setData(const Chan &data);
    Chan cs() const;
    void setCs(const Chan &cs);
    Trigger_Spi_CS getType() const;
    void setType(const Trigger_Spi_CS &type);
    quint64 getTmo() const;
    void setTmo(const quint64 &tmo);
    Trigger_value_cmp getDatCmp() const;
    void setDatCmp(const Trigger_value_cmp &datCmp);
    quint32 getDataLen() const;
    void setDataLen(const quint32 &dataLen);
/*
    QByteArray getDataMin() const;
    void setDataMin(const QByteArray &dataMin);
    QByteArray getDataMax() const;
    void setDataMax(const QByteArray &dataMax);
    QByteArray getDataMask() const;
    void setDataMask(const QByteArray &dataMask);

*/
    EdgeSlope getClkSlope() const;
    void setClkSlope(const EdgeSlope &clkSlope);
    EdgeSlope getCsSlope() const;
    void setCsSlope(const EdgeSlope &csSlope);

    QBitArray getDataMin() const;
    void setDataMin(const QBitArray &dataMin);
    QBitArray getDataMax() const;
    void setDataMax(const QBitArray &dataMax);
    QBitArray getDataMask() const;
    void setDataMask(const QBitArray &dataMask);
};

class TrigIICcfg : public EngineCfg
{
public:
    Chan mClk, mData;

    Trigger_I2C_Spec mSpec;

    Trigger_I2C_When mWhen;
    Trigger_I2C_WR mWr;

    Trigger_value_cmp mAddrCmp, mDatCmp;

    Trigger_I2C_Width mWidth;

    quint32 mAddrMin, mAddrMax, mAddrMask;
    QByteArray mDataMin, mDataMax, mDataMask;

public:
    Chan clk() const;
    void setClk(const Chan &clk);
    Chan data() const;
    void setData(const Chan &data);
    Trigger_I2C_Spec spec() const;
    void setSpec(const Trigger_I2C_Spec &spec);
    Trigger_I2C_When when() const;
    void setWhen(const Trigger_I2C_When &when);
    Trigger_I2C_WR wr() const;
    void setWr(const Trigger_I2C_WR &wr);
    Trigger_value_cmp addrCmp() const;
    void setAddrCmp(const Trigger_value_cmp &addrCmp);
    Trigger_value_cmp datCmp() const;
    void setDatCmp(const Trigger_value_cmp &datCmp);
    Trigger_I2C_Width width() const;
    void setWidth(const Trigger_I2C_Width &width);
    quint32 addrMin() const;
    void setAddrMin(const quint32 &addrMin);
    quint32 addrMax() const;
    void setAddrMax(const quint32 &addrMax);
    quint32 addrMask() const;
    void setAddrMask(const quint32 &addrMask);

    QByteArray dataMin() const;
    void setDataMin(const QByteArray &dataMin);
    QByteArray dataMax() const;
    void setDataMax(const QByteArray &dataMax);
    QByteArray dataMask() const;
    void setDataMask(const QByteArray &dataMask);
};

class TrigStd1553bCfg  : public EngineCfg
{
public:
    Chan mCh;
    TriggerPulsePolarity mPolarity;

    Trigger_1553_When mWhen;
    Trigger_1553_Sync mSync;
    Trigger_1553_Err mErr;

    Trigger_value_cmp mDatCmp;
    Trigger_value_cmp mRmtAddrCmp;
    Trigger_value_cmp mSubAddrCmp;
    Trigger_value_cmp mCntCodeCmp;

    quint32 mCmpMin, mCmpMax, mCmpMask;
    quint32 mCsData, mCsMask; //!RTA+11bit
public:
    Chan ch() const;
    void setCh(const Chan &ch);
    Trigger_1553_When when() const;
    void setWhen(const Trigger_1553_When &when);
    Trigger_1553_Sync sync() const;
    void setSync(const Trigger_1553_Sync &sync);
    Trigger_1553_Err err() const;
    void setErr(const Trigger_1553_Err &err);
    Trigger_value_cmp datCmp() const;
    void setDatCmp(const Trigger_value_cmp &datCmp);
    Trigger_value_cmp rmtAddrCmp() const;
    void setRmtAddrCmp(const Trigger_value_cmp &rmtAddrCmp);
    Trigger_value_cmp subAddrCmp() const;
    void setSubAddrCmp(const Trigger_value_cmp &subAddrCmp);
    Trigger_value_cmp cntCodeCmp() const;
    void setCntCodeCmp(const Trigger_value_cmp &cntCodeCmp);
    quint32 cmpMin() const;
    void setCmpMin(const quint32 &cmpMin);
    quint32 cmpMax() const;
    void setCmpMax(const quint32 &cmpMax);
    quint32 cmpMask() const;
    void setCmpMask(const quint32 &cmpMask);

    TriggerPulsePolarity polarity() const;
    void setPolarity(const TriggerPulsePolarity &polarity);
    quint32 csData() const;
    void setCsData(const quint32 &csData);
    quint32 csMask() const;
    void setCsMask(const quint32 &csMask);
};


class TrigABCfg   : public EngineCfg
{
public:
    TrigEdgeCfg    maEdgeCfg;
    TrigPulseCfg   maPulseCfg;

    TrigEdgeCfg    mbEdgeCfg;
    TriggerMode    maType, mbType;

    int            mbCount;
    qint64         mDelay;

public:
    void checkCfg();

    TriggerMode getaType() const;
    void setaType(const TriggerMode &value);
    TriggerMode getbType() const;
    void setbType(const TriggerMode &value);
    int getbCount() const;
    void setbCount(int value);
    qint64 getDelay() const;
    void setDelay(const qint64 &delay);
};


class TrigZoneCfg   : public EngineCfg
{
public:
    struct TZoneDataStru
    {
        bool    en;
        Chan    ch;
        bool    cross;
        qint64  tMin       ;
        qint64  tMax       ;
        qint32  thresholdH ;
        qint32  thresholdL ;

        bool operator !=(const TZoneDataStru &s )
        {
            if(en != s.en)
            {return true;}

            if(ch != s.ch)
            {return true;}

            if(cross != s.cross)
            {return true;}

            if(tMin != s.tMin)
            {return true;}

            if(tMax != s.tMax)
            {return true;}

            if(thresholdH != s.thresholdH)
            {return true;}

            if(thresholdL != s.thresholdL)
            {return true;}

            return false;
        }
    };

public:
    QVector<TZoneDataStru> a_zone;   

    bool operator==(TrigZoneCfg &s )
    {
        if(a_zone.count() != s.a_zone.count())
        {return false;}

        for(int i = 0 ; i < a_zone.count(); i++ )
        {
            if(a_zone[i] != s.a_zone.at(i))
            {
                return false;
            }
        }
        return true;
    }
};


#endif // ENGINECFG_TRIG

