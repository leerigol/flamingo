#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg     m_pViewSession->m_trigger.mABCfg

DsoErr dsoEngine::setTrigABCfg( TrigABCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigABCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigABCfg( TrigABCfg &cfg, IPhyTpu *pTpu )
{
    pTpu->rstIir();

    //!event - a
    switch (cfg.getaType())
    {
    case Trigger_Edge:
        aEventEdgeApply(cfg.maEdgeCfg, pTpu );
        break;

    case Trigger_Pulse:
        aEventPulseApply(cfg.maPulseCfg, pTpu );
        break;

    default:
        break;
    }

    //!event - b
    switch (cfg.getbType())
    {
    case Trigger_Edge:
        bEventEdgeApply(cfg.mbEdgeCfg, pTpu );
        break;
    default:
        break;
    }

    //!common
    pTpu->setTrigTmoOn(   0);
    pTpu->setTrigDelayOn( 1);
    pTpu->setTrigLoopOn(  1);

    pTpu->setEventNum(cfg.mbCount);
    pTpu->setTrigDelay(cfg.mDelay);

    pTpu->setTrigSrc( IPhyTpu::abr );
}


//! a - edge
void dsoEngine::aEventEdgeApply( TrigEdgeCfg &cfg, IPhyTpu *pTpu )
{
    Chan ch;
    ch = cfg.getChan();
    Q_ASSERT( toaCmpSrc_a( ch ) != IPhyTpu::invalid );

    pTpu->aEventWidth();
    pTpu->aEventCmpa( toaCmpSrc_a(ch) );
    pTpu->aEventCmpb( toaCmpSrc_b(ch) );

    EdgeSlope slp = cfg.getSlope();

    if ( slp == Trigger_Edge_Rising )
    {
        pTpu->aEventClkDat(  IPhyTpu::cmp_a, true,
                             IPhyTpu::cmp_a, true );
    }
    else if ( slp == Trigger_Edge_Falling )
    {
        pTpu->aEventClkDat(  IPhyTpu::cmp_b, false,
                             IPhyTpu::cmp_b, false );
    }
    else
    {

    }
}

//! a - Pulse
void dsoEngine::aEventPulseApply( TrigPulseCfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.getChan() );
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->aEventCmpa( src );
    pTpu->aEventCmpb( src );

    pTpu->aEventWidth( toWidthCmp(cfg.getCmp()),
                       cfg.getWidthL(),
                       cfg.getWidthH()
                       );

    if(Trigger_pulse_positive == cfg.getPolarity())
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                            IPhyTpu::cmp_a, true);
    }
    else
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_b, false,
                            IPhyTpu::cmp_b, false);
    }
}

//! b - edge
void dsoEngine::bEventEdgeApply( TrigEdgeCfg &cfg, IPhyTpu *pTpu )
{
    Chan ch;
    ch = cfg.getChan();
    Q_ASSERT( toaCmpSrc_a( ch ) != IPhyTpu::invalid );

    pTpu->bEventWidth();
    pTpu->bEventCmpa( toaCmpSrc_a(ch) );
    pTpu->bEventCmpb( toaCmpSrc_b(ch) );

    EdgeSlope slp = cfg.getSlope();

    if ( slp == Trigger_Edge_Rising )
    {
        pTpu->bEventClkDat(  IPhyTpu::cmp_a, true,
                             IPhyTpu::cmp_a, true );
    }
    else if ( slp == Trigger_Edge_Falling )
    {
        pTpu->bEventClkDat(  IPhyTpu::cmp_b, false,
                             IPhyTpu::cmp_b, false );
    }
    else
    {

    }
}
