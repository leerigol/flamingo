
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mIIcCfg

DsoErr dsoEngine::setTrigIicCfg( TrigIICcfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigIicCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

//!数据长度不得小于 I2C当前数据位宽4字节对齐后 的长度
#define CHECK_DATA_SIZE(data)   do{ \
                                    if(data.size() < (((cfg.mWidth+1) + 3)&(~(3))) )\
                                    Q_ASSERT(false);\
                                    }while(0)

////!数据长度不得小于 I2C数据位宽的最大长度
//#define CHECK_DATA_SIZE(data)  if(data.size() < ( trig_i2c_Width_5+1) ) Q_ASSERT(false)
#define GET_DATA_L(s)          data = (Tpu_reg*)s.data();               Q_ASSERT(data != NULL)
#define GET_DATA_H(s)          data = (Tpu_reg*)(s.data() + 4);         Q_ASSERT(data != NULL)

void dsoEngine::applyTrigIicCfg( TrigIICcfg &cfg, IPhyTpu *pTpu )
{
    //! src
    IPhyTpu::eCmpSrc src = toaCmpSrc( cfg.mClk);
    Q_ASSERT( src != IPhyTpu::invalid );
    Q_ASSERT( src != IPhyTpu::ac );

    pTpu->rstIir();

    //!source
    pTpu->aEventCmpa(toaCmpSrc(cfg.mClk ));
    pTpu->aEventClkDat(IPhyTpu::cmp_a, false,
                       IPhyTpu::cmp_a, false
                       );

    pTpu->bEventCmpa(toaCmpSrc(cfg.mData));
    pTpu->bEventClkDat(IPhyTpu::cmp_a, false,
                       IPhyTpu::cmp_a, false
                       );

    pTpu->setTrigSrc( IPhyTpu::iic );

    //!Iic
    pTpu->setiic_cfg_1_iic_trig_type(cfg.mWhen);


//    if( (cfg.mWhen == trig_i2c_addr)||(cfg.mWhen == trig_i2c_addr_data) )
//    {
        pTpu->setiic_cfg_1_iic_trig_addr_type(cfg.mAddrCmp);

        if(trig_i2c_addr8 == cfg.mSpec)
        {
            pTpu->setiic_cfg_1_iic_trig_wr_type(cfg.mAddrMin & 0X00000001);
            pTpu->setiic_cfg_1_iic_addr_length(trig_i2c_addr7            );
            pTpu->setiic_cfg_2_iic_addr_setmin(cfg.mAddrMin   >>1        );
            pTpu->setiic_cfg_2_iic_addr_setmax(cfg.mAddrMax   >>1        );
            pTpu->setiic_cfg_2_iic_addr_setmask(cfg.mAddrMask >>1        );
        }
        else
        {
            pTpu->setiic_cfg_1_iic_trig_wr_type(cfg.mWr);
            pTpu->setiic_cfg_1_iic_addr_length(cfg.mSpec);
            pTpu->setiic_cfg_2_iic_addr_setmin(cfg.mAddrMin);
            pTpu->setiic_cfg_2_iic_addr_setmax(cfg.mAddrMax);
            pTpu->setiic_cfg_2_iic_addr_setmask(cfg.mAddrMask);
        }
//    }

//    if( (cfg.mWhen == trig_i2c_data) || (cfg.mWhen == trig_i2c_addr_data) )
//    {
        pTpu->setiic_cfg_1_iic_trig_dat_type(cfg.mDatCmp);
        pTpu->setiic_cfg_1_iic_dat_length(cfg.mWidth);
/*
        CHECK_DATA_SIZE(cfg.mDataMin);
        CHECK_DATA_SIZE(cfg.mDataMax);
        CHECK_DATA_SIZE(cfg.mDataMask);

        Tpu_reg  *data = NULL;

        GET_DATA_L(cfg.mDataMin);
        pTpu->setiic_cfg_3_iic_dat_setmin_bits31_0(*data);
        GET_DATA_L(cfg.mDataMax);
        pTpu->setiic_cfg_4_iic_dat_setmax_bits31_0(*data);
        GET_DATA_L(cfg.mDataMask);
        pTpu->setiic_cfg_5_iic_dat_setmask_bits31_0(*data);

        if(cfg.mWidth > trig_i2c_Width_4)
        {
            GET_DATA_H(cfg.mDataMin);
            pTpu->setiic_cfg_6_iic_dat_setmin_bits39_32(*data);
            GET_DATA_H(cfg.mDataMax);
            pTpu->setiic_cfg_6_iic_dat_setmax_bits39_32(*data);
            GET_DATA_H(cfg.mDataMask);
            pTpu->setiic_cfg_6_iic_dat_setmask_bits39_32(*data);
        }
*/
        Q_ASSERT(cfg.mDataMin.count() >= 5);
        Q_ASSERT(cfg.mDataMax.count() >= 5);
        Q_ASSERT(cfg.mDataMask.count()>= 5);

        Tpu_reg   datMinL  = 0;
        Tpu_reg   datMinH  = 0;

        Tpu_reg   datMaxL  = 0;
        Tpu_reg   datMaxH  = 0;

        Tpu_reg   datMaskL  = 0;
        Tpu_reg   datMaskH  = 0;

        for(int i = 0; i<4; i++)
        {
            datMinL  |= (cfg.mDataMin[i]<<(i*8));
            datMaxL  |= (cfg.mDataMax[i]<<(i*8));
            datMaskL |= (cfg.mDataMask[i]<<(i*8));
        }
        int i = 0;
        datMinH  |= (cfg.mDataMin[i+4]<<((i*8)));
        datMaxH  |= (cfg.mDataMax[i+4]<<(i*8));
        datMaskH |= (cfg.mDataMask[i+4]<<(i*8));

//        qDebug()<<__FILE__<<__LINE__<<datMinL<<datMinH<<datMaxL
//                                    <<datMaxH<<datMaskL<<datMaskH;

        pTpu->setiic_cfg_3_iic_dat_setmin_bits31_0(  datMinL  );
        pTpu->setiic_cfg_4_iic_dat_setmax_bits31_0(  datMaxL  );
        pTpu->setiic_cfg_5_iic_dat_setmask_bits31_0( datMaskL );

        pTpu->setiic_cfg_6_iic_dat_setmin_bits39_32( datMinH  );
        pTpu->setiic_cfg_6_iic_dat_setmax_bits39_32( datMaxH  );
        pTpu->setiic_cfg_6_iic_dat_setmask_bits39_32(datMaskH );
//    }
}
