
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mNEdgeCfg

void dsoEngine::enterTrigNEdge()
{}
void dsoEngine::exitTrigNEdge()
{}

DsoErr dsoEngine::setTrigNEdgeCfg( TrigNEdgeCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigNEdgeCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigNEdgeCfg( TrigNEdgeCfg &cfg, IPhyTpu *pTpu )
{
    pTpu->rstIir();

    //! a信号的比较源设置
    pTpu->aEventCmpa( toaCmpSrc_a(cfg.mCh),
                      toCmpMode(cfg.mSlope));

    pTpu->aEventCmpb( toaCmpSrc_a(cfg.mCh),
                      toCmpMode(cfg.mSlope) );

    if( cfg.mSlope == Trigger_Edge_Rising )
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                            IPhyTpu::cmp_a, true );
    }
    else
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_b, false,
                            IPhyTpu::cmp_b, false );
    }

    //! a   b
    pTpu->aEventWidth();
    pTpu->bEventWidth();

    pTpu->settpu_trig_mux_abr_seq_loop_en(    1 );
    pTpu->settpu_trig_mux_abr_seq_delay_on(   1 );
    pTpu->settpu_trig_mux_abr_seq_timeout_on( 1 );
    pTpu->settpu_trig_mux_abr_seq_r_event_on( 0 );

    pTpu->settpu_trig_mux_seq_abr_trig_a_only(1);

    pTpu->setabr_a_event_num( cfg.mNum);
    pTpu->setTrigTmo(cfg.mIdleTime);

    pTpu->setTrigSrc( IPhyTpu::abr );
}
