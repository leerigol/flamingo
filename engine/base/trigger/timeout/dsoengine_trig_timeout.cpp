
#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mTimeoutCfg

void dsoEngine::enterTrigTimeout()
{}
void dsoEngine::exitTrigTimeout()
{}

DsoErr dsoEngine::setTrigTimeoutCfg( TrigTimeoutCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigTimeoutCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}
void dsoEngine::applyTrigTimeoutCfg( TrigTimeoutCfg &cfg, IPhyTpu *pTpu )
{
    pTpu->rstIir();

    pTpu->aEventCmpa( toaCmpSrc_a(cfg.mCh) );
    pTpu->bEventCmpa( toaCmpSrc_a(cfg.mCh) );

    pTpu->aEventCmpb( toaCmpSrc_b(cfg.mCh) );
    pTpu->bEventCmpb( toaCmpSrc_b(cfg.mCh) );

    pTpu->aEventWidth( IPhyTpu::tmo, cfg.mWidth );
    pTpu->bEventWidth( IPhyTpu::tmo, cfg.mWidth );

    if(Trigger_pulse_positive == cfg.mPolarity)
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                            IPhyTpu::cmp_a, false );
        pTpu->setTrigSrc( IPhyTpu::a );
    }
    else if(Trigger_pulse_negative == cfg.mPolarity)
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_a, true,
                            IPhyTpu::cmp_a, true );
        pTpu->setTrigSrc( IPhyTpu::a );
    }
    else
    {
        pTpu->aEventClkDat( IPhyTpu::cmp_a, false,
                            IPhyTpu::cmp_a, false );
        pTpu->bEventClkDat( IPhyTpu::cmp_b, true,
                            IPhyTpu::cmp_b, true );
        pTpu->setTrigSrc( IPhyTpu::a_or_b );
    }
}
