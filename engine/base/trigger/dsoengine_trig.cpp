#include "../dsoengine.h"

#include "./dsoengine_trig.h"

IPhyTpu* dsoEngine::getTpu()
{
    Q_ASSERT( NULL != m_pPhy );

    return &m_pPhy->mTpu;
}

DsoErr dsoEngine::setTrigHolder(TriggerHoldObj holder)
{
    m_pPhy->mTpu.setholdoff_time_h_holdoff_type(
                holder == Trigger_Hold_Event?
                IPhyTpu::event_hold : IPhyTpu::time_hold );

    return ERR_NONE;
}

DsoErr dsoEngine::setTrigHoldTime(quint64 time)
{
    m_pPhy->mTpu.setHoldoffTime( time );

    return ERR_NONE;
}

DsoErr dsoEngine::setTrigHoldNum( quint32 num )
{
    m_pPhy->mTpu.setholdoff_event_num( num );

    return ERR_NONE;
}

DsoErr dsoEngine::setTrigSweep( TriggerSweep sweep )
{
    m_pViewSession->m_trigger.setSweep( sweep );
LOG_DBG();
    return ERR_NONE;
}

DsoErr dsoEngine::applyTrigSweep( TriggerSweep sweep )
{
    if ( sweep == Trigger_Sweep_Normal )
    {
        m_pPhy->mCcu.setMODE_auto_trig( 0 );
        m_pPhy->mCcu.setCTRL_run_single( 0  );
    }
    else if ( sweep == Trigger_Sweep_Auto )
    {
        m_pPhy->mCcu.setMODE_auto_trig( 1 );
        m_pPhy->mCcu.setCTRL_run_single( 0  );
    }
    else if ( sweep == Trigger_Sweep_Single )
    {
        m_pPhy->mCcu.setMODE_auto_trig( 0 );
        m_pPhy->mCcu.setCTRL_run_single( 1 );
    }
    else
    {
        Q_ASSERT( false );
    }

    return ERR_NONE;
}

DsoErr dsoEngine::setTrigCoupling( Chan ch, Coupling coup )
{
    Q_ASSERT( ch >= chan1 && ch <= chan4 );
    Q_ASSERT( coup != GND );

    m_pPhy->mTpu.setIirSrc( toChSeq(ch) );
    m_pPhy->mTpu.setiir_set_iir_type( toIir(coup) );
    if( (AC == coup) || (LF == coup) )
    {
        m_pPhy->mSpuGp.setCTRL_coarse_trig_range(1);
    }
    else
    {
         m_pPhy->mSpuGp.setCTRL_coarse_trig_range(0);
    }
    return ERR_NONE;
}

DsoErr dsoEngine::setTrigForce()
{
    m_pPhy->mCcu.outTRACE_trace_once_req(1);
    m_pPhy->mTpu.setTrigForce();
    dsoEngine::m_bTraceRequestOnce = true;
    return ERR_NONE;
}

DsoErr dsoEngine::setTrigSingle()
{
    m_pPhy->mCcu.setCTRL_run_single( 1 );
    return ERR_NONE;
}

DsoErr dsoEngine::setTrigCHEn( Chan ch, bool bEn )
{
    if ( ch >= chan1 && ch <= chan4 )
    {
        m_pViewSession->m_ch[ ch - chan1 ].setTrigOnOff( bEn );
        return ERR_NONE;
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        m_pViewSession->mLa.setTrigOnOff( bEn );
        return ERR_NONE;
    }
    //! other no action
    else
    { return ERR_NONE; }
}

DsoErr dsoEngine::setDvmCountCHEn(Chan ch, bool bEn)
{
    if ( ch >= chan1 && ch <= chan4 )
    {
        m_pViewSession->m_ch[ ch - chan1 ].setDvmCountOnOff( bEn );
    }

    return ERR_NONE;
}

DsoErr dsoEngine::setTrigCHsRst()
{
    for ( int i = 0; i < array_count(m_pViewSession->m_ch); i++ )
    { m_pViewSession->m_ch[ i ].setTrigOnOff( false ); }
    m_pViewSession->mLa.setTrigOnOff( false );

    return ERR_NONE;
}

//! ch l h
DsoErr dsoEngine::setTrigLevelA( EnginePara &para )
{
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int l = para[1]->s32Val;
    int h = para[2]->s32Val;

    l = toAdc( ch, l );
    h = toAdc( ch, h );

    return _setTrigLevelA( ch, l, h );
}
DsoErr dsoEngine::setTrigLevelB( EnginePara &para )
{
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int l = para[1]->s32Val;
    int h = para[2]->s32Val;

    l = toAdc( ch, l );
    h = toAdc( ch, h );

    return _setTrigLevelB( ch, l, h );
}

DsoErr dsoEngine::setTrigLevelAAdc( EnginePara &para )
{
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int l = para[1]->s32Val;
    int h = para[2]->s32Val;

    return _setTrigLevelA( ch, l, h );
}
DsoErr dsoEngine::setTrigLevelBAdc( EnginePara &para )
{
    //! unpack value
    Chan ch = (Chan)para[0]->s32Val;
    int l = para[1]->s32Val;
    int h = para[2]->s32Val;

    return _setTrigLevelB( ch, l, h );
}

//! 范围限制本该[0,255],由于TPU边界有问题，限制为[1,254]
//! 又由于校准问题，会导致服务中的计算的范围与pty中不一致，所以限制到[5,250]，超出范围时强制设置dx = 5 。
#define trigAdcMax (250)
#define trigAdcMin (5)


//! a 是adc数值
DsoErr dsoEngine::_setTrigLevelA( Chan ch,
                                     int l, int h )
{
    h = qBound(trigAdcMin, h, 254);
    l = qBound(1, l, trigAdcMax);
    if(h <= l)
    {
        qDebug()<<"waring adcH <= adcL:"<<h<<l;
    }

    if ( ch >= chan1 && ch <= chan4 )
    {
        int gnd = adc_center /*m_pPhy->m_ch[ ch - chan1 ].getAfeGnd()*/;
        m_pPhy->mTpu.setiir_set_iir_offset(gnd);
        //qDebug()<<__FILE__<<__LINE__<<"chan:"<<ch<<"adc L:"<<l<<"adc H:"<<h<<"gnd:"<<gnd;
        m_pPhy->mTpu.setLevelA( (IPhyTpu::eChSeq)(ch - chan1 + IPhyTpu::c_a1),
                                l, h );
    }
    else
    {
        //Q_ASSERT( false );
        qDebug()<<"warnning invalid chan:" << ch;
    }

    return ERR_NONE;
}

DsoErr dsoEngine::_setTrigLevelB( Chan ch,
                                     int l, int h )
{
    h = qBound(trigAdcMin, h, 254);
    l = qBound(1, l, trigAdcMax);
    if(h <= l)
    {
        qDebug()<<"waring adcH <= adcL:"<<h<<l;
    }


    if ( ch >= chan1 && ch <= chan4 )
    {
        int gnd = adc_center /*m_pPhy->m_ch[ ch - chan1 ].getAfeGnd()*/;
        m_pPhy->mTpu.setiir_set_iir_offset(gnd);

        m_pPhy->mTpu.setLevelB( (IPhyTpu::eChSeq)(ch - chan1 + IPhyTpu::c_a1),
                                l,
                                h );
    }
    else
    {
        Q_ASSERT( false );
    }

    return ERR_NONE;
}

DsoErr dsoEngine::setExtTrigLevel( int lev, int sens )
{
    m_pPhy->mExt.setLevel( lev, sens );

    return ERR_NONE;
}
DsoErr dsoEngine::setExt5TrigLevel( int /*lev*/, int /*sens*/ )
{
    return ERR_NONE;
}

IPhyTpu::eCmpSrc dsoEngine::toaCmpSrc( Chan ch )
{
    return toaCmpSrc_a( ch );
}

IPhyTpu::eCmpSrc dsoEngine::toaCmpSrc_a( Chan ch )
{
    IPhyTpu::eCmpSrc src;

    if ( ch >= chan1 && ch <= chan4 )
    {
        src = (IPhyTpu::eCmpSrc)(IPhyTpu::ch1_a + (ch - chan1) * 2);
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        src = (IPhyTpu::eCmpSrc)(IPhyTpu::la0 + ch - d0);
    }
    else if ( ch == ext )
    {
        src = IPhyTpu::ext;
    }
    else if ( ch == acline )
    {
        src = IPhyTpu::ac;
    }
    else if ( ch == dg1_sync )
    {
        src = IPhyTpu::dg1_sync;
    }
    else if ( ch == dg2_sync )
    {
        src = IPhyTpu::dg2_sync;
    }
    else
    {
        src = IPhyTpu::invalid;
    }

    return src;
}

//! only for analog ch
IPhyTpu::eCmpSrc dsoEngine::toaCmpSrc_b( Chan ch )
{
    IPhyTpu::eCmpSrc src;

    if ( ch >= chan1 && ch <= chan4 )
    {
        src = (IPhyTpu::eCmpSrc)(IPhyTpu::ch1_b + (ch - chan1) * 2);
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        src = (IPhyTpu::eCmpSrc)(IPhyTpu::la0 + ch - d0);
    }
    else if ( ch == ext )
    {
        src = IPhyTpu::ext;
    }
    else if ( ch == acline )
    {
        src = IPhyTpu::ac;
    }
    else if ( ch == dg1_sync )
    {
        src = IPhyTpu::dg1_sync;
    }
    else if ( ch == dg2_sync )
    {
        src = IPhyTpu::dg2_sync;
    }
    else
    {
        src = IPhyTpu::invalid;
        Q_ASSERT( false );
    }

    return src;
}

//! only a
IPhyTpu::eCmpSrc dsoEngine::toaCmpSrc( IPhyTpu::eChSeq ch )
{
    if ( ch >= IPhyTpu::c_a1 && ch <= IPhyTpu::c_a4 )
    {
        return (IPhyTpu::eCmpSrc)(IPhyTpu::ch1_a + (ch - IPhyTpu::ch1_a) * 2);
    }
    else if ( ch >= IPhyTpu::c_d0 && ch <= IPhyTpu::c_d15 )
    {
        return (IPhyTpu::eCmpSrc)( IPhyTpu::la0 + ch - IPhyTpu::c_d0 );
    }
    else if ( ch == IPhyTpu::c_ext )
    {
        return IPhyTpu::ext;
    }
    else
    {
        Q_ASSERT( false );
        return IPhyTpu::ch1_a;
    }
}

IPhyTpu::eCmpMode dsoEngine::toCmpMode( EdgeSlope slp )
{
    switch( slp )
    {
    case Trigger_Edge_Rising:
        return IPhyTpu::rise;

    case Trigger_Edge_Falling:
        return IPhyTpu::fall;

    case Trigger_Edge_Any:
        return IPhyTpu::any;

    default:
        qWarning()<<"!!!slope any";
        return IPhyTpu::any;
    }
}

IPhyTpu::eWidthCmp dsoEngine::toWidthCmp( EMoreThan mt )
{
    if ( mt == Trigger_When_Morethan )
    { return IPhyTpu::gt; }
    else if ( mt == Trigger_When_Lessthan )
    { return IPhyTpu::lt; }
    else if ( mt == Trigger_When_MoreLess )
    { return IPhyTpu::in_range; }
    else if ( mt == Trigger_When_UnMoreLess )
    { return IPhyTpu::out_range; }
    else
    {
        Q_ASSERT(false);
        return IPhyTpu::gt;
    }
}

IPhyTpu::eChSeq dsoEngine::toChSeq( Chan ch )
{
    if ( ch >= chan1 && ch <= chan4 )
    {
        return (IPhyTpu::eChSeq)( ch - chan1 + IPhyTpu::c_a1 );
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        return (IPhyTpu::eChSeq)( ch - d0 + IPhyTpu::c_d0 );
    }
    else if ( ch == ext || ch == ext5 )
    { return IPhyTpu::c_ext; }
    else
    { Q_ASSERT(false);}

    return IPhyTpu::c_a1;
}

//Trigger_pat_h,
//Trigger_pat_l,
//Trigger_pat_x,
//Trigger_pat_rise,
//Trigger_pat_fall,

//logic_1 = 0,
//logic_0,
//logic_pass,
//logic_invert,
IPhyTpu::eLogicSel dsoEngine::toLogicSel( TriggerLogicOperator op,
                               TriggerPattern patt)
{
    //! valid patt
    if ( patt >= Trigger_pat_h && patt <= Trigger_pat_fall )
    {}
    else
    {
        Q_ASSERT( false );
        return IPhyTpu::logic_1;
    }

    IPhyTpu::eLogicSel andSels[]={
                                    IPhyTpu::logic_pass,
                                    IPhyTpu::logic_invert,
                                    IPhyTpu::logic_1,
                                    IPhyTpu::logic_1,
                                    IPhyTpu::logic_1,
                                    };

    IPhyTpu::eLogicSel orSels[]={
                                    IPhyTpu::logic_pass,
                                    IPhyTpu::logic_invert,
                                    IPhyTpu::logic_0,
                                    IPhyTpu::logic_0,
                                    IPhyTpu::logic_0,
                                    };

    if ( op == Trigger_logic_and )
    {
        return andSels[ patt - Trigger_pat_h ];
    }
    else if ( op == Trigger_logic_or )
    {
        return orSels[ patt - Trigger_pat_h ];
    }
    else if ( op == Trigger_logic_nand )
    {
        return andSels[ patt - Trigger_pat_h ];
    }
    else if ( op == Trigger_logic_nor )
    {
        return orSels[ patt - Trigger_pat_h ];
    }
    else
    {
        Q_ASSERT( false );
        return IPhyTpu::logic_1;
    }
}

IPhyTpu::eLogicOp dsoEngine::toLogicOp( TriggerLogicOperator op )
{
    if ( Trigger_logic_and == op )
    { return IPhyTpu::op_and; }
    else if ( Trigger_logic_or == op )
    { return IPhyTpu::op_or; }
    else
    {
        Q_ASSERT(false);
        return IPhyTpu::op_and;
    }
}

IPhyTpu::eIirType dsoEngine::toIir( Coupling coup )
{
    if ( coup == DC )
    { return IPhyTpu::iir_dc; }
    else if ( coup == AC )
    { return IPhyTpu::iir_ac; }
    else if ( coup == LF )
    { return IPhyTpu::iir_lf; }
    else if ( coup == HF )
    { return IPhyTpu::iir_hf; }
    else
    { Q_ASSERT( false ); }

    return IPhyTpu::iir_dc;
}



