#include "../../dsoengine.h"
#include "../dsoengine_trig.h"

#define selfTrigCfg    m_pViewSession->m_trigger.mStd1553bCfg

DsoErr dsoEngine::setTrigStd1553bCfg( TrigStd1553bCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    selfTrigCfg = *pCfg;

    applyTrigStd1553bCfg( selfTrigCfg, getTpu() );

    return ERR_NONE;
}

void dsoEngine::applyTrigStd1553bCfg(TrigStd1553bCfg &cfg, IPhyTpu *pTpu)
{
    //!reset
    pTpu->rstIir();

    //! src
    pTpu->rEventCmpa( toaCmpSrc_a(cfg.mCh) );
    pTpu->rEventCmpb( toaCmpSrc_b(cfg.mCh) );

    pTpu->rEventClkDat(IPhyTpu::cmp_a, (cfg.mPolarity==Trigger_pulse_positive)? IPhyTpu::rise :IPhyTpu::fall,
                       IPhyTpu::cmp_b, (cfg.mPolarity==Trigger_pulse_positive)? IPhyTpu::rise :IPhyTpu::fall
                       );
    pTpu->setTrigSrc(IPhyTpu::mil1553b);

    quint32 cmpMin = 0, cmpMax = 0, cmpMask = 0;
    if( (trig_1553_cmd == cfg.mWhen) ||
        (trig_1553_status == cfg.mWhen) )
    {
        cmpMin  = cfg.mCsData;
        cmpMax  = cfg.mCsData;
        cmpMask = cfg.mCsMask;
    }
    else if(trig_1553_data == cfg.mWhen)
    {
        cmpMin  = cfg.mCmpMin;
        cmpMax  = cfg.mCmpMax;
        cmpMask = cfg.mCmpMask;
    }

    pTpu->setstd1553b_cfg_1_std1553b_trig_type(cfg.mWhen);
    pTpu->setstd1553b_cfg_1_std1553b_trig_sync_type(cfg.mSync);
    pTpu->setstd1553b_cfg_1_std1553b_trig_dat_type(cfg.datCmp());
    pTpu->setstd1553b_cfg_1_std1553b_trig_remote_addr_type(cfg.mRmtAddrCmp);
    pTpu->setstd1553b_cfg_1_std1553b_trig_subaddr_mode_type(cfg.mSubAddrCmp);
    pTpu->setstd1553b_cfg_1_std1553b_trig_count_code_type(cfg.mCntCodeCmp);
    pTpu->setstd1553b_cfg_1_std1553b_trig_err_type(cfg.mErr);

    pTpu->setstd1553b_cfg_2_std1553b_trig_word_mask(cmpMask);
    pTpu->setstd1553b_cfg_3_std1553b_trig_dat_cmp_l(cmpMin);
    pTpu->setstd1553b_cfg_4_std1553b_trig_dat_cmp_g(cmpMax);
}
