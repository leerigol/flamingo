#include "../dsoengine.h"

#define selfHoriMain    m_pViewSession->m_horizontal.getMainView()
#define selfHoriZoom    m_pViewSession->m_horizontal.getZoomView()

//! vertical attr
void dsoEngine::setTrace_vAttr(Chan ch, DsoWfm *pWfm , CDsoSession *pSes)
{
    Q_ASSERT( NULL != pWfm );

    //! default ch
    int chId;
    if ( ch >= chan1 && ch <= chan4 )
    {
        chId = ch - chan1;
    }
    else if ( ch >= eye_ch1 && ch <= eye_ch4 )
    {
        chId = ch - eye_ch1;
    }
    else
    {
        Q_ASSERT( false );
    }

    if(pWfm->size() == 0)
    {
        pWfm->init( 1024 );
    }
    //! data vert attr
    pWfm->setyGnd(  pSes->m_ch[ chId ].getAfeGnd() );
    pWfm->setyInc( dsoFract( pSes->m_ch[ chId ].getAfeScale(), adc_vdiv_dots) );
    pWfm->setInvert( false );
    pWfm->setyDiv( dsoFract( adc_vdiv_dots * pSes->m_ch[ chId ].getScale(),
                             pSes->m_ch[ chId ].getAfeScale()) );

    //! data vert raw attr
    pWfm->setyRefScale( pSes->m_ch[ chId ].getScale() );
    pWfm->setyRefOffset( pSes->m_ch[ chId ].getOffset() );

    //! add the probe info
    DsoReal probeRatio = mViewSession.m_ch[ chId ].getProbeRatio();
    probeRatio.addE( E_N6 );
    pWfm->setyRefBase( probeRatio );
}

void dsoEngine::setTrace_probe(Chan chId, DsoWfm *pWfm)
{
    //! add the probe info
    DsoReal probeRatio = mViewSession.m_ch[ chId ].getProbeRatio();
    probeRatio.addE( E_N6 );
    if(probeRatio != pWfm->getyRefBase() )
    {
        pWfm->setyRefBase( probeRatio );
        pWfm->toValue();
    }
}



//! remove the no data channel
//! invalid chan = not on
void dsoEngine::requestTrim( traceRequest &req, CDsoSession *pSes )
{
    Q_ASSERT( NULL != pSes );

    bool bViewEns[2];

    bViewEns[0] = true;
    Q_ASSERT( NULL != m_pViewSession );
    bViewEns[1] = m_pViewSession->m_horizontal.getViewMode() == Horizontal_Zoom;

    for ( int i = 0; i < 2; i++ )
    {
        requestWfmTrim( req.mChWfms[i], chan1, chan4, pSes, bViewEns[i] );
        requestWfmTrim( req.mDxWfms[i], d0, d15, pSes, bViewEns[i] );

        requestWfmTrim( req.mLaWfms[i], la, la, pSes, bViewEns[i] );
    }

    //! no digi and eye in stop
    if ( engine::getActiveEngine() != m_pRecEngine )
    {
 //       req.mDigiWfms.clear();
        req.mEyeWfms.clear();
    }
    else
    {
        requestWfmTrim( req.mDigiWfms, digi_ch1, digi_ch4, pSes );
        requestWfmTrim( req.mDigiWfms, digi_ch1_l, digi_ch4_l, pSes );

        requestWfmTrim( req.mEyeWfms, eye_ch1, eye_ch4, pSes );
    }
}

void dsoEngine::requestWfmTrim( ChanWfmMap &wfmMap,
                                Chan chFrom, Chan chEnd,
                                CDsoSession *pSes,
                                bool bViewValid )
{
    Q_ASSERT( NULL != pSes );

    Chan ch;

    for ( int i = chFrom; ( ch = (Chan)i, i <= chEnd ); i++ )
    {
        //! ch valid
        if ( pSes->getSaCHValid( ch ) && bViewValid )
        {
        }
        //! no data
        else
        {
            if ( wfmMap.contains( ch ) )
            {
                Q_ASSERT( NULL != wfmMap[ch] );
                wfmMap[ch]->setPoint( NULL, 0, 0 );
            }
            else
            {}

            wfmMap.remove( ch );
        }
    }
}

void dsoEngine::requestAttachAddr( traceRequest &req,
                                   CDsoSession *pSes )
{
    //! main
    requestWfmAttachAddr( req.mChWfms[0], pSes );
    requestWfmAttachAddr( req.mDxWfms[0], pSes );
    requestWfmAttachAddr( req.mDigiWfms, pSes );

    requestWfmAttachAddr( req.mEyeWfms, pSes );
    if(!req.mLaWfms[0].isEmpty())
    {
        req.mLaWfms[0].value( la )->setAddr( m_pPhy->mZynqFcuWr.getLaAddr( 0 ) );
    }
    //! zoom
    requestWfmAttachAddr( req.mChWfms[1], pSes );
    requestWfmAttachAddr( req.mDxWfms[1], pSes );
    if(!req.mLaWfms[1].isEmpty())
    {
        req.mLaWfms[1].value( la )->setAddr( m_pPhy->mZynqFcuWr.getLaAddr( 1 ) );
    }
}

//! only valid chs
void dsoEngine::requestWfmAttachAddr( ChanWfmMap &wfmMap,
                           CDsoSession *pSes )
{
    Q_ASSERT( NULL != pSes );

    quint32 phyAddr;
    QMapIterator<Chan, DsoWfm*> iter( wfmMap );
    while (iter.hasNext())
    {
        iter.next();
        Q_ASSERT( iter.value()!=NULL );

        phyAddr = m_pPhy->mZynqFcuWr.getCHAddr( iter.key(), pSes->m_acquire.m_chCnt );
        Q_ASSERT( 0 != phyAddr );
        iter.value()->setAddr( phyAddr );
    }
}

////! only for the valid ch
//quint32 dsoEngine::getWfmPhyAddr( Chan ch, CDsoSession *pSes )
//{
//    Q_ASSERT( NULL != pSes );

//    return m_pPhy->mZynqFcuWr.getCHAddr( ch, pSes->m_acquire.m_chCnt );
//}

void dsoEngine::requestSetAttr( traceRequest &req,
                                CDsoSession *pSes )
{
    requestSetAttr_main( req, pSes );
    requestSetAttr_zoom( req, pSes );
}

//! main
void dsoEngine::requestSetAttr_main( traceRequest &req,
                                CDsoSession *pSes )
{
    do
    {
        //! ch
        QMapIterator<Chan, DsoWfm*> iter( req.mChWfms[0] );
        while (iter.hasNext())
        {
            iter.next();
            setTrace_vAttr( iter.key(), iter.value(), pSes );

            iter.value()->sett( m_pViewSession->m_acquire.mMainCHView.getFlow().getT0(),
                                m_pViewSession->m_acquire.mMainCHView.getFlow().getDotTime() );

            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );

    //! digit trace fill
    do
    {
        QMapIterator<Chan, DsoWfm*> iter( req.mDigiWfms );
        while (iter.hasNext())
        {
            iter.next();

            iter.value()->sett(
                                m_pViewSession->m_acquire.mMainCHView.getFlow().getT0(),
                                m_pViewSession->m_acquire.mMainCHView.getFlow().getDotTime()
                                );

            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );

    //! dx trace fill
    do
    {
        QMapIterator<Chan, DsoWfm*> iter( req.mDxWfms[0] );
        while (iter.hasNext())
        {
            iter.next();

            iter.value()->sett(
                        m_pViewSession->m_acquire.mMainCHView.getFlow().getT0(),
                        m_pViewSession->m_acquire.mMainCHView.getFlow().getDotTime()
                        );


            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );

    //! la trace fill
    do
    {
        QMapIterator<Chan, DsoWfm*> iter( req.mLaWfms[0] );
        while (iter.hasNext())
        {
            iter.next();

            iter.value()->sett(
                        m_pViewSession->m_acquire.mMainCHView.getFlow().getT0(),
                        m_pViewSession->m_acquire.mMainCHView.getFlow().getDotTime()
                        );

            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );

    //! eye trace fill
    do
    {
        QMapIterator<Chan, DsoWfm*> iter( req.mEyeWfms );
        while (iter.hasNext())
        {
            iter.next();
            setTrace_vAttr( iter.key(), iter.value(), pSes);

            iter.value()->sett(
                        m_pViewSession->m_acquire.mEyeView.getFlow().getT0(),
                        m_pViewSession->m_acquire.mEyeView.getFlow().getDotTime()
                        );


            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );
}
//! zoom
void dsoEngine::requestSetAttr_zoom( traceRequest &req,
                                CDsoSession *pSes )
{
    //! ch trace fill
    do
    {
        //! ch
        QMapIterator<Chan, DsoWfm*> iter( req.mChWfms[1] );
        while (iter.hasNext())
        {
            iter.next();
            setTrace_vAttr( iter.key(), iter.value(), pSes);

            iter.value()->sett(
                                m_pViewSession->m_acquire.mZoomCHView.getFlow().getT0(),
                                m_pViewSession->m_acquire.mZoomCHView.getFlow().getDotTime()
                                 );

            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );

    //! dx trace fill
    do
    {
        QMapIterator<Chan, DsoWfm*> iter( req.mDxWfms[1] );
        while (iter.hasNext())
        {
            iter.next();

            iter.value()->sett(
                        m_pViewSession->m_acquire.mZoomCHView.getFlow().getT0(),
                        m_pViewSession->m_acquire.mZoomCHView.getFlow().getDotTime()
                        );

            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );

    //! la trace fill
    do
    {
        QMapIterator<Chan, DsoWfm*> iter( req.mLaWfms[1] );
        while (iter.hasNext())
        {
            iter.next();

            //! la view == ch view
            iter.value()->sett(
                        m_pViewSession->m_acquire.mZoomCHView.getFlow().getT0(),
                        m_pViewSession->m_acquire.mZoomCHView.getFlow().getDotTime()
                        );

            iter.value()->setAttrId( dsoEngine::getSysConfigId() );
        }
    }while ( 0 );
}

DsoErr dsoEngine::requestData( traceRequest &req,
                               int tmous,
                               CDsoSession *pSes )
{
    Q_ASSERT( NULL != pSes );

    if(m_bTraceRequest || m_bTraceRequestOnce)
    {
        m_btraceRunning = true;
    }
    else
    {
        m_btraceRunning = false;
        //! 停止获取trace
        return ERR_NO_TRACE;
    }

    //! ch count
    req.mChCnt = pSes->m_acquire.m_chCnt;

    //! get data
    if(! dsoEngine::m_bTraceRequestOnce )
    {
        m_pPhy->m_trace.m_pZynqFcuWr->endTrace();
        m_pPhy->mCcu.outTRACE_trace_once_req( 1 );
    }
    req.mRequestOnce = dsoEngine::m_bTraceRequestOnce;
    dsoEngine::m_bTraceRequestOnce = false;

    //! 设置wfm数据的属性
    requestSetAttr( req, pSes );
    req.setChLength( m_pViewSession->m_acquire.mMainCHView.getFlow().getDepth().ceilLonglong(), 0 );

    req.setLaLength( m_pViewSession->m_acquire.mMainLAView.getFlow().getDepth().ceilLonglong(), 0 );

    req.setEyeLength( m_pHistSession->m_acquire.mEyeView.getFlow().getDepth().ceilLonglong() );

    //! length -- zoom
    req.setChLength( m_pViewSession->m_acquire.mZoomCHView.getFlow().getDepth().ceilLonglong(), 1 );
    req.setLaLength( m_pViewSession->m_acquire.mZoomLAView.getFlow().getDepth().ceilLonglong(), 1 );

    //! -- req stack
    //! req length -- main
    if( m_pViewSession->m_acquire.mMainCHView.getFlow().getDepth() == 0 )
    {
        //qWarning()<<"err:/*****************trace length init == 0********************/";
        //! for bug 2504:长度为0时post一条消息更新math
        return ERR_NONE;
    }

    //! dig. level
    req.setLevel( pSes->m_decode.getThresholds() );
    req.setLevelL( pSes->m_decode.getThresholdLs() );
    for(int i=0; i<4; i++)
    {
        req.mChOnOff[i] = pSes->m_ch[i].getOnOff();
    }

    req.setAcquireTimeMode( m_pViewSession->m_acquire.m_eTimeMode );

    //! request trim
    requestTrim( req, pSes );

    //! request atttach
    requestAttachAddr( req, pSes );

    int ret;
    req.rollChOffset = 0;
    req.rollLaOffset = 0;
    ret = m_pPhy->m_trace.requestData( req, tmous );

    if( sysHasArg("-log_id") )
    {
        qDebug()<<ret<<__LINE__<<req.mRequestAttrId<<dsoEngine::getSysConfigId()<<req.mRequestOnce;
    }

    if( req.mRequestAttrId % 2 == 0 )
    {
        pSes = getHistSession();
    }
    else
    {
        pSes = getViewSesion();
    }

    if( req.mRequestAttrId != dsoEngine::getSysConfigId() )
    {
        if(req.mRequestAttrId % 2 == 0 && (dsoEngine::getSysConfigId() % 2 == 1))
        {
            req.mRequestAttrId = dsoEngine::getSysConfigId();
        }
        else if(req.mRequestAttrId % 2 == 1 && (dsoEngine::getSysConfigId() % 2 == 0)
                && ((req.mRequestAttrId + 1) % 4096 == dsoEngine::getSysConfigId() ) )
        {
            req.mRequestAttrId = dsoEngine::getSysConfigId();
        }
        else
        {
            if( sysHasArg("-log_id"))
            {
                qDebug()<<__LINE__<<"different trace id"<<dsoEngine::m_bTraceRequestOnce<<req.mRequestOnce;
            }
            ret = ERR_NO_TRACE;
        }
    }

    requestSetAttr( req, pSes );

    if ( ret != 0 && req.mRequestOnce)
    {
        //! 防止由于回放造成单次请求trace失败,需要重新请求一次
        dsoEngine::m_bTraceRequestOnce = true;
        return ERR_NO_TRACE;
    }
    else
    {
    }
    return ret;
}

DsoErr dsoEngine::requestTrace( traceRequest &/*req*/,
                                int /*tmo*/ )
{
    return ERR_NO_TRACE;
}

//! int16--int16
//! hw   -- lw
//! max  -- min
qint32 dsoEngine::getPeak( int chId )
{
    if ( chId < 0 || chId > 3 )
    { return 0; }

    Q_ASSERT( NULL != m_pPhy );

    qlonglong screenTime;
    screenTime = m_pViewSession->m_horizontal.getMainView().getScale() * hori_div;
    qint32 rate;
    rate = screenTime / time_ms(3) + 20;
    if ( rate > 24 )
    { rate = 24; }

    m_pPhy->mSpu.startAdcPeak( rate );
    m_pPhy->mSpu.flushWCache();

    int i = 0;
    while( !m_pPhy->mSpu.getAdcPeakDone() )
    {
        QThread::msleep(1);
        i++;
        if(i>100) //!3.2ns * (2^24) = 54ms;
        {
            qWarning()<<__FILE__<<__LINE__<<"!!!wait adc peak done failure.";
            break;
        }
    }

    m_pPhy->mSpu.stopAdcPeak();
    m_pPhy->mSpu.flushWCache();

    qint32 peaks[8];
    m_pPhy->mSpu.getAdcPeaks( peaks );

    //! max-min
    //! hw - lw
    qint32 peak;
    peak = (peaks[ chId * 2] << 16) | peaks[ chId * 2 + 1 ];

    return peak;
}

bool dsoEngine::getTraceRequestOnOff()
{
    return m_bTraceRequest;
}

bool dsoEngine::getTraceOnceOnOff()
{
    return m_bTraceRequestOnce;
}

int dsoEngine::getSysConfigId()
{
    return m_ConfigId.getConfigId();
}

