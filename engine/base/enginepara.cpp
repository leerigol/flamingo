
#include "enginemsg.h"

namespace DsoEngine {

//EnginePara::EnginePara( EngineParaType t )
//{
//    mType = t;
//    mSize = 1;
//}

EnginePara::EnginePara( bool val )
{
    _ctor();

    mType = E_PARA_BOOL;
    bVal = val;
}
EnginePara::EnginePara( int val )
{
    _ctor();

    mType = E_PARA_S32;
    s32Val = val;
}

EnginePara::EnginePara( qint64 val )
{
    _ctor();

    mType = E_PARA_S64;
    s64Val = val;
}
EnginePara::EnginePara( void *p )
{
    _ctor();

    mType = E_PARA_PTR;
    ptr = p;
}

EnginePara::EnginePara( quint8 val )
{
    _ctor();

    mType = E_PARA_U8;
    u8Val = val;
}
EnginePara::EnginePara( quint16 val )
{
    _ctor();

    mType = E_PARA_U16;
    u16Val = val;
}
EnginePara::EnginePara( quint32 val )
{
    _ctor();

    mType = E_PARA_U32;
    u32Val = val;
}
EnginePara::EnginePara( quint64 val )
{
    _ctor();

    mType = E_PARA_U64;
    u64Val = val;
}

EnginePara::EnginePara( DsoReal val )
{
    _ctor();

    mType = E_PARA_REAL;

    realVal = val;
}

void EnginePara::_ctor()
{
    pNext = 0;
    mSize = 1;
}

void EnginePara::setType( EngineParaType t )
{
    mType = t;
}
void EnginePara::pushPara( EnginePara *para )
{
    EnginePara *pNxt;

    //! find the last
    pNxt = this;
    while ( pNxt->pNext != 0 )
    {
        pNxt = pNxt->pNext;
    }

    //! link to the node
    pNxt->pNext = para;
    mSize++;
}

EnginePara *EnginePara::operator[]( int index )
{
    Q_ASSERT( index >= 0 && index < mSize );

    EnginePara *pHead;
    int i;

    i = 0;

    pHead = this;
    while ( i < (index) )
    {
        pHead = pHead->pNext;
        Q_ASSERT( NULL != pHead );
        i++;
    }

    return pHead;
}

int EnginePara::size()
{
    return mSize;
}

}
