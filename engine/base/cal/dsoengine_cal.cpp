#include "../dsoengine.h"

DsoErr dsoEngine::setCHCal( Chan ch, void *pCal )
{
    Q_ASSERT( NULL != pCal );
    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    m_pPhy->m_ch[ ch - chan1 ].setCal( pCal );
    return ERR_NONE;
}

DsoErr dsoEngine::setCHHizTrim( Chan ch, void *pTrim )
{
    Q_ASSERT( NULL != pTrim );
    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    m_pPhy->m_ch[ ch - chan1].setHizTrim( pTrim );

    return ERR_NONE;
}

DsoErr dsoEngine::setAdcCoreGo( int id, void *pGo )
{
    Q_ASSERT( NULL != pGo );
    Q_ASSERT( id >=0 && id<=1 );

    m_pPhy->mAdc[ id ].setCoreGo( pGo );

    return ERR_NONE;
}
DsoErr dsoEngine::setAdcCorePhase( int id, void *pPhase )
{
    Q_ASSERT( NULL != pPhase );
    Q_ASSERT( id >=0 && id<=1 );

    m_pPhy->mAdc[ id ].setCorePhase( pPhase );

    return ERR_NONE;
}

DsoErr dsoEngine::setExtCal( int /*id*/, void *pCal )
{
    Q_ASSERT( NULL != pCal );

    m_pPhy->mExt.setCal( (pathCal*)pCal );

    return ERR_NONE;
}

DsoErr dsoEngine::setLaCal( int id, void *pCal )
{
    Q_ASSERT( NULL != pCal );

    if ( id >=0 && id < 2 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    m_pPhy->mLa[ id ].setCal( (pathCal*)pCal );

    return ERR_NONE;
}
DsoErr dsoEngine::setProbeCal( int id, void *pCal )
{
    Q_ASSERT( NULL != pCal );

    if ( id >=0 && id < 5 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    m_pPhy->mProbe[ id ].setCal( (pathCal*)pCal );

    return ERR_NONE;
}

DsoErr dsoEngine::setDgOffsetCal( int id, void *pCal )
{
    Q_ASSERT( NULL != pCal );

    if ( id >=0 && id < 2 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    m_pPhy->mDgOffset[ id ].setCal( (pathCal*)pCal );

    return ERR_NONE;
}
DsoErr dsoEngine::setDgRefCal( int id, void *pCal )
{
    Q_ASSERT( NULL != pCal );

    if ( id >=0 && id < 2 )
    {}
    else
    { return ERR_INVALID_INPUT; }

    m_pPhy->mDgRef[ id ].setCal( (pathCal*)pCal );

    return ERR_NONE;
}

//! tune clock
DsoErr dsoEngine::setTuneClock( int clock )
{
    m_pPhy->mPll.tuneClock( clock );

    return ERR_NONE;
}

DsoErr dsoEngine::set1_1gOut( bool b)
{
    m_pPhy->mPll.output1_1G( b );

    return ERR_NONE;
}

