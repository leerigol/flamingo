#include "../dsoengine.h"

#define selfLa   m_pViewSession->mLa

#define MAX_LEVEL   20*1000000  //! uv unit
#define MIN_LEVEL   -20*1000000

#define LA_LSB      (0.77*1000)   //!

//! \todo get from cal
#define LA_H_GND    32768
#define LA_L_GND    32768

#define LA_H_DAC_CH 0
#define LA_L_DAC_CH 1

bool dsoEngine::getLaProbe()
{
    m_pPhy->mSpu.inCTRL();

    return m_pPhy->mSpu.CTRL.la_probe;
}
DsoErr dsoEngine::setLaHLevel( int lev )
{
    DsoErr err = ERR_NONE;

    if ( lev > MAX_LEVEL )
    {
        lev = MAX_LEVEL;
        err = ERR_OVER_UPPER_RANGE;
    }
    else if ( lev < MIN_LEVEL )
    {
        lev = MIN_LEVEL;
        err = ERR_OVER_LOW_RANGE;
    }
    else
    {}

    m_pPhy->mLa[1].setLevel( lev );

    return err;
}
DsoErr dsoEngine::setLaLLevel( int lev )
{
    DsoErr err = ERR_NONE;

    if ( lev > MAX_LEVEL )
    {
        lev = MAX_LEVEL;
        err = ERR_OVER_UPPER_RANGE;
    }
    else if ( lev < MIN_LEVEL )
    {
        lev = MIN_LEVEL;
        err = ERR_OVER_LOW_RANGE;
    }
    else
    {}

    m_pPhy->mLa[0].setLevel( lev );

    return err;
}

DsoErr dsoEngine::setLaEn( bool b)
{
    selfLa.setShowOnOff( b );
    return ERR_NONE;
}
DsoErr dsoEngine::setLaOnOff( int onMask )
{
    selfLa.mEns = onMask;
    return ERR_NONE;
}
DsoErr dsoEngine::setLaActive( int actMask )
{
    selfLa.mActives = actMask;
    return ERR_NONE;
}
DsoErr dsoEngine::setLaGroup( int gp, int gpMask )
{
    Q_ASSERT( gp >= 0 && gp < array_count(selfLa.mGroupDx) );

    selfLa.mGroupDx[ gp ] = gpMask;

    return ERR_NONE;
}
DsoErr dsoEngine::setLaSize(  int size )
{
    selfLa.mSize = (LaScale)size;
    return ERR_NONE;
}
DsoErr dsoEngine::setLaPos( int pos[16] )
{
    for ( int i = 0; i < 16; i++ )
    {
        selfLa.mPoses[i] = pos[i];
    }

    return ERR_NONE;
}

DsoErr dsoEngine::setLaColor( int hel, quint32 color )
{
    selfLa.setColor( hel, color );

    return ERR_NONE;
}

void dsoEngine::laApply()
{
    //! disabled
    if ( !selfLa.getOnOff() )
    {
        m_pPhy->mCcu.setMODE_wave_la_en( 0 );
        m_pPhy->mWpu.setLA_en( 0 );
        return;
    }

    //! la show off
    if ( !selfLa.getShowOnOff() )
    {
        m_pPhy->mWpu.setLA_en( 0 );
        return;
    }

    //! quint32 color
    int gpColors[4];

    r_meta::CBMeta bMeta;
    bMeta.getMetaVal( "la/group_color/g0", gpColors[0] );
    bMeta.getMetaVal( "la/group_color/g1", gpColors[1] );
    bMeta.getMetaVal( "la/group_color/g2", gpColors[2] );
    bMeta.getMetaVal( "la/group_color/g3", gpColors[3] );

    //! config pos
    groupLa gpLa;

    gpLa.setDx( selfLa.mEns, selfLa.mActives, selfLa.mPoses );
    gpLa.setColor( selfLa.mColors[0],
                   selfLa.mColors[1],
                   selfLa.mColors[2] );

    //! foreach group color
    for ( int i = 0; i < array_count(selfLa.mGroupDx); i++ )
    {
        //! foreach dx
        for ( int j = 0; j < array_count(gpLa.mDxs); j++ )
        {
            if ( get_bit( selfLa.mGroupDx[i], j ) )
            {
                gpLa.setDxColor( j, gpColors[i] );
            }
        }
    }

    //! size
    //! zoom screen
    groupLa zoomGp;
    if ( m_pViewSession->m_horizontal.getViewMode() == Horizontal_Zoom )
    {
        gpLa.split( 320+16, 144, selfLa.mSize, false );
        zoomGp = gpLa;
        zoomGp.split( 0, 320, selfLa.mSize, false );
    }
    //! main screen
    else
    {
        gpLa.split( 0, 480, selfLa.mSize, false );
    }

    QList< dso_phy::laLine*> cfgLines;
    groupLa::initLaLines( cfgLines );

    //! main + zoom
    gpLa.exportLines( cfgLines, false );
    zoomGp.exportLines( cfgLines, true );

    groupLa::fillNullLines( wave_height, cfgLines );

    groupLa::sortLaLine( cfgLines );
    groupLa::logLaLine( cfgLines, "/rigol/log/groupla.csv" );

    //! apply the xxu
    m_pPhy->mCcu.setMODE_wave_la_en( 1 );
    m_pPhy->mWpu.setLA_en( selfLa.mEns );
    m_pPhy->mWpu.setLaLine( cfgLines );

    groupLa::deInitLaLines( cfgLines );
}

