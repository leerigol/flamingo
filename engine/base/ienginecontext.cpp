
#include "engine.h"
#include "cplatform.h"

void stopConfigContext::enter()
{
    cplatform *plat = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != plat );

    plat->m_pPhy->mCcu.setCcuRun( false );
    plat->m_pPhy->flushWCache();
}

void stopConfigContext::exit()
{
    cplatform *plat = cplatform::sysGetDsoPlatform();
    Q_ASSERT( NULL != plat );

    plat->m_pPhy->mCcu.setCcuRun( true );
    plat->m_pPhy->flushWCache();
}




