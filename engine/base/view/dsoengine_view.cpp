
#include "../dsoengine.h"

//! -- main view
DsoErr dsoEngine::queryMainViewAttr( EngineViewAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );

    qlonglong scale, offset;

    scale = m_pViewSession->m_horizontal.getMainView().getScale();
    offset = m_pViewSession->m_horizontal.getMainView().getOffset();

    pAttr->mt0 = offset - hori_div*scale/2;
    pAttr->mtInc = scale/adc_hdiv_dots;
    pAttr->mWidth = hori_div * adc_hdiv_dots;

    return ERR_NONE;

}

DsoErr dsoEngine::queryZoomViewAttr(EngineViewAttr *pAttr)
{
    Q_ASSERT( NULL != pAttr );

    qlonglong scale, offset;

    scale = m_pViewSession->m_horizontal.getZoomView().getScale();
    offset = m_pViewSession->m_horizontal.getZoomView().getOffset();

    pAttr->mt0 = offset - hori_div*scale/2;
    pAttr->mtInc = scale/adc_hdiv_dots;
    pAttr->mWidth = hori_div * adc_hdiv_dots;

    return ERR_NONE;
}

DsoErr dsoEngine::queryAcqChMainSaFlow(CSaFlow *pAttr)
{
    Q_ASSERT( NULL != pAttr );
    (*pAttr) = m_pViewSession->m_acquire.mCHFlow;
    return ERR_NONE;
}

DsoErr dsoEngine::queryAcqChZoomSaFlow(CSaFlow *pAttr)
{
    Q_ASSERT( NULL != pAttr );
    (*pAttr) = m_pViewSession->m_acquire.mCHFlow;
    return ERR_NONE;
}

DsoErr dsoEngine::queryAcqLaMainSaFlow(CSaFlow *pAttr)
{
    Q_ASSERT( NULL != pAttr );
    (*pAttr) = m_pViewSession->m_acquire.mLaFlow;
    return ERR_NONE;
}

DsoErr dsoEngine::queryAcqLaZoomSaFlow(CSaFlow *pAttr)
{
    Q_ASSERT( NULL != pAttr );
    (*pAttr) = m_pViewSession->m_acquire.mLaFlow;
    return ERR_NONE;
}
