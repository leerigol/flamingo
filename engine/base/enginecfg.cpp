#include "enginecfg.h"

void EngineTrace::clear()
{
    mWfms.clear();
    mChs.clear();
}
void EngineTrace::requestWfm( Chan ch, DsoWfm *pWfm )
{
    mWfms.append( pWfm );
    mChs.append( ch );
}

EngineFilter::EngineFilter()
{
    mBw = BW_20M;
}

void EngineFilter::attachWindow( Bandwidth bw,
                                 qint16 *pBase,
                                 int size )
{
    Q_ASSERT( NULL != pBase );

    mBw = bw;

    if ( size > 0 )
    {
        mWindow.clear();
        for ( int i = 0; i < size; i++ )
        {
            mWindow.append( pBase[i] );
        }
    }
    else
    {
        mWindow.clear();
    }

}

void EngineFilter::setBw( Bandwidth bw )
{ mBw = bw; }
Bandwidth EngineFilter::getBw()
{ return mBw; }
const QList<qint16>& EngineFilter::getWindow()
{
    return mWindow;
}

void EngineChLayer::setChLayer( Chan ch, int layer )
{
    Q_ASSERT( ch >= chan1 && ch <= chan4 );

    mChsLayer[ ch - chan1 ] = layer;
}

void EngineHoriInfo::setInfo(
              qlonglong saScale,
              qlonglong saOffset,
              qlonglong scale,
              int depth,
              int chCnt, int laCnt)
{
    mSaScale = saScale;
    mSaOffset = saOffset;
    mScale = scale;
    mDepth = depth;

    mChCnt = chCnt;
    mLaCnt = laCnt;
}

void EngineHoriReq::setOffset(
             qlonglong offset )
{
    mOffset = offset;
}

EngineCounter::EngineCounter()
{
    mSrc = chan1;
    mGateTime = 10000;

    mGateSrc = chan1;
    mMode = counter_freq;
    mGate = counter_gate_high;
    mEvent = counter_event_edge;
    mClear = false;
}

MaskRule::MaskRule()
{
    mOffset = 0;
    mLen = array_count(mRule);
    for ( int i = 0; i < array_count(mRule); i++ )
    {
        mRule[i] = DISABLE_MASK_POINT;
    }
}

MaskRule::MaskRule( MaskRule *pRule )
{
    Q_ASSERT( NULL != pRule );

    *this = *pRule;
}

MaskRule &MaskRule::operator=( const MaskRule &rule )
{
    mOffset = rule.mOffset;
    mLen = rule.mLen;

    memcpy( mRule, rule.mRule, sizeof(mRule[0])*array_count(mRule) );

    return *this;
}

EngineMaskRule::EngineMaskRule()
{
    mChan = chan1;
    myGnd = 128;
    myDiv.set( 25, 1 );
    for ( int i = 0; i < array_count(m_pRules); i++ )
    {
        m_pRules[i] = NULL;
        mRuleValidates[i] = false;
    }
}

EngineMaskRule::~EngineMaskRule()
{
    for ( int i = 0; i < array_count(m_pRules); i++ )
    {
        if ( m_pRules[i] != NULL )
        {
            delete m_pRules[i];
        }
    }
}

EngineMaskRule &EngineMaskRule::operator=( EngineMaskRule &mask )
{
    mChan = mask.mChan;
    myGnd = mask.myGnd;
    myDiv = mask.myDiv;

    //! validates
    memcpy( mRuleValidates,
            mask.mRuleValidates,
            array_count(mRuleValidates)*sizeof(mRuleValidates[0]) );

    //! rule assign
    for( int i = 0; i < array_count(m_pRules); i++ )
    {
        if ( NULL != m_pRules[i] )
        {}
        else
        {
            m_pRules[i] = new MaskRule(  );
        }

        //! src validate
        if ( mask.m_pRules[i] != NULL )
        {}
        else
        { continue; }

        //! assign
        if ( NULL != m_pRules[i] )
        {
            *m_pRules[i] = *mask.m_pRules[i];
        }
        else
        { m_pRules[i] = new MaskRule( mask.m_pRules[i] ); }
    }

    return *this;
}

void EngineMaskRule::attachRule( MaskRule *pRule, int id )
{
    Q_ASSERT( NULL != pRule && id >=0 && id < array_count(m_pRules) );

    //! copy
    if ( m_pRules[id] != NULL )
    {
        *m_pRules[id] = *pRule;
        mRuleValidates[id] = true;
    }
    else
    {
        m_pRules[id] = new MaskRule( pRule );
        Q_ASSERT( NULL != m_pRules );
        mRuleValidates[id] = true;
    }
}

EngineMaskRule* EngineMaskRule::create()
{
    for( int i = 0; i < array_count(m_pRules); i++ )
    {
        m_pRules[i] = new MaskRule();
        if ( NULL == m_pRules[i] )
        { return NULL; }
    }

    return this;
}

void EngineMaskRule::setInfo( Chan ch,
                           int ygnd,
                           dsoFract yDiv )
{
    mChan = ch;
    myGnd = ygnd;
    myDiv = yDiv;
}

void EngineMeasRet::setMeasMeta( const dso_phy::measMeta &meta )
{
    mMeta = meta;
}

EngineAdcCoreCH::EngineAdcCoreCH()
{
    mCHa = chan1;
    mCHb = chan2;
    mCHc = chan3;
    mCHd = chan4;
}
void EngineAdcCoreCH::assignCoreCH( Chan a,
                                    Chan b,
                                    Chan c,
                                    Chan d )
{
    mCHa = a;
    mCHb = b;
    mCHc = c;
    mCHd = d;
}

