#include "dsoengine.h"
#include "enginemsg.h"

#define on_cmd( msg, id, para, proc, pAttr )   ON_COMMAND( dsoEngine, msg, id, para, proc, pAttr )
#define on_range_id( msg, id1, id2, para, proc, pAttr )    ON_RANGE_ID(dsoEngine, msg, id1, id2, para, proc, pAttr )

#define on_noid_cmd( msg, para, proc, pAttr ) ON_COMMAND( dsoEngine, msg, E_SERVICE_ID_NONE, para, proc, pAttr )
#define on_noid_cmd_context( msg, para, proc, context, pAttr ) \
                                       ON_COMMAND_CONTEXT( dsoEngine, msg, E_SERVICE_ID_NONE, para, proc, pAttr, context )

#define on_noid_stack( msg, proc, pAttr, stackSize, type )   \
                                        ON_COMMAND_STACK( dsoEngine, \
                                        msg, \
                                        E_SERVICE_ID_NONE, \
                                        type, \
                                        proc, \
                                        pAttr,\
                                        stackSize )

#include "../engineentry.h"

IMPLEMENT_MSG_MAP( engine, dsoEngine )

//! tmo
on_noid_cmd_s32( ENGINE_TMO, onTimeout, NULL ),
on_noid_get_ptr( qENGINE_SESSION, querySession ),

on_noid_cmd_void( ENGINE_FREEZE, freezeEngine, NULL ),
on_noid_cmd_void( ENGINE_UNFREEZE, unFreezeEngine, NULL ),

on_noid_cfg_bool( ENGINE_ENTER_CAL, enterCal, NULL),
//! ch oper.
on_range_ch( ENGINE_CH_ON, E_PARA_RANGE_BOOL, chOnOff, NULL ),
on_range_ch( ENGINE_CH_ONOFF_NOPLAY, E_PARA_RANGE_BOOL, chOnOff, NULL ),
on_range_ch( ENGINE_CH_SCALE, E_PARA_RANGE_S32, chScale, NULL ),
on_range_ch( ENGINE_CH_OFFSET, E_PARA_RANGE_S32, chOffset, NULL ),
on_range_ch( ENGINE_CH_NULL, E_PARA_RANGE_S32, chNull, NULL ),
on_range_ch( ENGINE_CH_COUPLING, E_PARA_RANGE_S32, chCoupling, NULL ),

on_range_ch( ENGINE_CH_BANDLIMIT, E_PARA_RANGE_S32, chBandwidth, NULL ),
on_range_ch( ENGINE_CH_IMPEDANCE, E_PARA_RANGE_S32, chImpedance, NULL ),
on_range_ch( ENGINE_CH_DELAY, E_PARA_RANGE_S32, chDelay, NULL ),
on_range_ch( ENGINE_CH_INVERT, E_PARA_RANGE_BOOL, chInvert, NULL ),
on_range_ch( ENGINE_CH_PROBE_REAL, E_PARA_RANGE_REAL, chProbeRatio, NULL ),

on_noid_get_s32int( qENGINE_CH_AFE_SCALE, getChAfeScale ),

//! la op
on_noid_get_boolvoid( qENGINE_LA_PROBE, getLaProbe ),
on_noid_cmd_s32( ENGINE_LA_H_THRESHOLD, setLaHLevel, NULL ),
on_noid_cmd_s32( ENGINE_LA_L_THRESHOLD, setLaLLevel, NULL ),

on_noid_cmd_bool( ENGINE_LA_EN, setLaEn, NULL ),
on_noid_cmd_s32( ENGINE_LA_ON, setLaOnOff, NULL ),
on_noid_cmd_s32( ENGINE_LA_ACTIVE, setLaActive, NULL ),
on_noid_cmd_s32s32( ENGINE_LA_GROUP, setLaGroup, NULL ),
on_noid_cmd_s32( ENGINE_LA_SIZE, setLaSize, NULL ),
on_noid_cmd_ptr( ENGINE_LA_POS, setLaPos, NULL ),

on_noid_cmd_s32s32( ENGINE_LA_COLOR, setLaColor, NULL ),

//! hori
on_noid_cmd_s32( ENGINE_HORI_VIEW_MODE, setViewMode, NULL ),

on_noid_cmd_s64( ENGINE_MAIN_SCALE, setMainScale, NULL ),
on_noid_cmd_s64( ENGINE_MAIN_OFFSET, setMainOffset, NULL ),

on_noid_cmd_s64( ENGINE_ZOOM_SCALE, setZoomScale, NULL ),
on_noid_cmd_s64( ENGINE_ZOOM_OFFSET, setZoomOffset, NULL ),

on_noid_get_ptr( qENGINE_HORI_INFO, queryHoriInfo ),
on_noid_get_ptr( qENGINE_HORI_ATTR, queryHoriAttr ),

//! acquire
on_noid_cmd_s32( ENGINE_ACQUIRE_MODE, setAcquireMode, NULL ),
on_noid_cmd_s32( ENGINE_ACQUIRE_AVG_CNT, setAverageCount, NULL ),
on_noid_cmd_s32( ENGINE_ACQUIRE_INTERPLATE_MODE, setInterplateMode, NULL ),
on_noid_cmd_s32( ENGINE_ACQUIRE_TIMEMODE, setTimeMode, NULL ),

on_noid_cmd_s32( ENGINE_ACQUIRE_SAMPLE_MODE, setSampleMode, NULL ),
on_noid_cmd_bool( ENGINE_ACQUIRE_ANTI_ALIASING, setAntiAliasing, NULL ),

on_noid_cmd_s32( ENGINE_ACQUIRE_DEPTH, setMemDepth, NULL ),
on_noid_cmd_s32( ENGINE_ACQUIRE_LA_DEPTH, setLaMemDepth, NULL ),

on_noid_get_s64void( qENGINE_ACQUIRE_CH_LENGTH, getChLength ),
on_noid_get_s64void( qENGINE_ACQUIRE_LA_LENGTH, getLaLength ),

on_noid_get_s64void( qENGINE_ACQUIRE_SARATE, getSampleRate ),
on_noid_get_s64void( qENGINE_ACQUIRE_LA_SARATE, getLaSampleRate ),

on_noid_cmd_void( ENGINE_ACQUIRE_TEST, snapAcquireState, NULL ),

on_noid_cmd_s32( ENGINE_ACQUIRE_BAND, setSysRawBand, NULL ),

//! control
on_noid_cmd_s32( ENGINE_CONTROL_ACTION, setControlAction, NULL ),
on_noid_get_s32void( qENGINE_CONTROL_STATUS,  getControlStatus ),
on_noid_cmd_void(ENGINE_WAIT_CONTROL_STOP, waitControlStop, NULL ),

//! counter
on_noid_cmd_void( ENGINE_COUNTER_RST, setCounterRst, NULL ),
on_noid_cmd_bool( ENGINE_COUNTER_EN, setCounterEn, NULL ),
on_noid_cmd_s32( ENGINE_COUNTER_SOURCE, setCounterSource, NULL ),

on_noid_cmd_ptr( ENGINE_COUNTER_CFG, setCounterCfg, NULL ),
on_noid_get_ptr( qENGINE_COUNTER_VALUE, queryCounterValue ),

//! dmv
on_noid_cmd_s32( ENGINE_DVM_SOURCE, setDvmSource, NULL ),
on_noid_cmd_s32( ENGINE_DVM_GATE_TIME, setDvmGateTime, NULL ),
on_noid_get_ptr( qENGINE_DVM_VOLT, queryDvmValue ),

//! meas
on_noid_cmd_stack( ENGINE_MEAS_RANGE, setMeasRange, NULL, 2 ),
on_noid_cmd_s32s32( ENGINE_MEAS_VIEW, setMeasView, NULL ),
on_noid_cmd_u32( ENGINE_MEAS_CFG_TOTAL_NUM, setMeasCfgTotalNum, NULL ),

on_noid_cmd_u32( ENGINE_MEAS_EN, setMeasEn, NULL ),
on_noid_cmd_void( ENGINE_MEAS_ONCE_REQ, setMeasOnceReq, NULL),

on_noid_cmd_s32s32( ENGINE_MEAS_EDGE_TYPE, setMeasEdgeType, NULL ),
on_noid_cmd_s32s32( ENGINE_MEAS_PULSE_TYPE, setMeasPulseType, NULL ),

on_noid_cmd_ptr( ENGINE_MEAS_CFG, setMeasCfg, NULL ),
on_noid_cmd_bool( ENGINE_MEAS_RD_DURING_FLAG, setMeasRdDuringFlag, NULL ),
on_noid_get_ptr( qENGINE_MEAS_VALUE, queryMeasValue ),
on_noid_get_ptr( qENGINE_ADC_CORE_CH, queryAdcCoreCH ),
on_noid_get_u32void( qENGINE_MEAS_RESULT_DONE, queryMeasResultDone ),

//! cal data
on_noid_cmd_s32ptr( ENGINE_CH_CAL, setCHCal, NULL ),
on_noid_cmd_s32ptr( ENGINE_CH_HIZ_TRIM, setCHHizTrim, NULL ),
on_noid_cmd_s32ptr( ENGINE_ADC_CORE_GO, setAdcCoreGo, NULL ),
on_noid_cmd_s32ptr( ENGINE_ADC_CORE_PHASE, setAdcCorePhase, NULL ),

on_noid_cmd_s32ptr( ENGINE_EXT_CAL, setExtCal, NULL ),
on_noid_cmd_s32ptr( ENGINE_LA_CAL, setLaCal, NULL ),
on_noid_cmd_s32ptr( ENGINE_PROBE_CAL, setProbeCal, NULL ),

on_noid_cmd_s32ptr( ENGINE_DG_OFFSET_CAL, setDgOffsetCal, NULL ),
on_noid_cmd_s32ptr( ENGINE_DG_REF_CAL, setDgRefCal, NULL ),

//! config misc
on_noid_cfg_void( ENGINE_BEEP_TICK, tickBeep, NULL ),
on_noid_cfg_void( ENGINE_BEEP_SHORT, shortBeep, NULL ),
on_noid_cfg_void( ENGINE_BEEP_START, startBeep, NULL ),
on_noid_cfg_void( ENGINE_BEEP_STOP, stopBeep, NULL ),
on_noid_cfg_void( ENGINE_BEEP_INTERVAL, intervalBeep, NULL ),

on_noid_cfg_s32( ENGINE_LCD_LIGHT, setLcdLight, NULL ),
on_noid_cfg_s32( ENGINE_FAN_SPEED, setFanSpeed, NULL ),

on_noid_get_u16void( qENGINE_CPLD_VER, getCpldVer ),
on_noid_get_u16void( qENGINE_MB_VER, getMBVer ),

on_noid_get_u32void( qENGINE_SPU_VER, getSpuVer ),
on_noid_get_u32void( qENGINE_WPU_VER, getWpuVer ),
on_noid_get_u32void( qENGINE_SCU_VER, getScuVer ),
on_noid_get_u32void( qENGINE_FCU_VER, getFcuVer ),

on_noid_cfg_s32( ENGINE_LED_ON, setLedOn, NULL ),
on_noid_cfg_s32( ENGINE_LED_OFF, setLedOff, NULL ),

on_noid_cfg_s32bool( ENGINE_LED_OP, setLedOp, NULL ),
on_noid_cfg_s32( ENGINE_LED_STATUS, setLedStatus, NULL ),
on_noid_get_s32void( qENGINE_LED_STATUS, getLedStatus ),

on_noid_cfg_bool( ENGINE_POWER_SWITCH, setPowerSwitch, NULL ),
on_noid_get_boolvoid( qENGINE_POWER_SWITCH, getPowerSwitch ),

on_noid_cfg_void( ENGINE_POWER_SHUTDOWN, setPowerShutdown, NULL ),
on_noid_cfg_void( ENGINE_POWER_RESTART, setPowerRestart, NULL ),

on_noid_cfg_void( ENGINE_BOARD_RST_CTP, rstBoardCtp, NULL ),
on_noid_cfg_void( ENGINE_BOARD_CLR_INT, clrBoardInt, NULL ),

on_noid_get_u16void( qENGINE_BOARD_INT_STAT, getBoardIntStat ),
on_noid_get_u16void( qENGINE_IBOARD_INT_EN, getBoardIntEn ),
on_noid_get_u16void( qENGINE_BOARD_INT_MASK, getBoardIntMask ),

on_noid_cfg_u16( ENGINE_BOARD_TEST_WRITE, testBoardWrite, NULL ),
on_noid_get_u16void( qENGINE_BOARD_TEST_READ, testBoardRead ),

on_noid_cfg_bool( ENGINE_PROBE_1K_EN, setProbeEn, NULL ),
on_noid_cfg_s32( ENGINE_TRIG_OUT_MUX, setTrigOutSel, NULL ),

on_noid_cfg_bool( ENGINE_1_1G_OUT, set1_1gOut, NULL ),

on_noid_cmd_stack( ENGINE_REG, writeReg, NULL, 3 ),
on_noid_get_u32stack( qENGINE_REG, readReg, NULL, 2 ),

//! adc uuid
on_noid_get_u32int( qENGINE_ADC_ID, getAdcRomId ),
on_noid_get_ptr( qENGINE_ADC_UUID, getAdcUuid ),

on_noid_get_u32int( qENGINE_AFE_VER, getAfeVer ),

//! trigger
on_noid_cmd_s32( ENGINE_TRIGGER_HOLDER, setTrigHolder, NULL ),
on_noid_cmd_u32( ENGINE_TRIGGER_HOLD_COUNT, setTrigHoldNum, NULL ),
on_noid_cmd_u64( ENGINE_TRIGGER_HOLD_TIME, setTrigHoldTime, NULL ),

on_noid_cmd_s32s32( ENGINE_TRIGGER_COUPLING, setTrigCoupling, NULL ),
on_noid_cmd_s32( ENGINE_TRIGGER_SWEEP, setTrigSweep, NULL ),
on_noid_cmd_stack( ENGINE_TRIGGER_LEVEL_A, setTrigLevelA, NULL, 3 ),
on_noid_cmd_stack( ENGINE_TRIGGER_LEVEL_B, setTrigLevelB, NULL, 3 ),

on_noid_cmd_stack( ENGINE_TRIGGER_LEVEL_A_ADC, setTrigLevelAAdc, NULL, 3 ),
on_noid_cmd_stack( ENGINE_TRIGGER_LEVEL_B_ADC, setTrigLevelBAdc, NULL, 3 ),

on_noid_cmd_s32s32( ENGINE_TRIGGER_LEVEL_EXT, setExtTrigLevel, NULL ),    //! level && sens
on_noid_cmd_s32s32( ENGINE_TRIGGER_LEVEL_EXT5, setExt5TrigLevel, NULL ),

on_noid_cmd_void( ENGINE_TRIGGER_FORCE, setTrigForce, NULL ),

on_noid_cmd_s32bool( ENGINE_TRIGGER_CH_EN, setTrigCHEn, NULL ),
on_noid_cmd_s32bool( ENGINE_DVM_COUNTOR_CH_EN, setDvmCountCHEn, NULL),

on_noid_cmd_void( ENGINE_TRIGGER_CHs_EN_RST, setTrigCHsRst, NULL ),

//! .edge
on_noid_cmd_ptr( ENGINE_TRIGGER_EDGE_CFG, setTrigEdgeCfg, NULL ),

//! .pulse
on_noid_cmd_ptr( ENGINE_TRIGGER_PULSE_CFG, setTrigPulseCfg, NULL ),

//! .slope
on_noid_cmd_ptr( ENGINE_TRIGGER_SLOPE_CFG, setTrigSlopeCfg, NULL ),

//! .runt
on_noid_cmd_ptr( ENGINE_TRIGGER_RUNT_CFG, setTrigRuntCfg, NULL ),

//! .over
on_noid_cmd_ptr( ENGINE_TRIGGER_OVER_CFG, setTrigOverCfg, NULL ),

//! .window
on_noid_cmd_ptr( ENGINE_TRIGGER_WINDOW_CFG, setTrigWindowCfg, NULL ),

//! .nedge
on_noid_cmd_ptr( ENGINE_TRIGGER_NEDGE_CFG, setTrigNEdgeCfg, NULL ),
//! .slope
on_noid_cmd_ptr( ENGINE_TRIGGER_SLOPE_CFG, setTrigSlopeCfg, NULL ),
//! .tmo
on_noid_cmd_ptr( ENGINE_TRIGGER_TIMEOUT_CFG, setTrigTimeoutCfg, NULL ),
//! .pattern
on_noid_cmd_ptr( ENGINE_TRIGGER_LOGIC_CFG, setTrigPatternCfg, NULL ),
//! .sh
on_noid_cmd_ptr( ENGINE_TRIGGER_SETUPHOLD_CFG, setTrigSHCfg, NULL ),
//! delay
on_noid_cmd_ptr( ENGINE_TRIGGER_DELAY_CFG, setTrigDelayCfg, NULL ),

on_noid_cmd_ptr( ENGINE_TRIGGER_VIDEO_CFG, setTrigVideoCfg, NULL ),

on_noid_cmd_ptr( ENGINE_TRIGGER_DURATION_CFG, setTrigDurationCfg, NULL ),

on_noid_cmd_ptr( ENGINE_TRIGGER_AB_CFG, setTrigABCfg, NULL ),

on_noid_cmd_ptr( ENGINE_TRIGGER_UART_CFG, setTrigUartCfg, NULL ),
on_noid_cmd_ptr( ENGINE_TRIGGER_SPI_CFG,  setTrigSpiCfg, NULL ),
on_noid_cmd_ptr( ENGINE_TRIGGER_I2C_CFG, setTrigIicCfg, NULL  ),

on_noid_cmd_ptr( ENGINE_TRIGGER_CAN_CFG,     setTrigCanCfg, NULL     ),
on_noid_cmd_ptr( ENGINE_TRIGGER_FLEXRAY_CFG, setTrigFlexrayCfg, NULL ),
on_noid_cmd_ptr( ENGINE_TRIGGER_1553_CFG,    setTrigStd1553bCfg, NULL),
on_noid_cmd_ptr( ENGINE_TRIGGER_LIN_CFG,     setTrigLinCfg, NULL     ),
on_noid_cmd_ptr( ENGINE_TRIGGER_I2S_CFG,     setTrigI2SCfg, NULL     ),

on_noid_cmd_ptr(  ENGINE_TRIGGER_ZONE_CFG,    setTrigZoneCfg, NULL   ),
on_noid_cmd_bool( ENGINE_TRIGGER_ZONE_EN,     setTrigZoneEn,  NULL   ),

//on_noid_cmd_ptr( ENGINE_TRIGGER_429_CFG, ),
//on_noid_cmd_ptr( ENGINE_TRIGGER_SENT_CFG, ),
//on_noid_cmd_ptr( ENGINE_TRIGGER_DMX512_CFG, ),
//on_noid_cmd_ptr( ENGINE_TRIGGER_SBUS_CFG, ),
//on_noid_cmd_ptr( ENGINE_TRIGGER_1WIRE_CFG, ),

//! search
on_noid_get_ptr( qENGINE_SEARCH_REQ_DATA,      reqSearchData  ),
on_noid_get_ptr( qENGINE_SEARCH_GET_DATA,      getSearchData  ),
on_noid_cmd_ptr( ENGINE_SEARCH_CLEAR_DATA,     clearSearchData, NULL ),

on_noid_cmd_u32(   ENGINE_SEARCH_EN,    setSearchEn, NULL ),

on_noid_cmd_stack( ENGINE_SEARCH_LEVEL_A, setSearchLevelA, NULL, 3 ),
on_noid_cmd_stack( ENGINE_SEARCH_LEVEL_B, setSearchLevelB, NULL, 3 ),

on_noid_cmd_ptr( ENGINE_SEARCH_EDGE_CFG, setSearchEdgeCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_PULSE_CFG, setSearchPulseCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_SLOPE_CFG, setSearchSlopeCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_RUNT_CFG, setSearchRuntCfg, NULL ),

on_noid_cmd_ptr( ENGINE_SEARCH_OVER_CFG, setSearchOverCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_WINDOW_CFG, setSearchWindowCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_NEDGE_CFG, setSearchNEdgeCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_SLOPE_CFG, setSearchSlopeCfg, NULL ),

on_noid_cmd_ptr( ENGINE_SEARCH_TIMEOUT_CFG, setSearchTimeoutCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_LOGIC_CFG, setSearchPatternCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_SETUPHOLD_CFG, setSearchSHCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_DELAY_CFG, setSearchDelayCfg, NULL ),

on_noid_cmd_ptr( ENGINE_SEARCH_VIDEO_CFG, setSearchVideoCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_DURATION_CFG, setSearchDurationCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_AB_CFG, setSearchABCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_UART_CFG, setSearchUartCfg, NULL ),

on_noid_cmd_ptr( ENGINE_SEARCH_SPI_CFG,  setSearchSpiCfg, NULL ),
on_noid_cmd_ptr( ENGINE_SEARCH_I2C_CFG, setSearchIicCfg, NULL  ),
on_noid_cmd_ptr( ENGINE_SEARCH_CAN_CFG,     setSearchCanCfg, NULL     ),
on_noid_cmd_ptr( ENGINE_SEARCH_FLEXRAY_CFG, setSearchFlexrayCfg, NULL ),

on_noid_cmd_ptr( ENGINE_SEARCH_1553_CFG,    setSearchStd1553bCfg, NULL),
on_noid_cmd_ptr( ENGINE_SEARCH_LIN_CFG,     setSearchLinCfg, NULL     ),
on_noid_cmd_ptr( ENGINE_SEARCH_I2S_CFG,     setSearchI2SCfg, NULL     ),

//! display
on_noid_cmd_s32( ENGINE_DISP_TYPE, setLineType, NULL),
on_noid_cmd_s32bool( ENGINE_DISP_CH_EN, setChDispEn, NULL ),
on_noid_cmd_s32ptr( ENGINE_DISP_CH_ORDERS, setChDispOrder, NULL ),

on_noid_cmd_s32( ENGINE_DISP_PERSIST_TIME, setPersistTime, NULL ),

on_noid_cfg_s32( ENGINE_DISP_WFM_LIGHT, setIntensitity, NULL ),
on_noid_cmd_s32( ENGINE_DISP_PALETTE, setPalette, NULL ),
on_noid_cmd_bool( ENGINE_DISP_SCREEN_PERSIST, setScreenPersist, NULL ),

on_noid_cmd_bool( ENGINE_DISP_CLEAR, clearDisplay, NULL ),
on_noid_cmd_void( ENGINE_DISP_CLEAR_PERSIST, clearBgWave, NULL),

on_noid_cmd_ptr( ENGINE_DISP_SCREEN, setScrAttr, NULL ),

//! view
on_noid_get_ptr( qENGINE_MAIN_VIEW_ATTR, queryMainViewAttr ),
on_noid_get_ptr( qENGINE_ZOOM_VIEW_ATTR, queryZoomViewAttr),

on_noid_get_ptr( qENGINE_ACQ_CH_MAIN_SAFLOW, queryAcqChMainSaFlow),
on_noid_get_ptr( qENGINE_ACQ_CH_ZOOM_SAFLOW, queryAcqChZoomSaFlow),
on_noid_get_ptr( qENGINE_ACQ_LA_MAIN_SAFLOW, queryAcqLaMainSaFlow),
on_noid_get_ptr( qENGINE_ACQ_LA_ZOOM_SAFLOW, queryAcqLaZoomSaFlow),

//! decode
on_noid_cmd_stack( ENGINE_DECODE_THRESHOLD, setDecodeThre, NULL, 3 ),
on_noid_cmd_stack( ENGINE_DECODE_THRESHOLD_L, setDecodeThreL, NULL, 3 ),

//! record&play
on_noid_cmd_bool(ENGINE_RECORD_DONE,     setRecordDone, NULL ),
on_noid_cmd_s32( ENGINE_RECORD_ACTION,   setRecordAction, NULL ),
on_noid_cmd_s64( ENGINE_RECORD_INTERVAL, setRecordInterval, NULL ),
on_noid_cmd_s32( ENGINE_RECORD_FRAMES,   setRecordFrames, NULL ),

on_noid_cmd_s32( ENGINE_RECORD_PLAY_ACTION, setPlayAction, NULL ),
on_noid_cmd_s64( ENGINE_RECORD_PLAY_INTERVAL, setPlayInterval, NULL ),
on_noid_cmd_s32s32( ENGINE_RECORD_PLAY_RANGE, setPlayRange, NULL),

on_noid_cmd_s32( ENGINE_RECORD_PLAY_FRAME_ID, setPlayNow, NULL ),
on_noid_cmd_s32( ENGINE_RECORD_PLAY_DIRECTION, setPlayDirection, NULL ),
on_noid_cmd_bool( ENGINE_RECORD_PLAY_LOOP, setPlayLoop, NULL ),
on_noid_cmd_s32( ENGINE_RECORD_PLAY_STEP, setStepPlay, NULL ),

on_noid_get_s32void( qENGINE_RECORD_MAX_FRAMES, getRecordCapacity ),
on_noid_get_s32void( qENGINE_RECORD_PLAY_FRAME_ID, getFrameNow ),
on_noid_get_u64int( qENGINE_RECORD_PLAY_FRAME_TAG, getFrameTag ),
on_noid_get_boolvoid( qENGINE_RECORD_IDLE, getRecPlayIdle ),

//! mask
on_noid_cmd_bool( ENGINE_MASK_ON, setMaskOn, NULL ),
on_noid_cmd_s32( ENGINE_MASK_SOURCE, setMaskSource, NULL ),
on_noid_cmd_ptr( ENGINE_MASK_RULE, setMaskRule, NULL ),
on_noid_cmd_s32( ENGINE_MASK_ACTION_EVENT, setMaskOutAction , NULL),
on_noid_cmd_s32( ENGINE_MASK_ACTION_ACTION, setMaskActionAction, NULL ),

on_noid_cmd_bool( ENGINE_MASK_OUTPUT_ON, setMaskOutput, NULL ),
on_noid_cmd_bool( ENGINE_MASK_OUTPUT_HL, setMaskOutPN, NULL),
on_noid_cmd_s64( ENGINE_MASK_OUTPUT_DLY, setMaskOutDelay, NULL),
on_noid_cmd_s64( ENGINE_MASK_OUTPUT_TIME, setMaskOutTime, NULL ),

on_noid_cmd_void( ENGINE_MASK_RST, setMaskRst, NULL ),
on_noid_get_ptr( qENGINE_MASK_REPORT, queryMaskReport ),

//!1WIRE PROBE
on_noid_get_ptr( qENGINE_1WIRE_PROBE_INFO, queryProbeInfo ),
on_noid_cmd_ptr( ENGINE_1WIRE_PROBE_INFO,  setProbeInfo, NULL ),
on_noid_cmd_s32s32( ENGINE_1WIRE_PROBE_DAC, setProbeDac, NULL),

//! trace
on_noid_get_s32int( qENGINE_TRACE_PEAK, getPeak ),

//! test
//ON_COMMAND( dsoEngine, ENGINE_ADC1_CFG, E_SERVICE_ID_NONE, E_PARA_S32, setADC1 ),
//ON_COMMAND( dsoEngine, ENGINE_ADC2_CFG, E_SERVICE_ID_NONE, E_PARA_S32, setADC2 ),

//ON_COMMAND( dsoEngine, ENGINE_RELAY_CFG, E_SERVICE_ID_NONE, E_PARA_S32, setRelay ),
//ON_COMMAND( dsoEngine, ENGINE_VGA1_CFG, E_SERVICE_ID_NONE, E_PARA_S32, setVga1 ),
//ON_COMMAND( dsoEngine, ENGINE_VGA2_CFG, E_SERVICE_ID_NONE, E_PARA_S32, setVga2 ),
//ON_COMMAND( dsoEngine, ENGINE_VGA3_CFG, E_SERVICE_ID_NONE, E_PARA_S32, setVga3 ),

//ON_COMMAND( dsoEngine, ENGINE_VGA4_CFG, E_SERVICE_ID_NONE, E_PARA_S32, setVga4 ),

END_OF_ITEM()

