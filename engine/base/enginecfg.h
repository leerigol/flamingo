#ifndef ENGINECFG_H
#define ENGINECFG_H

#include "../../include/dsotype.h"
#include "../../baseclass/dsowfm.h"
#include "../../phy/trace/tracerequest.h"
using namespace DsoType;

class EngineCfg
{
};

class EngineTrace : public EngineCfg
{
public:
    void clear();
    void requestWfm( Chan ch, DsoWfm *pWfm );

public:
    QList< DsoWfm *> mWfms;
    QList< Chan > mChs;
};

class EngineFilter : public EngineCfg
{
public:
    EngineFilter();

    void attachWindow(  Bandwidth bw,
                        qint16 *pBase,
                        int size );

    void setBw( Bandwidth bw );
    Bandwidth getBw();

    const QList<qint16>&getWindow();

protected:
    QList<qint16> mWindow;
    Bandwidth mBw;
};

//! ch layer
//! 0 topmost
//! layers conains Chan
class EngineChLayer : public EngineCfg
{
public:
    void setChLayer( Chan ch, int layer );
public:
    int mChsLayer[ 6 ];   //! ch1/2/3/4
};


//! hori
class EngineHoriInfo : public EngineCfg
{
public:
                        //! in
    qlonglong mSaScale, mSaOffset;
    qlonglong mScale;
    int       mChCnt;   //! 1,2,4
    quint32   mChBm;    //! open ch bit mask

    int       mLaCnt;   //! 1,2
    int       mDepth;   //! 0 -- auto
                        //! other -- fixed

                        //! out
    qlonglong mOffMax;
    qlonglong mOffMin;
    qlonglong mPlayScaleMin;

    int       mChIntxp;
    qlonglong mChSa;
    dsoFract  mChDotTime;

    int       mLaIntxp;
    qlonglong mLaSa;
    dsoFract  mLaDotTime;

    dsoFract  mChLength;
    dsoFract  mLaLength;
                        //! viewLen = m/n
    dsoFract mChViewLen;
    dsoFract mLaViewLen;

public:
    void setInfo( qlonglong saScale,
                  qlonglong saOffset,
                  qlonglong scale,
                  int depth,
                  int chCnt, int LaCnt = 2 );

};
Q_DECLARE_METATYPE( EngineHoriInfo )

class EngineHoriReq : public EngineHoriInfo
{
public:
    void setOffset( qlonglong off );

public:
    AcquireTimemode mAcqTimeMode;
    qlonglong mOffset;
};
Q_DECLARE_METATYPE( EngineHoriReq )

class EngineHoriTag : public EngineCfg
{
public:
    //! zoom
    qlonglong mMainScale, mMainOffset;
    qlonglong mZoomScale, mZoomOffset;

    dsoFract mCHLength;
    dsoFract mLaLength;

    dsoFract mCHDotTime;
    dsoFract mLaDotTime;

};
//Q_DECLARE_METATYPE( HoriTag )

class EngineHoriAttr : public EngineCfg
{
public:
    bool mbPlay;
    EngineHoriTag mRecTag;
    EngineHoriTag mViewTag;
};

class CDsoSession;
class EngineSession : public EngineCfg
{
public:
    CDsoSession *m_pViewSession;
    CDsoSession *m_pRecSession;
};

class EngineViewAttr : public EngineCfg
{
public:
    //! --hattr
    qlonglong mt0;
    qlonglong mtInc;
    qlonglong mWidth;
};

class EngineCounter : public EngineCfg
{
public:
    EngineCounter();
public:
    Chan mSrc;
    quint32 mGateTime;      //! us unit

    Chan mGateSrc;          //! src
    CounterMode mMode;
    CounterGate mGate;
    CounterEvent mEvent;
    bool mClear;
};

class EngineCounterValue : public EngineCfg
{
public:
    quint32 mThead, mTtail;
    quint32 mEdgeCnt;
    quint64 signalWidth;
    quint64 signalEvent;

    quint32 mTimeUnit;  //! ps
};

class EngineDvmValue : public EngineCfg
{
public:
    quint32 mDvmRms, mDvmAver, mDvmRange;  //! in adc
    int mGnd, mScale;   //! in uv
    int mVDiv;
};

#define DISABLE_MASK_POINT  (-1)
class MaskRule
{
public:
    MaskRule();
    MaskRule( MaskRule *pRule );
    MaskRule &operator=( const MaskRule &rule );

public:
    int mOffset, mLen;
    qint16 mRule[1000];
};
//! |                   |
//! |___________________|       3
//!
//!         ______              2
//!         |_____|             1
//!
//! _____________________       0
//! |                   |
//! off -- -1
//! threshold disable -- NULL

class EngineMaskRule : public EngineCfg
{
public:
    EngineMaskRule();
    ~EngineMaskRule();
public:
    EngineMaskRule &operator=( EngineMaskRule &mask );

public:
    void attachRule( MaskRule *pRule, int id );

    //! create 4 rules
    EngineMaskRule* create();

    void setInfo( Chan ch,
                   int ygnd,
                   dsoFract yDiv );

public:
    Chan mChan;
    MaskRule *m_pRules[4];
    bool  mRuleValidates[4];

    //! chan attr
    int myGnd;
    dsoFract myDiv;
};

class EngineMaskReport : public EngineCfg
{
public:
    quint64 mA,mB,mC,mD,mTotal;

    quint64 mCH[4];     //! foreach channel
};

#include "../../phy/meas/iphymeascfg.h"
#include "../../phy/meas/iphymeasret.h"
class EngineMeasCfg : public EngineCfg, public dso_phy::MeasCfguMemProxy
{};

class EngineMeasRet : public EngineCfg,
                      public dso_phy::MeasRetuMemProxy

{
public:
    void setMeasMeta( const dso_phy::measMeta &meta );

public:
    dso_phy::measMeta mMeta;
};

class EngineAdcCoreCH : public EngineCfg
{
public:
    Chan mCHa;
    Chan mCHb;
    Chan mCHc;
    Chan mCHd;

public:
    EngineAdcCoreCH();
    void assignCoreCH( Chan a = chan1,
                       Chan b = chan2,
                       Chan c = chan3,
                       Chan d = chan4 );
};

#endif // ENGINECFG_H
