#include "../dsoengine.h"

#define selfCon     m_pViewSession->m_control

//! control
DsoErr dsoEngine::setControlAction( ControlAction action )
{

//    qDebug()<<__FUNCTION__<<__LINE__<<"/*********************************/";

    selfCon.setAction( action );

    if ( action == Control_Run )
    {
        //! enter run 本身就帶有clear操作
//        clearDisplay();
        //! run now
        toEngine( dsoEngine::m_pRecEngine );
    }
    else if ( action == Control_Stop )
    {
        m_pPhy->mCcu.setCcuRun( false );
        toEngine( dsoEngine::m_pPlayEngine );
    }
    else if ( action == Control_Single )
    {
        //! single
        m_pViewSession->m_trigger.setSweep( Trigger_Sweep_Single );

        toEngine( dsoEngine::m_pRecEngine );
    }
    else
    {}

    return ERR_NONE;
}

ControlStatus dsoEngine::getControlStatus()
{
    m_pPhy->mScu.inSTATE_DISPLAY();
    m_pPhy->mScu.outSTATE_DISPLAY_disp_trig_clr(1);//清除寄存器内标记。

    if ( m_pPhy->mScu.getSTATE_DISPLAY_sys_stop_state() == 1 )
    {
        if( m_pPhy->mScu.getSTATE_DISPLAY_mask_stop_state() == 1 ||
                m_pPhy->mScu.getSTATE_DISPLAY_rec_play_stop_state() == 1)
        {
            m_pPhy->mScu.outSTATE_DISPLAY_disp_trig_clr(1);//清除寄存器内标记。
            m_pPhy->mScu.outSTATE_DISPLAY_stop_stata_clr(1);
            return Control_Fpga_Force_Stopped;
        }
        return Control_Stoped;
    }

    //! -- trigger
    if ( m_pPhy->mScu.getSTATE_DISPLAY_disp_tpu_scu_trig() ==  1 )
    {
        m_pPhy->mScu.outSTATE_DISPLAY_disp_trig_clr(1);//清除寄存器内标记。
        return Control_td;
    }

    //! auto trig used
    if ( m_pPhy->mScu.getSTATE_DISPLAY_disp_sys_trig_d() == 1)
    { return Control_autoing; }

    if ( m_pPhy->mScu.getSTATE_DISPLAY_sys_pre_sa_done() == 1 )
    { return Control_waiting; }

    if ( m_pPhy->mScu.getSTATE_DISPLAY_scu_fsm() == 2 ||
         m_pPhy->mScu.getSTATE_DISPLAY_sys_pre_sa_done() == 0)
    { return Control_Runing; }

    return Control_waiting;
}

DsoErr dsoEngine::waitControlStop()
{
    QThread::msleep(PLAY_TIMEOUT); //!等引擎进入onEnter
    m_pPhy->mCcu.waitStop();
    return ERR_NONE;
}


