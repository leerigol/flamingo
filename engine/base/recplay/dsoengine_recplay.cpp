#define _DEBUG 1

#include "../dsoengine.h"

#define phy_ic_capacity (0x7000000*8)

DsoErr dsoEngine::setRecordDone(bool /*b*/)
{
//    qDebug()<<__FILE__<<__LINE__<<"---done:"<<b;
//    m_pViewSession->m_RecDone = b;
    return ERR_NONE;
}

DsoErr dsoEngine::setRecordAction( RecAction act )
{
    m_RecAction = act;

    if ( act == Rec_start )
    {
        m_pPhy->mRecPlay.startRec();
        qDebug()<<__FUNCTION__<<__LINE__<<"Rec start";

        //! toEngine 会自动run起来。
        if ( ERR_NONE != toEngine( dsoEngine::m_pRecEngine ) )
        {
            m_pPhy->mCcu.setCcuRun( 1 );
        }

        LOG_ENGINE();

        emit sigConRun( true );

        LOG_ENGINE();

        return ERR_NONE;
    }
    else if ( act == Rec_stop )
    {
        m_pPhy->mRecPlay.stopRec();

        if (0 != m_pPhy->mRecPlay.getNowIndex() )
        {
            m_pHistSession->m_RecDone = true;
            //qDebug()<<__FILE__<<__LINE__<<"---done:"<<m_pHistSession->m_RecDone;
        }

        toEngine( dsoEngine::m_pPlayEngine );

        LOG_ENGINE();

        emit sigConRun( false );

        return ERR_NONE;
    }
    else
    { return ERR_INVALID_INPUT; }
}
DsoErr dsoEngine::setRecordInterval( qlonglong ps )
{
    return m_pPhy->mRecPlay.setRecInterval( ps );
}
//! [0,max)
DsoErr dsoEngine::setRecordFrames( int maxFrames )
{
    Q_ASSERT( maxFrames > 0 );
    m_pPhy->mRecPlay.setNow( 0 );
    m_pPhy->mRecPlay.setRange( 0, maxFrames-1);

    return ERR_NONE;
}

DsoErr dsoEngine::setPlayAction( PlayAction act )
{
    if ( act == Play_start )
    {
        m_bTraceRequest = true;

        //! 1、关闭显示和trace
        if(!m_pPhy->mRecPlay.getDisplayOn())
        {
            m_pPhy->mCcu.setTRACE_wave_bypass_wpu(1);
            m_bTraceRequest = false;
            m_pPhy->mCcu.flushRCache();
        }

        //! 2、播放
        m_pPhy->mRecPlay.startPlay();

        return ERR_NONE;
    }
    else if ( act == Play_stop )
    {
        //! 1、停止播放
        m_bTraceRequest = false;
        m_pPhy->mRecPlay.stopPlay();

        //! 2、恢复wpu
        m_pPhy->mCcu.setTRACE_wave_bypass_wpu(0);
        m_pPhy->mCcu.flushRCache();

        return ERR_NONE;
    }
    else
    { return ERR_INVALID_INPUT; }
}
DsoErr dsoEngine::setPlayInterval( qlonglong ps )
{
    return m_pPhy->mRecPlay.setPlayInterval( ps );
}
//! [a,b)
DsoErr dsoEngine::setPlayRange( int a, int b )
{
    Q_ASSERT( a > 0 && a <= b );
    m_pPhy->mRecPlay.setRange( a - 1, b - 1);

    return ERR_NONE;
}
DsoErr dsoEngine::setPlayNow( int now )
{
    Q_ASSERT( now > 0 );
    m_pPhy->mRecPlay.setNow( now - 1 );
    return ERR_NONE;
}

DsoErr dsoEngine::setPlayDirection( PlayDirection dir )
{
    m_pPhy->mRecPlay.setDirection( dir );

    return ERR_NONE;
}
DsoErr dsoEngine::setPlayLoop( bool loop )
{
    m_pPhy->mRecPlay.setLoop( loop );

    return ERR_NONE;
}

DsoErr dsoEngine::setStepPlay( int now )
{
    Q_ASSERT( now > 0 );

    m_pPhy->mRecPlay.setNow( now - 1 );
    m_pPhy->mRecPlay.loadNow();

    dsoEngine::playback(m_pHistSession, m_pViewSession);
//    m_pPhy->mRecPlay.stepPlay();

    return ERR_NONE;
}

int dsoEngine::getRecordCapacity()
{
    qlonglong depth = m_pViewSession->m_acquire.mCHFlow.mDepth.ceilLonglong();
    int       chCnt = toChCnt( m_pViewSession->getSaCHBm() );
    Q_ASSERT( chCnt > 0 );

    qlonglong slice     = (depth * chCnt + 1024 );
    qlonglong capacity  =  phy_ic_capacity;

    if( m_pViewSession->m_acquire.getAcquireMode() == Acquire_Average )
    {
        capacity = capacity - 0x1A00000 * 8;
        slice = slice * 4;
    }

    slice =  ( ((slice>>11) + 1) <<11 ); //!逻辑要求，最少2K

    //qDebug()<<__FILE__<<__LINE__<<"capacity:"<<capacity<<"slice:"<<slice;

    return (capacity / slice);
}
int dsoEngine::getFrameNow()
{
    return m_pPhy->mRecPlay.getNowIndex();
}
//! \todo
quint64 dsoEngine::getFrameTag( int /*frameId*/ )
{
    return m_pPhy->mRecPlay.getTag();;
}

bool dsoEngine::getRecPlayIdle()
{
    return m_pPhy->mRecPlay.isIdle();
}

