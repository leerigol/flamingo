
#include "../dsoengine.h"

#include "../../../baseclass/resample/reSample.h"

DsoErr dsoEngine::playback(CDsoSession *pSesHist,
                           CDsoSession *pSesView,
                           bool bPlayWpu,
                           bool bPlayTrace)
{
    Q_ASSERT( NULL != pSesHist );
    Q_ASSERT( NULL != pSesView );
    LOG_FPGA_STATUS()<<"before play"<<getDebugTime();
    DsoErr err;

    if( !needPlay( pSesHist, pSesView) )
    {
        return ERR_NONE;
    }

    if( ! m_pHistSession->m_bPlayValid )
    {
        return ERR_NONE;
    }

    //! 从一些特殊模式切回普通yt
    {
        m_pPhy->mCcu.setMODE_mode_roll_en(0);
        m_pPhy->mCcu.setMODE_mode_scan_en(0);
        m_pPhy->mCcu.setMODE_mode_fast_ref(0);

        flushWCache();
    }

    //! add_by_zx: tr6晚 1k wave bug
    m_pPhy->mSpuGp.outCOARSE_DELAY_MAIN_delay_en(1);

    //! 关掉普通Yt下回放那一次的clear是为了解决回放时波形闪烁
    //! 在慢扫描时还加上clear是为了防止屏幕右边的错误波形
    if( m_pHistSession->m_acquire.m_eTimeMode == Acquire_SCAN )
    {
        m_pPhy->mWpu.clearMemory();
    }

    //! open wpu write channel
    m_pPhy->mWpu.outdisplay_en( 2 );

    if(pSesView->m_acquire.m_eTimeMode == Acquire_XY)
    {
        m_pPhy->mWpu.setNoise( 2 );
    }
    else
    {
        m_pPhy->mWpu.setNoise( 0 );
    }
    //! 除了当前显示模式是XY的，其他的都需要回放
    if(pSesView->m_acquire.m_eTimeMode != Acquire_XY)
    {
        m_pPhy->mWpu.settime_base_mode(0);
    }

    //! vert play
    err = chPlay( pSesHist, pSesView ); 
    if ( err != ERR_NONE ) return err;

    //! la play
    err = laPlay( pSesHist, pSesView );
    if ( err != ERR_NONE ) return err;

    if ( sysHasArg("-no_horiplay" ))
    {
    }
    else
    {
        //! hori play
        err = horiPlay( pSesHist, pSesView );
        if(err != ERR_NONE)  return err;

        m_pPhy->mScuGp.setMODE_10G_chn_10G( pSesHist->getSaCHBm() );
    }

    extraPlay(pSesHist, pSesView);

    //! meas apply
    measApply(pSesHist, pSesView);

    //! search
    searchApply(pSesView);

    //! order
    applyDispOrder( pSesHist->getSaCHBm() );

    m_pPhy->flushWCache();

    //! close wpu channel
    m_pPhy->mWpu.outdisplay_en( 3 );

    if(bPlayTrace)
    {
       m_pPhy->mZynqFcuWr.endTrace();
    }

    //! 【zx；dba】从函数前面挪到这里，解决bug2625，clear后水平相关参数不生效问题。
    //! check session validate
    if ( pSesHist->getValid() )
    {}
    //! no play
    else
    { return ERR_NONE; }

    LOG_FPGA_STATUS()<<"after play"<<getDebugTime();
    doPlay(bPlayWpu, bPlayTrace);
    dsoEngine::m_bTraceRequestOnce = true;
    LOG_FPGA_STATUS()<<"after play"<<getDebugTime();
    return ERR_NONE;
}

bool dsoEngine::needPlay(CDsoSession *pSesHist, CDsoSession *pSesView)
{
    AcquireTimemode histTimeMode = pSesHist->m_acquire.m_eTimeMode;
    AcquireTimemode viewTimeMode = pSesView->m_acquire.m_eTimeMode;
    if( histTimeMode == Acquire_ROLL && viewTimeMode != Acquire_ROLL )
    {
        m_pPhy->mWpu.syncClear();
        return false;
    }
    if( histTimeMode != Acquire_ROLL && viewTimeMode == Acquire_ROLL)
    {
        m_pPhy->mWpu.syncClear();
        return false;
    }
    if( histTimeMode == Acquire_XY && viewTimeMode != Acquire_XY )
    {
        m_pPhy->mWpu.syncClear();
        return false;
    }
    if( histTimeMode != Acquire_XY && viewTimeMode == Acquire_XY)
    {
        m_pPhy->mWpu.syncClear();
        return false;
    }

    if( pSesHist->m_acquire.mCHFlow.getTEnd() <= pSesHist->m_acquire.mCHFlow.getT0() )
    {
        return false;
    }

    return true;
}

bool dsoEngine::needCheckParam(CDsoSession *pSesHist, CDsoSession *pSesView)
{
    AcquireTimemode histTimeMode = pSesHist->m_acquire.m_eTimeMode;
    AcquireTimemode viewTimeMode = pSesView->m_acquire.m_eTimeMode;
    if( histTimeMode == Acquire_ROLL && viewTimeMode != Acquire_ROLL )
    {
        return false;
    }

    if( histTimeMode != Acquire_ROLL && viewTimeMode == Acquire_ROLL)
    {
        return false;
    }

    return true;

}

DsoErr dsoEngine::chPlay( CDsoSession *pSesHist,
                            CDsoSession *pSesView )
{
    Q_ASSERT( NULL != pSesHist );
    Q_ASSERT( NULL != pSesView );

    float gain;
    float deltaOffset;
    CCH *pCHHist, *pCHView;

    //! analog ch
    for ( int i = 0; i < 4; i++ )
    {
        pCHHist = pSesHist->m_ch + i;
        pCHView = pSesView->m_ch + i;

        //! recing on
        if ( pCHHist->getOnOff() )
        {
            //! by wpu
            gain = (float)pCHHist->getAfeScale() / pCHView->getScale();

            //! stoped offset
            deltaOffset = (float)( pCHView->getOffset() - pCHHist->getOffset() ) / pCHView->getScale();

            //! invert
            if ( pCHView->getInvert() != pCHHist->getInvert() )
            {
                gain = -gain;

                deltaOffset = deltaOffset + 2.0f * pCHHist->getOffset()  / pCHView->getScale();
            }
            else
            { }

            //! wpu gain
            m_pPhy->mWpu.setCHGain( i, gain, deltaOffset );
        }
    }

    //! wpu on/off
    quint32 histBm, viewBm;
    histBm = pSesHist->getSaCHBm();
    viewBm = pSesView->getShowCHBm();

    //! analog
    histBm = trim_analog_bm( histBm );
    viewBm = trim_analog_bm( viewBm );

    int chCnt = chBmCnt( histBm );
    m_pPhy->mWpu.setanalog_mode_analog_ch_mode( chCnt );
    m_pPhy->mWpu.setanalog_mode_analog_ch_en( histBm & viewBm );
    //qDebug()<<"cnt"<<chCnt<<"ch en"<< (histBm & viewBm);

    m_pPhy->mWpu.fitScreen();

    //! wpu interleave
    wpuInterleave( pSesHist->getSaCHBm() );

    return ERR_NONE;
}

//! \todo la play
DsoErr dsoEngine::laPlay(
                 CDsoSession *pSesHist,
                 CDsoSession*
                )
{
    //! ses valid
    if ( pSesHist->mLa.getOnOff() )
    {
        laApply();
        LOG_ENGINE();
    }
    //! close la
    else
    { }

    return ERR_NONE;
}

DsoErr dsoEngine::horiPlay( CDsoSession *pSesHist,
                            CDsoSession *pSesView )
{
    Q_ASSERT( NULL != pSesHist );
    Q_ASSERT( NULL != pSesView );

    DsoErr err;
    err = horiMainPlay( pSesHist, pSesView );
    if(err != ERR_NONE) {return err;}

    err = horiZoomPlay( pSesHist, pSesView );
    return err;
}

DsoErr dsoEngine::chZoomDelayPlay(CDsoSession *pSesHist, CDsoSession *pSesView)
{
    ChTimeDelay timeDelay[4];

    //! set delay
    for ( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].setDelay( pSesView->m_ch[i].getDelay() - pSesHist->m_ch[i].getDelay());
    }

    //! slove
    int intxp = m_pViewSession->m_acquire.mZoomCHView.mIntx;
    Q_ASSERT( intxp > 0 );
    for ( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].slove( m_pViewSession->m_acquire.mCHFlow.mDotTime.ceilLonglong(),
                            m_pViewSession->m_acquire.mCHFlow.mDotTime.ceilLonglong() / intxp );
    }

    //! align
    qint32 maxPre, maxPost, minDly;

    maxPre = timeDelay[0].mPreSa;
    maxPost = timeDelay[0].mPostSa;
    minDly = timeDelay[0].mCoarseDelay;
    for ( int i = 1; i < array_count(timeDelay); i++ )
    {
        maxPre = qMax( timeDelay[i].mPreSa, maxPre );
        maxPost = qMax( timeDelay[i].mPostSa, maxPost );
        minDly = qMin( timeDelay[i].mCoarseDelay, minDly );
    }

    //! realign
    for( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].align( minDly );
    }

    //! config chx delay
    for( int i = 0; i < array_count(timeDelay); i++ )
    {
        m_pPhy->mSpu.setZoomDelay( i,
                                   timeDelay[i].mCoarseDelay,
                                   timeDelay[i].mFineDelay );
//        LOG_HORI()<<i<<timeDelay[i].mCoarseDelay<<timeDelay[i].mFineDelay;
    }

    return ERR_NONE;
}

DsoErr dsoEngine::chMainDelayPlay(CDsoSession *pSesHist, CDsoSession *pSesView)
{
    ChTimeDelay timeDelay[4];

    //! set delay
    for ( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].setDelay( pSesView->m_ch[i].getDelay() - pSesHist->m_ch[i].getDelay());
    }

    //! slove
    int intxp = pSesView->m_acquire.mMainCHView.mIntx;
    Q_ASSERT( intxp > 0 );
    for ( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].slove( pSesHist->m_acquire.mCHFlow.mDotTime.ceilLonglong(),
                            pSesHist->m_acquire.mCHFlow.mDotTime.ceilLonglong() / intxp );
    }

    //! align
    qint32 maxPre, maxPost, minDly;

    maxPre = timeDelay[0].mPreSa;
    maxPost = timeDelay[0].mPostSa;
    minDly = timeDelay[0].mCoarseDelay;
    for ( int i = 1; i < array_count(timeDelay); i++ )
    {
        maxPre = qMax( timeDelay[i].mPreSa, maxPre );
        maxPost = qMax( timeDelay[i].mPostSa, maxPost );
        minDly = qMin( timeDelay[i].mCoarseDelay, minDly );
    }

    //! realign
    for( int i = 0; i < array_count(timeDelay); i++ )
    {
        timeDelay[i].align( minDly );
    }

    //! config chx delay
    for( int i = 0; i < array_count(timeDelay); i++ )
    {
        m_pPhy->mSpu.setMainDelay( i,
                                   timeDelay[i].mCoarseDelay,
                                   timeDelay[i].mFineDelay );
//        LOG_HORI()<<i<<timeDelay[i].mCoarseDelay<<timeDelay[i].mFineDelay;
    }

    return ERR_NONE;
}

void dsoEngine::horiViewTune( CDsoSession *pSesView )
{
    //! check enable
    if ( pSesView->m_horizontal.getViewMode() == Horizontal_Zoom )
    {
        pSesView->m_acquire.mMainCHView.cfgTransfer( trace_max_size / 2 );
        pSesView->m_acquire.mMainLAView.cfgTransfer( trace_max_size / 2 );

        pSesView->m_acquire.mZoomCHView.cfgTransfer( trace_max_size / 2 );
        pSesView->m_acquire.mZoomLAView.cfgTransfer( trace_max_size / 2 );
    }
    else
    {
        pSesView->m_acquire.mMainCHView.cfgTransfer( trace_max_size );
        pSesView->m_acquire.mMainLAView.cfgTransfer( trace_max_size );

        pSesView->m_acquire.mZoomCHView.cfgTransfer( trace_max_size );
        pSesView->m_acquire.mZoomLAView.cfgTransfer( trace_max_size );
    }
}

DsoErr dsoEngine::horiMainPlay(
                     CDsoSession *pSesHist,
                     CDsoSession *pSesView )
{
    Q_ASSERT( NULL != pSesHist );
    Q_ASSERT( NULL != pSesView );

    //! ch view
    DsoErr err = ERR_NONE;
    qlonglong scale, offset, tLeft;
    m_pPhy->mRecPlay.setOnDisplay(true);

    if( m_pViewSession->m_acquire.m_eTimeMode == Acquire_ROLL )
    {
        CHorizontalView* pHView = &m_pViewSession->m_horizontal.getMainView();
        pHView->setOffset( pHView->getRollOffset());
    }

    CFlowView *pCHView = &pSesView->m_acquire.mMainCHView;;
    if( pSesView->m_horizontal.getViewMode() == Horizontal_Main )
    {
        pCHView->cfgTransfer(1000000);
        mViewSession.m_acquire.mMainCHCoarseView.cfgTransfer(1000000);
    }
    else
    {
        pCHView->cfgTransfer(500000);
        mViewSession.m_acquire.mMainCHCoarseView.cfgTransfer(500000);

        mViewSession.m_acquire.mZoomCHCoarseView.cfgTransfer(500000);
        mViewSession.m_acquire.mZoomCHView.cfgTransfer(500000);
    }

    scale = pSesView->m_horizontal.getMainView().getScale();
    offset = pSesView->m_horizontal.getMainView().getOffset();
    tLeft = - hori_div/2 * scale + offset;//当前屏幕的tLeft

    //! ch
    pCHView = &pSesView->m_acquire.mMainCHView;
    pCHView->set( offset, tLeft, scale/adc_hdiv_dots );
    pCHView->setAcq( pSesHist->m_acquire.getAcquireMode(),
                   pSesHist->m_acquire.m_chCnt,
                   pSesHist->m_acquire.mFinePos
                   );

    if(pSesHist->m_acquire.m_eTimeMode == Acquire_ROLL)
    {
        pCHView->viewOn( pSesHist->m_acquire.mCHFlow, true);     

        if((pCHView->mCompL1 * pCHView->mCompL2 * 2 ) > pCHView->mMLen * pCHView->mIntx)
        {
            if(pCHView->mMLen > 2000)
            {
                pCHView->mTransferLen = 2000;
                pCHView->mIntx = 1;
                pCHView->mCompL1 = pCHView->mMLen / 2000;
                pCHView->mCompL2 = 1000;
                pCHView->mFixCompL2 = 1000;
            }
            else
            {
                pCHView->mTransferLen = pCHView->mMLen;
                pCHView->mIntx = 1;
                pCHView->mCompL1 = 1;
                pCHView->mCompL2 = pCHView->mMLen / 2;
                pCHView->mFixCompL2 = pCHView->mMLen / 2;
            }
        }

        if( pCHView->mTransferLen == 0 )
        {
            m_pPhy->mWpu.outdisplay_en(3);
            m_pPhy->mWpu.syncClear();

            m_pPhy->mRecPlay.setOnDisplay(false);
            emit sigClearTrace();

            return ERR_INVALID_CONFIG;
        }
        err = m_pPhy->m_hori.setRollPlay( *pCHView );
    }
    else
    {
        qlonglong normScale = ll_roof125( scale );

        if(normScale == scale)
        {
            mViewSession.m_acquire.mMainCHView.viewOn(   mHistSession.m_acquire.mCHFlow,
                                                        scale,
                                                        offset );
        }
        else
        {
            mViewSession.m_acquire.mMainCHCoarseView.viewOn(   mHistSession.m_acquire.mCHFlow,
                                                        normScale,
                                                        offset );

            mViewSession.m_acquire.mMainCHView.viewOnPlayFine( &mViewSession.m_acquire.mMainCHCoarseView,
                                                          mHistSession.m_acquire.mCHFlow,
                                                          normScale,
                                                          scale,
                                                          offset );
        }

        //! 当屏幕上压缩后的原始点列数太少时
        if((pCHView->mCompL1 * pCHView->mCompL2 * 2 ) > pCHView->mMLen * pCHView->mIntx)
        {
            if(pCHView->mMLen > 2000)
            {
                pCHView->mTransferLen = 2000;
                pCHView->mIntx = 1;
                pCHView->mCompL1 = pCHView->mMLen / 2000;
                pCHView->mCompL2 = 1000;
                pCHView->mFixCompL2 = 1000;
            }
            else
            {
                pCHView->mTransferLen = pCHView->mMLen;
                pCHView->mIntx = 1;
                pCHView->mCompL1 = 1;
                pCHView->mCompL2 = pCHView->mMLen / 2;
                pCHView->mFixCompL2 = pCHView->mMLen / 2;
            }
        }

        if( pCHView->mTransferLen == 0 )
        {
            m_pPhy->mWpu.outdisplay_en(3);
            m_pPhy->mWpu.syncClear();

            m_pPhy->mRecPlay.setOnDisplay(false);
            emit sigClearTrace();

            return ERR_INVALID_CONFIG;
        }

        err = m_pPhy->m_hori.setMainPlay( *pCHView );
        if(err != ERR_NONE)
        {return err;}
    }

    //! 配置精细延迟
    quint32 finePos;
    if( fineTrigPosition( finePos, mViewSession.m_acquire.mMainCHCoarseView.getFlow().getDotTime().ceilLonglong(),
                          mViewSession.m_horizontal.getMainView().getScale(),
                          mViewSession.m_horizontal.getMainView().getOffset(),
                          mViewSession.m_acquire.mMainCHCoarseView.mIntx ) )
    {
        m_pPhy->mSpuGp.setFINE_DELAY_MAIN_chn_delay( finePos );
    }

    //! la
    CFlowView* pLaView = &pSesView->m_acquire.mMainLAView;
    *pLaView = *pCHView;
    return err;
}

DsoErr dsoEngine::horiZoomPlay( CDsoSession *pSesHist,
                     CDsoSession *pSesView )
{
    Q_ASSERT( NULL != pSesHist );
    Q_ASSERT( NULL != pSesView );

    //! check enable
    if ( pSesView->m_horizontal.getViewMode() == Horizontal_Zoom )
    {
        m_pPhy->mWpu.setzoom_en_en( 1 );
        m_pPhy->mCcu.setMODE_zoom_en( 1 );
    }
    else
    {
        m_pPhy->mWpu.setzoom_en_en( 0 );
        m_pPhy->mCcu.setMODE_zoom_en( 0 );
        return ERR_NONE;
    }

    //! ch view
    CFlowView *pCHView = &pSesView->m_acquire.mZoomCHView;

    qlonglong scale, offset, tLeft;
    scale = pSesView->m_horizontal.getZoomView().getScale();
    offset = pSesView->m_horizontal.getZoomView().getOffset();
    tLeft = -hori_div/2 * scale + offset;

    pCHView->set( offset, tLeft, scale/adc_hdiv_dots );
    pCHView->setAcq( pSesHist->m_acquire.getAcquireMode(),
                   pSesHist->m_acquire.m_chCnt,
                   pSesHist->m_acquire.mFinePos
                   );

    pCHView->viewOn( pSesHist->m_acquire.mCHFlow );

    qlonglong normScale = ll_roof125( scale );
    mViewSession.m_acquire.mZoomCHCoarseView.viewOn(   mHistSession.m_acquire.mCHFlow,
                                                normScale,
                                                offset );
    if(normScale == scale)
    {
        mViewSession.m_acquire.mZoomCHView.viewOn(   mHistSession.m_acquire.mCHFlow,
                                                    scale,
                                                    offset );
    }
    else
    {
        mViewSession.m_acquire.mZoomCHView.viewOnPlayFine( &mViewSession.m_acquire.mZoomCHCoarseView,
                                                      mHistSession.m_acquire.mCHFlow,
                                                      normScale,
                                                      scale,
                                                      offset );
    }

    if( pCHView->mTransferLen == 0 )
    {
        m_pPhy->mWpu.syncClear();
        m_pViewSession->m_acquire.mZoomCHView.mTransferLen = 0;
        return ERR_INVALID_CONFIG;
    }

    //! 当屏幕上压缩后的原始点列数太少时
    if((pCHView->mCompL1 * pCHView->mCompL2 * 2 ) > pCHView->mMLen * pCHView->mIntx)
    {
        if(pCHView->mMLen > 2000)
        {
            pCHView->mTransferLen = 2000;
            pCHView->mIntx = 1;
            pCHView->mCompL1 = pCHView->mMLen / 2000;
            pCHView->mCompL2 = 1000;
            pCHView->mFixCompL2 = 1000;
        }
        else
        {
            pCHView->mTransferLen = pCHView->mMLen;
            pCHView->mIntx = 1;
            pCHView->mCompL1 = 1;
            pCHView->mCompL2 = pCHView->mMLen / 2;
            pCHView->mFixCompL2 = pCHView->mMLen / 2;
        }
    }

    if( pCHView->mTransferLen == 0 )
    {
        m_pPhy->mWpu.outdisplay_en(3);
        m_pPhy->mWpu.syncClear();
        emit sigClearTrace();
        return ERR_INVALID_CONFIG;
    }


    DsoErr err = m_pPhy->m_hori.setZoomPlay( *pCHView );

    if(err != ERR_NONE)
    { return err; }

    //! 配置精细延迟
    quint32 finePos;
    if( fineTrigPosition( finePos, mViewSession.m_acquire.mZoomCHCoarseView.getFlow().getDotTime().ceilLonglong(),
                          mViewSession.m_horizontal.getZoomView().getScale(),
                          mViewSession.m_horizontal.getZoomView().getOffset(),
                          mViewSession.m_acquire.mZoomCHCoarseView.mIntx ) )
    {
        m_pPhy->mSpuGp.setFINE_DELAY_ZOOM_chn_delay( finePos );
    }

    //! la
    CFlowView* pLaView = &pSesView->m_acquire.mZoomLAView;
    *pLaView = *pCHView;
    pCHView->debugOut();
    return err;
}

DsoErr dsoEngine::calcHoriPara(CDsoSession *pSesHist, CDsoSession *pSesView)
{
    //! ch view
    CFlowView *pCHView = &pSesView->m_acquire.mZoomCHView;

    qlonglong scale, offset, tLeft;
    scale = pSesView->m_horizontal.getZoomView().getScale();
    offset = pSesView->m_horizontal.getZoomView().getOffset();
    tLeft = -hori_div/2 * scale + offset;

    pCHView->set( offset, tLeft, scale/adc_hdiv_dots );
    pCHView->setAcq( pSesHist->m_acquire.getAcquireMode(),
                   pSesHist->m_acquire.m_chCnt,
                   pSesHist->m_acquire.mFinePos
                   );

    pCHView->viewOn( pSesHist->m_acquire.mCHFlow );

    qlonglong normScale = ll_roof125( scale );
    pSesView->m_acquire.mZoomCHCoarseView.viewOn(   pSesHist->m_acquire.mCHFlow,
                                                normScale,
                                                offset );
    if(normScale == scale)
    {
        pSesView->m_acquire.mZoomCHView.viewOn(      pSesHist->m_acquire.mCHFlow,
                                                scale,
                                                offset );
    }
    else
    {
        pSesView->m_acquire.mZoomCHView.viewOnPlayFine( &(pSesView->m_acquire.mZoomCHCoarseView),
                                                      mHistSession.m_acquire.mCHFlow,
                                                      normScale,
                                                      scale,
                                                      offset );
    }

    return ERR_NONE;
}

DsoErr dsoEngine::extraPlay(CDsoSession *pSesHist, CDsoSession *pSesView)
{
    Q_UNUSED(pSesView);
    //! 继承抗混叠对于压缩的配置
    if( pSesHist->m_acquire.getAntiAliasing() )
    {
        if(pSesHist->m_acquire.m_chCnt * pSesHist->m_acquire.getChSaRate() <= 5e15)
        {
            //! 抗混叠打开时需要打开峰值压缩
            m_pPhy->mSpu.setCOMPRESS_MAIN_peak(1);
            m_pPhy->mSpu.setCOMPRESS_ZOOM_peak(1);
        }
    }
    m_pPhy->mCcu.setPLOT_DELAY(time_ms(40) / TD_TICK);
    return ERR_NONE;
}

DsoErr dsoEngine::doPlay(bool bPlayWpu, bool bPlayTrace)
{
    m_pPhy->mCcu.inMODE();

    //qDebug()<<__FILE__<<__LINE__<<"RecDone:"<<m_pHistSession->m_RecDone;
    //! in play mode
    if ( m_pHistSession->m_RecDone)
    {
        //! check wpu len && spu len
        if ( m_pPhy->mWpu.getMainLength() < 1 ||
             m_pPhy->mSpuGp.getTxLen( 0 ) < 1 ) //! main len
        {
//            m_pPhy->mWpu.syncClear();
        }
        else
        {
            //! reload
            m_pPhy->mRecPlay.loadNow();
            //! play
            m_pPhy->mRecPlay.stepPlay();
        }
    }
    else if(1 == m_pPhy->mCcu.getMODE_mode_import_play())
    {/*! [dba: 2018-04-21] run:之前退出导入回放模式.*/
        m_pPhy->mCcu.outTRACE_trace_once_req(1);
        m_pPhy->mCcu.outTRACE_search_once_req(1);
        m_pPhy->mCcu.setCcuRun( 1 );
        m_pPhy->mCcu.flushWCache();
        m_pPhy->mCcu.waitStop();
    }
    //! normal play
    else
    {
        //! check wpu len && spu len
        if ( m_pPhy->mWpu.getMainLength() < 1 ||
             m_pPhy->mSpuGp.getTxLen( 0 ) < 1 ) //! main len
        {
//            m_pPhy->mWpu.syncClear();
        }
        else
        {
            Q_ASSERT( m_pPhy->mWpu.getwave_view_length() > 0 );

            LOG_DBG()<<getDebugTime();
            m_pPhy->mCcu.playLast(bPlayWpu, bPlayTrace);
        }
    }
    LOG_DBG()<<getDebugTime();
    m_pPhy->mWpu.waitWpuStop();
    LOG_DBG()<<getDebugTime();
    return ERR_NONE;
}
