//#define _DEBUG
#include "../dsoengine.h"

void dsoEngine::applyMask(CDsoSession *pSesView)
{
    Q_ASSERT(NULL != pSesView);

    //!***********************index en*******************************/
    //! src  =[1,2,4,8].
    int src   = pSesView->m_mask.mMaskSrc;
    int chCnt = pSesView->m_acquire.m_chCnt;
    //qDebug()<<__FILE__<<__LINE__<< "-----src:"<<src<<"ch cnt: "<<chCnt ;

    if(1 == chCnt)
    {
         m_pPhy->mSpuGp.setMASK_CTRL_mask_index_en( 0xf );
    }
    else if(2 == chCnt)
    {
        if( (chan1 == src) || (chan2 == src) )
        {
            m_pPhy->mSpuGp.setMASK_CTRL_mask_index_en( 0x5);
        }
        else
        {
            m_pPhy->mSpuGp.setMASK_CTRL_mask_index_en( 0xA );
        }
    }
    else if(4 == chCnt)
    {
        m_pPhy->mSpuGp.setMASK_CTRL_mask_index_en(src);
    }
    else
    {
        Q_ASSERT(false);
    }
}

DsoErr dsoEngine::setMaskOn( bool b )
{
    m_pViewSession->m_mask.mMaskEn = b;

    //! add_by_zx for bug:开关mask错误波形
    //m_pPhy->mSpuGp.outMASK_COMPRESS_en( b );

    //! add_by_zx for bug 1521:在慢扫描和fast模式无论打开或关闭mask都可能切换模式
    if( m_pViewSession->m_acquire.m_eTimeMode == Acquire_SCAN ||
            m_pViewSession->m_bFastTimeBase )
    {
        if( m_pViewSession->m_control.m_eAction == Control_Run)
        {
            return ERR_ACTION_DISABLED;
        }
    }

    m_pPhy->mCcu.setMODE_mask_pf_en( b );

    return ERR_NONE;
}
//! chan1~chan4
DsoErr dsoEngine::setMaskSource( qint32 chbm )
{
    m_pViewSession->m_mask.mMaskSrc =  (chbm >> 1);
    LOG_ENGINE()<<chbm;

    applyMask(m_pViewSession);

    return ERR_NONE;
}

DsoErr dsoEngine::setMaskOutAction( MaskActionEvent outputAction )
{
    m_pPhy->mSpuGp.setMASK_OUTPUT_pass_fail( outputAction );
    return ERR_NONE;
}

DsoErr dsoEngine::setMaskOutput( bool bOn )
{
    if ( bOn )
    {
        m_pPhy->mMisc.setMISC_TRIGOUT_SEL_sel( 3 );
    }
    else
    {
    }

    return ERR_NONE;
}
//! true -- positive pulse
DsoErr dsoEngine::setMaskOutPN( bool bPN )
{
    m_pPhy->mSpuGp.setMASK_OUTPUT_inv( !bPN );
    return ERR_NONE;
}
//! pulse time
DsoErr dsoEngine::setMaskOutTime( qlonglong timeps )
{
    qlonglong width;

    width = timeps /10000;  //! 10ns unit
    if ( width > 1 )
    { width -= 1; }
    else
    { width = 0; }

    //! range
    if ( width > 0x0ffffff )
    { width = 0xfffffff; }

    m_pPhy->mSpuGp.setMASK_OUTPUT_pulse_width( width );

    return ERR_NONE;
}
DsoErr dsoEngine::setMaskOutDelay( qlonglong)
{
    //! \todo fsm delay
    return ERR_NONE;
}

DsoErr dsoEngine::setMaskActionAction( MaskActionAction action )
{
    //! 当开启录制功能时，fail和pass都录制。
    m_pPhy->mCcu.setMODE_mask_save_fail(1);
    m_pPhy->mCcu.setMODE_mask_save_pass(1);

    if( action == mask_action_stop)
    {
        m_pPhy->mCcu.setMODE_mask_err_stop_en( mask_action_stop);
    }
    else
    {
        m_pPhy->mCcu.setMODE_mask_err_stop_en( mask_action_none);
    }

    return ERR_NONE;
}

DsoErr dsoEngine::setMaskRule( EngineMaskRule *pRule )
{
    Q_ASSERT( NULL != pRule );

    //! check ch
    if( pRule->mChan < chan1 || pRule->mChan > chan4 )
    { return ERR_INVALID_INPUT; }

    //! source
    //! \todo split to each chan
//    int index = pRule->mChan - chan1;
//    int chx;
//    chx = set_bit( chx, index );
    int chx = 0xf;          //! all channel

    if ( pRule == NULL )
    { return ERR_MEMORY_FAIL; }

    //! template rule
    EngineMaskRule *pCfgRule = newRule();
    fillRull( pCfgRule, pRule );

    //! config
    m_pPhy->mSpuGp.setMASK_TAB_tab_index( chx );

    m_pPhy->mSpuGp.outMASK_CTRL_mask_we( 1 );

    //! foreach point
#ifdef _DEBUG
    QString debugStr = QString("\r\n---------------------------------------\r\n");
#endif
    for ( int i = 0; i < pCfgRule->m_pRules[0]->mLen; i++ )
    {
        m_pPhy->mSpuGp.setMASK_TAB_column_addr( i );

        m_pPhy->mMask.setLess( pCfgRule->m_pRules[0]->mRule[i],
                               pCfgRule->mRuleValidates[0] );

        m_pPhy->mMask.setGreater( pCfgRule->m_pRules[3]->mRule[i],
                                  pCfgRule->mRuleValidates[3] );

        m_pPhy->mSpuGp.outMASK_TAB();
#ifdef _DEBUG
        debugStr += QString("[%1 %2 %3 %4 %5 %6],")
                    .arg(m_pPhy->mSpuGp.getMASK_TAB_tab_index(),              3 )
                    .arg(m_pPhy->mSpuGp.getMASK_TAB_column_addr(),            3 )
                    .arg(m_pPhy->mSpuGp.getMASK_TAB_threshold_h_greater_en(), 3 )
                    .arg(m_pPhy->mSpuGp.getMASK_TAB_threshold_h(),            3 )
                    .arg(m_pPhy->mSpuGp.getMASK_TAB_threshold_l_less_en(),    3 )
                    .arg(m_pPhy->mSpuGp.getMASK_TAB_threshold_l(),            3);
#endif
    }

    m_pPhy->mSpuGp.outMASK_CTRL_mask_we( 0 );

    deleteRule( pCfgRule );
#ifdef _DEBUG
    qDebug()<<__FILE__<<__LINE__
           <<"\n---------------------------------------\n"
           <<qPrintable(debugStr);
#endif
    return ERR_NONE;
}

//! 1->0
DsoErr dsoEngine::setMaskRst()
{
    m_pPhy->mSpuGp.setMASK_CTRL_sum_clr(1);
    m_pPhy->mSpuGp.setMASK_CTRL_cross_clr(1);
    m_pPhy->mSpuGp.outMASK_CTRL();

    m_pPhy->mSpuGp.setMASK_CTRL_sum_clr(0);
    m_pPhy->mSpuGp.setMASK_CTRL_cross_clr(0);
    m_pPhy->mSpuGp.outMASK_CTRL();

    return ERR_NONE;
}

EngineMaskRule *dsoEngine::newRule( )
{
    EngineMaskRule *pRule;

    //! rule
    pRule = new EngineMaskRule();
    if ( NULL == pRule )
    { return NULL; }

    //! create the template
    if ( NULL == pRule->create() )
    {
        delete pRule;
        return NULL;
    }

    return pRule;

}
//! deload the rule on config
void dsoEngine::fillRull( EngineMaskRule *pRule, EngineMaskRule *pInRule )
{
    Q_ASSERT( NULL != pRule && NULL != pInRule );

    MaskRule *pDst, *pSrc;

    //! foreach rule
    for( int i = 0; i < array_count(pRule->m_pRules); i++ )
    {
        pSrc = pInRule->m_pRules[i];
        pDst = pRule->m_pRules[i];

        //! deload data
        if ( pInRule->m_pRules[i] != NULL )
        {
            memcpy( pDst->mRule + pSrc->mOffset,
                    pSrc->mRule,
                    pSrc->mLen * sizeof( pSrc->mRule[0] )
                    );

            pRule->mRuleValidates[i] = true;
        }
        else
        {
            pRule->mRuleValidates[i] = false;
        }
    }

}
void dsoEngine::deleteRule( EngineMaskRule *pRule )
{
    Q_ASSERT( NULL != pRule );

    delete pRule;
}

#define trim_bits( val )    val = val & ( 0xffffffffffffll );
DsoErr dsoEngine::queryMaskReport( EngineMaskReport *pValue )
{
    Q_ASSERT( NULL != pValue );
    quint32 l,h;

    //! mask rd
    m_pPhy->mSpuGp.outMASK_CTRL_mask_result_rd( 1 );

    //! match
    l = m_pPhy->mSpuGp.inMASK_CROSS_CNT_L_CHA();
    h = m_pPhy->mSpuGp.inMASK_CROSS_CNT_H_CHA();
    pValue->mA = pack_uint64( h, l );
    trim_bits(pValue->mA);

    l = m_pPhy->mSpuGp.inMASK_CROSS_CNT_L_CHB();
    h = m_pPhy->mSpuGp.inMASK_CROSS_CNT_H_CHB();
    pValue->mB = pack_uint64( h, l );
    trim_bits(pValue->mB);

    l = m_pPhy->mSpuGp.inMASK_CROSS_CNT_L_CHC();
    h = m_pPhy->mSpuGp.inMASK_CROSS_CNT_H_CHC();
    pValue->mC = pack_uint64( h, l );
    trim_bits(pValue->mC);

    l = m_pPhy->mSpuGp.inMASK_CROSS_CNT_L_CHD();
    h = m_pPhy->mSpuGp.inMASK_CROSS_CNT_H_CHD();
    pValue->mD = pack_uint64( h, l );
    trim_bits(pValue->mD);

    //! total
    l = m_pPhy->mSpuGp.inMASK_FRM_CNT_L();
    h = m_pPhy->mSpuGp.inMASK_FRM_CNT_H();
    pValue->mTotal = pack_uint64( h, l );

//    //! pass
//    Q_ASSERT( pValue->mTotal >= pValue->mFail );
//    pValue->mPass = pValue->mTotal - pValue->mFail;

    //! \todo split to each channel
    m_pPhy->mSpuGp.outMASK_CTRL_mask_result_rd( 0 );

    return ERR_NONE;
}
