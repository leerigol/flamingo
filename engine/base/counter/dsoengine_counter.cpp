#include "../dsoengine.h"

#define thisCounter m_pPhy->mCounter

quint32 dsoEngine::toCounterSource( Chan ch )
{
    if ( ch == ext )
    {
        return IPhyFrequ::counter_ext;
    }
    else if ( ch >= chan1 && ch <= chan4 )
    {
        return (IPhyFrequ::counter_ch1 + ch - chan1);
    }
    else if ( ch >= d0 && ch <= d15 )
    {
        return (IPhyFrequ::counter_la0 + ch - d0 );
    }
    else
    {
        Q_ASSERT( false );
        return IPhyFrequ::counter_ch1;
    }
}

quint32 dsoEngine::toCounterLaSel( Chan la )
{
    Q_ASSERT( la >= d0 && la <= d15 );

    return la - d0;
}

DsoErr dsoEngine::setCounterEn( bool b )
{
    thisCounter.setfreq_cfg_1_ref_en( b );

    return ERR_NONE;
}
DsoErr dsoEngine::setCounterSource( Chan src )
{
    thisCounter.setfreq_cfg_1_src_sel( toCounterSource( src) );
    return ERR_NONE;
}

DsoErr dsoEngine::setCounterRst()
{
    thisCounter.setfreq_cfg_1_ref_en( 0 );
    thisCounter.setfreq_cfg_1_ctrl_clear( 1 );
    thisCounter.flushWCache();
    thisCounter.setfreq_cfg_1_ref_en( 1 );
    thisCounter.setfreq_cfg_1_ctrl_clear( 0 );
    thisCounter.flushWCache();

    return ERR_NONE;
}


DsoErr dsoEngine::setCounterCfg( EngineCounter *pEngineCfg )
{
    Q_ASSERT( NULL != pEngineCfg );

    thisCounter.setfreq_cfg_1_src_sel( toCounterSource(pEngineCfg->mSrc) );


    thisCounter.setfreq_cfg_1_gate_time(
                            thisCounter.gateTimeIndex( pEngineCfg->mGateTime )
                                        );

    thisCounter.setfreq_cfg_1_sel_gate( toCounterSource(pEngineCfg->mGateSrc) );
    thisCounter.setfreq_cfg_1_sel_mode( pEngineCfg->mMode );
    thisCounter.setfreq_cfg_1_ctrl_clear( pEngineCfg->mClear );
    thisCounter.setfreq_cfg_1_gate_posi( pEngineCfg->mGate );
    thisCounter.setfreq_cfg_1_sel_event( pEngineCfg->mEvent );

    return ERR_NONE;
}
DsoErr dsoEngine::queryCounterValue( EngineCounterValue *pEngineCntVal )
{
#if 1
    Q_ASSERT( NULL != pEngineCntVal );

    thisCounter.cacheIn();

    pEngineCntVal->mThead = thisCounter.getstart_time();
    pEngineCntVal->mTtail = thisCounter.getend_time();
    pEngineCntVal->mEdgeCnt = thisCounter.getcounter();
    pEngineCntVal->signalWidth = (((quint64)(thisCounter.getsignal_wide_l()))<<0)
                                 | (((quint64)(thisCounter.getsignal_wide_h()))<<32);

    pEngineCntVal->signalEvent = (((quint64)(thisCounter.getsignal_event_l()))<<0)
                                 | (((quint64)(thisCounter.getsignal_event_h()))<<32);


    pEngineCntVal->mTimeUnit = 400;    //! ps

    return ERR_NONE;
#else

    Q_ASSERT( NULL != pEngineCntVal );

    //!检测id，配置变化时，conter的数据无效。
    m_pPhy->mScu.inCONFIG_ID();
    int cfgId_1 = m_pPhy->mZynqFcuRd.getframe_id_frame_trace_id_expand();
    int cfgId_2 =  m_pPhy->mScu.getCONFIG_ID();

    if(cfgId_1 != cfgId_2)
    {
        qDebug()<<__FILE__<<__LINE__<<"cfg change:"<<cfgId_1<<cfgId_2;
        return ERR_INVALID_CONFIG;
    }

    thisCounter.cacheIn();

    pEngineCntVal->mThead = thisCounter.getstart_time();
    pEngineCntVal->mTtail = thisCounter.getend_time();
    pEngineCntVal->mEdgeCnt = thisCounter.getcounter();
    pEngineCntVal->signalWidth = (((quint64)(thisCounter.getsignal_wide_l()))<<0)
                                 | (((quint64)(thisCounter.getsignal_wide_h()))<<32);

    pEngineCntVal->signalEvent = (((quint64)(thisCounter.getsignal_event_l()))<<0)
                                 | (((quint64)(thisCounter.getsignal_event_h()))<<32);


    pEngineCntVal->mTimeUnit = 400;    //! ps

    return ERR_NONE;
#endif

}


