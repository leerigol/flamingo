
#include "../dsoengine.h"
#include "../../../meta/crmeta.h"
#define selfAcq     m_pViewSession->m_acquire

//! acquire
DsoErr dsoEngine::setAcquireMode( AcquireMode acqMode )
{
    selfAcq.setAcquireMode( acqMode );

    return ERR_NONE;
}

DsoErr dsoEngine::applyAcquireMode( AcquireMode acqMode )
{
    //! acquire mode
    if ( Acquire_Normal == acqMode )
    {
        m_pPhy->mSpu.setCTRL_peak( 0 );
        m_pPhy->mSpu.setCTRL_hi_res( 0 );
        m_pPhy->mSpu.setCTRL_trace_avg( 0 );
    }
    else if ( Acquire_Peak == acqMode )
    {
        m_pPhy->mSpu.setCTRL_hi_res( 0 );
        m_pPhy->mSpu.setCTRL_trace_avg( 0 );
    }
    else if ( Acquire_Average == acqMode )
    {
        m_pPhy->mSpu.setCTRL_peak( 0 );
        m_pPhy->mSpu.setCTRL_hi_res( 0 );
        m_pPhy->mSpu.setCTRL_trace_avg( 1 );
    }
    else if ( Acquire_HighResolution == acqMode )
    {
        m_pPhy->mSpu.setCTRL_peak( 0 );
        m_pPhy->mSpu.setCTRL_hi_res( 1 );
        m_pPhy->mSpu.setCTRL_trace_avg( 0 );
    }
    else
    {
        return ERR_INVALID_INPUT;
    }

    return ERR_NONE;
}

DsoErr dsoEngine::setAverageCount( int count )
{
    selfAcq.setAverageCount( count );

    m_pPhy->mSpu.setAverageCount( count );

    LOG_DBG()<<count;
    return ERR_NONE;
}
DsoErr dsoEngine::setInterplateMode( AcquireInterplate interplate )
{
    selfAcq.setInterplate( interplate );

    return ERR_NONE;
}
DsoErr dsoEngine::setTimeMode( AcquireTimemode timeMode )
{
    selfAcq.setTimeMode( timeMode );

    if ( timeMode == Acquire_XY )
    {
        m_pPhy->mWpu.setdata_mode( 4 );       //! 4 -- xy, 20 -- xyz
        m_pPhy->mWpu.setxy0_en( 1 );
        m_pPhy->mWpu.setxy1_en( 0 );
        m_pPhy->mWpu.setxy2_en( 1 );
        m_pPhy->mWpu.setxy3_en( 0 );

        //! ch1 offset to middle
        //! board meta
        r_meta::CBMeta meta;
        QRect rect;
        meta.getMetaVal("phy/hori/xy/full", rect );
        m_pPhy->mWpu.setxy0_en_column_start( rect.left() );
        m_pPhy->mWpu.setxy2_en_column_start( rect.left() );
    }
    else
    {
        m_pPhy->mWpu.setdata_mode( 0 );
    }
    return ERR_NONE;
}

DsoErr dsoEngine::setSampleMode( AcquireSamplemode saMode )
{
    selfAcq.setSampleMode( saMode );

    return ERR_NONE;
}

DsoErr dsoEngine::setAntiAliasing( bool b )
{
    selfAcq.setAntiAliasing( b );

//    m_pPhy->mSpu.setCTRL_anti_alias( b );

    return ERR_NONE;
}

DsoErr dsoEngine::setMemDepth( int depth )
{
    selfAcq.setChDepth( depth );

    return ERR_NONE;
}
int dsoEngine::getMemDepth()
{
    return selfAcq.getChDepth();
}
DsoErr dsoEngine::setSamplRate( qlonglong sa )
{
    selfAcq.setChSaRate( sa );

    return ERR_NONE;
}
qlonglong dsoEngine::getSampleRate()
{
    return selfAcq.getChSaRate();
}

DsoErr dsoEngine::setLaMemDepth( int depth )
{
    selfAcq.setLaDepth( depth );
    return ERR_NONE;
}
int dsoEngine::getLaMemDepth()
{
    return selfAcq.getLaDepth();
}
DsoErr dsoEngine::setLaSamplRate( qlonglong sa )
{
   selfAcq.setLaSaRate( sa );

   return ERR_NONE;
}
qlonglong dsoEngine::getLaSampleRate()
{
    return selfAcq.getLaSaRate();
}

dsoFract dsoEngine::getChLength()
{
    return selfAcq.getChLength();
}
dsoFract dsoEngine::getLaLength()
{
    return selfAcq.getLaLength();
}

void dsoEngine::snapAcquireState()
{
    m_pPhy->mSpu.snapState();
}

DsoErr dsoEngine::setSysRawBand(Bandwidth bw)
{
    selfAcq.setSysRawBand( bw );
    return ERR_NONE;
}
