
#include "../dsoengine.h"

void dsoEngine::wpuInterleave( quint32 chBm )
{
    //! split to groups
    AdcGroups groups;
    toAdcGroups( chBm, &groups );

    if ( groups.mGroupCnt == 1 )
    {
        Q_ASSERT( groups.mGroups[0].mSlaveCnt == 3 );

        m_pPhy->mWpu.setInterleave(
                    groups.mGroups[0].mMaster,
                    groups.mGroups[0].mSlaves[0],
                    groups.mGroups[0].mSlaves[1],
                    groups.mGroups[0].mSlaves[2]
                    );
    }
    else if ( groups.mGroupCnt == 2 )
    {
        Q_ASSERT( groups.mGroups[0].mSlaveCnt == 1 );
        Q_ASSERT( groups.mGroups[1].mSlaveCnt == 1 );

        m_pPhy->mWpu.setInterleave(
                    groups.mGroups[0].mMaster,
                    groups.mGroups[0].mSlaves[0]
                    );

        m_pPhy->mWpu.setInterleave(
                    groups.mGroups[1].mMaster,
                    groups.mGroups[1].mSlaves[0]
                    );
    }
    else if ( groups.mGroupCnt == 4 )
    {}
}

void dsoEngine::logWpuScuStatus()
{
#ifdef _LOG_FPGA
    m_pPhy->mCcu.inCTRL();
    qDebug()<<"ccu ctrl";
    qDebug("%X",m_pPhy->mCcu.getCTRL());

    m_pPhy->mWpu.inwpu_status();
    qDebug()<<"wpu status";
    qDebug("%X",m_pPhy->mWpu.getwpu_status());
#endif
}
