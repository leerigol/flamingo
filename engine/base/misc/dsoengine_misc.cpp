
#include "../dsoengine.h"

//! cpld
quint16 dsoEngine::getCpldVer()
{
    return m_pPhy->mBoard.getCpldVer();
}
quint16 dsoEngine::getMBVer()
{
    return m_pPhy->mBoard.getMBVer();
}

DsoErr dsoEngine::writeReg( EnginePara &para )
{
    //! deload para
    EngineBus bus;
    quint32 addr, val;

    bus = (EngineBus)para[0]->u32Val;
    addr = para[1]->u32Val;
    val = para[2]->u32Val;

    //! 3 buses
    Q_ASSERT( m_pPhy->mBuses.size() >= engine_bus_top );

    IBus *pBus;
    pBus = m_pPhy->mBuses[ bus - engine_bus_spi_cpld_0 ];
    Q_ASSERT( NULL != pBus );

    if ( engine_bus_axi == bus )
    {
        pBus->write( val, addr );
        return ERR_NONE;
    }
    else if ( engine_bus_spi_cpld_0 == bus )
    {
        pBus->write( val, addr, 0 );
        return ERR_NONE;
    }
    else if ( engine_bus_spi_cpld_1 == bus )
    {
        pBus->write( val, addr, 1 );
        return ERR_NONE;
    }
    else
    { return ERR_INVALID_INPUT; }
}
quint32 dsoEngine::readReg( EnginePara &para )
{
    EngineBus bus;
    quint32 addr;

    bus = (EngineBus)para[0]->u32Val;
    addr = para[1]->u32Val;

    //! 4 buses
    Q_ASSERT( m_pPhy->mBuses.size() >= engine_bus_top );

    IBus *pBus;
    pBus = m_pPhy->mBuses[ bus - engine_bus_spi_cpld_0 ];
    Q_ASSERT( NULL != pBus );

    quint32 dwdata;
    quint16 wdata;
    if ( engine_bus_axi == bus )
    {
        pBus->read( dwdata, addr );

        return dwdata;
    }
    else if ( engine_bus_spi_cpld_0 == bus )
    {
        pBus->read( wdata, addr, 0 );

        return wdata;
    }
    else if ( engine_bus_spi_cpld_1 == bus )
    {
        pBus->read( wdata, addr, 1 );

        return wdata;
    }
    else
    { return 0; }
}

DsoErr dsoEngine::rstBoardCtp()
{
    m_pPhy->mBoard.rstCtp();
    return ERR_NONE;
}

DsoErr dsoEngine::clrBoardInt()
{
    m_pPhy->mBoard.clrInt();
    return ERR_NONE;
}
quint16 dsoEngine::getBoardIntStat()
{
    return m_pPhy->mBoard.readIntStat();
}
quint16 dsoEngine::getBoardIntEn()
{
    return m_pPhy->mBoard.readIntEn();
}
quint16 dsoEngine::getBoardIntMask()
{
    return m_pPhy->mBoard.readIntMask();
}

DsoErr dsoEngine::testBoardWrite( quint16 val )
{
    m_pPhy->mBoard.testWrite( val );
    return ERR_NONE;
}
quint16 dsoEngine::testBoardRead()
{
    m_pPhy->mBoard.testRead();
    return 0;
}
DsoErr dsoEngine::tickBeep()
{
    m_pPhy->mBeeper.tick();
    return ERR_NONE;
}
DsoErr dsoEngine::shortBeep()
{
    m_pPhy->mBeeper.shortBeep();
    return ERR_NONE;
}
DsoErr dsoEngine::stopBeep()
{
    m_pPhy->mBeeper.stopBeep();
    return ERR_NONE;
}
DsoErr dsoEngine::startBeep()
{
    m_pPhy->mBeeper.startBeep();
    return ERR_NONE;
}

DsoErr dsoEngine::intervalBeep()
{
    return ERR_NONE;
}

DsoErr dsoEngine::setLcdLight( int light )
{
    m_pPhy->mLcd.setBklight( light );
    return ERR_NONE;
}
DsoErr dsoEngine::setFanSpeed( int speed )
{
    m_pPhy->mFan.setSpeed( speed );
    return ERR_NONE;
}

DsoErr dsoEngine::setProbeEn( bool en )
{
    m_pPhy->mMisc.setMISC_PROBE_EN_en( en );
    return ERR_NONE;
}
DsoErr dsoEngine::setTrigOutSel( int sel )
{
    m_pPhy->mMisc.setMISC_TRIGOUT_SEL_sel( sel );
    return ERR_NONE;
}

DsoErr dsoEngine::setLedOn( int led )
{
    m_pPhy->mLed.seLedOp( led, true );
    return ERR_NONE;
}
DsoErr dsoEngine::setLedOff( int led )
{
    m_pPhy->mLed.seLedOp( led, false );
    return ERR_NONE;
}

DsoErr dsoEngine::setLedOp( int led, bool b )
{
    m_pPhy->mLed.seLedOp( led, b );

    return ERR_NONE;
}

DsoErr dsoEngine::setLedStatus( int leds )
{
    LOG_DBG()<<QString::number( leds, 16 );
    for ( int i = led_base; i <= led_top; i++ )
    {
         m_pPhy->mLed.seLedOp( i, get_bit(leds,i) );
    }

    return ERR_NONE;
}

int dsoEngine::getLedStatus()
{
    return m_pPhy->mLed.getLed();
}

DsoErr dsoEngine::setPowerSwitch( bool b )
{
    m_pPhy->mBoard.setSwitch( b );

    return ERR_NONE;
}
bool dsoEngine::getPowerSwitch()
{
    bool b;
    b = m_pPhy->mBoard.getSwitch();
    return b;
}
DsoErr dsoEngine::setPowerRestart()
{
    m_pPhy->mBoard.setRePower();
    return ERR_NONE;
}
DsoErr dsoEngine::setPowerShutdown()
{
    m_pPhy->mBoard.setShutDown();
    return ERR_NONE;
}

//! adc
quint32 dsoEngine::getAdcRomId( int id )
{
    Q_ASSERT( id >= 0 && id <= 1 );

    if ( m_pPhy->isDs8000() )
    {
        return m_pPhy->mAdc[id].getRomID();
    }
    else
    {
        return m_pPhy->mAdc[0].getRomID();
    }

}
DsoErr dsoEngine::getAdcUuid( unsigned char *outBuf )
{
    Q_ASSERT( NULL != outBuf );

    m_pPhy->mAdc[0].getUuid( outBuf );

    return ERR_NONE;
}

//! afe
quint32 dsoEngine::getAfeVer( int id )
{
    Q_ASSERT( id >=0 && id <= 3 );

    return m_pPhy->mVGA[id].getVersion();
}

//! fpgas
quint32 dsoEngine::getSpuVer()
{
    return m_pPhy->mSpu.inVERSION();
}
quint32 dsoEngine::getScuVer()
{
    return m_pPhy->mScu.inVERSION();
}
quint32 dsoEngine::getWpuVer()
{
    return m_pPhy->mWpu.inversion();
}
quint32 dsoEngine::getTpuVer()
{
    return m_pPhy->mTpu.inversion();
}
quint32 dsoEngine::getFcuVer()
{
    return m_pPhy->mZynqFcuRd.inVERSION();
}
quint32 dsoEngine::getSearchVer()
{
    return m_pPhy->mSearch.insearch_version();
}
