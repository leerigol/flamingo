#include "../dsoengine.h"

DsoErr dsoEngine::clearDisplay(bool bClrValid)
{
    m_ConfigId.addSysConfigId();
    m_pPhy->mCcu.inCTRL();
    bool sys_state = m_pPhy->mCcu.getCTRL_sys_stop_state();
    //! 清屏操作：stop-> clear ->还原原来的状态
    m_pPhy->mCcu.setCcuRun( false );

    //! 解决500M存储深度下clear的时候出现错误波形的问题
    m_pPhy->mSpuGp.pulseCTRL_spu_clr( 1, 0, 1 );
    //! 复位trace
    m_pPhy->mZynqFcuWr.outroll_trace_waddr_rst( 1 );
    m_pPhy->mZynqFcuWr.outroll_trace_waddr_rst( 0 );

    //! 复位search
    m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(1);
    m_pPhy->mZynqFcuWr.outsearch_result_wdone_clr(0);

    m_pPhy->mSearch.outsearch_result_send_rst(1);
    m_pPhy->mSearch.outsearch_result_send_rst(0);

    if( m_pViewSession->m_acquire.m_eTimeMode == Acquire_SCAN )
    {
        //! 慢扫描下防止错误波形
        QThread::msleep(40);
    }
    //! wave clear
    m_pPhy->mWpu.syncClear();

    m_pPhy->mScuGp.outCONFIG_ID(getSysConfigId(),-1);
    m_pPhy->mCcu.setCcuRun(!sys_state);
    //! hist invalid
    Q_ASSERT( NULL != m_pHistSession );

    if(bClrValid)
    {
        m_pHistSession->setValid( false );
    }
    return ERR_NONE;
}

DsoErr dsoEngine::clearBgWave()
{
    //! only persist
    m_pPhy->mWpu.outwpu_clear( 3 );

    return ERR_NONE;
}

DsoErr dsoEngine::setPersistTime( int time )
{
    m_pViewSession->m_display.setPersistTime( time );

    applyPersistTime();

    return ERR_NONE;
}

void dsoEngine::applyPersistTime()
{
    int time = m_pViewSession->m_display.getPersistTime();

    //! inifinte
    if ( time < 0 )
    {
        m_pPhy->mWpu.setpersist_mode( 2 );
    }
    else if ( time == 0 )
    {
        m_pPhy->mWpu.setpersist_mode( 0 );
    }
    else
    {
        m_pPhy->mWpu.setpersist_mode( 1 );
        m_pPhy->mWpu.setPersistTime( time );
    }
}

DsoErr dsoEngine::setIntensitity( int perCent )
{
    if ( perCent < 0 || perCent >100 )
    { return ERR_INVALID_INPUT; }

    m_pViewSession->m_display.setIntensity( perCent );

    //! config palette
    if ( m_pViewSession->m_display.getPalette() == Wfm_color )
    {
        m_pPhy->mWpu.setIntensitity( perCent );
    }
    else
    {
        m_pPhy->mWpu.setColorSpec( perCent );
    }

    m_pPhy->mWpu.setcolor_map( 3 );

    return ERR_NONE;
}

DsoErr dsoEngine::setPalette( WfmPalette palette )
{
    m_pViewSession->m_display.setPalette( palette );

    //! config intensity
    setIntensitity( m_pViewSession->m_display.getIntensity() );

    return ERR_NONE;
}

DsoErr dsoEngine::setLineType( WfmLineType type )
{
    m_pViewSession->m_display.setLineType( type );

    if ( type == Wfm_Line )
    {
        m_pPhy->mWpu.setplot_mode( 1 );
    }
    else
    {
        m_pPhy->mWpu.setplot_mode( 0 );
    }

    //! -- noise
    //! average , close noise
    if ( m_pViewSession->m_acquire.getAcquireMode() == Acquire_Average )
    {
        m_pPhy->mWpu.setNoise( 0 );
    }
    else
    {
        m_pPhy->mWpu.setNoise( 2 );
    }

    //! line type
    if ( m_pViewSession->m_display.getLineType() == Wfm_Dot )
    {
        if ( m_pViewSession->m_acquire.m_eTimeMode == Acquire_XY )
        {}
        else
        {
            m_pPhy->mWpu.setNoise( 0 );
        }
    }

    return ERR_NONE;
}

DsoErr dsoEngine::setScreenPersist( bool b )
{
    m_pViewSession->m_display.setScreenPersist( b );
    return ERR_NONE;
}

DsoErr dsoEngine::setWfmColor( Chan /*chId*/, QColor /*color*/ )
{
    return ERR_NONE;
}

DsoErr dsoEngine::setChDispEn( Chan ch, bool en )
{
    m_pViewSession->m_display.setChanEn( ch, en );

    return ERR_NONE;
}

DsoErr dsoEngine::setChDispOrder( int chs, Chan chans[] )
{
    //! view order
    m_pViewSession->m_display.setViewOrder( chs, chans );

    return ERR_NONE;
}

void dsoEngine::applyDispOrder( quint32 saBm )
{
    //! no order
    if ( m_pViewSession->m_display.getViewOrder().size() > 0 )
    {}
    else
    { return; }

    Chan ch;
    quint32 chOrder, mathOrder;

    //! 0--analog, 1--math,2--logic
    QList< int > gpList;

    chOrder = 0;
    mathOrder = 0;

    //! filter the no data channel
    QList<Chan> validList, invalidList, sortList;

    //! bug_fix:当采样模式为双通道时，只打开一个通道，3\4和1\2不能在上两个
    if(toChCnt(m_pViewSession->getSaCHBm()) == 2 &&
            toChCnt(m_pViewSession->getShowCHBm()) == 1)
    {
        QList<Chan> & dispChList = m_pViewSession->m_display.getViewOrder();
        if(dispChList.at(0) >= chan3 && dispChList.at(1) >= chan3)
        {
            Chan temp ;
            temp = dispChList.at(1);
            dispChList[1] = dispChList.at(2);
            dispChList[2] = temp;
        }

        if(dispChList.at(0) <= chan2 && dispChList.at(1) <= chan2)
        {
            Chan temp ;
            temp = dispChList.at(1);
            dispChList[1] = dispChList.at(2);
            dispChList[2] = temp;
        }
    }

    foreach( Chan ch, m_pViewSession->m_display.getViewOrder() )
    {
        //! analog
        if ( ch >= chan1 && ch <= chan4 )
        {
            if ( get_bit( saBm, (ch-chan1) ) )
            { validList.append(ch); }
            else
            { invalidList.append(ch);}
        }
        else if ( (ch >= d0 && ch <= d15) || (ch == la) )
        {
            if ( get_bit( saBm, la_bm_bit) )
            { validList.append(ch); }
            else
            { invalidList.append(ch);}
        }
        else
        { validList.append(ch); }
    }

    //! sort order
    sortList.append( validList );
    sortList.append( invalidList );

    //! cfg the order
    //! from bottom to top
    for ( int i = sortList.size() - 1; i >=0; i-- )
    {
        ch = sortList[i];

        //! analog
        if ( ch >= chan1 && ch <= chan4 )
        {
            chOrder <<= 2;
            unset_attr( quint32, chOrder, 0x3 );
            chOrder |= (ch-chan1);

            //! set top
            if ( gpList.contains(0) )
            { gpList.removeAll(0); }
            gpList.prepend( 0 );
        }
        //! math
        else if ( ch >= m1 && ch <= m4 )
        {
            mathOrder <<= 2;
            unset_attr( quint32, mathOrder, 0x3 );
            mathOrder |= (ch-m1);

            //! set top
            if ( gpList.contains(1) )
            { gpList.removeAll(1); }
            gpList.prepend( 1 );
        }
        //! la
        else if ( ch == la )
        {
            //! set top
            if ( gpList.contains( 2 ) )
            { gpList.removeAll(2); }
            gpList.prepend( 2 );
        }
        else
        {}
    }

    //! config
    m_pPhy->mWpu.setview_layer_analog_order( chOrder );
    m_pPhy->mWpu.setview_layer_math_order( mathOrder );

    //! bottom to top
    quint32 gpOrder = 0;
    Q_ASSERT( gpList.size() == 3 );
    for ( int i = gpList.size() -1; i >= 0; i-- )
    {
        gpOrder <<= 2;
        unset_attr( quint32, gpOrder, 0x3 );
        gpOrder |= gpList.at(i);
    }
    m_pPhy->mWpu.setview_layer_group_order( gpOrder );
}

DsoErr dsoEngine::setScrAttr( EngineCfgScr *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    m_pPhy->mWpu.setScreen( pCfg->mScrId,
                            pCfg->mbEn,
                            pCfg->mHeight,
                            pCfg->mGnd );

    return ERR_NONE;
}
