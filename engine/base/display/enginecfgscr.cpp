#include "enginecfgscr.h"

EngineCfgScr::EngineCfgScr()
{
    mScrId = 0;
    mbEn = false;

    mHeight = 480;
    mGnd = 240;
}

void EngineCfgScr::cfg( int id, bool b, int height, int gnd )
{
    Q_ASSERT( id >= 0 );

    mScrId = id;
    mbEn = b;

    mHeight = height;
    mGnd = gnd;
}
