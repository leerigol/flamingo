#ifndef ENGINECFGSCR_H
#define ENGINECFGSCR_H

#include "../enginecfg.h"

class EngineCfgScr : public EngineCfg
{
public:
    EngineCfgScr();
    void cfg( int id, bool b, int height, int gnd );

public:
    int mScrId;
    bool mbEn;
    int mHeight, mGnd;
};

#endif // ENGINECFGSCR_H
