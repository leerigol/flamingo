#include "../dsoengine.h"


DsoErr dsoEngine::queryProbeInfo(OneWireProbeInfo *info)
{
    Q_ASSERT(info!=NULL);
    return m_pPhy->m1WireProbe[info->getId() - chan1].readProbeInfo(info->info, sizeof(info->info));
}

DsoErr dsoEngine::setProbeInfo(OneWireProbeInfo *info)
{
    Q_ASSERT(info!=NULL);
    info->dataCheckSum = info->getCheckSun(sizeof(info->info)-2);
    return m_pPhy->m1WireProbe[info->getId() - chan1].writeProbeInfo(info->info, sizeof(info->info));
}

DsoErr dsoEngine::setProbeDac(int ch, int dac)
{
    Q_ASSERT( (ch>=chan1) && (chan4>=ch) );
    m_pPhy->mProbe[ch - chan1].setDac(dac);
    //m_pPhy->mExt.setDac(dac);
    return ERR_NONE;
}

quint16 OneWireProbeInfo::getCheckSun(int size)
{
    Q_ASSERT( size<= ((int)sizeof(info)-2) );
    quint16 sum = 0;

    for(int i = 0; i<size; i++)
    {
        sum += info[i];
    }
    return (sum ^ 0xAA55);
}

bool OneWireProbeInfo::checkInfoValid()
{
    QRegExp re("^(?:[0-9a-z]+)$",Qt::CaseInsensitive);
    QString strSn(QByteArray( (char*)sn));
    if( re.indexIn(strSn,0) != (-1) )
    {
        return true;
    }

    return false;
}
