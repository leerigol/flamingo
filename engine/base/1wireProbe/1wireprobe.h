#ifndef ONE_WIRE_PROBE
#define ONE_WIRE_PROBE
#include "../enginecfg.h"

class OneWireProbeInfo
{
public:
    OneWireProbeInfo(Chan id = chan1):mChan(id){}

protected:
    Chan  mChan;
public:
    union
    {
        struct{
        quint8  sn[16];
        quint8  model[8];
        quint16 date[2];    //!28

        quint16 delay;      //!30
        quint16 zeroOff;    //!32
        quint16 offGain[2]; //!36
        quint16 zeroVal;    //!38
        quint16 reserved;   //!40

        quint16 defDelay;
        quint16 defZeroOff;
        quint16 defOffGain[2];
        quint16 defReserved;
        quint16 dataCheckSum;
        };
        quint8 info[52];
    };
public:
    void    setId( Chan id ){mChan = id;}
    Chan    getId(){return mChan;}
    void    clear(){for(int i = 0;i<52;i++){info[i]=0;}}
    quint16 getCheckSun(int size);
    bool    checkInfoValid();
};

#endif // 1WIREPROBE

