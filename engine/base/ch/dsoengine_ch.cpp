
#include "../dsoengine.h"

#define assert_ch_id( id )  Q_ASSERT( id >= E_SERVICE_ID_CH1 && id <= E_SERVICE_ID_CH4 );
#define ch_index( id )      (id - E_SERVICE_ID_CH1)

#define chObj(id)   m_pViewSession->m_ch[ ch_index(id)]

#define selfCh    chObj(id)

DsoErr dsoEngine::chOnOff( ServiceId id, bool b)
{
    assert_ch_id( id );

    selfCh.setShowOnOff( b );

    //! vertical
    m_pPhy->m_ch[ ch_index(id) ].setOnOff(  selfCh.getOnOff() );

    EngineHoriInfo horiInfo;
    horiInfo.mChBm = m_pViewSession->getSaCHBm();
    horiInfo.mChCnt = toChCnt( horiInfo.mChBm );
    emit sigHoriChanged( horiInfo );

    return ERR_NONE;
}
DsoErr dsoEngine::chScale( ServiceId id, int scale )
{
    assert_ch_id( id );

    selfCh.setScale( scale );

//    m_pPhy->m_trace.mGen.setChScale( m_pPhy->m_ch[  ch_index(id) ].getId(),
//                                      scale );
    m_pPhy->m_ch[  ch_index(id) ].setScale( scale );

    //! afe scale
    selfCh.setAfeScale( m_pPhy->m_ch[ ch_index(id)].getAfeScale() );

    return ERR_NONE;
}
DsoErr dsoEngine::chOffset( ServiceId id, int offset )
{
    assert_ch_id( id );
    selfCh.setOffset( offset );

//    m_pPhy->m_trace.mGen.setChOffset( m_pPhy->m_ch[  ch_index(id) ].getId(),
//                                      offset );

    return chPhyOffset( id );
}

DsoErr dsoEngine::chNull( ServiceId id, int null )
{
    assert_ch_id( id );

    selfCh.setNull( null );

    return chPhyOffset( id );
}

DsoErr dsoEngine::chPhyOffset( ServiceId id )
{
    assert_ch_id( id );

    int phyOff;

    int sign = 1;

    sign = ( selfCh.getOffsetSign() );

    phyOff = selfCh.getNull() + sign * selfCh.getOffset();

    return m_pPhy->m_ch[  ch_index(id) ].setOffset( phyOff * sign );
}

DsoErr dsoEngine::chCoupling( ServiceId id, Coupling coup )
{
    assert_ch_id( id );

    selfCh.setCoupling( coup );

    return m_pPhy->m_ch[  ch_index(id) ].setCoupling( coup );
}

DsoErr dsoEngine::chBandwidth( ServiceId id,
                               Bandwidth bw )
{
    assert_ch_id( id );

    selfCh.setBandwidth( bw );

    return ERR_NONE;
}
DsoErr dsoEngine::chImpedance( ServiceId id, Impedance imp )
{
    assert_ch_id( id );

    selfCh.setImpedance( imp );

    int ret  = m_pPhy->m_ch[  ch_index(id) ].setImpedance( imp );

    int chan = id - E_SERVICE_ID_CH1;
    unsigned short irq_mask = m_pPhy->mBoard.readIntEn();
    if (imp == IMP_50)//enable IRQ
    {
        irq_mask = irq_mask | (1 << (3-chan));
    }
    else
    {
        irq_mask = irq_mask & (~(1 << (3-chan)));
    }
    m_pPhy->mBoard.maskInt( irq_mask) ;
    m_pPhy->mBoard.clrInt();

    return ret;
}
DsoErr dsoEngine::chDelay( ServiceId id, int delay )
{
    assert_ch_id( id );

    selfCh.setDelay( delay );

    return ERR_NONE;
}

DsoErr dsoEngine::chInvert( ServiceId id, bool inv )
{
    assert_ch_id( id );

    selfCh.setInvert( inv );

    return ERR_NONE;
}

DsoErr dsoEngine::chProbeRatio( ServiceId id, DsoReal real )
{
    assert_ch_id( id );

    selfCh.setProbeRatio( real );

//    LOG_ENGINE()<<real.mA<<real.mE;

    return ERR_NONE;
}

int dsoEngine::getChAfeScale( ServiceId id )
{
    assert_ch_id( id );

    return m_pPhy->m_ch[  ch_index(id) ].getAfeScale();
}
