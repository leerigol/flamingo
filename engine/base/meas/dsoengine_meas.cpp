
#include "../dsoengine.h"

#define thisMeasCfg m_pPhy->mMeasCfg
#define thisMeasRet m_pPhy->mMeasRet

//! 3 bit
//! 0 -- zoom
//! 1 -- ch
//! 2 -- la
DsoErr dsoEngine::setMeasEn( quint32 bmEn )
{
    m_pViewSession->m_measure.setEnable( bmEn );

    m_pPhy->mCcu.setMODE_measure_en_zoom( get_bit(bmEn,0) );
    m_pPhy->mCcu.setMODE_measure_en_ch( get_bit(bmEn,1) );
    m_pPhy->mCcu.setMODE_measure_en_la( get_bit(bmEn,2) );

    return ERR_NONE;
}

//! s64 t0, s64 tlength
DsoErr dsoEngine::setMeasRange( EnginePara &para )
{
    m_pViewSession->m_measure.setRange( para[0]->s64Val,
                                        para[1]->s64Val );
    return ERR_NONE;
}

DsoErr dsoEngine::setMeasView( HorizontalView view )
{
    m_pViewSession->m_measure.setView( view );
    return ERR_NONE;
}

DsoErr dsoEngine::setMeasOnceReq()
{
    m_pPhy->mCcu.setTRACE_measure_once_req(1);
    return ERR_NONE;
}

DsoErr dsoEngine::setMeasCfgTotalNum(unsigned int num)
{
    thisMeasCfg.outtotal_num(num);
    return ERR_NONE;
}

DsoErr dsoEngine::setMeasCfg( EngineMeasCfg *pCfg )
{
    Q_ASSERT( NULL != pCfg );

    thisMeasCfg.push( pCfg );

    return ERR_NONE;
}

DsoErr dsoEngine::setMeasRdDuringFlag( bool b )
{
    thisMeasCfg.outresult_rdduring_flag_result_rdduring_flag( b );

    return ERR_NONE;
}

DsoErr dsoEngine::setMeasEdgeType( Chan /*ch*/, int /*type*/ )
{
    return ERR_NONE;
}
DsoErr dsoEngine::setMeasPulseType( Chan , int )
{
    return ERR_NONE;
}

DsoErr dsoEngine::queryMeasValue( EngineMeasRet *pRet )
{
    Q_ASSERT( NULL != pRet );

    thisMeasRet.pull( pRet );

    //! get meta
    pRet->mMeta = thisMeasRet.mMeta;

    return ERR_NONE;
}

quint32 dsoEngine::queryMeasResultDone( )
{
    thisMeasRet.inmeas_result_done();
    return thisMeasRet.getmeas_result_done_meas_result_done();
}

DsoErr dsoEngine::queryAdcCoreCH( EngineAdcCoreCH *pCoreCH )
{
    Q_ASSERT( NULL != pCoreCH );

    quint32 bm = m_pViewSession->getSaCHBm();

    toAdcCoreCH( bm, pCoreCH );

    return ERR_NONE;
}

DsoErr dsoEngine::measApply(CDsoSession *pSaView,
                            CDsoSession *pSesView)
{
    Q_ASSERT( m_pViewSession != NULL );

    HorizontalView view = pSesView->m_measure.getView();
    //! enable
    if ( pSesView->m_measure.getEnable() > 0 )
    {
        if( pSesView->m_measure.getView() == horizontal_view_main )
        {
            m_pPhy->mCcu.setMODE_measure_en_zoom( 0 );
        }
        else
        {
            m_pPhy->mCcu.setMODE_measure_en_zoom( 1 );
        }

        m_pPhy->mCcu.setMODE_measure_en_ch( 1 );
        m_pPhy->mCcu.setMODE_measure_en_la( 1 );
    }
    else
    {
        m_pPhy->mCcu.setMeasClose();
        return ERR_NONE;
    }

    //! get t
    qlonglong mt0, mtLength;
    m_pViewSession->m_measure.getRange( mt0, mtLength );

//    //! view by the reg
//    HorizontalView view;
//    if ( m_pPhy->mCcu.getMODE_measure_en_zoom() )
//    { view = horizontal_view_zoom; }
//    else
//    { view = horizontal_view_main; }

    //! map dot
    qint32 offset, length;
    int ret;

    //! chan
    {
        ret = timeRangeCHMapOnView( pSaView, pSesView,
                                 view, mt0, mtLength, &offset, &length );

        if ( ret != 0 )
        {
            m_pPhy->mCcu.setMeasClose();
            return ERR_NONE;
        }

        if ( length <= 0 )
        {
            m_pPhy->mCcu.setMeasClose();
            return ERR_NONE;
        }

        //! set length
        m_pPhy->mSpu.setMEAS_OFFSET_CH( offset );
        m_pPhy->mSpu.setMEAS_LEN_CH( length );
    }

    //! la
    {
        ret = timeRangeLAMapOnView( pSaView, pSesView,
                                  view, mt0, mtLength, &offset, &length );
        if ( ret != 0 )
        {
            m_pPhy->mCcu.setMeasClose();
            return ERR_NONE;
        }

        if ( length <= 0 )
        {
            m_pPhy->mCcu.setMeasClose();
            return ERR_NONE;
        }

        //! set length
        m_pPhy->mSpu.setMEAS_OFFSET_LA( offset );
        m_pPhy->mSpu.setMEAS_LEN_LA( length );
    }

    LOG_DBG()<<m_pPhy->mSpu.getMEAS_OFFSET_CH()<<m_pPhy->mSpu.getMEAS_LEN_CH();

    //! analog
    dso_phy::horiMeta hMeta;
    dsoFract t0;
    t0.set( mt0 );
    hMeta.setMeta( m_pPhy->mSpu.getMEAS_OFFSET_CH(),
                   m_pPhy->mSpu.getMEAS_LEN_CH(),
                   t0,
                   pSesView->m_acquire.mCHFlow.getDotTime() );
    m_pPhy->mMeasRet.mMeta.setHMeta( hMeta );
    m_pPhy->mMeasRet.mMeta.setHMeta( ana, hMeta );

    //! la
    hMeta.setMeta( m_pPhy->mSpu.getMEAS_OFFSET_LA(),
                   m_pPhy->mSpu.getMEAS_LEN_LA(),
                   t0,
                   pSesView->m_acquire.mLaFlow.getDotTime() );
    m_pPhy->mMeasRet.mMeta.setHMeta( la, hMeta );

    //! v meta
    dso_phy::vertMeta vMeta;
    for ( int i = 0; i < 4; i++ )
    {
        vMeta.setMeta( m_pPhy->m_ch[i].getAfeGnd(),
                       m_pPhy->m_ch[i].getAfeScale() );

        m_pPhy->mMeasRet.mMeta.setVMeta( i, vMeta );
    }

    return ERR_NONE;
}

bool dsoEngine::fineTrigPosition(quint32 &finePos, qlonglong transFerDotTime,
                                 qlonglong scale, qlonglong offset, qlonglong interp)
{
    int trigPosOffset = (-offset + 5 * scale) / transFerDotTime;
    int sincFineDly = (trigPosOffset + interp - 1) / interp * interp - trigPosOffset;
    finePos = sincFineDly;
    return true;
}

//! 0 -- no error
int dsoEngine::timeRangeMap(
                                  CSaFlow *pSaFlow,
                                  CSaFlow *pRefFlow,
                                  qlonglong mt0,
                                  qlonglong mtLength,
                                  qint32 *pOffset,
                                  qint32 *pLength )
{
    Q_ASSERT( NULL != pSaFlow );
    Q_ASSERT( NULL != pRefFlow );
    Q_ASSERT( NULL != pOffset );
    Q_ASSERT( NULL != pLength );

    //! to fract
    dsoFract t0, tLength;
    t0.set( mt0 );
    tLength.set( mtLength );

    //! convert meas time to pos
    if( t0 < pRefFlow->getT0() )
    {
        //return -1;
        //qDebug()<<__FILE__<<__LINE__<<"over meas t0";
        t0 = pRefFlow->getT0();
    }

    if( tLength > pRefFlow->getTLength().toLonglong())
    {
        //qDebug()<<__FILE__<<__LINE__<<"over meas length";
        tLength == pRefFlow->getTLength().toLonglong();
    }

    if( tLength <= 0 )
    {
        Q_ASSERT(false);
    }

    dsoFract offset, length;
    offset = ( t0 - pRefFlow->getT0() )/pSaFlow->getDotTime();

    //! check length
    if ( tLength > pRefFlow->getTLength() )
    { tLength = pRefFlow->getTLength(); }
    else
    {}

    length = ( tLength )/pSaFlow->getDotTime();

    //! check offset
    if ( offset > pSaFlow->getDepth() )
    {
        offset = 0;
        length = 0;
    }
    else
    {}

    //! check length
    if ( (offset+length) > pSaFlow->getDepth() )
    {
        length = pSaFlow->getDepth() - offset;
    }
    else
    {}

    //! export
    *pOffset = offset.ceilInt();
    *pLength = length.ceilInt();

    return 0;
}

int dsoEngine::timeRangeCHMapOnView(
                  CDsoSession *pSaView,
                  CDsoSession *pDispView,
                  HorizontalView view,
                  qlonglong mt0,
                  qlonglong mtLength,
                  qint32 *pOffset,
                  qint32 *pLength)
{
    if ( view == horizontal_view_main )
    {
        LOG_DBG()<<"meas main";
        return timeRangeMap( &pSaView->m_acquire.mCHFlow,
                             &pDispView->m_acquire.mMainCHView.getFlow(),
                         mt0,
                         mtLength,
                         pOffset,
                         pLength
                         );
    }
    else
    {
        LOG_DBG()<<"meas zoom";
        return timeRangeMap( &pSaView->m_acquire.mCHFlow,
                             &pDispView->m_acquire.mZoomCHView.getFlow(),
                         mt0,
                         mtLength,
                         pOffset,
                         pLength
                         );
    }
}

int dsoEngine::timeRangeLAMapOnView(
                  CDsoSession *pSaView,
                  CDsoSession *pDispView,
                  HorizontalView view,
                  qlonglong mt0,
                  qlonglong mtLength,
                  qint32 *pOffset,
                  qint32 *pLength)
{
    if ( view == horizontal_view_main )
    {
        return timeRangeMap( &pSaView->m_acquire.mLaFlow,
                             &pDispView->m_acquire.mMainLAView.getFlow(),
                         mt0,
                         mtLength,
                         pOffset,
                         pLength
                         );
    }
    else
    {
        return timeRangeMap( &pSaView->m_acquire.mLaFlow,
                             &pDispView->m_acquire.mZoomLAView.getFlow(),
                         mt0,
                         mtLength,
                         pOffset,
                         pLength
                         );
    }
}

