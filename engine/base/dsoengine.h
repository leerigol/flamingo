#ifndef DSOENGINE_H
#define DSOENGINE_H

#include <QTimer>

#include "../../phy/cdsophy.h"


using namespace dso_phy;

#include "enginecfg.h"
#include "./trigger/enginecfg_trig.h"
#include "./display/enginecfgscr.h"

#include "engine.h"
#include "../cdsosession.h"

#include "./adt/dsoengine_adt.h"
#include "./adt/handlememory.h"

#include "./1wireProbe/1wireprobe.h"

#define LOG_ENGINE()    if ( sysHasArg("-log_engine") )  LOG_DBG()

#define LOG_ADC_CAL()     if ( sysHasArg("-log_adc_cal") )  qDebug()

#define LOG_HORI()      if ( sysHasArg("-log_hori") ) qDebug()
#define LOG_VERT()      if ( sysHasArg("-log_vert") ) qDebug()

class CDsoEngineFactory;

#define PLAY_TIMEOUT    (100)   //! ms

#define POST_PLAY       (25)    //! ms
#define PLAY_TRACE_TIME (25)

//! timer id
#define PLAY_TIMEOUT_ID     1
#define POST_PLAY_ID        2
#define PLAY_TRACE_ID       3

#define TD_TICK         3200    //! 1/312.5M = 3200 ps

#define MIN_AUTO_TRIG_TMO time_ms(1010) //! 1.01s
#define MAX_AUTO_TRIG_TMO time_s(1)

#define TD_TMO  time_ms(1010)           //! 1.01s

#define trace_max_size  1000000

//#include "./search/enginecfg_search.h"
class EngineSearchReport;
class EngineSearchData;

class RTimer
{
public:
    RTimer();

public:
    void start( int tmo );
    void stop();
    bool isActive();

    bool tick( int nms );

private:
    bool mbRunning;
    int mTimeout;   //! ms
};

/*!
 * \brief The dsoEngine class
 * dso theory engine
 */

class CSysConfigId
{
public:
    void addSysConfigId();
    void changeSysConfigId_run();
    void changeSysConfigId_stop();
    int  getConfigId();

private:
    int m_SysConfigId;
};

class dsoEngine : public engine
{
    Q_OBJECT

    DECLARE_MSG_MAP()

protected:
    void onTimeout( int id );

public:
    dsoEngine();

protected:
    static const char *m_version;

    static CDsoSession *m_pViewSession;     //! view session
    static CDsoSession *m_pHistSession;     //! history session

    static CDsoSession mViewSession;
    static CDsoSession mHistSession;

    static dso_phy::CDsoPhy *m_pPhy;        //! phys

    static dsoEngine *m_pRecEngine;         //! engines
    static dsoEngine *m_pPlayEngine;
    static dsoEngine *m_pAutoStopEngine;

    static EngineMsgAttr _noPlay;

    static CSysConfigId m_ConfigId;


    static  bool   m_bTraceRequest;    //! trace使能
    static  bool   m_bTraceRequestOnce;//! trace的最后一帧

    static ControlStatus m_nLastStatus;//added by hxh

public:
    static RTimer * m_playoutTimer;         //! for play timeout
    static RTimer * m_postPlayTimer;
    static RTimer * m_playTraceTimer;
    static void timerTick( int nms = 1 );
    static  bool m_btraceRunning;
protected:
    static RecAction m_RecAction;

public:
    static const char * getVersion();

    static void init();

    static CDsoSession *getViewSesion();
    static CDsoSession *getHistSession();

    static DsoErr setPhy( dso_phy::CDsoPhy *pPhy );
    static dso_phy::CDsoPhy *getPhy();

    //! engines
    static DsoErr setRecEngine( dsoEngine *pEngine );
    static dsoEngine *getRecEngine();

    static DsoErr setPlayEngine( dsoEngine *pEngine );
    static dsoEngine *getPlayEngine();

    static DsoErr setAutoStopEngine( dsoEngine *pEngine );
    static dsoEngine *getAutoStopEngine();

Q_SIGNALS:
    void sigHoriInfo( EngineHoriInfo  );
    void sigHoriChanged( EngineHoriInfo );
    void sigHoriOffsetRoll( qlonglong );
    void sigConRun( bool bRun );
    void sigClearTrace();
    void sigConfigFinished();

public:
    virtual void preDoProc( int /*msg*/, ServiceId /*id*/ );
    virtual void postDoProc( int /*msg*/, ServiceId /*id*/ );

    void flushWCache();

protected:
    DsoErr querySession( EngineSession *pSes );

public:
    //! ch oper
    DsoErr chOnOff( ServiceId id, bool b);
    DsoErr chScale( ServiceId id, int scale );

    DsoErr chOffset( ServiceId id, int offset );
    DsoErr chNull( ServiceId id, int null );
    DsoErr chPhyOffset( ServiceId id );

    DsoErr chCoupling( ServiceId id, Coupling coup );

    DsoErr chBandwidth( ServiceId id, Bandwidth bw );
    DsoErr chImpedance( ServiceId id, Impedance imp );
    DsoErr chDelay( ServiceId id, int delay );
    DsoErr chInvert( ServiceId id, bool inv );
    DsoErr chProbeRatio( ServiceId id, DsoReal real );

    int getChAfeScale( ServiceId id );

    //! la
    bool   getLaProbe();
    DsoErr setLaHLevel( int lev );
    DsoErr setLaLLevel( int lev );

    DsoErr setLaEn( bool b);
    DsoErr setLaOnOff( int onMask );
    DsoErr setLaActive( int actMask );
    DsoErr setLaGroup( int gp, int gpMask );
    DsoErr setLaSize( int size );
    DsoErr setLaPos( int pos[16] );
    DsoErr setLaColor( int hel, quint32 color );

    DsoErr enterCal(bool);

    void laApply();

    //! hori op
    DsoErr setViewMode( HorizontalViewmode view );

    DsoErr setMainCfg( EngineHoriReq *pCfg );

    DsoErr setMainScale ( qlonglong scale );
    DsoErr setMainOffset( qlonglong offset );
    DsoErr setRollOffset( qlonglong rollOffset );

    DsoErr setZoomScale( qlonglong scale );
    DsoErr setZoomOffset( qlonglong offset );

    bool validatePlayScale( qlonglong scale, CDsoSession *pSession );

    //! running info
    DsoErr queryHoriInfo( EngineHoriInfo *pInfo );

    //! meta info in history
    DsoErr queryMetaHoriInfo( EngineHoriInfo *pInfo );
    //! main info
    DsoErr queryHoriInfoMain( EngineHoriInfo *pInfo );
    //! zoom info
    DsoErr queryHoriInfoZoom( EngineHoriInfo *pInfo );

    CHoriPage::HoriMode toHoriMode( int chCnt );

    qlonglong decideHoriMaxOffset(
                                   qlonglong t0,
                                   qlonglong defOff = time_s(1) );

    DsoErr queryHoriAttr( EngineHoriAttr *pAttr );

    int getLiveCHCnt();
    DsoErr setLiveCHCnt( int cnt );     //! 1/2/4

    //! control
    DsoErr setControlAction( ControlAction action );
    ControlStatus getControlStatus();
    DsoErr        waitControlStop();

    //! acq
    DsoErr setAcquireMode( AcquireMode acqMode );
    DsoErr applyAcquireMode( AcquireMode acqMode );

    DsoErr setAverageCount( int count );
    DsoErr setInterplateMode( AcquireInterplate interplate );
    DsoErr setTimeMode( AcquireTimemode timeMode );

    DsoErr setSampleMode( AcquireSamplemode saMode );
    DsoErr setAntiAliasing( bool b );

    DsoErr setMemDepth( int depth );
    int getMemDepth();

    DsoErr setSamplRate( qlonglong sa );
    qlonglong getSampleRate();

    DsoErr setLaMemDepth( int depth );
    int    getLaMemDepth();

    DsoErr    setLaSamplRate( qlonglong sa );
    qlonglong getLaSampleRate();

    DsoErr    setSysRawBand(Bandwidth bw = BW_100M);

    dsoFract getChLength();
    dsoFract getLaLength();

    void snapAcquireState();

    //! view
    DsoErr queryMainViewAttr( EngineViewAttr *pAttr );
    DsoErr queryZoomViewAttr( EngineViewAttr *pAttr );

    DsoErr queryAcqChMainSaFlow(CSaFlow *pAttr);
    DsoErr queryAcqChZoomSaFlow(CSaFlow *pAttr);
    DsoErr queryAcqLaMainSaFlow(CSaFlow *pAttr);
    DsoErr queryAcqLaZoomSaFlow(CSaFlow *pAttr);

    //! display
    DsoErr clearDisplay(bool bClrValid);

    DsoErr clearBgWave();

    //! -1 -- infinite
    //! 0 -- no time
    //! other -- time
    DsoErr setPersistTime( int time );  //! ms
    void   applyPersistTime();
    DsoErr setIntensitity( int perCent );

    DsoErr setPalette( WfmPalette palete );
    DsoErr setLineType( WfmLineType type );

    DsoErr setScreenPersist( bool b );

    DsoErr setWfmColor( Chan chId, QColor color );

    DsoErr setChDispEn( Chan ch, bool en );

    DsoErr setChDispOrder( int chs, Chan chans[] );
    void applyDispOrder( quint32 saBm );

    DsoErr setScrAttr( EngineCfgScr *pCfg );

    //! trigger
    IPhyTpu* getTpu();

    DsoErr setTrigHolder( TriggerHoldObj holder );
    DsoErr setTrigHoldType( TriggerHoldMode holdMode );

    DsoErr setTrigHoldTime( quint64 time );
    DsoErr setTrigHoldNum( quint32 num );
    DsoErr setTrigSweep( TriggerSweep sweep );

    DsoErr applyTrigSweep( TriggerSweep sweep );

    DsoErr setTrigCoupling( Chan ch, Coupling coup );

    DsoErr setTrigForce();
    DsoErr setTrigSingle();

    DsoErr setTrigOutputOn( bool on );
    DsoErr setTrigOutPolarity( bool b );
    DsoErr setTrigOutDelay( quint64 dly );
    DsoErr setTrigOutPulseWidth( quint64 wid );

    DsoErr setTrigLevelMode( TriggerLevelMode mode );

    DsoErr setTrigCHEn( Chan ch, bool bEn );
    DsoErr setDvmCountCHEn(Chan ch, bool bEn);

    DsoErr setTrigCHsRst();

    //! ch a b
    DsoErr setTrigLevelA( EnginePara &para );
    DsoErr setTrigLevelB( EnginePara &para );

    //! by adc
    DsoErr setTrigLevelAAdc( EnginePara &para );
    DsoErr setTrigLevelBAdc( EnginePara &para );

    DsoErr _setTrigLevelA(Chan ch, int l, int h );
    DsoErr _setTrigLevelB( Chan ch, int l, int h);


    DsoErr setTrigLevelARef( Chan ch, int a );
    DsoErr setTrigLevelBRef( Chan ch, int b );

    //! trigger
    DsoErr setExtTrigLevel( int lev, int sens );
    DsoErr setExt5TrigLevel( int lev, int sens );

    //! edge
    void preSetTrigEdge();

    DsoErr setTrigEdgeCfg( TrigEdgeCfg *pCfg );
    void   applyTrigEdge( TrigEdgeCfg &cfg, IPhyTpu *pTpu );

    //! pulse
    DsoErr setTrigPulseCfg( TrigPulseCfg *pCfg );
    void    applyTrigPulse( TrigPulseCfg &cfg, IPhyTpu *pTpu );

    //! slope
    void enterTrigSlope();
    void exitTrigSlope();

    DsoErr setTrigSlopeCfg( TrigSlopeCfg *pCfg );
    void applyTrigSlopeCfg( TrigSlopeCfg &cfg, IPhyTpu *pTpu );

    //! runt
    void enterTrigRunt();
    void exitTrigRunt();

    DsoErr setTrigRuntCfg( TrigRuntCfg *pCfg );
    void applyTrigRunt( TrigRuntCfg &cfg, IPhyTpu *pTpu );

    //! over
    void enterTrigOver();
    void exitTrigOver();

    DsoErr setTrigOverCfg( TrigOverCfg *pCfg );
    void applyTrigOverCfg( TrigOverCfg &cfg, IPhyTpu *pTpu );

    //! window
    void enterTrigWindow();
    void exitTrigWindow();

    DsoErr setTrigWindowCfg( TrigWindowCfg *pCfg );
    void applyTrigWindowCfg( TrigWindowCfg &cfg, IPhyTpu *pTpu );

    //! timeout
    void enterTrigTimeout();
    void exitTrigTimeout();

    DsoErr setTrigTimeoutCfg( TrigTimeoutCfg *pCfg );
    void applyTrigTimeoutCfg( TrigTimeoutCfg &cfg, IPhyTpu *pTpu );

    //! setup hold
    void enterTrigSH();
    void exitTrigSH();

    DsoErr setTrigSHCfg( TrigSHCfg *pCfg );
    void applyTrigSH( TrigSHCfg &cfg, IPhyTpu *pTpu );

    //! delay
    void enterTrigDelay();
    void exitTrigDelay();

    DsoErr setTrigDelayCfg( TrigDelayCfg *pCfg );
    void applyTrigDelay( TrigDelayCfg &cfg, IPhyTpu *pTpu );

    //! nedge
    void enterTrigNEdge();
    void exitTrigNEdge();

    DsoErr setTrigNEdgeCfg( TrigNEdgeCfg *pCfg );
    void applyTrigNEdgeCfg( TrigNEdgeCfg &cfg, IPhyTpu *pTpu );

    //! pattern
    void enterTrigPattern();
    void exitTrigPattern();

    DsoErr setTrigPatternCfg( TrigPatternCfg *pCfg );
    void applyTrigPatternCfg( TrigPatternCfg &cfg, IPhyTpu *pTpu );
    int selectPatternEdge( TrigPatternCfg &cfg, int *pFirstEdge );

    //! video
    void enterTrigVideo();
    void exitTrigVideo();

    DsoErr setTrigVideoCfg( TrigVideoCfg *pCfg );
    void applyTrigVideoCfg( TrigVideoCfg &cfg, IPhyTpu *pTpu );

    //! duration
    void enterTrigDuration();
    void exitTrigDuration();

    DsoErr setTrigDurationCfg( TrigDurationCfg *pCfg );
    void applyTrigDurationCfg( TrigDurationCfg &cfg, IPhyTpu *pTpu );

    //!uart
    DsoErr setTrigUartCfg( TrigRS232Cfg *pCfg );
    void applyTrigUartCfg( TrigRS232Cfg &cfg, IPhyTpu *pTpu );

    //!iic
    DsoErr setTrigIicCfg(TrigIICcfg *pCfg);
    void applyTrigIicCfg(TrigIICcfg &cfg, IPhyTpu *pTpu);

    //!spi
    DsoErr setTrigSpiCfg(TrigSpiCfg *pCfg);
    void applyTrigSpiCfg(TrigSpiCfg &cfg, IPhyTpu *pTpu);

    //!AB
    DsoErr setTrigABCfg(  TrigABCfg *pCfg   );
    void applyTrigABCfg(  TrigABCfg &cfg, IPhyTpu *pTpu    );
    void aEventEdgeApply( TrigEdgeCfg &cfg, IPhyTpu *pTpu  );
    void aEventPulseApply(TrigPulseCfg &cfg, IPhyTpu *pTpu );
    void bEventEdgeApply( TrigEdgeCfg &cfg, IPhyTpu *pTpu  );

    //!can
    DsoErr setTrigCanCfg(TrigCanCfg *pCfg);
    void applyTrigCanCfg(TrigCanCfg &cfg, IPhyTpu *pTpu);

    //!Flexray
    DsoErr setTrigFlexrayCfg(TrigFlexrayCfg *pCfg);
    void applyTrigFlexrayCfg(TrigFlexrayCfg &cfg, IPhyTpu *pTpu);

    //!Std1553b
    DsoErr setTrigStd1553bCfg(TrigStd1553bCfg *pCfg);
    void applyTrigStd1553bCfg(TrigStd1553bCfg &cfg, IPhyTpu *pTpu);

    //!Lin
    DsoErr setTrigLinCfg(TrigLinCfg *pCfg);
    void applyTrigLinCfg(TrigLinCfg &cfg, IPhyTpu *pTpu);

    //!I2S
    DsoErr setTrigI2SCfg(TrigI2SCfg *pCfg);
    void applyTrigI2SCfg(TrigI2SCfg &cfg, IPhyTpu *pTpu);

    //!area
    DsoErr setTrigZoneEn(bool en);
    DsoErr setTrigZoneCfg(TrigZoneCfg *pCfg);

    void applyTrigZone(CDsoSession *pSesView);
    void applyTrigZoneCfg(TrigZoneCfg &cfg);
    bool setTrigZoneX(void *pZoneTabReg,
                      TrigZoneCfg::TZoneDataStru item);

    //! ---search
    IPhySearch *getSearch();

    bool   inSearchData(   EngineSearchData *pCfg);

    DsoErr reqSearchData(  EngineSearchData *pCfg);
    DsoErr getSearchData(  EngineSearchData *pCfg);
    DsoErr clearSearchData(EngineSearchData *pCfg);

    //! ch a b
    DsoErr setSearchEn( quint32 bm );

    DsoErr applySearchCfg();

    DsoErr setSearchLevelA( EnginePara &para );
    DsoErr setSearchLevelB( EnginePara &para );

    DsoErr setSearchEdgeCfg( TrigEdgeCfg *pCfg );
    DsoErr setSearchPulseCfg( TrigPulseCfg *pCfg );
    DsoErr setSearchSlopeCfg( TrigSlopeCfg *pCfg );
    DsoErr setSearchRuntCfg( TrigRuntCfg *pCfg );

    DsoErr setSearchOverCfg( TrigOverCfg *pCfg );
    DsoErr setSearchWindowCfg( TrigWindowCfg *pCfg );
    DsoErr setSearchTimeoutCfg( TrigTimeoutCfg *pCfg );
    DsoErr setSearchSHCfg( TrigSHCfg *pCfg );

    DsoErr setSearchDelayCfg( TrigDelayCfg *pCfg );
    DsoErr setSearchNEdgeCfg( TrigNEdgeCfg *pCfg );
    DsoErr setSearchPatternCfg( TrigPatternCfg *pCfg );
    DsoErr setSearchVideoCfg( TrigVideoCfg *pCfg );

    DsoErr setSearchDurationCfg( TrigDurationCfg *pCfg );
    DsoErr setSearchUartCfg( TrigRS232Cfg *pCfg );
    DsoErr setSearchIicCfg(TrigIICcfg *pCfg);
    DsoErr setSearchSpiCfg(TrigSpiCfg *pCfg);

    DsoErr setSearchABCfg(  TrigABCfg *pCfg   );
    DsoErr setSearchCanCfg(TrigCanCfg *pCfg);
    DsoErr setSearchFlexrayCfg(TrigFlexrayCfg *pCfg);
    DsoErr setSearchStd1553bCfg(TrigStd1553bCfg *pCfg);

    DsoErr setSearchLinCfg(TrigLinCfg *pCfg);
    DsoErr setSearchI2SCfg(TrigI2SCfg *pCfg);

    //! counter
    quint32 toCounterSource( Chan ch );
    quint32 toCounterLaSel( Chan la );

    DsoErr setCounterEn( bool b );
    DsoErr setCounterSource( Chan src );
    DsoErr setCounterRst();

    DsoErr setCounterCfg( EngineCounter *pEngineCfg );
    DsoErr queryCounterValue( EngineCounterValue *pEngineCntVal );

    //! dvm
    DsoErr setDvmSource( Chan src );
    DsoErr setDvmGateTime( int time );  //! us
    DsoErr queryDvmValue( EngineDvmValue *pEngineCntVal );

    //! meas
    DsoErr setMeasRange( EnginePara &para );
    DsoErr setMeasView( HorizontalView view );
    DsoErr setMeasOnceReq();

    DsoErr setMeasCfgTotalNum(unsigned int num);
    DsoErr setMeasEn( quint32 bmEn  );

    DsoErr setMeasCfg( EngineMeasCfg *pCfg );
    DsoErr setMeasRdDuringFlag( bool );

    DsoErr setMeasEdgeType(Chan, int);
    DsoErr setMeasPulseType(Chan, int);

    DsoErr queryMeasValue( EngineMeasRet *pRet );
    quint32 queryMeasResultDone( );

    DsoErr queryAdcCoreCH( EngineAdcCoreCH *pCoreCH );

    //! rec && play
    DsoErr setRecordDone(bool b);
    DsoErr setRecordAction( RecAction act );
    DsoErr setRecordInterval( qlonglong ps );
    DsoErr setRecordFrames( int maxFrames );

    DsoErr setPlayAction( PlayAction act );
    DsoErr setPlayInterval( qlonglong ps );
    DsoErr setPlayRange( int a, int b );
    DsoErr setPlayNow( int now );

    DsoErr setPlayDirection( PlayDirection dir );
    DsoErr setPlayLoop( bool loop );
    DsoErr setStepPlay( int frame );

    int getRecordCapacity();
    int getFrameNow();
    quint64 getFrameTag( int frameId );
    bool getRecPlayIdle();

    //! mask
    void   applyMask(CDsoSession *pSesView);
    DsoErr setMaskOn( bool b );
    DsoErr setMaskSource( qint32 chbm );

    DsoErr setMaskOutAction( MaskActionEvent outputAction );
    DsoErr setMaskOutput( bool bON );
    DsoErr setMaskOutPN( bool bPN );
    DsoErr setMaskOutTime( qlonglong timeps );
    DsoErr setMaskOutDelay(qlonglong);

    DsoErr setMaskActionAction( MaskActionAction action );

    DsoErr setMaskRule( EngineMaskRule *pRule );
    DsoErr setMaskRst( );

    EngineMaskRule *newRule( );
    void fillRull( EngineMaskRule *pRule, EngineMaskRule *pInRule );
    void deleteRule( EngineMaskRule *pRule );

    DsoErr queryMaskReport( EngineMaskReport *pValue );

    //! utility
    DsoErr tickBeep();
    DsoErr shortBeep();
    DsoErr startBeep();
    DsoErr stopBeep();
    DsoErr intervalBeep();

    DsoErr setLcdLight( int light );
    DsoErr setFanSpeed( int speed );

    DsoErr setProbeEn( bool en );
    DsoErr setTrigOutSel( int sel );

    quint16 getCpldVer();
    quint16 getMBVer();

    DsoErr writeReg( EnginePara &para );
    quint32 readReg( EnginePara &para );

    //! decode
    DsoErr setDecodeThre( EnginePara &para );
    DsoErr setDecodeThreL( EnginePara &para );

    //! fpga vers
    quint32 getSpuVer();
    quint32 getScuVer();
    quint32 getWpuVer();
    quint32 getTpuVer();
    quint32 getFcuVer();
    quint32 getSearchVer();

    //! board cfg
    DsoErr rstBoardCtp();

    DsoErr clrBoardInt();
    quint16 getBoardIntStat();
    quint16 getBoardIntEn();
    quint16 getBoardIntMask();

    DsoErr testBoardWrite( quint16 val );
    quint16 testBoardRead();

    //! led
    DsoErr setLedOn( int led );
    DsoErr setLedOff( int led );

    DsoErr setLedOp( int led, bool b );

    DsoErr setLedStatus( int leds );
    int getLedStatus();

    DsoErr setPowerSwitch( bool b );
    bool getPowerSwitch();
    DsoErr setPowerRestart();
    DsoErr setPowerShutdown();

    //! attach ch cal data
    DsoErr setCHCal( Chan ch, void *pCal );
    DsoErr setCHHizTrim( Chan ch, void *pTrim );

    DsoErr setAdcCoreGo( int adcId, void *pGo );
    DsoErr setAdcCorePhase( int adcId, void *pPhase );

    DsoErr setExtCal( int id, void *pCal );
    DsoErr setLaCal( int id, void *pCal );
    DsoErr setProbeCal( int id, void *pCal );

    DsoErr setDgOffsetCal( int id, void *pCal );
    DsoErr setDgRefCal( int id, void *pCal );

    //! tune clock
    DsoErr setTuneClock( int clock );

    //! pll 1.1g
    DsoErr set1_1gOut( bool b);

    //! adc info
    quint32 getAdcRomId( int id );
    DsoErr getAdcUuid( unsigned char *outBuf );

    //! afe info
    quint32 getAfeVer( int id );

    //! trace
    void setTrace_vAttr( Chan chId, DsoWfm *pWfm, CDsoSession *pSes );

    static void setTrace_probe(Chan chId, DsoWfm *pWfm);

    void requestTrim( traceRequest &req,
                      CDsoSession *pSes );
    void requestWfmTrim( ChanWfmMap &wfmMap,
                         Chan chFrom,
                         Chan chEnd,
                         CDsoSession *pSes,
                         bool bViewValid = true );

    void requestAttachAddr( traceRequest &req,
                            CDsoSession *pSes );
    void requestWfmAttachAddr( ChanWfmMap &wfmMap,
                               CDsoSession *pSes );
//    quint32 getWfmPhyAddr( Chan ch,
//                           CDsoSession *pSes );

    void requestSetAttr( traceRequest &req,
                         CDsoSession *pSes );

    void requestSetAttr_main( traceRequest &req,
                         CDsoSession *pSes );
    void requestSetAttr_zoom( traceRequest &req,
                         CDsoSession *pSes );

    DsoErr requestData( traceRequest &req,
                        int tmo=2000,
                        CDsoSession *pSes = NULL );

    virtual DsoErr requestTrace( traceRequest &req,
                                 int tmo=2000 );

    qint32 getPeak( int chId );

    static int  getSysConfigId();
//    static void addSysConfigId();
//    static void sysConfigId_run();
//    static void sysConfigId_stop();

    static bool getTraceRequestOnOff();
    static bool getTraceOnceOnOff();

    //! memory
    handleMemory* openMemory( Chan chId );

    void exportInit(handleMemory *pHandle);
    void exportInit(handleMemory *pHandle, frameAttr &attr);
    void importInit(frameAttr &attr);

    int readMemory( handleMemory* handle, void *pOut, int from, int len );
    int writeMemory(handleMemory* handle, void *pIn,  int from, int len, bool eof);

    int readMisson( handleMemory *handle, void *pOut, int len );
    int writeMisson(handleMemory *handle, void *pIn,  int len );

    void exportEnd();
    void importEnd();

    void importPlay(frameAttr &attr);

    int closeMemory( handleMemory* handle );

    //!1wire probe
    DsoErr queryProbeInfo(     OneWireProbeInfo *info);
    DsoErr setProbeInfo(       OneWireProbeInfo *info);
    DsoErr setProbeDac(        int ch, int dac);

protected:
    DsoErr playback(CDsoSession *pSesHist,
                    CDsoSession *pSesView,
                    bool bPlayWpu = true,
                    bool bPlayTrace = true);

    bool   needPlay( CDsoSession *pSesHist,
                     CDsoSession *pSesView );

    bool   needCheckParam( CDsoSession *pSesHist,
                           CDsoSession *pSesView );

    DsoErr chPlay( CDsoSession *pSesHist,
                     CDsoSession *pSesView );
    DsoErr laPlay(CDsoSession *pSesHist,
                     CDsoSession *
                    );

    DsoErr horiPlay( CDsoSession *pSesHist,
                     CDsoSession *pSesView );

    DsoErr chMainDelayPlay(  CDsoSession *pSesHist,
                             CDsoSession *pSesView  );
    DsoErr chZoomDelayPlay(  CDsoSession *pSesHist,
                             CDsoSession *pSesView);

    void horiViewTune( CDsoSession *pSesView );

    DsoErr horiMainPlay( CDsoSession *pSesHist,
                         CDsoSession *pSesView );

    DsoErr horiZoomPlay( CDsoSession *pSesHist,
                         CDsoSession *pSesView );

    DsoErr calcHoriPara( CDsoSession *pSesHist,
                         CDsoSession *pSesView );

    DsoErr extraPlay( CDsoSession *pSesHist,
                      CDsoSession *pSesView );

    DsoErr doPlay(bool bPlayWpu = true,bool bPlayTrace = true);

    //! search
    DsoErr searchApply(CDsoSession *pSesView);

    //! measure
    DsoErr measApply(CDsoSession *pSaView, CDsoSession *pSesView);

    bool fineTrigPosition(quint32 &finePos, qlonglong transFerDotTime,
                                         qlonglong scale, qlonglong offset, qlonglong interp);

    int timeRangeMap( CSaFlow *pSaFlow,
                      CSaFlow *pRefFlow,
                      qlonglong t0,
                      qlonglong tSpan,
                      qint32 *pOffset,
                      qint32 *pLength );

    int timeRangeCHMapOnView(CDsoSession *pSaView,
                      CDsoSession *pDispView,
                      HorizontalView view,
                      qlonglong t0,
                      qlonglong tSpan,
                      qint32 *pOffset,
                      qint32 *pLength);

    int timeRangeLAMapOnView(CDsoSession *pSaView,
                      CDsoSession *pDispView,
                      HorizontalView view,
                      qlonglong t0,
                      qlonglong tSpan,
                      qint32 *pOffset,
                      qint32 *pLength );
protected:
    //! xpu op.
    //! wpu
    void wpuInterleave( quint32 chBm );
    void logWpuScuStatus();

protected:
    IPhyTpu::eCmpSrc toaCmpSrc( Chan ch );
    IPhyTpu::eCmpSrc toaCmpSrc_a( Chan ch );
    IPhyTpu::eCmpSrc toaCmpSrc_b( Chan ch );

    IPhyTpu::eCmpSrc toaCmpSrc( IPhyTpu::eChSeq ch );

    IPhyTpu::eCmpMode toCmpMode( EdgeSlope slp );

    IPhyTpu::eWidthCmp toWidthCmp( EMoreThan mt );
    IPhyTpu::eChSeq toChSeq( Chan ch );

    IPhyTpu::eLogicSel toLogicSel( TriggerLogicOperator op,
                                   TriggerPattern patt);
    IPhyTpu::eLogicOp toLogicOp( TriggerLogicOperator op );

    IPhyTpu::eIirType toIir( Coupling coup );


    int chBmCnt( quint32 bm );
    int toChCnt( quint32 bm );

    dso_phy::IPhyADC::eAdcInterleaveMode toAdcMod( quint32 bm );
    void toAdcCoreCH( quint32 bm,
                      EngineAdcCoreCH *pChCore );

    int toAdc( Chan ch, int lev );  //! in uv unit

    int toAdcGroups( quint32 bm,
                     AdcGroups *groups );

    //! 0--find success
    int selectFilter( int chCnt,
                      Bandwidth bw,
                      EngineFilter *pFilter );

    void engineErrMsgHandler(QtMsgType type, const QMessageLogContext &context,
                             const QString &msg);

    friend class CDsoEngineFactory;
};



#endif // DSOENGINE_H
