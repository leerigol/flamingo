//#define _DEBUG 1
#include "patchrequest.h"
#include "../../../include/dsodbg.h"
#include "../include/dsocfg.h"

CReadSplit::CReadSplit()
{
      mPatchCount  = 0;
      mPatchSize   = 0;

      mPrePadSize  = 0;
      mPostSize    = 0;

      mPostPatchSize   = 0;
      m_pPostPatch     = NULL;
}

CReadSplit::~CReadSplit()
{
    if ( NULL != m_pPostPatch )
    { delete []m_pPostPatch; }
}


int CReadSplit::split(int len,
                      int subPatchCount,
                      int subPatchSize)
{
    //! input
    int trimSize   = len;

    Q_ASSERT( subPatchCount > 0 && subPatchSize > 0 );
    mPatchSize = subPatchCount * subPatchSize;

    //! patch count
    mPatchCount = len / mPatchSize;
    trimSize    = trimSize - mPatchSize * mPatchCount;

    mPostSize       = trimSize;
    mPostPatchSize  = ((trimSize + subPatchSize - 1) / subPatchSize) * subPatchSize;

    mPrePadSize = 0;

    //! PostPatch memory
    if ( NULL != m_pPostPatch )
    { delete []m_pPostPatch; }

    m_pPostPatch = new quint8[ mPostPatchSize ];
    if ( NULL == m_pPostPatch )
    { return -1; }

    LOG_DBG()<<"mPatchCount:"<<mPatchCount<<"mPatchSize:"<<mPatchSize
             <<"mPostPatch:"<<mPostPatchSize<<"mPostSize:"<<mPostSize;

    return 0;
}

CWriteSplit::CWriteSplit()
{
     mPatchCount = 0;
     mPatchSize  = 0;
     mPostSize   = 0;
     mPrePadSize = 0;
     mPreSize    = 0;
     mPadSize    = 0;
     mOffset     = 0;

     m_pPrePadPatch = NULL;
}

CWriteSplit::~CWriteSplit()
{
    if ( NULL != m_pPrePadPatch )
    { delete []m_pPrePadPatch; }
}

int CWriteSplit::split(void *pIn,
                       void *pTempIn,
                       int   from,
                       int   len,
                       int   subPatchCount,
                       int   subPatchSize)
{
    Q_ASSERT(NULL != pIn);
    Q_ASSERT(NULL != pTempIn);
    Q_ASSERT( subPatchCount > 0 && subPatchSize > 0 );

    int mPreSize;
    int mPadSize;

    //! input
    int trimSize   = len;

    mPatchSize  = subPatchCount * subPatchSize;

    mPreSize    = from % mPatchSize;
    mPrePadSize = (mPreSize + subPatchSize -1) / subPatchSize * subPatchSize;
    mPadSize    =  qMin(trimSize, mPrePadSize - mPreSize);

    trimSize    = trimSize - mPadSize;

    mPatchCount = trimSize / mPatchSize;

    mPostSize   = trimSize - mPatchCount * mPatchSize;

    Q_ASSERT( mPreSize    >= 0 );
    Q_ASSERT( mPrePadSize >= 0 );
    Q_ASSERT( mPadSize    >= 0 );

    //! PostPatch memory
    if ( NULL != m_pPrePadPatch )
    { delete []m_pPrePadPatch; }

    m_pPrePadPatch = new quint8[ mPrePadSize];
    if ( NULL == m_pPrePadPatch )
    { return -1; }

    int offset = 0;
    memcpy( m_pPrePadPatch + offset, pTempIn, mPreSize    * sizeof(DsoPoint) );

    offset = offset + mPreSize;
    memcpy( m_pPrePadPatch + offset, pIn,     mPadSize    * sizeof(DsoPoint) );

    offset = mPadSize + mPatchSize * mPatchCount;
    memcpy( pTempIn, (DsoPoint*)pIn + offset, mPostSize * sizeof(DsoPoint) );

    mPostSize = (mPostSize + subPatchSize - 1) / subPatchSize * subPatchSize;

    LOG_DBG()<<"mPatchCount:"<<mPatchCount<<"mPatchSize:"<<mPatchSize
             <<"mPrePadSize:"<<mPrePadSize<<"mPreSize:"<<mPreSize<<"mPadSize:"<<mPadSize
             <<"mPostSize:"<<mPostSize;

    return 0;
}
