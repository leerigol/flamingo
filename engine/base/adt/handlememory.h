#ifndef HANDLEMEMORY
#define HANDLEMEMORY

#include <QtCore>
#include "../../../include/dsotype.h"

#include "../../../phy/trace/tracerequest.h"

using namespace DsoType;

struct frameAttr
{
    quint32 mCh;
    quint32 mFrameStart;
    quint32 mFrameId;
    quint32 mFrameEnd;
    quint32 mFrameLen;

    quint64 mTimeTag;
    quint32 mFinePos;
    quint32 mImportRatio;

    quint32  saChLen;
    quint32  saLaLen;
    bool     saValid;
};

class dsoEngine;
//! base class
class handleMemory
{
public:
    handleMemory( Chan ch );
    virtual ~handleMemory();
public:
    void setChan( Chan ch );
    Chan getChan();

    void setSize( int fullSize);
    int  getFullSize();

    void setChCnt( int chCnt );
    int  getChCnt();

    void setValid( bool b = true );
    bool getValid();

    void setEngine( dsoEngine *pEngine );
    dsoEngine *getEngine();

public:
    virtual void setOffset( int offs );
    virtual int  doRead( void *pOut, int len );
    virtual int  doWrite( void *pIn, int len );
    virtual int  attrRead(  frameAttr *pAttr );
    virtual int  attrWrite( frameAttr *pAttr );

    virtual void postSet();

public: //!封装trace操作
    virtual bool reqsTrace(   int mainLen, int txLen,         int timeOut = 200);
    virtual bool reqsDeinter( int len,     int timeOut = 200, int srcType = 0);
    virtual int  getOutData(  void *pOut,  int len);

public:
    dso_phy::traceRequest::traceSource mReqSrc; //! by chan

protected:
    Chan mChan;

    int mFullSize;

    int mOffset;

    int mChCnt;

    bool mbValid;

    dsoEngine *m_pEngine;
};

//! ch1/2/3/4
class handleMemoryCHx : public handleMemory
{
public:
    handleMemoryCHx( Chan ch );
public:
    int  doRead( void *pOut, int len );
    void setOffset( int offs );
    void postSet();
};

//! d0/d1/d2.../d15
class handleMemoryDx : public handleMemory
{
public:
    handleMemoryDx( Chan ch );
public:
    int  doRead( void *pOut, int len );
    void setOffset( int offs );
    void postSet();
};

//! d15-d0
class handleMemoryLa : public handleMemory
{
public:
    handleMemoryLa( Chan ch );
public:
    int  doRead( void *pOut, int len );
    void setOffset( int offs );
    void postSet();
    int  getOutData( void *pOut,  int len);
};

#endif // HANDLEMEMORY

