#ifndef DSOENGINE_ADT
#define DSOENGINE_ADT

#include <QtCore>
#include "../../../include/dsotype.h"
#include "../../../phy/cdsophy.h" //! phy types

using namespace DsoType;

//! datas
struct BandwidthFilter
{
    Bandwidth bw;
    short *m_pFilter;
};

class AdcGroup
{
public:
    AdcGroup();

    void setGroup( int master,
                   int slave1,
                   int slave2,
                   int slave3 );

    void setGroup( int master,
                   int slave1 );

    void setGroup( int master );

public:
    int mMaster;

    int mSlaveCnt;
    int mSlaves[3];

    quint32 mBm;
};

class AdcGroups
{
public:
    AdcGroups();
    void rst();

    void appendGroup( int master,
                      int slave1,
                      int slave2,
                      int slave3 );
    void appendGroup( int master,
                      int slave1 );

    void appendGroup( int master );
public:
    int mGroupCnt;
    AdcGroup mGroups[4];
};

class ChTimeDelay
{
public:
    ChTimeDelay();
    void setDelay( qlonglong delay );

    void slove( qlonglong dotTime,
                qlonglong pixTime );

    void align( qint32 baseDelay );
protected:
    void slovePre( qlonglong dotTime,
                   qlonglong pixTime );
    void slovePost( qlonglong dotTime,
                    qlonglong pixTime );

public:
    qlonglong mDelay;
    qlonglong mDotTime;
    qlonglong mPixTime;

    qint32 mPreSa;
    qint32 mPostSa;
    qint32 mCoarseDelay;
    qint32 mFineDelay;
};

//! la size info
//! head + line + edge + line + foot = pixSize
//! line + edge + line = body
struct _LaSizeInfo
{
    int mPixSize;
    int mHead;
    int mLine;      //! H or L
    int mFoot;
};

class LaSizeInfo : public _LaSizeInfo
{
public:
    static LaSizeInfo getSizeInfo( int pixSize );
public:
    LaSizeInfo();
    LaSizeInfo &operator=( _LaSizeInfo & );

    void setSize( int pix, int line = 1, int head=0, int foot=0 );
    int getBody();
};

struct LaColorInfo
{
    quint32 mNormalHighColor, mNormalLowColor;
    quint32 mActiveColor;
    quint32 mEdgeColor;

    LaColorInfo();
    void setColor( quint32 color );

    void setColor( quint32 hColor, quint32 eColor, quint32 lColor );

    quint32 getHighColor( bool bAct );
    quint32 getLowColor( bool bAct );
    quint32 getEdgeColor( bool bAct );
};

//! la dx
class laDx
{
public:
    laDx();

public:
    void setId( int id );
    void set( bool en, bool act, int pos );
    void setFromSpan( int from, int span );

    void exportLines(
                      QList<dso_phy::laLine*> &lines,
                      LaSizeInfo &sizeInfo,
                      LaColorInfo &colorInfo,
                      bool bZoom );
public:
    int mId;
    bool mbEn;

    bool mbActive;      //! color

    int mPos;           //! 0~cnt,eg, 0~15,0~32
    int mFrom, mSpan;
};

//! for each dx
class groupLa
{
public:
    static void initLaLines( QList<dso_phy::laLine*> &lines );
    static void deInitLaLines( QList<dso_phy::laLine*> &lines );
    static void fillNullLines( int height, QList<dso_phy::laLine*> &lines );
    static void sortLaLine( QList<dso_phy::laLine*> &lines );

    static int getAreas( LaScale size );

    static void logLaLine( QList<dso_phy::laLine*> &lines,
                           const QString &fileName );

public:
    groupLa();
protected:
    void setSizeInfo( const LaSizeInfo &info );
    void setSize( LaScale size );

public:
    void setDx( int dx,
                bool en,
                bool active,
                int pos );

    void setDx( int onOffMask,
                int activeMask,
                int pos[16] );

    void setDxColor( int dx,
                     quint32 color );
    void setColor( quint32 hColor, quint32 eColor, quint32 lColor );
public:
    void split( int base, int height, bool invert );
    void split( int base, int height,
                LaScale size,
                bool invert );

    int getDxGnd( int id, bool bInv = false );

public:
    void exportLines( QList<dso_phy::laLine*> &lines,
                      bool bZoom );

public:
    LaSizeInfo mSizeInfo;

    int mAreas;     //! 8,16,32 --> size
    laDx mDxs[16];

    LaColorInfo mDxColors[16];
};



#endif // DSOENGINE_ADT

