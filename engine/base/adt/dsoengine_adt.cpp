#include "./dsoengine_adt.h"
#include "../arith/rsplit.h"

#include "../phy/wpu/iphywpu.h"

AdcGroup::AdcGroup()
{
    mMaster = 0;
    mSlaveCnt = 0;

    mBm = 0;
}

void AdcGroup::setGroup( int master,
               int slave1,
               int slave2,
               int slave3 )
{
    mMaster = master;

    mSlaveCnt = 3;
    mSlaves[0] = slave1;
    mSlaves[1] = slave2;
    mSlaves[2] = slave3;

    set_bit( mBm, master );
    set_bit( mBm, slave1 );
    set_bit( mBm, slave2 );
    set_bit( mBm, slave3 );
}

void AdcGroup::setGroup( int master,
               int slave1 )
{
    mMaster = master;

    mSlaveCnt = 1;
    mSlaves[0] = slave1;

    set_bit( mBm, master );
    set_bit( mBm, slave1 );
}

void AdcGroup::setGroup( int master )
{
    mMaster = master;

    mSlaveCnt = 0;

    set_bit( mBm, master );
}

AdcGroups::AdcGroups()
{
    mGroupCnt = 0;
}

void AdcGroups::rst()
{
    mGroupCnt = 0;
}

void AdcGroups::appendGroup( int master,
                  int slave1,
                  int slave2,
                  int slave3 )
{
    Q_ASSERT( mGroupCnt < array_count(mGroups) );

    mGroups[mGroupCnt++].setGroup( master,
                                   slave1,
                                   slave2,
                                   slave3 );

}
void AdcGroups::appendGroup( int master,
                  int slave1 )
{
    Q_ASSERT( mGroupCnt < array_count(mGroups) );

    mGroups[mGroupCnt++].setGroup( master,
                                   slave1 );
}

void AdcGroups::appendGroup( int master )
{
    Q_ASSERT( mGroupCnt < array_count(mGroups) );

    mGroups[mGroupCnt++].setGroup( master );
}

ChTimeDelay::ChTimeDelay()
{
    mDelay = 0;
    mDotTime = 0;
    mPixTime = 0;

    mPreSa = 0;
    mPostSa = 0;
    mCoarseDelay = 0;
    mFineDelay = 0;
}

void ChTimeDelay::setDelay( qlonglong delay )
{
    mDelay = delay;
}

void ChTimeDelay::slove( qlonglong dotTime,
            qlonglong pixTime)
{
    if ( mDelay >= 0 )
    {
        slovePre( dotTime, pixTime );
    }
    else
    {
        slovePost( dotTime, pixTime );
    }
}

void ChTimeDelay::align( qint32 baseDelay )
{
    mCoarseDelay = mCoarseDelay - baseDelay;
}

void ChTimeDelay::slovePre( qlonglong dotTime,
                qlonglong pixTime )
{
    Q_ASSERT( dotTime > 0 );
    Q_ASSERT( pixTime > 0 );

    mPreSa = (mDelay+dotTime-1) / dotTime;
    mCoarseDelay = mPreSa;
    mFineDelay = (mPreSa * dotTime-mDelay)/pixTime;
}

void ChTimeDelay::slovePost( qlonglong dotTime,
                qlonglong pixTime  )
{
    Q_ASSERT( dotTime > 0 );
    Q_ASSERT( pixTime > 0 );

    Q_ASSERT( mDelay <= 0 );

    mPostSa = ((-mDelay)+dotTime-1) / dotTime;
    mCoarseDelay = -mPostSa;
    mFineDelay = (-mDelay)/pixTime;
}

//! -- size info
static _LaSizeInfo _laSizeInfo[] =
{
    { 60, 2, 1, 0 },   //! 480/8
    { 30, 2, 1, 0 },   //! 480/16
    { 15, 2, 1, 0 },   //! 480/32

    { 40, 2, 1, 0 },   //! 320/8
    { 20, 2, 1, 0 },   //! 320/16
    { 10, 2, 1, 0 },   //! 320/32

    { 18, 2, 1, 0 },   //! 144/8
    { 9, 2, 1, 0 },    //! 144/16
    { 4, 1, 1, 0 },    //! 144/32
};

LaSizeInfo LaSizeInfo::getSizeInfo( int pixSize )
{
    Q_ASSERT( pixSize > 0 );

    LaSizeInfo sizeInfo;
    for ( int i = 0; i < array_count(_laSizeInfo); i++ )
    {
        if ( _laSizeInfo[i].mPixSize == pixSize )
        {
            sizeInfo = _laSizeInfo[i];
            return sizeInfo;
        }
    }

    //! default
    sizeInfo.setSize( pixSize );
    return sizeInfo;
}

LaSizeInfo::LaSizeInfo()
{
    mPixSize = 0;
    mHead = 0;
    mFoot = 0;
}

void LaSizeInfo::setSize( int pix, int line, int head, int foot )
{
    mPixSize = pix;
    mLine = line;
    mHead = head;
    mFoot = foot;
}
LaSizeInfo &LaSizeInfo::operator=( _LaSizeInfo &size )
{
    mPixSize = size.mPixSize;
    mLine = size.mLine;
    mHead = size.mHead;
    mFoot = size.mFoot;

    return *this;
}
int LaSizeInfo::getBody()
{
    return mPixSize - mHead - mFoot;
}

//! -- color info
LaColorInfo::LaColorInfo()
{
    mNormalHighColor = 0x008000;
    mNormalLowColor = 0x00ff00;
    mActiveColor = 0xff0000;
    mEdgeColor = 0xffffff;
}

void LaColorInfo::setColor( quint32 color )
{
    mNormalHighColor = color;
    mNormalLowColor = color;
    mEdgeColor = color;
}

void LaColorInfo::setColor( quint32 hColor,
                            quint32 eColor,
                            quint32 lColor )
{
    mNormalHighColor = hColor;
    mNormalLowColor = lColor;
    mEdgeColor = eColor;
}

quint32 LaColorInfo::getHighColor( bool bAct )
{
    return bAct ? mActiveColor : mNormalHighColor;
}
quint32 LaColorInfo::getLowColor( bool bAct )
{
    return bAct ? mActiveColor : mNormalLowColor;
}
quint32 LaColorInfo::getEdgeColor( bool bAct )
{
    return bAct ? mActiveColor : mEdgeColor;
}

laDx::laDx()
{
    mbEn = 0;
    mbActive = false;
    mPos = 0;

    mFrom = 0;
    mSpan = 0;
}

void laDx::setId( int id )
{
    mId = id;
}

void laDx::set( bool en, bool act, int pos )
{
    mbEn = en;
    mbActive = act;

    mPos = pos;
}

void laDx::setFromSpan( int from, int span )
{
    mFrom = from;
    mSpan = span;
}

void laDx::exportLines( QList<dso_phy::laLine*> &lines,
                  LaSizeInfo &sizeInfo,
                  LaColorInfo &colorInfo,
                        bool bZoom )
{
    //! only enabled one
    if ( !mbEn )
    { return; }

    dso_phy::laLine *pLine;

    //! Low
    int now = mFrom + sizeInfo.mFoot;
    for ( int i = 0; i < sizeInfo.mLine; i++ )
    {
        pLine = new dso_phy::laLine();
        if ( NULL == pLine )
        { return; }

        pLine->set( mbEn, bZoom, now++, mId, 1 );
        pLine->setColor( colorInfo.getLowColor(mbActive) );

        lines.append( pLine );
    }

    //! edge
    for ( int i = 0; i < sizeInfo.getBody() - sizeInfo.mLine * 2; i++ )
    {
        pLine = new dso_phy::laLine();
        if ( NULL == pLine )
        { return; }

        pLine->set( mbEn, bZoom, now++, mId, 3 );
        pLine->setColor( colorInfo.getEdgeColor(mbActive) );

        lines.append( pLine );
    }

    //! high
    for ( int i = 0; i < sizeInfo.mLine; i++ )
    {
        pLine = new dso_phy::laLine();
        if ( NULL == pLine )
        { return; }

        pLine->set( mbEn, bZoom, now++, mId, 2 );
        pLine->setColor( colorInfo.getHighColor(mbActive) );

        lines.append( pLine );
    }
}

void groupLa::initLaLines( QList<dso_phy::laLine*> &/*lines*/ )
{}
void groupLa::deInitLaLines( QList<dso_phy::laLine*> &lines )
{
    foreach( dso_phy::laLine *pLine, lines )
    {
        Q_ASSERT( NULL != pLine );

        delete pLine;
    }
}
void groupLa::fillNullLines( int height,
                             QList<dso_phy::laLine*> &lines )
{
    //! create the mark 0: height - 1
    QList<int> markLines;
    for( int i = 0; i < height; i++ )
    { markLines.append( i ); }

    //! scan the exist lines
    foreach( dso_phy::laLine *pLine, lines )
    {
        Q_ASSERT( pLine != NULL );

        markLines.removeOne( pLine->mRow );
    }

    //! the null line
    dso_phy::laLine *pLine;
    foreach( int nullRow, markLines )
    {
        pLine = new dso_phy::laLine();
        if ( NULL == pLine )
        { return; }

        pLine->set( true, false, nullRow, 0, 0 );

        lines.append( pLine );
    }
}

static bool laLineLessThan(const dso_phy::laLine *pL1,
                           const dso_phy::laLine *pL2 )
{
    Q_ASSERT( NULL != pL1 && NULL != pL2 );
    return pL1->mRow < pL2->mRow;
}

void groupLa::sortLaLine( QList<dso_phy::laLine*> &lines )
{
    qSort( lines.begin(), lines.end(), laLineLessThan );
}

int groupLa::getAreas( LaScale size )
{
    int areas[]={ 32, 16, 8 };

    if ( size >= Small && size <= Large )
    {}
    else
    {
        qWarning()<<size;
        Q_ASSERT( false );
    }

    return areas[ size - Small ];
}

void groupLa::logLaLine( QList<dso_phy::laLine*> &lines,
                         const QString &fileName )
{
    QFile file( fileName );
    if ( !file.open( QIODevice::WriteOnly) )
    { return; }

    QTextStream txtStream( &file);

    foreach( dso_phy::laLine *pLine, lines )
    {
        txtStream<<pLine->mRow<<","<<pLine->mDx<<","<<pLine->mEn<<","<<pLine->mbZoom<<","<<pLine->mPatt<<","<<QString::number(pLine->mColor,16)<<endl;
    }

    txtStream.flush();
}

groupLa::groupLa()
{
    mAreas = 16;

    //! init id
    for ( int i = 0; i < array_count(mDxs);i++ )
    {
        mDxs[i].setId( i );
    }
}

void groupLa::setSizeInfo( const LaSizeInfo &info )
{
    mSizeInfo = info;
}
void groupLa::setSize( LaScale size )
{
    mAreas = groupLa::getAreas( size );
}

void groupLa::setDx( int dx,
                      bool en,
                      bool active,
                      int pos )
{
    Q_ASSERT( dx >= 0 && dx <= array_count(mDxs) );

    mDxs[ dx ].set( en, active, pos );
}

void groupLa::setDx( int onOffMask,
            int activeMask,
            int pos[16] )
{
    for( int i = 0; i < 16; i++ )
    {
        mDxs[ i ].set( get_bit(onOffMask,i),
                        get_bit(activeMask,i),
                        pos[i] );
    }
}

void groupLa::setDxColor( int dx,
                 quint32 color )
{
    Q_ASSERT( dx >= 0 && dx <= array_count(mDxs) );

    mDxColors[ dx ].setColor( color );
}

void groupLa::setColor( quint32 hColor, quint32 eColor, quint32 lColor )
{
    for ( int i = 0; i < array_count(mDxColors); i++ )
    {
        mDxColors[i].setColor( hColor, eColor, lColor );
    }
}

void groupLa::split( int base, int height, bool invert )
{
    //! split
    int from[ mAreas ], span[ mAreas ];
    rsplit( height, mAreas, base, from, span, invert );

    //! deload on each dx
    int pos;
    for( int i = 0; i < array_count(mDxs); i++ )
    {
        pos = mDxs[i].mPos;

        if ( mDxs[i].mbEn )
        {
            mDxs[i].setFromSpan( from[pos], span[pos] );
        }
    }
}

void groupLa::split( int base, int height,
                     LaScale size,
                     bool invert )
{
    //! areas
    setSize( size );

    //! size info
    setSizeInfo( LaSizeInfo::getSizeInfo( height/mAreas) );

    //! split
    split( base, height, invert );
}

int groupLa::getDxGnd( int id, bool bInv )
{
    Q_ASSERT( id >=0 && id < 16 );
    if ( bInv )
    { return mDxs[id].mFrom + mSizeInfo.mFoot; }
    else
    { return mDxs[id].mFrom - mSizeInfo.mFoot; }
}

void groupLa::exportLines( QList<dso_phy::laLine*> &lines,
                           bool bZoom )
{
    //! for each line
    for ( int i = 0; i < array_count(mDxs); i++ )
    {
        mDxs[i].exportLines( lines, mSizeInfo, mDxColors[i], bZoom );
    }
}





