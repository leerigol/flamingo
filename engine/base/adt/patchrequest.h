#ifndef PATCHREQUEST_H
#define PATCHREQUEST_H

#include <QtCore>

//! patch_size * sub_patch <= 1M
#define patch_size  (4000)      //!一个包包含子包数量
#define sub_patch   (128)      //!子包大小（与逻辑对齐字节相关）


/*! |<----------------------n*sub_patch------------>|
 *--|-----------------------------------------------|
 *  |                          |---mPostPatchSize---|
 *  |  mPatchCount*mPatchSize  |<--mPostSize-->|    |
 *--|------------------------------------------|----|
 *  |<----------------------len--------------->|
 */
class CReadSplit
{
public:
     CReadSplit();
    ~CReadSplit();

public:
    int split( int len,
               int subPatchCount = patch_size,
               int subPatchSize  = sub_patch );

public:
    int mPatchCount;
    int mPatchSize;

    int mPostSize;

    int mPrePadSize;

    int    mPostPatchSize;
    quint8 *m_pPostPatch;
};

/*!     form      |<------------------------len----------------------->|
 *-------|--------|--------|-----------------------------|-------------------|
 *       |mPreSize|mPadSize|   mPatchCount*mPatchSize    |<----mPostSize----->|
 *-------|--------|--------|-----------------------------|--------------------|
 *       |    mPrePadSize  |                             | !eof: next mPreSize|
 *                                                       |  eof:  post        |
 */
class CWriteSplit
{
public:
     CWriteSplit();
    ~CWriteSplit();

public:
    int split(void   *pIn,
               void  *pTempIn,
               int   from,
               int   len,
               int   subPatchCount = patch_size,
               int   subPatchSize  = sub_patch );

public:
    int mPatchCount;
    int mPatchSize;

    int mPostSize;

    int mPrePadSize;
    int mPreSize;
    int mPadSize;

    int mOffset;

    quint8   *m_pPrePadPatch;
};

#endif // PATCHREQUEST_H
