//#define _DEBUG 1
#include "handlememory.h"
#include "../dsoengine.h"

#define de_inter_tmo_ms (200)
#define request_tmo_ms  (200)

//! handle memory
handleMemory::handleMemory( Chan ch )
{
    setChan( ch );  //! chan && reqSrc

    mFullSize = 0;

    mOffset = 0;
    mChCnt = 1;

    mbValid = false;

    m_pEngine = NULL;
}

handleMemory::~handleMemory()
{}

void handleMemory::setChan( Chan ch )
{
    mChan = ch;

    if ( mChan >= chan1 && mChan <= chan4 )
    {
        mReqSrc = dso_phy::traceRequest::trace_ch_main;
    }
    else if ( mChan == ana )
    {
        mReqSrc = dso_phy::traceRequest::trace_ch_main;
    }
    else if ( mChan >= d0 && mChan <= d15 )
    {
        mReqSrc = dso_phy::traceRequest::trace_la_main;
    }
    else if ( (mChan == d0d7) || (mChan == d8d15) || (mChan == d0d15) )
    {
        mReqSrc = dso_phy::traceRequest::trace_la_main;
    }
    else if ( mChan == la )
    {
        mReqSrc = dso_phy::traceRequest::trace_la_main;
    }
    else if ( mChan == ana_la )
    {
        mReqSrc = dso_phy::traceRequest::trace_cmb_main;
    }
    else
    { Q_ASSERT( false ); }
}
Chan handleMemory::getChan()
{
    return mChan;
}

void handleMemory::setSize(int fullSize)
{
    mFullSize = fullSize;
}
int handleMemory::getFullSize()
{ return mFullSize; }

void handleMemory::setOffset( int offs )
{
    mOffset = offs / mChCnt;
}

void handleMemory::setChCnt( int chCnt )
{
    Q_ASSERT( chCnt ==1 || chCnt == 2 || chCnt == 4 );

    mChCnt = chCnt;
}
int  handleMemory::getChCnt()
{
    return mChCnt;
}

void handleMemory::setValid( bool b )
{ mbValid = b; }
bool handleMemory::getValid()
{ return mbValid; }


void handleMemory::setEngine( dsoEngine *pEngine )
{
    Q_ASSERT( NULL != pEngine );

    m_pEngine = pEngine;
}
dsoEngine *handleMemory::getEngine()
{
    return m_pEngine;
}

int handleMemory::doRead( void *pOut, int len )
{
    bool req = false;

    //!len: 内存数据长度; reqLen: 每通道的长度。
    int  reqLen =  len / mChCnt;

    req = reqsTrace(reqLen, reqLen, request_tmo_ms);

    if( !req )
    {
        return -1;
    }

    return getOutData(pOut, len);
}
int handleMemory::doWrite( void *pIn, int len )
{
    Q_ASSERT( pIn != NULL && len > 0 );


    //! vars
    Q_ASSERT( m_pEngine != NULL );
    CDsoPhy *pPhy = m_pEngine->getPhy();
    Q_ASSERT( NULL != pPhy );
//LOG_DBG();
    //! load
    int ret;
    chanMeta meta;
    pPhy->mZynqFcuWr.getChanMeta( mChan, &meta );
    ret = pPhy->m_trace.assign(
                        meta,
                        len,
                        len,
                        pIn
                    );
    if ( ret != len )
    { return -1; }

//LOG_DBG();

    //! start misson
    ret = pPhy->m_trace.writeMission( 1, meta.getAddr(), len, 5000);
    if ( ret != len )
    { return -2; }

//LOG_DBG();

    return len;
}

int handleMemory::attrRead(frameAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );
    Q_ASSERT( m_pEngine != NULL );

    return 0;
}
int handleMemory::attrWrite(frameAttr *pAttr )
{
    Q_ASSERT( NULL != pAttr );
    Q_ASSERT( m_pEngine != NULL );

    return 0;
}

//! ch ch ch ch (4)
void handleMemory::postSet()
{
    Q_ASSERT( mChCnt > 0 );

    mFullSize = mFullSize * mChCnt;
}

//! mainLen以字节为单位，la的txLen是点为单位。
//! LA一个点两个字节
bool handleMemory::reqsTrace(int mainLen, int txLen, int timeOut)
{
    Q_ASSERT( m_pEngine != NULL );
    CDsoPhy *pPhy = m_pEngine->getPhy();
    Q_ASSERT( NULL != pPhy );

    //len = 25000; //!测试用。
    //! 1 配置K7参数
    pPhy->mSpuGp.setWAVE_OFFSET_MAIN( mOffset );
    pPhy->mSpuGp.setWAVE_LEN_MAIN(    mainLen );
    pPhy->mSpuGp.setTX_LEN_2( 1, txLen);
    pPhy->mSpuGp.flushWCache();

    LOG_DBG()<<"offset:"<<mOffset<<"mainlen:"<<mainLen<<"txLen:"<<txLen;

    //! add offset
    mOffset += mainLen;

    //! 2 Trace类型
    pPhy->mZynqFcuWr.outtrace_req_trace_datype( mReqSrc);

    LOG_DBG()<<"befor playlast";
    //! 3 回放一帧
    pPhy->mCcu.playLast(false);

    //！4 等待Trace done
    do{
        QThread::msleep(1);
        if( (timeOut--) < 0)
        {
            LOG_DBG()<<"!!!wait trace req done failure";
            return false;
        }
    }while(!pPhy->m_trace.isInDataDone( mReqSrc ) );

    //! 5清除完成标志
    pPhy->mZynqFcuWr.endTrace();

    return true;
}

bool handleMemory::reqsDeinter(int len, int timeOut, int srcType)
{
    Q_ASSERT( m_pEngine != NULL );
    CDsoPhy *pPhy = m_pEngine->getPhy();
    Q_ASSERT( NULL != pPhy );

    //！1 配置解交织参数
    pPhy->mZynqFcuWr.outdeinterweave_property_cfg4_channel_mode( mChCnt );

    pPhy->mZynqFcuWr.outdeinterweave_property_cfg1_deinterweave_source_type(srcType);
    pPhy->mZynqFcuWr.outdeinterweave_property_cfg1_decode_en( 0 );
    pPhy->mZynqFcuWr.outdeinterweave_property_cfg1_decode_en_sec( 0 );

    pPhy->mZynqFcuWr.outdeinterweave_chx_lax_rdat_num( len );

    //! 2 解交织请求
    pPhy->mZynqFcuWr.requestDeinte();

    //! 3 等待完成
    do{
        QThread::msleep(1);
        if( (timeOut--) < 0 )
        {
            LOG_DBG()<<"!!!wait Deinter req done failure";
            return false;
        }
    }while(!pPhy->m_trace.isDeinteDone());

    //! 4清除完成标志
    pPhy->mZynqFcuWr.endDeinte();

    return true;
}

int handleMemory::getOutData(void *pOut, int len)
{
    Q_ASSERT( pOut != NULL && len > 0 );
    Q_ASSERT( m_pEngine != NULL );
    CDsoPhy *pPhy = m_pEngine->getPhy();
    Q_ASSERT( NULL != pPhy );

    //! -- unload to ptr
    chanMeta meta;
    pPhy->mZynqFcuWr.getChanMeta( mChan, &meta );
    len = pPhy->m_trace.assign( pOut,
                                len,
                                len,
                                meta);
    return len;
}


//! ch1/2/3/4
handleMemoryCHx::handleMemoryCHx( Chan ch ) : handleMemory(ch)
{}

int handleMemoryCHx::doRead( void *pOut, int len )
{
    bool req = false;
    req = reqsTrace(len, len, request_tmo_ms);
    if( !req )
    {return -1;}

    req = reqsDeinter(len, de_inter_tmo_ms, 0);
    if( !req )
    {return -2;}

    return getOutData(pOut, len);
}

void handleMemoryCHx::setOffset(int offs)
{
    mOffset = offs;
}

void handleMemoryCHx::postSet()
{
}

handleMemoryDx::handleMemoryDx( Chan ch ) : handleMemory(ch)
{}

//! \todo length /8
int handleMemoryDx::doRead( void *pOut, int len )
{
    bool req = false;

    req = reqsTrace( len * 16, len * 8, request_tmo_ms);
    if( !req )
    {return -1;}

    req = reqsDeinter(len * 16, de_inter_tmo_ms, 1);
    if( !req )
    {return -2;}

    return getOutData(pOut, len);
}

void handleMemoryDx::setOffset(int offs)
{
    mOffset = offs;
}

void handleMemoryDx::postSet()
{
    mFullSize = mFullSize * 2 / 8;
}

handleMemoryLa::handleMemoryLa(Chan ch) : handleMemory(ch)
{

}

int handleMemoryLa::doRead(void *pOut, int len)
{
    bool req = false;

    int mainLen = 0;
    int txLen   = 0;
    if( (mChan == d0d7) || (mChan == d8d15) )
    {
        txLen   =  len;
    }
    else if( (mChan == d0d15) || (mChan == la) )
    {
        txLen   =  len / 2;
    }
    else
    {
        Q_ASSERT(false);
    }

    mainLen =  txLen * 8 / getChCnt();

    req = reqsTrace( mainLen, txLen, request_tmo_ms);
    if( !req )
    {return -1;}

    return getOutData(pOut, len);
}

void handleMemoryLa::setOffset(int offs)
{
#if 0
    mOffset = offs;
#else
    if( (mChan == d0d7) || (mChan == d8d15) )
    {
        mOffset =  offs * 8 / mChCnt;
    }
    else if( (mChan == d0d15) || (mChan == la) )
    {
        mOffset =  offs / 2 * 8 / mChCnt;
    }
    else
    {
        Q_ASSERT(false);
    }
#endif
}

/*! 一个la点 16bit*/
void handleMemoryLa::postSet()
{
    if( (mChan == d0d7) || (mChan == d8d15) )
    {
         mFullSize = mFullSize;
    }
    else if( (mChan == d0d15) || (mChan == la) )
    {
        mFullSize = mFullSize * 2;
    }
    else
    {
        Q_ASSERT(false);
    }
}

int handleMemoryLa::getOutData(void *pOut, int len)
{
    int readLen = 0;

    Q_ASSERT( pOut != NULL && len > 0 );
    Q_ASSERT( m_pEngine != NULL );
    CDsoPhy *pPhy = m_pEngine->getPhy();
    Q_ASSERT( NULL != pPhy );

    if( (mChan == d0d7) || (mChan == d8d15) )
    {
        readLen =  len* 2;
    }
    else if( (mChan == d0d15) || (mChan == la) )
    {
        readLen =  len;
    }
    else
    {
        Q_ASSERT(false);
    }

    quint8 *pBuf = new quint8[ readLen ];
    Q_ASSERT( NULL != pBuf );

    //! read data
    chanMeta meta;
    pPhy->mZynqFcuWr.getChanMeta( mChan, &meta );
    int retLen = pPhy->m_trace.assign( pBuf,
                                   readLen,
                                   readLen,
                                   meta);
    if(retLen != readLen)
    {
        qDebug()<<__FILE__<<__LINE__<<"assign size:"<<retLen<<readLen;
        return -1;
    }

    //! copy data
    if( mChan == d8d15 )
    {
        for(int i = 0; i < len; i++)
        {
            int src_i = i*2;
            Q_ASSERT(src_i <= readLen);

            memcpy( (quint8*)pOut + i, pBuf + src_i, 1 );
        }
    }
    else if(mChan == d0d7)
    {
        for(int i = 0; i < len; i++)
        {
            int src_i = i*2+1;
            Q_ASSERT( src_i<= readLen);

            memcpy( (quint8*)pOut+i, pBuf + src_i, 1 );
        }
    }
    else
    {
        memcpy( pOut, pBuf, len );
    }

    delete []pBuf;
    pBuf = NULL;

    return len;
}
