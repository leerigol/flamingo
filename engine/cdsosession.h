#ifndef CDSOSESSION_H
#define CDSOSESSION_H

#include "./module/cch.h"
#include "./module/cla.h"
#include "./module/chorizontal.h"
#include "./module/cacquire.h"
#include "./module/ccontrol.h"

#include "./module/csequence.h"
#include "./module/ctrigger.h"
#include "./module/cdisplay.h"
#include "./module/cdecode.h"

#include "./module/cmask.h"
#include "./module/chwproc.h"

#include "./module/csearch.h"

class CDsoRecEngine;
class CDsoPlayEngine;
class CDsoAutoStopEngine;
class dsoEngine;

#define ch1_bm_bit  0
#define ch2_bm_bit  1
#define ch3_bm_bit  2
#define ch4_bm_bit  3

#define la_bm_bit   4

/*!
 * \brief The CDsoSession class
 * engine status
 * - ch
 * - hori
 * - acquire
 * - control
 * - sequence
 * - trig
 */
class CDsoSession
{
public:
    CDsoSession();
    CDsoSession( const CDsoSession &ses );
    CDsoSession & operator=( const CDsoSession &ses );

public:
    CCH m_ch[4];    //! 0--ch1

    CLa mLa;

    CHorizontal m_horizontal;
    CAcquire m_acquire;
    CControl m_control;
    CSequence m_sequence;

    CTrigger m_trigger;

    CDisplay m_display;

    CDecode m_decode;

    CMask m_mask;

    CHwProc m_measure;
    CSearch m_search;

    bool m_bFastTimeBase;
    bool m_bNeedPlayClear;
    static bool m_bPlayValid;

    bool m_bCalibrating;
    bool m_RecDone;
public:

    void setValid( bool b );
    bool getValid();

    bool getSaCHValid( Chan ch );

    //! bit0 -- ch1
    //! bit4 -- la
    quint32 getTrigCHBm();
    quint32 getShowCHBm();
    quint32 getSaCHBm();

    void snapHoriTag( EngineHoriTag *pTag );

friend class dsoEngine;
friend class CDsoRecEngine;
friend class CDsoPlayEngine;
friend class CDsoAutoStopEngine;

};

#endif // CDSOSESSION_H
