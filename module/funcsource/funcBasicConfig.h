#ifndef FUNCBASICCONFIG_H
#define FUNCBASICCONFIG_H

#include "../devdgu/devDgu.h"
#include "ComStru.h"
#include "qthread.h"

class BasicConfig
{
public:
    //! 只留了两个配置接口，一个是带有波表配置，另一个是单纯各种参数。
    s16 funcSetSourceEnabel(comBasicParaStru stSourcePara, devFpgaChanEu chan);
    s16 funcSetSourcePara(comBasicParaStru stSourcePara, devFpgaChanEu chan);
    s16 funcSetSourcePhaseAlign(devFpgaChanEu chan, comBasicParaStru stBasicPara);
    //!相位配置会影响波形，所以单独提取出来使用
    s16 funcSetSourcePhase(devFpgaChanEu chan, comBasicParaStru stBasicPara);
    //!方波和脉冲准备
    s16 funcSetSourceFreq(comBasicParaStru stSourcePara, devFpgaChanEu chan);
private:
    s16 funcSetSourceWaveType(devFpgaChanEu chan, comBasicParaStru stSourcePara);
    s16 funcSetSourceInternalWave(devFpgaChanEu chan, SourceInternalWaveformEnum enInterWaveform);

    s16 funcSetSourceFreq(devFpgaChanEu chan, u64 u64Freq);
    s16 funcSetSourceAmp(devFpgaChanEu chan, comBasicParaStru stBasicPara,
                         u16 u16CH1RelayStatus, u16 u16CH2RelayStatus);
    s16 funcSetSourceOffset(devFpgaChanEu chan, s32 s32Offset,
                            u16 u16CH1RelayStatus, u16 u16CH2RelayStatus);

    //s16 funcSetSourceSymmetry(devFpgaChanEu chan,u16 u16Symmetry);
    //s16 funcSetSourceDuty(devFpgaChanEu chan,u16 u16Duty);

    //s16 funcSetSourcePhase(devFpgaChanEu chan, comBasicParaStru stBasicPara);

    u16 funcGetSourceSysCtrlReg(devFpgaChanEu, comBasicParaStru stBasicPara);
    u16 funcGetSourceRelayCtrlReg(u16* pu16CH1RelayStauts, u16* pu16CH2RelayStauts,
                                  devFpgaChanEu chan, comBasicParaStru stBasicPara);
    void funcGetSource1RelayCtrlReg(u16* pu16CH1RelayStauts, u16* pu16RelayVal, comBasicParaStru stBasicPara);
    void funcGetSource2RelayCtrlReg(u16* pu16CH2RelayStauts, u16* pu16RelayVal, comBasicParaStru stBasicPara);

private:
    u32 m_bRelay;
};
#endif // FUNCBASICCONFIG_H

