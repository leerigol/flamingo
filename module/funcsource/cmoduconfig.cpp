#include "cmoduconfig.h"
#include "funcConstructWaveData.h"
#include <cmath>

extern u16 *g_pu16WaveSineModData;        //Sine调制波波表（1kpts）
extern u16 *g_pu16WaveSquareModData;      //Square调制波波表（1kpts）
extern u16 *g_pu16WaveRampModData;        //Ramp调制波波表
extern u16 *g_pu16WaveNoiseData;
s16 CModuConfig::funcSetSourceModEnable(devFpgaChanEu chan, comBasicParaStru stBasicPara,
                                        comModuParaStru stModuPara)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;

    u16 u16SysCtrlReg= 0;
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);
//    if(	  stSourcePara[chan-1].enSourceWaveform == WAVE_DC
//       || stSourcePara[chan-1].enSourceWaveform == WAVE_PULSE
//       || stSourcePara[chan-1].enSourceWaveform == WAVE_NOISE)
//    {
//        //如果之前的波形打开了调制，那么这里需要关闭
//        stSourcePara[chan-1].bModEnable = false;
//    }
//    else
//    {
//        stSourcePara[chan-1].bModEnable = stModuPara;
//    }

    if(stModuPara.bModEnable)
    {
        funcSetSourceModFreq(chan, stModuPara.u64ModFreq, stModuPara.enModWave);
        if (stModuPara.enModType == MOD_AM)
        {
            funcSetSourceModAMDepth(chan, stModuPara.u8AMDepth, stModuPara.bModEnable);
        }
        else if(stModuPara.enModType == MOD_FM)
        {
            funcSetSourceModFMDev(chan, stBasicPara, stModuPara);
        }
        else if(stModuPara.enModType == MOD_FSK)
        {
            LOG_DBG()<<stModuPara.u64HopFreq;
            //! 测试代码:解决从扫频直接切到FSK时没波形的问题（应该是需要配置DACcomp的原因）
            funcSetSourceModAMDepth(chan, stModuPara.u8AMDepth, stModuPara.bModEnable);

            funcSetSourceHopFreq(chan, stModuPara.u64HopFreq);
        }

        //! 配置调制波表
        if(stModuPara.enModType != MOD_FSK)
        {
            funcSetSourceModWave(chan, stModuPara);
        }
    }

    u16SysCtrlReg = funcGetSourceSysCtrlReg(chan, stBasicPara, stModuPara);
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);
    return s16Err;
}

s16 CModuConfig::funcSetSourceModPara(devFpgaChanEu chan, comBasicParaStru stBasicPara,
                                        comModuParaStru stModuPara)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;

    u16 u16SysCtrlReg= 0;
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

    if(stModuPara.bModEnable)
    {
        funcSetSourceModFreq(chan, stModuPara.u64ModFreq, stModuPara.enModWave);
        if (stModuPara.enModType == MOD_AM)
        {
            funcSetSourceModAMDepth(chan, stModuPara.u8AMDepth, stModuPara.bModEnable);
        }
        else if(stModuPara.enModType == MOD_FM)
        {
            funcSetSourceModFMDev(chan, stBasicPara, stModuPara);
        }
        else if(stModuPara.enModType == MOD_FSK)
        {
            LOG_DBG()<<stModuPara.u64HopFreq;
            funcSetSourceHopFreq(chan, stModuPara.u64HopFreq);
        }

//        //! 配置调制波表
//        if(stModuPara.enModType != MOD_FSK)
//        {
//            funcSetSourceModWave(chan, stModuPara);
//        }
    }

    u16SysCtrlReg = funcGetSourceSysCtrlReg(chan, stBasicPara, stModuPara);
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);
    return s16Err;
}

s16 CModuConfig::funcSetSourceModWave(devFpgaChanEu chan, comModuParaStru stModuPara)
{
    SourceModWave euModuWave = stModuPara.enModWave;
    if (euModuWave == MOD_WAVE_SINE)
    {
        devConfigWaveDataRam( g_pu16WaveSineModData, WAVE_MOD_SIZE, 0, chan, MODU_WAVE );
    }
    else if (euModuWave == MOD_WAVE_SQUARE)
    {
        funcConstuctPulseData(g_pu16WaveSquareModData, WAVE_MOD_AMP,
                              WAVE_MOD_SIZE, 500, stModuPara.u64ModFreq );
        devConfigWaveDataRam(g_pu16WaveSquareModData, WAVE_MOD_SIZE, 0, chan, MODU_WAVE );
    }
    else if (euModuWave == MOD_WAVE_RAMP)
    {
        devConfigWaveDataRam( g_pu16WaveRampModData, WAVE_MOD_SIZE, 0, chan, MODU_WAVE );
    }
    else
    {
        //! 噪声作为调制波频率单独设置
        u64 u64FreqVal = DEV_MOD_FREQ_WORD_GEN(50000*1e6);
        devConfigModuFreqReg(&u64FreqVal, chan);
        devConfigWaveDataRam( g_pu16WaveNoiseData, WAVE_MOD_SIZE, 0, chan, MODU_WAVE );
    }

    return ERR_DEV_DG_NONE;
}

s16 CModuConfig::funcSetSourceModFreq(devFpgaChanEu chan,u64 u64ModFreq,SourceModWave moduWave)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u64 u64FreqVal;

    if(moduWave == MOD_WAVE_NOISE)
    {
        u64FreqVal = DEV_MOD_FREQ_WORD_GEN(50000*1e6);
        devConfigModuFreqReg(&u64FreqVal, chan);
    }
    else
    {
        //频率设置
        u64FreqVal = DEV_MOD_FREQ_WORD_GEN(u64ModFreq);
        devConfigModuFreqReg(&u64FreqVal, chan);
    }

    return s16Err;
}

s16 CModuConfig::funcSetSourceHopFreq(devFpgaChanEu chan,u64 u64HopFreq)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u64 u64FreqVal = 0;
    //频率设置
    u64FreqVal = DEV_FREQ_WORD_GEN(u64HopFreq);
    devConfigHopFreqReg( &u64FreqVal, chan );

    return s16Err;
}

s16 CModuConfig::funcSetSourceModAMDepth(devFpgaChanEu chan, s8 u8AMDepth, bool moduEn)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u16 u16AmpValue = 0xffff;
    u64 u64AmDepth;
    u16 u16AmDacComp;

    u64AmDepth = FUN_CTRL_AM_DEV(u8AMDepth);
    u16AmDacComp = FUN_CTRL_AM_COMP(u8AMDepth);

    devConfigModuDevIndexReg (&u64AmDepth, chan);
    devConfigAmplDevIndexReg ( &u16AmpValue, chan);
    if (!moduEn)
    {
        u16AmDacComp = 0x8000;
    }
    devConfigAmDacCompReg(&u16AmDacComp, chan);

    return s16Err;
}

s16 CModuConfig::funcSetSourceModFMDev(devFpgaChanEu chan, comBasicParaStru stBasicPara,comModuParaStru stModuPara)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;

    u16 u16AmpValue = 0xffff;
    u16 u16AmDacComp = 0x8000;
    u64 u64ModFreq = stModuPara.u64ModFreq;

    u64 u64CarrierFreq = stBasicPara.s64Freq - stModuPara.u64FMDev;

    u64CarrierFreq = DEV_FREQ_WORD_GEN(u64CarrierFreq);

    u64 u64Span = stModuPara.u64FMDev << 1;
    u64Span = DEV_MOD_FM_FREQ_DEV_GEN(u64Span);

    //只有FM调制有效时重新配置参,否则会影响AM调制
    if(stModuPara.bModEnable == true && stModuPara.enModType == MOD_FM)
    {
        devConfigCenterFreqReg ( &u64CarrierFreq, chan );
        devConfigModuDevIndexReg ( &u64Span, chan );

        devConfigAmplDevIndexReg(&u16AmpValue, chan);
        devConfigAmDacCompReg(&u16AmDacComp, chan);

        u64ModFreq = DEV_MOD_FREQ_WORD_GEN(u64ModFreq);
        devConfigModuFreqReg(&u64ModFreq, chan);
    }

    return s16Err;
}

u16 CModuConfig::funcGetSourceSysCtrlReg(devFpgaChanEu /*chan*/, comBasicParaStru stBasicPara, comModuParaStru stModuPara)
{
    u16 u16SourceSysCtrl = 0;

    if (stBasicPara.bSourceEnable)
    {
        u16SourceSysCtrl |= DEV_CTRL_DDS_ON;
    }
    else
    {
        u16SourceSysCtrl &= ~DEV_CTRL_DDS_ON;
    }

    if (stBasicPara.enSourceWaveform == WAVE_NOISE)
    {
        u16SourceSysCtrl |= DEV_CTRL_NOISE_EN;
    }
    else
    {
        u16SourceSysCtrl &= ~DEV_CTRL_NOISE_EN;
    }

    if (stModuPara.bModEnable)
    {
        if (stModuPara.enModType == MOD_AM )
        {
            LOG_DBG();
            u16SourceSysCtrl |= DEV_CTRL_AM_EN | DEV_CTRL_MOD_EN;
        }
        else if (stModuPara.enModType == MOD_FM )
        {
            LOG_DBG();
            u16SourceSysCtrl |= DEV_CTRL_FM_EN | DEV_CTRL_MOD_EN;
        }
        else if (stModuPara.enModType == MOD_FSK)
        {
            LOG_DBG();
            u16SourceSysCtrl |= DEV_CTRL_FSK_EN | DEV_CTRL_MOD_EN;
        }
    }

    return u16SourceSysCtrl;
}
