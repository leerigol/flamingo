#include "funcBasicConfig.h"
#include "funcConstructWaveData.h"
#include "funcCalSource.h"
#include <cmath>

extern SourceCalParaStru stSourceCalPara;

extern u16 *g_pu16WaveRampData;
extern u16 *g_pu16WaveSineData;
extern u16 *g_pu16WaveSquareData;
extern u16 *g_pu16WavePulseData;
extern u16 *g_pu16WaveDCData;
extern u16 *g_pu16WaveNoiseData;
extern u16 *g_pu16WaveArbCh1Data;
extern u16 *g_pu16WaveArbCh2Data;
//extern u16 *g_pu16WaveSineModData;        //Sine调制波波表（1kpts）
//extern u16 *g_pu16WaveSquareModData;      //Square调制波波表（1kpts）
//extern u16 *g_pu16WaveRampModData;        //Ramp调制波波表


extern u16 g_u16WaveSincData[];
extern u16 g_u16WaveECGData[];
extern u16 g_u16WaveLorentzData[];
extern u16 g_u16WaveHaverSineData[];
extern u16 g_u16WaveGaussData[];
extern u16 g_u16WaveExpRiseData[];
extern u16 g_u16WaveExpFallData[];

s16 BasicConfig::funcSetSourceEnabel(comBasicParaStru stSourcePara, devFpgaChanEu chan)
{
    LOG_DBG();
    s16 s16Err = ERR_FUNC_SOURCE_NONE;

    u16 u16SysCtrlReg= 0;
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

    static u16 u16CH1RelayStatus = 0;
    static u16 u16CH2RelayStatus = 0;
    u32 u32Relay = funcGetSourceRelayCtrlReg(&u16CH1RelayStatus, &u16CH2RelayStatus,
                                             chan, stSourcePara);

    //!对比ds1000代码，添加
    if((stSourcePara.s64Freq% 1000000000000) == 0)
    {
        stSourcePara.s64Freq += (stSourcePara.s64Freq/1000000000000) * 1000000;
    }

    funcSetSourceWaveType(chan, stSourcePara);
    //! 电压设置
    if(stSourcePara.enSourceWaveform != WAVE_DC)
    {
        funcSetSourceAmp(chan,stSourcePara,u16CH1RelayStatus,u16CH2RelayStatus);
    }
    funcSetSourceOffset(chan,0,u16CH1RelayStatus,u16CH2RelayStatus);
    //! 频率设置  
    funcSetSourceFreq(chan, stSourcePara.s64Freq);
    //! 相位设置
    funcSetSourcePhase(chan, stSourcePara);
    //! 打开或者关闭继电器
    devConfigRelayCtrlReg( &u32Relay );
    QThread::msleep(20);

    funcSetSourceOffset(chan,stSourcePara.s32Offset/1000,u16CH1RelayStatus,u16CH2RelayStatus);
    //! 打开DDS
    u16SysCtrlReg = funcGetSourceSysCtrlReg(chan,stSourcePara);
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

    return s16Err;
}

s16 BasicConfig::funcSetSourceFreq(comBasicParaStru stSourcePara, devFpgaChanEu chan)
{
    LOG_DBG();
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u16 u16SysCtrlReg= 0;
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);
   //!对比ds1000代码，添加
   if( (stSourcePara.s64Freq% 1000000000000) == 0)
   {
       stSourcePara.s64Freq += (stSourcePara.s64Freq/1000000000000) * 1000000;
   }

    static u16 u16CH1RelayStatus = 0;
    static u16 u16CH2RelayStatus = 0;
    u32 u32Relay = funcGetSourceRelayCtrlReg(&u16CH1RelayStatus, &u16CH2RelayStatus,
                                             chan, stSourcePara);
    funcSetSourceWaveType(chan, stSourcePara);
    funcSetSourceFreq(chan, stSourcePara.s64Freq);
   // funcSetSourceWaveType(chan, stSourcePara);

    //! 打开或者关闭继电器
    devConfigRelayCtrlReg( &u32Relay );

    //! 打开DDS
    u16SysCtrlReg = funcGetSourceSysCtrlReg(chan,stSourcePara);
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

    return s16Err;
}

s16 BasicConfig::funcSetSourcePara(comBasicParaStru stSourcePara, devFpgaChanEu chan)
{
    LOG_DBG();
    s16 s16Err = ERR_FUNC_SOURCE_NONE;

    u16 u16SysCtrlReg= 0;
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

    u16 u16CH1RelayStatus = 0;
    u16 u16CH2RelayStatus = 0;
    u32 u32Relay = funcGetSourceRelayCtrlReg(&u16CH1RelayStatus, &u16CH2RelayStatus,
                                           chan, stSourcePara);
    //!对比ds1000代码，添加
    if( (stSourcePara.s64Freq% 1000000000000) == 0)
    {
        stSourcePara.s64Freq += (stSourcePara.s64Freq/1000000000000) * 1000000;
    }
    //! 电压设置
    funcSetSourceAmp(chan,stSourcePara,u16CH1RelayStatus,u16CH2RelayStatus);

    funcSetSourceOffset(chan,stSourcePara.s32Offset/1000,u16CH1RelayStatus,u16CH2RelayStatus);

    //! 没有波表配置
    // funcSetSourceWaveType(chan, stSourcePara);
    //!如果是方波或者脉冲需要重新配置波表
    if(stSourcePara.enSourceWaveform == WAVE_PULSE || stSourcePara.enSourceWaveform == WAVE_SQUARE)
    {
        funcSetSourceWaveType(chan, stSourcePara);
    }
    //! 频率设置
    funcSetSourceFreq(chan, stSourcePara.s64Freq);
    //! 相位设置
    //  funcSetSourcePhase(chan, stSourcePara)
    //bug 3352 by zy
    //! 打开或者关闭继电器
    devConfigRelayCtrlReg ( &u32Relay );
    if(m_bRelay != u32Relay)
    {
        QThread::msleep(200);
    }
    m_bRelay = u32Relay;

    //!打开
    u16SysCtrlReg = funcGetSourceSysCtrlReg(chan,stSourcePara);
    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

    return s16Err;

}

//! 方波和脉冲波改变频率、对称性、占空比的情况下都需要重新生成波表
s16 BasicConfig::funcSetSourceWaveType(devFpgaChanEu chan, comBasicParaStru stSourcePara)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    //u16 u16SysCtrlReg = 0;
    u16 u16Optimize = 0;

    SourceWaveformEnum euSourceWaveform = stSourcePara.enSourceWaveform;
   // u16 u16CfgVal = 0x2000;
    //devConfigDacDataIdleReg ( &u16CfgVal, ALL_FPGA );
    devConfigWaveOptReg(&u16Optimize, chan);

    if(euSourceWaveform == WAVE_SINE)
    {
        u16Optimize = DEV_WAVE_TYPE_SINE;
        devConfigWaveDataRam( g_pu16WaveSineData, WAVE_SIZE, 0, chan, CARRY_WAVE );
        //devConfigDacDataIdleReg ( &g_pu16WaveSineData[0], chan );
        devConfigWaveOptReg(&u16Optimize, chan);
    }
    else if (euSourceWaveform == WAVE_SQUARE)
    {
        funcConstuctPulseData(g_pu16WaveSquareData, WAVE_AMP, WAVE_SIZE,
                              500, stSourcePara.s64Freq);
        //devConfigDacDataIdleReg ( &g_pu16WaveSquareData[0], chan );
        devConfigWaveDataRam( g_pu16WaveSquareData, WAVE_SIZE, 0, chan, CARRY_WAVE );

    }
    else if (euSourceWaveform == WAVE_RAMP)
    {
        /*
        u16 u16Symmetry;
        if(stSourcePara.u16Symmetry == 0)
        {
            u16Symmetry = 1;
        }
        else if(stSourcePara.u16Symmetry == 1000)
        {
             u16Symmetry = 999;
        }
        else
        {
              u16Symmetry = stSourcePara.u16Symmetry;
        }
        */
         funcConstuctRampData(g_pu16WaveRampData, WAVE_AMP, WAVE_SIZE, stSourcePara.u16Symmetry);
      //  funcConstuctRampData(g_pu16WaveRampData, WAVE_AMP, WAVE_SIZE, u16Symmetry);
        devConfigWaveDataRam( g_pu16WaveRampData, WAVE_SIZE, 0, chan, CARRY_WAVE );
        //funcSetSourceSymmetry(chan, stSourcePara.u16Symmetry);
        //devConfigSysCtrlReg(&u16SysCtrlReg, chan);
       // devConfigDacDataIdleReg ( &u16CfgVal, chan );
    }
    else if (euSourceWaveform == WAVE_PULSE)
    {
        funcConstuctPulseData(g_pu16WavePulseData, WAVE_AMP, WAVE_SIZE,
                              stSourcePara.u16Duty, stSourcePara.s64Freq);
        devConfigWaveDataRam( g_pu16WavePulseData, WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (euSourceWaveform == WAVE_NOISE)
    {
        devConfigWaveDataRam( g_pu16WaveNoiseData, WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (euSourceWaveform == WAVE_DC)
    {
        devConfigWaveDataRam( g_pu16WaveDCData, WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (euSourceWaveform == WAVE_ARB_INTERNAL)
    {
        funcSetSourceInternalWave(chan, stSourcePara.enInterWaveform);
    }
    else
    {   
        if(chan == CH1_FPGA)
        {
            devConfigWaveDataRam( g_pu16WaveArbCh1Data, WAVE_SIZE, 0, chan, CARRY_WAVE );
        }
        else
        {
            devConfigWaveDataRam( g_pu16WaveArbCh2Data, WAVE_SIZE, 0, chan, CARRY_WAVE );
        }
    }
    return s16Err;
}

s16 BasicConfig::funcSetSourceFreq(devFpgaChanEu chan, u64 u64Freq)
{
     u64 u64FreqVal = 0;

    //! 频率设置:方波和三角波需要修改波表、幅度修改
    u64FreqVal = DEV_FREQ_WORD_GEN(u64Freq);
    //qDebug() << "u64FreqVal = " << u64FreqVal;
    devConfigCenterFreqReg ( &u64FreqVal, chan);
    return dg_err_none;
}

s16 BasicConfig:: funcSetSourceAmp(devFpgaChanEu chan, comBasicParaStru stBasicPara,
                              u16 u16CH1RelayStatus, u16 u16CH2RelayStatus)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u16 u16DACRefVal = 0;
    u16 u16DacOffsetVal = 0;

    u32 u32Amp = stBasicPara.u32Amp / 1000;
    s32 s32offset = stBasicPara.s32Offset / 1000;
    if(chan == CH1_FPGA)
    {
        if(stBasicPara.enSourceWaveform == WAVE_DC)
        {
            u16DACRefVal = ((20- stSourceCalPara.s16CH1AmpCalFactor[0][SOURCE_CAL_FACTOR_B]) * 65535)
                    / stSourceCalPara.s16CH1AmpCalFactor[0][SOURCE_CAL_FACTOR_A];

        }
        else
        {
            u16DACRefVal = ((u32Amp - stSourceCalPara.s16CH1AmpCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_B]) * 65535)
                           / stSourceCalPara.s16CH1AmpCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A];

        //  qDebug()<<"u32Amp = " << u32Amp<< "u16DACRefVal = " << u16DACRefVal;
       //   qDebug()<<stSourceCalPara.s16CH1AmpCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_B]<<
           //            stSourceCalPara.s16CH1AmpCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A];
        }
        devConfigParaDacReg ( &u16DACRefVal, DEV_PARA_DAC_REF_CH1 );

       // qDebug() << stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A];
        //由于幅度可能会影响到偏移，所以此处需要酌情配置
        if(stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A])
        {
            u16DacOffsetVal = ((s32offset - stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_B]) * 32768)
                            / stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A] + 32768;
        }
        else
        {
            qDebug() << "fail to setDGoffset";
           // ERR_INVALID_CONFIG
        }
        /*    LOG_DBG()<<s32offset<<u16DacOffsetVal;
        LOG_DBG()<<stSourceCalPara.s16CH2OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_B]<<
                   stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A];
*/
        devConfigParaDacReg ( &u16DacOffsetVal, DEV_PARA_DAC_OFFSET_CH1 );
    }
    else
    {
        LOG_DBG()<<u16CH2RelayStatus;

        if(stBasicPara.enSourceWaveform == WAVE_DC)
        {
            u16DACRefVal = ((20- stSourceCalPara.s16CH2AmpCalFactor[0][SOURCE_CAL_FACTOR_B]) * 65535)
                    / stSourceCalPara.s16CH2AmpCalFactor[0][SOURCE_CAL_FACTOR_A];
        }
        else
        {
            u16DACRefVal = ((u32Amp - stSourceCalPara.s16CH2AmpCalFactor[u16CH2RelayStatus][SOURCE_CAL_FACTOR_B]) * 65535)
                           / stSourceCalPara.s16CH2AmpCalFactor[u16CH2RelayStatus][SOURCE_CAL_FACTOR_A];
        }

        devConfigParaDacReg ( &u16DACRefVal, DEV_PARA_DAC_REF_CH2 );

        LOG_DBG()<<s32offset<<u16DacOffsetVal;
        LOG_DBG()<<stSourceCalPara.s16CH2OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_B]<<
                   stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A];
        //由于幅度可能会影响到偏移，所以此处需要酌情配置

         if(stSourceCalPara.s16CH2OffsetCalFactor[u16CH2RelayStatus][SOURCE_CAL_FACTOR_A])
         {
             u16DacOffsetVal = ((s32offset - stSourceCalPara.s16CH2OffsetCalFactor[u16CH2RelayStatus][SOURCE_CAL_FACTOR_B]) * 32768)
                            / stSourceCalPara.s16CH2OffsetCalFactor[u16CH2RelayStatus][SOURCE_CAL_FACTOR_A] + 32768;
         }
         else
         {
             qDebug() << "fail to setDGoffset";
         }
         devConfigParaDacReg ( &u16DacOffsetVal, DEV_PARA_DAC_OFFSET_CH2 );
    }

    return s16Err;
}


s16 BasicConfig::funcSetSourceOffset(devFpgaChanEu chan, s32 s32Offset,
                                     u16 u16CH1RelayStatus, u16 u16CH2RelayStatus)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u16 u16DacOffsetVal = 0;
    LOG_DBG();
    if (chan == CH1_FPGA)
    {
        u16DacOffsetVal = ((s32Offset - stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_B]) * 32768)
                                 / stSourceCalPara.s16CH1OffsetCalFactor[u16CH1RelayStatus][SOURCE_CAL_FACTOR_A] + 32768;
        devConfigParaDacReg ( &u16DacOffsetVal, DEV_PARA_DAC_OFFSET_CH1 );
    }
    else
    {
        u16DacOffsetVal = ((s32Offset - stSourceCalPara.s16CH2OffsetCalFactor[u16CH2RelayStatus][SOURCE_CAL_FACTOR_B]) * 32768)
                                 / stSourceCalPara.s16CH2OffsetCalFactor[u16CH2RelayStatus][SOURCE_CAL_FACTOR_A] + 32768;
        devConfigParaDacReg ( &u16DacOffsetVal, DEV_PARA_DAC_OFFSET_CH2 );
    }
    return s16Err;
}

s16 BasicConfig::funcSetSourceInternalWave(devFpgaChanEu chan, SourceInternalWaveformEnum enInterWaveform)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;

    if(enInterWaveform == INTERNAL_WAVE_SINC)
    {
        devConfigWaveDataRam( &g_u16WaveSincData[0], WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (enInterWaveform == INTERNAL_WAVE_EXP_RISE)
    {
        devConfigWaveDataRam( &g_u16WaveExpRiseData[0], WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (enInterWaveform == INTERNAL_WAVE_EXP_FAIL)
    {
        devConfigWaveDataRam( &g_u16WaveExpFallData[0], WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (enInterWaveform == INTERNAL_WAVE_CARDIAC)
    {
        devConfigWaveDataRam( &g_u16WaveECGData[0], WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (enInterWaveform == INTERNAL_WAVE_GAUSS)
    {
        devConfigWaveDataRam( &g_u16WaveGaussData[0], WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (enInterWaveform == INTERNAL_WAVE_LORENTZ)
    {
        devConfigWaveDataRam( &g_u16WaveLorentzData[0], WAVE_SIZE, 0, chan, CARRY_WAVE );
    }
    else if (enInterWaveform == INTERNAL_WAVE_HAVERSINE)
    {
        devConfigWaveDataRam( &g_u16WaveHaverSineData[0], WAVE_SIZE, 0, chan, CARRY_WAVE );
    }

    return s16Err;
}

s16 BasicConfig::funcSetSourcePhase(devFpgaChanEu chan,comBasicParaStru stBasicPara)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u16 u16PhaseVal = 0;

    if (chan == CH2_FPGA)
    {
        u16PhaseVal = stBasicPara.u16Phase + stSourceCalPara.s16PhaseDev;
    }
    else
    {
#if 1
        u16PhaseVal = stBasicPara.u16Phase;
  #else
        u16PhaseVal = stBasicPara.u16Phase + stSourceCalPara.s16PhaseDev;
#endif
    }

    u16PhaseVal = DEV_INIT_PHASE_WORD_GEN(u16PhaseVal);
    devConfigInitPhaseReg(&u16PhaseVal, chan);

   // funcSetSourcePhaseAlign(chan, stBasicPara);

    return s16Err;
}

s16 BasicConfig::funcSetSourcePhaseAlign(devFpgaChanEu chan, comBasicParaStru stBasicPara)
{

    chan = chan;
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u16 u16SysCtrlReg1 = 0;
    u16 u16SysCtrlReg2 = 0;
    static u16 u16SysCtrlRegTemp1 = 0;
    static u16 u16SysCtrlRegTemp2 = 0;

    //关闭DDS,并将DDS1和2的耦合打开
    u16SysCtrlReg1 |= DEV_CTRL_COUPLE_EN;
    u16SysCtrlReg2 |= DEV_CTRL_COUPLE_EN;
    devConfigSysCtrlReg(&u16SysCtrlReg1, CH1_FPGA);
    devConfigSysCtrlReg(&u16SysCtrlReg2, CH2_FPGA);


#if 0
    //打开DDS
    if(chan == CH1_FPGA)
    {
        u16SysCtrlRegTemp1 = funcGetSourceSysCtrlReg(chan, stBasicPara);
    }
    else
    {
        u16SysCtrlRegTemp2 = funcGetSourceSysCtrlReg(chan, stBasicPara);
    }
#else
    u16SysCtrlRegTemp1 = funcGetSourceSysCtrlReg(CH1_FPGA, stBasicPara);
    u16SysCtrlRegTemp2 = funcGetSourceSysCtrlReg(CH2_FPGA, stBasicPara);
#endif


    u16SysCtrlReg1 |= u16SysCtrlRegTemp1;
    u16SysCtrlReg2 |= u16SysCtrlRegTemp2;

    devConfigSysCtrlReg(&u16SysCtrlReg1, CH1_FPGA);
    devConfigSysCtrlReg(&u16SysCtrlReg2, CH2_FPGA);

    //关闭耦合情况
    u16SysCtrlReg1 &= ~DEV_CTRL_COUPLE_EN;
    u16SysCtrlReg2 &= ~DEV_CTRL_COUPLE_EN;
    devConfigSysCtrlReg(&u16SysCtrlReg1, CH1_FPGA);
    devConfigSysCtrlReg(&u16SysCtrlReg2, CH2_FPGA);

    return s16Err;
}

//! 改变对称性要修改波表
//s16 BasicConfig::funcSetSourceSymmetry(devFpgaChanEu chan,s16 u16Symmetry)
//{
//    s16 s16Err = ERR_FUNC_SOURCE_NONE;
//    u16 u16SysCtrlReg = 0;

//    //关闭DDS
//    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

//    //构建波表
//    funcConstuctRampData(g_pu16WaveRampData, WAVE_AMP, WAVE_SIZE, u16Symmetry);

//    devConfigWaveDataRam( g_pu16WaveRampData, WAVE_SIZE, 0, chan, CARRY_WAVE );
//    u16SysCtrlReg = funcGetSourceSysCtrlReg(chan);
//    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

//    return s16Err;
//}

////! 改变占空比要修改波表
//s16 BasicConfig::funcSetSourceDuty(devFpgaChanEu chan,u16 u16Duty)
//{
//    s16 s16Err = ERR_FUNC_SOURCE_NONE;
//    u16 u16SysCtrlReg = 0;

//    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

//    stBasicPara.u16Duty = u16Duty;

//    funcConstuctPulseData(g_pu16WavePulseData, WAVE_AMP, WAVE_SIZE,
//                          u16Duty, stBasicPara.s64Freq );
//    devConfigWaveDataRam( g_pu16WavePulseData, WAVE_SIZE, 0, chan, CARRY_WAVE );

//    u16SysCtrlReg = funcGetSourceSysCtrlReg(chan);
//    devConfigSysCtrlReg(&u16SysCtrlReg, chan);

//    return s16Err;
//}


u16 BasicConfig::funcGetSourceSysCtrlReg(devFpgaChanEu /*chan*/, comBasicParaStru stBasicPara)
{
    u16 u16SourceSysCtrl = 0;

    if (stBasicPara.bSourceEnable)
    {
        u16SourceSysCtrl |= DEV_CTRL_DDS_ON;
    }
    else
    {
        u16SourceSysCtrl &= ~DEV_CTRL_DDS_ON;
    }

    if (stBasicPara.enSourceWaveform == WAVE_NOISE)
    {
        u16SourceSysCtrl |= DEV_CTRL_NOISE_EN;
    }
    else
    {
        u16SourceSysCtrl &= ~DEV_CTRL_NOISE_EN;
    }

//    if (stBasicPara.bModEnable)
//    {
//        if (stBasicPara.enModType == MOD_AM )
//        {
//            u16SourceSysCtrl |= DEV_CTRL_AM_EN | DEV_CTRL_MOD_EN;
//        }
//        else if (stBasicPara.enModType == MOD_FM )
//        {
//            u16SourceSysCtrl |= DEV_CTRL_FM_EN | DEV_CTRL_MOD_EN;
//        }
//        else if (stBasicPara.enModType == MOD_FSK)
//        {
//            u16SourceSysCtrl |= DEV_CTRL_FSK_EN | DEV_CTRL_MOD_EN;
//        }
//    }

//    if (stSourcePara[chan - 1].euDgworkMode == DG_SWEEP)
//    {
//        u16SourceSysCtrl |= DEV_CTRL_FM_EN | DEV_CTRL_MOD_EN | DEV_CTRL_SWEEP_EN;
//    }
    return u16SourceSysCtrl;
}

u16 BasicConfig::funcGetSourceRelayCtrlReg(u16* pu16CH1RelayStatus, u16* pu16CH2RelayStatus,
                                           devFpgaChanEu chan, comBasicParaStru stBasicPara)
{
    static u16 u16RelayVal = 0;

    //! 将通道1的继电器控制置为0
    if( CH1_FPGA == chan)
    {
        u16RelayVal = (u16RelayVal & ~ CH1_DEV_RY_ATT & ~ CH1_DEV_RY_AMP & ~ CH1_DEV_RY_OUT);
        funcGetSource1RelayCtrlReg(pu16CH1RelayStatus, &u16RelayVal, stBasicPara);
    }
    else
    {
        u16RelayVal = (u16RelayVal & ~ CH2_DEV_RY_ATT & ~ CH2_DEV_RY_AMP & ~ CH2_DEV_RY_OUT);
        funcGetSource2RelayCtrlReg(pu16CH2RelayStatus, &u16RelayVal, stBasicPara);
    }

    return 	u16RelayVal;
}

void BasicConfig::funcGetSource1RelayCtrlReg(u16 *pu16CH1RelayStatus, u16 *pu16RelayVal,
                                             comBasicParaStru stBasicPara)
{
    u32 u32Amp = stBasicPara.u32Amp / 1000;
    s32 s32Offset = stBasicPara.s32Offset / 1000;
    if (stBasicPara.enSourceWaveform != WAVE_DC)
    {
        if (s32Offset <= 1000 && s32Offset >= -1000)
        {
            if (u32Amp < 160)                //衰减打开，放大关闭
            {
                *pu16RelayVal |= CH1_DEV_RY_ATT;
                *pu16CH1RelayStatus = AMP_CAL_ATT_ON_AMP_OFF;
            }
            else if (u32Amp < 1600 && u32Amp >= 160)         //衰减关闭，放大关闭
            {
                *pu16RelayVal |= 0;
                *pu16CH1RelayStatus = AMP_CAL_ATT_OFF_AMP_OFF;
            }
            else                                                     //衰减关闭，放大打开
            {
                *pu16CH1RelayStatus = AMP_CAL_ATT_OFF_AMP_ON;
                *pu16RelayVal |= CH1_DEV_RY_AMP;
            }
        }
        else
        {
            if (u32Amp < 900)
            {
                *pu16RelayVal |= CH1_DEV_RY_ATT;
                *pu16RelayVal |= CH1_DEV_RY_AMP;
                *pu16CH1RelayStatus = AMP_CAL_ATT_ON_AMP_ON;
            }
            else
            {
                *pu16RelayVal |= CH1_DEV_RY_AMP;
                *pu16CH1RelayStatus = AMP_CAL_ATT_OFF_AMP_ON;
            }
        }
    }
    else
    {
        //如果直流电压小于900mV时，衰减和放大均关闭，否则放大打开
        if (s32Offset <= 900 && s32Offset >= -900)
        {
            *pu16RelayVal |= 0;
            *pu16CH1RelayStatus = DC_CAL_ATT_OFF_AMP_OFF;
        }
        else
        {
            *pu16RelayVal |= CH1_DEV_RY_AMP;
            *pu16CH1RelayStatus = DC_CAL_ATT_OFF_AMP_ON;
        }
    }
    if (stBasicPara.bSourceEnable)
    {
        *pu16RelayVal |= CH1_DEV_RY_OUT;
    }
}

void BasicConfig::funcGetSource2RelayCtrlReg(u16 *pu16CH2RelayStatus, u16 *pu16RelayVal,
                                             comBasicParaStru stBasicPara)
{
    u32 u32Amp = stBasicPara.u32Amp / 1000;
    s32 s32Offset = stBasicPara.s32Offset / 1000;
    //! 配置CH2的继电器
    if (stBasicPara.enSourceWaveform != WAVE_DC)
    {
        if (s32Offset <= 1000 && s32Offset >= -1000)
        {
            if (u32Amp < 160)                //衰减打开，放大关闭
            {
                *pu16RelayVal |= CH2_DEV_RY_ATT;
                *pu16CH2RelayStatus = AMP_CAL_ATT_ON_AMP_OFF;
            }
            else if (u32Amp < 1600 && u32Amp >= 160)         //衰减关闭，放大关闭
            {
                *pu16RelayVal |= 0;
                *pu16CH2RelayStatus = AMP_CAL_ATT_OFF_AMP_OFF;
            }
            else                                        //衰减关闭，放大打开
            {
                *pu16CH2RelayStatus = AMP_CAL_ATT_OFF_AMP_ON;
                *pu16RelayVal |= CH2_DEV_RY_AMP;
            }
        }
        else
        {
            if (u32Amp < 900)
            {
                *pu16RelayVal |= CH2_DEV_RY_ATT;
                *pu16RelayVal |= CH2_DEV_RY_AMP;
                *pu16CH2RelayStatus = AMP_CAL_ATT_ON_AMP_ON;
            }
            else
            {
                *pu16RelayVal |= CH2_DEV_RY_AMP;
                *pu16CH2RelayStatus = AMP_CAL_ATT_OFF_AMP_ON;
            }
        }
    }
    else
    {
        //如果直流电压小于900mV时，衰减和放大均关闭，否则放大打开
        if (s32Offset <= 900 && s32Offset >= -900)
        {
            *pu16RelayVal |= 0;
            *pu16CH2RelayStatus = DC_CAL_ATT_OFF_AMP_OFF;
        }
        else
        {
            *pu16RelayVal |= CH2_DEV_RY_AMP;
            *pu16CH2RelayStatus = DC_CAL_ATT_OFF_AMP_ON;
        }
    }

    if (stBasicPara.bSourceEnable)
    {
        *pu16RelayVal |= CH2_DEV_RY_OUT;
    }
}
