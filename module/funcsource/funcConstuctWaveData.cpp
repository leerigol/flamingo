/*****************************************************************************
				普源精电科技有限公司版权所有(2004-2010)
******************************************************************************
源文件名: funcConstuctWaveData.c
功能描述: 生成波表函数
作 者: KarlChen
版 本: 1.0
创建日期:2012-07-31
修改历史:
   <作者> 		  <修改时间> 		<版本>		 <修改描述>
               
  KarlChen         12/07/31          1.0          build this moudle
*****************************************************************************/
#include <math.h>
#include <stdlib.h>
#include "ComStru.h"
#include<QDebug>
/*******************************外部变量声明 *******************************/

/*------------------------------- 局部常数和类型定义 -----------------------*/

/*------------------------------- 局部宏定义 -------------------------------*/
#define FUN_2PI                  (3.1415926 * 2.0)
#define RISE_TIME                 (f32)(0.000000000011)
/*------------------------------- 局部变量 ---------------------------------*/
/*------------------------------- 局部函数原形 -----------------------------*/

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 函数实现 $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*******************************************************************************
 函 数 名:    // funcConstuctSineData
 描    述:  构建Sine波波表(四分之一波长)
 输入参数:  *           参数名 				类型 					描述
			*        		           
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
s16 funcConstuctSineData(u16 *pu16WaveData, u16 u16WaveAmp, u32 u32WaveSize)
{
	s16 s16Err = ERR_FUNC_SOURCE_NONE;
	u32 u32i;
	f64 f64Sin1;
	u16 u16WaveData;
	
	for (u32i = 0; u32i< u32WaveSize; u32i++)
    {       
       f64Sin1 = sinf(FUN_2PI * (u32i) / u32WaveSize / 4);
       
       /* 将sinf计算出的值由[-1:1]归一化到[0:Amp]范围内 */
       u16WaveData = (f64Sin1 + 1) * u16WaveAmp / 2;
       pu16WaveData[u32i] = u16WaveData;
    }

	return s16Err;
}
/*******************************************************************************
 函 数 名:    // funcConstuctSineModData
 描    述:  构建Sine波波表(用于调制波表)
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
s16 funcConstuctSineModData(u16 *pu16WaveData, u16 u16WaveAmp, u32 u32WaveSize)
{
    s16 s16Err = ERR_FUNC_SOURCE_NONE;
    u32 u32i;
    f64 f64Sin1;
    u16 u16WaveData;

    for (u32i = 0; u32i< u32WaveSize; u32i++)
    {
       f64Sin1 = sinf(FUN_2PI * (u32i) / u32WaveSize);

       /* 将sinf计算出的值由[-1:1]归一化到[0:Amp]范围内 */
       u16WaveData = (f64Sin1 + 1) * u16WaveAmp / 2;
       pu16WaveData[u32i] = u16WaveData;
    }

    return s16Err;
}
/*******************************************************************************
 函 数 名:    // funcConstuctPulseData
 描    述:  构建Pulse波波表（方波波表也基于此）
 输入参数:  *           参数名 				类型 					描述
			*        		           
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
s16 funcConstuctPulseData(u16 *pu16WaveData, u16 u16WaveAmp, u32 u32WaveSize, u32 u32Duty, u64 u64Freq)
{
	s16 s16Err = ERR_FUNC_SOURCE_NONE;

    if(u64Freq >= 1000000000000)
    {
#if 1
        u32 u32HighSize;
        u32 u32RiseSize;
        u32 u32LowSize;

        f32 f32Step;
        u32 u32i;
        f32 f32RiseTime;
        u32 u32dif;

        f32RiseTime = 0.000000000015;
        u32RiseSize = f32RiseTime * (f32)u64Freq * 16;

        if (u32RiseSize < 8)
        {
            //u32RiseSize = u32RiseSize>1 ? u32RiseSize:2;
            //u32RiseSize = u32RiseSize>0 ? u32RiseSize:2;
            u32RiseSize = u32RiseSize>4?u32RiseSize:3;
            if(u64Freq <= 7000000000)
            {
                u32RiseSize = 2;
            }
            f32Step = (f32)u16WaveAmp/(u32RiseSize -1);
        }
        else
        {
            //计算出偏差较小的上升时间
            for(u32i = 0;u32i< u32RiseSize - 1; u32i++)
            {
                u32dif = u32RiseSize - u32i - 1;
                if(((16383.0 / (f32)u32dif - 16383 / u32dif) < 0.1) && ((16383.0 / (f32)u32dif - 16383 / u32dif) > 0.005))
                {
                    break;
                }
            }
            if(u32dif<2)
            {
                for(u32i = 0;u32i< u32RiseSize - 1; u32i++)
                {
                    u32dif = u32RiseSize - u32i - 1;
                    if(((16383.0 / (f32)u32dif - 16383 / u32dif) < 0.3) && ((16383.0 / (f32)u32dif - 16383 / u32dif) > 0.005))
                    {
                        break;
                    }
                }
            }
            if(u32dif<2)
            {
                for(u32i = 0;u32i < u32RiseSize - 1; u32i++)
                {
                    u32dif = u32RiseSize - u32i - 1;
                    if(((16383.0 / (f32)u32dif - 16383 / u32dif) < 0.5) && ((16383.0 / (f32)u32dif - 16383 / u32dif) > 0.005))
                    {
                        break;
                    }
                }
            }
            u32RiseSize = u32dif + 1;
            f32Step  = (f32)u16WaveAmp / (f32)u32dif;
        }

        if (u32Duty > 1000) return ERR_FUNC_SOURCE_PARA;
        u32HighSize = (u32WaveSize * u32Duty / 1000) - u32RiseSize;
        u32LowSize = u32WaveSize - 2 * u32RiseSize - u32HighSize;
    
        if ((u32LowSize + u32HighSize) > u32WaveSize) return ERR_FUNC_SOURCE_PARA;
         //上升沿
        for (u32i = 0; u32i< u32RiseSize; u32i++)
        {
            pu16WaveData[u32i] = (u16)(f32Step * u32i);
        }

        //高电平
        for(;u32i < (u32RiseSize + u32HighSize); u32i++)
        {
            pu16WaveData[u32i] = u16WaveAmp;
        }

        // 下降沿
        for(;u32i < (u32WaveSize - u32LowSize); u32i++)
        {
            pu16WaveData[u32i] = (u16)(u16WaveAmp - f32Step * (u32i - u32HighSize - u32RiseSize));
        }
	
        // 低电平
        for(;u32i < u32WaveSize; u32i++)
        {
            pu16WaveData[u32i] = 0;
        }

#else
        u32    i;
        u32    j;
        u32    u32DutySize; /* 占空点数 */
        f32     f32WaveDuty = (f32)u32Duty * 0.001;

        //yqs 2011-06-24 change:限制方波边沿为5ns
        s32  s32Leading;
        s32  s32L;
        s32  s32T;
        f32  f32Step;

        f32 f32StepK = 0.75;
        f32 f32Step2 = 0.0;
        s32Leading = 150;
        s32L = s32Leading*16384/(1e18/u64Freq);
        if(s32L<= 2)
            s32L = 2;
         s32T = s32L;

         f32Step = (f32)(f32StepK * u16WaveAmp) / (f32)(s32L/2-1);

         for (i = 0;i < (u32)s32L/2; i++)
         {
             pu16WaveData[i] = (u16)(f32Step * i);
         }
         u16 u16Temp = pu16WaveData[i - 1];
         f32Step2 = u16WaveAmp*(1 - f32StepK) / (f32)(s32L/2);
         for(j=1;j<=(u32)s32L/2;j++,i++)
         {
             pu16WaveData[i] = u16Temp + (u16)(f32Step2*j);
         }
         u32DutySize = u32WaveSize * f32WaveDuty - (s32L + s32T)/2;

         /* 高电平 */
         for ( j = 0; j < u32DutySize; i++,j++)
         {
             pu16WaveData[i] = u16WaveAmp;
         }

       //  f32Step = (f32)u16WaveAmp / (f32)(s32T-1);
       //  for (j = 0;j < (u32)s32T;j++,i++)
       //  {
         //     pu16WaveData[i] = u16WaveAmp - (u16)(f32Step * j);
        // }

        // ....................................
         f32Step = (f32)f32StepK*u16WaveAmp / (f32)(s32T/2 - 1);

         for(j = 0;j<(u32)s32T/2;j++,i++)
         {
             pu16WaveData[i] = u16WaveAmp - (u16)(f32Step*j);
         }

         u16Temp = pu16WaveData[i - 1];

         f32Step2 = u16WaveAmp*(1 - f32StepK) / (f32)(s32T/2);

         for(j=1;j<=(u32)s32T/2;j++,i++)
         {
             pu16WaveData[i] =  u16Temp - (u16)(f32Step2*j);
         }
         //.................................

         /* 低电平 */
         for ( ; i < u32WaveSize; i++)
         {
             pu16WaveData[i] = 0;
         }

#endif
    }
    else
    {
        u32    i;
        u32    j;
        u32    u32DutySize; /* 占空点数 */
        f32     f32WaveDuty = (f32)u32Duty * 0.001;

        //yqs 2011-06-24 change:限制方波边沿为5ns
        s32  s32Leading;
        s32  s32L;
        s32  s32T;
        f32  f32Step;

        if (u64Freq != 0)
        {
            //m_stBasic.s64Freq_Menu = 1e18/p
            /* 当周期不为0时进行赋值和运算 Han Hongrui 11.12.27 Add */
             s32Leading = 150;
             s32L = s32Leading*16384/(1e18/u64Freq);
             s32L = s32L * 1.3;
             if(s32L<= 2)
                 s32L = 2;
              s32T = s32L;
#if 1
             f32Step = (f32)u16WaveAmp / (f32)(s32L-1);

             for (i = 0;i < (u32)s32L; i++)
             {
                 pu16WaveData[i] = (u16)(f32Step * i);
             }
             u32DutySize = u32WaveSize * f32WaveDuty - (s32L + s32T)/2;

             /* 高电平 */
             for ( j = 0; j < u32DutySize; i++,j++)
             {
                 pu16WaveData[i] = u16WaveAmp;
             }

             f32Step = (f32)u16WaveAmp / (f32)(s32T-1);
             for (j = 0;j < (u32)s32T;j++,i++)
             {
                  pu16WaveData[i] = u16WaveAmp - (u16)(f32Step * j);
             }

             /* 低电平 */
             for ( ; i < u32WaveSize; i++)
             {
                 pu16WaveData[i] = 0;
             }
#else
             f32Step = (f32)u16WaveAmp / (f32)(s32L/2-1);

             for (i = 0;i < (u32)s32L/2; i++)
             {
                 pu16WaveData[i] = (u16)(f32Step * i);
             }
             u16 u16Temp = pu16WaveData[i - 1];
             f32 f32Step2 = u16WaveAmp*(1 - 0.75) / (f32)(s32L/2);
             for(j=1;j<=(u32)s32L/2;j++,i++)
             {
                 pu16WaveData[i] = u16Temp + (u16)(f32Step2*j);
             }
             u32DutySize = u32WaveSize * f32WaveDuty - (s32L + s32T)/2;

             /* 高电平 */
             for ( j = 0; j < u32DutySize; i++,j++)
             {
                 pu16WaveData[i] = u16WaveAmp;
             }

             f32Step = (f32)u16WaveAmp / (f32)(s32T-1);
             for (j = 0;j < (u32)s32T;j++,i++)
             {
                  pu16WaveData[i] = u16WaveAmp - (u16)(f32Step * j);
             }

             /* 低电平 */
             for ( ; i < u32WaveSize; i++)
             {
                 pu16WaveData[i] = 0;
             }
#endif
        }
    }
	return s16Err;
}
/*******************************************************************************
 函 数 名:    // funcConstuctRampData
 描    述:  构建Ramp波波表
 输入参数:  *           参数名 				类型 					描述
			*        		           
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
s16 funcConstuctRampData(u16 *pu16WaveData, u16 u16WaveAmp, u32 u32WaveSize, u32 u32Symmetry)
{
	s16 s16Err = ERR_FUNC_SOURCE_NONE;
	u32 u32RiseSize = 0;        //上升沿所占波表点个数
	f32 f32RiseStep = 0.0;
	f32 f32FallStep = 0.0;
	f32 f32CurAmp = 0.0;
	u32 u32i;
	
	u32RiseSize = u32Symmetry * u32WaveSize / 2000;
	
    //!构筑前 1/4 个周期（上升沿的上半部分）
	if (u32Symmetry != 0)
	{
		f32RiseStep = 0.5 * (f32)u16WaveAmp / (f32)u32RiseSize;
	}

    if(u32Symmetry >= 20)
    {
        for(u32i = 0; u32i < u32RiseSize; u32i++)
        {
            pu16WaveData[u32i] = (u16)(f32CurAmp + (u16WaveAmp>>1));
            f32CurAmp += f32RiseStep;
        }
        //! 为了防止误差，将最高点保证为幅度
        if (u32RiseSize > 0)
        {
            pu16WaveData[u32RiseSize - 1] = u16WaveAmp;
        }
        f32CurAmp = (f32)u16WaveAmp;
	
        //! 构建中间的1/4到3/4（下降沿）
        if (u32Symmetry != 1000)
        {
            f32FallStep = (f32)u16WaveAmp / (f32)(u32WaveSize - (f32)(u32RiseSize<<1));
        }
        for (; u32i < (u32WaveSize - u32RiseSize); u32i++)
        {
            if(f32CurAmp < 0)
            {
                f32CurAmp = 0;
            }
            pu16WaveData[u32i] = f32CurAmp;
            f32CurAmp -= f32FallStep;
        }

        //! 构建后1/4个周期(上升沿的前半部分)
        for(;u32i < u32WaveSize; u32i++)
        {
            if(f32CurAmp < 0)
            {
                f32CurAmp = 0;
            }
            pu16WaveData[u32i] = (u16)(f32CurAmp);
            f32CurAmp += f32RiseStep;
        }
    }
    else
    {
        for(u32i = 0; u32i < u32RiseSize * 2; u32i++)
        {
            pu16WaveData[u32i] = (u16)(f32CurAmp);
            f32CurAmp += f32RiseStep;
        }
        if (u32RiseSize > 0)
        {
            pu16WaveData[u32RiseSize * 2 - 1] = u16WaveAmp;
        }
        f32CurAmp = (f32)u16WaveAmp;

        //! 构建中间的1/4到3/4（下降沿）
        if (u32Symmetry != 1000)
        {
            f32FallStep = (f32)u16WaveAmp / (f32)(u32WaveSize);
        }
        for (; u32i < u32WaveSize; u32i++)
        {
            if(f32CurAmp < 0)
            {
                f32CurAmp = 0;
            }
            pu16WaveData[u32i] = f32CurAmp;
            f32CurAmp -= f32FallStep;
        }

    }
	return s16Err;
}
/*******************************************************************************
 函 数 名:    // funcConstuctDCData
 描    述:  构建DC波表
 输入参数:  *           参数名 				类型 					描述
			*        		           
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
s16 funcConstuctDCData(u16 *pu16WaveData,u32 u32WaveSize)
{
	s16 s16Err = ERR_FUNC_SOURCE_NONE;
	u32 u32i;
	
	for (u32i = 0; u32i < u32WaveSize; u32i++)
	{
#if 1
        pu16WaveData[u32i] = (WAVE_AMP)/2;
 #else
        pu16WaveData[u32i] = 0;
  #endif
	}
	
	return s16Err;	
}
/*******************************************************************************
 函 数 名:    // funcConstuctNoiseData
 描    述:  构建Noise波表
 输入参数:  *           参数名 				类型 					描述
			*        		           
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
s16 funcConstuctNoiseData(u16 *pu16WaveData,u32 u32WaveSize)
{
	s16 s16Err = ERR_FUNC_SOURCE_NONE;
	u32 u32i;
	for (u32i = 0; u32i < u32WaveSize; u32i++)
	{
		pu16WaveData[u32i] = rand();
	}
	return s16Err;
}
/*---------------------------------- 文件结束 --------------------------------*/
