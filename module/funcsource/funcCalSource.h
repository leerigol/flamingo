/*****************************************************************************
    普源精电科技有限公司版权所有(2004-2010)
******************************************************************************
头文件名: funcCalSource.h
功能描述: 信号源校准保存头文件
作    者: KarlChen
版    本: 1.0
完成日期:2012-8-13
修改历史： 
           <作者>   <修改时间>   <版本>    <修改描述>
          KarlChen   12/08/13     1.0   build this moudle
*****************************************************************************/
#ifndef _FUNC_CAL_SOURCE_H
#define _FUNC_CAL_SOURCE_H
/*------------------------------ 包含文件声明 --------------------------------*/
#include "../devdgu/devDgu.h"


/*------------------------------ 外部变量声明 --------------------------------*/


/*------------------------------ 常数和类型声明 ------------------------------*/


/*-------------------------------- 宏声明 -----------------------------------*/
//定义模拟前端的分段
#define AMP_CAL_ATT_ON_AMP_OFF                     0
#define AMP_CAL_ATT_OFF_AMP_OFF                    1
#define AMP_CAL_ATT_OFF_AMP_ON                     2  
#define AMP_CAL_ATT_ON_AMP_ON                      3 
#define DC_CAL_ATT_OFF_AMP_OFF                     4
#define DC_CAL_ATT_OFF_AMP_ON                      5

//校准参数
#define SOURCE_CAL_FACTOR_A                               0
#define SOURCE_CAL_FACTOR_B                               1

#define CAL_DG_DATA_FILE            "/rigol/data/calDg.bin"
/*-------------------------------- 变量声明 ---------------------------------*/ 

//通道参数枚举
typedef struct
{
	s16 s16PhaseDev;                      //相位相差
	u16 u16VCTOXFactor;                   //频率因子
	s16 s16CH1OffsetCalFactor[6][2];     //Offset校准参数
	s16 s16CH2OffsetCalFactor[6][2];     //Offset校准参数	
	s16 s16CH1AmpCalFactor[4][2];         //Amp校准参数
	s16 s16CH2AmpCalFactor[4][2];         //Amp校准参数
}SourceCalParaStru;
/*--------------------------------- 函数原形 --------------------------------*/
/*******************************************************************************
 函 数 名:    //funcSetSourceCalParaDefault
 描    述:    设计校准参数为默认
 输入参数:  *           参数名 				类型 					描述
			*       
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  数据长度
 说    明:                               			   
*******************************************************************************/
void funcSetSourceCalParaDefault( void );
/*******************************************************************************
 函 数 名:    //funcCalSourceVCTXO
 描    述:    校准信号源频率
 输入参数:  *           参数名 				类型 					描述
			*       
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
void funcCalSourceVCTXO(void);
/*******************************************************************************
 函 数 名:    //funcSetSourcePhaseDev
 描    述:    设置相位偏差
 输入参数:  *           参数名 				类型 					描述
			*       s16PhaseDev             s16                  相位偏差
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
void funcSetSourcePhaseDev(s16 s16PhaseDev);
/*******************************************************************************
 函 数 名:    //funcSetSourceVCTXO
 描    述:    设置VCTXO的数值
 输入参数:  *           参数名 				类型 					描述
			*       
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
void funcSetSourceVCTXO(u16 u16VCTOXFactor);

/*******************************************************************************
 函 数 名:    //funcSetSourceOffsetCalFactor
 描    述:    设置偏移校准系数
 输入参数:  *           参数名 				类型 					描述
			*          bSource1            bool                   通道，true为通道1
			           u16Stauts           u16                    通道状态
			                                                      AMP_CAL_ATT_ON_AMP_OFF
			                                                      AMP_CAL_ATT_OFF_AMP_OFF
			                                                      AMP_CAL_ATT_OFF_AMP_ON
			                                                      AMP_CAL_ATT_ON_AMP_ON
			                                                      DC_CAL_ATT_OFF_AMP_OFF
			                                                      DC_CAL_ATT_OFF_AMP_ON
			           s16FactorA          s16                    校准因数A  
			           s16FactorB          s16                    校准因数B                                         
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
void funcSetSourceOffsetCalFactor(bool bSource1, u16 u16Stauts, s16 s16FactorA, s16 s16FactorB);
/*******************************************************************************
 函 数 名:    //funcSetSourceAmpCalFactor
 描    述:    设置幅度校准系数
 输入参数:  *           参数名 				类型 					描述
			*         bSource1            bool                   通道，true为通道1
			          u16Stauts           u16                    通道状态
			                                                      AMP_CAL_ATT_ON_AMP_OFF
			                                                      AMP_CAL_ATT_OFF_AMP_OFF
			                                                      AMP_CAL_ATT_OFF_AMP_ON
			                                                      AMP_CAL_ATT_ON_AMP_ON
			           s16FactorA          s16                    校准因数A  
			           s16FactorB          s16                    校准因数B  
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
void funcSetSourceAmpCalFactor(bool bSource1, u16 u16Stauts, s16 s16FactorA, s16 s16FactorB);
/*******************************************************************************
 函 数 名:    //funcGetSourceCalPhase
 描    述:    获取信号源相位校准参数
 输入参数:  *           参数名 				类型 					描述
			*       
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  相位校准参数
 说    明:                               			   
*******************************************************************************/
s16 funcGetSourceCalPhase(void);
/*******************************************************************************
 函 数 名:    //funcGetSourceCalVCTXO
 描    述:    获取信号源频率校准参数
 输入参数:  *           参数名 				类型 					描述
			*       
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  频率校准系数
 说    明:                               			   
*******************************************************************************/
u16 funcGetSourceCalVCTXO(void);
/*******************************************************************************
 函 数 名:    //funcGetSourceCalOffset
 描    述:    获取信号源偏移校准参数（包括DC）
 输入参数:  *           参数名 				类型 					描述
			*          bChannel1            bool                  通道选择 ture为通道1
			         u8ChannelStatas         u8                   模拟前端状态
			                                                 DC_CAL_ATT_OFF_AMP_OFF
			                                                 DC_CAL_ATT_OFF_AMP_ON
			                                                 AMP_CAL_ATT_ON_AMP_OFF
			                                                 AMP_CAL_ATT_OFF_AMP_OFF
			                                                 AMP_CAL_ATT_OFF_AMP_ON
			                                                 AMP_CAL_ATT_ON_AMP_ON
			                                                 
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  无
 说    明:                               			   
*******************************************************************************/
void funcGetSourceCalOffset(bool bChannel1, u8 u8ChannelStatas, s16 *ps16FactorA, s16 *ps16FactorB);
/*******************************************************************************
 函 数 名:    //funcGetSourceCalAmp
 描    述:    获取信号源幅度校准参数
 输入参数:  *           参数名 				类型 					描述
			*       bChannel1            bool                  通道选择 ture为通道1
			        u8ChannelStatas         u8                   模拟前端状态
			                                                 DC_CAL_ATT_OFF_AMP_OFF
			                                                 DC_CAL_ATT_OFF_AMP_ON
			                                                 AMP_CAL_ATT_ON_AMP_OFF
			                                                 AMP_CAL_ATT_OFF_AMP_OFF
			                                                 AMP_CAL_ATT_OFF_AMP_ON
			                                                 AMP_CAL_ATT_ON_AMP_ON
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  无
 说    明:                               			   
*******************************************************************************/
void funcGetSourceCalAmp(bool bChannel1, u8 u8ChannelStatas, s16 *ps16FactorA, s16 *ps16FactorB);
/*******************************************************************************
 函 数 名:    funcUtiLoadSourceData
 描    述:    加载SOURCE的校准参数
 输入参数:  *           参数名 				类型 					描述
			*       
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  s32 < 0 错误， 0 成功
 说    明:                               			   
*******************************************************************************/
s32 funcUtiLoadSourceData( u16 *pu16Stream, ///< 存储的数据流
						   u32 u32Len ); 	///< 输出数据流,BYTEs

/*******************************************************************************
 函 数 名:    funcUtiSaveSourceData
 描    述:    存储SOURCE的校准参数
 输入参数:  *           参数名 				类型 					描述
			*       
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  s32 < 0 错误， 0 成功
 说    明:                               			   
*******************************************************************************/
s32 funcUtiSaveSourceData( u16 *pu16Stream,	///< 加载的数据流
						   u32 u32Len );	///< 加载的数据长度 BYTEs

/************************get cal data pointer and cal data****************************/
SourceCalParaStru* getSourCalPara();
void setSourCalPara(SourceCalParaStru sourcaltemp);
#endif
/*---------------------------------- 文件尾 ---------------------------------*/
