#ifndef FUNCBURSTCONFIG
#define FUNCBURSTCONFIG
#include "ComStru.h"
#include "../devdgu/devDgu.h"

/**********************************局部宏定义**********************************/
/* BURST寄存器中负责同步参数设置的位域掩码:bit[9:7] */
#define    FUN_BURST_REG_SYNC_MASK               0x0380

/***********************************局部变量***********************************/
//! add_by_zx 看不懂
/* 定义用于控制FUN_CTRL_BURST_REG的命令变量，由于该组寄存器的16个位域可控制两组
   参数：BURST参数和同步参数。设置BURST参数时，首先用BURST掩码清空负责BURST参数
   的位域，再或上要设置的参数，从而不影响同步参数原来的设置。反之，设置同步参数
   时，也先用同步掩码清空负责同步参数的位域，再或上同步的设置，从而不影响以往设
   置的BURST参数。这样，不用每次设置BURST参数时都判断同步参数的设置，也不用每次
   设置同步时都判断BURST参数 */

#if 0
/******************************************************************************
函数名称：funBurstGateConfig
描    述: 配置门控Burst。
输入参数：
          参数名称        参数类型                参数说明
          stBurstPara     comBurstParaStru        Burst相关参数
          euChannel       devFpgaChanEu               要配置的通道
输出参数：无
返 回 值：错误代码
说    明：内部函数。
******************************************************************************/
s32  funBurstGateConfig(comBurstParaStru  stBurstPara,
                                comBaseWaveEu  euCarrierWaveID,
                                comBurstParaCfgBmpStru  stBurstParaCfgBmp,
                                devFpgaChanEu  euChannel);
#endif

#endif // FUNCBURSTCONFIG

