
/*******************************************************************************
                普源精电科技技术有限公司版权所有(2008-2014)
********************************************************************************/
#include "funcSweepConfig.h"

s32  BurstConfig::funBurstManualTrig(devFpgaChanEu  euChannel, bool bInfiDisp)
{
    u16 randVal = 0;
    s32 s32RetCode = devTrigManual( &randVal,euChannel);
    //! burst 的手动无限触发模式下，如果已经显示了波形，则需要关闭，跳过复位
    if(!bInfiDisp)
    {
        s32RetCode = devBurstRstReg(euChannel);
    }
    return s32RetCode;
}

s32  BurstConfig::funBurstParaConfig(comBurstParaStru  stBurstPara,devFpgaChanEu  euChannel)
{
    s32  s32RetCode = dg_err_none;
        /* 否则说明是Burst参数或者基本波参数修改引起的配置 */
        /* 以下情况下需要重配基本波:
           (1) 输出变化模式发生
           (2) 当前不是处于BURST界面(不处于Burst界面而收到Burst消息，
               则说明是由于基本波参数修改引起的配置)
           (3) 载波发生变化 */
#if 0
        //! 基本波配置
        if (fun_astRunTimeState[euChannel].bWaveNeedCfg == true
            || *(u16*)&fun_stBasicParaCfgBmp[euChannel] != 0)
        {
            funBasicParaConfig(fun_stBasicPara[euChannel],
                              fun_stSystemState,
                              euChannel);
        }
#endif
        //! 根据burst类型，配置burst参数
        switch (stBurstPara.euBurstType)
        {
            case BURST_NCYCLE:      /* N循环 */
                //! 删去外部触发设置触发源操作
            funBurstNCycleConfig(stBurstPara, euChannel);
               break;

            case BURST_INFINITE:    /* 无限 */
#if 0
            //! 外部触发？
                //sxm 2013.3.11 change 输出为常规时才切外部继电器
                if(fun_stSystemInfo.stOutput[euChannel].bOutputMode)
                {
                     funExtTrigSet(euChannel, TRIG_EXTERNAL,TRIG_OFF);
                }
                else
                {
                     /* 设置触发源 */
                     funExtTrigSet(euChannel, stBurstPara.stTrigger.euTrig_Source,
                               stBurstPara.stTrigger.euTrig_Out);

                }
#endif
                funBurstInfConfig(stBurstPara, euChannel);
                break;
        }

    //配置完Burst参数后，设置Burst复位寄存器使设置的参数有效
    devBurstRstReg(euChannel);

    return  s32RetCode;
}

u16 BurstConfig::funGetTrigVal(comBurstParaStru stBurstPara)
{
    u16 trigMode = 0;
    if(stBurstPara.stTrigger.euTrig_Source == Trig_INTERNAL)
    {
        trigMode = trigMode | DEV_INT_TRIG_EN;
    }
    else if(stBurstPara.stTrigger.euTrig_Source == Trig_MANUAL)
    {
        trigMode = trigMode | DEV_MANUAL_TRIG_EN;
    }

    //! to do：方波的burst需要单独使能
    //! 除方波外的burst使能
    if(stBurstPara.bBurstEn) //! burst 打开
    {
        LOG_DBG()<<stBurstPara.bBurstEn;

        trigMode = trigMode | DEV_PULSE_BURST_EN;
        trigMode = trigMode | DEV_BURST_EN;
    }
    else
    {
          trigMode = trigMode & (~ DEV_BURST_EN);   
    }

    if(stBurstPara.euBurstType == BURST_INFINITE)
    {
        trigMode = trigMode | DEV_BURST_INF_EN;
    }
    return trigMode;
}

s32  BurstConfig::funBurstNCycleConfig(comBurstParaStru  stBurstPara, devFpgaChanEu  euChannel)
{
    /* 配置Burst寄存器:配置Burst类型、配置触发源 */
    u16 u16BurstRegCmd = funGetTrigVal(stBurstPara);
    devTrigMod(&u16BurstRegCmd, euChannel);  //在这个位置

    s32  s32RetCode = dg_err_none;
    /* 配置延迟参数 */
    //! 以10ns为单位
    u64 u64DelayVal = stBurstPara.s64Delay * 1e8/1e12;
    //qDebug() << "u64DelayVal = " << u64DelayVal;
    devBurstDelay(&u64DelayVal, euChannel);

    /* 配置循环数 */
    u32 u32CycleVal = stBurstPara.s32Cycle - 1;
    devBurstCycles(&u32CycleVal, euChannel);

    /* 配置循环周期 */
    if((stBurstPara.stTrigger.euTrig_Source == Trig_INTERNAL))
    {
        //! 以1us为单位
        u64 u64period = stBurstPara.stTrigger.s64Trig_Interval * 1e6 /1e12;
        devBurstPeriod(&u64period, euChannel);
    }

    /* 配置Burst寄存器:配置Burst类型、配置触发源 */
   // u16 u16BurstRegCmd = funGetTrigVal(stBurstPara);
    //devTrigMod(&u16BurstRegCmd, euChannel);  //在这个位置

    return  s32RetCode;
}

s32  BurstConfig::funBurstInfConfig(comBurstParaStru  stBurstPara, devFpgaChanEu  euChannel)
{
    s32  s32RetCode = dg_err_none;

    /* 配置Burst寄存器:配置Burst类型、配置触发源 */
    u16 u16BurstRegCmd = funGetTrigVal(stBurstPara);
    devTrigMod(&u16BurstRegCmd, euChannel);

    /* 配置延迟参数 */
    //! 以10ns为单位
    u64 u64DelayVal = stBurstPara.s64Delay * 1e8 / 1e12;
    devBurstDelay(&u64DelayVal, euChannel);

    /* 配置循环数 */
    u32 u32CycleVal = 0;
    devBurstCycles(&u32CycleVal, euChannel);

    return  s32RetCode;
}
/************************************文件尾************************************/
