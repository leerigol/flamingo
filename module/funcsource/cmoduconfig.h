#ifndef CMODUCOFIG_H
#define CMODUCOFIG_H

#include "../devdgu/devDgu.h"
#include "ComStru.h"


/* 计算AM调制偏移:X为调制深度，单位为0.01 */
#define   FUN_CTRL_AM_DEV(X)                 ((f32)X/100*0x800000000000)
/* AM改成内部实现，则计算公式有所变化:由逻辑给出，X为调制深度，单位为0.00001,
   包括百分比, */
#define   FUN_CTRL_AM_COMP(X)                ((X<=100)?    \
                                             (0xc000 - (f32)X/100*0x4000):\
                                             (0xFFFF-2*(f32)X/100*0x4000))

class CModuConfig
{
public:
    s16 funcSetSourceModEnable(devFpgaChanEu chan, comBasicParaStru stBasicPara,
                               comModuParaStru stModuPara);
    s16 funcSetSourceModPara(devFpgaChanEu chan, comBasicParaStru stBasicPara,
                           comModuParaStru stModuPara);
private:
    s16 funcSetSourceModWave(devFpgaChanEu chan, comModuParaStru stModuPara);
    s16 funcSetSourceModFreq(devFpgaChanEu chan, u64 u64ModFreq, SourceModWave moduWave);
    s16 funcSetSourceHopFreq(devFpgaChanEu chan, u64 u64HopFreq);
    s16 funcSetSourceModAMDepth(devFpgaChanEu chan, s8 u8AMDepth, bool moduEn);
    s16 funcSetSourceModFMDev(devFpgaChanEu chan, comBasicParaStru stBasicPara, comModuParaStru stModuPara);

    u16 funcGetSourceSysCtrlReg(devFpgaChanEu, comBasicParaStru stBasicPara, comModuParaStru stModuPara);
};

#endif // CMODUCOFIG_H
