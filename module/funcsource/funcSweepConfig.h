#ifndef FUNCSWEEPCONFIG
#define FUNCSWEEPCONFIG
#include "ComStru.h"
#include "../devdgu/devDgu.h"

//! 配置扫频的波形参数
class SweepConfig
{
public:
    s32  funcSweepParaConfig(comSweepParaStru  stSweepPara, devFpgaChanEu  euChannel);
    s32  funcSweepTrigManual(devFpgaChanEu  euChannel);

    bool m_bUpflag;
private:
    s32  funcConstructLine(u16 *pu16WaveData);
    s32  funcConstructLog(u16 *pu16WaveData, s64 s64StartFreq, s64 s64StopFreq);
    s32 funcConstStepData(u16 *pu16WaveData, int step);
    s32  funcSweepWaveConfig(comSweepParaStru  stSweep, devFpgaChanEu  euChannel);
    u16  funcGetSweepStageMode(comSweepParaStru stSweepPara);
    u16  funcGetSweepTrigVal(comSweepParaStru stSweepPara);

};

class BurstConfig
{
public:
    s32  funBurstManualTrig(devFpgaChanEu  euChannel,bool bInfiDisp = false);
    s32  funBurstParaConfig(comBurstParaStru  stBurstPara, devFpgaChanEu euChannel);
private:
    s32  funBurstNCycleConfig(comBurstParaStru  stBurstPara, devFpgaChanEu  euChannel);
    s32  funBurstInfConfig(comBurstParaStru  stBurstPara, devFpgaChanEu  euChannel);
    u16  funGetTrigVal(comBurstParaStru stBurstPara);
};
#endif
