#ifndef COMSTRU
#define COMSTRU
#include "../devdgu/data_type.h"


//信号波形类型
typedef enum
{
    WAVE_SINE,
    WAVE_SQUARE,
    WAVE_RAMP,
    WAVE_PULSE,
    WAVE_DC,
    WAVE_NOISE,
    WAVE_ARB_INTERNAL,
    WAVE_ARB_EXTERNAL,
}SourceWaveformEnum;

//内建信号类型
typedef enum
{
    INTERNAL_WAVE_SINC,
    INTERNAL_WAVE_EXP_RISE,
    INTERNAL_WAVE_EXP_FAIL,
    INTERNAL_WAVE_CARDIAC,
    INTERNAL_WAVE_GAUSS,
    INTERNAL_WAVE_LORENTZ,
    INTERNAL_WAVE_HAVERSINE,

}SourceInternalWaveformEnum;

//调制类型
typedef enum
{
    MOD_AM,
    MOD_FM,
    MOD_FSK,
}SourceModType;

//调制波形
typedef enum
{
    MOD_WAVE_SINE,
    MOD_WAVE_SQUARE,
    MOD_WAVE_RAMP,
    MOD_WAVE_NOISE,
}SourceModWave;

#define ERR_FUNC_SOURCE_NONE                        0
#define ERR_FUNC_SOURCE_MALLOC_FAILED               -1
#define ERR_FUNC_SOURCE_PARA                        -2
#define ERR_FUNC_SOURCE_INVALID                     -3
/**
 * 定义波表大小
 */
#define WAVE_AMP                16383
#define WAVE_MOD_AMP            65535
#define WAVE_SIZE               0x4000
#define WAVE_MOD_SIZE           0x400

#define FUNC_DG_SINE_WAVE_SIZE       WAVE_SIZE
#define FUNC_DG_SQUARE_WAVE_SIZE     WAVE_SIZE
#define FUNC_DG_RAMP_WAVE_SIZE       WAVE_SIZE
#define FUNC_DG_PULSE_WAVE_SIZE      WAVE_SIZE
#define FUNC_DG_DC_WAVE_SIZE         WAVE_SIZE
#define FUNC_DG_NOISE_WAVE_SIZE      WAVE_SIZE
#define FUNC_DG_ECG_WAVE_SIZE        WAVE_SIZE
#define FUNC_DG_EXPFALL_WAVE_SIZE    WAVE_SIZE
#define FUNC_DG_EXPRISE_WAVE_SIZE    WAVE_SIZE
#define FUNC_DG_GAUSS_WAVE_SIZE      WAVE_SIZE
#define FUNC_DG_HAVERSINE_WAVE_SIZE  WAVE_SIZE
#define FUNC_DG_LORENTZ_WAVE_SIZE    WAVE_SIZE
#define FUNC_DG_SINC_WAVE_SIZE       WAVE_SIZE
#define FUNC_DG_ARB_WAVE_SIZE       WAVE_SIZE

#define FUNC_DG_SINE_MOD_WAVE_SIZE   WAVE_MOD_SIZE
#define FUNC_DG_SQUARE_MOD_WAVE_SIZE WAVE_MOD_SIZE
#define FUNC_DG_RAMP_MOD_WAVE_SIZE   WAVE_MOD_SIZE

//频率 以uHz为单位
#define FREQ_0_1_HZ           100000
#define FREQ_25_MHZ           25000000000000
#define FREQ_80_MHZ           80000000000000
#define FREQ_15_MHZ	          15000000000000
#define FREQ_10_MHZ	          10000000000000
#define FREQ_1_MHZ	          1000000000000
#define FREQ_100_KHZ	      100000000000
#define FREQ_1_KHZ            1000000000

enum enumTrigSRC
{
    Trig_INTERNAL = 1,
    Trig_EXTERNAL,
    Trig_MANUAL,
};

enum enumBurstType
{
    BURST_NCYCLE,
    BURST_INFINITE,
};

enum enumSweepType
{
    SWEEP_LINEAR,
    SWEEP_LOG,
    SWEEP_STEP,
};

enum enumBurstIdel
{
    //! 没有空闲电平设置
    none,
};

enum enumDGWorkMode
{
    NONE_MODE = 0,
    DG_MODU = 1,
    DG_SWEEP = 2,
    DG_BURST = 3,
};

typedef struct
{
    enumTrigSRC        euTrig_Source;        /* 触发源：内部，外部，手动 */
//    comTrigOutEu       euTrig_Out;           /* 触发输出（外部无效）方式：关闭，上升沿，下降沿 */
//    comSlopeInEu       euSlope_In;           /* 触发输入（外部有效）方式：上升沿，下降沿 */
    s64                s64Trig_Interval;     /* 触发间隔，在BURST模式下为脉冲周期 */

}TriggerStru;

/***** BURST参数的结构定义 *****/
typedef struct
{
    bool            bBurstEn;
    bool            bGatePolarityNeg;     /* Gated极性 ,0为正，1为负*/
    s32             s32Cycle;
    s32             s32IdelLevel;
    s64             s64Delay;             /* 延迟 */
    enumBurstType   euBurstType;          /* 突发模式为：N循环和门控 */
    TriggerStru     stTrigger;            /* BURST触发相关 */
    enumBurstIdel   euIdelLevelType;      /* BURST空闲电平 Han Hongrui 15.04.15 Add */
}comBurstParaStru;

/***** SWEEP参数的结构体 *****/
typedef struct
{
    bool            bSweepEn;
    enumSweepType   sweepType;
    long long       step;
    long long       sweepTime;
    long long       returnTime;
    long long       startKeepTime;
    long long       endKeepTime;
    long long       startFreq;
    long long       endFreq;
    TriggerStru     stTrigger;
}comSweepParaStru;

//通道参数枚举
typedef struct
{
    bool bSourceEnable                               :1;      //信号源源开关 true为打开
    bool bSourceDisplay                              :1;      //信号源状态显示
    SourceWaveformEnum enSourceWaveform;                      //信号源波形
    SourceInternalWaveformEnum enInterWaveform;               //内建波形类型
    s64 s64Freq_Menu;                                         //菜单上显示的信号频率
    s64 s64Freq;                                              //真实配下去的信号频率，以uHz为单位
    u32 u32Amp;                                               //信号幅度，以1mVpp为单位
    s32 s32Offset;                                            //信号偏移，以1mVpp为单位
    u16 u16Phase;                                             //信号初始相位，以0.1度为单位
    u16 u16Symmetry;                                          //锯齿波对称性，以0.1%为单位
    u16 u16Duty;                                              //脉冲占空比，以0.1%为单位
}comBasicParaStru;

// enumDGWorkMode euDgworkMode;                              //mode、burst、sweep状态，用于改变基本波时获取系统状态
typedef struct{
    bool bModEnable                                  ;      //调制开关，true为打开
    bool bSweepEnable                                ;
    SourceModType enModType                          ;      //调制类型
    SourceModWave enModWave                          ;      //调制波形
    u64 u64ModFreq;                                         //调制频率，以Hz为单位
    u8 u8AMDepth;                                           //AM调制深度 以1%为单位
    u64 u64FMDev;                                           //FM频率偏移，以Hz为单位
    bool isPolarity                                  ;      //FSK极性,false为正极性
    u64 u64HopFreq_menu;                                    //菜单上显示的跳频频率
    u64 u64HopFreq;                                         //FSK跳频频率
}comModuParaStru;

#endif
