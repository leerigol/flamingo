/*****************************************************************************
                普源精电科技有限公司版权所有(2004-2012)
******************************************************************************
源文件名: funcCalSource.c
功能描述: 信号源校验相关函数
作 者: KarlChen
版 本: 1.0
创建日期:2012-08-13
修改历史:
   <作者> 		  <修改时间> 		<版本>		 <修改描述>

  KarlChen         12/08/13          1.0          build this moudle
*****************************************************************************/

#include "data_type.h"
#include "funcCalSource.h"

#include <QFile>
#include <QDataStream>
/********************************外部变量声明 *******************************/


/*------------------------------- 局部常数和类型定义 -----------------------*/
SourceCalParaStru stSourceCalPara;

/****************************** 函数实现 *******************************************
 函 数 名:    //funcSetSourceCalParaDefault
 描    述:    设计校准参数为默认
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  数据长度
 说    明:
*******************************************************************************/
void funcSetSourceCalParaDefault( void )
{

    stSourceCalPara.s16PhaseDev = 0;
    //stSourceCalPara.u16VCTOXFactor = 950;
    stSourceCalPara.u16VCTOXFactor = 10000;

    devCoefFreq(&stSourceCalPara.u16VCTOXFactor);

    stSourceCalPara.s16CH1OffsetCalFactor[DC_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_A]=2490;
    stSourceCalPara.s16CH1OffsetCalFactor[DC_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_B]= 10;
    stSourceCalPara.s16CH1OffsetCalFactor[DC_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_A]= 14550;
    stSourceCalPara.s16CH1OffsetCalFactor[DC_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_B]= 110;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_A]=2489;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_B]=8;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_A]=2450;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_B]=24;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_A]=13152;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_B]=231;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_A]=14753;
    stSourceCalPara.s16CH1OffsetCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_B]=30;

    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_A] = 351;
    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_B] = 1;
    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_A] = 3480;
    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_B] = 5;
    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_A] = 20675;
    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_B] =40;
    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_A] = 2070;
    stSourceCalPara.s16CH1AmpCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_B] =2;

    stSourceCalPara.s16CH2OffsetCalFactor[DC_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_A]=2490;
    stSourceCalPara.s16CH2OffsetCalFactor[DC_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_B]= 10;
    stSourceCalPara.s16CH2OffsetCalFactor[DC_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_A]=14550;
    stSourceCalPara.s16CH2OffsetCalFactor[DC_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_B]= 110;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_A]=2492;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_B]=10;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_A]=2450;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_B]=24;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_A]=13378;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_B]=171;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_A]=14751;
    stSourceCalPara.s16CH2OffsetCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_B]=30;

    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_A] = 354;
    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_ON_AMP_OFF][SOURCE_CAL_FACTOR_B] = 1;
    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_A] = 3498;
    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_OFF_AMP_OFF][SOURCE_CAL_FACTOR_B] = 6;
    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_A] = 20733;
    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_OFF_AMP_ON][SOURCE_CAL_FACTOR_B] =37;
    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_A] = 2066;
    stSourceCalPara.s16CH2AmpCalFactor[AMP_CAL_ATT_ON_AMP_ON][SOURCE_CAL_FACTOR_B] =4;
}
/*******************************************************************************
 函 数 名:    //funcCalSourceVCTXO
 描    述:    校准信号源频率
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
void funcCalSourceVCTXO(void)
{
    u16 u16VCTOXFactor;
    u16VCTOXFactor = stSourceCalPara.u16VCTOXFactor;

    devConfigVCTXOReg(&u16VCTOXFactor);
}

/*******************************************************************************
 函 数 名:    //funcSetSourcePhaseDev
 描    述:    设置相位偏差
 输入参数:  *           参数名 				类型 					描述
            *       s16PhaseDev             s16                  相位偏差
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
void funcSetSourcePhaseDev(s16 s16PhaseDev)
{
    stSourceCalPara.s16PhaseDev = s16PhaseDev;
}

/*******************************************************************************
 函 数 名:    //funcSetSourceVCTXO
 描    述:    设置VCTXO的数值
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
void funcSetSourceVCTXO(u16 u16VCTOXFactor)
{
    stSourceCalPara.u16VCTOXFactor = u16VCTOXFactor;

    funcCalSourceVCTXO();
}

/*******************************************************************************
 函 数 名:    //funcSetSourceOffsetCalFactor
 描    述:    设置偏移校准系数
 输入参数:  *           参数名 				类型 					描述
            *          bSource1            bool                   通道，true为通道1
                       u16Stauts           u16                    通道状态
                                                                  AMP_CAL_ATT_ON_AMP_OFF
                                                                  AMP_CAL_ATT_OFF_AMP_OFF
                                                                  AMP_CAL_ATT_OFF_AMP_ON
                                                                  AMP_CAL_ATT_ON_AMP_ON
                                                                  DC_CAL_ATT_OFF_AMP_OFF
                                                                  DC_CAL_ATT_OFF_AMP_ON
                       s16FactorA          s16                    校准因数A
                       s16FactorB          s16                    校准因数B
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
void funcSetSourceOffsetCalFactor(bool bSource1, u16 u16Stauts, s16 s16FactorA, s16 s16FactorB)
{
    if (bSource1)
    {
        stSourceCalPara.s16CH1OffsetCalFactor[u16Stauts][SOURCE_CAL_FACTOR_A] = s16FactorA;
        stSourceCalPara.s16CH1OffsetCalFactor[u16Stauts][SOURCE_CAL_FACTOR_B] = s16FactorB;
    }
    else
    {
        stSourceCalPara.s16CH2OffsetCalFactor[u16Stauts][SOURCE_CAL_FACTOR_A] = s16FactorA;
        stSourceCalPara.s16CH2OffsetCalFactor[u16Stauts][SOURCE_CAL_FACTOR_B] = s16FactorB;
    }
}

/*******************************************************************************
 函 数 名:    //funcSetSourceAmpCalFactor
 描    述:    设置幅度校准系数
 输入参数:  *           参数名 				类型 					描述
            *         bSource1            bool                   通道，true为通道1
                      u16Stauts           u16                    通道状态
                                                                  AMP_CAL_ATT_ON_AMP_OFF
                                                                  AMP_CAL_ATT_OFF_AMP_OFF
                                                                  AMP_CAL_ATT_OFF_AMP_ON
                                                                  AMP_CAL_ATT_ON_AMP_ON
                       s16FactorA          s16                    校准因数A
                       s16FactorB          s16                    校准因数B
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
void funcSetSourceAmpCalFactor(bool bSource1, u16 u16Stauts, s16 s16FactorA, s16 s16FactorB)
{
    if (bSource1)
    {
        stSourceCalPara.s16CH1AmpCalFactor[u16Stauts][SOURCE_CAL_FACTOR_A] = s16FactorA;
        stSourceCalPara.s16CH1AmpCalFactor[u16Stauts][SOURCE_CAL_FACTOR_B] = s16FactorB;
    }
    else
    {
        stSourceCalPara.s16CH2AmpCalFactor[u16Stauts][SOURCE_CAL_FACTOR_A] = s16FactorA;
        stSourceCalPara.s16CH2AmpCalFactor[u16Stauts][SOURCE_CAL_FACTOR_B] = s16FactorB;
    }
}

//*********************************** 获取校准参数接口 ********************************************
/*******************************************************************************
 函 数 名:    //funcGetSourceCalPhase
 描    述:    获取信号源相位校准参数
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  相位校准参数
 说    明:
*******************************************************************************/
s16 funcGetSourceCalPhase(void)
{
    return stSourceCalPara.s16PhaseDev;
}

/*******************************************************************************
 函 数 名:    //funcGetSourceCalVCTXO
 描    述:    获取信号源频率校准参数
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  频率校准系数
 说    明:
*******************************************************************************/
u16 funcGetSourceCalVCTXO(void)
{
    return stSourceCalPara.u16VCTOXFactor;
}

/*******************************************************************************
 函 数 名:    //funcGetSourceCalOffset
 描    述:    获取信号源偏移校准参数（包括DC）
 输入参数:  *           参数名 				类型 					描述
            *          bChannel1            bool                  通道选择 ture为通道1
                     u8ChannelStatas         u8                   模拟前端状态
                                                             DC_CAL_ATT_OFF_AMP_OFF
                                                             DC_CAL_ATT_OFF_AMP_ON
                                                             AMP_CAL_ATT_ON_AMP_OFF
                                                             AMP_CAL_ATT_OFF_AMP_OFF
                                                             AMP_CAL_ATT_OFF_AMP_ON
                                                             AMP_CAL_ATT_ON_AMP_ON

 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  无
 说    明:
*******************************************************************************/
void funcGetSourceCalOffset(bool bChannel1, u8 u8ChannelStatas, s16 *ps16FactorA, s16 *ps16FactorB)
{
    if (bChannel1)
    {
        *ps16FactorA = stSourceCalPara.s16CH1OffsetCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_A];
        *ps16FactorB = stSourceCalPara.s16CH1OffsetCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_B];
    }
    else
    {
        *ps16FactorA = stSourceCalPara.s16CH2OffsetCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_A];
        *ps16FactorB = stSourceCalPara.s16CH2OffsetCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_B];
    }
}

/*******************************************************************************
 函 数 名:    //funcGetSourceCalAmp
 描    述:    获取信号源幅度校准参数
 输入参数:  *           参数名 				类型 					描述
            *       bChannel1            bool                  通道选择 ture为通道1
                    u8ChannelStatas         u8                   模拟前端状态
                                                             DC_CAL_ATT_OFF_AMP_OFF
                                                             DC_CAL_ATT_OFF_AMP_ON
                                                             AMP_CAL_ATT_ON_AMP_OFF
                                                             AMP_CAL_ATT_OFF_AMP_OFF
                                                             AMP_CAL_ATT_OFF_AMP_ON
                                                             AMP_CAL_ATT_ON_AMP_ON
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  无
 说    明:
*******************************************************************************/
void funcGetSourceCalAmp(bool bChannel1, u8 u8ChannelStatas, s16 *ps16FactorA, s16 *ps16FactorB)
{
    if (bChannel1)
    {
        *ps16FactorA = stSourceCalPara.s16CH1AmpCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_A];
        *ps16FactorB = stSourceCalPara.s16CH1AmpCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_B];
    }
    else
    {
        *ps16FactorA = stSourceCalPara.s16CH2AmpCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_A];
        *ps16FactorB = stSourceCalPara.s16CH2AmpCalFactor[u8ChannelStatas][SOURCE_CAL_FACTOR_B];
    }
}


/*******************************************************************************
 函 数 名:    funcUtiLoadSourceData
 描    述:    加载SOURCE的校准参数
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  s32 < 0 错误， 0 成功
 说    明:
*******************************************************************************/
s32 funcUtiLoadSourceData( u16 *pu16Stream, ///< 存储的数据流
                           u32 u32Len ) 	///< 输出数据流,BYTEs
{
    s32 s32Error = 0;

    QFile file(CAL_DG_DATA_FILE);
    if(!file.open(QIODevice::ReadOnly))
    {
        return -1;
    }

    QDataStream in(&file);

    for(u32 i = 0;i < u32Len;i++)
    {
        in >> *(pu16Stream + i);
    }

    file.close();

    return s32Error;
}

/*******************************************************************************
 函 数 名:    funcUtiSaveSourceData
 描    述:    存储SOURCE的校准参数
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  s32 < 0 错误， 0 成功
 说    明:
*******************************************************************************/
s32 funcUtiSaveSourceData( u16 *pu16Stream,	///< 加载的数据流
                           u32 u32Len )	///< 加载的数据长度 BYTEs
{
    QFile file(CAL_DG_DATA_FILE);
    if(!file.open(QIODevice::WriteOnly))
    {
        Q_ASSERT(false);
        return -1;
    }

    QDataStream out(&file);
    for(u32 i = 0;i < u32Len;i++)
    {
        out << *(pu16Stream + i);
    }

    out.setVersion(QDataStream::Qt_5_5);

    file.flush();
    file.close();
    return 0;
}
/*************get cal data pointer and cal data **********************/
SourceCalParaStru* getSourCalPara()
{
    return &stSourceCalPara;
}

void setSourCalPara(SourceCalParaStru sourcaltemp)
{
    stSourceCalPara = sourcaltemp;
}
/*---------------------------------- 文件结束 --------------------------------*/
