/*****************************************************************************
				普源精电科技有限公司版权所有(2004-2010)
******************************************************************************
源文件名: funcSourceFpgaConfig.c
功能描述: 信号源FPGA配置
作 者: KarlChen
版 本: 1.0
创建日期:2012-07-13
修改历史:
   <作者> 		  <修改时间> 		<版本>		 <修改描述>
               
  KarlChen         12/07/13          1.0          build this moudle
*****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <QDebug>
#include "ComStru.h"
#include "data_type.h"
#include "funcConstructWaveData.h"
#include "funcSourceStartSet.h"
#include "funcCalSource.h"
/********************************外部变量声明 *******************************/

extern SourceCalParaStru stSourceCalPara;
/*------------------------------- 局部常数和类型定义 -----------------------*/

typedef enum
{
    WAVE_DATA_SINE,
    WAVE_DATA_SQUARE,
    WAVE_DATA_RAMP,
    WAVE_DATA_PULSE,
    WAVE_DATA_DC,
    WAVE_DATA_NOISE,
    WAVE_DATA_ARB1,
    WAVE_DATA_ARB2,
    WAVE_DATA_BOTT
}funcSourceWaveDataEnum;

typedef enum
{
    WAVE_MOD_DATA_SINE,
    WAVE_MOD_DATA_SQUARE,
    WAVE_MOD_DATA_RAMP,

    WAVE_MOD_DATA_BOTT
}funcSourceWaveModDataEnum;

u16 *g_pu16WaveSineData = NULL; //Sine波波表数据
u16 *g_pu16WaveSquareData = NULL; //方波波表数据
u16 *g_pu16WaveRampData = NULL; //Ramp波波表数据
u16 *g_pu16WavePulseData = NULL; //Pulse波波表数据
u16 *g_pu16WaveDCData = NULL; //DC波表数据
u16 *g_pu16WaveNoiseData = NULL; //DC波表数据
u16 *g_pu16WaveArbCh1Data = NULL;                   //用户波表数据1
u16 *g_pu16WaveArbCh2Data = NULL;                   //用户波表数据2
u16 *g_pu16WaveSineModData = NULL; //Sine调制波波表（1kpts）
u16 *g_pu16WaveSquareModData = NULL; //Square调制波波表（1kpts）
u16 *g_pu16WaveRampModData = NULL; //Ramp调制波波表

/*------------------------------- 局部宏定义 -------------------------------*/

#define SOURCE_FPGA_FLASH_PAGE    7   //信号源FPGA在flash上的页面
#define NOR_FLASH_SECTOR_LEN      0x20000       // Nor Flash扇区大小（最小擦除单位，128k）
#define NOR_FLASH_PAGE_LEN        0x200000      // Nor Flash页大小（2M）
#define SPI_DMA_BLK_SIZE         (65536)
#define DMA_CHAN_SPI             (7)
#define ERR_FUNC_NONE             0
#define FUN_2PI                  (3.1415926 * 2.0)

                                   /*参数默认值*/
#define SOURCE_FREQ_DEFAULT     1000000000    //1kHz
#define SOURCE_AMP_DEFAULT      1000            //5V
#define SOURCE_OFFSET_DEFAULT   0               //0V
#define SOURCE_PHASE_DEFAULT    0               //0
#define SOURCE_SYMMETRY_DEFAULT	100             //10%
#define SOURCE_DUTY_DEFAULT     200             //20%


#define SOURCE_MOD_FREQ_DEFAULT 1000000000            //1kHz
#define SOURCE_MOD_AM_DEPTH     100              //100%
//?zx
#define SOURCE_MOD_FM_DEV        1000000000       //1kHz
#define SOURCE_MOD_FSK_HOP       10000000000      //10kHz

#define		FUNC_MEM_CPY(dst,src,len)			memcpy( (void*)(dst), (void*)(src), len )
/*------------------------------- 局部变量 ---------------------------------*/
/*------------------------------- 局部函数原形 -----------------------------*/

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 函数实现 $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/*******************************************************************************
 函 数 名:    // initSourceWaveData
 描    述:  初始化波表函数
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
s16 initSourceWaveData(void)
{
    s16 s16Err = ERR_FUNC_NONE;
    u32 u32Duty = 500;

    /**
     * 分配空间
     */
    g_pu16WaveSineData = (u16*)calloc(WAVE_DATA_BOTT * WAVE_SIZE
                                    + WAVE_MOD_DATA_BOTT * WAVE_MOD_SIZE,
                                      sizeof(u16));

    g_pu16WaveSquareData = g_pu16WaveSineData + WAVE_SIZE;
    g_pu16WaveRampData = g_pu16WaveSquareData + WAVE_SIZE;
    g_pu16WavePulseData = g_pu16WaveRampData + WAVE_SIZE;
    g_pu16WaveDCData = g_pu16WavePulseData + WAVE_SIZE;
    g_pu16WaveNoiseData = g_pu16WaveDCData + WAVE_SIZE;
    g_pu16WaveArbCh1Data = g_pu16WaveNoiseData + WAVE_SIZE;
    g_pu16WaveArbCh2Data = g_pu16WaveArbCh1Data + WAVE_SIZE;

    g_pu16WaveSineModData = g_pu16WaveArbCh2Data + WAVE_SIZE;
    g_pu16WaveSquareModData = g_pu16WaveSineModData + WAVE_MOD_SIZE;
    g_pu16WaveRampModData = g_pu16WaveSquareModData + WAVE_MOD_SIZE;

    //生成Sine波波表
    funcConstuctSineData(g_pu16WaveSineData, WAVE_AMP, FUNC_DG_SINE_WAVE_SIZE);
    //生成方波波表
    funcConstuctPulseData(g_pu16WaveSquareData, WAVE_AMP, FUNC_DG_SQUARE_WAVE_SIZE,u32Duty, (u64)1000000000);
    funcConstuctDCData(g_pu16WaveDCData, FUNC_DG_DC_WAVE_SIZE);

#if 1
    funcConstuctDCData(g_pu16WaveArbCh1Data,FUNC_DG_ARB_WAVE_SIZE);
    funcConstuctDCData(g_pu16WaveArbCh2Data,FUNC_DG_ARB_WAVE_SIZE);
#endif
    funcConstuctNoiseData(g_pu16WaveNoiseData, FUNC_DG_NOISE_WAVE_SIZE);

    funcConstuctSineModData(g_pu16WaveSineModData, WAVE_MOD_AMP, FUNC_DG_SINE_MOD_WAVE_SIZE);
    funcConstuctPulseData(g_pu16WaveSquareModData, WAVE_MOD_AMP,FUNC_DG_SQUARE_MOD_WAVE_SIZE, 500, (u64)1000000000);
    funcConstuctRampData(g_pu16WaveRampModData, WAVE_MOD_AMP,FUNC_DG_RAMP_MOD_WAVE_SIZE, 500);

    funcSetSourceCalParaDefault();


    return s16Err;
}
/*******************************************************************************
 函 数 名:  funcGetArbWavePointer
 描    述:  获取任意波波表处指针
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
u16* funcGetArbWavePointer(devFpgaChanEu chan)
{
    if(chan == CH1_FPGA)
    {
        return g_pu16WaveArbCh1Data;
    }
    else
    {
        return g_pu16WaveArbCh2Data;
    }
}
/*******************************************************************************
 函 数 名:    // initSourceRelay
 描    述:  初始化外部继电器和接口
 输入参数:  *           参数名 				类型 					描述
			*        		           
 输出参数:  *           参数名 		  		类型 					描述
			*         
 返 回 值:  
 说    明:                               			   
*******************************************************************************/
s16 initSourceRelay(void)
{
    qDebug()<<__FUNCTION__;
	s16 s16Err = ERR_FUNC_NONE;
	u16 u16CfgVal;
	u32 u32CfgVal;
	u64 u64CfgVal;

    //u16CfgVal = 0xC000;
    u16CfgVal = 0xA000;
    devConfigParaDacReg ( &u16CfgVal, DEV_PARA_DAC_OFFSET_CH1 );
    
    //u16CfgVal = 0x4000;
    u16CfgVal = 0x7fff;
    devConfigParaDacReg ( &u16CfgVal, DEV_PARA_DAC_OFFSET_CH2 );
    
    //u16CfgVal = 0x7000;
    u16CfgVal = 0x7fff;
    devConfigParaDacReg ( &u16CfgVal, DEV_PARA_DAC_REF_CH1 );
    
    //u16CfgVal = 0x7AE2;
    u16CfgVal = 0x7fff;
    devConfigParaDacReg ( &u16CfgVal, DEV_PARA_DAC_REF_CH2 );
    
    //继电器设置 CH1_DEV_RY_ATT  CH1_DEV_RY_AMP

    u32CfgVal =     CH1_DEV_RY_OUT |   CH2_DEV_RY_OUT;// | CH2_DEV_RY_AMP | CH1_DEV_RY_AMP;
    devConfigRelayCtrlReg ( &u32CfgVal );

	//VCTXO设置(参数DAC中也有相应的功能，目前实际能控制晶振的是此处的配置)
    //u16CfgVal = 1155;
    //devConfigVCTXOReg(&stSourceCalPara.u16VCTOXFactor);
    
    //关闭dds
    qDebug()<<"close DDS";
	u16CfgVal = 0x0000;   
	devConfigSysCtrlReg ( &u16CfgVal ,ALL_FPGA );
	
	//同步信号选择
	u16CfgVal = DEV_SYNC_OUT_EN | DEV_SYNC_SRC_SQU_DIV;
	devConfigSynReg (  &u16CfgVal ,ALL_FPGA );

	//设置延时，分频系数
	u16CfgVal = ( 2<<8 ) | 4;
	devConfigSynDlyDivReg (  &u16CfgVal ,ALL_FPGA );
	
    //频率设置
    u64CfgVal = DEV_FREQ_WORD_GEN(1000000000);        //1kHz
    devConfigCenterFreqReg ( &u64CfgVal, ALL_FPGA );
	
	//初始相位
	u16CfgVal = DEV_INIT_PHASE_WORD_GEN(45);          //180°
	devConfigInitPhaseReg ( &u16CfgVal, ALL_FPGA );

	
	//调制偏移系数
	u64CfgVal = (u64)0x4000 << 32;        
    devConfigModuDevIndexReg ( &u64CfgVal, ALL_FPGA );
    
    //幅度偏移系数
    u16CfgVal = 0xffff;        
    devConfigAmplDevIndexReg ( &u16CfgVal, ALL_FPGA );
    
    //触发源选择
    u16CfgVal = DEV_EXT_TRIG_EN | 0x0008;
    devConfigTrigModReg ( &u16CfgVal, ALL_FPGA );
    
    //AM补偿幅度
    u16CfgVal = 0x8000;
    devConfigAmDacCompReg ( &u16CfgVal, ALL_FPGA );
    
    //DAC空置电平
    //u16CfgVal = 0x8000;
    u16CfgVal = 0x2000;
    devConfigDacDataIdleReg ( &u16CfgVal, ALL_FPGA );
	//打开dds
    //u16CfgVal = DEV_CTRL_DDS_ON;                      //dds使能
    //devConfigSysCtrlReg ( &u16CfgVal ,ALL_FPGA );
	
	return s16Err;
	
}

/*******************************************************************************
 函 数 名:    //funcSetSource1ParaDefault
 描    述:    设置信号源1参数默认值
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:
 说    明:
*******************************************************************************/
//s16 funcSetSourceParaDefault(devFpgaChanEu chan)
//{
//    stSourcePara[chan-1].bSourceEnable = false;
//    stSourcePara[chan-1].bSourceDisplay = false;
//    stSourcePara[chan-1].enSourceWaveform = WAVE_SINE;
//    stSourcePara[chan-1].enInterWaveform = INTERNAL_WAVE_SINC;
//    stSourcePara[chan-1].u64Freq = SOURCE_FREQ_DEFAULT;
//    stSourcePara[chan-1].u32Amp = SOURCE_AMP_DEFAULT;
//    stSourcePara[chan-1].s32Offset = SOURCE_OFFSET_DEFAULT;
//    stSourcePara[chan-1].u16Phase = SOURCE_PHASE_DEFAULT;
//    stSourcePara[chan-1].u16Symmetry = SOURCE_SYMMETRY_DEFAULT;
//    stSourcePara[chan-1].u16Duty = SOURCE_DUTY_DEFAULT;

//    stSourcePara[chan-1].bModEnable = false;
//    stSourcePara[chan-1].enModType = MOD_AM;
//    stSourcePara[chan-1].enModWave = MOD_WAVE_SINE;
//    stSourcePara[chan-1].u64ModFreq = SOURCE_MOD_FREQ_DEFAULT;
//    stSourcePara[chan-1].u8AMDepth = SOURCE_MOD_AM_DEPTH;
//    stSourcePara[chan-1].u64FMDev = SOURCE_MOD_FM_DEV;
//    stSourcePara[chan-1].u64HopFreq = SOURCE_MOD_FSK_HOP;

//    stSourcePara[chan-1].isPolarity = false;
//    stSourcePara[chan-1].euDgworkMode = NONE_MODE;
//    return 1;
//}
/*******************************************************************************
 函 数 名:    //funcSetSource1ParaStartUp
 描    述:    配置信号源1参数
 输入参数:  *           参数名 				类型 					描述
            *
 输出参数:  *           参数名 		  		类型 					描述
            *
 返 回 值:  数据长度
 说    明:
*******************************************************************************/
//s32 funcSetSourceParaStartUp(devFpgaChanEu chan)
//{
//    funcSetSourceFreq(chan, stSourcePara[chan-1].u64Freq);
//    funcSetSourceAmp(chan, stSourcePara[chan-1].u32Amp);
//    funcSetSourceOffset(chan, stSourcePara[chan-1].s32Offset);
//    funcSetSourcePhase(chan, stSourcePara[chan-1].u16Phase);
//    funcSetSourceModEnable(chan,stSourcePara[chan-1].bModEnable);
//    funcSetSourceSymmetry(chan, stSourcePara[chan-1].u16Symmetry);
//    funcSetSourceDuty(chan, stSourcePara[chan-1].u16Duty);
//    // Added by wuke For Bug2061, 20141028
//    funcSetSourceWaveType(chan, stSourcePara[chan-1].enSourceWaveform);
//    funcSetSourceHopFreq(chan,stSourcePara[chan-1].u64HopFreq);
//    //最后决定是否开关
//    funcSetSourceModWave(chan, stSourcePara[chan-1].enModWave);
//    funcSetSourceEnabel(chan, stSourcePara[chan-1].bSourceEnable);

//    return 1;
//}
/*---------------------------------- 文件结束 --------------------------------*/
