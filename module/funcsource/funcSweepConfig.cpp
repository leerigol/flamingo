#include <cmath>
#include "funcSweepConfig.h"
#include "funcConstructWaveData.h"


s32 SweepConfig::funcConstructLine(u16 *pu16WaveData)
{
    u32 u32RiseSize = WAVE_MOD_SIZE;        //上升沿所占波表点个数
    u32 u32i;

    //! 构筑一条直线
    f32 f32RiseStep = WAVE_MOD_AMP*1.0/WAVE_MOD_SIZE;
    for(u32i = 0; u32i < u32RiseSize; u32i++)
    {
        pu16WaveData[u32i] = f32RiseStep * u32i;
    }

    if (m_bUpflag)
    {
        u16 u16Data = 0;
        for(int j = 0; j < WAVE_MOD_SIZE/2; j++)
        {
             u16Data = pu16WaveData[j];
             pu16WaveData[j] = pu16WaveData[WAVE_MOD_SIZE - 1 - j];
             pu16WaveData[WAVE_MOD_SIZE - 1 - j] = u16Data;
        }
    }

    return dg_err_none;
}

s32 SweepConfig::funcConstructLog(u16 *pu16WaveData, s64 s64StartFreq, s64 s64StopFreq)
{
    f64 f64StartFreq;
    f64 f64StopFreq;
    f64 f64coef1;
    f64 f64coef2;
    if (s64StopFreq > s64StartFreq)
    {
        f64StartFreq = s64StartFreq;
        f64StopFreq = s64StopFreq;
    }
    else
    {
        f64StopFreq = s64StartFreq;
        f64StartFreq = s64StopFreq;
    }

    f64coef1 = WAVE_MOD_AMP / (f64StopFreq - f64StartFreq);
    f64coef2 = log10f(f64StopFreq/f64StartFreq)/WAVE_MOD_SIZE;

    for(int i = 0; i < WAVE_MOD_SIZE; i++)
    {
        pu16WaveData[i] = (u16)(f64coef1 * f64StartFreq * (pow(10, i * f64coef2) - 1));
    }
    if (m_bUpflag)
    {
        u16 u16Data = 0;
        for(int j = 0; j < WAVE_MOD_SIZE/2; j++)
        {
             u16Data = pu16WaveData[j];
             pu16WaveData[j] = pu16WaveData[WAVE_MOD_SIZE - 1 - j];
             pu16WaveData[WAVE_MOD_SIZE - 1 - j] = u16Data;
        }
    }
    return dg_err_none;
}

s32 SweepConfig::funcConstStepData(u16 *pu16WaveData, int step)
{
    u32 u32i = 0;
    u32 ampStep = WAVE_MOD_AMP / step;
    u32 sizeStep = WAVE_MOD_SIZE / step;
    for(int i = 0; i < step - 1;i++)
    {
        for(;u32i < (i+1) *sizeStep;u32i++)
        {
            pu16WaveData[u32i] = i * ampStep;
        }
    }

    for(; u32i < WAVE_MOD_SIZE; u32i++)
    {
        pu16WaveData[u32i] = WAVE_MOD_AMP;
    }

    if (m_bUpflag)
    {
        u16 u16Data = 0;
        for(int j = 0; j < WAVE_MOD_SIZE/2; j++)
        {
             u16Data = pu16WaveData[j];
             pu16WaveData[j] = pu16WaveData[WAVE_MOD_SIZE - 1 - j];
             pu16WaveData[WAVE_MOD_SIZE - 1 - j] = u16Data;
        }
    }
    return dg_err_none;
}

s32 SweepConfig::funcSweepParaConfig(comSweepParaStru stSweepPara, devFpgaChanEu euChannel)
{
    /* 控制系统运行寄存器FUN_CTRL_RUN_REG */
    u16   u16RunRegCmd = 0;
    devConfigSysCtrlReg(&u16RunRegCmd, euChannel);
    /* 控制触发寄存器FUN_CTRL_TRIG_REG */
    s32   s32RetCode = ERR_DEV_DG_NONE;
    u64   u64FreqDevVal, u64CarrierVal;

    /*配置触发*/
    u16 trigMode = funcGetSweepTrigVal(stSweepPara);
    devConfigTrigModReg(&trigMode, euChannel);

    u16 compenVal = 0;
    devCompenSate(&compenVal, euChannel);

    /* 根据扫描时间、停留时间、返回时间确定扫描周期内各时间段使能 */
    u16 stageEn = funcGetSweepStageMode(stSweepPara);
    devSweepStageEn(&stageEn, euChannel);
    /* 配置调制幅度指数 FUN_MODU_DEV_INDEX_REG */
    /* 配置扫频波表 */
    if (true)
    {
        if(stSweepPara.startFreq < stSweepPara.endFreq)
        {
            m_bUpflag = false;
            u64CarrierVal = DEV_FREQ_WORD_GEN(stSweepPara.startFreq);
            u64FreqDevVal = DEV_MOD_FM_FREQ_DEV_GEN(stSweepPara.endFreq - stSweepPara.startFreq);
        }
        else
        {
            m_bUpflag = true;
            u64FreqDevVal = DEV_MOD_FM_FREQ_DEV_GEN(stSweepPara.startFreq - stSweepPara.endFreq);
          //  u64FreqDevVal = DEV_MOD_FM_FREQ_DEV_GEN(stSweepPara.endFreq);
            u64CarrierVal = DEV_FREQ_WORD_GEN(stSweepPara.endFreq);
           // u64CarrierVal = DEV_FREQ_WORD_GEN(stSweepPara.startFreq);
        }

        /* 配置FM调制幅度指数 */
        devConfigCenterFreqReg((u64*)&u64CarrierVal, euChannel);
        devConfigModuDevIndexReg((u64*)&u64FreqDevVal, euChannel);

        funcSweepWaveConfig(stSweepPara, euChannel);
    }
    //! to do: 配置标记频率
    /* 配置开始终止保持时间 */
    u64 sweepStHoldTime = DEV_SWEEP_TIME_GEN(stSweepPara.startKeepTime);
    devSweepSTHold( &sweepStHoldTime, euChannel);
    u64 sweepEndHoldTime = DEV_SWEEP_TIME_GEN(stSweepPara.endKeepTime);
    devSweepENDHold( &sweepEndHoldTime, euChannel);

    /* 配置扫描时间 */
    u64 sweepTime = DEV_SWEEP_TIME_GEN(stSweepPara.sweepTime);
    devSweepTime(&sweepTime, euChannel);
    u64 returnTime = DEV_SWEEP_TIME_GEN(stSweepPara.returnTime);
    devSweepReturnTime(&returnTime, euChannel);

    /* 使能系统运行寄存器中的Sweep相应位 */
    if( stSweepPara.bSweepEn )
    {
        u16RunRegCmd |= DEV_CTRL_SWEEP_EN | DEV_CTRL_FM_EN | DEV_CTRL_MOD_EN | DEV_CTRL_DDS_ON;
    }
    else
    {
        u16RunRegCmd |= DEV_CTRL_DDS_ON;
    }
    devConfigSysCtrlReg(&u16RunRegCmd, euChannel);
    return  s32RetCode;
}

s32 SweepConfig::funcSweepWaveConfig(comSweepParaStru stSweep, devFpgaChanEu  euChannel)
{
    LOG_DBG();
    s32  s32RetCode = dg_err_none;

    //! 构建扫频线的波表
    static u16* pSweepModWave = NULL;
    if ( NULL == pSweepModWave )
    {
        pSweepModWave = (u16*)malloc( WAVE_MOD_SIZE * sizeof(u16) );
    }

    Q_ASSERT( NULL != pSweepModWave );
    //! fill 0
    memset( pSweepModWave, 0, WAVE_MOD_SIZE * sizeof(u16) );

    enumSweepType euSweepType = stSweep.sweepType;
    if (euSweepType == SWEEP_STEP) /* 分段扫描 */
    {
        funcConstStepData(pSweepModWave, stSweep.step);
        devConfigWaveDataRam(pSweepModWave, WAVE_MOD_SIZE, 0, euChannel, MODU_WAVE);
    }
    else
    {
        if (euSweepType == SWEEP_LINEAR) /* 线性扫描 */
        {
            funcConstructLine(pSweepModWave);
            devConfigWaveDataRam(pSweepModWave, WAVE_MOD_SIZE, 0, euChannel, MODU_WAVE);
        }
        else  /* 对数扫描 */
        {
            funcConstructLog(pSweepModWave, stSweep.startFreq, stSweep.endFreq);
            devConfigWaveDataRam(pSweepModWave, WAVE_MOD_SIZE, 0, euChannel, MODU_WAVE);
        }
    }

    //! 配置幅频补偿波表
    static u16* pAmpFreqWave = NULL;
    if(pAmpFreqWave == NULL)
    {
        pAmpFreqWave = (u16*)malloc( WAVE_MOD_SIZE * sizeof(u16) );
    }
    Q_ASSERT( NULL != pAmpFreqWave );
    memset( pAmpFreqWave, WAVE_MOD_AMP, WAVE_MOD_SIZE * sizeof(u16) );
    devConfigWaveDataRam(pAmpFreqWave, WAVE_MOD_SIZE, 0, euChannel, AMP_FREQ_WAVE);

    return  s32RetCode;
}

s32 SweepConfig::funcSweepTrigManual(devFpgaChanEu  euChannel)
{
    //! 手动触发一次就是讲触发模式配为无限模式，再切换位手动模式
    u16 u16TrigMode = DEV_INT_TRIG_EN;
    devTrigMod(&u16TrigMode, euChannel);

    u16TrigMode = DEV_MANUAL_TRIG_EN;
    devTrigMod(&u16TrigMode, euChannel);
    return dg_err_none;
}

u16 SweepConfig::funcGetSweepStageMode(comSweepParaStru stSweepPara)
{
    u16 sweepMode = 0;
    if(stSweepPara.sweepTime > 0)
    {
        sweepMode |= DEV_SWEEP_TIME_EN;
    }
    if(stSweepPara.returnTime > 0)
    {
        sweepMode |= DEV_RETURN_TIME_EN;
    }
    if(stSweepPara.startKeepTime > 0)
    {
        sweepMode |= DEV_ST_HOLD_TIME_EN;
    }
    if(stSweepPara.endKeepTime > 0)
    {
        sweepMode |= DEV_END_HOLD_TIME_EN;
    }
    return sweepMode;
}

u16 SweepConfig::funcGetSweepTrigVal(comSweepParaStru stSweepPara)
{
    u16 trigMode = 0;
    if(stSweepPara.stTrigger.euTrig_Source == Trig_INTERNAL)
    {
        trigMode = trigMode | DEV_INT_TRIG_EN;
    }
    else if(stSweepPara.stTrigger.euTrig_Source == Trig_MANUAL)
    {
        trigMode = trigMode | DEV_MANUAL_TRIG_EN;
    }

    return trigMode;
}
