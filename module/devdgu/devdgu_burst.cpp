#include "devDgu.h"

extern s32 _devFpgaConfigW(u16 *pu16TxAddr, u32 u32TxLength, u16 u16RegAddr, devFpgaChanEu euChannel);
/*************************描    述: 手动触发**************************************/
//!  burst
s32 devBurstRstReg(devFpgaChanEu euChannel)
{
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_SYS_CS, DEV_CTRL_BURST_RST_REG );
    u16 randval = 1;
    _devFpgaConfigW(&randval, DEV_CTRL_BURST_RST_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devTrigMod(u16 *pu16TrigMod, devFpgaChanEu euChannel)
{
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_SYS_CS, DEV_CTRL_TRIG_REG);
    _devFpgaConfigW(pu16TrigMod, DEV_CTRL_TRIG_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devTrigManual(u16 *pu16TrigVal, devFpgaChanEu euChannel)
{
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_SYS_CS, DEV_CTRL_MANUAL_TRIG_REG);
    _devFpgaConfigW(pu16TrigVal, DEV_CTRL_MANUAL_TRIG_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devBurstDelay(u64 *delayVal, devFpgaChanEu euChannel)
{
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_SYS_CS, DEV_CTRL_DELAY_COARSE_REG);
    u16* pu16DelayVal = (u16*) delayVal;
    _devFpgaConfigW(pu16DelayVal, DEV_CTRL_DELAY_COARSE_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devBurstCycles(u32 *cyclesVal, devFpgaChanEu euChannel)
{
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_SYS_CS, DEV_CTRL_BURST_CYCLES_REG);
    u16* pu16cyclesVal = (u16*) cyclesVal;
    _devFpgaConfigW(pu16cyclesVal, DEV_BURST_CYCLES_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devBurstPeriod(u64 *periodVal, devFpgaChanEu euChannel)
{
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_SYS_CS, DEV_CTRL_TRIG_INT_PER_REG);
    LOG_DBG();
    u16* pu16PeriodVal = (u16*) periodVal;

  //   qDebug() << "u64period 2 " << * pu16PeriodVal;
    _devFpgaConfigW(pu16PeriodVal, DEV_CTRL_TRIG_INT_PER_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}


/**********************sweep mode settings************************/
//! sweep
s32 devStartFreq(u64 *startFreq, devFpgaChanEu euChannel)
{
    LOG_DBG();
    devConfigCenterFreqReg( startFreq, euChannel );
    return dg_err_none;
}
/****************终止频率减去起始频率******************/
s32 devEndFreq(u64 *modFreq, devFpgaChanEu euChannel)
{
    LOG_DBG();
    devConfigModuDevIndexReg(modFreq, euChannel);
    return dg_err_none;
}

s32 devSweepSTHold(u64 *startHold, devFpgaChanEu euChannel)
{
    LOG_DBG();
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_MODU_CS, DEV_ST_HOLD_REG);
    _devFpgaConfigW((u16*) startHold, DEV_ST_HOLD_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devSweepENDHold(u64 *endHold, devFpgaChanEu euChannel)
{
    LOG_DBG();
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_MODU_CS, DEV_END_HOLD_REG);
    _devFpgaConfigW((u16*) endHold, DEV_END_HOLD_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devSweepTime(u64 *sweepTime, devFpgaChanEu euChannel)
{
    LOG_DBG();
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_MODU_CS, DEV_MODU_FREQ_REG);
    _devFpgaConfigW((u16*) sweepTime, DEV_MODU_FREQ_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devSweepReturnTime(u64 *returnTime, devFpgaChanEu euChannel)
{
    LOG_DBG();
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_MODU_CS, DEV_RETURN_REG);
    _devFpgaConfigW((u16*) returnTime, DEV_RETURN_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devSweepStageEn(u16 *sweepStageEn, devFpgaChanEu euChannel)
{
    LOG_DBG();
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_MODU_CS, DEV_SWEEP_STAGE_REG);
    _devFpgaConfigW((u16*) sweepStageEn, DEV_SWEEP_STAGE_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}

s32 devCompenSate(u16 *compenVal, devFpgaChanEu euChannel)
{
    LOG_DBG();
    u16 u16RegAddr = DEV_WR_ADDR_GEN( DEV_MODU_CS, DEV_AM_DAC_COMP_REG);
    _devFpgaConfigW((u16*) compenVal, DEV_AM_DAC_COMP_CMD_LEN, u16RegAddr, euChannel);
    return dg_err_none;
}
