/*******************************************************************************
                普源精电科技技术有限公司版权所有(2004-2010)
********************************************************************************
  源文件名: devSource.c
  功能描述: 信号源部分驱动
  作    者: XieJun
  版    本: V0.1
  完成日期: 20120701

  修改历史:
  作者              修改时间        版本        修改描述
  XieJun           20120701        V0.1        创建
*******************************************************************************/

/*********************************包含文件声明*********************************/
#include <QDebug>
#include <string.h>
#include <stdlib.h> //! calloc
#include "../engine/cplatform.h"
#include "devDgu.h"



//! 0x4000_20C0
#define DGU_CFG_ADDR    0x20C0 //offset,not absolute address. 4000

//static u16* n_pu16DevDgWaveTable = NULL;

u16* devDGu_coefFreq = 0;

static u32 *_dguTransFifo = NULL;
#define DGU_TRANS_FIFO_SIZE (0x4000+256)

/*********************************局部函数原形*********************************/

static s32 _devConfigRamInitAddrReg ( u32* pu32RamInitAddrRegVal, devFpgaChanEu euChannel);
static s32 _devConfigWaveDataReg ( u16* pu16WaveDataVal, u32 u32TxLength,devFpgaChanEu euChannel);
static s32 _devConfigWaveTypeReg ( u16* pu16WaveTypeRegVal, devFpgaChanEu euChannel );
s32 _devFpgaConfigW(u16 *pu16TxAddr, u32 u32TxLength, u16 u16RegAddr, devFpgaChanEu euChannel);
s32 _devFpgaConfigR(u16 *pu16RxAddr, u16 u16RegAddr);

/***********************************函数实现***********************************/

/*******************************************************************************
以下为系统组函数
*******************************************************************************/


/*******************************************************************************
函 数 名: _devFpgaConfigR
描    述: 配置系统控制寄存器
输入参数:
          参数名称           参数类型          参数说明
          pu16SysCtrlRegVal     u16*              存放要配置的系统控制寄存器参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigSysCtrlReg ( u16* pu16SysCtrlRegVal ,devFpgaChanEu euChannel )
{
    s32  s32RetCode = 0;
    u16  u16RegAddr = 0;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_SYS_CTRL_REG);
    s32RetCode = _devFpgaConfigW(pu16SysCtrlRegVal, DEV_SYS_CTRL_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigWaveOptReg
描    述: 配置波形优化寄存器
输入参数:
          参数名称           参数类型          参数说明
          pu16WaveOptRegVal     u16*              存放要配置的波形优化寄存器参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigWaveOptReg ( u16* pu16WaveOptRegVal ,devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_CTRL_WAVE_OPTIMIZE_REG);
    s32RetCode = _devFpgaConfigW(pu16WaveOptRegVal, DEV_CTRL_WAVE_OPTIMIZE_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}


/*******************************************************************************
函 数 名: devConfigTrigModReg
描    述: 配置触发模式寄存器
输入参数:
          参数名称           参数类型          参数说明
          pu16TrigModRegVal     u16*              存放要配置的触发模式寄存器参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigTrigModReg ( u16* pu16TrigModRegVal ,devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_CTRL_TRIG_REG);
    s32RetCode = _devFpgaConfigW(pu16TrigModRegVal, DEV_CTRL_TRIG_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}


/*******************************************************************************
函 数 名: devConfigSynReg
描    述: 配置同步寄存器
输入参数:
          参数名称           参数类型          参数说明
          pu16SynRegVal     u16*              存放要配置的同步寄存器参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigSynReg ( u16* pu16SynRegVal ,devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_CTRL_SYNC_REG);
    s32RetCode = _devFpgaConfigW(pu16SynRegVal, DEV_CTRL_SYNC_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigSynReg
描    述: 配置同步信号延时和高频信号分频系数寄存器
输入参数:
          参数名称           参数类型          参数说明
          pu16SynDlyDivRegVal     u16*              存放要配置的寄存器参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigSynDlyDivReg ( u16* pu16SynDlyDivRegVal ,devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_SYNC_DELALY_DIV_REG);
    s32RetCode = _devFpgaConfigW(pu16SynDlyDivRegVal, DEV_SYNC_DELALY_DIV_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigDacDataIdleReg
描    述: 配置DAC空置时的电平
输入参数:
          参数名称           参数类型          参数说明
          pu16DacDataIdleRegVal     u16*              存放DAC空置时的输出电压
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigDacDataIdleReg ( u16* pu16DacDataIdleRegVal, devFpgaChanEu euChannel)
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_DAC_DATA_IDLE_REG);
    s32RetCode = _devFpgaConfigW(pu16DacDataIdleRegVal, DEV_DAC_DATA_IDLE_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigWaveDataRam
描    述: 配置波表函数，由上边三个函数进行封装得来
输入参数:
          参数名称           参数类型          参数说明
          pu16WaveDataVal       u16*                指向波表首地址
          u32TxLength              u32                  波表数据个数
          pu32RamInitAddr        u32*                指向的参数表示波表在FPGA内部RAM中的起始地址
          euChannel          devFpgaChanEu            通道
          enWaveType          devWaveTypeEu        波表类型
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigWaveDataRam ( u16* pu16WaveDataVal, u32 u32TxLength, u32 u32RamInitAddr, devFpgaChanEu euChannel, devWaveTypeEu enWaveType )
{
    u16  u16WaveType = (u16)enWaveType;
    u32  u32RamInitAddrSub1 = u32RamInitAddr - 1;//配置的波表地址为实际地址 - 1

    s32  s32RetCode1 = ERR_DEV_DG_NONE;
    s32  s32RetCode2 = ERR_DEV_DG_NONE;
    s32  s32RetCode3 = ERR_DEV_DG_NONE;

    //bug 2206 by zy
    if(enWaveType == CARRY_WAVE)
    {
        devConfigDacDataIdleReg ( &pu16WaveDataVal[0], euChannel );
    }

    //首先选择要配置的波表类别
    s32RetCode1 = _devConfigWaveTypeReg ( &u16WaveType, euChannel);

    //确定要配置的波表起始地址(FPGA内部RAM地址)
    s32RetCode2 = _devConfigRamInitAddrReg ( &u32RamInitAddrSub1, euChannel);

    //向波表写入数据
    s32RetCode3 = _devConfigWaveDataReg ( pu16WaveDataVal, u32TxLength, euChannel);

    //返回三次返回值的或逻辑，有一次不是NO_ERROR就提示错误
    return (s32RetCode1 | s32RetCode2 | s32RetCode3);
}

/*******************************************************************************
函 数 名: devConfigInitPhaseReg
描    述: 配置波形初始相位
输入参数:
          参数名称           参数类型          参数说明
          pu16InitPhaseRegVal     u16*              存放波形初始相位参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigInitPhaseReg ( u16* pu16InitPhaseRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_INIT_PHASE_REG);
    s32RetCode = _devFpgaConfigW(pu16InitPhaseRegVal, DEV_INIT_PHASE_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigInitPhaseReg
描    述: 配置波形中心频率
输入参数:
          参数名称           参数类型          参数说明
          pu16CenterFreqRegVal     u16*              存放波形中心频率参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigCenterFreqReg ( u64* pu64CenterFreqRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_CENTER_FREQ_REG);
    s32RetCode = _devFpgaConfigW((u16*)pu64CenterFreqRegVal, DEV_CENTER_FREQ_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigHopFreqReg()
描    述: 配置波形Hop频率
输入参数:
          参数名称           参数类型          参数说明
          pu16CenterFreqRegVal     u16*              存放波形Hop频率参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigHopFreqReg ( u64* pu64CenterFreqRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

//    qDebug()<<"config Hop Freq";
//    qDebug("%X",*pu64CenterFreqRegVal);
    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_FSK_HOP_FREQ_REG);
    s32RetCode = _devFpgaConfigW((u16*)pu64CenterFreqRegVal, DEV_FSK_HOP_FREQ_CMD_LEN, u16RegAddr, euChannel);
    return s32RetCode;
}

/*******************************************************************************
以下为调制组函数
*******************************************************************************/
/*******************************************************************************
函 数 名: devConfigModuFreqReg
描    述: 配置调频/调相步长
输入参数:
          参数名称           参数类型          参数说明
          pu64ModuFreqRegVal       u64*              存放调频/调相步长参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:   低40bit有效
*******************************************************************************/

s32 devConfigModuFreqReg ( u64* pu64ModuFreqRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_MODU_CS,DEV_MODU_FREQ_REG);
    s32RetCode = _devFpgaConfigW((u16*)pu64ModuFreqRegVal, DEV_MODU_FREQ_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigModuDevIndexReg
描    述: 配置调制偏移系数
输入参数:
          参数名称           参数类型          参数说明
          pu64ModuDevIndexRegVal     u64*              存放调制偏移系数参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:低48bit有效
*******************************************************************************/

s32 devConfigModuDevIndexReg ( u64* pu64ModuDevIndexRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_MODU_CS,DEV_MODU_DEV_INDEX_REG);
    s32RetCode = _devFpgaConfigW((u16*)pu64ModuDevIndexRegVal, DEV_MODU_DEV_INDEX_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigAmplDevIndexReg
描    述: 配置幅度偏移系数
输入参数:
          参数名称           参数类型          参数说明
          pu16ModuDevIndexRegVal     u16*              存放幅度偏移系数参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigAmplDevIndexReg ( u16* pu16AmplDevIndexRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_MODU_CS,DEV_AMPL_DEV_INDEX_REG);
    s32RetCode = _devFpgaConfigW(pu16AmplDevIndexRegVal, DEV_AMPL_DEV_INDEX_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigAmDacCompReg
描    述: 配置AM补偿系数
输入参数:
          参数名称           参数类型          参数说明
          pu16AmDacCompRegVal     u16*              存放AM补偿系数参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigAmDacCompReg ( u16* pu16AmDacCompRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_MODU_CS,  DEV_AM_DAC_COMP_REG);
    s32RetCode = _devFpgaConfigW(pu16AmDacCompRegVal, DEV_AM_DAC_COMP_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}


/*******************************************************************************
以下为外设组函数
*******************************************************************************/

/*******************************************************************************
函 数 名: devConfigParaDacReg
描    述: 配置参数DAC
输入参数:
          参数名称           参数类型          参数说明
          pu16ParaDacRegVal     u16*              存放配置参数DAC的参数
          u16ParaDacRegAddr    u16                参数DAC通道对应地址
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigParaDacReg ( u16* pu16ParaDacRegVal, u16 u16ParaDacRegAddr )
{
    s32  s32RetCode = 0;
    int dacChan = (u16ParaDacRegAddr%64)/4;
    cplatform::sysGetDsoPlatform()->m_pPhy->mDgu.setDac(dacChan,*pu16ParaDacRegVal);
    cplatform::sysGetDsoPlatform()->m_pPhy->mDgu.flushWCache();

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigRelayCtrlReg
描    述: 配置继电器
输入参数:
          参数名称           参数类型          参数说明
          pu16RelayCtrlRegVal     u16*             存放配置继电器的参数
返 回 值: 错误代码
说    明:
*******************************************************************************/
s32 devConfigRelayCtrlReg ( u32* pu32RelayCtrlRegVal )
{
    if ( NULL == pu32RelayCtrlRegVal )
    { return ERR_DEV_DG_POINTER_NULL; }

    u16 value = *pu32RelayCtrlRegVal;

    //! config relay
    cplatform::sysGetDsoPlatform()->m_pPhy->mDgu.switchRelay( value );

    return ERR_DEV_DG_NONE;
}

/*******************************************************************************
函 数 名: devConfigMiscReg
描    述: 配置杂项
输入参数:
          参数名称           参数类型          参数说明
          pu16MiscRegVal     u16*                  存放杂项配置参数
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigMiscReg ( u16* pu16MiscRegVal )
{
    qDebug()<<__FUNCTION__;
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_PERIPH_CS,DEV_MISC_REG);
    s32RetCode = _devFpgaConfigW(pu16MiscRegVal, DEV_MISC_CMD_LEN, u16RegAddr, CH1_FPGA);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigVCTXOReg
描    述: 配置VCTXO
输入参数:
          参数名称           参数类型                 参数说明
          pu16MiscRegVal     u16*                  存放VCTXO配置参数
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devConfigVCTXOReg ( u16* pu16VCTXORegVal )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_PERIPH_CS,DEV_VCTXO_PWM_REG);
    s32RetCode = _devFpgaConfigW(pu16VCTXORegVal, DEV_VCTXO_PWM_CMD_LEN, u16RegAddr, CH1_FPGA);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devGetFpgaStatusReg
描    述:读取FPGA状态参数
输入参数:
          参数名称           参数类型          参数说明
          pu16FpgaStatusVal     u16*                 存放读出的FPGA状态值，对应位含义有宏定义
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 devGetFpgaStatusReg ( u16* pu16FpgaStatusVal )
{
    s32    s32RetCode;
    //读取FPGA状态寄存器
    s32RetCode = _devFpgaConfigR( pu16FpgaStatusVal, DEV_FPGA_STATE_REG );

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigRamInitAddrReg
描    述: 配置该寄存器表明从RAM的那个地址开始写入数据
输入参数:
          参数名称           参数类型          参数说明
          pu32RamInitAddrRegVal     u32*                存放表示本次配置波表在RAM中起始地址的参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 _devConfigRamInitAddrReg ( u32* pu32RamInitAddrRegVal, devFpgaChanEu euChannel)
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_RAM_INIT_ADDR_REG);
    s32RetCode = _devFpgaConfigW((u16*)pu32RamInitAddrRegVal, DEV_RAM_INIT_ADDR_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigWaveDataReg
描    述: 向波表数据寄存器接口写入数据
输入参数:
          参数名称           参数类型          参数说明
          pu16WaveDataVal       u16*                指向波表首地址
          u32TxLength              u32                  波表数据个数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/

s32 _devConfigWaveDataReg ( u16* pu16WaveDataVal, u32 u32TxLength,devFpgaChanEu euChannel)
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_CTRL_WAVE_ADDR_REG);
    s32RetCode = _devFpgaConfigW(pu16WaveDataVal, u32TxLength, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: devConfigWaveTypeReg
描    述: 配置该寄存器指定未来要配置的是那一种波表
输入参数:
          参数名称           参数类型          参数说明
          pu16WaveTypeRegVal     u16*                存放表示要配置的波形类别的参数
          euChannel          devFpgaChanEu            通道
返 回 值: 错误代码
说    明:
*******************************************************************************/
s32 _devConfigWaveTypeReg ( u16* pu16WaveTypeRegVal, devFpgaChanEu euChannel )
{
    s32  s32RetCode;
    u16  u16RegAddr;

    u16RegAddr = DEV_WR_ADDR_GEN(DEV_SYS_CS,DEV_WAVE_TYPE_REG);
    s32RetCode = _devFpgaConfigW(pu16WaveTypeRegVal, DEV_WAVE_TYPE_CMD_LEN, u16RegAddr, euChannel);

    return s32RetCode;
}

/*******************************************************************************
函 数 名: _devFpgaConfigW
描    述: 按照通信协议将要发送的数据打包发送到指定通道
输入参数:
          参数名称          参数类型             参数说明
          pu16TxParaAddr    u16*                 要发送的参数起始地址
          u8TxLength        u8                   要配置的参数长度
          u16RegAddr        u16                  要配置的参数寄存器
          euChannel         devFpgaChanEu            通道
输出参数: 无
返 回 值: 错误代码
说    明:
*******************************************************************************/
s32 _devFpgaConfigW(u16 *pu16TxAddr,
                    u32 u32TxLength,
                    u16 u16RegAddr,
                    devFpgaChanEu euChannel )
{
    Q_ASSERT( NULL != pu16TxAddr );

    s32  s32RetCode = ERR_DEV_DG_NONE;

    //! command header
    u32 u32CtrlCmdHeader = 0;
    u32CtrlCmdHeader = DEV_WRITE_CMD_GEN(euChannel,u16RegAddr,0);

    //! make the fifo
    if ( NULL == _dguTransFifo )
    {
        _dguTransFifo = (u32*)malloc( DGU_TRANS_FIFO_SIZE * sizeof(u32) );
    }

    Q_ASSERT( NULL != _dguTransFifo );
    Q_ASSERT( u32TxLength <= DGU_TRANS_FIFO_SIZE );

    //! fill 0
    memset( _dguTransFifo, 0, u32TxLength * sizeof(u32) );

    //! build data
    u32* TxData = NULL;
    TxData = _dguTransFifo;
    for(u32 i = 0;i < u32TxLength;i++)
    {
       TxData[i] = (u32CtrlCmdHeader<<16) + u32 (*(pu16TxAddr + i));
    }

    //! 发送波表时，地址FPGA自加
    //! 普通多位数据（如频率、周期等），地址位需要手动处理+1
    if(u32TxLength < 10 )
    {
       for(u32 i = 0;i < u32TxLength;i++)
       {
           *(TxData + i) += i<<(4+16);
       }
    }

    //! write
    cplatform::sysGetDsoPlatform()->m_pPhy->mDgu.write(
                                   DGU_CFG_ADDR,
                                   TxData,
                                   u32TxLength
                                   );
    return s32RetCode;
}

/*******************************************************************************
函 数 名: _devFpgaConfigR
描    述: 从指定地址读取参数值，先向相应的子寄存器地址写入读命令，再从给定的一
          级寄存器地址读回FPGA返回的数据。
输入参数:
          参数名称           参数类型          参数说明
          pu16RxAddr                u16*                         存放回读数据的地址
          u16RegAddr                u16                           需要回读的寄存器地址
输出参数:
返 回 值: 错误代码
说    明:
*******************************************************************************/
s32 _devFpgaConfigR(u16 */*pu16RxAddr*/, u16 /*u16RegAddr*/)
{
//    u16  u16DummyData = 0;
    s32  s32RetCode = 0;

//    发送一个空数据 ，并在最后读取接收寄存器的值
//    s32RetCode = _devFpgaConfigW(&u16DummyData, 1, u16RegAddr, CH1_FPGA);

//    从SPI的读数据寄存器中读取最后的数据
//    extern u16 devCPLD2DguRead(void);
//    *pu16RxAddr = devCPLD2DguRead();

    return s32RetCode;
}


void devCoefFreq ( u16* coef )
{
    devDGu_coefFreq = coef;
}
/***********************************文件结束***********************************/
