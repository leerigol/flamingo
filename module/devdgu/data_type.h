
#ifndef _DATATYPE_H
#define _DATATYPE_H

/*********************************包含文件声明*********************************/

/*********************************外部变量声明*********************************/

/********************************常数和类型声明********************************/

/************************************宏声明************************************/

//typedef unsigned char bool;
typedef unsigned char       u8;
typedef char        s8;
typedef short      s16;
typedef unsigned short     u16;
typedef int      s32;
typedef unsigned int     u32;
typedef long long      s64;
typedef unsigned long long     u64;

typedef float f32;
typedef double f64;

#ifndef BOOL
#define BOOL bool
#endif

#ifndef TRUE
#define TRUE (bool)1
#endif

#ifndef FALSE
#define FALSE (bool)0
#endif

#define dg_err_none 0

#endif

