TEMPLATE = lib
TARGET = ../lib$$(PLATFORM)/dbg
INCLUDEPATH += .

QT += widgets

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

CONFIG += static

SOURCES += ../include/dsodbg.cpp

HEADERS += ../include/dsodbg.h

