
#include "iphyzynqfcuwr.h"

namespace dso_phy {

#include "iphyfcu_addr.h"
#include "../ic/zynqfcuwr.cpp"

//! chan meta
chanMeta::chanMeta()
{
    mAddr = 0;
    mSize = 0;
    mChunk = 0;
}

void chanMeta::set( quint32 addr,
                    quint32 size,
                    quint32 chunk )
{
    mAddr = addr;
    mSize = size;
    mChunk = chunk;
}
quint32 chanMeta::getAddr()
{ return mAddr; }
quint32 chanMeta::getSize()
{ return mSize; }
quint32 chanMeta::getChunk()
{ return mChunk; }

IPhyZynqFcuWr::IPhyZynqFcuWr()
{

}

void IPhyZynqFcuWr::init()
{
    //! history trace
    outtrace_wbaseaddr( trace_wbase_addr );

    //! chx buf
    //! pool -- determined by the request
    //! request:
    //!     ch_main
    //!     la_main
    //!     la_ch_main
    //!     ...

    outroll_ana_trace_wbaseaddr( roll_trace_ana_begin );
    outroll_ana_trace_waddr_end( roll_trace_ana_begin + roll_trace_ana_slice * 4 * 2 );
    outroll_la_trace_wbaseaddr( roll_trace_la_begin );
    outroll_la_trace_waddr_end( roll_trace_la_begin + roll_trace_la_slice * 16 * 2 );

    outana_main_trace_wbaseaddr( trace_ana_main_wbase_addr );
    outla_main_trace_wbaseaddr(  trace_la_main_wbase_addr );
    outana_zoom_trace_wbaseaddr( trace_ana_zoom_wbase_addr );
    outla_zoom_trace_wbaseaddr(  trace_la_zoom_wbase_addr );

    outdeinterweave_chx_raddr( trace_wbase_addr );
    //! slice
    outdeinterweave_cha_waddr( trace_deinte_addr + 0 * trace_slice_size );
    outdeinterweave_chb_waddr( trace_deinte_addr + 1 * trace_slice_size );
    outdeinterweave_chc_waddr( trace_deinte_addr + 2 * trace_slice_size );
    outdeinterweave_chd_waddr( trace_deinte_addr + 3 * trace_slice_size );

    outdeinterweave_decode_cha_waddr( trace_deinit_digi_addr_h + 0 * dig_slize_size );
    outdeinterweave_decode_chb_waddr( trace_deinit_digi_addr_h + 1 * dig_slize_size );
    outdeinterweave_decode_chc_waddr( trace_deinit_digi_addr_h + 2 * dig_slize_size );
    outdeinterweave_decode_chd_waddr( trace_deinit_digi_addr_h + 3 * dig_slize_size );

    outdeinterweave_decode_cha_waddr_sec( trace_deinit_digi_addr_l + 0 * dig_slize_size );
    outdeinterweave_decode_chb_waddr_sec( trace_deinit_digi_addr_l + 1 * dig_slize_size );
    outdeinterweave_decode_chc_waddr_sec( trace_deinit_digi_addr_l + 2 * dig_slize_size );
    outdeinterweave_decode_chd_waddr_sec( trace_deinit_digi_addr_l + 3 * dig_slize_size );

    //! lax buf
    //! pool
    outdeinterweave_lax_raddr( trace_la_main_wbase_addr );
    //! slice
    outdeinterweave_la0_waddr( la_deinte_addr + 0 * la_slice_size );
    outdeinterweave_la1_waddr( la_deinte_addr + 1 * la_slice_size );
    outdeinterweave_la2_waddr( la_deinte_addr + 2 * la_slice_size );
    outdeinterweave_la3_waddr( la_deinte_addr + 3 * la_slice_size );

    outdeinterweave_la4_waddr( la_deinte_addr + 4 * la_slice_size );
    outdeinterweave_la5_waddr( la_deinte_addr + 5 * la_slice_size );
    outdeinterweave_la6_waddr( la_deinte_addr + 6 * la_slice_size );
    outdeinterweave_la7_waddr( la_deinte_addr + 7 * la_slice_size );

    outdeinterweave_la8_waddr( la_deinte_addr + 8 * la_slice_size );
    outdeinterweave_la9_waddr( la_deinte_addr + 9 * la_slice_size );
    outdeinterweave_la10_waddr( la_deinte_addr + 10 * la_slice_size );
    outdeinterweave_la11_waddr( la_deinte_addr + 11 * la_slice_size );

    outdeinterweave_la12_waddr( la_deinte_addr + 12 * la_slice_size );
    outdeinterweave_la13_waddr( la_deinte_addr + 13 * la_slice_size );
    outdeinterweave_la14_waddr( la_deinte_addr + 14 * la_slice_size );
    outdeinterweave_la15_waddr( la_deinte_addr + 15 * la_slice_size );

    //! import addr
    outfpga_rdat_cfg4_exdat_rbaseaddr( trace_wbase_addr );

    //! init addrs
    //! analog
    for ( int i = 0; i < array_count(mCHAddrs); i++ )
    {
        mCHAddrs[i] = trace_deinte_addr + i * trace_slice_size;
    }
    //! dx
    for ( int i = 0; i < array_count(mDxAddrs); i++ )
    {
        mDxAddrs[i] = la_deinte_addr + i * la_slice_size;
    }
    //! digi h
    for ( int i = 0; i < array_count(mDigiHAddrs); i++ )
    {
        mDigiHAddrs[i] = trace_deinit_digi_addr_h + i * dig_slize_size;
    }
    //! digi l
    for ( int i = 0; i < array_count(mDigiLAddrs); i++ )
    {
        mDigiLAddrs[i] = trace_deinit_digi_addr_l + i * dig_slize_size;
    }
}

void IPhyZynqFcuWr::loadOtp()
{
    _loadOtp();
}
void IPhyZynqFcuWr::reMap()
{
    _remap_ZynqFcuWr_();
}

//! 写完成信号复位
void IPhyZynqFcuWr::endTrace()
{
    outtrace_wdone_clr( 1 );
    outtrace_wdone_clr( 0 );
}

//! 启动解交织
void IPhyZynqFcuWr::requestDeinte()
{
    outdeinterweave_property_cfg2_deinterweave_rstart( 1 );
    outdeinterweave_property_cfg2_deinterweave_rstart( 0 );
}

void IPhyZynqFcuWr::endDeinte()
{
    outdeinterweave_property_cfg3_deinterweave_done_clr( 1 );
    outdeinterweave_property_cfg3_deinterweave_done_clr( 0 );
}

int IPhyZynqFcuWr::getChanMeta( Chan ch, chanMeta *pMeta )
{
    Q_ASSERT( NULL != pMeta );

    switch( ch )
    {
        //! chx
        case chan1: pMeta->set( deinterweave_cha_waddr, 1, 1 ); break;
        case chan2: pMeta->set( deinterweave_chb_waddr, 1, 1 ); break;
        case chan3: pMeta->set( deinterweave_chc_waddr, 1, 1 ); break;
        case chan4: pMeta->set( deinterweave_chd_waddr, 1, 1 ); break;

        //! dx
        case d0: pMeta->set( deinterweave_la0_waddr, 1, 1 ); break;
        case d1: pMeta->set( deinterweave_la1_waddr, 1, 1 ); break;
        case d2: pMeta->set( deinterweave_la2_waddr, 1, 1 ); break;
        case d3: pMeta->set( deinterweave_la3_waddr, 1, 1 ); break;

        case d4: pMeta->set( deinterweave_la4_waddr, 1, 1 ); break;
        case d5: pMeta->set( deinterweave_la5_waddr, 1, 1 ); break;
        case d6: pMeta->set( deinterweave_la6_waddr, 1, 1 ); break;
        case d7: pMeta->set( deinterweave_la7_waddr, 1, 1 ); break;

        case d8: pMeta->set( deinterweave_la8_waddr, 1, 1 ); break;
        case d9: pMeta->set( deinterweave_la9_waddr, 1, 1 ); break;
        case d10: pMeta->set( deinterweave_la10_waddr, 1, 1 ); break;
        case d11: pMeta->set( deinterweave_la11_waddr, 1, 1 ); break;

        case d12: pMeta->set( deinterweave_la12_waddr, 1, 1 ); break;
        case d13: pMeta->set( deinterweave_la13_waddr, 1, 1 ); break;
        case d14: pMeta->set( deinterweave_la14_waddr, 1, 1 ); break;
        case d15: pMeta->set( deinterweave_la15_waddr, 1, 1 ); break;
#if 0 //![dba:2018-03-14, 内存数据结构变化，以前CH和LA在一起，现在独立开]
        //! sibling
        case ana: pMeta->set( deinterweave_chx_raddr, 64*4, 64*5 ); break;
        case la: pMeta->set( deinterweave_chx_raddr + 64*4, 64*1, 64*5 ); break;
        //! combine
        case ana_la: pMeta->set( deinterweave_chx_raddr, 64*5, 64*5 ); break;
#else
        /*! [dba: 2018-06-08] bug: deinterweave_chx_raddr 在zoom打开时，是zoom的地址，会导致导出数据前面一截错误*/
        case d0d7 : pMeta->set( trace_la_main_wbase_addr/*deinterweave_lax_raddr*/, 1, 1 ); break;
        case d8d15: pMeta->set( trace_la_main_wbase_addr/*deinterweave_lax_raddr*/, 1, 1 ); break;
        case d0d15: pMeta->set( trace_la_main_wbase_addr/*deinterweave_lax_raddr*/, 1, 1 ); break;
        case la   : pMeta->set( trace_la_main_wbase_addr/*deinterweave_lax_raddr*/, 1, 1 ); break;

        //! sibling
        case ana: pMeta->set(    trace_ana_main_wbase_addr/*deinterweave_chx_raddr*/, 1, 1 ); break;

        //! combine
        case ana_la: pMeta->set( trace_ana_main_wbase_addr/*deinterweave_chx_raddr*/, 1, 1 ); break;

#endif
        default: Q_ASSERT( false ); return -1;
    }

    return 0;
}

quint32 IPhyZynqFcuWr::getCHAddr( Chan ch, int chCnt )
{
    if ( chCnt == 1 )
    { return get1CHAddr( ch ); }
    else if ( chCnt == 2 )
    { return get2CHAddr( ch ); }
    else if ( chCnt == 4 )
    { return get4CHAddr( ch ); }
    else
    {
        Q_ASSERT( false );
        return 0;
    }
}

quint32 IPhyZynqFcuWr::getLaAddr(int mainOrZoom)
{
    if( mainOrZoom == 0)
    {
        return trace_la_main_wbase_addr;
    }
    else
    {
        return trace_la_zoom_wbase_addr;
    }
}

quint32 IPhyZynqFcuWr::get1CHAddr( Chan ch )
{
    //! analog
    if ( ch >= chan1 && ch <= chan4 )
    { return mCHAddrs[0]; }
    //! dx
    else if ( ch >= d0 && ch <= d15 )
    { return mDxAddrs[ ch - d0 ]; }
    //! digi h
    else if ( ch >= digi_ch1 && ch <= digi_ch4 )
    { return mDigiHAddrs[0]; }
    //! digi l
    else if ( ch >= digi_ch1_l && ch <= digi_ch4_l )
    { return mDigiLAddrs[0]; }
    //! eye
    else if ( ch >= eye_ch1 && ch <= eye_ch4 )
    { return mCHAddrs[0]; }
    else if ( ch == la )
    { return deinterweave_lax_raddr; }
    else
    {
        Q_ASSERT( false );
        return 0;
    }
}

quint32 IPhyZynqFcuWr::get2CHAddr( Chan ch )
{
    //! analog
    if ( ch >= chan1 && ch <= chan2 )
    { return mCHAddrs[0]; }
    else if ( ch >= chan3 && ch <= chan4 )
    { return mCHAddrs[1]; }
    //! dx
    else if ( ch >= d0 && ch <= d15 )
    { return mDxAddrs[ ch - d0 ]; }
    //! digi h
    else if ( ch >= digi_ch1 && ch <= digi_ch2 )
    { return mDigiHAddrs[0]; }
    else if ( ch >= digi_ch3 && ch <= digi_ch4 )
    { return mDigiHAddrs[1]; }
    //! digi l
    else if ( ch >= digi_ch1_l && ch <= digi_ch2_l )
    { return mDigiLAddrs[0]; }
    else if ( ch >= digi_ch3_l && ch <= digi_ch4_l )
    { return mDigiLAddrs[1]; }
    //! eye
    else if ( ch >= eye_ch1 && ch <= eye_ch2 )
    { return mCHAddrs[0]; }
    else if ( ch >= eye_ch3 && ch <= eye_ch4 )
    { return mCHAddrs[1]; }
    else if ( ch == la )
    { return deinterweave_lax_raddr; }
    else
    {
        Q_ASSERT( false );
        return 0;
    }
}

quint32 IPhyZynqFcuWr::get4CHAddr( Chan ch )
{
    //! analog
    if ( ch >= chan1 && ch <= chan4 )
    { return mCHAddrs[ch - chan1]; }
    //! dx
    else if ( ch >= d0 && ch <= d15 )
    { return mDxAddrs[ ch - d0 ]; }
    //! digi h
    else if ( ch >= digi_ch1 && ch <= digi_ch4 )
    { return mDigiHAddrs[ ch - digi_ch1 ]; }
    //! digi l
    else if ( ch >= digi_ch1_l && ch <= digi_ch4_l )
    { return mDigiLAddrs[ ch - digi_ch1_l ]; }
    //! eye
    else if ( ch >= eye_ch1 && ch <= eye_ch4 )
    { return mCHAddrs[ch - eye_ch1]; }
    else if ( ch == la )
    { return deinterweave_lax_raddr; }
    else
    {
        Q_ASSERT( false );
        return 0;
    }
}

}
