#ifndef IPHYFCU_ADDR
#define IPHYFCU_ADDR


//! config
#define trace_wbase_addr    0x1c000000  //! 4M + 2M = 6M
                                        //! ch + la
#define trace_slice_size    0x100000

#define trace_ana_main_wbase_addr  trace_wbase_addr
#define trace_la_main_wbase_addr   trace_ana_main_wbase_addr + 4 * trace_slice_size
#define trace_ana_zoom_wbase_addr  0x1c800000
#define trace_la_zoom_wbase_addr   trace_ana_zoom_wbase_addr + 4 * trace_slice_size

#define trace_deinte_addr   0x1d000000  //! 4M + 4M/8*2 + 2M = 7M
                                        //! ch + digH + digL + la

#define la_deinte_addr      (trace_deinte_addr+0x800000)

#define la_slice_size      (trace_slice_size/8)
#define dig_slize_size     (trace_slice_size/8)

#define roll_trace_ana_slice  1000000
#define roll_trace_la_slice   roll_trace_ana_slice/8
#define roll_trace_ana_begin  0x1c000000
#define roll_trace_la_begin   0x1c800000

#define trace_deinit_digi_addr_h  (trace_deinte_addr+4*trace_slice_size)
#define trace_deinit_digi_addr_l  (trace_deinit_digi_addr_h+4*dig_slize_size)



#endif // IPHYFCU_ADDR

