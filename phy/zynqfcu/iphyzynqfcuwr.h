#ifndef IPHYZYNQFCUWR
#define IPHYZYNQFCUWR

#include "../iphyfpga.h"

namespace dso_phy
{

#include "../ic/zynqfcuwr.h"

class chanMeta
{
public:
    chanMeta();

public:
    void set( quint32 addr, quint32 size, quint32 chunk );
    quint32 getAddr();
    quint32 getSize();
    quint32 getChunk();

public:
    quint32 mAddr;      //! address
    quint32 mSize;      //! data size
    quint32 mChunk;     //! data chunk
};

class IPhyZynqFcuWr : public ZynqFcuWrMem
{
public:
    IPhyZynqFcuWr();

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();

public:
    void endTrace();

    void requestDeinte();
    void endDeinte();

    void setAnaSum( int src, int sum );

    int getChanMeta( Chan ch, chanMeta *pMeta );

    quint32 getCHAddr( Chan ch, int chCnt );
    quint32 getLaAddr( int mainOrZoom );
    quint32 get1CHAddr( Chan ch );
    quint32 get2CHAddr( Chan ch );
    quint32 get4CHAddr( Chan ch );

public:
    quint32 mCHAddrs[4];
    quint32 mDxAddrs[16];
    quint32 mDigiHAddrs[4];
    quint32 mDigiLAddrs[4];

};

}

#endif // IPHYZYNQFCUWR

