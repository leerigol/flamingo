#ifndef IPHYZYNQFCU_H
#define IPHYZYNQFCU_H


#include "../iphyfpga.h"

namespace dso_phy
{

#include "../ic/zynqfcurd.h"


class IPhyZynqFcuRd : public ZynqFcuRdMem
{
public:
    IPhyZynqFcuRd();

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();

public:

};

}

#endif
