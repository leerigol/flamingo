TEMPLATE = lib

TARGET = ../../lib$$(PLATFORM)/phy/iphyzynqfcu
CONFIG += static


#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

HEADERS += \
    iphyzynqfcuwr.h \
    iphyzynqfcurd.h \
    iphyfcu_addr.h

SOURCES += \
    iphyzynqfcuwr.cpp \
    iphyzynqfcurd.cpp
