#ifndef IPHYLED_H
#define IPHYLED_H

#include "iphy.h"

namespace dso_phy {

class IPhyLed : public IPhy
{
public:
    IPhyLed();

public:
    void seLedOp( int led, bool b );
    int getLed();
private:
    int mLed;
};

}

#endif // IPHYLED_H
