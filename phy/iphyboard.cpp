#include "iphyboard.h"

namespace dso_phy {

#define CPLD_VER_ADDR   0x00
#define MB_VER_ADDR     0x0b

#define SWITCH_ADDR     0x06
#define SWITCH_COL      7

#define board_chan      0

IPhyBoard::IPhyBoard()
{
    mbDirty = true;
}

//! get info from board
void IPhyBoard::loadOtp()
{
    refresh();
}

quint16 IPhyBoard::getCpldVer()
{
    refresh();

    return mCpldVer;
}

quint16 IPhyBoard::getMBVer()
{
    refresh();

    return mMBVer;
}

void IPhyBoard::setSwitch( bool b )
{ 
    push( 7, b | 0x02 );
}

bool IPhyBoard::getSwitch()
{
    board_reg val;
    push( 0x10, 0x1 );
    usleep(100);
    val =  pull( 0x11 );

    return (val & 0x1);
}

void IPhyBoard::setRePower()
{   
    push( 6, 1<<7 );
}

void IPhyBoard::setShutDown()
{  
    push( 6, 1<<6 );
}

void IPhyBoard::rstCtp()
{
//    push( 10, 0<<1 );
//    push( 10, 1<<1 );
}

void IPhyBoard::clrInt()
{
    push( 9, 1<<0 );
}

void IPhyBoard::maskInt(int mask)
{
    board_reg val = (mask & 0x1ff);
    push(15, val);
}

board_reg IPhyBoard::readIntStat()
{
    return pull( 13 );

}
board_reg IPhyBoard::readIntEn()
{
    return pull( 14 );
}
board_reg IPhyBoard::readIntMask()
{
    return pull( 15 );
}

void IPhyBoard::testWrite( board_reg tst )
{
    push( 255, tst );
}
board_reg IPhyBoard::testRead()
{
    return pull( 255 );
}

/*!
 * \brief IPhyBoard::refresh
 * get data from device
 */
void IPhyBoard::refresh()
{
    if ( mbDirty )
    {}
    else
    {  return; }

    //! versions 
    mCpldVer = pull( CPLD_VER_ADDR );
    mMBVer = pull( MB_VER_ADDR );

    mbDirty = false;
}

void IPhyBoard::push( quint8 addr, board_reg val )
{
    Q_ASSERT( NULL != m_pBus );

    toLE( val );
    m_pBus->write( val, addr, board_chan );
}
board_reg IPhyBoard::pull( quint8 addr )
{
    Q_ASSERT( NULL != m_pBus );

    board_reg rawData;
    rawData = 0;
    m_pBus->read( rawData, addr, board_chan );
    toLE( rawData );

    return rawData;
}

}

