TEMPLATE = lib

QT += widgets

TARGET = ../lib$$(PLATFORM)/phy/iphy
CONFIG += static

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

# dbg show
dbg = $$(_DEBUG)
equals(dbg, _DEBUG){
DEFINES += _DEBUG
}

model = $$(_MODEL)
equals( model, _DS8000){
DEFINES += _DS8000
}

equals( model, _DS7000){
DEFINES += _DS7000
}

HEADERS += \
    iphybeeper.h \
    iphy.h \
    cdsophy.h \
    iphylcd.h \
    iphyfan.h \
    iphyboard.h \
    iphypll.h \
    iphyled.h \
    iphydac.h \
    iphyfpga.h


SOURCES += \
    iphybeeper.cpp \
    iphy.cpp \
    cdsophy.cpp \
    iphylcd.cpp \
    iphyfan.cpp \
    iphyboard.cpp \
    iphypll.cpp \
    iphyled.cpp \
    iphydac.cpp \
    iphyfpga.cpp

