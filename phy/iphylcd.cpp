#include "iphylcd.h"

namespace dso_phy {

#define LIGHT_ROW   5

#define LIGHT_LED_COL   7
#define LIGHT_CHAN      0
IPhyLcd::IPhyLcd()
{
}

/*!
 * \brief IPhyLcd::setBklight
 * \param light
 * 0~100
 * 背光为0时关闭led电源
 */
void IPhyLcd::setBklight( int light )
{
    unsigned short busVal;

    //qDebug()<<__FUNCTION__<<__LINE__<<light;

    if ( light <= 0 )
    {
        busVal = (1<<7);
        toLE( busVal );
        m_pBus->write( busVal,
                       LIGHT_ROW,
                       LIGHT_CHAN );
    }
    else
    {
        if ( light >= 100 )
        { light = 100; }

        busVal = light | (1<<LIGHT_LED_COL);
        toLE( busVal );
        m_pBus->write( busVal, LIGHT_ROW, LIGHT_CHAN );
    }
}


}
