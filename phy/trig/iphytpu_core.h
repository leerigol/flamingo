#ifndef IPHYTPU_CORE_H
#define IPHYTPU_CORE_H

#include "../ic/tpu.h"

struct tpuCfg
{
    Tpu_reg addr;
    Tpu_reg data;
};

//! class api type
class IPhyTpu;
typedef void (TpuMem::*mutable_set)( Tpu_reg );
typedef Tpu_reg (TpuMem::*mutable_get)(  );

//! level
struct levelSets
{
    mutable_set level_ha;
    mutable_set level_la;
    mutable_set level_hb;
    mutable_set level_lb;
};

struct eventSets
{
    mutable_set logic_analog_la_sel;
    mutable_set logic_analog_sel;
    mutable_set logic_func;

    mutable_set cmpa_sel;
    mutable_set cmpa_mode;
    mutable_set cmpb_sel;
    mutable_set cmpb_mode;

    mutable_set clk_sel;
    mutable_set clk_inv;
    mutable_set dat_sel;
    mutable_set dat_inv;

    mutable_set width_cmp_l_31_0;
    mutable_set width_cmp_g_31_0;
    mutable_set width_cmp_l_39_32;
    mutable_set width_cmp_g_39_32;
    mutable_set width_cmp_mode;

    //! reg1/2
    mutable_set setReg1;
    mutable_set setReg2;

    mutable_get getReg1;
    mutable_get getReg2;
};

class IPhyTpu : public TpuMem
{
private:
    const static quint64 _c_trig_hold_tick = 3200;  //! ps = 1/312.5M
    const static quint64 _c_trig_tick      = 400 ;  //! ps = 1/2.5G

public:
    IPhyTpu();

private:
    void loadApiGroup();

public:
    virtual void      init();
    virtual void      loadOtp();
    virtual void      reMap();
    virtual quint64   get_c_tick();
    virtual quint64   get_td_tick();
public:
    enum eEventCh
    {
        event_a = 0,
        event_b,
        event_r,
    };

    enum eEventReg
    {
        event_reg1,
        event_reg2,
        event_reg3,
        event_reg4,

        event_reg5,
        event_reg6,
    };

    enum eLogicSel
    {
        logic_1 = 0,
        logic_0,
        logic_pass,
        logic_invert,
    };

    enum eLogicOp
    {
        op_and = 0,
        op_or,
        op_xor,
    };

    enum eChSeq
    {
        c_a1 = 0,c_a2,c_a3,c_a4,
        c_d0, c_d1, c_d2, c_d3,
        c_d4, c_d5, c_d6, c_d7,
        c_d8, c_d9, c_d10, c_d11,
        c_d12, c_d13, c_d14, c_d15,
        c_ext,
    };

    enum eCmpSrc
    {
        ch1_a = 0, ch1_b,
        ch2_a, ch2_b,
        ch3_a, ch3_b,
        ch4_a, ch4_b,

        la0 = 0x20, la1, la2, la3,
        la4, la5, la6, la7,
        la8, la9, la10, la11,
        la12, la13, la14, la15,

        ac = 0x30,
        ext = 0x31,
        dg1_sync = 0x32,
        dg2_sync = 0x33,

        invalid,
    };

    enum eCmpMode
    {
        rise = 0,
        fall,
        any,
    };

    enum eCmpName
    {
        logic = 0,
        cmp_c,
        cmp_a,
        cmp_b,
        cmp_a_sample,
        cmp_c_opti = 6,
    };

    enum eWidthCmp
    {
        gt = 0,
        lt,
        in_range,
        out_range,
        tmo
    };

    enum eTrigSrc
    {
        a = 0,
        a_or_b,
        a_and_b,
        abr,
        uart,
        iis,
        lin,
        can,

        flexray,
        arinc429,
        mil1553b,
        sent,

        spi,
        iic,
        video,
    };

    enum eHoldType
    {
        event_hold = 0,
        time_hold,
    };

    enum eIirType
    {
        iir_dc = 0,
        iir_hf,
        iir_lf,
        iir_ac,
    };

public:
    //! ch: 0~3
    void setLevelA( eChSeq ch, Tpu_reg l, Tpu_reg h );
    void setLevelB( eChSeq ch, Tpu_reg l, Tpu_reg h );

    //! eventCH: 0~2, a/b/r
    //! ch: 0~19---ch-la,20-ext
    void setEventLogic( eEventCh eventCh,
                        eChSeq ch,
                        eLogicSel evtLogic );
    void setEventLogicFunc( eEventCh eventCh,
                            eLogicOp func );

    void setEventCmpA( eEventCh eventCh,
                       eCmpSrc cmpSrc,
                       eCmpMode cmpMode = rise
                       );

    void setEventCmpB( eEventCh eventCh,
                       eCmpSrc cmpSrc,
                       eCmpMode cmpMode = rise );


    void setEventClk( eEventCh eventCh,
                      eCmpName clk,
                      Tpu_reg clkInv=0 );

    void setEventDat( eEventCh eventCh,
                      eCmpName dat,
                      Tpu_reg datInv=0 );

    void setEventWidthL( eEventCh eventCh,
                         quint64 width );
    void setEventWidthH( eEventCh eventCh,
                         quint64 width );
    void setEventWidthCmp( eEventCh eventCh,
                           eWidthCmp cmp );

    //! a event
    void aEventLogic( eChSeq seq, eLogicSel sel );
    void aEventLogicFunc( eLogicOp func );

    void aEventCmpa( eCmpSrc eSrc, eCmpMode eMode = rise );
    void aEventCmpb( eCmpSrc eSrc, eCmpMode eMode = rise );
    void aEventClkDat( eCmpName clkSrc, bool bClkInv,
                       eCmpName datSrc, bool bDatInv );

    void aEventWidth( eWidthCmp cmp = gt,
                      quint64 widL=0,
                      quint64 widH=0 );

    //! b event
    void bEventLogic( eChSeq seq, eLogicSel sel );
    void bEventLogicFunc( eLogicOp func );

    void bEventCmpa( eCmpSrc eSrc, eCmpMode eMode = rise );
    void bEventCmpb( eCmpSrc eSrc, eCmpMode eMode = rise );
    void bEventClkDat( eCmpName clkSrc, bool bClkInv,
                       eCmpName datSrc, bool bDatInv );
    void bEventWidth( eWidthCmp cmp = gt,
                      quint64 widL=0,
                      quint64 widH=0 );
    //! r event
    void rEventLogic( eChSeq seq, eLogicSel sel );
    void rEventLogicFunc( eLogicOp func );

    void rEventCmpa( eCmpSrc eSrc, eCmpMode eMode = rise );
    void rEventCmpb( eCmpSrc eSrc, eCmpMode eMode = rise );
    void rEventClkDat( eCmpName clkSrc, bool bClkInv,
                       eCmpName datSrc, bool bDatInv );
    void rEventWidth( eWidthCmp cmp = gt,
                      quint64 widL=0,
                      quint64 widH=0 );

    //! trig control
    void setTrigSrc( eTrigSrc src = a );

    void setTrigRst();
    void setTrigForce();
    void setTrigLoopOn( Tpu_reg en=0 );
    void setTrigDelayOn( Tpu_reg en=0 );

    void setTrigTmoOn( Tpu_reg en=0 );
    void setTrigREventOn( Tpu_reg en=0 );
    //! 用于区分通道数据模式
    void setChOn( Tpu_reg en );

    void setTrigDelay( quint64 dly );
    void setTrigTmo( quint64 tmo );

    void setEventNum( Tpu_reg num, eEventCh eventCh = event_b );

    void setHoldoffTime( quint64 time );
    quint64 getHoldOffTime();
    void setHoldoffNum( Tpu_reg num );
    void setHoldOffType( eHoldType mode );

    void setIirType( eIirType type );
    void setIirSrc( eChSeq src );
    void rstIir();

private:
    levelSets level_apis[4];
    eventSets event_apis[3];

private:
    quint64 mHoldOffTime;

};


#endif // IPHYTPU_CORE_H

