#include "iphytpu.h"

namespace dso_phy {

#define assert_eventCh()   Q_ASSERT( eventCh >= event_a && eventCh <= event_r );

static tpuCfg _tpuSequence[]=
{
    {0x1804,0x50a7778},
    {0x185c,0x3c00},
    {0x181c,0xbb0000},
    {0x1820,0x1},

    {0x1824,0x10},
    {0x1828,0x0},
};

#include "../ic/tpu.cpp"

IPhyTpu::IPhyTpu()
{
    mHoldOffTime = 0;

    loadApiGroup();

}

void IPhyTpu::init()
{
    int i;

    for ( i = 0;
          i < array_count(_tpuSequence);
          i++ )
    {
        m_pBus->write( _tpuSequence[i].data,
                       _tpuSequence[i].addr );
    }
}

void IPhyTpu::loadOtp()
{
    _loadOtp();
}

void IPhyTpu::reMap()
{
    _remap_Tpu_();
}

quint64 IPhyTpu::get_c_tick()
{
    return _c_trig_tick;
}

quint64 IPhyTpu::get_td_tick()
{
    return _c_trig_hold_tick;  //! 1/312.5M = 3200 ps
}


//! ch: 0~3
void IPhyTpu::setLevelA( eChSeq ch,
                         Tpu_reg lvlL,
                         Tpu_reg lvlH )
{
    Q_ASSERT( ch >= c_a1 && ch <= c_a4 );

//    chx_cmp_level[ ch - c_a1 ].lvl_la = lvlL;
//    chx_cmp_level[ ch - c_a1 ].lvl_ha = lvlH;

//    wcache( &chx_cmp_level[ ch - c_a1 ] );

     LOG_PHY()<<lvlL<<lvlH;

    (this->*(level_apis[ ch - c_a1 ].level_la))( lvlL );
    (this->*(level_apis[ ch - c_a1 ].level_ha))( lvlH );



}
void IPhyTpu::setLevelB( eChSeq ch,
                         Tpu_reg lvlL,
                         Tpu_reg lvlH )
{
    Q_ASSERT( ch >= c_a1 && ch <= c_a4 );

    (this->*(level_apis[ ch - c_a1 ].level_lb))( lvlL );
    (this->*(level_apis[ ch - c_a1 ].level_hb))( lvlH );
}

//! 设置组合逻辑
//! eventCH: 0~2, a/b/r
//! ch: 0~19---ch-la,20-ext
//! 对应于码型触发
void IPhyTpu::setEventLogic( eEventCh eventCh,
                    eChSeq ch,
                    eLogicSel evtLogic )
{
    assert_eventCh();
    Q_ASSERT( ch >= c_a1 && ch <= c_ext );

    Tpu_reg reg;
    if ( ch < c_d12 )
    {
        reg = (this->*(event_apis[eventCh - event_a ].getReg1))();

        setBits( reg,
                 evtLogic,
                 (ch-c_a1)* 2,
                 2 );

        (this->*(event_apis[eventCh - event_a ].setReg1))( reg );
    }
    else if ( ch <= c_ext )
    {
        reg = (this->*(event_apis[eventCh - event_a ].getReg2))();

        setBits( reg,
                 evtLogic,
                 (ch-c_d12)* 2,
                 2 );

        (this->*(event_apis[eventCh - event_a ].setReg2))( reg );
    }
    else
    {
        Q_ASSERT( false );
    }
}

//! 组合逻辑运算符
void IPhyTpu::setEventLogicFunc( eEventCh eventCh,
                        eLogicOp func )
{
    assert_eventCh();

//    x_event[ eventCh - event_a ].a_event_reg2.func = func;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg2 );

    (this->*(event_apis[eventCh - event_a ].logic_func))( func );


}
//! A 信号源
void IPhyTpu::setEventCmpA( eEventCh eventCh,
                            eCmpSrc cmpSrc,
                            eCmpMode cmpMode)
{
    assert_eventCh();

//    x_event[ eventCh-event_a].a_event_reg3.asel = cmpSrc;
//    x_event[ eventCh-event_a].a_event_reg3.amode = cmpMode;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg3 );

    (this->*(event_apis[eventCh - event_a ].cmpa_sel))( cmpSrc );
    (this->*(event_apis[eventCh - event_a ].cmpa_mode))( cmpMode );
}
//! B信号源
void IPhyTpu::setEventCmpB( eEventCh eventCh,
                            eCmpSrc cmpSrc,
                            eCmpMode cmpMode )
{
    assert_eventCh();

//    x_event[ eventCh-event_a].a_event_reg3.bsel = cmpSrc;
//    x_event[ eventCh-event_a].a_event_reg3.bmode = cmpMode;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg3 );

    (this->*(event_apis[eventCh - event_a ].cmpb_sel))( cmpSrc );
    (this->*(event_apis[eventCh - event_a ].cmpb_mode))( cmpMode );
}


//! 事件管理器时钟选择
void IPhyTpu::setEventClk( eEventCh eventCh,
                  eCmpName clk,
                  Tpu_reg clkInv )
{
    assert_eventCh();

//    x_event[ eventCh-event_a].a_event_reg3.clk = clk;
//    x_event[ eventCh-event_a].a_event_reg3.clk_inv = clkInv;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg3 );

    (this->*(event_apis[eventCh - event_a ].clk_sel))( clk );
    (this->*(event_apis[eventCh - event_a ].clk_inv))( clkInv );
}
//! 事件管理器数据选择
void IPhyTpu::setEventDat( eEventCh eventCh,
                  eCmpName dat,
                  Tpu_reg datInv )
{
    assert_eventCh();

//    x_event[ eventCh-event_a].a_event_reg3.dat = dat;
//    x_event[ eventCh-event_a].a_event_reg3.dat_inv = datInv;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg3 );

    (this->*(event_apis[eventCh - event_a ].dat_sel))( dat );
    (this->*(event_apis[eventCh - event_a ].dat_inv))( datInv );
}

//! 事件管理器脉宽设置
//! width in ps
void IPhyTpu::setEventWidthL( eEventCh eventCh,
                     quint64 width )
{
    assert_eventCh();

    quint64 unitWidth = width / get_c_tick() /*IPhyTpu::_c_trig_tick*/;
//    qDebug()<<__FILE__<<__LINE__
//           <<"user-l-w:"<<width<<"phy-l-w:"<<unitWidth
//           <<"tick:"<<get_c_tick();

//    x_event[ eventCh-event_a ].a_event_reg4.width = unitWidth;
//    x_event[ eventCh-event_a ].a_event_reg6.lw_x8 = ( unitWidth >> 32) & 0xff;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg4 );
//    wcache( &x_event[ eventCh - event_a ].a_event_reg6 );

    (this->*(event_apis[eventCh - event_a ].width_cmp_l_31_0))( unitWidth );
    (this->*(event_apis[eventCh - event_a ].width_cmp_l_39_32))( ( unitWidth >> 32) & 0xff );

}

//! 脉宽H
void IPhyTpu::setEventWidthH( eEventCh eventCh,
                     quint64 width )
{
    assert_eventCh();

    quint64 unitWidth = width / get_c_tick() /*IPhyTpu::_c_trig_tick*/;
//    qDebug()<<__FILE__<<__LINE__
//           <<"user-h-w:"<<width<<"phy-h-w:"<<unitWidth
//           <<"tick:"<<get_c_tick();

//    x_event[ eventCh-event_a ].a_event_reg5.width = unitWidth;
//    x_event[ eventCh-event_a ].a_event_reg6.hw_x8 = ( unitWidth >> 32) & 0xff;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg5 );
//    wcache( &x_event[ eventCh - event_a ].a_event_reg6 );

    (this->*(event_apis[eventCh - event_a ].width_cmp_g_31_0))( unitWidth );
    (this->*(event_apis[eventCh - event_a ].width_cmp_g_39_32))( ( unitWidth >> 32) & 0xff );
}
//! 脉宽比较运算符
void IPhyTpu::setEventWidthCmp( eEventCh eventCh,
                       eWidthCmp cmp )
{
    assert_eventCh();

//    x_event[ eventCh-event_a ].a_event_reg6.compare = cmp;

//    wcache( &x_event[ eventCh - event_a ].a_event_reg6 );

    (this->*(event_apis[eventCh - event_a ].width_cmp_mode))( cmp );
}

//! help func
//! a event
void IPhyTpu::aEventLogic( eChSeq seq, eLogicSel sel )
{
    setEventLogic( event_a, seq, sel );
}
void IPhyTpu::aEventLogicFunc( eLogicOp func )
{
    setEventLogicFunc( event_a, func );
}
void IPhyTpu::aEventCmpa( eCmpSrc eSrc, eCmpMode eMode  )
{
    setEventCmpA( event_a, eSrc, eMode );
}
void IPhyTpu::aEventCmpb( eCmpSrc eSrc, eCmpMode eMode  )
{
    setEventCmpB( event_a, eSrc, eMode);
}
void IPhyTpu::aEventClkDat( eCmpName clkSrc, bool bClkInv,
                   eCmpName datSrc, bool bDatInv )
{
    setEventClk( event_a, clkSrc, bClkInv );
    setEventDat( event_a, datSrc, bDatInv );
}
void IPhyTpu::aEventWidth( eWidthCmp cmp,
                  quint64 widL,
                  quint64 widH )
{
    setEventWidthH( event_a, widH );
    setEventWidthL( event_a, widL );
    setEventWidthCmp( event_a, cmp );
}

//! b event
void IPhyTpu::bEventLogic( eChSeq seq, eLogicSel sel )
{
    setEventLogic( event_b, seq, sel );
}
void IPhyTpu::bEventLogicFunc( eLogicOp func )
{
    setEventLogicFunc( event_b, func );
}
void IPhyTpu::bEventCmpa( eCmpSrc eSrc, eCmpMode eMode  )
{
    setEventCmpA( event_b, eSrc, eMode );
}
void IPhyTpu::bEventCmpb( eCmpSrc eSrc, eCmpMode eMode  )
{
    setEventCmpB( event_b, eSrc, eMode);
}
void IPhyTpu::bEventClkDat( eCmpName clkSrc, bool bClkInv,
                   eCmpName datSrc, bool bDatInv )
{
    setEventClk( event_b, clkSrc, bClkInv );
    setEventDat( event_b, datSrc, bDatInv );
}
void IPhyTpu::bEventWidth( eWidthCmp cmp,
                  quint64 widL,
                  quint64 widH )
{
    setEventWidthH( event_b, widH );
    setEventWidthL( event_b, widL );
    setEventWidthCmp( event_b, cmp );
}

//! r event
void IPhyTpu::rEventLogic( eChSeq seq, eLogicSel sel )
{
    setEventLogic( event_r, seq, sel );
}
void IPhyTpu::rEventLogicFunc( eLogicOp func )
{
    setEventLogicFunc( event_r, func );
}
void IPhyTpu::rEventCmpa( eCmpSrc eSrc, eCmpMode eMode  )
{
    setEventCmpA( event_r, eSrc, eMode );
}
void IPhyTpu::rEventCmpb( eCmpSrc eSrc, eCmpMode eMode  )
{
    setEventCmpB( event_r, eSrc, eMode);
}
void IPhyTpu::rEventClkDat( eCmpName clkSrc, bool bClkInv,
                   eCmpName datSrc, bool bDatInv )
{
    setEventClk( event_r, clkSrc, bClkInv );
    setEventDat( event_r, datSrc, bDatInv );
}
void IPhyTpu::rEventWidth( eWidthCmp cmp,
                  quint64 widL,
                  quint64 widH )
{
    setEventWidthH( event_r, widH );
    setEventWidthL( event_r, widL );
    setEventWidthCmp( event_r, cmp );
}

//! trig control
void IPhyTpu::setTrigSrc( eTrigSrc src )
{
    settpu_trig_mux_tpu_trig_type( src );
}

void IPhyTpu::setTrigRst()
{
    outtpu_trig_mux_trig_rst( 1 );
    outtpu_trig_mux_trig_rst( 0 );
}

void IPhyTpu::setTrigForce()
{
    outtpu_trig_mux_force_trig( 1 );
    outtpu_trig_mux_force_trig( 0 );
}

void IPhyTpu::setTrigLoopOn(unsigned int en)
{
    settpu_trig_mux_abr_seq_loop_en(en);
}

void IPhyTpu::setTrigDelayOn(unsigned int en)
{
    settpu_trig_mux_abr_seq_delay_on(en);
}

void IPhyTpu::setTrigTmoOn(unsigned int en)
{
    settpu_trig_mux_abr_seq_timeout_on(en);
}

void IPhyTpu::setTrigREventOn(unsigned int en)
{
    settpu_trig_mux_abr_seq_r_event_on(en);
}

void IPhyTpu::setTrigDelay( quint64 dly )
{
    quint64 alignVal;

    alignVal = dly / IPhyTpu::_c_trig_tick;

    setabr_delay_time_l_abr_delay_time_l( alignVal );
    setabr_delay_time_h_abr_delay_time_h( (alignVal>>32)&0xff );
}
void IPhyTpu::setTrigTmo( quint64 tmo )
{
    quint64 alignVal;

    alignVal = tmo / IPhyTpu::_c_trig_hold_tick;

    setabr_timeout_l_abr_timeout_l( alignVal );
    setabr_timeout_h_abr_timeout_h( (alignVal>>32)&0xff );
}

void IPhyTpu::setEventNum(unsigned int num, IPhyTpu::eEventCh eventCh)
{
    switch (eventCh)
    {
    case event_a:
        assignabr_a_event_num(num);
        break;
    case event_b:
        assignabr_b_event_num(num);
        break;
    default:
        Q_ASSERT(false);
        break;
    }
}

void IPhyTpu::setHoldoffTime( quint64 time )
{
    quint64 alignVal;

    mHoldOffTime = time;

    alignVal = time / IPhyTpu::_c_trig_hold_tick;

    setholdoff_time_l_holdoff_time_l( alignVal );
    setholdoff_time_h_holdoff_time_h( (alignVal>>32)&0xff );
    setholdoff_time_h_holdoff_type(4);
}

quint64 IPhyTpu::getHoldOffTime()
{
    return mHoldOffTime;
}

void IPhyTpu::setIirSrc( eChSeq src )
{
    Q_ASSERT( src >= c_a1 && src <= c_a4 );

    setiir_set_iir_src( src );
}

void IPhyTpu::rstIir()
{   
    outiir_set_iir_type( iir_dc );
}

}

