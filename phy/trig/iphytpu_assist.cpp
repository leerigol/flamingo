
#include "iphytpu.h"

namespace dso_phy
{

void IPhyTpu::loadApiGroup()
{
    //! level apis
    level_apis[0].level_la = &IPhyTpu::setch1_cmp_level_lvl_1_l_a;
    level_apis[0].level_ha = &IPhyTpu::setch1_cmp_level_lvl_1_h_a;
    level_apis[0].level_lb = &IPhyTpu::setch1_cmp_level_lvl_1_l_b;
    level_apis[0].level_hb = &IPhyTpu::setch1_cmp_level_lvl_1_h_b;

    level_apis[1].level_la = &IPhyTpu::setch2_cmp_level_lvl_2_l_a;
    level_apis[1].level_ha = &IPhyTpu::setch2_cmp_level_lvl_2_h_a;
    level_apis[1].level_lb = &IPhyTpu::setch2_cmp_level_lvl_2_l_b;
    level_apis[1].level_hb = &IPhyTpu::setch2_cmp_level_lvl_2_h_b;

    level_apis[2].level_la = &IPhyTpu::setch3_cmp_level_lvl_3_l_a;
    level_apis[2].level_ha = &IPhyTpu::setch3_cmp_level_lvl_3_h_a;
    level_apis[2].level_lb = &IPhyTpu::setch3_cmp_level_lvl_3_l_b;
    level_apis[2].level_hb = &IPhyTpu::setch3_cmp_level_lvl_3_h_b;

    level_apis[3].level_la = &IPhyTpu::setch4_cmp_level_lvl_4_l_a;
    level_apis[3].level_ha = &IPhyTpu::setch4_cmp_level_lvl_4_h_a;
    level_apis[3].level_lb = &IPhyTpu::setch4_cmp_level_lvl_4_l_b;
    level_apis[3].level_hb = &IPhyTpu::setch4_cmp_level_lvl_4_h_b;

    //! event api
    //! a event
    event_apis[0].logic_analog_la_sel = &IPhyTpu::setA_event_reg2_A_event_logic_analog_sel;
    event_apis[0].logic_analog_sel = &IPhyTpu::setA_event_reg1_A_event_logic_la_sel;
    event_apis[0].logic_func = &IPhyTpu::setA_event_reg2_A_event_logic_func;

    event_apis[0].cmpa_sel = &IPhyTpu::setA_event_reg3_A_event_cmpa_sel;
    event_apis[0].cmpa_mode = &IPhyTpu::setA_event_reg3_A_event_cmpa_mode;
    event_apis[0].cmpb_sel = &IPhyTpu::setA_event_reg3_A_event_cmpb_sel;
    event_apis[0].cmpb_mode = &IPhyTpu::setA_event_reg3_A_event_cmpb_mode;

    event_apis[0].clk_sel = &IPhyTpu::setA_event_reg3_A_event_clk_sel;
    event_apis[0].clk_inv = &IPhyTpu::setA_event_reg3_A_event_clk_sel_inv;
    event_apis[0].dat_sel = &IPhyTpu::setA_event_reg3_A_event_dat_sel;
    event_apis[0].dat_inv = &IPhyTpu::setA_event_reg3_A_event_dat_sel_inv;

    event_apis[0].width_cmp_l_31_0 = &IPhyTpu::setA_event_reg4_A_event_width_cmp_1_31_0;
    event_apis[0].width_cmp_g_31_0 = &IPhyTpu::setA_event_reg5_A_event_width_cmp_g_31_0;
    event_apis[0].width_cmp_l_39_32 = &IPhyTpu::setA_event_reg6_A_event_width_cmp_1_39_32;
    event_apis[0].width_cmp_g_39_32 = &IPhyTpu::setA_event_reg6_A_event_width_cmp_g_39_32;
    event_apis[0].width_cmp_mode = &IPhyTpu::setA_event_reg6_A_event_width_cmp_mode;

    event_apis[0].setReg1 = &IPhyTpu::setA_event_reg1;
    event_apis[0].setReg2 = &IPhyTpu::setA_event_reg2;

    event_apis[0].getReg1 = &IPhyTpu::getA_event_reg1;
    event_apis[0].getReg2 = &IPhyTpu::getA_event_reg2;

    //! b event
    event_apis[1].logic_analog_la_sel = &IPhyTpu::setB_event_reg2_B_event_logic_analog_sel;
    event_apis[1].logic_analog_sel = &IPhyTpu::setB_event_reg1_B_event_logic_la_sel;
    event_apis[1].logic_func = &IPhyTpu::setB_event_reg2_B_event_logic_func;

    event_apis[1].cmpa_sel = &IPhyTpu::setB_event_reg3_B_event_cmpa_sel;
    event_apis[1].cmpa_mode = &IPhyTpu::setB_event_reg3_B_event_cmpa_mode;
    event_apis[1].cmpb_sel = &IPhyTpu::setB_event_reg3_B_event_cmpb_sel;
    event_apis[1].cmpb_mode = &IPhyTpu::setB_event_reg3_B_event_cmpb_mode;

    event_apis[1].clk_sel = &IPhyTpu::setB_event_reg3_B_event_clk_sel;
    event_apis[1].clk_inv = &IPhyTpu::setB_event_reg3_B_event_clk_sel_inv;
    event_apis[1].dat_sel = &IPhyTpu::setB_event_reg3_B_event_dat_sel;
    event_apis[1].dat_inv = &IPhyTpu::setB_event_reg3_B_event_dat_sel_inv;

    event_apis[1].width_cmp_l_31_0 = &IPhyTpu::setB_event_reg4_B_event_width_cmp_1_31_0;
    event_apis[1].width_cmp_g_31_0 = &IPhyTpu::setB_event_reg5_B_event_width_cmp_g_31_0;
    event_apis[1].width_cmp_l_39_32 = &IPhyTpu::setB_event_reg6_B_event_width_cmp_1_39_32;
    event_apis[1].width_cmp_g_39_32 = &IPhyTpu::setB_event_reg6_B_event_width_cmp_g_39_32;
    event_apis[1].width_cmp_mode = &IPhyTpu::setB_event_reg6_B_event_width_cmp_mode;

    event_apis[1].setReg1 = &IPhyTpu::setB_event_reg1;
    event_apis[1].setReg2 = &IPhyTpu::setB_event_reg2;

    event_apis[1].getReg1 = &IPhyTpu::getB_event_reg1;
    event_apis[1].getReg2 = &IPhyTpu::getB_event_reg2;

    //! r event
    event_apis[2].logic_analog_la_sel = &IPhyTpu::setR_event_reg2_R_event_logic_analog_sel;
    event_apis[2].logic_analog_sel = &IPhyTpu::setR_event_reg1_R_event_logic_la_sel;
    event_apis[2].logic_func = &IPhyTpu::setR_event_reg2_R_event_logic_func;

    event_apis[2].cmpa_sel = &IPhyTpu::setR_event_reg3_R_event_cmpa_sel;
    event_apis[2].cmpa_mode = &IPhyTpu::setR_event_reg3_R_event_cmpa_mode;
    event_apis[2].cmpb_sel = &IPhyTpu::setR_event_reg3_R_event_cmpb_sel;
    event_apis[2].cmpb_mode = &IPhyTpu::setR_event_reg3_R_event_cmpb_mode;

    event_apis[2].clk_sel = &IPhyTpu::setR_event_reg3_R_event_clk_sel;
    event_apis[2].clk_inv = &IPhyTpu::setR_event_reg3_R_event_clk_sel_inv;
    event_apis[2].dat_sel = &IPhyTpu::setR_event_reg3_R_event_dat_sel;
    event_apis[2].dat_inv = &IPhyTpu::setR_event_reg3_R_event_dat_sel_inv;

    event_apis[2].width_cmp_l_31_0 = &IPhyTpu::setR_event_reg4_R_event_width_cmp_1_31_0;
    event_apis[2].width_cmp_g_31_0 = &IPhyTpu::setR_event_reg5_R_event_width_cmp_g_31_0;
    event_apis[2].width_cmp_l_39_32 = &IPhyTpu::setR_event_reg6_R_event_width_cmp_1_39_32;
    event_apis[2].width_cmp_g_39_32 = &IPhyTpu::setR_event_reg6_R_event_width_cmp_g_39_32;
    event_apis[2].width_cmp_mode = &IPhyTpu::setR_event_reg6_R_event_width_cmp_mode;

    event_apis[2].setReg1 = &IPhyTpu::setR_event_reg1;
    event_apis[2].setReg2 = &IPhyTpu::setR_event_reg2;

    event_apis[2].getReg1 = &IPhyTpu::getR_event_reg1;
    event_apis[2].getReg2 = &IPhyTpu::getR_event_reg2;
}

}
