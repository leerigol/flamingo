#ifndef IPHYFPGA_H
#define IPHYFPGA_H

#include "iphy.h"

namespace dso_phy {

#define mem_assign( member, val )  \
        member = val;

#define mem_assign_field( member, field, val ) \
        member.field = val;

#define reg_assign( member, val )  \
        member = val; \
        wcache( &member );

#define reg_assign_field( member, field, val ) \
        member.field = val;\
        wcache( &member );

#define out_assign( member, val )  \
        member = val; \
        out( &member );

#define out_assign_field( member, field, val ) \
        member.field = val;\
        out( &member );

#define out_member( member )    out( &member );

#define reg_in( reg )   in( &reg );

#define align_8( value )    (((value+7)/8)*8)

class IPhyFpga : public IPhy
{
public:
    IPhyFpga();

public:
    virtual void write( void *pMem, cache_addr addr );
    virtual void read( void *pMem, cache_addr addr );
};

}

#endif // IPHYFPGA_H
