
#include "adccal.h"

namespace dso_phy {

adcCoreCal::adcCoreCal()
{
    mGain = 512;
    mOffset = 512;
}

void adcCoreCal::setGain( int gain )
{
    mGain = gain;
}
void adcCoreCal::setOffset( int offset )
{
    mOffset = offset;
}

void adcCoreCal::exportOut( const QString &name, QTextStream &stream )
{
    stream<<name<<","<<mGain<<","<<mOffset<<endl;
}

void adcCal::exportOut( const QString &header,
                        QTextStream &stream )
{
    stream<<header<<endl;
    for ( int i = 0; i < 2; i++ )
    {
        mCores[i][0].exportOut( "A", stream );
        mCores[i][1].exportOut( "B", stream );
        mCores[i][2].exportOut( "C", stream );
        mCores[i][3].exportOut( "D", stream );
    }
}

void adcCalGroup::exportOut( QTextStream &stream )
{
    mAC.exportOut( "AC", stream );
    mBC.exportOut( "BC", stream );
    mAD.exportOut( "AD", stream );
    mBD.exportOut( "BD", stream );

    mA.exportOut( "A", stream );
    mB.exportOut( "B", stream );
    mC.exportOut( "C", stream );
    mD.exportOut( "D", stream );

    mABCD.exportOut( "ABCD", stream );
}

}
