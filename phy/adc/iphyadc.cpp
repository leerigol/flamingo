#include "iphyadc.h"
#include "../../meta/crmeta.h"
namespace dso_phy {

#define ADC_ROW 4
#define ADC_COL 4

#define ADC_DATA_ADDR_R   0x0c

#define ADC_RST_ROW 1
#define ADC_RST_COL 0

#define ADC_CHAN    1

static AdcBitsOp _errantOps_V3[] =
{
    {0x12, 0, 16, 0x8000},
    {0x13, 10, 1, 0 },
    {0x13, 6, 4, 7 },

    {0x14, 5, 2, 0 },
    {0x15, 10, 4, 6 },
    {0x16, 7, 8, 0 },
};

//! GT1 = 1, GT2 = 1
#define build_reg13( val )  ( (val) | (0x3<<12) )
static adcCfg _adcInitSeq_Control_Vl[]=
{

    {0x10,0x0a},
    {0x11, 0x4000 },//! TH_DCA = 4, i_lvds = 0

    {0x12,0x3800},
    {0x13, build_reg13( 0x31c0 ) },
    {0x14,0x2e},    //! TCMP_ADJ = 1, ITRIM_RAI = 0xe
    {0x15,0x0},
};

//! by yanbo mail
//! see rcms project circle
static adcCfg _adcInitSeq_Control_V3[]=
{
    {0x10,0x0a},
    {0x11, 0x4000 },//! TH_DCA = 4, i_lvds = 0

    {0x12,0x8000},
    {0x13, (build_reg13( 0x31c0 ) & (~((1<<10)|(0xf<<6)))) | (6<<6) },

    {0x14,0x2e & (~(3<<5)) },
    {0x15,0x0 | (6<<10)},
    {0x16,0x0 & (~(0xff<<7)) },
};

//! smp_dly_coa = 2, smp_dly_int = 8, smp_dly_fine = 500
#define SMPDLY      ( (2<<14) | (8<<10) | (500) )

//! smpdly
#define smpdly2 SMPDLY
#define smpdly3 SMPDLY
#define smpdly4 SMPDLY

#define nlt_a   30583
#define nlt_b   30583
#define nlt_c   30583
#define nlt_d   30583
#define nlt_e   119
#define nlt_f   ((119)+(0<<8))

static adcCfg _adcInitSeq_Page1[]=
{
    {0x00, 0x1ff},
    {0x01, 0x1ff},

    {0x02, nlt_a},
    {0x03, nlt_b},

    {0x04, nlt_c},
    {0x05, nlt_d},
    {0x06, nlt_e},
    {0x07, nlt_f},

    {0x08, SMPDLY },

    {0x0f, 0},
};

static adcCfg _adcInitSeq_Page2[]=
{
    {0x00, 0x1ff},
    {0x01, 0x1ff},

    {0x02, nlt_a},
    {0x03, nlt_b},

    {0x04, nlt_c},
    {0x05, nlt_d},
    {0x06, nlt_e},
    {0x07, nlt_f},

    {0x08, smpdly2 },

    {0x0f, 0},
};

static adcCfg _adcInitSeq_Page3[]=
{
    {0x00, 0x1ff},
    {0x01, 0x1ff},

    {0x02, nlt_a},
    {0x03, nlt_b},

    {0x04, nlt_c},
    {0x05, nlt_d},
    {0x06, nlt_e},
    {0x07, nlt_f},

    {0x08, smpdly3 },

    {0x0f, 0x0},
};

static adcCfg _adcInitSeq_Page4[]=
{
    {0x00, 0x1ff},
    {0x01, 0x1ff},

    {0x02, nlt_a},
    {0x03, nlt_b},

    {0x04, nlt_c},
    {0x05, nlt_d},
    {0x06, nlt_e},
    {0x07, nlt_f},

    {0x08, smpdly4 },

    {0x0f, 0},
};

#define sequence_do( chan, seqs )   \
                        setChannel( chan );\
                        doSeq( seqs,\
                        array_count(seqs)\
                        );

int IPhyADC::toChCnt( eAdcInterleaveMode eMode )
{
    switch( eMode )
    {
        case ch_abcd:
        case ch_chabcd:
            return 4;

        case ch_ac:
        case ch_bc:
        case ch_ad:
        case ch_bd:
            return 2;

        case ch_a:
        case ch_b:
        case ch_c:
        case ch_d:
            return 1;

        default:
            return 0;
    }
}

IPhyADC::IPhyADC( )
{
    setLink();

    mCtrl_Mod = 0;
    mId = 0;
    mChipId.payload = 0;

    //! rom head
    memset( &mRomHead, 0, sizeof(mRomHead) );
    memset( &mRomInls, 0, sizeof(mRomInls) );

    //! cal data
    m_pCoreGo = NULL;
    m_pCorePhase = NULL;
}

void IPhyADC::setLink( int cs, int rst )
{
    mAdcCs = cs;
    mAdcRst = rst;
}

void IPhyADC::setId( int id )
{
    mId = id;
}

void IPhyADC::init()
{
    mChipId.payload = getChipId();
    //qDebug()<<"Chip Id:"<<QString::number( mChipId.payload, 16 );

    if ( mChipId.majVer < 0x3 )
    {
        sequence_do(0, _adcInitSeq_Control_Vl );
    }
    else
    {
        sequence_do(0, _adcInitSeq_Control_V3 );
    }

    sequence_do(1, _adcInitSeq_Page1 );
    sequence_do(2, _adcInitSeq_Page2 );
    sequence_do(3, _adcInitSeq_Page3 );
    sequence_do(4, _adcInitSeq_Page4 );

    //! load rom
    loadRom();

    write( 0x12, 0x8000 );  //! force spi reg

    setChannel( 0 );

    write( 0x11, 0x4283 );  //4 chs,WZY

    for ( int i = 0; i < array_count(mSmpDlys); i++ )
    {
        mSmpDlys[i].payload = SMPDLY;
    }
}

void IPhyADC::doSeq( adcCfg seg[], int len )
{
    int i;

    for ( i = 0; i < len; i++ )
    {
        write( seg[i].addr, seg[i].data );
    }
}

void IPhyADC::set( unsigned int data, int /*len*/ )
{
    //! select adc
    unsigned short busVal;
    busVal = 1<<mAdcCs;
    toLE( busVal );
    m_pBus->write( busVal, 4, 0 );

    //! send data
    m_pBus->send( &data, 3, ADC_CHAN );
}

void IPhyADC::rst()
{
    unsigned short busVal;

    busVal = 1<<mAdcRst;
    toLE( busVal );
    m_pBus->write( busVal, 1, 0 );
    busVal = 0<<mAdcRst;
    toLE( busVal );
    m_pBus->write( busVal, 1, 0 );
}

void IPhyADC::write( int addr, int data )
{
    unsigned int packData;

    packData = pack24AddrData( addr , data );

    //! write
    packData |= 0x80;

    set( packData, 3 );

}
int IPhyADC::read( int addr )
{
    unsigned int packData;

    //! 地址在低字节上
    packData = (addr) & 0xff;

    //! read
    packData &= (~0x80);

    set( packData, 3 );

    unsigned short adcData;

    //! get register

    //! get data
    adcData = 0;
    m_pBus->read( adcData, ADC_DATA_ADDR_R, 0 );
    toLE( adcData );

    return adcData;
}

unsigned int IPhyADC::getRomID()
{
    unsigned int romId;

    romId = ( mRomHead.ID0<<24 )
            | ( mRomHead.ID1<<16 )
            | ( mRomHead.ID2<<8 )
            | ( mRomHead.ID3<<0 );

    return romId;
}

//! 128 bit = 8b * 16B
void IPhyADC::getUuid( unsigned char uuid[16] )
{
    Q_ASSERT( sizeof(mRomInls) > 16 );

    memcpy( uuid, &mRomInls, 16 );
}

void IPhyADC::setCoreGo( void *pCal )
{
    Q_ASSERT( NULL != pCal );

    m_pCoreGo = (adcCalGroup*)pCal;
}
void IPhyADC::setCorePhase( void *pCal )
{
    Q_ASSERT( NULL != pCal );

    m_pCorePhase = (adcSmpDelay*)pCal;
}

void IPhyADC::setAdcInteMode( eAdcInterleaveMode inteMode )
{
    Q_ASSERT( NULL != m_pCoreGo );

    if ( inteMode == ch_abcd )
    {
        setAdcInteCore( m_pCoreGo->mABCD, mId );
    }
    else if ( inteMode == ch_ac )
    {
        setAdcInteCore( m_pCoreGo->mAC, mId );
    }
    else if ( inteMode == ch_ad )
    {
        setAdcInteCore( m_pCoreGo->mAD, mId );
    }
    else if ( inteMode == ch_bc )
    {
        setAdcInteCore( m_pCoreGo->mBC, mId );
    }
    else if ( inteMode == ch_bd )
    {
        setAdcInteCore( m_pCoreGo->mBD, mId );
    }
    else if ( inteMode == ch_a )
    {
        setAdcInteCore( m_pCoreGo->mA, mId );
    }
    else if ( inteMode == ch_b )
    {
        setAdcInteCore( m_pCoreGo->mB, mId );
    }
    else if ( inteMode == ch_c )
    {
        setAdcInteCore( m_pCoreGo->mC, mId );
    }
    else if ( inteMode == ch_d )
    {
        setAdcInteCore( m_pCoreGo->mD, mId );
    }
    else
    { qWarning()<<"not adc inte mode"; }
}

void IPhyADC::setAdcInteCore( adcCal &coreCal, int id )
{
    for ( int i = 0; i < 4; i++ )
    {
        setGain( i+1, coreCal.mCores[id][i].mGain );
        setOffset( i+1, coreCal.mCores[id][i].mOffset );
    }
}

//! control regs
unsigned int IPhyADC::getChipId()
{
    return getControlReg(0x10, 0, 16);
}

bool IPhyADC::getSyncEn()
{
    return getControlReg(0x11, 15, 1 );
}
void IPhyADC::setSyncEn( bool val )
{
    setControlReg( 0x11, val, 15, 1 );
}

unsigned int IPhyADC::getThDca()
{
    return getControlReg(0x11, 12, 3 );
}
void IPhyADC::setThDca( unsigned int val )
{
    setControlReg( 0x11, val, 12, 3 );
}

bool IPhyADC::getTcEn()
{
    return getControlReg(0x11, 11, 1 );
}
void IPhyADC::setTcEn( bool val )
{
    setControlReg( 0x11, val, 11, 1 );
}

unsigned int IPhyADC::getItrimC()
{
    return getControlReg(0x11, 9, 2 );
}
void IPhyADC::setItrimC( unsigned int val )
{
    setControlReg( 0x11, val, 9, 2 );
}

unsigned int IPhyADC::getIlvds()
{
    return getControlReg(0x11, 7, 2 );
}
void IPhyADC::setIlvds( unsigned int val )
{
    setControlReg( 0x11, val, 7, 2 );
}

bool IPhyADC::getTest()
{
    return getControlReg(0x11, 6, 1 );
}
void IPhyADC::setTest( bool val )
{
    setControlReg( 0x11, val, 6, 1 );
}

unsigned int IPhyADC::getStdby()
{
    return getControlReg(0x11, 4, 2 );
}
void IPhyADC::setStdby( unsigned int val )
{
    setControlReg( 0x11, val, 4, 2 );
}

unsigned int IPhyADC::getMode()
{
    return mCtrl_Mod;
}
void IPhyADC::setMode( unsigned int val,
                       DsoWorkAim aim )
{
    mCtrl_Mod = val;
    setControlReg( 0x11, val, 0, 4 );

    if ( aim == aim_normal )
    {
        loadSmpDly( mCtrl_Mod );
    }
}

unsigned int IPhyADC::getAdcxup()
{
    return getControlReg(0x12, 0, 4 );
}

void IPhyADC::setRMOD( unsigned int mod )
{
    setControlReg( 0x13, mod, 11, 1 );
}

unsigned int IPhyADC::getItrimA()
{
    return getControlReg(0x13, 2, 2 );
}
void IPhyADC::setItrimA( unsigned int val )
{
    setControlReg( 0x13, val, 2, 2 );
}

bool IPhyADC::getDec()
{
    return getControlReg(0x13, 1, 1 );
}
void IPhyADC::setDec( bool val )
{
    setControlReg( 0x13, val, 1, 1 );
}

bool IPhyADC::getTrack()
{
    return getControlReg(0x13, 0, 1 );
}
void IPhyADC::setTrack( bool val )
{
    setControlReg( 0x13, val, 0, 1 );
}

bool IPhyADC::getTrimStatus()
{
    return getControlReg(0x14, 15, 1 );
}

void IPhyADC::setTrimRai( unsigned int val )
{
    setControlReg( 0x14, val, 0, 5 );
}

unsigned int IPhyADC::getDlyFine()
{
    return getControlReg(0x15, 0, 10 );
}
void IPhyADC::setDlyFine( unsigned int val )
{
    setControlReg( 0x15, val, 0, 10 );
}

bool IPhyADC::getFlip()
{
    return getControlReg(0x16, 15, 1 );
}
void IPhyADC::setFlip( bool val )
{
    setControlReg( 0x16, val, 15, 1 );
}

unsigned int IPhyADC::getDlyCoarse()
{
    return getControlReg(0x16, 4, 3 );
}
void IPhyADC::setDlyCoarse( unsigned int val )
{
    setControlReg( 0x16, val, 4, 3 );
}

unsigned int IPhyADC::getDlyInter()
{
    return getControlReg(0x16, 0, 4 );
}
void IPhyADC::setDlyInter( unsigned int val )
{
    setControlReg( 0x16, val, 0, 4 );
}

void IPhyADC::setChannel( unsigned int val )
{
    write( 0x1f, val );
}

//! page regs id = 1,2,3,4
//! 0~1023
void IPhyADC::setOffset( int page, unsigned int val )
{
    setPageReg( page, 0, val, 0, 10 );
}
//! page regs id = 1/2/3/4
//! 0~1023
void IPhyADC::setGain( int page, unsigned int val )
{
    setPageReg( page, 1, val, 0, 10 );
}

void IPhyADC::setNltA( int page, unsigned int val )
{
    setPageReg( page, 2, val, 0, 16, false );
}
void IPhyADC::setNltB( int page, unsigned int val )
{
    setPageReg( page, 3, val, 0, 16, false );
}
void IPhyADC::setNltC( int page, unsigned int val )
{
    setPageReg( page, 4, val, 0, 16, false );
}
void IPhyADC::setNltD( int page, unsigned int val )
{
    setPageReg( page, 5, val, 0, 16, false );
}

void IPhyADC::setNltE( int page, unsigned int val )
{
    setPageReg( page, 6, val, 0, 16, false );
}
void IPhyADC::setNltF( int page, unsigned int val )
{
    setPageReg( page, 7, val, 0, 16, false );
}

void IPhyADC::setSmpDly( int page, unsigned int val )
{
    setPageReg( page, 8, val, 0, 16, false );

    Q_ASSERT( page >= 1 && page <= 4 );

    mSmpDlys[ page - 1 ].payload = val;
}

adcSMPDLY IPhyADC::getSmpDly( int page )
{
    Q_ASSERT( page >= 1 && page <= 4 );

    return mSmpDlys[ page - 1];
}

adcSMPDLY IPhyADC::getDefaultSmpDly( int /*id*/ )
{
    adcSMPDLY dly;

    dly.payload = SMPDLY;

    return dly;
}

unsigned int IPhyADC::getRwLoop( int page )
{
    return getPageReg( page, 0x0f, 0, 16 );
}
void IPhyADC::setRwLoop( unsigned int val, int page )
{
    setPageReg( page, 0x0f, val, 0, 16 );
}

//! ret : read len
#define bank_size   256
int IPhyADC::readBank( int bank,
                       int offset,
                       int len,
                       int uLimit,   //! upper limit [offset,uRange)
                       unsigned char *pData
                        )
{
    OTP_DATA otp;           //! otp data
    int base, readLen;
                            //! read bank flag
    base = bank * 256;
    otp = readRom( base );  //! bank addr
    if ( otp.valid )
    {}
    else                    //! invalid bank
    { return 0; }
                            //! select invalid addr
    int invalidAddr = otp.data;

    if ( offset == 0 )      //! the first data
    {
        readLen = 0;
        pData[ readLen++ ] = otp.data;
    }
    else
    {
        readLen = 0;
    }

    while( 1 )              //! read n bytes
    {
                            //! read a byte
        if ( offset != invalidAddr )
        {
            otp = readRom( base + offset );
            offset++;
                            //! export data
            if ( otp.valid )
            {
                pData[ readLen++ ] = otp.data;
            }
        }
        else
        {
            offset++;
        }
                            //! read full
        if ( readLen >= len )
        { return len; }

        if ( offset >= uLimit )
        { return readLen; }
    }

    return -1;
}


unsigned int IPhyADC::getControlReg( int addr, int col, int width )
{
    unsigned int dat;

    setChannel( 0 );

    dat = read( addr );

    return getBits( dat, col, width );
}
void IPhyADC::setControlReg( int addr,
                             unsigned int val,
                             int col,
                             int width )
{
    unsigned int dat;

    setChannel( 0 );

    dat = read( addr );

    setBits( dat, val, col, width );

    write( addr, dat );
}

unsigned int IPhyADC::getPageReg( int page, int addr, int col, int width )
{
    unsigned int dat;

    setChannel( page );

    dat = read( addr );

    return getBits( dat, col, width );
}
void IPhyADC::setPageReg( int page, int addr, unsigned int val, int col, int width, bool bR )
{
    unsigned int dat;

    setChannel( page );

    if ( bR )
    { dat = read( addr ); }
    else
    { dat = 0; }

    setBits( dat, val, col, width );

    write( addr, dat );
}

//! read an OTP addr
OTP_DATA IPhyADC::readRom( int addr )
{
    //! 1. set addr, pwr
    OTP_AB ab;

    ab.payload = 0;

    ab.otp_addr = addr;
    ab.otp_pd = 1;
    write( 0x10, ab.payload );

    //! 2. precharge
    OTP_CDB cdb;
    cdb.payload = 0;

    cdb.otp_data = 0;
    cdb.otp_rw = 0;
    cdb.otp_pc = 3;
    cdb.otp_prog = 1;
    cdb.otp_sens = 1;

    write( 0x12, cdb.payload );

    //! 3.
    cdb.otp_pc  = 2;
    write( 0x12, cdb.payload );

    //! 4. wait
    //! no need as spi bus delay

    //! 5. read
    cdb.otp_data = (adc_reg)read( 0x12 );

    //! 6. power down
    ab.payload = 0;

    ab.otp_pd = 0;
    ab.otp_addr = 0;
    write( 0x10, ab.payload );

    OTP_DATA data;
    data.payload = cdb.otp_data;

    return data;
}

void IPhyADC::loadRom()
{    
    r_meta::CBMeta meta;
    int iLoad;
    meta.getMetaVal( "phy/adc/load_rom", iLoad );
    if ( iLoad > 0 )
    {}
    else
    {
        qWarning()<<"!!!adc rom not loaded";
        return;
    }

    if( loadRomHead() == 0 )
    {
        applyHead( mRomHead );
    }
    else
    {  qWarning()<<"!!!rom head fail"; }

    if ( loadRomInl() == 0 )
    {
        applyInl( mRomInls);
    }
    else
    {  qWarning()<<"!!!rom inl fail"; }
}

//! 0 -- no error
int IPhyADC::loadRomHead()
{
    int ret;

    ret = readBank( 0,
                    0,
                    sizeof(mRomHead),
                    32,
                    (unsigned char*)&mRomHead );
    if ( ret != sizeof(mRomHead) )
    { return -1; }
    else
    { return 0; }

}
//! 0 -- no error
int IPhyADC::loadRomInl()
{
    int ret;

    ret = readBank( 0,
                    32,
                    sizeof(mRomInls),
                    80,
                    (unsigned char*)&mRomInls );
    if ( ret != sizeof(mRomInls) )
    { return -1; }
    else
    { return 0; }
}

#define make_reg( l, h )    ((l)|((h)<<8))
void IPhyADC::applyHead( ROM_V1_HEAD &head )
{
    int regVal;

    setChannel( 0 );

    //! \todo sync lose clock
    //! only8000
    //! control reg
    regVal = make_reg( head.CONTROL_REG_L,
                       head.CONTROL_REG_H );
    regVal = errantReg( 0x11, regVal );
    write( 0x11,  regVal );

    //! test reg
    regVal = make_reg( head.TEST_REG_L,
                       head.TEST_REG_H );
    regVal = errantReg(0x13, regVal );
    write( 0x13, regVal );

    //! trim reg
    regVal = make_reg( head.Trimmer_REG_L,
                       head.Trimmer_REG_H );
    regVal = errantReg(0x14, regVal );
    write( 0x14, regVal );

    //! clk adjl reg
    regVal = make_reg( head.CLK_ADJ_L_REG_L,
                       head.CLK_ADJ_L_REG_H);
    regVal = errantReg(0x15, regVal );
    write( 0x15, regVal );
}

void IPhyADC::applyInl( ROM_V1_INL &inl )
{
    int pageId = 1;
    for ( int i = 0;
          i < 4;
          i++, pageId++ )
    {
        setNltA( pageId, make_reg( inl.nlts[i].NLT_Ax[0],
                                   inl.nlts[i].NLT_Ax[1]) );

        setNltB( pageId, make_reg( inl.nlts[i].NLT_Bx[0],
                                   inl.nlts[i].NLT_Bx[1]) );

        setNltC( pageId, make_reg( inl.nlts[i].NLT_Cx[0],
                                   inl.nlts[i].NLT_Cx[1]) );

        setNltD( pageId, make_reg( inl.nlts[i].NLT_Dx[0],
                                   inl.nlts[i].NLT_Dx[1]) );

        setNltE( pageId, make_reg( inl.nlts[i].NLT_Ex[0],
                                   inl.nlts[i].NLT_Ex[1]) );

        setNltF( pageId, make_reg( inl.nlts[i].NLT_Fx[0],
                                   inl.nlts[i].NLT_Fx[1]) );

//        setNltA( pageId, nlt_a );

//        setNltB( pageId, nlt_b );

//        setNltC( pageId, nlt_c );

//        setNltD( pageId, nlt_d );

//        setNltE( pageId, nlt_e );

//        setNltF( pageId, nlt_f );

//        setChannel( pageId );

//        write( 0, 0x1ff );
//        write( 1, 0x1ff );

//        write( 2, nlt_a );
//        write( 3, nlt_b );
//        write( 4, nlt_c );
//        write( 5, nlt_e );

//        write( 6, nlt_e );
//        write( 7, nlt_f );
    }
}

//! foreach core
void IPhyADC::loadSmpDly( int adcMode )
{
    if ( NULL == m_pCorePhase )
    { return; }

    Q_ASSERT( adcMode >= ch_abcd && adcMode <= ch_com_d );

    //! for each mode
    applySmpDly( m_pCorePhase->smpDelays + adcMode - ch_abcd );

}

void IPhyADC::applySmpDly( coreSmpDelay *smpDly )
{
    Q_ASSERT( NULL != smpDly );

    //! for each core
    for ( int i = 1; i <= array_count(smpDly->coreDelay); i++ )
    {
        setSmpDly( i, smpDly->coreDelay[i-1].payload );

        LOG_DBG()<<i<<smpDly->coreDelay[i-1].payload;
    }
}

int IPhyADC::errantReg( int addr, int data )
{
    if ( mChipId.majVer >= 3 )
    {
        return errantVxReg( addr, data,
                            _errantOps_V3, array_count(_errantOps_V3) );
    }
    else
    { return data; }
}

int IPhyADC::errantVxReg( int addr, int data,
                          AdcBitsOp *pOps, int cnt )
{
    Q_ASSERT( NULL != pOps );

    quint32 masks[]={ 0,
                      0x1, 0x3, 0x7, 0xf,
                      0x1f, 0x3f, 0x7f, 0xff,
                      0x1ff, 0x3ff, 0x7ff, 0xfff,
                      0x1fff, 0x3fff, 0x7fff, 0xffff,
                    };
    AdcBitsOp *pBitOp;

    for( int i = 0; i < cnt; i++ )
    {
        pBitOp = pOps + i;

        //! addr match
        if ( pBitOp->addr == addr )
        {
            Q_ASSERT( pBitOp->len > 0
                      && pBitOp->len < array_count(masks) );

            //! unset to 0
            data &= ( ~( masks[ pBitOp->len ] << pBitOp->from ) );

            //! attach value
            data |= pBitOp->bitsVal << pBitOp->from;
        }
    }

    return data;
}

}

