#ifndef ADCCAL
#define ADCCAL

#include <QtCore>

namespace dso_phy {

struct adcCoreCal
{
    int mGain;
    int mOffset;

    adcCoreCal();

    void setGain( int gain );
    void setOffset( int offset );

    void exportOut( const QString &name, QTextStream &stream );
};

struct adcCal
{
    adcCoreCal mCores[2][4];      //! master/slave cores
    void exportOut( const QString &header,
                    QTextStream &stream );
};

struct adcCalGroup
{
    //! 2 channels
    adcCal mAC;
    adcCal mAD;
    adcCal mBC;
    adcCal mBD;

    //! 1 channel
    adcCal mA;  //! ref a
    adcCal mB;  //! ref b
    adcCal mC;  //! ref c
    adcCal mD;  //! ref d

    //! 4 channels
    adcCal mABCD;

    void exportOut( QTextStream &stream );
};


}

#endif // ADCCAL

