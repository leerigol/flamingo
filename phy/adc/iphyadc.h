#ifndef IPHYADC_H
#define IPHYADC_H

#include "../iphy.h"
#include "adccal.h" //! adc cal

namespace dso_phy {


#define adc_reg unsigned short

typedef struct _adcCfg
{
    unsigned char addr;
    unsigned short data;
}adcCfg;

union adcSMPDLY
{
    struct
    {
        adc_reg smp_dly_fine : 10;
        adc_reg smp_dly_int : 4;
        adc_reg smp_dly_coa : 2;
    };
    adc_reg payload;
};

//! for each core
struct coreSmpDelay
{
    adcSMPDLY coreDelay[4];
};

//! for 16 mode
struct adcSmpDelay
{
    coreSmpDelay smpDelays[16]; //! 16 modes
};

union OTP_AB
{
    struct
    {
        adc_reg otp_addr : 10;
        adc_reg otp_pd : 1;
        adc_reg res : 5;
    };
    unsigned short payload;
};

union OTP_CDB
{
    struct
    {
        adc_reg otp_data : 9;
        adc_reg otp_rw : 1;
        adc_reg otp_pc : 2;
        adc_reg otp_prog : 2;
        adc_reg otp_sens : 2;
        adc_reg res :1;
    };
    adc_reg payload;
};

union OTP_DATA
{
    struct
    {
        adc_reg data : 8;
        adc_reg valid : 1;
    };
    adc_reg payload;
};

#define ROM_reg unsigned char
struct ROM_V1_HEAD
{
    ROM_reg BANK_FLAG;
    ROM_reg VER0, VER1, VER2, VER3;
    ROM_reg ID0, ID1, ID2, ID3;

    ROM_reg CHECK_SUM;
    ROM_reg PARA_FLAG;

    ROM_reg CONTROL_REG_L;
    ROM_reg CONTROL_REG_H;

    ROM_reg TEST_REG_L;
    ROM_reg TEST_REG_H;

    ROM_reg Trimmer_REG_L;
    ROM_reg Trimmer_REG_H;

    ROM_reg CLK_ADJ_L_REG_L;
    ROM_reg CLK_ADJ_L_REG_H;
};

struct _V1_INL
{
    ROM_reg NLT_Ax[2];  //! L,H
    ROM_reg NLT_Bx[2];
    ROM_reg NLT_Cx[2];
    ROM_reg NLT_Dx[2];

    ROM_reg NLT_Ex[2];
    ROM_reg NLT_Fx[2];

};

struct ROM_V1_INL
{
    _V1_INL nlts[4];    //! NLT1/2/3/4
};

struct AdcBitsOp
{
    int addr;
    int from;
    int len;
    int bitsVal;
};

union AdcChipId
{
    quint16 payload;
    struct{
        quint16 minVer : 2;
        quint16 majVer : 2;
        quint16 branch : 4;
        quint16 type   : 8;
    };
};

class IPhyADC : public IPhy
{
public:
    enum eAdcInterleaveMode
    {
        ch_abcd = 0,
        ext_tha_bc = 1,
        ext_tha_ad = 2,
        ch_chabcd = 3,

        ch_ac = 4,
        ch_bc = 5,
        ch_ad = 6,
        ch_bd = 7,

        ch_a = 8,
        ch_b = 9,
        ch_c = 10,
        ch_d = 11,

        ch_com_a = 12,
        ch_com_b = 13,
        ch_com_c = 14,
        ch_com_d = 15,
    };

public:
    static int toChCnt( eAdcInterleaveMode eMode );

public:
    IPhyADC();
    void setLink( int cs = 4, int rst = 0 );
    void setId( int id );
public:
    virtual void init();

protected:
    void doSeq( adcCfg seg[], int len );

    void set( unsigned int data, int len );
public:
    void rst();

    void write( int addr, int data );
    int read( int addr );

    unsigned int getRomID();

    //! 16*8 = 128bit
    void getUuid( unsigned char uuid[128] );

    void setCoreGo( void *pCal );
    void setCorePhase( void *pCal );

    void setAdcInteMode( eAdcInterleaveMode inteMode );
    void setAdcInteCore( adcCal &coreCal, int id = 0 );

public:
    //! control regs
    unsigned int getChipId();

    bool getSyncEn();
    void setSyncEn( bool );

    unsigned int getThDca();
    void setThDca( unsigned );

    bool getTcEn();
    void setTcEn( bool );

    unsigned int getItrimC();
    void setItrimC( unsigned int );

    unsigned int getIlvds();
    void setIlvds( unsigned int );

    bool getTest();
    void setTest( bool );

    unsigned int getStdby();
    void setStdby( unsigned int );

    unsigned int getMode();
    void setMode( unsigned int mode,
                  DsoWorkAim aim );

    unsigned int getAdcxup();

    void setRMOD( unsigned int mod );

    unsigned int getItrimA();
    void setItrimA( unsigned int );

    bool getDec();
    void setDec( bool b );

    bool getTrack();
    void setTrack( bool );

    bool getTrimStatus();

    void setTrimRai( unsigned int );

    unsigned int getDlyFine();
    void setDlyFine( unsigned int );

    bool getFlip();
    void setFlip( bool );

    unsigned int getDlyCoarse();
    void setDlyCoarse( unsigned int );

    unsigned int getDlyInter();
    void setDlyInter( unsigned int );

    void setChannel( unsigned int chan );

    //! page regs id = 1,2,3,4
    void setOffset( int id, unsigned int val );
    void setGain( int id, unsigned int val );

    void setNltA( int id, unsigned int val );
    void setNltB( int id, unsigned int val );
    void setNltC( int id, unsigned int val );
    void setNltD( int id, unsigned int val );

    void setNltE( int id, unsigned int val );
    void setNltF( int id, unsigned int val );

    void setSmpDly( int id, unsigned int val );
    adcSMPDLY getSmpDly( int id );
    adcSMPDLY getDefaultSmpDly( int id );

    unsigned int getRwLoop( int id = 1 );
    void setRwLoop( unsigned int, int id = 1);

public:
    int readBank( int bank,
                  int offset,
                  int len,
                  int uLimit,
                  unsigned char *pData );

private:
    unsigned int getControlReg( int addr, int col, int width );
    void setControlReg( int add, unsigned int val, int col, int width );

    unsigned int getPageReg( int page, int addr, int col, int width );
    void setPageReg( int page, int addr, unsigned int val, int col, int width, bool bR = true );

    OTP_DATA readRom( int addr );

    void loadRom();

    int loadRomHead();
    int loadRomInl();

    void applyHead( ROM_V1_HEAD &head );
    void applyInl( ROM_V1_INL &inl );

    void loadSmpDly( int adcMode );
    void applySmpDly( coreSmpDelay *smpDly );

    int errantReg( int addr, int data );
    int errantVxReg( int addr, int data, AdcBitsOp *pOps, int cnt );

private:
    int mAdcCs;
    int mAdcRst;
    int mId;

    unsigned int mCtrl_Mod;
    AdcChipId mChipId;

    adcSMPDLY mSmpDlys[4];

    //! roms
    ROM_V1_HEAD mRomHead;
    ROM_V1_INL mRomInls;

    //! cal data
    adcCalGroup *m_pCoreGo;
    adcSmpDelay *m_pCorePhase;
};

}

#endif // IPHYADC_H
