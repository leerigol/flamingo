#ifndef IPHYPROBE_H
#define IPHYPROBE_H
#include <QString>
#include "iphy1wire.h"
#include "../ch/iphypath.h"
namespace dso_phy
{

class IphyProbe : public IPhy1Wire
{
public:
    IphyProbe(Chan id = chan1);

public:
    enum cmd
    {
        CMD_SAVE_CAL_USER_DATA = 0x00,
        CMD_SAVE_CAL_PROJECT_DATA,
        CMD_GET_DEFAULT_DATA,
        CMD_SET_PROBE_SN,
        CMD_GET_CAL_USER_DATA,
        CMD_GET_CAL_PROJECT_DATA,
    };

public:
    DsoErr readProbeInfo(quint8 *pbuf, int size);
    DsoErr writeProbeInfo(quint8 *pbuf, int size);
};

}

#endif // IPHYPROBE_H
