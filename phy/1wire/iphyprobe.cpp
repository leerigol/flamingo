#include "iphyprobe.h"

namespace dso_phy {

IphyProbe::IphyProbe(Chan id):IPhy1Wire(id)
{

}

DsoErr IphyProbe::readProbeInfo(quint8 *pbuf, int size)
{
    int runtSize = ch_write_read( CMD_GET_CAL_USER_DATA, pbuf, size );
    if(runtSize != size)
    {
        qDebug()<<__FILE__<<__LINE__
               <<"err:"<<getId()<<"read size:"<<runtSize;
        return ERR_IN_READ;
    }
    return ERR_NONE;
}

DsoErr IphyProbe::writeProbeInfo(quint8 *pbuf, int size)
{
    int runtSize = ch_write( CMD_SAVE_CAL_USER_DATA, pbuf, size );
    if(runtSize != size)
    {
        qDebug()<<__FILE__<<__LINE__
               <<"err:"<<getId()<<"write size:"<<runtSize;
        return ERR_IN_WRITE;
    }
    return ERR_NONE;
}

}
