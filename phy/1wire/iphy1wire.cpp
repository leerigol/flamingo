#include "iphy1wire.h"

namespace dso_phy {

_chx_pFunc IPhy1Wire::pfunc[4] =
{
    {chan1,
     &IPhy1Wire::pulsePROBE_CH1_clr,
     &IPhy1Wire::setPROBE_CH1_cmd_type,
     &IPhy1Wire::setPROBE_CH1_wr_data_en,
     &IPhy1Wire::outPROBE_CH1_wr_data,
     &IPhy1Wire::setPROBE_CH1_rx_len,
     &IPhy1Wire::inPROBE_CH1,
     &IPhy1Wire::getPROBE_CH1_rd_data_vld,
    },
    {chan2,
     &IPhy1Wire::pulsePROBE_CH2_clr,
     &IPhy1Wire::setPROBE_CH2_cmd_type,
     &IPhy1Wire::setPROBE_CH2_wr_data_en,
     &IPhy1Wire::outPROBE_CH2_wr_data,
     &IPhy1Wire::setPROBE_CH2_rx_len,
     &IPhy1Wire::inPROBE_CH2,
     &IPhy1Wire::getPROBE_CH2_rd_data_vld,
    },
    {chan3,
     &IPhy1Wire::pulsePROBE_CH3_clr,
     &IPhy1Wire::setPROBE_CH3_cmd_type,
     &IPhy1Wire::setPROBE_CH3_wr_data_en,
     &IPhy1Wire::outPROBE_CH3_wr_data,
     &IPhy1Wire::setPROBE_CH3_rx_len,
     &IPhy1Wire::inPROBE_CH3,
     &IPhy1Wire::getPROBE_CH3_rd_data_vld,
    },
    {chan4,
     &IPhy1Wire::pulsePROBE_CH4_clr,
     &IPhy1Wire::setPROBE_CH4_cmd_type,
     &IPhy1Wire::setPROBE_CH4_wr_data_en,
     &IPhy1Wire::outPROBE_CH4_wr_data,
     &IPhy1Wire::setPROBE_CH4_rx_len,
     &IPhy1Wire::inPROBE_CH4,
     &IPhy1Wire::getPROBE_CH4_rd_data_vld,
    }
};

IPhy1Wire::IPhy1Wire(Chan id):mChan(id)
{
//    qDebug()<<__FILE__<<__LINE__<<mChan;
}

void IPhy1Wire::init()
{
    setCFG_1WIRE_bit_period(252);
    setCFG_1WIRE_bit_rx(150);
    setCFG_1WIRE_bit_tx(180);
    setCFG_1WIRE_bit_start(30);
    setPROBE_WAIT(2000);

    setPROBE_CH1_wr_data_en(0);
    pulsePROBE_CH1_clr(1,0,100);

    setPROBE_CH2_wr_data_en(0);
    pulsePROBE_CH2_clr(1,0,100);

    setPROBE_CH3_wr_data_en(0);
    pulsePROBE_CH3_clr(1,0,100);

    setPROBE_CH4_wr_data_en(0);
    pulsePROBE_CH4_clr(1,0,100);

}

void IPhy1Wire::loadOtp()
{
    _loadOtp();
}
void IPhy1Wire::reMap()
{
    _remap_K7mFcu_();
}

void IPhy1Wire::setId(Chan id)
{
    mChan = id;
}

Chan IPhy1Wire::getId()
{
    return mChan;
}

int IPhy1Wire::ch_read(quint8 */*pbuf*/, int /*size*/)
{
    //Q_ASSERT(size<128);
    return 0;
}

int IPhy1Wire::ch_write(quint8 addr,quint8 *pbuf, int size)
{
    Q_ASSERT(size<128);
//    qDebug()<<__FILE__<<__LINE__<<mChan;
    for(int n=0; n<4; n++)
    {
        if(pfunc[n].ch == mChan)
        {
            (this->*pfunc[n].pulsePROBE_CHX_clr)(1,0,100);

            //!write cmd
            (this->*pfunc[n].setPROBE_CHX_cmd_type)(0);
            (this->*pfunc[n].setPROBE_CHX_wr_data_en)(1);
            (this->*pfunc[n].outPROBE_CHX_wr_data)(addr);

            //!write size
            (this->*pfunc[n].setPROBE_CHX_cmd_type)(0);
            (this->*pfunc[n].setPROBE_CHX_wr_data_en)(1);
            (this->*pfunc[n].outPROBE_CHX_wr_data)(size);

            //!write data
            for(int i=0; i<size; i++)
            {
                (this->*pfunc[n].setPROBE_CHX_cmd_type)(0);
                (this->*pfunc[n].setPROBE_CHX_wr_data_en)(1);
                (this->*pfunc[n].outPROBE_CHX_wr_data)(pbuf[i]);
            }

            return size;
        }
    }
    return 0;
}

int IPhy1Wire::ch_write_read(quint8 addr, quint8 *r_pbuf,
                              int r_size, int timeout_ms)
{
    Q_ASSERT(r_size<128);
//    qDebug()<<__FILE__<<__LINE__<<mChan;
    for(int n=0; n<4; n++)
    {
        if(pfunc[n].ch == mChan)
        {
            (this->*pfunc[n].pulsePROBE_CHX_clr)(1,0,100);
            (this->*pfunc[n].setPROBE_CHX_cmd_type)(1);
            (this->*pfunc[n].setPROBE_CHX_rx_len)(r_size);

            (this->*pfunc[n].setPROBE_CHX_wr_data_en)(1);
            (this->*pfunc[n].outPROBE_CHX_wr_data)(addr);

            QThread::msleep(timeout_ms);

            for(int i=0; i<r_size; i++)
            {
                r_pbuf[i] = (this->*pfunc[n].inPROBE_CHX)();
                if( !(this->*pfunc[n].getPROBE_CHX_rd_data_vld)() ) return i;
                //qDebug("%d,%X",getPROBE_CHX_rd_data_vld(),r_pbuf[i] );
            }

            return r_size;
        }
    }

    return 0;
}

}
