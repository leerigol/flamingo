#ifndef IPHY1WIRE_H
#define IPHY1WIRE_H


#include "../iphyfpga.h"

namespace dso_phy
{

#include "../ic/k7mfcu.h"

class IPhy1Wire;

typedef void        (IPhy1Wire::*_fsetReg)(K7mFcu_reg);
typedef K7mFcu_reg  (IPhy1Wire::*_fgetReg)(void);
typedef void        (IPhy1Wire::*_foutReg)( K7mFcu_reg value );
typedef void        (IPhy1Wire::*_fpulseReg)(K7mFcu_reg, K7mFcu_reg , int);
typedef K7mFcu_reg  (IPhy1Wire::*_finReg)(void);

struct _chx_pFunc
{
    const Chan  ch;
    _fpulseReg  pulsePROBE_CHX_clr;
    _fsetReg    setPROBE_CHX_cmd_type;
    _fsetReg    setPROBE_CHX_wr_data_en;
    _foutReg    outPROBE_CHX_wr_data;
    _fsetReg    setPROBE_CHX_rx_len;
    _finReg     inPROBE_CHX;
    _fgetReg    getPROBE_CHX_rd_data_vld;
};


class IPhy1Wire : public K7mFcuMem
{
public:
    IPhy1Wire( Chan id = chan1 );

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();
public:
    void setId( Chan id );
    Chan getId();

    int ch_read( quint8 * pbuf, int size);
    int ch_write(quint8 addr, quint8 * pbuf, int size);
    int ch_write_read(quint8 addr,
                       quint8 *r_pbuf, int r_size,
                       int timeout_ms = 100);
private:
    static     _chx_pFunc pfunc[4];
    Chan        mChan;
};

}

#endif
