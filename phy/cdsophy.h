#ifndef CDSOPHY_H
#define CDSOPHY_H

#include "./ch/iphych.h"
#include "./hori/iphyhori.h"
#include "./trace/iphytrace.h"

#include "iphybeeper.h"
#include "iphylcd.h"
#include "iphyfan.h"
#include "iphyboard.h"

#include "./adc/iphyadc.h"
#include "iphydac.h"
#include "iphypll.h"
#include "./ch/iphypath.h"

#include "iphyled.h"
#include "./wpu/iphywpu.h"
#include "./scu/iphyscu.h"
#include "./trig/iphytpu.h"
#include "./search/iphysearch.h"

#include "./spu/iphyspu.h"
#include "./gtu/iphygtu.h"
#include "./ccu/iphyccu.h"

#include "./meas/iphymeascfg.h"
#include "./meas/iphymeasret.h"
#include "./counter/iphycounter.h"

#include "./dg/iphydg.h"
#include "./relay/iphyrelay.h"

#include "./mask/iphymask.h"

#include "./1wire/iphyprobe.h"
#include "./1wire/iphy1wire.h"
#include "./k7mfcu/iphyk7mfcu.h"

#include "./zynqfcu/iphyzynqfcurd.h"
#include "./zynqfcu/iphyzynqfcuwr.h"

#include "./trace/iphytrace.h"
#include "./recplay/iphyrecplay.h"

class CDsoRecEngine;
class CDsoPlayEngine;

namespace dso_phy {

/*!
 * \brief The CDsoPhy class
 * Dso phys
 */
class CDsoPhy
{
public:
    CDsoPhy();

public:
    DsoErr open();
    void close();

    void init();
    void appendBus( IBus *pBus );

    void flushWCache();

    int getAdcCnt();
    int getSpuCnt();
    int getScuCnt();
    int getWpuCnt();

public:
    bool isDs8000();

public:
    IPhyPLL mPll;
    IPhyADC mAdc[2];

    IPhyCH m_ch[4];
    IPhyPath mExt;
    IPhyPath mLa[2];    //! la L/H

    IPhyPath mProbe[4];

    //! dg
    IPhyPath mDgRef[2];
    IPhyPath mDgOffset[2];

    IPhyHori m_hori;
    IPhyTrace m_trace;

    IPhyBeeper mBeeper;
    IPhyLcd    mLcd;
    IPhyFan    mFan;
    IPhyBoard  mBoard;  

    IPhyAfe mVGA[4];

    IPhyLed mLed;

    IPhyWpu mWpu;
    IPhyDac mDac;

    IPhyTpu mTpu;
    IPhySearch mSearch;

    IPhySpu mSpu;
    IPhyScu mScu;

    IPhySpu mSpu2;
    IPhyScu mScu2;

    IPhyCcu mCcu;

    //! scu && spu group
    IPhySpuGp mSpuGp;
    IPhyScuGp mScuGp;

    IPhyGtu mGtu;

    //! proc
    IPhyFrequ mCounter;
    IPhyMeasCfgu mMeasCfg;
    IPhyMeasRetu mMeasRet;

    //! mask
    IPhyMask mMask;

    //! record
    IPhyRecPlay mRecPlay;

    //! probe
    IphyProbe m1WireProbe[4];

    //! zynq fcu
    IPhyZynqFcuWr mZynqFcuWr;
    IPhyZynqFcuRd mZynqFcuRd;


    //! misc
    IPhyK7mFcu mMisc;

    //! dg && relay
    IPhyDg  mDgu;
    IPhyRelay mRelay;

public:
    QList<IBus*> mBuses;

private:
    QList<IPhy*> mPhyList;

friend class CDsoRecEngine;
friend class CDsoPlayEngine;
};

}
#endif // CDSOPHY_H
