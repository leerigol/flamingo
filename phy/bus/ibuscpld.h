#ifndef IBUSCPLD_H
#define IBUSCPLD_H

#include "ibusspi.h"

//! cpld 存储器的单元
typedef unsigned short cpld_mem_chunk;

namespace dso_phy {

class IBusCpld : public IBusSpi
{
public:
    IBusCpld( int capacity = 0x0e );
    ~IBusCpld();
public:
    virtual DsoErr open( const char *path );
    virtual void close();

public:
    virtual int writeBits(
               unsigned int data,
               int bitWidth,
               busAddr row,
               busAddr col,
               busChan chan = no_buschan,
               bool bFlush = true );

private:
    cpld_mem_chunk getRMask( int bitN );
    cpld_mem_chunk getLMask( int bitN, int wid );

private:
    int mCapacity;  //! size in mem chunk
    cpld_mem_chunk *m_pMemory;

};

}

#endif // IBUSCPLD_H
