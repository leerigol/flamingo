#include "ibus.h"

namespace dso_phy {

IBus::IBus()
{
    mFd = -1;
}

DsoErr IBus::open( const char */*path*/ )
{
    return ERR_NONE;
}
void IBus::close()
{}

void IBus::rst()
{}
bool IBus::isReady()
{ return true; }

//! write
int IBus::writeOp(
            void */*pData*/,
            int len,
            busAddr /*addr*/,
            busChan /*chan*/
            )
{
    return len;
}
//! 直接向总线上发送数据
int IBus::send( void */*pData*/,
                int /*len*/,
                busChan /*chan*/)
{ return 0; }

//! 从总线接收数据
int IBus::recv( void */*pData*/,
                int /*len*/,
                busChan /*chan*/)
{ return 0; }

int IBus::writeBits(
               unsigned int /*data*/,
               int bitWidth,
               busAddr /*row*/,
               busAddr /*col*/,
               busChan /*chan*/,
               bool /*bFlush*/ )
{
    return bitWidth;
}

int IBus::write(
           int data,
           busAddr addr,
           busChan chan
            )
{
    return writeOp( &data, sizeof(data), addr, chan );
}

int IBus::write(
           unsigned int data,
           busAddr addr,
           busChan chan
            )
{
    return writeOp( &data, sizeof(data), addr, chan );
}


int IBus::write(
           char data,
           busAddr addr,
           busChan chan
            )
{
    return writeOp( &data, sizeof(data), addr, chan );
}

int IBus::write(
           unsigned char data,
           busAddr addr,
           busChan chan
            )
{
    return writeOp( &data, sizeof(data), addr, chan );
}

int IBus::write( unsigned short data,
           busAddr addr,
           busChan chan )
{
    return writeOp( &data, sizeof(data), addr, chan );
}

int IBus::readOp(
          void */*pData*/,
          int /*len*/,
          busAddr /*addr*/,
          busChan /*chan*/)
{
    return 0;
}

int IBus::read(
          int &data,
          busAddr addr,
          busChan chan )
{
    return readOp( &data, sizeof(data), addr, chan );
}

int IBus::read(
          unsigned int &data,
          busAddr addr,
          busChan chan )
{
    return readOp( &data, sizeof(data), addr, chan );
}

int IBus::read(
          char &data,
          busAddr addr,
          busChan chan )
{
    return readOp( &data, sizeof(data), addr, chan );
}

int IBus::read(
          unsigned char &data,
          busAddr addr,
          busChan chan )
{
    return readOp( &data, sizeof(data), addr, chan );
}

int IBus::read( unsigned short &data,
          busAddr addr,
          busChan chan )
{
    return readOp( &data, sizeof(data), addr, chan );
}

void *IBus::mmap( void *addr,
            size_t size )
{
    return ::mmap( NULL,
            size,
            PROT_READ|PROT_WRITE,
            MAP_SHARED|MAP_LOCKED,
            mFd,
            (off_t)addr );
}
void IBus::munmap( void *addr,
             size_t size )
{
    ::munmap( addr, size );
}

int IBus::getFd()
{ return mFd; }

}
