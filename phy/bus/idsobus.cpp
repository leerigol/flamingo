#include "idsobus.h"

namespace dso_phy {

#define spi_bus_name    "/dev/spidev0.0"
#define axi_bus_name    "/dev/axi"
#define axi_mem_name    "/dev/mem"

IDsoBus::IDsoBus()
{
}

DsoErr IDsoBus::open()
{
    DsoErr err;

    //! spi bus open
    err = mCpldBus.open( spi_bus_name );
    if ( err != ERR_NONE )
    {
        Q_ASSERT( false );
        return err;
    }

    //! axi bus
    err = mAxiBus.open( axi_bus_name );
    if ( err != ERR_NONE )
    {
        mCpldBus.close();
        Q_ASSERT( false );
        return err;
    }

    //! mem bus
    err = mMemBus.open( axi_mem_name );
    if ( err != ERR_NONE )
    {
        mCpldBus.close();
        mAxiBus.close();
        Q_ASSERT( false );
        return err;
    }

    return ERR_NONE;
}

void IDsoBus::close()
{
    mCpldBus.close();

    mAxiBus.close();

    mMemBus.close();

    return;
}

void IDsoBus::rst()
{}

bool IDsoBus::isReady()
{
    if ( mCpldBus.isReady() && mAxiBus.isReady() /*&& mMemBus.isReady()*/ )
    { return true; }
    else
    { return false; }
}

bool IDsoBus::checkReady()
{
    //! check ready
    //! not ready
    //! rst
    //! wait 1s
    //! rst again
    //! try 3 times
    int tmo = 0;
    int tmoRst = 0;
    while ( !isReady() )
    {
       mAxiBus.rst();

       QThread::msleep( 1000 );

        tmoRst = 0;
        //while ( tmoRst < 10 )
        {
            if ( isReady() )
            {
                return true;
            }

            tmoRst++;
            QThread::msleep( 200 );
        }

        //qWarning()<<__FUNCTION__<<__LINE__<<"GTP reset TIMEOUT" << tmo;
        tmo++;
        if ( tmo > 3 )
        {
            return false;
        }
    }

    return true;
}

}
