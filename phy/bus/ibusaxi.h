#ifndef IBUSAXI_H
#define IBUSAXI_H

#include "ibus.h"

namespace dso_phy {

class IBusAxi : public IBus
{
public:
    IBusAxi();
    ~IBusAxi();
public:
    virtual DsoErr open( const char *path );
    virtual void close();

    virtual void rst();
    virtual bool isReady();

    virtual int writeOp(
                void *pData,
                int len,
                busAddr addr = 0,
                busChan chan = no_buschan
                );
    virtual int readOp(
              void *pData,
              int len,
              busAddr addr = 0,
              busChan chan = no_buschan );

protected:
    quint32 doReadDW( busAddr addr, int *pOutLen );
    int     doWrite( busAddr addr, void *pData, int len );
    void    reset();
private:
    QMutex mMutex;

};

}

#endif // IBUSAXI_H
