#ifndef IBUSSPI_IMP
#define IBUSSPI_IMP

#include "../ibusspi.h"


#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include <QDebug>

namespace dso_phy {

IBusSpi::IBusSpi() : IBus()
{
    mSpiBits = 8;
    mSpiSpeed = 3000000;
    mSpiDelay = 0;
}
IBusSpi::~IBusSpi()
{}
DsoErr IBusSpi::open( const char */*path*/ )
{
    return ERR_NONE;
}
void IBusSpi::close()
{
    return;
}

int IBusSpi::writeOp(
            void */*pData*/,
            int len,
            busAddr /*addr*/,
            busChan /*chan*/
            )
{
//    qDebug()<<"spi write"<<len;
    return len;
}
int IBusSpi::readOp(
          void */*pData*/,
          int len,
          busAddr /*addr*/,
          busChan /*chan*/ )
{
//    qDebug()<<"spi read"<<len;
    return len;
}

int IBusSpi::send( void */*pData*/,
                      int /*len*/,
                      busChan /*chan*/)
{
    return 0;
}
}


#endif // IBUSSPI_IMP

