#ifndef IBUSAXI_IMP
#define IBUSAXI_IMP

#include "ibusaxi.h"

namespace dso_phy {
IBusAxi::IBusAxi()
{
}
IBusAxi::~IBusAxi()
{
}
DsoErr IBusAxi::open( const char */*path*/ )
{
    return ERR_NONE;
}
void IBusAxi::close()
{
}

void IBusAxi::rst()
{}
bool IBusAxi::isReady()
{ return true; }

int IBusAxi::writeOp(
            void */*pData*/,
            int /*len*/,
            busAddr /*addr*/,
            busChan /*chan*/
            )
{
   return 0;
}
int IBusAxi::readOp(
          void */*pData*/,
          int /*len*/,
          busAddr /*addr*/,
          busChan /*chan*/ )
{
    return 0;
}
}

#endif // IBUSAXI_IMP

