#ifndef IBUSGP_IMP
#define IBUSGP_IMP

#include "../ibusgp.h"

namespace dso_phy {

IBusGp::IBusGp()
{}

DsoErr IBusGp::open( const char */*path*/ )
{
    return ERR_NONE;
}
void IBusGp::close()
{}

//! write bytes
int IBusGp::writeOp(
            void */*pData*/,
            int /*len*/,
            busAddr /*addr*/,
            busChan /*chan*/
            )
{
    return 0;
}

//! write bits
int IBusGp::writeBits(
           unsigned int /*data*/,
           int /*bitWidth*/,
           busAddr /*row*/,
           busAddr /*col*/,
           busChan /*chan*/,
           bool /*bFlush*/ )
{
    return 0;
}

//! read
int IBusGp::readOp(
          void */*pData*/,
          int /*len*/,
          busAddr /*addr*/,
          busChan /*chan*/)
{
    return 0;
}

}

#endif // IBUSGP_IMP

