#ifndef IBUS_H
#define IBUS_H

#include "../../include/dsotype.h"
#include <sys/mman.h>
using namespace DsoType;

#define busAddr unsigned int
#define busChan int
#define no_buschan (-1)

#define LOG_BUS()   LOG_DBG()

namespace dso_phy {

/*!
 * \brief The IBus class
 * - 可以有地址
 * - 可以有通道，例如，一条spi控制器可以有多个cs
 */
class IBus
{
public:
    IBus();

public:
    virtual DsoErr open( const char *path );
    virtual void close();

    virtual void rst();
    virtual bool isReady();

    //! write bytes
    virtual int writeOp(
                void *pData,
                int len,
                busAddr addr = 0,
                busChan chan = no_buschan
                );
    //! no addr
    virtual int send( void *pData,
                      int len,
                      busChan chan=-1);

    virtual int recv( void *pData,
                      int len,
                      busChan chan=-1);

    //! write bits
    virtual int writeBits(
               unsigned int data,
               int bitWidth,
               busAddr row,
               busAddr col,
               busChan chan = no_buschan,
               bool bFlush = true );

    int write(
               int data,
               busAddr addr = 0,
               busChan chan = no_buschan
                );

    int write(
               unsigned int data,
               busAddr addr = 0,
               busChan chan = no_buschan
                );

    int write(
               char data,
               busAddr addr = 0,
               busChan chan = no_buschan
                );

    int write(
               unsigned char data,
               busAddr addr = 0,
               busChan chan = no_buschan
                );

    int write( unsigned short data,
               busAddr addr = 0,
               busChan chan = no_buschan );

    //! read
    virtual int readOp(
              void *pData,
              int len,
              busAddr addr = 0,
              busChan chan = no_buschan );

    int read(
              int &data,
              busAddr addr = 0,
              busChan chan = no_buschan );

    int read(
              unsigned int &data,
              busAddr addr = 0,
              busChan chan = no_buschan );

    int read(
              char &data,
              busAddr addr = 0,
              busChan chan = no_buschan );

    int read(
              unsigned char &data,
              busAddr addr = 0,
              busChan chan = no_buschan );

    int read( unsigned short &data,
              busAddr addr = 0,

              busChan chan = no_buschan );

    void *mmap( void *addr,
                size_t size );
    void munmap( void *addr,
                 size_t size );

    int getFd();
protected:
    int mFd;        /*!< file description */

};

}

#endif // IBUS_H
