#ifndef FPGADBG
#define FPGADBG

#include <QString>
#include <QMap>

namespace fpgaMap {

QMap<uint, QString>  readMap =
{
    {0x600,  "wpu_status"},
    {0x1c04, "scu_ctrl"},
    {0x1c08, "scu_state"},
};
QMap<uint, QString>  writeMap =
{
    {0x600,  "wpu_status"},
    {0x1c04, "scu_ctrl"},
    {0x1c08, "scu_state"},
};
}
#endif // FPGADBG

