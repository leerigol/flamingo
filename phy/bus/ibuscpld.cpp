#include "ibuscpld.h"

#include <QDebug>

namespace dso_phy {

IBusCpld::IBusCpld( int capacity )
{
    Q_ASSERT( capacity > 0 );

    mCapacity = capacity;
    m_pMemory = NULL;
}
IBusCpld::~IBusCpld()
{
    if ( NULL != m_pMemory )
    {
        delete []m_pMemory;
    }
}

DsoErr IBusCpld::open( const char *path )
{
    DsoErr err;

    //! base open
    err = IBusSpi::open( path );

    if ( err != ERR_NONE )
    {
        return err;
    }

    //! allocate memory
    m_pMemory = new cpld_mem_chunk[ mCapacity ];
    Q_ASSERT( m_pMemory != NULL );

    return ERR_NONE;
}

void IBusCpld::close()
{
    IBusSpi::close();

    if ( m_pMemory != NULL )
    {
        delete []m_pMemory;
        m_pMemory = NULL;
    }
}

/*!
 * \brief IBusCpld::writeBits
 * \param data
 * \param bitWidth
 * \param row
 * \param col  0~15
 * \param bFlush
 * \return
 * 按位写入指定地址
 * -如果宽度超出了字节范围，则向高地址继续写入
 */
int IBusCpld::writeBits(
               unsigned int data,
               int bitWidth,
               busAddr row,
               busAddr col,
               busChan chan,
              bool bFlush )
{
    cpld_mem_chunk raw;
    int leftWidth, curRow, lastRow;
    cpld_mem_chunk mask;

    //! write none
    if ( /*col < 0 ||*/ col >= 16 )
    {
        return 0;
    }

    curRow = row;

    //! pre pad
    raw = m_pMemory[ curRow ];
    mask = getLMask( col, bitWidth );
    raw = ( ( raw & (~mask) ) | (data<<col) ) & 0xffff;
    m_pMemory[ curRow ] = raw;

    leftWidth = bitWidth - ( 16 - col );
    data >>= col;

    //! aligned
    while ( leftWidth >= 16 )
    {
        curRow++;

        raw = m_pMemory[ curRow ];
        raw = ( data & 0xffff );
        m_pMemory[ curRow ] = raw;

        leftWidth -= 16;
        data >>= 16;
    }

    //! last pad
    if ( leftWidth > 0 )
    {
        curRow++;
        mask = getRMask( leftWidth );

        raw = m_pMemory[ curRow ];
        raw = ( raw & (~mask) ) | ( data & mask );
        m_pMemory[ curRow ] = raw;
    }
    lastRow = curRow;

    //! flush out
    if ( bFlush )
    {
        //! write the data
        for ( curRow = row; curRow <= lastRow; curRow++ )
        {
            IBus::write( m_pMemory[curRow],
                         (busAddr)curRow,
                         chan );
        }
    }

    return bitWidth;
}

/*!
 * \brief IBusCpld::getRMask
 * \param bitN
 * \return
 * bit0 ~ 0
 * bit1 ~ 1
 * bit2 ~ 3
 * 获取相应位宽的右掩码数值
 * -’右‘的意思是 从低位0开始
 * -bitW: 0--0
 * -bitW: 1--1
 * -bitW: 2--3
 * -bitW: 3--7
 */
cpld_mem_chunk IBusCpld::getRMask( int bitW )
{
    int i;
    cpld_mem_chunk mask;

    mask = 0;
    for ( i = 0; i < bitW; i++ )
    {
        mask <<= 1;
        mask |= 1;
    }

    return mask;
}

/*!
 * \brief IBusCpld::getLMask
 * \param bitN
 * \param wid
 * \return
 * 获取相应位宽的左掩码数值
 * -’左‘的意思是 从指定位到高位
 */
cpld_mem_chunk IBusCpld::getLMask( int bitN, int wid )
{
    cpld_mem_chunk mask;
    int w;

    mask = 1;
    for ( w = 1; w < wid; w++ )
    {
        mask <<= 1;
        mask |= 1;
    }

    mask <<= bitN;

    mask &= 0xffff;

    return mask;
}

}

