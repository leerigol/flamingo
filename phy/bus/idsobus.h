#ifndef IDSOBUS_H
#define IDSOBUS_H

#include "ibuscpld.h"
#include "ibusgp.h"
#include "ibusaxi.h"

namespace dso_phy {

/*!
 * \brief The IDsoBus class
 * bus manager
 */
class IDsoBus
{
public:
    IDsoBus();

public:
    DsoErr open();
    void close();

    void rst();
    bool isReady();
    bool checkReady();

public:
    IBusCpld mCpldBus;      //! spi on cpld
//    IBusGp   mGpBus;        //! gp bus on zynq
    IBusAxi  mAxiBus;
    IBusAxi  mMemBus;
};

}

#endif // IDSOBUS_H
