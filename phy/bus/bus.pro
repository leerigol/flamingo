TEMPLATE = lib

TARGET = ../../lib$$(PLATFORM)/bus

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

QT += widgets
#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

# dbg show
dbg = $$(_DEBUG)
equals(dbg, _DEBUG){
#DEFINES += _DEBUG
}
#DEFINES += AXI_LOG

CONFIG += static
CONFIG += c++11

HEADERS += \
    ibusspi.h \
    ibus.h \
    idsobus.h \
    simulate/ibusspi_imp.h \
    arm/ibusspi_imp.h \
    ibuscpld.h \
    ibusgp.h \
    simulate/ibusgp_imp.h \
    arm/ibusgp_imp.h \
    ibusaxi.h \
    simulate/ibusaxi_imp.h \
    arm/ibusaxi_imp.h \
    fpgadbg.h

SOURCES += \
    ibus.cpp \
    ibusspi.cpp \
    idsobus.cpp \
    ibuscpld.cpp \
    ibusgp.cpp \
    ibusaxi.cpp
