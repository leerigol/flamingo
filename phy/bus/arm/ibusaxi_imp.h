#ifndef IBUSAXI_IMP
#define IBUSAXI_IMP


#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <QDebug>
#include "ibusaxi.h"


#include "../../include/dsodbg.h"

namespace dso_phy {

#ifdef AXI_LOG
QFile axi_file("/tmp/axi.log");
QTextStream axi_log;
//static int counter = 0;
#endif

IBusAxi::IBusAxi()
{
}
IBusAxi::~IBusAxi()
{
    close();
}
DsoErr IBusAxi::open( const char *path )
{
    int fd;

    //! open device
    fd = ::open(path,O_RDWR);
    if (fd < 0)
        return ERR_AXI_OPEN_FAIL;

    //! save handle
    mFd = fd;
    return ERR_NONE;
}
void IBusAxi::close()
{
    if ( mFd != -1 )
    {
        ::close( mFd );
        mFd = -1;
    }
}

void IBusAxi::rst()
{
    write( (unsigned int)1, 0 );
    write( (unsigned int)0, 0 );
}
bool IBusAxi::isReady()
{
    unsigned int stat;
    read( stat, 8 );

    return ( stat == 1 );
}

int IBusAxi::writeOp(
            void *pData,
            int len,
            busAddr addr,
            busChan /*chan*/
            )
{

    Q_ASSERT( len == 4 && mFd != -1 );
    if ( addr >= 64 * 1024 )
    {
        LOG_DBG()<<addr<<mFd;
        Q_ASSERT( false );
    }

#if 0
//    if(addr == 0x1c08)
//    {
//        unsigned int _data = *(unsigned int*)pData;
//        int rlen;
//        {
//            quint32 _wpu_status = doReadDW( 0x600,  &rlen);
//            quint32  _scu_status = doReadDW( 0x1c04,  &rlen);
//            quint32  _scu_run   = doReadDW( 0x1c08,  &rlen);
//            qDebug() << "-------WPU STATUS BEFORE RUN SCU:" <<QString::number(_wpu_status,16);
//            qDebug() << "-------SCU STATUS" << QString::number(_scu_status,16);
//            qDebug() << "-------SCU CRTL BEFORE RUN SCU:" <<QString::number(_scu_run,16);

//            qDebug() << "-------RUN or STOP SCU Config:" <<QString::number( addr, 16 )<<QString::number(_data,16);
//            qDebug() << "---------------------------------------------------------";
//        }
//    }

//    if(addr == 0x414)
//    {
//        unsigned int _data;
//        int rlen;

//        quint32 _wpu_status = doReadDW( 0x600,  &rlen);
//        quint32  _scu_status = doReadDW( 0x1c04,  &rlen);
//        quint32  _scu_run   = doReadDW( 0x1c08,  &rlen);
//        qDebug() << "-------WPU STATUS BEFORE Config WPU:" <<QString::number(_wpu_status,16);
//        qDebug() << "-------SCU STATUS BEFORE Config WPU:" << QString::number(_scu_status,16);
//        qDebug() << "-------SCU CRTL BEFORE Config WPU:" <<QString::number(_scu_run,16);

//        _data = *(unsigned int*)pData;
//        qDebug() << "-------Config WPU:" <<QString::number( addr, 16 )<<QString::number(_data,16);
//        qDebug() << "---------------------------------------------------------";
//    }

    if(addr == 0x584)
    {
        unsigned int _data;
        int rlen;

        quint32 _wpu_status = doReadDW( 0x600,  &rlen);
        quint32  _scu_status = doReadDW( 0x1c04,  &rlen);
         quint32  _scu_run   = doReadDW( 0x1c08,  &rlen);
         quint32 _wpu550    = doReadDW( 0x550,  &rlen);
         qDebug() << "-------WPU STATUS BEFORE CLEAR WPU:" <<QString::number(_wpu_status,16);
         qDebug() << "-------SCU STATUS BEFORE CLEAR WPU:" << QString::number(_scu_status,16);
         qDebug() << "-------SCU CRTL BEFORE CLEAR WPU:" <<QString::number(_scu_run,16);
        qDebug() << "-------WPU COLORMAP BEFORE SET:" <<QString::number(_wpu550,16);

        _data = *(unsigned int*)pData;
        qDebug() << "-------CLEAR WPU :" <<QString::number( addr, 16 )<<QString::number(_data,16);
         qDebug() << "---------------------------------------------------------";
    }
#endif

#ifdef AXI_LOG
    static bool bLog = false;

    if(!bLog)
    {
        bLog = axi_file.open(QIODevice::WriteOnly|QIODevice::Text);
        axi_log.setDevice(&axi_file);
    }

    if(bLog)
    {
        bool need_flush = false;
        if(addr == 0x1c08)
        {
            unsigned int _data;
            int rlen;

            quint32 _wpu_status = doReadDW( 0x600,  &rlen);
            quint32  _scu_status = doReadDW( 0x1c04,  &rlen);
            quint32  _scu_run   = doReadDW( 0x1c08,  &rlen);
             axi_log << "-------WPU STATUS BEFORE RUN SCU:" <<QString::number(_wpu_status,16) << "\n";
             axi_log << "-------SCU STATUS BEFORE RUN SCU:" << QString::number(_scu_status,16)<< "\n";;
             axi_log << "-------SCU CRTL BEFORE RUN SCU:"   <<QString::number(_scu_run,16)<< "\n";;

            _data = *(unsigned int*)pData;
            axi_log << "-------RUN or STOP SCU Config:" <<QString::number( addr, 16 )<<QString::number(_data,16)<< "\n";;
            axi_log << "---------------------------------------------------------"<< "\n";

            need_flush=true;
        }

        if(addr == 0x414)
        {
            unsigned int _data;
            int rlen;

            quint32 _wpu_status = doReadDW( 0x600,  &rlen);
            quint32  _scu_status = doReadDW( 0x1c04,  &rlen);
             quint32  _scu_run = doReadDW( 0x1c08,  &rlen);
             axi_log << "-------WPU STATUS BEFORE RUN SCU:" <<QString::number(_wpu_status,16) << "\n";
             axi_log << "-------SCU STATUS BEFORE RUN SCU:" << QString::number(_scu_status,16)<< "\n";;
             axi_log << "-------SCU CRTL BEFORE RUN SCU:"   <<QString::number(_scu_run,16)<< "\n";;

            _data = *(unsigned int*)pData;
            axi_log << "-------Config WPU:" <<QString::number( addr, 16 )<<QString::number(_data,16)<<"\n";;
            axi_log << "---------------------------------------------------------"<< "\n";;
            need_flush=true;
        }

        if(addr == 0x584)
        {
            unsigned int _data;
            int rlen;

            quint32 _wpu_status = doReadDW( 0x600,  &rlen);
            quint32  _scu_status = doReadDW( 0x1c04,  &rlen);
             quint32  _scu_run   = doReadDW( 0x1c08,  &rlen);
             axi_log << "-------WPU STATUS BEFORE RUN SCU:" <<QString::number(_wpu_status,16) << "\n";
             axi_log << "-------SCU STATUS BEFORE RUN SCU:" << QString::number(_scu_status,16)<< "\n";;
             axi_log << "-------SCU CRTL BEFORE RUN SCU:"   <<QString::number(_scu_run,16)<< "\n";;


            _data = *(unsigned int*)pData;
            axi_log << "-------CLEAR WPU :" <<QString::number( addr, 16 )<<QString::number(_data,16) <<"\n";;
            axi_log << "---------------------------------------------------------"<< "\n";;
            need_flush=true;
        }
        if(need_flush)
        {
            fsync(axi_file.handle());
        }
    }
#endif

    mMutex.lock();
    int retLen;
    retLen = doWrite(addr, pData, len );

    //! can write only 1 time for dg
    mMutex.unlock();
    return retLen;
}

int IBusAxi::readOp(
          void *pData,
          int len,
          busAddr addr,
          busChan /*chan*/ )
{
    Q_ASSERT( len == 4 && mFd != -1 );

    mMutex.lock();

    int outLen;
    quint32 dwRead;

#ifndef _AXI_TEST
    //! for x times
    for ( int i = 0; i < 3; i++ )
    {
        dwRead = doReadDW( addr, &outLen );

        //! check error
        if ( outLen != 4 )
        { continue; }

        //! error code
        if ( dwRead == 0xeeeeeeee && doReadDW(0x0c04, &outLen) == 1 )
        {
            qWarning()<< "AXI read 0x" << QString().setNum(addr,16) << " fail!!!";
            //reset();
            continue;
        }
        else
        {
            break;
        }
    }
#else
    dwRead = doReadDW( addr, &outLen );
#endif

    mMutex.unlock();

    *(quint32*)pData = dwRead;

    return outLen;
}

quint32 IBusAxi::doReadDW( busAddr addr, int *pOutLen )
{
    quint32 dat;

    Q_ASSERT( NULL != pOutLen );

    ioctl( mFd, 0, addr );

    if ( ::read( mFd, &dat, 4 ) != 4 )
    {
        *pOutLen = 0;
        return 0;
    }
    else
    {
        *pOutLen = 4;
        return dat;
    }
}

int IBusAxi::doWrite( busAddr addr, void *pData, int len )
{
    ioctl( mFd, 0, addr );

    return ::write( mFd, pData, len );
}

void IBusAxi::reset()
{
    quint32 rstCode;

    rstCode = 1;
    doWrite( 0, &rstCode, 4 );

    rstCode = 0;
    doWrite( 0, &rstCode, 4 );
}

}

#endif // IBUSAXI_IMP

