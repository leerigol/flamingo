#ifndef AXI
#define AXI
#include <QMutex>
#include "../ibus.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

class IAxi
{
public:
    IAxi();
    ~IAxi();
public:
    int open( const char *path );
    void close();

    int writeOp(
                void *pData,
                int len,
                busAddr addr = 0,
                busChan chan = 0
                );
    int readOp(
              void *pData,
              int len,
              busAddr addr = 0,
              busChan chan = 0 );
private:
    QMutex mMutex;
    int mFd;//no meaning

};


#endif // AXI
