#ifndef IBUSSPI_IMP
#define IBUSSPI_IMP

#include <QDebug>
#include "ibusspi.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <linux/spi/spidev.h>

#define SPI_IOC_SELECT_CS   _IOW( SPI_IOC_MAGIC, 100, __u8 )
#define SPI_IOC_TRANSFER	_IOW(SPI_IOC_MAGIC, 102, __u8)
namespace dso_phy {

IBusSpi::IBusSpi() : IBus()
{
    mSpiBits = 8;
//    mSpiSpeed = 10000000;
//    mMaxSpiSpeed = 10000000;
    mSpiSpeed = 1000000;
    mMaxSpiSpeed = 1000000;
    mSpiDelay = 0;

    mFd = -1;
}

IBusSpi::~IBusSpi()
{
    close();
}

DsoErr IBusSpi::open( const char *path )
{
    int ret;
    int fd;
    int mode;

    //! open device
    fd = ::open(path,O_RDWR);
    if (fd < 0)
    {
        return ERR_SPI_OPEN_FAIL;
    }

    //! save handle
    mFd = fd;

    mode = SPI_MODE_0;
//    mode = SPI_MODE_3;
    //! config spi
    do
    {
        //! mode
        ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
        if (ret == -1)
           break;

        ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
        if (ret == -1)
           break;

        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &mSpiBits);
        if (ret == -1)
           break;

        ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &mSpiBits);
        if (ret == -1)
           break;

        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &mMaxSpiSpeed);
        if (ret == -1)
            break;

        ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &mMaxSpiSpeed);
        if (ret == -1)
            break;
    }while(0);

    if ( ret == -1 )
    {
        ::close( mFd );
        mFd = -1;
        return ERR_SPI_CFG_FAIL;
    }

    return ERR_NONE;
}
void IBusSpi::close()
{
    if ( mFd != -1 )
    {
        ::close( mFd );
        mFd = -1;
    }
}

int IBusSpi::writeOp(
            void *pData,
            int len,
            busAddr addr,
            busChan chan
            )
{
    if ( NULL == pData )  return 0;
    if ( len <= 0 ) return 0;

    unsigned char buf[ len + 1];

    buf[0] = addr & 0xff;
    memcpy( buf + 1, pData, len );

    return spiSend( buf, len + 1, chan );

}
int IBusSpi::readOp(
          void *pData,
          int len,
          busAddr addr,
          busChan chan )
{
    if ( NULL == pData )  return 0;
    if ( len <= 0 ) return 0;

    unsigned char buf[ len + 1];

    //! one addr byte
    spiRecv( addr, buf, len + 1 , chan );

    //! may need to invert
    memcpy( pData, buf + 1, len );

    return len;
}

int IBusSpi::send( void *pData,
                  int len,
                  busChan chan)
{
    return spiSend( pData, len, chan );
}



int IBusSpi::spiSend( void *pData, int len, busChan chan )
{
    int err = 1;
    int ret = 0;
    mMutex.lock();
//    //! set cs
//    int ret = ioctl( mFd, SPI_IOC_SELECT_CS, &chan );
//    if ( ret != 0 )
//    {
//        err = 0;
//    }
//    else
    {
        //! transaction
        struct spi_ioc_transfer tr;

        memset(&tr, 0, sizeof(tr));

        tr.tx_buf = (__u64)pData;
        tr.rx_buf = 0;
        tr.len = len;
        tr.delay_usecs = 100;//mSpiDelay;
        tr.speed_hz = mSpiSpeed;
        tr.bits_per_word = mSpiBits;
        tr.cs_change = chan;// 0-cpld 1-dev
        //执行spidev.c中ioctl的default进行数据传输
        //! transaction
        ret = ioctl( mFd, SPI_IOC_TRANSFER, &tr);
        if ( ret == -1 )
        {
            err = 0;
        }

//        if( 2 == read_touch( 0x34 ) )
//        {
//            qDebug() <<"To CPLD:" << QString().setNum(*(int*)pData,16);
//        }
    }
    mMutex.unlock();

    return err;
}

int IBusSpi::spiRecv( int addr,
                      void *pData,
                      int len,
                      busChan chan )
{
    Q_ASSERT( chan != no_buschan );

    //! buffer
    unsigned char txBuf[ len ];

    //! address
    txBuf[0] = addr;

    int ret;
    //! set cs
    ret = ioctl( mFd, SPI_IOC_SELECT_CS, &chan );
    if ( ret != 0 )
    { return 0; }

    //! transaction
    struct spi_ioc_transfer tr;

    tr.tx_buf = (__u64)txBuf;
    tr.rx_buf = (__u64)pData;
    tr.len = len;
    tr.delay_usecs = mSpiDelay;
    tr.speed_hz = mSpiSpeed;
    tr.bits_per_word = mSpiBits;
    tr.cs_change = 0;

    ret = ioctl( mFd, SPI_IOC_MESSAGE(1), &tr);
    if ( ret == -1 )
        return 0;

    //! attach data
//    dataByte = rxBuf[1];

//    quint8 *p8 = (quint8*)pData;
//    qDebug()<<p8[0]<<p8[1]<<p8[2];

    return 1;
}

}


#endif // IBUSSPI_IMP

