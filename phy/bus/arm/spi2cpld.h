#ifndef SPI2CPLD_H
#define SPI2CPLD_H

typedef unsigned short cpld_mem_chunk;

class spi2cpld
{
public:
    spi2cpld();

public:
    IBusCpld( int capacity = 0x0e );
    ~IBusCpld();
public:
    virtual DsoErr open( const char *path );
    virtual void close();

public:
    virtual int writeBits(
               unsigned int data,
               int bitWidth,
               busAddr row,
               busAddr col,
               busChan chan = no_buschan,
               bool bFlush = true );

private:
    cpld_mem_chunk getRMask( int bitN );
    cpld_mem_chunk getLMask( int bitN, int wid );

private:
    int mCapacity;  //! size in mem chunk
    cpld_mem_chunk *m_pMemory;

};

#endif // SPI2CPLD_H
class IBusCpld : public IBusSpi
{

};
