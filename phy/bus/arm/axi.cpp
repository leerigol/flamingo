#include "axi.h"
#include <QDebug>


#define O_RDWR		     02
IAxi::IAxi()
{
    mFd = -1;//no meaning
}
IAxi::~IAxi()
{
    close();
}
int IAxi::open( const char *path )
{
    int fd;

    //! open device
    fd = ::open(path,O_RDWR);
    if (fd < 0)
        return 0;

    //! save handle
    mFd = fd;

    return 0;
}
void IAxi::close()
{
    if ( mFd != -1 )
    {
        ::close( mFd );
        mFd = -1;
    }
}

int IAxi::writeOp(
            void *pData,
            int len,
            busAddr addr,
            busChan /*chan*/
            )
{
    qDebug()<<"";
    Q_ASSERT( len == 4 && mFd != -1 );
    if ( addr >= 64 * 1024 )
    {
        qDebug()<<addr<<mFd;
        Q_ASSERT( false );
    }

    mMutex.lock();

    ioctl( mFd, 0, addr );
    if ( ::write( mFd, pData, len ) != len )
    {
        mMutex.unlock();
        return 0;
    }
    else
    {
        mMutex.unlock();
        return len;
    }
}
int IAxi::readOp(
          void *pData,
          int len,
          busAddr addr,
          busChan /*chan*/ )
{
    Q_ASSERT( len == 4 && mFd != -1 );

//    qDebug()<<__FUNCTION__<<__LINE__<<mFd<<addr;

    mMutex.lock();

    ioctl( mFd, 0, addr );

    if ( ::read( mFd, pData, 4 ) != 4 )
    {
        mMutex.unlock();
        return 0;
    }
    else
    {
        mMutex.unlock();
        return len;
    }
}
