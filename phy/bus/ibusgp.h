#ifndef IBUSGP_H
#define IBUSGP_H

#include "ibus.h"

namespace dso_phy {

class IBusGp : public IBus
{
public:
    IBusGp();

public:
    virtual DsoErr open( const char *path );
    virtual void close();

    //! write bytes
    virtual int writeOp(
                void *pData,
                int len,
                busAddr addr = 0,
                busChan chan = no_buschan
                );

    //! write bits
    virtual int writeBits(
               unsigned int data,
               int bitWidth,
               busAddr row,
               busAddr col,
               busChan chan = no_buschan,
               bool bFlush = true );

    //! read
    virtual int readOp(
              void *pData,
              int len,
              busAddr addr = 0,
              busChan chan = no_buschan );
};

}

#endif // IBUSGP_H
