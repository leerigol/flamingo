#ifndef IBUSSPI_H
#define IBUSSPI_H

#include "ibus.h"

namespace dso_phy {

class IBusSpi : public IBus
{
public:
    IBusSpi();
    ~IBusSpi();
    /*!
     * \brief setCs
     * \param cs
     * 一个spi上可以连接多个cs
     */
    void setCs( int cs );

public:
    virtual DsoErr open( const char *path );
    virtual void close();

    virtual int writeOp(
                void *pData,
                int len,
                busAddr addr = 0,
                busChan chan = no_buschan
                );
    virtual int readOp(
              void *pData,
              int len,
              busAddr addr = 0,
              busChan chan = no_buschan );

    virtual int send( void *pData,
                      int len,
                      busChan chan=-1);

protected:
    int spiSend( void *pData,
                 int len,
                 busChan chan = no_buschan );
    int spiRecv( int addr,
                 void *pData,
                 int len,
                 busChan chan = no_buschan );

private:
    unsigned char mSpiMode;
    unsigned char mSpiBits;
    unsigned long mSpiSpeed;
    unsigned long mMaxSpiSpeed;
    unsigned short mSpiDelay;

    QMutex mMutex;
};

}

#endif // IBUSSPI_H
