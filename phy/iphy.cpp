#include "iphy.h"

namespace dso_phy {

QMutex IPhy::_phyMutex;
QMutex IPhy::_mapMutex;

regCache::regCache( cache_addr addr, void *pMem )
{
    mAddr = addr;
    m_pMem = pMem;
}
regCache::regCache()
{
}



IPhy::IPhy()
{
    m_pBus = NULL;
}

int IPhy::attachBus( IBus *pBus )
{
    Q_ASSERT( pBus != NULL );

    //! first bus
    if ( m_pBus == NULL )
    { m_pBus = pBus; }

    mBuses.append( pBus );

    return 0;
}
void IPhy::detachBus()
{
}

void IPhy::init()
{}

void IPhy::loadOtp()
{}
void IPhy::reMap()
{}

void IPhy::flushWCache()
{
    QMutexLocker locker(&_phyMutex);

    regCache *pItem;
    foreach( pItem, mwCache )
    {
        Q_ASSERT( NULL != pItem );
        Q_ASSERT( NULL != pItem->m_pMem );

        write( pItem->m_pMem, pItem->mAddr );

        delete pItem;
    }

    mwCache.clear();
}

void IPhy::flushRCache()
{
    QMutexLocker locker(&_phyMutex);

    regCache *pItem;
    foreach( pItem, mrCache )
    {
        Q_ASSERT( NULL != pItem );
        delete pItem;
    }
    mrCache.clear();
}

void IPhy::write( void */*pMem*/, cache_addr /*addr*/ )
{}
void IPhy::read( void */*pMem*/, cache_addr /*addr*/ )
{}

void IPhy::waitIdle( int nms )
{
    QThread::msleep( nms );
}

bool IPhy::checkMem( void *pMem )
{
    //QMutexLocker locker(&_mapMutex);
    _mapMutex.lock();

    Q_ASSERT( NULL != pMem );

    if( mMap.size() == 0 )
    {
        qDebug()<<__FILE__<<__LINE__<<"error of map";
    }

    bool b = mMap.contains( pMem);

    _mapMutex.unlock();

    return b;
}
void IPhy::lockWCache()
{
    IPhy::_phyMutex.lock();
}
void IPhy::unlockWCache()
{
    IPhy::_phyMutex.unlock();
}

void IPhy::appendWCache( cache_addr adr, void *pMem )
{
    QMutexLocker locker(&_phyMutex);

    regCache *pItem;
    pItem = findCache( adr, mwCache );
    //! do not exist now
    if ( NULL == pItem )
    {
        pItem = new regCache( adr, pMem );
        Q_ASSERT( NULL != pItem );

        mwCache.append( pItem );
    }
}
void IPhy::appendRCache( cache_addr adr, void *pMem )
{
    QMutexLocker locker(&_phyMutex);

    regCache *pItem;

    pItem = findCache( adr, mrCache );
    //! do not exist now
    if ( NULL == pItem )
    {
        pItem = new regCache( adr, pMem );
        Q_ASSERT( NULL != pItem );

        mrCache.append( pItem );
    }
}

void IPhy::removeWCache( cache_addr addr )
{
    QMutexLocker locker(&_phyMutex);
    if(!mwCache.isEmpty())
    {
        removeCache( addr, mwCache );
    }
}

void IPhy::mapIn( void *pMem, cache_addr addr )
{
    //QMutexLocker locker(&_mapMutex);
    _mapMutex.lock();

    if ( !mMap.contains( pMem) )
    {
        mMap.insert( pMem, addr );
    }
    _mapMutex.unlock();
}

void IPhy::mapOut( void *pMem )
{
    //QMutexLocker locker(&_mapMutex);
    _mapMutex.lock();

    if ( mMap.contains( pMem) )
    {
        mMap.remove( pMem );
    }
    else
    {
        qWarning()<<"!!!no map item";
    }
    _mapMutex.unlock();
}

void IPhy::wcache( void *pMem )
{
    //QMutexLocker locker(&_mapMutex);
    _mapMutex.lock();

    if ( mMap.contains(pMem) )
    {
        appendWCache( mMap[pMem], pMem );
    }
    else
    {
#ifndef _SIMULATE
        qWarning()<<"Error::no addr in map"<<QString::number( (quint32)pMem, 16  );
#endif
    }

    _mapMutex.unlock();
}

#if 0
//! direct out
void IPhy::out( void *pMem )
{
    QMutexLocker locker(&_mapMutex);

    if ( mMap.contains(pMem) )
    {
        write( pMem, mMap[pMem] );

        //! remove the addr from cache now
        removeWCache( mMap[pMem] );
    }
    else
    {
#ifndef _SIMULATE
        qWarning()<<"Error::no addr in map"<<QString::number( (quint32)pMem, 16  );
#endif
    }
}

#else //'2W次从180ms减到120ms

#define OUT_CACHE_SIZE  (10)
struct CAddrMap
{
    void       *pMem;
    cache_addr  addr;
};
CAddrMap  a_outCache[OUT_CACHE_SIZE];

/*!
 * \brief IPhy::out
 */
void IPhy::out( void *pMem )
{
    //QMutexLocker locker(&_mapMutex);

    _mapMutex.lock();

    cache_addr addr = 0;
    static int index = 0;
    //!在cache中查找
    bool bCache = false;
    for (int i = 0; i < OUT_CACHE_SIZE;  ++i)
    {
        if(a_outCache[i].pMem == pMem)
        {
            addr = a_outCache[i].addr;
            bCache = true;
        }
    }

    //!cache中没有pMem，在map中查找，并放入cache。
    if(!bCache)
    {
        if ( mMap.contains(pMem) )
        {
            a_outCache[index].pMem = pMem;
            a_outCache[index].addr = mMap[pMem];

            addr = a_outCache[index].addr;
            index++;

            if(index >= OUT_CACHE_SIZE )
            {
                index = 0;
                //qDebug()<<__FILE__<<__LINE__<<mMap.count();
            }
        }
        else
        {
            qWarning()<<"Error::no addr in map"<<QString::number( (quint32)pMem, 16  );
        }
    }

    //! 写入总线（这里虚函数层次太多也是一个耗时的地方，暂时没优化）
    Q_ASSERT(0 != addr);
    write( pMem, addr );

    //! remove the addr from cache now
    removeWCache( addr);

    _mapMutex.unlock();
}
#endif

void IPhy::in( void *pMem )
{
    //QMutexLocker locker(&_mapMutex);
    _mapMutex.lock();

    if ( mMap.contains(pMem) )
    {
        read( pMem, mMap[pMem] );
    }
    else
    {
#ifndef _SIMULATE
        qWarning()<<"Error::no addr in map"<<QString::number( (quint32)pMem, 16  );
#endif
    }
    _mapMutex.unlock();
}

void IPhy::setBits( unsigned int &dat,
                    unsigned int val,
                    int col,
                    int width )
{
    unsigned int mask;
    unsigned int tmp;

    mask = 1;
    while( width > 1 )
    {
        mask <<= 1;
        mask |= 1;
        width--;
    }

    //! shift mask
    mask <<= col;

    tmp = dat;

    val = ( val << col ) & mask;

    tmp &=~mask;
    tmp |= val;

    dat = tmp;
}

void IPhy::setBits( unsigned int *pData,
              unsigned int val,
              int col, int width )
{
    Q_ASSERT( NULL != pData );

    unsigned int data = *pData;

    setBits( data, val, col, width );

    *pData = data;
}

unsigned int IPhy::getBits( unsigned int dat,
                            int col,
                            int width )
{
    unsigned int mask;

    mask = 1;
    while( width > 1 )
    {
        mask <<= 1;
        mask |= 1;
        width--;
    }

    //! shift mask
    mask <<= col;

    dat = (dat & mask)>>col;

    return dat;
}

/*!
 * \brief IPhy::toLE
 * \param dat
 * 大小端数据调整
 */
void IPhy::toLE( short &dat )
{
    short raw;

    raw = dat;

    raw = (( dat << 8 )&0xff00) | ( (dat>>8) & 0x00ff );

    dat = raw;
}

void IPhy::toLE( unsigned short &dat )
{
    unsigned short raw;

    raw = dat;

    raw = (( dat << 8 )&0xff00) | ( (dat>>8) & 0x00ff );

    dat = raw;
}

unsigned int IPhy::pack24AddrData( int addr,
                                   unsigned short data )
{
    unsigned int packData;

    toLE( data );

    packData = (addr & 0xff) | ( data << 8);

    return packData;
}

regCache *IPhy::findCache( cache_addr addr,
                     QList <regCache*> & lst )
{
    regCache *pItem;

    foreach( pItem, lst )
    {
        if( pItem == NULL )
        {
            qDebug() << __FILE__ << __FUNCTION__ << "addr=" << addr << "size="<<lst.size();
            Q_ASSERT( NULL != pItem );
        }

        if ( pItem->mAddr == addr )
        {
            return pItem;
        }
    }

    return NULL;
}

void IPhy::removeCache( cache_addr addr,
                  QList <regCache*> &lst )
{
    regCache *pCache;

    pCache = findCache( addr, lst );

    if ( NULL != pCache )
    {
        lst.removeAll( pCache );
        delete pCache;
    }
}

static int _wait( int us )
{
    Q_ASSERT( us > 0 );

#if 0
    //! align to ms
    us = (us + 1000)/1000;

    QThread::msleep( 1 );

    return us * 1000;
#else
    QThread::usleep( us * 1000);
    return us;
#endif
}

//! 0 -- wait completed
//! -1 -- timeout
int IPhy::wait( IPhy::p_completed iscompleted,
                int tmous,
                int tickus
                )
{
    Q_ASSERT( tickus > 0 );
    Q_ASSERT( tmous > 0 );

    //! wait on condition
    if ( iscompleted != NULL )
    {
        if ( (this->*iscompleted)() )
        {
            return 0;
        }
    }
    //! wait for timeout
    else
    {
        Q_ASSERT(iscompleted != NULL);
    }

    do
    {
//        qDebug() << "get trace timeout:" << tmous;
        //! wait a few time
        if ( tmous > 0 )
        {
            if ( (this->*iscompleted)() )
            {
                return 0;
            }
            tmous -= _wait( tickus );
        }

        //! check end
        if ( tmous <= 0 )
        {
            //! wait on condition
            if ( iscompleted != NULL )
            {
                if ( (this->*iscompleted)() )
                {
                    return 0;
                }
                else
                {
                    qWarning()<<"trace wait time out";
                    return -1;
                }
            }
            //! wait for timeout
            else
            {
                Q_ASSERT( iscompleted != NULL);
                return 0;
            }
        }
    }while( 1 );

    return 0;
}

void IPhy::usleep( int tmous )
{
    _wait( tmous );
}

cache_addr IPhy::memberAddr( void *pMember )
{
    //QMutexLocker locker(&_mapMutex);
    _mapMutex.lock();

    cache_addr ret = 0;
    if ( mMap.contains(pMember) )
    {
        ret= mMap[pMember];
    }

    _mapMutex.unlock();

    return ret;
}

}

