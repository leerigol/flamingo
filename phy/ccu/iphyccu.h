#ifndef IPHYCCU_H
#define IPHYCCU_H

#include <QMutex>
#include "../iphyfpga.h"
#include "../spu/iphyspu.h"     //! control spu
#include "../scu/iphyscu.h"     //! control scu
namespace dso_phy {

#include "../ic/ccu.h"

class IPhyCcu : public CcuMem
{
public:
    IPhyCcu();
public:
    virtual void init();
    virtual void loadOtp();
    virtual void reMap();

    void config( bool g20 = false );

    void attachSpu( IPhySpuGp *pGp );
    void attachScu( IPhyScuGp *pGp );
public:
    //! write
    void setCcuRun( Ccu_reg b );
    Ccu_reg getCcuRun();
    Ccu_reg getCcuStop();

    void setForceStop();

    void playLast(bool playWpu = true, bool playTrace = true);
public:
    void waitStop();

    //! overwrite
public:
    //! ps
    void setAUTO_TRIG_TIMEOUT( qlonglong tmo );
    void setAUTO_TRIG_HOLD_OFF( qlonglong tmo );

    void setMeasClose();
    void setSearchClose();

protected:
    IPhySpuGp *m_pSpuGp;
    IPhyScuGp *m_pScuGp;
private:
    const static int _tickTime = 100;   //! us

};

}

#endif // IPHYCCU_H
