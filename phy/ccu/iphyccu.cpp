#include "iphyccu.h"
#include "../../service/servdso/sysdso.h"

namespace dso_phy {

#include "../ic/ccu.cpp"

IPhyCcu::IPhyCcu()
{
    m_pSpuGp = NULL;
    m_pScuGp = NULL;
}

void IPhyCcu::init()
{
    setCcuRun( false );     //! default to stop
}
void IPhyCcu::loadOtp()
{
    _loadOtp();
}
void IPhyCcu::reMap()
{
    _remap_Ccu_();
}

void IPhyCcu::config( bool g20 )
{
    assignMODE_en_20G( g20 );
}

void IPhyCcu::attachSpu( IPhySpuGp *pGp )
{
    Q_ASSERT( NULL != pGp );

    m_pSpuGp = pGp;
}
void IPhyCcu::attachScu( IPhyScuGp *pGp )
{
    Q_ASSERT( NULL != pGp );

    m_pScuGp = pGp;
}
void IPhyCcu::setCcuRun( Ccu_reg b )
{
//    //! 上电第一次进行一个复位操作
//    static bool resetFpga = true;
//    if(resetFpga)
//    {
//        LOG_FPGA_STATUS()<<"scu reset";
//        m_pSpuGp->outCTRL_spu_clr(1);
//        pulseCTRL_fsm_reset(1);
//        QThread::msleep(1);
//        resetFpga = false;
//        waitStop();
//        LOG_FPGA_STATUS()<<"scu reset";
//    }

//    LOG_FPGA_STATUS()<<"setCcu"<<b;

    //! run
    if ( b )
    {
        QThread::msleep(50);//wpu的画画时间
        //! rst state
        reg_assign_field( CTRL, cfg_fsm_run_stop_n, 0 );
        pulseCTRL_sys_stop_state( 1 );
        //! run again
        reg_assign_field( CTRL, cfg_fsm_run_stop_n, b );
    }
    //! stop
    else
    {
        reg_assign_field( CTRL, cfg_fsm_run_stop_n, b );
        flushWCache();
        waitStop();

        //! in fine trig
//        Q_ASSERT( NULL != m_pSpuGp );
//        m_pSpuGp->inFINE_TRIG_POSITION();
    }
}
Ccu_reg IPhyCcu::getCcuRun()
{
    inCTRL();

    return CTRL.scu_fsm_run;
}
Ccu_reg IPhyCcu::getCcuStop()
{
    inCTRL();

    return CTRL.sys_stop_state;
}

//not used
void IPhyCcu::setForceStop()
{
    outCTRL_fsm_reset( 1 );
    outCTRL_fsm_reset( 0 );
}

void IPhyCcu::playLast(bool playWpu, bool playTrace)
{
    //! rst state
    reg_assign_field( CTRL, cfg_fsm_run_stop_n, 0 );
    pulseCTRL_sys_stop_state( 1 );

    //! 回放的时候进行单次trace请求
    if(playTrace)
    {
        outTRACE_trace_once_req( 1 );
    }

    //!回放时如果搜索使能，单次请求search
    inMODE();
    int searchEn = 0;
    searchEn   |= getMODE_search_en_ch();
    searchEn   |= getMODE_search_en_la();
    searchEn   |= getMODE_search_en_zoom();
    if(0 != searchEn)
    {
        //qDebug()<<__FILE__<<__LINE__<<"search once req.";
        outTRACE_search_once_req( 1 );
    }

    if(playWpu)
    {

    }
    else
    {
        setTRACE_wave_bypass_wpu( 1 );
    }

    //! play
    setCTRL_mode_play_last( 1 );
    setCTRL_cfg_fsm_run_stop_n( 1 );
    flushWCache();

    assignCTRL_mode_play_last( 0 );
    flushWCache();//for ccu

    waitStop();
    if(playWpu)
    {

    }
    else
    {
        //! 进行完trace之后重新打开wpu使能
        setTRACE_wave_bypass_wpu( 0 );
    }
}

void IPhyCcu::waitStop()
{
    int tmo = 0;
    do
    {
        inCTRL();
        if( CTRL.sys_stop_state  == 1 )
        {
            break;
        }
        QThread::yieldCurrentThread();
        QThread::msleep(10);
    }while ( tmo++ < 400 );

    if(tmo >= 400)
    {
        qWarning()<<"!!!!!!!!!!!!!!!!!!!CCU wait stop fail---------------------";

        if(sysHasArg("-wait_assert"))
        {
            Q_ASSERT(false);
        }

        m_pSpuGp->outCTRL_spu_clr(1);
        pulseCTRL_fsm_reset(1);
        tmo = 0;
        do
        {
            inCTRL();
            QThread::usleep( IPhyCcu::_tickTime );
            tmo++;
        }while ( CTRL.sys_stop_state != 1 && tmo < 3);

        if(tmo == 3)
        {
            qWarning()<<"reset fpga fail";
        }
    }

    //! 解决水平档位切换的波形错误
    QThread::msleep( 1 );
}

#define TD_TICK         3200    //! 1/312.5M = 3200 ps

#define MAX_TMO         10000000000000ll
#define MIN_TMO         10000
#define trim_tmo( tmo ) if ( tmo < MIN_TMO ){ tmo = MIN_TMO; }\
                        else if ( tmo > MAX_TMO ){ tmo = MAX_TMO; }\
                        else {}
void IPhyCcu::setAUTO_TRIG_TIMEOUT( qlonglong tmo )
{
    trim_tmo( tmo );

    quint32 tick = tmo / TD_TICK;

    //! from 1
    if ( tick > 1 )
    { tick -= 1; }

    CcuMem::setAUTO_TRIG_TIMEOUT( tick );
}

void IPhyCcu::setAUTO_TRIG_HOLD_OFF( qlonglong tmo )
{
    trim_tmo( tmo );

    quint32 tick = tmo / TD_TICK;

    //! from 1
    if ( tick > 1 )
    { tick -= 1; }

    CcuMem::setAUTO_TRIG_HOLD_OFF( tick );
}

void IPhyCcu::setMeasClose()
{
    setMODE_measure_en_zoom( 0 );
    setMODE_measure_en_ch( 0 );
    setMODE_measure_en_la( 0 );
}

void IPhyCcu::setSearchClose()
{
    setMODE_search_en_zoom( 0 );
    setMODE_search_en_ch( 0 );
    setMODE_search_en_la( 0 );
    //qWarning()<<__FUNCTION__<<__LINE__<<"!!!auto close search";
}

}
