#ifndef IPHYRECPLAY_H
#define IPHYRECPLAY_H

#include "../iphy.h"

#include "../scu/iphyscu.h"
#include "../spu/iphyspu.h"
#include "../ccu/iphyccu.h"

namespace dso_phy {

class IPhyRecPlay : public IPhy
{
public:
    IPhyRecPlay();

public:
    void attachScuGp( IPhyScuGp *pScuGp );
    void attachSpuGp( IPhySpuGp *pSpuGp );
    void attachCcu( IPhyCcu *pCcu );

public:
    //! -- rec
    DsoErr setRecInterval( qint64 ps );
    DsoErr startRec();
    DsoErr stopRec();

    //! -- play
    DsoErr setPlayInterval( qint64 ps );
    DsoErr startPlay();
    DsoErr stopPlay();

    DsoErr stepPlay();

    DsoErr loadNow();

    //! -- common
    quint32 getNowIndex();
    quint64 getTag();

    bool isIdle();

    void setRange( int from, int to );
    void setNow( int now );
    void setLoop( bool bLoop );
    void setDirection( PlayDirection dir );

    void setOnDisplay(bool on);
    bool getDisplayOn();

protected:
    IPhyScuGp *m_pScuGp;
    IPhySpuGp *m_pSpuGp;
    IPhyCcu *m_pCcu;

    //! properties
    quint32 mRecInterval;
    quint32 mPlayInterval;

    quint32 mFrom, mTo, mNow;
    bool mbLoop;
    PlayDirection mPlayDir;
    qlonglong     mTag0;
    qlonglong     mTag;

protected:
    bool          m_onDisplay; //!当偏移挪出屏幕范围不显示波形。
};

}

#endif // IPHYRECPLAY_H
