#include "iphyrecplay.h"
#include "../../include/dsocfg.h"
namespace dso_phy {

#define sa_tick   3200  //! 3.2ns

#define max_interval    time_s( 13 )
#define min_interval    time_ns( 0 )

IPhyRecPlay::IPhyRecPlay()
{
    m_pScuGp = NULL;
    m_pSpuGp = NULL;
    m_pCcu = NULL;

    mRecInterval = 0;
    mPlayInterval = 0;

    mFrom = 0;
    mTo = 0;
    mNow = 0;
    mbLoop = false;

    mPlayDir = Play_Inc;

    mTag0 = 0;
    mTag  = 0;

    m_onDisplay = false;
}

void IPhyRecPlay::attachScuGp( IPhyScuGp *pScuGp )
{
    Q_ASSERT( NULL != pScuGp );

    m_pScuGp = pScuGp;
}
void IPhyRecPlay::attachSpuGp( IPhySpuGp *pSpuGp )
{
    Q_ASSERT( NULL != pSpuGp );

    m_pSpuGp = pSpuGp;
}
void IPhyRecPlay::attachCcu( IPhyCcu *pCcu )
{
    Q_ASSERT( NULL != pCcu );

    m_pCcu = pCcu;
}

DsoErr IPhyRecPlay::setRecInterval( qint64 ps )
{
    ps = qBound(min_interval, ps, max_interval);

    mRecInterval = ps / sa_tick;

    return ERR_NONE;
}
DsoErr IPhyRecPlay::startRec()
{
    Q_ASSERT( m_pScuGp != NULL );
    Q_ASSERT( m_pCcu != NULL );

    //! 0. stop
    m_pCcu->setCcuRun( 0 );

    //! 1. load index
    m_pScuGp->outREC_PLAY_index_load( 1 );
    m_pScuGp->outINDEX_CUR( mNow );
    m_pScuGp->outINDEX_BASE( mFrom );
    m_pScuGp->outINDEX_UPPER( mTo );
    m_pScuGp->outREC_PLAY_index_load( 0 );

    //! 2. to rec mode
    m_pScuGp->setREC_PLAY_loop_playback( 0 );
    m_pScuGp->setREC_PLAY_mode_rec( 1 );
    m_pScuGp->setREC_PLAY_mode_play( 0 );
    m_pScuGp->setREC_PLAY_index_inc( 1 );
    m_pScuGp->flushWCache();

    //! 4. interval
    m_pCcu->outFSM_DELAY( mRecInterval );

    //! 5. start rec
    m_pCcu->setCTRL_run_single( 0 );
    m_pCcu->setFRAME_PLOT_NUM( 1/*1000*/ );

    return ERR_NONE;
}

DsoErr IPhyRecPlay::stopRec()
{
    m_pCcu->setCcuRun( 0 );
    m_pScuGp->setREC_PLAY_mode_rec( 0 );
    m_pScuGp->setREC_PLAY_loop_playback( 0 );
    return ERR_NONE;
}

DsoErr IPhyRecPlay::setPlayInterval( qint64 ps )
{
    ps = qBound(min_interval, ps, max_interval);
    mPlayInterval = ps / sa_tick;

    qDebug()<<__FILE__<<__LINE__<<"mPlayInterval"<<mPlayInterval;
    m_pCcu->setFSM_DELAY( mPlayInterval );

    return ERR_NONE;
}
DsoErr IPhyRecPlay::startPlay()
{
    Q_ASSERT( m_pScuGp != NULL );
    Q_ASSERT( m_pCcu != NULL );

    //! 0. stop
    m_pCcu->setCcuRun( 0 ); 

    //! 1. index
    m_pScuGp->outREC_PLAY_index_load( 1 );
    m_pScuGp->outINDEX_CUR( mNow );
    m_pScuGp->outINDEX_BASE( mFrom );
    m_pScuGp->outINDEX_UPPER( mTo );
    m_pScuGp->outREC_PLAY_index_load( 0 );

    //qDebug()<<" mNow:"<<mNow<<" mFrom:"<<mFrom<<" mTo:"<<mTo<<" mPlayDir:"<<mPlayDir<<" mbLoop:"<<mbLoop;

    //! 2. to play mode
    m_pScuGp->setREC_PLAY_mode_rec( 0 );
    m_pScuGp->setREC_PLAY_mode_play( 1 );
    m_pScuGp->setREC_PLAY_loop_playback( mbLoop );
    m_pScuGp->setREC_PLAY_index_inc( mPlayDir == Play_Inc ? 1 : 0 );
    m_pScuGp->flushWCache();

    //! 4. interval
    m_pCcu->outFSM_DELAY( mPlayInterval );

    //! 5. start play
    m_pCcu->setFRAME_PLOT_NUM( 1 ); //!TX_len*chCount*NUM <= 4MB;

    m_pCcu->setCcuRun( 1 );

    return ERR_NONE;
}

DsoErr IPhyRecPlay::stopPlay()
{
    m_pCcu->setCcuRun( 0 );
    m_pScuGp->setREC_PLAY_mode_play( 0 );
    m_pScuGp->setREC_PLAY_loop_playback( 0 );
    m_pScuGp->flushWCache();
    return ERR_NONE;
}

DsoErr IPhyRecPlay::stepPlay()
{
    m_pCcu->outTRACE_trace_once_req( 1 );
    //! 1. RUN
    m_pCcu->setCcuRun( 1 );
    m_pCcu->flushWCache();

    //stop
    //m_pCcu->setCcuRun( 0 );
    m_pCcu->waitStop();

    //! 2. 读取时间戳
    getTag();

    //! 3. 退出回放模式，
    m_pScuGp->outREC_PLAY_mode_play( 0 );


    return ERR_NONE;
}

DsoErr IPhyRecPlay::loadNow()
{
    Q_ASSERT( m_pScuGp != NULL );
    Q_ASSERT( m_pCcu != NULL );

    //! 0. stop
    m_pCcu->setCcuRun( 0 );

    //! 1. load index
    m_pScuGp->outREC_PLAY_index_load( 1 );
    m_pScuGp->outINDEX_CUR( mNow );
    m_pScuGp->outINDEX_BASE( mNow );
    m_pScuGp->outINDEX_UPPER( mNow);
    m_pScuGp->outREC_PLAY_index_load( 0 );

    //! 2. play mode
    m_pScuGp->setREC_PLAY_mode_rec( 0 );
    m_pScuGp->setREC_PLAY_mode_play( 1 );
    m_pScuGp->setREC_PLAY_index_inc( 1 );
    m_pScuGp->flushWCache();

    //! 4. interval
    m_pCcu->outFSM_DELAY( 0 );
    m_pCcu->setFRAME_PLOT_NUM( 1 );
    m_pCcu->flushWCache();

    return ERR_NONE;
}

quint32 IPhyRecPlay::getNowIndex()
{
    m_pScuGp->inINDEX_CUR();
    mNow = m_pScuGp->getINDEX_CUR_index();

    m_pScuGp->inWAVE_INFO();
    int vld   = m_pScuGp->getWAVE_INFO_wave_play_vld();
    int index = mNow + 1;

    /* qDebug()<<__FILE__<<__FUNCTION__
           <<"valid:"<<vld<<"index:"<<index;*/

    return (vld == 1)? index : 0;
}

quint64 IPhyRecPlay::getTag()
{
    m_pSpuGp->inTIME_TAG_CTRL();
    if ( 0 !=  m_pSpuGp->getTIME_TAG_CTRL_rec_first_done() )
    {
        quint64 tagL = m_pSpuGp->inTIME_TAG_L_REC_FIRST();
        quint64 tagH = m_pSpuGp->inTIME_TAG_H_REC_FIRST();
        mTag0 = (tagH<<32) + tagL;
        //qDebug()<<__FILE__<<__LINE__<<"------: tag0 = "<<mTag0;
    }

    m_pScuGp->inREC_PLAY();
    //!(必须在回放模式或者录制模式读取)
    if ( (0 !=  m_pScuGp->getREC_PLAY_mode_play()) ||
         (0 !=  m_pScuGp->getREC_PLAY_mode_rec())  )
    {
        m_pSpuGp->outTIME_TAG_CTRL_rd_en(0);
        m_pSpuGp->outTIME_TAG_CTRL_rd_en(1);

        quint64 tagL = m_pSpuGp->inTIME_TAG_L();
        quint64 tagH = m_pSpuGp->inTIME_TAG_H();
        mTag = (tagH<<32) + tagL;

        qDebug()<<__FILE__<<__LINE__<<"------: mtag = "<<mTag
               <<"tag0 = "<<mTag0<<"dtag = "<<mTag - mTag0<<"mNow = "<<mNow;

        mTag = mTag - mTag0;
    }
    else
    {
        /*qDebug()<<__FILE__<<__LINE__<<"warning: tag = "<<mTag
                <<"mode_play = "<<m_pScuGp->getREC_PLAY_mode_play()
                <<"mode_rec  = "<<m_pScuGp->getREC_PLAY_mode_rec();*/
    }

    return mTag;
}

bool IPhyRecPlay::isIdle()
{
    return m_pCcu->getCcuStop() == 1;
}

void IPhyRecPlay::setRange( int from, int to )
{
    mFrom = from;
    mTo = to;
}
void IPhyRecPlay::setNow( int now )
{
    mNow = now;
}
void IPhyRecPlay::setLoop( bool bLoop )
{
    mbLoop = bLoop;
}
void IPhyRecPlay::setDirection( PlayDirection dir )
{
    mPlayDir = dir;
}

void IPhyRecPlay::setOnDisplay(bool on)
{
    m_onDisplay = on;
}

bool IPhyRecPlay::getDisplayOn()
{
    return m_onDisplay;
}

}

