#ifndef IPHYCOUNTER_H
#define IPHYCOUNTER_H

#include "../iphyfpga.h"

namespace dso_phy {


#include "../ic/frequ.h"

//! 一些编译器中不支持类中的常量计算
#define tick_time (1.0/2.5e9)

class IPhyFrequ : public FrequMem
{
public:
    enum eCounterSrc
    {
        counter_ext = 0,
        counter_ch1 = 1,
        counter_ch2 = 2,
        counter_ch3 = 3,
        counter_ch4 = 4,

        counter_la0 = 16,
        counter_la1,counter_la2,counter_la3,
        counter_la4,counter_la5,counter_la6,counter_la7,
        counter_la8,counter_la9,counter_la10,counter_la11,
        counter_la12,counter_la13,counter_la14,counter_la15,
    };

    enum eCounterGateTime
    {
        counter_gate_200ms = 0,
        counter_gate_1s,
        counter_gate_50ms,
        counter_gate_10ms
    };

    enum eDvmSrc
    {
        dvm_ch1 = 0,
        dvm_ch2,
        dvm_ch3,
        dvm_ch4,
    };

    enum eDvmGate
    {
        dvm_time_0 = 0,     //! 107ms
        dvm_time_1,         //! 3.43s
        dvm_time_2,         //! 0.85s
        dvm_time_3,         //! 13.4ms
    };

public:
    IPhyFrequ();

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();    

    void waitGated();

    void cacheIn();

public:
    double gateTime( int gate );
    quint32 gateTimeIndex( int gateTime );  //! us

    quint32 dvmTimeIndex( int gateTime );   //! us
public:
    double getFreq();
    double getPeri();
};

}

#endif // IPHYCOUNTER_H
