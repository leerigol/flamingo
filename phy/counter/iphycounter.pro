TEMPLATE = lib

TARGET = ../../lib$$(PLATFORM)/phy/iphycounter
CONFIG += static


#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

HEADERS += \
    iphycounter.h 

SOURCES += \
    iphycounter.cpp

