#include "iphycounter.h"

namespace dso_phy {

#include "../ic/frequ.cpp"

//! us
static int _gateCounterTimeus[]=
{
    200000,1000000,50000,10000,
};

IPhyFrequ::IPhyFrequ()
{

}

void IPhyFrequ::init()
{}

void IPhyFrequ::loadOtp()
{
    _loadOtp();
}
void IPhyFrequ::reMap()
{
    _remap_Frequ_();
}

//! wait frequ stable
void IPhyFrequ::waitGated()
{
    QThread::usleep(  _gateCounterTimeus[freq_cfg_1.gate_time] );
}
void IPhyFrequ::cacheIn()
{
    instart_time();
    inend_time();
    incounter();
    insignal_wide_l();
    insignal_wide_h();
    insignal_event_l();
    insignal_event_h();
}

double IPhyFrequ::gateTime( int gateTimeIndex )
{
    Q_ASSERT( gateTimeIndex >= 0 && gateTimeIndex < array_count(_gateCounterTimeus) );

    return _gateCounterTimeus[ gateTimeIndex ]*1.0e-6;
}

//! us
quint32 IPhyFrequ::gateTimeIndex( int gateTime )
{
    foreach_array( i, _gateCounterTimeus )
    {
        if ( gateTime == _gateCounterTimeus[i] )
        { return i;}
    }
    Q_ASSERT( false );
    return 0;
}

//! us
quint32 IPhyFrequ::dvmTimeIndex( int gateTime )
{
    static int _gateDvmTimeus[]=
    {
        107000,3430000,850000,13400,
    };

    foreach_array( i, _gateDvmTimeus )
    {
        if ( gateTime == _gateDvmTimeus[i] )
        { return i;}
    }
    Q_ASSERT( false );
    return 0;
}

double IPhyFrequ::getFreq()
{
    cacheIn();

    double gTime;
    gTime = gateTime( freq_cfg_1.gate_time );

    double padTime = ( start_time + end_time )*tick_time;
    double sigTime = gTime - padTime;

    if ( counter >= 1 && sigTime != 0)
    {
        return ( counter - 1 )/sigTime;
    }
    else
    { return 0.0; }
}

double IPhyFrequ::getPeri()
{
    cacheIn();

    //! freq
    if ( freq_cfg_1.sel_mode == 0 )
    {
        double gTime;
        gTime = gateTime( freq_cfg_1.gate_time );
        double padTime = ( start_time + end_time )*tick_time;

        double sigTime = gTime - padTime;

        LOG_DBG()<<"sigTime:"<<sigTime<<"gTime:"<<gTime<<"padTime:"<<padTime;
        LOG_DBG()<<"start_time:"<<start_time<<"end_time:"<<end_time<<"counter:"<<counter;

        if ( counter > 1 )
        {
            return sigTime/( counter - 1 );
        }
        else
        { return 0.0; }
    }
    else
    {
        quint64 width = pack_uint64( signal_wide_h, signal_wide_l );

        LOG_DBG()<<"signal_wide_h:"<<signal_wide_h<<"signal_wide_l:"<<signal_wide_l;

        if ( width < 1 ) return 0.0;

        return width * tick_time;
    }
}

}

