#ifndef IPHYPLL_H
#define IPHYPLL_H

#include "iphy.h"
#include "./iphydac.h"

namespace dso_phy {

typedef struct _pllCfg
{
    unsigned char regAddr;
    unsigned int  regData;
}pllCfg;

class IPhyPLL : public IPhy
{
public:
    IPhyPLL();

public:
    virtual void init();

public:
    void output5G();
    void output1_1G( bool bOnOff );

    void attachDac( IPhyDac *pDac, int dacChan );
    void tuneClock( int clock );

    void write5g( quint16 to, quint16 data );
    void write1_1g( quint16 to, quint16 data );

private:
    IPhyDac *m_pDac;
    int mDacChan;
};

}

#endif // IPHYPLL_H
