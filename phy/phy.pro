TEMPLATE = subdirs

SUBDIRS += ./iphy.pro
SUBDIRS += ./hori/iphyhori.pro
SUBDIRS += ./ch/iphych.pro
SUBDIRS += ./trig/iphytrig.pro
SUBDIRS += ./search/iphysearch.pro

SUBDIRS += ./dg/iphydg.pro
SUBDIRS += ./relay/relay.pro

SUBDIRS += ./meas/iphymeas.pro
SUBDIRS += ./counter/iphycounter.pro

SUBDIRS += ./mask/iphymask.pro
SUBDIRS += ./1wire/iphy1wire.pro
SUBDIRS += ./k7mfcu/iphyk7mfcu.pro

SUBDIRS += ./zynqfcu/iphyzynqfcu.pro
SUBDIRS += ./trace/iphytrace.pro

SUBDIRS += ./gtu/iphygtu.pro
SUBDIRS += ./spu/iphyspu.pro
SUBDIRS += ./scu/iphyscu.pro
SUBDIRS += ./ccu/iphyccu.pro
SUBDIRS += ./wpu/iphywpu.pro

SUBDIRS += ./recplay/iphyrecplay.pro

SUBDIRS += ./adc/iphyadc.pro
