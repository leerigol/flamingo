#ifndef IPHYFAN_H
#define IPHYFAN_H

#include "iphy.h"

namespace dso_phy {

class IPhyFan : public IPhy
{
public:
    IPhyFan();
public:
    virtual void init();
public:
    void setSpeed( int speed );
};

}

#endif // IPHYFAN_H
