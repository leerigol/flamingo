#ifndef _ZYNQFCUWR_IC_H_
#define _ZYNQFCUWR_IC_H_

#define ZynqFcuWr_reg unsigned int

union ZynqFcuWr_trace_req{
	struct{
	ZynqFcuWr_reg trace_req:1;
	ZynqFcuWr_reg trace_datype:4;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_roll_trace_waddr_rst{
	struct{
	ZynqFcuWr_reg roll_trace_waddr_rst_ctl:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_search_result_wdone_clr{
	struct{
	ZynqFcuWr_reg search_result_wdone_clr:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_decode_a_or_b_channel_cmp{
	struct{
	ZynqFcuWr_reg cmp_a_level_h:8;
	ZynqFcuWr_reg cmp_a_level_l:8;
	ZynqFcuWr_reg cmp_b_level_h:8;
	ZynqFcuWr_reg cmp_b_level_l:8;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_decode_c_or_d_channel_cmp{
	struct{
	ZynqFcuWr_reg cmp_c_level_h:8;
	ZynqFcuWr_reg cmp_c_level_l:8;
	ZynqFcuWr_reg cmp_d_level_h:8;
	ZynqFcuWr_reg cmp_d_level_l:8;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_deinterweave_property_cfg1{
	struct{
	ZynqFcuWr_reg deinterweave_source_type:1;
	ZynqFcuWr_reg decode_en:1;
	ZynqFcuWr_reg decode_en_sec:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_deinterweave_property_cfg2{
	struct{
	ZynqFcuWr_reg deinterweave_rstart:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_deinterweave_property_cfg3{
	struct{
	ZynqFcuWr_reg deinterweave_done_clr:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_deinterweave_property_cfg4{
	struct{
	ZynqFcuWr_reg channel_mode:3;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_fpga_rdat_cfg1{
	struct{
	ZynqFcuWr_reg fpga_rdat_num:24;
	ZynqFcuWr_reg fpga_rdat_type:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_fpga_rdat_cfg2{
	struct{
	ZynqFcuWr_reg fpga_rdat_start:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_fpga_rdat_cfg3{
	struct{
	ZynqFcuWr_reg fpga_rdat_done_clr:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_fpga_rdat_cfg4{
	struct{
	ZynqFcuWr_reg exdat_rbaseaddr:32;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_compress_type{
	struct{
	ZynqFcuWr_reg compress_type:1;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_decode_a_or_b_channel_cmp_sec{
	struct{
	ZynqFcuWr_reg cmp_a_level_h_sec:8;
	ZynqFcuWr_reg cmp_a_level_l_sec:8;
	ZynqFcuWr_reg cmp_b_level_h_sec:8;
	ZynqFcuWr_reg cmp_b_level_l_sec:8;
	};
	ZynqFcuWr_reg payload;
};

union ZynqFcuWr_decode_c_or_d_channel_cmp_sec{
	struct{
	ZynqFcuWr_reg cmp_c_level_h_sec:8;
	ZynqFcuWr_reg cmp_c_level_l_sec:8;
	ZynqFcuWr_reg cmp_d_level_h_sec:8;
	ZynqFcuWr_reg cmp_d_level_l_sec:8;
	};
	ZynqFcuWr_reg payload;
};

struct ZynqFcuWrMemProxy {
	ZynqFcuWr_trace_req trace_req; 
	ZynqFcuWr_reg eye_trace_wbaseaddr;
	ZynqFcuWr_reg roll_ana_trace_wbaseaddr;
	ZynqFcuWr_reg roll_la_trace_wbaseaddr;

	ZynqFcuWr_reg roll_ana_trace_waddr_end;
	ZynqFcuWr_reg roll_la_trace_waddr_end;
	ZynqFcuWr_roll_trace_waddr_rst roll_trace_waddr_rst; 
	ZynqFcuWr_reg search_result_wbaseaddr;

	ZynqFcuWr_search_result_wdone_clr search_result_wdone_clr; 
	ZynqFcuWr_reg ana_main_trace_wbaseaddr;
	ZynqFcuWr_reg la_main_trace_wbaseaddr;
	ZynqFcuWr_reg ana_zoom_trace_wbaseaddr;

	ZynqFcuWr_reg la_zoom_trace_wbaseaddr;
	ZynqFcuWr_reg reserved1;
	ZynqFcuWr_reg comm_test;
	ZynqFcuWr_reg trace_wdone_clr;

	ZynqFcuWr_reg trace_wbaseaddr;
	ZynqFcuWr_reg trace_dat_len;
	ZynqFcuWr_reg deinterweave_chx_lax_rdat_num;
	ZynqFcuWr_reg deinterweave_chx_raddr;

	ZynqFcuWr_reg deinterweave_cha_waddr;
	ZynqFcuWr_reg deinterweave_chb_waddr;
	ZynqFcuWr_reg deinterweave_chc_waddr;
	ZynqFcuWr_reg deinterweave_chd_waddr;

	ZynqFcuWr_reg deinterweave_decode_cha_waddr;
	ZynqFcuWr_reg deinterweave_decode_chb_waddr;
	ZynqFcuWr_reg deinterweave_decode_chc_waddr;
	ZynqFcuWr_reg deinterweave_decode_chd_waddr;

	ZynqFcuWr_reg deinterweave_lax_raddr;
	ZynqFcuWr_reg deinterweave_la0_waddr;
	ZynqFcuWr_reg deinterweave_la1_waddr;
	ZynqFcuWr_reg deinterweave_la2_waddr;

	ZynqFcuWr_reg deinterweave_la3_waddr;
	ZynqFcuWr_reg deinterweave_la4_waddr;
	ZynqFcuWr_reg deinterweave_la5_waddr;
	ZynqFcuWr_reg deinterweave_la6_waddr;

	ZynqFcuWr_reg deinterweave_la7_waddr;
	ZynqFcuWr_reg deinterweave_la8_waddr;
	ZynqFcuWr_reg deinterweave_la9_waddr;
	ZynqFcuWr_reg deinterweave_la10_waddr;

	ZynqFcuWr_reg deinterweave_la11_waddr;
	ZynqFcuWr_reg deinterweave_la12_waddr;
	ZynqFcuWr_reg deinterweave_la13_waddr;
	ZynqFcuWr_reg deinterweave_la14_waddr;

	ZynqFcuWr_reg deinterweave_la15_waddr;
	ZynqFcuWr_decode_a_or_b_channel_cmp decode_a_or_b_channel_cmp; 
	ZynqFcuWr_decode_c_or_d_channel_cmp decode_c_or_d_channel_cmp; 
	ZynqFcuWr_deinterweave_property_cfg1 deinterweave_property_cfg1; 

	ZynqFcuWr_deinterweave_property_cfg2 deinterweave_property_cfg2; 
	ZynqFcuWr_deinterweave_property_cfg3 deinterweave_property_cfg3; 
	ZynqFcuWr_deinterweave_property_cfg4 deinterweave_property_cfg4; 
	ZynqFcuWr_fpga_rdat_cfg1 fpga_rdat_cfg1; 

	ZynqFcuWr_fpga_rdat_cfg2 fpga_rdat_cfg2; 
	ZynqFcuWr_fpga_rdat_cfg3 fpga_rdat_cfg3; 
	ZynqFcuWr_fpga_rdat_cfg4 fpga_rdat_cfg4; 
	ZynqFcuWr_reg reserved6;

	ZynqFcuWr_reg reserved7;
	ZynqFcuWr_reg reserved8;
	ZynqFcuWr_reg reserved9;
	ZynqFcuWr_reg reserved10;

	ZynqFcuWr_reg reserved11;
	ZynqFcuWr_compress_type compress_type; 
	ZynqFcuWr_decode_a_or_b_channel_cmp_sec decode_a_or_b_channel_cmp_sec; 
	ZynqFcuWr_decode_c_or_d_channel_cmp_sec decode_c_or_d_channel_cmp_sec; 

	ZynqFcuWr_reg deinterweave_decode_cha_waddr_sec;
	ZynqFcuWr_reg deinterweave_decode_chb_waddr_sec;
	ZynqFcuWr_reg deinterweave_decode_chc_waddr_sec;
	ZynqFcuWr_reg deinterweave_decode_chd_waddr_sec;

	ZynqFcuWr_reg deinterweave_chx_raddr_end;
	ZynqFcuWr_reg deinterweave_lax_raddr_end;
	ZynqFcuWr_reg deinterweave_chx_raddr_base;
	ZynqFcuWr_reg deinterweave_lax_raddr_base;

	ZynqFcuWr_reg dbg_cfg1;
	void assigntrace_req_trace_req( ZynqFcuWr_reg value  );
	void assigntrace_req_trace_datype( ZynqFcuWr_reg value  );

	void assigntrace_req( ZynqFcuWr_reg value );


	void assigneye_trace_wbaseaddr( ZynqFcuWr_reg value );

	void assignroll_ana_trace_wbaseaddr( ZynqFcuWr_reg value );

	void assignroll_la_trace_wbaseaddr( ZynqFcuWr_reg value );

	void assignroll_ana_trace_waddr_end( ZynqFcuWr_reg value );

	void assignroll_la_trace_waddr_end( ZynqFcuWr_reg value );

	void assignroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg value  );

	void assignroll_trace_waddr_rst( ZynqFcuWr_reg value );


	void assignsearch_result_wbaseaddr( ZynqFcuWr_reg value );

	void assignsearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg value  );

	void assignsearch_result_wdone_clr( ZynqFcuWr_reg value );


	void assignana_main_trace_wbaseaddr( ZynqFcuWr_reg value );

	void assignla_main_trace_wbaseaddr( ZynqFcuWr_reg value );

	void assignana_zoom_trace_wbaseaddr( ZynqFcuWr_reg value );

	void assignla_zoom_trace_wbaseaddr( ZynqFcuWr_reg value );

	void assignreserved1( ZynqFcuWr_reg value );

	void assigncomm_test( ZynqFcuWr_reg value );

	void assigntrace_wdone_clr( ZynqFcuWr_reg value );

	void assigntrace_wbaseaddr( ZynqFcuWr_reg value );

	void assigntrace_dat_len( ZynqFcuWr_reg value );

	void assigndeinterweave_chx_lax_rdat_num( ZynqFcuWr_reg value );

	void assigndeinterweave_chx_raddr( ZynqFcuWr_reg value );

	void assigndeinterweave_cha_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_chb_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_chc_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_chd_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_decode_cha_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_decode_chb_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_decode_chc_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_decode_chd_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_lax_raddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la0_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la1_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la2_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la3_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la4_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la5_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la6_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la7_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la8_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la9_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la10_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la11_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la12_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la13_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la14_waddr( ZynqFcuWr_reg value );

	void assigndeinterweave_la15_waddr( ZynqFcuWr_reg value );

	void assigndecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg value  );
	void assigndecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg value  );
	void assigndecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg value  );
	void assigndecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg value  );

	void assigndecode_a_or_b_channel_cmp( ZynqFcuWr_reg value );


	void assigndecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg value  );
	void assigndecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg value  );
	void assigndecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg value  );
	void assigndecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg value  );

	void assigndecode_c_or_d_channel_cmp( ZynqFcuWr_reg value );


	void assigndeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg value  );
	void assigndeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg value  );
	void assigndeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg value  );

	void assigndeinterweave_property_cfg1( ZynqFcuWr_reg value );


	void assigndeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg value  );

	void assigndeinterweave_property_cfg2( ZynqFcuWr_reg value );


	void assigndeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg value  );

	void assigndeinterweave_property_cfg3( ZynqFcuWr_reg value );


	void assigndeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg value  );

	void assigndeinterweave_property_cfg4( ZynqFcuWr_reg value );


	void assignfpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg value  );
	void assignfpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg value  );

	void assignfpga_rdat_cfg1( ZynqFcuWr_reg value );


	void assignfpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg value  );

	void assignfpga_rdat_cfg2( ZynqFcuWr_reg value );


	void assignfpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg value  );

	void assignfpga_rdat_cfg3( ZynqFcuWr_reg value );


	void assignfpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg value  );

	void assignfpga_rdat_cfg4( ZynqFcuWr_reg value );


	void assignreserved6( ZynqFcuWr_reg value );

	ZynqFcuWr_reg getreserved6(  );

	void assignreserved7( ZynqFcuWr_reg value );

	ZynqFcuWr_reg getreserved7(  );

	void assignreserved8( ZynqFcuWr_reg value );

	ZynqFcuWr_reg getreserved8(  );

	void assignreserved9( ZynqFcuWr_reg value );

	ZynqFcuWr_reg getreserved9(  );

	void assignreserved10( ZynqFcuWr_reg value );

	ZynqFcuWr_reg getreserved10(  );

	void assignreserved11( ZynqFcuWr_reg value );

	ZynqFcuWr_reg getreserved11(  );

	void assigncompress_type_compress_type( ZynqFcuWr_reg value  );

	void assigncompress_type( ZynqFcuWr_reg value );


	void assigndecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg value  );
	void assigndecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg value  );
	void assigndecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg value  );
	void assigndecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg value  );

	void assigndecode_a_or_b_channel_cmp_sec( ZynqFcuWr_reg value );


	void assigndecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg value  );
	void assigndecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg value  );
	void assigndecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg value  );
	void assigndecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg value  );

	void assigndecode_c_or_d_channel_cmp_sec( ZynqFcuWr_reg value );


	void assigndeinterweave_decode_cha_waddr_sec( ZynqFcuWr_reg value );

	void assigndeinterweave_decode_chb_waddr_sec( ZynqFcuWr_reg value );

	void assigndeinterweave_decode_chc_waddr_sec( ZynqFcuWr_reg value );

	void assigndeinterweave_decode_chd_waddr_sec( ZynqFcuWr_reg value );

	void assigndeinterweave_chx_raddr_end( ZynqFcuWr_reg value );

	void assigndeinterweave_lax_raddr_end( ZynqFcuWr_reg value );

	void assigndeinterweave_chx_raddr_base( ZynqFcuWr_reg value );

	void assigndeinterweave_lax_raddr_base( ZynqFcuWr_reg value );

	void assigndbg_cfg1( ZynqFcuWr_reg value );

};
struct ZynqFcuWrMem : public ZynqFcuWrMemProxy, public IPhyFpga{
protected:
	ZynqFcuWr_reg mAddrBase;
public:
	ZynqFcuWrMem( ZynqFcuWr_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( ZynqFcuWr_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( ZynqFcuWrMemProxy *proxy);
	void pull( ZynqFcuWrMemProxy *proxy);

public:
	void settrace_req_trace_req( ZynqFcuWr_reg value );
	void settrace_req_trace_datype( ZynqFcuWr_reg value );

	void settrace_req( ZynqFcuWr_reg value );
	void outtrace_req_trace_req( ZynqFcuWr_reg value );
	void pulsetrace_req_trace_req( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outtrace_req_trace_datype( ZynqFcuWr_reg value );
	void pulsetrace_req_trace_datype( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outtrace_req( ZynqFcuWr_reg value );
	void outtrace_req(  );


	void seteye_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outeye_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outeye_trace_wbaseaddr(  );

	void setroll_ana_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outroll_ana_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outroll_ana_trace_wbaseaddr(  );

	void setroll_la_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outroll_la_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outroll_la_trace_wbaseaddr(  );

	void setroll_ana_trace_waddr_end( ZynqFcuWr_reg value );
	void outroll_ana_trace_waddr_end( ZynqFcuWr_reg value );
	void outroll_ana_trace_waddr_end(  );

	void setroll_la_trace_waddr_end( ZynqFcuWr_reg value );
	void outroll_la_trace_waddr_end( ZynqFcuWr_reg value );
	void outroll_la_trace_waddr_end(  );

	void setroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg value );

	void setroll_trace_waddr_rst( ZynqFcuWr_reg value );
	void outroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg value );
	void pulseroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outroll_trace_waddr_rst( ZynqFcuWr_reg value );
	void outroll_trace_waddr_rst(  );


	void setsearch_result_wbaseaddr( ZynqFcuWr_reg value );
	void outsearch_result_wbaseaddr( ZynqFcuWr_reg value );
	void outsearch_result_wbaseaddr(  );

	void setsearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg value );

	void setsearch_result_wdone_clr( ZynqFcuWr_reg value );
	void outsearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg value );
	void pulsesearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outsearch_result_wdone_clr( ZynqFcuWr_reg value );
	void outsearch_result_wdone_clr(  );


	void setana_main_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outana_main_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outana_main_trace_wbaseaddr(  );

	void setla_main_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outla_main_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outla_main_trace_wbaseaddr(  );

	void setana_zoom_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outana_zoom_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outana_zoom_trace_wbaseaddr(  );

	void setla_zoom_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outla_zoom_trace_wbaseaddr( ZynqFcuWr_reg value );
	void outla_zoom_trace_wbaseaddr(  );

	void setreserved1( ZynqFcuWr_reg value );
	void outreserved1( ZynqFcuWr_reg value );
	void outreserved1(  );

	void setcomm_test( ZynqFcuWr_reg value );
	void outcomm_test( ZynqFcuWr_reg value );
	void outcomm_test(  );

	void settrace_wdone_clr( ZynqFcuWr_reg value );
	void outtrace_wdone_clr( ZynqFcuWr_reg value );
	void outtrace_wdone_clr(  );

	void settrace_wbaseaddr( ZynqFcuWr_reg value );
	void outtrace_wbaseaddr( ZynqFcuWr_reg value );
	void outtrace_wbaseaddr(  );

	void settrace_dat_len( ZynqFcuWr_reg value );
	void outtrace_dat_len( ZynqFcuWr_reg value );
	void outtrace_dat_len(  );

	void setdeinterweave_chx_lax_rdat_num( ZynqFcuWr_reg value );
	void outdeinterweave_chx_lax_rdat_num( ZynqFcuWr_reg value );
	void outdeinterweave_chx_lax_rdat_num(  );

	void setdeinterweave_chx_raddr( ZynqFcuWr_reg value );
	void outdeinterweave_chx_raddr( ZynqFcuWr_reg value );
	void outdeinterweave_chx_raddr(  );

	void setdeinterweave_cha_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_cha_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_cha_waddr(  );

	void setdeinterweave_chb_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_chb_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_chb_waddr(  );

	void setdeinterweave_chc_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_chc_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_chc_waddr(  );

	void setdeinterweave_chd_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_chd_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_chd_waddr(  );

	void setdeinterweave_decode_cha_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_cha_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_cha_waddr(  );

	void setdeinterweave_decode_chb_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chb_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chb_waddr(  );

	void setdeinterweave_decode_chc_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chc_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chc_waddr(  );

	void setdeinterweave_decode_chd_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chd_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chd_waddr(  );

	void setdeinterweave_lax_raddr( ZynqFcuWr_reg value );
	void outdeinterweave_lax_raddr( ZynqFcuWr_reg value );
	void outdeinterweave_lax_raddr(  );

	void setdeinterweave_la0_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la0_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la0_waddr(  );

	void setdeinterweave_la1_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la1_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la1_waddr(  );

	void setdeinterweave_la2_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la2_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la2_waddr(  );

	void setdeinterweave_la3_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la3_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la3_waddr(  );

	void setdeinterweave_la4_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la4_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la4_waddr(  );

	void setdeinterweave_la5_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la5_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la5_waddr(  );

	void setdeinterweave_la6_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la6_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la6_waddr(  );

	void setdeinterweave_la7_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la7_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la7_waddr(  );

	void setdeinterweave_la8_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la8_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la8_waddr(  );

	void setdeinterweave_la9_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la9_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la9_waddr(  );

	void setdeinterweave_la10_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la10_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la10_waddr(  );

	void setdeinterweave_la11_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la11_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la11_waddr(  );

	void setdeinterweave_la12_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la12_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la12_waddr(  );

	void setdeinterweave_la13_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la13_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la13_waddr(  );

	void setdeinterweave_la14_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la14_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la14_waddr(  );

	void setdeinterweave_la15_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la15_waddr( ZynqFcuWr_reg value );
	void outdeinterweave_la15_waddr(  );

	void setdecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg value );
	void setdecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg value );
	void setdecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg value );
	void setdecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg value );

	void setdecode_a_or_b_channel_cmp( ZynqFcuWr_reg value );
	void outdecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdecode_a_or_b_channel_cmp( ZynqFcuWr_reg value );
	void outdecode_a_or_b_channel_cmp(  );


	void setdecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg value );
	void setdecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg value );
	void setdecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg value );
	void setdecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg value );

	void setdecode_c_or_d_channel_cmp( ZynqFcuWr_reg value );
	void outdecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdecode_c_or_d_channel_cmp( ZynqFcuWr_reg value );
	void outdecode_c_or_d_channel_cmp(  );


	void setdeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg value );
	void setdeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg value );
	void setdeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg value );

	void setdeinterweave_property_cfg1( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg value );
	void pulsedeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg value );
	void pulsedeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg value );
	void pulsedeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdeinterweave_property_cfg1( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg1(  );


	void setdeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg value );

	void setdeinterweave_property_cfg2( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg value );
	void pulsedeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdeinterweave_property_cfg2( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg2(  );


	void setdeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg value );

	void setdeinterweave_property_cfg3( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg value );
	void pulsedeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdeinterweave_property_cfg3( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg3(  );


	void setdeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg value );

	void setdeinterweave_property_cfg4( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg value );
	void pulsedeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdeinterweave_property_cfg4( ZynqFcuWr_reg value );
	void outdeinterweave_property_cfg4(  );


	void setfpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg value );
	void setfpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg value );

	void setfpga_rdat_cfg1( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg value );
	void pulsefpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outfpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg value );
	void pulsefpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outfpga_rdat_cfg1( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg1(  );


	void setfpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg value );

	void setfpga_rdat_cfg2( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg value );
	void pulsefpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outfpga_rdat_cfg2( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg2(  );


	void setfpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg value );

	void setfpga_rdat_cfg3( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg value );
	void pulsefpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outfpga_rdat_cfg3( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg3(  );


	void setfpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg value );

	void setfpga_rdat_cfg4( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg value );
	void pulsefpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outfpga_rdat_cfg4( ZynqFcuWr_reg value );
	void outfpga_rdat_cfg4(  );


	void setreserved6( ZynqFcuWr_reg value );
	void outreserved6( ZynqFcuWr_reg value );
	void outreserved6(  );

	ZynqFcuWr_reg inreserved6(  );

	void setreserved7( ZynqFcuWr_reg value );
	void outreserved7( ZynqFcuWr_reg value );
	void outreserved7(  );

	ZynqFcuWr_reg inreserved7(  );

	void setreserved8( ZynqFcuWr_reg value );
	void outreserved8( ZynqFcuWr_reg value );
	void outreserved8(  );

	ZynqFcuWr_reg inreserved8(  );

	void setreserved9( ZynqFcuWr_reg value );
	void outreserved9( ZynqFcuWr_reg value );
	void outreserved9(  );

	ZynqFcuWr_reg inreserved9(  );

	void setreserved10( ZynqFcuWr_reg value );
	void outreserved10( ZynqFcuWr_reg value );
	void outreserved10(  );

	ZynqFcuWr_reg inreserved10(  );

	void setreserved11( ZynqFcuWr_reg value );
	void outreserved11( ZynqFcuWr_reg value );
	void outreserved11(  );

	ZynqFcuWr_reg inreserved11(  );

	void setcompress_type_compress_type( ZynqFcuWr_reg value );

	void setcompress_type( ZynqFcuWr_reg value );
	void outcompress_type_compress_type( ZynqFcuWr_reg value );
	void pulsecompress_type_compress_type( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outcompress_type( ZynqFcuWr_reg value );
	void outcompress_type(  );


	void setdecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg value );
	void setdecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg value );
	void setdecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg value );
	void setdecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg value );

	void setdecode_a_or_b_channel_cmp_sec( ZynqFcuWr_reg value );
	void outdecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg value );
	void pulsedecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdecode_a_or_b_channel_cmp_sec( ZynqFcuWr_reg value );
	void outdecode_a_or_b_channel_cmp_sec(  );


	void setdecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg value );
	void setdecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg value );
	void setdecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg value );
	void setdecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg value );

	void setdecode_c_or_d_channel_cmp_sec( ZynqFcuWr_reg value );
	void outdecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );
	void outdecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg value );
	void pulsedecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV=0, int timeus=0 );

	void outdecode_c_or_d_channel_cmp_sec( ZynqFcuWr_reg value );
	void outdecode_c_or_d_channel_cmp_sec(  );


	void setdeinterweave_decode_cha_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_cha_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_cha_waddr_sec(  );

	void setdeinterweave_decode_chb_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chb_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chb_waddr_sec(  );

	void setdeinterweave_decode_chc_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chc_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chc_waddr_sec(  );

	void setdeinterweave_decode_chd_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chd_waddr_sec( ZynqFcuWr_reg value );
	void outdeinterweave_decode_chd_waddr_sec(  );

	void setdeinterweave_chx_raddr_end( ZynqFcuWr_reg value );
	void outdeinterweave_chx_raddr_end( ZynqFcuWr_reg value );
	void outdeinterweave_chx_raddr_end(  );

	void setdeinterweave_lax_raddr_end( ZynqFcuWr_reg value );
	void outdeinterweave_lax_raddr_end( ZynqFcuWr_reg value );
	void outdeinterweave_lax_raddr_end(  );

	void setdeinterweave_chx_raddr_base( ZynqFcuWr_reg value );
	void outdeinterweave_chx_raddr_base( ZynqFcuWr_reg value );
	void outdeinterweave_chx_raddr_base(  );

	void setdeinterweave_lax_raddr_base( ZynqFcuWr_reg value );
	void outdeinterweave_lax_raddr_base( ZynqFcuWr_reg value );
	void outdeinterweave_lax_raddr_base(  );

	void setdbg_cfg1( ZynqFcuWr_reg value );
	void outdbg_cfg1( ZynqFcuWr_reg value );
	void outdbg_cfg1(  );

public:
	cache_addr addr_trace_req();
	cache_addr addr_eye_trace_wbaseaddr();
	cache_addr addr_roll_ana_trace_wbaseaddr();
	cache_addr addr_roll_la_trace_wbaseaddr();

	cache_addr addr_roll_ana_trace_waddr_end();
	cache_addr addr_roll_la_trace_waddr_end();
	cache_addr addr_roll_trace_waddr_rst();
	cache_addr addr_search_result_wbaseaddr();

	cache_addr addr_search_result_wdone_clr();
	cache_addr addr_ana_main_trace_wbaseaddr();
	cache_addr addr_la_main_trace_wbaseaddr();
	cache_addr addr_ana_zoom_trace_wbaseaddr();

	cache_addr addr_la_zoom_trace_wbaseaddr();
	cache_addr addr_reserved1();
	cache_addr addr_comm_test();
	cache_addr addr_trace_wdone_clr();

	cache_addr addr_trace_wbaseaddr();
	cache_addr addr_trace_dat_len();
	cache_addr addr_deinterweave_chx_lax_rdat_num();
	cache_addr addr_deinterweave_chx_raddr();

	cache_addr addr_deinterweave_cha_waddr();
	cache_addr addr_deinterweave_chb_waddr();
	cache_addr addr_deinterweave_chc_waddr();
	cache_addr addr_deinterweave_chd_waddr();

	cache_addr addr_deinterweave_decode_cha_waddr();
	cache_addr addr_deinterweave_decode_chb_waddr();
	cache_addr addr_deinterweave_decode_chc_waddr();
	cache_addr addr_deinterweave_decode_chd_waddr();

	cache_addr addr_deinterweave_lax_raddr();
	cache_addr addr_deinterweave_la0_waddr();
	cache_addr addr_deinterweave_la1_waddr();
	cache_addr addr_deinterweave_la2_waddr();

	cache_addr addr_deinterweave_la3_waddr();
	cache_addr addr_deinterweave_la4_waddr();
	cache_addr addr_deinterweave_la5_waddr();
	cache_addr addr_deinterweave_la6_waddr();

	cache_addr addr_deinterweave_la7_waddr();
	cache_addr addr_deinterweave_la8_waddr();
	cache_addr addr_deinterweave_la9_waddr();
	cache_addr addr_deinterweave_la10_waddr();

	cache_addr addr_deinterweave_la11_waddr();
	cache_addr addr_deinterweave_la12_waddr();
	cache_addr addr_deinterweave_la13_waddr();
	cache_addr addr_deinterweave_la14_waddr();

	cache_addr addr_deinterweave_la15_waddr();
	cache_addr addr_decode_a_or_b_channel_cmp();
	cache_addr addr_decode_c_or_d_channel_cmp();
	cache_addr addr_deinterweave_property_cfg1();

	cache_addr addr_deinterweave_property_cfg2();
	cache_addr addr_deinterweave_property_cfg3();
	cache_addr addr_deinterweave_property_cfg4();
	cache_addr addr_fpga_rdat_cfg1();

	cache_addr addr_fpga_rdat_cfg2();
	cache_addr addr_fpga_rdat_cfg3();
	cache_addr addr_fpga_rdat_cfg4();
	cache_addr addr_reserved6();

	cache_addr addr_reserved7();
	cache_addr addr_reserved8();
	cache_addr addr_reserved9();
	cache_addr addr_reserved10();

	cache_addr addr_reserved11();
	cache_addr addr_compress_type();
	cache_addr addr_decode_a_or_b_channel_cmp_sec();
	cache_addr addr_decode_c_or_d_channel_cmp_sec();

	cache_addr addr_deinterweave_decode_cha_waddr_sec();
	cache_addr addr_deinterweave_decode_chb_waddr_sec();
	cache_addr addr_deinterweave_decode_chc_waddr_sec();
	cache_addr addr_deinterweave_decode_chd_waddr_sec();

	cache_addr addr_deinterweave_chx_raddr_end();
	cache_addr addr_deinterweave_lax_raddr_end();
	cache_addr addr_deinterweave_chx_raddr_base();
	cache_addr addr_deinterweave_lax_raddr_base();

	cache_addr addr_dbg_cfg1();
};
#define _remap_ZynqFcuWr_()\
	mapIn( &trace_req, 0x0018 + mAddrBase );\
	mapIn( &eye_trace_wbaseaddr, 0x001c + mAddrBase );\
	mapIn( &roll_ana_trace_wbaseaddr, 0x0020 + mAddrBase );\
	mapIn( &roll_la_trace_wbaseaddr, 0x0024 + mAddrBase );\
	\
	mapIn( &roll_ana_trace_waddr_end, 0x0028 + mAddrBase );\
	mapIn( &roll_la_trace_waddr_end, 0x002c + mAddrBase );\
	mapIn( &roll_trace_waddr_rst, 0x0030 + mAddrBase );\
	mapIn( &search_result_wbaseaddr, 0x0034 + mAddrBase );\
	\
	mapIn( &search_result_wdone_clr, 0x0038 + mAddrBase );\
	mapIn( &ana_main_trace_wbaseaddr, 0x003c + mAddrBase );\
	mapIn( &la_main_trace_wbaseaddr, 0x0040 + mAddrBase );\
	mapIn( &ana_zoom_trace_wbaseaddr, 0x0044 + mAddrBase );\
	\
	mapIn( &la_zoom_trace_wbaseaddr, 0x0048 + mAddrBase );\
	mapIn( &reserved1, 0x0800 + mAddrBase );\
	mapIn( &comm_test, 0x0804 + mAddrBase );\
	mapIn( &trace_wdone_clr, 0x0808 + mAddrBase );\
	\
	mapIn( &trace_wbaseaddr, 0x080C + mAddrBase );\
	mapIn( &trace_dat_len, 0x0810 + mAddrBase );\
	mapIn( &deinterweave_chx_lax_rdat_num, 0x0814 + mAddrBase );\
	mapIn( &deinterweave_chx_raddr, 0x0818 + mAddrBase );\
	\
	mapIn( &deinterweave_cha_waddr, 0x081c + mAddrBase );\
	mapIn( &deinterweave_chb_waddr, 0x0820 + mAddrBase );\
	mapIn( &deinterweave_chc_waddr, 0x0824 + mAddrBase );\
	mapIn( &deinterweave_chd_waddr, 0x0828 + mAddrBase );\
	\
	mapIn( &deinterweave_decode_cha_waddr, 0x082c + mAddrBase );\
	mapIn( &deinterweave_decode_chb_waddr, 0x0830 + mAddrBase );\
	mapIn( &deinterweave_decode_chc_waddr, 0x0834 + mAddrBase );\
	mapIn( &deinterweave_decode_chd_waddr, 0x0838 + mAddrBase );\
	\
	mapIn( &deinterweave_lax_raddr, 0x083c + mAddrBase );\
	mapIn( &deinterweave_la0_waddr, 0x0840 + mAddrBase );\
	mapIn( &deinterweave_la1_waddr, 0x0844 + mAddrBase );\
	mapIn( &deinterweave_la2_waddr, 0x0848 + mAddrBase );\
	\
	mapIn( &deinterweave_la3_waddr, 0x084c + mAddrBase );\
	mapIn( &deinterweave_la4_waddr, 0x0850 + mAddrBase );\
	mapIn( &deinterweave_la5_waddr, 0x0854 + mAddrBase );\
	mapIn( &deinterweave_la6_waddr, 0x0858 + mAddrBase );\
	\
	mapIn( &deinterweave_la7_waddr, 0x085c + mAddrBase );\
	mapIn( &deinterweave_la8_waddr, 0x0860 + mAddrBase );\
	mapIn( &deinterweave_la9_waddr, 0x0864 + mAddrBase );\
	mapIn( &deinterweave_la10_waddr, 0x0868 + mAddrBase );\
	\
	mapIn( &deinterweave_la11_waddr, 0x086c + mAddrBase );\
	mapIn( &deinterweave_la12_waddr, 0x0870 + mAddrBase );\
	mapIn( &deinterweave_la13_waddr, 0x0874 + mAddrBase );\
	mapIn( &deinterweave_la14_waddr, 0x0878 + mAddrBase );\
	\
	mapIn( &deinterweave_la15_waddr, 0x087c + mAddrBase );\
	mapIn( &decode_a_or_b_channel_cmp, 0x0880 + mAddrBase );\
	mapIn( &decode_c_or_d_channel_cmp, 0x0884 + mAddrBase );\
	mapIn( &deinterweave_property_cfg1, 0x0888 + mAddrBase );\
	\
	mapIn( &deinterweave_property_cfg2, 0x088c + mAddrBase );\
	mapIn( &deinterweave_property_cfg3, 0x0890 + mAddrBase );\
	mapIn( &deinterweave_property_cfg4, 0x0894 + mAddrBase );\
	mapIn( &fpga_rdat_cfg1, 0x0898 + mAddrBase );\
	\
	mapIn( &fpga_rdat_cfg2, 0x089c + mAddrBase );\
	mapIn( &fpga_rdat_cfg3, 0x08a0 + mAddrBase );\
	mapIn( &fpga_rdat_cfg4, 0x08a4 + mAddrBase );\
	mapIn( &reserved6, 0x08a8 + mAddrBase );\
	\
	mapIn( &reserved7, 0x08ac + mAddrBase );\
	mapIn( &reserved8, 0x08b0 + mAddrBase );\
	mapIn( &reserved9, 0x08b4 + mAddrBase );\
	mapIn( &reserved10, 0x08b8 + mAddrBase );\
	\
	mapIn( &reserved11, 0x08bc + mAddrBase );\
	mapIn( &compress_type, 0x08c0 + mAddrBase );\
	mapIn( &decode_a_or_b_channel_cmp_sec, 0x08c4 + mAddrBase );\
	mapIn( &decode_c_or_d_channel_cmp_sec, 0x08c8 + mAddrBase );\
	\
	mapIn( &deinterweave_decode_cha_waddr_sec, 0x08cc + mAddrBase );\
	mapIn( &deinterweave_decode_chb_waddr_sec, 0x08d0 + mAddrBase );\
	mapIn( &deinterweave_decode_chc_waddr_sec, 0x08d4 + mAddrBase );\
	mapIn( &deinterweave_decode_chd_waddr_sec, 0x08d8 + mAddrBase );\
	\
	mapIn( &deinterweave_chx_raddr_end, 0x08dc + mAddrBase );\
	mapIn( &deinterweave_lax_raddr_end, 0x08e0 + mAddrBase );\
	mapIn( &deinterweave_chx_raddr_base, 0x08e4 + mAddrBase );\
	mapIn( &deinterweave_lax_raddr_base, 0x08e8 + mAddrBase );\
	\
	mapIn( &dbg_cfg1, 0x0050 + mAddrBase );\


#endif
