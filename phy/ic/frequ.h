#ifndef _FREQU_IC_H_
#define _FREQU_IC_H_

#define Frequ_reg unsigned int

union Frequ_freq_cfg_1{
	struct{
	Frequ_reg ref_en:1;
	Frequ_reg res1:3;
	Frequ_reg gate_time:2;
	Frequ_reg res2:3;

	Frequ_reg sel_mode:1;
	Frequ_reg ctrl_clear:1;
	Frequ_reg gate_posi:1;
	Frequ_reg sel_event:1;

	Frequ_reg dvm_src:2;
	Frequ_reg dvm_gate:2;
	Frequ_reg src_sel:5;
	Frequ_reg sel_gate:5;
	};
	Frequ_reg payload;
};

struct FrequMemProxy {
	Frequ_freq_cfg_1 freq_cfg_1; 
	Frequ_reg start_time;
	Frequ_reg end_time;
	Frequ_reg counter;

	Frequ_reg signal_wide_l;
	Frequ_reg signal_wide_h;
	Frequ_reg signal_event_l;
	Frequ_reg signal_event_h;

	Frequ_reg dvm_rms;
	Frequ_reg dvm_aver;
	Frequ_reg dvm_range;
	void assignfreq_cfg_1_ref_en( Frequ_reg value  );
	void assignfreq_cfg_1_res1( Frequ_reg value  );
	void assignfreq_cfg_1_gate_time( Frequ_reg value  );
	void assignfreq_cfg_1_res2( Frequ_reg value  );
	void assignfreq_cfg_1_sel_mode( Frequ_reg value  );
	void assignfreq_cfg_1_ctrl_clear( Frequ_reg value  );
	void assignfreq_cfg_1_gate_posi( Frequ_reg value  );
	void assignfreq_cfg_1_sel_event( Frequ_reg value  );
	void assignfreq_cfg_1_dvm_src( Frequ_reg value  );
	void assignfreq_cfg_1_dvm_gate( Frequ_reg value  );
	void assignfreq_cfg_1_src_sel( Frequ_reg value  );
	void assignfreq_cfg_1_sel_gate( Frequ_reg value  );

	void assignfreq_cfg_1( Frequ_reg value );


	Frequ_reg getstart_time(  );

	Frequ_reg getend_time(  );

	Frequ_reg getcounter(  );

	Frequ_reg getsignal_wide_l(  );

	Frequ_reg getsignal_wide_h(  );

	Frequ_reg getsignal_event_l(  );

	Frequ_reg getsignal_event_h(  );

	Frequ_reg getdvm_rms(  );

	Frequ_reg getdvm_aver(  );

	Frequ_reg getdvm_range(  );

};
struct FrequMem : public FrequMemProxy, public IPhyFpga{
protected:
	Frequ_reg mAddrBase;
public:
	FrequMem( Frequ_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( Frequ_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( FrequMemProxy *proxy);
	void pull( FrequMemProxy *proxy);

public:
	void setfreq_cfg_1_ref_en( Frequ_reg value );
	void setfreq_cfg_1_res1( Frequ_reg value );
	void setfreq_cfg_1_gate_time( Frequ_reg value );
	void setfreq_cfg_1_res2( Frequ_reg value );
	void setfreq_cfg_1_sel_mode( Frequ_reg value );
	void setfreq_cfg_1_ctrl_clear( Frequ_reg value );
	void setfreq_cfg_1_gate_posi( Frequ_reg value );
	void setfreq_cfg_1_sel_event( Frequ_reg value );
	void setfreq_cfg_1_dvm_src( Frequ_reg value );
	void setfreq_cfg_1_dvm_gate( Frequ_reg value );
	void setfreq_cfg_1_src_sel( Frequ_reg value );
	void setfreq_cfg_1_sel_gate( Frequ_reg value );

	void setfreq_cfg_1( Frequ_reg value );
	void outfreq_cfg_1_ref_en( Frequ_reg value );
	void pulsefreq_cfg_1_ref_en( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_res1( Frequ_reg value );
	void pulsefreq_cfg_1_res1( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_gate_time( Frequ_reg value );
	void pulsefreq_cfg_1_gate_time( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_res2( Frequ_reg value );
	void pulsefreq_cfg_1_res2( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_sel_mode( Frequ_reg value );
	void pulsefreq_cfg_1_sel_mode( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_ctrl_clear( Frequ_reg value );
	void pulsefreq_cfg_1_ctrl_clear( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_gate_posi( Frequ_reg value );
	void pulsefreq_cfg_1_gate_posi( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_sel_event( Frequ_reg value );
	void pulsefreq_cfg_1_sel_event( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_dvm_src( Frequ_reg value );
	void pulsefreq_cfg_1_dvm_src( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_dvm_gate( Frequ_reg value );
	void pulsefreq_cfg_1_dvm_gate( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_src_sel( Frequ_reg value );
	void pulsefreq_cfg_1_src_sel( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );
	void outfreq_cfg_1_sel_gate( Frequ_reg value );
	void pulsefreq_cfg_1_sel_gate( Frequ_reg activeV, Frequ_reg idleV=0, int timeus=0 );

	void outfreq_cfg_1( Frequ_reg value );
	void outfreq_cfg_1(  );


	Frequ_reg instart_time(  );

	Frequ_reg inend_time(  );

	Frequ_reg incounter(  );

	Frequ_reg insignal_wide_l(  );

	Frequ_reg insignal_wide_h(  );

	Frequ_reg insignal_event_l(  );

	Frequ_reg insignal_event_h(  );

	Frequ_reg indvm_rms(  );

	Frequ_reg indvm_aver(  );

	Frequ_reg indvm_range(  );

public:
	cache_addr addr_freq_cfg_1();
	cache_addr addr_start_time();
	cache_addr addr_end_time();
	cache_addr addr_counter();

	cache_addr addr_signal_wide_l();
	cache_addr addr_signal_wide_h();
	cache_addr addr_signal_event_l();
	cache_addr addr_signal_event_h();

	cache_addr addr_dvm_rms();
	cache_addr addr_dvm_aver();
	cache_addr addr_dvm_range();
};
#define _remap_Frequ_()\
	mapIn( &freq_cfg_1, 0x1a00 + mAddrBase );\
	mapIn( &start_time, 0x1900 + mAddrBase );\
	mapIn( &end_time, 0x1904 + mAddrBase );\
	mapIn( &counter, 0x1908 + mAddrBase );\
	\
	mapIn( &signal_wide_l, 0x190c + mAddrBase );\
	mapIn( &signal_wide_h, 0x1910 + mAddrBase );\
	mapIn( &signal_event_l, 0x1914 + mAddrBase );\
	mapIn( &signal_event_h, 0x1918 + mAddrBase );\
	\
	mapIn( &dvm_rms, 0x191C + mAddrBase );\
	mapIn( &dvm_aver, 0x1920 + mAddrBase );\
	mapIn( &dvm_range, 0x1924 + mAddrBase );\


#endif
