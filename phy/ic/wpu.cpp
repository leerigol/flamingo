#include "wpu.h"
Wpu_reg WpuMemProxy::getversion()
{
	return version;
}


void WpuMemProxy::assigninit_pe( Wpu_reg value )
{
	mem_assign(init_pe,value);
}


void WpuMemProxy::assignconfig_ID( Wpu_reg value )
{
	mem_assign(config_ID,value);
}


Wpu_reg WpuMemProxy::getconfig_ID()
{
	return config_ID;
}


void WpuMemProxy::assigndisplay_en( Wpu_reg value )
{
	mem_assign(display_en,value);
}


Wpu_reg WpuMemProxy::getdisplay_en()
{
	return display_en;
}


void WpuMemProxy::assignanalog_mode_analog_ch_mode( Wpu_reg value )
{
	mem_assign_field(analog_mode,analog_ch_mode,value);
}
void WpuMemProxy::assignanalog_mode_analog_ch_en( Wpu_reg value )
{
	mem_assign_field(analog_mode,analog_ch_en,value);
}

void WpuMemProxy::assignanalog_mode( Wpu_reg value )
{
	mem_assign_field(analog_mode,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_mode_analog_ch_mode()
{
	return analog_mode.analog_ch_mode;
}
Wpu_reg WpuMemProxy::getanalog_mode_analog_ch_en()
{
	return analog_mode.analog_ch_en;
}

Wpu_reg WpuMemProxy::getanalog_mode()
{
	return analog_mode.payload;
}


void WpuMemProxy::assignLA_en( Wpu_reg value )
{
	mem_assign(LA_en,value);
}


Wpu_reg WpuMemProxy::getLA_en()
{
	return LA_en;
}


void WpuMemProxy::assignrefresh_time( Wpu_reg value )
{
	mem_assign(refresh_time,value);
}


Wpu_reg WpuMemProxy::getrefresh_time()
{
	return refresh_time;
}


void WpuMemProxy::assignpersist_mode( Wpu_reg value )
{
	mem_assign(persist_mode,value);
}


Wpu_reg WpuMemProxy::getpersist_mode()
{
	return persist_mode;
}


void WpuMemProxy::assignpersist_init( Wpu_reg value )
{
	mem_assign(persist_init,value);
}


Wpu_reg WpuMemProxy::getpersist_init()
{
	return persist_init;
}


void WpuMemProxy::assignpersist_dim_rate( Wpu_reg value )
{
	mem_assign(persist_dim_rate,value);
}


Wpu_reg WpuMemProxy::getpersist_dim_rate()
{
	return persist_dim_rate;
}


void WpuMemProxy::assignplot_mode( Wpu_reg value )
{
	mem_assign(plot_mode,value);
}


Wpu_reg WpuMemProxy::getplot_mode()
{
	return plot_mode;
}


void WpuMemProxy::assignslope_mode( Wpu_reg value )
{
	mem_assign(slope_mode,value);
}


Wpu_reg WpuMemProxy::getslope_mode()
{
	return slope_mode;
}


void WpuMemProxy::assignanalog_ch0_gain( Wpu_reg value )
{
	mem_assign(analog_ch0_gain,value);
}


Wpu_reg WpuMemProxy::getanalog_ch0_gain()
{
	return analog_ch0_gain;
}


void WpuMemProxy::assignanalog_ch0_noise_ch0_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch0_noise,ch0_noise_grade,value);
}
void WpuMemProxy::assignanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch0_noise,ch0_zoom_noise_grade,value);
}

void WpuMemProxy::assignanalog_ch0_noise( Wpu_reg value )
{
	mem_assign_field(analog_ch0_noise,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch0_noise_ch0_noise_grade()
{
	return analog_ch0_noise.ch0_noise_grade;
}
Wpu_reg WpuMemProxy::getanalog_ch0_noise_ch0_zoom_noise_grade()
{
	return analog_ch0_noise.ch0_zoom_noise_grade;
}

Wpu_reg WpuMemProxy::getanalog_ch0_noise()
{
	return analog_ch0_noise.payload;
}


void WpuMemProxy::assignanalog_ch0_offset( Wpu_reg value )
{
	mem_assign(analog_ch0_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch0_offset()
{
	return analog_ch0_offset;
}


void WpuMemProxy::assignanalog_ch1_gain( Wpu_reg value )
{
	mem_assign(analog_ch1_gain,value);
}


Wpu_reg WpuMemProxy::getanalog_ch1_gain()
{
	return analog_ch1_gain;
}


void WpuMemProxy::assignanalog_ch1_noise_ch1_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch1_noise,ch1_noise_grade,value);
}
void WpuMemProxy::assignanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch1_noise,ch1_zoom_noise_grade,value);
}

void WpuMemProxy::assignanalog_ch1_noise( Wpu_reg value )
{
	mem_assign_field(analog_ch1_noise,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch1_noise_ch1_noise_grade()
{
	return analog_ch1_noise.ch1_noise_grade;
}
Wpu_reg WpuMemProxy::getanalog_ch1_noise_ch1_zoom_noise_grade()
{
	return analog_ch1_noise.ch1_zoom_noise_grade;
}

Wpu_reg WpuMemProxy::getanalog_ch1_noise()
{
	return analog_ch1_noise.payload;
}


void WpuMemProxy::assignanalog_ch1_offset( Wpu_reg value )
{
	mem_assign(analog_ch1_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch1_offset()
{
	return analog_ch1_offset;
}


void WpuMemProxy::assignanalog_ch2_gain( Wpu_reg value )
{
	mem_assign(analog_ch2_gain,value);
}


Wpu_reg WpuMemProxy::getanalog_ch2_gain()
{
	return analog_ch2_gain;
}


void WpuMemProxy::assignanalog_ch2_noise_ch2_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch2_noise,ch2_noise_grade,value);
}
void WpuMemProxy::assignanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch2_noise,ch2_zoom_noise_grade,value);
}

void WpuMemProxy::assignanalog_ch2_noise( Wpu_reg value )
{
	mem_assign_field(analog_ch2_noise,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch2_noise_ch2_noise_grade()
{
	return analog_ch2_noise.ch2_noise_grade;
}
Wpu_reg WpuMemProxy::getanalog_ch2_noise_ch2_zoom_noise_grade()
{
	return analog_ch2_noise.ch2_zoom_noise_grade;
}

Wpu_reg WpuMemProxy::getanalog_ch2_noise()
{
	return analog_ch2_noise.payload;
}


void WpuMemProxy::assignanalog_ch2_offset( Wpu_reg value )
{
	mem_assign(analog_ch2_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch2_offset()
{
	return analog_ch2_offset;
}


void WpuMemProxy::assignanalog_ch3_gain( Wpu_reg value )
{
	mem_assign(analog_ch3_gain,value);
}


Wpu_reg WpuMemProxy::getanalog_ch3_gain()
{
	return analog_ch3_gain;
}


void WpuMemProxy::assignanalog_ch3_noise_ch3_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch3_noise,ch3_noise_grade,value);
}
void WpuMemProxy::assignanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg value )
{
	mem_assign_field(analog_ch3_noise,ch3_zoom_noise_grade,value);
}

void WpuMemProxy::assignanalog_ch3_noise( Wpu_reg value )
{
	mem_assign_field(analog_ch3_noise,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch3_noise_ch3_noise_grade()
{
	return analog_ch3_noise.ch3_noise_grade;
}
Wpu_reg WpuMemProxy::getanalog_ch3_noise_ch3_zoom_noise_grade()
{
	return analog_ch3_noise.ch3_zoom_noise_grade;
}

Wpu_reg WpuMemProxy::getanalog_ch3_noise()
{
	return analog_ch3_noise.payload;
}


void WpuMemProxy::assignanalog_ch3_offset( Wpu_reg value )
{
	mem_assign(analog_ch3_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch3_offset()
{
	return analog_ch3_offset;
}


void WpuMemProxy::assigncolor_map( Wpu_reg value )
{
	mem_assign(color_map,value);
}


Wpu_reg WpuMemProxy::getcolor_map()
{
	return color_map;
}


void WpuMemProxy::assignview_layer_group_order( Wpu_reg value )
{
	mem_assign_field(view_layer,group_order,value);
}
void WpuMemProxy::assignview_layer_analog_order( Wpu_reg value )
{
	mem_assign_field(view_layer,analog_order,value);
}
void WpuMemProxy::assignview_layer_math_order( Wpu_reg value )
{
	mem_assign_field(view_layer,math_order,value);
}

void WpuMemProxy::assignview_layer( Wpu_reg value )
{
	mem_assign_field(view_layer,payload,value);
}


Wpu_reg WpuMemProxy::getview_layer_group_order()
{
	return view_layer.group_order;
}
Wpu_reg WpuMemProxy::getview_layer_analog_order()
{
	return view_layer.analog_order;
}
Wpu_reg WpuMemProxy::getview_layer_math_order()
{
	return view_layer.math_order;
}

Wpu_reg WpuMemProxy::getview_layer()
{
	return view_layer.payload;
}


void WpuMemProxy::assigntrace_layer_addr_0( Wpu_reg value )
{
	mem_assign(trace_layer_addr_0,value);
}


Wpu_reg WpuMemProxy::gettrace_layer_addr_0()
{
	return trace_layer_addr_0;
}


void WpuMemProxy::assigntrace_layer_addr_1( Wpu_reg value )
{
	mem_assign(trace_layer_addr_1,value);
}


Wpu_reg WpuMemProxy::gettrace_layer_addr_1()
{
	return trace_layer_addr_1;
}


void WpuMemProxy::assignwpu_clear( Wpu_reg value )
{
	mem_assign(wpu_clear,value);
}


Wpu_reg WpuMemProxy::getsrn_num()
{
	return srn_num;
}


Wpu_reg WpuMemProxy::getwpu_status()
{
	return wpu_status;
}


Wpu_reg WpuMemProxy::getmem_status()
{
	return mem_status;
}


Wpu_reg WpuMemProxy::getinit_status()
{
	return init_status;
}


Wpu_reg WpuMemProxy::getload_config_id()
{
	return load_config_id;
}


void WpuMemProxy::assigninit_la_plot_table_la_plot_tbl_wen( Wpu_reg value )
{
	mem_assign_field(init_la_plot_table,la_plot_tbl_wen,value);
}
void WpuMemProxy::assigninit_la_plot_table_la_plot_tbl_waddr( Wpu_reg value )
{
	mem_assign_field(init_la_plot_table,la_plot_tbl_waddr,value);
}

void WpuMemProxy::assigninit_la_plot_table( Wpu_reg value )
{
	mem_assign_field(init_la_plot_table,payload,value);
}


Wpu_reg WpuMemProxy::getinit_la_plot_table_la_plot_tbl_wen()
{
	return init_la_plot_table.la_plot_tbl_wen;
}
Wpu_reg WpuMemProxy::getinit_la_plot_table_la_plot_tbl_waddr()
{
	return init_la_plot_table.la_plot_tbl_waddr;
}

Wpu_reg WpuMemProxy::getinit_la_plot_table()
{
	return init_la_plot_table.payload;
}


void WpuMemProxy::assignla_plot_tbl_data_COLOR( Wpu_reg value )
{
	mem_assign_field(la_plot_tbl_data,COLOR,value);
}
void WpuMemProxy::assignla_plot_tbl_data_POSITION( Wpu_reg value )
{
	mem_assign_field(la_plot_tbl_data,POSITION,value);
}
void WpuMemProxy::assignla_plot_tbl_data_reserved( Wpu_reg value )
{
	mem_assign_field(la_plot_tbl_data,reserved,value);
}
void WpuMemProxy::assignla_plot_tbl_data_zoom_chn_flag( Wpu_reg value )
{
	mem_assign_field(la_plot_tbl_data,zoom_chn_flag,value);
}
void WpuMemProxy::assignla_plot_tbl_data_CH_NO( Wpu_reg value )
{
	mem_assign_field(la_plot_tbl_data,CH_NO,value);
}

void WpuMemProxy::assignla_plot_tbl_data( Wpu_reg value )
{
	mem_assign_field(la_plot_tbl_data,payload,value);
}


Wpu_reg WpuMemProxy::getla_plot_tbl_data_COLOR()
{
	return la_plot_tbl_data.COLOR;
}
Wpu_reg WpuMemProxy::getla_plot_tbl_data_POSITION()
{
	return la_plot_tbl_data.POSITION;
}
Wpu_reg WpuMemProxy::getla_plot_tbl_data_reserved()
{
	return la_plot_tbl_data.reserved;
}
Wpu_reg WpuMemProxy::getla_plot_tbl_data_zoom_chn_flag()
{
	return la_plot_tbl_data.zoom_chn_flag;
}
Wpu_reg WpuMemProxy::getla_plot_tbl_data_CH_NO()
{
	return la_plot_tbl_data.CH_NO;
}

Wpu_reg WpuMemProxy::getla_plot_tbl_data()
{
	return la_plot_tbl_data.payload;
}


void WpuMemProxy::assignwave_view_start( Wpu_reg value )
{
	mem_assign(wave_view_start,value);
}


Wpu_reg WpuMemProxy::getwave_view_start()
{
	return wave_view_start;
}


void WpuMemProxy::assignwave_view_length( Wpu_reg value )
{
	mem_assign(wave_view_length,value);
}


Wpu_reg WpuMemProxy::getwave_view_length()
{
	return wave_view_length;
}


void WpuMemProxy::assignzoom_view_start( Wpu_reg value )
{
	mem_assign(zoom_view_start,value);
}


Wpu_reg WpuMemProxy::getzoom_view_start()
{
	return zoom_view_start;
}


void WpuMemProxy::assignzoom_view_length( Wpu_reg value )
{
	mem_assign(zoom_view_length,value);
}


Wpu_reg WpuMemProxy::getzoom_view_length()
{
	return zoom_view_length;
}


void WpuMemProxy::assignxy0_en_en( Wpu_reg value )
{
	mem_assign_field(xy0_en,en,value);
}
void WpuMemProxy::assignxy0_en_column_start( Wpu_reg value )
{
	mem_assign_field(xy0_en,column_start,value);
}

void WpuMemProxy::assignxy0_en( Wpu_reg value )
{
	mem_assign_field(xy0_en,payload,value);
}


Wpu_reg WpuMemProxy::getxy0_en_en()
{
	return xy0_en.en;
}
Wpu_reg WpuMemProxy::getxy0_en_column_start()
{
	return xy0_en.column_start;
}

Wpu_reg WpuMemProxy::getxy0_en()
{
	return xy0_en.payload;
}


void WpuMemProxy::assignxy1_en_en( Wpu_reg value )
{
	mem_assign_field(xy1_en,en,value);
}
void WpuMemProxy::assignxy1_en_column_start( Wpu_reg value )
{
	mem_assign_field(xy1_en,column_start,value);
}

void WpuMemProxy::assignxy1_en( Wpu_reg value )
{
	mem_assign_field(xy1_en,payload,value);
}


Wpu_reg WpuMemProxy::getxy1_en_en()
{
	return xy1_en.en;
}
Wpu_reg WpuMemProxy::getxy1_en_column_start()
{
	return xy1_en.column_start;
}

Wpu_reg WpuMemProxy::getxy1_en()
{
	return xy1_en.payload;
}


void WpuMemProxy::assignxy2_en_en( Wpu_reg value )
{
	mem_assign_field(xy2_en,en,value);
}
void WpuMemProxy::assignxy2_en_column_start( Wpu_reg value )
{
	mem_assign_field(xy2_en,column_start,value);
}

void WpuMemProxy::assignxy2_en( Wpu_reg value )
{
	mem_assign_field(xy2_en,payload,value);
}


Wpu_reg WpuMemProxy::getxy2_en_en()
{
	return xy2_en.en;
}
Wpu_reg WpuMemProxy::getxy2_en_column_start()
{
	return xy2_en.column_start;
}

Wpu_reg WpuMemProxy::getxy2_en()
{
	return xy2_en.payload;
}


void WpuMemProxy::assignxy3_en_en( Wpu_reg value )
{
	mem_assign_field(xy3_en,en,value);
}
void WpuMemProxy::assignxy3_en_column_start( Wpu_reg value )
{
	mem_assign_field(xy3_en,column_start,value);
}

void WpuMemProxy::assignxy3_en( Wpu_reg value )
{
	mem_assign_field(xy3_en,payload,value);
}


Wpu_reg WpuMemProxy::getxy3_en_en()
{
	return xy3_en.en;
}
Wpu_reg WpuMemProxy::getxy3_en_column_start()
{
	return xy3_en.column_start;
}

Wpu_reg WpuMemProxy::getxy3_en()
{
	return xy3_en.payload;
}


void WpuMemProxy::assigninit_palette_table_palette_tbl_wen( Wpu_reg value )
{
	mem_assign_field(init_palette_table,palette_tbl_wen,value);
}
void WpuMemProxy::assigninit_palette_table_palette_tbl_waddr( Wpu_reg value )
{
	mem_assign_field(init_palette_table,palette_tbl_waddr,value);
}
void WpuMemProxy::assigninit_palette_table_palette_tbl_ch( Wpu_reg value )
{
	mem_assign_field(init_palette_table,palette_tbl_ch,value);
}

void WpuMemProxy::assigninit_palette_table( Wpu_reg value )
{
	mem_assign_field(init_palette_table,payload,value);
}


void WpuMemProxy::assignpalette_tbl_wdata( Wpu_reg value )
{
	mem_assign(palette_tbl_wdata,value);
}


Wpu_reg WpuMemProxy::getpalette_tbl_wdata()
{
	return palette_tbl_wdata;
}


void WpuMemProxy::assigndata_mode( Wpu_reg value )
{
	mem_assign(data_mode,value);
}


Wpu_reg WpuMemProxy::getdata_mode()
{
	return data_mode;
}


void WpuMemProxy::assignscale_times_scale_times_m( Wpu_reg value )
{
	mem_assign_field(scale_times,scale_times_m,value);
}
void WpuMemProxy::assignscale_times_scale_times_n( Wpu_reg value )
{
	mem_assign_field(scale_times,scale_times_n,value);
}
void WpuMemProxy::assignscale_times_scale_times_en( Wpu_reg value )
{
	mem_assign_field(scale_times,scale_times_en,value);
}

void WpuMemProxy::assignscale_times( Wpu_reg value )
{
	mem_assign_field(scale_times,payload,value);
}


Wpu_reg WpuMemProxy::getscale_times_scale_times_m()
{
	return scale_times.scale_times_m;
}
Wpu_reg WpuMemProxy::getscale_times_scale_times_n()
{
	return scale_times.scale_times_n;
}
Wpu_reg WpuMemProxy::getscale_times_scale_times_en()
{
	return scale_times.scale_times_en;
}

Wpu_reg WpuMemProxy::getscale_times()
{
	return scale_times.payload;
}


void WpuMemProxy::assignscale_length( Wpu_reg value )
{
	mem_assign(scale_length,value);
}


Wpu_reg WpuMemProxy::getscale_length()
{
	return scale_length;
}


void WpuMemProxy::assignanalog_ch0_zoom_offset( Wpu_reg value )
{
	mem_assign(analog_ch0_zoom_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch0_zoom_offset()
{
	return analog_ch0_zoom_offset;
}


void WpuMemProxy::assignanalog_ch1_zoom_offset( Wpu_reg value )
{
	mem_assign(analog_ch1_zoom_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch1_zoom_offset()
{
	return analog_ch1_zoom_offset;
}


void WpuMemProxy::assignanalog_ch2_zoom_offset( Wpu_reg value )
{
	mem_assign(analog_ch2_zoom_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch2_zoom_offset()
{
	return analog_ch2_zoom_offset;
}


void WpuMemProxy::assignanalog_ch3_zoom_offset( Wpu_reg value )
{
	mem_assign(analog_ch3_zoom_offset,value);
}


Wpu_reg WpuMemProxy::getanalog_ch3_zoom_offset()
{
	return analog_ch3_zoom_offset;
}


void WpuMemProxy::assignmax_out_main_max_out( Wpu_reg value )
{
	mem_assign_field(max_out,main_max_out,value);
}
void WpuMemProxy::assignmax_out_zoom_max_out( Wpu_reg value )
{
	mem_assign_field(max_out,zoom_max_out,value);
}

void WpuMemProxy::assignmax_out( Wpu_reg value )
{
	mem_assign_field(max_out,payload,value);
}


Wpu_reg WpuMemProxy::getmax_out_main_max_out()
{
	return max_out.main_max_out;
}
Wpu_reg WpuMemProxy::getmax_out_zoom_max_out()
{
	return max_out.zoom_max_out;
}

Wpu_reg WpuMemProxy::getmax_out()
{
	return max_out.payload;
}


void WpuMemProxy::assignview_bottom_main_view_bottom( Wpu_reg value )
{
	mem_assign_field(view_bottom,main_view_bottom,value);
}
void WpuMemProxy::assignview_bottom_zoom_view_bottom( Wpu_reg value )
{
	mem_assign_field(view_bottom,zoom_view_bottom,value);
}

void WpuMemProxy::assignview_bottom( Wpu_reg value )
{
	mem_assign_field(view_bottom,payload,value);
}


Wpu_reg WpuMemProxy::getview_bottom_main_view_bottom()
{
	return view_bottom.main_view_bottom;
}
Wpu_reg WpuMemProxy::getview_bottom_zoom_view_bottom()
{
	return view_bottom.zoom_view_bottom;
}

Wpu_reg WpuMemProxy::getview_bottom()
{
	return view_bottom.payload;
}


void WpuMemProxy::assignzoom_en_en( Wpu_reg value )
{
	mem_assign_field(zoom_en,en,value);
}

void WpuMemProxy::assignzoom_en( Wpu_reg value )
{
	mem_assign_field(zoom_en,payload,value);
}


Wpu_reg WpuMemProxy::getzoom_en_en()
{
	return zoom_en.en;
}

Wpu_reg WpuMemProxy::getzoom_en()
{
	return zoom_en.payload;
}


void WpuMemProxy::assignanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch0_zoom_gain,analog_ch0_zoom_gain,value);
}

void WpuMemProxy::assignanalog_ch0_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch0_zoom_gain,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch0_zoom_gain_analog_ch0_zoom_gain()
{
	return analog_ch0_zoom_gain.analog_ch0_zoom_gain;
}

Wpu_reg WpuMemProxy::getanalog_ch0_zoom_gain()
{
	return analog_ch0_zoom_gain.payload;
}


void WpuMemProxy::assignanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch1_zoom_gain,analog_ch1_zoom_gain,value);
}

void WpuMemProxy::assignanalog_ch1_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch1_zoom_gain,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch1_zoom_gain_analog_ch1_zoom_gain()
{
	return analog_ch1_zoom_gain.analog_ch1_zoom_gain;
}

Wpu_reg WpuMemProxy::getanalog_ch1_zoom_gain()
{
	return analog_ch1_zoom_gain.payload;
}


void WpuMemProxy::assignanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch2_zoom_gain,analog_ch2_zoom_gain,value);
}

void WpuMemProxy::assignanalog_ch2_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch2_zoom_gain,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch2_zoom_gain_analog_ch2_zoom_gain()
{
	return analog_ch2_zoom_gain.analog_ch2_zoom_gain;
}

Wpu_reg WpuMemProxy::getanalog_ch2_zoom_gain()
{
	return analog_ch2_zoom_gain.payload;
}


void WpuMemProxy::assignanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch3_zoom_gain,analog_ch3_zoom_gain,value);
}

void WpuMemProxy::assignanalog_ch3_zoom_gain( Wpu_reg value )
{
	mem_assign_field(analog_ch3_zoom_gain,payload,value);
}


Wpu_reg WpuMemProxy::getanalog_ch3_zoom_gain_analog_ch3_zoom_gain()
{
	return analog_ch3_zoom_gain.analog_ch3_zoom_gain;
}

Wpu_reg WpuMemProxy::getanalog_ch3_zoom_gain()
{
	return analog_ch3_zoom_gain.payload;
}


Wpu_reg WpuMemProxy::getdebug_info_pm_plot_cnt()
{
	return debug_info_pm.plot_cnt;
}
Wpu_reg WpuMemProxy::getdebug_info_pm_local_plot_len()
{
	return debug_info_pm.local_plot_len;
}
Wpu_reg WpuMemProxy::getdebug_info_pm_plot_phase()
{
	return debug_info_pm.plot_phase;
}
Wpu_reg WpuMemProxy::getdebug_info_pm_zone_buf_empty_n()
{
	return debug_info_pm.zone_buf_empty_n;
}
Wpu_reg WpuMemProxy::getdebug_info_pm_pe_in_rdy()
{
	return debug_info_pm.pe_in_rdy;
}
Wpu_reg WpuMemProxy::getdebug_info_pm_pe_out_rdy()
{
	return debug_info_pm.pe_out_rdy;
}
Wpu_reg WpuMemProxy::getdebug_info_pm_plot_out_rdy()
{
	return debug_info_pm.plot_out_rdy;
}

Wpu_reg WpuMemProxy::getdebug_info_pm()
{
	return debug_info_pm.payload;
}


void WpuMemProxy::assignzoom_scale_times_zoom_scale_times_m( Wpu_reg value )
{
	mem_assign_field(zoom_scale_times,zoom_scale_times_m,value);
}
void WpuMemProxy::assignzoom_scale_times_zoom_scale_times_n( Wpu_reg value )
{
	mem_assign_field(zoom_scale_times,zoom_scale_times_n,value);
}
void WpuMemProxy::assignzoom_scale_times_zoom_scale_times_en( Wpu_reg value )
{
	mem_assign_field(zoom_scale_times,zoom_scale_times_en,value);
}

void WpuMemProxy::assignzoom_scale_times( Wpu_reg value )
{
	mem_assign_field(zoom_scale_times,payload,value);
}


Wpu_reg WpuMemProxy::getzoom_scale_times_zoom_scale_times_m()
{
	return zoom_scale_times.zoom_scale_times_m;
}
Wpu_reg WpuMemProxy::getzoom_scale_times_zoom_scale_times_n()
{
	return zoom_scale_times.zoom_scale_times_n;
}
Wpu_reg WpuMemProxy::getzoom_scale_times_zoom_scale_times_en()
{
	return zoom_scale_times.zoom_scale_times_en;
}

Wpu_reg WpuMemProxy::getzoom_scale_times()
{
	return zoom_scale_times.payload;
}


void WpuMemProxy::assignzoom_scale_length( Wpu_reg value )
{
	mem_assign(zoom_scale_length,value);
}


Wpu_reg WpuMemProxy::getzoom_scale_length()
{
	return zoom_scale_length;
}


void WpuMemProxy::assigntime_base_mode( Wpu_reg value )
{
	mem_assign(time_base_mode,value);
}


Wpu_reg WpuMemProxy::gettime_base_mode()
{
	return time_base_mode;
}


void WpuMemProxy::assignscan_trig_cnt_scan_trig_col( Wpu_reg value )
{
	mem_assign_field(scan_trig_cnt,scan_trig_col,value);
}
void WpuMemProxy::assignscan_trig_cnt_scan_trig_addr( Wpu_reg value )
{
	mem_assign_field(scan_trig_cnt,scan_trig_addr,value);
}

void WpuMemProxy::assignscan_trig_cnt( Wpu_reg value )
{
	mem_assign_field(scan_trig_cnt,payload,value);
}


Wpu_reg WpuMemProxy::getscan_trig_cnt_scan_trig_col()
{
	return scan_trig_cnt.scan_trig_col;
}
Wpu_reg WpuMemProxy::getscan_trig_cnt_scan_trig_addr()
{
	return scan_trig_cnt.scan_trig_addr;
}

Wpu_reg WpuMemProxy::getscan_trig_cnt()
{
	return scan_trig_cnt.payload;
}


Wpu_reg WpuMemProxy::getroll_lenth_roll_lenth_cnt()
{
	return roll_lenth.roll_lenth_cnt;
}
Wpu_reg WpuMemProxy::getroll_lenth_roll_full()
{
	return roll_lenth.roll_full;
}

Wpu_reg WpuMemProxy::getroll_lenth()
{
	return roll_lenth.payload;
}



void WpuMem::_loadOtp()
{
	version=0x0;
	init_pe=0x0;
	config_ID=0x0;
	display_en=0x2;

	analog_mode.payload=0x0;
		analog_mode.analog_ch_mode=0x4;
		analog_mode.analog_ch_en=0xf;

	LA_en=0xffff;
	refresh_time=0x98000;
	persist_mode=0x0;

	persist_init=0x0;
	persist_dim_rate=0x0;
	plot_mode=0x1;
	slope_mode=0xff;

	analog_ch0_gain=0x400;
	analog_ch0_noise.payload=0x0;
		analog_ch0_noise.ch0_noise_grade=0x0;
		analog_ch0_noise.ch0_zoom_noise_grade=0x0;

	analog_ch0_offset=0x0;
	analog_ch1_gain=0x400;

	analog_ch1_noise.payload=0x0;
		analog_ch1_noise.ch1_noise_grade=0x0;
		analog_ch1_noise.ch1_zoom_noise_grade=0x0;

	analog_ch1_offset=0x0;
	analog_ch2_gain=0x400;
	analog_ch2_noise.payload=0x0;
		analog_ch2_noise.ch2_noise_grade=0x0;
		analog_ch2_noise.ch2_zoom_noise_grade=0x0;


	analog_ch2_offset=0x0;
	analog_ch3_gain=0x400;
	analog_ch3_noise.payload=0x0;
		analog_ch3_noise.ch3_noise_grade=0x0;
		analog_ch3_noise.ch3_zoom_noise_grade=0x0;

	analog_ch3_offset=0x0;

	color_map=0x0;
	view_layer.payload=0x0;
		view_layer.group_order=0x0;
		view_layer.analog_order=0x0;
		view_layer.math_order=0x0;

	trace_layer_addr_0=0x0;
	trace_layer_addr_1=0x0;

	wpu_clear=0x0;
	srn_num=0x0;
	wpu_status=0x0;
	mem_status=0x0;

	init_status=0x0;
	load_config_id=0x0;
	init_la_plot_table.payload=0x0;
		init_la_plot_table.la_plot_tbl_wen=0x0;
		init_la_plot_table.la_plot_tbl_waddr=0x0;

	la_plot_tbl_data.payload=0x0;
		la_plot_tbl_data.COLOR=0xff00;
		la_plot_tbl_data.POSITION=0x0;
		la_plot_tbl_data.reserved=0x0;
		la_plot_tbl_data.zoom_chn_flag=0x0;

		la_plot_tbl_data.CH_NO=0x0;


	wave_view_start=0x0;
	wave_view_length=0x0;
	zoom_view_start=0x0;
	zoom_view_length=0x0;

	xy0_en.payload=0x0;
		xy0_en.en=0x0;
		xy0_en.column_start=0x0;

	xy1_en.payload=0x0;
		xy1_en.en=0x0;
		xy1_en.column_start=0x0;

	xy2_en.payload=0x0;
		xy2_en.en=0x0;
		xy2_en.column_start=0x0;

	xy3_en.payload=0x0;
		xy3_en.en=0x0;
		xy3_en.column_start=0x0;


	init_palette_table.payload=0x0;
		init_palette_table.palette_tbl_wen=0x0;
		init_palette_table.palette_tbl_waddr=0x0;
		init_palette_table.palette_tbl_ch=0x0;

	palette_tbl_wdata=0x0;
	data_mode=0x0;
	scale_times.payload=0x0;
		scale_times.scale_times_m=0x1;
		scale_times.scale_times_n=0x0;
		scale_times.scale_times_en=0x0;


	scale_length=0x0;
	analog_ch0_zoom_offset=0x0;
	analog_ch1_zoom_offset=0x0;
	analog_ch2_zoom_offset=0x0;

	analog_ch3_zoom_offset=0x0;
	max_out.payload=0x0;
		max_out.main_max_out=0x1;
		max_out.zoom_max_out=0x0;

	view_bottom.payload=0x0;
		view_bottom.main_view_bottom=0x0;
		view_bottom.zoom_view_bottom=0x0;

	zoom_en.payload=0x0;
		zoom_en.en=0x0;


	analog_ch0_zoom_gain.payload=0x0;
		analog_ch0_zoom_gain.analog_ch0_zoom_gain=0x0;

	analog_ch1_zoom_gain.payload=0x0;
		analog_ch1_zoom_gain.analog_ch1_zoom_gain=0x0;

	analog_ch2_zoom_gain.payload=0x0;
		analog_ch2_zoom_gain.analog_ch2_zoom_gain=0x0;

	analog_ch3_zoom_gain.payload=0x0;
		analog_ch3_zoom_gain.analog_ch3_zoom_gain=0x0;


	debug_info_pm.payload=0x0;
		debug_info_pm.plot_cnt=0x0;
		debug_info_pm.local_plot_len=0x0;
		debug_info_pm.plot_phase=0x0;
		debug_info_pm.zone_buf_empty_n=0x0;

		debug_info_pm.pe_in_rdy=0x0;
		debug_info_pm.pe_out_rdy=0x0;
		debug_info_pm.plot_out_rdy=0x0;

	zoom_scale_times.payload=0x0;
		zoom_scale_times.zoom_scale_times_m=0x1;
		zoom_scale_times.zoom_scale_times_n=0x0;
		zoom_scale_times.zoom_scale_times_en=0x0;

	zoom_scale_length=0x0;
	time_base_mode=0x0;

	scan_trig_cnt.payload=0x0;
		scan_trig_cnt.scan_trig_col=0x1;
		scan_trig_cnt.scan_trig_addr=0x0;

	roll_lenth.payload=0x0;
		roll_lenth.roll_lenth_cnt=0x1;
		roll_lenth.roll_full=0x0;

}

void WpuMem::_outOtp()
{
	outinit_pe();
	outconfig_ID();
	outdisplay_en();

	outanalog_mode();
	outLA_en();
	outrefresh_time();
	outpersist_mode();

	outpersist_init();
	outpersist_dim_rate();
	outplot_mode();
	outslope_mode();

	outanalog_ch0_gain();
	outanalog_ch0_noise();
	outanalog_ch0_offset();
	outanalog_ch1_gain();

	outanalog_ch1_noise();
	outanalog_ch1_offset();
	outanalog_ch2_gain();
	outanalog_ch2_noise();

	outanalog_ch2_offset();
	outanalog_ch3_gain();
	outanalog_ch3_noise();
	outanalog_ch3_offset();

	outcolor_map();
	outview_layer();
	outtrace_layer_addr_0();
	outtrace_layer_addr_1();

	outwpu_clear();

	outinit_la_plot_table();
	outla_plot_tbl_data();

	outwave_view_start();
	outwave_view_length();
	outzoom_view_start();
	outzoom_view_length();

	outxy0_en();
	outxy1_en();
	outxy2_en();
	outxy3_en();

	outinit_palette_table();
	outpalette_tbl_wdata();
	outdata_mode();
	outscale_times();

	outscale_length();
	outanalog_ch0_zoom_offset();
	outanalog_ch1_zoom_offset();
	outanalog_ch2_zoom_offset();

	outanalog_ch3_zoom_offset();
	outmax_out();
	outview_bottom();
	outzoom_en();

	outanalog_ch0_zoom_gain();
	outanalog_ch1_zoom_gain();
	outanalog_ch2_zoom_gain();
	outanalog_ch3_zoom_gain();

	outzoom_scale_times();
	outzoom_scale_length();
	outtime_base_mode();

	outscan_trig_cnt();
}
void WpuMem::push( WpuMemProxy *proxy )
{
	if ( init_pe != proxy->init_pe )
	{reg_assign(init_pe,proxy->init_pe);}

	if ( config_ID != proxy->config_ID )
	{reg_assign(config_ID,proxy->config_ID);}

	if ( display_en != proxy->display_en )
	{reg_assign(display_en,proxy->display_en);}

	if ( analog_mode.payload != proxy->analog_mode.payload )
	{reg_assign_field(analog_mode,payload,proxy->analog_mode.payload);}

	if ( LA_en != proxy->LA_en )
	{reg_assign(LA_en,proxy->LA_en);}

	if ( refresh_time != proxy->refresh_time )
	{reg_assign(refresh_time,proxy->refresh_time);}

	if ( persist_mode != proxy->persist_mode )
	{reg_assign(persist_mode,proxy->persist_mode);}

	if ( persist_init != proxy->persist_init )
	{reg_assign(persist_init,proxy->persist_init);}

	if ( persist_dim_rate != proxy->persist_dim_rate )
	{reg_assign(persist_dim_rate,proxy->persist_dim_rate);}

	if ( plot_mode != proxy->plot_mode )
	{reg_assign(plot_mode,proxy->plot_mode);}

	if ( slope_mode != proxy->slope_mode )
	{reg_assign(slope_mode,proxy->slope_mode);}

	if ( analog_ch0_gain != proxy->analog_ch0_gain )
	{reg_assign(analog_ch0_gain,proxy->analog_ch0_gain);}

	if ( analog_ch0_noise.payload != proxy->analog_ch0_noise.payload )
	{reg_assign_field(analog_ch0_noise,payload,proxy->analog_ch0_noise.payload);}

	if ( analog_ch0_offset != proxy->analog_ch0_offset )
	{reg_assign(analog_ch0_offset,proxy->analog_ch0_offset);}

	if ( analog_ch1_gain != proxy->analog_ch1_gain )
	{reg_assign(analog_ch1_gain,proxy->analog_ch1_gain);}

	if ( analog_ch1_noise.payload != proxy->analog_ch1_noise.payload )
	{reg_assign_field(analog_ch1_noise,payload,proxy->analog_ch1_noise.payload);}

	if ( analog_ch1_offset != proxy->analog_ch1_offset )
	{reg_assign(analog_ch1_offset,proxy->analog_ch1_offset);}

	if ( analog_ch2_gain != proxy->analog_ch2_gain )
	{reg_assign(analog_ch2_gain,proxy->analog_ch2_gain);}

	if ( analog_ch2_noise.payload != proxy->analog_ch2_noise.payload )
	{reg_assign_field(analog_ch2_noise,payload,proxy->analog_ch2_noise.payload);}

	if ( analog_ch2_offset != proxy->analog_ch2_offset )
	{reg_assign(analog_ch2_offset,proxy->analog_ch2_offset);}

	if ( analog_ch3_gain != proxy->analog_ch3_gain )
	{reg_assign(analog_ch3_gain,proxy->analog_ch3_gain);}

	if ( analog_ch3_noise.payload != proxy->analog_ch3_noise.payload )
	{reg_assign_field(analog_ch3_noise,payload,proxy->analog_ch3_noise.payload);}

	if ( analog_ch3_offset != proxy->analog_ch3_offset )
	{reg_assign(analog_ch3_offset,proxy->analog_ch3_offset);}

	if ( color_map != proxy->color_map )
	{reg_assign(color_map,proxy->color_map);}

	if ( view_layer.payload != proxy->view_layer.payload )
	{reg_assign_field(view_layer,payload,proxy->view_layer.payload);}

	if ( trace_layer_addr_0 != proxy->trace_layer_addr_0 )
	{reg_assign(trace_layer_addr_0,proxy->trace_layer_addr_0);}

	if ( trace_layer_addr_1 != proxy->trace_layer_addr_1 )
	{reg_assign(trace_layer_addr_1,proxy->trace_layer_addr_1);}

	if ( wpu_clear != proxy->wpu_clear )
	{reg_assign(wpu_clear,proxy->wpu_clear);}

	if ( init_la_plot_table.payload != proxy->init_la_plot_table.payload )
	{reg_assign_field(init_la_plot_table,payload,proxy->init_la_plot_table.payload);}

	if ( la_plot_tbl_data.payload != proxy->la_plot_tbl_data.payload )
	{reg_assign_field(la_plot_tbl_data,payload,proxy->la_plot_tbl_data.payload);}

	if ( wave_view_start != proxy->wave_view_start )
	{reg_assign(wave_view_start,proxy->wave_view_start);}

	if ( wave_view_length != proxy->wave_view_length )
	{reg_assign(wave_view_length,proxy->wave_view_length);}

	if ( zoom_view_start != proxy->zoom_view_start )
	{reg_assign(zoom_view_start,proxy->zoom_view_start);}

	if ( zoom_view_length != proxy->zoom_view_length )
	{reg_assign(zoom_view_length,proxy->zoom_view_length);}

	if ( xy0_en.payload != proxy->xy0_en.payload )
	{reg_assign_field(xy0_en,payload,proxy->xy0_en.payload);}

	if ( xy1_en.payload != proxy->xy1_en.payload )
	{reg_assign_field(xy1_en,payload,proxy->xy1_en.payload);}

	if ( xy2_en.payload != proxy->xy2_en.payload )
	{reg_assign_field(xy2_en,payload,proxy->xy2_en.payload);}

	if ( xy3_en.payload != proxy->xy3_en.payload )
	{reg_assign_field(xy3_en,payload,proxy->xy3_en.payload);}

	if ( init_palette_table.payload != proxy->init_palette_table.payload )
	{reg_assign_field(init_palette_table,payload,proxy->init_palette_table.payload);}

	if ( palette_tbl_wdata != proxy->palette_tbl_wdata )
	{reg_assign(palette_tbl_wdata,proxy->palette_tbl_wdata);}

	if ( data_mode != proxy->data_mode )
	{reg_assign(data_mode,proxy->data_mode);}

	if ( scale_times.payload != proxy->scale_times.payload )
	{reg_assign_field(scale_times,payload,proxy->scale_times.payload);}

	if ( scale_length != proxy->scale_length )
	{reg_assign(scale_length,proxy->scale_length);}

	if ( analog_ch0_zoom_offset != proxy->analog_ch0_zoom_offset )
	{reg_assign(analog_ch0_zoom_offset,proxy->analog_ch0_zoom_offset);}

	if ( analog_ch1_zoom_offset != proxy->analog_ch1_zoom_offset )
	{reg_assign(analog_ch1_zoom_offset,proxy->analog_ch1_zoom_offset);}

	if ( analog_ch2_zoom_offset != proxy->analog_ch2_zoom_offset )
	{reg_assign(analog_ch2_zoom_offset,proxy->analog_ch2_zoom_offset);}

	if ( analog_ch3_zoom_offset != proxy->analog_ch3_zoom_offset )
	{reg_assign(analog_ch3_zoom_offset,proxy->analog_ch3_zoom_offset);}

	if ( max_out.payload != proxy->max_out.payload )
	{reg_assign_field(max_out,payload,proxy->max_out.payload);}

	if ( view_bottom.payload != proxy->view_bottom.payload )
	{reg_assign_field(view_bottom,payload,proxy->view_bottom.payload);}

	if ( zoom_en.payload != proxy->zoom_en.payload )
	{reg_assign_field(zoom_en,payload,proxy->zoom_en.payload);}

	if ( analog_ch0_zoom_gain.payload != proxy->analog_ch0_zoom_gain.payload )
	{reg_assign_field(analog_ch0_zoom_gain,payload,proxy->analog_ch0_zoom_gain.payload);}

	if ( analog_ch1_zoom_gain.payload != proxy->analog_ch1_zoom_gain.payload )
	{reg_assign_field(analog_ch1_zoom_gain,payload,proxy->analog_ch1_zoom_gain.payload);}

	if ( analog_ch2_zoom_gain.payload != proxy->analog_ch2_zoom_gain.payload )
	{reg_assign_field(analog_ch2_zoom_gain,payload,proxy->analog_ch2_zoom_gain.payload);}

	if ( analog_ch3_zoom_gain.payload != proxy->analog_ch3_zoom_gain.payload )
	{reg_assign_field(analog_ch3_zoom_gain,payload,proxy->analog_ch3_zoom_gain.payload);}

	if ( zoom_scale_times.payload != proxy->zoom_scale_times.payload )
	{reg_assign_field(zoom_scale_times,payload,proxy->zoom_scale_times.payload);}

	if ( zoom_scale_length != proxy->zoom_scale_length )
	{reg_assign(zoom_scale_length,proxy->zoom_scale_length);}

	if ( time_base_mode != proxy->time_base_mode )
	{reg_assign(time_base_mode,proxy->time_base_mode);}

	if ( scan_trig_cnt.payload != proxy->scan_trig_cnt.payload )
	{reg_assign_field(scan_trig_cnt,payload,proxy->scan_trig_cnt.payload);}

}
void WpuMem::pull( WpuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(version);
	reg_in(config_ID);
	reg_in(display_en);
	reg_in(analog_mode);
	reg_in(LA_en);
	reg_in(refresh_time);
	reg_in(persist_mode);
	reg_in(persist_init);
	reg_in(persist_dim_rate);
	reg_in(plot_mode);
	reg_in(slope_mode);
	reg_in(analog_ch0_gain);
	reg_in(analog_ch0_noise);
	reg_in(analog_ch0_offset);
	reg_in(analog_ch1_gain);
	reg_in(analog_ch1_noise);
	reg_in(analog_ch1_offset);
	reg_in(analog_ch2_gain);
	reg_in(analog_ch2_noise);
	reg_in(analog_ch2_offset);
	reg_in(analog_ch3_gain);
	reg_in(analog_ch3_noise);
	reg_in(analog_ch3_offset);
	reg_in(color_map);
	reg_in(view_layer);
	reg_in(trace_layer_addr_0);
	reg_in(trace_layer_addr_1);
	reg_in(srn_num);
	reg_in(wpu_status);
	reg_in(mem_status);
	reg_in(init_status);
	reg_in(load_config_id);
	reg_in(init_la_plot_table);
	reg_in(la_plot_tbl_data);
	reg_in(wave_view_start);
	reg_in(wave_view_length);
	reg_in(zoom_view_start);
	reg_in(zoom_view_length);
	reg_in(xy0_en);
	reg_in(xy1_en);
	reg_in(xy2_en);
	reg_in(xy3_en);
	reg_in(palette_tbl_wdata);
	reg_in(data_mode);
	reg_in(scale_times);
	reg_in(scale_length);
	reg_in(analog_ch0_zoom_offset);
	reg_in(analog_ch1_zoom_offset);
	reg_in(analog_ch2_zoom_offset);
	reg_in(analog_ch3_zoom_offset);
	reg_in(max_out);
	reg_in(view_bottom);
	reg_in(zoom_en);
	reg_in(analog_ch0_zoom_gain);
	reg_in(analog_ch1_zoom_gain);
	reg_in(analog_ch2_zoom_gain);
	reg_in(analog_ch3_zoom_gain);
	reg_in(debug_info_pm);
	reg_in(zoom_scale_times);
	reg_in(zoom_scale_length);
	reg_in(time_base_mode);
	reg_in(scan_trig_cnt);
	reg_in(roll_lenth);

	*proxy = WpuMemProxy(*this);
}
Wpu_reg WpuMem::inversion()
{
	reg_in(version);
	return version;
}


void WpuMem::setinit_pe( Wpu_reg value )
{
	reg_assign(init_pe,value);
}

void WpuMem::outinit_pe( Wpu_reg value )
{
	out_assign(init_pe,value);
}

void WpuMem::outinit_pe()
{
	out_member(init_pe);
}


void WpuMem::setconfig_ID( Wpu_reg value )
{
	reg_assign(config_ID,value);
}

void WpuMem::outconfig_ID( Wpu_reg value )
{
	out_assign(config_ID,value);
}

void WpuMem::outconfig_ID()
{
	out_member(config_ID);
}


Wpu_reg WpuMem::inconfig_ID()
{
	reg_in(config_ID);
	return config_ID;
}


void WpuMem::setdisplay_en( Wpu_reg value )
{
	reg_assign(display_en,value);
}

void WpuMem::outdisplay_en( Wpu_reg value )
{
	out_assign(display_en,value);
}

void WpuMem::outdisplay_en()
{
	out_member(display_en);
}


Wpu_reg WpuMem::indisplay_en()
{
	reg_in(display_en);
	return display_en;
}


void WpuMem::setanalog_mode_analog_ch_mode( Wpu_reg value )
{
	reg_assign_field(analog_mode,analog_ch_mode,value);
}
void WpuMem::setanalog_mode_analog_ch_en( Wpu_reg value )
{
	reg_assign_field(analog_mode,analog_ch_en,value);
}

void WpuMem::setanalog_mode( Wpu_reg value )
{
	reg_assign_field(analog_mode,payload,value);
}

void WpuMem::outanalog_mode_analog_ch_mode( Wpu_reg value )
{
	out_assign_field(analog_mode,analog_ch_mode,value);
}
void WpuMem::pulseanalog_mode_analog_ch_mode( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_mode,analog_ch_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_mode,analog_ch_mode,idleV);
}
void WpuMem::outanalog_mode_analog_ch_en( Wpu_reg value )
{
	out_assign_field(analog_mode,analog_ch_en,value);
}
void WpuMem::pulseanalog_mode_analog_ch_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_mode,analog_ch_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_mode,analog_ch_en,idleV);
}

void WpuMem::outanalog_mode( Wpu_reg value )
{
	out_assign_field(analog_mode,payload,value);
}

void WpuMem::outanalog_mode()
{
	out_member(analog_mode);
}


Wpu_reg WpuMem::inanalog_mode()
{
	reg_in(analog_mode);
	return analog_mode.payload;
}


void WpuMem::setLA_en( Wpu_reg value )
{
	reg_assign(LA_en,value);
}

void WpuMem::outLA_en( Wpu_reg value )
{
	out_assign(LA_en,value);
}

void WpuMem::outLA_en()
{
	out_member(LA_en);
}


Wpu_reg WpuMem::inLA_en()
{
	reg_in(LA_en);
	return LA_en;
}


void WpuMem::setrefresh_time( Wpu_reg value )
{
	reg_assign(refresh_time,value);
}

void WpuMem::outrefresh_time( Wpu_reg value )
{
	out_assign(refresh_time,value);
}

void WpuMem::outrefresh_time()
{
	out_member(refresh_time);
}


Wpu_reg WpuMem::inrefresh_time()
{
	reg_in(refresh_time);
	return refresh_time;
}


void WpuMem::setpersist_mode( Wpu_reg value )
{
	reg_assign(persist_mode,value);
}

void WpuMem::outpersist_mode( Wpu_reg value )
{
	out_assign(persist_mode,value);
}

void WpuMem::outpersist_mode()
{
	out_member(persist_mode);
}


Wpu_reg WpuMem::inpersist_mode()
{
	reg_in(persist_mode);
	return persist_mode;
}


void WpuMem::setpersist_init( Wpu_reg value )
{
	reg_assign(persist_init,value);
}

void WpuMem::outpersist_init( Wpu_reg value )
{
	out_assign(persist_init,value);
}

void WpuMem::outpersist_init()
{
	out_member(persist_init);
}


Wpu_reg WpuMem::inpersist_init()
{
	reg_in(persist_init);
	return persist_init;
}


void WpuMem::setpersist_dim_rate( Wpu_reg value )
{
	reg_assign(persist_dim_rate,value);
}

void WpuMem::outpersist_dim_rate( Wpu_reg value )
{
	out_assign(persist_dim_rate,value);
}

void WpuMem::outpersist_dim_rate()
{
	out_member(persist_dim_rate);
}


Wpu_reg WpuMem::inpersist_dim_rate()
{
	reg_in(persist_dim_rate);
	return persist_dim_rate;
}


void WpuMem::setplot_mode( Wpu_reg value )
{
	reg_assign(plot_mode,value);
}

void WpuMem::outplot_mode( Wpu_reg value )
{
	out_assign(plot_mode,value);
}

void WpuMem::outplot_mode()
{
	out_member(plot_mode);
}


Wpu_reg WpuMem::inplot_mode()
{
	reg_in(plot_mode);
	return plot_mode;
}


void WpuMem::setslope_mode( Wpu_reg value )
{
	reg_assign(slope_mode,value);
}

void WpuMem::outslope_mode( Wpu_reg value )
{
	out_assign(slope_mode,value);
}

void WpuMem::outslope_mode()
{
	out_member(slope_mode);
}


Wpu_reg WpuMem::inslope_mode()
{
	reg_in(slope_mode);
	return slope_mode;
}


void WpuMem::setanalog_ch0_gain( Wpu_reg value )
{
	reg_assign(analog_ch0_gain,value);
}

void WpuMem::outanalog_ch0_gain( Wpu_reg value )
{
	out_assign(analog_ch0_gain,value);
}

void WpuMem::outanalog_ch0_gain()
{
	out_member(analog_ch0_gain);
}


Wpu_reg WpuMem::inanalog_ch0_gain()
{
	reg_in(analog_ch0_gain);
	return analog_ch0_gain;
}


void WpuMem::setanalog_ch0_noise_ch0_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch0_noise,ch0_noise_grade,value);
}
void WpuMem::setanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch0_noise,ch0_zoom_noise_grade,value);
}

void WpuMem::setanalog_ch0_noise( Wpu_reg value )
{
	reg_assign_field(analog_ch0_noise,payload,value);
}

void WpuMem::outanalog_ch0_noise_ch0_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch0_noise,ch0_noise_grade,value);
}
void WpuMem::pulseanalog_ch0_noise_ch0_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch0_noise,ch0_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch0_noise,ch0_noise_grade,idleV);
}
void WpuMem::outanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch0_noise,ch0_zoom_noise_grade,value);
}
void WpuMem::pulseanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch0_noise,ch0_zoom_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch0_noise,ch0_zoom_noise_grade,idleV);
}

void WpuMem::outanalog_ch0_noise( Wpu_reg value )
{
	out_assign_field(analog_ch0_noise,payload,value);
}

void WpuMem::outanalog_ch0_noise()
{
	out_member(analog_ch0_noise);
}


Wpu_reg WpuMem::inanalog_ch0_noise()
{
	reg_in(analog_ch0_noise);
	return analog_ch0_noise.payload;
}


void WpuMem::setanalog_ch0_offset( Wpu_reg value )
{
	reg_assign(analog_ch0_offset,value);
}

void WpuMem::outanalog_ch0_offset( Wpu_reg value )
{
	out_assign(analog_ch0_offset,value);
}

void WpuMem::outanalog_ch0_offset()
{
	out_member(analog_ch0_offset);
}


Wpu_reg WpuMem::inanalog_ch0_offset()
{
	reg_in(analog_ch0_offset);
	return analog_ch0_offset;
}


void WpuMem::setanalog_ch1_gain( Wpu_reg value )
{
	reg_assign(analog_ch1_gain,value);
}

void WpuMem::outanalog_ch1_gain( Wpu_reg value )
{
	out_assign(analog_ch1_gain,value);
}

void WpuMem::outanalog_ch1_gain()
{
	out_member(analog_ch1_gain);
}


Wpu_reg WpuMem::inanalog_ch1_gain()
{
	reg_in(analog_ch1_gain);
	return analog_ch1_gain;
}


void WpuMem::setanalog_ch1_noise_ch1_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch1_noise,ch1_noise_grade,value);
}
void WpuMem::setanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch1_noise,ch1_zoom_noise_grade,value);
}

void WpuMem::setanalog_ch1_noise( Wpu_reg value )
{
	reg_assign_field(analog_ch1_noise,payload,value);
}

void WpuMem::outanalog_ch1_noise_ch1_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch1_noise,ch1_noise_grade,value);
}
void WpuMem::pulseanalog_ch1_noise_ch1_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch1_noise,ch1_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch1_noise,ch1_noise_grade,idleV);
}
void WpuMem::outanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch1_noise,ch1_zoom_noise_grade,value);
}
void WpuMem::pulseanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch1_noise,ch1_zoom_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch1_noise,ch1_zoom_noise_grade,idleV);
}

void WpuMem::outanalog_ch1_noise( Wpu_reg value )
{
	out_assign_field(analog_ch1_noise,payload,value);
}

void WpuMem::outanalog_ch1_noise()
{
	out_member(analog_ch1_noise);
}


Wpu_reg WpuMem::inanalog_ch1_noise()
{
	reg_in(analog_ch1_noise);
	return analog_ch1_noise.payload;
}


void WpuMem::setanalog_ch1_offset( Wpu_reg value )
{
	reg_assign(analog_ch1_offset,value);
}

void WpuMem::outanalog_ch1_offset( Wpu_reg value )
{
	out_assign(analog_ch1_offset,value);
}

void WpuMem::outanalog_ch1_offset()
{
	out_member(analog_ch1_offset);
}


Wpu_reg WpuMem::inanalog_ch1_offset()
{
	reg_in(analog_ch1_offset);
	return analog_ch1_offset;
}


void WpuMem::setanalog_ch2_gain( Wpu_reg value )
{
	reg_assign(analog_ch2_gain,value);
}

void WpuMem::outanalog_ch2_gain( Wpu_reg value )
{
	out_assign(analog_ch2_gain,value);
}

void WpuMem::outanalog_ch2_gain()
{
	out_member(analog_ch2_gain);
}


Wpu_reg WpuMem::inanalog_ch2_gain()
{
	reg_in(analog_ch2_gain);
	return analog_ch2_gain;
}


void WpuMem::setanalog_ch2_noise_ch2_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch2_noise,ch2_noise_grade,value);
}
void WpuMem::setanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch2_noise,ch2_zoom_noise_grade,value);
}

void WpuMem::setanalog_ch2_noise( Wpu_reg value )
{
	reg_assign_field(analog_ch2_noise,payload,value);
}

void WpuMem::outanalog_ch2_noise_ch2_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch2_noise,ch2_noise_grade,value);
}
void WpuMem::pulseanalog_ch2_noise_ch2_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch2_noise,ch2_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch2_noise,ch2_noise_grade,idleV);
}
void WpuMem::outanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch2_noise,ch2_zoom_noise_grade,value);
}
void WpuMem::pulseanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch2_noise,ch2_zoom_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch2_noise,ch2_zoom_noise_grade,idleV);
}

void WpuMem::outanalog_ch2_noise( Wpu_reg value )
{
	out_assign_field(analog_ch2_noise,payload,value);
}

void WpuMem::outanalog_ch2_noise()
{
	out_member(analog_ch2_noise);
}


Wpu_reg WpuMem::inanalog_ch2_noise()
{
	reg_in(analog_ch2_noise);
	return analog_ch2_noise.payload;
}


void WpuMem::setanalog_ch2_offset( Wpu_reg value )
{
	reg_assign(analog_ch2_offset,value);
}

void WpuMem::outanalog_ch2_offset( Wpu_reg value )
{
	out_assign(analog_ch2_offset,value);
}

void WpuMem::outanalog_ch2_offset()
{
	out_member(analog_ch2_offset);
}


Wpu_reg WpuMem::inanalog_ch2_offset()
{
	reg_in(analog_ch2_offset);
	return analog_ch2_offset;
}


void WpuMem::setanalog_ch3_gain( Wpu_reg value )
{
	reg_assign(analog_ch3_gain,value);
}

void WpuMem::outanalog_ch3_gain( Wpu_reg value )
{
	out_assign(analog_ch3_gain,value);
}

void WpuMem::outanalog_ch3_gain()
{
	out_member(analog_ch3_gain);
}


Wpu_reg WpuMem::inanalog_ch3_gain()
{
	reg_in(analog_ch3_gain);
	return analog_ch3_gain;
}


void WpuMem::setanalog_ch3_noise_ch3_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch3_noise,ch3_noise_grade,value);
}
void WpuMem::setanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg value )
{
	reg_assign_field(analog_ch3_noise,ch3_zoom_noise_grade,value);
}

void WpuMem::setanalog_ch3_noise( Wpu_reg value )
{
	reg_assign_field(analog_ch3_noise,payload,value);
}

void WpuMem::outanalog_ch3_noise_ch3_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch3_noise,ch3_noise_grade,value);
}
void WpuMem::pulseanalog_ch3_noise_ch3_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch3_noise,ch3_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch3_noise,ch3_noise_grade,idleV);
}
void WpuMem::outanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg value )
{
	out_assign_field(analog_ch3_noise,ch3_zoom_noise_grade,value);
}
void WpuMem::pulseanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch3_noise,ch3_zoom_noise_grade,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch3_noise,ch3_zoom_noise_grade,idleV);
}

void WpuMem::outanalog_ch3_noise( Wpu_reg value )
{
	out_assign_field(analog_ch3_noise,payload,value);
}

void WpuMem::outanalog_ch3_noise()
{
	out_member(analog_ch3_noise);
}


Wpu_reg WpuMem::inanalog_ch3_noise()
{
	reg_in(analog_ch3_noise);
	return analog_ch3_noise.payload;
}


void WpuMem::setanalog_ch3_offset( Wpu_reg value )
{
	reg_assign(analog_ch3_offset,value);
}

void WpuMem::outanalog_ch3_offset( Wpu_reg value )
{
	out_assign(analog_ch3_offset,value);
}

void WpuMem::outanalog_ch3_offset()
{
	out_member(analog_ch3_offset);
}


Wpu_reg WpuMem::inanalog_ch3_offset()
{
	reg_in(analog_ch3_offset);
	return analog_ch3_offset;
}


void WpuMem::setcolor_map( Wpu_reg value )
{
	reg_assign(color_map,value);
}

void WpuMem::outcolor_map( Wpu_reg value )
{
	out_assign(color_map,value);
}

void WpuMem::outcolor_map()
{
	out_member(color_map);
}


Wpu_reg WpuMem::incolor_map()
{
	reg_in(color_map);
	return color_map;
}


void WpuMem::setview_layer_group_order( Wpu_reg value )
{
	reg_assign_field(view_layer,group_order,value);
}
void WpuMem::setview_layer_analog_order( Wpu_reg value )
{
	reg_assign_field(view_layer,analog_order,value);
}
void WpuMem::setview_layer_math_order( Wpu_reg value )
{
	reg_assign_field(view_layer,math_order,value);
}

void WpuMem::setview_layer( Wpu_reg value )
{
	reg_assign_field(view_layer,payload,value);
}

void WpuMem::outview_layer_group_order( Wpu_reg value )
{
	out_assign_field(view_layer,group_order,value);
}
void WpuMem::pulseview_layer_group_order( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(view_layer,group_order,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(view_layer,group_order,idleV);
}
void WpuMem::outview_layer_analog_order( Wpu_reg value )
{
	out_assign_field(view_layer,analog_order,value);
}
void WpuMem::pulseview_layer_analog_order( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(view_layer,analog_order,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(view_layer,analog_order,idleV);
}
void WpuMem::outview_layer_math_order( Wpu_reg value )
{
	out_assign_field(view_layer,math_order,value);
}
void WpuMem::pulseview_layer_math_order( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(view_layer,math_order,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(view_layer,math_order,idleV);
}

void WpuMem::outview_layer( Wpu_reg value )
{
	out_assign_field(view_layer,payload,value);
}

void WpuMem::outview_layer()
{
	out_member(view_layer);
}


Wpu_reg WpuMem::inview_layer()
{
	reg_in(view_layer);
	return view_layer.payload;
}


void WpuMem::settrace_layer_addr_0( Wpu_reg value )
{
	reg_assign(trace_layer_addr_0,value);
}

void WpuMem::outtrace_layer_addr_0( Wpu_reg value )
{
	out_assign(trace_layer_addr_0,value);
}

void WpuMem::outtrace_layer_addr_0()
{
	out_member(trace_layer_addr_0);
}


Wpu_reg WpuMem::intrace_layer_addr_0()
{
	reg_in(trace_layer_addr_0);
	return trace_layer_addr_0;
}


void WpuMem::settrace_layer_addr_1( Wpu_reg value )
{
	reg_assign(trace_layer_addr_1,value);
}

void WpuMem::outtrace_layer_addr_1( Wpu_reg value )
{
	out_assign(trace_layer_addr_1,value);
}

void WpuMem::outtrace_layer_addr_1()
{
	out_member(trace_layer_addr_1);
}


Wpu_reg WpuMem::intrace_layer_addr_1()
{
	reg_in(trace_layer_addr_1);
	return trace_layer_addr_1;
}


void WpuMem::setwpu_clear( Wpu_reg value )
{
	reg_assign(wpu_clear,value);
}

void WpuMem::outwpu_clear( Wpu_reg value )
{
	out_assign(wpu_clear,value);
}

void WpuMem::outwpu_clear()
{
	out_member(wpu_clear);
}


Wpu_reg WpuMem::insrn_num()
{
	reg_in(srn_num);
	return srn_num;
}


Wpu_reg WpuMem::inwpu_status()
{
	reg_in(wpu_status);
	return wpu_status;
}


Wpu_reg WpuMem::inmem_status()
{
	reg_in(mem_status);
	return mem_status;
}


Wpu_reg WpuMem::ininit_status()
{
	reg_in(init_status);
	return init_status;
}


Wpu_reg WpuMem::inload_config_id()
{
	reg_in(load_config_id);
	return load_config_id;
}


void WpuMem::setinit_la_plot_table_la_plot_tbl_wen( Wpu_reg value )
{
	reg_assign_field(init_la_plot_table,la_plot_tbl_wen,value);
}
void WpuMem::setinit_la_plot_table_la_plot_tbl_waddr( Wpu_reg value )
{
	reg_assign_field(init_la_plot_table,la_plot_tbl_waddr,value);
}

void WpuMem::setinit_la_plot_table( Wpu_reg value )
{
	reg_assign_field(init_la_plot_table,payload,value);
}

void WpuMem::outinit_la_plot_table_la_plot_tbl_wen( Wpu_reg value )
{
	out_assign_field(init_la_plot_table,la_plot_tbl_wen,value);
}
void WpuMem::pulseinit_la_plot_table_la_plot_tbl_wen( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(init_la_plot_table,la_plot_tbl_wen,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(init_la_plot_table,la_plot_tbl_wen,idleV);
}
void WpuMem::outinit_la_plot_table_la_plot_tbl_waddr( Wpu_reg value )
{
	out_assign_field(init_la_plot_table,la_plot_tbl_waddr,value);
}
void WpuMem::pulseinit_la_plot_table_la_plot_tbl_waddr( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(init_la_plot_table,la_plot_tbl_waddr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(init_la_plot_table,la_plot_tbl_waddr,idleV);
}

void WpuMem::outinit_la_plot_table( Wpu_reg value )
{
	out_assign_field(init_la_plot_table,payload,value);
}

void WpuMem::outinit_la_plot_table()
{
	out_member(init_la_plot_table);
}


Wpu_reg WpuMem::ininit_la_plot_table()
{
	reg_in(init_la_plot_table);
	return init_la_plot_table.payload;
}


void WpuMem::setla_plot_tbl_data_COLOR( Wpu_reg value )
{
	reg_assign_field(la_plot_tbl_data,COLOR,value);
}
void WpuMem::setla_plot_tbl_data_POSITION( Wpu_reg value )
{
	reg_assign_field(la_plot_tbl_data,POSITION,value);
}
void WpuMem::setla_plot_tbl_data_reserved( Wpu_reg value )
{
	reg_assign_field(la_plot_tbl_data,reserved,value);
}
void WpuMem::setla_plot_tbl_data_zoom_chn_flag( Wpu_reg value )
{
	reg_assign_field(la_plot_tbl_data,zoom_chn_flag,value);
}
void WpuMem::setla_plot_tbl_data_CH_NO( Wpu_reg value )
{
	reg_assign_field(la_plot_tbl_data,CH_NO,value);
}

void WpuMem::setla_plot_tbl_data( Wpu_reg value )
{
	reg_assign_field(la_plot_tbl_data,payload,value);
}

void WpuMem::outla_plot_tbl_data_COLOR( Wpu_reg value )
{
	out_assign_field(la_plot_tbl_data,COLOR,value);
}
void WpuMem::pulsela_plot_tbl_data_COLOR( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(la_plot_tbl_data,COLOR,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(la_plot_tbl_data,COLOR,idleV);
}
void WpuMem::outla_plot_tbl_data_POSITION( Wpu_reg value )
{
	out_assign_field(la_plot_tbl_data,POSITION,value);
}
void WpuMem::pulsela_plot_tbl_data_POSITION( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(la_plot_tbl_data,POSITION,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(la_plot_tbl_data,POSITION,idleV);
}
void WpuMem::outla_plot_tbl_data_reserved( Wpu_reg value )
{
	out_assign_field(la_plot_tbl_data,reserved,value);
}
void WpuMem::pulsela_plot_tbl_data_reserved( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(la_plot_tbl_data,reserved,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(la_plot_tbl_data,reserved,idleV);
}
void WpuMem::outla_plot_tbl_data_zoom_chn_flag( Wpu_reg value )
{
	out_assign_field(la_plot_tbl_data,zoom_chn_flag,value);
}
void WpuMem::pulsela_plot_tbl_data_zoom_chn_flag( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(la_plot_tbl_data,zoom_chn_flag,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(la_plot_tbl_data,zoom_chn_flag,idleV);
}
void WpuMem::outla_plot_tbl_data_CH_NO( Wpu_reg value )
{
	out_assign_field(la_plot_tbl_data,CH_NO,value);
}
void WpuMem::pulsela_plot_tbl_data_CH_NO( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(la_plot_tbl_data,CH_NO,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(la_plot_tbl_data,CH_NO,idleV);
}

void WpuMem::outla_plot_tbl_data( Wpu_reg value )
{
	out_assign_field(la_plot_tbl_data,payload,value);
}

void WpuMem::outla_plot_tbl_data()
{
	out_member(la_plot_tbl_data);
}


Wpu_reg WpuMem::inla_plot_tbl_data()
{
	reg_in(la_plot_tbl_data);
	return la_plot_tbl_data.payload;
}


void WpuMem::setwave_view_start( Wpu_reg value )
{
	reg_assign(wave_view_start,value);
}

void WpuMem::outwave_view_start( Wpu_reg value )
{
	out_assign(wave_view_start,value);
}

void WpuMem::outwave_view_start()
{
	out_member(wave_view_start);
}


Wpu_reg WpuMem::inwave_view_start()
{
	reg_in(wave_view_start);
	return wave_view_start;
}


void WpuMem::setwave_view_length( Wpu_reg value )
{
	reg_assign(wave_view_length,value);
}

void WpuMem::outwave_view_length( Wpu_reg value )
{
	out_assign(wave_view_length,value);
}

void WpuMem::outwave_view_length()
{
	out_member(wave_view_length);
}


Wpu_reg WpuMem::inwave_view_length()
{
	reg_in(wave_view_length);
	return wave_view_length;
}


void WpuMem::setzoom_view_start( Wpu_reg value )
{
	reg_assign(zoom_view_start,value);
}

void WpuMem::outzoom_view_start( Wpu_reg value )
{
	out_assign(zoom_view_start,value);
}

void WpuMem::outzoom_view_start()
{
	out_member(zoom_view_start);
}


Wpu_reg WpuMem::inzoom_view_start()
{
	reg_in(zoom_view_start);
	return zoom_view_start;
}


void WpuMem::setzoom_view_length( Wpu_reg value )
{
	reg_assign(zoom_view_length,value);
}

void WpuMem::outzoom_view_length( Wpu_reg value )
{
	out_assign(zoom_view_length,value);
}

void WpuMem::outzoom_view_length()
{
	out_member(zoom_view_length);
}


Wpu_reg WpuMem::inzoom_view_length()
{
	reg_in(zoom_view_length);
	return zoom_view_length;
}


void WpuMem::setxy0_en_en( Wpu_reg value )
{
	reg_assign_field(xy0_en,en,value);
}
void WpuMem::setxy0_en_column_start( Wpu_reg value )
{
	reg_assign_field(xy0_en,column_start,value);
}

void WpuMem::setxy0_en( Wpu_reg value )
{
	reg_assign_field(xy0_en,payload,value);
}

void WpuMem::outxy0_en_en( Wpu_reg value )
{
	out_assign_field(xy0_en,en,value);
}
void WpuMem::pulsexy0_en_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy0_en,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy0_en,en,idleV);
}
void WpuMem::outxy0_en_column_start( Wpu_reg value )
{
	out_assign_field(xy0_en,column_start,value);
}
void WpuMem::pulsexy0_en_column_start( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy0_en,column_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy0_en,column_start,idleV);
}

void WpuMem::outxy0_en( Wpu_reg value )
{
	out_assign_field(xy0_en,payload,value);
}

void WpuMem::outxy0_en()
{
	out_member(xy0_en);
}


Wpu_reg WpuMem::inxy0_en()
{
	reg_in(xy0_en);
	return xy0_en.payload;
}


void WpuMem::setxy1_en_en( Wpu_reg value )
{
	reg_assign_field(xy1_en,en,value);
}
void WpuMem::setxy1_en_column_start( Wpu_reg value )
{
	reg_assign_field(xy1_en,column_start,value);
}

void WpuMem::setxy1_en( Wpu_reg value )
{
	reg_assign_field(xy1_en,payload,value);
}

void WpuMem::outxy1_en_en( Wpu_reg value )
{
	out_assign_field(xy1_en,en,value);
}
void WpuMem::pulsexy1_en_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy1_en,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy1_en,en,idleV);
}
void WpuMem::outxy1_en_column_start( Wpu_reg value )
{
	out_assign_field(xy1_en,column_start,value);
}
void WpuMem::pulsexy1_en_column_start( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy1_en,column_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy1_en,column_start,idleV);
}

void WpuMem::outxy1_en( Wpu_reg value )
{
	out_assign_field(xy1_en,payload,value);
}

void WpuMem::outxy1_en()
{
	out_member(xy1_en);
}


Wpu_reg WpuMem::inxy1_en()
{
	reg_in(xy1_en);
	return xy1_en.payload;
}


void WpuMem::setxy2_en_en( Wpu_reg value )
{
	reg_assign_field(xy2_en,en,value);
}
void WpuMem::setxy2_en_column_start( Wpu_reg value )
{
	reg_assign_field(xy2_en,column_start,value);
}

void WpuMem::setxy2_en( Wpu_reg value )
{
	reg_assign_field(xy2_en,payload,value);
}

void WpuMem::outxy2_en_en( Wpu_reg value )
{
	out_assign_field(xy2_en,en,value);
}
void WpuMem::pulsexy2_en_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy2_en,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy2_en,en,idleV);
}
void WpuMem::outxy2_en_column_start( Wpu_reg value )
{
	out_assign_field(xy2_en,column_start,value);
}
void WpuMem::pulsexy2_en_column_start( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy2_en,column_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy2_en,column_start,idleV);
}

void WpuMem::outxy2_en( Wpu_reg value )
{
	out_assign_field(xy2_en,payload,value);
}

void WpuMem::outxy2_en()
{
	out_member(xy2_en);
}


Wpu_reg WpuMem::inxy2_en()
{
	reg_in(xy2_en);
	return xy2_en.payload;
}


void WpuMem::setxy3_en_en( Wpu_reg value )
{
	reg_assign_field(xy3_en,en,value);
}
void WpuMem::setxy3_en_column_start( Wpu_reg value )
{
	reg_assign_field(xy3_en,column_start,value);
}

void WpuMem::setxy3_en( Wpu_reg value )
{
	reg_assign_field(xy3_en,payload,value);
}

void WpuMem::outxy3_en_en( Wpu_reg value )
{
	out_assign_field(xy3_en,en,value);
}
void WpuMem::pulsexy3_en_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy3_en,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy3_en,en,idleV);
}
void WpuMem::outxy3_en_column_start( Wpu_reg value )
{
	out_assign_field(xy3_en,column_start,value);
}
void WpuMem::pulsexy3_en_column_start( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(xy3_en,column_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(xy3_en,column_start,idleV);
}

void WpuMem::outxy3_en( Wpu_reg value )
{
	out_assign_field(xy3_en,payload,value);
}

void WpuMem::outxy3_en()
{
	out_member(xy3_en);
}


Wpu_reg WpuMem::inxy3_en()
{
	reg_in(xy3_en);
	return xy3_en.payload;
}


void WpuMem::setinit_palette_table_palette_tbl_wen( Wpu_reg value )
{
	reg_assign_field(init_palette_table,palette_tbl_wen,value);
}
void WpuMem::setinit_palette_table_palette_tbl_waddr( Wpu_reg value )
{
	reg_assign_field(init_palette_table,palette_tbl_waddr,value);
}
void WpuMem::setinit_palette_table_palette_tbl_ch( Wpu_reg value )
{
	reg_assign_field(init_palette_table,palette_tbl_ch,value);
}

void WpuMem::setinit_palette_table( Wpu_reg value )
{
	reg_assign_field(init_palette_table,payload,value);
}

void WpuMem::outinit_palette_table_palette_tbl_wen( Wpu_reg value )
{
	out_assign_field(init_palette_table,palette_tbl_wen,value);
}
void WpuMem::pulseinit_palette_table_palette_tbl_wen( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(init_palette_table,palette_tbl_wen,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(init_palette_table,palette_tbl_wen,idleV);
}
void WpuMem::outinit_palette_table_palette_tbl_waddr( Wpu_reg value )
{
	out_assign_field(init_palette_table,palette_tbl_waddr,value);
}
void WpuMem::pulseinit_palette_table_palette_tbl_waddr( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(init_palette_table,palette_tbl_waddr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(init_palette_table,palette_tbl_waddr,idleV);
}
void WpuMem::outinit_palette_table_palette_tbl_ch( Wpu_reg value )
{
	out_assign_field(init_palette_table,palette_tbl_ch,value);
}
void WpuMem::pulseinit_palette_table_palette_tbl_ch( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(init_palette_table,palette_tbl_ch,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(init_palette_table,palette_tbl_ch,idleV);
}

void WpuMem::outinit_palette_table( Wpu_reg value )
{
	out_assign_field(init_palette_table,payload,value);
}

void WpuMem::outinit_palette_table()
{
	out_member(init_palette_table);
}


void WpuMem::setpalette_tbl_wdata( Wpu_reg value )
{
	reg_assign(palette_tbl_wdata,value);
}

void WpuMem::outpalette_tbl_wdata( Wpu_reg value )
{
	out_assign(palette_tbl_wdata,value);
}

void WpuMem::outpalette_tbl_wdata()
{
	out_member(palette_tbl_wdata);
}


Wpu_reg WpuMem::inpalette_tbl_wdata()
{
	reg_in(palette_tbl_wdata);
	return palette_tbl_wdata;
}


void WpuMem::setdata_mode( Wpu_reg value )
{
	reg_assign(data_mode,value);
}

void WpuMem::outdata_mode( Wpu_reg value )
{
	out_assign(data_mode,value);
}

void WpuMem::outdata_mode()
{
	out_member(data_mode);
}


Wpu_reg WpuMem::indata_mode()
{
	reg_in(data_mode);
	return data_mode;
}


void WpuMem::setscale_times_scale_times_m( Wpu_reg value )
{
	reg_assign_field(scale_times,scale_times_m,value);
}
void WpuMem::setscale_times_scale_times_n( Wpu_reg value )
{
	reg_assign_field(scale_times,scale_times_n,value);
}
void WpuMem::setscale_times_scale_times_en( Wpu_reg value )
{
	reg_assign_field(scale_times,scale_times_en,value);
}

void WpuMem::setscale_times( Wpu_reg value )
{
	reg_assign_field(scale_times,payload,value);
}

void WpuMem::outscale_times_scale_times_m( Wpu_reg value )
{
	out_assign_field(scale_times,scale_times_m,value);
}
void WpuMem::pulsescale_times_scale_times_m( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(scale_times,scale_times_m,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(scale_times,scale_times_m,idleV);
}
void WpuMem::outscale_times_scale_times_n( Wpu_reg value )
{
	out_assign_field(scale_times,scale_times_n,value);
}
void WpuMem::pulsescale_times_scale_times_n( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(scale_times,scale_times_n,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(scale_times,scale_times_n,idleV);
}
void WpuMem::outscale_times_scale_times_en( Wpu_reg value )
{
	out_assign_field(scale_times,scale_times_en,value);
}
void WpuMem::pulsescale_times_scale_times_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(scale_times,scale_times_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(scale_times,scale_times_en,idleV);
}

void WpuMem::outscale_times( Wpu_reg value )
{
	out_assign_field(scale_times,payload,value);
}

void WpuMem::outscale_times()
{
	out_member(scale_times);
}


Wpu_reg WpuMem::inscale_times()
{
	reg_in(scale_times);
	return scale_times.payload;
}


void WpuMem::setscale_length( Wpu_reg value )
{
	reg_assign(scale_length,value);
}

void WpuMem::outscale_length( Wpu_reg value )
{
	out_assign(scale_length,value);
}

void WpuMem::outscale_length()
{
	out_member(scale_length);
}


Wpu_reg WpuMem::inscale_length()
{
	reg_in(scale_length);
	return scale_length;
}


void WpuMem::setanalog_ch0_zoom_offset( Wpu_reg value )
{
	reg_assign(analog_ch0_zoom_offset,value);
}

void WpuMem::outanalog_ch0_zoom_offset( Wpu_reg value )
{
	out_assign(analog_ch0_zoom_offset,value);
}

void WpuMem::outanalog_ch0_zoom_offset()
{
	out_member(analog_ch0_zoom_offset);
}


Wpu_reg WpuMem::inanalog_ch0_zoom_offset()
{
	reg_in(analog_ch0_zoom_offset);
	return analog_ch0_zoom_offset;
}


void WpuMem::setanalog_ch1_zoom_offset( Wpu_reg value )
{
	reg_assign(analog_ch1_zoom_offset,value);
}

void WpuMem::outanalog_ch1_zoom_offset( Wpu_reg value )
{
	out_assign(analog_ch1_zoom_offset,value);
}

void WpuMem::outanalog_ch1_zoom_offset()
{
	out_member(analog_ch1_zoom_offset);
}


Wpu_reg WpuMem::inanalog_ch1_zoom_offset()
{
	reg_in(analog_ch1_zoom_offset);
	return analog_ch1_zoom_offset;
}


void WpuMem::setanalog_ch2_zoom_offset( Wpu_reg value )
{
	reg_assign(analog_ch2_zoom_offset,value);
}

void WpuMem::outanalog_ch2_zoom_offset( Wpu_reg value )
{
	out_assign(analog_ch2_zoom_offset,value);
}

void WpuMem::outanalog_ch2_zoom_offset()
{
	out_member(analog_ch2_zoom_offset);
}


Wpu_reg WpuMem::inanalog_ch2_zoom_offset()
{
	reg_in(analog_ch2_zoom_offset);
	return analog_ch2_zoom_offset;
}


void WpuMem::setanalog_ch3_zoom_offset( Wpu_reg value )
{
	reg_assign(analog_ch3_zoom_offset,value);
}

void WpuMem::outanalog_ch3_zoom_offset( Wpu_reg value )
{
	out_assign(analog_ch3_zoom_offset,value);
}

void WpuMem::outanalog_ch3_zoom_offset()
{
	out_member(analog_ch3_zoom_offset);
}


Wpu_reg WpuMem::inanalog_ch3_zoom_offset()
{
	reg_in(analog_ch3_zoom_offset);
	return analog_ch3_zoom_offset;
}


void WpuMem::setmax_out_main_max_out( Wpu_reg value )
{
	reg_assign_field(max_out,main_max_out,value);
}
void WpuMem::setmax_out_zoom_max_out( Wpu_reg value )
{
	reg_assign_field(max_out,zoom_max_out,value);
}

void WpuMem::setmax_out( Wpu_reg value )
{
	reg_assign_field(max_out,payload,value);
}

void WpuMem::outmax_out_main_max_out( Wpu_reg value )
{
	out_assign_field(max_out,main_max_out,value);
}
void WpuMem::pulsemax_out_main_max_out( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(max_out,main_max_out,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(max_out,main_max_out,idleV);
}
void WpuMem::outmax_out_zoom_max_out( Wpu_reg value )
{
	out_assign_field(max_out,zoom_max_out,value);
}
void WpuMem::pulsemax_out_zoom_max_out( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(max_out,zoom_max_out,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(max_out,zoom_max_out,idleV);
}

void WpuMem::outmax_out( Wpu_reg value )
{
	out_assign_field(max_out,payload,value);
}

void WpuMem::outmax_out()
{
	out_member(max_out);
}


Wpu_reg WpuMem::inmax_out()
{
	reg_in(max_out);
	return max_out.payload;
}


void WpuMem::setview_bottom_main_view_bottom( Wpu_reg value )
{
	reg_assign_field(view_bottom,main_view_bottom,value);
}
void WpuMem::setview_bottom_zoom_view_bottom( Wpu_reg value )
{
	reg_assign_field(view_bottom,zoom_view_bottom,value);
}

void WpuMem::setview_bottom( Wpu_reg value )
{
	reg_assign_field(view_bottom,payload,value);
}

void WpuMem::outview_bottom_main_view_bottom( Wpu_reg value )
{
	out_assign_field(view_bottom,main_view_bottom,value);
}
void WpuMem::pulseview_bottom_main_view_bottom( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(view_bottom,main_view_bottom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(view_bottom,main_view_bottom,idleV);
}
void WpuMem::outview_bottom_zoom_view_bottom( Wpu_reg value )
{
	out_assign_field(view_bottom,zoom_view_bottom,value);
}
void WpuMem::pulseview_bottom_zoom_view_bottom( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(view_bottom,zoom_view_bottom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(view_bottom,zoom_view_bottom,idleV);
}

void WpuMem::outview_bottom( Wpu_reg value )
{
	out_assign_field(view_bottom,payload,value);
}

void WpuMem::outview_bottom()
{
	out_member(view_bottom);
}


Wpu_reg WpuMem::inview_bottom()
{
	reg_in(view_bottom);
	return view_bottom.payload;
}


void WpuMem::setzoom_en_en( Wpu_reg value )
{
	reg_assign_field(zoom_en,en,value);
}

void WpuMem::setzoom_en( Wpu_reg value )
{
	reg_assign_field(zoom_en,payload,value);
}

void WpuMem::outzoom_en_en( Wpu_reg value )
{
	out_assign_field(zoom_en,en,value);
}
void WpuMem::pulsezoom_en_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(zoom_en,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(zoom_en,en,idleV);
}

void WpuMem::outzoom_en( Wpu_reg value )
{
	out_assign_field(zoom_en,payload,value);
}

void WpuMem::outzoom_en()
{
	out_member(zoom_en);
}


Wpu_reg WpuMem::inzoom_en()
{
	reg_in(zoom_en);
	return zoom_en.payload;
}


void WpuMem::setanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch0_zoom_gain,analog_ch0_zoom_gain,value);
}

void WpuMem::setanalog_ch0_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch0_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch0_zoom_gain,analog_ch0_zoom_gain,value);
}
void WpuMem::pulseanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch0_zoom_gain,analog_ch0_zoom_gain,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch0_zoom_gain,analog_ch0_zoom_gain,idleV);
}

void WpuMem::outanalog_ch0_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch0_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch0_zoom_gain()
{
	out_member(analog_ch0_zoom_gain);
}


Wpu_reg WpuMem::inanalog_ch0_zoom_gain()
{
	reg_in(analog_ch0_zoom_gain);
	return analog_ch0_zoom_gain.payload;
}


void WpuMem::setanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch1_zoom_gain,analog_ch1_zoom_gain,value);
}

void WpuMem::setanalog_ch1_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch1_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch1_zoom_gain,analog_ch1_zoom_gain,value);
}
void WpuMem::pulseanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch1_zoom_gain,analog_ch1_zoom_gain,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch1_zoom_gain,analog_ch1_zoom_gain,idleV);
}

void WpuMem::outanalog_ch1_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch1_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch1_zoom_gain()
{
	out_member(analog_ch1_zoom_gain);
}


Wpu_reg WpuMem::inanalog_ch1_zoom_gain()
{
	reg_in(analog_ch1_zoom_gain);
	return analog_ch1_zoom_gain.payload;
}


void WpuMem::setanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch2_zoom_gain,analog_ch2_zoom_gain,value);
}

void WpuMem::setanalog_ch2_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch2_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch2_zoom_gain,analog_ch2_zoom_gain,value);
}
void WpuMem::pulseanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch2_zoom_gain,analog_ch2_zoom_gain,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch2_zoom_gain,analog_ch2_zoom_gain,idleV);
}

void WpuMem::outanalog_ch2_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch2_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch2_zoom_gain()
{
	out_member(analog_ch2_zoom_gain);
}


Wpu_reg WpuMem::inanalog_ch2_zoom_gain()
{
	reg_in(analog_ch2_zoom_gain);
	return analog_ch2_zoom_gain.payload;
}


void WpuMem::setanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch3_zoom_gain,analog_ch3_zoom_gain,value);
}

void WpuMem::setanalog_ch3_zoom_gain( Wpu_reg value )
{
	reg_assign_field(analog_ch3_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch3_zoom_gain,analog_ch3_zoom_gain,value);
}
void WpuMem::pulseanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(analog_ch3_zoom_gain,analog_ch3_zoom_gain,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_ch3_zoom_gain,analog_ch3_zoom_gain,idleV);
}

void WpuMem::outanalog_ch3_zoom_gain( Wpu_reg value )
{
	out_assign_field(analog_ch3_zoom_gain,payload,value);
}

void WpuMem::outanalog_ch3_zoom_gain()
{
	out_member(analog_ch3_zoom_gain);
}


Wpu_reg WpuMem::inanalog_ch3_zoom_gain()
{
	reg_in(analog_ch3_zoom_gain);
	return analog_ch3_zoom_gain.payload;
}


Wpu_reg WpuMem::indebug_info_pm()
{
	reg_in(debug_info_pm);
	return debug_info_pm.payload;
}


void WpuMem::setzoom_scale_times_zoom_scale_times_m( Wpu_reg value )
{
	reg_assign_field(zoom_scale_times,zoom_scale_times_m,value);
}
void WpuMem::setzoom_scale_times_zoom_scale_times_n( Wpu_reg value )
{
	reg_assign_field(zoom_scale_times,zoom_scale_times_n,value);
}
void WpuMem::setzoom_scale_times_zoom_scale_times_en( Wpu_reg value )
{
	reg_assign_field(zoom_scale_times,zoom_scale_times_en,value);
}

void WpuMem::setzoom_scale_times( Wpu_reg value )
{
	reg_assign_field(zoom_scale_times,payload,value);
}

void WpuMem::outzoom_scale_times_zoom_scale_times_m( Wpu_reg value )
{
	out_assign_field(zoom_scale_times,zoom_scale_times_m,value);
}
void WpuMem::pulsezoom_scale_times_zoom_scale_times_m( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(zoom_scale_times,zoom_scale_times_m,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(zoom_scale_times,zoom_scale_times_m,idleV);
}
void WpuMem::outzoom_scale_times_zoom_scale_times_n( Wpu_reg value )
{
	out_assign_field(zoom_scale_times,zoom_scale_times_n,value);
}
void WpuMem::pulsezoom_scale_times_zoom_scale_times_n( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(zoom_scale_times,zoom_scale_times_n,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(zoom_scale_times,zoom_scale_times_n,idleV);
}
void WpuMem::outzoom_scale_times_zoom_scale_times_en( Wpu_reg value )
{
	out_assign_field(zoom_scale_times,zoom_scale_times_en,value);
}
void WpuMem::pulsezoom_scale_times_zoom_scale_times_en( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(zoom_scale_times,zoom_scale_times_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(zoom_scale_times,zoom_scale_times_en,idleV);
}

void WpuMem::outzoom_scale_times( Wpu_reg value )
{
	out_assign_field(zoom_scale_times,payload,value);
}

void WpuMem::outzoom_scale_times()
{
	out_member(zoom_scale_times);
}


Wpu_reg WpuMem::inzoom_scale_times()
{
	reg_in(zoom_scale_times);
	return zoom_scale_times.payload;
}


void WpuMem::setzoom_scale_length( Wpu_reg value )
{
	reg_assign(zoom_scale_length,value);
}

void WpuMem::outzoom_scale_length( Wpu_reg value )
{
	out_assign(zoom_scale_length,value);
}

void WpuMem::outzoom_scale_length()
{
	out_member(zoom_scale_length);
}


Wpu_reg WpuMem::inzoom_scale_length()
{
	reg_in(zoom_scale_length);
	return zoom_scale_length;
}


void WpuMem::settime_base_mode( Wpu_reg value )
{
	reg_assign(time_base_mode,value);
}

void WpuMem::outtime_base_mode( Wpu_reg value )
{
	out_assign(time_base_mode,value);
}

void WpuMem::outtime_base_mode()
{
	out_member(time_base_mode);
}


Wpu_reg WpuMem::intime_base_mode()
{
	reg_in(time_base_mode);
	return time_base_mode;
}


void WpuMem::setscan_trig_cnt_scan_trig_col( Wpu_reg value )
{
	reg_assign_field(scan_trig_cnt,scan_trig_col,value);
}
void WpuMem::setscan_trig_cnt_scan_trig_addr( Wpu_reg value )
{
	reg_assign_field(scan_trig_cnt,scan_trig_addr,value);
}

void WpuMem::setscan_trig_cnt( Wpu_reg value )
{
	reg_assign_field(scan_trig_cnt,payload,value);
}

void WpuMem::outscan_trig_cnt_scan_trig_col( Wpu_reg value )
{
	out_assign_field(scan_trig_cnt,scan_trig_col,value);
}
void WpuMem::pulsescan_trig_cnt_scan_trig_col( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(scan_trig_cnt,scan_trig_col,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(scan_trig_cnt,scan_trig_col,idleV);
}
void WpuMem::outscan_trig_cnt_scan_trig_addr( Wpu_reg value )
{
	out_assign_field(scan_trig_cnt,scan_trig_addr,value);
}
void WpuMem::pulsescan_trig_cnt_scan_trig_addr( Wpu_reg activeV, Wpu_reg idleV, int timeus )
{
	out_assign_field(scan_trig_cnt,scan_trig_addr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(scan_trig_cnt,scan_trig_addr,idleV);
}

void WpuMem::outscan_trig_cnt( Wpu_reg value )
{
	out_assign_field(scan_trig_cnt,payload,value);
}

void WpuMem::outscan_trig_cnt()
{
	out_member(scan_trig_cnt);
}


Wpu_reg WpuMem::inscan_trig_cnt()
{
	reg_in(scan_trig_cnt);
	return scan_trig_cnt.payload;
}


Wpu_reg WpuMem::inroll_lenth()
{
	reg_in(roll_lenth);
	return roll_lenth.payload;
}


cache_addr WpuMem::addr_version(){ return 0x400+mAddrBase; }
cache_addr WpuMem::addr_init_pe(){ return 0x0588+mAddrBase; }
cache_addr WpuMem::addr_config_ID(){ return 0x0410+mAddrBase; }
cache_addr WpuMem::addr_display_en(){ return 0x0414+mAddrBase; }

cache_addr WpuMem::addr_analog_mode(){ return 0x0420+mAddrBase; }
cache_addr WpuMem::addr_LA_en(){ return 0x0428+mAddrBase; }
cache_addr WpuMem::addr_refresh_time(){ return 0x042c+mAddrBase; }
cache_addr WpuMem::addr_persist_mode(){ return 0x043c+mAddrBase; }

cache_addr WpuMem::addr_persist_init(){ return 0x0440+mAddrBase; }
cache_addr WpuMem::addr_persist_dim_rate(){ return 0x0444+mAddrBase; }
cache_addr WpuMem::addr_plot_mode(){ return 0x0448+mAddrBase; }
cache_addr WpuMem::addr_slope_mode(){ return 0x044c+mAddrBase; }

cache_addr WpuMem::addr_analog_ch0_gain(){ return 0x0460+mAddrBase; }
cache_addr WpuMem::addr_analog_ch0_noise(){ return 0x0464+mAddrBase; }
cache_addr WpuMem::addr_analog_ch0_offset(){ return 0x0468+mAddrBase; }
cache_addr WpuMem::addr_analog_ch1_gain(){ return 0x0470+mAddrBase; }

cache_addr WpuMem::addr_analog_ch1_noise(){ return 0x0474+mAddrBase; }
cache_addr WpuMem::addr_analog_ch1_offset(){ return 0x0478+mAddrBase; }
cache_addr WpuMem::addr_analog_ch2_gain(){ return 0x0480+mAddrBase; }
cache_addr WpuMem::addr_analog_ch2_noise(){ return 0x0484+mAddrBase; }

cache_addr WpuMem::addr_analog_ch2_offset(){ return 0x0488+mAddrBase; }
cache_addr WpuMem::addr_analog_ch3_gain(){ return 0x0490+mAddrBase; }
cache_addr WpuMem::addr_analog_ch3_noise(){ return 0x0494+mAddrBase; }
cache_addr WpuMem::addr_analog_ch3_offset(){ return 0x0498+mAddrBase; }

cache_addr WpuMem::addr_color_map(){ return 0x0550+mAddrBase; }
cache_addr WpuMem::addr_view_layer(){ return 0x0554+mAddrBase; }
cache_addr WpuMem::addr_trace_layer_addr_0(){ return 0x0558+mAddrBase; }
cache_addr WpuMem::addr_trace_layer_addr_1(){ return 0x055c+mAddrBase; }

cache_addr WpuMem::addr_wpu_clear(){ return 0x0584+mAddrBase; }
cache_addr WpuMem::addr_srn_num(){ return 0x05FC+mAddrBase; }
cache_addr WpuMem::addr_wpu_status(){ return 0x0600+mAddrBase; }
cache_addr WpuMem::addr_mem_status(){ return 0x0604+mAddrBase; }

cache_addr WpuMem::addr_init_status(){ return 0x0608+mAddrBase; }
cache_addr WpuMem::addr_load_config_id(){ return 0x060c+mAddrBase; }
cache_addr WpuMem::addr_init_la_plot_table(){ return 0x059C+mAddrBase; }
cache_addr WpuMem::addr_la_plot_tbl_data(){ return 0x04E0+mAddrBase; }

cache_addr WpuMem::addr_wave_view_start(){ return 0x0450+mAddrBase; }
cache_addr WpuMem::addr_wave_view_length(){ return 0x0454+mAddrBase; }
cache_addr WpuMem::addr_zoom_view_start(){ return 0x0518+mAddrBase; }
cache_addr WpuMem::addr_zoom_view_length(){ return 0x051c+mAddrBase; }

cache_addr WpuMem::addr_xy0_en(){ return 0x0540+mAddrBase; }
cache_addr WpuMem::addr_xy1_en(){ return 0x0544+mAddrBase; }
cache_addr WpuMem::addr_xy2_en(){ return 0x0548+mAddrBase; }
cache_addr WpuMem::addr_xy3_en(){ return 0x054c+mAddrBase; }

cache_addr WpuMem::addr_init_palette_table(){ return 0x0590+mAddrBase; }
cache_addr WpuMem::addr_palette_tbl_wdata(){ return 0x0560+mAddrBase; }
cache_addr WpuMem::addr_data_mode(){ return 0x430+mAddrBase; }
cache_addr WpuMem::addr_scale_times(){ return 0x0570+mAddrBase; }

cache_addr WpuMem::addr_scale_length(){ return 0x0574+mAddrBase; }
cache_addr WpuMem::addr_analog_ch0_zoom_offset(){ return 0x04F0+mAddrBase; }
cache_addr WpuMem::addr_analog_ch1_zoom_offset(){ return 0x04F4+mAddrBase; }
cache_addr WpuMem::addr_analog_ch2_zoom_offset(){ return 0x04F8+mAddrBase; }

cache_addr WpuMem::addr_analog_ch3_zoom_offset(){ return 0x04FC+mAddrBase; }
cache_addr WpuMem::addr_max_out(){ return 0x0500+mAddrBase; }
cache_addr WpuMem::addr_view_bottom(){ return 0x0504+mAddrBase; }
cache_addr WpuMem::addr_zoom_en(){ return 0x0438+mAddrBase; }

cache_addr WpuMem::addr_analog_ch0_zoom_gain(){ return 0x0508+mAddrBase; }
cache_addr WpuMem::addr_analog_ch1_zoom_gain(){ return 0x050c+mAddrBase; }
cache_addr WpuMem::addr_analog_ch2_zoom_gain(){ return 0x0510+mAddrBase; }
cache_addr WpuMem::addr_analog_ch3_zoom_gain(){ return 0x0514+mAddrBase; }

cache_addr WpuMem::addr_debug_info_pm(){ return 0x638+mAddrBase; }
cache_addr WpuMem::addr_zoom_scale_times(){ return 0x0520+mAddrBase; }
cache_addr WpuMem::addr_zoom_scale_length(){ return 0x0524+mAddrBase; }
cache_addr WpuMem::addr_time_base_mode(){ return 0x0430+mAddrBase; }

cache_addr WpuMem::addr_scan_trig_cnt(){ return 0x0528+mAddrBase; }
cache_addr WpuMem::addr_roll_lenth(){ return 0x62c+mAddrBase; }


