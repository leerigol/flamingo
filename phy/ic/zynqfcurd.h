#ifndef _ZYNQFCURD_IC_H_
#define _ZYNQFCURD_IC_H_

#define ZynqFcuRd_reg unsigned int

union ZynqFcuRd_trace_wddr3_done{
	struct{
	ZynqFcuRd_reg trace_done:5;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_deinterweave_done{
	struct{
	ZynqFcuRd_reg deinter_done:1;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_roll_full_screen_wdone_flag{
	struct{
	ZynqFcuRd_reg roll_full_screen_wdone_flag:1;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_search_result_wdone{
	struct{
	ZynqFcuRd_reg search_result_current_col:10;
	ZynqFcuRd_reg search_result_wdone:1;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_frame_id{
	struct{
	ZynqFcuRd_reg frame_all_id:4;
	ZynqFcuRd_reg frame_trace_id:4;
	ZynqFcuRd_reg frame_all_id_expand:12;
	ZynqFcuRd_reg frame_trace_id_expand:12;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_axi_rd_overtime_flag{
	struct{
	ZynqFcuRd_reg axi_rd_overtime_flag:1;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_dbg_rd_reg_1{
	struct{
	ZynqFcuRd_reg trace_frame_cnt:16;
	ZynqFcuRd_reg trace_req_state:2;
	ZynqFcuRd_reg trace_dat_send_en:1;
	ZynqFcuRd_reg trace_dat_last_mask:1;

	ZynqFcuRd_reg trace_req_dtype_latch:4;
	ZynqFcuRd_reg current_dat_type:1;
	ZynqFcuRd_reg export_trace_flag:1;
	ZynqFcuRd_reg gtp_src_cnt:6;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_dbg_debug_head_frame1_63_32{
	struct{
	ZynqFcuRd_reg frm_data_type:3;
	ZynqFcuRd_reg sys_proc_la:1;
	ZynqFcuRd_reg frm_plot_type:3;
	ZynqFcuRd_reg sys_zoom:1;

	ZynqFcuRd_reg _pad0:5;
	ZynqFcuRd_reg sys_eye:1;
	ZynqFcuRd_reg frm_obj_wpu:1;
	ZynqFcuRd_reg frm_obj_trace:1;
	ZynqFcuRd_reg FRM_HEAD:16;
	};
	ZynqFcuRd_reg payload;
};

union ZynqFcuRd_dbg_debug_head_frame1_31_0{
	struct{
	ZynqFcuRd_reg tx_length_burst:20;
	ZynqFcuRd_reg sys_config_id:12;
	};
	ZynqFcuRd_reg payload;
};

struct ZynqFcuRdMemProxy {
	ZynqFcuRd_reg VERSION;
	ZynqFcuRd_reg comm_test;
	ZynqFcuRd_trace_wddr3_done trace_wddr3_done; 
	ZynqFcuRd_reg fpga_rdat_done;

	ZynqFcuRd_deinterweave_done deinterweave_done; 
	ZynqFcuRd_reg reserved;
	ZynqFcuRd_reg roll_ana_trace_waddr_latch;
	ZynqFcuRd_reg roll_la_trace_waddr_latch;

	ZynqFcuRd_roll_full_screen_wdone_flag roll_full_screen_wdone_flag; 
	ZynqFcuRd_search_result_wdone search_result_wdone; 
	ZynqFcuRd_frame_id frame_id; 
	ZynqFcuRd_axi_rd_overtime_flag axi_rd_overtime_flag; 

	ZynqFcuRd_dbg_rd_reg_1 dbg_rd_reg_1; 
	ZynqFcuRd_reg dbg_rd_reg_2;
	ZynqFcuRd_reg dbg_rd_reg_3;
	ZynqFcuRd_reg dbg_rd_reg_4;

	ZynqFcuRd_reg dbg_rd_reg_5;
	ZynqFcuRd_reg dbg_cfg2;
	ZynqFcuRd_reg dbg_main_ana_trace_num;
	ZynqFcuRd_reg dbg_zoom_ana_num;

	ZynqFcuRd_dbg_debug_head_frame1_63_32 dbg_debug_head_frame1_63_32; 
	ZynqFcuRd_dbg_debug_head_frame1_31_0 dbg_debug_head_frame1_31_0; 
	ZynqFcuRd_reg dbg_debug_head_frame2_63_32;
	ZynqFcuRd_reg dbg_debug_head_frame2_31_0;

	ZynqFcuRd_reg debug_frame_cnt;
	ZynqFcuRd_reg dbg_frame_sync_test;
	ZynqFcuRd_reg getVERSION(  );

	ZynqFcuRd_reg getcomm_test(  );

	ZynqFcuRd_reg gettrace_wddr3_done_trace_done(  );

	ZynqFcuRd_reg gettrace_wddr3_done(  );

	ZynqFcuRd_reg getfpga_rdat_done(  );

	ZynqFcuRd_reg getdeinterweave_done_deinter_done(  );

	ZynqFcuRd_reg getdeinterweave_done(  );

	ZynqFcuRd_reg getreserved(  );

	ZynqFcuRd_reg getroll_ana_trace_waddr_latch(  );

	ZynqFcuRd_reg getroll_la_trace_waddr_latch(  );

	ZynqFcuRd_reg getroll_full_screen_wdone_flag_roll_full_screen_wdone_flag(  );

	ZynqFcuRd_reg getroll_full_screen_wdone_flag(  );

	ZynqFcuRd_reg getsearch_result_wdone_search_result_current_col(  );
	ZynqFcuRd_reg getsearch_result_wdone_search_result_wdone(  );

	ZynqFcuRd_reg getsearch_result_wdone(  );

	ZynqFcuRd_reg getframe_id_frame_all_id(  );
	ZynqFcuRd_reg getframe_id_frame_trace_id(  );
	ZynqFcuRd_reg getframe_id_frame_all_id_expand(  );
	ZynqFcuRd_reg getframe_id_frame_trace_id_expand(  );

	ZynqFcuRd_reg getframe_id(  );

	ZynqFcuRd_reg getaxi_rd_overtime_flag_axi_rd_overtime_flag(  );

	ZynqFcuRd_reg getaxi_rd_overtime_flag(  );

	ZynqFcuRd_reg getdbg_rd_reg_1_trace_frame_cnt(  );
	ZynqFcuRd_reg getdbg_rd_reg_1_trace_req_state(  );
	ZynqFcuRd_reg getdbg_rd_reg_1_trace_dat_send_en(  );
	ZynqFcuRd_reg getdbg_rd_reg_1_trace_dat_last_mask(  );
	ZynqFcuRd_reg getdbg_rd_reg_1_trace_req_dtype_latch(  );
	ZynqFcuRd_reg getdbg_rd_reg_1_current_dat_type(  );
	ZynqFcuRd_reg getdbg_rd_reg_1_export_trace_flag(  );
	ZynqFcuRd_reg getdbg_rd_reg_1_gtp_src_cnt(  );

	ZynqFcuRd_reg getdbg_rd_reg_1(  );

	ZynqFcuRd_reg getdbg_rd_reg_2(  );

	ZynqFcuRd_reg getdbg_rd_reg_3(  );

	ZynqFcuRd_reg getdbg_rd_reg_4(  );

	ZynqFcuRd_reg getdbg_rd_reg_5(  );

	void assigndbg_cfg2( ZynqFcuRd_reg value );

	ZynqFcuRd_reg getdbg_main_ana_trace_num(  );

	ZynqFcuRd_reg getdbg_zoom_ana_num(  );

	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_frm_data_type(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_sys_proc_la(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_frm_plot_type(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_sys_zoom(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_sys_eye(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_frm_obj_wpu(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_frm_obj_trace(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32_FRM_HEAD(  );

	ZynqFcuRd_reg getdbg_debug_head_frame1_63_32(  );

	ZynqFcuRd_reg getdbg_debug_head_frame1_31_0_tx_length_burst(  );
	ZynqFcuRd_reg getdbg_debug_head_frame1_31_0_sys_config_id(  );

	ZynqFcuRd_reg getdbg_debug_head_frame1_31_0(  );

	ZynqFcuRd_reg getdbg_debug_head_frame2_63_32(  );

	ZynqFcuRd_reg getdbg_debug_head_frame2_31_0(  );

	ZynqFcuRd_reg getdebug_frame_cnt(  );

	ZynqFcuRd_reg getdbg_frame_sync_test(  );

};
struct ZynqFcuRdMem : public ZynqFcuRdMemProxy, public IPhyFpga{
protected:
	ZynqFcuRd_reg mAddrBase;
public:
	ZynqFcuRdMem( ZynqFcuRd_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( ZynqFcuRd_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( ZynqFcuRdMemProxy *proxy);
	void pull( ZynqFcuRdMemProxy *proxy);

public:
	ZynqFcuRd_reg inVERSION(  );

	ZynqFcuRd_reg incomm_test(  );

	ZynqFcuRd_reg intrace_wddr3_done(  );

	ZynqFcuRd_reg infpga_rdat_done(  );

	ZynqFcuRd_reg indeinterweave_done(  );

	ZynqFcuRd_reg inreserved(  );

	ZynqFcuRd_reg inroll_ana_trace_waddr_latch(  );

	ZynqFcuRd_reg inroll_la_trace_waddr_latch(  );

	ZynqFcuRd_reg inroll_full_screen_wdone_flag(  );

	ZynqFcuRd_reg insearch_result_wdone(  );

	ZynqFcuRd_reg inframe_id(  );

	ZynqFcuRd_reg inaxi_rd_overtime_flag(  );

	ZynqFcuRd_reg indbg_rd_reg_1(  );

	ZynqFcuRd_reg indbg_rd_reg_2(  );

	ZynqFcuRd_reg indbg_rd_reg_3(  );

	ZynqFcuRd_reg indbg_rd_reg_4(  );

	ZynqFcuRd_reg indbg_rd_reg_5(  );

	void setdbg_cfg2( ZynqFcuRd_reg value );
	void outdbg_cfg2( ZynqFcuRd_reg value );
	void outdbg_cfg2(  );

	ZynqFcuRd_reg indbg_main_ana_trace_num(  );

	ZynqFcuRd_reg indbg_zoom_ana_num(  );

	ZynqFcuRd_reg indbg_debug_head_frame1_63_32(  );

	ZynqFcuRd_reg indbg_debug_head_frame1_31_0(  );

	ZynqFcuRd_reg indbg_debug_head_frame2_63_32(  );

	ZynqFcuRd_reg indbg_debug_head_frame2_31_0(  );

	ZynqFcuRd_reg indebug_frame_cnt(  );

	ZynqFcuRd_reg indbg_frame_sync_test(  );

public:
	cache_addr addr_VERSION();
	cache_addr addr_comm_test();
	cache_addr addr_trace_wddr3_done();
	cache_addr addr_fpga_rdat_done();

	cache_addr addr_deinterweave_done();
	cache_addr addr_reserved();
	cache_addr addr_roll_ana_trace_waddr_latch();
	cache_addr addr_roll_la_trace_waddr_latch();

	cache_addr addr_roll_full_screen_wdone_flag();
	cache_addr addr_search_result_wdone();
	cache_addr addr_frame_id();
	cache_addr addr_axi_rd_overtime_flag();

	cache_addr addr_dbg_rd_reg_1();
	cache_addr addr_dbg_rd_reg_2();
	cache_addr addr_dbg_rd_reg_3();
	cache_addr addr_dbg_rd_reg_4();

	cache_addr addr_dbg_rd_reg_5();
	cache_addr addr_dbg_cfg2();
	cache_addr addr_dbg_main_ana_trace_num();
	cache_addr addr_dbg_zoom_ana_num();

	cache_addr addr_dbg_debug_head_frame1_63_32();
	cache_addr addr_dbg_debug_head_frame1_31_0();
	cache_addr addr_dbg_debug_head_frame2_63_32();
	cache_addr addr_dbg_debug_head_frame2_31_0();

	cache_addr addr_debug_frame_cnt();
	cache_addr addr_dbg_frame_sync_test();
};
#define _remap_ZynqFcuRd_()\
	mapIn( &VERSION, 0x0800 + mAddrBase );\
	mapIn( &comm_test, 0x0804 + mAddrBase );\
	mapIn( &trace_wddr3_done, 0x0808 + mAddrBase );\
	mapIn( &fpga_rdat_done, 0x080C + mAddrBase );\
	\
	mapIn( &deinterweave_done, 0x0810 + mAddrBase );\
	mapIn( &reserved, 0x0814 + mAddrBase );\
	mapIn( &roll_ana_trace_waddr_latch, 0x0014 + mAddrBase );\
	mapIn( &roll_la_trace_waddr_latch, 0x0018 + mAddrBase );\
	\
	mapIn( &roll_full_screen_wdone_flag, 0x001c + mAddrBase );\
	mapIn( &search_result_wdone, 0x0020 + mAddrBase );\
	mapIn( &frame_id, 0x0024 + mAddrBase );\
	mapIn( &axi_rd_overtime_flag, 0x0c04 + mAddrBase );\
	\
	mapIn( &dbg_rd_reg_1, 0x0074 + mAddrBase );\
	mapIn( &dbg_rd_reg_2, 0x0078 + mAddrBase );\
	mapIn( &dbg_rd_reg_3, 0x007c + mAddrBase );\
	mapIn( &dbg_rd_reg_4, 0x0838 + mAddrBase );\
	\
	mapIn( &dbg_rd_reg_5, 0x083c + mAddrBase );\
	mapIn( &dbg_cfg2, 0x08fc + mAddrBase );\
	mapIn( &dbg_main_ana_trace_num, 0x0048 + mAddrBase );\
	mapIn( &dbg_zoom_ana_num, 0x0044 + mAddrBase );\
	\
	mapIn( &dbg_debug_head_frame1_63_32, 0x0084 + mAddrBase );\
	mapIn( &dbg_debug_head_frame1_31_0, 0x0088 + mAddrBase );\
	mapIn( &dbg_debug_head_frame2_63_32, 0x008c + mAddrBase );\
	mapIn( &dbg_debug_head_frame2_31_0, 0x0090 + mAddrBase );\
	\
	mapIn( &debug_frame_cnt, 0x0094 + mAddrBase );\
	mapIn( &dbg_frame_sync_test, 0x0874 + mAddrBase );\


#endif
