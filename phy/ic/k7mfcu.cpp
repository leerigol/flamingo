#include "k7mfcu.h"
void K7mFcuMemProxy::assignLA_LEVEL1_LA_LEVEL1( K7mFcu_reg value )
{
	mem_assign_field(LA_LEVEL1,LA_LEVEL1,value);
}

void K7mFcuMemProxy::assignLA_LEVEL1( K7mFcu_reg value )
{
	mem_assign_field(LA_LEVEL1,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getLA_LEVEL1_LA_LEVEL1()
{
	return LA_LEVEL1.LA_LEVEL1;
}

K7mFcu_reg K7mFcuMemProxy::getLA_LEVEL1()
{
	return LA_LEVEL1.payload;
}


void K7mFcuMemProxy::assignLA_LEVEL2_LA_LEVEL2( K7mFcu_reg value )
{
	mem_assign_field(LA_LEVEL2,LA_LEVEL2,value);
}

void K7mFcuMemProxy::assignLA_LEVEL2( K7mFcu_reg value )
{
	mem_assign_field(LA_LEVEL2,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getLA_LEVEL2_LA_LEVEL2()
{
	return LA_LEVEL2.LA_LEVEL2;
}

K7mFcu_reg K7mFcuMemProxy::getLA_LEVEL2()
{
	return LA_LEVEL2.payload;
}


void K7mFcuMemProxy::assignFG1_OFFSET_FG1_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(FG1_OFFSET,FG1_OFFSET,value);
}

void K7mFcuMemProxy::assignFG1_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(FG1_OFFSET,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getFG1_OFFSET_FG1_OFFSET()
{
	return FG1_OFFSET.FG1_OFFSET;
}

K7mFcu_reg K7mFcuMemProxy::getFG1_OFFSET()
{
	return FG1_OFFSET.payload;
}


void K7mFcuMemProxy::assignFG1_REF_FG1_REF( K7mFcu_reg value )
{
	mem_assign_field(FG1_REF,FG1_REF,value);
}

void K7mFcuMemProxy::assignFG1_REF( K7mFcu_reg value )
{
	mem_assign_field(FG1_REF,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getFG1_REF_FG1_REF()
{
	return FG1_REF.FG1_REF;
}

K7mFcu_reg K7mFcuMemProxy::getFG1_REF()
{
	return FG1_REF.payload;
}


void K7mFcuMemProxy::assignFG2_OFFSET_FG2_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(FG2_OFFSET,FG2_OFFSET,value);
}

void K7mFcuMemProxy::assignFG2_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(FG2_OFFSET,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getFG2_OFFSET_FG2_OFFSET()
{
	return FG2_OFFSET.FG2_OFFSET;
}

K7mFcu_reg K7mFcuMemProxy::getFG2_OFFSET()
{
	return FG2_OFFSET.payload;
}


void K7mFcuMemProxy::assignFG2_REF_FG2_REF( K7mFcu_reg value )
{
	mem_assign_field(FG2_REF,FG2_REF,value);
}

void K7mFcuMemProxy::assignFG2_REF( K7mFcu_reg value )
{
	mem_assign_field(FG2_REF,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getFG2_REF_FG2_REF()
{
	return FG2_REF.FG2_REF;
}

K7mFcu_reg K7mFcuMemProxy::getFG2_REF()
{
	return FG2_REF.payload;
}


void K7mFcuMemProxy::assignPROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE1_OFFSET,PROBE1_OFFSET,value);
}

void K7mFcuMemProxy::assignPROBE1_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE1_OFFSET,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE1_OFFSET_PROBE1_OFFSET()
{
	return PROBE1_OFFSET.PROBE1_OFFSET;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE1_OFFSET()
{
	return PROBE1_OFFSET.payload;
}


void K7mFcuMemProxy::assignPROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE2_OFFSET,PROBE2_OFFSET,value);
}

void K7mFcuMemProxy::assignPROBE2_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE2_OFFSET,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE2_OFFSET_PROBE2_OFFSET()
{
	return PROBE2_OFFSET.PROBE2_OFFSET;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE2_OFFSET()
{
	return PROBE2_OFFSET.payload;
}


void K7mFcuMemProxy::assignPROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE3_OFFSET,PROBE3_OFFSET,value);
}

void K7mFcuMemProxy::assignPROBE3_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE3_OFFSET,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE3_OFFSET_PROBE3_OFFSET()
{
	return PROBE3_OFFSET.PROBE3_OFFSET;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE3_OFFSET()
{
	return PROBE3_OFFSET.payload;
}


void K7mFcuMemProxy::assignPROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE4_OFFSET,PROBE4_OFFSET,value);
}

void K7mFcuMemProxy::assignPROBE4_OFFSET( K7mFcu_reg value )
{
	mem_assign_field(PROBE4_OFFSET,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE4_OFFSET_PROBE4_OFFSET()
{
	return PROBE4_OFFSET.PROBE4_OFFSET;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE4_OFFSET()
{
	return PROBE4_OFFSET.payload;
}


void K7mFcuMemProxy::assignOFFSET1_OFFSET1( K7mFcu_reg value )
{
	mem_assign_field(OFFSET1,OFFSET1,value);
}

void K7mFcuMemProxy::assignOFFSET1( K7mFcu_reg value )
{
	mem_assign_field(OFFSET1,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getOFFSET1_OFFSET1()
{
	return OFFSET1.OFFSET1;
}

K7mFcu_reg K7mFcuMemProxy::getOFFSET1()
{
	return OFFSET1.payload;
}


void K7mFcuMemProxy::assignOFFSET2_OFFSET2( K7mFcu_reg value )
{
	mem_assign_field(OFFSET2,OFFSET2,value);
}

void K7mFcuMemProxy::assignOFFSET2( K7mFcu_reg value )
{
	mem_assign_field(OFFSET2,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getOFFSET2_OFFSET2()
{
	return OFFSET2.OFFSET2;
}

K7mFcu_reg K7mFcuMemProxy::getOFFSET2()
{
	return OFFSET2.payload;
}


void K7mFcuMemProxy::assignOFFSET3_OFFSET3( K7mFcu_reg value )
{
	mem_assign_field(OFFSET3,OFFSET3,value);
}

void K7mFcuMemProxy::assignOFFSET3( K7mFcu_reg value )
{
	mem_assign_field(OFFSET3,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getOFFSET3_OFFSET3()
{
	return OFFSET3.OFFSET3;
}

K7mFcu_reg K7mFcuMemProxy::getOFFSET3()
{
	return OFFSET3.payload;
}


void K7mFcuMemProxy::assignOFFSET4_OFFSET4( K7mFcu_reg value )
{
	mem_assign_field(OFFSET4,OFFSET4,value);
}

void K7mFcuMemProxy::assignOFFSET4( K7mFcu_reg value )
{
	mem_assign_field(OFFSET4,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getOFFSET4_OFFSET4()
{
	return OFFSET4.OFFSET4;
}

K7mFcu_reg K7mFcuMemProxy::getOFFSET4()
{
	return OFFSET4.payload;
}


void K7mFcuMemProxy::assignEXT_LEVEL_EXT_LEVEL( K7mFcu_reg value )
{
	mem_assign_field(EXT_LEVEL,EXT_LEVEL,value);
}

void K7mFcuMemProxy::assignEXT_LEVEL( K7mFcu_reg value )
{
	mem_assign_field(EXT_LEVEL,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getEXT_LEVEL_EXT_LEVEL()
{
	return EXT_LEVEL.EXT_LEVEL;
}

K7mFcu_reg K7mFcuMemProxy::getEXT_LEVEL()
{
	return EXT_LEVEL.payload;
}


void K7mFcuMemProxy::assignRESERVE_RESERVE( K7mFcu_reg value )
{
	mem_assign_field(RESERVE,RESERVE,value);
}

void K7mFcuMemProxy::assignRESERVE( K7mFcu_reg value )
{
	mem_assign_field(RESERVE,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getRESERVE_RESERVE()
{
	return RESERVE.RESERVE;
}

K7mFcu_reg K7mFcuMemProxy::getRESERVE()
{
	return RESERVE.payload;
}


void K7mFcuMemProxy::assignDAC_CHN_TIME_holdtime( K7mFcu_reg value )
{
	mem_assign_field(DAC_CHN_TIME,holdtime,value);
}

void K7mFcuMemProxy::assignDAC_CHN_TIME( K7mFcu_reg value )
{
	mem_assign_field(DAC_CHN_TIME,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getDAC_CHN_TIME_holdtime()
{
	return DAC_CHN_TIME.holdtime;
}

K7mFcu_reg K7mFcuMemProxy::getDAC_CHN_TIME()
{
	return DAC_CHN_TIME.payload;
}


void K7mFcuMemProxy::assignMISC_PROBE_EN_en( K7mFcu_reg value )
{
	mem_assign_field(MISC_PROBE_EN,en,value);
}
void K7mFcuMemProxy::assignMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg value )
{
	mem_assign_field(MISC_PROBE_EN,sel_1K_10K,value);
}

void K7mFcuMemProxy::assignMISC_PROBE_EN( K7mFcu_reg value )
{
	mem_assign_field(MISC_PROBE_EN,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getMISC_PROBE_EN_en()
{
	return MISC_PROBE_EN.en;
}
K7mFcu_reg K7mFcuMemProxy::getMISC_PROBE_EN_sel_1K_10K()
{
	return MISC_PROBE_EN.sel_1K_10K;
}

K7mFcu_reg K7mFcuMemProxy::getMISC_PROBE_EN()
{
	return MISC_PROBE_EN.payload;
}


void K7mFcuMemProxy::assignMISC_TRIGOUT_SEL_sel( K7mFcu_reg value )
{
	mem_assign_field(MISC_TRIGOUT_SEL,sel,value);
}

void K7mFcuMemProxy::assignMISC_TRIGOUT_SEL( K7mFcu_reg value )
{
	mem_assign_field(MISC_TRIGOUT_SEL,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getMISC_TRIGOUT_SEL_sel()
{
	return MISC_TRIGOUT_SEL.sel;
}

K7mFcu_reg K7mFcuMemProxy::getMISC_TRIGOUT_SEL()
{
	return MISC_TRIGOUT_SEL.payload;
}


K7mFcu_reg K7mFcuMemProxy::getVERSION()
{
	return VERSION;
}


void K7mFcuMemProxy::assignCFG_1WIRE_bit_start( K7mFcu_reg value )
{
	mem_assign_field(CFG_1WIRE,bit_start,value);
}
void K7mFcuMemProxy::assignCFG_1WIRE_bit_tx( K7mFcu_reg value )
{
	mem_assign_field(CFG_1WIRE,bit_tx,value);
}
void K7mFcuMemProxy::assignCFG_1WIRE_bit_rx( K7mFcu_reg value )
{
	mem_assign_field(CFG_1WIRE,bit_rx,value);
}
void K7mFcuMemProxy::assignCFG_1WIRE_bit_period( K7mFcu_reg value )
{
	mem_assign_field(CFG_1WIRE,bit_period,value);
}

void K7mFcuMemProxy::assignCFG_1WIRE( K7mFcu_reg value )
{
	mem_assign_field(CFG_1WIRE,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getCFG_1WIRE_bit_start()
{
	return CFG_1WIRE.bit_start;
}
K7mFcu_reg K7mFcuMemProxy::getCFG_1WIRE_bit_tx()
{
	return CFG_1WIRE.bit_tx;
}
K7mFcu_reg K7mFcuMemProxy::getCFG_1WIRE_bit_rx()
{
	return CFG_1WIRE.bit_rx;
}
K7mFcu_reg K7mFcuMemProxy::getCFG_1WIRE_bit_period()
{
	return CFG_1WIRE.bit_period;
}

K7mFcu_reg K7mFcuMemProxy::getCFG_1WIRE()
{
	return CFG_1WIRE.payload;
}


void K7mFcuMemProxy::assignPROBE_CH1_rd_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,rd_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH1_rd_data_vld( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,rd_data_vld,value);
}
void K7mFcuMemProxy::assignPROBE_CH1_rx_len( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,rx_len,value);
}
void K7mFcuMemProxy::assignPROBE_CH1_wr_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,wr_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH1_wr_data_en( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,wr_data_en,value);
}
void K7mFcuMemProxy::assignPROBE_CH1_cmd_type( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,cmd_type,value);
}
void K7mFcuMemProxy::assignPROBE_CH1_clr( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,clr,value);
}

void K7mFcuMemProxy::assignPROBE_CH1( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH1,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1_rd_data()
{
	return PROBE_CH1.rd_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1_rd_data_vld()
{
	return PROBE_CH1.rd_data_vld;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1_rx_len()
{
	return PROBE_CH1.rx_len;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1_wr_data()
{
	return PROBE_CH1.wr_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1_wr_data_en()
{
	return PROBE_CH1.wr_data_en;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1_cmd_type()
{
	return PROBE_CH1.cmd_type;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1_clr()
{
	return PROBE_CH1.clr;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE_CH1()
{
	return PROBE_CH1.payload;
}


void K7mFcuMemProxy::assignPROBE_CH2_rd_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,rd_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH2_rd_data_vld( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,rd_data_vld,value);
}
void K7mFcuMemProxy::assignPROBE_CH2_rx_len( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,rx_len,value);
}
void K7mFcuMemProxy::assignPROBE_CH2_wr_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,wr_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH2_wr_data_en( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,wr_data_en,value);
}
void K7mFcuMemProxy::assignPROBE_CH2_cmd_type( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,cmd_type,value);
}
void K7mFcuMemProxy::assignPROBE_CH2_clr( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,clr,value);
}

void K7mFcuMemProxy::assignPROBE_CH2( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH2,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2_rd_data()
{
	return PROBE_CH2.rd_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2_rd_data_vld()
{
	return PROBE_CH2.rd_data_vld;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2_rx_len()
{
	return PROBE_CH2.rx_len;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2_wr_data()
{
	return PROBE_CH2.wr_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2_wr_data_en()
{
	return PROBE_CH2.wr_data_en;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2_cmd_type()
{
	return PROBE_CH2.cmd_type;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2_clr()
{
	return PROBE_CH2.clr;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE_CH2()
{
	return PROBE_CH2.payload;
}


void K7mFcuMemProxy::assignPROBE_CH3_rd_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,rd_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH3_rd_data_vld( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,rd_data_vld,value);
}
void K7mFcuMemProxy::assignPROBE_CH3_rx_len( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,rx_len,value);
}
void K7mFcuMemProxy::assignPROBE_CH3_wr_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,wr_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH3_wr_data_en( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,wr_data_en,value);
}
void K7mFcuMemProxy::assignPROBE_CH3_cmd_type( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,cmd_type,value);
}
void K7mFcuMemProxy::assignPROBE_CH3_clr( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,clr,value);
}

void K7mFcuMemProxy::assignPROBE_CH3( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH3,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3_rd_data()
{
	return PROBE_CH3.rd_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3_rd_data_vld()
{
	return PROBE_CH3.rd_data_vld;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3_rx_len()
{
	return PROBE_CH3.rx_len;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3_wr_data()
{
	return PROBE_CH3.wr_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3_wr_data_en()
{
	return PROBE_CH3.wr_data_en;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3_cmd_type()
{
	return PROBE_CH3.cmd_type;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3_clr()
{
	return PROBE_CH3.clr;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE_CH3()
{
	return PROBE_CH3.payload;
}


void K7mFcuMemProxy::assignPROBE_CH4_rd_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,rd_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH4_rd_data_vld( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,rd_data_vld,value);
}
void K7mFcuMemProxy::assignPROBE_CH4_rx_len( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,rx_len,value);
}
void K7mFcuMemProxy::assignPROBE_CH4_wr_data( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,wr_data,value);
}
void K7mFcuMemProxy::assignPROBE_CH4_wr_data_en( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,wr_data_en,value);
}
void K7mFcuMemProxy::assignPROBE_CH4_cmd_type( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,cmd_type,value);
}
void K7mFcuMemProxy::assignPROBE_CH4_clr( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,clr,value);
}

void K7mFcuMemProxy::assignPROBE_CH4( K7mFcu_reg value )
{
	mem_assign_field(PROBE_CH4,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4_rd_data()
{
	return PROBE_CH4.rd_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4_rd_data_vld()
{
	return PROBE_CH4.rd_data_vld;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4_rx_len()
{
	return PROBE_CH4.rx_len;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4_wr_data()
{
	return PROBE_CH4.wr_data;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4_wr_data_en()
{
	return PROBE_CH4.wr_data_en;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4_cmd_type()
{
	return PROBE_CH4.cmd_type;
}
K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4_clr()
{
	return PROBE_CH4.clr;
}

K7mFcu_reg K7mFcuMemProxy::getPROBE_CH4()
{
	return PROBE_CH4.payload;
}


void K7mFcuMemProxy::assignPROBE_WAIT( K7mFcu_reg value )
{
	mem_assign(PROBE_WAIT,value);
}


K7mFcu_reg K7mFcuMemProxy::getPROBE_WAIT()
{
	return PROBE_WAIT;
}


void K7mFcuMemProxy::assignDG_CFG_data( K7mFcu_reg value )
{
	mem_assign_field(DG_CFG,data,value);
}
void K7mFcuMemProxy::assignDG_CFG_addr( K7mFcu_reg value )
{
	mem_assign_field(DG_CFG,addr,value);
}

void K7mFcuMemProxy::assignDG_CFG( K7mFcu_reg value )
{
	mem_assign_field(DG_CFG,payload,value);
}


K7mFcu_reg K7mFcuMemProxy::getDG_RD()
{
	return DG_RD;
}


K7mFcu_reg K7mFcuMemProxy::getdbg_search_event_num_latch()
{
	return dbg_search_event_num_latch;
}


K7mFcu_reg K7mFcuMemProxy::getdbg_search_frame_lenth_latch()
{
	return dbg_search_frame_lenth_latch;
}



void K7mFcuMem::_loadOtp()
{
	LA_LEVEL1.payload=0x0;
		LA_LEVEL1.LA_LEVEL1=0x8000;

	LA_LEVEL2.payload=0x0;
		LA_LEVEL2.LA_LEVEL2=0x8000;

	FG1_OFFSET.payload=0x0;
		FG1_OFFSET.FG1_OFFSET=0x8000;

	FG1_REF.payload=0x0;
		FG1_REF.FG1_REF=0x8000;


	FG2_OFFSET.payload=0x0;
		FG2_OFFSET.FG2_OFFSET=0x8000;

	FG2_REF.payload=0x0;
		FG2_REF.FG2_REF=0x8000;

	PROBE1_OFFSET.payload=0x0;
		PROBE1_OFFSET.PROBE1_OFFSET=0x8000;

	PROBE2_OFFSET.payload=0x0;
		PROBE2_OFFSET.PROBE2_OFFSET=0x8000;


	PROBE3_OFFSET.payload=0x0;
		PROBE3_OFFSET.PROBE3_OFFSET=0x8000;

	PROBE4_OFFSET.payload=0x0;
		PROBE4_OFFSET.PROBE4_OFFSET=0x8000;

	OFFSET1.payload=0x0;
		OFFSET1.OFFSET1=0x8000;

	OFFSET2.payload=0x0;
		OFFSET2.OFFSET2=0x8000;


	OFFSET3.payload=0x0;
		OFFSET3.OFFSET3=0x8000;

	OFFSET4.payload=0x0;
		OFFSET4.OFFSET4=0x8000;

	EXT_LEVEL.payload=0x0;
		EXT_LEVEL.EXT_LEVEL=0x8000;

	RESERVE.payload=0x0;
		RESERVE.RESERVE=0x8000;


	DAC_CHN_TIME.payload=0x0;
		DAC_CHN_TIME.holdtime=0x1f4;

	MISC_PROBE_EN.payload=0x0;
		MISC_PROBE_EN.en=0x1;
		MISC_PROBE_EN.sel_1K_10K=0x0;

	MISC_TRIGOUT_SEL.payload=0x0;
		MISC_TRIGOUT_SEL.sel=0x0;

	VERSION=0x0;

	CFG_1WIRE.payload=0x0;
		CFG_1WIRE.bit_start=0xa;
		CFG_1WIRE.bit_tx=0xb4;
		CFG_1WIRE.bit_rx=0x78;
		CFG_1WIRE.bit_period=0xfc;

	PROBE_CH1.payload=0x0;
		PROBE_CH1.rd_data=0x0;
		PROBE_CH1.rd_data_vld=0x0;
		PROBE_CH1.rx_len=0x34;
		PROBE_CH1.wr_data=0x0;

		PROBE_CH1.wr_data_en=0x1;
		PROBE_CH1.cmd_type=0x1;
		PROBE_CH1.clr=0x0;

	PROBE_CH2.payload=0x0;
		PROBE_CH2.rd_data=0x0;
		PROBE_CH2.rd_data_vld=0x0;
		PROBE_CH2.rx_len=0x34;
		PROBE_CH2.wr_data=0x0;

		PROBE_CH2.wr_data_en=0x1;
		PROBE_CH2.cmd_type=0x1;
		PROBE_CH2.clr=0x0;

	PROBE_CH3.payload=0x0;
		PROBE_CH3.rd_data=0x0;
		PROBE_CH3.rd_data_vld=0x0;
		PROBE_CH3.rx_len=0x34;
		PROBE_CH3.wr_data=0x0;

		PROBE_CH3.wr_data_en=0x1;
		PROBE_CH3.cmd_type=0x1;
		PROBE_CH3.clr=0x0;


	PROBE_CH4.payload=0x0;
		PROBE_CH4.rd_data=0x0;
		PROBE_CH4.rd_data_vld=0x0;
		PROBE_CH4.rx_len=0x34;
		PROBE_CH4.wr_data=0x0;

		PROBE_CH4.wr_data_en=0x1;
		PROBE_CH4.cmd_type=0x1;
		PROBE_CH4.clr=0x0;

	PROBE_WAIT=0x3e8;
	DG_CFG.payload=0x0;
		DG_CFG.data=0x0;
		DG_CFG.addr=0x0;

	DG_RD=0x0;

	dbg_search_event_num_latch=0x0;
	dbg_search_frame_lenth_latch=0x0;
}

void K7mFcuMem::_outOtp()
{
	outLA_LEVEL1();
	outLA_LEVEL2();
	outFG1_OFFSET();
	outFG1_REF();

	outFG2_OFFSET();
	outFG2_REF();
	outPROBE1_OFFSET();
	outPROBE2_OFFSET();

	outPROBE3_OFFSET();
	outPROBE4_OFFSET();
	outOFFSET1();
	outOFFSET2();

	outOFFSET3();
	outOFFSET4();
	outEXT_LEVEL();
	outRESERVE();

	outDAC_CHN_TIME();
	outMISC_PROBE_EN();
	outMISC_TRIGOUT_SEL();

	outCFG_1WIRE();
	outPROBE_CH1();
	outPROBE_CH2();
	outPROBE_CH3();

	outPROBE_CH4();
	outPROBE_WAIT();
	outDG_CFG();

}
void K7mFcuMem::push( K7mFcuMemProxy *proxy )
{
	if ( LA_LEVEL1.payload != proxy->LA_LEVEL1.payload )
	{reg_assign_field(LA_LEVEL1,payload,proxy->LA_LEVEL1.payload);}

	if ( LA_LEVEL2.payload != proxy->LA_LEVEL2.payload )
	{reg_assign_field(LA_LEVEL2,payload,proxy->LA_LEVEL2.payload);}

	if ( FG1_OFFSET.payload != proxy->FG1_OFFSET.payload )
	{reg_assign_field(FG1_OFFSET,payload,proxy->FG1_OFFSET.payload);}

	if ( FG1_REF.payload != proxy->FG1_REF.payload )
	{reg_assign_field(FG1_REF,payload,proxy->FG1_REF.payload);}

	if ( FG2_OFFSET.payload != proxy->FG2_OFFSET.payload )
	{reg_assign_field(FG2_OFFSET,payload,proxy->FG2_OFFSET.payload);}

	if ( FG2_REF.payload != proxy->FG2_REF.payload )
	{reg_assign_field(FG2_REF,payload,proxy->FG2_REF.payload);}

	if ( PROBE1_OFFSET.payload != proxy->PROBE1_OFFSET.payload )
	{reg_assign_field(PROBE1_OFFSET,payload,proxy->PROBE1_OFFSET.payload);}

	if ( PROBE2_OFFSET.payload != proxy->PROBE2_OFFSET.payload )
	{reg_assign_field(PROBE2_OFFSET,payload,proxy->PROBE2_OFFSET.payload);}

	if ( PROBE3_OFFSET.payload != proxy->PROBE3_OFFSET.payload )
	{reg_assign_field(PROBE3_OFFSET,payload,proxy->PROBE3_OFFSET.payload);}

	if ( PROBE4_OFFSET.payload != proxy->PROBE4_OFFSET.payload )
	{reg_assign_field(PROBE4_OFFSET,payload,proxy->PROBE4_OFFSET.payload);}

	if ( OFFSET1.payload != proxy->OFFSET1.payload )
	{reg_assign_field(OFFSET1,payload,proxy->OFFSET1.payload);}

	if ( OFFSET2.payload != proxy->OFFSET2.payload )
	{reg_assign_field(OFFSET2,payload,proxy->OFFSET2.payload);}

	if ( OFFSET3.payload != proxy->OFFSET3.payload )
	{reg_assign_field(OFFSET3,payload,proxy->OFFSET3.payload);}

	if ( OFFSET4.payload != proxy->OFFSET4.payload )
	{reg_assign_field(OFFSET4,payload,proxy->OFFSET4.payload);}

	if ( EXT_LEVEL.payload != proxy->EXT_LEVEL.payload )
	{reg_assign_field(EXT_LEVEL,payload,proxy->EXT_LEVEL.payload);}

	if ( RESERVE.payload != proxy->RESERVE.payload )
	{reg_assign_field(RESERVE,payload,proxy->RESERVE.payload);}

	if ( DAC_CHN_TIME.payload != proxy->DAC_CHN_TIME.payload )
	{reg_assign_field(DAC_CHN_TIME,payload,proxy->DAC_CHN_TIME.payload);}

	if ( MISC_PROBE_EN.payload != proxy->MISC_PROBE_EN.payload )
	{reg_assign_field(MISC_PROBE_EN,payload,proxy->MISC_PROBE_EN.payload);}

	if ( MISC_TRIGOUT_SEL.payload != proxy->MISC_TRIGOUT_SEL.payload )
	{reg_assign_field(MISC_TRIGOUT_SEL,payload,proxy->MISC_TRIGOUT_SEL.payload);}

	if ( CFG_1WIRE.payload != proxy->CFG_1WIRE.payload )
	{reg_assign_field(CFG_1WIRE,payload,proxy->CFG_1WIRE.payload);}

	if ( PROBE_CH1.payload != proxy->PROBE_CH1.payload )
	{reg_assign_field(PROBE_CH1,payload,proxy->PROBE_CH1.payload);}

	if ( PROBE_CH2.payload != proxy->PROBE_CH2.payload )
	{reg_assign_field(PROBE_CH2,payload,proxy->PROBE_CH2.payload);}

	if ( PROBE_CH3.payload != proxy->PROBE_CH3.payload )
	{reg_assign_field(PROBE_CH3,payload,proxy->PROBE_CH3.payload);}

	if ( PROBE_CH4.payload != proxy->PROBE_CH4.payload )
	{reg_assign_field(PROBE_CH4,payload,proxy->PROBE_CH4.payload);}

	if ( PROBE_WAIT != proxy->PROBE_WAIT )
	{reg_assign(PROBE_WAIT,proxy->PROBE_WAIT);}

	if ( DG_CFG.payload != proxy->DG_CFG.payload )
	{reg_assign_field(DG_CFG,payload,proxy->DG_CFG.payload);}

}
void K7mFcuMem::pull( K7mFcuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(LA_LEVEL1);
	reg_in(LA_LEVEL2);
	reg_in(FG1_OFFSET);
	reg_in(FG1_REF);
	reg_in(FG2_OFFSET);
	reg_in(FG2_REF);
	reg_in(PROBE1_OFFSET);
	reg_in(PROBE2_OFFSET);
	reg_in(PROBE3_OFFSET);
	reg_in(PROBE4_OFFSET);
	reg_in(OFFSET1);
	reg_in(OFFSET2);
	reg_in(OFFSET3);
	reg_in(OFFSET4);
	reg_in(EXT_LEVEL);
	reg_in(RESERVE);
	reg_in(DAC_CHN_TIME);
	reg_in(MISC_PROBE_EN);
	reg_in(MISC_TRIGOUT_SEL);
	reg_in(VERSION);
	reg_in(CFG_1WIRE);
	reg_in(PROBE_CH1);
	reg_in(PROBE_CH2);
	reg_in(PROBE_CH3);
	reg_in(PROBE_CH4);
	reg_in(PROBE_WAIT);
	reg_in(DG_RD);
	reg_in(dbg_search_event_num_latch);
	reg_in(dbg_search_frame_lenth_latch);

	*proxy = K7mFcuMemProxy(*this);
}
void K7mFcuMem::setLA_LEVEL1_LA_LEVEL1( K7mFcu_reg value )
{
	reg_assign_field(LA_LEVEL1,LA_LEVEL1,value);
}

void K7mFcuMem::setLA_LEVEL1( K7mFcu_reg value )
{
	reg_assign_field(LA_LEVEL1,payload,value);
}

void K7mFcuMem::outLA_LEVEL1_LA_LEVEL1( K7mFcu_reg value )
{
	out_assign_field(LA_LEVEL1,LA_LEVEL1,value);
}
void K7mFcuMem::pulseLA_LEVEL1_LA_LEVEL1( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(LA_LEVEL1,LA_LEVEL1,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_LEVEL1,LA_LEVEL1,idleV);
}

void K7mFcuMem::outLA_LEVEL1( K7mFcu_reg value )
{
	out_assign_field(LA_LEVEL1,payload,value);
}

void K7mFcuMem::outLA_LEVEL1()
{
	out_member(LA_LEVEL1);
}


K7mFcu_reg K7mFcuMem::inLA_LEVEL1()
{
	reg_in(LA_LEVEL1);
	return LA_LEVEL1.payload;
}


void K7mFcuMem::setLA_LEVEL2_LA_LEVEL2( K7mFcu_reg value )
{
	reg_assign_field(LA_LEVEL2,LA_LEVEL2,value);
}

void K7mFcuMem::setLA_LEVEL2( K7mFcu_reg value )
{
	reg_assign_field(LA_LEVEL2,payload,value);
}

void K7mFcuMem::outLA_LEVEL2_LA_LEVEL2( K7mFcu_reg value )
{
	out_assign_field(LA_LEVEL2,LA_LEVEL2,value);
}
void K7mFcuMem::pulseLA_LEVEL2_LA_LEVEL2( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(LA_LEVEL2,LA_LEVEL2,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_LEVEL2,LA_LEVEL2,idleV);
}

void K7mFcuMem::outLA_LEVEL2( K7mFcu_reg value )
{
	out_assign_field(LA_LEVEL2,payload,value);
}

void K7mFcuMem::outLA_LEVEL2()
{
	out_member(LA_LEVEL2);
}


K7mFcu_reg K7mFcuMem::inLA_LEVEL2()
{
	reg_in(LA_LEVEL2);
	return LA_LEVEL2.payload;
}


void K7mFcuMem::setFG1_OFFSET_FG1_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(FG1_OFFSET,FG1_OFFSET,value);
}

void K7mFcuMem::setFG1_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(FG1_OFFSET,payload,value);
}

void K7mFcuMem::outFG1_OFFSET_FG1_OFFSET( K7mFcu_reg value )
{
	out_assign_field(FG1_OFFSET,FG1_OFFSET,value);
}
void K7mFcuMem::pulseFG1_OFFSET_FG1_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(FG1_OFFSET,FG1_OFFSET,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FG1_OFFSET,FG1_OFFSET,idleV);
}

void K7mFcuMem::outFG1_OFFSET( K7mFcu_reg value )
{
	out_assign_field(FG1_OFFSET,payload,value);
}

void K7mFcuMem::outFG1_OFFSET()
{
	out_member(FG1_OFFSET);
}


K7mFcu_reg K7mFcuMem::inFG1_OFFSET()
{
	reg_in(FG1_OFFSET);
	return FG1_OFFSET.payload;
}


void K7mFcuMem::setFG1_REF_FG1_REF( K7mFcu_reg value )
{
	reg_assign_field(FG1_REF,FG1_REF,value);
}

void K7mFcuMem::setFG1_REF( K7mFcu_reg value )
{
	reg_assign_field(FG1_REF,payload,value);
}

void K7mFcuMem::outFG1_REF_FG1_REF( K7mFcu_reg value )
{
	out_assign_field(FG1_REF,FG1_REF,value);
}
void K7mFcuMem::pulseFG1_REF_FG1_REF( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(FG1_REF,FG1_REF,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FG1_REF,FG1_REF,idleV);
}

void K7mFcuMem::outFG1_REF( K7mFcu_reg value )
{
	out_assign_field(FG1_REF,payload,value);
}

void K7mFcuMem::outFG1_REF()
{
	out_member(FG1_REF);
}


K7mFcu_reg K7mFcuMem::inFG1_REF()
{
	reg_in(FG1_REF);
	return FG1_REF.payload;
}


void K7mFcuMem::setFG2_OFFSET_FG2_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(FG2_OFFSET,FG2_OFFSET,value);
}

void K7mFcuMem::setFG2_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(FG2_OFFSET,payload,value);
}

void K7mFcuMem::outFG2_OFFSET_FG2_OFFSET( K7mFcu_reg value )
{
	out_assign_field(FG2_OFFSET,FG2_OFFSET,value);
}
void K7mFcuMem::pulseFG2_OFFSET_FG2_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(FG2_OFFSET,FG2_OFFSET,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FG2_OFFSET,FG2_OFFSET,idleV);
}

void K7mFcuMem::outFG2_OFFSET( K7mFcu_reg value )
{
	out_assign_field(FG2_OFFSET,payload,value);
}

void K7mFcuMem::outFG2_OFFSET()
{
	out_member(FG2_OFFSET);
}


K7mFcu_reg K7mFcuMem::inFG2_OFFSET()
{
	reg_in(FG2_OFFSET);
	return FG2_OFFSET.payload;
}


void K7mFcuMem::setFG2_REF_FG2_REF( K7mFcu_reg value )
{
	reg_assign_field(FG2_REF,FG2_REF,value);
}

void K7mFcuMem::setFG2_REF( K7mFcu_reg value )
{
	reg_assign_field(FG2_REF,payload,value);
}

void K7mFcuMem::outFG2_REF_FG2_REF( K7mFcu_reg value )
{
	out_assign_field(FG2_REF,FG2_REF,value);
}
void K7mFcuMem::pulseFG2_REF_FG2_REF( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(FG2_REF,FG2_REF,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FG2_REF,FG2_REF,idleV);
}

void K7mFcuMem::outFG2_REF( K7mFcu_reg value )
{
	out_assign_field(FG2_REF,payload,value);
}

void K7mFcuMem::outFG2_REF()
{
	out_member(FG2_REF);
}


K7mFcu_reg K7mFcuMem::inFG2_REF()
{
	reg_in(FG2_REF);
	return FG2_REF.payload;
}


void K7mFcuMem::setPROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE1_OFFSET,PROBE1_OFFSET,value);
}

void K7mFcuMem::setPROBE1_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE1_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE1_OFFSET,PROBE1_OFFSET,value);
}
void K7mFcuMem::pulsePROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE1_OFFSET,PROBE1_OFFSET,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE1_OFFSET,PROBE1_OFFSET,idleV);
}

void K7mFcuMem::outPROBE1_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE1_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE1_OFFSET()
{
	out_member(PROBE1_OFFSET);
}


K7mFcu_reg K7mFcuMem::inPROBE1_OFFSET()
{
	reg_in(PROBE1_OFFSET);
	return PROBE1_OFFSET.payload;
}


void K7mFcuMem::setPROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE2_OFFSET,PROBE2_OFFSET,value);
}

void K7mFcuMem::setPROBE2_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE2_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE2_OFFSET,PROBE2_OFFSET,value);
}
void K7mFcuMem::pulsePROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE2_OFFSET,PROBE2_OFFSET,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE2_OFFSET,PROBE2_OFFSET,idleV);
}

void K7mFcuMem::outPROBE2_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE2_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE2_OFFSET()
{
	out_member(PROBE2_OFFSET);
}


K7mFcu_reg K7mFcuMem::inPROBE2_OFFSET()
{
	reg_in(PROBE2_OFFSET);
	return PROBE2_OFFSET.payload;
}


void K7mFcuMem::setPROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE3_OFFSET,PROBE3_OFFSET,value);
}

void K7mFcuMem::setPROBE3_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE3_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE3_OFFSET,PROBE3_OFFSET,value);
}
void K7mFcuMem::pulsePROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE3_OFFSET,PROBE3_OFFSET,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE3_OFFSET,PROBE3_OFFSET,idleV);
}

void K7mFcuMem::outPROBE3_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE3_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE3_OFFSET()
{
	out_member(PROBE3_OFFSET);
}


K7mFcu_reg K7mFcuMem::inPROBE3_OFFSET()
{
	reg_in(PROBE3_OFFSET);
	return PROBE3_OFFSET.payload;
}


void K7mFcuMem::setPROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE4_OFFSET,PROBE4_OFFSET,value);
}

void K7mFcuMem::setPROBE4_OFFSET( K7mFcu_reg value )
{
	reg_assign_field(PROBE4_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE4_OFFSET,PROBE4_OFFSET,value);
}
void K7mFcuMem::pulsePROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE4_OFFSET,PROBE4_OFFSET,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE4_OFFSET,PROBE4_OFFSET,idleV);
}

void K7mFcuMem::outPROBE4_OFFSET( K7mFcu_reg value )
{
	out_assign_field(PROBE4_OFFSET,payload,value);
}

void K7mFcuMem::outPROBE4_OFFSET()
{
	out_member(PROBE4_OFFSET);
}


K7mFcu_reg K7mFcuMem::inPROBE4_OFFSET()
{
	reg_in(PROBE4_OFFSET);
	return PROBE4_OFFSET.payload;
}


void K7mFcuMem::setOFFSET1_OFFSET1( K7mFcu_reg value )
{
	reg_assign_field(OFFSET1,OFFSET1,value);
}

void K7mFcuMem::setOFFSET1( K7mFcu_reg value )
{
	reg_assign_field(OFFSET1,payload,value);
}

void K7mFcuMem::outOFFSET1_OFFSET1( K7mFcu_reg value )
{
	out_assign_field(OFFSET1,OFFSET1,value);
}
void K7mFcuMem::pulseOFFSET1_OFFSET1( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(OFFSET1,OFFSET1,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(OFFSET1,OFFSET1,idleV);
}

void K7mFcuMem::outOFFSET1( K7mFcu_reg value )
{
	out_assign_field(OFFSET1,payload,value);
}

void K7mFcuMem::outOFFSET1()
{
	out_member(OFFSET1);
}


K7mFcu_reg K7mFcuMem::inOFFSET1()
{
	reg_in(OFFSET1);
	return OFFSET1.payload;
}


void K7mFcuMem::setOFFSET2_OFFSET2( K7mFcu_reg value )
{
	reg_assign_field(OFFSET2,OFFSET2,value);
}

void K7mFcuMem::setOFFSET2( K7mFcu_reg value )
{
	reg_assign_field(OFFSET2,payload,value);
}

void K7mFcuMem::outOFFSET2_OFFSET2( K7mFcu_reg value )
{
	out_assign_field(OFFSET2,OFFSET2,value);
}
void K7mFcuMem::pulseOFFSET2_OFFSET2( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(OFFSET2,OFFSET2,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(OFFSET2,OFFSET2,idleV);
}

void K7mFcuMem::outOFFSET2( K7mFcu_reg value )
{
	out_assign_field(OFFSET2,payload,value);
}

void K7mFcuMem::outOFFSET2()
{
	out_member(OFFSET2);
}


K7mFcu_reg K7mFcuMem::inOFFSET2()
{
	reg_in(OFFSET2);
	return OFFSET2.payload;
}


void K7mFcuMem::setOFFSET3_OFFSET3( K7mFcu_reg value )
{
	reg_assign_field(OFFSET3,OFFSET3,value);
}

void K7mFcuMem::setOFFSET3( K7mFcu_reg value )
{
	reg_assign_field(OFFSET3,payload,value);
}

void K7mFcuMem::outOFFSET3_OFFSET3( K7mFcu_reg value )
{
	out_assign_field(OFFSET3,OFFSET3,value);
}
void K7mFcuMem::pulseOFFSET3_OFFSET3( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(OFFSET3,OFFSET3,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(OFFSET3,OFFSET3,idleV);
}

void K7mFcuMem::outOFFSET3( K7mFcu_reg value )
{
	out_assign_field(OFFSET3,payload,value);
}

void K7mFcuMem::outOFFSET3()
{
	out_member(OFFSET3);
}


K7mFcu_reg K7mFcuMem::inOFFSET3()
{
	reg_in(OFFSET3);
	return OFFSET3.payload;
}


void K7mFcuMem::setOFFSET4_OFFSET4( K7mFcu_reg value )
{
	reg_assign_field(OFFSET4,OFFSET4,value);
}

void K7mFcuMem::setOFFSET4( K7mFcu_reg value )
{
	reg_assign_field(OFFSET4,payload,value);
}

void K7mFcuMem::outOFFSET4_OFFSET4( K7mFcu_reg value )
{
	out_assign_field(OFFSET4,OFFSET4,value);
}
void K7mFcuMem::pulseOFFSET4_OFFSET4( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(OFFSET4,OFFSET4,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(OFFSET4,OFFSET4,idleV);
}

void K7mFcuMem::outOFFSET4( K7mFcu_reg value )
{
	out_assign_field(OFFSET4,payload,value);
}

void K7mFcuMem::outOFFSET4()
{
	out_member(OFFSET4);
}


K7mFcu_reg K7mFcuMem::inOFFSET4()
{
	reg_in(OFFSET4);
	return OFFSET4.payload;
}


void K7mFcuMem::setEXT_LEVEL_EXT_LEVEL( K7mFcu_reg value )
{
	reg_assign_field(EXT_LEVEL,EXT_LEVEL,value);
}

void K7mFcuMem::setEXT_LEVEL( K7mFcu_reg value )
{
	reg_assign_field(EXT_LEVEL,payload,value);
}

void K7mFcuMem::outEXT_LEVEL_EXT_LEVEL( K7mFcu_reg value )
{
	out_assign_field(EXT_LEVEL,EXT_LEVEL,value);
}
void K7mFcuMem::pulseEXT_LEVEL_EXT_LEVEL( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(EXT_LEVEL,EXT_LEVEL,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(EXT_LEVEL,EXT_LEVEL,idleV);
}

void K7mFcuMem::outEXT_LEVEL( K7mFcu_reg value )
{
	out_assign_field(EXT_LEVEL,payload,value);
}

void K7mFcuMem::outEXT_LEVEL()
{
	out_member(EXT_LEVEL);
}


K7mFcu_reg K7mFcuMem::inEXT_LEVEL()
{
	reg_in(EXT_LEVEL);
	return EXT_LEVEL.payload;
}


void K7mFcuMem::setRESERVE_RESERVE( K7mFcu_reg value )
{
	reg_assign_field(RESERVE,RESERVE,value);
}

void K7mFcuMem::setRESERVE( K7mFcu_reg value )
{
	reg_assign_field(RESERVE,payload,value);
}

void K7mFcuMem::outRESERVE_RESERVE( K7mFcu_reg value )
{
	out_assign_field(RESERVE,RESERVE,value);
}
void K7mFcuMem::pulseRESERVE_RESERVE( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(RESERVE,RESERVE,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(RESERVE,RESERVE,idleV);
}

void K7mFcuMem::outRESERVE( K7mFcu_reg value )
{
	out_assign_field(RESERVE,payload,value);
}

void K7mFcuMem::outRESERVE()
{
	out_member(RESERVE);
}


K7mFcu_reg K7mFcuMem::inRESERVE()
{
	reg_in(RESERVE);
	return RESERVE.payload;
}


void K7mFcuMem::setDAC_CHN_TIME_holdtime( K7mFcu_reg value )
{
	reg_assign_field(DAC_CHN_TIME,holdtime,value);
}

void K7mFcuMem::setDAC_CHN_TIME( K7mFcu_reg value )
{
	reg_assign_field(DAC_CHN_TIME,payload,value);
}

void K7mFcuMem::outDAC_CHN_TIME_holdtime( K7mFcu_reg value )
{
	out_assign_field(DAC_CHN_TIME,holdtime,value);
}
void K7mFcuMem::pulseDAC_CHN_TIME_holdtime( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(DAC_CHN_TIME,holdtime,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DAC_CHN_TIME,holdtime,idleV);
}

void K7mFcuMem::outDAC_CHN_TIME( K7mFcu_reg value )
{
	out_assign_field(DAC_CHN_TIME,payload,value);
}

void K7mFcuMem::outDAC_CHN_TIME()
{
	out_member(DAC_CHN_TIME);
}


K7mFcu_reg K7mFcuMem::inDAC_CHN_TIME()
{
	reg_in(DAC_CHN_TIME);
	return DAC_CHN_TIME.payload;
}


void K7mFcuMem::setMISC_PROBE_EN_en( K7mFcu_reg value )
{
	reg_assign_field(MISC_PROBE_EN,en,value);
}
void K7mFcuMem::setMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg value )
{
	reg_assign_field(MISC_PROBE_EN,sel_1K_10K,value);
}

void K7mFcuMem::setMISC_PROBE_EN( K7mFcu_reg value )
{
	reg_assign_field(MISC_PROBE_EN,payload,value);
}

void K7mFcuMem::outMISC_PROBE_EN_en( K7mFcu_reg value )
{
	out_assign_field(MISC_PROBE_EN,en,value);
}
void K7mFcuMem::pulseMISC_PROBE_EN_en( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(MISC_PROBE_EN,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MISC_PROBE_EN,en,idleV);
}
void K7mFcuMem::outMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg value )
{
	out_assign_field(MISC_PROBE_EN,sel_1K_10K,value);
}
void K7mFcuMem::pulseMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(MISC_PROBE_EN,sel_1K_10K,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MISC_PROBE_EN,sel_1K_10K,idleV);
}

void K7mFcuMem::outMISC_PROBE_EN( K7mFcu_reg value )
{
	out_assign_field(MISC_PROBE_EN,payload,value);
}

void K7mFcuMem::outMISC_PROBE_EN()
{
	out_member(MISC_PROBE_EN);
}


K7mFcu_reg K7mFcuMem::inMISC_PROBE_EN()
{
	reg_in(MISC_PROBE_EN);
	return MISC_PROBE_EN.payload;
}


void K7mFcuMem::setMISC_TRIGOUT_SEL_sel( K7mFcu_reg value )
{
	reg_assign_field(MISC_TRIGOUT_SEL,sel,value);
}

void K7mFcuMem::setMISC_TRIGOUT_SEL( K7mFcu_reg value )
{
	reg_assign_field(MISC_TRIGOUT_SEL,payload,value);
}

void K7mFcuMem::outMISC_TRIGOUT_SEL_sel( K7mFcu_reg value )
{
	out_assign_field(MISC_TRIGOUT_SEL,sel,value);
}
void K7mFcuMem::pulseMISC_TRIGOUT_SEL_sel( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(MISC_TRIGOUT_SEL,sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MISC_TRIGOUT_SEL,sel,idleV);
}

void K7mFcuMem::outMISC_TRIGOUT_SEL( K7mFcu_reg value )
{
	out_assign_field(MISC_TRIGOUT_SEL,payload,value);
}

void K7mFcuMem::outMISC_TRIGOUT_SEL()
{
	out_member(MISC_TRIGOUT_SEL);
}


K7mFcu_reg K7mFcuMem::inMISC_TRIGOUT_SEL()
{
	reg_in(MISC_TRIGOUT_SEL);
	return MISC_TRIGOUT_SEL.payload;
}


K7mFcu_reg K7mFcuMem::inVERSION()
{
	reg_in(VERSION);
	return VERSION;
}


void K7mFcuMem::setCFG_1WIRE_bit_start( K7mFcu_reg value )
{
	reg_assign_field(CFG_1WIRE,bit_start,value);
}
void K7mFcuMem::setCFG_1WIRE_bit_tx( K7mFcu_reg value )
{
	reg_assign_field(CFG_1WIRE,bit_tx,value);
}
void K7mFcuMem::setCFG_1WIRE_bit_rx( K7mFcu_reg value )
{
	reg_assign_field(CFG_1WIRE,bit_rx,value);
}
void K7mFcuMem::setCFG_1WIRE_bit_period( K7mFcu_reg value )
{
	reg_assign_field(CFG_1WIRE,bit_period,value);
}

void K7mFcuMem::setCFG_1WIRE( K7mFcu_reg value )
{
	reg_assign_field(CFG_1WIRE,payload,value);
}

void K7mFcuMem::outCFG_1WIRE_bit_start( K7mFcu_reg value )
{
	out_assign_field(CFG_1WIRE,bit_start,value);
}
void K7mFcuMem::pulseCFG_1WIRE_bit_start( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(CFG_1WIRE,bit_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CFG_1WIRE,bit_start,idleV);
}
void K7mFcuMem::outCFG_1WIRE_bit_tx( K7mFcu_reg value )
{
	out_assign_field(CFG_1WIRE,bit_tx,value);
}
void K7mFcuMem::pulseCFG_1WIRE_bit_tx( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(CFG_1WIRE,bit_tx,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CFG_1WIRE,bit_tx,idleV);
}
void K7mFcuMem::outCFG_1WIRE_bit_rx( K7mFcu_reg value )
{
	out_assign_field(CFG_1WIRE,bit_rx,value);
}
void K7mFcuMem::pulseCFG_1WIRE_bit_rx( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(CFG_1WIRE,bit_rx,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CFG_1WIRE,bit_rx,idleV);
}
void K7mFcuMem::outCFG_1WIRE_bit_period( K7mFcu_reg value )
{
	out_assign_field(CFG_1WIRE,bit_period,value);
}
void K7mFcuMem::pulseCFG_1WIRE_bit_period( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(CFG_1WIRE,bit_period,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CFG_1WIRE,bit_period,idleV);
}

void K7mFcuMem::outCFG_1WIRE( K7mFcu_reg value )
{
	out_assign_field(CFG_1WIRE,payload,value);
}

void K7mFcuMem::outCFG_1WIRE()
{
	out_member(CFG_1WIRE);
}


K7mFcu_reg K7mFcuMem::inCFG_1WIRE()
{
	reg_in(CFG_1WIRE);
	return CFG_1WIRE.payload;
}


void K7mFcuMem::setPROBE_CH1_rd_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,rd_data,value);
}
void K7mFcuMem::setPROBE_CH1_rd_data_vld( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,rd_data_vld,value);
}
void K7mFcuMem::setPROBE_CH1_rx_len( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,rx_len,value);
}
void K7mFcuMem::setPROBE_CH1_wr_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,wr_data,value);
}
void K7mFcuMem::setPROBE_CH1_wr_data_en( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,wr_data_en,value);
}
void K7mFcuMem::setPROBE_CH1_cmd_type( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,cmd_type,value);
}
void K7mFcuMem::setPROBE_CH1_clr( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,clr,value);
}

void K7mFcuMem::setPROBE_CH1( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH1,payload,value);
}

void K7mFcuMem::outPROBE_CH1_rd_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,rd_data,value);
}
void K7mFcuMem::pulsePROBE_CH1_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH1,rd_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH1,rd_data,idleV);
}
void K7mFcuMem::outPROBE_CH1_rd_data_vld( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,rd_data_vld,value);
}
void K7mFcuMem::pulsePROBE_CH1_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH1,rd_data_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH1,rd_data_vld,idleV);
}
void K7mFcuMem::outPROBE_CH1_rx_len( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,rx_len,value);
}
void K7mFcuMem::pulsePROBE_CH1_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH1,rx_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH1,rx_len,idleV);
}
void K7mFcuMem::outPROBE_CH1_wr_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,wr_data,value);
}
void K7mFcuMem::pulsePROBE_CH1_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH1,wr_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH1,wr_data,idleV);
}
void K7mFcuMem::outPROBE_CH1_wr_data_en( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,wr_data_en,value);
}
void K7mFcuMem::pulsePROBE_CH1_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH1,wr_data_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH1,wr_data_en,idleV);
}
void K7mFcuMem::outPROBE_CH1_cmd_type( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,cmd_type,value);
}
void K7mFcuMem::pulsePROBE_CH1_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH1,cmd_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH1,cmd_type,idleV);
}
void K7mFcuMem::outPROBE_CH1_clr( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,clr,value);
}
void K7mFcuMem::pulsePROBE_CH1_clr( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH1,clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH1,clr,idleV);
}

void K7mFcuMem::outPROBE_CH1( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH1,payload,value);
}

void K7mFcuMem::outPROBE_CH1()
{
	out_member(PROBE_CH1);
}


K7mFcu_reg K7mFcuMem::inPROBE_CH1()
{
	reg_in(PROBE_CH1);
	return PROBE_CH1.payload;
}


void K7mFcuMem::setPROBE_CH2_rd_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,rd_data,value);
}
void K7mFcuMem::setPROBE_CH2_rd_data_vld( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,rd_data_vld,value);
}
void K7mFcuMem::setPROBE_CH2_rx_len( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,rx_len,value);
}
void K7mFcuMem::setPROBE_CH2_wr_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,wr_data,value);
}
void K7mFcuMem::setPROBE_CH2_wr_data_en( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,wr_data_en,value);
}
void K7mFcuMem::setPROBE_CH2_cmd_type( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,cmd_type,value);
}
void K7mFcuMem::setPROBE_CH2_clr( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,clr,value);
}

void K7mFcuMem::setPROBE_CH2( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH2,payload,value);
}

void K7mFcuMem::outPROBE_CH2_rd_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,rd_data,value);
}
void K7mFcuMem::pulsePROBE_CH2_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH2,rd_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH2,rd_data,idleV);
}
void K7mFcuMem::outPROBE_CH2_rd_data_vld( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,rd_data_vld,value);
}
void K7mFcuMem::pulsePROBE_CH2_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH2,rd_data_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH2,rd_data_vld,idleV);
}
void K7mFcuMem::outPROBE_CH2_rx_len( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,rx_len,value);
}
void K7mFcuMem::pulsePROBE_CH2_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH2,rx_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH2,rx_len,idleV);
}
void K7mFcuMem::outPROBE_CH2_wr_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,wr_data,value);
}
void K7mFcuMem::pulsePROBE_CH2_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH2,wr_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH2,wr_data,idleV);
}
void K7mFcuMem::outPROBE_CH2_wr_data_en( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,wr_data_en,value);
}
void K7mFcuMem::pulsePROBE_CH2_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH2,wr_data_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH2,wr_data_en,idleV);
}
void K7mFcuMem::outPROBE_CH2_cmd_type( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,cmd_type,value);
}
void K7mFcuMem::pulsePROBE_CH2_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH2,cmd_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH2,cmd_type,idleV);
}
void K7mFcuMem::outPROBE_CH2_clr( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,clr,value);
}
void K7mFcuMem::pulsePROBE_CH2_clr( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH2,clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH2,clr,idleV);
}

void K7mFcuMem::outPROBE_CH2( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH2,payload,value);
}

void K7mFcuMem::outPROBE_CH2()
{
	out_member(PROBE_CH2);
}


K7mFcu_reg K7mFcuMem::inPROBE_CH2()
{
	reg_in(PROBE_CH2);
	return PROBE_CH2.payload;
}


void K7mFcuMem::setPROBE_CH3_rd_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,rd_data,value);
}
void K7mFcuMem::setPROBE_CH3_rd_data_vld( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,rd_data_vld,value);
}
void K7mFcuMem::setPROBE_CH3_rx_len( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,rx_len,value);
}
void K7mFcuMem::setPROBE_CH3_wr_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,wr_data,value);
}
void K7mFcuMem::setPROBE_CH3_wr_data_en( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,wr_data_en,value);
}
void K7mFcuMem::setPROBE_CH3_cmd_type( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,cmd_type,value);
}
void K7mFcuMem::setPROBE_CH3_clr( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,clr,value);
}

void K7mFcuMem::setPROBE_CH3( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH3,payload,value);
}

void K7mFcuMem::outPROBE_CH3_rd_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,rd_data,value);
}
void K7mFcuMem::pulsePROBE_CH3_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH3,rd_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH3,rd_data,idleV);
}
void K7mFcuMem::outPROBE_CH3_rd_data_vld( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,rd_data_vld,value);
}
void K7mFcuMem::pulsePROBE_CH3_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH3,rd_data_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH3,rd_data_vld,idleV);
}
void K7mFcuMem::outPROBE_CH3_rx_len( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,rx_len,value);
}
void K7mFcuMem::pulsePROBE_CH3_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH3,rx_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH3,rx_len,idleV);
}
void K7mFcuMem::outPROBE_CH3_wr_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,wr_data,value);
}
void K7mFcuMem::pulsePROBE_CH3_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH3,wr_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH3,wr_data,idleV);
}
void K7mFcuMem::outPROBE_CH3_wr_data_en( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,wr_data_en,value);
}
void K7mFcuMem::pulsePROBE_CH3_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH3,wr_data_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH3,wr_data_en,idleV);
}
void K7mFcuMem::outPROBE_CH3_cmd_type( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,cmd_type,value);
}
void K7mFcuMem::pulsePROBE_CH3_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH3,cmd_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH3,cmd_type,idleV);
}
void K7mFcuMem::outPROBE_CH3_clr( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,clr,value);
}
void K7mFcuMem::pulsePROBE_CH3_clr( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH3,clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH3,clr,idleV);
}

void K7mFcuMem::outPROBE_CH3( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH3,payload,value);
}

void K7mFcuMem::outPROBE_CH3()
{
	out_member(PROBE_CH3);
}


K7mFcu_reg K7mFcuMem::inPROBE_CH3()
{
	reg_in(PROBE_CH3);
	return PROBE_CH3.payload;
}


void K7mFcuMem::setPROBE_CH4_rd_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,rd_data,value);
}
void K7mFcuMem::setPROBE_CH4_rd_data_vld( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,rd_data_vld,value);
}
void K7mFcuMem::setPROBE_CH4_rx_len( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,rx_len,value);
}
void K7mFcuMem::setPROBE_CH4_wr_data( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,wr_data,value);
}
void K7mFcuMem::setPROBE_CH4_wr_data_en( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,wr_data_en,value);
}
void K7mFcuMem::setPROBE_CH4_cmd_type( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,cmd_type,value);
}
void K7mFcuMem::setPROBE_CH4_clr( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,clr,value);
}

void K7mFcuMem::setPROBE_CH4( K7mFcu_reg value )
{
	reg_assign_field(PROBE_CH4,payload,value);
}

void K7mFcuMem::outPROBE_CH4_rd_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,rd_data,value);
}
void K7mFcuMem::pulsePROBE_CH4_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH4,rd_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH4,rd_data,idleV);
}
void K7mFcuMem::outPROBE_CH4_rd_data_vld( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,rd_data_vld,value);
}
void K7mFcuMem::pulsePROBE_CH4_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH4,rd_data_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH4,rd_data_vld,idleV);
}
void K7mFcuMem::outPROBE_CH4_rx_len( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,rx_len,value);
}
void K7mFcuMem::pulsePROBE_CH4_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH4,rx_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH4,rx_len,idleV);
}
void K7mFcuMem::outPROBE_CH4_wr_data( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,wr_data,value);
}
void K7mFcuMem::pulsePROBE_CH4_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH4,wr_data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH4,wr_data,idleV);
}
void K7mFcuMem::outPROBE_CH4_wr_data_en( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,wr_data_en,value);
}
void K7mFcuMem::pulsePROBE_CH4_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH4,wr_data_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH4,wr_data_en,idleV);
}
void K7mFcuMem::outPROBE_CH4_cmd_type( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,cmd_type,value);
}
void K7mFcuMem::pulsePROBE_CH4_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH4,cmd_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH4,cmd_type,idleV);
}
void K7mFcuMem::outPROBE_CH4_clr( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,clr,value);
}
void K7mFcuMem::pulsePROBE_CH4_clr( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(PROBE_CH4,clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PROBE_CH4,clr,idleV);
}

void K7mFcuMem::outPROBE_CH4( K7mFcu_reg value )
{
	out_assign_field(PROBE_CH4,payload,value);
}

void K7mFcuMem::outPROBE_CH4()
{
	out_member(PROBE_CH4);
}


K7mFcu_reg K7mFcuMem::inPROBE_CH4()
{
	reg_in(PROBE_CH4);
	return PROBE_CH4.payload;
}


void K7mFcuMem::setPROBE_WAIT( K7mFcu_reg value )
{
	reg_assign(PROBE_WAIT,value);
}

void K7mFcuMem::outPROBE_WAIT( K7mFcu_reg value )
{
	out_assign(PROBE_WAIT,value);
}

void K7mFcuMem::outPROBE_WAIT()
{
	out_member(PROBE_WAIT);
}


K7mFcu_reg K7mFcuMem::inPROBE_WAIT()
{
	reg_in(PROBE_WAIT);
	return PROBE_WAIT;
}


void K7mFcuMem::setDG_CFG_data( K7mFcu_reg value )
{
	reg_assign_field(DG_CFG,data,value);
}
void K7mFcuMem::setDG_CFG_addr( K7mFcu_reg value )
{
	reg_assign_field(DG_CFG,addr,value);
}

void K7mFcuMem::setDG_CFG( K7mFcu_reg value )
{
	reg_assign_field(DG_CFG,payload,value);
}

void K7mFcuMem::outDG_CFG_data( K7mFcu_reg value )
{
	out_assign_field(DG_CFG,data,value);
}
void K7mFcuMem::pulseDG_CFG_data( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(DG_CFG,data,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DG_CFG,data,idleV);
}
void K7mFcuMem::outDG_CFG_addr( K7mFcu_reg value )
{
	out_assign_field(DG_CFG,addr,value);
}
void K7mFcuMem::pulseDG_CFG_addr( K7mFcu_reg activeV, K7mFcu_reg idleV, int timeus )
{
	out_assign_field(DG_CFG,addr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DG_CFG,addr,idleV);
}

void K7mFcuMem::outDG_CFG( K7mFcu_reg value )
{
	out_assign_field(DG_CFG,payload,value);
}

void K7mFcuMem::outDG_CFG()
{
	out_member(DG_CFG);
}


K7mFcu_reg K7mFcuMem::inDG_RD()
{
	reg_in(DG_RD);
	return DG_RD;
}


K7mFcu_reg K7mFcuMem::indbg_search_event_num_latch()
{
	reg_in(dbg_search_event_num_latch);
	return dbg_search_event_num_latch;
}


K7mFcu_reg K7mFcuMem::indbg_search_frame_lenth_latch()
{
	reg_in(dbg_search_frame_lenth_latch);
	return dbg_search_frame_lenth_latch;
}


cache_addr K7mFcuMem::addr_LA_LEVEL1(){ return 0x2000+mAddrBase; }
cache_addr K7mFcuMem::addr_LA_LEVEL2(){ return 0x2004+mAddrBase; }
cache_addr K7mFcuMem::addr_FG1_OFFSET(){ return 0x2008+mAddrBase; }
cache_addr K7mFcuMem::addr_FG1_REF(){ return 0x200C+mAddrBase; }

cache_addr K7mFcuMem::addr_FG2_OFFSET(){ return 0x2010+mAddrBase; }
cache_addr K7mFcuMem::addr_FG2_REF(){ return 0x2014+mAddrBase; }
cache_addr K7mFcuMem::addr_PROBE1_OFFSET(){ return 0x2018+mAddrBase; }
cache_addr K7mFcuMem::addr_PROBE2_OFFSET(){ return 0x201C+mAddrBase; }

cache_addr K7mFcuMem::addr_PROBE3_OFFSET(){ return 0x2020+mAddrBase; }
cache_addr K7mFcuMem::addr_PROBE4_OFFSET(){ return 0x2024+mAddrBase; }
cache_addr K7mFcuMem::addr_OFFSET1(){ return 0x2028+mAddrBase; }
cache_addr K7mFcuMem::addr_OFFSET2(){ return 0x202C+mAddrBase; }

cache_addr K7mFcuMem::addr_OFFSET3(){ return 0x2030+mAddrBase; }
cache_addr K7mFcuMem::addr_OFFSET4(){ return 0x2034+mAddrBase; }
cache_addr K7mFcuMem::addr_EXT_LEVEL(){ return 0x2038+mAddrBase; }
cache_addr K7mFcuMem::addr_RESERVE(){ return 0x203C+mAddrBase; }

cache_addr K7mFcuMem::addr_DAC_CHN_TIME(){ return 0x2040+mAddrBase; }
cache_addr K7mFcuMem::addr_MISC_PROBE_EN(){ return 0x2044+mAddrBase; }
cache_addr K7mFcuMem::addr_MISC_TRIGOUT_SEL(){ return 0x2048+mAddrBase; }
cache_addr K7mFcuMem::addr_VERSION(){ return 0x2080+mAddrBase; }

cache_addr K7mFcuMem::addr_CFG_1WIRE(){ return 0x2090+mAddrBase; }
cache_addr K7mFcuMem::addr_PROBE_CH1(){ return 0x2094+mAddrBase; }
cache_addr K7mFcuMem::addr_PROBE_CH2(){ return 0x2098+mAddrBase; }
cache_addr K7mFcuMem::addr_PROBE_CH3(){ return 0x209C+mAddrBase; }

cache_addr K7mFcuMem::addr_PROBE_CH4(){ return 0x20A0+mAddrBase; }
cache_addr K7mFcuMem::addr_PROBE_WAIT(){ return 0x20A4+mAddrBase; }
cache_addr K7mFcuMem::addr_DG_CFG(){ return 0x20C0+mAddrBase; }
cache_addr K7mFcuMem::addr_DG_RD(){ return 0x20C4+mAddrBase; }

cache_addr K7mFcuMem::addr_dbg_search_event_num_latch(){ return 0x2ea8+mAddrBase; }
cache_addr K7mFcuMem::addr_dbg_search_frame_lenth_latch(){ return 0x2e90+mAddrBase; }


