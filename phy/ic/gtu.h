#ifndef _GTU_IC_H_
#define _GTU_IC_H_

#define Gtu_reg unsigned int

union Gtu_self_gtu_state{
	struct{
	Gtu_reg pdu_full:1;
	Gtu_reg ufc_full:1;
	};
	Gtu_reg payload;
};

union Gtu_receiver_state{
	struct{
	Gtu_reg pdu_full:1;
	Gtu_reg ufc_full:1;
	};
	Gtu_reg payload;
};

union Gtu_gt_buf_clr{
	struct{
	Gtu_reg pdu_rx_buf_clr:1;
	Gtu_reg pdu_tx_buf_clr:1;
	Gtu_reg ufc_rx_buf_clr:1;
	Gtu_reg ufc_tx_buf_clr:1;
	};
	Gtu_reg payload;
};

struct GtuMemProxy {
	Gtu_reg VERSION;
	Gtu_reg Soft_reset;
	Gtu_reg TEST;
	Gtu_reg gt_channel_up;

	Gtu_self_gtu_state self_gtu_state; 
	Gtu_receiver_state receiver_state; 
	Gtu_gt_buf_clr gt_buf_clr; 
	Gtu_reg getVERSION(  );

	void assignSoft_reset( Gtu_reg value );

	void assignTEST( Gtu_reg value );

	Gtu_reg getTEST(  );

	Gtu_reg getgt_channel_up(  );

	Gtu_reg getself_gtu_state_pdu_full(  );
	Gtu_reg getself_gtu_state_ufc_full(  );

	Gtu_reg getself_gtu_state(  );

	Gtu_reg getreceiver_state_pdu_full(  );
	Gtu_reg getreceiver_state_ufc_full(  );

	Gtu_reg getreceiver_state(  );

	void assigngt_buf_clr_pdu_rx_buf_clr( Gtu_reg value  );
	void assigngt_buf_clr_pdu_tx_buf_clr( Gtu_reg value  );
	void assigngt_buf_clr_ufc_rx_buf_clr( Gtu_reg value  );
	void assigngt_buf_clr_ufc_tx_buf_clr( Gtu_reg value  );

	void assigngt_buf_clr( Gtu_reg value );


};
struct GtuMem : public GtuMemProxy, public IPhyFpga{
protected:
	Gtu_reg mAddrBase;
public:
	GtuMem( Gtu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( Gtu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( GtuMemProxy *proxy);
	void pull( GtuMemProxy *proxy);

public:
	Gtu_reg inVERSION(  );

	void setSoft_reset( Gtu_reg value );
	void outSoft_reset( Gtu_reg value );
	void outSoft_reset(  );

	void setTEST( Gtu_reg value );
	void outTEST( Gtu_reg value );
	void outTEST(  );

	Gtu_reg inTEST(  );

	Gtu_reg ingt_channel_up(  );

	Gtu_reg inself_gtu_state(  );

	Gtu_reg inreceiver_state(  );

	void setgt_buf_clr_pdu_rx_buf_clr( Gtu_reg value );
	void setgt_buf_clr_pdu_tx_buf_clr( Gtu_reg value );
	void setgt_buf_clr_ufc_rx_buf_clr( Gtu_reg value );
	void setgt_buf_clr_ufc_tx_buf_clr( Gtu_reg value );

	void setgt_buf_clr( Gtu_reg value );
	void outgt_buf_clr_pdu_rx_buf_clr( Gtu_reg value );
	void pulsegt_buf_clr_pdu_rx_buf_clr( Gtu_reg activeV, Gtu_reg idleV=0, int timeus=0 );
	void outgt_buf_clr_pdu_tx_buf_clr( Gtu_reg value );
	void pulsegt_buf_clr_pdu_tx_buf_clr( Gtu_reg activeV, Gtu_reg idleV=0, int timeus=0 );
	void outgt_buf_clr_ufc_rx_buf_clr( Gtu_reg value );
	void pulsegt_buf_clr_ufc_rx_buf_clr( Gtu_reg activeV, Gtu_reg idleV=0, int timeus=0 );
	void outgt_buf_clr_ufc_tx_buf_clr( Gtu_reg value );
	void pulsegt_buf_clr_ufc_tx_buf_clr( Gtu_reg activeV, Gtu_reg idleV=0, int timeus=0 );

	void outgt_buf_clr( Gtu_reg value );
	void outgt_buf_clr(  );


public:
	cache_addr addr_VERSION();
	cache_addr addr_Soft_reset();
	cache_addr addr_TEST();
	cache_addr addr_gt_channel_up();

	cache_addr addr_self_gtu_state();
	cache_addr addr_receiver_state();
	cache_addr addr_gt_buf_clr();
};
#define _remap_Gtu_()\
	mapIn( &VERSION, 0x0000 + mAddrBase );\
	mapIn( &Soft_reset, 0x0000 + mAddrBase );\
	mapIn( &TEST, 0x0004 + mAddrBase );\
	mapIn( &gt_channel_up, 0x0008 + mAddrBase );\
	\
	mapIn( &self_gtu_state, 0x000C + mAddrBase );\
	mapIn( &receiver_state, 0x0010 + mAddrBase );\
	mapIn( &gt_buf_clr, 0x0014 + mAddrBase );\


#endif
