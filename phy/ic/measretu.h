#ifndef _MEASRETU_IC_H_
#define _MEASRETU_IC_H_

#define MeasRetu_reg unsigned int

union MeasRetu_cha_max_min_top_base{
	struct{
	MeasRetu_reg cha_vmin:8;
	MeasRetu_reg cha_vmax:8;
	MeasRetu_reg cha_vbase:8;
	MeasRetu_reg cha_vtop:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_threshold_l_mid_h{
	struct{
	MeasRetu_reg cha_threshold_l:8;
	MeasRetu_reg cha_threshold_mid:8;
	MeasRetu_reg cha_threshold_h:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_vmax_xloc{
	struct{
	MeasRetu_reg vmax_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmax_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_vmin_xloc{
	struct{
	MeasRetu_reg vmin_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmin_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_rtime{
	struct{
	MeasRetu_reg rtime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_rtime_xloc_end{
	struct{
	MeasRetu_reg rtime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_rtime_pre{
	struct{
	MeasRetu_reg rtime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_rtime_xloc_end_pre{
	struct{
	MeasRetu_reg rtime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ftime{
	struct{
	MeasRetu_reg ftime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ftime_xloc_end{
	struct{
	MeasRetu_reg ftime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ftime_pre{
	struct{
	MeasRetu_reg ftime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ftime_xloc_end_pre{
	struct{
	MeasRetu_reg ftime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_cycle_xloc_4{
	struct{
	MeasRetu_reg cycle_xloc_4:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_cycle_xloc_3{
	struct{
	MeasRetu_reg cycle_xloc_3:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_cycle_xloc_2{
	struct{
	MeasRetu_reg cycle_xloc_2:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_cycle_xloc_1{
	struct{
	MeasRetu_reg cycle_xloc_1:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ave_sum_n_bits31_0{
	struct{
	MeasRetu_reg ave_sum_n_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ave_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_1_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ave_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_2_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_ave_sum_x_bit39_32{
	struct{
	MeasRetu_reg ave_sum_n_bit39_32:8;
	MeasRetu_reg ave_sum_1_1_bit39_32:8;
	MeasRetu_reg ave_sum_1_2_bit39_32:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_vrms_sum_n_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_n_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_vrms_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_1_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_vrms_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_vrms_sum_n_1_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_n_bits47_32:16;
	MeasRetu_reg vrms_sum_1_1_bits47_32:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_vrms_sum_2_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits47_32:16;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_total_rf_num{
	struct{
	MeasRetu_reg total_rf_num:30;
	MeasRetu_reg slope:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_shoot{
	struct{
	MeasRetu_reg over_shoot:8;
	MeasRetu_reg pre_shoot:8;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_edge_cnt_start_xloc{
	struct{
	MeasRetu_reg edge_cnt_start_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_edge_cnt_end_xloc{
	struct{
	MeasRetu_reg edge_cnt_end_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_edge_cnt_start2_xloc{
	struct{
	MeasRetu_reg edge_cnt_start2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_cha_edge_cnt_end2_xloc{
	struct{
	MeasRetu_reg edge_cnt_end2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_max_min_top_base{
	struct{
	MeasRetu_reg cha_vmin:8;
	MeasRetu_reg cha_vmax:8;
	MeasRetu_reg cha_vbase:8;
	MeasRetu_reg cha_vtop:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_threshold_l_mid_h{
	struct{
	MeasRetu_reg cha_threshold_l:8;
	MeasRetu_reg cha_threshold_mid:8;
	MeasRetu_reg cha_threshold_h:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_vmax_xloc{
	struct{
	MeasRetu_reg vmax_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmax_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_vmin_xloc{
	struct{
	MeasRetu_reg vmin_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmin_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_rtime{
	struct{
	MeasRetu_reg rtime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_rtime_xloc_end{
	struct{
	MeasRetu_reg rtime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_rtime_pre{
	struct{
	MeasRetu_reg rtime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_rtime_xloc_end_pre{
	struct{
	MeasRetu_reg rtime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ftime{
	struct{
	MeasRetu_reg ftime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ftime_xloc_end{
	struct{
	MeasRetu_reg ftime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ftime_pre{
	struct{
	MeasRetu_reg ftime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ftime_xloc_end_pre{
	struct{
	MeasRetu_reg ftime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_cycle_xloc_4{
	struct{
	MeasRetu_reg cycle_xloc_4:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_cycle_xloc_3{
	struct{
	MeasRetu_reg cycle_xloc_3:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_cycle_xloc_2{
	struct{
	MeasRetu_reg cycle_xloc_2:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_cycle_xloc_1{
	struct{
	MeasRetu_reg cycle_xloc_1:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ave_sum_n_bits31_0{
	struct{
	MeasRetu_reg ave_sum_n_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ave_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_1_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ave_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_2_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_ave_sum_x_bit39_32{
	struct{
	MeasRetu_reg ave_sum_n_bit39_32:8;
	MeasRetu_reg ave_sum_1_1_bit39_32:8;
	MeasRetu_reg ave_sum_1_2_bit39_32:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_vrms_sum_n_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_n_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_vrms_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_1_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_vrms_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_vrms_sum_n_1_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_n_bits47_32:16;
	MeasRetu_reg vrms_sum_1_1_bits47_32:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_vrms_sum_2_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits47_32:16;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_total_rf_num{
	struct{
	MeasRetu_reg total_rf_num:30;
	MeasRetu_reg slope:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_shoot{
	struct{
	MeasRetu_reg over_shoot:8;
	MeasRetu_reg pre_shoot:8;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_edge_cnt_start_xloc{
	struct{
	MeasRetu_reg edge_cnt_start_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_edge_cnt_end_xloc{
	struct{
	MeasRetu_reg edge_cnt_end_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_edge_cnt_start2_xloc{
	struct{
	MeasRetu_reg edge_cnt_start2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chb_edge_cnt_end2_xloc{
	struct{
	MeasRetu_reg edge_cnt_end2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_max_min_top_base{
	struct{
	MeasRetu_reg cha_vmin:8;
	MeasRetu_reg cha_vmax:8;
	MeasRetu_reg cha_vbase:8;
	MeasRetu_reg cha_vtop:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_threshold_l_mid_h{
	struct{
	MeasRetu_reg cha_threshold_l:8;
	MeasRetu_reg cha_threshold_mid:8;
	MeasRetu_reg cha_threshold_h:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_vmax_xloc{
	struct{
	MeasRetu_reg vmax_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmax_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_vmin_xloc{
	struct{
	MeasRetu_reg vmin_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmin_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_rtime{
	struct{
	MeasRetu_reg rtime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_rtime_xloc_end{
	struct{
	MeasRetu_reg rtime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_rtime_pre{
	struct{
	MeasRetu_reg rtime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_rtime_xloc_end_pre{
	struct{
	MeasRetu_reg rtime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ftime{
	struct{
	MeasRetu_reg ftime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ftime_xloc_end{
	struct{
	MeasRetu_reg ftime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ftime_pre{
	struct{
	MeasRetu_reg ftime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ftime_xloc_end_pre{
	struct{
	MeasRetu_reg ftime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_cycle_xloc_4{
	struct{
	MeasRetu_reg cycle_xloc_4:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_cycle_xloc_3{
	struct{
	MeasRetu_reg cycle_xloc_3:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_cycle_xloc_2{
	struct{
	MeasRetu_reg cycle_xloc_2:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_cycle_xloc_1{
	struct{
	MeasRetu_reg cycle_xloc_1:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ave_sum_n_bits31_0{
	struct{
	MeasRetu_reg ave_sum_n_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ave_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_1_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ave_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_2_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_ave_sum_x_bit39_32{
	struct{
	MeasRetu_reg ave_sum_n_bit39_32:8;
	MeasRetu_reg ave_sum_1_1_bit39_32:8;
	MeasRetu_reg ave_sum_1_2_bit39_32:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_vrms_sum_n_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_n_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_vrms_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_1_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_vrms_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_vrms_sum_n_1_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_n_bits47_32:16;
	MeasRetu_reg vrms_sum_1_1_bits47_32:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_vrms_sum_2_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits47_32:16;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_total_rf_num{
	struct{
	MeasRetu_reg total_rf_num:30;
	MeasRetu_reg slope:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_shoot{
	struct{
	MeasRetu_reg over_shoot:8;
	MeasRetu_reg pre_shoot:8;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_edge_cnt_start_xloc{
	struct{
	MeasRetu_reg edge_cnt_start_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_edge_cnt_end_xloc{
	struct{
	MeasRetu_reg edge_cnt_end_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_edge_cnt_start2_xloc{
	struct{
	MeasRetu_reg edge_cnt_start2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chc_edge_cnt_end2_xloc{
	struct{
	MeasRetu_reg edge_cnt_end2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_max_min_top_base{
	struct{
	MeasRetu_reg cha_vmin:8;
	MeasRetu_reg cha_vmax:8;
	MeasRetu_reg cha_vbase:8;
	MeasRetu_reg cha_vtop:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_threshold_l_mid_h{
	struct{
	MeasRetu_reg cha_threshold_l:8;
	MeasRetu_reg cha_threshold_mid:8;
	MeasRetu_reg cha_threshold_h:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_vmax_xloc{
	struct{
	MeasRetu_reg vmax_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmax_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_vmin_xloc{
	struct{
	MeasRetu_reg vmin_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg vmin_xloc_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_rtime{
	struct{
	MeasRetu_reg rtime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_rtime_xloc_end{
	struct{
	MeasRetu_reg rtime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_rtime_pre{
	struct{
	MeasRetu_reg rtime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_rtime_xloc_end_pre{
	struct{
	MeasRetu_reg rtime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg rtime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ftime{
	struct{
	MeasRetu_reg ftime:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ftime_xloc_end{
	struct{
	MeasRetu_reg ftime_xloc_end:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ftime_pre{
	struct{
	MeasRetu_reg ftime_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ftime_xloc_end_pre{
	struct{
	MeasRetu_reg ftime_xloc_end_pre:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg ftime_xloc_end_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_cycle_xloc_4{
	struct{
	MeasRetu_reg cycle_xloc_4:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_cycle_xloc_3{
	struct{
	MeasRetu_reg cycle_xloc_3:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_cycle_xloc_2{
	struct{
	MeasRetu_reg cycle_xloc_2:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_cycle_xloc_1{
	struct{
	MeasRetu_reg cycle_xloc_1:30;
	MeasRetu_reg edge_type:1;
	MeasRetu_reg cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ave_sum_n_bits31_0{
	struct{
	MeasRetu_reg ave_sum_n_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ave_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_1_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ave_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg ave_sum_1_2_bit31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_ave_sum_x_bit39_32{
	struct{
	MeasRetu_reg ave_sum_n_bit39_32:8;
	MeasRetu_reg ave_sum_1_1_bit39_32:8;
	MeasRetu_reg ave_sum_1_2_bit39_32:8;
	MeasRetu_reg reserved:8;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_vrms_sum_n_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_n_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_vrms_sum_1_1_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_1_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_vrms_sum_1_2_bits31_0{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits31_0:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_vrms_sum_n_1_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_n_bits47_32:16;
	MeasRetu_reg vrms_sum_1_1_bits47_32:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_vrms_sum_2_bits47_32{
	struct{
	MeasRetu_reg vrms_sum_1_2_bits47_32:16;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_total_rf_num{
	struct{
	MeasRetu_reg total_rf_num:30;
	MeasRetu_reg slope:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_shoot{
	struct{
	MeasRetu_reg over_shoot:8;
	MeasRetu_reg pre_shoot:8;
	MeasRetu_reg reserved:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_edge_cnt_start_xloc{
	struct{
	MeasRetu_reg edge_cnt_start_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_edge_cnt_end_xloc{
	struct{
	MeasRetu_reg edge_cnt_end_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_edge_cnt_start2_xloc{
	struct{
	MeasRetu_reg edge_cnt_start2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_chd_edge_cnt_end2_xloc{
	struct{
	MeasRetu_reg edge_cnt_end2_xloc:30;
	MeasRetu_reg reserved:1;
	MeasRetu_reg valid:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_meas_version_rd{
	struct{
	MeasRetu_reg meas_version:32;
	};
	MeasRetu_reg payload;
};

union MeasRetu_meas_result_done{
	struct{
	MeasRetu_reg meas_result_done:1;
	MeasRetu_reg reserved:15;
	MeasRetu_reg rw_test_reg:16;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la0_cycle_xloc_4{
	struct{
	MeasRetu_reg la0_cycle_xloc_4:30;
	MeasRetu_reg la0_edge_type:1;
	MeasRetu_reg la0_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la0_cycle_xloc_3{
	struct{
	MeasRetu_reg la0_cycle_xloc_3:30;
	MeasRetu_reg la0_edge_type:1;
	MeasRetu_reg la0_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la0_cycle_xloc_2{
	struct{
	MeasRetu_reg la0_cycle_xloc_2:30;
	MeasRetu_reg la0_edge_type:1;
	MeasRetu_reg la0_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la0_cycle_xloc_1{
	struct{
	MeasRetu_reg la0_cycle_xloc_1:30;
	MeasRetu_reg la0_edge_type:1;
	MeasRetu_reg la0_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la1_cycle_xloc_4{
	struct{
	MeasRetu_reg la1_cycle_xloc_4:30;
	MeasRetu_reg la1_edge_type:1;
	MeasRetu_reg la1_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la1_cycle_xloc_3{
	struct{
	MeasRetu_reg la1_cycle_xloc_3:30;
	MeasRetu_reg la1_edge_type:1;
	MeasRetu_reg la1_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la1_cycle_xloc_2{
	struct{
	MeasRetu_reg la1_cycle_xloc_2:30;
	MeasRetu_reg la1_edge_type:1;
	MeasRetu_reg la1_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la1_cycle_xloc_1{
	struct{
	MeasRetu_reg la1_cycle_xloc_1:30;
	MeasRetu_reg la1_edge_type:1;
	MeasRetu_reg la1_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la2_cycle_xloc_4{
	struct{
	MeasRetu_reg la2_cycle_xloc_4:30;
	MeasRetu_reg la2_edge_type:1;
	MeasRetu_reg la2_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la2_cycle_xloc_3{
	struct{
	MeasRetu_reg la2_cycle_xloc_3:30;
	MeasRetu_reg la2_edge_type:1;
	MeasRetu_reg la2_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la2_cycle_xloc_2{
	struct{
	MeasRetu_reg la2_cycle_xloc_2:30;
	MeasRetu_reg la2_edge_type:1;
	MeasRetu_reg la2_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la2_cycle_xloc_1{
	struct{
	MeasRetu_reg la2_cycle_xloc_1:30;
	MeasRetu_reg la2_edge_type:1;
	MeasRetu_reg la2_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la3_cycle_xloc_4{
	struct{
	MeasRetu_reg la3_cycle_xloc_4:30;
	MeasRetu_reg la3_edge_type:1;
	MeasRetu_reg la3_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la3_cycle_xloc_3{
	struct{
	MeasRetu_reg la3_cycle_xloc_3:30;
	MeasRetu_reg la3_edge_type:1;
	MeasRetu_reg la3_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la3_cycle_xloc_2{
	struct{
	MeasRetu_reg la3_cycle_xloc_2:30;
	MeasRetu_reg la3_edge_type:1;
	MeasRetu_reg la3_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la3_cycle_xloc_1{
	struct{
	MeasRetu_reg la3_cycle_xloc_1:30;
	MeasRetu_reg la3_edge_type:1;
	MeasRetu_reg la3_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la4_cycle_xloc_4{
	struct{
	MeasRetu_reg la4_cycle_xloc_4:30;
	MeasRetu_reg la4_edge_type:1;
	MeasRetu_reg la4_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la4_cycle_xloc_3{
	struct{
	MeasRetu_reg la4_cycle_xloc_3:30;
	MeasRetu_reg la4_edge_type:1;
	MeasRetu_reg la4_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la4_cycle_xloc_2{
	struct{
	MeasRetu_reg la4_cycle_xloc_2:30;
	MeasRetu_reg la4_edge_type:1;
	MeasRetu_reg la4_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la4_cycle_xloc_1{
	struct{
	MeasRetu_reg la4_cycle_xloc_1:30;
	MeasRetu_reg la4_edge_type:1;
	MeasRetu_reg la4_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la5_cycle_xloc_4{
	struct{
	MeasRetu_reg la5_cycle_xloc_4:30;
	MeasRetu_reg la5_edge_type:1;
	MeasRetu_reg la5_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la5_cycle_xloc_3{
	struct{
	MeasRetu_reg la5_cycle_xloc_3:30;
	MeasRetu_reg la5_edge_type:1;
	MeasRetu_reg la5_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la5_cycle_xloc_2{
	struct{
	MeasRetu_reg la5_cycle_xloc_2:30;
	MeasRetu_reg la5_edge_type:1;
	MeasRetu_reg la5_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la5_cycle_xloc_1{
	struct{
	MeasRetu_reg la5_cycle_xloc_1:30;
	MeasRetu_reg la5_edge_type:1;
	MeasRetu_reg la5_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la6_cycle_xloc_4{
	struct{
	MeasRetu_reg la6_cycle_xloc_4:30;
	MeasRetu_reg la6_edge_type:1;
	MeasRetu_reg la6_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la6_cycle_xloc_3{
	struct{
	MeasRetu_reg la6_cycle_xloc_3:30;
	MeasRetu_reg la6_edge_type:1;
	MeasRetu_reg la6_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la6_cycle_xloc_2{
	struct{
	MeasRetu_reg la6_cycle_xloc_2:30;
	MeasRetu_reg la6_edge_type:1;
	MeasRetu_reg la6_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la6_cycle_xloc_1{
	struct{
	MeasRetu_reg la6_cycle_xloc_1:30;
	MeasRetu_reg la6_edge_type:1;
	MeasRetu_reg la6_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la7_cycle_xloc_4{
	struct{
	MeasRetu_reg la7_cycle_xloc_4:30;
	MeasRetu_reg la7_edge_type:1;
	MeasRetu_reg la7_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la7_cycle_xloc_3{
	struct{
	MeasRetu_reg la7_cycle_xloc_3:30;
	MeasRetu_reg la7_edge_type:1;
	MeasRetu_reg la7_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la7_cycle_xloc_2{
	struct{
	MeasRetu_reg la7_cycle_xloc_2:30;
	MeasRetu_reg la7_edge_type:1;
	MeasRetu_reg la7_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la7_cycle_xloc_1{
	struct{
	MeasRetu_reg la7_cycle_xloc_1:30;
	MeasRetu_reg la7_edge_type:1;
	MeasRetu_reg la7_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la8_cycle_xloc_4{
	struct{
	MeasRetu_reg la8_cycle_xloc_4:30;
	MeasRetu_reg la8_edge_type:1;
	MeasRetu_reg la8_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la8_cycle_xloc_3{
	struct{
	MeasRetu_reg la8_cycle_xloc_3:30;
	MeasRetu_reg la8_edge_type:1;
	MeasRetu_reg la8_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la8_cycle_xloc_2{
	struct{
	MeasRetu_reg la8_cycle_xloc_2:30;
	MeasRetu_reg la8_edge_type:1;
	MeasRetu_reg la8_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la8_cycle_xloc_1{
	struct{
	MeasRetu_reg la8_cycle_xloc_1:30;
	MeasRetu_reg la8_edge_type:1;
	MeasRetu_reg la8_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la9_cycle_xloc_4{
	struct{
	MeasRetu_reg la9_cycle_xloc_4:30;
	MeasRetu_reg la9_edge_type:1;
	MeasRetu_reg la9_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la9_cycle_xloc_3{
	struct{
	MeasRetu_reg la9_cycle_xloc_3:30;
	MeasRetu_reg la9_edge_type:1;
	MeasRetu_reg la9_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la9_cycle_xloc_2{
	struct{
	MeasRetu_reg la9_cycle_xloc_2:30;
	MeasRetu_reg la9_edge_type:1;
	MeasRetu_reg la9_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la9_cycle_xloc_1{
	struct{
	MeasRetu_reg la9_cycle_xloc_1:30;
	MeasRetu_reg la9_edge_type:1;
	MeasRetu_reg la9_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la10_cycle_xloc_4{
	struct{
	MeasRetu_reg la10_cycle_xloc_4:30;
	MeasRetu_reg la10_edge_type:1;
	MeasRetu_reg la10_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la10_cycle_xloc_3{
	struct{
	MeasRetu_reg la10_cycle_xloc_3:30;
	MeasRetu_reg la10_edge_type:1;
	MeasRetu_reg la10_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la10_cycle_xloc_2{
	struct{
	MeasRetu_reg la10_cycle_xloc_2:30;
	MeasRetu_reg la10_edge_type:1;
	MeasRetu_reg la10_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la10_cycle_xloc_1{
	struct{
	MeasRetu_reg la10_cycle_xloc_1:30;
	MeasRetu_reg la10_edge_type:1;
	MeasRetu_reg la10_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la11_cycle_xloc_4{
	struct{
	MeasRetu_reg la11_cycle_xloc_4:30;
	MeasRetu_reg la11_edge_type:1;
	MeasRetu_reg la11_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la11_cycle_xloc_3{
	struct{
	MeasRetu_reg la11_cycle_xloc_3:30;
	MeasRetu_reg la11_edge_type:1;
	MeasRetu_reg la11_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la11_cycle_xloc_2{
	struct{
	MeasRetu_reg la11_cycle_xloc_2:30;
	MeasRetu_reg la11_edge_type:1;
	MeasRetu_reg la11_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la11_cycle_xloc_1{
	struct{
	MeasRetu_reg la11_cycle_xloc_1:30;
	MeasRetu_reg la11_edge_type:1;
	MeasRetu_reg la11_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la12_cycle_xloc_4{
	struct{
	MeasRetu_reg la12_cycle_xloc_4:30;
	MeasRetu_reg la12_edge_type:1;
	MeasRetu_reg la12_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la12_cycle_xloc_3{
	struct{
	MeasRetu_reg la12_cycle_xloc_3:30;
	MeasRetu_reg la12_edge_type:1;
	MeasRetu_reg la12_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la12_cycle_xloc_2{
	struct{
	MeasRetu_reg la12_cycle_xloc_2:30;
	MeasRetu_reg la12_edge_type:1;
	MeasRetu_reg la12_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la12_cycle_xloc_1{
	struct{
	MeasRetu_reg la12_cycle_xloc_1:30;
	MeasRetu_reg la12_edge_type:1;
	MeasRetu_reg la12_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la13_cycle_xloc_4{
	struct{
	MeasRetu_reg la13_cycle_xloc_4:30;
	MeasRetu_reg la13_edge_type:1;
	MeasRetu_reg la13_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la13_cycle_xloc_3{
	struct{
	MeasRetu_reg la13_cycle_xloc_3:30;
	MeasRetu_reg la13_edge_type:1;
	MeasRetu_reg la13_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la13_cycle_xloc_2{
	struct{
	MeasRetu_reg la13_cycle_xloc_2:30;
	MeasRetu_reg la13_edge_type:1;
	MeasRetu_reg la13_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la13_cycle_xloc_1{
	struct{
	MeasRetu_reg la13_cycle_xloc_1:30;
	MeasRetu_reg la13_edge_type:1;
	MeasRetu_reg la13_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la14_cycle_xloc_4{
	struct{
	MeasRetu_reg la14_cycle_xloc_4:30;
	MeasRetu_reg la14_edge_type:1;
	MeasRetu_reg la14_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la14_cycle_xloc_3{
	struct{
	MeasRetu_reg la14_cycle_xloc_3:30;
	MeasRetu_reg la14_edge_type:1;
	MeasRetu_reg la14_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la14_cycle_xloc_2{
	struct{
	MeasRetu_reg la14_cycle_xloc_2:30;
	MeasRetu_reg la14_edge_type:1;
	MeasRetu_reg la14_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la14_cycle_xloc_1{
	struct{
	MeasRetu_reg la14_cycle_xloc_1:30;
	MeasRetu_reg la14_edge_type:1;
	MeasRetu_reg la14_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la15_cycle_xloc_4{
	struct{
	MeasRetu_reg la15_cycle_xloc_4:30;
	MeasRetu_reg la15_edge_type:1;
	MeasRetu_reg la15_cycle_xloc_4_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la15_cycle_xloc_3{
	struct{
	MeasRetu_reg la15_cycle_xloc_3:30;
	MeasRetu_reg la15_edge_type:1;
	MeasRetu_reg la15_cycle_xloc_3_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la15_cycle_xloc_2{
	struct{
	MeasRetu_reg la15_cycle_xloc_2:30;
	MeasRetu_reg la15_edge_type:1;
	MeasRetu_reg la15_cycle_xloc_2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_la15_cycle_xloc_1{
	struct{
	MeasRetu_reg la15_cycle_xloc_1:30;
	MeasRetu_reg la15_edge_type:1;
	MeasRetu_reg la15_cycle_xloc_1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult1_from_xloc2{
	struct{
	MeasRetu_reg dlyresult1_from_xloc2:31;
	MeasRetu_reg dlyresult1_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult1_from_xloc1{
	struct{
	MeasRetu_reg dlyresult1_from_xloc1:31;
	MeasRetu_reg dlyresult1_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult1_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult1_to_xloc2_pre:31;
	MeasRetu_reg dlyresult1_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult1_to_xloc2{
	struct{
	MeasRetu_reg dlyresult1_to_xloc2:31;
	MeasRetu_reg dlyresult1_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult1_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult1_to_xloc1_pre:31;
	MeasRetu_reg dlyresult1_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult1_to_xloc1{
	struct{
	MeasRetu_reg dlyresult1_to_xloc1:31;
	MeasRetu_reg dlyresult1_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult2_from_xloc2{
	struct{
	MeasRetu_reg dlyresult2_from_xloc2:31;
	MeasRetu_reg dlyresult2_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult2_from_xloc1{
	struct{
	MeasRetu_reg dlyresult2_from_xloc1:31;
	MeasRetu_reg dlyresult2_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult2_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult2_to_xloc2_pre:31;
	MeasRetu_reg dlyresult2_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult2_to_xloc2{
	struct{
	MeasRetu_reg dlyresult2_to_xloc2:31;
	MeasRetu_reg dlyresult2_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult2_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult2_to_xloc1_pre:31;
	MeasRetu_reg dlyresult2_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult2_to_xloc1{
	struct{
	MeasRetu_reg dlyresult2_to_xloc1:31;
	MeasRetu_reg dlyresult2_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult3_from_xloc2{
	struct{
	MeasRetu_reg dlyresult3_from_xloc2:31;
	MeasRetu_reg dlyresult3_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult3_from_xloc1{
	struct{
	MeasRetu_reg dlyresult3_from_xloc1:31;
	MeasRetu_reg dlyresult3_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult3_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult3_to_xloc2_pre:31;
	MeasRetu_reg dlyresult3_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult3_to_xloc2{
	struct{
	MeasRetu_reg dlyresult3_to_xloc2:31;
	MeasRetu_reg dlyresult3_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult3_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult3_to_xloc1_pre:31;
	MeasRetu_reg dlyresult3_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult3_to_xloc1{
	struct{
	MeasRetu_reg dlyresult3_to_xloc1:31;
	MeasRetu_reg dlyresult3_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult4_from_xloc2{
	struct{
	MeasRetu_reg dlyresult4_from_xloc2:31;
	MeasRetu_reg dlyresult4_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult4_from_xloc1{
	struct{
	MeasRetu_reg dlyresult4_from_xloc1:31;
	MeasRetu_reg dlyresult4_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult4_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult4_to_xloc2_pre:31;
	MeasRetu_reg dlyresult4_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult4_to_xloc2{
	struct{
	MeasRetu_reg dlyresult4_to_xloc2:31;
	MeasRetu_reg dlyresult4_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult4_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult4_to_xloc1_pre:31;
	MeasRetu_reg dlyresult4_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult4_to_xloc1{
	struct{
	MeasRetu_reg dlyresult4_to_xloc1:31;
	MeasRetu_reg dlyresult4_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult5_from_xloc2{
	struct{
	MeasRetu_reg dlyresult5_from_xloc2:31;
	MeasRetu_reg dlyresult5_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult5_from_xloc1{
	struct{
	MeasRetu_reg dlyresult5_from_xloc1:31;
	MeasRetu_reg dlyresult5_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult5_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult5_to_xloc2_pre:31;
	MeasRetu_reg dlyresult5_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult5_to_xloc2{
	struct{
	MeasRetu_reg dlyresult5_to_xloc2:31;
	MeasRetu_reg dlyresult5_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult5_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult5_to_xloc1_pre:31;
	MeasRetu_reg dlyresult5_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult5_to_xloc1{
	struct{
	MeasRetu_reg dlyresult5_to_xloc1:31;
	MeasRetu_reg dlyresult5_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult6_from_xloc2{
	struct{
	MeasRetu_reg dlyresult6_from_xloc2:31;
	MeasRetu_reg dlyresult6_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult6_from_xloc1{
	struct{
	MeasRetu_reg dlyresult6_from_xloc1:31;
	MeasRetu_reg dlyresult6_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult6_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult6_to_xloc2_pre:31;
	MeasRetu_reg dlyresult6_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult6_to_xloc2{
	struct{
	MeasRetu_reg dlyresult6_to_xloc2:31;
	MeasRetu_reg dlyresult6_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult6_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult6_to_xloc1_pre:31;
	MeasRetu_reg dlyresult6_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult6_to_xloc1{
	struct{
	MeasRetu_reg dlyresult6_to_xloc1:31;
	MeasRetu_reg dlyresult6_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult7_from_xloc2{
	struct{
	MeasRetu_reg dlyresult7_from_xloc2:31;
	MeasRetu_reg dlyresult7_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult7_from_xloc1{
	struct{
	MeasRetu_reg dlyresult7_from_xloc1:31;
	MeasRetu_reg dlyresult7_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult7_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult7_to_xloc2_pre:31;
	MeasRetu_reg dlyresult7_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult7_to_xloc2{
	struct{
	MeasRetu_reg dlyresult7_to_xloc2:31;
	MeasRetu_reg dlyresult7_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult7_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult7_to_xloc1_pre:31;
	MeasRetu_reg dlyresult7_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult7_to_xloc1{
	struct{
	MeasRetu_reg dlyresult7_to_xloc1:31;
	MeasRetu_reg dlyresult7_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult8_from_xloc2{
	struct{
	MeasRetu_reg dlyresult8_from_xloc2:31;
	MeasRetu_reg dlyresult8_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult8_from_xloc1{
	struct{
	MeasRetu_reg dlyresult8_from_xloc1:31;
	MeasRetu_reg dlyresult8_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult8_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult8_to_xloc2_pre:31;
	MeasRetu_reg dlyresult8_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult8_to_xloc2{
	struct{
	MeasRetu_reg dlyresult8_to_xloc2:31;
	MeasRetu_reg dlyresult8_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult8_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult8_to_xloc1_pre:31;
	MeasRetu_reg dlyresult8_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult8_to_xloc1{
	struct{
	MeasRetu_reg dlyresult8_to_xloc1:31;
	MeasRetu_reg dlyresult8_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult9_from_xloc2{
	struct{
	MeasRetu_reg dlyresult9_from_xloc2:31;
	MeasRetu_reg dlyresult9_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult9_from_xloc1{
	struct{
	MeasRetu_reg dlyresult9_from_xloc1:31;
	MeasRetu_reg dlyresult9_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult9_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult9_to_xloc2_pre:31;
	MeasRetu_reg dlyresult9_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult9_to_xloc2{
	struct{
	MeasRetu_reg dlyresult9_to_xloc2:31;
	MeasRetu_reg dlyresult9_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult9_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult9_to_xloc1_pre:31;
	MeasRetu_reg dlyresult9_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult9_to_xloc1{
	struct{
	MeasRetu_reg dlyresult9_to_xloc1:31;
	MeasRetu_reg dlyresult9_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult10_from_xloc2{
	struct{
	MeasRetu_reg dlyresult10_from_xloc2:31;
	MeasRetu_reg dlyresult10_from_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult10_from_xloc1{
	struct{
	MeasRetu_reg dlyresult10_from_xloc1:31;
	MeasRetu_reg dlyresult10_from_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult10_to_xloc2_pre{
	struct{
	MeasRetu_reg dlyresult10_to_xloc2_pre:31;
	MeasRetu_reg dlyresult10_to_xloc2_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult10_to_xloc2{
	struct{
	MeasRetu_reg dlyresult10_to_xloc2:31;
	MeasRetu_reg dlyresult10_to_xloc2_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult10_to_xloc1_pre{
	struct{
	MeasRetu_reg dlyresult10_to_xloc1_pre:31;
	MeasRetu_reg dlyresult10_to_xloc1_pre_vld:1;
	};
	MeasRetu_reg payload;
};

union MeasRetu_dlyresult10_to_xloc1{
	struct{
	MeasRetu_reg dlyresult10_to_xloc1:31;
	MeasRetu_reg dlyresult10_to_xloc1_vld:1;
	};
	MeasRetu_reg payload;
};

struct MeasRetuMemProxy {
	MeasRetu_cha_max_min_top_base cha_max_min_top_base; 
	MeasRetu_cha_threshold_l_mid_h cha_threshold_l_mid_h; 
	MeasRetu_cha_vmax_xloc cha_vmax_xloc; 
	MeasRetu_cha_vmin_xloc cha_vmin_xloc; 

	MeasRetu_cha_rtime cha_rtime; 
	MeasRetu_cha_rtime_xloc_end cha_rtime_xloc_end; 
	MeasRetu_cha_rtime_pre cha_rtime_pre; 
	MeasRetu_cha_rtime_xloc_end_pre cha_rtime_xloc_end_pre; 

	MeasRetu_cha_ftime cha_ftime; 
	MeasRetu_cha_ftime_xloc_end cha_ftime_xloc_end; 
	MeasRetu_cha_ftime_pre cha_ftime_pre; 
	MeasRetu_cha_ftime_xloc_end_pre cha_ftime_xloc_end_pre; 

	MeasRetu_cha_cycle_xloc_4 cha_cycle_xloc_4; 
	MeasRetu_cha_cycle_xloc_3 cha_cycle_xloc_3; 
	MeasRetu_cha_cycle_xloc_2 cha_cycle_xloc_2; 
	MeasRetu_cha_cycle_xloc_1 cha_cycle_xloc_1; 

	MeasRetu_cha_ave_sum_n_bits31_0 cha_ave_sum_n_bits31_0; 
	MeasRetu_cha_ave_sum_1_1_bits31_0 cha_ave_sum_1_1_bits31_0; 
	MeasRetu_cha_ave_sum_1_2_bits31_0 cha_ave_sum_1_2_bits31_0; 
	MeasRetu_cha_ave_sum_x_bit39_32 cha_ave_sum_x_bit39_32; 

	MeasRetu_cha_vrms_sum_n_bits31_0 cha_vrms_sum_n_bits31_0; 
	MeasRetu_cha_vrms_sum_1_1_bits31_0 cha_vrms_sum_1_1_bits31_0; 
	MeasRetu_cha_vrms_sum_1_2_bits31_0 cha_vrms_sum_1_2_bits31_0; 
	MeasRetu_cha_vrms_sum_n_1_bits47_32 cha_vrms_sum_n_1_bits47_32; 

	MeasRetu_cha_vrms_sum_2_bits47_32 cha_vrms_sum_2_bits47_32; 
	MeasRetu_cha_total_rf_num cha_total_rf_num; 
	MeasRetu_reg reserved_x1;
	MeasRetu_cha_shoot cha_shoot; 

	MeasRetu_cha_edge_cnt_start_xloc cha_edge_cnt_start_xloc; 
	MeasRetu_cha_edge_cnt_end_xloc cha_edge_cnt_end_xloc; 
	MeasRetu_cha_edge_cnt_start2_xloc cha_edge_cnt_start2_xloc; 
	MeasRetu_cha_edge_cnt_end2_xloc cha_edge_cnt_end2_xloc; 

	MeasRetu_chb_max_min_top_base chb_max_min_top_base; 
	MeasRetu_chb_threshold_l_mid_h chb_threshold_l_mid_h; 
	MeasRetu_chb_vmax_xloc chb_vmax_xloc; 
	MeasRetu_chb_vmin_xloc chb_vmin_xloc; 

	MeasRetu_chb_rtime chb_rtime; 
	MeasRetu_chb_rtime_xloc_end chb_rtime_xloc_end; 
	MeasRetu_chb_rtime_pre chb_rtime_pre; 
	MeasRetu_chb_rtime_xloc_end_pre chb_rtime_xloc_end_pre; 

	MeasRetu_chb_ftime chb_ftime; 
	MeasRetu_chb_ftime_xloc_end chb_ftime_xloc_end; 
	MeasRetu_chb_ftime_pre chb_ftime_pre; 
	MeasRetu_chb_ftime_xloc_end_pre chb_ftime_xloc_end_pre; 

	MeasRetu_chb_cycle_xloc_4 chb_cycle_xloc_4; 
	MeasRetu_chb_cycle_xloc_3 chb_cycle_xloc_3; 
	MeasRetu_chb_cycle_xloc_2 chb_cycle_xloc_2; 
	MeasRetu_chb_cycle_xloc_1 chb_cycle_xloc_1; 

	MeasRetu_chb_ave_sum_n_bits31_0 chb_ave_sum_n_bits31_0; 
	MeasRetu_chb_ave_sum_1_1_bits31_0 chb_ave_sum_1_1_bits31_0; 
	MeasRetu_chb_ave_sum_1_2_bits31_0 chb_ave_sum_1_2_bits31_0; 
	MeasRetu_chb_ave_sum_x_bit39_32 chb_ave_sum_x_bit39_32; 

	MeasRetu_chb_vrms_sum_n_bits31_0 chb_vrms_sum_n_bits31_0; 
	MeasRetu_chb_vrms_sum_1_1_bits31_0 chb_vrms_sum_1_1_bits31_0; 
	MeasRetu_chb_vrms_sum_1_2_bits31_0 chb_vrms_sum_1_2_bits31_0; 
	MeasRetu_chb_vrms_sum_n_1_bits47_32 chb_vrms_sum_n_1_bits47_32; 

	MeasRetu_chb_vrms_sum_2_bits47_32 chb_vrms_sum_2_bits47_32; 
	MeasRetu_chb_total_rf_num chb_total_rf_num; 
	MeasRetu_reg reserved_x2;
	MeasRetu_chb_shoot chb_shoot; 

	MeasRetu_chb_edge_cnt_start_xloc chb_edge_cnt_start_xloc; 
	MeasRetu_chb_edge_cnt_end_xloc chb_edge_cnt_end_xloc; 
	MeasRetu_chb_edge_cnt_start2_xloc chb_edge_cnt_start2_xloc; 
	MeasRetu_chb_edge_cnt_end2_xloc chb_edge_cnt_end2_xloc; 

	MeasRetu_chc_max_min_top_base chc_max_min_top_base; 
	MeasRetu_chc_threshold_l_mid_h chc_threshold_l_mid_h; 
	MeasRetu_chc_vmax_xloc chc_vmax_xloc; 
	MeasRetu_chc_vmin_xloc chc_vmin_xloc; 

	MeasRetu_chc_rtime chc_rtime; 
	MeasRetu_chc_rtime_xloc_end chc_rtime_xloc_end; 
	MeasRetu_chc_rtime_pre chc_rtime_pre; 
	MeasRetu_chc_rtime_xloc_end_pre chc_rtime_xloc_end_pre; 

	MeasRetu_chc_ftime chc_ftime; 
	MeasRetu_chc_ftime_xloc_end chc_ftime_xloc_end; 
	MeasRetu_chc_ftime_pre chc_ftime_pre; 
	MeasRetu_chc_ftime_xloc_end_pre chc_ftime_xloc_end_pre; 

	MeasRetu_chc_cycle_xloc_4 chc_cycle_xloc_4; 
	MeasRetu_chc_cycle_xloc_3 chc_cycle_xloc_3; 
	MeasRetu_chc_cycle_xloc_2 chc_cycle_xloc_2; 
	MeasRetu_chc_cycle_xloc_1 chc_cycle_xloc_1; 

	MeasRetu_chc_ave_sum_n_bits31_0 chc_ave_sum_n_bits31_0; 
	MeasRetu_chc_ave_sum_1_1_bits31_0 chc_ave_sum_1_1_bits31_0; 
	MeasRetu_chc_ave_sum_1_2_bits31_0 chc_ave_sum_1_2_bits31_0; 
	MeasRetu_chc_ave_sum_x_bit39_32 chc_ave_sum_x_bit39_32; 

	MeasRetu_chc_vrms_sum_n_bits31_0 chc_vrms_sum_n_bits31_0; 
	MeasRetu_chc_vrms_sum_1_1_bits31_0 chc_vrms_sum_1_1_bits31_0; 
	MeasRetu_chc_vrms_sum_1_2_bits31_0 chc_vrms_sum_1_2_bits31_0; 
	MeasRetu_chc_vrms_sum_n_1_bits47_32 chc_vrms_sum_n_1_bits47_32; 

	MeasRetu_chc_vrms_sum_2_bits47_32 chc_vrms_sum_2_bits47_32; 
	MeasRetu_chc_total_rf_num chc_total_rf_num; 
	MeasRetu_reg reserved_x3;
	MeasRetu_chc_shoot chc_shoot; 

	MeasRetu_chc_edge_cnt_start_xloc chc_edge_cnt_start_xloc; 
	MeasRetu_chc_edge_cnt_end_xloc chc_edge_cnt_end_xloc; 
	MeasRetu_chc_edge_cnt_start2_xloc chc_edge_cnt_start2_xloc; 
	MeasRetu_chc_edge_cnt_end2_xloc chc_edge_cnt_end2_xloc; 

	MeasRetu_chd_max_min_top_base chd_max_min_top_base; 
	MeasRetu_chd_threshold_l_mid_h chd_threshold_l_mid_h; 
	MeasRetu_chd_vmax_xloc chd_vmax_xloc; 
	MeasRetu_chd_vmin_xloc chd_vmin_xloc; 

	MeasRetu_chd_rtime chd_rtime; 
	MeasRetu_chd_rtime_xloc_end chd_rtime_xloc_end; 
	MeasRetu_chd_rtime_pre chd_rtime_pre; 
	MeasRetu_chd_rtime_xloc_end_pre chd_rtime_xloc_end_pre; 

	MeasRetu_chd_ftime chd_ftime; 
	MeasRetu_chd_ftime_xloc_end chd_ftime_xloc_end; 
	MeasRetu_chd_ftime_pre chd_ftime_pre; 
	MeasRetu_chd_ftime_xloc_end_pre chd_ftime_xloc_end_pre; 

	MeasRetu_chd_cycle_xloc_4 chd_cycle_xloc_4; 
	MeasRetu_chd_cycle_xloc_3 chd_cycle_xloc_3; 
	MeasRetu_chd_cycle_xloc_2 chd_cycle_xloc_2; 
	MeasRetu_chd_cycle_xloc_1 chd_cycle_xloc_1; 

	MeasRetu_chd_ave_sum_n_bits31_0 chd_ave_sum_n_bits31_0; 
	MeasRetu_chd_ave_sum_1_1_bits31_0 chd_ave_sum_1_1_bits31_0; 
	MeasRetu_chd_ave_sum_1_2_bits31_0 chd_ave_sum_1_2_bits31_0; 
	MeasRetu_chd_ave_sum_x_bit39_32 chd_ave_sum_x_bit39_32; 

	MeasRetu_chd_vrms_sum_n_bits31_0 chd_vrms_sum_n_bits31_0; 
	MeasRetu_chd_vrms_sum_1_1_bits31_0 chd_vrms_sum_1_1_bits31_0; 
	MeasRetu_chd_vrms_sum_1_2_bits31_0 chd_vrms_sum_1_2_bits31_0; 
	MeasRetu_chd_vrms_sum_n_1_bits47_32 chd_vrms_sum_n_1_bits47_32; 

	MeasRetu_chd_vrms_sum_2_bits47_32 chd_vrms_sum_2_bits47_32; 
	MeasRetu_chd_total_rf_num chd_total_rf_num; 
	MeasRetu_reg reservd_x4;
	MeasRetu_chd_shoot chd_shoot; 

	MeasRetu_chd_edge_cnt_start_xloc chd_edge_cnt_start_xloc; 
	MeasRetu_chd_edge_cnt_end_xloc chd_edge_cnt_end_xloc; 
	MeasRetu_chd_edge_cnt_start2_xloc chd_edge_cnt_start2_xloc; 
	MeasRetu_chd_edge_cnt_end2_xloc chd_edge_cnt_end2_xloc; 

	MeasRetu_meas_version_rd meas_version_rd; 
	MeasRetu_meas_result_done meas_result_done; 
	MeasRetu_la0_cycle_xloc_4 la0_cycle_xloc_4; 
	MeasRetu_la0_cycle_xloc_3 la0_cycle_xloc_3; 

	MeasRetu_la0_cycle_xloc_2 la0_cycle_xloc_2; 
	MeasRetu_la0_cycle_xloc_1 la0_cycle_xloc_1; 
	MeasRetu_la1_cycle_xloc_4 la1_cycle_xloc_4; 
	MeasRetu_la1_cycle_xloc_3 la1_cycle_xloc_3; 

	MeasRetu_la1_cycle_xloc_2 la1_cycle_xloc_2; 
	MeasRetu_la1_cycle_xloc_1 la1_cycle_xloc_1; 
	MeasRetu_la2_cycle_xloc_4 la2_cycle_xloc_4; 
	MeasRetu_la2_cycle_xloc_3 la2_cycle_xloc_3; 

	MeasRetu_la2_cycle_xloc_2 la2_cycle_xloc_2; 
	MeasRetu_la2_cycle_xloc_1 la2_cycle_xloc_1; 
	MeasRetu_la3_cycle_xloc_4 la3_cycle_xloc_4; 
	MeasRetu_la3_cycle_xloc_3 la3_cycle_xloc_3; 

	MeasRetu_la3_cycle_xloc_2 la3_cycle_xloc_2; 
	MeasRetu_la3_cycle_xloc_1 la3_cycle_xloc_1; 
	MeasRetu_la4_cycle_xloc_4 la4_cycle_xloc_4; 
	MeasRetu_la4_cycle_xloc_3 la4_cycle_xloc_3; 

	MeasRetu_la4_cycle_xloc_2 la4_cycle_xloc_2; 
	MeasRetu_la4_cycle_xloc_1 la4_cycle_xloc_1; 
	MeasRetu_la5_cycle_xloc_4 la5_cycle_xloc_4; 
	MeasRetu_la5_cycle_xloc_3 la5_cycle_xloc_3; 

	MeasRetu_la5_cycle_xloc_2 la5_cycle_xloc_2; 
	MeasRetu_la5_cycle_xloc_1 la5_cycle_xloc_1; 
	MeasRetu_la6_cycle_xloc_4 la6_cycle_xloc_4; 
	MeasRetu_la6_cycle_xloc_3 la6_cycle_xloc_3; 

	MeasRetu_la6_cycle_xloc_2 la6_cycle_xloc_2; 
	MeasRetu_la6_cycle_xloc_1 la6_cycle_xloc_1; 
	MeasRetu_la7_cycle_xloc_4 la7_cycle_xloc_4; 
	MeasRetu_la7_cycle_xloc_3 la7_cycle_xloc_3; 

	MeasRetu_la7_cycle_xloc_2 la7_cycle_xloc_2; 
	MeasRetu_la7_cycle_xloc_1 la7_cycle_xloc_1; 
	MeasRetu_la8_cycle_xloc_4 la8_cycle_xloc_4; 
	MeasRetu_la8_cycle_xloc_3 la8_cycle_xloc_3; 

	MeasRetu_la8_cycle_xloc_2 la8_cycle_xloc_2; 
	MeasRetu_la8_cycle_xloc_1 la8_cycle_xloc_1; 
	MeasRetu_la9_cycle_xloc_4 la9_cycle_xloc_4; 
	MeasRetu_la9_cycle_xloc_3 la9_cycle_xloc_3; 

	MeasRetu_la9_cycle_xloc_2 la9_cycle_xloc_2; 
	MeasRetu_la9_cycle_xloc_1 la9_cycle_xloc_1; 
	MeasRetu_la10_cycle_xloc_4 la10_cycle_xloc_4; 
	MeasRetu_la10_cycle_xloc_3 la10_cycle_xloc_3; 

	MeasRetu_la10_cycle_xloc_2 la10_cycle_xloc_2; 
	MeasRetu_la10_cycle_xloc_1 la10_cycle_xloc_1; 
	MeasRetu_la11_cycle_xloc_4 la11_cycle_xloc_4; 
	MeasRetu_la11_cycle_xloc_3 la11_cycle_xloc_3; 

	MeasRetu_la11_cycle_xloc_2 la11_cycle_xloc_2; 
	MeasRetu_la11_cycle_xloc_1 la11_cycle_xloc_1; 
	MeasRetu_la12_cycle_xloc_4 la12_cycle_xloc_4; 
	MeasRetu_la12_cycle_xloc_3 la12_cycle_xloc_3; 

	MeasRetu_la12_cycle_xloc_2 la12_cycle_xloc_2; 
	MeasRetu_la12_cycle_xloc_1 la12_cycle_xloc_1; 
	MeasRetu_la13_cycle_xloc_4 la13_cycle_xloc_4; 
	MeasRetu_la13_cycle_xloc_3 la13_cycle_xloc_3; 

	MeasRetu_la13_cycle_xloc_2 la13_cycle_xloc_2; 
	MeasRetu_la13_cycle_xloc_1 la13_cycle_xloc_1; 
	MeasRetu_la14_cycle_xloc_4 la14_cycle_xloc_4; 
	MeasRetu_la14_cycle_xloc_3 la14_cycle_xloc_3; 

	MeasRetu_la14_cycle_xloc_2 la14_cycle_xloc_2; 
	MeasRetu_la14_cycle_xloc_1 la14_cycle_xloc_1; 
	MeasRetu_la15_cycle_xloc_4 la15_cycle_xloc_4; 
	MeasRetu_la15_cycle_xloc_3 la15_cycle_xloc_3; 

	MeasRetu_la15_cycle_xloc_2 la15_cycle_xloc_2; 
	MeasRetu_la15_cycle_xloc_1 la15_cycle_xloc_1; 
	MeasRetu_dlyresult1_from_xloc2 dlyresult1_from_xloc2; 
	MeasRetu_dlyresult1_from_xloc1 dlyresult1_from_xloc1; 

	MeasRetu_dlyresult1_to_xloc2_pre dlyresult1_to_xloc2_pre; 
	MeasRetu_dlyresult1_to_xloc2 dlyresult1_to_xloc2; 
	MeasRetu_dlyresult1_to_xloc1_pre dlyresult1_to_xloc1_pre; 
	MeasRetu_dlyresult1_to_xloc1 dlyresult1_to_xloc1; 

	MeasRetu_dlyresult2_from_xloc2 dlyresult2_from_xloc2; 
	MeasRetu_dlyresult2_from_xloc1 dlyresult2_from_xloc1; 
	MeasRetu_dlyresult2_to_xloc2_pre dlyresult2_to_xloc2_pre; 
	MeasRetu_dlyresult2_to_xloc2 dlyresult2_to_xloc2; 

	MeasRetu_dlyresult2_to_xloc1_pre dlyresult2_to_xloc1_pre; 
	MeasRetu_dlyresult2_to_xloc1 dlyresult2_to_xloc1; 
	MeasRetu_dlyresult3_from_xloc2 dlyresult3_from_xloc2; 
	MeasRetu_dlyresult3_from_xloc1 dlyresult3_from_xloc1; 

	MeasRetu_dlyresult3_to_xloc2_pre dlyresult3_to_xloc2_pre; 
	MeasRetu_dlyresult3_to_xloc2 dlyresult3_to_xloc2; 
	MeasRetu_dlyresult3_to_xloc1_pre dlyresult3_to_xloc1_pre; 
	MeasRetu_dlyresult3_to_xloc1 dlyresult3_to_xloc1; 

	MeasRetu_dlyresult4_from_xloc2 dlyresult4_from_xloc2; 
	MeasRetu_dlyresult4_from_xloc1 dlyresult4_from_xloc1; 
	MeasRetu_dlyresult4_to_xloc2_pre dlyresult4_to_xloc2_pre; 
	MeasRetu_dlyresult4_to_xloc2 dlyresult4_to_xloc2; 

	MeasRetu_dlyresult4_to_xloc1_pre dlyresult4_to_xloc1_pre; 
	MeasRetu_dlyresult4_to_xloc1 dlyresult4_to_xloc1; 
	MeasRetu_dlyresult5_from_xloc2 dlyresult5_from_xloc2; 
	MeasRetu_dlyresult5_from_xloc1 dlyresult5_from_xloc1; 

	MeasRetu_dlyresult5_to_xloc2_pre dlyresult5_to_xloc2_pre; 
	MeasRetu_dlyresult5_to_xloc2 dlyresult5_to_xloc2; 
	MeasRetu_dlyresult5_to_xloc1_pre dlyresult5_to_xloc1_pre; 
	MeasRetu_dlyresult5_to_xloc1 dlyresult5_to_xloc1; 

	MeasRetu_dlyresult6_from_xloc2 dlyresult6_from_xloc2; 
	MeasRetu_dlyresult6_from_xloc1 dlyresult6_from_xloc1; 
	MeasRetu_dlyresult6_to_xloc2_pre dlyresult6_to_xloc2_pre; 
	MeasRetu_dlyresult6_to_xloc2 dlyresult6_to_xloc2; 

	MeasRetu_dlyresult6_to_xloc1_pre dlyresult6_to_xloc1_pre; 
	MeasRetu_dlyresult6_to_xloc1 dlyresult6_to_xloc1; 
	MeasRetu_dlyresult7_from_xloc2 dlyresult7_from_xloc2; 
	MeasRetu_dlyresult7_from_xloc1 dlyresult7_from_xloc1; 

	MeasRetu_dlyresult7_to_xloc2_pre dlyresult7_to_xloc2_pre; 
	MeasRetu_dlyresult7_to_xloc2 dlyresult7_to_xloc2; 
	MeasRetu_dlyresult7_to_xloc1_pre dlyresult7_to_xloc1_pre; 
	MeasRetu_dlyresult7_to_xloc1 dlyresult7_to_xloc1; 

	MeasRetu_dlyresult8_from_xloc2 dlyresult8_from_xloc2; 
	MeasRetu_dlyresult8_from_xloc1 dlyresult8_from_xloc1; 
	MeasRetu_dlyresult8_to_xloc2_pre dlyresult8_to_xloc2_pre; 
	MeasRetu_dlyresult8_to_xloc2 dlyresult8_to_xloc2; 

	MeasRetu_dlyresult8_to_xloc1_pre dlyresult8_to_xloc1_pre; 
	MeasRetu_dlyresult8_to_xloc1 dlyresult8_to_xloc1; 
	MeasRetu_dlyresult9_from_xloc2 dlyresult9_from_xloc2; 
	MeasRetu_dlyresult9_from_xloc1 dlyresult9_from_xloc1; 

	MeasRetu_dlyresult9_to_xloc2_pre dlyresult9_to_xloc2_pre; 
	MeasRetu_dlyresult9_to_xloc2 dlyresult9_to_xloc2; 
	MeasRetu_dlyresult9_to_xloc1_pre dlyresult9_to_xloc1_pre; 
	MeasRetu_dlyresult9_to_xloc1 dlyresult9_to_xloc1; 

	MeasRetu_dlyresult10_from_xloc2 dlyresult10_from_xloc2; 
	MeasRetu_dlyresult10_from_xloc1 dlyresult10_from_xloc1; 
	MeasRetu_dlyresult10_to_xloc2_pre dlyresult10_to_xloc2_pre; 
	MeasRetu_dlyresult10_to_xloc2 dlyresult10_to_xloc2; 

	MeasRetu_dlyresult10_to_xloc1_pre dlyresult10_to_xloc1_pre; 
	MeasRetu_dlyresult10_to_xloc1 dlyresult10_to_xloc1; 
	MeasRetu_reg getcha_max_min_top_base_cha_vmin(  );
	MeasRetu_reg getcha_max_min_top_base_cha_vmax(  );
	MeasRetu_reg getcha_max_min_top_base_cha_vbase(  );
	MeasRetu_reg getcha_max_min_top_base_cha_vtop(  );

	MeasRetu_reg getcha_max_min_top_base(  );

	MeasRetu_reg getcha_threshold_l_mid_h_cha_threshold_l(  );
	MeasRetu_reg getcha_threshold_l_mid_h_cha_threshold_mid(  );
	MeasRetu_reg getcha_threshold_l_mid_h_cha_threshold_h(  );
	MeasRetu_reg getcha_threshold_l_mid_h_reserved(  );

	MeasRetu_reg getcha_threshold_l_mid_h(  );

	MeasRetu_reg getcha_vmax_xloc_vmax_xloc(  );
	MeasRetu_reg getcha_vmax_xloc_reserved(  );
	MeasRetu_reg getcha_vmax_xloc_vmax_xloc_vld(  );

	MeasRetu_reg getcha_vmax_xloc(  );

	MeasRetu_reg getcha_vmin_xloc_vmin_xloc(  );
	MeasRetu_reg getcha_vmin_xloc_reserved(  );
	MeasRetu_reg getcha_vmin_xloc_vmin_xloc_vld(  );

	MeasRetu_reg getcha_vmin_xloc(  );

	MeasRetu_reg getcha_rtime_rtime(  );
	MeasRetu_reg getcha_rtime_reserved(  );
	MeasRetu_reg getcha_rtime_rtime_vld(  );

	MeasRetu_reg getcha_rtime(  );

	MeasRetu_reg getcha_rtime_xloc_end_rtime_xloc_end(  );
	MeasRetu_reg getcha_rtime_xloc_end_reserved(  );
	MeasRetu_reg getcha_rtime_xloc_end_rtime_xloc_end_vld(  );

	MeasRetu_reg getcha_rtime_xloc_end(  );

	MeasRetu_reg getcha_rtime_pre_rtime_pre(  );
	MeasRetu_reg getcha_rtime_pre_reserved(  );
	MeasRetu_reg getcha_rtime_pre_rtime_pre_vld(  );

	MeasRetu_reg getcha_rtime_pre(  );

	MeasRetu_reg getcha_rtime_xloc_end_pre_rtime_xloc_end_pre(  );
	MeasRetu_reg getcha_rtime_xloc_end_pre_reserved(  );
	MeasRetu_reg getcha_rtime_xloc_end_pre_rtime_xloc_end_pre_vld(  );

	MeasRetu_reg getcha_rtime_xloc_end_pre(  );

	MeasRetu_reg getcha_ftime_ftime(  );
	MeasRetu_reg getcha_ftime_reserved(  );
	MeasRetu_reg getcha_ftime_ftime_vld(  );

	MeasRetu_reg getcha_ftime(  );

	MeasRetu_reg getcha_ftime_xloc_end_ftime_xloc_end(  );
	MeasRetu_reg getcha_ftime_xloc_end_reserved(  );
	MeasRetu_reg getcha_ftime_xloc_end_ftime_xloc_end_vld(  );

	MeasRetu_reg getcha_ftime_xloc_end(  );

	MeasRetu_reg getcha_ftime_pre_ftime_pre(  );
	MeasRetu_reg getcha_ftime_pre_reserved(  );
	MeasRetu_reg getcha_ftime_pre_ftime_pre_vld(  );

	MeasRetu_reg getcha_ftime_pre(  );

	MeasRetu_reg getcha_ftime_xloc_end_pre_ftime_xloc_end_pre(  );
	MeasRetu_reg getcha_ftime_xloc_end_pre_reserved(  );
	MeasRetu_reg getcha_ftime_xloc_end_pre_ftime_xloc_end_pre_vld(  );

	MeasRetu_reg getcha_ftime_xloc_end_pre(  );

	MeasRetu_reg getcha_cycle_xloc_4_cycle_xloc_4(  );
	MeasRetu_reg getcha_cycle_xloc_4_edge_type(  );
	MeasRetu_reg getcha_cycle_xloc_4_cycle_xloc_4_vld(  );

	MeasRetu_reg getcha_cycle_xloc_4(  );

	MeasRetu_reg getcha_cycle_xloc_3_cycle_xloc_3(  );
	MeasRetu_reg getcha_cycle_xloc_3_edge_type(  );
	MeasRetu_reg getcha_cycle_xloc_3_cycle_xloc_3_vld(  );

	MeasRetu_reg getcha_cycle_xloc_3(  );

	MeasRetu_reg getcha_cycle_xloc_2_cycle_xloc_2(  );
	MeasRetu_reg getcha_cycle_xloc_2_edge_type(  );
	MeasRetu_reg getcha_cycle_xloc_2_cycle_xloc_2_vld(  );

	MeasRetu_reg getcha_cycle_xloc_2(  );

	MeasRetu_reg getcha_cycle_xloc_1_cycle_xloc_1(  );
	MeasRetu_reg getcha_cycle_xloc_1_edge_type(  );
	MeasRetu_reg getcha_cycle_xloc_1_cycle_xloc_1_vld(  );

	MeasRetu_reg getcha_cycle_xloc_1(  );

	MeasRetu_reg getcha_ave_sum_n_bits31_0_ave_sum_n_bit31_0(  );

	MeasRetu_reg getcha_ave_sum_n_bits31_0(  );

	MeasRetu_reg getcha_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0(  );

	MeasRetu_reg getcha_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg getcha_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0(  );

	MeasRetu_reg getcha_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg getcha_ave_sum_x_bit39_32_ave_sum_n_bit39_32(  );
	MeasRetu_reg getcha_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32(  );
	MeasRetu_reg getcha_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32(  );
	MeasRetu_reg getcha_ave_sum_x_bit39_32_reserved(  );

	MeasRetu_reg getcha_ave_sum_x_bit39_32(  );

	MeasRetu_reg getcha_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getcha_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getcha_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getcha_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getcha_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getcha_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getcha_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32(  );
	MeasRetu_reg getcha_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32(  );

	MeasRetu_reg getcha_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg getcha_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32(  );
	MeasRetu_reg getcha_vrms_sum_2_bits47_32_reserved(  );

	MeasRetu_reg getcha_vrms_sum_2_bits47_32(  );

	MeasRetu_reg getcha_total_rf_num_total_rf_num(  );
	MeasRetu_reg getcha_total_rf_num_slope(  );
	MeasRetu_reg getcha_total_rf_num_valid(  );

	MeasRetu_reg getcha_total_rf_num(  );

	MeasRetu_reg getreserved_x1(  );

	MeasRetu_reg getcha_shoot_over_shoot(  );
	MeasRetu_reg getcha_shoot_pre_shoot(  );
	MeasRetu_reg getcha_shoot_reserved(  );

	MeasRetu_reg getcha_shoot(  );

	MeasRetu_reg getcha_edge_cnt_start_xloc_edge_cnt_start_xloc(  );
	MeasRetu_reg getcha_edge_cnt_start_xloc_reserved(  );
	MeasRetu_reg getcha_edge_cnt_start_xloc_valid(  );

	MeasRetu_reg getcha_edge_cnt_start_xloc(  );

	MeasRetu_reg getcha_edge_cnt_end_xloc_edge_cnt_end_xloc(  );
	MeasRetu_reg getcha_edge_cnt_end_xloc_reserved(  );
	MeasRetu_reg getcha_edge_cnt_end_xloc_valid(  );

	MeasRetu_reg getcha_edge_cnt_end_xloc(  );

	MeasRetu_reg getcha_edge_cnt_start2_xloc_edge_cnt_start2_xloc(  );
	MeasRetu_reg getcha_edge_cnt_start2_xloc_reserved(  );
	MeasRetu_reg getcha_edge_cnt_start2_xloc_valid(  );

	MeasRetu_reg getcha_edge_cnt_start2_xloc(  );

	MeasRetu_reg getcha_edge_cnt_end2_xloc_edge_cnt_end2_xloc(  );
	MeasRetu_reg getcha_edge_cnt_end2_xloc_reserved(  );
	MeasRetu_reg getcha_edge_cnt_end2_xloc_valid(  );

	MeasRetu_reg getcha_edge_cnt_end2_xloc(  );

	MeasRetu_reg getchb_max_min_top_base_cha_vmin(  );
	MeasRetu_reg getchb_max_min_top_base_cha_vmax(  );
	MeasRetu_reg getchb_max_min_top_base_cha_vbase(  );
	MeasRetu_reg getchb_max_min_top_base_cha_vtop(  );

	MeasRetu_reg getchb_max_min_top_base(  );

	MeasRetu_reg getchb_threshold_l_mid_h_cha_threshold_l(  );
	MeasRetu_reg getchb_threshold_l_mid_h_cha_threshold_mid(  );
	MeasRetu_reg getchb_threshold_l_mid_h_cha_threshold_h(  );
	MeasRetu_reg getchb_threshold_l_mid_h_reserved(  );

	MeasRetu_reg getchb_threshold_l_mid_h(  );

	MeasRetu_reg getchb_vmax_xloc_vmax_xloc(  );
	MeasRetu_reg getchb_vmax_xloc_reserved(  );
	MeasRetu_reg getchb_vmax_xloc_vmax_xloc_vld(  );

	MeasRetu_reg getchb_vmax_xloc(  );

	MeasRetu_reg getchb_vmin_xloc_vmin_xloc(  );
	MeasRetu_reg getchb_vmin_xloc_reserved(  );
	MeasRetu_reg getchb_vmin_xloc_vmin_xloc_vld(  );

	MeasRetu_reg getchb_vmin_xloc(  );

	MeasRetu_reg getchb_rtime_rtime(  );
	MeasRetu_reg getchb_rtime_reserved(  );
	MeasRetu_reg getchb_rtime_rtime_vld(  );

	MeasRetu_reg getchb_rtime(  );

	MeasRetu_reg getchb_rtime_xloc_end_rtime_xloc_end(  );
	MeasRetu_reg getchb_rtime_xloc_end_reserved(  );
	MeasRetu_reg getchb_rtime_xloc_end_rtime_xloc_end_vld(  );

	MeasRetu_reg getchb_rtime_xloc_end(  );

	MeasRetu_reg getchb_rtime_pre_rtime_pre(  );
	MeasRetu_reg getchb_rtime_pre_reserved(  );
	MeasRetu_reg getchb_rtime_pre_rtime_pre_vld(  );

	MeasRetu_reg getchb_rtime_pre(  );

	MeasRetu_reg getchb_rtime_xloc_end_pre_rtime_xloc_end_pre(  );
	MeasRetu_reg getchb_rtime_xloc_end_pre_reserved(  );
	MeasRetu_reg getchb_rtime_xloc_end_pre_rtime_xloc_end_pre_vld(  );

	MeasRetu_reg getchb_rtime_xloc_end_pre(  );

	MeasRetu_reg getchb_ftime_ftime(  );
	MeasRetu_reg getchb_ftime_reserved(  );
	MeasRetu_reg getchb_ftime_ftime_vld(  );

	MeasRetu_reg getchb_ftime(  );

	MeasRetu_reg getchb_ftime_xloc_end_ftime_xloc_end(  );
	MeasRetu_reg getchb_ftime_xloc_end_reserved(  );
	MeasRetu_reg getchb_ftime_xloc_end_ftime_xloc_end_vld(  );

	MeasRetu_reg getchb_ftime_xloc_end(  );

	MeasRetu_reg getchb_ftime_pre_ftime_pre(  );
	MeasRetu_reg getchb_ftime_pre_reserved(  );
	MeasRetu_reg getchb_ftime_pre_ftime_pre_vld(  );

	MeasRetu_reg getchb_ftime_pre(  );

	MeasRetu_reg getchb_ftime_xloc_end_pre_ftime_xloc_end_pre(  );
	MeasRetu_reg getchb_ftime_xloc_end_pre_reserved(  );
	MeasRetu_reg getchb_ftime_xloc_end_pre_ftime_xloc_end_pre_vld(  );

	MeasRetu_reg getchb_ftime_xloc_end_pre(  );

	MeasRetu_reg getchb_cycle_xloc_4_cycle_xloc_4(  );
	MeasRetu_reg getchb_cycle_xloc_4_edge_type(  );
	MeasRetu_reg getchb_cycle_xloc_4_cycle_xloc_4_vld(  );

	MeasRetu_reg getchb_cycle_xloc_4(  );

	MeasRetu_reg getchb_cycle_xloc_3_cycle_xloc_3(  );
	MeasRetu_reg getchb_cycle_xloc_3_edge_type(  );
	MeasRetu_reg getchb_cycle_xloc_3_cycle_xloc_3_vld(  );

	MeasRetu_reg getchb_cycle_xloc_3(  );

	MeasRetu_reg getchb_cycle_xloc_2_cycle_xloc_2(  );
	MeasRetu_reg getchb_cycle_xloc_2_edge_type(  );
	MeasRetu_reg getchb_cycle_xloc_2_cycle_xloc_2_vld(  );

	MeasRetu_reg getchb_cycle_xloc_2(  );

	MeasRetu_reg getchb_cycle_xloc_1_cycle_xloc_1(  );
	MeasRetu_reg getchb_cycle_xloc_1_edge_type(  );
	MeasRetu_reg getchb_cycle_xloc_1_cycle_xloc_1_vld(  );

	MeasRetu_reg getchb_cycle_xloc_1(  );

	MeasRetu_reg getchb_ave_sum_n_bits31_0_ave_sum_n_bit31_0(  );

	MeasRetu_reg getchb_ave_sum_n_bits31_0(  );

	MeasRetu_reg getchb_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0(  );

	MeasRetu_reg getchb_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg getchb_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0(  );

	MeasRetu_reg getchb_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg getchb_ave_sum_x_bit39_32_ave_sum_n_bit39_32(  );
	MeasRetu_reg getchb_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32(  );
	MeasRetu_reg getchb_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32(  );
	MeasRetu_reg getchb_ave_sum_x_bit39_32_reserved(  );

	MeasRetu_reg getchb_ave_sum_x_bit39_32(  );

	MeasRetu_reg getchb_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getchb_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getchb_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getchb_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getchb_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getchb_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getchb_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32(  );
	MeasRetu_reg getchb_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32(  );

	MeasRetu_reg getchb_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg getchb_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32(  );
	MeasRetu_reg getchb_vrms_sum_2_bits47_32_reserved(  );

	MeasRetu_reg getchb_vrms_sum_2_bits47_32(  );

	MeasRetu_reg getchb_total_rf_num_total_rf_num(  );
	MeasRetu_reg getchb_total_rf_num_slope(  );
	MeasRetu_reg getchb_total_rf_num_valid(  );

	MeasRetu_reg getchb_total_rf_num(  );

	MeasRetu_reg getreserved_x2(  );

	MeasRetu_reg getchb_shoot_over_shoot(  );
	MeasRetu_reg getchb_shoot_pre_shoot(  );
	MeasRetu_reg getchb_shoot_reserved(  );

	MeasRetu_reg getchb_shoot(  );

	MeasRetu_reg getchb_edge_cnt_start_xloc_edge_cnt_start_xloc(  );
	MeasRetu_reg getchb_edge_cnt_start_xloc_reserved(  );
	MeasRetu_reg getchb_edge_cnt_start_xloc_valid(  );

	MeasRetu_reg getchb_edge_cnt_start_xloc(  );

	MeasRetu_reg getchb_edge_cnt_end_xloc_edge_cnt_end_xloc(  );
	MeasRetu_reg getchb_edge_cnt_end_xloc_reserved(  );
	MeasRetu_reg getchb_edge_cnt_end_xloc_valid(  );

	MeasRetu_reg getchb_edge_cnt_end_xloc(  );

	MeasRetu_reg getchb_edge_cnt_start2_xloc_edge_cnt_start2_xloc(  );
	MeasRetu_reg getchb_edge_cnt_start2_xloc_reserved(  );
	MeasRetu_reg getchb_edge_cnt_start2_xloc_valid(  );

	MeasRetu_reg getchb_edge_cnt_start2_xloc(  );

	MeasRetu_reg getchb_edge_cnt_end2_xloc_edge_cnt_end2_xloc(  );
	MeasRetu_reg getchb_edge_cnt_end2_xloc_reserved(  );
	MeasRetu_reg getchb_edge_cnt_end2_xloc_valid(  );

	MeasRetu_reg getchb_edge_cnt_end2_xloc(  );

	MeasRetu_reg getchc_max_min_top_base_cha_vmin(  );
	MeasRetu_reg getchc_max_min_top_base_cha_vmax(  );
	MeasRetu_reg getchc_max_min_top_base_cha_vbase(  );
	MeasRetu_reg getchc_max_min_top_base_cha_vtop(  );

	MeasRetu_reg getchc_max_min_top_base(  );

	MeasRetu_reg getchc_threshold_l_mid_h_cha_threshold_l(  );
	MeasRetu_reg getchc_threshold_l_mid_h_cha_threshold_mid(  );
	MeasRetu_reg getchc_threshold_l_mid_h_cha_threshold_h(  );
	MeasRetu_reg getchc_threshold_l_mid_h_reserved(  );

	MeasRetu_reg getchc_threshold_l_mid_h(  );

	MeasRetu_reg getchc_vmax_xloc_vmax_xloc(  );
	MeasRetu_reg getchc_vmax_xloc_reserved(  );
	MeasRetu_reg getchc_vmax_xloc_vmax_xloc_vld(  );

	MeasRetu_reg getchc_vmax_xloc(  );

	MeasRetu_reg getchc_vmin_xloc_vmin_xloc(  );
	MeasRetu_reg getchc_vmin_xloc_reserved(  );
	MeasRetu_reg getchc_vmin_xloc_vmin_xloc_vld(  );

	MeasRetu_reg getchc_vmin_xloc(  );

	MeasRetu_reg getchc_rtime_rtime(  );
	MeasRetu_reg getchc_rtime_reserved(  );
	MeasRetu_reg getchc_rtime_rtime_vld(  );

	MeasRetu_reg getchc_rtime(  );

	MeasRetu_reg getchc_rtime_xloc_end_rtime_xloc_end(  );
	MeasRetu_reg getchc_rtime_xloc_end_reserved(  );
	MeasRetu_reg getchc_rtime_xloc_end_rtime_xloc_end_vld(  );

	MeasRetu_reg getchc_rtime_xloc_end(  );

	MeasRetu_reg getchc_rtime_pre_rtime_pre(  );
	MeasRetu_reg getchc_rtime_pre_reserved(  );
	MeasRetu_reg getchc_rtime_pre_rtime_pre_vld(  );

	MeasRetu_reg getchc_rtime_pre(  );

	MeasRetu_reg getchc_rtime_xloc_end_pre_rtime_xloc_end_pre(  );
	MeasRetu_reg getchc_rtime_xloc_end_pre_reserved(  );
	MeasRetu_reg getchc_rtime_xloc_end_pre_rtime_xloc_end_pre_vld(  );

	MeasRetu_reg getchc_rtime_xloc_end_pre(  );

	MeasRetu_reg getchc_ftime_ftime(  );
	MeasRetu_reg getchc_ftime_reserved(  );
	MeasRetu_reg getchc_ftime_ftime_vld(  );

	MeasRetu_reg getchc_ftime(  );

	MeasRetu_reg getchc_ftime_xloc_end_ftime_xloc_end(  );
	MeasRetu_reg getchc_ftime_xloc_end_reserved(  );
	MeasRetu_reg getchc_ftime_xloc_end_ftime_xloc_end_vld(  );

	MeasRetu_reg getchc_ftime_xloc_end(  );

	MeasRetu_reg getchc_ftime_pre_ftime_pre(  );
	MeasRetu_reg getchc_ftime_pre_reserved(  );
	MeasRetu_reg getchc_ftime_pre_ftime_pre_vld(  );

	MeasRetu_reg getchc_ftime_pre(  );

	MeasRetu_reg getchc_ftime_xloc_end_pre_ftime_xloc_end_pre(  );
	MeasRetu_reg getchc_ftime_xloc_end_pre_reserved(  );
	MeasRetu_reg getchc_ftime_xloc_end_pre_ftime_xloc_end_pre_vld(  );

	MeasRetu_reg getchc_ftime_xloc_end_pre(  );

	MeasRetu_reg getchc_cycle_xloc_4_cycle_xloc_4(  );
	MeasRetu_reg getchc_cycle_xloc_4_edge_type(  );
	MeasRetu_reg getchc_cycle_xloc_4_cycle_xloc_4_vld(  );

	MeasRetu_reg getchc_cycle_xloc_4(  );

	MeasRetu_reg getchc_cycle_xloc_3_cycle_xloc_3(  );
	MeasRetu_reg getchc_cycle_xloc_3_edge_type(  );
	MeasRetu_reg getchc_cycle_xloc_3_cycle_xloc_3_vld(  );

	MeasRetu_reg getchc_cycle_xloc_3(  );

	MeasRetu_reg getchc_cycle_xloc_2_cycle_xloc_2(  );
	MeasRetu_reg getchc_cycle_xloc_2_edge_type(  );
	MeasRetu_reg getchc_cycle_xloc_2_cycle_xloc_2_vld(  );

	MeasRetu_reg getchc_cycle_xloc_2(  );

	MeasRetu_reg getchc_cycle_xloc_1_cycle_xloc_1(  );
	MeasRetu_reg getchc_cycle_xloc_1_edge_type(  );
	MeasRetu_reg getchc_cycle_xloc_1_cycle_xloc_1_vld(  );

	MeasRetu_reg getchc_cycle_xloc_1(  );

	MeasRetu_reg getchc_ave_sum_n_bits31_0_ave_sum_n_bit31_0(  );

	MeasRetu_reg getchc_ave_sum_n_bits31_0(  );

	MeasRetu_reg getchc_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0(  );

	MeasRetu_reg getchc_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg getchc_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0(  );

	MeasRetu_reg getchc_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg getchc_ave_sum_x_bit39_32_ave_sum_n_bit39_32(  );
	MeasRetu_reg getchc_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32(  );
	MeasRetu_reg getchc_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32(  );
	MeasRetu_reg getchc_ave_sum_x_bit39_32_reserved(  );

	MeasRetu_reg getchc_ave_sum_x_bit39_32(  );

	MeasRetu_reg getchc_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getchc_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getchc_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getchc_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getchc_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getchc_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getchc_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32(  );
	MeasRetu_reg getchc_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32(  );

	MeasRetu_reg getchc_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg getchc_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32(  );
	MeasRetu_reg getchc_vrms_sum_2_bits47_32_reserved(  );

	MeasRetu_reg getchc_vrms_sum_2_bits47_32(  );

	MeasRetu_reg getchc_total_rf_num_total_rf_num(  );
	MeasRetu_reg getchc_total_rf_num_slope(  );
	MeasRetu_reg getchc_total_rf_num_valid(  );

	MeasRetu_reg getchc_total_rf_num(  );

	MeasRetu_reg getreserved_x3(  );

	MeasRetu_reg getchc_shoot_over_shoot(  );
	MeasRetu_reg getchc_shoot_pre_shoot(  );
	MeasRetu_reg getchc_shoot_reserved(  );

	MeasRetu_reg getchc_shoot(  );

	MeasRetu_reg getchc_edge_cnt_start_xloc_edge_cnt_start_xloc(  );
	MeasRetu_reg getchc_edge_cnt_start_xloc_reserved(  );
	MeasRetu_reg getchc_edge_cnt_start_xloc_valid(  );

	MeasRetu_reg getchc_edge_cnt_start_xloc(  );

	MeasRetu_reg getchc_edge_cnt_end_xloc_edge_cnt_end_xloc(  );
	MeasRetu_reg getchc_edge_cnt_end_xloc_reserved(  );
	MeasRetu_reg getchc_edge_cnt_end_xloc_valid(  );

	MeasRetu_reg getchc_edge_cnt_end_xloc(  );

	MeasRetu_reg getchc_edge_cnt_start2_xloc_edge_cnt_start2_xloc(  );
	MeasRetu_reg getchc_edge_cnt_start2_xloc_reserved(  );
	MeasRetu_reg getchc_edge_cnt_start2_xloc_valid(  );

	MeasRetu_reg getchc_edge_cnt_start2_xloc(  );

	MeasRetu_reg getchc_edge_cnt_end2_xloc_edge_cnt_end2_xloc(  );
	MeasRetu_reg getchc_edge_cnt_end2_xloc_reserved(  );
	MeasRetu_reg getchc_edge_cnt_end2_xloc_valid(  );

	MeasRetu_reg getchc_edge_cnt_end2_xloc(  );

	MeasRetu_reg getchd_max_min_top_base_cha_vmin(  );
	MeasRetu_reg getchd_max_min_top_base_cha_vmax(  );
	MeasRetu_reg getchd_max_min_top_base_cha_vbase(  );
	MeasRetu_reg getchd_max_min_top_base_cha_vtop(  );

	MeasRetu_reg getchd_max_min_top_base(  );

	MeasRetu_reg getchd_threshold_l_mid_h_cha_threshold_l(  );
	MeasRetu_reg getchd_threshold_l_mid_h_cha_threshold_mid(  );
	MeasRetu_reg getchd_threshold_l_mid_h_cha_threshold_h(  );
	MeasRetu_reg getchd_threshold_l_mid_h_reserved(  );

	MeasRetu_reg getchd_threshold_l_mid_h(  );

	MeasRetu_reg getchd_vmax_xloc_vmax_xloc(  );
	MeasRetu_reg getchd_vmax_xloc_reserved(  );
	MeasRetu_reg getchd_vmax_xloc_vmax_xloc_vld(  );

	MeasRetu_reg getchd_vmax_xloc(  );

	MeasRetu_reg getchd_vmin_xloc_vmin_xloc(  );
	MeasRetu_reg getchd_vmin_xloc_reserved(  );
	MeasRetu_reg getchd_vmin_xloc_vmin_xloc_vld(  );

	MeasRetu_reg getchd_vmin_xloc(  );

	MeasRetu_reg getchd_rtime_rtime(  );
	MeasRetu_reg getchd_rtime_reserved(  );
	MeasRetu_reg getchd_rtime_rtime_vld(  );

	MeasRetu_reg getchd_rtime(  );

	MeasRetu_reg getchd_rtime_xloc_end_rtime_xloc_end(  );
	MeasRetu_reg getchd_rtime_xloc_end_reserved(  );
	MeasRetu_reg getchd_rtime_xloc_end_rtime_xloc_end_vld(  );

	MeasRetu_reg getchd_rtime_xloc_end(  );

	MeasRetu_reg getchd_rtime_pre_rtime_pre(  );
	MeasRetu_reg getchd_rtime_pre_reserved(  );
	MeasRetu_reg getchd_rtime_pre_rtime_pre_vld(  );

	MeasRetu_reg getchd_rtime_pre(  );

	MeasRetu_reg getchd_rtime_xloc_end_pre_rtime_xloc_end_pre(  );
	MeasRetu_reg getchd_rtime_xloc_end_pre_reserved(  );
	MeasRetu_reg getchd_rtime_xloc_end_pre_rtime_xloc_end_pre_vld(  );

	MeasRetu_reg getchd_rtime_xloc_end_pre(  );

	MeasRetu_reg getchd_ftime_ftime(  );
	MeasRetu_reg getchd_ftime_reserved(  );
	MeasRetu_reg getchd_ftime_ftime_vld(  );

	MeasRetu_reg getchd_ftime(  );

	MeasRetu_reg getchd_ftime_xloc_end_ftime_xloc_end(  );
	MeasRetu_reg getchd_ftime_xloc_end_reserved(  );
	MeasRetu_reg getchd_ftime_xloc_end_ftime_xloc_end_vld(  );

	MeasRetu_reg getchd_ftime_xloc_end(  );

	MeasRetu_reg getchd_ftime_pre_ftime_pre(  );
	MeasRetu_reg getchd_ftime_pre_reserved(  );
	MeasRetu_reg getchd_ftime_pre_ftime_pre_vld(  );

	MeasRetu_reg getchd_ftime_pre(  );

	MeasRetu_reg getchd_ftime_xloc_end_pre_ftime_xloc_end_pre(  );
	MeasRetu_reg getchd_ftime_xloc_end_pre_reserved(  );
	MeasRetu_reg getchd_ftime_xloc_end_pre_ftime_xloc_end_pre_vld(  );

	MeasRetu_reg getchd_ftime_xloc_end_pre(  );

	MeasRetu_reg getchd_cycle_xloc_4_cycle_xloc_4(  );
	MeasRetu_reg getchd_cycle_xloc_4_edge_type(  );
	MeasRetu_reg getchd_cycle_xloc_4_cycle_xloc_4_vld(  );

	MeasRetu_reg getchd_cycle_xloc_4(  );

	MeasRetu_reg getchd_cycle_xloc_3_cycle_xloc_3(  );
	MeasRetu_reg getchd_cycle_xloc_3_edge_type(  );
	MeasRetu_reg getchd_cycle_xloc_3_cycle_xloc_3_vld(  );

	MeasRetu_reg getchd_cycle_xloc_3(  );

	MeasRetu_reg getchd_cycle_xloc_2_cycle_xloc_2(  );
	MeasRetu_reg getchd_cycle_xloc_2_edge_type(  );
	MeasRetu_reg getchd_cycle_xloc_2_cycle_xloc_2_vld(  );

	MeasRetu_reg getchd_cycle_xloc_2(  );

	MeasRetu_reg getchd_cycle_xloc_1_cycle_xloc_1(  );
	MeasRetu_reg getchd_cycle_xloc_1_edge_type(  );
	MeasRetu_reg getchd_cycle_xloc_1_cycle_xloc_1_vld(  );

	MeasRetu_reg getchd_cycle_xloc_1(  );

	MeasRetu_reg getchd_ave_sum_n_bits31_0_ave_sum_n_bit31_0(  );

	MeasRetu_reg getchd_ave_sum_n_bits31_0(  );

	MeasRetu_reg getchd_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0(  );

	MeasRetu_reg getchd_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg getchd_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0(  );

	MeasRetu_reg getchd_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg getchd_ave_sum_x_bit39_32_ave_sum_n_bit39_32(  );
	MeasRetu_reg getchd_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32(  );
	MeasRetu_reg getchd_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32(  );
	MeasRetu_reg getchd_ave_sum_x_bit39_32_reserved(  );

	MeasRetu_reg getchd_ave_sum_x_bit39_32(  );

	MeasRetu_reg getchd_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getchd_vrms_sum_n_bits31_0(  );

	MeasRetu_reg getchd_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getchd_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg getchd_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getchd_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg getchd_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32(  );
	MeasRetu_reg getchd_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32(  );

	MeasRetu_reg getchd_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg getchd_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32(  );
	MeasRetu_reg getchd_vrms_sum_2_bits47_32_reserved(  );

	MeasRetu_reg getchd_vrms_sum_2_bits47_32(  );

	MeasRetu_reg getchd_total_rf_num_total_rf_num(  );
	MeasRetu_reg getchd_total_rf_num_slope(  );
	MeasRetu_reg getchd_total_rf_num_valid(  );

	MeasRetu_reg getchd_total_rf_num(  );

	MeasRetu_reg getreservd_x4(  );

	MeasRetu_reg getchd_shoot_over_shoot(  );
	MeasRetu_reg getchd_shoot_pre_shoot(  );
	MeasRetu_reg getchd_shoot_reserved(  );

	MeasRetu_reg getchd_shoot(  );

	MeasRetu_reg getchd_edge_cnt_start_xloc_edge_cnt_start_xloc(  );
	MeasRetu_reg getchd_edge_cnt_start_xloc_reserved(  );
	MeasRetu_reg getchd_edge_cnt_start_xloc_valid(  );

	MeasRetu_reg getchd_edge_cnt_start_xloc(  );

	MeasRetu_reg getchd_edge_cnt_end_xloc_edge_cnt_end_xloc(  );
	MeasRetu_reg getchd_edge_cnt_end_xloc_reserved(  );
	MeasRetu_reg getchd_edge_cnt_end_xloc_valid(  );

	MeasRetu_reg getchd_edge_cnt_end_xloc(  );

	MeasRetu_reg getchd_edge_cnt_start2_xloc_edge_cnt_start2_xloc(  );
	MeasRetu_reg getchd_edge_cnt_start2_xloc_reserved(  );
	MeasRetu_reg getchd_edge_cnt_start2_xloc_valid(  );

	MeasRetu_reg getchd_edge_cnt_start2_xloc(  );

	MeasRetu_reg getchd_edge_cnt_end2_xloc_edge_cnt_end2_xloc(  );
	MeasRetu_reg getchd_edge_cnt_end2_xloc_reserved(  );
	MeasRetu_reg getchd_edge_cnt_end2_xloc_valid(  );

	MeasRetu_reg getchd_edge_cnt_end2_xloc(  );

	MeasRetu_reg getmeas_version_rd_meas_version(  );

	MeasRetu_reg getmeas_version_rd(  );

	MeasRetu_reg getmeas_result_done_meas_result_done(  );
	MeasRetu_reg getmeas_result_done_reserved(  );
	MeasRetu_reg getmeas_result_done_rw_test_reg(  );

	MeasRetu_reg getmeas_result_done(  );

	MeasRetu_reg getla0_cycle_xloc_4_la0_cycle_xloc_4(  );
	MeasRetu_reg getla0_cycle_xloc_4_la0_edge_type(  );
	MeasRetu_reg getla0_cycle_xloc_4_la0_cycle_xloc_4_vld(  );

	MeasRetu_reg getla0_cycle_xloc_4(  );

	MeasRetu_reg getla0_cycle_xloc_3_la0_cycle_xloc_3(  );
	MeasRetu_reg getla0_cycle_xloc_3_la0_edge_type(  );
	MeasRetu_reg getla0_cycle_xloc_3_la0_cycle_xloc_3_vld(  );

	MeasRetu_reg getla0_cycle_xloc_3(  );

	MeasRetu_reg getla0_cycle_xloc_2_la0_cycle_xloc_2(  );
	MeasRetu_reg getla0_cycle_xloc_2_la0_edge_type(  );
	MeasRetu_reg getla0_cycle_xloc_2_la0_cycle_xloc_2_vld(  );

	MeasRetu_reg getla0_cycle_xloc_2(  );

	MeasRetu_reg getla0_cycle_xloc_1_la0_cycle_xloc_1(  );
	MeasRetu_reg getla0_cycle_xloc_1_la0_edge_type(  );
	MeasRetu_reg getla0_cycle_xloc_1_la0_cycle_xloc_1_vld(  );

	MeasRetu_reg getla0_cycle_xloc_1(  );

	MeasRetu_reg getla1_cycle_xloc_4_la1_cycle_xloc_4(  );
	MeasRetu_reg getla1_cycle_xloc_4_la1_edge_type(  );
	MeasRetu_reg getla1_cycle_xloc_4_la1_cycle_xloc_4_vld(  );

	MeasRetu_reg getla1_cycle_xloc_4(  );

	MeasRetu_reg getla1_cycle_xloc_3_la1_cycle_xloc_3(  );
	MeasRetu_reg getla1_cycle_xloc_3_la1_edge_type(  );
	MeasRetu_reg getla1_cycle_xloc_3_la1_cycle_xloc_3_vld(  );

	MeasRetu_reg getla1_cycle_xloc_3(  );

	MeasRetu_reg getla1_cycle_xloc_2_la1_cycle_xloc_2(  );
	MeasRetu_reg getla1_cycle_xloc_2_la1_edge_type(  );
	MeasRetu_reg getla1_cycle_xloc_2_la1_cycle_xloc_2_vld(  );

	MeasRetu_reg getla1_cycle_xloc_2(  );

	MeasRetu_reg getla1_cycle_xloc_1_la1_cycle_xloc_1(  );
	MeasRetu_reg getla1_cycle_xloc_1_la1_edge_type(  );
	MeasRetu_reg getla1_cycle_xloc_1_la1_cycle_xloc_1_vld(  );

	MeasRetu_reg getla1_cycle_xloc_1(  );

	MeasRetu_reg getla2_cycle_xloc_4_la2_cycle_xloc_4(  );
	MeasRetu_reg getla2_cycle_xloc_4_la2_edge_type(  );
	MeasRetu_reg getla2_cycle_xloc_4_la2_cycle_xloc_4_vld(  );

	MeasRetu_reg getla2_cycle_xloc_4(  );

	MeasRetu_reg getla2_cycle_xloc_3_la2_cycle_xloc_3(  );
	MeasRetu_reg getla2_cycle_xloc_3_la2_edge_type(  );
	MeasRetu_reg getla2_cycle_xloc_3_la2_cycle_xloc_3_vld(  );

	MeasRetu_reg getla2_cycle_xloc_3(  );

	MeasRetu_reg getla2_cycle_xloc_2_la2_cycle_xloc_2(  );
	MeasRetu_reg getla2_cycle_xloc_2_la2_edge_type(  );
	MeasRetu_reg getla2_cycle_xloc_2_la2_cycle_xloc_2_vld(  );

	MeasRetu_reg getla2_cycle_xloc_2(  );

	MeasRetu_reg getla2_cycle_xloc_1_la2_cycle_xloc_1(  );
	MeasRetu_reg getla2_cycle_xloc_1_la2_edge_type(  );
	MeasRetu_reg getla2_cycle_xloc_1_la2_cycle_xloc_1_vld(  );

	MeasRetu_reg getla2_cycle_xloc_1(  );

	MeasRetu_reg getla3_cycle_xloc_4_la3_cycle_xloc_4(  );
	MeasRetu_reg getla3_cycle_xloc_4_la3_edge_type(  );
	MeasRetu_reg getla3_cycle_xloc_4_la3_cycle_xloc_4_vld(  );

	MeasRetu_reg getla3_cycle_xloc_4(  );

	MeasRetu_reg getla3_cycle_xloc_3_la3_cycle_xloc_3(  );
	MeasRetu_reg getla3_cycle_xloc_3_la3_edge_type(  );
	MeasRetu_reg getla3_cycle_xloc_3_la3_cycle_xloc_3_vld(  );

	MeasRetu_reg getla3_cycle_xloc_3(  );

	MeasRetu_reg getla3_cycle_xloc_2_la3_cycle_xloc_2(  );
	MeasRetu_reg getla3_cycle_xloc_2_la3_edge_type(  );
	MeasRetu_reg getla3_cycle_xloc_2_la3_cycle_xloc_2_vld(  );

	MeasRetu_reg getla3_cycle_xloc_2(  );

	MeasRetu_reg getla3_cycle_xloc_1_la3_cycle_xloc_1(  );
	MeasRetu_reg getla3_cycle_xloc_1_la3_edge_type(  );
	MeasRetu_reg getla3_cycle_xloc_1_la3_cycle_xloc_1_vld(  );

	MeasRetu_reg getla3_cycle_xloc_1(  );

	MeasRetu_reg getla4_cycle_xloc_4_la4_cycle_xloc_4(  );
	MeasRetu_reg getla4_cycle_xloc_4_la4_edge_type(  );
	MeasRetu_reg getla4_cycle_xloc_4_la4_cycle_xloc_4_vld(  );

	MeasRetu_reg getla4_cycle_xloc_4(  );

	MeasRetu_reg getla4_cycle_xloc_3_la4_cycle_xloc_3(  );
	MeasRetu_reg getla4_cycle_xloc_3_la4_edge_type(  );
	MeasRetu_reg getla4_cycle_xloc_3_la4_cycle_xloc_3_vld(  );

	MeasRetu_reg getla4_cycle_xloc_3(  );

	MeasRetu_reg getla4_cycle_xloc_2_la4_cycle_xloc_2(  );
	MeasRetu_reg getla4_cycle_xloc_2_la4_edge_type(  );
	MeasRetu_reg getla4_cycle_xloc_2_la4_cycle_xloc_2_vld(  );

	MeasRetu_reg getla4_cycle_xloc_2(  );

	MeasRetu_reg getla4_cycle_xloc_1_la4_cycle_xloc_1(  );
	MeasRetu_reg getla4_cycle_xloc_1_la4_edge_type(  );
	MeasRetu_reg getla4_cycle_xloc_1_la4_cycle_xloc_1_vld(  );

	MeasRetu_reg getla4_cycle_xloc_1(  );

	MeasRetu_reg getla5_cycle_xloc_4_la5_cycle_xloc_4(  );
	MeasRetu_reg getla5_cycle_xloc_4_la5_edge_type(  );
	MeasRetu_reg getla5_cycle_xloc_4_la5_cycle_xloc_4_vld(  );

	MeasRetu_reg getla5_cycle_xloc_4(  );

	MeasRetu_reg getla5_cycle_xloc_3_la5_cycle_xloc_3(  );
	MeasRetu_reg getla5_cycle_xloc_3_la5_edge_type(  );
	MeasRetu_reg getla5_cycle_xloc_3_la5_cycle_xloc_3_vld(  );

	MeasRetu_reg getla5_cycle_xloc_3(  );

	MeasRetu_reg getla5_cycle_xloc_2_la5_cycle_xloc_2(  );
	MeasRetu_reg getla5_cycle_xloc_2_la5_edge_type(  );
	MeasRetu_reg getla5_cycle_xloc_2_la5_cycle_xloc_2_vld(  );

	MeasRetu_reg getla5_cycle_xloc_2(  );

	MeasRetu_reg getla5_cycle_xloc_1_la5_cycle_xloc_1(  );
	MeasRetu_reg getla5_cycle_xloc_1_la5_edge_type(  );
	MeasRetu_reg getla5_cycle_xloc_1_la5_cycle_xloc_1_vld(  );

	MeasRetu_reg getla5_cycle_xloc_1(  );

	MeasRetu_reg getla6_cycle_xloc_4_la6_cycle_xloc_4(  );
	MeasRetu_reg getla6_cycle_xloc_4_la6_edge_type(  );
	MeasRetu_reg getla6_cycle_xloc_4_la6_cycle_xloc_4_vld(  );

	MeasRetu_reg getla6_cycle_xloc_4(  );

	MeasRetu_reg getla6_cycle_xloc_3_la6_cycle_xloc_3(  );
	MeasRetu_reg getla6_cycle_xloc_3_la6_edge_type(  );
	MeasRetu_reg getla6_cycle_xloc_3_la6_cycle_xloc_3_vld(  );

	MeasRetu_reg getla6_cycle_xloc_3(  );

	MeasRetu_reg getla6_cycle_xloc_2_la6_cycle_xloc_2(  );
	MeasRetu_reg getla6_cycle_xloc_2_la6_edge_type(  );
	MeasRetu_reg getla6_cycle_xloc_2_la6_cycle_xloc_2_vld(  );

	MeasRetu_reg getla6_cycle_xloc_2(  );

	MeasRetu_reg getla6_cycle_xloc_1_la6_cycle_xloc_1(  );
	MeasRetu_reg getla6_cycle_xloc_1_la6_edge_type(  );
	MeasRetu_reg getla6_cycle_xloc_1_la6_cycle_xloc_1_vld(  );

	MeasRetu_reg getla6_cycle_xloc_1(  );

	MeasRetu_reg getla7_cycle_xloc_4_la7_cycle_xloc_4(  );
	MeasRetu_reg getla7_cycle_xloc_4_la7_edge_type(  );
	MeasRetu_reg getla7_cycle_xloc_4_la7_cycle_xloc_4_vld(  );

	MeasRetu_reg getla7_cycle_xloc_4(  );

	MeasRetu_reg getla7_cycle_xloc_3_la7_cycle_xloc_3(  );
	MeasRetu_reg getla7_cycle_xloc_3_la7_edge_type(  );
	MeasRetu_reg getla7_cycle_xloc_3_la7_cycle_xloc_3_vld(  );

	MeasRetu_reg getla7_cycle_xloc_3(  );

	MeasRetu_reg getla7_cycle_xloc_2_la7_cycle_xloc_2(  );
	MeasRetu_reg getla7_cycle_xloc_2_la7_edge_type(  );
	MeasRetu_reg getla7_cycle_xloc_2_la7_cycle_xloc_2_vld(  );

	MeasRetu_reg getla7_cycle_xloc_2(  );

	MeasRetu_reg getla7_cycle_xloc_1_la7_cycle_xloc_1(  );
	MeasRetu_reg getla7_cycle_xloc_1_la7_edge_type(  );
	MeasRetu_reg getla7_cycle_xloc_1_la7_cycle_xloc_1_vld(  );

	MeasRetu_reg getla7_cycle_xloc_1(  );

	MeasRetu_reg getla8_cycle_xloc_4_la8_cycle_xloc_4(  );
	MeasRetu_reg getla8_cycle_xloc_4_la8_edge_type(  );
	MeasRetu_reg getla8_cycle_xloc_4_la8_cycle_xloc_4_vld(  );

	MeasRetu_reg getla8_cycle_xloc_4(  );

	MeasRetu_reg getla8_cycle_xloc_3_la8_cycle_xloc_3(  );
	MeasRetu_reg getla8_cycle_xloc_3_la8_edge_type(  );
	MeasRetu_reg getla8_cycle_xloc_3_la8_cycle_xloc_3_vld(  );

	MeasRetu_reg getla8_cycle_xloc_3(  );

	MeasRetu_reg getla8_cycle_xloc_2_la8_cycle_xloc_2(  );
	MeasRetu_reg getla8_cycle_xloc_2_la8_edge_type(  );
	MeasRetu_reg getla8_cycle_xloc_2_la8_cycle_xloc_2_vld(  );

	MeasRetu_reg getla8_cycle_xloc_2(  );

	MeasRetu_reg getla8_cycle_xloc_1_la8_cycle_xloc_1(  );
	MeasRetu_reg getla8_cycle_xloc_1_la8_edge_type(  );
	MeasRetu_reg getla8_cycle_xloc_1_la8_cycle_xloc_1_vld(  );

	MeasRetu_reg getla8_cycle_xloc_1(  );

	MeasRetu_reg getla9_cycle_xloc_4_la9_cycle_xloc_4(  );
	MeasRetu_reg getla9_cycle_xloc_4_la9_edge_type(  );
	MeasRetu_reg getla9_cycle_xloc_4_la9_cycle_xloc_4_vld(  );

	MeasRetu_reg getla9_cycle_xloc_4(  );

	MeasRetu_reg getla9_cycle_xloc_3_la9_cycle_xloc_3(  );
	MeasRetu_reg getla9_cycle_xloc_3_la9_edge_type(  );
	MeasRetu_reg getla9_cycle_xloc_3_la9_cycle_xloc_3_vld(  );

	MeasRetu_reg getla9_cycle_xloc_3(  );

	MeasRetu_reg getla9_cycle_xloc_2_la9_cycle_xloc_2(  );
	MeasRetu_reg getla9_cycle_xloc_2_la9_edge_type(  );
	MeasRetu_reg getla9_cycle_xloc_2_la9_cycle_xloc_2_vld(  );

	MeasRetu_reg getla9_cycle_xloc_2(  );

	MeasRetu_reg getla9_cycle_xloc_1_la9_cycle_xloc_1(  );
	MeasRetu_reg getla9_cycle_xloc_1_la9_edge_type(  );
	MeasRetu_reg getla9_cycle_xloc_1_la9_cycle_xloc_1_vld(  );

	MeasRetu_reg getla9_cycle_xloc_1(  );

	MeasRetu_reg getla10_cycle_xloc_4_la10_cycle_xloc_4(  );
	MeasRetu_reg getla10_cycle_xloc_4_la10_edge_type(  );
	MeasRetu_reg getla10_cycle_xloc_4_la10_cycle_xloc_4_vld(  );

	MeasRetu_reg getla10_cycle_xloc_4(  );

	MeasRetu_reg getla10_cycle_xloc_3_la10_cycle_xloc_3(  );
	MeasRetu_reg getla10_cycle_xloc_3_la10_edge_type(  );
	MeasRetu_reg getla10_cycle_xloc_3_la10_cycle_xloc_3_vld(  );

	MeasRetu_reg getla10_cycle_xloc_3(  );

	MeasRetu_reg getla10_cycle_xloc_2_la10_cycle_xloc_2(  );
	MeasRetu_reg getla10_cycle_xloc_2_la10_edge_type(  );
	MeasRetu_reg getla10_cycle_xloc_2_la10_cycle_xloc_2_vld(  );

	MeasRetu_reg getla10_cycle_xloc_2(  );

	MeasRetu_reg getla10_cycle_xloc_1_la10_cycle_xloc_1(  );
	MeasRetu_reg getla10_cycle_xloc_1_la10_edge_type(  );
	MeasRetu_reg getla10_cycle_xloc_1_la10_cycle_xloc_1_vld(  );

	MeasRetu_reg getla10_cycle_xloc_1(  );

	MeasRetu_reg getla11_cycle_xloc_4_la11_cycle_xloc_4(  );
	MeasRetu_reg getla11_cycle_xloc_4_la11_edge_type(  );
	MeasRetu_reg getla11_cycle_xloc_4_la11_cycle_xloc_4_vld(  );

	MeasRetu_reg getla11_cycle_xloc_4(  );

	MeasRetu_reg getla11_cycle_xloc_3_la11_cycle_xloc_3(  );
	MeasRetu_reg getla11_cycle_xloc_3_la11_edge_type(  );
	MeasRetu_reg getla11_cycle_xloc_3_la11_cycle_xloc_3_vld(  );

	MeasRetu_reg getla11_cycle_xloc_3(  );

	MeasRetu_reg getla11_cycle_xloc_2_la11_cycle_xloc_2(  );
	MeasRetu_reg getla11_cycle_xloc_2_la11_edge_type(  );
	MeasRetu_reg getla11_cycle_xloc_2_la11_cycle_xloc_2_vld(  );

	MeasRetu_reg getla11_cycle_xloc_2(  );

	MeasRetu_reg getla11_cycle_xloc_1_la11_cycle_xloc_1(  );
	MeasRetu_reg getla11_cycle_xloc_1_la11_edge_type(  );
	MeasRetu_reg getla11_cycle_xloc_1_la11_cycle_xloc_1_vld(  );

	MeasRetu_reg getla11_cycle_xloc_1(  );

	MeasRetu_reg getla12_cycle_xloc_4_la12_cycle_xloc_4(  );
	MeasRetu_reg getla12_cycle_xloc_4_la12_edge_type(  );
	MeasRetu_reg getla12_cycle_xloc_4_la12_cycle_xloc_4_vld(  );

	MeasRetu_reg getla12_cycle_xloc_4(  );

	MeasRetu_reg getla12_cycle_xloc_3_la12_cycle_xloc_3(  );
	MeasRetu_reg getla12_cycle_xloc_3_la12_edge_type(  );
	MeasRetu_reg getla12_cycle_xloc_3_la12_cycle_xloc_3_vld(  );

	MeasRetu_reg getla12_cycle_xloc_3(  );

	MeasRetu_reg getla12_cycle_xloc_2_la12_cycle_xloc_2(  );
	MeasRetu_reg getla12_cycle_xloc_2_la12_edge_type(  );
	MeasRetu_reg getla12_cycle_xloc_2_la12_cycle_xloc_2_vld(  );

	MeasRetu_reg getla12_cycle_xloc_2(  );

	MeasRetu_reg getla12_cycle_xloc_1_la12_cycle_xloc_1(  );
	MeasRetu_reg getla12_cycle_xloc_1_la12_edge_type(  );
	MeasRetu_reg getla12_cycle_xloc_1_la12_cycle_xloc_1_vld(  );

	MeasRetu_reg getla12_cycle_xloc_1(  );

	MeasRetu_reg getla13_cycle_xloc_4_la13_cycle_xloc_4(  );
	MeasRetu_reg getla13_cycle_xloc_4_la13_edge_type(  );
	MeasRetu_reg getla13_cycle_xloc_4_la13_cycle_xloc_4_vld(  );

	MeasRetu_reg getla13_cycle_xloc_4(  );

	MeasRetu_reg getla13_cycle_xloc_3_la13_cycle_xloc_3(  );
	MeasRetu_reg getla13_cycle_xloc_3_la13_edge_type(  );
	MeasRetu_reg getla13_cycle_xloc_3_la13_cycle_xloc_3_vld(  );

	MeasRetu_reg getla13_cycle_xloc_3(  );

	MeasRetu_reg getla13_cycle_xloc_2_la13_cycle_xloc_2(  );
	MeasRetu_reg getla13_cycle_xloc_2_la13_edge_type(  );
	MeasRetu_reg getla13_cycle_xloc_2_la13_cycle_xloc_2_vld(  );

	MeasRetu_reg getla13_cycle_xloc_2(  );

	MeasRetu_reg getla13_cycle_xloc_1_la13_cycle_xloc_1(  );
	MeasRetu_reg getla13_cycle_xloc_1_la13_edge_type(  );
	MeasRetu_reg getla13_cycle_xloc_1_la13_cycle_xloc_1_vld(  );

	MeasRetu_reg getla13_cycle_xloc_1(  );

	MeasRetu_reg getla14_cycle_xloc_4_la14_cycle_xloc_4(  );
	MeasRetu_reg getla14_cycle_xloc_4_la14_edge_type(  );
	MeasRetu_reg getla14_cycle_xloc_4_la14_cycle_xloc_4_vld(  );

	MeasRetu_reg getla14_cycle_xloc_4(  );

	MeasRetu_reg getla14_cycle_xloc_3_la14_cycle_xloc_3(  );
	MeasRetu_reg getla14_cycle_xloc_3_la14_edge_type(  );
	MeasRetu_reg getla14_cycle_xloc_3_la14_cycle_xloc_3_vld(  );

	MeasRetu_reg getla14_cycle_xloc_3(  );

	MeasRetu_reg getla14_cycle_xloc_2_la14_cycle_xloc_2(  );
	MeasRetu_reg getla14_cycle_xloc_2_la14_edge_type(  );
	MeasRetu_reg getla14_cycle_xloc_2_la14_cycle_xloc_2_vld(  );

	MeasRetu_reg getla14_cycle_xloc_2(  );

	MeasRetu_reg getla14_cycle_xloc_1_la14_cycle_xloc_1(  );
	MeasRetu_reg getla14_cycle_xloc_1_la14_edge_type(  );
	MeasRetu_reg getla14_cycle_xloc_1_la14_cycle_xloc_1_vld(  );

	MeasRetu_reg getla14_cycle_xloc_1(  );

	MeasRetu_reg getla15_cycle_xloc_4_la15_cycle_xloc_4(  );
	MeasRetu_reg getla15_cycle_xloc_4_la15_edge_type(  );
	MeasRetu_reg getla15_cycle_xloc_4_la15_cycle_xloc_4_vld(  );

	MeasRetu_reg getla15_cycle_xloc_4(  );

	MeasRetu_reg getla15_cycle_xloc_3_la15_cycle_xloc_3(  );
	MeasRetu_reg getla15_cycle_xloc_3_la15_edge_type(  );
	MeasRetu_reg getla15_cycle_xloc_3_la15_cycle_xloc_3_vld(  );

	MeasRetu_reg getla15_cycle_xloc_3(  );

	MeasRetu_reg getla15_cycle_xloc_2_la15_cycle_xloc_2(  );
	MeasRetu_reg getla15_cycle_xloc_2_la15_edge_type(  );
	MeasRetu_reg getla15_cycle_xloc_2_la15_cycle_xloc_2_vld(  );

	MeasRetu_reg getla15_cycle_xloc_2(  );

	MeasRetu_reg getla15_cycle_xloc_1_la15_cycle_xloc_1(  );
	MeasRetu_reg getla15_cycle_xloc_1_la15_edge_type(  );
	MeasRetu_reg getla15_cycle_xloc_1_la15_cycle_xloc_1_vld(  );

	MeasRetu_reg getla15_cycle_xloc_1(  );

	MeasRetu_reg getdlyresult1_from_xloc2_dlyresult1_from_xloc2(  );
	MeasRetu_reg getdlyresult1_from_xloc2_dlyresult1_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult1_from_xloc2(  );

	MeasRetu_reg getdlyresult1_from_xloc1_dlyresult1_from_xloc1(  );
	MeasRetu_reg getdlyresult1_from_xloc1_dlyresult1_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult1_from_xloc1(  );

	MeasRetu_reg getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult1_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult1_to_xloc2_dlyresult1_to_xloc2(  );
	MeasRetu_reg getdlyresult1_to_xloc2_dlyresult1_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult1_to_xloc2(  );

	MeasRetu_reg getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult1_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult1_to_xloc1_dlyresult1_to_xloc1(  );
	MeasRetu_reg getdlyresult1_to_xloc1_dlyresult1_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult1_to_xloc1(  );

	MeasRetu_reg getdlyresult2_from_xloc2_dlyresult2_from_xloc2(  );
	MeasRetu_reg getdlyresult2_from_xloc2_dlyresult2_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult2_from_xloc2(  );

	MeasRetu_reg getdlyresult2_from_xloc1_dlyresult2_from_xloc1(  );
	MeasRetu_reg getdlyresult2_from_xloc1_dlyresult2_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult2_from_xloc1(  );

	MeasRetu_reg getdlyresult2_to_xloc2_pre_dlyresult2_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult2_to_xloc2_pre_dlyresult2_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult2_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult2_to_xloc2_dlyresult2_to_xloc2(  );
	MeasRetu_reg getdlyresult2_to_xloc2_dlyresult2_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult2_to_xloc2(  );

	MeasRetu_reg getdlyresult2_to_xloc1_pre_dlyresult2_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult2_to_xloc1_pre_dlyresult2_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult2_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult2_to_xloc1_dlyresult2_to_xloc1(  );
	MeasRetu_reg getdlyresult2_to_xloc1_dlyresult2_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult2_to_xloc1(  );

	MeasRetu_reg getdlyresult3_from_xloc2_dlyresult3_from_xloc2(  );
	MeasRetu_reg getdlyresult3_from_xloc2_dlyresult3_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult3_from_xloc2(  );

	MeasRetu_reg getdlyresult3_from_xloc1_dlyresult3_from_xloc1(  );
	MeasRetu_reg getdlyresult3_from_xloc1_dlyresult3_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult3_from_xloc1(  );

	MeasRetu_reg getdlyresult3_to_xloc2_pre_dlyresult3_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult3_to_xloc2_pre_dlyresult3_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult3_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult3_to_xloc2_dlyresult3_to_xloc2(  );
	MeasRetu_reg getdlyresult3_to_xloc2_dlyresult3_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult3_to_xloc2(  );

	MeasRetu_reg getdlyresult3_to_xloc1_pre_dlyresult3_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult3_to_xloc1_pre_dlyresult3_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult3_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult3_to_xloc1_dlyresult3_to_xloc1(  );
	MeasRetu_reg getdlyresult3_to_xloc1_dlyresult3_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult3_to_xloc1(  );

	MeasRetu_reg getdlyresult4_from_xloc2_dlyresult4_from_xloc2(  );
	MeasRetu_reg getdlyresult4_from_xloc2_dlyresult4_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult4_from_xloc2(  );

	MeasRetu_reg getdlyresult4_from_xloc1_dlyresult4_from_xloc1(  );
	MeasRetu_reg getdlyresult4_from_xloc1_dlyresult4_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult4_from_xloc1(  );

	MeasRetu_reg getdlyresult4_to_xloc2_pre_dlyresult4_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult4_to_xloc2_pre_dlyresult4_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult4_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult4_to_xloc2_dlyresult4_to_xloc2(  );
	MeasRetu_reg getdlyresult4_to_xloc2_dlyresult4_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult4_to_xloc2(  );

	MeasRetu_reg getdlyresult4_to_xloc1_pre_dlyresult4_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult4_to_xloc1_pre_dlyresult4_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult4_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult4_to_xloc1_dlyresult4_to_xloc1(  );
	MeasRetu_reg getdlyresult4_to_xloc1_dlyresult4_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult4_to_xloc1(  );

	MeasRetu_reg getdlyresult5_from_xloc2_dlyresult5_from_xloc2(  );
	MeasRetu_reg getdlyresult5_from_xloc2_dlyresult5_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult5_from_xloc2(  );

	MeasRetu_reg getdlyresult5_from_xloc1_dlyresult5_from_xloc1(  );
	MeasRetu_reg getdlyresult5_from_xloc1_dlyresult5_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult5_from_xloc1(  );

	MeasRetu_reg getdlyresult5_to_xloc2_pre_dlyresult5_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult5_to_xloc2_pre_dlyresult5_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult5_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult5_to_xloc2_dlyresult5_to_xloc2(  );
	MeasRetu_reg getdlyresult5_to_xloc2_dlyresult5_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult5_to_xloc2(  );

	MeasRetu_reg getdlyresult5_to_xloc1_pre_dlyresult5_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult5_to_xloc1_pre_dlyresult5_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult5_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult5_to_xloc1_dlyresult5_to_xloc1(  );
	MeasRetu_reg getdlyresult5_to_xloc1_dlyresult5_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult5_to_xloc1(  );

	MeasRetu_reg getdlyresult6_from_xloc2_dlyresult6_from_xloc2(  );
	MeasRetu_reg getdlyresult6_from_xloc2_dlyresult6_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult6_from_xloc2(  );

	MeasRetu_reg getdlyresult6_from_xloc1_dlyresult6_from_xloc1(  );
	MeasRetu_reg getdlyresult6_from_xloc1_dlyresult6_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult6_from_xloc1(  );

	MeasRetu_reg getdlyresult6_to_xloc2_pre_dlyresult6_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult6_to_xloc2_pre_dlyresult6_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult6_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult6_to_xloc2_dlyresult6_to_xloc2(  );
	MeasRetu_reg getdlyresult6_to_xloc2_dlyresult6_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult6_to_xloc2(  );

	MeasRetu_reg getdlyresult6_to_xloc1_pre_dlyresult6_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult6_to_xloc1_pre_dlyresult6_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult6_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult6_to_xloc1_dlyresult6_to_xloc1(  );
	MeasRetu_reg getdlyresult6_to_xloc1_dlyresult6_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult6_to_xloc1(  );

	MeasRetu_reg getdlyresult7_from_xloc2_dlyresult7_from_xloc2(  );
	MeasRetu_reg getdlyresult7_from_xloc2_dlyresult7_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult7_from_xloc2(  );

	MeasRetu_reg getdlyresult7_from_xloc1_dlyresult7_from_xloc1(  );
	MeasRetu_reg getdlyresult7_from_xloc1_dlyresult7_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult7_from_xloc1(  );

	MeasRetu_reg getdlyresult7_to_xloc2_pre_dlyresult7_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult7_to_xloc2_pre_dlyresult7_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult7_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult7_to_xloc2_dlyresult7_to_xloc2(  );
	MeasRetu_reg getdlyresult7_to_xloc2_dlyresult7_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult7_to_xloc2(  );

	MeasRetu_reg getdlyresult7_to_xloc1_pre_dlyresult7_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult7_to_xloc1_pre_dlyresult7_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult7_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult7_to_xloc1_dlyresult7_to_xloc1(  );
	MeasRetu_reg getdlyresult7_to_xloc1_dlyresult7_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult7_to_xloc1(  );

	MeasRetu_reg getdlyresult8_from_xloc2_dlyresult8_from_xloc2(  );
	MeasRetu_reg getdlyresult8_from_xloc2_dlyresult8_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult8_from_xloc2(  );

	MeasRetu_reg getdlyresult8_from_xloc1_dlyresult8_from_xloc1(  );
	MeasRetu_reg getdlyresult8_from_xloc1_dlyresult8_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult8_from_xloc1(  );

	MeasRetu_reg getdlyresult8_to_xloc2_pre_dlyresult8_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult8_to_xloc2_pre_dlyresult8_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult8_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult8_to_xloc2_dlyresult8_to_xloc2(  );
	MeasRetu_reg getdlyresult8_to_xloc2_dlyresult8_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult8_to_xloc2(  );

	MeasRetu_reg getdlyresult8_to_xloc1_pre_dlyresult8_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult8_to_xloc1_pre_dlyresult8_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult8_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult8_to_xloc1_dlyresult8_to_xloc1(  );
	MeasRetu_reg getdlyresult8_to_xloc1_dlyresult8_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult8_to_xloc1(  );

	MeasRetu_reg getdlyresult9_from_xloc2_dlyresult9_from_xloc2(  );
	MeasRetu_reg getdlyresult9_from_xloc2_dlyresult9_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult9_from_xloc2(  );

	MeasRetu_reg getdlyresult9_from_xloc1_dlyresult9_from_xloc1(  );
	MeasRetu_reg getdlyresult9_from_xloc1_dlyresult9_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult9_from_xloc1(  );

	MeasRetu_reg getdlyresult9_to_xloc2_pre_dlyresult9_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult9_to_xloc2_pre_dlyresult9_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult9_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult9_to_xloc2_dlyresult9_to_xloc2(  );
	MeasRetu_reg getdlyresult9_to_xloc2_dlyresult9_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult9_to_xloc2(  );

	MeasRetu_reg getdlyresult9_to_xloc1_pre_dlyresult9_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult9_to_xloc1_pre_dlyresult9_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult9_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult9_to_xloc1_dlyresult9_to_xloc1(  );
	MeasRetu_reg getdlyresult9_to_xloc1_dlyresult9_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult9_to_xloc1(  );

	MeasRetu_reg getdlyresult10_from_xloc2_dlyresult10_from_xloc2(  );
	MeasRetu_reg getdlyresult10_from_xloc2_dlyresult10_from_xloc2_vld(  );

	MeasRetu_reg getdlyresult10_from_xloc2(  );

	MeasRetu_reg getdlyresult10_from_xloc1_dlyresult10_from_xloc1(  );
	MeasRetu_reg getdlyresult10_from_xloc1_dlyresult10_from_xloc1_vld(  );

	MeasRetu_reg getdlyresult10_from_xloc1(  );

	MeasRetu_reg getdlyresult10_to_xloc2_pre_dlyresult10_to_xloc2_pre(  );
	MeasRetu_reg getdlyresult10_to_xloc2_pre_dlyresult10_to_xloc2_pre_vld(  );

	MeasRetu_reg getdlyresult10_to_xloc2_pre(  );

	MeasRetu_reg getdlyresult10_to_xloc2_dlyresult10_to_xloc2(  );
	MeasRetu_reg getdlyresult10_to_xloc2_dlyresult10_to_xloc2_vld(  );

	MeasRetu_reg getdlyresult10_to_xloc2(  );

	MeasRetu_reg getdlyresult10_to_xloc1_pre_dlyresult10_to_xloc1_pre(  );
	MeasRetu_reg getdlyresult10_to_xloc1_pre_dlyresult10_to_xloc1_pre_vld(  );

	MeasRetu_reg getdlyresult10_to_xloc1_pre(  );

	MeasRetu_reg getdlyresult10_to_xloc1_dlyresult10_to_xloc1(  );
	MeasRetu_reg getdlyresult10_to_xloc1_dlyresult10_to_xloc1_vld(  );

	MeasRetu_reg getdlyresult10_to_xloc1(  );

};
struct MeasRetuMem : public MeasRetuMemProxy, public IPhyFpga{
protected:
	MeasRetu_reg mAddrBase;
public:
	MeasRetuMem( MeasRetu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( MeasRetu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( MeasRetuMemProxy *proxy);
	void pull( MeasRetuMemProxy *proxy);

public:
	MeasRetu_reg incha_max_min_top_base(  );

	MeasRetu_reg incha_threshold_l_mid_h(  );

	MeasRetu_reg incha_vmax_xloc(  );

	MeasRetu_reg incha_vmin_xloc(  );

	MeasRetu_reg incha_rtime(  );

	MeasRetu_reg incha_rtime_xloc_end(  );

	MeasRetu_reg incha_rtime_pre(  );

	MeasRetu_reg incha_rtime_xloc_end_pre(  );

	MeasRetu_reg incha_ftime(  );

	MeasRetu_reg incha_ftime_xloc_end(  );

	MeasRetu_reg incha_ftime_pre(  );

	MeasRetu_reg incha_ftime_xloc_end_pre(  );

	MeasRetu_reg incha_cycle_xloc_4(  );

	MeasRetu_reg incha_cycle_xloc_3(  );

	MeasRetu_reg incha_cycle_xloc_2(  );

	MeasRetu_reg incha_cycle_xloc_1(  );

	MeasRetu_reg incha_ave_sum_n_bits31_0(  );

	MeasRetu_reg incha_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg incha_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg incha_ave_sum_x_bit39_32(  );

	MeasRetu_reg incha_vrms_sum_n_bits31_0(  );

	MeasRetu_reg incha_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg incha_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg incha_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg incha_vrms_sum_2_bits47_32(  );

	MeasRetu_reg incha_total_rf_num(  );

	MeasRetu_reg inreserved_x1(  );

	MeasRetu_reg incha_shoot(  );

	MeasRetu_reg incha_edge_cnt_start_xloc(  );

	MeasRetu_reg incha_edge_cnt_end_xloc(  );

	MeasRetu_reg incha_edge_cnt_start2_xloc(  );

	MeasRetu_reg incha_edge_cnt_end2_xloc(  );

	MeasRetu_reg inchb_max_min_top_base(  );

	MeasRetu_reg inchb_threshold_l_mid_h(  );

	MeasRetu_reg inchb_vmax_xloc(  );

	MeasRetu_reg inchb_vmin_xloc(  );

	MeasRetu_reg inchb_rtime(  );

	MeasRetu_reg inchb_rtime_xloc_end(  );

	MeasRetu_reg inchb_rtime_pre(  );

	MeasRetu_reg inchb_rtime_xloc_end_pre(  );

	MeasRetu_reg inchb_ftime(  );

	MeasRetu_reg inchb_ftime_xloc_end(  );

	MeasRetu_reg inchb_ftime_pre(  );

	MeasRetu_reg inchb_ftime_xloc_end_pre(  );

	MeasRetu_reg inchb_cycle_xloc_4(  );

	MeasRetu_reg inchb_cycle_xloc_3(  );

	MeasRetu_reg inchb_cycle_xloc_2(  );

	MeasRetu_reg inchb_cycle_xloc_1(  );

	MeasRetu_reg inchb_ave_sum_n_bits31_0(  );

	MeasRetu_reg inchb_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg inchb_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg inchb_ave_sum_x_bit39_32(  );

	MeasRetu_reg inchb_vrms_sum_n_bits31_0(  );

	MeasRetu_reg inchb_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg inchb_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg inchb_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg inchb_vrms_sum_2_bits47_32(  );

	MeasRetu_reg inchb_total_rf_num(  );

	MeasRetu_reg inreserved_x2(  );

	MeasRetu_reg inchb_shoot(  );

	MeasRetu_reg inchb_edge_cnt_start_xloc(  );

	MeasRetu_reg inchb_edge_cnt_end_xloc(  );

	MeasRetu_reg inchb_edge_cnt_start2_xloc(  );

	MeasRetu_reg inchb_edge_cnt_end2_xloc(  );

	MeasRetu_reg inchc_max_min_top_base(  );

	MeasRetu_reg inchc_threshold_l_mid_h(  );

	MeasRetu_reg inchc_vmax_xloc(  );

	MeasRetu_reg inchc_vmin_xloc(  );

	MeasRetu_reg inchc_rtime(  );

	MeasRetu_reg inchc_rtime_xloc_end(  );

	MeasRetu_reg inchc_rtime_pre(  );

	MeasRetu_reg inchc_rtime_xloc_end_pre(  );

	MeasRetu_reg inchc_ftime(  );

	MeasRetu_reg inchc_ftime_xloc_end(  );

	MeasRetu_reg inchc_ftime_pre(  );

	MeasRetu_reg inchc_ftime_xloc_end_pre(  );

	MeasRetu_reg inchc_cycle_xloc_4(  );

	MeasRetu_reg inchc_cycle_xloc_3(  );

	MeasRetu_reg inchc_cycle_xloc_2(  );

	MeasRetu_reg inchc_cycle_xloc_1(  );

	MeasRetu_reg inchc_ave_sum_n_bits31_0(  );

	MeasRetu_reg inchc_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg inchc_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg inchc_ave_sum_x_bit39_32(  );

	MeasRetu_reg inchc_vrms_sum_n_bits31_0(  );

	MeasRetu_reg inchc_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg inchc_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg inchc_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg inchc_vrms_sum_2_bits47_32(  );

	MeasRetu_reg inchc_total_rf_num(  );

	MeasRetu_reg inreserved_x3(  );

	MeasRetu_reg inchc_shoot(  );

	MeasRetu_reg inchc_edge_cnt_start_xloc(  );

	MeasRetu_reg inchc_edge_cnt_end_xloc(  );

	MeasRetu_reg inchc_edge_cnt_start2_xloc(  );

	MeasRetu_reg inchc_edge_cnt_end2_xloc(  );

	MeasRetu_reg inchd_max_min_top_base(  );

	MeasRetu_reg inchd_threshold_l_mid_h(  );

	MeasRetu_reg inchd_vmax_xloc(  );

	MeasRetu_reg inchd_vmin_xloc(  );

	MeasRetu_reg inchd_rtime(  );

	MeasRetu_reg inchd_rtime_xloc_end(  );

	MeasRetu_reg inchd_rtime_pre(  );

	MeasRetu_reg inchd_rtime_xloc_end_pre(  );

	MeasRetu_reg inchd_ftime(  );

	MeasRetu_reg inchd_ftime_xloc_end(  );

	MeasRetu_reg inchd_ftime_pre(  );

	MeasRetu_reg inchd_ftime_xloc_end_pre(  );

	MeasRetu_reg inchd_cycle_xloc_4(  );

	MeasRetu_reg inchd_cycle_xloc_3(  );

	MeasRetu_reg inchd_cycle_xloc_2(  );

	MeasRetu_reg inchd_cycle_xloc_1(  );

	MeasRetu_reg inchd_ave_sum_n_bits31_0(  );

	MeasRetu_reg inchd_ave_sum_1_1_bits31_0(  );

	MeasRetu_reg inchd_ave_sum_1_2_bits31_0(  );

	MeasRetu_reg inchd_ave_sum_x_bit39_32(  );

	MeasRetu_reg inchd_vrms_sum_n_bits31_0(  );

	MeasRetu_reg inchd_vrms_sum_1_1_bits31_0(  );

	MeasRetu_reg inchd_vrms_sum_1_2_bits31_0(  );

	MeasRetu_reg inchd_vrms_sum_n_1_bits47_32(  );

	MeasRetu_reg inchd_vrms_sum_2_bits47_32(  );

	MeasRetu_reg inchd_total_rf_num(  );

	MeasRetu_reg inreservd_x4(  );

	MeasRetu_reg inchd_shoot(  );

	MeasRetu_reg inchd_edge_cnt_start_xloc(  );

	MeasRetu_reg inchd_edge_cnt_end_xloc(  );

	MeasRetu_reg inchd_edge_cnt_start2_xloc(  );

	MeasRetu_reg inchd_edge_cnt_end2_xloc(  );

	MeasRetu_reg inmeas_version_rd(  );

	MeasRetu_reg inmeas_result_done(  );

	MeasRetu_reg inla0_cycle_xloc_4(  );

	MeasRetu_reg inla0_cycle_xloc_3(  );

	MeasRetu_reg inla0_cycle_xloc_2(  );

	MeasRetu_reg inla0_cycle_xloc_1(  );

	MeasRetu_reg inla1_cycle_xloc_4(  );

	MeasRetu_reg inla1_cycle_xloc_3(  );

	MeasRetu_reg inla1_cycle_xloc_2(  );

	MeasRetu_reg inla1_cycle_xloc_1(  );

	MeasRetu_reg inla2_cycle_xloc_4(  );

	MeasRetu_reg inla2_cycle_xloc_3(  );

	MeasRetu_reg inla2_cycle_xloc_2(  );

	MeasRetu_reg inla2_cycle_xloc_1(  );

	MeasRetu_reg inla3_cycle_xloc_4(  );

	MeasRetu_reg inla3_cycle_xloc_3(  );

	MeasRetu_reg inla3_cycle_xloc_2(  );

	MeasRetu_reg inla3_cycle_xloc_1(  );

	MeasRetu_reg inla4_cycle_xloc_4(  );

	MeasRetu_reg inla4_cycle_xloc_3(  );

	MeasRetu_reg inla4_cycle_xloc_2(  );

	MeasRetu_reg inla4_cycle_xloc_1(  );

	MeasRetu_reg inla5_cycle_xloc_4(  );

	MeasRetu_reg inla5_cycle_xloc_3(  );

	MeasRetu_reg inla5_cycle_xloc_2(  );

	MeasRetu_reg inla5_cycle_xloc_1(  );

	MeasRetu_reg inla6_cycle_xloc_4(  );

	MeasRetu_reg inla6_cycle_xloc_3(  );

	MeasRetu_reg inla6_cycle_xloc_2(  );

	MeasRetu_reg inla6_cycle_xloc_1(  );

	MeasRetu_reg inla7_cycle_xloc_4(  );

	MeasRetu_reg inla7_cycle_xloc_3(  );

	MeasRetu_reg inla7_cycle_xloc_2(  );

	MeasRetu_reg inla7_cycle_xloc_1(  );

	MeasRetu_reg inla8_cycle_xloc_4(  );

	MeasRetu_reg inla8_cycle_xloc_3(  );

	MeasRetu_reg inla8_cycle_xloc_2(  );

	MeasRetu_reg inla8_cycle_xloc_1(  );

	MeasRetu_reg inla9_cycle_xloc_4(  );

	MeasRetu_reg inla9_cycle_xloc_3(  );

	MeasRetu_reg inla9_cycle_xloc_2(  );

	MeasRetu_reg inla9_cycle_xloc_1(  );

	MeasRetu_reg inla10_cycle_xloc_4(  );

	MeasRetu_reg inla10_cycle_xloc_3(  );

	MeasRetu_reg inla10_cycle_xloc_2(  );

	MeasRetu_reg inla10_cycle_xloc_1(  );

	MeasRetu_reg inla11_cycle_xloc_4(  );

	MeasRetu_reg inla11_cycle_xloc_3(  );

	MeasRetu_reg inla11_cycle_xloc_2(  );

	MeasRetu_reg inla11_cycle_xloc_1(  );

	MeasRetu_reg inla12_cycle_xloc_4(  );

	MeasRetu_reg inla12_cycle_xloc_3(  );

	MeasRetu_reg inla12_cycle_xloc_2(  );

	MeasRetu_reg inla12_cycle_xloc_1(  );

	MeasRetu_reg inla13_cycle_xloc_4(  );

	MeasRetu_reg inla13_cycle_xloc_3(  );

	MeasRetu_reg inla13_cycle_xloc_2(  );

	MeasRetu_reg inla13_cycle_xloc_1(  );

	MeasRetu_reg inla14_cycle_xloc_4(  );

	MeasRetu_reg inla14_cycle_xloc_3(  );

	MeasRetu_reg inla14_cycle_xloc_2(  );

	MeasRetu_reg inla14_cycle_xloc_1(  );

	MeasRetu_reg inla15_cycle_xloc_4(  );

	MeasRetu_reg inla15_cycle_xloc_3(  );

	MeasRetu_reg inla15_cycle_xloc_2(  );

	MeasRetu_reg inla15_cycle_xloc_1(  );

	MeasRetu_reg indlyresult1_from_xloc2(  );

	MeasRetu_reg indlyresult1_from_xloc1(  );

	MeasRetu_reg indlyresult1_to_xloc2_pre(  );

	MeasRetu_reg indlyresult1_to_xloc2(  );

	MeasRetu_reg indlyresult1_to_xloc1_pre(  );

	MeasRetu_reg indlyresult1_to_xloc1(  );

	MeasRetu_reg indlyresult2_from_xloc2(  );

	MeasRetu_reg indlyresult2_from_xloc1(  );

	MeasRetu_reg indlyresult2_to_xloc2_pre(  );

	MeasRetu_reg indlyresult2_to_xloc2(  );

	MeasRetu_reg indlyresult2_to_xloc1_pre(  );

	MeasRetu_reg indlyresult2_to_xloc1(  );

	MeasRetu_reg indlyresult3_from_xloc2(  );

	MeasRetu_reg indlyresult3_from_xloc1(  );

	MeasRetu_reg indlyresult3_to_xloc2_pre(  );

	MeasRetu_reg indlyresult3_to_xloc2(  );

	MeasRetu_reg indlyresult3_to_xloc1_pre(  );

	MeasRetu_reg indlyresult3_to_xloc1(  );

	MeasRetu_reg indlyresult4_from_xloc2(  );

	MeasRetu_reg indlyresult4_from_xloc1(  );

	MeasRetu_reg indlyresult4_to_xloc2_pre(  );

	MeasRetu_reg indlyresult4_to_xloc2(  );

	MeasRetu_reg indlyresult4_to_xloc1_pre(  );

	MeasRetu_reg indlyresult4_to_xloc1(  );

	MeasRetu_reg indlyresult5_from_xloc2(  );

	MeasRetu_reg indlyresult5_from_xloc1(  );

	MeasRetu_reg indlyresult5_to_xloc2_pre(  );

	MeasRetu_reg indlyresult5_to_xloc2(  );

	MeasRetu_reg indlyresult5_to_xloc1_pre(  );

	MeasRetu_reg indlyresult5_to_xloc1(  );

	MeasRetu_reg indlyresult6_from_xloc2(  );

	MeasRetu_reg indlyresult6_from_xloc1(  );

	MeasRetu_reg indlyresult6_to_xloc2_pre(  );

	MeasRetu_reg indlyresult6_to_xloc2(  );

	MeasRetu_reg indlyresult6_to_xloc1_pre(  );

	MeasRetu_reg indlyresult6_to_xloc1(  );

	MeasRetu_reg indlyresult7_from_xloc2(  );

	MeasRetu_reg indlyresult7_from_xloc1(  );

	MeasRetu_reg indlyresult7_to_xloc2_pre(  );

	MeasRetu_reg indlyresult7_to_xloc2(  );

	MeasRetu_reg indlyresult7_to_xloc1_pre(  );

	MeasRetu_reg indlyresult7_to_xloc1(  );

	MeasRetu_reg indlyresult8_from_xloc2(  );

	MeasRetu_reg indlyresult8_from_xloc1(  );

	MeasRetu_reg indlyresult8_to_xloc2_pre(  );

	MeasRetu_reg indlyresult8_to_xloc2(  );

	MeasRetu_reg indlyresult8_to_xloc1_pre(  );

	MeasRetu_reg indlyresult8_to_xloc1(  );

	MeasRetu_reg indlyresult9_from_xloc2(  );

	MeasRetu_reg indlyresult9_from_xloc1(  );

	MeasRetu_reg indlyresult9_to_xloc2_pre(  );

	MeasRetu_reg indlyresult9_to_xloc2(  );

	MeasRetu_reg indlyresult9_to_xloc1_pre(  );

	MeasRetu_reg indlyresult9_to_xloc1(  );

	MeasRetu_reg indlyresult10_from_xloc2(  );

	MeasRetu_reg indlyresult10_from_xloc1(  );

	MeasRetu_reg indlyresult10_to_xloc2_pre(  );

	MeasRetu_reg indlyresult10_to_xloc2(  );

	MeasRetu_reg indlyresult10_to_xloc1_pre(  );

	MeasRetu_reg indlyresult10_to_xloc1(  );

public:
	cache_addr addr_cha_max_min_top_base();
	cache_addr addr_cha_threshold_l_mid_h();
	cache_addr addr_cha_vmax_xloc();
	cache_addr addr_cha_vmin_xloc();

	cache_addr addr_cha_rtime();
	cache_addr addr_cha_rtime_xloc_end();
	cache_addr addr_cha_rtime_pre();
	cache_addr addr_cha_rtime_xloc_end_pre();

	cache_addr addr_cha_ftime();
	cache_addr addr_cha_ftime_xloc_end();
	cache_addr addr_cha_ftime_pre();
	cache_addr addr_cha_ftime_xloc_end_pre();

	cache_addr addr_cha_cycle_xloc_4();
	cache_addr addr_cha_cycle_xloc_3();
	cache_addr addr_cha_cycle_xloc_2();
	cache_addr addr_cha_cycle_xloc_1();

	cache_addr addr_cha_ave_sum_n_bits31_0();
	cache_addr addr_cha_ave_sum_1_1_bits31_0();
	cache_addr addr_cha_ave_sum_1_2_bits31_0();
	cache_addr addr_cha_ave_sum_x_bit39_32();

	cache_addr addr_cha_vrms_sum_n_bits31_0();
	cache_addr addr_cha_vrms_sum_1_1_bits31_0();
	cache_addr addr_cha_vrms_sum_1_2_bits31_0();
	cache_addr addr_cha_vrms_sum_n_1_bits47_32();

	cache_addr addr_cha_vrms_sum_2_bits47_32();
	cache_addr addr_cha_total_rf_num();
	cache_addr addr_reserved_x1();
	cache_addr addr_cha_shoot();

	cache_addr addr_cha_edge_cnt_start_xloc();
	cache_addr addr_cha_edge_cnt_end_xloc();
	cache_addr addr_cha_edge_cnt_start2_xloc();
	cache_addr addr_cha_edge_cnt_end2_xloc();

	cache_addr addr_chb_max_min_top_base();
	cache_addr addr_chb_threshold_l_mid_h();
	cache_addr addr_chb_vmax_xloc();
	cache_addr addr_chb_vmin_xloc();

	cache_addr addr_chb_rtime();
	cache_addr addr_chb_rtime_xloc_end();
	cache_addr addr_chb_rtime_pre();
	cache_addr addr_chb_rtime_xloc_end_pre();

	cache_addr addr_chb_ftime();
	cache_addr addr_chb_ftime_xloc_end();
	cache_addr addr_chb_ftime_pre();
	cache_addr addr_chb_ftime_xloc_end_pre();

	cache_addr addr_chb_cycle_xloc_4();
	cache_addr addr_chb_cycle_xloc_3();
	cache_addr addr_chb_cycle_xloc_2();
	cache_addr addr_chb_cycle_xloc_1();

	cache_addr addr_chb_ave_sum_n_bits31_0();
	cache_addr addr_chb_ave_sum_1_1_bits31_0();
	cache_addr addr_chb_ave_sum_1_2_bits31_0();
	cache_addr addr_chb_ave_sum_x_bit39_32();

	cache_addr addr_chb_vrms_sum_n_bits31_0();
	cache_addr addr_chb_vrms_sum_1_1_bits31_0();
	cache_addr addr_chb_vrms_sum_1_2_bits31_0();
	cache_addr addr_chb_vrms_sum_n_1_bits47_32();

	cache_addr addr_chb_vrms_sum_2_bits47_32();
	cache_addr addr_chb_total_rf_num();
	cache_addr addr_reserved_x2();
	cache_addr addr_chb_shoot();

	cache_addr addr_chb_edge_cnt_start_xloc();
	cache_addr addr_chb_edge_cnt_end_xloc();
	cache_addr addr_chb_edge_cnt_start2_xloc();
	cache_addr addr_chb_edge_cnt_end2_xloc();

	cache_addr addr_chc_max_min_top_base();
	cache_addr addr_chc_threshold_l_mid_h();
	cache_addr addr_chc_vmax_xloc();
	cache_addr addr_chc_vmin_xloc();

	cache_addr addr_chc_rtime();
	cache_addr addr_chc_rtime_xloc_end();
	cache_addr addr_chc_rtime_pre();
	cache_addr addr_chc_rtime_xloc_end_pre();

	cache_addr addr_chc_ftime();
	cache_addr addr_chc_ftime_xloc_end();
	cache_addr addr_chc_ftime_pre();
	cache_addr addr_chc_ftime_xloc_end_pre();

	cache_addr addr_chc_cycle_xloc_4();
	cache_addr addr_chc_cycle_xloc_3();
	cache_addr addr_chc_cycle_xloc_2();
	cache_addr addr_chc_cycle_xloc_1();

	cache_addr addr_chc_ave_sum_n_bits31_0();
	cache_addr addr_chc_ave_sum_1_1_bits31_0();
	cache_addr addr_chc_ave_sum_1_2_bits31_0();
	cache_addr addr_chc_ave_sum_x_bit39_32();

	cache_addr addr_chc_vrms_sum_n_bits31_0();
	cache_addr addr_chc_vrms_sum_1_1_bits31_0();
	cache_addr addr_chc_vrms_sum_1_2_bits31_0();
	cache_addr addr_chc_vrms_sum_n_1_bits47_32();

	cache_addr addr_chc_vrms_sum_2_bits47_32();
	cache_addr addr_chc_total_rf_num();
	cache_addr addr_reserved_x3();
	cache_addr addr_chc_shoot();

	cache_addr addr_chc_edge_cnt_start_xloc();
	cache_addr addr_chc_edge_cnt_end_xloc();
	cache_addr addr_chc_edge_cnt_start2_xloc();
	cache_addr addr_chc_edge_cnt_end2_xloc();

	cache_addr addr_chd_max_min_top_base();
	cache_addr addr_chd_threshold_l_mid_h();
	cache_addr addr_chd_vmax_xloc();
	cache_addr addr_chd_vmin_xloc();

	cache_addr addr_chd_rtime();
	cache_addr addr_chd_rtime_xloc_end();
	cache_addr addr_chd_rtime_pre();
	cache_addr addr_chd_rtime_xloc_end_pre();

	cache_addr addr_chd_ftime();
	cache_addr addr_chd_ftime_xloc_end();
	cache_addr addr_chd_ftime_pre();
	cache_addr addr_chd_ftime_xloc_end_pre();

	cache_addr addr_chd_cycle_xloc_4();
	cache_addr addr_chd_cycle_xloc_3();
	cache_addr addr_chd_cycle_xloc_2();
	cache_addr addr_chd_cycle_xloc_1();

	cache_addr addr_chd_ave_sum_n_bits31_0();
	cache_addr addr_chd_ave_sum_1_1_bits31_0();
	cache_addr addr_chd_ave_sum_1_2_bits31_0();
	cache_addr addr_chd_ave_sum_x_bit39_32();

	cache_addr addr_chd_vrms_sum_n_bits31_0();
	cache_addr addr_chd_vrms_sum_1_1_bits31_0();
	cache_addr addr_chd_vrms_sum_1_2_bits31_0();
	cache_addr addr_chd_vrms_sum_n_1_bits47_32();

	cache_addr addr_chd_vrms_sum_2_bits47_32();
	cache_addr addr_chd_total_rf_num();
	cache_addr addr_reservd_x4();
	cache_addr addr_chd_shoot();

	cache_addr addr_chd_edge_cnt_start_xloc();
	cache_addr addr_chd_edge_cnt_end_xloc();
	cache_addr addr_chd_edge_cnt_start2_xloc();
	cache_addr addr_chd_edge_cnt_end2_xloc();

	cache_addr addr_meas_version_rd();
	cache_addr addr_meas_result_done();
	cache_addr addr_la0_cycle_xloc_4();
	cache_addr addr_la0_cycle_xloc_3();

	cache_addr addr_la0_cycle_xloc_2();
	cache_addr addr_la0_cycle_xloc_1();
	cache_addr addr_la1_cycle_xloc_4();
	cache_addr addr_la1_cycle_xloc_3();

	cache_addr addr_la1_cycle_xloc_2();
	cache_addr addr_la1_cycle_xloc_1();
	cache_addr addr_la2_cycle_xloc_4();
	cache_addr addr_la2_cycle_xloc_3();

	cache_addr addr_la2_cycle_xloc_2();
	cache_addr addr_la2_cycle_xloc_1();
	cache_addr addr_la3_cycle_xloc_4();
	cache_addr addr_la3_cycle_xloc_3();

	cache_addr addr_la3_cycle_xloc_2();
	cache_addr addr_la3_cycle_xloc_1();
	cache_addr addr_la4_cycle_xloc_4();
	cache_addr addr_la4_cycle_xloc_3();

	cache_addr addr_la4_cycle_xloc_2();
	cache_addr addr_la4_cycle_xloc_1();
	cache_addr addr_la5_cycle_xloc_4();
	cache_addr addr_la5_cycle_xloc_3();

	cache_addr addr_la5_cycle_xloc_2();
	cache_addr addr_la5_cycle_xloc_1();
	cache_addr addr_la6_cycle_xloc_4();
	cache_addr addr_la6_cycle_xloc_3();

	cache_addr addr_la6_cycle_xloc_2();
	cache_addr addr_la6_cycle_xloc_1();
	cache_addr addr_la7_cycle_xloc_4();
	cache_addr addr_la7_cycle_xloc_3();

	cache_addr addr_la7_cycle_xloc_2();
	cache_addr addr_la7_cycle_xloc_1();
	cache_addr addr_la8_cycle_xloc_4();
	cache_addr addr_la8_cycle_xloc_3();

	cache_addr addr_la8_cycle_xloc_2();
	cache_addr addr_la8_cycle_xloc_1();
	cache_addr addr_la9_cycle_xloc_4();
	cache_addr addr_la9_cycle_xloc_3();

	cache_addr addr_la9_cycle_xloc_2();
	cache_addr addr_la9_cycle_xloc_1();
	cache_addr addr_la10_cycle_xloc_4();
	cache_addr addr_la10_cycle_xloc_3();

	cache_addr addr_la10_cycle_xloc_2();
	cache_addr addr_la10_cycle_xloc_1();
	cache_addr addr_la11_cycle_xloc_4();
	cache_addr addr_la11_cycle_xloc_3();

	cache_addr addr_la11_cycle_xloc_2();
	cache_addr addr_la11_cycle_xloc_1();
	cache_addr addr_la12_cycle_xloc_4();
	cache_addr addr_la12_cycle_xloc_3();

	cache_addr addr_la12_cycle_xloc_2();
	cache_addr addr_la12_cycle_xloc_1();
	cache_addr addr_la13_cycle_xloc_4();
	cache_addr addr_la13_cycle_xloc_3();

	cache_addr addr_la13_cycle_xloc_2();
	cache_addr addr_la13_cycle_xloc_1();
	cache_addr addr_la14_cycle_xloc_4();
	cache_addr addr_la14_cycle_xloc_3();

	cache_addr addr_la14_cycle_xloc_2();
	cache_addr addr_la14_cycle_xloc_1();
	cache_addr addr_la15_cycle_xloc_4();
	cache_addr addr_la15_cycle_xloc_3();

	cache_addr addr_la15_cycle_xloc_2();
	cache_addr addr_la15_cycle_xloc_1();
	cache_addr addr_dlyresult1_from_xloc2();
	cache_addr addr_dlyresult1_from_xloc1();

	cache_addr addr_dlyresult1_to_xloc2_pre();
	cache_addr addr_dlyresult1_to_xloc2();
	cache_addr addr_dlyresult1_to_xloc1_pre();
	cache_addr addr_dlyresult1_to_xloc1();

	cache_addr addr_dlyresult2_from_xloc2();
	cache_addr addr_dlyresult2_from_xloc1();
	cache_addr addr_dlyresult2_to_xloc2_pre();
	cache_addr addr_dlyresult2_to_xloc2();

	cache_addr addr_dlyresult2_to_xloc1_pre();
	cache_addr addr_dlyresult2_to_xloc1();
	cache_addr addr_dlyresult3_from_xloc2();
	cache_addr addr_dlyresult3_from_xloc1();

	cache_addr addr_dlyresult3_to_xloc2_pre();
	cache_addr addr_dlyresult3_to_xloc2();
	cache_addr addr_dlyresult3_to_xloc1_pre();
	cache_addr addr_dlyresult3_to_xloc1();

	cache_addr addr_dlyresult4_from_xloc2();
	cache_addr addr_dlyresult4_from_xloc1();
	cache_addr addr_dlyresult4_to_xloc2_pre();
	cache_addr addr_dlyresult4_to_xloc2();

	cache_addr addr_dlyresult4_to_xloc1_pre();
	cache_addr addr_dlyresult4_to_xloc1();
	cache_addr addr_dlyresult5_from_xloc2();
	cache_addr addr_dlyresult5_from_xloc1();

	cache_addr addr_dlyresult5_to_xloc2_pre();
	cache_addr addr_dlyresult5_to_xloc2();
	cache_addr addr_dlyresult5_to_xloc1_pre();
	cache_addr addr_dlyresult5_to_xloc1();

	cache_addr addr_dlyresult6_from_xloc2();
	cache_addr addr_dlyresult6_from_xloc1();
	cache_addr addr_dlyresult6_to_xloc2_pre();
	cache_addr addr_dlyresult6_to_xloc2();

	cache_addr addr_dlyresult6_to_xloc1_pre();
	cache_addr addr_dlyresult6_to_xloc1();
	cache_addr addr_dlyresult7_from_xloc2();
	cache_addr addr_dlyresult7_from_xloc1();

	cache_addr addr_dlyresult7_to_xloc2_pre();
	cache_addr addr_dlyresult7_to_xloc2();
	cache_addr addr_dlyresult7_to_xloc1_pre();
	cache_addr addr_dlyresult7_to_xloc1();

	cache_addr addr_dlyresult8_from_xloc2();
	cache_addr addr_dlyresult8_from_xloc1();
	cache_addr addr_dlyresult8_to_xloc2_pre();
	cache_addr addr_dlyresult8_to_xloc2();

	cache_addr addr_dlyresult8_to_xloc1_pre();
	cache_addr addr_dlyresult8_to_xloc1();
	cache_addr addr_dlyresult9_from_xloc2();
	cache_addr addr_dlyresult9_from_xloc1();

	cache_addr addr_dlyresult9_to_xloc2_pre();
	cache_addr addr_dlyresult9_to_xloc2();
	cache_addr addr_dlyresult9_to_xloc1_pre();
	cache_addr addr_dlyresult9_to_xloc1();

	cache_addr addr_dlyresult10_from_xloc2();
	cache_addr addr_dlyresult10_from_xloc1();
	cache_addr addr_dlyresult10_to_xloc2_pre();
	cache_addr addr_dlyresult10_to_xloc2();

	cache_addr addr_dlyresult10_to_xloc1_pre();
	cache_addr addr_dlyresult10_to_xloc1();
};
#define _remap_MeasRetu_()\
	mapIn( &cha_max_min_top_base, 0x2400 + mAddrBase );\
	mapIn( &cha_threshold_l_mid_h, 0x2404 + mAddrBase );\
	mapIn( &cha_vmax_xloc, 0x2408 + mAddrBase );\
	mapIn( &cha_vmin_xloc, 0x240c + mAddrBase );\
	\
	mapIn( &cha_rtime, 0x2410 + mAddrBase );\
	mapIn( &cha_rtime_xloc_end, 0x2414 + mAddrBase );\
	mapIn( &cha_rtime_pre, 0x2418 + mAddrBase );\
	mapIn( &cha_rtime_xloc_end_pre, 0x241c + mAddrBase );\
	\
	mapIn( &cha_ftime, 0x2420 + mAddrBase );\
	mapIn( &cha_ftime_xloc_end, 0x2424 + mAddrBase );\
	mapIn( &cha_ftime_pre, 0x2428 + mAddrBase );\
	mapIn( &cha_ftime_xloc_end_pre, 0x242c + mAddrBase );\
	\
	mapIn( &cha_cycle_xloc_4, 0x2430 + mAddrBase );\
	mapIn( &cha_cycle_xloc_3, 0x2434 + mAddrBase );\
	mapIn( &cha_cycle_xloc_2, 0x2438 + mAddrBase );\
	mapIn( &cha_cycle_xloc_1, 0x243c + mAddrBase );\
	\
	mapIn( &cha_ave_sum_n_bits31_0, 0x2440 + mAddrBase );\
	mapIn( &cha_ave_sum_1_1_bits31_0, 0x2444 + mAddrBase );\
	mapIn( &cha_ave_sum_1_2_bits31_0, 0x2448 + mAddrBase );\
	mapIn( &cha_ave_sum_x_bit39_32, 0x244c + mAddrBase );\
	\
	mapIn( &cha_vrms_sum_n_bits31_0, 0x2450 + mAddrBase );\
	mapIn( &cha_vrms_sum_1_1_bits31_0, 0x2454 + mAddrBase );\
	mapIn( &cha_vrms_sum_1_2_bits31_0, 0x2458 + mAddrBase );\
	mapIn( &cha_vrms_sum_n_1_bits47_32, 0x245c + mAddrBase );\
	\
	mapIn( &cha_vrms_sum_2_bits47_32, 0x2460 + mAddrBase );\
	mapIn( &cha_total_rf_num, 0x2464 + mAddrBase );\
	mapIn( &reserved_x1, 0x2468 + mAddrBase );\
	mapIn( &cha_shoot, 0x246c + mAddrBase );\
	\
	mapIn( &cha_edge_cnt_start_xloc, 0x2470 + mAddrBase );\
	mapIn( &cha_edge_cnt_end_xloc, 0x2474 + mAddrBase );\
	mapIn( &cha_edge_cnt_start2_xloc, 0x2478 + mAddrBase );\
	mapIn( &cha_edge_cnt_end2_xloc, 0x247c + mAddrBase );\
	\
	mapIn( &chb_max_min_top_base, 0x2480 + mAddrBase );\
	mapIn( &chb_threshold_l_mid_h, 0x2484 + mAddrBase );\
	mapIn( &chb_vmax_xloc, 0x2488 + mAddrBase );\
	mapIn( &chb_vmin_xloc, 0x248c + mAddrBase );\
	\
	mapIn( &chb_rtime, 0x2490 + mAddrBase );\
	mapIn( &chb_rtime_xloc_end, 0x2494 + mAddrBase );\
	mapIn( &chb_rtime_pre, 0x2498 + mAddrBase );\
	mapIn( &chb_rtime_xloc_end_pre, 0x249c + mAddrBase );\
	\
	mapIn( &chb_ftime, 0x24a0 + mAddrBase );\
	mapIn( &chb_ftime_xloc_end, 0x24a4 + mAddrBase );\
	mapIn( &chb_ftime_pre, 0x24a8 + mAddrBase );\
	mapIn( &chb_ftime_xloc_end_pre, 0x24ac + mAddrBase );\
	\
	mapIn( &chb_cycle_xloc_4, 0x24b0 + mAddrBase );\
	mapIn( &chb_cycle_xloc_3, 0x24b4 + mAddrBase );\
	mapIn( &chb_cycle_xloc_2, 0x24b8 + mAddrBase );\
	mapIn( &chb_cycle_xloc_1, 0x24bc + mAddrBase );\
	\
	mapIn( &chb_ave_sum_n_bits31_0, 0x24c0 + mAddrBase );\
	mapIn( &chb_ave_sum_1_1_bits31_0, 0x24c4 + mAddrBase );\
	mapIn( &chb_ave_sum_1_2_bits31_0, 0x24c8 + mAddrBase );\
	mapIn( &chb_ave_sum_x_bit39_32, 0x24cc + mAddrBase );\
	\
	mapIn( &chb_vrms_sum_n_bits31_0, 0x24d0 + mAddrBase );\
	mapIn( &chb_vrms_sum_1_1_bits31_0, 0x24d4 + mAddrBase );\
	mapIn( &chb_vrms_sum_1_2_bits31_0, 0x24d8 + mAddrBase );\
	mapIn( &chb_vrms_sum_n_1_bits47_32, 0x24dc + mAddrBase );\
	\
	mapIn( &chb_vrms_sum_2_bits47_32, 0x24e0 + mAddrBase );\
	mapIn( &chb_total_rf_num, 0x24e4 + mAddrBase );\
	mapIn( &reserved_x2, 0x24e8 + mAddrBase );\
	mapIn( &chb_shoot, 0x24ec + mAddrBase );\
	\
	mapIn( &chb_edge_cnt_start_xloc, 0x24f0 + mAddrBase );\
	mapIn( &chb_edge_cnt_end_xloc, 0x24f4 + mAddrBase );\
	mapIn( &chb_edge_cnt_start2_xloc, 0x24f8 + mAddrBase );\
	mapIn( &chb_edge_cnt_end2_xloc, 0x24fc + mAddrBase );\
	\
	mapIn( &chc_max_min_top_base, 0x2500 + mAddrBase );\
	mapIn( &chc_threshold_l_mid_h, 0x2504 + mAddrBase );\
	mapIn( &chc_vmax_xloc, 0x2508 + mAddrBase );\
	mapIn( &chc_vmin_xloc, 0x250c + mAddrBase );\
	\
	mapIn( &chc_rtime, 0x2510 + mAddrBase );\
	mapIn( &chc_rtime_xloc_end, 0x2514 + mAddrBase );\
	mapIn( &chc_rtime_pre, 0x2518 + mAddrBase );\
	mapIn( &chc_rtime_xloc_end_pre, 0x251c + mAddrBase );\
	\
	mapIn( &chc_ftime, 0x2520 + mAddrBase );\
	mapIn( &chc_ftime_xloc_end, 0x2524 + mAddrBase );\
	mapIn( &chc_ftime_pre, 0x2528 + mAddrBase );\
	mapIn( &chc_ftime_xloc_end_pre, 0x252c + mAddrBase );\
	\
	mapIn( &chc_cycle_xloc_4, 0x2530 + mAddrBase );\
	mapIn( &chc_cycle_xloc_3, 0x2534 + mAddrBase );\
	mapIn( &chc_cycle_xloc_2, 0x2538 + mAddrBase );\
	mapIn( &chc_cycle_xloc_1, 0x253c + mAddrBase );\
	\
	mapIn( &chc_ave_sum_n_bits31_0, 0x2540 + mAddrBase );\
	mapIn( &chc_ave_sum_1_1_bits31_0, 0x2544 + mAddrBase );\
	mapIn( &chc_ave_sum_1_2_bits31_0, 0x2548 + mAddrBase );\
	mapIn( &chc_ave_sum_x_bit39_32, 0x254c + mAddrBase );\
	\
	mapIn( &chc_vrms_sum_n_bits31_0, 0x2550 + mAddrBase );\
	mapIn( &chc_vrms_sum_1_1_bits31_0, 0x2554 + mAddrBase );\
	mapIn( &chc_vrms_sum_1_2_bits31_0, 0x2558 + mAddrBase );\
	mapIn( &chc_vrms_sum_n_1_bits47_32, 0x255c + mAddrBase );\
	\
	mapIn( &chc_vrms_sum_2_bits47_32, 0x2560 + mAddrBase );\
	mapIn( &chc_total_rf_num, 0x2564 + mAddrBase );\
	mapIn( &reserved_x3, 0x2568 + mAddrBase );\
	mapIn( &chc_shoot, 0x256c + mAddrBase );\
	\
	mapIn( &chc_edge_cnt_start_xloc, 0x2570 + mAddrBase );\
	mapIn( &chc_edge_cnt_end_xloc, 0x2574 + mAddrBase );\
	mapIn( &chc_edge_cnt_start2_xloc, 0x2578 + mAddrBase );\
	mapIn( &chc_edge_cnt_end2_xloc, 0x257c + mAddrBase );\
	\
	mapIn( &chd_max_min_top_base, 0x2580 + mAddrBase );\
	mapIn( &chd_threshold_l_mid_h, 0x2584 + mAddrBase );\
	mapIn( &chd_vmax_xloc, 0x2588 + mAddrBase );\
	mapIn( &chd_vmin_xloc, 0x258c + mAddrBase );\
	\
	mapIn( &chd_rtime, 0x2590 + mAddrBase );\
	mapIn( &chd_rtime_xloc_end, 0x2594 + mAddrBase );\
	mapIn( &chd_rtime_pre, 0x2598 + mAddrBase );\
	mapIn( &chd_rtime_xloc_end_pre, 0x259c + mAddrBase );\
	\
	mapIn( &chd_ftime, 0x25a0 + mAddrBase );\
	mapIn( &chd_ftime_xloc_end, 0x25a4 + mAddrBase );\
	mapIn( &chd_ftime_pre, 0x25a8 + mAddrBase );\
	mapIn( &chd_ftime_xloc_end_pre, 0x25ac + mAddrBase );\
	\
	mapIn( &chd_cycle_xloc_4, 0x25b0 + mAddrBase );\
	mapIn( &chd_cycle_xloc_3, 0x25b4 + mAddrBase );\
	mapIn( &chd_cycle_xloc_2, 0x25b8 + mAddrBase );\
	mapIn( &chd_cycle_xloc_1, 0x25bc + mAddrBase );\
	\
	mapIn( &chd_ave_sum_n_bits31_0, 0x25c0 + mAddrBase );\
	mapIn( &chd_ave_sum_1_1_bits31_0, 0x25c4 + mAddrBase );\
	mapIn( &chd_ave_sum_1_2_bits31_0, 0x25c8 + mAddrBase );\
	mapIn( &chd_ave_sum_x_bit39_32, 0x25cc + mAddrBase );\
	\
	mapIn( &chd_vrms_sum_n_bits31_0, 0x25d0 + mAddrBase );\
	mapIn( &chd_vrms_sum_1_1_bits31_0, 0x25d4 + mAddrBase );\
	mapIn( &chd_vrms_sum_1_2_bits31_0, 0x25d8 + mAddrBase );\
	mapIn( &chd_vrms_sum_n_1_bits47_32, 0x25dc + mAddrBase );\
	\
	mapIn( &chd_vrms_sum_2_bits47_32, 0x25e0 + mAddrBase );\
	mapIn( &chd_total_rf_num, 0x25e4 + mAddrBase );\
	mapIn( &reservd_x4, 0x25e8 + mAddrBase );\
	mapIn( &chd_shoot, 0x25ec + mAddrBase );\
	\
	mapIn( &chd_edge_cnt_start_xloc, 0x25f0 + mAddrBase );\
	mapIn( &chd_edge_cnt_end_xloc, 0x25f4 + mAddrBase );\
	mapIn( &chd_edge_cnt_start2_xloc, 0x25f8 + mAddrBase );\
	mapIn( &chd_edge_cnt_end2_xloc, 0x25fc + mAddrBase );\
	\
	mapIn( &meas_version_rd, 0x27f8 + mAddrBase );\
	mapIn( &meas_result_done, 0x27fc + mAddrBase );\
	mapIn( &la0_cycle_xloc_4, 0x2600 + mAddrBase );\
	mapIn( &la0_cycle_xloc_3, 0x2604 + mAddrBase );\
	\
	mapIn( &la0_cycle_xloc_2, 0x2608 + mAddrBase );\
	mapIn( &la0_cycle_xloc_1, 0x260c + mAddrBase );\
	mapIn( &la1_cycle_xloc_4, 0x2600 + mAddrBase );\
	mapIn( &la1_cycle_xloc_3, 0x2614 + mAddrBase );\
	\
	mapIn( &la1_cycle_xloc_2, 0x2618 + mAddrBase );\
	mapIn( &la1_cycle_xloc_1, 0x261c + mAddrBase );\
	mapIn( &la2_cycle_xloc_4, 0x2620 + mAddrBase );\
	mapIn( &la2_cycle_xloc_3, 0x2624 + mAddrBase );\
	\
	mapIn( &la2_cycle_xloc_2, 0x2628 + mAddrBase );\
	mapIn( &la2_cycle_xloc_1, 0x262c + mAddrBase );\
	mapIn( &la3_cycle_xloc_4, 0x2630 + mAddrBase );\
	mapIn( &la3_cycle_xloc_3, 0x2634 + mAddrBase );\
	\
	mapIn( &la3_cycle_xloc_2, 0x2638 + mAddrBase );\
	mapIn( &la3_cycle_xloc_1, 0x263c + mAddrBase );\
	mapIn( &la4_cycle_xloc_4, 0x2640 + mAddrBase );\
	mapIn( &la4_cycle_xloc_3, 0x2644 + mAddrBase );\
	\
	mapIn( &la4_cycle_xloc_2, 0x2648 + mAddrBase );\
	mapIn( &la4_cycle_xloc_1, 0x264c + mAddrBase );\
	mapIn( &la5_cycle_xloc_4, 0x2650 + mAddrBase );\
	mapIn( &la5_cycle_xloc_3, 0x2654 + mAddrBase );\
	\
	mapIn( &la5_cycle_xloc_2, 0x2658 + mAddrBase );\
	mapIn( &la5_cycle_xloc_1, 0x265c + mAddrBase );\
	mapIn( &la6_cycle_xloc_4, 0x2660 + mAddrBase );\
	mapIn( &la6_cycle_xloc_3, 0x2664 + mAddrBase );\
	\
	mapIn( &la6_cycle_xloc_2, 0x2668 + mAddrBase );\
	mapIn( &la6_cycle_xloc_1, 0x266c + mAddrBase );\
	mapIn( &la7_cycle_xloc_4, 0x2670 + mAddrBase );\
	mapIn( &la7_cycle_xloc_3, 0x2674 + mAddrBase );\
	\
	mapIn( &la7_cycle_xloc_2, 0x2678 + mAddrBase );\
	mapIn( &la7_cycle_xloc_1, 0x267c + mAddrBase );\
	mapIn( &la8_cycle_xloc_4, 0x2680 + mAddrBase );\
	mapIn( &la8_cycle_xloc_3, 0x2684 + mAddrBase );\
	\
	mapIn( &la8_cycle_xloc_2, 0x2688 + mAddrBase );\
	mapIn( &la8_cycle_xloc_1, 0x268c + mAddrBase );\
	mapIn( &la9_cycle_xloc_4, 0x2690 + mAddrBase );\
	mapIn( &la9_cycle_xloc_3, 0x2694 + mAddrBase );\
	\
	mapIn( &la9_cycle_xloc_2, 0x2698 + mAddrBase );\
	mapIn( &la9_cycle_xloc_1, 0x269c + mAddrBase );\
	mapIn( &la10_cycle_xloc_4, 0x26a0 + mAddrBase );\
	mapIn( &la10_cycle_xloc_3, 0x26a4 + mAddrBase );\
	\
	mapIn( &la10_cycle_xloc_2, 0x26a8 + mAddrBase );\
	mapIn( &la10_cycle_xloc_1, 0x26ac + mAddrBase );\
	mapIn( &la11_cycle_xloc_4, 0x26b0 + mAddrBase );\
	mapIn( &la11_cycle_xloc_3, 0x26b4 + mAddrBase );\
	\
	mapIn( &la11_cycle_xloc_2, 0x26b8 + mAddrBase );\
	mapIn( &la11_cycle_xloc_1, 0x26bc + mAddrBase );\
	mapIn( &la12_cycle_xloc_4, 0x26c0 + mAddrBase );\
	mapIn( &la12_cycle_xloc_3, 0x26c4 + mAddrBase );\
	\
	mapIn( &la12_cycle_xloc_2, 0x26c8 + mAddrBase );\
	mapIn( &la12_cycle_xloc_1, 0x26cc + mAddrBase );\
	mapIn( &la13_cycle_xloc_4, 0x26d0 + mAddrBase );\
	mapIn( &la13_cycle_xloc_3, 0x26d4 + mAddrBase );\
	\
	mapIn( &la13_cycle_xloc_2, 0x26d8 + mAddrBase );\
	mapIn( &la13_cycle_xloc_1, 0x26dc + mAddrBase );\
	mapIn( &la14_cycle_xloc_4, 0x26e0 + mAddrBase );\
	mapIn( &la14_cycle_xloc_3, 0x26e4 + mAddrBase );\
	\
	mapIn( &la14_cycle_xloc_2, 0x26e8 + mAddrBase );\
	mapIn( &la14_cycle_xloc_1, 0x26ec + mAddrBase );\
	mapIn( &la15_cycle_xloc_4, 0x26f0 + mAddrBase );\
	mapIn( &la15_cycle_xloc_3, 0x26f4 + mAddrBase );\
	\
	mapIn( &la15_cycle_xloc_2, 0x26f8 + mAddrBase );\
	mapIn( &la15_cycle_xloc_1, 0x26fc + mAddrBase );\
	mapIn( &dlyresult1_from_xloc2, 0x2700 + mAddrBase );\
	mapIn( &dlyresult1_from_xloc1, 0x2704 + mAddrBase );\
	\
	mapIn( &dlyresult1_to_xloc2_pre, 0x2708 + mAddrBase );\
	mapIn( &dlyresult1_to_xloc2, 0x270c + mAddrBase );\
	mapIn( &dlyresult1_to_xloc1_pre, 0x2710 + mAddrBase );\
	mapIn( &dlyresult1_to_xloc1, 0x2714 + mAddrBase );\
	\
	mapIn( &dlyresult2_from_xloc2, 0x2718 + mAddrBase );\
	mapIn( &dlyresult2_from_xloc1, 0x271c    + mAddrBase );\
	mapIn( &dlyresult2_to_xloc2_pre, 0x2720    + mAddrBase );\
	mapIn( &dlyresult2_to_xloc2, 0x2724    + mAddrBase );\
	\
	mapIn( &dlyresult2_to_xloc1_pre, 0x2728    + mAddrBase );\
	mapIn( &dlyresult2_to_xloc1, 0x272c    + mAddrBase );\
	mapIn( &dlyresult3_from_xloc2, 0x2730 + mAddrBase );\
	mapIn( &dlyresult3_from_xloc1, 0x2734    + mAddrBase );\
	\
	mapIn( &dlyresult3_to_xloc2_pre, 0x2738    + mAddrBase );\
	mapIn( &dlyresult3_to_xloc2, 0x273c    + mAddrBase );\
	mapIn( &dlyresult3_to_xloc1_pre, 0x2740    + mAddrBase );\
	mapIn( &dlyresult3_to_xloc1, 0x2744    + mAddrBase );\
	\
	mapIn( &dlyresult4_from_xloc2, 0x2748 + mAddrBase );\
	mapIn( &dlyresult4_from_xloc1, 0x274c    + mAddrBase );\
	mapIn( &dlyresult4_to_xloc2_pre, 0x2750    + mAddrBase );\
	mapIn( &dlyresult4_to_xloc2, 0x2754    + mAddrBase );\
	\
	mapIn( &dlyresult4_to_xloc1_pre, 0x2758    + mAddrBase );\
	mapIn( &dlyresult4_to_xloc1, 0x275c    + mAddrBase );\
	mapIn( &dlyresult5_from_xloc2, 0x2760 + mAddrBase );\
	mapIn( &dlyresult5_from_xloc1, 0x2764    + mAddrBase );\
	\
	mapIn( &dlyresult5_to_xloc2_pre, 0x2768    + mAddrBase );\
	mapIn( &dlyresult5_to_xloc2, 0x276c    + mAddrBase );\
	mapIn( &dlyresult5_to_xloc1_pre, 0x2770    + mAddrBase );\
	mapIn( &dlyresult5_to_xloc1, 0x2774    + mAddrBase );\
	\
	mapIn( &dlyresult6_from_xloc2, 0x2778 + mAddrBase );\
	mapIn( &dlyresult6_from_xloc1, 0x277c    + mAddrBase );\
	mapIn( &dlyresult6_to_xloc2_pre, 0x2780    + mAddrBase );\
	mapIn( &dlyresult6_to_xloc2, 0x2784    + mAddrBase );\
	\
	mapIn( &dlyresult6_to_xloc1_pre, 0x2788    + mAddrBase );\
	mapIn( &dlyresult6_to_xloc1, 0x278c    + mAddrBase );\
	mapIn( &dlyresult7_from_xloc2, 0x2790 + mAddrBase );\
	mapIn( &dlyresult7_from_xloc1, 0x2794    + mAddrBase );\
	\
	mapIn( &dlyresult7_to_xloc2_pre, 0x2798    + mAddrBase );\
	mapIn( &dlyresult7_to_xloc2, 0x279c    + mAddrBase );\
	mapIn( &dlyresult7_to_xloc1_pre, 0x27a0    + mAddrBase );\
	mapIn( &dlyresult7_to_xloc1, 0x27a4    + mAddrBase );\
	\
	mapIn( &dlyresult8_from_xloc2, 0x27a8 + mAddrBase );\
	mapIn( &dlyresult8_from_xloc1, 0x27ac    + mAddrBase );\
	mapIn( &dlyresult8_to_xloc2_pre, 0x27b0    + mAddrBase );\
	mapIn( &dlyresult8_to_xloc2, 0x27b4    + mAddrBase );\
	\
	mapIn( &dlyresult8_to_xloc1_pre, 0x27b8    + mAddrBase );\
	mapIn( &dlyresult8_to_xloc1, 0x27bc    + mAddrBase );\
	mapIn( &dlyresult9_from_xloc2, 0x27c0 + mAddrBase );\
	mapIn( &dlyresult9_from_xloc1, 0x27c4    + mAddrBase );\
	\
	mapIn( &dlyresult9_to_xloc2_pre, 0x27c8    + mAddrBase );\
	mapIn( &dlyresult9_to_xloc2, 0x27cc    + mAddrBase );\
	mapIn( &dlyresult9_to_xloc1_pre, 0x27d0    + mAddrBase );\
	mapIn( &dlyresult9_to_xloc1, 0x27d4    + mAddrBase );\
	\
	mapIn( &dlyresult10_from_xloc2, 0x27d8 + mAddrBase );\
	mapIn( &dlyresult10_from_xloc1, 0x27dc     + mAddrBase );\
	mapIn( &dlyresult10_to_xloc2_pre, 0x27e0     + mAddrBase );\
	mapIn( &dlyresult10_to_xloc2, 0x27e4     + mAddrBase );\
	\
	mapIn( &dlyresult10_to_xloc1_pre, 0x27e8     + mAddrBase );\
	mapIn( &dlyresult10_to_xloc1, 0x27ec     + mAddrBase );\


#endif
