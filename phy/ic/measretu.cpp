#include "measretu.h"
MeasRetu_reg MeasRetuMemProxy::getcha_max_min_top_base_cha_vmin()
{
	return cha_max_min_top_base.cha_vmin;
}
MeasRetu_reg MeasRetuMemProxy::getcha_max_min_top_base_cha_vmax()
{
	return cha_max_min_top_base.cha_vmax;
}
MeasRetu_reg MeasRetuMemProxy::getcha_max_min_top_base_cha_vbase()
{
	return cha_max_min_top_base.cha_vbase;
}
MeasRetu_reg MeasRetuMemProxy::getcha_max_min_top_base_cha_vtop()
{
	return cha_max_min_top_base.cha_vtop;
}

MeasRetu_reg MeasRetuMemProxy::getcha_max_min_top_base()
{
	return cha_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_threshold_l_mid_h_cha_threshold_l()
{
	return cha_threshold_l_mid_h.cha_threshold_l;
}
MeasRetu_reg MeasRetuMemProxy::getcha_threshold_l_mid_h_cha_threshold_mid()
{
	return cha_threshold_l_mid_h.cha_threshold_mid;
}
MeasRetu_reg MeasRetuMemProxy::getcha_threshold_l_mid_h_cha_threshold_h()
{
	return cha_threshold_l_mid_h.cha_threshold_h;
}
MeasRetu_reg MeasRetuMemProxy::getcha_threshold_l_mid_h_reserved()
{
	return cha_threshold_l_mid_h.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getcha_threshold_l_mid_h()
{
	return cha_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_vmax_xloc_vmax_xloc()
{
	return cha_vmax_xloc.vmax_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getcha_vmax_xloc_reserved()
{
	return cha_vmax_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_vmax_xloc_vmax_xloc_vld()
{
	return cha_vmax_xloc.vmax_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_vmax_xloc()
{
	return cha_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_vmin_xloc_vmin_xloc()
{
	return cha_vmin_xloc.vmin_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getcha_vmin_xloc_reserved()
{
	return cha_vmin_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_vmin_xloc_vmin_xloc_vld()
{
	return cha_vmin_xloc.vmin_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_vmin_xloc()
{
	return cha_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_rtime_rtime()
{
	return cha_rtime.rtime;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_reserved()
{
	return cha_rtime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_rtime_vld()
{
	return cha_rtime.rtime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_rtime()
{
	return cha_rtime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end_rtime_xloc_end()
{
	return cha_rtime_xloc_end.rtime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end_reserved()
{
	return cha_rtime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end_rtime_xloc_end_vld()
{
	return cha_rtime_xloc_end.rtime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end()
{
	return cha_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_rtime_pre_rtime_pre()
{
	return cha_rtime_pre.rtime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_pre_reserved()
{
	return cha_rtime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_pre_rtime_pre_vld()
{
	return cha_rtime_pre.rtime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_rtime_pre()
{
	return cha_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end_pre_rtime_xloc_end_pre()
{
	return cha_rtime_xloc_end_pre.rtime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end_pre_reserved()
{
	return cha_rtime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end_pre_rtime_xloc_end_pre_vld()
{
	return cha_rtime_xloc_end_pre.rtime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_rtime_xloc_end_pre()
{
	return cha_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ftime_ftime()
{
	return cha_ftime.ftime;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_reserved()
{
	return cha_ftime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_ftime_vld()
{
	return cha_ftime.ftime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ftime()
{
	return cha_ftime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end_ftime_xloc_end()
{
	return cha_ftime_xloc_end.ftime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end_reserved()
{
	return cha_ftime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end_ftime_xloc_end_vld()
{
	return cha_ftime_xloc_end.ftime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end()
{
	return cha_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ftime_pre_ftime_pre()
{
	return cha_ftime_pre.ftime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_pre_reserved()
{
	return cha_ftime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_pre_ftime_pre_vld()
{
	return cha_ftime_pre.ftime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ftime_pre()
{
	return cha_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end_pre_ftime_xloc_end_pre()
{
	return cha_ftime_xloc_end_pre.ftime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end_pre_reserved()
{
	return cha_ftime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end_pre_ftime_xloc_end_pre_vld()
{
	return cha_ftime_xloc_end_pre.ftime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ftime_xloc_end_pre()
{
	return cha_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_4_cycle_xloc_4()
{
	return cha_cycle_xloc_4.cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_4_edge_type()
{
	return cha_cycle_xloc_4.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_4_cycle_xloc_4_vld()
{
	return cha_cycle_xloc_4.cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_4()
{
	return cha_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_3_cycle_xloc_3()
{
	return cha_cycle_xloc_3.cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_3_edge_type()
{
	return cha_cycle_xloc_3.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_3_cycle_xloc_3_vld()
{
	return cha_cycle_xloc_3.cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_3()
{
	return cha_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_2_cycle_xloc_2()
{
	return cha_cycle_xloc_2.cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_2_edge_type()
{
	return cha_cycle_xloc_2.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_2_cycle_xloc_2_vld()
{
	return cha_cycle_xloc_2.cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_2()
{
	return cha_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_1_cycle_xloc_1()
{
	return cha_cycle_xloc_1.cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_1_edge_type()
{
	return cha_cycle_xloc_1.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_1_cycle_xloc_1_vld()
{
	return cha_cycle_xloc_1.cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getcha_cycle_xloc_1()
{
	return cha_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_n_bits31_0_ave_sum_n_bit31_0()
{
	return cha_ave_sum_n_bits31_0.ave_sum_n_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_n_bits31_0()
{
	return cha_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0()
{
	return cha_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_1_1_bits31_0()
{
	return cha_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0()
{
	return cha_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_1_2_bits31_0()
{
	return cha_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_x_bit39_32_ave_sum_n_bit39_32()
{
	return cha_ave_sum_x_bit39_32.ave_sum_n_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32()
{
	return cha_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32()
{
	return cha_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_x_bit39_32_reserved()
{
	return cha_ave_sum_x_bit39_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getcha_ave_sum_x_bit39_32()
{
	return cha_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0()
{
	return cha_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_n_bits31_0()
{
	return cha_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0()
{
	return cha_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_1_1_bits31_0()
{
	return cha_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0()
{
	return cha_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_1_2_bits31_0()
{
	return cha_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32()
{
	return cha_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32()
{
	return cha_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32;
}

MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_n_1_bits47_32()
{
	return cha_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32()
{
	return cha_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_2_bits47_32_reserved()
{
	return cha_vrms_sum_2_bits47_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getcha_vrms_sum_2_bits47_32()
{
	return cha_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_total_rf_num_total_rf_num()
{
	return cha_total_rf_num.total_rf_num;
}
MeasRetu_reg MeasRetuMemProxy::getcha_total_rf_num_slope()
{
	return cha_total_rf_num.slope;
}
MeasRetu_reg MeasRetuMemProxy::getcha_total_rf_num_valid()
{
	return cha_total_rf_num.valid;
}

MeasRetu_reg MeasRetuMemProxy::getcha_total_rf_num()
{
	return cha_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMemProxy::getreserved_x1()
{
	return reserved_x1;
}


MeasRetu_reg MeasRetuMemProxy::getcha_shoot_over_shoot()
{
	return cha_shoot.over_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getcha_shoot_pre_shoot()
{
	return cha_shoot.pre_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getcha_shoot_reserved()
{
	return cha_shoot.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getcha_shoot()
{
	return cha_shoot.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start_xloc_edge_cnt_start_xloc()
{
	return cha_edge_cnt_start_xloc.edge_cnt_start_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start_xloc_reserved()
{
	return cha_edge_cnt_start_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start_xloc_valid()
{
	return cha_edge_cnt_start_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start_xloc()
{
	return cha_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end_xloc_edge_cnt_end_xloc()
{
	return cha_edge_cnt_end_xloc.edge_cnt_end_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end_xloc_reserved()
{
	return cha_edge_cnt_end_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end_xloc_valid()
{
	return cha_edge_cnt_end_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end_xloc()
{
	return cha_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start2_xloc_edge_cnt_start2_xloc()
{
	return cha_edge_cnt_start2_xloc.edge_cnt_start2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start2_xloc_reserved()
{
	return cha_edge_cnt_start2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start2_xloc_valid()
{
	return cha_edge_cnt_start2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_start2_xloc()
{
	return cha_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end2_xloc_edge_cnt_end2_xloc()
{
	return cha_edge_cnt_end2_xloc.edge_cnt_end2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end2_xloc_reserved()
{
	return cha_edge_cnt_end2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end2_xloc_valid()
{
	return cha_edge_cnt_end2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getcha_edge_cnt_end2_xloc()
{
	return cha_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_max_min_top_base_cha_vmin()
{
	return chb_max_min_top_base.cha_vmin;
}
MeasRetu_reg MeasRetuMemProxy::getchb_max_min_top_base_cha_vmax()
{
	return chb_max_min_top_base.cha_vmax;
}
MeasRetu_reg MeasRetuMemProxy::getchb_max_min_top_base_cha_vbase()
{
	return chb_max_min_top_base.cha_vbase;
}
MeasRetu_reg MeasRetuMemProxy::getchb_max_min_top_base_cha_vtop()
{
	return chb_max_min_top_base.cha_vtop;
}

MeasRetu_reg MeasRetuMemProxy::getchb_max_min_top_base()
{
	return chb_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_threshold_l_mid_h_cha_threshold_l()
{
	return chb_threshold_l_mid_h.cha_threshold_l;
}
MeasRetu_reg MeasRetuMemProxy::getchb_threshold_l_mid_h_cha_threshold_mid()
{
	return chb_threshold_l_mid_h.cha_threshold_mid;
}
MeasRetu_reg MeasRetuMemProxy::getchb_threshold_l_mid_h_cha_threshold_h()
{
	return chb_threshold_l_mid_h.cha_threshold_h;
}
MeasRetu_reg MeasRetuMemProxy::getchb_threshold_l_mid_h_reserved()
{
	return chb_threshold_l_mid_h.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchb_threshold_l_mid_h()
{
	return chb_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_vmax_xloc_vmax_xloc()
{
	return chb_vmax_xloc.vmax_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchb_vmax_xloc_reserved()
{
	return chb_vmax_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_vmax_xloc_vmax_xloc_vld()
{
	return chb_vmax_xloc.vmax_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_vmax_xloc()
{
	return chb_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_vmin_xloc_vmin_xloc()
{
	return chb_vmin_xloc.vmin_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchb_vmin_xloc_reserved()
{
	return chb_vmin_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_vmin_xloc_vmin_xloc_vld()
{
	return chb_vmin_xloc.vmin_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_vmin_xloc()
{
	return chb_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_rtime_rtime()
{
	return chb_rtime.rtime;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_reserved()
{
	return chb_rtime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_rtime_vld()
{
	return chb_rtime.rtime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_rtime()
{
	return chb_rtime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end_rtime_xloc_end()
{
	return chb_rtime_xloc_end.rtime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end_reserved()
{
	return chb_rtime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end_rtime_xloc_end_vld()
{
	return chb_rtime_xloc_end.rtime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end()
{
	return chb_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_rtime_pre_rtime_pre()
{
	return chb_rtime_pre.rtime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_pre_reserved()
{
	return chb_rtime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_pre_rtime_pre_vld()
{
	return chb_rtime_pre.rtime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_rtime_pre()
{
	return chb_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end_pre_rtime_xloc_end_pre()
{
	return chb_rtime_xloc_end_pre.rtime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end_pre_reserved()
{
	return chb_rtime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end_pre_rtime_xloc_end_pre_vld()
{
	return chb_rtime_xloc_end_pre.rtime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_rtime_xloc_end_pre()
{
	return chb_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ftime_ftime()
{
	return chb_ftime.ftime;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_reserved()
{
	return chb_ftime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_ftime_vld()
{
	return chb_ftime.ftime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ftime()
{
	return chb_ftime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end_ftime_xloc_end()
{
	return chb_ftime_xloc_end.ftime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end_reserved()
{
	return chb_ftime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end_ftime_xloc_end_vld()
{
	return chb_ftime_xloc_end.ftime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end()
{
	return chb_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ftime_pre_ftime_pre()
{
	return chb_ftime_pre.ftime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_pre_reserved()
{
	return chb_ftime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_pre_ftime_pre_vld()
{
	return chb_ftime_pre.ftime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ftime_pre()
{
	return chb_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end_pre_ftime_xloc_end_pre()
{
	return chb_ftime_xloc_end_pre.ftime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end_pre_reserved()
{
	return chb_ftime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end_pre_ftime_xloc_end_pre_vld()
{
	return chb_ftime_xloc_end_pre.ftime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ftime_xloc_end_pre()
{
	return chb_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_4_cycle_xloc_4()
{
	return chb_cycle_xloc_4.cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_4_edge_type()
{
	return chb_cycle_xloc_4.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_4_cycle_xloc_4_vld()
{
	return chb_cycle_xloc_4.cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_4()
{
	return chb_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_3_cycle_xloc_3()
{
	return chb_cycle_xloc_3.cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_3_edge_type()
{
	return chb_cycle_xloc_3.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_3_cycle_xloc_3_vld()
{
	return chb_cycle_xloc_3.cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_3()
{
	return chb_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_2_cycle_xloc_2()
{
	return chb_cycle_xloc_2.cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_2_edge_type()
{
	return chb_cycle_xloc_2.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_2_cycle_xloc_2_vld()
{
	return chb_cycle_xloc_2.cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_2()
{
	return chb_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_1_cycle_xloc_1()
{
	return chb_cycle_xloc_1.cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_1_edge_type()
{
	return chb_cycle_xloc_1.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_1_cycle_xloc_1_vld()
{
	return chb_cycle_xloc_1.cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchb_cycle_xloc_1()
{
	return chb_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_n_bits31_0_ave_sum_n_bit31_0()
{
	return chb_ave_sum_n_bits31_0.ave_sum_n_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_n_bits31_0()
{
	return chb_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0()
{
	return chb_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_1_1_bits31_0()
{
	return chb_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0()
{
	return chb_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_1_2_bits31_0()
{
	return chb_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_x_bit39_32_ave_sum_n_bit39_32()
{
	return chb_ave_sum_x_bit39_32.ave_sum_n_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32()
{
	return chb_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32()
{
	return chb_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_x_bit39_32_reserved()
{
	return chb_ave_sum_x_bit39_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchb_ave_sum_x_bit39_32()
{
	return chb_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0()
{
	return chb_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_n_bits31_0()
{
	return chb_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0()
{
	return chb_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_1_1_bits31_0()
{
	return chb_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0()
{
	return chb_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_1_2_bits31_0()
{
	return chb_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32()
{
	return chb_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32()
{
	return chb_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32;
}

MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_n_1_bits47_32()
{
	return chb_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32()
{
	return chb_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_2_bits47_32_reserved()
{
	return chb_vrms_sum_2_bits47_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchb_vrms_sum_2_bits47_32()
{
	return chb_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_total_rf_num_total_rf_num()
{
	return chb_total_rf_num.total_rf_num;
}
MeasRetu_reg MeasRetuMemProxy::getchb_total_rf_num_slope()
{
	return chb_total_rf_num.slope;
}
MeasRetu_reg MeasRetuMemProxy::getchb_total_rf_num_valid()
{
	return chb_total_rf_num.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchb_total_rf_num()
{
	return chb_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMemProxy::getreserved_x2()
{
	return reserved_x2;
}


MeasRetu_reg MeasRetuMemProxy::getchb_shoot_over_shoot()
{
	return chb_shoot.over_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getchb_shoot_pre_shoot()
{
	return chb_shoot.pre_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getchb_shoot_reserved()
{
	return chb_shoot.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchb_shoot()
{
	return chb_shoot.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start_xloc_edge_cnt_start_xloc()
{
	return chb_edge_cnt_start_xloc.edge_cnt_start_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start_xloc_reserved()
{
	return chb_edge_cnt_start_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start_xloc_valid()
{
	return chb_edge_cnt_start_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start_xloc()
{
	return chb_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end_xloc_edge_cnt_end_xloc()
{
	return chb_edge_cnt_end_xloc.edge_cnt_end_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end_xloc_reserved()
{
	return chb_edge_cnt_end_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end_xloc_valid()
{
	return chb_edge_cnt_end_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end_xloc()
{
	return chb_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start2_xloc_edge_cnt_start2_xloc()
{
	return chb_edge_cnt_start2_xloc.edge_cnt_start2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start2_xloc_reserved()
{
	return chb_edge_cnt_start2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start2_xloc_valid()
{
	return chb_edge_cnt_start2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_start2_xloc()
{
	return chb_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end2_xloc_edge_cnt_end2_xloc()
{
	return chb_edge_cnt_end2_xloc.edge_cnt_end2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end2_xloc_reserved()
{
	return chb_edge_cnt_end2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end2_xloc_valid()
{
	return chb_edge_cnt_end2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchb_edge_cnt_end2_xloc()
{
	return chb_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_max_min_top_base_cha_vmin()
{
	return chc_max_min_top_base.cha_vmin;
}
MeasRetu_reg MeasRetuMemProxy::getchc_max_min_top_base_cha_vmax()
{
	return chc_max_min_top_base.cha_vmax;
}
MeasRetu_reg MeasRetuMemProxy::getchc_max_min_top_base_cha_vbase()
{
	return chc_max_min_top_base.cha_vbase;
}
MeasRetu_reg MeasRetuMemProxy::getchc_max_min_top_base_cha_vtop()
{
	return chc_max_min_top_base.cha_vtop;
}

MeasRetu_reg MeasRetuMemProxy::getchc_max_min_top_base()
{
	return chc_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_threshold_l_mid_h_cha_threshold_l()
{
	return chc_threshold_l_mid_h.cha_threshold_l;
}
MeasRetu_reg MeasRetuMemProxy::getchc_threshold_l_mid_h_cha_threshold_mid()
{
	return chc_threshold_l_mid_h.cha_threshold_mid;
}
MeasRetu_reg MeasRetuMemProxy::getchc_threshold_l_mid_h_cha_threshold_h()
{
	return chc_threshold_l_mid_h.cha_threshold_h;
}
MeasRetu_reg MeasRetuMemProxy::getchc_threshold_l_mid_h_reserved()
{
	return chc_threshold_l_mid_h.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchc_threshold_l_mid_h()
{
	return chc_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_vmax_xloc_vmax_xloc()
{
	return chc_vmax_xloc.vmax_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchc_vmax_xloc_reserved()
{
	return chc_vmax_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_vmax_xloc_vmax_xloc_vld()
{
	return chc_vmax_xloc.vmax_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_vmax_xloc()
{
	return chc_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_vmin_xloc_vmin_xloc()
{
	return chc_vmin_xloc.vmin_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchc_vmin_xloc_reserved()
{
	return chc_vmin_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_vmin_xloc_vmin_xloc_vld()
{
	return chc_vmin_xloc.vmin_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_vmin_xloc()
{
	return chc_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_rtime_rtime()
{
	return chc_rtime.rtime;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_reserved()
{
	return chc_rtime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_rtime_vld()
{
	return chc_rtime.rtime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_rtime()
{
	return chc_rtime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end_rtime_xloc_end()
{
	return chc_rtime_xloc_end.rtime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end_reserved()
{
	return chc_rtime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end_rtime_xloc_end_vld()
{
	return chc_rtime_xloc_end.rtime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end()
{
	return chc_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_rtime_pre_rtime_pre()
{
	return chc_rtime_pre.rtime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_pre_reserved()
{
	return chc_rtime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_pre_rtime_pre_vld()
{
	return chc_rtime_pre.rtime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_rtime_pre()
{
	return chc_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end_pre_rtime_xloc_end_pre()
{
	return chc_rtime_xloc_end_pre.rtime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end_pre_reserved()
{
	return chc_rtime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end_pre_rtime_xloc_end_pre_vld()
{
	return chc_rtime_xloc_end_pre.rtime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_rtime_xloc_end_pre()
{
	return chc_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ftime_ftime()
{
	return chc_ftime.ftime;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_reserved()
{
	return chc_ftime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_ftime_vld()
{
	return chc_ftime.ftime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ftime()
{
	return chc_ftime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end_ftime_xloc_end()
{
	return chc_ftime_xloc_end.ftime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end_reserved()
{
	return chc_ftime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end_ftime_xloc_end_vld()
{
	return chc_ftime_xloc_end.ftime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end()
{
	return chc_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ftime_pre_ftime_pre()
{
	return chc_ftime_pre.ftime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_pre_reserved()
{
	return chc_ftime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_pre_ftime_pre_vld()
{
	return chc_ftime_pre.ftime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ftime_pre()
{
	return chc_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end_pre_ftime_xloc_end_pre()
{
	return chc_ftime_xloc_end_pre.ftime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end_pre_reserved()
{
	return chc_ftime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end_pre_ftime_xloc_end_pre_vld()
{
	return chc_ftime_xloc_end_pre.ftime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ftime_xloc_end_pre()
{
	return chc_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_4_cycle_xloc_4()
{
	return chc_cycle_xloc_4.cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_4_edge_type()
{
	return chc_cycle_xloc_4.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_4_cycle_xloc_4_vld()
{
	return chc_cycle_xloc_4.cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_4()
{
	return chc_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_3_cycle_xloc_3()
{
	return chc_cycle_xloc_3.cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_3_edge_type()
{
	return chc_cycle_xloc_3.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_3_cycle_xloc_3_vld()
{
	return chc_cycle_xloc_3.cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_3()
{
	return chc_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_2_cycle_xloc_2()
{
	return chc_cycle_xloc_2.cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_2_edge_type()
{
	return chc_cycle_xloc_2.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_2_cycle_xloc_2_vld()
{
	return chc_cycle_xloc_2.cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_2()
{
	return chc_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_1_cycle_xloc_1()
{
	return chc_cycle_xloc_1.cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_1_edge_type()
{
	return chc_cycle_xloc_1.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_1_cycle_xloc_1_vld()
{
	return chc_cycle_xloc_1.cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchc_cycle_xloc_1()
{
	return chc_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_n_bits31_0_ave_sum_n_bit31_0()
{
	return chc_ave_sum_n_bits31_0.ave_sum_n_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_n_bits31_0()
{
	return chc_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0()
{
	return chc_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_1_1_bits31_0()
{
	return chc_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0()
{
	return chc_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_1_2_bits31_0()
{
	return chc_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_x_bit39_32_ave_sum_n_bit39_32()
{
	return chc_ave_sum_x_bit39_32.ave_sum_n_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32()
{
	return chc_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32()
{
	return chc_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_x_bit39_32_reserved()
{
	return chc_ave_sum_x_bit39_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchc_ave_sum_x_bit39_32()
{
	return chc_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0()
{
	return chc_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_n_bits31_0()
{
	return chc_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0()
{
	return chc_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_1_1_bits31_0()
{
	return chc_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0()
{
	return chc_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_1_2_bits31_0()
{
	return chc_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32()
{
	return chc_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32()
{
	return chc_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32;
}

MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_n_1_bits47_32()
{
	return chc_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32()
{
	return chc_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_2_bits47_32_reserved()
{
	return chc_vrms_sum_2_bits47_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchc_vrms_sum_2_bits47_32()
{
	return chc_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_total_rf_num_total_rf_num()
{
	return chc_total_rf_num.total_rf_num;
}
MeasRetu_reg MeasRetuMemProxy::getchc_total_rf_num_slope()
{
	return chc_total_rf_num.slope;
}
MeasRetu_reg MeasRetuMemProxy::getchc_total_rf_num_valid()
{
	return chc_total_rf_num.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchc_total_rf_num()
{
	return chc_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMemProxy::getreserved_x3()
{
	return reserved_x3;
}


MeasRetu_reg MeasRetuMemProxy::getchc_shoot_over_shoot()
{
	return chc_shoot.over_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getchc_shoot_pre_shoot()
{
	return chc_shoot.pre_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getchc_shoot_reserved()
{
	return chc_shoot.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchc_shoot()
{
	return chc_shoot.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start_xloc_edge_cnt_start_xloc()
{
	return chc_edge_cnt_start_xloc.edge_cnt_start_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start_xloc_reserved()
{
	return chc_edge_cnt_start_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start_xloc_valid()
{
	return chc_edge_cnt_start_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start_xloc()
{
	return chc_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end_xloc_edge_cnt_end_xloc()
{
	return chc_edge_cnt_end_xloc.edge_cnt_end_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end_xloc_reserved()
{
	return chc_edge_cnt_end_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end_xloc_valid()
{
	return chc_edge_cnt_end_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end_xloc()
{
	return chc_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start2_xloc_edge_cnt_start2_xloc()
{
	return chc_edge_cnt_start2_xloc.edge_cnt_start2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start2_xloc_reserved()
{
	return chc_edge_cnt_start2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start2_xloc_valid()
{
	return chc_edge_cnt_start2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_start2_xloc()
{
	return chc_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end2_xloc_edge_cnt_end2_xloc()
{
	return chc_edge_cnt_end2_xloc.edge_cnt_end2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end2_xloc_reserved()
{
	return chc_edge_cnt_end2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end2_xloc_valid()
{
	return chc_edge_cnt_end2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchc_edge_cnt_end2_xloc()
{
	return chc_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_max_min_top_base_cha_vmin()
{
	return chd_max_min_top_base.cha_vmin;
}
MeasRetu_reg MeasRetuMemProxy::getchd_max_min_top_base_cha_vmax()
{
	return chd_max_min_top_base.cha_vmax;
}
MeasRetu_reg MeasRetuMemProxy::getchd_max_min_top_base_cha_vbase()
{
	return chd_max_min_top_base.cha_vbase;
}
MeasRetu_reg MeasRetuMemProxy::getchd_max_min_top_base_cha_vtop()
{
	return chd_max_min_top_base.cha_vtop;
}

MeasRetu_reg MeasRetuMemProxy::getchd_max_min_top_base()
{
	return chd_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_threshold_l_mid_h_cha_threshold_l()
{
	return chd_threshold_l_mid_h.cha_threshold_l;
}
MeasRetu_reg MeasRetuMemProxy::getchd_threshold_l_mid_h_cha_threshold_mid()
{
	return chd_threshold_l_mid_h.cha_threshold_mid;
}
MeasRetu_reg MeasRetuMemProxy::getchd_threshold_l_mid_h_cha_threshold_h()
{
	return chd_threshold_l_mid_h.cha_threshold_h;
}
MeasRetu_reg MeasRetuMemProxy::getchd_threshold_l_mid_h_reserved()
{
	return chd_threshold_l_mid_h.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchd_threshold_l_mid_h()
{
	return chd_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_vmax_xloc_vmax_xloc()
{
	return chd_vmax_xloc.vmax_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchd_vmax_xloc_reserved()
{
	return chd_vmax_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_vmax_xloc_vmax_xloc_vld()
{
	return chd_vmax_xloc.vmax_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_vmax_xloc()
{
	return chd_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_vmin_xloc_vmin_xloc()
{
	return chd_vmin_xloc.vmin_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchd_vmin_xloc_reserved()
{
	return chd_vmin_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_vmin_xloc_vmin_xloc_vld()
{
	return chd_vmin_xloc.vmin_xloc_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_vmin_xloc()
{
	return chd_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_rtime_rtime()
{
	return chd_rtime.rtime;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_reserved()
{
	return chd_rtime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_rtime_vld()
{
	return chd_rtime.rtime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_rtime()
{
	return chd_rtime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end_rtime_xloc_end()
{
	return chd_rtime_xloc_end.rtime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end_reserved()
{
	return chd_rtime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end_rtime_xloc_end_vld()
{
	return chd_rtime_xloc_end.rtime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end()
{
	return chd_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_rtime_pre_rtime_pre()
{
	return chd_rtime_pre.rtime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_pre_reserved()
{
	return chd_rtime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_pre_rtime_pre_vld()
{
	return chd_rtime_pre.rtime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_rtime_pre()
{
	return chd_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end_pre_rtime_xloc_end_pre()
{
	return chd_rtime_xloc_end_pre.rtime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end_pre_reserved()
{
	return chd_rtime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end_pre_rtime_xloc_end_pre_vld()
{
	return chd_rtime_xloc_end_pre.rtime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_rtime_xloc_end_pre()
{
	return chd_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ftime_ftime()
{
	return chd_ftime.ftime;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_reserved()
{
	return chd_ftime.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_ftime_vld()
{
	return chd_ftime.ftime_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ftime()
{
	return chd_ftime.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end_ftime_xloc_end()
{
	return chd_ftime_xloc_end.ftime_xloc_end;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end_reserved()
{
	return chd_ftime_xloc_end.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end_ftime_xloc_end_vld()
{
	return chd_ftime_xloc_end.ftime_xloc_end_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end()
{
	return chd_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ftime_pre_ftime_pre()
{
	return chd_ftime_pre.ftime_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_pre_reserved()
{
	return chd_ftime_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_pre_ftime_pre_vld()
{
	return chd_ftime_pre.ftime_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ftime_pre()
{
	return chd_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end_pre_ftime_xloc_end_pre()
{
	return chd_ftime_xloc_end_pre.ftime_xloc_end_pre;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end_pre_reserved()
{
	return chd_ftime_xloc_end_pre.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end_pre_ftime_xloc_end_pre_vld()
{
	return chd_ftime_xloc_end_pre.ftime_xloc_end_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ftime_xloc_end_pre()
{
	return chd_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_4_cycle_xloc_4()
{
	return chd_cycle_xloc_4.cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_4_edge_type()
{
	return chd_cycle_xloc_4.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_4_cycle_xloc_4_vld()
{
	return chd_cycle_xloc_4.cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_4()
{
	return chd_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_3_cycle_xloc_3()
{
	return chd_cycle_xloc_3.cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_3_edge_type()
{
	return chd_cycle_xloc_3.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_3_cycle_xloc_3_vld()
{
	return chd_cycle_xloc_3.cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_3()
{
	return chd_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_2_cycle_xloc_2()
{
	return chd_cycle_xloc_2.cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_2_edge_type()
{
	return chd_cycle_xloc_2.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_2_cycle_xloc_2_vld()
{
	return chd_cycle_xloc_2.cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_2()
{
	return chd_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_1_cycle_xloc_1()
{
	return chd_cycle_xloc_1.cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_1_edge_type()
{
	return chd_cycle_xloc_1.edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_1_cycle_xloc_1_vld()
{
	return chd_cycle_xloc_1.cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getchd_cycle_xloc_1()
{
	return chd_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_n_bits31_0_ave_sum_n_bit31_0()
{
	return chd_ave_sum_n_bits31_0.ave_sum_n_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_n_bits31_0()
{
	return chd_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_1_1_bits31_0_ave_sum_1_1_bit31_0()
{
	return chd_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_1_1_bits31_0()
{
	return chd_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_1_2_bits31_0_ave_sum_1_2_bit31_0()
{
	return chd_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_1_2_bits31_0()
{
	return chd_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_x_bit39_32_ave_sum_n_bit39_32()
{
	return chd_ave_sum_x_bit39_32.ave_sum_n_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_x_bit39_32_ave_sum_1_1_bit39_32()
{
	return chd_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_x_bit39_32_ave_sum_1_2_bit39_32()
{
	return chd_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32;
}
MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_x_bit39_32_reserved()
{
	return chd_ave_sum_x_bit39_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchd_ave_sum_x_bit39_32()
{
	return chd_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_n_bits31_0_vrms_sum_n_bits31_0()
{
	return chd_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_n_bits31_0()
{
	return chd_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_1_1_bits31_0_vrms_sum_1_1_bits31_0()
{
	return chd_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_1_1_bits31_0()
{
	return chd_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_1_2_bits31_0_vrms_sum_1_2_bits31_0()
{
	return chd_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0;
}

MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_1_2_bits31_0()
{
	return chd_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_n_1_bits47_32_vrms_sum_n_bits47_32()
{
	return chd_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_n_1_bits47_32_vrms_sum_1_1_bits47_32()
{
	return chd_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32;
}

MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_n_1_bits47_32()
{
	return chd_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_2_bits47_32_vrms_sum_1_2_bits47_32()
{
	return chd_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32;
}
MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_2_bits47_32_reserved()
{
	return chd_vrms_sum_2_bits47_32.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchd_vrms_sum_2_bits47_32()
{
	return chd_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_total_rf_num_total_rf_num()
{
	return chd_total_rf_num.total_rf_num;
}
MeasRetu_reg MeasRetuMemProxy::getchd_total_rf_num_slope()
{
	return chd_total_rf_num.slope;
}
MeasRetu_reg MeasRetuMemProxy::getchd_total_rf_num_valid()
{
	return chd_total_rf_num.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchd_total_rf_num()
{
	return chd_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMemProxy::getreservd_x4()
{
	return reservd_x4;
}


MeasRetu_reg MeasRetuMemProxy::getchd_shoot_over_shoot()
{
	return chd_shoot.over_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getchd_shoot_pre_shoot()
{
	return chd_shoot.pre_shoot;
}
MeasRetu_reg MeasRetuMemProxy::getchd_shoot_reserved()
{
	return chd_shoot.reserved;
}

MeasRetu_reg MeasRetuMemProxy::getchd_shoot()
{
	return chd_shoot.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start_xloc_edge_cnt_start_xloc()
{
	return chd_edge_cnt_start_xloc.edge_cnt_start_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start_xloc_reserved()
{
	return chd_edge_cnt_start_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start_xloc_valid()
{
	return chd_edge_cnt_start_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start_xloc()
{
	return chd_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end_xloc_edge_cnt_end_xloc()
{
	return chd_edge_cnt_end_xloc.edge_cnt_end_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end_xloc_reserved()
{
	return chd_edge_cnt_end_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end_xloc_valid()
{
	return chd_edge_cnt_end_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end_xloc()
{
	return chd_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start2_xloc_edge_cnt_start2_xloc()
{
	return chd_edge_cnt_start2_xloc.edge_cnt_start2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start2_xloc_reserved()
{
	return chd_edge_cnt_start2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start2_xloc_valid()
{
	return chd_edge_cnt_start2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_start2_xloc()
{
	return chd_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end2_xloc_edge_cnt_end2_xloc()
{
	return chd_edge_cnt_end2_xloc.edge_cnt_end2_xloc;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end2_xloc_reserved()
{
	return chd_edge_cnt_end2_xloc.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end2_xloc_valid()
{
	return chd_edge_cnt_end2_xloc.valid;
}

MeasRetu_reg MeasRetuMemProxy::getchd_edge_cnt_end2_xloc()
{
	return chd_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMemProxy::getmeas_version_rd_meas_version()
{
	return meas_version_rd.meas_version;
}

MeasRetu_reg MeasRetuMemProxy::getmeas_version_rd()
{
	return meas_version_rd.payload;
}


MeasRetu_reg MeasRetuMemProxy::getmeas_result_done_meas_result_done()
{
	return meas_result_done.meas_result_done;
}
MeasRetu_reg MeasRetuMemProxy::getmeas_result_done_reserved()
{
	return meas_result_done.reserved;
}
MeasRetu_reg MeasRetuMemProxy::getmeas_result_done_rw_test_reg()
{
	return meas_result_done.rw_test_reg;
}

MeasRetu_reg MeasRetuMemProxy::getmeas_result_done()
{
	return meas_result_done.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_4_la0_cycle_xloc_4()
{
	return la0_cycle_xloc_4.la0_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_4_la0_edge_type()
{
	return la0_cycle_xloc_4.la0_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_4_la0_cycle_xloc_4_vld()
{
	return la0_cycle_xloc_4.la0_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_4()
{
	return la0_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_3_la0_cycle_xloc_3()
{
	return la0_cycle_xloc_3.la0_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_3_la0_edge_type()
{
	return la0_cycle_xloc_3.la0_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_3_la0_cycle_xloc_3_vld()
{
	return la0_cycle_xloc_3.la0_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_3()
{
	return la0_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_2_la0_cycle_xloc_2()
{
	return la0_cycle_xloc_2.la0_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_2_la0_edge_type()
{
	return la0_cycle_xloc_2.la0_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_2_la0_cycle_xloc_2_vld()
{
	return la0_cycle_xloc_2.la0_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_2()
{
	return la0_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_1_la0_cycle_xloc_1()
{
	return la0_cycle_xloc_1.la0_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_1_la0_edge_type()
{
	return la0_cycle_xloc_1.la0_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_1_la0_cycle_xloc_1_vld()
{
	return la0_cycle_xloc_1.la0_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla0_cycle_xloc_1()
{
	return la0_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_4_la1_cycle_xloc_4()
{
	return la1_cycle_xloc_4.la1_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_4_la1_edge_type()
{
	return la1_cycle_xloc_4.la1_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_4_la1_cycle_xloc_4_vld()
{
	return la1_cycle_xloc_4.la1_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_4()
{
	return la1_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_3_la1_cycle_xloc_3()
{
	return la1_cycle_xloc_3.la1_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_3_la1_edge_type()
{
	return la1_cycle_xloc_3.la1_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_3_la1_cycle_xloc_3_vld()
{
	return la1_cycle_xloc_3.la1_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_3()
{
	return la1_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_2_la1_cycle_xloc_2()
{
	return la1_cycle_xloc_2.la1_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_2_la1_edge_type()
{
	return la1_cycle_xloc_2.la1_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_2_la1_cycle_xloc_2_vld()
{
	return la1_cycle_xloc_2.la1_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_2()
{
	return la1_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_1_la1_cycle_xloc_1()
{
	return la1_cycle_xloc_1.la1_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_1_la1_edge_type()
{
	return la1_cycle_xloc_1.la1_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_1_la1_cycle_xloc_1_vld()
{
	return la1_cycle_xloc_1.la1_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla1_cycle_xloc_1()
{
	return la1_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_4_la2_cycle_xloc_4()
{
	return la2_cycle_xloc_4.la2_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_4_la2_edge_type()
{
	return la2_cycle_xloc_4.la2_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_4_la2_cycle_xloc_4_vld()
{
	return la2_cycle_xloc_4.la2_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_4()
{
	return la2_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_3_la2_cycle_xloc_3()
{
	return la2_cycle_xloc_3.la2_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_3_la2_edge_type()
{
	return la2_cycle_xloc_3.la2_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_3_la2_cycle_xloc_3_vld()
{
	return la2_cycle_xloc_3.la2_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_3()
{
	return la2_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_2_la2_cycle_xloc_2()
{
	return la2_cycle_xloc_2.la2_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_2_la2_edge_type()
{
	return la2_cycle_xloc_2.la2_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_2_la2_cycle_xloc_2_vld()
{
	return la2_cycle_xloc_2.la2_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_2()
{
	return la2_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_1_la2_cycle_xloc_1()
{
	return la2_cycle_xloc_1.la2_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_1_la2_edge_type()
{
	return la2_cycle_xloc_1.la2_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_1_la2_cycle_xloc_1_vld()
{
	return la2_cycle_xloc_1.la2_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla2_cycle_xloc_1()
{
	return la2_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_4_la3_cycle_xloc_4()
{
	return la3_cycle_xloc_4.la3_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_4_la3_edge_type()
{
	return la3_cycle_xloc_4.la3_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_4_la3_cycle_xloc_4_vld()
{
	return la3_cycle_xloc_4.la3_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_4()
{
	return la3_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_3_la3_cycle_xloc_3()
{
	return la3_cycle_xloc_3.la3_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_3_la3_edge_type()
{
	return la3_cycle_xloc_3.la3_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_3_la3_cycle_xloc_3_vld()
{
	return la3_cycle_xloc_3.la3_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_3()
{
	return la3_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_2_la3_cycle_xloc_2()
{
	return la3_cycle_xloc_2.la3_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_2_la3_edge_type()
{
	return la3_cycle_xloc_2.la3_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_2_la3_cycle_xloc_2_vld()
{
	return la3_cycle_xloc_2.la3_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_2()
{
	return la3_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_1_la3_cycle_xloc_1()
{
	return la3_cycle_xloc_1.la3_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_1_la3_edge_type()
{
	return la3_cycle_xloc_1.la3_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_1_la3_cycle_xloc_1_vld()
{
	return la3_cycle_xloc_1.la3_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla3_cycle_xloc_1()
{
	return la3_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_4_la4_cycle_xloc_4()
{
	return la4_cycle_xloc_4.la4_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_4_la4_edge_type()
{
	return la4_cycle_xloc_4.la4_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_4_la4_cycle_xloc_4_vld()
{
	return la4_cycle_xloc_4.la4_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_4()
{
	return la4_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_3_la4_cycle_xloc_3()
{
	return la4_cycle_xloc_3.la4_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_3_la4_edge_type()
{
	return la4_cycle_xloc_3.la4_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_3_la4_cycle_xloc_3_vld()
{
	return la4_cycle_xloc_3.la4_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_3()
{
	return la4_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_2_la4_cycle_xloc_2()
{
	return la4_cycle_xloc_2.la4_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_2_la4_edge_type()
{
	return la4_cycle_xloc_2.la4_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_2_la4_cycle_xloc_2_vld()
{
	return la4_cycle_xloc_2.la4_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_2()
{
	return la4_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_1_la4_cycle_xloc_1()
{
	return la4_cycle_xloc_1.la4_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_1_la4_edge_type()
{
	return la4_cycle_xloc_1.la4_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_1_la4_cycle_xloc_1_vld()
{
	return la4_cycle_xloc_1.la4_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla4_cycle_xloc_1()
{
	return la4_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_4_la5_cycle_xloc_4()
{
	return la5_cycle_xloc_4.la5_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_4_la5_edge_type()
{
	return la5_cycle_xloc_4.la5_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_4_la5_cycle_xloc_4_vld()
{
	return la5_cycle_xloc_4.la5_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_4()
{
	return la5_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_3_la5_cycle_xloc_3()
{
	return la5_cycle_xloc_3.la5_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_3_la5_edge_type()
{
	return la5_cycle_xloc_3.la5_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_3_la5_cycle_xloc_3_vld()
{
	return la5_cycle_xloc_3.la5_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_3()
{
	return la5_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_2_la5_cycle_xloc_2()
{
	return la5_cycle_xloc_2.la5_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_2_la5_edge_type()
{
	return la5_cycle_xloc_2.la5_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_2_la5_cycle_xloc_2_vld()
{
	return la5_cycle_xloc_2.la5_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_2()
{
	return la5_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_1_la5_cycle_xloc_1()
{
	return la5_cycle_xloc_1.la5_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_1_la5_edge_type()
{
	return la5_cycle_xloc_1.la5_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_1_la5_cycle_xloc_1_vld()
{
	return la5_cycle_xloc_1.la5_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla5_cycle_xloc_1()
{
	return la5_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_4_la6_cycle_xloc_4()
{
	return la6_cycle_xloc_4.la6_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_4_la6_edge_type()
{
	return la6_cycle_xloc_4.la6_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_4_la6_cycle_xloc_4_vld()
{
	return la6_cycle_xloc_4.la6_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_4()
{
	return la6_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_3_la6_cycle_xloc_3()
{
	return la6_cycle_xloc_3.la6_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_3_la6_edge_type()
{
	return la6_cycle_xloc_3.la6_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_3_la6_cycle_xloc_3_vld()
{
	return la6_cycle_xloc_3.la6_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_3()
{
	return la6_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_2_la6_cycle_xloc_2()
{
	return la6_cycle_xloc_2.la6_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_2_la6_edge_type()
{
	return la6_cycle_xloc_2.la6_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_2_la6_cycle_xloc_2_vld()
{
	return la6_cycle_xloc_2.la6_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_2()
{
	return la6_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_1_la6_cycle_xloc_1()
{
	return la6_cycle_xloc_1.la6_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_1_la6_edge_type()
{
	return la6_cycle_xloc_1.la6_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_1_la6_cycle_xloc_1_vld()
{
	return la6_cycle_xloc_1.la6_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla6_cycle_xloc_1()
{
	return la6_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_4_la7_cycle_xloc_4()
{
	return la7_cycle_xloc_4.la7_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_4_la7_edge_type()
{
	return la7_cycle_xloc_4.la7_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_4_la7_cycle_xloc_4_vld()
{
	return la7_cycle_xloc_4.la7_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_4()
{
	return la7_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_3_la7_cycle_xloc_3()
{
	return la7_cycle_xloc_3.la7_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_3_la7_edge_type()
{
	return la7_cycle_xloc_3.la7_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_3_la7_cycle_xloc_3_vld()
{
	return la7_cycle_xloc_3.la7_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_3()
{
	return la7_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_2_la7_cycle_xloc_2()
{
	return la7_cycle_xloc_2.la7_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_2_la7_edge_type()
{
	return la7_cycle_xloc_2.la7_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_2_la7_cycle_xloc_2_vld()
{
	return la7_cycle_xloc_2.la7_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_2()
{
	return la7_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_1_la7_cycle_xloc_1()
{
	return la7_cycle_xloc_1.la7_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_1_la7_edge_type()
{
	return la7_cycle_xloc_1.la7_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_1_la7_cycle_xloc_1_vld()
{
	return la7_cycle_xloc_1.la7_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla7_cycle_xloc_1()
{
	return la7_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_4_la8_cycle_xloc_4()
{
	return la8_cycle_xloc_4.la8_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_4_la8_edge_type()
{
	return la8_cycle_xloc_4.la8_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_4_la8_cycle_xloc_4_vld()
{
	return la8_cycle_xloc_4.la8_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_4()
{
	return la8_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_3_la8_cycle_xloc_3()
{
	return la8_cycle_xloc_3.la8_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_3_la8_edge_type()
{
	return la8_cycle_xloc_3.la8_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_3_la8_cycle_xloc_3_vld()
{
	return la8_cycle_xloc_3.la8_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_3()
{
	return la8_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_2_la8_cycle_xloc_2()
{
	return la8_cycle_xloc_2.la8_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_2_la8_edge_type()
{
	return la8_cycle_xloc_2.la8_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_2_la8_cycle_xloc_2_vld()
{
	return la8_cycle_xloc_2.la8_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_2()
{
	return la8_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_1_la8_cycle_xloc_1()
{
	return la8_cycle_xloc_1.la8_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_1_la8_edge_type()
{
	return la8_cycle_xloc_1.la8_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_1_la8_cycle_xloc_1_vld()
{
	return la8_cycle_xloc_1.la8_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla8_cycle_xloc_1()
{
	return la8_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_4_la9_cycle_xloc_4()
{
	return la9_cycle_xloc_4.la9_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_4_la9_edge_type()
{
	return la9_cycle_xloc_4.la9_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_4_la9_cycle_xloc_4_vld()
{
	return la9_cycle_xloc_4.la9_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_4()
{
	return la9_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_3_la9_cycle_xloc_3()
{
	return la9_cycle_xloc_3.la9_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_3_la9_edge_type()
{
	return la9_cycle_xloc_3.la9_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_3_la9_cycle_xloc_3_vld()
{
	return la9_cycle_xloc_3.la9_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_3()
{
	return la9_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_2_la9_cycle_xloc_2()
{
	return la9_cycle_xloc_2.la9_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_2_la9_edge_type()
{
	return la9_cycle_xloc_2.la9_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_2_la9_cycle_xloc_2_vld()
{
	return la9_cycle_xloc_2.la9_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_2()
{
	return la9_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_1_la9_cycle_xloc_1()
{
	return la9_cycle_xloc_1.la9_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_1_la9_edge_type()
{
	return la9_cycle_xloc_1.la9_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_1_la9_cycle_xloc_1_vld()
{
	return la9_cycle_xloc_1.la9_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla9_cycle_xloc_1()
{
	return la9_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_4_la10_cycle_xloc_4()
{
	return la10_cycle_xloc_4.la10_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_4_la10_edge_type()
{
	return la10_cycle_xloc_4.la10_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_4_la10_cycle_xloc_4_vld()
{
	return la10_cycle_xloc_4.la10_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_4()
{
	return la10_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_3_la10_cycle_xloc_3()
{
	return la10_cycle_xloc_3.la10_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_3_la10_edge_type()
{
	return la10_cycle_xloc_3.la10_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_3_la10_cycle_xloc_3_vld()
{
	return la10_cycle_xloc_3.la10_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_3()
{
	return la10_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_2_la10_cycle_xloc_2()
{
	return la10_cycle_xloc_2.la10_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_2_la10_edge_type()
{
	return la10_cycle_xloc_2.la10_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_2_la10_cycle_xloc_2_vld()
{
	return la10_cycle_xloc_2.la10_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_2()
{
	return la10_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_1_la10_cycle_xloc_1()
{
	return la10_cycle_xloc_1.la10_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_1_la10_edge_type()
{
	return la10_cycle_xloc_1.la10_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_1_la10_cycle_xloc_1_vld()
{
	return la10_cycle_xloc_1.la10_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla10_cycle_xloc_1()
{
	return la10_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_4_la11_cycle_xloc_4()
{
	return la11_cycle_xloc_4.la11_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_4_la11_edge_type()
{
	return la11_cycle_xloc_4.la11_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_4_la11_cycle_xloc_4_vld()
{
	return la11_cycle_xloc_4.la11_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_4()
{
	return la11_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_3_la11_cycle_xloc_3()
{
	return la11_cycle_xloc_3.la11_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_3_la11_edge_type()
{
	return la11_cycle_xloc_3.la11_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_3_la11_cycle_xloc_3_vld()
{
	return la11_cycle_xloc_3.la11_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_3()
{
	return la11_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_2_la11_cycle_xloc_2()
{
	return la11_cycle_xloc_2.la11_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_2_la11_edge_type()
{
	return la11_cycle_xloc_2.la11_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_2_la11_cycle_xloc_2_vld()
{
	return la11_cycle_xloc_2.la11_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_2()
{
	return la11_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_1_la11_cycle_xloc_1()
{
	return la11_cycle_xloc_1.la11_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_1_la11_edge_type()
{
	return la11_cycle_xloc_1.la11_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_1_la11_cycle_xloc_1_vld()
{
	return la11_cycle_xloc_1.la11_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla11_cycle_xloc_1()
{
	return la11_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_4_la12_cycle_xloc_4()
{
	return la12_cycle_xloc_4.la12_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_4_la12_edge_type()
{
	return la12_cycle_xloc_4.la12_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_4_la12_cycle_xloc_4_vld()
{
	return la12_cycle_xloc_4.la12_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_4()
{
	return la12_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_3_la12_cycle_xloc_3()
{
	return la12_cycle_xloc_3.la12_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_3_la12_edge_type()
{
	return la12_cycle_xloc_3.la12_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_3_la12_cycle_xloc_3_vld()
{
	return la12_cycle_xloc_3.la12_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_3()
{
	return la12_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_2_la12_cycle_xloc_2()
{
	return la12_cycle_xloc_2.la12_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_2_la12_edge_type()
{
	return la12_cycle_xloc_2.la12_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_2_la12_cycle_xloc_2_vld()
{
	return la12_cycle_xloc_2.la12_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_2()
{
	return la12_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_1_la12_cycle_xloc_1()
{
	return la12_cycle_xloc_1.la12_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_1_la12_edge_type()
{
	return la12_cycle_xloc_1.la12_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_1_la12_cycle_xloc_1_vld()
{
	return la12_cycle_xloc_1.la12_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla12_cycle_xloc_1()
{
	return la12_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_4_la13_cycle_xloc_4()
{
	return la13_cycle_xloc_4.la13_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_4_la13_edge_type()
{
	return la13_cycle_xloc_4.la13_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_4_la13_cycle_xloc_4_vld()
{
	return la13_cycle_xloc_4.la13_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_4()
{
	return la13_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_3_la13_cycle_xloc_3()
{
	return la13_cycle_xloc_3.la13_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_3_la13_edge_type()
{
	return la13_cycle_xloc_3.la13_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_3_la13_cycle_xloc_3_vld()
{
	return la13_cycle_xloc_3.la13_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_3()
{
	return la13_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_2_la13_cycle_xloc_2()
{
	return la13_cycle_xloc_2.la13_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_2_la13_edge_type()
{
	return la13_cycle_xloc_2.la13_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_2_la13_cycle_xloc_2_vld()
{
	return la13_cycle_xloc_2.la13_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_2()
{
	return la13_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_1_la13_cycle_xloc_1()
{
	return la13_cycle_xloc_1.la13_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_1_la13_edge_type()
{
	return la13_cycle_xloc_1.la13_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_1_la13_cycle_xloc_1_vld()
{
	return la13_cycle_xloc_1.la13_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla13_cycle_xloc_1()
{
	return la13_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_4_la14_cycle_xloc_4()
{
	return la14_cycle_xloc_4.la14_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_4_la14_edge_type()
{
	return la14_cycle_xloc_4.la14_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_4_la14_cycle_xloc_4_vld()
{
	return la14_cycle_xloc_4.la14_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_4()
{
	return la14_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_3_la14_cycle_xloc_3()
{
	return la14_cycle_xloc_3.la14_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_3_la14_edge_type()
{
	return la14_cycle_xloc_3.la14_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_3_la14_cycle_xloc_3_vld()
{
	return la14_cycle_xloc_3.la14_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_3()
{
	return la14_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_2_la14_cycle_xloc_2()
{
	return la14_cycle_xloc_2.la14_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_2_la14_edge_type()
{
	return la14_cycle_xloc_2.la14_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_2_la14_cycle_xloc_2_vld()
{
	return la14_cycle_xloc_2.la14_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_2()
{
	return la14_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_1_la14_cycle_xloc_1()
{
	return la14_cycle_xloc_1.la14_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_1_la14_edge_type()
{
	return la14_cycle_xloc_1.la14_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_1_la14_cycle_xloc_1_vld()
{
	return la14_cycle_xloc_1.la14_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla14_cycle_xloc_1()
{
	return la14_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_4_la15_cycle_xloc_4()
{
	return la15_cycle_xloc_4.la15_cycle_xloc_4;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_4_la15_edge_type()
{
	return la15_cycle_xloc_4.la15_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_4_la15_cycle_xloc_4_vld()
{
	return la15_cycle_xloc_4.la15_cycle_xloc_4_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_4()
{
	return la15_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_3_la15_cycle_xloc_3()
{
	return la15_cycle_xloc_3.la15_cycle_xloc_3;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_3_la15_edge_type()
{
	return la15_cycle_xloc_3.la15_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_3_la15_cycle_xloc_3_vld()
{
	return la15_cycle_xloc_3.la15_cycle_xloc_3_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_3()
{
	return la15_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_2_la15_cycle_xloc_2()
{
	return la15_cycle_xloc_2.la15_cycle_xloc_2;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_2_la15_edge_type()
{
	return la15_cycle_xloc_2.la15_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_2_la15_cycle_xloc_2_vld()
{
	return la15_cycle_xloc_2.la15_cycle_xloc_2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_2()
{
	return la15_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_1_la15_cycle_xloc_1()
{
	return la15_cycle_xloc_1.la15_cycle_xloc_1;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_1_la15_edge_type()
{
	return la15_cycle_xloc_1.la15_edge_type;
}
MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_1_la15_cycle_xloc_1_vld()
{
	return la15_cycle_xloc_1.la15_cycle_xloc_1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getla15_cycle_xloc_1()
{
	return la15_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult1_from_xloc2_dlyresult1_from_xloc2()
{
	return dlyresult1_from_xloc2.dlyresult1_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult1_from_xloc2_dlyresult1_from_xloc2_vld()
{
	return dlyresult1_from_xloc2.dlyresult1_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult1_from_xloc2()
{
	return dlyresult1_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult1_from_xloc1_dlyresult1_from_xloc1()
{
	return dlyresult1_from_xloc1.dlyresult1_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult1_from_xloc1_dlyresult1_from_xloc1_vld()
{
	return dlyresult1_from_xloc1.dlyresult1_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult1_from_xloc1()
{
	return dlyresult1_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre()
{
	return dlyresult1_to_xloc2_pre.dlyresult1_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc2_pre_dlyresult1_to_xloc2_pre_vld()
{
	return dlyresult1_to_xloc2_pre.dlyresult1_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc2_pre()
{
	return dlyresult1_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc2_dlyresult1_to_xloc2()
{
	return dlyresult1_to_xloc2.dlyresult1_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc2_dlyresult1_to_xloc2_vld()
{
	return dlyresult1_to_xloc2.dlyresult1_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc2()
{
	return dlyresult1_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre()
{
	return dlyresult1_to_xloc1_pre.dlyresult1_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc1_pre_dlyresult1_to_xloc1_pre_vld()
{
	return dlyresult1_to_xloc1_pre.dlyresult1_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc1_pre()
{
	return dlyresult1_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc1_dlyresult1_to_xloc1()
{
	return dlyresult1_to_xloc1.dlyresult1_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc1_dlyresult1_to_xloc1_vld()
{
	return dlyresult1_to_xloc1.dlyresult1_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult1_to_xloc1()
{
	return dlyresult1_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult2_from_xloc2_dlyresult2_from_xloc2()
{
	return dlyresult2_from_xloc2.dlyresult2_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult2_from_xloc2_dlyresult2_from_xloc2_vld()
{
	return dlyresult2_from_xloc2.dlyresult2_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult2_from_xloc2()
{
	return dlyresult2_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult2_from_xloc1_dlyresult2_from_xloc1()
{
	return dlyresult2_from_xloc1.dlyresult2_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult2_from_xloc1_dlyresult2_from_xloc1_vld()
{
	return dlyresult2_from_xloc1.dlyresult2_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult2_from_xloc1()
{
	return dlyresult2_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc2_pre_dlyresult2_to_xloc2_pre()
{
	return dlyresult2_to_xloc2_pre.dlyresult2_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc2_pre_dlyresult2_to_xloc2_pre_vld()
{
	return dlyresult2_to_xloc2_pre.dlyresult2_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc2_pre()
{
	return dlyresult2_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc2_dlyresult2_to_xloc2()
{
	return dlyresult2_to_xloc2.dlyresult2_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc2_dlyresult2_to_xloc2_vld()
{
	return dlyresult2_to_xloc2.dlyresult2_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc2()
{
	return dlyresult2_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc1_pre_dlyresult2_to_xloc1_pre()
{
	return dlyresult2_to_xloc1_pre.dlyresult2_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc1_pre_dlyresult2_to_xloc1_pre_vld()
{
	return dlyresult2_to_xloc1_pre.dlyresult2_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc1_pre()
{
	return dlyresult2_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc1_dlyresult2_to_xloc1()
{
	return dlyresult2_to_xloc1.dlyresult2_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc1_dlyresult2_to_xloc1_vld()
{
	return dlyresult2_to_xloc1.dlyresult2_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult2_to_xloc1()
{
	return dlyresult2_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult3_from_xloc2_dlyresult3_from_xloc2()
{
	return dlyresult3_from_xloc2.dlyresult3_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult3_from_xloc2_dlyresult3_from_xloc2_vld()
{
	return dlyresult3_from_xloc2.dlyresult3_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult3_from_xloc2()
{
	return dlyresult3_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult3_from_xloc1_dlyresult3_from_xloc1()
{
	return dlyresult3_from_xloc1.dlyresult3_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult3_from_xloc1_dlyresult3_from_xloc1_vld()
{
	return dlyresult3_from_xloc1.dlyresult3_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult3_from_xloc1()
{
	return dlyresult3_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc2_pre_dlyresult3_to_xloc2_pre()
{
	return dlyresult3_to_xloc2_pre.dlyresult3_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc2_pre_dlyresult3_to_xloc2_pre_vld()
{
	return dlyresult3_to_xloc2_pre.dlyresult3_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc2_pre()
{
	return dlyresult3_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc2_dlyresult3_to_xloc2()
{
	return dlyresult3_to_xloc2.dlyresult3_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc2_dlyresult3_to_xloc2_vld()
{
	return dlyresult3_to_xloc2.dlyresult3_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc2()
{
	return dlyresult3_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc1_pre_dlyresult3_to_xloc1_pre()
{
	return dlyresult3_to_xloc1_pre.dlyresult3_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc1_pre_dlyresult3_to_xloc1_pre_vld()
{
	return dlyresult3_to_xloc1_pre.dlyresult3_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc1_pre()
{
	return dlyresult3_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc1_dlyresult3_to_xloc1()
{
	return dlyresult3_to_xloc1.dlyresult3_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc1_dlyresult3_to_xloc1_vld()
{
	return dlyresult3_to_xloc1.dlyresult3_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult3_to_xloc1()
{
	return dlyresult3_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult4_from_xloc2_dlyresult4_from_xloc2()
{
	return dlyresult4_from_xloc2.dlyresult4_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult4_from_xloc2_dlyresult4_from_xloc2_vld()
{
	return dlyresult4_from_xloc2.dlyresult4_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult4_from_xloc2()
{
	return dlyresult4_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult4_from_xloc1_dlyresult4_from_xloc1()
{
	return dlyresult4_from_xloc1.dlyresult4_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult4_from_xloc1_dlyresult4_from_xloc1_vld()
{
	return dlyresult4_from_xloc1.dlyresult4_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult4_from_xloc1()
{
	return dlyresult4_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc2_pre_dlyresult4_to_xloc2_pre()
{
	return dlyresult4_to_xloc2_pre.dlyresult4_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc2_pre_dlyresult4_to_xloc2_pre_vld()
{
	return dlyresult4_to_xloc2_pre.dlyresult4_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc2_pre()
{
	return dlyresult4_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc2_dlyresult4_to_xloc2()
{
	return dlyresult4_to_xloc2.dlyresult4_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc2_dlyresult4_to_xloc2_vld()
{
	return dlyresult4_to_xloc2.dlyresult4_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc2()
{
	return dlyresult4_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc1_pre_dlyresult4_to_xloc1_pre()
{
	return dlyresult4_to_xloc1_pre.dlyresult4_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc1_pre_dlyresult4_to_xloc1_pre_vld()
{
	return dlyresult4_to_xloc1_pre.dlyresult4_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc1_pre()
{
	return dlyresult4_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc1_dlyresult4_to_xloc1()
{
	return dlyresult4_to_xloc1.dlyresult4_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc1_dlyresult4_to_xloc1_vld()
{
	return dlyresult4_to_xloc1.dlyresult4_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult4_to_xloc1()
{
	return dlyresult4_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult5_from_xloc2_dlyresult5_from_xloc2()
{
	return dlyresult5_from_xloc2.dlyresult5_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult5_from_xloc2_dlyresult5_from_xloc2_vld()
{
	return dlyresult5_from_xloc2.dlyresult5_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult5_from_xloc2()
{
	return dlyresult5_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult5_from_xloc1_dlyresult5_from_xloc1()
{
	return dlyresult5_from_xloc1.dlyresult5_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult5_from_xloc1_dlyresult5_from_xloc1_vld()
{
	return dlyresult5_from_xloc1.dlyresult5_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult5_from_xloc1()
{
	return dlyresult5_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc2_pre_dlyresult5_to_xloc2_pre()
{
	return dlyresult5_to_xloc2_pre.dlyresult5_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc2_pre_dlyresult5_to_xloc2_pre_vld()
{
	return dlyresult5_to_xloc2_pre.dlyresult5_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc2_pre()
{
	return dlyresult5_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc2_dlyresult5_to_xloc2()
{
	return dlyresult5_to_xloc2.dlyresult5_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc2_dlyresult5_to_xloc2_vld()
{
	return dlyresult5_to_xloc2.dlyresult5_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc2()
{
	return dlyresult5_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc1_pre_dlyresult5_to_xloc1_pre()
{
	return dlyresult5_to_xloc1_pre.dlyresult5_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc1_pre_dlyresult5_to_xloc1_pre_vld()
{
	return dlyresult5_to_xloc1_pre.dlyresult5_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc1_pre()
{
	return dlyresult5_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc1_dlyresult5_to_xloc1()
{
	return dlyresult5_to_xloc1.dlyresult5_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc1_dlyresult5_to_xloc1_vld()
{
	return dlyresult5_to_xloc1.dlyresult5_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult5_to_xloc1()
{
	return dlyresult5_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult6_from_xloc2_dlyresult6_from_xloc2()
{
	return dlyresult6_from_xloc2.dlyresult6_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult6_from_xloc2_dlyresult6_from_xloc2_vld()
{
	return dlyresult6_from_xloc2.dlyresult6_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult6_from_xloc2()
{
	return dlyresult6_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult6_from_xloc1_dlyresult6_from_xloc1()
{
	return dlyresult6_from_xloc1.dlyresult6_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult6_from_xloc1_dlyresult6_from_xloc1_vld()
{
	return dlyresult6_from_xloc1.dlyresult6_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult6_from_xloc1()
{
	return dlyresult6_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc2_pre_dlyresult6_to_xloc2_pre()
{
	return dlyresult6_to_xloc2_pre.dlyresult6_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc2_pre_dlyresult6_to_xloc2_pre_vld()
{
	return dlyresult6_to_xloc2_pre.dlyresult6_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc2_pre()
{
	return dlyresult6_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc2_dlyresult6_to_xloc2()
{
	return dlyresult6_to_xloc2.dlyresult6_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc2_dlyresult6_to_xloc2_vld()
{
	return dlyresult6_to_xloc2.dlyresult6_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc2()
{
	return dlyresult6_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc1_pre_dlyresult6_to_xloc1_pre()
{
	return dlyresult6_to_xloc1_pre.dlyresult6_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc1_pre_dlyresult6_to_xloc1_pre_vld()
{
	return dlyresult6_to_xloc1_pre.dlyresult6_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc1_pre()
{
	return dlyresult6_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc1_dlyresult6_to_xloc1()
{
	return dlyresult6_to_xloc1.dlyresult6_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc1_dlyresult6_to_xloc1_vld()
{
	return dlyresult6_to_xloc1.dlyresult6_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult6_to_xloc1()
{
	return dlyresult6_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult7_from_xloc2_dlyresult7_from_xloc2()
{
	return dlyresult7_from_xloc2.dlyresult7_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult7_from_xloc2_dlyresult7_from_xloc2_vld()
{
	return dlyresult7_from_xloc2.dlyresult7_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult7_from_xloc2()
{
	return dlyresult7_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult7_from_xloc1_dlyresult7_from_xloc1()
{
	return dlyresult7_from_xloc1.dlyresult7_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult7_from_xloc1_dlyresult7_from_xloc1_vld()
{
	return dlyresult7_from_xloc1.dlyresult7_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult7_from_xloc1()
{
	return dlyresult7_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc2_pre_dlyresult7_to_xloc2_pre()
{
	return dlyresult7_to_xloc2_pre.dlyresult7_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc2_pre_dlyresult7_to_xloc2_pre_vld()
{
	return dlyresult7_to_xloc2_pre.dlyresult7_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc2_pre()
{
	return dlyresult7_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc2_dlyresult7_to_xloc2()
{
	return dlyresult7_to_xloc2.dlyresult7_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc2_dlyresult7_to_xloc2_vld()
{
	return dlyresult7_to_xloc2.dlyresult7_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc2()
{
	return dlyresult7_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc1_pre_dlyresult7_to_xloc1_pre()
{
	return dlyresult7_to_xloc1_pre.dlyresult7_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc1_pre_dlyresult7_to_xloc1_pre_vld()
{
	return dlyresult7_to_xloc1_pre.dlyresult7_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc1_pre()
{
	return dlyresult7_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc1_dlyresult7_to_xloc1()
{
	return dlyresult7_to_xloc1.dlyresult7_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc1_dlyresult7_to_xloc1_vld()
{
	return dlyresult7_to_xloc1.dlyresult7_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult7_to_xloc1()
{
	return dlyresult7_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult8_from_xloc2_dlyresult8_from_xloc2()
{
	return dlyresult8_from_xloc2.dlyresult8_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult8_from_xloc2_dlyresult8_from_xloc2_vld()
{
	return dlyresult8_from_xloc2.dlyresult8_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult8_from_xloc2()
{
	return dlyresult8_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult8_from_xloc1_dlyresult8_from_xloc1()
{
	return dlyresult8_from_xloc1.dlyresult8_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult8_from_xloc1_dlyresult8_from_xloc1_vld()
{
	return dlyresult8_from_xloc1.dlyresult8_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult8_from_xloc1()
{
	return dlyresult8_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc2_pre_dlyresult8_to_xloc2_pre()
{
	return dlyresult8_to_xloc2_pre.dlyresult8_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc2_pre_dlyresult8_to_xloc2_pre_vld()
{
	return dlyresult8_to_xloc2_pre.dlyresult8_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc2_pre()
{
	return dlyresult8_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc2_dlyresult8_to_xloc2()
{
	return dlyresult8_to_xloc2.dlyresult8_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc2_dlyresult8_to_xloc2_vld()
{
	return dlyresult8_to_xloc2.dlyresult8_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc2()
{
	return dlyresult8_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc1_pre_dlyresult8_to_xloc1_pre()
{
	return dlyresult8_to_xloc1_pre.dlyresult8_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc1_pre_dlyresult8_to_xloc1_pre_vld()
{
	return dlyresult8_to_xloc1_pre.dlyresult8_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc1_pre()
{
	return dlyresult8_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc1_dlyresult8_to_xloc1()
{
	return dlyresult8_to_xloc1.dlyresult8_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc1_dlyresult8_to_xloc1_vld()
{
	return dlyresult8_to_xloc1.dlyresult8_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult8_to_xloc1()
{
	return dlyresult8_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult9_from_xloc2_dlyresult9_from_xloc2()
{
	return dlyresult9_from_xloc2.dlyresult9_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult9_from_xloc2_dlyresult9_from_xloc2_vld()
{
	return dlyresult9_from_xloc2.dlyresult9_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult9_from_xloc2()
{
	return dlyresult9_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult9_from_xloc1_dlyresult9_from_xloc1()
{
	return dlyresult9_from_xloc1.dlyresult9_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult9_from_xloc1_dlyresult9_from_xloc1_vld()
{
	return dlyresult9_from_xloc1.dlyresult9_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult9_from_xloc1()
{
	return dlyresult9_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc2_pre_dlyresult9_to_xloc2_pre()
{
	return dlyresult9_to_xloc2_pre.dlyresult9_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc2_pre_dlyresult9_to_xloc2_pre_vld()
{
	return dlyresult9_to_xloc2_pre.dlyresult9_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc2_pre()
{
	return dlyresult9_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc2_dlyresult9_to_xloc2()
{
	return dlyresult9_to_xloc2.dlyresult9_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc2_dlyresult9_to_xloc2_vld()
{
	return dlyresult9_to_xloc2.dlyresult9_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc2()
{
	return dlyresult9_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc1_pre_dlyresult9_to_xloc1_pre()
{
	return dlyresult9_to_xloc1_pre.dlyresult9_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc1_pre_dlyresult9_to_xloc1_pre_vld()
{
	return dlyresult9_to_xloc1_pre.dlyresult9_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc1_pre()
{
	return dlyresult9_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc1_dlyresult9_to_xloc1()
{
	return dlyresult9_to_xloc1.dlyresult9_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc1_dlyresult9_to_xloc1_vld()
{
	return dlyresult9_to_xloc1.dlyresult9_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult9_to_xloc1()
{
	return dlyresult9_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult10_from_xloc2_dlyresult10_from_xloc2()
{
	return dlyresult10_from_xloc2.dlyresult10_from_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult10_from_xloc2_dlyresult10_from_xloc2_vld()
{
	return dlyresult10_from_xloc2.dlyresult10_from_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult10_from_xloc2()
{
	return dlyresult10_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult10_from_xloc1_dlyresult10_from_xloc1()
{
	return dlyresult10_from_xloc1.dlyresult10_from_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult10_from_xloc1_dlyresult10_from_xloc1_vld()
{
	return dlyresult10_from_xloc1.dlyresult10_from_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult10_from_xloc1()
{
	return dlyresult10_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc2_pre_dlyresult10_to_xloc2_pre()
{
	return dlyresult10_to_xloc2_pre.dlyresult10_to_xloc2_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc2_pre_dlyresult10_to_xloc2_pre_vld()
{
	return dlyresult10_to_xloc2_pre.dlyresult10_to_xloc2_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc2_pre()
{
	return dlyresult10_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc2_dlyresult10_to_xloc2()
{
	return dlyresult10_to_xloc2.dlyresult10_to_xloc2;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc2_dlyresult10_to_xloc2_vld()
{
	return dlyresult10_to_xloc2.dlyresult10_to_xloc2_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc2()
{
	return dlyresult10_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc1_pre_dlyresult10_to_xloc1_pre()
{
	return dlyresult10_to_xloc1_pre.dlyresult10_to_xloc1_pre;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc1_pre_dlyresult10_to_xloc1_pre_vld()
{
	return dlyresult10_to_xloc1_pre.dlyresult10_to_xloc1_pre_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc1_pre()
{
	return dlyresult10_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc1_dlyresult10_to_xloc1()
{
	return dlyresult10_to_xloc1.dlyresult10_to_xloc1;
}
MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc1_dlyresult10_to_xloc1_vld()
{
	return dlyresult10_to_xloc1.dlyresult10_to_xloc1_vld;
}

MeasRetu_reg MeasRetuMemProxy::getdlyresult10_to_xloc1()
{
	return dlyresult10_to_xloc1.payload;
}



void MeasRetuMem::_loadOtp()
{
	cha_max_min_top_base.payload=0x0;
		cha_max_min_top_base.cha_vmin=0x0;
		cha_max_min_top_base.cha_vmax=0x0;
		cha_max_min_top_base.cha_vbase=0x0;
		cha_max_min_top_base.cha_vtop=0x0;

	cha_threshold_l_mid_h.payload=0x0;
		cha_threshold_l_mid_h.cha_threshold_l=0x0;
		cha_threshold_l_mid_h.cha_threshold_mid=0x0;
		cha_threshold_l_mid_h.cha_threshold_h=0x0;
		cha_threshold_l_mid_h.reserved=0x0;

	cha_vmax_xloc.payload=0x0;
		cha_vmax_xloc.vmax_xloc=0x0;
		cha_vmax_xloc.reserved=0x0;
		cha_vmax_xloc.vmax_xloc_vld=0x0;

	cha_vmin_xloc.payload=0x0;
		cha_vmin_xloc.vmin_xloc=0x0;
		cha_vmin_xloc.reserved=0x0;
		cha_vmin_xloc.vmin_xloc_vld=0x0;


	cha_rtime.payload=0x0;
		cha_rtime.rtime=0x0;
		cha_rtime.reserved=0x0;
		cha_rtime.rtime_vld=0x0;

	cha_rtime_xloc_end.payload=0x0;
		cha_rtime_xloc_end.rtime_xloc_end=0x0;
		cha_rtime_xloc_end.reserved=0x0;
		cha_rtime_xloc_end.rtime_xloc_end_vld=0x0;

	cha_rtime_pre.payload=0x0;
		cha_rtime_pre.rtime_pre=0x0;
		cha_rtime_pre.reserved=0x0;
		cha_rtime_pre.rtime_pre_vld=0x0;

	cha_rtime_xloc_end_pre.payload=0x0;
		cha_rtime_xloc_end_pre.rtime_xloc_end_pre=0x0;
		cha_rtime_xloc_end_pre.reserved=0x0;
		cha_rtime_xloc_end_pre.rtime_xloc_end_pre_vld=0x0;


	cha_ftime.payload=0x0;
		cha_ftime.ftime=0x0;
		cha_ftime.reserved=0x0;
		cha_ftime.ftime_vld=0x0;

	cha_ftime_xloc_end.payload=0x0;
		cha_ftime_xloc_end.ftime_xloc_end=0x0;
		cha_ftime_xloc_end.reserved=0x0;
		cha_ftime_xloc_end.ftime_xloc_end_vld=0x0;

	cha_ftime_pre.payload=0x0;
		cha_ftime_pre.ftime_pre=0x0;
		cha_ftime_pre.reserved=0x0;
		cha_ftime_pre.ftime_pre_vld=0x0;

	cha_ftime_xloc_end_pre.payload=0x0;
		cha_ftime_xloc_end_pre.ftime_xloc_end_pre=0x0;
		cha_ftime_xloc_end_pre.reserved=0x0;
		cha_ftime_xloc_end_pre.ftime_xloc_end_pre_vld=0x0;


	cha_cycle_xloc_4.payload=0x0;
		cha_cycle_xloc_4.cycle_xloc_4=0x0;
		cha_cycle_xloc_4.edge_type=0x0;
		cha_cycle_xloc_4.cycle_xloc_4_vld=0x0;

	cha_cycle_xloc_3.payload=0x0;
		cha_cycle_xloc_3.cycle_xloc_3=0x0;
		cha_cycle_xloc_3.edge_type=0x0;
		cha_cycle_xloc_3.cycle_xloc_3_vld=0x0;

	cha_cycle_xloc_2.payload=0x0;
		cha_cycle_xloc_2.cycle_xloc_2=0x0;
		cha_cycle_xloc_2.edge_type=0x0;
		cha_cycle_xloc_2.cycle_xloc_2_vld=0x0;

	cha_cycle_xloc_1.payload=0x0;
		cha_cycle_xloc_1.cycle_xloc_1=0x0;
		cha_cycle_xloc_1.edge_type=0x0;
		cha_cycle_xloc_1.cycle_xloc_1_vld=0x0;


	cha_ave_sum_n_bits31_0.payload=0x0;
		cha_ave_sum_n_bits31_0.ave_sum_n_bit31_0=0x0;

	cha_ave_sum_1_1_bits31_0.payload=0x0;
		cha_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0=0x0;

	cha_ave_sum_1_2_bits31_0.payload=0x0;
		cha_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0=0x0;

	cha_ave_sum_x_bit39_32.payload=0x0;
		cha_ave_sum_x_bit39_32.ave_sum_n_bit39_32=0x0;
		cha_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32=0x0;
		cha_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32=0x0;
		cha_ave_sum_x_bit39_32.reserved=0x0;


	cha_vrms_sum_n_bits31_0.payload=0x0;
		cha_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0=0x0;

	cha_vrms_sum_1_1_bits31_0.payload=0x0;
		cha_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0=0x0;

	cha_vrms_sum_1_2_bits31_0.payload=0x0;
		cha_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0=0x0;

	cha_vrms_sum_n_1_bits47_32.payload=0x0;
		cha_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32=0x0;
		cha_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32=0x0;


	cha_vrms_sum_2_bits47_32.payload=0x0;
		cha_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32=0x0;
		cha_vrms_sum_2_bits47_32.reserved=0x0;

	cha_total_rf_num.payload=0x0;
		cha_total_rf_num.total_rf_num=0x0;
		cha_total_rf_num.slope=0x0;
		cha_total_rf_num.valid=0x0;

	reserved_x1=0x0;
	cha_shoot.payload=0x0;
		cha_shoot.over_shoot=0x0;
		cha_shoot.pre_shoot=0x0;
		cha_shoot.reserved=0x0;


	cha_edge_cnt_start_xloc.payload=0x0;
		cha_edge_cnt_start_xloc.edge_cnt_start_xloc=0x0;
		cha_edge_cnt_start_xloc.reserved=0x0;
		cha_edge_cnt_start_xloc.valid=0x0;

	cha_edge_cnt_end_xloc.payload=0x0;
		cha_edge_cnt_end_xloc.edge_cnt_end_xloc=0x0;
		cha_edge_cnt_end_xloc.reserved=0x0;
		cha_edge_cnt_end_xloc.valid=0x0;

	cha_edge_cnt_start2_xloc.payload=0x0;
		cha_edge_cnt_start2_xloc.edge_cnt_start2_xloc=0x0;
		cha_edge_cnt_start2_xloc.reserved=0x0;
		cha_edge_cnt_start2_xloc.valid=0x0;

	cha_edge_cnt_end2_xloc.payload=0x0;
		cha_edge_cnt_end2_xloc.edge_cnt_end2_xloc=0x0;
		cha_edge_cnt_end2_xloc.reserved=0x0;
		cha_edge_cnt_end2_xloc.valid=0x0;


	chb_max_min_top_base.payload=0x0;
		chb_max_min_top_base.cha_vmin=0x0;
		chb_max_min_top_base.cha_vmax=0x0;
		chb_max_min_top_base.cha_vbase=0x0;
		chb_max_min_top_base.cha_vtop=0x0;

	chb_threshold_l_mid_h.payload=0x0;
		chb_threshold_l_mid_h.cha_threshold_l=0x0;
		chb_threshold_l_mid_h.cha_threshold_mid=0x0;
		chb_threshold_l_mid_h.cha_threshold_h=0x0;
		chb_threshold_l_mid_h.reserved=0x0;

	chb_vmax_xloc.payload=0x0;
		chb_vmax_xloc.vmax_xloc=0x0;
		chb_vmax_xloc.reserved=0x0;
		chb_vmax_xloc.vmax_xloc_vld=0x0;

	chb_vmin_xloc.payload=0x0;
		chb_vmin_xloc.vmin_xloc=0x0;
		chb_vmin_xloc.reserved=0x0;
		chb_vmin_xloc.vmin_xloc_vld=0x0;


	chb_rtime.payload=0x0;
		chb_rtime.rtime=0x0;
		chb_rtime.reserved=0x0;
		chb_rtime.rtime_vld=0x0;

	chb_rtime_xloc_end.payload=0x0;
		chb_rtime_xloc_end.rtime_xloc_end=0x0;
		chb_rtime_xloc_end.reserved=0x0;
		chb_rtime_xloc_end.rtime_xloc_end_vld=0x0;

	chb_rtime_pre.payload=0x0;
		chb_rtime_pre.rtime_pre=0x0;
		chb_rtime_pre.reserved=0x0;
		chb_rtime_pre.rtime_pre_vld=0x0;

	chb_rtime_xloc_end_pre.payload=0x0;
		chb_rtime_xloc_end_pre.rtime_xloc_end_pre=0x0;
		chb_rtime_xloc_end_pre.reserved=0x0;
		chb_rtime_xloc_end_pre.rtime_xloc_end_pre_vld=0x0;


	chb_ftime.payload=0x0;
		chb_ftime.ftime=0x0;
		chb_ftime.reserved=0x0;
		chb_ftime.ftime_vld=0x0;

	chb_ftime_xloc_end.payload=0x0;
		chb_ftime_xloc_end.ftime_xloc_end=0x0;
		chb_ftime_xloc_end.reserved=0x0;
		chb_ftime_xloc_end.ftime_xloc_end_vld=0x0;

	chb_ftime_pre.payload=0x0;
		chb_ftime_pre.ftime_pre=0x0;
		chb_ftime_pre.reserved=0x0;
		chb_ftime_pre.ftime_pre_vld=0x0;

	chb_ftime_xloc_end_pre.payload=0x0;
		chb_ftime_xloc_end_pre.ftime_xloc_end_pre=0x0;
		chb_ftime_xloc_end_pre.reserved=0x0;
		chb_ftime_xloc_end_pre.ftime_xloc_end_pre_vld=0x0;


	chb_cycle_xloc_4.payload=0x0;
		chb_cycle_xloc_4.cycle_xloc_4=0x0;
		chb_cycle_xloc_4.edge_type=0x0;
		chb_cycle_xloc_4.cycle_xloc_4_vld=0x0;

	chb_cycle_xloc_3.payload=0x0;
		chb_cycle_xloc_3.cycle_xloc_3=0x0;
		chb_cycle_xloc_3.edge_type=0x0;
		chb_cycle_xloc_3.cycle_xloc_3_vld=0x0;

	chb_cycle_xloc_2.payload=0x0;
		chb_cycle_xloc_2.cycle_xloc_2=0x0;
		chb_cycle_xloc_2.edge_type=0x0;
		chb_cycle_xloc_2.cycle_xloc_2_vld=0x0;

	chb_cycle_xloc_1.payload=0x0;
		chb_cycle_xloc_1.cycle_xloc_1=0x0;
		chb_cycle_xloc_1.edge_type=0x0;
		chb_cycle_xloc_1.cycle_xloc_1_vld=0x0;


	chb_ave_sum_n_bits31_0.payload=0x0;
		chb_ave_sum_n_bits31_0.ave_sum_n_bit31_0=0x0;

	chb_ave_sum_1_1_bits31_0.payload=0x0;
		chb_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0=0x0;

	chb_ave_sum_1_2_bits31_0.payload=0x0;
		chb_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0=0x0;

	chb_ave_sum_x_bit39_32.payload=0x0;
		chb_ave_sum_x_bit39_32.ave_sum_n_bit39_32=0x0;
		chb_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32=0x0;
		chb_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32=0x0;
		chb_ave_sum_x_bit39_32.reserved=0x0;


	chb_vrms_sum_n_bits31_0.payload=0x0;
		chb_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0=0x0;

	chb_vrms_sum_1_1_bits31_0.payload=0x0;
		chb_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0=0x0;

	chb_vrms_sum_1_2_bits31_0.payload=0x0;
		chb_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0=0x0;

	chb_vrms_sum_n_1_bits47_32.payload=0x0;
		chb_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32=0x0;
		chb_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32=0x0;


	chb_vrms_sum_2_bits47_32.payload=0x0;
		chb_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32=0x0;
		chb_vrms_sum_2_bits47_32.reserved=0x0;

	chb_total_rf_num.payload=0x0;
		chb_total_rf_num.total_rf_num=0x0;
		chb_total_rf_num.slope=0x0;
		chb_total_rf_num.valid=0x0;

	reserved_x2=0x0;
	chb_shoot.payload=0x0;
		chb_shoot.over_shoot=0x0;
		chb_shoot.pre_shoot=0x0;
		chb_shoot.reserved=0x0;


	chb_edge_cnt_start_xloc.payload=0x0;
		chb_edge_cnt_start_xloc.edge_cnt_start_xloc=0x0;
		chb_edge_cnt_start_xloc.reserved=0x0;
		chb_edge_cnt_start_xloc.valid=0x0;

	chb_edge_cnt_end_xloc.payload=0x0;
		chb_edge_cnt_end_xloc.edge_cnt_end_xloc=0x0;
		chb_edge_cnt_end_xloc.reserved=0x0;
		chb_edge_cnt_end_xloc.valid=0x0;

	chb_edge_cnt_start2_xloc.payload=0x0;
		chb_edge_cnt_start2_xloc.edge_cnt_start2_xloc=0x0;
		chb_edge_cnt_start2_xloc.reserved=0x0;
		chb_edge_cnt_start2_xloc.valid=0x0;

	chb_edge_cnt_end2_xloc.payload=0x0;
		chb_edge_cnt_end2_xloc.edge_cnt_end2_xloc=0x0;
		chb_edge_cnt_end2_xloc.reserved=0x0;
		chb_edge_cnt_end2_xloc.valid=0x0;


	chc_max_min_top_base.payload=0x0;
		chc_max_min_top_base.cha_vmin=0x0;
		chc_max_min_top_base.cha_vmax=0x0;
		chc_max_min_top_base.cha_vbase=0x0;
		chc_max_min_top_base.cha_vtop=0x0;

	chc_threshold_l_mid_h.payload=0x0;
		chc_threshold_l_mid_h.cha_threshold_l=0x0;
		chc_threshold_l_mid_h.cha_threshold_mid=0x0;
		chc_threshold_l_mid_h.cha_threshold_h=0x0;
		chc_threshold_l_mid_h.reserved=0x0;

	chc_vmax_xloc.payload=0x0;
		chc_vmax_xloc.vmax_xloc=0x0;
		chc_vmax_xloc.reserved=0x0;
		chc_vmax_xloc.vmax_xloc_vld=0x0;

	chc_vmin_xloc.payload=0x0;
		chc_vmin_xloc.vmin_xloc=0x0;
		chc_vmin_xloc.reserved=0x0;
		chc_vmin_xloc.vmin_xloc_vld=0x0;


	chc_rtime.payload=0x0;
		chc_rtime.rtime=0x0;
		chc_rtime.reserved=0x0;
		chc_rtime.rtime_vld=0x0;

	chc_rtime_xloc_end.payload=0x0;
		chc_rtime_xloc_end.rtime_xloc_end=0x0;
		chc_rtime_xloc_end.reserved=0x0;
		chc_rtime_xloc_end.rtime_xloc_end_vld=0x0;

	chc_rtime_pre.payload=0x0;
		chc_rtime_pre.rtime_pre=0x0;
		chc_rtime_pre.reserved=0x0;
		chc_rtime_pre.rtime_pre_vld=0x0;

	chc_rtime_xloc_end_pre.payload=0x0;
		chc_rtime_xloc_end_pre.rtime_xloc_end_pre=0x0;
		chc_rtime_xloc_end_pre.reserved=0x0;
		chc_rtime_xloc_end_pre.rtime_xloc_end_pre_vld=0x0;


	chc_ftime.payload=0x0;
		chc_ftime.ftime=0x0;
		chc_ftime.reserved=0x0;
		chc_ftime.ftime_vld=0x0;

	chc_ftime_xloc_end.payload=0x0;
		chc_ftime_xloc_end.ftime_xloc_end=0x0;
		chc_ftime_xloc_end.reserved=0x0;
		chc_ftime_xloc_end.ftime_xloc_end_vld=0x0;

	chc_ftime_pre.payload=0x0;
		chc_ftime_pre.ftime_pre=0x0;
		chc_ftime_pre.reserved=0x0;
		chc_ftime_pre.ftime_pre_vld=0x0;

	chc_ftime_xloc_end_pre.payload=0x0;
		chc_ftime_xloc_end_pre.ftime_xloc_end_pre=0x0;
		chc_ftime_xloc_end_pre.reserved=0x0;
		chc_ftime_xloc_end_pre.ftime_xloc_end_pre_vld=0x0;


	chc_cycle_xloc_4.payload=0x0;
		chc_cycle_xloc_4.cycle_xloc_4=0x0;
		chc_cycle_xloc_4.edge_type=0x0;
		chc_cycle_xloc_4.cycle_xloc_4_vld=0x0;

	chc_cycle_xloc_3.payload=0x0;
		chc_cycle_xloc_3.cycle_xloc_3=0x0;
		chc_cycle_xloc_3.edge_type=0x0;
		chc_cycle_xloc_3.cycle_xloc_3_vld=0x0;

	chc_cycle_xloc_2.payload=0x0;
		chc_cycle_xloc_2.cycle_xloc_2=0x0;
		chc_cycle_xloc_2.edge_type=0x0;
		chc_cycle_xloc_2.cycle_xloc_2_vld=0x0;

	chc_cycle_xloc_1.payload=0x0;
		chc_cycle_xloc_1.cycle_xloc_1=0x0;
		chc_cycle_xloc_1.edge_type=0x0;
		chc_cycle_xloc_1.cycle_xloc_1_vld=0x0;


	chc_ave_sum_n_bits31_0.payload=0x0;
		chc_ave_sum_n_bits31_0.ave_sum_n_bit31_0=0x0;

	chc_ave_sum_1_1_bits31_0.payload=0x0;
		chc_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0=0x0;

	chc_ave_sum_1_2_bits31_0.payload=0x0;
		chc_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0=0x0;

	chc_ave_sum_x_bit39_32.payload=0x0;
		chc_ave_sum_x_bit39_32.ave_sum_n_bit39_32=0x0;
		chc_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32=0x0;
		chc_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32=0x0;
		chc_ave_sum_x_bit39_32.reserved=0x0;


	chc_vrms_sum_n_bits31_0.payload=0x0;
		chc_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0=0x0;

	chc_vrms_sum_1_1_bits31_0.payload=0x0;
		chc_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0=0x0;

	chc_vrms_sum_1_2_bits31_0.payload=0x0;
		chc_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0=0x0;

	chc_vrms_sum_n_1_bits47_32.payload=0x0;
		chc_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32=0x0;
		chc_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32=0x0;


	chc_vrms_sum_2_bits47_32.payload=0x0;
		chc_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32=0x0;
		chc_vrms_sum_2_bits47_32.reserved=0x0;

	chc_total_rf_num.payload=0x0;
		chc_total_rf_num.total_rf_num=0x0;
		chc_total_rf_num.slope=0x0;
		chc_total_rf_num.valid=0x0;

	reserved_x3=0x0;
	chc_shoot.payload=0x0;
		chc_shoot.over_shoot=0x0;
		chc_shoot.pre_shoot=0x0;
		chc_shoot.reserved=0x0;


	chc_edge_cnt_start_xloc.payload=0x0;
		chc_edge_cnt_start_xloc.edge_cnt_start_xloc=0x0;
		chc_edge_cnt_start_xloc.reserved=0x0;
		chc_edge_cnt_start_xloc.valid=0x0;

	chc_edge_cnt_end_xloc.payload=0x0;
		chc_edge_cnt_end_xloc.edge_cnt_end_xloc=0x0;
		chc_edge_cnt_end_xloc.reserved=0x0;
		chc_edge_cnt_end_xloc.valid=0x0;

	chc_edge_cnt_start2_xloc.payload=0x0;
		chc_edge_cnt_start2_xloc.edge_cnt_start2_xloc=0x0;
		chc_edge_cnt_start2_xloc.reserved=0x0;
		chc_edge_cnt_start2_xloc.valid=0x0;

	chc_edge_cnt_end2_xloc.payload=0x0;
		chc_edge_cnt_end2_xloc.edge_cnt_end2_xloc=0x0;
		chc_edge_cnt_end2_xloc.reserved=0x0;
		chc_edge_cnt_end2_xloc.valid=0x0;


	chd_max_min_top_base.payload=0x0;
		chd_max_min_top_base.cha_vmin=0x0;
		chd_max_min_top_base.cha_vmax=0x0;
		chd_max_min_top_base.cha_vbase=0x0;
		chd_max_min_top_base.cha_vtop=0x0;

	chd_threshold_l_mid_h.payload=0x0;
		chd_threshold_l_mid_h.cha_threshold_l=0x0;
		chd_threshold_l_mid_h.cha_threshold_mid=0x0;
		chd_threshold_l_mid_h.cha_threshold_h=0x0;
		chd_threshold_l_mid_h.reserved=0x0;

	chd_vmax_xloc.payload=0x0;
		chd_vmax_xloc.vmax_xloc=0x0;
		chd_vmax_xloc.reserved=0x0;
		chd_vmax_xloc.vmax_xloc_vld=0x0;

	chd_vmin_xloc.payload=0x0;
		chd_vmin_xloc.vmin_xloc=0x0;
		chd_vmin_xloc.reserved=0x0;
		chd_vmin_xloc.vmin_xloc_vld=0x0;


	chd_rtime.payload=0x0;
		chd_rtime.rtime=0x0;
		chd_rtime.reserved=0x0;
		chd_rtime.rtime_vld=0x0;

	chd_rtime_xloc_end.payload=0x0;
		chd_rtime_xloc_end.rtime_xloc_end=0x0;
		chd_rtime_xloc_end.reserved=0x0;
		chd_rtime_xloc_end.rtime_xloc_end_vld=0x0;

	chd_rtime_pre.payload=0x0;
		chd_rtime_pre.rtime_pre=0x0;
		chd_rtime_pre.reserved=0x0;
		chd_rtime_pre.rtime_pre_vld=0x0;

	chd_rtime_xloc_end_pre.payload=0x0;
		chd_rtime_xloc_end_pre.rtime_xloc_end_pre=0x0;
		chd_rtime_xloc_end_pre.reserved=0x0;
		chd_rtime_xloc_end_pre.rtime_xloc_end_pre_vld=0x0;


	chd_ftime.payload=0x0;
		chd_ftime.ftime=0x0;
		chd_ftime.reserved=0x0;
		chd_ftime.ftime_vld=0x0;

	chd_ftime_xloc_end.payload=0x0;
		chd_ftime_xloc_end.ftime_xloc_end=0x0;
		chd_ftime_xloc_end.reserved=0x0;
		chd_ftime_xloc_end.ftime_xloc_end_vld=0x0;

	chd_ftime_pre.payload=0x0;
		chd_ftime_pre.ftime_pre=0x0;
		chd_ftime_pre.reserved=0x0;
		chd_ftime_pre.ftime_pre_vld=0x0;

	chd_ftime_xloc_end_pre.payload=0x0;
		chd_ftime_xloc_end_pre.ftime_xloc_end_pre=0x0;
		chd_ftime_xloc_end_pre.reserved=0x0;
		chd_ftime_xloc_end_pre.ftime_xloc_end_pre_vld=0x0;


	chd_cycle_xloc_4.payload=0x0;
		chd_cycle_xloc_4.cycle_xloc_4=0x0;
		chd_cycle_xloc_4.edge_type=0x0;
		chd_cycle_xloc_4.cycle_xloc_4_vld=0x0;

	chd_cycle_xloc_3.payload=0x0;
		chd_cycle_xloc_3.cycle_xloc_3=0x0;
		chd_cycle_xloc_3.edge_type=0x0;
		chd_cycle_xloc_3.cycle_xloc_3_vld=0x0;

	chd_cycle_xloc_2.payload=0x0;
		chd_cycle_xloc_2.cycle_xloc_2=0x0;
		chd_cycle_xloc_2.edge_type=0x0;
		chd_cycle_xloc_2.cycle_xloc_2_vld=0x0;

	chd_cycle_xloc_1.payload=0x0;
		chd_cycle_xloc_1.cycle_xloc_1=0x0;
		chd_cycle_xloc_1.edge_type=0x0;
		chd_cycle_xloc_1.cycle_xloc_1_vld=0x0;


	chd_ave_sum_n_bits31_0.payload=0x0;
		chd_ave_sum_n_bits31_0.ave_sum_n_bit31_0=0x0;

	chd_ave_sum_1_1_bits31_0.payload=0x0;
		chd_ave_sum_1_1_bits31_0.ave_sum_1_1_bit31_0=0x0;

	chd_ave_sum_1_2_bits31_0.payload=0x0;
		chd_ave_sum_1_2_bits31_0.ave_sum_1_2_bit31_0=0x0;

	chd_ave_sum_x_bit39_32.payload=0x0;
		chd_ave_sum_x_bit39_32.ave_sum_n_bit39_32=0x0;
		chd_ave_sum_x_bit39_32.ave_sum_1_1_bit39_32=0x0;
		chd_ave_sum_x_bit39_32.ave_sum_1_2_bit39_32=0x0;
		chd_ave_sum_x_bit39_32.reserved=0x0;


	chd_vrms_sum_n_bits31_0.payload=0x0;
		chd_vrms_sum_n_bits31_0.vrms_sum_n_bits31_0=0x0;

	chd_vrms_sum_1_1_bits31_0.payload=0x0;
		chd_vrms_sum_1_1_bits31_0.vrms_sum_1_1_bits31_0=0x0;

	chd_vrms_sum_1_2_bits31_0.payload=0x0;
		chd_vrms_sum_1_2_bits31_0.vrms_sum_1_2_bits31_0=0x0;

	chd_vrms_sum_n_1_bits47_32.payload=0x0;
		chd_vrms_sum_n_1_bits47_32.vrms_sum_n_bits47_32=0x0;
		chd_vrms_sum_n_1_bits47_32.vrms_sum_1_1_bits47_32=0x0;


	chd_vrms_sum_2_bits47_32.payload=0x0;
		chd_vrms_sum_2_bits47_32.vrms_sum_1_2_bits47_32=0x0;
		chd_vrms_sum_2_bits47_32.reserved=0x0;

	chd_total_rf_num.payload=0x0;
		chd_total_rf_num.total_rf_num=0x0;
		chd_total_rf_num.slope=0x0;
		chd_total_rf_num.valid=0x0;

	reservd_x4=0x0;
	chd_shoot.payload=0x0;
		chd_shoot.over_shoot=0x0;
		chd_shoot.pre_shoot=0x0;
		chd_shoot.reserved=0x0;


	chd_edge_cnt_start_xloc.payload=0x0;
		chd_edge_cnt_start_xloc.edge_cnt_start_xloc=0x0;
		chd_edge_cnt_start_xloc.reserved=0x0;
		chd_edge_cnt_start_xloc.valid=0x0;

	chd_edge_cnt_end_xloc.payload=0x0;
		chd_edge_cnt_end_xloc.edge_cnt_end_xloc=0x0;
		chd_edge_cnt_end_xloc.reserved=0x0;
		chd_edge_cnt_end_xloc.valid=0x0;

	chd_edge_cnt_start2_xloc.payload=0x0;
		chd_edge_cnt_start2_xloc.edge_cnt_start2_xloc=0x0;
		chd_edge_cnt_start2_xloc.reserved=0x0;
		chd_edge_cnt_start2_xloc.valid=0x0;

	chd_edge_cnt_end2_xloc.payload=0x0;
		chd_edge_cnt_end2_xloc.edge_cnt_end2_xloc=0x0;
		chd_edge_cnt_end2_xloc.reserved=0x0;
		chd_edge_cnt_end2_xloc.valid=0x0;


	meas_version_rd.payload=0x0;
		meas_version_rd.meas_version=0x170405;

	meas_result_done.payload=0x0;
		meas_result_done.meas_result_done=0x0;
		meas_result_done.reserved=0x0;
		meas_result_done.rw_test_reg=0x0;

	la0_cycle_xloc_4.payload=0x0;
		la0_cycle_xloc_4.la0_cycle_xloc_4=0x0;
		la0_cycle_xloc_4.la0_edge_type=0x0;
		la0_cycle_xloc_4.la0_cycle_xloc_4_vld=0x0;

	la0_cycle_xloc_3.payload=0x0;
		la0_cycle_xloc_3.la0_cycle_xloc_3=0x0;
		la0_cycle_xloc_3.la0_edge_type=0x0;
		la0_cycle_xloc_3.la0_cycle_xloc_3_vld=0x0;


	la0_cycle_xloc_2.payload=0x0;
		la0_cycle_xloc_2.la0_cycle_xloc_2=0x0;
		la0_cycle_xloc_2.la0_edge_type=0x0;
		la0_cycle_xloc_2.la0_cycle_xloc_2_vld=0x0;

	la0_cycle_xloc_1.payload=0x0;
		la0_cycle_xloc_1.la0_cycle_xloc_1=0x0;
		la0_cycle_xloc_1.la0_edge_type=0x0;
		la0_cycle_xloc_1.la0_cycle_xloc_1_vld=0x0;

	la1_cycle_xloc_4.payload=0x0;
		la1_cycle_xloc_4.la1_cycle_xloc_4=0x0;
		la1_cycle_xloc_4.la1_edge_type=0x0;
		la1_cycle_xloc_4.la1_cycle_xloc_4_vld=0x0;

	la1_cycle_xloc_3.payload=0x0;
		la1_cycle_xloc_3.la1_cycle_xloc_3=0x0;
		la1_cycle_xloc_3.la1_edge_type=0x0;
		la1_cycle_xloc_3.la1_cycle_xloc_3_vld=0x0;


	la1_cycle_xloc_2.payload=0x0;
		la1_cycle_xloc_2.la1_cycle_xloc_2=0x0;
		la1_cycle_xloc_2.la1_edge_type=0x0;
		la1_cycle_xloc_2.la1_cycle_xloc_2_vld=0x0;

	la1_cycle_xloc_1.payload=0x0;
		la1_cycle_xloc_1.la1_cycle_xloc_1=0x0;
		la1_cycle_xloc_1.la1_edge_type=0x0;
		la1_cycle_xloc_1.la1_cycle_xloc_1_vld=0x0;

	la2_cycle_xloc_4.payload=0x0;
		la2_cycle_xloc_4.la2_cycle_xloc_4=0x0;
		la2_cycle_xloc_4.la2_edge_type=0x0;
		la2_cycle_xloc_4.la2_cycle_xloc_4_vld=0x0;

	la2_cycle_xloc_3.payload=0x0;
		la2_cycle_xloc_3.la2_cycle_xloc_3=0x0;
		la2_cycle_xloc_3.la2_edge_type=0x0;
		la2_cycle_xloc_3.la2_cycle_xloc_3_vld=0x0;


	la2_cycle_xloc_2.payload=0x0;
		la2_cycle_xloc_2.la2_cycle_xloc_2=0x0;
		la2_cycle_xloc_2.la2_edge_type=0x0;
		la2_cycle_xloc_2.la2_cycle_xloc_2_vld=0x0;

	la2_cycle_xloc_1.payload=0x0;
		la2_cycle_xloc_1.la2_cycle_xloc_1=0x0;
		la2_cycle_xloc_1.la2_edge_type=0x0;
		la2_cycle_xloc_1.la2_cycle_xloc_1_vld=0x0;

	la3_cycle_xloc_4.payload=0x0;
		la3_cycle_xloc_4.la3_cycle_xloc_4=0x0;
		la3_cycle_xloc_4.la3_edge_type=0x0;
		la3_cycle_xloc_4.la3_cycle_xloc_4_vld=0x0;

	la3_cycle_xloc_3.payload=0x0;
		la3_cycle_xloc_3.la3_cycle_xloc_3=0x0;
		la3_cycle_xloc_3.la3_edge_type=0x0;
		la3_cycle_xloc_3.la3_cycle_xloc_3_vld=0x0;


	la3_cycle_xloc_2.payload=0x0;
		la3_cycle_xloc_2.la3_cycle_xloc_2=0x0;
		la3_cycle_xloc_2.la3_edge_type=0x0;
		la3_cycle_xloc_2.la3_cycle_xloc_2_vld=0x0;

	la3_cycle_xloc_1.payload=0x0;
		la3_cycle_xloc_1.la3_cycle_xloc_1=0x0;
		la3_cycle_xloc_1.la3_edge_type=0x0;
		la3_cycle_xloc_1.la3_cycle_xloc_1_vld=0x0;

	la4_cycle_xloc_4.payload=0x0;
		la4_cycle_xloc_4.la4_cycle_xloc_4=0x0;
		la4_cycle_xloc_4.la4_edge_type=0x0;
		la4_cycle_xloc_4.la4_cycle_xloc_4_vld=0x0;

	la4_cycle_xloc_3.payload=0x0;
		la4_cycle_xloc_3.la4_cycle_xloc_3=0x0;
		la4_cycle_xloc_3.la4_edge_type=0x0;
		la4_cycle_xloc_3.la4_cycle_xloc_3_vld=0x0;


	la4_cycle_xloc_2.payload=0x0;
		la4_cycle_xloc_2.la4_cycle_xloc_2=0x0;
		la4_cycle_xloc_2.la4_edge_type=0x0;
		la4_cycle_xloc_2.la4_cycle_xloc_2_vld=0x0;

	la4_cycle_xloc_1.payload=0x0;
		la4_cycle_xloc_1.la4_cycle_xloc_1=0x0;
		la4_cycle_xloc_1.la4_edge_type=0x0;
		la4_cycle_xloc_1.la4_cycle_xloc_1_vld=0x0;

	la5_cycle_xloc_4.payload=0x0;
		la5_cycle_xloc_4.la5_cycle_xloc_4=0x0;
		la5_cycle_xloc_4.la5_edge_type=0x0;
		la5_cycle_xloc_4.la5_cycle_xloc_4_vld=0x0;

	la5_cycle_xloc_3.payload=0x0;
		la5_cycle_xloc_3.la5_cycle_xloc_3=0x0;
		la5_cycle_xloc_3.la5_edge_type=0x0;
		la5_cycle_xloc_3.la5_cycle_xloc_3_vld=0x0;


	la5_cycle_xloc_2.payload=0x0;
		la5_cycle_xloc_2.la5_cycle_xloc_2=0x0;
		la5_cycle_xloc_2.la5_edge_type=0x0;
		la5_cycle_xloc_2.la5_cycle_xloc_2_vld=0x0;

	la5_cycle_xloc_1.payload=0x0;
		la5_cycle_xloc_1.la5_cycle_xloc_1=0x0;
		la5_cycle_xloc_1.la5_edge_type=0x0;
		la5_cycle_xloc_1.la5_cycle_xloc_1_vld=0x0;

	la6_cycle_xloc_4.payload=0x0;
		la6_cycle_xloc_4.la6_cycle_xloc_4=0x0;
		la6_cycle_xloc_4.la6_edge_type=0x0;
		la6_cycle_xloc_4.la6_cycle_xloc_4_vld=0x0;

	la6_cycle_xloc_3.payload=0x0;
		la6_cycle_xloc_3.la6_cycle_xloc_3=0x0;
		la6_cycle_xloc_3.la6_edge_type=0x0;
		la6_cycle_xloc_3.la6_cycle_xloc_3_vld=0x0;


	la6_cycle_xloc_2.payload=0x0;
		la6_cycle_xloc_2.la6_cycle_xloc_2=0x0;
		la6_cycle_xloc_2.la6_edge_type=0x0;
		la6_cycle_xloc_2.la6_cycle_xloc_2_vld=0x0;

	la6_cycle_xloc_1.payload=0x0;
		la6_cycle_xloc_1.la6_cycle_xloc_1=0x0;
		la6_cycle_xloc_1.la6_edge_type=0x0;
		la6_cycle_xloc_1.la6_cycle_xloc_1_vld=0x0;

	la7_cycle_xloc_4.payload=0x0;
		la7_cycle_xloc_4.la7_cycle_xloc_4=0x0;
		la7_cycle_xloc_4.la7_edge_type=0x0;
		la7_cycle_xloc_4.la7_cycle_xloc_4_vld=0x0;

	la7_cycle_xloc_3.payload=0x0;
		la7_cycle_xloc_3.la7_cycle_xloc_3=0x0;
		la7_cycle_xloc_3.la7_edge_type=0x0;
		la7_cycle_xloc_3.la7_cycle_xloc_3_vld=0x0;


	la7_cycle_xloc_2.payload=0x0;
		la7_cycle_xloc_2.la7_cycle_xloc_2=0x0;
		la7_cycle_xloc_2.la7_edge_type=0x0;
		la7_cycle_xloc_2.la7_cycle_xloc_2_vld=0x0;

	la7_cycle_xloc_1.payload=0x0;
		la7_cycle_xloc_1.la7_cycle_xloc_1=0x0;
		la7_cycle_xloc_1.la7_edge_type=0x0;
		la7_cycle_xloc_1.la7_cycle_xloc_1_vld=0x0;

	la8_cycle_xloc_4.payload=0x0;
		la8_cycle_xloc_4.la8_cycle_xloc_4=0x0;
		la8_cycle_xloc_4.la8_edge_type=0x0;
		la8_cycle_xloc_4.la8_cycle_xloc_4_vld=0x0;

	la8_cycle_xloc_3.payload=0x0;
		la8_cycle_xloc_3.la8_cycle_xloc_3=0x0;
		la8_cycle_xloc_3.la8_edge_type=0x0;
		la8_cycle_xloc_3.la8_cycle_xloc_3_vld=0x0;


	la8_cycle_xloc_2.payload=0x0;
		la8_cycle_xloc_2.la8_cycle_xloc_2=0x0;
		la8_cycle_xloc_2.la8_edge_type=0x0;
		la8_cycle_xloc_2.la8_cycle_xloc_2_vld=0x0;

	la8_cycle_xloc_1.payload=0x0;
		la8_cycle_xloc_1.la8_cycle_xloc_1=0x0;
		la8_cycle_xloc_1.la8_edge_type=0x0;
		la8_cycle_xloc_1.la8_cycle_xloc_1_vld=0x0;

	la9_cycle_xloc_4.payload=0x0;
		la9_cycle_xloc_4.la9_cycle_xloc_4=0x0;
		la9_cycle_xloc_4.la9_edge_type=0x0;
		la9_cycle_xloc_4.la9_cycle_xloc_4_vld=0x0;

	la9_cycle_xloc_3.payload=0x0;
		la9_cycle_xloc_3.la9_cycle_xloc_3=0x0;
		la9_cycle_xloc_3.la9_edge_type=0x0;
		la9_cycle_xloc_3.la9_cycle_xloc_3_vld=0x0;


	la9_cycle_xloc_2.payload=0x0;
		la9_cycle_xloc_2.la9_cycle_xloc_2=0x0;
		la9_cycle_xloc_2.la9_edge_type=0x0;
		la9_cycle_xloc_2.la9_cycle_xloc_2_vld=0x0;

	la9_cycle_xloc_1.payload=0x0;
		la9_cycle_xloc_1.la9_cycle_xloc_1=0x0;
		la9_cycle_xloc_1.la9_edge_type=0x0;
		la9_cycle_xloc_1.la9_cycle_xloc_1_vld=0x0;

	la10_cycle_xloc_4.payload=0x0;
		la10_cycle_xloc_4.la10_cycle_xloc_4=0x0;
		la10_cycle_xloc_4.la10_edge_type=0x0;
		la10_cycle_xloc_4.la10_cycle_xloc_4_vld=0x0;

	la10_cycle_xloc_3.payload=0x0;
		la10_cycle_xloc_3.la10_cycle_xloc_3=0x0;
		la10_cycle_xloc_3.la10_edge_type=0x0;
		la10_cycle_xloc_3.la10_cycle_xloc_3_vld=0x0;


	la10_cycle_xloc_2.payload=0x0;
		la10_cycle_xloc_2.la10_cycle_xloc_2=0x0;
		la10_cycle_xloc_2.la10_edge_type=0x0;
		la10_cycle_xloc_2.la10_cycle_xloc_2_vld=0x0;

	la10_cycle_xloc_1.payload=0x0;
		la10_cycle_xloc_1.la10_cycle_xloc_1=0x0;
		la10_cycle_xloc_1.la10_edge_type=0x0;
		la10_cycle_xloc_1.la10_cycle_xloc_1_vld=0x0;

	la11_cycle_xloc_4.payload=0x0;
		la11_cycle_xloc_4.la11_cycle_xloc_4=0x0;
		la11_cycle_xloc_4.la11_edge_type=0x0;
		la11_cycle_xloc_4.la11_cycle_xloc_4_vld=0x0;

	la11_cycle_xloc_3.payload=0x0;
		la11_cycle_xloc_3.la11_cycle_xloc_3=0x0;
		la11_cycle_xloc_3.la11_edge_type=0x0;
		la11_cycle_xloc_3.la11_cycle_xloc_3_vld=0x0;


	la11_cycle_xloc_2.payload=0x0;
		la11_cycle_xloc_2.la11_cycle_xloc_2=0x0;
		la11_cycle_xloc_2.la11_edge_type=0x0;
		la11_cycle_xloc_2.la11_cycle_xloc_2_vld=0x0;

	la11_cycle_xloc_1.payload=0x0;
		la11_cycle_xloc_1.la11_cycle_xloc_1=0x0;
		la11_cycle_xloc_1.la11_edge_type=0x0;
		la11_cycle_xloc_1.la11_cycle_xloc_1_vld=0x0;

	la12_cycle_xloc_4.payload=0x0;
		la12_cycle_xloc_4.la12_cycle_xloc_4=0x0;
		la12_cycle_xloc_4.la12_edge_type=0x0;
		la12_cycle_xloc_4.la12_cycle_xloc_4_vld=0x0;

	la12_cycle_xloc_3.payload=0x0;
		la12_cycle_xloc_3.la12_cycle_xloc_3=0x0;
		la12_cycle_xloc_3.la12_edge_type=0x0;
		la12_cycle_xloc_3.la12_cycle_xloc_3_vld=0x0;


	la12_cycle_xloc_2.payload=0x0;
		la12_cycle_xloc_2.la12_cycle_xloc_2=0x0;
		la12_cycle_xloc_2.la12_edge_type=0x0;
		la12_cycle_xloc_2.la12_cycle_xloc_2_vld=0x0;

	la12_cycle_xloc_1.payload=0x0;
		la12_cycle_xloc_1.la12_cycle_xloc_1=0x0;
		la12_cycle_xloc_1.la12_edge_type=0x0;
		la12_cycle_xloc_1.la12_cycle_xloc_1_vld=0x0;

	la13_cycle_xloc_4.payload=0x0;
		la13_cycle_xloc_4.la13_cycle_xloc_4=0x0;
		la13_cycle_xloc_4.la13_edge_type=0x0;
		la13_cycle_xloc_4.la13_cycle_xloc_4_vld=0x0;

	la13_cycle_xloc_3.payload=0x0;
		la13_cycle_xloc_3.la13_cycle_xloc_3=0x0;
		la13_cycle_xloc_3.la13_edge_type=0x0;
		la13_cycle_xloc_3.la13_cycle_xloc_3_vld=0x0;


	la13_cycle_xloc_2.payload=0x0;
		la13_cycle_xloc_2.la13_cycle_xloc_2=0x0;
		la13_cycle_xloc_2.la13_edge_type=0x0;
		la13_cycle_xloc_2.la13_cycle_xloc_2_vld=0x0;

	la13_cycle_xloc_1.payload=0x0;
		la13_cycle_xloc_1.la13_cycle_xloc_1=0x0;
		la13_cycle_xloc_1.la13_edge_type=0x0;
		la13_cycle_xloc_1.la13_cycle_xloc_1_vld=0x0;

	la14_cycle_xloc_4.payload=0x0;
		la14_cycle_xloc_4.la14_cycle_xloc_4=0x0;
		la14_cycle_xloc_4.la14_edge_type=0x0;
		la14_cycle_xloc_4.la14_cycle_xloc_4_vld=0x0;

	la14_cycle_xloc_3.payload=0x0;
		la14_cycle_xloc_3.la14_cycle_xloc_3=0x0;
		la14_cycle_xloc_3.la14_edge_type=0x0;
		la14_cycle_xloc_3.la14_cycle_xloc_3_vld=0x0;


	la14_cycle_xloc_2.payload=0x0;
		la14_cycle_xloc_2.la14_cycle_xloc_2=0x0;
		la14_cycle_xloc_2.la14_edge_type=0x0;
		la14_cycle_xloc_2.la14_cycle_xloc_2_vld=0x0;

	la14_cycle_xloc_1.payload=0x0;
		la14_cycle_xloc_1.la14_cycle_xloc_1=0x0;
		la14_cycle_xloc_1.la14_edge_type=0x0;
		la14_cycle_xloc_1.la14_cycle_xloc_1_vld=0x0;

	la15_cycle_xloc_4.payload=0x0;
		la15_cycle_xloc_4.la15_cycle_xloc_4=0x0;
		la15_cycle_xloc_4.la15_edge_type=0x0;
		la15_cycle_xloc_4.la15_cycle_xloc_4_vld=0x0;

	la15_cycle_xloc_3.payload=0x0;
		la15_cycle_xloc_3.la15_cycle_xloc_3=0x0;
		la15_cycle_xloc_3.la15_edge_type=0x0;
		la15_cycle_xloc_3.la15_cycle_xloc_3_vld=0x0;


	la15_cycle_xloc_2.payload=0x0;
		la15_cycle_xloc_2.la15_cycle_xloc_2=0x0;
		la15_cycle_xloc_2.la15_edge_type=0x0;
		la15_cycle_xloc_2.la15_cycle_xloc_2_vld=0x0;

	la15_cycle_xloc_1.payload=0x0;
		la15_cycle_xloc_1.la15_cycle_xloc_1=0x0;
		la15_cycle_xloc_1.la15_edge_type=0x0;
		la15_cycle_xloc_1.la15_cycle_xloc_1_vld=0x0;

	dlyresult1_from_xloc2.payload=0x0;
		dlyresult1_from_xloc2.dlyresult1_from_xloc2=0x0;
		dlyresult1_from_xloc2.dlyresult1_from_xloc2_vld=0x0;

	dlyresult1_from_xloc1.payload=0x0;
		dlyresult1_from_xloc1.dlyresult1_from_xloc1=0x0;
		dlyresult1_from_xloc1.dlyresult1_from_xloc1_vld=0x0;


	dlyresult1_to_xloc2_pre.payload=0x0;
		dlyresult1_to_xloc2_pre.dlyresult1_to_xloc2_pre=0x0;
		dlyresult1_to_xloc2_pre.dlyresult1_to_xloc2_pre_vld=0x0;

	dlyresult1_to_xloc2.payload=0x0;
		dlyresult1_to_xloc2.dlyresult1_to_xloc2=0x0;
		dlyresult1_to_xloc2.dlyresult1_to_xloc2_vld=0x0;

	dlyresult1_to_xloc1_pre.payload=0x0;
		dlyresult1_to_xloc1_pre.dlyresult1_to_xloc1_pre=0x0;
		dlyresult1_to_xloc1_pre.dlyresult1_to_xloc1_pre_vld=0x0;

	dlyresult1_to_xloc1.payload=0x0;
		dlyresult1_to_xloc1.dlyresult1_to_xloc1=0x0;
		dlyresult1_to_xloc1.dlyresult1_to_xloc1_vld=0x0;


	dlyresult2_from_xloc2.payload=0x0;
		dlyresult2_from_xloc2.dlyresult2_from_xloc2=0x0;
		dlyresult2_from_xloc2.dlyresult2_from_xloc2_vld=0x0;

	dlyresult2_from_xloc1.payload=0x0;
		dlyresult2_from_xloc1.dlyresult2_from_xloc1=0x0;
		dlyresult2_from_xloc1.dlyresult2_from_xloc1_vld=0x0;

	dlyresult2_to_xloc2_pre.payload=0x0;
		dlyresult2_to_xloc2_pre.dlyresult2_to_xloc2_pre=0x0;
		dlyresult2_to_xloc2_pre.dlyresult2_to_xloc2_pre_vld=0x0;

	dlyresult2_to_xloc2.payload=0x0;
		dlyresult2_to_xloc2.dlyresult2_to_xloc2=0x0;
		dlyresult2_to_xloc2.dlyresult2_to_xloc2_vld=0x0;


	dlyresult2_to_xloc1_pre.payload=0x0;
		dlyresult2_to_xloc1_pre.dlyresult2_to_xloc1_pre=0x0;
		dlyresult2_to_xloc1_pre.dlyresult2_to_xloc1_pre_vld=0x0;

	dlyresult2_to_xloc1.payload=0x0;
		dlyresult2_to_xloc1.dlyresult2_to_xloc1=0x0;
		dlyresult2_to_xloc1.dlyresult2_to_xloc1_vld=0x0;

	dlyresult3_from_xloc2.payload=0x0;
		dlyresult3_from_xloc2.dlyresult3_from_xloc2=0x0;
		dlyresult3_from_xloc2.dlyresult3_from_xloc2_vld=0x0;

	dlyresult3_from_xloc1.payload=0x0;
		dlyresult3_from_xloc1.dlyresult3_from_xloc1=0x0;
		dlyresult3_from_xloc1.dlyresult3_from_xloc1_vld=0x0;


	dlyresult3_to_xloc2_pre.payload=0x0;
		dlyresult3_to_xloc2_pre.dlyresult3_to_xloc2_pre=0x0;
		dlyresult3_to_xloc2_pre.dlyresult3_to_xloc2_pre_vld=0x0;

	dlyresult3_to_xloc2.payload=0x0;
		dlyresult3_to_xloc2.dlyresult3_to_xloc2=0x0;
		dlyresult3_to_xloc2.dlyresult3_to_xloc2_vld=0x0;

	dlyresult3_to_xloc1_pre.payload=0x0;
		dlyresult3_to_xloc1_pre.dlyresult3_to_xloc1_pre=0x0;
		dlyresult3_to_xloc1_pre.dlyresult3_to_xloc1_pre_vld=0x0;

	dlyresult3_to_xloc1.payload=0x0;
		dlyresult3_to_xloc1.dlyresult3_to_xloc1=0x0;
		dlyresult3_to_xloc1.dlyresult3_to_xloc1_vld=0x0;


	dlyresult4_from_xloc2.payload=0x0;
		dlyresult4_from_xloc2.dlyresult4_from_xloc2=0x0;
		dlyresult4_from_xloc2.dlyresult4_from_xloc2_vld=0x0;

	dlyresult4_from_xloc1.payload=0x0;
		dlyresult4_from_xloc1.dlyresult4_from_xloc1=0x0;
		dlyresult4_from_xloc1.dlyresult4_from_xloc1_vld=0x0;

	dlyresult4_to_xloc2_pre.payload=0x0;
		dlyresult4_to_xloc2_pre.dlyresult4_to_xloc2_pre=0x0;
		dlyresult4_to_xloc2_pre.dlyresult4_to_xloc2_pre_vld=0x0;

	dlyresult4_to_xloc2.payload=0x0;
		dlyresult4_to_xloc2.dlyresult4_to_xloc2=0x0;
		dlyresult4_to_xloc2.dlyresult4_to_xloc2_vld=0x0;


	dlyresult4_to_xloc1_pre.payload=0x0;
		dlyresult4_to_xloc1_pre.dlyresult4_to_xloc1_pre=0x0;
		dlyresult4_to_xloc1_pre.dlyresult4_to_xloc1_pre_vld=0x0;

	dlyresult4_to_xloc1.payload=0x0;
		dlyresult4_to_xloc1.dlyresult4_to_xloc1=0x0;
		dlyresult4_to_xloc1.dlyresult4_to_xloc1_vld=0x0;

	dlyresult5_from_xloc2.payload=0x0;
		dlyresult5_from_xloc2.dlyresult5_from_xloc2=0x0;
		dlyresult5_from_xloc2.dlyresult5_from_xloc2_vld=0x0;

	dlyresult5_from_xloc1.payload=0x0;
		dlyresult5_from_xloc1.dlyresult5_from_xloc1=0x0;
		dlyresult5_from_xloc1.dlyresult5_from_xloc1_vld=0x0;


	dlyresult5_to_xloc2_pre.payload=0x0;
		dlyresult5_to_xloc2_pre.dlyresult5_to_xloc2_pre=0x0;
		dlyresult5_to_xloc2_pre.dlyresult5_to_xloc2_pre_vld=0x0;

	dlyresult5_to_xloc2.payload=0x0;
		dlyresult5_to_xloc2.dlyresult5_to_xloc2=0x0;
		dlyresult5_to_xloc2.dlyresult5_to_xloc2_vld=0x0;

	dlyresult5_to_xloc1_pre.payload=0x0;
		dlyresult5_to_xloc1_pre.dlyresult5_to_xloc1_pre=0x0;
		dlyresult5_to_xloc1_pre.dlyresult5_to_xloc1_pre_vld=0x0;

	dlyresult5_to_xloc1.payload=0x0;
		dlyresult5_to_xloc1.dlyresult5_to_xloc1=0x0;
		dlyresult5_to_xloc1.dlyresult5_to_xloc1_vld=0x0;


	dlyresult6_from_xloc2.payload=0x0;
		dlyresult6_from_xloc2.dlyresult6_from_xloc2=0x0;
		dlyresult6_from_xloc2.dlyresult6_from_xloc2_vld=0x0;

	dlyresult6_from_xloc1.payload=0x0;
		dlyresult6_from_xloc1.dlyresult6_from_xloc1=0x0;
		dlyresult6_from_xloc1.dlyresult6_from_xloc1_vld=0x0;

	dlyresult6_to_xloc2_pre.payload=0x0;
		dlyresult6_to_xloc2_pre.dlyresult6_to_xloc2_pre=0x0;
		dlyresult6_to_xloc2_pre.dlyresult6_to_xloc2_pre_vld=0x0;

	dlyresult6_to_xloc2.payload=0x0;
		dlyresult6_to_xloc2.dlyresult6_to_xloc2=0x0;
		dlyresult6_to_xloc2.dlyresult6_to_xloc2_vld=0x0;


	dlyresult6_to_xloc1_pre.payload=0x0;
		dlyresult6_to_xloc1_pre.dlyresult6_to_xloc1_pre=0x0;
		dlyresult6_to_xloc1_pre.dlyresult6_to_xloc1_pre_vld=0x0;

	dlyresult6_to_xloc1.payload=0x0;
		dlyresult6_to_xloc1.dlyresult6_to_xloc1=0x0;
		dlyresult6_to_xloc1.dlyresult6_to_xloc1_vld=0x0;

	dlyresult7_from_xloc2.payload=0x0;
		dlyresult7_from_xloc2.dlyresult7_from_xloc2=0x0;
		dlyresult7_from_xloc2.dlyresult7_from_xloc2_vld=0x0;

	dlyresult7_from_xloc1.payload=0x0;
		dlyresult7_from_xloc1.dlyresult7_from_xloc1=0x0;
		dlyresult7_from_xloc1.dlyresult7_from_xloc1_vld=0x0;


	dlyresult7_to_xloc2_pre.payload=0x0;
		dlyresult7_to_xloc2_pre.dlyresult7_to_xloc2_pre=0x0;
		dlyresult7_to_xloc2_pre.dlyresult7_to_xloc2_pre_vld=0x0;

	dlyresult7_to_xloc2.payload=0x0;
		dlyresult7_to_xloc2.dlyresult7_to_xloc2=0x0;
		dlyresult7_to_xloc2.dlyresult7_to_xloc2_vld=0x0;

	dlyresult7_to_xloc1_pre.payload=0x0;
		dlyresult7_to_xloc1_pre.dlyresult7_to_xloc1_pre=0x0;
		dlyresult7_to_xloc1_pre.dlyresult7_to_xloc1_pre_vld=0x0;

	dlyresult7_to_xloc1.payload=0x0;
		dlyresult7_to_xloc1.dlyresult7_to_xloc1=0x0;
		dlyresult7_to_xloc1.dlyresult7_to_xloc1_vld=0x0;


	dlyresult8_from_xloc2.payload=0x0;
		dlyresult8_from_xloc2.dlyresult8_from_xloc2=0x0;
		dlyresult8_from_xloc2.dlyresult8_from_xloc2_vld=0x0;

	dlyresult8_from_xloc1.payload=0x0;
		dlyresult8_from_xloc1.dlyresult8_from_xloc1=0x0;
		dlyresult8_from_xloc1.dlyresult8_from_xloc1_vld=0x0;

	dlyresult8_to_xloc2_pre.payload=0x0;
		dlyresult8_to_xloc2_pre.dlyresult8_to_xloc2_pre=0x0;
		dlyresult8_to_xloc2_pre.dlyresult8_to_xloc2_pre_vld=0x0;

	dlyresult8_to_xloc2.payload=0x0;
		dlyresult8_to_xloc2.dlyresult8_to_xloc2=0x0;
		dlyresult8_to_xloc2.dlyresult8_to_xloc2_vld=0x0;


	dlyresult8_to_xloc1_pre.payload=0x0;
		dlyresult8_to_xloc1_pre.dlyresult8_to_xloc1_pre=0x0;
		dlyresult8_to_xloc1_pre.dlyresult8_to_xloc1_pre_vld=0x0;

	dlyresult8_to_xloc1.payload=0x0;
		dlyresult8_to_xloc1.dlyresult8_to_xloc1=0x0;
		dlyresult8_to_xloc1.dlyresult8_to_xloc1_vld=0x0;

	dlyresult9_from_xloc2.payload=0x0;
		dlyresult9_from_xloc2.dlyresult9_from_xloc2=0x0;
		dlyresult9_from_xloc2.dlyresult9_from_xloc2_vld=0x0;

	dlyresult9_from_xloc1.payload=0x0;
		dlyresult9_from_xloc1.dlyresult9_from_xloc1=0x0;
		dlyresult9_from_xloc1.dlyresult9_from_xloc1_vld=0x0;


	dlyresult9_to_xloc2_pre.payload=0x0;
		dlyresult9_to_xloc2_pre.dlyresult9_to_xloc2_pre=0x0;
		dlyresult9_to_xloc2_pre.dlyresult9_to_xloc2_pre_vld=0x0;

	dlyresult9_to_xloc2.payload=0x0;
		dlyresult9_to_xloc2.dlyresult9_to_xloc2=0x0;
		dlyresult9_to_xloc2.dlyresult9_to_xloc2_vld=0x0;

	dlyresult9_to_xloc1_pre.payload=0x0;
		dlyresult9_to_xloc1_pre.dlyresult9_to_xloc1_pre=0x0;
		dlyresult9_to_xloc1_pre.dlyresult9_to_xloc1_pre_vld=0x0;

	dlyresult9_to_xloc1.payload=0x0;
		dlyresult9_to_xloc1.dlyresult9_to_xloc1=0x0;
		dlyresult9_to_xloc1.dlyresult9_to_xloc1_vld=0x0;


	dlyresult10_from_xloc2.payload=0x0;
		dlyresult10_from_xloc2.dlyresult10_from_xloc2=0x0;
		dlyresult10_from_xloc2.dlyresult10_from_xloc2_vld=0x0;

	dlyresult10_from_xloc1.payload=0x0;
		dlyresult10_from_xloc1.dlyresult10_from_xloc1=0x0;
		dlyresult10_from_xloc1.dlyresult10_from_xloc1_vld=0x0;

	dlyresult10_to_xloc2_pre.payload=0x0;
		dlyresult10_to_xloc2_pre.dlyresult10_to_xloc2_pre=0x0;
		dlyresult10_to_xloc2_pre.dlyresult10_to_xloc2_pre_vld=0x0;

	dlyresult10_to_xloc2.payload=0x0;
		dlyresult10_to_xloc2.dlyresult10_to_xloc2=0x0;
		dlyresult10_to_xloc2.dlyresult10_to_xloc2_vld=0x0;


	dlyresult10_to_xloc1_pre.payload=0x0;
		dlyresult10_to_xloc1_pre.dlyresult10_to_xloc1_pre=0x0;
		dlyresult10_to_xloc1_pre.dlyresult10_to_xloc1_pre_vld=0x0;

	dlyresult10_to_xloc1.payload=0x0;
		dlyresult10_to_xloc1.dlyresult10_to_xloc1=0x0;
		dlyresult10_to_xloc1.dlyresult10_to_xloc1_vld=0x0;

}

void MeasRetuMem::_outOtp()
{































































}
void MeasRetuMem::push( MeasRetuMemProxy */*proxy*/ )
{
}
void MeasRetuMem::pull( MeasRetuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(cha_max_min_top_base);
	reg_in(cha_threshold_l_mid_h);
	reg_in(cha_vmax_xloc);
	reg_in(cha_vmin_xloc);
	reg_in(cha_rtime);
	reg_in(cha_rtime_xloc_end);
	reg_in(cha_rtime_pre);
	reg_in(cha_rtime_xloc_end_pre);
	reg_in(cha_ftime);
	reg_in(cha_ftime_xloc_end);
	reg_in(cha_ftime_pre);
	reg_in(cha_ftime_xloc_end_pre);
	reg_in(cha_cycle_xloc_4);
	reg_in(cha_cycle_xloc_3);
	reg_in(cha_cycle_xloc_2);
	reg_in(cha_cycle_xloc_1);
	reg_in(cha_ave_sum_n_bits31_0);
	reg_in(cha_ave_sum_1_1_bits31_0);
	reg_in(cha_ave_sum_1_2_bits31_0);
	reg_in(cha_ave_sum_x_bit39_32);
	reg_in(cha_vrms_sum_n_bits31_0);
	reg_in(cha_vrms_sum_1_1_bits31_0);
	reg_in(cha_vrms_sum_1_2_bits31_0);
	reg_in(cha_vrms_sum_n_1_bits47_32);
	reg_in(cha_vrms_sum_2_bits47_32);
	reg_in(cha_total_rf_num);
	reg_in(reserved_x1);
	reg_in(cha_shoot);
	reg_in(cha_edge_cnt_start_xloc);
	reg_in(cha_edge_cnt_end_xloc);
	reg_in(cha_edge_cnt_start2_xloc);
	reg_in(cha_edge_cnt_end2_xloc);
	reg_in(chb_max_min_top_base);
	reg_in(chb_threshold_l_mid_h);
	reg_in(chb_vmax_xloc);
	reg_in(chb_vmin_xloc);
	reg_in(chb_rtime);
	reg_in(chb_rtime_xloc_end);
	reg_in(chb_rtime_pre);
	reg_in(chb_rtime_xloc_end_pre);
	reg_in(chb_ftime);
	reg_in(chb_ftime_xloc_end);
	reg_in(chb_ftime_pre);
	reg_in(chb_ftime_xloc_end_pre);
	reg_in(chb_cycle_xloc_4);
	reg_in(chb_cycle_xloc_3);
	reg_in(chb_cycle_xloc_2);
	reg_in(chb_cycle_xloc_1);
	reg_in(chb_ave_sum_n_bits31_0);
	reg_in(chb_ave_sum_1_1_bits31_0);
	reg_in(chb_ave_sum_1_2_bits31_0);
	reg_in(chb_ave_sum_x_bit39_32);
	reg_in(chb_vrms_sum_n_bits31_0);
	reg_in(chb_vrms_sum_1_1_bits31_0);
	reg_in(chb_vrms_sum_1_2_bits31_0);
	reg_in(chb_vrms_sum_n_1_bits47_32);
	reg_in(chb_vrms_sum_2_bits47_32);
	reg_in(chb_total_rf_num);
	reg_in(reserved_x2);
	reg_in(chb_shoot);
	reg_in(chb_edge_cnt_start_xloc);
	reg_in(chb_edge_cnt_end_xloc);
	reg_in(chb_edge_cnt_start2_xloc);
	reg_in(chb_edge_cnt_end2_xloc);
	reg_in(chc_max_min_top_base);
	reg_in(chc_threshold_l_mid_h);
	reg_in(chc_vmax_xloc);
	reg_in(chc_vmin_xloc);
	reg_in(chc_rtime);
	reg_in(chc_rtime_xloc_end);
	reg_in(chc_rtime_pre);
	reg_in(chc_rtime_xloc_end_pre);
	reg_in(chc_ftime);
	reg_in(chc_ftime_xloc_end);
	reg_in(chc_ftime_pre);
	reg_in(chc_ftime_xloc_end_pre);
	reg_in(chc_cycle_xloc_4);
	reg_in(chc_cycle_xloc_3);
	reg_in(chc_cycle_xloc_2);
	reg_in(chc_cycle_xloc_1);
	reg_in(chc_ave_sum_n_bits31_0);
	reg_in(chc_ave_sum_1_1_bits31_0);
	reg_in(chc_ave_sum_1_2_bits31_0);
	reg_in(chc_ave_sum_x_bit39_32);
	reg_in(chc_vrms_sum_n_bits31_0);
	reg_in(chc_vrms_sum_1_1_bits31_0);
	reg_in(chc_vrms_sum_1_2_bits31_0);
	reg_in(chc_vrms_sum_n_1_bits47_32);
	reg_in(chc_vrms_sum_2_bits47_32);
	reg_in(chc_total_rf_num);
	reg_in(reserved_x3);
	reg_in(chc_shoot);
	reg_in(chc_edge_cnt_start_xloc);
	reg_in(chc_edge_cnt_end_xloc);
	reg_in(chc_edge_cnt_start2_xloc);
	reg_in(chc_edge_cnt_end2_xloc);
	reg_in(chd_max_min_top_base);
	reg_in(chd_threshold_l_mid_h);
	reg_in(chd_vmax_xloc);
	reg_in(chd_vmin_xloc);
	reg_in(chd_rtime);
	reg_in(chd_rtime_xloc_end);
	reg_in(chd_rtime_pre);
	reg_in(chd_rtime_xloc_end_pre);
	reg_in(chd_ftime);
	reg_in(chd_ftime_xloc_end);
	reg_in(chd_ftime_pre);
	reg_in(chd_ftime_xloc_end_pre);
	reg_in(chd_cycle_xloc_4);
	reg_in(chd_cycle_xloc_3);
	reg_in(chd_cycle_xloc_2);
	reg_in(chd_cycle_xloc_1);
	reg_in(chd_ave_sum_n_bits31_0);
	reg_in(chd_ave_sum_1_1_bits31_0);
	reg_in(chd_ave_sum_1_2_bits31_0);
	reg_in(chd_ave_sum_x_bit39_32);
	reg_in(chd_vrms_sum_n_bits31_0);
	reg_in(chd_vrms_sum_1_1_bits31_0);
	reg_in(chd_vrms_sum_1_2_bits31_0);
	reg_in(chd_vrms_sum_n_1_bits47_32);
	reg_in(chd_vrms_sum_2_bits47_32);
	reg_in(chd_total_rf_num);
	reg_in(reservd_x4);
	reg_in(chd_shoot);
	reg_in(chd_edge_cnt_start_xloc);
	reg_in(chd_edge_cnt_end_xloc);
	reg_in(chd_edge_cnt_start2_xloc);
	reg_in(chd_edge_cnt_end2_xloc);
	reg_in(meas_version_rd);
	reg_in(meas_result_done);
	reg_in(la0_cycle_xloc_4);
	reg_in(la0_cycle_xloc_3);
	reg_in(la0_cycle_xloc_2);
	reg_in(la0_cycle_xloc_1);
	reg_in(la1_cycle_xloc_4);
	reg_in(la1_cycle_xloc_3);
	reg_in(la1_cycle_xloc_2);
	reg_in(la1_cycle_xloc_1);
	reg_in(la2_cycle_xloc_4);
	reg_in(la2_cycle_xloc_3);
	reg_in(la2_cycle_xloc_2);
	reg_in(la2_cycle_xloc_1);
	reg_in(la3_cycle_xloc_4);
	reg_in(la3_cycle_xloc_3);
	reg_in(la3_cycle_xloc_2);
	reg_in(la3_cycle_xloc_1);
	reg_in(la4_cycle_xloc_4);
	reg_in(la4_cycle_xloc_3);
	reg_in(la4_cycle_xloc_2);
	reg_in(la4_cycle_xloc_1);
	reg_in(la5_cycle_xloc_4);
	reg_in(la5_cycle_xloc_3);
	reg_in(la5_cycle_xloc_2);
	reg_in(la5_cycle_xloc_1);
	reg_in(la6_cycle_xloc_4);
	reg_in(la6_cycle_xloc_3);
	reg_in(la6_cycle_xloc_2);
	reg_in(la6_cycle_xloc_1);
	reg_in(la7_cycle_xloc_4);
	reg_in(la7_cycle_xloc_3);
	reg_in(la7_cycle_xloc_2);
	reg_in(la7_cycle_xloc_1);
	reg_in(la8_cycle_xloc_4);
	reg_in(la8_cycle_xloc_3);
	reg_in(la8_cycle_xloc_2);
	reg_in(la8_cycle_xloc_1);
	reg_in(la9_cycle_xloc_4);
	reg_in(la9_cycle_xloc_3);
	reg_in(la9_cycle_xloc_2);
	reg_in(la9_cycle_xloc_1);
	reg_in(la10_cycle_xloc_4);
	reg_in(la10_cycle_xloc_3);
	reg_in(la10_cycle_xloc_2);
	reg_in(la10_cycle_xloc_1);
	reg_in(la11_cycle_xloc_4);
	reg_in(la11_cycle_xloc_3);
	reg_in(la11_cycle_xloc_2);
	reg_in(la11_cycle_xloc_1);
	reg_in(la12_cycle_xloc_4);
	reg_in(la12_cycle_xloc_3);
	reg_in(la12_cycle_xloc_2);
	reg_in(la12_cycle_xloc_1);
	reg_in(la13_cycle_xloc_4);
	reg_in(la13_cycle_xloc_3);
	reg_in(la13_cycle_xloc_2);
	reg_in(la13_cycle_xloc_1);
	reg_in(la14_cycle_xloc_4);
	reg_in(la14_cycle_xloc_3);
	reg_in(la14_cycle_xloc_2);
	reg_in(la14_cycle_xloc_1);
	reg_in(la15_cycle_xloc_4);
	reg_in(la15_cycle_xloc_3);
	reg_in(la15_cycle_xloc_2);
	reg_in(la15_cycle_xloc_1);
	reg_in(dlyresult1_from_xloc2);
	reg_in(dlyresult1_from_xloc1);
	reg_in(dlyresult1_to_xloc2_pre);
	reg_in(dlyresult1_to_xloc2);
	reg_in(dlyresult1_to_xloc1_pre);
	reg_in(dlyresult1_to_xloc1);
	reg_in(dlyresult2_from_xloc2);
	reg_in(dlyresult2_from_xloc1);
	reg_in(dlyresult2_to_xloc2_pre);
	reg_in(dlyresult2_to_xloc2);
	reg_in(dlyresult2_to_xloc1_pre);
	reg_in(dlyresult2_to_xloc1);
	reg_in(dlyresult3_from_xloc2);
	reg_in(dlyresult3_from_xloc1);
	reg_in(dlyresult3_to_xloc2_pre);
	reg_in(dlyresult3_to_xloc2);
	reg_in(dlyresult3_to_xloc1_pre);
	reg_in(dlyresult3_to_xloc1);
	reg_in(dlyresult4_from_xloc2);
	reg_in(dlyresult4_from_xloc1);
	reg_in(dlyresult4_to_xloc2_pre);
	reg_in(dlyresult4_to_xloc2);
	reg_in(dlyresult4_to_xloc1_pre);
	reg_in(dlyresult4_to_xloc1);
	reg_in(dlyresult5_from_xloc2);
	reg_in(dlyresult5_from_xloc1);
	reg_in(dlyresult5_to_xloc2_pre);
	reg_in(dlyresult5_to_xloc2);
	reg_in(dlyresult5_to_xloc1_pre);
	reg_in(dlyresult5_to_xloc1);
	reg_in(dlyresult6_from_xloc2);
	reg_in(dlyresult6_from_xloc1);
	reg_in(dlyresult6_to_xloc2_pre);
	reg_in(dlyresult6_to_xloc2);
	reg_in(dlyresult6_to_xloc1_pre);
	reg_in(dlyresult6_to_xloc1);
	reg_in(dlyresult7_from_xloc2);
	reg_in(dlyresult7_from_xloc1);
	reg_in(dlyresult7_to_xloc2_pre);
	reg_in(dlyresult7_to_xloc2);
	reg_in(dlyresult7_to_xloc1_pre);
	reg_in(dlyresult7_to_xloc1);
	reg_in(dlyresult8_from_xloc2);
	reg_in(dlyresult8_from_xloc1);
	reg_in(dlyresult8_to_xloc2_pre);
	reg_in(dlyresult8_to_xloc2);
	reg_in(dlyresult8_to_xloc1_pre);
	reg_in(dlyresult8_to_xloc1);
	reg_in(dlyresult9_from_xloc2);
	reg_in(dlyresult9_from_xloc1);
	reg_in(dlyresult9_to_xloc2_pre);
	reg_in(dlyresult9_to_xloc2);
	reg_in(dlyresult9_to_xloc1_pre);
	reg_in(dlyresult9_to_xloc1);
	reg_in(dlyresult10_from_xloc2);
	reg_in(dlyresult10_from_xloc1);
	reg_in(dlyresult10_to_xloc2_pre);
	reg_in(dlyresult10_to_xloc2);
	reg_in(dlyresult10_to_xloc1_pre);
	reg_in(dlyresult10_to_xloc1);

	*proxy = MeasRetuMemProxy(*this);
}
MeasRetu_reg MeasRetuMem::incha_max_min_top_base()
{
	reg_in(cha_max_min_top_base);
	return cha_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMem::incha_threshold_l_mid_h()
{
	reg_in(cha_threshold_l_mid_h);
	return cha_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMem::incha_vmax_xloc()
{
	reg_in(cha_vmax_xloc);
	return cha_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMem::incha_vmin_xloc()
{
	reg_in(cha_vmin_xloc);
	return cha_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMem::incha_rtime()
{
	reg_in(cha_rtime);
	return cha_rtime.payload;
}


MeasRetu_reg MeasRetuMem::incha_rtime_xloc_end()
{
	reg_in(cha_rtime_xloc_end);
	return cha_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::incha_rtime_pre()
{
	reg_in(cha_rtime_pre);
	return cha_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMem::incha_rtime_xloc_end_pre()
{
	reg_in(cha_rtime_xloc_end_pre);
	return cha_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::incha_ftime()
{
	reg_in(cha_ftime);
	return cha_ftime.payload;
}


MeasRetu_reg MeasRetuMem::incha_ftime_xloc_end()
{
	reg_in(cha_ftime_xloc_end);
	return cha_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::incha_ftime_pre()
{
	reg_in(cha_ftime_pre);
	return cha_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMem::incha_ftime_xloc_end_pre()
{
	reg_in(cha_ftime_xloc_end_pre);
	return cha_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::incha_cycle_xloc_4()
{
	reg_in(cha_cycle_xloc_4);
	return cha_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::incha_cycle_xloc_3()
{
	reg_in(cha_cycle_xloc_3);
	return cha_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::incha_cycle_xloc_2()
{
	reg_in(cha_cycle_xloc_2);
	return cha_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::incha_cycle_xloc_1()
{
	reg_in(cha_cycle_xloc_1);
	return cha_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::incha_ave_sum_n_bits31_0()
{
	reg_in(cha_ave_sum_n_bits31_0);
	return cha_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::incha_ave_sum_1_1_bits31_0()
{
	reg_in(cha_ave_sum_1_1_bits31_0);
	return cha_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::incha_ave_sum_1_2_bits31_0()
{
	reg_in(cha_ave_sum_1_2_bits31_0);
	return cha_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::incha_ave_sum_x_bit39_32()
{
	reg_in(cha_ave_sum_x_bit39_32);
	return cha_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMem::incha_vrms_sum_n_bits31_0()
{
	reg_in(cha_vrms_sum_n_bits31_0);
	return cha_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::incha_vrms_sum_1_1_bits31_0()
{
	reg_in(cha_vrms_sum_1_1_bits31_0);
	return cha_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::incha_vrms_sum_1_2_bits31_0()
{
	reg_in(cha_vrms_sum_1_2_bits31_0);
	return cha_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::incha_vrms_sum_n_1_bits47_32()
{
	reg_in(cha_vrms_sum_n_1_bits47_32);
	return cha_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::incha_vrms_sum_2_bits47_32()
{
	reg_in(cha_vrms_sum_2_bits47_32);
	return cha_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::incha_total_rf_num()
{
	reg_in(cha_total_rf_num);
	return cha_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMem::inreserved_x1()
{
	reg_in(reserved_x1);
	return reserved_x1;
}


MeasRetu_reg MeasRetuMem::incha_shoot()
{
	reg_in(cha_shoot);
	return cha_shoot.payload;
}


MeasRetu_reg MeasRetuMem::incha_edge_cnt_start_xloc()
{
	reg_in(cha_edge_cnt_start_xloc);
	return cha_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMem::incha_edge_cnt_end_xloc()
{
	reg_in(cha_edge_cnt_end_xloc);
	return cha_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMem::incha_edge_cnt_start2_xloc()
{
	reg_in(cha_edge_cnt_start2_xloc);
	return cha_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::incha_edge_cnt_end2_xloc()
{
	reg_in(cha_edge_cnt_end2_xloc);
	return cha_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchb_max_min_top_base()
{
	reg_in(chb_max_min_top_base);
	return chb_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMem::inchb_threshold_l_mid_h()
{
	reg_in(chb_threshold_l_mid_h);
	return chb_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMem::inchb_vmax_xloc()
{
	reg_in(chb_vmax_xloc);
	return chb_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchb_vmin_xloc()
{
	reg_in(chb_vmin_xloc);
	return chb_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchb_rtime()
{
	reg_in(chb_rtime);
	return chb_rtime.payload;
}


MeasRetu_reg MeasRetuMem::inchb_rtime_xloc_end()
{
	reg_in(chb_rtime_xloc_end);
	return chb_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::inchb_rtime_pre()
{
	reg_in(chb_rtime_pre);
	return chb_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchb_rtime_xloc_end_pre()
{
	reg_in(chb_rtime_xloc_end_pre);
	return chb_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ftime()
{
	reg_in(chb_ftime);
	return chb_ftime.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ftime_xloc_end()
{
	reg_in(chb_ftime_xloc_end);
	return chb_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ftime_pre()
{
	reg_in(chb_ftime_pre);
	return chb_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ftime_xloc_end_pre()
{
	reg_in(chb_ftime_xloc_end_pre);
	return chb_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchb_cycle_xloc_4()
{
	reg_in(chb_cycle_xloc_4);
	return chb_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inchb_cycle_xloc_3()
{
	reg_in(chb_cycle_xloc_3);
	return chb_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inchb_cycle_xloc_2()
{
	reg_in(chb_cycle_xloc_2);
	return chb_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inchb_cycle_xloc_1()
{
	reg_in(chb_cycle_xloc_1);
	return chb_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ave_sum_n_bits31_0()
{
	reg_in(chb_ave_sum_n_bits31_0);
	return chb_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ave_sum_1_1_bits31_0()
{
	reg_in(chb_ave_sum_1_1_bits31_0);
	return chb_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ave_sum_1_2_bits31_0()
{
	reg_in(chb_ave_sum_1_2_bits31_0);
	return chb_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchb_ave_sum_x_bit39_32()
{
	reg_in(chb_ave_sum_x_bit39_32);
	return chb_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMem::inchb_vrms_sum_n_bits31_0()
{
	reg_in(chb_vrms_sum_n_bits31_0);
	return chb_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchb_vrms_sum_1_1_bits31_0()
{
	reg_in(chb_vrms_sum_1_1_bits31_0);
	return chb_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchb_vrms_sum_1_2_bits31_0()
{
	reg_in(chb_vrms_sum_1_2_bits31_0);
	return chb_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchb_vrms_sum_n_1_bits47_32()
{
	reg_in(chb_vrms_sum_n_1_bits47_32);
	return chb_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::inchb_vrms_sum_2_bits47_32()
{
	reg_in(chb_vrms_sum_2_bits47_32);
	return chb_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::inchb_total_rf_num()
{
	reg_in(chb_total_rf_num);
	return chb_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMem::inreserved_x2()
{
	reg_in(reserved_x2);
	return reserved_x2;
}


MeasRetu_reg MeasRetuMem::inchb_shoot()
{
	reg_in(chb_shoot);
	return chb_shoot.payload;
}


MeasRetu_reg MeasRetuMem::inchb_edge_cnt_start_xloc()
{
	reg_in(chb_edge_cnt_start_xloc);
	return chb_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchb_edge_cnt_end_xloc()
{
	reg_in(chb_edge_cnt_end_xloc);
	return chb_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchb_edge_cnt_start2_xloc()
{
	reg_in(chb_edge_cnt_start2_xloc);
	return chb_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchb_edge_cnt_end2_xloc()
{
	reg_in(chb_edge_cnt_end2_xloc);
	return chb_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchc_max_min_top_base()
{
	reg_in(chc_max_min_top_base);
	return chc_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMem::inchc_threshold_l_mid_h()
{
	reg_in(chc_threshold_l_mid_h);
	return chc_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMem::inchc_vmax_xloc()
{
	reg_in(chc_vmax_xloc);
	return chc_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchc_vmin_xloc()
{
	reg_in(chc_vmin_xloc);
	return chc_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchc_rtime()
{
	reg_in(chc_rtime);
	return chc_rtime.payload;
}


MeasRetu_reg MeasRetuMem::inchc_rtime_xloc_end()
{
	reg_in(chc_rtime_xloc_end);
	return chc_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::inchc_rtime_pre()
{
	reg_in(chc_rtime_pre);
	return chc_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchc_rtime_xloc_end_pre()
{
	reg_in(chc_rtime_xloc_end_pre);
	return chc_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ftime()
{
	reg_in(chc_ftime);
	return chc_ftime.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ftime_xloc_end()
{
	reg_in(chc_ftime_xloc_end);
	return chc_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ftime_pre()
{
	reg_in(chc_ftime_pre);
	return chc_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ftime_xloc_end_pre()
{
	reg_in(chc_ftime_xloc_end_pre);
	return chc_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchc_cycle_xloc_4()
{
	reg_in(chc_cycle_xloc_4);
	return chc_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inchc_cycle_xloc_3()
{
	reg_in(chc_cycle_xloc_3);
	return chc_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inchc_cycle_xloc_2()
{
	reg_in(chc_cycle_xloc_2);
	return chc_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inchc_cycle_xloc_1()
{
	reg_in(chc_cycle_xloc_1);
	return chc_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ave_sum_n_bits31_0()
{
	reg_in(chc_ave_sum_n_bits31_0);
	return chc_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ave_sum_1_1_bits31_0()
{
	reg_in(chc_ave_sum_1_1_bits31_0);
	return chc_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ave_sum_1_2_bits31_0()
{
	reg_in(chc_ave_sum_1_2_bits31_0);
	return chc_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchc_ave_sum_x_bit39_32()
{
	reg_in(chc_ave_sum_x_bit39_32);
	return chc_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMem::inchc_vrms_sum_n_bits31_0()
{
	reg_in(chc_vrms_sum_n_bits31_0);
	return chc_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchc_vrms_sum_1_1_bits31_0()
{
	reg_in(chc_vrms_sum_1_1_bits31_0);
	return chc_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchc_vrms_sum_1_2_bits31_0()
{
	reg_in(chc_vrms_sum_1_2_bits31_0);
	return chc_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchc_vrms_sum_n_1_bits47_32()
{
	reg_in(chc_vrms_sum_n_1_bits47_32);
	return chc_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::inchc_vrms_sum_2_bits47_32()
{
	reg_in(chc_vrms_sum_2_bits47_32);
	return chc_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::inchc_total_rf_num()
{
	reg_in(chc_total_rf_num);
	return chc_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMem::inreserved_x3()
{
	reg_in(reserved_x3);
	return reserved_x3;
}


MeasRetu_reg MeasRetuMem::inchc_shoot()
{
	reg_in(chc_shoot);
	return chc_shoot.payload;
}


MeasRetu_reg MeasRetuMem::inchc_edge_cnt_start_xloc()
{
	reg_in(chc_edge_cnt_start_xloc);
	return chc_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchc_edge_cnt_end_xloc()
{
	reg_in(chc_edge_cnt_end_xloc);
	return chc_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchc_edge_cnt_start2_xloc()
{
	reg_in(chc_edge_cnt_start2_xloc);
	return chc_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchc_edge_cnt_end2_xloc()
{
	reg_in(chc_edge_cnt_end2_xloc);
	return chc_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchd_max_min_top_base()
{
	reg_in(chd_max_min_top_base);
	return chd_max_min_top_base.payload;
}


MeasRetu_reg MeasRetuMem::inchd_threshold_l_mid_h()
{
	reg_in(chd_threshold_l_mid_h);
	return chd_threshold_l_mid_h.payload;
}


MeasRetu_reg MeasRetuMem::inchd_vmax_xloc()
{
	reg_in(chd_vmax_xloc);
	return chd_vmax_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchd_vmin_xloc()
{
	reg_in(chd_vmin_xloc);
	return chd_vmin_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchd_rtime()
{
	reg_in(chd_rtime);
	return chd_rtime.payload;
}


MeasRetu_reg MeasRetuMem::inchd_rtime_xloc_end()
{
	reg_in(chd_rtime_xloc_end);
	return chd_rtime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::inchd_rtime_pre()
{
	reg_in(chd_rtime_pre);
	return chd_rtime_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchd_rtime_xloc_end_pre()
{
	reg_in(chd_rtime_xloc_end_pre);
	return chd_rtime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ftime()
{
	reg_in(chd_ftime);
	return chd_ftime.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ftime_xloc_end()
{
	reg_in(chd_ftime_xloc_end);
	return chd_ftime_xloc_end.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ftime_pre()
{
	reg_in(chd_ftime_pre);
	return chd_ftime_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ftime_xloc_end_pre()
{
	reg_in(chd_ftime_xloc_end_pre);
	return chd_ftime_xloc_end_pre.payload;
}


MeasRetu_reg MeasRetuMem::inchd_cycle_xloc_4()
{
	reg_in(chd_cycle_xloc_4);
	return chd_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inchd_cycle_xloc_3()
{
	reg_in(chd_cycle_xloc_3);
	return chd_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inchd_cycle_xloc_2()
{
	reg_in(chd_cycle_xloc_2);
	return chd_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inchd_cycle_xloc_1()
{
	reg_in(chd_cycle_xloc_1);
	return chd_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ave_sum_n_bits31_0()
{
	reg_in(chd_ave_sum_n_bits31_0);
	return chd_ave_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ave_sum_1_1_bits31_0()
{
	reg_in(chd_ave_sum_1_1_bits31_0);
	return chd_ave_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ave_sum_1_2_bits31_0()
{
	reg_in(chd_ave_sum_1_2_bits31_0);
	return chd_ave_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchd_ave_sum_x_bit39_32()
{
	reg_in(chd_ave_sum_x_bit39_32);
	return chd_ave_sum_x_bit39_32.payload;
}


MeasRetu_reg MeasRetuMem::inchd_vrms_sum_n_bits31_0()
{
	reg_in(chd_vrms_sum_n_bits31_0);
	return chd_vrms_sum_n_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchd_vrms_sum_1_1_bits31_0()
{
	reg_in(chd_vrms_sum_1_1_bits31_0);
	return chd_vrms_sum_1_1_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchd_vrms_sum_1_2_bits31_0()
{
	reg_in(chd_vrms_sum_1_2_bits31_0);
	return chd_vrms_sum_1_2_bits31_0.payload;
}


MeasRetu_reg MeasRetuMem::inchd_vrms_sum_n_1_bits47_32()
{
	reg_in(chd_vrms_sum_n_1_bits47_32);
	return chd_vrms_sum_n_1_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::inchd_vrms_sum_2_bits47_32()
{
	reg_in(chd_vrms_sum_2_bits47_32);
	return chd_vrms_sum_2_bits47_32.payload;
}


MeasRetu_reg MeasRetuMem::inchd_total_rf_num()
{
	reg_in(chd_total_rf_num);
	return chd_total_rf_num.payload;
}


MeasRetu_reg MeasRetuMem::inreservd_x4()
{
	reg_in(reservd_x4);
	return reservd_x4;
}


MeasRetu_reg MeasRetuMem::inchd_shoot()
{
	reg_in(chd_shoot);
	return chd_shoot.payload;
}


MeasRetu_reg MeasRetuMem::inchd_edge_cnt_start_xloc()
{
	reg_in(chd_edge_cnt_start_xloc);
	return chd_edge_cnt_start_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchd_edge_cnt_end_xloc()
{
	reg_in(chd_edge_cnt_end_xloc);
	return chd_edge_cnt_end_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchd_edge_cnt_start2_xloc()
{
	reg_in(chd_edge_cnt_start2_xloc);
	return chd_edge_cnt_start2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inchd_edge_cnt_end2_xloc()
{
	reg_in(chd_edge_cnt_end2_xloc);
	return chd_edge_cnt_end2_xloc.payload;
}


MeasRetu_reg MeasRetuMem::inmeas_version_rd()
{
	reg_in(meas_version_rd);
	return meas_version_rd.payload;
}


MeasRetu_reg MeasRetuMem::inmeas_result_done()
{
	reg_in(meas_result_done);
	return meas_result_done.payload;
}


MeasRetu_reg MeasRetuMem::inla0_cycle_xloc_4()
{
	reg_in(la0_cycle_xloc_4);
	return la0_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla0_cycle_xloc_3()
{
	reg_in(la0_cycle_xloc_3);
	return la0_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla0_cycle_xloc_2()
{
	reg_in(la0_cycle_xloc_2);
	return la0_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla0_cycle_xloc_1()
{
	reg_in(la0_cycle_xloc_1);
	return la0_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla1_cycle_xloc_4()
{
	reg_in(la1_cycle_xloc_4);
	return la1_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla1_cycle_xloc_3()
{
	reg_in(la1_cycle_xloc_3);
	return la1_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla1_cycle_xloc_2()
{
	reg_in(la1_cycle_xloc_2);
	return la1_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla1_cycle_xloc_1()
{
	reg_in(la1_cycle_xloc_1);
	return la1_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla2_cycle_xloc_4()
{
	reg_in(la2_cycle_xloc_4);
	return la2_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla2_cycle_xloc_3()
{
	reg_in(la2_cycle_xloc_3);
	return la2_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla2_cycle_xloc_2()
{
	reg_in(la2_cycle_xloc_2);
	return la2_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla2_cycle_xloc_1()
{
	reg_in(la2_cycle_xloc_1);
	return la2_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla3_cycle_xloc_4()
{
	reg_in(la3_cycle_xloc_4);
	return la3_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla3_cycle_xloc_3()
{
	reg_in(la3_cycle_xloc_3);
	return la3_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla3_cycle_xloc_2()
{
	reg_in(la3_cycle_xloc_2);
	return la3_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla3_cycle_xloc_1()
{
	reg_in(la3_cycle_xloc_1);
	return la3_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla4_cycle_xloc_4()
{
	reg_in(la4_cycle_xloc_4);
	return la4_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla4_cycle_xloc_3()
{
	reg_in(la4_cycle_xloc_3);
	return la4_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla4_cycle_xloc_2()
{
	reg_in(la4_cycle_xloc_2);
	return la4_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla4_cycle_xloc_1()
{
	reg_in(la4_cycle_xloc_1);
	return la4_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla5_cycle_xloc_4()
{
	reg_in(la5_cycle_xloc_4);
	return la5_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla5_cycle_xloc_3()
{
	reg_in(la5_cycle_xloc_3);
	return la5_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla5_cycle_xloc_2()
{
	reg_in(la5_cycle_xloc_2);
	return la5_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla5_cycle_xloc_1()
{
	reg_in(la5_cycle_xloc_1);
	return la5_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla6_cycle_xloc_4()
{
	reg_in(la6_cycle_xloc_4);
	return la6_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla6_cycle_xloc_3()
{
	reg_in(la6_cycle_xloc_3);
	return la6_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla6_cycle_xloc_2()
{
	reg_in(la6_cycle_xloc_2);
	return la6_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla6_cycle_xloc_1()
{
	reg_in(la6_cycle_xloc_1);
	return la6_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla7_cycle_xloc_4()
{
	reg_in(la7_cycle_xloc_4);
	return la7_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla7_cycle_xloc_3()
{
	reg_in(la7_cycle_xloc_3);
	return la7_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla7_cycle_xloc_2()
{
	reg_in(la7_cycle_xloc_2);
	return la7_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla7_cycle_xloc_1()
{
	reg_in(la7_cycle_xloc_1);
	return la7_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla8_cycle_xloc_4()
{
	reg_in(la8_cycle_xloc_4);
	return la8_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla8_cycle_xloc_3()
{
	reg_in(la8_cycle_xloc_3);
	return la8_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla8_cycle_xloc_2()
{
	reg_in(la8_cycle_xloc_2);
	return la8_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla8_cycle_xloc_1()
{
	reg_in(la8_cycle_xloc_1);
	return la8_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla9_cycle_xloc_4()
{
	reg_in(la9_cycle_xloc_4);
	return la9_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla9_cycle_xloc_3()
{
	reg_in(la9_cycle_xloc_3);
	return la9_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla9_cycle_xloc_2()
{
	reg_in(la9_cycle_xloc_2);
	return la9_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla9_cycle_xloc_1()
{
	reg_in(la9_cycle_xloc_1);
	return la9_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla10_cycle_xloc_4()
{
	reg_in(la10_cycle_xloc_4);
	return la10_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla10_cycle_xloc_3()
{
	reg_in(la10_cycle_xloc_3);
	return la10_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla10_cycle_xloc_2()
{
	reg_in(la10_cycle_xloc_2);
	return la10_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla10_cycle_xloc_1()
{
	reg_in(la10_cycle_xloc_1);
	return la10_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla11_cycle_xloc_4()
{
	reg_in(la11_cycle_xloc_4);
	return la11_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla11_cycle_xloc_3()
{
	reg_in(la11_cycle_xloc_3);
	return la11_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla11_cycle_xloc_2()
{
	reg_in(la11_cycle_xloc_2);
	return la11_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla11_cycle_xloc_1()
{
	reg_in(la11_cycle_xloc_1);
	return la11_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla12_cycle_xloc_4()
{
	reg_in(la12_cycle_xloc_4);
	return la12_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla12_cycle_xloc_3()
{
	reg_in(la12_cycle_xloc_3);
	return la12_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla12_cycle_xloc_2()
{
	reg_in(la12_cycle_xloc_2);
	return la12_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla12_cycle_xloc_1()
{
	reg_in(la12_cycle_xloc_1);
	return la12_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla13_cycle_xloc_4()
{
	reg_in(la13_cycle_xloc_4);
	return la13_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla13_cycle_xloc_3()
{
	reg_in(la13_cycle_xloc_3);
	return la13_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla13_cycle_xloc_2()
{
	reg_in(la13_cycle_xloc_2);
	return la13_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla13_cycle_xloc_1()
{
	reg_in(la13_cycle_xloc_1);
	return la13_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla14_cycle_xloc_4()
{
	reg_in(la14_cycle_xloc_4);
	return la14_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla14_cycle_xloc_3()
{
	reg_in(la14_cycle_xloc_3);
	return la14_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla14_cycle_xloc_2()
{
	reg_in(la14_cycle_xloc_2);
	return la14_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla14_cycle_xloc_1()
{
	reg_in(la14_cycle_xloc_1);
	return la14_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::inla15_cycle_xloc_4()
{
	reg_in(la15_cycle_xloc_4);
	return la15_cycle_xloc_4.payload;
}


MeasRetu_reg MeasRetuMem::inla15_cycle_xloc_3()
{
	reg_in(la15_cycle_xloc_3);
	return la15_cycle_xloc_3.payload;
}


MeasRetu_reg MeasRetuMem::inla15_cycle_xloc_2()
{
	reg_in(la15_cycle_xloc_2);
	return la15_cycle_xloc_2.payload;
}


MeasRetu_reg MeasRetuMem::inla15_cycle_xloc_1()
{
	reg_in(la15_cycle_xloc_1);
	return la15_cycle_xloc_1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult1_from_xloc2()
{
	reg_in(dlyresult1_from_xloc2);
	return dlyresult1_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult1_from_xloc1()
{
	reg_in(dlyresult1_from_xloc1);
	return dlyresult1_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult1_to_xloc2_pre()
{
	reg_in(dlyresult1_to_xloc2_pre);
	return dlyresult1_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult1_to_xloc2()
{
	reg_in(dlyresult1_to_xloc2);
	return dlyresult1_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult1_to_xloc1_pre()
{
	reg_in(dlyresult1_to_xloc1_pre);
	return dlyresult1_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult1_to_xloc1()
{
	reg_in(dlyresult1_to_xloc1);
	return dlyresult1_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult2_from_xloc2()
{
	reg_in(dlyresult2_from_xloc2);
	return dlyresult2_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult2_from_xloc1()
{
	reg_in(dlyresult2_from_xloc1);
	return dlyresult2_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult2_to_xloc2_pre()
{
	reg_in(dlyresult2_to_xloc2_pre);
	return dlyresult2_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult2_to_xloc2()
{
	reg_in(dlyresult2_to_xloc2);
	return dlyresult2_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult2_to_xloc1_pre()
{
	reg_in(dlyresult2_to_xloc1_pre);
	return dlyresult2_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult2_to_xloc1()
{
	reg_in(dlyresult2_to_xloc1);
	return dlyresult2_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult3_from_xloc2()
{
	reg_in(dlyresult3_from_xloc2);
	return dlyresult3_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult3_from_xloc1()
{
	reg_in(dlyresult3_from_xloc1);
	return dlyresult3_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult3_to_xloc2_pre()
{
	reg_in(dlyresult3_to_xloc2_pre);
	return dlyresult3_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult3_to_xloc2()
{
	reg_in(dlyresult3_to_xloc2);
	return dlyresult3_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult3_to_xloc1_pre()
{
	reg_in(dlyresult3_to_xloc1_pre);
	return dlyresult3_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult3_to_xloc1()
{
	reg_in(dlyresult3_to_xloc1);
	return dlyresult3_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult4_from_xloc2()
{
	reg_in(dlyresult4_from_xloc2);
	return dlyresult4_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult4_from_xloc1()
{
	reg_in(dlyresult4_from_xloc1);
	return dlyresult4_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult4_to_xloc2_pre()
{
	reg_in(dlyresult4_to_xloc2_pre);
	return dlyresult4_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult4_to_xloc2()
{
	reg_in(dlyresult4_to_xloc2);
	return dlyresult4_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult4_to_xloc1_pre()
{
	reg_in(dlyresult4_to_xloc1_pre);
	return dlyresult4_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult4_to_xloc1()
{
	reg_in(dlyresult4_to_xloc1);
	return dlyresult4_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult5_from_xloc2()
{
	reg_in(dlyresult5_from_xloc2);
	return dlyresult5_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult5_from_xloc1()
{
	reg_in(dlyresult5_from_xloc1);
	return dlyresult5_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult5_to_xloc2_pre()
{
	reg_in(dlyresult5_to_xloc2_pre);
	return dlyresult5_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult5_to_xloc2()
{
	reg_in(dlyresult5_to_xloc2);
	return dlyresult5_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult5_to_xloc1_pre()
{
	reg_in(dlyresult5_to_xloc1_pre);
	return dlyresult5_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult5_to_xloc1()
{
	reg_in(dlyresult5_to_xloc1);
	return dlyresult5_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult6_from_xloc2()
{
	reg_in(dlyresult6_from_xloc2);
	return dlyresult6_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult6_from_xloc1()
{
	reg_in(dlyresult6_from_xloc1);
	return dlyresult6_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult6_to_xloc2_pre()
{
	reg_in(dlyresult6_to_xloc2_pre);
	return dlyresult6_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult6_to_xloc2()
{
	reg_in(dlyresult6_to_xloc2);
	return dlyresult6_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult6_to_xloc1_pre()
{
	reg_in(dlyresult6_to_xloc1_pre);
	return dlyresult6_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult6_to_xloc1()
{
	reg_in(dlyresult6_to_xloc1);
	return dlyresult6_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult7_from_xloc2()
{
	reg_in(dlyresult7_from_xloc2);
	return dlyresult7_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult7_from_xloc1()
{
	reg_in(dlyresult7_from_xloc1);
	return dlyresult7_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult7_to_xloc2_pre()
{
	reg_in(dlyresult7_to_xloc2_pre);
	return dlyresult7_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult7_to_xloc2()
{
	reg_in(dlyresult7_to_xloc2);
	return dlyresult7_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult7_to_xloc1_pre()
{
	reg_in(dlyresult7_to_xloc1_pre);
	return dlyresult7_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult7_to_xloc1()
{
	reg_in(dlyresult7_to_xloc1);
	return dlyresult7_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult8_from_xloc2()
{
	reg_in(dlyresult8_from_xloc2);
	return dlyresult8_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult8_from_xloc1()
{
	reg_in(dlyresult8_from_xloc1);
	return dlyresult8_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult8_to_xloc2_pre()
{
	reg_in(dlyresult8_to_xloc2_pre);
	return dlyresult8_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult8_to_xloc2()
{
	reg_in(dlyresult8_to_xloc2);
	return dlyresult8_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult8_to_xloc1_pre()
{
	reg_in(dlyresult8_to_xloc1_pre);
	return dlyresult8_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult8_to_xloc1()
{
	reg_in(dlyresult8_to_xloc1);
	return dlyresult8_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult9_from_xloc2()
{
	reg_in(dlyresult9_from_xloc2);
	return dlyresult9_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult9_from_xloc1()
{
	reg_in(dlyresult9_from_xloc1);
	return dlyresult9_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult9_to_xloc2_pre()
{
	reg_in(dlyresult9_to_xloc2_pre);
	return dlyresult9_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult9_to_xloc2()
{
	reg_in(dlyresult9_to_xloc2);
	return dlyresult9_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult9_to_xloc1_pre()
{
	reg_in(dlyresult9_to_xloc1_pre);
	return dlyresult9_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult9_to_xloc1()
{
	reg_in(dlyresult9_to_xloc1);
	return dlyresult9_to_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult10_from_xloc2()
{
	reg_in(dlyresult10_from_xloc2);
	return dlyresult10_from_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult10_from_xloc1()
{
	reg_in(dlyresult10_from_xloc1);
	return dlyresult10_from_xloc1.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult10_to_xloc2_pre()
{
	reg_in(dlyresult10_to_xloc2_pre);
	return dlyresult10_to_xloc2_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult10_to_xloc2()
{
	reg_in(dlyresult10_to_xloc2);
	return dlyresult10_to_xloc2.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult10_to_xloc1_pre()
{
	reg_in(dlyresult10_to_xloc1_pre);
	return dlyresult10_to_xloc1_pre.payload;
}


MeasRetu_reg MeasRetuMem::indlyresult10_to_xloc1()
{
	reg_in(dlyresult10_to_xloc1);
	return dlyresult10_to_xloc1.payload;
}


cache_addr MeasRetuMem::addr_cha_max_min_top_base(){ return 0x2400+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_threshold_l_mid_h(){ return 0x2404+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_vmax_xloc(){ return 0x2408+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_vmin_xloc(){ return 0x240c+mAddrBase; }

cache_addr MeasRetuMem::addr_cha_rtime(){ return 0x2410+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_rtime_xloc_end(){ return 0x2414+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_rtime_pre(){ return 0x2418+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_rtime_xloc_end_pre(){ return 0x241c+mAddrBase; }

cache_addr MeasRetuMem::addr_cha_ftime(){ return 0x2420+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_ftime_xloc_end(){ return 0x2424+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_ftime_pre(){ return 0x2428+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_ftime_xloc_end_pre(){ return 0x242c+mAddrBase; }

cache_addr MeasRetuMem::addr_cha_cycle_xloc_4(){ return 0x2430+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_cycle_xloc_3(){ return 0x2434+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_cycle_xloc_2(){ return 0x2438+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_cycle_xloc_1(){ return 0x243c+mAddrBase; }

cache_addr MeasRetuMem::addr_cha_ave_sum_n_bits31_0(){ return 0x2440+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_ave_sum_1_1_bits31_0(){ return 0x2444+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_ave_sum_1_2_bits31_0(){ return 0x2448+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_ave_sum_x_bit39_32(){ return 0x244c+mAddrBase; }

cache_addr MeasRetuMem::addr_cha_vrms_sum_n_bits31_0(){ return 0x2450+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_vrms_sum_1_1_bits31_0(){ return 0x2454+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_vrms_sum_1_2_bits31_0(){ return 0x2458+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_vrms_sum_n_1_bits47_32(){ return 0x245c+mAddrBase; }

cache_addr MeasRetuMem::addr_cha_vrms_sum_2_bits47_32(){ return 0x2460+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_total_rf_num(){ return 0x2464+mAddrBase; }
cache_addr MeasRetuMem::addr_reserved_x1(){ return 0x2468+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_shoot(){ return 0x246c+mAddrBase; }

cache_addr MeasRetuMem::addr_cha_edge_cnt_start_xloc(){ return 0x2470+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_edge_cnt_end_xloc(){ return 0x2474+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_edge_cnt_start2_xloc(){ return 0x2478+mAddrBase; }
cache_addr MeasRetuMem::addr_cha_edge_cnt_end2_xloc(){ return 0x247c+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_max_min_top_base(){ return 0x2480+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_threshold_l_mid_h(){ return 0x2484+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_vmax_xloc(){ return 0x2488+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_vmin_xloc(){ return 0x248c+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_rtime(){ return 0x2490+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_rtime_xloc_end(){ return 0x2494+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_rtime_pre(){ return 0x2498+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_rtime_xloc_end_pre(){ return 0x249c+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_ftime(){ return 0x24a0+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_ftime_xloc_end(){ return 0x24a4+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_ftime_pre(){ return 0x24a8+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_ftime_xloc_end_pre(){ return 0x24ac+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_cycle_xloc_4(){ return 0x24b0+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_cycle_xloc_3(){ return 0x24b4+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_cycle_xloc_2(){ return 0x24b8+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_cycle_xloc_1(){ return 0x24bc+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_ave_sum_n_bits31_0(){ return 0x24c0+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_ave_sum_1_1_bits31_0(){ return 0x24c4+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_ave_sum_1_2_bits31_0(){ return 0x24c8+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_ave_sum_x_bit39_32(){ return 0x24cc+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_vrms_sum_n_bits31_0(){ return 0x24d0+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_vrms_sum_1_1_bits31_0(){ return 0x24d4+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_vrms_sum_1_2_bits31_0(){ return 0x24d8+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_vrms_sum_n_1_bits47_32(){ return 0x24dc+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_vrms_sum_2_bits47_32(){ return 0x24e0+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_total_rf_num(){ return 0x24e4+mAddrBase; }
cache_addr MeasRetuMem::addr_reserved_x2(){ return 0x24e8+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_shoot(){ return 0x24ec+mAddrBase; }

cache_addr MeasRetuMem::addr_chb_edge_cnt_start_xloc(){ return 0x24f0+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_edge_cnt_end_xloc(){ return 0x24f4+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_edge_cnt_start2_xloc(){ return 0x24f8+mAddrBase; }
cache_addr MeasRetuMem::addr_chb_edge_cnt_end2_xloc(){ return 0x24fc+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_max_min_top_base(){ return 0x2500+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_threshold_l_mid_h(){ return 0x2504+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_vmax_xloc(){ return 0x2508+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_vmin_xloc(){ return 0x250c+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_rtime(){ return 0x2510+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_rtime_xloc_end(){ return 0x2514+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_rtime_pre(){ return 0x2518+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_rtime_xloc_end_pre(){ return 0x251c+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_ftime(){ return 0x2520+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_ftime_xloc_end(){ return 0x2524+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_ftime_pre(){ return 0x2528+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_ftime_xloc_end_pre(){ return 0x252c+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_cycle_xloc_4(){ return 0x2530+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_cycle_xloc_3(){ return 0x2534+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_cycle_xloc_2(){ return 0x2538+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_cycle_xloc_1(){ return 0x253c+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_ave_sum_n_bits31_0(){ return 0x2540+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_ave_sum_1_1_bits31_0(){ return 0x2544+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_ave_sum_1_2_bits31_0(){ return 0x2548+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_ave_sum_x_bit39_32(){ return 0x254c+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_vrms_sum_n_bits31_0(){ return 0x2550+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_vrms_sum_1_1_bits31_0(){ return 0x2554+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_vrms_sum_1_2_bits31_0(){ return 0x2558+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_vrms_sum_n_1_bits47_32(){ return 0x255c+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_vrms_sum_2_bits47_32(){ return 0x2560+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_total_rf_num(){ return 0x2564+mAddrBase; }
cache_addr MeasRetuMem::addr_reserved_x3(){ return 0x2568+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_shoot(){ return 0x256c+mAddrBase; }

cache_addr MeasRetuMem::addr_chc_edge_cnt_start_xloc(){ return 0x2570+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_edge_cnt_end_xloc(){ return 0x2574+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_edge_cnt_start2_xloc(){ return 0x2578+mAddrBase; }
cache_addr MeasRetuMem::addr_chc_edge_cnt_end2_xloc(){ return 0x257c+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_max_min_top_base(){ return 0x2580+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_threshold_l_mid_h(){ return 0x2584+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_vmax_xloc(){ return 0x2588+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_vmin_xloc(){ return 0x258c+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_rtime(){ return 0x2590+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_rtime_xloc_end(){ return 0x2594+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_rtime_pre(){ return 0x2598+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_rtime_xloc_end_pre(){ return 0x259c+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_ftime(){ return 0x25a0+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_ftime_xloc_end(){ return 0x25a4+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_ftime_pre(){ return 0x25a8+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_ftime_xloc_end_pre(){ return 0x25ac+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_cycle_xloc_4(){ return 0x25b0+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_cycle_xloc_3(){ return 0x25b4+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_cycle_xloc_2(){ return 0x25b8+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_cycle_xloc_1(){ return 0x25bc+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_ave_sum_n_bits31_0(){ return 0x25c0+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_ave_sum_1_1_bits31_0(){ return 0x25c4+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_ave_sum_1_2_bits31_0(){ return 0x25c8+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_ave_sum_x_bit39_32(){ return 0x25cc+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_vrms_sum_n_bits31_0(){ return 0x25d0+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_vrms_sum_1_1_bits31_0(){ return 0x25d4+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_vrms_sum_1_2_bits31_0(){ return 0x25d8+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_vrms_sum_n_1_bits47_32(){ return 0x25dc+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_vrms_sum_2_bits47_32(){ return 0x25e0+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_total_rf_num(){ return 0x25e4+mAddrBase; }
cache_addr MeasRetuMem::addr_reservd_x4(){ return 0x25e8+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_shoot(){ return 0x25ec+mAddrBase; }

cache_addr MeasRetuMem::addr_chd_edge_cnt_start_xloc(){ return 0x25f0+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_edge_cnt_end_xloc(){ return 0x25f4+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_edge_cnt_start2_xloc(){ return 0x25f8+mAddrBase; }
cache_addr MeasRetuMem::addr_chd_edge_cnt_end2_xloc(){ return 0x25fc+mAddrBase; }

cache_addr MeasRetuMem::addr_meas_version_rd(){ return 0x27f8+mAddrBase; }
cache_addr MeasRetuMem::addr_meas_result_done(){ return 0x27fc+mAddrBase; }
cache_addr MeasRetuMem::addr_la0_cycle_xloc_4(){ return 0x2600+mAddrBase; }
cache_addr MeasRetuMem::addr_la0_cycle_xloc_3(){ return 0x2604+mAddrBase; }

cache_addr MeasRetuMem::addr_la0_cycle_xloc_2(){ return 0x2608+mAddrBase; }
cache_addr MeasRetuMem::addr_la0_cycle_xloc_1(){ return 0x260c+mAddrBase; }
cache_addr MeasRetuMem::addr_la1_cycle_xloc_4(){ return 0x2600+mAddrBase; }
cache_addr MeasRetuMem::addr_la1_cycle_xloc_3(){ return 0x2614+mAddrBase; }

cache_addr MeasRetuMem::addr_la1_cycle_xloc_2(){ return 0x2618+mAddrBase; }
cache_addr MeasRetuMem::addr_la1_cycle_xloc_1(){ return 0x261c+mAddrBase; }
cache_addr MeasRetuMem::addr_la2_cycle_xloc_4(){ return 0x2620+mAddrBase; }
cache_addr MeasRetuMem::addr_la2_cycle_xloc_3(){ return 0x2624+mAddrBase; }

cache_addr MeasRetuMem::addr_la2_cycle_xloc_2(){ return 0x2628+mAddrBase; }
cache_addr MeasRetuMem::addr_la2_cycle_xloc_1(){ return 0x262c+mAddrBase; }
cache_addr MeasRetuMem::addr_la3_cycle_xloc_4(){ return 0x2630+mAddrBase; }
cache_addr MeasRetuMem::addr_la3_cycle_xloc_3(){ return 0x2634+mAddrBase; }

cache_addr MeasRetuMem::addr_la3_cycle_xloc_2(){ return 0x2638+mAddrBase; }
cache_addr MeasRetuMem::addr_la3_cycle_xloc_1(){ return 0x263c+mAddrBase; }
cache_addr MeasRetuMem::addr_la4_cycle_xloc_4(){ return 0x2640+mAddrBase; }
cache_addr MeasRetuMem::addr_la4_cycle_xloc_3(){ return 0x2644+mAddrBase; }

cache_addr MeasRetuMem::addr_la4_cycle_xloc_2(){ return 0x2648+mAddrBase; }
cache_addr MeasRetuMem::addr_la4_cycle_xloc_1(){ return 0x264c+mAddrBase; }
cache_addr MeasRetuMem::addr_la5_cycle_xloc_4(){ return 0x2650+mAddrBase; }
cache_addr MeasRetuMem::addr_la5_cycle_xloc_3(){ return 0x2654+mAddrBase; }

cache_addr MeasRetuMem::addr_la5_cycle_xloc_2(){ return 0x2658+mAddrBase; }
cache_addr MeasRetuMem::addr_la5_cycle_xloc_1(){ return 0x265c+mAddrBase; }
cache_addr MeasRetuMem::addr_la6_cycle_xloc_4(){ return 0x2660+mAddrBase; }
cache_addr MeasRetuMem::addr_la6_cycle_xloc_3(){ return 0x2664+mAddrBase; }

cache_addr MeasRetuMem::addr_la6_cycle_xloc_2(){ return 0x2668+mAddrBase; }
cache_addr MeasRetuMem::addr_la6_cycle_xloc_1(){ return 0x266c+mAddrBase; }
cache_addr MeasRetuMem::addr_la7_cycle_xloc_4(){ return 0x2670+mAddrBase; }
cache_addr MeasRetuMem::addr_la7_cycle_xloc_3(){ return 0x2674+mAddrBase; }

cache_addr MeasRetuMem::addr_la7_cycle_xloc_2(){ return 0x2678+mAddrBase; }
cache_addr MeasRetuMem::addr_la7_cycle_xloc_1(){ return 0x267c+mAddrBase; }
cache_addr MeasRetuMem::addr_la8_cycle_xloc_4(){ return 0x2680+mAddrBase; }
cache_addr MeasRetuMem::addr_la8_cycle_xloc_3(){ return 0x2684+mAddrBase; }

cache_addr MeasRetuMem::addr_la8_cycle_xloc_2(){ return 0x2688+mAddrBase; }
cache_addr MeasRetuMem::addr_la8_cycle_xloc_1(){ return 0x268c+mAddrBase; }
cache_addr MeasRetuMem::addr_la9_cycle_xloc_4(){ return 0x2690+mAddrBase; }
cache_addr MeasRetuMem::addr_la9_cycle_xloc_3(){ return 0x2694+mAddrBase; }

cache_addr MeasRetuMem::addr_la9_cycle_xloc_2(){ return 0x2698+mAddrBase; }
cache_addr MeasRetuMem::addr_la9_cycle_xloc_1(){ return 0x269c+mAddrBase; }
cache_addr MeasRetuMem::addr_la10_cycle_xloc_4(){ return 0x26a0+mAddrBase; }
cache_addr MeasRetuMem::addr_la10_cycle_xloc_3(){ return 0x26a4+mAddrBase; }

cache_addr MeasRetuMem::addr_la10_cycle_xloc_2(){ return 0x26a8+mAddrBase; }
cache_addr MeasRetuMem::addr_la10_cycle_xloc_1(){ return 0x26ac+mAddrBase; }
cache_addr MeasRetuMem::addr_la11_cycle_xloc_4(){ return 0x26b0+mAddrBase; }
cache_addr MeasRetuMem::addr_la11_cycle_xloc_3(){ return 0x26b4+mAddrBase; }

cache_addr MeasRetuMem::addr_la11_cycle_xloc_2(){ return 0x26b8+mAddrBase; }
cache_addr MeasRetuMem::addr_la11_cycle_xloc_1(){ return 0x26bc+mAddrBase; }
cache_addr MeasRetuMem::addr_la12_cycle_xloc_4(){ return 0x26c0+mAddrBase; }
cache_addr MeasRetuMem::addr_la12_cycle_xloc_3(){ return 0x26c4+mAddrBase; }

cache_addr MeasRetuMem::addr_la12_cycle_xloc_2(){ return 0x26c8+mAddrBase; }
cache_addr MeasRetuMem::addr_la12_cycle_xloc_1(){ return 0x26cc+mAddrBase; }
cache_addr MeasRetuMem::addr_la13_cycle_xloc_4(){ return 0x26d0+mAddrBase; }
cache_addr MeasRetuMem::addr_la13_cycle_xloc_3(){ return 0x26d4+mAddrBase; }

cache_addr MeasRetuMem::addr_la13_cycle_xloc_2(){ return 0x26d8+mAddrBase; }
cache_addr MeasRetuMem::addr_la13_cycle_xloc_1(){ return 0x26dc+mAddrBase; }
cache_addr MeasRetuMem::addr_la14_cycle_xloc_4(){ return 0x26e0+mAddrBase; }
cache_addr MeasRetuMem::addr_la14_cycle_xloc_3(){ return 0x26e4+mAddrBase; }

cache_addr MeasRetuMem::addr_la14_cycle_xloc_2(){ return 0x26e8+mAddrBase; }
cache_addr MeasRetuMem::addr_la14_cycle_xloc_1(){ return 0x26ec+mAddrBase; }
cache_addr MeasRetuMem::addr_la15_cycle_xloc_4(){ return 0x26f0+mAddrBase; }
cache_addr MeasRetuMem::addr_la15_cycle_xloc_3(){ return 0x26f4+mAddrBase; }

cache_addr MeasRetuMem::addr_la15_cycle_xloc_2(){ return 0x26f8+mAddrBase; }
cache_addr MeasRetuMem::addr_la15_cycle_xloc_1(){ return 0x26fc+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult1_from_xloc2(){ return 0x2700+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult1_from_xloc1(){ return 0x2704+mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult1_to_xloc2_pre(){ return 0x2708+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult1_to_xloc2(){ return 0x270c+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult1_to_xloc1_pre(){ return 0x2710+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult1_to_xloc1(){ return 0x2714+mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult2_from_xloc2(){ return 0x2718+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult2_from_xloc1(){ return 0x271c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult2_to_xloc2_pre(){ return 0x2720   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult2_to_xloc2(){ return 0x2724   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult2_to_xloc1_pre(){ return 0x2728   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult2_to_xloc1(){ return 0x272c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult3_from_xloc2(){ return 0x2730+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult3_from_xloc1(){ return 0x2734   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult3_to_xloc2_pre(){ return 0x2738   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult3_to_xloc2(){ return 0x273c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult3_to_xloc1_pre(){ return 0x2740   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult3_to_xloc1(){ return 0x2744   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult4_from_xloc2(){ return 0x2748+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult4_from_xloc1(){ return 0x274c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult4_to_xloc2_pre(){ return 0x2750   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult4_to_xloc2(){ return 0x2754   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult4_to_xloc1_pre(){ return 0x2758   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult4_to_xloc1(){ return 0x275c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult5_from_xloc2(){ return 0x2760+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult5_from_xloc1(){ return 0x2764   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult5_to_xloc2_pre(){ return 0x2768   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult5_to_xloc2(){ return 0x276c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult5_to_xloc1_pre(){ return 0x2770   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult5_to_xloc1(){ return 0x2774   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult6_from_xloc2(){ return 0x2778+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult6_from_xloc1(){ return 0x277c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult6_to_xloc2_pre(){ return 0x2780   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult6_to_xloc2(){ return 0x2784   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult6_to_xloc1_pre(){ return 0x2788   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult6_to_xloc1(){ return 0x278c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult7_from_xloc2(){ return 0x2790+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult7_from_xloc1(){ return 0x2794   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult7_to_xloc2_pre(){ return 0x2798   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult7_to_xloc2(){ return 0x279c   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult7_to_xloc1_pre(){ return 0x27a0   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult7_to_xloc1(){ return 0x27a4   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult8_from_xloc2(){ return 0x27a8+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult8_from_xloc1(){ return 0x27ac   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult8_to_xloc2_pre(){ return 0x27b0   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult8_to_xloc2(){ return 0x27b4   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult8_to_xloc1_pre(){ return 0x27b8   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult8_to_xloc1(){ return 0x27bc   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult9_from_xloc2(){ return 0x27c0+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult9_from_xloc1(){ return 0x27c4   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult9_to_xloc2_pre(){ return 0x27c8   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult9_to_xloc2(){ return 0x27cc   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult9_to_xloc1_pre(){ return 0x27d0   +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult9_to_xloc1(){ return 0x27d4   +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult10_from_xloc2(){ return 0x27d8+mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult10_from_xloc1(){ return 0x27dc    +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult10_to_xloc2_pre(){ return 0x27e0    +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult10_to_xloc2(){ return 0x27e4    +mAddrBase; }

cache_addr MeasRetuMem::addr_dlyresult10_to_xloc1_pre(){ return 0x27e8    +mAddrBase; }
cache_addr MeasRetuMem::addr_dlyresult10_to_xloc1(){ return 0x27ec    +mAddrBase; }


