#ifndef _TPU_IC_H_
#define _TPU_IC_H_

#define Tpu_reg unsigned int

union Tpu_ch1_cmp_level{
	struct{
	Tpu_reg lvl_1_h_a:8;
	Tpu_reg lvl_1_l_a:8;
	Tpu_reg lvl_1_h_b:8;
	Tpu_reg lvl_1_l_b:8;
	};
	Tpu_reg payload;
};

union Tpu_ch2_cmp_level{
	struct{
	Tpu_reg lvl_2_h_a:8;
	Tpu_reg lvl_2_l_a:8;
	Tpu_reg lvl_2_h_b:8;
	Tpu_reg lvl_2_l_b:8;
	};
	Tpu_reg payload;
};

union Tpu_ch3_cmp_level{
	struct{
	Tpu_reg lvl_3_h_a:8;
	Tpu_reg lvl_3_l_a:8;
	Tpu_reg lvl_3_h_b:8;
	Tpu_reg lvl_3_l_b:8;
	};
	Tpu_reg payload;
};

union Tpu_ch4_cmp_level{
	struct{
	Tpu_reg lvl_4_h_a:8;
	Tpu_reg lvl_4_l_a:8;
	Tpu_reg lvl_4_h_b:8;
	Tpu_reg lvl_4_l_b:8;
	};
	Tpu_reg payload;
};

union Tpu_A_event_reg1{
	struct{
	Tpu_reg A_event_logic_la_sel:32;
	};
	Tpu_reg payload;
};

union Tpu_A_event_reg2{
	struct{
	Tpu_reg A_event_logic_analog_sel:10;
	Tpu_reg A_event_logic_func:2;
	};
	Tpu_reg payload;
};

union Tpu_A_event_reg3{
	struct{
	Tpu_reg A_event_cmpa_sel:6;
	Tpu_reg A_event_cmpa_mode:2;
	Tpu_reg A_event_cmpb_sel:6;
	Tpu_reg A_event_cmpb_mode:2;

	Tpu_reg A_event_clk_sel:3;
	Tpu_reg A_event_clk_sel_inv:1;
	Tpu_reg A_event_dat_sel:3;
	Tpu_reg A_event_dat_sel_inv:1;
	};
	Tpu_reg payload;
};

union Tpu_A_event_reg4{
	struct{
	Tpu_reg A_event_width_cmp_1_31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_A_event_reg5{
	struct{
	Tpu_reg A_event_width_cmp_g_31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_A_event_reg6{
	struct{
	Tpu_reg A_event_width_cmp_1_39_32:8;
	Tpu_reg A_event_width_cmp_g_39_32:8;
	Tpu_reg A_event_width_cmp_mode:3;
	};
	Tpu_reg payload;
};

union Tpu_B_event_reg1{
	struct{
	Tpu_reg B_event_logic_la_sel:32;
	};
	Tpu_reg payload;
};

union Tpu_B_event_reg2{
	struct{
	Tpu_reg B_event_logic_analog_sel:10;
	Tpu_reg B_event_logic_func:2;
	};
	Tpu_reg payload;
};

union Tpu_B_event_reg3{
	struct{
	Tpu_reg B_event_cmpa_sel:6;
	Tpu_reg B_event_cmpa_mode:2;
	Tpu_reg B_event_cmpb_sel:6;
	Tpu_reg B_event_cmpb_mode:2;

	Tpu_reg B_event_clk_sel:3;
	Tpu_reg B_event_clk_sel_inv:1;
	Tpu_reg B_event_dat_sel:3;
	Tpu_reg B_event_dat_sel_inv:1;
	};
	Tpu_reg payload;
};

union Tpu_B_event_reg4{
	struct{
	Tpu_reg B_event_width_cmp_1_31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_B_event_reg5{
	struct{
	Tpu_reg B_event_width_cmp_g_31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_B_event_reg6{
	struct{
	Tpu_reg B_event_width_cmp_1_39_32:8;
	Tpu_reg B_event_width_cmp_g_39_32:8;
	Tpu_reg B_event_width_cmp_mode:3;
	};
	Tpu_reg payload;
};

union Tpu_R_event_reg1{
	struct{
	Tpu_reg R_event_logic_la_sel:32;
	};
	Tpu_reg payload;
};

union Tpu_R_event_reg2{
	struct{
	Tpu_reg R_event_logic_analog_sel:10;
	Tpu_reg R_event_logic_func:2;
	};
	Tpu_reg payload;
};

union Tpu_R_event_reg3{
	struct{
	Tpu_reg R_event_cmpa_sel:6;
	Tpu_reg R_event_cmpa_mode:2;
	Tpu_reg R_event_cmpb_sel:6;
	Tpu_reg R_event_cmpb_mode:2;

	Tpu_reg R_event_clk_sel:3;
	Tpu_reg R_event_clk_sel_inv:1;
	Tpu_reg R_event_dat_sel:3;
	Tpu_reg R_event_dat_sel_inv:1;
	};
	Tpu_reg payload;
};

union Tpu_R_event_reg4{
	struct{
	Tpu_reg R_event_width_cmp_1_31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_R_event_reg5{
	struct{
	Tpu_reg R_event_width_cmp_g_31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_R_event_reg6{
	struct{
	Tpu_reg R_event_width_cmp_1_39_32:8;
	Tpu_reg R_event_width_cmp_g_39_32:8;
	Tpu_reg R_event_width_cmp_mode:3;
	};
	Tpu_reg payload;
};

union Tpu_tpu_trig_mux{
	struct{
	Tpu_reg tpu_trig_type:4;
	Tpu_reg trig_rst:1;
	Tpu_reg force_trig:1;
	Tpu_reg abr_seq_loop_en:1;

	Tpu_reg abr_seq_delay_on:1;
	Tpu_reg abr_seq_timeout_on:1;
	Tpu_reg abr_seq_r_event_on:1;
	Tpu_reg seq_abr_trig_a_only:1;
	};
	Tpu_reg payload;
};

union Tpu_abr_delay_time_l{
	struct{
	Tpu_reg abr_delay_time_l:32;
	};
	Tpu_reg payload;
};

union Tpu_abr_delay_time_h{
	struct{
	Tpu_reg abr_delay_time_h:8;
	};
	Tpu_reg payload;
};

union Tpu_abr_timeout_l{
	struct{
	Tpu_reg abr_timeout_l:32;
	};
	Tpu_reg payload;
};

union Tpu_abr_timeout_h{
	struct{
	Tpu_reg abr_timeout_h:8;
	};
	Tpu_reg payload;
};

union Tpu_holdoff_time_l{
	struct{
	Tpu_reg holdoff_time_l:32;
	};
	Tpu_reg payload;
};

union Tpu_holdoff_time_h{
	struct{
	Tpu_reg holdoff_time_h:8;
	Tpu_reg holdoff_type:3;
	};
	Tpu_reg payload;
};

union Tpu_iir_set{
	struct{
	Tpu_reg iir_type:2;
	Tpu_reg iir_src:2;
	Tpu_reg iir_offset:10;
	};
	Tpu_reg payload;
};

union Tpu_uart_cfg_1{
	struct{
	Tpu_reg uart_clk_div:21;
	Tpu_reg uart_trig_type:2;
	Tpu_reg uart_trig_dat:3;
	Tpu_reg uart_trig_err:2;

	Tpu_reg uart_eof:2;
	Tpu_reg uart_parity:2;
	};
	Tpu_reg payload;
};

union Tpu_uart_cfg_2{
	struct{
	Tpu_reg uart_len:3;
	Tpu_reg uart_dat_min:8;
	Tpu_reg uart_dat_max:8;
	};
	Tpu_reg payload;
};

union Tpu_iis_cfg_1{
	struct{
	Tpu_reg iis_trig_type:3;
	Tpu_reg iis_trig_channel_sel:2;
	Tpu_reg iis_trig_bus_type:2;
	Tpu_reg iis_trig_bitnum_vld:6;

	Tpu_reg iis_trig_bitnum_total:6;
	};
	Tpu_reg payload;
};

union Tpu_iis_cfg_2{
	struct{
	Tpu_reg iis_trig_word_min:32;
	};
	Tpu_reg payload;
};

union Tpu_iis_cfg_3{
	struct{
	Tpu_reg iis_trig_word_max:32;
	};
	Tpu_reg payload;
};

union Tpu_iis_cfg_4{
	struct{
	Tpu_reg iis_trig_word_mask:32;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_1{
	struct{
	Tpu_reg lin_trig_type:3;
	Tpu_reg lin_trig_type_id:3;
	Tpu_reg lin_trig_type_dat:3;
	Tpu_reg lin_trig_type_err:2;

	Tpu_reg lin_trig_spec:2;
	Tpu_reg lin_trig_id_min:6;
	Tpu_reg lin_trig_id_max:6;
	Tpu_reg lin_trig_dat_cmpnum:4;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_2{
	struct{
	Tpu_reg lin_trig_clk_div:20;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_3{
	struct{
	Tpu_reg lin_trig_sa_pos:20;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_4{
	struct{
	Tpu_reg lin_trig_dat_min_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_5{
	struct{
	Tpu_reg lin_trig_dat_min_bits63_32:32;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_6{
	struct{
	Tpu_reg lin_trig_dat_max_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_7{
	struct{
	Tpu_reg lin_trig_dat_max_bits63_32:32;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_8{
	struct{
	Tpu_reg lin_trig_dat_mask_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_lin_cfg_9{
	struct{
	Tpu_reg lin_trig_dat_maak_bits63_32:32;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_1{
	struct{
	Tpu_reg can_trig_type:2;
	Tpu_reg can_trig_type_field:3;
	Tpu_reg can_trig_type_frame:2;
	Tpu_reg can_trig_type_err:3;

	Tpu_reg can_trig_type_id:3;
	Tpu_reg can_trig_type_id_mask:1;
	Tpu_reg can_trig_type_dat:3;
	Tpu_reg can_trig_type_dat_mask:1;

	Tpu_reg can_trig_bus_type:2;
	Tpu_reg can_trig_std_extend_sel:1;
	Tpu_reg can_trig_dat_cmpnum:4;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_2{
	struct{
	Tpu_reg can_trig_clk_div:16;
	Tpu_reg can_trig_sa_pos:16;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_3{
	struct{
	Tpu_reg can_trig_id_min:29;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_4{
	struct{
	Tpu_reg can_trig_id_max:29;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_5{
	struct{
	Tpu_reg can_trig_id_mask:29;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_6{
	struct{
	Tpu_reg can_trig_dat_min_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_7{
	struct{
	Tpu_reg can_trig_dat_min_bits63_32:32;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_8{
	struct{
	Tpu_reg can_trig_dat_max_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_9{
	struct{
	Tpu_reg can_trig_dat_max_bits63_32:32;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_10{
	struct{
	Tpu_reg can_trig_dat_mask_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_can_cfg_11{
	struct{
	Tpu_reg can_trig_dat_mask_bits63_32:32;
	};
	Tpu_reg payload;
};

union Tpu_flexray_cfg_1{
	struct{
	Tpu_reg flexray_trig_type:2;
	Tpu_reg flexray_trig_type_location:2;
	Tpu_reg flexray_trig_type_frame:2;
	Tpu_reg flexray_trig_type_symbol:2;

	Tpu_reg flexray_trig_type_err:2;
	Tpu_reg flexray_trig_type_id:3;
	Tpu_reg flexray_trig_type_id_mask:1;
	Tpu_reg flexray_trig_type_cyccount:3;

	Tpu_reg flexray_trig_type_cyc_mask:1;
	Tpu_reg flexray_trig_rate_sel:2;
	Tpu_reg flexray_trig_channel_sel:1;
	Tpu_reg flexray_trig_gdTSSTransmitter:4;

	Tpu_reg flexray_trig_gdCASxLowMax:7;
	};
	Tpu_reg payload;
};

union Tpu_flexray_cfg_2{
	struct{
	Tpu_reg flexray_trig_frame_id_min:11;
	Tpu_reg flexray_trig_frame_id_max:11;
	};
	Tpu_reg payload;
};

union Tpu_flexray_cfg_3{
	struct{
	Tpu_reg flexray_trig_frame_id_mask:11;
	};
	Tpu_reg payload;
};

union Tpu_flexray_cfg_4{
	struct{
	Tpu_reg flexray_trig_frame_cycount_min:6;
	Tpu_reg flexray_trig_frame_cycount_max:6;
	Tpu_reg flexray_trig_frame_cycount_mask:6;
	};
	Tpu_reg payload;
};

union Tpu_spi_cfg_1{
	struct{
	Tpu_reg spi_trig_type:1;
	Tpu_reg spi_trig_dat_type:3;
	Tpu_reg spi_trig_dat_bit_num:5;
	};
	Tpu_reg payload;
};

union Tpu_spi_cfg_2{
	struct{
	Tpu_reg spi_trig_timeout:32;
	};
	Tpu_reg payload;
};

union Tpu_spi_cfg_3{
	struct{
	Tpu_reg spi_trig_dat_min:32;
	};
	Tpu_reg payload;
};

union Tpu_spi_cfg_4{
	struct{
	Tpu_reg spi_trig_dat_max:32;
	};
	Tpu_reg payload;
};

union Tpu_spi_cfg_5{
	struct{
	Tpu_reg spi_trig_dat_mask:32;
	};
	Tpu_reg payload;
};

union Tpu_iic_cfg_1{
	struct{
	Tpu_reg iic_trig_type:3;
	Tpu_reg iic_trig_addr_type:3;
	Tpu_reg iic_trig_dat_type:3;
	Tpu_reg iic_trig_wr_type:2;

	Tpu_reg iic_addr_length:1;
	Tpu_reg iic_dat_length:3;
	};
	Tpu_reg payload;
};

union Tpu_iic_cfg_2{
	struct{
	Tpu_reg iic_addr_setmin:10;
	Tpu_reg iic_addr_setmax:10;
	Tpu_reg iic_addr_setmask:10;
	};
	Tpu_reg payload;
};

union Tpu_iic_cfg_3{
	struct{
	Tpu_reg iic_dat_setmin_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_iic_cfg_4{
	struct{
	Tpu_reg iic_dat_setmax_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_iic_cfg_5{
	struct{
	Tpu_reg iic_dat_setmask_bits31_0:32;
	};
	Tpu_reg payload;
};

union Tpu_iic_cfg_6{
	struct{
	Tpu_reg iic_dat_setmin_bits39_32:8;
	Tpu_reg iic_dat_setmax_bits39_32:8;
	Tpu_reg iic_dat_setmask_bits39_32:8;
	};
	Tpu_reg payload;
};

union Tpu_video_cfg_1{
	struct{
	Tpu_reg video_trig_type:2;
	Tpu_reg video_trig_line:11;
	};
	Tpu_reg payload;
};

union Tpu_video_cfg_2{
	struct{
	Tpu_reg fpulse_width:16;
	Tpu_reg hsync_width:16;
	};
	Tpu_reg payload;
};

union Tpu_video_cfg_3{
	struct{
	Tpu_reg start_line:11;
	Tpu_reg total_line:11;
	Tpu_reg level_type:1;
	};
	Tpu_reg payload;
};

union Tpu_std1553b_cfg_1{
	struct{
	Tpu_reg std1553b_trig_type:3;
	Tpu_reg std1553b_trig_sync_type:2;
	Tpu_reg std1553b_trig_dat_type:3;
	Tpu_reg std1553b_trig_remote_addr_type:3;

	Tpu_reg std1553b_trig_subaddr_mode_type:3;
	Tpu_reg std1553b_trig_count_code_type:3;
	Tpu_reg std1553b_trig_err_type:2;
	};
	Tpu_reg payload;
};

union Tpu_std1553b_cfg_2{
	struct{
	Tpu_reg std1553b_trig_word_mask:17;
	};
	Tpu_reg payload;
};

union Tpu_std1553b_cfg_3{
	struct{
	Tpu_reg std1553b_trig_dat_cmp_l:17;
	};
	Tpu_reg payload;
};

union Tpu_std1553b_cfg_4{
	struct{
	Tpu_reg std1553b_trig_dat_cmp_g:17;
	};
	Tpu_reg payload;
};

union Tpu_search_dat_compress_num{
	struct{
	Tpu_reg search_dat_compress_num:20;
	};
	Tpu_reg payload;
};

union Tpu_search_result_send_en{
	struct{
	Tpu_reg search_result_send_en:1;
	};
	Tpu_reg payload;
};

union Tpu_search_result_send_rst{
	struct{
	Tpu_reg search_result_send_rst:1;
	};
	Tpu_reg payload;
};

union Tpu_search_event_total_num{
	struct{
	Tpu_reg search_event_total_num:21;
	};
	Tpu_reg payload;
};

struct TpuMemProxy {
	Tpu_reg version;
	Tpu_ch1_cmp_level ch1_cmp_level; 
	Tpu_ch2_cmp_level ch2_cmp_level; 
	Tpu_ch3_cmp_level ch3_cmp_level; 

	Tpu_ch4_cmp_level ch4_cmp_level; 
	Tpu_A_event_reg1 A_event_reg1; 
	Tpu_A_event_reg2 A_event_reg2; 
	Tpu_A_event_reg3 A_event_reg3; 

	Tpu_A_event_reg4 A_event_reg4; 
	Tpu_A_event_reg5 A_event_reg5; 
	Tpu_A_event_reg6 A_event_reg6; 
	Tpu_B_event_reg1 B_event_reg1; 

	Tpu_B_event_reg2 B_event_reg2; 
	Tpu_B_event_reg3 B_event_reg3; 
	Tpu_B_event_reg4 B_event_reg4; 
	Tpu_B_event_reg5 B_event_reg5; 

	Tpu_B_event_reg6 B_event_reg6; 
	Tpu_R_event_reg1 R_event_reg1; 
	Tpu_R_event_reg2 R_event_reg2; 
	Tpu_R_event_reg3 R_event_reg3; 

	Tpu_R_event_reg4 R_event_reg4; 
	Tpu_R_event_reg5 R_event_reg5; 
	Tpu_R_event_reg6 R_event_reg6; 
	Tpu_tpu_trig_mux tpu_trig_mux; 

	Tpu_abr_delay_time_l abr_delay_time_l; 
	Tpu_abr_delay_time_h abr_delay_time_h; 
	Tpu_abr_timeout_l abr_timeout_l; 
	Tpu_abr_timeout_h abr_timeout_h; 

	Tpu_reg abr_b_event_num;
	Tpu_reg preprocess_type;
	Tpu_reg abr_a_event_num;
	Tpu_reg holdoff_event_num;

	Tpu_holdoff_time_l holdoff_time_l; 
	Tpu_holdoff_time_h holdoff_time_h; 
	Tpu_iir_set iir_set; 
	Tpu_uart_cfg_1 uart_cfg_1; 

	Tpu_uart_cfg_2 uart_cfg_2; 
	Tpu_iis_cfg_1 iis_cfg_1; 
	Tpu_iis_cfg_2 iis_cfg_2; 
	Tpu_iis_cfg_3 iis_cfg_3; 

	Tpu_iis_cfg_4 iis_cfg_4; 
	Tpu_lin_cfg_1 lin_cfg_1; 
	Tpu_lin_cfg_2 lin_cfg_2; 
	Tpu_lin_cfg_3 lin_cfg_3; 

	Tpu_lin_cfg_4 lin_cfg_4; 
	Tpu_lin_cfg_5 lin_cfg_5; 
	Tpu_lin_cfg_6 lin_cfg_6; 
	Tpu_lin_cfg_7 lin_cfg_7; 

	Tpu_lin_cfg_8 lin_cfg_8; 
	Tpu_lin_cfg_9 lin_cfg_9; 
	Tpu_can_cfg_1 can_cfg_1; 
	Tpu_can_cfg_2 can_cfg_2; 

	Tpu_can_cfg_3 can_cfg_3; 
	Tpu_can_cfg_4 can_cfg_4; 
	Tpu_can_cfg_5 can_cfg_5; 
	Tpu_can_cfg_6 can_cfg_6; 

	Tpu_can_cfg_7 can_cfg_7; 
	Tpu_can_cfg_8 can_cfg_8; 
	Tpu_can_cfg_9 can_cfg_9; 
	Tpu_can_cfg_10 can_cfg_10; 

	Tpu_can_cfg_11 can_cfg_11; 
	Tpu_flexray_cfg_1 flexray_cfg_1; 
	Tpu_flexray_cfg_2 flexray_cfg_2; 
	Tpu_flexray_cfg_3 flexray_cfg_3; 

	Tpu_flexray_cfg_4 flexray_cfg_4; 
	Tpu_spi_cfg_1 spi_cfg_1; 
	Tpu_spi_cfg_2 spi_cfg_2; 
	Tpu_spi_cfg_3 spi_cfg_3; 

	Tpu_spi_cfg_4 spi_cfg_4; 
	Tpu_spi_cfg_5 spi_cfg_5; 
	Tpu_iic_cfg_1 iic_cfg_1; 
	Tpu_iic_cfg_2 iic_cfg_2; 

	Tpu_iic_cfg_3 iic_cfg_3; 
	Tpu_iic_cfg_4 iic_cfg_4; 
	Tpu_iic_cfg_5 iic_cfg_5; 
	Tpu_iic_cfg_6 iic_cfg_6; 

	Tpu_video_cfg_1 video_cfg_1; 
	Tpu_video_cfg_2 video_cfg_2; 
	Tpu_video_cfg_3 video_cfg_3; 
	Tpu_std1553b_cfg_1 std1553b_cfg_1; 

	Tpu_std1553b_cfg_2 std1553b_cfg_2; 
	Tpu_std1553b_cfg_3 std1553b_cfg_3; 
	Tpu_std1553b_cfg_4 std1553b_cfg_4; 
	Tpu_reg search_version;

	Tpu_search_dat_compress_num search_dat_compress_num; 
	Tpu_search_result_send_en search_result_send_en; 
	Tpu_search_result_send_rst search_result_send_rst; 
	Tpu_search_event_total_num search_event_total_num; 

	Tpu_reg getversion(  );

	void assignch1_cmp_level_lvl_1_h_a( Tpu_reg value  );
	void assignch1_cmp_level_lvl_1_l_a( Tpu_reg value  );
	void assignch1_cmp_level_lvl_1_h_b( Tpu_reg value  );
	void assignch1_cmp_level_lvl_1_l_b( Tpu_reg value  );

	void assignch1_cmp_level( Tpu_reg value );


	Tpu_reg getch1_cmp_level_lvl_1_h_a(  );
	Tpu_reg getch1_cmp_level_lvl_1_l_a(  );
	Tpu_reg getch1_cmp_level_lvl_1_h_b(  );
	Tpu_reg getch1_cmp_level_lvl_1_l_b(  );

	Tpu_reg getch1_cmp_level(  );

	void assignch2_cmp_level_lvl_2_h_a( Tpu_reg value  );
	void assignch2_cmp_level_lvl_2_l_a( Tpu_reg value  );
	void assignch2_cmp_level_lvl_2_h_b( Tpu_reg value  );
	void assignch2_cmp_level_lvl_2_l_b( Tpu_reg value  );

	void assignch2_cmp_level( Tpu_reg value );


	Tpu_reg getch2_cmp_level_lvl_2_h_a(  );
	Tpu_reg getch2_cmp_level_lvl_2_l_a(  );
	Tpu_reg getch2_cmp_level_lvl_2_h_b(  );
	Tpu_reg getch2_cmp_level_lvl_2_l_b(  );

	Tpu_reg getch2_cmp_level(  );

	void assignch3_cmp_level_lvl_3_h_a( Tpu_reg value  );
	void assignch3_cmp_level_lvl_3_l_a( Tpu_reg value  );
	void assignch3_cmp_level_lvl_3_h_b( Tpu_reg value  );
	void assignch3_cmp_level_lvl_3_l_b( Tpu_reg value  );

	void assignch3_cmp_level( Tpu_reg value );


	Tpu_reg getch3_cmp_level_lvl_3_h_a(  );
	Tpu_reg getch3_cmp_level_lvl_3_l_a(  );
	Tpu_reg getch3_cmp_level_lvl_3_h_b(  );
	Tpu_reg getch3_cmp_level_lvl_3_l_b(  );

	Tpu_reg getch3_cmp_level(  );

	void assignch4_cmp_level_lvl_4_h_a( Tpu_reg value  );
	void assignch4_cmp_level_lvl_4_l_a( Tpu_reg value  );
	void assignch4_cmp_level_lvl_4_h_b( Tpu_reg value  );
	void assignch4_cmp_level_lvl_4_l_b( Tpu_reg value  );

	void assignch4_cmp_level( Tpu_reg value );


	Tpu_reg getch4_cmp_level_lvl_4_h_a(  );
	Tpu_reg getch4_cmp_level_lvl_4_l_a(  );
	Tpu_reg getch4_cmp_level_lvl_4_h_b(  );
	Tpu_reg getch4_cmp_level_lvl_4_l_b(  );

	Tpu_reg getch4_cmp_level(  );

	void assignA_event_reg1_A_event_logic_la_sel( Tpu_reg value  );

	void assignA_event_reg1( Tpu_reg value );


	Tpu_reg getA_event_reg1_A_event_logic_la_sel(  );

	Tpu_reg getA_event_reg1(  );

	void assignA_event_reg2_A_event_logic_analog_sel( Tpu_reg value  );
	void assignA_event_reg2_A_event_logic_func( Tpu_reg value  );

	void assignA_event_reg2( Tpu_reg value );


	Tpu_reg getA_event_reg2_A_event_logic_analog_sel(  );
	Tpu_reg getA_event_reg2_A_event_logic_func(  );

	Tpu_reg getA_event_reg2(  );

	void assignA_event_reg3_A_event_cmpa_sel( Tpu_reg value  );
	void assignA_event_reg3_A_event_cmpa_mode( Tpu_reg value  );
	void assignA_event_reg3_A_event_cmpb_sel( Tpu_reg value  );
	void assignA_event_reg3_A_event_cmpb_mode( Tpu_reg value  );
	void assignA_event_reg3_A_event_clk_sel( Tpu_reg value  );
	void assignA_event_reg3_A_event_clk_sel_inv( Tpu_reg value  );
	void assignA_event_reg3_A_event_dat_sel( Tpu_reg value  );
	void assignA_event_reg3_A_event_dat_sel_inv( Tpu_reg value  );

	void assignA_event_reg3( Tpu_reg value );


	Tpu_reg getA_event_reg3_A_event_cmpa_sel(  );
	Tpu_reg getA_event_reg3_A_event_cmpa_mode(  );
	Tpu_reg getA_event_reg3_A_event_cmpb_sel(  );
	Tpu_reg getA_event_reg3_A_event_cmpb_mode(  );
	Tpu_reg getA_event_reg3_A_event_clk_sel(  );
	Tpu_reg getA_event_reg3_A_event_clk_sel_inv(  );
	Tpu_reg getA_event_reg3_A_event_dat_sel(  );
	Tpu_reg getA_event_reg3_A_event_dat_sel_inv(  );

	Tpu_reg getA_event_reg3(  );

	void assignA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg value  );

	void assignA_event_reg4( Tpu_reg value );


	Tpu_reg getA_event_reg4_A_event_width_cmp_1_31_0(  );

	Tpu_reg getA_event_reg4(  );

	void assignA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg value  );

	void assignA_event_reg5( Tpu_reg value );


	Tpu_reg getA_event_reg5_A_event_width_cmp_g_31_0(  );

	Tpu_reg getA_event_reg5(  );

	void assignA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg value  );
	void assignA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg value  );
	void assignA_event_reg6_A_event_width_cmp_mode( Tpu_reg value  );

	void assignA_event_reg6( Tpu_reg value );


	Tpu_reg getA_event_reg6_A_event_width_cmp_1_39_32(  );
	Tpu_reg getA_event_reg6_A_event_width_cmp_g_39_32(  );
	Tpu_reg getA_event_reg6_A_event_width_cmp_mode(  );

	Tpu_reg getA_event_reg6(  );

	void assignB_event_reg1_B_event_logic_la_sel( Tpu_reg value  );

	void assignB_event_reg1( Tpu_reg value );


	Tpu_reg getB_event_reg1_B_event_logic_la_sel(  );

	Tpu_reg getB_event_reg1(  );

	void assignB_event_reg2_B_event_logic_analog_sel( Tpu_reg value  );
	void assignB_event_reg2_B_event_logic_func( Tpu_reg value  );

	void assignB_event_reg2( Tpu_reg value );


	Tpu_reg getB_event_reg2_B_event_logic_analog_sel(  );
	Tpu_reg getB_event_reg2_B_event_logic_func(  );

	Tpu_reg getB_event_reg2(  );

	void assignB_event_reg3_B_event_cmpa_sel( Tpu_reg value  );
	void assignB_event_reg3_B_event_cmpa_mode( Tpu_reg value  );
	void assignB_event_reg3_B_event_cmpb_sel( Tpu_reg value  );
	void assignB_event_reg3_B_event_cmpb_mode( Tpu_reg value  );
	void assignB_event_reg3_B_event_clk_sel( Tpu_reg value  );
	void assignB_event_reg3_B_event_clk_sel_inv( Tpu_reg value  );
	void assignB_event_reg3_B_event_dat_sel( Tpu_reg value  );
	void assignB_event_reg3_B_event_dat_sel_inv( Tpu_reg value  );

	void assignB_event_reg3( Tpu_reg value );


	Tpu_reg getB_event_reg3_B_event_cmpa_sel(  );
	Tpu_reg getB_event_reg3_B_event_cmpa_mode(  );
	Tpu_reg getB_event_reg3_B_event_cmpb_sel(  );
	Tpu_reg getB_event_reg3_B_event_cmpb_mode(  );
	Tpu_reg getB_event_reg3_B_event_clk_sel(  );
	Tpu_reg getB_event_reg3_B_event_clk_sel_inv(  );
	Tpu_reg getB_event_reg3_B_event_dat_sel(  );
	Tpu_reg getB_event_reg3_B_event_dat_sel_inv(  );

	Tpu_reg getB_event_reg3(  );

	void assignB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg value  );

	void assignB_event_reg4( Tpu_reg value );


	Tpu_reg getB_event_reg4_B_event_width_cmp_1_31_0(  );

	Tpu_reg getB_event_reg4(  );

	void assignB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg value  );

	void assignB_event_reg5( Tpu_reg value );


	Tpu_reg getB_event_reg5_B_event_width_cmp_g_31_0(  );

	Tpu_reg getB_event_reg5(  );

	void assignB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg value  );
	void assignB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg value  );
	void assignB_event_reg6_B_event_width_cmp_mode( Tpu_reg value  );

	void assignB_event_reg6( Tpu_reg value );


	Tpu_reg getB_event_reg6_B_event_width_cmp_1_39_32(  );
	Tpu_reg getB_event_reg6_B_event_width_cmp_g_39_32(  );
	Tpu_reg getB_event_reg6_B_event_width_cmp_mode(  );

	Tpu_reg getB_event_reg6(  );

	void assignR_event_reg1_R_event_logic_la_sel( Tpu_reg value  );

	void assignR_event_reg1( Tpu_reg value );


	Tpu_reg getR_event_reg1_R_event_logic_la_sel(  );

	Tpu_reg getR_event_reg1(  );

	void assignR_event_reg2_R_event_logic_analog_sel( Tpu_reg value  );
	void assignR_event_reg2_R_event_logic_func( Tpu_reg value  );

	void assignR_event_reg2( Tpu_reg value );


	Tpu_reg getR_event_reg2_R_event_logic_analog_sel(  );
	Tpu_reg getR_event_reg2_R_event_logic_func(  );

	Tpu_reg getR_event_reg2(  );

	void assignR_event_reg3_R_event_cmpa_sel( Tpu_reg value  );
	void assignR_event_reg3_R_event_cmpa_mode( Tpu_reg value  );
	void assignR_event_reg3_R_event_cmpb_sel( Tpu_reg value  );
	void assignR_event_reg3_R_event_cmpb_mode( Tpu_reg value  );
	void assignR_event_reg3_R_event_clk_sel( Tpu_reg value  );
	void assignR_event_reg3_R_event_clk_sel_inv( Tpu_reg value  );
	void assignR_event_reg3_R_event_dat_sel( Tpu_reg value  );
	void assignR_event_reg3_R_event_dat_sel_inv( Tpu_reg value  );

	void assignR_event_reg3( Tpu_reg value );


	Tpu_reg getR_event_reg3_R_event_cmpa_sel(  );
	Tpu_reg getR_event_reg3_R_event_cmpa_mode(  );
	Tpu_reg getR_event_reg3_R_event_cmpb_sel(  );
	Tpu_reg getR_event_reg3_R_event_cmpb_mode(  );
	Tpu_reg getR_event_reg3_R_event_clk_sel(  );
	Tpu_reg getR_event_reg3_R_event_clk_sel_inv(  );
	Tpu_reg getR_event_reg3_R_event_dat_sel(  );
	Tpu_reg getR_event_reg3_R_event_dat_sel_inv(  );

	Tpu_reg getR_event_reg3(  );

	void assignR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg value  );

	void assignR_event_reg4( Tpu_reg value );


	Tpu_reg getR_event_reg4_R_event_width_cmp_1_31_0(  );

	Tpu_reg getR_event_reg4(  );

	void assignR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg value  );

	void assignR_event_reg5( Tpu_reg value );


	Tpu_reg getR_event_reg5_R_event_width_cmp_g_31_0(  );

	Tpu_reg getR_event_reg5(  );

	void assignR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg value  );
	void assignR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg value  );
	void assignR_event_reg6_R_event_width_cmp_mode( Tpu_reg value  );

	void assignR_event_reg6( Tpu_reg value );


	Tpu_reg getR_event_reg6_R_event_width_cmp_1_39_32(  );
	Tpu_reg getR_event_reg6_R_event_width_cmp_g_39_32(  );
	Tpu_reg getR_event_reg6_R_event_width_cmp_mode(  );

	Tpu_reg getR_event_reg6(  );

	void assigntpu_trig_mux_tpu_trig_type( Tpu_reg value  );
	void assigntpu_trig_mux_trig_rst( Tpu_reg value  );
	void assigntpu_trig_mux_force_trig( Tpu_reg value  );
	void assigntpu_trig_mux_abr_seq_loop_en( Tpu_reg value  );
	void assigntpu_trig_mux_abr_seq_delay_on( Tpu_reg value  );
	void assigntpu_trig_mux_abr_seq_timeout_on( Tpu_reg value  );
	void assigntpu_trig_mux_abr_seq_r_event_on( Tpu_reg value  );
	void assigntpu_trig_mux_seq_abr_trig_a_only( Tpu_reg value  );

	void assigntpu_trig_mux( Tpu_reg value );


	Tpu_reg gettpu_trig_mux_tpu_trig_type(  );
	Tpu_reg gettpu_trig_mux_trig_rst(  );
	Tpu_reg gettpu_trig_mux_force_trig(  );
	Tpu_reg gettpu_trig_mux_abr_seq_loop_en(  );
	Tpu_reg gettpu_trig_mux_abr_seq_delay_on(  );
	Tpu_reg gettpu_trig_mux_abr_seq_timeout_on(  );
	Tpu_reg gettpu_trig_mux_abr_seq_r_event_on(  );
	Tpu_reg gettpu_trig_mux_seq_abr_trig_a_only(  );

	Tpu_reg gettpu_trig_mux(  );

	void assignabr_delay_time_l_abr_delay_time_l( Tpu_reg value  );

	void assignabr_delay_time_l( Tpu_reg value );


	void assignabr_delay_time_h_abr_delay_time_h( Tpu_reg value  );

	void assignabr_delay_time_h( Tpu_reg value );


	void assignabr_timeout_l_abr_timeout_l( Tpu_reg value  );

	void assignabr_timeout_l( Tpu_reg value );


	void assignabr_timeout_h_abr_timeout_h( Tpu_reg value  );

	void assignabr_timeout_h( Tpu_reg value );


	void assignabr_b_event_num( Tpu_reg value );

	void assignpreprocess_type( Tpu_reg value );

	void assignabr_a_event_num( Tpu_reg value );

	void assignholdoff_event_num( Tpu_reg value );

	Tpu_reg getholdoff_event_num(  );

	void assignholdoff_time_l_holdoff_time_l( Tpu_reg value  );

	void assignholdoff_time_l( Tpu_reg value );


	Tpu_reg getholdoff_time_l_holdoff_time_l(  );

	Tpu_reg getholdoff_time_l(  );

	void assignholdoff_time_h_holdoff_time_h( Tpu_reg value  );
	void assignholdoff_time_h_holdoff_type( Tpu_reg value  );

	void assignholdoff_time_h( Tpu_reg value );


	Tpu_reg getholdoff_time_h_holdoff_time_h(  );
	Tpu_reg getholdoff_time_h_holdoff_type(  );

	Tpu_reg getholdoff_time_h(  );

	void assigniir_set_iir_type( Tpu_reg value  );
	void assigniir_set_iir_src( Tpu_reg value  );
	void assigniir_set_iir_offset( Tpu_reg value  );

	void assigniir_set( Tpu_reg value );


	Tpu_reg getiir_set_iir_type(  );
	Tpu_reg getiir_set_iir_src(  );
	Tpu_reg getiir_set_iir_offset(  );

	Tpu_reg getiir_set(  );

	void assignuart_cfg_1_uart_clk_div( Tpu_reg value  );
	void assignuart_cfg_1_uart_trig_type( Tpu_reg value  );
	void assignuart_cfg_1_uart_trig_dat( Tpu_reg value  );
	void assignuart_cfg_1_uart_trig_err( Tpu_reg value  );
	void assignuart_cfg_1_uart_eof( Tpu_reg value  );
	void assignuart_cfg_1_uart_parity( Tpu_reg value  );

	void assignuart_cfg_1( Tpu_reg value );


	void assignuart_cfg_2_uart_len( Tpu_reg value  );
	void assignuart_cfg_2_uart_dat_min( Tpu_reg value  );
	void assignuart_cfg_2_uart_dat_max( Tpu_reg value  );

	void assignuart_cfg_2( Tpu_reg value );


	void assigniis_cfg_1_iis_trig_type( Tpu_reg value  );
	void assigniis_cfg_1_iis_trig_channel_sel( Tpu_reg value  );
	void assigniis_cfg_1_iis_trig_bus_type( Tpu_reg value  );
	void assigniis_cfg_1_iis_trig_bitnum_vld( Tpu_reg value  );
	void assigniis_cfg_1_iis_trig_bitnum_total( Tpu_reg value  );

	void assigniis_cfg_1( Tpu_reg value );


	void assigniis_cfg_2_iis_trig_word_min( Tpu_reg value  );

	void assigniis_cfg_2( Tpu_reg value );


	void assigniis_cfg_3_iis_trig_word_max( Tpu_reg value  );

	void assigniis_cfg_3( Tpu_reg value );


	void assigniis_cfg_4_iis_trig_word_mask( Tpu_reg value  );

	void assigniis_cfg_4( Tpu_reg value );


	void assignlin_cfg_1_lin_trig_type( Tpu_reg value  );
	void assignlin_cfg_1_lin_trig_type_id( Tpu_reg value  );
	void assignlin_cfg_1_lin_trig_type_dat( Tpu_reg value  );
	void assignlin_cfg_1_lin_trig_type_err( Tpu_reg value  );
	void assignlin_cfg_1_lin_trig_spec( Tpu_reg value  );
	void assignlin_cfg_1_lin_trig_id_min( Tpu_reg value  );
	void assignlin_cfg_1_lin_trig_id_max( Tpu_reg value  );
	void assignlin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg value  );

	void assignlin_cfg_1( Tpu_reg value );


	void assignlin_cfg_2_lin_trig_clk_div( Tpu_reg value  );

	void assignlin_cfg_2( Tpu_reg value );


	void assignlin_cfg_3_lin_trig_sa_pos( Tpu_reg value  );

	void assignlin_cfg_3( Tpu_reg value );


	void assignlin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg value  );

	void assignlin_cfg_4( Tpu_reg value );


	void assignlin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg value  );

	void assignlin_cfg_5( Tpu_reg value );


	void assignlin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg value  );

	void assignlin_cfg_6( Tpu_reg value );


	void assignlin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg value  );

	void assignlin_cfg_7( Tpu_reg value );


	void assignlin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg value  );

	void assignlin_cfg_8( Tpu_reg value );


	void assignlin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg value  );

	void assignlin_cfg_9( Tpu_reg value );


	void assigncan_cfg_1_can_trig_type( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_type_field( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_type_frame( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_type_err( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_type_id( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_type_id_mask( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_type_dat( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_type_dat_mask( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_bus_type( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_std_extend_sel( Tpu_reg value  );
	void assigncan_cfg_1_can_trig_dat_cmpnum( Tpu_reg value  );

	void assigncan_cfg_1( Tpu_reg value );


	void assigncan_cfg_2_can_trig_clk_div( Tpu_reg value  );
	void assigncan_cfg_2_can_trig_sa_pos( Tpu_reg value  );

	void assigncan_cfg_2( Tpu_reg value );


	void assigncan_cfg_3_can_trig_id_min( Tpu_reg value  );

	void assigncan_cfg_3( Tpu_reg value );


	void assigncan_cfg_4_can_trig_id_max( Tpu_reg value  );

	void assigncan_cfg_4( Tpu_reg value );


	void assigncan_cfg_5_can_trig_id_mask( Tpu_reg value  );

	void assigncan_cfg_5( Tpu_reg value );


	void assigncan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg value  );

	void assigncan_cfg_6( Tpu_reg value );


	void assigncan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg value  );

	void assigncan_cfg_7( Tpu_reg value );


	void assigncan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg value  );

	void assigncan_cfg_8( Tpu_reg value );


	void assigncan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg value  );

	void assigncan_cfg_9( Tpu_reg value );


	void assigncan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg value  );

	void assigncan_cfg_10( Tpu_reg value );


	void assigncan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg value  );

	void assigncan_cfg_11( Tpu_reg value );


	void assignflexray_cfg_1_flexray_trig_type( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_location( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_frame( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_err( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_id( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg value  );
	void assignflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg value  );

	void assignflexray_cfg_1( Tpu_reg value );


	void assignflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg value  );
	void assignflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg value  );

	void assignflexray_cfg_2( Tpu_reg value );


	void assignflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg value  );

	void assignflexray_cfg_3( Tpu_reg value );


	void assignflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg value  );
	void assignflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg value  );
	void assignflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg value  );

	void assignflexray_cfg_4( Tpu_reg value );


	void assignspi_cfg_1_spi_trig_type( Tpu_reg value  );
	void assignspi_cfg_1_spi_trig_dat_type( Tpu_reg value  );
	void assignspi_cfg_1_spi_trig_dat_bit_num( Tpu_reg value  );

	void assignspi_cfg_1( Tpu_reg value );


	void assignspi_cfg_2_spi_trig_timeout( Tpu_reg value  );

	void assignspi_cfg_2( Tpu_reg value );


	void assignspi_cfg_3_spi_trig_dat_min( Tpu_reg value  );

	void assignspi_cfg_3( Tpu_reg value );


	void assignspi_cfg_4_spi_trig_dat_max( Tpu_reg value  );

	void assignspi_cfg_4( Tpu_reg value );


	void assignspi_cfg_5_spi_trig_dat_mask( Tpu_reg value  );

	void assignspi_cfg_5( Tpu_reg value );


	void assigniic_cfg_1_iic_trig_type( Tpu_reg value  );
	void assigniic_cfg_1_iic_trig_addr_type( Tpu_reg value  );
	void assigniic_cfg_1_iic_trig_dat_type( Tpu_reg value  );
	void assigniic_cfg_1_iic_trig_wr_type( Tpu_reg value  );
	void assigniic_cfg_1_iic_addr_length( Tpu_reg value  );
	void assigniic_cfg_1_iic_dat_length( Tpu_reg value  );

	void assigniic_cfg_1( Tpu_reg value );


	void assigniic_cfg_2_iic_addr_setmin( Tpu_reg value  );
	void assigniic_cfg_2_iic_addr_setmax( Tpu_reg value  );
	void assigniic_cfg_2_iic_addr_setmask( Tpu_reg value  );

	void assigniic_cfg_2( Tpu_reg value );


	void assigniic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg value  );

	void assigniic_cfg_3( Tpu_reg value );


	void assigniic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg value  );

	void assigniic_cfg_4( Tpu_reg value );


	void assigniic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg value  );

	void assigniic_cfg_5( Tpu_reg value );


	void assigniic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg value  );
	void assigniic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg value  );
	void assigniic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg value  );

	void assigniic_cfg_6( Tpu_reg value );


	void assignvideo_cfg_1_video_trig_type( Tpu_reg value  );
	void assignvideo_cfg_1_video_trig_line( Tpu_reg value  );

	void assignvideo_cfg_1( Tpu_reg value );


	void assignvideo_cfg_2_fpulse_width( Tpu_reg value  );
	void assignvideo_cfg_2_hsync_width( Tpu_reg value  );

	void assignvideo_cfg_2( Tpu_reg value );


	void assignvideo_cfg_3_start_line( Tpu_reg value  );
	void assignvideo_cfg_3_total_line( Tpu_reg value  );
	void assignvideo_cfg_3_level_type( Tpu_reg value  );

	void assignvideo_cfg_3( Tpu_reg value );


	void assignstd1553b_cfg_1_std1553b_trig_type( Tpu_reg value  );
	void assignstd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg value  );
	void assignstd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg value  );
	void assignstd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg value  );
	void assignstd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg value  );
	void assignstd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg value  );
	void assignstd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg value  );

	void assignstd1553b_cfg_1( Tpu_reg value );


	void assignstd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg value  );

	void assignstd1553b_cfg_2( Tpu_reg value );


	void assignstd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg value  );

	void assignstd1553b_cfg_3( Tpu_reg value );


	void assignstd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg value  );

	void assignstd1553b_cfg_4( Tpu_reg value );


	Tpu_reg getsearch_version(  );

	void assignsearch_dat_compress_num_search_dat_compress_num( Tpu_reg value  );

	void assignsearch_dat_compress_num( Tpu_reg value );


	void assignsearch_result_send_en_search_result_send_en( Tpu_reg value  );

	void assignsearch_result_send_en( Tpu_reg value );


	void assignsearch_result_send_rst_search_result_send_rst( Tpu_reg value  );

	void assignsearch_result_send_rst( Tpu_reg value );


	Tpu_reg getsearch_event_total_num_search_event_total_num(  );

	Tpu_reg getsearch_event_total_num(  );

};
struct TpuMem : public TpuMemProxy, public IPhyFpga{
protected:
	Tpu_reg mAddrBase;
public:
	TpuMem( Tpu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( Tpu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( TpuMemProxy *proxy);
	void pull( TpuMemProxy *proxy);

public:
	Tpu_reg inversion(  );

	void setch1_cmp_level_lvl_1_h_a( Tpu_reg value );
	void setch1_cmp_level_lvl_1_l_a( Tpu_reg value );
	void setch1_cmp_level_lvl_1_h_b( Tpu_reg value );
	void setch1_cmp_level_lvl_1_l_b( Tpu_reg value );

	void setch1_cmp_level( Tpu_reg value );
	void outch1_cmp_level_lvl_1_h_a( Tpu_reg value );
	void pulsech1_cmp_level_lvl_1_h_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch1_cmp_level_lvl_1_l_a( Tpu_reg value );
	void pulsech1_cmp_level_lvl_1_l_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch1_cmp_level_lvl_1_h_b( Tpu_reg value );
	void pulsech1_cmp_level_lvl_1_h_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch1_cmp_level_lvl_1_l_b( Tpu_reg value );
	void pulsech1_cmp_level_lvl_1_l_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outch1_cmp_level( Tpu_reg value );
	void outch1_cmp_level(  );


	Tpu_reg inch1_cmp_level(  );

	void setch2_cmp_level_lvl_2_h_a( Tpu_reg value );
	void setch2_cmp_level_lvl_2_l_a( Tpu_reg value );
	void setch2_cmp_level_lvl_2_h_b( Tpu_reg value );
	void setch2_cmp_level_lvl_2_l_b( Tpu_reg value );

	void setch2_cmp_level( Tpu_reg value );
	void outch2_cmp_level_lvl_2_h_a( Tpu_reg value );
	void pulsech2_cmp_level_lvl_2_h_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch2_cmp_level_lvl_2_l_a( Tpu_reg value );
	void pulsech2_cmp_level_lvl_2_l_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch2_cmp_level_lvl_2_h_b( Tpu_reg value );
	void pulsech2_cmp_level_lvl_2_h_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch2_cmp_level_lvl_2_l_b( Tpu_reg value );
	void pulsech2_cmp_level_lvl_2_l_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outch2_cmp_level( Tpu_reg value );
	void outch2_cmp_level(  );


	Tpu_reg inch2_cmp_level(  );

	void setch3_cmp_level_lvl_3_h_a( Tpu_reg value );
	void setch3_cmp_level_lvl_3_l_a( Tpu_reg value );
	void setch3_cmp_level_lvl_3_h_b( Tpu_reg value );
	void setch3_cmp_level_lvl_3_l_b( Tpu_reg value );

	void setch3_cmp_level( Tpu_reg value );
	void outch3_cmp_level_lvl_3_h_a( Tpu_reg value );
	void pulsech3_cmp_level_lvl_3_h_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch3_cmp_level_lvl_3_l_a( Tpu_reg value );
	void pulsech3_cmp_level_lvl_3_l_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch3_cmp_level_lvl_3_h_b( Tpu_reg value );
	void pulsech3_cmp_level_lvl_3_h_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch3_cmp_level_lvl_3_l_b( Tpu_reg value );
	void pulsech3_cmp_level_lvl_3_l_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outch3_cmp_level( Tpu_reg value );
	void outch3_cmp_level(  );


	Tpu_reg inch3_cmp_level(  );

	void setch4_cmp_level_lvl_4_h_a( Tpu_reg value );
	void setch4_cmp_level_lvl_4_l_a( Tpu_reg value );
	void setch4_cmp_level_lvl_4_h_b( Tpu_reg value );
	void setch4_cmp_level_lvl_4_l_b( Tpu_reg value );

	void setch4_cmp_level( Tpu_reg value );
	void outch4_cmp_level_lvl_4_h_a( Tpu_reg value );
	void pulsech4_cmp_level_lvl_4_h_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch4_cmp_level_lvl_4_l_a( Tpu_reg value );
	void pulsech4_cmp_level_lvl_4_l_a( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch4_cmp_level_lvl_4_h_b( Tpu_reg value );
	void pulsech4_cmp_level_lvl_4_h_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outch4_cmp_level_lvl_4_l_b( Tpu_reg value );
	void pulsech4_cmp_level_lvl_4_l_b( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outch4_cmp_level( Tpu_reg value );
	void outch4_cmp_level(  );


	Tpu_reg inch4_cmp_level(  );

	void setA_event_reg1_A_event_logic_la_sel( Tpu_reg value );

	void setA_event_reg1( Tpu_reg value );
	void outA_event_reg1_A_event_logic_la_sel( Tpu_reg value );
	void pulseA_event_reg1_A_event_logic_la_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outA_event_reg1( Tpu_reg value );
	void outA_event_reg1(  );


	Tpu_reg inA_event_reg1(  );

	void setA_event_reg2_A_event_logic_analog_sel( Tpu_reg value );
	void setA_event_reg2_A_event_logic_func( Tpu_reg value );

	void setA_event_reg2( Tpu_reg value );
	void outA_event_reg2_A_event_logic_analog_sel( Tpu_reg value );
	void pulseA_event_reg2_A_event_logic_analog_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg2_A_event_logic_func( Tpu_reg value );
	void pulseA_event_reg2_A_event_logic_func( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outA_event_reg2( Tpu_reg value );
	void outA_event_reg2(  );


	Tpu_reg inA_event_reg2(  );

	void setA_event_reg3_A_event_cmpa_sel( Tpu_reg value );
	void setA_event_reg3_A_event_cmpa_mode( Tpu_reg value );
	void setA_event_reg3_A_event_cmpb_sel( Tpu_reg value );
	void setA_event_reg3_A_event_cmpb_mode( Tpu_reg value );
	void setA_event_reg3_A_event_clk_sel( Tpu_reg value );
	void setA_event_reg3_A_event_clk_sel_inv( Tpu_reg value );
	void setA_event_reg3_A_event_dat_sel( Tpu_reg value );
	void setA_event_reg3_A_event_dat_sel_inv( Tpu_reg value );

	void setA_event_reg3( Tpu_reg value );
	void outA_event_reg3_A_event_cmpa_sel( Tpu_reg value );
	void pulseA_event_reg3_A_event_cmpa_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg3_A_event_cmpa_mode( Tpu_reg value );
	void pulseA_event_reg3_A_event_cmpa_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg3_A_event_cmpb_sel( Tpu_reg value );
	void pulseA_event_reg3_A_event_cmpb_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg3_A_event_cmpb_mode( Tpu_reg value );
	void pulseA_event_reg3_A_event_cmpb_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg3_A_event_clk_sel( Tpu_reg value );
	void pulseA_event_reg3_A_event_clk_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg3_A_event_clk_sel_inv( Tpu_reg value );
	void pulseA_event_reg3_A_event_clk_sel_inv( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg3_A_event_dat_sel( Tpu_reg value );
	void pulseA_event_reg3_A_event_dat_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg3_A_event_dat_sel_inv( Tpu_reg value );
	void pulseA_event_reg3_A_event_dat_sel_inv( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outA_event_reg3( Tpu_reg value );
	void outA_event_reg3(  );


	Tpu_reg inA_event_reg3(  );

	void setA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg value );

	void setA_event_reg4( Tpu_reg value );
	void outA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg value );
	void pulseA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outA_event_reg4( Tpu_reg value );
	void outA_event_reg4(  );


	Tpu_reg inA_event_reg4(  );

	void setA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg value );

	void setA_event_reg5( Tpu_reg value );
	void outA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg value );
	void pulseA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outA_event_reg5( Tpu_reg value );
	void outA_event_reg5(  );


	Tpu_reg inA_event_reg5(  );

	void setA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg value );
	void setA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg value );
	void setA_event_reg6_A_event_width_cmp_mode( Tpu_reg value );

	void setA_event_reg6( Tpu_reg value );
	void outA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg value );
	void pulseA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg value );
	void pulseA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outA_event_reg6_A_event_width_cmp_mode( Tpu_reg value );
	void pulseA_event_reg6_A_event_width_cmp_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outA_event_reg6( Tpu_reg value );
	void outA_event_reg6(  );


	Tpu_reg inA_event_reg6(  );

	void setB_event_reg1_B_event_logic_la_sel( Tpu_reg value );

	void setB_event_reg1( Tpu_reg value );
	void outB_event_reg1_B_event_logic_la_sel( Tpu_reg value );
	void pulseB_event_reg1_B_event_logic_la_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outB_event_reg1( Tpu_reg value );
	void outB_event_reg1(  );


	Tpu_reg inB_event_reg1(  );

	void setB_event_reg2_B_event_logic_analog_sel( Tpu_reg value );
	void setB_event_reg2_B_event_logic_func( Tpu_reg value );

	void setB_event_reg2( Tpu_reg value );
	void outB_event_reg2_B_event_logic_analog_sel( Tpu_reg value );
	void pulseB_event_reg2_B_event_logic_analog_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg2_B_event_logic_func( Tpu_reg value );
	void pulseB_event_reg2_B_event_logic_func( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outB_event_reg2( Tpu_reg value );
	void outB_event_reg2(  );


	Tpu_reg inB_event_reg2(  );

	void setB_event_reg3_B_event_cmpa_sel( Tpu_reg value );
	void setB_event_reg3_B_event_cmpa_mode( Tpu_reg value );
	void setB_event_reg3_B_event_cmpb_sel( Tpu_reg value );
	void setB_event_reg3_B_event_cmpb_mode( Tpu_reg value );
	void setB_event_reg3_B_event_clk_sel( Tpu_reg value );
	void setB_event_reg3_B_event_clk_sel_inv( Tpu_reg value );
	void setB_event_reg3_B_event_dat_sel( Tpu_reg value );
	void setB_event_reg3_B_event_dat_sel_inv( Tpu_reg value );

	void setB_event_reg3( Tpu_reg value );
	void outB_event_reg3_B_event_cmpa_sel( Tpu_reg value );
	void pulseB_event_reg3_B_event_cmpa_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg3_B_event_cmpa_mode( Tpu_reg value );
	void pulseB_event_reg3_B_event_cmpa_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg3_B_event_cmpb_sel( Tpu_reg value );
	void pulseB_event_reg3_B_event_cmpb_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg3_B_event_cmpb_mode( Tpu_reg value );
	void pulseB_event_reg3_B_event_cmpb_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg3_B_event_clk_sel( Tpu_reg value );
	void pulseB_event_reg3_B_event_clk_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg3_B_event_clk_sel_inv( Tpu_reg value );
	void pulseB_event_reg3_B_event_clk_sel_inv( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg3_B_event_dat_sel( Tpu_reg value );
	void pulseB_event_reg3_B_event_dat_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg3_B_event_dat_sel_inv( Tpu_reg value );
	void pulseB_event_reg3_B_event_dat_sel_inv( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outB_event_reg3( Tpu_reg value );
	void outB_event_reg3(  );


	Tpu_reg inB_event_reg3(  );

	void setB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg value );

	void setB_event_reg4( Tpu_reg value );
	void outB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg value );
	void pulseB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outB_event_reg4( Tpu_reg value );
	void outB_event_reg4(  );


	Tpu_reg inB_event_reg4(  );

	void setB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg value );

	void setB_event_reg5( Tpu_reg value );
	void outB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg value );
	void pulseB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outB_event_reg5( Tpu_reg value );
	void outB_event_reg5(  );


	Tpu_reg inB_event_reg5(  );

	void setB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg value );
	void setB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg value );
	void setB_event_reg6_B_event_width_cmp_mode( Tpu_reg value );

	void setB_event_reg6( Tpu_reg value );
	void outB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg value );
	void pulseB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg value );
	void pulseB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outB_event_reg6_B_event_width_cmp_mode( Tpu_reg value );
	void pulseB_event_reg6_B_event_width_cmp_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outB_event_reg6( Tpu_reg value );
	void outB_event_reg6(  );


	Tpu_reg inB_event_reg6(  );

	void setR_event_reg1_R_event_logic_la_sel( Tpu_reg value );

	void setR_event_reg1( Tpu_reg value );
	void outR_event_reg1_R_event_logic_la_sel( Tpu_reg value );
	void pulseR_event_reg1_R_event_logic_la_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outR_event_reg1( Tpu_reg value );
	void outR_event_reg1(  );


	Tpu_reg inR_event_reg1(  );

	void setR_event_reg2_R_event_logic_analog_sel( Tpu_reg value );
	void setR_event_reg2_R_event_logic_func( Tpu_reg value );

	void setR_event_reg2( Tpu_reg value );
	void outR_event_reg2_R_event_logic_analog_sel( Tpu_reg value );
	void pulseR_event_reg2_R_event_logic_analog_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg2_R_event_logic_func( Tpu_reg value );
	void pulseR_event_reg2_R_event_logic_func( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outR_event_reg2( Tpu_reg value );
	void outR_event_reg2(  );


	Tpu_reg inR_event_reg2(  );

	void setR_event_reg3_R_event_cmpa_sel( Tpu_reg value );
	void setR_event_reg3_R_event_cmpa_mode( Tpu_reg value );
	void setR_event_reg3_R_event_cmpb_sel( Tpu_reg value );
	void setR_event_reg3_R_event_cmpb_mode( Tpu_reg value );
	void setR_event_reg3_R_event_clk_sel( Tpu_reg value );
	void setR_event_reg3_R_event_clk_sel_inv( Tpu_reg value );
	void setR_event_reg3_R_event_dat_sel( Tpu_reg value );
	void setR_event_reg3_R_event_dat_sel_inv( Tpu_reg value );

	void setR_event_reg3( Tpu_reg value );
	void outR_event_reg3_R_event_cmpa_sel( Tpu_reg value );
	void pulseR_event_reg3_R_event_cmpa_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg3_R_event_cmpa_mode( Tpu_reg value );
	void pulseR_event_reg3_R_event_cmpa_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg3_R_event_cmpb_sel( Tpu_reg value );
	void pulseR_event_reg3_R_event_cmpb_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg3_R_event_cmpb_mode( Tpu_reg value );
	void pulseR_event_reg3_R_event_cmpb_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg3_R_event_clk_sel( Tpu_reg value );
	void pulseR_event_reg3_R_event_clk_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg3_R_event_clk_sel_inv( Tpu_reg value );
	void pulseR_event_reg3_R_event_clk_sel_inv( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg3_R_event_dat_sel( Tpu_reg value );
	void pulseR_event_reg3_R_event_dat_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg3_R_event_dat_sel_inv( Tpu_reg value );
	void pulseR_event_reg3_R_event_dat_sel_inv( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outR_event_reg3( Tpu_reg value );
	void outR_event_reg3(  );


	Tpu_reg inR_event_reg3(  );

	void setR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg value );

	void setR_event_reg4( Tpu_reg value );
	void outR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg value );
	void pulseR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outR_event_reg4( Tpu_reg value );
	void outR_event_reg4(  );


	Tpu_reg inR_event_reg4(  );

	void setR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg value );

	void setR_event_reg5( Tpu_reg value );
	void outR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg value );
	void pulseR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outR_event_reg5( Tpu_reg value );
	void outR_event_reg5(  );


	Tpu_reg inR_event_reg5(  );

	void setR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg value );
	void setR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg value );
	void setR_event_reg6_R_event_width_cmp_mode( Tpu_reg value );

	void setR_event_reg6( Tpu_reg value );
	void outR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg value );
	void pulseR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg value );
	void pulseR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outR_event_reg6_R_event_width_cmp_mode( Tpu_reg value );
	void pulseR_event_reg6_R_event_width_cmp_mode( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outR_event_reg6( Tpu_reg value );
	void outR_event_reg6(  );


	Tpu_reg inR_event_reg6(  );

	void settpu_trig_mux_tpu_trig_type( Tpu_reg value );
	void settpu_trig_mux_trig_rst( Tpu_reg value );
	void settpu_trig_mux_force_trig( Tpu_reg value );
	void settpu_trig_mux_abr_seq_loop_en( Tpu_reg value );
	void settpu_trig_mux_abr_seq_delay_on( Tpu_reg value );
	void settpu_trig_mux_abr_seq_timeout_on( Tpu_reg value );
	void settpu_trig_mux_abr_seq_r_event_on( Tpu_reg value );
	void settpu_trig_mux_seq_abr_trig_a_only( Tpu_reg value );

	void settpu_trig_mux( Tpu_reg value );
	void outtpu_trig_mux_tpu_trig_type( Tpu_reg value );
	void pulsetpu_trig_mux_tpu_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outtpu_trig_mux_trig_rst( Tpu_reg value );
	void pulsetpu_trig_mux_trig_rst( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outtpu_trig_mux_force_trig( Tpu_reg value );
	void pulsetpu_trig_mux_force_trig( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outtpu_trig_mux_abr_seq_loop_en( Tpu_reg value );
	void pulsetpu_trig_mux_abr_seq_loop_en( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outtpu_trig_mux_abr_seq_delay_on( Tpu_reg value );
	void pulsetpu_trig_mux_abr_seq_delay_on( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outtpu_trig_mux_abr_seq_timeout_on( Tpu_reg value );
	void pulsetpu_trig_mux_abr_seq_timeout_on( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outtpu_trig_mux_abr_seq_r_event_on( Tpu_reg value );
	void pulsetpu_trig_mux_abr_seq_r_event_on( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outtpu_trig_mux_seq_abr_trig_a_only( Tpu_reg value );
	void pulsetpu_trig_mux_seq_abr_trig_a_only( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outtpu_trig_mux( Tpu_reg value );
	void outtpu_trig_mux(  );


	Tpu_reg intpu_trig_mux(  );

	void setabr_delay_time_l_abr_delay_time_l( Tpu_reg value );

	void setabr_delay_time_l( Tpu_reg value );
	void outabr_delay_time_l_abr_delay_time_l( Tpu_reg value );
	void pulseabr_delay_time_l_abr_delay_time_l( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outabr_delay_time_l( Tpu_reg value );
	void outabr_delay_time_l(  );


	void setabr_delay_time_h_abr_delay_time_h( Tpu_reg value );

	void setabr_delay_time_h( Tpu_reg value );
	void outabr_delay_time_h_abr_delay_time_h( Tpu_reg value );
	void pulseabr_delay_time_h_abr_delay_time_h( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outabr_delay_time_h( Tpu_reg value );
	void outabr_delay_time_h(  );


	void setabr_timeout_l_abr_timeout_l( Tpu_reg value );

	void setabr_timeout_l( Tpu_reg value );
	void outabr_timeout_l_abr_timeout_l( Tpu_reg value );
	void pulseabr_timeout_l_abr_timeout_l( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outabr_timeout_l( Tpu_reg value );
	void outabr_timeout_l(  );


	void setabr_timeout_h_abr_timeout_h( Tpu_reg value );

	void setabr_timeout_h( Tpu_reg value );
	void outabr_timeout_h_abr_timeout_h( Tpu_reg value );
	void pulseabr_timeout_h_abr_timeout_h( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outabr_timeout_h( Tpu_reg value );
	void outabr_timeout_h(  );


	void setabr_b_event_num( Tpu_reg value );
	void outabr_b_event_num( Tpu_reg value );
	void outabr_b_event_num(  );

	void setpreprocess_type( Tpu_reg value );
	void outpreprocess_type( Tpu_reg value );
	void outpreprocess_type(  );

	void setabr_a_event_num( Tpu_reg value );
	void outabr_a_event_num( Tpu_reg value );
	void outabr_a_event_num(  );

	void setholdoff_event_num( Tpu_reg value );
	void outholdoff_event_num( Tpu_reg value );
	void outholdoff_event_num(  );

	Tpu_reg inholdoff_event_num(  );

	void setholdoff_time_l_holdoff_time_l( Tpu_reg value );

	void setholdoff_time_l( Tpu_reg value );
	void outholdoff_time_l_holdoff_time_l( Tpu_reg value );
	void pulseholdoff_time_l_holdoff_time_l( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outholdoff_time_l( Tpu_reg value );
	void outholdoff_time_l(  );


	Tpu_reg inholdoff_time_l(  );

	void setholdoff_time_h_holdoff_time_h( Tpu_reg value );
	void setholdoff_time_h_holdoff_type( Tpu_reg value );

	void setholdoff_time_h( Tpu_reg value );
	void outholdoff_time_h_holdoff_time_h( Tpu_reg value );
	void pulseholdoff_time_h_holdoff_time_h( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outholdoff_time_h_holdoff_type( Tpu_reg value );
	void pulseholdoff_time_h_holdoff_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outholdoff_time_h( Tpu_reg value );
	void outholdoff_time_h(  );


	Tpu_reg inholdoff_time_h(  );

	void setiir_set_iir_type( Tpu_reg value );
	void setiir_set_iir_src( Tpu_reg value );
	void setiir_set_iir_offset( Tpu_reg value );

	void setiir_set( Tpu_reg value );
	void outiir_set_iir_type( Tpu_reg value );
	void pulseiir_set_iir_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiir_set_iir_src( Tpu_reg value );
	void pulseiir_set_iir_src( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiir_set_iir_offset( Tpu_reg value );
	void pulseiir_set_iir_offset( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiir_set( Tpu_reg value );
	void outiir_set(  );


	Tpu_reg iniir_set(  );

	void setuart_cfg_1_uart_clk_div( Tpu_reg value );
	void setuart_cfg_1_uart_trig_type( Tpu_reg value );
	void setuart_cfg_1_uart_trig_dat( Tpu_reg value );
	void setuart_cfg_1_uart_trig_err( Tpu_reg value );
	void setuart_cfg_1_uart_eof( Tpu_reg value );
	void setuart_cfg_1_uart_parity( Tpu_reg value );

	void setuart_cfg_1( Tpu_reg value );
	void outuart_cfg_1_uart_clk_div( Tpu_reg value );
	void pulseuart_cfg_1_uart_clk_div( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outuart_cfg_1_uart_trig_type( Tpu_reg value );
	void pulseuart_cfg_1_uart_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outuart_cfg_1_uart_trig_dat( Tpu_reg value );
	void pulseuart_cfg_1_uart_trig_dat( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outuart_cfg_1_uart_trig_err( Tpu_reg value );
	void pulseuart_cfg_1_uart_trig_err( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outuart_cfg_1_uart_eof( Tpu_reg value );
	void pulseuart_cfg_1_uart_eof( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outuart_cfg_1_uart_parity( Tpu_reg value );
	void pulseuart_cfg_1_uart_parity( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outuart_cfg_1( Tpu_reg value );
	void outuart_cfg_1(  );


	void setuart_cfg_2_uart_len( Tpu_reg value );
	void setuart_cfg_2_uart_dat_min( Tpu_reg value );
	void setuart_cfg_2_uart_dat_max( Tpu_reg value );

	void setuart_cfg_2( Tpu_reg value );
	void outuart_cfg_2_uart_len( Tpu_reg value );
	void pulseuart_cfg_2_uart_len( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outuart_cfg_2_uart_dat_min( Tpu_reg value );
	void pulseuart_cfg_2_uart_dat_min( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outuart_cfg_2_uart_dat_max( Tpu_reg value );
	void pulseuart_cfg_2_uart_dat_max( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outuart_cfg_2( Tpu_reg value );
	void outuart_cfg_2(  );


	void setiis_cfg_1_iis_trig_type( Tpu_reg value );
	void setiis_cfg_1_iis_trig_channel_sel( Tpu_reg value );
	void setiis_cfg_1_iis_trig_bus_type( Tpu_reg value );
	void setiis_cfg_1_iis_trig_bitnum_vld( Tpu_reg value );
	void setiis_cfg_1_iis_trig_bitnum_total( Tpu_reg value );

	void setiis_cfg_1( Tpu_reg value );
	void outiis_cfg_1_iis_trig_type( Tpu_reg value );
	void pulseiis_cfg_1_iis_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiis_cfg_1_iis_trig_channel_sel( Tpu_reg value );
	void pulseiis_cfg_1_iis_trig_channel_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiis_cfg_1_iis_trig_bus_type( Tpu_reg value );
	void pulseiis_cfg_1_iis_trig_bus_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiis_cfg_1_iis_trig_bitnum_vld( Tpu_reg value );
	void pulseiis_cfg_1_iis_trig_bitnum_vld( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiis_cfg_1_iis_trig_bitnum_total( Tpu_reg value );
	void pulseiis_cfg_1_iis_trig_bitnum_total( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiis_cfg_1( Tpu_reg value );
	void outiis_cfg_1(  );


	void setiis_cfg_2_iis_trig_word_min( Tpu_reg value );

	void setiis_cfg_2( Tpu_reg value );
	void outiis_cfg_2_iis_trig_word_min( Tpu_reg value );
	void pulseiis_cfg_2_iis_trig_word_min( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiis_cfg_2( Tpu_reg value );
	void outiis_cfg_2(  );


	void setiis_cfg_3_iis_trig_word_max( Tpu_reg value );

	void setiis_cfg_3( Tpu_reg value );
	void outiis_cfg_3_iis_trig_word_max( Tpu_reg value );
	void pulseiis_cfg_3_iis_trig_word_max( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiis_cfg_3( Tpu_reg value );
	void outiis_cfg_3(  );


	void setiis_cfg_4_iis_trig_word_mask( Tpu_reg value );

	void setiis_cfg_4( Tpu_reg value );
	void outiis_cfg_4_iis_trig_word_mask( Tpu_reg value );
	void pulseiis_cfg_4_iis_trig_word_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiis_cfg_4( Tpu_reg value );
	void outiis_cfg_4(  );


	void setlin_cfg_1_lin_trig_type( Tpu_reg value );
	void setlin_cfg_1_lin_trig_type_id( Tpu_reg value );
	void setlin_cfg_1_lin_trig_type_dat( Tpu_reg value );
	void setlin_cfg_1_lin_trig_type_err( Tpu_reg value );
	void setlin_cfg_1_lin_trig_spec( Tpu_reg value );
	void setlin_cfg_1_lin_trig_id_min( Tpu_reg value );
	void setlin_cfg_1_lin_trig_id_max( Tpu_reg value );
	void setlin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg value );

	void setlin_cfg_1( Tpu_reg value );
	void outlin_cfg_1_lin_trig_type( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outlin_cfg_1_lin_trig_type_id( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_type_id( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outlin_cfg_1_lin_trig_type_dat( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_type_dat( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outlin_cfg_1_lin_trig_type_err( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_type_err( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outlin_cfg_1_lin_trig_spec( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_spec( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outlin_cfg_1_lin_trig_id_min( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_id_min( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outlin_cfg_1_lin_trig_id_max( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_id_max( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outlin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg value );
	void pulselin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_1( Tpu_reg value );
	void outlin_cfg_1(  );


	void setlin_cfg_2_lin_trig_clk_div( Tpu_reg value );

	void setlin_cfg_2( Tpu_reg value );
	void outlin_cfg_2_lin_trig_clk_div( Tpu_reg value );
	void pulselin_cfg_2_lin_trig_clk_div( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_2( Tpu_reg value );
	void outlin_cfg_2(  );


	void setlin_cfg_3_lin_trig_sa_pos( Tpu_reg value );

	void setlin_cfg_3( Tpu_reg value );
	void outlin_cfg_3_lin_trig_sa_pos( Tpu_reg value );
	void pulselin_cfg_3_lin_trig_sa_pos( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_3( Tpu_reg value );
	void outlin_cfg_3(  );


	void setlin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg value );

	void setlin_cfg_4( Tpu_reg value );
	void outlin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg value );
	void pulselin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_4( Tpu_reg value );
	void outlin_cfg_4(  );


	void setlin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg value );

	void setlin_cfg_5( Tpu_reg value );
	void outlin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg value );
	void pulselin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_5( Tpu_reg value );
	void outlin_cfg_5(  );


	void setlin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg value );

	void setlin_cfg_6( Tpu_reg value );
	void outlin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg value );
	void pulselin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_6( Tpu_reg value );
	void outlin_cfg_6(  );


	void setlin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg value );

	void setlin_cfg_7( Tpu_reg value );
	void outlin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg value );
	void pulselin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_7( Tpu_reg value );
	void outlin_cfg_7(  );


	void setlin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg value );

	void setlin_cfg_8( Tpu_reg value );
	void outlin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg value );
	void pulselin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_8( Tpu_reg value );
	void outlin_cfg_8(  );


	void setlin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg value );

	void setlin_cfg_9( Tpu_reg value );
	void outlin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg value );
	void pulselin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outlin_cfg_9( Tpu_reg value );
	void outlin_cfg_9(  );


	void setcan_cfg_1_can_trig_type( Tpu_reg value );
	void setcan_cfg_1_can_trig_type_field( Tpu_reg value );
	void setcan_cfg_1_can_trig_type_frame( Tpu_reg value );
	void setcan_cfg_1_can_trig_type_err( Tpu_reg value );
	void setcan_cfg_1_can_trig_type_id( Tpu_reg value );
	void setcan_cfg_1_can_trig_type_id_mask( Tpu_reg value );
	void setcan_cfg_1_can_trig_type_dat( Tpu_reg value );
	void setcan_cfg_1_can_trig_type_dat_mask( Tpu_reg value );
	void setcan_cfg_1_can_trig_bus_type( Tpu_reg value );
	void setcan_cfg_1_can_trig_std_extend_sel( Tpu_reg value );
	void setcan_cfg_1_can_trig_dat_cmpnum( Tpu_reg value );

	void setcan_cfg_1( Tpu_reg value );
	void outcan_cfg_1_can_trig_type( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_type_field( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type_field( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_type_frame( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type_frame( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_type_err( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type_err( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_type_id( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type_id( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_type_id_mask( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type_id_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_type_dat( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type_dat( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_type_dat_mask( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_type_dat_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_bus_type( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_bus_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_std_extend_sel( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_std_extend_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_1_can_trig_dat_cmpnum( Tpu_reg value );
	void pulsecan_cfg_1_can_trig_dat_cmpnum( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_1( Tpu_reg value );
	void outcan_cfg_1(  );


	void setcan_cfg_2_can_trig_clk_div( Tpu_reg value );
	void setcan_cfg_2_can_trig_sa_pos( Tpu_reg value );

	void setcan_cfg_2( Tpu_reg value );
	void outcan_cfg_2_can_trig_clk_div( Tpu_reg value );
	void pulsecan_cfg_2_can_trig_clk_div( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outcan_cfg_2_can_trig_sa_pos( Tpu_reg value );
	void pulsecan_cfg_2_can_trig_sa_pos( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_2( Tpu_reg value );
	void outcan_cfg_2(  );


	void setcan_cfg_3_can_trig_id_min( Tpu_reg value );

	void setcan_cfg_3( Tpu_reg value );
	void outcan_cfg_3_can_trig_id_min( Tpu_reg value );
	void pulsecan_cfg_3_can_trig_id_min( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_3( Tpu_reg value );
	void outcan_cfg_3(  );


	void setcan_cfg_4_can_trig_id_max( Tpu_reg value );

	void setcan_cfg_4( Tpu_reg value );
	void outcan_cfg_4_can_trig_id_max( Tpu_reg value );
	void pulsecan_cfg_4_can_trig_id_max( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_4( Tpu_reg value );
	void outcan_cfg_4(  );


	void setcan_cfg_5_can_trig_id_mask( Tpu_reg value );

	void setcan_cfg_5( Tpu_reg value );
	void outcan_cfg_5_can_trig_id_mask( Tpu_reg value );
	void pulsecan_cfg_5_can_trig_id_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_5( Tpu_reg value );
	void outcan_cfg_5(  );


	void setcan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg value );

	void setcan_cfg_6( Tpu_reg value );
	void outcan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg value );
	void pulsecan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_6( Tpu_reg value );
	void outcan_cfg_6(  );


	void setcan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg value );

	void setcan_cfg_7( Tpu_reg value );
	void outcan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg value );
	void pulsecan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_7( Tpu_reg value );
	void outcan_cfg_7(  );


	void setcan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg value );

	void setcan_cfg_8( Tpu_reg value );
	void outcan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg value );
	void pulsecan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_8( Tpu_reg value );
	void outcan_cfg_8(  );


	void setcan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg value );

	void setcan_cfg_9( Tpu_reg value );
	void outcan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg value );
	void pulsecan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_9( Tpu_reg value );
	void outcan_cfg_9(  );


	void setcan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg value );

	void setcan_cfg_10( Tpu_reg value );
	void outcan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg value );
	void pulsecan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_10( Tpu_reg value );
	void outcan_cfg_10(  );


	void setcan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg value );

	void setcan_cfg_11( Tpu_reg value );
	void outcan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg value );
	void pulsecan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outcan_cfg_11( Tpu_reg value );
	void outcan_cfg_11(  );


	void setflexray_cfg_1_flexray_trig_type( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_location( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_frame( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_err( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_id( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg value );
	void setflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg value );

	void setflexray_cfg_1( Tpu_reg value );
	void outflexray_cfg_1_flexray_trig_type( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_location( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_location( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_frame( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_frame( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_err( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_err( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_id( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_id( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg value );
	void pulseflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outflexray_cfg_1( Tpu_reg value );
	void outflexray_cfg_1(  );


	void setflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg value );
	void setflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg value );

	void setflexray_cfg_2( Tpu_reg value );
	void outflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg value );
	void pulseflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg value );
	void pulseflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outflexray_cfg_2( Tpu_reg value );
	void outflexray_cfg_2(  );


	void setflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg value );

	void setflexray_cfg_3( Tpu_reg value );
	void outflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg value );
	void pulseflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outflexray_cfg_3( Tpu_reg value );
	void outflexray_cfg_3(  );


	void setflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg value );
	void setflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg value );
	void setflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg value );

	void setflexray_cfg_4( Tpu_reg value );
	void outflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg value );
	void pulseflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg value );
	void pulseflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg value );
	void pulseflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outflexray_cfg_4( Tpu_reg value );
	void outflexray_cfg_4(  );


	void setspi_cfg_1_spi_trig_type( Tpu_reg value );
	void setspi_cfg_1_spi_trig_dat_type( Tpu_reg value );
	void setspi_cfg_1_spi_trig_dat_bit_num( Tpu_reg value );

	void setspi_cfg_1( Tpu_reg value );
	void outspi_cfg_1_spi_trig_type( Tpu_reg value );
	void pulsespi_cfg_1_spi_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outspi_cfg_1_spi_trig_dat_type( Tpu_reg value );
	void pulsespi_cfg_1_spi_trig_dat_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outspi_cfg_1_spi_trig_dat_bit_num( Tpu_reg value );
	void pulsespi_cfg_1_spi_trig_dat_bit_num( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outspi_cfg_1( Tpu_reg value );
	void outspi_cfg_1(  );


	void setspi_cfg_2_spi_trig_timeout( Tpu_reg value );

	void setspi_cfg_2( Tpu_reg value );
	void outspi_cfg_2_spi_trig_timeout( Tpu_reg value );
	void pulsespi_cfg_2_spi_trig_timeout( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outspi_cfg_2( Tpu_reg value );
	void outspi_cfg_2(  );


	void setspi_cfg_3_spi_trig_dat_min( Tpu_reg value );

	void setspi_cfg_3( Tpu_reg value );
	void outspi_cfg_3_spi_trig_dat_min( Tpu_reg value );
	void pulsespi_cfg_3_spi_trig_dat_min( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outspi_cfg_3( Tpu_reg value );
	void outspi_cfg_3(  );


	void setspi_cfg_4_spi_trig_dat_max( Tpu_reg value );

	void setspi_cfg_4( Tpu_reg value );
	void outspi_cfg_4_spi_trig_dat_max( Tpu_reg value );
	void pulsespi_cfg_4_spi_trig_dat_max( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outspi_cfg_4( Tpu_reg value );
	void outspi_cfg_4(  );


	void setspi_cfg_5_spi_trig_dat_mask( Tpu_reg value );

	void setspi_cfg_5( Tpu_reg value );
	void outspi_cfg_5_spi_trig_dat_mask( Tpu_reg value );
	void pulsespi_cfg_5_spi_trig_dat_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outspi_cfg_5( Tpu_reg value );
	void outspi_cfg_5(  );


	void setiic_cfg_1_iic_trig_type( Tpu_reg value );
	void setiic_cfg_1_iic_trig_addr_type( Tpu_reg value );
	void setiic_cfg_1_iic_trig_dat_type( Tpu_reg value );
	void setiic_cfg_1_iic_trig_wr_type( Tpu_reg value );
	void setiic_cfg_1_iic_addr_length( Tpu_reg value );
	void setiic_cfg_1_iic_dat_length( Tpu_reg value );

	void setiic_cfg_1( Tpu_reg value );
	void outiic_cfg_1_iic_trig_type( Tpu_reg value );
	void pulseiic_cfg_1_iic_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_1_iic_trig_addr_type( Tpu_reg value );
	void pulseiic_cfg_1_iic_trig_addr_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_1_iic_trig_dat_type( Tpu_reg value );
	void pulseiic_cfg_1_iic_trig_dat_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_1_iic_trig_wr_type( Tpu_reg value );
	void pulseiic_cfg_1_iic_trig_wr_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_1_iic_addr_length( Tpu_reg value );
	void pulseiic_cfg_1_iic_addr_length( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_1_iic_dat_length( Tpu_reg value );
	void pulseiic_cfg_1_iic_dat_length( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiic_cfg_1( Tpu_reg value );
	void outiic_cfg_1(  );


	void setiic_cfg_2_iic_addr_setmin( Tpu_reg value );
	void setiic_cfg_2_iic_addr_setmax( Tpu_reg value );
	void setiic_cfg_2_iic_addr_setmask( Tpu_reg value );

	void setiic_cfg_2( Tpu_reg value );
	void outiic_cfg_2_iic_addr_setmin( Tpu_reg value );
	void pulseiic_cfg_2_iic_addr_setmin( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_2_iic_addr_setmax( Tpu_reg value );
	void pulseiic_cfg_2_iic_addr_setmax( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_2_iic_addr_setmask( Tpu_reg value );
	void pulseiic_cfg_2_iic_addr_setmask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiic_cfg_2( Tpu_reg value );
	void outiic_cfg_2(  );


	void setiic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg value );

	void setiic_cfg_3( Tpu_reg value );
	void outiic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg value );
	void pulseiic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiic_cfg_3( Tpu_reg value );
	void outiic_cfg_3(  );


	void setiic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg value );

	void setiic_cfg_4( Tpu_reg value );
	void outiic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg value );
	void pulseiic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiic_cfg_4( Tpu_reg value );
	void outiic_cfg_4(  );


	void setiic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg value );

	void setiic_cfg_5( Tpu_reg value );
	void outiic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg value );
	void pulseiic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiic_cfg_5( Tpu_reg value );
	void outiic_cfg_5(  );


	void setiic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg value );
	void setiic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg value );
	void setiic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg value );

	void setiic_cfg_6( Tpu_reg value );
	void outiic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg value );
	void pulseiic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg value );
	void pulseiic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outiic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg value );
	void pulseiic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outiic_cfg_6( Tpu_reg value );
	void outiic_cfg_6(  );


	void setvideo_cfg_1_video_trig_type( Tpu_reg value );
	void setvideo_cfg_1_video_trig_line( Tpu_reg value );

	void setvideo_cfg_1( Tpu_reg value );
	void outvideo_cfg_1_video_trig_type( Tpu_reg value );
	void pulsevideo_cfg_1_video_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outvideo_cfg_1_video_trig_line( Tpu_reg value );
	void pulsevideo_cfg_1_video_trig_line( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outvideo_cfg_1( Tpu_reg value );
	void outvideo_cfg_1(  );


	void setvideo_cfg_2_fpulse_width( Tpu_reg value );
	void setvideo_cfg_2_hsync_width( Tpu_reg value );

	void setvideo_cfg_2( Tpu_reg value );
	void outvideo_cfg_2_fpulse_width( Tpu_reg value );
	void pulsevideo_cfg_2_fpulse_width( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outvideo_cfg_2_hsync_width( Tpu_reg value );
	void pulsevideo_cfg_2_hsync_width( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outvideo_cfg_2( Tpu_reg value );
	void outvideo_cfg_2(  );


	void setvideo_cfg_3_start_line( Tpu_reg value );
	void setvideo_cfg_3_total_line( Tpu_reg value );
	void setvideo_cfg_3_level_type( Tpu_reg value );

	void setvideo_cfg_3( Tpu_reg value );
	void outvideo_cfg_3_start_line( Tpu_reg value );
	void pulsevideo_cfg_3_start_line( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outvideo_cfg_3_total_line( Tpu_reg value );
	void pulsevideo_cfg_3_total_line( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outvideo_cfg_3_level_type( Tpu_reg value );
	void pulsevideo_cfg_3_level_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outvideo_cfg_3( Tpu_reg value );
	void outvideo_cfg_3(  );


	void setstd1553b_cfg_1_std1553b_trig_type( Tpu_reg value );
	void setstd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg value );
	void setstd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg value );
	void setstd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg value );
	void setstd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg value );
	void setstd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg value );
	void setstd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg value );

	void setstd1553b_cfg_1( Tpu_reg value );
	void outstd1553b_cfg_1_std1553b_trig_type( Tpu_reg value );
	void pulsestd1553b_cfg_1_std1553b_trig_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outstd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg value );
	void pulsestd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outstd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg value );
	void pulsestd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outstd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg value );
	void pulsestd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outstd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg value );
	void pulsestd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outstd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg value );
	void pulsestd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );
	void outstd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg value );
	void pulsestd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outstd1553b_cfg_1( Tpu_reg value );
	void outstd1553b_cfg_1(  );


	void setstd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg value );

	void setstd1553b_cfg_2( Tpu_reg value );
	void outstd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg value );
	void pulsestd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outstd1553b_cfg_2( Tpu_reg value );
	void outstd1553b_cfg_2(  );


	void setstd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg value );

	void setstd1553b_cfg_3( Tpu_reg value );
	void outstd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg value );
	void pulsestd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outstd1553b_cfg_3( Tpu_reg value );
	void outstd1553b_cfg_3(  );


	void setstd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg value );

	void setstd1553b_cfg_4( Tpu_reg value );
	void outstd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg value );
	void pulsestd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outstd1553b_cfg_4( Tpu_reg value );
	void outstd1553b_cfg_4(  );


	Tpu_reg insearch_version(  );

	void setsearch_dat_compress_num_search_dat_compress_num( Tpu_reg value );

	void setsearch_dat_compress_num( Tpu_reg value );
	void outsearch_dat_compress_num_search_dat_compress_num( Tpu_reg value );
	void pulsesearch_dat_compress_num_search_dat_compress_num( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outsearch_dat_compress_num( Tpu_reg value );
	void outsearch_dat_compress_num(  );


	void setsearch_result_send_en_search_result_send_en( Tpu_reg value );

	void setsearch_result_send_en( Tpu_reg value );
	void outsearch_result_send_en_search_result_send_en( Tpu_reg value );
	void pulsesearch_result_send_en_search_result_send_en( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outsearch_result_send_en( Tpu_reg value );
	void outsearch_result_send_en(  );


	void setsearch_result_send_rst_search_result_send_rst( Tpu_reg value );

	void setsearch_result_send_rst( Tpu_reg value );
	void outsearch_result_send_rst_search_result_send_rst( Tpu_reg value );
	void pulsesearch_result_send_rst_search_result_send_rst( Tpu_reg activeV, Tpu_reg idleV=0, int timeus=0 );

	void outsearch_result_send_rst( Tpu_reg value );
	void outsearch_result_send_rst(  );


	Tpu_reg insearch_event_total_num(  );

public:
	cache_addr addr_version();
	cache_addr addr_ch1_cmp_level();
	cache_addr addr_ch2_cmp_level();
	cache_addr addr_ch3_cmp_level();

	cache_addr addr_ch4_cmp_level();
	cache_addr addr_A_event_reg1();
	cache_addr addr_A_event_reg2();
	cache_addr addr_A_event_reg3();

	cache_addr addr_A_event_reg4();
	cache_addr addr_A_event_reg5();
	cache_addr addr_A_event_reg6();
	cache_addr addr_B_event_reg1();

	cache_addr addr_B_event_reg2();
	cache_addr addr_B_event_reg3();
	cache_addr addr_B_event_reg4();
	cache_addr addr_B_event_reg5();

	cache_addr addr_B_event_reg6();
	cache_addr addr_R_event_reg1();
	cache_addr addr_R_event_reg2();
	cache_addr addr_R_event_reg3();

	cache_addr addr_R_event_reg4();
	cache_addr addr_R_event_reg5();
	cache_addr addr_R_event_reg6();
	cache_addr addr_tpu_trig_mux();

	cache_addr addr_abr_delay_time_l();
	cache_addr addr_abr_delay_time_h();
	cache_addr addr_abr_timeout_l();
	cache_addr addr_abr_timeout_h();

	cache_addr addr_abr_b_event_num();
	cache_addr addr_preprocess_type();
	cache_addr addr_abr_a_event_num();
	cache_addr addr_holdoff_event_num();

	cache_addr addr_holdoff_time_l();
	cache_addr addr_holdoff_time_h();
	cache_addr addr_iir_set();
	cache_addr addr_uart_cfg_1();

	cache_addr addr_uart_cfg_2();
	cache_addr addr_iis_cfg_1();
	cache_addr addr_iis_cfg_2();
	cache_addr addr_iis_cfg_3();

	cache_addr addr_iis_cfg_4();
	cache_addr addr_lin_cfg_1();
	cache_addr addr_lin_cfg_2();
	cache_addr addr_lin_cfg_3();

	cache_addr addr_lin_cfg_4();
	cache_addr addr_lin_cfg_5();
	cache_addr addr_lin_cfg_6();
	cache_addr addr_lin_cfg_7();

	cache_addr addr_lin_cfg_8();
	cache_addr addr_lin_cfg_9();
	cache_addr addr_can_cfg_1();
	cache_addr addr_can_cfg_2();

	cache_addr addr_can_cfg_3();
	cache_addr addr_can_cfg_4();
	cache_addr addr_can_cfg_5();
	cache_addr addr_can_cfg_6();

	cache_addr addr_can_cfg_7();
	cache_addr addr_can_cfg_8();
	cache_addr addr_can_cfg_9();
	cache_addr addr_can_cfg_10();

	cache_addr addr_can_cfg_11();
	cache_addr addr_flexray_cfg_1();
	cache_addr addr_flexray_cfg_2();
	cache_addr addr_flexray_cfg_3();

	cache_addr addr_flexray_cfg_4();
	cache_addr addr_spi_cfg_1();
	cache_addr addr_spi_cfg_2();
	cache_addr addr_spi_cfg_3();

	cache_addr addr_spi_cfg_4();
	cache_addr addr_spi_cfg_5();
	cache_addr addr_iic_cfg_1();
	cache_addr addr_iic_cfg_2();

	cache_addr addr_iic_cfg_3();
	cache_addr addr_iic_cfg_4();
	cache_addr addr_iic_cfg_5();
	cache_addr addr_iic_cfg_6();

	cache_addr addr_video_cfg_1();
	cache_addr addr_video_cfg_2();
	cache_addr addr_video_cfg_3();
	cache_addr addr_std1553b_cfg_1();

	cache_addr addr_std1553b_cfg_2();
	cache_addr addr_std1553b_cfg_3();
	cache_addr addr_std1553b_cfg_4();
	cache_addr addr_search_version();

	cache_addr addr_search_dat_compress_num();
	cache_addr addr_search_result_send_en();
	cache_addr addr_search_result_send_rst();
	cache_addr addr_search_event_total_num();

};
#define _remap_Tpu_()\
	mapIn( &version, 0x1800 + mAddrBase );\
	mapIn( &ch1_cmp_level, 0x1804 + mAddrBase );\
	mapIn( &ch2_cmp_level, 0x1808 + mAddrBase );\
	mapIn( &ch3_cmp_level, 0x180c + mAddrBase );\
	\
	mapIn( &ch4_cmp_level, 0x1810 + mAddrBase );\
	mapIn( &A_event_reg1, 0x1814 + mAddrBase );\
	mapIn( &A_event_reg2, 0x1818 + mAddrBase );\
	mapIn( &A_event_reg3, 0x181c + mAddrBase );\
	\
	mapIn( &A_event_reg4, 0x1820 + mAddrBase );\
	mapIn( &A_event_reg5, 0x1824 + mAddrBase );\
	mapIn( &A_event_reg6, 0x1828 + mAddrBase );\
	mapIn( &B_event_reg1, 0x182c + mAddrBase );\
	\
	mapIn( &B_event_reg2, 0x1830 + mAddrBase );\
	mapIn( &B_event_reg3, 0x1834 + mAddrBase );\
	mapIn( &B_event_reg4, 0x1838 + mAddrBase );\
	mapIn( &B_event_reg5, 0x183c + mAddrBase );\
	\
	mapIn( &B_event_reg6, 0x1840 + mAddrBase );\
	mapIn( &R_event_reg1, 0x1844 + mAddrBase );\
	mapIn( &R_event_reg2, 0x1848 + mAddrBase );\
	mapIn( &R_event_reg3, 0x184c + mAddrBase );\
	\
	mapIn( &R_event_reg4, 0x1850 + mAddrBase );\
	mapIn( &R_event_reg5, 0x1854 + mAddrBase );\
	mapIn( &R_event_reg6, 0x1858 + mAddrBase );\
	mapIn( &tpu_trig_mux, 0x185c + mAddrBase );\
	\
	mapIn( &abr_delay_time_l, 0x1860 + mAddrBase );\
	mapIn( &abr_delay_time_h, 0x1864 + mAddrBase );\
	mapIn( &abr_timeout_l, 0x1868 + mAddrBase );\
	mapIn( &abr_timeout_h, 0x186c + mAddrBase );\
	\
	mapIn( &abr_b_event_num, 0x1870 + mAddrBase );\
	mapIn( &preprocess_type, 0x1874 + mAddrBase );\
	mapIn( &abr_a_event_num, 0x1878 + mAddrBase );\
	mapIn( &holdoff_event_num, 0x1880 + mAddrBase );\
	\
	mapIn( &holdoff_time_l, 0x1884 + mAddrBase );\
	mapIn( &holdoff_time_h, 0x1888 + mAddrBase );\
	mapIn( &iir_set, 0x188c + mAddrBase );\
	mapIn( &uart_cfg_1, 0x1900 + mAddrBase );\
	\
	mapIn( &uart_cfg_2, 0x1904 + mAddrBase );\
	mapIn( &iis_cfg_1, 0x1908 + mAddrBase );\
	mapIn( &iis_cfg_2, 0x190c + mAddrBase );\
	mapIn( &iis_cfg_3, 0x1910 + mAddrBase );\
	\
	mapIn( &iis_cfg_4, 0x1914 + mAddrBase );\
	mapIn( &lin_cfg_1, 0x1918 + mAddrBase );\
	mapIn( &lin_cfg_2, 0x191c + mAddrBase );\
	mapIn( &lin_cfg_3, 0x1920 + mAddrBase );\
	\
	mapIn( &lin_cfg_4, 0x1924 + mAddrBase );\
	mapIn( &lin_cfg_5, 0x1928 + mAddrBase );\
	mapIn( &lin_cfg_6, 0x192c + mAddrBase );\
	mapIn( &lin_cfg_7, 0x1930 + mAddrBase );\
	\
	mapIn( &lin_cfg_8, 0x1934 + mAddrBase );\
	mapIn( &lin_cfg_9, 0x1938 + mAddrBase );\
	mapIn( &can_cfg_1, 0x193c + mAddrBase );\
	mapIn( &can_cfg_2, 0x1940 + mAddrBase );\
	\
	mapIn( &can_cfg_3, 0x1944 + mAddrBase );\
	mapIn( &can_cfg_4, 0x1948 + mAddrBase );\
	mapIn( &can_cfg_5, 0x194c + mAddrBase );\
	mapIn( &can_cfg_6, 0x1950 + mAddrBase );\
	\
	mapIn( &can_cfg_7, 0x1954 + mAddrBase );\
	mapIn( &can_cfg_8, 0x1958 + mAddrBase );\
	mapIn( &can_cfg_9, 0x195c + mAddrBase );\
	mapIn( &can_cfg_10, 0x1960 + mAddrBase );\
	\
	mapIn( &can_cfg_11, 0x1964 + mAddrBase );\
	mapIn( &flexray_cfg_1, 0x1968 + mAddrBase );\
	mapIn( &flexray_cfg_2, 0x196c + mAddrBase );\
	mapIn( &flexray_cfg_3, 0x1970 + mAddrBase );\
	\
	mapIn( &flexray_cfg_4, 0x1974 + mAddrBase );\
	mapIn( &spi_cfg_1, 0x1978 + mAddrBase );\
	mapIn( &spi_cfg_2, 0x197c + mAddrBase );\
	mapIn( &spi_cfg_3, 0x1980 + mAddrBase );\
	\
	mapIn( &spi_cfg_4, 0x1984 + mAddrBase );\
	mapIn( &spi_cfg_5, 0x1988 + mAddrBase );\
	mapIn( &iic_cfg_1, 0x198c + mAddrBase );\
	mapIn( &iic_cfg_2, 0x1990 + mAddrBase );\
	\
	mapIn( &iic_cfg_3, 0x1994 + mAddrBase );\
	mapIn( &iic_cfg_4, 0x1998 + mAddrBase );\
	mapIn( &iic_cfg_5, 0x199c + mAddrBase );\
	mapIn( &iic_cfg_6, 0x19a0 + mAddrBase );\
	\
	mapIn( &video_cfg_1, 0x19a4 + mAddrBase );\
	mapIn( &video_cfg_2, 0x19a8 + mAddrBase );\
	mapIn( &video_cfg_3, 0x19ac + mAddrBase );\
	mapIn( &std1553b_cfg_1, 0x19b0 + mAddrBase );\
	\
	mapIn( &std1553b_cfg_2, 0x19b4 + mAddrBase );\
	mapIn( &std1553b_cfg_3, 0x19b8 + mAddrBase );\
	mapIn( &std1553b_cfg_4, 0x19bc + mAddrBase );\
	mapIn( &search_version, 0x1a80 + mAddrBase );\
	\
	mapIn( &search_dat_compress_num, 0x1a84 + mAddrBase );\
	mapIn( &search_result_send_en, 0x1a88 + mAddrBase );\
	mapIn( &search_result_send_rst, 0x1a8c + mAddrBase );\
	mapIn( &search_event_total_num, 0x1a88 + mAddrBase );\
	\


#endif
