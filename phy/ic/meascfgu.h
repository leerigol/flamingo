#ifndef _MEASCFGU_IC_H_
#define _MEASCFGU_IC_H_

#define MeasCfgu_reg unsigned int

union MeasCfgu_meas_vtop_base_result_sel{
	struct{
	MeasCfgu_reg cha_top_type_sel:2;
	MeasCfgu_reg cha_base_type_sel:2;
	MeasCfgu_reg chb_top_type_sel:2;
	MeasCfgu_reg chb_base_type_sel:2;

	MeasCfgu_reg chc_top_type_sel:2;
	MeasCfgu_reg chc_base_type_sel:2;
	MeasCfgu_reg chd_top_type_sel:2;
	MeasCfgu_reg chd_base_type_sel:2;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_cha_threshold_h_mid_l_cfg{
	struct{
	MeasCfgu_reg cha_threshold_l_cfg:8;
	MeasCfgu_reg cha_threshold_mid_cfg:8;
	MeasCfgu_reg cha_threshold_h_cfg:8;
	MeasCfgu_reg cha_abs_percent_sel:1;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_chb_threshold_h_mid_l_cfg{
	struct{
	MeasCfgu_reg chb_threshold_l_cfg:8;
	MeasCfgu_reg chb_threshold_mid_cfg:8;
	MeasCfgu_reg chb_threshold_h_cfg:8;
	MeasCfgu_reg chb_abs_percent_sel:1;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_chc_threshold_h_mid_l_cfg{
	struct{
	MeasCfgu_reg chc_threshold_l_cfg:8;
	MeasCfgu_reg chc_threshold_mid_cfg:8;
	MeasCfgu_reg chc_threshold_h_cfg:8;
	MeasCfgu_reg chc_abs_percent_sel:1;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_chd_threshold_h_mid_l_cfg{
	struct{
	MeasCfgu_reg chd_threshold_l_cfg:8;
	MeasCfgu_reg chd_threshold_mid_cfg:8;
	MeasCfgu_reg chd_threshold_h_cfg:8;
	MeasCfgu_reg chd_abs_percent_sel:1;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_1{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_2{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_3{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_4{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_5{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_6{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_7{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_8{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_9{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_dly_phase_cfg_10{
	struct{
	MeasCfgu_reg edge_from_type:1;
	MeasCfgu_reg edge_to_type:1;
	MeasCfgu_reg edge_from_channel_sel:5;
	MeasCfgu_reg edge_to_channel_sel:5;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_meas_cycnum{
	struct{
	MeasCfgu_reg meas_cycnum:2;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_analog_la_sample_rate{
	struct{
	MeasCfgu_reg analog_la_sample_rate:6;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_result_rdduring_flag{
	struct{
	MeasCfgu_reg result_rdduring_flag:1;
	};
	MeasCfgu_reg payload;
};

union MeasCfgu_rw_test_reg{
	struct{
	MeasCfgu_reg rw_test_reg:16;
	};
	MeasCfgu_reg payload;
};

struct MeasCfguMemProxy {
	MeasCfgu_reg total_num;
	MeasCfgu_reg meas_statis_ctl;
	MeasCfgu_meas_vtop_base_result_sel meas_vtop_base_result_sel; 
	MeasCfgu_cha_threshold_h_mid_l_cfg cha_threshold_h_mid_l_cfg; 

	MeasCfgu_chb_threshold_h_mid_l_cfg chb_threshold_h_mid_l_cfg; 
	MeasCfgu_chc_threshold_h_mid_l_cfg chc_threshold_h_mid_l_cfg; 
	MeasCfgu_chd_threshold_h_mid_l_cfg chd_threshold_h_mid_l_cfg; 
	MeasCfgu_dly_phase_cfg_1 dly_phase_cfg_1; 

	MeasCfgu_dly_phase_cfg_2 dly_phase_cfg_2; 
	MeasCfgu_dly_phase_cfg_3 dly_phase_cfg_3; 
	MeasCfgu_dly_phase_cfg_4 dly_phase_cfg_4; 
	MeasCfgu_dly_phase_cfg_5 dly_phase_cfg_5; 

	MeasCfgu_dly_phase_cfg_6 dly_phase_cfg_6; 
	MeasCfgu_dly_phase_cfg_7 dly_phase_cfg_7; 
	MeasCfgu_dly_phase_cfg_8 dly_phase_cfg_8; 
	MeasCfgu_dly_phase_cfg_9 dly_phase_cfg_9; 

	MeasCfgu_dly_phase_cfg_10 dly_phase_cfg_10; 
	MeasCfgu_meas_cycnum meas_cycnum; 
	MeasCfgu_analog_la_sample_rate analog_la_sample_rate; 
	MeasCfgu_result_rdduring_flag result_rdduring_flag; 

	MeasCfgu_rw_test_reg rw_test_reg; 
	void assigntotal_num( MeasCfgu_reg value );

	void assignmeas_statis_ctl( MeasCfgu_reg value );

	void assignmeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg value  );
	void assignmeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg value  );
	void assignmeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg value  );
	void assignmeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg value  );
	void assignmeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg value  );
	void assignmeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg value  );
	void assignmeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg value  );
	void assignmeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg value  );

	void assignmeas_vtop_base_result_sel( MeasCfgu_reg value );


	void assigncha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg value  );
	void assigncha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg value  );
	void assigncha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg value  );
	void assigncha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg value  );

	void assigncha_threshold_h_mid_l_cfg( MeasCfgu_reg value );


	void assignchb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg value  );
	void assignchb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg value  );
	void assignchb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg value  );
	void assignchb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg value  );

	void assignchb_threshold_h_mid_l_cfg( MeasCfgu_reg value );


	void assignchc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg value  );
	void assignchc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg value  );
	void assignchc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg value  );
	void assignchc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg value  );

	void assignchc_threshold_h_mid_l_cfg( MeasCfgu_reg value );


	void assignchd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg value  );
	void assignchd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg value  );
	void assignchd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg value  );
	void assignchd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg value  );

	void assignchd_threshold_h_mid_l_cfg( MeasCfgu_reg value );


	void assigndly_phase_cfg_1_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_1_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_1( MeasCfgu_reg value );


	void assigndly_phase_cfg_2_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_2_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_2( MeasCfgu_reg value );


	void assigndly_phase_cfg_3_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_3_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_3( MeasCfgu_reg value );


	void assigndly_phase_cfg_4_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_4_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_4( MeasCfgu_reg value );


	void assigndly_phase_cfg_5_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_5_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_5( MeasCfgu_reg value );


	void assigndly_phase_cfg_6_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_6_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_6( MeasCfgu_reg value );


	void assigndly_phase_cfg_7_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_7_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_7( MeasCfgu_reg value );


	void assigndly_phase_cfg_8_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_8_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_8( MeasCfgu_reg value );


	void assigndly_phase_cfg_9_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_9_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_9( MeasCfgu_reg value );


	void assigndly_phase_cfg_10_edge_from_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_10_edge_to_type( MeasCfgu_reg value  );
	void assigndly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg value  );
	void assigndly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg value  );

	void assigndly_phase_cfg_10( MeasCfgu_reg value );


	void assignmeas_cycnum_meas_cycnum( MeasCfgu_reg value  );

	void assignmeas_cycnum( MeasCfgu_reg value );


	void assignanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg value  );

	void assignanalog_la_sample_rate( MeasCfgu_reg value );


	void assignresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg value  );

	void assignresult_rdduring_flag( MeasCfgu_reg value );


	void assignrw_test_reg_rw_test_reg( MeasCfgu_reg value  );

	void assignrw_test_reg( MeasCfgu_reg value );


};
struct MeasCfguMem : public MeasCfguMemProxy, public IPhyFpga{
protected:
	MeasCfgu_reg mAddrBase;
public:
	MeasCfguMem( MeasCfgu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( MeasCfgu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( MeasCfguMemProxy *proxy);
	void pull( MeasCfguMemProxy *proxy);

public:
	void settotal_num( MeasCfgu_reg value );
	void outtotal_num( MeasCfgu_reg value );
	void outtotal_num(  );

	void setmeas_statis_ctl( MeasCfgu_reg value );
	void outmeas_statis_ctl( MeasCfgu_reg value );
	void outmeas_statis_ctl(  );

	void setmeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg value );
	void setmeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg value );
	void setmeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg value );
	void setmeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg value );
	void setmeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg value );
	void setmeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg value );
	void setmeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg value );
	void setmeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg value );

	void setmeas_vtop_base_result_sel( MeasCfgu_reg value );
	void outmeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outmeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outmeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outmeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outmeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outmeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outmeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outmeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg value );
	void pulsemeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outmeas_vtop_base_result_sel( MeasCfgu_reg value );
	void outmeas_vtop_base_result_sel(  );


	void setcha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg value );
	void setcha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg value );
	void setcha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg value );
	void setcha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg value );

	void setcha_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outcha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg value );
	void pulsecha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outcha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg value );
	void pulsecha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outcha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg value );
	void pulsecha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outcha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg value );
	void pulsecha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outcha_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outcha_threshold_h_mid_l_cfg(  );


	void setchb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg value );
	void setchb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg value );
	void setchb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg value );
	void setchb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg value );

	void setchb_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outchb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg value );
	void pulsechb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg value );
	void pulsechb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg value );
	void pulsechb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg value );
	void pulsechb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outchb_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outchb_threshold_h_mid_l_cfg(  );


	void setchc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg value );
	void setchc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg value );
	void setchc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg value );
	void setchc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg value );

	void setchc_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outchc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg value );
	void pulsechc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg value );
	void pulsechc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg value );
	void pulsechc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg value );
	void pulsechc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outchc_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outchc_threshold_h_mid_l_cfg(  );


	void setchd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg value );
	void setchd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg value );
	void setchd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg value );
	void setchd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg value );

	void setchd_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outchd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg value );
	void pulsechd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg value );
	void pulsechd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg value );
	void pulsechd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outchd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg value );
	void pulsechd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outchd_threshold_h_mid_l_cfg( MeasCfgu_reg value );
	void outchd_threshold_h_mid_l_cfg(  );


	void setdly_phase_cfg_1_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_1_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_1( MeasCfgu_reg value );
	void outdly_phase_cfg_1_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_1_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_1_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_1_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_1( MeasCfgu_reg value );
	void outdly_phase_cfg_1(  );


	void setdly_phase_cfg_2_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_2_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_2( MeasCfgu_reg value );
	void outdly_phase_cfg_2_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_2_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_2_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_2_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_2( MeasCfgu_reg value );
	void outdly_phase_cfg_2(  );


	void setdly_phase_cfg_3_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_3_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_3( MeasCfgu_reg value );
	void outdly_phase_cfg_3_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_3_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_3_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_3_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_3( MeasCfgu_reg value );
	void outdly_phase_cfg_3(  );


	void setdly_phase_cfg_4_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_4_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_4( MeasCfgu_reg value );
	void outdly_phase_cfg_4_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_4_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_4_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_4_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_4( MeasCfgu_reg value );
	void outdly_phase_cfg_4(  );


	void setdly_phase_cfg_5_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_5_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_5( MeasCfgu_reg value );
	void outdly_phase_cfg_5_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_5_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_5_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_5_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_5( MeasCfgu_reg value );
	void outdly_phase_cfg_5(  );


	void setdly_phase_cfg_6_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_6_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_6( MeasCfgu_reg value );
	void outdly_phase_cfg_6_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_6_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_6_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_6_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_6( MeasCfgu_reg value );
	void outdly_phase_cfg_6(  );


	void setdly_phase_cfg_7_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_7_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_7( MeasCfgu_reg value );
	void outdly_phase_cfg_7_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_7_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_7_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_7_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_7( MeasCfgu_reg value );
	void outdly_phase_cfg_7(  );


	void setdly_phase_cfg_8_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_8_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_8( MeasCfgu_reg value );
	void outdly_phase_cfg_8_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_8_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_8_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_8_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_8( MeasCfgu_reg value );
	void outdly_phase_cfg_8(  );


	void setdly_phase_cfg_9_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_9_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_9( MeasCfgu_reg value );
	void outdly_phase_cfg_9_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_9_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_9_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_9_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_9( MeasCfgu_reg value );
	void outdly_phase_cfg_9(  );


	void setdly_phase_cfg_10_edge_from_type( MeasCfgu_reg value );
	void setdly_phase_cfg_10_edge_to_type( MeasCfgu_reg value );
	void setdly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg value );
	void setdly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg value );

	void setdly_phase_cfg_10( MeasCfgu_reg value );
	void outdly_phase_cfg_10_edge_from_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_10_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_10_edge_to_type( MeasCfgu_reg value );
	void pulsedly_phase_cfg_10_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );
	void outdly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg value );
	void pulsedly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outdly_phase_cfg_10( MeasCfgu_reg value );
	void outdly_phase_cfg_10(  );


	void setmeas_cycnum_meas_cycnum( MeasCfgu_reg value );

	void setmeas_cycnum( MeasCfgu_reg value );
	void outmeas_cycnum_meas_cycnum( MeasCfgu_reg value );
	void pulsemeas_cycnum_meas_cycnum( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outmeas_cycnum( MeasCfgu_reg value );
	void outmeas_cycnum(  );


	void setanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg value );

	void setanalog_la_sample_rate( MeasCfgu_reg value );
	void outanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg value );
	void pulseanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outanalog_la_sample_rate( MeasCfgu_reg value );
	void outanalog_la_sample_rate(  );


	void setresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg value );

	void setresult_rdduring_flag( MeasCfgu_reg value );
	void outresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg value );
	void pulseresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outresult_rdduring_flag( MeasCfgu_reg value );
	void outresult_rdduring_flag(  );


	void setrw_test_reg_rw_test_reg( MeasCfgu_reg value );

	void setrw_test_reg( MeasCfgu_reg value );
	void outrw_test_reg_rw_test_reg( MeasCfgu_reg value );
	void pulserw_test_reg_rw_test_reg( MeasCfgu_reg activeV, MeasCfgu_reg idleV=0, int timeus=0 );

	void outrw_test_reg( MeasCfgu_reg value );
	void outrw_test_reg(  );


public:
	cache_addr addr_total_num();
	cache_addr addr_meas_statis_ctl();
	cache_addr addr_meas_vtop_base_result_sel();
	cache_addr addr_cha_threshold_h_mid_l_cfg();

	cache_addr addr_chb_threshold_h_mid_l_cfg();
	cache_addr addr_chc_threshold_h_mid_l_cfg();
	cache_addr addr_chd_threshold_h_mid_l_cfg();
	cache_addr addr_dly_phase_cfg_1();

	cache_addr addr_dly_phase_cfg_2();
	cache_addr addr_dly_phase_cfg_3();
	cache_addr addr_dly_phase_cfg_4();
	cache_addr addr_dly_phase_cfg_5();

	cache_addr addr_dly_phase_cfg_6();
	cache_addr addr_dly_phase_cfg_7();
	cache_addr addr_dly_phase_cfg_8();
	cache_addr addr_dly_phase_cfg_9();

	cache_addr addr_dly_phase_cfg_10();
	cache_addr addr_meas_cycnum();
	cache_addr addr_analog_la_sample_rate();
	cache_addr addr_result_rdduring_flag();

	cache_addr addr_rw_test_reg();
};
#define _remap_MeasCfgu_()\
	mapIn( &total_num, 0x2400 + mAddrBase );\
	mapIn( &meas_statis_ctl, 0x2404 + mAddrBase );\
	mapIn( &meas_vtop_base_result_sel, 0x2408 + mAddrBase );\
	mapIn( &cha_threshold_h_mid_l_cfg, 0x240c + mAddrBase );\
	\
	mapIn( &chb_threshold_h_mid_l_cfg, 0x2410 + mAddrBase );\
	mapIn( &chc_threshold_h_mid_l_cfg, 0x2414 + mAddrBase );\
	mapIn( &chd_threshold_h_mid_l_cfg, 0x2418 + mAddrBase );\
	mapIn( &dly_phase_cfg_1, 0x241c + mAddrBase );\
	\
	mapIn( &dly_phase_cfg_2, 0x2420 + mAddrBase );\
	mapIn( &dly_phase_cfg_3, 0x2424 + mAddrBase );\
	mapIn( &dly_phase_cfg_4, 0x2428 + mAddrBase );\
	mapIn( &dly_phase_cfg_5, 0x242c + mAddrBase );\
	\
	mapIn( &dly_phase_cfg_6, 0x2430 + mAddrBase );\
	mapIn( &dly_phase_cfg_7, 0x2434 + mAddrBase );\
	mapIn( &dly_phase_cfg_8, 0x2438 + mAddrBase );\
	mapIn( &dly_phase_cfg_9, 0x243c + mAddrBase );\
	\
	mapIn( &dly_phase_cfg_10, 0x2440 + mAddrBase );\
	mapIn( &meas_cycnum, 0x2470 + mAddrBase );\
	mapIn( &analog_la_sample_rate, 0x2474 + mAddrBase );\
	mapIn( &result_rdduring_flag, 0x2478 + mAddrBase );\
	\
	mapIn( &rw_test_reg, 0x247c + mAddrBase );\


#endif
