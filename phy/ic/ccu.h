#ifndef _CCU_IC_H_
#define _CCU_IC_H_

#define Ccu_reg unsigned int

union Ccu_SCU_STATE{
	struct{
	Ccu_reg scu_fsm:4;
	Ccu_reg scu_sa_fsm:2;
	Ccu_reg sys_stop:1;
	Ccu_reg scu_stop:1;

	Ccu_reg scu_loop:1;
	Ccu_reg sys_force_stop:1;
	Ccu_reg process_sa:1;
	Ccu_reg process_sa_done:1;

	Ccu_reg sys_sa_en:1;
	Ccu_reg sys_pre_sa_done:1;
	Ccu_reg sys_pos_sa_done:1;
	Ccu_reg spu_wave_mem_wr_done_hold:1;

	Ccu_reg sys_trig_d:1;
	Ccu_reg tpu_scu_trig:1;
	Ccu_reg fine_trig_done_buf:1;
	Ccu_reg avg_proc_done:1;

	Ccu_reg sys_avg_proc:1;
	Ccu_reg sys_avg_exp:1;
	Ccu_reg sys_wave_rd_en:1;
	Ccu_reg sys_wave_proc_en:1;

	Ccu_reg wave_mem_rd_done_hold:1;
	Ccu_reg spu_tx_done_hold:1;
	Ccu_reg gt_wave_tx_done:1;
	Ccu_reg spu_wav_rd_meas_once_tx_done:1;

	Ccu_reg task_wave_tx_done_LA:1;
	Ccu_reg task_wave_tx_done_CH:1;
	Ccu_reg spu_proc_done:1;
	Ccu_reg scan_roll_sa_done_mem_tx_null:1;
	};
	Ccu_reg payload;
};

union Ccu_CTRL{
	struct{
	Ccu_reg sys_stop_state:1;
	Ccu_reg cfg_fsm_run_stop_n:1;
	Ccu_reg run_single:1;
	Ccu_reg mode_play_last:1;

	Ccu_reg scu_fsm_run:1;
	Ccu_reg force_trig:1;
	Ccu_reg wpu_plot_display_finish:1;
	Ccu_reg _pad0:22;
	Ccu_reg soft_reset_gt:1;

	Ccu_reg ms_sync:1;
	Ccu_reg fsm_reset:1;
	};
	Ccu_reg payload;
};

union Ccu_MODE{
	struct{
	Ccu_reg cfg_mode_scan:1;
	Ccu_reg sys_mode_scan_trace:1;
	Ccu_reg sys_mode_scan_zoom:1;
	Ccu_reg mode_scan_en:1;

	Ccu_reg search_en_zoom:1;
	Ccu_reg search_en_ch:1;
	Ccu_reg search_en_la:1;
	Ccu_reg mode_import_type:1;

	Ccu_reg mode_export:1;
	Ccu_reg mode_import_play:1;
	Ccu_reg mode_import_rec:1;
	Ccu_reg measure_en_zoom:1;

	Ccu_reg measure_en_ch:1;
	Ccu_reg measure_en_la:1;
	Ccu_reg wave_ch_en:1;
	Ccu_reg wave_la_en:1;

	Ccu_reg zoom_en:1;
	Ccu_reg mask_err_stop_en:1;
	Ccu_reg mask_save_fail:1;
	Ccu_reg mask_save_pass:1;

	Ccu_reg mask_pf_en:1;
	Ccu_reg zone_trig_en:1;
	Ccu_reg mode_roll_en:1;
	Ccu_reg mode_fast_ref:1;

	Ccu_reg auto_trig:1;
	Ccu_reg interleave_20G:4;
	Ccu_reg _pad0:2;
	Ccu_reg en_20G:1;
	};
	Ccu_reg payload;
};

union Ccu_MODE_10G{
	struct{
	Ccu_reg chn_10G:4;
	Ccu_reg cfg_chn:4;
	};
	Ccu_reg payload;
};

union Ccu_SCAN_TRIG_POSITION{
	struct{
	Ccu_reg position:31;
	Ccu_reg trig_left_side:1;
	};
	Ccu_reg payload;
};

union Ccu_REC_PLAY{
	struct{
	Ccu_reg _pad0:26;
	Ccu_reg index_full:1;
	Ccu_reg loop_playback:1;
	Ccu_reg index_load:1;
	Ccu_reg index_inc:1;

	Ccu_reg mode_play:1;
	Ccu_reg mode_rec:1;
	};
	Ccu_reg payload;
};

union Ccu_WAVE_INFO{
	struct{
	Ccu_reg wave_plot_frame_cnt:16;
	Ccu_reg _pad0:15;
	Ccu_reg wave_play_vld:1;
	};
	Ccu_reg payload;
};

union Ccu_INDEX_CUR{
	struct{
	Ccu_reg index:20;
	};
	Ccu_reg payload;
};

union Ccu_INDEX_BASE{
	struct{
	Ccu_reg index:20;
	};
	Ccu_reg payload;
};

union Ccu_INDEX_UPPER{
	struct{
	Ccu_reg index:20;
	};
	Ccu_reg payload;
};

union Ccu_TRACE{
	struct{
	Ccu_reg trace_once_req:1;
	Ccu_reg measure_once_req:1;
	Ccu_reg search_once_req:1;
	Ccu_reg eye_once_req:1;

	Ccu_reg wave_bypass_wpu:1;
	Ccu_reg wave_bypass_trace:1;
	Ccu_reg _pad0:25;
	Ccu_reg avg_clr:1;
	};
	Ccu_reg payload;
};

union Ccu_MASK_FRM_NUM_H{
	struct{
	Ccu_reg frm_num_h:16;
	Ccu_reg _pad0:15;
	Ccu_reg frm_num_en:1;
	};
	Ccu_reg payload;
};

union Ccu_MASK_TIMEOUT{
	struct{
	Ccu_reg timeout:24;
	Ccu_reg _pad0:7;
	Ccu_reg timeout_en:1;
	};
	Ccu_reg payload;
};

union Ccu_STATE_DISPLAY{
	struct{
	Ccu_reg scu_fsm:4;
	Ccu_reg sys_pre_sa_done:1;
	Ccu_reg sys_stop_state:1;
	Ccu_reg disp_tpu_scu_trig:1;

	Ccu_reg disp_sys_trig_d:1;
	Ccu_reg rec_play_stop_state:1;
	Ccu_reg mask_stop_state:1;
	Ccu_reg _pad0:20;
	Ccu_reg stop_stata_clr:1;

	Ccu_reg disp_trig_clr:1;
	};
	Ccu_reg payload;
};

union Ccu_DEUBG{
	struct{
	Ccu_reg wave_sa_ctrl:1;
	Ccu_reg sys_auto_trig_act:1;
	Ccu_reg sys_auto_trig_en:1;
	Ccu_reg sys_fine_trig_req:1;

	Ccu_reg sys_meas_only:1;
	Ccu_reg sys_meas_en:1;
	Ccu_reg sys_meas_type:2;
	Ccu_reg spu_tx_roll_null:1;

	Ccu_reg measure_once_done_hold:1;
	Ccu_reg measure_finish_done_hold:1;
	Ccu_reg _pad0:1;
	Ccu_reg sys_mask_en:1;
	Ccu_reg mask_pf_result_vld_hold:1;

	Ccu_reg mask_pf_result_cross_hold:1;
	Ccu_reg mask_pf_timeout:1;
	Ccu_reg mask_frm_overflow:1;
	Ccu_reg measure_once_buf:1;

	Ccu_reg zone_result_vld_hold:1;
	Ccu_reg zone_result_trig_hold:1;
	Ccu_reg wave_proc_la:1;
	Ccu_reg sys_zoom:1;

	Ccu_reg sys_fast_ref:1;
	Ccu_reg spu_tx_done:1;
	Ccu_reg sys_avg_en:1;
	Ccu_reg _pad1:1;
	Ccu_reg sys_trace:1;

	Ccu_reg sys_pos_sa_last_frm:1;
	Ccu_reg sys_pos_sa_view:1;
	Ccu_reg spu_wav_sa_rd_tx_done:1;
	Ccu_reg scan_roll_pos_trig_frm_buf:1;

	Ccu_reg scan_roll_pos_last_frm_buf:1;
	};
	Ccu_reg payload;
};

union Ccu_DEBUG_GT{
	struct{
	Ccu_reg receiver_gtu_state:32;
	};
	Ccu_reg payload;
};

union Ccu_DEBUG_PLOT_DONE{
	struct{
	Ccu_reg wpu_plot_done_dly:32;
	};
	Ccu_reg payload;
};

union Ccu_FPGA_SYNC{
	struct{
	Ccu_reg sync_cnt:8;
	Ccu_reg debug_scu_reset:1;
	Ccu_reg _pad0:15;
	Ccu_reg debug_sys_rst_cnt:8;
	};
	Ccu_reg payload;
};

union Ccu_WPU_ID{
	struct{
	Ccu_reg sys_wpu_plot_id:4;
	Ccu_reg _pad0:4;
	Ccu_reg wpu_plot_done_id:4;
	Ccu_reg wpu_display_done_id:4;
	};
	Ccu_reg payload;
};

union Ccu_DEBUG_WPU_PLOT{
	struct{
	Ccu_reg wpu_plot_display_finish:1;
	Ccu_reg wpu_fast_done_buf:1;
	Ccu_reg wpu_display_done_buf:1;
	Ccu_reg wpu_plot_display_id_equ:1;

	Ccu_reg scu_wpu_plot_last:1;
	Ccu_reg scu_wave_plot_req:1;
	Ccu_reg scu_wpu_start:1;
	Ccu_reg sys_wpu_plot:1;

	Ccu_reg wpu_plot_done_buf:1;
	Ccu_reg scu_wpu_plot_en:1;
	Ccu_reg wpu_plot_scan_finish_buf:1;
	Ccu_reg wpu_plot_scan_zoom_finish_buf:1;

	Ccu_reg scu_wpu_stop_plot_en:1;
	Ccu_reg scu_wpu_stop_plot_vld:1;
	Ccu_reg _pad0:16;
	Ccu_reg cfg_wpu_flush:1;
	Ccu_reg cfg_stop_plot:1;
	};
	Ccu_reg payload;
};

union Ccu_DEBUG_GT_CRC{
	struct{
	Ccu_reg fail_sum:28;
	Ccu_reg _pad0:2;
	Ccu_reg crc_pass_fail_n_i:1;
	Ccu_reg crc_valid_i:1;
	};
	Ccu_reg payload;
};

struct CcuMemProxy {
	Ccu_reg VERSION;
	Ccu_SCU_STATE SCU_STATE; 
	Ccu_CTRL CTRL; 
	Ccu_MODE MODE; 

	Ccu_MODE_10G MODE_10G; 
	Ccu_reg PRE_SA;
	Ccu_reg POS_SA;
	Ccu_SCAN_TRIG_POSITION SCAN_TRIG_POSITION; 

	Ccu_reg AUTO_TRIG_TIMEOUT;
	Ccu_reg AUTO_TRIG_HOLD_OFF;
	Ccu_reg FSM_DELAY;
	Ccu_reg PLOT_DELAY;

	Ccu_reg FRAME_PLOT_NUM;
	Ccu_REC_PLAY REC_PLAY; 
	Ccu_WAVE_INFO WAVE_INFO; 
	Ccu_INDEX_CUR INDEX_CUR; 

	Ccu_INDEX_BASE INDEX_BASE; 
	Ccu_INDEX_UPPER INDEX_UPPER; 
	Ccu_TRACE TRACE; 
	Ccu_reg TRIG_DLY;

	Ccu_reg POS_SA_LAST_VIEW;
	Ccu_reg MASK_FRM_NUM_L;
	Ccu_MASK_FRM_NUM_H MASK_FRM_NUM_H; 
	Ccu_MASK_TIMEOUT MASK_TIMEOUT; 

	Ccu_STATE_DISPLAY STATE_DISPLAY; 
	Ccu_reg MASK_TIMER;
	Ccu_reg CONFIG_ID;
	Ccu_DEUBG DEUBG; 

	Ccu_DEBUG_GT DEBUG_GT; 
	Ccu_DEBUG_PLOT_DONE DEBUG_PLOT_DONE; 
	Ccu_FPGA_SYNC FPGA_SYNC; 
	Ccu_WPU_ID WPU_ID; 

	Ccu_DEBUG_WPU_PLOT DEBUG_WPU_PLOT; 
	Ccu_DEBUG_GT_CRC DEBUG_GT_CRC; 
	Ccu_reg getVERSION(  );

	void assignSCU_STATE_scu_fsm( Ccu_reg value  );
	void assignSCU_STATE_scu_sa_fsm( Ccu_reg value  );
	void assignSCU_STATE_sys_stop( Ccu_reg value  );
	void assignSCU_STATE_scu_stop( Ccu_reg value  );
	void assignSCU_STATE_scu_loop( Ccu_reg value  );
	void assignSCU_STATE_sys_force_stop( Ccu_reg value  );
	void assignSCU_STATE_process_sa( Ccu_reg value  );
	void assignSCU_STATE_process_sa_done( Ccu_reg value  );
	void assignSCU_STATE_sys_sa_en( Ccu_reg value  );
	void assignSCU_STATE_sys_pre_sa_done( Ccu_reg value  );
	void assignSCU_STATE_sys_pos_sa_done( Ccu_reg value  );
	void assignSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg value  );
	void assignSCU_STATE_sys_trig_d( Ccu_reg value  );
	void assignSCU_STATE_tpu_scu_trig( Ccu_reg value  );
	void assignSCU_STATE_fine_trig_done_buf( Ccu_reg value  );
	void assignSCU_STATE_avg_proc_done( Ccu_reg value  );
	void assignSCU_STATE_sys_avg_proc( Ccu_reg value  );
	void assignSCU_STATE_sys_avg_exp( Ccu_reg value  );
	void assignSCU_STATE_sys_wave_rd_en( Ccu_reg value  );
	void assignSCU_STATE_sys_wave_proc_en( Ccu_reg value  );
	void assignSCU_STATE_wave_mem_rd_done_hold( Ccu_reg value  );
	void assignSCU_STATE_spu_tx_done_hold( Ccu_reg value  );
	void assignSCU_STATE_gt_wave_tx_done( Ccu_reg value  );
	void assignSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg value  );
	void assignSCU_STATE_task_wave_tx_done_LA( Ccu_reg value  );
	void assignSCU_STATE_task_wave_tx_done_CH( Ccu_reg value  );
	void assignSCU_STATE_spu_proc_done( Ccu_reg value  );
	void assignSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg value  );

	void assignSCU_STATE( Ccu_reg value );


	Ccu_reg getSCU_STATE_scu_fsm(  );
	Ccu_reg getSCU_STATE_scu_sa_fsm(  );
	Ccu_reg getSCU_STATE_sys_stop(  );
	Ccu_reg getSCU_STATE_scu_stop(  );
	Ccu_reg getSCU_STATE_scu_loop(  );
	Ccu_reg getSCU_STATE_sys_force_stop(  );
	Ccu_reg getSCU_STATE_process_sa(  );
	Ccu_reg getSCU_STATE_process_sa_done(  );
	Ccu_reg getSCU_STATE_sys_sa_en(  );
	Ccu_reg getSCU_STATE_sys_pre_sa_done(  );
	Ccu_reg getSCU_STATE_sys_pos_sa_done(  );
	Ccu_reg getSCU_STATE_spu_wave_mem_wr_done_hold(  );
	Ccu_reg getSCU_STATE_sys_trig_d(  );
	Ccu_reg getSCU_STATE_tpu_scu_trig(  );
	Ccu_reg getSCU_STATE_fine_trig_done_buf(  );
	Ccu_reg getSCU_STATE_avg_proc_done(  );
	Ccu_reg getSCU_STATE_sys_avg_proc(  );
	Ccu_reg getSCU_STATE_sys_avg_exp(  );
	Ccu_reg getSCU_STATE_sys_wave_rd_en(  );
	Ccu_reg getSCU_STATE_sys_wave_proc_en(  );
	Ccu_reg getSCU_STATE_wave_mem_rd_done_hold(  );
	Ccu_reg getSCU_STATE_spu_tx_done_hold(  );
	Ccu_reg getSCU_STATE_gt_wave_tx_done(  );
	Ccu_reg getSCU_STATE_spu_wav_rd_meas_once_tx_done(  );
	Ccu_reg getSCU_STATE_task_wave_tx_done_LA(  );
	Ccu_reg getSCU_STATE_task_wave_tx_done_CH(  );
	Ccu_reg getSCU_STATE_spu_proc_done(  );
	Ccu_reg getSCU_STATE_scan_roll_sa_done_mem_tx_null(  );

	Ccu_reg getSCU_STATE(  );

	void assignCTRL_sys_stop_state( Ccu_reg value  );
	void assignCTRL_cfg_fsm_run_stop_n( Ccu_reg value  );
	void assignCTRL_run_single( Ccu_reg value  );
	void assignCTRL_mode_play_last( Ccu_reg value  );
	void assignCTRL_scu_fsm_run( Ccu_reg value  );
	void assignCTRL_force_trig( Ccu_reg value  );
	void assignCTRL_wpu_plot_display_finish( Ccu_reg value  );
	void assignCTRL_soft_reset_gt( Ccu_reg value  );
	void assignCTRL_ms_sync( Ccu_reg value  );
	void assignCTRL_fsm_reset( Ccu_reg value  );

	void assignCTRL( Ccu_reg value );


	Ccu_reg getCTRL_sys_stop_state(  );
	Ccu_reg getCTRL_cfg_fsm_run_stop_n(  );
	Ccu_reg getCTRL_run_single(  );
	Ccu_reg getCTRL_mode_play_last(  );
	Ccu_reg getCTRL_scu_fsm_run(  );
	Ccu_reg getCTRL_force_trig(  );
	Ccu_reg getCTRL_wpu_plot_display_finish(  );
	Ccu_reg getCTRL_soft_reset_gt(  );
	Ccu_reg getCTRL_ms_sync(  );
	Ccu_reg getCTRL_fsm_reset(  );

	Ccu_reg getCTRL(  );

	void assignMODE_cfg_mode_scan( Ccu_reg value  );
	void assignMODE_sys_mode_scan_trace( Ccu_reg value  );
	void assignMODE_sys_mode_scan_zoom( Ccu_reg value  );
	void assignMODE_mode_scan_en( Ccu_reg value  );
	void assignMODE_search_en_zoom( Ccu_reg value  );
	void assignMODE_search_en_ch( Ccu_reg value  );
	void assignMODE_search_en_la( Ccu_reg value  );
	void assignMODE_mode_import_type( Ccu_reg value  );
	void assignMODE_mode_export( Ccu_reg value  );
	void assignMODE_mode_import_play( Ccu_reg value  );
	void assignMODE_mode_import_rec( Ccu_reg value  );
	void assignMODE_measure_en_zoom( Ccu_reg value  );
	void assignMODE_measure_en_ch( Ccu_reg value  );
	void assignMODE_measure_en_la( Ccu_reg value  );
	void assignMODE_wave_ch_en( Ccu_reg value  );
	void assignMODE_wave_la_en( Ccu_reg value  );
	void assignMODE_zoom_en( Ccu_reg value  );
	void assignMODE_mask_err_stop_en( Ccu_reg value  );
	void assignMODE_mask_save_fail( Ccu_reg value  );
	void assignMODE_mask_save_pass( Ccu_reg value  );
	void assignMODE_mask_pf_en( Ccu_reg value  );
	void assignMODE_zone_trig_en( Ccu_reg value  );
	void assignMODE_mode_roll_en( Ccu_reg value  );
	void assignMODE_mode_fast_ref( Ccu_reg value  );
	void assignMODE_auto_trig( Ccu_reg value  );
	void assignMODE_interleave_20G( Ccu_reg value  );
	void assignMODE_en_20G( Ccu_reg value  );

	void assignMODE( Ccu_reg value );


	Ccu_reg getMODE_cfg_mode_scan(  );
	Ccu_reg getMODE_sys_mode_scan_trace(  );
	Ccu_reg getMODE_sys_mode_scan_zoom(  );
	Ccu_reg getMODE_mode_scan_en(  );
	Ccu_reg getMODE_search_en_zoom(  );
	Ccu_reg getMODE_search_en_ch(  );
	Ccu_reg getMODE_search_en_la(  );
	Ccu_reg getMODE_mode_import_type(  );
	Ccu_reg getMODE_mode_export(  );
	Ccu_reg getMODE_mode_import_play(  );
	Ccu_reg getMODE_mode_import_rec(  );
	Ccu_reg getMODE_measure_en_zoom(  );
	Ccu_reg getMODE_measure_en_ch(  );
	Ccu_reg getMODE_measure_en_la(  );
	Ccu_reg getMODE_wave_ch_en(  );
	Ccu_reg getMODE_wave_la_en(  );
	Ccu_reg getMODE_zoom_en(  );
	Ccu_reg getMODE_mask_err_stop_en(  );
	Ccu_reg getMODE_mask_save_fail(  );
	Ccu_reg getMODE_mask_save_pass(  );
	Ccu_reg getMODE_mask_pf_en(  );
	Ccu_reg getMODE_zone_trig_en(  );
	Ccu_reg getMODE_mode_roll_en(  );
	Ccu_reg getMODE_mode_fast_ref(  );
	Ccu_reg getMODE_auto_trig(  );
	Ccu_reg getMODE_interleave_20G(  );
	Ccu_reg getMODE_en_20G(  );

	Ccu_reg getMODE(  );

	void assignMODE_10G_chn_10G( Ccu_reg value  );
	void assignMODE_10G_cfg_chn( Ccu_reg value  );

	void assignMODE_10G( Ccu_reg value );


	Ccu_reg getMODE_10G_chn_10G(  );
	Ccu_reg getMODE_10G_cfg_chn(  );

	Ccu_reg getMODE_10G(  );

	void assignPRE_SA( Ccu_reg value );

	Ccu_reg getPRE_SA(  );

	void assignPOS_SA( Ccu_reg value );

	Ccu_reg getPOS_SA(  );

	void assignSCAN_TRIG_POSITION_position( Ccu_reg value  );
	void assignSCAN_TRIG_POSITION_trig_left_side( Ccu_reg value  );

	void assignSCAN_TRIG_POSITION( Ccu_reg value );


	Ccu_reg getSCAN_TRIG_POSITION_position(  );
	Ccu_reg getSCAN_TRIG_POSITION_trig_left_side(  );

	Ccu_reg getSCAN_TRIG_POSITION(  );

	void assignAUTO_TRIG_TIMEOUT( Ccu_reg value );

	Ccu_reg getAUTO_TRIG_TIMEOUT(  );

	void assignAUTO_TRIG_HOLD_OFF( Ccu_reg value );

	Ccu_reg getAUTO_TRIG_HOLD_OFF(  );

	void assignFSM_DELAY( Ccu_reg value );

	Ccu_reg getFSM_DELAY(  );

	void assignPLOT_DELAY( Ccu_reg value );

	Ccu_reg getPLOT_DELAY(  );

	void assignFRAME_PLOT_NUM( Ccu_reg value );

	Ccu_reg getFRAME_PLOT_NUM(  );

	void assignREC_PLAY_index_full( Ccu_reg value  );
	void assignREC_PLAY_loop_playback( Ccu_reg value  );
	void assignREC_PLAY_index_load( Ccu_reg value  );
	void assignREC_PLAY_index_inc( Ccu_reg value  );
	void assignREC_PLAY_mode_play( Ccu_reg value  );
	void assignREC_PLAY_mode_rec( Ccu_reg value  );

	void assignREC_PLAY( Ccu_reg value );


	Ccu_reg getREC_PLAY_index_full(  );
	Ccu_reg getREC_PLAY_loop_playback(  );
	Ccu_reg getREC_PLAY_index_load(  );
	Ccu_reg getREC_PLAY_index_inc(  );
	Ccu_reg getREC_PLAY_mode_play(  );
	Ccu_reg getREC_PLAY_mode_rec(  );

	Ccu_reg getREC_PLAY(  );

	void assignWAVE_INFO_wave_plot_frame_cnt( Ccu_reg value  );
	void assignWAVE_INFO_wave_play_vld( Ccu_reg value  );

	void assignWAVE_INFO( Ccu_reg value );


	Ccu_reg getWAVE_INFO_wave_plot_frame_cnt(  );
	Ccu_reg getWAVE_INFO_wave_play_vld(  );

	Ccu_reg getWAVE_INFO(  );

	void assignINDEX_CUR_index( Ccu_reg value  );

	void assignINDEX_CUR( Ccu_reg value );


	Ccu_reg getINDEX_CUR_index(  );

	Ccu_reg getINDEX_CUR(  );

	void assignINDEX_BASE_index( Ccu_reg value  );

	void assignINDEX_BASE( Ccu_reg value );


	Ccu_reg getINDEX_BASE_index(  );

	Ccu_reg getINDEX_BASE(  );

	void assignINDEX_UPPER_index( Ccu_reg value  );

	void assignINDEX_UPPER( Ccu_reg value );


	Ccu_reg getINDEX_UPPER_index(  );

	Ccu_reg getINDEX_UPPER(  );

	void assignTRACE_trace_once_req( Ccu_reg value  );
	void assignTRACE_measure_once_req( Ccu_reg value  );
	void assignTRACE_search_once_req( Ccu_reg value  );
	void assignTRACE_eye_once_req( Ccu_reg value  );
	void assignTRACE_wave_bypass_wpu( Ccu_reg value  );
	void assignTRACE_wave_bypass_trace( Ccu_reg value  );
	void assignTRACE_avg_clr( Ccu_reg value  );

	void assignTRACE( Ccu_reg value );


	Ccu_reg getTRACE_trace_once_req(  );
	Ccu_reg getTRACE_measure_once_req(  );
	Ccu_reg getTRACE_search_once_req(  );
	Ccu_reg getTRACE_eye_once_req(  );
	Ccu_reg getTRACE_wave_bypass_wpu(  );
	Ccu_reg getTRACE_wave_bypass_trace(  );
	Ccu_reg getTRACE_avg_clr(  );

	Ccu_reg getTRACE(  );

	void assignTRIG_DLY( Ccu_reg value );

	Ccu_reg getTRIG_DLY(  );

	void assignPOS_SA_LAST_VIEW( Ccu_reg value );

	Ccu_reg getPOS_SA_LAST_VIEW(  );

	void assignMASK_FRM_NUM_L( Ccu_reg value );

	Ccu_reg getMASK_FRM_NUM_L(  );

	void assignMASK_FRM_NUM_H_frm_num_h( Ccu_reg value  );
	void assignMASK_FRM_NUM_H_frm_num_en( Ccu_reg value  );

	void assignMASK_FRM_NUM_H( Ccu_reg value );


	Ccu_reg getMASK_FRM_NUM_H_frm_num_h(  );
	Ccu_reg getMASK_FRM_NUM_H_frm_num_en(  );

	Ccu_reg getMASK_FRM_NUM_H(  );

	void assignMASK_TIMEOUT_timeout( Ccu_reg value  );
	void assignMASK_TIMEOUT_timeout_en( Ccu_reg value  );

	void assignMASK_TIMEOUT( Ccu_reg value );


	Ccu_reg getMASK_TIMEOUT_timeout(  );
	Ccu_reg getMASK_TIMEOUT_timeout_en(  );

	Ccu_reg getMASK_TIMEOUT(  );

	void assignSTATE_DISPLAY_scu_fsm( Ccu_reg value  );
	void assignSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg value  );
	void assignSTATE_DISPLAY_sys_stop_state( Ccu_reg value  );
	void assignSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg value  );
	void assignSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg value  );
	void assignSTATE_DISPLAY_rec_play_stop_state( Ccu_reg value  );
	void assignSTATE_DISPLAY_mask_stop_state( Ccu_reg value  );
	void assignSTATE_DISPLAY_stop_stata_clr( Ccu_reg value  );
	void assignSTATE_DISPLAY_disp_trig_clr( Ccu_reg value  );

	void assignSTATE_DISPLAY( Ccu_reg value );


	Ccu_reg getSTATE_DISPLAY_scu_fsm(  );
	Ccu_reg getSTATE_DISPLAY_sys_pre_sa_done(  );
	Ccu_reg getSTATE_DISPLAY_sys_stop_state(  );
	Ccu_reg getSTATE_DISPLAY_disp_tpu_scu_trig(  );
	Ccu_reg getSTATE_DISPLAY_disp_sys_trig_d(  );
	Ccu_reg getSTATE_DISPLAY_rec_play_stop_state(  );
	Ccu_reg getSTATE_DISPLAY_mask_stop_state(  );
	Ccu_reg getSTATE_DISPLAY_stop_stata_clr(  );
	Ccu_reg getSTATE_DISPLAY_disp_trig_clr(  );

	Ccu_reg getSTATE_DISPLAY(  );

	Ccu_reg getMASK_TIMER(  );

	void assignCONFIG_ID( Ccu_reg value );

	Ccu_reg getCONFIG_ID(  );

	void assignDEUBG_wave_sa_ctrl( Ccu_reg value  );
	void assignDEUBG_sys_auto_trig_act( Ccu_reg value  );
	void assignDEUBG_sys_auto_trig_en( Ccu_reg value  );
	void assignDEUBG_sys_fine_trig_req( Ccu_reg value  );
	void assignDEUBG_sys_meas_only( Ccu_reg value  );
	void assignDEUBG_sys_meas_en( Ccu_reg value  );
	void assignDEUBG_sys_meas_type( Ccu_reg value  );
	void assignDEUBG_spu_tx_roll_null( Ccu_reg value  );
	void assignDEUBG_measure_once_done_hold( Ccu_reg value  );
	void assignDEUBG_measure_finish_done_hold( Ccu_reg value  );
	void assignDEUBG_sys_mask_en( Ccu_reg value  );
	void assignDEUBG_mask_pf_result_vld_hold( Ccu_reg value  );
	void assignDEUBG_mask_pf_result_cross_hold( Ccu_reg value  );
	void assignDEUBG_mask_pf_timeout( Ccu_reg value  );
	void assignDEUBG_mask_frm_overflow( Ccu_reg value  );
	void assignDEUBG_measure_once_buf( Ccu_reg value  );
	void assignDEUBG_zone_result_vld_hold( Ccu_reg value  );
	void assignDEUBG_zone_result_trig_hold( Ccu_reg value  );
	void assignDEUBG_wave_proc_la( Ccu_reg value  );
	void assignDEUBG_sys_zoom( Ccu_reg value  );
	void assignDEUBG_sys_fast_ref( Ccu_reg value  );
	void assignDEUBG_spu_tx_done( Ccu_reg value  );
	void assignDEUBG_sys_avg_en( Ccu_reg value  );
	void assignDEUBG_sys_trace( Ccu_reg value  );
	void assignDEUBG_sys_pos_sa_last_frm( Ccu_reg value  );
	void assignDEUBG_sys_pos_sa_view( Ccu_reg value  );
	void assignDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg value  );
	void assignDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg value  );
	void assignDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg value  );

	void assignDEUBG( Ccu_reg value );


	Ccu_reg getDEUBG_wave_sa_ctrl(  );
	Ccu_reg getDEUBG_sys_auto_trig_act(  );
	Ccu_reg getDEUBG_sys_auto_trig_en(  );
	Ccu_reg getDEUBG_sys_fine_trig_req(  );
	Ccu_reg getDEUBG_sys_meas_only(  );
	Ccu_reg getDEUBG_sys_meas_en(  );
	Ccu_reg getDEUBG_sys_meas_type(  );
	Ccu_reg getDEUBG_spu_tx_roll_null(  );
	Ccu_reg getDEUBG_measure_once_done_hold(  );
	Ccu_reg getDEUBG_measure_finish_done_hold(  );
	Ccu_reg getDEUBG_sys_mask_en(  );
	Ccu_reg getDEUBG_mask_pf_result_vld_hold(  );
	Ccu_reg getDEUBG_mask_pf_result_cross_hold(  );
	Ccu_reg getDEUBG_mask_pf_timeout(  );
	Ccu_reg getDEUBG_mask_frm_overflow(  );
	Ccu_reg getDEUBG_measure_once_buf(  );
	Ccu_reg getDEUBG_zone_result_vld_hold(  );
	Ccu_reg getDEUBG_zone_result_trig_hold(  );
	Ccu_reg getDEUBG_wave_proc_la(  );
	Ccu_reg getDEUBG_sys_zoom(  );
	Ccu_reg getDEUBG_sys_fast_ref(  );
	Ccu_reg getDEUBG_spu_tx_done(  );
	Ccu_reg getDEUBG_sys_avg_en(  );
	Ccu_reg getDEUBG_sys_trace(  );
	Ccu_reg getDEUBG_sys_pos_sa_last_frm(  );
	Ccu_reg getDEUBG_sys_pos_sa_view(  );
	Ccu_reg getDEUBG_spu_wav_sa_rd_tx_done(  );
	Ccu_reg getDEUBG_scan_roll_pos_trig_frm_buf(  );
	Ccu_reg getDEUBG_scan_roll_pos_last_frm_buf(  );

	Ccu_reg getDEUBG(  );

	Ccu_reg getDEBUG_GT_receiver_gtu_state(  );

	Ccu_reg getDEBUG_GT(  );

	void assignDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg value  );

	void assignDEBUG_PLOT_DONE( Ccu_reg value );


	Ccu_reg getDEBUG_PLOT_DONE_wpu_plot_done_dly(  );

	Ccu_reg getDEBUG_PLOT_DONE(  );

	void assignFPGA_SYNC_sync_cnt( Ccu_reg value  );
	void assignFPGA_SYNC_debug_scu_reset( Ccu_reg value  );
	void assignFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg value  );

	void assignFPGA_SYNC( Ccu_reg value );


	Ccu_reg getFPGA_SYNC_sync_cnt(  );
	Ccu_reg getFPGA_SYNC_debug_scu_reset(  );
	Ccu_reg getFPGA_SYNC_debug_sys_rst_cnt(  );

	Ccu_reg getFPGA_SYNC(  );

	Ccu_reg getWPU_ID_sys_wpu_plot_id(  );
	Ccu_reg getWPU_ID_wpu_plot_done_id(  );
	Ccu_reg getWPU_ID_wpu_display_done_id(  );

	Ccu_reg getWPU_ID(  );

	void assignDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg value  );
	void assignDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg value  );

	void assignDEBUG_WPU_PLOT( Ccu_reg value );


	Ccu_reg getDEBUG_WPU_PLOT_wpu_plot_display_finish(  );
	Ccu_reg getDEBUG_WPU_PLOT_wpu_fast_done_buf(  );
	Ccu_reg getDEBUG_WPU_PLOT_wpu_display_done_buf(  );
	Ccu_reg getDEBUG_WPU_PLOT_wpu_plot_display_id_equ(  );
	Ccu_reg getDEBUG_WPU_PLOT_scu_wpu_plot_last(  );
	Ccu_reg getDEBUG_WPU_PLOT_scu_wave_plot_req(  );
	Ccu_reg getDEBUG_WPU_PLOT_scu_wpu_start(  );
	Ccu_reg getDEBUG_WPU_PLOT_sys_wpu_plot(  );
	Ccu_reg getDEBUG_WPU_PLOT_wpu_plot_done_buf(  );
	Ccu_reg getDEBUG_WPU_PLOT_scu_wpu_plot_en(  );
	Ccu_reg getDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(  );
	Ccu_reg getDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(  );
	Ccu_reg getDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(  );
	Ccu_reg getDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(  );
	Ccu_reg getDEBUG_WPU_PLOT_cfg_wpu_flush(  );
	Ccu_reg getDEBUG_WPU_PLOT_cfg_stop_plot(  );

	Ccu_reg getDEBUG_WPU_PLOT(  );

	void assignDEBUG_GT_CRC_fail_sum( Ccu_reg value  );
	void assignDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg value  );
	void assignDEBUG_GT_CRC_crc_valid_i( Ccu_reg value  );

	void assignDEBUG_GT_CRC( Ccu_reg value );


	Ccu_reg getDEBUG_GT_CRC_fail_sum(  );
	Ccu_reg getDEBUG_GT_CRC_crc_pass_fail_n_i(  );
	Ccu_reg getDEBUG_GT_CRC_crc_valid_i(  );

	Ccu_reg getDEBUG_GT_CRC(  );

};
struct CcuMem : public CcuMemProxy, public IPhyFpga{
protected:
	Ccu_reg mAddrBase;
public:
	CcuMem( Ccu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( Ccu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( CcuMemProxy *proxy);
	void pull( CcuMemProxy *proxy);

public:
	Ccu_reg inVERSION(  );

	void setSCU_STATE_scu_fsm( Ccu_reg value );
	void setSCU_STATE_scu_sa_fsm( Ccu_reg value );
	void setSCU_STATE_sys_stop( Ccu_reg value );
	void setSCU_STATE_scu_stop( Ccu_reg value );
	void setSCU_STATE_scu_loop( Ccu_reg value );
	void setSCU_STATE_sys_force_stop( Ccu_reg value );
	void setSCU_STATE_process_sa( Ccu_reg value );
	void setSCU_STATE_process_sa_done( Ccu_reg value );
	void setSCU_STATE_sys_sa_en( Ccu_reg value );
	void setSCU_STATE_sys_pre_sa_done( Ccu_reg value );
	void setSCU_STATE_sys_pos_sa_done( Ccu_reg value );
	void setSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg value );
	void setSCU_STATE_sys_trig_d( Ccu_reg value );
	void setSCU_STATE_tpu_scu_trig( Ccu_reg value );
	void setSCU_STATE_fine_trig_done_buf( Ccu_reg value );
	void setSCU_STATE_avg_proc_done( Ccu_reg value );
	void setSCU_STATE_sys_avg_proc( Ccu_reg value );
	void setSCU_STATE_sys_avg_exp( Ccu_reg value );
	void setSCU_STATE_sys_wave_rd_en( Ccu_reg value );
	void setSCU_STATE_sys_wave_proc_en( Ccu_reg value );
	void setSCU_STATE_wave_mem_rd_done_hold( Ccu_reg value );
	void setSCU_STATE_spu_tx_done_hold( Ccu_reg value );
	void setSCU_STATE_gt_wave_tx_done( Ccu_reg value );
	void setSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg value );
	void setSCU_STATE_task_wave_tx_done_LA( Ccu_reg value );
	void setSCU_STATE_task_wave_tx_done_CH( Ccu_reg value );
	void setSCU_STATE_spu_proc_done( Ccu_reg value );
	void setSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg value );

	void setSCU_STATE( Ccu_reg value );
	void outSCU_STATE_scu_fsm( Ccu_reg value );
	void pulseSCU_STATE_scu_fsm( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scu_sa_fsm( Ccu_reg value );
	void pulseSCU_STATE_scu_sa_fsm( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_stop( Ccu_reg value );
	void pulseSCU_STATE_sys_stop( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scu_stop( Ccu_reg value );
	void pulseSCU_STATE_scu_stop( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scu_loop( Ccu_reg value );
	void pulseSCU_STATE_scu_loop( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_force_stop( Ccu_reg value );
	void pulseSCU_STATE_sys_force_stop( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_process_sa( Ccu_reg value );
	void pulseSCU_STATE_process_sa( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_process_sa_done( Ccu_reg value );
	void pulseSCU_STATE_process_sa_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_sa_en( Ccu_reg value );
	void pulseSCU_STATE_sys_sa_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_pre_sa_done( Ccu_reg value );
	void pulseSCU_STATE_sys_pre_sa_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_pos_sa_done( Ccu_reg value );
	void pulseSCU_STATE_sys_pos_sa_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg value );
	void pulseSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_trig_d( Ccu_reg value );
	void pulseSCU_STATE_sys_trig_d( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_tpu_scu_trig( Ccu_reg value );
	void pulseSCU_STATE_tpu_scu_trig( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_fine_trig_done_buf( Ccu_reg value );
	void pulseSCU_STATE_fine_trig_done_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_avg_proc_done( Ccu_reg value );
	void pulseSCU_STATE_avg_proc_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_avg_proc( Ccu_reg value );
	void pulseSCU_STATE_sys_avg_proc( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_avg_exp( Ccu_reg value );
	void pulseSCU_STATE_sys_avg_exp( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_wave_rd_en( Ccu_reg value );
	void pulseSCU_STATE_sys_wave_rd_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_wave_proc_en( Ccu_reg value );
	void pulseSCU_STATE_sys_wave_proc_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_wave_mem_rd_done_hold( Ccu_reg value );
	void pulseSCU_STATE_wave_mem_rd_done_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_tx_done_hold( Ccu_reg value );
	void pulseSCU_STATE_spu_tx_done_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_gt_wave_tx_done( Ccu_reg value );
	void pulseSCU_STATE_gt_wave_tx_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg value );
	void pulseSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_task_wave_tx_done_LA( Ccu_reg value );
	void pulseSCU_STATE_task_wave_tx_done_LA( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_task_wave_tx_done_CH( Ccu_reg value );
	void pulseSCU_STATE_task_wave_tx_done_CH( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_proc_done( Ccu_reg value );
	void pulseSCU_STATE_spu_proc_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg value );
	void pulseSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outSCU_STATE( Ccu_reg value );
	void outSCU_STATE(  );


	Ccu_reg inSCU_STATE(  );

	void setCTRL_sys_stop_state( Ccu_reg value );
	void setCTRL_cfg_fsm_run_stop_n( Ccu_reg value );
	void setCTRL_run_single( Ccu_reg value );
	void setCTRL_mode_play_last( Ccu_reg value );
	void setCTRL_scu_fsm_run( Ccu_reg value );
	void setCTRL_force_trig( Ccu_reg value );
	void setCTRL_wpu_plot_display_finish( Ccu_reg value );
	void setCTRL_soft_reset_gt( Ccu_reg value );
	void setCTRL_ms_sync( Ccu_reg value );
	void setCTRL_fsm_reset( Ccu_reg value );

	void setCTRL( Ccu_reg value );
	void outCTRL_sys_stop_state( Ccu_reg value );
	void pulseCTRL_sys_stop_state( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_cfg_fsm_run_stop_n( Ccu_reg value );
	void pulseCTRL_cfg_fsm_run_stop_n( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_run_single( Ccu_reg value );
	void pulseCTRL_run_single( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_mode_play_last( Ccu_reg value );
	void pulseCTRL_mode_play_last( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_scu_fsm_run( Ccu_reg value );
	void pulseCTRL_scu_fsm_run( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_force_trig( Ccu_reg value );
	void pulseCTRL_force_trig( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_wpu_plot_display_finish( Ccu_reg value );
	void pulseCTRL_wpu_plot_display_finish( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_soft_reset_gt( Ccu_reg value );
	void pulseCTRL_soft_reset_gt( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_ms_sync( Ccu_reg value );
	void pulseCTRL_ms_sync( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outCTRL_fsm_reset( Ccu_reg value );
	void pulseCTRL_fsm_reset( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outCTRL( Ccu_reg value );
	void outCTRL(  );


	Ccu_reg inCTRL(  );

	void setMODE_cfg_mode_scan( Ccu_reg value );
	void setMODE_sys_mode_scan_trace( Ccu_reg value );
	void setMODE_sys_mode_scan_zoom( Ccu_reg value );
	void setMODE_mode_scan_en( Ccu_reg value );
	void setMODE_search_en_zoom( Ccu_reg value );
	void setMODE_search_en_ch( Ccu_reg value );
	void setMODE_search_en_la( Ccu_reg value );
	void setMODE_mode_import_type( Ccu_reg value );
	void setMODE_mode_export( Ccu_reg value );
	void setMODE_mode_import_play( Ccu_reg value );
	void setMODE_mode_import_rec( Ccu_reg value );
	void setMODE_measure_en_zoom( Ccu_reg value );
	void setMODE_measure_en_ch( Ccu_reg value );
	void setMODE_measure_en_la( Ccu_reg value );
	void setMODE_wave_ch_en( Ccu_reg value );
	void setMODE_wave_la_en( Ccu_reg value );
	void setMODE_zoom_en( Ccu_reg value );
	void setMODE_mask_err_stop_en( Ccu_reg value );
	void setMODE_mask_save_fail( Ccu_reg value );
	void setMODE_mask_save_pass( Ccu_reg value );
	void setMODE_mask_pf_en( Ccu_reg value );
	void setMODE_zone_trig_en( Ccu_reg value );
	void setMODE_mode_roll_en( Ccu_reg value );
	void setMODE_mode_fast_ref( Ccu_reg value );
	void setMODE_auto_trig( Ccu_reg value );
	void setMODE_interleave_20G( Ccu_reg value );
	void setMODE_en_20G( Ccu_reg value );

	void setMODE( Ccu_reg value );
	void outMODE_cfg_mode_scan( Ccu_reg value );
	void pulseMODE_cfg_mode_scan( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_sys_mode_scan_trace( Ccu_reg value );
	void pulseMODE_sys_mode_scan_trace( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_sys_mode_scan_zoom( Ccu_reg value );
	void pulseMODE_sys_mode_scan_zoom( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mode_scan_en( Ccu_reg value );
	void pulseMODE_mode_scan_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_search_en_zoom( Ccu_reg value );
	void pulseMODE_search_en_zoom( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_search_en_ch( Ccu_reg value );
	void pulseMODE_search_en_ch( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_search_en_la( Ccu_reg value );
	void pulseMODE_search_en_la( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mode_import_type( Ccu_reg value );
	void pulseMODE_mode_import_type( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mode_export( Ccu_reg value );
	void pulseMODE_mode_export( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mode_import_play( Ccu_reg value );
	void pulseMODE_mode_import_play( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mode_import_rec( Ccu_reg value );
	void pulseMODE_mode_import_rec( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_measure_en_zoom( Ccu_reg value );
	void pulseMODE_measure_en_zoom( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_measure_en_ch( Ccu_reg value );
	void pulseMODE_measure_en_ch( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_measure_en_la( Ccu_reg value );
	void pulseMODE_measure_en_la( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_wave_ch_en( Ccu_reg value );
	void pulseMODE_wave_ch_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_wave_la_en( Ccu_reg value );
	void pulseMODE_wave_la_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_zoom_en( Ccu_reg value );
	void pulseMODE_zoom_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mask_err_stop_en( Ccu_reg value );
	void pulseMODE_mask_err_stop_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mask_save_fail( Ccu_reg value );
	void pulseMODE_mask_save_fail( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mask_save_pass( Ccu_reg value );
	void pulseMODE_mask_save_pass( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mask_pf_en( Ccu_reg value );
	void pulseMODE_mask_pf_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_zone_trig_en( Ccu_reg value );
	void pulseMODE_zone_trig_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mode_roll_en( Ccu_reg value );
	void pulseMODE_mode_roll_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_mode_fast_ref( Ccu_reg value );
	void pulseMODE_mode_fast_ref( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_auto_trig( Ccu_reg value );
	void pulseMODE_auto_trig( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_interleave_20G( Ccu_reg value );
	void pulseMODE_interleave_20G( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_en_20G( Ccu_reg value );
	void pulseMODE_en_20G( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outMODE( Ccu_reg value );
	void outMODE(  );


	Ccu_reg inMODE(  );

	void setMODE_10G_chn_10G( Ccu_reg value );
	void setMODE_10G_cfg_chn( Ccu_reg value );

	void setMODE_10G( Ccu_reg value );
	void outMODE_10G_chn_10G( Ccu_reg value );
	void pulseMODE_10G_chn_10G( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMODE_10G_cfg_chn( Ccu_reg value );
	void pulseMODE_10G_cfg_chn( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outMODE_10G( Ccu_reg value );
	void outMODE_10G(  );


	Ccu_reg inMODE_10G(  );

	void setPRE_SA( Ccu_reg value );
	void outPRE_SA( Ccu_reg value );
	void outPRE_SA(  );

	Ccu_reg inPRE_SA(  );

	void setPOS_SA( Ccu_reg value );
	void outPOS_SA( Ccu_reg value );
	void outPOS_SA(  );

	Ccu_reg inPOS_SA(  );

	void setSCAN_TRIG_POSITION_position( Ccu_reg value );
	void setSCAN_TRIG_POSITION_trig_left_side( Ccu_reg value );

	void setSCAN_TRIG_POSITION( Ccu_reg value );
	void outSCAN_TRIG_POSITION_position( Ccu_reg value );
	void pulseSCAN_TRIG_POSITION_position( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSCAN_TRIG_POSITION_trig_left_side( Ccu_reg value );
	void pulseSCAN_TRIG_POSITION_trig_left_side( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outSCAN_TRIG_POSITION( Ccu_reg value );
	void outSCAN_TRIG_POSITION(  );


	Ccu_reg inSCAN_TRIG_POSITION(  );

	void setAUTO_TRIG_TIMEOUT( Ccu_reg value );
	void outAUTO_TRIG_TIMEOUT( Ccu_reg value );
	void outAUTO_TRIG_TIMEOUT(  );

	Ccu_reg inAUTO_TRIG_TIMEOUT(  );

	void setAUTO_TRIG_HOLD_OFF( Ccu_reg value );
	void outAUTO_TRIG_HOLD_OFF( Ccu_reg value );
	void outAUTO_TRIG_HOLD_OFF(  );

	Ccu_reg inAUTO_TRIG_HOLD_OFF(  );

	void setFSM_DELAY( Ccu_reg value );
	void outFSM_DELAY( Ccu_reg value );
	void outFSM_DELAY(  );

	Ccu_reg inFSM_DELAY(  );

	void setPLOT_DELAY( Ccu_reg value );
	void outPLOT_DELAY( Ccu_reg value );
	void outPLOT_DELAY(  );

	Ccu_reg inPLOT_DELAY(  );

	void setFRAME_PLOT_NUM( Ccu_reg value );
	void outFRAME_PLOT_NUM( Ccu_reg value );
	void outFRAME_PLOT_NUM(  );

	Ccu_reg inFRAME_PLOT_NUM(  );

	void setREC_PLAY_index_full( Ccu_reg value );
	void setREC_PLAY_loop_playback( Ccu_reg value );
	void setREC_PLAY_index_load( Ccu_reg value );
	void setREC_PLAY_index_inc( Ccu_reg value );
	void setREC_PLAY_mode_play( Ccu_reg value );
	void setREC_PLAY_mode_rec( Ccu_reg value );

	void setREC_PLAY( Ccu_reg value );
	void outREC_PLAY_index_full( Ccu_reg value );
	void pulseREC_PLAY_index_full( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_loop_playback( Ccu_reg value );
	void pulseREC_PLAY_loop_playback( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_index_load( Ccu_reg value );
	void pulseREC_PLAY_index_load( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_index_inc( Ccu_reg value );
	void pulseREC_PLAY_index_inc( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_mode_play( Ccu_reg value );
	void pulseREC_PLAY_mode_play( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_mode_rec( Ccu_reg value );
	void pulseREC_PLAY_mode_rec( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outREC_PLAY( Ccu_reg value );
	void outREC_PLAY(  );


	Ccu_reg inREC_PLAY(  );

	void setWAVE_INFO_wave_plot_frame_cnt( Ccu_reg value );
	void setWAVE_INFO_wave_play_vld( Ccu_reg value );

	void setWAVE_INFO( Ccu_reg value );
	void outWAVE_INFO_wave_plot_frame_cnt( Ccu_reg value );
	void pulseWAVE_INFO_wave_plot_frame_cnt( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outWAVE_INFO_wave_play_vld( Ccu_reg value );
	void pulseWAVE_INFO_wave_play_vld( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outWAVE_INFO( Ccu_reg value );
	void outWAVE_INFO(  );


	Ccu_reg inWAVE_INFO(  );

	void setINDEX_CUR_index( Ccu_reg value );

	void setINDEX_CUR( Ccu_reg value );
	void outINDEX_CUR_index( Ccu_reg value );
	void pulseINDEX_CUR_index( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outINDEX_CUR( Ccu_reg value );
	void outINDEX_CUR(  );


	Ccu_reg inINDEX_CUR(  );

	void setINDEX_BASE_index( Ccu_reg value );

	void setINDEX_BASE( Ccu_reg value );
	void outINDEX_BASE_index( Ccu_reg value );
	void pulseINDEX_BASE_index( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outINDEX_BASE( Ccu_reg value );
	void outINDEX_BASE(  );


	Ccu_reg inINDEX_BASE(  );

	void setINDEX_UPPER_index( Ccu_reg value );

	void setINDEX_UPPER( Ccu_reg value );
	void outINDEX_UPPER_index( Ccu_reg value );
	void pulseINDEX_UPPER_index( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outINDEX_UPPER( Ccu_reg value );
	void outINDEX_UPPER(  );


	Ccu_reg inINDEX_UPPER(  );

	void setTRACE_trace_once_req( Ccu_reg value );
	void setTRACE_measure_once_req( Ccu_reg value );
	void setTRACE_search_once_req( Ccu_reg value );
	void setTRACE_eye_once_req( Ccu_reg value );
	void setTRACE_wave_bypass_wpu( Ccu_reg value );
	void setTRACE_wave_bypass_trace( Ccu_reg value );
	void setTRACE_avg_clr( Ccu_reg value );

	void setTRACE( Ccu_reg value );
	void outTRACE_trace_once_req( Ccu_reg value );
	void pulseTRACE_trace_once_req( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outTRACE_measure_once_req( Ccu_reg value );
	void pulseTRACE_measure_once_req( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outTRACE_search_once_req( Ccu_reg value );
	void pulseTRACE_search_once_req( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outTRACE_eye_once_req( Ccu_reg value );
	void pulseTRACE_eye_once_req( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outTRACE_wave_bypass_wpu( Ccu_reg value );
	void pulseTRACE_wave_bypass_wpu( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outTRACE_wave_bypass_trace( Ccu_reg value );
	void pulseTRACE_wave_bypass_trace( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outTRACE_avg_clr( Ccu_reg value );
	void pulseTRACE_avg_clr( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outTRACE( Ccu_reg value );
	void outTRACE(  );


	Ccu_reg inTRACE(  );

	void setTRIG_DLY( Ccu_reg value );
	void outTRIG_DLY( Ccu_reg value );
	void outTRIG_DLY(  );

	Ccu_reg inTRIG_DLY(  );

	void setPOS_SA_LAST_VIEW( Ccu_reg value );
	void outPOS_SA_LAST_VIEW( Ccu_reg value );
	void outPOS_SA_LAST_VIEW(  );

	Ccu_reg inPOS_SA_LAST_VIEW(  );

	void setMASK_FRM_NUM_L( Ccu_reg value );
	void outMASK_FRM_NUM_L( Ccu_reg value );
	void outMASK_FRM_NUM_L(  );

	Ccu_reg inMASK_FRM_NUM_L(  );

	void setMASK_FRM_NUM_H_frm_num_h( Ccu_reg value );
	void setMASK_FRM_NUM_H_frm_num_en( Ccu_reg value );

	void setMASK_FRM_NUM_H( Ccu_reg value );
	void outMASK_FRM_NUM_H_frm_num_h( Ccu_reg value );
	void pulseMASK_FRM_NUM_H_frm_num_h( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMASK_FRM_NUM_H_frm_num_en( Ccu_reg value );
	void pulseMASK_FRM_NUM_H_frm_num_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outMASK_FRM_NUM_H( Ccu_reg value );
	void outMASK_FRM_NUM_H(  );


	Ccu_reg inMASK_FRM_NUM_H(  );

	void setMASK_TIMEOUT_timeout( Ccu_reg value );
	void setMASK_TIMEOUT_timeout_en( Ccu_reg value );

	void setMASK_TIMEOUT( Ccu_reg value );
	void outMASK_TIMEOUT_timeout( Ccu_reg value );
	void pulseMASK_TIMEOUT_timeout( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outMASK_TIMEOUT_timeout_en( Ccu_reg value );
	void pulseMASK_TIMEOUT_timeout_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outMASK_TIMEOUT( Ccu_reg value );
	void outMASK_TIMEOUT(  );


	Ccu_reg inMASK_TIMEOUT(  );

	void setSTATE_DISPLAY_scu_fsm( Ccu_reg value );
	void setSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg value );
	void setSTATE_DISPLAY_sys_stop_state( Ccu_reg value );
	void setSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg value );
	void setSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg value );
	void setSTATE_DISPLAY_rec_play_stop_state( Ccu_reg value );
	void setSTATE_DISPLAY_mask_stop_state( Ccu_reg value );
	void setSTATE_DISPLAY_stop_stata_clr( Ccu_reg value );
	void setSTATE_DISPLAY_disp_trig_clr( Ccu_reg value );

	void setSTATE_DISPLAY( Ccu_reg value );
	void outSTATE_DISPLAY_scu_fsm( Ccu_reg value );
	void pulseSTATE_DISPLAY_scu_fsm( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg value );
	void pulseSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_sys_stop_state( Ccu_reg value );
	void pulseSTATE_DISPLAY_sys_stop_state( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg value );
	void pulseSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg value );
	void pulseSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_rec_play_stop_state( Ccu_reg value );
	void pulseSTATE_DISPLAY_rec_play_stop_state( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_mask_stop_state( Ccu_reg value );
	void pulseSTATE_DISPLAY_mask_stop_state( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_stop_stata_clr( Ccu_reg value );
	void pulseSTATE_DISPLAY_stop_stata_clr( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_disp_trig_clr( Ccu_reg value );
	void pulseSTATE_DISPLAY_disp_trig_clr( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outSTATE_DISPLAY( Ccu_reg value );
	void outSTATE_DISPLAY(  );


	Ccu_reg inSTATE_DISPLAY(  );

	Ccu_reg inMASK_TIMER(  );

	void setCONFIG_ID( Ccu_reg value );
	void outCONFIG_ID( Ccu_reg value );
	void outCONFIG_ID(  );

	Ccu_reg inCONFIG_ID(  );

	void setDEUBG_wave_sa_ctrl( Ccu_reg value );
	void setDEUBG_sys_auto_trig_act( Ccu_reg value );
	void setDEUBG_sys_auto_trig_en( Ccu_reg value );
	void setDEUBG_sys_fine_trig_req( Ccu_reg value );
	void setDEUBG_sys_meas_only( Ccu_reg value );
	void setDEUBG_sys_meas_en( Ccu_reg value );
	void setDEUBG_sys_meas_type( Ccu_reg value );
	void setDEUBG_spu_tx_roll_null( Ccu_reg value );
	void setDEUBG_measure_once_done_hold( Ccu_reg value );
	void setDEUBG_measure_finish_done_hold( Ccu_reg value );
	void setDEUBG_sys_mask_en( Ccu_reg value );
	void setDEUBG_mask_pf_result_vld_hold( Ccu_reg value );
	void setDEUBG_mask_pf_result_cross_hold( Ccu_reg value );
	void setDEUBG_mask_pf_timeout( Ccu_reg value );
	void setDEUBG_mask_frm_overflow( Ccu_reg value );
	void setDEUBG_measure_once_buf( Ccu_reg value );
	void setDEUBG_zone_result_vld_hold( Ccu_reg value );
	void setDEUBG_zone_result_trig_hold( Ccu_reg value );
	void setDEUBG_wave_proc_la( Ccu_reg value );
	void setDEUBG_sys_zoom( Ccu_reg value );
	void setDEUBG_sys_fast_ref( Ccu_reg value );
	void setDEUBG_spu_tx_done( Ccu_reg value );
	void setDEUBG_sys_avg_en( Ccu_reg value );
	void setDEUBG_sys_trace( Ccu_reg value );
	void setDEUBG_sys_pos_sa_last_frm( Ccu_reg value );
	void setDEUBG_sys_pos_sa_view( Ccu_reg value );
	void setDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg value );
	void setDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg value );
	void setDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg value );

	void setDEUBG( Ccu_reg value );
	void outDEUBG_wave_sa_ctrl( Ccu_reg value );
	void pulseDEUBG_wave_sa_ctrl( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_auto_trig_act( Ccu_reg value );
	void pulseDEUBG_sys_auto_trig_act( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_auto_trig_en( Ccu_reg value );
	void pulseDEUBG_sys_auto_trig_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_fine_trig_req( Ccu_reg value );
	void pulseDEUBG_sys_fine_trig_req( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_meas_only( Ccu_reg value );
	void pulseDEUBG_sys_meas_only( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_meas_en( Ccu_reg value );
	void pulseDEUBG_sys_meas_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_meas_type( Ccu_reg value );
	void pulseDEUBG_sys_meas_type( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_spu_tx_roll_null( Ccu_reg value );
	void pulseDEUBG_spu_tx_roll_null( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_measure_once_done_hold( Ccu_reg value );
	void pulseDEUBG_measure_once_done_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_measure_finish_done_hold( Ccu_reg value );
	void pulseDEUBG_measure_finish_done_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_mask_en( Ccu_reg value );
	void pulseDEUBG_sys_mask_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_pf_result_vld_hold( Ccu_reg value );
	void pulseDEUBG_mask_pf_result_vld_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_pf_result_cross_hold( Ccu_reg value );
	void pulseDEUBG_mask_pf_result_cross_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_pf_timeout( Ccu_reg value );
	void pulseDEUBG_mask_pf_timeout( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_frm_overflow( Ccu_reg value );
	void pulseDEUBG_mask_frm_overflow( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_measure_once_buf( Ccu_reg value );
	void pulseDEUBG_measure_once_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_zone_result_vld_hold( Ccu_reg value );
	void pulseDEUBG_zone_result_vld_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_zone_result_trig_hold( Ccu_reg value );
	void pulseDEUBG_zone_result_trig_hold( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_wave_proc_la( Ccu_reg value );
	void pulseDEUBG_wave_proc_la( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_zoom( Ccu_reg value );
	void pulseDEUBG_sys_zoom( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_fast_ref( Ccu_reg value );
	void pulseDEUBG_sys_fast_ref( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_spu_tx_done( Ccu_reg value );
	void pulseDEUBG_spu_tx_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_avg_en( Ccu_reg value );
	void pulseDEUBG_sys_avg_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_trace( Ccu_reg value );
	void pulseDEUBG_sys_trace( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_pos_sa_last_frm( Ccu_reg value );
	void pulseDEUBG_sys_pos_sa_last_frm( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_pos_sa_view( Ccu_reg value );
	void pulseDEUBG_sys_pos_sa_view( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg value );
	void pulseDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg value );
	void pulseDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg value );
	void pulseDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outDEUBG( Ccu_reg value );
	void outDEUBG(  );


	Ccu_reg inDEUBG(  );

	Ccu_reg inDEBUG_GT(  );

	void setDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg value );

	void setDEBUG_PLOT_DONE( Ccu_reg value );
	void outDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg value );
	void pulseDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outDEBUG_PLOT_DONE( Ccu_reg value );
	void outDEBUG_PLOT_DONE(  );


	Ccu_reg inDEBUG_PLOT_DONE(  );

	void setFPGA_SYNC_sync_cnt( Ccu_reg value );
	void setFPGA_SYNC_debug_scu_reset( Ccu_reg value );
	void setFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg value );

	void setFPGA_SYNC( Ccu_reg value );
	void outFPGA_SYNC_sync_cnt( Ccu_reg value );
	void pulseFPGA_SYNC_sync_cnt( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outFPGA_SYNC_debug_scu_reset( Ccu_reg value );
	void pulseFPGA_SYNC_debug_scu_reset( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg value );
	void pulseFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outFPGA_SYNC( Ccu_reg value );
	void outFPGA_SYNC(  );


	Ccu_reg inFPGA_SYNC(  );

	Ccu_reg inWPU_ID(  );

	void setDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg value );
	void setDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg value );
	void setDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg value );
	void setDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg value );
	void setDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg value );
	void setDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg value );
	void setDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg value );

	void setDEBUG_WPU_PLOT( Ccu_reg value );
	void outDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg value );
	void pulseDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outDEBUG_WPU_PLOT( Ccu_reg value );
	void outDEBUG_WPU_PLOT(  );


	Ccu_reg inDEBUG_WPU_PLOT(  );

	void setDEBUG_GT_CRC_fail_sum( Ccu_reg value );
	void setDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg value );
	void setDEBUG_GT_CRC_crc_valid_i( Ccu_reg value );

	void setDEBUG_GT_CRC( Ccu_reg value );
	void outDEBUG_GT_CRC_fail_sum( Ccu_reg value );
	void pulseDEBUG_GT_CRC_fail_sum( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg value );
	void pulseDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );
	void outDEBUG_GT_CRC_crc_valid_i( Ccu_reg value );
	void pulseDEBUG_GT_CRC_crc_valid_i( Ccu_reg activeV, Ccu_reg idleV=0, int timeus=0 );

	void outDEBUG_GT_CRC( Ccu_reg value );
	void outDEBUG_GT_CRC(  );


	Ccu_reg inDEBUG_GT_CRC(  );

public:
	cache_addr addr_VERSION();
	cache_addr addr_SCU_STATE();
	cache_addr addr_CTRL();
	cache_addr addr_MODE();

	cache_addr addr_MODE_10G();
	cache_addr addr_PRE_SA();
	cache_addr addr_POS_SA();
	cache_addr addr_SCAN_TRIG_POSITION();

	cache_addr addr_AUTO_TRIG_TIMEOUT();
	cache_addr addr_AUTO_TRIG_HOLD_OFF();
	cache_addr addr_FSM_DELAY();
	cache_addr addr_PLOT_DELAY();

	cache_addr addr_FRAME_PLOT_NUM();
	cache_addr addr_REC_PLAY();
	cache_addr addr_WAVE_INFO();
	cache_addr addr_INDEX_CUR();

	cache_addr addr_INDEX_BASE();
	cache_addr addr_INDEX_UPPER();
	cache_addr addr_TRACE();
	cache_addr addr_TRIG_DLY();

	cache_addr addr_POS_SA_LAST_VIEW();
	cache_addr addr_MASK_FRM_NUM_L();
	cache_addr addr_MASK_FRM_NUM_H();
	cache_addr addr_MASK_TIMEOUT();

	cache_addr addr_STATE_DISPLAY();
	cache_addr addr_MASK_TIMER();
	cache_addr addr_CONFIG_ID();
	cache_addr addr_DEUBG();

	cache_addr addr_DEBUG_GT();
	cache_addr addr_DEBUG_PLOT_DONE();
	cache_addr addr_FPGA_SYNC();
	cache_addr addr_WPU_ID();

	cache_addr addr_DEBUG_WPU_PLOT();
	cache_addr addr_DEBUG_GT_CRC();
};
#define _remap_Ccu_()\
	mapIn( &VERSION, 0x1C00 + mAddrBase );\
	mapIn( &SCU_STATE, 0x1C04 + mAddrBase );\
	mapIn( &CTRL, 0x1C08 + mAddrBase );\
	mapIn( &MODE, 0x1C0C + mAddrBase );\
	\
	mapIn( &MODE_10G, 0x1C10 + mAddrBase );\
	mapIn( &PRE_SA, 0x1C14 + mAddrBase );\
	mapIn( &POS_SA, 0x1C18 + mAddrBase );\
	mapIn( &SCAN_TRIG_POSITION, 0x1C1C + mAddrBase );\
	\
	mapIn( &AUTO_TRIG_TIMEOUT, 0x1C20 + mAddrBase );\
	mapIn( &AUTO_TRIG_HOLD_OFF, 0x1C24 + mAddrBase );\
	mapIn( &FSM_DELAY, 0x1C28 + mAddrBase );\
	mapIn( &PLOT_DELAY, 0x1C34 + mAddrBase );\
	\
	mapIn( &FRAME_PLOT_NUM, 0x1C38 + mAddrBase );\
	mapIn( &REC_PLAY, 0x1C3C + mAddrBase );\
	mapIn( &WAVE_INFO, 0x1C40 + mAddrBase );\
	mapIn( &INDEX_CUR, 0x1C44 + mAddrBase );\
	\
	mapIn( &INDEX_BASE, 0x1C48 + mAddrBase );\
	mapIn( &INDEX_UPPER, 0x1C4C + mAddrBase );\
	mapIn( &TRACE, 0x1C50 + mAddrBase );\
	mapIn( &TRIG_DLY, 0x1C54 + mAddrBase );\
	\
	mapIn( &POS_SA_LAST_VIEW, 0x1C5C + mAddrBase );\
	mapIn( &MASK_FRM_NUM_L, 0x1C60 + mAddrBase );\
	mapIn( &MASK_FRM_NUM_H, 0x1C64 + mAddrBase );\
	mapIn( &MASK_TIMEOUT, 0x1C68 + mAddrBase );\
	\
	mapIn( &STATE_DISPLAY, 0x1C6C + mAddrBase );\
	mapIn( &MASK_TIMER, 0x1C70 + mAddrBase );\
	mapIn( &CONFIG_ID, 0x1C74 + mAddrBase );\
	mapIn( &DEUBG, 0x1C80 + mAddrBase );\
	\
	mapIn( &DEBUG_GT, 0x1C84 + mAddrBase );\
	mapIn( &DEBUG_PLOT_DONE, 0x1C88 + mAddrBase );\
	mapIn( &FPGA_SYNC, 0x1C8C + mAddrBase );\
	mapIn( &WPU_ID, 0x1C90 + mAddrBase );\
	\
	mapIn( &DEBUG_WPU_PLOT, 0x1C94 + mAddrBase );\
	mapIn( &DEBUG_GT_CRC, 0x1C98 + mAddrBase );\


#endif
