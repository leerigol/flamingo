#include "frequ.h"
void FrequMemProxy::assignfreq_cfg_1_ref_en( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,ref_en,value);
}
void FrequMemProxy::assignfreq_cfg_1_res1( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,res1,value);
}
void FrequMemProxy::assignfreq_cfg_1_gate_time( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,gate_time,value);
}
void FrequMemProxy::assignfreq_cfg_1_res2( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,res2,value);
}
void FrequMemProxy::assignfreq_cfg_1_sel_mode( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,sel_mode,value);
}
void FrequMemProxy::assignfreq_cfg_1_ctrl_clear( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,ctrl_clear,value);
}
void FrequMemProxy::assignfreq_cfg_1_gate_posi( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,gate_posi,value);
}
void FrequMemProxy::assignfreq_cfg_1_sel_event( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,sel_event,value);
}
void FrequMemProxy::assignfreq_cfg_1_dvm_src( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,dvm_src,value);
}
void FrequMemProxy::assignfreq_cfg_1_dvm_gate( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,dvm_gate,value);
}
void FrequMemProxy::assignfreq_cfg_1_src_sel( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,src_sel,value);
}
void FrequMemProxy::assignfreq_cfg_1_sel_gate( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,sel_gate,value);
}

void FrequMemProxy::assignfreq_cfg_1( Frequ_reg value )
{
	mem_assign_field(freq_cfg_1,payload,value);
}


Frequ_reg FrequMemProxy::getstart_time()
{
	return start_time;
}


Frequ_reg FrequMemProxy::getend_time()
{
	return end_time;
}


Frequ_reg FrequMemProxy::getcounter()
{
	return counter;
}


Frequ_reg FrequMemProxy::getsignal_wide_l()
{
	return signal_wide_l;
}


Frequ_reg FrequMemProxy::getsignal_wide_h()
{
	return signal_wide_h;
}


Frequ_reg FrequMemProxy::getsignal_event_l()
{
	return signal_event_l;
}


Frequ_reg FrequMemProxy::getsignal_event_h()
{
	return signal_event_h;
}


Frequ_reg FrequMemProxy::getdvm_rms()
{
	return dvm_rms;
}


Frequ_reg FrequMemProxy::getdvm_aver()
{
	return dvm_aver;
}


Frequ_reg FrequMemProxy::getdvm_range()
{
	return dvm_range;
}



void FrequMem::_loadOtp()
{
	freq_cfg_1.payload=0x0;
		freq_cfg_1.ref_en=0x0;
		freq_cfg_1.res1=0x0;
		freq_cfg_1.gate_time=0x0;
		freq_cfg_1.res2=0x0;

		freq_cfg_1.sel_mode=0x0;
		freq_cfg_1.ctrl_clear=0x0;
		freq_cfg_1.gate_posi=0x0;
		freq_cfg_1.sel_event=0x0;

		freq_cfg_1.dvm_src=0x0;
		freq_cfg_1.dvm_gate=0x0;
		freq_cfg_1.src_sel=0x0;
		freq_cfg_1.sel_gate=0x0;

	start_time=0x0;
	end_time=0x0;
	counter=0x0;

	signal_wide_l=0x0;
	signal_wide_h=0x0;
	signal_event_l=0x0;
	signal_event_h=0x0;

	dvm_rms=0x0;
	dvm_aver=0x0;
	dvm_range=0x0;
}

void FrequMem::_outOtp()
{
	outfreq_cfg_1();


}
void FrequMem::push( FrequMemProxy *proxy )
{
	if ( freq_cfg_1.payload != proxy->freq_cfg_1.payload )
	{reg_assign_field(freq_cfg_1,payload,proxy->freq_cfg_1.payload);}

}
void FrequMem::pull( FrequMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(start_time);
	reg_in(end_time);
	reg_in(counter);
	reg_in(signal_wide_l);
	reg_in(signal_wide_h);
	reg_in(signal_event_l);
	reg_in(signal_event_h);
	reg_in(dvm_rms);
	reg_in(dvm_aver);
	reg_in(dvm_range);

	*proxy = FrequMemProxy(*this);
}
void FrequMem::setfreq_cfg_1_ref_en( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,ref_en,value);
}
void FrequMem::setfreq_cfg_1_res1( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,res1,value);
}
void FrequMem::setfreq_cfg_1_gate_time( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,gate_time,value);
}
void FrequMem::setfreq_cfg_1_res2( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,res2,value);
}
void FrequMem::setfreq_cfg_1_sel_mode( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,sel_mode,value);
}
void FrequMem::setfreq_cfg_1_ctrl_clear( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,ctrl_clear,value);
}
void FrequMem::setfreq_cfg_1_gate_posi( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,gate_posi,value);
}
void FrequMem::setfreq_cfg_1_sel_event( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,sel_event,value);
}
void FrequMem::setfreq_cfg_1_dvm_src( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,dvm_src,value);
}
void FrequMem::setfreq_cfg_1_dvm_gate( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,dvm_gate,value);
}
void FrequMem::setfreq_cfg_1_src_sel( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,src_sel,value);
}
void FrequMem::setfreq_cfg_1_sel_gate( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,sel_gate,value);
}

void FrequMem::setfreq_cfg_1( Frequ_reg value )
{
	reg_assign_field(freq_cfg_1,payload,value);
}

void FrequMem::outfreq_cfg_1_ref_en( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,ref_en,value);
}
void FrequMem::pulsefreq_cfg_1_ref_en( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,ref_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,ref_en,idleV);
}
void FrequMem::outfreq_cfg_1_res1( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,res1,value);
}
void FrequMem::pulsefreq_cfg_1_res1( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,res1,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,res1,idleV);
}
void FrequMem::outfreq_cfg_1_gate_time( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,gate_time,value);
}
void FrequMem::pulsefreq_cfg_1_gate_time( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,gate_time,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,gate_time,idleV);
}
void FrequMem::outfreq_cfg_1_res2( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,res2,value);
}
void FrequMem::pulsefreq_cfg_1_res2( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,res2,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,res2,idleV);
}
void FrequMem::outfreq_cfg_1_sel_mode( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,sel_mode,value);
}
void FrequMem::pulsefreq_cfg_1_sel_mode( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,sel_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,sel_mode,idleV);
}
void FrequMem::outfreq_cfg_1_ctrl_clear( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,ctrl_clear,value);
}
void FrequMem::pulsefreq_cfg_1_ctrl_clear( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,ctrl_clear,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,ctrl_clear,idleV);
}
void FrequMem::outfreq_cfg_1_gate_posi( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,gate_posi,value);
}
void FrequMem::pulsefreq_cfg_1_gate_posi( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,gate_posi,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,gate_posi,idleV);
}
void FrequMem::outfreq_cfg_1_sel_event( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,sel_event,value);
}
void FrequMem::pulsefreq_cfg_1_sel_event( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,sel_event,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,sel_event,idleV);
}
void FrequMem::outfreq_cfg_1_dvm_src( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,dvm_src,value);
}
void FrequMem::pulsefreq_cfg_1_dvm_src( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,dvm_src,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,dvm_src,idleV);
}
void FrequMem::outfreq_cfg_1_dvm_gate( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,dvm_gate,value);
}
void FrequMem::pulsefreq_cfg_1_dvm_gate( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,dvm_gate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,dvm_gate,idleV);
}
void FrequMem::outfreq_cfg_1_src_sel( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,src_sel,value);
}
void FrequMem::pulsefreq_cfg_1_src_sel( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,src_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,src_sel,idleV);
}
void FrequMem::outfreq_cfg_1_sel_gate( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,sel_gate,value);
}
void FrequMem::pulsefreq_cfg_1_sel_gate( Frequ_reg activeV, Frequ_reg idleV, int timeus )
{
	out_assign_field(freq_cfg_1,sel_gate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(freq_cfg_1,sel_gate,idleV);
}

void FrequMem::outfreq_cfg_1( Frequ_reg value )
{
	out_assign_field(freq_cfg_1,payload,value);
}

void FrequMem::outfreq_cfg_1()
{
	out_member(freq_cfg_1);
}


Frequ_reg FrequMem::instart_time()
{
	reg_in(start_time);
	return start_time;
}


Frequ_reg FrequMem::inend_time()
{
	reg_in(end_time);
	return end_time;
}


Frequ_reg FrequMem::incounter()
{
	reg_in(counter);
	return counter;
}


Frequ_reg FrequMem::insignal_wide_l()
{
	reg_in(signal_wide_l);
	return signal_wide_l;
}


Frequ_reg FrequMem::insignal_wide_h()
{
	reg_in(signal_wide_h);
	return signal_wide_h;
}


Frequ_reg FrequMem::insignal_event_l()
{
	reg_in(signal_event_l);
	return signal_event_l;
}


Frequ_reg FrequMem::insignal_event_h()
{
	reg_in(signal_event_h);
	return signal_event_h;
}


Frequ_reg FrequMem::indvm_rms()
{
	reg_in(dvm_rms);
	return dvm_rms;
}


Frequ_reg FrequMem::indvm_aver()
{
	reg_in(dvm_aver);
	return dvm_aver;
}


Frequ_reg FrequMem::indvm_range()
{
	reg_in(dvm_range);
	return dvm_range;
}


cache_addr FrequMem::addr_freq_cfg_1(){ return 0x1a00+mAddrBase; }
cache_addr FrequMem::addr_start_time(){ return 0x1900+mAddrBase; }
cache_addr FrequMem::addr_end_time(){ return 0x1904+mAddrBase; }
cache_addr FrequMem::addr_counter(){ return 0x1908+mAddrBase; }

cache_addr FrequMem::addr_signal_wide_l(){ return 0x190c+mAddrBase; }
cache_addr FrequMem::addr_signal_wide_h(){ return 0x1910+mAddrBase; }
cache_addr FrequMem::addr_signal_event_l(){ return 0x1914+mAddrBase; }
cache_addr FrequMem::addr_signal_event_h(){ return 0x1918+mAddrBase; }

cache_addr FrequMem::addr_dvm_rms(){ return 0x191C+mAddrBase; }
cache_addr FrequMem::addr_dvm_aver(){ return 0x1920+mAddrBase; }
cache_addr FrequMem::addr_dvm_range(){ return 0x1924+mAddrBase; }


