#include "gtu.h"
Gtu_reg GtuMemProxy::getVERSION()
{
	return VERSION;
}


void GtuMemProxy::assignSoft_reset( Gtu_reg value )
{
	mem_assign(Soft_reset,value);
}


void GtuMemProxy::assignTEST( Gtu_reg value )
{
	mem_assign(TEST,value);
}


Gtu_reg GtuMemProxy::getTEST()
{
	return TEST;
}


Gtu_reg GtuMemProxy::getgt_channel_up()
{
	return gt_channel_up;
}


Gtu_reg GtuMemProxy::getself_gtu_state_pdu_full()
{
	return self_gtu_state.pdu_full;
}
Gtu_reg GtuMemProxy::getself_gtu_state_ufc_full()
{
	return self_gtu_state.ufc_full;
}

Gtu_reg GtuMemProxy::getself_gtu_state()
{
	return self_gtu_state.payload;
}


Gtu_reg GtuMemProxy::getreceiver_state_pdu_full()
{
	return receiver_state.pdu_full;
}
Gtu_reg GtuMemProxy::getreceiver_state_ufc_full()
{
	return receiver_state.ufc_full;
}

Gtu_reg GtuMemProxy::getreceiver_state()
{
	return receiver_state.payload;
}


void GtuMemProxy::assigngt_buf_clr_pdu_rx_buf_clr( Gtu_reg value )
{
	mem_assign_field(gt_buf_clr,pdu_rx_buf_clr,value);
}
void GtuMemProxy::assigngt_buf_clr_pdu_tx_buf_clr( Gtu_reg value )
{
	mem_assign_field(gt_buf_clr,pdu_tx_buf_clr,value);
}
void GtuMemProxy::assigngt_buf_clr_ufc_rx_buf_clr( Gtu_reg value )
{
	mem_assign_field(gt_buf_clr,ufc_rx_buf_clr,value);
}
void GtuMemProxy::assigngt_buf_clr_ufc_tx_buf_clr( Gtu_reg value )
{
	mem_assign_field(gt_buf_clr,ufc_tx_buf_clr,value);
}

void GtuMemProxy::assigngt_buf_clr( Gtu_reg value )
{
	mem_assign_field(gt_buf_clr,payload,value);
}



void GtuMem::_loadOtp()
{
	VERSION=0x0;
	Soft_reset=0x0;
	TEST=0x0;
	gt_channel_up=0x0;

	self_gtu_state.payload=0x0;
		self_gtu_state.pdu_full=0x0;
		self_gtu_state.ufc_full=0x0;

	receiver_state.payload=0x0;
		receiver_state.pdu_full=0x0;
		receiver_state.ufc_full=0x0;

	gt_buf_clr.payload=0x0;
		gt_buf_clr.pdu_rx_buf_clr=0x0;
		gt_buf_clr.pdu_tx_buf_clr=0x0;
		gt_buf_clr.ufc_rx_buf_clr=0x0;
		gt_buf_clr.ufc_tx_buf_clr=0x0;

}

void GtuMem::_outOtp()
{
	outSoft_reset();
	outTEST();

	outgt_buf_clr();
}
void GtuMem::push( GtuMemProxy *proxy )
{
	if ( Soft_reset != proxy->Soft_reset )
	{reg_assign(Soft_reset,proxy->Soft_reset);}

	if ( TEST != proxy->TEST )
	{reg_assign(TEST,proxy->TEST);}

	if ( gt_buf_clr.payload != proxy->gt_buf_clr.payload )
	{reg_assign_field(gt_buf_clr,payload,proxy->gt_buf_clr.payload);}

}
void GtuMem::pull( GtuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(VERSION);
	reg_in(TEST);
	reg_in(gt_channel_up);
	reg_in(self_gtu_state);
	reg_in(receiver_state);

	*proxy = GtuMemProxy(*this);
}
Gtu_reg GtuMem::inVERSION()
{
	reg_in(VERSION);
	return VERSION;
}


void GtuMem::setSoft_reset( Gtu_reg value )
{
	reg_assign(Soft_reset,value);
}

void GtuMem::outSoft_reset( Gtu_reg value )
{
	out_assign(Soft_reset,value);
}

void GtuMem::outSoft_reset()
{
	out_member(Soft_reset);
}


void GtuMem::setTEST( Gtu_reg value )
{
	reg_assign(TEST,value);
}

void GtuMem::outTEST( Gtu_reg value )
{
	out_assign(TEST,value);
}

void GtuMem::outTEST()
{
	out_member(TEST);
}


Gtu_reg GtuMem::inTEST()
{
	reg_in(TEST);
	return TEST;
}


Gtu_reg GtuMem::ingt_channel_up()
{
	reg_in(gt_channel_up);
	return gt_channel_up;
}


Gtu_reg GtuMem::inself_gtu_state()
{
	reg_in(self_gtu_state);
	return self_gtu_state.payload;
}


Gtu_reg GtuMem::inreceiver_state()
{
	reg_in(receiver_state);
	return receiver_state.payload;
}


void GtuMem::setgt_buf_clr_pdu_rx_buf_clr( Gtu_reg value )
{
	reg_assign_field(gt_buf_clr,pdu_rx_buf_clr,value);
}
void GtuMem::setgt_buf_clr_pdu_tx_buf_clr( Gtu_reg value )
{
	reg_assign_field(gt_buf_clr,pdu_tx_buf_clr,value);
}
void GtuMem::setgt_buf_clr_ufc_rx_buf_clr( Gtu_reg value )
{
	reg_assign_field(gt_buf_clr,ufc_rx_buf_clr,value);
}
void GtuMem::setgt_buf_clr_ufc_tx_buf_clr( Gtu_reg value )
{
	reg_assign_field(gt_buf_clr,ufc_tx_buf_clr,value);
}

void GtuMem::setgt_buf_clr( Gtu_reg value )
{
	reg_assign_field(gt_buf_clr,payload,value);
}

void GtuMem::outgt_buf_clr_pdu_rx_buf_clr( Gtu_reg value )
{
	out_assign_field(gt_buf_clr,pdu_rx_buf_clr,value);
}
void GtuMem::pulsegt_buf_clr_pdu_rx_buf_clr( Gtu_reg activeV, Gtu_reg idleV, int timeus )
{
	out_assign_field(gt_buf_clr,pdu_rx_buf_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(gt_buf_clr,pdu_rx_buf_clr,idleV);
}
void GtuMem::outgt_buf_clr_pdu_tx_buf_clr( Gtu_reg value )
{
	out_assign_field(gt_buf_clr,pdu_tx_buf_clr,value);
}
void GtuMem::pulsegt_buf_clr_pdu_tx_buf_clr( Gtu_reg activeV, Gtu_reg idleV, int timeus )
{
	out_assign_field(gt_buf_clr,pdu_tx_buf_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(gt_buf_clr,pdu_tx_buf_clr,idleV);
}
void GtuMem::outgt_buf_clr_ufc_rx_buf_clr( Gtu_reg value )
{
	out_assign_field(gt_buf_clr,ufc_rx_buf_clr,value);
}
void GtuMem::pulsegt_buf_clr_ufc_rx_buf_clr( Gtu_reg activeV, Gtu_reg idleV, int timeus )
{
	out_assign_field(gt_buf_clr,ufc_rx_buf_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(gt_buf_clr,ufc_rx_buf_clr,idleV);
}
void GtuMem::outgt_buf_clr_ufc_tx_buf_clr( Gtu_reg value )
{
	out_assign_field(gt_buf_clr,ufc_tx_buf_clr,value);
}
void GtuMem::pulsegt_buf_clr_ufc_tx_buf_clr( Gtu_reg activeV, Gtu_reg idleV, int timeus )
{
	out_assign_field(gt_buf_clr,ufc_tx_buf_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(gt_buf_clr,ufc_tx_buf_clr,idleV);
}

void GtuMem::outgt_buf_clr( Gtu_reg value )
{
	out_assign_field(gt_buf_clr,payload,value);
}

void GtuMem::outgt_buf_clr()
{
	out_member(gt_buf_clr);
}


cache_addr GtuMem::addr_VERSION(){ return 0x0000+mAddrBase; }
cache_addr GtuMem::addr_Soft_reset(){ return 0x0000+mAddrBase; }
cache_addr GtuMem::addr_TEST(){ return 0x0004+mAddrBase; }
cache_addr GtuMem::addr_gt_channel_up(){ return 0x0008+mAddrBase; }

cache_addr GtuMem::addr_self_gtu_state(){ return 0x000C+mAddrBase; }
cache_addr GtuMem::addr_receiver_state(){ return 0x0010+mAddrBase; }
cache_addr GtuMem::addr_gt_buf_clr(){ return 0x0014+mAddrBase; }


