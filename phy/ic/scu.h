#ifndef _SCU_IC_H_
#define _SCU_IC_H_

#define Scu_reg unsigned int

union Scu_SCU_STATE{
	struct{
	Scu_reg scu_fsm:4;
	Scu_reg scu_sa_fsm:2;
	Scu_reg sys_stop:1;
	Scu_reg scu_stop:1;

	Scu_reg scu_loop:1;
	Scu_reg sys_force_stop:1;
	Scu_reg process_sa:1;
	Scu_reg process_sa_done:1;

	Scu_reg sys_sa_en:1;
	Scu_reg sys_pre_sa_done:1;
	Scu_reg sys_pos_sa_done:1;
	Scu_reg spu_wave_mem_wr_done_hold:1;

	Scu_reg sys_trig_d:1;
	Scu_reg tpu_scu_trig:1;
	Scu_reg fine_trig_done_buf:1;
	Scu_reg avg_proc_done:1;

	Scu_reg sys_avg_proc:1;
	Scu_reg sys_avg_exp:1;
	Scu_reg sys_wave_rd_en:1;
	Scu_reg sys_wave_proc_en:1;

	Scu_reg wave_mem_rd_done_hold:1;
	Scu_reg spu_tx_done_hold:1;
	Scu_reg gt_wave_tx_done:1;
	Scu_reg spu_wav_rd_meas_once_tx_done:1;

	Scu_reg task_wave_tx_done_LA:1;
	Scu_reg task_wave_tx_done_CH:1;
	Scu_reg spu_proc_done:1;
	Scu_reg scan_roll_sa_done_mem_tx_null:1;
	};
	Scu_reg payload;
};

union Scu_CTRL{
	struct{
	Scu_reg sys_stop_state:1;
	Scu_reg cfg_fsm_run_stop_n:1;
	Scu_reg run_single:1;
	Scu_reg mode_play_last:1;

	Scu_reg scu_fsm_run:1;
	Scu_reg force_trig:1;
	Scu_reg wpu_plot_display_finish:1;
	Scu_reg _pad0:22;
	Scu_reg soft_reset_gt:1;

	Scu_reg ms_sync:1;
	Scu_reg fsm_reset:1;
	};
	Scu_reg payload;
};

union Scu_MODE{
	struct{
	Scu_reg cfg_mode_scan:1;
	Scu_reg sys_mode_scan_trace:1;
	Scu_reg sys_mode_scan_zoom:1;
	Scu_reg mode_scan_en:1;

	Scu_reg search_en_zoom:1;
	Scu_reg search_en_ch:1;
	Scu_reg search_en_la:1;
	Scu_reg mode_import_type:1;

	Scu_reg mode_export:1;
	Scu_reg mode_import_play:1;
	Scu_reg mode_import_rec:1;
	Scu_reg measure_en_zoom:1;

	Scu_reg measure_en_ch:1;
	Scu_reg measure_en_la:1;
	Scu_reg wave_ch_en:1;
	Scu_reg wave_la_en:1;

	Scu_reg zoom_en:1;
	Scu_reg mask_err_stop_en:1;
	Scu_reg mask_save_fail:1;
	Scu_reg mask_save_pass:1;

	Scu_reg mask_pf_en:1;
	Scu_reg zone_trig_en:1;
	Scu_reg mode_roll_en:1;
	Scu_reg mode_fast_ref:1;

	Scu_reg auto_trig:1;
	Scu_reg interleave_20G:4;
	Scu_reg _pad0:2;
	Scu_reg en_20G:1;
	};
	Scu_reg payload;
};

union Scu_MODE_10G{
	struct{
	Scu_reg chn_10G:4;
	Scu_reg cfg_chn:4;
	};
	Scu_reg payload;
};

union Scu_SCAN_TRIG_POSITION{
	struct{
	Scu_reg position:31;
	Scu_reg trig_left_side:1;
	};
	Scu_reg payload;
};

union Scu_REC_PLAY{
	struct{
	Scu_reg _pad0:26;
	Scu_reg index_full:1;
	Scu_reg loop_playback:1;
	Scu_reg index_load:1;
	Scu_reg index_inc:1;

	Scu_reg mode_play:1;
	Scu_reg mode_rec:1;
	};
	Scu_reg payload;
};

union Scu_WAVE_INFO{
	struct{
	Scu_reg wave_plot_frame_cnt:16;
	Scu_reg _pad0:15;
	Scu_reg wave_play_vld:1;
	};
	Scu_reg payload;
};

union Scu_INDEX_CUR{
	struct{
	Scu_reg index:20;
	};
	Scu_reg payload;
};

union Scu_INDEX_BASE{
	struct{
	Scu_reg index:20;
	};
	Scu_reg payload;
};

union Scu_INDEX_UPPER{
	struct{
	Scu_reg index:20;
	};
	Scu_reg payload;
};

union Scu_TRACE{
	struct{
	Scu_reg trace_once_req:1;
	Scu_reg measure_once_req:1;
	Scu_reg search_once_req:1;
	Scu_reg eye_once_req:1;

	Scu_reg wave_bypass_wpu:1;
	Scu_reg wave_bypass_trace:1;
	Scu_reg _pad0:25;
	Scu_reg avg_clr:1;
	};
	Scu_reg payload;
};

union Scu_MASK_FRM_NUM_H{
	struct{
	Scu_reg frm_num_h:16;
	Scu_reg _pad0:15;
	Scu_reg frm_num_en:1;
	};
	Scu_reg payload;
};

union Scu_MASK_TIMEOUT{
	struct{
	Scu_reg timeout:24;
	Scu_reg _pad0:7;
	Scu_reg timeout_en:1;
	};
	Scu_reg payload;
};

union Scu_STATE_DISPLAY{
	struct{
	Scu_reg scu_fsm:4;
	Scu_reg sys_pre_sa_done:1;
	Scu_reg sys_stop_state:1;
	Scu_reg disp_tpu_scu_trig:1;

	Scu_reg disp_sys_trig_d:1;
	Scu_reg rec_play_stop_state:1;
	Scu_reg mask_stop_state:1;
	Scu_reg _pad0:20;
	Scu_reg stop_stata_clr:1;

	Scu_reg disp_trig_clr:1;
	};
	Scu_reg payload;
};

union Scu_DEUBG{
	struct{
	Scu_reg wave_sa_ctrl:1;
	Scu_reg sys_auto_trig_act:1;
	Scu_reg sys_auto_trig_en:1;
	Scu_reg sys_fine_trig_req:1;

	Scu_reg sys_meas_only:1;
	Scu_reg sys_meas_en:1;
	Scu_reg sys_meas_type:2;
	Scu_reg spu_tx_roll_null:1;

	Scu_reg measure_once_done_hold:1;
	Scu_reg measure_finish_done_hold:1;
	Scu_reg _pad0:1;
	Scu_reg sys_mask_en:1;
	Scu_reg mask_pf_result_vld_hold:1;

	Scu_reg mask_pf_result_cross_hold:1;
	Scu_reg mask_pf_timeout:1;
	Scu_reg mask_frm_overflow:1;
	Scu_reg measure_once_buf:1;

	Scu_reg zone_result_vld_hold:1;
	Scu_reg zone_result_trig_hold:1;
	Scu_reg wave_proc_la:1;
	Scu_reg sys_zoom:1;

	Scu_reg sys_fast_ref:1;
	Scu_reg spu_tx_done:1;
	Scu_reg sys_avg_en:1;
	Scu_reg _pad1:1;
	Scu_reg sys_trace:1;

	Scu_reg sys_pos_sa_last_frm:1;
	Scu_reg sys_pos_sa_view:1;
	Scu_reg spu_wav_sa_rd_tx_done:1;
	Scu_reg scan_roll_pos_trig_frm_buf:1;

	Scu_reg scan_roll_pos_last_frm_buf:1;
	};
	Scu_reg payload;
};

union Scu_DEBUG_GT{
	struct{
	Scu_reg receiver_gtu_state:32;
	};
	Scu_reg payload;
};

union Scu_DEBUG_PLOT_DONE{
	struct{
	Scu_reg wpu_plot_done_dly:32;
	};
	Scu_reg payload;
};

union Scu_FPGA_SYNC{
	struct{
	Scu_reg sync_cnt:8;
	Scu_reg debug_scu_reset:1;
	Scu_reg _pad0:15;
	Scu_reg debug_sys_rst_cnt:8;
	};
	Scu_reg payload;
};

union Scu_WPU_ID{
	struct{
	Scu_reg sys_wpu_plot_id:4;
	Scu_reg _pad0:4;
	Scu_reg wpu_plot_done_id:4;
	Scu_reg wpu_display_done_id:4;
	};
	Scu_reg payload;
};

union Scu_DEBUG_WPU_PLOT{
	struct{
	Scu_reg wpu_plot_display_finish:1;
	Scu_reg wpu_fast_done_buf:1;
	Scu_reg wpu_display_done_buf:1;
	Scu_reg wpu_plot_display_id_equ:1;

	Scu_reg scu_wpu_plot_last:1;
	Scu_reg scu_wave_plot_req:1;
	Scu_reg scu_wpu_start:1;
	Scu_reg sys_wpu_plot:1;

	Scu_reg wpu_plot_done_buf:1;
	Scu_reg scu_wpu_plot_en:1;
	Scu_reg wpu_plot_scan_finish_buf:1;
	Scu_reg wpu_plot_scan_zoom_finish_buf:1;

	Scu_reg scu_wpu_stop_plot_en:1;
	Scu_reg scu_wpu_stop_plot_vld:1;
	Scu_reg _pad0:16;
	Scu_reg cfg_wpu_flush:1;
	Scu_reg cfg_stop_plot:1;
	};
	Scu_reg payload;
};

union Scu_DEBUG_GT_CRC{
	struct{
	Scu_reg fail_sum:28;
	Scu_reg _pad0:2;
	Scu_reg crc_pass_fail_n_i:1;
	Scu_reg crc_valid_i:1;
	};
	Scu_reg payload;
};

struct ScuMemProxy {
	Scu_reg VERSION;
	Scu_SCU_STATE SCU_STATE; 
	Scu_CTRL CTRL; 
	Scu_MODE MODE; 

	Scu_MODE_10G MODE_10G; 
	Scu_reg PRE_SA;
	Scu_reg POS_SA;
	Scu_SCAN_TRIG_POSITION SCAN_TRIG_POSITION; 

	Scu_reg AUTO_TRIG_TIMEOUT;
	Scu_reg AUTO_TRIG_HOLD_OFF;
	Scu_reg FSM_DELAY;
	Scu_reg PLOT_DELAY;

	Scu_reg FRAME_PLOT_NUM;
	Scu_REC_PLAY REC_PLAY; 
	Scu_WAVE_INFO WAVE_INFO; 
	Scu_INDEX_CUR INDEX_CUR; 

	Scu_INDEX_BASE INDEX_BASE; 
	Scu_INDEX_UPPER INDEX_UPPER; 
	Scu_TRACE TRACE; 
	Scu_reg TRIG_DLY;

	Scu_reg POS_SA_LAST_VIEW;
	Scu_reg MASK_FRM_NUM_L;
	Scu_MASK_FRM_NUM_H MASK_FRM_NUM_H; 
	Scu_MASK_TIMEOUT MASK_TIMEOUT; 

	Scu_STATE_DISPLAY STATE_DISPLAY; 
	Scu_reg MASK_TIMER;
	Scu_reg CONFIG_ID;
	Scu_DEUBG DEUBG; 

	Scu_DEBUG_GT DEBUG_GT; 
	Scu_DEBUG_PLOT_DONE DEBUG_PLOT_DONE; 
	Scu_FPGA_SYNC FPGA_SYNC; 
	Scu_WPU_ID WPU_ID; 

	Scu_DEBUG_WPU_PLOT DEBUG_WPU_PLOT; 
	Scu_DEBUG_GT_CRC DEBUG_GT_CRC; 
	Scu_reg getVERSION(  );

	void assignSCU_STATE_scu_fsm( Scu_reg value  );
	void assignSCU_STATE_scu_sa_fsm( Scu_reg value  );
	void assignSCU_STATE_sys_stop( Scu_reg value  );
	void assignSCU_STATE_scu_stop( Scu_reg value  );
	void assignSCU_STATE_scu_loop( Scu_reg value  );
	void assignSCU_STATE_sys_force_stop( Scu_reg value  );
	void assignSCU_STATE_process_sa( Scu_reg value  );
	void assignSCU_STATE_process_sa_done( Scu_reg value  );
	void assignSCU_STATE_sys_sa_en( Scu_reg value  );
	void assignSCU_STATE_sys_pre_sa_done( Scu_reg value  );
	void assignSCU_STATE_sys_pos_sa_done( Scu_reg value  );
	void assignSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value  );
	void assignSCU_STATE_sys_trig_d( Scu_reg value  );
	void assignSCU_STATE_tpu_scu_trig( Scu_reg value  );
	void assignSCU_STATE_fine_trig_done_buf( Scu_reg value  );
	void assignSCU_STATE_avg_proc_done( Scu_reg value  );
	void assignSCU_STATE_sys_avg_proc( Scu_reg value  );
	void assignSCU_STATE_sys_avg_exp( Scu_reg value  );
	void assignSCU_STATE_sys_wave_rd_en( Scu_reg value  );
	void assignSCU_STATE_sys_wave_proc_en( Scu_reg value  );
	void assignSCU_STATE_wave_mem_rd_done_hold( Scu_reg value  );
	void assignSCU_STATE_spu_tx_done_hold( Scu_reg value  );
	void assignSCU_STATE_gt_wave_tx_done( Scu_reg value  );
	void assignSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value  );
	void assignSCU_STATE_task_wave_tx_done_LA( Scu_reg value  );
	void assignSCU_STATE_task_wave_tx_done_CH( Scu_reg value  );
	void assignSCU_STATE_spu_proc_done( Scu_reg value  );
	void assignSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value  );

	void assignSCU_STATE( Scu_reg value );


	Scu_reg getSCU_STATE_scu_fsm(  );
	Scu_reg getSCU_STATE_scu_sa_fsm(  );
	Scu_reg getSCU_STATE_sys_stop(  );
	Scu_reg getSCU_STATE_scu_stop(  );
	Scu_reg getSCU_STATE_scu_loop(  );
	Scu_reg getSCU_STATE_sys_force_stop(  );
	Scu_reg getSCU_STATE_process_sa(  );
	Scu_reg getSCU_STATE_process_sa_done(  );
	Scu_reg getSCU_STATE_sys_sa_en(  );
	Scu_reg getSCU_STATE_sys_pre_sa_done(  );
	Scu_reg getSCU_STATE_sys_pos_sa_done(  );
	Scu_reg getSCU_STATE_spu_wave_mem_wr_done_hold(  );
	Scu_reg getSCU_STATE_sys_trig_d(  );
	Scu_reg getSCU_STATE_tpu_scu_trig(  );
	Scu_reg getSCU_STATE_fine_trig_done_buf(  );
	Scu_reg getSCU_STATE_avg_proc_done(  );
	Scu_reg getSCU_STATE_sys_avg_proc(  );
	Scu_reg getSCU_STATE_sys_avg_exp(  );
	Scu_reg getSCU_STATE_sys_wave_rd_en(  );
	Scu_reg getSCU_STATE_sys_wave_proc_en(  );
	Scu_reg getSCU_STATE_wave_mem_rd_done_hold(  );
	Scu_reg getSCU_STATE_spu_tx_done_hold(  );
	Scu_reg getSCU_STATE_gt_wave_tx_done(  );
	Scu_reg getSCU_STATE_spu_wav_rd_meas_once_tx_done(  );
	Scu_reg getSCU_STATE_task_wave_tx_done_LA(  );
	Scu_reg getSCU_STATE_task_wave_tx_done_CH(  );
	Scu_reg getSCU_STATE_spu_proc_done(  );
	Scu_reg getSCU_STATE_scan_roll_sa_done_mem_tx_null(  );

	Scu_reg getSCU_STATE(  );

	void assignCTRL_sys_stop_state( Scu_reg value  );
	void assignCTRL_cfg_fsm_run_stop_n( Scu_reg value  );
	void assignCTRL_run_single( Scu_reg value  );
	void assignCTRL_mode_play_last( Scu_reg value  );
	void assignCTRL_scu_fsm_run( Scu_reg value  );
	void assignCTRL_force_trig( Scu_reg value  );
	void assignCTRL_wpu_plot_display_finish( Scu_reg value  );
	void assignCTRL_soft_reset_gt( Scu_reg value  );
	void assignCTRL_ms_sync( Scu_reg value  );
	void assignCTRL_fsm_reset( Scu_reg value  );

	void assignCTRL( Scu_reg value );


	Scu_reg getCTRL_sys_stop_state(  );
	Scu_reg getCTRL_cfg_fsm_run_stop_n(  );
	Scu_reg getCTRL_run_single(  );
	Scu_reg getCTRL_mode_play_last(  );
	Scu_reg getCTRL_scu_fsm_run(  );
	Scu_reg getCTRL_force_trig(  );
	Scu_reg getCTRL_wpu_plot_display_finish(  );
	Scu_reg getCTRL_soft_reset_gt(  );
	Scu_reg getCTRL_ms_sync(  );
	Scu_reg getCTRL_fsm_reset(  );

	Scu_reg getCTRL(  );

	void assignMODE_cfg_mode_scan( Scu_reg value  );
	void assignMODE_sys_mode_scan_trace( Scu_reg value  );
	void assignMODE_sys_mode_scan_zoom( Scu_reg value  );
	void assignMODE_mode_scan_en( Scu_reg value  );
	void assignMODE_search_en_zoom( Scu_reg value  );
	void assignMODE_search_en_ch( Scu_reg value  );
	void assignMODE_search_en_la( Scu_reg value  );
	void assignMODE_mode_import_type( Scu_reg value  );
	void assignMODE_mode_export( Scu_reg value  );
	void assignMODE_mode_import_play( Scu_reg value  );
	void assignMODE_mode_import_rec( Scu_reg value  );
	void assignMODE_measure_en_zoom( Scu_reg value  );
	void assignMODE_measure_en_ch( Scu_reg value  );
	void assignMODE_measure_en_la( Scu_reg value  );
	void assignMODE_wave_ch_en( Scu_reg value  );
	void assignMODE_wave_la_en( Scu_reg value  );
	void assignMODE_zoom_en( Scu_reg value  );
	void assignMODE_mask_err_stop_en( Scu_reg value  );
	void assignMODE_mask_save_fail( Scu_reg value  );
	void assignMODE_mask_save_pass( Scu_reg value  );
	void assignMODE_mask_pf_en( Scu_reg value  );
	void assignMODE_zone_trig_en( Scu_reg value  );
	void assignMODE_mode_roll_en( Scu_reg value  );
	void assignMODE_mode_fast_ref( Scu_reg value  );
	void assignMODE_auto_trig( Scu_reg value  );
	void assignMODE_interleave_20G( Scu_reg value  );
	void assignMODE_en_20G( Scu_reg value  );

	void assignMODE( Scu_reg value );


	Scu_reg getMODE_cfg_mode_scan(  );
	Scu_reg getMODE_sys_mode_scan_trace(  );
	Scu_reg getMODE_sys_mode_scan_zoom(  );
	Scu_reg getMODE_mode_scan_en(  );
	Scu_reg getMODE_search_en_zoom(  );
	Scu_reg getMODE_search_en_ch(  );
	Scu_reg getMODE_search_en_la(  );
	Scu_reg getMODE_mode_import_type(  );
	Scu_reg getMODE_mode_export(  );
	Scu_reg getMODE_mode_import_play(  );
	Scu_reg getMODE_mode_import_rec(  );
	Scu_reg getMODE_measure_en_zoom(  );
	Scu_reg getMODE_measure_en_ch(  );
	Scu_reg getMODE_measure_en_la(  );
	Scu_reg getMODE_wave_ch_en(  );
	Scu_reg getMODE_wave_la_en(  );
	Scu_reg getMODE_zoom_en(  );
	Scu_reg getMODE_mask_err_stop_en(  );
	Scu_reg getMODE_mask_save_fail(  );
	Scu_reg getMODE_mask_save_pass(  );
	Scu_reg getMODE_mask_pf_en(  );
	Scu_reg getMODE_zone_trig_en(  );
	Scu_reg getMODE_mode_roll_en(  );
	Scu_reg getMODE_mode_fast_ref(  );
	Scu_reg getMODE_auto_trig(  );
	Scu_reg getMODE_interleave_20G(  );
	Scu_reg getMODE_en_20G(  );

	Scu_reg getMODE(  );

	void assignMODE_10G_chn_10G( Scu_reg value  );
	void assignMODE_10G_cfg_chn( Scu_reg value  );

	void assignMODE_10G( Scu_reg value );


	Scu_reg getMODE_10G_chn_10G(  );
	Scu_reg getMODE_10G_cfg_chn(  );

	Scu_reg getMODE_10G(  );

	void assignPRE_SA( Scu_reg value );

	Scu_reg getPRE_SA(  );

	void assignPOS_SA( Scu_reg value );

	Scu_reg getPOS_SA(  );

	void assignSCAN_TRIG_POSITION_position( Scu_reg value  );
	void assignSCAN_TRIG_POSITION_trig_left_side( Scu_reg value  );

	void assignSCAN_TRIG_POSITION( Scu_reg value );


	Scu_reg getSCAN_TRIG_POSITION_position(  );
	Scu_reg getSCAN_TRIG_POSITION_trig_left_side(  );

	Scu_reg getSCAN_TRIG_POSITION(  );

	void assignAUTO_TRIG_TIMEOUT( Scu_reg value );

	Scu_reg getAUTO_TRIG_TIMEOUT(  );

	void assignAUTO_TRIG_HOLD_OFF( Scu_reg value );

	Scu_reg getAUTO_TRIG_HOLD_OFF(  );

	void assignFSM_DELAY( Scu_reg value );

	Scu_reg getFSM_DELAY(  );

	void assignPLOT_DELAY( Scu_reg value );

	Scu_reg getPLOT_DELAY(  );

	void assignFRAME_PLOT_NUM( Scu_reg value );

	Scu_reg getFRAME_PLOT_NUM(  );

	void assignREC_PLAY_index_full( Scu_reg value  );
	void assignREC_PLAY_loop_playback( Scu_reg value  );
	void assignREC_PLAY_index_load( Scu_reg value  );
	void assignREC_PLAY_index_inc( Scu_reg value  );
	void assignREC_PLAY_mode_play( Scu_reg value  );
	void assignREC_PLAY_mode_rec( Scu_reg value  );

	void assignREC_PLAY( Scu_reg value );


	Scu_reg getREC_PLAY_index_full(  );
	Scu_reg getREC_PLAY_loop_playback(  );
	Scu_reg getREC_PLAY_index_load(  );
	Scu_reg getREC_PLAY_index_inc(  );
	Scu_reg getREC_PLAY_mode_play(  );
	Scu_reg getREC_PLAY_mode_rec(  );

	Scu_reg getREC_PLAY(  );

	void assignWAVE_INFO_wave_plot_frame_cnt( Scu_reg value  );
	void assignWAVE_INFO_wave_play_vld( Scu_reg value  );

	void assignWAVE_INFO( Scu_reg value );


	Scu_reg getWAVE_INFO_wave_plot_frame_cnt(  );
	Scu_reg getWAVE_INFO_wave_play_vld(  );

	Scu_reg getWAVE_INFO(  );

	void assignINDEX_CUR_index( Scu_reg value  );

	void assignINDEX_CUR( Scu_reg value );


	Scu_reg getINDEX_CUR_index(  );

	Scu_reg getINDEX_CUR(  );

	void assignINDEX_BASE_index( Scu_reg value  );

	void assignINDEX_BASE( Scu_reg value );


	Scu_reg getINDEX_BASE_index(  );

	Scu_reg getINDEX_BASE(  );

	void assignINDEX_UPPER_index( Scu_reg value  );

	void assignINDEX_UPPER( Scu_reg value );


	Scu_reg getINDEX_UPPER_index(  );

	Scu_reg getINDEX_UPPER(  );

	void assignTRACE_trace_once_req( Scu_reg value  );
	void assignTRACE_measure_once_req( Scu_reg value  );
	void assignTRACE_search_once_req( Scu_reg value  );
	void assignTRACE_eye_once_req( Scu_reg value  );
	void assignTRACE_wave_bypass_wpu( Scu_reg value  );
	void assignTRACE_wave_bypass_trace( Scu_reg value  );
	void assignTRACE_avg_clr( Scu_reg value  );

	void assignTRACE( Scu_reg value );


	Scu_reg getTRACE_trace_once_req(  );
	Scu_reg getTRACE_measure_once_req(  );
	Scu_reg getTRACE_search_once_req(  );
	Scu_reg getTRACE_eye_once_req(  );
	Scu_reg getTRACE_wave_bypass_wpu(  );
	Scu_reg getTRACE_wave_bypass_trace(  );
	Scu_reg getTRACE_avg_clr(  );

	Scu_reg getTRACE(  );

	void assignTRIG_DLY( Scu_reg value );

	Scu_reg getTRIG_DLY(  );

	void assignPOS_SA_LAST_VIEW( Scu_reg value );

	Scu_reg getPOS_SA_LAST_VIEW(  );

	void assignMASK_FRM_NUM_L( Scu_reg value );

	Scu_reg getMASK_FRM_NUM_L(  );

	void assignMASK_FRM_NUM_H_frm_num_h( Scu_reg value  );
	void assignMASK_FRM_NUM_H_frm_num_en( Scu_reg value  );

	void assignMASK_FRM_NUM_H( Scu_reg value );


	Scu_reg getMASK_FRM_NUM_H_frm_num_h(  );
	Scu_reg getMASK_FRM_NUM_H_frm_num_en(  );

	Scu_reg getMASK_FRM_NUM_H(  );

	void assignMASK_TIMEOUT_timeout( Scu_reg value  );
	void assignMASK_TIMEOUT_timeout_en( Scu_reg value  );

	void assignMASK_TIMEOUT( Scu_reg value );


	Scu_reg getMASK_TIMEOUT_timeout(  );
	Scu_reg getMASK_TIMEOUT_timeout_en(  );

	Scu_reg getMASK_TIMEOUT(  );

	void assignSTATE_DISPLAY_scu_fsm( Scu_reg value  );
	void assignSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value  );
	void assignSTATE_DISPLAY_sys_stop_state( Scu_reg value  );
	void assignSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value  );
	void assignSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value  );
	void assignSTATE_DISPLAY_rec_play_stop_state( Scu_reg value  );
	void assignSTATE_DISPLAY_mask_stop_state( Scu_reg value  );
	void assignSTATE_DISPLAY_stop_stata_clr( Scu_reg value  );
	void assignSTATE_DISPLAY_disp_trig_clr( Scu_reg value  );

	void assignSTATE_DISPLAY( Scu_reg value );


	Scu_reg getSTATE_DISPLAY_scu_fsm(  );
	Scu_reg getSTATE_DISPLAY_sys_pre_sa_done(  );
	Scu_reg getSTATE_DISPLAY_sys_stop_state(  );
	Scu_reg getSTATE_DISPLAY_disp_tpu_scu_trig(  );
	Scu_reg getSTATE_DISPLAY_disp_sys_trig_d(  );
	Scu_reg getSTATE_DISPLAY_rec_play_stop_state(  );
	Scu_reg getSTATE_DISPLAY_mask_stop_state(  );
	Scu_reg getSTATE_DISPLAY_stop_stata_clr(  );
	Scu_reg getSTATE_DISPLAY_disp_trig_clr(  );

	Scu_reg getSTATE_DISPLAY(  );

	Scu_reg getMASK_TIMER(  );

	void assignCONFIG_ID( Scu_reg value );

	Scu_reg getCONFIG_ID(  );

	void assignDEUBG_wave_sa_ctrl( Scu_reg value  );
	void assignDEUBG_sys_auto_trig_act( Scu_reg value  );
	void assignDEUBG_sys_auto_trig_en( Scu_reg value  );
	void assignDEUBG_sys_fine_trig_req( Scu_reg value  );
	void assignDEUBG_sys_meas_only( Scu_reg value  );
	void assignDEUBG_sys_meas_en( Scu_reg value  );
	void assignDEUBG_sys_meas_type( Scu_reg value  );
	void assignDEUBG_spu_tx_roll_null( Scu_reg value  );
	void assignDEUBG_measure_once_done_hold( Scu_reg value  );
	void assignDEUBG_measure_finish_done_hold( Scu_reg value  );
	void assignDEUBG_sys_mask_en( Scu_reg value  );
	void assignDEUBG_mask_pf_result_vld_hold( Scu_reg value  );
	void assignDEUBG_mask_pf_result_cross_hold( Scu_reg value  );
	void assignDEUBG_mask_pf_timeout( Scu_reg value  );
	void assignDEUBG_mask_frm_overflow( Scu_reg value  );
	void assignDEUBG_measure_once_buf( Scu_reg value  );
	void assignDEUBG_zone_result_vld_hold( Scu_reg value  );
	void assignDEUBG_zone_result_trig_hold( Scu_reg value  );
	void assignDEUBG_wave_proc_la( Scu_reg value  );
	void assignDEUBG_sys_zoom( Scu_reg value  );
	void assignDEUBG_sys_fast_ref( Scu_reg value  );
	void assignDEUBG_spu_tx_done( Scu_reg value  );
	void assignDEUBG_sys_avg_en( Scu_reg value  );
	void assignDEUBG_sys_trace( Scu_reg value  );
	void assignDEUBG_sys_pos_sa_last_frm( Scu_reg value  );
	void assignDEUBG_sys_pos_sa_view( Scu_reg value  );
	void assignDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value  );
	void assignDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value  );
	void assignDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value  );

	void assignDEUBG( Scu_reg value );


	Scu_reg getDEUBG_wave_sa_ctrl(  );
	Scu_reg getDEUBG_sys_auto_trig_act(  );
	Scu_reg getDEUBG_sys_auto_trig_en(  );
	Scu_reg getDEUBG_sys_fine_trig_req(  );
	Scu_reg getDEUBG_sys_meas_only(  );
	Scu_reg getDEUBG_sys_meas_en(  );
	Scu_reg getDEUBG_sys_meas_type(  );
	Scu_reg getDEUBG_spu_tx_roll_null(  );
	Scu_reg getDEUBG_measure_once_done_hold(  );
	Scu_reg getDEUBG_measure_finish_done_hold(  );
	Scu_reg getDEUBG_sys_mask_en(  );
	Scu_reg getDEUBG_mask_pf_result_vld_hold(  );
	Scu_reg getDEUBG_mask_pf_result_cross_hold(  );
	Scu_reg getDEUBG_mask_pf_timeout(  );
	Scu_reg getDEUBG_mask_frm_overflow(  );
	Scu_reg getDEUBG_measure_once_buf(  );
	Scu_reg getDEUBG_zone_result_vld_hold(  );
	Scu_reg getDEUBG_zone_result_trig_hold(  );
	Scu_reg getDEUBG_wave_proc_la(  );
	Scu_reg getDEUBG_sys_zoom(  );
	Scu_reg getDEUBG_sys_fast_ref(  );
	Scu_reg getDEUBG_spu_tx_done(  );
	Scu_reg getDEUBG_sys_avg_en(  );
	Scu_reg getDEUBG_sys_trace(  );
	Scu_reg getDEUBG_sys_pos_sa_last_frm(  );
	Scu_reg getDEUBG_sys_pos_sa_view(  );
	Scu_reg getDEUBG_spu_wav_sa_rd_tx_done(  );
	Scu_reg getDEUBG_scan_roll_pos_trig_frm_buf(  );
	Scu_reg getDEUBG_scan_roll_pos_last_frm_buf(  );

	Scu_reg getDEUBG(  );

	Scu_reg getDEBUG_GT_receiver_gtu_state(  );

	Scu_reg getDEBUG_GT(  );

	void assignDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value  );

	void assignDEBUG_PLOT_DONE( Scu_reg value );


	Scu_reg getDEBUG_PLOT_DONE_wpu_plot_done_dly(  );

	Scu_reg getDEBUG_PLOT_DONE(  );

	void assignFPGA_SYNC_sync_cnt( Scu_reg value  );
	void assignFPGA_SYNC_debug_scu_reset( Scu_reg value  );
	void assignFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value  );

	void assignFPGA_SYNC( Scu_reg value );


	Scu_reg getFPGA_SYNC_sync_cnt(  );
	Scu_reg getFPGA_SYNC_debug_scu_reset(  );
	Scu_reg getFPGA_SYNC_debug_sys_rst_cnt(  );

	Scu_reg getFPGA_SYNC(  );

	Scu_reg getWPU_ID_sys_wpu_plot_id(  );
	Scu_reg getWPU_ID_wpu_plot_done_id(  );
	Scu_reg getWPU_ID_wpu_display_done_id(  );

	Scu_reg getWPU_ID(  );

	void assignDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value  );
	void assignDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value  );

	void assignDEBUG_WPU_PLOT( Scu_reg value );


	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_display_finish(  );
	Scu_reg getDEBUG_WPU_PLOT_wpu_fast_done_buf(  );
	Scu_reg getDEBUG_WPU_PLOT_wpu_display_done_buf(  );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_display_id_equ(  );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_plot_last(  );
	Scu_reg getDEBUG_WPU_PLOT_scu_wave_plot_req(  );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_start(  );
	Scu_reg getDEBUG_WPU_PLOT_sys_wpu_plot(  );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_done_buf(  );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_plot_en(  );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(  );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(  );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(  );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(  );
	Scu_reg getDEBUG_WPU_PLOT_cfg_wpu_flush(  );
	Scu_reg getDEBUG_WPU_PLOT_cfg_stop_plot(  );

	Scu_reg getDEBUG_WPU_PLOT(  );

	void assignDEBUG_GT_CRC_fail_sum( Scu_reg value  );
	void assignDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value  );
	void assignDEBUG_GT_CRC_crc_valid_i( Scu_reg value  );

	void assignDEBUG_GT_CRC( Scu_reg value );


	Scu_reg getDEBUG_GT_CRC_fail_sum(  );
	Scu_reg getDEBUG_GT_CRC_crc_pass_fail_n_i(  );
	Scu_reg getDEBUG_GT_CRC_crc_valid_i(  );

	Scu_reg getDEBUG_GT_CRC(  );

};
struct ScuMem : public ScuMemProxy, public IPhyFpga{
protected:
	Scu_reg mAddrBase;
public:
	ScuMem( Scu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( Scu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( ScuMemProxy *proxy);
	void pull( ScuMemProxy *proxy);

public:
	Scu_reg inVERSION(  );

	void setSCU_STATE_scu_fsm( Scu_reg value );
	void setSCU_STATE_scu_sa_fsm( Scu_reg value );
	void setSCU_STATE_sys_stop( Scu_reg value );
	void setSCU_STATE_scu_stop( Scu_reg value );
	void setSCU_STATE_scu_loop( Scu_reg value );
	void setSCU_STATE_sys_force_stop( Scu_reg value );
	void setSCU_STATE_process_sa( Scu_reg value );
	void setSCU_STATE_process_sa_done( Scu_reg value );
	void setSCU_STATE_sys_sa_en( Scu_reg value );
	void setSCU_STATE_sys_pre_sa_done( Scu_reg value );
	void setSCU_STATE_sys_pos_sa_done( Scu_reg value );
	void setSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value );
	void setSCU_STATE_sys_trig_d( Scu_reg value );
	void setSCU_STATE_tpu_scu_trig( Scu_reg value );
	void setSCU_STATE_fine_trig_done_buf( Scu_reg value );
	void setSCU_STATE_avg_proc_done( Scu_reg value );
	void setSCU_STATE_sys_avg_proc( Scu_reg value );
	void setSCU_STATE_sys_avg_exp( Scu_reg value );
	void setSCU_STATE_sys_wave_rd_en( Scu_reg value );
	void setSCU_STATE_sys_wave_proc_en( Scu_reg value );
	void setSCU_STATE_wave_mem_rd_done_hold( Scu_reg value );
	void setSCU_STATE_spu_tx_done_hold( Scu_reg value );
	void setSCU_STATE_gt_wave_tx_done( Scu_reg value );
	void setSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value );
	void setSCU_STATE_task_wave_tx_done_LA( Scu_reg value );
	void setSCU_STATE_task_wave_tx_done_CH( Scu_reg value );
	void setSCU_STATE_spu_proc_done( Scu_reg value );
	void setSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value );

	void setSCU_STATE( Scu_reg value );
	void outSCU_STATE_scu_fsm( Scu_reg value );
	void pulseSCU_STATE_scu_fsm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scu_sa_fsm( Scu_reg value );
	void pulseSCU_STATE_scu_sa_fsm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_stop( Scu_reg value );
	void pulseSCU_STATE_sys_stop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scu_stop( Scu_reg value );
	void pulseSCU_STATE_scu_stop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scu_loop( Scu_reg value );
	void pulseSCU_STATE_scu_loop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_force_stop( Scu_reg value );
	void pulseSCU_STATE_sys_force_stop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_process_sa( Scu_reg value );
	void pulseSCU_STATE_process_sa( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_process_sa_done( Scu_reg value );
	void pulseSCU_STATE_process_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_sa_en( Scu_reg value );
	void pulseSCU_STATE_sys_sa_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_pre_sa_done( Scu_reg value );
	void pulseSCU_STATE_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_pos_sa_done( Scu_reg value );
	void pulseSCU_STATE_sys_pos_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value );
	void pulseSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_trig_d( Scu_reg value );
	void pulseSCU_STATE_sys_trig_d( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_tpu_scu_trig( Scu_reg value );
	void pulseSCU_STATE_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_fine_trig_done_buf( Scu_reg value );
	void pulseSCU_STATE_fine_trig_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_avg_proc_done( Scu_reg value );
	void pulseSCU_STATE_avg_proc_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_avg_proc( Scu_reg value );
	void pulseSCU_STATE_sys_avg_proc( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_avg_exp( Scu_reg value );
	void pulseSCU_STATE_sys_avg_exp( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_wave_rd_en( Scu_reg value );
	void pulseSCU_STATE_sys_wave_rd_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_sys_wave_proc_en( Scu_reg value );
	void pulseSCU_STATE_sys_wave_proc_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_wave_mem_rd_done_hold( Scu_reg value );
	void pulseSCU_STATE_wave_mem_rd_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_tx_done_hold( Scu_reg value );
	void pulseSCU_STATE_spu_tx_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_gt_wave_tx_done( Scu_reg value );
	void pulseSCU_STATE_gt_wave_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value );
	void pulseSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_task_wave_tx_done_LA( Scu_reg value );
	void pulseSCU_STATE_task_wave_tx_done_LA( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_task_wave_tx_done_CH( Scu_reg value );
	void pulseSCU_STATE_task_wave_tx_done_CH( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_spu_proc_done( Scu_reg value );
	void pulseSCU_STATE_spu_proc_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value );
	void pulseSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outSCU_STATE( Scu_reg value );
	void outSCU_STATE(  );


	Scu_reg inSCU_STATE(  );

	void setCTRL_sys_stop_state( Scu_reg value );
	void setCTRL_cfg_fsm_run_stop_n( Scu_reg value );
	void setCTRL_run_single( Scu_reg value );
	void setCTRL_mode_play_last( Scu_reg value );
	void setCTRL_scu_fsm_run( Scu_reg value );
	void setCTRL_force_trig( Scu_reg value );
	void setCTRL_wpu_plot_display_finish( Scu_reg value );
	void setCTRL_soft_reset_gt( Scu_reg value );
	void setCTRL_ms_sync( Scu_reg value );
	void setCTRL_fsm_reset( Scu_reg value );

	void setCTRL( Scu_reg value );
	void outCTRL_sys_stop_state( Scu_reg value );
	void pulseCTRL_sys_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_cfg_fsm_run_stop_n( Scu_reg value );
	void pulseCTRL_cfg_fsm_run_stop_n( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_run_single( Scu_reg value );
	void pulseCTRL_run_single( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_mode_play_last( Scu_reg value );
	void pulseCTRL_mode_play_last( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_scu_fsm_run( Scu_reg value );
	void pulseCTRL_scu_fsm_run( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_force_trig( Scu_reg value );
	void pulseCTRL_force_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_wpu_plot_display_finish( Scu_reg value );
	void pulseCTRL_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_soft_reset_gt( Scu_reg value );
	void pulseCTRL_soft_reset_gt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_ms_sync( Scu_reg value );
	void pulseCTRL_ms_sync( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outCTRL_fsm_reset( Scu_reg value );
	void pulseCTRL_fsm_reset( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outCTRL( Scu_reg value );
	void outCTRL(  );


	Scu_reg inCTRL(  );

	void setMODE_cfg_mode_scan( Scu_reg value );
	void setMODE_sys_mode_scan_trace( Scu_reg value );
	void setMODE_sys_mode_scan_zoom( Scu_reg value );
	void setMODE_mode_scan_en( Scu_reg value );
	void setMODE_search_en_zoom( Scu_reg value );
	void setMODE_search_en_ch( Scu_reg value );
	void setMODE_search_en_la( Scu_reg value );
	void setMODE_mode_import_type( Scu_reg value );
	void setMODE_mode_export( Scu_reg value );
	void setMODE_mode_import_play( Scu_reg value );
	void setMODE_mode_import_rec( Scu_reg value );
	void setMODE_measure_en_zoom( Scu_reg value );
	void setMODE_measure_en_ch( Scu_reg value );
	void setMODE_measure_en_la( Scu_reg value );
	void setMODE_wave_ch_en( Scu_reg value );
	void setMODE_wave_la_en( Scu_reg value );
	void setMODE_zoom_en( Scu_reg value );
	void setMODE_mask_err_stop_en( Scu_reg value );
	void setMODE_mask_save_fail( Scu_reg value );
	void setMODE_mask_save_pass( Scu_reg value );
	void setMODE_mask_pf_en( Scu_reg value );
	void setMODE_zone_trig_en( Scu_reg value );
	void setMODE_mode_roll_en( Scu_reg value );
	void setMODE_mode_fast_ref( Scu_reg value );
	void setMODE_auto_trig( Scu_reg value );
	void setMODE_interleave_20G( Scu_reg value );
	void setMODE_en_20G( Scu_reg value );

	void setMODE( Scu_reg value );
	void outMODE_cfg_mode_scan( Scu_reg value );
	void pulseMODE_cfg_mode_scan( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_sys_mode_scan_trace( Scu_reg value );
	void pulseMODE_sys_mode_scan_trace( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_sys_mode_scan_zoom( Scu_reg value );
	void pulseMODE_sys_mode_scan_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mode_scan_en( Scu_reg value );
	void pulseMODE_mode_scan_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_search_en_zoom( Scu_reg value );
	void pulseMODE_search_en_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_search_en_ch( Scu_reg value );
	void pulseMODE_search_en_ch( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_search_en_la( Scu_reg value );
	void pulseMODE_search_en_la( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mode_import_type( Scu_reg value );
	void pulseMODE_mode_import_type( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mode_export( Scu_reg value );
	void pulseMODE_mode_export( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mode_import_play( Scu_reg value );
	void pulseMODE_mode_import_play( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mode_import_rec( Scu_reg value );
	void pulseMODE_mode_import_rec( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_measure_en_zoom( Scu_reg value );
	void pulseMODE_measure_en_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_measure_en_ch( Scu_reg value );
	void pulseMODE_measure_en_ch( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_measure_en_la( Scu_reg value );
	void pulseMODE_measure_en_la( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_wave_ch_en( Scu_reg value );
	void pulseMODE_wave_ch_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_wave_la_en( Scu_reg value );
	void pulseMODE_wave_la_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_zoom_en( Scu_reg value );
	void pulseMODE_zoom_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mask_err_stop_en( Scu_reg value );
	void pulseMODE_mask_err_stop_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mask_save_fail( Scu_reg value );
	void pulseMODE_mask_save_fail( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mask_save_pass( Scu_reg value );
	void pulseMODE_mask_save_pass( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mask_pf_en( Scu_reg value );
	void pulseMODE_mask_pf_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_zone_trig_en( Scu_reg value );
	void pulseMODE_zone_trig_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mode_roll_en( Scu_reg value );
	void pulseMODE_mode_roll_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_mode_fast_ref( Scu_reg value );
	void pulseMODE_mode_fast_ref( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_auto_trig( Scu_reg value );
	void pulseMODE_auto_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_interleave_20G( Scu_reg value );
	void pulseMODE_interleave_20G( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_en_20G( Scu_reg value );
	void pulseMODE_en_20G( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outMODE( Scu_reg value );
	void outMODE(  );


	Scu_reg inMODE(  );

	void setMODE_10G_chn_10G( Scu_reg value );
	void setMODE_10G_cfg_chn( Scu_reg value );

	void setMODE_10G( Scu_reg value );
	void outMODE_10G_chn_10G( Scu_reg value );
	void pulseMODE_10G_chn_10G( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMODE_10G_cfg_chn( Scu_reg value );
	void pulseMODE_10G_cfg_chn( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outMODE_10G( Scu_reg value );
	void outMODE_10G(  );


	Scu_reg inMODE_10G(  );

	void setPRE_SA( Scu_reg value );
	void outPRE_SA( Scu_reg value );
	void outPRE_SA(  );

	Scu_reg inPRE_SA(  );

	void setPOS_SA( Scu_reg value );
	void outPOS_SA( Scu_reg value );
	void outPOS_SA(  );

	Scu_reg inPOS_SA(  );

	void setSCAN_TRIG_POSITION_position( Scu_reg value );
	void setSCAN_TRIG_POSITION_trig_left_side( Scu_reg value );

	void setSCAN_TRIG_POSITION( Scu_reg value );
	void outSCAN_TRIG_POSITION_position( Scu_reg value );
	void pulseSCAN_TRIG_POSITION_position( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSCAN_TRIG_POSITION_trig_left_side( Scu_reg value );
	void pulseSCAN_TRIG_POSITION_trig_left_side( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outSCAN_TRIG_POSITION( Scu_reg value );
	void outSCAN_TRIG_POSITION(  );


	Scu_reg inSCAN_TRIG_POSITION(  );

	void setAUTO_TRIG_TIMEOUT( Scu_reg value );
	void outAUTO_TRIG_TIMEOUT( Scu_reg value );
	void outAUTO_TRIG_TIMEOUT(  );

	Scu_reg inAUTO_TRIG_TIMEOUT(  );

	void setAUTO_TRIG_HOLD_OFF( Scu_reg value );
	void outAUTO_TRIG_HOLD_OFF( Scu_reg value );
	void outAUTO_TRIG_HOLD_OFF(  );

	Scu_reg inAUTO_TRIG_HOLD_OFF(  );

	void setFSM_DELAY( Scu_reg value );
	void outFSM_DELAY( Scu_reg value );
	void outFSM_DELAY(  );

	Scu_reg inFSM_DELAY(  );

	void setPLOT_DELAY( Scu_reg value );
	void outPLOT_DELAY( Scu_reg value );
	void outPLOT_DELAY(  );

	Scu_reg inPLOT_DELAY(  );

	void setFRAME_PLOT_NUM( Scu_reg value );
	void outFRAME_PLOT_NUM( Scu_reg value );
	void outFRAME_PLOT_NUM(  );

	Scu_reg inFRAME_PLOT_NUM(  );

	void setREC_PLAY_index_full( Scu_reg value );
	void setREC_PLAY_loop_playback( Scu_reg value );
	void setREC_PLAY_index_load( Scu_reg value );
	void setREC_PLAY_index_inc( Scu_reg value );
	void setREC_PLAY_mode_play( Scu_reg value );
	void setREC_PLAY_mode_rec( Scu_reg value );

	void setREC_PLAY( Scu_reg value );
	void outREC_PLAY_index_full( Scu_reg value );
	void pulseREC_PLAY_index_full( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_loop_playback( Scu_reg value );
	void pulseREC_PLAY_loop_playback( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_index_load( Scu_reg value );
	void pulseREC_PLAY_index_load( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_index_inc( Scu_reg value );
	void pulseREC_PLAY_index_inc( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_mode_play( Scu_reg value );
	void pulseREC_PLAY_mode_play( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outREC_PLAY_mode_rec( Scu_reg value );
	void pulseREC_PLAY_mode_rec( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outREC_PLAY( Scu_reg value );
	void outREC_PLAY(  );


	Scu_reg inREC_PLAY(  );

	void setWAVE_INFO_wave_plot_frame_cnt( Scu_reg value );
	void setWAVE_INFO_wave_play_vld( Scu_reg value );

	void setWAVE_INFO( Scu_reg value );
	void outWAVE_INFO_wave_plot_frame_cnt( Scu_reg value );
	void pulseWAVE_INFO_wave_plot_frame_cnt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outWAVE_INFO_wave_play_vld( Scu_reg value );
	void pulseWAVE_INFO_wave_play_vld( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outWAVE_INFO( Scu_reg value );
	void outWAVE_INFO(  );


	Scu_reg inWAVE_INFO(  );

	void setINDEX_CUR_index( Scu_reg value );

	void setINDEX_CUR( Scu_reg value );
	void outINDEX_CUR_index( Scu_reg value );
	void pulseINDEX_CUR_index( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outINDEX_CUR( Scu_reg value );
	void outINDEX_CUR(  );


	Scu_reg inINDEX_CUR(  );

	void setINDEX_BASE_index( Scu_reg value );

	void setINDEX_BASE( Scu_reg value );
	void outINDEX_BASE_index( Scu_reg value );
	void pulseINDEX_BASE_index( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outINDEX_BASE( Scu_reg value );
	void outINDEX_BASE(  );


	Scu_reg inINDEX_BASE(  );

	void setINDEX_UPPER_index( Scu_reg value );

	void setINDEX_UPPER( Scu_reg value );
	void outINDEX_UPPER_index( Scu_reg value );
	void pulseINDEX_UPPER_index( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outINDEX_UPPER( Scu_reg value );
	void outINDEX_UPPER(  );


	Scu_reg inINDEX_UPPER(  );

	void setTRACE_trace_once_req( Scu_reg value );
	void setTRACE_measure_once_req( Scu_reg value );
	void setTRACE_search_once_req( Scu_reg value );
	void setTRACE_eye_once_req( Scu_reg value );
	void setTRACE_wave_bypass_wpu( Scu_reg value );
	void setTRACE_wave_bypass_trace( Scu_reg value );
	void setTRACE_avg_clr( Scu_reg value );

	void setTRACE( Scu_reg value );
	void outTRACE_trace_once_req( Scu_reg value );
	void pulseTRACE_trace_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outTRACE_measure_once_req( Scu_reg value );
	void pulseTRACE_measure_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outTRACE_search_once_req( Scu_reg value );
	void pulseTRACE_search_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outTRACE_eye_once_req( Scu_reg value );
	void pulseTRACE_eye_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outTRACE_wave_bypass_wpu( Scu_reg value );
	void pulseTRACE_wave_bypass_wpu( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outTRACE_wave_bypass_trace( Scu_reg value );
	void pulseTRACE_wave_bypass_trace( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outTRACE_avg_clr( Scu_reg value );
	void pulseTRACE_avg_clr( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outTRACE( Scu_reg value );
	void outTRACE(  );


	Scu_reg inTRACE(  );

	void setTRIG_DLY( Scu_reg value );
	void outTRIG_DLY( Scu_reg value );
	void outTRIG_DLY(  );

	Scu_reg inTRIG_DLY(  );

	void setPOS_SA_LAST_VIEW( Scu_reg value );
	void outPOS_SA_LAST_VIEW( Scu_reg value );
	void outPOS_SA_LAST_VIEW(  );

	Scu_reg inPOS_SA_LAST_VIEW(  );

	void setMASK_FRM_NUM_L( Scu_reg value );
	void outMASK_FRM_NUM_L( Scu_reg value );
	void outMASK_FRM_NUM_L(  );

	Scu_reg inMASK_FRM_NUM_L(  );

	void setMASK_FRM_NUM_H_frm_num_h( Scu_reg value );
	void setMASK_FRM_NUM_H_frm_num_en( Scu_reg value );

	void setMASK_FRM_NUM_H( Scu_reg value );
	void outMASK_FRM_NUM_H_frm_num_h( Scu_reg value );
	void pulseMASK_FRM_NUM_H_frm_num_h( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMASK_FRM_NUM_H_frm_num_en( Scu_reg value );
	void pulseMASK_FRM_NUM_H_frm_num_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outMASK_FRM_NUM_H( Scu_reg value );
	void outMASK_FRM_NUM_H(  );


	Scu_reg inMASK_FRM_NUM_H(  );

	void setMASK_TIMEOUT_timeout( Scu_reg value );
	void setMASK_TIMEOUT_timeout_en( Scu_reg value );

	void setMASK_TIMEOUT( Scu_reg value );
	void outMASK_TIMEOUT_timeout( Scu_reg value );
	void pulseMASK_TIMEOUT_timeout( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outMASK_TIMEOUT_timeout_en( Scu_reg value );
	void pulseMASK_TIMEOUT_timeout_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outMASK_TIMEOUT( Scu_reg value );
	void outMASK_TIMEOUT(  );


	Scu_reg inMASK_TIMEOUT(  );

	void setSTATE_DISPLAY_scu_fsm( Scu_reg value );
	void setSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value );
	void setSTATE_DISPLAY_sys_stop_state( Scu_reg value );
	void setSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value );
	void setSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value );
	void setSTATE_DISPLAY_rec_play_stop_state( Scu_reg value );
	void setSTATE_DISPLAY_mask_stop_state( Scu_reg value );
	void setSTATE_DISPLAY_stop_stata_clr( Scu_reg value );
	void setSTATE_DISPLAY_disp_trig_clr( Scu_reg value );

	void setSTATE_DISPLAY( Scu_reg value );
	void outSTATE_DISPLAY_scu_fsm( Scu_reg value );
	void pulseSTATE_DISPLAY_scu_fsm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value );
	void pulseSTATE_DISPLAY_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_sys_stop_state( Scu_reg value );
	void pulseSTATE_DISPLAY_sys_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value );
	void pulseSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value );
	void pulseSTATE_DISPLAY_disp_sys_trig_d( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_rec_play_stop_state( Scu_reg value );
	void pulseSTATE_DISPLAY_rec_play_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_mask_stop_state( Scu_reg value );
	void pulseSTATE_DISPLAY_mask_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_stop_stata_clr( Scu_reg value );
	void pulseSTATE_DISPLAY_stop_stata_clr( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outSTATE_DISPLAY_disp_trig_clr( Scu_reg value );
	void pulseSTATE_DISPLAY_disp_trig_clr( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outSTATE_DISPLAY( Scu_reg value );
	void outSTATE_DISPLAY(  );


	Scu_reg inSTATE_DISPLAY(  );

	Scu_reg inMASK_TIMER(  );

	void setCONFIG_ID( Scu_reg value );
	void outCONFIG_ID( Scu_reg value );
	void outCONFIG_ID(  );

	Scu_reg inCONFIG_ID(  );

	void setDEUBG_wave_sa_ctrl( Scu_reg value );
	void setDEUBG_sys_auto_trig_act( Scu_reg value );
	void setDEUBG_sys_auto_trig_en( Scu_reg value );
	void setDEUBG_sys_fine_trig_req( Scu_reg value );
	void setDEUBG_sys_meas_only( Scu_reg value );
	void setDEUBG_sys_meas_en( Scu_reg value );
	void setDEUBG_sys_meas_type( Scu_reg value );
	void setDEUBG_spu_tx_roll_null( Scu_reg value );
	void setDEUBG_measure_once_done_hold( Scu_reg value );
	void setDEUBG_measure_finish_done_hold( Scu_reg value );
	void setDEUBG_sys_mask_en( Scu_reg value );
	void setDEUBG_mask_pf_result_vld_hold( Scu_reg value );
	void setDEUBG_mask_pf_result_cross_hold( Scu_reg value );
	void setDEUBG_mask_pf_timeout( Scu_reg value );
	void setDEUBG_mask_frm_overflow( Scu_reg value );
	void setDEUBG_measure_once_buf( Scu_reg value );
	void setDEUBG_zone_result_vld_hold( Scu_reg value );
	void setDEUBG_zone_result_trig_hold( Scu_reg value );
	void setDEUBG_wave_proc_la( Scu_reg value );
	void setDEUBG_sys_zoom( Scu_reg value );
	void setDEUBG_sys_fast_ref( Scu_reg value );
	void setDEUBG_spu_tx_done( Scu_reg value );
	void setDEUBG_sys_avg_en( Scu_reg value );
	void setDEUBG_sys_trace( Scu_reg value );
	void setDEUBG_sys_pos_sa_last_frm( Scu_reg value );
	void setDEUBG_sys_pos_sa_view( Scu_reg value );
	void setDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value );
	void setDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value );
	void setDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value );

	void setDEUBG( Scu_reg value );
	void outDEUBG_wave_sa_ctrl( Scu_reg value );
	void pulseDEUBG_wave_sa_ctrl( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_auto_trig_act( Scu_reg value );
	void pulseDEUBG_sys_auto_trig_act( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_auto_trig_en( Scu_reg value );
	void pulseDEUBG_sys_auto_trig_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_fine_trig_req( Scu_reg value );
	void pulseDEUBG_sys_fine_trig_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_meas_only( Scu_reg value );
	void pulseDEUBG_sys_meas_only( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_meas_en( Scu_reg value );
	void pulseDEUBG_sys_meas_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_meas_type( Scu_reg value );
	void pulseDEUBG_sys_meas_type( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_spu_tx_roll_null( Scu_reg value );
	void pulseDEUBG_spu_tx_roll_null( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_measure_once_done_hold( Scu_reg value );
	void pulseDEUBG_measure_once_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_measure_finish_done_hold( Scu_reg value );
	void pulseDEUBG_measure_finish_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_mask_en( Scu_reg value );
	void pulseDEUBG_sys_mask_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_pf_result_vld_hold( Scu_reg value );
	void pulseDEUBG_mask_pf_result_vld_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_pf_result_cross_hold( Scu_reg value );
	void pulseDEUBG_mask_pf_result_cross_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_pf_timeout( Scu_reg value );
	void pulseDEUBG_mask_pf_timeout( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_mask_frm_overflow( Scu_reg value );
	void pulseDEUBG_mask_frm_overflow( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_measure_once_buf( Scu_reg value );
	void pulseDEUBG_measure_once_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_zone_result_vld_hold( Scu_reg value );
	void pulseDEUBG_zone_result_vld_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_zone_result_trig_hold( Scu_reg value );
	void pulseDEUBG_zone_result_trig_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_wave_proc_la( Scu_reg value );
	void pulseDEUBG_wave_proc_la( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_zoom( Scu_reg value );
	void pulseDEUBG_sys_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_fast_ref( Scu_reg value );
	void pulseDEUBG_sys_fast_ref( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_spu_tx_done( Scu_reg value );
	void pulseDEUBG_spu_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_avg_en( Scu_reg value );
	void pulseDEUBG_sys_avg_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_trace( Scu_reg value );
	void pulseDEUBG_sys_trace( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_pos_sa_last_frm( Scu_reg value );
	void pulseDEUBG_sys_pos_sa_last_frm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_sys_pos_sa_view( Scu_reg value );
	void pulseDEUBG_sys_pos_sa_view( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value );
	void pulseDEUBG_spu_wav_sa_rd_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value );
	void pulseDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value );
	void pulseDEUBG_scan_roll_pos_last_frm_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outDEUBG( Scu_reg value );
	void outDEUBG(  );


	Scu_reg inDEUBG(  );

	Scu_reg inDEBUG_GT(  );

	void setDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value );

	void setDEBUG_PLOT_DONE( Scu_reg value );
	void outDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value );
	void pulseDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outDEBUG_PLOT_DONE( Scu_reg value );
	void outDEBUG_PLOT_DONE(  );


	Scu_reg inDEBUG_PLOT_DONE(  );

	void setFPGA_SYNC_sync_cnt( Scu_reg value );
	void setFPGA_SYNC_debug_scu_reset( Scu_reg value );
	void setFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value );

	void setFPGA_SYNC( Scu_reg value );
	void outFPGA_SYNC_sync_cnt( Scu_reg value );
	void pulseFPGA_SYNC_sync_cnt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outFPGA_SYNC_debug_scu_reset( Scu_reg value );
	void pulseFPGA_SYNC_debug_scu_reset( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value );
	void pulseFPGA_SYNC_debug_sys_rst_cnt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outFPGA_SYNC( Scu_reg value );
	void outFPGA_SYNC(  );


	Scu_reg inFPGA_SYNC(  );

	Scu_reg inWPU_ID(  );

	void setDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value );
	void setDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value );
	void setDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value );
	void setDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value );
	void setDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value );
	void setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value );
	void setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value );
	void setDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value );
	void setDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value );

	void setDEBUG_WPU_PLOT( Scu_reg value );
	void outDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value );
	void pulseDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outDEBUG_WPU_PLOT( Scu_reg value );
	void outDEBUG_WPU_PLOT(  );


	Scu_reg inDEBUG_WPU_PLOT(  );

	void setDEBUG_GT_CRC_fail_sum( Scu_reg value );
	void setDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value );
	void setDEBUG_GT_CRC_crc_valid_i( Scu_reg value );

	void setDEBUG_GT_CRC( Scu_reg value );
	void outDEBUG_GT_CRC_fail_sum( Scu_reg value );
	void pulseDEBUG_GT_CRC_fail_sum( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value );
	void pulseDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );
	void outDEBUG_GT_CRC_crc_valid_i( Scu_reg value );
	void pulseDEBUG_GT_CRC_crc_valid_i( Scu_reg activeV, Scu_reg idleV=0, int timeus=0 );

	void outDEBUG_GT_CRC( Scu_reg value );
	void outDEBUG_GT_CRC(  );


	Scu_reg inDEBUG_GT_CRC(  );

public:
	cache_addr addr_VERSION();
	cache_addr addr_SCU_STATE();
	cache_addr addr_CTRL();
	cache_addr addr_MODE();

	cache_addr addr_MODE_10G();
	cache_addr addr_PRE_SA();
	cache_addr addr_POS_SA();
	cache_addr addr_SCAN_TRIG_POSITION();

	cache_addr addr_AUTO_TRIG_TIMEOUT();
	cache_addr addr_AUTO_TRIG_HOLD_OFF();
	cache_addr addr_FSM_DELAY();
	cache_addr addr_PLOT_DELAY();

	cache_addr addr_FRAME_PLOT_NUM();
	cache_addr addr_REC_PLAY();
	cache_addr addr_WAVE_INFO();
	cache_addr addr_INDEX_CUR();

	cache_addr addr_INDEX_BASE();
	cache_addr addr_INDEX_UPPER();
	cache_addr addr_TRACE();
	cache_addr addr_TRIG_DLY();

	cache_addr addr_POS_SA_LAST_VIEW();
	cache_addr addr_MASK_FRM_NUM_L();
	cache_addr addr_MASK_FRM_NUM_H();
	cache_addr addr_MASK_TIMEOUT();

	cache_addr addr_STATE_DISPLAY();
	cache_addr addr_MASK_TIMER();
	cache_addr addr_CONFIG_ID();
	cache_addr addr_DEUBG();

	cache_addr addr_DEBUG_GT();
	cache_addr addr_DEBUG_PLOT_DONE();
	cache_addr addr_FPGA_SYNC();
	cache_addr addr_WPU_ID();

	cache_addr addr_DEBUG_WPU_PLOT();
	cache_addr addr_DEBUG_GT_CRC();
};
#define _remap_Scu_()\
	mapIn( &VERSION, 0x1C00 + mAddrBase );\
	mapIn( &SCU_STATE, 0x1C04 + mAddrBase );\
	mapIn( &CTRL, 0x1C08 + mAddrBase );\
	mapIn( &MODE, 0x1C0C + mAddrBase );\
	\
	mapIn( &MODE_10G, 0x1C10 + mAddrBase );\
	mapIn( &PRE_SA, 0x1C14 + mAddrBase );\
	mapIn( &POS_SA, 0x1C18 + mAddrBase );\
	mapIn( &SCAN_TRIG_POSITION, 0x1C1C + mAddrBase );\
	\
	mapIn( &AUTO_TRIG_TIMEOUT, 0x1C20 + mAddrBase );\
	mapIn( &AUTO_TRIG_HOLD_OFF, 0x1C24 + mAddrBase );\
	mapIn( &FSM_DELAY, 0x1C28 + mAddrBase );\
	mapIn( &PLOT_DELAY, 0x1C34 + mAddrBase );\
	\
	mapIn( &FRAME_PLOT_NUM, 0x1C38 + mAddrBase );\
	mapIn( &REC_PLAY, 0x1C3C + mAddrBase );\
	mapIn( &WAVE_INFO, 0x1C40 + mAddrBase );\
	mapIn( &INDEX_CUR, 0x1C44 + mAddrBase );\
	\
	mapIn( &INDEX_BASE, 0x1C48 + mAddrBase );\
	mapIn( &INDEX_UPPER, 0x1C4C + mAddrBase );\
	mapIn( &TRACE, 0x1C50 + mAddrBase );\
	mapIn( &TRIG_DLY, 0x1C54 + mAddrBase );\
	\
	mapIn( &POS_SA_LAST_VIEW, 0x1C5C + mAddrBase );\
	mapIn( &MASK_FRM_NUM_L, 0x1C60 + mAddrBase );\
	mapIn( &MASK_FRM_NUM_H, 0x1C64 + mAddrBase );\
	mapIn( &MASK_TIMEOUT, 0x1C68 + mAddrBase );\
	\
	mapIn( &STATE_DISPLAY, 0x1C6C + mAddrBase );\
	mapIn( &MASK_TIMER, 0x1C70 + mAddrBase );\
	mapIn( &CONFIG_ID, 0x1C74 + mAddrBase );\
	mapIn( &DEUBG, 0x1C80 + mAddrBase );\
	\
	mapIn( &DEBUG_GT, 0x1C84 + mAddrBase );\
	mapIn( &DEBUG_PLOT_DONE, 0x1C88 + mAddrBase );\
	mapIn( &FPGA_SYNC, 0x1C8C + mAddrBase );\
	mapIn( &WPU_ID, 0x1C90 + mAddrBase );\
	\
	mapIn( &DEBUG_WPU_PLOT, 0x1C94 + mAddrBase );\
	mapIn( &DEBUG_GT_CRC, 0x1C98 + mAddrBase );\



class IPhyScu;
class ScuGp{
public:
	ScuGp(){}
protected:
	QList<IPhyScu*> mSubItems;

public:
	void attach( IPhyScu *pSubItem)
	{
		Q_ASSERT(NULL!=pSubItem);
		mSubItems.append(pSubItem);
	}

	IPhyScu * operator[](int id)
	{
		return mSubItems[id];
	}

	IPhyScu * at(int id)
	{
		return mSubItems[id];
	}

	int size()
	{
		return mSubItems.size();
	}

	void flushWCache(int id=-1);


public:
	Scu_reg getVERSION(  int id = 0 );

	Scu_reg inVERSION(  int id = 0 );

	void assignSCU_STATE_scu_fsm( Scu_reg value,int id=-1  );
	void assignSCU_STATE_scu_sa_fsm( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_stop( Scu_reg value,int id=-1  );
	void assignSCU_STATE_scu_stop( Scu_reg value,int id=-1  );
	void assignSCU_STATE_scu_loop( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_force_stop( Scu_reg value,int id=-1  );
	void assignSCU_STATE_process_sa( Scu_reg value,int id=-1  );
	void assignSCU_STATE_process_sa_done( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_sa_en( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_pre_sa_done( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_pos_sa_done( Scu_reg value,int id=-1  );
	void assignSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_trig_d( Scu_reg value,int id=-1  );
	void assignSCU_STATE_tpu_scu_trig( Scu_reg value,int id=-1  );
	void assignSCU_STATE_fine_trig_done_buf( Scu_reg value,int id=-1  );
	void assignSCU_STATE_avg_proc_done( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_avg_proc( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_avg_exp( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_wave_rd_en( Scu_reg value,int id=-1  );
	void assignSCU_STATE_sys_wave_proc_en( Scu_reg value,int id=-1  );
	void assignSCU_STATE_wave_mem_rd_done_hold( Scu_reg value,int id=-1  );
	void assignSCU_STATE_spu_tx_done_hold( Scu_reg value,int id=-1  );
	void assignSCU_STATE_gt_wave_tx_done( Scu_reg value,int id=-1  );
	void assignSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value,int id=-1  );
	void assignSCU_STATE_task_wave_tx_done_LA( Scu_reg value,int id=-1  );
	void assignSCU_STATE_task_wave_tx_done_CH( Scu_reg value,int id=-1  );
	void assignSCU_STATE_spu_proc_done( Scu_reg value,int id=-1  );
	void assignSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value,int id=-1  );

	void assignSCU_STATE( Scu_reg value,int id=-1 );


	void setSCU_STATE_scu_fsm( Scu_reg value,int id=-1 );
	void setSCU_STATE_scu_sa_fsm( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_stop( Scu_reg value,int id=-1 );
	void setSCU_STATE_scu_stop( Scu_reg value,int id=-1 );
	void setSCU_STATE_scu_loop( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_force_stop( Scu_reg value,int id=-1 );
	void setSCU_STATE_process_sa( Scu_reg value,int id=-1 );
	void setSCU_STATE_process_sa_done( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_sa_en( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_pre_sa_done( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_pos_sa_done( Scu_reg value,int id=-1 );
	void setSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_trig_d( Scu_reg value,int id=-1 );
	void setSCU_STATE_tpu_scu_trig( Scu_reg value,int id=-1 );
	void setSCU_STATE_fine_trig_done_buf( Scu_reg value,int id=-1 );
	void setSCU_STATE_avg_proc_done( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_avg_proc( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_avg_exp( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_wave_rd_en( Scu_reg value,int id=-1 );
	void setSCU_STATE_sys_wave_proc_en( Scu_reg value,int id=-1 );
	void setSCU_STATE_wave_mem_rd_done_hold( Scu_reg value,int id=-1 );
	void setSCU_STATE_spu_tx_done_hold( Scu_reg value,int id=-1 );
	void setSCU_STATE_gt_wave_tx_done( Scu_reg value,int id=-1 );
	void setSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value,int id=-1 );
	void setSCU_STATE_task_wave_tx_done_LA( Scu_reg value,int id=-1 );
	void setSCU_STATE_task_wave_tx_done_CH( Scu_reg value,int id=-1 );
	void setSCU_STATE_spu_proc_done( Scu_reg value,int id=-1 );
	void setSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value,int id=-1 );

	void setSCU_STATE( Scu_reg value,int id=-1 );
	void outSCU_STATE_scu_fsm( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_scu_fsm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_scu_sa_fsm( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_scu_sa_fsm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_stop( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_stop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_scu_stop( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_scu_stop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_scu_loop( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_scu_loop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_force_stop( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_force_stop( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_process_sa( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_process_sa( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_process_sa_done( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_process_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_sa_en( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_sa_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_pre_sa_done( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_pos_sa_done( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_pos_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_trig_d( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_trig_d( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_tpu_scu_trig( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_fine_trig_done_buf( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_fine_trig_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_avg_proc_done( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_avg_proc_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_avg_proc( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_avg_proc( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_avg_exp( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_avg_exp( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_wave_rd_en( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_wave_rd_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_sys_wave_proc_en( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_sys_wave_proc_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_wave_mem_rd_done_hold( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_wave_mem_rd_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_spu_tx_done_hold( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_spu_tx_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_gt_wave_tx_done( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_gt_wave_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_task_wave_tx_done_LA( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_task_wave_tx_done_LA( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_task_wave_tx_done_CH( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_task_wave_tx_done_CH( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_spu_proc_done( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_spu_proc_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value,int id=-1 );
	void pulseSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outSCU_STATE( Scu_reg value,int id=-1 );
	void outSCU_STATE( int id=-1 );


	Scu_reg getSCU_STATE_scu_fsm(  int id = 0 );
	Scu_reg getSCU_STATE_scu_sa_fsm(  int id = 0 );
	Scu_reg getSCU_STATE_sys_stop(  int id = 0 );
	Scu_reg getSCU_STATE_scu_stop(  int id = 0 );
	Scu_reg getSCU_STATE_scu_loop(  int id = 0 );
	Scu_reg getSCU_STATE_sys_force_stop(  int id = 0 );
	Scu_reg getSCU_STATE_process_sa(  int id = 0 );
	Scu_reg getSCU_STATE_process_sa_done(  int id = 0 );
	Scu_reg getSCU_STATE_sys_sa_en(  int id = 0 );
	Scu_reg getSCU_STATE_sys_pre_sa_done(  int id = 0 );
	Scu_reg getSCU_STATE_sys_pos_sa_done(  int id = 0 );
	Scu_reg getSCU_STATE_spu_wave_mem_wr_done_hold(  int id = 0 );
	Scu_reg getSCU_STATE_sys_trig_d(  int id = 0 );
	Scu_reg getSCU_STATE_tpu_scu_trig(  int id = 0 );
	Scu_reg getSCU_STATE_fine_trig_done_buf(  int id = 0 );
	Scu_reg getSCU_STATE_avg_proc_done(  int id = 0 );
	Scu_reg getSCU_STATE_sys_avg_proc(  int id = 0 );
	Scu_reg getSCU_STATE_sys_avg_exp(  int id = 0 );
	Scu_reg getSCU_STATE_sys_wave_rd_en(  int id = 0 );
	Scu_reg getSCU_STATE_sys_wave_proc_en(  int id = 0 );
	Scu_reg getSCU_STATE_wave_mem_rd_done_hold(  int id = 0 );
	Scu_reg getSCU_STATE_spu_tx_done_hold(  int id = 0 );
	Scu_reg getSCU_STATE_gt_wave_tx_done(  int id = 0 );
	Scu_reg getSCU_STATE_spu_wav_rd_meas_once_tx_done(  int id = 0 );
	Scu_reg getSCU_STATE_task_wave_tx_done_LA(  int id = 0 );
	Scu_reg getSCU_STATE_task_wave_tx_done_CH(  int id = 0 );
	Scu_reg getSCU_STATE_spu_proc_done(  int id = 0 );
	Scu_reg getSCU_STATE_scan_roll_sa_done_mem_tx_null(  int id = 0 );

	Scu_reg getSCU_STATE(  int id = 0 );

	Scu_reg inSCU_STATE(  int id = 0 );

	void assignCTRL_sys_stop_state( Scu_reg value,int id=-1  );
	void assignCTRL_cfg_fsm_run_stop_n( Scu_reg value,int id=-1  );
	void assignCTRL_run_single( Scu_reg value,int id=-1  );
	void assignCTRL_mode_play_last( Scu_reg value,int id=-1  );
	void assignCTRL_scu_fsm_run( Scu_reg value,int id=-1  );
	void assignCTRL_force_trig( Scu_reg value,int id=-1  );
	void assignCTRL_wpu_plot_display_finish( Scu_reg value,int id=-1  );
	void assignCTRL_soft_reset_gt( Scu_reg value,int id=-1  );
	void assignCTRL_ms_sync( Scu_reg value,int id=-1  );
	void assignCTRL_fsm_reset( Scu_reg value,int id=-1  );

	void assignCTRL( Scu_reg value,int id=-1 );


	void setCTRL_sys_stop_state( Scu_reg value,int id=-1 );
	void setCTRL_cfg_fsm_run_stop_n( Scu_reg value,int id=-1 );
	void setCTRL_run_single( Scu_reg value,int id=-1 );
	void setCTRL_mode_play_last( Scu_reg value,int id=-1 );
	void setCTRL_scu_fsm_run( Scu_reg value,int id=-1 );
	void setCTRL_force_trig( Scu_reg value,int id=-1 );
	void setCTRL_wpu_plot_display_finish( Scu_reg value,int id=-1 );
	void setCTRL_soft_reset_gt( Scu_reg value,int id=-1 );
	void setCTRL_ms_sync( Scu_reg value,int id=-1 );
	void setCTRL_fsm_reset( Scu_reg value,int id=-1 );

	void setCTRL( Scu_reg value,int id=-1 );
	void outCTRL_sys_stop_state( Scu_reg value,int id=-1 );
	void pulseCTRL_sys_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_cfg_fsm_run_stop_n( Scu_reg value,int id=-1 );
	void pulseCTRL_cfg_fsm_run_stop_n( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_run_single( Scu_reg value,int id=-1 );
	void pulseCTRL_run_single( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_mode_play_last( Scu_reg value,int id=-1 );
	void pulseCTRL_mode_play_last( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_scu_fsm_run( Scu_reg value,int id=-1 );
	void pulseCTRL_scu_fsm_run( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_force_trig( Scu_reg value,int id=-1 );
	void pulseCTRL_force_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_wpu_plot_display_finish( Scu_reg value,int id=-1 );
	void pulseCTRL_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_soft_reset_gt( Scu_reg value,int id=-1 );
	void pulseCTRL_soft_reset_gt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_ms_sync( Scu_reg value,int id=-1 );
	void pulseCTRL_ms_sync( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_fsm_reset( Scu_reg value,int id=-1 );
	void pulseCTRL_fsm_reset( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outCTRL( Scu_reg value,int id=-1 );
	void outCTRL( int id=-1 );


	Scu_reg getCTRL_sys_stop_state(  int id = 0 );
	Scu_reg getCTRL_cfg_fsm_run_stop_n(  int id = 0 );
	Scu_reg getCTRL_run_single(  int id = 0 );
	Scu_reg getCTRL_mode_play_last(  int id = 0 );
	Scu_reg getCTRL_scu_fsm_run(  int id = 0 );
	Scu_reg getCTRL_force_trig(  int id = 0 );
	Scu_reg getCTRL_wpu_plot_display_finish(  int id = 0 );
	Scu_reg getCTRL_soft_reset_gt(  int id = 0 );
	Scu_reg getCTRL_ms_sync(  int id = 0 );
	Scu_reg getCTRL_fsm_reset(  int id = 0 );

	Scu_reg getCTRL(  int id = 0 );

	Scu_reg inCTRL(  int id = 0 );

	void assignMODE_cfg_mode_scan( Scu_reg value,int id=-1  );
	void assignMODE_sys_mode_scan_trace( Scu_reg value,int id=-1  );
	void assignMODE_sys_mode_scan_zoom( Scu_reg value,int id=-1  );
	void assignMODE_mode_scan_en( Scu_reg value,int id=-1  );
	void assignMODE_search_en_zoom( Scu_reg value,int id=-1  );
	void assignMODE_search_en_ch( Scu_reg value,int id=-1  );
	void assignMODE_search_en_la( Scu_reg value,int id=-1  );
	void assignMODE_mode_import_type( Scu_reg value,int id=-1  );
	void assignMODE_mode_export( Scu_reg value,int id=-1  );
	void assignMODE_mode_import_play( Scu_reg value,int id=-1  );
	void assignMODE_mode_import_rec( Scu_reg value,int id=-1  );
	void assignMODE_measure_en_zoom( Scu_reg value,int id=-1  );
	void assignMODE_measure_en_ch( Scu_reg value,int id=-1  );
	void assignMODE_measure_en_la( Scu_reg value,int id=-1  );
	void assignMODE_wave_ch_en( Scu_reg value,int id=-1  );
	void assignMODE_wave_la_en( Scu_reg value,int id=-1  );
	void assignMODE_zoom_en( Scu_reg value,int id=-1  );
	void assignMODE_mask_err_stop_en( Scu_reg value,int id=-1  );
	void assignMODE_mask_save_fail( Scu_reg value,int id=-1  );
	void assignMODE_mask_save_pass( Scu_reg value,int id=-1  );
	void assignMODE_mask_pf_en( Scu_reg value,int id=-1  );
	void assignMODE_zone_trig_en( Scu_reg value,int id=-1  );
	void assignMODE_mode_roll_en( Scu_reg value,int id=-1  );
	void assignMODE_mode_fast_ref( Scu_reg value,int id=-1  );
	void assignMODE_auto_trig( Scu_reg value,int id=-1  );
	void assignMODE_interleave_20G( Scu_reg value,int id=-1  );
	void assignMODE_en_20G( Scu_reg value,int id=-1  );

	void assignMODE( Scu_reg value,int id=-1 );


	void setMODE_cfg_mode_scan( Scu_reg value,int id=-1 );
	void setMODE_sys_mode_scan_trace( Scu_reg value,int id=-1 );
	void setMODE_sys_mode_scan_zoom( Scu_reg value,int id=-1 );
	void setMODE_mode_scan_en( Scu_reg value,int id=-1 );
	void setMODE_search_en_zoom( Scu_reg value,int id=-1 );
	void setMODE_search_en_ch( Scu_reg value,int id=-1 );
	void setMODE_search_en_la( Scu_reg value,int id=-1 );
	void setMODE_mode_import_type( Scu_reg value,int id=-1 );
	void setMODE_mode_export( Scu_reg value,int id=-1 );
	void setMODE_mode_import_play( Scu_reg value,int id=-1 );
	void setMODE_mode_import_rec( Scu_reg value,int id=-1 );
	void setMODE_measure_en_zoom( Scu_reg value,int id=-1 );
	void setMODE_measure_en_ch( Scu_reg value,int id=-1 );
	void setMODE_measure_en_la( Scu_reg value,int id=-1 );
	void setMODE_wave_ch_en( Scu_reg value,int id=-1 );
	void setMODE_wave_la_en( Scu_reg value,int id=-1 );
	void setMODE_zoom_en( Scu_reg value,int id=-1 );
	void setMODE_mask_err_stop_en( Scu_reg value,int id=-1 );
	void setMODE_mask_save_fail( Scu_reg value,int id=-1 );
	void setMODE_mask_save_pass( Scu_reg value,int id=-1 );
	void setMODE_mask_pf_en( Scu_reg value,int id=-1 );
	void setMODE_zone_trig_en( Scu_reg value,int id=-1 );
	void setMODE_mode_roll_en( Scu_reg value,int id=-1 );
	void setMODE_mode_fast_ref( Scu_reg value,int id=-1 );
	void setMODE_auto_trig( Scu_reg value,int id=-1 );
	void setMODE_interleave_20G( Scu_reg value,int id=-1 );
	void setMODE_en_20G( Scu_reg value,int id=-1 );

	void setMODE( Scu_reg value,int id=-1 );
	void outMODE_cfg_mode_scan( Scu_reg value,int id=-1 );
	void pulseMODE_cfg_mode_scan( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_sys_mode_scan_trace( Scu_reg value,int id=-1 );
	void pulseMODE_sys_mode_scan_trace( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_sys_mode_scan_zoom( Scu_reg value,int id=-1 );
	void pulseMODE_sys_mode_scan_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mode_scan_en( Scu_reg value,int id=-1 );
	void pulseMODE_mode_scan_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_search_en_zoom( Scu_reg value,int id=-1 );
	void pulseMODE_search_en_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_search_en_ch( Scu_reg value,int id=-1 );
	void pulseMODE_search_en_ch( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_search_en_la( Scu_reg value,int id=-1 );
	void pulseMODE_search_en_la( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mode_import_type( Scu_reg value,int id=-1 );
	void pulseMODE_mode_import_type( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mode_export( Scu_reg value,int id=-1 );
	void pulseMODE_mode_export( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mode_import_play( Scu_reg value,int id=-1 );
	void pulseMODE_mode_import_play( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mode_import_rec( Scu_reg value,int id=-1 );
	void pulseMODE_mode_import_rec( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_measure_en_zoom( Scu_reg value,int id=-1 );
	void pulseMODE_measure_en_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_measure_en_ch( Scu_reg value,int id=-1 );
	void pulseMODE_measure_en_ch( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_measure_en_la( Scu_reg value,int id=-1 );
	void pulseMODE_measure_en_la( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_wave_ch_en( Scu_reg value,int id=-1 );
	void pulseMODE_wave_ch_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_wave_la_en( Scu_reg value,int id=-1 );
	void pulseMODE_wave_la_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_zoom_en( Scu_reg value,int id=-1 );
	void pulseMODE_zoom_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mask_err_stop_en( Scu_reg value,int id=-1 );
	void pulseMODE_mask_err_stop_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mask_save_fail( Scu_reg value,int id=-1 );
	void pulseMODE_mask_save_fail( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mask_save_pass( Scu_reg value,int id=-1 );
	void pulseMODE_mask_save_pass( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mask_pf_en( Scu_reg value,int id=-1 );
	void pulseMODE_mask_pf_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_zone_trig_en( Scu_reg value,int id=-1 );
	void pulseMODE_zone_trig_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mode_roll_en( Scu_reg value,int id=-1 );
	void pulseMODE_mode_roll_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_mode_fast_ref( Scu_reg value,int id=-1 );
	void pulseMODE_mode_fast_ref( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_auto_trig( Scu_reg value,int id=-1 );
	void pulseMODE_auto_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_interleave_20G( Scu_reg value,int id=-1 );
	void pulseMODE_interleave_20G( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_en_20G( Scu_reg value,int id=-1 );
	void pulseMODE_en_20G( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outMODE( Scu_reg value,int id=-1 );
	void outMODE( int id=-1 );


	Scu_reg getMODE_cfg_mode_scan(  int id = 0 );
	Scu_reg getMODE_sys_mode_scan_trace(  int id = 0 );
	Scu_reg getMODE_sys_mode_scan_zoom(  int id = 0 );
	Scu_reg getMODE_mode_scan_en(  int id = 0 );
	Scu_reg getMODE_search_en_zoom(  int id = 0 );
	Scu_reg getMODE_search_en_ch(  int id = 0 );
	Scu_reg getMODE_search_en_la(  int id = 0 );
	Scu_reg getMODE_mode_import_type(  int id = 0 );
	Scu_reg getMODE_mode_export(  int id = 0 );
	Scu_reg getMODE_mode_import_play(  int id = 0 );
	Scu_reg getMODE_mode_import_rec(  int id = 0 );
	Scu_reg getMODE_measure_en_zoom(  int id = 0 );
	Scu_reg getMODE_measure_en_ch(  int id = 0 );
	Scu_reg getMODE_measure_en_la(  int id = 0 );
	Scu_reg getMODE_wave_ch_en(  int id = 0 );
	Scu_reg getMODE_wave_la_en(  int id = 0 );
	Scu_reg getMODE_zoom_en(  int id = 0 );
	Scu_reg getMODE_mask_err_stop_en(  int id = 0 );
	Scu_reg getMODE_mask_save_fail(  int id = 0 );
	Scu_reg getMODE_mask_save_pass(  int id = 0 );
	Scu_reg getMODE_mask_pf_en(  int id = 0 );
	Scu_reg getMODE_zone_trig_en(  int id = 0 );
	Scu_reg getMODE_mode_roll_en(  int id = 0 );
	Scu_reg getMODE_mode_fast_ref(  int id = 0 );
	Scu_reg getMODE_auto_trig(  int id = 0 );
	Scu_reg getMODE_interleave_20G(  int id = 0 );
	Scu_reg getMODE_en_20G(  int id = 0 );

	Scu_reg getMODE(  int id = 0 );

	Scu_reg inMODE(  int id = 0 );

	void assignMODE_10G_chn_10G( Scu_reg value,int id=-1  );
	void assignMODE_10G_cfg_chn( Scu_reg value,int id=-1  );

	void assignMODE_10G( Scu_reg value,int id=-1 );


	void setMODE_10G_chn_10G( Scu_reg value,int id=-1 );
	void setMODE_10G_cfg_chn( Scu_reg value,int id=-1 );

	void setMODE_10G( Scu_reg value,int id=-1 );
	void outMODE_10G_chn_10G( Scu_reg value,int id=-1 );
	void pulseMODE_10G_chn_10G( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMODE_10G_cfg_chn( Scu_reg value,int id=-1 );
	void pulseMODE_10G_cfg_chn( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outMODE_10G( Scu_reg value,int id=-1 );
	void outMODE_10G( int id=-1 );


	Scu_reg getMODE_10G_chn_10G(  int id = 0 );
	Scu_reg getMODE_10G_cfg_chn(  int id = 0 );

	Scu_reg getMODE_10G(  int id = 0 );

	Scu_reg inMODE_10G(  int id = 0 );

	void assignPRE_SA( Scu_reg value,int id=-1 );

	void setPRE_SA( Scu_reg value,int id=-1 );
	void outPRE_SA( Scu_reg value,int id=-1 );
	void outPRE_SA( int id=-1 );

	Scu_reg getPRE_SA(  int id = 0 );

	Scu_reg inPRE_SA(  int id = 0 );

	void assignPOS_SA( Scu_reg value,int id=-1 );

	void setPOS_SA( Scu_reg value,int id=-1 );
	void outPOS_SA( Scu_reg value,int id=-1 );
	void outPOS_SA( int id=-1 );

	Scu_reg getPOS_SA(  int id = 0 );

	Scu_reg inPOS_SA(  int id = 0 );

	void assignSCAN_TRIG_POSITION_position( Scu_reg value,int id=-1  );
	void assignSCAN_TRIG_POSITION_trig_left_side( Scu_reg value,int id=-1  );

	void assignSCAN_TRIG_POSITION( Scu_reg value,int id=-1 );


	void setSCAN_TRIG_POSITION_position( Scu_reg value,int id=-1 );
	void setSCAN_TRIG_POSITION_trig_left_side( Scu_reg value,int id=-1 );

	void setSCAN_TRIG_POSITION( Scu_reg value,int id=-1 );
	void outSCAN_TRIG_POSITION_position( Scu_reg value,int id=-1 );
	void pulseSCAN_TRIG_POSITION_position( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSCAN_TRIG_POSITION_trig_left_side( Scu_reg value,int id=-1 );
	void pulseSCAN_TRIG_POSITION_trig_left_side( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outSCAN_TRIG_POSITION( Scu_reg value,int id=-1 );
	void outSCAN_TRIG_POSITION( int id=-1 );


	Scu_reg getSCAN_TRIG_POSITION_position(  int id = 0 );
	Scu_reg getSCAN_TRIG_POSITION_trig_left_side(  int id = 0 );

	Scu_reg getSCAN_TRIG_POSITION(  int id = 0 );

	Scu_reg inSCAN_TRIG_POSITION(  int id = 0 );

	void assignAUTO_TRIG_TIMEOUT( Scu_reg value,int id=-1 );

	void setAUTO_TRIG_TIMEOUT( Scu_reg value,int id=-1 );
	void outAUTO_TRIG_TIMEOUT( Scu_reg value,int id=-1 );
	void outAUTO_TRIG_TIMEOUT( int id=-1 );

	Scu_reg getAUTO_TRIG_TIMEOUT(  int id = 0 );

	Scu_reg inAUTO_TRIG_TIMEOUT(  int id = 0 );

	void assignAUTO_TRIG_HOLD_OFF( Scu_reg value,int id=-1 );

	void setAUTO_TRIG_HOLD_OFF( Scu_reg value,int id=-1 );
	void outAUTO_TRIG_HOLD_OFF( Scu_reg value,int id=-1 );
	void outAUTO_TRIG_HOLD_OFF( int id=-1 );

	Scu_reg getAUTO_TRIG_HOLD_OFF(  int id = 0 );

	Scu_reg inAUTO_TRIG_HOLD_OFF(  int id = 0 );

	void assignFSM_DELAY( Scu_reg value,int id=-1 );

	void setFSM_DELAY( Scu_reg value,int id=-1 );
	void outFSM_DELAY( Scu_reg value,int id=-1 );
	void outFSM_DELAY( int id=-1 );

	Scu_reg getFSM_DELAY(  int id = 0 );

	Scu_reg inFSM_DELAY(  int id = 0 );

	void assignPLOT_DELAY( Scu_reg value,int id=-1 );

	void setPLOT_DELAY( Scu_reg value,int id=-1 );
	void outPLOT_DELAY( Scu_reg value,int id=-1 );
	void outPLOT_DELAY( int id=-1 );

	Scu_reg getPLOT_DELAY(  int id = 0 );

	Scu_reg inPLOT_DELAY(  int id = 0 );

	void assignFRAME_PLOT_NUM( Scu_reg value,int id=-1 );

	void setFRAME_PLOT_NUM( Scu_reg value,int id=-1 );
	void outFRAME_PLOT_NUM( Scu_reg value,int id=-1 );
	void outFRAME_PLOT_NUM( int id=-1 );

	Scu_reg getFRAME_PLOT_NUM(  int id = 0 );

	Scu_reg inFRAME_PLOT_NUM(  int id = 0 );

	void assignREC_PLAY_index_full( Scu_reg value,int id=-1  );
	void assignREC_PLAY_loop_playback( Scu_reg value,int id=-1  );
	void assignREC_PLAY_index_load( Scu_reg value,int id=-1  );
	void assignREC_PLAY_index_inc( Scu_reg value,int id=-1  );
	void assignREC_PLAY_mode_play( Scu_reg value,int id=-1  );
	void assignREC_PLAY_mode_rec( Scu_reg value,int id=-1  );

	void assignREC_PLAY( Scu_reg value,int id=-1 );


	void setREC_PLAY_index_full( Scu_reg value,int id=-1 );
	void setREC_PLAY_loop_playback( Scu_reg value,int id=-1 );
	void setREC_PLAY_index_load( Scu_reg value,int id=-1 );
	void setREC_PLAY_index_inc( Scu_reg value,int id=-1 );
	void setREC_PLAY_mode_play( Scu_reg value,int id=-1 );
	void setREC_PLAY_mode_rec( Scu_reg value,int id=-1 );

	void setREC_PLAY( Scu_reg value,int id=-1 );
	void outREC_PLAY_index_full( Scu_reg value,int id=-1 );
	void pulseREC_PLAY_index_full( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outREC_PLAY_loop_playback( Scu_reg value,int id=-1 );
	void pulseREC_PLAY_loop_playback( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outREC_PLAY_index_load( Scu_reg value,int id=-1 );
	void pulseREC_PLAY_index_load( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outREC_PLAY_index_inc( Scu_reg value,int id=-1 );
	void pulseREC_PLAY_index_inc( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outREC_PLAY_mode_play( Scu_reg value,int id=-1 );
	void pulseREC_PLAY_mode_play( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outREC_PLAY_mode_rec( Scu_reg value,int id=-1 );
	void pulseREC_PLAY_mode_rec( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outREC_PLAY( Scu_reg value,int id=-1 );
	void outREC_PLAY( int id=-1 );


	Scu_reg getREC_PLAY_index_full(  int id = 0 );
	Scu_reg getREC_PLAY_loop_playback(  int id = 0 );
	Scu_reg getREC_PLAY_index_load(  int id = 0 );
	Scu_reg getREC_PLAY_index_inc(  int id = 0 );
	Scu_reg getREC_PLAY_mode_play(  int id = 0 );
	Scu_reg getREC_PLAY_mode_rec(  int id = 0 );

	Scu_reg getREC_PLAY(  int id = 0 );

	Scu_reg inREC_PLAY(  int id = 0 );

	void assignWAVE_INFO_wave_plot_frame_cnt( Scu_reg value,int id=-1  );
	void assignWAVE_INFO_wave_play_vld( Scu_reg value,int id=-1  );

	void assignWAVE_INFO( Scu_reg value,int id=-1 );


	void setWAVE_INFO_wave_plot_frame_cnt( Scu_reg value,int id=-1 );
	void setWAVE_INFO_wave_play_vld( Scu_reg value,int id=-1 );

	void setWAVE_INFO( Scu_reg value,int id=-1 );
	void outWAVE_INFO_wave_plot_frame_cnt( Scu_reg value,int id=-1 );
	void pulseWAVE_INFO_wave_plot_frame_cnt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outWAVE_INFO_wave_play_vld( Scu_reg value,int id=-1 );
	void pulseWAVE_INFO_wave_play_vld( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outWAVE_INFO( Scu_reg value,int id=-1 );
	void outWAVE_INFO( int id=-1 );


	Scu_reg getWAVE_INFO_wave_plot_frame_cnt(  int id = 0 );
	Scu_reg getWAVE_INFO_wave_play_vld(  int id = 0 );

	Scu_reg getWAVE_INFO(  int id = 0 );

	Scu_reg inWAVE_INFO(  int id = 0 );

	void assignINDEX_CUR_index( Scu_reg value,int id=-1  );

	void assignINDEX_CUR( Scu_reg value,int id=-1 );


	void setINDEX_CUR_index( Scu_reg value,int id=-1 );

	void setINDEX_CUR( Scu_reg value,int id=-1 );
	void outINDEX_CUR_index( Scu_reg value,int id=-1 );
	void pulseINDEX_CUR_index( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outINDEX_CUR( Scu_reg value,int id=-1 );
	void outINDEX_CUR( int id=-1 );


	Scu_reg getINDEX_CUR_index(  int id = 0 );

	Scu_reg getINDEX_CUR(  int id = 0 );

	Scu_reg inINDEX_CUR(  int id = 0 );

	void assignINDEX_BASE_index( Scu_reg value,int id=-1  );

	void assignINDEX_BASE( Scu_reg value,int id=-1 );


	void setINDEX_BASE_index( Scu_reg value,int id=-1 );

	void setINDEX_BASE( Scu_reg value,int id=-1 );
	void outINDEX_BASE_index( Scu_reg value,int id=-1 );
	void pulseINDEX_BASE_index( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outINDEX_BASE( Scu_reg value,int id=-1 );
	void outINDEX_BASE( int id=-1 );


	Scu_reg getINDEX_BASE_index(  int id = 0 );

	Scu_reg getINDEX_BASE(  int id = 0 );

	Scu_reg inINDEX_BASE(  int id = 0 );

	void assignINDEX_UPPER_index( Scu_reg value,int id=-1  );

	void assignINDEX_UPPER( Scu_reg value,int id=-1 );


	void setINDEX_UPPER_index( Scu_reg value,int id=-1 );

	void setINDEX_UPPER( Scu_reg value,int id=-1 );
	void outINDEX_UPPER_index( Scu_reg value,int id=-1 );
	void pulseINDEX_UPPER_index( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outINDEX_UPPER( Scu_reg value,int id=-1 );
	void outINDEX_UPPER( int id=-1 );


	Scu_reg getINDEX_UPPER_index(  int id = 0 );

	Scu_reg getINDEX_UPPER(  int id = 0 );

	Scu_reg inINDEX_UPPER(  int id = 0 );

	void assignTRACE_trace_once_req( Scu_reg value,int id=-1  );
	void assignTRACE_measure_once_req( Scu_reg value,int id=-1  );
	void assignTRACE_search_once_req( Scu_reg value,int id=-1  );
	void assignTRACE_eye_once_req( Scu_reg value,int id=-1  );
	void assignTRACE_wave_bypass_wpu( Scu_reg value,int id=-1  );
	void assignTRACE_wave_bypass_trace( Scu_reg value,int id=-1  );
	void assignTRACE_avg_clr( Scu_reg value,int id=-1  );

	void assignTRACE( Scu_reg value,int id=-1 );


	void setTRACE_trace_once_req( Scu_reg value,int id=-1 );
	void setTRACE_measure_once_req( Scu_reg value,int id=-1 );
	void setTRACE_search_once_req( Scu_reg value,int id=-1 );
	void setTRACE_eye_once_req( Scu_reg value,int id=-1 );
	void setTRACE_wave_bypass_wpu( Scu_reg value,int id=-1 );
	void setTRACE_wave_bypass_trace( Scu_reg value,int id=-1 );
	void setTRACE_avg_clr( Scu_reg value,int id=-1 );

	void setTRACE( Scu_reg value,int id=-1 );
	void outTRACE_trace_once_req( Scu_reg value,int id=-1 );
	void pulseTRACE_trace_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_measure_once_req( Scu_reg value,int id=-1 );
	void pulseTRACE_measure_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_search_once_req( Scu_reg value,int id=-1 );
	void pulseTRACE_search_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_eye_once_req( Scu_reg value,int id=-1 );
	void pulseTRACE_eye_once_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_wave_bypass_wpu( Scu_reg value,int id=-1 );
	void pulseTRACE_wave_bypass_wpu( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_wave_bypass_trace( Scu_reg value,int id=-1 );
	void pulseTRACE_wave_bypass_trace( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_avg_clr( Scu_reg value,int id=-1 );
	void pulseTRACE_avg_clr( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outTRACE( Scu_reg value,int id=-1 );
	void outTRACE( int id=-1 );


	Scu_reg getTRACE_trace_once_req(  int id = 0 );
	Scu_reg getTRACE_measure_once_req(  int id = 0 );
	Scu_reg getTRACE_search_once_req(  int id = 0 );
	Scu_reg getTRACE_eye_once_req(  int id = 0 );
	Scu_reg getTRACE_wave_bypass_wpu(  int id = 0 );
	Scu_reg getTRACE_wave_bypass_trace(  int id = 0 );
	Scu_reg getTRACE_avg_clr(  int id = 0 );

	Scu_reg getTRACE(  int id = 0 );

	Scu_reg inTRACE(  int id = 0 );

	void assignTRIG_DLY( Scu_reg value,int id=-1 );

	void setTRIG_DLY( Scu_reg value,int id=-1 );
	void outTRIG_DLY( Scu_reg value,int id=-1 );
	void outTRIG_DLY( int id=-1 );

	Scu_reg getTRIG_DLY(  int id = 0 );

	Scu_reg inTRIG_DLY(  int id = 0 );

	void assignPOS_SA_LAST_VIEW( Scu_reg value,int id=-1 );

	void setPOS_SA_LAST_VIEW( Scu_reg value,int id=-1 );
	void outPOS_SA_LAST_VIEW( Scu_reg value,int id=-1 );
	void outPOS_SA_LAST_VIEW( int id=-1 );

	Scu_reg getPOS_SA_LAST_VIEW(  int id = 0 );

	Scu_reg inPOS_SA_LAST_VIEW(  int id = 0 );

	void assignMASK_FRM_NUM_L( Scu_reg value,int id=-1 );

	void setMASK_FRM_NUM_L( Scu_reg value,int id=-1 );
	void outMASK_FRM_NUM_L( Scu_reg value,int id=-1 );
	void outMASK_FRM_NUM_L( int id=-1 );

	Scu_reg getMASK_FRM_NUM_L(  int id = 0 );

	Scu_reg inMASK_FRM_NUM_L(  int id = 0 );

	void assignMASK_FRM_NUM_H_frm_num_h( Scu_reg value,int id=-1  );
	void assignMASK_FRM_NUM_H_frm_num_en( Scu_reg value,int id=-1  );

	void assignMASK_FRM_NUM_H( Scu_reg value,int id=-1 );


	void setMASK_FRM_NUM_H_frm_num_h( Scu_reg value,int id=-1 );
	void setMASK_FRM_NUM_H_frm_num_en( Scu_reg value,int id=-1 );

	void setMASK_FRM_NUM_H( Scu_reg value,int id=-1 );
	void outMASK_FRM_NUM_H_frm_num_h( Scu_reg value,int id=-1 );
	void pulseMASK_FRM_NUM_H_frm_num_h( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_FRM_NUM_H_frm_num_en( Scu_reg value,int id=-1 );
	void pulseMASK_FRM_NUM_H_frm_num_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outMASK_FRM_NUM_H( Scu_reg value,int id=-1 );
	void outMASK_FRM_NUM_H( int id=-1 );


	Scu_reg getMASK_FRM_NUM_H_frm_num_h(  int id = 0 );
	Scu_reg getMASK_FRM_NUM_H_frm_num_en(  int id = 0 );

	Scu_reg getMASK_FRM_NUM_H(  int id = 0 );

	Scu_reg inMASK_FRM_NUM_H(  int id = 0 );

	void assignMASK_TIMEOUT_timeout( Scu_reg value,int id=-1  );
	void assignMASK_TIMEOUT_timeout_en( Scu_reg value,int id=-1  );

	void assignMASK_TIMEOUT( Scu_reg value,int id=-1 );


	void setMASK_TIMEOUT_timeout( Scu_reg value,int id=-1 );
	void setMASK_TIMEOUT_timeout_en( Scu_reg value,int id=-1 );

	void setMASK_TIMEOUT( Scu_reg value,int id=-1 );
	void outMASK_TIMEOUT_timeout( Scu_reg value,int id=-1 );
	void pulseMASK_TIMEOUT_timeout( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TIMEOUT_timeout_en( Scu_reg value,int id=-1 );
	void pulseMASK_TIMEOUT_timeout_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outMASK_TIMEOUT( Scu_reg value,int id=-1 );
	void outMASK_TIMEOUT( int id=-1 );


	Scu_reg getMASK_TIMEOUT_timeout(  int id = 0 );
	Scu_reg getMASK_TIMEOUT_timeout_en(  int id = 0 );

	Scu_reg getMASK_TIMEOUT(  int id = 0 );

	Scu_reg inMASK_TIMEOUT(  int id = 0 );

	void assignSTATE_DISPLAY_scu_fsm( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_sys_stop_state( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_rec_play_stop_state( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_mask_stop_state( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_stop_stata_clr( Scu_reg value,int id=-1  );
	void assignSTATE_DISPLAY_disp_trig_clr( Scu_reg value,int id=-1  );

	void assignSTATE_DISPLAY( Scu_reg value,int id=-1 );


	void setSTATE_DISPLAY_scu_fsm( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_sys_stop_state( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_rec_play_stop_state( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_mask_stop_state( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_stop_stata_clr( Scu_reg value,int id=-1 );
	void setSTATE_DISPLAY_disp_trig_clr( Scu_reg value,int id=-1 );

	void setSTATE_DISPLAY( Scu_reg value,int id=-1 );
	void outSTATE_DISPLAY_scu_fsm( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_scu_fsm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_sys_stop_state( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_sys_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_disp_sys_trig_d( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_rec_play_stop_state( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_rec_play_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_mask_stop_state( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_mask_stop_state( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_stop_stata_clr( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_stop_stata_clr( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outSTATE_DISPLAY_disp_trig_clr( Scu_reg value,int id=-1 );
	void pulseSTATE_DISPLAY_disp_trig_clr( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outSTATE_DISPLAY( Scu_reg value,int id=-1 );
	void outSTATE_DISPLAY( int id=-1 );


	Scu_reg getSTATE_DISPLAY_scu_fsm(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_sys_pre_sa_done(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_sys_stop_state(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_disp_tpu_scu_trig(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_disp_sys_trig_d(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_rec_play_stop_state(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_mask_stop_state(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_stop_stata_clr(  int id = 0 );
	Scu_reg getSTATE_DISPLAY_disp_trig_clr(  int id = 0 );

	Scu_reg getSTATE_DISPLAY(  int id = 0 );

	Scu_reg inSTATE_DISPLAY(  int id = 0 );

	Scu_reg getMASK_TIMER(  int id = 0 );

	Scu_reg inMASK_TIMER(  int id = 0 );

	void assignCONFIG_ID( Scu_reg value,int id=-1 );

	void setCONFIG_ID( Scu_reg value,int id=-1 );
	void outCONFIG_ID( Scu_reg value,int id=-1 );
	void outCONFIG_ID( int id=-1 );

	Scu_reg getCONFIG_ID(  int id = 0 );

	Scu_reg inCONFIG_ID(  int id = 0 );

	void assignDEUBG_wave_sa_ctrl( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_auto_trig_act( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_auto_trig_en( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_fine_trig_req( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_meas_only( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_meas_en( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_meas_type( Scu_reg value,int id=-1  );
	void assignDEUBG_spu_tx_roll_null( Scu_reg value,int id=-1  );
	void assignDEUBG_measure_once_done_hold( Scu_reg value,int id=-1  );
	void assignDEUBG_measure_finish_done_hold( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_mask_en( Scu_reg value,int id=-1  );
	void assignDEUBG_mask_pf_result_vld_hold( Scu_reg value,int id=-1  );
	void assignDEUBG_mask_pf_result_cross_hold( Scu_reg value,int id=-1  );
	void assignDEUBG_mask_pf_timeout( Scu_reg value,int id=-1  );
	void assignDEUBG_mask_frm_overflow( Scu_reg value,int id=-1  );
	void assignDEUBG_measure_once_buf( Scu_reg value,int id=-1  );
	void assignDEUBG_zone_result_vld_hold( Scu_reg value,int id=-1  );
	void assignDEUBG_zone_result_trig_hold( Scu_reg value,int id=-1  );
	void assignDEUBG_wave_proc_la( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_zoom( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_fast_ref( Scu_reg value,int id=-1  );
	void assignDEUBG_spu_tx_done( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_avg_en( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_trace( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_pos_sa_last_frm( Scu_reg value,int id=-1  );
	void assignDEUBG_sys_pos_sa_view( Scu_reg value,int id=-1  );
	void assignDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value,int id=-1  );
	void assignDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value,int id=-1  );
	void assignDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value,int id=-1  );

	void assignDEUBG( Scu_reg value,int id=-1 );


	void setDEUBG_wave_sa_ctrl( Scu_reg value,int id=-1 );
	void setDEUBG_sys_auto_trig_act( Scu_reg value,int id=-1 );
	void setDEUBG_sys_auto_trig_en( Scu_reg value,int id=-1 );
	void setDEUBG_sys_fine_trig_req( Scu_reg value,int id=-1 );
	void setDEUBG_sys_meas_only( Scu_reg value,int id=-1 );
	void setDEUBG_sys_meas_en( Scu_reg value,int id=-1 );
	void setDEUBG_sys_meas_type( Scu_reg value,int id=-1 );
	void setDEUBG_spu_tx_roll_null( Scu_reg value,int id=-1 );
	void setDEUBG_measure_once_done_hold( Scu_reg value,int id=-1 );
	void setDEUBG_measure_finish_done_hold( Scu_reg value,int id=-1 );
	void setDEUBG_sys_mask_en( Scu_reg value,int id=-1 );
	void setDEUBG_mask_pf_result_vld_hold( Scu_reg value,int id=-1 );
	void setDEUBG_mask_pf_result_cross_hold( Scu_reg value,int id=-1 );
	void setDEUBG_mask_pf_timeout( Scu_reg value,int id=-1 );
	void setDEUBG_mask_frm_overflow( Scu_reg value,int id=-1 );
	void setDEUBG_measure_once_buf( Scu_reg value,int id=-1 );
	void setDEUBG_zone_result_vld_hold( Scu_reg value,int id=-1 );
	void setDEUBG_zone_result_trig_hold( Scu_reg value,int id=-1 );
	void setDEUBG_wave_proc_la( Scu_reg value,int id=-1 );
	void setDEUBG_sys_zoom( Scu_reg value,int id=-1 );
	void setDEUBG_sys_fast_ref( Scu_reg value,int id=-1 );
	void setDEUBG_spu_tx_done( Scu_reg value,int id=-1 );
	void setDEUBG_sys_avg_en( Scu_reg value,int id=-1 );
	void setDEUBG_sys_trace( Scu_reg value,int id=-1 );
	void setDEUBG_sys_pos_sa_last_frm( Scu_reg value,int id=-1 );
	void setDEUBG_sys_pos_sa_view( Scu_reg value,int id=-1 );
	void setDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value,int id=-1 );
	void setDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value,int id=-1 );
	void setDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value,int id=-1 );

	void setDEUBG( Scu_reg value,int id=-1 );
	void outDEUBG_wave_sa_ctrl( Scu_reg value,int id=-1 );
	void pulseDEUBG_wave_sa_ctrl( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_auto_trig_act( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_auto_trig_act( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_auto_trig_en( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_auto_trig_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_fine_trig_req( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_fine_trig_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_meas_only( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_meas_only( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_meas_en( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_meas_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_meas_type( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_meas_type( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_spu_tx_roll_null( Scu_reg value,int id=-1 );
	void pulseDEUBG_spu_tx_roll_null( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_measure_once_done_hold( Scu_reg value,int id=-1 );
	void pulseDEUBG_measure_once_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_measure_finish_done_hold( Scu_reg value,int id=-1 );
	void pulseDEUBG_measure_finish_done_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_mask_en( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_mask_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_mask_pf_result_vld_hold( Scu_reg value,int id=-1 );
	void pulseDEUBG_mask_pf_result_vld_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_mask_pf_result_cross_hold( Scu_reg value,int id=-1 );
	void pulseDEUBG_mask_pf_result_cross_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_mask_pf_timeout( Scu_reg value,int id=-1 );
	void pulseDEUBG_mask_pf_timeout( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_mask_frm_overflow( Scu_reg value,int id=-1 );
	void pulseDEUBG_mask_frm_overflow( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_measure_once_buf( Scu_reg value,int id=-1 );
	void pulseDEUBG_measure_once_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_zone_result_vld_hold( Scu_reg value,int id=-1 );
	void pulseDEUBG_zone_result_vld_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_zone_result_trig_hold( Scu_reg value,int id=-1 );
	void pulseDEUBG_zone_result_trig_hold( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_wave_proc_la( Scu_reg value,int id=-1 );
	void pulseDEUBG_wave_proc_la( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_zoom( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_zoom( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_fast_ref( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_fast_ref( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_spu_tx_done( Scu_reg value,int id=-1 );
	void pulseDEUBG_spu_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_avg_en( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_avg_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_trace( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_trace( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_pos_sa_last_frm( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_pos_sa_last_frm( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_sys_pos_sa_view( Scu_reg value,int id=-1 );
	void pulseDEUBG_sys_pos_sa_view( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value,int id=-1 );
	void pulseDEUBG_spu_wav_sa_rd_tx_done( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value,int id=-1 );
	void pulseDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value,int id=-1 );
	void pulseDEUBG_scan_roll_pos_last_frm_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEUBG( Scu_reg value,int id=-1 );
	void outDEUBG( int id=-1 );


	Scu_reg getDEUBG_wave_sa_ctrl(  int id = 0 );
	Scu_reg getDEUBG_sys_auto_trig_act(  int id = 0 );
	Scu_reg getDEUBG_sys_auto_trig_en(  int id = 0 );
	Scu_reg getDEUBG_sys_fine_trig_req(  int id = 0 );
	Scu_reg getDEUBG_sys_meas_only(  int id = 0 );
	Scu_reg getDEUBG_sys_meas_en(  int id = 0 );
	Scu_reg getDEUBG_sys_meas_type(  int id = 0 );
	Scu_reg getDEUBG_spu_tx_roll_null(  int id = 0 );
	Scu_reg getDEUBG_measure_once_done_hold(  int id = 0 );
	Scu_reg getDEUBG_measure_finish_done_hold(  int id = 0 );
	Scu_reg getDEUBG_sys_mask_en(  int id = 0 );
	Scu_reg getDEUBG_mask_pf_result_vld_hold(  int id = 0 );
	Scu_reg getDEUBG_mask_pf_result_cross_hold(  int id = 0 );
	Scu_reg getDEUBG_mask_pf_timeout(  int id = 0 );
	Scu_reg getDEUBG_mask_frm_overflow(  int id = 0 );
	Scu_reg getDEUBG_measure_once_buf(  int id = 0 );
	Scu_reg getDEUBG_zone_result_vld_hold(  int id = 0 );
	Scu_reg getDEUBG_zone_result_trig_hold(  int id = 0 );
	Scu_reg getDEUBG_wave_proc_la(  int id = 0 );
	Scu_reg getDEUBG_sys_zoom(  int id = 0 );
	Scu_reg getDEUBG_sys_fast_ref(  int id = 0 );
	Scu_reg getDEUBG_spu_tx_done(  int id = 0 );
	Scu_reg getDEUBG_sys_avg_en(  int id = 0 );
	Scu_reg getDEUBG_sys_trace(  int id = 0 );
	Scu_reg getDEUBG_sys_pos_sa_last_frm(  int id = 0 );
	Scu_reg getDEUBG_sys_pos_sa_view(  int id = 0 );
	Scu_reg getDEUBG_spu_wav_sa_rd_tx_done(  int id = 0 );
	Scu_reg getDEUBG_scan_roll_pos_trig_frm_buf(  int id = 0 );
	Scu_reg getDEUBG_scan_roll_pos_last_frm_buf(  int id = 0 );

	Scu_reg getDEUBG(  int id = 0 );

	Scu_reg inDEUBG(  int id = 0 );

	Scu_reg getDEBUG_GT_receiver_gtu_state(  int id = 0 );

	Scu_reg getDEBUG_GT(  int id = 0 );

	Scu_reg inDEBUG_GT(  int id = 0 );

	void assignDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value,int id=-1  );

	void assignDEBUG_PLOT_DONE( Scu_reg value,int id=-1 );


	void setDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value,int id=-1 );

	void setDEBUG_PLOT_DONE( Scu_reg value,int id=-1 );
	void outDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value,int id=-1 );
	void pulseDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEBUG_PLOT_DONE( Scu_reg value,int id=-1 );
	void outDEBUG_PLOT_DONE( int id=-1 );


	Scu_reg getDEBUG_PLOT_DONE_wpu_plot_done_dly(  int id = 0 );

	Scu_reg getDEBUG_PLOT_DONE(  int id = 0 );

	Scu_reg inDEBUG_PLOT_DONE(  int id = 0 );

	void assignFPGA_SYNC_sync_cnt( Scu_reg value,int id=-1  );
	void assignFPGA_SYNC_debug_scu_reset( Scu_reg value,int id=-1  );
	void assignFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value,int id=-1  );

	void assignFPGA_SYNC( Scu_reg value,int id=-1 );


	void setFPGA_SYNC_sync_cnt( Scu_reg value,int id=-1 );
	void setFPGA_SYNC_debug_scu_reset( Scu_reg value,int id=-1 );
	void setFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value,int id=-1 );

	void setFPGA_SYNC( Scu_reg value,int id=-1 );
	void outFPGA_SYNC_sync_cnt( Scu_reg value,int id=-1 );
	void pulseFPGA_SYNC_sync_cnt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outFPGA_SYNC_debug_scu_reset( Scu_reg value,int id=-1 );
	void pulseFPGA_SYNC_debug_scu_reset( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value,int id=-1 );
	void pulseFPGA_SYNC_debug_sys_rst_cnt( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outFPGA_SYNC( Scu_reg value,int id=-1 );
	void outFPGA_SYNC( int id=-1 );


	Scu_reg getFPGA_SYNC_sync_cnt(  int id = 0 );
	Scu_reg getFPGA_SYNC_debug_scu_reset(  int id = 0 );
	Scu_reg getFPGA_SYNC_debug_sys_rst_cnt(  int id = 0 );

	Scu_reg getFPGA_SYNC(  int id = 0 );

	Scu_reg inFPGA_SYNC(  int id = 0 );

	Scu_reg getWPU_ID_sys_wpu_plot_id(  int id = 0 );
	Scu_reg getWPU_ID_wpu_plot_done_id(  int id = 0 );
	Scu_reg getWPU_ID_wpu_display_done_id(  int id = 0 );

	Scu_reg getWPU_ID(  int id = 0 );

	Scu_reg inWPU_ID(  int id = 0 );

	void assignDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value,int id=-1  );
	void assignDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value,int id=-1  );

	void assignDEBUG_WPU_PLOT( Scu_reg value,int id=-1 );


	void setDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value,int id=-1 );
	void setDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value,int id=-1 );

	void setDEBUG_WPU_PLOT( Scu_reg value,int id=-1 );
	void outDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value,int id=-1 );
	void pulseDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEBUG_WPU_PLOT( Scu_reg value,int id=-1 );
	void outDEBUG_WPU_PLOT( int id=-1 );


	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_display_finish(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_wpu_fast_done_buf(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_wpu_display_done_buf(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_display_id_equ(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_plot_last(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_scu_wave_plot_req(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_start(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_sys_wpu_plot(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_done_buf(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_plot_en(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_cfg_wpu_flush(  int id = 0 );
	Scu_reg getDEBUG_WPU_PLOT_cfg_stop_plot(  int id = 0 );

	Scu_reg getDEBUG_WPU_PLOT(  int id = 0 );

	Scu_reg inDEBUG_WPU_PLOT(  int id = 0 );

	void assignDEBUG_GT_CRC_fail_sum( Scu_reg value,int id=-1  );
	void assignDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value,int id=-1  );
	void assignDEBUG_GT_CRC_crc_valid_i( Scu_reg value,int id=-1  );

	void assignDEBUG_GT_CRC( Scu_reg value,int id=-1 );


	void setDEBUG_GT_CRC_fail_sum( Scu_reg value,int id=-1 );
	void setDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value,int id=-1 );
	void setDEBUG_GT_CRC_crc_valid_i( Scu_reg value,int id=-1 );

	void setDEBUG_GT_CRC( Scu_reg value,int id=-1 );
	void outDEBUG_GT_CRC_fail_sum( Scu_reg value,int id=-1 );
	void pulseDEBUG_GT_CRC_fail_sum( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value,int id=-1 );
	void pulseDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_GT_CRC_crc_valid_i( Scu_reg value,int id=-1 );
	void pulseDEBUG_GT_CRC_crc_valid_i( Scu_reg activeV, Scu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEBUG_GT_CRC( Scu_reg value,int id=-1 );
	void outDEBUG_GT_CRC( int id=-1 );


	Scu_reg getDEBUG_GT_CRC_fail_sum(  int id = 0 );
	Scu_reg getDEBUG_GT_CRC_crc_pass_fail_n_i(  int id = 0 );
	Scu_reg getDEBUG_GT_CRC_crc_valid_i(  int id = 0 );

	Scu_reg getDEBUG_GT_CRC(  int id = 0 );

	Scu_reg inDEBUG_GT_CRC(  int id = 0 );

};
#endif
