#include "tpu.h"
Tpu_reg TpuMemProxy::getversion()
{
	return version;
}


void TpuMemProxy::assignch1_cmp_level_lvl_1_h_a( Tpu_reg value )
{
	mem_assign_field(ch1_cmp_level,lvl_1_h_a,value);
}
void TpuMemProxy::assignch1_cmp_level_lvl_1_l_a( Tpu_reg value )
{
	mem_assign_field(ch1_cmp_level,lvl_1_l_a,value);
}
void TpuMemProxy::assignch1_cmp_level_lvl_1_h_b( Tpu_reg value )
{
	mem_assign_field(ch1_cmp_level,lvl_1_h_b,value);
}
void TpuMemProxy::assignch1_cmp_level_lvl_1_l_b( Tpu_reg value )
{
	mem_assign_field(ch1_cmp_level,lvl_1_l_b,value);
}

void TpuMemProxy::assignch1_cmp_level( Tpu_reg value )
{
	mem_assign_field(ch1_cmp_level,payload,value);
}


Tpu_reg TpuMemProxy::getch1_cmp_level_lvl_1_h_a()
{
	return ch1_cmp_level.lvl_1_h_a;
}
Tpu_reg TpuMemProxy::getch1_cmp_level_lvl_1_l_a()
{
	return ch1_cmp_level.lvl_1_l_a;
}
Tpu_reg TpuMemProxy::getch1_cmp_level_lvl_1_h_b()
{
	return ch1_cmp_level.lvl_1_h_b;
}
Tpu_reg TpuMemProxy::getch1_cmp_level_lvl_1_l_b()
{
	return ch1_cmp_level.lvl_1_l_b;
}

Tpu_reg TpuMemProxy::getch1_cmp_level()
{
	return ch1_cmp_level.payload;
}


void TpuMemProxy::assignch2_cmp_level_lvl_2_h_a( Tpu_reg value )
{
	mem_assign_field(ch2_cmp_level,lvl_2_h_a,value);
}
void TpuMemProxy::assignch2_cmp_level_lvl_2_l_a( Tpu_reg value )
{
	mem_assign_field(ch2_cmp_level,lvl_2_l_a,value);
}
void TpuMemProxy::assignch2_cmp_level_lvl_2_h_b( Tpu_reg value )
{
	mem_assign_field(ch2_cmp_level,lvl_2_h_b,value);
}
void TpuMemProxy::assignch2_cmp_level_lvl_2_l_b( Tpu_reg value )
{
	mem_assign_field(ch2_cmp_level,lvl_2_l_b,value);
}

void TpuMemProxy::assignch2_cmp_level( Tpu_reg value )
{
	mem_assign_field(ch2_cmp_level,payload,value);
}


Tpu_reg TpuMemProxy::getch2_cmp_level_lvl_2_h_a()
{
	return ch2_cmp_level.lvl_2_h_a;
}
Tpu_reg TpuMemProxy::getch2_cmp_level_lvl_2_l_a()
{
	return ch2_cmp_level.lvl_2_l_a;
}
Tpu_reg TpuMemProxy::getch2_cmp_level_lvl_2_h_b()
{
	return ch2_cmp_level.lvl_2_h_b;
}
Tpu_reg TpuMemProxy::getch2_cmp_level_lvl_2_l_b()
{
	return ch2_cmp_level.lvl_2_l_b;
}

Tpu_reg TpuMemProxy::getch2_cmp_level()
{
	return ch2_cmp_level.payload;
}


void TpuMemProxy::assignch3_cmp_level_lvl_3_h_a( Tpu_reg value )
{
	mem_assign_field(ch3_cmp_level,lvl_3_h_a,value);
}
void TpuMemProxy::assignch3_cmp_level_lvl_3_l_a( Tpu_reg value )
{
	mem_assign_field(ch3_cmp_level,lvl_3_l_a,value);
}
void TpuMemProxy::assignch3_cmp_level_lvl_3_h_b( Tpu_reg value )
{
	mem_assign_field(ch3_cmp_level,lvl_3_h_b,value);
}
void TpuMemProxy::assignch3_cmp_level_lvl_3_l_b( Tpu_reg value )
{
	mem_assign_field(ch3_cmp_level,lvl_3_l_b,value);
}

void TpuMemProxy::assignch3_cmp_level( Tpu_reg value )
{
	mem_assign_field(ch3_cmp_level,payload,value);
}


Tpu_reg TpuMemProxy::getch3_cmp_level_lvl_3_h_a()
{
	return ch3_cmp_level.lvl_3_h_a;
}
Tpu_reg TpuMemProxy::getch3_cmp_level_lvl_3_l_a()
{
	return ch3_cmp_level.lvl_3_l_a;
}
Tpu_reg TpuMemProxy::getch3_cmp_level_lvl_3_h_b()
{
	return ch3_cmp_level.lvl_3_h_b;
}
Tpu_reg TpuMemProxy::getch3_cmp_level_lvl_3_l_b()
{
	return ch3_cmp_level.lvl_3_l_b;
}

Tpu_reg TpuMemProxy::getch3_cmp_level()
{
	return ch3_cmp_level.payload;
}


void TpuMemProxy::assignch4_cmp_level_lvl_4_h_a( Tpu_reg value )
{
	mem_assign_field(ch4_cmp_level,lvl_4_h_a,value);
}
void TpuMemProxy::assignch4_cmp_level_lvl_4_l_a( Tpu_reg value )
{
	mem_assign_field(ch4_cmp_level,lvl_4_l_a,value);
}
void TpuMemProxy::assignch4_cmp_level_lvl_4_h_b( Tpu_reg value )
{
	mem_assign_field(ch4_cmp_level,lvl_4_h_b,value);
}
void TpuMemProxy::assignch4_cmp_level_lvl_4_l_b( Tpu_reg value )
{
	mem_assign_field(ch4_cmp_level,lvl_4_l_b,value);
}

void TpuMemProxy::assignch4_cmp_level( Tpu_reg value )
{
	mem_assign_field(ch4_cmp_level,payload,value);
}


Tpu_reg TpuMemProxy::getch4_cmp_level_lvl_4_h_a()
{
	return ch4_cmp_level.lvl_4_h_a;
}
Tpu_reg TpuMemProxy::getch4_cmp_level_lvl_4_l_a()
{
	return ch4_cmp_level.lvl_4_l_a;
}
Tpu_reg TpuMemProxy::getch4_cmp_level_lvl_4_h_b()
{
	return ch4_cmp_level.lvl_4_h_b;
}
Tpu_reg TpuMemProxy::getch4_cmp_level_lvl_4_l_b()
{
	return ch4_cmp_level.lvl_4_l_b;
}

Tpu_reg TpuMemProxy::getch4_cmp_level()
{
	return ch4_cmp_level.payload;
}


void TpuMemProxy::assignA_event_reg1_A_event_logic_la_sel( Tpu_reg value )
{
	mem_assign_field(A_event_reg1,A_event_logic_la_sel,value);
}

void TpuMemProxy::assignA_event_reg1( Tpu_reg value )
{
	mem_assign_field(A_event_reg1,payload,value);
}


Tpu_reg TpuMemProxy::getA_event_reg1_A_event_logic_la_sel()
{
	return A_event_reg1.A_event_logic_la_sel;
}

Tpu_reg TpuMemProxy::getA_event_reg1()
{
	return A_event_reg1.payload;
}


void TpuMemProxy::assignA_event_reg2_A_event_logic_analog_sel( Tpu_reg value )
{
	mem_assign_field(A_event_reg2,A_event_logic_analog_sel,value);
}
void TpuMemProxy::assignA_event_reg2_A_event_logic_func( Tpu_reg value )
{
	mem_assign_field(A_event_reg2,A_event_logic_func,value);
}

void TpuMemProxy::assignA_event_reg2( Tpu_reg value )
{
	mem_assign_field(A_event_reg2,payload,value);
}


Tpu_reg TpuMemProxy::getA_event_reg2_A_event_logic_analog_sel()
{
	return A_event_reg2.A_event_logic_analog_sel;
}
Tpu_reg TpuMemProxy::getA_event_reg2_A_event_logic_func()
{
	return A_event_reg2.A_event_logic_func;
}

Tpu_reg TpuMemProxy::getA_event_reg2()
{
	return A_event_reg2.payload;
}


void TpuMemProxy::assignA_event_reg3_A_event_cmpa_sel( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_cmpa_sel,value);
}
void TpuMemProxy::assignA_event_reg3_A_event_cmpa_mode( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_cmpa_mode,value);
}
void TpuMemProxy::assignA_event_reg3_A_event_cmpb_sel( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_cmpb_sel,value);
}
void TpuMemProxy::assignA_event_reg3_A_event_cmpb_mode( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_cmpb_mode,value);
}
void TpuMemProxy::assignA_event_reg3_A_event_clk_sel( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_clk_sel,value);
}
void TpuMemProxy::assignA_event_reg3_A_event_clk_sel_inv( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_clk_sel_inv,value);
}
void TpuMemProxy::assignA_event_reg3_A_event_dat_sel( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_dat_sel,value);
}
void TpuMemProxy::assignA_event_reg3_A_event_dat_sel_inv( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,A_event_dat_sel_inv,value);
}

void TpuMemProxy::assignA_event_reg3( Tpu_reg value )
{
	mem_assign_field(A_event_reg3,payload,value);
}


Tpu_reg TpuMemProxy::getA_event_reg3_A_event_cmpa_sel()
{
	return A_event_reg3.A_event_cmpa_sel;
}
Tpu_reg TpuMemProxy::getA_event_reg3_A_event_cmpa_mode()
{
	return A_event_reg3.A_event_cmpa_mode;
}
Tpu_reg TpuMemProxy::getA_event_reg3_A_event_cmpb_sel()
{
	return A_event_reg3.A_event_cmpb_sel;
}
Tpu_reg TpuMemProxy::getA_event_reg3_A_event_cmpb_mode()
{
	return A_event_reg3.A_event_cmpb_mode;
}
Tpu_reg TpuMemProxy::getA_event_reg3_A_event_clk_sel()
{
	return A_event_reg3.A_event_clk_sel;
}
Tpu_reg TpuMemProxy::getA_event_reg3_A_event_clk_sel_inv()
{
	return A_event_reg3.A_event_clk_sel_inv;
}
Tpu_reg TpuMemProxy::getA_event_reg3_A_event_dat_sel()
{
	return A_event_reg3.A_event_dat_sel;
}
Tpu_reg TpuMemProxy::getA_event_reg3_A_event_dat_sel_inv()
{
	return A_event_reg3.A_event_dat_sel_inv;
}

Tpu_reg TpuMemProxy::getA_event_reg3()
{
	return A_event_reg3.payload;
}


void TpuMemProxy::assignA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg value )
{
	mem_assign_field(A_event_reg4,A_event_width_cmp_1_31_0,value);
}

void TpuMemProxy::assignA_event_reg4( Tpu_reg value )
{
	mem_assign_field(A_event_reg4,payload,value);
}


Tpu_reg TpuMemProxy::getA_event_reg4_A_event_width_cmp_1_31_0()
{
	return A_event_reg4.A_event_width_cmp_1_31_0;
}

Tpu_reg TpuMemProxy::getA_event_reg4()
{
	return A_event_reg4.payload;
}


void TpuMemProxy::assignA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg value )
{
	mem_assign_field(A_event_reg5,A_event_width_cmp_g_31_0,value);
}

void TpuMemProxy::assignA_event_reg5( Tpu_reg value )
{
	mem_assign_field(A_event_reg5,payload,value);
}


Tpu_reg TpuMemProxy::getA_event_reg5_A_event_width_cmp_g_31_0()
{
	return A_event_reg5.A_event_width_cmp_g_31_0;
}

Tpu_reg TpuMemProxy::getA_event_reg5()
{
	return A_event_reg5.payload;
}


void TpuMemProxy::assignA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg value )
{
	mem_assign_field(A_event_reg6,A_event_width_cmp_1_39_32,value);
}
void TpuMemProxy::assignA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg value )
{
	mem_assign_field(A_event_reg6,A_event_width_cmp_g_39_32,value);
}
void TpuMemProxy::assignA_event_reg6_A_event_width_cmp_mode( Tpu_reg value )
{
	mem_assign_field(A_event_reg6,A_event_width_cmp_mode,value);
}

void TpuMemProxy::assignA_event_reg6( Tpu_reg value )
{
	mem_assign_field(A_event_reg6,payload,value);
}


Tpu_reg TpuMemProxy::getA_event_reg6_A_event_width_cmp_1_39_32()
{
	return A_event_reg6.A_event_width_cmp_1_39_32;
}
Tpu_reg TpuMemProxy::getA_event_reg6_A_event_width_cmp_g_39_32()
{
	return A_event_reg6.A_event_width_cmp_g_39_32;
}
Tpu_reg TpuMemProxy::getA_event_reg6_A_event_width_cmp_mode()
{
	return A_event_reg6.A_event_width_cmp_mode;
}

Tpu_reg TpuMemProxy::getA_event_reg6()
{
	return A_event_reg6.payload;
}


void TpuMemProxy::assignB_event_reg1_B_event_logic_la_sel( Tpu_reg value )
{
	mem_assign_field(B_event_reg1,B_event_logic_la_sel,value);
}

void TpuMemProxy::assignB_event_reg1( Tpu_reg value )
{
	mem_assign_field(B_event_reg1,payload,value);
}


Tpu_reg TpuMemProxy::getB_event_reg1_B_event_logic_la_sel()
{
	return B_event_reg1.B_event_logic_la_sel;
}

Tpu_reg TpuMemProxy::getB_event_reg1()
{
	return B_event_reg1.payload;
}


void TpuMemProxy::assignB_event_reg2_B_event_logic_analog_sel( Tpu_reg value )
{
	mem_assign_field(B_event_reg2,B_event_logic_analog_sel,value);
}
void TpuMemProxy::assignB_event_reg2_B_event_logic_func( Tpu_reg value )
{
	mem_assign_field(B_event_reg2,B_event_logic_func,value);
}

void TpuMemProxy::assignB_event_reg2( Tpu_reg value )
{
	mem_assign_field(B_event_reg2,payload,value);
}


Tpu_reg TpuMemProxy::getB_event_reg2_B_event_logic_analog_sel()
{
	return B_event_reg2.B_event_logic_analog_sel;
}
Tpu_reg TpuMemProxy::getB_event_reg2_B_event_logic_func()
{
	return B_event_reg2.B_event_logic_func;
}

Tpu_reg TpuMemProxy::getB_event_reg2()
{
	return B_event_reg2.payload;
}


void TpuMemProxy::assignB_event_reg3_B_event_cmpa_sel( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_cmpa_sel,value);
}
void TpuMemProxy::assignB_event_reg3_B_event_cmpa_mode( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_cmpa_mode,value);
}
void TpuMemProxy::assignB_event_reg3_B_event_cmpb_sel( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_cmpb_sel,value);
}
void TpuMemProxy::assignB_event_reg3_B_event_cmpb_mode( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_cmpb_mode,value);
}
void TpuMemProxy::assignB_event_reg3_B_event_clk_sel( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_clk_sel,value);
}
void TpuMemProxy::assignB_event_reg3_B_event_clk_sel_inv( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_clk_sel_inv,value);
}
void TpuMemProxy::assignB_event_reg3_B_event_dat_sel( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_dat_sel,value);
}
void TpuMemProxy::assignB_event_reg3_B_event_dat_sel_inv( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,B_event_dat_sel_inv,value);
}

void TpuMemProxy::assignB_event_reg3( Tpu_reg value )
{
	mem_assign_field(B_event_reg3,payload,value);
}


Tpu_reg TpuMemProxy::getB_event_reg3_B_event_cmpa_sel()
{
	return B_event_reg3.B_event_cmpa_sel;
}
Tpu_reg TpuMemProxy::getB_event_reg3_B_event_cmpa_mode()
{
	return B_event_reg3.B_event_cmpa_mode;
}
Tpu_reg TpuMemProxy::getB_event_reg3_B_event_cmpb_sel()
{
	return B_event_reg3.B_event_cmpb_sel;
}
Tpu_reg TpuMemProxy::getB_event_reg3_B_event_cmpb_mode()
{
	return B_event_reg3.B_event_cmpb_mode;
}
Tpu_reg TpuMemProxy::getB_event_reg3_B_event_clk_sel()
{
	return B_event_reg3.B_event_clk_sel;
}
Tpu_reg TpuMemProxy::getB_event_reg3_B_event_clk_sel_inv()
{
	return B_event_reg3.B_event_clk_sel_inv;
}
Tpu_reg TpuMemProxy::getB_event_reg3_B_event_dat_sel()
{
	return B_event_reg3.B_event_dat_sel;
}
Tpu_reg TpuMemProxy::getB_event_reg3_B_event_dat_sel_inv()
{
	return B_event_reg3.B_event_dat_sel_inv;
}

Tpu_reg TpuMemProxy::getB_event_reg3()
{
	return B_event_reg3.payload;
}


void TpuMemProxy::assignB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg value )
{
	mem_assign_field(B_event_reg4,B_event_width_cmp_1_31_0,value);
}

void TpuMemProxy::assignB_event_reg4( Tpu_reg value )
{
	mem_assign_field(B_event_reg4,payload,value);
}


Tpu_reg TpuMemProxy::getB_event_reg4_B_event_width_cmp_1_31_0()
{
	return B_event_reg4.B_event_width_cmp_1_31_0;
}

Tpu_reg TpuMemProxy::getB_event_reg4()
{
	return B_event_reg4.payload;
}


void TpuMemProxy::assignB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg value )
{
	mem_assign_field(B_event_reg5,B_event_width_cmp_g_31_0,value);
}

void TpuMemProxy::assignB_event_reg5( Tpu_reg value )
{
	mem_assign_field(B_event_reg5,payload,value);
}


Tpu_reg TpuMemProxy::getB_event_reg5_B_event_width_cmp_g_31_0()
{
	return B_event_reg5.B_event_width_cmp_g_31_0;
}

Tpu_reg TpuMemProxy::getB_event_reg5()
{
	return B_event_reg5.payload;
}


void TpuMemProxy::assignB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg value )
{
	mem_assign_field(B_event_reg6,B_event_width_cmp_1_39_32,value);
}
void TpuMemProxy::assignB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg value )
{
	mem_assign_field(B_event_reg6,B_event_width_cmp_g_39_32,value);
}
void TpuMemProxy::assignB_event_reg6_B_event_width_cmp_mode( Tpu_reg value )
{
	mem_assign_field(B_event_reg6,B_event_width_cmp_mode,value);
}

void TpuMemProxy::assignB_event_reg6( Tpu_reg value )
{
	mem_assign_field(B_event_reg6,payload,value);
}


Tpu_reg TpuMemProxy::getB_event_reg6_B_event_width_cmp_1_39_32()
{
	return B_event_reg6.B_event_width_cmp_1_39_32;
}
Tpu_reg TpuMemProxy::getB_event_reg6_B_event_width_cmp_g_39_32()
{
	return B_event_reg6.B_event_width_cmp_g_39_32;
}
Tpu_reg TpuMemProxy::getB_event_reg6_B_event_width_cmp_mode()
{
	return B_event_reg6.B_event_width_cmp_mode;
}

Tpu_reg TpuMemProxy::getB_event_reg6()
{
	return B_event_reg6.payload;
}


void TpuMemProxy::assignR_event_reg1_R_event_logic_la_sel( Tpu_reg value )
{
	mem_assign_field(R_event_reg1,R_event_logic_la_sel,value);
}

void TpuMemProxy::assignR_event_reg1( Tpu_reg value )
{
	mem_assign_field(R_event_reg1,payload,value);
}


Tpu_reg TpuMemProxy::getR_event_reg1_R_event_logic_la_sel()
{
	return R_event_reg1.R_event_logic_la_sel;
}

Tpu_reg TpuMemProxy::getR_event_reg1()
{
	return R_event_reg1.payload;
}


void TpuMemProxy::assignR_event_reg2_R_event_logic_analog_sel( Tpu_reg value )
{
	mem_assign_field(R_event_reg2,R_event_logic_analog_sel,value);
}
void TpuMemProxy::assignR_event_reg2_R_event_logic_func( Tpu_reg value )
{
	mem_assign_field(R_event_reg2,R_event_logic_func,value);
}

void TpuMemProxy::assignR_event_reg2( Tpu_reg value )
{
	mem_assign_field(R_event_reg2,payload,value);
}


Tpu_reg TpuMemProxy::getR_event_reg2_R_event_logic_analog_sel()
{
	return R_event_reg2.R_event_logic_analog_sel;
}
Tpu_reg TpuMemProxy::getR_event_reg2_R_event_logic_func()
{
	return R_event_reg2.R_event_logic_func;
}

Tpu_reg TpuMemProxy::getR_event_reg2()
{
	return R_event_reg2.payload;
}


void TpuMemProxy::assignR_event_reg3_R_event_cmpa_sel( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_cmpa_sel,value);
}
void TpuMemProxy::assignR_event_reg3_R_event_cmpa_mode( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_cmpa_mode,value);
}
void TpuMemProxy::assignR_event_reg3_R_event_cmpb_sel( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_cmpb_sel,value);
}
void TpuMemProxy::assignR_event_reg3_R_event_cmpb_mode( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_cmpb_mode,value);
}
void TpuMemProxy::assignR_event_reg3_R_event_clk_sel( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_clk_sel,value);
}
void TpuMemProxy::assignR_event_reg3_R_event_clk_sel_inv( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_clk_sel_inv,value);
}
void TpuMemProxy::assignR_event_reg3_R_event_dat_sel( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_dat_sel,value);
}
void TpuMemProxy::assignR_event_reg3_R_event_dat_sel_inv( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,R_event_dat_sel_inv,value);
}

void TpuMemProxy::assignR_event_reg3( Tpu_reg value )
{
	mem_assign_field(R_event_reg3,payload,value);
}


Tpu_reg TpuMemProxy::getR_event_reg3_R_event_cmpa_sel()
{
	return R_event_reg3.R_event_cmpa_sel;
}
Tpu_reg TpuMemProxy::getR_event_reg3_R_event_cmpa_mode()
{
	return R_event_reg3.R_event_cmpa_mode;
}
Tpu_reg TpuMemProxy::getR_event_reg3_R_event_cmpb_sel()
{
	return R_event_reg3.R_event_cmpb_sel;
}
Tpu_reg TpuMemProxy::getR_event_reg3_R_event_cmpb_mode()
{
	return R_event_reg3.R_event_cmpb_mode;
}
Tpu_reg TpuMemProxy::getR_event_reg3_R_event_clk_sel()
{
	return R_event_reg3.R_event_clk_sel;
}
Tpu_reg TpuMemProxy::getR_event_reg3_R_event_clk_sel_inv()
{
	return R_event_reg3.R_event_clk_sel_inv;
}
Tpu_reg TpuMemProxy::getR_event_reg3_R_event_dat_sel()
{
	return R_event_reg3.R_event_dat_sel;
}
Tpu_reg TpuMemProxy::getR_event_reg3_R_event_dat_sel_inv()
{
	return R_event_reg3.R_event_dat_sel_inv;
}

Tpu_reg TpuMemProxy::getR_event_reg3()
{
	return R_event_reg3.payload;
}


void TpuMemProxy::assignR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg value )
{
	mem_assign_field(R_event_reg4,R_event_width_cmp_1_31_0,value);
}

void TpuMemProxy::assignR_event_reg4( Tpu_reg value )
{
	mem_assign_field(R_event_reg4,payload,value);
}


Tpu_reg TpuMemProxy::getR_event_reg4_R_event_width_cmp_1_31_0()
{
	return R_event_reg4.R_event_width_cmp_1_31_0;
}

Tpu_reg TpuMemProxy::getR_event_reg4()
{
	return R_event_reg4.payload;
}


void TpuMemProxy::assignR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg value )
{
	mem_assign_field(R_event_reg5,R_event_width_cmp_g_31_0,value);
}

void TpuMemProxy::assignR_event_reg5( Tpu_reg value )
{
	mem_assign_field(R_event_reg5,payload,value);
}


Tpu_reg TpuMemProxy::getR_event_reg5_R_event_width_cmp_g_31_0()
{
	return R_event_reg5.R_event_width_cmp_g_31_0;
}

Tpu_reg TpuMemProxy::getR_event_reg5()
{
	return R_event_reg5.payload;
}


void TpuMemProxy::assignR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg value )
{
	mem_assign_field(R_event_reg6,R_event_width_cmp_1_39_32,value);
}
void TpuMemProxy::assignR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg value )
{
	mem_assign_field(R_event_reg6,R_event_width_cmp_g_39_32,value);
}
void TpuMemProxy::assignR_event_reg6_R_event_width_cmp_mode( Tpu_reg value )
{
	mem_assign_field(R_event_reg6,R_event_width_cmp_mode,value);
}

void TpuMemProxy::assignR_event_reg6( Tpu_reg value )
{
	mem_assign_field(R_event_reg6,payload,value);
}


Tpu_reg TpuMemProxy::getR_event_reg6_R_event_width_cmp_1_39_32()
{
	return R_event_reg6.R_event_width_cmp_1_39_32;
}
Tpu_reg TpuMemProxy::getR_event_reg6_R_event_width_cmp_g_39_32()
{
	return R_event_reg6.R_event_width_cmp_g_39_32;
}
Tpu_reg TpuMemProxy::getR_event_reg6_R_event_width_cmp_mode()
{
	return R_event_reg6.R_event_width_cmp_mode;
}

Tpu_reg TpuMemProxy::getR_event_reg6()
{
	return R_event_reg6.payload;
}


void TpuMemProxy::assigntpu_trig_mux_tpu_trig_type( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,tpu_trig_type,value);
}
void TpuMemProxy::assigntpu_trig_mux_trig_rst( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,trig_rst,value);
}
void TpuMemProxy::assigntpu_trig_mux_force_trig( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,force_trig,value);
}
void TpuMemProxy::assigntpu_trig_mux_abr_seq_loop_en( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,abr_seq_loop_en,value);
}
void TpuMemProxy::assigntpu_trig_mux_abr_seq_delay_on( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,abr_seq_delay_on,value);
}
void TpuMemProxy::assigntpu_trig_mux_abr_seq_timeout_on( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,abr_seq_timeout_on,value);
}
void TpuMemProxy::assigntpu_trig_mux_abr_seq_r_event_on( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,abr_seq_r_event_on,value);
}
void TpuMemProxy::assigntpu_trig_mux_seq_abr_trig_a_only( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,seq_abr_trig_a_only,value);
}

void TpuMemProxy::assigntpu_trig_mux( Tpu_reg value )
{
	mem_assign_field(tpu_trig_mux,payload,value);
}


Tpu_reg TpuMemProxy::gettpu_trig_mux_tpu_trig_type()
{
	return tpu_trig_mux.tpu_trig_type;
}
Tpu_reg TpuMemProxy::gettpu_trig_mux_trig_rst()
{
	return tpu_trig_mux.trig_rst;
}
Tpu_reg TpuMemProxy::gettpu_trig_mux_force_trig()
{
	return tpu_trig_mux.force_trig;
}
Tpu_reg TpuMemProxy::gettpu_trig_mux_abr_seq_loop_en()
{
	return tpu_trig_mux.abr_seq_loop_en;
}
Tpu_reg TpuMemProxy::gettpu_trig_mux_abr_seq_delay_on()
{
	return tpu_trig_mux.abr_seq_delay_on;
}
Tpu_reg TpuMemProxy::gettpu_trig_mux_abr_seq_timeout_on()
{
	return tpu_trig_mux.abr_seq_timeout_on;
}
Tpu_reg TpuMemProxy::gettpu_trig_mux_abr_seq_r_event_on()
{
	return tpu_trig_mux.abr_seq_r_event_on;
}
Tpu_reg TpuMemProxy::gettpu_trig_mux_seq_abr_trig_a_only()
{
	return tpu_trig_mux.seq_abr_trig_a_only;
}

Tpu_reg TpuMemProxy::gettpu_trig_mux()
{
	return tpu_trig_mux.payload;
}


void TpuMemProxy::assignabr_delay_time_l_abr_delay_time_l( Tpu_reg value )
{
	mem_assign_field(abr_delay_time_l,abr_delay_time_l,value);
}

void TpuMemProxy::assignabr_delay_time_l( Tpu_reg value )
{
	mem_assign_field(abr_delay_time_l,payload,value);
}


void TpuMemProxy::assignabr_delay_time_h_abr_delay_time_h( Tpu_reg value )
{
	mem_assign_field(abr_delay_time_h,abr_delay_time_h,value);
}

void TpuMemProxy::assignabr_delay_time_h( Tpu_reg value )
{
	mem_assign_field(abr_delay_time_h,payload,value);
}


void TpuMemProxy::assignabr_timeout_l_abr_timeout_l( Tpu_reg value )
{
	mem_assign_field(abr_timeout_l,abr_timeout_l,value);
}

void TpuMemProxy::assignabr_timeout_l( Tpu_reg value )
{
	mem_assign_field(abr_timeout_l,payload,value);
}


void TpuMemProxy::assignabr_timeout_h_abr_timeout_h( Tpu_reg value )
{
	mem_assign_field(abr_timeout_h,abr_timeout_h,value);
}

void TpuMemProxy::assignabr_timeout_h( Tpu_reg value )
{
	mem_assign_field(abr_timeout_h,payload,value);
}


void TpuMemProxy::assignabr_b_event_num( Tpu_reg value )
{
	mem_assign(abr_b_event_num,value);
}


void TpuMemProxy::assignpreprocess_type( Tpu_reg value )
{
	mem_assign(preprocess_type,value);
}


void TpuMemProxy::assignabr_a_event_num( Tpu_reg value )
{
	mem_assign(abr_a_event_num,value);
}


void TpuMemProxy::assignholdoff_event_num( Tpu_reg value )
{
	mem_assign(holdoff_event_num,value);
}


Tpu_reg TpuMemProxy::getholdoff_event_num()
{
	return holdoff_event_num;
}


void TpuMemProxy::assignholdoff_time_l_holdoff_time_l( Tpu_reg value )
{
	mem_assign_field(holdoff_time_l,holdoff_time_l,value);
}

void TpuMemProxy::assignholdoff_time_l( Tpu_reg value )
{
	mem_assign_field(holdoff_time_l,payload,value);
}


Tpu_reg TpuMemProxy::getholdoff_time_l_holdoff_time_l()
{
	return holdoff_time_l.holdoff_time_l;
}

Tpu_reg TpuMemProxy::getholdoff_time_l()
{
	return holdoff_time_l.payload;
}


void TpuMemProxy::assignholdoff_time_h_holdoff_time_h( Tpu_reg value )
{
	mem_assign_field(holdoff_time_h,holdoff_time_h,value);
}
void TpuMemProxy::assignholdoff_time_h_holdoff_type( Tpu_reg value )
{
	mem_assign_field(holdoff_time_h,holdoff_type,value);
}

void TpuMemProxy::assignholdoff_time_h( Tpu_reg value )
{
	mem_assign_field(holdoff_time_h,payload,value);
}


Tpu_reg TpuMemProxy::getholdoff_time_h_holdoff_time_h()
{
	return holdoff_time_h.holdoff_time_h;
}
Tpu_reg TpuMemProxy::getholdoff_time_h_holdoff_type()
{
	return holdoff_time_h.holdoff_type;
}

Tpu_reg TpuMemProxy::getholdoff_time_h()
{
	return holdoff_time_h.payload;
}


void TpuMemProxy::assigniir_set_iir_type( Tpu_reg value )
{
	mem_assign_field(iir_set,iir_type,value);
}
void TpuMemProxy::assigniir_set_iir_src( Tpu_reg value )
{
	mem_assign_field(iir_set,iir_src,value);
}
void TpuMemProxy::assigniir_set_iir_offset( Tpu_reg value )
{
	mem_assign_field(iir_set,iir_offset,value);
}

void TpuMemProxy::assigniir_set( Tpu_reg value )
{
	mem_assign_field(iir_set,payload,value);
}


Tpu_reg TpuMemProxy::getiir_set_iir_type()
{
	return iir_set.iir_type;
}
Tpu_reg TpuMemProxy::getiir_set_iir_src()
{
	return iir_set.iir_src;
}
Tpu_reg TpuMemProxy::getiir_set_iir_offset()
{
	return iir_set.iir_offset;
}

Tpu_reg TpuMemProxy::getiir_set()
{
	return iir_set.payload;
}


void TpuMemProxy::assignuart_cfg_1_uart_clk_div( Tpu_reg value )
{
	mem_assign_field(uart_cfg_1,uart_clk_div,value);
}
void TpuMemProxy::assignuart_cfg_1_uart_trig_type( Tpu_reg value )
{
	mem_assign_field(uart_cfg_1,uart_trig_type,value);
}
void TpuMemProxy::assignuart_cfg_1_uart_trig_dat( Tpu_reg value )
{
	mem_assign_field(uart_cfg_1,uart_trig_dat,value);
}
void TpuMemProxy::assignuart_cfg_1_uart_trig_err( Tpu_reg value )
{
	mem_assign_field(uart_cfg_1,uart_trig_err,value);
}
void TpuMemProxy::assignuart_cfg_1_uart_eof( Tpu_reg value )
{
	mem_assign_field(uart_cfg_1,uart_eof,value);
}
void TpuMemProxy::assignuart_cfg_1_uart_parity( Tpu_reg value )
{
	mem_assign_field(uart_cfg_1,uart_parity,value);
}

void TpuMemProxy::assignuart_cfg_1( Tpu_reg value )
{
	mem_assign_field(uart_cfg_1,payload,value);
}


void TpuMemProxy::assignuart_cfg_2_uart_len( Tpu_reg value )
{
	mem_assign_field(uart_cfg_2,uart_len,value);
}
void TpuMemProxy::assignuart_cfg_2_uart_dat_min( Tpu_reg value )
{
	mem_assign_field(uart_cfg_2,uart_dat_min,value);
}
void TpuMemProxy::assignuart_cfg_2_uart_dat_max( Tpu_reg value )
{
	mem_assign_field(uart_cfg_2,uart_dat_max,value);
}

void TpuMemProxy::assignuart_cfg_2( Tpu_reg value )
{
	mem_assign_field(uart_cfg_2,payload,value);
}


void TpuMemProxy::assigniis_cfg_1_iis_trig_type( Tpu_reg value )
{
	mem_assign_field(iis_cfg_1,iis_trig_type,value);
}
void TpuMemProxy::assigniis_cfg_1_iis_trig_channel_sel( Tpu_reg value )
{
	mem_assign_field(iis_cfg_1,iis_trig_channel_sel,value);
}
void TpuMemProxy::assigniis_cfg_1_iis_trig_bus_type( Tpu_reg value )
{
	mem_assign_field(iis_cfg_1,iis_trig_bus_type,value);
}
void TpuMemProxy::assigniis_cfg_1_iis_trig_bitnum_vld( Tpu_reg value )
{
	mem_assign_field(iis_cfg_1,iis_trig_bitnum_vld,value);
}
void TpuMemProxy::assigniis_cfg_1_iis_trig_bitnum_total( Tpu_reg value )
{
	mem_assign_field(iis_cfg_1,iis_trig_bitnum_total,value);
}

void TpuMemProxy::assigniis_cfg_1( Tpu_reg value )
{
	mem_assign_field(iis_cfg_1,payload,value);
}


void TpuMemProxy::assigniis_cfg_2_iis_trig_word_min( Tpu_reg value )
{
	mem_assign_field(iis_cfg_2,iis_trig_word_min,value);
}

void TpuMemProxy::assigniis_cfg_2( Tpu_reg value )
{
	mem_assign_field(iis_cfg_2,payload,value);
}


void TpuMemProxy::assigniis_cfg_3_iis_trig_word_max( Tpu_reg value )
{
	mem_assign_field(iis_cfg_3,iis_trig_word_max,value);
}

void TpuMemProxy::assigniis_cfg_3( Tpu_reg value )
{
	mem_assign_field(iis_cfg_3,payload,value);
}


void TpuMemProxy::assigniis_cfg_4_iis_trig_word_mask( Tpu_reg value )
{
	mem_assign_field(iis_cfg_4,iis_trig_word_mask,value);
}

void TpuMemProxy::assigniis_cfg_4( Tpu_reg value )
{
	mem_assign_field(iis_cfg_4,payload,value);
}


void TpuMemProxy::assignlin_cfg_1_lin_trig_type( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_type,value);
}
void TpuMemProxy::assignlin_cfg_1_lin_trig_type_id( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_type_id,value);
}
void TpuMemProxy::assignlin_cfg_1_lin_trig_type_dat( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_type_dat,value);
}
void TpuMemProxy::assignlin_cfg_1_lin_trig_type_err( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_type_err,value);
}
void TpuMemProxy::assignlin_cfg_1_lin_trig_spec( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_spec,value);
}
void TpuMemProxy::assignlin_cfg_1_lin_trig_id_min( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_id_min,value);
}
void TpuMemProxy::assignlin_cfg_1_lin_trig_id_max( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_id_max,value);
}
void TpuMemProxy::assignlin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,lin_trig_dat_cmpnum,value);
}

void TpuMemProxy::assignlin_cfg_1( Tpu_reg value )
{
	mem_assign_field(lin_cfg_1,payload,value);
}


void TpuMemProxy::assignlin_cfg_2_lin_trig_clk_div( Tpu_reg value )
{
	mem_assign_field(lin_cfg_2,lin_trig_clk_div,value);
}

void TpuMemProxy::assignlin_cfg_2( Tpu_reg value )
{
	mem_assign_field(lin_cfg_2,payload,value);
}


void TpuMemProxy::assignlin_cfg_3_lin_trig_sa_pos( Tpu_reg value )
{
	mem_assign_field(lin_cfg_3,lin_trig_sa_pos,value);
}

void TpuMemProxy::assignlin_cfg_3( Tpu_reg value )
{
	mem_assign_field(lin_cfg_3,payload,value);
}


void TpuMemProxy::assignlin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg value )
{
	mem_assign_field(lin_cfg_4,lin_trig_dat_min_bits31_0,value);
}

void TpuMemProxy::assignlin_cfg_4( Tpu_reg value )
{
	mem_assign_field(lin_cfg_4,payload,value);
}


void TpuMemProxy::assignlin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg value )
{
	mem_assign_field(lin_cfg_5,lin_trig_dat_min_bits63_32,value);
}

void TpuMemProxy::assignlin_cfg_5( Tpu_reg value )
{
	mem_assign_field(lin_cfg_5,payload,value);
}


void TpuMemProxy::assignlin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg value )
{
	mem_assign_field(lin_cfg_6,lin_trig_dat_max_bits31_0,value);
}

void TpuMemProxy::assignlin_cfg_6( Tpu_reg value )
{
	mem_assign_field(lin_cfg_6,payload,value);
}


void TpuMemProxy::assignlin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg value )
{
	mem_assign_field(lin_cfg_7,lin_trig_dat_max_bits63_32,value);
}

void TpuMemProxy::assignlin_cfg_7( Tpu_reg value )
{
	mem_assign_field(lin_cfg_7,payload,value);
}


void TpuMemProxy::assignlin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg value )
{
	mem_assign_field(lin_cfg_8,lin_trig_dat_mask_bits31_0,value);
}

void TpuMemProxy::assignlin_cfg_8( Tpu_reg value )
{
	mem_assign_field(lin_cfg_8,payload,value);
}


void TpuMemProxy::assignlin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg value )
{
	mem_assign_field(lin_cfg_9,lin_trig_dat_maak_bits63_32,value);
}

void TpuMemProxy::assignlin_cfg_9( Tpu_reg value )
{
	mem_assign_field(lin_cfg_9,payload,value);
}


void TpuMemProxy::assigncan_cfg_1_can_trig_type( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_type_field( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type_field,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_type_frame( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type_frame,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_type_err( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type_err,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_type_id( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type_id,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_type_id_mask( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type_id_mask,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_type_dat( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type_dat,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_type_dat_mask( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_type_dat_mask,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_bus_type( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_bus_type,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_std_extend_sel( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_std_extend_sel,value);
}
void TpuMemProxy::assigncan_cfg_1_can_trig_dat_cmpnum( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,can_trig_dat_cmpnum,value);
}

void TpuMemProxy::assigncan_cfg_1( Tpu_reg value )
{
	mem_assign_field(can_cfg_1,payload,value);
}


void TpuMemProxy::assigncan_cfg_2_can_trig_clk_div( Tpu_reg value )
{
	mem_assign_field(can_cfg_2,can_trig_clk_div,value);
}
void TpuMemProxy::assigncan_cfg_2_can_trig_sa_pos( Tpu_reg value )
{
	mem_assign_field(can_cfg_2,can_trig_sa_pos,value);
}

void TpuMemProxy::assigncan_cfg_2( Tpu_reg value )
{
	mem_assign_field(can_cfg_2,payload,value);
}


void TpuMemProxy::assigncan_cfg_3_can_trig_id_min( Tpu_reg value )
{
	mem_assign_field(can_cfg_3,can_trig_id_min,value);
}

void TpuMemProxy::assigncan_cfg_3( Tpu_reg value )
{
	mem_assign_field(can_cfg_3,payload,value);
}


void TpuMemProxy::assigncan_cfg_4_can_trig_id_max( Tpu_reg value )
{
	mem_assign_field(can_cfg_4,can_trig_id_max,value);
}

void TpuMemProxy::assigncan_cfg_4( Tpu_reg value )
{
	mem_assign_field(can_cfg_4,payload,value);
}


void TpuMemProxy::assigncan_cfg_5_can_trig_id_mask( Tpu_reg value )
{
	mem_assign_field(can_cfg_5,can_trig_id_mask,value);
}

void TpuMemProxy::assigncan_cfg_5( Tpu_reg value )
{
	mem_assign_field(can_cfg_5,payload,value);
}


void TpuMemProxy::assigncan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg value )
{
	mem_assign_field(can_cfg_6,can_trig_dat_min_bits31_0,value);
}

void TpuMemProxy::assigncan_cfg_6( Tpu_reg value )
{
	mem_assign_field(can_cfg_6,payload,value);
}


void TpuMemProxy::assigncan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg value )
{
	mem_assign_field(can_cfg_7,can_trig_dat_min_bits63_32,value);
}

void TpuMemProxy::assigncan_cfg_7( Tpu_reg value )
{
	mem_assign_field(can_cfg_7,payload,value);
}


void TpuMemProxy::assigncan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg value )
{
	mem_assign_field(can_cfg_8,can_trig_dat_max_bits31_0,value);
}

void TpuMemProxy::assigncan_cfg_8( Tpu_reg value )
{
	mem_assign_field(can_cfg_8,payload,value);
}


void TpuMemProxy::assigncan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg value )
{
	mem_assign_field(can_cfg_9,can_trig_dat_max_bits63_32,value);
}

void TpuMemProxy::assigncan_cfg_9( Tpu_reg value )
{
	mem_assign_field(can_cfg_9,payload,value);
}


void TpuMemProxy::assigncan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg value )
{
	mem_assign_field(can_cfg_10,can_trig_dat_mask_bits31_0,value);
}

void TpuMemProxy::assigncan_cfg_10( Tpu_reg value )
{
	mem_assign_field(can_cfg_10,payload,value);
}


void TpuMemProxy::assigncan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg value )
{
	mem_assign_field(can_cfg_11,can_trig_dat_mask_bits63_32,value);
}

void TpuMemProxy::assigncan_cfg_11( Tpu_reg value )
{
	mem_assign_field(can_cfg_11,payload,value);
}


void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_location( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_location,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_frame( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_frame,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_symbol,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_err( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_err,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_id( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_id,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_id_mask,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_cyccount,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_type_cyc_mask,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_rate_sel,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_channel_sel,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_gdTSSTransmitter,value);
}
void TpuMemProxy::assignflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,flexray_trig_gdCASxLowMax,value);
}

void TpuMemProxy::assignflexray_cfg_1( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_1,payload,value);
}


void TpuMemProxy::assignflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_2,flexray_trig_frame_id_min,value);
}
void TpuMemProxy::assignflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_2,flexray_trig_frame_id_max,value);
}

void TpuMemProxy::assignflexray_cfg_2( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_2,payload,value);
}


void TpuMemProxy::assignflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_3,flexray_trig_frame_id_mask,value);
}

void TpuMemProxy::assignflexray_cfg_3( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_3,payload,value);
}


void TpuMemProxy::assignflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_min,value);
}
void TpuMemProxy::assignflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_max,value);
}
void TpuMemProxy::assignflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_mask,value);
}

void TpuMemProxy::assignflexray_cfg_4( Tpu_reg value )
{
	mem_assign_field(flexray_cfg_4,payload,value);
}


void TpuMemProxy::assignspi_cfg_1_spi_trig_type( Tpu_reg value )
{
	mem_assign_field(spi_cfg_1,spi_trig_type,value);
}
void TpuMemProxy::assignspi_cfg_1_spi_trig_dat_type( Tpu_reg value )
{
	mem_assign_field(spi_cfg_1,spi_trig_dat_type,value);
}
void TpuMemProxy::assignspi_cfg_1_spi_trig_dat_bit_num( Tpu_reg value )
{
	mem_assign_field(spi_cfg_1,spi_trig_dat_bit_num,value);
}

void TpuMemProxy::assignspi_cfg_1( Tpu_reg value )
{
	mem_assign_field(spi_cfg_1,payload,value);
}


void TpuMemProxy::assignspi_cfg_2_spi_trig_timeout( Tpu_reg value )
{
	mem_assign_field(spi_cfg_2,spi_trig_timeout,value);
}

void TpuMemProxy::assignspi_cfg_2( Tpu_reg value )
{
	mem_assign_field(spi_cfg_2,payload,value);
}


void TpuMemProxy::assignspi_cfg_3_spi_trig_dat_min( Tpu_reg value )
{
	mem_assign_field(spi_cfg_3,spi_trig_dat_min,value);
}

void TpuMemProxy::assignspi_cfg_3( Tpu_reg value )
{
	mem_assign_field(spi_cfg_3,payload,value);
}


void TpuMemProxy::assignspi_cfg_4_spi_trig_dat_max( Tpu_reg value )
{
	mem_assign_field(spi_cfg_4,spi_trig_dat_max,value);
}

void TpuMemProxy::assignspi_cfg_4( Tpu_reg value )
{
	mem_assign_field(spi_cfg_4,payload,value);
}


void TpuMemProxy::assignspi_cfg_5_spi_trig_dat_mask( Tpu_reg value )
{
	mem_assign_field(spi_cfg_5,spi_trig_dat_mask,value);
}

void TpuMemProxy::assignspi_cfg_5( Tpu_reg value )
{
	mem_assign_field(spi_cfg_5,payload,value);
}


void TpuMemProxy::assigniic_cfg_1_iic_trig_type( Tpu_reg value )
{
	mem_assign_field(iic_cfg_1,iic_trig_type,value);
}
void TpuMemProxy::assigniic_cfg_1_iic_trig_addr_type( Tpu_reg value )
{
	mem_assign_field(iic_cfg_1,iic_trig_addr_type,value);
}
void TpuMemProxy::assigniic_cfg_1_iic_trig_dat_type( Tpu_reg value )
{
	mem_assign_field(iic_cfg_1,iic_trig_dat_type,value);
}
void TpuMemProxy::assigniic_cfg_1_iic_trig_wr_type( Tpu_reg value )
{
	mem_assign_field(iic_cfg_1,iic_trig_wr_type,value);
}
void TpuMemProxy::assigniic_cfg_1_iic_addr_length( Tpu_reg value )
{
	mem_assign_field(iic_cfg_1,iic_addr_length,value);
}
void TpuMemProxy::assigniic_cfg_1_iic_dat_length( Tpu_reg value )
{
	mem_assign_field(iic_cfg_1,iic_dat_length,value);
}

void TpuMemProxy::assigniic_cfg_1( Tpu_reg value )
{
	mem_assign_field(iic_cfg_1,payload,value);
}


void TpuMemProxy::assigniic_cfg_2_iic_addr_setmin( Tpu_reg value )
{
	mem_assign_field(iic_cfg_2,iic_addr_setmin,value);
}
void TpuMemProxy::assigniic_cfg_2_iic_addr_setmax( Tpu_reg value )
{
	mem_assign_field(iic_cfg_2,iic_addr_setmax,value);
}
void TpuMemProxy::assigniic_cfg_2_iic_addr_setmask( Tpu_reg value )
{
	mem_assign_field(iic_cfg_2,iic_addr_setmask,value);
}

void TpuMemProxy::assigniic_cfg_2( Tpu_reg value )
{
	mem_assign_field(iic_cfg_2,payload,value);
}


void TpuMemProxy::assigniic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg value )
{
	mem_assign_field(iic_cfg_3,iic_dat_setmin_bits31_0,value);
}

void TpuMemProxy::assigniic_cfg_3( Tpu_reg value )
{
	mem_assign_field(iic_cfg_3,payload,value);
}


void TpuMemProxy::assigniic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg value )
{
	mem_assign_field(iic_cfg_4,iic_dat_setmax_bits31_0,value);
}

void TpuMemProxy::assigniic_cfg_4( Tpu_reg value )
{
	mem_assign_field(iic_cfg_4,payload,value);
}


void TpuMemProxy::assigniic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg value )
{
	mem_assign_field(iic_cfg_5,iic_dat_setmask_bits31_0,value);
}

void TpuMemProxy::assigniic_cfg_5( Tpu_reg value )
{
	mem_assign_field(iic_cfg_5,payload,value);
}


void TpuMemProxy::assigniic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg value )
{
	mem_assign_field(iic_cfg_6,iic_dat_setmin_bits39_32,value);
}
void TpuMemProxy::assigniic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg value )
{
	mem_assign_field(iic_cfg_6,iic_dat_setmax_bits39_32,value);
}
void TpuMemProxy::assigniic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg value )
{
	mem_assign_field(iic_cfg_6,iic_dat_setmask_bits39_32,value);
}

void TpuMemProxy::assigniic_cfg_6( Tpu_reg value )
{
	mem_assign_field(iic_cfg_6,payload,value);
}


void TpuMemProxy::assignvideo_cfg_1_video_trig_type( Tpu_reg value )
{
	mem_assign_field(video_cfg_1,video_trig_type,value);
}
void TpuMemProxy::assignvideo_cfg_1_video_trig_line( Tpu_reg value )
{
	mem_assign_field(video_cfg_1,video_trig_line,value);
}

void TpuMemProxy::assignvideo_cfg_1( Tpu_reg value )
{
	mem_assign_field(video_cfg_1,payload,value);
}


void TpuMemProxy::assignvideo_cfg_2_fpulse_width( Tpu_reg value )
{
	mem_assign_field(video_cfg_2,fpulse_width,value);
}
void TpuMemProxy::assignvideo_cfg_2_hsync_width( Tpu_reg value )
{
	mem_assign_field(video_cfg_2,hsync_width,value);
}

void TpuMemProxy::assignvideo_cfg_2( Tpu_reg value )
{
	mem_assign_field(video_cfg_2,payload,value);
}


void TpuMemProxy::assignvideo_cfg_3_start_line( Tpu_reg value )
{
	mem_assign_field(video_cfg_3,start_line,value);
}
void TpuMemProxy::assignvideo_cfg_3_total_line( Tpu_reg value )
{
	mem_assign_field(video_cfg_3,total_line,value);
}
void TpuMemProxy::assignvideo_cfg_3_level_type( Tpu_reg value )
{
	mem_assign_field(video_cfg_3,level_type,value);
}

void TpuMemProxy::assignvideo_cfg_3( Tpu_reg value )
{
	mem_assign_field(video_cfg_3,payload,value);
}


void TpuMemProxy::assignstd1553b_cfg_1_std1553b_trig_type( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,std1553b_trig_type,value);
}
void TpuMemProxy::assignstd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,std1553b_trig_sync_type,value);
}
void TpuMemProxy::assignstd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,std1553b_trig_dat_type,value);
}
void TpuMemProxy::assignstd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,std1553b_trig_remote_addr_type,value);
}
void TpuMemProxy::assignstd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,std1553b_trig_subaddr_mode_type,value);
}
void TpuMemProxy::assignstd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,std1553b_trig_count_code_type,value);
}
void TpuMemProxy::assignstd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,std1553b_trig_err_type,value);
}

void TpuMemProxy::assignstd1553b_cfg_1( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_1,payload,value);
}


void TpuMemProxy::assignstd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_2,std1553b_trig_word_mask,value);
}

void TpuMemProxy::assignstd1553b_cfg_2( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_2,payload,value);
}


void TpuMemProxy::assignstd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_3,std1553b_trig_dat_cmp_l,value);
}

void TpuMemProxy::assignstd1553b_cfg_3( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_3,payload,value);
}


void TpuMemProxy::assignstd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_4,std1553b_trig_dat_cmp_g,value);
}

void TpuMemProxy::assignstd1553b_cfg_4( Tpu_reg value )
{
	mem_assign_field(std1553b_cfg_4,payload,value);
}


Tpu_reg TpuMemProxy::getsearch_version()
{
	return search_version;
}


void TpuMemProxy::assignsearch_dat_compress_num_search_dat_compress_num( Tpu_reg value )
{
	mem_assign_field(search_dat_compress_num,search_dat_compress_num,value);
}

void TpuMemProxy::assignsearch_dat_compress_num( Tpu_reg value )
{
	mem_assign_field(search_dat_compress_num,payload,value);
}


void TpuMemProxy::assignsearch_result_send_en_search_result_send_en( Tpu_reg value )
{
	mem_assign_field(search_result_send_en,search_result_send_en,value);
}

void TpuMemProxy::assignsearch_result_send_en( Tpu_reg value )
{
	mem_assign_field(search_result_send_en,payload,value);
}


void TpuMemProxy::assignsearch_result_send_rst_search_result_send_rst( Tpu_reg value )
{
	mem_assign_field(search_result_send_rst,search_result_send_rst,value);
}

void TpuMemProxy::assignsearch_result_send_rst( Tpu_reg value )
{
	mem_assign_field(search_result_send_rst,payload,value);
}


Tpu_reg TpuMemProxy::getsearch_event_total_num_search_event_total_num()
{
	return search_event_total_num.search_event_total_num;
}

Tpu_reg TpuMemProxy::getsearch_event_total_num()
{
	return search_event_total_num.payload;
}



void TpuMem::_loadOtp()
{
	version=0x0;
	ch1_cmp_level.payload=0x0;
		ch1_cmp_level.lvl_1_h_a=0x64;
		ch1_cmp_level.lvl_1_l_a=0x5f;
		ch1_cmp_level.lvl_1_h_b=0xa;
		ch1_cmp_level.lvl_1_l_b=0x5;

	ch2_cmp_level.payload=0x0;
		ch2_cmp_level.lvl_2_h_a=0x64;
		ch2_cmp_level.lvl_2_l_a=0x5f;
		ch2_cmp_level.lvl_2_h_b=0xa;
		ch2_cmp_level.lvl_2_l_b=0x5;

	ch3_cmp_level.payload=0x0;
		ch3_cmp_level.lvl_3_h_a=0x64;
		ch3_cmp_level.lvl_3_l_a=0x5f;
		ch3_cmp_level.lvl_3_h_b=0xa;
		ch3_cmp_level.lvl_3_l_b=0x5;


	ch4_cmp_level.payload=0x0;
		ch4_cmp_level.lvl_4_h_a=0x64;
		ch4_cmp_level.lvl_4_l_a=0x5f;
		ch4_cmp_level.lvl_4_h_b=0xa;
		ch4_cmp_level.lvl_4_l_b=0x5;

	A_event_reg1.payload=0x0;
		A_event_reg1.A_event_logic_la_sel=0x0;

	A_event_reg2.payload=0x0;
		A_event_reg2.A_event_logic_analog_sel=0x0;
		A_event_reg2.A_event_logic_func=0x0;

	A_event_reg3.payload=0x0;
		A_event_reg3.A_event_cmpa_sel=0x0;
		A_event_reg3.A_event_cmpa_mode=0x0;
		A_event_reg3.A_event_cmpb_sel=0x0;
		A_event_reg3.A_event_cmpb_mode=0x0;

		A_event_reg3.A_event_clk_sel=0x0;
		A_event_reg3.A_event_clk_sel_inv=0x0;
		A_event_reg3.A_event_dat_sel=0x5;
		A_event_reg3.A_event_dat_sel_inv=0x0;


	A_event_reg4.payload=0x0;
		A_event_reg4.A_event_width_cmp_1_31_0=0x1;

	A_event_reg5.payload=0x0;
		A_event_reg5.A_event_width_cmp_g_31_0=0x10;

	A_event_reg6.payload=0x0;
		A_event_reg6.A_event_width_cmp_1_39_32=0x0;
		A_event_reg6.A_event_width_cmp_g_39_32=0x0;
		A_event_reg6.A_event_width_cmp_mode=0x0;

	B_event_reg1.payload=0x0;
		B_event_reg1.B_event_logic_la_sel=0x0;


	B_event_reg2.payload=0x0;
		B_event_reg2.B_event_logic_analog_sel=0x0;
		B_event_reg2.B_event_logic_func=0x0;

	B_event_reg3.payload=0x0;
		B_event_reg3.B_event_cmpa_sel=0x0;
		B_event_reg3.B_event_cmpa_mode=0x0;
		B_event_reg3.B_event_cmpb_sel=0x0;
		B_event_reg3.B_event_cmpb_mode=0x0;

		B_event_reg3.B_event_clk_sel=0x0;
		B_event_reg3.B_event_clk_sel_inv=0x0;
		B_event_reg3.B_event_dat_sel=0x5;
		B_event_reg3.B_event_dat_sel_inv=0x0;

	B_event_reg4.payload=0x0;
		B_event_reg4.B_event_width_cmp_1_31_0=0x1;

	B_event_reg5.payload=0x0;
		B_event_reg5.B_event_width_cmp_g_31_0=0x10;


	B_event_reg6.payload=0x0;
		B_event_reg6.B_event_width_cmp_1_39_32=0x0;
		B_event_reg6.B_event_width_cmp_g_39_32=0x0;
		B_event_reg6.B_event_width_cmp_mode=0x0;

	R_event_reg1.payload=0x0;
		R_event_reg1.R_event_logic_la_sel=0x0;

	R_event_reg2.payload=0x0;
		R_event_reg2.R_event_logic_analog_sel=0x0;
		R_event_reg2.R_event_logic_func=0x0;

	R_event_reg3.payload=0x0;
		R_event_reg3.R_event_cmpa_sel=0x0;
		R_event_reg3.R_event_cmpa_mode=0x0;
		R_event_reg3.R_event_cmpb_sel=0x0;
		R_event_reg3.R_event_cmpb_mode=0x0;

		R_event_reg3.R_event_clk_sel=0x0;
		R_event_reg3.R_event_clk_sel_inv=0x0;
		R_event_reg3.R_event_dat_sel=0x5;
		R_event_reg3.R_event_dat_sel_inv=0x0;


	R_event_reg4.payload=0x0;
		R_event_reg4.R_event_width_cmp_1_31_0=0x1;

	R_event_reg5.payload=0x0;
		R_event_reg5.R_event_width_cmp_g_31_0=0x10;

	R_event_reg6.payload=0x0;
		R_event_reg6.R_event_width_cmp_1_39_32=0x0;
		R_event_reg6.R_event_width_cmp_g_39_32=0x0;
		R_event_reg6.R_event_width_cmp_mode=0x0;

	tpu_trig_mux.payload=0x0;
		tpu_trig_mux.tpu_trig_type=0x0;
		tpu_trig_mux.trig_rst=0x0;
		tpu_trig_mux.force_trig=0x0;
		tpu_trig_mux.abr_seq_loop_en=0x0;

		tpu_trig_mux.abr_seq_delay_on=0x0;
		tpu_trig_mux.abr_seq_timeout_on=0x0;
		tpu_trig_mux.abr_seq_r_event_on=0x0;
		tpu_trig_mux.seq_abr_trig_a_only=0x0;


	abr_delay_time_l.payload=0x0;
		abr_delay_time_l.abr_delay_time_l=0x0;

	abr_delay_time_h.payload=0x0;
		abr_delay_time_h.abr_delay_time_h=0x0;

	abr_timeout_l.payload=0x0;
		abr_timeout_l.abr_timeout_l=0x0;

	abr_timeout_h.payload=0x0;
		abr_timeout_h.abr_timeout_h=0x0;


	abr_b_event_num=0x0;
	preprocess_type=0x0;
	abr_a_event_num=0x0;
	holdoff_event_num=0x0;

	holdoff_time_l.payload=0x0;
		holdoff_time_l.holdoff_time_l=0x0;

	holdoff_time_h.payload=0x0;
		holdoff_time_h.holdoff_time_h=0x0;
		holdoff_time_h.holdoff_type=0x0;

	iir_set.payload=0x0;
		iir_set.iir_type=0x0;
		iir_set.iir_src=0x0;
		iir_set.iir_offset=0x0;

	uart_cfg_1.payload=0x0;
		uart_cfg_1.uart_clk_div=0x0;
		uart_cfg_1.uart_trig_type=0x0;
		uart_cfg_1.uart_trig_dat=0x0;
		uart_cfg_1.uart_trig_err=0x0;

		uart_cfg_1.uart_eof=0x0;
		uart_cfg_1.uart_parity=0x0;


	uart_cfg_2.payload=0x0;
		uart_cfg_2.uart_len=0x0;
		uart_cfg_2.uart_dat_min=0x0;
		uart_cfg_2.uart_dat_max=0x0;

	iis_cfg_1.payload=0x0;
		iis_cfg_1.iis_trig_type=0x0;
		iis_cfg_1.iis_trig_channel_sel=0x0;
		iis_cfg_1.iis_trig_bus_type=0x0;
		iis_cfg_1.iis_trig_bitnum_vld=0x0;

		iis_cfg_1.iis_trig_bitnum_total=0x0;

	iis_cfg_2.payload=0x0;
		iis_cfg_2.iis_trig_word_min=0x0;

	iis_cfg_3.payload=0x0;
		iis_cfg_3.iis_trig_word_max=0x0;


	iis_cfg_4.payload=0x0;
		iis_cfg_4.iis_trig_word_mask=0x0;

	lin_cfg_1.payload=0x0;
		lin_cfg_1.lin_trig_type=0x0;
		lin_cfg_1.lin_trig_type_id=0x0;
		lin_cfg_1.lin_trig_type_dat=0x0;
		lin_cfg_1.lin_trig_type_err=0x0;

		lin_cfg_1.lin_trig_spec=0x0;
		lin_cfg_1.lin_trig_id_min=0x0;
		lin_cfg_1.lin_trig_id_max=0x0;
		lin_cfg_1.lin_trig_dat_cmpnum=0x0;

	lin_cfg_2.payload=0x0;
		lin_cfg_2.lin_trig_clk_div=0x0;

	lin_cfg_3.payload=0x0;
		lin_cfg_3.lin_trig_sa_pos=0x0;


	lin_cfg_4.payload=0x0;
		lin_cfg_4.lin_trig_dat_min_bits31_0=0x0;

	lin_cfg_5.payload=0x0;
		lin_cfg_5.lin_trig_dat_min_bits63_32=0x0;

	lin_cfg_6.payload=0x0;
		lin_cfg_6.lin_trig_dat_max_bits31_0=0x0;

	lin_cfg_7.payload=0x0;
		lin_cfg_7.lin_trig_dat_max_bits63_32=0x0;


	lin_cfg_8.payload=0x0;
		lin_cfg_8.lin_trig_dat_mask_bits31_0=0x0;

	lin_cfg_9.payload=0x0;
		lin_cfg_9.lin_trig_dat_maak_bits63_32=0x0;

	can_cfg_1.payload=0x0;
		can_cfg_1.can_trig_type=0x0;
		can_cfg_1.can_trig_type_field=0x0;
		can_cfg_1.can_trig_type_frame=0x0;
		can_cfg_1.can_trig_type_err=0x0;

		can_cfg_1.can_trig_type_id=0x0;
		can_cfg_1.can_trig_type_id_mask=0x0;
		can_cfg_1.can_trig_type_dat=0x0;
		can_cfg_1.can_trig_type_dat_mask=0x0;

		can_cfg_1.can_trig_bus_type=0x0;
		can_cfg_1.can_trig_std_extend_sel=0x0;
		can_cfg_1.can_trig_dat_cmpnum=0x0;

	can_cfg_2.payload=0x0;
		can_cfg_2.can_trig_clk_div=0x0;
		can_cfg_2.can_trig_sa_pos=0x0;


	can_cfg_3.payload=0x0;
		can_cfg_3.can_trig_id_min=0x0;

	can_cfg_4.payload=0x0;
		can_cfg_4.can_trig_id_max=0x0;

	can_cfg_5.payload=0x0;
		can_cfg_5.can_trig_id_mask=0x0;

	can_cfg_6.payload=0x0;
		can_cfg_6.can_trig_dat_min_bits31_0=0x0;


	can_cfg_7.payload=0x0;
		can_cfg_7.can_trig_dat_min_bits63_32=0x0;

	can_cfg_8.payload=0x0;
		can_cfg_8.can_trig_dat_max_bits31_0=0x0;

	can_cfg_9.payload=0x0;
		can_cfg_9.can_trig_dat_max_bits63_32=0x0;

	can_cfg_10.payload=0x0;
		can_cfg_10.can_trig_dat_mask_bits31_0=0x0;


	can_cfg_11.payload=0x0;
		can_cfg_11.can_trig_dat_mask_bits63_32=0x0;

	flexray_cfg_1.payload=0x0;
		flexray_cfg_1.flexray_trig_type=0x0;
		flexray_cfg_1.flexray_trig_type_location=0x0;
		flexray_cfg_1.flexray_trig_type_frame=0x0;
		flexray_cfg_1.flexray_trig_type_symbol=0x0;

		flexray_cfg_1.flexray_trig_type_err=0x0;
		flexray_cfg_1.flexray_trig_type_id=0x0;
		flexray_cfg_1.flexray_trig_type_id_mask=0x0;
		flexray_cfg_1.flexray_trig_type_cyccount=0x0;

		flexray_cfg_1.flexray_trig_type_cyc_mask=0x0;
		flexray_cfg_1.flexray_trig_rate_sel=0x0;
		flexray_cfg_1.flexray_trig_channel_sel=0x0;
		flexray_cfg_1.flexray_trig_gdTSSTransmitter=0x0;

		flexray_cfg_1.flexray_trig_gdCASxLowMax=0x0;

	flexray_cfg_2.payload=0x0;
		flexray_cfg_2.flexray_trig_frame_id_min=0x0;
		flexray_cfg_2.flexray_trig_frame_id_max=0x0;

	flexray_cfg_3.payload=0x0;
		flexray_cfg_3.flexray_trig_frame_id_mask=0x0;


	flexray_cfg_4.payload=0x0;
		flexray_cfg_4.flexray_trig_frame_cycount_min=0x0;
		flexray_cfg_4.flexray_trig_frame_cycount_max=0x0;
		flexray_cfg_4.flexray_trig_frame_cycount_mask=0x0;

	spi_cfg_1.payload=0x0;
		spi_cfg_1.spi_trig_type=0x0;
		spi_cfg_1.spi_trig_dat_type=0x0;
		spi_cfg_1.spi_trig_dat_bit_num=0x0;

	spi_cfg_2.payload=0x0;
		spi_cfg_2.spi_trig_timeout=0x0;

	spi_cfg_3.payload=0x0;
		spi_cfg_3.spi_trig_dat_min=0x0;


	spi_cfg_4.payload=0x0;
		spi_cfg_4.spi_trig_dat_max=0x0;

	spi_cfg_5.payload=0x0;
		spi_cfg_5.spi_trig_dat_mask=0x0;

	iic_cfg_1.payload=0x0;
		iic_cfg_1.iic_trig_type=0x0;
		iic_cfg_1.iic_trig_addr_type=0x0;
		iic_cfg_1.iic_trig_dat_type=0x0;
		iic_cfg_1.iic_trig_wr_type=0x0;

		iic_cfg_1.iic_addr_length=0x0;
		iic_cfg_1.iic_dat_length=0x0;

	iic_cfg_2.payload=0x0;
		iic_cfg_2.iic_addr_setmin=0x0;
		iic_cfg_2.iic_addr_setmax=0x0;
		iic_cfg_2.iic_addr_setmask=0x0;


	iic_cfg_3.payload=0x0;
		iic_cfg_3.iic_dat_setmin_bits31_0=0x0;

	iic_cfg_4.payload=0x0;
		iic_cfg_4.iic_dat_setmax_bits31_0=0x0;

	iic_cfg_5.payload=0x0;
		iic_cfg_5.iic_dat_setmask_bits31_0=0x0;

	iic_cfg_6.payload=0x0;
		iic_cfg_6.iic_dat_setmin_bits39_32=0x0;
		iic_cfg_6.iic_dat_setmax_bits39_32=0x0;
		iic_cfg_6.iic_dat_setmask_bits39_32=0x0;


	video_cfg_1.payload=0x0;
		video_cfg_1.video_trig_type=0x0;
		video_cfg_1.video_trig_line=0x0;

	video_cfg_2.payload=0x0;
		video_cfg_2.fpulse_width=0x0;
		video_cfg_2.hsync_width=0x0;

	video_cfg_3.payload=0x0;
		video_cfg_3.start_line=0x0;
		video_cfg_3.total_line=0x0;
		video_cfg_3.level_type=0x0;

	std1553b_cfg_1.payload=0x0;
		std1553b_cfg_1.std1553b_trig_type=0x0;
		std1553b_cfg_1.std1553b_trig_sync_type=0x0;
		std1553b_cfg_1.std1553b_trig_dat_type=0x0;
		std1553b_cfg_1.std1553b_trig_remote_addr_type=0x0;

		std1553b_cfg_1.std1553b_trig_subaddr_mode_type=0x0;
		std1553b_cfg_1.std1553b_trig_count_code_type=0x0;
		std1553b_cfg_1.std1553b_trig_err_type=0x0;


	std1553b_cfg_2.payload=0x0;
		std1553b_cfg_2.std1553b_trig_word_mask=0x0;

	std1553b_cfg_3.payload=0x0;
		std1553b_cfg_3.std1553b_trig_dat_cmp_l=0x0;

	std1553b_cfg_4.payload=0x0;
		std1553b_cfg_4.std1553b_trig_dat_cmp_g=0x0;

	search_version=0x0;

	search_dat_compress_num.payload=0x0;
		search_dat_compress_num.search_dat_compress_num=0x0;

	search_result_send_en.payload=0x0;
		search_result_send_en.search_result_send_en=0x0;

	search_result_send_rst.payload=0x0;
		search_result_send_rst.search_result_send_rst=0x0;

	search_event_total_num.payload=0x0;
		search_event_total_num.search_event_total_num=0x0;


}

void TpuMem::_outOtp()
{
	outch1_cmp_level();
	outch2_cmp_level();
	outch3_cmp_level();

	outch4_cmp_level();
	outA_event_reg1();
	outA_event_reg2();
	outA_event_reg3();

	outA_event_reg4();
	outA_event_reg5();
	outA_event_reg6();
	outB_event_reg1();

	outB_event_reg2();
	outB_event_reg3();
	outB_event_reg4();
	outB_event_reg5();

	outB_event_reg6();
	outR_event_reg1();
	outR_event_reg2();
	outR_event_reg3();

	outR_event_reg4();
	outR_event_reg5();
	outR_event_reg6();
	outtpu_trig_mux();

	outabr_delay_time_l();
	outabr_delay_time_h();
	outabr_timeout_l();
	outabr_timeout_h();

	outabr_b_event_num();
	outpreprocess_type();
	outabr_a_event_num();
	outholdoff_event_num();

	outholdoff_time_l();
	outholdoff_time_h();
	outiir_set();
	outuart_cfg_1();

	outuart_cfg_2();
	outiis_cfg_1();
	outiis_cfg_2();
	outiis_cfg_3();

	outiis_cfg_4();
	outlin_cfg_1();
	outlin_cfg_2();
	outlin_cfg_3();

	outlin_cfg_4();
	outlin_cfg_5();
	outlin_cfg_6();
	outlin_cfg_7();

	outlin_cfg_8();
	outlin_cfg_9();
	outcan_cfg_1();
	outcan_cfg_2();

	outcan_cfg_3();
	outcan_cfg_4();
	outcan_cfg_5();
	outcan_cfg_6();

	outcan_cfg_7();
	outcan_cfg_8();
	outcan_cfg_9();
	outcan_cfg_10();

	outcan_cfg_11();
	outflexray_cfg_1();
	outflexray_cfg_2();
	outflexray_cfg_3();

	outflexray_cfg_4();
	outspi_cfg_1();
	outspi_cfg_2();
	outspi_cfg_3();

	outspi_cfg_4();
	outspi_cfg_5();
	outiic_cfg_1();
	outiic_cfg_2();

	outiic_cfg_3();
	outiic_cfg_4();
	outiic_cfg_5();
	outiic_cfg_6();

	outvideo_cfg_1();
	outvideo_cfg_2();
	outvideo_cfg_3();
	outstd1553b_cfg_1();

	outstd1553b_cfg_2();
	outstd1553b_cfg_3();
	outstd1553b_cfg_4();

	outsearch_dat_compress_num();
	outsearch_result_send_en();
	outsearch_result_send_rst();

}
void TpuMem::push( TpuMemProxy *proxy )
{
	if ( ch1_cmp_level.payload != proxy->ch1_cmp_level.payload )
	{reg_assign_field(ch1_cmp_level,payload,proxy->ch1_cmp_level.payload);}

	if ( ch2_cmp_level.payload != proxy->ch2_cmp_level.payload )
	{reg_assign_field(ch2_cmp_level,payload,proxy->ch2_cmp_level.payload);}

	if ( ch3_cmp_level.payload != proxy->ch3_cmp_level.payload )
	{reg_assign_field(ch3_cmp_level,payload,proxy->ch3_cmp_level.payload);}

	if ( ch4_cmp_level.payload != proxy->ch4_cmp_level.payload )
	{reg_assign_field(ch4_cmp_level,payload,proxy->ch4_cmp_level.payload);}

	if ( A_event_reg1.payload != proxy->A_event_reg1.payload )
	{reg_assign_field(A_event_reg1,payload,proxy->A_event_reg1.payload);}

	if ( A_event_reg2.payload != proxy->A_event_reg2.payload )
	{reg_assign_field(A_event_reg2,payload,proxy->A_event_reg2.payload);}

	if ( A_event_reg3.payload != proxy->A_event_reg3.payload )
	{reg_assign_field(A_event_reg3,payload,proxy->A_event_reg3.payload);}

	if ( A_event_reg4.payload != proxy->A_event_reg4.payload )
	{reg_assign_field(A_event_reg4,payload,proxy->A_event_reg4.payload);}

	if ( A_event_reg5.payload != proxy->A_event_reg5.payload )
	{reg_assign_field(A_event_reg5,payload,proxy->A_event_reg5.payload);}

	if ( A_event_reg6.payload != proxy->A_event_reg6.payload )
	{reg_assign_field(A_event_reg6,payload,proxy->A_event_reg6.payload);}

	if ( B_event_reg1.payload != proxy->B_event_reg1.payload )
	{reg_assign_field(B_event_reg1,payload,proxy->B_event_reg1.payload);}

	if ( B_event_reg2.payload != proxy->B_event_reg2.payload )
	{reg_assign_field(B_event_reg2,payload,proxy->B_event_reg2.payload);}

	if ( B_event_reg3.payload != proxy->B_event_reg3.payload )
	{reg_assign_field(B_event_reg3,payload,proxy->B_event_reg3.payload);}

	if ( B_event_reg4.payload != proxy->B_event_reg4.payload )
	{reg_assign_field(B_event_reg4,payload,proxy->B_event_reg4.payload);}

	if ( B_event_reg5.payload != proxy->B_event_reg5.payload )
	{reg_assign_field(B_event_reg5,payload,proxy->B_event_reg5.payload);}

	if ( B_event_reg6.payload != proxy->B_event_reg6.payload )
	{reg_assign_field(B_event_reg6,payload,proxy->B_event_reg6.payload);}

	if ( R_event_reg1.payload != proxy->R_event_reg1.payload )
	{reg_assign_field(R_event_reg1,payload,proxy->R_event_reg1.payload);}

	if ( R_event_reg2.payload != proxy->R_event_reg2.payload )
	{reg_assign_field(R_event_reg2,payload,proxy->R_event_reg2.payload);}

	if ( R_event_reg3.payload != proxy->R_event_reg3.payload )
	{reg_assign_field(R_event_reg3,payload,proxy->R_event_reg3.payload);}

	if ( R_event_reg4.payload != proxy->R_event_reg4.payload )
	{reg_assign_field(R_event_reg4,payload,proxy->R_event_reg4.payload);}

	if ( R_event_reg5.payload != proxy->R_event_reg5.payload )
	{reg_assign_field(R_event_reg5,payload,proxy->R_event_reg5.payload);}

	if ( R_event_reg6.payload != proxy->R_event_reg6.payload )
	{reg_assign_field(R_event_reg6,payload,proxy->R_event_reg6.payload);}

	if ( tpu_trig_mux.payload != proxy->tpu_trig_mux.payload )
	{reg_assign_field(tpu_trig_mux,payload,proxy->tpu_trig_mux.payload);}

	if ( abr_delay_time_l.payload != proxy->abr_delay_time_l.payload )
	{reg_assign_field(abr_delay_time_l,payload,proxy->abr_delay_time_l.payload);}

	if ( abr_delay_time_h.payload != proxy->abr_delay_time_h.payload )
	{reg_assign_field(abr_delay_time_h,payload,proxy->abr_delay_time_h.payload);}

	if ( abr_timeout_l.payload != proxy->abr_timeout_l.payload )
	{reg_assign_field(abr_timeout_l,payload,proxy->abr_timeout_l.payload);}

	if ( abr_timeout_h.payload != proxy->abr_timeout_h.payload )
	{reg_assign_field(abr_timeout_h,payload,proxy->abr_timeout_h.payload);}

	if ( abr_b_event_num != proxy->abr_b_event_num )
	{reg_assign(abr_b_event_num,proxy->abr_b_event_num);}

	if ( preprocess_type != proxy->preprocess_type )
	{reg_assign(preprocess_type,proxy->preprocess_type);}

	if ( abr_a_event_num != proxy->abr_a_event_num )
	{reg_assign(abr_a_event_num,proxy->abr_a_event_num);}

	if ( holdoff_event_num != proxy->holdoff_event_num )
	{reg_assign(holdoff_event_num,proxy->holdoff_event_num);}

	if ( holdoff_time_l.payload != proxy->holdoff_time_l.payload )
	{reg_assign_field(holdoff_time_l,payload,proxy->holdoff_time_l.payload);}

	if ( holdoff_time_h.payload != proxy->holdoff_time_h.payload )
	{reg_assign_field(holdoff_time_h,payload,proxy->holdoff_time_h.payload);}

	if ( iir_set.payload != proxy->iir_set.payload )
	{reg_assign_field(iir_set,payload,proxy->iir_set.payload);}

	if ( uart_cfg_1.payload != proxy->uart_cfg_1.payload )
	{reg_assign_field(uart_cfg_1,payload,proxy->uart_cfg_1.payload);}

	if ( uart_cfg_2.payload != proxy->uart_cfg_2.payload )
	{reg_assign_field(uart_cfg_2,payload,proxy->uart_cfg_2.payload);}

	if ( iis_cfg_1.payload != proxy->iis_cfg_1.payload )
	{reg_assign_field(iis_cfg_1,payload,proxy->iis_cfg_1.payload);}

	if ( iis_cfg_2.payload != proxy->iis_cfg_2.payload )
	{reg_assign_field(iis_cfg_2,payload,proxy->iis_cfg_2.payload);}

	if ( iis_cfg_3.payload != proxy->iis_cfg_3.payload )
	{reg_assign_field(iis_cfg_3,payload,proxy->iis_cfg_3.payload);}

	if ( iis_cfg_4.payload != proxy->iis_cfg_4.payload )
	{reg_assign_field(iis_cfg_4,payload,proxy->iis_cfg_4.payload);}

	if ( lin_cfg_1.payload != proxy->lin_cfg_1.payload )
	{reg_assign_field(lin_cfg_1,payload,proxy->lin_cfg_1.payload);}

	if ( lin_cfg_2.payload != proxy->lin_cfg_2.payload )
	{reg_assign_field(lin_cfg_2,payload,proxy->lin_cfg_2.payload);}

	if ( lin_cfg_3.payload != proxy->lin_cfg_3.payload )
	{reg_assign_field(lin_cfg_3,payload,proxy->lin_cfg_3.payload);}

	if ( lin_cfg_4.payload != proxy->lin_cfg_4.payload )
	{reg_assign_field(lin_cfg_4,payload,proxy->lin_cfg_4.payload);}

	if ( lin_cfg_5.payload != proxy->lin_cfg_5.payload )
	{reg_assign_field(lin_cfg_5,payload,proxy->lin_cfg_5.payload);}

	if ( lin_cfg_6.payload != proxy->lin_cfg_6.payload )
	{reg_assign_field(lin_cfg_6,payload,proxy->lin_cfg_6.payload);}

	if ( lin_cfg_7.payload != proxy->lin_cfg_7.payload )
	{reg_assign_field(lin_cfg_7,payload,proxy->lin_cfg_7.payload);}

	if ( lin_cfg_8.payload != proxy->lin_cfg_8.payload )
	{reg_assign_field(lin_cfg_8,payload,proxy->lin_cfg_8.payload);}

	if ( lin_cfg_9.payload != proxy->lin_cfg_9.payload )
	{reg_assign_field(lin_cfg_9,payload,proxy->lin_cfg_9.payload);}

	if ( can_cfg_1.payload != proxy->can_cfg_1.payload )
	{reg_assign_field(can_cfg_1,payload,proxy->can_cfg_1.payload);}

	if ( can_cfg_2.payload != proxy->can_cfg_2.payload )
	{reg_assign_field(can_cfg_2,payload,proxy->can_cfg_2.payload);}

	if ( can_cfg_3.payload != proxy->can_cfg_3.payload )
	{reg_assign_field(can_cfg_3,payload,proxy->can_cfg_3.payload);}

	if ( can_cfg_4.payload != proxy->can_cfg_4.payload )
	{reg_assign_field(can_cfg_4,payload,proxy->can_cfg_4.payload);}

	if ( can_cfg_5.payload != proxy->can_cfg_5.payload )
	{reg_assign_field(can_cfg_5,payload,proxy->can_cfg_5.payload);}

	if ( can_cfg_6.payload != proxy->can_cfg_6.payload )
	{reg_assign_field(can_cfg_6,payload,proxy->can_cfg_6.payload);}

	if ( can_cfg_7.payload != proxy->can_cfg_7.payload )
	{reg_assign_field(can_cfg_7,payload,proxy->can_cfg_7.payload);}

	if ( can_cfg_8.payload != proxy->can_cfg_8.payload )
	{reg_assign_field(can_cfg_8,payload,proxy->can_cfg_8.payload);}

	if ( can_cfg_9.payload != proxy->can_cfg_9.payload )
	{reg_assign_field(can_cfg_9,payload,proxy->can_cfg_9.payload);}

	if ( can_cfg_10.payload != proxy->can_cfg_10.payload )
	{reg_assign_field(can_cfg_10,payload,proxy->can_cfg_10.payload);}

	if ( can_cfg_11.payload != proxy->can_cfg_11.payload )
	{reg_assign_field(can_cfg_11,payload,proxy->can_cfg_11.payload);}

	if ( flexray_cfg_1.payload != proxy->flexray_cfg_1.payload )
	{reg_assign_field(flexray_cfg_1,payload,proxy->flexray_cfg_1.payload);}

	if ( flexray_cfg_2.payload != proxy->flexray_cfg_2.payload )
	{reg_assign_field(flexray_cfg_2,payload,proxy->flexray_cfg_2.payload);}

	if ( flexray_cfg_3.payload != proxy->flexray_cfg_3.payload )
	{reg_assign_field(flexray_cfg_3,payload,proxy->flexray_cfg_3.payload);}

	if ( flexray_cfg_4.payload != proxy->flexray_cfg_4.payload )
	{reg_assign_field(flexray_cfg_4,payload,proxy->flexray_cfg_4.payload);}

	if ( spi_cfg_1.payload != proxy->spi_cfg_1.payload )
	{reg_assign_field(spi_cfg_1,payload,proxy->spi_cfg_1.payload);}

	if ( spi_cfg_2.payload != proxy->spi_cfg_2.payload )
	{reg_assign_field(spi_cfg_2,payload,proxy->spi_cfg_2.payload);}

	if ( spi_cfg_3.payload != proxy->spi_cfg_3.payload )
	{reg_assign_field(spi_cfg_3,payload,proxy->spi_cfg_3.payload);}

	if ( spi_cfg_4.payload != proxy->spi_cfg_4.payload )
	{reg_assign_field(spi_cfg_4,payload,proxy->spi_cfg_4.payload);}

	if ( spi_cfg_5.payload != proxy->spi_cfg_5.payload )
	{reg_assign_field(spi_cfg_5,payload,proxy->spi_cfg_5.payload);}

	if ( iic_cfg_1.payload != proxy->iic_cfg_1.payload )
	{reg_assign_field(iic_cfg_1,payload,proxy->iic_cfg_1.payload);}

	if ( iic_cfg_2.payload != proxy->iic_cfg_2.payload )
	{reg_assign_field(iic_cfg_2,payload,proxy->iic_cfg_2.payload);}

	if ( iic_cfg_3.payload != proxy->iic_cfg_3.payload )
	{reg_assign_field(iic_cfg_3,payload,proxy->iic_cfg_3.payload);}

	if ( iic_cfg_4.payload != proxy->iic_cfg_4.payload )
	{reg_assign_field(iic_cfg_4,payload,proxy->iic_cfg_4.payload);}

	if ( iic_cfg_5.payload != proxy->iic_cfg_5.payload )
	{reg_assign_field(iic_cfg_5,payload,proxy->iic_cfg_5.payload);}

	if ( iic_cfg_6.payload != proxy->iic_cfg_6.payload )
	{reg_assign_field(iic_cfg_6,payload,proxy->iic_cfg_6.payload);}

	if ( video_cfg_1.payload != proxy->video_cfg_1.payload )
	{reg_assign_field(video_cfg_1,payload,proxy->video_cfg_1.payload);}

	if ( video_cfg_2.payload != proxy->video_cfg_2.payload )
	{reg_assign_field(video_cfg_2,payload,proxy->video_cfg_2.payload);}

	if ( video_cfg_3.payload != proxy->video_cfg_3.payload )
	{reg_assign_field(video_cfg_3,payload,proxy->video_cfg_3.payload);}

	if ( std1553b_cfg_1.payload != proxy->std1553b_cfg_1.payload )
	{reg_assign_field(std1553b_cfg_1,payload,proxy->std1553b_cfg_1.payload);}

	if ( std1553b_cfg_2.payload != proxy->std1553b_cfg_2.payload )
	{reg_assign_field(std1553b_cfg_2,payload,proxy->std1553b_cfg_2.payload);}

	if ( std1553b_cfg_3.payload != proxy->std1553b_cfg_3.payload )
	{reg_assign_field(std1553b_cfg_3,payload,proxy->std1553b_cfg_3.payload);}

	if ( std1553b_cfg_4.payload != proxy->std1553b_cfg_4.payload )
	{reg_assign_field(std1553b_cfg_4,payload,proxy->std1553b_cfg_4.payload);}

	if ( search_dat_compress_num.payload != proxy->search_dat_compress_num.payload )
	{reg_assign_field(search_dat_compress_num,payload,proxy->search_dat_compress_num.payload);}

	if ( search_result_send_en.payload != proxy->search_result_send_en.payload )
	{reg_assign_field(search_result_send_en,payload,proxy->search_result_send_en.payload);}

	if ( search_result_send_rst.payload != proxy->search_result_send_rst.payload )
	{reg_assign_field(search_result_send_rst,payload,proxy->search_result_send_rst.payload);}

}
void TpuMem::pull( TpuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(version);
	reg_in(ch1_cmp_level);
	reg_in(ch2_cmp_level);
	reg_in(ch3_cmp_level);
	reg_in(ch4_cmp_level);
	reg_in(A_event_reg1);
	reg_in(A_event_reg2);
	reg_in(A_event_reg3);
	reg_in(A_event_reg4);
	reg_in(A_event_reg5);
	reg_in(A_event_reg6);
	reg_in(B_event_reg1);
	reg_in(B_event_reg2);
	reg_in(B_event_reg3);
	reg_in(B_event_reg4);
	reg_in(B_event_reg5);
	reg_in(B_event_reg6);
	reg_in(R_event_reg1);
	reg_in(R_event_reg2);
	reg_in(R_event_reg3);
	reg_in(R_event_reg4);
	reg_in(R_event_reg5);
	reg_in(R_event_reg6);
	reg_in(tpu_trig_mux);
	reg_in(holdoff_event_num);
	reg_in(holdoff_time_l);
	reg_in(holdoff_time_h);
	reg_in(iir_set);
	reg_in(search_version);
	reg_in(search_event_total_num);

	*proxy = TpuMemProxy(*this);
}
Tpu_reg TpuMem::inversion()
{
	reg_in(version);
	return version;
}


void TpuMem::setch1_cmp_level_lvl_1_h_a( Tpu_reg value )
{
	reg_assign_field(ch1_cmp_level,lvl_1_h_a,value);
}
void TpuMem::setch1_cmp_level_lvl_1_l_a( Tpu_reg value )
{
	reg_assign_field(ch1_cmp_level,lvl_1_l_a,value);
}
void TpuMem::setch1_cmp_level_lvl_1_h_b( Tpu_reg value )
{
	reg_assign_field(ch1_cmp_level,lvl_1_h_b,value);
}
void TpuMem::setch1_cmp_level_lvl_1_l_b( Tpu_reg value )
{
	reg_assign_field(ch1_cmp_level,lvl_1_l_b,value);
}

void TpuMem::setch1_cmp_level( Tpu_reg value )
{
	reg_assign_field(ch1_cmp_level,payload,value);
}

void TpuMem::outch1_cmp_level_lvl_1_h_a( Tpu_reg value )
{
	out_assign_field(ch1_cmp_level,lvl_1_h_a,value);
}
void TpuMem::pulsech1_cmp_level_lvl_1_h_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch1_cmp_level,lvl_1_h_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch1_cmp_level,lvl_1_h_a,idleV);
}
void TpuMem::outch1_cmp_level_lvl_1_l_a( Tpu_reg value )
{
	out_assign_field(ch1_cmp_level,lvl_1_l_a,value);
}
void TpuMem::pulsech1_cmp_level_lvl_1_l_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch1_cmp_level,lvl_1_l_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch1_cmp_level,lvl_1_l_a,idleV);
}
void TpuMem::outch1_cmp_level_lvl_1_h_b( Tpu_reg value )
{
	out_assign_field(ch1_cmp_level,lvl_1_h_b,value);
}
void TpuMem::pulsech1_cmp_level_lvl_1_h_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch1_cmp_level,lvl_1_h_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch1_cmp_level,lvl_1_h_b,idleV);
}
void TpuMem::outch1_cmp_level_lvl_1_l_b( Tpu_reg value )
{
	out_assign_field(ch1_cmp_level,lvl_1_l_b,value);
}
void TpuMem::pulsech1_cmp_level_lvl_1_l_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch1_cmp_level,lvl_1_l_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch1_cmp_level,lvl_1_l_b,idleV);
}

void TpuMem::outch1_cmp_level( Tpu_reg value )
{
	out_assign_field(ch1_cmp_level,payload,value);
}

void TpuMem::outch1_cmp_level()
{
	out_member(ch1_cmp_level);
}


Tpu_reg TpuMem::inch1_cmp_level()
{
	reg_in(ch1_cmp_level);
	return ch1_cmp_level.payload;
}


void TpuMem::setch2_cmp_level_lvl_2_h_a( Tpu_reg value )
{
	reg_assign_field(ch2_cmp_level,lvl_2_h_a,value);
}
void TpuMem::setch2_cmp_level_lvl_2_l_a( Tpu_reg value )
{
	reg_assign_field(ch2_cmp_level,lvl_2_l_a,value);
}
void TpuMem::setch2_cmp_level_lvl_2_h_b( Tpu_reg value )
{
	reg_assign_field(ch2_cmp_level,lvl_2_h_b,value);
}
void TpuMem::setch2_cmp_level_lvl_2_l_b( Tpu_reg value )
{
	reg_assign_field(ch2_cmp_level,lvl_2_l_b,value);
}

void TpuMem::setch2_cmp_level( Tpu_reg value )
{
	reg_assign_field(ch2_cmp_level,payload,value);
}

void TpuMem::outch2_cmp_level_lvl_2_h_a( Tpu_reg value )
{
	out_assign_field(ch2_cmp_level,lvl_2_h_a,value);
}
void TpuMem::pulsech2_cmp_level_lvl_2_h_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch2_cmp_level,lvl_2_h_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch2_cmp_level,lvl_2_h_a,idleV);
}
void TpuMem::outch2_cmp_level_lvl_2_l_a( Tpu_reg value )
{
	out_assign_field(ch2_cmp_level,lvl_2_l_a,value);
}
void TpuMem::pulsech2_cmp_level_lvl_2_l_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch2_cmp_level,lvl_2_l_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch2_cmp_level,lvl_2_l_a,idleV);
}
void TpuMem::outch2_cmp_level_lvl_2_h_b( Tpu_reg value )
{
	out_assign_field(ch2_cmp_level,lvl_2_h_b,value);
}
void TpuMem::pulsech2_cmp_level_lvl_2_h_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch2_cmp_level,lvl_2_h_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch2_cmp_level,lvl_2_h_b,idleV);
}
void TpuMem::outch2_cmp_level_lvl_2_l_b( Tpu_reg value )
{
	out_assign_field(ch2_cmp_level,lvl_2_l_b,value);
}
void TpuMem::pulsech2_cmp_level_lvl_2_l_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch2_cmp_level,lvl_2_l_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch2_cmp_level,lvl_2_l_b,idleV);
}

void TpuMem::outch2_cmp_level( Tpu_reg value )
{
	out_assign_field(ch2_cmp_level,payload,value);
}

void TpuMem::outch2_cmp_level()
{
	out_member(ch2_cmp_level);
}


Tpu_reg TpuMem::inch2_cmp_level()
{
	reg_in(ch2_cmp_level);
	return ch2_cmp_level.payload;
}


void TpuMem::setch3_cmp_level_lvl_3_h_a( Tpu_reg value )
{
	reg_assign_field(ch3_cmp_level,lvl_3_h_a,value);
}
void TpuMem::setch3_cmp_level_lvl_3_l_a( Tpu_reg value )
{
	reg_assign_field(ch3_cmp_level,lvl_3_l_a,value);
}
void TpuMem::setch3_cmp_level_lvl_3_h_b( Tpu_reg value )
{
	reg_assign_field(ch3_cmp_level,lvl_3_h_b,value);
}
void TpuMem::setch3_cmp_level_lvl_3_l_b( Tpu_reg value )
{
	reg_assign_field(ch3_cmp_level,lvl_3_l_b,value);
}

void TpuMem::setch3_cmp_level( Tpu_reg value )
{
	reg_assign_field(ch3_cmp_level,payload,value);
}

void TpuMem::outch3_cmp_level_lvl_3_h_a( Tpu_reg value )
{
	out_assign_field(ch3_cmp_level,lvl_3_h_a,value);
}
void TpuMem::pulsech3_cmp_level_lvl_3_h_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch3_cmp_level,lvl_3_h_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch3_cmp_level,lvl_3_h_a,idleV);
}
void TpuMem::outch3_cmp_level_lvl_3_l_a( Tpu_reg value )
{
	out_assign_field(ch3_cmp_level,lvl_3_l_a,value);
}
void TpuMem::pulsech3_cmp_level_lvl_3_l_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch3_cmp_level,lvl_3_l_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch3_cmp_level,lvl_3_l_a,idleV);
}
void TpuMem::outch3_cmp_level_lvl_3_h_b( Tpu_reg value )
{
	out_assign_field(ch3_cmp_level,lvl_3_h_b,value);
}
void TpuMem::pulsech3_cmp_level_lvl_3_h_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch3_cmp_level,lvl_3_h_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch3_cmp_level,lvl_3_h_b,idleV);
}
void TpuMem::outch3_cmp_level_lvl_3_l_b( Tpu_reg value )
{
	out_assign_field(ch3_cmp_level,lvl_3_l_b,value);
}
void TpuMem::pulsech3_cmp_level_lvl_3_l_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch3_cmp_level,lvl_3_l_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch3_cmp_level,lvl_3_l_b,idleV);
}

void TpuMem::outch3_cmp_level( Tpu_reg value )
{
	out_assign_field(ch3_cmp_level,payload,value);
}

void TpuMem::outch3_cmp_level()
{
	out_member(ch3_cmp_level);
}


Tpu_reg TpuMem::inch3_cmp_level()
{
	reg_in(ch3_cmp_level);
	return ch3_cmp_level.payload;
}


void TpuMem::setch4_cmp_level_lvl_4_h_a( Tpu_reg value )
{
	reg_assign_field(ch4_cmp_level,lvl_4_h_a,value);
}
void TpuMem::setch4_cmp_level_lvl_4_l_a( Tpu_reg value )
{
	reg_assign_field(ch4_cmp_level,lvl_4_l_a,value);
}
void TpuMem::setch4_cmp_level_lvl_4_h_b( Tpu_reg value )
{
	reg_assign_field(ch4_cmp_level,lvl_4_h_b,value);
}
void TpuMem::setch4_cmp_level_lvl_4_l_b( Tpu_reg value )
{
	reg_assign_field(ch4_cmp_level,lvl_4_l_b,value);
}

void TpuMem::setch4_cmp_level( Tpu_reg value )
{
	reg_assign_field(ch4_cmp_level,payload,value);
}

void TpuMem::outch4_cmp_level_lvl_4_h_a( Tpu_reg value )
{
	out_assign_field(ch4_cmp_level,lvl_4_h_a,value);
}
void TpuMem::pulsech4_cmp_level_lvl_4_h_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch4_cmp_level,lvl_4_h_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch4_cmp_level,lvl_4_h_a,idleV);
}
void TpuMem::outch4_cmp_level_lvl_4_l_a( Tpu_reg value )
{
	out_assign_field(ch4_cmp_level,lvl_4_l_a,value);
}
void TpuMem::pulsech4_cmp_level_lvl_4_l_a( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch4_cmp_level,lvl_4_l_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch4_cmp_level,lvl_4_l_a,idleV);
}
void TpuMem::outch4_cmp_level_lvl_4_h_b( Tpu_reg value )
{
	out_assign_field(ch4_cmp_level,lvl_4_h_b,value);
}
void TpuMem::pulsech4_cmp_level_lvl_4_h_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch4_cmp_level,lvl_4_h_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch4_cmp_level,lvl_4_h_b,idleV);
}
void TpuMem::outch4_cmp_level_lvl_4_l_b( Tpu_reg value )
{
	out_assign_field(ch4_cmp_level,lvl_4_l_b,value);
}
void TpuMem::pulsech4_cmp_level_lvl_4_l_b( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(ch4_cmp_level,lvl_4_l_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ch4_cmp_level,lvl_4_l_b,idleV);
}

void TpuMem::outch4_cmp_level( Tpu_reg value )
{
	out_assign_field(ch4_cmp_level,payload,value);
}

void TpuMem::outch4_cmp_level()
{
	out_member(ch4_cmp_level);
}


Tpu_reg TpuMem::inch4_cmp_level()
{
	reg_in(ch4_cmp_level);
	return ch4_cmp_level.payload;
}


void TpuMem::setA_event_reg1_A_event_logic_la_sel( Tpu_reg value )
{
	reg_assign_field(A_event_reg1,A_event_logic_la_sel,value);
}

void TpuMem::setA_event_reg1( Tpu_reg value )
{
	reg_assign_field(A_event_reg1,payload,value);
}

void TpuMem::outA_event_reg1_A_event_logic_la_sel( Tpu_reg value )
{
	out_assign_field(A_event_reg1,A_event_logic_la_sel,value);
}
void TpuMem::pulseA_event_reg1_A_event_logic_la_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg1,A_event_logic_la_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg1,A_event_logic_la_sel,idleV);
}

void TpuMem::outA_event_reg1( Tpu_reg value )
{
	out_assign_field(A_event_reg1,payload,value);
}

void TpuMem::outA_event_reg1()
{
	out_member(A_event_reg1);
}


Tpu_reg TpuMem::inA_event_reg1()
{
	reg_in(A_event_reg1);
	return A_event_reg1.payload;
}


void TpuMem::setA_event_reg2_A_event_logic_analog_sel( Tpu_reg value )
{
	reg_assign_field(A_event_reg2,A_event_logic_analog_sel,value);
}
void TpuMem::setA_event_reg2_A_event_logic_func( Tpu_reg value )
{
	reg_assign_field(A_event_reg2,A_event_logic_func,value);
}

void TpuMem::setA_event_reg2( Tpu_reg value )
{
	reg_assign_field(A_event_reg2,payload,value);
}

void TpuMem::outA_event_reg2_A_event_logic_analog_sel( Tpu_reg value )
{
	out_assign_field(A_event_reg2,A_event_logic_analog_sel,value);
}
void TpuMem::pulseA_event_reg2_A_event_logic_analog_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg2,A_event_logic_analog_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg2,A_event_logic_analog_sel,idleV);
}
void TpuMem::outA_event_reg2_A_event_logic_func( Tpu_reg value )
{
	out_assign_field(A_event_reg2,A_event_logic_func,value);
}
void TpuMem::pulseA_event_reg2_A_event_logic_func( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg2,A_event_logic_func,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg2,A_event_logic_func,idleV);
}

void TpuMem::outA_event_reg2( Tpu_reg value )
{
	out_assign_field(A_event_reg2,payload,value);
}

void TpuMem::outA_event_reg2()
{
	out_member(A_event_reg2);
}


Tpu_reg TpuMem::inA_event_reg2()
{
	reg_in(A_event_reg2);
	return A_event_reg2.payload;
}


void TpuMem::setA_event_reg3_A_event_cmpa_sel( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_cmpa_sel,value);
}
void TpuMem::setA_event_reg3_A_event_cmpa_mode( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_cmpa_mode,value);
}
void TpuMem::setA_event_reg3_A_event_cmpb_sel( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_cmpb_sel,value);
}
void TpuMem::setA_event_reg3_A_event_cmpb_mode( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_cmpb_mode,value);
}
void TpuMem::setA_event_reg3_A_event_clk_sel( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_clk_sel,value);
}
void TpuMem::setA_event_reg3_A_event_clk_sel_inv( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_clk_sel_inv,value);
}
void TpuMem::setA_event_reg3_A_event_dat_sel( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_dat_sel,value);
}
void TpuMem::setA_event_reg3_A_event_dat_sel_inv( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,A_event_dat_sel_inv,value);
}

void TpuMem::setA_event_reg3( Tpu_reg value )
{
	reg_assign_field(A_event_reg3,payload,value);
}

void TpuMem::outA_event_reg3_A_event_cmpa_sel( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_cmpa_sel,value);
}
void TpuMem::pulseA_event_reg3_A_event_cmpa_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_cmpa_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_cmpa_sel,idleV);
}
void TpuMem::outA_event_reg3_A_event_cmpa_mode( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_cmpa_mode,value);
}
void TpuMem::pulseA_event_reg3_A_event_cmpa_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_cmpa_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_cmpa_mode,idleV);
}
void TpuMem::outA_event_reg3_A_event_cmpb_sel( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_cmpb_sel,value);
}
void TpuMem::pulseA_event_reg3_A_event_cmpb_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_cmpb_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_cmpb_sel,idleV);
}
void TpuMem::outA_event_reg3_A_event_cmpb_mode( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_cmpb_mode,value);
}
void TpuMem::pulseA_event_reg3_A_event_cmpb_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_cmpb_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_cmpb_mode,idleV);
}
void TpuMem::outA_event_reg3_A_event_clk_sel( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_clk_sel,value);
}
void TpuMem::pulseA_event_reg3_A_event_clk_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_clk_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_clk_sel,idleV);
}
void TpuMem::outA_event_reg3_A_event_clk_sel_inv( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_clk_sel_inv,value);
}
void TpuMem::pulseA_event_reg3_A_event_clk_sel_inv( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_clk_sel_inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_clk_sel_inv,idleV);
}
void TpuMem::outA_event_reg3_A_event_dat_sel( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_dat_sel,value);
}
void TpuMem::pulseA_event_reg3_A_event_dat_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_dat_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_dat_sel,idleV);
}
void TpuMem::outA_event_reg3_A_event_dat_sel_inv( Tpu_reg value )
{
	out_assign_field(A_event_reg3,A_event_dat_sel_inv,value);
}
void TpuMem::pulseA_event_reg3_A_event_dat_sel_inv( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg3,A_event_dat_sel_inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg3,A_event_dat_sel_inv,idleV);
}

void TpuMem::outA_event_reg3( Tpu_reg value )
{
	out_assign_field(A_event_reg3,payload,value);
}

void TpuMem::outA_event_reg3()
{
	out_member(A_event_reg3);
}


Tpu_reg TpuMem::inA_event_reg3()
{
	reg_in(A_event_reg3);
	return A_event_reg3.payload;
}


void TpuMem::setA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg value )
{
	reg_assign_field(A_event_reg4,A_event_width_cmp_1_31_0,value);
}

void TpuMem::setA_event_reg4( Tpu_reg value )
{
	reg_assign_field(A_event_reg4,payload,value);
}

void TpuMem::outA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg value )
{
	out_assign_field(A_event_reg4,A_event_width_cmp_1_31_0,value);
}
void TpuMem::pulseA_event_reg4_A_event_width_cmp_1_31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg4,A_event_width_cmp_1_31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg4,A_event_width_cmp_1_31_0,idleV);
}

void TpuMem::outA_event_reg4( Tpu_reg value )
{
	out_assign_field(A_event_reg4,payload,value);
}

void TpuMem::outA_event_reg4()
{
	out_member(A_event_reg4);
}


Tpu_reg TpuMem::inA_event_reg4()
{
	reg_in(A_event_reg4);
	return A_event_reg4.payload;
}


void TpuMem::setA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg value )
{
	reg_assign_field(A_event_reg5,A_event_width_cmp_g_31_0,value);
}

void TpuMem::setA_event_reg5( Tpu_reg value )
{
	reg_assign_field(A_event_reg5,payload,value);
}

void TpuMem::outA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg value )
{
	out_assign_field(A_event_reg5,A_event_width_cmp_g_31_0,value);
}
void TpuMem::pulseA_event_reg5_A_event_width_cmp_g_31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg5,A_event_width_cmp_g_31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg5,A_event_width_cmp_g_31_0,idleV);
}

void TpuMem::outA_event_reg5( Tpu_reg value )
{
	out_assign_field(A_event_reg5,payload,value);
}

void TpuMem::outA_event_reg5()
{
	out_member(A_event_reg5);
}


Tpu_reg TpuMem::inA_event_reg5()
{
	reg_in(A_event_reg5);
	return A_event_reg5.payload;
}


void TpuMem::setA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg value )
{
	reg_assign_field(A_event_reg6,A_event_width_cmp_1_39_32,value);
}
void TpuMem::setA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg value )
{
	reg_assign_field(A_event_reg6,A_event_width_cmp_g_39_32,value);
}
void TpuMem::setA_event_reg6_A_event_width_cmp_mode( Tpu_reg value )
{
	reg_assign_field(A_event_reg6,A_event_width_cmp_mode,value);
}

void TpuMem::setA_event_reg6( Tpu_reg value )
{
	reg_assign_field(A_event_reg6,payload,value);
}

void TpuMem::outA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg value )
{
	out_assign_field(A_event_reg6,A_event_width_cmp_1_39_32,value);
}
void TpuMem::pulseA_event_reg6_A_event_width_cmp_1_39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg6,A_event_width_cmp_1_39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg6,A_event_width_cmp_1_39_32,idleV);
}
void TpuMem::outA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg value )
{
	out_assign_field(A_event_reg6,A_event_width_cmp_g_39_32,value);
}
void TpuMem::pulseA_event_reg6_A_event_width_cmp_g_39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg6,A_event_width_cmp_g_39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg6,A_event_width_cmp_g_39_32,idleV);
}
void TpuMem::outA_event_reg6_A_event_width_cmp_mode( Tpu_reg value )
{
	out_assign_field(A_event_reg6,A_event_width_cmp_mode,value);
}
void TpuMem::pulseA_event_reg6_A_event_width_cmp_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(A_event_reg6,A_event_width_cmp_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(A_event_reg6,A_event_width_cmp_mode,idleV);
}

void TpuMem::outA_event_reg6( Tpu_reg value )
{
	out_assign_field(A_event_reg6,payload,value);
}

void TpuMem::outA_event_reg6()
{
	out_member(A_event_reg6);
}


Tpu_reg TpuMem::inA_event_reg6()
{
	reg_in(A_event_reg6);
	return A_event_reg6.payload;
}


void TpuMem::setB_event_reg1_B_event_logic_la_sel( Tpu_reg value )
{
	reg_assign_field(B_event_reg1,B_event_logic_la_sel,value);
}

void TpuMem::setB_event_reg1( Tpu_reg value )
{
	reg_assign_field(B_event_reg1,payload,value);
}

void TpuMem::outB_event_reg1_B_event_logic_la_sel( Tpu_reg value )
{
	out_assign_field(B_event_reg1,B_event_logic_la_sel,value);
}
void TpuMem::pulseB_event_reg1_B_event_logic_la_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg1,B_event_logic_la_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg1,B_event_logic_la_sel,idleV);
}

void TpuMem::outB_event_reg1( Tpu_reg value )
{
	out_assign_field(B_event_reg1,payload,value);
}

void TpuMem::outB_event_reg1()
{
	out_member(B_event_reg1);
}


Tpu_reg TpuMem::inB_event_reg1()
{
	reg_in(B_event_reg1);
	return B_event_reg1.payload;
}


void TpuMem::setB_event_reg2_B_event_logic_analog_sel( Tpu_reg value )
{
	reg_assign_field(B_event_reg2,B_event_logic_analog_sel,value);
}
void TpuMem::setB_event_reg2_B_event_logic_func( Tpu_reg value )
{
	reg_assign_field(B_event_reg2,B_event_logic_func,value);
}

void TpuMem::setB_event_reg2( Tpu_reg value )
{
	reg_assign_field(B_event_reg2,payload,value);
}

void TpuMem::outB_event_reg2_B_event_logic_analog_sel( Tpu_reg value )
{
	out_assign_field(B_event_reg2,B_event_logic_analog_sel,value);
}
void TpuMem::pulseB_event_reg2_B_event_logic_analog_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg2,B_event_logic_analog_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg2,B_event_logic_analog_sel,idleV);
}
void TpuMem::outB_event_reg2_B_event_logic_func( Tpu_reg value )
{
	out_assign_field(B_event_reg2,B_event_logic_func,value);
}
void TpuMem::pulseB_event_reg2_B_event_logic_func( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg2,B_event_logic_func,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg2,B_event_logic_func,idleV);
}

void TpuMem::outB_event_reg2( Tpu_reg value )
{
	out_assign_field(B_event_reg2,payload,value);
}

void TpuMem::outB_event_reg2()
{
	out_member(B_event_reg2);
}


Tpu_reg TpuMem::inB_event_reg2()
{
	reg_in(B_event_reg2);
	return B_event_reg2.payload;
}


void TpuMem::setB_event_reg3_B_event_cmpa_sel( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_cmpa_sel,value);
}
void TpuMem::setB_event_reg3_B_event_cmpa_mode( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_cmpa_mode,value);
}
void TpuMem::setB_event_reg3_B_event_cmpb_sel( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_cmpb_sel,value);
}
void TpuMem::setB_event_reg3_B_event_cmpb_mode( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_cmpb_mode,value);
}
void TpuMem::setB_event_reg3_B_event_clk_sel( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_clk_sel,value);
}
void TpuMem::setB_event_reg3_B_event_clk_sel_inv( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_clk_sel_inv,value);
}
void TpuMem::setB_event_reg3_B_event_dat_sel( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_dat_sel,value);
}
void TpuMem::setB_event_reg3_B_event_dat_sel_inv( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,B_event_dat_sel_inv,value);
}

void TpuMem::setB_event_reg3( Tpu_reg value )
{
	reg_assign_field(B_event_reg3,payload,value);
}

void TpuMem::outB_event_reg3_B_event_cmpa_sel( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_cmpa_sel,value);
}
void TpuMem::pulseB_event_reg3_B_event_cmpa_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_cmpa_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_cmpa_sel,idleV);
}
void TpuMem::outB_event_reg3_B_event_cmpa_mode( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_cmpa_mode,value);
}
void TpuMem::pulseB_event_reg3_B_event_cmpa_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_cmpa_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_cmpa_mode,idleV);
}
void TpuMem::outB_event_reg3_B_event_cmpb_sel( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_cmpb_sel,value);
}
void TpuMem::pulseB_event_reg3_B_event_cmpb_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_cmpb_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_cmpb_sel,idleV);
}
void TpuMem::outB_event_reg3_B_event_cmpb_mode( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_cmpb_mode,value);
}
void TpuMem::pulseB_event_reg3_B_event_cmpb_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_cmpb_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_cmpb_mode,idleV);
}
void TpuMem::outB_event_reg3_B_event_clk_sel( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_clk_sel,value);
}
void TpuMem::pulseB_event_reg3_B_event_clk_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_clk_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_clk_sel,idleV);
}
void TpuMem::outB_event_reg3_B_event_clk_sel_inv( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_clk_sel_inv,value);
}
void TpuMem::pulseB_event_reg3_B_event_clk_sel_inv( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_clk_sel_inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_clk_sel_inv,idleV);
}
void TpuMem::outB_event_reg3_B_event_dat_sel( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_dat_sel,value);
}
void TpuMem::pulseB_event_reg3_B_event_dat_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_dat_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_dat_sel,idleV);
}
void TpuMem::outB_event_reg3_B_event_dat_sel_inv( Tpu_reg value )
{
	out_assign_field(B_event_reg3,B_event_dat_sel_inv,value);
}
void TpuMem::pulseB_event_reg3_B_event_dat_sel_inv( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg3,B_event_dat_sel_inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg3,B_event_dat_sel_inv,idleV);
}

void TpuMem::outB_event_reg3( Tpu_reg value )
{
	out_assign_field(B_event_reg3,payload,value);
}

void TpuMem::outB_event_reg3()
{
	out_member(B_event_reg3);
}


Tpu_reg TpuMem::inB_event_reg3()
{
	reg_in(B_event_reg3);
	return B_event_reg3.payload;
}


void TpuMem::setB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg value )
{
	reg_assign_field(B_event_reg4,B_event_width_cmp_1_31_0,value);
}

void TpuMem::setB_event_reg4( Tpu_reg value )
{
	reg_assign_field(B_event_reg4,payload,value);
}

void TpuMem::outB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg value )
{
	out_assign_field(B_event_reg4,B_event_width_cmp_1_31_0,value);
}
void TpuMem::pulseB_event_reg4_B_event_width_cmp_1_31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg4,B_event_width_cmp_1_31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg4,B_event_width_cmp_1_31_0,idleV);
}

void TpuMem::outB_event_reg4( Tpu_reg value )
{
	out_assign_field(B_event_reg4,payload,value);
}

void TpuMem::outB_event_reg4()
{
	out_member(B_event_reg4);
}


Tpu_reg TpuMem::inB_event_reg4()
{
	reg_in(B_event_reg4);
	return B_event_reg4.payload;
}


void TpuMem::setB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg value )
{
	reg_assign_field(B_event_reg5,B_event_width_cmp_g_31_0,value);
}

void TpuMem::setB_event_reg5( Tpu_reg value )
{
	reg_assign_field(B_event_reg5,payload,value);
}

void TpuMem::outB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg value )
{
	out_assign_field(B_event_reg5,B_event_width_cmp_g_31_0,value);
}
void TpuMem::pulseB_event_reg5_B_event_width_cmp_g_31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg5,B_event_width_cmp_g_31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg5,B_event_width_cmp_g_31_0,idleV);
}

void TpuMem::outB_event_reg5( Tpu_reg value )
{
	out_assign_field(B_event_reg5,payload,value);
}

void TpuMem::outB_event_reg5()
{
	out_member(B_event_reg5);
}


Tpu_reg TpuMem::inB_event_reg5()
{
	reg_in(B_event_reg5);
	return B_event_reg5.payload;
}


void TpuMem::setB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg value )
{
	reg_assign_field(B_event_reg6,B_event_width_cmp_1_39_32,value);
}
void TpuMem::setB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg value )
{
	reg_assign_field(B_event_reg6,B_event_width_cmp_g_39_32,value);
}
void TpuMem::setB_event_reg6_B_event_width_cmp_mode( Tpu_reg value )
{
	reg_assign_field(B_event_reg6,B_event_width_cmp_mode,value);
}

void TpuMem::setB_event_reg6( Tpu_reg value )
{
	reg_assign_field(B_event_reg6,payload,value);
}

void TpuMem::outB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg value )
{
	out_assign_field(B_event_reg6,B_event_width_cmp_1_39_32,value);
}
void TpuMem::pulseB_event_reg6_B_event_width_cmp_1_39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg6,B_event_width_cmp_1_39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg6,B_event_width_cmp_1_39_32,idleV);
}
void TpuMem::outB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg value )
{
	out_assign_field(B_event_reg6,B_event_width_cmp_g_39_32,value);
}
void TpuMem::pulseB_event_reg6_B_event_width_cmp_g_39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg6,B_event_width_cmp_g_39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg6,B_event_width_cmp_g_39_32,idleV);
}
void TpuMem::outB_event_reg6_B_event_width_cmp_mode( Tpu_reg value )
{
	out_assign_field(B_event_reg6,B_event_width_cmp_mode,value);
}
void TpuMem::pulseB_event_reg6_B_event_width_cmp_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(B_event_reg6,B_event_width_cmp_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(B_event_reg6,B_event_width_cmp_mode,idleV);
}

void TpuMem::outB_event_reg6( Tpu_reg value )
{
	out_assign_field(B_event_reg6,payload,value);
}

void TpuMem::outB_event_reg6()
{
	out_member(B_event_reg6);
}


Tpu_reg TpuMem::inB_event_reg6()
{
	reg_in(B_event_reg6);
	return B_event_reg6.payload;
}


void TpuMem::setR_event_reg1_R_event_logic_la_sel( Tpu_reg value )
{
	reg_assign_field(R_event_reg1,R_event_logic_la_sel,value);
}

void TpuMem::setR_event_reg1( Tpu_reg value )
{
	reg_assign_field(R_event_reg1,payload,value);
}

void TpuMem::outR_event_reg1_R_event_logic_la_sel( Tpu_reg value )
{
	out_assign_field(R_event_reg1,R_event_logic_la_sel,value);
}
void TpuMem::pulseR_event_reg1_R_event_logic_la_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg1,R_event_logic_la_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg1,R_event_logic_la_sel,idleV);
}

void TpuMem::outR_event_reg1( Tpu_reg value )
{
	out_assign_field(R_event_reg1,payload,value);
}

void TpuMem::outR_event_reg1()
{
	out_member(R_event_reg1);
}


Tpu_reg TpuMem::inR_event_reg1()
{
	reg_in(R_event_reg1);
	return R_event_reg1.payload;
}


void TpuMem::setR_event_reg2_R_event_logic_analog_sel( Tpu_reg value )
{
	reg_assign_field(R_event_reg2,R_event_logic_analog_sel,value);
}
void TpuMem::setR_event_reg2_R_event_logic_func( Tpu_reg value )
{
	reg_assign_field(R_event_reg2,R_event_logic_func,value);
}

void TpuMem::setR_event_reg2( Tpu_reg value )
{
	reg_assign_field(R_event_reg2,payload,value);
}

void TpuMem::outR_event_reg2_R_event_logic_analog_sel( Tpu_reg value )
{
	out_assign_field(R_event_reg2,R_event_logic_analog_sel,value);
}
void TpuMem::pulseR_event_reg2_R_event_logic_analog_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg2,R_event_logic_analog_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg2,R_event_logic_analog_sel,idleV);
}
void TpuMem::outR_event_reg2_R_event_logic_func( Tpu_reg value )
{
	out_assign_field(R_event_reg2,R_event_logic_func,value);
}
void TpuMem::pulseR_event_reg2_R_event_logic_func( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg2,R_event_logic_func,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg2,R_event_logic_func,idleV);
}

void TpuMem::outR_event_reg2( Tpu_reg value )
{
	out_assign_field(R_event_reg2,payload,value);
}

void TpuMem::outR_event_reg2()
{
	out_member(R_event_reg2);
}


Tpu_reg TpuMem::inR_event_reg2()
{
	reg_in(R_event_reg2);
	return R_event_reg2.payload;
}


void TpuMem::setR_event_reg3_R_event_cmpa_sel( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_cmpa_sel,value);
}
void TpuMem::setR_event_reg3_R_event_cmpa_mode( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_cmpa_mode,value);
}
void TpuMem::setR_event_reg3_R_event_cmpb_sel( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_cmpb_sel,value);
}
void TpuMem::setR_event_reg3_R_event_cmpb_mode( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_cmpb_mode,value);
}
void TpuMem::setR_event_reg3_R_event_clk_sel( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_clk_sel,value);
}
void TpuMem::setR_event_reg3_R_event_clk_sel_inv( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_clk_sel_inv,value);
}
void TpuMem::setR_event_reg3_R_event_dat_sel( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_dat_sel,value);
}
void TpuMem::setR_event_reg3_R_event_dat_sel_inv( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,R_event_dat_sel_inv,value);
}

void TpuMem::setR_event_reg3( Tpu_reg value )
{
	reg_assign_field(R_event_reg3,payload,value);
}

void TpuMem::outR_event_reg3_R_event_cmpa_sel( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_cmpa_sel,value);
}
void TpuMem::pulseR_event_reg3_R_event_cmpa_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_cmpa_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_cmpa_sel,idleV);
}
void TpuMem::outR_event_reg3_R_event_cmpa_mode( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_cmpa_mode,value);
}
void TpuMem::pulseR_event_reg3_R_event_cmpa_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_cmpa_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_cmpa_mode,idleV);
}
void TpuMem::outR_event_reg3_R_event_cmpb_sel( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_cmpb_sel,value);
}
void TpuMem::pulseR_event_reg3_R_event_cmpb_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_cmpb_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_cmpb_sel,idleV);
}
void TpuMem::outR_event_reg3_R_event_cmpb_mode( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_cmpb_mode,value);
}
void TpuMem::pulseR_event_reg3_R_event_cmpb_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_cmpb_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_cmpb_mode,idleV);
}
void TpuMem::outR_event_reg3_R_event_clk_sel( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_clk_sel,value);
}
void TpuMem::pulseR_event_reg3_R_event_clk_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_clk_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_clk_sel,idleV);
}
void TpuMem::outR_event_reg3_R_event_clk_sel_inv( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_clk_sel_inv,value);
}
void TpuMem::pulseR_event_reg3_R_event_clk_sel_inv( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_clk_sel_inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_clk_sel_inv,idleV);
}
void TpuMem::outR_event_reg3_R_event_dat_sel( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_dat_sel,value);
}
void TpuMem::pulseR_event_reg3_R_event_dat_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_dat_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_dat_sel,idleV);
}
void TpuMem::outR_event_reg3_R_event_dat_sel_inv( Tpu_reg value )
{
	out_assign_field(R_event_reg3,R_event_dat_sel_inv,value);
}
void TpuMem::pulseR_event_reg3_R_event_dat_sel_inv( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg3,R_event_dat_sel_inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg3,R_event_dat_sel_inv,idleV);
}

void TpuMem::outR_event_reg3( Tpu_reg value )
{
	out_assign_field(R_event_reg3,payload,value);
}

void TpuMem::outR_event_reg3()
{
	out_member(R_event_reg3);
}


Tpu_reg TpuMem::inR_event_reg3()
{
	reg_in(R_event_reg3);
	return R_event_reg3.payload;
}


void TpuMem::setR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg value )
{
	reg_assign_field(R_event_reg4,R_event_width_cmp_1_31_0,value);
}

void TpuMem::setR_event_reg4( Tpu_reg value )
{
	reg_assign_field(R_event_reg4,payload,value);
}

void TpuMem::outR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg value )
{
	out_assign_field(R_event_reg4,R_event_width_cmp_1_31_0,value);
}
void TpuMem::pulseR_event_reg4_R_event_width_cmp_1_31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg4,R_event_width_cmp_1_31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg4,R_event_width_cmp_1_31_0,idleV);
}

void TpuMem::outR_event_reg4( Tpu_reg value )
{
	out_assign_field(R_event_reg4,payload,value);
}

void TpuMem::outR_event_reg4()
{
	out_member(R_event_reg4);
}


Tpu_reg TpuMem::inR_event_reg4()
{
	reg_in(R_event_reg4);
	return R_event_reg4.payload;
}


void TpuMem::setR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg value )
{
	reg_assign_field(R_event_reg5,R_event_width_cmp_g_31_0,value);
}

void TpuMem::setR_event_reg5( Tpu_reg value )
{
	reg_assign_field(R_event_reg5,payload,value);
}

void TpuMem::outR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg value )
{
	out_assign_field(R_event_reg5,R_event_width_cmp_g_31_0,value);
}
void TpuMem::pulseR_event_reg5_R_event_width_cmp_g_31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg5,R_event_width_cmp_g_31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg5,R_event_width_cmp_g_31_0,idleV);
}

void TpuMem::outR_event_reg5( Tpu_reg value )
{
	out_assign_field(R_event_reg5,payload,value);
}

void TpuMem::outR_event_reg5()
{
	out_member(R_event_reg5);
}


Tpu_reg TpuMem::inR_event_reg5()
{
	reg_in(R_event_reg5);
	return R_event_reg5.payload;
}


void TpuMem::setR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg value )
{
	reg_assign_field(R_event_reg6,R_event_width_cmp_1_39_32,value);
}
void TpuMem::setR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg value )
{
	reg_assign_field(R_event_reg6,R_event_width_cmp_g_39_32,value);
}
void TpuMem::setR_event_reg6_R_event_width_cmp_mode( Tpu_reg value )
{
	reg_assign_field(R_event_reg6,R_event_width_cmp_mode,value);
}

void TpuMem::setR_event_reg6( Tpu_reg value )
{
	reg_assign_field(R_event_reg6,payload,value);
}

void TpuMem::outR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg value )
{
	out_assign_field(R_event_reg6,R_event_width_cmp_1_39_32,value);
}
void TpuMem::pulseR_event_reg6_R_event_width_cmp_1_39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg6,R_event_width_cmp_1_39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg6,R_event_width_cmp_1_39_32,idleV);
}
void TpuMem::outR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg value )
{
	out_assign_field(R_event_reg6,R_event_width_cmp_g_39_32,value);
}
void TpuMem::pulseR_event_reg6_R_event_width_cmp_g_39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg6,R_event_width_cmp_g_39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg6,R_event_width_cmp_g_39_32,idleV);
}
void TpuMem::outR_event_reg6_R_event_width_cmp_mode( Tpu_reg value )
{
	out_assign_field(R_event_reg6,R_event_width_cmp_mode,value);
}
void TpuMem::pulseR_event_reg6_R_event_width_cmp_mode( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(R_event_reg6,R_event_width_cmp_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(R_event_reg6,R_event_width_cmp_mode,idleV);
}

void TpuMem::outR_event_reg6( Tpu_reg value )
{
	out_assign_field(R_event_reg6,payload,value);
}

void TpuMem::outR_event_reg6()
{
	out_member(R_event_reg6);
}


Tpu_reg TpuMem::inR_event_reg6()
{
	reg_in(R_event_reg6);
	return R_event_reg6.payload;
}


void TpuMem::settpu_trig_mux_tpu_trig_type( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,tpu_trig_type,value);
}
void TpuMem::settpu_trig_mux_trig_rst( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,trig_rst,value);
}
void TpuMem::settpu_trig_mux_force_trig( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,force_trig,value);
}
void TpuMem::settpu_trig_mux_abr_seq_loop_en( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,abr_seq_loop_en,value);
}
void TpuMem::settpu_trig_mux_abr_seq_delay_on( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,abr_seq_delay_on,value);
}
void TpuMem::settpu_trig_mux_abr_seq_timeout_on( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,abr_seq_timeout_on,value);
}
void TpuMem::settpu_trig_mux_abr_seq_r_event_on( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,abr_seq_r_event_on,value);
}
void TpuMem::settpu_trig_mux_seq_abr_trig_a_only( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,seq_abr_trig_a_only,value);
}

void TpuMem::settpu_trig_mux( Tpu_reg value )
{
	reg_assign_field(tpu_trig_mux,payload,value);
}

void TpuMem::outtpu_trig_mux_tpu_trig_type( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,tpu_trig_type,value);
}
void TpuMem::pulsetpu_trig_mux_tpu_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,tpu_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,tpu_trig_type,idleV);
}
void TpuMem::outtpu_trig_mux_trig_rst( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,trig_rst,value);
}
void TpuMem::pulsetpu_trig_mux_trig_rst( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,trig_rst,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,trig_rst,idleV);
}
void TpuMem::outtpu_trig_mux_force_trig( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,force_trig,value);
}
void TpuMem::pulsetpu_trig_mux_force_trig( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,force_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,force_trig,idleV);
}
void TpuMem::outtpu_trig_mux_abr_seq_loop_en( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,abr_seq_loop_en,value);
}
void TpuMem::pulsetpu_trig_mux_abr_seq_loop_en( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,abr_seq_loop_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,abr_seq_loop_en,idleV);
}
void TpuMem::outtpu_trig_mux_abr_seq_delay_on( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,abr_seq_delay_on,value);
}
void TpuMem::pulsetpu_trig_mux_abr_seq_delay_on( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,abr_seq_delay_on,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,abr_seq_delay_on,idleV);
}
void TpuMem::outtpu_trig_mux_abr_seq_timeout_on( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,abr_seq_timeout_on,value);
}
void TpuMem::pulsetpu_trig_mux_abr_seq_timeout_on( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,abr_seq_timeout_on,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,abr_seq_timeout_on,idleV);
}
void TpuMem::outtpu_trig_mux_abr_seq_r_event_on( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,abr_seq_r_event_on,value);
}
void TpuMem::pulsetpu_trig_mux_abr_seq_r_event_on( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,abr_seq_r_event_on,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,abr_seq_r_event_on,idleV);
}
void TpuMem::outtpu_trig_mux_seq_abr_trig_a_only( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,seq_abr_trig_a_only,value);
}
void TpuMem::pulsetpu_trig_mux_seq_abr_trig_a_only( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(tpu_trig_mux,seq_abr_trig_a_only,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(tpu_trig_mux,seq_abr_trig_a_only,idleV);
}

void TpuMem::outtpu_trig_mux( Tpu_reg value )
{
	out_assign_field(tpu_trig_mux,payload,value);
}

void TpuMem::outtpu_trig_mux()
{
	out_member(tpu_trig_mux);
}


Tpu_reg TpuMem::intpu_trig_mux()
{
	reg_in(tpu_trig_mux);
	return tpu_trig_mux.payload;
}


void TpuMem::setabr_delay_time_l_abr_delay_time_l( Tpu_reg value )
{
	reg_assign_field(abr_delay_time_l,abr_delay_time_l,value);
}

void TpuMem::setabr_delay_time_l( Tpu_reg value )
{
	reg_assign_field(abr_delay_time_l,payload,value);
}

void TpuMem::outabr_delay_time_l_abr_delay_time_l( Tpu_reg value )
{
	out_assign_field(abr_delay_time_l,abr_delay_time_l,value);
}
void TpuMem::pulseabr_delay_time_l_abr_delay_time_l( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(abr_delay_time_l,abr_delay_time_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(abr_delay_time_l,abr_delay_time_l,idleV);
}

void TpuMem::outabr_delay_time_l( Tpu_reg value )
{
	out_assign_field(abr_delay_time_l,payload,value);
}

void TpuMem::outabr_delay_time_l()
{
	out_member(abr_delay_time_l);
}


void TpuMem::setabr_delay_time_h_abr_delay_time_h( Tpu_reg value )
{
	reg_assign_field(abr_delay_time_h,abr_delay_time_h,value);
}

void TpuMem::setabr_delay_time_h( Tpu_reg value )
{
	reg_assign_field(abr_delay_time_h,payload,value);
}

void TpuMem::outabr_delay_time_h_abr_delay_time_h( Tpu_reg value )
{
	out_assign_field(abr_delay_time_h,abr_delay_time_h,value);
}
void TpuMem::pulseabr_delay_time_h_abr_delay_time_h( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(abr_delay_time_h,abr_delay_time_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(abr_delay_time_h,abr_delay_time_h,idleV);
}

void TpuMem::outabr_delay_time_h( Tpu_reg value )
{
	out_assign_field(abr_delay_time_h,payload,value);
}

void TpuMem::outabr_delay_time_h()
{
	out_member(abr_delay_time_h);
}


void TpuMem::setabr_timeout_l_abr_timeout_l( Tpu_reg value )
{
	reg_assign_field(abr_timeout_l,abr_timeout_l,value);
}

void TpuMem::setabr_timeout_l( Tpu_reg value )
{
	reg_assign_field(abr_timeout_l,payload,value);
}

void TpuMem::outabr_timeout_l_abr_timeout_l( Tpu_reg value )
{
	out_assign_field(abr_timeout_l,abr_timeout_l,value);
}
void TpuMem::pulseabr_timeout_l_abr_timeout_l( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(abr_timeout_l,abr_timeout_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(abr_timeout_l,abr_timeout_l,idleV);
}

void TpuMem::outabr_timeout_l( Tpu_reg value )
{
	out_assign_field(abr_timeout_l,payload,value);
}

void TpuMem::outabr_timeout_l()
{
	out_member(abr_timeout_l);
}


void TpuMem::setabr_timeout_h_abr_timeout_h( Tpu_reg value )
{
	reg_assign_field(abr_timeout_h,abr_timeout_h,value);
}

void TpuMem::setabr_timeout_h( Tpu_reg value )
{
	reg_assign_field(abr_timeout_h,payload,value);
}

void TpuMem::outabr_timeout_h_abr_timeout_h( Tpu_reg value )
{
	out_assign_field(abr_timeout_h,abr_timeout_h,value);
}
void TpuMem::pulseabr_timeout_h_abr_timeout_h( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(abr_timeout_h,abr_timeout_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(abr_timeout_h,abr_timeout_h,idleV);
}

void TpuMem::outabr_timeout_h( Tpu_reg value )
{
	out_assign_field(abr_timeout_h,payload,value);
}

void TpuMem::outabr_timeout_h()
{
	out_member(abr_timeout_h);
}


void TpuMem::setabr_b_event_num( Tpu_reg value )
{
	reg_assign(abr_b_event_num,value);
}

void TpuMem::outabr_b_event_num( Tpu_reg value )
{
	out_assign(abr_b_event_num,value);
}

void TpuMem::outabr_b_event_num()
{
	out_member(abr_b_event_num);
}


void TpuMem::setpreprocess_type( Tpu_reg value )
{
	reg_assign(preprocess_type,value);
}

void TpuMem::outpreprocess_type( Tpu_reg value )
{
	out_assign(preprocess_type,value);
}

void TpuMem::outpreprocess_type()
{
	out_member(preprocess_type);
}


void TpuMem::setabr_a_event_num( Tpu_reg value )
{
	reg_assign(abr_a_event_num,value);
}

void TpuMem::outabr_a_event_num( Tpu_reg value )
{
	out_assign(abr_a_event_num,value);
}

void TpuMem::outabr_a_event_num()
{
	out_member(abr_a_event_num);
}


void TpuMem::setholdoff_event_num( Tpu_reg value )
{
	reg_assign(holdoff_event_num,value);
}

void TpuMem::outholdoff_event_num( Tpu_reg value )
{
	out_assign(holdoff_event_num,value);
}

void TpuMem::outholdoff_event_num()
{
	out_member(holdoff_event_num);
}


Tpu_reg TpuMem::inholdoff_event_num()
{
	reg_in(holdoff_event_num);
	return holdoff_event_num;
}


void TpuMem::setholdoff_time_l_holdoff_time_l( Tpu_reg value )
{
	reg_assign_field(holdoff_time_l,holdoff_time_l,value);
}

void TpuMem::setholdoff_time_l( Tpu_reg value )
{
	reg_assign_field(holdoff_time_l,payload,value);
}

void TpuMem::outholdoff_time_l_holdoff_time_l( Tpu_reg value )
{
	out_assign_field(holdoff_time_l,holdoff_time_l,value);
}
void TpuMem::pulseholdoff_time_l_holdoff_time_l( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(holdoff_time_l,holdoff_time_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(holdoff_time_l,holdoff_time_l,idleV);
}

void TpuMem::outholdoff_time_l( Tpu_reg value )
{
	out_assign_field(holdoff_time_l,payload,value);
}

void TpuMem::outholdoff_time_l()
{
	out_member(holdoff_time_l);
}


Tpu_reg TpuMem::inholdoff_time_l()
{
	reg_in(holdoff_time_l);
	return holdoff_time_l.payload;
}


void TpuMem::setholdoff_time_h_holdoff_time_h( Tpu_reg value )
{
	reg_assign_field(holdoff_time_h,holdoff_time_h,value);
}
void TpuMem::setholdoff_time_h_holdoff_type( Tpu_reg value )
{
	reg_assign_field(holdoff_time_h,holdoff_type,value);
}

void TpuMem::setholdoff_time_h( Tpu_reg value )
{
	reg_assign_field(holdoff_time_h,payload,value);
}

void TpuMem::outholdoff_time_h_holdoff_time_h( Tpu_reg value )
{
	out_assign_field(holdoff_time_h,holdoff_time_h,value);
}
void TpuMem::pulseholdoff_time_h_holdoff_time_h( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(holdoff_time_h,holdoff_time_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(holdoff_time_h,holdoff_time_h,idleV);
}
void TpuMem::outholdoff_time_h_holdoff_type( Tpu_reg value )
{
	out_assign_field(holdoff_time_h,holdoff_type,value);
}
void TpuMem::pulseholdoff_time_h_holdoff_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(holdoff_time_h,holdoff_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(holdoff_time_h,holdoff_type,idleV);
}

void TpuMem::outholdoff_time_h( Tpu_reg value )
{
	out_assign_field(holdoff_time_h,payload,value);
}

void TpuMem::outholdoff_time_h()
{
	out_member(holdoff_time_h);
}


Tpu_reg TpuMem::inholdoff_time_h()
{
	reg_in(holdoff_time_h);
	return holdoff_time_h.payload;
}


void TpuMem::setiir_set_iir_type( Tpu_reg value )
{
	reg_assign_field(iir_set,iir_type,value);
}
void TpuMem::setiir_set_iir_src( Tpu_reg value )
{
	reg_assign_field(iir_set,iir_src,value);
}
void TpuMem::setiir_set_iir_offset( Tpu_reg value )
{
	reg_assign_field(iir_set,iir_offset,value);
}

void TpuMem::setiir_set( Tpu_reg value )
{
	reg_assign_field(iir_set,payload,value);
}

void TpuMem::outiir_set_iir_type( Tpu_reg value )
{
	out_assign_field(iir_set,iir_type,value);
}
void TpuMem::pulseiir_set_iir_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iir_set,iir_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iir_set,iir_type,idleV);
}
void TpuMem::outiir_set_iir_src( Tpu_reg value )
{
	out_assign_field(iir_set,iir_src,value);
}
void TpuMem::pulseiir_set_iir_src( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iir_set,iir_src,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iir_set,iir_src,idleV);
}
void TpuMem::outiir_set_iir_offset( Tpu_reg value )
{
	out_assign_field(iir_set,iir_offset,value);
}
void TpuMem::pulseiir_set_iir_offset( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iir_set,iir_offset,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iir_set,iir_offset,idleV);
}

void TpuMem::outiir_set( Tpu_reg value )
{
	out_assign_field(iir_set,payload,value);
}

void TpuMem::outiir_set()
{
	out_member(iir_set);
}


Tpu_reg TpuMem::iniir_set()
{
	reg_in(iir_set);
	return iir_set.payload;
}


void TpuMem::setuart_cfg_1_uart_clk_div( Tpu_reg value )
{
	reg_assign_field(uart_cfg_1,uart_clk_div,value);
}
void TpuMem::setuart_cfg_1_uart_trig_type( Tpu_reg value )
{
	reg_assign_field(uart_cfg_1,uart_trig_type,value);
}
void TpuMem::setuart_cfg_1_uart_trig_dat( Tpu_reg value )
{
	reg_assign_field(uart_cfg_1,uart_trig_dat,value);
}
void TpuMem::setuart_cfg_1_uart_trig_err( Tpu_reg value )
{
	reg_assign_field(uart_cfg_1,uart_trig_err,value);
}
void TpuMem::setuart_cfg_1_uart_eof( Tpu_reg value )
{
	reg_assign_field(uart_cfg_1,uart_eof,value);
}
void TpuMem::setuart_cfg_1_uart_parity( Tpu_reg value )
{
	reg_assign_field(uart_cfg_1,uart_parity,value);
}

void TpuMem::setuart_cfg_1( Tpu_reg value )
{
	reg_assign_field(uart_cfg_1,payload,value);
}

void TpuMem::outuart_cfg_1_uart_clk_div( Tpu_reg value )
{
	out_assign_field(uart_cfg_1,uart_clk_div,value);
}
void TpuMem::pulseuart_cfg_1_uart_clk_div( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_1,uart_clk_div,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_1,uart_clk_div,idleV);
}
void TpuMem::outuart_cfg_1_uart_trig_type( Tpu_reg value )
{
	out_assign_field(uart_cfg_1,uart_trig_type,value);
}
void TpuMem::pulseuart_cfg_1_uart_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_1,uart_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_1,uart_trig_type,idleV);
}
void TpuMem::outuart_cfg_1_uart_trig_dat( Tpu_reg value )
{
	out_assign_field(uart_cfg_1,uart_trig_dat,value);
}
void TpuMem::pulseuart_cfg_1_uart_trig_dat( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_1,uart_trig_dat,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_1,uart_trig_dat,idleV);
}
void TpuMem::outuart_cfg_1_uart_trig_err( Tpu_reg value )
{
	out_assign_field(uart_cfg_1,uart_trig_err,value);
}
void TpuMem::pulseuart_cfg_1_uart_trig_err( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_1,uart_trig_err,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_1,uart_trig_err,idleV);
}
void TpuMem::outuart_cfg_1_uart_eof( Tpu_reg value )
{
	out_assign_field(uart_cfg_1,uart_eof,value);
}
void TpuMem::pulseuart_cfg_1_uart_eof( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_1,uart_eof,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_1,uart_eof,idleV);
}
void TpuMem::outuart_cfg_1_uart_parity( Tpu_reg value )
{
	out_assign_field(uart_cfg_1,uart_parity,value);
}
void TpuMem::pulseuart_cfg_1_uart_parity( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_1,uart_parity,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_1,uart_parity,idleV);
}

void TpuMem::outuart_cfg_1( Tpu_reg value )
{
	out_assign_field(uart_cfg_1,payload,value);
}

void TpuMem::outuart_cfg_1()
{
	out_member(uart_cfg_1);
}


void TpuMem::setuart_cfg_2_uart_len( Tpu_reg value )
{
	reg_assign_field(uart_cfg_2,uart_len,value);
}
void TpuMem::setuart_cfg_2_uart_dat_min( Tpu_reg value )
{
	reg_assign_field(uart_cfg_2,uart_dat_min,value);
}
void TpuMem::setuart_cfg_2_uart_dat_max( Tpu_reg value )
{
	reg_assign_field(uart_cfg_2,uart_dat_max,value);
}

void TpuMem::setuart_cfg_2( Tpu_reg value )
{
	reg_assign_field(uart_cfg_2,payload,value);
}

void TpuMem::outuart_cfg_2_uart_len( Tpu_reg value )
{
	out_assign_field(uart_cfg_2,uart_len,value);
}
void TpuMem::pulseuart_cfg_2_uart_len( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_2,uart_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_2,uart_len,idleV);
}
void TpuMem::outuart_cfg_2_uart_dat_min( Tpu_reg value )
{
	out_assign_field(uart_cfg_2,uart_dat_min,value);
}
void TpuMem::pulseuart_cfg_2_uart_dat_min( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_2,uart_dat_min,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_2,uart_dat_min,idleV);
}
void TpuMem::outuart_cfg_2_uart_dat_max( Tpu_reg value )
{
	out_assign_field(uart_cfg_2,uart_dat_max,value);
}
void TpuMem::pulseuart_cfg_2_uart_dat_max( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(uart_cfg_2,uart_dat_max,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(uart_cfg_2,uart_dat_max,idleV);
}

void TpuMem::outuart_cfg_2( Tpu_reg value )
{
	out_assign_field(uart_cfg_2,payload,value);
}

void TpuMem::outuart_cfg_2()
{
	out_member(uart_cfg_2);
}


void TpuMem::setiis_cfg_1_iis_trig_type( Tpu_reg value )
{
	reg_assign_field(iis_cfg_1,iis_trig_type,value);
}
void TpuMem::setiis_cfg_1_iis_trig_channel_sel( Tpu_reg value )
{
	reg_assign_field(iis_cfg_1,iis_trig_channel_sel,value);
}
void TpuMem::setiis_cfg_1_iis_trig_bus_type( Tpu_reg value )
{
	reg_assign_field(iis_cfg_1,iis_trig_bus_type,value);
}
void TpuMem::setiis_cfg_1_iis_trig_bitnum_vld( Tpu_reg value )
{
	reg_assign_field(iis_cfg_1,iis_trig_bitnum_vld,value);
}
void TpuMem::setiis_cfg_1_iis_trig_bitnum_total( Tpu_reg value )
{
	reg_assign_field(iis_cfg_1,iis_trig_bitnum_total,value);
}

void TpuMem::setiis_cfg_1( Tpu_reg value )
{
	reg_assign_field(iis_cfg_1,payload,value);
}

void TpuMem::outiis_cfg_1_iis_trig_type( Tpu_reg value )
{
	out_assign_field(iis_cfg_1,iis_trig_type,value);
}
void TpuMem::pulseiis_cfg_1_iis_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_1,iis_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_1,iis_trig_type,idleV);
}
void TpuMem::outiis_cfg_1_iis_trig_channel_sel( Tpu_reg value )
{
	out_assign_field(iis_cfg_1,iis_trig_channel_sel,value);
}
void TpuMem::pulseiis_cfg_1_iis_trig_channel_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_1,iis_trig_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_1,iis_trig_channel_sel,idleV);
}
void TpuMem::outiis_cfg_1_iis_trig_bus_type( Tpu_reg value )
{
	out_assign_field(iis_cfg_1,iis_trig_bus_type,value);
}
void TpuMem::pulseiis_cfg_1_iis_trig_bus_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_1,iis_trig_bus_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_1,iis_trig_bus_type,idleV);
}
void TpuMem::outiis_cfg_1_iis_trig_bitnum_vld( Tpu_reg value )
{
	out_assign_field(iis_cfg_1,iis_trig_bitnum_vld,value);
}
void TpuMem::pulseiis_cfg_1_iis_trig_bitnum_vld( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_1,iis_trig_bitnum_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_1,iis_trig_bitnum_vld,idleV);
}
void TpuMem::outiis_cfg_1_iis_trig_bitnum_total( Tpu_reg value )
{
	out_assign_field(iis_cfg_1,iis_trig_bitnum_total,value);
}
void TpuMem::pulseiis_cfg_1_iis_trig_bitnum_total( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_1,iis_trig_bitnum_total,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_1,iis_trig_bitnum_total,idleV);
}

void TpuMem::outiis_cfg_1( Tpu_reg value )
{
	out_assign_field(iis_cfg_1,payload,value);
}

void TpuMem::outiis_cfg_1()
{
	out_member(iis_cfg_1);
}


void TpuMem::setiis_cfg_2_iis_trig_word_min( Tpu_reg value )
{
	reg_assign_field(iis_cfg_2,iis_trig_word_min,value);
}

void TpuMem::setiis_cfg_2( Tpu_reg value )
{
	reg_assign_field(iis_cfg_2,payload,value);
}

void TpuMem::outiis_cfg_2_iis_trig_word_min( Tpu_reg value )
{
	out_assign_field(iis_cfg_2,iis_trig_word_min,value);
}
void TpuMem::pulseiis_cfg_2_iis_trig_word_min( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_2,iis_trig_word_min,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_2,iis_trig_word_min,idleV);
}

void TpuMem::outiis_cfg_2( Tpu_reg value )
{
	out_assign_field(iis_cfg_2,payload,value);
}

void TpuMem::outiis_cfg_2()
{
	out_member(iis_cfg_2);
}


void TpuMem::setiis_cfg_3_iis_trig_word_max( Tpu_reg value )
{
	reg_assign_field(iis_cfg_3,iis_trig_word_max,value);
}

void TpuMem::setiis_cfg_3( Tpu_reg value )
{
	reg_assign_field(iis_cfg_3,payload,value);
}

void TpuMem::outiis_cfg_3_iis_trig_word_max( Tpu_reg value )
{
	out_assign_field(iis_cfg_3,iis_trig_word_max,value);
}
void TpuMem::pulseiis_cfg_3_iis_trig_word_max( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_3,iis_trig_word_max,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_3,iis_trig_word_max,idleV);
}

void TpuMem::outiis_cfg_3( Tpu_reg value )
{
	out_assign_field(iis_cfg_3,payload,value);
}

void TpuMem::outiis_cfg_3()
{
	out_member(iis_cfg_3);
}


void TpuMem::setiis_cfg_4_iis_trig_word_mask( Tpu_reg value )
{
	reg_assign_field(iis_cfg_4,iis_trig_word_mask,value);
}

void TpuMem::setiis_cfg_4( Tpu_reg value )
{
	reg_assign_field(iis_cfg_4,payload,value);
}

void TpuMem::outiis_cfg_4_iis_trig_word_mask( Tpu_reg value )
{
	out_assign_field(iis_cfg_4,iis_trig_word_mask,value);
}
void TpuMem::pulseiis_cfg_4_iis_trig_word_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iis_cfg_4,iis_trig_word_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iis_cfg_4,iis_trig_word_mask,idleV);
}

void TpuMem::outiis_cfg_4( Tpu_reg value )
{
	out_assign_field(iis_cfg_4,payload,value);
}

void TpuMem::outiis_cfg_4()
{
	out_member(iis_cfg_4);
}


void TpuMem::setlin_cfg_1_lin_trig_type( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_type,value);
}
void TpuMem::setlin_cfg_1_lin_trig_type_id( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_type_id,value);
}
void TpuMem::setlin_cfg_1_lin_trig_type_dat( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_type_dat,value);
}
void TpuMem::setlin_cfg_1_lin_trig_type_err( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_type_err,value);
}
void TpuMem::setlin_cfg_1_lin_trig_spec( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_spec,value);
}
void TpuMem::setlin_cfg_1_lin_trig_id_min( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_id_min,value);
}
void TpuMem::setlin_cfg_1_lin_trig_id_max( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_id_max,value);
}
void TpuMem::setlin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,lin_trig_dat_cmpnum,value);
}

void TpuMem::setlin_cfg_1( Tpu_reg value )
{
	reg_assign_field(lin_cfg_1,payload,value);
}

void TpuMem::outlin_cfg_1_lin_trig_type( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_type,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_type,idleV);
}
void TpuMem::outlin_cfg_1_lin_trig_type_id( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_type_id,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_type_id( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_type_id,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_type_id,idleV);
}
void TpuMem::outlin_cfg_1_lin_trig_type_dat( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_type_dat,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_type_dat( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_type_dat,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_type_dat,idleV);
}
void TpuMem::outlin_cfg_1_lin_trig_type_err( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_type_err,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_type_err( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_type_err,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_type_err,idleV);
}
void TpuMem::outlin_cfg_1_lin_trig_spec( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_spec,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_spec( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_spec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_spec,idleV);
}
void TpuMem::outlin_cfg_1_lin_trig_id_min( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_id_min,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_id_min( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_id_min,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_id_min,idleV);
}
void TpuMem::outlin_cfg_1_lin_trig_id_max( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_id_max,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_id_max( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_id_max,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_id_max,idleV);
}
void TpuMem::outlin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,lin_trig_dat_cmpnum,value);
}
void TpuMem::pulselin_cfg_1_lin_trig_dat_cmpnum( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_1,lin_trig_dat_cmpnum,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_1,lin_trig_dat_cmpnum,idleV);
}

void TpuMem::outlin_cfg_1( Tpu_reg value )
{
	out_assign_field(lin_cfg_1,payload,value);
}

void TpuMem::outlin_cfg_1()
{
	out_member(lin_cfg_1);
}


void TpuMem::setlin_cfg_2_lin_trig_clk_div( Tpu_reg value )
{
	reg_assign_field(lin_cfg_2,lin_trig_clk_div,value);
}

void TpuMem::setlin_cfg_2( Tpu_reg value )
{
	reg_assign_field(lin_cfg_2,payload,value);
}

void TpuMem::outlin_cfg_2_lin_trig_clk_div( Tpu_reg value )
{
	out_assign_field(lin_cfg_2,lin_trig_clk_div,value);
}
void TpuMem::pulselin_cfg_2_lin_trig_clk_div( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_2,lin_trig_clk_div,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_2,lin_trig_clk_div,idleV);
}

void TpuMem::outlin_cfg_2( Tpu_reg value )
{
	out_assign_field(lin_cfg_2,payload,value);
}

void TpuMem::outlin_cfg_2()
{
	out_member(lin_cfg_2);
}


void TpuMem::setlin_cfg_3_lin_trig_sa_pos( Tpu_reg value )
{
	reg_assign_field(lin_cfg_3,lin_trig_sa_pos,value);
}

void TpuMem::setlin_cfg_3( Tpu_reg value )
{
	reg_assign_field(lin_cfg_3,payload,value);
}

void TpuMem::outlin_cfg_3_lin_trig_sa_pos( Tpu_reg value )
{
	out_assign_field(lin_cfg_3,lin_trig_sa_pos,value);
}
void TpuMem::pulselin_cfg_3_lin_trig_sa_pos( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_3,lin_trig_sa_pos,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_3,lin_trig_sa_pos,idleV);
}

void TpuMem::outlin_cfg_3( Tpu_reg value )
{
	out_assign_field(lin_cfg_3,payload,value);
}

void TpuMem::outlin_cfg_3()
{
	out_member(lin_cfg_3);
}


void TpuMem::setlin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg value )
{
	reg_assign_field(lin_cfg_4,lin_trig_dat_min_bits31_0,value);
}

void TpuMem::setlin_cfg_4( Tpu_reg value )
{
	reg_assign_field(lin_cfg_4,payload,value);
}

void TpuMem::outlin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg value )
{
	out_assign_field(lin_cfg_4,lin_trig_dat_min_bits31_0,value);
}
void TpuMem::pulselin_cfg_4_lin_trig_dat_min_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_4,lin_trig_dat_min_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_4,lin_trig_dat_min_bits31_0,idleV);
}

void TpuMem::outlin_cfg_4( Tpu_reg value )
{
	out_assign_field(lin_cfg_4,payload,value);
}

void TpuMem::outlin_cfg_4()
{
	out_member(lin_cfg_4);
}


void TpuMem::setlin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg value )
{
	reg_assign_field(lin_cfg_5,lin_trig_dat_min_bits63_32,value);
}

void TpuMem::setlin_cfg_5( Tpu_reg value )
{
	reg_assign_field(lin_cfg_5,payload,value);
}

void TpuMem::outlin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg value )
{
	out_assign_field(lin_cfg_5,lin_trig_dat_min_bits63_32,value);
}
void TpuMem::pulselin_cfg_5_lin_trig_dat_min_bits63_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_5,lin_trig_dat_min_bits63_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_5,lin_trig_dat_min_bits63_32,idleV);
}

void TpuMem::outlin_cfg_5( Tpu_reg value )
{
	out_assign_field(lin_cfg_5,payload,value);
}

void TpuMem::outlin_cfg_5()
{
	out_member(lin_cfg_5);
}


void TpuMem::setlin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg value )
{
	reg_assign_field(lin_cfg_6,lin_trig_dat_max_bits31_0,value);
}

void TpuMem::setlin_cfg_6( Tpu_reg value )
{
	reg_assign_field(lin_cfg_6,payload,value);
}

void TpuMem::outlin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg value )
{
	out_assign_field(lin_cfg_6,lin_trig_dat_max_bits31_0,value);
}
void TpuMem::pulselin_cfg_6_lin_trig_dat_max_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_6,lin_trig_dat_max_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_6,lin_trig_dat_max_bits31_0,idleV);
}

void TpuMem::outlin_cfg_6( Tpu_reg value )
{
	out_assign_field(lin_cfg_6,payload,value);
}

void TpuMem::outlin_cfg_6()
{
	out_member(lin_cfg_6);
}


void TpuMem::setlin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg value )
{
	reg_assign_field(lin_cfg_7,lin_trig_dat_max_bits63_32,value);
}

void TpuMem::setlin_cfg_7( Tpu_reg value )
{
	reg_assign_field(lin_cfg_7,payload,value);
}

void TpuMem::outlin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg value )
{
	out_assign_field(lin_cfg_7,lin_trig_dat_max_bits63_32,value);
}
void TpuMem::pulselin_cfg_7_lin_trig_dat_max_bits63_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_7,lin_trig_dat_max_bits63_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_7,lin_trig_dat_max_bits63_32,idleV);
}

void TpuMem::outlin_cfg_7( Tpu_reg value )
{
	out_assign_field(lin_cfg_7,payload,value);
}

void TpuMem::outlin_cfg_7()
{
	out_member(lin_cfg_7);
}


void TpuMem::setlin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg value )
{
	reg_assign_field(lin_cfg_8,lin_trig_dat_mask_bits31_0,value);
}

void TpuMem::setlin_cfg_8( Tpu_reg value )
{
	reg_assign_field(lin_cfg_8,payload,value);
}

void TpuMem::outlin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg value )
{
	out_assign_field(lin_cfg_8,lin_trig_dat_mask_bits31_0,value);
}
void TpuMem::pulselin_cfg_8_lin_trig_dat_mask_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_8,lin_trig_dat_mask_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_8,lin_trig_dat_mask_bits31_0,idleV);
}

void TpuMem::outlin_cfg_8( Tpu_reg value )
{
	out_assign_field(lin_cfg_8,payload,value);
}

void TpuMem::outlin_cfg_8()
{
	out_member(lin_cfg_8);
}


void TpuMem::setlin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg value )
{
	reg_assign_field(lin_cfg_9,lin_trig_dat_maak_bits63_32,value);
}

void TpuMem::setlin_cfg_9( Tpu_reg value )
{
	reg_assign_field(lin_cfg_9,payload,value);
}

void TpuMem::outlin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg value )
{
	out_assign_field(lin_cfg_9,lin_trig_dat_maak_bits63_32,value);
}
void TpuMem::pulselin_cfg_9_lin_trig_dat_maak_bits63_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(lin_cfg_9,lin_trig_dat_maak_bits63_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(lin_cfg_9,lin_trig_dat_maak_bits63_32,idleV);
}

void TpuMem::outlin_cfg_9( Tpu_reg value )
{
	out_assign_field(lin_cfg_9,payload,value);
}

void TpuMem::outlin_cfg_9()
{
	out_member(lin_cfg_9);
}


void TpuMem::setcan_cfg_1_can_trig_type( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type,value);
}
void TpuMem::setcan_cfg_1_can_trig_type_field( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type_field,value);
}
void TpuMem::setcan_cfg_1_can_trig_type_frame( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type_frame,value);
}
void TpuMem::setcan_cfg_1_can_trig_type_err( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type_err,value);
}
void TpuMem::setcan_cfg_1_can_trig_type_id( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type_id,value);
}
void TpuMem::setcan_cfg_1_can_trig_type_id_mask( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type_id_mask,value);
}
void TpuMem::setcan_cfg_1_can_trig_type_dat( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type_dat,value);
}
void TpuMem::setcan_cfg_1_can_trig_type_dat_mask( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_type_dat_mask,value);
}
void TpuMem::setcan_cfg_1_can_trig_bus_type( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_bus_type,value);
}
void TpuMem::setcan_cfg_1_can_trig_std_extend_sel( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_std_extend_sel,value);
}
void TpuMem::setcan_cfg_1_can_trig_dat_cmpnum( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,can_trig_dat_cmpnum,value);
}

void TpuMem::setcan_cfg_1( Tpu_reg value )
{
	reg_assign_field(can_cfg_1,payload,value);
}

void TpuMem::outcan_cfg_1_can_trig_type( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_type_field( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type_field,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type_field( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type_field,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type_field,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_type_frame( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type_frame,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type_frame( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type_frame,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type_frame,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_type_err( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type_err,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type_err( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type_err,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type_err,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_type_id( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type_id,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type_id( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type_id,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type_id,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_type_id_mask( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type_id_mask,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type_id_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type_id_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type_id_mask,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_type_dat( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type_dat,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type_dat( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type_dat,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type_dat,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_type_dat_mask( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_type_dat_mask,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_type_dat_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_type_dat_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_type_dat_mask,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_bus_type( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_bus_type,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_bus_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_bus_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_bus_type,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_std_extend_sel( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_std_extend_sel,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_std_extend_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_std_extend_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_std_extend_sel,idleV);
}
void TpuMem::outcan_cfg_1_can_trig_dat_cmpnum( Tpu_reg value )
{
	out_assign_field(can_cfg_1,can_trig_dat_cmpnum,value);
}
void TpuMem::pulsecan_cfg_1_can_trig_dat_cmpnum( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_1,can_trig_dat_cmpnum,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_1,can_trig_dat_cmpnum,idleV);
}

void TpuMem::outcan_cfg_1( Tpu_reg value )
{
	out_assign_field(can_cfg_1,payload,value);
}

void TpuMem::outcan_cfg_1()
{
	out_member(can_cfg_1);
}


void TpuMem::setcan_cfg_2_can_trig_clk_div( Tpu_reg value )
{
	reg_assign_field(can_cfg_2,can_trig_clk_div,value);
}
void TpuMem::setcan_cfg_2_can_trig_sa_pos( Tpu_reg value )
{
	reg_assign_field(can_cfg_2,can_trig_sa_pos,value);
}

void TpuMem::setcan_cfg_2( Tpu_reg value )
{
	reg_assign_field(can_cfg_2,payload,value);
}

void TpuMem::outcan_cfg_2_can_trig_clk_div( Tpu_reg value )
{
	out_assign_field(can_cfg_2,can_trig_clk_div,value);
}
void TpuMem::pulsecan_cfg_2_can_trig_clk_div( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_2,can_trig_clk_div,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_2,can_trig_clk_div,idleV);
}
void TpuMem::outcan_cfg_2_can_trig_sa_pos( Tpu_reg value )
{
	out_assign_field(can_cfg_2,can_trig_sa_pos,value);
}
void TpuMem::pulsecan_cfg_2_can_trig_sa_pos( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_2,can_trig_sa_pos,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_2,can_trig_sa_pos,idleV);
}

void TpuMem::outcan_cfg_2( Tpu_reg value )
{
	out_assign_field(can_cfg_2,payload,value);
}

void TpuMem::outcan_cfg_2()
{
	out_member(can_cfg_2);
}


void TpuMem::setcan_cfg_3_can_trig_id_min( Tpu_reg value )
{
	reg_assign_field(can_cfg_3,can_trig_id_min,value);
}

void TpuMem::setcan_cfg_3( Tpu_reg value )
{
	reg_assign_field(can_cfg_3,payload,value);
}

void TpuMem::outcan_cfg_3_can_trig_id_min( Tpu_reg value )
{
	out_assign_field(can_cfg_3,can_trig_id_min,value);
}
void TpuMem::pulsecan_cfg_3_can_trig_id_min( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_3,can_trig_id_min,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_3,can_trig_id_min,idleV);
}

void TpuMem::outcan_cfg_3( Tpu_reg value )
{
	out_assign_field(can_cfg_3,payload,value);
}

void TpuMem::outcan_cfg_3()
{
	out_member(can_cfg_3);
}


void TpuMem::setcan_cfg_4_can_trig_id_max( Tpu_reg value )
{
	reg_assign_field(can_cfg_4,can_trig_id_max,value);
}

void TpuMem::setcan_cfg_4( Tpu_reg value )
{
	reg_assign_field(can_cfg_4,payload,value);
}

void TpuMem::outcan_cfg_4_can_trig_id_max( Tpu_reg value )
{
	out_assign_field(can_cfg_4,can_trig_id_max,value);
}
void TpuMem::pulsecan_cfg_4_can_trig_id_max( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_4,can_trig_id_max,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_4,can_trig_id_max,idleV);
}

void TpuMem::outcan_cfg_4( Tpu_reg value )
{
	out_assign_field(can_cfg_4,payload,value);
}

void TpuMem::outcan_cfg_4()
{
	out_member(can_cfg_4);
}


void TpuMem::setcan_cfg_5_can_trig_id_mask( Tpu_reg value )
{
	reg_assign_field(can_cfg_5,can_trig_id_mask,value);
}

void TpuMem::setcan_cfg_5( Tpu_reg value )
{
	reg_assign_field(can_cfg_5,payload,value);
}

void TpuMem::outcan_cfg_5_can_trig_id_mask( Tpu_reg value )
{
	out_assign_field(can_cfg_5,can_trig_id_mask,value);
}
void TpuMem::pulsecan_cfg_5_can_trig_id_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_5,can_trig_id_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_5,can_trig_id_mask,idleV);
}

void TpuMem::outcan_cfg_5( Tpu_reg value )
{
	out_assign_field(can_cfg_5,payload,value);
}

void TpuMem::outcan_cfg_5()
{
	out_member(can_cfg_5);
}


void TpuMem::setcan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg value )
{
	reg_assign_field(can_cfg_6,can_trig_dat_min_bits31_0,value);
}

void TpuMem::setcan_cfg_6( Tpu_reg value )
{
	reg_assign_field(can_cfg_6,payload,value);
}

void TpuMem::outcan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg value )
{
	out_assign_field(can_cfg_6,can_trig_dat_min_bits31_0,value);
}
void TpuMem::pulsecan_cfg_6_can_trig_dat_min_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_6,can_trig_dat_min_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_6,can_trig_dat_min_bits31_0,idleV);
}

void TpuMem::outcan_cfg_6( Tpu_reg value )
{
	out_assign_field(can_cfg_6,payload,value);
}

void TpuMem::outcan_cfg_6()
{
	out_member(can_cfg_6);
}


void TpuMem::setcan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg value )
{
	reg_assign_field(can_cfg_7,can_trig_dat_min_bits63_32,value);
}

void TpuMem::setcan_cfg_7( Tpu_reg value )
{
	reg_assign_field(can_cfg_7,payload,value);
}

void TpuMem::outcan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg value )
{
	out_assign_field(can_cfg_7,can_trig_dat_min_bits63_32,value);
}
void TpuMem::pulsecan_cfg_7_can_trig_dat_min_bits63_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_7,can_trig_dat_min_bits63_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_7,can_trig_dat_min_bits63_32,idleV);
}

void TpuMem::outcan_cfg_7( Tpu_reg value )
{
	out_assign_field(can_cfg_7,payload,value);
}

void TpuMem::outcan_cfg_7()
{
	out_member(can_cfg_7);
}


void TpuMem::setcan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg value )
{
	reg_assign_field(can_cfg_8,can_trig_dat_max_bits31_0,value);
}

void TpuMem::setcan_cfg_8( Tpu_reg value )
{
	reg_assign_field(can_cfg_8,payload,value);
}

void TpuMem::outcan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg value )
{
	out_assign_field(can_cfg_8,can_trig_dat_max_bits31_0,value);
}
void TpuMem::pulsecan_cfg_8_can_trig_dat_max_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_8,can_trig_dat_max_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_8,can_trig_dat_max_bits31_0,idleV);
}

void TpuMem::outcan_cfg_8( Tpu_reg value )
{
	out_assign_field(can_cfg_8,payload,value);
}

void TpuMem::outcan_cfg_8()
{
	out_member(can_cfg_8);
}


void TpuMem::setcan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg value )
{
	reg_assign_field(can_cfg_9,can_trig_dat_max_bits63_32,value);
}

void TpuMem::setcan_cfg_9( Tpu_reg value )
{
	reg_assign_field(can_cfg_9,payload,value);
}

void TpuMem::outcan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg value )
{
	out_assign_field(can_cfg_9,can_trig_dat_max_bits63_32,value);
}
void TpuMem::pulsecan_cfg_9_can_trig_dat_max_bits63_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_9,can_trig_dat_max_bits63_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_9,can_trig_dat_max_bits63_32,idleV);
}

void TpuMem::outcan_cfg_9( Tpu_reg value )
{
	out_assign_field(can_cfg_9,payload,value);
}

void TpuMem::outcan_cfg_9()
{
	out_member(can_cfg_9);
}


void TpuMem::setcan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg value )
{
	reg_assign_field(can_cfg_10,can_trig_dat_mask_bits31_0,value);
}

void TpuMem::setcan_cfg_10( Tpu_reg value )
{
	reg_assign_field(can_cfg_10,payload,value);
}

void TpuMem::outcan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg value )
{
	out_assign_field(can_cfg_10,can_trig_dat_mask_bits31_0,value);
}
void TpuMem::pulsecan_cfg_10_can_trig_dat_mask_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_10,can_trig_dat_mask_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_10,can_trig_dat_mask_bits31_0,idleV);
}

void TpuMem::outcan_cfg_10( Tpu_reg value )
{
	out_assign_field(can_cfg_10,payload,value);
}

void TpuMem::outcan_cfg_10()
{
	out_member(can_cfg_10);
}


void TpuMem::setcan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg value )
{
	reg_assign_field(can_cfg_11,can_trig_dat_mask_bits63_32,value);
}

void TpuMem::setcan_cfg_11( Tpu_reg value )
{
	reg_assign_field(can_cfg_11,payload,value);
}

void TpuMem::outcan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg value )
{
	out_assign_field(can_cfg_11,can_trig_dat_mask_bits63_32,value);
}
void TpuMem::pulsecan_cfg_11_can_trig_dat_mask_bits63_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(can_cfg_11,can_trig_dat_mask_bits63_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(can_cfg_11,can_trig_dat_mask_bits63_32,idleV);
}

void TpuMem::outcan_cfg_11( Tpu_reg value )
{
	out_assign_field(can_cfg_11,payload,value);
}

void TpuMem::outcan_cfg_11()
{
	out_member(can_cfg_11);
}


void TpuMem::setflexray_cfg_1_flexray_trig_type( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_location( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_location,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_frame( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_frame,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_symbol,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_err( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_err,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_id( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_id,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_id_mask,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_cyccount,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_type_cyc_mask,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_rate_sel,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_channel_sel,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_gdTSSTransmitter,value);
}
void TpuMem::setflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,flexray_trig_gdCASxLowMax,value);
}

void TpuMem::setflexray_cfg_1( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_1,payload,value);
}

void TpuMem::outflexray_cfg_1_flexray_trig_type( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_location( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_location,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_location( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_location,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_location,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_frame( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_frame,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_frame( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_frame,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_frame,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_symbol,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_symbol( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_symbol,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_symbol,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_err( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_err,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_err( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_err,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_err,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_id( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_id,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_id( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_id,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_id,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_id_mask,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_id_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_id_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_id_mask,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_cyccount,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_cyccount( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_cyccount,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_cyccount,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_cyc_mask,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_type_cyc_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_type_cyc_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_type_cyc_mask,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_rate_sel,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_rate_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_rate_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_rate_sel,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_channel_sel,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_channel_sel( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_channel_sel,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_gdTSSTransmitter,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_gdTSSTransmitter( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_gdTSSTransmitter,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_gdTSSTransmitter,idleV);
}
void TpuMem::outflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,flexray_trig_gdCASxLowMax,value);
}
void TpuMem::pulseflexray_cfg_1_flexray_trig_gdCASxLowMax( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_1,flexray_trig_gdCASxLowMax,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_1,flexray_trig_gdCASxLowMax,idleV);
}

void TpuMem::outflexray_cfg_1( Tpu_reg value )
{
	out_assign_field(flexray_cfg_1,payload,value);
}

void TpuMem::outflexray_cfg_1()
{
	out_member(flexray_cfg_1);
}


void TpuMem::setflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_2,flexray_trig_frame_id_min,value);
}
void TpuMem::setflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_2,flexray_trig_frame_id_max,value);
}

void TpuMem::setflexray_cfg_2( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_2,payload,value);
}

void TpuMem::outflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg value )
{
	out_assign_field(flexray_cfg_2,flexray_trig_frame_id_min,value);
}
void TpuMem::pulseflexray_cfg_2_flexray_trig_frame_id_min( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_2,flexray_trig_frame_id_min,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_2,flexray_trig_frame_id_min,idleV);
}
void TpuMem::outflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg value )
{
	out_assign_field(flexray_cfg_2,flexray_trig_frame_id_max,value);
}
void TpuMem::pulseflexray_cfg_2_flexray_trig_frame_id_max( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_2,flexray_trig_frame_id_max,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_2,flexray_trig_frame_id_max,idleV);
}

void TpuMem::outflexray_cfg_2( Tpu_reg value )
{
	out_assign_field(flexray_cfg_2,payload,value);
}

void TpuMem::outflexray_cfg_2()
{
	out_member(flexray_cfg_2);
}


void TpuMem::setflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_3,flexray_trig_frame_id_mask,value);
}

void TpuMem::setflexray_cfg_3( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_3,payload,value);
}

void TpuMem::outflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg value )
{
	out_assign_field(flexray_cfg_3,flexray_trig_frame_id_mask,value);
}
void TpuMem::pulseflexray_cfg_3_flexray_trig_frame_id_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_3,flexray_trig_frame_id_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_3,flexray_trig_frame_id_mask,idleV);
}

void TpuMem::outflexray_cfg_3( Tpu_reg value )
{
	out_assign_field(flexray_cfg_3,payload,value);
}

void TpuMem::outflexray_cfg_3()
{
	out_member(flexray_cfg_3);
}


void TpuMem::setflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_min,value);
}
void TpuMem::setflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_max,value);
}
void TpuMem::setflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_mask,value);
}

void TpuMem::setflexray_cfg_4( Tpu_reg value )
{
	reg_assign_field(flexray_cfg_4,payload,value);
}

void TpuMem::outflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg value )
{
	out_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_min,value);
}
void TpuMem::pulseflexray_cfg_4_flexray_trig_frame_cycount_min( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_min,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_min,idleV);
}
void TpuMem::outflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg value )
{
	out_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_max,value);
}
void TpuMem::pulseflexray_cfg_4_flexray_trig_frame_cycount_max( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_max,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_max,idleV);
}
void TpuMem::outflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg value )
{
	out_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_mask,value);
}
void TpuMem::pulseflexray_cfg_4_flexray_trig_frame_cycount_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(flexray_cfg_4,flexray_trig_frame_cycount_mask,idleV);
}

void TpuMem::outflexray_cfg_4( Tpu_reg value )
{
	out_assign_field(flexray_cfg_4,payload,value);
}

void TpuMem::outflexray_cfg_4()
{
	out_member(flexray_cfg_4);
}


void TpuMem::setspi_cfg_1_spi_trig_type( Tpu_reg value )
{
	reg_assign_field(spi_cfg_1,spi_trig_type,value);
}
void TpuMem::setspi_cfg_1_spi_trig_dat_type( Tpu_reg value )
{
	reg_assign_field(spi_cfg_1,spi_trig_dat_type,value);
}
void TpuMem::setspi_cfg_1_spi_trig_dat_bit_num( Tpu_reg value )
{
	reg_assign_field(spi_cfg_1,spi_trig_dat_bit_num,value);
}

void TpuMem::setspi_cfg_1( Tpu_reg value )
{
	reg_assign_field(spi_cfg_1,payload,value);
}

void TpuMem::outspi_cfg_1_spi_trig_type( Tpu_reg value )
{
	out_assign_field(spi_cfg_1,spi_trig_type,value);
}
void TpuMem::pulsespi_cfg_1_spi_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(spi_cfg_1,spi_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(spi_cfg_1,spi_trig_type,idleV);
}
void TpuMem::outspi_cfg_1_spi_trig_dat_type( Tpu_reg value )
{
	out_assign_field(spi_cfg_1,spi_trig_dat_type,value);
}
void TpuMem::pulsespi_cfg_1_spi_trig_dat_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(spi_cfg_1,spi_trig_dat_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(spi_cfg_1,spi_trig_dat_type,idleV);
}
void TpuMem::outspi_cfg_1_spi_trig_dat_bit_num( Tpu_reg value )
{
	out_assign_field(spi_cfg_1,spi_trig_dat_bit_num,value);
}
void TpuMem::pulsespi_cfg_1_spi_trig_dat_bit_num( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(spi_cfg_1,spi_trig_dat_bit_num,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(spi_cfg_1,spi_trig_dat_bit_num,idleV);
}

void TpuMem::outspi_cfg_1( Tpu_reg value )
{
	out_assign_field(spi_cfg_1,payload,value);
}

void TpuMem::outspi_cfg_1()
{
	out_member(spi_cfg_1);
}


void TpuMem::setspi_cfg_2_spi_trig_timeout( Tpu_reg value )
{
	reg_assign_field(spi_cfg_2,spi_trig_timeout,value);
}

void TpuMem::setspi_cfg_2( Tpu_reg value )
{
	reg_assign_field(spi_cfg_2,payload,value);
}

void TpuMem::outspi_cfg_2_spi_trig_timeout( Tpu_reg value )
{
	out_assign_field(spi_cfg_2,spi_trig_timeout,value);
}
void TpuMem::pulsespi_cfg_2_spi_trig_timeout( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(spi_cfg_2,spi_trig_timeout,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(spi_cfg_2,spi_trig_timeout,idleV);
}

void TpuMem::outspi_cfg_2( Tpu_reg value )
{
	out_assign_field(spi_cfg_2,payload,value);
}

void TpuMem::outspi_cfg_2()
{
	out_member(spi_cfg_2);
}


void TpuMem::setspi_cfg_3_spi_trig_dat_min( Tpu_reg value )
{
	reg_assign_field(spi_cfg_3,spi_trig_dat_min,value);
}

void TpuMem::setspi_cfg_3( Tpu_reg value )
{
	reg_assign_field(spi_cfg_3,payload,value);
}

void TpuMem::outspi_cfg_3_spi_trig_dat_min( Tpu_reg value )
{
	out_assign_field(spi_cfg_3,spi_trig_dat_min,value);
}
void TpuMem::pulsespi_cfg_3_spi_trig_dat_min( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(spi_cfg_3,spi_trig_dat_min,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(spi_cfg_3,spi_trig_dat_min,idleV);
}

void TpuMem::outspi_cfg_3( Tpu_reg value )
{
	out_assign_field(spi_cfg_3,payload,value);
}

void TpuMem::outspi_cfg_3()
{
	out_member(spi_cfg_3);
}


void TpuMem::setspi_cfg_4_spi_trig_dat_max( Tpu_reg value )
{
	reg_assign_field(spi_cfg_4,spi_trig_dat_max,value);
}

void TpuMem::setspi_cfg_4( Tpu_reg value )
{
	reg_assign_field(spi_cfg_4,payload,value);
}

void TpuMem::outspi_cfg_4_spi_trig_dat_max( Tpu_reg value )
{
	out_assign_field(spi_cfg_4,spi_trig_dat_max,value);
}
void TpuMem::pulsespi_cfg_4_spi_trig_dat_max( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(spi_cfg_4,spi_trig_dat_max,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(spi_cfg_4,spi_trig_dat_max,idleV);
}

void TpuMem::outspi_cfg_4( Tpu_reg value )
{
	out_assign_field(spi_cfg_4,payload,value);
}

void TpuMem::outspi_cfg_4()
{
	out_member(spi_cfg_4);
}


void TpuMem::setspi_cfg_5_spi_trig_dat_mask( Tpu_reg value )
{
	reg_assign_field(spi_cfg_5,spi_trig_dat_mask,value);
}

void TpuMem::setspi_cfg_5( Tpu_reg value )
{
	reg_assign_field(spi_cfg_5,payload,value);
}

void TpuMem::outspi_cfg_5_spi_trig_dat_mask( Tpu_reg value )
{
	out_assign_field(spi_cfg_5,spi_trig_dat_mask,value);
}
void TpuMem::pulsespi_cfg_5_spi_trig_dat_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(spi_cfg_5,spi_trig_dat_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(spi_cfg_5,spi_trig_dat_mask,idleV);
}

void TpuMem::outspi_cfg_5( Tpu_reg value )
{
	out_assign_field(spi_cfg_5,payload,value);
}

void TpuMem::outspi_cfg_5()
{
	out_member(spi_cfg_5);
}


void TpuMem::setiic_cfg_1_iic_trig_type( Tpu_reg value )
{
	reg_assign_field(iic_cfg_1,iic_trig_type,value);
}
void TpuMem::setiic_cfg_1_iic_trig_addr_type( Tpu_reg value )
{
	reg_assign_field(iic_cfg_1,iic_trig_addr_type,value);
}
void TpuMem::setiic_cfg_1_iic_trig_dat_type( Tpu_reg value )
{
	reg_assign_field(iic_cfg_1,iic_trig_dat_type,value);
}
void TpuMem::setiic_cfg_1_iic_trig_wr_type( Tpu_reg value )
{
	reg_assign_field(iic_cfg_1,iic_trig_wr_type,value);
}
void TpuMem::setiic_cfg_1_iic_addr_length( Tpu_reg value )
{
	reg_assign_field(iic_cfg_1,iic_addr_length,value);
}
void TpuMem::setiic_cfg_1_iic_dat_length( Tpu_reg value )
{
	reg_assign_field(iic_cfg_1,iic_dat_length,value);
}

void TpuMem::setiic_cfg_1( Tpu_reg value )
{
	reg_assign_field(iic_cfg_1,payload,value);
}

void TpuMem::outiic_cfg_1_iic_trig_type( Tpu_reg value )
{
	out_assign_field(iic_cfg_1,iic_trig_type,value);
}
void TpuMem::pulseiic_cfg_1_iic_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_1,iic_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_1,iic_trig_type,idleV);
}
void TpuMem::outiic_cfg_1_iic_trig_addr_type( Tpu_reg value )
{
	out_assign_field(iic_cfg_1,iic_trig_addr_type,value);
}
void TpuMem::pulseiic_cfg_1_iic_trig_addr_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_1,iic_trig_addr_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_1,iic_trig_addr_type,idleV);
}
void TpuMem::outiic_cfg_1_iic_trig_dat_type( Tpu_reg value )
{
	out_assign_field(iic_cfg_1,iic_trig_dat_type,value);
}
void TpuMem::pulseiic_cfg_1_iic_trig_dat_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_1,iic_trig_dat_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_1,iic_trig_dat_type,idleV);
}
void TpuMem::outiic_cfg_1_iic_trig_wr_type( Tpu_reg value )
{
	out_assign_field(iic_cfg_1,iic_trig_wr_type,value);
}
void TpuMem::pulseiic_cfg_1_iic_trig_wr_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_1,iic_trig_wr_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_1,iic_trig_wr_type,idleV);
}
void TpuMem::outiic_cfg_1_iic_addr_length( Tpu_reg value )
{
	out_assign_field(iic_cfg_1,iic_addr_length,value);
}
void TpuMem::pulseiic_cfg_1_iic_addr_length( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_1,iic_addr_length,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_1,iic_addr_length,idleV);
}
void TpuMem::outiic_cfg_1_iic_dat_length( Tpu_reg value )
{
	out_assign_field(iic_cfg_1,iic_dat_length,value);
}
void TpuMem::pulseiic_cfg_1_iic_dat_length( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_1,iic_dat_length,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_1,iic_dat_length,idleV);
}

void TpuMem::outiic_cfg_1( Tpu_reg value )
{
	out_assign_field(iic_cfg_1,payload,value);
}

void TpuMem::outiic_cfg_1()
{
	out_member(iic_cfg_1);
}


void TpuMem::setiic_cfg_2_iic_addr_setmin( Tpu_reg value )
{
	reg_assign_field(iic_cfg_2,iic_addr_setmin,value);
}
void TpuMem::setiic_cfg_2_iic_addr_setmax( Tpu_reg value )
{
	reg_assign_field(iic_cfg_2,iic_addr_setmax,value);
}
void TpuMem::setiic_cfg_2_iic_addr_setmask( Tpu_reg value )
{
	reg_assign_field(iic_cfg_2,iic_addr_setmask,value);
}

void TpuMem::setiic_cfg_2( Tpu_reg value )
{
	reg_assign_field(iic_cfg_2,payload,value);
}

void TpuMem::outiic_cfg_2_iic_addr_setmin( Tpu_reg value )
{
	out_assign_field(iic_cfg_2,iic_addr_setmin,value);
}
void TpuMem::pulseiic_cfg_2_iic_addr_setmin( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_2,iic_addr_setmin,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_2,iic_addr_setmin,idleV);
}
void TpuMem::outiic_cfg_2_iic_addr_setmax( Tpu_reg value )
{
	out_assign_field(iic_cfg_2,iic_addr_setmax,value);
}
void TpuMem::pulseiic_cfg_2_iic_addr_setmax( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_2,iic_addr_setmax,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_2,iic_addr_setmax,idleV);
}
void TpuMem::outiic_cfg_2_iic_addr_setmask( Tpu_reg value )
{
	out_assign_field(iic_cfg_2,iic_addr_setmask,value);
}
void TpuMem::pulseiic_cfg_2_iic_addr_setmask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_2,iic_addr_setmask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_2,iic_addr_setmask,idleV);
}

void TpuMem::outiic_cfg_2( Tpu_reg value )
{
	out_assign_field(iic_cfg_2,payload,value);
}

void TpuMem::outiic_cfg_2()
{
	out_member(iic_cfg_2);
}


void TpuMem::setiic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg value )
{
	reg_assign_field(iic_cfg_3,iic_dat_setmin_bits31_0,value);
}

void TpuMem::setiic_cfg_3( Tpu_reg value )
{
	reg_assign_field(iic_cfg_3,payload,value);
}

void TpuMem::outiic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg value )
{
	out_assign_field(iic_cfg_3,iic_dat_setmin_bits31_0,value);
}
void TpuMem::pulseiic_cfg_3_iic_dat_setmin_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_3,iic_dat_setmin_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_3,iic_dat_setmin_bits31_0,idleV);
}

void TpuMem::outiic_cfg_3( Tpu_reg value )
{
	out_assign_field(iic_cfg_3,payload,value);
}

void TpuMem::outiic_cfg_3()
{
	out_member(iic_cfg_3);
}


void TpuMem::setiic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg value )
{
	reg_assign_field(iic_cfg_4,iic_dat_setmax_bits31_0,value);
}

void TpuMem::setiic_cfg_4( Tpu_reg value )
{
	reg_assign_field(iic_cfg_4,payload,value);
}

void TpuMem::outiic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg value )
{
	out_assign_field(iic_cfg_4,iic_dat_setmax_bits31_0,value);
}
void TpuMem::pulseiic_cfg_4_iic_dat_setmax_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_4,iic_dat_setmax_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_4,iic_dat_setmax_bits31_0,idleV);
}

void TpuMem::outiic_cfg_4( Tpu_reg value )
{
	out_assign_field(iic_cfg_4,payload,value);
}

void TpuMem::outiic_cfg_4()
{
	out_member(iic_cfg_4);
}


void TpuMem::setiic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg value )
{
	reg_assign_field(iic_cfg_5,iic_dat_setmask_bits31_0,value);
}

void TpuMem::setiic_cfg_5( Tpu_reg value )
{
	reg_assign_field(iic_cfg_5,payload,value);
}

void TpuMem::outiic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg value )
{
	out_assign_field(iic_cfg_5,iic_dat_setmask_bits31_0,value);
}
void TpuMem::pulseiic_cfg_5_iic_dat_setmask_bits31_0( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_5,iic_dat_setmask_bits31_0,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_5,iic_dat_setmask_bits31_0,idleV);
}

void TpuMem::outiic_cfg_5( Tpu_reg value )
{
	out_assign_field(iic_cfg_5,payload,value);
}

void TpuMem::outiic_cfg_5()
{
	out_member(iic_cfg_5);
}


void TpuMem::setiic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg value )
{
	reg_assign_field(iic_cfg_6,iic_dat_setmin_bits39_32,value);
}
void TpuMem::setiic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg value )
{
	reg_assign_field(iic_cfg_6,iic_dat_setmax_bits39_32,value);
}
void TpuMem::setiic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg value )
{
	reg_assign_field(iic_cfg_6,iic_dat_setmask_bits39_32,value);
}

void TpuMem::setiic_cfg_6( Tpu_reg value )
{
	reg_assign_field(iic_cfg_6,payload,value);
}

void TpuMem::outiic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg value )
{
	out_assign_field(iic_cfg_6,iic_dat_setmin_bits39_32,value);
}
void TpuMem::pulseiic_cfg_6_iic_dat_setmin_bits39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_6,iic_dat_setmin_bits39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_6,iic_dat_setmin_bits39_32,idleV);
}
void TpuMem::outiic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg value )
{
	out_assign_field(iic_cfg_6,iic_dat_setmax_bits39_32,value);
}
void TpuMem::pulseiic_cfg_6_iic_dat_setmax_bits39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_6,iic_dat_setmax_bits39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_6,iic_dat_setmax_bits39_32,idleV);
}
void TpuMem::outiic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg value )
{
	out_assign_field(iic_cfg_6,iic_dat_setmask_bits39_32,value);
}
void TpuMem::pulseiic_cfg_6_iic_dat_setmask_bits39_32( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(iic_cfg_6,iic_dat_setmask_bits39_32,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(iic_cfg_6,iic_dat_setmask_bits39_32,idleV);
}

void TpuMem::outiic_cfg_6( Tpu_reg value )
{
	out_assign_field(iic_cfg_6,payload,value);
}

void TpuMem::outiic_cfg_6()
{
	out_member(iic_cfg_6);
}


void TpuMem::setvideo_cfg_1_video_trig_type( Tpu_reg value )
{
	reg_assign_field(video_cfg_1,video_trig_type,value);
}
void TpuMem::setvideo_cfg_1_video_trig_line( Tpu_reg value )
{
	reg_assign_field(video_cfg_1,video_trig_line,value);
}

void TpuMem::setvideo_cfg_1( Tpu_reg value )
{
	reg_assign_field(video_cfg_1,payload,value);
}

void TpuMem::outvideo_cfg_1_video_trig_type( Tpu_reg value )
{
	out_assign_field(video_cfg_1,video_trig_type,value);
}
void TpuMem::pulsevideo_cfg_1_video_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(video_cfg_1,video_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(video_cfg_1,video_trig_type,idleV);
}
void TpuMem::outvideo_cfg_1_video_trig_line( Tpu_reg value )
{
	out_assign_field(video_cfg_1,video_trig_line,value);
}
void TpuMem::pulsevideo_cfg_1_video_trig_line( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(video_cfg_1,video_trig_line,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(video_cfg_1,video_trig_line,idleV);
}

void TpuMem::outvideo_cfg_1( Tpu_reg value )
{
	out_assign_field(video_cfg_1,payload,value);
}

void TpuMem::outvideo_cfg_1()
{
	out_member(video_cfg_1);
}


void TpuMem::setvideo_cfg_2_fpulse_width( Tpu_reg value )
{
	reg_assign_field(video_cfg_2,fpulse_width,value);
}
void TpuMem::setvideo_cfg_2_hsync_width( Tpu_reg value )
{
	reg_assign_field(video_cfg_2,hsync_width,value);
}

void TpuMem::setvideo_cfg_2( Tpu_reg value )
{
	reg_assign_field(video_cfg_2,payload,value);
}

void TpuMem::outvideo_cfg_2_fpulse_width( Tpu_reg value )
{
	out_assign_field(video_cfg_2,fpulse_width,value);
}
void TpuMem::pulsevideo_cfg_2_fpulse_width( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(video_cfg_2,fpulse_width,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(video_cfg_2,fpulse_width,idleV);
}
void TpuMem::outvideo_cfg_2_hsync_width( Tpu_reg value )
{
	out_assign_field(video_cfg_2,hsync_width,value);
}
void TpuMem::pulsevideo_cfg_2_hsync_width( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(video_cfg_2,hsync_width,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(video_cfg_2,hsync_width,idleV);
}

void TpuMem::outvideo_cfg_2( Tpu_reg value )
{
	out_assign_field(video_cfg_2,payload,value);
}

void TpuMem::outvideo_cfg_2()
{
	out_member(video_cfg_2);
}


void TpuMem::setvideo_cfg_3_start_line( Tpu_reg value )
{
	reg_assign_field(video_cfg_3,start_line,value);
}
void TpuMem::setvideo_cfg_3_total_line( Tpu_reg value )
{
	reg_assign_field(video_cfg_3,total_line,value);
}
void TpuMem::setvideo_cfg_3_level_type( Tpu_reg value )
{
	reg_assign_field(video_cfg_3,level_type,value);
}

void TpuMem::setvideo_cfg_3( Tpu_reg value )
{
	reg_assign_field(video_cfg_3,payload,value);
}

void TpuMem::outvideo_cfg_3_start_line( Tpu_reg value )
{
	out_assign_field(video_cfg_3,start_line,value);
}
void TpuMem::pulsevideo_cfg_3_start_line( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(video_cfg_3,start_line,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(video_cfg_3,start_line,idleV);
}
void TpuMem::outvideo_cfg_3_total_line( Tpu_reg value )
{
	out_assign_field(video_cfg_3,total_line,value);
}
void TpuMem::pulsevideo_cfg_3_total_line( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(video_cfg_3,total_line,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(video_cfg_3,total_line,idleV);
}
void TpuMem::outvideo_cfg_3_level_type( Tpu_reg value )
{
	out_assign_field(video_cfg_3,level_type,value);
}
void TpuMem::pulsevideo_cfg_3_level_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(video_cfg_3,level_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(video_cfg_3,level_type,idleV);
}

void TpuMem::outvideo_cfg_3( Tpu_reg value )
{
	out_assign_field(video_cfg_3,payload,value);
}

void TpuMem::outvideo_cfg_3()
{
	out_member(video_cfg_3);
}


void TpuMem::setstd1553b_cfg_1_std1553b_trig_type( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,std1553b_trig_type,value);
}
void TpuMem::setstd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,std1553b_trig_sync_type,value);
}
void TpuMem::setstd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,std1553b_trig_dat_type,value);
}
void TpuMem::setstd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,std1553b_trig_remote_addr_type,value);
}
void TpuMem::setstd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,std1553b_trig_subaddr_mode_type,value);
}
void TpuMem::setstd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,std1553b_trig_count_code_type,value);
}
void TpuMem::setstd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,std1553b_trig_err_type,value);
}

void TpuMem::setstd1553b_cfg_1( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_1,payload,value);
}

void TpuMem::outstd1553b_cfg_1_std1553b_trig_type( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_type,value);
}
void TpuMem::pulsestd1553b_cfg_1_std1553b_trig_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_1,std1553b_trig_type,idleV);
}
void TpuMem::outstd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_sync_type,value);
}
void TpuMem::pulsestd1553b_cfg_1_std1553b_trig_sync_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_sync_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_1,std1553b_trig_sync_type,idleV);
}
void TpuMem::outstd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_dat_type,value);
}
void TpuMem::pulsestd1553b_cfg_1_std1553b_trig_dat_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_dat_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_1,std1553b_trig_dat_type,idleV);
}
void TpuMem::outstd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_remote_addr_type,value);
}
void TpuMem::pulsestd1553b_cfg_1_std1553b_trig_remote_addr_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_remote_addr_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_1,std1553b_trig_remote_addr_type,idleV);
}
void TpuMem::outstd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_subaddr_mode_type,value);
}
void TpuMem::pulsestd1553b_cfg_1_std1553b_trig_subaddr_mode_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_subaddr_mode_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_1,std1553b_trig_subaddr_mode_type,idleV);
}
void TpuMem::outstd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_count_code_type,value);
}
void TpuMem::pulsestd1553b_cfg_1_std1553b_trig_count_code_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_count_code_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_1,std1553b_trig_count_code_type,idleV);
}
void TpuMem::outstd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_err_type,value);
}
void TpuMem::pulsestd1553b_cfg_1_std1553b_trig_err_type( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_1,std1553b_trig_err_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_1,std1553b_trig_err_type,idleV);
}

void TpuMem::outstd1553b_cfg_1( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_1,payload,value);
}

void TpuMem::outstd1553b_cfg_1()
{
	out_member(std1553b_cfg_1);
}


void TpuMem::setstd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_2,std1553b_trig_word_mask,value);
}

void TpuMem::setstd1553b_cfg_2( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_2,payload,value);
}

void TpuMem::outstd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_2,std1553b_trig_word_mask,value);
}
void TpuMem::pulsestd1553b_cfg_2_std1553b_trig_word_mask( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_2,std1553b_trig_word_mask,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_2,std1553b_trig_word_mask,idleV);
}

void TpuMem::outstd1553b_cfg_2( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_2,payload,value);
}

void TpuMem::outstd1553b_cfg_2()
{
	out_member(std1553b_cfg_2);
}


void TpuMem::setstd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_3,std1553b_trig_dat_cmp_l,value);
}

void TpuMem::setstd1553b_cfg_3( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_3,payload,value);
}

void TpuMem::outstd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_3,std1553b_trig_dat_cmp_l,value);
}
void TpuMem::pulsestd1553b_cfg_3_std1553b_trig_dat_cmp_l( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_3,std1553b_trig_dat_cmp_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_3,std1553b_trig_dat_cmp_l,idleV);
}

void TpuMem::outstd1553b_cfg_3( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_3,payload,value);
}

void TpuMem::outstd1553b_cfg_3()
{
	out_member(std1553b_cfg_3);
}


void TpuMem::setstd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_4,std1553b_trig_dat_cmp_g,value);
}

void TpuMem::setstd1553b_cfg_4( Tpu_reg value )
{
	reg_assign_field(std1553b_cfg_4,payload,value);
}

void TpuMem::outstd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_4,std1553b_trig_dat_cmp_g,value);
}
void TpuMem::pulsestd1553b_cfg_4_std1553b_trig_dat_cmp_g( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(std1553b_cfg_4,std1553b_trig_dat_cmp_g,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(std1553b_cfg_4,std1553b_trig_dat_cmp_g,idleV);
}

void TpuMem::outstd1553b_cfg_4( Tpu_reg value )
{
	out_assign_field(std1553b_cfg_4,payload,value);
}

void TpuMem::outstd1553b_cfg_4()
{
	out_member(std1553b_cfg_4);
}


Tpu_reg TpuMem::insearch_version()
{
	reg_in(search_version);
	return search_version;
}


void TpuMem::setsearch_dat_compress_num_search_dat_compress_num( Tpu_reg value )
{
	reg_assign_field(search_dat_compress_num,search_dat_compress_num,value);
}

void TpuMem::setsearch_dat_compress_num( Tpu_reg value )
{
	reg_assign_field(search_dat_compress_num,payload,value);
}

void TpuMem::outsearch_dat_compress_num_search_dat_compress_num( Tpu_reg value )
{
	out_assign_field(search_dat_compress_num,search_dat_compress_num,value);
}
void TpuMem::pulsesearch_dat_compress_num_search_dat_compress_num( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(search_dat_compress_num,search_dat_compress_num,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(search_dat_compress_num,search_dat_compress_num,idleV);
}

void TpuMem::outsearch_dat_compress_num( Tpu_reg value )
{
	out_assign_field(search_dat_compress_num,payload,value);
}

void TpuMem::outsearch_dat_compress_num()
{
	out_member(search_dat_compress_num);
}


void TpuMem::setsearch_result_send_en_search_result_send_en( Tpu_reg value )
{
	reg_assign_field(search_result_send_en,search_result_send_en,value);
}

void TpuMem::setsearch_result_send_en( Tpu_reg value )
{
	reg_assign_field(search_result_send_en,payload,value);
}

void TpuMem::outsearch_result_send_en_search_result_send_en( Tpu_reg value )
{
	out_assign_field(search_result_send_en,search_result_send_en,value);
}
void TpuMem::pulsesearch_result_send_en_search_result_send_en( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(search_result_send_en,search_result_send_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(search_result_send_en,search_result_send_en,idleV);
}

void TpuMem::outsearch_result_send_en( Tpu_reg value )
{
	out_assign_field(search_result_send_en,payload,value);
}

void TpuMem::outsearch_result_send_en()
{
	out_member(search_result_send_en);
}


void TpuMem::setsearch_result_send_rst_search_result_send_rst( Tpu_reg value )
{
	reg_assign_field(search_result_send_rst,search_result_send_rst,value);
}

void TpuMem::setsearch_result_send_rst( Tpu_reg value )
{
	reg_assign_field(search_result_send_rst,payload,value);
}

void TpuMem::outsearch_result_send_rst_search_result_send_rst( Tpu_reg value )
{
	out_assign_field(search_result_send_rst,search_result_send_rst,value);
}
void TpuMem::pulsesearch_result_send_rst_search_result_send_rst( Tpu_reg activeV, Tpu_reg idleV, int timeus )
{
	out_assign_field(search_result_send_rst,search_result_send_rst,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(search_result_send_rst,search_result_send_rst,idleV);
}

void TpuMem::outsearch_result_send_rst( Tpu_reg value )
{
	out_assign_field(search_result_send_rst,payload,value);
}

void TpuMem::outsearch_result_send_rst()
{
	out_member(search_result_send_rst);
}


Tpu_reg TpuMem::insearch_event_total_num()
{
	reg_in(search_event_total_num);
	return search_event_total_num.payload;
}


cache_addr TpuMem::addr_version(){ return 0x1800+mAddrBase; }
cache_addr TpuMem::addr_ch1_cmp_level(){ return 0x1804+mAddrBase; }
cache_addr TpuMem::addr_ch2_cmp_level(){ return 0x1808+mAddrBase; }
cache_addr TpuMem::addr_ch3_cmp_level(){ return 0x180c+mAddrBase; }

cache_addr TpuMem::addr_ch4_cmp_level(){ return 0x1810+mAddrBase; }
cache_addr TpuMem::addr_A_event_reg1(){ return 0x1814+mAddrBase; }
cache_addr TpuMem::addr_A_event_reg2(){ return 0x1818+mAddrBase; }
cache_addr TpuMem::addr_A_event_reg3(){ return 0x181c+mAddrBase; }

cache_addr TpuMem::addr_A_event_reg4(){ return 0x1820+mAddrBase; }
cache_addr TpuMem::addr_A_event_reg5(){ return 0x1824+mAddrBase; }
cache_addr TpuMem::addr_A_event_reg6(){ return 0x1828+mAddrBase; }
cache_addr TpuMem::addr_B_event_reg1(){ return 0x182c+mAddrBase; }

cache_addr TpuMem::addr_B_event_reg2(){ return 0x1830+mAddrBase; }
cache_addr TpuMem::addr_B_event_reg3(){ return 0x1834+mAddrBase; }
cache_addr TpuMem::addr_B_event_reg4(){ return 0x1838+mAddrBase; }
cache_addr TpuMem::addr_B_event_reg5(){ return 0x183c+mAddrBase; }

cache_addr TpuMem::addr_B_event_reg6(){ return 0x1840+mAddrBase; }
cache_addr TpuMem::addr_R_event_reg1(){ return 0x1844+mAddrBase; }
cache_addr TpuMem::addr_R_event_reg2(){ return 0x1848+mAddrBase; }
cache_addr TpuMem::addr_R_event_reg3(){ return 0x184c+mAddrBase; }

cache_addr TpuMem::addr_R_event_reg4(){ return 0x1850+mAddrBase; }
cache_addr TpuMem::addr_R_event_reg5(){ return 0x1854+mAddrBase; }
cache_addr TpuMem::addr_R_event_reg6(){ return 0x1858+mAddrBase; }
cache_addr TpuMem::addr_tpu_trig_mux(){ return 0x185c+mAddrBase; }

cache_addr TpuMem::addr_abr_delay_time_l(){ return 0x1860+mAddrBase; }
cache_addr TpuMem::addr_abr_delay_time_h(){ return 0x1864+mAddrBase; }
cache_addr TpuMem::addr_abr_timeout_l(){ return 0x1868+mAddrBase; }
cache_addr TpuMem::addr_abr_timeout_h(){ return 0x186c+mAddrBase; }

cache_addr TpuMem::addr_abr_b_event_num(){ return 0x1870+mAddrBase; }
cache_addr TpuMem::addr_preprocess_type(){ return 0x1874+mAddrBase; }
cache_addr TpuMem::addr_abr_a_event_num(){ return 0x1878+mAddrBase; }
cache_addr TpuMem::addr_holdoff_event_num(){ return 0x1880+mAddrBase; }

cache_addr TpuMem::addr_holdoff_time_l(){ return 0x1884+mAddrBase; }
cache_addr TpuMem::addr_holdoff_time_h(){ return 0x1888+mAddrBase; }
cache_addr TpuMem::addr_iir_set(){ return 0x188c+mAddrBase; }
cache_addr TpuMem::addr_uart_cfg_1(){ return 0x1900+mAddrBase; }

cache_addr TpuMem::addr_uart_cfg_2(){ return 0x1904+mAddrBase; }
cache_addr TpuMem::addr_iis_cfg_1(){ return 0x1908+mAddrBase; }
cache_addr TpuMem::addr_iis_cfg_2(){ return 0x190c+mAddrBase; }
cache_addr TpuMem::addr_iis_cfg_3(){ return 0x1910+mAddrBase; }

cache_addr TpuMem::addr_iis_cfg_4(){ return 0x1914+mAddrBase; }
cache_addr TpuMem::addr_lin_cfg_1(){ return 0x1918+mAddrBase; }
cache_addr TpuMem::addr_lin_cfg_2(){ return 0x191c+mAddrBase; }
cache_addr TpuMem::addr_lin_cfg_3(){ return 0x1920+mAddrBase; }

cache_addr TpuMem::addr_lin_cfg_4(){ return 0x1924+mAddrBase; }
cache_addr TpuMem::addr_lin_cfg_5(){ return 0x1928+mAddrBase; }
cache_addr TpuMem::addr_lin_cfg_6(){ return 0x192c+mAddrBase; }
cache_addr TpuMem::addr_lin_cfg_7(){ return 0x1930+mAddrBase; }

cache_addr TpuMem::addr_lin_cfg_8(){ return 0x1934+mAddrBase; }
cache_addr TpuMem::addr_lin_cfg_9(){ return 0x1938+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_1(){ return 0x193c+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_2(){ return 0x1940+mAddrBase; }

cache_addr TpuMem::addr_can_cfg_3(){ return 0x1944+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_4(){ return 0x1948+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_5(){ return 0x194c+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_6(){ return 0x1950+mAddrBase; }

cache_addr TpuMem::addr_can_cfg_7(){ return 0x1954+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_8(){ return 0x1958+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_9(){ return 0x195c+mAddrBase; }
cache_addr TpuMem::addr_can_cfg_10(){ return 0x1960+mAddrBase; }

cache_addr TpuMem::addr_can_cfg_11(){ return 0x1964+mAddrBase; }
cache_addr TpuMem::addr_flexray_cfg_1(){ return 0x1968+mAddrBase; }
cache_addr TpuMem::addr_flexray_cfg_2(){ return 0x196c+mAddrBase; }
cache_addr TpuMem::addr_flexray_cfg_3(){ return 0x1970+mAddrBase; }

cache_addr TpuMem::addr_flexray_cfg_4(){ return 0x1974+mAddrBase; }
cache_addr TpuMem::addr_spi_cfg_1(){ return 0x1978+mAddrBase; }
cache_addr TpuMem::addr_spi_cfg_2(){ return 0x197c+mAddrBase; }
cache_addr TpuMem::addr_spi_cfg_3(){ return 0x1980+mAddrBase; }

cache_addr TpuMem::addr_spi_cfg_4(){ return 0x1984+mAddrBase; }
cache_addr TpuMem::addr_spi_cfg_5(){ return 0x1988+mAddrBase; }
cache_addr TpuMem::addr_iic_cfg_1(){ return 0x198c+mAddrBase; }
cache_addr TpuMem::addr_iic_cfg_2(){ return 0x1990+mAddrBase; }

cache_addr TpuMem::addr_iic_cfg_3(){ return 0x1994+mAddrBase; }
cache_addr TpuMem::addr_iic_cfg_4(){ return 0x1998+mAddrBase; }
cache_addr TpuMem::addr_iic_cfg_5(){ return 0x199c+mAddrBase; }
cache_addr TpuMem::addr_iic_cfg_6(){ return 0x19a0+mAddrBase; }

cache_addr TpuMem::addr_video_cfg_1(){ return 0x19a4+mAddrBase; }
cache_addr TpuMem::addr_video_cfg_2(){ return 0x19a8+mAddrBase; }
cache_addr TpuMem::addr_video_cfg_3(){ return 0x19ac+mAddrBase; }
cache_addr TpuMem::addr_std1553b_cfg_1(){ return 0x19b0+mAddrBase; }

cache_addr TpuMem::addr_std1553b_cfg_2(){ return 0x19b4+mAddrBase; }
cache_addr TpuMem::addr_std1553b_cfg_3(){ return 0x19b8+mAddrBase; }
cache_addr TpuMem::addr_std1553b_cfg_4(){ return 0x19bc+mAddrBase; }
cache_addr TpuMem::addr_search_version(){ return 0x1a80+mAddrBase; }

cache_addr TpuMem::addr_search_dat_compress_num(){ return 0x1a84+mAddrBase; }
cache_addr TpuMem::addr_search_result_send_en(){ return 0x1a88+mAddrBase; }
cache_addr TpuMem::addr_search_result_send_rst(){ return 0x1a8c+mAddrBase; }
cache_addr TpuMem::addr_search_event_total_num(){ return 0x1a88+mAddrBase; }



