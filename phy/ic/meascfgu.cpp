#include "meascfgu.h"
void MeasCfguMemProxy::assigntotal_num( MeasCfgu_reg value )
{
	mem_assign(total_num,value);
}


void MeasCfguMemProxy::assignmeas_statis_ctl( MeasCfgu_reg value )
{
	mem_assign(meas_statis_ctl,value);
}


void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,cha_top_type_sel,value);
}
void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,cha_base_type_sel,value);
}
void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,chb_top_type_sel,value);
}
void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,chb_base_type_sel,value);
}
void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,chc_top_type_sel,value);
}
void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,chc_base_type_sel,value);
}
void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,chd_top_type_sel,value);
}
void MeasCfguMemProxy::assignmeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,chd_base_type_sel,value);
}

void MeasCfguMemProxy::assignmeas_vtop_base_result_sel( MeasCfgu_reg value )
{
	mem_assign_field(meas_vtop_base_result_sel,payload,value);
}


void MeasCfguMemProxy::assigncha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_l_cfg,value);
}
void MeasCfguMemProxy::assigncha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg value )
{
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_mid_cfg,value);
}
void MeasCfguMemProxy::assigncha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg value )
{
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_h_cfg,value);
}
void MeasCfguMemProxy::assigncha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg value )
{
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_abs_percent_sel,value);
}

void MeasCfguMemProxy::assigncha_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(cha_threshold_h_mid_l_cfg,payload,value);
}


void MeasCfguMemProxy::assignchb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_l_cfg,value);
}
void MeasCfguMemProxy::assignchb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_mid_cfg,value);
}
void MeasCfguMemProxy::assignchb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_h_cfg,value);
}
void MeasCfguMemProxy::assignchb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg value )
{
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_abs_percent_sel,value);
}

void MeasCfguMemProxy::assignchb_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chb_threshold_h_mid_l_cfg,payload,value);
}


void MeasCfguMemProxy::assignchc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_l_cfg,value);
}
void MeasCfguMemProxy::assignchc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_mid_cfg,value);
}
void MeasCfguMemProxy::assignchc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_h_cfg,value);
}
void MeasCfguMemProxy::assignchc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg value )
{
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_abs_percent_sel,value);
}

void MeasCfguMemProxy::assignchc_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chc_threshold_h_mid_l_cfg,payload,value);
}


void MeasCfguMemProxy::assignchd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_l_cfg,value);
}
void MeasCfguMemProxy::assignchd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_mid_cfg,value);
}
void MeasCfguMemProxy::assignchd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_h_cfg,value);
}
void MeasCfguMemProxy::assignchd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg value )
{
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_abs_percent_sel,value);
}

void MeasCfguMemProxy::assignchd_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	mem_assign_field(chd_threshold_h_mid_l_cfg,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_1_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_1,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_1_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_1,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_1,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_1,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_1( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_1,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_2_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_2,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_2_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_2,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_2,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_2,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_2( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_2,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_3_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_3,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_3_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_3,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_3,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_3,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_3( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_3,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_4_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_4,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_4_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_4,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_4,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_4,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_4( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_4,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_5_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_5,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_5_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_5,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_5,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_5,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_5( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_5,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_6_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_6,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_6_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_6,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_6,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_6,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_6( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_6,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_7_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_7,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_7_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_7,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_7,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_7,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_7( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_7,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_8_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_8,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_8_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_8,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_8,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_8,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_8( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_8,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_9_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_9,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_9_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_9,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_9,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_9,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_9( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_9,payload,value);
}


void MeasCfguMemProxy::assigndly_phase_cfg_10_edge_from_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_10,edge_from_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_10_edge_to_type( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_10,edge_to_type,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_10,edge_from_channel_sel,value);
}
void MeasCfguMemProxy::assigndly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_10,edge_to_channel_sel,value);
}

void MeasCfguMemProxy::assigndly_phase_cfg_10( MeasCfgu_reg value )
{
	mem_assign_field(dly_phase_cfg_10,payload,value);
}


void MeasCfguMemProxy::assignmeas_cycnum_meas_cycnum( MeasCfgu_reg value )
{
	mem_assign_field(meas_cycnum,meas_cycnum,value);
}

void MeasCfguMemProxy::assignmeas_cycnum( MeasCfgu_reg value )
{
	mem_assign_field(meas_cycnum,payload,value);
}


void MeasCfguMemProxy::assignanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg value )
{
	mem_assign_field(analog_la_sample_rate,analog_la_sample_rate,value);
}

void MeasCfguMemProxy::assignanalog_la_sample_rate( MeasCfgu_reg value )
{
	mem_assign_field(analog_la_sample_rate,payload,value);
}


void MeasCfguMemProxy::assignresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg value )
{
	mem_assign_field(result_rdduring_flag,result_rdduring_flag,value);
}

void MeasCfguMemProxy::assignresult_rdduring_flag( MeasCfgu_reg value )
{
	mem_assign_field(result_rdduring_flag,payload,value);
}


void MeasCfguMemProxy::assignrw_test_reg_rw_test_reg( MeasCfgu_reg value )
{
	mem_assign_field(rw_test_reg,rw_test_reg,value);
}

void MeasCfguMemProxy::assignrw_test_reg( MeasCfgu_reg value )
{
	mem_assign_field(rw_test_reg,payload,value);
}



void MeasCfguMem::_loadOtp()
{
	total_num=0x0;
	meas_statis_ctl=0x0;
	meas_vtop_base_result_sel.payload=0x0;
		meas_vtop_base_result_sel.cha_top_type_sel=0x0;
		meas_vtop_base_result_sel.cha_base_type_sel=0x0;
		meas_vtop_base_result_sel.chb_top_type_sel=0x0;
		meas_vtop_base_result_sel.chb_base_type_sel=0x0;

		meas_vtop_base_result_sel.chc_top_type_sel=0x0;
		meas_vtop_base_result_sel.chc_base_type_sel=0x0;
		meas_vtop_base_result_sel.chd_top_type_sel=0x0;
		meas_vtop_base_result_sel.chd_base_type_sel=0x0;

	cha_threshold_h_mid_l_cfg.payload=0x0;
		cha_threshold_h_mid_l_cfg.cha_threshold_l_cfg=0x14;
		cha_threshold_h_mid_l_cfg.cha_threshold_mid_cfg=0x32;
		cha_threshold_h_mid_l_cfg.cha_threshold_h_cfg=0x50;
		cha_threshold_h_mid_l_cfg.cha_abs_percent_sel=0x0;


	chb_threshold_h_mid_l_cfg.payload=0x0;
		chb_threshold_h_mid_l_cfg.chb_threshold_l_cfg=0x14;
		chb_threshold_h_mid_l_cfg.chb_threshold_mid_cfg=0x32;
		chb_threshold_h_mid_l_cfg.chb_threshold_h_cfg=0x50;
		chb_threshold_h_mid_l_cfg.chb_abs_percent_sel=0x0;

	chc_threshold_h_mid_l_cfg.payload=0x0;
		chc_threshold_h_mid_l_cfg.chc_threshold_l_cfg=0x14;
		chc_threshold_h_mid_l_cfg.chc_threshold_mid_cfg=0x32;
		chc_threshold_h_mid_l_cfg.chc_threshold_h_cfg=0x50;
		chc_threshold_h_mid_l_cfg.chc_abs_percent_sel=0x0;

	chd_threshold_h_mid_l_cfg.payload=0x0;
		chd_threshold_h_mid_l_cfg.chd_threshold_l_cfg=0x14;
		chd_threshold_h_mid_l_cfg.chd_threshold_mid_cfg=0x32;
		chd_threshold_h_mid_l_cfg.chd_threshold_h_cfg=0x50;
		chd_threshold_h_mid_l_cfg.chd_abs_percent_sel=0x0;

	dly_phase_cfg_1.payload=0x0;
		dly_phase_cfg_1.edge_from_type=0x0;
		dly_phase_cfg_1.edge_to_type=0x0;
		dly_phase_cfg_1.edge_from_channel_sel=0x0;
		dly_phase_cfg_1.edge_to_channel_sel=0x0;


	dly_phase_cfg_2.payload=0x0;
		dly_phase_cfg_2.edge_from_type=0x0;
		dly_phase_cfg_2.edge_to_type=0x0;
		dly_phase_cfg_2.edge_from_channel_sel=0x0;
		dly_phase_cfg_2.edge_to_channel_sel=0x1;

	dly_phase_cfg_3.payload=0x0;
		dly_phase_cfg_3.edge_from_type=0x0;
		dly_phase_cfg_3.edge_to_type=0x0;
		dly_phase_cfg_3.edge_from_channel_sel=0x0;
		dly_phase_cfg_3.edge_to_channel_sel=0x10;

	dly_phase_cfg_4.payload=0x0;
		dly_phase_cfg_4.edge_from_type=0x0;
		dly_phase_cfg_4.edge_to_type=0x0;
		dly_phase_cfg_4.edge_from_channel_sel=0x0;
		dly_phase_cfg_4.edge_to_channel_sel=0x0;

	dly_phase_cfg_5.payload=0x0;
		dly_phase_cfg_5.edge_from_type=0x0;
		dly_phase_cfg_5.edge_to_type=0x0;
		dly_phase_cfg_5.edge_from_channel_sel=0x0;
		dly_phase_cfg_5.edge_to_channel_sel=0x0;


	dly_phase_cfg_6.payload=0x0;
		dly_phase_cfg_6.edge_from_type=0x0;
		dly_phase_cfg_6.edge_to_type=0x0;
		dly_phase_cfg_6.edge_from_channel_sel=0x0;
		dly_phase_cfg_6.edge_to_channel_sel=0x0;

	dly_phase_cfg_7.payload=0x0;
		dly_phase_cfg_7.edge_from_type=0x0;
		dly_phase_cfg_7.edge_to_type=0x0;
		dly_phase_cfg_7.edge_from_channel_sel=0x0;
		dly_phase_cfg_7.edge_to_channel_sel=0x0;

	dly_phase_cfg_8.payload=0x0;
		dly_phase_cfg_8.edge_from_type=0x0;
		dly_phase_cfg_8.edge_to_type=0x0;
		dly_phase_cfg_8.edge_from_channel_sel=0x0;
		dly_phase_cfg_8.edge_to_channel_sel=0x1;

	dly_phase_cfg_9.payload=0x0;
		dly_phase_cfg_9.edge_from_type=0x0;
		dly_phase_cfg_9.edge_to_type=0x0;
		dly_phase_cfg_9.edge_from_channel_sel=0x0;
		dly_phase_cfg_9.edge_to_channel_sel=0x10;


	dly_phase_cfg_10.payload=0x0;
		dly_phase_cfg_10.edge_from_type=0x0;
		dly_phase_cfg_10.edge_to_type=0x0;
		dly_phase_cfg_10.edge_from_channel_sel=0x0;
		dly_phase_cfg_10.edge_to_channel_sel=0x0;

	meas_cycnum.payload=0x0;
		meas_cycnum.meas_cycnum=0x0;

	analog_la_sample_rate.payload=0x0;
		analog_la_sample_rate.analog_la_sample_rate=0x0;

	result_rdduring_flag.payload=0x0;
		result_rdduring_flag.result_rdduring_flag=0x0;


	rw_test_reg.payload=0x0;
		rw_test_reg.rw_test_reg=0x1234;

}

void MeasCfguMem::_outOtp()
{
	outtotal_num();
	outmeas_statis_ctl();
	outmeas_vtop_base_result_sel();
	outcha_threshold_h_mid_l_cfg();

	outchb_threshold_h_mid_l_cfg();
	outchc_threshold_h_mid_l_cfg();
	outchd_threshold_h_mid_l_cfg();
	outdly_phase_cfg_1();

	outdly_phase_cfg_2();
	outdly_phase_cfg_3();
	outdly_phase_cfg_4();
	outdly_phase_cfg_5();

	outdly_phase_cfg_6();
	outdly_phase_cfg_7();
	outdly_phase_cfg_8();
	outdly_phase_cfg_9();

	outdly_phase_cfg_10();
	outmeas_cycnum();
	outanalog_la_sample_rate();
	outresult_rdduring_flag();

	outrw_test_reg();
}
void MeasCfguMem::push( MeasCfguMemProxy *proxy )
{
	if ( total_num != proxy->total_num )
	{reg_assign(total_num,proxy->total_num);}

	if ( meas_statis_ctl != proxy->meas_statis_ctl )
	{reg_assign(meas_statis_ctl,proxy->meas_statis_ctl);}

	if ( meas_vtop_base_result_sel.payload != proxy->meas_vtop_base_result_sel.payload )
	{reg_assign_field(meas_vtop_base_result_sel,payload,proxy->meas_vtop_base_result_sel.payload);}

	if ( cha_threshold_h_mid_l_cfg.payload != proxy->cha_threshold_h_mid_l_cfg.payload )
	{reg_assign_field(cha_threshold_h_mid_l_cfg,payload,proxy->cha_threshold_h_mid_l_cfg.payload);}

	if ( chb_threshold_h_mid_l_cfg.payload != proxy->chb_threshold_h_mid_l_cfg.payload )
	{reg_assign_field(chb_threshold_h_mid_l_cfg,payload,proxy->chb_threshold_h_mid_l_cfg.payload);}

	if ( chc_threshold_h_mid_l_cfg.payload != proxy->chc_threshold_h_mid_l_cfg.payload )
	{reg_assign_field(chc_threshold_h_mid_l_cfg,payload,proxy->chc_threshold_h_mid_l_cfg.payload);}

	if ( chd_threshold_h_mid_l_cfg.payload != proxy->chd_threshold_h_mid_l_cfg.payload )
	{reg_assign_field(chd_threshold_h_mid_l_cfg,payload,proxy->chd_threshold_h_mid_l_cfg.payload);}

	if ( dly_phase_cfg_1.payload != proxy->dly_phase_cfg_1.payload )
	{reg_assign_field(dly_phase_cfg_1,payload,proxy->dly_phase_cfg_1.payload);}

	if ( dly_phase_cfg_2.payload != proxy->dly_phase_cfg_2.payload )
	{reg_assign_field(dly_phase_cfg_2,payload,proxy->dly_phase_cfg_2.payload);}

	if ( dly_phase_cfg_3.payload != proxy->dly_phase_cfg_3.payload )
	{reg_assign_field(dly_phase_cfg_3,payload,proxy->dly_phase_cfg_3.payload);}

	if ( dly_phase_cfg_4.payload != proxy->dly_phase_cfg_4.payload )
	{reg_assign_field(dly_phase_cfg_4,payload,proxy->dly_phase_cfg_4.payload);}

	if ( dly_phase_cfg_5.payload != proxy->dly_phase_cfg_5.payload )
	{reg_assign_field(dly_phase_cfg_5,payload,proxy->dly_phase_cfg_5.payload);}

	if ( dly_phase_cfg_6.payload != proxy->dly_phase_cfg_6.payload )
	{reg_assign_field(dly_phase_cfg_6,payload,proxy->dly_phase_cfg_6.payload);}

	if ( dly_phase_cfg_7.payload != proxy->dly_phase_cfg_7.payload )
	{reg_assign_field(dly_phase_cfg_7,payload,proxy->dly_phase_cfg_7.payload);}

	if ( dly_phase_cfg_8.payload != proxy->dly_phase_cfg_8.payload )
	{reg_assign_field(dly_phase_cfg_8,payload,proxy->dly_phase_cfg_8.payload);}

	if ( dly_phase_cfg_9.payload != proxy->dly_phase_cfg_9.payload )
	{reg_assign_field(dly_phase_cfg_9,payload,proxy->dly_phase_cfg_9.payload);}

	if ( dly_phase_cfg_10.payload != proxy->dly_phase_cfg_10.payload )
	{reg_assign_field(dly_phase_cfg_10,payload,proxy->dly_phase_cfg_10.payload);}

	if ( meas_cycnum.payload != proxy->meas_cycnum.payload )
	{reg_assign_field(meas_cycnum,payload,proxy->meas_cycnum.payload);}

	if ( analog_la_sample_rate.payload != proxy->analog_la_sample_rate.payload )
	{reg_assign_field(analog_la_sample_rate,payload,proxy->analog_la_sample_rate.payload);}

	if ( result_rdduring_flag.payload != proxy->result_rdduring_flag.payload )
	{reg_assign_field(result_rdduring_flag,payload,proxy->result_rdduring_flag.payload);}

	if ( rw_test_reg.payload != proxy->rw_test_reg.payload )
	{reg_assign_field(rw_test_reg,payload,proxy->rw_test_reg.payload);}

}
void MeasCfguMem::pull( MeasCfguMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);


	*proxy = MeasCfguMemProxy(*this);
}
void MeasCfguMem::settotal_num( MeasCfgu_reg value )
{
	reg_assign(total_num,value);
}

void MeasCfguMem::outtotal_num( MeasCfgu_reg value )
{
	out_assign(total_num,value);
}

void MeasCfguMem::outtotal_num()
{
	out_member(total_num);
}


void MeasCfguMem::setmeas_statis_ctl( MeasCfgu_reg value )
{
	reg_assign(meas_statis_ctl,value);
}

void MeasCfguMem::outmeas_statis_ctl( MeasCfgu_reg value )
{
	out_assign(meas_statis_ctl,value);
}

void MeasCfguMem::outmeas_statis_ctl()
{
	out_member(meas_statis_ctl);
}


void MeasCfguMem::setmeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,cha_top_type_sel,value);
}
void MeasCfguMem::setmeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,cha_base_type_sel,value);
}
void MeasCfguMem::setmeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,chb_top_type_sel,value);
}
void MeasCfguMem::setmeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,chb_base_type_sel,value);
}
void MeasCfguMem::setmeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,chc_top_type_sel,value);
}
void MeasCfguMem::setmeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,chc_base_type_sel,value);
}
void MeasCfguMem::setmeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,chd_top_type_sel,value);
}
void MeasCfguMem::setmeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,chd_base_type_sel,value);
}

void MeasCfguMem::setmeas_vtop_base_result_sel( MeasCfgu_reg value )
{
	reg_assign_field(meas_vtop_base_result_sel,payload,value);
}

void MeasCfguMem::outmeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,cha_top_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_cha_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,cha_top_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,cha_top_type_sel,idleV);
}
void MeasCfguMem::outmeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,cha_base_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_cha_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,cha_base_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,cha_base_type_sel,idleV);
}
void MeasCfguMem::outmeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,chb_top_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_chb_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,chb_top_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,chb_top_type_sel,idleV);
}
void MeasCfguMem::outmeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,chb_base_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_chb_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,chb_base_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,chb_base_type_sel,idleV);
}
void MeasCfguMem::outmeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,chc_top_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_chc_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,chc_top_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,chc_top_type_sel,idleV);
}
void MeasCfguMem::outmeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,chc_base_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_chc_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,chc_base_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,chc_base_type_sel,idleV);
}
void MeasCfguMem::outmeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,chd_top_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_chd_top_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,chd_top_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,chd_top_type_sel,idleV);
}
void MeasCfguMem::outmeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,chd_base_type_sel,value);
}
void MeasCfguMem::pulsemeas_vtop_base_result_sel_chd_base_type_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_vtop_base_result_sel,chd_base_type_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_vtop_base_result_sel,chd_base_type_sel,idleV);
}

void MeasCfguMem::outmeas_vtop_base_result_sel( MeasCfgu_reg value )
{
	out_assign_field(meas_vtop_base_result_sel,payload,value);
}

void MeasCfguMem::outmeas_vtop_base_result_sel()
{
	out_member(meas_vtop_base_result_sel);
}


void MeasCfguMem::setcha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_l_cfg,value);
}
void MeasCfguMem::setcha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg value )
{
	reg_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_mid_cfg,value);
}
void MeasCfguMem::setcha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg value )
{
	reg_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_h_cfg,value);
}
void MeasCfguMem::setcha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg value )
{
	reg_assign_field(cha_threshold_h_mid_l_cfg,cha_abs_percent_sel,value);
}

void MeasCfguMem::setcha_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(cha_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outcha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_l_cfg,value);
}
void MeasCfguMem::pulsecha_threshold_h_mid_l_cfg_cha_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_l_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_l_cfg,idleV);
}
void MeasCfguMem::outcha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg value )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_mid_cfg,value);
}
void MeasCfguMem::pulsecha_threshold_h_mid_l_cfg_cha_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_mid_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_mid_cfg,idleV);
}
void MeasCfguMem::outcha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg value )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_h_cfg,value);
}
void MeasCfguMem::pulsecha_threshold_h_mid_l_cfg_cha_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_h_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_threshold_h_cfg,idleV);
}
void MeasCfguMem::outcha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg value )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_abs_percent_sel,value);
}
void MeasCfguMem::pulsecha_threshold_h_mid_l_cfg_cha_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,cha_abs_percent_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(cha_threshold_h_mid_l_cfg,cha_abs_percent_sel,idleV);
}

void MeasCfguMem::outcha_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(cha_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outcha_threshold_h_mid_l_cfg()
{
	out_member(cha_threshold_h_mid_l_cfg);
}


void MeasCfguMem::setchb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_l_cfg,value);
}
void MeasCfguMem::setchb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_mid_cfg,value);
}
void MeasCfguMem::setchb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_h_cfg,value);
}
void MeasCfguMem::setchb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg value )
{
	reg_assign_field(chb_threshold_h_mid_l_cfg,chb_abs_percent_sel,value);
}

void MeasCfguMem::setchb_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chb_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outchb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_l_cfg,value);
}
void MeasCfguMem::pulsechb_threshold_h_mid_l_cfg_chb_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_l_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_l_cfg,idleV);
}
void MeasCfguMem::outchb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg value )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_mid_cfg,value);
}
void MeasCfguMem::pulsechb_threshold_h_mid_l_cfg_chb_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_mid_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_mid_cfg,idleV);
}
void MeasCfguMem::outchb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg value )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_h_cfg,value);
}
void MeasCfguMem::pulsechb_threshold_h_mid_l_cfg_chb_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_h_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_threshold_h_cfg,idleV);
}
void MeasCfguMem::outchb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg value )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_abs_percent_sel,value);
}
void MeasCfguMem::pulsechb_threshold_h_mid_l_cfg_chb_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,chb_abs_percent_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chb_threshold_h_mid_l_cfg,chb_abs_percent_sel,idleV);
}

void MeasCfguMem::outchb_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(chb_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outchb_threshold_h_mid_l_cfg()
{
	out_member(chb_threshold_h_mid_l_cfg);
}


void MeasCfguMem::setchc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_l_cfg,value);
}
void MeasCfguMem::setchc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_mid_cfg,value);
}
void MeasCfguMem::setchc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_h_cfg,value);
}
void MeasCfguMem::setchc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg value )
{
	reg_assign_field(chc_threshold_h_mid_l_cfg,chc_abs_percent_sel,value);
}

void MeasCfguMem::setchc_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chc_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outchc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_l_cfg,value);
}
void MeasCfguMem::pulsechc_threshold_h_mid_l_cfg_chc_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_l_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_l_cfg,idleV);
}
void MeasCfguMem::outchc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg value )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_mid_cfg,value);
}
void MeasCfguMem::pulsechc_threshold_h_mid_l_cfg_chc_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_mid_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_mid_cfg,idleV);
}
void MeasCfguMem::outchc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg value )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_h_cfg,value);
}
void MeasCfguMem::pulsechc_threshold_h_mid_l_cfg_chc_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_h_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_threshold_h_cfg,idleV);
}
void MeasCfguMem::outchc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg value )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_abs_percent_sel,value);
}
void MeasCfguMem::pulsechc_threshold_h_mid_l_cfg_chc_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,chc_abs_percent_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chc_threshold_h_mid_l_cfg,chc_abs_percent_sel,idleV);
}

void MeasCfguMem::outchc_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(chc_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outchc_threshold_h_mid_l_cfg()
{
	out_member(chc_threshold_h_mid_l_cfg);
}


void MeasCfguMem::setchd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_l_cfg,value);
}
void MeasCfguMem::setchd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_mid_cfg,value);
}
void MeasCfguMem::setchd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_h_cfg,value);
}
void MeasCfguMem::setchd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg value )
{
	reg_assign_field(chd_threshold_h_mid_l_cfg,chd_abs_percent_sel,value);
}

void MeasCfguMem::setchd_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	reg_assign_field(chd_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outchd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_l_cfg,value);
}
void MeasCfguMem::pulsechd_threshold_h_mid_l_cfg_chd_threshold_l_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_l_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_l_cfg,idleV);
}
void MeasCfguMem::outchd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg value )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_mid_cfg,value);
}
void MeasCfguMem::pulsechd_threshold_h_mid_l_cfg_chd_threshold_mid_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_mid_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_mid_cfg,idleV);
}
void MeasCfguMem::outchd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg value )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_h_cfg,value);
}
void MeasCfguMem::pulsechd_threshold_h_mid_l_cfg_chd_threshold_h_cfg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_h_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_threshold_h_cfg,idleV);
}
void MeasCfguMem::outchd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg value )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_abs_percent_sel,value);
}
void MeasCfguMem::pulsechd_threshold_h_mid_l_cfg_chd_abs_percent_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,chd_abs_percent_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(chd_threshold_h_mid_l_cfg,chd_abs_percent_sel,idleV);
}

void MeasCfguMem::outchd_threshold_h_mid_l_cfg( MeasCfgu_reg value )
{
	out_assign_field(chd_threshold_h_mid_l_cfg,payload,value);
}

void MeasCfguMem::outchd_threshold_h_mid_l_cfg()
{
	out_member(chd_threshold_h_mid_l_cfg);
}


void MeasCfguMem::setdly_phase_cfg_1_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_1,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_1_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_1,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_1,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_1,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_1( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_1,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_1_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_1,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_1_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_1,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_1,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_1_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_1,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_1_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_1,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_1,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_1,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_1_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_1,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_1,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_1,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_1_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_1,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_1,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_1( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_1,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_1()
{
	out_member(dly_phase_cfg_1);
}


void MeasCfguMem::setdly_phase_cfg_2_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_2,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_2_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_2,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_2,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_2,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_2( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_2,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_2_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_2,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_2_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_2,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_2,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_2_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_2,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_2_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_2,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_2,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_2,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_2_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_2,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_2,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_2,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_2_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_2,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_2,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_2( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_2,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_2()
{
	out_member(dly_phase_cfg_2);
}


void MeasCfguMem::setdly_phase_cfg_3_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_3,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_3_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_3,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_3,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_3,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_3( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_3,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_3_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_3,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_3_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_3,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_3,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_3_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_3,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_3_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_3,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_3,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_3,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_3_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_3,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_3,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_3,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_3_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_3,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_3,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_3( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_3,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_3()
{
	out_member(dly_phase_cfg_3);
}


void MeasCfguMem::setdly_phase_cfg_4_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_4,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_4_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_4,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_4,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_4,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_4( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_4,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_4_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_4,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_4_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_4,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_4,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_4_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_4,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_4_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_4,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_4,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_4,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_4_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_4,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_4,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_4,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_4_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_4,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_4,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_4( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_4,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_4()
{
	out_member(dly_phase_cfg_4);
}


void MeasCfguMem::setdly_phase_cfg_5_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_5,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_5_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_5,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_5,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_5,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_5( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_5,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_5_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_5,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_5_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_5,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_5,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_5_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_5,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_5_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_5,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_5,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_5,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_5_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_5,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_5,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_5,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_5_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_5,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_5,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_5( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_5,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_5()
{
	out_member(dly_phase_cfg_5);
}


void MeasCfguMem::setdly_phase_cfg_6_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_6,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_6_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_6,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_6,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_6,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_6( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_6,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_6_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_6,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_6_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_6,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_6,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_6_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_6,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_6_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_6,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_6,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_6,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_6_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_6,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_6,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_6,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_6_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_6,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_6,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_6( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_6,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_6()
{
	out_member(dly_phase_cfg_6);
}


void MeasCfguMem::setdly_phase_cfg_7_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_7,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_7_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_7,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_7,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_7,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_7( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_7,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_7_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_7,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_7_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_7,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_7,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_7_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_7,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_7_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_7,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_7,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_7,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_7_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_7,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_7,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_7,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_7_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_7,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_7,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_7( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_7,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_7()
{
	out_member(dly_phase_cfg_7);
}


void MeasCfguMem::setdly_phase_cfg_8_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_8,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_8_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_8,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_8,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_8,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_8( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_8,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_8_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_8,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_8_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_8,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_8,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_8_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_8,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_8_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_8,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_8,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_8,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_8_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_8,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_8,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_8,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_8_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_8,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_8,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_8( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_8,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_8()
{
	out_member(dly_phase_cfg_8);
}


void MeasCfguMem::setdly_phase_cfg_9_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_9,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_9_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_9,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_9,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_9,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_9( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_9,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_9_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_9,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_9_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_9,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_9,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_9_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_9,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_9_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_9,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_9,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_9,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_9_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_9,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_9,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_9,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_9_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_9,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_9,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_9( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_9,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_9()
{
	out_member(dly_phase_cfg_9);
}


void MeasCfguMem::setdly_phase_cfg_10_edge_from_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_10,edge_from_type,value);
}
void MeasCfguMem::setdly_phase_cfg_10_edge_to_type( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_10,edge_to_type,value);
}
void MeasCfguMem::setdly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_10,edge_from_channel_sel,value);
}
void MeasCfguMem::setdly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_10,edge_to_channel_sel,value);
}

void MeasCfguMem::setdly_phase_cfg_10( MeasCfgu_reg value )
{
	reg_assign_field(dly_phase_cfg_10,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_10_edge_from_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_10,edge_from_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_10_edge_from_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_10,edge_from_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_10,edge_from_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_10_edge_to_type( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_10,edge_to_type,value);
}
void MeasCfguMem::pulsedly_phase_cfg_10_edge_to_type( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_10,edge_to_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_10,edge_to_type,idleV);
}
void MeasCfguMem::outdly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_10,edge_from_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_10_edge_from_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_10,edge_from_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_10,edge_from_channel_sel,idleV);
}
void MeasCfguMem::outdly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_10,edge_to_channel_sel,value);
}
void MeasCfguMem::pulsedly_phase_cfg_10_edge_to_channel_sel( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(dly_phase_cfg_10,edge_to_channel_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(dly_phase_cfg_10,edge_to_channel_sel,idleV);
}

void MeasCfguMem::outdly_phase_cfg_10( MeasCfgu_reg value )
{
	out_assign_field(dly_phase_cfg_10,payload,value);
}

void MeasCfguMem::outdly_phase_cfg_10()
{
	out_member(dly_phase_cfg_10);
}


void MeasCfguMem::setmeas_cycnum_meas_cycnum( MeasCfgu_reg value )
{
	reg_assign_field(meas_cycnum,meas_cycnum,value);
}

void MeasCfguMem::setmeas_cycnum( MeasCfgu_reg value )
{
	reg_assign_field(meas_cycnum,payload,value);
}

void MeasCfguMem::outmeas_cycnum_meas_cycnum( MeasCfgu_reg value )
{
	out_assign_field(meas_cycnum,meas_cycnum,value);
}
void MeasCfguMem::pulsemeas_cycnum_meas_cycnum( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(meas_cycnum,meas_cycnum,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(meas_cycnum,meas_cycnum,idleV);
}

void MeasCfguMem::outmeas_cycnum( MeasCfgu_reg value )
{
	out_assign_field(meas_cycnum,payload,value);
}

void MeasCfguMem::outmeas_cycnum()
{
	out_member(meas_cycnum);
}


void MeasCfguMem::setanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg value )
{
	reg_assign_field(analog_la_sample_rate,analog_la_sample_rate,value);
}

void MeasCfguMem::setanalog_la_sample_rate( MeasCfgu_reg value )
{
	reg_assign_field(analog_la_sample_rate,payload,value);
}

void MeasCfguMem::outanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg value )
{
	out_assign_field(analog_la_sample_rate,analog_la_sample_rate,value);
}
void MeasCfguMem::pulseanalog_la_sample_rate_analog_la_sample_rate( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(analog_la_sample_rate,analog_la_sample_rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(analog_la_sample_rate,analog_la_sample_rate,idleV);
}

void MeasCfguMem::outanalog_la_sample_rate( MeasCfgu_reg value )
{
	out_assign_field(analog_la_sample_rate,payload,value);
}

void MeasCfguMem::outanalog_la_sample_rate()
{
	out_member(analog_la_sample_rate);
}


void MeasCfguMem::setresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg value )
{
	reg_assign_field(result_rdduring_flag,result_rdduring_flag,value);
}

void MeasCfguMem::setresult_rdduring_flag( MeasCfgu_reg value )
{
	reg_assign_field(result_rdduring_flag,payload,value);
}

void MeasCfguMem::outresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg value )
{
	out_assign_field(result_rdduring_flag,result_rdduring_flag,value);
}
void MeasCfguMem::pulseresult_rdduring_flag_result_rdduring_flag( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(result_rdduring_flag,result_rdduring_flag,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(result_rdduring_flag,result_rdduring_flag,idleV);
}

void MeasCfguMem::outresult_rdduring_flag( MeasCfgu_reg value )
{
	out_assign_field(result_rdduring_flag,payload,value);
}

void MeasCfguMem::outresult_rdduring_flag()
{
	out_member(result_rdduring_flag);
}


void MeasCfguMem::setrw_test_reg_rw_test_reg( MeasCfgu_reg value )
{
	reg_assign_field(rw_test_reg,rw_test_reg,value);
}

void MeasCfguMem::setrw_test_reg( MeasCfgu_reg value )
{
	reg_assign_field(rw_test_reg,payload,value);
}

void MeasCfguMem::outrw_test_reg_rw_test_reg( MeasCfgu_reg value )
{
	out_assign_field(rw_test_reg,rw_test_reg,value);
}
void MeasCfguMem::pulserw_test_reg_rw_test_reg( MeasCfgu_reg activeV, MeasCfgu_reg idleV, int timeus )
{
	out_assign_field(rw_test_reg,rw_test_reg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(rw_test_reg,rw_test_reg,idleV);
}

void MeasCfguMem::outrw_test_reg( MeasCfgu_reg value )
{
	out_assign_field(rw_test_reg,payload,value);
}

void MeasCfguMem::outrw_test_reg()
{
	out_member(rw_test_reg);
}


cache_addr MeasCfguMem::addr_total_num(){ return 0x2400+mAddrBase; }
cache_addr MeasCfguMem::addr_meas_statis_ctl(){ return 0x2404+mAddrBase; }
cache_addr MeasCfguMem::addr_meas_vtop_base_result_sel(){ return 0x2408+mAddrBase; }
cache_addr MeasCfguMem::addr_cha_threshold_h_mid_l_cfg(){ return 0x240c+mAddrBase; }

cache_addr MeasCfguMem::addr_chb_threshold_h_mid_l_cfg(){ return 0x2410+mAddrBase; }
cache_addr MeasCfguMem::addr_chc_threshold_h_mid_l_cfg(){ return 0x2414+mAddrBase; }
cache_addr MeasCfguMem::addr_chd_threshold_h_mid_l_cfg(){ return 0x2418+mAddrBase; }
cache_addr MeasCfguMem::addr_dly_phase_cfg_1(){ return 0x241c+mAddrBase; }

cache_addr MeasCfguMem::addr_dly_phase_cfg_2(){ return 0x2420+mAddrBase; }
cache_addr MeasCfguMem::addr_dly_phase_cfg_3(){ return 0x2424+mAddrBase; }
cache_addr MeasCfguMem::addr_dly_phase_cfg_4(){ return 0x2428+mAddrBase; }
cache_addr MeasCfguMem::addr_dly_phase_cfg_5(){ return 0x242c+mAddrBase; }

cache_addr MeasCfguMem::addr_dly_phase_cfg_6(){ return 0x2430+mAddrBase; }
cache_addr MeasCfguMem::addr_dly_phase_cfg_7(){ return 0x2434+mAddrBase; }
cache_addr MeasCfguMem::addr_dly_phase_cfg_8(){ return 0x2438+mAddrBase; }
cache_addr MeasCfguMem::addr_dly_phase_cfg_9(){ return 0x243c+mAddrBase; }

cache_addr MeasCfguMem::addr_dly_phase_cfg_10(){ return 0x2440+mAddrBase; }
cache_addr MeasCfguMem::addr_meas_cycnum(){ return 0x2470+mAddrBase; }
cache_addr MeasCfguMem::addr_analog_la_sample_rate(){ return 0x2474+mAddrBase; }
cache_addr MeasCfguMem::addr_result_rdduring_flag(){ return 0x2478+mAddrBase; }

cache_addr MeasCfguMem::addr_rw_test_reg(){ return 0x247c+mAddrBase; }


