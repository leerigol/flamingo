#include "zynqfcurd.h"
ZynqFcuRd_reg ZynqFcuRdMemProxy::getVERSION()
{
	return VERSION;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getcomm_test()
{
	return comm_test;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::gettrace_wddr3_done_trace_done()
{
	return trace_wddr3_done.trace_done;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::gettrace_wddr3_done()
{
	return trace_wddr3_done.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getfpga_rdat_done()
{
	return fpga_rdat_done;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdeinterweave_done_deinter_done()
{
	return deinterweave_done.deinter_done;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getdeinterweave_done()
{
	return deinterweave_done.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getreserved()
{
	return reserved;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getroll_ana_trace_waddr_latch()
{
	return roll_ana_trace_waddr_latch;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getroll_la_trace_waddr_latch()
{
	return roll_la_trace_waddr_latch;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getroll_full_screen_wdone_flag_roll_full_screen_wdone_flag()
{
	return roll_full_screen_wdone_flag.roll_full_screen_wdone_flag;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getroll_full_screen_wdone_flag()
{
	return roll_full_screen_wdone_flag.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getsearch_result_wdone_search_result_current_col()
{
	return search_result_wdone.search_result_current_col;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getsearch_result_wdone_search_result_wdone()
{
	return search_result_wdone.search_result_wdone;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getsearch_result_wdone()
{
	return search_result_wdone.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getframe_id_frame_all_id()
{
	return frame_id.frame_all_id;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getframe_id_frame_trace_id()
{
	return frame_id.frame_trace_id;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getframe_id_frame_all_id_expand()
{
	return frame_id.frame_all_id_expand;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getframe_id_frame_trace_id_expand()
{
	return frame_id.frame_trace_id_expand;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getframe_id()
{
	return frame_id.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getaxi_rd_overtime_flag_axi_rd_overtime_flag()
{
	return axi_rd_overtime_flag.axi_rd_overtime_flag;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getaxi_rd_overtime_flag()
{
	return axi_rd_overtime_flag.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_trace_frame_cnt()
{
	return dbg_rd_reg_1.trace_frame_cnt;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_trace_req_state()
{
	return dbg_rd_reg_1.trace_req_state;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_trace_dat_send_en()
{
	return dbg_rd_reg_1.trace_dat_send_en;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_trace_dat_last_mask()
{
	return dbg_rd_reg_1.trace_dat_last_mask;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_trace_req_dtype_latch()
{
	return dbg_rd_reg_1.trace_req_dtype_latch;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_current_dat_type()
{
	return dbg_rd_reg_1.current_dat_type;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_export_trace_flag()
{
	return dbg_rd_reg_1.export_trace_flag;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1_gtp_src_cnt()
{
	return dbg_rd_reg_1.gtp_src_cnt;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_1()
{
	return dbg_rd_reg_1.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_2()
{
	return dbg_rd_reg_2;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_3()
{
	return dbg_rd_reg_3;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_4()
{
	return dbg_rd_reg_4;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_rd_reg_5()
{
	return dbg_rd_reg_5;
}


void ZynqFcuRdMemProxy::assigndbg_cfg2( ZynqFcuRd_reg value )
{
	mem_assign(dbg_cfg2,value);
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_main_ana_trace_num()
{
	return dbg_main_ana_trace_num;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_zoom_ana_num()
{
	return dbg_zoom_ana_num;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_frm_data_type()
{
	return dbg_debug_head_frame1_63_32.frm_data_type;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_sys_proc_la()
{
	return dbg_debug_head_frame1_63_32.sys_proc_la;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_frm_plot_type()
{
	return dbg_debug_head_frame1_63_32.frm_plot_type;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_sys_zoom()
{
	return dbg_debug_head_frame1_63_32.sys_zoom;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_sys_eye()
{
	return dbg_debug_head_frame1_63_32.sys_eye;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_frm_obj_wpu()
{
	return dbg_debug_head_frame1_63_32.frm_obj_wpu;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_frm_obj_trace()
{
	return dbg_debug_head_frame1_63_32.frm_obj_trace;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32_FRM_HEAD()
{
	return dbg_debug_head_frame1_63_32.FRM_HEAD;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_63_32()
{
	return dbg_debug_head_frame1_63_32.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_31_0_tx_length_burst()
{
	return dbg_debug_head_frame1_31_0.tx_length_burst;
}
ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_31_0_sys_config_id()
{
	return dbg_debug_head_frame1_31_0.sys_config_id;
}

ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame1_31_0()
{
	return dbg_debug_head_frame1_31_0.payload;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame2_63_32()
{
	return dbg_debug_head_frame2_63_32;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_debug_head_frame2_31_0()
{
	return dbg_debug_head_frame2_31_0;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdebug_frame_cnt()
{
	return debug_frame_cnt;
}


ZynqFcuRd_reg ZynqFcuRdMemProxy::getdbg_frame_sync_test()
{
	return dbg_frame_sync_test;
}



void ZynqFcuRdMem::_loadOtp()
{
	VERSION=0x0;
	comm_test=0x0;
	trace_wddr3_done.payload=0x0;
		trace_wddr3_done.trace_done=0x0;

	fpga_rdat_done=0x0;

	deinterweave_done.payload=0x0;
		deinterweave_done.deinter_done=0x0;

	reserved=0x0;
	roll_ana_trace_waddr_latch=0x0;
	roll_la_trace_waddr_latch=0x0;

	roll_full_screen_wdone_flag.payload=0x0;
		roll_full_screen_wdone_flag.roll_full_screen_wdone_flag=0x0;

	search_result_wdone.payload=0x0;
		search_result_wdone.search_result_current_col=0x0;
		search_result_wdone.search_result_wdone=0x0;

	frame_id.payload=0x0;
		frame_id.frame_all_id=0x0;
		frame_id.frame_trace_id=0x0;
		frame_id.frame_all_id_expand=0x0;
		frame_id.frame_trace_id_expand=0x0;

	axi_rd_overtime_flag.payload=0x0;
		axi_rd_overtime_flag.axi_rd_overtime_flag=0x0;


	dbg_rd_reg_1.payload=0x0;
		dbg_rd_reg_1.trace_frame_cnt=0x0;
		dbg_rd_reg_1.trace_req_state=0x0;
		dbg_rd_reg_1.trace_dat_send_en=0x0;
		dbg_rd_reg_1.trace_dat_last_mask=0x0;

		dbg_rd_reg_1.trace_req_dtype_latch=0x0;
		dbg_rd_reg_1.current_dat_type=0x0;
		dbg_rd_reg_1.export_trace_flag=0x0;
		dbg_rd_reg_1.gtp_src_cnt=0x0;

	dbg_rd_reg_2=0x0;
	dbg_rd_reg_3=0x0;
	dbg_rd_reg_4=0x0;

	dbg_rd_reg_5=0x0;
	dbg_cfg2=0x0;
	dbg_main_ana_trace_num=0x0;
	dbg_zoom_ana_num=0x0;

	dbg_debug_head_frame1_63_32.payload=0x0;
		dbg_debug_head_frame1_63_32.frm_data_type=0x0;
		dbg_debug_head_frame1_63_32.sys_proc_la=0x0;
		dbg_debug_head_frame1_63_32.frm_plot_type=0x0;
		dbg_debug_head_frame1_63_32.sys_zoom=0x0;

		dbg_debug_head_frame1_63_32.sys_eye=0x0;
		dbg_debug_head_frame1_63_32.frm_obj_wpu=0x0;
		dbg_debug_head_frame1_63_32.frm_obj_trace=0x0;
		dbg_debug_head_frame1_63_32.FRM_HEAD=0x0;

	dbg_debug_head_frame1_31_0.payload=0x0;
		dbg_debug_head_frame1_31_0.tx_length_burst=0x0;
		dbg_debug_head_frame1_31_0.sys_config_id=0x0;

	dbg_debug_head_frame2_63_32=0x0;
	dbg_debug_head_frame2_31_0=0x0;

	debug_frame_cnt=0x0;
	dbg_frame_sync_test=0x0;
}

void ZynqFcuRdMem::_outOtp()
{




	outdbg_cfg2();


}
void ZynqFcuRdMem::push( ZynqFcuRdMemProxy *proxy )
{
	if ( dbg_cfg2 != proxy->dbg_cfg2 )
	{reg_assign(dbg_cfg2,proxy->dbg_cfg2);}

}
void ZynqFcuRdMem::pull( ZynqFcuRdMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(VERSION);
	reg_in(comm_test);
	reg_in(trace_wddr3_done);
	reg_in(fpga_rdat_done);
	reg_in(deinterweave_done);
	reg_in(reserved);
	reg_in(roll_ana_trace_waddr_latch);
	reg_in(roll_la_trace_waddr_latch);
	reg_in(roll_full_screen_wdone_flag);
	reg_in(search_result_wdone);
	reg_in(frame_id);
	reg_in(axi_rd_overtime_flag);
	reg_in(dbg_rd_reg_1);
	reg_in(dbg_rd_reg_2);
	reg_in(dbg_rd_reg_3);
	reg_in(dbg_rd_reg_4);
	reg_in(dbg_rd_reg_5);
	reg_in(dbg_main_ana_trace_num);
	reg_in(dbg_zoom_ana_num);
	reg_in(dbg_debug_head_frame1_63_32);
	reg_in(dbg_debug_head_frame1_31_0);
	reg_in(dbg_debug_head_frame2_63_32);
	reg_in(dbg_debug_head_frame2_31_0);
	reg_in(debug_frame_cnt);
	reg_in(dbg_frame_sync_test);

	*proxy = ZynqFcuRdMemProxy(*this);
}
ZynqFcuRd_reg ZynqFcuRdMem::inVERSION()
{
	reg_in(VERSION);
	return VERSION;
}


ZynqFcuRd_reg ZynqFcuRdMem::incomm_test()
{
	reg_in(comm_test);
	return comm_test;
}


ZynqFcuRd_reg ZynqFcuRdMem::intrace_wddr3_done()
{
	reg_in(trace_wddr3_done);
	return trace_wddr3_done.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::infpga_rdat_done()
{
	reg_in(fpga_rdat_done);
	return fpga_rdat_done;
}


ZynqFcuRd_reg ZynqFcuRdMem::indeinterweave_done()
{
	reg_in(deinterweave_done);
	return deinterweave_done.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::inreserved()
{
	reg_in(reserved);
	return reserved;
}


ZynqFcuRd_reg ZynqFcuRdMem::inroll_ana_trace_waddr_latch()
{
	reg_in(roll_ana_trace_waddr_latch);
	return roll_ana_trace_waddr_latch;
}


ZynqFcuRd_reg ZynqFcuRdMem::inroll_la_trace_waddr_latch()
{
	reg_in(roll_la_trace_waddr_latch);
	return roll_la_trace_waddr_latch;
}


ZynqFcuRd_reg ZynqFcuRdMem::inroll_full_screen_wdone_flag()
{
	reg_in(roll_full_screen_wdone_flag);
	return roll_full_screen_wdone_flag.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::insearch_result_wdone()
{
	reg_in(search_result_wdone);
	return search_result_wdone.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::inframe_id()
{
	reg_in(frame_id);
	return frame_id.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::inaxi_rd_overtime_flag()
{
	reg_in(axi_rd_overtime_flag);
	return axi_rd_overtime_flag.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_rd_reg_1()
{
	reg_in(dbg_rd_reg_1);
	return dbg_rd_reg_1.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_rd_reg_2()
{
	reg_in(dbg_rd_reg_2);
	return dbg_rd_reg_2;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_rd_reg_3()
{
	reg_in(dbg_rd_reg_3);
	return dbg_rd_reg_3;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_rd_reg_4()
{
	reg_in(dbg_rd_reg_4);
	return dbg_rd_reg_4;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_rd_reg_5()
{
	reg_in(dbg_rd_reg_5);
	return dbg_rd_reg_5;
}


void ZynqFcuRdMem::setdbg_cfg2( ZynqFcuRd_reg value )
{
	reg_assign(dbg_cfg2,value);
}

void ZynqFcuRdMem::outdbg_cfg2( ZynqFcuRd_reg value )
{
	out_assign(dbg_cfg2,value);
}

void ZynqFcuRdMem::outdbg_cfg2()
{
	out_member(dbg_cfg2);
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_main_ana_trace_num()
{
	reg_in(dbg_main_ana_trace_num);
	return dbg_main_ana_trace_num;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_zoom_ana_num()
{
	reg_in(dbg_zoom_ana_num);
	return dbg_zoom_ana_num;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_debug_head_frame1_63_32()
{
	reg_in(dbg_debug_head_frame1_63_32);
	return dbg_debug_head_frame1_63_32.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_debug_head_frame1_31_0()
{
	reg_in(dbg_debug_head_frame1_31_0);
	return dbg_debug_head_frame1_31_0.payload;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_debug_head_frame2_63_32()
{
	reg_in(dbg_debug_head_frame2_63_32);
	return dbg_debug_head_frame2_63_32;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_debug_head_frame2_31_0()
{
	reg_in(dbg_debug_head_frame2_31_0);
	return dbg_debug_head_frame2_31_0;
}


ZynqFcuRd_reg ZynqFcuRdMem::indebug_frame_cnt()
{
	reg_in(debug_frame_cnt);
	return debug_frame_cnt;
}


ZynqFcuRd_reg ZynqFcuRdMem::indbg_frame_sync_test()
{
	reg_in(dbg_frame_sync_test);
	return dbg_frame_sync_test;
}


cache_addr ZynqFcuRdMem::addr_VERSION(){ return 0x0800+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_comm_test(){ return 0x0804+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_trace_wddr3_done(){ return 0x0808+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_fpga_rdat_done(){ return 0x080C+mAddrBase; }

cache_addr ZynqFcuRdMem::addr_deinterweave_done(){ return 0x0810+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_reserved(){ return 0x0814+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_roll_ana_trace_waddr_latch(){ return 0x0014+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_roll_la_trace_waddr_latch(){ return 0x0018+mAddrBase; }

cache_addr ZynqFcuRdMem::addr_roll_full_screen_wdone_flag(){ return 0x001c+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_search_result_wdone(){ return 0x0020+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_frame_id(){ return 0x0024+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_axi_rd_overtime_flag(){ return 0x0c04+mAddrBase; }

cache_addr ZynqFcuRdMem::addr_dbg_rd_reg_1(){ return 0x0074+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_rd_reg_2(){ return 0x0078+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_rd_reg_3(){ return 0x007c+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_rd_reg_4(){ return 0x0838+mAddrBase; }

cache_addr ZynqFcuRdMem::addr_dbg_rd_reg_5(){ return 0x083c+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_cfg2(){ return 0x08fc+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_main_ana_trace_num(){ return 0x0048+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_zoom_ana_num(){ return 0x0044+mAddrBase; }

cache_addr ZynqFcuRdMem::addr_dbg_debug_head_frame1_63_32(){ return 0x0084+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_debug_head_frame1_31_0(){ return 0x0088+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_debug_head_frame2_63_32(){ return 0x008c+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_debug_head_frame2_31_0(){ return 0x0090+mAddrBase; }

cache_addr ZynqFcuRdMem::addr_debug_frame_cnt(){ return 0x0094+mAddrBase; }
cache_addr ZynqFcuRdMem::addr_dbg_frame_sync_test(){ return 0x0874+mAddrBase; }


