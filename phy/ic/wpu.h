#ifndef _WPU_IC_H_
#define _WPU_IC_H_

#define Wpu_reg unsigned int

union Wpu_analog_mode{
	struct{
	Wpu_reg analog_ch_mode:4;
	Wpu_reg analog_ch_en:4;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch0_noise{
	struct{
	Wpu_reg ch0_noise_grade:2;
	Wpu_reg ch0_zoom_noise_grade:2;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch1_noise{
	struct{
	Wpu_reg ch1_noise_grade:2;
	Wpu_reg ch1_zoom_noise_grade:2;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch2_noise{
	struct{
	Wpu_reg ch2_noise_grade:2;
	Wpu_reg ch2_zoom_noise_grade:2;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch3_noise{
	struct{
	Wpu_reg ch3_noise_grade:2;
	Wpu_reg ch3_zoom_noise_grade:2;
	};
	Wpu_reg payload;
};

union Wpu_view_layer{
	struct{
	Wpu_reg group_order:6;
	Wpu_reg _pad0:2;
	Wpu_reg analog_order:8;
	Wpu_reg math_order:8;
	};
	Wpu_reg payload;
};

union Wpu_init_la_plot_table{
	struct{
	Wpu_reg la_plot_tbl_wen:1;
	Wpu_reg la_plot_tbl_waddr:9;
	};
	Wpu_reg payload;
};

union Wpu_la_plot_tbl_data{
	struct{
	Wpu_reg COLOR:24;
	Wpu_reg POSITION:2;
	Wpu_reg reserved:1;
	Wpu_reg zoom_chn_flag:1;

	Wpu_reg CH_NO:4;
	};
	Wpu_reg payload;
};

union Wpu_xy0_en{
	struct{
	Wpu_reg en:1;
	Wpu_reg column_start:10;
	};
	Wpu_reg payload;
};

union Wpu_xy1_en{
	struct{
	Wpu_reg en:1;
	Wpu_reg column_start:10;
	};
	Wpu_reg payload;
};

union Wpu_xy2_en{
	struct{
	Wpu_reg en:1;
	Wpu_reg column_start:10;
	};
	Wpu_reg payload;
};

union Wpu_xy3_en{
	struct{
	Wpu_reg en:1;
	Wpu_reg column_start:10;
	};
	Wpu_reg payload;
};

union Wpu_init_palette_table{
	struct{
	Wpu_reg palette_tbl_wen:1;
	Wpu_reg palette_tbl_waddr:8;
	Wpu_reg palette_tbl_ch:3;
	};
	Wpu_reg payload;
};

union Wpu_scale_times{
	struct{
	Wpu_reg scale_times_m:12;
	Wpu_reg scale_times_n:12;
	Wpu_reg scale_times_en:1;
	};
	Wpu_reg payload;
};

union Wpu_max_out{
	struct{
	Wpu_reg main_max_out:12;
	Wpu_reg zoom_max_out:12;
	};
	Wpu_reg payload;
};

union Wpu_view_bottom{
	struct{
	Wpu_reg main_view_bottom:12;
	Wpu_reg zoom_view_bottom:12;
	};
	Wpu_reg payload;
};

union Wpu_zoom_en{
	struct{
	Wpu_reg en:1;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch0_zoom_gain{
	struct{
	Wpu_reg analog_ch0_zoom_gain:27;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch1_zoom_gain{
	struct{
	Wpu_reg analog_ch1_zoom_gain:27;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch2_zoom_gain{
	struct{
	Wpu_reg analog_ch2_zoom_gain:27;
	};
	Wpu_reg payload;
};

union Wpu_analog_ch3_zoom_gain{
	struct{
	Wpu_reg analog_ch3_zoom_gain:27;
	};
	Wpu_reg payload;
};

union Wpu_debug_info_pm{
	struct{
	Wpu_reg plot_cnt:10;
	Wpu_reg local_plot_len:10;
	Wpu_reg plot_phase:4;
	Wpu_reg _pad0:1;
	Wpu_reg zone_buf_empty_n:1;

	Wpu_reg pe_in_rdy:1;
	Wpu_reg pe_out_rdy:1;
	Wpu_reg plot_out_rdy:1;
	};
	Wpu_reg payload;
};

union Wpu_zoom_scale_times{
	struct{
	Wpu_reg zoom_scale_times_m:12;
	Wpu_reg zoom_scale_times_n:12;
	Wpu_reg zoom_scale_times_en:1;
	};
	Wpu_reg payload;
};

union Wpu_scan_trig_cnt{
	struct{
	Wpu_reg scan_trig_col:12;
	Wpu_reg scan_trig_addr:20;
	};
	Wpu_reg payload;
};

union Wpu_roll_lenth{
	struct{
	Wpu_reg roll_lenth_cnt:10;
	Wpu_reg roll_full:1;
	};
	Wpu_reg payload;
};

struct WpuMemProxy {
	Wpu_reg version;
	Wpu_reg init_pe;
	Wpu_reg config_ID;
	Wpu_reg display_en;

	Wpu_analog_mode analog_mode; 
	Wpu_reg LA_en;
	Wpu_reg refresh_time;
	Wpu_reg persist_mode;

	Wpu_reg persist_init;
	Wpu_reg persist_dim_rate;
	Wpu_reg plot_mode;
	Wpu_reg slope_mode;

	Wpu_reg analog_ch0_gain;
	Wpu_analog_ch0_noise analog_ch0_noise; 
	Wpu_reg analog_ch0_offset;
	Wpu_reg analog_ch1_gain;

	Wpu_analog_ch1_noise analog_ch1_noise; 
	Wpu_reg analog_ch1_offset;
	Wpu_reg analog_ch2_gain;
	Wpu_analog_ch2_noise analog_ch2_noise; 

	Wpu_reg analog_ch2_offset;
	Wpu_reg analog_ch3_gain;
	Wpu_analog_ch3_noise analog_ch3_noise; 
	Wpu_reg analog_ch3_offset;

	Wpu_reg color_map;
	Wpu_view_layer view_layer; 
	Wpu_reg trace_layer_addr_0;
	Wpu_reg trace_layer_addr_1;

	Wpu_reg wpu_clear;
	Wpu_reg srn_num;
	Wpu_reg wpu_status;
	Wpu_reg mem_status;

	Wpu_reg init_status;
	Wpu_reg load_config_id;
	Wpu_init_la_plot_table init_la_plot_table; 
	Wpu_la_plot_tbl_data la_plot_tbl_data; 

	Wpu_reg wave_view_start;
	Wpu_reg wave_view_length;
	Wpu_reg zoom_view_start;
	Wpu_reg zoom_view_length;

	Wpu_xy0_en xy0_en; 
	Wpu_xy1_en xy1_en; 
	Wpu_xy2_en xy2_en; 
	Wpu_xy3_en xy3_en; 

	Wpu_init_palette_table init_palette_table; 
	Wpu_reg palette_tbl_wdata;
	Wpu_reg data_mode;
	Wpu_scale_times scale_times; 

	Wpu_reg scale_length;
	Wpu_reg analog_ch0_zoom_offset;
	Wpu_reg analog_ch1_zoom_offset;
	Wpu_reg analog_ch2_zoom_offset;

	Wpu_reg analog_ch3_zoom_offset;
	Wpu_max_out max_out; 
	Wpu_view_bottom view_bottom; 
	Wpu_zoom_en zoom_en; 

	Wpu_analog_ch0_zoom_gain analog_ch0_zoom_gain; 
	Wpu_analog_ch1_zoom_gain analog_ch1_zoom_gain; 
	Wpu_analog_ch2_zoom_gain analog_ch2_zoom_gain; 
	Wpu_analog_ch3_zoom_gain analog_ch3_zoom_gain; 

	Wpu_debug_info_pm debug_info_pm; 
	Wpu_zoom_scale_times zoom_scale_times; 
	Wpu_reg zoom_scale_length;
	Wpu_reg time_base_mode;

	Wpu_scan_trig_cnt scan_trig_cnt; 
	Wpu_roll_lenth roll_lenth; 
	Wpu_reg getversion(  );

	void assigninit_pe( Wpu_reg value );

	void assignconfig_ID( Wpu_reg value );

	Wpu_reg getconfig_ID(  );

	void assigndisplay_en( Wpu_reg value );

	Wpu_reg getdisplay_en(  );

	void assignanalog_mode_analog_ch_mode( Wpu_reg value  );
	void assignanalog_mode_analog_ch_en( Wpu_reg value  );

	void assignanalog_mode( Wpu_reg value );


	Wpu_reg getanalog_mode_analog_ch_mode(  );
	Wpu_reg getanalog_mode_analog_ch_en(  );

	Wpu_reg getanalog_mode(  );

	void assignLA_en( Wpu_reg value );

	Wpu_reg getLA_en(  );

	void assignrefresh_time( Wpu_reg value );

	Wpu_reg getrefresh_time(  );

	void assignpersist_mode( Wpu_reg value );

	Wpu_reg getpersist_mode(  );

	void assignpersist_init( Wpu_reg value );

	Wpu_reg getpersist_init(  );

	void assignpersist_dim_rate( Wpu_reg value );

	Wpu_reg getpersist_dim_rate(  );

	void assignplot_mode( Wpu_reg value );

	Wpu_reg getplot_mode(  );

	void assignslope_mode( Wpu_reg value );

	Wpu_reg getslope_mode(  );

	void assignanalog_ch0_gain( Wpu_reg value );

	Wpu_reg getanalog_ch0_gain(  );

	void assignanalog_ch0_noise_ch0_noise_grade( Wpu_reg value  );
	void assignanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg value  );

	void assignanalog_ch0_noise( Wpu_reg value );


	Wpu_reg getanalog_ch0_noise_ch0_noise_grade(  );
	Wpu_reg getanalog_ch0_noise_ch0_zoom_noise_grade(  );

	Wpu_reg getanalog_ch0_noise(  );

	void assignanalog_ch0_offset( Wpu_reg value );

	Wpu_reg getanalog_ch0_offset(  );

	void assignanalog_ch1_gain( Wpu_reg value );

	Wpu_reg getanalog_ch1_gain(  );

	void assignanalog_ch1_noise_ch1_noise_grade( Wpu_reg value  );
	void assignanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg value  );

	void assignanalog_ch1_noise( Wpu_reg value );


	Wpu_reg getanalog_ch1_noise_ch1_noise_grade(  );
	Wpu_reg getanalog_ch1_noise_ch1_zoom_noise_grade(  );

	Wpu_reg getanalog_ch1_noise(  );

	void assignanalog_ch1_offset( Wpu_reg value );

	Wpu_reg getanalog_ch1_offset(  );

	void assignanalog_ch2_gain( Wpu_reg value );

	Wpu_reg getanalog_ch2_gain(  );

	void assignanalog_ch2_noise_ch2_noise_grade( Wpu_reg value  );
	void assignanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg value  );

	void assignanalog_ch2_noise( Wpu_reg value );


	Wpu_reg getanalog_ch2_noise_ch2_noise_grade(  );
	Wpu_reg getanalog_ch2_noise_ch2_zoom_noise_grade(  );

	Wpu_reg getanalog_ch2_noise(  );

	void assignanalog_ch2_offset( Wpu_reg value );

	Wpu_reg getanalog_ch2_offset(  );

	void assignanalog_ch3_gain( Wpu_reg value );

	Wpu_reg getanalog_ch3_gain(  );

	void assignanalog_ch3_noise_ch3_noise_grade( Wpu_reg value  );
	void assignanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg value  );

	void assignanalog_ch3_noise( Wpu_reg value );


	Wpu_reg getanalog_ch3_noise_ch3_noise_grade(  );
	Wpu_reg getanalog_ch3_noise_ch3_zoom_noise_grade(  );

	Wpu_reg getanalog_ch3_noise(  );

	void assignanalog_ch3_offset( Wpu_reg value );

	Wpu_reg getanalog_ch3_offset(  );

	void assigncolor_map( Wpu_reg value );

	Wpu_reg getcolor_map(  );

	void assignview_layer_group_order( Wpu_reg value  );
	void assignview_layer_analog_order( Wpu_reg value  );
	void assignview_layer_math_order( Wpu_reg value  );

	void assignview_layer( Wpu_reg value );


	Wpu_reg getview_layer_group_order(  );
	Wpu_reg getview_layer_analog_order(  );
	Wpu_reg getview_layer_math_order(  );

	Wpu_reg getview_layer(  );

	void assigntrace_layer_addr_0( Wpu_reg value );

	Wpu_reg gettrace_layer_addr_0(  );

	void assigntrace_layer_addr_1( Wpu_reg value );

	Wpu_reg gettrace_layer_addr_1(  );

	void assignwpu_clear( Wpu_reg value );

	Wpu_reg getsrn_num(  );

	Wpu_reg getwpu_status(  );

	Wpu_reg getmem_status(  );

	Wpu_reg getinit_status(  );

	Wpu_reg getload_config_id(  );

	void assigninit_la_plot_table_la_plot_tbl_wen( Wpu_reg value  );
	void assigninit_la_plot_table_la_plot_tbl_waddr( Wpu_reg value  );

	void assigninit_la_plot_table( Wpu_reg value );


	Wpu_reg getinit_la_plot_table_la_plot_tbl_wen(  );
	Wpu_reg getinit_la_plot_table_la_plot_tbl_waddr(  );

	Wpu_reg getinit_la_plot_table(  );

	void assignla_plot_tbl_data_COLOR( Wpu_reg value  );
	void assignla_plot_tbl_data_POSITION( Wpu_reg value  );
	void assignla_plot_tbl_data_reserved( Wpu_reg value  );
	void assignla_plot_tbl_data_zoom_chn_flag( Wpu_reg value  );
	void assignla_plot_tbl_data_CH_NO( Wpu_reg value  );

	void assignla_plot_tbl_data( Wpu_reg value );


	Wpu_reg getla_plot_tbl_data_COLOR(  );
	Wpu_reg getla_plot_tbl_data_POSITION(  );
	Wpu_reg getla_plot_tbl_data_reserved(  );
	Wpu_reg getla_plot_tbl_data_zoom_chn_flag(  );
	Wpu_reg getla_plot_tbl_data_CH_NO(  );

	Wpu_reg getla_plot_tbl_data(  );

	void assignwave_view_start( Wpu_reg value );

	Wpu_reg getwave_view_start(  );

	void assignwave_view_length( Wpu_reg value );

	Wpu_reg getwave_view_length(  );

	void assignzoom_view_start( Wpu_reg value );

	Wpu_reg getzoom_view_start(  );

	void assignzoom_view_length( Wpu_reg value );

	Wpu_reg getzoom_view_length(  );

	void assignxy0_en_en( Wpu_reg value  );
	void assignxy0_en_column_start( Wpu_reg value  );

	void assignxy0_en( Wpu_reg value );


	Wpu_reg getxy0_en_en(  );
	Wpu_reg getxy0_en_column_start(  );

	Wpu_reg getxy0_en(  );

	void assignxy1_en_en( Wpu_reg value  );
	void assignxy1_en_column_start( Wpu_reg value  );

	void assignxy1_en( Wpu_reg value );


	Wpu_reg getxy1_en_en(  );
	Wpu_reg getxy1_en_column_start(  );

	Wpu_reg getxy1_en(  );

	void assignxy2_en_en( Wpu_reg value  );
	void assignxy2_en_column_start( Wpu_reg value  );

	void assignxy2_en( Wpu_reg value );


	Wpu_reg getxy2_en_en(  );
	Wpu_reg getxy2_en_column_start(  );

	Wpu_reg getxy2_en(  );

	void assignxy3_en_en( Wpu_reg value  );
	void assignxy3_en_column_start( Wpu_reg value  );

	void assignxy3_en( Wpu_reg value );


	Wpu_reg getxy3_en_en(  );
	Wpu_reg getxy3_en_column_start(  );

	Wpu_reg getxy3_en(  );

	void assigninit_palette_table_palette_tbl_wen( Wpu_reg value  );
	void assigninit_palette_table_palette_tbl_waddr( Wpu_reg value  );
	void assigninit_palette_table_palette_tbl_ch( Wpu_reg value  );

	void assigninit_palette_table( Wpu_reg value );


	void assignpalette_tbl_wdata( Wpu_reg value );

	Wpu_reg getpalette_tbl_wdata(  );

	void assigndata_mode( Wpu_reg value );

	Wpu_reg getdata_mode(  );

	void assignscale_times_scale_times_m( Wpu_reg value  );
	void assignscale_times_scale_times_n( Wpu_reg value  );
	void assignscale_times_scale_times_en( Wpu_reg value  );

	void assignscale_times( Wpu_reg value );


	Wpu_reg getscale_times_scale_times_m(  );
	Wpu_reg getscale_times_scale_times_n(  );
	Wpu_reg getscale_times_scale_times_en(  );

	Wpu_reg getscale_times(  );

	void assignscale_length( Wpu_reg value );

	Wpu_reg getscale_length(  );

	void assignanalog_ch0_zoom_offset( Wpu_reg value );

	Wpu_reg getanalog_ch0_zoom_offset(  );

	void assignanalog_ch1_zoom_offset( Wpu_reg value );

	Wpu_reg getanalog_ch1_zoom_offset(  );

	void assignanalog_ch2_zoom_offset( Wpu_reg value );

	Wpu_reg getanalog_ch2_zoom_offset(  );

	void assignanalog_ch3_zoom_offset( Wpu_reg value );

	Wpu_reg getanalog_ch3_zoom_offset(  );

	void assignmax_out_main_max_out( Wpu_reg value  );
	void assignmax_out_zoom_max_out( Wpu_reg value  );

	void assignmax_out( Wpu_reg value );


	Wpu_reg getmax_out_main_max_out(  );
	Wpu_reg getmax_out_zoom_max_out(  );

	Wpu_reg getmax_out(  );

	void assignview_bottom_main_view_bottom( Wpu_reg value  );
	void assignview_bottom_zoom_view_bottom( Wpu_reg value  );

	void assignview_bottom( Wpu_reg value );


	Wpu_reg getview_bottom_main_view_bottom(  );
	Wpu_reg getview_bottom_zoom_view_bottom(  );

	Wpu_reg getview_bottom(  );

	void assignzoom_en_en( Wpu_reg value  );

	void assignzoom_en( Wpu_reg value );


	Wpu_reg getzoom_en_en(  );

	Wpu_reg getzoom_en(  );

	void assignanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg value  );

	void assignanalog_ch0_zoom_gain( Wpu_reg value );


	Wpu_reg getanalog_ch0_zoom_gain_analog_ch0_zoom_gain(  );

	Wpu_reg getanalog_ch0_zoom_gain(  );

	void assignanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg value  );

	void assignanalog_ch1_zoom_gain( Wpu_reg value );


	Wpu_reg getanalog_ch1_zoom_gain_analog_ch1_zoom_gain(  );

	Wpu_reg getanalog_ch1_zoom_gain(  );

	void assignanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg value  );

	void assignanalog_ch2_zoom_gain( Wpu_reg value );


	Wpu_reg getanalog_ch2_zoom_gain_analog_ch2_zoom_gain(  );

	Wpu_reg getanalog_ch2_zoom_gain(  );

	void assignanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg value  );

	void assignanalog_ch3_zoom_gain( Wpu_reg value );


	Wpu_reg getanalog_ch3_zoom_gain_analog_ch3_zoom_gain(  );

	Wpu_reg getanalog_ch3_zoom_gain(  );

	Wpu_reg getdebug_info_pm_plot_cnt(  );
	Wpu_reg getdebug_info_pm_local_plot_len(  );
	Wpu_reg getdebug_info_pm_plot_phase(  );
	Wpu_reg getdebug_info_pm_zone_buf_empty_n(  );
	Wpu_reg getdebug_info_pm_pe_in_rdy(  );
	Wpu_reg getdebug_info_pm_pe_out_rdy(  );
	Wpu_reg getdebug_info_pm_plot_out_rdy(  );

	Wpu_reg getdebug_info_pm(  );

	void assignzoom_scale_times_zoom_scale_times_m( Wpu_reg value  );
	void assignzoom_scale_times_zoom_scale_times_n( Wpu_reg value  );
	void assignzoom_scale_times_zoom_scale_times_en( Wpu_reg value  );

	void assignzoom_scale_times( Wpu_reg value );


	Wpu_reg getzoom_scale_times_zoom_scale_times_m(  );
	Wpu_reg getzoom_scale_times_zoom_scale_times_n(  );
	Wpu_reg getzoom_scale_times_zoom_scale_times_en(  );

	Wpu_reg getzoom_scale_times(  );

	void assignzoom_scale_length( Wpu_reg value );

	Wpu_reg getzoom_scale_length(  );

	void assigntime_base_mode( Wpu_reg value );

	Wpu_reg gettime_base_mode(  );

	void assignscan_trig_cnt_scan_trig_col( Wpu_reg value  );
	void assignscan_trig_cnt_scan_trig_addr( Wpu_reg value  );

	void assignscan_trig_cnt( Wpu_reg value );


	Wpu_reg getscan_trig_cnt_scan_trig_col(  );
	Wpu_reg getscan_trig_cnt_scan_trig_addr(  );

	Wpu_reg getscan_trig_cnt(  );

	Wpu_reg getroll_lenth_roll_lenth_cnt(  );
	Wpu_reg getroll_lenth_roll_full(  );

	Wpu_reg getroll_lenth(  );

};
struct WpuMem : public WpuMemProxy, public IPhyFpga{
protected:
	Wpu_reg mAddrBase;
public:
	WpuMem( Wpu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( Wpu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( WpuMemProxy *proxy);
	void pull( WpuMemProxy *proxy);

public:
	Wpu_reg inversion(  );

	void setinit_pe( Wpu_reg value );
	void outinit_pe( Wpu_reg value );
	void outinit_pe(  );

	void setconfig_ID( Wpu_reg value );
	void outconfig_ID( Wpu_reg value );
	void outconfig_ID(  );

	Wpu_reg inconfig_ID(  );

	void setdisplay_en( Wpu_reg value );
	void outdisplay_en( Wpu_reg value );
	void outdisplay_en(  );

	Wpu_reg indisplay_en(  );

	void setanalog_mode_analog_ch_mode( Wpu_reg value );
	void setanalog_mode_analog_ch_en( Wpu_reg value );

	void setanalog_mode( Wpu_reg value );
	void outanalog_mode_analog_ch_mode( Wpu_reg value );
	void pulseanalog_mode_analog_ch_mode( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outanalog_mode_analog_ch_en( Wpu_reg value );
	void pulseanalog_mode_analog_ch_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_mode( Wpu_reg value );
	void outanalog_mode(  );


	Wpu_reg inanalog_mode(  );

	void setLA_en( Wpu_reg value );
	void outLA_en( Wpu_reg value );
	void outLA_en(  );

	Wpu_reg inLA_en(  );

	void setrefresh_time( Wpu_reg value );
	void outrefresh_time( Wpu_reg value );
	void outrefresh_time(  );

	Wpu_reg inrefresh_time(  );

	void setpersist_mode( Wpu_reg value );
	void outpersist_mode( Wpu_reg value );
	void outpersist_mode(  );

	Wpu_reg inpersist_mode(  );

	void setpersist_init( Wpu_reg value );
	void outpersist_init( Wpu_reg value );
	void outpersist_init(  );

	Wpu_reg inpersist_init(  );

	void setpersist_dim_rate( Wpu_reg value );
	void outpersist_dim_rate( Wpu_reg value );
	void outpersist_dim_rate(  );

	Wpu_reg inpersist_dim_rate(  );

	void setplot_mode( Wpu_reg value );
	void outplot_mode( Wpu_reg value );
	void outplot_mode(  );

	Wpu_reg inplot_mode(  );

	void setslope_mode( Wpu_reg value );
	void outslope_mode( Wpu_reg value );
	void outslope_mode(  );

	Wpu_reg inslope_mode(  );

	void setanalog_ch0_gain( Wpu_reg value );
	void outanalog_ch0_gain( Wpu_reg value );
	void outanalog_ch0_gain(  );

	Wpu_reg inanalog_ch0_gain(  );

	void setanalog_ch0_noise_ch0_noise_grade( Wpu_reg value );
	void setanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg value );

	void setanalog_ch0_noise( Wpu_reg value );
	void outanalog_ch0_noise_ch0_noise_grade( Wpu_reg value );
	void pulseanalog_ch0_noise_ch0_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg value );
	void pulseanalog_ch0_noise_ch0_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch0_noise( Wpu_reg value );
	void outanalog_ch0_noise(  );


	Wpu_reg inanalog_ch0_noise(  );

	void setanalog_ch0_offset( Wpu_reg value );
	void outanalog_ch0_offset( Wpu_reg value );
	void outanalog_ch0_offset(  );

	Wpu_reg inanalog_ch0_offset(  );

	void setanalog_ch1_gain( Wpu_reg value );
	void outanalog_ch1_gain( Wpu_reg value );
	void outanalog_ch1_gain(  );

	Wpu_reg inanalog_ch1_gain(  );

	void setanalog_ch1_noise_ch1_noise_grade( Wpu_reg value );
	void setanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg value );

	void setanalog_ch1_noise( Wpu_reg value );
	void outanalog_ch1_noise_ch1_noise_grade( Wpu_reg value );
	void pulseanalog_ch1_noise_ch1_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg value );
	void pulseanalog_ch1_noise_ch1_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch1_noise( Wpu_reg value );
	void outanalog_ch1_noise(  );


	Wpu_reg inanalog_ch1_noise(  );

	void setanalog_ch1_offset( Wpu_reg value );
	void outanalog_ch1_offset( Wpu_reg value );
	void outanalog_ch1_offset(  );

	Wpu_reg inanalog_ch1_offset(  );

	void setanalog_ch2_gain( Wpu_reg value );
	void outanalog_ch2_gain( Wpu_reg value );
	void outanalog_ch2_gain(  );

	Wpu_reg inanalog_ch2_gain(  );

	void setanalog_ch2_noise_ch2_noise_grade( Wpu_reg value );
	void setanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg value );

	void setanalog_ch2_noise( Wpu_reg value );
	void outanalog_ch2_noise_ch2_noise_grade( Wpu_reg value );
	void pulseanalog_ch2_noise_ch2_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg value );
	void pulseanalog_ch2_noise_ch2_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch2_noise( Wpu_reg value );
	void outanalog_ch2_noise(  );


	Wpu_reg inanalog_ch2_noise(  );

	void setanalog_ch2_offset( Wpu_reg value );
	void outanalog_ch2_offset( Wpu_reg value );
	void outanalog_ch2_offset(  );

	Wpu_reg inanalog_ch2_offset(  );

	void setanalog_ch3_gain( Wpu_reg value );
	void outanalog_ch3_gain( Wpu_reg value );
	void outanalog_ch3_gain(  );

	Wpu_reg inanalog_ch3_gain(  );

	void setanalog_ch3_noise_ch3_noise_grade( Wpu_reg value );
	void setanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg value );

	void setanalog_ch3_noise( Wpu_reg value );
	void outanalog_ch3_noise_ch3_noise_grade( Wpu_reg value );
	void pulseanalog_ch3_noise_ch3_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg value );
	void pulseanalog_ch3_noise_ch3_zoom_noise_grade( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch3_noise( Wpu_reg value );
	void outanalog_ch3_noise(  );


	Wpu_reg inanalog_ch3_noise(  );

	void setanalog_ch3_offset( Wpu_reg value );
	void outanalog_ch3_offset( Wpu_reg value );
	void outanalog_ch3_offset(  );

	Wpu_reg inanalog_ch3_offset(  );

	void setcolor_map( Wpu_reg value );
	void outcolor_map( Wpu_reg value );
	void outcolor_map(  );

	Wpu_reg incolor_map(  );

	void setview_layer_group_order( Wpu_reg value );
	void setview_layer_analog_order( Wpu_reg value );
	void setview_layer_math_order( Wpu_reg value );

	void setview_layer( Wpu_reg value );
	void outview_layer_group_order( Wpu_reg value );
	void pulseview_layer_group_order( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outview_layer_analog_order( Wpu_reg value );
	void pulseview_layer_analog_order( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outview_layer_math_order( Wpu_reg value );
	void pulseview_layer_math_order( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outview_layer( Wpu_reg value );
	void outview_layer(  );


	Wpu_reg inview_layer(  );

	void settrace_layer_addr_0( Wpu_reg value );
	void outtrace_layer_addr_0( Wpu_reg value );
	void outtrace_layer_addr_0(  );

	Wpu_reg intrace_layer_addr_0(  );

	void settrace_layer_addr_1( Wpu_reg value );
	void outtrace_layer_addr_1( Wpu_reg value );
	void outtrace_layer_addr_1(  );

	Wpu_reg intrace_layer_addr_1(  );

	void setwpu_clear( Wpu_reg value );
	void outwpu_clear( Wpu_reg value );
	void outwpu_clear(  );

	Wpu_reg insrn_num(  );

	Wpu_reg inwpu_status(  );

	Wpu_reg inmem_status(  );

	Wpu_reg ininit_status(  );

	Wpu_reg inload_config_id(  );

	void setinit_la_plot_table_la_plot_tbl_wen( Wpu_reg value );
	void setinit_la_plot_table_la_plot_tbl_waddr( Wpu_reg value );

	void setinit_la_plot_table( Wpu_reg value );
	void outinit_la_plot_table_la_plot_tbl_wen( Wpu_reg value );
	void pulseinit_la_plot_table_la_plot_tbl_wen( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outinit_la_plot_table_la_plot_tbl_waddr( Wpu_reg value );
	void pulseinit_la_plot_table_la_plot_tbl_waddr( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outinit_la_plot_table( Wpu_reg value );
	void outinit_la_plot_table(  );


	Wpu_reg ininit_la_plot_table(  );

	void setla_plot_tbl_data_COLOR( Wpu_reg value );
	void setla_plot_tbl_data_POSITION( Wpu_reg value );
	void setla_plot_tbl_data_reserved( Wpu_reg value );
	void setla_plot_tbl_data_zoom_chn_flag( Wpu_reg value );
	void setla_plot_tbl_data_CH_NO( Wpu_reg value );

	void setla_plot_tbl_data( Wpu_reg value );
	void outla_plot_tbl_data_COLOR( Wpu_reg value );
	void pulsela_plot_tbl_data_COLOR( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outla_plot_tbl_data_POSITION( Wpu_reg value );
	void pulsela_plot_tbl_data_POSITION( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outla_plot_tbl_data_reserved( Wpu_reg value );
	void pulsela_plot_tbl_data_reserved( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outla_plot_tbl_data_zoom_chn_flag( Wpu_reg value );
	void pulsela_plot_tbl_data_zoom_chn_flag( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outla_plot_tbl_data_CH_NO( Wpu_reg value );
	void pulsela_plot_tbl_data_CH_NO( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outla_plot_tbl_data( Wpu_reg value );
	void outla_plot_tbl_data(  );


	Wpu_reg inla_plot_tbl_data(  );

	void setwave_view_start( Wpu_reg value );
	void outwave_view_start( Wpu_reg value );
	void outwave_view_start(  );

	Wpu_reg inwave_view_start(  );

	void setwave_view_length( Wpu_reg value );
	void outwave_view_length( Wpu_reg value );
	void outwave_view_length(  );

	Wpu_reg inwave_view_length(  );

	void setzoom_view_start( Wpu_reg value );
	void outzoom_view_start( Wpu_reg value );
	void outzoom_view_start(  );

	Wpu_reg inzoom_view_start(  );

	void setzoom_view_length( Wpu_reg value );
	void outzoom_view_length( Wpu_reg value );
	void outzoom_view_length(  );

	Wpu_reg inzoom_view_length(  );

	void setxy0_en_en( Wpu_reg value );
	void setxy0_en_column_start( Wpu_reg value );

	void setxy0_en( Wpu_reg value );
	void outxy0_en_en( Wpu_reg value );
	void pulsexy0_en_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outxy0_en_column_start( Wpu_reg value );
	void pulsexy0_en_column_start( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outxy0_en( Wpu_reg value );
	void outxy0_en(  );


	Wpu_reg inxy0_en(  );

	void setxy1_en_en( Wpu_reg value );
	void setxy1_en_column_start( Wpu_reg value );

	void setxy1_en( Wpu_reg value );
	void outxy1_en_en( Wpu_reg value );
	void pulsexy1_en_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outxy1_en_column_start( Wpu_reg value );
	void pulsexy1_en_column_start( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outxy1_en( Wpu_reg value );
	void outxy1_en(  );


	Wpu_reg inxy1_en(  );

	void setxy2_en_en( Wpu_reg value );
	void setxy2_en_column_start( Wpu_reg value );

	void setxy2_en( Wpu_reg value );
	void outxy2_en_en( Wpu_reg value );
	void pulsexy2_en_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outxy2_en_column_start( Wpu_reg value );
	void pulsexy2_en_column_start( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outxy2_en( Wpu_reg value );
	void outxy2_en(  );


	Wpu_reg inxy2_en(  );

	void setxy3_en_en( Wpu_reg value );
	void setxy3_en_column_start( Wpu_reg value );

	void setxy3_en( Wpu_reg value );
	void outxy3_en_en( Wpu_reg value );
	void pulsexy3_en_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outxy3_en_column_start( Wpu_reg value );
	void pulsexy3_en_column_start( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outxy3_en( Wpu_reg value );
	void outxy3_en(  );


	Wpu_reg inxy3_en(  );

	void setinit_palette_table_palette_tbl_wen( Wpu_reg value );
	void setinit_palette_table_palette_tbl_waddr( Wpu_reg value );
	void setinit_palette_table_palette_tbl_ch( Wpu_reg value );

	void setinit_palette_table( Wpu_reg value );
	void outinit_palette_table_palette_tbl_wen( Wpu_reg value );
	void pulseinit_palette_table_palette_tbl_wen( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outinit_palette_table_palette_tbl_waddr( Wpu_reg value );
	void pulseinit_palette_table_palette_tbl_waddr( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outinit_palette_table_palette_tbl_ch( Wpu_reg value );
	void pulseinit_palette_table_palette_tbl_ch( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outinit_palette_table( Wpu_reg value );
	void outinit_palette_table(  );


	void setpalette_tbl_wdata( Wpu_reg value );
	void outpalette_tbl_wdata( Wpu_reg value );
	void outpalette_tbl_wdata(  );

	Wpu_reg inpalette_tbl_wdata(  );

	void setdata_mode( Wpu_reg value );
	void outdata_mode( Wpu_reg value );
	void outdata_mode(  );

	Wpu_reg indata_mode(  );

	void setscale_times_scale_times_m( Wpu_reg value );
	void setscale_times_scale_times_n( Wpu_reg value );
	void setscale_times_scale_times_en( Wpu_reg value );

	void setscale_times( Wpu_reg value );
	void outscale_times_scale_times_m( Wpu_reg value );
	void pulsescale_times_scale_times_m( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outscale_times_scale_times_n( Wpu_reg value );
	void pulsescale_times_scale_times_n( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outscale_times_scale_times_en( Wpu_reg value );
	void pulsescale_times_scale_times_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outscale_times( Wpu_reg value );
	void outscale_times(  );


	Wpu_reg inscale_times(  );

	void setscale_length( Wpu_reg value );
	void outscale_length( Wpu_reg value );
	void outscale_length(  );

	Wpu_reg inscale_length(  );

	void setanalog_ch0_zoom_offset( Wpu_reg value );
	void outanalog_ch0_zoom_offset( Wpu_reg value );
	void outanalog_ch0_zoom_offset(  );

	Wpu_reg inanalog_ch0_zoom_offset(  );

	void setanalog_ch1_zoom_offset( Wpu_reg value );
	void outanalog_ch1_zoom_offset( Wpu_reg value );
	void outanalog_ch1_zoom_offset(  );

	Wpu_reg inanalog_ch1_zoom_offset(  );

	void setanalog_ch2_zoom_offset( Wpu_reg value );
	void outanalog_ch2_zoom_offset( Wpu_reg value );
	void outanalog_ch2_zoom_offset(  );

	Wpu_reg inanalog_ch2_zoom_offset(  );

	void setanalog_ch3_zoom_offset( Wpu_reg value );
	void outanalog_ch3_zoom_offset( Wpu_reg value );
	void outanalog_ch3_zoom_offset(  );

	Wpu_reg inanalog_ch3_zoom_offset(  );

	void setmax_out_main_max_out( Wpu_reg value );
	void setmax_out_zoom_max_out( Wpu_reg value );

	void setmax_out( Wpu_reg value );
	void outmax_out_main_max_out( Wpu_reg value );
	void pulsemax_out_main_max_out( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outmax_out_zoom_max_out( Wpu_reg value );
	void pulsemax_out_zoom_max_out( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outmax_out( Wpu_reg value );
	void outmax_out(  );


	Wpu_reg inmax_out(  );

	void setview_bottom_main_view_bottom( Wpu_reg value );
	void setview_bottom_zoom_view_bottom( Wpu_reg value );

	void setview_bottom( Wpu_reg value );
	void outview_bottom_main_view_bottom( Wpu_reg value );
	void pulseview_bottom_main_view_bottom( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outview_bottom_zoom_view_bottom( Wpu_reg value );
	void pulseview_bottom_zoom_view_bottom( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outview_bottom( Wpu_reg value );
	void outview_bottom(  );


	Wpu_reg inview_bottom(  );

	void setzoom_en_en( Wpu_reg value );

	void setzoom_en( Wpu_reg value );
	void outzoom_en_en( Wpu_reg value );
	void pulsezoom_en_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outzoom_en( Wpu_reg value );
	void outzoom_en(  );


	Wpu_reg inzoom_en(  );

	void setanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg value );

	void setanalog_ch0_zoom_gain( Wpu_reg value );
	void outanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg value );
	void pulseanalog_ch0_zoom_gain_analog_ch0_zoom_gain( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch0_zoom_gain( Wpu_reg value );
	void outanalog_ch0_zoom_gain(  );


	Wpu_reg inanalog_ch0_zoom_gain(  );

	void setanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg value );

	void setanalog_ch1_zoom_gain( Wpu_reg value );
	void outanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg value );
	void pulseanalog_ch1_zoom_gain_analog_ch1_zoom_gain( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch1_zoom_gain( Wpu_reg value );
	void outanalog_ch1_zoom_gain(  );


	Wpu_reg inanalog_ch1_zoom_gain(  );

	void setanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg value );

	void setanalog_ch2_zoom_gain( Wpu_reg value );
	void outanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg value );
	void pulseanalog_ch2_zoom_gain_analog_ch2_zoom_gain( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch2_zoom_gain( Wpu_reg value );
	void outanalog_ch2_zoom_gain(  );


	Wpu_reg inanalog_ch2_zoom_gain(  );

	void setanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg value );

	void setanalog_ch3_zoom_gain( Wpu_reg value );
	void outanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg value );
	void pulseanalog_ch3_zoom_gain_analog_ch3_zoom_gain( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outanalog_ch3_zoom_gain( Wpu_reg value );
	void outanalog_ch3_zoom_gain(  );


	Wpu_reg inanalog_ch3_zoom_gain(  );

	Wpu_reg indebug_info_pm(  );

	void setzoom_scale_times_zoom_scale_times_m( Wpu_reg value );
	void setzoom_scale_times_zoom_scale_times_n( Wpu_reg value );
	void setzoom_scale_times_zoom_scale_times_en( Wpu_reg value );

	void setzoom_scale_times( Wpu_reg value );
	void outzoom_scale_times_zoom_scale_times_m( Wpu_reg value );
	void pulsezoom_scale_times_zoom_scale_times_m( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outzoom_scale_times_zoom_scale_times_n( Wpu_reg value );
	void pulsezoom_scale_times_zoom_scale_times_n( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outzoom_scale_times_zoom_scale_times_en( Wpu_reg value );
	void pulsezoom_scale_times_zoom_scale_times_en( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outzoom_scale_times( Wpu_reg value );
	void outzoom_scale_times(  );


	Wpu_reg inzoom_scale_times(  );

	void setzoom_scale_length( Wpu_reg value );
	void outzoom_scale_length( Wpu_reg value );
	void outzoom_scale_length(  );

	Wpu_reg inzoom_scale_length(  );

	void settime_base_mode( Wpu_reg value );
	void outtime_base_mode( Wpu_reg value );
	void outtime_base_mode(  );

	Wpu_reg intime_base_mode(  );

	void setscan_trig_cnt_scan_trig_col( Wpu_reg value );
	void setscan_trig_cnt_scan_trig_addr( Wpu_reg value );

	void setscan_trig_cnt( Wpu_reg value );
	void outscan_trig_cnt_scan_trig_col( Wpu_reg value );
	void pulsescan_trig_cnt_scan_trig_col( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );
	void outscan_trig_cnt_scan_trig_addr( Wpu_reg value );
	void pulsescan_trig_cnt_scan_trig_addr( Wpu_reg activeV, Wpu_reg idleV=0, int timeus=0 );

	void outscan_trig_cnt( Wpu_reg value );
	void outscan_trig_cnt(  );


	Wpu_reg inscan_trig_cnt(  );

	Wpu_reg inroll_lenth(  );

public:
	cache_addr addr_version();
	cache_addr addr_init_pe();
	cache_addr addr_config_ID();
	cache_addr addr_display_en();

	cache_addr addr_analog_mode();
	cache_addr addr_LA_en();
	cache_addr addr_refresh_time();
	cache_addr addr_persist_mode();

	cache_addr addr_persist_init();
	cache_addr addr_persist_dim_rate();
	cache_addr addr_plot_mode();
	cache_addr addr_slope_mode();

	cache_addr addr_analog_ch0_gain();
	cache_addr addr_analog_ch0_noise();
	cache_addr addr_analog_ch0_offset();
	cache_addr addr_analog_ch1_gain();

	cache_addr addr_analog_ch1_noise();
	cache_addr addr_analog_ch1_offset();
	cache_addr addr_analog_ch2_gain();
	cache_addr addr_analog_ch2_noise();

	cache_addr addr_analog_ch2_offset();
	cache_addr addr_analog_ch3_gain();
	cache_addr addr_analog_ch3_noise();
	cache_addr addr_analog_ch3_offset();

	cache_addr addr_color_map();
	cache_addr addr_view_layer();
	cache_addr addr_trace_layer_addr_0();
	cache_addr addr_trace_layer_addr_1();

	cache_addr addr_wpu_clear();
	cache_addr addr_srn_num();
	cache_addr addr_wpu_status();
	cache_addr addr_mem_status();

	cache_addr addr_init_status();
	cache_addr addr_load_config_id();
	cache_addr addr_init_la_plot_table();
	cache_addr addr_la_plot_tbl_data();

	cache_addr addr_wave_view_start();
	cache_addr addr_wave_view_length();
	cache_addr addr_zoom_view_start();
	cache_addr addr_zoom_view_length();

	cache_addr addr_xy0_en();
	cache_addr addr_xy1_en();
	cache_addr addr_xy2_en();
	cache_addr addr_xy3_en();

	cache_addr addr_init_palette_table();
	cache_addr addr_palette_tbl_wdata();
	cache_addr addr_data_mode();
	cache_addr addr_scale_times();

	cache_addr addr_scale_length();
	cache_addr addr_analog_ch0_zoom_offset();
	cache_addr addr_analog_ch1_zoom_offset();
	cache_addr addr_analog_ch2_zoom_offset();

	cache_addr addr_analog_ch3_zoom_offset();
	cache_addr addr_max_out();
	cache_addr addr_view_bottom();
	cache_addr addr_zoom_en();

	cache_addr addr_analog_ch0_zoom_gain();
	cache_addr addr_analog_ch1_zoom_gain();
	cache_addr addr_analog_ch2_zoom_gain();
	cache_addr addr_analog_ch3_zoom_gain();

	cache_addr addr_debug_info_pm();
	cache_addr addr_zoom_scale_times();
	cache_addr addr_zoom_scale_length();
	cache_addr addr_time_base_mode();

	cache_addr addr_scan_trig_cnt();
	cache_addr addr_roll_lenth();
};
#define _remap_Wpu_()\
	mapIn( &version, 0x400 + mAddrBase );\
	mapIn( &init_pe, 0x0588 + mAddrBase );\
	mapIn( &config_ID, 0x0410 + mAddrBase );\
	mapIn( &display_en, 0x0414 + mAddrBase );\
	\
	mapIn( &analog_mode, 0x0420 + mAddrBase );\
	mapIn( &LA_en, 0x0428 + mAddrBase );\
	mapIn( &refresh_time, 0x042c + mAddrBase );\
	mapIn( &persist_mode, 0x043c + mAddrBase );\
	\
	mapIn( &persist_init, 0x0440 + mAddrBase );\
	mapIn( &persist_dim_rate, 0x0444 + mAddrBase );\
	mapIn( &plot_mode, 0x0448 + mAddrBase );\
	mapIn( &slope_mode, 0x044c + mAddrBase );\
	\
	mapIn( &analog_ch0_gain, 0x0460 + mAddrBase );\
	mapIn( &analog_ch0_noise, 0x0464 + mAddrBase );\
	mapIn( &analog_ch0_offset, 0x0468 + mAddrBase );\
	mapIn( &analog_ch1_gain, 0x0470 + mAddrBase );\
	\
	mapIn( &analog_ch1_noise, 0x0474 + mAddrBase );\
	mapIn( &analog_ch1_offset, 0x0478 + mAddrBase );\
	mapIn( &analog_ch2_gain, 0x0480 + mAddrBase );\
	mapIn( &analog_ch2_noise, 0x0484 + mAddrBase );\
	\
	mapIn( &analog_ch2_offset, 0x0488 + mAddrBase );\
	mapIn( &analog_ch3_gain, 0x0490 + mAddrBase );\
	mapIn( &analog_ch3_noise, 0x0494 + mAddrBase );\
	mapIn( &analog_ch3_offset, 0x0498 + mAddrBase );\
	\
	mapIn( &color_map, 0x0550 + mAddrBase );\
	mapIn( &view_layer, 0x0554 + mAddrBase );\
	mapIn( &trace_layer_addr_0, 0x0558 + mAddrBase );\
	mapIn( &trace_layer_addr_1, 0x055c + mAddrBase );\
	\
	mapIn( &wpu_clear, 0x0584 + mAddrBase );\
	mapIn( &srn_num, 0x05FC + mAddrBase );\
	mapIn( &wpu_status, 0x0600 + mAddrBase );\
	mapIn( &mem_status, 0x0604 + mAddrBase );\
	\
	mapIn( &init_status, 0x0608 + mAddrBase );\
	mapIn( &load_config_id, 0x060c + mAddrBase );\
	mapIn( &init_la_plot_table, 0x059C + mAddrBase );\
	mapIn( &la_plot_tbl_data, 0x04E0 + mAddrBase );\
	\
	mapIn( &wave_view_start, 0x0450 + mAddrBase );\
	mapIn( &wave_view_length, 0x0454 + mAddrBase );\
	mapIn( &zoom_view_start, 0x0518 + mAddrBase );\
	mapIn( &zoom_view_length, 0x051c + mAddrBase );\
	\
	mapIn( &xy0_en, 0x0540 + mAddrBase );\
	mapIn( &xy1_en, 0x0544 + mAddrBase );\
	mapIn( &xy2_en, 0x0548 + mAddrBase );\
	mapIn( &xy3_en, 0x054c + mAddrBase );\
	\
	mapIn( &init_palette_table, 0x0590 + mAddrBase );\
	mapIn( &palette_tbl_wdata, 0x0560 + mAddrBase );\
	mapIn( &data_mode, 0x430 + mAddrBase );\
	mapIn( &scale_times, 0x0570 + mAddrBase );\
	\
	mapIn( &scale_length, 0x0574 + mAddrBase );\
	mapIn( &analog_ch0_zoom_offset, 0x04F0 + mAddrBase );\
	mapIn( &analog_ch1_zoom_offset, 0x04F4 + mAddrBase );\
	mapIn( &analog_ch2_zoom_offset, 0x04F8 + mAddrBase );\
	\
	mapIn( &analog_ch3_zoom_offset, 0x04FC + mAddrBase );\
	mapIn( &max_out, 0x0500 + mAddrBase );\
	mapIn( &view_bottom, 0x0504 + mAddrBase );\
	mapIn( &zoom_en, 0x0438 + mAddrBase );\
	\
	mapIn( &analog_ch0_zoom_gain, 0x0508 + mAddrBase );\
	mapIn( &analog_ch1_zoom_gain, 0x050c + mAddrBase );\
	mapIn( &analog_ch2_zoom_gain, 0x0510 + mAddrBase );\
	mapIn( &analog_ch3_zoom_gain, 0x0514 + mAddrBase );\
	\
	mapIn( &debug_info_pm, 0x638 + mAddrBase );\
	mapIn( &zoom_scale_times, 0x0520 + mAddrBase );\
	mapIn( &zoom_scale_length, 0x0524 + mAddrBase );\
	mapIn( &time_base_mode, 0x0430 + mAddrBase );\
	\
	mapIn( &scan_trig_cnt, 0x0528 + mAddrBase );\
	mapIn( &roll_lenth, 0x62c + mAddrBase );\


#endif
