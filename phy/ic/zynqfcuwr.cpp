#include "zynqfcuwr.h"
void ZynqFcuWrMemProxy::assigntrace_req_trace_req( ZynqFcuWr_reg value )
{
	mem_assign_field(trace_req,trace_req,value);
}
void ZynqFcuWrMemProxy::assigntrace_req_trace_datype( ZynqFcuWr_reg value )
{
	mem_assign_field(trace_req,trace_datype,value);
}

void ZynqFcuWrMemProxy::assigntrace_req( ZynqFcuWr_reg value )
{
	mem_assign_field(trace_req,payload,value);
}


void ZynqFcuWrMemProxy::assigneye_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(eye_trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignroll_ana_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(roll_ana_trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignroll_la_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(roll_la_trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignroll_ana_trace_waddr_end( ZynqFcuWr_reg value )
{
	mem_assign(roll_ana_trace_waddr_end,value);
}


void ZynqFcuWrMemProxy::assignroll_la_trace_waddr_end( ZynqFcuWr_reg value )
{
	mem_assign(roll_la_trace_waddr_end,value);
}


void ZynqFcuWrMemProxy::assignroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg value )
{
	mem_assign_field(roll_trace_waddr_rst,roll_trace_waddr_rst_ctl,value);
}

void ZynqFcuWrMemProxy::assignroll_trace_waddr_rst( ZynqFcuWr_reg value )
{
	mem_assign_field(roll_trace_waddr_rst,payload,value);
}


void ZynqFcuWrMemProxy::assignsearch_result_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(search_result_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignsearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg value )
{
	mem_assign_field(search_result_wdone_clr,search_result_wdone_clr,value);
}

void ZynqFcuWrMemProxy::assignsearch_result_wdone_clr( ZynqFcuWr_reg value )
{
	mem_assign_field(search_result_wdone_clr,payload,value);
}


void ZynqFcuWrMemProxy::assignana_main_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(ana_main_trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignla_main_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(la_main_trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignana_zoom_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(ana_zoom_trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignla_zoom_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(la_zoom_trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assignreserved1( ZynqFcuWr_reg value )
{
	mem_assign(reserved1,value);
}


void ZynqFcuWrMemProxy::assigncomm_test( ZynqFcuWr_reg value )
{
	mem_assign(comm_test,value);
}


void ZynqFcuWrMemProxy::assigntrace_wdone_clr( ZynqFcuWr_reg value )
{
	mem_assign(trace_wdone_clr,value);
}


void ZynqFcuWrMemProxy::assigntrace_wbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign(trace_wbaseaddr,value);
}


void ZynqFcuWrMemProxy::assigntrace_dat_len( ZynqFcuWr_reg value )
{
	mem_assign(trace_dat_len,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_chx_lax_rdat_num( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_chx_lax_rdat_num,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_chx_raddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_chx_raddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_cha_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_cha_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_chb_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_chb_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_chc_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_chc_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_chd_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_chd_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_cha_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_cha_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_chb_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_chb_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_chc_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_chc_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_chd_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_chd_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_lax_raddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_lax_raddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la0_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la0_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la1_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la1_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la2_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la2_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la3_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la3_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la4_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la4_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la5_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la5_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la6_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la6_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la7_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la7_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la8_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la8_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la9_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la9_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la10_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la10_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la11_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la11_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la12_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la12_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la13_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la13_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la14_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la14_waddr,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_la15_waddr( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_la15_waddr,value);
}


void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_h,value);
}
void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_l,value);
}
void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_h,value);
}
void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_l,value);
}

void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp,payload,value);
}


void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_h,value);
}
void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_l,value);
}
void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_h,value);
}
void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_l,value);
}

void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp,payload,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg1,deinterweave_source_type,value);
}
void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg1,decode_en,value);
}
void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg1,decode_en_sec,value);
}

void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg1( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg1,payload,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg2,deinterweave_rstart,value);
}

void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg2( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg2,payload,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg3,deinterweave_done_clr,value);
}

void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg3( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg3,payload,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg4,channel_mode,value);
}

void ZynqFcuWrMemProxy::assigndeinterweave_property_cfg4( ZynqFcuWr_reg value )
{
	mem_assign_field(deinterweave_property_cfg4,payload,value);
}


void ZynqFcuWrMemProxy::assignfpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg1,fpga_rdat_num,value);
}
void ZynqFcuWrMemProxy::assignfpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg1,fpga_rdat_type,value);
}

void ZynqFcuWrMemProxy::assignfpga_rdat_cfg1( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg1,payload,value);
}


void ZynqFcuWrMemProxy::assignfpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg2,fpga_rdat_start,value);
}

void ZynqFcuWrMemProxy::assignfpga_rdat_cfg2( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg2,payload,value);
}


void ZynqFcuWrMemProxy::assignfpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg3,fpga_rdat_done_clr,value);
}

void ZynqFcuWrMemProxy::assignfpga_rdat_cfg3( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg3,payload,value);
}


void ZynqFcuWrMemProxy::assignfpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg4,exdat_rbaseaddr,value);
}

void ZynqFcuWrMemProxy::assignfpga_rdat_cfg4( ZynqFcuWr_reg value )
{
	mem_assign_field(fpga_rdat_cfg4,payload,value);
}


void ZynqFcuWrMemProxy::assignreserved6( ZynqFcuWr_reg value )
{
	mem_assign(reserved6,value);
}


ZynqFcuWr_reg ZynqFcuWrMemProxy::getreserved6()
{
	return reserved6;
}


void ZynqFcuWrMemProxy::assignreserved7( ZynqFcuWr_reg value )
{
	mem_assign(reserved7,value);
}


ZynqFcuWr_reg ZynqFcuWrMemProxy::getreserved7()
{
	return reserved7;
}


void ZynqFcuWrMemProxy::assignreserved8( ZynqFcuWr_reg value )
{
	mem_assign(reserved8,value);
}


ZynqFcuWr_reg ZynqFcuWrMemProxy::getreserved8()
{
	return reserved8;
}


void ZynqFcuWrMemProxy::assignreserved9( ZynqFcuWr_reg value )
{
	mem_assign(reserved9,value);
}


ZynqFcuWr_reg ZynqFcuWrMemProxy::getreserved9()
{
	return reserved9;
}


void ZynqFcuWrMemProxy::assignreserved10( ZynqFcuWr_reg value )
{
	mem_assign(reserved10,value);
}


ZynqFcuWr_reg ZynqFcuWrMemProxy::getreserved10()
{
	return reserved10;
}


void ZynqFcuWrMemProxy::assignreserved11( ZynqFcuWr_reg value )
{
	mem_assign(reserved11,value);
}


ZynqFcuWr_reg ZynqFcuWrMemProxy::getreserved11()
{
	return reserved11;
}


void ZynqFcuWrMemProxy::assigncompress_type_compress_type( ZynqFcuWr_reg value )
{
	mem_assign_field(compress_type,compress_type,value);
}

void ZynqFcuWrMemProxy::assigncompress_type( ZynqFcuWr_reg value )
{
	mem_assign_field(compress_type,payload,value);
}


void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_h_sec,value);
}
void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_l_sec,value);
}
void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_h_sec,value);
}
void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_l_sec,value);
}

void ZynqFcuWrMemProxy::assigndecode_a_or_b_channel_cmp_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_a_or_b_channel_cmp_sec,payload,value);
}


void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_h_sec,value);
}
void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_l_sec,value);
}
void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_h_sec,value);
}
void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_l_sec,value);
}

void ZynqFcuWrMemProxy::assigndecode_c_or_d_channel_cmp_sec( ZynqFcuWr_reg value )
{
	mem_assign_field(decode_c_or_d_channel_cmp_sec,payload,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_cha_waddr_sec( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_cha_waddr_sec,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_chb_waddr_sec( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_chb_waddr_sec,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_chc_waddr_sec( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_chc_waddr_sec,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_decode_chd_waddr_sec( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_decode_chd_waddr_sec,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_chx_raddr_end( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_chx_raddr_end,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_lax_raddr_end( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_lax_raddr_end,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_chx_raddr_base( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_chx_raddr_base,value);
}


void ZynqFcuWrMemProxy::assigndeinterweave_lax_raddr_base( ZynqFcuWr_reg value )
{
	mem_assign(deinterweave_lax_raddr_base,value);
}


void ZynqFcuWrMemProxy::assigndbg_cfg1( ZynqFcuWr_reg value )
{
	mem_assign(dbg_cfg1,value);
}



void ZynqFcuWrMem::_loadOtp()
{
	trace_req.payload=0x0;
		trace_req.trace_req=0x0;
		trace_req.trace_datype=0x0;

	eye_trace_wbaseaddr=0x1c000000;
	roll_ana_trace_wbaseaddr=0x0;
	roll_la_trace_wbaseaddr=0x0;

	roll_ana_trace_waddr_end=0x0;
	roll_la_trace_waddr_end=0x0;
	roll_trace_waddr_rst.payload=0x0;
		roll_trace_waddr_rst.roll_trace_waddr_rst_ctl=0x0;

	search_result_wbaseaddr=0x1f000000;

	search_result_wdone_clr.payload=0x0;
		search_result_wdone_clr.search_result_wdone_clr=0x0;

	ana_main_trace_wbaseaddr=0x1c000000;
	la_main_trace_wbaseaddr=0x1c400000;
	ana_zoom_trace_wbaseaddr=0x1c800000;

	la_zoom_trace_wbaseaddr=0x1cc00000;
	reserved1=0x0;
	comm_test=0x0;
	trace_wdone_clr=0x0;

	trace_wbaseaddr=0x0;
	trace_dat_len=0x0;
	deinterweave_chx_lax_rdat_num=0x0;
	deinterweave_chx_raddr=0x0;

	deinterweave_cha_waddr=0x0;
	deinterweave_chb_waddr=0x0;
	deinterweave_chc_waddr=0x0;
	deinterweave_chd_waddr=0x0;

	deinterweave_decode_cha_waddr=0x0;
	deinterweave_decode_chb_waddr=0x0;
	deinterweave_decode_chc_waddr=0x0;
	deinterweave_decode_chd_waddr=0x0;

	deinterweave_lax_raddr=0x0;
	deinterweave_la0_waddr=0x0;
	deinterweave_la1_waddr=0x0;
	deinterweave_la2_waddr=0x0;

	deinterweave_la3_waddr=0x0;
	deinterweave_la4_waddr=0x0;
	deinterweave_la5_waddr=0x0;
	deinterweave_la6_waddr=0x0;

	deinterweave_la7_waddr=0x0;
	deinterweave_la8_waddr=0x0;
	deinterweave_la9_waddr=0x0;
	deinterweave_la10_waddr=0x0;

	deinterweave_la11_waddr=0x0;
	deinterweave_la12_waddr=0x0;
	deinterweave_la13_waddr=0x0;
	deinterweave_la14_waddr=0x0;

	deinterweave_la15_waddr=0x0;
	decode_a_or_b_channel_cmp.payload=0x0;
		decode_a_or_b_channel_cmp.cmp_a_level_h=0x0;
		decode_a_or_b_channel_cmp.cmp_a_level_l=0x0;
		decode_a_or_b_channel_cmp.cmp_b_level_h=0x0;
		decode_a_or_b_channel_cmp.cmp_b_level_l=0x0;

	decode_c_or_d_channel_cmp.payload=0x0;
		decode_c_or_d_channel_cmp.cmp_c_level_h=0x0;
		decode_c_or_d_channel_cmp.cmp_c_level_l=0x0;
		decode_c_or_d_channel_cmp.cmp_d_level_h=0x0;
		decode_c_or_d_channel_cmp.cmp_d_level_l=0x0;

	deinterweave_property_cfg1.payload=0x0;
		deinterweave_property_cfg1.deinterweave_source_type=0x0;
		deinterweave_property_cfg1.decode_en=0x0;
		deinterweave_property_cfg1.decode_en_sec=0x0;


	deinterweave_property_cfg2.payload=0x0;
		deinterweave_property_cfg2.deinterweave_rstart=0x0;

	deinterweave_property_cfg3.payload=0x0;
		deinterweave_property_cfg3.deinterweave_done_clr=0x0;

	deinterweave_property_cfg4.payload=0x0;
		deinterweave_property_cfg4.channel_mode=0x0;

	fpga_rdat_cfg1.payload=0x0;
		fpga_rdat_cfg1.fpga_rdat_num=0x0;
		fpga_rdat_cfg1.fpga_rdat_type=0x0;


	fpga_rdat_cfg2.payload=0x0;
		fpga_rdat_cfg2.fpga_rdat_start=0x0;

	fpga_rdat_cfg3.payload=0x0;
		fpga_rdat_cfg3.fpga_rdat_done_clr=0x0;

	fpga_rdat_cfg4.payload=0x0;
		fpga_rdat_cfg4.exdat_rbaseaddr=0x0;

	reserved6=0x0;

	reserved7=0x0;
	reserved8=0x0;
	reserved9=0x0;
	reserved10=0x0;

	reserved11=0x0;
	compress_type.payload=0x0;
		compress_type.compress_type=0x0;

	decode_a_or_b_channel_cmp_sec.payload=0x0;
		decode_a_or_b_channel_cmp_sec.cmp_a_level_h_sec=0x0;
		decode_a_or_b_channel_cmp_sec.cmp_a_level_l_sec=0x0;
		decode_a_or_b_channel_cmp_sec.cmp_b_level_h_sec=0x0;
		decode_a_or_b_channel_cmp_sec.cmp_b_level_l_sec=0x0;

	decode_c_or_d_channel_cmp_sec.payload=0x0;
		decode_c_or_d_channel_cmp_sec.cmp_c_level_h_sec=0x0;
		decode_c_or_d_channel_cmp_sec.cmp_c_level_l_sec=0x0;
		decode_c_or_d_channel_cmp_sec.cmp_d_level_h_sec=0x0;
		decode_c_or_d_channel_cmp_sec.cmp_d_level_l_sec=0x0;


	deinterweave_decode_cha_waddr_sec=0x0;
	deinterweave_decode_chb_waddr_sec=0x0;
	deinterweave_decode_chc_waddr_sec=0x0;
	deinterweave_decode_chd_waddr_sec=0x0;

	deinterweave_chx_raddr_end=0x0;
	deinterweave_lax_raddr_end=0x0;
	deinterweave_chx_raddr_base=0x0;
	deinterweave_lax_raddr_base=0x0;

	dbg_cfg1=0x0;
}

void ZynqFcuWrMem::_outOtp()
{
	outtrace_req();
	outeye_trace_wbaseaddr();
	outroll_ana_trace_wbaseaddr();
	outroll_la_trace_wbaseaddr();

	outroll_ana_trace_waddr_end();
	outroll_la_trace_waddr_end();
	outroll_trace_waddr_rst();
	outsearch_result_wbaseaddr();

	outsearch_result_wdone_clr();
	outana_main_trace_wbaseaddr();
	outla_main_trace_wbaseaddr();
	outana_zoom_trace_wbaseaddr();

	outla_zoom_trace_wbaseaddr();
	outreserved1();
	outcomm_test();
	outtrace_wdone_clr();

	outtrace_wbaseaddr();
	outtrace_dat_len();
	outdeinterweave_chx_lax_rdat_num();
	outdeinterweave_chx_raddr();

	outdeinterweave_cha_waddr();
	outdeinterweave_chb_waddr();
	outdeinterweave_chc_waddr();
	outdeinterweave_chd_waddr();

	outdeinterweave_decode_cha_waddr();
	outdeinterweave_decode_chb_waddr();
	outdeinterweave_decode_chc_waddr();
	outdeinterweave_decode_chd_waddr();

	outdeinterweave_lax_raddr();
	outdeinterweave_la0_waddr();
	outdeinterweave_la1_waddr();
	outdeinterweave_la2_waddr();

	outdeinterweave_la3_waddr();
	outdeinterweave_la4_waddr();
	outdeinterweave_la5_waddr();
	outdeinterweave_la6_waddr();

	outdeinterweave_la7_waddr();
	outdeinterweave_la8_waddr();
	outdeinterweave_la9_waddr();
	outdeinterweave_la10_waddr();

	outdeinterweave_la11_waddr();
	outdeinterweave_la12_waddr();
	outdeinterweave_la13_waddr();
	outdeinterweave_la14_waddr();

	outdeinterweave_la15_waddr();
	outdecode_a_or_b_channel_cmp();
	outdecode_c_or_d_channel_cmp();
	outdeinterweave_property_cfg1();

	outdeinterweave_property_cfg2();
	outdeinterweave_property_cfg3();
	outdeinterweave_property_cfg4();
	outfpga_rdat_cfg1();

	outfpga_rdat_cfg2();
	outfpga_rdat_cfg3();
	outfpga_rdat_cfg4();
	outreserved6();

	outreserved7();
	outreserved8();
	outreserved9();
	outreserved10();

	outreserved11();
	outcompress_type();
	outdecode_a_or_b_channel_cmp_sec();
	outdecode_c_or_d_channel_cmp_sec();

	outdeinterweave_decode_cha_waddr_sec();
	outdeinterweave_decode_chb_waddr_sec();
	outdeinterweave_decode_chc_waddr_sec();
	outdeinterweave_decode_chd_waddr_sec();

	outdeinterweave_chx_raddr_end();
	outdeinterweave_lax_raddr_end();
	outdeinterweave_chx_raddr_base();
	outdeinterweave_lax_raddr_base();

	outdbg_cfg1();
}
void ZynqFcuWrMem::push( ZynqFcuWrMemProxy *proxy )
{
	if ( trace_req.payload != proxy->trace_req.payload )
	{reg_assign_field(trace_req,payload,proxy->trace_req.payload);}

	if ( eye_trace_wbaseaddr != proxy->eye_trace_wbaseaddr )
	{reg_assign(eye_trace_wbaseaddr,proxy->eye_trace_wbaseaddr);}

	if ( roll_ana_trace_wbaseaddr != proxy->roll_ana_trace_wbaseaddr )
	{reg_assign(roll_ana_trace_wbaseaddr,proxy->roll_ana_trace_wbaseaddr);}

	if ( roll_la_trace_wbaseaddr != proxy->roll_la_trace_wbaseaddr )
	{reg_assign(roll_la_trace_wbaseaddr,proxy->roll_la_trace_wbaseaddr);}

	if ( roll_ana_trace_waddr_end != proxy->roll_ana_trace_waddr_end )
	{reg_assign(roll_ana_trace_waddr_end,proxy->roll_ana_trace_waddr_end);}

	if ( roll_la_trace_waddr_end != proxy->roll_la_trace_waddr_end )
	{reg_assign(roll_la_trace_waddr_end,proxy->roll_la_trace_waddr_end);}

	if ( roll_trace_waddr_rst.payload != proxy->roll_trace_waddr_rst.payload )
	{reg_assign_field(roll_trace_waddr_rst,payload,proxy->roll_trace_waddr_rst.payload);}

	if ( search_result_wbaseaddr != proxy->search_result_wbaseaddr )
	{reg_assign(search_result_wbaseaddr,proxy->search_result_wbaseaddr);}

	if ( search_result_wdone_clr.payload != proxy->search_result_wdone_clr.payload )
	{reg_assign_field(search_result_wdone_clr,payload,proxy->search_result_wdone_clr.payload);}

	if ( ana_main_trace_wbaseaddr != proxy->ana_main_trace_wbaseaddr )
	{reg_assign(ana_main_trace_wbaseaddr,proxy->ana_main_trace_wbaseaddr);}

	if ( la_main_trace_wbaseaddr != proxy->la_main_trace_wbaseaddr )
	{reg_assign(la_main_trace_wbaseaddr,proxy->la_main_trace_wbaseaddr);}

	if ( ana_zoom_trace_wbaseaddr != proxy->ana_zoom_trace_wbaseaddr )
	{reg_assign(ana_zoom_trace_wbaseaddr,proxy->ana_zoom_trace_wbaseaddr);}

	if ( la_zoom_trace_wbaseaddr != proxy->la_zoom_trace_wbaseaddr )
	{reg_assign(la_zoom_trace_wbaseaddr,proxy->la_zoom_trace_wbaseaddr);}

	if ( reserved1 != proxy->reserved1 )
	{reg_assign(reserved1,proxy->reserved1);}

	if ( comm_test != proxy->comm_test )
	{reg_assign(comm_test,proxy->comm_test);}

	if ( trace_wdone_clr != proxy->trace_wdone_clr )
	{reg_assign(trace_wdone_clr,proxy->trace_wdone_clr);}

	if ( trace_wbaseaddr != proxy->trace_wbaseaddr )
	{reg_assign(trace_wbaseaddr,proxy->trace_wbaseaddr);}

	if ( trace_dat_len != proxy->trace_dat_len )
	{reg_assign(trace_dat_len,proxy->trace_dat_len);}

	if ( deinterweave_chx_lax_rdat_num != proxy->deinterweave_chx_lax_rdat_num )
	{reg_assign(deinterweave_chx_lax_rdat_num,proxy->deinterweave_chx_lax_rdat_num);}

	if ( deinterweave_chx_raddr != proxy->deinterweave_chx_raddr )
	{reg_assign(deinterweave_chx_raddr,proxy->deinterweave_chx_raddr);}

	if ( deinterweave_cha_waddr != proxy->deinterweave_cha_waddr )
	{reg_assign(deinterweave_cha_waddr,proxy->deinterweave_cha_waddr);}

	if ( deinterweave_chb_waddr != proxy->deinterweave_chb_waddr )
	{reg_assign(deinterweave_chb_waddr,proxy->deinterweave_chb_waddr);}

	if ( deinterweave_chc_waddr != proxy->deinterweave_chc_waddr )
	{reg_assign(deinterweave_chc_waddr,proxy->deinterweave_chc_waddr);}

	if ( deinterweave_chd_waddr != proxy->deinterweave_chd_waddr )
	{reg_assign(deinterweave_chd_waddr,proxy->deinterweave_chd_waddr);}

	if ( deinterweave_decode_cha_waddr != proxy->deinterweave_decode_cha_waddr )
	{reg_assign(deinterweave_decode_cha_waddr,proxy->deinterweave_decode_cha_waddr);}

	if ( deinterweave_decode_chb_waddr != proxy->deinterweave_decode_chb_waddr )
	{reg_assign(deinterweave_decode_chb_waddr,proxy->deinterweave_decode_chb_waddr);}

	if ( deinterweave_decode_chc_waddr != proxy->deinterweave_decode_chc_waddr )
	{reg_assign(deinterweave_decode_chc_waddr,proxy->deinterweave_decode_chc_waddr);}

	if ( deinterweave_decode_chd_waddr != proxy->deinterweave_decode_chd_waddr )
	{reg_assign(deinterweave_decode_chd_waddr,proxy->deinterweave_decode_chd_waddr);}

	if ( deinterweave_lax_raddr != proxy->deinterweave_lax_raddr )
	{reg_assign(deinterweave_lax_raddr,proxy->deinterweave_lax_raddr);}

	if ( deinterweave_la0_waddr != proxy->deinterweave_la0_waddr )
	{reg_assign(deinterweave_la0_waddr,proxy->deinterweave_la0_waddr);}

	if ( deinterweave_la1_waddr != proxy->deinterweave_la1_waddr )
	{reg_assign(deinterweave_la1_waddr,proxy->deinterweave_la1_waddr);}

	if ( deinterweave_la2_waddr != proxy->deinterweave_la2_waddr )
	{reg_assign(deinterweave_la2_waddr,proxy->deinterweave_la2_waddr);}

	if ( deinterweave_la3_waddr != proxy->deinterweave_la3_waddr )
	{reg_assign(deinterweave_la3_waddr,proxy->deinterweave_la3_waddr);}

	if ( deinterweave_la4_waddr != proxy->deinterweave_la4_waddr )
	{reg_assign(deinterweave_la4_waddr,proxy->deinterweave_la4_waddr);}

	if ( deinterweave_la5_waddr != proxy->deinterweave_la5_waddr )
	{reg_assign(deinterweave_la5_waddr,proxy->deinterweave_la5_waddr);}

	if ( deinterweave_la6_waddr != proxy->deinterweave_la6_waddr )
	{reg_assign(deinterweave_la6_waddr,proxy->deinterweave_la6_waddr);}

	if ( deinterweave_la7_waddr != proxy->deinterweave_la7_waddr )
	{reg_assign(deinterweave_la7_waddr,proxy->deinterweave_la7_waddr);}

	if ( deinterweave_la8_waddr != proxy->deinterweave_la8_waddr )
	{reg_assign(deinterweave_la8_waddr,proxy->deinterweave_la8_waddr);}

	if ( deinterweave_la9_waddr != proxy->deinterweave_la9_waddr )
	{reg_assign(deinterweave_la9_waddr,proxy->deinterweave_la9_waddr);}

	if ( deinterweave_la10_waddr != proxy->deinterweave_la10_waddr )
	{reg_assign(deinterweave_la10_waddr,proxy->deinterweave_la10_waddr);}

	if ( deinterweave_la11_waddr != proxy->deinterweave_la11_waddr )
	{reg_assign(deinterweave_la11_waddr,proxy->deinterweave_la11_waddr);}

	if ( deinterweave_la12_waddr != proxy->deinterweave_la12_waddr )
	{reg_assign(deinterweave_la12_waddr,proxy->deinterweave_la12_waddr);}

	if ( deinterweave_la13_waddr != proxy->deinterweave_la13_waddr )
	{reg_assign(deinterweave_la13_waddr,proxy->deinterweave_la13_waddr);}

	if ( deinterweave_la14_waddr != proxy->deinterweave_la14_waddr )
	{reg_assign(deinterweave_la14_waddr,proxy->deinterweave_la14_waddr);}

	if ( deinterweave_la15_waddr != proxy->deinterweave_la15_waddr )
	{reg_assign(deinterweave_la15_waddr,proxy->deinterweave_la15_waddr);}

	if ( decode_a_or_b_channel_cmp.payload != proxy->decode_a_or_b_channel_cmp.payload )
	{reg_assign_field(decode_a_or_b_channel_cmp,payload,proxy->decode_a_or_b_channel_cmp.payload);}

	if ( decode_c_or_d_channel_cmp.payload != proxy->decode_c_or_d_channel_cmp.payload )
	{reg_assign_field(decode_c_or_d_channel_cmp,payload,proxy->decode_c_or_d_channel_cmp.payload);}

	if ( deinterweave_property_cfg1.payload != proxy->deinterweave_property_cfg1.payload )
	{reg_assign_field(deinterweave_property_cfg1,payload,proxy->deinterweave_property_cfg1.payload);}

	if ( deinterweave_property_cfg2.payload != proxy->deinterweave_property_cfg2.payload )
	{reg_assign_field(deinterweave_property_cfg2,payload,proxy->deinterweave_property_cfg2.payload);}

	if ( deinterweave_property_cfg3.payload != proxy->deinterweave_property_cfg3.payload )
	{reg_assign_field(deinterweave_property_cfg3,payload,proxy->deinterweave_property_cfg3.payload);}

	if ( deinterweave_property_cfg4.payload != proxy->deinterweave_property_cfg4.payload )
	{reg_assign_field(deinterweave_property_cfg4,payload,proxy->deinterweave_property_cfg4.payload);}

	if ( fpga_rdat_cfg1.payload != proxy->fpga_rdat_cfg1.payload )
	{reg_assign_field(fpga_rdat_cfg1,payload,proxy->fpga_rdat_cfg1.payload);}

	if ( fpga_rdat_cfg2.payload != proxy->fpga_rdat_cfg2.payload )
	{reg_assign_field(fpga_rdat_cfg2,payload,proxy->fpga_rdat_cfg2.payload);}

	if ( fpga_rdat_cfg3.payload != proxy->fpga_rdat_cfg3.payload )
	{reg_assign_field(fpga_rdat_cfg3,payload,proxy->fpga_rdat_cfg3.payload);}

	if ( fpga_rdat_cfg4.payload != proxy->fpga_rdat_cfg4.payload )
	{reg_assign_field(fpga_rdat_cfg4,payload,proxy->fpga_rdat_cfg4.payload);}

	if ( reserved6 != proxy->reserved6 )
	{reg_assign(reserved6,proxy->reserved6);}

	if ( reserved7 != proxy->reserved7 )
	{reg_assign(reserved7,proxy->reserved7);}

	if ( reserved8 != proxy->reserved8 )
	{reg_assign(reserved8,proxy->reserved8);}

	if ( reserved9 != proxy->reserved9 )
	{reg_assign(reserved9,proxy->reserved9);}

	if ( reserved10 != proxy->reserved10 )
	{reg_assign(reserved10,proxy->reserved10);}

	if ( reserved11 != proxy->reserved11 )
	{reg_assign(reserved11,proxy->reserved11);}

	if ( compress_type.payload != proxy->compress_type.payload )
	{reg_assign_field(compress_type,payload,proxy->compress_type.payload);}

	if ( decode_a_or_b_channel_cmp_sec.payload != proxy->decode_a_or_b_channel_cmp_sec.payload )
	{reg_assign_field(decode_a_or_b_channel_cmp_sec,payload,proxy->decode_a_or_b_channel_cmp_sec.payload);}

	if ( decode_c_or_d_channel_cmp_sec.payload != proxy->decode_c_or_d_channel_cmp_sec.payload )
	{reg_assign_field(decode_c_or_d_channel_cmp_sec,payload,proxy->decode_c_or_d_channel_cmp_sec.payload);}

	if ( deinterweave_decode_cha_waddr_sec != proxy->deinterweave_decode_cha_waddr_sec )
	{reg_assign(deinterweave_decode_cha_waddr_sec,proxy->deinterweave_decode_cha_waddr_sec);}

	if ( deinterweave_decode_chb_waddr_sec != proxy->deinterweave_decode_chb_waddr_sec )
	{reg_assign(deinterweave_decode_chb_waddr_sec,proxy->deinterweave_decode_chb_waddr_sec);}

	if ( deinterweave_decode_chc_waddr_sec != proxy->deinterweave_decode_chc_waddr_sec )
	{reg_assign(deinterweave_decode_chc_waddr_sec,proxy->deinterweave_decode_chc_waddr_sec);}

	if ( deinterweave_decode_chd_waddr_sec != proxy->deinterweave_decode_chd_waddr_sec )
	{reg_assign(deinterweave_decode_chd_waddr_sec,proxy->deinterweave_decode_chd_waddr_sec);}

	if ( deinterweave_chx_raddr_end != proxy->deinterweave_chx_raddr_end )
	{reg_assign(deinterweave_chx_raddr_end,proxy->deinterweave_chx_raddr_end);}

	if ( deinterweave_lax_raddr_end != proxy->deinterweave_lax_raddr_end )
	{reg_assign(deinterweave_lax_raddr_end,proxy->deinterweave_lax_raddr_end);}

	if ( deinterweave_chx_raddr_base != proxy->deinterweave_chx_raddr_base )
	{reg_assign(deinterweave_chx_raddr_base,proxy->deinterweave_chx_raddr_base);}

	if ( deinterweave_lax_raddr_base != proxy->deinterweave_lax_raddr_base )
	{reg_assign(deinterweave_lax_raddr_base,proxy->deinterweave_lax_raddr_base);}

	if ( dbg_cfg1 != proxy->dbg_cfg1 )
	{reg_assign(dbg_cfg1,proxy->dbg_cfg1);}

}
void ZynqFcuWrMem::pull( ZynqFcuWrMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(reserved6);
	reg_in(reserved7);
	reg_in(reserved8);
	reg_in(reserved9);
	reg_in(reserved10);
	reg_in(reserved11);

	*proxy = ZynqFcuWrMemProxy(*this);
}
void ZynqFcuWrMem::settrace_req_trace_req( ZynqFcuWr_reg value )
{
	reg_assign_field(trace_req,trace_req,value);
}
void ZynqFcuWrMem::settrace_req_trace_datype( ZynqFcuWr_reg value )
{
	reg_assign_field(trace_req,trace_datype,value);
}

void ZynqFcuWrMem::settrace_req( ZynqFcuWr_reg value )
{
	reg_assign_field(trace_req,payload,value);
}

void ZynqFcuWrMem::outtrace_req_trace_req( ZynqFcuWr_reg value )
{
	out_assign_field(trace_req,trace_req,value);
}
void ZynqFcuWrMem::pulsetrace_req_trace_req( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(trace_req,trace_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(trace_req,trace_req,idleV);
}
void ZynqFcuWrMem::outtrace_req_trace_datype( ZynqFcuWr_reg value )
{
	out_assign_field(trace_req,trace_datype,value);
}
void ZynqFcuWrMem::pulsetrace_req_trace_datype( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(trace_req,trace_datype,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(trace_req,trace_datype,idleV);
}

void ZynqFcuWrMem::outtrace_req( ZynqFcuWr_reg value )
{
	out_assign_field(trace_req,payload,value);
}

void ZynqFcuWrMem::outtrace_req()
{
	out_member(trace_req);
}


void ZynqFcuWrMem::seteye_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(eye_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outeye_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(eye_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outeye_trace_wbaseaddr()
{
	out_member(eye_trace_wbaseaddr);
}


void ZynqFcuWrMem::setroll_ana_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(roll_ana_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outroll_ana_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(roll_ana_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outroll_ana_trace_wbaseaddr()
{
	out_member(roll_ana_trace_wbaseaddr);
}


void ZynqFcuWrMem::setroll_la_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(roll_la_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outroll_la_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(roll_la_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outroll_la_trace_wbaseaddr()
{
	out_member(roll_la_trace_wbaseaddr);
}


void ZynqFcuWrMem::setroll_ana_trace_waddr_end( ZynqFcuWr_reg value )
{
	reg_assign(roll_ana_trace_waddr_end,value);
}

void ZynqFcuWrMem::outroll_ana_trace_waddr_end( ZynqFcuWr_reg value )
{
	out_assign(roll_ana_trace_waddr_end,value);
}

void ZynqFcuWrMem::outroll_ana_trace_waddr_end()
{
	out_member(roll_ana_trace_waddr_end);
}


void ZynqFcuWrMem::setroll_la_trace_waddr_end( ZynqFcuWr_reg value )
{
	reg_assign(roll_la_trace_waddr_end,value);
}

void ZynqFcuWrMem::outroll_la_trace_waddr_end( ZynqFcuWr_reg value )
{
	out_assign(roll_la_trace_waddr_end,value);
}

void ZynqFcuWrMem::outroll_la_trace_waddr_end()
{
	out_member(roll_la_trace_waddr_end);
}


void ZynqFcuWrMem::setroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg value )
{
	reg_assign_field(roll_trace_waddr_rst,roll_trace_waddr_rst_ctl,value);
}

void ZynqFcuWrMem::setroll_trace_waddr_rst( ZynqFcuWr_reg value )
{
	reg_assign_field(roll_trace_waddr_rst,payload,value);
}

void ZynqFcuWrMem::outroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg value )
{
	out_assign_field(roll_trace_waddr_rst,roll_trace_waddr_rst_ctl,value);
}
void ZynqFcuWrMem::pulseroll_trace_waddr_rst_roll_trace_waddr_rst_ctl( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(roll_trace_waddr_rst,roll_trace_waddr_rst_ctl,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(roll_trace_waddr_rst,roll_trace_waddr_rst_ctl,idleV);
}

void ZynqFcuWrMem::outroll_trace_waddr_rst( ZynqFcuWr_reg value )
{
	out_assign_field(roll_trace_waddr_rst,payload,value);
}

void ZynqFcuWrMem::outroll_trace_waddr_rst()
{
	out_member(roll_trace_waddr_rst);
}


void ZynqFcuWrMem::setsearch_result_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(search_result_wbaseaddr,value);
}

void ZynqFcuWrMem::outsearch_result_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(search_result_wbaseaddr,value);
}

void ZynqFcuWrMem::outsearch_result_wbaseaddr()
{
	out_member(search_result_wbaseaddr);
}


void ZynqFcuWrMem::setsearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg value )
{
	reg_assign_field(search_result_wdone_clr,search_result_wdone_clr,value);
}

void ZynqFcuWrMem::setsearch_result_wdone_clr( ZynqFcuWr_reg value )
{
	reg_assign_field(search_result_wdone_clr,payload,value);
}

void ZynqFcuWrMem::outsearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg value )
{
	out_assign_field(search_result_wdone_clr,search_result_wdone_clr,value);
}
void ZynqFcuWrMem::pulsesearch_result_wdone_clr_search_result_wdone_clr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(search_result_wdone_clr,search_result_wdone_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(search_result_wdone_clr,search_result_wdone_clr,idleV);
}

void ZynqFcuWrMem::outsearch_result_wdone_clr( ZynqFcuWr_reg value )
{
	out_assign_field(search_result_wdone_clr,payload,value);
}

void ZynqFcuWrMem::outsearch_result_wdone_clr()
{
	out_member(search_result_wdone_clr);
}


void ZynqFcuWrMem::setana_main_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(ana_main_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outana_main_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(ana_main_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outana_main_trace_wbaseaddr()
{
	out_member(ana_main_trace_wbaseaddr);
}


void ZynqFcuWrMem::setla_main_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(la_main_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outla_main_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(la_main_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outla_main_trace_wbaseaddr()
{
	out_member(la_main_trace_wbaseaddr);
}


void ZynqFcuWrMem::setana_zoom_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(ana_zoom_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outana_zoom_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(ana_zoom_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outana_zoom_trace_wbaseaddr()
{
	out_member(ana_zoom_trace_wbaseaddr);
}


void ZynqFcuWrMem::setla_zoom_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(la_zoom_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outla_zoom_trace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(la_zoom_trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outla_zoom_trace_wbaseaddr()
{
	out_member(la_zoom_trace_wbaseaddr);
}


void ZynqFcuWrMem::setreserved1( ZynqFcuWr_reg value )
{
	reg_assign(reserved1,value);
}

void ZynqFcuWrMem::outreserved1( ZynqFcuWr_reg value )
{
	out_assign(reserved1,value);
}

void ZynqFcuWrMem::outreserved1()
{
	out_member(reserved1);
}


void ZynqFcuWrMem::setcomm_test( ZynqFcuWr_reg value )
{
	reg_assign(comm_test,value);
}

void ZynqFcuWrMem::outcomm_test( ZynqFcuWr_reg value )
{
	out_assign(comm_test,value);
}

void ZynqFcuWrMem::outcomm_test()
{
	out_member(comm_test);
}


void ZynqFcuWrMem::settrace_wdone_clr( ZynqFcuWr_reg value )
{
	reg_assign(trace_wdone_clr,value);
}

void ZynqFcuWrMem::outtrace_wdone_clr( ZynqFcuWr_reg value )
{
	out_assign(trace_wdone_clr,value);
}

void ZynqFcuWrMem::outtrace_wdone_clr()
{
	out_member(trace_wdone_clr);
}


void ZynqFcuWrMem::settrace_wbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign(trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outtrace_wbaseaddr( ZynqFcuWr_reg value )
{
	out_assign(trace_wbaseaddr,value);
}

void ZynqFcuWrMem::outtrace_wbaseaddr()
{
	out_member(trace_wbaseaddr);
}


void ZynqFcuWrMem::settrace_dat_len( ZynqFcuWr_reg value )
{
	reg_assign(trace_dat_len,value);
}

void ZynqFcuWrMem::outtrace_dat_len( ZynqFcuWr_reg value )
{
	out_assign(trace_dat_len,value);
}

void ZynqFcuWrMem::outtrace_dat_len()
{
	out_member(trace_dat_len);
}


void ZynqFcuWrMem::setdeinterweave_chx_lax_rdat_num( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_chx_lax_rdat_num,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_lax_rdat_num( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_chx_lax_rdat_num,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_lax_rdat_num()
{
	out_member(deinterweave_chx_lax_rdat_num);
}


void ZynqFcuWrMem::setdeinterweave_chx_raddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_chx_raddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_raddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_chx_raddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_raddr()
{
	out_member(deinterweave_chx_raddr);
}


void ZynqFcuWrMem::setdeinterweave_cha_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_cha_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_cha_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_cha_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_cha_waddr()
{
	out_member(deinterweave_cha_waddr);
}


void ZynqFcuWrMem::setdeinterweave_chb_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_chb_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chb_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_chb_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chb_waddr()
{
	out_member(deinterweave_chb_waddr);
}


void ZynqFcuWrMem::setdeinterweave_chc_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_chc_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chc_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_chc_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chc_waddr()
{
	out_member(deinterweave_chc_waddr);
}


void ZynqFcuWrMem::setdeinterweave_chd_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_chd_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chd_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_chd_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_chd_waddr()
{
	out_member(deinterweave_chd_waddr);
}


void ZynqFcuWrMem::setdeinterweave_decode_cha_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_cha_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_cha_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_cha_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_cha_waddr()
{
	out_member(deinterweave_decode_cha_waddr);
}


void ZynqFcuWrMem::setdeinterweave_decode_chb_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_chb_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chb_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_chb_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chb_waddr()
{
	out_member(deinterweave_decode_chb_waddr);
}


void ZynqFcuWrMem::setdeinterweave_decode_chc_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_chc_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chc_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_chc_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chc_waddr()
{
	out_member(deinterweave_decode_chc_waddr);
}


void ZynqFcuWrMem::setdeinterweave_decode_chd_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_chd_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chd_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_chd_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chd_waddr()
{
	out_member(deinterweave_decode_chd_waddr);
}


void ZynqFcuWrMem::setdeinterweave_lax_raddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_lax_raddr,value);
}

void ZynqFcuWrMem::outdeinterweave_lax_raddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_lax_raddr,value);
}

void ZynqFcuWrMem::outdeinterweave_lax_raddr()
{
	out_member(deinterweave_lax_raddr);
}


void ZynqFcuWrMem::setdeinterweave_la0_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la0_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la0_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la0_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la0_waddr()
{
	out_member(deinterweave_la0_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la1_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la1_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la1_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la1_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la1_waddr()
{
	out_member(deinterweave_la1_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la2_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la2_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la2_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la2_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la2_waddr()
{
	out_member(deinterweave_la2_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la3_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la3_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la3_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la3_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la3_waddr()
{
	out_member(deinterweave_la3_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la4_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la4_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la4_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la4_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la4_waddr()
{
	out_member(deinterweave_la4_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la5_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la5_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la5_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la5_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la5_waddr()
{
	out_member(deinterweave_la5_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la6_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la6_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la6_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la6_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la6_waddr()
{
	out_member(deinterweave_la6_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la7_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la7_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la7_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la7_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la7_waddr()
{
	out_member(deinterweave_la7_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la8_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la8_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la8_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la8_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la8_waddr()
{
	out_member(deinterweave_la8_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la9_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la9_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la9_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la9_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la9_waddr()
{
	out_member(deinterweave_la9_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la10_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la10_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la10_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la10_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la10_waddr()
{
	out_member(deinterweave_la10_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la11_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la11_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la11_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la11_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la11_waddr()
{
	out_member(deinterweave_la11_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la12_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la12_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la12_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la12_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la12_waddr()
{
	out_member(deinterweave_la12_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la13_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la13_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la13_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la13_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la13_waddr()
{
	out_member(deinterweave_la13_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la14_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la14_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la14_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la14_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la14_waddr()
{
	out_member(deinterweave_la14_waddr);
}


void ZynqFcuWrMem::setdeinterweave_la15_waddr( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_la15_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la15_waddr( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_la15_waddr,value);
}

void ZynqFcuWrMem::outdeinterweave_la15_waddr()
{
	out_member(deinterweave_la15_waddr);
}


void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_h,value);
}
void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_l,value);
}
void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_h,value);
}
void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_l,value);
}

void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp,payload,value);
}

void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_h,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_cmp_a_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_h,idleV);
}
void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_l,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_cmp_a_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_a_level_l,idleV);
}
void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_h,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_cmp_b_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_h,idleV);
}
void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_l,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_cmp_b_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp,cmp_b_level_l,idleV);
}

void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp,payload,value);
}

void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp()
{
	out_member(decode_a_or_b_channel_cmp);
}


void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_h,value);
}
void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_l,value);
}
void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_h,value);
}
void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_l,value);
}

void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp,payload,value);
}

void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_h,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_cmp_c_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_h,idleV);
}
void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_l,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_cmp_c_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_c_level_l,idleV);
}
void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_h,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_cmp_d_level_h( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_h,idleV);
}
void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_l,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_cmp_d_level_l( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp,cmp_d_level_l,idleV);
}

void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp,payload,value);
}

void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp()
{
	out_member(decode_c_or_d_channel_cmp);
}


void ZynqFcuWrMem::setdeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg1,deinterweave_source_type,value);
}
void ZynqFcuWrMem::setdeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg1,decode_en,value);
}
void ZynqFcuWrMem::setdeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg1,decode_en_sec,value);
}

void ZynqFcuWrMem::setdeinterweave_property_cfg1( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg1,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg1,deinterweave_source_type,value);
}
void ZynqFcuWrMem::pulsedeinterweave_property_cfg1_deinterweave_source_type( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(deinterweave_property_cfg1,deinterweave_source_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(deinterweave_property_cfg1,deinterweave_source_type,idleV);
}
void ZynqFcuWrMem::outdeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg1,decode_en,value);
}
void ZynqFcuWrMem::pulsedeinterweave_property_cfg1_decode_en( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(deinterweave_property_cfg1,decode_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(deinterweave_property_cfg1,decode_en,idleV);
}
void ZynqFcuWrMem::outdeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg1,decode_en_sec,value);
}
void ZynqFcuWrMem::pulsedeinterweave_property_cfg1_decode_en_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(deinterweave_property_cfg1,decode_en_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(deinterweave_property_cfg1,decode_en_sec,idleV);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg1( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg1,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg1()
{
	out_member(deinterweave_property_cfg1);
}


void ZynqFcuWrMem::setdeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg2,deinterweave_rstart,value);
}

void ZynqFcuWrMem::setdeinterweave_property_cfg2( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg2,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg2,deinterweave_rstart,value);
}
void ZynqFcuWrMem::pulsedeinterweave_property_cfg2_deinterweave_rstart( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(deinterweave_property_cfg2,deinterweave_rstart,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(deinterweave_property_cfg2,deinterweave_rstart,idleV);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg2( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg2,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg2()
{
	out_member(deinterweave_property_cfg2);
}


void ZynqFcuWrMem::setdeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg3,deinterweave_done_clr,value);
}

void ZynqFcuWrMem::setdeinterweave_property_cfg3( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg3,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg3,deinterweave_done_clr,value);
}
void ZynqFcuWrMem::pulsedeinterweave_property_cfg3_deinterweave_done_clr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(deinterweave_property_cfg3,deinterweave_done_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(deinterweave_property_cfg3,deinterweave_done_clr,idleV);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg3( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg3,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg3()
{
	out_member(deinterweave_property_cfg3);
}


void ZynqFcuWrMem::setdeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg4,channel_mode,value);
}

void ZynqFcuWrMem::setdeinterweave_property_cfg4( ZynqFcuWr_reg value )
{
	reg_assign_field(deinterweave_property_cfg4,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg4,channel_mode,value);
}
void ZynqFcuWrMem::pulsedeinterweave_property_cfg4_channel_mode( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(deinterweave_property_cfg4,channel_mode,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(deinterweave_property_cfg4,channel_mode,idleV);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg4( ZynqFcuWr_reg value )
{
	out_assign_field(deinterweave_property_cfg4,payload,value);
}

void ZynqFcuWrMem::outdeinterweave_property_cfg4()
{
	out_member(deinterweave_property_cfg4);
}


void ZynqFcuWrMem::setfpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg1,fpga_rdat_num,value);
}
void ZynqFcuWrMem::setfpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg1,fpga_rdat_type,value);
}

void ZynqFcuWrMem::setfpga_rdat_cfg1( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg1,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg1,fpga_rdat_num,value);
}
void ZynqFcuWrMem::pulsefpga_rdat_cfg1_fpga_rdat_num( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(fpga_rdat_cfg1,fpga_rdat_num,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(fpga_rdat_cfg1,fpga_rdat_num,idleV);
}
void ZynqFcuWrMem::outfpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg1,fpga_rdat_type,value);
}
void ZynqFcuWrMem::pulsefpga_rdat_cfg1_fpga_rdat_type( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(fpga_rdat_cfg1,fpga_rdat_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(fpga_rdat_cfg1,fpga_rdat_type,idleV);
}

void ZynqFcuWrMem::outfpga_rdat_cfg1( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg1,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg1()
{
	out_member(fpga_rdat_cfg1);
}


void ZynqFcuWrMem::setfpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg2,fpga_rdat_start,value);
}

void ZynqFcuWrMem::setfpga_rdat_cfg2( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg2,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg2,fpga_rdat_start,value);
}
void ZynqFcuWrMem::pulsefpga_rdat_cfg2_fpga_rdat_start( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(fpga_rdat_cfg2,fpga_rdat_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(fpga_rdat_cfg2,fpga_rdat_start,idleV);
}

void ZynqFcuWrMem::outfpga_rdat_cfg2( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg2,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg2()
{
	out_member(fpga_rdat_cfg2);
}


void ZynqFcuWrMem::setfpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg3,fpga_rdat_done_clr,value);
}

void ZynqFcuWrMem::setfpga_rdat_cfg3( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg3,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg3,fpga_rdat_done_clr,value);
}
void ZynqFcuWrMem::pulsefpga_rdat_cfg3_fpga_rdat_done_clr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(fpga_rdat_cfg3,fpga_rdat_done_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(fpga_rdat_cfg3,fpga_rdat_done_clr,idleV);
}

void ZynqFcuWrMem::outfpga_rdat_cfg3( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg3,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg3()
{
	out_member(fpga_rdat_cfg3);
}


void ZynqFcuWrMem::setfpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg4,exdat_rbaseaddr,value);
}

void ZynqFcuWrMem::setfpga_rdat_cfg4( ZynqFcuWr_reg value )
{
	reg_assign_field(fpga_rdat_cfg4,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg4,exdat_rbaseaddr,value);
}
void ZynqFcuWrMem::pulsefpga_rdat_cfg4_exdat_rbaseaddr( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(fpga_rdat_cfg4,exdat_rbaseaddr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(fpga_rdat_cfg4,exdat_rbaseaddr,idleV);
}

void ZynqFcuWrMem::outfpga_rdat_cfg4( ZynqFcuWr_reg value )
{
	out_assign_field(fpga_rdat_cfg4,payload,value);
}

void ZynqFcuWrMem::outfpga_rdat_cfg4()
{
	out_member(fpga_rdat_cfg4);
}


void ZynqFcuWrMem::setreserved6( ZynqFcuWr_reg value )
{
	reg_assign(reserved6,value);
}

void ZynqFcuWrMem::outreserved6( ZynqFcuWr_reg value )
{
	out_assign(reserved6,value);
}

void ZynqFcuWrMem::outreserved6()
{
	out_member(reserved6);
}


ZynqFcuWr_reg ZynqFcuWrMem::inreserved6()
{
	reg_in(reserved6);
	return reserved6;
}


void ZynqFcuWrMem::setreserved7( ZynqFcuWr_reg value )
{
	reg_assign(reserved7,value);
}

void ZynqFcuWrMem::outreserved7( ZynqFcuWr_reg value )
{
	out_assign(reserved7,value);
}

void ZynqFcuWrMem::outreserved7()
{
	out_member(reserved7);
}


ZynqFcuWr_reg ZynqFcuWrMem::inreserved7()
{
	reg_in(reserved7);
	return reserved7;
}


void ZynqFcuWrMem::setreserved8( ZynqFcuWr_reg value )
{
	reg_assign(reserved8,value);
}

void ZynqFcuWrMem::outreserved8( ZynqFcuWr_reg value )
{
	out_assign(reserved8,value);
}

void ZynqFcuWrMem::outreserved8()
{
	out_member(reserved8);
}


ZynqFcuWr_reg ZynqFcuWrMem::inreserved8()
{
	reg_in(reserved8);
	return reserved8;
}


void ZynqFcuWrMem::setreserved9( ZynqFcuWr_reg value )
{
	reg_assign(reserved9,value);
}

void ZynqFcuWrMem::outreserved9( ZynqFcuWr_reg value )
{
	out_assign(reserved9,value);
}

void ZynqFcuWrMem::outreserved9()
{
	out_member(reserved9);
}


ZynqFcuWr_reg ZynqFcuWrMem::inreserved9()
{
	reg_in(reserved9);
	return reserved9;
}


void ZynqFcuWrMem::setreserved10( ZynqFcuWr_reg value )
{
	reg_assign(reserved10,value);
}

void ZynqFcuWrMem::outreserved10( ZynqFcuWr_reg value )
{
	out_assign(reserved10,value);
}

void ZynqFcuWrMem::outreserved10()
{
	out_member(reserved10);
}


ZynqFcuWr_reg ZynqFcuWrMem::inreserved10()
{
	reg_in(reserved10);
	return reserved10;
}


void ZynqFcuWrMem::setreserved11( ZynqFcuWr_reg value )
{
	reg_assign(reserved11,value);
}

void ZynqFcuWrMem::outreserved11( ZynqFcuWr_reg value )
{
	out_assign(reserved11,value);
}

void ZynqFcuWrMem::outreserved11()
{
	out_member(reserved11);
}


ZynqFcuWr_reg ZynqFcuWrMem::inreserved11()
{
	reg_in(reserved11);
	return reserved11;
}


void ZynqFcuWrMem::setcompress_type_compress_type( ZynqFcuWr_reg value )
{
	reg_assign_field(compress_type,compress_type,value);
}

void ZynqFcuWrMem::setcompress_type( ZynqFcuWr_reg value )
{
	reg_assign_field(compress_type,payload,value);
}

void ZynqFcuWrMem::outcompress_type_compress_type( ZynqFcuWr_reg value )
{
	out_assign_field(compress_type,compress_type,value);
}
void ZynqFcuWrMem::pulsecompress_type_compress_type( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(compress_type,compress_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(compress_type,compress_type,idleV);
}

void ZynqFcuWrMem::outcompress_type( ZynqFcuWr_reg value )
{
	out_assign_field(compress_type,payload,value);
}

void ZynqFcuWrMem::outcompress_type()
{
	out_member(compress_type);
}


void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_h_sec,value);
}
void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_l_sec,value);
}
void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_h_sec,value);
}
void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_l_sec,value);
}

void ZynqFcuWrMem::setdecode_a_or_b_channel_cmp_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_a_or_b_channel_cmp_sec,payload,value);
}

void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_h_sec,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_h_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_h_sec,idleV);
}
void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_l_sec,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_l_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_a_level_l_sec,idleV);
}
void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_h_sec,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_h_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_h_sec,idleV);
}
void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_l_sec,value);
}
void ZynqFcuWrMem::pulsedecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_l_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_a_or_b_channel_cmp_sec,cmp_b_level_l_sec,idleV);
}

void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_a_or_b_channel_cmp_sec,payload,value);
}

void ZynqFcuWrMem::outdecode_a_or_b_channel_cmp_sec()
{
	out_member(decode_a_or_b_channel_cmp_sec);
}


void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_h_sec,value);
}
void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_l_sec,value);
}
void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_h_sec,value);
}
void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_l_sec,value);
}

void ZynqFcuWrMem::setdecode_c_or_d_channel_cmp_sec( ZynqFcuWr_reg value )
{
	reg_assign_field(decode_c_or_d_channel_cmp_sec,payload,value);
}

void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_h_sec,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_h_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_h_sec,idleV);
}
void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_l_sec,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_l_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_c_level_l_sec,idleV);
}
void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_h_sec,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_h_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_h_sec,idleV);
}
void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_l_sec,value);
}
void ZynqFcuWrMem::pulsedecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( ZynqFcuWr_reg activeV, ZynqFcuWr_reg idleV, int timeus )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_l_sec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(decode_c_or_d_channel_cmp_sec,cmp_d_level_l_sec,idleV);
}

void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_sec( ZynqFcuWr_reg value )
{
	out_assign_field(decode_c_or_d_channel_cmp_sec,payload,value);
}

void ZynqFcuWrMem::outdecode_c_or_d_channel_cmp_sec()
{
	out_member(decode_c_or_d_channel_cmp_sec);
}


void ZynqFcuWrMem::setdeinterweave_decode_cha_waddr_sec( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_cha_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_cha_waddr_sec( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_cha_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_cha_waddr_sec()
{
	out_member(deinterweave_decode_cha_waddr_sec);
}


void ZynqFcuWrMem::setdeinterweave_decode_chb_waddr_sec( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_chb_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chb_waddr_sec( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_chb_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chb_waddr_sec()
{
	out_member(deinterweave_decode_chb_waddr_sec);
}


void ZynqFcuWrMem::setdeinterweave_decode_chc_waddr_sec( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_chc_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chc_waddr_sec( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_chc_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chc_waddr_sec()
{
	out_member(deinterweave_decode_chc_waddr_sec);
}


void ZynqFcuWrMem::setdeinterweave_decode_chd_waddr_sec( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_decode_chd_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chd_waddr_sec( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_decode_chd_waddr_sec,value);
}

void ZynqFcuWrMem::outdeinterweave_decode_chd_waddr_sec()
{
	out_member(deinterweave_decode_chd_waddr_sec);
}


void ZynqFcuWrMem::setdeinterweave_chx_raddr_end( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_chx_raddr_end,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_raddr_end( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_chx_raddr_end,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_raddr_end()
{
	out_member(deinterweave_chx_raddr_end);
}


void ZynqFcuWrMem::setdeinterweave_lax_raddr_end( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_lax_raddr_end,value);
}

void ZynqFcuWrMem::outdeinterweave_lax_raddr_end( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_lax_raddr_end,value);
}

void ZynqFcuWrMem::outdeinterweave_lax_raddr_end()
{
	out_member(deinterweave_lax_raddr_end);
}


void ZynqFcuWrMem::setdeinterweave_chx_raddr_base( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_chx_raddr_base,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_raddr_base( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_chx_raddr_base,value);
}

void ZynqFcuWrMem::outdeinterweave_chx_raddr_base()
{
	out_member(deinterweave_chx_raddr_base);
}


void ZynqFcuWrMem::setdeinterweave_lax_raddr_base( ZynqFcuWr_reg value )
{
	reg_assign(deinterweave_lax_raddr_base,value);
}

void ZynqFcuWrMem::outdeinterweave_lax_raddr_base( ZynqFcuWr_reg value )
{
	out_assign(deinterweave_lax_raddr_base,value);
}

void ZynqFcuWrMem::outdeinterweave_lax_raddr_base()
{
	out_member(deinterweave_lax_raddr_base);
}


void ZynqFcuWrMem::setdbg_cfg1( ZynqFcuWr_reg value )
{
	reg_assign(dbg_cfg1,value);
}

void ZynqFcuWrMem::outdbg_cfg1( ZynqFcuWr_reg value )
{
	out_assign(dbg_cfg1,value);
}

void ZynqFcuWrMem::outdbg_cfg1()
{
	out_member(dbg_cfg1);
}


cache_addr ZynqFcuWrMem::addr_trace_req(){ return 0x0018+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_eye_trace_wbaseaddr(){ return 0x001c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_roll_ana_trace_wbaseaddr(){ return 0x0020+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_roll_la_trace_wbaseaddr(){ return 0x0024+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_roll_ana_trace_waddr_end(){ return 0x0028+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_roll_la_trace_waddr_end(){ return 0x002c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_roll_trace_waddr_rst(){ return 0x0030+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_search_result_wbaseaddr(){ return 0x0034+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_search_result_wdone_clr(){ return 0x0038+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_ana_main_trace_wbaseaddr(){ return 0x003c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_la_main_trace_wbaseaddr(){ return 0x0040+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_ana_zoom_trace_wbaseaddr(){ return 0x0044+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_la_zoom_trace_wbaseaddr(){ return 0x0048+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_reserved1(){ return 0x0800+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_comm_test(){ return 0x0804+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_trace_wdone_clr(){ return 0x0808+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_trace_wbaseaddr(){ return 0x080C+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_trace_dat_len(){ return 0x0810+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_chx_lax_rdat_num(){ return 0x0814+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_chx_raddr(){ return 0x0818+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_cha_waddr(){ return 0x081c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_chb_waddr(){ return 0x0820+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_chc_waddr(){ return 0x0824+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_chd_waddr(){ return 0x0828+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_decode_cha_waddr(){ return 0x082c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_decode_chb_waddr(){ return 0x0830+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_decode_chc_waddr(){ return 0x0834+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_decode_chd_waddr(){ return 0x0838+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_lax_raddr(){ return 0x083c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la0_waddr(){ return 0x0840+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la1_waddr(){ return 0x0844+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la2_waddr(){ return 0x0848+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_la3_waddr(){ return 0x084c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la4_waddr(){ return 0x0850+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la5_waddr(){ return 0x0854+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la6_waddr(){ return 0x0858+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_la7_waddr(){ return 0x085c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la8_waddr(){ return 0x0860+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la9_waddr(){ return 0x0864+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la10_waddr(){ return 0x0868+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_la11_waddr(){ return 0x086c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la12_waddr(){ return 0x0870+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la13_waddr(){ return 0x0874+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_la14_waddr(){ return 0x0878+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_la15_waddr(){ return 0x087c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_decode_a_or_b_channel_cmp(){ return 0x0880+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_decode_c_or_d_channel_cmp(){ return 0x0884+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_property_cfg1(){ return 0x0888+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_property_cfg2(){ return 0x088c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_property_cfg3(){ return 0x0890+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_property_cfg4(){ return 0x0894+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_fpga_rdat_cfg1(){ return 0x0898+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_fpga_rdat_cfg2(){ return 0x089c+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_fpga_rdat_cfg3(){ return 0x08a0+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_fpga_rdat_cfg4(){ return 0x08a4+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_reserved6(){ return 0x08a8+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_reserved7(){ return 0x08ac+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_reserved8(){ return 0x08b0+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_reserved9(){ return 0x08b4+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_reserved10(){ return 0x08b8+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_reserved11(){ return 0x08bc+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_compress_type(){ return 0x08c0+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_decode_a_or_b_channel_cmp_sec(){ return 0x08c4+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_decode_c_or_d_channel_cmp_sec(){ return 0x08c8+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_decode_cha_waddr_sec(){ return 0x08cc+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_decode_chb_waddr_sec(){ return 0x08d0+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_decode_chc_waddr_sec(){ return 0x08d4+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_decode_chd_waddr_sec(){ return 0x08d8+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_deinterweave_chx_raddr_end(){ return 0x08dc+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_lax_raddr_end(){ return 0x08e0+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_chx_raddr_base(){ return 0x08e4+mAddrBase; }
cache_addr ZynqFcuWrMem::addr_deinterweave_lax_raddr_base(){ return 0x08e8+mAddrBase; }

cache_addr ZynqFcuWrMem::addr_dbg_cfg1(){ return 0x0050+mAddrBase; }


