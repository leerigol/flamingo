#ifndef _K7MFCU_IC_H_
#define _K7MFCU_IC_H_

#define K7mFcu_reg unsigned int

union K7mFcu_LA_LEVEL1{
	struct{
	K7mFcu_reg LA_LEVEL1:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_LA_LEVEL2{
	struct{
	K7mFcu_reg LA_LEVEL2:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_FG1_OFFSET{
	struct{
	K7mFcu_reg FG1_OFFSET:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_FG1_REF{
	struct{
	K7mFcu_reg FG1_REF:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_FG2_OFFSET{
	struct{
	K7mFcu_reg FG2_OFFSET:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_FG2_REF{
	struct{
	K7mFcu_reg FG2_REF:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE1_OFFSET{
	struct{
	K7mFcu_reg PROBE1_OFFSET:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE2_OFFSET{
	struct{
	K7mFcu_reg PROBE2_OFFSET:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE3_OFFSET{
	struct{
	K7mFcu_reg PROBE3_OFFSET:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE4_OFFSET{
	struct{
	K7mFcu_reg PROBE4_OFFSET:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_OFFSET1{
	struct{
	K7mFcu_reg OFFSET1:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_OFFSET2{
	struct{
	K7mFcu_reg OFFSET2:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_OFFSET3{
	struct{
	K7mFcu_reg OFFSET3:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_OFFSET4{
	struct{
	K7mFcu_reg OFFSET4:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_EXT_LEVEL{
	struct{
	K7mFcu_reg EXT_LEVEL:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_RESERVE{
	struct{
	K7mFcu_reg RESERVE:16;
	};
	K7mFcu_reg payload;
};

union K7mFcu_DAC_CHN_TIME{
	struct{
	K7mFcu_reg holdtime:10;
	};
	K7mFcu_reg payload;
};

union K7mFcu_MISC_PROBE_EN{
	struct{
	K7mFcu_reg en:1;
	K7mFcu_reg sel_1K_10K:1;
	};
	K7mFcu_reg payload;
};

union K7mFcu_MISC_TRIGOUT_SEL{
	struct{
	K7mFcu_reg sel:3;
	};
	K7mFcu_reg payload;
};

union K7mFcu_CFG_1WIRE{
	struct{
	K7mFcu_reg bit_start:8;
	K7mFcu_reg bit_tx:8;
	K7mFcu_reg bit_rx:8;
	K7mFcu_reg bit_period:8;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE_CH1{
	struct{
	K7mFcu_reg rd_data:8;
	K7mFcu_reg rd_data_vld:1;
	K7mFcu_reg rx_len:7;
	K7mFcu_reg wr_data:8;

	K7mFcu_reg wr_data_en:1;
	K7mFcu_reg _pad0:5;
	K7mFcu_reg cmd_type:1;
	K7mFcu_reg clr:1;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE_CH2{
	struct{
	K7mFcu_reg rd_data:8;
	K7mFcu_reg rd_data_vld:1;
	K7mFcu_reg rx_len:7;
	K7mFcu_reg wr_data:8;

	K7mFcu_reg wr_data_en:1;
	K7mFcu_reg _pad0:5;
	K7mFcu_reg cmd_type:1;
	K7mFcu_reg clr:1;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE_CH3{
	struct{
	K7mFcu_reg rd_data:8;
	K7mFcu_reg rd_data_vld:1;
	K7mFcu_reg rx_len:7;
	K7mFcu_reg wr_data:8;

	K7mFcu_reg wr_data_en:1;
	K7mFcu_reg _pad0:5;
	K7mFcu_reg cmd_type:1;
	K7mFcu_reg clr:1;
	};
	K7mFcu_reg payload;
};

union K7mFcu_PROBE_CH4{
	struct{
	K7mFcu_reg rd_data:8;
	K7mFcu_reg rd_data_vld:1;
	K7mFcu_reg rx_len:7;
	K7mFcu_reg wr_data:8;

	K7mFcu_reg wr_data_en:1;
	K7mFcu_reg _pad0:5;
	K7mFcu_reg cmd_type:1;
	K7mFcu_reg clr:1;
	};
	K7mFcu_reg payload;
};

union K7mFcu_DG_CFG{
	struct{
	K7mFcu_reg data:16;
	K7mFcu_reg addr:16;
	};
	K7mFcu_reg payload;
};

struct K7mFcuMemProxy {
	K7mFcu_LA_LEVEL1 LA_LEVEL1; 
	K7mFcu_LA_LEVEL2 LA_LEVEL2; 
	K7mFcu_FG1_OFFSET FG1_OFFSET; 
	K7mFcu_FG1_REF FG1_REF; 

	K7mFcu_FG2_OFFSET FG2_OFFSET; 
	K7mFcu_FG2_REF FG2_REF; 
	K7mFcu_PROBE1_OFFSET PROBE1_OFFSET; 
	K7mFcu_PROBE2_OFFSET PROBE2_OFFSET; 

	K7mFcu_PROBE3_OFFSET PROBE3_OFFSET; 
	K7mFcu_PROBE4_OFFSET PROBE4_OFFSET; 
	K7mFcu_OFFSET1 OFFSET1; 
	K7mFcu_OFFSET2 OFFSET2; 

	K7mFcu_OFFSET3 OFFSET3; 
	K7mFcu_OFFSET4 OFFSET4; 
	K7mFcu_EXT_LEVEL EXT_LEVEL; 
	K7mFcu_RESERVE RESERVE; 

	K7mFcu_DAC_CHN_TIME DAC_CHN_TIME; 
	K7mFcu_MISC_PROBE_EN MISC_PROBE_EN; 
	K7mFcu_MISC_TRIGOUT_SEL MISC_TRIGOUT_SEL; 
	K7mFcu_reg VERSION;

	K7mFcu_CFG_1WIRE CFG_1WIRE; 
	K7mFcu_PROBE_CH1 PROBE_CH1; 
	K7mFcu_PROBE_CH2 PROBE_CH2; 
	K7mFcu_PROBE_CH3 PROBE_CH3; 

	K7mFcu_PROBE_CH4 PROBE_CH4; 
	K7mFcu_reg PROBE_WAIT;
	K7mFcu_DG_CFG DG_CFG; 
	K7mFcu_reg DG_RD;

	K7mFcu_reg dbg_search_event_num_latch;
	K7mFcu_reg dbg_search_frame_lenth_latch;
	void assignLA_LEVEL1_LA_LEVEL1( K7mFcu_reg value  );

	void assignLA_LEVEL1( K7mFcu_reg value );


	K7mFcu_reg getLA_LEVEL1_LA_LEVEL1(  );

	K7mFcu_reg getLA_LEVEL1(  );

	void assignLA_LEVEL2_LA_LEVEL2( K7mFcu_reg value  );

	void assignLA_LEVEL2( K7mFcu_reg value );


	K7mFcu_reg getLA_LEVEL2_LA_LEVEL2(  );

	K7mFcu_reg getLA_LEVEL2(  );

	void assignFG1_OFFSET_FG1_OFFSET( K7mFcu_reg value  );

	void assignFG1_OFFSET( K7mFcu_reg value );


	K7mFcu_reg getFG1_OFFSET_FG1_OFFSET(  );

	K7mFcu_reg getFG1_OFFSET(  );

	void assignFG1_REF_FG1_REF( K7mFcu_reg value  );

	void assignFG1_REF( K7mFcu_reg value );


	K7mFcu_reg getFG1_REF_FG1_REF(  );

	K7mFcu_reg getFG1_REF(  );

	void assignFG2_OFFSET_FG2_OFFSET( K7mFcu_reg value  );

	void assignFG2_OFFSET( K7mFcu_reg value );


	K7mFcu_reg getFG2_OFFSET_FG2_OFFSET(  );

	K7mFcu_reg getFG2_OFFSET(  );

	void assignFG2_REF_FG2_REF( K7mFcu_reg value  );

	void assignFG2_REF( K7mFcu_reg value );


	K7mFcu_reg getFG2_REF_FG2_REF(  );

	K7mFcu_reg getFG2_REF(  );

	void assignPROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg value  );

	void assignPROBE1_OFFSET( K7mFcu_reg value );


	K7mFcu_reg getPROBE1_OFFSET_PROBE1_OFFSET(  );

	K7mFcu_reg getPROBE1_OFFSET(  );

	void assignPROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg value  );

	void assignPROBE2_OFFSET( K7mFcu_reg value );


	K7mFcu_reg getPROBE2_OFFSET_PROBE2_OFFSET(  );

	K7mFcu_reg getPROBE2_OFFSET(  );

	void assignPROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg value  );

	void assignPROBE3_OFFSET( K7mFcu_reg value );


	K7mFcu_reg getPROBE3_OFFSET_PROBE3_OFFSET(  );

	K7mFcu_reg getPROBE3_OFFSET(  );

	void assignPROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg value  );

	void assignPROBE4_OFFSET( K7mFcu_reg value );


	K7mFcu_reg getPROBE4_OFFSET_PROBE4_OFFSET(  );

	K7mFcu_reg getPROBE4_OFFSET(  );

	void assignOFFSET1_OFFSET1( K7mFcu_reg value  );

	void assignOFFSET1( K7mFcu_reg value );


	K7mFcu_reg getOFFSET1_OFFSET1(  );

	K7mFcu_reg getOFFSET1(  );

	void assignOFFSET2_OFFSET2( K7mFcu_reg value  );

	void assignOFFSET2( K7mFcu_reg value );


	K7mFcu_reg getOFFSET2_OFFSET2(  );

	K7mFcu_reg getOFFSET2(  );

	void assignOFFSET3_OFFSET3( K7mFcu_reg value  );

	void assignOFFSET3( K7mFcu_reg value );


	K7mFcu_reg getOFFSET3_OFFSET3(  );

	K7mFcu_reg getOFFSET3(  );

	void assignOFFSET4_OFFSET4( K7mFcu_reg value  );

	void assignOFFSET4( K7mFcu_reg value );


	K7mFcu_reg getOFFSET4_OFFSET4(  );

	K7mFcu_reg getOFFSET4(  );

	void assignEXT_LEVEL_EXT_LEVEL( K7mFcu_reg value  );

	void assignEXT_LEVEL( K7mFcu_reg value );


	K7mFcu_reg getEXT_LEVEL_EXT_LEVEL(  );

	K7mFcu_reg getEXT_LEVEL(  );

	void assignRESERVE_RESERVE( K7mFcu_reg value  );

	void assignRESERVE( K7mFcu_reg value );


	K7mFcu_reg getRESERVE_RESERVE(  );

	K7mFcu_reg getRESERVE(  );

	void assignDAC_CHN_TIME_holdtime( K7mFcu_reg value  );

	void assignDAC_CHN_TIME( K7mFcu_reg value );


	K7mFcu_reg getDAC_CHN_TIME_holdtime(  );

	K7mFcu_reg getDAC_CHN_TIME(  );

	void assignMISC_PROBE_EN_en( K7mFcu_reg value  );
	void assignMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg value  );

	void assignMISC_PROBE_EN( K7mFcu_reg value );


	K7mFcu_reg getMISC_PROBE_EN_en(  );
	K7mFcu_reg getMISC_PROBE_EN_sel_1K_10K(  );

	K7mFcu_reg getMISC_PROBE_EN(  );

	void assignMISC_TRIGOUT_SEL_sel( K7mFcu_reg value  );

	void assignMISC_TRIGOUT_SEL( K7mFcu_reg value );


	K7mFcu_reg getMISC_TRIGOUT_SEL_sel(  );

	K7mFcu_reg getMISC_TRIGOUT_SEL(  );

	K7mFcu_reg getVERSION(  );

	void assignCFG_1WIRE_bit_start( K7mFcu_reg value  );
	void assignCFG_1WIRE_bit_tx( K7mFcu_reg value  );
	void assignCFG_1WIRE_bit_rx( K7mFcu_reg value  );
	void assignCFG_1WIRE_bit_period( K7mFcu_reg value  );

	void assignCFG_1WIRE( K7mFcu_reg value );


	K7mFcu_reg getCFG_1WIRE_bit_start(  );
	K7mFcu_reg getCFG_1WIRE_bit_tx(  );
	K7mFcu_reg getCFG_1WIRE_bit_rx(  );
	K7mFcu_reg getCFG_1WIRE_bit_period(  );

	K7mFcu_reg getCFG_1WIRE(  );

	void assignPROBE_CH1_rd_data( K7mFcu_reg value  );
	void assignPROBE_CH1_rd_data_vld( K7mFcu_reg value  );
	void assignPROBE_CH1_rx_len( K7mFcu_reg value  );
	void assignPROBE_CH1_wr_data( K7mFcu_reg value  );
	void assignPROBE_CH1_wr_data_en( K7mFcu_reg value  );
	void assignPROBE_CH1_cmd_type( K7mFcu_reg value  );
	void assignPROBE_CH1_clr( K7mFcu_reg value  );

	void assignPROBE_CH1( K7mFcu_reg value );


	K7mFcu_reg getPROBE_CH1_rd_data(  );
	K7mFcu_reg getPROBE_CH1_rd_data_vld(  );
	K7mFcu_reg getPROBE_CH1_rx_len(  );
	K7mFcu_reg getPROBE_CH1_wr_data(  );
	K7mFcu_reg getPROBE_CH1_wr_data_en(  );
	K7mFcu_reg getPROBE_CH1_cmd_type(  );
	K7mFcu_reg getPROBE_CH1_clr(  );

	K7mFcu_reg getPROBE_CH1(  );

	void assignPROBE_CH2_rd_data( K7mFcu_reg value  );
	void assignPROBE_CH2_rd_data_vld( K7mFcu_reg value  );
	void assignPROBE_CH2_rx_len( K7mFcu_reg value  );
	void assignPROBE_CH2_wr_data( K7mFcu_reg value  );
	void assignPROBE_CH2_wr_data_en( K7mFcu_reg value  );
	void assignPROBE_CH2_cmd_type( K7mFcu_reg value  );
	void assignPROBE_CH2_clr( K7mFcu_reg value  );

	void assignPROBE_CH2( K7mFcu_reg value );


	K7mFcu_reg getPROBE_CH2_rd_data(  );
	K7mFcu_reg getPROBE_CH2_rd_data_vld(  );
	K7mFcu_reg getPROBE_CH2_rx_len(  );
	K7mFcu_reg getPROBE_CH2_wr_data(  );
	K7mFcu_reg getPROBE_CH2_wr_data_en(  );
	K7mFcu_reg getPROBE_CH2_cmd_type(  );
	K7mFcu_reg getPROBE_CH2_clr(  );

	K7mFcu_reg getPROBE_CH2(  );

	void assignPROBE_CH3_rd_data( K7mFcu_reg value  );
	void assignPROBE_CH3_rd_data_vld( K7mFcu_reg value  );
	void assignPROBE_CH3_rx_len( K7mFcu_reg value  );
	void assignPROBE_CH3_wr_data( K7mFcu_reg value  );
	void assignPROBE_CH3_wr_data_en( K7mFcu_reg value  );
	void assignPROBE_CH3_cmd_type( K7mFcu_reg value  );
	void assignPROBE_CH3_clr( K7mFcu_reg value  );

	void assignPROBE_CH3( K7mFcu_reg value );


	K7mFcu_reg getPROBE_CH3_rd_data(  );
	K7mFcu_reg getPROBE_CH3_rd_data_vld(  );
	K7mFcu_reg getPROBE_CH3_rx_len(  );
	K7mFcu_reg getPROBE_CH3_wr_data(  );
	K7mFcu_reg getPROBE_CH3_wr_data_en(  );
	K7mFcu_reg getPROBE_CH3_cmd_type(  );
	K7mFcu_reg getPROBE_CH3_clr(  );

	K7mFcu_reg getPROBE_CH3(  );

	void assignPROBE_CH4_rd_data( K7mFcu_reg value  );
	void assignPROBE_CH4_rd_data_vld( K7mFcu_reg value  );
	void assignPROBE_CH4_rx_len( K7mFcu_reg value  );
	void assignPROBE_CH4_wr_data( K7mFcu_reg value  );
	void assignPROBE_CH4_wr_data_en( K7mFcu_reg value  );
	void assignPROBE_CH4_cmd_type( K7mFcu_reg value  );
	void assignPROBE_CH4_clr( K7mFcu_reg value  );

	void assignPROBE_CH4( K7mFcu_reg value );


	K7mFcu_reg getPROBE_CH4_rd_data(  );
	K7mFcu_reg getPROBE_CH4_rd_data_vld(  );
	K7mFcu_reg getPROBE_CH4_rx_len(  );
	K7mFcu_reg getPROBE_CH4_wr_data(  );
	K7mFcu_reg getPROBE_CH4_wr_data_en(  );
	K7mFcu_reg getPROBE_CH4_cmd_type(  );
	K7mFcu_reg getPROBE_CH4_clr(  );

	K7mFcu_reg getPROBE_CH4(  );

	void assignPROBE_WAIT( K7mFcu_reg value );

	K7mFcu_reg getPROBE_WAIT(  );

	void assignDG_CFG_data( K7mFcu_reg value  );
	void assignDG_CFG_addr( K7mFcu_reg value  );

	void assignDG_CFG( K7mFcu_reg value );


	K7mFcu_reg getDG_RD(  );

	K7mFcu_reg getdbg_search_event_num_latch(  );

	K7mFcu_reg getdbg_search_frame_lenth_latch(  );

};
struct K7mFcuMem : public K7mFcuMemProxy, public IPhyFpga{
protected:
	K7mFcu_reg mAddrBase;
public:
	K7mFcuMem( K7mFcu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( K7mFcu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( K7mFcuMemProxy *proxy);
	void pull( K7mFcuMemProxy *proxy);

public:
	void setLA_LEVEL1_LA_LEVEL1( K7mFcu_reg value );

	void setLA_LEVEL1( K7mFcu_reg value );
	void outLA_LEVEL1_LA_LEVEL1( K7mFcu_reg value );
	void pulseLA_LEVEL1_LA_LEVEL1( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outLA_LEVEL1( K7mFcu_reg value );
	void outLA_LEVEL1(  );


	K7mFcu_reg inLA_LEVEL1(  );

	void setLA_LEVEL2_LA_LEVEL2( K7mFcu_reg value );

	void setLA_LEVEL2( K7mFcu_reg value );
	void outLA_LEVEL2_LA_LEVEL2( K7mFcu_reg value );
	void pulseLA_LEVEL2_LA_LEVEL2( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outLA_LEVEL2( K7mFcu_reg value );
	void outLA_LEVEL2(  );


	K7mFcu_reg inLA_LEVEL2(  );

	void setFG1_OFFSET_FG1_OFFSET( K7mFcu_reg value );

	void setFG1_OFFSET( K7mFcu_reg value );
	void outFG1_OFFSET_FG1_OFFSET( K7mFcu_reg value );
	void pulseFG1_OFFSET_FG1_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outFG1_OFFSET( K7mFcu_reg value );
	void outFG1_OFFSET(  );


	K7mFcu_reg inFG1_OFFSET(  );

	void setFG1_REF_FG1_REF( K7mFcu_reg value );

	void setFG1_REF( K7mFcu_reg value );
	void outFG1_REF_FG1_REF( K7mFcu_reg value );
	void pulseFG1_REF_FG1_REF( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outFG1_REF( K7mFcu_reg value );
	void outFG1_REF(  );


	K7mFcu_reg inFG1_REF(  );

	void setFG2_OFFSET_FG2_OFFSET( K7mFcu_reg value );

	void setFG2_OFFSET( K7mFcu_reg value );
	void outFG2_OFFSET_FG2_OFFSET( K7mFcu_reg value );
	void pulseFG2_OFFSET_FG2_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outFG2_OFFSET( K7mFcu_reg value );
	void outFG2_OFFSET(  );


	K7mFcu_reg inFG2_OFFSET(  );

	void setFG2_REF_FG2_REF( K7mFcu_reg value );

	void setFG2_REF( K7mFcu_reg value );
	void outFG2_REF_FG2_REF( K7mFcu_reg value );
	void pulseFG2_REF_FG2_REF( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outFG2_REF( K7mFcu_reg value );
	void outFG2_REF(  );


	K7mFcu_reg inFG2_REF(  );

	void setPROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg value );

	void setPROBE1_OFFSET( K7mFcu_reg value );
	void outPROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg value );
	void pulsePROBE1_OFFSET_PROBE1_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE1_OFFSET( K7mFcu_reg value );
	void outPROBE1_OFFSET(  );


	K7mFcu_reg inPROBE1_OFFSET(  );

	void setPROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg value );

	void setPROBE2_OFFSET( K7mFcu_reg value );
	void outPROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg value );
	void pulsePROBE2_OFFSET_PROBE2_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE2_OFFSET( K7mFcu_reg value );
	void outPROBE2_OFFSET(  );


	K7mFcu_reg inPROBE2_OFFSET(  );

	void setPROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg value );

	void setPROBE3_OFFSET( K7mFcu_reg value );
	void outPROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg value );
	void pulsePROBE3_OFFSET_PROBE3_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE3_OFFSET( K7mFcu_reg value );
	void outPROBE3_OFFSET(  );


	K7mFcu_reg inPROBE3_OFFSET(  );

	void setPROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg value );

	void setPROBE4_OFFSET( K7mFcu_reg value );
	void outPROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg value );
	void pulsePROBE4_OFFSET_PROBE4_OFFSET( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE4_OFFSET( K7mFcu_reg value );
	void outPROBE4_OFFSET(  );


	K7mFcu_reg inPROBE4_OFFSET(  );

	void setOFFSET1_OFFSET1( K7mFcu_reg value );

	void setOFFSET1( K7mFcu_reg value );
	void outOFFSET1_OFFSET1( K7mFcu_reg value );
	void pulseOFFSET1_OFFSET1( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outOFFSET1( K7mFcu_reg value );
	void outOFFSET1(  );


	K7mFcu_reg inOFFSET1(  );

	void setOFFSET2_OFFSET2( K7mFcu_reg value );

	void setOFFSET2( K7mFcu_reg value );
	void outOFFSET2_OFFSET2( K7mFcu_reg value );
	void pulseOFFSET2_OFFSET2( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outOFFSET2( K7mFcu_reg value );
	void outOFFSET2(  );


	K7mFcu_reg inOFFSET2(  );

	void setOFFSET3_OFFSET3( K7mFcu_reg value );

	void setOFFSET3( K7mFcu_reg value );
	void outOFFSET3_OFFSET3( K7mFcu_reg value );
	void pulseOFFSET3_OFFSET3( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outOFFSET3( K7mFcu_reg value );
	void outOFFSET3(  );


	K7mFcu_reg inOFFSET3(  );

	void setOFFSET4_OFFSET4( K7mFcu_reg value );

	void setOFFSET4( K7mFcu_reg value );
	void outOFFSET4_OFFSET4( K7mFcu_reg value );
	void pulseOFFSET4_OFFSET4( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outOFFSET4( K7mFcu_reg value );
	void outOFFSET4(  );


	K7mFcu_reg inOFFSET4(  );

	void setEXT_LEVEL_EXT_LEVEL( K7mFcu_reg value );

	void setEXT_LEVEL( K7mFcu_reg value );
	void outEXT_LEVEL_EXT_LEVEL( K7mFcu_reg value );
	void pulseEXT_LEVEL_EXT_LEVEL( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outEXT_LEVEL( K7mFcu_reg value );
	void outEXT_LEVEL(  );


	K7mFcu_reg inEXT_LEVEL(  );

	void setRESERVE_RESERVE( K7mFcu_reg value );

	void setRESERVE( K7mFcu_reg value );
	void outRESERVE_RESERVE( K7mFcu_reg value );
	void pulseRESERVE_RESERVE( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outRESERVE( K7mFcu_reg value );
	void outRESERVE(  );


	K7mFcu_reg inRESERVE(  );

	void setDAC_CHN_TIME_holdtime( K7mFcu_reg value );

	void setDAC_CHN_TIME( K7mFcu_reg value );
	void outDAC_CHN_TIME_holdtime( K7mFcu_reg value );
	void pulseDAC_CHN_TIME_holdtime( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outDAC_CHN_TIME( K7mFcu_reg value );
	void outDAC_CHN_TIME(  );


	K7mFcu_reg inDAC_CHN_TIME(  );

	void setMISC_PROBE_EN_en( K7mFcu_reg value );
	void setMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg value );

	void setMISC_PROBE_EN( K7mFcu_reg value );
	void outMISC_PROBE_EN_en( K7mFcu_reg value );
	void pulseMISC_PROBE_EN_en( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg value );
	void pulseMISC_PROBE_EN_sel_1K_10K( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outMISC_PROBE_EN( K7mFcu_reg value );
	void outMISC_PROBE_EN(  );


	K7mFcu_reg inMISC_PROBE_EN(  );

	void setMISC_TRIGOUT_SEL_sel( K7mFcu_reg value );

	void setMISC_TRIGOUT_SEL( K7mFcu_reg value );
	void outMISC_TRIGOUT_SEL_sel( K7mFcu_reg value );
	void pulseMISC_TRIGOUT_SEL_sel( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outMISC_TRIGOUT_SEL( K7mFcu_reg value );
	void outMISC_TRIGOUT_SEL(  );


	K7mFcu_reg inMISC_TRIGOUT_SEL(  );

	K7mFcu_reg inVERSION(  );

	void setCFG_1WIRE_bit_start( K7mFcu_reg value );
	void setCFG_1WIRE_bit_tx( K7mFcu_reg value );
	void setCFG_1WIRE_bit_rx( K7mFcu_reg value );
	void setCFG_1WIRE_bit_period( K7mFcu_reg value );

	void setCFG_1WIRE( K7mFcu_reg value );
	void outCFG_1WIRE_bit_start( K7mFcu_reg value );
	void pulseCFG_1WIRE_bit_start( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outCFG_1WIRE_bit_tx( K7mFcu_reg value );
	void pulseCFG_1WIRE_bit_tx( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outCFG_1WIRE_bit_rx( K7mFcu_reg value );
	void pulseCFG_1WIRE_bit_rx( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outCFG_1WIRE_bit_period( K7mFcu_reg value );
	void pulseCFG_1WIRE_bit_period( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outCFG_1WIRE( K7mFcu_reg value );
	void outCFG_1WIRE(  );


	K7mFcu_reg inCFG_1WIRE(  );

	void setPROBE_CH1_rd_data( K7mFcu_reg value );
	void setPROBE_CH1_rd_data_vld( K7mFcu_reg value );
	void setPROBE_CH1_rx_len( K7mFcu_reg value );
	void setPROBE_CH1_wr_data( K7mFcu_reg value );
	void setPROBE_CH1_wr_data_en( K7mFcu_reg value );
	void setPROBE_CH1_cmd_type( K7mFcu_reg value );
	void setPROBE_CH1_clr( K7mFcu_reg value );

	void setPROBE_CH1( K7mFcu_reg value );
	void outPROBE_CH1_rd_data( K7mFcu_reg value );
	void pulsePROBE_CH1_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH1_rd_data_vld( K7mFcu_reg value );
	void pulsePROBE_CH1_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH1_rx_len( K7mFcu_reg value );
	void pulsePROBE_CH1_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH1_wr_data( K7mFcu_reg value );
	void pulsePROBE_CH1_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH1_wr_data_en( K7mFcu_reg value );
	void pulsePROBE_CH1_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH1_cmd_type( K7mFcu_reg value );
	void pulsePROBE_CH1_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH1_clr( K7mFcu_reg value );
	void pulsePROBE_CH1_clr( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE_CH1( K7mFcu_reg value );
	void outPROBE_CH1(  );


	K7mFcu_reg inPROBE_CH1(  );

	void setPROBE_CH2_rd_data( K7mFcu_reg value );
	void setPROBE_CH2_rd_data_vld( K7mFcu_reg value );
	void setPROBE_CH2_rx_len( K7mFcu_reg value );
	void setPROBE_CH2_wr_data( K7mFcu_reg value );
	void setPROBE_CH2_wr_data_en( K7mFcu_reg value );
	void setPROBE_CH2_cmd_type( K7mFcu_reg value );
	void setPROBE_CH2_clr( K7mFcu_reg value );

	void setPROBE_CH2( K7mFcu_reg value );
	void outPROBE_CH2_rd_data( K7mFcu_reg value );
	void pulsePROBE_CH2_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH2_rd_data_vld( K7mFcu_reg value );
	void pulsePROBE_CH2_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH2_rx_len( K7mFcu_reg value );
	void pulsePROBE_CH2_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH2_wr_data( K7mFcu_reg value );
	void pulsePROBE_CH2_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH2_wr_data_en( K7mFcu_reg value );
	void pulsePROBE_CH2_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH2_cmd_type( K7mFcu_reg value );
	void pulsePROBE_CH2_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH2_clr( K7mFcu_reg value );
	void pulsePROBE_CH2_clr( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE_CH2( K7mFcu_reg value );
	void outPROBE_CH2(  );


	K7mFcu_reg inPROBE_CH2(  );

	void setPROBE_CH3_rd_data( K7mFcu_reg value );
	void setPROBE_CH3_rd_data_vld( K7mFcu_reg value );
	void setPROBE_CH3_rx_len( K7mFcu_reg value );
	void setPROBE_CH3_wr_data( K7mFcu_reg value );
	void setPROBE_CH3_wr_data_en( K7mFcu_reg value );
	void setPROBE_CH3_cmd_type( K7mFcu_reg value );
	void setPROBE_CH3_clr( K7mFcu_reg value );

	void setPROBE_CH3( K7mFcu_reg value );
	void outPROBE_CH3_rd_data( K7mFcu_reg value );
	void pulsePROBE_CH3_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH3_rd_data_vld( K7mFcu_reg value );
	void pulsePROBE_CH3_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH3_rx_len( K7mFcu_reg value );
	void pulsePROBE_CH3_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH3_wr_data( K7mFcu_reg value );
	void pulsePROBE_CH3_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH3_wr_data_en( K7mFcu_reg value );
	void pulsePROBE_CH3_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH3_cmd_type( K7mFcu_reg value );
	void pulsePROBE_CH3_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH3_clr( K7mFcu_reg value );
	void pulsePROBE_CH3_clr( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE_CH3( K7mFcu_reg value );
	void outPROBE_CH3(  );


	K7mFcu_reg inPROBE_CH3(  );

	void setPROBE_CH4_rd_data( K7mFcu_reg value );
	void setPROBE_CH4_rd_data_vld( K7mFcu_reg value );
	void setPROBE_CH4_rx_len( K7mFcu_reg value );
	void setPROBE_CH4_wr_data( K7mFcu_reg value );
	void setPROBE_CH4_wr_data_en( K7mFcu_reg value );
	void setPROBE_CH4_cmd_type( K7mFcu_reg value );
	void setPROBE_CH4_clr( K7mFcu_reg value );

	void setPROBE_CH4( K7mFcu_reg value );
	void outPROBE_CH4_rd_data( K7mFcu_reg value );
	void pulsePROBE_CH4_rd_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH4_rd_data_vld( K7mFcu_reg value );
	void pulsePROBE_CH4_rd_data_vld( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH4_rx_len( K7mFcu_reg value );
	void pulsePROBE_CH4_rx_len( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH4_wr_data( K7mFcu_reg value );
	void pulsePROBE_CH4_wr_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH4_wr_data_en( K7mFcu_reg value );
	void pulsePROBE_CH4_wr_data_en( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH4_cmd_type( K7mFcu_reg value );
	void pulsePROBE_CH4_cmd_type( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outPROBE_CH4_clr( K7mFcu_reg value );
	void pulsePROBE_CH4_clr( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outPROBE_CH4( K7mFcu_reg value );
	void outPROBE_CH4(  );


	K7mFcu_reg inPROBE_CH4(  );

	void setPROBE_WAIT( K7mFcu_reg value );
	void outPROBE_WAIT( K7mFcu_reg value );
	void outPROBE_WAIT(  );

	K7mFcu_reg inPROBE_WAIT(  );

	void setDG_CFG_data( K7mFcu_reg value );
	void setDG_CFG_addr( K7mFcu_reg value );

	void setDG_CFG( K7mFcu_reg value );
	void outDG_CFG_data( K7mFcu_reg value );
	void pulseDG_CFG_data( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );
	void outDG_CFG_addr( K7mFcu_reg value );
	void pulseDG_CFG_addr( K7mFcu_reg activeV, K7mFcu_reg idleV=0, int timeus=0 );

	void outDG_CFG( K7mFcu_reg value );
	void outDG_CFG(  );


	K7mFcu_reg inDG_RD(  );

	K7mFcu_reg indbg_search_event_num_latch(  );

	K7mFcu_reg indbg_search_frame_lenth_latch(  );

public:
	cache_addr addr_LA_LEVEL1();
	cache_addr addr_LA_LEVEL2();
	cache_addr addr_FG1_OFFSET();
	cache_addr addr_FG1_REF();

	cache_addr addr_FG2_OFFSET();
	cache_addr addr_FG2_REF();
	cache_addr addr_PROBE1_OFFSET();
	cache_addr addr_PROBE2_OFFSET();

	cache_addr addr_PROBE3_OFFSET();
	cache_addr addr_PROBE4_OFFSET();
	cache_addr addr_OFFSET1();
	cache_addr addr_OFFSET2();

	cache_addr addr_OFFSET3();
	cache_addr addr_OFFSET4();
	cache_addr addr_EXT_LEVEL();
	cache_addr addr_RESERVE();

	cache_addr addr_DAC_CHN_TIME();
	cache_addr addr_MISC_PROBE_EN();
	cache_addr addr_MISC_TRIGOUT_SEL();
	cache_addr addr_VERSION();

	cache_addr addr_CFG_1WIRE();
	cache_addr addr_PROBE_CH1();
	cache_addr addr_PROBE_CH2();
	cache_addr addr_PROBE_CH3();

	cache_addr addr_PROBE_CH4();
	cache_addr addr_PROBE_WAIT();
	cache_addr addr_DG_CFG();
	cache_addr addr_DG_RD();

	cache_addr addr_dbg_search_event_num_latch();
	cache_addr addr_dbg_search_frame_lenth_latch();
};
#define _remap_K7mFcu_()\
	mapIn( &LA_LEVEL1, 0x2000 + mAddrBase );\
	mapIn( &LA_LEVEL2, 0x2004 + mAddrBase );\
	mapIn( &FG1_OFFSET, 0x2008 + mAddrBase );\
	mapIn( &FG1_REF, 0x200C + mAddrBase );\
	\
	mapIn( &FG2_OFFSET, 0x2010 + mAddrBase );\
	mapIn( &FG2_REF, 0x2014 + mAddrBase );\
	mapIn( &PROBE1_OFFSET, 0x2018 + mAddrBase );\
	mapIn( &PROBE2_OFFSET, 0x201C + mAddrBase );\
	\
	mapIn( &PROBE3_OFFSET, 0x2020 + mAddrBase );\
	mapIn( &PROBE4_OFFSET, 0x2024 + mAddrBase );\
	mapIn( &OFFSET1, 0x2028 + mAddrBase );\
	mapIn( &OFFSET2, 0x202C + mAddrBase );\
	\
	mapIn( &OFFSET3, 0x2030 + mAddrBase );\
	mapIn( &OFFSET4, 0x2034 + mAddrBase );\
	mapIn( &EXT_LEVEL, 0x2038 + mAddrBase );\
	mapIn( &RESERVE, 0x203C + mAddrBase );\
	\
	mapIn( &DAC_CHN_TIME, 0x2040 + mAddrBase );\
	mapIn( &MISC_PROBE_EN, 0x2044 + mAddrBase );\
	mapIn( &MISC_TRIGOUT_SEL, 0x2048 + mAddrBase );\
	mapIn( &VERSION, 0x2080 + mAddrBase );\
	\
	mapIn( &CFG_1WIRE, 0x2090 + mAddrBase );\
	mapIn( &PROBE_CH1, 0x2094 + mAddrBase );\
	mapIn( &PROBE_CH2, 0x2098 + mAddrBase );\
	mapIn( &PROBE_CH3, 0x209C + mAddrBase );\
	\
	mapIn( &PROBE_CH4, 0x20A0 + mAddrBase );\
	mapIn( &PROBE_WAIT, 0x20A4 + mAddrBase );\
	mapIn( &DG_CFG, 0x20C0 + mAddrBase );\
	mapIn( &DG_RD, 0x20C4 + mAddrBase );\
	\
	mapIn( &dbg_search_event_num_latch, 0x2ea8 + mAddrBase );\
	mapIn( &dbg_search_frame_lenth_latch, 0x2e90 + mAddrBase );\


#endif
