#ifndef _DAMREY_H_
#define _DAMREY_H_


#define DAMREY_VER_1_5_x
//#define DAMREY_VER_1_5
//#define DAMREY_VER_1_4


#define damrey_reg  unsigned short
#ifndef NULL
#define NULL    0
#endif

#ifdef DAMREY_VER_1_5_x
struct reg0
{
    damrey_reg rms_gain: 5;

    damrey_reg atten: 3;
    damrey_reg prog : 1; // move from reg2
    damrey_reg dsel2: 1; //new for v5
    damrey_reg th_trim: 5; //new for v5

    damrey_reg ng:1;//not used
};
struct reg0_r
{
    damrey_reg ov_det : 1;
    damrey_reg z_det : 1;
    damrey_reg tp2_zdet : 1;
    damrey_reg otp_rb : 1; //new for v5
    damrey_reg reg_rb : 9;
    damrey_reg version : 3;
};
struct reg1
{
    damrey_reg cturn3 : 2;
    damrey_reg cturn2 : 2;
    damrey_reg cturn1 : 2;
    damrey_reg cturn0 : 2;
    damrey_reg c_sel : 4;
    damrey_reg r_sel : 3;
    damrey_reg sga1_gs : 1;
};

struct reg2
{
    damrey_reg gpo0: 1;
    damrey_reg vos_trim1: 5;
    damrey_reg fb_sel : 4;
    damrey_reg fs_sel : 3;

    damrey_reg f_rw : 1;
    damrey_reg dsel : 1;
    damrey_reg vfs_ctrl : 1;
};

struct reg3
{
    damrey_reg dc_trim : 8;
    damrey_reg isel : 2;
    damrey_reg tp_sw : 2;
    damrey_reg tp1_typ : 1;
    damrey_reg tp_pol : 1;
};

struct reg4
{
    damrey_reg en_dclp : 1;
    damrey_reg dac_sel : 1;
    damrey_reg gtrim1 : 1;
    damrey_reg gsel1 : 1;
    damrey_reg gtrim2 : 2;
    damrey_reg gsel2 : 2;
    damrey_reg vos_trim2 : 5;
    damrey_reg gtrim3 : 2;
    damrey_reg gsel3 : 1;
};

struct reg5
{
    damrey_reg vos_trim3 : 5;
    damrey_reg nc : 2;
    damrey_reg oen : 2;
    damrey_reg osel : 2;
    damrey_reg gtrim4 : 4;
    damrey_reg gsel4 : 1;
};

struct reg6
{
    damrey_reg ol_en : 1;
    damrey_reg vos_trim4 : 5;
    damrey_reg vos_trim5 : 5;
    damrey_reg g_trim5 : 4;
    damrey_reg gsel5 : 1;
};

struct reg7
{
    damrey_reg vos : 16;
};

struct reg8
{
    damrey_reg k : 5;
    damrey_reg vos_exp : 1;
    damrey_reg gpo1 : 1;
    damrey_reg lf_trim : 8;
    damrey_reg nc : 1;
};

struct reg9
{
    damrey_reg hf_trim : 7;
    damrey_reg mf_c_trim : 7;
};

struct reg10
{
    damrey_reg az_vos_trim : 10;
    damrey_reg az_vos_p : 1;
    damrey_reg az_isel : 2;
    damrey_reg en_zd : 1;
    damrey_reg hz_npd : 1;
    damrey_reg lpp : 1;
};

struct reg11_r
{
    damrey_reg fuse_rb : 16;
};

struct reg11
{
    damrey_reg nl_trim41 : 4;
    damrey_reg nl_trim40 : 4;
    damrey_reg cc_trim4B : 3;
    damrey_reg cc_trim4A : 2;
    damrey_reg cc_trim3 : 2;
};

struct reg12
{
    damrey_reg lz_npd : 1;
    damrey_reg _d1 : 1;
    damrey_reg hz_itrim : 3;
    damrey_reg lz_itrim : 3;
    damrey_reg nl_trim31 : 4;
    damrey_reg nl_trim30 : 4;
};

struct reg13
{
    damrey_reg nl_trim21 : 4;
    damrey_reg nl_trim20 : 4;
    damrey_reg nl_trim11 : 4;
    damrey_reg nl_trim10 : 4;
};

struct reg14
{
    damrey_reg cc_trim2 : 2;
    damrey_reg vclp : 3;
    damrey_reg cc_trim1 : 3;

    damrey_reg hz_vos_trim2: 4;
    damrey_reg hz_vos_trim3: 4;
};

//new for v5
struct reg15
{
    damrey_reg ib_trim: 11;
    damrey_reg hz_vos_trim4 : 5;
};

struct damrey
{
    union{ reg0 rs0; damrey_reg r0; };
    union{ reg0_r r_rs0; damrey_reg r_r0; };

    union{ reg1 rs1; damrey_reg r1; };
    union{ reg2 rs2; damrey_reg r2; };
    union{ reg3 rs3; damrey_reg r3; };

    union{ reg4 rs4; damrey_reg r4; };
    union{ reg5 rs5; damrey_reg r5; };
    union{ reg6 rs6; damrey_reg r6; };
    union{ reg7 rs7; damrey_reg r7; };

    union{ reg8 rs8; damrey_reg r8; };
    union{ reg9 rs9; damrey_reg r9; };
    union{ reg10 rs10; damrey_reg r10; };
    union{ reg11_r r_rs11; damrey_reg r_r11; };
    union{ reg11 rs11; damrey_reg r11; };

    union{ reg12 rs12; damrey_reg r12; };
    union{ reg13 rs13; damrey_reg r13; };
    union{ reg14 rs14; damrey_reg r14; };
    union{ reg15 rs15; damrey_reg r15; };
};
#endif

#ifdef DAMREY_VER_1_5

struct reg0
{
    damrey_reg rms_gain: 5;

    damrey_reg atten: 3;
    damrey_reg hz_vos_trim2 : 4;
    damrey_reg hz_vos_trim3 : 4;
};
struct reg0_r
{
    damrey_reg ov_det : 1;
    damrey_reg z_det : 1;
    damrey_reg tp2_zdet : 1;
    damrey_reg reg_rb : 10;
    damrey_reg version : 3;
};
struct reg1
{
    damrey_reg cturn3 : 2;
    damrey_reg cturn2 : 2;
    damrey_reg cturn1 : 2;
    damrey_reg cturn0 : 2;
    damrey_reg c_sel : 4;
    damrey_reg r_sel : 3;
    damrey_reg sga1_gs : 1;
};

struct reg2
{
    damrey_reg gpo0: 1;
    damrey_reg hz_vos_trim4 : 5;
    damrey_reg fb_csel : 4;
    damrey_reg fb_rsel : 2;
    damrey_reg prog : 1;
    damrey_reg f_rw : 1;
    damrey_reg dsel : 1;
    damrey_reg vfs_ctrl : 1;
};

struct reg3
{
    damrey_reg dc_trim : 8;
    damrey_reg isel : 2;
    damrey_reg vos_trim1_01 : 2;
    damrey_reg tp_sw : 2;
    damrey_reg tp1_typ : 1;
    damrey_reg tp_pol : 1;
};

struct reg4
{
    damrey_reg en_dclp : 1;
    damrey_reg dac_sel : 1;
    damrey_reg gtrim1 : 1;
    damrey_reg gsel1 : 1;
    damrey_reg gtrim2 : 2;
    damrey_reg gsel2 : 2;
    damrey_reg vos_trim2 : 5;
    damrey_reg gtrim3 : 2;
    damrey_reg gsel3 : 1;
};

struct reg5
{
    damrey_reg vos_trim3 : 5;
    damrey_reg nc : 2;
    damrey_reg oen : 2;
    damrey_reg osel : 2;
    damrey_reg gtrim4 : 4;
    damrey_reg gsel4 : 1;
};

struct reg6
{
    damrey_reg ol_en : 1;
    damrey_reg vos_trim4 : 5;
    damrey_reg vos_trim5 : 5;
    damrey_reg g_trim5 : 4;
    damrey_reg gsel5 : 1;
};

struct reg7
{
    damrey_reg vos : 16;
};

struct reg8
{
    damrey_reg k : 5;
    damrey_reg vos_exp : 1;
    damrey_reg gpo1 : 1;
    damrey_reg lf_trim : 8;
    damrey_reg nc : 1;
};

struct reg9
{
    damrey_reg hf_trim : 7;
    damrey_reg mf_c_trim : 7;
    damrey_reg vos_trim1_23 : 2;
};

struct reg10
{
    damrey_reg az_vos_trim : 10;
    damrey_reg az_vos_p : 1;
    damrey_reg az_isel : 2;
    damrey_reg en_zd : 1;
    damrey_reg hz_npd : 1;
    damrey_reg lpp : 1;
};

struct reg11_r
{
    damrey_reg fuse_rb : 16;
};

struct reg11
{
    damrey_reg nl_trim41 : 4;
    damrey_reg nl_trim40 : 4;
    damrey_reg cc_trim4B : 3;
    damrey_reg cc_trim4A : 2;
    damrey_reg cc_trim3 : 2;
    damrey_reg vos_trim1_4 : 1;
};

struct reg12
{
    damrey_reg lz_npd : 1;
    damrey_reg _d1 : 1;
    damrey_reg hz_itrim : 3;
    damrey_reg lz_itrim : 3;
    damrey_reg nl_trim31 : 4;
    damrey_reg nl_trim30 : 4;
};

struct reg13
{
    damrey_reg nl_trim21 : 4;
    damrey_reg nl_trim20 : 4;
    damrey_reg nl_trim11 : 4;
    damrey_reg nl_trim10 : 4;
};

struct reg14
{
    damrey_reg cc_trim2 : 2;
    damrey_reg vclp : 3;
    damrey_reg cc_trim1 : 3;

    damrey_reg _nc : 8;
};

struct damrey
{
    union{ reg0 rs0; damrey_reg r0; };
    union{ reg0_r r_rs0; damrey_reg r_r0; };

    union{ reg1 rs1; damrey_reg r1; };
    union{ reg2 rs2; damrey_reg r2; };
    union{ reg3 rs3; damrey_reg r3; };

    union{ reg4 rs4; damrey_reg r4; };
    union{ reg5 rs5; damrey_reg r5; };
    union{ reg6 rs6; damrey_reg r6; };
    union{ reg7 rs7; damrey_reg r7; };

    union{ reg8 rs8; damrey_reg r8; };
    union{ reg9 rs9; damrey_reg r9; };
    union{ reg10 rs10; damrey_reg r10; };
    union{ reg11_r r_rs11; damrey_reg r_r11; };
    union{ reg11 rs11; damrey_reg r11; };

    union{ reg12 rs12; damrey_reg r12; };
    union{ reg13 rs13; damrey_reg r13; };
    union{ reg14 rs14; damrey_reg r14; };
};
#endif

#ifdef DAMREY_VER_1_4

struct reg0
{
    damrey_reg ov_det: 1;
    damrey_reg ov_lvl: 2;
    damrey_reg tp_sw: 2;
    damrey_reg atten: 3;
    damrey_reg hz_vos_trim2 : 4;
    damrey_reg hz_vos_trim3 : 4;
};
struct reg0_r
{
    damrey_reg ov_det : 1;
    damrey_reg z_det : 1;
    damrey_reg reg_rb : 14;
};
struct reg1
{
    damrey_reg cturn3 : 2;
    damrey_reg cturn2 : 2;
    damrey_reg cturn1 : 2;
    damrey_reg cturn0 : 2;
    damrey_reg c_sel : 4;
    damrey_reg r_sel : 3;
    damrey_reg sga1_gs : 1;
};

struct reg2
{
    damrey_reg gpo0: 1;
    damrey_reg hz_vos_trim4 : 5;
    damrey_reg fb_csel : 4;
    damrey_reg fb_rsel : 2;
    damrey_reg prog : 1;
    damrey_reg f_rw : 1;
    damrey_reg dsel : 1;
    damrey_reg vfs_ctrl : 1;
};

struct reg3
{
    damrey_reg dc_trim : 8;
    damrey_reg isel : 2;
    damrey_reg vclamp : 2;
    damrey_reg ov_lvl : 4;
};

struct reg4
{
    damrey_reg en_dclp : 1;
    damrey_reg dac_sel : 1;
    damrey_reg gtrim1 : 1;
    damrey_reg gsel1 : 1;
    damrey_reg gtrim2 : 2;
    damrey_reg gsel2 : 2;
    damrey_reg vos_trim2 : 5;
    damrey_reg gtrim3 : 2;
    damrey_reg gsel3 : 1;
};

struct reg5
{
    damrey_reg vos_trim3 : 5;
    damrey_reg tp1_typ : 1;
    damrey_reg tp_pol : 1;
    damrey_reg oen : 2;
    damrey_reg osel : 2;
    damrey_reg gtrim4 : 4;
    damrey_reg gsel4 : 1;
};

struct reg6
{
    damrey_reg ov_rng : 1;
    damrey_reg vos_trim4 : 5;
    damrey_reg vos_trim5 : 5;
    damrey_reg g_trim5 : 4;
    damrey_reg gsel5 : 1;
};

struct reg7
{
    damrey_reg vos : 16;
};

struct reg8
{
    damrey_reg k : 5;
    damrey_reg vos_exp : 1;
    damrey_reg gpo1 : 1;
    damrey_reg lf_trim : 8;
    damrey_reg ilo_en : 1;
};

struct reg9
{
    damrey_reg hf_trim : 7;
    damrey_reg mf_c_trim : 7;
};

struct reg10
{
    damrey_reg az_vos_trim : 10;
    damrey_reg az_vos_p : 1;
    damrey_reg az_isel : 2;
    damrey_reg en_zd : 1;
    damrey_reg hz_npd : 1;
    damrey_reg lpp : 1;
};

struct reg11_r
{
    damrey_reg fuse_rb : 16;
};

struct reg11
{
    damrey_reg nl_trim41 : 4;
    damrey_reg nl_trim40 : 4;
    damrey_reg cc_trim4B : 3;
    damrey_reg cc_trim4A : 2;
    damrey_reg cc_trim3 : 2;
};

struct reg12
{
    damrey_reg lz_npd : 1;
    damrey_reg _d1 : 1;
    damrey_reg hz_itrim : 3;
    damrey_reg lz_itrim : 3;
    damrey_reg nl_trim31 : 4;
    damrey_reg nl_trim30 : 4;
};

struct reg13
{
    damrey_reg nl_trim21 : 4;
    damrey_reg nl_trim20 : 4;
    damrey_reg nl_trim11 : 4;
    damrey_reg nl_trim10 : 4;
};

struct reg14
{
    damrey_reg cc_trim2 : 2;
    damrey_reg vclp : 3;
    damrey_reg cc_trim1 : 3;
};

struct damrey
{
    union{ reg0 rs0; damrey_reg r0; };
    union{ reg0_r r_rs0; damrey_reg r_r0; };

    union{ reg1 rs1; damrey_reg r1; };
    union{ reg2 rs2; damrey_reg r2; };
    union{ reg3 rs3; damrey_reg r3; };

    union{ reg4 rs4; damrey_reg r4; };
    union{ reg5 rs5; damrey_reg r5; };
    union{ reg6 rs6; damrey_reg r6; };
    union{ reg7 rs7; damrey_reg r7; };

    union{ reg8 rs8; damrey_reg r8; };
    union{ reg9 rs9; damrey_reg r9; };
    union{ reg10 rs10; damrey_reg r10; };
    union{ reg11_r r_rs11; damrey_reg r_r11; };
    union{ reg11 rs11; damrey_reg r11; };

    union{ reg12 rs12; damrey_reg r12; };
    union{ reg13 rs13; damrey_reg r13; };
    union{ reg14 rs14; damrey_reg r14; };
};

//! configs
struct lozScaleCfg
{
    int scale;  //! 1uv unit

    damrey_reg gsel1;
    damrey_reg gtrim1;
    damrey_reg gsel2;
    damrey_reg gtrim2;

    damrey_reg vos_trim2;
    damrey_reg gsel3;
    damrey_reg gtrim3;
    damrey_reg vos_trim3;

    damrey_reg gsel4;
    damrey_reg gtrim4;
    damrey_reg vos_trim4;
    damrey_reg gsel5;

    damrey_reg gtrim5;
    damrey_reg vos_trim5;
    damrey_reg dc_trim;
};

struct hizScaleCfg
{
    int scale;

    damrey_reg k;
    damrey_reg sga1_gs;
    damrey_reg atten;
    damrey_reg dc_trim;

    damrey_reg lf_trim;
    damrey_reg mf_c_trim;
    damrey_reg hf_trim;
};

lozScaleCfg *getLozScaleCfg( int scale );
hizScaleCfg *getHizScaleCfg( int scale );
float getKAtten( int k );
#endif

#endif // DAMREY

