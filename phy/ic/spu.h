#ifndef _SPU_IC_H_
#define _SPU_IC_H_

#define Spu_reg unsigned int

union Spu_CTRL{
	struct{
	Spu_reg spu_clr:1;
	Spu_reg adc_sync:1;
	Spu_reg adc_align_check:2;
	Spu_reg _pad0:3;
	Spu_reg non_inter:1;

	Spu_reg peak:1;
	Spu_reg hi_res:1;
	Spu_reg anti_alias:1;
	Spu_reg _pad1:2;
	Spu_reg trace_avg:1;

	Spu_reg trace_avg_mem:1;
	Spu_reg _pad2:2;
	Spu_reg adc_data_inv:4;
	Spu_reg _pad3:8;
	Spu_reg la_probe:1;
	Spu_reg coarse_trig_range:1;
	};
	Spu_reg payload;
};

union Spu_DEBUG{
	struct{
	Spu_reg gray_bypass:1;
	Spu_reg ms_order:1;
	Spu_reg s_view:1;
	Spu_reg bram_ddr_n:1;

	Spu_reg ddr_init:1;
	Spu_reg ddr_err:1;
	Spu_reg dbg_trig_src:1;
	Spu_reg _pad0:1;
	Spu_reg ddr_fsm_state:8;

	Spu_reg debug_mem:16;
	};
	Spu_reg payload;
};

union Spu_ADC_DATA_IDELAY{
	struct{
	Spu_reg idelay_var:5;
	Spu_reg _pad0:11;
	Spu_reg chn_bit:8;
	Spu_reg h:1;
	Spu_reg l:1;

	Spu_reg _pad1:2;
	Spu_reg core_ABCD:4;
	};
	Spu_reg payload;
};

union Spu_ADC_DATA_CHECK{
	struct{
	Spu_reg a_l:4;
	Spu_reg a_h:4;
	Spu_reg b_l:4;
	Spu_reg b_h:4;

	Spu_reg c_l:4;
	Spu_reg c_h:4;
	Spu_reg d_l:4;
	Spu_reg d_h:4;
	};
	Spu_reg payload;
};

union Spu_ADC_CHN_ALIGN_CHK{
	struct{
	Spu_reg adc_d:8;
	Spu_reg adc_c:8;
	Spu_reg adc_b:8;
	Spu_reg adc_a:8;
	};
	Spu_reg payload;
};

union Spu_ADC_CHN_ALIGN{
	struct{
	Spu_reg adc_d:8;
	Spu_reg adc_c:8;
	Spu_reg adc_b:8;
	Spu_reg adc_a:8;
	};
	Spu_reg payload;
};

union Spu_PRE_PROC{
	struct{
	Spu_reg proc_en:4;
	Spu_reg _pad0:4;
	Spu_reg cfg_filter_en:1;
	};
	Spu_reg payload;
};

union Spu_FILTER{
	struct{
	Spu_reg coeff:16;
	Spu_reg coeff_id:10;
	Spu_reg coeff_sel:2;
	Spu_reg coeff_ch:4;
	};
	Spu_reg payload;
};

union Spu_DOWN_SAMP_FAST{
	struct{
	Spu_reg down_samp_fast:6;
	Spu_reg _pad0:9;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_DOWN_SAMP_SLOW_H{
	struct{
	Spu_reg down_samp_slow_h:8;
	};
	Spu_reg payload;
};

union Spu_CHN_DELAY_AB{
	struct{
	Spu_reg B:16;
	Spu_reg A:16;
	};
	Spu_reg payload;
};

union Spu_CHN_DELAY_CD{
	struct{
	Spu_reg D:16;
	Spu_reg C:16;
	};
	Spu_reg payload;
};

union Spu_EXT_TRIG_IDEALY{
	struct{
	Spu_reg idelay_var:4;
	Spu_reg _pad0:4;
	Spu_reg ld:1;
	};
	Spu_reg payload;
};

union Spu_COARSE_DELAY_MAIN{
	struct{
	Spu_reg chn_delay:10;
	Spu_reg chn_ce:4;
	Spu_reg _pad0:16;
	Spu_reg offset_en:1;
	Spu_reg delay_en:1;
	};
	Spu_reg payload;
};

union Spu_COARSE_DELAY_ZOOM{
	struct{
	Spu_reg chn_delay:10;
	Spu_reg chn_ce:4;
	Spu_reg _pad0:16;
	Spu_reg offset_en:1;
	Spu_reg delay_en:1;
	};
	Spu_reg payload;
};

union Spu_FINE_DELAY_MAIN{
	struct{
	Spu_reg chn_delay:10;
	Spu_reg chn_ce:4;
	Spu_reg _pad0:16;
	Spu_reg offset_en:1;
	Spu_reg delay_en:1;
	};
	Spu_reg payload;
};

union Spu_FINE_DELAY_ZOOM{
	struct{
	Spu_reg chn_delay:10;
	Spu_reg chn_ce:4;
	Spu_reg _pad0:16;
	Spu_reg offset_en:1;
	Spu_reg delay_en:1;
	};
	Spu_reg payload;
};

union Spu_INTERP_MULT_MAIN{
	struct{
	Spu_reg coef_len:8;
	Spu_reg comm_mult:8;
	Spu_reg mult:8;
	Spu_reg _pad0:7;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_INTERP_MULT_ZOOM{
	struct{
	Spu_reg coef_len:8;
	Spu_reg comm_mult:8;
	Spu_reg mult:8;
	Spu_reg _pad0:7;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_INTERP_MULT_MAIN_FINE{
	struct{
	Spu_reg coef_len:8;
	Spu_reg comm_mult:8;
	Spu_reg mult:8;
	Spu_reg _pad0:7;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_INTERP_MULT_EYE{
	struct{
	Spu_reg coef_len:8;
	Spu_reg comm_mult:8;
	Spu_reg mult:8;
	Spu_reg _pad0:7;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_INTERP_COEF_TAP{
	struct{
	Spu_reg tap:8;
	Spu_reg ce:4;
	};
	Spu_reg payload;
};

union Spu_INTERP_COEF_WR{
	struct{
	Spu_reg wdata:16;
	Spu_reg waddr:8;
	Spu_reg coef_group:2;
	Spu_reg _pad0:5;
	Spu_reg wr_en:1;
	};
	Spu_reg payload;
};

union Spu_COMPRESS_MAIN{
	struct{
	Spu_reg rate:28;
	Spu_reg _pad0:2;
	Spu_reg peak:1;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_COMPRESS_ZOOM{
	struct{
	Spu_reg rate:28;
	Spu_reg _pad0:2;
	Spu_reg peak:1;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_LA_FINE_DELAY_MAIN{
	struct{
	Spu_reg _pad0:12;
	Spu_reg delay:12;
	Spu_reg _pad1:7;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_LA_FINE_DELAY_ZOOM{
	struct{
	Spu_reg _pad0:12;
	Spu_reg delay:12;
	Spu_reg _pad1:7;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_LA_COMP_MAIN{
	struct{
	Spu_reg rate:26;
	Spu_reg _pad0:5;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_LA_COMP_ZOOM{
	struct{
	Spu_reg rate:26;
	Spu_reg _pad0:5;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_DEBUG_ACQ{
	struct{
	Spu_reg samp_extract_vld:1;
	Spu_reg acq_dly_cfg_en:1;
	Spu_reg _pad0:2;
	Spu_reg acq_dly_cfg:4;
	Spu_reg trig_src_type:1;
	};
	Spu_reg payload;
};

union Spu_DEBUG_TX{
	struct{
	Spu_reg spu_tx_cnt:20;
	Spu_reg _pad0:4;
	Spu_reg spu_frm_fsm:4;
	Spu_reg _pad1:1;
	Spu_reg tx_cnt_clr:1;
	Spu_reg v_tx_bypass:1;

	Spu_reg frm_head_jmp:1;
	};
	Spu_reg payload;
};

union Spu_DEBUG_MEM{
	struct{
	Spu_reg trig_tpu_coarse:1;
	Spu_reg _pad0:7;
	Spu_reg mem_bram_dly:4;
	Spu_reg trig_offset_dot_pre:1;
	Spu_reg trig_dly_dot_out:1;

	Spu_reg trig_coarse_fine:1;
	Spu_reg _pad1:1;
	Spu_reg mem_last_dot:1;
	};
	Spu_reg payload;
};

union Spu_DEBUG_SCAN{
	struct{
	Spu_reg pos_sa_view_frm_cnt:20;
	};
	Spu_reg payload;
};

union Spu_RECORD_SUM{
	struct{
	Spu_reg wav_view_len:30;
	Spu_reg _pad0:1;
	Spu_reg wav_view_full:1;
	};
	Spu_reg payload;
};

union Spu_SCAN_ROLL_FRM{
	struct{
	Spu_reg scan_roll_frm_num:20;
	};
	Spu_reg payload;
};

union Spu_ADC_AVG{
	struct{
	Spu_reg rate:5;
	Spu_reg _pad0:25;
	Spu_reg done:1;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_ADC_AVG_RES_AB{
	struct{
	Spu_reg B:16;
	Spu_reg A:16;
	};
	Spu_reg payload;
};

union Spu_ADC_AVG_RES_CD{
	struct{
	Spu_reg D:16;
	Spu_reg C:16;
	};
	Spu_reg payload;
};

union Spu_ADC_AVG_MAX{
	struct{
	Spu_reg max_D:8;
	Spu_reg max_C:8;
	Spu_reg max_B:8;
	Spu_reg max_A:8;
	};
	Spu_reg payload;
};

union Spu_ADC_AVG_MIN{
	struct{
	Spu_reg min_D:8;
	Spu_reg min_C:8;
	Spu_reg min_B:8;
	Spu_reg min_A:8;
	};
	Spu_reg payload;
};

union Spu_LA_SA_RES{
	struct{
	Spu_reg la:16;
	};
	Spu_reg payload;
};

union Spu_ZONE_TRIG_AUX{
	struct{
	Spu_reg pulse_width:28;
	Spu_reg _pad0:2;
	Spu_reg inv:1;
	Spu_reg pass_fail:1;
	};
	Spu_reg payload;
};

union Spu_ZONE_TRIG_TAB1{
	struct{
	Spu_reg threshold_l:8;
	Spu_reg threshold_h:8;
	Spu_reg threshold_en:1;
	Spu_reg _pad0:1;
	Spu_reg column_addr:10;

	Spu_reg tab_index:4;
	};
	Spu_reg payload;
};

union Spu_ZONE_TRIG_TAB2{
	struct{
	Spu_reg threshold_l:8;
	Spu_reg threshold_h:8;
	Spu_reg threshold_en:1;
	Spu_reg _pad0:1;
	Spu_reg column_addr:10;

	Spu_reg tab_index:4;
	};
	Spu_reg payload;
};

union Spu_MASK_TAB{
	struct{
	Spu_reg threshold_l:8;
	Spu_reg threshold_l_less_en:1;
	Spu_reg threshold_h:8;
	Spu_reg threshold_h_greater_en:1;

	Spu_reg column_addr:10;
	Spu_reg tab_index:4;
	};
	Spu_reg payload;
};

union Spu_MASK_TAB_EX{
	struct{
	Spu_reg threshold_l:8;
	Spu_reg threshold_l_greater_en:1;
	Spu_reg threshold_h:8;
	Spu_reg threshold_h_less_en:1;

	Spu_reg column_addr:10;
	Spu_reg tab_index:4;
	};
	Spu_reg payload;
};

union Spu_MASK_COMPRESS{
	struct{
	Spu_reg rate:28;
	Spu_reg _pad0:2;
	Spu_reg peak:1;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_MASK_CTRL{
	struct{
	Spu_reg _pad0:16;
	Spu_reg cross_index:4;
	Spu_reg mask_index_en:4;
	Spu_reg zone1_trig_tab_en:1;
	Spu_reg zone2_trig_tab_en:1;

	Spu_reg zone1_trig_cross:1;
	Spu_reg zone2_trig_cross:1;
	Spu_reg mask_result_rd:1;
	Spu_reg sum_clr:1;

	Spu_reg cross_clr:1;
	Spu_reg mask_we:1;
	};
	Spu_reg payload;
};

union Spu_MASK_OUTPUT{
	struct{
	Spu_reg pulse_width:28;
	Spu_reg _pad0:2;
	Spu_reg inv:1;
	Spu_reg pass_fail:1;
	};
	Spu_reg payload;
};

union Spu_TIME_TAG_CTRL{
	struct{
	Spu_reg rd_en:1;
	Spu_reg rec_first_done:1;
	};
	Spu_reg payload;
};

union Spu_IMPORT_INFO{
	struct{
	Spu_reg _pad0:31;
	Spu_reg import_done:1;
	};
	Spu_reg payload;
};

union Spu_LINEAR_INTX_CTRL{
	struct{
	Spu_reg mian_bypass:1;
	Spu_reg zoom_bypass:1;
	};
	Spu_reg payload;
};

union Spu_COMPRESS_EYE{
	struct{
	Spu_reg rate:28;
	Spu_reg _pad0:2;
	Spu_reg peak:1;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_LA_LINEAR_INTX_CTRL{
	struct{
	Spu_reg mian_bypass:1;
	Spu_reg zoom_bypass:1;
	};
	Spu_reg payload;
};

union Spu_TRACE_COMPRESS_MAIN{
	struct{
	Spu_reg rate:28;
	Spu_reg _pad0:2;
	Spu_reg peak:1;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_TRACE_COMPRESS_ZOOM{
	struct{
	Spu_reg rate:28;
	Spu_reg _pad0:2;
	Spu_reg peak:1;
	Spu_reg en:1;
	};
	Spu_reg payload;
};

union Spu_ZYNQ_COMPRESS_MAIN{
	struct{
	Spu_reg rate:20;
	};
	Spu_reg payload;
};

union Spu_ZYNQ_COMPRESS_ZOOM{
	struct{
	Spu_reg rate:20;
	};
	Spu_reg payload;
};

union Spu_DEBUG_TX_LA{
	struct{
	Spu_reg spu_tx_cnt:28;
	};
	Spu_reg payload;
};

union Spu_DEBUG_TX_FRM{
	struct{
	Spu_reg spu_tx_frm_cnt:32;
	};
	Spu_reg payload;
};

union Spu_IMPORT_CNT_CH{
	struct{
	Spu_reg cnt:31;
	Spu_reg cnt_clr:1;
	};
	Spu_reg payload;
};

union Spu_IMPORT_CNT_LA{
	struct{
	Spu_reg cnt:31;
	Spu_reg cnt_clr:1;
	};
	Spu_reg payload;
};

union Spu_DEBUG_TX_LEN_BURST{
	struct{
	Spu_reg tx_length_burst:32;
	};
	Spu_reg payload;
};

struct SpuMemProxy {
	Spu_reg VERSION;
	Spu_reg TEST;
	Spu_CTRL CTRL; 
	Spu_DEBUG DEBUG; 

	Spu_ADC_DATA_IDELAY ADC_DATA_IDELAY; 
	Spu_ADC_DATA_CHECK ADC_DATA_CHECK; 
	Spu_ADC_CHN_ALIGN_CHK ADC_CHN_ALIGN_CHK; 
	Spu_ADC_CHN_ALIGN ADC_CHN_ALIGN; 

	Spu_PRE_PROC PRE_PROC; 
	Spu_FILTER FILTER; 
	Spu_DOWN_SAMP_FAST DOWN_SAMP_FAST; 
	Spu_DOWN_SAMP_SLOW_H DOWN_SAMP_SLOW_H; 

	Spu_reg DOWN_SAMP_SLOW_L;
	Spu_reg LA_DELAY;
	Spu_CHN_DELAY_AB CHN_DELAY_AB; 
	Spu_CHN_DELAY_CD CHN_DELAY_CD; 

	Spu_reg RANDOM_RANGE;
	Spu_EXT_TRIG_IDEALY EXT_TRIG_IDEALY; 
	Spu_reg SEGMENT_SIZE;
	Spu_reg RECORD_LEN;

	Spu_reg WAVE_LEN_MAIN;
	Spu_reg WAVE_LEN_ZOOM;
	Spu_reg WAVE_OFFSET_MAIN;
	Spu_reg WAVE_OFFSET_ZOOM;

	Spu_reg MEAS_OFFSET_CH;
	Spu_reg MEAS_LEN_CH;
	Spu_reg MEAS_OFFSET_LA;
	Spu_reg MEAS_LEN_LA;

	Spu_COARSE_DELAY_MAIN COARSE_DELAY_MAIN; 
	Spu_COARSE_DELAY_ZOOM COARSE_DELAY_ZOOM; 
	Spu_FINE_DELAY_MAIN FINE_DELAY_MAIN; 
	Spu_FINE_DELAY_ZOOM FINE_DELAY_ZOOM; 

	Spu_INTERP_MULT_MAIN INTERP_MULT_MAIN; 
	Spu_INTERP_MULT_ZOOM INTERP_MULT_ZOOM; 
	Spu_INTERP_MULT_MAIN_FINE INTERP_MULT_MAIN_FINE; 
	Spu_INTERP_MULT_EYE INTERP_MULT_EYE; 

	Spu_INTERP_COEF_TAP INTERP_COEF_TAP; 
	Spu_INTERP_COEF_WR INTERP_COEF_WR; 
	Spu_COMPRESS_MAIN COMPRESS_MAIN; 
	Spu_COMPRESS_ZOOM COMPRESS_ZOOM; 

	Spu_LA_FINE_DELAY_MAIN LA_FINE_DELAY_MAIN; 
	Spu_LA_FINE_DELAY_ZOOM LA_FINE_DELAY_ZOOM; 
	Spu_LA_COMP_MAIN LA_COMP_MAIN; 
	Spu_LA_COMP_ZOOM LA_COMP_ZOOM; 

	Spu_reg TX_LEN_CH;
	Spu_reg TX_LEN_LA;
	Spu_reg TX_LEN_ZOOM_CH;
	Spu_reg TX_LEN_ZOOM_LA;

	Spu_reg TX_LEN_TRACE_CH;
	Spu_reg TX_LEN_TRACE_LA;
	Spu_reg TX_LEN_COL_CH;
	Spu_reg TX_LEN_COL_LA;

	Spu_DEBUG_ACQ DEBUG_ACQ; 
	Spu_DEBUG_TX DEBUG_TX; 
	Spu_DEBUG_MEM DEBUG_MEM; 
	Spu_DEBUG_SCAN DEBUG_SCAN; 

	Spu_RECORD_SUM RECORD_SUM; 
	Spu_SCAN_ROLL_FRM SCAN_ROLL_FRM; 
	Spu_ADC_AVG ADC_AVG; 
	Spu_ADC_AVG_RES_AB ADC_AVG_RES_AB; 

	Spu_ADC_AVG_RES_CD ADC_AVG_RES_CD; 
	Spu_ADC_AVG_MAX ADC_AVG_MAX; 
	Spu_ADC_AVG_MIN ADC_AVG_MIN; 
	Spu_LA_SA_RES LA_SA_RES; 

	Spu_ZONE_TRIG_AUX ZONE_TRIG_AUX; 
	Spu_ZONE_TRIG_TAB1 ZONE_TRIG_TAB1; 
	Spu_ZONE_TRIG_TAB2 ZONE_TRIG_TAB2; 
	Spu_MASK_TAB MASK_TAB; 

	Spu_MASK_TAB_EX MASK_TAB_EX; 
	Spu_MASK_COMPRESS MASK_COMPRESS; 
	Spu_MASK_CTRL MASK_CTRL; 
	Spu_reg MASK_FRM_CNT_L;

	Spu_reg MASK_FRM_CNT_H;
	Spu_reg MASK_CROSS_CNT_L_CHA;
	Spu_reg MASK_CROSS_CNT_H_CHA;
	Spu_reg MASK_CROSS_CNT_L_CHB;

	Spu_reg MASK_CROSS_CNT_H_CHB;
	Spu_reg MASK_CROSS_CNT_L_CHC;
	Spu_reg MASK_CROSS_CNT_H_CHC;
	Spu_reg MASK_CROSS_CNT_L_CHD;

	Spu_reg MASK_CROSS_CNT_H_CHD;
	Spu_MASK_OUTPUT MASK_OUTPUT; 
	Spu_reg SEARCH_OFFSET_CH;
	Spu_reg SEARCH_LEN_CH;

	Spu_reg SEARCH_OFFSET_LA;
	Spu_reg SEARCH_LEN_LA;
	Spu_reg TIME_TAG_L;
	Spu_reg TIME_TAG_H;

	Spu_reg FINE_TRIG_POSITION;
	Spu_TIME_TAG_CTRL TIME_TAG_CTRL; 
	Spu_IMPORT_INFO IMPORT_INFO; 
	Spu_reg TIME_TAG_L_REC_FIRST;

	Spu_reg TIME_TAG_H_REC_FIRST;
	Spu_reg TRACE_AVG_RATE_X;
	Spu_reg TRACE_AVG_RATE_Y;
	Spu_LINEAR_INTX_CTRL LINEAR_INTX_CTRL; 

	Spu_reg LINEAR_INTX_INIT;
	Spu_reg LINEAR_INTX_STEP;
	Spu_reg LINEAR_INTX_INIT_ZOOM;
	Spu_reg LINEAR_INTX_STEP_ZOOM;

	Spu_reg WAVE_OFFSET_EYE;
	Spu_reg WAVE_LEN_EYE;
	Spu_COMPRESS_EYE COMPRESS_EYE; 
	Spu_reg TX_LEN_EYE;

	Spu_LA_LINEAR_INTX_CTRL LA_LINEAR_INTX_CTRL; 
	Spu_reg LA_LINEAR_INTX_INIT;
	Spu_reg LA_LINEAR_INTX_STEP;
	Spu_reg LA_LINEAR_INTX_INIT_ZOOM;

	Spu_reg LA_LINEAR_INTX_STEP_ZOOM;
	Spu_TRACE_COMPRESS_MAIN TRACE_COMPRESS_MAIN; 
	Spu_TRACE_COMPRESS_ZOOM TRACE_COMPRESS_ZOOM; 
	Spu_ZYNQ_COMPRESS_MAIN ZYNQ_COMPRESS_MAIN; 

	Spu_ZYNQ_COMPRESS_ZOOM ZYNQ_COMPRESS_ZOOM; 
	Spu_DEBUG_TX_LA DEBUG_TX_LA; 
	Spu_DEBUG_TX_FRM DEBUG_TX_FRM; 
	Spu_IMPORT_CNT_CH IMPORT_CNT_CH; 

	Spu_IMPORT_CNT_LA IMPORT_CNT_LA; 
	Spu_reg DEBUG_LINEAR_INTX1;
	Spu_reg DEBUG_LINEAR_INTX2;
	Spu_reg DEBUG_DDR;

	Spu_DEBUG_TX_LEN_BURST DEBUG_TX_LEN_BURST; 
	Spu_reg getVERSION(  );

	Spu_reg getTEST(  );

	void assignCTRL_spu_clr( Spu_reg value  );
	void assignCTRL_adc_sync( Spu_reg value  );
	void assignCTRL_adc_align_check( Spu_reg value  );
	void assignCTRL_non_inter( Spu_reg value  );
	void assignCTRL_peak( Spu_reg value  );
	void assignCTRL_hi_res( Spu_reg value  );
	void assignCTRL_anti_alias( Spu_reg value  );
	void assignCTRL_trace_avg( Spu_reg value  );
	void assignCTRL_trace_avg_mem( Spu_reg value  );
	void assignCTRL_adc_data_inv( Spu_reg value  );
	void assignCTRL_la_probe( Spu_reg value  );
	void assignCTRL_coarse_trig_range( Spu_reg value  );

	void assignCTRL( Spu_reg value );


	Spu_reg getCTRL_spu_clr(  );
	Spu_reg getCTRL_adc_sync(  );
	Spu_reg getCTRL_adc_align_check(  );
	Spu_reg getCTRL_non_inter(  );
	Spu_reg getCTRL_peak(  );
	Spu_reg getCTRL_hi_res(  );
	Spu_reg getCTRL_anti_alias(  );
	Spu_reg getCTRL_trace_avg(  );
	Spu_reg getCTRL_trace_avg_mem(  );
	Spu_reg getCTRL_adc_data_inv(  );
	Spu_reg getCTRL_la_probe(  );
	Spu_reg getCTRL_coarse_trig_range(  );

	Spu_reg getCTRL(  );

	void assignDEBUG_gray_bypass( Spu_reg value  );
	void assignDEBUG_ms_order( Spu_reg value  );
	void assignDEBUG_s_view( Spu_reg value  );
	void assignDEBUG_bram_ddr_n( Spu_reg value  );
	void assignDEBUG_ddr_init( Spu_reg value  );
	void assignDEBUG_ddr_err( Spu_reg value  );
	void assignDEBUG_dbg_trig_src( Spu_reg value  );
	void assignDEBUG_ddr_fsm_state( Spu_reg value  );
	void assignDEBUG_debug_mem( Spu_reg value  );

	void assignDEBUG( Spu_reg value );


	Spu_reg getDEBUG_gray_bypass(  );
	Spu_reg getDEBUG_ms_order(  );
	Spu_reg getDEBUG_s_view(  );
	Spu_reg getDEBUG_bram_ddr_n(  );
	Spu_reg getDEBUG_ddr_init(  );
	Spu_reg getDEBUG_ddr_err(  );
	Spu_reg getDEBUG_dbg_trig_src(  );
	Spu_reg getDEBUG_ddr_fsm_state(  );
	Spu_reg getDEBUG_debug_mem(  );

	Spu_reg getDEBUG(  );

	void assignADC_DATA_IDELAY_idelay_var( Spu_reg value  );
	void assignADC_DATA_IDELAY_chn_bit( Spu_reg value  );
	void assignADC_DATA_IDELAY_h( Spu_reg value  );
	void assignADC_DATA_IDELAY_l( Spu_reg value  );
	void assignADC_DATA_IDELAY_core_ABCD( Spu_reg value  );

	void assignADC_DATA_IDELAY( Spu_reg value );


	Spu_reg getADC_DATA_IDELAY_idelay_var(  );
	Spu_reg getADC_DATA_IDELAY_chn_bit(  );
	Spu_reg getADC_DATA_IDELAY_h(  );
	Spu_reg getADC_DATA_IDELAY_l(  );
	Spu_reg getADC_DATA_IDELAY_core_ABCD(  );

	Spu_reg getADC_DATA_IDELAY(  );

	Spu_reg getADC_DATA_CHECK_a_l(  );
	Spu_reg getADC_DATA_CHECK_a_h(  );
	Spu_reg getADC_DATA_CHECK_b_l(  );
	Spu_reg getADC_DATA_CHECK_b_h(  );
	Spu_reg getADC_DATA_CHECK_c_l(  );
	Spu_reg getADC_DATA_CHECK_c_h(  );
	Spu_reg getADC_DATA_CHECK_d_l(  );
	Spu_reg getADC_DATA_CHECK_d_h(  );

	Spu_reg getADC_DATA_CHECK(  );

	Spu_reg getADC_CHN_ALIGN_CHK_adc_d(  );
	Spu_reg getADC_CHN_ALIGN_CHK_adc_c(  );
	Spu_reg getADC_CHN_ALIGN_CHK_adc_b(  );
	Spu_reg getADC_CHN_ALIGN_CHK_adc_a(  );

	Spu_reg getADC_CHN_ALIGN_CHK(  );

	void assignADC_CHN_ALIGN_adc_d( Spu_reg value  );
	void assignADC_CHN_ALIGN_adc_c( Spu_reg value  );
	void assignADC_CHN_ALIGN_adc_b( Spu_reg value  );
	void assignADC_CHN_ALIGN_adc_a( Spu_reg value  );

	void assignADC_CHN_ALIGN( Spu_reg value );


	Spu_reg getADC_CHN_ALIGN_adc_d(  );
	Spu_reg getADC_CHN_ALIGN_adc_c(  );
	Spu_reg getADC_CHN_ALIGN_adc_b(  );
	Spu_reg getADC_CHN_ALIGN_adc_a(  );

	Spu_reg getADC_CHN_ALIGN(  );

	void assignPRE_PROC_proc_en( Spu_reg value  );
	void assignPRE_PROC_cfg_filter_en( Spu_reg value  );

	void assignPRE_PROC( Spu_reg value );


	void assignFILTER_coeff( Spu_reg value  );
	void assignFILTER_coeff_id( Spu_reg value  );
	void assignFILTER_coeff_sel( Spu_reg value  );
	void assignFILTER_coeff_ch( Spu_reg value  );

	void assignFILTER( Spu_reg value );


	void assignDOWN_SAMP_FAST_down_samp_fast( Spu_reg value  );
	void assignDOWN_SAMP_FAST_en( Spu_reg value  );

	void assignDOWN_SAMP_FAST( Spu_reg value );


	Spu_reg getDOWN_SAMP_FAST_down_samp_fast(  );
	Spu_reg getDOWN_SAMP_FAST_en(  );

	Spu_reg getDOWN_SAMP_FAST(  );

	void assignDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value  );

	void assignDOWN_SAMP_SLOW_H( Spu_reg value );


	Spu_reg getDOWN_SAMP_SLOW_H_down_samp_slow_h(  );

	Spu_reg getDOWN_SAMP_SLOW_H(  );

	void assignDOWN_SAMP_SLOW_L( Spu_reg value );

	Spu_reg getDOWN_SAMP_SLOW_L(  );

	void assignLA_DELAY( Spu_reg value );

	Spu_reg getLA_DELAY(  );

	void assignCHN_DELAY_AB_B( Spu_reg value  );
	void assignCHN_DELAY_AB_A( Spu_reg value  );

	void assignCHN_DELAY_AB( Spu_reg value );


	Spu_reg getCHN_DELAY_AB_B(  );
	Spu_reg getCHN_DELAY_AB_A(  );

	Spu_reg getCHN_DELAY_AB(  );

	void assignCHN_DELAY_CD_D( Spu_reg value  );
	void assignCHN_DELAY_CD_C( Spu_reg value  );

	void assignCHN_DELAY_CD( Spu_reg value );


	Spu_reg getCHN_DELAY_CD_D(  );
	Spu_reg getCHN_DELAY_CD_C(  );

	Spu_reg getCHN_DELAY_CD(  );

	void assignRANDOM_RANGE( Spu_reg value );

	Spu_reg getRANDOM_RANGE(  );

	void assignEXT_TRIG_IDEALY_idelay_var( Spu_reg value  );
	void assignEXT_TRIG_IDEALY_ld( Spu_reg value  );

	void assignEXT_TRIG_IDEALY( Spu_reg value );


	Spu_reg getEXT_TRIG_IDEALY_idelay_var(  );
	Spu_reg getEXT_TRIG_IDEALY_ld(  );

	Spu_reg getEXT_TRIG_IDEALY(  );

	void assignSEGMENT_SIZE( Spu_reg value );

	Spu_reg getSEGMENT_SIZE(  );

	void assignRECORD_LEN( Spu_reg value );

	Spu_reg getRECORD_LEN(  );

	void assignWAVE_LEN_MAIN( Spu_reg value );

	Spu_reg getWAVE_LEN_MAIN(  );

	void assignWAVE_LEN_ZOOM( Spu_reg value );

	Spu_reg getWAVE_LEN_ZOOM(  );

	void assignWAVE_OFFSET_MAIN( Spu_reg value );

	Spu_reg getWAVE_OFFSET_MAIN(  );

	void assignWAVE_OFFSET_ZOOM( Spu_reg value );

	Spu_reg getWAVE_OFFSET_ZOOM(  );

	void assignMEAS_OFFSET_CH( Spu_reg value );

	Spu_reg getMEAS_OFFSET_CH(  );

	void assignMEAS_LEN_CH( Spu_reg value );

	Spu_reg getMEAS_LEN_CH(  );

	void assignMEAS_OFFSET_LA( Spu_reg value );

	Spu_reg getMEAS_OFFSET_LA(  );

	void assignMEAS_LEN_LA( Spu_reg value );

	Spu_reg getMEAS_LEN_LA(  );

	void assignCOARSE_DELAY_MAIN_chn_delay( Spu_reg value  );
	void assignCOARSE_DELAY_MAIN_chn_ce( Spu_reg value  );
	void assignCOARSE_DELAY_MAIN_offset_en( Spu_reg value  );
	void assignCOARSE_DELAY_MAIN_delay_en( Spu_reg value  );

	void assignCOARSE_DELAY_MAIN( Spu_reg value );


	Spu_reg getCOARSE_DELAY_MAIN_chn_delay(  );
	Spu_reg getCOARSE_DELAY_MAIN_chn_ce(  );
	Spu_reg getCOARSE_DELAY_MAIN_offset_en(  );
	Spu_reg getCOARSE_DELAY_MAIN_delay_en(  );

	Spu_reg getCOARSE_DELAY_MAIN(  );

	void assignCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value  );
	void assignCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value  );
	void assignCOARSE_DELAY_ZOOM_offset_en( Spu_reg value  );
	void assignCOARSE_DELAY_ZOOM_delay_en( Spu_reg value  );

	void assignCOARSE_DELAY_ZOOM( Spu_reg value );


	Spu_reg getCOARSE_DELAY_ZOOM_chn_delay(  );
	Spu_reg getCOARSE_DELAY_ZOOM_chn_ce(  );
	Spu_reg getCOARSE_DELAY_ZOOM_offset_en(  );
	Spu_reg getCOARSE_DELAY_ZOOM_delay_en(  );

	Spu_reg getCOARSE_DELAY_ZOOM(  );

	void assignFINE_DELAY_MAIN_chn_delay( Spu_reg value  );
	void assignFINE_DELAY_MAIN_chn_ce( Spu_reg value  );
	void assignFINE_DELAY_MAIN_offset_en( Spu_reg value  );
	void assignFINE_DELAY_MAIN_delay_en( Spu_reg value  );

	void assignFINE_DELAY_MAIN( Spu_reg value );


	Spu_reg getFINE_DELAY_MAIN_chn_delay(  );
	Spu_reg getFINE_DELAY_MAIN_chn_ce(  );
	Spu_reg getFINE_DELAY_MAIN_offset_en(  );
	Spu_reg getFINE_DELAY_MAIN_delay_en(  );

	Spu_reg getFINE_DELAY_MAIN(  );

	void assignFINE_DELAY_ZOOM_chn_delay( Spu_reg value  );
	void assignFINE_DELAY_ZOOM_chn_ce( Spu_reg value  );
	void assignFINE_DELAY_ZOOM_offset_en( Spu_reg value  );
	void assignFINE_DELAY_ZOOM_delay_en( Spu_reg value  );

	void assignFINE_DELAY_ZOOM( Spu_reg value );


	Spu_reg getFINE_DELAY_ZOOM_chn_delay(  );
	Spu_reg getFINE_DELAY_ZOOM_chn_ce(  );
	Spu_reg getFINE_DELAY_ZOOM_offset_en(  );
	Spu_reg getFINE_DELAY_ZOOM_delay_en(  );

	Spu_reg getFINE_DELAY_ZOOM(  );

	void assignINTERP_MULT_MAIN_coef_len( Spu_reg value  );
	void assignINTERP_MULT_MAIN_comm_mult( Spu_reg value  );
	void assignINTERP_MULT_MAIN_mult( Spu_reg value  );
	void assignINTERP_MULT_MAIN_en( Spu_reg value  );

	void assignINTERP_MULT_MAIN( Spu_reg value );


	Spu_reg getINTERP_MULT_MAIN_coef_len(  );
	Spu_reg getINTERP_MULT_MAIN_comm_mult(  );
	Spu_reg getINTERP_MULT_MAIN_mult(  );
	Spu_reg getINTERP_MULT_MAIN_en(  );

	Spu_reg getINTERP_MULT_MAIN(  );

	void assignINTERP_MULT_ZOOM_coef_len( Spu_reg value  );
	void assignINTERP_MULT_ZOOM_comm_mult( Spu_reg value  );
	void assignINTERP_MULT_ZOOM_mult( Spu_reg value  );
	void assignINTERP_MULT_ZOOM_en( Spu_reg value  );

	void assignINTERP_MULT_ZOOM( Spu_reg value );


	Spu_reg getINTERP_MULT_ZOOM_coef_len(  );
	Spu_reg getINTERP_MULT_ZOOM_comm_mult(  );
	Spu_reg getINTERP_MULT_ZOOM_mult(  );
	Spu_reg getINTERP_MULT_ZOOM_en(  );

	Spu_reg getINTERP_MULT_ZOOM(  );

	void assignINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value  );
	void assignINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value  );
	void assignINTERP_MULT_MAIN_FINE_mult( Spu_reg value  );
	void assignINTERP_MULT_MAIN_FINE_en( Spu_reg value  );

	void assignINTERP_MULT_MAIN_FINE( Spu_reg value );


	Spu_reg getINTERP_MULT_MAIN_FINE_coef_len(  );
	Spu_reg getINTERP_MULT_MAIN_FINE_comm_mult(  );
	Spu_reg getINTERP_MULT_MAIN_FINE_mult(  );
	Spu_reg getINTERP_MULT_MAIN_FINE_en(  );

	Spu_reg getINTERP_MULT_MAIN_FINE(  );

	void assignINTERP_MULT_EYE_coef_len( Spu_reg value  );
	void assignINTERP_MULT_EYE_comm_mult( Spu_reg value  );
	void assignINTERP_MULT_EYE_mult( Spu_reg value  );
	void assignINTERP_MULT_EYE_en( Spu_reg value  );

	void assignINTERP_MULT_EYE( Spu_reg value );


	Spu_reg getINTERP_MULT_EYE_coef_len(  );
	Spu_reg getINTERP_MULT_EYE_comm_mult(  );
	Spu_reg getINTERP_MULT_EYE_mult(  );
	Spu_reg getINTERP_MULT_EYE_en(  );

	Spu_reg getINTERP_MULT_EYE(  );

	void assignINTERP_COEF_TAP_tap( Spu_reg value  );
	void assignINTERP_COEF_TAP_ce( Spu_reg value  );

	void assignINTERP_COEF_TAP( Spu_reg value );


	void assignINTERP_COEF_WR_wdata( Spu_reg value  );
	void assignINTERP_COEF_WR_waddr( Spu_reg value  );
	void assignINTERP_COEF_WR_coef_group( Spu_reg value  );
	void assignINTERP_COEF_WR_wr_en( Spu_reg value  );

	void assignINTERP_COEF_WR( Spu_reg value );


	void assignCOMPRESS_MAIN_rate( Spu_reg value  );
	void assignCOMPRESS_MAIN_peak( Spu_reg value  );
	void assignCOMPRESS_MAIN_en( Spu_reg value  );

	void assignCOMPRESS_MAIN( Spu_reg value );


	Spu_reg getCOMPRESS_MAIN_rate(  );
	Spu_reg getCOMPRESS_MAIN_peak(  );
	Spu_reg getCOMPRESS_MAIN_en(  );

	Spu_reg getCOMPRESS_MAIN(  );

	void assignCOMPRESS_ZOOM_rate( Spu_reg value  );
	void assignCOMPRESS_ZOOM_peak( Spu_reg value  );
	void assignCOMPRESS_ZOOM_en( Spu_reg value  );

	void assignCOMPRESS_ZOOM( Spu_reg value );


	Spu_reg getCOMPRESS_ZOOM_rate(  );
	Spu_reg getCOMPRESS_ZOOM_peak(  );
	Spu_reg getCOMPRESS_ZOOM_en(  );

	Spu_reg getCOMPRESS_ZOOM(  );

	void assignLA_FINE_DELAY_MAIN_delay( Spu_reg value  );
	void assignLA_FINE_DELAY_MAIN_en( Spu_reg value  );

	void assignLA_FINE_DELAY_MAIN( Spu_reg value );


	Spu_reg getLA_FINE_DELAY_MAIN_delay(  );
	Spu_reg getLA_FINE_DELAY_MAIN_en(  );

	Spu_reg getLA_FINE_DELAY_MAIN(  );

	void assignLA_FINE_DELAY_ZOOM_delay( Spu_reg value  );
	void assignLA_FINE_DELAY_ZOOM_en( Spu_reg value  );

	void assignLA_FINE_DELAY_ZOOM( Spu_reg value );


	Spu_reg getLA_FINE_DELAY_ZOOM_delay(  );
	Spu_reg getLA_FINE_DELAY_ZOOM_en(  );

	Spu_reg getLA_FINE_DELAY_ZOOM(  );

	void assignLA_COMP_MAIN_rate( Spu_reg value  );
	void assignLA_COMP_MAIN_en( Spu_reg value  );

	void assignLA_COMP_MAIN( Spu_reg value );


	Spu_reg getLA_COMP_MAIN_rate(  );
	Spu_reg getLA_COMP_MAIN_en(  );

	Spu_reg getLA_COMP_MAIN(  );

	void assignLA_COMP_ZOOM_rate( Spu_reg value  );
	void assignLA_COMP_ZOOM_en( Spu_reg value  );

	void assignLA_COMP_ZOOM( Spu_reg value );


	Spu_reg getLA_COMP_ZOOM_rate(  );
	Spu_reg getLA_COMP_ZOOM_en(  );

	Spu_reg getLA_COMP_ZOOM(  );

	void assignTX_LEN_CH( Spu_reg value );

	Spu_reg getTX_LEN_CH(  );

	void assignTX_LEN_LA( Spu_reg value );

	Spu_reg getTX_LEN_LA(  );

	void assignTX_LEN_ZOOM_CH( Spu_reg value );

	Spu_reg getTX_LEN_ZOOM_CH(  );

	void assignTX_LEN_ZOOM_LA( Spu_reg value );

	Spu_reg getTX_LEN_ZOOM_LA(  );

	void assignTX_LEN_TRACE_CH( Spu_reg value );

	Spu_reg getTX_LEN_TRACE_CH(  );

	void assignTX_LEN_TRACE_LA( Spu_reg value );

	Spu_reg getTX_LEN_TRACE_LA(  );

	void assignTX_LEN_COL_CH( Spu_reg value );

	Spu_reg getTX_LEN_COL_CH(  );

	void assignTX_LEN_COL_LA( Spu_reg value );

	Spu_reg getTX_LEN_COL_LA(  );

	void assignDEBUG_ACQ_samp_extract_vld( Spu_reg value  );
	void assignDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value  );
	void assignDEBUG_ACQ_acq_dly_cfg( Spu_reg value  );
	void assignDEBUG_ACQ_trig_src_type( Spu_reg value  );

	void assignDEBUG_ACQ( Spu_reg value );


	Spu_reg getDEBUG_ACQ_samp_extract_vld(  );
	Spu_reg getDEBUG_ACQ_acq_dly_cfg_en(  );
	Spu_reg getDEBUG_ACQ_acq_dly_cfg(  );
	Spu_reg getDEBUG_ACQ_trig_src_type(  );

	Spu_reg getDEBUG_ACQ(  );

	void assignDEBUG_TX_spu_tx_cnt( Spu_reg value  );
	void assignDEBUG_TX_spu_frm_fsm( Spu_reg value  );
	void assignDEBUG_TX_tx_cnt_clr( Spu_reg value  );
	void assignDEBUG_TX_v_tx_bypass( Spu_reg value  );
	void assignDEBUG_TX_frm_head_jmp( Spu_reg value  );

	void assignDEBUG_TX( Spu_reg value );


	Spu_reg getDEBUG_TX_spu_tx_cnt(  );
	Spu_reg getDEBUG_TX_spu_frm_fsm(  );
	Spu_reg getDEBUG_TX_tx_cnt_clr(  );
	Spu_reg getDEBUG_TX_v_tx_bypass(  );
	Spu_reg getDEBUG_TX_frm_head_jmp(  );

	Spu_reg getDEBUG_TX(  );

	void assignDEBUG_MEM_trig_tpu_coarse( Spu_reg value  );
	void assignDEBUG_MEM_mem_bram_dly( Spu_reg value  );
	void assignDEBUG_MEM_trig_offset_dot_pre( Spu_reg value  );
	void assignDEBUG_MEM_trig_dly_dot_out( Spu_reg value  );
	void assignDEBUG_MEM_trig_coarse_fine( Spu_reg value  );
	void assignDEBUG_MEM_mem_last_dot( Spu_reg value  );

	void assignDEBUG_MEM( Spu_reg value );


	Spu_reg getDEBUG_MEM_trig_tpu_coarse(  );
	Spu_reg getDEBUG_MEM_mem_bram_dly(  );
	Spu_reg getDEBUG_MEM_trig_offset_dot_pre(  );
	Spu_reg getDEBUG_MEM_trig_dly_dot_out(  );
	Spu_reg getDEBUG_MEM_trig_coarse_fine(  );
	Spu_reg getDEBUG_MEM_mem_last_dot(  );

	Spu_reg getDEBUG_MEM(  );

	Spu_reg getDEBUG_SCAN_pos_sa_view_frm_cnt(  );

	Spu_reg getDEBUG_SCAN(  );

	Spu_reg getRECORD_SUM_wav_view_len(  );
	Spu_reg getRECORD_SUM_wav_view_full(  );

	Spu_reg getRECORD_SUM(  );

	void assignSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value  );

	void assignSCAN_ROLL_FRM( Spu_reg value );


	Spu_reg getSCAN_ROLL_FRM_scan_roll_frm_num(  );

	Spu_reg getSCAN_ROLL_FRM(  );

	void assignADC_AVG_rate( Spu_reg value  );
	void assignADC_AVG_done( Spu_reg value  );
	void assignADC_AVG_en( Spu_reg value  );

	void assignADC_AVG( Spu_reg value );


	Spu_reg getADC_AVG_rate(  );
	Spu_reg getADC_AVG_done(  );
	Spu_reg getADC_AVG_en(  );

	Spu_reg getADC_AVG(  );

	Spu_reg getADC_AVG_RES_AB_B(  );
	Spu_reg getADC_AVG_RES_AB_A(  );

	Spu_reg getADC_AVG_RES_AB(  );

	Spu_reg getADC_AVG_RES_CD_D(  );
	Spu_reg getADC_AVG_RES_CD_C(  );

	Spu_reg getADC_AVG_RES_CD(  );

	Spu_reg getADC_AVG_MAX_max_D(  );
	Spu_reg getADC_AVG_MAX_max_C(  );
	Spu_reg getADC_AVG_MAX_max_B(  );
	Spu_reg getADC_AVG_MAX_max_A(  );

	Spu_reg getADC_AVG_MAX(  );

	Spu_reg getADC_AVG_MIN_min_D(  );
	Spu_reg getADC_AVG_MIN_min_C(  );
	Spu_reg getADC_AVG_MIN_min_B(  );
	Spu_reg getADC_AVG_MIN_min_A(  );

	Spu_reg getADC_AVG_MIN(  );

	Spu_reg getLA_SA_RES_la(  );

	Spu_reg getLA_SA_RES(  );

	void assignZONE_TRIG_AUX_pulse_width( Spu_reg value  );
	void assignZONE_TRIG_AUX_inv( Spu_reg value  );
	void assignZONE_TRIG_AUX_pass_fail( Spu_reg value  );

	void assignZONE_TRIG_AUX( Spu_reg value );


	Spu_reg getZONE_TRIG_AUX_pulse_width(  );
	Spu_reg getZONE_TRIG_AUX_inv(  );
	Spu_reg getZONE_TRIG_AUX_pass_fail(  );

	Spu_reg getZONE_TRIG_AUX(  );

	void assignZONE_TRIG_TAB1_threshold_l( Spu_reg value  );
	void assignZONE_TRIG_TAB1_threshold_h( Spu_reg value  );
	void assignZONE_TRIG_TAB1_threshold_en( Spu_reg value  );
	void assignZONE_TRIG_TAB1_column_addr( Spu_reg value  );
	void assignZONE_TRIG_TAB1_tab_index( Spu_reg value  );

	void assignZONE_TRIG_TAB1( Spu_reg value );


	void assignZONE_TRIG_TAB2_threshold_l( Spu_reg value  );
	void assignZONE_TRIG_TAB2_threshold_h( Spu_reg value  );
	void assignZONE_TRIG_TAB2_threshold_en( Spu_reg value  );
	void assignZONE_TRIG_TAB2_column_addr( Spu_reg value  );
	void assignZONE_TRIG_TAB2_tab_index( Spu_reg value  );

	void assignZONE_TRIG_TAB2( Spu_reg value );


	void assignMASK_TAB_threshold_l( Spu_reg value  );
	void assignMASK_TAB_threshold_l_less_en( Spu_reg value  );
	void assignMASK_TAB_threshold_h( Spu_reg value  );
	void assignMASK_TAB_threshold_h_greater_en( Spu_reg value  );
	void assignMASK_TAB_column_addr( Spu_reg value  );
	void assignMASK_TAB_tab_index( Spu_reg value  );

	void assignMASK_TAB( Spu_reg value );


	Spu_reg getMASK_TAB_threshold_l(  );
	Spu_reg getMASK_TAB_threshold_l_less_en(  );
	Spu_reg getMASK_TAB_threshold_h(  );
	Spu_reg getMASK_TAB_threshold_h_greater_en(  );
	Spu_reg getMASK_TAB_column_addr(  );
	Spu_reg getMASK_TAB_tab_index(  );

	Spu_reg getMASK_TAB(  );

	void assignMASK_TAB_EX_threshold_l( Spu_reg value  );
	void assignMASK_TAB_EX_threshold_l_greater_en( Spu_reg value  );
	void assignMASK_TAB_EX_threshold_h( Spu_reg value  );
	void assignMASK_TAB_EX_threshold_h_less_en( Spu_reg value  );
	void assignMASK_TAB_EX_column_addr( Spu_reg value  );
	void assignMASK_TAB_EX_tab_index( Spu_reg value  );

	void assignMASK_TAB_EX( Spu_reg value );


	Spu_reg getMASK_TAB_EX_threshold_l(  );
	Spu_reg getMASK_TAB_EX_threshold_l_greater_en(  );
	Spu_reg getMASK_TAB_EX_threshold_h(  );
	Spu_reg getMASK_TAB_EX_threshold_h_less_en(  );
	Spu_reg getMASK_TAB_EX_column_addr(  );
	Spu_reg getMASK_TAB_EX_tab_index(  );

	Spu_reg getMASK_TAB_EX(  );

	void assignMASK_COMPRESS_rate( Spu_reg value  );
	void assignMASK_COMPRESS_peak( Spu_reg value  );
	void assignMASK_COMPRESS_en( Spu_reg value  );

	void assignMASK_COMPRESS( Spu_reg value );


	Spu_reg getMASK_COMPRESS_rate(  );
	Spu_reg getMASK_COMPRESS_peak(  );
	Spu_reg getMASK_COMPRESS_en(  );

	Spu_reg getMASK_COMPRESS(  );

	void assignMASK_CTRL_cross_index( Spu_reg value  );
	void assignMASK_CTRL_mask_index_en( Spu_reg value  );
	void assignMASK_CTRL_zone1_trig_tab_en( Spu_reg value  );
	void assignMASK_CTRL_zone2_trig_tab_en( Spu_reg value  );
	void assignMASK_CTRL_zone1_trig_cross( Spu_reg value  );
	void assignMASK_CTRL_zone2_trig_cross( Spu_reg value  );
	void assignMASK_CTRL_mask_result_rd( Spu_reg value  );
	void assignMASK_CTRL_sum_clr( Spu_reg value  );
	void assignMASK_CTRL_cross_clr( Spu_reg value  );
	void assignMASK_CTRL_mask_we( Spu_reg value  );

	void assignMASK_CTRL( Spu_reg value );


	Spu_reg getMASK_CTRL_cross_index(  );
	Spu_reg getMASK_CTRL_mask_index_en(  );
	Spu_reg getMASK_CTRL_zone1_trig_tab_en(  );
	Spu_reg getMASK_CTRL_zone2_trig_tab_en(  );
	Spu_reg getMASK_CTRL_zone1_trig_cross(  );
	Spu_reg getMASK_CTRL_zone2_trig_cross(  );
	Spu_reg getMASK_CTRL_mask_result_rd(  );
	Spu_reg getMASK_CTRL_sum_clr(  );
	Spu_reg getMASK_CTRL_cross_clr(  );
	Spu_reg getMASK_CTRL_mask_we(  );

	Spu_reg getMASK_CTRL(  );

	Spu_reg getMASK_FRM_CNT_L(  );

	Spu_reg getMASK_FRM_CNT_H(  );

	Spu_reg getMASK_CROSS_CNT_L_CHA(  );

	Spu_reg getMASK_CROSS_CNT_H_CHA(  );

	Spu_reg getMASK_CROSS_CNT_L_CHB(  );

	Spu_reg getMASK_CROSS_CNT_H_CHB(  );

	Spu_reg getMASK_CROSS_CNT_L_CHC(  );

	Spu_reg getMASK_CROSS_CNT_H_CHC(  );

	Spu_reg getMASK_CROSS_CNT_L_CHD(  );

	Spu_reg getMASK_CROSS_CNT_H_CHD(  );

	void assignMASK_OUTPUT_pulse_width( Spu_reg value  );
	void assignMASK_OUTPUT_inv( Spu_reg value  );
	void assignMASK_OUTPUT_pass_fail( Spu_reg value  );

	void assignMASK_OUTPUT( Spu_reg value );


	Spu_reg getMASK_OUTPUT_pulse_width(  );
	Spu_reg getMASK_OUTPUT_inv(  );
	Spu_reg getMASK_OUTPUT_pass_fail(  );

	Spu_reg getMASK_OUTPUT(  );

	void assignSEARCH_OFFSET_CH( Spu_reg value );

	Spu_reg getSEARCH_OFFSET_CH(  );

	void assignSEARCH_LEN_CH( Spu_reg value );

	Spu_reg getSEARCH_LEN_CH(  );

	void assignSEARCH_OFFSET_LA( Spu_reg value );

	Spu_reg getSEARCH_OFFSET_LA(  );

	void assignSEARCH_LEN_LA( Spu_reg value );

	Spu_reg getSEARCH_LEN_LA(  );

	void assignTIME_TAG_L( Spu_reg value );

	Spu_reg getTIME_TAG_L(  );

	void assignTIME_TAG_H( Spu_reg value );

	Spu_reg getTIME_TAG_H(  );

	void assignFINE_TRIG_POSITION( Spu_reg value );

	Spu_reg getFINE_TRIG_POSITION(  );

	void assignTIME_TAG_CTRL_rd_en( Spu_reg value  );
	void assignTIME_TAG_CTRL_rec_first_done( Spu_reg value  );

	void assignTIME_TAG_CTRL( Spu_reg value );


	Spu_reg getTIME_TAG_CTRL_rd_en(  );
	Spu_reg getTIME_TAG_CTRL_rec_first_done(  );

	Spu_reg getTIME_TAG_CTRL(  );

	void assignIMPORT_INFO_import_done( Spu_reg value  );

	void assignIMPORT_INFO( Spu_reg value );


	Spu_reg getIMPORT_INFO_import_done(  );

	Spu_reg getIMPORT_INFO(  );

	Spu_reg getTIME_TAG_L_REC_FIRST(  );

	Spu_reg getTIME_TAG_H_REC_FIRST(  );

	void assignTRACE_AVG_RATE_X( Spu_reg value );

	Spu_reg getTRACE_AVG_RATE_X(  );

	void assignTRACE_AVG_RATE_Y( Spu_reg value );

	Spu_reg getTRACE_AVG_RATE_Y(  );

	void assignLINEAR_INTX_CTRL_mian_bypass( Spu_reg value  );
	void assignLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value  );

	void assignLINEAR_INTX_CTRL( Spu_reg value );


	Spu_reg getLINEAR_INTX_CTRL_mian_bypass(  );
	Spu_reg getLINEAR_INTX_CTRL_zoom_bypass(  );

	Spu_reg getLINEAR_INTX_CTRL(  );

	void assignLINEAR_INTX_INIT( Spu_reg value );

	Spu_reg getLINEAR_INTX_INIT(  );

	void assignLINEAR_INTX_STEP( Spu_reg value );

	Spu_reg getLINEAR_INTX_STEP(  );

	void assignLINEAR_INTX_INIT_ZOOM( Spu_reg value );

	Spu_reg getLINEAR_INTX_INIT_ZOOM(  );

	void assignLINEAR_INTX_STEP_ZOOM( Spu_reg value );

	Spu_reg getLINEAR_INTX_STEP_ZOOM(  );

	void assignWAVE_OFFSET_EYE( Spu_reg value );

	Spu_reg getWAVE_OFFSET_EYE(  );

	void assignWAVE_LEN_EYE( Spu_reg value );

	Spu_reg getWAVE_LEN_EYE(  );

	void assignCOMPRESS_EYE_rate( Spu_reg value  );
	void assignCOMPRESS_EYE_peak( Spu_reg value  );
	void assignCOMPRESS_EYE_en( Spu_reg value  );

	void assignCOMPRESS_EYE( Spu_reg value );


	Spu_reg getCOMPRESS_EYE_rate(  );
	Spu_reg getCOMPRESS_EYE_peak(  );
	Spu_reg getCOMPRESS_EYE_en(  );

	Spu_reg getCOMPRESS_EYE(  );

	void assignTX_LEN_EYE( Spu_reg value );

	Spu_reg getTX_LEN_EYE(  );

	void assignLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value  );
	void assignLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value  );

	void assignLA_LINEAR_INTX_CTRL( Spu_reg value );


	Spu_reg getLA_LINEAR_INTX_CTRL_mian_bypass(  );
	Spu_reg getLA_LINEAR_INTX_CTRL_zoom_bypass(  );

	Spu_reg getLA_LINEAR_INTX_CTRL(  );

	void assignLA_LINEAR_INTX_INIT( Spu_reg value );

	Spu_reg getLA_LINEAR_INTX_INIT(  );

	void assignLA_LINEAR_INTX_STEP( Spu_reg value );

	Spu_reg getLA_LINEAR_INTX_STEP(  );

	void assignLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value );

	Spu_reg getLA_LINEAR_INTX_INIT_ZOOM(  );

	void assignLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value );

	Spu_reg getLA_LINEAR_INTX_STEP_ZOOM(  );

	void assignTRACE_COMPRESS_MAIN_rate( Spu_reg value  );
	void assignTRACE_COMPRESS_MAIN_peak( Spu_reg value  );
	void assignTRACE_COMPRESS_MAIN_en( Spu_reg value  );

	void assignTRACE_COMPRESS_MAIN( Spu_reg value );


	Spu_reg getTRACE_COMPRESS_MAIN_rate(  );
	Spu_reg getTRACE_COMPRESS_MAIN_peak(  );
	Spu_reg getTRACE_COMPRESS_MAIN_en(  );

	Spu_reg getTRACE_COMPRESS_MAIN(  );

	void assignTRACE_COMPRESS_ZOOM_rate( Spu_reg value  );
	void assignTRACE_COMPRESS_ZOOM_peak( Spu_reg value  );
	void assignTRACE_COMPRESS_ZOOM_en( Spu_reg value  );

	void assignTRACE_COMPRESS_ZOOM( Spu_reg value );


	Spu_reg getTRACE_COMPRESS_ZOOM_rate(  );
	Spu_reg getTRACE_COMPRESS_ZOOM_peak(  );
	Spu_reg getTRACE_COMPRESS_ZOOM_en(  );

	Spu_reg getTRACE_COMPRESS_ZOOM(  );

	void assignZYNQ_COMPRESS_MAIN_rate( Spu_reg value  );

	void assignZYNQ_COMPRESS_MAIN( Spu_reg value );


	Spu_reg getZYNQ_COMPRESS_MAIN_rate(  );

	Spu_reg getZYNQ_COMPRESS_MAIN(  );

	void assignZYNQ_COMPRESS_ZOOM_rate( Spu_reg value  );

	void assignZYNQ_COMPRESS_ZOOM( Spu_reg value );


	Spu_reg getZYNQ_COMPRESS_ZOOM_rate(  );

	Spu_reg getZYNQ_COMPRESS_ZOOM(  );

	Spu_reg getDEBUG_TX_LA_spu_tx_cnt(  );

	Spu_reg getDEBUG_TX_LA(  );

	Spu_reg getDEBUG_TX_FRM_spu_tx_frm_cnt(  );

	Spu_reg getDEBUG_TX_FRM(  );

	Spu_reg getIMPORT_CNT_CH_cnt(  );
	Spu_reg getIMPORT_CNT_CH_cnt_clr(  );

	Spu_reg getIMPORT_CNT_CH(  );

	Spu_reg getIMPORT_CNT_LA_cnt(  );
	Spu_reg getIMPORT_CNT_LA_cnt_clr(  );

	Spu_reg getIMPORT_CNT_LA(  );

	Spu_reg getDEBUG_LINEAR_INTX1(  );

	Spu_reg getDEBUG_LINEAR_INTX2(  );

	Spu_reg getDEBUG_DDR(  );

	Spu_reg getDEBUG_TX_LEN_BURST_tx_length_burst(  );

	Spu_reg getDEBUG_TX_LEN_BURST(  );

};
struct SpuMem : public SpuMemProxy, public IPhyFpga{
protected:
	Spu_reg mAddrBase;
public:
	SpuMem( Spu_reg base = 0) : mAddrBase(base)
	{}
	void setAddrBase( Spu_reg base )
	{ mAddrBase = base; }
	//! set/get
	void _loadOtp();
	void _remap_();
	void _outOtp();

public:
	//! proxy, pull, push
	void push( SpuMemProxy *proxy);
	void pull( SpuMemProxy *proxy);

public:
	Spu_reg inVERSION(  );

	Spu_reg inTEST(  );

	void setCTRL_spu_clr( Spu_reg value );
	void setCTRL_adc_sync( Spu_reg value );
	void setCTRL_adc_align_check( Spu_reg value );
	void setCTRL_non_inter( Spu_reg value );
	void setCTRL_peak( Spu_reg value );
	void setCTRL_hi_res( Spu_reg value );
	void setCTRL_anti_alias( Spu_reg value );
	void setCTRL_trace_avg( Spu_reg value );
	void setCTRL_trace_avg_mem( Spu_reg value );
	void setCTRL_adc_data_inv( Spu_reg value );
	void setCTRL_la_probe( Spu_reg value );
	void setCTRL_coarse_trig_range( Spu_reg value );

	void setCTRL( Spu_reg value );
	void outCTRL_spu_clr( Spu_reg value );
	void pulseCTRL_spu_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_adc_sync( Spu_reg value );
	void pulseCTRL_adc_sync( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_adc_align_check( Spu_reg value );
	void pulseCTRL_adc_align_check( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_non_inter( Spu_reg value );
	void pulseCTRL_non_inter( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_peak( Spu_reg value );
	void pulseCTRL_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_hi_res( Spu_reg value );
	void pulseCTRL_hi_res( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_anti_alias( Spu_reg value );
	void pulseCTRL_anti_alias( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_trace_avg( Spu_reg value );
	void pulseCTRL_trace_avg( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_trace_avg_mem( Spu_reg value );
	void pulseCTRL_trace_avg_mem( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_adc_data_inv( Spu_reg value );
	void pulseCTRL_adc_data_inv( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_la_probe( Spu_reg value );
	void pulseCTRL_la_probe( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCTRL_coarse_trig_range( Spu_reg value );
	void pulseCTRL_coarse_trig_range( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCTRL( Spu_reg value );
	void outCTRL(  );


	Spu_reg inCTRL(  );

	void setDEBUG_gray_bypass( Spu_reg value );
	void setDEBUG_ms_order( Spu_reg value );
	void setDEBUG_s_view( Spu_reg value );
	void setDEBUG_bram_ddr_n( Spu_reg value );
	void setDEBUG_ddr_init( Spu_reg value );
	void setDEBUG_ddr_err( Spu_reg value );
	void setDEBUG_dbg_trig_src( Spu_reg value );
	void setDEBUG_ddr_fsm_state( Spu_reg value );
	void setDEBUG_debug_mem( Spu_reg value );

	void setDEBUG( Spu_reg value );
	void outDEBUG_gray_bypass( Spu_reg value );
	void pulseDEBUG_gray_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_ms_order( Spu_reg value );
	void pulseDEBUG_ms_order( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_s_view( Spu_reg value );
	void pulseDEBUG_s_view( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_bram_ddr_n( Spu_reg value );
	void pulseDEBUG_bram_ddr_n( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_ddr_init( Spu_reg value );
	void pulseDEBUG_ddr_init( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_ddr_err( Spu_reg value );
	void pulseDEBUG_ddr_err( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_dbg_trig_src( Spu_reg value );
	void pulseDEBUG_dbg_trig_src( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_ddr_fsm_state( Spu_reg value );
	void pulseDEBUG_ddr_fsm_state( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_debug_mem( Spu_reg value );
	void pulseDEBUG_debug_mem( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outDEBUG( Spu_reg value );
	void outDEBUG(  );


	Spu_reg inDEBUG(  );

	void setADC_DATA_IDELAY_idelay_var( Spu_reg value );
	void setADC_DATA_IDELAY_chn_bit( Spu_reg value );
	void setADC_DATA_IDELAY_h( Spu_reg value );
	void setADC_DATA_IDELAY_l( Spu_reg value );
	void setADC_DATA_IDELAY_core_ABCD( Spu_reg value );

	void setADC_DATA_IDELAY( Spu_reg value );
	void outADC_DATA_IDELAY_idelay_var( Spu_reg value );
	void pulseADC_DATA_IDELAY_idelay_var( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_DATA_IDELAY_chn_bit( Spu_reg value );
	void pulseADC_DATA_IDELAY_chn_bit( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_DATA_IDELAY_h( Spu_reg value );
	void pulseADC_DATA_IDELAY_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_DATA_IDELAY_l( Spu_reg value );
	void pulseADC_DATA_IDELAY_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_DATA_IDELAY_core_ABCD( Spu_reg value );
	void pulseADC_DATA_IDELAY_core_ABCD( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outADC_DATA_IDELAY( Spu_reg value );
	void outADC_DATA_IDELAY(  );


	Spu_reg inADC_DATA_IDELAY(  );

	Spu_reg inADC_DATA_CHECK(  );

	Spu_reg inADC_CHN_ALIGN_CHK(  );

	void setADC_CHN_ALIGN_adc_d( Spu_reg value );
	void setADC_CHN_ALIGN_adc_c( Spu_reg value );
	void setADC_CHN_ALIGN_adc_b( Spu_reg value );
	void setADC_CHN_ALIGN_adc_a( Spu_reg value );

	void setADC_CHN_ALIGN( Spu_reg value );
	void outADC_CHN_ALIGN_adc_d( Spu_reg value );
	void pulseADC_CHN_ALIGN_adc_d( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_CHN_ALIGN_adc_c( Spu_reg value );
	void pulseADC_CHN_ALIGN_adc_c( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_CHN_ALIGN_adc_b( Spu_reg value );
	void pulseADC_CHN_ALIGN_adc_b( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_CHN_ALIGN_adc_a( Spu_reg value );
	void pulseADC_CHN_ALIGN_adc_a( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outADC_CHN_ALIGN( Spu_reg value );
	void outADC_CHN_ALIGN(  );


	Spu_reg inADC_CHN_ALIGN(  );

	void setPRE_PROC_proc_en( Spu_reg value );
	void setPRE_PROC_cfg_filter_en( Spu_reg value );

	void setPRE_PROC( Spu_reg value );
	void outPRE_PROC_proc_en( Spu_reg value );
	void pulsePRE_PROC_proc_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outPRE_PROC_cfg_filter_en( Spu_reg value );
	void pulsePRE_PROC_cfg_filter_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outPRE_PROC( Spu_reg value );
	void outPRE_PROC(  );


	void setFILTER_coeff( Spu_reg value );
	void setFILTER_coeff_id( Spu_reg value );
	void setFILTER_coeff_sel( Spu_reg value );
	void setFILTER_coeff_ch( Spu_reg value );

	void setFILTER( Spu_reg value );
	void outFILTER_coeff( Spu_reg value );
	void pulseFILTER_coeff( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFILTER_coeff_id( Spu_reg value );
	void pulseFILTER_coeff_id( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFILTER_coeff_sel( Spu_reg value );
	void pulseFILTER_coeff_sel( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFILTER_coeff_ch( Spu_reg value );
	void pulseFILTER_coeff_ch( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outFILTER( Spu_reg value );
	void outFILTER(  );


	void setDOWN_SAMP_FAST_down_samp_fast( Spu_reg value );
	void setDOWN_SAMP_FAST_en( Spu_reg value );

	void setDOWN_SAMP_FAST( Spu_reg value );
	void outDOWN_SAMP_FAST_down_samp_fast( Spu_reg value );
	void pulseDOWN_SAMP_FAST_down_samp_fast( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDOWN_SAMP_FAST_en( Spu_reg value );
	void pulseDOWN_SAMP_FAST_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outDOWN_SAMP_FAST( Spu_reg value );
	void outDOWN_SAMP_FAST(  );


	Spu_reg inDOWN_SAMP_FAST(  );

	void setDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value );

	void setDOWN_SAMP_SLOW_H( Spu_reg value );
	void outDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value );
	void pulseDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outDOWN_SAMP_SLOW_H( Spu_reg value );
	void outDOWN_SAMP_SLOW_H(  );


	Spu_reg inDOWN_SAMP_SLOW_H(  );

	void setDOWN_SAMP_SLOW_L( Spu_reg value );
	void outDOWN_SAMP_SLOW_L( Spu_reg value );
	void outDOWN_SAMP_SLOW_L(  );

	Spu_reg inDOWN_SAMP_SLOW_L(  );

	void setLA_DELAY( Spu_reg value );
	void outLA_DELAY( Spu_reg value );
	void outLA_DELAY(  );

	Spu_reg inLA_DELAY(  );

	void setCHN_DELAY_AB_B( Spu_reg value );
	void setCHN_DELAY_AB_A( Spu_reg value );

	void setCHN_DELAY_AB( Spu_reg value );
	void outCHN_DELAY_AB_B( Spu_reg value );
	void pulseCHN_DELAY_AB_B( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCHN_DELAY_AB_A( Spu_reg value );
	void pulseCHN_DELAY_AB_A( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCHN_DELAY_AB( Spu_reg value );
	void outCHN_DELAY_AB(  );


	Spu_reg inCHN_DELAY_AB(  );

	void setCHN_DELAY_CD_D( Spu_reg value );
	void setCHN_DELAY_CD_C( Spu_reg value );

	void setCHN_DELAY_CD( Spu_reg value );
	void outCHN_DELAY_CD_D( Spu_reg value );
	void pulseCHN_DELAY_CD_D( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCHN_DELAY_CD_C( Spu_reg value );
	void pulseCHN_DELAY_CD_C( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCHN_DELAY_CD( Spu_reg value );
	void outCHN_DELAY_CD(  );


	Spu_reg inCHN_DELAY_CD(  );

	void setRANDOM_RANGE( Spu_reg value );
	void outRANDOM_RANGE( Spu_reg value );
	void outRANDOM_RANGE(  );

	Spu_reg inRANDOM_RANGE(  );

	void setEXT_TRIG_IDEALY_idelay_var( Spu_reg value );
	void setEXT_TRIG_IDEALY_ld( Spu_reg value );

	void setEXT_TRIG_IDEALY( Spu_reg value );
	void outEXT_TRIG_IDEALY_idelay_var( Spu_reg value );
	void pulseEXT_TRIG_IDEALY_idelay_var( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outEXT_TRIG_IDEALY_ld( Spu_reg value );
	void pulseEXT_TRIG_IDEALY_ld( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outEXT_TRIG_IDEALY( Spu_reg value );
	void outEXT_TRIG_IDEALY(  );


	Spu_reg inEXT_TRIG_IDEALY(  );

	void setSEGMENT_SIZE( Spu_reg value );
	void outSEGMENT_SIZE( Spu_reg value );
	void outSEGMENT_SIZE(  );

	Spu_reg inSEGMENT_SIZE(  );

	void setRECORD_LEN( Spu_reg value );
	void outRECORD_LEN( Spu_reg value );
	void outRECORD_LEN(  );

	Spu_reg inRECORD_LEN(  );

	void setWAVE_LEN_MAIN( Spu_reg value );
	void outWAVE_LEN_MAIN( Spu_reg value );
	void outWAVE_LEN_MAIN(  );

	Spu_reg inWAVE_LEN_MAIN(  );

	void setWAVE_LEN_ZOOM( Spu_reg value );
	void outWAVE_LEN_ZOOM( Spu_reg value );
	void outWAVE_LEN_ZOOM(  );

	Spu_reg inWAVE_LEN_ZOOM(  );

	void setWAVE_OFFSET_MAIN( Spu_reg value );
	void outWAVE_OFFSET_MAIN( Spu_reg value );
	void outWAVE_OFFSET_MAIN(  );

	Spu_reg inWAVE_OFFSET_MAIN(  );

	void setWAVE_OFFSET_ZOOM( Spu_reg value );
	void outWAVE_OFFSET_ZOOM( Spu_reg value );
	void outWAVE_OFFSET_ZOOM(  );

	Spu_reg inWAVE_OFFSET_ZOOM(  );

	void setMEAS_OFFSET_CH( Spu_reg value );
	void outMEAS_OFFSET_CH( Spu_reg value );
	void outMEAS_OFFSET_CH(  );

	Spu_reg inMEAS_OFFSET_CH(  );

	void setMEAS_LEN_CH( Spu_reg value );
	void outMEAS_LEN_CH( Spu_reg value );
	void outMEAS_LEN_CH(  );

	Spu_reg inMEAS_LEN_CH(  );

	void setMEAS_OFFSET_LA( Spu_reg value );
	void outMEAS_OFFSET_LA( Spu_reg value );
	void outMEAS_OFFSET_LA(  );

	Spu_reg inMEAS_OFFSET_LA(  );

	void setMEAS_LEN_LA( Spu_reg value );
	void outMEAS_LEN_LA( Spu_reg value );
	void outMEAS_LEN_LA(  );

	Spu_reg inMEAS_LEN_LA(  );

	void setCOARSE_DELAY_MAIN_chn_delay( Spu_reg value );
	void setCOARSE_DELAY_MAIN_chn_ce( Spu_reg value );
	void setCOARSE_DELAY_MAIN_offset_en( Spu_reg value );
	void setCOARSE_DELAY_MAIN_delay_en( Spu_reg value );

	void setCOARSE_DELAY_MAIN( Spu_reg value );
	void outCOARSE_DELAY_MAIN_chn_delay( Spu_reg value );
	void pulseCOARSE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOARSE_DELAY_MAIN_chn_ce( Spu_reg value );
	void pulseCOARSE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOARSE_DELAY_MAIN_offset_en( Spu_reg value );
	void pulseCOARSE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOARSE_DELAY_MAIN_delay_en( Spu_reg value );
	void pulseCOARSE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCOARSE_DELAY_MAIN( Spu_reg value );
	void outCOARSE_DELAY_MAIN(  );


	Spu_reg inCOARSE_DELAY_MAIN(  );

	void setCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value );
	void setCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value );
	void setCOARSE_DELAY_ZOOM_offset_en( Spu_reg value );
	void setCOARSE_DELAY_ZOOM_delay_en( Spu_reg value );

	void setCOARSE_DELAY_ZOOM( Spu_reg value );
	void outCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value );
	void pulseCOARSE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value );
	void pulseCOARSE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOARSE_DELAY_ZOOM_offset_en( Spu_reg value );
	void pulseCOARSE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOARSE_DELAY_ZOOM_delay_en( Spu_reg value );
	void pulseCOARSE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCOARSE_DELAY_ZOOM( Spu_reg value );
	void outCOARSE_DELAY_ZOOM(  );


	Spu_reg inCOARSE_DELAY_ZOOM(  );

	void setFINE_DELAY_MAIN_chn_delay( Spu_reg value );
	void setFINE_DELAY_MAIN_chn_ce( Spu_reg value );
	void setFINE_DELAY_MAIN_offset_en( Spu_reg value );
	void setFINE_DELAY_MAIN_delay_en( Spu_reg value );

	void setFINE_DELAY_MAIN( Spu_reg value );
	void outFINE_DELAY_MAIN_chn_delay( Spu_reg value );
	void pulseFINE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFINE_DELAY_MAIN_chn_ce( Spu_reg value );
	void pulseFINE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFINE_DELAY_MAIN_offset_en( Spu_reg value );
	void pulseFINE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFINE_DELAY_MAIN_delay_en( Spu_reg value );
	void pulseFINE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outFINE_DELAY_MAIN( Spu_reg value );
	void outFINE_DELAY_MAIN(  );


	Spu_reg inFINE_DELAY_MAIN(  );

	void setFINE_DELAY_ZOOM_chn_delay( Spu_reg value );
	void setFINE_DELAY_ZOOM_chn_ce( Spu_reg value );
	void setFINE_DELAY_ZOOM_offset_en( Spu_reg value );
	void setFINE_DELAY_ZOOM_delay_en( Spu_reg value );

	void setFINE_DELAY_ZOOM( Spu_reg value );
	void outFINE_DELAY_ZOOM_chn_delay( Spu_reg value );
	void pulseFINE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFINE_DELAY_ZOOM_chn_ce( Spu_reg value );
	void pulseFINE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFINE_DELAY_ZOOM_offset_en( Spu_reg value );
	void pulseFINE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outFINE_DELAY_ZOOM_delay_en( Spu_reg value );
	void pulseFINE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outFINE_DELAY_ZOOM( Spu_reg value );
	void outFINE_DELAY_ZOOM(  );


	Spu_reg inFINE_DELAY_ZOOM(  );

	void setINTERP_MULT_MAIN_coef_len( Spu_reg value );
	void setINTERP_MULT_MAIN_comm_mult( Spu_reg value );
	void setINTERP_MULT_MAIN_mult( Spu_reg value );
	void setINTERP_MULT_MAIN_en( Spu_reg value );

	void setINTERP_MULT_MAIN( Spu_reg value );
	void outINTERP_MULT_MAIN_coef_len( Spu_reg value );
	void pulseINTERP_MULT_MAIN_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_MAIN_comm_mult( Spu_reg value );
	void pulseINTERP_MULT_MAIN_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_MAIN_mult( Spu_reg value );
	void pulseINTERP_MULT_MAIN_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_MAIN_en( Spu_reg value );
	void pulseINTERP_MULT_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outINTERP_MULT_MAIN( Spu_reg value );
	void outINTERP_MULT_MAIN(  );


	Spu_reg inINTERP_MULT_MAIN(  );

	void setINTERP_MULT_ZOOM_coef_len( Spu_reg value );
	void setINTERP_MULT_ZOOM_comm_mult( Spu_reg value );
	void setINTERP_MULT_ZOOM_mult( Spu_reg value );
	void setINTERP_MULT_ZOOM_en( Spu_reg value );

	void setINTERP_MULT_ZOOM( Spu_reg value );
	void outINTERP_MULT_ZOOM_coef_len( Spu_reg value );
	void pulseINTERP_MULT_ZOOM_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_ZOOM_comm_mult( Spu_reg value );
	void pulseINTERP_MULT_ZOOM_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_ZOOM_mult( Spu_reg value );
	void pulseINTERP_MULT_ZOOM_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_ZOOM_en( Spu_reg value );
	void pulseINTERP_MULT_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outINTERP_MULT_ZOOM( Spu_reg value );
	void outINTERP_MULT_ZOOM(  );


	Spu_reg inINTERP_MULT_ZOOM(  );

	void setINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value );
	void setINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value );
	void setINTERP_MULT_MAIN_FINE_mult( Spu_reg value );
	void setINTERP_MULT_MAIN_FINE_en( Spu_reg value );

	void setINTERP_MULT_MAIN_FINE( Spu_reg value );
	void outINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value );
	void pulseINTERP_MULT_MAIN_FINE_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value );
	void pulseINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_MAIN_FINE_mult( Spu_reg value );
	void pulseINTERP_MULT_MAIN_FINE_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_MAIN_FINE_en( Spu_reg value );
	void pulseINTERP_MULT_MAIN_FINE_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outINTERP_MULT_MAIN_FINE( Spu_reg value );
	void outINTERP_MULT_MAIN_FINE(  );


	Spu_reg inINTERP_MULT_MAIN_FINE(  );

	void setINTERP_MULT_EYE_coef_len( Spu_reg value );
	void setINTERP_MULT_EYE_comm_mult( Spu_reg value );
	void setINTERP_MULT_EYE_mult( Spu_reg value );
	void setINTERP_MULT_EYE_en( Spu_reg value );

	void setINTERP_MULT_EYE( Spu_reg value );
	void outINTERP_MULT_EYE_coef_len( Spu_reg value );
	void pulseINTERP_MULT_EYE_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_EYE_comm_mult( Spu_reg value );
	void pulseINTERP_MULT_EYE_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_EYE_mult( Spu_reg value );
	void pulseINTERP_MULT_EYE_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_MULT_EYE_en( Spu_reg value );
	void pulseINTERP_MULT_EYE_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outINTERP_MULT_EYE( Spu_reg value );
	void outINTERP_MULT_EYE(  );


	Spu_reg inINTERP_MULT_EYE(  );

	void setINTERP_COEF_TAP_tap( Spu_reg value );
	void setINTERP_COEF_TAP_ce( Spu_reg value );

	void setINTERP_COEF_TAP( Spu_reg value );
	void outINTERP_COEF_TAP_tap( Spu_reg value );
	void pulseINTERP_COEF_TAP_tap( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_COEF_TAP_ce( Spu_reg value );
	void pulseINTERP_COEF_TAP_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outINTERP_COEF_TAP( Spu_reg value );
	void outINTERP_COEF_TAP(  );


	void setINTERP_COEF_WR_wdata( Spu_reg value );
	void setINTERP_COEF_WR_waddr( Spu_reg value );
	void setINTERP_COEF_WR_coef_group( Spu_reg value );
	void setINTERP_COEF_WR_wr_en( Spu_reg value );

	void setINTERP_COEF_WR( Spu_reg value );
	void outINTERP_COEF_WR_wdata( Spu_reg value );
	void pulseINTERP_COEF_WR_wdata( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_COEF_WR_waddr( Spu_reg value );
	void pulseINTERP_COEF_WR_waddr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_COEF_WR_coef_group( Spu_reg value );
	void pulseINTERP_COEF_WR_coef_group( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outINTERP_COEF_WR_wr_en( Spu_reg value );
	void pulseINTERP_COEF_WR_wr_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outINTERP_COEF_WR( Spu_reg value );
	void outINTERP_COEF_WR(  );


	void setCOMPRESS_MAIN_rate( Spu_reg value );
	void setCOMPRESS_MAIN_peak( Spu_reg value );
	void setCOMPRESS_MAIN_en( Spu_reg value );

	void setCOMPRESS_MAIN( Spu_reg value );
	void outCOMPRESS_MAIN_rate( Spu_reg value );
	void pulseCOMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOMPRESS_MAIN_peak( Spu_reg value );
	void pulseCOMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOMPRESS_MAIN_en( Spu_reg value );
	void pulseCOMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCOMPRESS_MAIN( Spu_reg value );
	void outCOMPRESS_MAIN(  );


	Spu_reg inCOMPRESS_MAIN(  );

	void setCOMPRESS_ZOOM_rate( Spu_reg value );
	void setCOMPRESS_ZOOM_peak( Spu_reg value );
	void setCOMPRESS_ZOOM_en( Spu_reg value );

	void setCOMPRESS_ZOOM( Spu_reg value );
	void outCOMPRESS_ZOOM_rate( Spu_reg value );
	void pulseCOMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOMPRESS_ZOOM_peak( Spu_reg value );
	void pulseCOMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOMPRESS_ZOOM_en( Spu_reg value );
	void pulseCOMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCOMPRESS_ZOOM( Spu_reg value );
	void outCOMPRESS_ZOOM(  );


	Spu_reg inCOMPRESS_ZOOM(  );

	void setLA_FINE_DELAY_MAIN_delay( Spu_reg value );
	void setLA_FINE_DELAY_MAIN_en( Spu_reg value );

	void setLA_FINE_DELAY_MAIN( Spu_reg value );
	void outLA_FINE_DELAY_MAIN_delay( Spu_reg value );
	void pulseLA_FINE_DELAY_MAIN_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outLA_FINE_DELAY_MAIN_en( Spu_reg value );
	void pulseLA_FINE_DELAY_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outLA_FINE_DELAY_MAIN( Spu_reg value );
	void outLA_FINE_DELAY_MAIN(  );


	Spu_reg inLA_FINE_DELAY_MAIN(  );

	void setLA_FINE_DELAY_ZOOM_delay( Spu_reg value );
	void setLA_FINE_DELAY_ZOOM_en( Spu_reg value );

	void setLA_FINE_DELAY_ZOOM( Spu_reg value );
	void outLA_FINE_DELAY_ZOOM_delay( Spu_reg value );
	void pulseLA_FINE_DELAY_ZOOM_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outLA_FINE_DELAY_ZOOM_en( Spu_reg value );
	void pulseLA_FINE_DELAY_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outLA_FINE_DELAY_ZOOM( Spu_reg value );
	void outLA_FINE_DELAY_ZOOM(  );


	Spu_reg inLA_FINE_DELAY_ZOOM(  );

	void setLA_COMP_MAIN_rate( Spu_reg value );
	void setLA_COMP_MAIN_en( Spu_reg value );

	void setLA_COMP_MAIN( Spu_reg value );
	void outLA_COMP_MAIN_rate( Spu_reg value );
	void pulseLA_COMP_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outLA_COMP_MAIN_en( Spu_reg value );
	void pulseLA_COMP_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outLA_COMP_MAIN( Spu_reg value );
	void outLA_COMP_MAIN(  );


	Spu_reg inLA_COMP_MAIN(  );

	void setLA_COMP_ZOOM_rate( Spu_reg value );
	void setLA_COMP_ZOOM_en( Spu_reg value );

	void setLA_COMP_ZOOM( Spu_reg value );
	void outLA_COMP_ZOOM_rate( Spu_reg value );
	void pulseLA_COMP_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outLA_COMP_ZOOM_en( Spu_reg value );
	void pulseLA_COMP_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outLA_COMP_ZOOM( Spu_reg value );
	void outLA_COMP_ZOOM(  );


	Spu_reg inLA_COMP_ZOOM(  );

	void setTX_LEN_CH( Spu_reg value );
	void outTX_LEN_CH( Spu_reg value );
	void outTX_LEN_CH(  );

	Spu_reg inTX_LEN_CH(  );

	void setTX_LEN_LA( Spu_reg value );
	void outTX_LEN_LA( Spu_reg value );
	void outTX_LEN_LA(  );

	Spu_reg inTX_LEN_LA(  );

	void setTX_LEN_ZOOM_CH( Spu_reg value );
	void outTX_LEN_ZOOM_CH( Spu_reg value );
	void outTX_LEN_ZOOM_CH(  );

	Spu_reg inTX_LEN_ZOOM_CH(  );

	void setTX_LEN_ZOOM_LA( Spu_reg value );
	void outTX_LEN_ZOOM_LA( Spu_reg value );
	void outTX_LEN_ZOOM_LA(  );

	Spu_reg inTX_LEN_ZOOM_LA(  );

	void setTX_LEN_TRACE_CH( Spu_reg value );
	void outTX_LEN_TRACE_CH( Spu_reg value );
	void outTX_LEN_TRACE_CH(  );

	Spu_reg inTX_LEN_TRACE_CH(  );

	void setTX_LEN_TRACE_LA( Spu_reg value );
	void outTX_LEN_TRACE_LA( Spu_reg value );
	void outTX_LEN_TRACE_LA(  );

	Spu_reg inTX_LEN_TRACE_LA(  );

	void setTX_LEN_COL_CH( Spu_reg value );
	void outTX_LEN_COL_CH( Spu_reg value );
	void outTX_LEN_COL_CH(  );

	Spu_reg inTX_LEN_COL_CH(  );

	void setTX_LEN_COL_LA( Spu_reg value );
	void outTX_LEN_COL_LA( Spu_reg value );
	void outTX_LEN_COL_LA(  );

	Spu_reg inTX_LEN_COL_LA(  );

	void setDEBUG_ACQ_samp_extract_vld( Spu_reg value );
	void setDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value );
	void setDEBUG_ACQ_acq_dly_cfg( Spu_reg value );
	void setDEBUG_ACQ_trig_src_type( Spu_reg value );

	void setDEBUG_ACQ( Spu_reg value );
	void outDEBUG_ACQ_samp_extract_vld( Spu_reg value );
	void pulseDEBUG_ACQ_samp_extract_vld( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value );
	void pulseDEBUG_ACQ_acq_dly_cfg_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_ACQ_acq_dly_cfg( Spu_reg value );
	void pulseDEBUG_ACQ_acq_dly_cfg( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_ACQ_trig_src_type( Spu_reg value );
	void pulseDEBUG_ACQ_trig_src_type( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outDEBUG_ACQ( Spu_reg value );
	void outDEBUG_ACQ(  );


	Spu_reg inDEBUG_ACQ(  );

	void setDEBUG_TX_spu_tx_cnt( Spu_reg value );
	void setDEBUG_TX_spu_frm_fsm( Spu_reg value );
	void setDEBUG_TX_tx_cnt_clr( Spu_reg value );
	void setDEBUG_TX_v_tx_bypass( Spu_reg value );
	void setDEBUG_TX_frm_head_jmp( Spu_reg value );

	void setDEBUG_TX( Spu_reg value );
	void outDEBUG_TX_spu_tx_cnt( Spu_reg value );
	void pulseDEBUG_TX_spu_tx_cnt( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_TX_spu_frm_fsm( Spu_reg value );
	void pulseDEBUG_TX_spu_frm_fsm( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_TX_tx_cnt_clr( Spu_reg value );
	void pulseDEBUG_TX_tx_cnt_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_TX_v_tx_bypass( Spu_reg value );
	void pulseDEBUG_TX_v_tx_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_TX_frm_head_jmp( Spu_reg value );
	void pulseDEBUG_TX_frm_head_jmp( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outDEBUG_TX( Spu_reg value );
	void outDEBUG_TX(  );


	Spu_reg inDEBUG_TX(  );

	void setDEBUG_MEM_trig_tpu_coarse( Spu_reg value );
	void setDEBUG_MEM_mem_bram_dly( Spu_reg value );
	void setDEBUG_MEM_trig_offset_dot_pre( Spu_reg value );
	void setDEBUG_MEM_trig_dly_dot_out( Spu_reg value );
	void setDEBUG_MEM_trig_coarse_fine( Spu_reg value );
	void setDEBUG_MEM_mem_last_dot( Spu_reg value );

	void setDEBUG_MEM( Spu_reg value );
	void outDEBUG_MEM_trig_tpu_coarse( Spu_reg value );
	void pulseDEBUG_MEM_trig_tpu_coarse( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_MEM_mem_bram_dly( Spu_reg value );
	void pulseDEBUG_MEM_mem_bram_dly( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_MEM_trig_offset_dot_pre( Spu_reg value );
	void pulseDEBUG_MEM_trig_offset_dot_pre( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_MEM_trig_dly_dot_out( Spu_reg value );
	void pulseDEBUG_MEM_trig_dly_dot_out( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_MEM_trig_coarse_fine( Spu_reg value );
	void pulseDEBUG_MEM_trig_coarse_fine( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outDEBUG_MEM_mem_last_dot( Spu_reg value );
	void pulseDEBUG_MEM_mem_last_dot( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outDEBUG_MEM( Spu_reg value );
	void outDEBUG_MEM(  );


	Spu_reg inDEBUG_MEM(  );

	Spu_reg inDEBUG_SCAN(  );

	Spu_reg inRECORD_SUM(  );

	void setSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value );

	void setSCAN_ROLL_FRM( Spu_reg value );
	void outSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value );
	void pulseSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outSCAN_ROLL_FRM( Spu_reg value );
	void outSCAN_ROLL_FRM(  );


	Spu_reg inSCAN_ROLL_FRM(  );

	void setADC_AVG_rate( Spu_reg value );
	void setADC_AVG_done( Spu_reg value );
	void setADC_AVG_en( Spu_reg value );

	void setADC_AVG( Spu_reg value );
	void outADC_AVG_rate( Spu_reg value );
	void pulseADC_AVG_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_AVG_done( Spu_reg value );
	void pulseADC_AVG_done( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outADC_AVG_en( Spu_reg value );
	void pulseADC_AVG_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outADC_AVG( Spu_reg value );
	void outADC_AVG(  );


	Spu_reg inADC_AVG(  );

	Spu_reg inADC_AVG_RES_AB(  );

	Spu_reg inADC_AVG_RES_CD(  );

	Spu_reg inADC_AVG_MAX(  );

	Spu_reg inADC_AVG_MIN(  );

	Spu_reg inLA_SA_RES(  );

	void setZONE_TRIG_AUX_pulse_width( Spu_reg value );
	void setZONE_TRIG_AUX_inv( Spu_reg value );
	void setZONE_TRIG_AUX_pass_fail( Spu_reg value );

	void setZONE_TRIG_AUX( Spu_reg value );
	void outZONE_TRIG_AUX_pulse_width( Spu_reg value );
	void pulseZONE_TRIG_AUX_pulse_width( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_AUX_inv( Spu_reg value );
	void pulseZONE_TRIG_AUX_inv( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_AUX_pass_fail( Spu_reg value );
	void pulseZONE_TRIG_AUX_pass_fail( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outZONE_TRIG_AUX( Spu_reg value );
	void outZONE_TRIG_AUX(  );


	Spu_reg inZONE_TRIG_AUX(  );

	void setZONE_TRIG_TAB1_threshold_l( Spu_reg value );
	void setZONE_TRIG_TAB1_threshold_h( Spu_reg value );
	void setZONE_TRIG_TAB1_threshold_en( Spu_reg value );
	void setZONE_TRIG_TAB1_column_addr( Spu_reg value );
	void setZONE_TRIG_TAB1_tab_index( Spu_reg value );

	void setZONE_TRIG_TAB1( Spu_reg value );
	void outZONE_TRIG_TAB1_threshold_l( Spu_reg value );
	void pulseZONE_TRIG_TAB1_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB1_threshold_h( Spu_reg value );
	void pulseZONE_TRIG_TAB1_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB1_threshold_en( Spu_reg value );
	void pulseZONE_TRIG_TAB1_threshold_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB1_column_addr( Spu_reg value );
	void pulseZONE_TRIG_TAB1_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB1_tab_index( Spu_reg value );
	void pulseZONE_TRIG_TAB1_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outZONE_TRIG_TAB1( Spu_reg value );
	void outZONE_TRIG_TAB1(  );


	void setZONE_TRIG_TAB2_threshold_l( Spu_reg value );
	void setZONE_TRIG_TAB2_threshold_h( Spu_reg value );
	void setZONE_TRIG_TAB2_threshold_en( Spu_reg value );
	void setZONE_TRIG_TAB2_column_addr( Spu_reg value );
	void setZONE_TRIG_TAB2_tab_index( Spu_reg value );

	void setZONE_TRIG_TAB2( Spu_reg value );
	void outZONE_TRIG_TAB2_threshold_l( Spu_reg value );
	void pulseZONE_TRIG_TAB2_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB2_threshold_h( Spu_reg value );
	void pulseZONE_TRIG_TAB2_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB2_threshold_en( Spu_reg value );
	void pulseZONE_TRIG_TAB2_threshold_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB2_column_addr( Spu_reg value );
	void pulseZONE_TRIG_TAB2_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outZONE_TRIG_TAB2_tab_index( Spu_reg value );
	void pulseZONE_TRIG_TAB2_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outZONE_TRIG_TAB2( Spu_reg value );
	void outZONE_TRIG_TAB2(  );


	void setMASK_TAB_threshold_l( Spu_reg value );
	void setMASK_TAB_threshold_l_less_en( Spu_reg value );
	void setMASK_TAB_threshold_h( Spu_reg value );
	void setMASK_TAB_threshold_h_greater_en( Spu_reg value );
	void setMASK_TAB_column_addr( Spu_reg value );
	void setMASK_TAB_tab_index( Spu_reg value );

	void setMASK_TAB( Spu_reg value );
	void outMASK_TAB_threshold_l( Spu_reg value );
	void pulseMASK_TAB_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_threshold_l_less_en( Spu_reg value );
	void pulseMASK_TAB_threshold_l_less_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_threshold_h( Spu_reg value );
	void pulseMASK_TAB_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_threshold_h_greater_en( Spu_reg value );
	void pulseMASK_TAB_threshold_h_greater_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_column_addr( Spu_reg value );
	void pulseMASK_TAB_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_tab_index( Spu_reg value );
	void pulseMASK_TAB_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outMASK_TAB( Spu_reg value );
	void outMASK_TAB(  );


	Spu_reg inMASK_TAB(  );

	void setMASK_TAB_EX_threshold_l( Spu_reg value );
	void setMASK_TAB_EX_threshold_l_greater_en( Spu_reg value );
	void setMASK_TAB_EX_threshold_h( Spu_reg value );
	void setMASK_TAB_EX_threshold_h_less_en( Spu_reg value );
	void setMASK_TAB_EX_column_addr( Spu_reg value );
	void setMASK_TAB_EX_tab_index( Spu_reg value );

	void setMASK_TAB_EX( Spu_reg value );
	void outMASK_TAB_EX_threshold_l( Spu_reg value );
	void pulseMASK_TAB_EX_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_EX_threshold_l_greater_en( Spu_reg value );
	void pulseMASK_TAB_EX_threshold_l_greater_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_EX_threshold_h( Spu_reg value );
	void pulseMASK_TAB_EX_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_EX_threshold_h_less_en( Spu_reg value );
	void pulseMASK_TAB_EX_threshold_h_less_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_EX_column_addr( Spu_reg value );
	void pulseMASK_TAB_EX_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_TAB_EX_tab_index( Spu_reg value );
	void pulseMASK_TAB_EX_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outMASK_TAB_EX( Spu_reg value );
	void outMASK_TAB_EX(  );


	Spu_reg inMASK_TAB_EX(  );

	void setMASK_COMPRESS_rate( Spu_reg value );
	void setMASK_COMPRESS_peak( Spu_reg value );
	void setMASK_COMPRESS_en( Spu_reg value );

	void setMASK_COMPRESS( Spu_reg value );
	void outMASK_COMPRESS_rate( Spu_reg value );
	void pulseMASK_COMPRESS_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_COMPRESS_peak( Spu_reg value );
	void pulseMASK_COMPRESS_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_COMPRESS_en( Spu_reg value );
	void pulseMASK_COMPRESS_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outMASK_COMPRESS( Spu_reg value );
	void outMASK_COMPRESS(  );


	Spu_reg inMASK_COMPRESS(  );

	void setMASK_CTRL_cross_index( Spu_reg value );
	void setMASK_CTRL_mask_index_en( Spu_reg value );
	void setMASK_CTRL_zone1_trig_tab_en( Spu_reg value );
	void setMASK_CTRL_zone2_trig_tab_en( Spu_reg value );
	void setMASK_CTRL_zone1_trig_cross( Spu_reg value );
	void setMASK_CTRL_zone2_trig_cross( Spu_reg value );
	void setMASK_CTRL_mask_result_rd( Spu_reg value );
	void setMASK_CTRL_sum_clr( Spu_reg value );
	void setMASK_CTRL_cross_clr( Spu_reg value );
	void setMASK_CTRL_mask_we( Spu_reg value );

	void setMASK_CTRL( Spu_reg value );
	void outMASK_CTRL_cross_index( Spu_reg value );
	void pulseMASK_CTRL_cross_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_mask_index_en( Spu_reg value );
	void pulseMASK_CTRL_mask_index_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_zone1_trig_tab_en( Spu_reg value );
	void pulseMASK_CTRL_zone1_trig_tab_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_zone2_trig_tab_en( Spu_reg value );
	void pulseMASK_CTRL_zone2_trig_tab_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_zone1_trig_cross( Spu_reg value );
	void pulseMASK_CTRL_zone1_trig_cross( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_zone2_trig_cross( Spu_reg value );
	void pulseMASK_CTRL_zone2_trig_cross( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_mask_result_rd( Spu_reg value );
	void pulseMASK_CTRL_mask_result_rd( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_sum_clr( Spu_reg value );
	void pulseMASK_CTRL_sum_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_cross_clr( Spu_reg value );
	void pulseMASK_CTRL_cross_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_CTRL_mask_we( Spu_reg value );
	void pulseMASK_CTRL_mask_we( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outMASK_CTRL( Spu_reg value );
	void outMASK_CTRL(  );


	Spu_reg inMASK_CTRL(  );

	Spu_reg inMASK_FRM_CNT_L(  );

	Spu_reg inMASK_FRM_CNT_H(  );

	Spu_reg inMASK_CROSS_CNT_L_CHA(  );

	Spu_reg inMASK_CROSS_CNT_H_CHA(  );

	Spu_reg inMASK_CROSS_CNT_L_CHB(  );

	Spu_reg inMASK_CROSS_CNT_H_CHB(  );

	Spu_reg inMASK_CROSS_CNT_L_CHC(  );

	Spu_reg inMASK_CROSS_CNT_H_CHC(  );

	Spu_reg inMASK_CROSS_CNT_L_CHD(  );

	Spu_reg inMASK_CROSS_CNT_H_CHD(  );

	void setMASK_OUTPUT_pulse_width( Spu_reg value );
	void setMASK_OUTPUT_inv( Spu_reg value );
	void setMASK_OUTPUT_pass_fail( Spu_reg value );

	void setMASK_OUTPUT( Spu_reg value );
	void outMASK_OUTPUT_pulse_width( Spu_reg value );
	void pulseMASK_OUTPUT_pulse_width( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_OUTPUT_inv( Spu_reg value );
	void pulseMASK_OUTPUT_inv( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outMASK_OUTPUT_pass_fail( Spu_reg value );
	void pulseMASK_OUTPUT_pass_fail( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outMASK_OUTPUT( Spu_reg value );
	void outMASK_OUTPUT(  );


	Spu_reg inMASK_OUTPUT(  );

	void setSEARCH_OFFSET_CH( Spu_reg value );
	void outSEARCH_OFFSET_CH( Spu_reg value );
	void outSEARCH_OFFSET_CH(  );

	Spu_reg inSEARCH_OFFSET_CH(  );

	void setSEARCH_LEN_CH( Spu_reg value );
	void outSEARCH_LEN_CH( Spu_reg value );
	void outSEARCH_LEN_CH(  );

	Spu_reg inSEARCH_LEN_CH(  );

	void setSEARCH_OFFSET_LA( Spu_reg value );
	void outSEARCH_OFFSET_LA( Spu_reg value );
	void outSEARCH_OFFSET_LA(  );

	Spu_reg inSEARCH_OFFSET_LA(  );

	void setSEARCH_LEN_LA( Spu_reg value );
	void outSEARCH_LEN_LA( Spu_reg value );
	void outSEARCH_LEN_LA(  );

	Spu_reg inSEARCH_LEN_LA(  );

	void setTIME_TAG_L( Spu_reg value );
	void outTIME_TAG_L( Spu_reg value );
	void outTIME_TAG_L(  );

	Spu_reg inTIME_TAG_L(  );

	void setTIME_TAG_H( Spu_reg value );
	void outTIME_TAG_H( Spu_reg value );
	void outTIME_TAG_H(  );

	Spu_reg inTIME_TAG_H(  );

	void setFINE_TRIG_POSITION( Spu_reg value );
	void outFINE_TRIG_POSITION( Spu_reg value );
	void outFINE_TRIG_POSITION(  );

	Spu_reg inFINE_TRIG_POSITION(  );

	void setTIME_TAG_CTRL_rd_en( Spu_reg value );
	void setTIME_TAG_CTRL_rec_first_done( Spu_reg value );

	void setTIME_TAG_CTRL( Spu_reg value );
	void outTIME_TAG_CTRL_rd_en( Spu_reg value );
	void pulseTIME_TAG_CTRL_rd_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outTIME_TAG_CTRL_rec_first_done( Spu_reg value );
	void pulseTIME_TAG_CTRL_rec_first_done( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outTIME_TAG_CTRL( Spu_reg value );
	void outTIME_TAG_CTRL(  );


	Spu_reg inTIME_TAG_CTRL(  );

	void setIMPORT_INFO_import_done( Spu_reg value );

	void setIMPORT_INFO( Spu_reg value );
	void outIMPORT_INFO_import_done( Spu_reg value );
	void pulseIMPORT_INFO_import_done( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outIMPORT_INFO( Spu_reg value );
	void outIMPORT_INFO(  );


	Spu_reg inIMPORT_INFO(  );

	Spu_reg inTIME_TAG_L_REC_FIRST(  );

	Spu_reg inTIME_TAG_H_REC_FIRST(  );

	void setTRACE_AVG_RATE_X( Spu_reg value );
	void outTRACE_AVG_RATE_X( Spu_reg value );
	void outTRACE_AVG_RATE_X(  );

	Spu_reg inTRACE_AVG_RATE_X(  );

	void setTRACE_AVG_RATE_Y( Spu_reg value );
	void outTRACE_AVG_RATE_Y( Spu_reg value );
	void outTRACE_AVG_RATE_Y(  );

	Spu_reg inTRACE_AVG_RATE_Y(  );

	void setLINEAR_INTX_CTRL_mian_bypass( Spu_reg value );
	void setLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value );

	void setLINEAR_INTX_CTRL( Spu_reg value );
	void outLINEAR_INTX_CTRL_mian_bypass( Spu_reg value );
	void pulseLINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value );
	void pulseLINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outLINEAR_INTX_CTRL( Spu_reg value );
	void outLINEAR_INTX_CTRL(  );


	Spu_reg inLINEAR_INTX_CTRL(  );

	void setLINEAR_INTX_INIT( Spu_reg value );
	void outLINEAR_INTX_INIT( Spu_reg value );
	void outLINEAR_INTX_INIT(  );

	Spu_reg inLINEAR_INTX_INIT(  );

	void setLINEAR_INTX_STEP( Spu_reg value );
	void outLINEAR_INTX_STEP( Spu_reg value );
	void outLINEAR_INTX_STEP(  );

	Spu_reg inLINEAR_INTX_STEP(  );

	void setLINEAR_INTX_INIT_ZOOM( Spu_reg value );
	void outLINEAR_INTX_INIT_ZOOM( Spu_reg value );
	void outLINEAR_INTX_INIT_ZOOM(  );

	Spu_reg inLINEAR_INTX_INIT_ZOOM(  );

	void setLINEAR_INTX_STEP_ZOOM( Spu_reg value );
	void outLINEAR_INTX_STEP_ZOOM( Spu_reg value );
	void outLINEAR_INTX_STEP_ZOOM(  );

	Spu_reg inLINEAR_INTX_STEP_ZOOM(  );

	void setWAVE_OFFSET_EYE( Spu_reg value );
	void outWAVE_OFFSET_EYE( Spu_reg value );
	void outWAVE_OFFSET_EYE(  );

	Spu_reg inWAVE_OFFSET_EYE(  );

	void setWAVE_LEN_EYE( Spu_reg value );
	void outWAVE_LEN_EYE( Spu_reg value );
	void outWAVE_LEN_EYE(  );

	Spu_reg inWAVE_LEN_EYE(  );

	void setCOMPRESS_EYE_rate( Spu_reg value );
	void setCOMPRESS_EYE_peak( Spu_reg value );
	void setCOMPRESS_EYE_en( Spu_reg value );

	void setCOMPRESS_EYE( Spu_reg value );
	void outCOMPRESS_EYE_rate( Spu_reg value );
	void pulseCOMPRESS_EYE_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOMPRESS_EYE_peak( Spu_reg value );
	void pulseCOMPRESS_EYE_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outCOMPRESS_EYE_en( Spu_reg value );
	void pulseCOMPRESS_EYE_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outCOMPRESS_EYE( Spu_reg value );
	void outCOMPRESS_EYE(  );


	Spu_reg inCOMPRESS_EYE(  );

	void setTX_LEN_EYE( Spu_reg value );
	void outTX_LEN_EYE( Spu_reg value );
	void outTX_LEN_EYE(  );

	Spu_reg inTX_LEN_EYE(  );

	void setLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value );
	void setLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value );

	void setLA_LINEAR_INTX_CTRL( Spu_reg value );
	void outLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value );
	void pulseLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value );
	void pulseLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outLA_LINEAR_INTX_CTRL( Spu_reg value );
	void outLA_LINEAR_INTX_CTRL(  );


	Spu_reg inLA_LINEAR_INTX_CTRL(  );

	void setLA_LINEAR_INTX_INIT( Spu_reg value );
	void outLA_LINEAR_INTX_INIT( Spu_reg value );
	void outLA_LINEAR_INTX_INIT(  );

	Spu_reg inLA_LINEAR_INTX_INIT(  );

	void setLA_LINEAR_INTX_STEP( Spu_reg value );
	void outLA_LINEAR_INTX_STEP( Spu_reg value );
	void outLA_LINEAR_INTX_STEP(  );

	Spu_reg inLA_LINEAR_INTX_STEP(  );

	void setLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value );
	void outLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value );
	void outLA_LINEAR_INTX_INIT_ZOOM(  );

	Spu_reg inLA_LINEAR_INTX_INIT_ZOOM(  );

	void setLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value );
	void outLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value );
	void outLA_LINEAR_INTX_STEP_ZOOM(  );

	Spu_reg inLA_LINEAR_INTX_STEP_ZOOM(  );

	void setTRACE_COMPRESS_MAIN_rate( Spu_reg value );
	void setTRACE_COMPRESS_MAIN_peak( Spu_reg value );
	void setTRACE_COMPRESS_MAIN_en( Spu_reg value );

	void setTRACE_COMPRESS_MAIN( Spu_reg value );
	void outTRACE_COMPRESS_MAIN_rate( Spu_reg value );
	void pulseTRACE_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outTRACE_COMPRESS_MAIN_peak( Spu_reg value );
	void pulseTRACE_COMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outTRACE_COMPRESS_MAIN_en( Spu_reg value );
	void pulseTRACE_COMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outTRACE_COMPRESS_MAIN( Spu_reg value );
	void outTRACE_COMPRESS_MAIN(  );


	Spu_reg inTRACE_COMPRESS_MAIN(  );

	void setTRACE_COMPRESS_ZOOM_rate( Spu_reg value );
	void setTRACE_COMPRESS_ZOOM_peak( Spu_reg value );
	void setTRACE_COMPRESS_ZOOM_en( Spu_reg value );

	void setTRACE_COMPRESS_ZOOM( Spu_reg value );
	void outTRACE_COMPRESS_ZOOM_rate( Spu_reg value );
	void pulseTRACE_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outTRACE_COMPRESS_ZOOM_peak( Spu_reg value );
	void pulseTRACE_COMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );
	void outTRACE_COMPRESS_ZOOM_en( Spu_reg value );
	void pulseTRACE_COMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outTRACE_COMPRESS_ZOOM( Spu_reg value );
	void outTRACE_COMPRESS_ZOOM(  );


	Spu_reg inTRACE_COMPRESS_ZOOM(  );

	void setZYNQ_COMPRESS_MAIN_rate( Spu_reg value );

	void setZYNQ_COMPRESS_MAIN( Spu_reg value );
	void outZYNQ_COMPRESS_MAIN_rate( Spu_reg value );
	void pulseZYNQ_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outZYNQ_COMPRESS_MAIN( Spu_reg value );
	void outZYNQ_COMPRESS_MAIN(  );


	Spu_reg inZYNQ_COMPRESS_MAIN(  );

	void setZYNQ_COMPRESS_ZOOM_rate( Spu_reg value );

	void setZYNQ_COMPRESS_ZOOM( Spu_reg value );
	void outZYNQ_COMPRESS_ZOOM_rate( Spu_reg value );
	void pulseZYNQ_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0 );

	void outZYNQ_COMPRESS_ZOOM( Spu_reg value );
	void outZYNQ_COMPRESS_ZOOM(  );


	Spu_reg inZYNQ_COMPRESS_ZOOM(  );

	Spu_reg inDEBUG_TX_LA(  );

	Spu_reg inDEBUG_TX_FRM(  );

	Spu_reg inIMPORT_CNT_CH(  );

	Spu_reg inIMPORT_CNT_LA(  );

	Spu_reg inDEBUG_LINEAR_INTX1(  );

	Spu_reg inDEBUG_LINEAR_INTX2(  );

	Spu_reg inDEBUG_DDR(  );

	Spu_reg inDEBUG_TX_LEN_BURST(  );

public:
	cache_addr addr_VERSION();
	cache_addr addr_TEST();
	cache_addr addr_CTRL();
	cache_addr addr_DEBUG();

	cache_addr addr_ADC_DATA_IDELAY();
	cache_addr addr_ADC_DATA_CHECK();
	cache_addr addr_ADC_CHN_ALIGN_CHK();
	cache_addr addr_ADC_CHN_ALIGN();

	cache_addr addr_PRE_PROC();
	cache_addr addr_FILTER();
	cache_addr addr_DOWN_SAMP_FAST();
	cache_addr addr_DOWN_SAMP_SLOW_H();

	cache_addr addr_DOWN_SAMP_SLOW_L();
	cache_addr addr_LA_DELAY();
	cache_addr addr_CHN_DELAY_AB();
	cache_addr addr_CHN_DELAY_CD();

	cache_addr addr_RANDOM_RANGE();
	cache_addr addr_EXT_TRIG_IDEALY();
	cache_addr addr_SEGMENT_SIZE();
	cache_addr addr_RECORD_LEN();

	cache_addr addr_WAVE_LEN_MAIN();
	cache_addr addr_WAVE_LEN_ZOOM();
	cache_addr addr_WAVE_OFFSET_MAIN();
	cache_addr addr_WAVE_OFFSET_ZOOM();

	cache_addr addr_MEAS_OFFSET_CH();
	cache_addr addr_MEAS_LEN_CH();
	cache_addr addr_MEAS_OFFSET_LA();
	cache_addr addr_MEAS_LEN_LA();

	cache_addr addr_COARSE_DELAY_MAIN();
	cache_addr addr_COARSE_DELAY_ZOOM();
	cache_addr addr_FINE_DELAY_MAIN();
	cache_addr addr_FINE_DELAY_ZOOM();

	cache_addr addr_INTERP_MULT_MAIN();
	cache_addr addr_INTERP_MULT_ZOOM();
	cache_addr addr_INTERP_MULT_MAIN_FINE();
	cache_addr addr_INTERP_MULT_EYE();

	cache_addr addr_INTERP_COEF_TAP();
	cache_addr addr_INTERP_COEF_WR();
	cache_addr addr_COMPRESS_MAIN();
	cache_addr addr_COMPRESS_ZOOM();

	cache_addr addr_LA_FINE_DELAY_MAIN();
	cache_addr addr_LA_FINE_DELAY_ZOOM();
	cache_addr addr_LA_COMP_MAIN();
	cache_addr addr_LA_COMP_ZOOM();

	cache_addr addr_TX_LEN_CH();
	cache_addr addr_TX_LEN_LA();
	cache_addr addr_TX_LEN_ZOOM_CH();
	cache_addr addr_TX_LEN_ZOOM_LA();

	cache_addr addr_TX_LEN_TRACE_CH();
	cache_addr addr_TX_LEN_TRACE_LA();
	cache_addr addr_TX_LEN_COL_CH();
	cache_addr addr_TX_LEN_COL_LA();

	cache_addr addr_DEBUG_ACQ();
	cache_addr addr_DEBUG_TX();
	cache_addr addr_DEBUG_MEM();
	cache_addr addr_DEBUG_SCAN();

	cache_addr addr_RECORD_SUM();
	cache_addr addr_SCAN_ROLL_FRM();
	cache_addr addr_ADC_AVG();
	cache_addr addr_ADC_AVG_RES_AB();

	cache_addr addr_ADC_AVG_RES_CD();
	cache_addr addr_ADC_AVG_MAX();
	cache_addr addr_ADC_AVG_MIN();
	cache_addr addr_LA_SA_RES();

	cache_addr addr_ZONE_TRIG_AUX();
	cache_addr addr_ZONE_TRIG_TAB1();
	cache_addr addr_ZONE_TRIG_TAB2();
	cache_addr addr_MASK_TAB();

	cache_addr addr_MASK_TAB_EX();
	cache_addr addr_MASK_COMPRESS();
	cache_addr addr_MASK_CTRL();
	cache_addr addr_MASK_FRM_CNT_L();

	cache_addr addr_MASK_FRM_CNT_H();
	cache_addr addr_MASK_CROSS_CNT_L_CHA();
	cache_addr addr_MASK_CROSS_CNT_H_CHA();
	cache_addr addr_MASK_CROSS_CNT_L_CHB();

	cache_addr addr_MASK_CROSS_CNT_H_CHB();
	cache_addr addr_MASK_CROSS_CNT_L_CHC();
	cache_addr addr_MASK_CROSS_CNT_H_CHC();
	cache_addr addr_MASK_CROSS_CNT_L_CHD();

	cache_addr addr_MASK_CROSS_CNT_H_CHD();
	cache_addr addr_MASK_OUTPUT();
	cache_addr addr_SEARCH_OFFSET_CH();
	cache_addr addr_SEARCH_LEN_CH();

	cache_addr addr_SEARCH_OFFSET_LA();
	cache_addr addr_SEARCH_LEN_LA();
	cache_addr addr_TIME_TAG_L();
	cache_addr addr_TIME_TAG_H();

	cache_addr addr_FINE_TRIG_POSITION();
	cache_addr addr_TIME_TAG_CTRL();
	cache_addr addr_IMPORT_INFO();
	cache_addr addr_TIME_TAG_L_REC_FIRST();

	cache_addr addr_TIME_TAG_H_REC_FIRST();
	cache_addr addr_TRACE_AVG_RATE_X();
	cache_addr addr_TRACE_AVG_RATE_Y();
	cache_addr addr_LINEAR_INTX_CTRL();

	cache_addr addr_LINEAR_INTX_INIT();
	cache_addr addr_LINEAR_INTX_STEP();
	cache_addr addr_LINEAR_INTX_INIT_ZOOM();
	cache_addr addr_LINEAR_INTX_STEP_ZOOM();

	cache_addr addr_WAVE_OFFSET_EYE();
	cache_addr addr_WAVE_LEN_EYE();
	cache_addr addr_COMPRESS_EYE();
	cache_addr addr_TX_LEN_EYE();

	cache_addr addr_LA_LINEAR_INTX_CTRL();
	cache_addr addr_LA_LINEAR_INTX_INIT();
	cache_addr addr_LA_LINEAR_INTX_STEP();
	cache_addr addr_LA_LINEAR_INTX_INIT_ZOOM();

	cache_addr addr_LA_LINEAR_INTX_STEP_ZOOM();
	cache_addr addr_TRACE_COMPRESS_MAIN();
	cache_addr addr_TRACE_COMPRESS_ZOOM();
	cache_addr addr_ZYNQ_COMPRESS_MAIN();

	cache_addr addr_ZYNQ_COMPRESS_ZOOM();
	cache_addr addr_DEBUG_TX_LA();
	cache_addr addr_DEBUG_TX_FRM();
	cache_addr addr_IMPORT_CNT_CH();

	cache_addr addr_IMPORT_CNT_LA();
	cache_addr addr_DEBUG_LINEAR_INTX1();
	cache_addr addr_DEBUG_LINEAR_INTX2();
	cache_addr addr_DEBUG_DDR();

	cache_addr addr_DEBUG_TX_LEN_BURST();
};
#define _remap_Spu_()\
	mapIn( &VERSION, 0x1400 + mAddrBase );\
	mapIn( &TEST, 0x1404 + mAddrBase );\
	mapIn( &CTRL, 0x1408 + mAddrBase );\
	mapIn( &DEBUG, 0x140C + mAddrBase );\
	\
	mapIn( &ADC_DATA_IDELAY, 0x1410 + mAddrBase );\
	mapIn( &ADC_DATA_CHECK, 0x1414 + mAddrBase );\
	mapIn( &ADC_CHN_ALIGN_CHK, 0x1418 + mAddrBase );\
	mapIn( &ADC_CHN_ALIGN, 0x141C + mAddrBase );\
	\
	mapIn( &PRE_PROC, 0x1420 + mAddrBase );\
	mapIn( &FILTER, 0x1424 + mAddrBase );\
	mapIn( &DOWN_SAMP_FAST, 0x1428 + mAddrBase );\
	mapIn( &DOWN_SAMP_SLOW_H, 0x142C + mAddrBase );\
	\
	mapIn( &DOWN_SAMP_SLOW_L, 0x1430 + mAddrBase );\
	mapIn( &LA_DELAY, 0x1434 + mAddrBase );\
	mapIn( &CHN_DELAY_AB, 0x1438 + mAddrBase );\
	mapIn( &CHN_DELAY_CD, 0x143C + mAddrBase );\
	\
	mapIn( &RANDOM_RANGE, 0x1440 + mAddrBase );\
	mapIn( &EXT_TRIG_IDEALY, 0x1444 + mAddrBase );\
	mapIn( &SEGMENT_SIZE, 0x144C + mAddrBase );\
	mapIn( &RECORD_LEN, 0x1450 + mAddrBase );\
	\
	mapIn( &WAVE_LEN_MAIN, 0x1454 + mAddrBase );\
	mapIn( &WAVE_LEN_ZOOM, 0x1458 + mAddrBase );\
	mapIn( &WAVE_OFFSET_MAIN, 0x145C + mAddrBase );\
	mapIn( &WAVE_OFFSET_ZOOM, 0x1460 + mAddrBase );\
	\
	mapIn( &MEAS_OFFSET_CH, 0x1470 + mAddrBase );\
	mapIn( &MEAS_LEN_CH, 0x1474 + mAddrBase );\
	mapIn( &MEAS_OFFSET_LA, 0x1478 + mAddrBase );\
	mapIn( &MEAS_LEN_LA, 0x147C + mAddrBase );\
	\
	mapIn( &COARSE_DELAY_MAIN, 0x1480 + mAddrBase );\
	mapIn( &COARSE_DELAY_ZOOM, 0x1484 + mAddrBase );\
	mapIn( &FINE_DELAY_MAIN, 0x1488 + mAddrBase );\
	mapIn( &FINE_DELAY_ZOOM, 0x148C + mAddrBase );\
	\
	mapIn( &INTERP_MULT_MAIN, 0x1490 + mAddrBase );\
	mapIn( &INTERP_MULT_ZOOM, 0x1494 + mAddrBase );\
	mapIn( &INTERP_MULT_MAIN_FINE, 0x1498 + mAddrBase );\
	mapIn( &INTERP_MULT_EYE, 0x149C + mAddrBase );\
	\
	mapIn( &INTERP_COEF_TAP, 0x14A0 + mAddrBase );\
	mapIn( &INTERP_COEF_WR, 0x14A4 + mAddrBase );\
	mapIn( &COMPRESS_MAIN, 0x14A8 + mAddrBase );\
	mapIn( &COMPRESS_ZOOM, 0x14AC + mAddrBase );\
	\
	mapIn( &LA_FINE_DELAY_MAIN, 0x14B0 + mAddrBase );\
	mapIn( &LA_FINE_DELAY_ZOOM, 0x14B4 + mAddrBase );\
	mapIn( &LA_COMP_MAIN, 0x14B8 + mAddrBase );\
	mapIn( &LA_COMP_ZOOM, 0x14BC + mAddrBase );\
	\
	mapIn( &TX_LEN_CH, 0x14C0 + mAddrBase );\
	mapIn( &TX_LEN_LA, 0x14C4 + mAddrBase );\
	mapIn( &TX_LEN_ZOOM_CH, 0x14C8 + mAddrBase );\
	mapIn( &TX_LEN_ZOOM_LA, 0x14CC + mAddrBase );\
	\
	mapIn( &TX_LEN_TRACE_CH, 0x14D0 + mAddrBase );\
	mapIn( &TX_LEN_TRACE_LA, 0x14D4 + mAddrBase );\
	mapIn( &TX_LEN_COL_CH, 0x14D8 + mAddrBase );\
	mapIn( &TX_LEN_COL_LA, 0x14DC + mAddrBase );\
	\
	mapIn( &DEBUG_ACQ, 0x14E0 + mAddrBase );\
	mapIn( &DEBUG_TX, 0x14E4 + mAddrBase );\
	mapIn( &DEBUG_MEM, 0x14EC + mAddrBase );\
	mapIn( &DEBUG_SCAN, 0x14F0 + mAddrBase );\
	\
	mapIn( &RECORD_SUM, 0x14F4 + mAddrBase );\
	mapIn( &SCAN_ROLL_FRM, 0x14F8 + mAddrBase );\
	mapIn( &ADC_AVG, 0x1500 + mAddrBase );\
	mapIn( &ADC_AVG_RES_AB, 0x1504 + mAddrBase );\
	\
	mapIn( &ADC_AVG_RES_CD, 0x1508 + mAddrBase );\
	mapIn( &ADC_AVG_MAX, 0x150C + mAddrBase );\
	mapIn( &ADC_AVG_MIN, 0x1510 + mAddrBase );\
	mapIn( &LA_SA_RES, 0x1514 + mAddrBase );\
	\
	mapIn( &ZONE_TRIG_AUX, 0x1524 + mAddrBase );\
	mapIn( &ZONE_TRIG_TAB1, 0x1528 + mAddrBase );\
	mapIn( &ZONE_TRIG_TAB2, 0x152C + mAddrBase );\
	mapIn( &MASK_TAB, 0x1530 + mAddrBase );\
	\
	mapIn( &MASK_TAB_EX, 0x1534 + mAddrBase );\
	mapIn( &MASK_COMPRESS, 0x1538 + mAddrBase );\
	mapIn( &MASK_CTRL, 0x153C + mAddrBase );\
	mapIn( &MASK_FRM_CNT_L, 0x1540 + mAddrBase );\
	\
	mapIn( &MASK_FRM_CNT_H, 0x1544 + mAddrBase );\
	mapIn( &MASK_CROSS_CNT_L_CHA, 0x1548 + mAddrBase );\
	mapIn( &MASK_CROSS_CNT_H_CHA, 0x154C + mAddrBase );\
	mapIn( &MASK_CROSS_CNT_L_CHB, 0x1550 + mAddrBase );\
	\
	mapIn( &MASK_CROSS_CNT_H_CHB, 0x1554 + mAddrBase );\
	mapIn( &MASK_CROSS_CNT_L_CHC, 0x1558 + mAddrBase );\
	mapIn( &MASK_CROSS_CNT_H_CHC, 0x155C + mAddrBase );\
	mapIn( &MASK_CROSS_CNT_L_CHD, 0x1560 + mAddrBase );\
	\
	mapIn( &MASK_CROSS_CNT_H_CHD, 0x1564 + mAddrBase );\
	mapIn( &MASK_OUTPUT, 0x1568 + mAddrBase );\
	mapIn( &SEARCH_OFFSET_CH, 0x1570 + mAddrBase );\
	mapIn( &SEARCH_LEN_CH, 0x1574 + mAddrBase );\
	\
	mapIn( &SEARCH_OFFSET_LA, 0x1578 + mAddrBase );\
	mapIn( &SEARCH_LEN_LA, 0x157C + mAddrBase );\
	mapIn( &TIME_TAG_L, 0x1580 + mAddrBase );\
	mapIn( &TIME_TAG_H, 0x1584 + mAddrBase );\
	\
	mapIn( &FINE_TRIG_POSITION, 0x1588 + mAddrBase );\
	mapIn( &TIME_TAG_CTRL, 0x158C + mAddrBase );\
	mapIn( &IMPORT_INFO, 0x1590 + mAddrBase );\
	mapIn( &TIME_TAG_L_REC_FIRST, 0x1598 + mAddrBase );\
	\
	mapIn( &TIME_TAG_H_REC_FIRST, 0x159C + mAddrBase );\
	mapIn( &TRACE_AVG_RATE_X, 0x15A0 + mAddrBase );\
	mapIn( &TRACE_AVG_RATE_Y, 0x15A4 + mAddrBase );\
	mapIn( &LINEAR_INTX_CTRL, 0x15AC + mAddrBase );\
	\
	mapIn( &LINEAR_INTX_INIT, 0x15B0 + mAddrBase );\
	mapIn( &LINEAR_INTX_STEP, 0x15B4 + mAddrBase );\
	mapIn( &LINEAR_INTX_INIT_ZOOM, 0x15B8 + mAddrBase );\
	mapIn( &LINEAR_INTX_STEP_ZOOM, 0x15BC + mAddrBase );\
	\
	mapIn( &WAVE_OFFSET_EYE, 0x15C0 + mAddrBase );\
	mapIn( &WAVE_LEN_EYE, 0x15C4 + mAddrBase );\
	mapIn( &COMPRESS_EYE, 0x15C8 + mAddrBase );\
	mapIn( &TX_LEN_EYE, 0x15CC + mAddrBase );\
	\
	mapIn( &LA_LINEAR_INTX_CTRL, 0x15D0 + mAddrBase );\
	mapIn( &LA_LINEAR_INTX_INIT, 0x15D4 + mAddrBase );\
	mapIn( &LA_LINEAR_INTX_STEP, 0x15D8 + mAddrBase );\
	mapIn( &LA_LINEAR_INTX_INIT_ZOOM, 0x15DC + mAddrBase );\
	\
	mapIn( &LA_LINEAR_INTX_STEP_ZOOM, 0x15E0 + mAddrBase );\
	mapIn( &TRACE_COMPRESS_MAIN, 0x15F8 + mAddrBase );\
	mapIn( &TRACE_COMPRESS_ZOOM, 0x15FC + mAddrBase );\
	mapIn( &ZYNQ_COMPRESS_MAIN, 0x1600 + mAddrBase );\
	\
	mapIn( &ZYNQ_COMPRESS_ZOOM, 0x1604 + mAddrBase );\
	mapIn( &DEBUG_TX_LA, 0x1610 + mAddrBase );\
	mapIn( &DEBUG_TX_FRM, 0x1614 + mAddrBase );\
	mapIn( &IMPORT_CNT_CH, 0x1618 + mAddrBase );\
	\
	mapIn( &IMPORT_CNT_LA, 0x161C + mAddrBase );\
	mapIn( &DEBUG_LINEAR_INTX1, 0x1620 + mAddrBase );\
	mapIn( &DEBUG_LINEAR_INTX2, 0x1624 + mAddrBase );\
	mapIn( &DEBUG_DDR, 0x1628 + mAddrBase );\
	\
	mapIn( &DEBUG_TX_LEN_BURST, 0x1630 + mAddrBase );\



class IPhySpu;
class SpuGp{
public:
	SpuGp(){}
protected:
	QList<IPhySpu*> mSubItems;

public:
	void attach( IPhySpu *pSubItem)
	{
		Q_ASSERT(NULL!=pSubItem);
		mSubItems.append(pSubItem);
	}

	IPhySpu * operator[](int id)
	{
		return mSubItems[id];
	}

	IPhySpu * at(int id)
	{
		return mSubItems[id];
	}

	int size()
	{
		return mSubItems.size();
	}

	void flushWCache(int id=-1);


public:
	Spu_reg getVERSION(  int id = 0 );

	Spu_reg inVERSION(  int id = 0 );

	Spu_reg getTEST(  int id = 0 );

	Spu_reg inTEST(  int id = 0 );

	void assignCTRL_spu_clr( Spu_reg value,int id=-1  );
	void assignCTRL_adc_sync( Spu_reg value,int id=-1  );
	void assignCTRL_adc_align_check( Spu_reg value,int id=-1  );
	void assignCTRL_non_inter( Spu_reg value,int id=-1  );
	void assignCTRL_peak( Spu_reg value,int id=-1  );
	void assignCTRL_hi_res( Spu_reg value,int id=-1  );
	void assignCTRL_anti_alias( Spu_reg value,int id=-1  );
	void assignCTRL_trace_avg( Spu_reg value,int id=-1  );
	void assignCTRL_trace_avg_mem( Spu_reg value,int id=-1  );
	void assignCTRL_adc_data_inv( Spu_reg value,int id=-1  );
	void assignCTRL_la_probe( Spu_reg value,int id=-1  );
	void assignCTRL_coarse_trig_range( Spu_reg value,int id=-1  );

	void assignCTRL( Spu_reg value,int id=-1 );


	void setCTRL_spu_clr( Spu_reg value,int id=-1 );
	void setCTRL_adc_sync( Spu_reg value,int id=-1 );
	void setCTRL_adc_align_check( Spu_reg value,int id=-1 );
	void setCTRL_non_inter( Spu_reg value,int id=-1 );
	void setCTRL_peak( Spu_reg value,int id=-1 );
	void setCTRL_hi_res( Spu_reg value,int id=-1 );
	void setCTRL_anti_alias( Spu_reg value,int id=-1 );
	void setCTRL_trace_avg( Spu_reg value,int id=-1 );
	void setCTRL_trace_avg_mem( Spu_reg value,int id=-1 );
	void setCTRL_adc_data_inv( Spu_reg value,int id=-1 );
	void setCTRL_la_probe( Spu_reg value,int id=-1 );
	void setCTRL_coarse_trig_range( Spu_reg value,int id=-1 );

	void setCTRL( Spu_reg value,int id=-1 );
	void outCTRL_spu_clr( Spu_reg value,int id=-1 );
	void pulseCTRL_spu_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_adc_sync( Spu_reg value,int id=-1 );
	void pulseCTRL_adc_sync( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_adc_align_check( Spu_reg value,int id=-1 );
	void pulseCTRL_adc_align_check( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_non_inter( Spu_reg value,int id=-1 );
	void pulseCTRL_non_inter( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_peak( Spu_reg value,int id=-1 );
	void pulseCTRL_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_hi_res( Spu_reg value,int id=-1 );
	void pulseCTRL_hi_res( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_anti_alias( Spu_reg value,int id=-1 );
	void pulseCTRL_anti_alias( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_trace_avg( Spu_reg value,int id=-1 );
	void pulseCTRL_trace_avg( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_trace_avg_mem( Spu_reg value,int id=-1 );
	void pulseCTRL_trace_avg_mem( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_adc_data_inv( Spu_reg value,int id=-1 );
	void pulseCTRL_adc_data_inv( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_la_probe( Spu_reg value,int id=-1 );
	void pulseCTRL_la_probe( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCTRL_coarse_trig_range( Spu_reg value,int id=-1 );
	void pulseCTRL_coarse_trig_range( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCTRL( Spu_reg value,int id=-1 );
	void outCTRL( int id=-1 );


	Spu_reg getCTRL_spu_clr(  int id = 0 );
	Spu_reg getCTRL_adc_sync(  int id = 0 );
	Spu_reg getCTRL_adc_align_check(  int id = 0 );
	Spu_reg getCTRL_non_inter(  int id = 0 );
	Spu_reg getCTRL_peak(  int id = 0 );
	Spu_reg getCTRL_hi_res(  int id = 0 );
	Spu_reg getCTRL_anti_alias(  int id = 0 );
	Spu_reg getCTRL_trace_avg(  int id = 0 );
	Spu_reg getCTRL_trace_avg_mem(  int id = 0 );
	Spu_reg getCTRL_adc_data_inv(  int id = 0 );
	Spu_reg getCTRL_la_probe(  int id = 0 );
	Spu_reg getCTRL_coarse_trig_range(  int id = 0 );

	Spu_reg getCTRL(  int id = 0 );

	Spu_reg inCTRL(  int id = 0 );

	void assignDEBUG_gray_bypass( Spu_reg value,int id=-1  );
	void assignDEBUG_ms_order( Spu_reg value,int id=-1  );
	void assignDEBUG_s_view( Spu_reg value,int id=-1  );
	void assignDEBUG_bram_ddr_n( Spu_reg value,int id=-1  );
	void assignDEBUG_ddr_init( Spu_reg value,int id=-1  );
	void assignDEBUG_ddr_err( Spu_reg value,int id=-1  );
	void assignDEBUG_dbg_trig_src( Spu_reg value,int id=-1  );
	void assignDEBUG_ddr_fsm_state( Spu_reg value,int id=-1  );
	void assignDEBUG_debug_mem( Spu_reg value,int id=-1  );

	void assignDEBUG( Spu_reg value,int id=-1 );


	void setDEBUG_gray_bypass( Spu_reg value,int id=-1 );
	void setDEBUG_ms_order( Spu_reg value,int id=-1 );
	void setDEBUG_s_view( Spu_reg value,int id=-1 );
	void setDEBUG_bram_ddr_n( Spu_reg value,int id=-1 );
	void setDEBUG_ddr_init( Spu_reg value,int id=-1 );
	void setDEBUG_ddr_err( Spu_reg value,int id=-1 );
	void setDEBUG_dbg_trig_src( Spu_reg value,int id=-1 );
	void setDEBUG_ddr_fsm_state( Spu_reg value,int id=-1 );
	void setDEBUG_debug_mem( Spu_reg value,int id=-1 );

	void setDEBUG( Spu_reg value,int id=-1 );
	void outDEBUG_gray_bypass( Spu_reg value,int id=-1 );
	void pulseDEBUG_gray_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_ms_order( Spu_reg value,int id=-1 );
	void pulseDEBUG_ms_order( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_s_view( Spu_reg value,int id=-1 );
	void pulseDEBUG_s_view( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_bram_ddr_n( Spu_reg value,int id=-1 );
	void pulseDEBUG_bram_ddr_n( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_ddr_init( Spu_reg value,int id=-1 );
	void pulseDEBUG_ddr_init( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_ddr_err( Spu_reg value,int id=-1 );
	void pulseDEBUG_ddr_err( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_dbg_trig_src( Spu_reg value,int id=-1 );
	void pulseDEBUG_dbg_trig_src( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_ddr_fsm_state( Spu_reg value,int id=-1 );
	void pulseDEBUG_ddr_fsm_state( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_debug_mem( Spu_reg value,int id=-1 );
	void pulseDEBUG_debug_mem( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEBUG( Spu_reg value,int id=-1 );
	void outDEBUG( int id=-1 );


	Spu_reg getDEBUG_gray_bypass(  int id = 0 );
	Spu_reg getDEBUG_ms_order(  int id = 0 );
	Spu_reg getDEBUG_s_view(  int id = 0 );
	Spu_reg getDEBUG_bram_ddr_n(  int id = 0 );
	Spu_reg getDEBUG_ddr_init(  int id = 0 );
	Spu_reg getDEBUG_ddr_err(  int id = 0 );
	Spu_reg getDEBUG_dbg_trig_src(  int id = 0 );
	Spu_reg getDEBUG_ddr_fsm_state(  int id = 0 );
	Spu_reg getDEBUG_debug_mem(  int id = 0 );

	Spu_reg getDEBUG(  int id = 0 );

	Spu_reg inDEBUG(  int id = 0 );

	void assignADC_DATA_IDELAY_idelay_var( Spu_reg value,int id=-1  );
	void assignADC_DATA_IDELAY_chn_bit( Spu_reg value,int id=-1  );
	void assignADC_DATA_IDELAY_h( Spu_reg value,int id=-1  );
	void assignADC_DATA_IDELAY_l( Spu_reg value,int id=-1  );
	void assignADC_DATA_IDELAY_core_ABCD( Spu_reg value,int id=-1  );

	void assignADC_DATA_IDELAY( Spu_reg value,int id=-1 );


	void setADC_DATA_IDELAY_idelay_var( Spu_reg value,int id=-1 );
	void setADC_DATA_IDELAY_chn_bit( Spu_reg value,int id=-1 );
	void setADC_DATA_IDELAY_h( Spu_reg value,int id=-1 );
	void setADC_DATA_IDELAY_l( Spu_reg value,int id=-1 );
	void setADC_DATA_IDELAY_core_ABCD( Spu_reg value,int id=-1 );

	void setADC_DATA_IDELAY( Spu_reg value,int id=-1 );
	void outADC_DATA_IDELAY_idelay_var( Spu_reg value,int id=-1 );
	void pulseADC_DATA_IDELAY_idelay_var( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_DATA_IDELAY_chn_bit( Spu_reg value,int id=-1 );
	void pulseADC_DATA_IDELAY_chn_bit( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_DATA_IDELAY_h( Spu_reg value,int id=-1 );
	void pulseADC_DATA_IDELAY_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_DATA_IDELAY_l( Spu_reg value,int id=-1 );
	void pulseADC_DATA_IDELAY_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_DATA_IDELAY_core_ABCD( Spu_reg value,int id=-1 );
	void pulseADC_DATA_IDELAY_core_ABCD( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outADC_DATA_IDELAY( Spu_reg value,int id=-1 );
	void outADC_DATA_IDELAY( int id=-1 );


	Spu_reg getADC_DATA_IDELAY_idelay_var(  int id = 0 );
	Spu_reg getADC_DATA_IDELAY_chn_bit(  int id = 0 );
	Spu_reg getADC_DATA_IDELAY_h(  int id = 0 );
	Spu_reg getADC_DATA_IDELAY_l(  int id = 0 );
	Spu_reg getADC_DATA_IDELAY_core_ABCD(  int id = 0 );

	Spu_reg getADC_DATA_IDELAY(  int id = 0 );

	Spu_reg inADC_DATA_IDELAY(  int id = 0 );

	Spu_reg getADC_DATA_CHECK_a_l(  int id = 0 );
	Spu_reg getADC_DATA_CHECK_a_h(  int id = 0 );
	Spu_reg getADC_DATA_CHECK_b_l(  int id = 0 );
	Spu_reg getADC_DATA_CHECK_b_h(  int id = 0 );
	Spu_reg getADC_DATA_CHECK_c_l(  int id = 0 );
	Spu_reg getADC_DATA_CHECK_c_h(  int id = 0 );
	Spu_reg getADC_DATA_CHECK_d_l(  int id = 0 );
	Spu_reg getADC_DATA_CHECK_d_h(  int id = 0 );

	Spu_reg getADC_DATA_CHECK(  int id = 0 );

	Spu_reg inADC_DATA_CHECK(  int id = 0 );

	Spu_reg getADC_CHN_ALIGN_CHK_adc_d(  int id = 0 );
	Spu_reg getADC_CHN_ALIGN_CHK_adc_c(  int id = 0 );
	Spu_reg getADC_CHN_ALIGN_CHK_adc_b(  int id = 0 );
	Spu_reg getADC_CHN_ALIGN_CHK_adc_a(  int id = 0 );

	Spu_reg getADC_CHN_ALIGN_CHK(  int id = 0 );

	Spu_reg inADC_CHN_ALIGN_CHK(  int id = 0 );

	void assignADC_CHN_ALIGN_adc_d( Spu_reg value,int id=-1  );
	void assignADC_CHN_ALIGN_adc_c( Spu_reg value,int id=-1  );
	void assignADC_CHN_ALIGN_adc_b( Spu_reg value,int id=-1  );
	void assignADC_CHN_ALIGN_adc_a( Spu_reg value,int id=-1  );

	void assignADC_CHN_ALIGN( Spu_reg value,int id=-1 );


	void setADC_CHN_ALIGN_adc_d( Spu_reg value,int id=-1 );
	void setADC_CHN_ALIGN_adc_c( Spu_reg value,int id=-1 );
	void setADC_CHN_ALIGN_adc_b( Spu_reg value,int id=-1 );
	void setADC_CHN_ALIGN_adc_a( Spu_reg value,int id=-1 );

	void setADC_CHN_ALIGN( Spu_reg value,int id=-1 );
	void outADC_CHN_ALIGN_adc_d( Spu_reg value,int id=-1 );
	void pulseADC_CHN_ALIGN_adc_d( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_CHN_ALIGN_adc_c( Spu_reg value,int id=-1 );
	void pulseADC_CHN_ALIGN_adc_c( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_CHN_ALIGN_adc_b( Spu_reg value,int id=-1 );
	void pulseADC_CHN_ALIGN_adc_b( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_CHN_ALIGN_adc_a( Spu_reg value,int id=-1 );
	void pulseADC_CHN_ALIGN_adc_a( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outADC_CHN_ALIGN( Spu_reg value,int id=-1 );
	void outADC_CHN_ALIGN( int id=-1 );


	Spu_reg getADC_CHN_ALIGN_adc_d(  int id = 0 );
	Spu_reg getADC_CHN_ALIGN_adc_c(  int id = 0 );
	Spu_reg getADC_CHN_ALIGN_adc_b(  int id = 0 );
	Spu_reg getADC_CHN_ALIGN_adc_a(  int id = 0 );

	Spu_reg getADC_CHN_ALIGN(  int id = 0 );

	Spu_reg inADC_CHN_ALIGN(  int id = 0 );

	void assignPRE_PROC_proc_en( Spu_reg value,int id=-1  );
	void assignPRE_PROC_cfg_filter_en( Spu_reg value,int id=-1  );

	void assignPRE_PROC( Spu_reg value,int id=-1 );


	void setPRE_PROC_proc_en( Spu_reg value,int id=-1 );
	void setPRE_PROC_cfg_filter_en( Spu_reg value,int id=-1 );

	void setPRE_PROC( Spu_reg value,int id=-1 );
	void outPRE_PROC_proc_en( Spu_reg value,int id=-1 );
	void pulsePRE_PROC_proc_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outPRE_PROC_cfg_filter_en( Spu_reg value,int id=-1 );
	void pulsePRE_PROC_cfg_filter_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outPRE_PROC( Spu_reg value,int id=-1 );
	void outPRE_PROC( int id=-1 );


	void assignFILTER_coeff( Spu_reg value,int id=-1  );
	void assignFILTER_coeff_id( Spu_reg value,int id=-1  );
	void assignFILTER_coeff_sel( Spu_reg value,int id=-1  );
	void assignFILTER_coeff_ch( Spu_reg value,int id=-1  );

	void assignFILTER( Spu_reg value,int id=-1 );


	void setFILTER_coeff( Spu_reg value,int id=-1 );
	void setFILTER_coeff_id( Spu_reg value,int id=-1 );
	void setFILTER_coeff_sel( Spu_reg value,int id=-1 );
	void setFILTER_coeff_ch( Spu_reg value,int id=-1 );

	void setFILTER( Spu_reg value,int id=-1 );
	void outFILTER_coeff( Spu_reg value,int id=-1 );
	void pulseFILTER_coeff( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFILTER_coeff_id( Spu_reg value,int id=-1 );
	void pulseFILTER_coeff_id( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFILTER_coeff_sel( Spu_reg value,int id=-1 );
	void pulseFILTER_coeff_sel( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFILTER_coeff_ch( Spu_reg value,int id=-1 );
	void pulseFILTER_coeff_ch( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outFILTER( Spu_reg value,int id=-1 );
	void outFILTER( int id=-1 );


	void assignDOWN_SAMP_FAST_down_samp_fast( Spu_reg value,int id=-1  );
	void assignDOWN_SAMP_FAST_en( Spu_reg value,int id=-1  );

	void assignDOWN_SAMP_FAST( Spu_reg value,int id=-1 );


	void setDOWN_SAMP_FAST_down_samp_fast( Spu_reg value,int id=-1 );
	void setDOWN_SAMP_FAST_en( Spu_reg value,int id=-1 );

	void setDOWN_SAMP_FAST( Spu_reg value,int id=-1 );
	void outDOWN_SAMP_FAST_down_samp_fast( Spu_reg value,int id=-1 );
	void pulseDOWN_SAMP_FAST_down_samp_fast( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDOWN_SAMP_FAST_en( Spu_reg value,int id=-1 );
	void pulseDOWN_SAMP_FAST_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outDOWN_SAMP_FAST( Spu_reg value,int id=-1 );
	void outDOWN_SAMP_FAST( int id=-1 );


	Spu_reg getDOWN_SAMP_FAST_down_samp_fast(  int id = 0 );
	Spu_reg getDOWN_SAMP_FAST_en(  int id = 0 );

	Spu_reg getDOWN_SAMP_FAST(  int id = 0 );

	Spu_reg inDOWN_SAMP_FAST(  int id = 0 );

	void assignDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value,int id=-1  );

	void assignDOWN_SAMP_SLOW_H( Spu_reg value,int id=-1 );


	void setDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value,int id=-1 );

	void setDOWN_SAMP_SLOW_H( Spu_reg value,int id=-1 );
	void outDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value,int id=-1 );
	void pulseDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outDOWN_SAMP_SLOW_H( Spu_reg value,int id=-1 );
	void outDOWN_SAMP_SLOW_H( int id=-1 );


	Spu_reg getDOWN_SAMP_SLOW_H_down_samp_slow_h(  int id = 0 );

	Spu_reg getDOWN_SAMP_SLOW_H(  int id = 0 );

	Spu_reg inDOWN_SAMP_SLOW_H(  int id = 0 );

	void assignDOWN_SAMP_SLOW_L( Spu_reg value,int id=-1 );

	void setDOWN_SAMP_SLOW_L( Spu_reg value,int id=-1 );
	void outDOWN_SAMP_SLOW_L( Spu_reg value,int id=-1 );
	void outDOWN_SAMP_SLOW_L( int id=-1 );

	Spu_reg getDOWN_SAMP_SLOW_L(  int id = 0 );

	Spu_reg inDOWN_SAMP_SLOW_L(  int id = 0 );

	void assignLA_DELAY( Spu_reg value,int id=-1 );

	void setLA_DELAY( Spu_reg value,int id=-1 );
	void outLA_DELAY( Spu_reg value,int id=-1 );
	void outLA_DELAY( int id=-1 );

	Spu_reg getLA_DELAY(  int id = 0 );

	Spu_reg inLA_DELAY(  int id = 0 );

	void assignCHN_DELAY_AB_B( Spu_reg value,int id=-1  );
	void assignCHN_DELAY_AB_A( Spu_reg value,int id=-1  );

	void assignCHN_DELAY_AB( Spu_reg value,int id=-1 );


	void setCHN_DELAY_AB_B( Spu_reg value,int id=-1 );
	void setCHN_DELAY_AB_A( Spu_reg value,int id=-1 );

	void setCHN_DELAY_AB( Spu_reg value,int id=-1 );
	void outCHN_DELAY_AB_B( Spu_reg value,int id=-1 );
	void pulseCHN_DELAY_AB_B( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCHN_DELAY_AB_A( Spu_reg value,int id=-1 );
	void pulseCHN_DELAY_AB_A( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCHN_DELAY_AB( Spu_reg value,int id=-1 );
	void outCHN_DELAY_AB( int id=-1 );


	Spu_reg getCHN_DELAY_AB_B(  int id = 0 );
	Spu_reg getCHN_DELAY_AB_A(  int id = 0 );

	Spu_reg getCHN_DELAY_AB(  int id = 0 );

	Spu_reg inCHN_DELAY_AB(  int id = 0 );

	void assignCHN_DELAY_CD_D( Spu_reg value,int id=-1  );
	void assignCHN_DELAY_CD_C( Spu_reg value,int id=-1  );

	void assignCHN_DELAY_CD( Spu_reg value,int id=-1 );


	void setCHN_DELAY_CD_D( Spu_reg value,int id=-1 );
	void setCHN_DELAY_CD_C( Spu_reg value,int id=-1 );

	void setCHN_DELAY_CD( Spu_reg value,int id=-1 );
	void outCHN_DELAY_CD_D( Spu_reg value,int id=-1 );
	void pulseCHN_DELAY_CD_D( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCHN_DELAY_CD_C( Spu_reg value,int id=-1 );
	void pulseCHN_DELAY_CD_C( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCHN_DELAY_CD( Spu_reg value,int id=-1 );
	void outCHN_DELAY_CD( int id=-1 );


	Spu_reg getCHN_DELAY_CD_D(  int id = 0 );
	Spu_reg getCHN_DELAY_CD_C(  int id = 0 );

	Spu_reg getCHN_DELAY_CD(  int id = 0 );

	Spu_reg inCHN_DELAY_CD(  int id = 0 );

	void assignRANDOM_RANGE( Spu_reg value,int id=-1 );

	void setRANDOM_RANGE( Spu_reg value,int id=-1 );
	void outRANDOM_RANGE( Spu_reg value,int id=-1 );
	void outRANDOM_RANGE( int id=-1 );

	Spu_reg getRANDOM_RANGE(  int id = 0 );

	Spu_reg inRANDOM_RANGE(  int id = 0 );

	void assignEXT_TRIG_IDEALY_idelay_var( Spu_reg value,int id=-1  );
	void assignEXT_TRIG_IDEALY_ld( Spu_reg value,int id=-1  );

	void assignEXT_TRIG_IDEALY( Spu_reg value,int id=-1 );


	void setEXT_TRIG_IDEALY_idelay_var( Spu_reg value,int id=-1 );
	void setEXT_TRIG_IDEALY_ld( Spu_reg value,int id=-1 );

	void setEXT_TRIG_IDEALY( Spu_reg value,int id=-1 );
	void outEXT_TRIG_IDEALY_idelay_var( Spu_reg value,int id=-1 );
	void pulseEXT_TRIG_IDEALY_idelay_var( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outEXT_TRIG_IDEALY_ld( Spu_reg value,int id=-1 );
	void pulseEXT_TRIG_IDEALY_ld( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outEXT_TRIG_IDEALY( Spu_reg value,int id=-1 );
	void outEXT_TRIG_IDEALY( int id=-1 );


	Spu_reg getEXT_TRIG_IDEALY_idelay_var(  int id = 0 );
	Spu_reg getEXT_TRIG_IDEALY_ld(  int id = 0 );

	Spu_reg getEXT_TRIG_IDEALY(  int id = 0 );

	Spu_reg inEXT_TRIG_IDEALY(  int id = 0 );

	void assignSEGMENT_SIZE( Spu_reg value,int id=-1 );

	void setSEGMENT_SIZE( Spu_reg value,int id=-1 );
	void outSEGMENT_SIZE( Spu_reg value,int id=-1 );
	void outSEGMENT_SIZE( int id=-1 );

	Spu_reg getSEGMENT_SIZE(  int id = 0 );

	Spu_reg inSEGMENT_SIZE(  int id = 0 );

	void assignRECORD_LEN( Spu_reg value,int id=-1 );

	void setRECORD_LEN( Spu_reg value,int id=-1 );
	void outRECORD_LEN( Spu_reg value,int id=-1 );
	void outRECORD_LEN( int id=-1 );

	Spu_reg getRECORD_LEN(  int id = 0 );

	Spu_reg inRECORD_LEN(  int id = 0 );

	void assignWAVE_LEN_MAIN( Spu_reg value,int id=-1 );

	void setWAVE_LEN_MAIN( Spu_reg value,int id=-1 );
	void outWAVE_LEN_MAIN( Spu_reg value,int id=-1 );
	void outWAVE_LEN_MAIN( int id=-1 );

	Spu_reg getWAVE_LEN_MAIN(  int id = 0 );

	Spu_reg inWAVE_LEN_MAIN(  int id = 0 );

	void assignWAVE_LEN_ZOOM( Spu_reg value,int id=-1 );

	void setWAVE_LEN_ZOOM( Spu_reg value,int id=-1 );
	void outWAVE_LEN_ZOOM( Spu_reg value,int id=-1 );
	void outWAVE_LEN_ZOOM( int id=-1 );

	Spu_reg getWAVE_LEN_ZOOM(  int id = 0 );

	Spu_reg inWAVE_LEN_ZOOM(  int id = 0 );

	void assignWAVE_OFFSET_MAIN( Spu_reg value,int id=-1 );

	void setWAVE_OFFSET_MAIN( Spu_reg value,int id=-1 );
	void outWAVE_OFFSET_MAIN( Spu_reg value,int id=-1 );
	void outWAVE_OFFSET_MAIN( int id=-1 );

	Spu_reg getWAVE_OFFSET_MAIN(  int id = 0 );

	Spu_reg inWAVE_OFFSET_MAIN(  int id = 0 );

	void assignWAVE_OFFSET_ZOOM( Spu_reg value,int id=-1 );

	void setWAVE_OFFSET_ZOOM( Spu_reg value,int id=-1 );
	void outWAVE_OFFSET_ZOOM( Spu_reg value,int id=-1 );
	void outWAVE_OFFSET_ZOOM( int id=-1 );

	Spu_reg getWAVE_OFFSET_ZOOM(  int id = 0 );

	Spu_reg inWAVE_OFFSET_ZOOM(  int id = 0 );

	void assignMEAS_OFFSET_CH( Spu_reg value,int id=-1 );

	void setMEAS_OFFSET_CH( Spu_reg value,int id=-1 );
	void outMEAS_OFFSET_CH( Spu_reg value,int id=-1 );
	void outMEAS_OFFSET_CH( int id=-1 );

	Spu_reg getMEAS_OFFSET_CH(  int id = 0 );

	Spu_reg inMEAS_OFFSET_CH(  int id = 0 );

	void assignMEAS_LEN_CH( Spu_reg value,int id=-1 );

	void setMEAS_LEN_CH( Spu_reg value,int id=-1 );
	void outMEAS_LEN_CH( Spu_reg value,int id=-1 );
	void outMEAS_LEN_CH( int id=-1 );

	Spu_reg getMEAS_LEN_CH(  int id = 0 );

	Spu_reg inMEAS_LEN_CH(  int id = 0 );

	void assignMEAS_OFFSET_LA( Spu_reg value,int id=-1 );

	void setMEAS_OFFSET_LA( Spu_reg value,int id=-1 );
	void outMEAS_OFFSET_LA( Spu_reg value,int id=-1 );
	void outMEAS_OFFSET_LA( int id=-1 );

	Spu_reg getMEAS_OFFSET_LA(  int id = 0 );

	Spu_reg inMEAS_OFFSET_LA(  int id = 0 );

	void assignMEAS_LEN_LA( Spu_reg value,int id=-1 );

	void setMEAS_LEN_LA( Spu_reg value,int id=-1 );
	void outMEAS_LEN_LA( Spu_reg value,int id=-1 );
	void outMEAS_LEN_LA( int id=-1 );

	Spu_reg getMEAS_LEN_LA(  int id = 0 );

	Spu_reg inMEAS_LEN_LA(  int id = 0 );

	void assignCOARSE_DELAY_MAIN_chn_delay( Spu_reg value,int id=-1  );
	void assignCOARSE_DELAY_MAIN_chn_ce( Spu_reg value,int id=-1  );
	void assignCOARSE_DELAY_MAIN_offset_en( Spu_reg value,int id=-1  );
	void assignCOARSE_DELAY_MAIN_delay_en( Spu_reg value,int id=-1  );

	void assignCOARSE_DELAY_MAIN( Spu_reg value,int id=-1 );


	void setCOARSE_DELAY_MAIN_chn_delay( Spu_reg value,int id=-1 );
	void setCOARSE_DELAY_MAIN_chn_ce( Spu_reg value,int id=-1 );
	void setCOARSE_DELAY_MAIN_offset_en( Spu_reg value,int id=-1 );
	void setCOARSE_DELAY_MAIN_delay_en( Spu_reg value,int id=-1 );

	void setCOARSE_DELAY_MAIN( Spu_reg value,int id=-1 );
	void outCOARSE_DELAY_MAIN_chn_delay( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOARSE_DELAY_MAIN_chn_ce( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOARSE_DELAY_MAIN_offset_en( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOARSE_DELAY_MAIN_delay_en( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCOARSE_DELAY_MAIN( Spu_reg value,int id=-1 );
	void outCOARSE_DELAY_MAIN( int id=-1 );


	Spu_reg getCOARSE_DELAY_MAIN_chn_delay(  int id = 0 );
	Spu_reg getCOARSE_DELAY_MAIN_chn_ce(  int id = 0 );
	Spu_reg getCOARSE_DELAY_MAIN_offset_en(  int id = 0 );
	Spu_reg getCOARSE_DELAY_MAIN_delay_en(  int id = 0 );

	Spu_reg getCOARSE_DELAY_MAIN(  int id = 0 );

	Spu_reg inCOARSE_DELAY_MAIN(  int id = 0 );

	void assignCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value,int id=-1  );
	void assignCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value,int id=-1  );
	void assignCOARSE_DELAY_ZOOM_offset_en( Spu_reg value,int id=-1  );
	void assignCOARSE_DELAY_ZOOM_delay_en( Spu_reg value,int id=-1  );

	void assignCOARSE_DELAY_ZOOM( Spu_reg value,int id=-1 );


	void setCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value,int id=-1 );
	void setCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value,int id=-1 );
	void setCOARSE_DELAY_ZOOM_offset_en( Spu_reg value,int id=-1 );
	void setCOARSE_DELAY_ZOOM_delay_en( Spu_reg value,int id=-1 );

	void setCOARSE_DELAY_ZOOM( Spu_reg value,int id=-1 );
	void outCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOARSE_DELAY_ZOOM_offset_en( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOARSE_DELAY_ZOOM_delay_en( Spu_reg value,int id=-1 );
	void pulseCOARSE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCOARSE_DELAY_ZOOM( Spu_reg value,int id=-1 );
	void outCOARSE_DELAY_ZOOM( int id=-1 );


	Spu_reg getCOARSE_DELAY_ZOOM_chn_delay(  int id = 0 );
	Spu_reg getCOARSE_DELAY_ZOOM_chn_ce(  int id = 0 );
	Spu_reg getCOARSE_DELAY_ZOOM_offset_en(  int id = 0 );
	Spu_reg getCOARSE_DELAY_ZOOM_delay_en(  int id = 0 );

	Spu_reg getCOARSE_DELAY_ZOOM(  int id = 0 );

	Spu_reg inCOARSE_DELAY_ZOOM(  int id = 0 );

	void assignFINE_DELAY_MAIN_chn_delay( Spu_reg value,int id=-1  );
	void assignFINE_DELAY_MAIN_chn_ce( Spu_reg value,int id=-1  );
	void assignFINE_DELAY_MAIN_offset_en( Spu_reg value,int id=-1  );
	void assignFINE_DELAY_MAIN_delay_en( Spu_reg value,int id=-1  );

	void assignFINE_DELAY_MAIN( Spu_reg value,int id=-1 );


	void setFINE_DELAY_MAIN_chn_delay( Spu_reg value,int id=-1 );
	void setFINE_DELAY_MAIN_chn_ce( Spu_reg value,int id=-1 );
	void setFINE_DELAY_MAIN_offset_en( Spu_reg value,int id=-1 );
	void setFINE_DELAY_MAIN_delay_en( Spu_reg value,int id=-1 );

	void setFINE_DELAY_MAIN( Spu_reg value,int id=-1 );
	void outFINE_DELAY_MAIN_chn_delay( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFINE_DELAY_MAIN_chn_ce( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFINE_DELAY_MAIN_offset_en( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFINE_DELAY_MAIN_delay_en( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outFINE_DELAY_MAIN( Spu_reg value,int id=-1 );
	void outFINE_DELAY_MAIN( int id=-1 );


	Spu_reg getFINE_DELAY_MAIN_chn_delay(  int id = 0 );
	Spu_reg getFINE_DELAY_MAIN_chn_ce(  int id = 0 );
	Spu_reg getFINE_DELAY_MAIN_offset_en(  int id = 0 );
	Spu_reg getFINE_DELAY_MAIN_delay_en(  int id = 0 );

	Spu_reg getFINE_DELAY_MAIN(  int id = 0 );

	Spu_reg inFINE_DELAY_MAIN(  int id = 0 );

	void assignFINE_DELAY_ZOOM_chn_delay( Spu_reg value,int id=-1  );
	void assignFINE_DELAY_ZOOM_chn_ce( Spu_reg value,int id=-1  );
	void assignFINE_DELAY_ZOOM_offset_en( Spu_reg value,int id=-1  );
	void assignFINE_DELAY_ZOOM_delay_en( Spu_reg value,int id=-1  );

	void assignFINE_DELAY_ZOOM( Spu_reg value,int id=-1 );


	void setFINE_DELAY_ZOOM_chn_delay( Spu_reg value,int id=-1 );
	void setFINE_DELAY_ZOOM_chn_ce( Spu_reg value,int id=-1 );
	void setFINE_DELAY_ZOOM_offset_en( Spu_reg value,int id=-1 );
	void setFINE_DELAY_ZOOM_delay_en( Spu_reg value,int id=-1 );

	void setFINE_DELAY_ZOOM( Spu_reg value,int id=-1 );
	void outFINE_DELAY_ZOOM_chn_delay( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFINE_DELAY_ZOOM_chn_ce( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFINE_DELAY_ZOOM_offset_en( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outFINE_DELAY_ZOOM_delay_en( Spu_reg value,int id=-1 );
	void pulseFINE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outFINE_DELAY_ZOOM( Spu_reg value,int id=-1 );
	void outFINE_DELAY_ZOOM( int id=-1 );


	Spu_reg getFINE_DELAY_ZOOM_chn_delay(  int id = 0 );
	Spu_reg getFINE_DELAY_ZOOM_chn_ce(  int id = 0 );
	Spu_reg getFINE_DELAY_ZOOM_offset_en(  int id = 0 );
	Spu_reg getFINE_DELAY_ZOOM_delay_en(  int id = 0 );

	Spu_reg getFINE_DELAY_ZOOM(  int id = 0 );

	Spu_reg inFINE_DELAY_ZOOM(  int id = 0 );

	void assignINTERP_MULT_MAIN_coef_len( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_MAIN_comm_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_MAIN_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_MAIN_en( Spu_reg value,int id=-1  );

	void assignINTERP_MULT_MAIN( Spu_reg value,int id=-1 );


	void setINTERP_MULT_MAIN_coef_len( Spu_reg value,int id=-1 );
	void setINTERP_MULT_MAIN_comm_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_MAIN_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_MAIN_en( Spu_reg value,int id=-1 );

	void setINTERP_MULT_MAIN( Spu_reg value,int id=-1 );
	void outINTERP_MULT_MAIN_coef_len( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_MAIN_comm_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_MAIN_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_MAIN_en( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outINTERP_MULT_MAIN( Spu_reg value,int id=-1 );
	void outINTERP_MULT_MAIN( int id=-1 );


	Spu_reg getINTERP_MULT_MAIN_coef_len(  int id = 0 );
	Spu_reg getINTERP_MULT_MAIN_comm_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_MAIN_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_MAIN_en(  int id = 0 );

	Spu_reg getINTERP_MULT_MAIN(  int id = 0 );

	Spu_reg inINTERP_MULT_MAIN(  int id = 0 );

	void assignINTERP_MULT_ZOOM_coef_len( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_ZOOM_comm_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_ZOOM_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_ZOOM_en( Spu_reg value,int id=-1  );

	void assignINTERP_MULT_ZOOM( Spu_reg value,int id=-1 );


	void setINTERP_MULT_ZOOM_coef_len( Spu_reg value,int id=-1 );
	void setINTERP_MULT_ZOOM_comm_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_ZOOM_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_ZOOM_en( Spu_reg value,int id=-1 );

	void setINTERP_MULT_ZOOM( Spu_reg value,int id=-1 );
	void outINTERP_MULT_ZOOM_coef_len( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_ZOOM_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_ZOOM_comm_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_ZOOM_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_ZOOM_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_ZOOM_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_ZOOM_en( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outINTERP_MULT_ZOOM( Spu_reg value,int id=-1 );
	void outINTERP_MULT_ZOOM( int id=-1 );


	Spu_reg getINTERP_MULT_ZOOM_coef_len(  int id = 0 );
	Spu_reg getINTERP_MULT_ZOOM_comm_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_ZOOM_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_ZOOM_en(  int id = 0 );

	Spu_reg getINTERP_MULT_ZOOM(  int id = 0 );

	Spu_reg inINTERP_MULT_ZOOM(  int id = 0 );

	void assignINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_MAIN_FINE_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_MAIN_FINE_en( Spu_reg value,int id=-1  );

	void assignINTERP_MULT_MAIN_FINE( Spu_reg value,int id=-1 );


	void setINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value,int id=-1 );
	void setINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_MAIN_FINE_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_MAIN_FINE_en( Spu_reg value,int id=-1 );

	void setINTERP_MULT_MAIN_FINE( Spu_reg value,int id=-1 );
	void outINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_FINE_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_MAIN_FINE_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_FINE_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_MAIN_FINE_en( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_MAIN_FINE_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outINTERP_MULT_MAIN_FINE( Spu_reg value,int id=-1 );
	void outINTERP_MULT_MAIN_FINE( int id=-1 );


	Spu_reg getINTERP_MULT_MAIN_FINE_coef_len(  int id = 0 );
	Spu_reg getINTERP_MULT_MAIN_FINE_comm_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_MAIN_FINE_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_MAIN_FINE_en(  int id = 0 );

	Spu_reg getINTERP_MULT_MAIN_FINE(  int id = 0 );

	Spu_reg inINTERP_MULT_MAIN_FINE(  int id = 0 );

	void assignINTERP_MULT_EYE_coef_len( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_EYE_comm_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_EYE_mult( Spu_reg value,int id=-1  );
	void assignINTERP_MULT_EYE_en( Spu_reg value,int id=-1  );

	void assignINTERP_MULT_EYE( Spu_reg value,int id=-1 );


	void setINTERP_MULT_EYE_coef_len( Spu_reg value,int id=-1 );
	void setINTERP_MULT_EYE_comm_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_EYE_mult( Spu_reg value,int id=-1 );
	void setINTERP_MULT_EYE_en( Spu_reg value,int id=-1 );

	void setINTERP_MULT_EYE( Spu_reg value,int id=-1 );
	void outINTERP_MULT_EYE_coef_len( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_EYE_coef_len( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_EYE_comm_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_EYE_comm_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_EYE_mult( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_EYE_mult( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_MULT_EYE_en( Spu_reg value,int id=-1 );
	void pulseINTERP_MULT_EYE_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outINTERP_MULT_EYE( Spu_reg value,int id=-1 );
	void outINTERP_MULT_EYE( int id=-1 );


	Spu_reg getINTERP_MULT_EYE_coef_len(  int id = 0 );
	Spu_reg getINTERP_MULT_EYE_comm_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_EYE_mult(  int id = 0 );
	Spu_reg getINTERP_MULT_EYE_en(  int id = 0 );

	Spu_reg getINTERP_MULT_EYE(  int id = 0 );

	Spu_reg inINTERP_MULT_EYE(  int id = 0 );

	void assignINTERP_COEF_TAP_tap( Spu_reg value,int id=-1  );
	void assignINTERP_COEF_TAP_ce( Spu_reg value,int id=-1  );

	void assignINTERP_COEF_TAP( Spu_reg value,int id=-1 );


	void setINTERP_COEF_TAP_tap( Spu_reg value,int id=-1 );
	void setINTERP_COEF_TAP_ce( Spu_reg value,int id=-1 );

	void setINTERP_COEF_TAP( Spu_reg value,int id=-1 );
	void outINTERP_COEF_TAP_tap( Spu_reg value,int id=-1 );
	void pulseINTERP_COEF_TAP_tap( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_COEF_TAP_ce( Spu_reg value,int id=-1 );
	void pulseINTERP_COEF_TAP_ce( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outINTERP_COEF_TAP( Spu_reg value,int id=-1 );
	void outINTERP_COEF_TAP( int id=-1 );


	void assignINTERP_COEF_WR_wdata( Spu_reg value,int id=-1  );
	void assignINTERP_COEF_WR_waddr( Spu_reg value,int id=-1  );
	void assignINTERP_COEF_WR_coef_group( Spu_reg value,int id=-1  );
	void assignINTERP_COEF_WR_wr_en( Spu_reg value,int id=-1  );

	void assignINTERP_COEF_WR( Spu_reg value,int id=-1 );


	void setINTERP_COEF_WR_wdata( Spu_reg value,int id=-1 );
	void setINTERP_COEF_WR_waddr( Spu_reg value,int id=-1 );
	void setINTERP_COEF_WR_coef_group( Spu_reg value,int id=-1 );
	void setINTERP_COEF_WR_wr_en( Spu_reg value,int id=-1 );

	void setINTERP_COEF_WR( Spu_reg value,int id=-1 );
	void outINTERP_COEF_WR_wdata( Spu_reg value,int id=-1 );
	void pulseINTERP_COEF_WR_wdata( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_COEF_WR_waddr( Spu_reg value,int id=-1 );
	void pulseINTERP_COEF_WR_waddr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_COEF_WR_coef_group( Spu_reg value,int id=-1 );
	void pulseINTERP_COEF_WR_coef_group( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outINTERP_COEF_WR_wr_en( Spu_reg value,int id=-1 );
	void pulseINTERP_COEF_WR_wr_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outINTERP_COEF_WR( Spu_reg value,int id=-1 );
	void outINTERP_COEF_WR( int id=-1 );


	void assignCOMPRESS_MAIN_rate( Spu_reg value,int id=-1  );
	void assignCOMPRESS_MAIN_peak( Spu_reg value,int id=-1  );
	void assignCOMPRESS_MAIN_en( Spu_reg value,int id=-1  );

	void assignCOMPRESS_MAIN( Spu_reg value,int id=-1 );


	void setCOMPRESS_MAIN_rate( Spu_reg value,int id=-1 );
	void setCOMPRESS_MAIN_peak( Spu_reg value,int id=-1 );
	void setCOMPRESS_MAIN_en( Spu_reg value,int id=-1 );

	void setCOMPRESS_MAIN( Spu_reg value,int id=-1 );
	void outCOMPRESS_MAIN_rate( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOMPRESS_MAIN_peak( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOMPRESS_MAIN_en( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCOMPRESS_MAIN( Spu_reg value,int id=-1 );
	void outCOMPRESS_MAIN( int id=-1 );


	Spu_reg getCOMPRESS_MAIN_rate(  int id = 0 );
	Spu_reg getCOMPRESS_MAIN_peak(  int id = 0 );
	Spu_reg getCOMPRESS_MAIN_en(  int id = 0 );

	Spu_reg getCOMPRESS_MAIN(  int id = 0 );

	Spu_reg inCOMPRESS_MAIN(  int id = 0 );

	void assignCOMPRESS_ZOOM_rate( Spu_reg value,int id=-1  );
	void assignCOMPRESS_ZOOM_peak( Spu_reg value,int id=-1  );
	void assignCOMPRESS_ZOOM_en( Spu_reg value,int id=-1  );

	void assignCOMPRESS_ZOOM( Spu_reg value,int id=-1 );


	void setCOMPRESS_ZOOM_rate( Spu_reg value,int id=-1 );
	void setCOMPRESS_ZOOM_peak( Spu_reg value,int id=-1 );
	void setCOMPRESS_ZOOM_en( Spu_reg value,int id=-1 );

	void setCOMPRESS_ZOOM( Spu_reg value,int id=-1 );
	void outCOMPRESS_ZOOM_rate( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOMPRESS_ZOOM_peak( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOMPRESS_ZOOM_en( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCOMPRESS_ZOOM( Spu_reg value,int id=-1 );
	void outCOMPRESS_ZOOM( int id=-1 );


	Spu_reg getCOMPRESS_ZOOM_rate(  int id = 0 );
	Spu_reg getCOMPRESS_ZOOM_peak(  int id = 0 );
	Spu_reg getCOMPRESS_ZOOM_en(  int id = 0 );

	Spu_reg getCOMPRESS_ZOOM(  int id = 0 );

	Spu_reg inCOMPRESS_ZOOM(  int id = 0 );

	void assignLA_FINE_DELAY_MAIN_delay( Spu_reg value,int id=-1  );
	void assignLA_FINE_DELAY_MAIN_en( Spu_reg value,int id=-1  );

	void assignLA_FINE_DELAY_MAIN( Spu_reg value,int id=-1 );


	void setLA_FINE_DELAY_MAIN_delay( Spu_reg value,int id=-1 );
	void setLA_FINE_DELAY_MAIN_en( Spu_reg value,int id=-1 );

	void setLA_FINE_DELAY_MAIN( Spu_reg value,int id=-1 );
	void outLA_FINE_DELAY_MAIN_delay( Spu_reg value,int id=-1 );
	void pulseLA_FINE_DELAY_MAIN_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outLA_FINE_DELAY_MAIN_en( Spu_reg value,int id=-1 );
	void pulseLA_FINE_DELAY_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outLA_FINE_DELAY_MAIN( Spu_reg value,int id=-1 );
	void outLA_FINE_DELAY_MAIN( int id=-1 );


	Spu_reg getLA_FINE_DELAY_MAIN_delay(  int id = 0 );
	Spu_reg getLA_FINE_DELAY_MAIN_en(  int id = 0 );

	Spu_reg getLA_FINE_DELAY_MAIN(  int id = 0 );

	Spu_reg inLA_FINE_DELAY_MAIN(  int id = 0 );

	void assignLA_FINE_DELAY_ZOOM_delay( Spu_reg value,int id=-1  );
	void assignLA_FINE_DELAY_ZOOM_en( Spu_reg value,int id=-1  );

	void assignLA_FINE_DELAY_ZOOM( Spu_reg value,int id=-1 );


	void setLA_FINE_DELAY_ZOOM_delay( Spu_reg value,int id=-1 );
	void setLA_FINE_DELAY_ZOOM_en( Spu_reg value,int id=-1 );

	void setLA_FINE_DELAY_ZOOM( Spu_reg value,int id=-1 );
	void outLA_FINE_DELAY_ZOOM_delay( Spu_reg value,int id=-1 );
	void pulseLA_FINE_DELAY_ZOOM_delay( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outLA_FINE_DELAY_ZOOM_en( Spu_reg value,int id=-1 );
	void pulseLA_FINE_DELAY_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outLA_FINE_DELAY_ZOOM( Spu_reg value,int id=-1 );
	void outLA_FINE_DELAY_ZOOM( int id=-1 );


	Spu_reg getLA_FINE_DELAY_ZOOM_delay(  int id = 0 );
	Spu_reg getLA_FINE_DELAY_ZOOM_en(  int id = 0 );

	Spu_reg getLA_FINE_DELAY_ZOOM(  int id = 0 );

	Spu_reg inLA_FINE_DELAY_ZOOM(  int id = 0 );

	void assignLA_COMP_MAIN_rate( Spu_reg value,int id=-1  );
	void assignLA_COMP_MAIN_en( Spu_reg value,int id=-1  );

	void assignLA_COMP_MAIN( Spu_reg value,int id=-1 );


	void setLA_COMP_MAIN_rate( Spu_reg value,int id=-1 );
	void setLA_COMP_MAIN_en( Spu_reg value,int id=-1 );

	void setLA_COMP_MAIN( Spu_reg value,int id=-1 );
	void outLA_COMP_MAIN_rate( Spu_reg value,int id=-1 );
	void pulseLA_COMP_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outLA_COMP_MAIN_en( Spu_reg value,int id=-1 );
	void pulseLA_COMP_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outLA_COMP_MAIN( Spu_reg value,int id=-1 );
	void outLA_COMP_MAIN( int id=-1 );


	Spu_reg getLA_COMP_MAIN_rate(  int id = 0 );
	Spu_reg getLA_COMP_MAIN_en(  int id = 0 );

	Spu_reg getLA_COMP_MAIN(  int id = 0 );

	Spu_reg inLA_COMP_MAIN(  int id = 0 );

	void assignLA_COMP_ZOOM_rate( Spu_reg value,int id=-1  );
	void assignLA_COMP_ZOOM_en( Spu_reg value,int id=-1  );

	void assignLA_COMP_ZOOM( Spu_reg value,int id=-1 );


	void setLA_COMP_ZOOM_rate( Spu_reg value,int id=-1 );
	void setLA_COMP_ZOOM_en( Spu_reg value,int id=-1 );

	void setLA_COMP_ZOOM( Spu_reg value,int id=-1 );
	void outLA_COMP_ZOOM_rate( Spu_reg value,int id=-1 );
	void pulseLA_COMP_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outLA_COMP_ZOOM_en( Spu_reg value,int id=-1 );
	void pulseLA_COMP_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outLA_COMP_ZOOM( Spu_reg value,int id=-1 );
	void outLA_COMP_ZOOM( int id=-1 );


	Spu_reg getLA_COMP_ZOOM_rate(  int id = 0 );
	Spu_reg getLA_COMP_ZOOM_en(  int id = 0 );

	Spu_reg getLA_COMP_ZOOM(  int id = 0 );

	Spu_reg inLA_COMP_ZOOM(  int id = 0 );

	void assignTX_LEN_CH( Spu_reg value,int id=-1 );

	void setTX_LEN_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_CH( int id=-1 );

	Spu_reg getTX_LEN_CH(  int id = 0 );

	Spu_reg inTX_LEN_CH(  int id = 0 );

	void assignTX_LEN_LA( Spu_reg value,int id=-1 );

	void setTX_LEN_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_LA( int id=-1 );

	Spu_reg getTX_LEN_LA(  int id = 0 );

	Spu_reg inTX_LEN_LA(  int id = 0 );

	void assignTX_LEN_ZOOM_CH( Spu_reg value,int id=-1 );

	void setTX_LEN_ZOOM_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_ZOOM_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_ZOOM_CH( int id=-1 );

	Spu_reg getTX_LEN_ZOOM_CH(  int id = 0 );

	Spu_reg inTX_LEN_ZOOM_CH(  int id = 0 );

	void assignTX_LEN_ZOOM_LA( Spu_reg value,int id=-1 );

	void setTX_LEN_ZOOM_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_ZOOM_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_ZOOM_LA( int id=-1 );

	Spu_reg getTX_LEN_ZOOM_LA(  int id = 0 );

	Spu_reg inTX_LEN_ZOOM_LA(  int id = 0 );

	void assignTX_LEN_TRACE_CH( Spu_reg value,int id=-1 );

	void setTX_LEN_TRACE_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_TRACE_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_TRACE_CH( int id=-1 );

	Spu_reg getTX_LEN_TRACE_CH(  int id = 0 );

	Spu_reg inTX_LEN_TRACE_CH(  int id = 0 );

	void assignTX_LEN_TRACE_LA( Spu_reg value,int id=-1 );

	void setTX_LEN_TRACE_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_TRACE_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_TRACE_LA( int id=-1 );

	Spu_reg getTX_LEN_TRACE_LA(  int id = 0 );

	Spu_reg inTX_LEN_TRACE_LA(  int id = 0 );

	void assignTX_LEN_COL_CH( Spu_reg value,int id=-1 );

	void setTX_LEN_COL_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_COL_CH( Spu_reg value,int id=-1 );
	void outTX_LEN_COL_CH( int id=-1 );

	Spu_reg getTX_LEN_COL_CH(  int id = 0 );

	Spu_reg inTX_LEN_COL_CH(  int id = 0 );

	void assignTX_LEN_COL_LA( Spu_reg value,int id=-1 );

	void setTX_LEN_COL_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_COL_LA( Spu_reg value,int id=-1 );
	void outTX_LEN_COL_LA( int id=-1 );

	Spu_reg getTX_LEN_COL_LA(  int id = 0 );

	Spu_reg inTX_LEN_COL_LA(  int id = 0 );

	void assignDEBUG_ACQ_samp_extract_vld( Spu_reg value,int id=-1  );
	void assignDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value,int id=-1  );
	void assignDEBUG_ACQ_acq_dly_cfg( Spu_reg value,int id=-1  );
	void assignDEBUG_ACQ_trig_src_type( Spu_reg value,int id=-1  );

	void assignDEBUG_ACQ( Spu_reg value,int id=-1 );


	void setDEBUG_ACQ_samp_extract_vld( Spu_reg value,int id=-1 );
	void setDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value,int id=-1 );
	void setDEBUG_ACQ_acq_dly_cfg( Spu_reg value,int id=-1 );
	void setDEBUG_ACQ_trig_src_type( Spu_reg value,int id=-1 );

	void setDEBUG_ACQ( Spu_reg value,int id=-1 );
	void outDEBUG_ACQ_samp_extract_vld( Spu_reg value,int id=-1 );
	void pulseDEBUG_ACQ_samp_extract_vld( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value,int id=-1 );
	void pulseDEBUG_ACQ_acq_dly_cfg_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_ACQ_acq_dly_cfg( Spu_reg value,int id=-1 );
	void pulseDEBUG_ACQ_acq_dly_cfg( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_ACQ_trig_src_type( Spu_reg value,int id=-1 );
	void pulseDEBUG_ACQ_trig_src_type( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEBUG_ACQ( Spu_reg value,int id=-1 );
	void outDEBUG_ACQ( int id=-1 );


	Spu_reg getDEBUG_ACQ_samp_extract_vld(  int id = 0 );
	Spu_reg getDEBUG_ACQ_acq_dly_cfg_en(  int id = 0 );
	Spu_reg getDEBUG_ACQ_acq_dly_cfg(  int id = 0 );
	Spu_reg getDEBUG_ACQ_trig_src_type(  int id = 0 );

	Spu_reg getDEBUG_ACQ(  int id = 0 );

	Spu_reg inDEBUG_ACQ(  int id = 0 );

	void assignDEBUG_TX_spu_tx_cnt( Spu_reg value,int id=-1  );
	void assignDEBUG_TX_spu_frm_fsm( Spu_reg value,int id=-1  );
	void assignDEBUG_TX_tx_cnt_clr( Spu_reg value,int id=-1  );
	void assignDEBUG_TX_v_tx_bypass( Spu_reg value,int id=-1  );
	void assignDEBUG_TX_frm_head_jmp( Spu_reg value,int id=-1  );

	void assignDEBUG_TX( Spu_reg value,int id=-1 );


	void setDEBUG_TX_spu_tx_cnt( Spu_reg value,int id=-1 );
	void setDEBUG_TX_spu_frm_fsm( Spu_reg value,int id=-1 );
	void setDEBUG_TX_tx_cnt_clr( Spu_reg value,int id=-1 );
	void setDEBUG_TX_v_tx_bypass( Spu_reg value,int id=-1 );
	void setDEBUG_TX_frm_head_jmp( Spu_reg value,int id=-1 );

	void setDEBUG_TX( Spu_reg value,int id=-1 );
	void outDEBUG_TX_spu_tx_cnt( Spu_reg value,int id=-1 );
	void pulseDEBUG_TX_spu_tx_cnt( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_TX_spu_frm_fsm( Spu_reg value,int id=-1 );
	void pulseDEBUG_TX_spu_frm_fsm( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_TX_tx_cnt_clr( Spu_reg value,int id=-1 );
	void pulseDEBUG_TX_tx_cnt_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_TX_v_tx_bypass( Spu_reg value,int id=-1 );
	void pulseDEBUG_TX_v_tx_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_TX_frm_head_jmp( Spu_reg value,int id=-1 );
	void pulseDEBUG_TX_frm_head_jmp( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEBUG_TX( Spu_reg value,int id=-1 );
	void outDEBUG_TX( int id=-1 );


	Spu_reg getDEBUG_TX_spu_tx_cnt(  int id = 0 );
	Spu_reg getDEBUG_TX_spu_frm_fsm(  int id = 0 );
	Spu_reg getDEBUG_TX_tx_cnt_clr(  int id = 0 );
	Spu_reg getDEBUG_TX_v_tx_bypass(  int id = 0 );
	Spu_reg getDEBUG_TX_frm_head_jmp(  int id = 0 );

	Spu_reg getDEBUG_TX(  int id = 0 );

	Spu_reg inDEBUG_TX(  int id = 0 );

	void assignDEBUG_MEM_trig_tpu_coarse( Spu_reg value,int id=-1  );
	void assignDEBUG_MEM_mem_bram_dly( Spu_reg value,int id=-1  );
	void assignDEBUG_MEM_trig_offset_dot_pre( Spu_reg value,int id=-1  );
	void assignDEBUG_MEM_trig_dly_dot_out( Spu_reg value,int id=-1  );
	void assignDEBUG_MEM_trig_coarse_fine( Spu_reg value,int id=-1  );
	void assignDEBUG_MEM_mem_last_dot( Spu_reg value,int id=-1  );

	void assignDEBUG_MEM( Spu_reg value,int id=-1 );


	void setDEBUG_MEM_trig_tpu_coarse( Spu_reg value,int id=-1 );
	void setDEBUG_MEM_mem_bram_dly( Spu_reg value,int id=-1 );
	void setDEBUG_MEM_trig_offset_dot_pre( Spu_reg value,int id=-1 );
	void setDEBUG_MEM_trig_dly_dot_out( Spu_reg value,int id=-1 );
	void setDEBUG_MEM_trig_coarse_fine( Spu_reg value,int id=-1 );
	void setDEBUG_MEM_mem_last_dot( Spu_reg value,int id=-1 );

	void setDEBUG_MEM( Spu_reg value,int id=-1 );
	void outDEBUG_MEM_trig_tpu_coarse( Spu_reg value,int id=-1 );
	void pulseDEBUG_MEM_trig_tpu_coarse( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_MEM_mem_bram_dly( Spu_reg value,int id=-1 );
	void pulseDEBUG_MEM_mem_bram_dly( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_MEM_trig_offset_dot_pre( Spu_reg value,int id=-1 );
	void pulseDEBUG_MEM_trig_offset_dot_pre( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_MEM_trig_dly_dot_out( Spu_reg value,int id=-1 );
	void pulseDEBUG_MEM_trig_dly_dot_out( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_MEM_trig_coarse_fine( Spu_reg value,int id=-1 );
	void pulseDEBUG_MEM_trig_coarse_fine( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outDEBUG_MEM_mem_last_dot( Spu_reg value,int id=-1 );
	void pulseDEBUG_MEM_mem_last_dot( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outDEBUG_MEM( Spu_reg value,int id=-1 );
	void outDEBUG_MEM( int id=-1 );


	Spu_reg getDEBUG_MEM_trig_tpu_coarse(  int id = 0 );
	Spu_reg getDEBUG_MEM_mem_bram_dly(  int id = 0 );
	Spu_reg getDEBUG_MEM_trig_offset_dot_pre(  int id = 0 );
	Spu_reg getDEBUG_MEM_trig_dly_dot_out(  int id = 0 );
	Spu_reg getDEBUG_MEM_trig_coarse_fine(  int id = 0 );
	Spu_reg getDEBUG_MEM_mem_last_dot(  int id = 0 );

	Spu_reg getDEBUG_MEM(  int id = 0 );

	Spu_reg inDEBUG_MEM(  int id = 0 );

	Spu_reg getDEBUG_SCAN_pos_sa_view_frm_cnt(  int id = 0 );

	Spu_reg getDEBUG_SCAN(  int id = 0 );

	Spu_reg inDEBUG_SCAN(  int id = 0 );

	Spu_reg getRECORD_SUM_wav_view_len(  int id = 0 );
	Spu_reg getRECORD_SUM_wav_view_full(  int id = 0 );

	Spu_reg getRECORD_SUM(  int id = 0 );

	Spu_reg inRECORD_SUM(  int id = 0 );

	void assignSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value,int id=-1  );

	void assignSCAN_ROLL_FRM( Spu_reg value,int id=-1 );


	void setSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value,int id=-1 );

	void setSCAN_ROLL_FRM( Spu_reg value,int id=-1 );
	void outSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value,int id=-1 );
	void pulseSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outSCAN_ROLL_FRM( Spu_reg value,int id=-1 );
	void outSCAN_ROLL_FRM( int id=-1 );


	Spu_reg getSCAN_ROLL_FRM_scan_roll_frm_num(  int id = 0 );

	Spu_reg getSCAN_ROLL_FRM(  int id = 0 );

	Spu_reg inSCAN_ROLL_FRM(  int id = 0 );

	void assignADC_AVG_rate( Spu_reg value,int id=-1  );
	void assignADC_AVG_done( Spu_reg value,int id=-1  );
	void assignADC_AVG_en( Spu_reg value,int id=-1  );

	void assignADC_AVG( Spu_reg value,int id=-1 );


	void setADC_AVG_rate( Spu_reg value,int id=-1 );
	void setADC_AVG_done( Spu_reg value,int id=-1 );
	void setADC_AVG_en( Spu_reg value,int id=-1 );

	void setADC_AVG( Spu_reg value,int id=-1 );
	void outADC_AVG_rate( Spu_reg value,int id=-1 );
	void pulseADC_AVG_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_AVG_done( Spu_reg value,int id=-1 );
	void pulseADC_AVG_done( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outADC_AVG_en( Spu_reg value,int id=-1 );
	void pulseADC_AVG_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outADC_AVG( Spu_reg value,int id=-1 );
	void outADC_AVG( int id=-1 );


	Spu_reg getADC_AVG_rate(  int id = 0 );
	Spu_reg getADC_AVG_done(  int id = 0 );
	Spu_reg getADC_AVG_en(  int id = 0 );

	Spu_reg getADC_AVG(  int id = 0 );

	Spu_reg inADC_AVG(  int id = 0 );

	Spu_reg getADC_AVG_RES_AB_B(  int id = 0 );
	Spu_reg getADC_AVG_RES_AB_A(  int id = 0 );

	Spu_reg getADC_AVG_RES_AB(  int id = 0 );

	Spu_reg inADC_AVG_RES_AB(  int id = 0 );

	Spu_reg getADC_AVG_RES_CD_D(  int id = 0 );
	Spu_reg getADC_AVG_RES_CD_C(  int id = 0 );

	Spu_reg getADC_AVG_RES_CD(  int id = 0 );

	Spu_reg inADC_AVG_RES_CD(  int id = 0 );

	Spu_reg getADC_AVG_MAX_max_D(  int id = 0 );
	Spu_reg getADC_AVG_MAX_max_C(  int id = 0 );
	Spu_reg getADC_AVG_MAX_max_B(  int id = 0 );
	Spu_reg getADC_AVG_MAX_max_A(  int id = 0 );

	Spu_reg getADC_AVG_MAX(  int id = 0 );

	Spu_reg inADC_AVG_MAX(  int id = 0 );

	Spu_reg getADC_AVG_MIN_min_D(  int id = 0 );
	Spu_reg getADC_AVG_MIN_min_C(  int id = 0 );
	Spu_reg getADC_AVG_MIN_min_B(  int id = 0 );
	Spu_reg getADC_AVG_MIN_min_A(  int id = 0 );

	Spu_reg getADC_AVG_MIN(  int id = 0 );

	Spu_reg inADC_AVG_MIN(  int id = 0 );

	Spu_reg getLA_SA_RES_la(  int id = 0 );

	Spu_reg getLA_SA_RES(  int id = 0 );

	Spu_reg inLA_SA_RES(  int id = 0 );

	void assignZONE_TRIG_AUX_pulse_width( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_AUX_inv( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_AUX_pass_fail( Spu_reg value,int id=-1  );

	void assignZONE_TRIG_AUX( Spu_reg value,int id=-1 );


	void setZONE_TRIG_AUX_pulse_width( Spu_reg value,int id=-1 );
	void setZONE_TRIG_AUX_inv( Spu_reg value,int id=-1 );
	void setZONE_TRIG_AUX_pass_fail( Spu_reg value,int id=-1 );

	void setZONE_TRIG_AUX( Spu_reg value,int id=-1 );
	void outZONE_TRIG_AUX_pulse_width( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_AUX_pulse_width( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_AUX_inv( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_AUX_inv( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_AUX_pass_fail( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_AUX_pass_fail( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outZONE_TRIG_AUX( Spu_reg value,int id=-1 );
	void outZONE_TRIG_AUX( int id=-1 );


	Spu_reg getZONE_TRIG_AUX_pulse_width(  int id = 0 );
	Spu_reg getZONE_TRIG_AUX_inv(  int id = 0 );
	Spu_reg getZONE_TRIG_AUX_pass_fail(  int id = 0 );

	Spu_reg getZONE_TRIG_AUX(  int id = 0 );

	Spu_reg inZONE_TRIG_AUX(  int id = 0 );

	void assignZONE_TRIG_TAB1_threshold_l( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB1_threshold_h( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB1_threshold_en( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB1_column_addr( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB1_tab_index( Spu_reg value,int id=-1  );

	void assignZONE_TRIG_TAB1( Spu_reg value,int id=-1 );


	void setZONE_TRIG_TAB1_threshold_l( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB1_threshold_h( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB1_threshold_en( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB1_column_addr( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB1_tab_index( Spu_reg value,int id=-1 );

	void setZONE_TRIG_TAB1( Spu_reg value,int id=-1 );
	void outZONE_TRIG_TAB1_threshold_l( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB1_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB1_threshold_h( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB1_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB1_threshold_en( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB1_threshold_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB1_column_addr( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB1_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB1_tab_index( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB1_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outZONE_TRIG_TAB1( Spu_reg value,int id=-1 );
	void outZONE_TRIG_TAB1( int id=-1 );


	void assignZONE_TRIG_TAB2_threshold_l( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB2_threshold_h( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB2_threshold_en( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB2_column_addr( Spu_reg value,int id=-1  );
	void assignZONE_TRIG_TAB2_tab_index( Spu_reg value,int id=-1  );

	void assignZONE_TRIG_TAB2( Spu_reg value,int id=-1 );


	void setZONE_TRIG_TAB2_threshold_l( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB2_threshold_h( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB2_threshold_en( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB2_column_addr( Spu_reg value,int id=-1 );
	void setZONE_TRIG_TAB2_tab_index( Spu_reg value,int id=-1 );

	void setZONE_TRIG_TAB2( Spu_reg value,int id=-1 );
	void outZONE_TRIG_TAB2_threshold_l( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB2_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB2_threshold_h( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB2_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB2_threshold_en( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB2_threshold_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB2_column_addr( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB2_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outZONE_TRIG_TAB2_tab_index( Spu_reg value,int id=-1 );
	void pulseZONE_TRIG_TAB2_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outZONE_TRIG_TAB2( Spu_reg value,int id=-1 );
	void outZONE_TRIG_TAB2( int id=-1 );


	void assignMASK_TAB_threshold_l( Spu_reg value,int id=-1  );
	void assignMASK_TAB_threshold_l_less_en( Spu_reg value,int id=-1  );
	void assignMASK_TAB_threshold_h( Spu_reg value,int id=-1  );
	void assignMASK_TAB_threshold_h_greater_en( Spu_reg value,int id=-1  );
	void assignMASK_TAB_column_addr( Spu_reg value,int id=-1  );
	void assignMASK_TAB_tab_index( Spu_reg value,int id=-1  );

	void assignMASK_TAB( Spu_reg value,int id=-1 );


	void setMASK_TAB_threshold_l( Spu_reg value,int id=-1 );
	void setMASK_TAB_threshold_l_less_en( Spu_reg value,int id=-1 );
	void setMASK_TAB_threshold_h( Spu_reg value,int id=-1 );
	void setMASK_TAB_threshold_h_greater_en( Spu_reg value,int id=-1 );
	void setMASK_TAB_column_addr( Spu_reg value,int id=-1 );
	void setMASK_TAB_tab_index( Spu_reg value,int id=-1 );

	void setMASK_TAB( Spu_reg value,int id=-1 );
	void outMASK_TAB_threshold_l( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_threshold_l_less_en( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_threshold_l_less_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_threshold_h( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_threshold_h_greater_en( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_threshold_h_greater_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_column_addr( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_tab_index( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outMASK_TAB( Spu_reg value,int id=-1 );
	void outMASK_TAB( int id=-1 );


	Spu_reg getMASK_TAB_threshold_l(  int id = 0 );
	Spu_reg getMASK_TAB_threshold_l_less_en(  int id = 0 );
	Spu_reg getMASK_TAB_threshold_h(  int id = 0 );
	Spu_reg getMASK_TAB_threshold_h_greater_en(  int id = 0 );
	Spu_reg getMASK_TAB_column_addr(  int id = 0 );
	Spu_reg getMASK_TAB_tab_index(  int id = 0 );

	Spu_reg getMASK_TAB(  int id = 0 );

	Spu_reg inMASK_TAB(  int id = 0 );

	void assignMASK_TAB_EX_threshold_l( Spu_reg value,int id=-1  );
	void assignMASK_TAB_EX_threshold_l_greater_en( Spu_reg value,int id=-1  );
	void assignMASK_TAB_EX_threshold_h( Spu_reg value,int id=-1  );
	void assignMASK_TAB_EX_threshold_h_less_en( Spu_reg value,int id=-1  );
	void assignMASK_TAB_EX_column_addr( Spu_reg value,int id=-1  );
	void assignMASK_TAB_EX_tab_index( Spu_reg value,int id=-1  );

	void assignMASK_TAB_EX( Spu_reg value,int id=-1 );


	void setMASK_TAB_EX_threshold_l( Spu_reg value,int id=-1 );
	void setMASK_TAB_EX_threshold_l_greater_en( Spu_reg value,int id=-1 );
	void setMASK_TAB_EX_threshold_h( Spu_reg value,int id=-1 );
	void setMASK_TAB_EX_threshold_h_less_en( Spu_reg value,int id=-1 );
	void setMASK_TAB_EX_column_addr( Spu_reg value,int id=-1 );
	void setMASK_TAB_EX_tab_index( Spu_reg value,int id=-1 );

	void setMASK_TAB_EX( Spu_reg value,int id=-1 );
	void outMASK_TAB_EX_threshold_l( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_EX_threshold_l( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_EX_threshold_l_greater_en( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_EX_threshold_l_greater_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_EX_threshold_h( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_EX_threshold_h( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_EX_threshold_h_less_en( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_EX_threshold_h_less_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_EX_column_addr( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_EX_column_addr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_TAB_EX_tab_index( Spu_reg value,int id=-1 );
	void pulseMASK_TAB_EX_tab_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outMASK_TAB_EX( Spu_reg value,int id=-1 );
	void outMASK_TAB_EX( int id=-1 );


	Spu_reg getMASK_TAB_EX_threshold_l(  int id = 0 );
	Spu_reg getMASK_TAB_EX_threshold_l_greater_en(  int id = 0 );
	Spu_reg getMASK_TAB_EX_threshold_h(  int id = 0 );
	Spu_reg getMASK_TAB_EX_threshold_h_less_en(  int id = 0 );
	Spu_reg getMASK_TAB_EX_column_addr(  int id = 0 );
	Spu_reg getMASK_TAB_EX_tab_index(  int id = 0 );

	Spu_reg getMASK_TAB_EX(  int id = 0 );

	Spu_reg inMASK_TAB_EX(  int id = 0 );

	void assignMASK_COMPRESS_rate( Spu_reg value,int id=-1  );
	void assignMASK_COMPRESS_peak( Spu_reg value,int id=-1  );
	void assignMASK_COMPRESS_en( Spu_reg value,int id=-1  );

	void assignMASK_COMPRESS( Spu_reg value,int id=-1 );


	void setMASK_COMPRESS_rate( Spu_reg value,int id=-1 );
	void setMASK_COMPRESS_peak( Spu_reg value,int id=-1 );
	void setMASK_COMPRESS_en( Spu_reg value,int id=-1 );

	void setMASK_COMPRESS( Spu_reg value,int id=-1 );
	void outMASK_COMPRESS_rate( Spu_reg value,int id=-1 );
	void pulseMASK_COMPRESS_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_COMPRESS_peak( Spu_reg value,int id=-1 );
	void pulseMASK_COMPRESS_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_COMPRESS_en( Spu_reg value,int id=-1 );
	void pulseMASK_COMPRESS_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outMASK_COMPRESS( Spu_reg value,int id=-1 );
	void outMASK_COMPRESS( int id=-1 );


	Spu_reg getMASK_COMPRESS_rate(  int id = 0 );
	Spu_reg getMASK_COMPRESS_peak(  int id = 0 );
	Spu_reg getMASK_COMPRESS_en(  int id = 0 );

	Spu_reg getMASK_COMPRESS(  int id = 0 );

	Spu_reg inMASK_COMPRESS(  int id = 0 );

	void assignMASK_CTRL_cross_index( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_mask_index_en( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_zone1_trig_tab_en( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_zone2_trig_tab_en( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_zone1_trig_cross( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_zone2_trig_cross( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_mask_result_rd( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_sum_clr( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_cross_clr( Spu_reg value,int id=-1  );
	void assignMASK_CTRL_mask_we( Spu_reg value,int id=-1  );

	void assignMASK_CTRL( Spu_reg value,int id=-1 );


	void setMASK_CTRL_cross_index( Spu_reg value,int id=-1 );
	void setMASK_CTRL_mask_index_en( Spu_reg value,int id=-1 );
	void setMASK_CTRL_zone1_trig_tab_en( Spu_reg value,int id=-1 );
	void setMASK_CTRL_zone2_trig_tab_en( Spu_reg value,int id=-1 );
	void setMASK_CTRL_zone1_trig_cross( Spu_reg value,int id=-1 );
	void setMASK_CTRL_zone2_trig_cross( Spu_reg value,int id=-1 );
	void setMASK_CTRL_mask_result_rd( Spu_reg value,int id=-1 );
	void setMASK_CTRL_sum_clr( Spu_reg value,int id=-1 );
	void setMASK_CTRL_cross_clr( Spu_reg value,int id=-1 );
	void setMASK_CTRL_mask_we( Spu_reg value,int id=-1 );

	void setMASK_CTRL( Spu_reg value,int id=-1 );
	void outMASK_CTRL_cross_index( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_cross_index( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_mask_index_en( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_mask_index_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_zone1_trig_tab_en( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_zone1_trig_tab_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_zone2_trig_tab_en( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_zone2_trig_tab_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_zone1_trig_cross( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_zone1_trig_cross( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_zone2_trig_cross( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_zone2_trig_cross( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_mask_result_rd( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_mask_result_rd( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_sum_clr( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_sum_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_cross_clr( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_cross_clr( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_CTRL_mask_we( Spu_reg value,int id=-1 );
	void pulseMASK_CTRL_mask_we( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outMASK_CTRL( Spu_reg value,int id=-1 );
	void outMASK_CTRL( int id=-1 );


	Spu_reg getMASK_CTRL_cross_index(  int id = 0 );
	Spu_reg getMASK_CTRL_mask_index_en(  int id = 0 );
	Spu_reg getMASK_CTRL_zone1_trig_tab_en(  int id = 0 );
	Spu_reg getMASK_CTRL_zone2_trig_tab_en(  int id = 0 );
	Spu_reg getMASK_CTRL_zone1_trig_cross(  int id = 0 );
	Spu_reg getMASK_CTRL_zone2_trig_cross(  int id = 0 );
	Spu_reg getMASK_CTRL_mask_result_rd(  int id = 0 );
	Spu_reg getMASK_CTRL_sum_clr(  int id = 0 );
	Spu_reg getMASK_CTRL_cross_clr(  int id = 0 );
	Spu_reg getMASK_CTRL_mask_we(  int id = 0 );

	Spu_reg getMASK_CTRL(  int id = 0 );

	Spu_reg inMASK_CTRL(  int id = 0 );

	Spu_reg getMASK_FRM_CNT_L(  int id = 0 );

	Spu_reg inMASK_FRM_CNT_L(  int id = 0 );

	Spu_reg getMASK_FRM_CNT_H(  int id = 0 );

	Spu_reg inMASK_FRM_CNT_H(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_L_CHA(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_L_CHA(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_H_CHA(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_H_CHA(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_L_CHB(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_L_CHB(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_H_CHB(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_H_CHB(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_L_CHC(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_L_CHC(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_H_CHC(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_H_CHC(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_L_CHD(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_L_CHD(  int id = 0 );

	Spu_reg getMASK_CROSS_CNT_H_CHD(  int id = 0 );

	Spu_reg inMASK_CROSS_CNT_H_CHD(  int id = 0 );

	void assignMASK_OUTPUT_pulse_width( Spu_reg value,int id=-1  );
	void assignMASK_OUTPUT_inv( Spu_reg value,int id=-1  );
	void assignMASK_OUTPUT_pass_fail( Spu_reg value,int id=-1  );

	void assignMASK_OUTPUT( Spu_reg value,int id=-1 );


	void setMASK_OUTPUT_pulse_width( Spu_reg value,int id=-1 );
	void setMASK_OUTPUT_inv( Spu_reg value,int id=-1 );
	void setMASK_OUTPUT_pass_fail( Spu_reg value,int id=-1 );

	void setMASK_OUTPUT( Spu_reg value,int id=-1 );
	void outMASK_OUTPUT_pulse_width( Spu_reg value,int id=-1 );
	void pulseMASK_OUTPUT_pulse_width( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_OUTPUT_inv( Spu_reg value,int id=-1 );
	void pulseMASK_OUTPUT_inv( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outMASK_OUTPUT_pass_fail( Spu_reg value,int id=-1 );
	void pulseMASK_OUTPUT_pass_fail( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outMASK_OUTPUT( Spu_reg value,int id=-1 );
	void outMASK_OUTPUT( int id=-1 );


	Spu_reg getMASK_OUTPUT_pulse_width(  int id = 0 );
	Spu_reg getMASK_OUTPUT_inv(  int id = 0 );
	Spu_reg getMASK_OUTPUT_pass_fail(  int id = 0 );

	Spu_reg getMASK_OUTPUT(  int id = 0 );

	Spu_reg inMASK_OUTPUT(  int id = 0 );

	void assignSEARCH_OFFSET_CH( Spu_reg value,int id=-1 );

	void setSEARCH_OFFSET_CH( Spu_reg value,int id=-1 );
	void outSEARCH_OFFSET_CH( Spu_reg value,int id=-1 );
	void outSEARCH_OFFSET_CH( int id=-1 );

	Spu_reg getSEARCH_OFFSET_CH(  int id = 0 );

	Spu_reg inSEARCH_OFFSET_CH(  int id = 0 );

	void assignSEARCH_LEN_CH( Spu_reg value,int id=-1 );

	void setSEARCH_LEN_CH( Spu_reg value,int id=-1 );
	void outSEARCH_LEN_CH( Spu_reg value,int id=-1 );
	void outSEARCH_LEN_CH( int id=-1 );

	Spu_reg getSEARCH_LEN_CH(  int id = 0 );

	Spu_reg inSEARCH_LEN_CH(  int id = 0 );

	void assignSEARCH_OFFSET_LA( Spu_reg value,int id=-1 );

	void setSEARCH_OFFSET_LA( Spu_reg value,int id=-1 );
	void outSEARCH_OFFSET_LA( Spu_reg value,int id=-1 );
	void outSEARCH_OFFSET_LA( int id=-1 );

	Spu_reg getSEARCH_OFFSET_LA(  int id = 0 );

	Spu_reg inSEARCH_OFFSET_LA(  int id = 0 );

	void assignSEARCH_LEN_LA( Spu_reg value,int id=-1 );

	void setSEARCH_LEN_LA( Spu_reg value,int id=-1 );
	void outSEARCH_LEN_LA( Spu_reg value,int id=-1 );
	void outSEARCH_LEN_LA( int id=-1 );

	Spu_reg getSEARCH_LEN_LA(  int id = 0 );

	Spu_reg inSEARCH_LEN_LA(  int id = 0 );

	void assignTIME_TAG_L( Spu_reg value,int id=-1 );

	void setTIME_TAG_L( Spu_reg value,int id=-1 );
	void outTIME_TAG_L( Spu_reg value,int id=-1 );
	void outTIME_TAG_L( int id=-1 );

	Spu_reg getTIME_TAG_L(  int id = 0 );

	Spu_reg inTIME_TAG_L(  int id = 0 );

	void assignTIME_TAG_H( Spu_reg value,int id=-1 );

	void setTIME_TAG_H( Spu_reg value,int id=-1 );
	void outTIME_TAG_H( Spu_reg value,int id=-1 );
	void outTIME_TAG_H( int id=-1 );

	Spu_reg getTIME_TAG_H(  int id = 0 );

	Spu_reg inTIME_TAG_H(  int id = 0 );

	void assignFINE_TRIG_POSITION( Spu_reg value,int id=-1 );

	void setFINE_TRIG_POSITION( Spu_reg value,int id=-1 );
	void outFINE_TRIG_POSITION( Spu_reg value,int id=-1 );
	void outFINE_TRIG_POSITION( int id=-1 );

	Spu_reg getFINE_TRIG_POSITION(  int id = 0 );

	Spu_reg inFINE_TRIG_POSITION(  int id = 0 );

	void assignTIME_TAG_CTRL_rd_en( Spu_reg value,int id=-1  );
	void assignTIME_TAG_CTRL_rec_first_done( Spu_reg value,int id=-1  );

	void assignTIME_TAG_CTRL( Spu_reg value,int id=-1 );


	void setTIME_TAG_CTRL_rd_en( Spu_reg value,int id=-1 );
	void setTIME_TAG_CTRL_rec_first_done( Spu_reg value,int id=-1 );

	void setTIME_TAG_CTRL( Spu_reg value,int id=-1 );
	void outTIME_TAG_CTRL_rd_en( Spu_reg value,int id=-1 );
	void pulseTIME_TAG_CTRL_rd_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outTIME_TAG_CTRL_rec_first_done( Spu_reg value,int id=-1 );
	void pulseTIME_TAG_CTRL_rec_first_done( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outTIME_TAG_CTRL( Spu_reg value,int id=-1 );
	void outTIME_TAG_CTRL( int id=-1 );


	Spu_reg getTIME_TAG_CTRL_rd_en(  int id = 0 );
	Spu_reg getTIME_TAG_CTRL_rec_first_done(  int id = 0 );

	Spu_reg getTIME_TAG_CTRL(  int id = 0 );

	Spu_reg inTIME_TAG_CTRL(  int id = 0 );

	void assignIMPORT_INFO_import_done( Spu_reg value,int id=-1  );

	void assignIMPORT_INFO( Spu_reg value,int id=-1 );


	void setIMPORT_INFO_import_done( Spu_reg value,int id=-1 );

	void setIMPORT_INFO( Spu_reg value,int id=-1 );
	void outIMPORT_INFO_import_done( Spu_reg value,int id=-1 );
	void pulseIMPORT_INFO_import_done( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outIMPORT_INFO( Spu_reg value,int id=-1 );
	void outIMPORT_INFO( int id=-1 );


	Spu_reg getIMPORT_INFO_import_done(  int id = 0 );

	Spu_reg getIMPORT_INFO(  int id = 0 );

	Spu_reg inIMPORT_INFO(  int id = 0 );

	Spu_reg getTIME_TAG_L_REC_FIRST(  int id = 0 );

	Spu_reg inTIME_TAG_L_REC_FIRST(  int id = 0 );

	Spu_reg getTIME_TAG_H_REC_FIRST(  int id = 0 );

	Spu_reg inTIME_TAG_H_REC_FIRST(  int id = 0 );

	void assignTRACE_AVG_RATE_X( Spu_reg value,int id=-1 );

	void setTRACE_AVG_RATE_X( Spu_reg value,int id=-1 );
	void outTRACE_AVG_RATE_X( Spu_reg value,int id=-1 );
	void outTRACE_AVG_RATE_X( int id=-1 );

	Spu_reg getTRACE_AVG_RATE_X(  int id = 0 );

	Spu_reg inTRACE_AVG_RATE_X(  int id = 0 );

	void assignTRACE_AVG_RATE_Y( Spu_reg value,int id=-1 );

	void setTRACE_AVG_RATE_Y( Spu_reg value,int id=-1 );
	void outTRACE_AVG_RATE_Y( Spu_reg value,int id=-1 );
	void outTRACE_AVG_RATE_Y( int id=-1 );

	Spu_reg getTRACE_AVG_RATE_Y(  int id = 0 );

	Spu_reg inTRACE_AVG_RATE_Y(  int id = 0 );

	void assignLINEAR_INTX_CTRL_mian_bypass( Spu_reg value,int id=-1  );
	void assignLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value,int id=-1  );

	void assignLINEAR_INTX_CTRL( Spu_reg value,int id=-1 );


	void setLINEAR_INTX_CTRL_mian_bypass( Spu_reg value,int id=-1 );
	void setLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value,int id=-1 );

	void setLINEAR_INTX_CTRL( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_CTRL_mian_bypass( Spu_reg value,int id=-1 );
	void pulseLINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value,int id=-1 );
	void pulseLINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outLINEAR_INTX_CTRL( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_CTRL( int id=-1 );


	Spu_reg getLINEAR_INTX_CTRL_mian_bypass(  int id = 0 );
	Spu_reg getLINEAR_INTX_CTRL_zoom_bypass(  int id = 0 );

	Spu_reg getLINEAR_INTX_CTRL(  int id = 0 );

	Spu_reg inLINEAR_INTX_CTRL(  int id = 0 );

	void assignLINEAR_INTX_INIT( Spu_reg value,int id=-1 );

	void setLINEAR_INTX_INIT( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_INIT( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_INIT( int id=-1 );

	Spu_reg getLINEAR_INTX_INIT(  int id = 0 );

	Spu_reg inLINEAR_INTX_INIT(  int id = 0 );

	void assignLINEAR_INTX_STEP( Spu_reg value,int id=-1 );

	void setLINEAR_INTX_STEP( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_STEP( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_STEP( int id=-1 );

	Spu_reg getLINEAR_INTX_STEP(  int id = 0 );

	Spu_reg inLINEAR_INTX_STEP(  int id = 0 );

	void assignLINEAR_INTX_INIT_ZOOM( Spu_reg value,int id=-1 );

	void setLINEAR_INTX_INIT_ZOOM( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_INIT_ZOOM( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_INIT_ZOOM( int id=-1 );

	Spu_reg getLINEAR_INTX_INIT_ZOOM(  int id = 0 );

	Spu_reg inLINEAR_INTX_INIT_ZOOM(  int id = 0 );

	void assignLINEAR_INTX_STEP_ZOOM( Spu_reg value,int id=-1 );

	void setLINEAR_INTX_STEP_ZOOM( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_STEP_ZOOM( Spu_reg value,int id=-1 );
	void outLINEAR_INTX_STEP_ZOOM( int id=-1 );

	Spu_reg getLINEAR_INTX_STEP_ZOOM(  int id = 0 );

	Spu_reg inLINEAR_INTX_STEP_ZOOM(  int id = 0 );

	void assignWAVE_OFFSET_EYE( Spu_reg value,int id=-1 );

	void setWAVE_OFFSET_EYE( Spu_reg value,int id=-1 );
	void outWAVE_OFFSET_EYE( Spu_reg value,int id=-1 );
	void outWAVE_OFFSET_EYE( int id=-1 );

	Spu_reg getWAVE_OFFSET_EYE(  int id = 0 );

	Spu_reg inWAVE_OFFSET_EYE(  int id = 0 );

	void assignWAVE_LEN_EYE( Spu_reg value,int id=-1 );

	void setWAVE_LEN_EYE( Spu_reg value,int id=-1 );
	void outWAVE_LEN_EYE( Spu_reg value,int id=-1 );
	void outWAVE_LEN_EYE( int id=-1 );

	Spu_reg getWAVE_LEN_EYE(  int id = 0 );

	Spu_reg inWAVE_LEN_EYE(  int id = 0 );

	void assignCOMPRESS_EYE_rate( Spu_reg value,int id=-1  );
	void assignCOMPRESS_EYE_peak( Spu_reg value,int id=-1  );
	void assignCOMPRESS_EYE_en( Spu_reg value,int id=-1  );

	void assignCOMPRESS_EYE( Spu_reg value,int id=-1 );


	void setCOMPRESS_EYE_rate( Spu_reg value,int id=-1 );
	void setCOMPRESS_EYE_peak( Spu_reg value,int id=-1 );
	void setCOMPRESS_EYE_en( Spu_reg value,int id=-1 );

	void setCOMPRESS_EYE( Spu_reg value,int id=-1 );
	void outCOMPRESS_EYE_rate( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_EYE_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOMPRESS_EYE_peak( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_EYE_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outCOMPRESS_EYE_en( Spu_reg value,int id=-1 );
	void pulseCOMPRESS_EYE_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outCOMPRESS_EYE( Spu_reg value,int id=-1 );
	void outCOMPRESS_EYE( int id=-1 );


	Spu_reg getCOMPRESS_EYE_rate(  int id = 0 );
	Spu_reg getCOMPRESS_EYE_peak(  int id = 0 );
	Spu_reg getCOMPRESS_EYE_en(  int id = 0 );

	Spu_reg getCOMPRESS_EYE(  int id = 0 );

	Spu_reg inCOMPRESS_EYE(  int id = 0 );

	void assignTX_LEN_EYE( Spu_reg value,int id=-1 );

	void setTX_LEN_EYE( Spu_reg value,int id=-1 );
	void outTX_LEN_EYE( Spu_reg value,int id=-1 );
	void outTX_LEN_EYE( int id=-1 );

	Spu_reg getTX_LEN_EYE(  int id = 0 );

	Spu_reg inTX_LEN_EYE(  int id = 0 );

	void assignLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value,int id=-1  );
	void assignLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value,int id=-1  );

	void assignLA_LINEAR_INTX_CTRL( Spu_reg value,int id=-1 );


	void setLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value,int id=-1 );
	void setLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value,int id=-1 );

	void setLA_LINEAR_INTX_CTRL( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value,int id=-1 );
	void pulseLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value,int id=-1 );
	void pulseLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outLA_LINEAR_INTX_CTRL( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_CTRL( int id=-1 );


	Spu_reg getLA_LINEAR_INTX_CTRL_mian_bypass(  int id = 0 );
	Spu_reg getLA_LINEAR_INTX_CTRL_zoom_bypass(  int id = 0 );

	Spu_reg getLA_LINEAR_INTX_CTRL(  int id = 0 );

	Spu_reg inLA_LINEAR_INTX_CTRL(  int id = 0 );

	void assignLA_LINEAR_INTX_INIT( Spu_reg value,int id=-1 );

	void setLA_LINEAR_INTX_INIT( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_INIT( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_INIT( int id=-1 );

	Spu_reg getLA_LINEAR_INTX_INIT(  int id = 0 );

	Spu_reg inLA_LINEAR_INTX_INIT(  int id = 0 );

	void assignLA_LINEAR_INTX_STEP( Spu_reg value,int id=-1 );

	void setLA_LINEAR_INTX_STEP( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_STEP( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_STEP( int id=-1 );

	Spu_reg getLA_LINEAR_INTX_STEP(  int id = 0 );

	Spu_reg inLA_LINEAR_INTX_STEP(  int id = 0 );

	void assignLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value,int id=-1 );

	void setLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_INIT_ZOOM( int id=-1 );

	Spu_reg getLA_LINEAR_INTX_INIT_ZOOM(  int id = 0 );

	Spu_reg inLA_LINEAR_INTX_INIT_ZOOM(  int id = 0 );

	void assignLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value,int id=-1 );

	void setLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value,int id=-1 );
	void outLA_LINEAR_INTX_STEP_ZOOM( int id=-1 );

	Spu_reg getLA_LINEAR_INTX_STEP_ZOOM(  int id = 0 );

	Spu_reg inLA_LINEAR_INTX_STEP_ZOOM(  int id = 0 );

	void assignTRACE_COMPRESS_MAIN_rate( Spu_reg value,int id=-1  );
	void assignTRACE_COMPRESS_MAIN_peak( Spu_reg value,int id=-1  );
	void assignTRACE_COMPRESS_MAIN_en( Spu_reg value,int id=-1  );

	void assignTRACE_COMPRESS_MAIN( Spu_reg value,int id=-1 );


	void setTRACE_COMPRESS_MAIN_rate( Spu_reg value,int id=-1 );
	void setTRACE_COMPRESS_MAIN_peak( Spu_reg value,int id=-1 );
	void setTRACE_COMPRESS_MAIN_en( Spu_reg value,int id=-1 );

	void setTRACE_COMPRESS_MAIN( Spu_reg value,int id=-1 );
	void outTRACE_COMPRESS_MAIN_rate( Spu_reg value,int id=-1 );
	void pulseTRACE_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_COMPRESS_MAIN_peak( Spu_reg value,int id=-1 );
	void pulseTRACE_COMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_COMPRESS_MAIN_en( Spu_reg value,int id=-1 );
	void pulseTRACE_COMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outTRACE_COMPRESS_MAIN( Spu_reg value,int id=-1 );
	void outTRACE_COMPRESS_MAIN( int id=-1 );


	Spu_reg getTRACE_COMPRESS_MAIN_rate(  int id = 0 );
	Spu_reg getTRACE_COMPRESS_MAIN_peak(  int id = 0 );
	Spu_reg getTRACE_COMPRESS_MAIN_en(  int id = 0 );

	Spu_reg getTRACE_COMPRESS_MAIN(  int id = 0 );

	Spu_reg inTRACE_COMPRESS_MAIN(  int id = 0 );

	void assignTRACE_COMPRESS_ZOOM_rate( Spu_reg value,int id=-1  );
	void assignTRACE_COMPRESS_ZOOM_peak( Spu_reg value,int id=-1  );
	void assignTRACE_COMPRESS_ZOOM_en( Spu_reg value,int id=-1  );

	void assignTRACE_COMPRESS_ZOOM( Spu_reg value,int id=-1 );


	void setTRACE_COMPRESS_ZOOM_rate( Spu_reg value,int id=-1 );
	void setTRACE_COMPRESS_ZOOM_peak( Spu_reg value,int id=-1 );
	void setTRACE_COMPRESS_ZOOM_en( Spu_reg value,int id=-1 );

	void setTRACE_COMPRESS_ZOOM( Spu_reg value,int id=-1 );
	void outTRACE_COMPRESS_ZOOM_rate( Spu_reg value,int id=-1 );
	void pulseTRACE_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_COMPRESS_ZOOM_peak( Spu_reg value,int id=-1 );
	void pulseTRACE_COMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );
	void outTRACE_COMPRESS_ZOOM_en( Spu_reg value,int id=-1 );
	void pulseTRACE_COMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outTRACE_COMPRESS_ZOOM( Spu_reg value,int id=-1 );
	void outTRACE_COMPRESS_ZOOM( int id=-1 );


	Spu_reg getTRACE_COMPRESS_ZOOM_rate(  int id = 0 );
	Spu_reg getTRACE_COMPRESS_ZOOM_peak(  int id = 0 );
	Spu_reg getTRACE_COMPRESS_ZOOM_en(  int id = 0 );

	Spu_reg getTRACE_COMPRESS_ZOOM(  int id = 0 );

	Spu_reg inTRACE_COMPRESS_ZOOM(  int id = 0 );

	void assignZYNQ_COMPRESS_MAIN_rate( Spu_reg value,int id=-1  );

	void assignZYNQ_COMPRESS_MAIN( Spu_reg value,int id=-1 );


	void setZYNQ_COMPRESS_MAIN_rate( Spu_reg value,int id=-1 );

	void setZYNQ_COMPRESS_MAIN( Spu_reg value,int id=-1 );
	void outZYNQ_COMPRESS_MAIN_rate( Spu_reg value,int id=-1 );
	void pulseZYNQ_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outZYNQ_COMPRESS_MAIN( Spu_reg value,int id=-1 );
	void outZYNQ_COMPRESS_MAIN( int id=-1 );


	Spu_reg getZYNQ_COMPRESS_MAIN_rate(  int id = 0 );

	Spu_reg getZYNQ_COMPRESS_MAIN(  int id = 0 );

	Spu_reg inZYNQ_COMPRESS_MAIN(  int id = 0 );

	void assignZYNQ_COMPRESS_ZOOM_rate( Spu_reg value,int id=-1  );

	void assignZYNQ_COMPRESS_ZOOM( Spu_reg value,int id=-1 );


	void setZYNQ_COMPRESS_ZOOM_rate( Spu_reg value,int id=-1 );

	void setZYNQ_COMPRESS_ZOOM( Spu_reg value,int id=-1 );
	void outZYNQ_COMPRESS_ZOOM_rate( Spu_reg value,int id=-1 );
	void pulseZYNQ_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV=0, int timeus=0,int id=-1 );

	void outZYNQ_COMPRESS_ZOOM( Spu_reg value,int id=-1 );
	void outZYNQ_COMPRESS_ZOOM( int id=-1 );


	Spu_reg getZYNQ_COMPRESS_ZOOM_rate(  int id = 0 );

	Spu_reg getZYNQ_COMPRESS_ZOOM(  int id = 0 );

	Spu_reg inZYNQ_COMPRESS_ZOOM(  int id = 0 );

	Spu_reg getDEBUG_TX_LA_spu_tx_cnt(  int id = 0 );

	Spu_reg getDEBUG_TX_LA(  int id = 0 );

	Spu_reg inDEBUG_TX_LA(  int id = 0 );

	Spu_reg getDEBUG_TX_FRM_spu_tx_frm_cnt(  int id = 0 );

	Spu_reg getDEBUG_TX_FRM(  int id = 0 );

	Spu_reg inDEBUG_TX_FRM(  int id = 0 );

	Spu_reg getIMPORT_CNT_CH_cnt(  int id = 0 );
	Spu_reg getIMPORT_CNT_CH_cnt_clr(  int id = 0 );

	Spu_reg getIMPORT_CNT_CH(  int id = 0 );

	Spu_reg inIMPORT_CNT_CH(  int id = 0 );

	Spu_reg getIMPORT_CNT_LA_cnt(  int id = 0 );
	Spu_reg getIMPORT_CNT_LA_cnt_clr(  int id = 0 );

	Spu_reg getIMPORT_CNT_LA(  int id = 0 );

	Spu_reg inIMPORT_CNT_LA(  int id = 0 );

	Spu_reg getDEBUG_LINEAR_INTX1(  int id = 0 );

	Spu_reg inDEBUG_LINEAR_INTX1(  int id = 0 );

	Spu_reg getDEBUG_LINEAR_INTX2(  int id = 0 );

	Spu_reg inDEBUG_LINEAR_INTX2(  int id = 0 );

	Spu_reg getDEBUG_DDR(  int id = 0 );

	Spu_reg inDEBUG_DDR(  int id = 0 );

	Spu_reg getDEBUG_TX_LEN_BURST_tx_length_burst(  int id = 0 );

	Spu_reg getDEBUG_TX_LEN_BURST(  int id = 0 );

	Spu_reg inDEBUG_TX_LEN_BURST(  int id = 0 );

};
#endif
