#include "scu.h"
Scu_reg ScuMemProxy::getVERSION()
{
	return VERSION;
}


void ScuMemProxy::assignSCU_STATE_scu_fsm( Scu_reg value )
{
	mem_assign_field(SCU_STATE,scu_fsm,value);
}
void ScuMemProxy::assignSCU_STATE_scu_sa_fsm( Scu_reg value )
{
	mem_assign_field(SCU_STATE,scu_sa_fsm,value);
}
void ScuMemProxy::assignSCU_STATE_sys_stop( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_stop,value);
}
void ScuMemProxy::assignSCU_STATE_scu_stop( Scu_reg value )
{
	mem_assign_field(SCU_STATE,scu_stop,value);
}
void ScuMemProxy::assignSCU_STATE_scu_loop( Scu_reg value )
{
	mem_assign_field(SCU_STATE,scu_loop,value);
}
void ScuMemProxy::assignSCU_STATE_sys_force_stop( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_force_stop,value);
}
void ScuMemProxy::assignSCU_STATE_process_sa( Scu_reg value )
{
	mem_assign_field(SCU_STATE,process_sa,value);
}
void ScuMemProxy::assignSCU_STATE_process_sa_done( Scu_reg value )
{
	mem_assign_field(SCU_STATE,process_sa_done,value);
}
void ScuMemProxy::assignSCU_STATE_sys_sa_en( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_sa_en,value);
}
void ScuMemProxy::assignSCU_STATE_sys_pre_sa_done( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_pre_sa_done,value);
}
void ScuMemProxy::assignSCU_STATE_sys_pos_sa_done( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_pos_sa_done,value);
}
void ScuMemProxy::assignSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value )
{
	mem_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,value);
}
void ScuMemProxy::assignSCU_STATE_sys_trig_d( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_trig_d,value);
}
void ScuMemProxy::assignSCU_STATE_tpu_scu_trig( Scu_reg value )
{
	mem_assign_field(SCU_STATE,tpu_scu_trig,value);
}
void ScuMemProxy::assignSCU_STATE_fine_trig_done_buf( Scu_reg value )
{
	mem_assign_field(SCU_STATE,fine_trig_done_buf,value);
}
void ScuMemProxy::assignSCU_STATE_avg_proc_done( Scu_reg value )
{
	mem_assign_field(SCU_STATE,avg_proc_done,value);
}
void ScuMemProxy::assignSCU_STATE_sys_avg_proc( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_avg_proc,value);
}
void ScuMemProxy::assignSCU_STATE_sys_avg_exp( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_avg_exp,value);
}
void ScuMemProxy::assignSCU_STATE_sys_wave_rd_en( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_wave_rd_en,value);
}
void ScuMemProxy::assignSCU_STATE_sys_wave_proc_en( Scu_reg value )
{
	mem_assign_field(SCU_STATE,sys_wave_proc_en,value);
}
void ScuMemProxy::assignSCU_STATE_wave_mem_rd_done_hold( Scu_reg value )
{
	mem_assign_field(SCU_STATE,wave_mem_rd_done_hold,value);
}
void ScuMemProxy::assignSCU_STATE_spu_tx_done_hold( Scu_reg value )
{
	mem_assign_field(SCU_STATE,spu_tx_done_hold,value);
}
void ScuMemProxy::assignSCU_STATE_gt_wave_tx_done( Scu_reg value )
{
	mem_assign_field(SCU_STATE,gt_wave_tx_done,value);
}
void ScuMemProxy::assignSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value )
{
	mem_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,value);
}
void ScuMemProxy::assignSCU_STATE_task_wave_tx_done_LA( Scu_reg value )
{
	mem_assign_field(SCU_STATE,task_wave_tx_done_LA,value);
}
void ScuMemProxy::assignSCU_STATE_task_wave_tx_done_CH( Scu_reg value )
{
	mem_assign_field(SCU_STATE,task_wave_tx_done_CH,value);
}
void ScuMemProxy::assignSCU_STATE_spu_proc_done( Scu_reg value )
{
	mem_assign_field(SCU_STATE,spu_proc_done,value);
}
void ScuMemProxy::assignSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value )
{
	mem_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,value);
}

void ScuMemProxy::assignSCU_STATE( Scu_reg value )
{
	mem_assign_field(SCU_STATE,payload,value);
}


Scu_reg ScuMemProxy::getSCU_STATE_scu_fsm()
{
	return SCU_STATE.scu_fsm;
}
Scu_reg ScuMemProxy::getSCU_STATE_scu_sa_fsm()
{
	return SCU_STATE.scu_sa_fsm;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_stop()
{
	return SCU_STATE.sys_stop;
}
Scu_reg ScuMemProxy::getSCU_STATE_scu_stop()
{
	return SCU_STATE.scu_stop;
}
Scu_reg ScuMemProxy::getSCU_STATE_scu_loop()
{
	return SCU_STATE.scu_loop;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_force_stop()
{
	return SCU_STATE.sys_force_stop;
}
Scu_reg ScuMemProxy::getSCU_STATE_process_sa()
{
	return SCU_STATE.process_sa;
}
Scu_reg ScuMemProxy::getSCU_STATE_process_sa_done()
{
	return SCU_STATE.process_sa_done;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_sa_en()
{
	return SCU_STATE.sys_sa_en;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_pre_sa_done()
{
	return SCU_STATE.sys_pre_sa_done;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_pos_sa_done()
{
	return SCU_STATE.sys_pos_sa_done;
}
Scu_reg ScuMemProxy::getSCU_STATE_spu_wave_mem_wr_done_hold()
{
	return SCU_STATE.spu_wave_mem_wr_done_hold;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_trig_d()
{
	return SCU_STATE.sys_trig_d;
}
Scu_reg ScuMemProxy::getSCU_STATE_tpu_scu_trig()
{
	return SCU_STATE.tpu_scu_trig;
}
Scu_reg ScuMemProxy::getSCU_STATE_fine_trig_done_buf()
{
	return SCU_STATE.fine_trig_done_buf;
}
Scu_reg ScuMemProxy::getSCU_STATE_avg_proc_done()
{
	return SCU_STATE.avg_proc_done;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_avg_proc()
{
	return SCU_STATE.sys_avg_proc;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_avg_exp()
{
	return SCU_STATE.sys_avg_exp;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_wave_rd_en()
{
	return SCU_STATE.sys_wave_rd_en;
}
Scu_reg ScuMemProxy::getSCU_STATE_sys_wave_proc_en()
{
	return SCU_STATE.sys_wave_proc_en;
}
Scu_reg ScuMemProxy::getSCU_STATE_wave_mem_rd_done_hold()
{
	return SCU_STATE.wave_mem_rd_done_hold;
}
Scu_reg ScuMemProxy::getSCU_STATE_spu_tx_done_hold()
{
	return SCU_STATE.spu_tx_done_hold;
}
Scu_reg ScuMemProxy::getSCU_STATE_gt_wave_tx_done()
{
	return SCU_STATE.gt_wave_tx_done;
}
Scu_reg ScuMemProxy::getSCU_STATE_spu_wav_rd_meas_once_tx_done()
{
	return SCU_STATE.spu_wav_rd_meas_once_tx_done;
}
Scu_reg ScuMemProxy::getSCU_STATE_task_wave_tx_done_LA()
{
	return SCU_STATE.task_wave_tx_done_LA;
}
Scu_reg ScuMemProxy::getSCU_STATE_task_wave_tx_done_CH()
{
	return SCU_STATE.task_wave_tx_done_CH;
}
Scu_reg ScuMemProxy::getSCU_STATE_spu_proc_done()
{
	return SCU_STATE.spu_proc_done;
}
Scu_reg ScuMemProxy::getSCU_STATE_scan_roll_sa_done_mem_tx_null()
{
	return SCU_STATE.scan_roll_sa_done_mem_tx_null;
}

Scu_reg ScuMemProxy::getSCU_STATE()
{
	return SCU_STATE.payload;
}


void ScuMemProxy::assignCTRL_sys_stop_state( Scu_reg value )
{
	mem_assign_field(CTRL,sys_stop_state,value);
}
void ScuMemProxy::assignCTRL_cfg_fsm_run_stop_n( Scu_reg value )
{
	mem_assign_field(CTRL,cfg_fsm_run_stop_n,value);
}
void ScuMemProxy::assignCTRL_run_single( Scu_reg value )
{
	mem_assign_field(CTRL,run_single,value);
}
void ScuMemProxy::assignCTRL_mode_play_last( Scu_reg value )
{
	mem_assign_field(CTRL,mode_play_last,value);
}
void ScuMemProxy::assignCTRL_scu_fsm_run( Scu_reg value )
{
	mem_assign_field(CTRL,scu_fsm_run,value);
}
void ScuMemProxy::assignCTRL_force_trig( Scu_reg value )
{
	mem_assign_field(CTRL,force_trig,value);
}
void ScuMemProxy::assignCTRL_wpu_plot_display_finish( Scu_reg value )
{
	mem_assign_field(CTRL,wpu_plot_display_finish,value);
}
void ScuMemProxy::assignCTRL_soft_reset_gt( Scu_reg value )
{
	mem_assign_field(CTRL,soft_reset_gt,value);
}
void ScuMemProxy::assignCTRL_ms_sync( Scu_reg value )
{
	mem_assign_field(CTRL,ms_sync,value);
}
void ScuMemProxy::assignCTRL_fsm_reset( Scu_reg value )
{
	mem_assign_field(CTRL,fsm_reset,value);
}

void ScuMemProxy::assignCTRL( Scu_reg value )
{
	mem_assign_field(CTRL,payload,value);
}


Scu_reg ScuMemProxy::getCTRL_sys_stop_state()
{
	return CTRL.sys_stop_state;
}
Scu_reg ScuMemProxy::getCTRL_cfg_fsm_run_stop_n()
{
	return CTRL.cfg_fsm_run_stop_n;
}
Scu_reg ScuMemProxy::getCTRL_run_single()
{
	return CTRL.run_single;
}
Scu_reg ScuMemProxy::getCTRL_mode_play_last()
{
	return CTRL.mode_play_last;
}
Scu_reg ScuMemProxy::getCTRL_scu_fsm_run()
{
	return CTRL.scu_fsm_run;
}
Scu_reg ScuMemProxy::getCTRL_force_trig()
{
	return CTRL.force_trig;
}
Scu_reg ScuMemProxy::getCTRL_wpu_plot_display_finish()
{
	return CTRL.wpu_plot_display_finish;
}
Scu_reg ScuMemProxy::getCTRL_soft_reset_gt()
{
	return CTRL.soft_reset_gt;
}
Scu_reg ScuMemProxy::getCTRL_ms_sync()
{
	return CTRL.ms_sync;
}
Scu_reg ScuMemProxy::getCTRL_fsm_reset()
{
	return CTRL.fsm_reset;
}

Scu_reg ScuMemProxy::getCTRL()
{
	return CTRL.payload;
}


void ScuMemProxy::assignMODE_cfg_mode_scan( Scu_reg value )
{
	mem_assign_field(MODE,cfg_mode_scan,value);
}
void ScuMemProxy::assignMODE_sys_mode_scan_trace( Scu_reg value )
{
	mem_assign_field(MODE,sys_mode_scan_trace,value);
}
void ScuMemProxy::assignMODE_sys_mode_scan_zoom( Scu_reg value )
{
	mem_assign_field(MODE,sys_mode_scan_zoom,value);
}
void ScuMemProxy::assignMODE_mode_scan_en( Scu_reg value )
{
	mem_assign_field(MODE,mode_scan_en,value);
}
void ScuMemProxy::assignMODE_search_en_zoom( Scu_reg value )
{
	mem_assign_field(MODE,search_en_zoom,value);
}
void ScuMemProxy::assignMODE_search_en_ch( Scu_reg value )
{
	mem_assign_field(MODE,search_en_ch,value);
}
void ScuMemProxy::assignMODE_search_en_la( Scu_reg value )
{
	mem_assign_field(MODE,search_en_la,value);
}
void ScuMemProxy::assignMODE_mode_import_type( Scu_reg value )
{
	mem_assign_field(MODE,mode_import_type,value);
}
void ScuMemProxy::assignMODE_mode_export( Scu_reg value )
{
	mem_assign_field(MODE,mode_export,value);
}
void ScuMemProxy::assignMODE_mode_import_play( Scu_reg value )
{
	mem_assign_field(MODE,mode_import_play,value);
}
void ScuMemProxy::assignMODE_mode_import_rec( Scu_reg value )
{
	mem_assign_field(MODE,mode_import_rec,value);
}
void ScuMemProxy::assignMODE_measure_en_zoom( Scu_reg value )
{
	mem_assign_field(MODE,measure_en_zoom,value);
}
void ScuMemProxy::assignMODE_measure_en_ch( Scu_reg value )
{
	mem_assign_field(MODE,measure_en_ch,value);
}
void ScuMemProxy::assignMODE_measure_en_la( Scu_reg value )
{
	mem_assign_field(MODE,measure_en_la,value);
}
void ScuMemProxy::assignMODE_wave_ch_en( Scu_reg value )
{
	mem_assign_field(MODE,wave_ch_en,value);
}
void ScuMemProxy::assignMODE_wave_la_en( Scu_reg value )
{
	mem_assign_field(MODE,wave_la_en,value);
}
void ScuMemProxy::assignMODE_zoom_en( Scu_reg value )
{
	mem_assign_field(MODE,zoom_en,value);
}
void ScuMemProxy::assignMODE_mask_err_stop_en( Scu_reg value )
{
	mem_assign_field(MODE,mask_err_stop_en,value);
}
void ScuMemProxy::assignMODE_mask_save_fail( Scu_reg value )
{
	mem_assign_field(MODE,mask_save_fail,value);
}
void ScuMemProxy::assignMODE_mask_save_pass( Scu_reg value )
{
	mem_assign_field(MODE,mask_save_pass,value);
}
void ScuMemProxy::assignMODE_mask_pf_en( Scu_reg value )
{
	mem_assign_field(MODE,mask_pf_en,value);
}
void ScuMemProxy::assignMODE_zone_trig_en( Scu_reg value )
{
	mem_assign_field(MODE,zone_trig_en,value);
}
void ScuMemProxy::assignMODE_mode_roll_en( Scu_reg value )
{
	mem_assign_field(MODE,mode_roll_en,value);
}
void ScuMemProxy::assignMODE_mode_fast_ref( Scu_reg value )
{
	mem_assign_field(MODE,mode_fast_ref,value);
}
void ScuMemProxy::assignMODE_auto_trig( Scu_reg value )
{
	mem_assign_field(MODE,auto_trig,value);
}
void ScuMemProxy::assignMODE_interleave_20G( Scu_reg value )
{
	mem_assign_field(MODE,interleave_20G,value);
}
void ScuMemProxy::assignMODE_en_20G( Scu_reg value )
{
	mem_assign_field(MODE,en_20G,value);
}

void ScuMemProxy::assignMODE( Scu_reg value )
{
	mem_assign_field(MODE,payload,value);
}


Scu_reg ScuMemProxy::getMODE_cfg_mode_scan()
{
	return MODE.cfg_mode_scan;
}
Scu_reg ScuMemProxy::getMODE_sys_mode_scan_trace()
{
	return MODE.sys_mode_scan_trace;
}
Scu_reg ScuMemProxy::getMODE_sys_mode_scan_zoom()
{
	return MODE.sys_mode_scan_zoom;
}
Scu_reg ScuMemProxy::getMODE_mode_scan_en()
{
	return MODE.mode_scan_en;
}
Scu_reg ScuMemProxy::getMODE_search_en_zoom()
{
	return MODE.search_en_zoom;
}
Scu_reg ScuMemProxy::getMODE_search_en_ch()
{
	return MODE.search_en_ch;
}
Scu_reg ScuMemProxy::getMODE_search_en_la()
{
	return MODE.search_en_la;
}
Scu_reg ScuMemProxy::getMODE_mode_import_type()
{
	return MODE.mode_import_type;
}
Scu_reg ScuMemProxy::getMODE_mode_export()
{
	return MODE.mode_export;
}
Scu_reg ScuMemProxy::getMODE_mode_import_play()
{
	return MODE.mode_import_play;
}
Scu_reg ScuMemProxy::getMODE_mode_import_rec()
{
	return MODE.mode_import_rec;
}
Scu_reg ScuMemProxy::getMODE_measure_en_zoom()
{
	return MODE.measure_en_zoom;
}
Scu_reg ScuMemProxy::getMODE_measure_en_ch()
{
	return MODE.measure_en_ch;
}
Scu_reg ScuMemProxy::getMODE_measure_en_la()
{
	return MODE.measure_en_la;
}
Scu_reg ScuMemProxy::getMODE_wave_ch_en()
{
	return MODE.wave_ch_en;
}
Scu_reg ScuMemProxy::getMODE_wave_la_en()
{
	return MODE.wave_la_en;
}
Scu_reg ScuMemProxy::getMODE_zoom_en()
{
	return MODE.zoom_en;
}
Scu_reg ScuMemProxy::getMODE_mask_err_stop_en()
{
	return MODE.mask_err_stop_en;
}
Scu_reg ScuMemProxy::getMODE_mask_save_fail()
{
	return MODE.mask_save_fail;
}
Scu_reg ScuMemProxy::getMODE_mask_save_pass()
{
	return MODE.mask_save_pass;
}
Scu_reg ScuMemProxy::getMODE_mask_pf_en()
{
	return MODE.mask_pf_en;
}
Scu_reg ScuMemProxy::getMODE_zone_trig_en()
{
	return MODE.zone_trig_en;
}
Scu_reg ScuMemProxy::getMODE_mode_roll_en()
{
	return MODE.mode_roll_en;
}
Scu_reg ScuMemProxy::getMODE_mode_fast_ref()
{
	return MODE.mode_fast_ref;
}
Scu_reg ScuMemProxy::getMODE_auto_trig()
{
	return MODE.auto_trig;
}
Scu_reg ScuMemProxy::getMODE_interleave_20G()
{
	return MODE.interleave_20G;
}
Scu_reg ScuMemProxy::getMODE_en_20G()
{
	return MODE.en_20G;
}

Scu_reg ScuMemProxy::getMODE()
{
	return MODE.payload;
}


void ScuMemProxy::assignMODE_10G_chn_10G( Scu_reg value )
{
	mem_assign_field(MODE_10G,chn_10G,value);
}
void ScuMemProxy::assignMODE_10G_cfg_chn( Scu_reg value )
{
	mem_assign_field(MODE_10G,cfg_chn,value);
}

void ScuMemProxy::assignMODE_10G( Scu_reg value )
{
	mem_assign_field(MODE_10G,payload,value);
}


Scu_reg ScuMemProxy::getMODE_10G_chn_10G()
{
	return MODE_10G.chn_10G;
}
Scu_reg ScuMemProxy::getMODE_10G_cfg_chn()
{
	return MODE_10G.cfg_chn;
}

Scu_reg ScuMemProxy::getMODE_10G()
{
	return MODE_10G.payload;
}


void ScuMemProxy::assignPRE_SA( Scu_reg value )
{
	mem_assign(PRE_SA,value);
}


Scu_reg ScuMemProxy::getPRE_SA()
{
	return PRE_SA;
}


void ScuMemProxy::assignPOS_SA( Scu_reg value )
{
	mem_assign(POS_SA,value);
}


Scu_reg ScuMemProxy::getPOS_SA()
{
	return POS_SA;
}


void ScuMemProxy::assignSCAN_TRIG_POSITION_position( Scu_reg value )
{
	mem_assign_field(SCAN_TRIG_POSITION,position,value);
}
void ScuMemProxy::assignSCAN_TRIG_POSITION_trig_left_side( Scu_reg value )
{
	mem_assign_field(SCAN_TRIG_POSITION,trig_left_side,value);
}

void ScuMemProxy::assignSCAN_TRIG_POSITION( Scu_reg value )
{
	mem_assign_field(SCAN_TRIG_POSITION,payload,value);
}


Scu_reg ScuMemProxy::getSCAN_TRIG_POSITION_position()
{
	return SCAN_TRIG_POSITION.position;
}
Scu_reg ScuMemProxy::getSCAN_TRIG_POSITION_trig_left_side()
{
	return SCAN_TRIG_POSITION.trig_left_side;
}

Scu_reg ScuMemProxy::getSCAN_TRIG_POSITION()
{
	return SCAN_TRIG_POSITION.payload;
}


void ScuMemProxy::assignAUTO_TRIG_TIMEOUT( Scu_reg value )
{
	mem_assign(AUTO_TRIG_TIMEOUT,value);
}


Scu_reg ScuMemProxy::getAUTO_TRIG_TIMEOUT()
{
	return AUTO_TRIG_TIMEOUT;
}


void ScuMemProxy::assignAUTO_TRIG_HOLD_OFF( Scu_reg value )
{
	mem_assign(AUTO_TRIG_HOLD_OFF,value);
}


Scu_reg ScuMemProxy::getAUTO_TRIG_HOLD_OFF()
{
	return AUTO_TRIG_HOLD_OFF;
}


void ScuMemProxy::assignFSM_DELAY( Scu_reg value )
{
	mem_assign(FSM_DELAY,value);
}


Scu_reg ScuMemProxy::getFSM_DELAY()
{
	return FSM_DELAY;
}


void ScuMemProxy::assignPLOT_DELAY( Scu_reg value )
{
	mem_assign(PLOT_DELAY,value);
}


Scu_reg ScuMemProxy::getPLOT_DELAY()
{
	return PLOT_DELAY;
}


void ScuMemProxy::assignFRAME_PLOT_NUM( Scu_reg value )
{
	mem_assign(FRAME_PLOT_NUM,value);
}


Scu_reg ScuMemProxy::getFRAME_PLOT_NUM()
{
	return FRAME_PLOT_NUM;
}


void ScuMemProxy::assignREC_PLAY_index_full( Scu_reg value )
{
	mem_assign_field(REC_PLAY,index_full,value);
}
void ScuMemProxy::assignREC_PLAY_loop_playback( Scu_reg value )
{
	mem_assign_field(REC_PLAY,loop_playback,value);
}
void ScuMemProxy::assignREC_PLAY_index_load( Scu_reg value )
{
	mem_assign_field(REC_PLAY,index_load,value);
}
void ScuMemProxy::assignREC_PLAY_index_inc( Scu_reg value )
{
	mem_assign_field(REC_PLAY,index_inc,value);
}
void ScuMemProxy::assignREC_PLAY_mode_play( Scu_reg value )
{
	mem_assign_field(REC_PLAY,mode_play,value);
}
void ScuMemProxy::assignREC_PLAY_mode_rec( Scu_reg value )
{
	mem_assign_field(REC_PLAY,mode_rec,value);
}

void ScuMemProxy::assignREC_PLAY( Scu_reg value )
{
	mem_assign_field(REC_PLAY,payload,value);
}


Scu_reg ScuMemProxy::getREC_PLAY_index_full()
{
	return REC_PLAY.index_full;
}
Scu_reg ScuMemProxy::getREC_PLAY_loop_playback()
{
	return REC_PLAY.loop_playback;
}
Scu_reg ScuMemProxy::getREC_PLAY_index_load()
{
	return REC_PLAY.index_load;
}
Scu_reg ScuMemProxy::getREC_PLAY_index_inc()
{
	return REC_PLAY.index_inc;
}
Scu_reg ScuMemProxy::getREC_PLAY_mode_play()
{
	return REC_PLAY.mode_play;
}
Scu_reg ScuMemProxy::getREC_PLAY_mode_rec()
{
	return REC_PLAY.mode_rec;
}

Scu_reg ScuMemProxy::getREC_PLAY()
{
	return REC_PLAY.payload;
}


void ScuMemProxy::assignWAVE_INFO_wave_plot_frame_cnt( Scu_reg value )
{
	mem_assign_field(WAVE_INFO,wave_plot_frame_cnt,value);
}
void ScuMemProxy::assignWAVE_INFO_wave_play_vld( Scu_reg value )
{
	mem_assign_field(WAVE_INFO,wave_play_vld,value);
}

void ScuMemProxy::assignWAVE_INFO( Scu_reg value )
{
	mem_assign_field(WAVE_INFO,payload,value);
}


Scu_reg ScuMemProxy::getWAVE_INFO_wave_plot_frame_cnt()
{
	return WAVE_INFO.wave_plot_frame_cnt;
}
Scu_reg ScuMemProxy::getWAVE_INFO_wave_play_vld()
{
	return WAVE_INFO.wave_play_vld;
}

Scu_reg ScuMemProxy::getWAVE_INFO()
{
	return WAVE_INFO.payload;
}


void ScuMemProxy::assignINDEX_CUR_index( Scu_reg value )
{
	mem_assign_field(INDEX_CUR,index,value);
}

void ScuMemProxy::assignINDEX_CUR( Scu_reg value )
{
	mem_assign_field(INDEX_CUR,payload,value);
}


Scu_reg ScuMemProxy::getINDEX_CUR_index()
{
	return INDEX_CUR.index;
}

Scu_reg ScuMemProxy::getINDEX_CUR()
{
	return INDEX_CUR.payload;
}


void ScuMemProxy::assignINDEX_BASE_index( Scu_reg value )
{
	mem_assign_field(INDEX_BASE,index,value);
}

void ScuMemProxy::assignINDEX_BASE( Scu_reg value )
{
	mem_assign_field(INDEX_BASE,payload,value);
}


Scu_reg ScuMemProxy::getINDEX_BASE_index()
{
	return INDEX_BASE.index;
}

Scu_reg ScuMemProxy::getINDEX_BASE()
{
	return INDEX_BASE.payload;
}


void ScuMemProxy::assignINDEX_UPPER_index( Scu_reg value )
{
	mem_assign_field(INDEX_UPPER,index,value);
}

void ScuMemProxy::assignINDEX_UPPER( Scu_reg value )
{
	mem_assign_field(INDEX_UPPER,payload,value);
}


Scu_reg ScuMemProxy::getINDEX_UPPER_index()
{
	return INDEX_UPPER.index;
}

Scu_reg ScuMemProxy::getINDEX_UPPER()
{
	return INDEX_UPPER.payload;
}


void ScuMemProxy::assignTRACE_trace_once_req( Scu_reg value )
{
	mem_assign_field(TRACE,trace_once_req,value);
}
void ScuMemProxy::assignTRACE_measure_once_req( Scu_reg value )
{
	mem_assign_field(TRACE,measure_once_req,value);
}
void ScuMemProxy::assignTRACE_search_once_req( Scu_reg value )
{
	mem_assign_field(TRACE,search_once_req,value);
}
void ScuMemProxy::assignTRACE_eye_once_req( Scu_reg value )
{
	mem_assign_field(TRACE,eye_once_req,value);
}
void ScuMemProxy::assignTRACE_wave_bypass_wpu( Scu_reg value )
{
	mem_assign_field(TRACE,wave_bypass_wpu,value);
}
void ScuMemProxy::assignTRACE_wave_bypass_trace( Scu_reg value )
{
	mem_assign_field(TRACE,wave_bypass_trace,value);
}
void ScuMemProxy::assignTRACE_avg_clr( Scu_reg value )
{
	mem_assign_field(TRACE,avg_clr,value);
}

void ScuMemProxy::assignTRACE( Scu_reg value )
{
	mem_assign_field(TRACE,payload,value);
}


Scu_reg ScuMemProxy::getTRACE_trace_once_req()
{
	return TRACE.trace_once_req;
}
Scu_reg ScuMemProxy::getTRACE_measure_once_req()
{
	return TRACE.measure_once_req;
}
Scu_reg ScuMemProxy::getTRACE_search_once_req()
{
	return TRACE.search_once_req;
}
Scu_reg ScuMemProxy::getTRACE_eye_once_req()
{
	return TRACE.eye_once_req;
}
Scu_reg ScuMemProxy::getTRACE_wave_bypass_wpu()
{
	return TRACE.wave_bypass_wpu;
}
Scu_reg ScuMemProxy::getTRACE_wave_bypass_trace()
{
	return TRACE.wave_bypass_trace;
}
Scu_reg ScuMemProxy::getTRACE_avg_clr()
{
	return TRACE.avg_clr;
}

Scu_reg ScuMemProxy::getTRACE()
{
	return TRACE.payload;
}


void ScuMemProxy::assignTRIG_DLY( Scu_reg value )
{
	mem_assign(TRIG_DLY,value);
}


Scu_reg ScuMemProxy::getTRIG_DLY()
{
	return TRIG_DLY;
}


void ScuMemProxy::assignPOS_SA_LAST_VIEW( Scu_reg value )
{
	mem_assign(POS_SA_LAST_VIEW,value);
}


Scu_reg ScuMemProxy::getPOS_SA_LAST_VIEW()
{
	return POS_SA_LAST_VIEW;
}


void ScuMemProxy::assignMASK_FRM_NUM_L( Scu_reg value )
{
	mem_assign(MASK_FRM_NUM_L,value);
}


Scu_reg ScuMemProxy::getMASK_FRM_NUM_L()
{
	return MASK_FRM_NUM_L;
}


void ScuMemProxy::assignMASK_FRM_NUM_H_frm_num_h( Scu_reg value )
{
	mem_assign_field(MASK_FRM_NUM_H,frm_num_h,value);
}
void ScuMemProxy::assignMASK_FRM_NUM_H_frm_num_en( Scu_reg value )
{
	mem_assign_field(MASK_FRM_NUM_H,frm_num_en,value);
}

void ScuMemProxy::assignMASK_FRM_NUM_H( Scu_reg value )
{
	mem_assign_field(MASK_FRM_NUM_H,payload,value);
}


Scu_reg ScuMemProxy::getMASK_FRM_NUM_H_frm_num_h()
{
	return MASK_FRM_NUM_H.frm_num_h;
}
Scu_reg ScuMemProxy::getMASK_FRM_NUM_H_frm_num_en()
{
	return MASK_FRM_NUM_H.frm_num_en;
}

Scu_reg ScuMemProxy::getMASK_FRM_NUM_H()
{
	return MASK_FRM_NUM_H.payload;
}


void ScuMemProxy::assignMASK_TIMEOUT_timeout( Scu_reg value )
{
	mem_assign_field(MASK_TIMEOUT,timeout,value);
}
void ScuMemProxy::assignMASK_TIMEOUT_timeout_en( Scu_reg value )
{
	mem_assign_field(MASK_TIMEOUT,timeout_en,value);
}

void ScuMemProxy::assignMASK_TIMEOUT( Scu_reg value )
{
	mem_assign_field(MASK_TIMEOUT,payload,value);
}


Scu_reg ScuMemProxy::getMASK_TIMEOUT_timeout()
{
	return MASK_TIMEOUT.timeout;
}
Scu_reg ScuMemProxy::getMASK_TIMEOUT_timeout_en()
{
	return MASK_TIMEOUT.timeout_en;
}

Scu_reg ScuMemProxy::getMASK_TIMEOUT()
{
	return MASK_TIMEOUT.payload;
}


void ScuMemProxy::assignSTATE_DISPLAY_scu_fsm( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,scu_fsm,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,sys_pre_sa_done,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_sys_stop_state( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,sys_stop_state,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,disp_sys_trig_d,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_rec_play_stop_state( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,rec_play_stop_state,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_mask_stop_state( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,mask_stop_state,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_stop_stata_clr( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,stop_stata_clr,value);
}
void ScuMemProxy::assignSTATE_DISPLAY_disp_trig_clr( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,disp_trig_clr,value);
}

void ScuMemProxy::assignSTATE_DISPLAY( Scu_reg value )
{
	mem_assign_field(STATE_DISPLAY,payload,value);
}


Scu_reg ScuMemProxy::getSTATE_DISPLAY_scu_fsm()
{
	return STATE_DISPLAY.scu_fsm;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_sys_pre_sa_done()
{
	return STATE_DISPLAY.sys_pre_sa_done;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_sys_stop_state()
{
	return STATE_DISPLAY.sys_stop_state;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_disp_tpu_scu_trig()
{
	return STATE_DISPLAY.disp_tpu_scu_trig;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_disp_sys_trig_d()
{
	return STATE_DISPLAY.disp_sys_trig_d;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_rec_play_stop_state()
{
	return STATE_DISPLAY.rec_play_stop_state;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_mask_stop_state()
{
	return STATE_DISPLAY.mask_stop_state;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_stop_stata_clr()
{
	return STATE_DISPLAY.stop_stata_clr;
}
Scu_reg ScuMemProxy::getSTATE_DISPLAY_disp_trig_clr()
{
	return STATE_DISPLAY.disp_trig_clr;
}

Scu_reg ScuMemProxy::getSTATE_DISPLAY()
{
	return STATE_DISPLAY.payload;
}


Scu_reg ScuMemProxy::getMASK_TIMER()
{
	return MASK_TIMER;
}


void ScuMemProxy::assignCONFIG_ID( Scu_reg value )
{
	mem_assign(CONFIG_ID,value);
}


Scu_reg ScuMemProxy::getCONFIG_ID()
{
	return CONFIG_ID;
}


void ScuMemProxy::assignDEUBG_wave_sa_ctrl( Scu_reg value )
{
	mem_assign_field(DEUBG,wave_sa_ctrl,value);
}
void ScuMemProxy::assignDEUBG_sys_auto_trig_act( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_auto_trig_act,value);
}
void ScuMemProxy::assignDEUBG_sys_auto_trig_en( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_auto_trig_en,value);
}
void ScuMemProxy::assignDEUBG_sys_fine_trig_req( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_fine_trig_req,value);
}
void ScuMemProxy::assignDEUBG_sys_meas_only( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_meas_only,value);
}
void ScuMemProxy::assignDEUBG_sys_meas_en( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_meas_en,value);
}
void ScuMemProxy::assignDEUBG_sys_meas_type( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_meas_type,value);
}
void ScuMemProxy::assignDEUBG_spu_tx_roll_null( Scu_reg value )
{
	mem_assign_field(DEUBG,spu_tx_roll_null,value);
}
void ScuMemProxy::assignDEUBG_measure_once_done_hold( Scu_reg value )
{
	mem_assign_field(DEUBG,measure_once_done_hold,value);
}
void ScuMemProxy::assignDEUBG_measure_finish_done_hold( Scu_reg value )
{
	mem_assign_field(DEUBG,measure_finish_done_hold,value);
}
void ScuMemProxy::assignDEUBG_sys_mask_en( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_mask_en,value);
}
void ScuMemProxy::assignDEUBG_mask_pf_result_vld_hold( Scu_reg value )
{
	mem_assign_field(DEUBG,mask_pf_result_vld_hold,value);
}
void ScuMemProxy::assignDEUBG_mask_pf_result_cross_hold( Scu_reg value )
{
	mem_assign_field(DEUBG,mask_pf_result_cross_hold,value);
}
void ScuMemProxy::assignDEUBG_mask_pf_timeout( Scu_reg value )
{
	mem_assign_field(DEUBG,mask_pf_timeout,value);
}
void ScuMemProxy::assignDEUBG_mask_frm_overflow( Scu_reg value )
{
	mem_assign_field(DEUBG,mask_frm_overflow,value);
}
void ScuMemProxy::assignDEUBG_measure_once_buf( Scu_reg value )
{
	mem_assign_field(DEUBG,measure_once_buf,value);
}
void ScuMemProxy::assignDEUBG_zone_result_vld_hold( Scu_reg value )
{
	mem_assign_field(DEUBG,zone_result_vld_hold,value);
}
void ScuMemProxy::assignDEUBG_zone_result_trig_hold( Scu_reg value )
{
	mem_assign_field(DEUBG,zone_result_trig_hold,value);
}
void ScuMemProxy::assignDEUBG_wave_proc_la( Scu_reg value )
{
	mem_assign_field(DEUBG,wave_proc_la,value);
}
void ScuMemProxy::assignDEUBG_sys_zoom( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_zoom,value);
}
void ScuMemProxy::assignDEUBG_sys_fast_ref( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_fast_ref,value);
}
void ScuMemProxy::assignDEUBG_spu_tx_done( Scu_reg value )
{
	mem_assign_field(DEUBG,spu_tx_done,value);
}
void ScuMemProxy::assignDEUBG_sys_avg_en( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_avg_en,value);
}
void ScuMemProxy::assignDEUBG_sys_trace( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_trace,value);
}
void ScuMemProxy::assignDEUBG_sys_pos_sa_last_frm( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_pos_sa_last_frm,value);
}
void ScuMemProxy::assignDEUBG_sys_pos_sa_view( Scu_reg value )
{
	mem_assign_field(DEUBG,sys_pos_sa_view,value);
}
void ScuMemProxy::assignDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value )
{
	mem_assign_field(DEUBG,spu_wav_sa_rd_tx_done,value);
}
void ScuMemProxy::assignDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value )
{
	mem_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,value);
}
void ScuMemProxy::assignDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value )
{
	mem_assign_field(DEUBG,scan_roll_pos_last_frm_buf,value);
}

void ScuMemProxy::assignDEUBG( Scu_reg value )
{
	mem_assign_field(DEUBG,payload,value);
}


Scu_reg ScuMemProxy::getDEUBG_wave_sa_ctrl()
{
	return DEUBG.wave_sa_ctrl;
}
Scu_reg ScuMemProxy::getDEUBG_sys_auto_trig_act()
{
	return DEUBG.sys_auto_trig_act;
}
Scu_reg ScuMemProxy::getDEUBG_sys_auto_trig_en()
{
	return DEUBG.sys_auto_trig_en;
}
Scu_reg ScuMemProxy::getDEUBG_sys_fine_trig_req()
{
	return DEUBG.sys_fine_trig_req;
}
Scu_reg ScuMemProxy::getDEUBG_sys_meas_only()
{
	return DEUBG.sys_meas_only;
}
Scu_reg ScuMemProxy::getDEUBG_sys_meas_en()
{
	return DEUBG.sys_meas_en;
}
Scu_reg ScuMemProxy::getDEUBG_sys_meas_type()
{
	return DEUBG.sys_meas_type;
}
Scu_reg ScuMemProxy::getDEUBG_spu_tx_roll_null()
{
	return DEUBG.spu_tx_roll_null;
}
Scu_reg ScuMemProxy::getDEUBG_measure_once_done_hold()
{
	return DEUBG.measure_once_done_hold;
}
Scu_reg ScuMemProxy::getDEUBG_measure_finish_done_hold()
{
	return DEUBG.measure_finish_done_hold;
}
Scu_reg ScuMemProxy::getDEUBG_sys_mask_en()
{
	return DEUBG.sys_mask_en;
}
Scu_reg ScuMemProxy::getDEUBG_mask_pf_result_vld_hold()
{
	return DEUBG.mask_pf_result_vld_hold;
}
Scu_reg ScuMemProxy::getDEUBG_mask_pf_result_cross_hold()
{
	return DEUBG.mask_pf_result_cross_hold;
}
Scu_reg ScuMemProxy::getDEUBG_mask_pf_timeout()
{
	return DEUBG.mask_pf_timeout;
}
Scu_reg ScuMemProxy::getDEUBG_mask_frm_overflow()
{
	return DEUBG.mask_frm_overflow;
}
Scu_reg ScuMemProxy::getDEUBG_measure_once_buf()
{
	return DEUBG.measure_once_buf;
}
Scu_reg ScuMemProxy::getDEUBG_zone_result_vld_hold()
{
	return DEUBG.zone_result_vld_hold;
}
Scu_reg ScuMemProxy::getDEUBG_zone_result_trig_hold()
{
	return DEUBG.zone_result_trig_hold;
}
Scu_reg ScuMemProxy::getDEUBG_wave_proc_la()
{
	return DEUBG.wave_proc_la;
}
Scu_reg ScuMemProxy::getDEUBG_sys_zoom()
{
	return DEUBG.sys_zoom;
}
Scu_reg ScuMemProxy::getDEUBG_sys_fast_ref()
{
	return DEUBG.sys_fast_ref;
}
Scu_reg ScuMemProxy::getDEUBG_spu_tx_done()
{
	return DEUBG.spu_tx_done;
}
Scu_reg ScuMemProxy::getDEUBG_sys_avg_en()
{
	return DEUBG.sys_avg_en;
}
Scu_reg ScuMemProxy::getDEUBG_sys_trace()
{
	return DEUBG.sys_trace;
}
Scu_reg ScuMemProxy::getDEUBG_sys_pos_sa_last_frm()
{
	return DEUBG.sys_pos_sa_last_frm;
}
Scu_reg ScuMemProxy::getDEUBG_sys_pos_sa_view()
{
	return DEUBG.sys_pos_sa_view;
}
Scu_reg ScuMemProxy::getDEUBG_spu_wav_sa_rd_tx_done()
{
	return DEUBG.spu_wav_sa_rd_tx_done;
}
Scu_reg ScuMemProxy::getDEUBG_scan_roll_pos_trig_frm_buf()
{
	return DEUBG.scan_roll_pos_trig_frm_buf;
}
Scu_reg ScuMemProxy::getDEUBG_scan_roll_pos_last_frm_buf()
{
	return DEUBG.scan_roll_pos_last_frm_buf;
}

Scu_reg ScuMemProxy::getDEUBG()
{
	return DEUBG.payload;
}


Scu_reg ScuMemProxy::getDEBUG_GT_receiver_gtu_state()
{
	return DEBUG_GT.receiver_gtu_state;
}

Scu_reg ScuMemProxy::getDEBUG_GT()
{
	return DEBUG_GT.payload;
}


void ScuMemProxy::assignDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value )
{
	mem_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,value);
}

void ScuMemProxy::assignDEBUG_PLOT_DONE( Scu_reg value )
{
	mem_assign_field(DEBUG_PLOT_DONE,payload,value);
}


Scu_reg ScuMemProxy::getDEBUG_PLOT_DONE_wpu_plot_done_dly()
{
	return DEBUG_PLOT_DONE.wpu_plot_done_dly;
}

Scu_reg ScuMemProxy::getDEBUG_PLOT_DONE()
{
	return DEBUG_PLOT_DONE.payload;
}


void ScuMemProxy::assignFPGA_SYNC_sync_cnt( Scu_reg value )
{
	mem_assign_field(FPGA_SYNC,sync_cnt,value);
}
void ScuMemProxy::assignFPGA_SYNC_debug_scu_reset( Scu_reg value )
{
	mem_assign_field(FPGA_SYNC,debug_scu_reset,value);
}
void ScuMemProxy::assignFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value )
{
	mem_assign_field(FPGA_SYNC,debug_sys_rst_cnt,value);
}

void ScuMemProxy::assignFPGA_SYNC( Scu_reg value )
{
	mem_assign_field(FPGA_SYNC,payload,value);
}


Scu_reg ScuMemProxy::getFPGA_SYNC_sync_cnt()
{
	return FPGA_SYNC.sync_cnt;
}
Scu_reg ScuMemProxy::getFPGA_SYNC_debug_scu_reset()
{
	return FPGA_SYNC.debug_scu_reset;
}
Scu_reg ScuMemProxy::getFPGA_SYNC_debug_sys_rst_cnt()
{
	return FPGA_SYNC.debug_sys_rst_cnt;
}

Scu_reg ScuMemProxy::getFPGA_SYNC()
{
	return FPGA_SYNC.payload;
}


Scu_reg ScuMemProxy::getWPU_ID_sys_wpu_plot_id()
{
	return WPU_ID.sys_wpu_plot_id;
}
Scu_reg ScuMemProxy::getWPU_ID_wpu_plot_done_id()
{
	return WPU_ID.wpu_plot_done_id;
}
Scu_reg ScuMemProxy::getWPU_ID_wpu_display_done_id()
{
	return WPU_ID.wpu_display_done_id;
}

Scu_reg ScuMemProxy::getWPU_ID()
{
	return WPU_ID.payload;
}


void ScuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,value);
}
void ScuMemProxy::assignDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,value);
}

void ScuMemProxy::assignDEBUG_WPU_PLOT( Scu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,payload,value);
}


Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_display_finish()
{
	return DEBUG_WPU_PLOT.wpu_plot_display_finish;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_wpu_fast_done_buf()
{
	return DEBUG_WPU_PLOT.wpu_fast_done_buf;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_wpu_display_done_buf()
{
	return DEBUG_WPU_PLOT.wpu_display_done_buf;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_display_id_equ()
{
	return DEBUG_WPU_PLOT.wpu_plot_display_id_equ;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_plot_last()
{
	return DEBUG_WPU_PLOT.scu_wpu_plot_last;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_scu_wave_plot_req()
{
	return DEBUG_WPU_PLOT.scu_wave_plot_req;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_start()
{
	return DEBUG_WPU_PLOT.scu_wpu_start;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_sys_wpu_plot()
{
	return DEBUG_WPU_PLOT.sys_wpu_plot;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_done_buf()
{
	return DEBUG_WPU_PLOT.wpu_plot_done_buf;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_plot_en()
{
	return DEBUG_WPU_PLOT.scu_wpu_plot_en;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf()
{
	return DEBUG_WPU_PLOT.wpu_plot_scan_finish_buf;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf()
{
	return DEBUG_WPU_PLOT.wpu_plot_scan_zoom_finish_buf;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_stop_plot_en()
{
	return DEBUG_WPU_PLOT.scu_wpu_stop_plot_en;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld()
{
	return DEBUG_WPU_PLOT.scu_wpu_stop_plot_vld;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_cfg_wpu_flush()
{
	return DEBUG_WPU_PLOT.cfg_wpu_flush;
}
Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT_cfg_stop_plot()
{
	return DEBUG_WPU_PLOT.cfg_stop_plot;
}

Scu_reg ScuMemProxy::getDEBUG_WPU_PLOT()
{
	return DEBUG_WPU_PLOT.payload;
}


void ScuMemProxy::assignDEBUG_GT_CRC_fail_sum( Scu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,fail_sum,value);
}
void ScuMemProxy::assignDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,value);
}
void ScuMemProxy::assignDEBUG_GT_CRC_crc_valid_i( Scu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,crc_valid_i,value);
}

void ScuMemProxy::assignDEBUG_GT_CRC( Scu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,payload,value);
}


Scu_reg ScuMemProxy::getDEBUG_GT_CRC_fail_sum()
{
	return DEBUG_GT_CRC.fail_sum;
}
Scu_reg ScuMemProxy::getDEBUG_GT_CRC_crc_pass_fail_n_i()
{
	return DEBUG_GT_CRC.crc_pass_fail_n_i;
}
Scu_reg ScuMemProxy::getDEBUG_GT_CRC_crc_valid_i()
{
	return DEBUG_GT_CRC.crc_valid_i;
}

Scu_reg ScuMemProxy::getDEBUG_GT_CRC()
{
	return DEBUG_GT_CRC.payload;
}



void ScuMem::_loadOtp()
{
	VERSION=0x0;
	SCU_STATE.payload=0x0;
		SCU_STATE.scu_fsm=0x0;
		SCU_STATE.scu_sa_fsm=0x0;
		SCU_STATE.sys_stop=0x0;
		SCU_STATE.scu_stop=0x0;

		SCU_STATE.scu_loop=0x0;
		SCU_STATE.sys_force_stop=0x0;
		SCU_STATE.process_sa=0x0;
		SCU_STATE.process_sa_done=0x0;

		SCU_STATE.sys_sa_en=0x0;
		SCU_STATE.sys_pre_sa_done=0x0;
		SCU_STATE.sys_pos_sa_done=0x0;
		SCU_STATE.spu_wave_mem_wr_done_hold=0x0;

		SCU_STATE.sys_trig_d=0x0;
		SCU_STATE.tpu_scu_trig=0x0;
		SCU_STATE.fine_trig_done_buf=0x0;
		SCU_STATE.avg_proc_done=0x0;

		SCU_STATE.sys_avg_proc=0x0;
		SCU_STATE.sys_avg_exp=0x0;
		SCU_STATE.sys_wave_rd_en=0x0;
		SCU_STATE.sys_wave_proc_en=0x0;

		SCU_STATE.wave_mem_rd_done_hold=0x0;
		SCU_STATE.spu_tx_done_hold=0x0;
		SCU_STATE.gt_wave_tx_done=0x0;
		SCU_STATE.spu_wav_rd_meas_once_tx_done=0x0;

		SCU_STATE.task_wave_tx_done_LA=0x0;
		SCU_STATE.task_wave_tx_done_CH=0x0;
		SCU_STATE.spu_proc_done=0x0;
		SCU_STATE.scan_roll_sa_done_mem_tx_null=0x0;

	CTRL.payload=0x0;
		CTRL.sys_stop_state=0x0;
		CTRL.cfg_fsm_run_stop_n=0x0;
		CTRL.run_single=0x0;
		CTRL.mode_play_last=0x0;

		CTRL.scu_fsm_run=0x0;
		CTRL.force_trig=0x0;
		CTRL.wpu_plot_display_finish=0x0;
		CTRL.soft_reset_gt=0x0;

		CTRL.ms_sync=0x0;
		CTRL.fsm_reset=0x0;

	MODE.payload=0x0;
		MODE.cfg_mode_scan=0x0;
		MODE.sys_mode_scan_trace=0x0;
		MODE.sys_mode_scan_zoom=0x0;
		MODE.mode_scan_en=0x0;

		MODE.search_en_zoom=0x0;
		MODE.search_en_ch=0x1;
		MODE.search_en_la=0x1;
		MODE.mode_import_type=0x0;

		MODE.mode_export=0x0;
		MODE.mode_import_play=0x0;
		MODE.mode_import_rec=0x0;
		MODE.measure_en_zoom=0x0;

		MODE.measure_en_ch=0x0;
		MODE.measure_en_la=0x0;
		MODE.wave_ch_en=0x1;
		MODE.wave_la_en=0x0;

		MODE.zoom_en=0x0;
		MODE.mask_err_stop_en=0x0;
		MODE.mask_save_fail=0x1;
		MODE.mask_save_pass=0x1;

		MODE.mask_pf_en=0x0;
		MODE.zone_trig_en=0x0;
		MODE.mode_roll_en=0x0;
		MODE.mode_fast_ref=0x0;

		MODE.auto_trig=0x1;
		MODE.interleave_20G=0x4;
		MODE.en_20G=0x0;


	MODE_10G.payload=0x0;
		MODE_10G.chn_10G=0xf;
		MODE_10G.cfg_chn=0x0;

	PRE_SA=0x1f4;
	POS_SA=0x1f4;
	SCAN_TRIG_POSITION.payload=0x0;
		SCAN_TRIG_POSITION.position=0x0;
		SCAN_TRIG_POSITION.trig_left_side=0x0;


	AUTO_TRIG_TIMEOUT=0x3e8;
	AUTO_TRIG_HOLD_OFF=0x3e8;
	FSM_DELAY=0x0;
	PLOT_DELAY=0xbebc20;

	FRAME_PLOT_NUM=0x3e8;
	REC_PLAY.payload=0x0;
		REC_PLAY.index_full=0x0;
		REC_PLAY.loop_playback=0x0;
		REC_PLAY.index_load=0x0;
		REC_PLAY.index_inc=0x1;

		REC_PLAY.mode_play=0x0;
		REC_PLAY.mode_rec=0x0;

	WAVE_INFO.payload=0x0;
		WAVE_INFO.wave_plot_frame_cnt=0x0;
		WAVE_INFO.wave_play_vld=0x0;

	INDEX_CUR.payload=0x0;
		INDEX_CUR.index=0x0;


	INDEX_BASE.payload=0x0;
		INDEX_BASE.index=0x0;

	INDEX_UPPER.payload=0x64;
		INDEX_UPPER.index=0x3e7;

	TRACE.payload=0x0;
		TRACE.trace_once_req=0x1;
		TRACE.measure_once_req=0x0;
		TRACE.search_once_req=0x0;
		TRACE.eye_once_req=0x0;

		TRACE.wave_bypass_wpu=0x0;
		TRACE.wave_bypass_trace=0x0;
		TRACE.avg_clr=0x0;

	TRIG_DLY=0x28;

	POS_SA_LAST_VIEW=0x79d38;
	MASK_FRM_NUM_L=0x3e8;
	MASK_FRM_NUM_H.payload=0x0;
		MASK_FRM_NUM_H.frm_num_h=0x0;
		MASK_FRM_NUM_H.frm_num_en=0x1;

	MASK_TIMEOUT.payload=0x0;
		MASK_TIMEOUT.timeout=0x3e8;
		MASK_TIMEOUT.timeout_en=0x0;


	STATE_DISPLAY.payload=0x0;
		STATE_DISPLAY.scu_fsm=0x0;
		STATE_DISPLAY.sys_pre_sa_done=0x0;
		STATE_DISPLAY.sys_stop_state=0x0;
		STATE_DISPLAY.disp_tpu_scu_trig=0x0;

		STATE_DISPLAY.disp_sys_trig_d=0x0;
		STATE_DISPLAY.rec_play_stop_state=0x0;
		STATE_DISPLAY.mask_stop_state=0x0;
		STATE_DISPLAY.stop_stata_clr=0x0;

		STATE_DISPLAY.disp_trig_clr=0x0;

	MASK_TIMER=0x0;
	CONFIG_ID=0x0;
	DEUBG.payload=0x0;
		DEUBG.wave_sa_ctrl=0x0;
		DEUBG.sys_auto_trig_act=0x0;
		DEUBG.sys_auto_trig_en=0x0;
		DEUBG.sys_fine_trig_req=0x0;

		DEUBG.sys_meas_only=0x0;
		DEUBG.sys_meas_en=0x0;
		DEUBG.sys_meas_type=0x0;
		DEUBG.spu_tx_roll_null=0x0;

		DEUBG.measure_once_done_hold=0x0;
		DEUBG.measure_finish_done_hold=0x0;
		DEUBG.sys_mask_en=0x0;
		DEUBG.mask_pf_result_vld_hold=0x0;

		DEUBG.mask_pf_result_cross_hold=0x0;
		DEUBG.mask_pf_timeout=0x0;
		DEUBG.mask_frm_overflow=0x0;
		DEUBG.measure_once_buf=0x0;

		DEUBG.zone_result_vld_hold=0x0;
		DEUBG.zone_result_trig_hold=0x0;
		DEUBG.wave_proc_la=0x0;
		DEUBG.sys_zoom=0x0;

		DEUBG.sys_fast_ref=0x0;
		DEUBG.spu_tx_done=0x0;
		DEUBG.sys_avg_en=0x0;
		DEUBG.sys_trace=0x0;

		DEUBG.sys_pos_sa_last_frm=0x0;
		DEUBG.sys_pos_sa_view=0x0;
		DEUBG.spu_wav_sa_rd_tx_done=0x0;
		DEUBG.scan_roll_pos_trig_frm_buf=0x0;

		DEUBG.scan_roll_pos_last_frm_buf=0x0;


	DEBUG_GT.payload=0x0;
		DEBUG_GT.receiver_gtu_state=0x0;

	DEBUG_PLOT_DONE.payload=0x0;
		DEBUG_PLOT_DONE.wpu_plot_done_dly=0x0;

	FPGA_SYNC.payload=0x0;
		FPGA_SYNC.sync_cnt=0x0;
		FPGA_SYNC.debug_scu_reset=0x0;
		FPGA_SYNC.debug_sys_rst_cnt=0x0;

	WPU_ID.payload=0x0;
		WPU_ID.sys_wpu_plot_id=0x0;
		WPU_ID.wpu_plot_done_id=0x0;
		WPU_ID.wpu_display_done_id=0x0;


	DEBUG_WPU_PLOT.payload=0x0;
		DEBUG_WPU_PLOT.wpu_plot_display_finish=0x0;
		DEBUG_WPU_PLOT.wpu_fast_done_buf=0x0;
		DEBUG_WPU_PLOT.wpu_display_done_buf=0x0;
		DEBUG_WPU_PLOT.wpu_plot_display_id_equ=0x0;

		DEBUG_WPU_PLOT.scu_wpu_plot_last=0x0;
		DEBUG_WPU_PLOT.scu_wave_plot_req=0x0;
		DEBUG_WPU_PLOT.scu_wpu_start=0x0;
		DEBUG_WPU_PLOT.sys_wpu_plot=0x0;

		DEBUG_WPU_PLOT.wpu_plot_done_buf=0x0;
		DEBUG_WPU_PLOT.scu_wpu_plot_en=0x0;
		DEBUG_WPU_PLOT.wpu_plot_scan_finish_buf=0x0;
		DEBUG_WPU_PLOT.wpu_plot_scan_zoom_finish_buf=0x0;

		DEBUG_WPU_PLOT.scu_wpu_stop_plot_en=0x0;
		DEBUG_WPU_PLOT.scu_wpu_stop_plot_vld=0x0;
		DEBUG_WPU_PLOT.cfg_wpu_flush=0x0;
		DEBUG_WPU_PLOT.cfg_stop_plot=0x0;

	DEBUG_GT_CRC.payload=0x0;
		DEBUG_GT_CRC.fail_sum=0x0;
		DEBUG_GT_CRC.crc_pass_fail_n_i=0x0;
		DEBUG_GT_CRC.crc_valid_i=0x0;

}

void ScuMem::_outOtp()
{
	outSCU_STATE();
	outCTRL();
	outMODE();

	outMODE_10G();
	outPRE_SA();
	outPOS_SA();
	outSCAN_TRIG_POSITION();

	outAUTO_TRIG_TIMEOUT();
	outAUTO_TRIG_HOLD_OFF();
	outFSM_DELAY();
	outPLOT_DELAY();

	outFRAME_PLOT_NUM();
	outREC_PLAY();
	outWAVE_INFO();
	outINDEX_CUR();

	outINDEX_BASE();
	outINDEX_UPPER();
	outTRACE();
	outTRIG_DLY();

	outPOS_SA_LAST_VIEW();
	outMASK_FRM_NUM_L();
	outMASK_FRM_NUM_H();
	outMASK_TIMEOUT();

	outSTATE_DISPLAY();
	outCONFIG_ID();
	outDEUBG();

	outDEBUG_PLOT_DONE();
	outFPGA_SYNC();

	outDEBUG_WPU_PLOT();
	outDEBUG_GT_CRC();
}
void ScuMem::push( ScuMemProxy *proxy )
{
	if ( SCU_STATE.payload != proxy->SCU_STATE.payload )
	{reg_assign_field(SCU_STATE,payload,proxy->SCU_STATE.payload);}

	if ( CTRL.payload != proxy->CTRL.payload )
	{reg_assign_field(CTRL,payload,proxy->CTRL.payload);}

	if ( MODE.payload != proxy->MODE.payload )
	{reg_assign_field(MODE,payload,proxy->MODE.payload);}

	if ( MODE_10G.payload != proxy->MODE_10G.payload )
	{reg_assign_field(MODE_10G,payload,proxy->MODE_10G.payload);}

	if ( PRE_SA != proxy->PRE_SA )
	{reg_assign(PRE_SA,proxy->PRE_SA);}

	if ( POS_SA != proxy->POS_SA )
	{reg_assign(POS_SA,proxy->POS_SA);}

	if ( SCAN_TRIG_POSITION.payload != proxy->SCAN_TRIG_POSITION.payload )
	{reg_assign_field(SCAN_TRIG_POSITION,payload,proxy->SCAN_TRIG_POSITION.payload);}

	if ( AUTO_TRIG_TIMEOUT != proxy->AUTO_TRIG_TIMEOUT )
	{reg_assign(AUTO_TRIG_TIMEOUT,proxy->AUTO_TRIG_TIMEOUT);}

	if ( AUTO_TRIG_HOLD_OFF != proxy->AUTO_TRIG_HOLD_OFF )
	{reg_assign(AUTO_TRIG_HOLD_OFF,proxy->AUTO_TRIG_HOLD_OFF);}

	if ( FSM_DELAY != proxy->FSM_DELAY )
	{reg_assign(FSM_DELAY,proxy->FSM_DELAY);}

	if ( PLOT_DELAY != proxy->PLOT_DELAY )
	{reg_assign(PLOT_DELAY,proxy->PLOT_DELAY);}

	if ( FRAME_PLOT_NUM != proxy->FRAME_PLOT_NUM )
	{reg_assign(FRAME_PLOT_NUM,proxy->FRAME_PLOT_NUM);}

	if ( REC_PLAY.payload != proxy->REC_PLAY.payload )
	{reg_assign_field(REC_PLAY,payload,proxy->REC_PLAY.payload);}

	if ( WAVE_INFO.payload != proxy->WAVE_INFO.payload )
	{reg_assign_field(WAVE_INFO,payload,proxy->WAVE_INFO.payload);}

	if ( INDEX_CUR.payload != proxy->INDEX_CUR.payload )
	{reg_assign_field(INDEX_CUR,payload,proxy->INDEX_CUR.payload);}

	if ( INDEX_BASE.payload != proxy->INDEX_BASE.payload )
	{reg_assign_field(INDEX_BASE,payload,proxy->INDEX_BASE.payload);}

	if ( INDEX_UPPER.payload != proxy->INDEX_UPPER.payload )
	{reg_assign_field(INDEX_UPPER,payload,proxy->INDEX_UPPER.payload);}

	if ( TRACE.payload != proxy->TRACE.payload )
	{reg_assign_field(TRACE,payload,proxy->TRACE.payload);}

	if ( TRIG_DLY != proxy->TRIG_DLY )
	{reg_assign(TRIG_DLY,proxy->TRIG_DLY);}

	if ( POS_SA_LAST_VIEW != proxy->POS_SA_LAST_VIEW )
	{reg_assign(POS_SA_LAST_VIEW,proxy->POS_SA_LAST_VIEW);}

	if ( MASK_FRM_NUM_L != proxy->MASK_FRM_NUM_L )
	{reg_assign(MASK_FRM_NUM_L,proxy->MASK_FRM_NUM_L);}

	if ( MASK_FRM_NUM_H.payload != proxy->MASK_FRM_NUM_H.payload )
	{reg_assign_field(MASK_FRM_NUM_H,payload,proxy->MASK_FRM_NUM_H.payload);}

	if ( MASK_TIMEOUT.payload != proxy->MASK_TIMEOUT.payload )
	{reg_assign_field(MASK_TIMEOUT,payload,proxy->MASK_TIMEOUT.payload);}

	if ( STATE_DISPLAY.payload != proxy->STATE_DISPLAY.payload )
	{reg_assign_field(STATE_DISPLAY,payload,proxy->STATE_DISPLAY.payload);}

	if ( CONFIG_ID != proxy->CONFIG_ID )
	{reg_assign(CONFIG_ID,proxy->CONFIG_ID);}

	if ( DEUBG.payload != proxy->DEUBG.payload )
	{reg_assign_field(DEUBG,payload,proxy->DEUBG.payload);}

	if ( DEBUG_PLOT_DONE.payload != proxy->DEBUG_PLOT_DONE.payload )
	{reg_assign_field(DEBUG_PLOT_DONE,payload,proxy->DEBUG_PLOT_DONE.payload);}

	if ( FPGA_SYNC.payload != proxy->FPGA_SYNC.payload )
	{reg_assign_field(FPGA_SYNC,payload,proxy->FPGA_SYNC.payload);}

	if ( DEBUG_WPU_PLOT.payload != proxy->DEBUG_WPU_PLOT.payload )
	{reg_assign_field(DEBUG_WPU_PLOT,payload,proxy->DEBUG_WPU_PLOT.payload);}

	if ( DEBUG_GT_CRC.payload != proxy->DEBUG_GT_CRC.payload )
	{reg_assign_field(DEBUG_GT_CRC,payload,proxy->DEBUG_GT_CRC.payload);}

}
void ScuMem::pull( ScuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(VERSION);
	reg_in(SCU_STATE);
	reg_in(CTRL);
	reg_in(MODE);
	reg_in(MODE_10G);
	reg_in(PRE_SA);
	reg_in(POS_SA);
	reg_in(SCAN_TRIG_POSITION);
	reg_in(AUTO_TRIG_TIMEOUT);
	reg_in(AUTO_TRIG_HOLD_OFF);
	reg_in(FSM_DELAY);
	reg_in(PLOT_DELAY);
	reg_in(FRAME_PLOT_NUM);
	reg_in(REC_PLAY);
	reg_in(WAVE_INFO);
	reg_in(INDEX_CUR);
	reg_in(INDEX_BASE);
	reg_in(INDEX_UPPER);
	reg_in(TRACE);
	reg_in(TRIG_DLY);
	reg_in(POS_SA_LAST_VIEW);
	reg_in(MASK_FRM_NUM_L);
	reg_in(MASK_FRM_NUM_H);
	reg_in(MASK_TIMEOUT);
	reg_in(STATE_DISPLAY);
	reg_in(MASK_TIMER);
	reg_in(CONFIG_ID);
	reg_in(DEUBG);
	reg_in(DEBUG_GT);
	reg_in(DEBUG_PLOT_DONE);
	reg_in(FPGA_SYNC);
	reg_in(WPU_ID);
	reg_in(DEBUG_WPU_PLOT);
	reg_in(DEBUG_GT_CRC);

	*proxy = ScuMemProxy(*this);
}
Scu_reg ScuMem::inVERSION()
{
	reg_in(VERSION);
	return VERSION;
}


void ScuMem::setSCU_STATE_scu_fsm( Scu_reg value )
{
	reg_assign_field(SCU_STATE,scu_fsm,value);
}
void ScuMem::setSCU_STATE_scu_sa_fsm( Scu_reg value )
{
	reg_assign_field(SCU_STATE,scu_sa_fsm,value);
}
void ScuMem::setSCU_STATE_sys_stop( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_stop,value);
}
void ScuMem::setSCU_STATE_scu_stop( Scu_reg value )
{
	reg_assign_field(SCU_STATE,scu_stop,value);
}
void ScuMem::setSCU_STATE_scu_loop( Scu_reg value )
{
	reg_assign_field(SCU_STATE,scu_loop,value);
}
void ScuMem::setSCU_STATE_sys_force_stop( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_force_stop,value);
}
void ScuMem::setSCU_STATE_process_sa( Scu_reg value )
{
	reg_assign_field(SCU_STATE,process_sa,value);
}
void ScuMem::setSCU_STATE_process_sa_done( Scu_reg value )
{
	reg_assign_field(SCU_STATE,process_sa_done,value);
}
void ScuMem::setSCU_STATE_sys_sa_en( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_sa_en,value);
}
void ScuMem::setSCU_STATE_sys_pre_sa_done( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_pre_sa_done,value);
}
void ScuMem::setSCU_STATE_sys_pos_sa_done( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_pos_sa_done,value);
}
void ScuMem::setSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value )
{
	reg_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,value);
}
void ScuMem::setSCU_STATE_sys_trig_d( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_trig_d,value);
}
void ScuMem::setSCU_STATE_tpu_scu_trig( Scu_reg value )
{
	reg_assign_field(SCU_STATE,tpu_scu_trig,value);
}
void ScuMem::setSCU_STATE_fine_trig_done_buf( Scu_reg value )
{
	reg_assign_field(SCU_STATE,fine_trig_done_buf,value);
}
void ScuMem::setSCU_STATE_avg_proc_done( Scu_reg value )
{
	reg_assign_field(SCU_STATE,avg_proc_done,value);
}
void ScuMem::setSCU_STATE_sys_avg_proc( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_avg_proc,value);
}
void ScuMem::setSCU_STATE_sys_avg_exp( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_avg_exp,value);
}
void ScuMem::setSCU_STATE_sys_wave_rd_en( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_wave_rd_en,value);
}
void ScuMem::setSCU_STATE_sys_wave_proc_en( Scu_reg value )
{
	reg_assign_field(SCU_STATE,sys_wave_proc_en,value);
}
void ScuMem::setSCU_STATE_wave_mem_rd_done_hold( Scu_reg value )
{
	reg_assign_field(SCU_STATE,wave_mem_rd_done_hold,value);
}
void ScuMem::setSCU_STATE_spu_tx_done_hold( Scu_reg value )
{
	reg_assign_field(SCU_STATE,spu_tx_done_hold,value);
}
void ScuMem::setSCU_STATE_gt_wave_tx_done( Scu_reg value )
{
	reg_assign_field(SCU_STATE,gt_wave_tx_done,value);
}
void ScuMem::setSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value )
{
	reg_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,value);
}
void ScuMem::setSCU_STATE_task_wave_tx_done_LA( Scu_reg value )
{
	reg_assign_field(SCU_STATE,task_wave_tx_done_LA,value);
}
void ScuMem::setSCU_STATE_task_wave_tx_done_CH( Scu_reg value )
{
	reg_assign_field(SCU_STATE,task_wave_tx_done_CH,value);
}
void ScuMem::setSCU_STATE_spu_proc_done( Scu_reg value )
{
	reg_assign_field(SCU_STATE,spu_proc_done,value);
}
void ScuMem::setSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value )
{
	reg_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,value);
}

void ScuMem::setSCU_STATE( Scu_reg value )
{
	reg_assign_field(SCU_STATE,payload,value);
}

void ScuMem::outSCU_STATE_scu_fsm( Scu_reg value )
{
	out_assign_field(SCU_STATE,scu_fsm,value);
}
void ScuMem::pulseSCU_STATE_scu_fsm( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_fsm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_fsm,idleV);
}
void ScuMem::outSCU_STATE_scu_sa_fsm( Scu_reg value )
{
	out_assign_field(SCU_STATE,scu_sa_fsm,value);
}
void ScuMem::pulseSCU_STATE_scu_sa_fsm( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_sa_fsm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_sa_fsm,idleV);
}
void ScuMem::outSCU_STATE_sys_stop( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_stop,value);
}
void ScuMem::pulseSCU_STATE_sys_stop( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_stop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_stop,idleV);
}
void ScuMem::outSCU_STATE_scu_stop( Scu_reg value )
{
	out_assign_field(SCU_STATE,scu_stop,value);
}
void ScuMem::pulseSCU_STATE_scu_stop( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_stop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_stop,idleV);
}
void ScuMem::outSCU_STATE_scu_loop( Scu_reg value )
{
	out_assign_field(SCU_STATE,scu_loop,value);
}
void ScuMem::pulseSCU_STATE_scu_loop( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_loop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_loop,idleV);
}
void ScuMem::outSCU_STATE_sys_force_stop( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_force_stop,value);
}
void ScuMem::pulseSCU_STATE_sys_force_stop( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_force_stop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_force_stop,idleV);
}
void ScuMem::outSCU_STATE_process_sa( Scu_reg value )
{
	out_assign_field(SCU_STATE,process_sa,value);
}
void ScuMem::pulseSCU_STATE_process_sa( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,process_sa,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,process_sa,idleV);
}
void ScuMem::outSCU_STATE_process_sa_done( Scu_reg value )
{
	out_assign_field(SCU_STATE,process_sa_done,value);
}
void ScuMem::pulseSCU_STATE_process_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,process_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,process_sa_done,idleV);
}
void ScuMem::outSCU_STATE_sys_sa_en( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_sa_en,value);
}
void ScuMem::pulseSCU_STATE_sys_sa_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_sa_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_sa_en,idleV);
}
void ScuMem::outSCU_STATE_sys_pre_sa_done( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_pre_sa_done,value);
}
void ScuMem::pulseSCU_STATE_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_pre_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_pre_sa_done,idleV);
}
void ScuMem::outSCU_STATE_sys_pos_sa_done( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_pos_sa_done,value);
}
void ScuMem::pulseSCU_STATE_sys_pos_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_pos_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_pos_sa_done,idleV);
}
void ScuMem::outSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value )
{
	out_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,value);
}
void ScuMem::pulseSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,idleV);
}
void ScuMem::outSCU_STATE_sys_trig_d( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_trig_d,value);
}
void ScuMem::pulseSCU_STATE_sys_trig_d( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_trig_d,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_trig_d,idleV);
}
void ScuMem::outSCU_STATE_tpu_scu_trig( Scu_reg value )
{
	out_assign_field(SCU_STATE,tpu_scu_trig,value);
}
void ScuMem::pulseSCU_STATE_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,tpu_scu_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,tpu_scu_trig,idleV);
}
void ScuMem::outSCU_STATE_fine_trig_done_buf( Scu_reg value )
{
	out_assign_field(SCU_STATE,fine_trig_done_buf,value);
}
void ScuMem::pulseSCU_STATE_fine_trig_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,fine_trig_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,fine_trig_done_buf,idleV);
}
void ScuMem::outSCU_STATE_avg_proc_done( Scu_reg value )
{
	out_assign_field(SCU_STATE,avg_proc_done,value);
}
void ScuMem::pulseSCU_STATE_avg_proc_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,avg_proc_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,avg_proc_done,idleV);
}
void ScuMem::outSCU_STATE_sys_avg_proc( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_avg_proc,value);
}
void ScuMem::pulseSCU_STATE_sys_avg_proc( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_avg_proc,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_avg_proc,idleV);
}
void ScuMem::outSCU_STATE_sys_avg_exp( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_avg_exp,value);
}
void ScuMem::pulseSCU_STATE_sys_avg_exp( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_avg_exp,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_avg_exp,idleV);
}
void ScuMem::outSCU_STATE_sys_wave_rd_en( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_wave_rd_en,value);
}
void ScuMem::pulseSCU_STATE_sys_wave_rd_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_wave_rd_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_wave_rd_en,idleV);
}
void ScuMem::outSCU_STATE_sys_wave_proc_en( Scu_reg value )
{
	out_assign_field(SCU_STATE,sys_wave_proc_en,value);
}
void ScuMem::pulseSCU_STATE_sys_wave_proc_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_wave_proc_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_wave_proc_en,idleV);
}
void ScuMem::outSCU_STATE_wave_mem_rd_done_hold( Scu_reg value )
{
	out_assign_field(SCU_STATE,wave_mem_rd_done_hold,value);
}
void ScuMem::pulseSCU_STATE_wave_mem_rd_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,wave_mem_rd_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,wave_mem_rd_done_hold,idleV);
}
void ScuMem::outSCU_STATE_spu_tx_done_hold( Scu_reg value )
{
	out_assign_field(SCU_STATE,spu_tx_done_hold,value);
}
void ScuMem::pulseSCU_STATE_spu_tx_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_tx_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_tx_done_hold,idleV);
}
void ScuMem::outSCU_STATE_gt_wave_tx_done( Scu_reg value )
{
	out_assign_field(SCU_STATE,gt_wave_tx_done,value);
}
void ScuMem::pulseSCU_STATE_gt_wave_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,gt_wave_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,gt_wave_tx_done,idleV);
}
void ScuMem::outSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value )
{
	out_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,value);
}
void ScuMem::pulseSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,idleV);
}
void ScuMem::outSCU_STATE_task_wave_tx_done_LA( Scu_reg value )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_LA,value);
}
void ScuMem::pulseSCU_STATE_task_wave_tx_done_LA( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_LA,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,task_wave_tx_done_LA,idleV);
}
void ScuMem::outSCU_STATE_task_wave_tx_done_CH( Scu_reg value )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_CH,value);
}
void ScuMem::pulseSCU_STATE_task_wave_tx_done_CH( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_CH,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,task_wave_tx_done_CH,idleV);
}
void ScuMem::outSCU_STATE_spu_proc_done( Scu_reg value )
{
	out_assign_field(SCU_STATE,spu_proc_done,value);
}
void ScuMem::pulseSCU_STATE_spu_proc_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_proc_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_proc_done,idleV);
}
void ScuMem::outSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value )
{
	out_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,value);
}
void ScuMem::pulseSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,idleV);
}

void ScuMem::outSCU_STATE( Scu_reg value )
{
	out_assign_field(SCU_STATE,payload,value);
}

void ScuMem::outSCU_STATE()
{
	out_member(SCU_STATE);
}


Scu_reg ScuMem::inSCU_STATE()
{
	reg_in(SCU_STATE);
	return SCU_STATE.payload;
}


void ScuMem::setCTRL_sys_stop_state( Scu_reg value )
{
	reg_assign_field(CTRL,sys_stop_state,value);
}
void ScuMem::setCTRL_cfg_fsm_run_stop_n( Scu_reg value )
{
	reg_assign_field(CTRL,cfg_fsm_run_stop_n,value);
}
void ScuMem::setCTRL_run_single( Scu_reg value )
{
	reg_assign_field(CTRL,run_single,value);
}
void ScuMem::setCTRL_mode_play_last( Scu_reg value )
{
	reg_assign_field(CTRL,mode_play_last,value);
}
void ScuMem::setCTRL_scu_fsm_run( Scu_reg value )
{
	reg_assign_field(CTRL,scu_fsm_run,value);
}
void ScuMem::setCTRL_force_trig( Scu_reg value )
{
	reg_assign_field(CTRL,force_trig,value);
}
void ScuMem::setCTRL_wpu_plot_display_finish( Scu_reg value )
{
	reg_assign_field(CTRL,wpu_plot_display_finish,value);
}
void ScuMem::setCTRL_soft_reset_gt( Scu_reg value )
{
	reg_assign_field(CTRL,soft_reset_gt,value);
}
void ScuMem::setCTRL_ms_sync( Scu_reg value )
{
	reg_assign_field(CTRL,ms_sync,value);
}
void ScuMem::setCTRL_fsm_reset( Scu_reg value )
{
	reg_assign_field(CTRL,fsm_reset,value);
}

void ScuMem::setCTRL( Scu_reg value )
{
	reg_assign_field(CTRL,payload,value);
}

void ScuMem::outCTRL_sys_stop_state( Scu_reg value )
{
	out_assign_field(CTRL,sys_stop_state,value);
}
void ScuMem::pulseCTRL_sys_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,sys_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,sys_stop_state,idleV);
}
void ScuMem::outCTRL_cfg_fsm_run_stop_n( Scu_reg value )
{
	out_assign_field(CTRL,cfg_fsm_run_stop_n,value);
}
void ScuMem::pulseCTRL_cfg_fsm_run_stop_n( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,cfg_fsm_run_stop_n,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,cfg_fsm_run_stop_n,idleV);
}
void ScuMem::outCTRL_run_single( Scu_reg value )
{
	out_assign_field(CTRL,run_single,value);
}
void ScuMem::pulseCTRL_run_single( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,run_single,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,run_single,idleV);
}
void ScuMem::outCTRL_mode_play_last( Scu_reg value )
{
	out_assign_field(CTRL,mode_play_last,value);
}
void ScuMem::pulseCTRL_mode_play_last( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,mode_play_last,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,mode_play_last,idleV);
}
void ScuMem::outCTRL_scu_fsm_run( Scu_reg value )
{
	out_assign_field(CTRL,scu_fsm_run,value);
}
void ScuMem::pulseCTRL_scu_fsm_run( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,scu_fsm_run,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,scu_fsm_run,idleV);
}
void ScuMem::outCTRL_force_trig( Scu_reg value )
{
	out_assign_field(CTRL,force_trig,value);
}
void ScuMem::pulseCTRL_force_trig( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,force_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,force_trig,idleV);
}
void ScuMem::outCTRL_wpu_plot_display_finish( Scu_reg value )
{
	out_assign_field(CTRL,wpu_plot_display_finish,value);
}
void ScuMem::pulseCTRL_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,wpu_plot_display_finish,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,wpu_plot_display_finish,idleV);
}
void ScuMem::outCTRL_soft_reset_gt( Scu_reg value )
{
	out_assign_field(CTRL,soft_reset_gt,value);
}
void ScuMem::pulseCTRL_soft_reset_gt( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,soft_reset_gt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,soft_reset_gt,idleV);
}
void ScuMem::outCTRL_ms_sync( Scu_reg value )
{
	out_assign_field(CTRL,ms_sync,value);
}
void ScuMem::pulseCTRL_ms_sync( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,ms_sync,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,ms_sync,idleV);
}
void ScuMem::outCTRL_fsm_reset( Scu_reg value )
{
	out_assign_field(CTRL,fsm_reset,value);
}
void ScuMem::pulseCTRL_fsm_reset( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(CTRL,fsm_reset,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,fsm_reset,idleV);
}

void ScuMem::outCTRL( Scu_reg value )
{
	out_assign_field(CTRL,payload,value);
}

void ScuMem::outCTRL()
{
	out_member(CTRL);
}


Scu_reg ScuMem::inCTRL()
{
	reg_in(CTRL);
	return CTRL.payload;
}


void ScuMem::setMODE_cfg_mode_scan( Scu_reg value )
{
	reg_assign_field(MODE,cfg_mode_scan,value);
}
void ScuMem::setMODE_sys_mode_scan_trace( Scu_reg value )
{
	reg_assign_field(MODE,sys_mode_scan_trace,value);
}
void ScuMem::setMODE_sys_mode_scan_zoom( Scu_reg value )
{
	reg_assign_field(MODE,sys_mode_scan_zoom,value);
}
void ScuMem::setMODE_mode_scan_en( Scu_reg value )
{
	reg_assign_field(MODE,mode_scan_en,value);
}
void ScuMem::setMODE_search_en_zoom( Scu_reg value )
{
	reg_assign_field(MODE,search_en_zoom,value);
}
void ScuMem::setMODE_search_en_ch( Scu_reg value )
{
	reg_assign_field(MODE,search_en_ch,value);
}
void ScuMem::setMODE_search_en_la( Scu_reg value )
{
	reg_assign_field(MODE,search_en_la,value);
}
void ScuMem::setMODE_mode_import_type( Scu_reg value )
{
	reg_assign_field(MODE,mode_import_type,value);
}
void ScuMem::setMODE_mode_export( Scu_reg value )
{
	reg_assign_field(MODE,mode_export,value);
}
void ScuMem::setMODE_mode_import_play( Scu_reg value )
{
	reg_assign_field(MODE,mode_import_play,value);
}
void ScuMem::setMODE_mode_import_rec( Scu_reg value )
{
	reg_assign_field(MODE,mode_import_rec,value);
}
void ScuMem::setMODE_measure_en_zoom( Scu_reg value )
{
	reg_assign_field(MODE,measure_en_zoom,value);
}
void ScuMem::setMODE_measure_en_ch( Scu_reg value )
{
	reg_assign_field(MODE,measure_en_ch,value);
}
void ScuMem::setMODE_measure_en_la( Scu_reg value )
{
	reg_assign_field(MODE,measure_en_la,value);
}
void ScuMem::setMODE_wave_ch_en( Scu_reg value )
{
	reg_assign_field(MODE,wave_ch_en,value);
}
void ScuMem::setMODE_wave_la_en( Scu_reg value )
{
	reg_assign_field(MODE,wave_la_en,value);
}
void ScuMem::setMODE_zoom_en( Scu_reg value )
{
	reg_assign_field(MODE,zoom_en,value);
}
void ScuMem::setMODE_mask_err_stop_en( Scu_reg value )
{
	reg_assign_field(MODE,mask_err_stop_en,value);
}
void ScuMem::setMODE_mask_save_fail( Scu_reg value )
{
	reg_assign_field(MODE,mask_save_fail,value);
}
void ScuMem::setMODE_mask_save_pass( Scu_reg value )
{
	reg_assign_field(MODE,mask_save_pass,value);
}
void ScuMem::setMODE_mask_pf_en( Scu_reg value )
{
	reg_assign_field(MODE,mask_pf_en,value);
}
void ScuMem::setMODE_zone_trig_en( Scu_reg value )
{
	reg_assign_field(MODE,zone_trig_en,value);
}
void ScuMem::setMODE_mode_roll_en( Scu_reg value )
{
	reg_assign_field(MODE,mode_roll_en,value);
}
void ScuMem::setMODE_mode_fast_ref( Scu_reg value )
{
	reg_assign_field(MODE,mode_fast_ref,value);
}
void ScuMem::setMODE_auto_trig( Scu_reg value )
{
	reg_assign_field(MODE,auto_trig,value);
}
void ScuMem::setMODE_interleave_20G( Scu_reg value )
{
	reg_assign_field(MODE,interleave_20G,value);
}
void ScuMem::setMODE_en_20G( Scu_reg value )
{
	reg_assign_field(MODE,en_20G,value);
}

void ScuMem::setMODE( Scu_reg value )
{
	reg_assign_field(MODE,payload,value);
}

void ScuMem::outMODE_cfg_mode_scan( Scu_reg value )
{
	out_assign_field(MODE,cfg_mode_scan,value);
}
void ScuMem::pulseMODE_cfg_mode_scan( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,cfg_mode_scan,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,cfg_mode_scan,idleV);
}
void ScuMem::outMODE_sys_mode_scan_trace( Scu_reg value )
{
	out_assign_field(MODE,sys_mode_scan_trace,value);
}
void ScuMem::pulseMODE_sys_mode_scan_trace( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,sys_mode_scan_trace,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,sys_mode_scan_trace,idleV);
}
void ScuMem::outMODE_sys_mode_scan_zoom( Scu_reg value )
{
	out_assign_field(MODE,sys_mode_scan_zoom,value);
}
void ScuMem::pulseMODE_sys_mode_scan_zoom( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,sys_mode_scan_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,sys_mode_scan_zoom,idleV);
}
void ScuMem::outMODE_mode_scan_en( Scu_reg value )
{
	out_assign_field(MODE,mode_scan_en,value);
}
void ScuMem::pulseMODE_mode_scan_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_scan_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_scan_en,idleV);
}
void ScuMem::outMODE_search_en_zoom( Scu_reg value )
{
	out_assign_field(MODE,search_en_zoom,value);
}
void ScuMem::pulseMODE_search_en_zoom( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,search_en_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,search_en_zoom,idleV);
}
void ScuMem::outMODE_search_en_ch( Scu_reg value )
{
	out_assign_field(MODE,search_en_ch,value);
}
void ScuMem::pulseMODE_search_en_ch( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,search_en_ch,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,search_en_ch,idleV);
}
void ScuMem::outMODE_search_en_la( Scu_reg value )
{
	out_assign_field(MODE,search_en_la,value);
}
void ScuMem::pulseMODE_search_en_la( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,search_en_la,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,search_en_la,idleV);
}
void ScuMem::outMODE_mode_import_type( Scu_reg value )
{
	out_assign_field(MODE,mode_import_type,value);
}
void ScuMem::pulseMODE_mode_import_type( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_import_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_import_type,idleV);
}
void ScuMem::outMODE_mode_export( Scu_reg value )
{
	out_assign_field(MODE,mode_export,value);
}
void ScuMem::pulseMODE_mode_export( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_export,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_export,idleV);
}
void ScuMem::outMODE_mode_import_play( Scu_reg value )
{
	out_assign_field(MODE,mode_import_play,value);
}
void ScuMem::pulseMODE_mode_import_play( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_import_play,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_import_play,idleV);
}
void ScuMem::outMODE_mode_import_rec( Scu_reg value )
{
	out_assign_field(MODE,mode_import_rec,value);
}
void ScuMem::pulseMODE_mode_import_rec( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_import_rec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_import_rec,idleV);
}
void ScuMem::outMODE_measure_en_zoom( Scu_reg value )
{
	out_assign_field(MODE,measure_en_zoom,value);
}
void ScuMem::pulseMODE_measure_en_zoom( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,measure_en_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,measure_en_zoom,idleV);
}
void ScuMem::outMODE_measure_en_ch( Scu_reg value )
{
	out_assign_field(MODE,measure_en_ch,value);
}
void ScuMem::pulseMODE_measure_en_ch( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,measure_en_ch,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,measure_en_ch,idleV);
}
void ScuMem::outMODE_measure_en_la( Scu_reg value )
{
	out_assign_field(MODE,measure_en_la,value);
}
void ScuMem::pulseMODE_measure_en_la( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,measure_en_la,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,measure_en_la,idleV);
}
void ScuMem::outMODE_wave_ch_en( Scu_reg value )
{
	out_assign_field(MODE,wave_ch_en,value);
}
void ScuMem::pulseMODE_wave_ch_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,wave_ch_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,wave_ch_en,idleV);
}
void ScuMem::outMODE_wave_la_en( Scu_reg value )
{
	out_assign_field(MODE,wave_la_en,value);
}
void ScuMem::pulseMODE_wave_la_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,wave_la_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,wave_la_en,idleV);
}
void ScuMem::outMODE_zoom_en( Scu_reg value )
{
	out_assign_field(MODE,zoom_en,value);
}
void ScuMem::pulseMODE_zoom_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,zoom_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,zoom_en,idleV);
}
void ScuMem::outMODE_mask_err_stop_en( Scu_reg value )
{
	out_assign_field(MODE,mask_err_stop_en,value);
}
void ScuMem::pulseMODE_mask_err_stop_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_err_stop_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_err_stop_en,idleV);
}
void ScuMem::outMODE_mask_save_fail( Scu_reg value )
{
	out_assign_field(MODE,mask_save_fail,value);
}
void ScuMem::pulseMODE_mask_save_fail( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_save_fail,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_save_fail,idleV);
}
void ScuMem::outMODE_mask_save_pass( Scu_reg value )
{
	out_assign_field(MODE,mask_save_pass,value);
}
void ScuMem::pulseMODE_mask_save_pass( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_save_pass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_save_pass,idleV);
}
void ScuMem::outMODE_mask_pf_en( Scu_reg value )
{
	out_assign_field(MODE,mask_pf_en,value);
}
void ScuMem::pulseMODE_mask_pf_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_pf_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_pf_en,idleV);
}
void ScuMem::outMODE_zone_trig_en( Scu_reg value )
{
	out_assign_field(MODE,zone_trig_en,value);
}
void ScuMem::pulseMODE_zone_trig_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,zone_trig_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,zone_trig_en,idleV);
}
void ScuMem::outMODE_mode_roll_en( Scu_reg value )
{
	out_assign_field(MODE,mode_roll_en,value);
}
void ScuMem::pulseMODE_mode_roll_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_roll_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_roll_en,idleV);
}
void ScuMem::outMODE_mode_fast_ref( Scu_reg value )
{
	out_assign_field(MODE,mode_fast_ref,value);
}
void ScuMem::pulseMODE_mode_fast_ref( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_fast_ref,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_fast_ref,idleV);
}
void ScuMem::outMODE_auto_trig( Scu_reg value )
{
	out_assign_field(MODE,auto_trig,value);
}
void ScuMem::pulseMODE_auto_trig( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,auto_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,auto_trig,idleV);
}
void ScuMem::outMODE_interleave_20G( Scu_reg value )
{
	out_assign_field(MODE,interleave_20G,value);
}
void ScuMem::pulseMODE_interleave_20G( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,interleave_20G,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,interleave_20G,idleV);
}
void ScuMem::outMODE_en_20G( Scu_reg value )
{
	out_assign_field(MODE,en_20G,value);
}
void ScuMem::pulseMODE_en_20G( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE,en_20G,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,en_20G,idleV);
}

void ScuMem::outMODE( Scu_reg value )
{
	out_assign_field(MODE,payload,value);
}

void ScuMem::outMODE()
{
	out_member(MODE);
}


Scu_reg ScuMem::inMODE()
{
	reg_in(MODE);
	return MODE.payload;
}


void ScuMem::setMODE_10G_chn_10G( Scu_reg value )
{
	reg_assign_field(MODE_10G,chn_10G,value);
}
void ScuMem::setMODE_10G_cfg_chn( Scu_reg value )
{
	reg_assign_field(MODE_10G,cfg_chn,value);
}

void ScuMem::setMODE_10G( Scu_reg value )
{
	reg_assign_field(MODE_10G,payload,value);
}

void ScuMem::outMODE_10G_chn_10G( Scu_reg value )
{
	out_assign_field(MODE_10G,chn_10G,value);
}
void ScuMem::pulseMODE_10G_chn_10G( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE_10G,chn_10G,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE_10G,chn_10G,idleV);
}
void ScuMem::outMODE_10G_cfg_chn( Scu_reg value )
{
	out_assign_field(MODE_10G,cfg_chn,value);
}
void ScuMem::pulseMODE_10G_cfg_chn( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MODE_10G,cfg_chn,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE_10G,cfg_chn,idleV);
}

void ScuMem::outMODE_10G( Scu_reg value )
{
	out_assign_field(MODE_10G,payload,value);
}

void ScuMem::outMODE_10G()
{
	out_member(MODE_10G);
}


Scu_reg ScuMem::inMODE_10G()
{
	reg_in(MODE_10G);
	return MODE_10G.payload;
}


void ScuMem::setPRE_SA( Scu_reg value )
{
	reg_assign(PRE_SA,value);
}

void ScuMem::outPRE_SA( Scu_reg value )
{
	out_assign(PRE_SA,value);
}

void ScuMem::outPRE_SA()
{
	out_member(PRE_SA);
}


Scu_reg ScuMem::inPRE_SA()
{
	reg_in(PRE_SA);
	return PRE_SA;
}


void ScuMem::setPOS_SA( Scu_reg value )
{
	reg_assign(POS_SA,value);
}

void ScuMem::outPOS_SA( Scu_reg value )
{
	out_assign(POS_SA,value);
}

void ScuMem::outPOS_SA()
{
	out_member(POS_SA);
}


Scu_reg ScuMem::inPOS_SA()
{
	reg_in(POS_SA);
	return POS_SA;
}


void ScuMem::setSCAN_TRIG_POSITION_position( Scu_reg value )
{
	reg_assign_field(SCAN_TRIG_POSITION,position,value);
}
void ScuMem::setSCAN_TRIG_POSITION_trig_left_side( Scu_reg value )
{
	reg_assign_field(SCAN_TRIG_POSITION,trig_left_side,value);
}

void ScuMem::setSCAN_TRIG_POSITION( Scu_reg value )
{
	reg_assign_field(SCAN_TRIG_POSITION,payload,value);
}

void ScuMem::outSCAN_TRIG_POSITION_position( Scu_reg value )
{
	out_assign_field(SCAN_TRIG_POSITION,position,value);
}
void ScuMem::pulseSCAN_TRIG_POSITION_position( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCAN_TRIG_POSITION,position,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCAN_TRIG_POSITION,position,idleV);
}
void ScuMem::outSCAN_TRIG_POSITION_trig_left_side( Scu_reg value )
{
	out_assign_field(SCAN_TRIG_POSITION,trig_left_side,value);
}
void ScuMem::pulseSCAN_TRIG_POSITION_trig_left_side( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(SCAN_TRIG_POSITION,trig_left_side,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCAN_TRIG_POSITION,trig_left_side,idleV);
}

void ScuMem::outSCAN_TRIG_POSITION( Scu_reg value )
{
	out_assign_field(SCAN_TRIG_POSITION,payload,value);
}

void ScuMem::outSCAN_TRIG_POSITION()
{
	out_member(SCAN_TRIG_POSITION);
}


Scu_reg ScuMem::inSCAN_TRIG_POSITION()
{
	reg_in(SCAN_TRIG_POSITION);
	return SCAN_TRIG_POSITION.payload;
}


void ScuMem::setAUTO_TRIG_TIMEOUT( Scu_reg value )
{
	reg_assign(AUTO_TRIG_TIMEOUT,value);
}

void ScuMem::outAUTO_TRIG_TIMEOUT( Scu_reg value )
{
	out_assign(AUTO_TRIG_TIMEOUT,value);
}

void ScuMem::outAUTO_TRIG_TIMEOUT()
{
	out_member(AUTO_TRIG_TIMEOUT);
}


Scu_reg ScuMem::inAUTO_TRIG_TIMEOUT()
{
	reg_in(AUTO_TRIG_TIMEOUT);
	return AUTO_TRIG_TIMEOUT;
}


void ScuMem::setAUTO_TRIG_HOLD_OFF( Scu_reg value )
{
	reg_assign(AUTO_TRIG_HOLD_OFF,value);
}

void ScuMem::outAUTO_TRIG_HOLD_OFF( Scu_reg value )
{
	out_assign(AUTO_TRIG_HOLD_OFF,value);
}

void ScuMem::outAUTO_TRIG_HOLD_OFF()
{
	out_member(AUTO_TRIG_HOLD_OFF);
}


Scu_reg ScuMem::inAUTO_TRIG_HOLD_OFF()
{
	reg_in(AUTO_TRIG_HOLD_OFF);
	return AUTO_TRIG_HOLD_OFF;
}


void ScuMem::setFSM_DELAY( Scu_reg value )
{
	reg_assign(FSM_DELAY,value);
}

void ScuMem::outFSM_DELAY( Scu_reg value )
{
	out_assign(FSM_DELAY,value);
}

void ScuMem::outFSM_DELAY()
{
	out_member(FSM_DELAY);
}


Scu_reg ScuMem::inFSM_DELAY()
{
	reg_in(FSM_DELAY);
	return FSM_DELAY;
}


void ScuMem::setPLOT_DELAY( Scu_reg value )
{
	reg_assign(PLOT_DELAY,value);
}

void ScuMem::outPLOT_DELAY( Scu_reg value )
{
	out_assign(PLOT_DELAY,value);
}

void ScuMem::outPLOT_DELAY()
{
	out_member(PLOT_DELAY);
}


Scu_reg ScuMem::inPLOT_DELAY()
{
	reg_in(PLOT_DELAY);
	return PLOT_DELAY;
}


void ScuMem::setFRAME_PLOT_NUM( Scu_reg value )
{
	reg_assign(FRAME_PLOT_NUM,value);
}

void ScuMem::outFRAME_PLOT_NUM( Scu_reg value )
{
	out_assign(FRAME_PLOT_NUM,value);
}

void ScuMem::outFRAME_PLOT_NUM()
{
	out_member(FRAME_PLOT_NUM);
}


Scu_reg ScuMem::inFRAME_PLOT_NUM()
{
	reg_in(FRAME_PLOT_NUM);
	return FRAME_PLOT_NUM;
}


void ScuMem::setREC_PLAY_index_full( Scu_reg value )
{
	reg_assign_field(REC_PLAY,index_full,value);
}
void ScuMem::setREC_PLAY_loop_playback( Scu_reg value )
{
	reg_assign_field(REC_PLAY,loop_playback,value);
}
void ScuMem::setREC_PLAY_index_load( Scu_reg value )
{
	reg_assign_field(REC_PLAY,index_load,value);
}
void ScuMem::setREC_PLAY_index_inc( Scu_reg value )
{
	reg_assign_field(REC_PLAY,index_inc,value);
}
void ScuMem::setREC_PLAY_mode_play( Scu_reg value )
{
	reg_assign_field(REC_PLAY,mode_play,value);
}
void ScuMem::setREC_PLAY_mode_rec( Scu_reg value )
{
	reg_assign_field(REC_PLAY,mode_rec,value);
}

void ScuMem::setREC_PLAY( Scu_reg value )
{
	reg_assign_field(REC_PLAY,payload,value);
}

void ScuMem::outREC_PLAY_index_full( Scu_reg value )
{
	out_assign_field(REC_PLAY,index_full,value);
}
void ScuMem::pulseREC_PLAY_index_full( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,index_full,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,index_full,idleV);
}
void ScuMem::outREC_PLAY_loop_playback( Scu_reg value )
{
	out_assign_field(REC_PLAY,loop_playback,value);
}
void ScuMem::pulseREC_PLAY_loop_playback( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,loop_playback,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,loop_playback,idleV);
}
void ScuMem::outREC_PLAY_index_load( Scu_reg value )
{
	out_assign_field(REC_PLAY,index_load,value);
}
void ScuMem::pulseREC_PLAY_index_load( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,index_load,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,index_load,idleV);
}
void ScuMem::outREC_PLAY_index_inc( Scu_reg value )
{
	out_assign_field(REC_PLAY,index_inc,value);
}
void ScuMem::pulseREC_PLAY_index_inc( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,index_inc,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,index_inc,idleV);
}
void ScuMem::outREC_PLAY_mode_play( Scu_reg value )
{
	out_assign_field(REC_PLAY,mode_play,value);
}
void ScuMem::pulseREC_PLAY_mode_play( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,mode_play,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,mode_play,idleV);
}
void ScuMem::outREC_PLAY_mode_rec( Scu_reg value )
{
	out_assign_field(REC_PLAY,mode_rec,value);
}
void ScuMem::pulseREC_PLAY_mode_rec( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,mode_rec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,mode_rec,idleV);
}

void ScuMem::outREC_PLAY( Scu_reg value )
{
	out_assign_field(REC_PLAY,payload,value);
}

void ScuMem::outREC_PLAY()
{
	out_member(REC_PLAY);
}


Scu_reg ScuMem::inREC_PLAY()
{
	reg_in(REC_PLAY);
	return REC_PLAY.payload;
}


void ScuMem::setWAVE_INFO_wave_plot_frame_cnt( Scu_reg value )
{
	reg_assign_field(WAVE_INFO,wave_plot_frame_cnt,value);
}
void ScuMem::setWAVE_INFO_wave_play_vld( Scu_reg value )
{
	reg_assign_field(WAVE_INFO,wave_play_vld,value);
}

void ScuMem::setWAVE_INFO( Scu_reg value )
{
	reg_assign_field(WAVE_INFO,payload,value);
}

void ScuMem::outWAVE_INFO_wave_plot_frame_cnt( Scu_reg value )
{
	out_assign_field(WAVE_INFO,wave_plot_frame_cnt,value);
}
void ScuMem::pulseWAVE_INFO_wave_plot_frame_cnt( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(WAVE_INFO,wave_plot_frame_cnt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(WAVE_INFO,wave_plot_frame_cnt,idleV);
}
void ScuMem::outWAVE_INFO_wave_play_vld( Scu_reg value )
{
	out_assign_field(WAVE_INFO,wave_play_vld,value);
}
void ScuMem::pulseWAVE_INFO_wave_play_vld( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(WAVE_INFO,wave_play_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(WAVE_INFO,wave_play_vld,idleV);
}

void ScuMem::outWAVE_INFO( Scu_reg value )
{
	out_assign_field(WAVE_INFO,payload,value);
}

void ScuMem::outWAVE_INFO()
{
	out_member(WAVE_INFO);
}


Scu_reg ScuMem::inWAVE_INFO()
{
	reg_in(WAVE_INFO);
	return WAVE_INFO.payload;
}


void ScuMem::setINDEX_CUR_index( Scu_reg value )
{
	reg_assign_field(INDEX_CUR,index,value);
}

void ScuMem::setINDEX_CUR( Scu_reg value )
{
	reg_assign_field(INDEX_CUR,payload,value);
}

void ScuMem::outINDEX_CUR_index( Scu_reg value )
{
	out_assign_field(INDEX_CUR,index,value);
}
void ScuMem::pulseINDEX_CUR_index( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(INDEX_CUR,index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INDEX_CUR,index,idleV);
}

void ScuMem::outINDEX_CUR( Scu_reg value )
{
	out_assign_field(INDEX_CUR,payload,value);
}

void ScuMem::outINDEX_CUR()
{
	out_member(INDEX_CUR);
}


Scu_reg ScuMem::inINDEX_CUR()
{
	reg_in(INDEX_CUR);
	return INDEX_CUR.payload;
}


void ScuMem::setINDEX_BASE_index( Scu_reg value )
{
	reg_assign_field(INDEX_BASE,index,value);
}

void ScuMem::setINDEX_BASE( Scu_reg value )
{
	reg_assign_field(INDEX_BASE,payload,value);
}

void ScuMem::outINDEX_BASE_index( Scu_reg value )
{
	out_assign_field(INDEX_BASE,index,value);
}
void ScuMem::pulseINDEX_BASE_index( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(INDEX_BASE,index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INDEX_BASE,index,idleV);
}

void ScuMem::outINDEX_BASE( Scu_reg value )
{
	out_assign_field(INDEX_BASE,payload,value);
}

void ScuMem::outINDEX_BASE()
{
	out_member(INDEX_BASE);
}


Scu_reg ScuMem::inINDEX_BASE()
{
	reg_in(INDEX_BASE);
	return INDEX_BASE.payload;
}


void ScuMem::setINDEX_UPPER_index( Scu_reg value )
{
	reg_assign_field(INDEX_UPPER,index,value);
}

void ScuMem::setINDEX_UPPER( Scu_reg value )
{
	reg_assign_field(INDEX_UPPER,payload,value);
}

void ScuMem::outINDEX_UPPER_index( Scu_reg value )
{
	out_assign_field(INDEX_UPPER,index,value);
}
void ScuMem::pulseINDEX_UPPER_index( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(INDEX_UPPER,index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INDEX_UPPER,index,idleV);
}

void ScuMem::outINDEX_UPPER( Scu_reg value )
{
	out_assign_field(INDEX_UPPER,payload,value);
}

void ScuMem::outINDEX_UPPER()
{
	out_member(INDEX_UPPER);
}


Scu_reg ScuMem::inINDEX_UPPER()
{
	reg_in(INDEX_UPPER);
	return INDEX_UPPER.payload;
}


void ScuMem::setTRACE_trace_once_req( Scu_reg value )
{
	reg_assign_field(TRACE,trace_once_req,value);
}
void ScuMem::setTRACE_measure_once_req( Scu_reg value )
{
	reg_assign_field(TRACE,measure_once_req,value);
}
void ScuMem::setTRACE_search_once_req( Scu_reg value )
{
	reg_assign_field(TRACE,search_once_req,value);
}
void ScuMem::setTRACE_eye_once_req( Scu_reg value )
{
	reg_assign_field(TRACE,eye_once_req,value);
}
void ScuMem::setTRACE_wave_bypass_wpu( Scu_reg value )
{
	reg_assign_field(TRACE,wave_bypass_wpu,value);
}
void ScuMem::setTRACE_wave_bypass_trace( Scu_reg value )
{
	reg_assign_field(TRACE,wave_bypass_trace,value);
}
void ScuMem::setTRACE_avg_clr( Scu_reg value )
{
	reg_assign_field(TRACE,avg_clr,value);
}

void ScuMem::setTRACE( Scu_reg value )
{
	reg_assign_field(TRACE,payload,value);
}

void ScuMem::outTRACE_trace_once_req( Scu_reg value )
{
	out_assign_field(TRACE,trace_once_req,value);
}
void ScuMem::pulseTRACE_trace_once_req( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(TRACE,trace_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,trace_once_req,idleV);
}
void ScuMem::outTRACE_measure_once_req( Scu_reg value )
{
	out_assign_field(TRACE,measure_once_req,value);
}
void ScuMem::pulseTRACE_measure_once_req( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(TRACE,measure_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,measure_once_req,idleV);
}
void ScuMem::outTRACE_search_once_req( Scu_reg value )
{
	out_assign_field(TRACE,search_once_req,value);
}
void ScuMem::pulseTRACE_search_once_req( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(TRACE,search_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,search_once_req,idleV);
}
void ScuMem::outTRACE_eye_once_req( Scu_reg value )
{
	out_assign_field(TRACE,eye_once_req,value);
}
void ScuMem::pulseTRACE_eye_once_req( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(TRACE,eye_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,eye_once_req,idleV);
}
void ScuMem::outTRACE_wave_bypass_wpu( Scu_reg value )
{
	out_assign_field(TRACE,wave_bypass_wpu,value);
}
void ScuMem::pulseTRACE_wave_bypass_wpu( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(TRACE,wave_bypass_wpu,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,wave_bypass_wpu,idleV);
}
void ScuMem::outTRACE_wave_bypass_trace( Scu_reg value )
{
	out_assign_field(TRACE,wave_bypass_trace,value);
}
void ScuMem::pulseTRACE_wave_bypass_trace( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(TRACE,wave_bypass_trace,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,wave_bypass_trace,idleV);
}
void ScuMem::outTRACE_avg_clr( Scu_reg value )
{
	out_assign_field(TRACE,avg_clr,value);
}
void ScuMem::pulseTRACE_avg_clr( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(TRACE,avg_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,avg_clr,idleV);
}

void ScuMem::outTRACE( Scu_reg value )
{
	out_assign_field(TRACE,payload,value);
}

void ScuMem::outTRACE()
{
	out_member(TRACE);
}


Scu_reg ScuMem::inTRACE()
{
	reg_in(TRACE);
	return TRACE.payload;
}


void ScuMem::setTRIG_DLY( Scu_reg value )
{
	reg_assign(TRIG_DLY,value);
}

void ScuMem::outTRIG_DLY( Scu_reg value )
{
	out_assign(TRIG_DLY,value);
}

void ScuMem::outTRIG_DLY()
{
	out_member(TRIG_DLY);
}


Scu_reg ScuMem::inTRIG_DLY()
{
	reg_in(TRIG_DLY);
	return TRIG_DLY;
}


void ScuMem::setPOS_SA_LAST_VIEW( Scu_reg value )
{
	reg_assign(POS_SA_LAST_VIEW,value);
}

void ScuMem::outPOS_SA_LAST_VIEW( Scu_reg value )
{
	out_assign(POS_SA_LAST_VIEW,value);
}

void ScuMem::outPOS_SA_LAST_VIEW()
{
	out_member(POS_SA_LAST_VIEW);
}


Scu_reg ScuMem::inPOS_SA_LAST_VIEW()
{
	reg_in(POS_SA_LAST_VIEW);
	return POS_SA_LAST_VIEW;
}


void ScuMem::setMASK_FRM_NUM_L( Scu_reg value )
{
	reg_assign(MASK_FRM_NUM_L,value);
}

void ScuMem::outMASK_FRM_NUM_L( Scu_reg value )
{
	out_assign(MASK_FRM_NUM_L,value);
}

void ScuMem::outMASK_FRM_NUM_L()
{
	out_member(MASK_FRM_NUM_L);
}


Scu_reg ScuMem::inMASK_FRM_NUM_L()
{
	reg_in(MASK_FRM_NUM_L);
	return MASK_FRM_NUM_L;
}


void ScuMem::setMASK_FRM_NUM_H_frm_num_h( Scu_reg value )
{
	reg_assign_field(MASK_FRM_NUM_H,frm_num_h,value);
}
void ScuMem::setMASK_FRM_NUM_H_frm_num_en( Scu_reg value )
{
	reg_assign_field(MASK_FRM_NUM_H,frm_num_en,value);
}

void ScuMem::setMASK_FRM_NUM_H( Scu_reg value )
{
	reg_assign_field(MASK_FRM_NUM_H,payload,value);
}

void ScuMem::outMASK_FRM_NUM_H_frm_num_h( Scu_reg value )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_h,value);
}
void ScuMem::pulseMASK_FRM_NUM_H_frm_num_h( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_FRM_NUM_H,frm_num_h,idleV);
}
void ScuMem::outMASK_FRM_NUM_H_frm_num_en( Scu_reg value )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_en,value);
}
void ScuMem::pulseMASK_FRM_NUM_H_frm_num_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_FRM_NUM_H,frm_num_en,idleV);
}

void ScuMem::outMASK_FRM_NUM_H( Scu_reg value )
{
	out_assign_field(MASK_FRM_NUM_H,payload,value);
}

void ScuMem::outMASK_FRM_NUM_H()
{
	out_member(MASK_FRM_NUM_H);
}


Scu_reg ScuMem::inMASK_FRM_NUM_H()
{
	reg_in(MASK_FRM_NUM_H);
	return MASK_FRM_NUM_H.payload;
}


void ScuMem::setMASK_TIMEOUT_timeout( Scu_reg value )
{
	reg_assign_field(MASK_TIMEOUT,timeout,value);
}
void ScuMem::setMASK_TIMEOUT_timeout_en( Scu_reg value )
{
	reg_assign_field(MASK_TIMEOUT,timeout_en,value);
}

void ScuMem::setMASK_TIMEOUT( Scu_reg value )
{
	reg_assign_field(MASK_TIMEOUT,payload,value);
}

void ScuMem::outMASK_TIMEOUT_timeout( Scu_reg value )
{
	out_assign_field(MASK_TIMEOUT,timeout,value);
}
void ScuMem::pulseMASK_TIMEOUT_timeout( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MASK_TIMEOUT,timeout,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TIMEOUT,timeout,idleV);
}
void ScuMem::outMASK_TIMEOUT_timeout_en( Scu_reg value )
{
	out_assign_field(MASK_TIMEOUT,timeout_en,value);
}
void ScuMem::pulseMASK_TIMEOUT_timeout_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(MASK_TIMEOUT,timeout_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TIMEOUT,timeout_en,idleV);
}

void ScuMem::outMASK_TIMEOUT( Scu_reg value )
{
	out_assign_field(MASK_TIMEOUT,payload,value);
}

void ScuMem::outMASK_TIMEOUT()
{
	out_member(MASK_TIMEOUT);
}


Scu_reg ScuMem::inMASK_TIMEOUT()
{
	reg_in(MASK_TIMEOUT);
	return MASK_TIMEOUT.payload;
}


void ScuMem::setSTATE_DISPLAY_scu_fsm( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,scu_fsm,value);
}
void ScuMem::setSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,sys_pre_sa_done,value);
}
void ScuMem::setSTATE_DISPLAY_sys_stop_state( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,sys_stop_state,value);
}
void ScuMem::setSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,value);
}
void ScuMem::setSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,disp_sys_trig_d,value);
}
void ScuMem::setSTATE_DISPLAY_rec_play_stop_state( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,rec_play_stop_state,value);
}
void ScuMem::setSTATE_DISPLAY_mask_stop_state( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,mask_stop_state,value);
}
void ScuMem::setSTATE_DISPLAY_stop_stata_clr( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,stop_stata_clr,value);
}
void ScuMem::setSTATE_DISPLAY_disp_trig_clr( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,disp_trig_clr,value);
}

void ScuMem::setSTATE_DISPLAY( Scu_reg value )
{
	reg_assign_field(STATE_DISPLAY,payload,value);
}

void ScuMem::outSTATE_DISPLAY_scu_fsm( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,scu_fsm,value);
}
void ScuMem::pulseSTATE_DISPLAY_scu_fsm( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,scu_fsm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,scu_fsm,idleV);
}
void ScuMem::outSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,sys_pre_sa_done,value);
}
void ScuMem::pulseSTATE_DISPLAY_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,sys_pre_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,sys_pre_sa_done,idleV);
}
void ScuMem::outSTATE_DISPLAY_sys_stop_state( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,sys_stop_state,value);
}
void ScuMem::pulseSTATE_DISPLAY_sys_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,sys_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,sys_stop_state,idleV);
}
void ScuMem::outSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,value);
}
void ScuMem::pulseSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,idleV);
}
void ScuMem::outSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,disp_sys_trig_d,value);
}
void ScuMem::pulseSTATE_DISPLAY_disp_sys_trig_d( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,disp_sys_trig_d,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,disp_sys_trig_d,idleV);
}
void ScuMem::outSTATE_DISPLAY_rec_play_stop_state( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,rec_play_stop_state,value);
}
void ScuMem::pulseSTATE_DISPLAY_rec_play_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,rec_play_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,rec_play_stop_state,idleV);
}
void ScuMem::outSTATE_DISPLAY_mask_stop_state( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,mask_stop_state,value);
}
void ScuMem::pulseSTATE_DISPLAY_mask_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,mask_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,mask_stop_state,idleV);
}
void ScuMem::outSTATE_DISPLAY_stop_stata_clr( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,stop_stata_clr,value);
}
void ScuMem::pulseSTATE_DISPLAY_stop_stata_clr( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,stop_stata_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,stop_stata_clr,idleV);
}
void ScuMem::outSTATE_DISPLAY_disp_trig_clr( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,disp_trig_clr,value);
}
void ScuMem::pulseSTATE_DISPLAY_disp_trig_clr( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,disp_trig_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,disp_trig_clr,idleV);
}

void ScuMem::outSTATE_DISPLAY( Scu_reg value )
{
	out_assign_field(STATE_DISPLAY,payload,value);
}

void ScuMem::outSTATE_DISPLAY()
{
	out_member(STATE_DISPLAY);
}


Scu_reg ScuMem::inSTATE_DISPLAY()
{
	reg_in(STATE_DISPLAY);
	return STATE_DISPLAY.payload;
}


Scu_reg ScuMem::inMASK_TIMER()
{
	reg_in(MASK_TIMER);
	return MASK_TIMER;
}


void ScuMem::setCONFIG_ID( Scu_reg value )
{
	reg_assign(CONFIG_ID,value);
}

void ScuMem::outCONFIG_ID( Scu_reg value )
{
	out_assign(CONFIG_ID,value);
}

void ScuMem::outCONFIG_ID()
{
	out_member(CONFIG_ID);
}


Scu_reg ScuMem::inCONFIG_ID()
{
	reg_in(CONFIG_ID);
	return CONFIG_ID;
}


void ScuMem::setDEUBG_wave_sa_ctrl( Scu_reg value )
{
	reg_assign_field(DEUBG,wave_sa_ctrl,value);
}
void ScuMem::setDEUBG_sys_auto_trig_act( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_auto_trig_act,value);
}
void ScuMem::setDEUBG_sys_auto_trig_en( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_auto_trig_en,value);
}
void ScuMem::setDEUBG_sys_fine_trig_req( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_fine_trig_req,value);
}
void ScuMem::setDEUBG_sys_meas_only( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_meas_only,value);
}
void ScuMem::setDEUBG_sys_meas_en( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_meas_en,value);
}
void ScuMem::setDEUBG_sys_meas_type( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_meas_type,value);
}
void ScuMem::setDEUBG_spu_tx_roll_null( Scu_reg value )
{
	reg_assign_field(DEUBG,spu_tx_roll_null,value);
}
void ScuMem::setDEUBG_measure_once_done_hold( Scu_reg value )
{
	reg_assign_field(DEUBG,measure_once_done_hold,value);
}
void ScuMem::setDEUBG_measure_finish_done_hold( Scu_reg value )
{
	reg_assign_field(DEUBG,measure_finish_done_hold,value);
}
void ScuMem::setDEUBG_sys_mask_en( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_mask_en,value);
}
void ScuMem::setDEUBG_mask_pf_result_vld_hold( Scu_reg value )
{
	reg_assign_field(DEUBG,mask_pf_result_vld_hold,value);
}
void ScuMem::setDEUBG_mask_pf_result_cross_hold( Scu_reg value )
{
	reg_assign_field(DEUBG,mask_pf_result_cross_hold,value);
}
void ScuMem::setDEUBG_mask_pf_timeout( Scu_reg value )
{
	reg_assign_field(DEUBG,mask_pf_timeout,value);
}
void ScuMem::setDEUBG_mask_frm_overflow( Scu_reg value )
{
	reg_assign_field(DEUBG,mask_frm_overflow,value);
}
void ScuMem::setDEUBG_measure_once_buf( Scu_reg value )
{
	reg_assign_field(DEUBG,measure_once_buf,value);
}
void ScuMem::setDEUBG_zone_result_vld_hold( Scu_reg value )
{
	reg_assign_field(DEUBG,zone_result_vld_hold,value);
}
void ScuMem::setDEUBG_zone_result_trig_hold( Scu_reg value )
{
	reg_assign_field(DEUBG,zone_result_trig_hold,value);
}
void ScuMem::setDEUBG_wave_proc_la( Scu_reg value )
{
	reg_assign_field(DEUBG,wave_proc_la,value);
}
void ScuMem::setDEUBG_sys_zoom( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_zoom,value);
}
void ScuMem::setDEUBG_sys_fast_ref( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_fast_ref,value);
}
void ScuMem::setDEUBG_spu_tx_done( Scu_reg value )
{
	reg_assign_field(DEUBG,spu_tx_done,value);
}
void ScuMem::setDEUBG_sys_avg_en( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_avg_en,value);
}
void ScuMem::setDEUBG_sys_trace( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_trace,value);
}
void ScuMem::setDEUBG_sys_pos_sa_last_frm( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_pos_sa_last_frm,value);
}
void ScuMem::setDEUBG_sys_pos_sa_view( Scu_reg value )
{
	reg_assign_field(DEUBG,sys_pos_sa_view,value);
}
void ScuMem::setDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value )
{
	reg_assign_field(DEUBG,spu_wav_sa_rd_tx_done,value);
}
void ScuMem::setDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value )
{
	reg_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,value);
}
void ScuMem::setDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value )
{
	reg_assign_field(DEUBG,scan_roll_pos_last_frm_buf,value);
}

void ScuMem::setDEUBG( Scu_reg value )
{
	reg_assign_field(DEUBG,payload,value);
}

void ScuMem::outDEUBG_wave_sa_ctrl( Scu_reg value )
{
	out_assign_field(DEUBG,wave_sa_ctrl,value);
}
void ScuMem::pulseDEUBG_wave_sa_ctrl( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,wave_sa_ctrl,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,wave_sa_ctrl,idleV);
}
void ScuMem::outDEUBG_sys_auto_trig_act( Scu_reg value )
{
	out_assign_field(DEUBG,sys_auto_trig_act,value);
}
void ScuMem::pulseDEUBG_sys_auto_trig_act( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_auto_trig_act,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_auto_trig_act,idleV);
}
void ScuMem::outDEUBG_sys_auto_trig_en( Scu_reg value )
{
	out_assign_field(DEUBG,sys_auto_trig_en,value);
}
void ScuMem::pulseDEUBG_sys_auto_trig_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_auto_trig_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_auto_trig_en,idleV);
}
void ScuMem::outDEUBG_sys_fine_trig_req( Scu_reg value )
{
	out_assign_field(DEUBG,sys_fine_trig_req,value);
}
void ScuMem::pulseDEUBG_sys_fine_trig_req( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_fine_trig_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_fine_trig_req,idleV);
}
void ScuMem::outDEUBG_sys_meas_only( Scu_reg value )
{
	out_assign_field(DEUBG,sys_meas_only,value);
}
void ScuMem::pulseDEUBG_sys_meas_only( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_meas_only,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_meas_only,idleV);
}
void ScuMem::outDEUBG_sys_meas_en( Scu_reg value )
{
	out_assign_field(DEUBG,sys_meas_en,value);
}
void ScuMem::pulseDEUBG_sys_meas_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_meas_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_meas_en,idleV);
}
void ScuMem::outDEUBG_sys_meas_type( Scu_reg value )
{
	out_assign_field(DEUBG,sys_meas_type,value);
}
void ScuMem::pulseDEUBG_sys_meas_type( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_meas_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_meas_type,idleV);
}
void ScuMem::outDEUBG_spu_tx_roll_null( Scu_reg value )
{
	out_assign_field(DEUBG,spu_tx_roll_null,value);
}
void ScuMem::pulseDEUBG_spu_tx_roll_null( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,spu_tx_roll_null,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,spu_tx_roll_null,idleV);
}
void ScuMem::outDEUBG_measure_once_done_hold( Scu_reg value )
{
	out_assign_field(DEUBG,measure_once_done_hold,value);
}
void ScuMem::pulseDEUBG_measure_once_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,measure_once_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,measure_once_done_hold,idleV);
}
void ScuMem::outDEUBG_measure_finish_done_hold( Scu_reg value )
{
	out_assign_field(DEUBG,measure_finish_done_hold,value);
}
void ScuMem::pulseDEUBG_measure_finish_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,measure_finish_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,measure_finish_done_hold,idleV);
}
void ScuMem::outDEUBG_sys_mask_en( Scu_reg value )
{
	out_assign_field(DEUBG,sys_mask_en,value);
}
void ScuMem::pulseDEUBG_sys_mask_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_mask_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_mask_en,idleV);
}
void ScuMem::outDEUBG_mask_pf_result_vld_hold( Scu_reg value )
{
	out_assign_field(DEUBG,mask_pf_result_vld_hold,value);
}
void ScuMem::pulseDEUBG_mask_pf_result_vld_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_pf_result_vld_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_pf_result_vld_hold,idleV);
}
void ScuMem::outDEUBG_mask_pf_result_cross_hold( Scu_reg value )
{
	out_assign_field(DEUBG,mask_pf_result_cross_hold,value);
}
void ScuMem::pulseDEUBG_mask_pf_result_cross_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_pf_result_cross_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_pf_result_cross_hold,idleV);
}
void ScuMem::outDEUBG_mask_pf_timeout( Scu_reg value )
{
	out_assign_field(DEUBG,mask_pf_timeout,value);
}
void ScuMem::pulseDEUBG_mask_pf_timeout( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_pf_timeout,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_pf_timeout,idleV);
}
void ScuMem::outDEUBG_mask_frm_overflow( Scu_reg value )
{
	out_assign_field(DEUBG,mask_frm_overflow,value);
}
void ScuMem::pulseDEUBG_mask_frm_overflow( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_frm_overflow,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_frm_overflow,idleV);
}
void ScuMem::outDEUBG_measure_once_buf( Scu_reg value )
{
	out_assign_field(DEUBG,measure_once_buf,value);
}
void ScuMem::pulseDEUBG_measure_once_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,measure_once_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,measure_once_buf,idleV);
}
void ScuMem::outDEUBG_zone_result_vld_hold( Scu_reg value )
{
	out_assign_field(DEUBG,zone_result_vld_hold,value);
}
void ScuMem::pulseDEUBG_zone_result_vld_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,zone_result_vld_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,zone_result_vld_hold,idleV);
}
void ScuMem::outDEUBG_zone_result_trig_hold( Scu_reg value )
{
	out_assign_field(DEUBG,zone_result_trig_hold,value);
}
void ScuMem::pulseDEUBG_zone_result_trig_hold( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,zone_result_trig_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,zone_result_trig_hold,idleV);
}
void ScuMem::outDEUBG_wave_proc_la( Scu_reg value )
{
	out_assign_field(DEUBG,wave_proc_la,value);
}
void ScuMem::pulseDEUBG_wave_proc_la( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,wave_proc_la,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,wave_proc_la,idleV);
}
void ScuMem::outDEUBG_sys_zoom( Scu_reg value )
{
	out_assign_field(DEUBG,sys_zoom,value);
}
void ScuMem::pulseDEUBG_sys_zoom( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_zoom,idleV);
}
void ScuMem::outDEUBG_sys_fast_ref( Scu_reg value )
{
	out_assign_field(DEUBG,sys_fast_ref,value);
}
void ScuMem::pulseDEUBG_sys_fast_ref( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_fast_ref,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_fast_ref,idleV);
}
void ScuMem::outDEUBG_spu_tx_done( Scu_reg value )
{
	out_assign_field(DEUBG,spu_tx_done,value);
}
void ScuMem::pulseDEUBG_spu_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,spu_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,spu_tx_done,idleV);
}
void ScuMem::outDEUBG_sys_avg_en( Scu_reg value )
{
	out_assign_field(DEUBG,sys_avg_en,value);
}
void ScuMem::pulseDEUBG_sys_avg_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_avg_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_avg_en,idleV);
}
void ScuMem::outDEUBG_sys_trace( Scu_reg value )
{
	out_assign_field(DEUBG,sys_trace,value);
}
void ScuMem::pulseDEUBG_sys_trace( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_trace,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_trace,idleV);
}
void ScuMem::outDEUBG_sys_pos_sa_last_frm( Scu_reg value )
{
	out_assign_field(DEUBG,sys_pos_sa_last_frm,value);
}
void ScuMem::pulseDEUBG_sys_pos_sa_last_frm( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_pos_sa_last_frm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_pos_sa_last_frm,idleV);
}
void ScuMem::outDEUBG_sys_pos_sa_view( Scu_reg value )
{
	out_assign_field(DEUBG,sys_pos_sa_view,value);
}
void ScuMem::pulseDEUBG_sys_pos_sa_view( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_pos_sa_view,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_pos_sa_view,idleV);
}
void ScuMem::outDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value )
{
	out_assign_field(DEUBG,spu_wav_sa_rd_tx_done,value);
}
void ScuMem::pulseDEUBG_spu_wav_sa_rd_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,spu_wav_sa_rd_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,spu_wav_sa_rd_tx_done,idleV);
}
void ScuMem::outDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value )
{
	out_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,value);
}
void ScuMem::pulseDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,idleV);
}
void ScuMem::outDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value )
{
	out_assign_field(DEUBG,scan_roll_pos_last_frm_buf,value);
}
void ScuMem::pulseDEUBG_scan_roll_pos_last_frm_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,scan_roll_pos_last_frm_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,scan_roll_pos_last_frm_buf,idleV);
}

void ScuMem::outDEUBG( Scu_reg value )
{
	out_assign_field(DEUBG,payload,value);
}

void ScuMem::outDEUBG()
{
	out_member(DEUBG);
}


Scu_reg ScuMem::inDEUBG()
{
	reg_in(DEUBG);
	return DEUBG.payload;
}


Scu_reg ScuMem::inDEBUG_GT()
{
	reg_in(DEBUG_GT);
	return DEBUG_GT.payload;
}


void ScuMem::setDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value )
{
	reg_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,value);
}

void ScuMem::setDEBUG_PLOT_DONE( Scu_reg value )
{
	reg_assign_field(DEBUG_PLOT_DONE,payload,value);
}

void ScuMem::outDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value )
{
	out_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,value);
}
void ScuMem::pulseDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,idleV);
}

void ScuMem::outDEBUG_PLOT_DONE( Scu_reg value )
{
	out_assign_field(DEBUG_PLOT_DONE,payload,value);
}

void ScuMem::outDEBUG_PLOT_DONE()
{
	out_member(DEBUG_PLOT_DONE);
}


Scu_reg ScuMem::inDEBUG_PLOT_DONE()
{
	reg_in(DEBUG_PLOT_DONE);
	return DEBUG_PLOT_DONE.payload;
}


void ScuMem::setFPGA_SYNC_sync_cnt( Scu_reg value )
{
	reg_assign_field(FPGA_SYNC,sync_cnt,value);
}
void ScuMem::setFPGA_SYNC_debug_scu_reset( Scu_reg value )
{
	reg_assign_field(FPGA_SYNC,debug_scu_reset,value);
}
void ScuMem::setFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value )
{
	reg_assign_field(FPGA_SYNC,debug_sys_rst_cnt,value);
}

void ScuMem::setFPGA_SYNC( Scu_reg value )
{
	reg_assign_field(FPGA_SYNC,payload,value);
}

void ScuMem::outFPGA_SYNC_sync_cnt( Scu_reg value )
{
	out_assign_field(FPGA_SYNC,sync_cnt,value);
}
void ScuMem::pulseFPGA_SYNC_sync_cnt( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(FPGA_SYNC,sync_cnt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FPGA_SYNC,sync_cnt,idleV);
}
void ScuMem::outFPGA_SYNC_debug_scu_reset( Scu_reg value )
{
	out_assign_field(FPGA_SYNC,debug_scu_reset,value);
}
void ScuMem::pulseFPGA_SYNC_debug_scu_reset( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(FPGA_SYNC,debug_scu_reset,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FPGA_SYNC,debug_scu_reset,idleV);
}
void ScuMem::outFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value )
{
	out_assign_field(FPGA_SYNC,debug_sys_rst_cnt,value);
}
void ScuMem::pulseFPGA_SYNC_debug_sys_rst_cnt( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(FPGA_SYNC,debug_sys_rst_cnt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FPGA_SYNC,debug_sys_rst_cnt,idleV);
}

void ScuMem::outFPGA_SYNC( Scu_reg value )
{
	out_assign_field(FPGA_SYNC,payload,value);
}

void ScuMem::outFPGA_SYNC()
{
	out_member(FPGA_SYNC);
}


Scu_reg ScuMem::inFPGA_SYNC()
{
	reg_in(FPGA_SYNC);
	return FPGA_SYNC.payload;
}


Scu_reg ScuMem::inWPU_ID()
{
	reg_in(WPU_ID);
	return WPU_ID.payload;
}


void ScuMem::setDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,value);
}
void ScuMem::setDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,value);
}
void ScuMem::setDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,value);
}
void ScuMem::setDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,value);
}
void ScuMem::setDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,value);
}
void ScuMem::setDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,value);
}
void ScuMem::setDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,value);
}
void ScuMem::setDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,value);
}
void ScuMem::setDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,value);
}
void ScuMem::setDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,value);
}
void ScuMem::setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,value);
}
void ScuMem::setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,value);
}
void ScuMem::setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,value);
}
void ScuMem::setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,value);
}
void ScuMem::setDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,value);
}
void ScuMem::setDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,value);
}

void ScuMem::setDEBUG_WPU_PLOT( Scu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,payload,value);
}

void ScuMem::outDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,idleV);
}
void ScuMem::outDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,value);
}
void ScuMem::pulseDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,idleV);
}

void ScuMem::outDEBUG_WPU_PLOT( Scu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,payload,value);
}

void ScuMem::outDEBUG_WPU_PLOT()
{
	out_member(DEBUG_WPU_PLOT);
}


Scu_reg ScuMem::inDEBUG_WPU_PLOT()
{
	reg_in(DEBUG_WPU_PLOT);
	return DEBUG_WPU_PLOT.payload;
}


void ScuMem::setDEBUG_GT_CRC_fail_sum( Scu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,fail_sum,value);
}
void ScuMem::setDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,value);
}
void ScuMem::setDEBUG_GT_CRC_crc_valid_i( Scu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,crc_valid_i,value);
}

void ScuMem::setDEBUG_GT_CRC( Scu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,payload,value);
}

void ScuMem::outDEBUG_GT_CRC_fail_sum( Scu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,fail_sum,value);
}
void ScuMem::pulseDEBUG_GT_CRC_fail_sum( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_GT_CRC,fail_sum,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_GT_CRC,fail_sum,idleV);
}
void ScuMem::outDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,value);
}
void ScuMem::pulseDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,idleV);
}
void ScuMem::outDEBUG_GT_CRC_crc_valid_i( Scu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,crc_valid_i,value);
}
void ScuMem::pulseDEBUG_GT_CRC_crc_valid_i( Scu_reg activeV, Scu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_GT_CRC,crc_valid_i,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_GT_CRC,crc_valid_i,idleV);
}

void ScuMem::outDEBUG_GT_CRC( Scu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,payload,value);
}

void ScuMem::outDEBUG_GT_CRC()
{
	out_member(DEBUG_GT_CRC);
}


Scu_reg ScuMem::inDEBUG_GT_CRC()
{
	reg_in(DEBUG_GT_CRC);
	return DEBUG_GT_CRC.payload;
}


cache_addr ScuMem::addr_VERSION(){ return 0x1C00+mAddrBase; }
cache_addr ScuMem::addr_SCU_STATE(){ return 0x1C04+mAddrBase; }
cache_addr ScuMem::addr_CTRL(){ return 0x1C08+mAddrBase; }
cache_addr ScuMem::addr_MODE(){ return 0x1C0C+mAddrBase; }

cache_addr ScuMem::addr_MODE_10G(){ return 0x1C10+mAddrBase; }
cache_addr ScuMem::addr_PRE_SA(){ return 0x1C14+mAddrBase; }
cache_addr ScuMem::addr_POS_SA(){ return 0x1C18+mAddrBase; }
cache_addr ScuMem::addr_SCAN_TRIG_POSITION(){ return 0x1C1C+mAddrBase; }

cache_addr ScuMem::addr_AUTO_TRIG_TIMEOUT(){ return 0x1C20+mAddrBase; }
cache_addr ScuMem::addr_AUTO_TRIG_HOLD_OFF(){ return 0x1C24+mAddrBase; }
cache_addr ScuMem::addr_FSM_DELAY(){ return 0x1C28+mAddrBase; }
cache_addr ScuMem::addr_PLOT_DELAY(){ return 0x1C34+mAddrBase; }

cache_addr ScuMem::addr_FRAME_PLOT_NUM(){ return 0x1C38+mAddrBase; }
cache_addr ScuMem::addr_REC_PLAY(){ return 0x1C3C+mAddrBase; }
cache_addr ScuMem::addr_WAVE_INFO(){ return 0x1C40+mAddrBase; }
cache_addr ScuMem::addr_INDEX_CUR(){ return 0x1C44+mAddrBase; }

cache_addr ScuMem::addr_INDEX_BASE(){ return 0x1C48+mAddrBase; }
cache_addr ScuMem::addr_INDEX_UPPER(){ return 0x1C4C+mAddrBase; }
cache_addr ScuMem::addr_TRACE(){ return 0x1C50+mAddrBase; }
cache_addr ScuMem::addr_TRIG_DLY(){ return 0x1C54+mAddrBase; }

cache_addr ScuMem::addr_POS_SA_LAST_VIEW(){ return 0x1C5C+mAddrBase; }
cache_addr ScuMem::addr_MASK_FRM_NUM_L(){ return 0x1C60+mAddrBase; }
cache_addr ScuMem::addr_MASK_FRM_NUM_H(){ return 0x1C64+mAddrBase; }
cache_addr ScuMem::addr_MASK_TIMEOUT(){ return 0x1C68+mAddrBase; }

cache_addr ScuMem::addr_STATE_DISPLAY(){ return 0x1C6C+mAddrBase; }
cache_addr ScuMem::addr_MASK_TIMER(){ return 0x1C70+mAddrBase; }
cache_addr ScuMem::addr_CONFIG_ID(){ return 0x1C74+mAddrBase; }
cache_addr ScuMem::addr_DEUBG(){ return 0x1C80+mAddrBase; }

cache_addr ScuMem::addr_DEBUG_GT(){ return 0x1C84+mAddrBase; }
cache_addr ScuMem::addr_DEBUG_PLOT_DONE(){ return 0x1C88+mAddrBase; }
cache_addr ScuMem::addr_FPGA_SYNC(){ return 0x1C8C+mAddrBase; }
cache_addr ScuMem::addr_WPU_ID(){ return 0x1C90+mAddrBase; }

cache_addr ScuMem::addr_DEBUG_WPU_PLOT(){ return 0x1C94+mAddrBase; }
cache_addr ScuMem::addr_DEBUG_GT_CRC(){ return 0x1C98+mAddrBase; }


void ScuGp::flushWCache(int id)
{
	if(id==-1){ foreach( IPhyScu *pItem, mSubItems){ Q_ASSERT(NULL!=pItem); pItem->flushWCache(); } }
	else{ Q_ASSERT( id>=0 && mSubItems[id] != NULL); mSubItems[id]->flushWCache(); }
}

Scu_reg ScuGp::getVERSION( int id )
{
	return mSubItems[id]->getVERSION();
}

Scu_reg ScuGp::inVERSION( int id )
{
	return mSubItems[id]->inVERSION();
}


void ScuGp::setSCU_STATE_scu_fsm(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_scu_fsm(value);} }
	else{ mSubItems[id]->setSCU_STATE_scu_fsm( value); }
}
void ScuGp::setSCU_STATE_scu_sa_fsm(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_scu_sa_fsm(value);} }
	else{ mSubItems[id]->setSCU_STATE_scu_sa_fsm( value); }
}
void ScuGp::setSCU_STATE_sys_stop(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_stop(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_stop( value); }
}
void ScuGp::setSCU_STATE_scu_stop(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_scu_stop(value);} }
	else{ mSubItems[id]->setSCU_STATE_scu_stop( value); }
}
void ScuGp::setSCU_STATE_scu_loop(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_scu_loop(value);} }
	else{ mSubItems[id]->setSCU_STATE_scu_loop( value); }
}
void ScuGp::setSCU_STATE_sys_force_stop(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_force_stop(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_force_stop( value); }
}
void ScuGp::setSCU_STATE_process_sa(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_process_sa(value);} }
	else{ mSubItems[id]->setSCU_STATE_process_sa( value); }
}
void ScuGp::setSCU_STATE_process_sa_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_process_sa_done(value);} }
	else{ mSubItems[id]->setSCU_STATE_process_sa_done( value); }
}
void ScuGp::setSCU_STATE_sys_sa_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_sa_en(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_sa_en( value); }
}
void ScuGp::setSCU_STATE_sys_pre_sa_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_pre_sa_done(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_pre_sa_done( value); }
}
void ScuGp::setSCU_STATE_sys_pos_sa_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_pos_sa_done(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_pos_sa_done( value); }
}
void ScuGp::setSCU_STATE_spu_wave_mem_wr_done_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_spu_wave_mem_wr_done_hold(value);} }
	else{ mSubItems[id]->setSCU_STATE_spu_wave_mem_wr_done_hold( value); }
}
void ScuGp::setSCU_STATE_sys_trig_d(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_trig_d(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_trig_d( value); }
}
void ScuGp::setSCU_STATE_tpu_scu_trig(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_tpu_scu_trig(value);} }
	else{ mSubItems[id]->setSCU_STATE_tpu_scu_trig( value); }
}
void ScuGp::setSCU_STATE_fine_trig_done_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_fine_trig_done_buf(value);} }
	else{ mSubItems[id]->setSCU_STATE_fine_trig_done_buf( value); }
}
void ScuGp::setSCU_STATE_avg_proc_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_avg_proc_done(value);} }
	else{ mSubItems[id]->setSCU_STATE_avg_proc_done( value); }
}
void ScuGp::setSCU_STATE_sys_avg_proc(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_avg_proc(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_avg_proc( value); }
}
void ScuGp::setSCU_STATE_sys_avg_exp(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_avg_exp(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_avg_exp( value); }
}
void ScuGp::setSCU_STATE_sys_wave_rd_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_wave_rd_en(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_wave_rd_en( value); }
}
void ScuGp::setSCU_STATE_sys_wave_proc_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_sys_wave_proc_en(value);} }
	else{ mSubItems[id]->setSCU_STATE_sys_wave_proc_en( value); }
}
void ScuGp::setSCU_STATE_wave_mem_rd_done_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_wave_mem_rd_done_hold(value);} }
	else{ mSubItems[id]->setSCU_STATE_wave_mem_rd_done_hold( value); }
}
void ScuGp::setSCU_STATE_spu_tx_done_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_spu_tx_done_hold(value);} }
	else{ mSubItems[id]->setSCU_STATE_spu_tx_done_hold( value); }
}
void ScuGp::setSCU_STATE_gt_wave_tx_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_gt_wave_tx_done(value);} }
	else{ mSubItems[id]->setSCU_STATE_gt_wave_tx_done( value); }
}
void ScuGp::setSCU_STATE_spu_wav_rd_meas_once_tx_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_spu_wav_rd_meas_once_tx_done(value);} }
	else{ mSubItems[id]->setSCU_STATE_spu_wav_rd_meas_once_tx_done( value); }
}
void ScuGp::setSCU_STATE_task_wave_tx_done_LA(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_task_wave_tx_done_LA(value);} }
	else{ mSubItems[id]->setSCU_STATE_task_wave_tx_done_LA( value); }
}
void ScuGp::setSCU_STATE_task_wave_tx_done_CH(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_task_wave_tx_done_CH(value);} }
	else{ mSubItems[id]->setSCU_STATE_task_wave_tx_done_CH( value); }
}
void ScuGp::setSCU_STATE_spu_proc_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_spu_proc_done(value);} }
	else{ mSubItems[id]->setSCU_STATE_spu_proc_done( value); }
}
void ScuGp::setSCU_STATE_scan_roll_sa_done_mem_tx_null(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE_scan_roll_sa_done_mem_tx_null(value);} }
	else{ mSubItems[id]->setSCU_STATE_scan_roll_sa_done_mem_tx_null( value); }
}

void ScuGp::setSCU_STATE( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCU_STATE(value);} }
	else{ mSubItems[id]->setSCU_STATE( value); }
}

void ScuGp::outSCU_STATE_scu_fsm( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_fsm(value);}}
	else{ mSubItems[id]->outSCU_STATE_scu_fsm( value); }
}
void ScuGp::pulseSCU_STATE_scu_fsm( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_fsm(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_scu_fsm(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_scu_fsm( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_scu_sa_fsm( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_sa_fsm(value);}}
	else{ mSubItems[id]->outSCU_STATE_scu_sa_fsm( value); }
}
void ScuGp::pulseSCU_STATE_scu_sa_fsm( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_sa_fsm(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_scu_sa_fsm(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_scu_sa_fsm( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_stop( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_stop(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_stop( value); }
}
void ScuGp::pulseSCU_STATE_sys_stop( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_stop(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_stop(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_stop( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_scu_stop( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_stop(value);}}
	else{ mSubItems[id]->outSCU_STATE_scu_stop( value); }
}
void ScuGp::pulseSCU_STATE_scu_stop( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_stop(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_scu_stop(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_scu_stop( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_scu_loop( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_loop(value);}}
	else{ mSubItems[id]->outSCU_STATE_scu_loop( value); }
}
void ScuGp::pulseSCU_STATE_scu_loop( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scu_loop(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_scu_loop(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_scu_loop( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_force_stop( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_force_stop(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_force_stop( value); }
}
void ScuGp::pulseSCU_STATE_sys_force_stop( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_force_stop(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_force_stop(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_force_stop( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_process_sa( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_process_sa(value);}}
	else{ mSubItems[id]->outSCU_STATE_process_sa( value); }
}
void ScuGp::pulseSCU_STATE_process_sa( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_process_sa(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_process_sa(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_process_sa( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_process_sa_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_process_sa_done(value);}}
	else{ mSubItems[id]->outSCU_STATE_process_sa_done( value); }
}
void ScuGp::pulseSCU_STATE_process_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_process_sa_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_process_sa_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_process_sa_done( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_sa_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_sa_en(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_sa_en( value); }
}
void ScuGp::pulseSCU_STATE_sys_sa_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_sa_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_sa_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_sa_en( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_pre_sa_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_pre_sa_done(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_pre_sa_done( value); }
}
void ScuGp::pulseSCU_STATE_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_pre_sa_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_pre_sa_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_pre_sa_done( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_pos_sa_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_pos_sa_done(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_pos_sa_done( value); }
}
void ScuGp::pulseSCU_STATE_sys_pos_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_pos_sa_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_pos_sa_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_pos_sa_done( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_wave_mem_wr_done_hold(value);}}
	else{ mSubItems[id]->outSCU_STATE_spu_wave_mem_wr_done_hold( value); }
}
void ScuGp::pulseSCU_STATE_spu_wave_mem_wr_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_wave_mem_wr_done_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_spu_wave_mem_wr_done_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_spu_wave_mem_wr_done_hold( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_trig_d( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_trig_d(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_trig_d( value); }
}
void ScuGp::pulseSCU_STATE_sys_trig_d( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_trig_d(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_trig_d(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_trig_d( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_tpu_scu_trig( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_tpu_scu_trig(value);}}
	else{ mSubItems[id]->outSCU_STATE_tpu_scu_trig( value); }
}
void ScuGp::pulseSCU_STATE_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_tpu_scu_trig(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_tpu_scu_trig(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_tpu_scu_trig( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_fine_trig_done_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_fine_trig_done_buf(value);}}
	else{ mSubItems[id]->outSCU_STATE_fine_trig_done_buf( value); }
}
void ScuGp::pulseSCU_STATE_fine_trig_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_fine_trig_done_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_fine_trig_done_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_fine_trig_done_buf( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_avg_proc_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_avg_proc_done(value);}}
	else{ mSubItems[id]->outSCU_STATE_avg_proc_done( value); }
}
void ScuGp::pulseSCU_STATE_avg_proc_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_avg_proc_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_avg_proc_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_avg_proc_done( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_avg_proc( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_avg_proc(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_avg_proc( value); }
}
void ScuGp::pulseSCU_STATE_sys_avg_proc( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_avg_proc(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_avg_proc(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_avg_proc( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_avg_exp( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_avg_exp(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_avg_exp( value); }
}
void ScuGp::pulseSCU_STATE_sys_avg_exp( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_avg_exp(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_avg_exp(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_avg_exp( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_wave_rd_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_wave_rd_en(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_wave_rd_en( value); }
}
void ScuGp::pulseSCU_STATE_sys_wave_rd_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_wave_rd_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_wave_rd_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_wave_rd_en( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_sys_wave_proc_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_wave_proc_en(value);}}
	else{ mSubItems[id]->outSCU_STATE_sys_wave_proc_en( value); }
}
void ScuGp::pulseSCU_STATE_sys_wave_proc_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_sys_wave_proc_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_sys_wave_proc_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_sys_wave_proc_en( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_wave_mem_rd_done_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_wave_mem_rd_done_hold(value);}}
	else{ mSubItems[id]->outSCU_STATE_wave_mem_rd_done_hold( value); }
}
void ScuGp::pulseSCU_STATE_wave_mem_rd_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_wave_mem_rd_done_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_wave_mem_rd_done_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_wave_mem_rd_done_hold( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_spu_tx_done_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_tx_done_hold(value);}}
	else{ mSubItems[id]->outSCU_STATE_spu_tx_done_hold( value); }
}
void ScuGp::pulseSCU_STATE_spu_tx_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_tx_done_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_spu_tx_done_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_spu_tx_done_hold( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_gt_wave_tx_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_gt_wave_tx_done(value);}}
	else{ mSubItems[id]->outSCU_STATE_gt_wave_tx_done( value); }
}
void ScuGp::pulseSCU_STATE_gt_wave_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_gt_wave_tx_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_gt_wave_tx_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_gt_wave_tx_done( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_wav_rd_meas_once_tx_done(value);}}
	else{ mSubItems[id]->outSCU_STATE_spu_wav_rd_meas_once_tx_done( value); }
}
void ScuGp::pulseSCU_STATE_spu_wav_rd_meas_once_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_wav_rd_meas_once_tx_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_spu_wav_rd_meas_once_tx_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_spu_wav_rd_meas_once_tx_done( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_task_wave_tx_done_LA( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_task_wave_tx_done_LA(value);}}
	else{ mSubItems[id]->outSCU_STATE_task_wave_tx_done_LA( value); }
}
void ScuGp::pulseSCU_STATE_task_wave_tx_done_LA( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_task_wave_tx_done_LA(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_task_wave_tx_done_LA(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_task_wave_tx_done_LA( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_task_wave_tx_done_CH( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_task_wave_tx_done_CH(value);}}
	else{ mSubItems[id]->outSCU_STATE_task_wave_tx_done_CH( value); }
}
void ScuGp::pulseSCU_STATE_task_wave_tx_done_CH( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_task_wave_tx_done_CH(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_task_wave_tx_done_CH(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_task_wave_tx_done_CH( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_spu_proc_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_proc_done(value);}}
	else{ mSubItems[id]->outSCU_STATE_spu_proc_done( value); }
}
void ScuGp::pulseSCU_STATE_spu_proc_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_spu_proc_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_spu_proc_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_spu_proc_done( activeV, idleV, timeus ); }
}
void ScuGp::outSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scan_roll_sa_done_mem_tx_null(value);}}
	else{ mSubItems[id]->outSCU_STATE_scan_roll_sa_done_mem_tx_null( value); }
}
void ScuGp::pulseSCU_STATE_scan_roll_sa_done_mem_tx_null( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE_scan_roll_sa_done_mem_tx_null(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCU_STATE_scan_roll_sa_done_mem_tx_null(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCU_STATE_scan_roll_sa_done_mem_tx_null( activeV, idleV, timeus ); }
}

void ScuGp::outSCU_STATE(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE(value);} }
	else{ mSubItems[id]->outSCU_STATE( value); }
}

void ScuGp::outSCU_STATE( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCU_STATE();} }
	else{ mSubItems[id]->outSCU_STATE(); }
}


Scu_reg ScuGp::getSCU_STATE_scu_fsm( int id )
{
	return mSubItems[id]->getSCU_STATE_scu_fsm();
}
Scu_reg ScuGp::getSCU_STATE_scu_sa_fsm( int id )
{
	return mSubItems[id]->getSCU_STATE_scu_sa_fsm();
}
Scu_reg ScuGp::getSCU_STATE_sys_stop( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_stop();
}
Scu_reg ScuGp::getSCU_STATE_scu_stop( int id )
{
	return mSubItems[id]->getSCU_STATE_scu_stop();
}
Scu_reg ScuGp::getSCU_STATE_scu_loop( int id )
{
	return mSubItems[id]->getSCU_STATE_scu_loop();
}
Scu_reg ScuGp::getSCU_STATE_sys_force_stop( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_force_stop();
}
Scu_reg ScuGp::getSCU_STATE_process_sa( int id )
{
	return mSubItems[id]->getSCU_STATE_process_sa();
}
Scu_reg ScuGp::getSCU_STATE_process_sa_done( int id )
{
	return mSubItems[id]->getSCU_STATE_process_sa_done();
}
Scu_reg ScuGp::getSCU_STATE_sys_sa_en( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_sa_en();
}
Scu_reg ScuGp::getSCU_STATE_sys_pre_sa_done( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_pre_sa_done();
}
Scu_reg ScuGp::getSCU_STATE_sys_pos_sa_done( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_pos_sa_done();
}
Scu_reg ScuGp::getSCU_STATE_spu_wave_mem_wr_done_hold( int id )
{
	return mSubItems[id]->getSCU_STATE_spu_wave_mem_wr_done_hold();
}
Scu_reg ScuGp::getSCU_STATE_sys_trig_d( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_trig_d();
}
Scu_reg ScuGp::getSCU_STATE_tpu_scu_trig( int id )
{
	return mSubItems[id]->getSCU_STATE_tpu_scu_trig();
}
Scu_reg ScuGp::getSCU_STATE_fine_trig_done_buf( int id )
{
	return mSubItems[id]->getSCU_STATE_fine_trig_done_buf();
}
Scu_reg ScuGp::getSCU_STATE_avg_proc_done( int id )
{
	return mSubItems[id]->getSCU_STATE_avg_proc_done();
}
Scu_reg ScuGp::getSCU_STATE_sys_avg_proc( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_avg_proc();
}
Scu_reg ScuGp::getSCU_STATE_sys_avg_exp( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_avg_exp();
}
Scu_reg ScuGp::getSCU_STATE_sys_wave_rd_en( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_wave_rd_en();
}
Scu_reg ScuGp::getSCU_STATE_sys_wave_proc_en( int id )
{
	return mSubItems[id]->getSCU_STATE_sys_wave_proc_en();
}
Scu_reg ScuGp::getSCU_STATE_wave_mem_rd_done_hold( int id )
{
	return mSubItems[id]->getSCU_STATE_wave_mem_rd_done_hold();
}
Scu_reg ScuGp::getSCU_STATE_spu_tx_done_hold( int id )
{
	return mSubItems[id]->getSCU_STATE_spu_tx_done_hold();
}
Scu_reg ScuGp::getSCU_STATE_gt_wave_tx_done( int id )
{
	return mSubItems[id]->getSCU_STATE_gt_wave_tx_done();
}
Scu_reg ScuGp::getSCU_STATE_spu_wav_rd_meas_once_tx_done( int id )
{
	return mSubItems[id]->getSCU_STATE_spu_wav_rd_meas_once_tx_done();
}
Scu_reg ScuGp::getSCU_STATE_task_wave_tx_done_LA( int id )
{
	return mSubItems[id]->getSCU_STATE_task_wave_tx_done_LA();
}
Scu_reg ScuGp::getSCU_STATE_task_wave_tx_done_CH( int id )
{
	return mSubItems[id]->getSCU_STATE_task_wave_tx_done_CH();
}
Scu_reg ScuGp::getSCU_STATE_spu_proc_done( int id )
{
	return mSubItems[id]->getSCU_STATE_spu_proc_done();
}
Scu_reg ScuGp::getSCU_STATE_scan_roll_sa_done_mem_tx_null( int id )
{
	return mSubItems[id]->getSCU_STATE_scan_roll_sa_done_mem_tx_null();
}

Scu_reg ScuGp::getSCU_STATE( int id )
{
	return mSubItems[id]->getSCU_STATE();
}

Scu_reg ScuGp::inSCU_STATE( int id )
{
	return mSubItems[id]->inSCU_STATE();
}


void ScuGp::setCTRL_sys_stop_state(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_sys_stop_state(value);} }
	else{ mSubItems[id]->setCTRL_sys_stop_state( value); }
}
void ScuGp::setCTRL_cfg_fsm_run_stop_n(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_cfg_fsm_run_stop_n(value);} }
	else{ mSubItems[id]->setCTRL_cfg_fsm_run_stop_n( value); }
}
void ScuGp::setCTRL_run_single(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_run_single(value);} }
	else{ mSubItems[id]->setCTRL_run_single( value); }
}
void ScuGp::setCTRL_mode_play_last(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_mode_play_last(value);} }
	else{ mSubItems[id]->setCTRL_mode_play_last( value); }
}
void ScuGp::setCTRL_scu_fsm_run(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_scu_fsm_run(value);} }
	else{ mSubItems[id]->setCTRL_scu_fsm_run( value); }
}
void ScuGp::setCTRL_force_trig(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_force_trig(value);} }
	else{ mSubItems[id]->setCTRL_force_trig( value); }
}
void ScuGp::setCTRL_wpu_plot_display_finish(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_wpu_plot_display_finish(value);} }
	else{ mSubItems[id]->setCTRL_wpu_plot_display_finish( value); }
}
void ScuGp::setCTRL_soft_reset_gt(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_soft_reset_gt(value);} }
	else{ mSubItems[id]->setCTRL_soft_reset_gt( value); }
}
void ScuGp::setCTRL_ms_sync(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_ms_sync(value);} }
	else{ mSubItems[id]->setCTRL_ms_sync( value); }
}
void ScuGp::setCTRL_fsm_reset(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL_fsm_reset(value);} }
	else{ mSubItems[id]->setCTRL_fsm_reset( value); }
}

void ScuGp::setCTRL( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCTRL(value);} }
	else{ mSubItems[id]->setCTRL( value); }
}

void ScuGp::outCTRL_sys_stop_state( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_sys_stop_state(value);}}
	else{ mSubItems[id]->outCTRL_sys_stop_state( value); }
}
void ScuGp::pulseCTRL_sys_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_sys_stop_state(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_sys_stop_state(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_sys_stop_state( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_cfg_fsm_run_stop_n( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_cfg_fsm_run_stop_n(value);}}
	else{ mSubItems[id]->outCTRL_cfg_fsm_run_stop_n( value); }
}
void ScuGp::pulseCTRL_cfg_fsm_run_stop_n( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_cfg_fsm_run_stop_n(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_cfg_fsm_run_stop_n(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_cfg_fsm_run_stop_n( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_run_single( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_run_single(value);}}
	else{ mSubItems[id]->outCTRL_run_single( value); }
}
void ScuGp::pulseCTRL_run_single( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_run_single(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_run_single(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_run_single( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_mode_play_last( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_mode_play_last(value);}}
	else{ mSubItems[id]->outCTRL_mode_play_last( value); }
}
void ScuGp::pulseCTRL_mode_play_last( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_mode_play_last(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_mode_play_last(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_mode_play_last( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_scu_fsm_run( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_scu_fsm_run(value);}}
	else{ mSubItems[id]->outCTRL_scu_fsm_run( value); }
}
void ScuGp::pulseCTRL_scu_fsm_run( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_scu_fsm_run(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_scu_fsm_run(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_scu_fsm_run( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_force_trig( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_force_trig(value);}}
	else{ mSubItems[id]->outCTRL_force_trig( value); }
}
void ScuGp::pulseCTRL_force_trig( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_force_trig(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_force_trig(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_force_trig( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_wpu_plot_display_finish( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_wpu_plot_display_finish(value);}}
	else{ mSubItems[id]->outCTRL_wpu_plot_display_finish( value); }
}
void ScuGp::pulseCTRL_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_wpu_plot_display_finish(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_wpu_plot_display_finish(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_wpu_plot_display_finish( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_soft_reset_gt( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_soft_reset_gt(value);}}
	else{ mSubItems[id]->outCTRL_soft_reset_gt( value); }
}
void ScuGp::pulseCTRL_soft_reset_gt( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_soft_reset_gt(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_soft_reset_gt(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_soft_reset_gt( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_ms_sync( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_ms_sync(value);}}
	else{ mSubItems[id]->outCTRL_ms_sync( value); }
}
void ScuGp::pulseCTRL_ms_sync( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_ms_sync(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_ms_sync(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_ms_sync( activeV, idleV, timeus ); }
}
void ScuGp::outCTRL_fsm_reset( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_fsm_reset(value);}}
	else{ mSubItems[id]->outCTRL_fsm_reset( value); }
}
void ScuGp::pulseCTRL_fsm_reset( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL_fsm_reset(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignCTRL_fsm_reset(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_fsm_reset( activeV, idleV, timeus ); }
}

void ScuGp::outCTRL(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL(value);} }
	else{ mSubItems[id]->outCTRL( value); }
}

void ScuGp::outCTRL( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCTRL();} }
	else{ mSubItems[id]->outCTRL(); }
}


Scu_reg ScuGp::getCTRL_sys_stop_state( int id )
{
	return mSubItems[id]->getCTRL_sys_stop_state();
}
Scu_reg ScuGp::getCTRL_cfg_fsm_run_stop_n( int id )
{
	return mSubItems[id]->getCTRL_cfg_fsm_run_stop_n();
}
Scu_reg ScuGp::getCTRL_run_single( int id )
{
	return mSubItems[id]->getCTRL_run_single();
}
Scu_reg ScuGp::getCTRL_mode_play_last( int id )
{
	return mSubItems[id]->getCTRL_mode_play_last();
}
Scu_reg ScuGp::getCTRL_scu_fsm_run( int id )
{
	return mSubItems[id]->getCTRL_scu_fsm_run();
}
Scu_reg ScuGp::getCTRL_force_trig( int id )
{
	return mSubItems[id]->getCTRL_force_trig();
}
Scu_reg ScuGp::getCTRL_wpu_plot_display_finish( int id )
{
	return mSubItems[id]->getCTRL_wpu_plot_display_finish();
}
Scu_reg ScuGp::getCTRL_soft_reset_gt( int id )
{
	return mSubItems[id]->getCTRL_soft_reset_gt();
}
Scu_reg ScuGp::getCTRL_ms_sync( int id )
{
	return mSubItems[id]->getCTRL_ms_sync();
}
Scu_reg ScuGp::getCTRL_fsm_reset( int id )
{
	return mSubItems[id]->getCTRL_fsm_reset();
}

Scu_reg ScuGp::getCTRL( int id )
{
	return mSubItems[id]->getCTRL();
}

Scu_reg ScuGp::inCTRL( int id )
{
	return mSubItems[id]->inCTRL();
}


void ScuGp::setMODE_cfg_mode_scan(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_cfg_mode_scan(value);} }
	else{ mSubItems[id]->setMODE_cfg_mode_scan( value); }
}
void ScuGp::setMODE_sys_mode_scan_trace(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_sys_mode_scan_trace(value);} }
	else{ mSubItems[id]->setMODE_sys_mode_scan_trace( value); }
}
void ScuGp::setMODE_sys_mode_scan_zoom(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_sys_mode_scan_zoom(value);} }
	else{ mSubItems[id]->setMODE_sys_mode_scan_zoom( value); }
}
void ScuGp::setMODE_mode_scan_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mode_scan_en(value);} }
	else{ mSubItems[id]->setMODE_mode_scan_en( value); }
}
void ScuGp::setMODE_search_en_zoom(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_search_en_zoom(value);} }
	else{ mSubItems[id]->setMODE_search_en_zoom( value); }
}
void ScuGp::setMODE_search_en_ch(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_search_en_ch(value);} }
	else{ mSubItems[id]->setMODE_search_en_ch( value); }
}
void ScuGp::setMODE_search_en_la(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_search_en_la(value);} }
	else{ mSubItems[id]->setMODE_search_en_la( value); }
}
void ScuGp::setMODE_mode_import_type(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mode_import_type(value);} }
	else{ mSubItems[id]->setMODE_mode_import_type( value); }
}
void ScuGp::setMODE_mode_export(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mode_export(value);} }
	else{ mSubItems[id]->setMODE_mode_export( value); }
}
void ScuGp::setMODE_mode_import_play(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mode_import_play(value);} }
	else{ mSubItems[id]->setMODE_mode_import_play( value); }
}
void ScuGp::setMODE_mode_import_rec(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mode_import_rec(value);} }
	else{ mSubItems[id]->setMODE_mode_import_rec( value); }
}
void ScuGp::setMODE_measure_en_zoom(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_measure_en_zoom(value);} }
	else{ mSubItems[id]->setMODE_measure_en_zoom( value); }
}
void ScuGp::setMODE_measure_en_ch(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_measure_en_ch(value);} }
	else{ mSubItems[id]->setMODE_measure_en_ch( value); }
}
void ScuGp::setMODE_measure_en_la(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_measure_en_la(value);} }
	else{ mSubItems[id]->setMODE_measure_en_la( value); }
}
void ScuGp::setMODE_wave_ch_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_wave_ch_en(value);} }
	else{ mSubItems[id]->setMODE_wave_ch_en( value); }
}
void ScuGp::setMODE_wave_la_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_wave_la_en(value);} }
	else{ mSubItems[id]->setMODE_wave_la_en( value); }
}
void ScuGp::setMODE_zoom_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_zoom_en(value);} }
	else{ mSubItems[id]->setMODE_zoom_en( value); }
}
void ScuGp::setMODE_mask_err_stop_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mask_err_stop_en(value);} }
	else{ mSubItems[id]->setMODE_mask_err_stop_en( value); }
}
void ScuGp::setMODE_mask_save_fail(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mask_save_fail(value);} }
	else{ mSubItems[id]->setMODE_mask_save_fail( value); }
}
void ScuGp::setMODE_mask_save_pass(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mask_save_pass(value);} }
	else{ mSubItems[id]->setMODE_mask_save_pass( value); }
}
void ScuGp::setMODE_mask_pf_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mask_pf_en(value);} }
	else{ mSubItems[id]->setMODE_mask_pf_en( value); }
}
void ScuGp::setMODE_zone_trig_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_zone_trig_en(value);} }
	else{ mSubItems[id]->setMODE_zone_trig_en( value); }
}
void ScuGp::setMODE_mode_roll_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mode_roll_en(value);} }
	else{ mSubItems[id]->setMODE_mode_roll_en( value); }
}
void ScuGp::setMODE_mode_fast_ref(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_mode_fast_ref(value);} }
	else{ mSubItems[id]->setMODE_mode_fast_ref( value); }
}
void ScuGp::setMODE_auto_trig(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_auto_trig(value);} }
	else{ mSubItems[id]->setMODE_auto_trig( value); }
}
void ScuGp::setMODE_interleave_20G(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_interleave_20G(value);} }
	else{ mSubItems[id]->setMODE_interleave_20G( value); }
}
void ScuGp::setMODE_en_20G(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_en_20G(value);} }
	else{ mSubItems[id]->setMODE_en_20G( value); }
}

void ScuGp::setMODE( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE(value);} }
	else{ mSubItems[id]->setMODE( value); }
}

void ScuGp::outMODE_cfg_mode_scan( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_cfg_mode_scan(value);}}
	else{ mSubItems[id]->outMODE_cfg_mode_scan( value); }
}
void ScuGp::pulseMODE_cfg_mode_scan( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_cfg_mode_scan(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_cfg_mode_scan(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_cfg_mode_scan( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_sys_mode_scan_trace( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_sys_mode_scan_trace(value);}}
	else{ mSubItems[id]->outMODE_sys_mode_scan_trace( value); }
}
void ScuGp::pulseMODE_sys_mode_scan_trace( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_sys_mode_scan_trace(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_sys_mode_scan_trace(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_sys_mode_scan_trace( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_sys_mode_scan_zoom( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_sys_mode_scan_zoom(value);}}
	else{ mSubItems[id]->outMODE_sys_mode_scan_zoom( value); }
}
void ScuGp::pulseMODE_sys_mode_scan_zoom( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_sys_mode_scan_zoom(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_sys_mode_scan_zoom(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_sys_mode_scan_zoom( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mode_scan_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_scan_en(value);}}
	else{ mSubItems[id]->outMODE_mode_scan_en( value); }
}
void ScuGp::pulseMODE_mode_scan_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_scan_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mode_scan_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mode_scan_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_search_en_zoom( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_search_en_zoom(value);}}
	else{ mSubItems[id]->outMODE_search_en_zoom( value); }
}
void ScuGp::pulseMODE_search_en_zoom( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_search_en_zoom(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_search_en_zoom(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_search_en_zoom( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_search_en_ch( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_search_en_ch(value);}}
	else{ mSubItems[id]->outMODE_search_en_ch( value); }
}
void ScuGp::pulseMODE_search_en_ch( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_search_en_ch(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_search_en_ch(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_search_en_ch( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_search_en_la( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_search_en_la(value);}}
	else{ mSubItems[id]->outMODE_search_en_la( value); }
}
void ScuGp::pulseMODE_search_en_la( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_search_en_la(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_search_en_la(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_search_en_la( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mode_import_type( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_import_type(value);}}
	else{ mSubItems[id]->outMODE_mode_import_type( value); }
}
void ScuGp::pulseMODE_mode_import_type( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_import_type(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mode_import_type(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mode_import_type( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mode_export( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_export(value);}}
	else{ mSubItems[id]->outMODE_mode_export( value); }
}
void ScuGp::pulseMODE_mode_export( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_export(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mode_export(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mode_export( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mode_import_play( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_import_play(value);}}
	else{ mSubItems[id]->outMODE_mode_import_play( value); }
}
void ScuGp::pulseMODE_mode_import_play( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_import_play(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mode_import_play(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mode_import_play( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mode_import_rec( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_import_rec(value);}}
	else{ mSubItems[id]->outMODE_mode_import_rec( value); }
}
void ScuGp::pulseMODE_mode_import_rec( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_import_rec(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mode_import_rec(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mode_import_rec( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_measure_en_zoom( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_measure_en_zoom(value);}}
	else{ mSubItems[id]->outMODE_measure_en_zoom( value); }
}
void ScuGp::pulseMODE_measure_en_zoom( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_measure_en_zoom(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_measure_en_zoom(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_measure_en_zoom( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_measure_en_ch( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_measure_en_ch(value);}}
	else{ mSubItems[id]->outMODE_measure_en_ch( value); }
}
void ScuGp::pulseMODE_measure_en_ch( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_measure_en_ch(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_measure_en_ch(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_measure_en_ch( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_measure_en_la( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_measure_en_la(value);}}
	else{ mSubItems[id]->outMODE_measure_en_la( value); }
}
void ScuGp::pulseMODE_measure_en_la( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_measure_en_la(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_measure_en_la(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_measure_en_la( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_wave_ch_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_wave_ch_en(value);}}
	else{ mSubItems[id]->outMODE_wave_ch_en( value); }
}
void ScuGp::pulseMODE_wave_ch_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_wave_ch_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_wave_ch_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_wave_ch_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_wave_la_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_wave_la_en(value);}}
	else{ mSubItems[id]->outMODE_wave_la_en( value); }
}
void ScuGp::pulseMODE_wave_la_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_wave_la_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_wave_la_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_wave_la_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_zoom_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_zoom_en(value);}}
	else{ mSubItems[id]->outMODE_zoom_en( value); }
}
void ScuGp::pulseMODE_zoom_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_zoom_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_zoom_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_zoom_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mask_err_stop_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_err_stop_en(value);}}
	else{ mSubItems[id]->outMODE_mask_err_stop_en( value); }
}
void ScuGp::pulseMODE_mask_err_stop_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_err_stop_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mask_err_stop_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mask_err_stop_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mask_save_fail( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_save_fail(value);}}
	else{ mSubItems[id]->outMODE_mask_save_fail( value); }
}
void ScuGp::pulseMODE_mask_save_fail( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_save_fail(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mask_save_fail(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mask_save_fail( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mask_save_pass( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_save_pass(value);}}
	else{ mSubItems[id]->outMODE_mask_save_pass( value); }
}
void ScuGp::pulseMODE_mask_save_pass( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_save_pass(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mask_save_pass(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mask_save_pass( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mask_pf_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_pf_en(value);}}
	else{ mSubItems[id]->outMODE_mask_pf_en( value); }
}
void ScuGp::pulseMODE_mask_pf_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mask_pf_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mask_pf_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mask_pf_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_zone_trig_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_zone_trig_en(value);}}
	else{ mSubItems[id]->outMODE_zone_trig_en( value); }
}
void ScuGp::pulseMODE_zone_trig_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_zone_trig_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_zone_trig_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_zone_trig_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mode_roll_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_roll_en(value);}}
	else{ mSubItems[id]->outMODE_mode_roll_en( value); }
}
void ScuGp::pulseMODE_mode_roll_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_roll_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mode_roll_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mode_roll_en( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_mode_fast_ref( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_fast_ref(value);}}
	else{ mSubItems[id]->outMODE_mode_fast_ref( value); }
}
void ScuGp::pulseMODE_mode_fast_ref( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_mode_fast_ref(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_mode_fast_ref(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_mode_fast_ref( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_auto_trig( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_auto_trig(value);}}
	else{ mSubItems[id]->outMODE_auto_trig( value); }
}
void ScuGp::pulseMODE_auto_trig( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_auto_trig(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_auto_trig(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_auto_trig( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_interleave_20G( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_interleave_20G(value);}}
	else{ mSubItems[id]->outMODE_interleave_20G( value); }
}
void ScuGp::pulseMODE_interleave_20G( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_interleave_20G(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_interleave_20G(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_interleave_20G( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_en_20G( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_en_20G(value);}}
	else{ mSubItems[id]->outMODE_en_20G( value); }
}
void ScuGp::pulseMODE_en_20G( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_en_20G(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_en_20G(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_en_20G( activeV, idleV, timeus ); }
}

void ScuGp::outMODE(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE(value);} }
	else{ mSubItems[id]->outMODE( value); }
}

void ScuGp::outMODE( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE();} }
	else{ mSubItems[id]->outMODE(); }
}


Scu_reg ScuGp::getMODE_cfg_mode_scan( int id )
{
	return mSubItems[id]->getMODE_cfg_mode_scan();
}
Scu_reg ScuGp::getMODE_sys_mode_scan_trace( int id )
{
	return mSubItems[id]->getMODE_sys_mode_scan_trace();
}
Scu_reg ScuGp::getMODE_sys_mode_scan_zoom( int id )
{
	return mSubItems[id]->getMODE_sys_mode_scan_zoom();
}
Scu_reg ScuGp::getMODE_mode_scan_en( int id )
{
	return mSubItems[id]->getMODE_mode_scan_en();
}
Scu_reg ScuGp::getMODE_search_en_zoom( int id )
{
	return mSubItems[id]->getMODE_search_en_zoom();
}
Scu_reg ScuGp::getMODE_search_en_ch( int id )
{
	return mSubItems[id]->getMODE_search_en_ch();
}
Scu_reg ScuGp::getMODE_search_en_la( int id )
{
	return mSubItems[id]->getMODE_search_en_la();
}
Scu_reg ScuGp::getMODE_mode_import_type( int id )
{
	return mSubItems[id]->getMODE_mode_import_type();
}
Scu_reg ScuGp::getMODE_mode_export( int id )
{
	return mSubItems[id]->getMODE_mode_export();
}
Scu_reg ScuGp::getMODE_mode_import_play( int id )
{
	return mSubItems[id]->getMODE_mode_import_play();
}
Scu_reg ScuGp::getMODE_mode_import_rec( int id )
{
	return mSubItems[id]->getMODE_mode_import_rec();
}
Scu_reg ScuGp::getMODE_measure_en_zoom( int id )
{
	return mSubItems[id]->getMODE_measure_en_zoom();
}
Scu_reg ScuGp::getMODE_measure_en_ch( int id )
{
	return mSubItems[id]->getMODE_measure_en_ch();
}
Scu_reg ScuGp::getMODE_measure_en_la( int id )
{
	return mSubItems[id]->getMODE_measure_en_la();
}
Scu_reg ScuGp::getMODE_wave_ch_en( int id )
{
	return mSubItems[id]->getMODE_wave_ch_en();
}
Scu_reg ScuGp::getMODE_wave_la_en( int id )
{
	return mSubItems[id]->getMODE_wave_la_en();
}
Scu_reg ScuGp::getMODE_zoom_en( int id )
{
	return mSubItems[id]->getMODE_zoom_en();
}
Scu_reg ScuGp::getMODE_mask_err_stop_en( int id )
{
	return mSubItems[id]->getMODE_mask_err_stop_en();
}
Scu_reg ScuGp::getMODE_mask_save_fail( int id )
{
	return mSubItems[id]->getMODE_mask_save_fail();
}
Scu_reg ScuGp::getMODE_mask_save_pass( int id )
{
	return mSubItems[id]->getMODE_mask_save_pass();
}
Scu_reg ScuGp::getMODE_mask_pf_en( int id )
{
	return mSubItems[id]->getMODE_mask_pf_en();
}
Scu_reg ScuGp::getMODE_zone_trig_en( int id )
{
	return mSubItems[id]->getMODE_zone_trig_en();
}
Scu_reg ScuGp::getMODE_mode_roll_en( int id )
{
	return mSubItems[id]->getMODE_mode_roll_en();
}
Scu_reg ScuGp::getMODE_mode_fast_ref( int id )
{
	return mSubItems[id]->getMODE_mode_fast_ref();
}
Scu_reg ScuGp::getMODE_auto_trig( int id )
{
	return mSubItems[id]->getMODE_auto_trig();
}
Scu_reg ScuGp::getMODE_interleave_20G( int id )
{
	return mSubItems[id]->getMODE_interleave_20G();
}
Scu_reg ScuGp::getMODE_en_20G( int id )
{
	return mSubItems[id]->getMODE_en_20G();
}

Scu_reg ScuGp::getMODE( int id )
{
	return mSubItems[id]->getMODE();
}

Scu_reg ScuGp::inMODE( int id )
{
	return mSubItems[id]->inMODE();
}


void ScuGp::setMODE_10G_chn_10G(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_10G_chn_10G(value);} }
	else{ mSubItems[id]->setMODE_10G_chn_10G( value); }
}
void ScuGp::setMODE_10G_cfg_chn(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_10G_cfg_chn(value);} }
	else{ mSubItems[id]->setMODE_10G_cfg_chn( value); }
}

void ScuGp::setMODE_10G( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMODE_10G(value);} }
	else{ mSubItems[id]->setMODE_10G( value); }
}

void ScuGp::outMODE_10G_chn_10G( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_10G_chn_10G(value);}}
	else{ mSubItems[id]->outMODE_10G_chn_10G( value); }
}
void ScuGp::pulseMODE_10G_chn_10G( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_10G_chn_10G(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_10G_chn_10G(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_10G_chn_10G( activeV, idleV, timeus ); }
}
void ScuGp::outMODE_10G_cfg_chn( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_10G_cfg_chn(value);}}
	else{ mSubItems[id]->outMODE_10G_cfg_chn( value); }
}
void ScuGp::pulseMODE_10G_cfg_chn( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_10G_cfg_chn(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMODE_10G_cfg_chn(idleV);}
	}
	else
	{ mSubItems[id]->pulseMODE_10G_cfg_chn( activeV, idleV, timeus ); }
}

void ScuGp::outMODE_10G(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_10G(value);} }
	else{ mSubItems[id]->outMODE_10G( value); }
}

void ScuGp::outMODE_10G( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMODE_10G();} }
	else{ mSubItems[id]->outMODE_10G(); }
}


Scu_reg ScuGp::getMODE_10G_chn_10G( int id )
{
	return mSubItems[id]->getMODE_10G_chn_10G();
}
Scu_reg ScuGp::getMODE_10G_cfg_chn( int id )
{
	return mSubItems[id]->getMODE_10G_cfg_chn();
}

Scu_reg ScuGp::getMODE_10G( int id )
{
	return mSubItems[id]->getMODE_10G();
}

Scu_reg ScuGp::inMODE_10G( int id )
{
	return mSubItems[id]->inMODE_10G();
}


void ScuGp::setPRE_SA( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setPRE_SA(value);} }
	else{ mSubItems[id]->setPRE_SA( value); }
}

void ScuGp::outPRE_SA( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPRE_SA(value);} }
	else{ mSubItems[id]->outPRE_SA( value); }
}

void ScuGp::outPRE_SA( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPRE_SA();} }
	else{ mSubItems[id]->outPRE_SA(); }
}


Scu_reg ScuGp::getPRE_SA( int id )
{
	return mSubItems[id]->getPRE_SA();
}

Scu_reg ScuGp::inPRE_SA( int id )
{
	return mSubItems[id]->inPRE_SA();
}


void ScuGp::setPOS_SA( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setPOS_SA(value);} }
	else{ mSubItems[id]->setPOS_SA( value); }
}

void ScuGp::outPOS_SA( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPOS_SA(value);} }
	else{ mSubItems[id]->outPOS_SA( value); }
}

void ScuGp::outPOS_SA( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPOS_SA();} }
	else{ mSubItems[id]->outPOS_SA(); }
}


Scu_reg ScuGp::getPOS_SA( int id )
{
	return mSubItems[id]->getPOS_SA();
}

Scu_reg ScuGp::inPOS_SA( int id )
{
	return mSubItems[id]->inPOS_SA();
}


void ScuGp::setSCAN_TRIG_POSITION_position(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCAN_TRIG_POSITION_position(value);} }
	else{ mSubItems[id]->setSCAN_TRIG_POSITION_position( value); }
}
void ScuGp::setSCAN_TRIG_POSITION_trig_left_side(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCAN_TRIG_POSITION_trig_left_side(value);} }
	else{ mSubItems[id]->setSCAN_TRIG_POSITION_trig_left_side( value); }
}

void ScuGp::setSCAN_TRIG_POSITION( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSCAN_TRIG_POSITION(value);} }
	else{ mSubItems[id]->setSCAN_TRIG_POSITION( value); }
}

void ScuGp::outSCAN_TRIG_POSITION_position( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCAN_TRIG_POSITION_position(value);}}
	else{ mSubItems[id]->outSCAN_TRIG_POSITION_position( value); }
}
void ScuGp::pulseSCAN_TRIG_POSITION_position( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCAN_TRIG_POSITION_position(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCAN_TRIG_POSITION_position(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCAN_TRIG_POSITION_position( activeV, idleV, timeus ); }
}
void ScuGp::outSCAN_TRIG_POSITION_trig_left_side( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCAN_TRIG_POSITION_trig_left_side(value);}}
	else{ mSubItems[id]->outSCAN_TRIG_POSITION_trig_left_side( value); }
}
void ScuGp::pulseSCAN_TRIG_POSITION_trig_left_side( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSCAN_TRIG_POSITION_trig_left_side(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSCAN_TRIG_POSITION_trig_left_side(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCAN_TRIG_POSITION_trig_left_side( activeV, idleV, timeus ); }
}

void ScuGp::outSCAN_TRIG_POSITION(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCAN_TRIG_POSITION(value);} }
	else{ mSubItems[id]->outSCAN_TRIG_POSITION( value); }
}

void ScuGp::outSCAN_TRIG_POSITION( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSCAN_TRIG_POSITION();} }
	else{ mSubItems[id]->outSCAN_TRIG_POSITION(); }
}


Scu_reg ScuGp::getSCAN_TRIG_POSITION_position( int id )
{
	return mSubItems[id]->getSCAN_TRIG_POSITION_position();
}
Scu_reg ScuGp::getSCAN_TRIG_POSITION_trig_left_side( int id )
{
	return mSubItems[id]->getSCAN_TRIG_POSITION_trig_left_side();
}

Scu_reg ScuGp::getSCAN_TRIG_POSITION( int id )
{
	return mSubItems[id]->getSCAN_TRIG_POSITION();
}

Scu_reg ScuGp::inSCAN_TRIG_POSITION( int id )
{
	return mSubItems[id]->inSCAN_TRIG_POSITION();
}


void ScuGp::setAUTO_TRIG_TIMEOUT( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setAUTO_TRIG_TIMEOUT(value);} }
	else{ mSubItems[id]->setAUTO_TRIG_TIMEOUT( value); }
}

void ScuGp::outAUTO_TRIG_TIMEOUT( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outAUTO_TRIG_TIMEOUT(value);} }
	else{ mSubItems[id]->outAUTO_TRIG_TIMEOUT( value); }
}

void ScuGp::outAUTO_TRIG_TIMEOUT( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outAUTO_TRIG_TIMEOUT();} }
	else{ mSubItems[id]->outAUTO_TRIG_TIMEOUT(); }
}


Scu_reg ScuGp::getAUTO_TRIG_TIMEOUT( int id )
{
	return mSubItems[id]->getAUTO_TRIG_TIMEOUT();
}

Scu_reg ScuGp::inAUTO_TRIG_TIMEOUT( int id )
{
	return mSubItems[id]->inAUTO_TRIG_TIMEOUT();
}


void ScuGp::setAUTO_TRIG_HOLD_OFF( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setAUTO_TRIG_HOLD_OFF(value);} }
	else{ mSubItems[id]->setAUTO_TRIG_HOLD_OFF( value); }
}

void ScuGp::outAUTO_TRIG_HOLD_OFF( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outAUTO_TRIG_HOLD_OFF(value);} }
	else{ mSubItems[id]->outAUTO_TRIG_HOLD_OFF( value); }
}

void ScuGp::outAUTO_TRIG_HOLD_OFF( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outAUTO_TRIG_HOLD_OFF();} }
	else{ mSubItems[id]->outAUTO_TRIG_HOLD_OFF(); }
}


Scu_reg ScuGp::getAUTO_TRIG_HOLD_OFF( int id )
{
	return mSubItems[id]->getAUTO_TRIG_HOLD_OFF();
}

Scu_reg ScuGp::inAUTO_TRIG_HOLD_OFF( int id )
{
	return mSubItems[id]->inAUTO_TRIG_HOLD_OFF();
}


void ScuGp::setFSM_DELAY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setFSM_DELAY(value);} }
	else{ mSubItems[id]->setFSM_DELAY( value); }
}

void ScuGp::outFSM_DELAY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFSM_DELAY(value);} }
	else{ mSubItems[id]->outFSM_DELAY( value); }
}

void ScuGp::outFSM_DELAY( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFSM_DELAY();} }
	else{ mSubItems[id]->outFSM_DELAY(); }
}


Scu_reg ScuGp::getFSM_DELAY( int id )
{
	return mSubItems[id]->getFSM_DELAY();
}

Scu_reg ScuGp::inFSM_DELAY( int id )
{
	return mSubItems[id]->inFSM_DELAY();
}


void ScuGp::setPLOT_DELAY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setPLOT_DELAY(value);} }
	else{ mSubItems[id]->setPLOT_DELAY( value); }
}

void ScuGp::outPLOT_DELAY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPLOT_DELAY(value);} }
	else{ mSubItems[id]->outPLOT_DELAY( value); }
}

void ScuGp::outPLOT_DELAY( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPLOT_DELAY();} }
	else{ mSubItems[id]->outPLOT_DELAY(); }
}


Scu_reg ScuGp::getPLOT_DELAY( int id )
{
	return mSubItems[id]->getPLOT_DELAY();
}

Scu_reg ScuGp::inPLOT_DELAY( int id )
{
	return mSubItems[id]->inPLOT_DELAY();
}


void ScuGp::setFRAME_PLOT_NUM( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setFRAME_PLOT_NUM(value);} }
	else{ mSubItems[id]->setFRAME_PLOT_NUM( value); }
}

void ScuGp::outFRAME_PLOT_NUM( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFRAME_PLOT_NUM(value);} }
	else{ mSubItems[id]->outFRAME_PLOT_NUM( value); }
}

void ScuGp::outFRAME_PLOT_NUM( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFRAME_PLOT_NUM();} }
	else{ mSubItems[id]->outFRAME_PLOT_NUM(); }
}


Scu_reg ScuGp::getFRAME_PLOT_NUM( int id )
{
	return mSubItems[id]->getFRAME_PLOT_NUM();
}

Scu_reg ScuGp::inFRAME_PLOT_NUM( int id )
{
	return mSubItems[id]->inFRAME_PLOT_NUM();
}


void ScuGp::setREC_PLAY_index_full(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setREC_PLAY_index_full(value);} }
	else{ mSubItems[id]->setREC_PLAY_index_full( value); }
}
void ScuGp::setREC_PLAY_loop_playback(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setREC_PLAY_loop_playback(value);} }
	else{ mSubItems[id]->setREC_PLAY_loop_playback( value); }
}
void ScuGp::setREC_PLAY_index_load(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setREC_PLAY_index_load(value);} }
	else{ mSubItems[id]->setREC_PLAY_index_load( value); }
}
void ScuGp::setREC_PLAY_index_inc(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setREC_PLAY_index_inc(value);} }
	else{ mSubItems[id]->setREC_PLAY_index_inc( value); }
}
void ScuGp::setREC_PLAY_mode_play(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setREC_PLAY_mode_play(value);} }
	else{ mSubItems[id]->setREC_PLAY_mode_play( value); }
}
void ScuGp::setREC_PLAY_mode_rec(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setREC_PLAY_mode_rec(value);} }
	else{ mSubItems[id]->setREC_PLAY_mode_rec( value); }
}

void ScuGp::setREC_PLAY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setREC_PLAY(value);} }
	else{ mSubItems[id]->setREC_PLAY( value); }
}

void ScuGp::outREC_PLAY_index_full( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_index_full(value);}}
	else{ mSubItems[id]->outREC_PLAY_index_full( value); }
}
void ScuGp::pulseREC_PLAY_index_full( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_index_full(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignREC_PLAY_index_full(idleV);}
	}
	else
	{ mSubItems[id]->pulseREC_PLAY_index_full( activeV, idleV, timeus ); }
}
void ScuGp::outREC_PLAY_loop_playback( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_loop_playback(value);}}
	else{ mSubItems[id]->outREC_PLAY_loop_playback( value); }
}
void ScuGp::pulseREC_PLAY_loop_playback( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_loop_playback(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignREC_PLAY_loop_playback(idleV);}
	}
	else
	{ mSubItems[id]->pulseREC_PLAY_loop_playback( activeV, idleV, timeus ); }
}
void ScuGp::outREC_PLAY_index_load( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_index_load(value);}}
	else{ mSubItems[id]->outREC_PLAY_index_load( value); }
}
void ScuGp::pulseREC_PLAY_index_load( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_index_load(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignREC_PLAY_index_load(idleV);}
	}
	else
	{ mSubItems[id]->pulseREC_PLAY_index_load( activeV, idleV, timeus ); }
}
void ScuGp::outREC_PLAY_index_inc( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_index_inc(value);}}
	else{ mSubItems[id]->outREC_PLAY_index_inc( value); }
}
void ScuGp::pulseREC_PLAY_index_inc( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_index_inc(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignREC_PLAY_index_inc(idleV);}
	}
	else
	{ mSubItems[id]->pulseREC_PLAY_index_inc( activeV, idleV, timeus ); }
}
void ScuGp::outREC_PLAY_mode_play( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_mode_play(value);}}
	else{ mSubItems[id]->outREC_PLAY_mode_play( value); }
}
void ScuGp::pulseREC_PLAY_mode_play( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_mode_play(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignREC_PLAY_mode_play(idleV);}
	}
	else
	{ mSubItems[id]->pulseREC_PLAY_mode_play( activeV, idleV, timeus ); }
}
void ScuGp::outREC_PLAY_mode_rec( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_mode_rec(value);}}
	else{ mSubItems[id]->outREC_PLAY_mode_rec( value); }
}
void ScuGp::pulseREC_PLAY_mode_rec( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY_mode_rec(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignREC_PLAY_mode_rec(idleV);}
	}
	else
	{ mSubItems[id]->pulseREC_PLAY_mode_rec( activeV, idleV, timeus ); }
}

void ScuGp::outREC_PLAY(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY(value);} }
	else{ mSubItems[id]->outREC_PLAY( value); }
}

void ScuGp::outREC_PLAY( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outREC_PLAY();} }
	else{ mSubItems[id]->outREC_PLAY(); }
}


Scu_reg ScuGp::getREC_PLAY_index_full( int id )
{
	return mSubItems[id]->getREC_PLAY_index_full();
}
Scu_reg ScuGp::getREC_PLAY_loop_playback( int id )
{
	return mSubItems[id]->getREC_PLAY_loop_playback();
}
Scu_reg ScuGp::getREC_PLAY_index_load( int id )
{
	return mSubItems[id]->getREC_PLAY_index_load();
}
Scu_reg ScuGp::getREC_PLAY_index_inc( int id )
{
	return mSubItems[id]->getREC_PLAY_index_inc();
}
Scu_reg ScuGp::getREC_PLAY_mode_play( int id )
{
	return mSubItems[id]->getREC_PLAY_mode_play();
}
Scu_reg ScuGp::getREC_PLAY_mode_rec( int id )
{
	return mSubItems[id]->getREC_PLAY_mode_rec();
}

Scu_reg ScuGp::getREC_PLAY( int id )
{
	return mSubItems[id]->getREC_PLAY();
}

Scu_reg ScuGp::inREC_PLAY( int id )
{
	return mSubItems[id]->inREC_PLAY();
}


void ScuGp::setWAVE_INFO_wave_plot_frame_cnt(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setWAVE_INFO_wave_plot_frame_cnt(value);} }
	else{ mSubItems[id]->setWAVE_INFO_wave_plot_frame_cnt( value); }
}
void ScuGp::setWAVE_INFO_wave_play_vld(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setWAVE_INFO_wave_play_vld(value);} }
	else{ mSubItems[id]->setWAVE_INFO_wave_play_vld( value); }
}

void ScuGp::setWAVE_INFO( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setWAVE_INFO(value);} }
	else{ mSubItems[id]->setWAVE_INFO( value); }
}

void ScuGp::outWAVE_INFO_wave_plot_frame_cnt( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outWAVE_INFO_wave_plot_frame_cnt(value);}}
	else{ mSubItems[id]->outWAVE_INFO_wave_plot_frame_cnt( value); }
}
void ScuGp::pulseWAVE_INFO_wave_plot_frame_cnt( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outWAVE_INFO_wave_plot_frame_cnt(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignWAVE_INFO_wave_plot_frame_cnt(idleV);}
	}
	else
	{ mSubItems[id]->pulseWAVE_INFO_wave_plot_frame_cnt( activeV, idleV, timeus ); }
}
void ScuGp::outWAVE_INFO_wave_play_vld( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outWAVE_INFO_wave_play_vld(value);}}
	else{ mSubItems[id]->outWAVE_INFO_wave_play_vld( value); }
}
void ScuGp::pulseWAVE_INFO_wave_play_vld( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outWAVE_INFO_wave_play_vld(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignWAVE_INFO_wave_play_vld(idleV);}
	}
	else
	{ mSubItems[id]->pulseWAVE_INFO_wave_play_vld( activeV, idleV, timeus ); }
}

void ScuGp::outWAVE_INFO(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outWAVE_INFO(value);} }
	else{ mSubItems[id]->outWAVE_INFO( value); }
}

void ScuGp::outWAVE_INFO( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outWAVE_INFO();} }
	else{ mSubItems[id]->outWAVE_INFO(); }
}


Scu_reg ScuGp::getWAVE_INFO_wave_plot_frame_cnt( int id )
{
	return mSubItems[id]->getWAVE_INFO_wave_plot_frame_cnt();
}
Scu_reg ScuGp::getWAVE_INFO_wave_play_vld( int id )
{
	return mSubItems[id]->getWAVE_INFO_wave_play_vld();
}

Scu_reg ScuGp::getWAVE_INFO( int id )
{
	return mSubItems[id]->getWAVE_INFO();
}

Scu_reg ScuGp::inWAVE_INFO( int id )
{
	return mSubItems[id]->inWAVE_INFO();
}


void ScuGp::setINDEX_CUR_index(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setINDEX_CUR_index(value);} }
	else{ mSubItems[id]->setINDEX_CUR_index( value); }
}

void ScuGp::setINDEX_CUR( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setINDEX_CUR(value);} }
	else{ mSubItems[id]->setINDEX_CUR( value); }
}

void ScuGp::outINDEX_CUR_index( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_CUR_index(value);}}
	else{ mSubItems[id]->outINDEX_CUR_index( value); }
}
void ScuGp::pulseINDEX_CUR_index( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_CUR_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignINDEX_CUR_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseINDEX_CUR_index( activeV, idleV, timeus ); }
}

void ScuGp::outINDEX_CUR(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_CUR(value);} }
	else{ mSubItems[id]->outINDEX_CUR( value); }
}

void ScuGp::outINDEX_CUR( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_CUR();} }
	else{ mSubItems[id]->outINDEX_CUR(); }
}


Scu_reg ScuGp::getINDEX_CUR_index( int id )
{
	return mSubItems[id]->getINDEX_CUR_index();
}

Scu_reg ScuGp::getINDEX_CUR( int id )
{
	return mSubItems[id]->getINDEX_CUR();
}

Scu_reg ScuGp::inINDEX_CUR( int id )
{
	return mSubItems[id]->inINDEX_CUR();
}


void ScuGp::setINDEX_BASE_index(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setINDEX_BASE_index(value);} }
	else{ mSubItems[id]->setINDEX_BASE_index( value); }
}

void ScuGp::setINDEX_BASE( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setINDEX_BASE(value);} }
	else{ mSubItems[id]->setINDEX_BASE( value); }
}

void ScuGp::outINDEX_BASE_index( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_BASE_index(value);}}
	else{ mSubItems[id]->outINDEX_BASE_index( value); }
}
void ScuGp::pulseINDEX_BASE_index( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_BASE_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignINDEX_BASE_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseINDEX_BASE_index( activeV, idleV, timeus ); }
}

void ScuGp::outINDEX_BASE(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_BASE(value);} }
	else{ mSubItems[id]->outINDEX_BASE( value); }
}

void ScuGp::outINDEX_BASE( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_BASE();} }
	else{ mSubItems[id]->outINDEX_BASE(); }
}


Scu_reg ScuGp::getINDEX_BASE_index( int id )
{
	return mSubItems[id]->getINDEX_BASE_index();
}

Scu_reg ScuGp::getINDEX_BASE( int id )
{
	return mSubItems[id]->getINDEX_BASE();
}

Scu_reg ScuGp::inINDEX_BASE( int id )
{
	return mSubItems[id]->inINDEX_BASE();
}


void ScuGp::setINDEX_UPPER_index(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setINDEX_UPPER_index(value);} }
	else{ mSubItems[id]->setINDEX_UPPER_index( value); }
}

void ScuGp::setINDEX_UPPER( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setINDEX_UPPER(value);} }
	else{ mSubItems[id]->setINDEX_UPPER( value); }
}

void ScuGp::outINDEX_UPPER_index( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_UPPER_index(value);}}
	else{ mSubItems[id]->outINDEX_UPPER_index( value); }
}
void ScuGp::pulseINDEX_UPPER_index( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_UPPER_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignINDEX_UPPER_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseINDEX_UPPER_index( activeV, idleV, timeus ); }
}

void ScuGp::outINDEX_UPPER(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_UPPER(value);} }
	else{ mSubItems[id]->outINDEX_UPPER( value); }
}

void ScuGp::outINDEX_UPPER( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outINDEX_UPPER();} }
	else{ mSubItems[id]->outINDEX_UPPER(); }
}


Scu_reg ScuGp::getINDEX_UPPER_index( int id )
{
	return mSubItems[id]->getINDEX_UPPER_index();
}

Scu_reg ScuGp::getINDEX_UPPER( int id )
{
	return mSubItems[id]->getINDEX_UPPER();
}

Scu_reg ScuGp::inINDEX_UPPER( int id )
{
	return mSubItems[id]->inINDEX_UPPER();
}


void ScuGp::setTRACE_trace_once_req(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE_trace_once_req(value);} }
	else{ mSubItems[id]->setTRACE_trace_once_req( value); }
}
void ScuGp::setTRACE_measure_once_req(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE_measure_once_req(value);} }
	else{ mSubItems[id]->setTRACE_measure_once_req( value); }
}
void ScuGp::setTRACE_search_once_req(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE_search_once_req(value);} }
	else{ mSubItems[id]->setTRACE_search_once_req( value); }
}
void ScuGp::setTRACE_eye_once_req(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE_eye_once_req(value);} }
	else{ mSubItems[id]->setTRACE_eye_once_req( value); }
}
void ScuGp::setTRACE_wave_bypass_wpu(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE_wave_bypass_wpu(value);} }
	else{ mSubItems[id]->setTRACE_wave_bypass_wpu( value); }
}
void ScuGp::setTRACE_wave_bypass_trace(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE_wave_bypass_trace(value);} }
	else{ mSubItems[id]->setTRACE_wave_bypass_trace( value); }
}
void ScuGp::setTRACE_avg_clr(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE_avg_clr(value);} }
	else{ mSubItems[id]->setTRACE_avg_clr( value); }
}

void ScuGp::setTRACE( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRACE(value);} }
	else{ mSubItems[id]->setTRACE( value); }
}

void ScuGp::outTRACE_trace_once_req( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_trace_once_req(value);}}
	else{ mSubItems[id]->outTRACE_trace_once_req( value); }
}
void ScuGp::pulseTRACE_trace_once_req( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_trace_once_req(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignTRACE_trace_once_req(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_trace_once_req( activeV, idleV, timeus ); }
}
void ScuGp::outTRACE_measure_once_req( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_measure_once_req(value);}}
	else{ mSubItems[id]->outTRACE_measure_once_req( value); }
}
void ScuGp::pulseTRACE_measure_once_req( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_measure_once_req(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignTRACE_measure_once_req(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_measure_once_req( activeV, idleV, timeus ); }
}
void ScuGp::outTRACE_search_once_req( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_search_once_req(value);}}
	else{ mSubItems[id]->outTRACE_search_once_req( value); }
}
void ScuGp::pulseTRACE_search_once_req( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_search_once_req(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignTRACE_search_once_req(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_search_once_req( activeV, idleV, timeus ); }
}
void ScuGp::outTRACE_eye_once_req( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_eye_once_req(value);}}
	else{ mSubItems[id]->outTRACE_eye_once_req( value); }
}
void ScuGp::pulseTRACE_eye_once_req( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_eye_once_req(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignTRACE_eye_once_req(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_eye_once_req( activeV, idleV, timeus ); }
}
void ScuGp::outTRACE_wave_bypass_wpu( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_wave_bypass_wpu(value);}}
	else{ mSubItems[id]->outTRACE_wave_bypass_wpu( value); }
}
void ScuGp::pulseTRACE_wave_bypass_wpu( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_wave_bypass_wpu(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignTRACE_wave_bypass_wpu(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_wave_bypass_wpu( activeV, idleV, timeus ); }
}
void ScuGp::outTRACE_wave_bypass_trace( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_wave_bypass_trace(value);}}
	else{ mSubItems[id]->outTRACE_wave_bypass_trace( value); }
}
void ScuGp::pulseTRACE_wave_bypass_trace( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_wave_bypass_trace(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignTRACE_wave_bypass_trace(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_wave_bypass_trace( activeV, idleV, timeus ); }
}
void ScuGp::outTRACE_avg_clr( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_avg_clr(value);}}
	else{ mSubItems[id]->outTRACE_avg_clr( value); }
}
void ScuGp::pulseTRACE_avg_clr( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE_avg_clr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignTRACE_avg_clr(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_avg_clr( activeV, idleV, timeus ); }
}

void ScuGp::outTRACE(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE(value);} }
	else{ mSubItems[id]->outTRACE( value); }
}

void ScuGp::outTRACE( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRACE();} }
	else{ mSubItems[id]->outTRACE(); }
}


Scu_reg ScuGp::getTRACE_trace_once_req( int id )
{
	return mSubItems[id]->getTRACE_trace_once_req();
}
Scu_reg ScuGp::getTRACE_measure_once_req( int id )
{
	return mSubItems[id]->getTRACE_measure_once_req();
}
Scu_reg ScuGp::getTRACE_search_once_req( int id )
{
	return mSubItems[id]->getTRACE_search_once_req();
}
Scu_reg ScuGp::getTRACE_eye_once_req( int id )
{
	return mSubItems[id]->getTRACE_eye_once_req();
}
Scu_reg ScuGp::getTRACE_wave_bypass_wpu( int id )
{
	return mSubItems[id]->getTRACE_wave_bypass_wpu();
}
Scu_reg ScuGp::getTRACE_wave_bypass_trace( int id )
{
	return mSubItems[id]->getTRACE_wave_bypass_trace();
}
Scu_reg ScuGp::getTRACE_avg_clr( int id )
{
	return mSubItems[id]->getTRACE_avg_clr();
}

Scu_reg ScuGp::getTRACE( int id )
{
	return mSubItems[id]->getTRACE();
}

Scu_reg ScuGp::inTRACE( int id )
{
	return mSubItems[id]->inTRACE();
}


void ScuGp::setTRIG_DLY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setTRIG_DLY(value);} }
	else{ mSubItems[id]->setTRIG_DLY( value); }
}

void ScuGp::outTRIG_DLY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRIG_DLY(value);} }
	else{ mSubItems[id]->outTRIG_DLY( value); }
}

void ScuGp::outTRIG_DLY( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outTRIG_DLY();} }
	else{ mSubItems[id]->outTRIG_DLY(); }
}


Scu_reg ScuGp::getTRIG_DLY( int id )
{
	return mSubItems[id]->getTRIG_DLY();
}

Scu_reg ScuGp::inTRIG_DLY( int id )
{
	return mSubItems[id]->inTRIG_DLY();
}


void ScuGp::setPOS_SA_LAST_VIEW( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setPOS_SA_LAST_VIEW(value);} }
	else{ mSubItems[id]->setPOS_SA_LAST_VIEW( value); }
}

void ScuGp::outPOS_SA_LAST_VIEW( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPOS_SA_LAST_VIEW(value);} }
	else{ mSubItems[id]->outPOS_SA_LAST_VIEW( value); }
}

void ScuGp::outPOS_SA_LAST_VIEW( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outPOS_SA_LAST_VIEW();} }
	else{ mSubItems[id]->outPOS_SA_LAST_VIEW(); }
}


Scu_reg ScuGp::getPOS_SA_LAST_VIEW( int id )
{
	return mSubItems[id]->getPOS_SA_LAST_VIEW();
}

Scu_reg ScuGp::inPOS_SA_LAST_VIEW( int id )
{
	return mSubItems[id]->inPOS_SA_LAST_VIEW();
}


void ScuGp::setMASK_FRM_NUM_L( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMASK_FRM_NUM_L(value);} }
	else{ mSubItems[id]->setMASK_FRM_NUM_L( value); }
}

void ScuGp::outMASK_FRM_NUM_L( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_L(value);} }
	else{ mSubItems[id]->outMASK_FRM_NUM_L( value); }
}

void ScuGp::outMASK_FRM_NUM_L( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_L();} }
	else{ mSubItems[id]->outMASK_FRM_NUM_L(); }
}


Scu_reg ScuGp::getMASK_FRM_NUM_L( int id )
{
	return mSubItems[id]->getMASK_FRM_NUM_L();
}

Scu_reg ScuGp::inMASK_FRM_NUM_L( int id )
{
	return mSubItems[id]->inMASK_FRM_NUM_L();
}


void ScuGp::setMASK_FRM_NUM_H_frm_num_h(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMASK_FRM_NUM_H_frm_num_h(value);} }
	else{ mSubItems[id]->setMASK_FRM_NUM_H_frm_num_h( value); }
}
void ScuGp::setMASK_FRM_NUM_H_frm_num_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMASK_FRM_NUM_H_frm_num_en(value);} }
	else{ mSubItems[id]->setMASK_FRM_NUM_H_frm_num_en( value); }
}

void ScuGp::setMASK_FRM_NUM_H( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMASK_FRM_NUM_H(value);} }
	else{ mSubItems[id]->setMASK_FRM_NUM_H( value); }
}

void ScuGp::outMASK_FRM_NUM_H_frm_num_h( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_H_frm_num_h(value);}}
	else{ mSubItems[id]->outMASK_FRM_NUM_H_frm_num_h( value); }
}
void ScuGp::pulseMASK_FRM_NUM_H_frm_num_h( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_H_frm_num_h(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMASK_FRM_NUM_H_frm_num_h(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_FRM_NUM_H_frm_num_h( activeV, idleV, timeus ); }
}
void ScuGp::outMASK_FRM_NUM_H_frm_num_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_H_frm_num_en(value);}}
	else{ mSubItems[id]->outMASK_FRM_NUM_H_frm_num_en( value); }
}
void ScuGp::pulseMASK_FRM_NUM_H_frm_num_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_H_frm_num_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMASK_FRM_NUM_H_frm_num_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_FRM_NUM_H_frm_num_en( activeV, idleV, timeus ); }
}

void ScuGp::outMASK_FRM_NUM_H(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_H(value);} }
	else{ mSubItems[id]->outMASK_FRM_NUM_H( value); }
}

void ScuGp::outMASK_FRM_NUM_H( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_FRM_NUM_H();} }
	else{ mSubItems[id]->outMASK_FRM_NUM_H(); }
}


Scu_reg ScuGp::getMASK_FRM_NUM_H_frm_num_h( int id )
{
	return mSubItems[id]->getMASK_FRM_NUM_H_frm_num_h();
}
Scu_reg ScuGp::getMASK_FRM_NUM_H_frm_num_en( int id )
{
	return mSubItems[id]->getMASK_FRM_NUM_H_frm_num_en();
}

Scu_reg ScuGp::getMASK_FRM_NUM_H( int id )
{
	return mSubItems[id]->getMASK_FRM_NUM_H();
}

Scu_reg ScuGp::inMASK_FRM_NUM_H( int id )
{
	return mSubItems[id]->inMASK_FRM_NUM_H();
}


void ScuGp::setMASK_TIMEOUT_timeout(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMASK_TIMEOUT_timeout(value);} }
	else{ mSubItems[id]->setMASK_TIMEOUT_timeout( value); }
}
void ScuGp::setMASK_TIMEOUT_timeout_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMASK_TIMEOUT_timeout_en(value);} }
	else{ mSubItems[id]->setMASK_TIMEOUT_timeout_en( value); }
}

void ScuGp::setMASK_TIMEOUT( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setMASK_TIMEOUT(value);} }
	else{ mSubItems[id]->setMASK_TIMEOUT( value); }
}

void ScuGp::outMASK_TIMEOUT_timeout( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_TIMEOUT_timeout(value);}}
	else{ mSubItems[id]->outMASK_TIMEOUT_timeout( value); }
}
void ScuGp::pulseMASK_TIMEOUT_timeout( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_TIMEOUT_timeout(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMASK_TIMEOUT_timeout(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TIMEOUT_timeout( activeV, idleV, timeus ); }
}
void ScuGp::outMASK_TIMEOUT_timeout_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_TIMEOUT_timeout_en(value);}}
	else{ mSubItems[id]->outMASK_TIMEOUT_timeout_en( value); }
}
void ScuGp::pulseMASK_TIMEOUT_timeout_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_TIMEOUT_timeout_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignMASK_TIMEOUT_timeout_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TIMEOUT_timeout_en( activeV, idleV, timeus ); }
}

void ScuGp::outMASK_TIMEOUT(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_TIMEOUT(value);} }
	else{ mSubItems[id]->outMASK_TIMEOUT( value); }
}

void ScuGp::outMASK_TIMEOUT( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outMASK_TIMEOUT();} }
	else{ mSubItems[id]->outMASK_TIMEOUT(); }
}


Scu_reg ScuGp::getMASK_TIMEOUT_timeout( int id )
{
	return mSubItems[id]->getMASK_TIMEOUT_timeout();
}
Scu_reg ScuGp::getMASK_TIMEOUT_timeout_en( int id )
{
	return mSubItems[id]->getMASK_TIMEOUT_timeout_en();
}

Scu_reg ScuGp::getMASK_TIMEOUT( int id )
{
	return mSubItems[id]->getMASK_TIMEOUT();
}

Scu_reg ScuGp::inMASK_TIMEOUT( int id )
{
	return mSubItems[id]->inMASK_TIMEOUT();
}


void ScuGp::setSTATE_DISPLAY_scu_fsm(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_scu_fsm(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_scu_fsm( value); }
}
void ScuGp::setSTATE_DISPLAY_sys_pre_sa_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_sys_pre_sa_done(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_sys_pre_sa_done( value); }
}
void ScuGp::setSTATE_DISPLAY_sys_stop_state(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_sys_stop_state(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_sys_stop_state( value); }
}
void ScuGp::setSTATE_DISPLAY_disp_tpu_scu_trig(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_disp_tpu_scu_trig(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_disp_tpu_scu_trig( value); }
}
void ScuGp::setSTATE_DISPLAY_disp_sys_trig_d(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_disp_sys_trig_d(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_disp_sys_trig_d( value); }
}
void ScuGp::setSTATE_DISPLAY_rec_play_stop_state(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_rec_play_stop_state(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_rec_play_stop_state( value); }
}
void ScuGp::setSTATE_DISPLAY_mask_stop_state(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_mask_stop_state(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_mask_stop_state( value); }
}
void ScuGp::setSTATE_DISPLAY_stop_stata_clr(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_stop_stata_clr(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_stop_stata_clr( value); }
}
void ScuGp::setSTATE_DISPLAY_disp_trig_clr(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY_disp_trig_clr(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY_disp_trig_clr( value); }
}

void ScuGp::setSTATE_DISPLAY( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setSTATE_DISPLAY(value);} }
	else{ mSubItems[id]->setSTATE_DISPLAY( value); }
}

void ScuGp::outSTATE_DISPLAY_scu_fsm( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_scu_fsm(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_scu_fsm( value); }
}
void ScuGp::pulseSTATE_DISPLAY_scu_fsm( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_scu_fsm(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_scu_fsm(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_scu_fsm( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_sys_pre_sa_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_sys_pre_sa_done(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_sys_pre_sa_done( value); }
}
void ScuGp::pulseSTATE_DISPLAY_sys_pre_sa_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_sys_pre_sa_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_sys_pre_sa_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_sys_pre_sa_done( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_sys_stop_state( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_sys_stop_state(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_sys_stop_state( value); }
}
void ScuGp::pulseSTATE_DISPLAY_sys_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_sys_stop_state(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_sys_stop_state(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_sys_stop_state( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_disp_tpu_scu_trig(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_disp_tpu_scu_trig( value); }
}
void ScuGp::pulseSTATE_DISPLAY_disp_tpu_scu_trig( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_disp_tpu_scu_trig(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_disp_tpu_scu_trig(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_disp_tpu_scu_trig( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_disp_sys_trig_d( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_disp_sys_trig_d(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_disp_sys_trig_d( value); }
}
void ScuGp::pulseSTATE_DISPLAY_disp_sys_trig_d( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_disp_sys_trig_d(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_disp_sys_trig_d(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_disp_sys_trig_d( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_rec_play_stop_state( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_rec_play_stop_state(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_rec_play_stop_state( value); }
}
void ScuGp::pulseSTATE_DISPLAY_rec_play_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_rec_play_stop_state(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_rec_play_stop_state(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_rec_play_stop_state( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_mask_stop_state( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_mask_stop_state(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_mask_stop_state( value); }
}
void ScuGp::pulseSTATE_DISPLAY_mask_stop_state( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_mask_stop_state(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_mask_stop_state(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_mask_stop_state( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_stop_stata_clr( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_stop_stata_clr(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_stop_stata_clr( value); }
}
void ScuGp::pulseSTATE_DISPLAY_stop_stata_clr( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_stop_stata_clr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_stop_stata_clr(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_stop_stata_clr( activeV, idleV, timeus ); }
}
void ScuGp::outSTATE_DISPLAY_disp_trig_clr( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_disp_trig_clr(value);}}
	else{ mSubItems[id]->outSTATE_DISPLAY_disp_trig_clr( value); }
}
void ScuGp::pulseSTATE_DISPLAY_disp_trig_clr( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY_disp_trig_clr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignSTATE_DISPLAY_disp_trig_clr(idleV);}
	}
	else
	{ mSubItems[id]->pulseSTATE_DISPLAY_disp_trig_clr( activeV, idleV, timeus ); }
}

void ScuGp::outSTATE_DISPLAY(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY(value);} }
	else{ mSubItems[id]->outSTATE_DISPLAY( value); }
}

void ScuGp::outSTATE_DISPLAY( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outSTATE_DISPLAY();} }
	else{ mSubItems[id]->outSTATE_DISPLAY(); }
}


Scu_reg ScuGp::getSTATE_DISPLAY_scu_fsm( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_scu_fsm();
}
Scu_reg ScuGp::getSTATE_DISPLAY_sys_pre_sa_done( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_sys_pre_sa_done();
}
Scu_reg ScuGp::getSTATE_DISPLAY_sys_stop_state( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_sys_stop_state();
}
Scu_reg ScuGp::getSTATE_DISPLAY_disp_tpu_scu_trig( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_disp_tpu_scu_trig();
}
Scu_reg ScuGp::getSTATE_DISPLAY_disp_sys_trig_d( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_disp_sys_trig_d();
}
Scu_reg ScuGp::getSTATE_DISPLAY_rec_play_stop_state( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_rec_play_stop_state();
}
Scu_reg ScuGp::getSTATE_DISPLAY_mask_stop_state( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_mask_stop_state();
}
Scu_reg ScuGp::getSTATE_DISPLAY_stop_stata_clr( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_stop_stata_clr();
}
Scu_reg ScuGp::getSTATE_DISPLAY_disp_trig_clr( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY_disp_trig_clr();
}

Scu_reg ScuGp::getSTATE_DISPLAY( int id )
{
	return mSubItems[id]->getSTATE_DISPLAY();
}

Scu_reg ScuGp::inSTATE_DISPLAY( int id )
{
	return mSubItems[id]->inSTATE_DISPLAY();
}


Scu_reg ScuGp::getMASK_TIMER( int id )
{
	return mSubItems[id]->getMASK_TIMER();
}

Scu_reg ScuGp::inMASK_TIMER( int id )
{
	return mSubItems[id]->inMASK_TIMER();
}


void ScuGp::setCONFIG_ID( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setCONFIG_ID(value);} }
	else{ mSubItems[id]->setCONFIG_ID( value); }
}

void ScuGp::outCONFIG_ID( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCONFIG_ID(value);} }
	else{ mSubItems[id]->outCONFIG_ID( value); }
}

void ScuGp::outCONFIG_ID( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outCONFIG_ID();} }
	else{ mSubItems[id]->outCONFIG_ID(); }
}


Scu_reg ScuGp::getCONFIG_ID( int id )
{
	return mSubItems[id]->getCONFIG_ID();
}

Scu_reg ScuGp::inCONFIG_ID( int id )
{
	return mSubItems[id]->inCONFIG_ID();
}


void ScuGp::setDEUBG_wave_sa_ctrl(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_wave_sa_ctrl(value);} }
	else{ mSubItems[id]->setDEUBG_wave_sa_ctrl( value); }
}
void ScuGp::setDEUBG_sys_auto_trig_act(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_auto_trig_act(value);} }
	else{ mSubItems[id]->setDEUBG_sys_auto_trig_act( value); }
}
void ScuGp::setDEUBG_sys_auto_trig_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_auto_trig_en(value);} }
	else{ mSubItems[id]->setDEUBG_sys_auto_trig_en( value); }
}
void ScuGp::setDEUBG_sys_fine_trig_req(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_fine_trig_req(value);} }
	else{ mSubItems[id]->setDEUBG_sys_fine_trig_req( value); }
}
void ScuGp::setDEUBG_sys_meas_only(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_meas_only(value);} }
	else{ mSubItems[id]->setDEUBG_sys_meas_only( value); }
}
void ScuGp::setDEUBG_sys_meas_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_meas_en(value);} }
	else{ mSubItems[id]->setDEUBG_sys_meas_en( value); }
}
void ScuGp::setDEUBG_sys_meas_type(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_meas_type(value);} }
	else{ mSubItems[id]->setDEUBG_sys_meas_type( value); }
}
void ScuGp::setDEUBG_spu_tx_roll_null(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_spu_tx_roll_null(value);} }
	else{ mSubItems[id]->setDEUBG_spu_tx_roll_null( value); }
}
void ScuGp::setDEUBG_measure_once_done_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_measure_once_done_hold(value);} }
	else{ mSubItems[id]->setDEUBG_measure_once_done_hold( value); }
}
void ScuGp::setDEUBG_measure_finish_done_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_measure_finish_done_hold(value);} }
	else{ mSubItems[id]->setDEUBG_measure_finish_done_hold( value); }
}
void ScuGp::setDEUBG_sys_mask_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_mask_en(value);} }
	else{ mSubItems[id]->setDEUBG_sys_mask_en( value); }
}
void ScuGp::setDEUBG_mask_pf_result_vld_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_mask_pf_result_vld_hold(value);} }
	else{ mSubItems[id]->setDEUBG_mask_pf_result_vld_hold( value); }
}
void ScuGp::setDEUBG_mask_pf_result_cross_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_mask_pf_result_cross_hold(value);} }
	else{ mSubItems[id]->setDEUBG_mask_pf_result_cross_hold( value); }
}
void ScuGp::setDEUBG_mask_pf_timeout(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_mask_pf_timeout(value);} }
	else{ mSubItems[id]->setDEUBG_mask_pf_timeout( value); }
}
void ScuGp::setDEUBG_mask_frm_overflow(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_mask_frm_overflow(value);} }
	else{ mSubItems[id]->setDEUBG_mask_frm_overflow( value); }
}
void ScuGp::setDEUBG_measure_once_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_measure_once_buf(value);} }
	else{ mSubItems[id]->setDEUBG_measure_once_buf( value); }
}
void ScuGp::setDEUBG_zone_result_vld_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_zone_result_vld_hold(value);} }
	else{ mSubItems[id]->setDEUBG_zone_result_vld_hold( value); }
}
void ScuGp::setDEUBG_zone_result_trig_hold(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_zone_result_trig_hold(value);} }
	else{ mSubItems[id]->setDEUBG_zone_result_trig_hold( value); }
}
void ScuGp::setDEUBG_wave_proc_la(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_wave_proc_la(value);} }
	else{ mSubItems[id]->setDEUBG_wave_proc_la( value); }
}
void ScuGp::setDEUBG_sys_zoom(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_zoom(value);} }
	else{ mSubItems[id]->setDEUBG_sys_zoom( value); }
}
void ScuGp::setDEUBG_sys_fast_ref(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_fast_ref(value);} }
	else{ mSubItems[id]->setDEUBG_sys_fast_ref( value); }
}
void ScuGp::setDEUBG_spu_tx_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_spu_tx_done(value);} }
	else{ mSubItems[id]->setDEUBG_spu_tx_done( value); }
}
void ScuGp::setDEUBG_sys_avg_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_avg_en(value);} }
	else{ mSubItems[id]->setDEUBG_sys_avg_en( value); }
}
void ScuGp::setDEUBG_sys_trace(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_trace(value);} }
	else{ mSubItems[id]->setDEUBG_sys_trace( value); }
}
void ScuGp::setDEUBG_sys_pos_sa_last_frm(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_pos_sa_last_frm(value);} }
	else{ mSubItems[id]->setDEUBG_sys_pos_sa_last_frm( value); }
}
void ScuGp::setDEUBG_sys_pos_sa_view(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_sys_pos_sa_view(value);} }
	else{ mSubItems[id]->setDEUBG_sys_pos_sa_view( value); }
}
void ScuGp::setDEUBG_spu_wav_sa_rd_tx_done(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_spu_wav_sa_rd_tx_done(value);} }
	else{ mSubItems[id]->setDEUBG_spu_wav_sa_rd_tx_done( value); }
}
void ScuGp::setDEUBG_scan_roll_pos_trig_frm_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_scan_roll_pos_trig_frm_buf(value);} }
	else{ mSubItems[id]->setDEUBG_scan_roll_pos_trig_frm_buf( value); }
}
void ScuGp::setDEUBG_scan_roll_pos_last_frm_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG_scan_roll_pos_last_frm_buf(value);} }
	else{ mSubItems[id]->setDEUBG_scan_roll_pos_last_frm_buf( value); }
}

void ScuGp::setDEUBG( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEUBG(value);} }
	else{ mSubItems[id]->setDEUBG( value); }
}

void ScuGp::outDEUBG_wave_sa_ctrl( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_wave_sa_ctrl(value);}}
	else{ mSubItems[id]->outDEUBG_wave_sa_ctrl( value); }
}
void ScuGp::pulseDEUBG_wave_sa_ctrl( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_wave_sa_ctrl(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_wave_sa_ctrl(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_wave_sa_ctrl( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_auto_trig_act( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_auto_trig_act(value);}}
	else{ mSubItems[id]->outDEUBG_sys_auto_trig_act( value); }
}
void ScuGp::pulseDEUBG_sys_auto_trig_act( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_auto_trig_act(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_auto_trig_act(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_auto_trig_act( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_auto_trig_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_auto_trig_en(value);}}
	else{ mSubItems[id]->outDEUBG_sys_auto_trig_en( value); }
}
void ScuGp::pulseDEUBG_sys_auto_trig_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_auto_trig_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_auto_trig_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_auto_trig_en( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_fine_trig_req( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_fine_trig_req(value);}}
	else{ mSubItems[id]->outDEUBG_sys_fine_trig_req( value); }
}
void ScuGp::pulseDEUBG_sys_fine_trig_req( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_fine_trig_req(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_fine_trig_req(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_fine_trig_req( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_meas_only( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_meas_only(value);}}
	else{ mSubItems[id]->outDEUBG_sys_meas_only( value); }
}
void ScuGp::pulseDEUBG_sys_meas_only( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_meas_only(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_meas_only(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_meas_only( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_meas_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_meas_en(value);}}
	else{ mSubItems[id]->outDEUBG_sys_meas_en( value); }
}
void ScuGp::pulseDEUBG_sys_meas_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_meas_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_meas_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_meas_en( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_meas_type( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_meas_type(value);}}
	else{ mSubItems[id]->outDEUBG_sys_meas_type( value); }
}
void ScuGp::pulseDEUBG_sys_meas_type( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_meas_type(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_meas_type(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_meas_type( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_spu_tx_roll_null( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_spu_tx_roll_null(value);}}
	else{ mSubItems[id]->outDEUBG_spu_tx_roll_null( value); }
}
void ScuGp::pulseDEUBG_spu_tx_roll_null( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_spu_tx_roll_null(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_spu_tx_roll_null(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_spu_tx_roll_null( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_measure_once_done_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_measure_once_done_hold(value);}}
	else{ mSubItems[id]->outDEUBG_measure_once_done_hold( value); }
}
void ScuGp::pulseDEUBG_measure_once_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_measure_once_done_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_measure_once_done_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_measure_once_done_hold( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_measure_finish_done_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_measure_finish_done_hold(value);}}
	else{ mSubItems[id]->outDEUBG_measure_finish_done_hold( value); }
}
void ScuGp::pulseDEUBG_measure_finish_done_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_measure_finish_done_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_measure_finish_done_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_measure_finish_done_hold( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_mask_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_mask_en(value);}}
	else{ mSubItems[id]->outDEUBG_sys_mask_en( value); }
}
void ScuGp::pulseDEUBG_sys_mask_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_mask_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_mask_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_mask_en( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_mask_pf_result_vld_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_pf_result_vld_hold(value);}}
	else{ mSubItems[id]->outDEUBG_mask_pf_result_vld_hold( value); }
}
void ScuGp::pulseDEUBG_mask_pf_result_vld_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_pf_result_vld_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_mask_pf_result_vld_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_mask_pf_result_vld_hold( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_mask_pf_result_cross_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_pf_result_cross_hold(value);}}
	else{ mSubItems[id]->outDEUBG_mask_pf_result_cross_hold( value); }
}
void ScuGp::pulseDEUBG_mask_pf_result_cross_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_pf_result_cross_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_mask_pf_result_cross_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_mask_pf_result_cross_hold( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_mask_pf_timeout( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_pf_timeout(value);}}
	else{ mSubItems[id]->outDEUBG_mask_pf_timeout( value); }
}
void ScuGp::pulseDEUBG_mask_pf_timeout( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_pf_timeout(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_mask_pf_timeout(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_mask_pf_timeout( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_mask_frm_overflow( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_frm_overflow(value);}}
	else{ mSubItems[id]->outDEUBG_mask_frm_overflow( value); }
}
void ScuGp::pulseDEUBG_mask_frm_overflow( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_mask_frm_overflow(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_mask_frm_overflow(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_mask_frm_overflow( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_measure_once_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_measure_once_buf(value);}}
	else{ mSubItems[id]->outDEUBG_measure_once_buf( value); }
}
void ScuGp::pulseDEUBG_measure_once_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_measure_once_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_measure_once_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_measure_once_buf( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_zone_result_vld_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_zone_result_vld_hold(value);}}
	else{ mSubItems[id]->outDEUBG_zone_result_vld_hold( value); }
}
void ScuGp::pulseDEUBG_zone_result_vld_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_zone_result_vld_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_zone_result_vld_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_zone_result_vld_hold( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_zone_result_trig_hold( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_zone_result_trig_hold(value);}}
	else{ mSubItems[id]->outDEUBG_zone_result_trig_hold( value); }
}
void ScuGp::pulseDEUBG_zone_result_trig_hold( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_zone_result_trig_hold(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_zone_result_trig_hold(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_zone_result_trig_hold( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_wave_proc_la( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_wave_proc_la(value);}}
	else{ mSubItems[id]->outDEUBG_wave_proc_la( value); }
}
void ScuGp::pulseDEUBG_wave_proc_la( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_wave_proc_la(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_wave_proc_la(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_wave_proc_la( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_zoom( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_zoom(value);}}
	else{ mSubItems[id]->outDEUBG_sys_zoom( value); }
}
void ScuGp::pulseDEUBG_sys_zoom( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_zoom(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_zoom(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_zoom( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_fast_ref( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_fast_ref(value);}}
	else{ mSubItems[id]->outDEUBG_sys_fast_ref( value); }
}
void ScuGp::pulseDEUBG_sys_fast_ref( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_fast_ref(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_fast_ref(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_fast_ref( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_spu_tx_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_spu_tx_done(value);}}
	else{ mSubItems[id]->outDEUBG_spu_tx_done( value); }
}
void ScuGp::pulseDEUBG_spu_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_spu_tx_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_spu_tx_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_spu_tx_done( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_avg_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_avg_en(value);}}
	else{ mSubItems[id]->outDEUBG_sys_avg_en( value); }
}
void ScuGp::pulseDEUBG_sys_avg_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_avg_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_avg_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_avg_en( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_trace( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_trace(value);}}
	else{ mSubItems[id]->outDEUBG_sys_trace( value); }
}
void ScuGp::pulseDEUBG_sys_trace( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_trace(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_trace(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_trace( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_pos_sa_last_frm( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_pos_sa_last_frm(value);}}
	else{ mSubItems[id]->outDEUBG_sys_pos_sa_last_frm( value); }
}
void ScuGp::pulseDEUBG_sys_pos_sa_last_frm( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_pos_sa_last_frm(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_pos_sa_last_frm(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_pos_sa_last_frm( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_sys_pos_sa_view( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_pos_sa_view(value);}}
	else{ mSubItems[id]->outDEUBG_sys_pos_sa_view( value); }
}
void ScuGp::pulseDEUBG_sys_pos_sa_view( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_sys_pos_sa_view(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_sys_pos_sa_view(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_sys_pos_sa_view( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_spu_wav_sa_rd_tx_done( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_spu_wav_sa_rd_tx_done(value);}}
	else{ mSubItems[id]->outDEUBG_spu_wav_sa_rd_tx_done( value); }
}
void ScuGp::pulseDEUBG_spu_wav_sa_rd_tx_done( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_spu_wav_sa_rd_tx_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_spu_wav_sa_rd_tx_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_spu_wav_sa_rd_tx_done( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_scan_roll_pos_trig_frm_buf(value);}}
	else{ mSubItems[id]->outDEUBG_scan_roll_pos_trig_frm_buf( value); }
}
void ScuGp::pulseDEUBG_scan_roll_pos_trig_frm_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_scan_roll_pos_trig_frm_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_scan_roll_pos_trig_frm_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_scan_roll_pos_trig_frm_buf( activeV, idleV, timeus ); }
}
void ScuGp::outDEUBG_scan_roll_pos_last_frm_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_scan_roll_pos_last_frm_buf(value);}}
	else{ mSubItems[id]->outDEUBG_scan_roll_pos_last_frm_buf( value); }
}
void ScuGp::pulseDEUBG_scan_roll_pos_last_frm_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG_scan_roll_pos_last_frm_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEUBG_scan_roll_pos_last_frm_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEUBG_scan_roll_pos_last_frm_buf( activeV, idleV, timeus ); }
}

void ScuGp::outDEUBG(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG(value);} }
	else{ mSubItems[id]->outDEUBG( value); }
}

void ScuGp::outDEUBG( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEUBG();} }
	else{ mSubItems[id]->outDEUBG(); }
}


Scu_reg ScuGp::getDEUBG_wave_sa_ctrl( int id )
{
	return mSubItems[id]->getDEUBG_wave_sa_ctrl();
}
Scu_reg ScuGp::getDEUBG_sys_auto_trig_act( int id )
{
	return mSubItems[id]->getDEUBG_sys_auto_trig_act();
}
Scu_reg ScuGp::getDEUBG_sys_auto_trig_en( int id )
{
	return mSubItems[id]->getDEUBG_sys_auto_trig_en();
}
Scu_reg ScuGp::getDEUBG_sys_fine_trig_req( int id )
{
	return mSubItems[id]->getDEUBG_sys_fine_trig_req();
}
Scu_reg ScuGp::getDEUBG_sys_meas_only( int id )
{
	return mSubItems[id]->getDEUBG_sys_meas_only();
}
Scu_reg ScuGp::getDEUBG_sys_meas_en( int id )
{
	return mSubItems[id]->getDEUBG_sys_meas_en();
}
Scu_reg ScuGp::getDEUBG_sys_meas_type( int id )
{
	return mSubItems[id]->getDEUBG_sys_meas_type();
}
Scu_reg ScuGp::getDEUBG_spu_tx_roll_null( int id )
{
	return mSubItems[id]->getDEUBG_spu_tx_roll_null();
}
Scu_reg ScuGp::getDEUBG_measure_once_done_hold( int id )
{
	return mSubItems[id]->getDEUBG_measure_once_done_hold();
}
Scu_reg ScuGp::getDEUBG_measure_finish_done_hold( int id )
{
	return mSubItems[id]->getDEUBG_measure_finish_done_hold();
}
Scu_reg ScuGp::getDEUBG_sys_mask_en( int id )
{
	return mSubItems[id]->getDEUBG_sys_mask_en();
}
Scu_reg ScuGp::getDEUBG_mask_pf_result_vld_hold( int id )
{
	return mSubItems[id]->getDEUBG_mask_pf_result_vld_hold();
}
Scu_reg ScuGp::getDEUBG_mask_pf_result_cross_hold( int id )
{
	return mSubItems[id]->getDEUBG_mask_pf_result_cross_hold();
}
Scu_reg ScuGp::getDEUBG_mask_pf_timeout( int id )
{
	return mSubItems[id]->getDEUBG_mask_pf_timeout();
}
Scu_reg ScuGp::getDEUBG_mask_frm_overflow( int id )
{
	return mSubItems[id]->getDEUBG_mask_frm_overflow();
}
Scu_reg ScuGp::getDEUBG_measure_once_buf( int id )
{
	return mSubItems[id]->getDEUBG_measure_once_buf();
}
Scu_reg ScuGp::getDEUBG_zone_result_vld_hold( int id )
{
	return mSubItems[id]->getDEUBG_zone_result_vld_hold();
}
Scu_reg ScuGp::getDEUBG_zone_result_trig_hold( int id )
{
	return mSubItems[id]->getDEUBG_zone_result_trig_hold();
}
Scu_reg ScuGp::getDEUBG_wave_proc_la( int id )
{
	return mSubItems[id]->getDEUBG_wave_proc_la();
}
Scu_reg ScuGp::getDEUBG_sys_zoom( int id )
{
	return mSubItems[id]->getDEUBG_sys_zoom();
}
Scu_reg ScuGp::getDEUBG_sys_fast_ref( int id )
{
	return mSubItems[id]->getDEUBG_sys_fast_ref();
}
Scu_reg ScuGp::getDEUBG_spu_tx_done( int id )
{
	return mSubItems[id]->getDEUBG_spu_tx_done();
}
Scu_reg ScuGp::getDEUBG_sys_avg_en( int id )
{
	return mSubItems[id]->getDEUBG_sys_avg_en();
}
Scu_reg ScuGp::getDEUBG_sys_trace( int id )
{
	return mSubItems[id]->getDEUBG_sys_trace();
}
Scu_reg ScuGp::getDEUBG_sys_pos_sa_last_frm( int id )
{
	return mSubItems[id]->getDEUBG_sys_pos_sa_last_frm();
}
Scu_reg ScuGp::getDEUBG_sys_pos_sa_view( int id )
{
	return mSubItems[id]->getDEUBG_sys_pos_sa_view();
}
Scu_reg ScuGp::getDEUBG_spu_wav_sa_rd_tx_done( int id )
{
	return mSubItems[id]->getDEUBG_spu_wav_sa_rd_tx_done();
}
Scu_reg ScuGp::getDEUBG_scan_roll_pos_trig_frm_buf( int id )
{
	return mSubItems[id]->getDEUBG_scan_roll_pos_trig_frm_buf();
}
Scu_reg ScuGp::getDEUBG_scan_roll_pos_last_frm_buf( int id )
{
	return mSubItems[id]->getDEUBG_scan_roll_pos_last_frm_buf();
}

Scu_reg ScuGp::getDEUBG( int id )
{
	return mSubItems[id]->getDEUBG();
}

Scu_reg ScuGp::inDEUBG( int id )
{
	return mSubItems[id]->inDEUBG();
}


Scu_reg ScuGp::getDEBUG_GT_receiver_gtu_state( int id )
{
	return mSubItems[id]->getDEBUG_GT_receiver_gtu_state();
}

Scu_reg ScuGp::getDEBUG_GT( int id )
{
	return mSubItems[id]->getDEBUG_GT();
}

Scu_reg ScuGp::inDEBUG_GT( int id )
{
	return mSubItems[id]->inDEBUG_GT();
}


void ScuGp::setDEBUG_PLOT_DONE_wpu_plot_done_dly(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_PLOT_DONE_wpu_plot_done_dly(value);} }
	else{ mSubItems[id]->setDEBUG_PLOT_DONE_wpu_plot_done_dly( value); }
}

void ScuGp::setDEBUG_PLOT_DONE( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_PLOT_DONE(value);} }
	else{ mSubItems[id]->setDEBUG_PLOT_DONE( value); }
}

void ScuGp::outDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_PLOT_DONE_wpu_plot_done_dly(value);}}
	else{ mSubItems[id]->outDEBUG_PLOT_DONE_wpu_plot_done_dly( value); }
}
void ScuGp::pulseDEBUG_PLOT_DONE_wpu_plot_done_dly( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_PLOT_DONE_wpu_plot_done_dly(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_PLOT_DONE_wpu_plot_done_dly(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_PLOT_DONE_wpu_plot_done_dly( activeV, idleV, timeus ); }
}

void ScuGp::outDEBUG_PLOT_DONE(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_PLOT_DONE(value);} }
	else{ mSubItems[id]->outDEBUG_PLOT_DONE( value); }
}

void ScuGp::outDEBUG_PLOT_DONE( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_PLOT_DONE();} }
	else{ mSubItems[id]->outDEBUG_PLOT_DONE(); }
}


Scu_reg ScuGp::getDEBUG_PLOT_DONE_wpu_plot_done_dly( int id )
{
	return mSubItems[id]->getDEBUG_PLOT_DONE_wpu_plot_done_dly();
}

Scu_reg ScuGp::getDEBUG_PLOT_DONE( int id )
{
	return mSubItems[id]->getDEBUG_PLOT_DONE();
}

Scu_reg ScuGp::inDEBUG_PLOT_DONE( int id )
{
	return mSubItems[id]->inDEBUG_PLOT_DONE();
}


void ScuGp::setFPGA_SYNC_sync_cnt(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setFPGA_SYNC_sync_cnt(value);} }
	else{ mSubItems[id]->setFPGA_SYNC_sync_cnt( value); }
}
void ScuGp::setFPGA_SYNC_debug_scu_reset(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setFPGA_SYNC_debug_scu_reset(value);} }
	else{ mSubItems[id]->setFPGA_SYNC_debug_scu_reset( value); }
}
void ScuGp::setFPGA_SYNC_debug_sys_rst_cnt(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setFPGA_SYNC_debug_sys_rst_cnt(value);} }
	else{ mSubItems[id]->setFPGA_SYNC_debug_sys_rst_cnt( value); }
}

void ScuGp::setFPGA_SYNC( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setFPGA_SYNC(value);} }
	else{ mSubItems[id]->setFPGA_SYNC( value); }
}

void ScuGp::outFPGA_SYNC_sync_cnt( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC_sync_cnt(value);}}
	else{ mSubItems[id]->outFPGA_SYNC_sync_cnt( value); }
}
void ScuGp::pulseFPGA_SYNC_sync_cnt( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC_sync_cnt(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignFPGA_SYNC_sync_cnt(idleV);}
	}
	else
	{ mSubItems[id]->pulseFPGA_SYNC_sync_cnt( activeV, idleV, timeus ); }
}
void ScuGp::outFPGA_SYNC_debug_scu_reset( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC_debug_scu_reset(value);}}
	else{ mSubItems[id]->outFPGA_SYNC_debug_scu_reset( value); }
}
void ScuGp::pulseFPGA_SYNC_debug_scu_reset( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC_debug_scu_reset(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignFPGA_SYNC_debug_scu_reset(idleV);}
	}
	else
	{ mSubItems[id]->pulseFPGA_SYNC_debug_scu_reset( activeV, idleV, timeus ); }
}
void ScuGp::outFPGA_SYNC_debug_sys_rst_cnt( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC_debug_sys_rst_cnt(value);}}
	else{ mSubItems[id]->outFPGA_SYNC_debug_sys_rst_cnt( value); }
}
void ScuGp::pulseFPGA_SYNC_debug_sys_rst_cnt( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC_debug_sys_rst_cnt(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignFPGA_SYNC_debug_sys_rst_cnt(idleV);}
	}
	else
	{ mSubItems[id]->pulseFPGA_SYNC_debug_sys_rst_cnt( activeV, idleV, timeus ); }
}

void ScuGp::outFPGA_SYNC(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC(value);} }
	else{ mSubItems[id]->outFPGA_SYNC( value); }
}

void ScuGp::outFPGA_SYNC( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outFPGA_SYNC();} }
	else{ mSubItems[id]->outFPGA_SYNC(); }
}


Scu_reg ScuGp::getFPGA_SYNC_sync_cnt( int id )
{
	return mSubItems[id]->getFPGA_SYNC_sync_cnt();
}
Scu_reg ScuGp::getFPGA_SYNC_debug_scu_reset( int id )
{
	return mSubItems[id]->getFPGA_SYNC_debug_scu_reset();
}
Scu_reg ScuGp::getFPGA_SYNC_debug_sys_rst_cnt( int id )
{
	return mSubItems[id]->getFPGA_SYNC_debug_sys_rst_cnt();
}

Scu_reg ScuGp::getFPGA_SYNC( int id )
{
	return mSubItems[id]->getFPGA_SYNC();
}

Scu_reg ScuGp::inFPGA_SYNC( int id )
{
	return mSubItems[id]->inFPGA_SYNC();
}


Scu_reg ScuGp::getWPU_ID_sys_wpu_plot_id( int id )
{
	return mSubItems[id]->getWPU_ID_sys_wpu_plot_id();
}
Scu_reg ScuGp::getWPU_ID_wpu_plot_done_id( int id )
{
	return mSubItems[id]->getWPU_ID_wpu_plot_done_id();
}
Scu_reg ScuGp::getWPU_ID_wpu_display_done_id( int id )
{
	return mSubItems[id]->getWPU_ID_wpu_display_done_id();
}

Scu_reg ScuGp::getWPU_ID( int id )
{
	return mSubItems[id]->getWPU_ID();
}

Scu_reg ScuGp::inWPU_ID( int id )
{
	return mSubItems[id]->inWPU_ID();
}


void ScuGp::setDEBUG_WPU_PLOT_wpu_plot_display_finish(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_wpu_plot_display_finish(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_wpu_plot_display_finish( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_wpu_fast_done_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_wpu_fast_done_buf(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_wpu_fast_done_buf( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_wpu_display_done_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_wpu_display_done_buf(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_wpu_display_done_buf( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_wpu_plot_display_id_equ(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_wpu_plot_display_id_equ(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_wpu_plot_display_id_equ( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_scu_wpu_plot_last(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_scu_wpu_plot_last(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_scu_wpu_plot_last( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_scu_wave_plot_req(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_scu_wave_plot_req(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_scu_wave_plot_req( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_scu_wpu_start(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_scu_wpu_start(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_scu_wpu_start( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_sys_wpu_plot(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_sys_wpu_plot(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_sys_wpu_plot( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_wpu_plot_done_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_wpu_plot_done_buf(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_wpu_plot_done_buf( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_scu_wpu_plot_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_scu_wpu_plot_en(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_scu_wpu_plot_en( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_cfg_wpu_flush(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_cfg_wpu_flush(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_cfg_wpu_flush( value); }
}
void ScuGp::setDEBUG_WPU_PLOT_cfg_stop_plot(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT_cfg_stop_plot(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT_cfg_stop_plot( value); }
}

void ScuGp::setDEBUG_WPU_PLOT( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_WPU_PLOT(value);} }
	else{ mSubItems[id]->setDEBUG_WPU_PLOT( value); }
}

void ScuGp::outDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_display_finish(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_wpu_plot_display_finish( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_wpu_plot_display_finish( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_display_finish(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_wpu_plot_display_finish(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_wpu_plot_display_finish( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_fast_done_buf(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_wpu_fast_done_buf( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_wpu_fast_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_fast_done_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_wpu_fast_done_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_wpu_fast_done_buf( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_display_done_buf(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_wpu_display_done_buf( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_wpu_display_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_display_done_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_wpu_display_done_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_wpu_display_done_buf( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_display_id_equ(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_wpu_plot_display_id_equ( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_display_id_equ(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_wpu_plot_display_id_equ(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_wpu_plot_display_id_equ( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_plot_last(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_scu_wpu_plot_last( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_scu_wpu_plot_last( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_plot_last(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_scu_wpu_plot_last(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_scu_wpu_plot_last( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wave_plot_req(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_scu_wave_plot_req( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_scu_wave_plot_req( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wave_plot_req(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_scu_wave_plot_req(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_scu_wave_plot_req( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_start(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_scu_wpu_start( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_scu_wpu_start( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_start(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_scu_wpu_start(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_scu_wpu_start( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_sys_wpu_plot(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_sys_wpu_plot( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_sys_wpu_plot( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_sys_wpu_plot(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_sys_wpu_plot(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_sys_wpu_plot( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_done_buf(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_wpu_plot_done_buf( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_wpu_plot_done_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_done_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_wpu_plot_done_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_wpu_plot_done_buf( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_plot_en(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_scu_wpu_plot_en( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_scu_wpu_plot_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_plot_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_scu_wpu_plot_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_scu_wpu_plot_en( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_cfg_wpu_flush(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_cfg_wpu_flush( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_cfg_wpu_flush( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_cfg_wpu_flush(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_cfg_wpu_flush(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_cfg_wpu_flush( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_cfg_stop_plot(value);}}
	else{ mSubItems[id]->outDEBUG_WPU_PLOT_cfg_stop_plot( value); }
}
void ScuGp::pulseDEBUG_WPU_PLOT_cfg_stop_plot( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT_cfg_stop_plot(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_WPU_PLOT_cfg_stop_plot(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_WPU_PLOT_cfg_stop_plot( activeV, idleV, timeus ); }
}

void ScuGp::outDEBUG_WPU_PLOT(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT(value);} }
	else{ mSubItems[id]->outDEBUG_WPU_PLOT( value); }
}

void ScuGp::outDEBUG_WPU_PLOT( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_WPU_PLOT();} }
	else{ mSubItems[id]->outDEBUG_WPU_PLOT(); }
}


Scu_reg ScuGp::getDEBUG_WPU_PLOT_wpu_plot_display_finish( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_wpu_plot_display_finish();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_wpu_fast_done_buf( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_wpu_fast_done_buf();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_wpu_display_done_buf( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_wpu_display_done_buf();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_wpu_plot_display_id_equ( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_wpu_plot_display_id_equ();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_scu_wpu_plot_last( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_scu_wpu_plot_last();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_scu_wave_plot_req( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_scu_wave_plot_req();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_scu_wpu_start( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_scu_wpu_start();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_sys_wpu_plot( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_sys_wpu_plot();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_wpu_plot_done_buf( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_wpu_plot_done_buf();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_scu_wpu_plot_en( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_scu_wpu_plot_en();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_scu_wpu_stop_plot_en();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_cfg_wpu_flush( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_cfg_wpu_flush();
}
Scu_reg ScuGp::getDEBUG_WPU_PLOT_cfg_stop_plot( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT_cfg_stop_plot();
}

Scu_reg ScuGp::getDEBUG_WPU_PLOT( int id )
{
	return mSubItems[id]->getDEBUG_WPU_PLOT();
}

Scu_reg ScuGp::inDEBUG_WPU_PLOT( int id )
{
	return mSubItems[id]->inDEBUG_WPU_PLOT();
}


void ScuGp::setDEBUG_GT_CRC_fail_sum(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_GT_CRC_fail_sum(value);} }
	else{ mSubItems[id]->setDEBUG_GT_CRC_fail_sum( value); }
}
void ScuGp::setDEBUG_GT_CRC_crc_pass_fail_n_i(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_GT_CRC_crc_pass_fail_n_i(value);} }
	else{ mSubItems[id]->setDEBUG_GT_CRC_crc_pass_fail_n_i( value); }
}
void ScuGp::setDEBUG_GT_CRC_crc_valid_i(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_GT_CRC_crc_valid_i(value);} }
	else{ mSubItems[id]->setDEBUG_GT_CRC_crc_valid_i( value); }
}

void ScuGp::setDEBUG_GT_CRC( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->setDEBUG_GT_CRC(value);} }
	else{ mSubItems[id]->setDEBUG_GT_CRC( value); }
}

void ScuGp::outDEBUG_GT_CRC_fail_sum( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC_fail_sum(value);}}
	else{ mSubItems[id]->outDEBUG_GT_CRC_fail_sum( value); }
}
void ScuGp::pulseDEBUG_GT_CRC_fail_sum( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC_fail_sum(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_GT_CRC_fail_sum(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_GT_CRC_fail_sum( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC_crc_pass_fail_n_i(value);}}
	else{ mSubItems[id]->outDEBUG_GT_CRC_crc_pass_fail_n_i( value); }
}
void ScuGp::pulseDEBUG_GT_CRC_crc_pass_fail_n_i( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC_crc_pass_fail_n_i(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_GT_CRC_crc_pass_fail_n_i(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_GT_CRC_crc_pass_fail_n_i( activeV, idleV, timeus ); }
}
void ScuGp::outDEBUG_GT_CRC_crc_valid_i( Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC_crc_valid_i(value);}}
	else{ mSubItems[id]->outDEBUG_GT_CRC_crc_valid_i( value); }
}
void ScuGp::pulseDEBUG_GT_CRC_crc_valid_i( Scu_reg activeV, Scu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC_crc_valid_i(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhyScu *pItem, mSubItems){ pItem->assignDEBUG_GT_CRC_crc_valid_i(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_GT_CRC_crc_valid_i( activeV, idleV, timeus ); }
}

void ScuGp::outDEBUG_GT_CRC(Scu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC(value);} }
	else{ mSubItems[id]->outDEBUG_GT_CRC( value); }
}

void ScuGp::outDEBUG_GT_CRC( int id )
{
	if ( id == -1 ){ foreach( IPhyScu *pItem, mSubItems){ pItem->outDEBUG_GT_CRC();} }
	else{ mSubItems[id]->outDEBUG_GT_CRC(); }
}


Scu_reg ScuGp::getDEBUG_GT_CRC_fail_sum( int id )
{
	return mSubItems[id]->getDEBUG_GT_CRC_fail_sum();
}
Scu_reg ScuGp::getDEBUG_GT_CRC_crc_pass_fail_n_i( int id )
{
	return mSubItems[id]->getDEBUG_GT_CRC_crc_pass_fail_n_i();
}
Scu_reg ScuGp::getDEBUG_GT_CRC_crc_valid_i( int id )
{
	return mSubItems[id]->getDEBUG_GT_CRC_crc_valid_i();
}

Scu_reg ScuGp::getDEBUG_GT_CRC( int id )
{
	return mSubItems[id]->getDEBUG_GT_CRC();
}

Scu_reg ScuGp::inDEBUG_GT_CRC( int id )
{
	return mSubItems[id]->inDEBUG_GT_CRC();
}


